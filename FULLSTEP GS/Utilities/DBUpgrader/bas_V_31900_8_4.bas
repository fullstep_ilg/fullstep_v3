Attribute VB_Name = "bas_V_31900_8_4"
Option Explicit

Public Function CodigoDeActualizacion31900_08_1909_A31900_08_2000() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim oADOConnection As ADODB.Connection
    Dim sConsulta As String

    On Error GoTo Error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
                  
    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Actualiza Tablas"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    frmProgreso.ProgressBar1.Refresh
    V_31900_8_Storeds_200
    
    sConsulta = "UPDATE VERSION SET NUM ='3.00.20.000'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103), PRODVER='31900.8'"
    ExecuteSQL gRDOCon, sConsulta
        
    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False
        
    CodigoDeActualizacion31900_08_1909_A31900_08_2000 = True
    Exit Function

Error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    CodigoDeActualizacion31900_08_1909_A31900_08_2000 = False
End Function

Public Sub V_31900_8_Storeds_200()
    Dim sConsulta As String
            
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSQA_GET_CERTIFICADOS]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSQA_GET_CERTIFICADOS]" & vbCrLf
    gRDOCon.QueryTimeout = 7200
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "CREATE PROCEDURE [dbo].[FSQA_GET_CERTIFICADOS] " & vbCrLf
    sConsulta = sConsulta & "   @IDI NVARCHAR(20), " & vbCrLf
    sConsulta = sConsulta & "   @USU NVARCHAR(50)," & vbCrLf
    sConsulta = sConsulta & "   @PER NVARCHAR(50)," & vbCrLf
    sConsulta = sConsulta & "   @UON1 NVARCHAR(50)=NULL," & vbCrLf
    sConsulta = sConsulta & "   @UON2 NVARCHAR(50)=NULL," & vbCrLf
    sConsulta = sConsulta & "   @UON3 NVARCHAR(50)=NULL," & vbCrLf
    sConsulta = sConsulta & "   @DEP NVARCHAR(50)=NULL," & vbCrLf
    sConsulta = sConsulta & "   @FECDESDE DATE=NULL," & vbCrLf
    sConsulta = sConsulta & "   @FECHASTA DATE =NULL," & vbCrLf
    sConsulta = sConsulta & "   @TIPOPROV INT=3," & vbCrLf
    sConsulta = sConsulta & "   @BAJAPROV INT=0," & vbCrLf
    sConsulta = sConsulta & "   @CODPROV NVARCHAR(50)=NULL, " & vbCrLf
    sConsulta = sConsulta & "   @CERTIFICADOS NVARCHAR(1000)=NULL," & vbCrLf
    sConsulta = sConsulta & "   @MATERIALES NVARCHAR(1000)=NULL, " & vbCrLf
    sConsulta = sConsulta & "   @CERTIFBAJA TINYINT=0," & vbCrLf
    sConsulta = sConsulta & "   @ESTADO NVARCHAR(50)=NULL," & vbCrLf
    sConsulta = sConsulta & "   @PETICIONARIO NVARCHAR(50)," & vbCrLf
    sConsulta = sConsulta & "   @QUEFECHA TINYINT=1," & vbCrLf
    sConsulta = sConsulta & "   @NOMBRE_TABLA NVARCHAR(50)=''," & vbCrLf
    sConsulta = sConsulta & "   @ORDEN NVARCHAR(1000)=NULL, " & vbCrLf
    sConsulta = sConsulta & "   @SOLOCONTADORES TINYINT=0," & vbCrLf
    sConsulta = sConsulta & "   @PAGENUMBER INT=1," & vbCrLf
    sConsulta = sConsulta & "   @PAGESIZE INT=1000," & vbCrLf
    sConsulta = sConsulta & "   @OBTENERTODASRELACIONES TINYINT=1," & vbCrLf
    sConsulta = sConsulta & "   @OBTENERTODOSCERTIFICADOS TINYINT=0," & vbCrLf
    sConsulta = sConsulta & "   @REST_SOLICIT_USU TINYINT=0," & vbCrLf
    sConsulta = sConsulta & "   @REST_SOLICIT_UO TINYINT=0," & vbCrLf
    sConsulta = sConsulta & "   @REST_SOLICIT_DEP TINYINT=0," & vbCrLf
    sConsulta = sConsulta & "   @REST_PROV_MAT TINYINT=0," & vbCrLf
    sConsulta = sConsulta & "   @REST_PROV_EQP TINYINT=0," & vbCrLf
    sConsulta = sConsulta & "   @REST_PROV_CON TINYINT=0 " & vbCrLf
    sConsulta = sConsulta & "AS" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL AS NVARCHAR(MAX)" & vbCrLf
    sConsulta = sConsulta & "SET @SQL=''" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SEL AS NVARCHAR(MAX)" & vbCrLf
    sConsulta = sConsulta & "SET @SEL=''" & vbCrLf
    sConsulta = sConsulta & "DECLARE @JOIN AS NVARCHAR(MAX)" & vbCrLf
    sConsulta = sConsulta & "SET @JOIN=''" & vbCrLf
    sConsulta = sConsulta & "DECLARE @JOIN2 AS NVARCHAR(MAX)" & vbCrLf
    sConsulta = sConsulta & "SET @JOIN2=''" & vbCrLf
    sConsulta = sConsulta & "DECLARE @JOIN3 AS NVARCHAR(MAX)" & vbCrLf
    sConsulta = sConsulta & "SET @JOIN3=''" & vbCrLf
    sConsulta = sConsulta & "DECLARE @WHERE AS NVARCHAR(MAX)" & vbCrLf
    sConsulta = sConsulta & "SET @WHERE=''" & vbCrLf
    sConsulta = sConsulta & "DECLARE @WHERE3 AS NVARCHAR(MAX)" & vbCrLf
    sConsulta = sConsulta & "SET @WHERE3=''" & vbCrLf
    sConsulta = sConsulta & "DECLARE @JOIN_RESTRIC_SINSOLIC AS NVARCHAR(MAX)" & vbCrLf
    sConsulta = sConsulta & "SET @JOIN_RESTRIC_SINSOLIC =N''" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FSQA_PROX_EXPIRAR INT" & vbCrLf
    sConsulta = sConsulta & "DECLARE @WHESTADO NVARCHAR(1000)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @HAYESTADO TINYINT" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FILTROFECHA AS  NVARCHAR(50)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL2 AS NVARCHAR(MAX)" & vbCrLf
    sConsulta = sConsulta & "SET @SQL2='' --Contiene lo q devuelva la 1ra select. La q saca los certificados solic y no solic de la pagina correspondiente. " & vbCrLf
    sConsulta = sConsulta & "--As� tendre para la 3ra select, la de materiales, los @PAGESIZE registros correspondidentes, de esta info saco los proveedores y tipo_certificado" & vbCrLf
    sConsulta = sConsulta & "--para ir a material_qa_certificados." & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "--CAMBIOS" & vbCrLf
    sConsulta = sConsulta & "--1� De la Select de certificados quito PUBLICADA_DEN,REAL_DEN,BAJA_DEN,MODO_SOLIC_DEN" & vbCrLf
    sConsulta = sConsulta & "--" & vbCrLf
    sConsulta = sConsulta & "--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" & vbCrLf
    sConsulta = sConsulta & "--Pasar por par�metro" & vbCrLf
    sConsulta = sConsulta & "--el par�metro FSQA_PROX_EXPIRAR, se podia obtener en FSNServer/Root/GetAccessType>Parametros_GetData> FSPM_GETPARAMETROS, eliminar la SELECT cd se haga" & vbCrLf
    sConsulta = sConsulta & "--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" & vbCrLf
    sConsulta = sConsulta & "SELECT @FSQA_PROX_EXPIRAR=FSQA_PROX_EXPIRAR+1 FROM PARGEN_QA WITH(NOLOCK) WHERE ID=1" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "--***********************************************************************************************************" & vbCrLf
    sConsulta = sConsulta & "--1� SELECT PARTES COMUNES" & vbCrLf
    sConsulta = sConsulta & "--***********************************************************************************************************" & vbCrLf
    sConsulta = sConsulta & "SET @SEL='SELECT DISTINCT" & vbCrLf
    sConsulta = sConsulta & "   C.NUM_VERSION,C.ID ID_CERTIF,C.PUBLICADA,C.FEC_DESPUB,C.FEC_SOLICITUD,C.FEC_RESPUESTA,C.FEC_EXPIRACION,C.FEC_LIM_CUMPLIM" & vbCrLf
    sConsulta = sConsulta & "   ,C.PER,C.FEC_PUBLICACION,C.FECACT FEC_ACT,C.CERTIFICADO,C.FEC_CUMPLIM" & vbCrLf
    sConsulta = sConsulta & "   ,PR.COD PROVE_COD,PR.DEN PROVE_DEN,ISNULL(PR.CALIDAD_POT,0) REAL,ISNULL(PR.BAJA_CALIDAD,0) BAJA" & vbCrLf
    sConsulta = sConsulta & "   ,S.ID AS TIPO_CERTIFICADO,S.DEN_SPA DEN_TIPO_CERTIFICADO,ISNULL(S.PLAZO_CUMPL,0) PLAZO_CUMPL,~S.OPCIONAL OBLIGATORIO,S.MODO_SOLIC" & vbCrLf
    sConsulta = sConsulta & "   ,PT.NOM + '' '' + PT.APE NOMBREPETICIONARIO " & vbCrLf
    sConsulta = sConsulta & "   ,ISNULL(I.EN_PROCESO,0) EN_PROCESO" & vbCrLf
    sConsulta = sConsulta & "   ,PR.COD + ''#'' + CAST(S.ID AS VARCHAR(10)) AS DATAKEY" & vbCrLf
    sConsulta = sConsulta & "   ,CASE WHEN C.FEC_CUMPLIM IS NULL THEN 2 " & vbCrLf
    sConsulta = sConsulta & "   WHEN C.FEC_EXPIRACION IS NULL THEN 3 " & vbCrLf
    sConsulta = sConsulta & "   WHEN DATEDIFF(day,getdate(),C.FEC_EXPIRACION)>@FSQA_PROX_EXPIRAR THEN 3 " & vbCrLf
    sConsulta = sConsulta & "   WHEN DATEDIFF(day,getdate(),C.FEC_EXPIRACION) BETWEEN 0 AND @FSQA_PROX_EXPIRAR THEN 4 " & vbCrLf
    sConsulta = sConsulta & "   ELSE 5 END ESTADO,'''' AS ESTADO_DEN" & vbCrLf
    sConsulta = sConsulta & "   , '''' AS DEN_MATERIAL, '''' AS ID_MATERIAL" & vbCrLf
    sConsulta = sConsulta & "   ,CC.EMAIL CON_EMAIL,0 AS NOPETIC'" & vbCrLf
    sConsulta = sConsulta & "SET @JOIN =  N' FROM CERTIFICADO C    WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "INNER JOIN INSTANCIA I WITH(NOLOCK) ON C.INSTANCIA=I.ID" & vbCrLf
    sConsulta = sConsulta & "INNER JOIN SOLICITUD S WITH(NOLOCK) ON I.SOLICITUD=S.ID AND S.PUB=1'" & vbCrLf
    sConsulta = sConsulta & "IF @NOMBRE_TABLA <> ''" & vbCrLf
    sConsulta = sConsulta & "   SET @JOIN = @JOIN + N' AND S.FORMULARIO=(SELECT FORMULARIO FROM QA_FILTROS WITH(NOLOCK) WHERE NOM_TABLA=''' + @NOMBRE_TABLA + ''')'" & vbCrLf
    sConsulta = sConsulta & "SET @JOIN = @JOIN + N' INNER JOIN PROVE_CERTIFICADO PC   WITH(NOLOCK) ON S.ID=PC.TIPO_CERTIF AND C.ID=PC.ID_CERTIF" & vbCrLf
    sConsulta = sConsulta & "INNER JOIN PROVE PR    WITH(NOLOCK) ON C.PROVE=PR.COD AND ((S.TIPO_PROVE IS NULL) OR (PR.CALIDAD_POT=S.TIPO_PROVE))" & vbCrLf
    sConsulta = sConsulta & "INNER JOIN PER PT WITH(NOLOCK) ON C.PER=PT.COD" & vbCrLf
    sConsulta = sConsulta & "LEFT JOIN CON CC WITH(NOLOCK) ON C.PROVE=CC.PROVE AND C.CONTACTO=CC.ID'" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "--A�adir RESTRICCIONES de CONSULTA DE CERTIFICADOS del USUARIO" & vbCrLf
    sConsulta = sConsulta & "-- Sin solicitar no se controla @REST_SOLICIT_USU=1 OR @REST_SOLICIT_DEP=1 OR @REST_SOLICIT_UO=1  " & vbCrLf
    sConsulta = sConsulta & "IF @REST_PROV_CON=1" & vbCrLf
    sConsulta = sConsulta & "BEGIN" & vbCrLf
    sConsulta = sConsulta & "    SET @JOIN_RESTRIC_SINSOLIC = @JOIN_RESTRIC_SINSOLIC + N' INNER JOIN CON WITH(NOLOCK) ON P4.PROVE=CON.PROVE AND CON.CALIDAD=1'" & vbCrLf
    sConsulta = sConsulta & "   SET @JOIN = @JOIN + N' INNER JOIN CON WITH(NOLOCK) ON C.PROVE=CON.PROVE AND CON.CALIDAD=1'" & vbCrLf
    sConsulta = sConsulta & "END" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "IF @REST_PROV_EQP=1" & vbCrLf
    sConsulta = sConsulta & "BEGIN" & vbCrLf
    sConsulta = sConsulta & "   SET @JOIN_RESTRIC_SINSOLIC = @JOIN_RESTRIC_SINSOLIC + N' INNER JOIN PER P WITH(NOLOCK) ON P.COD=@PER'" & vbCrLf
    sConsulta = sConsulta & "   SET @JOIN_RESTRIC_SINSOLIC = @JOIN_RESTRIC_SINSOLIC + N' INNER JOIN PROVE_EQP PE WITH(NOLOCK) ON P.EQP=PE.EQP AND P4.PROVE=PE.PROVE'   " & vbCrLf
    sConsulta = sConsulta & "   " & vbCrLf
    sConsulta = sConsulta & "   SET @JOIN = @JOIN + N' INNER JOIN PER P WITH(NOLOCK) ON P.COD=C.PER'    " & vbCrLf
    sConsulta = sConsulta & "   SET @JOIN = @JOIN + N' INNER JOIN PROVE_EQP PE WITH(NOLOCK)  ON P.EQP=PE.EQP AND C.PROVE=PE.PROVE'   " & vbCrLf
    sConsulta = sConsulta & "END" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "--1� Los que a�aden m�s tablas al FROM (SELECCI�N POR MATERIAL)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "IF @MATERIALES IS NOT NULL" & vbCrLf
    sConsulta = sConsulta & "BEGIN" & vbCrLf
    sConsulta = sConsulta & "   SET @JOIN = @JOIN + N' INNER JOIN MATERIAL_QA_CERTIFICADOS MC WITH(NOLOCK) ON MC.TIPO_CERTIFICADO=S.ID'     " & vbCrLf
    sConsulta = sConsulta & "   SET @JOIN = @JOIN + N' AND MC.MATERIAL IN (' + @MATERIALES + ')'" & vbCrLf
    sConsulta = sConsulta & "END" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "IF @REST_PROV_MAT=1" & vbCrLf
    sConsulta = sConsulta & "BEGIN" & vbCrLf
    sConsulta = sConsulta & "   SET @JOIN_RESTRIC_SINSOLIC = @JOIN_RESTRIC_SINSOLIC + N' INNER JOIN COM_GMN4 C4 WITH(NOLOCK) ON C4.COM=@PER AND C4.GMN1=P4.GMN1 AND C4.GMN2=P4.GMN2 AND C4.GMN3=P4.GMN3 AND C4.GMN4=P4.GMN4'" & vbCrLf
    sConsulta = sConsulta & "   " & vbCrLf
    sConsulta = sConsulta & "   IF @MATERIALES IS NULL" & vbCrLf
    sConsulta = sConsulta & "       SET @JOIN = @JOIN + N' INNER JOIN MATERIAL_QA_CERTIFICADOS MC WITH(NOLOCK) ON MC.TIPO_CERTIFICADO=S.ID'" & vbCrLf
    sConsulta = sConsulta & "   " & vbCrLf
    sConsulta = sConsulta & "   SET @JOIN = @JOIN + N' INNER JOIN MATERIAL_QA_GMN4 M4 WITH (NOLOCK) ON MC.MATERIAL=M4.MATERIAL_QA " & vbCrLf
    sConsulta = sConsulta & "       INNER JOIN PROVE_GMN4 P4 WITH (NOLOCK) ON M4.GMN1=P4.GMN1 AND M4.GMN2=P4.GMN2 AND M4.GMN3=P4.GMN3 " & vbCrLf
    sConsulta = sConsulta & "           AND M4.GMN4=P4.GMN4 AND P4.PROVE=PR.COD '   " & vbCrLf
    sConsulta = sConsulta & "   SET @JOIN = @JOIN + N' INNER JOIN COM_GMN4 C4 WITH(NOLOCK) ON C4.COM=@PER AND C4.GMN1=P4.GMN1 AND C4.GMN2=P4.GMN2 AND C4.GMN3=P4.GMN3 AND C4.GMN4=P4.GMN4'" & vbCrLf
    sConsulta = sConsulta & "END" & vbCrLf
    sConsulta = sConsulta & "--Filtros" & vbCrLf
    sConsulta = sConsulta & "IF @NOMBRE_TABLA <> ''" & vbCrLf
    sConsulta = sConsulta & "  BEGIN" & vbCrLf
    sConsulta = sConsulta & "   SET @JOIN2 = @JOIN + N' LEFT JOIN ' + @IDI + '_' + @NOMBRE_TABLA + ' FILTRO WITH(NOLOCK)ON C.INSTANCIA = FILTRO.INSTANCIA '" & vbCrLf
    sConsulta = sConsulta & "   SET @JOIN = @JOIN + N' INNER JOIN ' + @IDI + '_' + @NOMBRE_TABLA + ' FILTRO WITH(NOLOCK)ON C.INSTANCIA = FILTRO.INSTANCIA '" & vbCrLf
    sConsulta = sConsulta & "  END" & vbCrLf
    sConsulta = sConsulta & "--A�adir CRITERIOS DE SELECCI�N" & vbCrLf
    sConsulta = sConsulta & "SET @WHERE =  N' WHERE 1=1'" & vbCrLf
    sConsulta = sConsulta & "IF @REST_SOLICIT_USU=1" & vbCrLf
    sConsulta = sConsulta & "    SET @WHERE = @WHERE + ' AND C.USU=@USU'" & vbCrLf
    sConsulta = sConsulta & "IF @CODPROV IS NOT NULL" & vbCrLf
    sConsulta = sConsulta & "   SET @WHERE = @WHERE + ' AND PR.COD=@CODPROV'" & vbCrLf
    sConsulta = sConsulta & "IF @BAJAPROV=0" & vbCrLf
    sConsulta = sConsulta & "   SET @WHERE = @WHERE + ' AND PR.BAJA_CALIDAD=0'" & vbCrLf
    sConsulta = sConsulta & "IF @TIPOPROV=1" & vbCrLf
    sConsulta = sConsulta & "   SET @WHERE = @WHERE + ' AND PR.CALIDAD_POT=1'  --Proveedor potencial" & vbCrLf
    sConsulta = sConsulta & "ELSE" & vbCrLf
    sConsulta = sConsulta & "   BEGIN" & vbCrLf
    sConsulta = sConsulta & "   IF @TIPOPROV=2" & vbCrLf
    sConsulta = sConsulta & "       SET @WHERE = @WHERE + ' AND PR.CALIDAD_POT=2'  --Proveedor real" & vbCrLf
    sConsulta = sConsulta & "   ELSE" & vbCrLf
    sConsulta = sConsulta & "       SET @WHERE= @WHERE + '  AND (PR.CALIDAD_POT=1 OR PR.CALIDAD_POT=2) '" & vbCrLf
    sConsulta = sConsulta & "   END" & vbCrLf
    sConsulta = sConsulta & "IF @CERTIFICADOS IS NOT NULL" & vbCrLf
    sConsulta = sConsulta & "   SET @WHERE = @WHERE +  ' AND S.ID IN (' + @CERTIFICADOS + ')'" & vbCrLf
    sConsulta = sConsulta & "IF @CERTIFBAJA=0" & vbCrLf
    sConsulta = sConsulta & "   SET @WHERE = @WHERE +  ' AND S.BAJA=0 ' " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @WHESTADO=N' AND ('    " & vbCrLf
    sConsulta = sConsulta & "SET @HAYESTADO = 0" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "IF NOT(CHARINDEX('2',@ESTADO)=0)    " & vbCrLf
    sConsulta = sConsulta & "BEGIN" & vbCrLf
    sConsulta = sConsulta & "   IF @HAYESTADO=1 " & vbCrLf
    sConsulta = sConsulta & "       SET @WHESTADO = @WHESTADO + ' OR '" & vbCrLf
    sConsulta = sConsulta & "           " & vbCrLf
    sConsulta = sConsulta & "   SET @WHESTADO = @WHESTADO + 'C.FEC_CUMPLIM IS NULL'" & vbCrLf
    sConsulta = sConsulta & "   SET @HAYESTADO = 1" & vbCrLf
    sConsulta = sConsulta & "END" & vbCrLf
    sConsulta = sConsulta & "IF NOT(CHARINDEX('3',@ESTADO)=0)    " & vbCrLf
    sConsulta = sConsulta & "BEGIN" & vbCrLf
    sConsulta = sConsulta & "   IF @HAYESTADO=1 " & vbCrLf
    sConsulta = sConsulta & "       SET @WHESTADO = @WHESTADO + ' OR '" & vbCrLf
    sConsulta = sConsulta & "   SET @WHESTADO = @WHESTADO + '('" & vbCrLf
    sConsulta = sConsulta & "   SET @WHESTADO = @WHESTADO + 'C.FEC_CUMPLIM IS NOT NULL'" & vbCrLf
    sConsulta = sConsulta & "   SET @WHESTADO = @WHESTADO + ' AND ((C.FEC_EXPIRACION IS NULL)'" & vbCrLf
    sConsulta = sConsulta & "   SET @WHESTADO = @WHESTADO + ' OR ((C.FEC_EXPIRACION IS NOT NULL) AND DATEDIFF(day,getdate(),C.FEC_EXPIRACION)>@FSQA_PROX_EXPIRAR)'" & vbCrLf
    sConsulta = sConsulta & "   SET @WHESTADO = @WHESTADO + ')'" & vbCrLf
    sConsulta = sConsulta & "   SET @WHESTADO = @WHESTADO + ')'" & vbCrLf
    sConsulta = sConsulta & "   SET @HAYESTADO = 1" & vbCrLf
    sConsulta = sConsulta & "END        " & vbCrLf
    sConsulta = sConsulta & "IF NOT(CHARINDEX('4',@ESTADO)=0)    " & vbCrLf
    sConsulta = sConsulta & "BEGIN" & vbCrLf
    sConsulta = sConsulta & "   IF @HAYESTADO=1 " & vbCrLf
    sConsulta = sConsulta & "       SET @WHESTADO = @WHESTADO + ' OR '" & vbCrLf
    sConsulta = sConsulta & "   " & vbCrLf
    sConsulta = sConsulta & "   SET @WHESTADO = @WHESTADO + '(C.FEC_CUMPLIM IS NOT NULL AND C.FEC_EXPIRACION IS NOT NULL' " & vbCrLf
    sConsulta = sConsulta & "   SET @WHESTADO = @WHESTADO + ' AND DATEDIFF(day,getdate(),C.FEC_EXPIRACION) BETWEEN 0 AND @FSQA_PROX_EXPIRAR)'" & vbCrLf
    sConsulta = sConsulta & "   SET @HAYESTADO = 1" & vbCrLf
    sConsulta = sConsulta & "END            " & vbCrLf
    sConsulta = sConsulta & "IF NOT(CHARINDEX('5',@ESTADO)=0)    " & vbCrLf
    sConsulta = sConsulta & "BEGIN" & vbCrLf
    sConsulta = sConsulta & "   IF @HAYESTADO=1 " & vbCrLf
    sConsulta = sConsulta & "       SET @WHESTADO = @WHESTADO + ' OR '" & vbCrLf
    sConsulta = sConsulta & "   " & vbCrLf
    sConsulta = sConsulta & "   SET @WHESTADO = @WHESTADO + '(C.FEC_CUMPLIM IS NOT NULL AND C.FEC_EXPIRACION IS NOT NULL' " & vbCrLf
    sConsulta = sConsulta & "   SET @WHESTADO = @WHESTADO + ' AND DATEDIFF(day,getdate(),C.FEC_EXPIRACION)<0)'" & vbCrLf
    sConsulta = sConsulta & "   SET @HAYESTADO = 1" & vbCrLf
    sConsulta = sConsulta & "END        " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "IF @HAYESTADO=1 " & vbCrLf
    sConsulta = sConsulta & "   SET @WHERE = @WHERE + @WHESTADO + ')'" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "IF @PETICIONARIO IS NOT NULL" & vbCrLf
    sConsulta = sConsulta & "   SET @WHERE = @WHERE +  ' AND C.PER IN (' + @PETICIONARIO + ')'       " & vbCrLf
    sConsulta = sConsulta & "   " & vbCrLf
    sConsulta = sConsulta & "IF @REST_SOLICIT_DEP=1 OR @REST_SOLICIT_UO=1 " & vbCrLf
    sConsulta = sConsulta & "BEGIN      " & vbCrLf
    sConsulta = sConsulta & "   IF @REST_SOLICIT_DEP=1" & vbCrLf
    sConsulta = sConsulta & "       SET @WHERE = @WHERE + N' AND C.DEP=@DEP '" & vbCrLf
    sConsulta = sConsulta & "   IF @REST_SOLICIT_UO=1" & vbCrLf
    sConsulta = sConsulta & "       SET @WHERE = @WHERE + N' AND (@UON1 IS NULL OR C.UON1=@UON1) AND (@UON2 IS NULL OR C.UON2=@UON2) AND (@UON3 IS NULL OR C.UON3=@UON3) '       " & vbCrLf
    sConsulta = sConsulta & "END" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "IF @QUEFECHA=1" & vbCrLf
    sConsulta = sConsulta & "   SET @FILTROFECHA='C.FEC_SOLICITUD'" & vbCrLf
    sConsulta = sConsulta & "ELSE" & vbCrLf
    sConsulta = sConsulta & "   IF @QUEFECHA=2" & vbCrLf
    sConsulta = sConsulta & "       SET @FILTROFECHA='C.FEC_LIM_CUMPLIM'    " & vbCrLf
    sConsulta = sConsulta & "   ELSE" & vbCrLf
    sConsulta = sConsulta & "       IF @QUEFECHA=3" & vbCrLf
    sConsulta = sConsulta & "           SET @FILTROFECHA='C.FECACT'     " & vbCrLf
    sConsulta = sConsulta & "       ELSE" & vbCrLf
    sConsulta = sConsulta & "           IF @QUEFECHA=4" & vbCrLf
    sConsulta = sConsulta & "               SET @FILTROFECHA='C.FEC_EXPIRACION' " & vbCrLf
    sConsulta = sConsulta & "       " & vbCrLf
    sConsulta = sConsulta & "IF @FECDESDE IS NOT NULL" & vbCrLf
    sConsulta = sConsulta & "    SET @WHERE = @WHERE + '  AND (' + @FILTROFECHA + '>=@FECDESDE OR ' + @FILTROFECHA + ' IS NULL)'" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "IF @FECHASTA IS NOT NULL" & vbCrLf
    sConsulta = sConsulta & "    SET @WHERE = @WHERE + '  AND (' + @FILTROFECHA + '<=@FECHASTA OR ' + @FILTROFECHA + ' IS NULL)'    " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "--***********************************************************************************************************" & vbCrLf
    sConsulta = sConsulta & "--1� SELECT DE CERTIFICADOS: SOLICITADOS" & vbCrLf
    sConsulta = sConsulta & "--***********************************************************************************************************" & vbCrLf
    sConsulta = sConsulta & "IF @ESTADO IS NULL OR NOT(CHARINDEX('2',@ESTADO)=0) OR NOT(CHARINDEX('3',@ESTADO)=0) OR NOT(CHARINDEX('4',@ESTADO)=0) OR NOT(CHARINDEX('5',@ESTADO)=0)" & vbCrLf
    sConsulta = sConsulta & "BEGIN" & vbCrLf
    sConsulta = sConsulta & "IF @SOLOCONTADORES =0" & vbCrLf
    sConsulta = sConsulta & "   BEGIN" & vbCrLf
    sConsulta = sConsulta & "   --CALIDAD. Este * es imprencisdible la defini�n de la tabla FILTRO es desconocida de antemano. Se crea y mantiene dinamicamnte con las columnas del formulario." & vbCrLf
    sConsulta = sConsulta & "   IF @NOMBRE_TABLA <> ''  " & vbCrLf
    sConsulta = sConsulta & "       SET @SQL = @SEL +  ',FILTRO.*'" & vbCrLf
    sConsulta = sConsulta & "   ELSE" & vbCrLf
    sConsulta = sConsulta & "       SET @SQL = @SEL +  ',C.INSTANCIA'" & vbCrLf
    sConsulta = sConsulta & "   " & vbCrLf
    sConsulta = sConsulta & "   END" & vbCrLf
    sConsulta = sConsulta & "ELSE" & vbCrLf
    sConsulta = sConsulta & "   BEGIN" & vbCrLf
    sConsulta = sConsulta & "   SET @SQL=N'SELECT COUNT(DISTINCT DATAKEY) NUM FROM (SELECT DISTINCT PR.COD + ''#'' + S.COD AS DATAKEY'" & vbCrLf
    sConsulta = sConsulta & "   END" & vbCrLf
    sConsulta = sConsulta & "SET @SQL = @SQL + @JOIN + @WHERE" & vbCrLf
    sConsulta = sConsulta & "END" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "--***********************************************************************************************************" & vbCrLf
    sConsulta = sConsulta & "--1� SELECT DE CERTIFICADOS: PDTE CUMPLIMENTAR" & vbCrLf
    sConsulta = sConsulta & "--Existe registro en certificado, prove_certificado y instancia CON instancia.version a 0" & vbCrLf
    sConsulta = sConsulta & "--Q pasa ahora si trabajas con SPA_QA_FILTROxx pues q hasta q alguien en el portal lo envia no hay nada en SPA_QA_FILTROxx y " & vbCrLf
    sConsulta = sConsulta & "--en consecuencia INNER JOIN SPA_QA_FILTROxx .... no los saca, vamos q la select anterior no los comtempla" & vbCrLf
    sConsulta = sConsulta & "--***********************************************************************************************************" & vbCrLf
    sConsulta = sConsulta & "IF @NOMBRE_TABLA <> '' " & vbCrLf
    sConsulta = sConsulta & "BEGIN" & vbCrLf
    sConsulta = sConsulta & " IF @ESTADO IS NULL OR NOT(CHARINDEX('2',@ESTADO)=0) OR NOT(CHARINDEX('3',@ESTADO)=0) OR NOT(CHARINDEX('4',@ESTADO)=0) OR NOT(CHARINDEX('5',@ESTADO)=0)" & vbCrLf
    sConsulta = sConsulta & " BEGIN" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   IF @SQL<>''" & vbCrLf
    sConsulta = sConsulta & "     SET @SQL = @SQL + ' UNION '" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   IF @SOLOCONTADORES =0" & vbCrLf
    sConsulta = sConsulta & "     BEGIN" & vbCrLf
    sConsulta = sConsulta & "     --CALIDAD. Este * es imprencisdible la defini�n de la tabla FILTRO es desconocida de antemano. Se crea y mantiene dinamicamnte con las columnas del formulario." & vbCrLf
    sConsulta = sConsulta & "     SET @SQL = @SQL + @SEL +  ',FILTRO.*'     -- Como es Union hay q respetar las columnas, pero se q va a ser null" & vbCrLf
    sConsulta = sConsulta & "     END" & vbCrLf
    sConsulta = sConsulta & "   ELSE" & vbCrLf
    sConsulta = sConsulta & "     BEGIN   " & vbCrLf
    sConsulta = sConsulta & "     IF @SQL<>''" & vbCrLf
    sConsulta = sConsulta & "         SET @SQL = @SQL + 'SELECT DISTINCT PR.COD + ''#'' + S.COD AS DATAKEY'" & vbCrLf
    sConsulta = sConsulta & "     ELSE" & vbCrLf
    sConsulta = sConsulta & "         SET @SQL=N'SELECT COUNT(DISTINCT DATAKEY) NUM FROM (SELECT DISTINCT PR.COD + ''#'' + S.COD AS DATAKEY'     " & vbCrLf
    sConsulta = sConsulta & "     END" & vbCrLf
    sConsulta = sConsulta & "   --La join es la JOIN2" & vbCrLf
    sConsulta = sConsulta & "   SET @SQL = @SQL + @JOIN2    " & vbCrLf
    sConsulta = sConsulta & "   --La WHERE es la comun" & vbCrLf
    sConsulta = sConsulta & "   SET @SQL = @SQL + @WHERE  " & vbCrLf
    sConsulta = sConsulta & " END" & vbCrLf
    sConsulta = sConsulta & "END" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "--***********************************************************************************************************" & vbCrLf
    sConsulta = sConsulta & "--1� SELECT DE CERTIFICADOS: UNION CON SIN SOLICITAR" & vbCrLf
    sConsulta = sConsulta & "--***********************************************************************************************************" & vbCrLf
    sConsulta = sConsulta & "IF @ESTADO IS NULL OR NOT(CHARINDEX('1',@ESTADO)=0)     " & vbCrLf
    sConsulta = sConsulta & "BEGIN" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQLAUX NVARCHAR(MAX)" & vbCrLf
    sConsulta = sConsulta & "IF @SQL<>''" & vbCrLf
    sConsulta = sConsulta & "   SET @SQL = @SQL + ' UNION '" & vbCrLf
    sConsulta = sConsulta & "IF @SOLOCONTADORES=0" & vbCrLf
    sConsulta = sConsulta & "BEGIN" & vbCrLf
    sConsulta = sConsulta & "   --CALIDAD. Este * es imprencisdible la defini�n de la tabla FILTRO es desconocida de antemano. Se crea y mantiene dinamicamnte con las columnas del formulario. " & vbCrLf
    sConsulta = sConsulta & "   SET @SQLAUX = 'SELECT  DISTINCT" & vbCrLf
    sConsulta = sConsulta & "   NULL AS NUM_VERSION,NULL AS  ID_CERTIF,NULL AS PUBLICADA,NULL AS FEC_DESPUB,NULL AS FEC_SOLICITUD,NULL AS FEC_RESPUESTA,NULL AS FEC_EXPIRACION" & vbCrLf
    sConsulta = sConsulta & "   ,NULL AS FEC_LIM_CUMPLIM,NULL AS PER,NULL AS FEC_PUBLICACION,NULL AS FEC_ACT,NULL AS CERTIFICADO,NULL AS FEC_CUMPLIM" & vbCrLf
    sConsulta = sConsulta & "   ,PR.COD PROVE_COD,PR.DEN PROVE_DEN,ISNULL(PR.CALIDAD_POT,0) REAL,ISNULL(PR.BAJA_CALIDAD,0) BAJA" & vbCrLf
    sConsulta = sConsulta & "   ,S.ID AS TIPO_CERTIFICADO,S.DEN_SPA DEN_TIPO_CERTIFICADO,ISNULL(S.PLAZO_CUMPL,0) PLAZO_CUMPL,~S.OPCIONAL OBLIGATORIO,S.MODO_SOLIC" & vbCrLf
    sConsulta = sConsulta & "   ,NULL AS NOMBREPETICIONARIO " & vbCrLf
    sConsulta = sConsulta & "   ,0 AS EN_PROCESO" & vbCrLf
    sConsulta = sConsulta & "   ,PR.COD + ''#'' + CAST(S.ID AS VARCHAR(10)) AS DATAKEY" & vbCrLf
    sConsulta = sConsulta & "   ,1 ESTADO ,'''' AS ESTADO_DEN" & vbCrLf
    sConsulta = sConsulta & "   , '''' AS DEN_MATERIAL, '''' AS ID_MATERIAL" & vbCrLf
    sConsulta = sConsulta & "   ,NULL AS CON_EMAIL,0 AS NOPETIC'" & vbCrLf
    sConsulta = sConsulta & "   IF @NOMBRE_TABLA <> ''  " & vbCrLf
    sConsulta = sConsulta & "       SET @SQLAUX = @SQLAUX +  ',FILTRO.*'" & vbCrLf
    sConsulta = sConsulta & "   ELSE" & vbCrLf
    sConsulta = sConsulta & "       SET @SQLAUX = @SQLAUX +  ',NULL AS INSTANCIA'" & vbCrLf
    sConsulta = sConsulta & "END" & vbCrLf
    sConsulta = sConsulta & "ELSE" & vbCrLf
    sConsulta = sConsulta & "BEGIN" & vbCrLf
    sConsulta = sConsulta & "   IF @SQL<>''" & vbCrLf
    sConsulta = sConsulta & "      SET @SQLAUX ='SELECT DISTINCT PR.COD + ''#'' + CAST(S.ID AS VARCHAR(10)) AS DATAKEY'" & vbCrLf
    sConsulta = sConsulta & "   ELSE" & vbCrLf
    sConsulta = sConsulta & "   BEGIN" & vbCrLf
    sConsulta = sConsulta & "       SET @SQL=N'SELECT COUNT(DISTINCT DATAKEY) NUM FROM ('" & vbCrLf
    sConsulta = sConsulta & "       SET @SQLAUX=N'SELECT DISTINCT PR.COD + ''#'' + CAST(S.ID AS VARCHAR(10)) AS DATAKEY'  " & vbCrLf
    sConsulta = sConsulta & "   END" & vbCrLf
    sConsulta = sConsulta & "END" & vbCrLf
    sConsulta = sConsulta & "SET @JOIN3 = N' FROM MATERIAL_QA_CERTIFICADOS MC WITH(NOLOCK)  " & vbCrLf
    sConsulta = sConsulta & "INNER JOIN SOLICITUD S WITH(NOLOCK) ON MC.TIPO_CERTIFICADO=S.ID AND S.PUB=1 '" & vbCrLf
    sConsulta = sConsulta & "IF @NOMBRE_TABLA <> ''" & vbCrLf
    sConsulta = sConsulta & "      SET @JOIN3 = @JOIN3+ N' AND S.FORMULARIO=(SELECT FORMULARIO FROM QA_FILTROS WITH(NOLOCK) WHERE NOM_TABLA=''' + @NOMBRE_TABLA + ''')'" & vbCrLf
    sConsulta = sConsulta & " SET @JOIN3 = @JOIN3 + N' INNER JOIN MATERIAL_QA_GMN4 M4 WITH(NOLOCK) ON MC.MATERIAL=M4.MATERIAL_QA " & vbCrLf
    sConsulta = sConsulta & "INNER JOIN PROVE_GMN4 P4 WITH(NOLOCK) ON M4.GMN1=P4.GMN1 AND M4.GMN2=P4.GMN2 AND M4.GMN3=P4.GMN3 AND M4.GMN4=P4.GMN4 " & vbCrLf
    sConsulta = sConsulta & "INNER JOIN PROVE PR WITH(NOLOCK) ON P4.PROVE=PR.COD AND ((S.TIPO_PROVE IS NULL) OR (PR.CALIDAD_POT=S.TIPO_PROVE))" & vbCrLf
    sConsulta = sConsulta & "LEFT JOIN PROVE_CERTIFICADO PC WITH(NOLOCK) ON PC.PROVE=P4.PROVE AND PC.TIPO_CERTIF=MC.TIPO_CERTIFICADO '" & vbCrLf
    sConsulta = sConsulta & "--Filtros" & vbCrLf
    sConsulta = sConsulta & "IF @NOMBRE_TABLA <> ''" & vbCrLf
    sConsulta = sConsulta & "     SET @JOIN3 = @JOIN3 + N' LEFT JOIN ' + @IDI + '_' + @NOMBRE_TABLA + ' FILTRO WITH(NOLOCK) ON FILTRO.INSTANCIA=NULL '" & vbCrLf
    sConsulta = sConsulta & "SET @JOIN3 = @JOIN3 + @JOIN_RESTRIC_SINSOLIC" & vbCrLf
    sConsulta = sConsulta & "SET @WHERE3 = N' WHERE PC.PROVE IS NULL AND PC.TIPO_CERTIF IS NULL' " & vbCrLf
    sConsulta = sConsulta & "IF @MATERIALES IS NOT NULL" & vbCrLf
    sConsulta = sConsulta & "    SET @WHERE3  = @WHERE3 + N' AND MC.MATERIAL IN (' + @MATERIALES + ')'" & vbCrLf
    sConsulta = sConsulta & "IF @CODPROV IS NOT NULL" & vbCrLf
    sConsulta = sConsulta & "    SET @WHERE3 = @WHERE3 + ' AND PR.COD=@CODPROV'" & vbCrLf
    sConsulta = sConsulta & "IF @BAJAPROV=0" & vbCrLf
    sConsulta = sConsulta & "    SET @WHERE3 = @WHERE3 + ' AND PR.BAJA_CALIDAD=0'" & vbCrLf
    sConsulta = sConsulta & "IF @TIPOPROV=1" & vbCrLf
    sConsulta = sConsulta & "    SET @WHERE3 = @WHERE3 + ' AND PR.CALIDAD_POT=1'  --Proveedor potencial" & vbCrLf
    sConsulta = sConsulta & "ELSE" & vbCrLf
    sConsulta = sConsulta & "   BEGIN" & vbCrLf
    sConsulta = sConsulta & "      IF @TIPOPROV=2" & vbCrLf
    sConsulta = sConsulta & "          SET @WHERE3 = @WHERE3 + ' AND PR.CALIDAD_POT=2'  --Proveedor real" & vbCrLf
    sConsulta = sConsulta & "       ELSE" & vbCrLf
    sConsulta = sConsulta & "          SET @WHERE3 = @WHERE3 + '  AND (PR.CALIDAD_POT=1 OR PR.CALIDAD_POT=2) '" & vbCrLf
    sConsulta = sConsulta & "   END" & vbCrLf
    sConsulta = sConsulta & "IF @CODPROV IS NOT NULL" & vbCrLf
    sConsulta = sConsulta & "    SET @WHERE3 = @WHERE3 + ' AND PR.COD=@CODPROV'" & vbCrLf
    sConsulta = sConsulta & "IF @CERTIFICADOS IS NOT NULL" & vbCrLf
    sConsulta = sConsulta & "    SET @WHERE3 = @WHERE3 +  ' AND S.ID IN (' + @CERTIFICADOS + ')'" & vbCrLf
    sConsulta = sConsulta & "IF @CERTIFBAJA=0" & vbCrLf
    sConsulta = sConsulta & "    SET @WHERE3 = @WHERE3 +  ' AND S.BAJA=0 '" & vbCrLf
    sConsulta = sConsulta & "IF @REST_SOLICIT_DEP=1 OR @REST_SOLICIT_UO=1 OR @REST_SOLICIT_USU=1" & vbCrLf
    sConsulta = sConsulta & "BEGIN  " & vbCrLf
    sConsulta = sConsulta & "   SET @SQL=@SQL+@SQLAUX+@JOIN3+N' LEFT JOIN PETICIONARIOS WITH(NOLOCK) ON PETICIONARIOS.SOLICITUD=S.ID '+@WHERE3+N' AND PETICIONARIOS.ID IS NULL '" & vbCrLf
    sConsulta = sConsulta & "   SET @SQL=@SQL++N'UNION '+@SQLAUX+@JOIN3+N' INNER JOIN PETICIONARIOS WITH(NOLOCK) ON PETICIONARIOS.SOLICITUD=S.ID '" & vbCrLf
    sConsulta = sConsulta & "   IF @REST_SOLICIT_UO=1" & vbCrLf
    sConsulta = sConsulta & "       SET @SQL=@SQL+N'AND (@UON1 IS NULL OR PETICIONARIOS.UON1=@UON1) AND (@UON2 IS NULL OR PETICIONARIOS.UON2=@UON2) AND (@UON3 IS NULL OR PETICIONARIOS.UON3=@UON3) '" & vbCrLf
    sConsulta = sConsulta & "   IF @REST_SOLICIT_DEP=1" & vbCrLf
    sConsulta = sConsulta & "       SET @SQL=@SQL+N'AND PETICIONARIOS.DEP=@DEP '" & vbCrLf
    sConsulta = sConsulta & "   IF @REST_SOLICIT_USU=1" & vbCrLf
    sConsulta = sConsulta & "       SET @SQL=@SQL+N'AND PETICIONARIOS.PER=@PER '" & vbCrLf
    sConsulta = sConsulta & "   SET @SQL=@SQL+@WHERE3" & vbCrLf
    sConsulta = sConsulta & "END" & vbCrLf
    sConsulta = sConsulta & "ELSE" & vbCrLf
    sConsulta = sConsulta & "   SET @SQL=@SQL+@SQLAUX+@JOIN3+@WHERE3" & vbCrLf
    sConsulta = sConsulta & "END" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "--Montamos la query" & vbCrLf
    sConsulta = sConsulta & "IF @SOLOCONTADORES =0" & vbCrLf
    sConsulta = sConsulta & "BEGIN" & vbCrLf
    sConsulta = sConsulta & "   IF @ORDEN IS NULL" & vbCrLf
    sConsulta = sConsulta & "       SET @ORDEN= 'PROVE_DEN'" & vbCrLf
    sConsulta = sConsulta & "   ELSE" & vbCrLf
    sConsulta = sConsulta & "   BEGIN" & vbCrLf
    sConsulta = sConsulta & "       IF @ORDEN = ''" & vbCrLf
    sConsulta = sConsulta & "           SET @ORDEN=  'PROVE_DEN'" & vbCrLf
    sConsulta = sConsulta & "   END" & vbCrLf
    sConsulta = sConsulta & "   DECLARE @ROW INT = ((@PAGENUMBER-1) * @PAGESIZE) +1" & vbCrLf
    sConsulta = sConsulta & "   IF @OBTENERTODOSCERTIFICADOS=0" & vbCrLf
    sConsulta = sConsulta & "       BEGIN             " & vbCrLf
    sConsulta = sConsulta & "       SET @SQL2 = @SQL" & vbCrLf
    sConsulta = sConsulta & "       --CALIDAD. Estos dos * se quedan pq no es la tabla de bbdd certificados sino una tabla hecha ad hoc mediante WITH CERTIFICADOS AS (...) y en estos puntos suspensivos puede venir filtro.* o no -> no se conocen las columnas" & vbCrLf
    sConsulta = sConsulta & "       SET @SQL = ';WITH CERTIFICADOS AS (' + @SQL + '), TOTCERTIF AS (SELECT *,ROW_NUMBER() OVER (ORDER BY ' + @ORDEN + ') AS ROWNUMBER FROM CERTIFICADOS WITH (NOLOCK)) SELECT *,ROWNUMBER FROM TOTCERTIF WITH (NOLOCK) WHERE ROWNUMBER BETWEEN ' + CONVERT(VARCHAR(10),@ROW) + ' AND ' + CONVERT(VARCHAR(10),(@ROW + @PAGESIZE - 1))" & vbCrLf
    sConsulta = sConsulta & "       END" & vbCrLf
    sConsulta = sConsulta & "END" & vbCrLf
    sConsulta = sConsulta & "ELSE" & vbCrLf
    sConsulta = sConsulta & "BEGIN" & vbCrLf
    sConsulta = sConsulta & "   SET @SQL = @SQL + ') CERT'" & vbCrLf
    sConsulta = sConsulta & "END" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL,N'@PER VARCHAR(50),@USU VARCHAR(50),@FSQA_PROX_EXPIRAR INT,@CODPROV NVARCHAR(50),@FECDESDE AS DATE,@FECHASTA AS DATE,@UON1 NVARCHAR(50),@UON2 NVARCHAR(50),@UON3 NVARCHAR(50),@DEP NVARCHAR(50)'," & vbCrLf
    sConsulta = sConsulta & "@PER=@PER,@USU=@USU,@FSQA_PROX_EXPIRAR=@FSQA_PROX_EXPIRAR,@CODPROV=@CODPROV,@FECDESDE=@FECDESDE,@FECHASTA=@FECHASTA,@UON1=@UON1,@UON2=@UON2,@UON3=@UON3,@DEP=@DEP" & vbCrLf
    sConsulta = sConsulta & "--*********************************************************************************************************************************************************" & vbCrLf
    sConsulta = sConsulta & "--2� SELECT DE PERMISOS DE SOLICITUD DE CERTIFICADOS DEL USUARIO (devuelve un rgistro por cada tio de certificado que el usuario puede solicitar)" & vbCrLf
    sConsulta = sConsulta & "--Desde c�digo se dar� valor a la columna de la 1� SELECT: NOPETIC" & vbCrLf
    sConsulta = sConsulta & "--*********************************************************************************************************************************************************" & vbCrLf
    sConsulta = sConsulta & "IF @SOLOCONTADORES =0" & vbCrLf
    sConsulta = sConsulta & "BEGIN" & vbCrLf
    sConsulta = sConsulta & "SET @SQL=N'SELECT S.ID AS TIPO_CERTIFICADO" & vbCrLf
    sConsulta = sConsulta & ",CASE WHEN PET.COD IS NULL THEN CASE WHEN PU.ID IS NOT NULL THEN 0 ELSE 1 END ELSE 0 END  AS NOPETIC" & vbCrLf
    sConsulta = sConsulta & "FROM SOLICITUD S WITH (NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "INNER JOIN TIPO_SOLICITUDES TS WITH (NOLOCK) ON S.TIPO=TS.ID AND TS.TIPO=2" & vbCrLf
    sConsulta = sConsulta & "INNER JOIN USU WITH (NOLOCK) ON USU.COD=@USU" & vbCrLf
    sConsulta = sConsulta & "LEFT JOIN VIEW_PETICIONARIOS PET WITH (NOLOCK)  ON PET.SOLICITUD=S.ID AND USU.PER = PET.COD" & vbCrLf
    sConsulta = sConsulta & "LEFT JOIN (SELECT S.ID " & vbCrLf
    sConsulta = sConsulta & "   FROM SOLICITUD S WITH (NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN TIPO_SOLICITUDES TS WITH (NOLOCK) ON S.TIPO=TS.ID AND TS.TIPO =2" & vbCrLf
    sConsulta = sConsulta & "   LEFT JOIN VIEW_PETICIONARIOS PET WITH(NOLOCK) ON PET.SOLICITUD=S.ID" & vbCrLf
    sConsulta = sConsulta & "   WHERE S.PUB=1 AND S.BAJA=0 AND PET.SOLICITUD IS NULL" & vbCrLf
    sConsulta = sConsulta & ") PU ON S.ID=PU.ID" & vbCrLf
    sConsulta = sConsulta & "WHERE S.PUB=1'" & vbCrLf
    sConsulta = sConsulta & "IF @CERTIFBAJA=0" & vbCrLf
    sConsulta = sConsulta & "   SET @SQL = @SQL +  ' AND S.BAJA=0 ' " & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL , N'@USU VARCHAR(50)',@USU=@USU" & vbCrLf
    sConsulta = sConsulta & "END" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "--*********************************************************************************************************************************************************" & vbCrLf
    sConsulta = sConsulta & "--3� SELECT DE MATERIALES DE QA POR CADA PROVEEDOR Y TIPO DE CERTIFICADO  que seleccionamos en la 1� query" & vbCrLf
    sConsulta = sConsulta & "--Desde c�digo se dar� valor a las columnas de la 1� SELECT: DEN_MATERIAL y ID_MATERIAL" & vbCrLf
    sConsulta = sConsulta & "--*********************************************************************************************************************************************************" & vbCrLf
    sConsulta = sConsulta & "IF @SOLOCONTADORES =0 AND @OBTENERTODASRELACIONES=1" & vbCrLf
    sConsulta = sConsulta & "BEGIN" & vbCrLf
    sConsulta = sConsulta & "  set @SEL= N'SELECT DISTINCT PR.COD PROVE_COD,M.DEN_SPA DEN_MATERIAL,M.ID ID_MATERIAL, S.ID TIPO_CERTIFICADO '" & vbCrLf
    sConsulta = sConsulta & "  SET @SQL=''" & vbCrLf
    sConsulta = sConsulta & "  --Solicitados" & vbCrLf
    sConsulta = sConsulta & "  IF @ESTADO IS NULL OR NOT(CHARINDEX('2',@ESTADO)=0) OR NOT(CHARINDEX('3',@ESTADO)=0) OR NOT(CHARINDEX('4',@ESTADO)=0) OR NOT(CHARINDEX('5',@ESTADO)=0)" & vbCrLf
    sConsulta = sConsulta & "  BEGIN" & vbCrLf
    sConsulta = sConsulta & "    SET @SQL = @SEL + @JOIN " & vbCrLf
    sConsulta = sConsulta & "   IF @MATERIALES IS NULL AND @REST_PROV_MAT=0" & vbCrLf
    sConsulta = sConsulta & "       SET @SQL = @SQL + N' INNER JOIN MATERIAL_QA_CERTIFICADOS MC WITH(NOLOCK) ON MC.TIPO_CERTIFICADO=S.ID'" & vbCrLf
    sConsulta = sConsulta & "   SET @SQL = @SQL + N' INNER JOIN MATERIALES_QA M WITH(NOLOCK) ON MC.MATERIAL=M.ID'" & vbCrLf
    sConsulta = sConsulta & "    SET @SQL = @SQL + @WHERE   " & vbCrLf
    sConsulta = sConsulta & "  END" & vbCrLf
    sConsulta = sConsulta & "  --Filtro PDTE CUMPLIMENTAR" & vbCrLf
    sConsulta = sConsulta & "  IF @NOMBRE_TABLA <> '' " & vbCrLf
    sConsulta = sConsulta & "  BEGIN" & vbCrLf
    sConsulta = sConsulta & "     IF @ESTADO IS NULL OR NOT(CHARINDEX('2',@ESTADO)=0) OR NOT(CHARINDEX('3',@ESTADO)=0) OR NOT(CHARINDEX('4',@ESTADO)=0) OR NOT(CHARINDEX('5',@ESTADO)=0)" & vbCrLf
    sConsulta = sConsulta & "     BEGIN" & vbCrLf
    sConsulta = sConsulta & "       IF @SQL<>''" & vbCrLf
    sConsulta = sConsulta & "           SET @SQL = @SQL + ' UNION '" & vbCrLf
    sConsulta = sConsulta & "       SET @SQL = @SQL + @SEL + @JOIN2 " & vbCrLf
    sConsulta = sConsulta & "       IF @MATERIALES IS NULL AND @REST_PROV_MAT=0 " & vbCrLf
    sConsulta = sConsulta & "           SET @SQL = @SQL + N' INNER JOIN MATERIAL_QA_CERTIFICADOS MC WITH(NOLOCK) ON MC.TIPO_CERTIFICADO=S.ID'" & vbCrLf
    sConsulta = sConsulta & "       SET @SQL = @SQL + N' INNER JOIN MATERIALES_QA M WITH(NOLOCK) ON MC.MATERIAL=M.ID'" & vbCrLf
    sConsulta = sConsulta & "      SET @SQL = @SQL + @WHERE " & vbCrLf
    sConsulta = sConsulta & "     END" & vbCrLf
    sConsulta = sConsulta & "  END" & vbCrLf
    sConsulta = sConsulta & "  --CON SIN SOLICITAR" & vbCrLf
    sConsulta = sConsulta & "  IF @ESTADO IS NULL OR NOT(CHARINDEX('1',@ESTADO)=0)     " & vbCrLf
    sConsulta = sConsulta & "  BEGIN" & vbCrLf
    sConsulta = sConsulta & "    IF @SQL<>''" & vbCrLf
    sConsulta = sConsulta & "      SET @SQL = @SQL + ' UNION '" & vbCrLf
    sConsulta = sConsulta & "    SET @SQL = @SQL + @SEL + @JOIN3" & vbCrLf
    sConsulta = sConsulta & "    SET @SQL = @SQL + N' INNER JOIN MATERIALES_QA M WITH(NOLOCK) ON MC.MATERIAL=M.ID'" & vbCrLf
    sConsulta = sConsulta & "    SET @SQL = @SQL + @WHERE3  " & vbCrLf
    sConsulta = sConsulta & "  END       " & vbCrLf
    sConsulta = sConsulta & "   " & vbCrLf
    sConsulta = sConsulta & "   IF @OBTENERTODOSCERTIFICADOS=0" & vbCrLf
    sConsulta = sConsulta & "   BEGIN" & vbCrLf
    sConsulta = sConsulta & "       --CALIDAD. Estos dos * se quedan pq no es la tabla de bbdd certificados sino una tabla hecha ad hoc mediante WITH CERTIFICADOS AS (...) y en estos puntos suspensivos puede venir filtro.* o no -> no se conocen las columnas" & vbCrLf
    sConsulta = sConsulta & "       SET @SQL = ';WITH CERTIFICADOS2 AS (' + @SQL2 + ')" & vbCrLf
    sConsulta = sConsulta & "       , TOTCERTIF2 AS (SELECT PROVE_COD, TIPO_CERTIFICADO ,ROW_NUMBER() OVER (ORDER BY ' + @ORDEN + ') AS ROWNUMBER FROM CERTIFICADOS2 WITH (NOLOCK)) " & vbCrLf
    sConsulta = sConsulta & "       SELECT TOTCERTIF2.PROVE_COD,TOTCERTIF2.TIPO_CERTIFICADO ,M.DEN_SPA DEN_MATERIAL,M.ID ID_MATERIAL" & vbCrLf
    sConsulta = sConsulta & "           FROM TOTCERTIF2 WITH (NOLOCK) " & vbCrLf
    sConsulta = sConsulta & "           INNER JOIN MATERIAL_QA_CERTIFICADOS MC WITH(NOLOCK) ON MC.TIPO_CERTIFICADO=TOTCERTIF2.TIPO_CERTIFICADO" & vbCrLf
    sConsulta = sConsulta & "           INNER JOIN MATERIALES_QA M WITH(NOLOCK) ON MC.MATERIAL=M.ID" & vbCrLf
    sConsulta = sConsulta & "       WHERE ROWNUMBER BETWEEN ' + CONVERT(VARCHAR(10),@ROW) + ' AND ' + CONVERT(VARCHAR(10),(@ROW + @PAGESIZE - 1))               " & vbCrLf
    sConsulta = sConsulta & "   END" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "  EXEC SP_EXECUTESQL @SQL , N'@PER VARCHAR(50),@USU VARCHAR(50),@FSQA_PROX_EXPIRAR INT,@CODPROV NVARCHAR(50),@FECDESDE AS DATE,@FECHASTA AS DATE,@UON1 NVARCHAR(50),@UON2 NVARCHAR(50),@UON3 NVARCHAR(50),@DEP NVARCHAR(50)',@PER=@PER,@USU=@USU,@FSQA_PROX_EXPIRAR=@FSQA_PROX_EXPIRAR,@CODPROV=@CODPROV,@FECDESDE=@FECDESDE,@FECHASTA=@FECHASTA ,@UON1=@UON1,@UON2=@UON2,@UON3=@UON3,@DEP=@DEP" & vbCrLf
    sConsulta = sConsulta & " " & vbCrLf
    sConsulta = sConsulta & "END" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CN_OBTENER_DATOSADICIONALES_MENSAJE]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[CN_OBTENER_DATOSADICIONALES_MENSAJE]" & vbCrLf
    gRDOCon.QueryTimeout = 7200
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "CREATE PROCEDURE [dbo].[CN_OBTENER_DATOSADICIONALES_MENSAJE] " & vbCrLf
    sConsulta = sConsulta & "   @IDI NVARCHAR(20)," & vbCrLf
    sConsulta = sConsulta & "   @USU NVARCHAR(20)=NULL, " & vbCrLf
    sConsulta = sConsulta & "   @IDMEN INT=NULL," & vbCrLf
    sConsulta = sConsulta & "   @IDCATEGORIA INT=NULL," & vbCrLf
    sConsulta = sConsulta & "   @UON1 NVARCHAR(50)=NULL," & vbCrLf
    sConsulta = sConsulta & "   @UON2 NVARCHAR(50)=NULL," & vbCrLf
    sConsulta = sConsulta & "   @UON3 NVARCHAR(50)=NULL," & vbCrLf
    sConsulta = sConsulta & "   @DEP NVARCHAR(50)=NULL," & vbCrLf
    sConsulta = sConsulta & "   @RESTR_UO BIT=0," & vbCrLf
    sConsulta = sConsulta & "   @RESTR_DEP BIT=0," & vbCrLf
    sConsulta = sConsulta & "   @EQP NVARCHAR(50)=NULL," & vbCrLf
    sConsulta = sConsulta & "   @RESTR_EQP BIT=0" & vbCrLf
    sConsulta = sConsulta & "AS" & vbCrLf
    sConsulta = sConsulta & "BEGIN" & vbCrLf
    sConsulta = sConsulta & "   DECLARE @SQL NVARCHAR(MAX)" & vbCrLf
    sConsulta = sConsulta & "   " & vbCrLf
    sConsulta = sConsulta & "   SELECT TOP 1 @IDMEN=ID,@IDCATEGORIA=CAT " & vbCrLf
    sConsulta = sConsulta & "   FROM CN_MEN WITH(NOLOCK) " & vbCrLf
    sConsulta = sConsulta & "   WHERE (@USU IS NULL OR USU=@USU) AND (@IDCATEGORIA IS NULL OR CAT=@IDCATEGORIA) AND (@IDMEN IS NULL OR ID=@IDMEN)" & vbCrLf
    sConsulta = sConsulta & "   ORDER BY FECALTA DESC" & vbCrLf
    sConsulta = sConsulta & "   " & vbCrLf
    sConsulta = sConsulta & "   --INFO" & vbCrLf
    sConsulta = sConsulta & "   SELECT @IDMEN IDMEN,@IDCATEGORIA IDCATEGORIA" & vbCrLf
    sConsulta = sConsulta & "       " & vbCrLf
    sConsulta = sConsulta & "   --PARA UON" & vbCrLf
    sConsulta = sConsulta & "   DECLARE @SQLUON NVARCHAR(MAX)" & vbCrLf
    sConsulta = sConsulta & "   SET @SQLUON=N''" & vbCrLf
    sConsulta = sConsulta & "   IF @RESTR_DEP=0 AND @RESTR_UO=0" & vbCrLf
    sConsulta = sConsulta & "   SET @SQLUON=N'SELECT 0 UON0,NULL UON1,NULL UON2,NULL UON3,NULL DEP,NULL USU,PARGEN_LIT.DEN" & vbCrLf
    sConsulta = sConsulta & "   FROM CN_MEN WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PARGEN_LIT WITH(NOLOCK) ON PARGEN_LIT.ID=1 AND IDI=@IDI" & vbCrLf
    sConsulta = sConsulta & "   WHERE CN_MEN.ID=@IDMEN AND UON0=1 UNION ALL '" & vbCrLf
    sConsulta = sConsulta & "   IF @RESTR_DEP=0" & vbCrLf
    sConsulta = sConsulta & "   BEGIN" & vbCrLf
    sConsulta = sConsulta & "       SET @SQLUON=@SQLUON + N'SELECT 0 UON0,UON1.COD UON1,NULL UON2,NULL UON3,NULL DEP,NULL USU,DEN" & vbCrLf
    sConsulta = sConsulta & "       FROM CN_MEN MEN WITH(NOLOCK) " & vbCrLf
    sConsulta = sConsulta & "       INNER JOIN CN_MEN_UON1 WITH(NOLOCK) ON CN_MEN_UON1.MEN=MEN.ID " & vbCrLf
    sConsulta = sConsulta & "       INNER JOIN UON1 WITH(NOLOCK) ON UON1.COD=CN_MEN_UON1.UON1 '" & vbCrLf
    sConsulta = sConsulta & "       IF @RESTR_UO=1" & vbCrLf
    sConsulta = sConsulta & "           SET @SQLUON=@SQLUON + N'AND UON1.COD=@UON1 '" & vbCrLf
    sConsulta = sConsulta & "       SET @SQLUON=@SQLUON + N'WHERE MEN.ID=@IDMEN UNION ALL" & vbCrLf
    sConsulta = sConsulta & "       SELECT 0 UON0,UON2.UON1,UON2.COD UON2,NULL UON3,NULL DEP,NULL USU,DEN" & vbCrLf
    sConsulta = sConsulta & "       FROM CN_MEN MEN WITH(NOLOCK) " & vbCrLf
    sConsulta = sConsulta & "       INNER JOIN CN_MEN_UON2 WITH(NOLOCK) ON CN_MEN_UON2.MEN=MEN.ID " & vbCrLf
    sConsulta = sConsulta & "       INNER JOIN UON2 WITH(NOLOCK) ON UON2.UON1=CN_MEN_UON2.UON1 AND UON2.COD=CN_MEN_UON2.UON2 '" & vbCrLf
    sConsulta = sConsulta & "       IF @RESTR_UO=1" & vbCrLf
    sConsulta = sConsulta & "           SET @SQLUON=@SQLUON + N'AND UON2.UON1=@UON1 AND UON2.COD=@UON2 '" & vbCrLf
    sConsulta = sConsulta & "       SET @SQLUON=@SQLUON + N'WHERE MEN.ID=@IDMEN UNION ALL" & vbCrLf
    sConsulta = sConsulta & "       SELECT 0 UON0,UON3.UON1,UON3.UON2,UON3.COD UON3,NULL DEP,NULL USU,DEN" & vbCrLf
    sConsulta = sConsulta & "       FROM CN_MEN MEN WITH(NOLOCK) " & vbCrLf
    sConsulta = sConsulta & "       INNER JOIN CN_MEN_UON3 WITH(NOLOCK) ON CN_MEN_UON3.MEN=MEN.ID " & vbCrLf
    sConsulta = sConsulta & "       INNER JOIN UON3 WITH(NOLOCK) ON UON3.UON1=CN_MEN_UON3.UON1 AND UON3.UON2=CN_MEN_UON3.UON2 AND UON3.COD=CN_MEN_UON3.UON3 '" & vbCrLf
    sConsulta = sConsulta & "       IF @RESTR_UO=1" & vbCrLf
    sConsulta = sConsulta & "           SET @SQLUON=@SQLUON + N'AND UON3.UON1=@UON1 AND UON3.UON2=@UON2 AND UON3.COD=@UON3 '" & vbCrLf
    sConsulta = sConsulta & "       SET @SQLUON=@SQLUON + N'WHERE MEN.ID=@IDMEN '" & vbCrLf
    sConsulta = sConsulta & "   END" & vbCrLf
    sConsulta = sConsulta & "   IF @SQLUON<>''" & vbCrLf
    sConsulta = sConsulta & "   SET @SQLUON=@SQLUON + N'UNION ALL '" & vbCrLf
    sConsulta = sConsulta & "   IF @RESTR_UO=0 OR (@RESTR_UO=1 AND @UON1 IS NULL)" & vbCrLf
    sConsulta = sConsulta & "   BEGIN   " & vbCrLf
    sConsulta = sConsulta & "       SET @SQLUON=@SQLUON + N'SELECT 0 UON0,NULL UON1,NULL UON2,NULL UON3,DEP,NULL USU,DEN" & vbCrLf
    sConsulta = sConsulta & "       FROM CN_MEN MEN WITH(NOLOCK) " & vbCrLf
    sConsulta = sConsulta & "       INNER JOIN CN_MEN_UON0_DEP WITH(NOLOCK) ON CN_MEN_UON0_DEP.MEN=MEN.ID " & vbCrLf
    sConsulta = sConsulta & "       INNER JOIN DEP WITH(NOLOCK) ON DEP.COD=CN_MEN_UON0_DEP.DEP '" & vbCrLf
    sConsulta = sConsulta & "       IF @RESTR_DEP=1" & vbCrLf
    sConsulta = sConsulta & "           SET @SQLUON=@SQLUON + N'AND DEP.COD=@DEP '" & vbCrLf
    sConsulta = sConsulta & "       SET @SQLUON=@SQLUON + N'WHERE MEN.ID=@IDMEN UNION ALL '" & vbCrLf
    sConsulta = sConsulta & "   END" & vbCrLf
    sConsulta = sConsulta & "   SET @SQLUON=@SQLUON + N'SELECT 0 UON0,UON1,NULL UON2,NULL UON3,DEP,NULL USU,DEN" & vbCrLf
    sConsulta = sConsulta & "   FROM CN_MEN MEN WITH(NOLOCK) " & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN CN_MEN_UON1_DEP WITH(NOLOCK) ON CN_MEN_UON1_DEP.MEN=MEN.ID '" & vbCrLf
    sConsulta = sConsulta & "   IF @RESTR_UO=1" & vbCrLf
    sConsulta = sConsulta & "       SET @SQLUON=@SQLUON + N'AND CN_MEN_UON1_DEP.UON1=@UON1 '" & vbCrLf
    sConsulta = sConsulta & "   IF @RESTR_DEP=1" & vbCrLf
    sConsulta = sConsulta & "       SET @SQLUON=@SQLUON + N'AND CN_MEN_UON1_DEP.DEP=@DEP '" & vbCrLf
    sConsulta = sConsulta & "   SET @SQLUON=@SQLUON + N'INNER JOIN DEP WITH(NOLOCK) ON DEP.COD=CN_MEN_UON1_DEP.DEP" & vbCrLf
    sConsulta = sConsulta & "   WHERE MEN.ID=@IDMEN" & vbCrLf
    sConsulta = sConsulta & "   UNION ALL" & vbCrLf
    sConsulta = sConsulta & "   SELECT 0 UON0,UON1,UON2,NULL UON3,DEP,NULL USU,DEN" & vbCrLf
    sConsulta = sConsulta & "   FROM CN_MEN MEN WITH(NOLOCK) " & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN CN_MEN_UON2_DEP WITH(NOLOCK) ON CN_MEN_UON2_DEP.MEN=MEN.ID '" & vbCrLf
    sConsulta = sConsulta & "   IF @RESTR_UO=1" & vbCrLf
    sConsulta = sConsulta & "       SET @SQLUON=@SQLUON + N'AND CN_MEN_UON2_DEP.UON1=@UON1 AND CN_MEN_UON2_DEP.UON2=@UON2 '" & vbCrLf
    sConsulta = sConsulta & "   IF @RESTR_DEP=1" & vbCrLf
    sConsulta = sConsulta & "       SET @SQLUON=@SQLUON + N'AND CN_MEN_UON2_DEP.DEP=@DEP '" & vbCrLf
    sConsulta = sConsulta & "   SET @SQLUON=@SQLUON + N'INNER JOIN DEP WITH(NOLOCK) ON DEP.COD=CN_MEN_UON2_DEP.DEP" & vbCrLf
    sConsulta = sConsulta & "   WHERE MEN.ID=@IDMEN" & vbCrLf
    sConsulta = sConsulta & "   UNION ALL" & vbCrLf
    sConsulta = sConsulta & "   SELECT 0 UON0,UON1,UON2,UON3,DEP,NULL USU,DEN" & vbCrLf
    sConsulta = sConsulta & "   FROM CN_MEN MEN WITH(NOLOCK) " & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN CN_MEN_UON3_DEP WITH(NOLOCK) ON CN_MEN_UON3_DEP.MEN=MEN.ID '" & vbCrLf
    sConsulta = sConsulta & "   IF @RESTR_UO=1" & vbCrLf
    sConsulta = sConsulta & "       SET @SQLUON=@SQLUON + N'AND CN_MEN_UON3_DEP.UON1=@UON1 AND CN_MEN_UON3_DEP.UON2=@UON2 " & vbCrLf
    sConsulta = sConsulta & "           AND CN_MEN_UON3_DEP.UON2=@UON3 '" & vbCrLf
    sConsulta = sConsulta & "   IF @RESTR_DEP=1" & vbCrLf
    sConsulta = sConsulta & "       SET @SQLUON=@SQLUON + N'AND CN_MEN_UON3_DEP.DEP=@DEP '" & vbCrLf
    sConsulta = sConsulta & "   SET @SQLUON=@SQLUON + N'INNER JOIN DEP WITH(NOLOCK) ON DEP.COD=CN_MEN_UON3_DEP.DEP" & vbCrLf
    sConsulta = sConsulta & "   WHERE MEN.ID=@IDMEN" & vbCrLf
    sConsulta = sConsulta & "   UNION ALL" & vbCrLf
    sConsulta = sConsulta & "   SELECT 0 UON0,UON1,UON2,UON3,DEP,CN_MEN_USU.USU,ISNULL(PER.NOM,'''') + '' '' + APE DEN" & vbCrLf
    sConsulta = sConsulta & "   FROM CN_MEN MEN WITH(NOLOCK) " & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN CN_MEN_USU WITH(NOLOCK) ON CN_MEN_USU.MEN=MEN.ID" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN USU WITH(NOLOCK) ON USU.COD=CN_MEN_USU.USU AND USU.BAJA=0" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PER WITH(NOLOCK) ON PER.COD=USU.PER AND PER.BAJALOG=0 '" & vbCrLf
    sConsulta = sConsulta & "   IF @RESTR_UO=1" & vbCrLf
    sConsulta = sConsulta & "       SET @SQLUON=@SQLUON + N'AND (PER.UON1=@UON1 OR (@UON1 IS NULL AND PER.UON1 IS NULL)) " & vbCrLf
    sConsulta = sConsulta & "       AND (PER.UON2=@UON2 OR (@UON2 IS NULL AND PER.UON2 IS NULL)) " & vbCrLf
    sConsulta = sConsulta & "       AND (PER.UON2=@UON3 OR (@UON3 IS NULL AND PER.UON3 IS NULL)) '" & vbCrLf
    sConsulta = sConsulta & "   IF @RESTR_DEP=1" & vbCrLf
    sConsulta = sConsulta & "       SET @SQLUON=@SQLUON + N'AND PER.DEP=@DEP '" & vbCrLf
    sConsulta = sConsulta & "   SET @SQLUON=@SQLUON + N'WHERE MEN.ID=@IDMEN '" & vbCrLf
    sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQLUON, N'@IDMEN INT,@IDI NVARCHAR(20),@UON1 NVARCHAR(50),@UON2 NVARCHAR(50),@UON3 NVARCHAR(50),@DEP NVARCHAR(50)', " & vbCrLf
    sConsulta = sConsulta & "       @IDMEN=@IDMEN,@IDI=@IDI,@UON1=@UON1,@UON2=@UON2,@UON3=@UON3,@DEP=@DEP" & vbCrLf
    sConsulta = sConsulta & "   " & vbCrLf
    sConsulta = sConsulta & "   --PARA PROVEEDORES" & vbCrLf
    sConsulta = sConsulta & "   SET @SQL=N'SELECT NULL PROVECOD,NULL PROVECON,PROVEPARA,NULL GMN1,NULL GMN2,NULL GMN3,NULL GMN4,NULL MATERIALES_QA,NULL DEN" & vbCrLf
    sConsulta = sConsulta & "   FROM CN_MEN MEN WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   WHERE MEN.ID=@IDMEN AND PROVEPARA NOT IN (0,5,6)" & vbCrLf
    sConsulta = sConsulta & "   UNION ALL" & vbCrLf
    sConsulta = sConsulta & "   SELECT NULL PROVECOD,NULL PROVECON,PROVEPARA,GMN1.COD GMN1,NULL GMN2,NULL GMN3,NULL GMN4,NULL MATERIALES_QA,GMN1.DEN_' + @IDI + ' DEN" & vbCrLf
    sConsulta = sConsulta & "   FROM CN_MEN MEN WITH(NOLOCK) " & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN GMN1 WITH(NOLOCK) " & vbCrLf
    sConsulta = sConsulta & "   ON GMN1.COD=MEN.GMN1 AND MEN.GMN2 IS NULL AND MEN.GMN3 IS NULL AND MEN.GMN4 IS NULL AND MEN.PROVEPARA=6" & vbCrLf
    sConsulta = sConsulta & "   WHERE MEN.ID=@IDMEN" & vbCrLf
    sConsulta = sConsulta & "   UNION ALL" & vbCrLf
    sConsulta = sConsulta & "   SELECT NULL PROVECOD,NULL PROVECON,PROVEPARA,GMN2.GMN1 GMN1,GMN2.COD GMN2,NULL GMN3,NULL GMN4,NULL MATERIALES_QA,GMN2.DEN_' + @IDI + ' DEN" & vbCrLf
    sConsulta = sConsulta & "   FROM CN_MEN MEN WITH(NOLOCK) " & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN GMN2 WITH(NOLOCK) " & vbCrLf
    sConsulta = sConsulta & "   ON GMN2.GMN1=MEN.GMN1 AND GMN2.COD=MEN.GMN2 AND MEN.GMN3 IS NULL AND MEN.GMN4 IS NULL AND MEN.PROVEPARA=6" & vbCrLf
    sConsulta = sConsulta & "   WHERE MEN.ID=@IDMEN" & vbCrLf
    sConsulta = sConsulta & "   UNION ALL" & vbCrLf
    sConsulta = sConsulta & "   SELECT NULL PROVECOD,NULL PROVECON,PROVEPARA,GMN3.GMN1 GMN1,GMN3.GMN2 GMN2,GMN3.COD GMN3,NULL GMN4,NULL MATERIALES_QA,GMN3.DEN_' + @IDI + ' DEN" & vbCrLf
    sConsulta = sConsulta & "   FROM CN_MEN MEN WITH(NOLOCK) " & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN GMN3 WITH(NOLOCK) " & vbCrLf
    sConsulta = sConsulta & "   ON GMN3.GMN1=MEN.GMN1 AND GMN3.GMN2=MEN.GMN2 AND GMN3.COD=MEN.GMN3 AND MEN.GMN4 IS NULL AND MEN.PROVEPARA=6" & vbCrLf
    sConsulta = sConsulta & "   WHERE MEN.ID=@IDMEN" & vbCrLf
    sConsulta = sConsulta & "   UNION ALL" & vbCrLf
    sConsulta = sConsulta & "   SELECT NULL PROVECOD,NULL PROVECON,PROVEPARA,GMN4.GMN1 GMN1,GMN4.GMN2 GMN2,GMN4.GMN3 GMN3,GMN4.COD GMN4,NULL MATERIALES_QA,GMN4.DEN_' + @IDI + ' DEN" & vbCrLf
    sConsulta = sConsulta & "   FROM CN_MEN MEN WITH(NOLOCK) " & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN GMN4 WITH(NOLOCK) " & vbCrLf
    sConsulta = sConsulta & "   ON GMN4.GMN1=MEN.GMN1 AND GMN4.GMN2=MEN.GMN2 AND GMN4.GMN3=MEN.GMN3 AND GMN4.COD=MEN.GMN4 AND MEN.PROVEPARA=6" & vbCrLf
    sConsulta = sConsulta & "   WHERE MEN.ID=@IDMEN" & vbCrLf
    sConsulta = sConsulta & "   UNION ALL" & vbCrLf
    sConsulta = sConsulta & "   SELECT NULL PROVECOD,NULL PROVECON,PROVEPARA,NULL GMN1,NULL GMN2,NULL GMN3,NULL GMN4,MATERIALES_QA.ID MATERIALES_QA,MATERIALES_QA.DEN_' + @IDI + ' DEN" & vbCrLf
    sConsulta = sConsulta & "   FROM CN_MEN MEN WITH(NOLOCK) INNER JOIN MATERIALES_QA WITH(NOLOCK) " & vbCrLf
    sConsulta = sConsulta & "   ON MATERIALES_QA.ID=MEN.MATERIALES_QA AND MEN.PROVEPARA=5" & vbCrLf
    sConsulta = sConsulta & "   WHERE MEN.ID=@IDMEN" & vbCrLf
    sConsulta = sConsulta & "   UNION ALL" & vbCrLf
    sConsulta = sConsulta & "   SELECT PROVE.COD PROVECOD,CON.ID PROVECON,0 PROVEPARA,NULL GMN1,NULL GMN2,NULL GMN3,NULL GMN4,NULL MATERIALES_QA,PROVE.COD + '' - '' + PROVE.DEN + '' ('' + CON.EMAIL + '')'' DEN" & vbCrLf
    sConsulta = sConsulta & "   FROM CN_MEN MEN WITH(NOLOCK) " & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN CN_MEN_CON WITH(NOLOCK) ON CN_MEN_CON.MEN=MEN.ID" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PROVE WITH(NOLOCK) ON PROVE.COD=CN_MEN_CON.PROVE" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN CON WITH(NOLOCK) ON CON.ID=CN_MEN_CON.CON AND CON.PROVE=CN_MEN_CON.PROVE" & vbCrLf
    sConsulta = sConsulta & "   WHERE MEN.ID=@IDMEN'" & vbCrLf
    sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL, N'@IDMEN INT,@EQP NVARCHAR(50)',@IDMEN=@IDMEN,@EQP=@EQP" & vbCrLf
    sConsulta = sConsulta & "   " & vbCrLf
    sConsulta = sConsulta & "   --PARA PROCE COMPRAS" & vbCrLf
    sConsulta = sConsulta & "   SELECT PROCE_ANYO,PROCE_GMN1,PROCE_COD,PROCEPARARESP,PROCEPARAINVITADO,PROCEPARACOMP,PROCEPARAPROVE" & vbCrLf
    sConsulta = sConsulta & "   FROM CN_MEN WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   WHERE ID=@IDMEN AND (PROCEPARARESP=1 OR PROCEPARAINVITADO=1 OR PROCEPARACOMP=1 OR PROCEPARAPROVE=1)" & vbCrLf
    sConsulta = sConsulta & "   " & vbCrLf
    sConsulta = sConsulta & "   --PARA ESTRUC COMPRAS" & vbCrLf
    sConsulta = sConsulta & "   SET @SQL=N'SELECT NULL GMN1,NULL GMN2,NULL GMN3,NULL GMN4,EQP.COD EQP,EQP.DEN" & vbCrLf
    sConsulta = sConsulta & "   FROM CN_MEN MEN WITH(NOLOCK) INNER JOIN CN_MEN_EQP WITH(NOLOCK) ON CN_MEN_EQP.MEN=MEN.ID" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN EQP WITH(NOLOCK) ON EQP.COD=CN_MEN_EQP.EQP '" & vbCrLf
    sConsulta = sConsulta & "   IF @RESTR_EQP=1" & vbCrLf
    sConsulta = sConsulta & "       SET @SQL=@SQL + N'AND CN_MEN_EQP.EQP=@EQP '" & vbCrLf
    sConsulta = sConsulta & "   SET @SQL=@SQL + N'WHERE MEN.ID=@IDMEN" & vbCrLf
    sConsulta = sConsulta & "   UNION ALL" & vbCrLf
    sConsulta = sConsulta & "   SELECT GMN1.COD GMN1,NULL GMN2,NULL GMN3,NULL GMN4,NULL EQP,GMN1.DEN_' + @IDI + ' DEN" & vbCrLf
    sConsulta = sConsulta & "   FROM CN_MEN MEN WITH(NOLOCK) " & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN GMN1 WITH(NOLOCK) " & vbCrLf
    sConsulta = sConsulta & "   ON GMN1.COD=MEN.GMN1 AND MEN.GMN2 IS NULL AND MEN.GMN3 IS NULL AND MEN.GMN4 IS NULL AND MEN.COMPPARA=1" & vbCrLf
    sConsulta = sConsulta & "   WHERE MEN.ID=@IDMEN" & vbCrLf
    sConsulta = sConsulta & "   UNION ALL" & vbCrLf
    sConsulta = sConsulta & "   SELECT GMN2.GMN1 GMN1,GMN2.COD GMN2,NULL GMN3,NULL GMN4,NULL EQP,GMN2.DEN_' + @IDI + ' DEN" & vbCrLf
    sConsulta = sConsulta & "   FROM CN_MEN MEN WITH(NOLOCK) " & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN GMN2 WITH(NOLOCK) " & vbCrLf
    sConsulta = sConsulta & "   ON GMN2.GMN1=MEN.GMN1 AND GMN2.COD=MEN.GMN2 AND MEN.GMN3 IS NULL AND MEN.GMN4 IS NULL AND MEN.COMPPARA=1" & vbCrLf
    sConsulta = sConsulta & "   WHERE MEN.ID=@IDMEN" & vbCrLf
    sConsulta = sConsulta & "   UNION ALL" & vbCrLf
    sConsulta = sConsulta & "   SELECT GMN3.GMN1 GMN1,GMN3.GMN2 GMN2,GMN3.COD GMN3,NULL GMN4,NULL EQP,GMN3.DEN_' + @IDI + ' DEN" & vbCrLf
    sConsulta = sConsulta & "   FROM CN_MEN MEN WITH(NOLOCK) " & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN GMN3 WITH(NOLOCK) " & vbCrLf
    sConsulta = sConsulta & "   ON GMN3.GMN1=MEN.GMN1 AND GMN3.GMN2=MEN.GMN2 AND GMN3.COD=MEN.GMN3 AND MEN.GMN4 IS NULL AND MEN.COMPPARA=1" & vbCrLf
    sConsulta = sConsulta & "   WHERE MEN.ID=@IDMEN" & vbCrLf
    sConsulta = sConsulta & "   UNION ALL" & vbCrLf
    sConsulta = sConsulta & "   SELECT GMN4.GMN1 GMN1,GMN4.GMN2 GMN2,GMN4.GMN3 GMN3,GMN4.COD GMN4,NULL EQP,GMN4.DEN_' + @IDI + ' DEN" & vbCrLf
    sConsulta = sConsulta & "   FROM CN_MEN MEN WITH(NOLOCK) " & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN GMN4 WITH(NOLOCK) " & vbCrLf
    sConsulta = sConsulta & "   ON GMN4.GMN1=MEN.GMN1 AND GMN4.GMN2=MEN.GMN2 AND GMN4.GMN3=MEN.GMN3 AND GMN4.COD=MEN.GMN4 AND MEN.COMPPARA=1" & vbCrLf
    sConsulta = sConsulta & "   WHERE MEN.ID=@IDMEN'" & vbCrLf
    sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL, N'@IDMEN  INT,@EQP NVARCHAR(50)', @IDMEN=@IDMEN,@EQP=@EQP" & vbCrLf
    sConsulta = sConsulta & "   " & vbCrLf
    sConsulta = sConsulta & "   --PARA GRUPOS" & vbCrLf
    sConsulta = sConsulta & "   SELECT USUEPPARA,USUGSPARA,USUPMPARA,USUQAPARA,USUSMPARA,NULL GRUPO,NULL DEN" & vbCrLf
    sConsulta = sConsulta & "   FROM CN_MEN WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   WHERE ID=@IDMEN AND (USUEPPARA=1 OR USUGSPARA=1 OR USUPMPARA=1 OR USUQAPARA=1 OR USUSMPARA=1)" & vbCrLf
    sConsulta = sConsulta & "   UNION ALL" & vbCrLf
    sConsulta = sConsulta & "   SELECT NULL USUEPPARA,NULL USUGSPARA,NULL USUPMPARA,NULL USUQAPARA,NULL USUSMPARA,CN_GRUPO.ID GRUPO,CN_GRUPO_IDI.DEN" & vbCrLf
    sConsulta = sConsulta & "   FROM CN_MEN MEN WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN CN_MEN_GRUPO WITH(NOLOCK) ON CN_MEN_GRUPO.MEN=MEN.ID" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN CN_GRUPO WITH(NOLOCK) ON CN_GRUPO.ID=CN_MEN_GRUPO.GRUPO" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN CN_GRUPO_IDI WITH(NOLOCK) ON CN_GRUPO_IDI.GRUPO=CN_GRUPO.ID AND CN_GRUPO_IDI.IDI=@IDI" & vbCrLf
    sConsulta = sConsulta & "   WHERE MEN.ID=@IDMEN" & vbCrLf
    sConsulta = sConsulta & "END" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CN_OBTENER_CATEGORIAS_MENSAJES_USU]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[CN_OBTENER_CATEGORIAS_MENSAJES_USU]" & vbCrLf
    gRDOCon.QueryTimeout = 7200
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "CREATE PROCEDURE [dbo].[CN_OBTENER_CATEGORIAS_MENSAJES_USU]" & vbCrLf
    sConsulta = sConsulta & "   @USU NVARCHAR(50)," & vbCrLf
    sConsulta = sConsulta & "   @IDI NVARCHAR(50)," & vbCrLf
    sConsulta = sConsulta & "   @ACCESOEP BIT=0," & vbCrLf
    sConsulta = sConsulta & "   @ACCESOGS BIT=0," & vbCrLf
    sConsulta = sConsulta & "   @ACCESOPM BIT=0," & vbCrLf
    sConsulta = sConsulta & "   @ACCESOQA BIT=0," & vbCrLf
    sConsulta = sConsulta & "   @ACCESOSM BIT=0" & vbCrLf
    sConsulta = sConsulta & "AS" & vbCrLf
    sConsulta = sConsulta & "BEGIN" & vbCrLf
    sConsulta = sConsulta & "   ;WITH CategoriasCN AS(" & vbCrLf
    sConsulta = sConsulta & "       SELECT DISTINCT CC.ID,CC.CAT,CC.NIVEL,CCI.DEN,CC.PERMISOPARAPUB   " & vbCrLf
    sConsulta = sConsulta & "           FROM CN_MEN CM WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "           INNER JOIN CN_OPT_MEN_USU COM WITH(NOLOCK) ON COM.MEN=CM.ID AND COM.USU=@USU" & vbCrLf
    sConsulta = sConsulta & "           INNER JOIN CN_CAT CC WITH(NOLOCK) ON CC.ID=CM.CAT " & vbCrLf
    sConsulta = sConsulta & "           INNER JOIN CN_CAT_IDI CCI WITH(NOLOCK) ON CCI.CAT=CM.CAT AND CCI.IDI=@IDI" & vbCrLf
    sConsulta = sConsulta & "       UNION " & vbCrLf
    sConsulta = sConsulta & "          SELECT DISTINCT CC.ID,CC.CAT,CC.NIVEL,CCI.DEN,CC.PERMISOPARAPUB   " & vbCrLf
    sConsulta = sConsulta & "          FROM CN_MEN CM WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "          INNER JOIN CN_CAT CC WITH(NOLOCK) ON CC.ID=CM.CAT " & vbCrLf
    sConsulta = sConsulta & "          INNER JOIN CN_CAT_IDI CCI WITH(NOLOCK) ON CCI.CAT=CM.CAT AND CCI.IDI=@IDI" & vbCrLf
    sConsulta = sConsulta & "          WHERE CM.UON0=1 " & vbCrLf
    sConsulta = sConsulta & "           OR (@ACCESOEP=1 AND ISNULL(CM.USUEPPARA,0)=1) " & vbCrLf
    sConsulta = sConsulta & "           OR (@ACCESOGS=1 AND ISNULL(CM.USUGSPARA,0)=1)" & vbCrLf
    sConsulta = sConsulta & "           OR (@ACCESOPM=1 AND ISNULL(CM.USUPMPARA,0)=1) " & vbCrLf
    sConsulta = sConsulta & "           OR (@ACCESOQA=1 AND ISNULL(CM.USUQAPARA,0)=1) " & vbCrLf
    sConsulta = sConsulta & "           OR (@ACCESOSM=1 AND ISNULL(CM.USUSMPARA,0)=1)       " & vbCrLf
    sConsulta = sConsulta & "       UNION ALL " & vbCrLf
    sConsulta = sConsulta & "           SELECT CC0.ID, CC0.CAT,CC0.NIVEL,CCI0.DEN,CC0.PERMISOPARAPUB   " & vbCrLf
    sConsulta = sConsulta & "           FROM CN_CAT AS CC0  WITH(NOLOCK) " & vbCrLf
    sConsulta = sConsulta & "           INNER JOIN CategoriasCN AS CCN ON CCN.CAT=CC0.ID " & vbCrLf
    sConsulta = sConsulta & "           INNER JOIN CN_CAT_IDI AS CCI0 WITH (NOLOCK) ON CCI0.CAT=CC0.ID AND CCI0.IDI=@IDI)" & vbCrLf
    sConsulta = sConsulta & "       SELECT DISTINCT ID,CAT,NIVEL,DEN,PERMISOPARAPUB FROM CategoriasCN" & vbCrLf
    sConsulta = sConsulta & "END" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CN_OBTENER_MENSAJESUSUARIO]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[CN_OBTENER_MENSAJESUSUARIO]" & vbCrLf
    gRDOCon.QueryTimeout = 7200
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "CREATE PROCEDURE [dbo].[CN_OBTENER_MENSAJESUSUARIO]" & vbCrLf
    sConsulta = sConsulta & "   @USU NVARCHAR(20)," & vbCrLf
    sConsulta = sConsulta & "   @IDI NVARCHAR(20)," & vbCrLf
    sConsulta = sConsulta & "   @UON1 NVARCHAR(20)=NULL," & vbCrLf
    sConsulta = sConsulta & "   @UON2 NVARCHAR(20)=NULL," & vbCrLf
    sConsulta = sConsulta & "   @UON3 NVARCHAR(20)=NULL," & vbCrLf
    sConsulta = sConsulta & "   @DEP NVARCHAR(20)=NULL," & vbCrLf
    sConsulta = sConsulta & "   @ACCESOEP BIT=NULL," & vbCrLf
    sConsulta = sConsulta & "   @ACCESOGS BIT=NULL," & vbCrLf
    sConsulta = sConsulta & "   @ACCESOPM BIT=NULL," & vbCrLf
    sConsulta = sConsulta & "   @ACCESOQA BIT=NULL," & vbCrLf
    sConsulta = sConsulta & "   @ACCESOSM BIT=NULL," & vbCrLf
    sConsulta = sConsulta & "   @TIPO INT =NULL," & vbCrLf
    sConsulta = sConsulta & "   @LIMITEMENSAJESCARGADOS INT," & vbCrLf
    sConsulta = sConsulta & "   @PAGINA INT," & vbCrLf
    sConsulta = sConsulta & "   @IDCATEGORIA INT=NULL," & vbCrLf
    sConsulta = sConsulta & "   @IDGRUPO INT=NULL," & vbCrLf
    sConsulta = sConsulta & "   @MEGUSTA BIT=NULL," & vbCrLf
    sConsulta = sConsulta & "   @HISTORICO BIT=0," & vbCrLf
    sConsulta = sConsulta & "   @DISCREPANCIAS BIT=0," & vbCrLf
    sConsulta = sConsulta & "   @FACTURA INT=NULL," & vbCrLf
    sConsulta = sConsulta & "   @LINEA INT=NULL" & vbCrLf
    sConsulta = sConsulta & "AS" & vbCrLf
    sConsulta = sConsulta & "BEGIN" & vbCrLf
    sConsulta = sConsulta & "   DECLARE @REGINI INT" & vbCrLf
    sConsulta = sConsulta & "   DECLARE @REGFIN INT" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   SET @REGINI = (@LIMITEMENSAJESCARGADOS * (@PAGINA-1))+1" & vbCrLf
    sConsulta = sConsulta & "   SET @REGFIN = @REGINI + @LIMITEMENSAJESCARGADOS-1" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   DECLARE @HIST VARCHAR(2)=''" & vbCrLf
    sConsulta = sConsulta & "   IF @HISTORICO=1" & vbCrLf
    sConsulta = sConsulta & "       SET @HIST='H_'" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   DECLARE @SQL NVARCHAR(MAX)              " & vbCrLf
    sConsulta = sConsulta & "   SET @SQL=N';WITH IDMENSAJES AS (SELECT MEN.ID, CN_OPT_MEN_USU.OCULTO,MEN.FECALTA,MEN.CAT," & vbCrLf
    sConsulta = sConsulta & "       MEN.FECACT,MEN.USU,MEN.TIPO,MEN.TITULO,MEN.CONTENIDO,MEN.DONDE,MEN.CUANDO,MEN.PROVE," & vbCrLf
    sConsulta = sConsulta & "       MEN.IDCON,DISCRP.ID IDDISCREPANCIA '" & vbCrLf
    sConsulta = sConsulta & "   IF @DISCREPANCIAS=1" & vbCrLf
    sConsulta = sConsulta & "       SET @SQL=@SQL + N',DISCRP.EST DISCREPANCIAABIERTA '" & vbCrLf
    sConsulta = sConsulta & "   SET @SQL=@SQL + N'FROM CN_' + @HIST + 'MEN MEN WITH(NOLOCK) " & vbCrLf
    sConsulta = sConsulta & "       INNER JOIN CN_' + @HIST + 'OPT_MEN_USU WITH(NOLOCK) " & vbCrLf
    sConsulta = sConsulta & "       ON CN_' + @HIST + 'OPT_MEN_USU.MEN=MEN.ID AND CN_' + @HIST + 'OPT_MEN_USU.USU=@USU" & vbCrLf
    sConsulta = sConsulta & "       AND (@TIPO IS NULL OR MEN.TIPO=@TIPO) AND (@IDCATEGORIA IS NULL OR MEN.CAT=@IDCATEGORIA) " & vbCrLf
    sConsulta = sConsulta & "       INNER JOIN CN_CAT WITH(NOLOCK) ON CN_CAT.ID=MEN.CAT AND CN_CAT.DESPUBENRED=0 '      " & vbCrLf
    sConsulta = sConsulta & "   IF @IDGRUPO IS NOT NULL" & vbCrLf
    sConsulta = sConsulta & "       SET @SQL=@SQL + N'INNER JOIN CN_' + @HIST + 'MEN_GRUPO GRUPO WITH(NOLOCK) ON GRUPO.MEN=MEN.ID AND GRUPO.GRUPO=@IDGRUPO '" & vbCrLf
    sConsulta = sConsulta & "   IF @MEGUSTA IS NOT NULL" & vbCrLf
    sConsulta = sConsulta & "       SET @SQL=@SQL + N'INNER JOIN CN_' + @HIST + 'RESP RESP WITH(NOLOCK) ON RESP.MEN=MEN.ID AND RESP.USU=@USU AND RESP.MEGUSTA=1 '" & vbCrLf
    sConsulta = sConsulta & "   IF @DISCREPANCIAS=1" & vbCrLf
    sConsulta = sConsulta & "       SET @SQL=@SQL + N'INNER JOIN LIN_FACTURA_DISCRP DISCRP WITH(NOLOCK) ON DISCRP.MEN=MEN.ID " & vbCrLf
    sConsulta = sConsulta & "           AND (@FACTURA IS NULL OR DISCRP.FACTURA=@FACTURA) AND (@LINEA IS NULL OR DISCRP.LINEA=@LINEA) '" & vbCrLf
    sConsulta = sConsulta & "   ELSE" & vbCrLf
    sConsulta = sConsulta & "       SET @SQL=@SQL + N'LEFT JOIN LIN_FACTURA_DISCRP DISCRP WITH(NOLOCK) ON DISCRP.MEN=MEN.ID '" & vbCrLf
    sConsulta = sConsulta & "   SET @SQL=@SQL + N'UNION   " & vbCrLf
    sConsulta = sConsulta & "       SELECT MEN.ID, ISNULL(CN_' + @HIST + 'OPT_MEN_USU.OCULTO,0) OCULTO,MEN.FECALTA,MEN.CAT," & vbCrLf
    sConsulta = sConsulta & "       MEN.FECACT,MEN.USU,MEN.TIPO,MEN.TITULO,MEN.CONTENIDO,MEN.DONDE,MEN.CUANDO,MEN.PROVE," & vbCrLf
    sConsulta = sConsulta & "       MEN.IDCON,DISCRP.ID IDDISCREPANCIA '" & vbCrLf
    sConsulta = sConsulta & "   IF @DISCREPANCIAS=1" & vbCrLf
    sConsulta = sConsulta & "       SET @SQL=@SQL + N',DISCRP.EST DISCREPANCIAABIERTA '" & vbCrLf
    sConsulta = sConsulta & "   SET @SQL=@SQL + N'FROM CN_' + @HIST + 'MEN MEN WITH(NOLOCK) " & vbCrLf
    sConsulta = sConsulta & "       INNER JOIN CN_CAT WITH(NOLOCK) ON CN_CAT.ID=MEN.CAT AND CN_CAT.DESPUBENRED=0" & vbCrLf
    sConsulta = sConsulta & "       LEFT JOIN CN_' + @HIST + 'OPT_MEN_USU WITH(NOLOCK) ON CN_' + @HIST + 'OPT_MEN_USU.MEN=MEN.ID AND CN_' + @HIST + 'OPT_MEN_USU.USU=@USU '" & vbCrLf
    sConsulta = sConsulta & "   IF @IDGRUPO IS NOT NULL" & vbCrLf
    sConsulta = sConsulta & "       SET @SQL=@SQL + N'INNER JOIN CN_' + @HIST + 'MEN_GRUPO GRUPO WITH(NOLOCK) ON GRUPO.MEN=MEN.ID AND GRUPO.GRUPO=@IDGRUPO '" & vbCrLf
    sConsulta = sConsulta & "   IF @MEGUSTA IS NOT NULL" & vbCrLf
    sConsulta = sConsulta & "       SET @SQL=@SQL + N'INNER JOIN CN_' + @HIST + 'RESP RESP WITH(NOLOCK) ON RESP.MEN=MEN.ID AND RESP.USU=@USU AND RESP.MEGUSTA=1 '" & vbCrLf
    sConsulta = sConsulta & "   IF @DISCREPANCIAS=1" & vbCrLf
    sConsulta = sConsulta & "       SET @SQL=@SQL + N'INNER JOIN LIN_FACTURA_DISCRP DISCRP WITH(NOLOCK) ON DISCRP.MEN=MEN.ID " & vbCrLf
    sConsulta = sConsulta & "           AND (@FACTURA IS NULL OR DISCRP.FACTURA=@FACTURA) AND (@LINEA IS NULL OR DISCRP.LINEA=@LINEA) '" & vbCrLf
    sConsulta = sConsulta & "   ELSE" & vbCrLf
    sConsulta = sConsulta & "       SET @SQL=@SQL + N'LEFT JOIN LIN_FACTURA_DISCRP DISCRP WITH(NOLOCK) ON DISCRP.MEN=MEN.ID '" & vbCrLf
    sConsulta = sConsulta & "   SET @SQL=@SQL + N'WHERE ( MEN.UON0=1 --Estruc. Org. Nivel 0" & vbCrLf
    sConsulta = sConsulta & "           OR (@ACCESOEP=1 AND ISNULL(MEN.USUEPPARA,0)=1) --grupo corporativo EP  " & vbCrLf
    sConsulta = sConsulta & "           OR (@ACCESOGS=1 AND ISNULL(MEN.USUGSPARA,0)=1) --grupo corporativo GS" & vbCrLf
    sConsulta = sConsulta & "           OR (@ACCESOPM=1 AND ISNULL(MEN.USUPMPARA,0)=1) --grupo corporativo PM" & vbCrLf
    sConsulta = sConsulta & "           OR (@ACCESOQA=1 AND ISNULL(MEN.USUQAPARA,0)=1) --grupo corporativo QA" & vbCrLf
    sConsulta = sConsulta & "           OR (@ACCESOSM=1 AND ISNULL(MEN.USUSMPARA,0)=1) --grupo corporativo SM" & vbCrLf
    sConsulta = sConsulta & "           )" & vbCrLf
    sConsulta = sConsulta & "           AND (@TIPO IS NULL OR MEN.TIPO=@TIPO) AND (@IDCATEGORIA IS NULL OR MEN.CAT=@IDCATEGORIA)" & vbCrLf
    sConsulta = sConsulta & "       ),TOTMENSAJES AS (SELECT IDMENSAJES.ID,IDMENSAJES.FECALTA," & vbCrLf
    sConsulta = sConsulta & "       IDMENSAJES.FECACT,IDMENSAJES.USU,IDMENSAJES.TIPO,IDMENSAJES.TITULO,IDMENSAJES.CONTENIDO,IDMENSAJES.DONDE,IDMENSAJES.CUANDO," & vbCrLf
    sConsulta = sConsulta & "       PER.APE,PER.NOM,DEP.DEN SITUACIONUO,CN_USU_ADJUN.DGUID GUIDIMAGENUSU,IDMENSAJES.IDDISCREPANCIA," & vbCrLf
    sConsulta = sConsulta & "       CATNOMBRE.DEN CATEGORIA,CN_USU_NOTIF.USU LEIDO,IDMENSAJES.OCULTO,IDMENSAJES.PROVE,IDMENSAJES.IDCON CON," & vbCrLf
    sConsulta = sConsulta & "       CON.APE APEPROVE,CON.NOM NOMPROVE,CON.DEP SITUACIONUOPROVE,PROVE.DEN PROVEDEN,CN_USU_ADJUN2.DGUID GUIDIMAGENPROVE," & vbCrLf
    sConsulta = sConsulta & "       ROW_NUMBER() OVER (ORDER BY IDMENSAJES.FECACT DESC,IDMENSAJES.ID ASC) AS ROWNUMBER '" & vbCrLf
    sConsulta = sConsulta & "   IF @DISCREPANCIAS=1" & vbCrLf
    sConsulta = sConsulta & "       SET @SQL=@SQL + N',IDMENSAJES.DISCREPANCIAABIERTA ' " & vbCrLf
    sConsulta = sConsulta & "   SET @SQL=@SQL + N'FROM IDMENSAJES WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "       LEFT JOIN CN_USU_NOTIF WITH(NOLOCK) ON CN_USU_NOTIF.MEN=IDMENSAJES.ID AND CN_USU_NOTIF.USU=@USU AND CN_USU_NOTIF.IDRESPUESTA=0 '        " & vbCrLf
    sConsulta = sConsulta & "   SET @SQL=@SQL + N'LEFT JOIN USU WITH(NOLOCK) ON USU.COD=IDMENSAJES.USU" & vbCrLf
    sConsulta = sConsulta & "       LEFT JOIN PER WITH(NOLOCK) ON PER.COD=USU.PER " & vbCrLf
    sConsulta = sConsulta & "       LEFT JOIN DEP WITH(NOLOCK) ON DEP.COD=PER.DEP" & vbCrLf
    sConsulta = sConsulta & "       LEFT JOIN CON WITH(NOLOCK) ON CON.ID=IDMENSAJES.IDCON AND CON.PROVE=IDMENSAJES.PROVE" & vbCrLf
    sConsulta = sConsulta & "       LEFT JOIN PROVE WITH(NOLOCK) ON PROVE.COD=CON.PROVE" & vbCrLf
    sConsulta = sConsulta & "       LEFT JOIN CN_USU WITH(NOLOCK) ON CN_USU.COD=IDMENSAJES.USU  " & vbCrLf
    sConsulta = sConsulta & "       LEFT JOIN CN_USU CN_USU2 WITH(NOLOCK) ON CN_USU2.PROVE=IDMENSAJES.PROVE AND CN_USU2.CON=IDMENSAJES.IDCON" & vbCrLf
    sConsulta = sConsulta & "       LEFT JOIN CN_USU_ADJUN WITH(NOLOCK) ON CN_USU.ID_USU_ADJUN= CN_USU_ADJUN.ID" & vbCrLf
    sConsulta = sConsulta & "       LEFT JOIN CN_USU_ADJUN CN_USU_ADJUN2 WITH(NOLOCK) ON CN_USU2.ID_USU_ADJUN=CN_USU_ADJUN2.ID" & vbCrLf
    sConsulta = sConsulta & "       INNER JOIN CN_CAT_IDI CATNOMBRE WITH(NOLOCK) ON CATNOMBRE.CAT=IDMENSAJES.CAT AND CATNOMBRE.IDI=@IDI)" & vbCrLf
    sConsulta = sConsulta & "       SELECT ID,FECALTA,FECACT,USU,TIPO,TITULO,CONTENIDO,DONDE,CUANDO,APE,NOM,SITUACIONUO,IDDISCREPANCIA," & vbCrLf
    sConsulta = sConsulta & "       GUIDIMAGENUSU,CATEGORIA,LEIDO,OCULTO,' + CONVERT(VARCHAR(1),@HISTORICO) + ' MENSAJEHISTORICO," & vbCrLf
    sConsulta = sConsulta & "       PROVE,CON,APEPROVE,NOMPROVE,SITUACIONUOPROVE,PROVEDEN,GUIDIMAGENPROVE,ROWNUMBER '" & vbCrLf
    sConsulta = sConsulta & "   IF @DISCREPANCIAS=1" & vbCrLf
    sConsulta = sConsulta & "       SET @SQL=@SQL + N',IDDISCREPANCIA,DISCREPANCIAABIERTA '" & vbCrLf
    sConsulta = sConsulta & "   SET @SQL=@SQL + N'FROM TOTMENSAJES WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "       WHERE ROWNUMBER BETWEEN @REGINI AND @REGFIN '" & vbCrLf
    sConsulta = sConsulta & "   IF @DISCREPANCIAS=1" & vbCrLf
    sConsulta = sConsulta & "       SET @SQL=@SQL + N'ORDER BY DISCREPANCIAABIERTA '" & vbCrLf
    sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL, N'@UON1 NVARCHAR(20),@UON2 NVARCHAR(20),@UON3 NVARCHAR(20),@DEP NVARCHAR(20)," & vbCrLf
    sConsulta = sConsulta & "       @USU NVARCHAR(20),@IDI NVARCHAR(20),@TIPO INT,@REGINI INT,@REGFIN INT,@IDCATEGORIA INT,@IDGRUPO INT," & vbCrLf
    sConsulta = sConsulta & "       @FACTURA INT,@LINEA INT,@ACCESOEP BIT=NULL,@ACCESOGS BIT=NULL,@ACCESOPM BIT=NULL,@ACCESOQA BIT=NULL,@ACCESOSM BIT=NULL', " & vbCrLf
    sConsulta = sConsulta & "       @UON1=@UON1,@UON2=@UON2,@UON3=@UON3,@DEP=@DEP," & vbCrLf
    sConsulta = sConsulta & "       @USU=@USU,@IDI=@IDI,@TIPO=@TIPO,@REGINI=@REGINI,@REGFIN=@REGFIN,@IDCATEGORIA=@IDCATEGORIA,@IDGRUPO=@IDGRUPO," & vbCrLf
    sConsulta = sConsulta & "       @FACTURA=@FACTURA,@LINEA=@LINEA, @ACCESOEP=@ACCESOEP,@ACCESOGS=@ACCESOGS,@ACCESOPM=@ACCESOPM,@ACCESOQA=@ACCESOQA,@ACCESOSM=@ACCESOSM" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   DECLARE @SQLRESPUESTAS NVARCHAR(MAX)    " & vbCrLf
    sConsulta = sConsulta & "   SET @SQLRESPUESTAS =N';WITH IDRESPUESTAS AS (SELECT MEN.ID, CN_OPT_MEN_USU.OCULTO,MEN.FECALTA,MEN.CAT," & vbCrLf
    sConsulta = sConsulta & "       MEN.FECACT,MEN.USU,MEN.TIPO,MEN.TITULO,MEN.CONTENIDO,MEN.DONDE,MEN.CUANDO" & vbCrLf
    sConsulta = sConsulta & "       FROM CN_' + @HIST + 'MEN MEN WITH(NOLOCK) " & vbCrLf
    sConsulta = sConsulta & "       INNER JOIN CN_' + @HIST + 'OPT_MEN_USU WITH(NOLOCK) " & vbCrLf
    sConsulta = sConsulta & "       ON CN_' + @HIST + 'OPT_MEN_USU.MEN=MEN.ID AND CN_' + @HIST + 'OPT_MEN_USU.USU=@USU" & vbCrLf
    sConsulta = sConsulta & "       AND (@TIPO IS NULL OR MEN.TIPO=@TIPO) AND (@IDCATEGORIA IS NULL OR MEN.CAT=@IDCATEGORIA)" & vbCrLf
    sConsulta = sConsulta & "       INNER JOIN CN_CAT WITH(NOLOCK) ON CN_CAT.ID=MEN.CAT AND CN_CAT.DESPUBENRED=0 ' " & vbCrLf
    sConsulta = sConsulta & "   IF @IDGRUPO IS NOT NULL" & vbCrLf
    sConsulta = sConsulta & "       SET @SQLRESPUESTAS=@SQLRESPUESTAS + N'INNER JOIN CN_' + @HIST + 'MEN_GRUPO GRUPO WITH(NOLOCK) ON GRUPO.MEN=MEN.ID AND GRUPO.GRUPO=@IDGRUPO '" & vbCrLf
    sConsulta = sConsulta & "   IF @MEGUSTA IS NOT NULL" & vbCrLf
    sConsulta = sConsulta & "       SET @SQLRESPUESTAS=@SQLRESPUESTAS + N'INNER JOIN CN_' + @HIST + 'RESP RESP WITH(NOLOCK) ON RESP.MEN=MEN.ID AND RESP.USU=@USU AND RESP.MEGUSTA=1 '" & vbCrLf
    sConsulta = sConsulta & "   IF @DISCREPANCIAS=1" & vbCrLf
    sConsulta = sConsulta & "       SET @SQLRESPUESTAS=@SQLRESPUESTAS + N'INNER JOIN LIN_FACTURA_DISCRP DISCRP WITH(NOLOCK) ON DISCRP.MEN=MEN.ID " & vbCrLf
    sConsulta = sConsulta & "           AND (@FACTURA IS NULL OR DISCRP.FACTURA=@FACTURA) AND (@LINEA IS NULL OR DISCRP.LINEA=@LINEA) '" & vbCrLf
    sConsulta = sConsulta & "   SET @SQLRESPUESTAS=@SQLRESPUESTAS + N'UNION   " & vbCrLf
    sConsulta = sConsulta & "       SELECT MEN.ID, ISNULL(CN_' + @HIST + 'OPT_MEN_USU.OCULTO,0) OCULTO,MEN.FECALTA,MEN.CAT," & vbCrLf
    sConsulta = sConsulta & "       MEN.FECACT,MEN.USU,MEN.TIPO,MEN.TITULO,MEN.CONTENIDO,MEN.DONDE,MEN.CUANDO" & vbCrLf
    sConsulta = sConsulta & "       FROM CN_' + @HIST + 'MEN MEN WITH(NOLOCK) " & vbCrLf
    sConsulta = sConsulta & "       INNER JOIN CN_CAT WITH(NOLOCK) ON CN_CAT.ID=MEN.CAT AND CN_CAT.DESPUBENRED=0" & vbCrLf
    sConsulta = sConsulta & "       LEFT JOIN CN_' + @HIST + 'OPT_MEN_USU WITH(NOLOCK) ON CN_' + @HIST + 'OPT_MEN_USU.MEN=MEN.ID AND CN_' + @HIST + 'OPT_MEN_USU.USU=@USU '" & vbCrLf
    sConsulta = sConsulta & "   IF @IDGRUPO IS NOT NULL" & vbCrLf
    sConsulta = sConsulta & "       SET @SQLRESPUESTAS=@SQLRESPUESTAS + N'INNER JOIN CN_' + @HIST + 'MEN_GRUPO GRUPO WITH(NOLOCK) ON GRUPO.MEN=MEN.ID AND GRUPO.GRUPO=@IDGRUPO '" & vbCrLf
    sConsulta = sConsulta & "   IF @MEGUSTA IS NOT NULL" & vbCrLf
    sConsulta = sConsulta & "       SET @SQLRESPUESTAS=@SQLRESPUESTAS + N'INNER JOIN CN_' + @HIST + 'RESP RESP WITH(NOLOCK) ON RESP.MEN=MEN.ID AND RESP.USU=@USU AND RESP.MEGUSTA=1 '" & vbCrLf
    sConsulta = sConsulta & "   IF @DISCREPANCIAS=1" & vbCrLf
    sConsulta = sConsulta & "       SET @SQLRESPUESTAS=@SQLRESPUESTAS + N'INNER JOIN LIN_FACTURA_DISCRP DISCRP WITH(NOLOCK) ON DISCRP.MEN=MEN.ID " & vbCrLf
    sConsulta = sConsulta & "           AND (@FACTURA IS NULL OR DISCRP.FACTURA=@FACTURA) AND (@LINEA IS NULL OR DISCRP.LINEA=@LINEA) '" & vbCrLf
    sConsulta = sConsulta & "   SET @SQLRESPUESTAS=@SQLRESPUESTAS + N'WHERE ( MEN.UON0=1 --Estruc. Org. Nivel 0" & vbCrLf
    sConsulta = sConsulta & "           OR (@ACCESOEP=1 AND ISNULL(MEN.USUEPPARA,0)=1) --grupo corporativo EP  " & vbCrLf
    sConsulta = sConsulta & "           OR (@ACCESOGS=1 AND ISNULL(MEN.USUGSPARA,0)=1) --grupo corporativo GS" & vbCrLf
    sConsulta = sConsulta & "           OR (@ACCESOPM=1 AND ISNULL(MEN.USUPMPARA,0)=1) --grupo corporativo PM" & vbCrLf
    sConsulta = sConsulta & "           OR (@ACCESOQA=1 AND ISNULL(MEN.USUQAPARA,0)=1) --grupo corporativo QA" & vbCrLf
    sConsulta = sConsulta & "           OR (@ACCESOSM=1 AND ISNULL(MEN.USUSMPARA,0)=1) --grupo corporativo SM" & vbCrLf
    sConsulta = sConsulta & "           ) " & vbCrLf
    sConsulta = sConsulta & "           AND (@TIPO IS NULL OR MEN.TIPO=@TIPO) AND (@IDCATEGORIA IS NULL OR MEN.CAT=@IDCATEGORIA) " & vbCrLf
    sConsulta = sConsulta & "       ),TOTRESPUESTAS AS (SELECT DISTINCT RESP.ID,NTF.IDRESPUESTA VISIBLE,RESP.USU,PER.APE,PER.NOM,DEP.DEN SITUACIONUO,CN_USU_ADJUN.DGUID GUIDIMAGENUSU," & vbCrLf
    sConsulta = sConsulta & "       RESP.CONTENIDO,RESP.MEGUSTA,RESP.FECALTA,RESP.FECACT,RESP.MEN," & vbCrLf
    sConsulta = sConsulta & "       USU2.COD USUCITADO,PER2.APE APECITADO,PER2.NOM NOMCITADO,DEP2.DEN SITUACIONUOCITADO,RESP.RESP," & vbCrLf
    sConsulta = sConsulta & "       RESP.PROVE,RESP.CON,CON.APE APEPROVE,CON.NOM NOMPROVE,CON.DEP SITUACIONUOPROVE,PROVE.DEN PROVEDEN," & vbCrLf
    sConsulta = sConsulta & "       RESP.PROVECITADO,RESP.CONCITADO,CON2.APE APEPROVECITADO,CON2.NOM NOMPROVECITADO," & vbCrLf
    sConsulta = sConsulta & "       CON2.DEP SITUACIONUOPROVECITADO,PROVE2.DEN PROVEDENCITADO,CN_USU_ADJUN2.DGUID  GUIDIMAGENPROVE," & vbCrLf
    sConsulta = sConsulta & "       ROW_NUMBER() OVER (ORDER BY IDRESPUESTAS.FECACT DESC,IDRESPUESTAS.ID ASC) AS ROWNUMBER" & vbCrLf
    sConsulta = sConsulta & "       FROM IDRESPUESTAS WITH(NOLOCK) " & vbCrLf
    sConsulta = sConsulta & "       INNER JOIN CN_' + @HIST + 'RESP RESP WITH(NOLOCK) ON RESP.MEN=IDRESPUESTAS.ID AND RESP.MEGUSTA=0" & vbCrLf
    sConsulta = sConsulta & "       LEFT JOIN CON WITH(NOLOCK) ON CON.PROVE=RESP.PROVE AND CON.ID=RESP.CON" & vbCrLf
    sConsulta = sConsulta & "       LEFT JOIN PROVE WITH(NOLOCK) ON PROVE.COD=CON.PROVE" & vbCrLf
    sConsulta = sConsulta & "       LEFT JOIN CN_USU WITH(NOLOCK) ON CN_USU.COD=RESP.USU" & vbCrLf
    sConsulta = sConsulta & "       LEFT JOIN CN_USU CN_USU2 WITH(NOLOCK) ON CN_USU2.PROVE=RESP.PROVE AND CN_USU2.CON=RESP.CON" & vbCrLf
    sConsulta = sConsulta & "       LEFT JOIN USU WITH(NOLOCK) ON USU.COD=RESP.USU" & vbCrLf
    sConsulta = sConsulta & "       LEFT JOIN PER WITH(NOLOCK) ON PER.COD=USU.PER " & vbCrLf
    sConsulta = sConsulta & "       LEFT JOIN DEP WITH(NOLOCK) ON DEP.COD=PER.DEP" & vbCrLf
    sConsulta = sConsulta & "       LEFT JOIN CN_USU_ADJUN WITH(NOLOCK) ON CN_USU.ID_USU_ADJUN= CN_USU_ADJUN.ID " & vbCrLf
    sConsulta = sConsulta & "       LEFT JOIN CN_USU_ADJUN CN_USU_ADJUN2 WITH(NOLOCK) ON CN_USU2.ID_USU_ADJUN=CN_USU_ADJUN2.ID  " & vbCrLf
    sConsulta = sConsulta & "       LEFT JOIN CON CON2 WITH(NOLOCK) ON CON2.PROVE=RESP.PROVECITADO AND CON2.ID=RESP.CONCITADO   " & vbCrLf
    sConsulta = sConsulta & "       LEFT JOIN PROVE PROVE2 WITH(NOLOCK) ON PROVE2.COD=CON2.PROVE    " & vbCrLf
    sConsulta = sConsulta & "       LEFT JOIN USU USU2 WITH(NOLOCK) ON USU2.COD=RESP.USUCITADO" & vbCrLf
    sConsulta = sConsulta & "       LEFT JOIN PER PER2 WITH(NOLOCK) ON PER2.COD=USU2.PER" & vbCrLf
    sConsulta = sConsulta & "       LEFT JOIN DEP DEP2 WITH(NOLOCK) ON DEP2.COD=PER2.DEP" & vbCrLf
    sConsulta = sConsulta & "       LEFT JOIN CN_USU_NOTIF NTF WITH(NOLOCK) ON NTF.MEN=RESP.MEN AND NTF.IDRESPUESTA=RESP.ID AND NTF.USU=@USU)" & vbCrLf
    sConsulta = sConsulta & "       SELECT DISTINCT ID,VISIBLE,USU,APE,NOM,SITUACIONUO,GUIDIMAGENUSU,CONTENIDO,MEGUSTA,FECALTA,FECACT,MEN," & vbCrLf
    sConsulta = sConsulta & "       USUCITADO,APECITADO,NOMCITADO,SITUACIONUOCITADO,RESP,PROVE,CON,APEPROVE,NOMPROVE,SITUACIONUOPROVE,PROVEDEN," & vbCrLf
    sConsulta = sConsulta & "       PROVECITADO,CONCITADO,APEPROVECITADO,NOMPROVECITADO,SITUACIONUOPROVECITADO,PROVEDENCITADO,GUIDIMAGENPROVE" & vbCrLf
    sConsulta = sConsulta & "       FROM TOTRESPUESTAS WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "       ORDER BY FECACT ASC'" & vbCrLf
    sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQLRESPUESTAS, N'@UON1 NVARCHAR(20),@UON2 NVARCHAR(20),@UON3 NVARCHAR(20),@DEP NVARCHAR(20)," & vbCrLf
    sConsulta = sConsulta & "       @USU NVARCHAR(20),@IDI NVARCHAR(20),@TIPO INT,@REGINI INT,@REGFIN INT,@IDCATEGORIA INT,@IDGRUPO INT," & vbCrLf
    sConsulta = sConsulta & "       @FACTURA INT,@LINEA INT,@ACCESOEP BIT=NULL,@ACCESOGS BIT=NULL,@ACCESOPM BIT=NULL,@ACCESOQA BIT=NULL,@ACCESOSM BIT=NULL', " & vbCrLf
    sConsulta = sConsulta & "       @UON1=@UON1,@UON2=@UON2,@UON3=@UON3,@DEP=@DEP," & vbCrLf
    sConsulta = sConsulta & "       @USU=@USU,@IDI=@IDI,@TIPO=@TIPO,@REGINI=@REGINI,@REGFIN=@REGFIN,@IDCATEGORIA=@IDCATEGORIA,@IDGRUPO=@IDGRUPO," & vbCrLf
    sConsulta = sConsulta & "       @FACTURA=@FACTURA,@LINEA=@LINEA, @ACCESOEP=@ACCESOEP,@ACCESOGS=@ACCESOGS,@ACCESOPM=@ACCESOPM,@ACCESOQA=@ACCESOQA,@ACCESOSM=@ACCESOSM" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   DECLARE @SQLMEGUSTA NVARCHAR(MAX)" & vbCrLf
    sConsulta = sConsulta & "   SET @SQLMEGUSTA=N';WITH IDMEGUSTA AS (SELECT MEN.ID, CN_OPT_MEN_USU.OCULTO,MEN.FECALTA,MEN.CAT," & vbCrLf
    sConsulta = sConsulta & "       MEN.FECACT,MEN.USU,MEN.TIPO,MEN.TITULO,MEN.CONTENIDO,MEN.DONDE,MEN.CUANDO" & vbCrLf
    sConsulta = sConsulta & "       FROM CN_' + @HIST + 'MEN MEN WITH(NOLOCK) " & vbCrLf
    sConsulta = sConsulta & "       INNER JOIN CN_' + @HIST + 'OPT_MEN_USU WITH(NOLOCK) " & vbCrLf
    sConsulta = sConsulta & "       ON CN_' + @HIST + 'OPT_MEN_USU.MEN=MEN.ID AND CN_' + @HIST + 'OPT_MEN_USU.USU=@USU" & vbCrLf
    sConsulta = sConsulta & "       AND (@TIPO IS NULL OR MEN.TIPO=@TIPO) AND (@IDCATEGORIA IS NULL OR MEN.CAT=@IDCATEGORIA)" & vbCrLf
    sConsulta = sConsulta & "       INNER JOIN CN_CAT WITH(NOLOCK) ON CN_CAT.ID=MEN.CAT AND CN_CAT.DESPUBENRED=0 ' " & vbCrLf
    sConsulta = sConsulta & "   IF @IDGRUPO IS NOT NULL" & vbCrLf
    sConsulta = sConsulta & "       SET @SQLMEGUSTA=@SQLMEGUSTA + N'INNER JOIN CN_' + @HIST + 'MEN_GRUPO GRUPO WITH(NOLOCK) ON GRUPO.MEN=MEN.ID AND GRUPO.GRUPO=@IDGRUPO '" & vbCrLf
    sConsulta = sConsulta & "   IF @MEGUSTA IS NOT NULL" & vbCrLf
    sConsulta = sConsulta & "       SET @SQLMEGUSTA=@SQLMEGUSTA + N'INNER JOIN CN_' + @HIST + 'RESP RESP WITH(NOLOCK) ON RESP.MEN=MEN.ID AND RESP.USU=@USU AND RESP.MEGUSTA=1 '" & vbCrLf
    sConsulta = sConsulta & "   IF @DISCREPANCIAS=1" & vbCrLf
    sConsulta = sConsulta & "       SET @SQLMEGUSTA=@SQLMEGUSTA + N'INNER JOIN LIN_FACTURA_DISCRP DISCRP WITH(NOLOCK) ON DISCRP.MEN=MEN.ID " & vbCrLf
    sConsulta = sConsulta & "           AND (@FACTURA IS NULL OR DISCRP.FACTURA=@FACTURA) AND (@LINEA IS NULL OR DISCRP.LINEA=@LINEA) '" & vbCrLf
    sConsulta = sConsulta & "   SET @SQLMEGUSTA=@SQLMEGUSTA + N'UNION   " & vbCrLf
    sConsulta = sConsulta & "       SELECT MEN.ID, ISNULL(CN_' + @HIST + 'OPT_MEN_USU.OCULTO,0) OCULTO,MEN.FECALTA,MEN.CAT," & vbCrLf
    sConsulta = sConsulta & "       MEN.FECACT,MEN.USU,MEN.TIPO,MEN.TITULO,MEN.CONTENIDO,MEN.DONDE,MEN.CUANDO" & vbCrLf
    sConsulta = sConsulta & "       FROM CN_' + @HIST + 'MEN MEN WITH(NOLOCK) " & vbCrLf
    sConsulta = sConsulta & "       INNER JOIN CN_CAT WITH(NOLOCK) ON CN_CAT.ID=MEN.CAT AND CN_CAT.DESPUBENRED=0" & vbCrLf
    sConsulta = sConsulta & "       LEFT JOIN CN_' + @HIST + 'OPT_MEN_USU WITH(NOLOCK) ON CN_' + @HIST + 'OPT_MEN_USU.MEN=MEN.ID AND CN_' + @HIST + 'OPT_MEN_USU.USU=@USU '" & vbCrLf
    sConsulta = sConsulta & "   IF @IDGRUPO IS NOT NULL" & vbCrLf
    sConsulta = sConsulta & "       SET @SQLMEGUSTA=@SQLMEGUSTA + N'INNER JOIN CN_' + @HIST + 'MEN_GRUPO GRUPO WITH(NOLOCK) ON GRUPO.MEN=MEN.ID AND GRUPO.GRUPO=@IDGRUPO '" & vbCrLf
    sConsulta = sConsulta & "   IF @MEGUSTA IS NOT NULL" & vbCrLf
    sConsulta = sConsulta & "       SET @SQLMEGUSTA=@SQLMEGUSTA + N'INNER JOIN CN_' + @HIST + 'RESP RESP WITH(NOLOCK) ON RESP.MEN=MEN.ID AND RESP.USU=@USU AND RESP.MEGUSTA=1 '" & vbCrLf
    sConsulta = sConsulta & "   IF @DISCREPANCIAS=1" & vbCrLf
    sConsulta = sConsulta & "       SET @SQLMEGUSTA=@SQLMEGUSTA + N'INNER JOIN LIN_FACTURA_DISCRP DISCRP WITH(NOLOCK) ON DISCRP.MEN=MEN.ID " & vbCrLf
    sConsulta = sConsulta & "           AND (@FACTURA IS NULL OR DISCRP.FACTURA=@FACTURA) AND (@LINEA IS NULL OR DISCRP.LINEA=@LINEA) '" & vbCrLf
    sConsulta = sConsulta & "   SET @SQLMEGUSTA=@SQLMEGUSTA + N'WHERE ( MEN.UON0=1 --Estruc. Org. Nivel 0" & vbCrLf
    sConsulta = sConsulta & "           OR (@ACCESOEP=1 AND ISNULL(MEN.USUEPPARA,0)=1) --grupo corporativo EP  " & vbCrLf
    sConsulta = sConsulta & "           OR (@ACCESOGS=1 AND ISNULL(MEN.USUGSPARA,0)=1) --grupo corporativo GS" & vbCrLf
    sConsulta = sConsulta & "           OR (@ACCESOPM=1 AND ISNULL(MEN.USUPMPARA,0)=1) --grupo corporativo PM" & vbCrLf
    sConsulta = sConsulta & "           OR (@ACCESOQA=1 AND ISNULL(MEN.USUQAPARA,0)=1) --grupo corporativo QA" & vbCrLf
    sConsulta = sConsulta & "           OR (@ACCESOSM=1 AND ISNULL(MEN.USUSMPARA,0)=1) --grupo corporativo SM" & vbCrLf
    sConsulta = sConsulta & "           ) " & vbCrLf
    sConsulta = sConsulta & "           AND (@TIPO IS NULL OR MEN.TIPO=@TIPO) AND (@IDCATEGORIA IS NULL OR MEN.CAT=@IDCATEGORIA) " & vbCrLf
    sConsulta = sConsulta & "       ),TOTMEGUSTA AS (SELECT DISTINCT RESP.ID,RESP.FECALTA,RESP.FECACT,RESP.MEN,RESP.RESP," & vbCrLf
    sConsulta = sConsulta & "       RESP.USU,PER.APE,PER.NOM,DEP.DEN SITUACIONUO,           " & vbCrLf
    sConsulta = sConsulta & "       USU2.COD USUCITADO,PER2.APE APECITADO,PER2.NOM NOMCITADO,DEP2.DEN SITUACIONUOCITADO," & vbCrLf
    sConsulta = sConsulta & "       RESP.PROVE,RESP.CON,CON.APE APEPROVE,CON.NOM NOMPROVE,CON.DEP SITUACIONUOPROVE,PROVE.DEN PROVEDEN," & vbCrLf
    sConsulta = sConsulta & "       RESP.PROVECITADO,RESP.CONCITADO,CON2.APE APEPROVECITADO,CON2.NOM NOMPROVECITADO," & vbCrLf
    sConsulta = sConsulta & "       CON2.DEP SITUACIONUOPROVECITADO,PROVE2.DEN PROVEDENCITADO," & vbCrLf
    sConsulta = sConsulta & "       ROW_NUMBER() OVER (ORDER BY IDMEGUSTA.FECACT DESC,IDMEGUSTA.ID ASC) AS ROWNUMBER" & vbCrLf
    sConsulta = sConsulta & "       FROM IDMEGUSTA WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "       INNER JOIN CN_' + @HIST + 'RESP RESP WITH(NOLOCK) ON RESP.MEN=IDMEGUSTA.ID AND RESP.MEGUSTA=1 AND RESP.RESP IS NULL" & vbCrLf
    sConsulta = sConsulta & "       LEFT JOIN CON WITH(NOLOCK) ON CON.PROVE=RESP.PROVE AND CON.ID=RESP.CON" & vbCrLf
    sConsulta = sConsulta & "       LEFT JOIN PROVE WITH(NOLOCK) ON PROVE.COD=CON.PROVE" & vbCrLf
    sConsulta = sConsulta & "       LEFT JOIN USU WITH(NOLOCK) ON USU.COD=RESP.USU" & vbCrLf
    sConsulta = sConsulta & "       LEFT JOIN PER WITH(NOLOCK) ON PER.COD=USU.PER " & vbCrLf
    sConsulta = sConsulta & "       LEFT JOIN DEP WITH(NOLOCK) ON DEP.COD=PER.DEP" & vbCrLf
    sConsulta = sConsulta & "       LEFT JOIN CON CON2 WITH(NOLOCK) ON CON2.PROVE=RESP.PROVECITADO AND CON2.ID=RESP.CONCITADO" & vbCrLf
    sConsulta = sConsulta & "       LEFT JOIN PROVE PROVE2 WITH(NOLOCK) ON PROVE2.COD=CON2.PROVE        " & vbCrLf
    sConsulta = sConsulta & "       LEFT JOIN USU USU2 WITH(NOLOCK) ON USU2.COD=RESP.USUCITADO" & vbCrLf
    sConsulta = sConsulta & "       LEFT JOIN PER PER2 WITH(NOLOCK) ON PER2.COD=USU2.PER" & vbCrLf
    sConsulta = sConsulta & "       LEFT JOIN DEP DEP2 WITH(NOLOCK) ON DEP2.COD=PER2.DEP)" & vbCrLf
    sConsulta = sConsulta & "       SELECT DISTINCT ID,FECALTA,FECACT,MEN,RESP,USU,APE,NOM,SITUACIONUO,         " & vbCrLf
    sConsulta = sConsulta & "       USUCITADO,APECITADO,NOMCITADO,SITUACIONUOCITADO,PROVE,CON,APEPROVE,NOMPROVE,SITUACIONUOPROVE,PROVEDEN," & vbCrLf
    sConsulta = sConsulta & "       PROVECITADO,CONCITADO,APEPROVECITADO,NOMPROVECITADO,SITUACIONUOPROVECITADO,PROVEDENCITADO" & vbCrLf
    sConsulta = sConsulta & "       FROM TOTMEGUSTA WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "       ORDER BY FECACT ASC'    " & vbCrLf
    sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQLMEGUSTA, N'@UON1 NVARCHAR(20),@UON2 NVARCHAR(20),@UON3 NVARCHAR(20),@DEP NVARCHAR(20)," & vbCrLf
    sConsulta = sConsulta & "       @USU NVARCHAR(20),@IDI NVARCHAR(20),@TIPO INT,@REGINI INT,@REGFIN INT,@IDCATEGORIA INT,@IDGRUPO INT," & vbCrLf
    sConsulta = sConsulta & "       @FACTURA INT,@LINEA INT,@ACCESOEP BIT=NULL,@ACCESOGS BIT=NULL,@ACCESOPM BIT=NULL,@ACCESOQA BIT=NULL,@ACCESOSM BIT=NULL', " & vbCrLf
    sConsulta = sConsulta & "       @UON1=@UON1,@UON2=@UON2,@UON3=@UON3,@DEP=@DEP," & vbCrLf
    sConsulta = sConsulta & "       @USU=@USU,@IDI=@IDI,@TIPO=@TIPO,@REGINI=@REGINI,@REGFIN=@REGFIN,@IDCATEGORIA=@IDCATEGORIA,@IDGRUPO=@IDGRUPO," & vbCrLf
    sConsulta = sConsulta & "       @FACTURA=@FACTURA,@LINEA=@LINEA, @ACCESOEP=@ACCESOEP,@ACCESOGS=@ACCESOGS,@ACCESOPM=@ACCESOPM,@ACCESOQA=@ACCESOQA,@ACCESOSM=@ACCESOSM" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   DECLARE @SQLMEGUSTARESPUESTA NVARCHAR(MAX)" & vbCrLf
    sConsulta = sConsulta & "   SET @SQLMEGUSTARESPUESTA =N';WITH IDMEGUSTARESPUESTA AS (SELECT MEN.ID, CN_OPT_MEN_USU.OCULTO,MEN.FECALTA,MEN.CAT," & vbCrLf
    sConsulta = sConsulta & "       MEN.FECACT,MEN.USU,MEN.TIPO,MEN.TITULO,MEN.CONTENIDO,MEN.DONDE,MEN.CUANDO" & vbCrLf
    sConsulta = sConsulta & "       FROM CN_' + @HIST + 'MEN MEN WITH(NOLOCK) " & vbCrLf
    sConsulta = sConsulta & "       INNER JOIN CN_' + @HIST + 'OPT_MEN_USU WITH(NOLOCK) " & vbCrLf
    sConsulta = sConsulta & "       ON CN_' + @HIST + 'OPT_MEN_USU.MEN=MEN.ID AND CN_' + @HIST + 'OPT_MEN_USU.USU=@USU" & vbCrLf
    sConsulta = sConsulta & "       AND (@TIPO IS NULL OR MEN.TIPO=@TIPO) AND (@IDCATEGORIA IS NULL OR MEN.CAT=@IDCATEGORIA)" & vbCrLf
    sConsulta = sConsulta & "       INNER JOIN CN_CAT WITH(NOLOCK) ON CN_CAT.ID=MEN.CAT AND CN_CAT.DESPUBENRED=0 ' " & vbCrLf
    sConsulta = sConsulta & "   IF @IDGRUPO IS NOT NULL" & vbCrLf
    sConsulta = sConsulta & "       SET @SQLMEGUSTARESPUESTA=@SQLMEGUSTARESPUESTA + N'INNER JOIN CN_' + @HIST + 'MEN_GRUPO GRUPO WITH(NOLOCK) ON GRUPO.MEN=MEN.ID AND GRUPO.GRUPO=@IDGRUPO '" & vbCrLf
    sConsulta = sConsulta & "   IF @MEGUSTA IS NOT NULL" & vbCrLf
    sConsulta = sConsulta & "       SET @SQLMEGUSTARESPUESTA=@SQLMEGUSTARESPUESTA + N'INNER JOIN CN_' + @HIST + 'RESP RESP WITH(NOLOCK) ON RESP.MEN=MEN.ID AND RESP.USU=@USU AND RESP.MEGUSTA=1 '" & vbCrLf
    sConsulta = sConsulta & "   IF @DISCREPANCIAS=1" & vbCrLf
    sConsulta = sConsulta & "       SET @SQLMEGUSTARESPUESTA=@SQLMEGUSTARESPUESTA + N'INNER JOIN LIN_FACTURA_DISCRP DISCRP WITH(NOLOCK) ON DISCRP.MEN=MEN.ID " & vbCrLf
    sConsulta = sConsulta & "           AND (@FACTURA IS NULL OR DISCRP.FACTURA=@FACTURA) AND (@LINEA IS NULL OR DISCRP.LINEA=@LINEA) '" & vbCrLf
    sConsulta = sConsulta & "   SET @SQLMEGUSTARESPUESTA=@SQLMEGUSTARESPUESTA + N'UNION   " & vbCrLf
    sConsulta = sConsulta & "       SELECT MEN.ID, ISNULL(CN_' + @HIST + 'OPT_MEN_USU.OCULTO,0) OCULTO,MEN.FECALTA,MEN.CAT," & vbCrLf
    sConsulta = sConsulta & "       MEN.FECACT,MEN.USU,MEN.TIPO,MEN.TITULO,MEN.CONTENIDO,MEN.DONDE,MEN.CUANDO" & vbCrLf
    sConsulta = sConsulta & "       FROM CN_' + @HIST + 'MEN MEN WITH(NOLOCK) " & vbCrLf
    sConsulta = sConsulta & "       INNER JOIN CN_CAT WITH(NOLOCK) ON CN_CAT.ID=MEN.CAT AND CN_CAT.DESPUBENRED=0" & vbCrLf
    sConsulta = sConsulta & "       LEFT JOIN CN_' + @HIST + 'OPT_MEN_USU WITH(NOLOCK) ON CN_' + @HIST + 'OPT_MEN_USU.MEN=MEN.ID AND CN_' + @HIST + 'OPT_MEN_USU.USU=@USU '" & vbCrLf
    sConsulta = sConsulta & "   IF @IDGRUPO IS NOT NULL" & vbCrLf
    sConsulta = sConsulta & "       SET @SQLMEGUSTARESPUESTA=@SQLMEGUSTARESPUESTA + N'INNER JOIN CN_' + @HIST + 'MEN_GRUPO GRUPO WITH(NOLOCK) ON GRUPO.MEN=MEN.ID AND GRUPO.GRUPO=@IDGRUPO '" & vbCrLf
    sConsulta = sConsulta & "   IF @MEGUSTA IS NOT NULL" & vbCrLf
    sConsulta = sConsulta & "       SET @SQLMEGUSTARESPUESTA=@SQLMEGUSTARESPUESTA + N'INNER JOIN CN_' + @HIST + 'RESP RESP WITH(NOLOCK) ON RESP.MEN=MEN.ID AND RESP.USU=@USU AND RESP.MEGUSTA=1 '" & vbCrLf
    sConsulta = sConsulta & "   IF @DISCREPANCIAS=1" & vbCrLf
    sConsulta = sConsulta & "       SET @SQLMEGUSTARESPUESTA=@SQLMEGUSTARESPUESTA + N'INNER JOIN LIN_FACTURA_DISCRP DISCRP WITH(NOLOCK) ON DISCRP.MEN=MEN.ID " & vbCrLf
    sConsulta = sConsulta & "           AND (@FACTURA IS NULL OR DISCRP.FACTURA=@FACTURA) AND (@LINEA IS NULL OR DISCRP.LINEA=@LINEA) '" & vbCrLf
    sConsulta = sConsulta & "   SET @SQLMEGUSTARESPUESTA=@SQLMEGUSTARESPUESTA + N'WHERE ( MEN.UON0=1 --Estruc. Org. Nivel 0" & vbCrLf
    sConsulta = sConsulta & "           OR (@ACCESOEP=1 AND ISNULL(MEN.USUEPPARA,0)=1) --grupo corporativo EP  " & vbCrLf
    sConsulta = sConsulta & "           OR (@ACCESOGS=1 AND ISNULL(MEN.USUGSPARA,0)=1) --grupo corporativo GS" & vbCrLf
    sConsulta = sConsulta & "           OR (@ACCESOPM=1 AND ISNULL(MEN.USUPMPARA,0)=1) --grupo corporativo PM" & vbCrLf
    sConsulta = sConsulta & "           OR (@ACCESOQA=1 AND ISNULL(MEN.USUQAPARA,0)=1) --grupo corporativo QA" & vbCrLf
    sConsulta = sConsulta & "           OR (@ACCESOSM=1 AND ISNULL(MEN.USUSMPARA,0)=1) --grupo corporativo SM" & vbCrLf
    sConsulta = sConsulta & "           )" & vbCrLf
    sConsulta = sConsulta & "           AND (@TIPO IS NULL OR MEN.TIPO=@TIPO) AND (@IDCATEGORIA IS NULL OR MEN.CAT=@IDCATEGORIA)" & vbCrLf
    sConsulta = sConsulta & "       ),TOTMEGUSTARESPUESTA AS (SELECT DISTINCT RESP.ID,RESP.FECALTA,RESP.FECACT,RESP.MEN,RESP.RESP," & vbCrLf
    sConsulta = sConsulta & "       RESP.USU,PER.APE,PER.NOM,DEP.DEN SITUACIONUO,           " & vbCrLf
    sConsulta = sConsulta & "       USU2.COD USUCITADO,PER2.APE APECITADO,PER2.NOM NOMCITADO,DEP2.DEN SITUACIONUOCITADO," & vbCrLf
    sConsulta = sConsulta & "       RESP.PROVE,RESP.CON,CON.APE APEPROVE,CON.NOM NOMPROVE,CON.DEP SITUACIONUOPROVE,PROVE.DEN PROVEDEN," & vbCrLf
    sConsulta = sConsulta & "       RESP.PROVECITADO,RESP.CONCITADO,CON2.APE APEPROVECITADO,CON2.NOM NOMPROVECITADO," & vbCrLf
    sConsulta = sConsulta & "       CON2.DEP SITUACIONUOPROVECITADO,PROVE2.DEN PROVEDENCITADO," & vbCrLf
    sConsulta = sConsulta & "       ROW_NUMBER() OVER (ORDER BY IDMEGUSTARESPUESTA.FECACT DESC,IDMEGUSTARESPUESTA.ID ASC) AS ROWNUMBER" & vbCrLf
    sConsulta = sConsulta & "       FROM IDMEGUSTARESPUESTA WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "       INNER JOIN CN_' + @HIST + 'RESP RESP WITH(NOLOCK) ON RESP.MEN=IDMEGUSTARESPUESTA.ID AND RESP.MEGUSTA=1 AND RESP.RESP IS NOT NULL" & vbCrLf
    sConsulta = sConsulta & "       LEFT JOIN CON WITH(NOLOCK) ON CON.PROVE=RESP.PROVE AND CON.ID=RESP.CON" & vbCrLf
    sConsulta = sConsulta & "       LEFT JOIN PROVE WITH(NOLOCK) ON PROVE.COD=CON.PROVE" & vbCrLf
    sConsulta = sConsulta & "       LEFT JOIN USU WITH(NOLOCK) ON USU.COD=RESP.USU" & vbCrLf
    sConsulta = sConsulta & "       LEFT JOIN PER WITH(NOLOCK) ON PER.COD=USU.PER " & vbCrLf
    sConsulta = sConsulta & "       LEFT JOIN DEP WITH(NOLOCK) ON DEP.COD=PER.DEP" & vbCrLf
    sConsulta = sConsulta & "       LEFT JOIN CON CON2 WITH(NOLOCK) ON CON2.PROVE=RESP.PROVECITADO AND CON2.ID=RESP.CONCITADO" & vbCrLf
    sConsulta = sConsulta & "       LEFT JOIN PROVE PROVE2 WITH(NOLOCK) ON PROVE2.COD=CON2.PROVE        " & vbCrLf
    sConsulta = sConsulta & "       LEFT JOIN USU USU2 WITH(NOLOCK) ON USU2.COD=RESP.USUCITADO" & vbCrLf
    sConsulta = sConsulta & "       LEFT JOIN PER PER2 WITH(NOLOCK) ON PER2.COD=USU2.PER" & vbCrLf
    sConsulta = sConsulta & "       LEFT JOIN DEP DEP2 WITH(NOLOCK) ON DEP2.COD=PER2.DEP)" & vbCrLf
    sConsulta = sConsulta & "       SELECT DISTINCT ID,FECALTA,FECACT,MEN,RESP,USU,APE,NOM,SITUACIONUO,         " & vbCrLf
    sConsulta = sConsulta & "       USUCITADO,APECITADO,NOMCITADO,SITUACIONUOCITADO,PROVE,CON,APEPROVE,NOMPROVE,SITUACIONUOPROVE,PROVEDEN," & vbCrLf
    sConsulta = sConsulta & "       PROVECITADO,CONCITADO,APEPROVECITADO,NOMPROVECITADO,SITUACIONUOPROVECITADO,PROVEDENCITADO" & vbCrLf
    sConsulta = sConsulta & "       FROM TOTMEGUSTARESPUESTA WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "       ORDER BY FECACT ASC'" & vbCrLf
    sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQLMEGUSTARESPUESTA, N'@UON1 NVARCHAR(20),@UON2 NVARCHAR(20),@UON3 NVARCHAR(20),@DEP NVARCHAR(20)," & vbCrLf
    sConsulta = sConsulta & "       @USU NVARCHAR(20),@IDI NVARCHAR(20),@TIPO INT,@REGINI INT,@REGFIN INT,@IDCATEGORIA INT,@IDGRUPO INT," & vbCrLf
    sConsulta = sConsulta & "       @FACTURA INT,@LINEA INT,@ACCESOEP BIT=NULL,@ACCESOGS BIT=NULL,@ACCESOPM BIT=NULL,@ACCESOQA BIT=NULL,@ACCESOSM BIT=NULL', " & vbCrLf
    sConsulta = sConsulta & "       @UON1=@UON1,@UON2=@UON2,@UON3=@UON3,@DEP=@DEP," & vbCrLf
    sConsulta = sConsulta & "       @USU=@USU,@IDI=@IDI,@TIPO=@TIPO,@REGINI=@REGINI,@REGFIN=@REGFIN,@IDCATEGORIA=@IDCATEGORIA,@IDGRUPO=@IDGRUPO," & vbCrLf
    sConsulta = sConsulta & "       @FACTURA=@FACTURA,@LINEA=@LINEA, @ACCESOEP=@ACCESOEP,@ACCESOGS=@ACCESOGS,@ACCESOPM=@ACCESOPM,@ACCESOQA=@ACCESOQA,@ACCESOSM=@ACCESOSM" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   DECLARE @SQLADJUNTOSMENSAJE NVARCHAR(MAX)   " & vbCrLf
    sConsulta = sConsulta & "   SET @SQLADJUNTOSMENSAJE =N'WITH IDADJUNTOS AS (SELECT MEN.ID, CN_OPT_MEN_USU.OCULTO,MEN.FECALTA,MEN.CAT," & vbCrLf
    sConsulta = sConsulta & "       MEN.FECACT,MEN.USU,MEN.TIPO,MEN.TITULO,MEN.CONTENIDO,MEN.DONDE,MEN.CUANDO" & vbCrLf
    sConsulta = sConsulta & "       FROM CN_' + @HIST + 'MEN MEN WITH(NOLOCK) " & vbCrLf
    sConsulta = sConsulta & "       INNER JOIN CN_' + @HIST + 'OPT_MEN_USU WITH(NOLOCK) " & vbCrLf
    sConsulta = sConsulta & "       ON CN_' + @HIST + 'OPT_MEN_USU.MEN=MEN.ID AND CN_' + @HIST + 'OPT_MEN_USU.USU=@USU" & vbCrLf
    sConsulta = sConsulta & "       AND (@TIPO IS NULL OR MEN.TIPO=@TIPO) AND (@IDCATEGORIA IS NULL OR MEN.CAT=@IDCATEGORIA)" & vbCrLf
    sConsulta = sConsulta & "       INNER JOIN CN_CAT WITH(NOLOCK) ON CN_CAT.ID=MEN.CAT AND CN_CAT.DESPUBENRED=0 ' " & vbCrLf
    sConsulta = sConsulta & "   IF @IDGRUPO IS NOT NULL" & vbCrLf
    sConsulta = sConsulta & "       SET @SQLADJUNTOSMENSAJE=@SQLADJUNTOSMENSAJE + N'INNER JOIN CN_' + @HIST + 'MEN_GRUPO GRUPO WITH(NOLOCK) ON GRUPO.MEN=MEN.ID AND GRUPO.GRUPO=@IDGRUPO '" & vbCrLf
    sConsulta = sConsulta & "   IF @MEGUSTA IS NOT NULL" & vbCrLf
    sConsulta = sConsulta & "       SET @SQLADJUNTOSMENSAJE=@SQLADJUNTOSMENSAJE + N'INNER JOIN CN_' + @HIST + 'RESP RESP WITH(NOLOCK) ON RESP.MEN=MEN.ID AND RESP.USU=@USU AND RESP.MEGUSTA=1 '" & vbCrLf
    sConsulta = sConsulta & "   IF @DISCREPANCIAS=1" & vbCrLf
    sConsulta = sConsulta & "       SET @SQLADJUNTOSMENSAJE=@SQLADJUNTOSMENSAJE + N'INNER JOIN LIN_FACTURA_DISCRP DISCRP WITH(NOLOCK) ON DISCRP.MEN=MEN.ID " & vbCrLf
    sConsulta = sConsulta & "           AND (@FACTURA IS NULL OR DISCRP.FACTURA=@FACTURA) AND (@LINEA IS NULL OR DISCRP.LINEA=@LINEA) '" & vbCrLf
    sConsulta = sConsulta & "   SET @SQLADJUNTOSMENSAJE=@SQLADJUNTOSMENSAJE + N'UNION   " & vbCrLf
    sConsulta = sConsulta & "       SELECT MEN.ID, ISNULL(CN_' + @HIST + 'OPT_MEN_USU.OCULTO,0) OCULTO,MEN.FECALTA,MEN.CAT," & vbCrLf
    sConsulta = sConsulta & "       MEN.FECACT,MEN.USU,MEN.TIPO,MEN.TITULO,MEN.CONTENIDO,MEN.DONDE,MEN.CUANDO" & vbCrLf
    sConsulta = sConsulta & "       FROM CN_' + @HIST + 'MEN MEN WITH(NOLOCK) " & vbCrLf
    sConsulta = sConsulta & "       INNER JOIN CN_CAT WITH(NOLOCK) ON CN_CAT.ID=MEN.CAT AND CN_CAT.DESPUBENRED=0" & vbCrLf
    sConsulta = sConsulta & "       LEFT JOIN CN_' + @HIST + 'OPT_MEN_USU WITH(NOLOCK) ON CN_' + @HIST + 'OPT_MEN_USU.MEN=MEN.ID AND CN_' + @HIST + 'OPT_MEN_USU.USU=@USU '" & vbCrLf
    sConsulta = sConsulta & "   IF @IDGRUPO IS NOT NULL" & vbCrLf
    sConsulta = sConsulta & "       SET @SQLADJUNTOSMENSAJE=@SQLADJUNTOSMENSAJE + N'INNER JOIN CN_' + @HIST + 'MEN_GRUPO GRUPO WITH(NOLOCK) ON GRUPO.MEN=MEN.ID AND GRUPO.GRUPO=@IDGRUPO '" & vbCrLf
    sConsulta = sConsulta & "   IF @MEGUSTA IS NOT NULL" & vbCrLf
    sConsulta = sConsulta & "       SET @SQLADJUNTOSMENSAJE=@SQLADJUNTOSMENSAJE + N'INNER JOIN CN_' + @HIST + 'RESP RESP WITH(NOLOCK) ON RESP.MEN=MEN.ID AND RESP.USU=@USU AND RESP.MEGUSTA=1 '" & vbCrLf
    sConsulta = sConsulta & "   IF @DISCREPANCIAS=1" & vbCrLf
    sConsulta = sConsulta & "       SET @SQLADJUNTOSMENSAJE=@SQLADJUNTOSMENSAJE + N'INNER JOIN LIN_FACTURA_DISCRP DISCRP WITH(NOLOCK) ON DISCRP.MEN=MEN.ID " & vbCrLf
    sConsulta = sConsulta & "           AND (@FACTURA IS NULL OR DISCRP.FACTURA=@FACTURA) AND (@LINEA IS NULL OR DISCRP.LINEA=@LINEA) '" & vbCrLf
    sConsulta = sConsulta & "   SET @SQLADJUNTOSMENSAJE=@SQLADJUNTOSMENSAJE + N'WHERE ( MEN.UON0=1 --Estruc. Org. Nivel 0" & vbCrLf
    sConsulta = sConsulta & "           OR (@ACCESOEP=1 AND ISNULL(MEN.USUEPPARA,0)=1) --grupo corporativo EP  " & vbCrLf
    sConsulta = sConsulta & "           OR (@ACCESOGS=1 AND ISNULL(MEN.USUGSPARA,0)=1) --grupo corporativo GS" & vbCrLf
    sConsulta = sConsulta & "           OR (@ACCESOPM=1 AND ISNULL(MEN.USUPMPARA,0)=1) --grupo corporativo PM" & vbCrLf
    sConsulta = sConsulta & "           OR (@ACCESOQA=1 AND ISNULL(MEN.USUQAPARA,0)=1) --grupo corporativo QA" & vbCrLf
    sConsulta = sConsulta & "           OR (@ACCESOSM=1 AND ISNULL(MEN.USUSMPARA,0)=1) --grupo corporativo SM" & vbCrLf
    sConsulta = sConsulta & "           )" & vbCrLf
    sConsulta = sConsulta & "           AND (@TIPO IS NULL OR MEN.TIPO=@TIPO) AND (@IDCATEGORIA IS NULL OR MEN.CAT=@IDCATEGORIA)" & vbCrLf
    sConsulta = sConsulta & "       ),TOTADJUNTOS AS (SELECT DISTINCT ADJUN.ID,ADJUN.NOMBRE,ADJUN.DGUID," & vbCrLf
    sConsulta = sConsulta & "       ADJUN.SIZE,ADJUN.SIZEUNIT,ADJUN.TIPOADJUNTO,ADJUN.FECALTA,ADJUN.MEN,ADJUN.RESP," & vbCrLf
    sConsulta = sConsulta & "       ROW_NUMBER() OVER (ORDER BY IDADJUNTOS.FECACT DESC,IDADJUNTOS.ID ASC) AS ROWNUMBER " & vbCrLf
    sConsulta = sConsulta & "       FROM IDADJUNTOS WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "       INNER JOIN CN_' + @HIST + 'ADJUN ADJUN WITH(NOLOCK) ON ADJUN.MEN=IDADJUNTOS.ID AND NOT ADJUN.TIPOADJUNTO=0)" & vbCrLf
    sConsulta = sConsulta & "       SELECT DISTINCT ID,NOMBRE,DGUID,SIZE,SIZEUNIT,TIPOADJUNTO,FECALTA,MEN,RESP" & vbCrLf
    sConsulta = sConsulta & "       FROM TOTADJUNTOS WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "       ORDER BY FECALTA ASC'" & vbCrLf
    sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQLADJUNTOSMENSAJE, N'@UON1 NVARCHAR(20),@UON2 NVARCHAR(20),@UON3 NVARCHAR(20),@DEP NVARCHAR(20)," & vbCrLf
    sConsulta = sConsulta & "       @USU NVARCHAR(20),@IDI NVARCHAR(20),@TIPO INT,@REGINI INT,@REGFIN INT,@IDCATEGORIA INT,@IDGRUPO INT," & vbCrLf
    sConsulta = sConsulta & "       @FACTURA INT,@LINEA INT,@ACCESOEP BIT=NULL,@ACCESOGS BIT=NULL,@ACCESOPM BIT=NULL,@ACCESOQA BIT=NULL,@ACCESOSM BIT=NULL', " & vbCrLf
    sConsulta = sConsulta & "       @UON1=@UON1,@UON2=@UON2,@UON3=@UON3,@DEP=@DEP," & vbCrLf
    sConsulta = sConsulta & "       @USU=@USU,@IDI=@IDI,@TIPO=@TIPO,@REGINI=@REGINI,@REGFIN=@REGFIN,@IDCATEGORIA=@IDCATEGORIA,@IDGRUPO=@IDGRUPO," & vbCrLf
    sConsulta = sConsulta & "       @FACTURA=@FACTURA,@LINEA=@LINEA, @ACCESOEP=@ACCESOEP,@ACCESOGS=@ACCESOGS,@ACCESOPM=@ACCESOPM,@ACCESOQA=@ACCESOQA,@ACCESOSM=@ACCESOSM" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   DELETE FROM CN_USU_NOTIF WHERE USU=@USU" & vbCrLf
    sConsulta = sConsulta & "END" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSINT_COMPROBAR_INTEGRACION_ARTICULO_ERP]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSINT_COMPROBAR_INTEGRACION_ARTICULO_ERP]" & vbCrLf
    gRDOCon.QueryTimeout = 7200
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "CREATE PROCEDURE [dbo].[FSINT_COMPROBAR_INTEGRACION_ARTICULO_ERP] @ARTICULO AS VARCHAR(50),@ERP AS INTEGER AS" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SENTIDO tinyint" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SELECT @SENTIDO=SENTIDO from TABLAS_INTEGRACION_ERP where ERP = @ERP and TABLA=7" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "IF @SENTIDO=1--SENTIDO SALIDA" & vbCrLf
    sConsulta = sConsulta & "   SELECT TOP 1 ESTADO " & vbCrLf
    sConsulta = sConsulta & "   FROM LOG_GRAL WITH(NOLOCK) " & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN LOG_ART4 L4 WITH(NOLOCK) ON LOG_GRAL.ID_TABLA=L4.ID " & vbCrLf
    sConsulta = sConsulta & "   WHERE TABLA=7 AND L4.COD =@ARTICULO AND L4.ORIGEN IN (0,2) AND L4.ERP=@ERP " & vbCrLf
    sConsulta = sConsulta & "   ORDER BY LOG_GRAL.ID DESC" & vbCrLf
    sConsulta = sConsulta & "ELSE" & vbCrLf
    sConsulta = sConsulta & "   IF @SENTIDO=3--SENTIDO ENTRADA/SALIDA" & vbCrLf
    sConsulta = sConsulta & "       SELECT TOP 1 ESTADO " & vbCrLf
    sConsulta = sConsulta & "       FROM LOG_GRAL WITH(NOLOCK) " & vbCrLf
    sConsulta = sConsulta & "       INNER JOIN LOG_ART4 L4 WITH(NOLOCK) ON LOG_GRAL.ID_TABLA=L4.ID " & vbCrLf
    sConsulta = sConsulta & "       WHERE TABLA=7 AND L4.COD =@ARTICULO AND L4.ORIGEN IN (0,2,3) AND L4.ERP=@ERP " & vbCrLf
    sConsulta = sConsulta & "       ORDER BY LOG_GRAL.ID DESC" & vbCrLf
    sConsulta = sConsulta & "   ELSE --SENTIDO ENTRADA" & vbCrLf
    sConsulta = sConsulta & "       SELECT 4 ESTADO" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_ROL_PARTICIPANTES]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_ROL_PARTICIPANTES]" & vbCrLf
    gRDOCon.QueryTimeout = 7200
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "CREATE PROCEDURE [dbo].[FSPM_ROL_PARTICIPANTES] @BLOQUE INT, @ROL INT, @INSTANCIA INT,@IDI VARCHAR(50), @ID_FAVORITA INT=0 AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "IF @INSTANCIA<>0" & vbCrLf
    sConsulta = sConsulta & "    BEGIN" & vbCrLf
    sConsulta = sConsulta & "    SET CONCAT_NULL_YIELDS_NULL OFF" & vbCrLf
    sConsulta = sConsulta & "       SELECT R.DEN RDEN,R.ID ROL,R.TIPO,R.RESTRINGIR, CASE WHEN R.PER IS NULL THEN R.PROVE ELSE R.PER END PART," & vbCrLf
    sConsulta = sConsulta & "   CASE WHEN R.PER IS NOT NULL THEN " & vbCrLf
    sConsulta = sConsulta & "       PER.NOM + ' ' + PER.APE+ ' (' + PER.EMAIL + ')' " & vbCrLf
    sConsulta = sConsulta & "   ELSE " & vbCrLf
    sConsulta = sConsulta & "       (CASE WHEN R.PROVE IS NULL THEN " & vbCrLf
    sConsulta = sConsulta & "           NULL " & vbCrLf
    sConsulta = sConsulta & "       ELSE " & vbCrLf
    sConsulta = sConsulta & "           PROVE.DEN" & vbCrLf
    sConsulta = sConsulta & "       END) " & vbCrLf
    sConsulta = sConsulta & "   END PARTNOM, NULL AS ID_CONTACTO" & vbCrLf
    sConsulta = sConsulta & "       FROM PM_COPIA_ROL R WITH (NOLOCK) " & vbCrLf
    sConsulta = sConsulta & "   LEFT JOIN PER WITH (NOLOCK)  ON PER.COD=R.PER " & vbCrLf
    sConsulta = sConsulta & "   LEFT JOIN PROVE WITH (NOLOCK)  ON PROVE.COD=R.PROVE " & vbCrLf
    sConsulta = sConsulta & "   WHERE R.INSTANCIA=@INSTANCIA AND R.BLOQUE=@BLOQUE AND R.ROL=@ROL AND R.COMO_ASIGNAR=2" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   SELECT RB.ROL,RB.BLOQUE,B.DEN BDEN" & vbCrLf
    sConsulta = sConsulta & "   FROM PM_COPIA_ROL R WITH (NOLOCK)  " & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL_BLOQUE RB WITH (NOLOCK)  ON RB.ROL= R.ID" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_BLOQUE_DEN B WITH (NOLOCK)  ON  B.BLOQUE=RB.BLOQUE " & vbCrLf
    sConsulta = sConsulta & "   WHERE R.INSTANCIA=@INSTANCIA AND R.BLOQUE=@BLOQUE AND R.ROL=@ROL AND R.COMO_ASIGNAR=2 AND B.IDI=@IDI" & vbCrLf
    sConsulta = sConsulta & "    SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
    sConsulta = sConsulta & "    END" & vbCrLf
    sConsulta = sConsulta & "ELSE" & vbCrLf
    sConsulta = sConsulta & "    BEGIN" & vbCrLf
    sConsulta = sConsulta & "    DECLARE @SQL NVARCHAR(4000)" & vbCrLf
    sConsulta = sConsulta & "    DECLARE @CNT INTEGER" & vbCrLf
    sConsulta = sConsulta & "    " & vbCrLf
    sConsulta = sConsulta & "    SELECT @CNT=COUNT(*) FROM PM_PARTICIPANTES_FAV WITH(NOLOCK) WHERE SOLICIT_FAV=@ID_FAVORITA" & vbCrLf
    sConsulta = sConsulta & "    IF  @CNT = 0" & vbCrLf
    sConsulta = sConsulta & "       SET @ID_FAVORITA = 0" & vbCrLf
    sConsulta = sConsulta & "       " & vbCrLf
    sConsulta = sConsulta & "    SET @SQL = N'SELECT DISTINCT R.DEN RDEN, R.ID ROL, R.TIPO, R.RESTRINGIR, '" & vbCrLf
    sConsulta = sConsulta & "    IF @ID_FAVORITA <> 0" & vbCrLf
    sConsulta = sConsulta & "       SET @SQL = @SQL + 'CASE WHEN PF.PER IS NULL THEN PF.PROVE " & vbCrLf
    sConsulta = sConsulta & "                       ELSE  PF.PER END PART," & vbCrLf
    sConsulta = sConsulta & "                           CASE WHEN PF.PER IS NOT NULL THEN PER.NOM + '' '' + PER.APE+ '' ('' + PER.EMAIL + '')''" & vbCrLf
    sConsulta = sConsulta & "                           ELSE " & vbCrLf
    sConsulta = sConsulta & "                               (CASE WHEN PF.PROVE IS NULL THEN " & vbCrLf
    sConsulta = sConsulta & "                                   NULL " & vbCrLf
    sConsulta = sConsulta & "                               ELSE " & vbCrLf
    sConsulta = sConsulta & "                                   PROVE.DEN + '' ('' + LEFT(b.EmailsConcatenados , LEN(b.EmailsConcatenados )-1) + '')'' " & vbCrLf
    sConsulta = sConsulta & "                               END) " & vbCrLf
    sConsulta = sConsulta & "                       END PARTNOM, a.ContactosConcatenados ID_CONTACTO'" & vbCrLf
    sConsulta = sConsulta & "   ELSE" & vbCrLf
    sConsulta = sConsulta & "       SET @SQL = @SQL + 'CASE WHEN R.PER IS NULL THEN R.PROVE " & vbCrLf
    sConsulta = sConsulta & "                       ELSE R.PER END PART," & vbCrLf
    sConsulta = sConsulta & "                           CASE WHEN R.PER IS NOT NULL THEN PER.NOM + '' '' + PER.APE+ '' ('' + PER.EMAIL + '')''" & vbCrLf
    sConsulta = sConsulta & "                           ELSE " & vbCrLf
    sConsulta = sConsulta & "                               (CASE WHEN R.PROVE IS NULL THEN " & vbCrLf
    sConsulta = sConsulta & "                                   NULL " & vbCrLf
    sConsulta = sConsulta & "                               ELSE " & vbCrLf
    sConsulta = sConsulta & "                                   PROVE.DEN + '' ('' + CON.EMAIL + '')'' " & vbCrLf
    sConsulta = sConsulta & "                               END) " & vbCrLf
    sConsulta = sConsulta & "                       END PARTNOM, CON.ID ID_CONTACTO '" & vbCrLf
    sConsulta = sConsulta & "    " & vbCrLf
    sConsulta = sConsulta & "   SET @SQL = @SQL + ' FROM PM_ROL R WITH (NOLOCK)'    " & vbCrLf
    sConsulta = sConsulta & "    " & vbCrLf
    sConsulta = sConsulta & "   IF @ID_FAVORITA <> 0" & vbCrLf
    sConsulta = sConsulta & "       SET @SQL = @SQL + ' LEFT JOIN PM_PARTICIPANTES_FAV PF WITH (NOLOCK) ON R.ID=PF.ROL " & vbCrLf
    sConsulta = sConsulta & "                           CROSS APPLY" & vbCrLf
    sConsulta = sConsulta & "                           (" & vbCrLf
    sConsulta = sConsulta & "                           SELECT CON + ''|''" & vbCrLf
    sConsulta = sConsulta & "                           FROM PM_PARTICIPANTES_CON_FAV pcf WITH (NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "                           WHERE pf.ID = pcf.PARTICIPANTES_FAV " & vbCrLf
    sConsulta = sConsulta & "                           FOR XML PATH('''')" & vbCrLf
    sConsulta = sConsulta & "                           ) a (ContactosConcatenados)" & vbCrLf
    sConsulta = sConsulta & "                           " & vbCrLf
    sConsulta = sConsulta & "                           CROSS APPLY" & vbCrLf
    sConsulta = sConsulta & "                           (" & vbCrLf
    sConsulta = sConsulta & "                           SELECT CON.EMAIL + '',''" & vbCrLf
    sConsulta = sConsulta & "                           FROM PM_PARTICIPANTES_CON_FAV PCF WITH (NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "                           LEFT JOIN CON WITH(NOLOCK) ON CON.ID = PCF.CON AND CON.PROVE = PF.PROVE" & vbCrLf
    sConsulta = sConsulta & "                           WHERE PF.ID = PCF.PARTICIPANTES_FAV " & vbCrLf
    sConsulta = sConsulta & "                           FOR XML PATH('''')" & vbCrLf
    sConsulta = sConsulta & "                           ) b (EmailsConcatenados)  " & vbCrLf
    sConsulta = sConsulta & "                           " & vbCrLf
    sConsulta = sConsulta & "                           LEFT JOIN PER WITH (NOLOCK)  ON PER.COD = PF.PER" & vbCrLf
    sConsulta = sConsulta & "                           LEFT JOIN PROVE WITH (NOLOCK)  ON PROVE.COD=PF.PROVE '" & vbCrLf
    sConsulta = sConsulta & "   ELSE" & vbCrLf
    sConsulta = sConsulta & "       SET @SQL = @SQL + ' LEFT JOIN PER WITH (NOLOCK)  ON PER.COD = R.PER" & vbCrLf
    sConsulta = sConsulta & "       LEFT JOIN PROVE WITH (NOLOCK)  ON PROVE.COD=R.PROVE" & vbCrLf
    sConsulta = sConsulta & "       LEFT JOIN CON WITH (NOLOCK)  ON PROVE.COD=CON.PROVE AND CON.ID=R.CON'" & vbCrLf
    sConsulta = sConsulta & "       " & vbCrLf
    sConsulta = sConsulta & "   SET @SQL = @SQL + ' WHERE R.BLOQUE_ASIGNA=@BLOQUE AND R.ROL_ASIGNA=@ROL AND R.COMO_ASIGNAR=2' " & vbCrLf
    sConsulta = sConsulta & "       " & vbCrLf
    sConsulta = sConsulta & "   IF @ID_FAVORITA <> 0" & vbCrLf
    sConsulta = sConsulta & "       SET @SQL = @SQL + 'AND SOLICIT_FAV=@ID_FAVORITA" & vbCrLf
    sConsulta = sConsulta & "       GROUP BY R.BLOQUE_ASIGNA, R.DEN, R.ID, R.TIPO, R.RESTRINGIR, PF.PER, PER.NOM, PER.APE, PER.EMAIL, PF.PROVE, PROVE.DEN, a.ContactosConcatenados, b.EmailsConcatenados'" & vbCrLf
    sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL,N'@BLOQUE INT, @ROL INT, @ID_FAVORITA INT',@BLOQUE=@BLOQUE, @ROL=@ROL, @ID_FAVORITA=@ID_FAVORITA" & vbCrLf
    sConsulta = sConsulta & "    " & vbCrLf
    sConsulta = sConsulta & "    SELECT RB.ROL,RB.BLOQUE,B.DEN BDEN " & vbCrLf
    sConsulta = sConsulta & "    FROM PM_ROL R  WITH (NOLOCK) " & vbCrLf
    sConsulta = sConsulta & "    INNER JOIN PM_ROL_BLOQUE RB WITH (NOLOCK)  ON RB.ROL= R.ID" & vbCrLf
    sConsulta = sConsulta & "    INNER JOIN PM_BLOQUE_DEN B WITH (NOLOCK)  ON  B.BLOQUE=RB.BLOQUE " & vbCrLf
    sConsulta = sConsulta & "    WHERE R.BLOQUE_ASIGNA=@BLOQUE AND R.ROL_ASIGNA=@ROL AND R.COMO_ASIGNAR=2 AND B.IDI=@IDI" & vbCrLf
    sConsulta = sConsulta & "    END" & vbCrLf
    sConsulta = sConsulta & "    SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_ACTUALIZAR_CONTACTO]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_ACTUALIZAR_CONTACTO]" & vbCrLf
    gRDOCon.QueryTimeout = 7200
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "CREATE PROCEDURE  [dbo].[SP_ACTUALIZAR_CONTACTO] (@USU_APE VARCHAR(100),@USU_NOM VARCHAR(20), @USU_DEP VARCHAR(100)," & vbCrLf
    sConsulta = sConsulta & "                             @USU_CAR VARCHAR(100), @USU_TFNO VARCHAR(20), @USU_FAX VARCHAR(20)," & vbCrLf
    sConsulta = sConsulta & "                             @USU_EMAIL VARCHAR(100), @USU_TFNO2 VARCHAR(20)," & vbCrLf
    sConsulta = sConsulta & "                             @USU_TFNO_MOVIL VARCHAR(20),@COD_PROVE_CIA VARCHAR(200),@ID_P INT,@IDI VARCHAR(100),@TIPOEMAIL SMALLINT," & vbCrLf
    sConsulta = sConsulta & "                             @THOUSANFMT CHAR(1),@DECIMALFMT CHAR(1),@PRECISIONFMT TINYINT, @DATEFMT VARCHAR(50), @USU_NIF NVARCHAR(30) ) AS" & vbCrLf
    sConsulta = sConsulta & "DECLARE @INDICE INT" & vbCrLf
    sConsulta = sConsulta & "DECLARE @ACTIVA BIT" & vbCrLf
    sConsulta = sConsulta & "DECLARE @INT TINYINT" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SENTIDO TINYINT" & vbCrLf
    sConsulta = sConsulta & "DECLARE @ID_LOG_CON INT" & vbCrLf
    sConsulta = sConsulta & "DECLARE @ERP VARCHAR(50)" & vbCrLf
    sConsulta = sConsulta & "/*****************************************************************************************************************************************************************/" & vbCrLf
    sConsulta = sConsulta & "/******       INTEGRACION: Est� activada, si lo est� a nivel gral  y en concreto la entidad PROVE en sentido salida o entrada/salida   *****/" & vbCrLf
    sConsulta = sConsulta & "/****************************************************************************************************************************************************************/" & vbCrLf
    sConsulta = sConsulta & "SET @INT = (SELECT INTEGRACION FROM PARGEN_INTERNO WITH(NOLOCK) WHERE ID=1)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "/*********************************************************************************************************************************************************************/" & vbCrLf
    sConsulta = sConsulta & "/******       INTEGRACION :  Si INT=1 Grabamos por cada contacto nuevo una fila en LOG_CON y su correspondiente LOG_PROVE    ******/" & vbCrLf
    sConsulta = sConsulta & "/******                                    recuperamos el �ltimo ID de LOG_CON para insertarlo en LOG_PROVE e indicar as� que est� ligados       ******/" & vbCrLf
    sConsulta = sConsulta & "/********************************************************************************************************************************************************************/" & vbCrLf
    sConsulta = sConsulta & "IF (SELECT COUNT(*) FROM CON WITH(NOLOCK) WHERE PROVE=@COD_PROVE_CIA AND ID_PORT=@ID_P)=0" & vbCrLf
    sConsulta = sConsulta & "BEGIN" & vbCrLf
    sConsulta = sConsulta & "   --Calidad: sin  WITH(NOLOCK) pq es clave insert" & vbCrLf
    sConsulta = sConsulta & "    SET @INDICE = (SELECT MAX(ID) FROM CON WHERE PROVE = @COD_PROVE_CIA)" & vbCrLf
    sConsulta = sConsulta & "    IF @INDICE IS NULL" & vbCrLf
    sConsulta = sConsulta & "      BEGIN" & vbCrLf
    sConsulta = sConsulta & "       SET @INDICE = 1" & vbCrLf
    sConsulta = sConsulta & "      End" & vbCrLf
    sConsulta = sConsulta & "    Else" & vbCrLf
    sConsulta = sConsulta & "      BEGIN" & vbCrLf
    sConsulta = sConsulta & "       SET @INDICE = @INDICE + 1" & vbCrLf
    sConsulta = sConsulta & "      End" & vbCrLf
    sConsulta = sConsulta & "    INSERT INTO CON (PROVE,ID,APE,NOM,DEP,CAR,TFNO,FAX,EMAIL,RECPET,TFNO2,PORT,TFNO_MOVIL,ID_PORT,IDI,TIPOEMAIL,THOUSANFMT,DECIMALFMT,PRECISIONFMT, DATEFMT,NIF)" & vbCrLf
    sConsulta = sConsulta & "             VALUES(@COD_PROVE_CIA,@INDICE,@USU_APE,@USU_NOM,@USU_DEP,@USU_CAR,@USU_TFNO," & vbCrLf
    sConsulta = sConsulta & "                      @USU_FAX,@USU_EMAIL,1,@USU_TFNO2,1,@USU_TFNO_MOVIL,@ID_P,@IDI,@TIPOEMAIL," & vbCrLf
    sConsulta = sConsulta & "                      @THOUSANFMT,@DECIMALFMT,@PRECISIONFMT, @DATEFMT,@USU_NIF)" & vbCrLf
    sConsulta = sConsulta & "/******* Grabar en LOG ***/" & vbCrLf
    sConsulta = sConsulta & "   IF @INT = 1" & vbCrLf
    sConsulta = sConsulta & "/* MPB - MultiERP */" & vbCrLf
    sConsulta = sConsulta & "    BEGIN" & vbCrLf
    sConsulta = sConsulta & "   DECLARE Cur_ERP Cursor FOR SELECT COD FROM ERP WITH(NOLOCK) " & vbCrLf
    sConsulta = sConsulta & "   OPEN Cur_ERP" & vbCrLf
    sConsulta = sConsulta & "   FETCH NEXT FROM Cur_ERP into @ERP" & vbCrLf
    sConsulta = sConsulta & "   WHILE @@FETCH_STATUS=0" & vbCrLf
    sConsulta = sConsulta & "   BEGIN" & vbCrLf
    sConsulta = sConsulta & "       SELECT @ACTIVA=ACTIVA,@SENTIDO=SENTIDO FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=8 AND ERP=@ERP " & vbCrLf
    sConsulta = sConsulta & "       IF @ACTIVA = 1 AND (@SENTIDO = 1 OR @SENTIDO=3)" & vbCrLf
    sConsulta = sConsulta & "       begin" & vbCrLf
    sConsulta = sConsulta & "           INSERT INTO LOG_CON (ACCION,PROVE,ID_CON,APE,NOM,DEP,CAR,TFNO,FAX,EMAIL,RECPET,TFNO2,TFNO_MOVIL,ORIGEN,USU,ERP,NIF)" & vbCrLf
    sConsulta = sConsulta & "                    VALUES('I',@COD_PROVE_CIA,@INDICE,@USU_APE,@USU_NOM,@USU_DEP,@USU_CAR,@USU_TFNO," & vbCrLf
    sConsulta = sConsulta & "                             @USU_FAX,@USU_EMAIL,1,@USU_TFNO2,@USU_TFNO_MOVIL,0,'',@ERP,@USU_NIF)" & vbCrLf
    sConsulta = sConsulta & "           --CALIDAD: sin  WITH(NOLOCK) pq es clave insert" & vbCrLf
    sConsulta = sConsulta & "           SET @ID_LOG_CON = (SELECT MAX(ID) FROM LOG_CON)" & vbCrLf
    sConsulta = sConsulta & "          --CALIDAD: sin  WITH(NOLOCK) pq es lectura limpia" & vbCrLf
    sConsulta = sConsulta & "           INSERT INTO LOG_PROVE (ACCION,ID_LOG_CON,COD,DEN,DIR,CP,POB,PAI,PROVI,MON,NIF,URLPROVE,ORIGEN,USU,ERP) SELECT 'R',@ID_LOG_CON,COD,DEN,DIR,CP,POB,PAI,PROVI,MON,NIF,URLPROVE,0,'',@ERP  FROM PROVE WHERE COD =@COD_PROVE_CIA" & vbCrLf
    sConsulta = sConsulta & "       end" & vbCrLf
    sConsulta = sConsulta & "           FETCH NEXT FROM Cur_ERP INTO @ERP" & vbCrLf
    sConsulta = sConsulta & "   END" & vbCrLf
    sConsulta = sConsulta & "   Close Cur_ERP" & vbCrLf
    sConsulta = sConsulta & "   DEALLOCATE Cur_ERP" & vbCrLf
    sConsulta = sConsulta & "   END" & vbCrLf
    sConsulta = sConsulta & "/****************************/" & vbCrLf
    sConsulta = sConsulta & "End" & vbCrLf
    sConsulta = sConsulta & "Else" & vbCrLf
    sConsulta = sConsulta & "UPDATE CON SET APE=@USU_APE, NOM=@USU_NOM, DEP=@USU_DEP, CAR=@USU_CAR, TFNO=@USU_TFNO, FAX=@USU_FAX, EMAIL=@USU_EMAIL,TFNO2=@USU_TFNO2," & vbCrLf
    sConsulta = sConsulta & "                   TFNO_MOVIL=@USU_TFNO_MOVIL,IDI=@IDI,TIPOEMAIL=@TIPOEMAIL," & vbCrLf
    sConsulta = sConsulta & "                   THOUSANFMT= @THOUSANFMT,DECIMALFMT=@DECIMALFMT,PRECISIONFMT=@PRECISIONFMT,DATEFMT =@DATEFMT, NIF=@USU_NIF" & vbCrLf
    sConsulta = sConsulta & "    WHERE PROVE=@COD_PROVE_CIA AND ID_PORT=@ID_P" & vbCrLf
    sConsulta = sConsulta & "IF @INT = 1" & vbCrLf
    sConsulta = sConsulta & " BEGIN   " & vbCrLf
    sConsulta = sConsulta & "   DECLARE Cur_ERP Cursor FOR SELECT COD FROM ERP  WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   OPEN Cur_ERP" & vbCrLf
    sConsulta = sConsulta & "   FETCH NEXT FROM Cur_ERP into @ERP" & vbCrLf
    sConsulta = sConsulta & "   WHILE @@FETCH_STATUS=0" & vbCrLf
    sConsulta = sConsulta & "   BEGIN" & vbCrLf
    sConsulta = sConsulta & "       SELECT @ACTIVA=ACTIVA,@SENTIDO=SENTIDO FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=8 AND ERP=@ERP " & vbCrLf
    sConsulta = sConsulta & "       IF @ACTIVA = 1 AND (@SENTIDO = 1 OR @SENTIDO=3)" & vbCrLf
    sConsulta = sConsulta & "       begin" & vbCrLf
    sConsulta = sConsulta & "           INSERT INTO LOG_CON (ACCION,PROVE,ID_CON,APE,NOM,DEP,CAR,TFNO,FAX,EMAIL,TFNO2,TFNO_MOVIL,ORIGEN,USU,ERP,NIF)" & vbCrLf
    sConsulta = sConsulta & "           SELECT 'U',PROVE,ID,APE,NOM,DEP,CAR,TFNO,FAX,EMAIL,TFNO2,TFNO_MOVIL,0,'',@ERP,NIF FROM CON WITH(NOLOCK) WHERE PROVE = @COD_PROVE_CIA AND ID_PORT=@ID_P" & vbCrLf
    sConsulta = sConsulta & "           --CALIDAD: sin  WITH(NOLOCK) pq es clave insert" & vbCrLf
    sConsulta = sConsulta & "          SET @ID_LOG_CON = (SELECT MAX(ID) FROM LOG_CON)" & vbCrLf
    sConsulta = sConsulta & "          --CALIDAD: sin  WITH(NOLOCK) pq es lectura limpia" & vbCrLf
    sConsulta = sConsulta & "           INSERT INTO LOG_PROVE (ACCION,ID_LOG_CON,COD,DEN,DIR,CP,POB,PAI,PROVI,MON,NIF,URLPROVE,ORIGEN,USU,ERP) SELECT 'R',@ID_LOG_CON,COD,DEN,DIR,CP,POB,PAI,PROVI,MON,NIF,URLPROVE,0,'',@ERP  FROM PROVE WHERE COD =@COD_PROVE_CIA" & vbCrLf
    sConsulta = sConsulta & "       end" & vbCrLf
    sConsulta = sConsulta & "          FETCH NEXT FROM Cur_ERP INTO @ERP" & vbCrLf
    sConsulta = sConsulta & "   END" & vbCrLf
    sConsulta = sConsulta & "   Close Cur_ERP" & vbCrLf
    sConsulta = sConsulta & "   DEALLOCATE Cur_ERP" & vbCrLf
    sConsulta = sConsulta & "   END" & vbCrLf
    sConsulta = sConsulta & "   " & vbCrLf
    sConsulta = sConsulta & "   " & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[INF_TIPOSOLICITUD_PYMES]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[INF_TIPOSOLICITUD_PYMES]" & vbCrLf
    gRDOCon.QueryTimeout = 7200
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "CREATE PROCEDURE [dbo].[INF_TIPOSOLICITUD_PYMES] (@USU VARCHAR(50)) AS " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "--''' <summary>" & vbCrLf
    sConsulta = sConsulta & "--'''  Procedimiento que se utiliza en el Interfaz para el paso de par�metros de la platadorma a un DTSX" & vbCrLf
    sConsulta = sConsulta & "--''' </summary>       " & vbCrLf
    sConsulta = sConsulta & "--''' <param name=""@IDIOMA"">Idioma</param>        " & vbCrLf
    sConsulta = sConsulta & "DECLARE @IDIOMA VARCHAR(20)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @PARAM VARCHAR(10)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @IDIOMA = 'SPA'" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SELECT @PARAM=UON1 FROM PER WITH (NOLOCK) WHERE PER.COD = @USU" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL='SELECT DISTINCT SOLICITUD.ID,DEN_' + @IDIOMA + ' AS DEN" & vbCrLf
    sConsulta = sConsulta & "       FROM SOLICITUD WITH(NOLOCK) " & vbCrLf
    sConsulta = sConsulta & "           INNER JOIN INSTANCIA WITH (NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "           ON SOLICITUD.ID=INSTANCIA.SOLICITUD" & vbCrLf
    sConsulta = sConsulta & "           INNER JOIN PER WITH (NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "           ON INSTANCIA.PETICIONARIO=PER.COD" & vbCrLf
    sConsulta = sConsulta & "       WHERE SOLICITUD.BAJA = 0 " & vbCrLf
    sConsulta = sConsulta & "       AND PER.UON1 = ''' + @PARAM + '''" & vbCrLf
    sConsulta = sConsulta & "       ORDER BY DEN_' + @IDIOMA + ''" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL,N'@USU VARCHAR (20)',@USU= @USU" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[INF_TIPOSOLICITUD]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[INF_TIPOSOLICITUD]" & vbCrLf
    gRDOCon.QueryTimeout = 7200
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "CREATE PROCEDURE [dbo].[INF_TIPOSOLICITUD] AS " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "--''' <summary>" & vbCrLf
    sConsulta = sConsulta & "--'''  Procedimiento que se utiliza en el Interfaz para el paso de par�metros de la platadorma a un DTSX" & vbCrLf
    sConsulta = sConsulta & "--''' </summary>       " & vbCrLf
    sConsulta = sConsulta & "--''' <param name=""@IDIOMA"">Idioma</param>        " & vbCrLf
    sConsulta = sConsulta & "DECLARE @IDIOMA VARCHAR(20)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @IDIOMA = 'SPA'" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL='SELECT DISTINCT ID,DEN_' + @IDIOMA + ' AS DEN" & vbCrLf
    sConsulta = sConsulta & "       FROM SOLICITUD WITH(NOLOCK) WHERE BAJA = 0 ORDER BY DEN_' + @IDIOMA + ''" & vbCrLf
    sConsulta = sConsulta & "       " & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL,N'@IDIOMA VARCHAR (20)',@IDIOMA = @IDIOMA" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[INF_PERSONARESPONSABLE]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[INF_PERSONARESPONSABLE]" & vbCrLf
    gRDOCon.QueryTimeout = 7200
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "CREATE PROCEDURE [dbo].[INF_PERSONARESPONSABLE]  AS " & vbCrLf
    sConsulta = sConsulta & "-- Procedimiento que se utiliza en el Interfaz para el paso de par�metros de la platadorma a un DTSX" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SELECT DISTINCT ie.PER AS [PER]" & vbCrLf
    sConsulta = sConsulta & "   ,per.NOM + ' ' + per.APE as [NombreApellido]" & vbCrLf
    sConsulta = sConsulta & "FROM instancia_est ie WITH (NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PER per" & vbCrLf
    sConsulta = sConsulta & "   ON ie.PER = per.COD" & vbCrLf
    sConsulta = sConsulta & "ORDER BY NombreApellido" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[INF_PERSONARESPONSABLE_PYMES]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[INF_PERSONARESPONSABLE_PYMES]" & vbCrLf
    gRDOCon.QueryTimeout = 7200
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "CREATE PROCEDURE [dbo].[INF_PERSONARESPONSABLE_PYMES] (@USU VARCHAR(50)) AS " & vbCrLf
    sConsulta = sConsulta & "-- Procedimiento que se utiliza en el Interfaz para el paso de par�metros de la platadorma a un DTSX" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @PARAM VARCHAR(10)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SELECT @PARAM=UON1 FROM PER WITH (NOLOCK) WHERE PER.COD = @USU" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @SQL='SELECT DISTINCT ie.PER AS [PER]" & vbCrLf
    sConsulta = sConsulta & "       ,per.NOM + '' '' + per.APE as [NombreApellido]" & vbCrLf
    sConsulta = sConsulta & "       FROM instancia_est ie WITH (NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "           INNER JOIN PER per WITH (NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "           ON ie.PER = per.COD" & vbCrLf
    sConsulta = sConsulta & "           INNER JOIN INSTANCIA WITH (NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "           ON PER.COD = INSTANCIA.PETICIONARIO" & vbCrLf
    sConsulta = sConsulta & "       WHERE PER.UON1 = ''' + @PARAM + '''" & vbCrLf
    sConsulta = sConsulta & "       AND per.NOM is not null" & vbCrLf
    sConsulta = sConsulta & "       AND per.APE is not null" & vbCrLf
    sConsulta = sConsulta & "       ORDER BY NombreApellido'" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL,N'@USU VARCHAR (20)',@USU = @USU" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_REGISTRAR_ACCESO]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_REGISTRAR_ACCESO]" & vbCrLf
    gRDOCon.QueryTimeout = 7200
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "CREATE PROCEDURE [dbo].[FSAL_REGISTRAR_ACCESO] @TIPO INT ,@PAGINA NVARCHAR(2000) ,@FPETICION VARCHAR(50), @FECHA varchar(50),@IDSESION VARCHAR(40),@USUCOD VARCHAR(50),@IPDIR VARCHAR(50),@CIACODGS VARCHAR(50),@CIACOD VARCHAR(50) ,@PRODUCTO VARCHAR(50),@PAGINAORIGEN NVARCHAR(2000),@NAVEGADOR NVARCHAR(200) AS" & vbCrLf
    sConsulta = sConsulta & "DECLARE @TSERVER INT" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FISERVERDATE DATETIME" & vbCrLf
    sConsulta = sConsulta & "-----" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FICLIRESPDATE DATETIME" & vbCrLf
    sConsulta = sConsulta & "DECLARE @TRESPUESTA INT" & vbCrLf
    sConsulta = sConsulta & "DECLARE @TDOWNLOAD INT" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "DECLARE @ENTORNO VARCHAR(15)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SELECT TOP 1 @ENTORNO=COD FROM FSAL_ENTORNOS WITH (NOLOCK) ORDER BY ID ASC" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "IF @TIPO=1" & vbCrLf
    sConsulta = sConsulta & "BEGIN" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "INSERT INTO FSAL_ACCESOS(PAGINA,FSAL_ENTORNO,PRODUCTO, FISERVER,USUARIO, PROVE,IP,HTTP_USER_AGENT ) VALUES (@PAGINA,@ENTORNO,@PRODUCTO,CAST(@FPETICION AS DATETIME),ISNULL (@USUCOD,''),ISNULL(@CIACODGS,@CIACOD),@IPDIR,@NAVEGADOR)" & vbCrLf
    sConsulta = sConsulta & "END" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "------" & vbCrLf
    sConsulta = sConsulta & "IF @TIPO=2" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "BEGIN" & vbCrLf
    sConsulta = sConsulta & "SET @TSERVER = (SELECT DATEDIFF(MS, CAST(@FPETICION AS DATETIME),CAST(@FECHA AS DATETIME) ))" & vbCrLf
    sConsulta = sConsulta & "UPDATE FSAL_ACCESOS SET FFSERVER= CAST (@FECHA AS DATETIME) , TSERVER=@TSERVER , HTTP_USER_AGENT=@NAVEGADOR WHERE PAGINA=@PAGINA AND FISERVER=CAST(@FPETICION AS DATETIME)" & vbCrLf
    sConsulta = sConsulta & "END" & vbCrLf
    sConsulta = sConsulta & "------" & vbCrLf
    sConsulta = sConsulta & "IF @TIPO=3" & vbCrLf
    sConsulta = sConsulta & "BEGIN" & vbCrLf
    sConsulta = sConsulta & "SET @FISERVERDATE = (SELECT TOP 1 FFSERVER FROM FSAL_ACCESOS WITH (NOLOCK)WHERE FISERVER=CAST(@FPETICION AS DATETIME)AND PAGINA=@PAGINA)" & vbCrLf
    sConsulta = sConsulta & "SET @TSERVER = (SELECT DATEDIFF(MS, CAST(@FISERVERDATE AS DATETIME),CAST(@FECHA AS DATETIME) ))" & vbCrLf
    sConsulta = sConsulta & "UPDATE FSAL_ACCESOS SET FICLIRESP= CAST(@FECHA AS DATETIME),TDOWNLOAD=@TSERVER WHERE PAGINA=@PAGINA AND FISERVER=CAST(@FPETICION AS DATETIME)" & vbCrLf
    sConsulta = sConsulta & "END" & vbCrLf
    sConsulta = sConsulta & "------" & vbCrLf
    sConsulta = sConsulta & "IF @TIPO=4" & vbCrLf
    sConsulta = sConsulta & "BEGIN" & vbCrLf
    sConsulta = sConsulta & "SET @FICLIRESPDATE = (SELECT TOP 1 FICLIRESP FROM FSAL_ACCESOS WITH (NOLOCK)WHERE FISERVER=CAST(@FPETICION AS DATETIME)AND PAGINA=@PAGINA)" & vbCrLf
    sConsulta = sConsulta & "SET @TRESPUESTA = (SELECT DATEDIFF(MS, CAST(@FICLIRESPDATE AS DATETIME),CAST(@FECHA AS DATETIME) ))" & vbCrLf
    sConsulta = sConsulta & "SET @TSERVER =(SELECT TOP 1 TSERVER FROM FSAL_ACCESOS WITH (NOLOCK)WHERE FISERVER=CAST(@FPETICION AS DATETIME)AND PAGINA=@PAGINA)" & vbCrLf
    sConsulta = sConsulta & "SET @TDOWNLOAD =(SELECT TOP 1 TDOWNLOAD FROM FSAL_ACCESOS WITH (NOLOCK)WHERE FISERVER=CAST(@FPETICION AS DATETIME)AND PAGINA=@PAGINA)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "UPDATE FSAL_ACCESOS SET FFCLIRESP= CAST (@FECHA AS DATETIME),TRESPUESTA=@TRESPUESTA ,TTOTAL=@TSERVER+@TDOWNLOAD+@TRESPUESTA WHERE PAGINA=@PAGINA AND FISERVER=CAST(@FPETICION AS DATETIME)" & vbCrLf
    sConsulta = sConsulta & "END" & vbCrLf
    sConsulta = sConsulta & "IF @TIPO=5" & vbCrLf
    sConsulta = sConsulta & "BEGIN" & vbCrLf
    sConsulta = sConsulta & "INSERT INTO FSAL_ACCESOS(PAGINA,FSAL_ENTORNO,PRODUCTO,FISERVER,FICLIPREPAR,PAGINAORIGEN,USUARIO, PROVE,IP) VALUES (@PAGINA,@ENTORNO,@PRODUCTO,CAST(@FPETICION AS DATETIME),CAST(@FECHA AS DATETIME),@PAGINAORIGEN,ISNULL (@USUCOD,''),ISNULL(@CIACODGS,@CIACOD),@IPDIR)" & vbCrLf
    sConsulta = sConsulta & "END" & vbCrLf
    sConsulta = sConsulta & "IF @TIPO=6" & vbCrLf
    sConsulta = sConsulta & "BEGIN" & vbCrLf
    sConsulta = sConsulta & "SET @FICLIRESPDATE = (SELECT TOP 1 FICLIPREPAR FROM FSAL_ACCESOS WITH (NOLOCK)WHERE FISERVER=CAST(@FPETICION AS DATETIME)AND PAGINA=@PAGINA)" & vbCrLf
    sConsulta = sConsulta & "SET @TSERVER = (SELECT DATEDIFF(MS, CAST(@FICLIRESPDATE AS DATETIME),CAST(@FECHA AS DATETIME) ))" & vbCrLf
    sConsulta = sConsulta & "UPDATE FSAL_ACCESOS SET FFCLIPREPAR= CAST (@FECHA AS DATETIME) , TPREPAR=@TSERVER WHERE PAGINA=@PAGINA AND FISERVER=CAST(@FPETICION AS DATETIME)" & vbCrLf
    sConsulta = sConsulta & "END" & vbCrLf
    sConsulta = sConsulta & "IF @TIPO=7" & vbCrLf
    sConsulta = sConsulta & "BEGIN" & vbCrLf
    sConsulta = sConsulta & "UPDATE FSAL_ACCESOS SET FISERVER= CAST(@FECHA AS DATETIME) WHERE PAGINA=@PAGINA AND FISERVER=CAST(@FPETICION AS DATETIME)" & vbCrLf
    sConsulta = sConsulta & "END" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_EMITIR_SOLICITUD]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_EMITIR_SOLICITUD]" & vbCrLf
    gRDOCon.QueryTimeout = 7200
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSWS_SOLICITUD_DATOS_EMAIL]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSWS_SOLICITUD_DATOS_EMAIL]" & vbCrLf
    gRDOCon.QueryTimeout = 7200
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_REEMITIR_SOLICITUD]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_REEMITIR_SOLICITUD]" & vbCrLf
    gRDOCon.QueryTimeout = 7200
    ExecuteSQL gRDOCon, sConsulta
    
End Sub


Public Function CodigoDeActualizacion31900_08_2000_A31900_08_2001() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim oADOConnection As ADODB.Connection
    Dim sConsulta As String

    On Error GoTo Error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
                  
    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Actualiza Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    frmProgreso.ProgressBar1.Refresh
    V_31900_8_Storeds_201
    
    sConsulta = "UPDATE VERSION SET NUM ='3.00.20.001'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103), PRODVER='31900.8'"
    ExecuteSQL gRDOCon, sConsulta
        
    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False
        
    CodigoDeActualizacion31900_08_2000_A31900_08_2001 = True
    Exit Function

Error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    CodigoDeActualizacion31900_08_2000_A31900_08_2001 = False
End Function

Public Sub V_31900_8_Storeds_201()
    Dim sConsulta As String
            
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSQA_GET_VARCERT_POND]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSQA_GET_VARCERT_POND]" & vbCrLf
    gRDOCon.QueryTimeout = 7200
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "CREATE PROCEDURE [dbo].[FSQA_GET_VARCERT_POND]" & vbCrLf
    sConsulta = sConsulta & "   @VARCAL AS INT," & vbCrLf
    sConsulta = sConsulta & "   @NIVEL AS SMALLINT, " & vbCrLf
    sConsulta = sConsulta & "   @PROVE AS VARCHAR(50)," & vbCrLf
    sConsulta = sConsulta & "   @ORIGEN INT     " & vbCrLf
    sConsulta = sConsulta & "AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SELECT C.FEC_CUMPLIM " & vbCrLf
    sConsulta = sConsulta & "FROM CERTIFICADO C  WITH(NOLOCK) " & vbCrLf
    sConsulta = sConsulta & "INNER JOIN PROVE_CERTIFICADO PC WITH(NOLOCK) ON PC.ID_CERTIF=C.ID AND PC.TIPO_CERTIF=@ORIGEN AND PC.PROVE=@PROVE" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SELECT CCD.CAMPO_ORIGEN AS ID,CCD.SUBTIPO,VP.TIPOPOND,VP.FORMULA,VP.PUNT_SI,VP.PUNT_NO,VP.NUM," & vbCrLf
    sConsulta = sConsulta & "CC.ID COPIA_CAMPO,CC.VALOR_NUM COPIA_NUM,CC.VALOR_FEC COPIA_FEC,CC.VALOR_BOOL COPIA_BOOL," & vbCrLf
    sConsulta = sConsulta & "COUNT(DISTINCT CCA.ID) AS TIENECERTIFICADO,C.FEC_EXPIRACION" & vbCrLf
    sConsulta = sConsulta & "FROM COPIA_CAMPO_DEF CCD WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "INNER JOIN VAR_CAL_PCERT VP WITH(NOLOCK) ON VP.CAMPO=CCD.CAMPO_ORIGEN AND VP.VARCAL=@VARCAL AND VP.NIVEL=@NIVEL" & vbCrLf
    sConsulta = sConsulta & "INNER JOIN COPIA_CAMPO CC WITH(NOLOCK) ON CC.COPIA_CAMPO_DEF=CCD.ID" & vbCrLf
    sConsulta = sConsulta & "INNER JOIN CERTIFICADO C WITH(NOLOCK) ON C.INSTANCIA=CC.INSTANCIA AND C.NUM_VERSION=CC.NUM_VERSION" & vbCrLf
    sConsulta = sConsulta & "INNER JOIN PROVE_CERTIFICADO PC WITH(NOLOCK) ON PC.ID_CERTIF=C.ID AND PC.TIPO_CERTIF=@ORIGEN AND PC.PROVE=@PROVE" & vbCrLf
    sConsulta = sConsulta & "LEFT JOIN COPIA_CAMPO_ADJUN CCA WITH(NOLOCK) ON CCA.CAMPO=CC.ID" & vbCrLf
    sConsulta = sConsulta & "GROUP BY CCD.CAMPO_ORIGEN,CCD.SUBTIPO,VP.TIPOPOND, VP.FORMULA, VP.PUNT_SI, VP.PUNT_NO, VP.NUM" & vbCrLf
    sConsulta = sConsulta & ",CC.ID,CC.VALOR_NUM,CC.VALOR_FEC,CC.VALOR_BOOL, C.FEC_EXPIRACION" & vbCrLf
    sConsulta = sConsulta & "ORDER BY NUM DESC" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SELECT CAMPO,VALOR_NUM,VALOR_FEC,DESDE_NUM,HASTA_NUM,DESDE_FEC,HASTA_FEC,VALOR_POND" & vbCrLf
    sConsulta = sConsulta & "FROM VAR_CAL_PCERT_LISTA WITH(NOLOCK) " & vbCrLf
    sConsulta = sConsulta & "WHERE VARCAL=@VARCAL AND NIVEL=@NIVEL" & vbCrLf
    sConsulta = sConsulta & "ORDER BY CAMPO,ORDEN" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSQA_GET_CERTIFICADOS]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSQA_GET_CERTIFICADOS]" & vbCrLf
    gRDOCon.QueryTimeout = 7200
    ExecuteSQL gRDOCon, sConsulta
    
sConsulta = "CREATE PROCEDURE [dbo].[FSQA_GET_CERTIFICADOS] " & vbCrLf
sConsulta = sConsulta & "   @IDI NVARCHAR(20), " & vbCrLf
sConsulta = sConsulta & "   @USU NVARCHAR(50)," & vbCrLf
sConsulta = sConsulta & "   @PER NVARCHAR(50)," & vbCrLf
sConsulta = sConsulta & "   @UON1 NVARCHAR(50)=NULL," & vbCrLf
sConsulta = sConsulta & "   @UON2 NVARCHAR(50)=NULL," & vbCrLf
sConsulta = sConsulta & "   @UON3 NVARCHAR(50)=NULL," & vbCrLf
sConsulta = sConsulta & "   @DEP NVARCHAR(50)=NULL," & vbCrLf
sConsulta = sConsulta & "   @FECDESDE DATE=NULL," & vbCrLf
sConsulta = sConsulta & "   @FECHASTA DATE =NULL," & vbCrLf
sConsulta = sConsulta & "   @TIPOPROV INT=3," & vbCrLf
sConsulta = sConsulta & "   @BAJAPROV INT=0," & vbCrLf
sConsulta = sConsulta & "   @CODPROV NVARCHAR(50)=NULL, " & vbCrLf
sConsulta = sConsulta & "   @CERTIFICADOS NVARCHAR(1000)=NULL," & vbCrLf
sConsulta = sConsulta & "   @MATERIALES NVARCHAR(1000)=NULL, " & vbCrLf
sConsulta = sConsulta & "   @CERTIFBAJA TINYINT=0," & vbCrLf
sConsulta = sConsulta & "   @ESTADO NVARCHAR(50)=NULL," & vbCrLf
sConsulta = sConsulta & "   @PETICIONARIO NVARCHAR(50)," & vbCrLf
sConsulta = sConsulta & "   @QUEFECHA TINYINT=1," & vbCrLf
sConsulta = sConsulta & "   @NOMBRE_TABLA NVARCHAR(50)=''," & vbCrLf
sConsulta = sConsulta & "   @ORDEN NVARCHAR(1000)=NULL, " & vbCrLf
sConsulta = sConsulta & "   @SOLOCONTADORES TINYINT=0," & vbCrLf
sConsulta = sConsulta & "   @PAGENUMBER INT=1," & vbCrLf
sConsulta = sConsulta & "   @PAGESIZE INT=1000," & vbCrLf
sConsulta = sConsulta & "   @OBTENERTODASRELACIONES TINYINT=1," & vbCrLf
sConsulta = sConsulta & "   @OBTENERTODOSCERTIFICADOS TINYINT=0," & vbCrLf
sConsulta = sConsulta & "   @REST_SOLICIT_USU TINYINT=0," & vbCrLf
sConsulta = sConsulta & "   @REST_SOLICIT_UO TINYINT=0," & vbCrLf
sConsulta = sConsulta & "   @REST_SOLICIT_DEP TINYINT=0," & vbCrLf
sConsulta = sConsulta & "   @REST_PROV_MAT TINYINT=0," & vbCrLf
sConsulta = sConsulta & "   @REST_PROV_EQP TINYINT=0," & vbCrLf
sConsulta = sConsulta & "   @REST_PROV_CON TINYINT=0 " & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "SET @SQL=''" & vbCrLf
sConsulta = sConsulta & "DECLARE @SEL AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "SET @SEL=''" & vbCrLf
sConsulta = sConsulta & "DECLARE @JOIN AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "SET @JOIN=''" & vbCrLf
sConsulta = sConsulta & "DECLARE @JOIN2 AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "SET @JOIN2=''" & vbCrLf
sConsulta = sConsulta & "DECLARE @JOIN3 AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "SET @JOIN3=''" & vbCrLf
sConsulta = sConsulta & "DECLARE @WHERE AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "SET @WHERE=''" & vbCrLf
sConsulta = sConsulta & "DECLARE @WHERE3 AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "SET @WHERE3=''" & vbCrLf
sConsulta = sConsulta & "DECLARE @JOIN_RESTRIC_SINSOLIC AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "SET @JOIN_RESTRIC_SINSOLIC =N''" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSQA_PROX_EXPIRAR INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @WHESTADO NVARCHAR(1000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @HAYESTADO TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @FILTROFECHA AS  NVARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL2 AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "SET @SQL2='' --Contiene lo q devuelva la 1ra select. La q saca los certificados solic y no solic de la pagina correspondiente. " & vbCrLf
sConsulta = sConsulta & "--As� tendre para la 3ra select, la de materiales, los @PAGESIZE registros correspondidentes, de esta info saco los proveedores y tipo_certificado" & vbCrLf
sConsulta = sConsulta & "--para ir a material_qa_certificados." & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--CAMBIOS" & vbCrLf
sConsulta = sConsulta & "--1� De la Select de certificados quito PUBLICADA_DEN,REAL_DEN,BAJA_DEN,MODO_SOLIC_DEN" & vbCrLf
sConsulta = sConsulta & "--" & vbCrLf
sConsulta = sConsulta & "--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" & vbCrLf
sConsulta = sConsulta & "--Pasar por par�metro" & vbCrLf
sConsulta = sConsulta & "--el par�metro FSQA_PROX_EXPIRAR, se podia obtener en FSNServer/Root/GetAccessType>Parametros_GetData> FSPM_GETPARAMETROS, eliminar la SELECT cd se haga" & vbCrLf
sConsulta = sConsulta & "--!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" & vbCrLf
sConsulta = sConsulta & "SELECT @FSQA_PROX_EXPIRAR=FSQA_PROX_EXPIRAR+1 FROM PARGEN_QA WITH(NOLOCK) WHERE ID=1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--***********************************************************************************************************" & vbCrLf
sConsulta = sConsulta & "--1� SELECT PARTES COMUNES" & vbCrLf
sConsulta = sConsulta & "--***********************************************************************************************************" & vbCrLf
sConsulta = sConsulta & "SET @SEL='SELECT DISTINCT" & vbCrLf
sConsulta = sConsulta & "   C.NUM_VERSION,C.ID ID_CERTIF,C.PUBLICADA,C.FEC_DESPUB,C.FEC_SOLICITUD,C.FEC_RESPUESTA,C.FEC_EXPIRACION,C.FEC_LIM_CUMPLIM" & vbCrLf
sConsulta = sConsulta & "   ,C.PER,C.FEC_PUBLICACION,C.FECACT FEC_ACT,C.CERTIFICADO,C.FEC_CUMPLIM" & vbCrLf
sConsulta = sConsulta & "   ,PR.COD PROVE_COD,PR.DEN PROVE_DEN,ISNULL(PR.CALIDAD_POT,0) REAL,ISNULL(PR.BAJA_CALIDAD,0) BAJA" & vbCrLf
sConsulta = sConsulta & "   ,S.ID AS TIPO_CERTIFICADO,S.DEN_' + @IDI + ' DEN_TIPO_CERTIFICADO,ISNULL(S.PLAZO_CUMPL,0) PLAZO_CUMPL,~S.OPCIONAL OBLIGATORIO,S.MODO_SOLIC" & vbCrLf
sConsulta = sConsulta & "   ,PT.NOM + '' '' + PT.APE NOMBREPETICIONARIO " & vbCrLf
sConsulta = sConsulta & "   ,ISNULL(I.EN_PROCESO,0) EN_PROCESO" & vbCrLf
sConsulta = sConsulta & "   ,PR.COD + ''#'' + CAST(S.ID AS VARCHAR(10)) AS DATAKEY" & vbCrLf
sConsulta = sConsulta & "   ,CASE WHEN C.FEC_CUMPLIM IS NULL THEN 2 " & vbCrLf
sConsulta = sConsulta & "   WHEN C.FEC_EXPIRACION IS NULL THEN 3 " & vbCrLf
sConsulta = sConsulta & "   WHEN DATEDIFF(day,getdate(),C.FEC_EXPIRACION)>@FSQA_PROX_EXPIRAR THEN 3 " & vbCrLf
sConsulta = sConsulta & "   WHEN DATEDIFF(day,getdate(),C.FEC_EXPIRACION) BETWEEN 0 AND @FSQA_PROX_EXPIRAR THEN 4 " & vbCrLf
sConsulta = sConsulta & "   ELSE 5 END ESTADO,'''' AS ESTADO_DEN" & vbCrLf
sConsulta = sConsulta & "   , '''' AS DEN_MATERIAL, '''' AS ID_MATERIAL" & vbCrLf
sConsulta = sConsulta & "   ,CC.EMAIL CON_EMAIL,0 AS NOPETIC'" & vbCrLf
sConsulta = sConsulta & "SET @JOIN =  N' FROM CERTIFICADO C    WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN INSTANCIA I WITH(NOLOCK) ON C.INSTANCIA=I.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN SOLICITUD S WITH(NOLOCK) ON I.SOLICITUD=S.ID AND S.PUB=1'" & vbCrLf
sConsulta = sConsulta & "IF @NOMBRE_TABLA <> ''" & vbCrLf
sConsulta = sConsulta & "   SET @JOIN = @JOIN + N' AND S.FORMULARIO=(SELECT FORMULARIO FROM QA_FILTROS WITH(NOLOCK) WHERE NOM_TABLA=''' + @NOMBRE_TABLA + ''')'" & vbCrLf
sConsulta = sConsulta & "SET @JOIN = @JOIN + N' INNER JOIN PROVE_CERTIFICADO PC   WITH(NOLOCK) ON S.ID=PC.TIPO_CERTIF AND C.ID=PC.ID_CERTIF" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PROVE PR    WITH(NOLOCK) ON C.PROVE=PR.COD AND ((S.TIPO_PROVE IS NULL) OR (PR.CALIDAD_POT=S.TIPO_PROVE))" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PER PT WITH(NOLOCK) ON C.PER=PT.COD" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN CON CC WITH(NOLOCK) ON C.PROVE=CC.PROVE AND C.CONTACTO=CC.ID'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--A�adir RESTRICCIONES de CONSULTA DE CERTIFICADOS del USUARIO" & vbCrLf
sConsulta = sConsulta & "-- Sin solicitar no se controla @REST_SOLICIT_USU=1 OR @REST_SOLICIT_DEP=1 OR @REST_SOLICIT_UO=1  " & vbCrLf
sConsulta = sConsulta & "IF @REST_PROV_CON=1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "    SET @JOIN_RESTRIC_SINSOLIC = @JOIN_RESTRIC_SINSOLIC + N' INNER JOIN CON WITH(NOLOCK) ON P4.PROVE=CON.PROVE AND CON.CALIDAD=1'" & vbCrLf
sConsulta = sConsulta & "   SET @JOIN = @JOIN + N' INNER JOIN CON WITH(NOLOCK) ON C.PROVE=CON.PROVE AND CON.CALIDAD=1'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @REST_PROV_EQP=1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @JOIN_RESTRIC_SINSOLIC = @JOIN_RESTRIC_SINSOLIC + N' INNER JOIN PER P WITH(NOLOCK) ON P.COD=@PER'" & vbCrLf
sConsulta = sConsulta & "   SET @JOIN_RESTRIC_SINSOLIC = @JOIN_RESTRIC_SINSOLIC + N' INNER JOIN PROVE_EQP PE WITH(NOLOCK) ON P.EQP=PE.EQP AND P4.PROVE=PE.PROVE'   " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   SET @JOIN = @JOIN + N' INNER JOIN PER P WITH(NOLOCK) ON P.COD=C.PER'    " & vbCrLf
sConsulta = sConsulta & "   SET @JOIN = @JOIN + N' INNER JOIN PROVE_EQP PE WITH(NOLOCK)  ON P.EQP=PE.EQP AND C.PROVE=PE.PROVE'   " & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--1� Los que a�aden m�s tablas al FROM (SELECCI�N POR MATERIAL)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @MATERIALES IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @JOIN = @JOIN + N' INNER JOIN MATERIAL_QA_CERTIFICADOS MC WITH(NOLOCK) ON MC.TIPO_CERTIFICADO=S.ID'     " & vbCrLf
sConsulta = sConsulta & "   SET @JOIN = @JOIN + N' AND MC.MATERIAL IN (' + @MATERIALES + ')'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @REST_PROV_MAT=1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @JOIN_RESTRIC_SINSOLIC = @JOIN_RESTRIC_SINSOLIC + N' INNER JOIN COM_GMN4 C4 WITH(NOLOCK) ON C4.COM=@PER AND C4.GMN1=P4.GMN1 AND C4.GMN2=P4.GMN2 AND C4.GMN3=P4.GMN3 AND C4.GMN4=P4.GMN4'" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   IF @MATERIALES IS NULL" & vbCrLf
sConsulta = sConsulta & "       SET @JOIN = @JOIN + N' INNER JOIN MATERIAL_QA_CERTIFICADOS MC WITH(NOLOCK) ON MC.TIPO_CERTIFICADO=S.ID'" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   SET @JOIN = @JOIN + N' INNER JOIN MATERIAL_QA_GMN4 M4 WITH (NOLOCK) ON MC.MATERIAL=M4.MATERIAL_QA " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PROVE_GMN4 P4 WITH (NOLOCK) ON M4.GMN1=P4.GMN1 AND M4.GMN2=P4.GMN2 AND M4.GMN3=P4.GMN3 " & vbCrLf
sConsulta = sConsulta & "           AND M4.GMN4=P4.GMN4 AND P4.PROVE=PR.COD '   " & vbCrLf
sConsulta = sConsulta & "   SET @JOIN = @JOIN + N' INNER JOIN COM_GMN4 C4 WITH(NOLOCK) ON C4.COM=@PER AND C4.GMN1=P4.GMN1 AND C4.GMN2=P4.GMN2 AND C4.GMN3=P4.GMN3 AND C4.GMN4=P4.GMN4'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "--Filtros" & vbCrLf
sConsulta = sConsulta & "IF @NOMBRE_TABLA <> ''" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @JOIN2 = @JOIN + N' LEFT JOIN ' + @IDI + '_' + @NOMBRE_TABLA + ' FILTRO WITH(NOLOCK)ON C.INSTANCIA = FILTRO.INSTANCIA '" & vbCrLf
sConsulta = sConsulta & "   SET @JOIN = @JOIN + N' INNER JOIN ' + @IDI + '_' + @NOMBRE_TABLA + ' FILTRO WITH(NOLOCK)ON C.INSTANCIA = FILTRO.INSTANCIA '" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "--A�adir CRITERIOS DE SELECCI�N" & vbCrLf
sConsulta = sConsulta & "SET @WHERE =  N' WHERE 1=1'" & vbCrLf
sConsulta = sConsulta & "IF @REST_SOLICIT_USU=1" & vbCrLf
sConsulta = sConsulta & "    SET @WHERE = @WHERE + ' AND C.USU=@USU'" & vbCrLf
sConsulta = sConsulta & "IF @CODPROV IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @WHERE = @WHERE + ' AND PR.COD=@CODPROV'" & vbCrLf
sConsulta = sConsulta & "IF @BAJAPROV=0" & vbCrLf
sConsulta = sConsulta & "   SET @WHERE = @WHERE + ' AND PR.BAJA_CALIDAD=0'" & vbCrLf
sConsulta = sConsulta & "IF @TIPOPROV=1" & vbCrLf
sConsulta = sConsulta & "   SET @WHERE = @WHERE + ' AND PR.CALIDAD_POT=1'  --Proveedor potencial" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "   IF @TIPOPROV=2" & vbCrLf
sConsulta = sConsulta & "       SET @WHERE = @WHERE + ' AND PR.CALIDAD_POT=2'  --Proveedor real" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "       SET @WHERE= @WHERE + '  AND (PR.CALIDAD_POT=1 OR PR.CALIDAD_POT=2) '" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "IF @CERTIFICADOS IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @WHERE = @WHERE +  ' AND S.ID IN (' + @CERTIFICADOS + ')'" & vbCrLf
sConsulta = sConsulta & "IF @CERTIFBAJA=0" & vbCrLf
sConsulta = sConsulta & "   SET @WHERE = @WHERE +  ' AND S.BAJA=0 ' " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @WHESTADO=N' AND ('    " & vbCrLf
sConsulta = sConsulta & "SET @HAYESTADO = 0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF NOT(CHARINDEX('2',@ESTADO)=0)    " & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   IF @HAYESTADO=1 " & vbCrLf
sConsulta = sConsulta & "       SET @WHESTADO = @WHESTADO + ' OR '" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "   SET @WHESTADO = @WHESTADO + 'C.FEC_CUMPLIM IS NULL'" & vbCrLf
sConsulta = sConsulta & "   SET @HAYESTADO = 1" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "IF NOT(CHARINDEX('3',@ESTADO)=0)    " & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   IF @HAYESTADO=1 " & vbCrLf
sConsulta = sConsulta & "       SET @WHESTADO = @WHESTADO + ' OR '" & vbCrLf
sConsulta = sConsulta & "   SET @WHESTADO = @WHESTADO + '('" & vbCrLf
sConsulta = sConsulta & "   SET @WHESTADO = @WHESTADO + 'C.FEC_CUMPLIM IS NOT NULL'" & vbCrLf
sConsulta = sConsulta & "   SET @WHESTADO = @WHESTADO + ' AND ((C.FEC_EXPIRACION IS NULL)'" & vbCrLf
sConsulta = sConsulta & "   SET @WHESTADO = @WHESTADO + ' OR ((C.FEC_EXPIRACION IS NOT NULL) AND DATEDIFF(day,getdate(),C.FEC_EXPIRACION)>@FSQA_PROX_EXPIRAR)'" & vbCrLf
sConsulta = sConsulta & "   SET @WHESTADO = @WHESTADO + ')'" & vbCrLf
sConsulta = sConsulta & "   SET @WHESTADO = @WHESTADO + ')'" & vbCrLf
sConsulta = sConsulta & "   SET @HAYESTADO = 1" & vbCrLf
sConsulta = sConsulta & "END        " & vbCrLf
sConsulta = sConsulta & "IF NOT(CHARINDEX('4',@ESTADO)=0)    " & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   IF @HAYESTADO=1 " & vbCrLf
sConsulta = sConsulta & "       SET @WHESTADO = @WHESTADO + ' OR '" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   SET @WHESTADO = @WHESTADO + '(C.FEC_CUMPLIM IS NOT NULL AND C.FEC_EXPIRACION IS NOT NULL' " & vbCrLf
sConsulta = sConsulta & "   SET @WHESTADO = @WHESTADO + ' AND DATEDIFF(day,getdate(),C.FEC_EXPIRACION) BETWEEN 0 AND @FSQA_PROX_EXPIRAR)'" & vbCrLf
sConsulta = sConsulta & "   SET @HAYESTADO = 1" & vbCrLf
sConsulta = sConsulta & "END            " & vbCrLf
sConsulta = sConsulta & "IF NOT(CHARINDEX('5',@ESTADO)=0)    " & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   IF @HAYESTADO=1 " & vbCrLf
sConsulta = sConsulta & "       SET @WHESTADO = @WHESTADO + ' OR '" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   SET @WHESTADO = @WHESTADO + '(C.FEC_CUMPLIM IS NOT NULL AND C.FEC_EXPIRACION IS NOT NULL' " & vbCrLf
sConsulta = sConsulta & "   SET @WHESTADO = @WHESTADO + ' AND DATEDIFF(day,getdate(),C.FEC_EXPIRACION)<0)'" & vbCrLf
sConsulta = sConsulta & "   SET @HAYESTADO = 1" & vbCrLf
sConsulta = sConsulta & "END        " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @HAYESTADO=1 " & vbCrLf
sConsulta = sConsulta & "   SET @WHERE = @WHERE + @WHESTADO + ')'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @PETICIONARIO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @WHERE = @WHERE +  ' AND C.PER IN (' + @PETICIONARIO + ')'       " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "IF @REST_SOLICIT_DEP=1 OR @REST_SOLICIT_UO=1 " & vbCrLf
sConsulta = sConsulta & "BEGIN      " & vbCrLf
sConsulta = sConsulta & "   IF @REST_SOLICIT_DEP=1" & vbCrLf
sConsulta = sConsulta & "       SET @WHERE = @WHERE + N' AND C.DEP=@DEP '" & vbCrLf
sConsulta = sConsulta & "   IF @REST_SOLICIT_UO=1" & vbCrLf
sConsulta = sConsulta & "       SET @WHERE = @WHERE + N' AND (@UON1 IS NULL OR C.UON1=@UON1) AND (@UON2 IS NULL OR C.UON2=@UON2) AND (@UON3 IS NULL OR C.UON3=@UON3) '       " & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @QUEFECHA=1" & vbCrLf
sConsulta = sConsulta & "   SET @FILTROFECHA='C.FEC_SOLICITUD'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   IF @QUEFECHA=2" & vbCrLf
sConsulta = sConsulta & "       SET @FILTROFECHA='C.FEC_LIM_CUMPLIM'    " & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "       IF @QUEFECHA=3" & vbCrLf
sConsulta = sConsulta & "           SET @FILTROFECHA='C.FECACT'     " & vbCrLf
sConsulta = sConsulta & "       ELSE" & vbCrLf
sConsulta = sConsulta & "           IF @QUEFECHA=4" & vbCrLf
sConsulta = sConsulta & "               SET @FILTROFECHA='C.FEC_EXPIRACION' " & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "IF @FECDESDE IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "    SET @WHERE = @WHERE + '  AND (' + @FILTROFECHA + '>=@FECDESDE OR ' + @FILTROFECHA + ' IS NULL)'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @FECHASTA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "    SET @WHERE = @WHERE + '  AND (' + @FILTROFECHA + '<=@FECHASTA OR ' + @FILTROFECHA + ' IS NULL)'    " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--***********************************************************************************************************" & vbCrLf
sConsulta = sConsulta & "--1� SELECT DE CERTIFICADOS: SOLICITADOS" & vbCrLf
sConsulta = sConsulta & "--***********************************************************************************************************" & vbCrLf
sConsulta = sConsulta & "IF @ESTADO IS NULL OR NOT(CHARINDEX('2',@ESTADO)=0) OR NOT(CHARINDEX('3',@ESTADO)=0) OR NOT(CHARINDEX('4',@ESTADO)=0) OR NOT(CHARINDEX('5',@ESTADO)=0)" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @SOLOCONTADORES =0" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "   --CALIDAD. Este * es imprencisdible la defini�n de la tabla FILTRO es desconocida de antemano. Se crea y mantiene dinamicamnte con las columnas del formulario." & vbCrLf
sConsulta = sConsulta & "   IF @NOMBRE_TABLA <> ''  " & vbCrLf
sConsulta = sConsulta & "       SET @SQL = @SEL +  ',FILTRO.*'" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = @SEL +  ',C.INSTANCIA'" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=N'SELECT COUNT(DISTINCT DATAKEY) NUM FROM (SELECT DISTINCT PR.COD + ''#'' + S.COD AS DATAKEY'" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + @JOIN + @WHERE" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--***********************************************************************************************************" & vbCrLf
sConsulta = sConsulta & "--1� SELECT DE CERTIFICADOS: PDTE CUMPLIMENTAR" & vbCrLf
sConsulta = sConsulta & "--Existe registro en certificado, prove_certificado y instancia CON instancia.version a 0" & vbCrLf
sConsulta = sConsulta & "--Q pasa ahora si trabajas con SPA_QA_FILTROxx pues q hasta q alguien en el portal lo envia no hay nada en SPA_QA_FILTROxx y " & vbCrLf
sConsulta = sConsulta & "--en consecuencia INNER JOIN SPA_QA_FILTROxx .... no los saca, vamos q la select anterior no los comtempla" & vbCrLf
sConsulta = sConsulta & "--***********************************************************************************************************" & vbCrLf
sConsulta = sConsulta & "IF @NOMBRE_TABLA <> '' " & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & " IF @ESTADO IS NULL OR NOT(CHARINDEX('2',@ESTADO)=0) OR NOT(CHARINDEX('3',@ESTADO)=0) OR NOT(CHARINDEX('4',@ESTADO)=0) OR NOT(CHARINDEX('5',@ESTADO)=0)" & vbCrLf
sConsulta = sConsulta & " BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @SQL<>''" & vbCrLf
sConsulta = sConsulta & "     SET @SQL = @SQL + ' UNION '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @SOLOCONTADORES =0" & vbCrLf
sConsulta = sConsulta & "     BEGIN" & vbCrLf
sConsulta = sConsulta & "     --CALIDAD. Este * es imprencisdible la defini�n de la tabla FILTRO es desconocida de antemano. Se crea y mantiene dinamicamnte con las columnas del formulario." & vbCrLf
sConsulta = sConsulta & "     SET @SQL = @SQL + @SEL +  ',FILTRO.*'     -- Como es Union hay q respetar las columnas, pero se q va a ser null" & vbCrLf
sConsulta = sConsulta & "     END" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "     BEGIN   " & vbCrLf
sConsulta = sConsulta & "     IF @SQL<>''" & vbCrLf
sConsulta = sConsulta & "         SET @SQL = @SQL + 'SELECT DISTINCT PR.COD + ''#'' + S.COD AS DATAKEY'" & vbCrLf
sConsulta = sConsulta & "     ELSE" & vbCrLf
sConsulta = sConsulta & "         SET @SQL=N'SELECT COUNT(DISTINCT DATAKEY) NUM FROM (SELECT DISTINCT PR.COD + ''#'' + S.COD AS DATAKEY'     " & vbCrLf
sConsulta = sConsulta & "     END" & vbCrLf
sConsulta = sConsulta & "   --La join es la JOIN2" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + @JOIN2    " & vbCrLf
sConsulta = sConsulta & "   --La WHERE es la comun" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + @WHERE  " & vbCrLf
sConsulta = sConsulta & " END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--***********************************************************************************************************" & vbCrLf
sConsulta = sConsulta & "--1� SELECT DE CERTIFICADOS: UNION CON SIN SOLICITAR" & vbCrLf
sConsulta = sConsulta & "--***********************************************************************************************************" & vbCrLf
sConsulta = sConsulta & "IF @ESTADO IS NULL OR NOT(CHARINDEX('1',@ESTADO)=0)     " & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLAUX NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "IF @SQL<>''" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + ' UNION '" & vbCrLf
sConsulta = sConsulta & "IF @SOLOCONTADORES=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   --CALIDAD. Este * es imprencisdible la defini�n de la tabla FILTRO es desconocida de antemano. Se crea y mantiene dinamicamnte con las columnas del formulario. " & vbCrLf
sConsulta = sConsulta & "   SET @SQLAUX = 'SELECT  DISTINCT" & vbCrLf
sConsulta = sConsulta & "   NULL AS NUM_VERSION,NULL AS  ID_CERTIF,NULL AS PUBLICADA,NULL AS FEC_DESPUB,NULL AS FEC_SOLICITUD,NULL AS FEC_RESPUESTA,NULL AS FEC_EXPIRACION" & vbCrLf
sConsulta = sConsulta & "   ,NULL AS FEC_LIM_CUMPLIM,NULL AS PER,NULL AS FEC_PUBLICACION,NULL AS FEC_ACT,NULL AS CERTIFICADO,NULL AS FEC_CUMPLIM" & vbCrLf
sConsulta = sConsulta & "   ,PR.COD PROVE_COD,PR.DEN PROVE_DEN,ISNULL(PR.CALIDAD_POT,0) REAL,ISNULL(PR.BAJA_CALIDAD,0) BAJA" & vbCrLf
sConsulta = sConsulta & "   ,S.ID AS TIPO_CERTIFICADO,S.DEN_' + @IDI + ' DEN_TIPO_CERTIFICADO,ISNULL(S.PLAZO_CUMPL,0) PLAZO_CUMPL,~S.OPCIONAL OBLIGATORIO,S.MODO_SOLIC" & vbCrLf
sConsulta = sConsulta & "   ,NULL AS NOMBREPETICIONARIO " & vbCrLf
sConsulta = sConsulta & "   ,0 AS EN_PROCESO" & vbCrLf
sConsulta = sConsulta & "   ,PR.COD + ''#'' + CAST(S.ID AS VARCHAR(10)) AS DATAKEY" & vbCrLf
sConsulta = sConsulta & "   ,1 ESTADO ,'''' AS ESTADO_DEN" & vbCrLf
sConsulta = sConsulta & "   , '''' AS DEN_MATERIAL, '''' AS ID_MATERIAL" & vbCrLf
sConsulta = sConsulta & "   ,NULL AS CON_EMAIL,0 AS NOPETIC'" & vbCrLf
sConsulta = sConsulta & "   IF @NOMBRE_TABLA <> ''  " & vbCrLf
sConsulta = sConsulta & "       SET @SQLAUX = @SQLAUX +  ',FILTRO.*'" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "       SET @SQLAUX = @SQLAUX +  ',NULL AS INSTANCIA'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   IF @SQL<>''" & vbCrLf
sConsulta = sConsulta & "      SET @SQLAUX ='SELECT DISTINCT PR.COD + ''#'' + CAST(S.ID AS VARCHAR(10)) AS DATAKEY'" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=N'SELECT COUNT(DISTINCT DATAKEY) NUM FROM ('" & vbCrLf
sConsulta = sConsulta & "       SET @SQLAUX=N'SELECT DISTINCT PR.COD + ''#'' + CAST(S.ID AS VARCHAR(10)) AS DATAKEY'  " & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "SET @JOIN3 = N' FROM MATERIAL_QA_CERTIFICADOS MC WITH(NOLOCK)  " & vbCrLf
sConsulta = sConsulta & "INNER JOIN SOLICITUD S WITH(NOLOCK) ON MC.TIPO_CERTIFICADO=S.ID AND S.PUB=1 '" & vbCrLf
sConsulta = sConsulta & "IF @NOMBRE_TABLA <> ''" & vbCrLf
sConsulta = sConsulta & "      SET @JOIN3 = @JOIN3+ N' AND S.FORMULARIO=(SELECT FORMULARIO FROM QA_FILTROS WITH(NOLOCK) WHERE NOM_TABLA=''' + @NOMBRE_TABLA + ''')'" & vbCrLf
sConsulta = sConsulta & " SET @JOIN3 = @JOIN3 + N' INNER JOIN MATERIAL_QA_GMN4 M4 WITH(NOLOCK) ON MC.MATERIAL=M4.MATERIAL_QA " & vbCrLf
sConsulta = sConsulta & "INNER JOIN PROVE_GMN4 P4 WITH(NOLOCK) ON M4.GMN1=P4.GMN1 AND M4.GMN2=P4.GMN2 AND M4.GMN3=P4.GMN3 AND M4.GMN4=P4.GMN4 " & vbCrLf
sConsulta = sConsulta & "INNER JOIN PROVE PR WITH(NOLOCK) ON P4.PROVE=PR.COD AND ((S.TIPO_PROVE IS NULL) OR (PR.CALIDAD_POT=S.TIPO_PROVE))" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PROVE_CERTIFICADO PC WITH(NOLOCK) ON PC.PROVE=P4.PROVE AND PC.TIPO_CERTIF=MC.TIPO_CERTIFICADO '" & vbCrLf
sConsulta = sConsulta & "--Filtros" & vbCrLf
sConsulta = sConsulta & "IF @NOMBRE_TABLA <> ''" & vbCrLf
sConsulta = sConsulta & "     SET @JOIN3 = @JOIN3 + N' LEFT JOIN ' + @IDI + '_' + @NOMBRE_TABLA + ' FILTRO WITH(NOLOCK) ON FILTRO.INSTANCIA=NULL '" & vbCrLf
sConsulta = sConsulta & "SET @JOIN3 = @JOIN3 + @JOIN_RESTRIC_SINSOLIC" & vbCrLf
sConsulta = sConsulta & "SET @WHERE3 = N' WHERE PC.PROVE IS NULL AND PC.TIPO_CERTIF IS NULL' " & vbCrLf
sConsulta = sConsulta & "IF @MATERIALES IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "    SET @WHERE3  = @WHERE3 + N' AND MC.MATERIAL IN (' + @MATERIALES + ')'" & vbCrLf
sConsulta = sConsulta & "IF @CODPROV IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "    SET @WHERE3 = @WHERE3 + ' AND PR.COD=@CODPROV'" & vbCrLf
sConsulta = sConsulta & "IF @BAJAPROV=0" & vbCrLf
sConsulta = sConsulta & "    SET @WHERE3 = @WHERE3 + ' AND PR.BAJA_CALIDAD=0'" & vbCrLf
sConsulta = sConsulta & "IF @TIPOPROV=1" & vbCrLf
sConsulta = sConsulta & "    SET @WHERE3 = @WHERE3 + ' AND PR.CALIDAD_POT=1'  --Proveedor potencial" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "      IF @TIPOPROV=2" & vbCrLf
sConsulta = sConsulta & "          SET @WHERE3 = @WHERE3 + ' AND PR.CALIDAD_POT=2'  --Proveedor real" & vbCrLf
sConsulta = sConsulta & "       ELSE" & vbCrLf
sConsulta = sConsulta & "          SET @WHERE3 = @WHERE3 + '  AND (PR.CALIDAD_POT=1 OR PR.CALIDAD_POT=2) '" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "IF @CODPROV IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "    SET @WHERE3 = @WHERE3 + ' AND PR.COD=@CODPROV'" & vbCrLf
sConsulta = sConsulta & "IF @CERTIFICADOS IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "    SET @WHERE3 = @WHERE3 +  ' AND S.ID IN (' + @CERTIFICADOS + ')'" & vbCrLf
sConsulta = sConsulta & "IF @CERTIFBAJA=0" & vbCrLf
sConsulta = sConsulta & "    SET @WHERE3 = @WHERE3 +  ' AND S.BAJA=0 '" & vbCrLf
sConsulta = sConsulta & "IF @REST_SOLICIT_DEP=1 OR @REST_SOLICIT_UO=1 OR @REST_SOLICIT_USU=1" & vbCrLf
sConsulta = sConsulta & "BEGIN  " & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL+@SQLAUX+@JOIN3+N' LEFT JOIN PETICIONARIOS WITH(NOLOCK) ON PETICIONARIOS.SOLICITUD=S.ID '+@WHERE3+N' AND PETICIONARIOS.ID IS NULL '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL++N'UNION '+@SQLAUX+@JOIN3+N' INNER JOIN PETICIONARIOS WITH(NOLOCK) ON PETICIONARIOS.SOLICITUD=S.ID '" & vbCrLf
sConsulta = sConsulta & "   IF @REST_SOLICIT_UO=1" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'AND (@UON1 IS NULL OR PETICIONARIOS.UON1=@UON1) AND (@UON2 IS NULL OR PETICIONARIOS.UON2=@UON2) AND (@UON3 IS NULL OR PETICIONARIOS.UON3=@UON3) '" & vbCrLf
sConsulta = sConsulta & "   IF @REST_SOLICIT_DEP=1" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'AND PETICIONARIOS.DEP=@DEP '" & vbCrLf
sConsulta = sConsulta & "   IF @REST_SOLICIT_USU=1" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'AND PETICIONARIOS.PER=@PER '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL+@WHERE3" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL+@SQLAUX+@JOIN3+@WHERE3" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Montamos la query" & vbCrLf
sConsulta = sConsulta & "IF @SOLOCONTADORES =0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   IF @ORDEN IS NULL" & vbCrLf
sConsulta = sConsulta & "       SET @ORDEN= 'PROVE_DEN'" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       IF @ORDEN = ''" & vbCrLf
sConsulta = sConsulta & "           SET @ORDEN=  'PROVE_DEN'" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "   DECLARE @ROW INT = ((@PAGENUMBER-1) * @PAGESIZE) +1" & vbCrLf
sConsulta = sConsulta & "   IF @OBTENERTODOSCERTIFICADOS=0" & vbCrLf
sConsulta = sConsulta & "       BEGIN             " & vbCrLf
sConsulta = sConsulta & "       SET @SQL2 = @SQL" & vbCrLf
sConsulta = sConsulta & "       --CALIDAD. Estos dos * se quedan pq no es la tabla de bbdd certificados sino una tabla hecha ad hoc mediante WITH CERTIFICADOS AS (...) y en estos puntos suspensivos puede venir filtro.* o no -> no se conocen las columnas" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = ';WITH CERTIFICADOS AS (' + @SQL + '), TOTCERTIF AS (SELECT *,ROW_NUMBER() OVER (ORDER BY ' + @ORDEN + ') AS ROWNUMBER FROM CERTIFICADOS WITH (NOLOCK)) SELECT *,ROWNUMBER FROM TOTCERTIF WITH (NOLOCK) WHERE ROWNUMBER BETWEEN ' + CONVERT(VARCHAR(10),@ROW) + ' AND ' + CONVERT(VARCHAR(10),(@ROW + @PAGESIZE - 1))" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + ') CERT'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL,N'@PER VARCHAR(50),@USU VARCHAR(50),@FSQA_PROX_EXPIRAR INT,@CODPROV NVARCHAR(50),@FECDESDE AS DATE,@FECHASTA AS DATE,@UON1 NVARCHAR(50),@UON2 NVARCHAR(50),@UON3 NVARCHAR(50),@DEP NVARCHAR(50)'," & vbCrLf
sConsulta = sConsulta & "@PER=@PER,@USU=@USU,@FSQA_PROX_EXPIRAR=@FSQA_PROX_EXPIRAR,@CODPROV=@CODPROV,@FECDESDE=@FECDESDE,@FECHASTA=@FECHASTA,@UON1=@UON1,@UON2=@UON2,@UON3=@UON3,@DEP=@DEP" & vbCrLf
sConsulta = sConsulta & "--*********************************************************************************************************************************************************" & vbCrLf
sConsulta = sConsulta & "--2� SELECT DE PERMISOS DE SOLICITUD DE CERTIFICADOS DEL USUARIO (devuelve un rgistro por cada tio de certificado que el usuario puede solicitar)" & vbCrLf
sConsulta = sConsulta & "--Desde c�digo se dar� valor a la columna de la 1� SELECT: NOPETIC" & vbCrLf
sConsulta = sConsulta & "--*********************************************************************************************************************************************************" & vbCrLf
sConsulta = sConsulta & "IF @SOLOCONTADORES =0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SQL=N'SELECT S.ID AS TIPO_CERTIFICADO" & vbCrLf
sConsulta = sConsulta & ",CASE WHEN PET.COD IS NULL THEN CASE WHEN PU.ID IS NOT NULL THEN 0 ELSE 1 END ELSE 0 END  AS NOPETIC" & vbCrLf
sConsulta = sConsulta & "FROM SOLICITUD S WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN TIPO_SOLICITUDES TS WITH (NOLOCK) ON S.TIPO=TS.ID AND TS.TIPO=2" & vbCrLf
sConsulta = sConsulta & "INNER JOIN USU WITH (NOLOCK) ON USU.COD=@USU" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN VIEW_PETICIONARIOS PET WITH (NOLOCK)  ON PET.SOLICITUD=S.ID AND USU.PER = PET.COD" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN (SELECT S.ID " & vbCrLf
sConsulta = sConsulta & "   FROM SOLICITUD S WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN TIPO_SOLICITUDES TS WITH (NOLOCK) ON S.TIPO=TS.ID AND TS.TIPO =2" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN VIEW_PETICIONARIOS PET WITH(NOLOCK) ON PET.SOLICITUD=S.ID" & vbCrLf
sConsulta = sConsulta & "   WHERE S.PUB=1 AND S.BAJA=0 AND PET.SOLICITUD IS NULL" & vbCrLf
sConsulta = sConsulta & ") PU ON S.ID=PU.ID" & vbCrLf
sConsulta = sConsulta & "WHERE S.PUB=1'" & vbCrLf
sConsulta = sConsulta & "IF @CERTIFBAJA=0" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL +  ' AND S.BAJA=0 ' " & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL , N'@USU VARCHAR(50)',@USU=@USU" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--*********************************************************************************************************************************************************" & vbCrLf
sConsulta = sConsulta & "--3� SELECT DE MATERIALES DE QA POR CADA PROVEEDOR Y TIPO DE CERTIFICADO  que seleccionamos en la 1� query" & vbCrLf
sConsulta = sConsulta & "--Desde c�digo se dar� valor a las columnas de la 1� SELECT: DEN_MATERIAL y ID_MATERIAL" & vbCrLf
sConsulta = sConsulta & "--*********************************************************************************************************************************************************" & vbCrLf
sConsulta = sConsulta & "IF @SOLOCONTADORES =0 AND @OBTENERTODASRELACIONES=1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "  set @SEL= N'SELECT DISTINCT PR.COD PROVE_COD,M.DEN_' + @IDI + ' DEN_MATERIAL,M.ID ID_MATERIAL, S.ID TIPO_CERTIFICADO '" & vbCrLf
sConsulta = sConsulta & "  SET @SQL=''" & vbCrLf
sConsulta = sConsulta & "  --Solicitados" & vbCrLf
sConsulta = sConsulta & "  IF @ESTADO IS NULL OR NOT(CHARINDEX('2',@ESTADO)=0) OR NOT(CHARINDEX('3',@ESTADO)=0) OR NOT(CHARINDEX('4',@ESTADO)=0) OR NOT(CHARINDEX('5',@ESTADO)=0)" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    SET @SQL = @SEL + @JOIN " & vbCrLf
sConsulta = sConsulta & "   IF @MATERIALES IS NULL AND @REST_PROV_MAT=0" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = @SQL + N' INNER JOIN MATERIAL_QA_CERTIFICADOS MC WITH(NOLOCK) ON MC.TIPO_CERTIFICADO=S.ID'" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N' INNER JOIN MATERIALES_QA M WITH(NOLOCK) ON MC.MATERIAL=M.ID'" & vbCrLf
sConsulta = sConsulta & "    SET @SQL = @SQL + @WHERE   " & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "  --Filtro PDTE CUMPLIMENTAR" & vbCrLf
sConsulta = sConsulta & "  IF @NOMBRE_TABLA <> '' " & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "     IF @ESTADO IS NULL OR NOT(CHARINDEX('2',@ESTADO)=0) OR NOT(CHARINDEX('3',@ESTADO)=0) OR NOT(CHARINDEX('4',@ESTADO)=0) OR NOT(CHARINDEX('5',@ESTADO)=0)" & vbCrLf
sConsulta = sConsulta & "     BEGIN" & vbCrLf
sConsulta = sConsulta & "       IF @SQL<>''" & vbCrLf
sConsulta = sConsulta & "           SET @SQL = @SQL + ' UNION '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = @SQL + @SEL + @JOIN2 " & vbCrLf
sConsulta = sConsulta & "       IF @MATERIALES IS NULL AND @REST_PROV_MAT=0 " & vbCrLf
sConsulta = sConsulta & "           SET @SQL = @SQL + N' INNER JOIN MATERIAL_QA_CERTIFICADOS MC WITH(NOLOCK) ON MC.TIPO_CERTIFICADO=S.ID'" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = @SQL + N' INNER JOIN MATERIALES_QA M WITH(NOLOCK) ON MC.MATERIAL=M.ID'" & vbCrLf
sConsulta = sConsulta & "      SET @SQL = @SQL + @WHERE " & vbCrLf
sConsulta = sConsulta & "     END" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "  --CON SIN SOLICITAR" & vbCrLf
sConsulta = sConsulta & "  IF @ESTADO IS NULL OR NOT(CHARINDEX('1',@ESTADO)=0)     " & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    IF @SQL<>''" & vbCrLf
sConsulta = sConsulta & "      SET @SQL = @SQL + ' UNION '" & vbCrLf
sConsulta = sConsulta & "    SET @SQL = @SQL + @SEL + @JOIN3" & vbCrLf
sConsulta = sConsulta & "    SET @SQL = @SQL + N' INNER JOIN MATERIALES_QA M WITH(NOLOCK) ON MC.MATERIAL=M.ID'" & vbCrLf
sConsulta = sConsulta & "    SET @SQL = @SQL + @WHERE3  " & vbCrLf
sConsulta = sConsulta & "  END       " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   IF @OBTENERTODOSCERTIFICADOS=0" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       --CALIDAD. Estos dos * se quedan pq no es la tabla de bbdd certificados sino una tabla hecha ad hoc mediante WITH CERTIFICADOS AS (...) y en estos puntos suspensivos puede venir filtro.* o no -> no se conocen las columnas" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = ';WITH CERTIFICADOS2 AS (' + @SQL2 + ')" & vbCrLf
sConsulta = sConsulta & "       , TOTCERTIF2 AS (SELECT PROVE_COD, TIPO_CERTIFICADO ,ROW_NUMBER() OVER (ORDER BY ' + @ORDEN + ') AS ROWNUMBER FROM CERTIFICADOS2 WITH (NOLOCK)) " & vbCrLf
sConsulta = sConsulta & "       SELECT TOTCERTIF2.PROVE_COD,TOTCERTIF2.TIPO_CERTIFICADO ,M.DEN_' + @IDI + ' DEN_MATERIAL,M.ID ID_MATERIAL" & vbCrLf
sConsulta = sConsulta & "           FROM TOTCERTIF2 WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN MATERIAL_QA_CERTIFICADOS MC WITH(NOLOCK) ON MC.TIPO_CERTIFICADO=TOTCERTIF2.TIPO_CERTIFICADO" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN MATERIALES_QA M WITH(NOLOCK) ON MC.MATERIAL=M.ID" & vbCrLf
sConsulta = sConsulta & "       WHERE ROWNUMBER BETWEEN ' + CONVERT(VARCHAR(10),@ROW) + ' AND ' + CONVERT(VARCHAR(10),(@ROW + @PAGESIZE - 1))               " & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  EXEC SP_EXECUTESQL @SQL , N'@PER VARCHAR(50),@USU VARCHAR(50),@FSQA_PROX_EXPIRAR INT,@CODPROV NVARCHAR(50),@FECDESDE AS DATE,@FECHASTA AS DATE,@UON1 NVARCHAR(50),@UON2 NVARCHAR(50),@UON3 NVARCHAR(50),@DEP NVARCHAR(50)',@PER=@PER,@USU=@USU,@FSQA_PROX_EXPIRAR=@FSQA_PROX_EXPIRAR,@CODPROV=@CODPROV,@FECDESDE=@FECDESDE,@FECHASTA=@FECHASTA ,@UON1=@UON1,@UON2=@UON2,@UON3=@UON3,@DEP=@DEP" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UON3_COD]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[UON3_COD]" & vbCrLf
    gRDOCon.QueryTimeout = 7200
    ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [DBO].[UON3_COD] (@OLD VARCHAR(50),@NEW VARCHAR(50), @UON1 VARCHAR(50), @UON2 VARCHAR(50))  AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT ON" & vbCrLf
sConsulta = sConsulta & "BEGIN TRANSACTION" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR FOR SELECT COD FROM UON3 WITH(NOLOCK) WHERE UON1=@UON1 AND UON2=@UON2 AND COD=@OLD" & vbCrLf
sConsulta = sConsulta & "OPEN C" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C INTO @VAR_COD" & vbCrLf
sConsulta = sConsulta & "IF @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3_DEP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PER NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3_DEST NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISOR NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CERTIFICADO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE APROBADORES NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PETICIONARIOS NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_ROL NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_ENLACE_CONDICIONES NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_COPIA_ROL NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_COPIA_ENLACE_CONDICION NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_UON NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU_CC_CONTROL NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU_CC_IMPUTACION NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES5_UON NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CENTRO_SM_UON NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON4_DEST NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_PED_IMPUTACION NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PERF_UON NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_CAT_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_GRUPO_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_MEN_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE UON3 SET COD=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND COD=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON4 SET UON3 = @NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON3_DEP SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PER SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE COM SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_UON3 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_UON3 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON3_DEST SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_UON3 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_UON3 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES1_NIV1 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES1_NIV2 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES1_NIV3 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES1_NIV4 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES2_NIV1 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES2_NIV2 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES2_NIV3 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES2_NIV4 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES3_NIV1 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES3_NIV2 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES3_NIV3 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES3_NIV4 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES4_NIV1 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES4_NIV2 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES4_NIV3 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES4_NIV4 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_1 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_2 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_3 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_4 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_1 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_2 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_3 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_4 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON1 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON2 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON3 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON4 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY1 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY2 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY3 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY4 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_PRESCON3 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_PRESCON4 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISOR SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CERTIFICADO SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE APROBADORES SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PETICIONARIOS SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PM_ROL SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PM_COPIA_ROL SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4_UON SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET UON3_APER=@NEW WHERE UON1_APER=@UON1 AND UON2_APER=@UON2 AND UON3_APER=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE USU_CC_CONTROL SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE USU_CC_IMPUTACION SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES5_UON SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CENTRO_SM_UON SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON4_DEST SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE LINEAS_PED_IMPUTACION SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PERF_UON SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_CAT_UON3 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_GRUPO_UON3 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_MEN_UON3 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @LEN_COD_UON1 AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @LEN_COD_UON2 AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @LEN_COD_UON3 AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE C2 CURSOR FOR SELECT (SELECT LONGITUD FROM DIC WITH(NOLOCK) WHERE NOMBRE='UON1') AS L1,(SELECT LONGITUD FROM DIC WITH(NOLOCK) WHERE NOMBRE='UON2') AS L2,(SELECT LONGITUD FROM DIC WITH(NOLOCK) WHERE NOMBRE='UON3') AS L3" & vbCrLf
sConsulta = sConsulta & "OPEN C2" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C2 INTO @LEN_COD_UON1,@LEN_COD_UON2,@LEN_COD_UON3" & vbCrLf
sConsulta = sConsulta & "IF @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "  UPDATE PM_ENLACE_CONDICIONES SET VALOR_TEXT=STUFF(VALOR_TEXT,@LEN_COD_UON1 + @LEN_COD_UON2 + 3,@LEN_COD_UON3,@NEW + SPACE(@LEN_COD_UON3 - LEN(@NEW))) WHERE TIPO_CAMPO=4 AND TIPO_VALOR=2 AND LTRIM(RTRIM(SUBSTRING(VALOR_TEXT,1,@LEN_COD_UON1)))=@UON1 AND LTRIM(RTRIM(SUBSTRING(VALOR_TEXT,@LEN_COD_UON1+2, @LEN_COD_UON2)))=@UON2 AND LTRIM(RTRIM(SUBSTRING(VALOR_TEXT,@LEN_COD_UON1+@LEN_COD_UON2+3, @LEN_COD_UON3)))=@OLD" & vbCrLf
sConsulta = sConsulta & "  UPDATE PM_COPIA_ENLACE_CONDICION SET VALOR_TEXT=STUFF(VALOR_TEXT,@LEN_COD_UON1 + @LEN_COD_UON2 + 3,@LEN_COD_UON3,@NEW + SPACE(@LEN_COD_UON3 - LEN(@NEW))) WHERE TIPO_CAMPO=4 AND TIPO_VALOR=2 AND LTRIM(RTRIM(SUBSTRING(VALOR_TEXT,1,@LEN_COD_UON1)))=@UON1 AND LTRIM(RTRIM(SUBSTRING(VALOR_TEXT,@LEN_COD_UON1+2, @LEN_COD_UON2)))=@UON2 AND LTRIM(RTRIM(SUBSTRING(VALOR_TEXT,@LEN_COD_UON1+@LEN_COD_UON2+3, @LEN_COD_UON3)))=@OLD" & vbCrLf
sConsulta = sConsulta & "CLOSE C2" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C2" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--INICIO ACTUALIZACION COPIA_CAMPO, COPIA_LINEA_DESGLOSE, INSTANCIA_CENTROS_PARTIDAS, PM_CONTR_FILTRO_USU_BUSQUEDA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--ACTUALIZAR DATOS DE UONS EN LOS CENTROS Y PARTIDAS GRABADOS EN " & vbCrLf
sConsulta = sConsulta & "   --COPIA_CAMPO" & vbCrLf
sConsulta = sConsulta & "   --COPIA_LINEA_DESGLOSE" & vbCrLf
sConsulta = sConsulta & "   --INSTANCIA_CENTROS_DESGLOSE --> TABLA EN LA QUE SE HACE COPIA DE LOS DATOS DE CENTROS Y PARTIDAS DE COPIA_CAMPO Y COPIA_LINEA_DESGLOSE PARA ACELERAR FILTRADOS EN EL BUSCADOR DEL VISOR DE CONTRATOS" & vbCrLf
sConsulta = sConsulta & "   --PM_CONTR_FILTRO_USU_BUSQUEDA --> TABLA EN LA QUE SE HACE COPIA DE LOS VALORES POR DEFECTO PARA LOS FILTROS DEL BUSCADOR DEL VISOR DE CONTRATOS, QUE INCLUYE, ENTRE OTRAS COSAS CENTROS Y PARTIDAS." & vbCrLf
sConsulta = sConsulta & "--HAY QUE MODIFICARLO PORQUE EL MODO EN QUE SE GRABAN CENTROS Y PARTIDAS ES:" & vbCrLf
sConsulta = sConsulta & "--CENTROS: UON1#UON2#... HASTA EL NIVEL AL QUE EST� VINCULADO EL CENTRO" & vbCrLf
sConsulta = sConsulta & "--PARTIDAS: UON1#UON2#...#PRESN: UONS HASTA EL NIVEL AL QUE EST� VINCULADA LA PARTIDA Y PRESN, DONDE N ES EL NIVEL DE IMPUTACION QUE SE INDICA EN PARGEN_SM.IMP_NIVEL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- QU� ES MEJOR PARA BUSCAR LAS UON EN LOS CAMPOS, GUIARSE POR LA LONGITUD DEL C�DIGO UON O POR EL SEPARADOR #: " & vbCrLf
sConsulta = sConsulta & "-- DADO QUE SE PUEDE CAMBIAR LA LONGITUD EN LA TABLA DIC Y QUE EN LAS 4 TABLAS AL GUARDAR LOS DATOS NO SE TIENE EN CUENTA ESA LONGITUD, USAREMOS #" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--EN LAS PARTIDAS, EL �LTIMO VALOR ES SIEMPRE UNA PARTIDA PRESUPUESTARIA. AL MENOS HABR� SIEMPRE UN UON (UON1) PERO A PARTIR DE LOS UON2, PUEDE QUE NO HAYA, POR LO QUE TENEMOS QUE ASEGURARNOS DE NO CONFUNDIR LA PARTIDA CON UN UON AL REEMPLAZAR: VAMOS A IGNORAR LA PARTIDA AL FILTRAR" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- COPIA_CAMPO" & vbCrLf
sConsulta = sConsulta & "--     CENTROS" & vbCrLf
sConsulta = sConsulta & "DECLARE @SEPARADOR VARCHAR(3) = '#'" & vbCrLf
sConsulta = sConsulta & "UPDATE CC SET VALOR_TEXT = COALESCE(STUFF(CC.VALOR_TEXT, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1, LEN(@OLD), @NEW) , VALOR_TEXT)" & vbCrLf
sConsulta = sConsulta & "FROM COPIA_CAMPO CC WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_CAMPO_DEF CD WITH(NOLOCK) ON CC.COPIA_CAMPO_DEF=CD.ID" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "   CD.TIPO_CAMPO_GS = 129 AND " & vbCrLf
sConsulta = sConsulta & "   CC.VALOR_TEXT IS NOT NULL AND " & vbCrLf
sConsulta = sConsulta & "   SUBSTRING(CC.VALOR_TEXT, " & vbCrLf
sConsulta = sConsulta & "             1, " & vbCrLf
sConsulta = sConsulta & "             CASE WHEN " & vbCrLf
sConsulta = sConsulta & "               CHARINDEX(@SEPARADOR,CC.VALOR_TEXT, CHARINDEX(@SEPARADOR,CC.VALOR_TEXT, CHARINDEX (@SEPARADOR,CC.VALOR_TEXT) + 1) + 1)=0 THEN " & vbCrLf
sConsulta = sConsulta & "                   LEN(CC.VALOR_TEXT) " & vbCrLf
sConsulta = sConsulta & "               ELSE " & vbCrLf
sConsulta = sConsulta & "                   CHARINDEX(@SEPARADOR,CC.VALOR_TEXT, CHARINDEX(@SEPARADOR,CC.VALOR_TEXT, CHARINDEX (@SEPARADOR,CC.VALOR_TEXT) + 1) + 1)-1" & vbCrLf
sConsulta = sConsulta & "               END" & vbCrLf
sConsulta = sConsulta & "           )=@UON1 + @SEPARADOR + @UON2 + @SEPARADOR + @OLD" & vbCrLf
sConsulta = sConsulta & "--     PARTIDAS" & vbCrLf
sConsulta = sConsulta & "UPDATE CC SET VALOR_TEXT = COALESCE(STUFF(CC.VALOR_TEXT, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1, LEN(@OLD), @NEW) , VALOR_TEXT)" & vbCrLf
sConsulta = sConsulta & "FROM COPIA_CAMPO CC WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_CAMPO_DEF CD WITH(NOLOCK) ON CC.COPIA_CAMPO_DEF=CD.ID" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "   CD.TIPO_CAMPO_GS = 130 AND " & vbCrLf
sConsulta = sConsulta & "   CC.VALOR_TEXT IS NOT NULL AND " & vbCrLf
sConsulta = sConsulta & "   SUBSTRING(SUBSTRING(CC.VALOR_TEXT, 1, LEN(CC.VALOR_TEXT) - CHARINDEX(@SEPARADOR, REVERSE(CC.VALOR_TEXT))),  --QUITAMOS LA PARTIDA PRES" & vbCrLf
sConsulta = sConsulta & "             1, " & vbCrLf
sConsulta = sConsulta & "             CASE WHEN " & vbCrLf
sConsulta = sConsulta & "               CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                         SUBSTRING(CC.VALOR_TEXT, 1, LEN(CC.VALOR_TEXT) - CHARINDEX(@SEPARADOR, REVERSE(CC.VALOR_TEXT))), " & vbCrLf
sConsulta = sConsulta & "                         CHARINDEX(@SEPARADOR,CC.VALOR_TEXT, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1)" & vbCrLf
sConsulta = sConsulta & "                         )=0 THEN " & vbCrLf
sConsulta = sConsulta & "                   LEN(SUBSTRING(CC.VALOR_TEXT, 1, LEN(CC.VALOR_TEXT) - CHARINDEX(@SEPARADOR, REVERSE(CC.VALOR_TEXT))))" & vbCrLf
sConsulta = sConsulta & "               ELSE " & vbCrLf
sConsulta = sConsulta & "                   CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                             SUBSTRING(CC.VALOR_TEXT, 1, LEN(CC.VALOR_TEXT) - CHARINDEX(@SEPARADOR, REVERSE(CC.VALOR_TEXT))), " & vbCrLf
sConsulta = sConsulta & "                             CHARINDEX(@SEPARADOR,CC.VALOR_TEXT, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1))-1" & vbCrLf
sConsulta = sConsulta & "               END" & vbCrLf
sConsulta = sConsulta & "           )=@UON1 + @SEPARADOR + @UON2 + @SEPARADOR + @OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- COPIA_LINEA_DESGLOSE" & vbCrLf
sConsulta = sConsulta & "--     CENTROS" & vbCrLf
sConsulta = sConsulta & "UPDATE CLD SET VALOR_TEXT = COALESCE(STUFF(CLD.VALOR_TEXT, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1, LEN(@OLD), @NEW) , CLD.VALOR_TEXT)" & vbCrLf
sConsulta = sConsulta & "FROM COPIA_LINEA_DESGLOSE CLD WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_CAMPO CC WITH (NOLOCK) ON CLD.CAMPO_HIJO = CC.ID " & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_CAMPO_DEF CCD WITH (NOLOCK) ON CC.COPIA_CAMPO_DEF = CCD.ID" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "   CCD.TIPO_CAMPO_GS = 129 AND " & vbCrLf
sConsulta = sConsulta & "   CLD.VALOR_TEXT IS NOT NULL AND " & vbCrLf
sConsulta = sConsulta & "   SUBSTRING(CLD.VALOR_TEXT, " & vbCrLf
sConsulta = sConsulta & "             1, " & vbCrLf
sConsulta = sConsulta & "             CASE WHEN " & vbCrLf
sConsulta = sConsulta & "               CHARINDEX(@SEPARADOR,CLD.VALOR_TEXT, CHARINDEX(@SEPARADOR,CLD.VALOR_TEXT, CHARINDEX (@SEPARADOR,CLD.VALOR_TEXT) + 1) + 1)=0 THEN " & vbCrLf
sConsulta = sConsulta & "                   LEN(CLD.VALOR_TEXT) " & vbCrLf
sConsulta = sConsulta & "               ELSE " & vbCrLf
sConsulta = sConsulta & "                   CHARINDEX(@SEPARADOR,CLD.VALOR_TEXT, CHARINDEX(@SEPARADOR,CLD.VALOR_TEXT, CHARINDEX (@SEPARADOR,CLD.VALOR_TEXT) + 1) + 1)-1" & vbCrLf
sConsulta = sConsulta & "               END" & vbCrLf
sConsulta = sConsulta & "           )=@UON1 + @SEPARADOR + @UON2 + @SEPARADOR + @OLD" & vbCrLf
sConsulta = sConsulta & "--     PARTIDAS" & vbCrLf
sConsulta = sConsulta & "UPDATE CLD SET VALOR_TEXT = COALESCE(STUFF(CLD.VALOR_TEXT, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1, LEN(@OLD), @NEW) , CLD.VALOR_TEXT)" & vbCrLf
sConsulta = sConsulta & "FROM COPIA_LINEA_DESGLOSE CLD WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_CAMPO CC WITH (NOLOCK) ON CLD.CAMPO_HIJO = CC.ID " & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_CAMPO_DEF CCD WITH (NOLOCK) ON CC.COPIA_CAMPO_DEF = CCD.ID" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "   CCD.TIPO_CAMPO_GS = 130 AND " & vbCrLf
sConsulta = sConsulta & "   CLD.VALOR_TEXT IS NOT NULL AND " & vbCrLf
sConsulta = sConsulta & "   SUBSTRING(SUBSTRING(CLD.VALOR_TEXT, 1, LEN(CLD.VALOR_TEXT) - CHARINDEX(@SEPARADOR, REVERSE(CLD.VALOR_TEXT))),  --QUITAMOS LA PARTIDA PRES" & vbCrLf
sConsulta = sConsulta & "             1, " & vbCrLf
sConsulta = sConsulta & "             CASE WHEN " & vbCrLf
sConsulta = sConsulta & "               CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                         SUBSTRING(CLD.VALOR_TEXT, 1, LEN(CLD.VALOR_TEXT) - CHARINDEX(@SEPARADOR, REVERSE(CLD.VALOR_TEXT))), " & vbCrLf
sConsulta = sConsulta & "                         CHARINDEX(@SEPARADOR,CLD.VALOR_TEXT, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1)" & vbCrLf
sConsulta = sConsulta & "                         )=0 THEN " & vbCrLf
sConsulta = sConsulta & "                   LEN(SUBSTRING(CLD.VALOR_TEXT, 1, LEN(CLD.VALOR_TEXT) - CHARINDEX(@SEPARADOR, REVERSE(CLD.VALOR_TEXT))))" & vbCrLf
sConsulta = sConsulta & "               ELSE " & vbCrLf
sConsulta = sConsulta & "                   CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                             SUBSTRING(CLD.VALOR_TEXT, 1, LEN(CLD.VALOR_TEXT) - CHARINDEX(@SEPARADOR, REVERSE(CLD.VALOR_TEXT))), " & vbCrLf
sConsulta = sConsulta & "                             CHARINDEX(@SEPARADOR,CLD.VALOR_TEXT, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1))-1" & vbCrLf
sConsulta = sConsulta & "               END" & vbCrLf
sConsulta = sConsulta & "           )=@UON1 + @SEPARADOR + @UON2 + @SEPARADOR + @OLD" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "-- INSTANCIA_CENTROS_PARTIDAS" & vbCrLf
sConsulta = sConsulta & "--     CENTROS" & vbCrLf
sConsulta = sConsulta & "UPDATE ICP SET VALOR_TEXT = COALESCE(STUFF(ICP.VALOR_TEXT, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1, LEN(@OLD), @NEW) , VALOR_TEXT)" & vbCrLf
sConsulta = sConsulta & "FROM INSTANCIA_CENTROS_PARTIDAS ICP WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "ICP.TIPO_CAMPO_GS = 129 AND " & vbCrLf
sConsulta = sConsulta & "ICP.VALOR_TEXT IS NOT NULL AND" & vbCrLf
sConsulta = sConsulta & "SUBSTRING(ICP.VALOR_TEXT, " & vbCrLf
sConsulta = sConsulta & "         1, " & vbCrLf
sConsulta = sConsulta & "         CASE WHEN " & vbCrLf
sConsulta = sConsulta & "           CHARINDEX(@SEPARADOR,ICP.VALOR_TEXT, CHARINDEX(@SEPARADOR,ICP.VALOR_TEXT, CHARINDEX (@SEPARADOR,ICP.VALOR_TEXT) + 1) + 1)=0 THEN " & vbCrLf
sConsulta = sConsulta & "               LEN(ICP.VALOR_TEXT) " & vbCrLf
sConsulta = sConsulta & "           ELSE " & vbCrLf
sConsulta = sConsulta & "               CHARINDEX(@SEPARADOR,ICP.VALOR_TEXT, CHARINDEX(@SEPARADOR,ICP.VALOR_TEXT, CHARINDEX (@SEPARADOR,ICP.VALOR_TEXT) + 1) + 1)-1" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "       )=@UON1 + @SEPARADOR + @UON2 + @SEPARADOR + @OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--     PARTIDAS" & vbCrLf
sConsulta = sConsulta & "UPDATE ICP SET VALOR_TEXT = COALESCE(STUFF(ICP.VALOR_TEXT, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1, LEN(@OLD), @NEW) , VALOR_TEXT)" & vbCrLf
sConsulta = sConsulta & "FROM INSTANCIA_CENTROS_PARTIDAS ICP WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "ICP.TIPO_CAMPO_GS  = 130 AND " & vbCrLf
sConsulta = sConsulta & "ICP.VALOR_TEXT IS NOT NULL AND" & vbCrLf
sConsulta = sConsulta & "SUBSTRING(SUBSTRING(ICP.VALOR_TEXT, 1, LEN(ICP.VALOR_TEXT) - CHARINDEX(@SEPARADOR, REVERSE(ICP.VALOR_TEXT))),  --QUITAMOS LA PARTIDA PRES" & vbCrLf
sConsulta = sConsulta & "         1, " & vbCrLf
sConsulta = sConsulta & "         CASE WHEN " & vbCrLf
sConsulta = sConsulta & "           CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                     SUBSTRING(ICP.VALOR_TEXT, 1, LEN(ICP.VALOR_TEXT) - CHARINDEX(@SEPARADOR, REVERSE(ICP.VALOR_TEXT))), " & vbCrLf
sConsulta = sConsulta & "                     CHARINDEX(@SEPARADOR,ICP.VALOR_TEXT, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1)" & vbCrLf
sConsulta = sConsulta & "                     )=0 THEN " & vbCrLf
sConsulta = sConsulta & "               LEN(SUBSTRING(ICP.VALOR_TEXT, 1, LEN(ICP.VALOR_TEXT) - CHARINDEX(@SEPARADOR, REVERSE(ICP.VALOR_TEXT))))" & vbCrLf
sConsulta = sConsulta & "           ELSE " & vbCrLf
sConsulta = sConsulta & "               CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                         SUBSTRING(ICP.VALOR_TEXT, 1, LEN(ICP.VALOR_TEXT) - CHARINDEX(@SEPARADOR, REVERSE(ICP.VALOR_TEXT))), " & vbCrLf
sConsulta = sConsulta & "                         CHARINDEX(@SEPARADOR,ICP.VALOR_TEXT, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1))-1" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "       )=@UON1 + @SEPARADOR + @UON2 + @SEPARADOR + @OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- PM_CONTR_FILTRO_USU_BUSQUEDA" & vbCrLf
sConsulta = sConsulta & "UPDATE P SET CENTRO_COSTE = COALESCE(STUFF(CENTRO_COSTE, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1, LEN(@OLD), @NEW) , CENTRO_COSTE)" & vbCrLf
sConsulta = sConsulta & "FROM PM_CONTR_FILTRO_USU_BUSQUEDA P WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "P.CENTRO_COSTE IS NOT NULL AND" & vbCrLf
sConsulta = sConsulta & "SUBSTRING(P.CENTRO_COSTE, " & vbCrLf
sConsulta = sConsulta & "         1, " & vbCrLf
sConsulta = sConsulta & "         CASE WHEN " & vbCrLf
sConsulta = sConsulta & "           CHARINDEX(@SEPARADOR,P.CENTRO_COSTE, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1)=0 THEN " & vbCrLf
sConsulta = sConsulta & "               LEN(P.CENTRO_COSTE) " & vbCrLf
sConsulta = sConsulta & "           ELSE " & vbCrLf
sConsulta = sConsulta & "               CHARINDEX(@SEPARADOR,P.CENTRO_COSTE, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1)-1" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "       )=@UON1 + @SEPARADOR + @UON2 + @SEPARADOR + @OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE P SET PARTIDA1 = COALESCE(STUFF(PARTIDA1, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1, LEN(@OLD), @NEW) , PARTIDA1)" & vbCrLf
sConsulta = sConsulta & "FROM PM_CONTR_FILTRO_USU_BUSQUEDA P WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "P.PARTIDA1 IS NOT NULL AND" & vbCrLf
sConsulta = sConsulta & "SUBSTRING(SUBSTRING(P.PARTIDA1, 1, LEN(P.PARTIDA1) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA1))),  --QUITAMOS LA PARTIDA PRES" & vbCrLf
sConsulta = sConsulta & "         1, " & vbCrLf
sConsulta = sConsulta & "         CASE WHEN " & vbCrLf
sConsulta = sConsulta & "           CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                     SUBSTRING(P.PARTIDA1, 1, LEN(P.PARTIDA1) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA1))), " & vbCrLf
sConsulta = sConsulta & "                     CHARINDEX(@SEPARADOR,P.PARTIDA1, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1)" & vbCrLf
sConsulta = sConsulta & "                     )=0 THEN " & vbCrLf
sConsulta = sConsulta & "               LEN(SUBSTRING(P.PARTIDA1, 1, LEN(P.PARTIDA1) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA1))))" & vbCrLf
sConsulta = sConsulta & "           ELSE " & vbCrLf
sConsulta = sConsulta & "               CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                         SUBSTRING(P.PARTIDA1, 1, LEN(P.PARTIDA1) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA1))), " & vbCrLf
sConsulta = sConsulta & "                         CHARINDEX(@SEPARADOR,P.PARTIDA1, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1))-1" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "       )=@UON1 + @SEPARADOR + @UON2 + @SEPARADOR + @OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE P SET PARTIDA2 = COALESCE(STUFF(PARTIDA2, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1, LEN(@OLD), @NEW) , PARTIDA2)" & vbCrLf
sConsulta = sConsulta & "FROM PM_CONTR_FILTRO_USU_BUSQUEDA P WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "P.PARTIDA2 IS NOT NULL AND" & vbCrLf
sConsulta = sConsulta & "SUBSTRING(SUBSTRING(P.PARTIDA2, 1, LEN(P.PARTIDA2) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA2))),  --QUITAMOS LA PARTIDA PRES" & vbCrLf
sConsulta = sConsulta & "         1, " & vbCrLf
sConsulta = sConsulta & "         CASE WHEN " & vbCrLf
sConsulta = sConsulta & "           CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                     SUBSTRING(P.PARTIDA2, 1, LEN(P.PARTIDA2) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA2))), " & vbCrLf
sConsulta = sConsulta & "                     CHARINDEX(@SEPARADOR,P.PARTIDA2, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1)" & vbCrLf
sConsulta = sConsulta & "                     )=0 THEN " & vbCrLf
sConsulta = sConsulta & "               LEN(SUBSTRING(P.PARTIDA2, 1, LEN(P.PARTIDA2) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA2))))" & vbCrLf
sConsulta = sConsulta & "           ELSE " & vbCrLf
sConsulta = sConsulta & "               CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                         SUBSTRING(P.PARTIDA2, 1, LEN(P.PARTIDA2) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA2))), " & vbCrLf
sConsulta = sConsulta & "                         CHARINDEX(@SEPARADOR,P.PARTIDA2, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1))-1" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "       )=@UON1 + @SEPARADOR + @UON2 + @SEPARADOR + @OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE P SET PARTIDA3 = COALESCE(STUFF(PARTIDA3, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1, LEN(@OLD), @NEW) , PARTIDA3)" & vbCrLf
sConsulta = sConsulta & "FROM PM_CONTR_FILTRO_USU_BUSQUEDA P WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "P.PARTIDA3 IS NOT NULL AND" & vbCrLf
sConsulta = sConsulta & "SUBSTRING(SUBSTRING(P.PARTIDA3, 1, LEN(P.PARTIDA3) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA3))),  --QUITAMOS LA PARTIDA PRES" & vbCrLf
sConsulta = sConsulta & "         1, " & vbCrLf
sConsulta = sConsulta & "         CASE WHEN " & vbCrLf
sConsulta = sConsulta & "           CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                     SUBSTRING(P.PARTIDA3, 1, LEN(P.PARTIDA3) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA3))), " & vbCrLf
sConsulta = sConsulta & "                     CHARINDEX(@SEPARADOR,P.PARTIDA3, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1)" & vbCrLf
sConsulta = sConsulta & "                     )=0 THEN " & vbCrLf
sConsulta = sConsulta & "               LEN(SUBSTRING(P.PARTIDA3, 1, LEN(P.PARTIDA3) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA3))))" & vbCrLf
sConsulta = sConsulta & "           ELSE " & vbCrLf
sConsulta = sConsulta & "               CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                         SUBSTRING(P.PARTIDA3, 1, LEN(P.PARTIDA3) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA3))), " & vbCrLf
sConsulta = sConsulta & "                         CHARINDEX(@SEPARADOR,P.PARTIDA3, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1))-1" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "       )=@UON1 + @SEPARADOR + @UON2 + @SEPARADOR + @OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE P SET PARTIDA4 = COALESCE(STUFF(PARTIDA4, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1, LEN(@OLD), @NEW) , PARTIDA4)" & vbCrLf
sConsulta = sConsulta & "FROM PM_CONTR_FILTRO_USU_BUSQUEDA P WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "P.PARTIDA4 IS NOT NULL AND" & vbCrLf
sConsulta = sConsulta & "SUBSTRING(SUBSTRING(P.PARTIDA4, 1, LEN(P.PARTIDA4) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA4))),  --QUITAMOS LA PARTIDA PRES" & vbCrLf
sConsulta = sConsulta & "         1, " & vbCrLf
sConsulta = sConsulta & "         CASE WHEN " & vbCrLf
sConsulta = sConsulta & "           CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                     SUBSTRING(P.PARTIDA4, 1, LEN(P.PARTIDA4) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA4))), " & vbCrLf
sConsulta = sConsulta & "                     CHARINDEX(@SEPARADOR,P.PARTIDA4, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1)" & vbCrLf
sConsulta = sConsulta & "                     )=0 THEN " & vbCrLf
sConsulta = sConsulta & "               LEN(SUBSTRING(P.PARTIDA4, 1, LEN(P.PARTIDA4) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA4))))" & vbCrLf
sConsulta = sConsulta & "           ELSE " & vbCrLf
sConsulta = sConsulta & "               CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                         SUBSTRING(P.PARTIDA4, 1, LEN(P.PARTIDA4) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA4))), " & vbCrLf
sConsulta = sConsulta & "                         CHARINDEX(@SEPARADOR,P.PARTIDA4, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1))-1" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "       )=@UON1 + @SEPARADOR + @UON2 + @SEPARADOR + @OLD" & vbCrLf
sConsulta = sConsulta & "--FIN ACTUALIZACION COPIA_CAMPO, COPIA_LINEA_DESGLOSE, INSTANCIA_CENTROS_PARTIDAS, PM_CONTR_FILTRO_USU_BUSQUEDA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3_DEP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PER CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3_DEST CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISOR CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CERTIFICADO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE APROBADORES CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PETICIONARIOS CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_ROL CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_ENLACE_CONDICIONES CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_COPIA_ROL CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_COPIA_ENLACE_CONDICION CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_UON CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU_CC_CONTROL CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU_CC_IMPUTACION CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES5_UON CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CENTRO_SM_UON CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON4_DEST CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_PED_IMPUTACION CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PERF_UON CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_CAT_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_GRUPO_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_MEN_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "CLOSE C" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "COMMIT TRANSACTION" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT OFF" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UON2_COD]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[UON2_COD]" & vbCrLf
    gRDOCon.QueryTimeout = 7200
    ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE UON2_COD (@OLD VARCHAR(50),@NEW VARCHAR(50), @UON1 VARCHAR(50))  AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT ON" & vbCrLf
sConsulta = sConsulta & "BEGIN TRANSACTION" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR FOR SELECT COD FROM UON2 WITH(NOLOCK) WHERE UON1=@UON1 AND COD=@OLD" & vbCrLf
sConsulta = sConsulta & "OPEN C" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C INTO @VAR_COD" & vbCrLf
sConsulta = sConsulta & "IF @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON2_DEP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3_DEP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PER NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON2_DEST NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3_DEST NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISOR NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CERTIFICADO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE APROBADORES NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PETICIONARIOS NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_ROL NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_ENLACE_CONDICIONES NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_COPIA_ROL NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_COPIA_ENLACE_CONDICION NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_UON NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU_CC_CONTROL NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU_CC_IMPUTACION NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES5_UON NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CENTRO_SM_UON NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON4_DEST NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_PED_IMPUTACION NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PERF_UON NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_CAT_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_GRUPO_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_MEN_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE UON2 SET COD=@NEW WHERE UON1=@UON1 AND COD=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON3 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON4 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON2_DEP SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON3_DEP SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PER SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE COM SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_UON2 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_UON3 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_UON2 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_UON3 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON2_DEST SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON3_DEST SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_UON2 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_UON3 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_UON2 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_UON3 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_1 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_2 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_3 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_4 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_1 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_2 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_3 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_4 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON1 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON2 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON3 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON4 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY1 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY2 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY3 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY4 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_PRESCON3 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_PRESCON4 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES1_NIV1 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES1_NIV2 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES1_NIV3 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES1_NIV4 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES2_NIV1 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES2_NIV2 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES2_NIV3 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES2_NIV4 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES3_NIV1 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES3_NIV2 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES3_NIV3 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES3_NIV4 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES4_NIV1 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES4_NIV2 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES4_NIV3 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES4_NIV4 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISOR SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CERTIFICADO SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE APROBADORES SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PETICIONARIOS SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PM_ROL SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PM_COPIA_ROL SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4_UON SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET UON2_APER=@NEW WHERE UON1_APER=@UON1 AND UON2_APER=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE USU_CC_CONTROL SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE USU_CC_IMPUTACION SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES5_UON SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CENTRO_SM_UON SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON4_DEST SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE LINEAS_PED_IMPUTACION SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PERF_UON SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_CAT_UON2 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_GRUPO_UON2 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_MEN_UON2 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @LEN_COD_UON1 AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @LEN_COD_UON2 AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE C2 CURSOR FOR SELECT (SELECT LONGITUD FROM DIC WITH(NOLOCK) WHERE NOMBRE='UON1') AS L1,(SELECT LONGITUD FROM DIC WITH(NOLOCK) WHERE NOMBRE='UON2') AS L2" & vbCrLf
sConsulta = sConsulta & "OPEN C2" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C2 INTO @LEN_COD_UON1,@LEN_COD_UON2" & vbCrLf
sConsulta = sConsulta & "IF @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "  UPDATE PM_ENLACE_CONDICIONES SET VALOR_TEXT=STUFF(VALOR_TEXT,@LEN_COD_UON1 + 2,@LEN_COD_UON2,@NEW + SPACE(@LEN_COD_UON2 - LEN(@NEW))) WHERE TIPO_CAMPO=4 AND TIPO_VALOR=2 AND LTRIM(RTRIM(SUBSTRING(VALOR_TEXT,1,@LEN_COD_UON1)))=@UON1 AND LTRIM(RTRIM(SUBSTRING(VALOR_TEXT,@LEN_COD_UON1+2, @LEN_COD_UON2)))=@OLD" & vbCrLf
sConsulta = sConsulta & "  UPDATE PM_COPIA_ENLACE_CONDICION SET VALOR_TEXT=STUFF(VALOR_TEXT,@LEN_COD_UON1 + 2,@LEN_COD_UON2,@NEW + SPACE(@LEN_COD_UON2 - LEN(@NEW))) WHERE TIPO_CAMPO=4 AND TIPO_VALOR=2 AND LTRIM(RTRIM(SUBSTRING(VALOR_TEXT,1,@LEN_COD_UON1)))=@UON1 AND LTRIM(RTRIM(SUBSTRING(VALOR_TEXT,@LEN_COD_UON1+2, @LEN_COD_UON2)))=@OLD" & vbCrLf
sConsulta = sConsulta & "CLOSE C2" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C2" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--INICIO ACTUALIZACION COPIA_CAMPO, COPIA_LINEA_DESGLOSE, INSTANCIA_CENTROS_PARTIDAS, PM_CONTR_FILTRO_USU_BUSQUEDA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--ACTUALIZAR DATOS DE UONS EN LOS CENTROS Y PARTIDAS GRABADOS EN " & vbCrLf
sConsulta = sConsulta & "   --COPIA_CAMPO" & vbCrLf
sConsulta = sConsulta & "   --COPIA_LINEA_DESGLOSE" & vbCrLf
sConsulta = sConsulta & "   --INSTANCIA_CENTROS_DESGLOSE --> TABLA EN LA QUE SE HACE COPIA DE LOS DATOS DE CENTROS Y PARTIDAS DE COPIA_CAMPO Y COPIA_LINEA_DESGLOSE PARA ACELERAR FILTRADOS EN EL BUSCADOR DEL VISOR DE CONTRATOS" & vbCrLf
sConsulta = sConsulta & "   --PM_CONTR_FILTRO_USU_BUSQUEDA --> TABLA EN LA QUE SE HACE COPIA DE LOS VALORES POR DEFECTO PARA LOS FILTROS DEL BUSCADOR DEL VISOR DE CONTRATOS, QUE INCLUYE, ENTRE OTRAS COSAS CENTROS Y PARTIDAS." & vbCrLf
sConsulta = sConsulta & "--HAY QUE MODIFICARLO PORQUE EL MODO EN QUE SE GRABAN CENTROS Y PARTIDAS ES:" & vbCrLf
sConsulta = sConsulta & "--CENTROS: UON1#UON2#... HASTA EL NIVEL AL QUE EST� VINCULADO EL CENTRO" & vbCrLf
sConsulta = sConsulta & "--PARTIDAS: UON1#UON2#...#PRESN: UONS HASTA EL NIVEL AL QUE EST� VINCULADA LA PARTIDA Y PRESN, DONDE N ES EL NIVEL DE IMPUTACION QUE SE INDICA EN PARGEN_SM.IMP_NIVEL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- QU� ES MEJOR PARA BUSCAR LAS UON EN LOS CAMPOS, GUIARSE POR LA LONGITUD DEL C�DIGO UON O POR EL SEPARADOR #: " & vbCrLf
sConsulta = sConsulta & "-- DADO QUE SE PUEDE CAMBIAR LA LONGITUD EN LA TABLA DIC Y QUE EN LAS 4 TABLAS AL GUARDAR LOS DATOS NO SE TIENE EN CUENTA ESA LONGITUD, USAREMOS #" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--EN LAS PARTIDAS, EL �LTIMO VALOR ES SIEMPRE UNA PARTIDA PRESUPUESTARIA. AL MENOS HABR� SIEMPRE UN UON (UON1) PERO A PARTIR DE LOS UON2, PUEDE QUE NO HAYA, POR LO QUE TENEMOS QUE ASEGURARNOS DE NO CONFUNDIR LA PARTIDA CON UN UON AL REEMPLAZAR: VAMOS A IGNORAR LA PARTIDA AL FILTRAR" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- COPIA_CAMPO" & vbCrLf
sConsulta = sConsulta & "--     CENTROS" & vbCrLf
sConsulta = sConsulta & "DECLARE @SEPARADOR VARCHAR(3) = '#'" & vbCrLf
sConsulta = sConsulta & "UPDATE CC SET VALOR_TEXT = COALESCE(STUFF(CC.VALOR_TEXT, LEN(@UON1 + @SEPARADOR) + 1, LEN(@OLD), @NEW) , VALOR_TEXT)" & vbCrLf
sConsulta = sConsulta & "FROM COPIA_CAMPO CC WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_CAMPO_DEF CD WITH(NOLOCK) ON CC.COPIA_CAMPO_DEF=CD.ID" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "   CD.TIPO_CAMPO_GS = 129 AND " & vbCrLf
sConsulta = sConsulta & "   CC.VALOR_TEXT IS NOT NULL AND " & vbCrLf
sConsulta = sConsulta & "   SUBSTRING(CC.VALOR_TEXT, " & vbCrLf
sConsulta = sConsulta & "             1, " & vbCrLf
sConsulta = sConsulta & "             CASE WHEN " & vbCrLf
sConsulta = sConsulta & "               CHARINDEX(@SEPARADOR,CC.VALOR_TEXT, CHARINDEX(@SEPARADOR,CC.VALOR_TEXT) + 1)=0 THEN " & vbCrLf
sConsulta = sConsulta & "                   LEN(CC.VALOR_TEXT) " & vbCrLf
sConsulta = sConsulta & "               ELSE " & vbCrLf
sConsulta = sConsulta & "                   CHARINDEX(@SEPARADOR,CC.VALOR_TEXT, CHARINDEX(@SEPARADOR,CC.VALOR_TEXT) + 1)-1" & vbCrLf
sConsulta = sConsulta & "               END" & vbCrLf
sConsulta = sConsulta & "           )=@UON1 + @SEPARADOR + @OLD" & vbCrLf
sConsulta = sConsulta & "--     PARTIDAS" & vbCrLf
sConsulta = sConsulta & "UPDATE CC SET VALOR_TEXT = COALESCE(STUFF(CC.VALOR_TEXT, LEN(@UON1 + @SEPARADOR) + 1, LEN(@OLD), @NEW) , VALOR_TEXT)" & vbCrLf
sConsulta = sConsulta & "FROM COPIA_CAMPO CC WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_CAMPO_DEF CD WITH(NOLOCK) ON CC.COPIA_CAMPO_DEF=CD.ID" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "   CD.TIPO_CAMPO_GS = 130 AND " & vbCrLf
sConsulta = sConsulta & "   CC.VALOR_TEXT IS NOT NULL AND " & vbCrLf
sConsulta = sConsulta & "   SUBSTRING(SUBSTRING(CC.VALOR_TEXT, 1, LEN(CC.VALOR_TEXT) - CHARINDEX(@SEPARADOR, REVERSE(CC.VALOR_TEXT))),  --QUITAMOS LA PARTIDA PRES" & vbCrLf
sConsulta = sConsulta & "             1, " & vbCrLf
sConsulta = sConsulta & "             CASE WHEN " & vbCrLf
sConsulta = sConsulta & "               CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                         SUBSTRING(CC.VALOR_TEXT, 1, LEN(CC.VALOR_TEXT) - CHARINDEX(@SEPARADOR, REVERSE(CC.VALOR_TEXT))), " & vbCrLf
sConsulta = sConsulta & "                         CHARINDEX(@SEPARADOR,CC.VALOR_TEXT, LEN(@UON1 + @SEPARADOR) + 1)" & vbCrLf
sConsulta = sConsulta & "                         )=0 THEN " & vbCrLf
sConsulta = sConsulta & "                   LEN(SUBSTRING(CC.VALOR_TEXT, 1, LEN(CC.VALOR_TEXT) - CHARINDEX(@SEPARADOR, REVERSE(CC.VALOR_TEXT))))" & vbCrLf
sConsulta = sConsulta & "               ELSE " & vbCrLf
sConsulta = sConsulta & "                   CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                             SUBSTRING(CC.VALOR_TEXT, 1, LEN(CC.VALOR_TEXT) - CHARINDEX(@SEPARADOR, REVERSE(CC.VALOR_TEXT))), " & vbCrLf
sConsulta = sConsulta & "                             CHARINDEX(@SEPARADOR,CC.VALOR_TEXT, CHARINDEX(@SEPARADOR,CC.VALOR_TEXT) + 1))-1" & vbCrLf
sConsulta = sConsulta & "               END" & vbCrLf
sConsulta = sConsulta & "           )=@UON1 + @SEPARADOR + @OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- COPIA_LINEA_DESGLOSE" & vbCrLf
sConsulta = sConsulta & "--     CENTROS" & vbCrLf
sConsulta = sConsulta & "UPDATE CLD SET VALOR_TEXT = COALESCE(STUFF(CLD.VALOR_TEXT, LEN(@UON1 + @SEPARADOR) + 1, LEN(@OLD), @NEW) , CLD.VALOR_TEXT)" & vbCrLf
sConsulta = sConsulta & "FROM COPIA_LINEA_DESGLOSE CLD WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_CAMPO CC WITH (NOLOCK) ON CLD.CAMPO_HIJO = CC.ID " & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_CAMPO_DEF CCD WITH (NOLOCK) ON CC.COPIA_CAMPO_DEF = CCD.ID" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "   CCD.TIPO_CAMPO_GS = 129 AND " & vbCrLf
sConsulta = sConsulta & "   CLD.VALOR_TEXT IS NOT NULL AND " & vbCrLf
sConsulta = sConsulta & "   SUBSTRING(CLD.VALOR_TEXT, " & vbCrLf
sConsulta = sConsulta & "             1, " & vbCrLf
sConsulta = sConsulta & "             CASE WHEN " & vbCrLf
sConsulta = sConsulta & "               CHARINDEX(@SEPARADOR,CLD.VALOR_TEXT, LEN(@UON1 + @SEPARADOR) + 1)=0 THEN " & vbCrLf
sConsulta = sConsulta & "                   LEN(CLD.VALOR_TEXT) " & vbCrLf
sConsulta = sConsulta & "               ELSE " & vbCrLf
sConsulta = sConsulta & "                   CHARINDEX(@SEPARADOR,CLD.VALOR_TEXT, LEN(@UON1 + @SEPARADOR) + 1)-1" & vbCrLf
sConsulta = sConsulta & "               END" & vbCrLf
sConsulta = sConsulta & "           )=@UON1 + @SEPARADOR + @OLD" & vbCrLf
sConsulta = sConsulta & "--     PARTIDAS" & vbCrLf
sConsulta = sConsulta & "UPDATE CLD SET VALOR_TEXT = COALESCE(STUFF(CLD.VALOR_TEXT, LEN(@UON1 + @SEPARADOR) + 1, LEN(@OLD), @NEW) , CLD.VALOR_TEXT)" & vbCrLf
sConsulta = sConsulta & "FROM COPIA_LINEA_DESGLOSE CLD WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_CAMPO CC WITH (NOLOCK) ON CLD.CAMPO_HIJO = CC.ID " & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_CAMPO_DEF CCD WITH (NOLOCK) ON CC.COPIA_CAMPO_DEF = CCD.ID" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "   CCD.TIPO_CAMPO_GS = 130 AND " & vbCrLf
sConsulta = sConsulta & "   CLD.VALOR_TEXT IS NOT NULL AND " & vbCrLf
sConsulta = sConsulta & "   SUBSTRING(SUBSTRING(CLD.VALOR_TEXT, 1, LEN(CLD.VALOR_TEXT) - CHARINDEX(@SEPARADOR, REVERSE(CLD.VALOR_TEXT))),  --QUITAMOS LA PARTIDA PRES" & vbCrLf
sConsulta = sConsulta & "             1, " & vbCrLf
sConsulta = sConsulta & "             CASE WHEN " & vbCrLf
sConsulta = sConsulta & "               CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                         SUBSTRING(CLD.VALOR_TEXT, 1, LEN(CLD.VALOR_TEXT) - CHARINDEX(@SEPARADOR, REVERSE(CLD.VALOR_TEXT))), " & vbCrLf
sConsulta = sConsulta & "                         CHARINDEX(@SEPARADOR,CLD.VALOR_TEXT, LEN(@UON1 + @SEPARADOR) + 1)" & vbCrLf
sConsulta = sConsulta & "                         )=0 THEN " & vbCrLf
sConsulta = sConsulta & "                   LEN(SUBSTRING(CLD.VALOR_TEXT, 1, LEN(CLD.VALOR_TEXT) - CHARINDEX(@SEPARADOR, REVERSE(CLD.VALOR_TEXT))))" & vbCrLf
sConsulta = sConsulta & "               ELSE " & vbCrLf
sConsulta = sConsulta & "                   CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                             SUBSTRING(CLD.VALOR_TEXT, 1, LEN(CLD.VALOR_TEXT) - CHARINDEX(@SEPARADOR, REVERSE(CLD.VALOR_TEXT))), " & vbCrLf
sConsulta = sConsulta & "                             CHARINDEX(@SEPARADOR,CLD.VALOR_TEXT, LEN(@UON1 + @SEPARADOR) + 1))-1" & vbCrLf
sConsulta = sConsulta & "               END" & vbCrLf
sConsulta = sConsulta & "           )=@UON1 + @SEPARADOR + @OLD" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "-- INSTANCIA_CENTROS_PARTIDAS" & vbCrLf
sConsulta = sConsulta & "--     CENTROS" & vbCrLf
sConsulta = sConsulta & "UPDATE ICP SET VALOR_TEXT = COALESCE(STUFF(ICP.VALOR_TEXT, LEN(@UON1 + @SEPARADOR) + 1, LEN(@OLD), @NEW) , VALOR_TEXT)" & vbCrLf
sConsulta = sConsulta & "FROM INSTANCIA_CENTROS_PARTIDAS ICP WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "ICP.TIPO_CAMPO_GS = 129 AND " & vbCrLf
sConsulta = sConsulta & "ICP.VALOR_TEXT IS NOT NULL AND" & vbCrLf
sConsulta = sConsulta & "SUBSTRING(ICP.VALOR_TEXT, " & vbCrLf
sConsulta = sConsulta & "         1, " & vbCrLf
sConsulta = sConsulta & "         CASE WHEN " & vbCrLf
sConsulta = sConsulta & "           CHARINDEX(@SEPARADOR,ICP.VALOR_TEXT, LEN(@UON1 + @SEPARADOR) + 1)=0 THEN " & vbCrLf
sConsulta = sConsulta & "               LEN(ICP.VALOR_TEXT) " & vbCrLf
sConsulta = sConsulta & "           ELSE " & vbCrLf
sConsulta = sConsulta & "               CHARINDEX(@SEPARADOR,ICP.VALOR_TEXT, LEN(@UON1 + @SEPARADOR) + 1)-1" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "       )=@UON1 + @SEPARADOR + @OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--     PARTIDAS" & vbCrLf
sConsulta = sConsulta & "UPDATE ICP SET VALOR_TEXT = COALESCE(STUFF(ICP.VALOR_TEXT, LEN(@UON1 + @SEPARADOR) + 1, LEN(@OLD), @NEW) , VALOR_TEXT)" & vbCrLf
sConsulta = sConsulta & "FROM INSTANCIA_CENTROS_PARTIDAS ICP WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "ICP.TIPO_CAMPO_GS  = 130 AND " & vbCrLf
sConsulta = sConsulta & "ICP.VALOR_TEXT IS NOT NULL AND" & vbCrLf
sConsulta = sConsulta & "SUBSTRING(SUBSTRING(ICP.VALOR_TEXT, 1, LEN(ICP.VALOR_TEXT) - CHARINDEX(@SEPARADOR, REVERSE(ICP.VALOR_TEXT))),  --QUITAMOS LA PARTIDA PRES" & vbCrLf
sConsulta = sConsulta & "         1, " & vbCrLf
sConsulta = sConsulta & "         CASE WHEN " & vbCrLf
sConsulta = sConsulta & "           CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                     SUBSTRING(ICP.VALOR_TEXT, 1, LEN(ICP.VALOR_TEXT) - CHARINDEX(@SEPARADOR, REVERSE(ICP.VALOR_TEXT))), " & vbCrLf
sConsulta = sConsulta & "                     CHARINDEX(@SEPARADOR,ICP.VALOR_TEXT, LEN(@UON1 + @SEPARADOR) + 1)" & vbCrLf
sConsulta = sConsulta & "                     )=0 THEN " & vbCrLf
sConsulta = sConsulta & "               LEN(SUBSTRING(ICP.VALOR_TEXT, 1, LEN(ICP.VALOR_TEXT) - CHARINDEX(@SEPARADOR, REVERSE(ICP.VALOR_TEXT))))" & vbCrLf
sConsulta = sConsulta & "           ELSE " & vbCrLf
sConsulta = sConsulta & "               CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                         SUBSTRING(ICP.VALOR_TEXT, 1, LEN(ICP.VALOR_TEXT) - CHARINDEX(@SEPARADOR, REVERSE(ICP.VALOR_TEXT))), " & vbCrLf
sConsulta = sConsulta & "                         CHARINDEX(@SEPARADOR,ICP.VALOR_TEXT, LEN(@UON1 + @SEPARADOR) + 1))-1" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "       )=@UON1 + @SEPARADOR + @OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- PM_CONTR_FILTRO_USU_BUSQUEDA" & vbCrLf
sConsulta = sConsulta & "UPDATE P SET CENTRO_COSTE = COALESCE(STUFF(CENTRO_COSTE, LEN(@UON1 + @SEPARADOR) + 1, LEN(@OLD), @NEW) , CENTRO_COSTE)" & vbCrLf
sConsulta = sConsulta & "FROM PM_CONTR_FILTRO_USU_BUSQUEDA P WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "P.CENTRO_COSTE IS NOT NULL AND" & vbCrLf
sConsulta = sConsulta & "SUBSTRING(P.CENTRO_COSTE, " & vbCrLf
sConsulta = sConsulta & "         1, " & vbCrLf
sConsulta = sConsulta & "         CASE WHEN " & vbCrLf
sConsulta = sConsulta & "           CHARINDEX(@SEPARADOR,P.CENTRO_COSTE, LEN(@UON1 + @SEPARADOR) + 1)=0 THEN " & vbCrLf
sConsulta = sConsulta & "               LEN(P.CENTRO_COSTE) " & vbCrLf
sConsulta = sConsulta & "           ELSE " & vbCrLf
sConsulta = sConsulta & "               CHARINDEX(@SEPARADOR,P.CENTRO_COSTE, LEN(@UON1 + @SEPARADOR) + 1)-1" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "       )=@UON1 + @SEPARADOR + @OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE P SET PARTIDA1 = COALESCE(STUFF(PARTIDA1, LEN(@UON1 + @SEPARADOR) + 1, LEN(@OLD), @NEW) , PARTIDA1)" & vbCrLf
sConsulta = sConsulta & "FROM PM_CONTR_FILTRO_USU_BUSQUEDA P WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "P.PARTIDA1 IS NOT NULL AND" & vbCrLf
sConsulta = sConsulta & "SUBSTRING(SUBSTRING(P.PARTIDA1, 1, LEN(P.PARTIDA1) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA1))),  --QUITAMOS LA PARTIDA PRES" & vbCrLf
sConsulta = sConsulta & "         1, " & vbCrLf
sConsulta = sConsulta & "         CASE WHEN " & vbCrLf
sConsulta = sConsulta & "           CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                     SUBSTRING(P.PARTIDA1, 1, LEN(P.PARTIDA1) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA1))), " & vbCrLf
sConsulta = sConsulta & "                     CHARINDEX(@SEPARADOR,P.PARTIDA1, LEN(@UON1 + @SEPARADOR) + 1)" & vbCrLf
sConsulta = sConsulta & "                     )=0 THEN " & vbCrLf
sConsulta = sConsulta & "               LEN(SUBSTRING(P.PARTIDA1, 1, LEN(P.PARTIDA1) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA1))))" & vbCrLf
sConsulta = sConsulta & "           ELSE " & vbCrLf
sConsulta = sConsulta & "               CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                         SUBSTRING(P.PARTIDA1, 1, LEN(P.PARTIDA1) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA1))), " & vbCrLf
sConsulta = sConsulta & "                         CHARINDEX(@SEPARADOR,P.PARTIDA1, LEN(@UON1 + @SEPARADOR) + 1))-1" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "       )=@UON1 + @SEPARADOR + @OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE P SET PARTIDA2 = COALESCE(STUFF(PARTIDA2, LEN(@UON1 + @SEPARADOR) + 1, LEN(@OLD), @NEW) , PARTIDA2)" & vbCrLf
sConsulta = sConsulta & "FROM PM_CONTR_FILTRO_USU_BUSQUEDA P WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "P.PARTIDA2 IS NOT NULL AND" & vbCrLf
sConsulta = sConsulta & "SUBSTRING(SUBSTRING(P.PARTIDA2, 1, LEN(P.PARTIDA2) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA2))),  --QUITAMOS LA PARTIDA PRES" & vbCrLf
sConsulta = sConsulta & "         1, " & vbCrLf
sConsulta = sConsulta & "         CASE WHEN " & vbCrLf
sConsulta = sConsulta & "           CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                     SUBSTRING(P.PARTIDA2, 1, LEN(P.PARTIDA2) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA2))), " & vbCrLf
sConsulta = sConsulta & "                     CHARINDEX(@SEPARADOR,P.PARTIDA2, LEN(@UON1 + @SEPARADOR) + 1)" & vbCrLf
sConsulta = sConsulta & "                     )=0 THEN " & vbCrLf
sConsulta = sConsulta & "               LEN(SUBSTRING(P.PARTIDA2, 1, LEN(P.PARTIDA2) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA2))))" & vbCrLf
sConsulta = sConsulta & "           ELSE " & vbCrLf
sConsulta = sConsulta & "               CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                         SUBSTRING(P.PARTIDA2, 1, LEN(P.PARTIDA2) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA2))), " & vbCrLf
sConsulta = sConsulta & "                         CHARINDEX(@SEPARADOR,P.PARTIDA2, LEN(@UON1 + @SEPARADOR) + 1))-1" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "       )=@UON1 + @SEPARADOR + @OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE P SET PARTIDA3 = COALESCE(STUFF(PARTIDA3, LEN(@UON1 + @SEPARADOR) + 1, LEN(@OLD), @NEW) , PARTIDA3)" & vbCrLf
sConsulta = sConsulta & "FROM PM_CONTR_FILTRO_USU_BUSQUEDA P WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "P.PARTIDA3 IS NOT NULL AND" & vbCrLf
sConsulta = sConsulta & "SUBSTRING(SUBSTRING(P.PARTIDA3, 1, LEN(P.PARTIDA3) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA3))),  --QUITAMOS LA PARTIDA PRES" & vbCrLf
sConsulta = sConsulta & "         1, " & vbCrLf
sConsulta = sConsulta & "         CASE WHEN " & vbCrLf
sConsulta = sConsulta & "           CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                     SUBSTRING(P.PARTIDA3, 1, LEN(P.PARTIDA3) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA3))), " & vbCrLf
sConsulta = sConsulta & "                     CHARINDEX(@SEPARADOR,P.PARTIDA3, LEN(@UON1 + @SEPARADOR) + 1)" & vbCrLf
sConsulta = sConsulta & "                     )=0 THEN " & vbCrLf
sConsulta = sConsulta & "               LEN(SUBSTRING(P.PARTIDA3, 1, LEN(P.PARTIDA3) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA3))))" & vbCrLf
sConsulta = sConsulta & "           ELSE " & vbCrLf
sConsulta = sConsulta & "               CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                         SUBSTRING(P.PARTIDA3, 1, LEN(P.PARTIDA3) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA3))), " & vbCrLf
sConsulta = sConsulta & "                         CHARINDEX(@SEPARADOR,P.PARTIDA3, LEN(@UON1 + @SEPARADOR) + 1))-1" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "       )=@UON1 + @SEPARADOR + @OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE P SET PARTIDA4 = COALESCE(STUFF(PARTIDA4, LEN(@UON1 + @SEPARADOR) + 1, LEN(@OLD), @NEW) , PARTIDA4)" & vbCrLf
sConsulta = sConsulta & "FROM PM_CONTR_FILTRO_USU_BUSQUEDA P WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "P.PARTIDA4 IS NOT NULL AND" & vbCrLf
sConsulta = sConsulta & "SUBSTRING(SUBSTRING(P.PARTIDA4, 1, LEN(P.PARTIDA4) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA4))),  --QUITAMOS LA PARTIDA PRES" & vbCrLf
sConsulta = sConsulta & "         1, " & vbCrLf
sConsulta = sConsulta & "         CASE WHEN " & vbCrLf
sConsulta = sConsulta & "           CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                     SUBSTRING(P.PARTIDA4, 1, LEN(P.PARTIDA4) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA4))), " & vbCrLf
sConsulta = sConsulta & "                     CHARINDEX(@SEPARADOR,P.PARTIDA4, LEN(@UON1 + @SEPARADOR) + 1)" & vbCrLf
sConsulta = sConsulta & "                     )=0 THEN " & vbCrLf
sConsulta = sConsulta & "               LEN(SUBSTRING(P.PARTIDA4, 1, LEN(P.PARTIDA4) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA4))))" & vbCrLf
sConsulta = sConsulta & "           ELSE " & vbCrLf
sConsulta = sConsulta & "               CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                         SUBSTRING(P.PARTIDA4, 1, LEN(P.PARTIDA4) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA4))), " & vbCrLf
sConsulta = sConsulta & "                         CHARINDEX(@SEPARADOR,P.PARTIDA4, LEN(@UON1 + @SEPARADOR) + 1))-1" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "       )=@UON1 + @SEPARADOR + @OLD" & vbCrLf
sConsulta = sConsulta & "--FIN ACTUALIZACION COPIA_CAMPO, COPIA_LINEA_DESGLOSE, INSTANCIA_CENTROS_PARTIDAS, PM_CONTR_FILTRO_USU_BUSQUEDA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON2_DEP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3_DEP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PER CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON2_DEST CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3_DEST CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISOR CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CERTIFICADO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE APROBADORES CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PETICIONARIOS CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_ROL CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_ENLACE_CONDICIONES CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_COPIA_ROL CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_COPIA_ENLACE_CONDICION CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_UON CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU_CC_CONTROL CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU_CC_IMPUTACION CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES5_UON CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CENTRO_SM_UON CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON4_DEST CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_PED_IMPUTACION CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PERF_UON CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_CAT_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_GRUPO_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_MEN_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "CLOSE C" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "COMMIT TRANSACTION" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT OFF" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UON1_COD]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[UON1_COD]" & vbCrLf
    gRDOCon.QueryTimeout = 7200
    ExecuteSQL gRDOCon, sConsulta
    
sConsulta = "CREATE PROCEDURE UON1_COD (@OLD VARCHAR(50),@NEW VARCHAR(50)) AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT ON" & vbCrLf
sConsulta = sConsulta & "BEGIN TRANSACTION" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR FOR SELECT COD FROM UON1 WHERE COD=@OLD" & vbCrLf
sConsulta = sConsulta & "OPEN C" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C INTO @VAR_COD" & vbCrLf
sConsulta = sConsulta & "IF @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON1_DEP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON2_DEP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3_DEP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PER NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON1_DEST NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON2_DEST NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3_DEST NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISOR NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CERTIFICADO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE APROBADORES NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PETICIONARIOS NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_ROL NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_ENLACE_CONDICIONES NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_COPIA_ROL NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_COPIA_ENLACE_CONDICION NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_UON NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU_CC_CONTROL NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU_CC_IMPUTACION NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES5_UON NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CENTRO_SM_UON NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON4_DEST NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_PED_IMPUTACION NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PERF_UON NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_CAT_UON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_GRUPO_UON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_MEN_UON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE UON1 SET COD=@NEW WHERE COD=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON2 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON3 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON4 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON1_DEP SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON2_DEP SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON3_DEP SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PER SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE COM SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_UON1 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_UON2 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_UON3 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_UON1 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_UON2 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_UON3 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON1_DEST SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON2_DEST SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON3_DEST SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_UON1 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_UON2 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_UON3 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_UON1 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_UON2 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_UON3 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES1_NIV1 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES1_NIV2 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES1_NIV3 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES1_NIV4 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES2_NIV1 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES2_NIV2 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES2_NIV3 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES2_NIV4 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES3_NIV1 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES3_NIV2 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES3_NIV3 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES3_NIV4 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES4_NIV1 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES4_NIV2 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES4_NIV3 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES4_NIV4 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_1 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_2 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_3 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_4 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_1 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_2 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_3 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_4 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON1 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON2 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON3 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON4 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY1 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY2 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY3 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY4 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_PRESCON3 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_PRESCON4 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISOR SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CERTIFICADO SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE APROBADORES SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PETICIONARIOS SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PM_ROL SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PM_COPIA_ROL SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4_UON SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET UON1_APER=@NEW WHERE UON1_APER=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE USU_CC_CONTROL   SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE USU_CC_IMPUTACION SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES5_UON SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CENTRO_SM_UON SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON4_DEST SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE LINEAS_PED_IMPUTACION SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PERF_UON SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_CAT_UON1 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_GRUPO_UON1 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_MEN_UON1 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @LEN_COD_UON1 AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE C2 CURSOR FOR SELECT LONGITUD FROM DIC WHERE NOMBRE='UON1'" & vbCrLf
sConsulta = sConsulta & "OPEN C2" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C2 INTO @LEN_COD_UON1" & vbCrLf
sConsulta = sConsulta & "IF @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "  UPDATE PM_ENLACE_CONDICIONES SET VALOR_TEXT=STUFF(VALOR_TEXT,1,@LEN_COD_UON1,@NEW + SPACE(@LEN_COD_UON1 - LEN(@NEW))) WHERE TIPO_CAMPO=4 AND TIPO_VALOR=2 AND LTRIM(RTRIM(SUBSTRING(VALOR_TEXT,1,@LEN_COD_UON1)))=@OLD" & vbCrLf
sConsulta = sConsulta & "  UPDATE PM_COPIA_ENLACE_CONDICION SET VALOR_TEXT=STUFF(VALOR_TEXT,1,@LEN_COD_UON1,@NEW + SPACE(@LEN_COD_UON1 - LEN(@NEW))) WHERE TIPO_CAMPO=4 AND TIPO_VALOR=2 AND LTRIM(RTRIM(SUBSTRING(VALOR_TEXT,1,@LEN_COD_UON1)))=@OLD" & vbCrLf
sConsulta = sConsulta & "CLOSE C2" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C2" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--INICIO ACTUALIZACION COPIA_CAMPO, COPIA_LINEA_DESGLOSE, INSTANCIA_CENTROS_PARTIDAS, PM_CONTR_FILTRO_USU_BUSQUEDA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--ACTUALIZAR DATOS DE UONS EN LOS CENTROS Y PARTIDAS GRABADOS EN " & vbCrLf
sConsulta = sConsulta & "   --COPIA_CAMPO" & vbCrLf
sConsulta = sConsulta & "   --COPIA_LINEA_DESGLOSE" & vbCrLf
sConsulta = sConsulta & "   --INSTANCIA_CENTROS_DESGLOSE --> TABLA EN LA QUE SE HACE COPIA DE LOS DATOS DE CENTROS Y PARTIDAS DE COPIA_CAMPO Y COPIA_LINEA_DESGLOSE PARA ACELERAR FILTRADOS EN EL BUSCADOR DEL VISOR DE CONTRATOS" & vbCrLf
sConsulta = sConsulta & "   --PM_CONTR_FILTRO_USU_BUSQUEDA --> TABLA EN LA QUE SE HACE COPIA DE LOS VALORES POR DEFECTO PARA LOS FILTROS DEL BUSCADOR DEL VISOR DE CONTRATOS, QUE INCLUYE, ENTRE OTRAS COSAS CENTROS Y PARTIDAS." & vbCrLf
sConsulta = sConsulta & "--HAY QUE MODIFICARLO PORQUE EL MODO EN QUE SE GRABAN CENTROS Y PARTIDAS ES:" & vbCrLf
sConsulta = sConsulta & "--CENTROS: UON1#UON2#... HASTA EL NIVEL AL QUE EST� VINCULADO EL CENTRO" & vbCrLf
sConsulta = sConsulta & "--PARTIDAS: UON1#UON2#...#PRESN: UONS HASTA EL NIVEL AL QUE EST� VINCULADA LA PARTIDA Y PRESN, DONDE N ES EL NIVEL DE IMPUTACION QUE SE INDICA EN PARGEN_SM.IMP_NIVEL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- QU� ES MEJOR PARA BUSCAR LAS UON EN LOS CAMPOS, GUIARSE POR LA LONGITUD DEL C�DIGO UON O POR EL SEPARADOR #: " & vbCrLf
sConsulta = sConsulta & "-- DADO QUE SE PUEDE CAMBIAR LA LONGITUD EN LA TABLA DIC Y QUE EN LAS 4 TABLAS AL GUARDAR LOS DATOS NO SE TIENE EN CUENTA ESA LONGITUD, USAREMOS #" & vbCrLf
sConsulta = sConsulta & "-- COPIA_CAMPO" & vbCrLf
sConsulta = sConsulta & "DECLARE @SEPARADOR VARCHAR(3) = '#'" & vbCrLf
sConsulta = sConsulta & "UPDATE CC SET VALOR_TEXT = STUFF(CC.VALOR_TEXT,1,LEN(@OLD),@NEW) " & vbCrLf
sConsulta = sConsulta & "FROM COPIA_CAMPO CC WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_CAMPO_DEF CD WITH(NOLOCK) ON CC.COPIA_CAMPO_DEF=CD.ID" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "   CD.TIPO_CAMPO_GS IN (129,130) AND " & vbCrLf
sConsulta = sConsulta & "   CC.VALOR_TEXT IS NOT NULL AND " & vbCrLf
sConsulta = sConsulta & "   (LTRIM(RTRIM(SUBSTRING(CC.VALOR_TEXT,1,CASE WHEN (CHARINDEX (@SEPARADOR,CC.VALOR_TEXT))=0 THEN 0 ELSE (CHARINDEX (@SEPARADOR,CC.VALOR_TEXT)-1) END)))=@OLD" & vbCrLf
sConsulta = sConsulta & "   OR" & vbCrLf
sConsulta = sConsulta & "   CC.VALOR_TEXT = @OLD) --CUANDO SE IMPUTE UN CENTRO A UNA UON1, EL VALOR_TEXT SER� EXACTAMENTE EL UON1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- COPIA_LINEA_DESGLOSE" & vbCrLf
sConsulta = sConsulta & "UPDATE CLD SET VALOR_TEXT = STUFF(CLD.VALOR_TEXT,1,LEN(@OLD),@NEW) " & vbCrLf
sConsulta = sConsulta & "FROM COPIA_LINEA_DESGLOSE CLD WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_CAMPO CC WITH (NOLOCK) ON CLD.CAMPO_HIJO = CC.ID " & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_CAMPO_DEF CCD WITH (NOLOCK) ON CC.COPIA_CAMPO_DEF = CCD.ID" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "   CCD.TIPO_CAMPO_GS IN (129,130) AND " & vbCrLf
sConsulta = sConsulta & "   CLD.VALOR_TEXT IS NOT NULL AND " & vbCrLf
sConsulta = sConsulta & "   (LTRIM(RTRIM(SUBSTRING(CLD.VALOR_TEXT,1,CASE WHEN (CHARINDEX (@SEPARADOR,CLD.VALOR_TEXT))=0 THEN 0 ELSE (CHARINDEX (@SEPARADOR,CLD.VALOR_TEXT)-1) END)))=@OLD" & vbCrLf
sConsulta = sConsulta & "   OR" & vbCrLf
sConsulta = sConsulta & "   CLD.VALOR_TEXT = @OLD)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- INSTANCIA_CENTROS_PARTIDAS" & vbCrLf
sConsulta = sConsulta & "UPDATE ICP SET VALOR_TEXT = STUFF(ICP.VALOR_TEXT,1,LEN(@OLD),@NEW) " & vbCrLf
sConsulta = sConsulta & "FROM INSTANCIA_CENTROS_PARTIDAS ICP WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "ICP.TIPO_CAMPO_GS IN (129, 130) AND " & vbCrLf
sConsulta = sConsulta & "ICP.VALOR_TEXT IS NOT NULL AND" & vbCrLf
sConsulta = sConsulta & "(LTRIM(RTRIM(SUBSTRING(ICP.VALOR_TEXT,1,CASE WHEN (CHARINDEX (@SEPARADOR,ICP.VALOR_TEXT))=0 THEN 0 ELSE (CHARINDEX (@SEPARADOR,ICP.VALOR_TEXT)-1) END)))=@OLD" & vbCrLf
sConsulta = sConsulta & "OR" & vbCrLf
sConsulta = sConsulta & "ICP.VALOR_TEXT = @OLD) --CUANDO SE IMPUTE UN CENTRO A UNA UON1, EL VALOR_TEXT SER� EXACTAMENTE EL UON1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- PM_CONTR_FILTRO_USU_BUSQUEDA" & vbCrLf
sConsulta = sConsulta & "UPDATE P SET CENTRO_COSTE = STUFF(P.CENTRO_COSTE,1,LEN(@OLD),@NEW) " & vbCrLf
sConsulta = sConsulta & "FROM PM_CONTR_FILTRO_USU_BUSQUEDA P WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "P.CENTRO_COSTE IS NOT NULL AND" & vbCrLf
sConsulta = sConsulta & "(LTRIM(RTRIM(SUBSTRING(P.CENTRO_COSTE,1,CASE WHEN (CHARINDEX (@SEPARADOR,P.CENTRO_COSTE))=0 THEN 0 ELSE (CHARINDEX (@SEPARADOR,P.CENTRO_COSTE)-1) END)))=@OLD" & vbCrLf
sConsulta = sConsulta & "OR" & vbCrLf
sConsulta = sConsulta & "P.CENTRO_COSTE = @OLD) --CUANDO SE IMPUTE UN CENTRO A UNA UON1, EL CENTRO_COSTE SER� EXACTAMENTE EL UON1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE P SET PARTIDA1 = STUFF(P.PARTIDA1,1,LEN(@OLD),@NEW) " & vbCrLf
sConsulta = sConsulta & "FROM PM_CONTR_FILTRO_USU_BUSQUEDA P WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "P.PARTIDA1 IS NOT NULL AND" & vbCrLf
sConsulta = sConsulta & "LTRIM(RTRIM(SUBSTRING(P.PARTIDA1,1,CASE WHEN (CHARINDEX (@SEPARADOR,P.PARTIDA1))=0 THEN 0 ELSE (CHARINDEX (@SEPARADOR,P.PARTIDA1)-1) END)))=@OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE P SET PARTIDA2 = STUFF(P.PARTIDA2,1,LEN(@OLD),@NEW) " & vbCrLf
sConsulta = sConsulta & "FROM PM_CONTR_FILTRO_USU_BUSQUEDA P WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "P.PARTIDA2 IS NOT NULL AND" & vbCrLf
sConsulta = sConsulta & "LTRIM(RTRIM(SUBSTRING(P.PARTIDA2,1,CASE WHEN (CHARINDEX (@SEPARADOR,P.PARTIDA2))=0 THEN 0 ELSE (CHARINDEX (@SEPARADOR,P.PARTIDA2)-1) END)))=@OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE P SET PARTIDA3 = STUFF(P.PARTIDA3,1,LEN(@OLD),@NEW) " & vbCrLf
sConsulta = sConsulta & "FROM PM_CONTR_FILTRO_USU_BUSQUEDA P WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "P.PARTIDA3 IS NOT NULL AND" & vbCrLf
sConsulta = sConsulta & "LTRIM(RTRIM(SUBSTRING(P.PARTIDA3,1,CASE WHEN (CHARINDEX (@SEPARADOR,P.PARTIDA3))=0 THEN 0 ELSE (CHARINDEX (@SEPARADOR,P.PARTIDA3)-1) END)))=@OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE P SET PARTIDA4 = STUFF(P.PARTIDA4,1,LEN(@OLD),@NEW) " & vbCrLf
sConsulta = sConsulta & "FROM PM_CONTR_FILTRO_USU_BUSQUEDA P WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "P.PARTIDA4 IS NOT NULL AND" & vbCrLf
sConsulta = sConsulta & "LTRIM(RTRIM(SUBSTRING(P.PARTIDA4,1,CASE WHEN (CHARINDEX (@SEPARADOR,P.PARTIDA4))=0 THEN 0 ELSE (CHARINDEX (@SEPARADOR,P.PARTIDA4)-1) END)))=@OLD" & vbCrLf
sConsulta = sConsulta & "--FIN ACTUALIZACION COPIA_CAMPO, COPIA_LINEA_DESGLOSE, INSTANCIA_CENTROS_PARTIDAS, PM_CONTR_FILTRO_USU_BUSQUEDA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON1_DEP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON2_DEP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3_DEP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PER CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON1_DEST CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON2_DEST CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3_DEST CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISOR CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CERTIFICADO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE APROBADORES CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PETICIONARIOS CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_ROL CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_ENLACE_CONDICIONES CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_COPIA_ROL CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_COPIA_ENLACE_CONDICION CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_UON CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU_CC_CONTROL CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU_CC_IMPUTACION CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES5_UON CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CENTRO_SM_UON CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON4_DEST CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_PED_IMPUTACION CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PERF_UON CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_CAT_UON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_GRUPO_UON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_MEN_UON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "CLOSE C" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "COMMIT TRANSACTION" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT OFF" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


    
End Sub


Public Function CodigoDeActualizacion31900_08_2001_A31900_08_2002() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim oADOConnection As ADODB.Connection
    Dim sConsulta As String

    On Error GoTo Error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
                  
    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Actualiza Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    frmProgreso.ProgressBar1.Refresh
    V_31900_8_Storeds_202
    
    sConsulta = "UPDATE VERSION SET NUM ='3.00.20.002'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103), PRODVER='31900.8'"
    ExecuteSQL gRDOCon, sConsulta
        
    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False
        
    CodigoDeActualizacion31900_08_2001_A31900_08_2002 = True
    Exit Function

Error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    CodigoDeActualizacion31900_08_2001_A31900_08_2002 = False
End Function

Public Sub V_31900_8_Storeds_202()
    Dim sConsulta As String
            
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UON1_COD]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[UON1_COD]" & vbCrLf
    gRDOCon.QueryTimeout = 7200
    ExecuteSQL gRDOCon, sConsulta
    
sConsulta = "CREATE PROCEDURE UON1_COD (@OLD VARCHAR(50),@NEW VARCHAR(50)) AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT ON" & vbCrLf
sConsulta = sConsulta & "BEGIN TRANSACTION" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR FOR SELECT COD FROM UON1 WHERE COD=@OLD" & vbCrLf
sConsulta = sConsulta & "OPEN C" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C INTO @VAR_COD" & vbCrLf
sConsulta = sConsulta & "IF @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON1_DEP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON2_DEP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3_DEP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PER NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON1_DEST NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON2_DEST NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3_DEST NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISOR NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CERTIFICADO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE NOCONFORMIDAD NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE APROBADORES NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PETICIONARIOS NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_ROL NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_ENLACE_CONDICIONES NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_COPIA_ROL NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_COPIA_ENLACE_CONDICION NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_UON NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU_CC_CONTROL NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU_CC_IMPUTACION NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES5_UON NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CENTRO_SM_UON NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON4_DEST NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_PED_IMPUTACION NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PERF_UON NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_CAT_UON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_GRUPO_UON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_MEN_UON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE UON1 SET COD=@NEW WHERE COD=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON2 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON3 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON4 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON1_DEP SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON2_DEP SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON3_DEP SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PER SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE COM SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_UON1 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_UON2 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_UON3 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_UON1 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_UON2 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_UON3 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON1_DEST SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON2_DEST SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON3_DEST SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_UON1 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_UON2 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_UON3 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_UON1 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_UON2 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_UON3 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES1_NIV1 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES1_NIV2 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES1_NIV3 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES1_NIV4 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES2_NIV1 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES2_NIV2 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES2_NIV3 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES2_NIV4 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES3_NIV1 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES3_NIV2 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES3_NIV3 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES3_NIV4 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES4_NIV1 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES4_NIV2 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES4_NIV3 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES4_NIV4 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_1 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_2 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_3 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_4 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_1 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_2 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_3 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_4 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON1 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON2 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON3 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON4 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY1 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY2 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY3 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY4 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_PRESCON3 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_PRESCON4 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISOR SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CERTIFICADO SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE NOCONFORMIDAD SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE APROBADORES SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PETICIONARIOS SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PM_ROL SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PM_COPIA_ROL SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4_UON SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET UON1_APER=@NEW WHERE UON1_APER=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE USU_CC_CONTROL   SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE USU_CC_IMPUTACION SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES5_UON SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CENTRO_SM_UON SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON4_DEST SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE LINEAS_PED_IMPUTACION SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PERF_UON SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_CAT_UON1 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_GRUPO_UON1 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_MEN_UON1 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @LEN_COD_UON1 AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE C2 CURSOR FOR SELECT LONGITUD FROM DIC WHERE NOMBRE='UON1'" & vbCrLf
sConsulta = sConsulta & "OPEN C2" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C2 INTO @LEN_COD_UON1" & vbCrLf
sConsulta = sConsulta & "IF @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "  UPDATE PM_ENLACE_CONDICIONES SET VALOR_TEXT=STUFF(VALOR_TEXT,1,@LEN_COD_UON1,@NEW + SPACE(@LEN_COD_UON1 - LEN(@NEW))) WHERE TIPO_CAMPO=4 AND TIPO_VALOR=2 AND LTRIM(RTRIM(SUBSTRING(VALOR_TEXT,1,@LEN_COD_UON1)))=@OLD" & vbCrLf
sConsulta = sConsulta & "  UPDATE PM_COPIA_ENLACE_CONDICION SET VALOR_TEXT=STUFF(VALOR_TEXT,1,@LEN_COD_UON1,@NEW + SPACE(@LEN_COD_UON1 - LEN(@NEW))) WHERE TIPO_CAMPO=4 AND TIPO_VALOR=2 AND LTRIM(RTRIM(SUBSTRING(VALOR_TEXT,1,@LEN_COD_UON1)))=@OLD" & vbCrLf
sConsulta = sConsulta & "CLOSE C2" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C2" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--INICIO ACTUALIZACION COPIA_CAMPO, COPIA_LINEA_DESGLOSE, INSTANCIA_CENTROS_PARTIDAS, PM_CONTR_FILTRO_USU_BUSQUEDA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--ACTUALIZAR DATOS DE UONS EN LOS CENTROS Y PARTIDAS GRABADOS EN " & vbCrLf
sConsulta = sConsulta & "   --COPIA_CAMPO" & vbCrLf
sConsulta = sConsulta & "   --COPIA_LINEA_DESGLOSE" & vbCrLf
sConsulta = sConsulta & "   --INSTANCIA_CENTROS_DESGLOSE --> TABLA EN LA QUE SE HACE COPIA DE LOS DATOS DE CENTROS Y PARTIDAS DE COPIA_CAMPO Y COPIA_LINEA_DESGLOSE PARA ACELERAR FILTRADOS EN EL BUSCADOR DEL VISOR DE CONTRATOS" & vbCrLf
sConsulta = sConsulta & "   --PM_CONTR_FILTRO_USU_BUSQUEDA --> TABLA EN LA QUE SE HACE COPIA DE LOS VALORES POR DEFECTO PARA LOS FILTROS DEL BUSCADOR DEL VISOR DE CONTRATOS, QUE INCLUYE, ENTRE OTRAS COSAS CENTROS Y PARTIDAS." & vbCrLf
sConsulta = sConsulta & "--HAY QUE MODIFICARLO PORQUE EL MODO EN QUE SE GRABAN CENTROS Y PARTIDAS ES:" & vbCrLf
sConsulta = sConsulta & "--CENTROS: UON1#UON2#... HASTA EL NIVEL AL QUE EST� VINCULADO EL CENTRO" & vbCrLf
sConsulta = sConsulta & "--PARTIDAS: UON1#UON2#...#PRESN: UONS HASTA EL NIVEL AL QUE EST� VINCULADA LA PARTIDA Y PRESN, DONDE N ES EL NIVEL DE IMPUTACION QUE SE INDICA EN PARGEN_SM.IMP_NIVEL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- QU� ES MEJOR PARA BUSCAR LAS UON EN LOS CAMPOS, GUIARSE POR LA LONGITUD DEL C�DIGO UON O POR EL SEPARADOR #: " & vbCrLf
sConsulta = sConsulta & "-- DADO QUE SE PUEDE CAMBIAR LA LONGITUD EN LA TABLA DIC Y QUE EN LAS 4 TABLAS AL GUARDAR LOS DATOS NO SE TIENE EN CUENTA ESA LONGITUD, USAREMOS #" & vbCrLf
sConsulta = sConsulta & "-- COPIA_CAMPO" & vbCrLf
sConsulta = sConsulta & "DECLARE @SEPARADOR VARCHAR(3) = '#'" & vbCrLf
sConsulta = sConsulta & "UPDATE CC SET VALOR_TEXT = STUFF(CC.VALOR_TEXT,1,LEN(@OLD),@NEW) " & vbCrLf
sConsulta = sConsulta & "FROM COPIA_CAMPO CC WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_CAMPO_DEF CD WITH(NOLOCK) ON CC.COPIA_CAMPO_DEF=CD.ID" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "   CD.TIPO_CAMPO_GS IN (129,130) AND " & vbCrLf
sConsulta = sConsulta & "   CC.VALOR_TEXT IS NOT NULL AND " & vbCrLf
sConsulta = sConsulta & "   (LTRIM(RTRIM(SUBSTRING(CC.VALOR_TEXT,1,CASE WHEN (CHARINDEX (@SEPARADOR,CC.VALOR_TEXT))=0 THEN 0 ELSE (CHARINDEX (@SEPARADOR,CC.VALOR_TEXT)-1) END)))=@OLD" & vbCrLf
sConsulta = sConsulta & "   OR" & vbCrLf
sConsulta = sConsulta & "   CC.VALOR_TEXT = @OLD) --CUANDO SE IMPUTE UN CENTRO A UNA UON1, EL VALOR_TEXT SER� EXACTAMENTE EL UON1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- COPIA_LINEA_DESGLOSE" & vbCrLf
sConsulta = sConsulta & "UPDATE CLD SET VALOR_TEXT = STUFF(CLD.VALOR_TEXT,1,LEN(@OLD),@NEW) " & vbCrLf
sConsulta = sConsulta & "FROM COPIA_LINEA_DESGLOSE CLD WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_CAMPO CC WITH (NOLOCK) ON CLD.CAMPO_HIJO = CC.ID " & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_CAMPO_DEF CCD WITH (NOLOCK) ON CC.COPIA_CAMPO_DEF = CCD.ID" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "   CCD.TIPO_CAMPO_GS IN (129,130) AND " & vbCrLf
sConsulta = sConsulta & "   CLD.VALOR_TEXT IS NOT NULL AND " & vbCrLf
sConsulta = sConsulta & "   (LTRIM(RTRIM(SUBSTRING(CLD.VALOR_TEXT,1,CASE WHEN (CHARINDEX (@SEPARADOR,CLD.VALOR_TEXT))=0 THEN 0 ELSE (CHARINDEX (@SEPARADOR,CLD.VALOR_TEXT)-1) END)))=@OLD" & vbCrLf
sConsulta = sConsulta & "   OR" & vbCrLf
sConsulta = sConsulta & "   CLD.VALOR_TEXT = @OLD)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- INSTANCIA_CENTROS_PARTIDAS" & vbCrLf
sConsulta = sConsulta & "UPDATE ICP SET VALOR_TEXT = STUFF(ICP.VALOR_TEXT,1,LEN(@OLD),@NEW) " & vbCrLf
sConsulta = sConsulta & "FROM INSTANCIA_CENTROS_PARTIDAS ICP WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "ICP.TIPO_CAMPO_GS IN (129, 130) AND " & vbCrLf
sConsulta = sConsulta & "ICP.VALOR_TEXT IS NOT NULL AND" & vbCrLf
sConsulta = sConsulta & "(LTRIM(RTRIM(SUBSTRING(ICP.VALOR_TEXT,1,CASE WHEN (CHARINDEX (@SEPARADOR,ICP.VALOR_TEXT))=0 THEN 0 ELSE (CHARINDEX (@SEPARADOR,ICP.VALOR_TEXT)-1) END)))=@OLD" & vbCrLf
sConsulta = sConsulta & "OR" & vbCrLf
sConsulta = sConsulta & "ICP.VALOR_TEXT = @OLD) --CUANDO SE IMPUTE UN CENTRO A UNA UON1, EL VALOR_TEXT SER� EXACTAMENTE EL UON1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- PM_CONTR_FILTRO_USU_BUSQUEDA" & vbCrLf
sConsulta = sConsulta & "UPDATE P SET CENTRO_COSTE = STUFF(P.CENTRO_COSTE,1,LEN(@OLD),@NEW) " & vbCrLf
sConsulta = sConsulta & "FROM PM_CONTR_FILTRO_USU_BUSQUEDA P WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "P.CENTRO_COSTE IS NOT NULL AND" & vbCrLf
sConsulta = sConsulta & "(LTRIM(RTRIM(SUBSTRING(P.CENTRO_COSTE,1,CASE WHEN (CHARINDEX (@SEPARADOR,P.CENTRO_COSTE))=0 THEN 0 ELSE (CHARINDEX (@SEPARADOR,P.CENTRO_COSTE)-1) END)))=@OLD" & vbCrLf
sConsulta = sConsulta & "OR" & vbCrLf
sConsulta = sConsulta & "P.CENTRO_COSTE = @OLD) --CUANDO SE IMPUTE UN CENTRO A UNA UON1, EL CENTRO_COSTE SER� EXACTAMENTE EL UON1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE P SET PARTIDA1 = STUFF(P.PARTIDA1,1,LEN(@OLD),@NEW) " & vbCrLf
sConsulta = sConsulta & "FROM PM_CONTR_FILTRO_USU_BUSQUEDA P WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "P.PARTIDA1 IS NOT NULL AND" & vbCrLf
sConsulta = sConsulta & "LTRIM(RTRIM(SUBSTRING(P.PARTIDA1,1,CASE WHEN (CHARINDEX (@SEPARADOR,P.PARTIDA1))=0 THEN 0 ELSE (CHARINDEX (@SEPARADOR,P.PARTIDA1)-1) END)))=@OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE P SET PARTIDA2 = STUFF(P.PARTIDA2,1,LEN(@OLD),@NEW) " & vbCrLf
sConsulta = sConsulta & "FROM PM_CONTR_FILTRO_USU_BUSQUEDA P WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "P.PARTIDA2 IS NOT NULL AND" & vbCrLf
sConsulta = sConsulta & "LTRIM(RTRIM(SUBSTRING(P.PARTIDA2,1,CASE WHEN (CHARINDEX (@SEPARADOR,P.PARTIDA2))=0 THEN 0 ELSE (CHARINDEX (@SEPARADOR,P.PARTIDA2)-1) END)))=@OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE P SET PARTIDA3 = STUFF(P.PARTIDA3,1,LEN(@OLD),@NEW) " & vbCrLf
sConsulta = sConsulta & "FROM PM_CONTR_FILTRO_USU_BUSQUEDA P WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "P.PARTIDA3 IS NOT NULL AND" & vbCrLf
sConsulta = sConsulta & "LTRIM(RTRIM(SUBSTRING(P.PARTIDA3,1,CASE WHEN (CHARINDEX (@SEPARADOR,P.PARTIDA3))=0 THEN 0 ELSE (CHARINDEX (@SEPARADOR,P.PARTIDA3)-1) END)))=@OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE P SET PARTIDA4 = STUFF(P.PARTIDA4,1,LEN(@OLD),@NEW) " & vbCrLf
sConsulta = sConsulta & "FROM PM_CONTR_FILTRO_USU_BUSQUEDA P WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "P.PARTIDA4 IS NOT NULL AND" & vbCrLf
sConsulta = sConsulta & "LTRIM(RTRIM(SUBSTRING(P.PARTIDA4,1,CASE WHEN (CHARINDEX (@SEPARADOR,P.PARTIDA4))=0 THEN 0 ELSE (CHARINDEX (@SEPARADOR,P.PARTIDA4)-1) END)))=@OLD" & vbCrLf
sConsulta = sConsulta & "--FIN ACTUALIZACION COPIA_CAMPO, COPIA_LINEA_DESGLOSE, INSTANCIA_CENTROS_PARTIDAS, PM_CONTR_FILTRO_USU_BUSQUEDA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON1_DEP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON2_DEP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3_DEP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PER CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON1_DEST CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON2_DEST CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3_DEST CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISOR CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CERTIFICADO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE NOCONFORMIDAD CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE APROBADORES CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PETICIONARIOS CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_ROL CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_ENLACE_CONDICIONES CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_COPIA_ROL CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_COPIA_ENLACE_CONDICION CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_UON CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU_CC_CONTROL CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU_CC_IMPUTACION CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES5_UON CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CENTRO_SM_UON CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON4_DEST CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_PED_IMPUTACION CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PERF_UON CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_CAT_UON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_GRUPO_UON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_MEN_UON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "CLOSE C" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "COMMIT TRANSACTION" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT OFF" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UON2_COD]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[UON2_COD]" & vbCrLf
    gRDOCon.QueryTimeout = 7200
    ExecuteSQL gRDOCon, sConsulta
    
sConsulta = "CREATE PROCEDURE UON2_COD (@OLD VARCHAR(50),@NEW VARCHAR(50), @UON1 VARCHAR(50))  AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT ON" & vbCrLf
sConsulta = sConsulta & "BEGIN TRANSACTION" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR FOR SELECT COD FROM UON2 WITH(NOLOCK) WHERE UON1=@UON1 AND COD=@OLD" & vbCrLf
sConsulta = sConsulta & "OPEN C" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C INTO @VAR_COD" & vbCrLf
sConsulta = sConsulta & "IF @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON2_DEP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3_DEP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PER NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON2_DEST NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3_DEST NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISOR NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CERTIFICADO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE NOCONFORMIDAD NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE APROBADORES NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PETICIONARIOS NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_ROL NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_ENLACE_CONDICIONES NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_COPIA_ROL NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_COPIA_ENLACE_CONDICION NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_UON NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU_CC_CONTROL NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU_CC_IMPUTACION NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES5_UON NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CENTRO_SM_UON NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON4_DEST NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_PED_IMPUTACION NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PERF_UON NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_CAT_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_GRUPO_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_MEN_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE UON2 SET COD=@NEW WHERE UON1=@UON1 AND COD=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON3 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON4 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON2_DEP SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON3_DEP SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PER SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE COM SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_UON2 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_UON3 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_UON2 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_UON3 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON2_DEST SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON3_DEST SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_UON2 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_UON3 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_UON2 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_UON3 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_1 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_2 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_3 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_4 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_1 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_2 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_3 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_4 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON1 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON2 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON3 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON4 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY1 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY2 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY3 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY4 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_PRESCON3 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_PRESCON4 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES1_NIV1 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES1_NIV2 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES1_NIV3 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES1_NIV4 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES2_NIV1 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES2_NIV2 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES2_NIV3 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES2_NIV4 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES3_NIV1 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES3_NIV2 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES3_NIV3 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES3_NIV4 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES4_NIV1 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES4_NIV2 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES4_NIV3 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES4_NIV4 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISOR SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CERTIFICADO SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE NOCONFORMIDAD SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE APROBADORES SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PETICIONARIOS SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PM_ROL SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PM_COPIA_ROL SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4_UON SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET UON2_APER=@NEW WHERE UON1_APER=@UON1 AND UON2_APER=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE USU_CC_CONTROL SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE USU_CC_IMPUTACION SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES5_UON SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CENTRO_SM_UON SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON4_DEST SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE LINEAS_PED_IMPUTACION SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PERF_UON SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_CAT_UON2 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_GRUPO_UON2 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_MEN_UON2 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @LEN_COD_UON1 AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @LEN_COD_UON2 AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE C2 CURSOR FOR SELECT (SELECT LONGITUD FROM DIC WITH(NOLOCK) WHERE NOMBRE='UON1') AS L1,(SELECT LONGITUD FROM DIC WITH(NOLOCK) WHERE NOMBRE='UON2') AS L2" & vbCrLf
sConsulta = sConsulta & "OPEN C2" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C2 INTO @LEN_COD_UON1,@LEN_COD_UON2" & vbCrLf
sConsulta = sConsulta & "IF @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "  UPDATE PM_ENLACE_CONDICIONES SET VALOR_TEXT=STUFF(VALOR_TEXT,@LEN_COD_UON1 + 2,@LEN_COD_UON2,@NEW + SPACE(@LEN_COD_UON2 - LEN(@NEW))) WHERE TIPO_CAMPO=4 AND TIPO_VALOR=2 AND LTRIM(RTRIM(SUBSTRING(VALOR_TEXT,1,@LEN_COD_UON1)))=@UON1 AND LTRIM(RTRIM(SUBSTRING(VALOR_TEXT,@LEN_COD_UON1+2, @LEN_COD_UON2)))=@OLD" & vbCrLf
sConsulta = sConsulta & "  UPDATE PM_COPIA_ENLACE_CONDICION SET VALOR_TEXT=STUFF(VALOR_TEXT,@LEN_COD_UON1 + 2,@LEN_COD_UON2,@NEW + SPACE(@LEN_COD_UON2 - LEN(@NEW))) WHERE TIPO_CAMPO=4 AND TIPO_VALOR=2 AND LTRIM(RTRIM(SUBSTRING(VALOR_TEXT,1,@LEN_COD_UON1)))=@UON1 AND LTRIM(RTRIM(SUBSTRING(VALOR_TEXT,@LEN_COD_UON1+2, @LEN_COD_UON2)))=@OLD" & vbCrLf
sConsulta = sConsulta & "CLOSE C2" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C2" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--INICIO ACTUALIZACION COPIA_CAMPO, COPIA_LINEA_DESGLOSE, INSTANCIA_CENTROS_PARTIDAS, PM_CONTR_FILTRO_USU_BUSQUEDA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--ACTUALIZAR DATOS DE UONS EN LOS CENTROS Y PARTIDAS GRABADOS EN " & vbCrLf
sConsulta = sConsulta & "   --COPIA_CAMPO" & vbCrLf
sConsulta = sConsulta & "   --COPIA_LINEA_DESGLOSE" & vbCrLf
sConsulta = sConsulta & "   --INSTANCIA_CENTROS_DESGLOSE --> TABLA EN LA QUE SE HACE COPIA DE LOS DATOS DE CENTROS Y PARTIDAS DE COPIA_CAMPO Y COPIA_LINEA_DESGLOSE PARA ACELERAR FILTRADOS EN EL BUSCADOR DEL VISOR DE CONTRATOS" & vbCrLf
sConsulta = sConsulta & "   --PM_CONTR_FILTRO_USU_BUSQUEDA --> TABLA EN LA QUE SE HACE COPIA DE LOS VALORES POR DEFECTO PARA LOS FILTROS DEL BUSCADOR DEL VISOR DE CONTRATOS, QUE INCLUYE, ENTRE OTRAS COSAS CENTROS Y PARTIDAS." & vbCrLf
sConsulta = sConsulta & "--HAY QUE MODIFICARLO PORQUE EL MODO EN QUE SE GRABAN CENTROS Y PARTIDAS ES:" & vbCrLf
sConsulta = sConsulta & "--CENTROS: UON1#UON2#... HASTA EL NIVEL AL QUE EST� VINCULADO EL CENTRO" & vbCrLf
sConsulta = sConsulta & "--PARTIDAS: UON1#UON2#...#PRESN: UONS HASTA EL NIVEL AL QUE EST� VINCULADA LA PARTIDA Y PRESN, DONDE N ES EL NIVEL DE IMPUTACION QUE SE INDICA EN PARGEN_SM.IMP_NIVEL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- QU� ES MEJOR PARA BUSCAR LAS UON EN LOS CAMPOS, GUIARSE POR LA LONGITUD DEL C�DIGO UON O POR EL SEPARADOR #: " & vbCrLf
sConsulta = sConsulta & "-- DADO QUE SE PUEDE CAMBIAR LA LONGITUD EN LA TABLA DIC Y QUE EN LAS 4 TABLAS AL GUARDAR LOS DATOS NO SE TIENE EN CUENTA ESA LONGITUD, USAREMOS #" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--EN LAS PARTIDAS, EL �LTIMO VALOR ES SIEMPRE UNA PARTIDA PRESUPUESTARIA. AL MENOS HABR� SIEMPRE UN UON (UON1) PERO A PARTIR DE LOS UON2, PUEDE QUE NO HAYA, POR LO QUE TENEMOS QUE ASEGURARNOS DE NO CONFUNDIR LA PARTIDA CON UN UON AL REEMPLAZAR: VAMOS A IGNORAR LA PARTIDA AL FILTRAR" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- COPIA_CAMPO" & vbCrLf
sConsulta = sConsulta & "--     CENTROS" & vbCrLf
sConsulta = sConsulta & "DECLARE @SEPARADOR VARCHAR(3) = '#'" & vbCrLf
sConsulta = sConsulta & "UPDATE CC SET VALOR_TEXT = COALESCE(STUFF(CC.VALOR_TEXT, LEN(@UON1 + @SEPARADOR) + 1, LEN(@OLD), @NEW) , VALOR_TEXT)" & vbCrLf
sConsulta = sConsulta & "FROM COPIA_CAMPO CC WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_CAMPO_DEF CD WITH(NOLOCK) ON CC.COPIA_CAMPO_DEF=CD.ID" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "   CD.TIPO_CAMPO_GS = 129 AND " & vbCrLf
sConsulta = sConsulta & "   CC.VALOR_TEXT IS NOT NULL AND " & vbCrLf
sConsulta = sConsulta & "   SUBSTRING(CC.VALOR_TEXT, " & vbCrLf
sConsulta = sConsulta & "             1, " & vbCrLf
sConsulta = sConsulta & "             CASE WHEN " & vbCrLf
sConsulta = sConsulta & "               CHARINDEX(@SEPARADOR,CC.VALOR_TEXT, CHARINDEX(@SEPARADOR,CC.VALOR_TEXT) + 1)=0 THEN " & vbCrLf
sConsulta = sConsulta & "                   LEN(CC.VALOR_TEXT) " & vbCrLf
sConsulta = sConsulta & "               ELSE " & vbCrLf
sConsulta = sConsulta & "                   CHARINDEX(@SEPARADOR,CC.VALOR_TEXT, CHARINDEX(@SEPARADOR,CC.VALOR_TEXT) + 1)-1" & vbCrLf
sConsulta = sConsulta & "               END" & vbCrLf
sConsulta = sConsulta & "           )=@UON1 + @SEPARADOR + @OLD" & vbCrLf
sConsulta = sConsulta & "--     PARTIDAS" & vbCrLf
sConsulta = sConsulta & "UPDATE CC SET VALOR_TEXT = COALESCE(STUFF(CC.VALOR_TEXT, LEN(@UON1 + @SEPARADOR) + 1, LEN(@OLD), @NEW) , VALOR_TEXT)" & vbCrLf
sConsulta = sConsulta & "FROM COPIA_CAMPO CC WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_CAMPO_DEF CD WITH(NOLOCK) ON CC.COPIA_CAMPO_DEF=CD.ID" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "   CD.TIPO_CAMPO_GS = 130 AND " & vbCrLf
sConsulta = sConsulta & "   CC.VALOR_TEXT IS NOT NULL AND " & vbCrLf
sConsulta = sConsulta & "   SUBSTRING(SUBSTRING(CC.VALOR_TEXT, 1, LEN(CC.VALOR_TEXT) - CHARINDEX(@SEPARADOR, REVERSE(CC.VALOR_TEXT))),  --QUITAMOS LA PARTIDA PRES" & vbCrLf
sConsulta = sConsulta & "             1, " & vbCrLf
sConsulta = sConsulta & "             CASE WHEN " & vbCrLf
sConsulta = sConsulta & "               CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                         SUBSTRING(CC.VALOR_TEXT, 1, LEN(CC.VALOR_TEXT) - CHARINDEX(@SEPARADOR, REVERSE(CC.VALOR_TEXT))), " & vbCrLf
sConsulta = sConsulta & "                         CHARINDEX(@SEPARADOR,CC.VALOR_TEXT, LEN(@UON1 + @SEPARADOR) + 1)" & vbCrLf
sConsulta = sConsulta & "                         )=0 THEN " & vbCrLf
sConsulta = sConsulta & "                   LEN(SUBSTRING(CC.VALOR_TEXT, 1, LEN(CC.VALOR_TEXT) - CHARINDEX(@SEPARADOR, REVERSE(CC.VALOR_TEXT))))" & vbCrLf
sConsulta = sConsulta & "               ELSE " & vbCrLf
sConsulta = sConsulta & "                   CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                             SUBSTRING(CC.VALOR_TEXT, 1, LEN(CC.VALOR_TEXT) - CHARINDEX(@SEPARADOR, REVERSE(CC.VALOR_TEXT))), " & vbCrLf
sConsulta = sConsulta & "                             CHARINDEX(@SEPARADOR,CC.VALOR_TEXT, CHARINDEX(@SEPARADOR,CC.VALOR_TEXT) + 1))-1" & vbCrLf
sConsulta = sConsulta & "               END" & vbCrLf
sConsulta = sConsulta & "           )=@UON1 + @SEPARADOR + @OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- COPIA_LINEA_DESGLOSE" & vbCrLf
sConsulta = sConsulta & "--     CENTROS" & vbCrLf
sConsulta = sConsulta & "UPDATE CLD SET VALOR_TEXT = COALESCE(STUFF(CLD.VALOR_TEXT, LEN(@UON1 + @SEPARADOR) + 1, LEN(@OLD), @NEW) , CLD.VALOR_TEXT)" & vbCrLf
sConsulta = sConsulta & "FROM COPIA_LINEA_DESGLOSE CLD WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_CAMPO CC WITH (NOLOCK) ON CLD.CAMPO_HIJO = CC.ID " & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_CAMPO_DEF CCD WITH (NOLOCK) ON CC.COPIA_CAMPO_DEF = CCD.ID" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "   CCD.TIPO_CAMPO_GS = 129 AND " & vbCrLf
sConsulta = sConsulta & "   CLD.VALOR_TEXT IS NOT NULL AND " & vbCrLf
sConsulta = sConsulta & "   SUBSTRING(CLD.VALOR_TEXT, " & vbCrLf
sConsulta = sConsulta & "             1, " & vbCrLf
sConsulta = sConsulta & "             CASE WHEN " & vbCrLf
sConsulta = sConsulta & "               CHARINDEX(@SEPARADOR,CLD.VALOR_TEXT, LEN(@UON1 + @SEPARADOR) + 1)=0 THEN " & vbCrLf
sConsulta = sConsulta & "                   LEN(CLD.VALOR_TEXT) " & vbCrLf
sConsulta = sConsulta & "               ELSE " & vbCrLf
sConsulta = sConsulta & "                   CHARINDEX(@SEPARADOR,CLD.VALOR_TEXT, LEN(@UON1 + @SEPARADOR) + 1)-1" & vbCrLf
sConsulta = sConsulta & "               END" & vbCrLf
sConsulta = sConsulta & "           )=@UON1 + @SEPARADOR + @OLD" & vbCrLf
sConsulta = sConsulta & "--     PARTIDAS" & vbCrLf
sConsulta = sConsulta & "UPDATE CLD SET VALOR_TEXT = COALESCE(STUFF(CLD.VALOR_TEXT, LEN(@UON1 + @SEPARADOR) + 1, LEN(@OLD), @NEW) , CLD.VALOR_TEXT)" & vbCrLf
sConsulta = sConsulta & "FROM COPIA_LINEA_DESGLOSE CLD WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_CAMPO CC WITH (NOLOCK) ON CLD.CAMPO_HIJO = CC.ID " & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_CAMPO_DEF CCD WITH (NOLOCK) ON CC.COPIA_CAMPO_DEF = CCD.ID" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "   CCD.TIPO_CAMPO_GS = 130 AND " & vbCrLf
sConsulta = sConsulta & "   CLD.VALOR_TEXT IS NOT NULL AND " & vbCrLf
sConsulta = sConsulta & "   SUBSTRING(SUBSTRING(CLD.VALOR_TEXT, 1, LEN(CLD.VALOR_TEXT) - CHARINDEX(@SEPARADOR, REVERSE(CLD.VALOR_TEXT))),  --QUITAMOS LA PARTIDA PRES" & vbCrLf
sConsulta = sConsulta & "             1, " & vbCrLf
sConsulta = sConsulta & "             CASE WHEN " & vbCrLf
sConsulta = sConsulta & "               CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                         SUBSTRING(CLD.VALOR_TEXT, 1, LEN(CLD.VALOR_TEXT) - CHARINDEX(@SEPARADOR, REVERSE(CLD.VALOR_TEXT))), " & vbCrLf
sConsulta = sConsulta & "                         CHARINDEX(@SEPARADOR,CLD.VALOR_TEXT, LEN(@UON1 + @SEPARADOR) + 1)" & vbCrLf
sConsulta = sConsulta & "                         )=0 THEN " & vbCrLf
sConsulta = sConsulta & "                   LEN(SUBSTRING(CLD.VALOR_TEXT, 1, LEN(CLD.VALOR_TEXT) - CHARINDEX(@SEPARADOR, REVERSE(CLD.VALOR_TEXT))))" & vbCrLf
sConsulta = sConsulta & "               ELSE " & vbCrLf
sConsulta = sConsulta & "                   CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                             SUBSTRING(CLD.VALOR_TEXT, 1, LEN(CLD.VALOR_TEXT) - CHARINDEX(@SEPARADOR, REVERSE(CLD.VALOR_TEXT))), " & vbCrLf
sConsulta = sConsulta & "                             CHARINDEX(@SEPARADOR,CLD.VALOR_TEXT, LEN(@UON1 + @SEPARADOR) + 1))-1" & vbCrLf
sConsulta = sConsulta & "               END" & vbCrLf
sConsulta = sConsulta & "           )=@UON1 + @SEPARADOR + @OLD" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "-- INSTANCIA_CENTROS_PARTIDAS" & vbCrLf
sConsulta = sConsulta & "--     CENTROS" & vbCrLf
sConsulta = sConsulta & "UPDATE ICP SET VALOR_TEXT = COALESCE(STUFF(ICP.VALOR_TEXT, LEN(@UON1 + @SEPARADOR) + 1, LEN(@OLD), @NEW) , VALOR_TEXT)" & vbCrLf
sConsulta = sConsulta & "FROM INSTANCIA_CENTROS_PARTIDAS ICP WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "ICP.TIPO_CAMPO_GS = 129 AND " & vbCrLf
sConsulta = sConsulta & "ICP.VALOR_TEXT IS NOT NULL AND" & vbCrLf
sConsulta = sConsulta & "SUBSTRING(ICP.VALOR_TEXT, " & vbCrLf
sConsulta = sConsulta & "         1, " & vbCrLf
sConsulta = sConsulta & "         CASE WHEN " & vbCrLf
sConsulta = sConsulta & "           CHARINDEX(@SEPARADOR,ICP.VALOR_TEXT, LEN(@UON1 + @SEPARADOR) + 1)=0 THEN " & vbCrLf
sConsulta = sConsulta & "               LEN(ICP.VALOR_TEXT) " & vbCrLf
sConsulta = sConsulta & "           ELSE " & vbCrLf
sConsulta = sConsulta & "               CHARINDEX(@SEPARADOR,ICP.VALOR_TEXT, LEN(@UON1 + @SEPARADOR) + 1)-1" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "       )=@UON1 + @SEPARADOR + @OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--     PARTIDAS" & vbCrLf
sConsulta = sConsulta & "UPDATE ICP SET VALOR_TEXT = COALESCE(STUFF(ICP.VALOR_TEXT, LEN(@UON1 + @SEPARADOR) + 1, LEN(@OLD), @NEW) , VALOR_TEXT)" & vbCrLf
sConsulta = sConsulta & "FROM INSTANCIA_CENTROS_PARTIDAS ICP WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "ICP.TIPO_CAMPO_GS  = 130 AND " & vbCrLf
sConsulta = sConsulta & "ICP.VALOR_TEXT IS NOT NULL AND" & vbCrLf
sConsulta = sConsulta & "SUBSTRING(SUBSTRING(ICP.VALOR_TEXT, 1, LEN(ICP.VALOR_TEXT) - CHARINDEX(@SEPARADOR, REVERSE(ICP.VALOR_TEXT))),  --QUITAMOS LA PARTIDA PRES" & vbCrLf
sConsulta = sConsulta & "         1, " & vbCrLf
sConsulta = sConsulta & "         CASE WHEN " & vbCrLf
sConsulta = sConsulta & "           CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                     SUBSTRING(ICP.VALOR_TEXT, 1, LEN(ICP.VALOR_TEXT) - CHARINDEX(@SEPARADOR, REVERSE(ICP.VALOR_TEXT))), " & vbCrLf
sConsulta = sConsulta & "                     CHARINDEX(@SEPARADOR,ICP.VALOR_TEXT, LEN(@UON1 + @SEPARADOR) + 1)" & vbCrLf
sConsulta = sConsulta & "                     )=0 THEN " & vbCrLf
sConsulta = sConsulta & "               LEN(SUBSTRING(ICP.VALOR_TEXT, 1, LEN(ICP.VALOR_TEXT) - CHARINDEX(@SEPARADOR, REVERSE(ICP.VALOR_TEXT))))" & vbCrLf
sConsulta = sConsulta & "           ELSE " & vbCrLf
sConsulta = sConsulta & "               CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                         SUBSTRING(ICP.VALOR_TEXT, 1, LEN(ICP.VALOR_TEXT) - CHARINDEX(@SEPARADOR, REVERSE(ICP.VALOR_TEXT))), " & vbCrLf
sConsulta = sConsulta & "                         CHARINDEX(@SEPARADOR,ICP.VALOR_TEXT, LEN(@UON1 + @SEPARADOR) + 1))-1" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "       )=@UON1 + @SEPARADOR + @OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- PM_CONTR_FILTRO_USU_BUSQUEDA" & vbCrLf
sConsulta = sConsulta & "UPDATE P SET CENTRO_COSTE = COALESCE(STUFF(CENTRO_COSTE, LEN(@UON1 + @SEPARADOR) + 1, LEN(@OLD), @NEW) , CENTRO_COSTE)" & vbCrLf
sConsulta = sConsulta & "FROM PM_CONTR_FILTRO_USU_BUSQUEDA P WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "P.CENTRO_COSTE IS NOT NULL AND" & vbCrLf
sConsulta = sConsulta & "SUBSTRING(P.CENTRO_COSTE, " & vbCrLf
sConsulta = sConsulta & "         1, " & vbCrLf
sConsulta = sConsulta & "         CASE WHEN " & vbCrLf
sConsulta = sConsulta & "           CHARINDEX(@SEPARADOR,P.CENTRO_COSTE, LEN(@UON1 + @SEPARADOR) + 1)=0 THEN " & vbCrLf
sConsulta = sConsulta & "               LEN(P.CENTRO_COSTE) " & vbCrLf
sConsulta = sConsulta & "           ELSE " & vbCrLf
sConsulta = sConsulta & "               CHARINDEX(@SEPARADOR,P.CENTRO_COSTE, LEN(@UON1 + @SEPARADOR) + 1)-1" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "       )=@UON1 + @SEPARADOR + @OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE P SET PARTIDA1 = COALESCE(STUFF(PARTIDA1, LEN(@UON1 + @SEPARADOR) + 1, LEN(@OLD), @NEW) , PARTIDA1)" & vbCrLf
sConsulta = sConsulta & "FROM PM_CONTR_FILTRO_USU_BUSQUEDA P WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "P.PARTIDA1 IS NOT NULL AND" & vbCrLf
sConsulta = sConsulta & "SUBSTRING(SUBSTRING(P.PARTIDA1, 1, LEN(P.PARTIDA1) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA1))),  --QUITAMOS LA PARTIDA PRES" & vbCrLf
sConsulta = sConsulta & "         1, " & vbCrLf
sConsulta = sConsulta & "         CASE WHEN " & vbCrLf
sConsulta = sConsulta & "           CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                     SUBSTRING(P.PARTIDA1, 1, LEN(P.PARTIDA1) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA1))), " & vbCrLf
sConsulta = sConsulta & "                     CHARINDEX(@SEPARADOR,P.PARTIDA1, LEN(@UON1 + @SEPARADOR) + 1)" & vbCrLf
sConsulta = sConsulta & "                     )=0 THEN " & vbCrLf
sConsulta = sConsulta & "               LEN(SUBSTRING(P.PARTIDA1, 1, LEN(P.PARTIDA1) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA1))))" & vbCrLf
sConsulta = sConsulta & "           ELSE " & vbCrLf
sConsulta = sConsulta & "               CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                         SUBSTRING(P.PARTIDA1, 1, LEN(P.PARTIDA1) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA1))), " & vbCrLf
sConsulta = sConsulta & "                         CHARINDEX(@SEPARADOR,P.PARTIDA1, LEN(@UON1 + @SEPARADOR) + 1))-1" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "       )=@UON1 + @SEPARADOR + @OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE P SET PARTIDA2 = COALESCE(STUFF(PARTIDA2, LEN(@UON1 + @SEPARADOR) + 1, LEN(@OLD), @NEW) , PARTIDA2)" & vbCrLf
sConsulta = sConsulta & "FROM PM_CONTR_FILTRO_USU_BUSQUEDA P WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "P.PARTIDA2 IS NOT NULL AND" & vbCrLf
sConsulta = sConsulta & "SUBSTRING(SUBSTRING(P.PARTIDA2, 1, LEN(P.PARTIDA2) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA2))),  --QUITAMOS LA PARTIDA PRES" & vbCrLf
sConsulta = sConsulta & "         1, " & vbCrLf
sConsulta = sConsulta & "         CASE WHEN " & vbCrLf
sConsulta = sConsulta & "           CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                     SUBSTRING(P.PARTIDA2, 1, LEN(P.PARTIDA2) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA2))), " & vbCrLf
sConsulta = sConsulta & "                     CHARINDEX(@SEPARADOR,P.PARTIDA2, LEN(@UON1 + @SEPARADOR) + 1)" & vbCrLf
sConsulta = sConsulta & "                     )=0 THEN " & vbCrLf
sConsulta = sConsulta & "               LEN(SUBSTRING(P.PARTIDA2, 1, LEN(P.PARTIDA2) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA2))))" & vbCrLf
sConsulta = sConsulta & "           ELSE " & vbCrLf
sConsulta = sConsulta & "               CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                         SUBSTRING(P.PARTIDA2, 1, LEN(P.PARTIDA2) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA2))), " & vbCrLf
sConsulta = sConsulta & "                         CHARINDEX(@SEPARADOR,P.PARTIDA2, LEN(@UON1 + @SEPARADOR) + 1))-1" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "       )=@UON1 + @SEPARADOR + @OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE P SET PARTIDA3 = COALESCE(STUFF(PARTIDA3, LEN(@UON1 + @SEPARADOR) + 1, LEN(@OLD), @NEW) , PARTIDA3)" & vbCrLf
sConsulta = sConsulta & "FROM PM_CONTR_FILTRO_USU_BUSQUEDA P WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "P.PARTIDA3 IS NOT NULL AND" & vbCrLf
sConsulta = sConsulta & "SUBSTRING(SUBSTRING(P.PARTIDA3, 1, LEN(P.PARTIDA3) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA3))),  --QUITAMOS LA PARTIDA PRES" & vbCrLf
sConsulta = sConsulta & "         1, " & vbCrLf
sConsulta = sConsulta & "         CASE WHEN " & vbCrLf
sConsulta = sConsulta & "           CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                     SUBSTRING(P.PARTIDA3, 1, LEN(P.PARTIDA3) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA3))), " & vbCrLf
sConsulta = sConsulta & "                     CHARINDEX(@SEPARADOR,P.PARTIDA3, LEN(@UON1 + @SEPARADOR) + 1)" & vbCrLf
sConsulta = sConsulta & "                     )=0 THEN " & vbCrLf
sConsulta = sConsulta & "               LEN(SUBSTRING(P.PARTIDA3, 1, LEN(P.PARTIDA3) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA3))))" & vbCrLf
sConsulta = sConsulta & "           ELSE " & vbCrLf
sConsulta = sConsulta & "               CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                         SUBSTRING(P.PARTIDA3, 1, LEN(P.PARTIDA3) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA3))), " & vbCrLf
sConsulta = sConsulta & "                         CHARINDEX(@SEPARADOR,P.PARTIDA3, LEN(@UON1 + @SEPARADOR) + 1))-1" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "       )=@UON1 + @SEPARADOR + @OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE P SET PARTIDA4 = COALESCE(STUFF(PARTIDA4, LEN(@UON1 + @SEPARADOR) + 1, LEN(@OLD), @NEW) , PARTIDA4)" & vbCrLf
sConsulta = sConsulta & "FROM PM_CONTR_FILTRO_USU_BUSQUEDA P WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "P.PARTIDA4 IS NOT NULL AND" & vbCrLf
sConsulta = sConsulta & "SUBSTRING(SUBSTRING(P.PARTIDA4, 1, LEN(P.PARTIDA4) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA4))),  --QUITAMOS LA PARTIDA PRES" & vbCrLf
sConsulta = sConsulta & "         1, " & vbCrLf
sConsulta = sConsulta & "         CASE WHEN " & vbCrLf
sConsulta = sConsulta & "           CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                     SUBSTRING(P.PARTIDA4, 1, LEN(P.PARTIDA4) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA4))), " & vbCrLf
sConsulta = sConsulta & "                     CHARINDEX(@SEPARADOR,P.PARTIDA4, LEN(@UON1 + @SEPARADOR) + 1)" & vbCrLf
sConsulta = sConsulta & "                     )=0 THEN " & vbCrLf
sConsulta = sConsulta & "               LEN(SUBSTRING(P.PARTIDA4, 1, LEN(P.PARTIDA4) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA4))))" & vbCrLf
sConsulta = sConsulta & "           ELSE " & vbCrLf
sConsulta = sConsulta & "               CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                         SUBSTRING(P.PARTIDA4, 1, LEN(P.PARTIDA4) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA4))), " & vbCrLf
sConsulta = sConsulta & "                         CHARINDEX(@SEPARADOR,P.PARTIDA4, LEN(@UON1 + @SEPARADOR) + 1))-1" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "       )=@UON1 + @SEPARADOR + @OLD" & vbCrLf
sConsulta = sConsulta & "--FIN ACTUALIZACION COPIA_CAMPO, COPIA_LINEA_DESGLOSE, INSTANCIA_CENTROS_PARTIDAS, PM_CONTR_FILTRO_USU_BUSQUEDA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON2_DEP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3_DEP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PER CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON2_DEST CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3_DEST CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISOR CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CERTIFICADO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE NOCONFORMIDAD CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE APROBADORES CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PETICIONARIOS CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_ROL CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_ENLACE_CONDICIONES CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_COPIA_ROL CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_COPIA_ENLACE_CONDICION CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_UON CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU_CC_CONTROL CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU_CC_IMPUTACION CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES5_UON CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CENTRO_SM_UON CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON4_DEST CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_PED_IMPUTACION CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PERF_UON CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_CAT_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_GRUPO_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_MEN_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "CLOSE C" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "COMMIT TRANSACTION" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT OFF" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UON3_COD]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[UON3_COD]" & vbCrLf
    gRDOCon.QueryTimeout = 7200
    ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [DBO].[UON3_COD] (@OLD VARCHAR(50),@NEW VARCHAR(50), @UON1 VARCHAR(50), @UON2 VARCHAR(50))  AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT ON" & vbCrLf
sConsulta = sConsulta & "BEGIN TRANSACTION" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR FOR SELECT COD FROM UON3 WITH(NOLOCK) WHERE UON1=@UON1 AND UON2=@UON2 AND COD=@OLD" & vbCrLf
sConsulta = sConsulta & "OPEN C" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C INTO @VAR_COD" & vbCrLf
sConsulta = sConsulta & "IF @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3_DEP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PER NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3_DEST NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISOR NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CERTIFICADO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE NOCONFORMIDAD NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE APROBADORES NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PETICIONARIOS NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_ROL NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_ENLACE_CONDICIONES NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_COPIA_ROL NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_COPIA_ENLACE_CONDICION NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_UON NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU_CC_CONTROL NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU_CC_IMPUTACION NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES5_UON NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CENTRO_SM_UON NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON4_DEST NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_PED_IMPUTACION NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PERF_UON NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_CAT_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_GRUPO_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_MEN_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE UON3 SET COD=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND COD=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON4 SET UON3 = @NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON3_DEP SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PER SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE COM SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_UON3 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_UON3 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON3_DEST SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_UON3 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_UON3 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES1_NIV1 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES1_NIV2 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES1_NIV3 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES1_NIV4 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES2_NIV1 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES2_NIV2 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES2_NIV3 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES2_NIV4 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES3_NIV1 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES3_NIV2 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES3_NIV3 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES3_NIV4 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES4_NIV1 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES4_NIV2 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES4_NIV3 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES4_NIV4 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_1 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_2 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_3 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_4 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_1 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_2 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_3 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_4 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON1 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON2 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON3 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON4 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY1 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY2 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY3 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY4 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_PRESCON3 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_PRESCON4 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISOR SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CERTIFICADO SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE NOCONFORMIDAD SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE APROBADORES SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PETICIONARIOS SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PM_ROL SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PM_COPIA_ROL SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4_UON SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET UON3_APER=@NEW WHERE UON1_APER=@UON1 AND UON2_APER=@UON2 AND UON3_APER=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE USU_CC_CONTROL SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE USU_CC_IMPUTACION SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES5_UON SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CENTRO_SM_UON SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON4_DEST SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE LINEAS_PED_IMPUTACION SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PERF_UON SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_CAT_UON3 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_GRUPO_UON3 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_MEN_UON3 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @LEN_COD_UON1 AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @LEN_COD_UON2 AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @LEN_COD_UON3 AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE C2 CURSOR FOR SELECT (SELECT LONGITUD FROM DIC WITH(NOLOCK) WHERE NOMBRE='UON1') AS L1,(SELECT LONGITUD FROM DIC WITH(NOLOCK) WHERE NOMBRE='UON2') AS L2,(SELECT LONGITUD FROM DIC WITH(NOLOCK) WHERE NOMBRE='UON3') AS L3" & vbCrLf
sConsulta = sConsulta & "OPEN C2" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C2 INTO @LEN_COD_UON1,@LEN_COD_UON2,@LEN_COD_UON3" & vbCrLf
sConsulta = sConsulta & "IF @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "  UPDATE PM_ENLACE_CONDICIONES SET VALOR_TEXT=STUFF(VALOR_TEXT,@LEN_COD_UON1 + @LEN_COD_UON2 + 3,@LEN_COD_UON3,@NEW + SPACE(@LEN_COD_UON3 - LEN(@NEW))) WHERE TIPO_CAMPO=4 AND TIPO_VALOR=2 AND LTRIM(RTRIM(SUBSTRING(VALOR_TEXT,1,@LEN_COD_UON1)))=@UON1 AND LTRIM(RTRIM(SUBSTRING(VALOR_TEXT,@LEN_COD_UON1+2, @LEN_COD_UON2)))=@UON2 AND LTRIM(RTRIM(SUBSTRING(VALOR_TEXT,@LEN_COD_UON1+@LEN_COD_UON2+3, @LEN_COD_UON3)))=@OLD" & vbCrLf
sConsulta = sConsulta & "  UPDATE PM_COPIA_ENLACE_CONDICION SET VALOR_TEXT=STUFF(VALOR_TEXT,@LEN_COD_UON1 + @LEN_COD_UON2 + 3,@LEN_COD_UON3,@NEW + SPACE(@LEN_COD_UON3 - LEN(@NEW))) WHERE TIPO_CAMPO=4 AND TIPO_VALOR=2 AND LTRIM(RTRIM(SUBSTRING(VALOR_TEXT,1,@LEN_COD_UON1)))=@UON1 AND LTRIM(RTRIM(SUBSTRING(VALOR_TEXT,@LEN_COD_UON1+2, @LEN_COD_UON2)))=@UON2 AND LTRIM(RTRIM(SUBSTRING(VALOR_TEXT,@LEN_COD_UON1+@LEN_COD_UON2+3, @LEN_COD_UON3)))=@OLD" & vbCrLf
sConsulta = sConsulta & "CLOSE C2" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C2" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--INICIO ACTUALIZACION COPIA_CAMPO, COPIA_LINEA_DESGLOSE, INSTANCIA_CENTROS_PARTIDAS, PM_CONTR_FILTRO_USU_BUSQUEDA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--ACTUALIZAR DATOS DE UONS EN LOS CENTROS Y PARTIDAS GRABADOS EN " & vbCrLf
sConsulta = sConsulta & "   --COPIA_CAMPO" & vbCrLf
sConsulta = sConsulta & "   --COPIA_LINEA_DESGLOSE" & vbCrLf
sConsulta = sConsulta & "   --INSTANCIA_CENTROS_DESGLOSE --> TABLA EN LA QUE SE HACE COPIA DE LOS DATOS DE CENTROS Y PARTIDAS DE COPIA_CAMPO Y COPIA_LINEA_DESGLOSE PARA ACELERAR FILTRADOS EN EL BUSCADOR DEL VISOR DE CONTRATOS" & vbCrLf
sConsulta = sConsulta & "   --PM_CONTR_FILTRO_USU_BUSQUEDA --> TABLA EN LA QUE SE HACE COPIA DE LOS VALORES POR DEFECTO PARA LOS FILTROS DEL BUSCADOR DEL VISOR DE CONTRATOS, QUE INCLUYE, ENTRE OTRAS COSAS CENTROS Y PARTIDAS." & vbCrLf
sConsulta = sConsulta & "--HAY QUE MODIFICARLO PORQUE EL MODO EN QUE SE GRABAN CENTROS Y PARTIDAS ES:" & vbCrLf
sConsulta = sConsulta & "--CENTROS: UON1#UON2#... HASTA EL NIVEL AL QUE EST� VINCULADO EL CENTRO" & vbCrLf
sConsulta = sConsulta & "--PARTIDAS: UON1#UON2#...#PRESN: UONS HASTA EL NIVEL AL QUE EST� VINCULADA LA PARTIDA Y PRESN, DONDE N ES EL NIVEL DE IMPUTACION QUE SE INDICA EN PARGEN_SM.IMP_NIVEL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- QU� ES MEJOR PARA BUSCAR LAS UON EN LOS CAMPOS, GUIARSE POR LA LONGITUD DEL C�DIGO UON O POR EL SEPARADOR #: " & vbCrLf
sConsulta = sConsulta & "-- DADO QUE SE PUEDE CAMBIAR LA LONGITUD EN LA TABLA DIC Y QUE EN LAS 4 TABLAS AL GUARDAR LOS DATOS NO SE TIENE EN CUENTA ESA LONGITUD, USAREMOS #" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--EN LAS PARTIDAS, EL �LTIMO VALOR ES SIEMPRE UNA PARTIDA PRESUPUESTARIA. AL MENOS HABR� SIEMPRE UN UON (UON1) PERO A PARTIR DE LOS UON2, PUEDE QUE NO HAYA, POR LO QUE TENEMOS QUE ASEGURARNOS DE NO CONFUNDIR LA PARTIDA CON UN UON AL REEMPLAZAR: VAMOS A IGNORAR LA PARTIDA AL FILTRAR" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- COPIA_CAMPO" & vbCrLf
sConsulta = sConsulta & "--     CENTROS" & vbCrLf
sConsulta = sConsulta & "DECLARE @SEPARADOR VARCHAR(3) = '#'" & vbCrLf
sConsulta = sConsulta & "UPDATE CC SET VALOR_TEXT = COALESCE(STUFF(CC.VALOR_TEXT, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1, LEN(@OLD), @NEW) , VALOR_TEXT)" & vbCrLf
sConsulta = sConsulta & "FROM COPIA_CAMPO CC WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_CAMPO_DEF CD WITH(NOLOCK) ON CC.COPIA_CAMPO_DEF=CD.ID" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "   CD.TIPO_CAMPO_GS = 129 AND " & vbCrLf
sConsulta = sConsulta & "   CC.VALOR_TEXT IS NOT NULL AND " & vbCrLf
sConsulta = sConsulta & "   SUBSTRING(CC.VALOR_TEXT, " & vbCrLf
sConsulta = sConsulta & "             1, " & vbCrLf
sConsulta = sConsulta & "             CASE WHEN " & vbCrLf
sConsulta = sConsulta & "               CHARINDEX(@SEPARADOR,CC.VALOR_TEXT, CHARINDEX(@SEPARADOR,CC.VALOR_TEXT, CHARINDEX (@SEPARADOR,CC.VALOR_TEXT) + 1) + 1)=0 THEN " & vbCrLf
sConsulta = sConsulta & "                   LEN(CC.VALOR_TEXT) " & vbCrLf
sConsulta = sConsulta & "               ELSE " & vbCrLf
sConsulta = sConsulta & "                   CHARINDEX(@SEPARADOR,CC.VALOR_TEXT, CHARINDEX(@SEPARADOR,CC.VALOR_TEXT, CHARINDEX (@SEPARADOR,CC.VALOR_TEXT) + 1) + 1)-1" & vbCrLf
sConsulta = sConsulta & "               END" & vbCrLf
sConsulta = sConsulta & "           )=@UON1 + @SEPARADOR + @UON2 + @SEPARADOR + @OLD" & vbCrLf
sConsulta = sConsulta & "--     PARTIDAS" & vbCrLf
sConsulta = sConsulta & "UPDATE CC SET VALOR_TEXT = COALESCE(STUFF(CC.VALOR_TEXT, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1, LEN(@OLD), @NEW) , VALOR_TEXT)" & vbCrLf
sConsulta = sConsulta & "FROM COPIA_CAMPO CC WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_CAMPO_DEF CD WITH(NOLOCK) ON CC.COPIA_CAMPO_DEF=CD.ID" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "   CD.TIPO_CAMPO_GS = 130 AND " & vbCrLf
sConsulta = sConsulta & "   CC.VALOR_TEXT IS NOT NULL AND " & vbCrLf
sConsulta = sConsulta & "   SUBSTRING(SUBSTRING(CC.VALOR_TEXT, 1, LEN(CC.VALOR_TEXT) - CHARINDEX(@SEPARADOR, REVERSE(CC.VALOR_TEXT))),  --QUITAMOS LA PARTIDA PRES" & vbCrLf
sConsulta = sConsulta & "             1, " & vbCrLf
sConsulta = sConsulta & "             CASE WHEN " & vbCrLf
sConsulta = sConsulta & "               CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                         SUBSTRING(CC.VALOR_TEXT, 1, LEN(CC.VALOR_TEXT) - CHARINDEX(@SEPARADOR, REVERSE(CC.VALOR_TEXT))), " & vbCrLf
sConsulta = sConsulta & "                         CHARINDEX(@SEPARADOR,CC.VALOR_TEXT, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1)" & vbCrLf
sConsulta = sConsulta & "                         )=0 THEN " & vbCrLf
sConsulta = sConsulta & "                   LEN(SUBSTRING(CC.VALOR_TEXT, 1, LEN(CC.VALOR_TEXT) - CHARINDEX(@SEPARADOR, REVERSE(CC.VALOR_TEXT))))" & vbCrLf
sConsulta = sConsulta & "               ELSE " & vbCrLf
sConsulta = sConsulta & "                   CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                             SUBSTRING(CC.VALOR_TEXT, 1, LEN(CC.VALOR_TEXT) - CHARINDEX(@SEPARADOR, REVERSE(CC.VALOR_TEXT))), " & vbCrLf
sConsulta = sConsulta & "                             CHARINDEX(@SEPARADOR,CC.VALOR_TEXT, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1))-1" & vbCrLf
sConsulta = sConsulta & "               END" & vbCrLf
sConsulta = sConsulta & "           )=@UON1 + @SEPARADOR + @UON2 + @SEPARADOR + @OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- COPIA_LINEA_DESGLOSE" & vbCrLf
sConsulta = sConsulta & "--     CENTROS" & vbCrLf
sConsulta = sConsulta & "UPDATE CLD SET VALOR_TEXT = COALESCE(STUFF(CLD.VALOR_TEXT, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1, LEN(@OLD), @NEW) , CLD.VALOR_TEXT)" & vbCrLf
sConsulta = sConsulta & "FROM COPIA_LINEA_DESGLOSE CLD WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_CAMPO CC WITH (NOLOCK) ON CLD.CAMPO_HIJO = CC.ID " & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_CAMPO_DEF CCD WITH (NOLOCK) ON CC.COPIA_CAMPO_DEF = CCD.ID" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "   CCD.TIPO_CAMPO_GS = 129 AND " & vbCrLf
sConsulta = sConsulta & "   CLD.VALOR_TEXT IS NOT NULL AND " & vbCrLf
sConsulta = sConsulta & "   SUBSTRING(CLD.VALOR_TEXT, " & vbCrLf
sConsulta = sConsulta & "             1, " & vbCrLf
sConsulta = sConsulta & "             CASE WHEN " & vbCrLf
sConsulta = sConsulta & "               CHARINDEX(@SEPARADOR,CLD.VALOR_TEXT, CHARINDEX(@SEPARADOR,CLD.VALOR_TEXT, CHARINDEX (@SEPARADOR,CLD.VALOR_TEXT) + 1) + 1)=0 THEN " & vbCrLf
sConsulta = sConsulta & "                   LEN(CLD.VALOR_TEXT) " & vbCrLf
sConsulta = sConsulta & "               ELSE " & vbCrLf
sConsulta = sConsulta & "                   CHARINDEX(@SEPARADOR,CLD.VALOR_TEXT, CHARINDEX(@SEPARADOR,CLD.VALOR_TEXT, CHARINDEX (@SEPARADOR,CLD.VALOR_TEXT) + 1) + 1)-1" & vbCrLf
sConsulta = sConsulta & "               END" & vbCrLf
sConsulta = sConsulta & "           )=@UON1 + @SEPARADOR + @UON2 + @SEPARADOR + @OLD" & vbCrLf
sConsulta = sConsulta & "--     PARTIDAS" & vbCrLf
sConsulta = sConsulta & "UPDATE CLD SET VALOR_TEXT = COALESCE(STUFF(CLD.VALOR_TEXT, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1, LEN(@OLD), @NEW) , CLD.VALOR_TEXT)" & vbCrLf
sConsulta = sConsulta & "FROM COPIA_LINEA_DESGLOSE CLD WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_CAMPO CC WITH (NOLOCK) ON CLD.CAMPO_HIJO = CC.ID " & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_CAMPO_DEF CCD WITH (NOLOCK) ON CC.COPIA_CAMPO_DEF = CCD.ID" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "   CCD.TIPO_CAMPO_GS = 130 AND " & vbCrLf
sConsulta = sConsulta & "   CLD.VALOR_TEXT IS NOT NULL AND " & vbCrLf
sConsulta = sConsulta & "   SUBSTRING(SUBSTRING(CLD.VALOR_TEXT, 1, LEN(CLD.VALOR_TEXT) - CHARINDEX(@SEPARADOR, REVERSE(CLD.VALOR_TEXT))),  --QUITAMOS LA PARTIDA PRES" & vbCrLf
sConsulta = sConsulta & "             1, " & vbCrLf
sConsulta = sConsulta & "             CASE WHEN " & vbCrLf
sConsulta = sConsulta & "               CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                         SUBSTRING(CLD.VALOR_TEXT, 1, LEN(CLD.VALOR_TEXT) - CHARINDEX(@SEPARADOR, REVERSE(CLD.VALOR_TEXT))), " & vbCrLf
sConsulta = sConsulta & "                         CHARINDEX(@SEPARADOR,CLD.VALOR_TEXT, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1)" & vbCrLf
sConsulta = sConsulta & "                         )=0 THEN " & vbCrLf
sConsulta = sConsulta & "                   LEN(SUBSTRING(CLD.VALOR_TEXT, 1, LEN(CLD.VALOR_TEXT) - CHARINDEX(@SEPARADOR, REVERSE(CLD.VALOR_TEXT))))" & vbCrLf
sConsulta = sConsulta & "               ELSE " & vbCrLf
sConsulta = sConsulta & "                   CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                             SUBSTRING(CLD.VALOR_TEXT, 1, LEN(CLD.VALOR_TEXT) - CHARINDEX(@SEPARADOR, REVERSE(CLD.VALOR_TEXT))), " & vbCrLf
sConsulta = sConsulta & "                             CHARINDEX(@SEPARADOR,CLD.VALOR_TEXT, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1))-1" & vbCrLf
sConsulta = sConsulta & "               END" & vbCrLf
sConsulta = sConsulta & "           )=@UON1 + @SEPARADOR + @UON2 + @SEPARADOR + @OLD" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "-- INSTANCIA_CENTROS_PARTIDAS" & vbCrLf
sConsulta = sConsulta & "--     CENTROS" & vbCrLf
sConsulta = sConsulta & "UPDATE ICP SET VALOR_TEXT = COALESCE(STUFF(ICP.VALOR_TEXT, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1, LEN(@OLD), @NEW) , VALOR_TEXT)" & vbCrLf
sConsulta = sConsulta & "FROM INSTANCIA_CENTROS_PARTIDAS ICP WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "ICP.TIPO_CAMPO_GS = 129 AND " & vbCrLf
sConsulta = sConsulta & "ICP.VALOR_TEXT IS NOT NULL AND" & vbCrLf
sConsulta = sConsulta & "SUBSTRING(ICP.VALOR_TEXT, " & vbCrLf
sConsulta = sConsulta & "         1, " & vbCrLf
sConsulta = sConsulta & "         CASE WHEN " & vbCrLf
sConsulta = sConsulta & "           CHARINDEX(@SEPARADOR,ICP.VALOR_TEXT, CHARINDEX(@SEPARADOR,ICP.VALOR_TEXT, CHARINDEX (@SEPARADOR,ICP.VALOR_TEXT) + 1) + 1)=0 THEN " & vbCrLf
sConsulta = sConsulta & "               LEN(ICP.VALOR_TEXT) " & vbCrLf
sConsulta = sConsulta & "           ELSE " & vbCrLf
sConsulta = sConsulta & "               CHARINDEX(@SEPARADOR,ICP.VALOR_TEXT, CHARINDEX(@SEPARADOR,ICP.VALOR_TEXT, CHARINDEX (@SEPARADOR,ICP.VALOR_TEXT) + 1) + 1)-1" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "       )=@UON1 + @SEPARADOR + @UON2 + @SEPARADOR + @OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--     PARTIDAS" & vbCrLf
sConsulta = sConsulta & "UPDATE ICP SET VALOR_TEXT = COALESCE(STUFF(ICP.VALOR_TEXT, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1, LEN(@OLD), @NEW) , VALOR_TEXT)" & vbCrLf
sConsulta = sConsulta & "FROM INSTANCIA_CENTROS_PARTIDAS ICP WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "ICP.TIPO_CAMPO_GS  = 130 AND " & vbCrLf
sConsulta = sConsulta & "ICP.VALOR_TEXT IS NOT NULL AND" & vbCrLf
sConsulta = sConsulta & "SUBSTRING(SUBSTRING(ICP.VALOR_TEXT, 1, LEN(ICP.VALOR_TEXT) - CHARINDEX(@SEPARADOR, REVERSE(ICP.VALOR_TEXT))),  --QUITAMOS LA PARTIDA PRES" & vbCrLf
sConsulta = sConsulta & "         1, " & vbCrLf
sConsulta = sConsulta & "         CASE WHEN " & vbCrLf
sConsulta = sConsulta & "           CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                     SUBSTRING(ICP.VALOR_TEXT, 1, LEN(ICP.VALOR_TEXT) - CHARINDEX(@SEPARADOR, REVERSE(ICP.VALOR_TEXT))), " & vbCrLf
sConsulta = sConsulta & "                     CHARINDEX(@SEPARADOR,ICP.VALOR_TEXT, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1)" & vbCrLf
sConsulta = sConsulta & "                     )=0 THEN " & vbCrLf
sConsulta = sConsulta & "               LEN(SUBSTRING(ICP.VALOR_TEXT, 1, LEN(ICP.VALOR_TEXT) - CHARINDEX(@SEPARADOR, REVERSE(ICP.VALOR_TEXT))))" & vbCrLf
sConsulta = sConsulta & "           ELSE " & vbCrLf
sConsulta = sConsulta & "               CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                         SUBSTRING(ICP.VALOR_TEXT, 1, LEN(ICP.VALOR_TEXT) - CHARINDEX(@SEPARADOR, REVERSE(ICP.VALOR_TEXT))), " & vbCrLf
sConsulta = sConsulta & "                         CHARINDEX(@SEPARADOR,ICP.VALOR_TEXT, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1))-1" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "       )=@UON1 + @SEPARADOR + @UON2 + @SEPARADOR + @OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- PM_CONTR_FILTRO_USU_BUSQUEDA" & vbCrLf
sConsulta = sConsulta & "UPDATE P SET CENTRO_COSTE = COALESCE(STUFF(CENTRO_COSTE, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1, LEN(@OLD), @NEW) , CENTRO_COSTE)" & vbCrLf
sConsulta = sConsulta & "FROM PM_CONTR_FILTRO_USU_BUSQUEDA P WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "P.CENTRO_COSTE IS NOT NULL AND" & vbCrLf
sConsulta = sConsulta & "SUBSTRING(P.CENTRO_COSTE, " & vbCrLf
sConsulta = sConsulta & "         1, " & vbCrLf
sConsulta = sConsulta & "         CASE WHEN " & vbCrLf
sConsulta = sConsulta & "           CHARINDEX(@SEPARADOR,P.CENTRO_COSTE, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1)=0 THEN " & vbCrLf
sConsulta = sConsulta & "               LEN(P.CENTRO_COSTE) " & vbCrLf
sConsulta = sConsulta & "           ELSE " & vbCrLf
sConsulta = sConsulta & "               CHARINDEX(@SEPARADOR,P.CENTRO_COSTE, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1)-1" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "       )=@UON1 + @SEPARADOR + @UON2 + @SEPARADOR + @OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE P SET PARTIDA1 = COALESCE(STUFF(PARTIDA1, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1, LEN(@OLD), @NEW) , PARTIDA1)" & vbCrLf
sConsulta = sConsulta & "FROM PM_CONTR_FILTRO_USU_BUSQUEDA P WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "P.PARTIDA1 IS NOT NULL AND" & vbCrLf
sConsulta = sConsulta & "SUBSTRING(SUBSTRING(P.PARTIDA1, 1, LEN(P.PARTIDA1) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA1))),  --QUITAMOS LA PARTIDA PRES" & vbCrLf
sConsulta = sConsulta & "         1, " & vbCrLf
sConsulta = sConsulta & "         CASE WHEN " & vbCrLf
sConsulta = sConsulta & "           CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                     SUBSTRING(P.PARTIDA1, 1, LEN(P.PARTIDA1) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA1))), " & vbCrLf
sConsulta = sConsulta & "                     CHARINDEX(@SEPARADOR,P.PARTIDA1, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1)" & vbCrLf
sConsulta = sConsulta & "                     )=0 THEN " & vbCrLf
sConsulta = sConsulta & "               LEN(SUBSTRING(P.PARTIDA1, 1, LEN(P.PARTIDA1) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA1))))" & vbCrLf
sConsulta = sConsulta & "           ELSE " & vbCrLf
sConsulta = sConsulta & "               CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                         SUBSTRING(P.PARTIDA1, 1, LEN(P.PARTIDA1) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA1))), " & vbCrLf
sConsulta = sConsulta & "                         CHARINDEX(@SEPARADOR,P.PARTIDA1, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1))-1" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "       )=@UON1 + @SEPARADOR + @UON2 + @SEPARADOR + @OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE P SET PARTIDA2 = COALESCE(STUFF(PARTIDA2, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1, LEN(@OLD), @NEW) , PARTIDA2)" & vbCrLf
sConsulta = sConsulta & "FROM PM_CONTR_FILTRO_USU_BUSQUEDA P WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "P.PARTIDA2 IS NOT NULL AND" & vbCrLf
sConsulta = sConsulta & "SUBSTRING(SUBSTRING(P.PARTIDA2, 1, LEN(P.PARTIDA2) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA2))),  --QUITAMOS LA PARTIDA PRES" & vbCrLf
sConsulta = sConsulta & "         1, " & vbCrLf
sConsulta = sConsulta & "         CASE WHEN " & vbCrLf
sConsulta = sConsulta & "           CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                     SUBSTRING(P.PARTIDA2, 1, LEN(P.PARTIDA2) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA2))), " & vbCrLf
sConsulta = sConsulta & "                     CHARINDEX(@SEPARADOR,P.PARTIDA2, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1)" & vbCrLf
sConsulta = sConsulta & "                     )=0 THEN " & vbCrLf
sConsulta = sConsulta & "               LEN(SUBSTRING(P.PARTIDA2, 1, LEN(P.PARTIDA2) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA2))))" & vbCrLf
sConsulta = sConsulta & "           ELSE " & vbCrLf
sConsulta = sConsulta & "               CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                         SUBSTRING(P.PARTIDA2, 1, LEN(P.PARTIDA2) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA2))), " & vbCrLf
sConsulta = sConsulta & "                         CHARINDEX(@SEPARADOR,P.PARTIDA2, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1))-1" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "       )=@UON1 + @SEPARADOR + @UON2 + @SEPARADOR + @OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE P SET PARTIDA3 = COALESCE(STUFF(PARTIDA3, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1, LEN(@OLD), @NEW) , PARTIDA3)" & vbCrLf
sConsulta = sConsulta & "FROM PM_CONTR_FILTRO_USU_BUSQUEDA P WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "P.PARTIDA3 IS NOT NULL AND" & vbCrLf
sConsulta = sConsulta & "SUBSTRING(SUBSTRING(P.PARTIDA3, 1, LEN(P.PARTIDA3) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA3))),  --QUITAMOS LA PARTIDA PRES" & vbCrLf
sConsulta = sConsulta & "         1, " & vbCrLf
sConsulta = sConsulta & "         CASE WHEN " & vbCrLf
sConsulta = sConsulta & "           CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                     SUBSTRING(P.PARTIDA3, 1, LEN(P.PARTIDA3) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA3))), " & vbCrLf
sConsulta = sConsulta & "                     CHARINDEX(@SEPARADOR,P.PARTIDA3, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1)" & vbCrLf
sConsulta = sConsulta & "                     )=0 THEN " & vbCrLf
sConsulta = sConsulta & "               LEN(SUBSTRING(P.PARTIDA3, 1, LEN(P.PARTIDA3) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA3))))" & vbCrLf
sConsulta = sConsulta & "           ELSE " & vbCrLf
sConsulta = sConsulta & "               CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                         SUBSTRING(P.PARTIDA3, 1, LEN(P.PARTIDA3) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA3))), " & vbCrLf
sConsulta = sConsulta & "                         CHARINDEX(@SEPARADOR,P.PARTIDA3, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1))-1" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "       )=@UON1 + @SEPARADOR + @UON2 + @SEPARADOR + @OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE P SET PARTIDA4 = COALESCE(STUFF(PARTIDA4, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1, LEN(@OLD), @NEW) , PARTIDA4)" & vbCrLf
sConsulta = sConsulta & "FROM PM_CONTR_FILTRO_USU_BUSQUEDA P WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "P.PARTIDA4 IS NOT NULL AND" & vbCrLf
sConsulta = sConsulta & "SUBSTRING(SUBSTRING(P.PARTIDA4, 1, LEN(P.PARTIDA4) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA4))),  --QUITAMOS LA PARTIDA PRES" & vbCrLf
sConsulta = sConsulta & "         1, " & vbCrLf
sConsulta = sConsulta & "         CASE WHEN " & vbCrLf
sConsulta = sConsulta & "           CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                     SUBSTRING(P.PARTIDA4, 1, LEN(P.PARTIDA4) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA4))), " & vbCrLf
sConsulta = sConsulta & "                     CHARINDEX(@SEPARADOR,P.PARTIDA4, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1)" & vbCrLf
sConsulta = sConsulta & "                     )=0 THEN " & vbCrLf
sConsulta = sConsulta & "               LEN(SUBSTRING(P.PARTIDA4, 1, LEN(P.PARTIDA4) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA4))))" & vbCrLf
sConsulta = sConsulta & "           ELSE " & vbCrLf
sConsulta = sConsulta & "               CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                         SUBSTRING(P.PARTIDA4, 1, LEN(P.PARTIDA4) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA4))), " & vbCrLf
sConsulta = sConsulta & "                         CHARINDEX(@SEPARADOR,P.PARTIDA4, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1))-1" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "       )=@UON1 + @SEPARADOR + @UON2 + @SEPARADOR + @OLD" & vbCrLf
sConsulta = sConsulta & "--FIN ACTUALIZACION COPIA_CAMPO, COPIA_LINEA_DESGLOSE, INSTANCIA_CENTROS_PARTIDAS, PM_CONTR_FILTRO_USU_BUSQUEDA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3_DEP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PER CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3_DEST CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISOR CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CERTIFICADO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE NOCONFORMIDAD CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE APROBADORES CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PETICIONARIOS CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_ROL CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_ENLACE_CONDICIONES CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_COPIA_ROL CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_COPIA_ENLACE_CONDICION CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_UON CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU_CC_CONTROL CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU_CC_IMPUTACION CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES5_UON CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CENTRO_SM_UON CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON4_DEST CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_PED_IMPUTACION CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PERF_UON CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_CAT_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_GRUPO_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_MEN_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "CLOSE C" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "COMMIT TRANSACTION" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT OFF" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DEP_COD]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[DEP_COD]" & vbCrLf
    gRDOCon.QueryTimeout = 7200
    ExecuteSQL gRDOCon, sConsulta
    
sConsulta = "CREATE PROCEDURE [dbo].[DEP_COD] (@OLD VARCHAR(50),@NEW VARCHAR(50))  AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT ON" & vbCrLf
sConsulta = sConsulta & "BEGIN TRANSACTION" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR FOR SELECT COD FROM DEP WITH (NOLOCK) WHERE COD=@OLD" & vbCrLf
sConsulta = sConsulta & "OPEN C" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C INTO @VAR_COD" & vbCrLf
sConsulta = sConsulta & "IF @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE DEP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON0_DEP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON1_DEP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON2_DEP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3_DEP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PER NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CERTIFICADO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE NOCONFORMIDAD NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE APROBADORES NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PETICIONARIOS NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_ROL NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_ENLACE_CONDICIONES NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_COPIA_ROL NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_COPIA_ENLACE_CONDICION NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE DEP SET COD=@NEW WHERE COD=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON0_DEP SET DEP=@NEW WHERE DEP=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON1_DEP SET DEP=@NEW WHERE DEP=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON2_DEP SET DEP=@NEW WHERE DEP=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON3_DEP SET DEP=@NEW WHERE DEP=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PER SET DEP=@NEW WHERE DEP=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE COM SET DEP=@NEW WHERE DEP=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CERTIFICADO SET DEP=@NEW WHERE DEP=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE NOCONFORMIDAD SET DEP=@NEW WHERE DEP=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE APROBADORES SET DEP=@NEW WHERE DEP=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PETICIONARIOS SET DEP=@NEW WHERE DEP=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PM_ROL SET DEP=@NEW WHERE DEP=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PM_ENLACE_CONDICIONES SET VALOR_TEXT=@NEW WHERE TIPO_CAMPO=3 AND TIPO_VALOR=2 AND VALOR_TEXT=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PM_COPIA_ROL SET DEP=@NEW WHERE DEP=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PM_COPIA_ENLACE_CONDICION SET VALOR_TEXT=@NEW WHERE TIPO_CAMPO=3 AND TIPO_VALOR=2 AND VALOR_TEXT=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET DEP_APER=@NEW WHERE DEP_APER=@OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE DEP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON0_DEP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON1_DEP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON2_DEP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3_DEP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PER CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CERTIFICADO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE NOCONFORMIDAD CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE APROBADORES CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PETICIONARIOS CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_ROL CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_ENLACE_CONDICIONES CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_COPIA_ROL CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_COPIA_ENLACE_CONDICION CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close C" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT OFF" & vbCrLf
sConsulta = sConsulta & "COMMIT TRANSACTION" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROVE_COD]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[PROVE_COD]" & vbCrLf
    gRDOCon.QueryTimeout = 7200
    ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[PROVE_COD] (@OLD VARCHAR(50),@NEW VARCHAR(50))  AS " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "-- Quitamos la transacci�n porque se hace desde programa" & vbCrLf
sConsulta = sConsulta & "BEGIN " & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR LOCAL FOR SELECT COD FROM PROVE WITH (NOLOCK) WHERE COD=@OLD " & vbCrLf
sConsulta = sConsulta & "OPEN C " & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C INTO @VAR_COD " & vbCrLf
sConsulta = sConsulta & "IF @@FETCH_STATUS=0 " & vbCrLf
sConsulta = sConsulta & "BEGIN " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_PUNT_ERRORES NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE NOCONF_CONPROVE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONTRATO NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CON NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART1_ADJ NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART2_ADJ NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART3_ADJ NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ADJ NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ADJ NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ADJREU NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OFE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OFE_WEB NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE DETALLE_PUJAS NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFEMIN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_ADJUN NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_WEB_ADJUN NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_WEB NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_PET NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_NO_POT NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PEDIDO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_EQP NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN1 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN2 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN3 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN4 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVENOTIF NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ATRIB NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATALOG_LIN NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ORDEN_ENTREGA NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_PEDIDO NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART_UP NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ESP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ESP_PORTAL NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ITEM_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_GR_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ITEM_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_GR_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GRUPO_OFE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GRUPO_OFE_REU NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_CONTR NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FAVORITOS_ORDEN_ENTREGA NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FAVORITOS_LINEAS_PEDIDO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATN1_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATN2_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATN3_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATN4_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATN5_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_EMPRESA_COD_ERP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_GRUPOS NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE VERSION_INSTANCIA NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CERTIFICADO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_CERTIFICADO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE INSTANCIA_EST NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE NOCONFORMIDAD NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_ROL NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_COPIA_ROL NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_LEIDO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_FAVORITOS NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_UNQA NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE VAR_PROVE_UNQA_PUNT NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE VAR_PROVE_UNQA_X NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE VAR_PROVE_UNQA_PUNT_HIST NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE VAR_PROVE_UNQA_X_HIST NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE VAR_CAL_INT NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE VAR_CAL_MANUAL NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PPM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CARGO_PROVEEDORES NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE TASA_SERVICIO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_BANCA NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FACTURA NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE REGISTRO_EMAIL NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CESTA_CABECERA NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CESTA NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_GR_COSTESDESC NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FSGA_H_CAT_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FSGA_CAT_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE IMPUESTOS_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_PARTICIPANTES_FAV NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_REL NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "--CN" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_CON_NOTIF NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_MEN_CON NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_RESP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_USU NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE SET COD=@NEW WHERE COD=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE CON SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_PROVE SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART1_ADJ SET CODPROVE=@NEW WHERE CODPROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ART2_ADJ SET CODPROVE=@NEW WHERE CODPROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ART3_ADJ SET CODPROVE=@NEW WHERE CODPROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ART4_ADJ SET CODPROVE=@NEW WHERE CODPROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM SET PROVEACT=@NEW WHERE PROVEACT=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_ADJ SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_ADJREU SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_OFE SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_OFE_WEB SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE DETALLE_PUJAS SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFEMIN SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE_ADJUN SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE_WEB_ADJUN SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE_WEB SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE_PET SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE_NO_POT SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PEDIDO SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_EQP SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_GMN1 SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_GMN2 SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_GMN3 SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_GMN4 SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART1 SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART2 SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART3 SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART4 SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVENOTIF SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE USU SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART4_ATRIB SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CATALOG_LIN SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ORDEN_ENTREGA SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE LINEAS_PEDIDO SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART_UP SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ESP SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ESP_PORTAL SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET PROVEACT=@NEW WHERE PROVEACT=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GRUPO SET PROVEACT=@NEW WHERE PROVEACT=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ALL_PROVE SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_GRUPO_PROVE SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ITEM_PROVE SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_PROCE_PROVE SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_ATRIB SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_ITEM_ATRIB SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_GR_ATRIB SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_ITEM_ADJUN SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_GR_ADJUN SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GRUPO_OFE SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GRUPO_OFE_REU SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_PROVE SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_PROVE SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_CONTR SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "--2.12" & vbCrLf
sConsulta = sConsulta & "UPDATE CATN1_PROVE  SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CATN2_PROVE  SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CATN3_PROVE  SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CATN4_PROVE  SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CATN5_PROVE  SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART4_ADJUN  SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE FAVORITOS_ORDEN_ENTREGA  SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE FAVORITOS_LINEAS_PEDIDO  SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE_GRUPOS SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_EMPRESA_COD_ERP SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE FORM_CAMPO SET VALOR_TEXT=@NEW WHERE VALOR_TEXT=@OLD AND TIPO_CAMPO_GS=100" & vbCrLf
sConsulta = sConsulta & "UPDATE LINEA_DESGLOSE SET VALOR_TEXT=@NEW WHERE VALOR_TEXT=@OLD AND CAMPO_HIJO IN (SELECT ID FROM FORM_CAMPO WITH (NOLOCK) WHERE ES_SUBCAMPO=1 AND TIPO_CAMPO_GS=100)" & vbCrLf
sConsulta = sConsulta & "UPDATE COPIA_CAMPO SET VALOR_TEXT=@NEW WHERE VALOR_TEXT=@OLD AND COPIA_CAMPO_DEF IN (SELECT ID FROM COPIA_CAMPO_DEF WITH (NOLOCK) WHERE TIPO_CAMPO_GS=100)" & vbCrLf
sConsulta = sConsulta & "UPDATE COPIA_LINEA_DESGLOSE SET VALOR_TEXT=@NEW WHERE VALOR_TEXT=@OLD AND CAMPO_HIJO IN (SELECT COPIA_CAMPO.ID FROM COPIA_CAMPO WITH (NOLOCK) INNER JOIN COPIA_CAMPO_DEF WITH (NOLOCK) ON COPIA_CAMPO.COPIA_CAMPO_DEF=COPIA_CAMPO_DEF.ID WHERE ES_SUBCAMPO=1 AND TIPO_CAMPO_GS=100)" & vbCrLf
sConsulta = sConsulta & "UPDATE VERSION_INSTANCIA  SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CERTIFICADO  SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_CERTIFICADO  SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE INSTANCIA_EST  SET DESTINATARIO_PROV=@NEW WHERE DESTINATARIO_PROV=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE NOCONFORMIDAD  SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PM_ROL  SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PM_COPIA_ROL  SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE INSTANCIA_EST  SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_LEIDO  SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_FAVORITOS  SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_UNQA  SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CESTA_CABECERA SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CESTA SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE VAR_PROVE_UNQA_PUNT  SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE VAR_PROVE_UNQA_X  SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE VAR_PROVE_UNQA_PUNT_HIST  SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE VAR_PROVE_UNQA_X_HIST  SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE VAR_CAL_INT  SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE VAR_CAL_MANUAL  SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PPM  SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CARGO_PROVEEDORES  SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE TASA_SERVICIO  SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_BANCA  SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE FACTURA SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE REGISTRO_EMAIL SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE NOCONF_CONPROVE SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_GR_COSTESDESC SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONTRATO SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_PUNT_ERRORES SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE FSGA_H_CAT_PROVE SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE FSGA_CAT_PROVE SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE IMPUESTOS_PROVE SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PM_PARTICIPANTES_FAV SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_REL SET PROVEP=@NEW WHERE PROVEP=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_REL SET PROVES=@NEW WHERE PROVES=@OLD" & vbCrLf
sConsulta = sConsulta & "--CN" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_CON_NOTIF SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_MEN_CON SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_RESP SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_RESP SET PROVECITADO=@NEW WHERE PROVECITADO=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_USU SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CON CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART1_ADJ CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART2_ADJ CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART3_ADJ CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ADJ CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ADJ CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ADJREU CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OFE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OFE_WEB CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE DETALLE_PUJAS CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFEMIN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_ADJUN CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_WEB_ADJUN CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_WEB CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_PET CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_NO_POT CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PEDIDO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_EQP CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN1 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN2 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN3 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN4 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVENOTIF CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ATRIB CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATALOG_LIN CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ORDEN_ENTREGA CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_PEDIDO CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART_UP CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ESP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ESP_PORTAL CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ITEM_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_GR_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ITEM_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_GR_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GRUPO_OFE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GRUPO_OFE_REU CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_CONTR CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FAVORITOS_ORDEN_ENTREGA CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FAVORITOS_LINEAS_PEDIDO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATN1_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATN2_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATN3_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATN4_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATN5_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_EMPRESA_COD_ERP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_GRUPOS CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE VERSION_INSTANCIA CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CERTIFICADO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_CERTIFICADO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE INSTANCIA_EST CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE NOCONFORMIDAD CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_ROL CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_COPIA_ROL CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_LEIDO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_FAVORITOS CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_UNQA CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE VAR_PROVE_UNQA_PUNT CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE VAR_PROVE_UNQA_X CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE VAR_PROVE_UNQA_PUNT_HIST CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE VAR_PROVE_UNQA_X_HIST CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE VAR_CAL_INT CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE VAR_CAL_MANUAL CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PPM CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CARGO_PROVEEDORES CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE TASA_SERVICIO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_BANCA CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FACTURA CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE REGISTRO_EMAIL CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE NOCONF_CONPROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CESTA CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CESTA_CABECERA CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_GR_COSTESDESC CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONTRATO CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_PUNT_ERRORES CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FSGA_H_CAT_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FSGA_CAT_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE IMPUESTOS_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_PARTICIPANTES_FAV CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_REL CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "--CN" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_CON_NOTIF CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_MEN_CON CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_RESP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_USU CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "End " & vbCrLf
sConsulta = sConsulta & "Close C " & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C " & vbCrLf
sConsulta = sConsulta & "End " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


    
End Sub


Public Function CodigoDeActualizacion31900_08_2002_A31900_08_2003() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim oADOConnection As ADODB.Connection
    Dim sConsulta As String

    On Error GoTo Error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
                  
    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Actualiza Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    frmProgreso.ProgressBar1.Refresh
    V_31900_8_Storeds_203
    
    sConsulta = "UPDATE VERSION SET NUM ='3.00.20.003'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103), PRODVER='31900.8'"
    ExecuteSQL gRDOCon, sConsulta
        
    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False
        
    CodigoDeActualizacion31900_08_2002_A31900_08_2003 = True
    Exit Function

Error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    CodigoDeActualizacion31900_08_2002_A31900_08_2003 = False
End Function

Public Sub V_31900_8_Storeds_203()
    Dim sConsulta As String
            
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSGSW_DEVOLVER_PROVEEDORADJUDICADO]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSGSW_DEVOLVER_PROVEEDORADJUDICADO]" & vbCrLf
    gRDOCon.QueryTimeout = 7200
    ExecuteSQL gRDOCon, sConsulta
sConsulta = "CREATE PROCEDURE [dbo].[FSGSW_DEVOLVER_PROVEEDORADJUDICADO] @ANYO INT, @GMN1 VARCHAR(50), @PROCE VARCHAR(50), @CODPROVE NVARCHAR(30), @IDEMP INT ,@IDI NVARCHAR(5) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT PROVE.COD PROVECOD,PROVE.DEN PROVEDEN,LTRIM(RTRIM(PE.COD_ERP)) COD_ERP,LTRIM(RTRIM(PE.COD_ERP)) + ' - ' + PE.DEN_ERP DEN_ERP ,PE.EMPRESA ,PROVE.NIF,PRI_PV.ESTADO_ADJ EST_INTEGRACION" & vbCrLf
sConsulta = sConsulta & ",PE.CAMPO1,PE.CAMPO2,PE.CAMPO3,PE.CAMPO4,PG.ACTCAMPO1,PG.ACTCAMPO2,PG.ACTCAMPO3,PG.ACTCAMPO4 " & vbCrLf
sConsulta = sConsulta & "FROM PROVE WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PROVE_ERP PE WITH (NOLOCK) ON PE.EMPRESA=@IDEMP AND PE.NIF=PROVE.NIF AND PE.BAJALOG=0 " & vbCrLf
sConsulta = sConsulta & "INNER JOIN PARGEN_GEST PG WITH (NOLOCK)  ON 1=1" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PROCE_INT PRI_PV  WITH(NOLOCK) ON PRI_PV.ANYO =@ANYO AND PRI_PV.GMN1=@GMN1 AND PRI_PV.PROCE=@PROCE AND PRI_PV.PROVE=@CODPROVE AND PRI_PV.GRUPO IS NULL AND PRI_PV.EMPRESA=@IDEMP " & vbCrLf
sConsulta = sConsulta & "WHERE PROVE.COD=@CODPROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT ID,DEN FROM PARGEN_LIT WITH (NOLOCK) WHERE ID BETWEEN 32 AND 35 AND IDI=@IDI" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSGSW_DEVOLVER_ITEMSGRUPO]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSGSW_DEVOLVER_ITEMSGRUPO]" & vbCrLf
    gRDOCon.QueryTimeout = 7200
    ExecuteSQL gRDOCon, sConsulta
sConsulta = "CREATE PROCEDURE [dbo].[FSGSW_DEVOLVER_ITEMSGRUPO] @ANYO INT, @GMN1 VARCHAR(50), @PROCE INT, @CODGRUPO VARCHAR(20),@PROVE VARCHAR(50),@IDEMP INT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @EST INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PARAM AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @ERP INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ESCALADOS INT=0" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDGRUPO INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @USAR_ART4_UON INT=0" & vbCrLf
sConsulta = sConsulta & "DECLARE @TRASPASO_PLANIF INT=0" & vbCrLf
sConsulta = sConsulta & "DECLARE @USAR_ORGCOMPRAS INT =0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @ESCALADOS=COUNT(ID) FROM PROCE_GRUPO PG WITH (NOLOCK) WHERE PG.ANYO=@ANYO  AND PG.GMN1=@GMN1 AND PG.PROCE=@PROCE AND PG.COD=@CODGRUPO AND PG.ESCALADOS=1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @EST=EST FROM PROCE P WITH (NOLOCK) WHERE ANYO =@ANYO AND COD=@PROCE AND GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @ERP=ES.ERP FROM ERP_SOCIEDAD ES WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN EMP E WITH (NOLOCK) ON E.SOCIEDAD=ES.SOCIEDAD AND E.ID=@IDEMP" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "SELECT @IDGRUPO=ID FROM PROCE_GRUPO PG WITH (NOLOCK) WHERE PG.ANYO=@ANYO AND PG.GMN1=@GMN1 AND PG.PROCE=@PROCE AND PG.COD=@CODGRUPO " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @USAR_ART4_UON=USAR_ART4_UON, @TRASPASO_PLANIF=TRASPASO_ART_PLANIF, @USAR_ORGCOMPRAS=USAR_ORGCOMPRAS  FROM PARGEN_INTERNO WITH(NOLOCK) WHERE ID=1" & vbCrLf
sConsulta = sConsulta & "    " & vbCrLf
sConsulta = sConsulta & "   SET @SQL='SELECT DISTINCT COALESCE(LIA1.ID,LIA.ID) LIA_ID,I.ID ID_ITEM,I.ART CODART,I.DESCR DENART,I.UNI,'" & vbCrLf
sConsulta = sConsulta & "   if  @ESCALADOS>0 " & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + ' PGE.ID ESC_ID,PGE.INICIO,PGE.FIN,'" & vbCrLf
sConsulta = sConsulta & "   else" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + 'IA.CANT_ADJ*IT_EMP.PORCEN CANT_ADJ,'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + ' IOF.PREC_VALIDO PREC_ADJ,P.MON,'" & vbCrLf
sConsulta = sConsulta & "   IF @USAR_ORGCOMPRAS =1  --Si el �ltimo traspaso es un borrado, lee lo que haya en el proceso. Si no (era una I), lee lo de log_item_adj" & vbCrLf
sConsulta = sConsulta & "      SET @SQL=@SQL + ' CASE WHEN LIA3.ACCION=''D'' THEN IT_CEN.CENTRO ELSE COALESCE(it_cen.CENTRO,LIA.CENTRO,'''') end CENTROS,'''' AS CENTRO,'" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "      SET @SQL=@SQL + ''''' CENTROS,'''' AS CENTRO,'" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + ' CASE WHEN LIA3.ACCION = ''D'' AND LG.ESTADO=4  THEN NULL ELSE LG.ESTADO END AS ESTADOINT,CASE WHEN LIA3.ACCION = ''D'' AND LG.ESTADO=4 THEN NULL ELSE LG.FECENV END FECENV,CASE WHEN LIA3.ACCION = ''D'' AND LG.ESTADO=4 THEN NULL ELSE LIA3.USU  END USU " & vbCrLf
sConsulta = sConsulta & "   , I.FECINI FECINI_SUM,I.FECFIN FECFIN_SUM,LIA.PROVE_ERP,PE.DEN_ERP," & vbCrLf
sConsulta = sConsulta & "   COALESCE(IT_ATRIB1.INT_ATRIB_ID,IAE.ATRIB,PGA.ATRIB,PATR.ATRIB,PA.ATRIB,DA.ID) ATRIBID," & vbCrLf
sConsulta = sConsulta & "   DA.COD ATRIBCOD,DA.DEN ATRIBDEN," & vbCrLf
sConsulta = sConsulta & "   COALESCE(IT_ATRIB1.VALOR_TEXT,IAE.VALOR_TEXT,PGA.VALOR_TEXT,PATR.VALOR_TEXT,OA.VALOR_TEXT,OGATR.VALOR_TEXT,OATR.VALOR_TEXT,IAT.VALOR_TEXT) ATRIBVALOR_TEXT ," & vbCrLf
sConsulta = sConsulta & "   COALESCE(IT_ATRIB1.VALOR_NUM ,IAE.VALOR_NUM,PGA.VALOR_NUM,PATR.VALOR_NUM,OA.VALOR_NUM,OGATR.VALOR_NUM,OATR.VALOR_NUM,IAT.VALOR_NUM) ATRIBVALOR_NUM," & vbCrLf
sConsulta = sConsulta & "   COALESCE(IT_ATRIB1.VALOR_FEC ,IAE.VALOR_FEC,PGA.VALOR_FEC,PATR.VALOR_FEC,OA.VALOR_FEC,OGATR.VALOR_FEC,OATR.VALOR_FEC,IAT.VALOR_FEC) ATRIBVALOR_FEC," & vbCrLf
sConsulta = sConsulta & "   COALESCE(IT_ATRIB1.VALOR_BOOL,IAE.VALOR_BOOL,PGA.VALOR_BOOL,PATR.VALOR_BOOL,OA.VALOR_BOOL,OGATR.VALOR_BOOL,OATR.VALOR_BOOL,IAT.VALOR_BOOL) ATRIBVALOR_BOOL," & vbCrLf
sConsulta = sConsulta & "   DA.TIPO ATRIBTIPO,IAT.OBLIGATORIO ATRIBOBLIGATORIO,'" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   IF @USAR_ART4_UON  =1 " & vbCrLf
sConsulta = sConsulta & "      SET @SQL=@SQL + 'CASE WHEN IT_EMP.UON1 IS not NULL THEN IT_EMP.UON1 ELSE ART4_UON.UON1 END UON1," & vbCrLf
sConsulta = sConsulta & "      CASE WHEN IT_EMP.UON2 IS not NULL THEN IT_EMP.UON2 ELSE ART4_UON.UON2 END UON2," & vbCrLf
sConsulta = sConsulta & "      CASE WHEN IT_EMP.UON3 IS not NULL THEN IT_EMP.UON3 ELSE ART4_UON.UON3 END UON3 '" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "      SET @SQL=@SQL + 'IT_EMP.UON1 UON1,IT_EMP.UON2 UON2,IT_EMP.UON3 UON3 '" & vbCrLf
sConsulta = sConsulta & "      " & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + ',CASE WHEN DISTR.DISTRIBUIDOR IS NULL THEN 0 ELSE 1 END DISTRIBUIDOR,'" & vbCrLf
sConsulta = sConsulta & "   IF @ESCALADOS>0 " & vbCrLf
sConsulta = sConsulta & "      SET @SQL=@SQL + '1 ESCALADOS,PG.ESCALAUNI '" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "      SET @SQL=@SQL + '0 ESCALADOS '" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   --------FROM--------------------------------------------" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + 'FROM PROCE P WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PROCE_GRUPO PG WITH (NOLOCK) ON PG.ANYO=P.ANYO AND PG.GMN1=P.GMN1 AND PG.PROCE=P.COD AND PG.COD=@CODGRUPO " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN ITEM I WITH (NOLOCK) ON I.ANYO=P.ANYO AND I.GMN1_PROCE=P.GMN1 AND I.PROCE=P.COD AND I.GRUPO=PG.ID '" & vbCrLf
sConsulta = sConsulta & "   IF @EST=11 --Si el proceso esta parcialmente cerrado" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL + ' AND I.EST=1 '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @ESCALADOS>0 --Recoge los datos de adjudicaciones de las tablas de escalados" & vbCrLf
sConsulta = sConsulta & "      SET @SQL =@SQL + 'INNER JOIN ITEM_ADJESC IA WITH (NOLOCK) ON IA.ANYO=I.ANYO AND IA.GMN1=I.GMN1_PROCE AND IA.PROCE=I.PROCE AND IA.ITEM=I.ID AND IA.PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PROCE_GR_ESC PGE WITH (NOLOCK) ON PGE.ANYO=I.ANYO AND PGE.GMN1=I.GMN1_PROCE AND PGE.PROCE=I.PROCE AND PGE.GRUPO =PG.ID AND PGE.ID=IA.ESC '" & vbCrLf
sConsulta = sConsulta & "   ELSE -- Recoge la adjudicaci�n" & vbCrLf
sConsulta = sConsulta & "      SET @SQL =@SQL + 'INNER JOIN ITEM_ADJ IA WITH (NOLOCK) ON I.ANYO=IA.ANYO AND I.GMN1_PROCE=IA.GMN1 AND I.PROCE=IA.PROCE AND I.ID=IA.ITEM AND IA.PROVE=@PROVE '" & vbCrLf
sConsulta = sConsulta & "    " & vbCrLf
sConsulta = sConsulta & "    ---Obtiene las empresas a las que se ha distribuido la compra" & vbCrLf
sConsulta = sConsulta & "   SET @SQL =@SQL + 'INNER JOIN " & vbCrLf
sConsulta = sConsulta & "   (select distinct iu.item,iu.uon1,null uon2,null uon3,u.empresa,iu.porcen " & vbCrLf
sConsulta = sConsulta & "   from item_uon1 iu WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   inner join uon1 u WITH(NOLOCK) on iu.uon1 = u.cod where IU.ANYO=@ANYO AND IU.GMN1=@GMN1 AND IU.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   Union " & vbCrLf
sConsulta = sConsulta & "   select distinct iu.item,iu.uon1,IU.uon2 ,null uon3,ISNULL(u1.empresa,u2.empresa) as empresa,iu.porcen " & vbCrLf
sConsulta = sConsulta & "   from item_uon2 iu WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   inner join uon2 u2 WITH(NOLOCK) on iu.uon1=u2.uon1 and iu.uon2=u2.cod " & vbCrLf
sConsulta = sConsulta & "   inner join uon1 u1 WITH(NOLOCK) on u1.cod=u2.uon1 where IU.ANYO=@ANYO AND IU.GMN1=@GMN1 AND IU.PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "   Union select distinct iu.item,iu.uon1, IU.uon2 ,IU.uon3,ISNULL(u3.empresa,ISNULL(u2.empresa,u1.empresa)) as empresa,iu.porcen " & vbCrLf
sConsulta = sConsulta & "   from item_uon3 iu WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   inner join uon3 u3 WITH(NOLOCK) on iu.uon1=u3.uon1 and iu.uon2=u3.uon2 and iu.uon3=u3.cod " & vbCrLf
sConsulta = sConsulta & "   inner join uon2 u2 WITH(NOLOCK) on u3.uon1=u2.uon1 and u3.uon2=u2.cod " & vbCrLf
sConsulta = sConsulta & "   inner join uon1 u1 WITH(NOLOCK) on u1.cod=u3.uon1 where IU.ANYO=@ANYO AND IU.GMN1=@GMN1 AND IU.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   ) IT_EMP ON IT_EMP.ITEM=IA.ITEM AND IT_EMP.EMPRESA=@IDEMP '" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   --Si los art�culos est�n vinculados a las unidades organizativas, solamente aparecen las empresas a las que pertenece el item (unidades organizativas comunes entre el art�culo y la distribuci�n del item)" & vbCrLf
sConsulta = sConsulta & "   IF @USAR_ART4_UON =1" & vbCrLf
sConsulta = sConsulta & "   begin" & vbCrLf
sConsulta = sConsulta & "      SET @SQL=@SQL + 'INNER JOIN ART4_UON WITH(NOLOCK) ON I.ART=ART4_UON.ART4 AND IT_EMP.UON1=ART4_UON.UON1 AND ISNULL(IT_EMP.UON2,ISNULL(ART4_UON.UON2,0))=ISNULL(ART4_UON.UON2,0) " & vbCrLf
sConsulta = sConsulta & "         AND ISNULL(IT_EMP.UON3,ISNULL(ART4_UON.UON3,0))=ISNULL(ART4_UON.UON3,0) AND ART4_UON.BAJALOG=0 '" & vbCrLf
sConsulta = sConsulta & "      IF  @TRASPASO_PLANIF=1" & vbCrLf
sConsulta = sConsulta & "           SET @SQL=@SQL + 'AND ART4_UON.CAMPO1 = ''1'' '" & vbCrLf
sConsulta = sConsulta & "      " & vbCrLf
sConsulta = sConsulta & "      IF @USAR_ORGCOMPRAS=1" & vbCrLf
sConsulta = sConsulta & "      begin " & vbCrLf
sConsulta = sConsulta & "          SET @SQL =@SQL + 'LEFT JOIN(" & vbCrLf
sConsulta = sConsulta & "          select distinct au.art4,au.uon1,aU.uon2,aU.uon3,ISNULL(u3.empresa,ISNULL(u2.empresa,u1.empresa)) as empresa,ISNULL(u3.CENTROS,ISNULL(u2.CENTROS,u1.CENTROS)) CENTRO" & vbCrLf
sConsulta = sConsulta & "          from art4_uon au WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "          inner join uon3 u3 WITH(NOLOCK) on au.uon1=u3.uon1 and au.uon2=u3.uon2 and au.uon3=u3.cod " & vbCrLf
sConsulta = sConsulta & "          inner join uon2 u2 WITH(NOLOCK) on u3.uon1=u2.uon1 and u3.uon2=u2.cod " & vbCrLf
sConsulta = sConsulta & "          inner join uon1 u1 WITH(NOLOCK) on u1.cod=u3.uon1" & vbCrLf
sConsulta = sConsulta & "          where au.bajalog=0 '" & vbCrLf
sConsulta = sConsulta & "          IF  @TRASPASO_PLANIF=1" & vbCrLf
sConsulta = sConsulta & "              SET @SQL =@SQL + ' and au.campo1=''1'''" & vbCrLf
sConsulta = sConsulta & "          SET @SQL =@SQL + ') IT_CEN ON I.ART=IT_CEN.ART4 AND IT_CEN.EMPRESA=@IDEMP AND IT_CEN.UON1=IT_EMP.UON1 AND IT_CEN.UON2=ISNULL(IT_EMP.uon2,IT_CEN.UON2) AND ISNULL(IT_EMP.uon3,IT_CEN.UON3) =IT_CEN.UON3 '" & vbCrLf
sConsulta = sConsulta & "      end" & vbCrLf
sConsulta = sConsulta & "   end" & vbCrLf
sConsulta = sConsulta & "  " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   IF @ESCALADOS>0 --Recoge las ofertas de la tabla de escalados" & vbCrLf
sConsulta = sConsulta & "      SET @SQL =@SQL + 'INNER JOIN ITEM_OFEESC IOF WITH (NOLOCK) ON IA.ANYO =IOF.ANYO AND IA.GMN1 =IOF.GMN1 AND IA.PROCE =IOF.PROCE AND IA.ITEM =IOF.ITEM AND IA.PROVE=IOF.PROVE AND IA.OFE =IOF.OFE AND IOF.ESC=PGE.ID '" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "      SET @SQL=@SQL + 'INNER JOIN ITEM_OFE IOF WITH (NOLOCK) ON IA.ANYO =IOF.ANYO AND IA.GMN1 =IOF.GMN1 AND IA.PROCE =IOF.PROCE AND IA.ITEM =IOF.ITEM AND IA.PROVE=IOF.PROVE AND IA.OFE =IOF.NUM '" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   ---Datos de integraci�n:" & vbCrLf
sConsulta = sConsulta & "   SET @SQL= @SQL + ' LEFT JOIN (SELECT ID,LIA.PROVEPADRE,LIA.PROVE,LIA.ANYO,LIA.GMN1_PROCE,LIA.PROCE," & vbCrLf
sConsulta = sConsulta & "       LIA.ID_ITEM FROM LOG_ITEM_ADJ LIA WITH (NOLOCK) WHERE (LIA.ORIGEN=0 OR LIA.ORIGEN=2) AND LIA.ULTIMO=1" & vbCrLf
sConsulta = sConsulta & "       GROUP BY ID,LIA.PROVE,LIA.PROVEPADRE,LIA.ANYO,LIA.GMN1_PROCE,LIA.PROCE," & vbCrLf
sConsulta = sConsulta & "       LIA.ID_ITEM) LIA2" & vbCrLf
sConsulta = sConsulta & "       ON I.ANYO=LIA2.ANYO AND I.GMN1_PROCE=LIA2.GMN1_PROCE AND I.PROCE =LIA2.PROCE AND LIA2.PROVEPADRE=@PROVE AND LIA2.PROVE=@PROVE  AND I.ID=LIA2.ID_ITEM" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN LOG_ITEM_ADJ LIA WITH (NOLOCK) ON LIA.ID=LIA2.ID" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN (SELECT ID,LIA.PROVEPADRE,LIA.ANYO,LIA.GMN1_PROCE,LIA.PROCE," & vbCrLf
sConsulta = sConsulta & "       LIA.ID_ITEM,LIA.CENTRO  " & vbCrLf
sConsulta = sConsulta & "       FROM LOG_ITEM_ADJ LIA WITH (NOLOCK) WHERE (LIA.ORIGEN=0 OR LIA.ORIGEN=2) AND LIA.ULTIMO=1 " & vbCrLf
sConsulta = sConsulta & "       GROUP BY ID,LIA.PROVEPADRE,LIA.ANYO,LIA.GMN1_PROCE,LIA.PROCE," & vbCrLf
sConsulta = sConsulta & "       LIA.ID_ITEM,LIA.CENTRO) LIA1 " & vbCrLf
sConsulta = sConsulta & "   ON I.ANYO=LIA1.ANYO AND I.GMN1_PROCE=LIA1.GMN1_PROCE AND I.PROCE =LIA1.PROCE AND LIA1.PROVEPADRE=@PROVE  AND I.ID=LIA1.ID_ITEM '" & vbCrLf
sConsulta = sConsulta & "   IF @USAR_ORGCOMPRAS=1" & vbCrLf
sConsulta = sConsulta & "       SET @SQL= @SQL + ' AND LIA1.CENTRO=IT_CEN.CENTRO '  " & vbCrLf
sConsulta = sConsulta & "   SET @SQL= @SQL + ' LEFT JOIN LOG_ITEM_ADJ LIA3 WITH(NOLOCK) ON LIA3.ID=LIA1.ID " & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN LOG_GRAL LG WITH (NOLOCK) ON LG.ID_TABLA=LIA1.ID AND LG.TABLA=9" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN PROVE_ERP PE WITH (NOLOCK) ON LIA.PROVE_ERP=PE.COD_ERP AND PE.EMPRESA=@IDEMP " & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN (" & vbCrLf
sConsulta = sConsulta & "   SELECT DISTINCT PROVEP DISTRIBUIDOR FROM PROVE_REL PR WITH (NOLOCK) ' " & vbCrLf
sConsulta = sConsulta & "   SET @SQL =@SQL + ' INNER JOIN GS_PROVETREL GP WITH (NOLOCK) ON PR.PROVETREL=GP.ID AND PR.PROVEP=@PROVE AND GP.PEDIDO=1)" & vbCrLf
sConsulta = sConsulta & "    DISTR ON DISTR.DISTRIBUIDOR=@PROVE" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN INT_ATRIB IAT WITH(NOLOCK) ON IAT.ADJUDICACION=1 AND IAT.AMBITO=3 AND IAT.ERP=@ERP" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN DEF_ATRIB DA WITH (NOLOCK) ON DA.ID=IAT.ATRIB  AND IAT.ADJUDICACION=1 AND IAT.AMBITO=3 AND" & vbCrLf
sConsulta = sConsulta & "IAT.ERP=@ERP" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN ITEM_ATRIBESP IAE WITH (NOLOCK) ON IAE.ANYO=@ANYO AND IAE.GMN1=@GMN1 AND IAE.PROCE=@PROCE AND IAE.ATRIB=IAT.ATRIB AND IAE.ITEM=I.ID" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN PROCE_GRUPO_ATRIBESP PGA WITH (NOLOCK) ON PGA.ANYO=@ANYO AND PGA.GMN1=@GMN1 AND PGA.PROCE=@PROCE AND PGA.ATRIB=IAT.ATRIB AND PGA.GRUPO=@IDGRUPO" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN PROCE_ATRIBESP PATR WITH(NOLOCK) ON PATR.ANYO=@ANYO AND PATR.GMN1=@GMN1 AND PATR.PROCE=@PROCE AND PATR.ATRIB=IAT.ATRIB" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & " LEFT JOIN (SELECT TOP 1 OFE,ANYO,GMN1,PROCE,PROVE FROM ITEM_ADJ WITH (NOLOCK) WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE) IAD ON IAD.ANYO=@ANYO  AND IAD.GMN1=@GMN1 AND IAD.PROCE=@PROCE AND IAD.PROVE=@PROVE   " & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN PROCE_ATRIB PA WITH (NOLOCK) ON PA.ANYO=@ANYO AND PA.GMN1=@GMN1 AND PA.PROCE=@PROCE AND PA.AMBITO=3 AND DA.ID=PA.ATRIB AND (PA.GRUPO=@IDGRUPO OR PA.GRUPO IS NULL)   " & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN OFE_ITEM_ATRIB OA WITH (NOLOCK) ON OA.ANYO=@ANYO AND OA.GMN1=@GMN1 AND OA.PROCE=@PROCE AND OA.ATRIB_ID =PA.ID AND OA.PROVE=@PROVE AND OA.OFE=IAD.OFE AND OA.ITEM=I.ID" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN OFE_GR_ATRIB OGATR WITH(NOLOCK) ON OGATR.ANYO=@ANYO AND OGATR.GMN1=@GMN1 AND OGATR.PROCE=@PROCE AND OGATR.ATRIB_ID =PA.ID AND OGATR.PROVE=@PROVE AND OGATR.OFE=IAD.OFE AND OGATR.GRUPO=I.GRUPO" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN OFE_ATRIB OATR WITH(NOLOCK) ON OATR.ANYO=@ANYO AND OATR.GMN1=@GMN1 AND OATR.PROCE=@PROCE AND OATR.ATRIB_ID =PA.ID AND OATR.PROVE=@PROVE AND OATR.OFE=IAD.OFE " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    LEFT JOIN " & vbCrLf
sConsulta = sConsulta & "   (SELECT DA.ID INT_ATRIB_ID,LIAA.ID_LOG_ITEM_ADJ , DA.ID, DA.COD,DA.DEN," & vbCrLf
sConsulta = sConsulta & "   DA.TIPO,IA.INTRO,IA.OBLIGATORIO,LIAA.VALOR_TEXT ,LIAA.VALOR_NUM,LIAA.VALOR_FEC,LIAA.VALOR_BOOL" & vbCrLf
sConsulta = sConsulta & "   FROM INT_ATRIB IA WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN DEF_ATRIB DA WITH (NOLOCK) ON DA.ID=IA.ATRIB AND IA.ADJUDICACION=1 AND IA.AMBITO=3 AND IA.ERP=@ERP" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN LOG_ITEM_ADJ_ATRIB LIAA WITH (NOLOCK) ON LIAA.ATRIB=IA.ATRIB" & vbCrLf
sConsulta = sConsulta & "    ) IT_ATRIB1 ON IT_ATRIB1.INT_ATRIB_ID=IAT.ID AND LIA.ID=IT_ATRIB1.ID_LOG_ITEM_ADJ" & vbCrLf
sConsulta = sConsulta & "    WHERE P.ANYO=@ANYO AND P.GMN1=@GMN1 AND P.COD=@PROCE  " & vbCrLf
sConsulta = sConsulta & "   ORDER BY I.ID ASC' " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @PARAM =N'@ANYO INT, @GMN1 VARCHAR(50), @PROCE VARCHAR(50), @CODGRUPO VARCHAR(20),@PROVE VARCHAR(50),@IDEMP INT,@ERP INT,@IDGRUPO INT'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL ,@PARAM,@ANYO=@ANYO,@GMN1=@GMN1,@PROCE=@PROCE,@CODGRUPO=@CODGRUPO,@PROVE=@PROVE,@IDEMP=@IDEMP,@ERP=@ERP ,@IDGRUPO=@IDGRUPO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UON3_COD]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[UON3_COD]" & vbCrLf
    gRDOCon.QueryTimeout = 7200
    ExecuteSQL gRDOCon, sConsulta
    
    
sConsulta = "CREATE  PROCEDURE [DBO].[UON3_COD] (@OLD VARCHAR(50),@NEW VARCHAR(50), @UON1 VARCHAR(50), @UON2 VARCHAR(50))  AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT ON" & vbCrLf
sConsulta = sConsulta & "BEGIN TRANSACTION" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR FOR SELECT COD FROM UON3 WITH(NOLOCK) WHERE UON1=@UON1 AND UON2=@UON2 AND COD=@OLD" & vbCrLf
sConsulta = sConsulta & "OPEN C" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C INTO @VAR_COD" & vbCrLf
sConsulta = sConsulta & "IF @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3_DEP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PER NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3_DEST NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISOR NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CERTIFICADO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE NOCONFORMIDAD NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE APROBADORES NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PETICIONARIOS NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_ROL NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_ENLACE_CONDICIONES NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_COPIA_ROL NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_COPIA_ENLACE_CONDICION NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_UON NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU_CC_CONTROL NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU_CC_IMPUTACION NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES5_UON NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CENTRO_SM_UON NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON4_DEST NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_PED_IMPUTACION NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PERF_UON NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_CAT_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_CAT_UON3_DEP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_GRUPO_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_GRUPO_UON3_DEP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_MEN_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_MEN_UON3_DEP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CUSTOM_TEMPLATE_UON NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE UON3 SET COD=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND COD=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON4 SET UON3 = @NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON3_DEP SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PER SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE COM SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_UON3 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_UON3 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON3_DEST SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_UON3 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_UON3 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES1_NIV1 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES1_NIV2 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES1_NIV3 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES1_NIV4 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES2_NIV1 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES2_NIV2 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES2_NIV3 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES2_NIV4 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES3_NIV1 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES3_NIV2 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES3_NIV3 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES3_NIV4 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES4_NIV1 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES4_NIV2 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES4_NIV3 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES4_NIV4 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_1 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_2 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_3 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_4 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_1 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_2 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_3 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_4 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON1 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON2 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON3 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON4 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY1 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY2 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY3 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY4 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_PRESCON3 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_PRESCON4 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISOR SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CERTIFICADO SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE NOCONFORMIDAD SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE APROBADORES SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PETICIONARIOS SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PM_ROL SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PM_COPIA_ROL SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4_UON SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET UON3_APER=@NEW WHERE UON1_APER=@UON1 AND UON2_APER=@UON2 AND UON3_APER=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE USU_CC_CONTROL SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE USU_CC_IMPUTACION SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES5_UON SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CENTRO_SM_UON SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON4_DEST SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE LINEAS_PED_IMPUTACION SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PERF_UON SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_CAT_UON3 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_CAT_UON3_DEP SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_GRUPO_UON3 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_GRUPO_UON3_DEP SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_MEN_UON3 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_MEN_UON3_DEP SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CUSTOM_TEMPLATE_UON SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @LEN_COD_UON1 AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @LEN_COD_UON2 AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @LEN_COD_UON3 AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE C2 CURSOR FOR SELECT (SELECT LONGITUD FROM DIC WITH(NOLOCK) WHERE NOMBRE='UON1') AS L1,(SELECT LONGITUD FROM DIC WITH(NOLOCK) WHERE NOMBRE='UON2') AS L2,(SELECT LONGITUD FROM DIC WITH(NOLOCK) WHERE NOMBRE='UON3') AS L3" & vbCrLf
sConsulta = sConsulta & "OPEN C2" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C2 INTO @LEN_COD_UON1,@LEN_COD_UON2,@LEN_COD_UON3" & vbCrLf
sConsulta = sConsulta & "IF @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "  UPDATE PM_ENLACE_CONDICIONES SET VALOR_TEXT=STUFF(VALOR_TEXT,@LEN_COD_UON1 + @LEN_COD_UON2 + 3,@LEN_COD_UON3,@NEW + SPACE(@LEN_COD_UON3 - LEN(@NEW))) WHERE TIPO_CAMPO=4 AND TIPO_VALOR=2 AND LTRIM(RTRIM(SUBSTRING(VALOR_TEXT,1,@LEN_COD_UON1)))=@UON1 AND LTRIM(RTRIM(SUBSTRING(VALOR_TEXT,@LEN_COD_UON1+2, @LEN_COD_UON2)))=@UON2 AND LTRIM(RTRIM(SUBSTRING(VALOR_TEXT,@LEN_COD_UON1+@LEN_COD_UON2+3, @LEN_COD_UON3)))=@OLD" & vbCrLf
sConsulta = sConsulta & "  UPDATE PM_COPIA_ENLACE_CONDICION SET VALOR_TEXT=STUFF(VALOR_TEXT,@LEN_COD_UON1 + @LEN_COD_UON2 + 3,@LEN_COD_UON3,@NEW + SPACE(@LEN_COD_UON3 - LEN(@NEW))) WHERE TIPO_CAMPO=4 AND TIPO_VALOR=2 AND LTRIM(RTRIM(SUBSTRING(VALOR_TEXT,1,@LEN_COD_UON1)))=@UON1 AND LTRIM(RTRIM(SUBSTRING(VALOR_TEXT,@LEN_COD_UON1+2, @LEN_COD_UON2)))=@UON2 AND LTRIM(RTRIM(SUBSTRING(VALOR_TEXT,@LEN_COD_UON1+@LEN_COD_UON2+3, @LEN_COD_UON3)))=@OLD" & vbCrLf
sConsulta = sConsulta & "CLOSE C2" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C2" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--INICIO ACTUALIZACION COPIA_CAMPO, COPIA_LINEA_DESGLOSE, INSTANCIA_CENTROS_PARTIDAS, PM_CONTR_FILTRO_USU_BUSQUEDA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--ACTUALIZAR DATOS DE UONS EN LOS CENTROS Y PARTIDAS GRABADOS EN " & vbCrLf
sConsulta = sConsulta & "   --COPIA_CAMPO" & vbCrLf
sConsulta = sConsulta & "   --COPIA_LINEA_DESGLOSE" & vbCrLf
sConsulta = sConsulta & "   --INSTANCIA_CENTROS_DESGLOSE --> TABLA EN LA QUE SE HACE COPIA DE LOS DATOS DE CENTROS Y PARTIDAS DE COPIA_CAMPO Y COPIA_LINEA_DESGLOSE PARA ACELERAR FILTRADOS EN EL BUSCADOR DEL VISOR DE CONTRATOS" & vbCrLf
sConsulta = sConsulta & "   --PM_CONTR_FILTRO_USU_BUSQUEDA --> TABLA EN LA QUE SE HACE COPIA DE LOS VALORES POR DEFECTO PARA LOS FILTROS DEL BUSCADOR DEL VISOR DE CONTRATOS, QUE INCLUYE, ENTRE OTRAS COSAS CENTROS Y PARTIDAS." & vbCrLf
sConsulta = sConsulta & "--HAY QUE MODIFICARLO PORQUE EL MODO EN QUE SE GRABAN CENTROS Y PARTIDAS ES:" & vbCrLf
sConsulta = sConsulta & "--CENTROS: UON1#UON2#... HASTA EL NIVEL AL QUE EST� VINCULADO EL CENTRO" & vbCrLf
sConsulta = sConsulta & "--PARTIDAS: UON1#UON2#...#PRESN: UONS HASTA EL NIVEL AL QUE EST� VINCULADA LA PARTIDA Y PRESN, DONDE N ES EL NIVEL DE IMPUTACION QUE SE INDICA EN PARGEN_SM.IMP_NIVEL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- QU� ES MEJOR PARA BUSCAR LAS UON EN LOS CAMPOS, GUIARSE POR LA LONGITUD DEL C�DIGO UON O POR EL SEPARADOR #: " & vbCrLf
sConsulta = sConsulta & "-- DADO QUE SE PUEDE CAMBIAR LA LONGITUD EN LA TABLA DIC Y QUE EN LAS 4 TABLAS AL GUARDAR LOS DATOS NO SE TIENE EN CUENTA ESA LONGITUD, USAREMOS #" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--EN LAS PARTIDAS, EL �LTIMO VALOR ES SIEMPRE UNA PARTIDA PRESUPUESTARIA. AL MENOS HABR� SIEMPRE UN UON (UON1) PERO A PARTIR DE LOS UON2, PUEDE QUE NO HAYA, POR LO QUE TENEMOS QUE ASEGURARNOS DE NO CONFUNDIR LA PARTIDA CON UN UON AL REEMPLAZAR: VAMOS A IGNORAR LA PARTIDA AL FILTRAR" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- COPIA_CAMPO" & vbCrLf
sConsulta = sConsulta & "--     CENTROS" & vbCrLf
sConsulta = sConsulta & "DECLARE @SEPARADOR VARCHAR(3) = '#'" & vbCrLf
sConsulta = sConsulta & "UPDATE CC SET VALOR_TEXT = COALESCE(STUFF(CC.VALOR_TEXT, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1, LEN(@OLD), @NEW) , VALOR_TEXT)" & vbCrLf
sConsulta = sConsulta & "FROM COPIA_CAMPO CC WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_CAMPO_DEF CD WITH(NOLOCK) ON CC.COPIA_CAMPO_DEF=CD.ID" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "   CD.TIPO_CAMPO_GS = 129 AND " & vbCrLf
sConsulta = sConsulta & "   CC.VALOR_TEXT IS NOT NULL AND " & vbCrLf
sConsulta = sConsulta & "   SUBSTRING(CC.VALOR_TEXT, " & vbCrLf
sConsulta = sConsulta & "             1, " & vbCrLf
sConsulta = sConsulta & "             CASE WHEN " & vbCrLf
sConsulta = sConsulta & "               CHARINDEX(@SEPARADOR,CC.VALOR_TEXT, CHARINDEX(@SEPARADOR,CC.VALOR_TEXT, CHARINDEX (@SEPARADOR,CC.VALOR_TEXT) + 1) + 1)=0 THEN " & vbCrLf
sConsulta = sConsulta & "                   LEN(CC.VALOR_TEXT) " & vbCrLf
sConsulta = sConsulta & "               ELSE " & vbCrLf
sConsulta = sConsulta & "                   CHARINDEX(@SEPARADOR,CC.VALOR_TEXT, CHARINDEX(@SEPARADOR,CC.VALOR_TEXT, CHARINDEX (@SEPARADOR,CC.VALOR_TEXT) + 1) + 1)-1" & vbCrLf
sConsulta = sConsulta & "               END" & vbCrLf
sConsulta = sConsulta & "           )=@UON1 + @SEPARADOR + @UON2 + @SEPARADOR + @OLD" & vbCrLf
sConsulta = sConsulta & "--     PARTIDAS" & vbCrLf
sConsulta = sConsulta & "UPDATE CC SET VALOR_TEXT = COALESCE(STUFF(CC.VALOR_TEXT, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1, LEN(@OLD), @NEW) , VALOR_TEXT)" & vbCrLf
sConsulta = sConsulta & "FROM COPIA_CAMPO CC WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_CAMPO_DEF CD WITH(NOLOCK) ON CC.COPIA_CAMPO_DEF=CD.ID" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "   CD.TIPO_CAMPO_GS = 130 AND " & vbCrLf
sConsulta = sConsulta & "   CC.VALOR_TEXT IS NOT NULL AND " & vbCrLf
sConsulta = sConsulta & "   SUBSTRING(SUBSTRING(CC.VALOR_TEXT, 1, LEN(CC.VALOR_TEXT) - CHARINDEX(@SEPARADOR, REVERSE(CC.VALOR_TEXT))),  --QUITAMOS LA PARTIDA PRES" & vbCrLf
sConsulta = sConsulta & "             1, " & vbCrLf
sConsulta = sConsulta & "             CASE WHEN " & vbCrLf
sConsulta = sConsulta & "               CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                         SUBSTRING(CC.VALOR_TEXT, 1, LEN(CC.VALOR_TEXT) - CHARINDEX(@SEPARADOR, REVERSE(CC.VALOR_TEXT))), " & vbCrLf
sConsulta = sConsulta & "                         CHARINDEX(@SEPARADOR,CC.VALOR_TEXT, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1)" & vbCrLf
sConsulta = sConsulta & "                         )=0 THEN " & vbCrLf
sConsulta = sConsulta & "                   LEN(SUBSTRING(CC.VALOR_TEXT, 1, LEN(CC.VALOR_TEXT) - CHARINDEX(@SEPARADOR, REVERSE(CC.VALOR_TEXT))))" & vbCrLf
sConsulta = sConsulta & "               ELSE " & vbCrLf
sConsulta = sConsulta & "                   CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                             SUBSTRING(CC.VALOR_TEXT, 1, LEN(CC.VALOR_TEXT) - CHARINDEX(@SEPARADOR, REVERSE(CC.VALOR_TEXT))), " & vbCrLf
sConsulta = sConsulta & "                             CHARINDEX(@SEPARADOR,CC.VALOR_TEXT, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1))-1" & vbCrLf
sConsulta = sConsulta & "               END" & vbCrLf
sConsulta = sConsulta & "           )=@UON1 + @SEPARADOR + @UON2 + @SEPARADOR + @OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- COPIA_LINEA_DESGLOSE" & vbCrLf
sConsulta = sConsulta & "--     CENTROS" & vbCrLf
sConsulta = sConsulta & "UPDATE CLD SET VALOR_TEXT = COALESCE(STUFF(CLD.VALOR_TEXT, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1, LEN(@OLD), @NEW) , CLD.VALOR_TEXT)" & vbCrLf
sConsulta = sConsulta & "FROM COPIA_LINEA_DESGLOSE CLD WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_CAMPO CC WITH (NOLOCK) ON CLD.CAMPO_HIJO = CC.ID " & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_CAMPO_DEF CCD WITH (NOLOCK) ON CC.COPIA_CAMPO_DEF = CCD.ID" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "   CCD.TIPO_CAMPO_GS = 129 AND " & vbCrLf
sConsulta = sConsulta & "   CLD.VALOR_TEXT IS NOT NULL AND " & vbCrLf
sConsulta = sConsulta & "   SUBSTRING(CLD.VALOR_TEXT, " & vbCrLf
sConsulta = sConsulta & "             1, " & vbCrLf
sConsulta = sConsulta & "             CASE WHEN " & vbCrLf
sConsulta = sConsulta & "               CHARINDEX(@SEPARADOR,CLD.VALOR_TEXT, CHARINDEX(@SEPARADOR,CLD.VALOR_TEXT, CHARINDEX (@SEPARADOR,CLD.VALOR_TEXT) + 1) + 1)=0 THEN " & vbCrLf
sConsulta = sConsulta & "                   LEN(CLD.VALOR_TEXT) " & vbCrLf
sConsulta = sConsulta & "               ELSE " & vbCrLf
sConsulta = sConsulta & "                   CHARINDEX(@SEPARADOR,CLD.VALOR_TEXT, CHARINDEX(@SEPARADOR,CLD.VALOR_TEXT, CHARINDEX (@SEPARADOR,CLD.VALOR_TEXT) + 1) + 1)-1" & vbCrLf
sConsulta = sConsulta & "               END" & vbCrLf
sConsulta = sConsulta & "           )=@UON1 + @SEPARADOR + @UON2 + @SEPARADOR + @OLD" & vbCrLf
sConsulta = sConsulta & "--     PARTIDAS" & vbCrLf
sConsulta = sConsulta & "UPDATE CLD SET VALOR_TEXT = COALESCE(STUFF(CLD.VALOR_TEXT, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1, LEN(@OLD), @NEW) , CLD.VALOR_TEXT)" & vbCrLf
sConsulta = sConsulta & "FROM COPIA_LINEA_DESGLOSE CLD WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_CAMPO CC WITH (NOLOCK) ON CLD.CAMPO_HIJO = CC.ID " & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_CAMPO_DEF CCD WITH (NOLOCK) ON CC.COPIA_CAMPO_DEF = CCD.ID" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "   CCD.TIPO_CAMPO_GS = 130 AND " & vbCrLf
sConsulta = sConsulta & "   CLD.VALOR_TEXT IS NOT NULL AND " & vbCrLf
sConsulta = sConsulta & "   SUBSTRING(SUBSTRING(CLD.VALOR_TEXT, 1, LEN(CLD.VALOR_TEXT) - CHARINDEX(@SEPARADOR, REVERSE(CLD.VALOR_TEXT))),  --QUITAMOS LA PARTIDA PRES" & vbCrLf
sConsulta = sConsulta & "             1, " & vbCrLf
sConsulta = sConsulta & "             CASE WHEN " & vbCrLf
sConsulta = sConsulta & "               CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                         SUBSTRING(CLD.VALOR_TEXT, 1, LEN(CLD.VALOR_TEXT) - CHARINDEX(@SEPARADOR, REVERSE(CLD.VALOR_TEXT))), " & vbCrLf
sConsulta = sConsulta & "                         CHARINDEX(@SEPARADOR,CLD.VALOR_TEXT, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1)" & vbCrLf
sConsulta = sConsulta & "                         )=0 THEN " & vbCrLf
sConsulta = sConsulta & "                   LEN(SUBSTRING(CLD.VALOR_TEXT, 1, LEN(CLD.VALOR_TEXT) - CHARINDEX(@SEPARADOR, REVERSE(CLD.VALOR_TEXT))))" & vbCrLf
sConsulta = sConsulta & "               ELSE " & vbCrLf
sConsulta = sConsulta & "                   CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                             SUBSTRING(CLD.VALOR_TEXT, 1, LEN(CLD.VALOR_TEXT) - CHARINDEX(@SEPARADOR, REVERSE(CLD.VALOR_TEXT))), " & vbCrLf
sConsulta = sConsulta & "                             CHARINDEX(@SEPARADOR,CLD.VALOR_TEXT, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1))-1" & vbCrLf
sConsulta = sConsulta & "               END" & vbCrLf
sConsulta = sConsulta & "           )=@UON1 + @SEPARADOR + @UON2 + @SEPARADOR + @OLD" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "-- INSTANCIA_CENTROS_PARTIDAS" & vbCrLf
sConsulta = sConsulta & "--     CENTROS" & vbCrLf
sConsulta = sConsulta & "UPDATE ICP SET VALOR_TEXT = COALESCE(STUFF(ICP.VALOR_TEXT, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1, LEN(@OLD), @NEW) , VALOR_TEXT)" & vbCrLf
sConsulta = sConsulta & "FROM INSTANCIA_CENTROS_PARTIDAS ICP WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "ICP.TIPO_CAMPO_GS = 129 AND " & vbCrLf
sConsulta = sConsulta & "ICP.VALOR_TEXT IS NOT NULL AND" & vbCrLf
sConsulta = sConsulta & "SUBSTRING(ICP.VALOR_TEXT, " & vbCrLf
sConsulta = sConsulta & "         1, " & vbCrLf
sConsulta = sConsulta & "         CASE WHEN " & vbCrLf
sConsulta = sConsulta & "           CHARINDEX(@SEPARADOR,ICP.VALOR_TEXT, CHARINDEX(@SEPARADOR,ICP.VALOR_TEXT, CHARINDEX (@SEPARADOR,ICP.VALOR_TEXT) + 1) + 1)=0 THEN " & vbCrLf
sConsulta = sConsulta & "               LEN(ICP.VALOR_TEXT) " & vbCrLf
sConsulta = sConsulta & "           ELSE " & vbCrLf
sConsulta = sConsulta & "               CHARINDEX(@SEPARADOR,ICP.VALOR_TEXT, CHARINDEX(@SEPARADOR,ICP.VALOR_TEXT, CHARINDEX (@SEPARADOR,ICP.VALOR_TEXT) + 1) + 1)-1" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "       )=@UON1 + @SEPARADOR + @UON2 + @SEPARADOR + @OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--     PARTIDAS" & vbCrLf
sConsulta = sConsulta & "UPDATE ICP SET VALOR_TEXT = COALESCE(STUFF(ICP.VALOR_TEXT, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1, LEN(@OLD), @NEW) , VALOR_TEXT)" & vbCrLf
sConsulta = sConsulta & "FROM INSTANCIA_CENTROS_PARTIDAS ICP WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "ICP.TIPO_CAMPO_GS  = 130 AND " & vbCrLf
sConsulta = sConsulta & "ICP.VALOR_TEXT IS NOT NULL AND" & vbCrLf
sConsulta = sConsulta & "SUBSTRING(SUBSTRING(ICP.VALOR_TEXT, 1, LEN(ICP.VALOR_TEXT) - CHARINDEX(@SEPARADOR, REVERSE(ICP.VALOR_TEXT))),  --QUITAMOS LA PARTIDA PRES" & vbCrLf
sConsulta = sConsulta & "         1, " & vbCrLf
sConsulta = sConsulta & "         CASE WHEN " & vbCrLf
sConsulta = sConsulta & "           CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                     SUBSTRING(ICP.VALOR_TEXT, 1, LEN(ICP.VALOR_TEXT) - CHARINDEX(@SEPARADOR, REVERSE(ICP.VALOR_TEXT))), " & vbCrLf
sConsulta = sConsulta & "                     CHARINDEX(@SEPARADOR,ICP.VALOR_TEXT, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1)" & vbCrLf
sConsulta = sConsulta & "                     )=0 THEN " & vbCrLf
sConsulta = sConsulta & "               LEN(SUBSTRING(ICP.VALOR_TEXT, 1, LEN(ICP.VALOR_TEXT) - CHARINDEX(@SEPARADOR, REVERSE(ICP.VALOR_TEXT))))" & vbCrLf
sConsulta = sConsulta & "           ELSE " & vbCrLf
sConsulta = sConsulta & "               CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                         SUBSTRING(ICP.VALOR_TEXT, 1, LEN(ICP.VALOR_TEXT) - CHARINDEX(@SEPARADOR, REVERSE(ICP.VALOR_TEXT))), " & vbCrLf
sConsulta = sConsulta & "                         CHARINDEX(@SEPARADOR,ICP.VALOR_TEXT, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1))-1" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "       )=@UON1 + @SEPARADOR + @UON2 + @SEPARADOR + @OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- PM_CONTR_FILTRO_USU_BUSQUEDA" & vbCrLf
sConsulta = sConsulta & "UPDATE P SET CENTRO_COSTE = COALESCE(STUFF(CENTRO_COSTE, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1, LEN(@OLD), @NEW) , CENTRO_COSTE)" & vbCrLf
sConsulta = sConsulta & "FROM PM_CONTR_FILTRO_USU_BUSQUEDA P WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "P.CENTRO_COSTE IS NOT NULL AND" & vbCrLf
sConsulta = sConsulta & "SUBSTRING(P.CENTRO_COSTE, " & vbCrLf
sConsulta = sConsulta & "         1, " & vbCrLf
sConsulta = sConsulta & "         CASE WHEN " & vbCrLf
sConsulta = sConsulta & "           CHARINDEX(@SEPARADOR,P.CENTRO_COSTE, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1)=0 THEN " & vbCrLf
sConsulta = sConsulta & "               LEN(P.CENTRO_COSTE) " & vbCrLf
sConsulta = sConsulta & "           ELSE " & vbCrLf
sConsulta = sConsulta & "               CHARINDEX(@SEPARADOR,P.CENTRO_COSTE, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1)-1" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "       )=@UON1 + @SEPARADOR + @UON2 + @SEPARADOR + @OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE P SET PARTIDA1 = COALESCE(STUFF(PARTIDA1, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1, LEN(@OLD), @NEW) , PARTIDA1)" & vbCrLf
sConsulta = sConsulta & "FROM PM_CONTR_FILTRO_USU_BUSQUEDA P WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "P.PARTIDA1 IS NOT NULL AND" & vbCrLf
sConsulta = sConsulta & "SUBSTRING(SUBSTRING(P.PARTIDA1, 1, LEN(P.PARTIDA1) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA1))),  --QUITAMOS LA PARTIDA PRES" & vbCrLf
sConsulta = sConsulta & "         1, " & vbCrLf
sConsulta = sConsulta & "         CASE WHEN " & vbCrLf
sConsulta = sConsulta & "           CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                     SUBSTRING(P.PARTIDA1, 1, LEN(P.PARTIDA1) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA1))), " & vbCrLf
sConsulta = sConsulta & "                     CHARINDEX(@SEPARADOR,P.PARTIDA1, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1)" & vbCrLf
sConsulta = sConsulta & "                     )=0 THEN " & vbCrLf
sConsulta = sConsulta & "               LEN(SUBSTRING(P.PARTIDA1, 1, LEN(P.PARTIDA1) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA1))))" & vbCrLf
sConsulta = sConsulta & "           ELSE " & vbCrLf
sConsulta = sConsulta & "               CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                         SUBSTRING(P.PARTIDA1, 1, LEN(P.PARTIDA1) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA1))), " & vbCrLf
sConsulta = sConsulta & "                         CHARINDEX(@SEPARADOR,P.PARTIDA1, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1))-1" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "       )=@UON1 + @SEPARADOR + @UON2 + @SEPARADOR + @OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE P SET PARTIDA2 = COALESCE(STUFF(PARTIDA2, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1, LEN(@OLD), @NEW) , PARTIDA2)" & vbCrLf
sConsulta = sConsulta & "FROM PM_CONTR_FILTRO_USU_BUSQUEDA P WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "P.PARTIDA2 IS NOT NULL AND" & vbCrLf
sConsulta = sConsulta & "SUBSTRING(SUBSTRING(P.PARTIDA2, 1, LEN(P.PARTIDA2) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA2))),  --QUITAMOS LA PARTIDA PRES" & vbCrLf
sConsulta = sConsulta & "         1, " & vbCrLf
sConsulta = sConsulta & "         CASE WHEN " & vbCrLf
sConsulta = sConsulta & "           CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                     SUBSTRING(P.PARTIDA2, 1, LEN(P.PARTIDA2) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA2))), " & vbCrLf
sConsulta = sConsulta & "                     CHARINDEX(@SEPARADOR,P.PARTIDA2, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1)" & vbCrLf
sConsulta = sConsulta & "                     )=0 THEN " & vbCrLf
sConsulta = sConsulta & "               LEN(SUBSTRING(P.PARTIDA2, 1, LEN(P.PARTIDA2) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA2))))" & vbCrLf
sConsulta = sConsulta & "           ELSE " & vbCrLf
sConsulta = sConsulta & "               CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                         SUBSTRING(P.PARTIDA2, 1, LEN(P.PARTIDA2) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA2))), " & vbCrLf
sConsulta = sConsulta & "                         CHARINDEX(@SEPARADOR,P.PARTIDA2, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1))-1" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "       )=@UON1 + @SEPARADOR + @UON2 + @SEPARADOR + @OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE P SET PARTIDA3 = COALESCE(STUFF(PARTIDA3, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1, LEN(@OLD), @NEW) , PARTIDA3)" & vbCrLf
sConsulta = sConsulta & "FROM PM_CONTR_FILTRO_USU_BUSQUEDA P WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "P.PARTIDA3 IS NOT NULL AND" & vbCrLf
sConsulta = sConsulta & "SUBSTRING(SUBSTRING(P.PARTIDA3, 1, LEN(P.PARTIDA3) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA3))),  --QUITAMOS LA PARTIDA PRES" & vbCrLf
sConsulta = sConsulta & "         1, " & vbCrLf
sConsulta = sConsulta & "         CASE WHEN " & vbCrLf
sConsulta = sConsulta & "           CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                     SUBSTRING(P.PARTIDA3, 1, LEN(P.PARTIDA3) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA3))), " & vbCrLf
sConsulta = sConsulta & "                     CHARINDEX(@SEPARADOR,P.PARTIDA3, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1)" & vbCrLf
sConsulta = sConsulta & "                     )=0 THEN " & vbCrLf
sConsulta = sConsulta & "               LEN(SUBSTRING(P.PARTIDA3, 1, LEN(P.PARTIDA3) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA3))))" & vbCrLf
sConsulta = sConsulta & "           ELSE " & vbCrLf
sConsulta = sConsulta & "               CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                         SUBSTRING(P.PARTIDA3, 1, LEN(P.PARTIDA3) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA3))), " & vbCrLf
sConsulta = sConsulta & "                         CHARINDEX(@SEPARADOR,P.PARTIDA3, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1))-1" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "       )=@UON1 + @SEPARADOR + @UON2 + @SEPARADOR + @OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE P SET PARTIDA4 = COALESCE(STUFF(PARTIDA4, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1, LEN(@OLD), @NEW) , PARTIDA4)" & vbCrLf
sConsulta = sConsulta & "FROM PM_CONTR_FILTRO_USU_BUSQUEDA P WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "P.PARTIDA4 IS NOT NULL AND" & vbCrLf
sConsulta = sConsulta & "SUBSTRING(SUBSTRING(P.PARTIDA4, 1, LEN(P.PARTIDA4) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA4))),  --QUITAMOS LA PARTIDA PRES" & vbCrLf
sConsulta = sConsulta & "         1, " & vbCrLf
sConsulta = sConsulta & "         CASE WHEN " & vbCrLf
sConsulta = sConsulta & "           CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                     SUBSTRING(P.PARTIDA4, 1, LEN(P.PARTIDA4) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA4))), " & vbCrLf
sConsulta = sConsulta & "                     CHARINDEX(@SEPARADOR,P.PARTIDA4, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1)" & vbCrLf
sConsulta = sConsulta & "                     )=0 THEN " & vbCrLf
sConsulta = sConsulta & "               LEN(SUBSTRING(P.PARTIDA4, 1, LEN(P.PARTIDA4) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA4))))" & vbCrLf
sConsulta = sConsulta & "           ELSE " & vbCrLf
sConsulta = sConsulta & "               CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                         SUBSTRING(P.PARTIDA4, 1, LEN(P.PARTIDA4) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA4))), " & vbCrLf
sConsulta = sConsulta & "                         CHARINDEX(@SEPARADOR,P.PARTIDA4, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1))-1" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "       )=@UON1 + @SEPARADOR + @UON2 + @SEPARADOR + @OLD" & vbCrLf
sConsulta = sConsulta & "--FIN ACTUALIZACION COPIA_CAMPO, COPIA_LINEA_DESGLOSE, INSTANCIA_CENTROS_PARTIDAS, PM_CONTR_FILTRO_USU_BUSQUEDA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3_DEP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PER CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3_DEST CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISOR CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CERTIFICADO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE NOCONFORMIDAD CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE APROBADORES CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PETICIONARIOS CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_ROL CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_ENLACE_CONDICIONES CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_COPIA_ROL CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_COPIA_ENLACE_CONDICION CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_UON CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU_CC_CONTROL CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU_CC_IMPUTACION CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES5_UON CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CENTRO_SM_UON CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON4_DEST CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_PED_IMPUTACION CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PERF_UON CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_CAT_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_CAT_UON3_DEP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_GRUPO_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_GRUPO_UON3_DEP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_MEN_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_MEN_UON3_DEP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CUSTOM_TEMPLATE_UON CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "CLOSE C" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "COMMIT TRANSACTION" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT OFF" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UON2_COD]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[UON2_COD]" & vbCrLf
    gRDOCon.QueryTimeout = 7200
    ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE  PROCEDURE UON2_COD (@OLD VARCHAR(50),@NEW VARCHAR(50), @UON1 VARCHAR(50))  AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT ON" & vbCrLf
sConsulta = sConsulta & "BEGIN TRANSACTION" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR FOR SELECT COD FROM UON2 WITH(NOLOCK) WHERE UON1=@UON1 AND COD=@OLD" & vbCrLf
sConsulta = sConsulta & "OPEN C" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C INTO @VAR_COD" & vbCrLf
sConsulta = sConsulta & "IF @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON2_DEP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3_DEP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PER NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON2_DEST NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3_DEST NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISOR NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CERTIFICADO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE NOCONFORMIDAD NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE APROBADORES NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PETICIONARIOS NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_ROL NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_ENLACE_CONDICIONES NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_COPIA_ROL NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_COPIA_ENLACE_CONDICION NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_UON NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU_CC_CONTROL NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU_CC_IMPUTACION NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES5_UON NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CENTRO_SM_UON NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON4_DEST NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_PED_IMPUTACION NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PERF_UON NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_CAT_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_CAT_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_CAT_UON2_DEP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_CAT_UON3_DEP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_GRUPO_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_GRUPO_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_GRUPO_UON2_DEP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_GRUPO_UON3_DEP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_MEN_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_MEN_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_MEN_UON2_DEP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_MEN_UON3_DEP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CUSTOM_TEMPLATE_UON NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE UON2 SET COD=@NEW WHERE UON1=@UON1 AND COD=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON3 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON4 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON2_DEP SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON3_DEP SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PER SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE COM SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_UON2 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_UON3 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_UON2 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_UON3 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON2_DEST SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON3_DEST SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_UON2 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_UON3 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_UON2 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_UON3 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_1 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_2 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_3 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_4 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_1 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_2 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_3 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_4 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON1 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON2 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON3 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON4 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY1 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY2 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY3 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY4 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_PRESCON3 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_PRESCON4 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES1_NIV1 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES1_NIV2 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES1_NIV3 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES1_NIV4 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES2_NIV1 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES2_NIV2 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES2_NIV3 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES2_NIV4 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES3_NIV1 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES3_NIV2 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES3_NIV3 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES3_NIV4 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES4_NIV1 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES4_NIV2 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES4_NIV3 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES4_NIV4 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISOR SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CERTIFICADO SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE NOCONFORMIDAD SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE APROBADORES SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PETICIONARIOS SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PM_ROL SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PM_COPIA_ROL SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4_UON SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET UON2_APER=@NEW WHERE UON1_APER=@UON1 AND UON2_APER=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE USU_CC_CONTROL SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE USU_CC_IMPUTACION SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES5_UON SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CENTRO_SM_UON SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON4_DEST SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE LINEAS_PED_IMPUTACION SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PERF_UON SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_CAT_UON2 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_CAT_UON3 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_CAT_UON2_DEP SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_CAT_UON3_DEP SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_GRUPO_UON2 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_GRUPO_UON3 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_GRUPO_UON2_DEP SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_GRUPO_UON3_DEP SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_MEN_UON2 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_MEN_UON3 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_MEN_UON2_DEP SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_MEN_UON3_DEP SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CUSTOM_TEMPLATE_UON SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @LEN_COD_UON1 AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @LEN_COD_UON2 AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE C2 CURSOR FOR SELECT (SELECT LONGITUD FROM DIC WITH(NOLOCK) WHERE NOMBRE='UON1') AS L1,(SELECT LONGITUD FROM DIC WITH(NOLOCK) WHERE NOMBRE='UON2') AS L2" & vbCrLf
sConsulta = sConsulta & "OPEN C2" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C2 INTO @LEN_COD_UON1,@LEN_COD_UON2" & vbCrLf
sConsulta = sConsulta & "IF @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "  UPDATE PM_ENLACE_CONDICIONES SET VALOR_TEXT=STUFF(VALOR_TEXT,@LEN_COD_UON1 + 2,@LEN_COD_UON2,@NEW + SPACE(@LEN_COD_UON2 - LEN(@NEW))) WHERE TIPO_CAMPO=4 AND TIPO_VALOR=2 AND LTRIM(RTRIM(SUBSTRING(VALOR_TEXT,1,@LEN_COD_UON1)))=@UON1 AND LTRIM(RTRIM(SUBSTRING(VALOR_TEXT,@LEN_COD_UON1+2, @LEN_COD_UON2)))=@OLD" & vbCrLf
sConsulta = sConsulta & "  UPDATE PM_COPIA_ENLACE_CONDICION SET VALOR_TEXT=STUFF(VALOR_TEXT,@LEN_COD_UON1 + 2,@LEN_COD_UON2,@NEW + SPACE(@LEN_COD_UON2 - LEN(@NEW))) WHERE TIPO_CAMPO=4 AND TIPO_VALOR=2 AND LTRIM(RTRIM(SUBSTRING(VALOR_TEXT,1,@LEN_COD_UON1)))=@UON1 AND LTRIM(RTRIM(SUBSTRING(VALOR_TEXT,@LEN_COD_UON1+2, @LEN_COD_UON2)))=@OLD" & vbCrLf
sConsulta = sConsulta & "CLOSE C2" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C2" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--INICIO ACTUALIZACION COPIA_CAMPO, COPIA_LINEA_DESGLOSE, INSTANCIA_CENTROS_PARTIDAS, PM_CONTR_FILTRO_USU_BUSQUEDA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--ACTUALIZAR DATOS DE UONS EN LOS CENTROS Y PARTIDAS GRABADOS EN " & vbCrLf
sConsulta = sConsulta & "   --COPIA_CAMPO" & vbCrLf
sConsulta = sConsulta & "   --COPIA_LINEA_DESGLOSE" & vbCrLf
sConsulta = sConsulta & "   --INSTANCIA_CENTROS_DESGLOSE --> TABLA EN LA QUE SE HACE COPIA DE LOS DATOS DE CENTROS Y PARTIDAS DE COPIA_CAMPO Y COPIA_LINEA_DESGLOSE PARA ACELERAR FILTRADOS EN EL BUSCADOR DEL VISOR DE CONTRATOS" & vbCrLf
sConsulta = sConsulta & "   --PM_CONTR_FILTRO_USU_BUSQUEDA --> TABLA EN LA QUE SE HACE COPIA DE LOS VALORES POR DEFECTO PARA LOS FILTROS DEL BUSCADOR DEL VISOR DE CONTRATOS, QUE INCLUYE, ENTRE OTRAS COSAS CENTROS Y PARTIDAS." & vbCrLf
sConsulta = sConsulta & "--HAY QUE MODIFICARLO PORQUE EL MODO EN QUE SE GRABAN CENTROS Y PARTIDAS ES:" & vbCrLf
sConsulta = sConsulta & "--CENTROS: UON1#UON2#... HASTA EL NIVEL AL QUE EST� VINCULADO EL CENTRO" & vbCrLf
sConsulta = sConsulta & "--PARTIDAS: UON1#UON2#...#PRESN: UONS HASTA EL NIVEL AL QUE EST� VINCULADA LA PARTIDA Y PRESN, DONDE N ES EL NIVEL DE IMPUTACION QUE SE INDICA EN PARGEN_SM.IMP_NIVEL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- QU� ES MEJOR PARA BUSCAR LAS UON EN LOS CAMPOS, GUIARSE POR LA LONGITUD DEL C�DIGO UON O POR EL SEPARADOR #: " & vbCrLf
sConsulta = sConsulta & "-- DADO QUE SE PUEDE CAMBIAR LA LONGITUD EN LA TABLA DIC Y QUE EN LAS 4 TABLAS AL GUARDAR LOS DATOS NO SE TIENE EN CUENTA ESA LONGITUD, USAREMOS #" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--EN LAS PARTIDAS, EL �LTIMO VALOR ES SIEMPRE UNA PARTIDA PRESUPUESTARIA. AL MENOS HABR� SIEMPRE UN UON (UON1) PERO A PARTIR DE LOS UON2, PUEDE QUE NO HAYA, POR LO QUE TENEMOS QUE ASEGURARNOS DE NO CONFUNDIR LA PARTIDA CON UN UON AL REEMPLAZAR: VAMOS A IGNORAR LA PARTIDA AL FILTRAR" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- COPIA_CAMPO" & vbCrLf
sConsulta = sConsulta & "--     CENTROS" & vbCrLf
sConsulta = sConsulta & "DECLARE @SEPARADOR VARCHAR(3) = '#'" & vbCrLf
sConsulta = sConsulta & "UPDATE CC SET VALOR_TEXT = COALESCE(STUFF(CC.VALOR_TEXT, LEN(@UON1 + @SEPARADOR) + 1, LEN(@OLD), @NEW) , VALOR_TEXT)" & vbCrLf
sConsulta = sConsulta & "FROM COPIA_CAMPO CC WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_CAMPO_DEF CD WITH(NOLOCK) ON CC.COPIA_CAMPO_DEF=CD.ID" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "   CD.TIPO_CAMPO_GS = 129 AND " & vbCrLf
sConsulta = sConsulta & "   CC.VALOR_TEXT IS NOT NULL AND " & vbCrLf
sConsulta = sConsulta & "   SUBSTRING(CC.VALOR_TEXT, " & vbCrLf
sConsulta = sConsulta & "             1, " & vbCrLf
sConsulta = sConsulta & "             CASE WHEN " & vbCrLf
sConsulta = sConsulta & "               CHARINDEX(@SEPARADOR,CC.VALOR_TEXT, CHARINDEX(@SEPARADOR,CC.VALOR_TEXT) + 1)=0 THEN " & vbCrLf
sConsulta = sConsulta & "                   LEN(CC.VALOR_TEXT) " & vbCrLf
sConsulta = sConsulta & "               ELSE " & vbCrLf
sConsulta = sConsulta & "                   CHARINDEX(@SEPARADOR,CC.VALOR_TEXT, CHARINDEX(@SEPARADOR,CC.VALOR_TEXT) + 1)-1" & vbCrLf
sConsulta = sConsulta & "               END" & vbCrLf
sConsulta = sConsulta & "           )=@UON1 + @SEPARADOR + @OLD" & vbCrLf
sConsulta = sConsulta & "--     PARTIDAS" & vbCrLf
sConsulta = sConsulta & "UPDATE CC SET VALOR_TEXT = COALESCE(STUFF(CC.VALOR_TEXT, LEN(@UON1 + @SEPARADOR) + 1, LEN(@OLD), @NEW) , VALOR_TEXT)" & vbCrLf
sConsulta = sConsulta & "FROM COPIA_CAMPO CC WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_CAMPO_DEF CD WITH(NOLOCK) ON CC.COPIA_CAMPO_DEF=CD.ID" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "   CD.TIPO_CAMPO_GS = 130 AND " & vbCrLf
sConsulta = sConsulta & "   CC.VALOR_TEXT IS NOT NULL AND " & vbCrLf
sConsulta = sConsulta & "   SUBSTRING(SUBSTRING(CC.VALOR_TEXT, 1, LEN(CC.VALOR_TEXT) - CHARINDEX(@SEPARADOR, REVERSE(CC.VALOR_TEXT))),  --QUITAMOS LA PARTIDA PRES" & vbCrLf
sConsulta = sConsulta & "             1, " & vbCrLf
sConsulta = sConsulta & "             CASE WHEN " & vbCrLf
sConsulta = sConsulta & "               CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                         SUBSTRING(CC.VALOR_TEXT, 1, LEN(CC.VALOR_TEXT) - CHARINDEX(@SEPARADOR, REVERSE(CC.VALOR_TEXT))), " & vbCrLf
sConsulta = sConsulta & "                         CHARINDEX(@SEPARADOR,CC.VALOR_TEXT, LEN(@UON1 + @SEPARADOR) + 1)" & vbCrLf
sConsulta = sConsulta & "                         )=0 THEN " & vbCrLf
sConsulta = sConsulta & "                   LEN(SUBSTRING(CC.VALOR_TEXT, 1, LEN(CC.VALOR_TEXT) - CHARINDEX(@SEPARADOR, REVERSE(CC.VALOR_TEXT))))" & vbCrLf
sConsulta = sConsulta & "               ELSE " & vbCrLf
sConsulta = sConsulta & "                   CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                             SUBSTRING(CC.VALOR_TEXT, 1, LEN(CC.VALOR_TEXT) - CHARINDEX(@SEPARADOR, REVERSE(CC.VALOR_TEXT))), " & vbCrLf
sConsulta = sConsulta & "                             CHARINDEX(@SEPARADOR,CC.VALOR_TEXT, CHARINDEX(@SEPARADOR,CC.VALOR_TEXT) + 1))-1" & vbCrLf
sConsulta = sConsulta & "               END" & vbCrLf
sConsulta = sConsulta & "           )=@UON1 + @SEPARADOR + @OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- COPIA_LINEA_DESGLOSE" & vbCrLf
sConsulta = sConsulta & "--     CENTROS" & vbCrLf
sConsulta = sConsulta & "UPDATE CLD SET VALOR_TEXT = COALESCE(STUFF(CLD.VALOR_TEXT, LEN(@UON1 + @SEPARADOR) + 1, LEN(@OLD), @NEW) , CLD.VALOR_TEXT)" & vbCrLf
sConsulta = sConsulta & "FROM COPIA_LINEA_DESGLOSE CLD WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_CAMPO CC WITH (NOLOCK) ON CLD.CAMPO_HIJO = CC.ID " & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_CAMPO_DEF CCD WITH (NOLOCK) ON CC.COPIA_CAMPO_DEF = CCD.ID" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "   CCD.TIPO_CAMPO_GS = 129 AND " & vbCrLf
sConsulta = sConsulta & "   CLD.VALOR_TEXT IS NOT NULL AND " & vbCrLf
sConsulta = sConsulta & "   SUBSTRING(CLD.VALOR_TEXT, " & vbCrLf
sConsulta = sConsulta & "             1, " & vbCrLf
sConsulta = sConsulta & "             CASE WHEN " & vbCrLf
sConsulta = sConsulta & "               CHARINDEX(@SEPARADOR,CLD.VALOR_TEXT, LEN(@UON1 + @SEPARADOR) + 1)=0 THEN " & vbCrLf
sConsulta = sConsulta & "                   LEN(CLD.VALOR_TEXT) " & vbCrLf
sConsulta = sConsulta & "               ELSE " & vbCrLf
sConsulta = sConsulta & "                   CHARINDEX(@SEPARADOR,CLD.VALOR_TEXT, LEN(@UON1 + @SEPARADOR) + 1)-1" & vbCrLf
sConsulta = sConsulta & "               END" & vbCrLf
sConsulta = sConsulta & "           )=@UON1 + @SEPARADOR + @OLD" & vbCrLf
sConsulta = sConsulta & "--     PARTIDAS" & vbCrLf
sConsulta = sConsulta & "UPDATE CLD SET VALOR_TEXT = COALESCE(STUFF(CLD.VALOR_TEXT, LEN(@UON1 + @SEPARADOR) + 1, LEN(@OLD), @NEW) , CLD.VALOR_TEXT)" & vbCrLf
sConsulta = sConsulta & "FROM COPIA_LINEA_DESGLOSE CLD WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_CAMPO CC WITH (NOLOCK) ON CLD.CAMPO_HIJO = CC.ID " & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_CAMPO_DEF CCD WITH (NOLOCK) ON CC.COPIA_CAMPO_DEF = CCD.ID" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "   CCD.TIPO_CAMPO_GS = 130 AND " & vbCrLf
sConsulta = sConsulta & "   CLD.VALOR_TEXT IS NOT NULL AND " & vbCrLf
sConsulta = sConsulta & "   SUBSTRING(SUBSTRING(CLD.VALOR_TEXT, 1, LEN(CLD.VALOR_TEXT) - CHARINDEX(@SEPARADOR, REVERSE(CLD.VALOR_TEXT))),  --QUITAMOS LA PARTIDA PRES" & vbCrLf
sConsulta = sConsulta & "             1, " & vbCrLf
sConsulta = sConsulta & "             CASE WHEN " & vbCrLf
sConsulta = sConsulta & "               CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                         SUBSTRING(CLD.VALOR_TEXT, 1, LEN(CLD.VALOR_TEXT) - CHARINDEX(@SEPARADOR, REVERSE(CLD.VALOR_TEXT))), " & vbCrLf
sConsulta = sConsulta & "                         CHARINDEX(@SEPARADOR,CLD.VALOR_TEXT, LEN(@UON1 + @SEPARADOR) + 1)" & vbCrLf
sConsulta = sConsulta & "                         )=0 THEN " & vbCrLf
sConsulta = sConsulta & "                   LEN(SUBSTRING(CLD.VALOR_TEXT, 1, LEN(CLD.VALOR_TEXT) - CHARINDEX(@SEPARADOR, REVERSE(CLD.VALOR_TEXT))))" & vbCrLf
sConsulta = sConsulta & "               ELSE " & vbCrLf
sConsulta = sConsulta & "                   CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                             SUBSTRING(CLD.VALOR_TEXT, 1, LEN(CLD.VALOR_TEXT) - CHARINDEX(@SEPARADOR, REVERSE(CLD.VALOR_TEXT))), " & vbCrLf
sConsulta = sConsulta & "                             CHARINDEX(@SEPARADOR,CLD.VALOR_TEXT, LEN(@UON1 + @SEPARADOR) + 1))-1" & vbCrLf
sConsulta = sConsulta & "               END" & vbCrLf
sConsulta = sConsulta & "           )=@UON1 + @SEPARADOR + @OLD" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "-- INSTANCIA_CENTROS_PARTIDAS" & vbCrLf
sConsulta = sConsulta & "--     CENTROS" & vbCrLf
sConsulta = sConsulta & "UPDATE ICP SET VALOR_TEXT = COALESCE(STUFF(ICP.VALOR_TEXT, LEN(@UON1 + @SEPARADOR) + 1, LEN(@OLD), @NEW) , VALOR_TEXT)" & vbCrLf
sConsulta = sConsulta & "FROM INSTANCIA_CENTROS_PARTIDAS ICP WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "ICP.TIPO_CAMPO_GS = 129 AND " & vbCrLf
sConsulta = sConsulta & "ICP.VALOR_TEXT IS NOT NULL AND" & vbCrLf
sConsulta = sConsulta & "SUBSTRING(ICP.VALOR_TEXT, " & vbCrLf
sConsulta = sConsulta & "         1, " & vbCrLf
sConsulta = sConsulta & "         CASE WHEN " & vbCrLf
sConsulta = sConsulta & "           CHARINDEX(@SEPARADOR,ICP.VALOR_TEXT, LEN(@UON1 + @SEPARADOR) + 1)=0 THEN " & vbCrLf
sConsulta = sConsulta & "               LEN(ICP.VALOR_TEXT) " & vbCrLf
sConsulta = sConsulta & "           ELSE " & vbCrLf
sConsulta = sConsulta & "               CHARINDEX(@SEPARADOR,ICP.VALOR_TEXT, LEN(@UON1 + @SEPARADOR) + 1)-1" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "       )=@UON1 + @SEPARADOR + @OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--     PARTIDAS" & vbCrLf
sConsulta = sConsulta & "UPDATE ICP SET VALOR_TEXT = COALESCE(STUFF(ICP.VALOR_TEXT, LEN(@UON1 + @SEPARADOR) + 1, LEN(@OLD), @NEW) , VALOR_TEXT)" & vbCrLf
sConsulta = sConsulta & "FROM INSTANCIA_CENTROS_PARTIDAS ICP WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "ICP.TIPO_CAMPO_GS  = 130 AND " & vbCrLf
sConsulta = sConsulta & "ICP.VALOR_TEXT IS NOT NULL AND" & vbCrLf
sConsulta = sConsulta & "SUBSTRING(SUBSTRING(ICP.VALOR_TEXT, 1, LEN(ICP.VALOR_TEXT) - CHARINDEX(@SEPARADOR, REVERSE(ICP.VALOR_TEXT))),  --QUITAMOS LA PARTIDA PRES" & vbCrLf
sConsulta = sConsulta & "         1, " & vbCrLf
sConsulta = sConsulta & "         CASE WHEN " & vbCrLf
sConsulta = sConsulta & "           CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                     SUBSTRING(ICP.VALOR_TEXT, 1, LEN(ICP.VALOR_TEXT) - CHARINDEX(@SEPARADOR, REVERSE(ICP.VALOR_TEXT))), " & vbCrLf
sConsulta = sConsulta & "                     CHARINDEX(@SEPARADOR,ICP.VALOR_TEXT, LEN(@UON1 + @SEPARADOR) + 1)" & vbCrLf
sConsulta = sConsulta & "                     )=0 THEN " & vbCrLf
sConsulta = sConsulta & "               LEN(SUBSTRING(ICP.VALOR_TEXT, 1, LEN(ICP.VALOR_TEXT) - CHARINDEX(@SEPARADOR, REVERSE(ICP.VALOR_TEXT))))" & vbCrLf
sConsulta = sConsulta & "           ELSE " & vbCrLf
sConsulta = sConsulta & "               CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                         SUBSTRING(ICP.VALOR_TEXT, 1, LEN(ICP.VALOR_TEXT) - CHARINDEX(@SEPARADOR, REVERSE(ICP.VALOR_TEXT))), " & vbCrLf
sConsulta = sConsulta & "                         CHARINDEX(@SEPARADOR,ICP.VALOR_TEXT, LEN(@UON1 + @SEPARADOR) + 1))-1" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "       )=@UON1 + @SEPARADOR + @OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- PM_CONTR_FILTRO_USU_BUSQUEDA" & vbCrLf
sConsulta = sConsulta & "UPDATE P SET CENTRO_COSTE = COALESCE(STUFF(CENTRO_COSTE, LEN(@UON1 + @SEPARADOR) + 1, LEN(@OLD), @NEW) , CENTRO_COSTE)" & vbCrLf
sConsulta = sConsulta & "FROM PM_CONTR_FILTRO_USU_BUSQUEDA P WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "P.CENTRO_COSTE IS NOT NULL AND" & vbCrLf
sConsulta = sConsulta & "SUBSTRING(P.CENTRO_COSTE, " & vbCrLf
sConsulta = sConsulta & "         1, " & vbCrLf
sConsulta = sConsulta & "         CASE WHEN " & vbCrLf
sConsulta = sConsulta & "           CHARINDEX(@SEPARADOR,P.CENTRO_COSTE, LEN(@UON1 + @SEPARADOR) + 1)=0 THEN " & vbCrLf
sConsulta = sConsulta & "               LEN(P.CENTRO_COSTE) " & vbCrLf
sConsulta = sConsulta & "           ELSE " & vbCrLf
sConsulta = sConsulta & "               CHARINDEX(@SEPARADOR,P.CENTRO_COSTE, LEN(@UON1 + @SEPARADOR) + 1)-1" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "       )=@UON1 + @SEPARADOR + @OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE P SET PARTIDA1 = COALESCE(STUFF(PARTIDA1, LEN(@UON1 + @SEPARADOR) + 1, LEN(@OLD), @NEW) , PARTIDA1)" & vbCrLf
sConsulta = sConsulta & "FROM PM_CONTR_FILTRO_USU_BUSQUEDA P WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "P.PARTIDA1 IS NOT NULL AND" & vbCrLf
sConsulta = sConsulta & "SUBSTRING(SUBSTRING(P.PARTIDA1, 1, LEN(P.PARTIDA1) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA1))),  --QUITAMOS LA PARTIDA PRES" & vbCrLf
sConsulta = sConsulta & "         1, " & vbCrLf
sConsulta = sConsulta & "         CASE WHEN " & vbCrLf
sConsulta = sConsulta & "           CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                     SUBSTRING(P.PARTIDA1, 1, LEN(P.PARTIDA1) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA1))), " & vbCrLf
sConsulta = sConsulta & "                     CHARINDEX(@SEPARADOR,P.PARTIDA1, LEN(@UON1 + @SEPARADOR) + 1)" & vbCrLf
sConsulta = sConsulta & "                     )=0 THEN " & vbCrLf
sConsulta = sConsulta & "               LEN(SUBSTRING(P.PARTIDA1, 1, LEN(P.PARTIDA1) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA1))))" & vbCrLf
sConsulta = sConsulta & "           ELSE " & vbCrLf
sConsulta = sConsulta & "               CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                         SUBSTRING(P.PARTIDA1, 1, LEN(P.PARTIDA1) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA1))), " & vbCrLf
sConsulta = sConsulta & "                         CHARINDEX(@SEPARADOR,P.PARTIDA1, LEN(@UON1 + @SEPARADOR) + 1))-1" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "       )=@UON1 + @SEPARADOR + @OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE P SET PARTIDA2 = COALESCE(STUFF(PARTIDA2, LEN(@UON1 + @SEPARADOR) + 1, LEN(@OLD), @NEW) , PARTIDA2)" & vbCrLf
sConsulta = sConsulta & "FROM PM_CONTR_FILTRO_USU_BUSQUEDA P WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "P.PARTIDA2 IS NOT NULL AND" & vbCrLf
sConsulta = sConsulta & "SUBSTRING(SUBSTRING(P.PARTIDA2, 1, LEN(P.PARTIDA2) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA2))),  --QUITAMOS LA PARTIDA PRES" & vbCrLf
sConsulta = sConsulta & "         1, " & vbCrLf
sConsulta = sConsulta & "         CASE WHEN " & vbCrLf
sConsulta = sConsulta & "           CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                     SUBSTRING(P.PARTIDA2, 1, LEN(P.PARTIDA2) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA2))), " & vbCrLf
sConsulta = sConsulta & "                     CHARINDEX(@SEPARADOR,P.PARTIDA2, LEN(@UON1 + @SEPARADOR) + 1)" & vbCrLf
sConsulta = sConsulta & "                     )=0 THEN " & vbCrLf
sConsulta = sConsulta & "               LEN(SUBSTRING(P.PARTIDA2, 1, LEN(P.PARTIDA2) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA2))))" & vbCrLf
sConsulta = sConsulta & "           ELSE " & vbCrLf
sConsulta = sConsulta & "               CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                         SUBSTRING(P.PARTIDA2, 1, LEN(P.PARTIDA2) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA2))), " & vbCrLf
sConsulta = sConsulta & "                         CHARINDEX(@SEPARADOR,P.PARTIDA2, LEN(@UON1 + @SEPARADOR) + 1))-1" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "       )=@UON1 + @SEPARADOR + @OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE P SET PARTIDA3 = COALESCE(STUFF(PARTIDA3, LEN(@UON1 + @SEPARADOR) + 1, LEN(@OLD), @NEW) , PARTIDA3)" & vbCrLf
sConsulta = sConsulta & "FROM PM_CONTR_FILTRO_USU_BUSQUEDA P WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "P.PARTIDA3 IS NOT NULL AND" & vbCrLf
sConsulta = sConsulta & "SUBSTRING(SUBSTRING(P.PARTIDA3, 1, LEN(P.PARTIDA3) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA3))),  --QUITAMOS LA PARTIDA PRES" & vbCrLf
sConsulta = sConsulta & "         1, " & vbCrLf
sConsulta = sConsulta & "         CASE WHEN " & vbCrLf
sConsulta = sConsulta & "           CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                     SUBSTRING(P.PARTIDA3, 1, LEN(P.PARTIDA3) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA3))), " & vbCrLf
sConsulta = sConsulta & "                     CHARINDEX(@SEPARADOR,P.PARTIDA3, LEN(@UON1 + @SEPARADOR) + 1)" & vbCrLf
sConsulta = sConsulta & "                     )=0 THEN " & vbCrLf
sConsulta = sConsulta & "               LEN(SUBSTRING(P.PARTIDA3, 1, LEN(P.PARTIDA3) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA3))))" & vbCrLf
sConsulta = sConsulta & "           ELSE " & vbCrLf
sConsulta = sConsulta & "               CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                         SUBSTRING(P.PARTIDA3, 1, LEN(P.PARTIDA3) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA3))), " & vbCrLf
sConsulta = sConsulta & "                         CHARINDEX(@SEPARADOR,P.PARTIDA3, LEN(@UON1 + @SEPARADOR) + 1))-1" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "       )=@UON1 + @SEPARADOR + @OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE P SET PARTIDA4 = COALESCE(STUFF(PARTIDA4, LEN(@UON1 + @SEPARADOR) + 1, LEN(@OLD), @NEW) , PARTIDA4)" & vbCrLf
sConsulta = sConsulta & "FROM PM_CONTR_FILTRO_USU_BUSQUEDA P WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "P.PARTIDA4 IS NOT NULL AND" & vbCrLf
sConsulta = sConsulta & "SUBSTRING(SUBSTRING(P.PARTIDA4, 1, LEN(P.PARTIDA4) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA4))),  --QUITAMOS LA PARTIDA PRES" & vbCrLf
sConsulta = sConsulta & "         1, " & vbCrLf
sConsulta = sConsulta & "         CASE WHEN " & vbCrLf
sConsulta = sConsulta & "           CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                     SUBSTRING(P.PARTIDA4, 1, LEN(P.PARTIDA4) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA4))), " & vbCrLf
sConsulta = sConsulta & "                     CHARINDEX(@SEPARADOR,P.PARTIDA4, LEN(@UON1 + @SEPARADOR) + 1)" & vbCrLf
sConsulta = sConsulta & "                     )=0 THEN " & vbCrLf
sConsulta = sConsulta & "               LEN(SUBSTRING(P.PARTIDA4, 1, LEN(P.PARTIDA4) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA4))))" & vbCrLf
sConsulta = sConsulta & "           ELSE " & vbCrLf
sConsulta = sConsulta & "               CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                         SUBSTRING(P.PARTIDA4, 1, LEN(P.PARTIDA4) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA4))), " & vbCrLf
sConsulta = sConsulta & "                         CHARINDEX(@SEPARADOR,P.PARTIDA4, LEN(@UON1 + @SEPARADOR) + 1))-1" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "       )=@UON1 + @SEPARADOR + @OLD" & vbCrLf
sConsulta = sConsulta & "--FIN ACTUALIZACION COPIA_CAMPO, COPIA_LINEA_DESGLOSE, INSTANCIA_CENTROS_PARTIDAS, PM_CONTR_FILTRO_USU_BUSQUEDA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON2_DEP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3_DEP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PER CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON2_DEST CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3_DEST CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISOR CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CERTIFICADO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE NOCONFORMIDAD CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE APROBADORES CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PETICIONARIOS CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_ROL CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_ENLACE_CONDICIONES CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_COPIA_ROL CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_COPIA_ENLACE_CONDICION CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_UON CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU_CC_CONTROL CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU_CC_IMPUTACION CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES5_UON CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CENTRO_SM_UON CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON4_DEST CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_PED_IMPUTACION CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PERF_UON CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_CAT_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_CAT_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_CAT_UON2_DEP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_CAT_UON3_DEP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_GRUPO_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_GRUPO_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_GRUPO_UON2_DEP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_GRUPO_UON3_DEP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_MEN_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_MEN_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_MEN_UON2_DEP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_MEN_UON3_DEP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CUSTOM_TEMPLATE_UON CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "CLOSE C" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "COMMIT TRANSACTION" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT OFF" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UON1_COD]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[UON1_COD]" & vbCrLf
    gRDOCon.QueryTimeout = 7200
    ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE  PROCEDURE UON1_COD (@OLD VARCHAR(50),@NEW VARCHAR(50)) AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT ON" & vbCrLf
sConsulta = sConsulta & "BEGIN TRANSACTION" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR FOR SELECT COD FROM UON1 WHERE COD=@OLD" & vbCrLf
sConsulta = sConsulta & "OPEN C" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C INTO @VAR_COD" & vbCrLf
sConsulta = sConsulta & "IF @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON1_DEP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON2_DEP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3_DEP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PER NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON1_DEST NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON2_DEST NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3_DEST NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISOR NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CERTIFICADO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE NOCONFORMIDAD NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE APROBADORES NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PETICIONARIOS NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_ROL NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_ENLACE_CONDICIONES NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_COPIA_ROL NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_COPIA_ENLACE_CONDICION NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_UON NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU_CC_CONTROL NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU_CC_IMPUTACION NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES5_UON NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CENTRO_SM_UON NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON4_DEST NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_PED_IMPUTACION NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PERF_UON NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_CAT_UON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_CAT_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_CAT_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_CAT_UON1_DEP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_CAT_UON2_DEP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_CAT_UON3_DEP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_GRUPO_UON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_GRUPO_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_GRUPO_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_MEN_UON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_MEN_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_MEN_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_MEN_UON1_DEP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_MEN_UON2_DEP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_MEN_UON3_DEP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CUSTOM_TEMPLATE_UON NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE UON1 SET COD=@NEW WHERE COD=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON2 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON3 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON4 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON1_DEP SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON2_DEP SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON3_DEP SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PER SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE COM SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_UON1 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_UON2 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_UON3 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_UON1 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_UON2 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_UON3 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON1_DEST SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON2_DEST SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON3_DEST SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_UON1 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_UON2 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_UON3 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_UON1 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_UON2 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_UON3 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES1_NIV1 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES1_NIV2 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES1_NIV3 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES1_NIV4 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES2_NIV1 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES2_NIV2 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES2_NIV3 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES2_NIV4 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES3_NIV1 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES3_NIV2 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES3_NIV3 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES3_NIV4 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES4_NIV1 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES4_NIV2 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES4_NIV3 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES4_NIV4 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_1 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_2 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_3 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_4 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_1 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_2 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_3 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_4 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON1 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON2 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON3 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON4 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY1 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY2 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY3 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY4 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_PRESCON3 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_PRESCON4 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISOR SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CERTIFICADO SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE NOCONFORMIDAD SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE APROBADORES SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PETICIONARIOS SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PM_ROL SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PM_COPIA_ROL SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4_UON SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET UON1_APER=@NEW WHERE UON1_APER=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE USU_CC_CONTROL   SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE USU_CC_IMPUTACION SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES5_UON SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CENTRO_SM_UON SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON4_DEST SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE LINEAS_PED_IMPUTACION SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PERF_UON SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_CAT_UON1 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_CAT_UON2 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_CAT_UON3 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_CAT_UON1_DEP SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_CAT_UON2_DEP SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_CAT_UON3_DEP SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_GRUPO_UON1 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_GRUPO_UON2 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_GRUPO_UON3 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_MEN_UON1 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_MEN_UON2 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_MEN_UON3 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_MEN_UON1_DEP SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_MEN_UON2_DEP SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_MEN_UON3_DEP SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CUSTOM_TEMPLATE_UON SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @LEN_COD_UON1 AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE C2 CURSOR FOR SELECT LONGITUD FROM DIC WHERE NOMBRE='UON1'" & vbCrLf
sConsulta = sConsulta & "OPEN C2" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C2 INTO @LEN_COD_UON1" & vbCrLf
sConsulta = sConsulta & "IF @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "  UPDATE PM_ENLACE_CONDICIONES SET VALOR_TEXT=STUFF(VALOR_TEXT,1,@LEN_COD_UON1,@NEW + SPACE(@LEN_COD_UON1 - LEN(@NEW))) WHERE TIPO_CAMPO=4 AND TIPO_VALOR=2 AND LTRIM(RTRIM(SUBSTRING(VALOR_TEXT,1,@LEN_COD_UON1)))=@OLD" & vbCrLf
sConsulta = sConsulta & "  UPDATE PM_COPIA_ENLACE_CONDICION SET VALOR_TEXT=STUFF(VALOR_TEXT,1,@LEN_COD_UON1,@NEW + SPACE(@LEN_COD_UON1 - LEN(@NEW))) WHERE TIPO_CAMPO=4 AND TIPO_VALOR=2 AND LTRIM(RTRIM(SUBSTRING(VALOR_TEXT,1,@LEN_COD_UON1)))=@OLD" & vbCrLf
sConsulta = sConsulta & "CLOSE C2" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C2" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--INICIO ACTUALIZACION COPIA_CAMPO, COPIA_LINEA_DESGLOSE, INSTANCIA_CENTROS_PARTIDAS, PM_CONTR_FILTRO_USU_BUSQUEDA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--ACTUALIZAR DATOS DE UONS EN LOS CENTROS Y PARTIDAS GRABADOS EN " & vbCrLf
sConsulta = sConsulta & "   --COPIA_CAMPO" & vbCrLf
sConsulta = sConsulta & "   --COPIA_LINEA_DESGLOSE" & vbCrLf
sConsulta = sConsulta & "   --INSTANCIA_CENTROS_DESGLOSE --> TABLA EN LA QUE SE HACE COPIA DE LOS DATOS DE CENTROS Y PARTIDAS DE COPIA_CAMPO Y COPIA_LINEA_DESGLOSE PARA ACELERAR FILTRADOS EN EL BUSCADOR DEL VISOR DE CONTRATOS" & vbCrLf
sConsulta = sConsulta & "   --PM_CONTR_FILTRO_USU_BUSQUEDA --> TABLA EN LA QUE SE HACE COPIA DE LOS VALORES POR DEFECTO PARA LOS FILTROS DEL BUSCADOR DEL VISOR DE CONTRATOS, QUE INCLUYE, ENTRE OTRAS COSAS CENTROS Y PARTIDAS." & vbCrLf
sConsulta = sConsulta & "--HAY QUE MODIFICARLO PORQUE EL MODO EN QUE SE GRABAN CENTROS Y PARTIDAS ES:" & vbCrLf
sConsulta = sConsulta & "--CENTROS: UON1#UON2#... HASTA EL NIVEL AL QUE EST� VINCULADO EL CENTRO" & vbCrLf
sConsulta = sConsulta & "--PARTIDAS: UON1#UON2#...#PRESN: UONS HASTA EL NIVEL AL QUE EST� VINCULADA LA PARTIDA Y PRESN, DONDE N ES EL NIVEL DE IMPUTACION QUE SE INDICA EN PARGEN_SM.IMP_NIVEL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- QU� ES MEJOR PARA BUSCAR LAS UON EN LOS CAMPOS, GUIARSE POR LA LONGITUD DEL C�DIGO UON O POR EL SEPARADOR #: " & vbCrLf
sConsulta = sConsulta & "-- DADO QUE SE PUEDE CAMBIAR LA LONGITUD EN LA TABLA DIC Y QUE EN LAS 4 TABLAS AL GUARDAR LOS DATOS NO SE TIENE EN CUENTA ESA LONGITUD, USAREMOS #" & vbCrLf
sConsulta = sConsulta & "-- COPIA_CAMPO" & vbCrLf
sConsulta = sConsulta & "DECLARE @SEPARADOR VARCHAR(3) = '#'" & vbCrLf
sConsulta = sConsulta & "UPDATE CC SET VALOR_TEXT = STUFF(CC.VALOR_TEXT,1,LEN(@OLD),@NEW) " & vbCrLf
sConsulta = sConsulta & "FROM COPIA_CAMPO CC WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_CAMPO_DEF CD WITH(NOLOCK) ON CC.COPIA_CAMPO_DEF=CD.ID" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "   CD.TIPO_CAMPO_GS IN (129,130) AND " & vbCrLf
sConsulta = sConsulta & "   CC.VALOR_TEXT IS NOT NULL AND " & vbCrLf
sConsulta = sConsulta & "   (LTRIM(RTRIM(SUBSTRING(CC.VALOR_TEXT,1,CASE WHEN (CHARINDEX (@SEPARADOR,CC.VALOR_TEXT))=0 THEN 0 ELSE (CHARINDEX (@SEPARADOR,CC.VALOR_TEXT)-1) END)))=@OLD" & vbCrLf
sConsulta = sConsulta & "   OR" & vbCrLf
sConsulta = sConsulta & "   CC.VALOR_TEXT = @OLD) --CUANDO SE IMPUTE UN CENTRO A UNA UON1, EL VALOR_TEXT SER� EXACTAMENTE EL UON1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- COPIA_LINEA_DESGLOSE" & vbCrLf
sConsulta = sConsulta & "UPDATE CLD SET VALOR_TEXT = STUFF(CLD.VALOR_TEXT,1,LEN(@OLD),@NEW) " & vbCrLf
sConsulta = sConsulta & "FROM COPIA_LINEA_DESGLOSE CLD WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_CAMPO CC WITH (NOLOCK) ON CLD.CAMPO_HIJO = CC.ID " & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_CAMPO_DEF CCD WITH (NOLOCK) ON CC.COPIA_CAMPO_DEF = CCD.ID" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "   CCD.TIPO_CAMPO_GS IN (129,130) AND " & vbCrLf
sConsulta = sConsulta & "   CLD.VALOR_TEXT IS NOT NULL AND " & vbCrLf
sConsulta = sConsulta & "   (LTRIM(RTRIM(SUBSTRING(CLD.VALOR_TEXT,1,CASE WHEN (CHARINDEX (@SEPARADOR,CLD.VALOR_TEXT))=0 THEN 0 ELSE (CHARINDEX (@SEPARADOR,CLD.VALOR_TEXT)-1) END)))=@OLD" & vbCrLf
sConsulta = sConsulta & "   OR" & vbCrLf
sConsulta = sConsulta & "   CLD.VALOR_TEXT = @OLD)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- INSTANCIA_CENTROS_PARTIDAS" & vbCrLf
sConsulta = sConsulta & "UPDATE ICP SET VALOR_TEXT = STUFF(ICP.VALOR_TEXT,1,LEN(@OLD),@NEW) " & vbCrLf
sConsulta = sConsulta & "FROM INSTANCIA_CENTROS_PARTIDAS ICP WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "ICP.TIPO_CAMPO_GS IN (129, 130) AND " & vbCrLf
sConsulta = sConsulta & "ICP.VALOR_TEXT IS NOT NULL AND" & vbCrLf
sConsulta = sConsulta & "(LTRIM(RTRIM(SUBSTRING(ICP.VALOR_TEXT,1,CASE WHEN (CHARINDEX (@SEPARADOR,ICP.VALOR_TEXT))=0 THEN 0 ELSE (CHARINDEX (@SEPARADOR,ICP.VALOR_TEXT)-1) END)))=@OLD" & vbCrLf
sConsulta = sConsulta & "OR" & vbCrLf
sConsulta = sConsulta & "ICP.VALOR_TEXT = @OLD) --CUANDO SE IMPUTE UN CENTRO A UNA UON1, EL VALOR_TEXT SER� EXACTAMENTE EL UON1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- PM_CONTR_FILTRO_USU_BUSQUEDA" & vbCrLf
sConsulta = sConsulta & "UPDATE P SET CENTRO_COSTE = STUFF(P.CENTRO_COSTE,1,LEN(@OLD),@NEW) " & vbCrLf
sConsulta = sConsulta & "FROM PM_CONTR_FILTRO_USU_BUSQUEDA P WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "P.CENTRO_COSTE IS NOT NULL AND" & vbCrLf
sConsulta = sConsulta & "(LTRIM(RTRIM(SUBSTRING(P.CENTRO_COSTE,1,CASE WHEN (CHARINDEX (@SEPARADOR,P.CENTRO_COSTE))=0 THEN 0 ELSE (CHARINDEX (@SEPARADOR,P.CENTRO_COSTE)-1) END)))=@OLD" & vbCrLf
sConsulta = sConsulta & "OR" & vbCrLf
sConsulta = sConsulta & "P.CENTRO_COSTE = @OLD) --CUANDO SE IMPUTE UN CENTRO A UNA UON1, EL CENTRO_COSTE SER� EXACTAMENTE EL UON1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE P SET PARTIDA1 = STUFF(P.PARTIDA1,1,LEN(@OLD),@NEW) " & vbCrLf
sConsulta = sConsulta & "FROM PM_CONTR_FILTRO_USU_BUSQUEDA P WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "P.PARTIDA1 IS NOT NULL AND" & vbCrLf
sConsulta = sConsulta & "LTRIM(RTRIM(SUBSTRING(P.PARTIDA1,1,CASE WHEN (CHARINDEX (@SEPARADOR,P.PARTIDA1))=0 THEN 0 ELSE (CHARINDEX (@SEPARADOR,P.PARTIDA1)-1) END)))=@OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE P SET PARTIDA2 = STUFF(P.PARTIDA2,1,LEN(@OLD),@NEW) " & vbCrLf
sConsulta = sConsulta & "FROM PM_CONTR_FILTRO_USU_BUSQUEDA P WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "P.PARTIDA2 IS NOT NULL AND" & vbCrLf
sConsulta = sConsulta & "LTRIM(RTRIM(SUBSTRING(P.PARTIDA2,1,CASE WHEN (CHARINDEX (@SEPARADOR,P.PARTIDA2))=0 THEN 0 ELSE (CHARINDEX (@SEPARADOR,P.PARTIDA2)-1) END)))=@OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE P SET PARTIDA3 = STUFF(P.PARTIDA3,1,LEN(@OLD),@NEW) " & vbCrLf
sConsulta = sConsulta & "FROM PM_CONTR_FILTRO_USU_BUSQUEDA P WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "P.PARTIDA3 IS NOT NULL AND" & vbCrLf
sConsulta = sConsulta & "LTRIM(RTRIM(SUBSTRING(P.PARTIDA3,1,CASE WHEN (CHARINDEX (@SEPARADOR,P.PARTIDA3))=0 THEN 0 ELSE (CHARINDEX (@SEPARADOR,P.PARTIDA3)-1) END)))=@OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE P SET PARTIDA4 = STUFF(P.PARTIDA4,1,LEN(@OLD),@NEW) " & vbCrLf
sConsulta = sConsulta & "FROM PM_CONTR_FILTRO_USU_BUSQUEDA P WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "P.PARTIDA4 IS NOT NULL AND" & vbCrLf
sConsulta = sConsulta & "LTRIM(RTRIM(SUBSTRING(P.PARTIDA4,1,CASE WHEN (CHARINDEX (@SEPARADOR,P.PARTIDA4))=0 THEN 0 ELSE (CHARINDEX (@SEPARADOR,P.PARTIDA4)-1) END)))=@OLD" & vbCrLf
sConsulta = sConsulta & "--FIN ACTUALIZACION COPIA_CAMPO, COPIA_LINEA_DESGLOSE, INSTANCIA_CENTROS_PARTIDAS, PM_CONTR_FILTRO_USU_BUSQUEDA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON1_DEP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON2_DEP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3_DEP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PER CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON1_DEST CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON2_DEST CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3_DEST CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISOR CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CERTIFICADO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE NOCONFORMIDAD CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE APROBADORES CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PETICIONARIOS CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_ROL CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_ENLACE_CONDICIONES CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_COPIA_ROL CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_COPIA_ENLACE_CONDICION CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_UON CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU_CC_CONTROL CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU_CC_IMPUTACION CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES5_UON CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CENTRO_SM_UON CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON4_DEST CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_PED_IMPUTACION CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PERF_UON CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_CAT_UON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_CAT_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_CAT_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_CAT_UON1_DEP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_CAT_UON2_DEP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_CAT_UON3_DEP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_GRUPO_UON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_GRUPO_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_GRUPO_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_MEN_UON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_MEN_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_MEN_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_MEN_UON1_DEP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_MEN_UON2_DEP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_MEN_UON3_DEP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CUSTOM_TEMPLATE_UON CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "CLOSE C" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "COMMIT TRANSACTION" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT OFF" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


End Sub


Public Function CodigoDeActualizacion31900_08_2003_A31900_08_2004() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim oADOConnection As ADODB.Connection
    Dim sConsulta As String

    On Error GoTo Error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
                  
    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Actualiza Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    frmProgreso.ProgressBar1.Refresh
    V_31900_8_Storeds_204
    
    sConsulta = "UPDATE VERSION SET NUM ='3.00.20.004'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103), PRODVER='31900.8'"
    ExecuteSQL gRDOCon, sConsulta
        
    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False
        
    CodigoDeActualizacion31900_08_2003_A31900_08_2004 = True
    Exit Function

Error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    CodigoDeActualizacion31900_08_2003_A31900_08_2004 = False
End Function

Public Sub V_31900_8_Storeds_204()
    Dim sConsulta As String
            
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_ROL_PETICIONARIOS]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_ROL_PETICIONARIOS]" & vbCrLf
    gRDOCon.QueryTimeout = 7200
    ExecuteSQL gRDOCon, sConsulta
    
    
    
    sConsulta = "CREATE PROCEDURE [dbo].[FSPM_ROL_PETICIONARIOS] @USU VARCHAR(50) = NULL,@FILTRO INT = NULL,@MULTISELECCION INT = 0, @ESCONTRATO TINYINT=0 AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "SET @SQL=''" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @MULTISELECCION=0 " & vbCrLf
sConsulta = sConsulta & "    SET @SQL =@SQL + 'SELECT NULL AS COD, NULL AS DEN UNION '" & vbCrLf
sConsulta = sConsulta & "      " & vbCrLf
sConsulta = sConsulta & "IF @FILTRO IS NULL" & vbCrLf
sConsulta = sConsulta & " BEGIN" & vbCrLf
sConsulta = sConsulta & "   IF @ESCONTRATO = 1" & vbCrLf
sConsulta = sConsulta & "       SET @SQL =@SQL + ' SELECT DISTINCT P.COD, P.NOM + '' '' + P.APE AS DEN '" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "       SET @SQL =@SQL + ' SELECT DISTINCT TOP 900 P.COD, P.NOM + '' '' + P.APE AS DEN '" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "   SET @SQL =@SQL + ' FROM PER P WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN VIEW_ROL_PETICIONARIO R WITH(NOLOCK) ON P.COD = R.COD" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN SOLICITUD S WITH(NOLOCK) ON R.SOLICITUD = S.ID" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN FORMULARIO F WITH(NOLOCK) ON S.FORMULARIO = F.ID" & vbCrLf
sConsulta = sConsulta & "   WHERE P.BAJALOG= 0 AND F.ID IN (SELECT DISTINCT F.ID" & vbCrLf
sConsulta = sConsulta & "   FROM SOLICITUD S WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN TIPO_SOLICITUDES TS WITH (NOLOCK) ON S.TIPO=TS.ID" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN FORMULARIO F WITH (NOLOCK) ON S.FORMULARIO = F.ID" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PM_ROL R WITH (NOLOCK) ON R.WORKFLOW=S.WORKFLOW AND (PER=@USU OR SUSTITUTO=@USU)" & vbCrLf
sConsulta = sConsulta & "   WHERE TS.TIPO NOT IN (2,3)" & vbCrLf
sConsulta = sConsulta & "   UNION" & vbCrLf
sConsulta = sConsulta & "   SELECT DISTINCT F.ID" & vbCrLf
sConsulta = sConsulta & "   FROM SOLICITUD S WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN TIPO_SOLICITUDES TS WITH (NOLOCK) ON S.TIPO=TS.ID" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN FORMULARIO F WITH (NOLOCK) ON S.FORMULARIO = F.ID" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN VIEW_ROL_PETICIONARIO R WITH (NOLOCK) ON S.ID = R.SOLICITUD AND R.COD=@USU" & vbCrLf
sConsulta = sConsulta & "   WHERE TS.TIPO NOT IN (2,3))" & vbCrLf
sConsulta = sConsulta & "   ORDER BY DEN'" & vbCrLf
sConsulta = sConsulta & " END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "   IF @ESCONTRATO = 1" & vbCrLf
sConsulta = sConsulta & "       SET @SQL =@SQL + ' SELECT DISTINCT P.COD, P.NOM + '' '' + P.APE AS DEN'" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "       SET @SQL =@SQL + ' SELECT DISTINCT TOP 900 P.COD, P.NOM + '' '' + P.APE AS DEN'" & vbCrLf
sConsulta = sConsulta & "         " & vbCrLf
sConsulta = sConsulta & "     SET @SQL =@SQL + ' FROM PER P WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PM_ROL R WITH(NOLOCK) ON P.COD = R.PER" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN WORKFLOW W WITH(NOLOCK) ON R.WORKFLOW = W.ID" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN SOLICITUD S WITH(NOLOCK) ON W.ID = S.WORKFLOW" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN FORMULARIO F WITH(NOLOCK) ON S.FORMULARIO = F.ID" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PM_FILTROS PF WITH(NOLOCK) ON F.ID=PF.FORMULARIO" & vbCrLf
sConsulta = sConsulta & "           WHERE R.TIPO = 4 AND P.BAJALOG= 0 AND PF.ID = @FILTRO" & vbCrLf
sConsulta = sConsulta & "           UNION" & vbCrLf
sConsulta = sConsulta & "           SELECT DISTINCT P.COD, P.NOM + '' '' + P.APE AS DEN FROM PER P WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN VIEW_ROL_PETICIONARIO R WITH(NOLOCK) ON P.COD = R.COD" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN SOLICITUD S WITH(NOLOCK) ON R.SOLICITUD = S.ID" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN FORMULARIO F WITH(NOLOCK) ON S.FORMULARIO = F.ID" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PM_FILTROS PF WITH(NOLOCK) ON F.ID=PF.FORMULARIO" & vbCrLf
sConsulta = sConsulta & "           WHERE P.BAJALOG= 0 AND PF.ID = @FILTRO" & vbCrLf
sConsulta = sConsulta & "           ORDER BY DEN'" & vbCrLf
sConsulta = sConsulta & "     END  " & vbCrLf
sConsulta = sConsulta & "     " & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@USU VARCHAR(50), @FILTRO INT', @USU=@USU, @FILTRO=@FILTRO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

            
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSGSW_DEVOLVER_PROVEEDORESADJUDICADOS]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSGSW_DEVOLVER_PROVEEDORESADJUDICADOS]" & vbCrLf
    gRDOCon.QueryTimeout = 7200
    ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSGSW_DEVOLVER_PROVEEDORESADJUDICADOS] @IDI NVARCHAR(5),@IDEMP INT,  @ANYO INT, @GMN1 VARCHAR(50), @PROCE VARCHAR(50) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @ESTADO INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @ESTADO=EST FROM PROCE WITH (NOLOCK) WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ESTADO=11 --Parcialmente adjudicado, se buscaran aquellos items que esten adjudicados(I.EST=1)" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       SELECT DISTINCT P.ANYO,P.GMN1,P.COD, PROVE.COD PROVECOD,PROVE.DEN PROVEDEN,LTRIM(RTRIM(PE.COD_ERP)) COD_ERP,LTRIM(RTRIM(PE.COD_ERP)) + ' - ' + PE.DEN_ERP DEN_ERP,PE.EMPRESA ,PROVE.NIF" & vbCrLf
sConsulta = sConsulta & "       ,PE.CAMPO1,PE.CAMPO2,PE.CAMPO3,PE.CAMPO4,PG.ACTCAMPO1,PG.ACTCAMPO2,PG.ACTCAMPO3,PG.ACTCAMPO4,ISNULL(PR.NUM_DISTR,0) NUM_DISTR,1 EST_INTEGRACION " & vbCrLf
sConsulta = sConsulta & "       FROM ITEM_ADJ IA WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN " & vbCrLf
sConsulta = sConsulta & "       (select distinct iu.item,iu.uon1, null uon2,null uon3,u.empresa " & vbCrLf
sConsulta = sConsulta & "       from item_uon1 iu WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "       inner join uon1 u WITH(NOLOCK) on iu.uon1 = u.cod  where IU.ANYO=@ANYO AND IU.GMN1=@GMN1 AND IU.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "       Union " & vbCrLf
sConsulta = sConsulta & "       select distinct iu.item,iu.uon1, IU.uon2 ,null uon3,ISNULL(u1.empresa,u2.empresa) as empresa " & vbCrLf
sConsulta = sConsulta & "       from item_uon2 iu WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "       inner join uon2 u2 WITH(NOLOCK) on iu.uon1 = u2.uon1 and iu.uon2=u2.cod " & vbCrLf
sConsulta = sConsulta & "       inner join uon1 u1 WITH(NOLOCK) on u1.cod = u2.uon1  where IU.ANYO=@ANYO AND IU.GMN1=@GMN1 AND IU.PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "       Union select distinct iu.item,iu.uon1, IU.uon2 ,IU.uon3,ISNULL(u3.empresa,ISNULL(u2.empresa,u1.empresa)) as empresa " & vbCrLf
sConsulta = sConsulta & "       from item_uon3 iu WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "       inner join uon3 u3 WITH(NOLOCK) on iu.uon1 = u3.uon1 and iu.uon2=u3.uon2 and iu.uon3=u3.cod " & vbCrLf
sConsulta = sConsulta & "       inner join uon2 u2 WITH(NOLOCK) on u3.uon1 = u2.uon1 and u3.uon2=u2.cod " & vbCrLf
sConsulta = sConsulta & "       inner join uon1 u1 WITH(NOLOCK) on u1.cod = u3.uon1  where IU.ANYO=@ANYO AND IU.GMN1=@GMN1 AND IU.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "       ) IT_EMP ON IT_EMP.ITEM=IA.ITEM AND IT_EMP.EMPRESA =@IDEMP " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN ITEM I WITH (NOLOCK) ON IA.ANYO=I.ANYO AND IA.GMN1=I.GMN1_PROCE AND IA.PROCE =I.PROCE AND IA.ITEM=I.ID AND I.EST=1" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PROCE P WITH (NOLOCK) ON IA.ANYO=P.ANYO AND IA.GMN1=P.GMN1 AND IA.PROCE =P.COD" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PROVE WITH (NOLOCK) ON PROVE.COD=IA.PROVE" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN (SELECT COUNT(PROVES) AS NUM_DISTR, PROVEP FROM PROVE_REL PR WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN GS_PROVETREL GP WITH (NOLOCK) ON PR.PROVETREL=GP.ID WHERE GP.PEDIDO=1 GROUP BY PR.PROVEP) AS PR ON PROVE.COD=PR.PROVEP  " & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN PROVE_ERP PE WITH (NOLOCK) ON PE.EMPRESA=@IDEMP AND PE.NIF=PROVE.NIF AND PE.BAJALOG=0  " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PARGEN_GEST PG WITH (NOLOCK)  ON 1=1" & vbCrLf
sConsulta = sConsulta & "       WHERE IA.ANYO=@ANYO AND IA.GMN1=@GMN1 AND IA.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       SELECT DISTINCT P.ANYO,P.GMN1,P.COD, PROVE.COD PROVECOD,PROVE.DEN PROVEDEN,LTRIM(RTRIM(PE.COD_ERP)) COD_ERP,LTRIM(RTRIM(PE.COD_ERP)) + ' - ' + PE.DEN_ERP DEN_ERP,PE.EMPRESA ,PROVE.NIF" & vbCrLf
sConsulta = sConsulta & "       ,PE.CAMPO1,PE.CAMPO2,PE.CAMPO3,PE.CAMPO4,PG.ACTCAMPO1,PG.ACTCAMPO2,PG.ACTCAMPO3,PG.ACTCAMPO4,ISNULL(PR.NUM_DISTR,0) NUM_DISTR,1 EST_INTEGRACION  " & vbCrLf
sConsulta = sConsulta & "       FROM ITEM_ADJ IA WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "          INNER JOIN " & vbCrLf
sConsulta = sConsulta & "       (select distinct iu.item,iu.uon1, null uon2,null uon3,u.empresa " & vbCrLf
sConsulta = sConsulta & "       from item_uon1 iu WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "       inner join uon1 u WITH(NOLOCK) on iu.uon1 = u.cod  where IU.ANYO=@ANYO AND IU.GMN1=@GMN1 AND IU.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "       Union " & vbCrLf
sConsulta = sConsulta & "       select distinct iu.item,iu.uon1, IU.uon2 ,null uon3,ISNULL(u1.empresa,u2.empresa) as empresa " & vbCrLf
sConsulta = sConsulta & "       from item_uon2 iu WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "       inner join uon2 u2 WITH(NOLOCK) on iu.uon1 = u2.uon1 and iu.uon2=u2.cod " & vbCrLf
sConsulta = sConsulta & "       inner join uon1 u1 WITH(NOLOCK) on u1.cod = u2.uon1  where IU.ANYO=@ANYO AND IU.GMN1=@GMN1 AND IU.PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "       Union select distinct iu.item,iu.uon1, IU.uon2 ,IU.uon3,ISNULL(u3.empresa,ISNULL(u2.empresa,u1.empresa)) as empresa " & vbCrLf
sConsulta = sConsulta & "       from item_uon3 iu WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "       inner join uon3 u3 WITH(NOLOCK) on iu.uon1 = u3.uon1 and iu.uon2=u3.uon2 and iu.uon3=u3.cod " & vbCrLf
sConsulta = sConsulta & "       inner join uon2 u2 WITH(NOLOCK) on u3.uon1 = u2.uon1 and u3.uon2=u2.cod " & vbCrLf
sConsulta = sConsulta & "       inner join uon1 u1 WITH(NOLOCK) on u1.cod = u3.uon1  where IU.ANYO=@ANYO AND IU.GMN1=@GMN1 AND IU.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "       ) IT_EMP ON IT_EMP.ITEM=IA.ITEM AND IT_EMP.EMPRESA =@IDEMP " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN ITEM I WITH (NOLOCK) ON IA.ANYO=I.ANYO AND IA.GMN1=I.GMN1_PROCE AND IA.PROCE =I.PROCE AND IA.ITEM=I.ID" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PROCE P WITH (NOLOCK) ON IA.ANYO=P.ANYO AND IA.GMN1=P.GMN1 AND IA.PROCE =P.COD" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PROVE WITH (NOLOCK) ON PROVE.COD=IA.PROVE" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN (SELECT COUNT(PROVES) AS NUM_DISTR, PROVEP FROM PROVE_REL PR WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN GS_PROVETREL GP WITH (NOLOCK) ON PR.PROVETREL=GP.ID WHERE GP.PEDIDO=1 GROUP BY PR.PROVEP) AS PR ON PROVE.COD=PR.PROVEP " & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN PROVE_ERP PE WITH (NOLOCK) ON PE.EMPRESA=@IDEMP AND PE.NIF=PROVE.NIF AND PE.BAJALOG=0  " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PARGEN_GEST PG WITH (NOLOCK)  ON 1=1" & vbCrLf
sConsulta = sConsulta & "       WHERE IA.ANYO=@ANYO AND IA.GMN1=@GMN1 AND IA.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT ID,DEN FROM PARGEN_LIT WITH (NOLOCK) WHERE ID BETWEEN 32 AND 35 AND IDI=@IDI" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT ERP.COD + ' - ' + ERP.DEN ERP_ASOCIADO_EMP FROM EMP WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN ERP_SOCIEDAD ES WITH (NOLOCK) ON EMP.SOCIEDAD =ES.SOCIEDAD " & vbCrLf
sConsulta = sConsulta & "INNER JOIN ERP WITH (NOLOCK) ON ES.ERP =ERP.COD " & vbCrLf
sConsulta = sConsulta & "WHERE EMP.ID=@IDEMP" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UON1_COD]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[UON1_COD]" & vbCrLf
    gRDOCon.QueryTimeout = 7200
    ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE  PROCEDURE UON1_COD (@OLD VARCHAR(50),@NEW VARCHAR(50)) AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT ON" & vbCrLf
sConsulta = sConsulta & "BEGIN TRANSACTION" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR FOR SELECT COD FROM UON1 WHERE COD=@OLD" & vbCrLf
sConsulta = sConsulta & "OPEN C" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C INTO @VAR_COD" & vbCrLf
sConsulta = sConsulta & "IF @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON1_DEP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON2_DEP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3_DEP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PER NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON1_DEST NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON2_DEST NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3_DEST NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISOR NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CERTIFICADO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE NOCONFORMIDAD NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE APROBADORES NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PETICIONARIOS NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_ROL NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_ENLACE_CONDICIONES NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_COPIA_ROL NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_COPIA_ENLACE_CONDICION NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_UON NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU_CC_CONTROL NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU_CC_IMPUTACION NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES5_UON NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CENTRO_SM_UON NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON4_DEST NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_PED_IMPUTACION NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PERF_UON NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_CAT_UON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_CAT_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_CAT_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_CAT_UON1_DEP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_CAT_UON2_DEP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_CAT_UON3_DEP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_GRUPO_UON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_GRUPO_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_GRUPO_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_MEN_UON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_MEN_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_MEN_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_MEN_UON1_DEP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_MEN_UON2_DEP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_MEN_UON3_DEP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CUSTOM_TEMPLATE_UON NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE UON1 SET COD=@NEW WHERE COD=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON2 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON3 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON4 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON1_DEP SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON2_DEP SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON3_DEP SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PER SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE COM SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_UON1 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_UON2 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_UON3 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_UON1 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_UON2 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_UON3 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON1_DEST SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON2_DEST SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON3_DEST SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_UON1 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_UON2 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_UON3 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_UON1 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_UON2 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_UON3 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES1_NIV1 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES1_NIV2 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES1_NIV3 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES1_NIV4 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES2_NIV1 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES2_NIV2 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES2_NIV3 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES2_NIV4 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES3_NIV1 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES3_NIV2 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES3_NIV3 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES3_NIV4 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES4_NIV1 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES4_NIV2 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES4_NIV3 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES4_NIV4 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_1 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_2 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_3 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_4 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_1 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_2 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_3 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_4 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON1 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON2 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON3 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON4 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY1 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY2 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY3 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY4 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_PRESCON3 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_PRESCON4 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISOR SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CERTIFICADO SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE NOCONFORMIDAD SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE APROBADORES SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PETICIONARIOS SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PM_ROL SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PM_COPIA_ROL SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4_UON SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET UON1_APER=@NEW WHERE UON1_APER=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE USU_CC_CONTROL   SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE USU_CC_IMPUTACION SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES5_UON SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CENTRO_SM_UON SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON4_DEST SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE LINEAS_PED_IMPUTACION SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PERF_UON SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_CAT_UON1 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_CAT_UON2 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_CAT_UON3 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_CAT_UON1_DEP SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_CAT_UON2_DEP SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_CAT_UON3_DEP SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_GRUPO_UON1 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_GRUPO_UON2 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_GRUPO_UON3 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_MEN_UON1 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_MEN_UON2 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_MEN_UON3 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_MEN_UON1_DEP SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_MEN_UON2_DEP SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_MEN_UON3_DEP SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CUSTOM_TEMPLATE_UON SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @LEN_COD_UON1 AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE C2 CURSOR FOR SELECT LONGITUD FROM DIC WHERE NOMBRE='UON1'" & vbCrLf
sConsulta = sConsulta & "OPEN C2" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C2 INTO @LEN_COD_UON1" & vbCrLf
sConsulta = sConsulta & "IF @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "  UPDATE PM_ENLACE_CONDICIONES SET VALOR_TEXT=STUFF(VALOR_TEXT,1,@LEN_COD_UON1,@NEW + SPACE(@LEN_COD_UON1 - LEN(@NEW))) WHERE TIPO_CAMPO=4 AND TIPO_VALOR=2 AND LTRIM(RTRIM(SUBSTRING(VALOR_TEXT,1,@LEN_COD_UON1)))=@OLD" & vbCrLf
sConsulta = sConsulta & "  UPDATE PM_COPIA_ENLACE_CONDICION SET VALOR_TEXT=STUFF(VALOR_TEXT,1,@LEN_COD_UON1,@NEW + SPACE(@LEN_COD_UON1 - LEN(@NEW))) WHERE TIPO_CAMPO=4 AND TIPO_VALOR=2 AND LTRIM(RTRIM(SUBSTRING(VALOR_TEXT,1,@LEN_COD_UON1)))=@OLD" & vbCrLf
sConsulta = sConsulta & "CLOSE C2" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C2" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--INICIO ACTUALIZACION COPIA_CAMPO, COPIA_LINEA_DESGLOSE, INSTANCIA_CENTROS_PARTIDAS, PM_CONTR_FILTRO_USU_BUSQUEDA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--ACTUALIZAR DATOS DE UONS EN LOS CENTROS Y PARTIDAS GRABADOS EN " & vbCrLf
sConsulta = sConsulta & "   --COPIA_CAMPO" & vbCrLf
sConsulta = sConsulta & "   --COPIA_LINEA_DESGLOSE" & vbCrLf
sConsulta = sConsulta & "   --INSTANCIA_CENTROS_DESGLOSE --> TABLA EN LA QUE SE HACE COPIA DE LOS DATOS DE CENTROS Y PARTIDAS DE COPIA_CAMPO Y COPIA_LINEA_DESGLOSE PARA ACELERAR FILTRADOS EN EL BUSCADOR DEL VISOR DE CONTRATOS" & vbCrLf
sConsulta = sConsulta & "   --PM_CONTR_FILTRO_USU_BUSQUEDA --> TABLA EN LA QUE SE HACE COPIA DE LOS VALORES POR DEFECTO PARA LOS FILTROS DEL BUSCADOR DEL VISOR DE CONTRATOS, QUE INCLUYE, ENTRE OTRAS COSAS CENTROS Y PARTIDAS." & vbCrLf
sConsulta = sConsulta & "--HAY QUE MODIFICARLO PORQUE EL MODO EN QUE SE GRABAN CENTROS Y PARTIDAS ES:" & vbCrLf
sConsulta = sConsulta & "--CENTROS: UON1#UON2#... HASTA EL NIVEL AL QUE EST� VINCULADO EL CENTRO" & vbCrLf
sConsulta = sConsulta & "--PARTIDAS: UON1#UON2#...#PRESN: UONS HASTA EL NIVEL AL QUE EST� VINCULADA LA PARTIDA Y PRESN, DONDE N ES EL NIVEL DE IMPUTACION QUE SE INDICA EN PARGEN_SM.IMP_NIVEL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- QU� ES MEJOR PARA BUSCAR LAS UON EN LOS CAMPOS, GUIARSE POR LA LONGITUD DEL C�DIGO UON O POR EL SEPARADOR #: " & vbCrLf
sConsulta = sConsulta & "-- DADO QUE SE PUEDE CAMBIAR LA LONGITUD EN LA TABLA DIC Y QUE EN LAS 4 TABLAS AL GUARDAR LOS DATOS NO SE TIENE EN CUENTA ESA LONGITUD, USAREMOS #" & vbCrLf
sConsulta = sConsulta & "-- COPIA_CAMPO" & vbCrLf
sConsulta = sConsulta & "DECLARE @SEPARADOR VARCHAR(3) = '#'" & vbCrLf
sConsulta = sConsulta & "UPDATE CC SET VALOR_TEXT = STUFF(CC.VALOR_TEXT,1,LEN(@OLD),@NEW) " & vbCrLf
sConsulta = sConsulta & "FROM COPIA_CAMPO CC WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_CAMPO_DEF CD WITH(NOLOCK) ON CC.COPIA_CAMPO_DEF=CD.ID" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "   CD.TIPO_CAMPO_GS IN (129,130) AND " & vbCrLf
sConsulta = sConsulta & "   CC.VALOR_TEXT IS NOT NULL AND " & vbCrLf
sConsulta = sConsulta & "   (LTRIM(RTRIM(SUBSTRING(CC.VALOR_TEXT,1,CASE WHEN (CHARINDEX (@SEPARADOR,CC.VALOR_TEXT))=0 THEN 0 ELSE (CHARINDEX (@SEPARADOR,CC.VALOR_TEXT)-1) END)))=@OLD" & vbCrLf
sConsulta = sConsulta & "   OR" & vbCrLf
sConsulta = sConsulta & "   CC.VALOR_TEXT = @OLD) --CUANDO SE IMPUTE UN CENTRO A UNA UON1, EL VALOR_TEXT SER� EXACTAMENTE EL UON1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- COPIA_LINEA_DESGLOSE" & vbCrLf
sConsulta = sConsulta & "UPDATE CLD SET VALOR_TEXT = STUFF(CLD.VALOR_TEXT,1,LEN(@OLD),@NEW) " & vbCrLf
sConsulta = sConsulta & "FROM COPIA_LINEA_DESGLOSE CLD WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_CAMPO CC WITH (NOLOCK) ON CLD.CAMPO_HIJO = CC.ID " & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_CAMPO_DEF CCD WITH (NOLOCK) ON CC.COPIA_CAMPO_DEF = CCD.ID" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "   CCD.TIPO_CAMPO_GS IN (129,130) AND " & vbCrLf
sConsulta = sConsulta & "   CLD.VALOR_TEXT IS NOT NULL AND " & vbCrLf
sConsulta = sConsulta & "   (LTRIM(RTRIM(SUBSTRING(CLD.VALOR_TEXT,1,CASE WHEN (CHARINDEX (@SEPARADOR,CLD.VALOR_TEXT))=0 THEN 0 ELSE (CHARINDEX (@SEPARADOR,CLD.VALOR_TEXT)-1) END)))=@OLD" & vbCrLf
sConsulta = sConsulta & "   OR" & vbCrLf
sConsulta = sConsulta & "   CLD.VALOR_TEXT = @OLD)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- INSTANCIA_CENTROS_PARTIDAS" & vbCrLf
sConsulta = sConsulta & "UPDATE ICP SET VALOR_TEXT = STUFF(ICP.VALOR_TEXT,1,LEN(@OLD),@NEW) " & vbCrLf
sConsulta = sConsulta & "FROM INSTANCIA_CENTROS_PARTIDAS ICP WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "ICP.TIPO_CAMPO_GS IN (129, 130) AND " & vbCrLf
sConsulta = sConsulta & "ICP.VALOR_TEXT IS NOT NULL AND" & vbCrLf
sConsulta = sConsulta & "(LTRIM(RTRIM(SUBSTRING(ICP.VALOR_TEXT,1,CASE WHEN (CHARINDEX (@SEPARADOR,ICP.VALOR_TEXT))=0 THEN 0 ELSE (CHARINDEX (@SEPARADOR,ICP.VALOR_TEXT)-1) END)))=@OLD" & vbCrLf
sConsulta = sConsulta & "OR" & vbCrLf
sConsulta = sConsulta & "ICP.VALOR_TEXT = @OLD) --CUANDO SE IMPUTE UN CENTRO A UNA UON1, EL VALOR_TEXT SER� EXACTAMENTE EL UON1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- PM_CONTR_FILTRO_USU_BUSQUEDA" & vbCrLf
sConsulta = sConsulta & "UPDATE P SET CENTRO_COSTE = STUFF(P.CENTRO_COSTE,1,LEN(@OLD),@NEW) " & vbCrLf
sConsulta = sConsulta & "FROM PM_CONTR_FILTRO_USU_BUSQUEDA P WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "P.CENTRO_COSTE IS NOT NULL AND" & vbCrLf
sConsulta = sConsulta & "(LTRIM(RTRIM(SUBSTRING(P.CENTRO_COSTE,1,CASE WHEN (CHARINDEX (@SEPARADOR,P.CENTRO_COSTE))=0 THEN 0 ELSE (CHARINDEX (@SEPARADOR,P.CENTRO_COSTE)-1) END)))=@OLD" & vbCrLf
sConsulta = sConsulta & "OR" & vbCrLf
sConsulta = sConsulta & "P.CENTRO_COSTE = @OLD) --CUANDO SE IMPUTE UN CENTRO A UNA UON1, EL CENTRO_COSTE SER� EXACTAMENTE EL UON1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE P SET PARTIDA1 = STUFF(P.PARTIDA1,1,LEN(@OLD),@NEW) " & vbCrLf
sConsulta = sConsulta & "FROM PM_CONTR_FILTRO_USU_BUSQUEDA P WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "P.PARTIDA1 IS NOT NULL AND" & vbCrLf
sConsulta = sConsulta & "LTRIM(RTRIM(SUBSTRING(P.PARTIDA1,1,CASE WHEN (CHARINDEX (@SEPARADOR,P.PARTIDA1))=0 THEN 0 ELSE (CHARINDEX (@SEPARADOR,P.PARTIDA1)-1) END)))=@OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE P SET PARTIDA2 = STUFF(P.PARTIDA2,1,LEN(@OLD),@NEW) " & vbCrLf
sConsulta = sConsulta & "FROM PM_CONTR_FILTRO_USU_BUSQUEDA P WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "P.PARTIDA2 IS NOT NULL AND" & vbCrLf
sConsulta = sConsulta & "LTRIM(RTRIM(SUBSTRING(P.PARTIDA2,1,CASE WHEN (CHARINDEX (@SEPARADOR,P.PARTIDA2))=0 THEN 0 ELSE (CHARINDEX (@SEPARADOR,P.PARTIDA2)-1) END)))=@OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE P SET PARTIDA3 = STUFF(P.PARTIDA3,1,LEN(@OLD),@NEW) " & vbCrLf
sConsulta = sConsulta & "FROM PM_CONTR_FILTRO_USU_BUSQUEDA P WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "P.PARTIDA3 IS NOT NULL AND" & vbCrLf
sConsulta = sConsulta & "LTRIM(RTRIM(SUBSTRING(P.PARTIDA3,1,CASE WHEN (CHARINDEX (@SEPARADOR,P.PARTIDA3))=0 THEN 0 ELSE (CHARINDEX (@SEPARADOR,P.PARTIDA3)-1) END)))=@OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE P SET PARTIDA4 = STUFF(P.PARTIDA4,1,LEN(@OLD),@NEW) " & vbCrLf
sConsulta = sConsulta & "FROM PM_CONTR_FILTRO_USU_BUSQUEDA P WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "P.PARTIDA4 IS NOT NULL AND" & vbCrLf
sConsulta = sConsulta & "LTRIM(RTRIM(SUBSTRING(P.PARTIDA4,1,CASE WHEN (CHARINDEX (@SEPARADOR,P.PARTIDA4))=0 THEN 0 ELSE (CHARINDEX (@SEPARADOR,P.PARTIDA4)-1) END)))=@OLD" & vbCrLf
sConsulta = sConsulta & "--FIN ACTUALIZACION COPIA_CAMPO, COPIA_LINEA_DESGLOSE, INSTANCIA_CENTROS_PARTIDAS, PM_CONTR_FILTRO_USU_BUSQUEDA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON1_DEP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON2_DEP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3_DEP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PER CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON1_DEST CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON2_DEST CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3_DEST CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISOR CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CERTIFICADO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE NOCONFORMIDAD CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE APROBADORES CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PETICIONARIOS CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_ROL CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_ENLACE_CONDICIONES CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_COPIA_ROL CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_COPIA_ENLACE_CONDICION CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_UON CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU_CC_CONTROL CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU_CC_IMPUTACION CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES5_UON CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CENTRO_SM_UON CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON4_DEST CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_PED_IMPUTACION CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PERF_UON CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_CAT_UON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_CAT_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_CAT_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_CAT_UON1_DEP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_CAT_UON2_DEP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_CAT_UON3_DEP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_GRUPO_UON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_GRUPO_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_GRUPO_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_MEN_UON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_MEN_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_MEN_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_MEN_UON1_DEP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_MEN_UON2_DEP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_MEN_UON3_DEP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CUSTOM_TEMPLATE_UON CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "CLOSE C" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "COMMIT TRANSACTION" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT OFF" & vbCrLf
ExecuteSQL gRDOCon, sConsulta



    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UON2_COD]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[UON2_COD]" & vbCrLf
    gRDOCon.QueryTimeout = 7200
    ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE  PROCEDURE UON2_COD (@OLD VARCHAR(50),@NEW VARCHAR(50), @UON1 VARCHAR(50))  AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT ON" & vbCrLf
sConsulta = sConsulta & "BEGIN TRANSACTION" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR FOR SELECT COD FROM UON2 WITH(NOLOCK) WHERE UON1=@UON1 AND COD=@OLD" & vbCrLf
sConsulta = sConsulta & "OPEN C" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C INTO @VAR_COD" & vbCrLf
sConsulta = sConsulta & "IF @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON2_DEP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3_DEP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PER NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON2_DEST NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3_DEST NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISOR NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CERTIFICADO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE NOCONFORMIDAD NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE APROBADORES NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PETICIONARIOS NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_ROL NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_ENLACE_CONDICIONES NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_COPIA_ROL NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_COPIA_ENLACE_CONDICION NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_UON NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU_CC_CONTROL NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU_CC_IMPUTACION NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES5_UON NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CENTRO_SM_UON NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON4_DEST NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_PED_IMPUTACION NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PERF_UON NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_CAT_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_CAT_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_CAT_UON2_DEP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_CAT_UON3_DEP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_GRUPO_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_GRUPO_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_GRUPO_UON2_DEP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_GRUPO_UON3_DEP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_MEN_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_MEN_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_MEN_UON2_DEP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_MEN_UON3_DEP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CUSTOM_TEMPLATE_UON NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE UON2 SET COD=@NEW WHERE UON1=@UON1 AND COD=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON3 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON4 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON2_DEP SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON3_DEP SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PER SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE COM SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_UON2 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_UON3 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_UON2 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_UON3 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON2_DEST SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON3_DEST SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_UON2 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_UON3 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_UON2 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_UON3 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_1 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_2 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_3 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_4 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_1 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_2 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_3 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_4 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON1 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON2 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON3 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON4 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY1 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY2 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY3 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY4 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_PRESCON3 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_PRESCON4 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES1_NIV1 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES1_NIV2 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES1_NIV3 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES1_NIV4 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES2_NIV1 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES2_NIV2 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES2_NIV3 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES2_NIV4 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES3_NIV1 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES3_NIV2 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES3_NIV3 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES3_NIV4 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES4_NIV1 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES4_NIV2 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES4_NIV3 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES4_NIV4 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISOR SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CERTIFICADO SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE NOCONFORMIDAD SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE APROBADORES SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PETICIONARIOS SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PM_ROL SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PM_COPIA_ROL SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4_UON SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET UON2_APER=@NEW WHERE UON1_APER=@UON1 AND UON2_APER=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE USU_CC_CONTROL SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE USU_CC_IMPUTACION SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES5_UON SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CENTRO_SM_UON SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON4_DEST SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE LINEAS_PED_IMPUTACION SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PERF_UON SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_CAT_UON2 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_CAT_UON3 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_CAT_UON2_DEP SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_CAT_UON3_DEP SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_GRUPO_UON2 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_GRUPO_UON3 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_GRUPO_UON2_DEP SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_GRUPO_UON3_DEP SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_MEN_UON2 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_MEN_UON3 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_MEN_UON2_DEP SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_MEN_UON3_DEP SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CUSTOM_TEMPLATE_UON SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @LEN_COD_UON1 AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @LEN_COD_UON2 AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE C2 CURSOR FOR SELECT (SELECT LONGITUD FROM DIC WITH(NOLOCK) WHERE NOMBRE='UON1') AS L1,(SELECT LONGITUD FROM DIC WITH(NOLOCK) WHERE NOMBRE='UON2') AS L2" & vbCrLf
sConsulta = sConsulta & "OPEN C2" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C2 INTO @LEN_COD_UON1,@LEN_COD_UON2" & vbCrLf
sConsulta = sConsulta & "IF @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "  UPDATE PM_ENLACE_CONDICIONES SET VALOR_TEXT=STUFF(VALOR_TEXT,@LEN_COD_UON1 + 2,@LEN_COD_UON2,@NEW + SPACE(@LEN_COD_UON2 - LEN(@NEW))) WHERE TIPO_CAMPO=4 AND TIPO_VALOR=2 AND LTRIM(RTRIM(SUBSTRING(VALOR_TEXT,1,@LEN_COD_UON1)))=@UON1 AND LTRIM(RTRIM(SUBSTRING(VALOR_TEXT,@LEN_COD_UON1+2, @LEN_COD_UON2)))=@OLD" & vbCrLf
sConsulta = sConsulta & "  UPDATE PM_COPIA_ENLACE_CONDICION SET VALOR_TEXT=STUFF(VALOR_TEXT,@LEN_COD_UON1 + 2,@LEN_COD_UON2,@NEW + SPACE(@LEN_COD_UON2 - LEN(@NEW))) WHERE TIPO_CAMPO=4 AND TIPO_VALOR=2 AND LTRIM(RTRIM(SUBSTRING(VALOR_TEXT,1,@LEN_COD_UON1)))=@UON1 AND LTRIM(RTRIM(SUBSTRING(VALOR_TEXT,@LEN_COD_UON1+2, @LEN_COD_UON2)))=@OLD" & vbCrLf
sConsulta = sConsulta & "CLOSE C2" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C2" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--INICIO ACTUALIZACION COPIA_CAMPO, COPIA_LINEA_DESGLOSE, INSTANCIA_CENTROS_PARTIDAS, PM_CONTR_FILTRO_USU_BUSQUEDA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--ACTUALIZAR DATOS DE UONS EN LOS CENTROS Y PARTIDAS GRABADOS EN " & vbCrLf
sConsulta = sConsulta & "   --COPIA_CAMPO" & vbCrLf
sConsulta = sConsulta & "   --COPIA_LINEA_DESGLOSE" & vbCrLf
sConsulta = sConsulta & "   --INSTANCIA_CENTROS_DESGLOSE --> TABLA EN LA QUE SE HACE COPIA DE LOS DATOS DE CENTROS Y PARTIDAS DE COPIA_CAMPO Y COPIA_LINEA_DESGLOSE PARA ACELERAR FILTRADOS EN EL BUSCADOR DEL VISOR DE CONTRATOS" & vbCrLf
sConsulta = sConsulta & "   --PM_CONTR_FILTRO_USU_BUSQUEDA --> TABLA EN LA QUE SE HACE COPIA DE LOS VALORES POR DEFECTO PARA LOS FILTROS DEL BUSCADOR DEL VISOR DE CONTRATOS, QUE INCLUYE, ENTRE OTRAS COSAS CENTROS Y PARTIDAS." & vbCrLf
sConsulta = sConsulta & "--HAY QUE MODIFICARLO PORQUE EL MODO EN QUE SE GRABAN CENTROS Y PARTIDAS ES:" & vbCrLf
sConsulta = sConsulta & "--CENTROS: UON1#UON2#... HASTA EL NIVEL AL QUE EST� VINCULADO EL CENTRO" & vbCrLf
sConsulta = sConsulta & "--PARTIDAS: UON1#UON2#...#PRESN: UONS HASTA EL NIVEL AL QUE EST� VINCULADA LA PARTIDA Y PRESN, DONDE N ES EL NIVEL DE IMPUTACION QUE SE INDICA EN PARGEN_SM.IMP_NIVEL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- QU� ES MEJOR PARA BUSCAR LAS UON EN LOS CAMPOS, GUIARSE POR LA LONGITUD DEL C�DIGO UON O POR EL SEPARADOR #: " & vbCrLf
sConsulta = sConsulta & "-- DADO QUE SE PUEDE CAMBIAR LA LONGITUD EN LA TABLA DIC Y QUE EN LAS 4 TABLAS AL GUARDAR LOS DATOS NO SE TIENE EN CUENTA ESA LONGITUD, USAREMOS #" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--EN LAS PARTIDAS, EL �LTIMO VALOR ES SIEMPRE UNA PARTIDA PRESUPUESTARIA. AL MENOS HABR� SIEMPRE UN UON (UON1) PERO A PARTIR DE LOS UON2, PUEDE QUE NO HAYA, POR LO QUE TENEMOS QUE ASEGURARNOS DE NO CONFUNDIR LA PARTIDA CON UN UON AL REEMPLAZAR: VAMOS A IGNORAR LA PARTIDA AL FILTRAR" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- COPIA_CAMPO" & vbCrLf
sConsulta = sConsulta & "--     CENTROS" & vbCrLf
sConsulta = sConsulta & "DECLARE @SEPARADOR VARCHAR(3) = '#'" & vbCrLf
sConsulta = sConsulta & "UPDATE CC SET VALOR_TEXT = COALESCE(STUFF(CC.VALOR_TEXT, LEN(@UON1 + @SEPARADOR) + 1, LEN(@OLD), @NEW) , VALOR_TEXT)" & vbCrLf
sConsulta = sConsulta & "FROM COPIA_CAMPO CC WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_CAMPO_DEF CD WITH(NOLOCK) ON CC.COPIA_CAMPO_DEF=CD.ID" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "   CD.TIPO_CAMPO_GS = 129 AND " & vbCrLf
sConsulta = sConsulta & "   CC.VALOR_TEXT IS NOT NULL AND " & vbCrLf
sConsulta = sConsulta & "   SUBSTRING(CC.VALOR_TEXT, " & vbCrLf
sConsulta = sConsulta & "             1, " & vbCrLf
sConsulta = sConsulta & "             CASE WHEN " & vbCrLf
sConsulta = sConsulta & "               CHARINDEX(@SEPARADOR,CC.VALOR_TEXT, CHARINDEX(@SEPARADOR,CC.VALOR_TEXT) + 1)=0 THEN " & vbCrLf
sConsulta = sConsulta & "                   LEN(CC.VALOR_TEXT) " & vbCrLf
sConsulta = sConsulta & "               ELSE " & vbCrLf
sConsulta = sConsulta & "                   CHARINDEX(@SEPARADOR,CC.VALOR_TEXT, CHARINDEX(@SEPARADOR,CC.VALOR_TEXT) + 1)-1" & vbCrLf
sConsulta = sConsulta & "               END" & vbCrLf
sConsulta = sConsulta & "           )=@UON1 + @SEPARADOR + @OLD" & vbCrLf
sConsulta = sConsulta & "--     PARTIDAS" & vbCrLf
sConsulta = sConsulta & "UPDATE CC SET VALOR_TEXT = COALESCE(STUFF(CC.VALOR_TEXT, LEN(@UON1 + @SEPARADOR) + 1, LEN(@OLD), @NEW) , VALOR_TEXT)" & vbCrLf
sConsulta = sConsulta & "FROM COPIA_CAMPO CC WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_CAMPO_DEF CD WITH(NOLOCK) ON CC.COPIA_CAMPO_DEF=CD.ID" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "   CD.TIPO_CAMPO_GS = 130 AND " & vbCrLf
sConsulta = sConsulta & "   CC.VALOR_TEXT IS NOT NULL AND " & vbCrLf
sConsulta = sConsulta & "   SUBSTRING(SUBSTRING(CC.VALOR_TEXT, 1, LEN(CC.VALOR_TEXT) - CHARINDEX(@SEPARADOR, REVERSE(CC.VALOR_TEXT))),  --QUITAMOS LA PARTIDA PRES" & vbCrLf
sConsulta = sConsulta & "             1, " & vbCrLf
sConsulta = sConsulta & "             CASE WHEN " & vbCrLf
sConsulta = sConsulta & "               CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                         SUBSTRING(CC.VALOR_TEXT, 1, LEN(CC.VALOR_TEXT) - CHARINDEX(@SEPARADOR, REVERSE(CC.VALOR_TEXT))), " & vbCrLf
sConsulta = sConsulta & "                         CHARINDEX(@SEPARADOR,CC.VALOR_TEXT, LEN(@UON1 + @SEPARADOR) + 1)" & vbCrLf
sConsulta = sConsulta & "                         )=0 THEN " & vbCrLf
sConsulta = sConsulta & "                   LEN(SUBSTRING(CC.VALOR_TEXT, 1, LEN(CC.VALOR_TEXT) - CHARINDEX(@SEPARADOR, REVERSE(CC.VALOR_TEXT))))" & vbCrLf
sConsulta = sConsulta & "               ELSE " & vbCrLf
sConsulta = sConsulta & "                   CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                             SUBSTRING(CC.VALOR_TEXT, 1, LEN(CC.VALOR_TEXT) - CHARINDEX(@SEPARADOR, REVERSE(CC.VALOR_TEXT))), " & vbCrLf
sConsulta = sConsulta & "                             CHARINDEX(@SEPARADOR,CC.VALOR_TEXT, CHARINDEX(@SEPARADOR,CC.VALOR_TEXT) + 1))-1" & vbCrLf
sConsulta = sConsulta & "               END" & vbCrLf
sConsulta = sConsulta & "           )=@UON1 + @SEPARADOR + @OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- COPIA_LINEA_DESGLOSE" & vbCrLf
sConsulta = sConsulta & "--     CENTROS" & vbCrLf
sConsulta = sConsulta & "UPDATE CLD SET VALOR_TEXT = COALESCE(STUFF(CLD.VALOR_TEXT, LEN(@UON1 + @SEPARADOR) + 1, LEN(@OLD), @NEW) , CLD.VALOR_TEXT)" & vbCrLf
sConsulta = sConsulta & "FROM COPIA_LINEA_DESGLOSE CLD WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_CAMPO CC WITH (NOLOCK) ON CLD.CAMPO_HIJO = CC.ID " & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_CAMPO_DEF CCD WITH (NOLOCK) ON CC.COPIA_CAMPO_DEF = CCD.ID" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "   CCD.TIPO_CAMPO_GS = 129 AND " & vbCrLf
sConsulta = sConsulta & "   CLD.VALOR_TEXT IS NOT NULL AND " & vbCrLf
sConsulta = sConsulta & "   SUBSTRING(CLD.VALOR_TEXT, " & vbCrLf
sConsulta = sConsulta & "             1, " & vbCrLf
sConsulta = sConsulta & "             CASE WHEN " & vbCrLf
sConsulta = sConsulta & "               CHARINDEX(@SEPARADOR,CLD.VALOR_TEXT, LEN(@UON1 + @SEPARADOR) + 1)=0 THEN " & vbCrLf
sConsulta = sConsulta & "                   LEN(CLD.VALOR_TEXT) " & vbCrLf
sConsulta = sConsulta & "               ELSE " & vbCrLf
sConsulta = sConsulta & "                   CHARINDEX(@SEPARADOR,CLD.VALOR_TEXT, LEN(@UON1 + @SEPARADOR) + 1)-1" & vbCrLf
sConsulta = sConsulta & "               END" & vbCrLf
sConsulta = sConsulta & "           )=@UON1 + @SEPARADOR + @OLD" & vbCrLf
sConsulta = sConsulta & "--     PARTIDAS" & vbCrLf
sConsulta = sConsulta & "UPDATE CLD SET VALOR_TEXT = COALESCE(STUFF(CLD.VALOR_TEXT, LEN(@UON1 + @SEPARADOR) + 1, LEN(@OLD), @NEW) , CLD.VALOR_TEXT)" & vbCrLf
sConsulta = sConsulta & "FROM COPIA_LINEA_DESGLOSE CLD WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_CAMPO CC WITH (NOLOCK) ON CLD.CAMPO_HIJO = CC.ID " & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_CAMPO_DEF CCD WITH (NOLOCK) ON CC.COPIA_CAMPO_DEF = CCD.ID" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "   CCD.TIPO_CAMPO_GS = 130 AND " & vbCrLf
sConsulta = sConsulta & "   CLD.VALOR_TEXT IS NOT NULL AND " & vbCrLf
sConsulta = sConsulta & "   SUBSTRING(SUBSTRING(CLD.VALOR_TEXT, 1, LEN(CLD.VALOR_TEXT) - CHARINDEX(@SEPARADOR, REVERSE(CLD.VALOR_TEXT))),  --QUITAMOS LA PARTIDA PRES" & vbCrLf
sConsulta = sConsulta & "             1, " & vbCrLf
sConsulta = sConsulta & "             CASE WHEN " & vbCrLf
sConsulta = sConsulta & "               CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                         SUBSTRING(CLD.VALOR_TEXT, 1, LEN(CLD.VALOR_TEXT) - CHARINDEX(@SEPARADOR, REVERSE(CLD.VALOR_TEXT))), " & vbCrLf
sConsulta = sConsulta & "                         CHARINDEX(@SEPARADOR,CLD.VALOR_TEXT, LEN(@UON1 + @SEPARADOR) + 1)" & vbCrLf
sConsulta = sConsulta & "                         )=0 THEN " & vbCrLf
sConsulta = sConsulta & "                   LEN(SUBSTRING(CLD.VALOR_TEXT, 1, LEN(CLD.VALOR_TEXT) - CHARINDEX(@SEPARADOR, REVERSE(CLD.VALOR_TEXT))))" & vbCrLf
sConsulta = sConsulta & "               ELSE " & vbCrLf
sConsulta = sConsulta & "                   CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                             SUBSTRING(CLD.VALOR_TEXT, 1, LEN(CLD.VALOR_TEXT) - CHARINDEX(@SEPARADOR, REVERSE(CLD.VALOR_TEXT))), " & vbCrLf
sConsulta = sConsulta & "                             CHARINDEX(@SEPARADOR,CLD.VALOR_TEXT, LEN(@UON1 + @SEPARADOR) + 1))-1" & vbCrLf
sConsulta = sConsulta & "               END" & vbCrLf
sConsulta = sConsulta & "           )=@UON1 + @SEPARADOR + @OLD" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "-- INSTANCIA_CENTROS_PARTIDAS" & vbCrLf
sConsulta = sConsulta & "--     CENTROS" & vbCrLf
sConsulta = sConsulta & "UPDATE ICP SET VALOR_TEXT = COALESCE(STUFF(ICP.VALOR_TEXT, LEN(@UON1 + @SEPARADOR) + 1, LEN(@OLD), @NEW) , VALOR_TEXT)" & vbCrLf
sConsulta = sConsulta & "FROM INSTANCIA_CENTROS_PARTIDAS ICP WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "ICP.TIPO_CAMPO_GS = 129 AND " & vbCrLf
sConsulta = sConsulta & "ICP.VALOR_TEXT IS NOT NULL AND" & vbCrLf
sConsulta = sConsulta & "SUBSTRING(ICP.VALOR_TEXT, " & vbCrLf
sConsulta = sConsulta & "         1, " & vbCrLf
sConsulta = sConsulta & "         CASE WHEN " & vbCrLf
sConsulta = sConsulta & "           CHARINDEX(@SEPARADOR,ICP.VALOR_TEXT, LEN(@UON1 + @SEPARADOR) + 1)=0 THEN " & vbCrLf
sConsulta = sConsulta & "               LEN(ICP.VALOR_TEXT) " & vbCrLf
sConsulta = sConsulta & "           ELSE " & vbCrLf
sConsulta = sConsulta & "               CHARINDEX(@SEPARADOR,ICP.VALOR_TEXT, LEN(@UON1 + @SEPARADOR) + 1)-1" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "       )=@UON1 + @SEPARADOR + @OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--     PARTIDAS" & vbCrLf
sConsulta = sConsulta & "UPDATE ICP SET VALOR_TEXT = COALESCE(STUFF(ICP.VALOR_TEXT, LEN(@UON1 + @SEPARADOR) + 1, LEN(@OLD), @NEW) , VALOR_TEXT)" & vbCrLf
sConsulta = sConsulta & "FROM INSTANCIA_CENTROS_PARTIDAS ICP WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "ICP.TIPO_CAMPO_GS  = 130 AND " & vbCrLf
sConsulta = sConsulta & "ICP.VALOR_TEXT IS NOT NULL AND" & vbCrLf
sConsulta = sConsulta & "SUBSTRING(SUBSTRING(ICP.VALOR_TEXT, 1, LEN(ICP.VALOR_TEXT) - CHARINDEX(@SEPARADOR, REVERSE(ICP.VALOR_TEXT))),  --QUITAMOS LA PARTIDA PRES" & vbCrLf
sConsulta = sConsulta & "         1, " & vbCrLf
sConsulta = sConsulta & "         CASE WHEN " & vbCrLf
sConsulta = sConsulta & "           CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                     SUBSTRING(ICP.VALOR_TEXT, 1, LEN(ICP.VALOR_TEXT) - CHARINDEX(@SEPARADOR, REVERSE(ICP.VALOR_TEXT))), " & vbCrLf
sConsulta = sConsulta & "                     CHARINDEX(@SEPARADOR,ICP.VALOR_TEXT, LEN(@UON1 + @SEPARADOR) + 1)" & vbCrLf
sConsulta = sConsulta & "                     )=0 THEN " & vbCrLf
sConsulta = sConsulta & "               LEN(SUBSTRING(ICP.VALOR_TEXT, 1, LEN(ICP.VALOR_TEXT) - CHARINDEX(@SEPARADOR, REVERSE(ICP.VALOR_TEXT))))" & vbCrLf
sConsulta = sConsulta & "           ELSE " & vbCrLf
sConsulta = sConsulta & "               CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                         SUBSTRING(ICP.VALOR_TEXT, 1, LEN(ICP.VALOR_TEXT) - CHARINDEX(@SEPARADOR, REVERSE(ICP.VALOR_TEXT))), " & vbCrLf
sConsulta = sConsulta & "                         CHARINDEX(@SEPARADOR,ICP.VALOR_TEXT, LEN(@UON1 + @SEPARADOR) + 1))-1" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "       )=@UON1 + @SEPARADOR + @OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- PM_CONTR_FILTRO_USU_BUSQUEDA" & vbCrLf
sConsulta = sConsulta & "UPDATE P SET CENTRO_COSTE = COALESCE(STUFF(CENTRO_COSTE, LEN(@UON1 + @SEPARADOR) + 1, LEN(@OLD), @NEW) , CENTRO_COSTE)" & vbCrLf
sConsulta = sConsulta & "FROM PM_CONTR_FILTRO_USU_BUSQUEDA P WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "P.CENTRO_COSTE IS NOT NULL AND" & vbCrLf
sConsulta = sConsulta & "SUBSTRING(P.CENTRO_COSTE, " & vbCrLf
sConsulta = sConsulta & "         1, " & vbCrLf
sConsulta = sConsulta & "         CASE WHEN " & vbCrLf
sConsulta = sConsulta & "           CHARINDEX(@SEPARADOR,P.CENTRO_COSTE, LEN(@UON1 + @SEPARADOR) + 1)=0 THEN " & vbCrLf
sConsulta = sConsulta & "               LEN(P.CENTRO_COSTE) " & vbCrLf
sConsulta = sConsulta & "           ELSE " & vbCrLf
sConsulta = sConsulta & "               CHARINDEX(@SEPARADOR,P.CENTRO_COSTE, LEN(@UON1 + @SEPARADOR) + 1)-1" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "       )=@UON1 + @SEPARADOR + @OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE P SET PARTIDA1 = COALESCE(STUFF(PARTIDA1, LEN(@UON1 + @SEPARADOR) + 1, LEN(@OLD), @NEW) , PARTIDA1)" & vbCrLf
sConsulta = sConsulta & "FROM PM_CONTR_FILTRO_USU_BUSQUEDA P WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "P.PARTIDA1 IS NOT NULL AND" & vbCrLf
sConsulta = sConsulta & "SUBSTRING(SUBSTRING(P.PARTIDA1, 1, LEN(P.PARTIDA1) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA1))),  --QUITAMOS LA PARTIDA PRES" & vbCrLf
sConsulta = sConsulta & "         1, " & vbCrLf
sConsulta = sConsulta & "         CASE WHEN " & vbCrLf
sConsulta = sConsulta & "           CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                     SUBSTRING(P.PARTIDA1, 1, LEN(P.PARTIDA1) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA1))), " & vbCrLf
sConsulta = sConsulta & "                     CHARINDEX(@SEPARADOR,P.PARTIDA1, LEN(@UON1 + @SEPARADOR) + 1)" & vbCrLf
sConsulta = sConsulta & "                     )=0 THEN " & vbCrLf
sConsulta = sConsulta & "               LEN(SUBSTRING(P.PARTIDA1, 1, LEN(P.PARTIDA1) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA1))))" & vbCrLf
sConsulta = sConsulta & "           ELSE " & vbCrLf
sConsulta = sConsulta & "               CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                         SUBSTRING(P.PARTIDA1, 1, LEN(P.PARTIDA1) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA1))), " & vbCrLf
sConsulta = sConsulta & "                         CHARINDEX(@SEPARADOR,P.PARTIDA1, LEN(@UON1 + @SEPARADOR) + 1))-1" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "       )=@UON1 + @SEPARADOR + @OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE P SET PARTIDA2 = COALESCE(STUFF(PARTIDA2, LEN(@UON1 + @SEPARADOR) + 1, LEN(@OLD), @NEW) , PARTIDA2)" & vbCrLf
sConsulta = sConsulta & "FROM PM_CONTR_FILTRO_USU_BUSQUEDA P WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "P.PARTIDA2 IS NOT NULL AND" & vbCrLf
sConsulta = sConsulta & "SUBSTRING(SUBSTRING(P.PARTIDA2, 1, LEN(P.PARTIDA2) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA2))),  --QUITAMOS LA PARTIDA PRES" & vbCrLf
sConsulta = sConsulta & "         1, " & vbCrLf
sConsulta = sConsulta & "         CASE WHEN " & vbCrLf
sConsulta = sConsulta & "           CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                     SUBSTRING(P.PARTIDA2, 1, LEN(P.PARTIDA2) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA2))), " & vbCrLf
sConsulta = sConsulta & "                     CHARINDEX(@SEPARADOR,P.PARTIDA2, LEN(@UON1 + @SEPARADOR) + 1)" & vbCrLf
sConsulta = sConsulta & "                     )=0 THEN " & vbCrLf
sConsulta = sConsulta & "               LEN(SUBSTRING(P.PARTIDA2, 1, LEN(P.PARTIDA2) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA2))))" & vbCrLf
sConsulta = sConsulta & "           ELSE " & vbCrLf
sConsulta = sConsulta & "               CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                         SUBSTRING(P.PARTIDA2, 1, LEN(P.PARTIDA2) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA2))), " & vbCrLf
sConsulta = sConsulta & "                         CHARINDEX(@SEPARADOR,P.PARTIDA2, LEN(@UON1 + @SEPARADOR) + 1))-1" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "       )=@UON1 + @SEPARADOR + @OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE P SET PARTIDA3 = COALESCE(STUFF(PARTIDA3, LEN(@UON1 + @SEPARADOR) + 1, LEN(@OLD), @NEW) , PARTIDA3)" & vbCrLf
sConsulta = sConsulta & "FROM PM_CONTR_FILTRO_USU_BUSQUEDA P WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "P.PARTIDA3 IS NOT NULL AND" & vbCrLf
sConsulta = sConsulta & "SUBSTRING(SUBSTRING(P.PARTIDA3, 1, LEN(P.PARTIDA3) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA3))),  --QUITAMOS LA PARTIDA PRES" & vbCrLf
sConsulta = sConsulta & "         1, " & vbCrLf
sConsulta = sConsulta & "         CASE WHEN " & vbCrLf
sConsulta = sConsulta & "           CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                     SUBSTRING(P.PARTIDA3, 1, LEN(P.PARTIDA3) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA3))), " & vbCrLf
sConsulta = sConsulta & "                     CHARINDEX(@SEPARADOR,P.PARTIDA3, LEN(@UON1 + @SEPARADOR) + 1)" & vbCrLf
sConsulta = sConsulta & "                     )=0 THEN " & vbCrLf
sConsulta = sConsulta & "               LEN(SUBSTRING(P.PARTIDA3, 1, LEN(P.PARTIDA3) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA3))))" & vbCrLf
sConsulta = sConsulta & "           ELSE " & vbCrLf
sConsulta = sConsulta & "               CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                         SUBSTRING(P.PARTIDA3, 1, LEN(P.PARTIDA3) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA3))), " & vbCrLf
sConsulta = sConsulta & "                         CHARINDEX(@SEPARADOR,P.PARTIDA3, LEN(@UON1 + @SEPARADOR) + 1))-1" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "       )=@UON1 + @SEPARADOR + @OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE P SET PARTIDA4 = COALESCE(STUFF(PARTIDA4, LEN(@UON1 + @SEPARADOR) + 1, LEN(@OLD), @NEW) , PARTIDA4)" & vbCrLf
sConsulta = sConsulta & "FROM PM_CONTR_FILTRO_USU_BUSQUEDA P WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "P.PARTIDA4 IS NOT NULL AND" & vbCrLf
sConsulta = sConsulta & "SUBSTRING(SUBSTRING(P.PARTIDA4, 1, LEN(P.PARTIDA4) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA4))),  --QUITAMOS LA PARTIDA PRES" & vbCrLf
sConsulta = sConsulta & "         1, " & vbCrLf
sConsulta = sConsulta & "         CASE WHEN " & vbCrLf
sConsulta = sConsulta & "           CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                     SUBSTRING(P.PARTIDA4, 1, LEN(P.PARTIDA4) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA4))), " & vbCrLf
sConsulta = sConsulta & "                     CHARINDEX(@SEPARADOR,P.PARTIDA4, LEN(@UON1 + @SEPARADOR) + 1)" & vbCrLf
sConsulta = sConsulta & "                     )=0 THEN " & vbCrLf
sConsulta = sConsulta & "               LEN(SUBSTRING(P.PARTIDA4, 1, LEN(P.PARTIDA4) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA4))))" & vbCrLf
sConsulta = sConsulta & "           ELSE " & vbCrLf
sConsulta = sConsulta & "               CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                         SUBSTRING(P.PARTIDA4, 1, LEN(P.PARTIDA4) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA4))), " & vbCrLf
sConsulta = sConsulta & "                         CHARINDEX(@SEPARADOR,P.PARTIDA4, LEN(@UON1 + @SEPARADOR) + 1))-1" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "       )=@UON1 + @SEPARADOR + @OLD" & vbCrLf
sConsulta = sConsulta & "--FIN ACTUALIZACION COPIA_CAMPO, COPIA_LINEA_DESGLOSE, INSTANCIA_CENTROS_PARTIDAS, PM_CONTR_FILTRO_USU_BUSQUEDA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON2_DEP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3_DEP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PER CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON2_DEST CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3_DEST CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISOR CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CERTIFICADO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE NOCONFORMIDAD CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE APROBADORES CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PETICIONARIOS CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_ROL CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_ENLACE_CONDICIONES CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_COPIA_ROL CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_COPIA_ENLACE_CONDICION CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_UON CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU_CC_CONTROL CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU_CC_IMPUTACION CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES5_UON CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CENTRO_SM_UON CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON4_DEST CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_PED_IMPUTACION CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PERF_UON CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_CAT_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_CAT_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_CAT_UON2_DEP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_CAT_UON3_DEP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_GRUPO_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_GRUPO_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_GRUPO_UON2_DEP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_GRUPO_UON3_DEP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_MEN_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_MEN_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_MEN_UON2_DEP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_MEN_UON3_DEP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CUSTOM_TEMPLATE_UON CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "CLOSE C" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "COMMIT TRANSACTION" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT OFF" & vbCrLf
ExecuteSQL gRDOCon, sConsulta



    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UON3_COD]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[UON3_COD]" & vbCrLf
    gRDOCon.QueryTimeout = 7200
    ExecuteSQL gRDOCon, sConsulta


sConsulta = "CREATE  PROCEDURE [DBO].[UON3_COD] (@OLD VARCHAR(50),@NEW VARCHAR(50), @UON1 VARCHAR(50), @UON2 VARCHAR(50))  AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT ON" & vbCrLf
sConsulta = sConsulta & "BEGIN TRANSACTION" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR FOR SELECT COD FROM UON3 WITH(NOLOCK) WHERE UON1=@UON1 AND UON2=@UON2 AND COD=@OLD" & vbCrLf
sConsulta = sConsulta & "OPEN C" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C INTO @VAR_COD" & vbCrLf
sConsulta = sConsulta & "IF @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3_DEP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PER NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3_DEST NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISOR NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CERTIFICADO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE NOCONFORMIDAD NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE APROBADORES NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PETICIONARIOS NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_ROL NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_ENLACE_CONDICIONES NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_COPIA_ROL NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_COPIA_ENLACE_CONDICION NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_UON NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU_CC_CONTROL NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU_CC_IMPUTACION NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES5_UON NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CENTRO_SM_UON NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON4_DEST NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_PED_IMPUTACION NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PERF_UON NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_CAT_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_CAT_UON3_DEP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_GRUPO_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_GRUPO_UON3_DEP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_MEN_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_MEN_UON3_DEP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CUSTOM_TEMPLATE_UON NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE UON3 SET COD=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND COD=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON4 SET UON3 = @NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON3_DEP SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PER SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE COM SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_UON3 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_UON3 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON3_DEST SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_UON3 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_UON3 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES1_NIV1 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES1_NIV2 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES1_NIV3 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES1_NIV4 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES2_NIV1 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES2_NIV2 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES2_NIV3 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES2_NIV4 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES3_NIV1 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES3_NIV2 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES3_NIV3 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES3_NIV4 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES4_NIV1 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES4_NIV2 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES4_NIV3 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES4_NIV4 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_1 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_2 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_3 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_4 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_1 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_2 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_3 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_4 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON1 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON2 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON3 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON4 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY1 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY2 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY3 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY4 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_PRESCON3 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_PRESCON4 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISOR SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CERTIFICADO SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE NOCONFORMIDAD SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE APROBADORES SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PETICIONARIOS SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PM_ROL SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PM_COPIA_ROL SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4_UON SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET UON3_APER=@NEW WHERE UON1_APER=@UON1 AND UON2_APER=@UON2 AND UON3_APER=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE USU_CC_CONTROL SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE USU_CC_IMPUTACION SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES5_UON SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CENTRO_SM_UON SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON4_DEST SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE LINEAS_PED_IMPUTACION SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PERF_UON SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_CAT_UON3 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_CAT_UON3_DEP SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_GRUPO_UON3 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_GRUPO_UON3_DEP SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_MEN_UON3 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_MEN_UON3_DEP SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CUSTOM_TEMPLATE_UON SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @LEN_COD_UON1 AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @LEN_COD_UON2 AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @LEN_COD_UON3 AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE C2 CURSOR FOR SELECT (SELECT LONGITUD FROM DIC WITH(NOLOCK) WHERE NOMBRE='UON1') AS L1,(SELECT LONGITUD FROM DIC WITH(NOLOCK) WHERE NOMBRE='UON2') AS L2,(SELECT LONGITUD FROM DIC WITH(NOLOCK) WHERE NOMBRE='UON3') AS L3" & vbCrLf
sConsulta = sConsulta & "OPEN C2" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C2 INTO @LEN_COD_UON1,@LEN_COD_UON2,@LEN_COD_UON3" & vbCrLf
sConsulta = sConsulta & "IF @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "  UPDATE PM_ENLACE_CONDICIONES SET VALOR_TEXT=STUFF(VALOR_TEXT,@LEN_COD_UON1 + @LEN_COD_UON2 + 3,@LEN_COD_UON3,@NEW + SPACE(@LEN_COD_UON3 - LEN(@NEW))) WHERE TIPO_CAMPO=4 AND TIPO_VALOR=2 AND LTRIM(RTRIM(SUBSTRING(VALOR_TEXT,1,@LEN_COD_UON1)))=@UON1 AND LTRIM(RTRIM(SUBSTRING(VALOR_TEXT,@LEN_COD_UON1+2, @LEN_COD_UON2)))=@UON2 AND LTRIM(RTRIM(SUBSTRING(VALOR_TEXT,@LEN_COD_UON1+@LEN_COD_UON2+3, @LEN_COD_UON3)))=@OLD" & vbCrLf
sConsulta = sConsulta & "  UPDATE PM_COPIA_ENLACE_CONDICION SET VALOR_TEXT=STUFF(VALOR_TEXT,@LEN_COD_UON1 + @LEN_COD_UON2 + 3,@LEN_COD_UON3,@NEW + SPACE(@LEN_COD_UON3 - LEN(@NEW))) WHERE TIPO_CAMPO=4 AND TIPO_VALOR=2 AND LTRIM(RTRIM(SUBSTRING(VALOR_TEXT,1,@LEN_COD_UON1)))=@UON1 AND LTRIM(RTRIM(SUBSTRING(VALOR_TEXT,@LEN_COD_UON1+2, @LEN_COD_UON2)))=@UON2 AND LTRIM(RTRIM(SUBSTRING(VALOR_TEXT,@LEN_COD_UON1+@LEN_COD_UON2+3, @LEN_COD_UON3)))=@OLD" & vbCrLf
sConsulta = sConsulta & "CLOSE C2" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C2" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--INICIO ACTUALIZACION COPIA_CAMPO, COPIA_LINEA_DESGLOSE, INSTANCIA_CENTROS_PARTIDAS, PM_CONTR_FILTRO_USU_BUSQUEDA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--ACTUALIZAR DATOS DE UONS EN LOS CENTROS Y PARTIDAS GRABADOS EN " & vbCrLf
sConsulta = sConsulta & "   --COPIA_CAMPO" & vbCrLf
sConsulta = sConsulta & "   --COPIA_LINEA_DESGLOSE" & vbCrLf
sConsulta = sConsulta & "   --INSTANCIA_CENTROS_DESGLOSE --> TABLA EN LA QUE SE HACE COPIA DE LOS DATOS DE CENTROS Y PARTIDAS DE COPIA_CAMPO Y COPIA_LINEA_DESGLOSE PARA ACELERAR FILTRADOS EN EL BUSCADOR DEL VISOR DE CONTRATOS" & vbCrLf
sConsulta = sConsulta & "   --PM_CONTR_FILTRO_USU_BUSQUEDA --> TABLA EN LA QUE SE HACE COPIA DE LOS VALORES POR DEFECTO PARA LOS FILTROS DEL BUSCADOR DEL VISOR DE CONTRATOS, QUE INCLUYE, ENTRE OTRAS COSAS CENTROS Y PARTIDAS." & vbCrLf
sConsulta = sConsulta & "--HAY QUE MODIFICARLO PORQUE EL MODO EN QUE SE GRABAN CENTROS Y PARTIDAS ES:" & vbCrLf
sConsulta = sConsulta & "--CENTROS: UON1#UON2#... HASTA EL NIVEL AL QUE EST� VINCULADO EL CENTRO" & vbCrLf
sConsulta = sConsulta & "--PARTIDAS: UON1#UON2#...#PRESN: UONS HASTA EL NIVEL AL QUE EST� VINCULADA LA PARTIDA Y PRESN, DONDE N ES EL NIVEL DE IMPUTACION QUE SE INDICA EN PARGEN_SM.IMP_NIVEL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- QU� ES MEJOR PARA BUSCAR LAS UON EN LOS CAMPOS, GUIARSE POR LA LONGITUD DEL C�DIGO UON O POR EL SEPARADOR #: " & vbCrLf
sConsulta = sConsulta & "-- DADO QUE SE PUEDE CAMBIAR LA LONGITUD EN LA TABLA DIC Y QUE EN LAS 4 TABLAS AL GUARDAR LOS DATOS NO SE TIENE EN CUENTA ESA LONGITUD, USAREMOS #" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--EN LAS PARTIDAS, EL �LTIMO VALOR ES SIEMPRE UNA PARTIDA PRESUPUESTARIA. AL MENOS HABR� SIEMPRE UN UON (UON1) PERO A PARTIR DE LOS UON2, PUEDE QUE NO HAYA, POR LO QUE TENEMOS QUE ASEGURARNOS DE NO CONFUNDIR LA PARTIDA CON UN UON AL REEMPLAZAR: VAMOS A IGNORAR LA PARTIDA AL FILTRAR" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- COPIA_CAMPO" & vbCrLf
sConsulta = sConsulta & "--     CENTROS" & vbCrLf
sConsulta = sConsulta & "DECLARE @SEPARADOR VARCHAR(3) = '#'" & vbCrLf
sConsulta = sConsulta & "UPDATE CC SET VALOR_TEXT = COALESCE(STUFF(CC.VALOR_TEXT, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1, LEN(@OLD), @NEW) , VALOR_TEXT)" & vbCrLf
sConsulta = sConsulta & "FROM COPIA_CAMPO CC WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_CAMPO_DEF CD WITH(NOLOCK) ON CC.COPIA_CAMPO_DEF=CD.ID" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "   CD.TIPO_CAMPO_GS = 129 AND " & vbCrLf
sConsulta = sConsulta & "   CC.VALOR_TEXT IS NOT NULL AND " & vbCrLf
sConsulta = sConsulta & "   SUBSTRING(CC.VALOR_TEXT, " & vbCrLf
sConsulta = sConsulta & "             1, " & vbCrLf
sConsulta = sConsulta & "             CASE WHEN " & vbCrLf
sConsulta = sConsulta & "               CHARINDEX(@SEPARADOR,CC.VALOR_TEXT, CHARINDEX(@SEPARADOR,CC.VALOR_TEXT, CHARINDEX (@SEPARADOR,CC.VALOR_TEXT) + 1) + 1)=0 THEN " & vbCrLf
sConsulta = sConsulta & "                   LEN(CC.VALOR_TEXT) " & vbCrLf
sConsulta = sConsulta & "               ELSE " & vbCrLf
sConsulta = sConsulta & "                   CHARINDEX(@SEPARADOR,CC.VALOR_TEXT, CHARINDEX(@SEPARADOR,CC.VALOR_TEXT, CHARINDEX (@SEPARADOR,CC.VALOR_TEXT) + 1) + 1)-1" & vbCrLf
sConsulta = sConsulta & "               END" & vbCrLf
sConsulta = sConsulta & "           )=@UON1 + @SEPARADOR + @UON2 + @SEPARADOR + @OLD" & vbCrLf
sConsulta = sConsulta & "--     PARTIDAS" & vbCrLf
sConsulta = sConsulta & "UPDATE CC SET VALOR_TEXT = COALESCE(STUFF(CC.VALOR_TEXT, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1, LEN(@OLD), @NEW) , VALOR_TEXT)" & vbCrLf
sConsulta = sConsulta & "FROM COPIA_CAMPO CC WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_CAMPO_DEF CD WITH(NOLOCK) ON CC.COPIA_CAMPO_DEF=CD.ID" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "   CD.TIPO_CAMPO_GS = 130 AND " & vbCrLf
sConsulta = sConsulta & "   CC.VALOR_TEXT IS NOT NULL AND " & vbCrLf
sConsulta = sConsulta & "   SUBSTRING(SUBSTRING(CC.VALOR_TEXT, 1, LEN(CC.VALOR_TEXT) - CHARINDEX(@SEPARADOR, REVERSE(CC.VALOR_TEXT))),  --QUITAMOS LA PARTIDA PRES" & vbCrLf
sConsulta = sConsulta & "             1, " & vbCrLf
sConsulta = sConsulta & "             CASE WHEN " & vbCrLf
sConsulta = sConsulta & "               CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                         SUBSTRING(CC.VALOR_TEXT, 1, LEN(CC.VALOR_TEXT) - CHARINDEX(@SEPARADOR, REVERSE(CC.VALOR_TEXT))), " & vbCrLf
sConsulta = sConsulta & "                         CHARINDEX(@SEPARADOR,CC.VALOR_TEXT, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1)" & vbCrLf
sConsulta = sConsulta & "                         )=0 THEN " & vbCrLf
sConsulta = sConsulta & "                   LEN(SUBSTRING(CC.VALOR_TEXT, 1, LEN(CC.VALOR_TEXT) - CHARINDEX(@SEPARADOR, REVERSE(CC.VALOR_TEXT))))" & vbCrLf
sConsulta = sConsulta & "               ELSE " & vbCrLf
sConsulta = sConsulta & "                   CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                             SUBSTRING(CC.VALOR_TEXT, 1, LEN(CC.VALOR_TEXT) - CHARINDEX(@SEPARADOR, REVERSE(CC.VALOR_TEXT))), " & vbCrLf
sConsulta = sConsulta & "                             CHARINDEX(@SEPARADOR,CC.VALOR_TEXT, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1))-1" & vbCrLf
sConsulta = sConsulta & "               END" & vbCrLf
sConsulta = sConsulta & "           )=@UON1 + @SEPARADOR + @UON2 + @SEPARADOR + @OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- COPIA_LINEA_DESGLOSE" & vbCrLf
sConsulta = sConsulta & "--     CENTROS" & vbCrLf
sConsulta = sConsulta & "UPDATE CLD SET VALOR_TEXT = COALESCE(STUFF(CLD.VALOR_TEXT, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1, LEN(@OLD), @NEW) , CLD.VALOR_TEXT)" & vbCrLf
sConsulta = sConsulta & "FROM COPIA_LINEA_DESGLOSE CLD WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_CAMPO CC WITH (NOLOCK) ON CLD.CAMPO_HIJO = CC.ID " & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_CAMPO_DEF CCD WITH (NOLOCK) ON CC.COPIA_CAMPO_DEF = CCD.ID" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "   CCD.TIPO_CAMPO_GS = 129 AND " & vbCrLf
sConsulta = sConsulta & "   CLD.VALOR_TEXT IS NOT NULL AND " & vbCrLf
sConsulta = sConsulta & "   SUBSTRING(CLD.VALOR_TEXT, " & vbCrLf
sConsulta = sConsulta & "             1, " & vbCrLf
sConsulta = sConsulta & "             CASE WHEN " & vbCrLf
sConsulta = sConsulta & "               CHARINDEX(@SEPARADOR,CLD.VALOR_TEXT, CHARINDEX(@SEPARADOR,CLD.VALOR_TEXT, CHARINDEX (@SEPARADOR,CLD.VALOR_TEXT) + 1) + 1)=0 THEN " & vbCrLf
sConsulta = sConsulta & "                   LEN(CLD.VALOR_TEXT) " & vbCrLf
sConsulta = sConsulta & "               ELSE " & vbCrLf
sConsulta = sConsulta & "                   CHARINDEX(@SEPARADOR,CLD.VALOR_TEXT, CHARINDEX(@SEPARADOR,CLD.VALOR_TEXT, CHARINDEX (@SEPARADOR,CLD.VALOR_TEXT) + 1) + 1)-1" & vbCrLf
sConsulta = sConsulta & "               END" & vbCrLf
sConsulta = sConsulta & "           )=@UON1 + @SEPARADOR + @UON2 + @SEPARADOR + @OLD" & vbCrLf
sConsulta = sConsulta & "--     PARTIDAS" & vbCrLf
sConsulta = sConsulta & "UPDATE CLD SET VALOR_TEXT = COALESCE(STUFF(CLD.VALOR_TEXT, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1, LEN(@OLD), @NEW) , CLD.VALOR_TEXT)" & vbCrLf
sConsulta = sConsulta & "FROM COPIA_LINEA_DESGLOSE CLD WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_CAMPO CC WITH (NOLOCK) ON CLD.CAMPO_HIJO = CC.ID " & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_CAMPO_DEF CCD WITH (NOLOCK) ON CC.COPIA_CAMPO_DEF = CCD.ID" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "   CCD.TIPO_CAMPO_GS = 130 AND " & vbCrLf
sConsulta = sConsulta & "   CLD.VALOR_TEXT IS NOT NULL AND " & vbCrLf
sConsulta = sConsulta & "   SUBSTRING(SUBSTRING(CLD.VALOR_TEXT, 1, LEN(CLD.VALOR_TEXT) - CHARINDEX(@SEPARADOR, REVERSE(CLD.VALOR_TEXT))),  --QUITAMOS LA PARTIDA PRES" & vbCrLf
sConsulta = sConsulta & "             1, " & vbCrLf
sConsulta = sConsulta & "             CASE WHEN " & vbCrLf
sConsulta = sConsulta & "               CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                         SUBSTRING(CLD.VALOR_TEXT, 1, LEN(CLD.VALOR_TEXT) - CHARINDEX(@SEPARADOR, REVERSE(CLD.VALOR_TEXT))), " & vbCrLf
sConsulta = sConsulta & "                         CHARINDEX(@SEPARADOR,CLD.VALOR_TEXT, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1)" & vbCrLf
sConsulta = sConsulta & "                         )=0 THEN " & vbCrLf
sConsulta = sConsulta & "                   LEN(SUBSTRING(CLD.VALOR_TEXT, 1, LEN(CLD.VALOR_TEXT) - CHARINDEX(@SEPARADOR, REVERSE(CLD.VALOR_TEXT))))" & vbCrLf
sConsulta = sConsulta & "               ELSE " & vbCrLf
sConsulta = sConsulta & "                   CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                             SUBSTRING(CLD.VALOR_TEXT, 1, LEN(CLD.VALOR_TEXT) - CHARINDEX(@SEPARADOR, REVERSE(CLD.VALOR_TEXT))), " & vbCrLf
sConsulta = sConsulta & "                             CHARINDEX(@SEPARADOR,CLD.VALOR_TEXT, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1))-1" & vbCrLf
sConsulta = sConsulta & "               END" & vbCrLf
sConsulta = sConsulta & "           )=@UON1 + @SEPARADOR + @UON2 + @SEPARADOR + @OLD" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "-- INSTANCIA_CENTROS_PARTIDAS" & vbCrLf
sConsulta = sConsulta & "--     CENTROS" & vbCrLf
sConsulta = sConsulta & "UPDATE ICP SET VALOR_TEXT = COALESCE(STUFF(ICP.VALOR_TEXT, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1, LEN(@OLD), @NEW) , VALOR_TEXT)" & vbCrLf
sConsulta = sConsulta & "FROM INSTANCIA_CENTROS_PARTIDAS ICP WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "ICP.TIPO_CAMPO_GS = 129 AND " & vbCrLf
sConsulta = sConsulta & "ICP.VALOR_TEXT IS NOT NULL AND" & vbCrLf
sConsulta = sConsulta & "SUBSTRING(ICP.VALOR_TEXT, " & vbCrLf
sConsulta = sConsulta & "         1, " & vbCrLf
sConsulta = sConsulta & "         CASE WHEN " & vbCrLf
sConsulta = sConsulta & "           CHARINDEX(@SEPARADOR,ICP.VALOR_TEXT, CHARINDEX(@SEPARADOR,ICP.VALOR_TEXT, CHARINDEX (@SEPARADOR,ICP.VALOR_TEXT) + 1) + 1)=0 THEN " & vbCrLf
sConsulta = sConsulta & "               LEN(ICP.VALOR_TEXT) " & vbCrLf
sConsulta = sConsulta & "           ELSE " & vbCrLf
sConsulta = sConsulta & "               CHARINDEX(@SEPARADOR,ICP.VALOR_TEXT, CHARINDEX(@SEPARADOR,ICP.VALOR_TEXT, CHARINDEX (@SEPARADOR,ICP.VALOR_TEXT) + 1) + 1)-1" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "       )=@UON1 + @SEPARADOR + @UON2 + @SEPARADOR + @OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--     PARTIDAS" & vbCrLf
sConsulta = sConsulta & "UPDATE ICP SET VALOR_TEXT = COALESCE(STUFF(ICP.VALOR_TEXT, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1, LEN(@OLD), @NEW) , VALOR_TEXT)" & vbCrLf
sConsulta = sConsulta & "FROM INSTANCIA_CENTROS_PARTIDAS ICP WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "ICP.TIPO_CAMPO_GS  = 130 AND " & vbCrLf
sConsulta = sConsulta & "ICP.VALOR_TEXT IS NOT NULL AND" & vbCrLf
sConsulta = sConsulta & "SUBSTRING(SUBSTRING(ICP.VALOR_TEXT, 1, LEN(ICP.VALOR_TEXT) - CHARINDEX(@SEPARADOR, REVERSE(ICP.VALOR_TEXT))),  --QUITAMOS LA PARTIDA PRES" & vbCrLf
sConsulta = sConsulta & "         1, " & vbCrLf
sConsulta = sConsulta & "         CASE WHEN " & vbCrLf
sConsulta = sConsulta & "           CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                     SUBSTRING(ICP.VALOR_TEXT, 1, LEN(ICP.VALOR_TEXT) - CHARINDEX(@SEPARADOR, REVERSE(ICP.VALOR_TEXT))), " & vbCrLf
sConsulta = sConsulta & "                     CHARINDEX(@SEPARADOR,ICP.VALOR_TEXT, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1)" & vbCrLf
sConsulta = sConsulta & "                     )=0 THEN " & vbCrLf
sConsulta = sConsulta & "               LEN(SUBSTRING(ICP.VALOR_TEXT, 1, LEN(ICP.VALOR_TEXT) - CHARINDEX(@SEPARADOR, REVERSE(ICP.VALOR_TEXT))))" & vbCrLf
sConsulta = sConsulta & "           ELSE " & vbCrLf
sConsulta = sConsulta & "               CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                         SUBSTRING(ICP.VALOR_TEXT, 1, LEN(ICP.VALOR_TEXT) - CHARINDEX(@SEPARADOR, REVERSE(ICP.VALOR_TEXT))), " & vbCrLf
sConsulta = sConsulta & "                         CHARINDEX(@SEPARADOR,ICP.VALOR_TEXT, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1))-1" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "       )=@UON1 + @SEPARADOR + @UON2 + @SEPARADOR + @OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- PM_CONTR_FILTRO_USU_BUSQUEDA" & vbCrLf
sConsulta = sConsulta & "UPDATE P SET CENTRO_COSTE = COALESCE(STUFF(CENTRO_COSTE, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1, LEN(@OLD), @NEW) , CENTRO_COSTE)" & vbCrLf
sConsulta = sConsulta & "FROM PM_CONTR_FILTRO_USU_BUSQUEDA P WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "P.CENTRO_COSTE IS NOT NULL AND" & vbCrLf
sConsulta = sConsulta & "SUBSTRING(P.CENTRO_COSTE, " & vbCrLf
sConsulta = sConsulta & "         1, " & vbCrLf
sConsulta = sConsulta & "         CASE WHEN " & vbCrLf
sConsulta = sConsulta & "           CHARINDEX(@SEPARADOR,P.CENTRO_COSTE, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1)=0 THEN " & vbCrLf
sConsulta = sConsulta & "               LEN(P.CENTRO_COSTE) " & vbCrLf
sConsulta = sConsulta & "           ELSE " & vbCrLf
sConsulta = sConsulta & "               CHARINDEX(@SEPARADOR,P.CENTRO_COSTE, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1)-1" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "       )=@UON1 + @SEPARADOR + @UON2 + @SEPARADOR + @OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE P SET PARTIDA1 = COALESCE(STUFF(PARTIDA1, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1, LEN(@OLD), @NEW) , PARTIDA1)" & vbCrLf
sConsulta = sConsulta & "FROM PM_CONTR_FILTRO_USU_BUSQUEDA P WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "P.PARTIDA1 IS NOT NULL AND" & vbCrLf
sConsulta = sConsulta & "SUBSTRING(SUBSTRING(P.PARTIDA1, 1, LEN(P.PARTIDA1) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA1))),  --QUITAMOS LA PARTIDA PRES" & vbCrLf
sConsulta = sConsulta & "         1, " & vbCrLf
sConsulta = sConsulta & "         CASE WHEN " & vbCrLf
sConsulta = sConsulta & "           CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                     SUBSTRING(P.PARTIDA1, 1, LEN(P.PARTIDA1) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA1))), " & vbCrLf
sConsulta = sConsulta & "                     CHARINDEX(@SEPARADOR,P.PARTIDA1, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1)" & vbCrLf
sConsulta = sConsulta & "                     )=0 THEN " & vbCrLf
sConsulta = sConsulta & "               LEN(SUBSTRING(P.PARTIDA1, 1, LEN(P.PARTIDA1) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA1))))" & vbCrLf
sConsulta = sConsulta & "           ELSE " & vbCrLf
sConsulta = sConsulta & "               CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                         SUBSTRING(P.PARTIDA1, 1, LEN(P.PARTIDA1) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA1))), " & vbCrLf
sConsulta = sConsulta & "                         CHARINDEX(@SEPARADOR,P.PARTIDA1, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1))-1" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "       )=@UON1 + @SEPARADOR + @UON2 + @SEPARADOR + @OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE P SET PARTIDA2 = COALESCE(STUFF(PARTIDA2, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1, LEN(@OLD), @NEW) , PARTIDA2)" & vbCrLf
sConsulta = sConsulta & "FROM PM_CONTR_FILTRO_USU_BUSQUEDA P WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "P.PARTIDA2 IS NOT NULL AND" & vbCrLf
sConsulta = sConsulta & "SUBSTRING(SUBSTRING(P.PARTIDA2, 1, LEN(P.PARTIDA2) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA2))),  --QUITAMOS LA PARTIDA PRES" & vbCrLf
sConsulta = sConsulta & "         1, " & vbCrLf
sConsulta = sConsulta & "         CASE WHEN " & vbCrLf
sConsulta = sConsulta & "           CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                     SUBSTRING(P.PARTIDA2, 1, LEN(P.PARTIDA2) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA2))), " & vbCrLf
sConsulta = sConsulta & "                     CHARINDEX(@SEPARADOR,P.PARTIDA2, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1)" & vbCrLf
sConsulta = sConsulta & "                     )=0 THEN " & vbCrLf
sConsulta = sConsulta & "               LEN(SUBSTRING(P.PARTIDA2, 1, LEN(P.PARTIDA2) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA2))))" & vbCrLf
sConsulta = sConsulta & "           ELSE " & vbCrLf
sConsulta = sConsulta & "               CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                         SUBSTRING(P.PARTIDA2, 1, LEN(P.PARTIDA2) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA2))), " & vbCrLf
sConsulta = sConsulta & "                         CHARINDEX(@SEPARADOR,P.PARTIDA2, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1))-1" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "       )=@UON1 + @SEPARADOR + @UON2 + @SEPARADOR + @OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE P SET PARTIDA3 = COALESCE(STUFF(PARTIDA3, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1, LEN(@OLD), @NEW) , PARTIDA3)" & vbCrLf
sConsulta = sConsulta & "FROM PM_CONTR_FILTRO_USU_BUSQUEDA P WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "P.PARTIDA3 IS NOT NULL AND" & vbCrLf
sConsulta = sConsulta & "SUBSTRING(SUBSTRING(P.PARTIDA3, 1, LEN(P.PARTIDA3) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA3))),  --QUITAMOS LA PARTIDA PRES" & vbCrLf
sConsulta = sConsulta & "         1, " & vbCrLf
sConsulta = sConsulta & "         CASE WHEN " & vbCrLf
sConsulta = sConsulta & "           CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                     SUBSTRING(P.PARTIDA3, 1, LEN(P.PARTIDA3) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA3))), " & vbCrLf
sConsulta = sConsulta & "                     CHARINDEX(@SEPARADOR,P.PARTIDA3, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1)" & vbCrLf
sConsulta = sConsulta & "                     )=0 THEN " & vbCrLf
sConsulta = sConsulta & "               LEN(SUBSTRING(P.PARTIDA3, 1, LEN(P.PARTIDA3) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA3))))" & vbCrLf
sConsulta = sConsulta & "           ELSE " & vbCrLf
sConsulta = sConsulta & "               CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                         SUBSTRING(P.PARTIDA3, 1, LEN(P.PARTIDA3) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA3))), " & vbCrLf
sConsulta = sConsulta & "                         CHARINDEX(@SEPARADOR,P.PARTIDA3, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1))-1" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "       )=@UON1 + @SEPARADOR + @UON2 + @SEPARADOR + @OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE P SET PARTIDA4 = COALESCE(STUFF(PARTIDA4, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1, LEN(@OLD), @NEW) , PARTIDA4)" & vbCrLf
sConsulta = sConsulta & "FROM PM_CONTR_FILTRO_USU_BUSQUEDA P WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "P.PARTIDA4 IS NOT NULL AND" & vbCrLf
sConsulta = sConsulta & "SUBSTRING(SUBSTRING(P.PARTIDA4, 1, LEN(P.PARTIDA4) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA4))),  --QUITAMOS LA PARTIDA PRES" & vbCrLf
sConsulta = sConsulta & "         1, " & vbCrLf
sConsulta = sConsulta & "         CASE WHEN " & vbCrLf
sConsulta = sConsulta & "           CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                     SUBSTRING(P.PARTIDA4, 1, LEN(P.PARTIDA4) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA4))), " & vbCrLf
sConsulta = sConsulta & "                     CHARINDEX(@SEPARADOR,P.PARTIDA4, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1)" & vbCrLf
sConsulta = sConsulta & "                     )=0 THEN " & vbCrLf
sConsulta = sConsulta & "               LEN(SUBSTRING(P.PARTIDA4, 1, LEN(P.PARTIDA4) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA4))))" & vbCrLf
sConsulta = sConsulta & "           ELSE " & vbCrLf
sConsulta = sConsulta & "               CHARINDEX(@SEPARADOR," & vbCrLf
sConsulta = sConsulta & "                         SUBSTRING(P.PARTIDA4, 1, LEN(P.PARTIDA4) - CHARINDEX(@SEPARADOR, REVERSE(P.PARTIDA4))), " & vbCrLf
sConsulta = sConsulta & "                         CHARINDEX(@SEPARADOR,P.PARTIDA4, LEN(@UON1 + @SEPARADOR + @UON2 + @SEPARADOR) + 1))-1" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "       )=@UON1 + @SEPARADOR + @UON2 + @SEPARADOR + @OLD" & vbCrLf
sConsulta = sConsulta & "--FIN ACTUALIZACION COPIA_CAMPO, COPIA_LINEA_DESGLOSE, INSTANCIA_CENTROS_PARTIDAS, PM_CONTR_FILTRO_USU_BUSQUEDA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3_DEP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PER CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3_DEST CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISOR CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CERTIFICADO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE NOCONFORMIDAD CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE APROBADORES CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PETICIONARIOS CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_ROL CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_ENLACE_CONDICIONES CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_COPIA_ROL CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_COPIA_ENLACE_CONDICION CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_UON CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU_CC_CONTROL CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU_CC_IMPUTACION CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES5_UON CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CENTRO_SM_UON CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON4_DEST CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_PED_IMPUTACION CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PERF_UON CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_CAT_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_CAT_UON3_DEP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_GRUPO_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_GRUPO_UON3_DEP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_MEN_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_MEN_UON3_DEP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CUSTOM_TEMPLATE_UON CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "CLOSE C" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "COMMIT TRANSACTION" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT OFF" & vbCrLf
ExecuteSQL gRDOCon, sConsulta



End Sub


Public Function CodigoDeActualizacion31900_08_2004_A31900_08_2005() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim oADOConnection As ADODB.Connection
    Dim sConsulta As String

    On Error GoTo Error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
                  
    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Actualiza Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    frmProgreso.ProgressBar1.Refresh
    V_31900_8_Storeds_205
    
    sConsulta = "UPDATE VERSION SET NUM ='3.00.20.005'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103), PRODVER='31900.8'"
    ExecuteSQL gRDOCon, sConsulta
        
    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False
        
    CodigoDeActualizacion31900_08_2004_A31900_08_2005 = True
    Exit Function

Error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    CodigoDeActualizacion31900_08_2004_A31900_08_2005 = False
End Function

Public Sub V_31900_8_Storeds_205()
Dim sConsulta As String
        
sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSGSW_DEVOLVER_DISTRIBUIDORESPROVEEDOR_ITEM]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSGSW_DEVOLVER_DISTRIBUIDORESPROVEEDOR_ITEM]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSGSW_DEVOLVER_DISTRIBUIDORESPROVEEDOR_ITEM] @PROVE VARCHAR(50),@IDEMP INT,@ANYO INT,@GMN1 NVARCHAR(20),@PROCE INT,@IDITEM INT,@UON1 NVARCHAR(20)=NULL,@UON2 NVARCHAR(20)=NULL,@UON3 NVARCHAR(20)=NULL AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT DISTRIB.ORDEN  ,DISTRIB.COD,DISTRIB.DEN,LIA.CANT_ADJ LIA_CANT_ADJ,IA.CANT_ADJ  * IT_EMP.PORCEN IA_CANT_ADJ,CASE WHEN LIA.PORCEN IS NOT NULL THEN LIA.PORCEN * 100 ELSE IA.PORCEN END PORCEN,DISTRIB.COD_ERP,DISTRIB.DEN_ERP,LIA.PROVE_ERP PROVE_ERP_SEL FROM" & vbCrLf
sConsulta = sConsulta & "((SELECT 0 ORDEN ,P.COD,P.DEN,ISNULL(PE.COD_ERP,'NULL') COD_ERP,ISNULL(PE.COD_ERP,'NULL') + ' - ' + PE.DEN_ERP DEN_ERP  FROM PROVE P WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PROVE_ERP PE WITH (NOLOCK) ON PE.NIF=P.NIF AND PE.EMPRESA=@IDEMP" & vbCrLf
sConsulta = sConsulta & " WHERE P.COD= @PROVE) DISTRIB" & vbCrLf
sConsulta = sConsulta & " LEFT JOIN (SELECT MAX(ID) ID,ANYO,GMN1_PROCE,PROCE,ID_ITEM,PROVE,ORIGEN   FROM LOG_ITEM_ADJ LIA  WITH (NOLOCK) GROUP BY ANYO,GMN1_PROCE,PROCE,ID_ITEM,PROVE,ORIGEN ) AS LIA1 ON LIA1.ANYO=@ANYO AND LIA1.GMN1_PROCE=@GMN1 " & vbCrLf
sConsulta = sConsulta & "AND LIA1.PROCE=@PROCE AND LIA1.ID_ITEM=@IDITEM AND LIA1.PROVE=DISTRIB.COD AND (LIA1.ORIGEN=0 OR LIA1.ORIGEN=2)" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN LOG_ITEM_ADJ LIA WITH (NOLOCK) ON LIA.ID=LIA1.ID" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN ITEM_ADJ IA WITH (NOLOCK) ON IA.ANYO=@ANYO AND IA.GMN1=@GMN1 AND IA.PROCE=@PROCE AND IA.ITEM=@IDITEM AND IA.PROVE=DISTRIB.COD" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN " & vbCrLf
sConsulta = sConsulta & "   (select distinct iu.item,iu.uon1,null uon2,null uon3,u.empresa,iu.porcen " & vbCrLf
sConsulta = sConsulta & "   from item_uon1 iu WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   inner join uon1 u WITH(NOLOCK) on iu.uon1 = u.cod where IU.ANYO=@ANYO AND IU.GMN1=@GMN1 AND IU.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   Union " & vbCrLf
sConsulta = sConsulta & "   select distinct iu.item,iu.uon1,IU.uon2 ,null uon3,ISNULL(u1.empresa,u2.empresa) as empresa,iu.porcen " & vbCrLf
sConsulta = sConsulta & "   from item_uon2 iu WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   inner join uon2 u2 WITH(NOLOCK) on iu.uon1=u2.uon1 and iu.uon2=u2.cod " & vbCrLf
sConsulta = sConsulta & "   inner join uon1 u1 WITH(NOLOCK) on u1.cod=u2.uon1 where IU.ANYO=@ANYO AND IU.GMN1=@GMN1 AND IU.PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "   Union select distinct iu.item,iu.uon1, IU.uon2 ,IU.uon3,ISNULL(u3.empresa,ISNULL(u2.empresa,u1.empresa)) as empresa,iu.porcen " & vbCrLf
sConsulta = sConsulta & "   from item_uon3 iu WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   inner join uon3 u3 WITH(NOLOCK) on iu.uon1=u3.uon1 and iu.uon2=u3.uon2 and iu.uon3=u3.cod " & vbCrLf
sConsulta = sConsulta & "   inner join uon2 u2 WITH(NOLOCK) on u3.uon1=u2.uon1 and u3.uon2=u2.cod " & vbCrLf
sConsulta = sConsulta & "   inner join uon1 u1 WITH(NOLOCK) on u1.cod=u3.uon1 where IU.ANYO=@ANYO AND IU.GMN1=@GMN1 AND IU.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   ) IT_EMP ON IT_EMP.ITEM=IA.ITEM AND IT_EMP.EMPRESA=@IDEMP AND IT_EMP.UON1=@UON1 AND IT_EMP.UON2=@UON2 AND IT_EMP.UON3=@UON3)" & vbCrLf
sConsulta = sConsulta & "UNION ALL" & vbCrLf
sConsulta = sConsulta & "SELECT DISTRIB.ORDEN  ,DISTRIB.COD,DISTRIB.DEN,LIA.CANT_ADJ LIA_CANT_ADJ,IA.CANT_ADJ  * IT_EMP.PORCEN IA_CANT_ADJ,CASE WHEN LIA.PORCEN IS NOT NULL THEN LIA.PORCEN * 100 ELSE IA.PORCEN END PORCEN,DISTRIB.COD_ERP,DISTRIB.DEN_ERP,LIA.PROVE_ERP PROVE_ERP_SEL FROM          " & vbCrLf
sConsulta = sConsulta & "(SELECT ROW_NUMBER() OVER(ORDER BY P.COD ASC) AS ORDEN,P.COD,P.DEN,ISNULL(PE.COD_ERP,'NULL') COD_ERP,ISNULL(PE.COD_ERP,'NULL') + ' - ' + PE.DEN_ERP DEN_ERP  FROM PROVE_REL PR WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "INNER JOIN GS_PROVETREL GP WITH (NOLOCK) ON PR.PROVETREL=GP.ID AND PR.PROVEP=@PROVE AND GP.PEDIDO=1" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PROVE P WITH (NOLOCK) ON P.COD=PR.PROVES" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PROVE_ERP PE WITH (NOLOCK) ON PE.NIF=P.NIF AND PE.EMPRESA=@IDEMP) DISTRIB" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN (SELECT MAX(ID) ID,ANYO,GMN1_PROCE,PROCE,ID_ITEM,PROVE,ORIGEN   FROM LOG_ITEM_ADJ LIA  WITH (NOLOCK) GROUP BY ANYO,GMN1_PROCE,PROCE,ID_ITEM,PROVE,ORIGEN ) AS LIA1 ON LIA1.ANYO=@ANYO AND LIA1.GMN1_PROCE=@GMN1 " & vbCrLf
sConsulta = sConsulta & "AND LIA1.PROCE=@PROCE AND LIA1.ID_ITEM=@IDITEM AND LIA1.PROVE=DISTRIB.COD AND (LIA1.ORIGEN=0 OR LIA1.ORIGEN=2)" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN LOG_ITEM_ADJ LIA WITH (NOLOCK) ON LIA.ID=LIA1.ID" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN ITEM_ADJ IA WITH (NOLOCK) ON IA.ANYO=@ANYO AND IA.GMN1=@GMN1 AND IA.PROCE=@PROCE AND IA.ITEM=@IDITEM AND IA.PROVE=DISTRIB.COD" & vbCrLf
sConsulta = sConsulta & " LEFT JOIN " & vbCrLf
sConsulta = sConsulta & "   (select distinct iu.item,iu.uon1,null uon2,null uon3,u.empresa,iu.porcen " & vbCrLf
sConsulta = sConsulta & "   from item_uon1 iu WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   inner join uon1 u WITH(NOLOCK) on iu.uon1 = u.cod where IU.ANYO=@ANYO AND IU.GMN1=@GMN1 AND IU.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   Union " & vbCrLf
sConsulta = sConsulta & "   select distinct iu.item,iu.uon1,IU.uon2 ,null uon3,ISNULL(u1.empresa,u2.empresa) as empresa,iu.porcen " & vbCrLf
sConsulta = sConsulta & "   from item_uon2 iu WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   inner join uon2 u2 WITH(NOLOCK) on iu.uon1=u2.uon1 and iu.uon2=u2.cod " & vbCrLf
sConsulta = sConsulta & "   inner join uon1 u1 WITH(NOLOCK) on u1.cod=u2.uon1 where IU.ANYO=@ANYO AND IU.GMN1=@GMN1 AND IU.PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "   Union select distinct iu.item,iu.uon1, IU.uon2 ,IU.uon3,ISNULL(u3.empresa,ISNULL(u2.empresa,u1.empresa)) as empresa,iu.porcen " & vbCrLf
sConsulta = sConsulta & "   from item_uon3 iu WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   inner join uon3 u3 WITH(NOLOCK) on iu.uon1=u3.uon1 and iu.uon2=u3.uon2 and iu.uon3=u3.cod " & vbCrLf
sConsulta = sConsulta & "   inner join uon2 u2 WITH(NOLOCK) on u3.uon1=u2.uon1 and u3.uon2=u2.cod " & vbCrLf
sConsulta = sConsulta & "   inner join uon1 u1 WITH(NOLOCK) on u1.cod=u3.uon1 where IU.ANYO=@ANYO AND IU.GMN1=@GMN1 AND IU.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   ) IT_EMP ON IT_EMP.ITEM=IA.ITEM AND IT_EMP.EMPRESA=@IDEMP AND IT_EMP.UON1=@UON1 AND IT_EMP.UON2=@UON2 AND IT_EMP.UON3=@UON3" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

        
        
sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSP_GET_PROCE_OFE]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSP_GET_PROCE_OFE]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta
    
sConsulta = "CREATE PROCEDURE[dbo].[FSP_GET_PROCE_OFE] @PROVE VARCHAR(50), @FECOFEDESDE DATETIME = NULL, @FECOFEHASTA DATETIME = NULL, @ANYO INT = NULL, @GMN1 VARCHAR(50) = NULL, @PROCE INT = NULL, @DEN_PROCE VARCHAR(200)=NULL, @COD_ART VARCHAR(50)=NULL, @DEN_ART VARCHAR(200)=NULL" & vbCrLf
sConsulta = sConsulta & " AS" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @COD_PROVE AS VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @FECOFEHASTA_MAS1 AS DATETIME " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @FECOFEHASTA_MAS1 = DATEADD(DD,1,@FECOFEHASTA)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @COD_PROVE= COD FROM PROVE WITH(NOLOCK) WHERE FSP_COD=@PROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL='SELECT DISTINCT (convert(varchar, P.ANYO) + ''/'' + P.GMN1 + ''/'' + convert(varchar, P.COD)) AS CODIGO,P.ANYO,P.GMN1,P.COD,P.DEN AS DESCRIPCION, PO.FECREC AS FECOFE,PO.OFE AS NUMOFE, ISNULL(AFD.NOM,'''') AS DETALLE " & vbCrLf
sConsulta = sConsulta & "FROM PROCE_OFE PO WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PROCE P WITH(NOLOCK) ON PO.ANYO=P.ANYO AND PO.GMN1=P.GMN1 AND PO.PROCE=P.COD" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN ADJUN_FIRMA_DIGITAL AFD WITH(NOLOCK) ON PO.ADJUN_FIRMA_DIGITAL=AFD.ID'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @COD_ART IS NOT NULL OR @DEN_ART IS NOT NULL" & vbCrLf
sConsulta = sConsulta & " BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + ' INNER JOIN ITEM_OFE IO WITH(NOLOCK) ON PO.ANYO=IO.ANYO AND PO.GMN1=IO.GMN1 AND PO.PROCE=IO.PROCE'" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + ' INNER JOIN ITEM I WITH(NOLOCK) ON IO.ANYO=I.ANYO AND IO.GMN1=I.GMN1_PROCE AND IO.PROCE=IO.PROCE AND IO.ITEM=I.ID'" & vbCrLf
sConsulta = sConsulta & " END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' WHERE PO.PROVE= @PROVE'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @FECOFEDESDE<>'' AND @FECOFEDESDE IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "  SET @SQL = @SQL + ' AND PO.FECREC >= @FECOFEDESDE'" & vbCrLf
sConsulta = sConsulta & "IF @FECOFEHASTA<>'' AND @FECOFEHASTA IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "  SET @SQL = @SQL + ' AND PO.FECREC < @FECOFEHASTA_MAS1'" & vbCrLf
sConsulta = sConsulta & "IF @ANYO IS NOT NULL AND @ANYO<>0" & vbCrLf
sConsulta = sConsulta & "  SET @SQL= @SQL + ' AND P.ANYO= @ANYO'" & vbCrLf
sConsulta = sConsulta & "IF @GMN1 IS NOT NULL AND @GMN1<>''" & vbCrLf
sConsulta = sConsulta & "  SET @SQL= @SQL + ' AND P.GMN1=@GMN1'" & vbCrLf
sConsulta = sConsulta & "IF @PROCE IS NOT NULL AND @PROCE<>0" & vbCrLf
sConsulta = sConsulta & "  SET @SQL= @SQL + ' AND P.COD=@PROCE'" & vbCrLf
sConsulta = sConsulta & "IF @DEN_PROCE IS NOT NULL AND @DEN_PROCE<>''" & vbCrLf
sConsulta = sConsulta & "  SET @SQL= @SQL + ' AND P.DEN LIKE ''%'' +  @DEN_PROCE + ''%'''" & vbCrLf
sConsulta = sConsulta & "IF @COD_ART IS NOT NULL AND @COD_ART<>''" & vbCrLf
sConsulta = sConsulta & "  SET @SQL= @SQL + ' AND I.ART=@COD_ART'" & vbCrLf
sConsulta = sConsulta & "IF @DEN_ART IS NOT NULL AND @DEN_ART<>''" & vbCrLf
sConsulta = sConsulta & "  SET @SQL= @SQL + ' AND I.DESCR LIKE ''%'' +  @DEN_ART + ''%'''" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "SET @SQL= @SQL + ' ORDER BY P.ANYO,P.GMN1,P.COD'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, " & vbCrLf
sConsulta = sConsulta & "                      N'@PROVE NVARCHAR(50), @FECOFEDESDE DATETIME, @FECOFEHASTA_MAS1 DATETIME, @ANYO INT, @GMN1 VARCHAR(50), @PROCE INT, @DEN_PROCE VARCHAR(200),@COD_ART VARCHAR(50),@DEN_ART VARCHAR(200)'," & vbCrLf
sConsulta = sConsulta & "                      @PROVE=@COD_PROVE," & vbCrLf
sConsulta = sConsulta & "                      @FECOFEDESDE=@FECOFEDESDE," & vbCrLf
sConsulta = sConsulta & "                      @FECOFEHASTA_MAS1=@FECOFEHASTA_MAS1," & vbCrLf
sConsulta = sConsulta & "                      @ANYO=@ANYO," & vbCrLf
sConsulta = sConsulta & "                      @GMN1=@GMN1," & vbCrLf
sConsulta = sConsulta & "                      @PROCE=@PROCE," & vbCrLf
sConsulta = sConsulta & "                      @DEN_PROCE=@DEN_PROCE," & vbCrLf
sConsulta = sConsulta & "                      @COD_ART=@COD_ART," & vbCrLf
sConsulta = sConsulta & "                      @DEN_ART=@DEN_ART" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


End Sub
    

Public Function CodigoDeActualizacion31900_08_2005_A31900_08_2006() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim oADOConnection As ADODB.Connection
    Dim sConsulta As String

    On Error GoTo Error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
                  
    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Actualiza Tablas"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    frmProgreso.ProgressBar1.Refresh
    V_31900_8_Tablas_206
    
    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Actualiza Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    frmProgreso.ProgressBar1.Refresh
    V_31900_8_Storeds_206
    
    sConsulta = "UPDATE VERSION SET NUM ='3.00.20.006'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103), PRODVER='31900.8'"
    ExecuteSQL gRDOCon, sConsulta
        
    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False
        
    CodigoDeActualizacion31900_08_2005_A31900_08_2006 = True
    Exit Function

Error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    CodigoDeActualizacion31900_08_2005_A31900_08_2006 = False
End Function

Public Sub V_31900_8_Storeds_206()
Dim sConsulta As String
        
sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_ESTADISTICAS_ENTORNO_SEMANA]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_ESTADISTICAS_ENTORNO_SEMANA]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSAL_ESTADISTICAS_ENTORNO_SEMANA]  " & vbCrLf
sConsulta = sConsulta & "   @Dia DATETIME, -- Primer dia de la semana" & vbCrLf
sConsulta = sConsulta & "   @entorno nvarchar(50)," & vbCrLf
sConsulta = sConsulta & "   @pyme nvarchar(50)" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- Ahora se cargan los usuarios activos por Semana basandose en el m�ximo de usuarios de cada bloque de D�a" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "Declare @TOTAL_GS as int" & vbCrLf
sConsulta = sConsulta & "Declare @TOTAL_WEB as int" & vbCrLf
sConsulta = sConsulta & "Declare @TOTAL_PORTAL as int" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "select @TOTAL_GS = count(distinct USU) from fsal_usuarios_dis_semana WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "where anyo = datepart(""yy"",@DIA)" & vbCrLf
sConsulta = sConsulta & "   and mes = datepart(""mm"",@DIA)" & vbCrLf
sConsulta = sConsulta & "   and dia = datepart(""dd"",@Dia)" & vbCrLf
sConsulta = sConsulta & "   and entorno = @entorno" & vbCrLf
sConsulta = sConsulta & "   and isnull(pyme,'') =isnull(@pyme,'')" & vbCrLf
sConsulta = sConsulta & "   and producto = 'FULLSTEP GS'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "select @TOTAL_WEB = count(distinct USU) from fsal_usuarios_dis_semana WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "where anyo = datepart(""yy"",@DIA)" & vbCrLf
sConsulta = sConsulta & "   and mes = datepart(""mm"",@DIA)" & vbCrLf
sConsulta = sConsulta & "   and dia = datepart(""dd"",@Dia)" & vbCrLf
sConsulta = sConsulta & "   and entorno = @entorno" & vbCrLf
sConsulta = sConsulta & "   and isnull(pyme,'') =isnull(@pyme,'')" & vbCrLf
sConsulta = sConsulta & "   and producto = 'FULLSTEP WEB'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "select @TOTAL_PORTAL = count(distinct USU) from fsal_usuarios_dis_semana WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "where anyo = datepart(""yy"",@DIA)" & vbCrLf
sConsulta = sConsulta & "   and mes = datepart(""mm"",@DIA)" & vbCrLf
sConsulta = sConsulta & "   and dia = datepart(""dd"",@Dia)" & vbCrLf
sConsulta = sConsulta & "   and entorno = @entorno" & vbCrLf
sConsulta = sConsulta & "   and isnull(pyme,'') =isnull(@pyme,'')" & vbCrLf
sConsulta = sConsulta & "   and producto = 'FULLSTEP PORTAL'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO FSAL_USUARIOS_SEMANA (FECHA,ANYO,MES,DIA,ENTORNO,PYME,TOTAL_GS,TOTAL_WEB,TOTAL_PORTAL,TRASPASO) " & vbCrLf
sConsulta = sConsulta & "    SELECT  Convert(datetime,convert(varchar(10),@Dia,121)) as FECHA" & vbCrLf
sConsulta = sConsulta & "           ,DATEPART(""yy"",@Dia) as ANYO" & vbCrLf
sConsulta = sConsulta & "           ,DATEPART(""mm"",@Dia) as MES" & vbCrLf
sConsulta = sConsulta & "           ,DATEPART(""dd"",@Dia) as DIA" & vbCrLf
sConsulta = sConsulta & "           ,@entorno as ENTORNO" & vbCrLf
sConsulta = sConsulta & "           ,@PYME as PYME" & vbCrLf
sConsulta = sConsulta & "           ,@TOTAL_GS as TOTAL_GS" & vbCrLf
sConsulta = sConsulta & "           ,@TOTAL_WEB as TOTAL_WEB" & vbCrLf
sConsulta = sConsulta & "           ,@TOTAL_PORTAL as TOTAL_PORTAL" & vbCrLf
sConsulta = sConsulta & "           ,0 as TRASPASO" & vbCrLf
sConsulta = sConsulta & "    WHERE (@TOTAL_GS > 0 OR @TOTAL_WEB > 0 or @TOTAL_PORTAL > 0)" & vbCrLf
sConsulta = sConsulta & "       AND NOT EXISTS(" & vbCrLf
sConsulta = sConsulta & "           SELECT FECHA FROM FSAL_USUARIOS_SEMANA WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "               WHERE FECHA = Convert(datetime,convert(varchar(10),@Dia,121)) AND ENTORNO= @Entorno and isnull(PYME,'') = ISNULL(@PYME,'')" & vbCrLf
sConsulta = sConsulta & "       )" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- Y la update en caso de existir" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   UPDATE FSAL_USUARIOS_SEMANA " & vbCrLf
sConsulta = sConsulta & "   set " & vbCrLf
sConsulta = sConsulta & "       TOTAL_GS = REGISTROS_A_ACTUALIZAR.TOTAL_GS" & vbCrLf
sConsulta = sConsulta & "       ,TOTAL_WEB = REGISTROS_A_ACTUALIZAR.TOTAL_WEB" & vbCrLf
sConsulta = sConsulta & "       ,TOTAL_PORTAL = REGISTROS_A_ACTUALIZAR.TOTAL_PORTAL" & vbCrLf
sConsulta = sConsulta & "       ,TRASPASO = 0" & vbCrLf
sConsulta = sConsulta & "    from " & vbCrLf
sConsulta = sConsulta & "   FSAL_USUARIOS_SEMANA with(nolock)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN" & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "    SELECT  Convert(datetime,convert(varchar(10),@Dia,121)) as FECHA" & vbCrLf
sConsulta = sConsulta & "           ,DATEPART(""yy"",@Dia) as ANYO" & vbCrLf
sConsulta = sConsulta & "           ,DATEPART(""mm"",@Dia) as MES" & vbCrLf
sConsulta = sConsulta & "           ,DATEPART(""dd"",@Dia) as DIA" & vbCrLf
sConsulta = sConsulta & "           ,@entorno as ENTORNO" & vbCrLf
sConsulta = sConsulta & "           ,@PYME as PYME" & vbCrLf
sConsulta = sConsulta & "           ,@TOTAL_GS as TOTAL_GS" & vbCrLf
sConsulta = sConsulta & "           ,@TOTAL_WEB as TOTAL_WEB" & vbCrLf
sConsulta = sConsulta & "           ,@TOTAL_PORTAL as TOTAL_PORTAL" & vbCrLf
sConsulta = sConsulta & "           ,0 as TRASPASO" & vbCrLf
sConsulta = sConsulta & "    WHERE (@TOTAL_GS > 0 OR @TOTAL_WEB > 0 or @TOTAL_PORTAL > 0)" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "   )REGISTROS_A_ACTUALIZAR" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   ON  FSAL_USUARIOS_SEMANA.FECHA  =  REGISTROS_A_ACTUALIZAR.FECHA" & vbCrLf
sConsulta = sConsulta & "   AND FSAL_USUARIOS_SEMANA.ENTORNO  =  REGISTROS_A_ACTUALIZAR.ENTORNO" & vbCrLf
sConsulta = sConsulta & "   AND isnull(FSAL_USUARIOS_SEMANA.PYME,'')  =  isnull(REGISTROS_A_ACTUALIZAR.PYME,'')" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_ESTADISTICAS_ENTORNO_HORA]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_ESTADISTICAS_ENTORNO_HORA]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

            
sConsulta = "CREATE PROCEDURE [dbo].[FSAL_ESTADISTICAS_ENTORNO_HORA]  " & vbCrLf
sConsulta = sConsulta & "   @Desde DATETIME," & vbCrLf
sConsulta = sConsulta & "   @Hasta DATETIME," & vbCrLf
sConsulta = sConsulta & "   @entorno nvarchar(50)," & vbCrLf
sConsulta = sConsulta & "   @pyme nvarchar(50)" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "Declare @HoraSig as datetime" & vbCrLf
sConsulta = sConsulta & "set @Horasig = dateadd(hour,1,@Desde);" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if (@Horasig > @hasta) begin set @horasig = @hasta end;" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- COMMON TABLE EXPRESSION para obtener todas las entradas (bloques de 5 min) entre las fechas-horas indicadas" & vbCrLf
sConsulta = sConsulta & "-- OJO! Esta CTE s�lo admite 100 pasos de iteraci�n/Recursi�n, pero se supone que es v�lida ya que son se van" & vbCrLf
sConsulta = sConsulta & "-- a consultar fragmentos de 1 hora de duracci�n (12 bloques de 5 min)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "WITH cteMinutos as" & vbCrLf
sConsulta = sConsulta & "( " & vbCrLf
sConsulta = sConsulta & "    SELECT @Desde  AS MisMinutos" & vbCrLf
sConsulta = sConsulta & "    UNION ALL" & vbCrLf
sConsulta = sConsulta & "    SELECT dateadd(MINUTE,5,MisMinutos)" & vbCrLf
sConsulta = sConsulta & "    FROM cteMinutos  -- SIN with(Nolock), es una CTE" & vbCrLf
sConsulta = sConsulta & "    WHERE dateadd(minute,5,MisMinutos) < @HoraSig" & vbCrLf
sConsulta = sConsulta & ")" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO FSAL_USUARIOS_MINUTO (FECHA,ANYO,Mes,Dia,Hora,Min,Entorno,Pyme,ACTIVOS_GS,ACTIVOS_WEB,ACTIVOS_PORTAL,TRASPASO)" & vbCrLf
sConsulta = sConsulta & "SELECT " & vbCrLf
sConsulta = sConsulta & "   MisMinutos as FECHA" & vbCrLf
sConsulta = sConsulta & "   ,DATEPART(""yy"",MisMinutos) as ANYO" & vbCrLf
sConsulta = sConsulta & "   ,DATEPART(""mm"",MisMinutos) as MES" & vbCrLf
sConsulta = sConsulta & "   ,DATEPART(""dd"",MisMinutos) as DIA" & vbCrLf
sConsulta = sConsulta & "   ,DATEPART(""hh"",MisMinutos) as HORA  " & vbCrLf
sConsulta = sConsulta & "   ,right(replicate('0', 2) + ltrim(CAST((((60*DATEPART(""hh"",MisMinutos)+DATEPART(""Mi"",MisMinutos)) / 5) * 5) % 60 AS varchar)), 2) as MIN" & vbCrLf
sConsulta = sConsulta & "   ,@entorno as ENTORNO" & vbCrLf
sConsulta = sConsulta & "   ,ISNULL(@pyme,'') as PYME" & vbCrLf
sConsulta = sConsulta & "   ,ISNULL(NUM_USUARIOS_GS,0) as ACTIVOS_GS" & vbCrLf
sConsulta = sConsulta & "   ,ISNULL(NUM_USUARIOS_WEB,0) as ACTIVOS_WEB" & vbCrLf
sConsulta = sConsulta & "   ,ISNULL(NUM_USUARIOS_PORTAL,0) as ACTIVOS_PORTAL" & vbCrLf
sConsulta = sConsulta & "   ,0 as traspaso" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "FROM cteMinutos -- Sin NOLOCK, es una CTE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "left JOIN " & vbCrLf
sConsulta = sConsulta & "   (  -- Usuarios de PORTAL" & vbCrLf
sConsulta = sConsulta & "     Select right(replicate('0', 2) + ltrim(cast((((60*DATEPART(""hh"",FISERVER)+DATEPART(""Mi"",FISERVER)) / 5) * 5) / 60 as varchar)), 2)  + ':' + " & vbCrLf
sConsulta = sConsulta & "     right(replicate('0', 2) + ltrim(CAST((((60*DATEPART(""hh"",FISERVER)+DATEPART(""Mi"",FISERVER)) / 5) * 5) % 60 AS varchar)), 2) as HORA_ACCESO " & vbCrLf
sConsulta = sConsulta & "   ,COUNT(DISTINCT USUARIO) as NUM_USUARIOS_PORTAL" & vbCrLf
sConsulta = sConsulta & "   from fsal_accesos WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   WHERE FISERVER < @HoraSig AND FISERVER > @Desde AND PRODUCTO = 'FULLSTEP PORTAL' AND FSAL_ENTORNO = @entorno AND isnull(FSAL_PYME,'') = isnull(@pyme,'')" & vbCrLf
sConsulta = sConsulta & "   GROUP BY right(replicate('0', 2) + ltrim(cast((((60*DATEPART(""hh"",FISERVER)+DATEPART(""Mi"",FISERVER)) / 5) * 5) / 60 as varchar)), 2)  + ':' + " & vbCrLf
sConsulta = sConsulta & "   right(replicate('0', 2) + ltrim(CAST((((60*DATEPART(""hh"",FISERVER)+DATEPART(""Mi"",FISERVER)) / 5) * 5) % 60 AS varchar)), 2) ) as fsal_horas_portal " & vbCrLf
sConsulta = sConsulta & "   on right(replicate('0', 2) + ltrim(cast((((60*DATEPART(""hh"",MisMinutos)+ DATEPART(""Mi"",MisMinutos)) / 5) * 5) /60 as varchar)), 2)+ ':' + " & vbCrLf
sConsulta = sConsulta & "   right(replicate('0', 2) + ltrim(CAST((((60*DATEPART(""hh"",MisMinutos)+DATEPART(""Mi"",MisMinutos)) / 5) * 5) % 60 AS varchar)), 2)=fsal_horas_portal.HORA_ACCESO " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "left JOIN ( -- Usuarios de FSNWEB" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   Select right(replicate('0', 2) + ltrim(cast((((60*DATEPART(""hh"",FISERVER)+DATEPART(""Mi"",FISERVER)) / 5) * 5) / 60 as varchar)), 2)  + ':' + " & vbCrLf
sConsulta = sConsulta & "   right(replicate('0', 2) + ltrim(CAST((((60*DATEPART(""hh"",FISERVER)+DATEPART(""Mi"",FISERVER)) / 5) * 5) % 60 AS varchar)), 2) as HORA_ACCESO " & vbCrLf
sConsulta = sConsulta & "   ,COUNT(DISTINCT USUARIO) as NUM_USUARIOS_WEB" & vbCrLf
sConsulta = sConsulta & "   from fsal_accesos WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   WHERE FISERVER < @HoraSig AND FISERVER > @Desde AND PRODUCTO = 'FULLSTEP WEB' AND FSAL_ENTORNO=@entorno AND isnull(FSAL_PYME,'') = isnull(@pyme,'')" & vbCrLf
sConsulta = sConsulta & "   GROUP BY right(replicate('0', 2) + ltrim(cast((((60*DATEPART(""hh"",FISERVER)+DATEPART(""Mi"",FISERVER)) / 5) * 5) / 60 as varchar)), 2)  + ':' + " & vbCrLf
sConsulta = sConsulta & "   right(replicate('0', 2) + ltrim(CAST((((60*DATEPART(""hh"",FISERVER)+DATEPART(""Mi"",FISERVER)) / 5) * 5) % 60 AS varchar)), 2) ) as fsal_horas_web " & vbCrLf
sConsulta = sConsulta & "   on right(replicate('0', 2) + ltrim(cast((((60*DATEPART(""hh"",MisMinutos)+ DATEPART(""Mi"",MisMinutos)) / 5) * 5) /60 as varchar)), 2)+ ':' + " & vbCrLf
sConsulta = sConsulta & "   right(replicate('0', 2) + ltrim(CAST((((60*DATEPART(""hh"",MisMinutos)+DATEPART(""Mi"",MisMinutos)) / 5) * 5) % 60 AS varchar)), 2)=fsal_horas_web.HORA_ACCESO " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "left JOIN (  -- Usuarios de GS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   Select right(replicate('0', 2) + ltrim(cast((((60*DATEPART(""hh"",FISERVER)+DATEPART(""Mi"",FISERVER)) / 5) * 5) / 60 as varchar)), 2)  + ':' + " & vbCrLf
sConsulta = sConsulta & "   right(replicate('0', 2) + ltrim(CAST((((60*DATEPART(""hh"",FISERVER)+DATEPART(""Mi"",FISERVER)) / 5) * 5) % 60 AS varchar)), 2) as HORA_ACCESO " & vbCrLf
sConsulta = sConsulta & "   ,COUNT(DISTINCT USUARIO) as NUM_USUARIOS_GS" & vbCrLf
sConsulta = sConsulta & "   from fsal_accesos WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   WHERE FISERVER < @HoraSig AND FISERVER > @Desde AND PRODUCTO = 'FULLSTEP GS' AND FSAL_ENTORNO=@entorno AND isnull(FSAL_PYME,'') = isnull(@pyme,'')" & vbCrLf
sConsulta = sConsulta & "   GROUP BY right(replicate('0', 2) + ltrim(cast((((60*DATEPART(""hh"",FISERVER)+DATEPART(""Mi"",FISERVER)) / 5) * 5) / 60 as varchar)), 2)  + ':' + " & vbCrLf
sConsulta = sConsulta & "   right(replicate('0', 2) + ltrim(CAST((((60*DATEPART(""hh"",FISERVER)+DATEPART(""Mi"",FISERVER)) / 5) * 5) % 60 AS varchar)), 2) ) as fsal_horas_gs " & vbCrLf
sConsulta = sConsulta & "   on right(replicate('0', 2) + ltrim(cast((((60*DATEPART(""hh"",MisMinutos)+ DATEPART(""Mi"",MisMinutos)) / 5) * 5) /60 as varchar)), 2)+ ':' + " & vbCrLf
sConsulta = sConsulta & "   right(replicate('0', 2) + ltrim(CAST((((60*DATEPART(""hh"",MisMinutos)+DATEPART(""Mi"",MisMinutos)) / 5) * 5) % 60 AS varchar)), 2)=fsal_horas_gs.HORA_ACCESO " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "WHERE   ( ISNULL(NUM_USUARIOS_GS,0) >0 " & vbCrLf
sConsulta = sConsulta & "       or ISNULL(NUM_USUARIOS_WEB,0) > 0" & vbCrLf
sConsulta = sConsulta & "       or ISNULL(NUM_USUARIOS_PORTAL,0) >0 )" & vbCrLf
sConsulta = sConsulta & "       and not exists" & vbCrLf
sConsulta = sConsulta & "       (" & vbCrLf
sConsulta = sConsulta & "       SELECT FECHA FROM FSAL_USUARIOS_MINUTO WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "           WHERE FECHA = MisMinutos" & vbCrLf
sConsulta = sConsulta & "           AND Entorno = @entorno" & vbCrLf
sConsulta = sConsulta & "           AND isnull(Pyme,'') = ISNULL(@pyme,'')" & vbCrLf
sConsulta = sConsulta & "       );" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- Update para actualizar los registros que ya existan" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "WITH cteMinutos as" & vbCrLf
sConsulta = sConsulta & "( " & vbCrLf
sConsulta = sConsulta & "    SELECT @Desde  AS MisMinutos" & vbCrLf
sConsulta = sConsulta & "    UNION ALL" & vbCrLf
sConsulta = sConsulta & "    SELECT dateadd(MINUTE,5,MisMinutos)" & vbCrLf
sConsulta = sConsulta & "    FROM cteMinutos -- Sin witH(nolock) es una CTE" & vbCrLf
sConsulta = sConsulta & "    WHERE dateadd(minute,5,MisMinutos) < @HoraSig" & vbCrLf
sConsulta = sConsulta & ")" & vbCrLf
sConsulta = sConsulta & "UPDATE FSAL_USUARIOS_MINUTO " & vbCrLf
sConsulta = sConsulta & "   SET ACTIVOS_GS = REGISTROS_A_ACTUALIZAR.activos_GS" & vbCrLf
sConsulta = sConsulta & "   ,ACTIVOS_WEB = REGISTROS_A_ACTUALIZAR.activos_web" & vbCrLf
sConsulta = sConsulta & "   ,ACTIVOS_PORTAL = REGISTROS_A_ACTUALIZAR.activos_portal" & vbCrLf
sConsulta = sConsulta & "   ,traspaso=0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "FROM FSAL_USUARIOS_MINUTO WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN (" & vbCrLf
sConsulta = sConsulta & "SELECT " & vbCrLf
sConsulta = sConsulta & "   MisMinutos as FECHA" & vbCrLf
sConsulta = sConsulta & "   ,DATEPART(""yy"",MisMinutos) as ANYO" & vbCrLf
sConsulta = sConsulta & "   ,DATEPART(""mm"",MisMinutos) as MES" & vbCrLf
sConsulta = sConsulta & "   ,DATEPART(""dd"",MisMinutos) as DIA" & vbCrLf
sConsulta = sConsulta & "   ,DATEPART(""hh"",MisMinutos) as HORA  " & vbCrLf
sConsulta = sConsulta & "   ,right(replicate('0', 2) + ltrim(CAST((((60*DATEPART(""hh"",MisMinutos)+DATEPART(""Mi"",MisMinutos)) / 5) * 5) % 60 AS varchar)), 2) as MIN" & vbCrLf
sConsulta = sConsulta & "   ,@entorno as ENTORNO" & vbCrLf
sConsulta = sConsulta & "   ,ISNULL(@pyme,'') as PYME" & vbCrLf
sConsulta = sConsulta & "   ,ISNULL(NUM_USUARIOS_GS,0) as ACTIVOS_GS" & vbCrLf
sConsulta = sConsulta & "   ,ISNULL(NUM_USUARIOS_WEB,0) as ACTIVOS_WEB" & vbCrLf
sConsulta = sConsulta & "   ,ISNULL(NUM_USUARIOS_PORTAL,0) as ACTIVOS_PORTAL" & vbCrLf
sConsulta = sConsulta & "   ,0 as traspaso" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "FROM cteMinutos -- sin WITH(NOLOCK) es una CTE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "left JOIN " & vbCrLf
sConsulta = sConsulta & "   (  -- Usuarios de PORTAL" & vbCrLf
sConsulta = sConsulta & "     Select right(replicate('0', 2) + ltrim(cast((((60*DATEPART(""hh"",FISERVER)+DATEPART(""Mi"",FISERVER)) / 5) * 5) / 60 as varchar)), 2)  + ':' + " & vbCrLf
sConsulta = sConsulta & "     right(replicate('0', 2) + ltrim(CAST((((60*DATEPART(""hh"",FISERVER)+DATEPART(""Mi"",FISERVER)) / 5) * 5) % 60 AS varchar)), 2) as HORA_ACCESO " & vbCrLf
sConsulta = sConsulta & "   ,COUNT(DISTINCT USUARIO) as NUM_USUARIOS_PORTAL" & vbCrLf
sConsulta = sConsulta & "   from fsal_accesos WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   WHERE FISERVER < @HoraSig AND FISERVER > @Desde AND PRODUCTO = 'FULLSTEP PORTAL' AND FSAL_ENTORNO = @entorno AND isnull(FSAL_PYME,'') = isnull(@pyme,'')" & vbCrLf
sConsulta = sConsulta & "   GROUP BY right(replicate('0', 2) + ltrim(cast((((60*DATEPART(""hh"",FISERVER)+DATEPART(""Mi"",FISERVER)) / 5) * 5) / 60 as varchar)), 2)  + ':' + " & vbCrLf
sConsulta = sConsulta & "   right(replicate('0', 2) + ltrim(CAST((((60*DATEPART(""hh"",FISERVER)+DATEPART(""Mi"",FISERVER)) / 5) * 5) % 60 AS varchar)), 2) ) as fsal_horas_portal " & vbCrLf
sConsulta = sConsulta & "   on right(replicate('0', 2) + ltrim(cast((((60*DATEPART(""hh"",MisMinutos)+ DATEPART(""Mi"",MisMinutos)) / 5) * 5) /60 as varchar)), 2)+ ':' + " & vbCrLf
sConsulta = sConsulta & "   right(replicate('0', 2) + ltrim(CAST((((60*DATEPART(""hh"",MisMinutos)+DATEPART(""Mi"",MisMinutos)) / 5) * 5) % 60 AS varchar)), 2)=fsal_horas_portal.HORA_ACCESO " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "left JOIN ( -- Usuarios de FSNWEB" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   Select right(replicate('0', 2) + ltrim(cast((((60*DATEPART(""hh"",FISERVER)+DATEPART(""Mi"",FISERVER)) / 5) * 5) / 60 as varchar)), 2)  + ':' + " & vbCrLf
sConsulta = sConsulta & "   right(replicate('0', 2) + ltrim(CAST((((60*DATEPART(""hh"",FISERVER)+DATEPART(""Mi"",FISERVER)) / 5) * 5) % 60 AS varchar)), 2) as HORA_ACCESO " & vbCrLf
sConsulta = sConsulta & "   ,COUNT(DISTINCT USUARIO) as NUM_USUARIOS_WEB" & vbCrLf
sConsulta = sConsulta & "   from fsal_accesos WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   WHERE FISERVER < @HoraSig AND FISERVER > @Desde AND PRODUCTO = 'FULLSTEP WEB' AND FSAL_ENTORNO=@entorno AND isnull(FSAL_PYME,'') = isnull(@pyme,'')" & vbCrLf
sConsulta = sConsulta & "   GROUP BY right(replicate('0', 2) + ltrim(cast((((60*DATEPART(""hh"",FISERVER)+DATEPART(""Mi"",FISERVER)) / 5) * 5) / 60 as varchar)), 2)  + ':' + " & vbCrLf
sConsulta = sConsulta & "   right(replicate('0', 2) + ltrim(CAST((((60*DATEPART(""hh"",FISERVER)+DATEPART(""Mi"",FISERVER)) / 5) * 5) % 60 AS varchar)), 2) ) as fsal_horas_web " & vbCrLf
sConsulta = sConsulta & "   on right(replicate('0', 2) + ltrim(cast((((60*DATEPART(""hh"",MisMinutos)+ DATEPART(""Mi"",MisMinutos)) / 5) * 5) /60 as varchar)), 2)+ ':' + " & vbCrLf
sConsulta = sConsulta & "   right(replicate('0', 2) + ltrim(CAST((((60*DATEPART(""hh"",MisMinutos)+DATEPART(""Mi"",MisMinutos)) / 5) * 5) % 60 AS varchar)), 2)=fsal_horas_web.HORA_ACCESO " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "left JOIN (  -- Usuarios de GS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   Select right(replicate('0', 2) + ltrim(cast((((60*DATEPART(""hh"",FISERVER)+DATEPART(""Mi"",FISERVER)) / 5) * 5) / 60 as varchar)), 2)  + ':' + " & vbCrLf
sConsulta = sConsulta & "   right(replicate('0', 2) + ltrim(CAST((((60*DATEPART(""hh"",FISERVER)+DATEPART(""Mi"",FISERVER)) / 5) * 5) % 60 AS varchar)), 2) as HORA_ACCESO " & vbCrLf
sConsulta = sConsulta & "   ,COUNT(DISTINCT USUARIO) as NUM_USUARIOS_GS" & vbCrLf
sConsulta = sConsulta & "   from fsal_accesos WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   WHERE FISERVER < @HoraSig AND FISERVER > @Desde AND PRODUCTO = 'FULLSTEP GS' AND FSAL_ENTORNO=@entorno AND isnull(FSAL_PYME,'') = isnull(@pyme,'')" & vbCrLf
sConsulta = sConsulta & "   GROUP BY right(replicate('0', 2) + ltrim(cast((((60*DATEPART(""hh"",FISERVER)+DATEPART(""Mi"",FISERVER)) / 5) * 5) / 60 as varchar)), 2)  + ':' + " & vbCrLf
sConsulta = sConsulta & "   right(replicate('0', 2) + ltrim(CAST((((60*DATEPART(""hh"",FISERVER)+DATEPART(""Mi"",FISERVER)) / 5) * 5) % 60 AS varchar)), 2) ) as fsal_horas_gs " & vbCrLf
sConsulta = sConsulta & "   on right(replicate('0', 2) + ltrim(cast((((60*DATEPART(""hh"",MisMinutos)+ DATEPART(""Mi"",MisMinutos)) / 5) * 5) /60 as varchar)), 2)+ ':' + " & vbCrLf
sConsulta = sConsulta & "   right(replicate('0', 2) + ltrim(CAST((((60*DATEPART(""hh"",MisMinutos)+DATEPART(""Mi"",MisMinutos)) / 5) * 5) % 60 AS varchar)), 2)=fsal_horas_gs.HORA_ACCESO " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "WHERE    ISNULL(NUM_USUARIOS_GS,0) >0 " & vbCrLf
sConsulta = sConsulta & "       or ISNULL(NUM_USUARIOS_WEB,0) > 0" & vbCrLf
sConsulta = sConsulta & "       or ISNULL(NUM_USUARIOS_PORTAL,0) >0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & ") REGISTROS_A_ACTUALIZAR" & vbCrLf
sConsulta = sConsulta & "ON FSAL_USUARIOS_MINUTO.fecha = REGISTROS_A_ACTUALIZAR.FECHA" & vbCrLf
sConsulta = sConsulta & "   and FSAL_USUARIOS_MINUTO.entorno = REGISTROS_A_ACTUALIZAR.entorno" & vbCrLf
sConsulta = sConsulta & "   and isnull(FSAL_USUARIOS_MINUTO.pyme,'') = isnull(REGISTROS_A_ACTUALIZAR.pyme,'')" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- Se cargan los usuarios activos por horas basandose en el m�ximo de usuarios de cada bloque de 5 minutos" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   Declare @DIA datetime" & vbCrLf
sConsulta = sConsulta & "   set @DIA = convert(datetime,convert(varchar(10),@Desde,102))" & vbCrLf
sConsulta = sConsulta & "              " & vbCrLf
sConsulta = sConsulta & "   INSERT INTO FSAL_USUARIOS_HORA (FECHA,ANYO,MES,DIA,HORA,ENTORNO,PYME,ACTIVOS_GS,ACTIVOS_WEB,ACTIVOS_PORTAL,TOTAL_GS,TOTAL_WEB,TOTAL_PORTAL,traspaso)          " & vbCrLf
sConsulta = sConsulta & "   Select" & vbCrLf
sConsulta = sConsulta & "       ACTIVOS.FECHA," & vbCrLf
sConsulta = sConsulta & "       ACTIVOS.ANYO," & vbCrLf
sConsulta = sConsulta & "       ACTIVOS.MES," & vbCrLf
sConsulta = sConsulta & "       ACTIVOS.DIA," & vbCrLf
sConsulta = sConsulta & "       ACTIVOS.HORA," & vbCrLf
sConsulta = sConsulta & "       ACTIVOS.ENTORNO," & vbCrLf
sConsulta = sConsulta & "       ACTIVOS.PYME," & vbCrLf
sConsulta = sConsulta & "       ACTIVOS.ACTIVOS_GS," & vbCrLf
sConsulta = sConsulta & "       ACTIVOS.ACTIVOS_WEB," & vbCrLf
sConsulta = sConsulta & "       ACTIVOS.ACTIVOS_PORTAL," & vbCrLf
sConsulta = sConsulta & "       ISNULL(USUARIOS_GS.TOTAL_GS,0) AS TOTAL_GS," & vbCrLf
sConsulta = sConsulta & "       ISNULL(USUARIOS_WEB.TOTAL_WEB,0) AS TOTAL_WEB," & vbCrLf
sConsulta = sConsulta & "       ISNULL(USUARIOS_PORTAL.TOTAL_PORTAL,0) AS TOTAL_PORTAL," & vbCrLf
sConsulta = sConsulta & "       0 as traspaso" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   FROM (" & vbCrLf
sConsulta = sConsulta & "   SELECT" & vbCrLf
sConsulta = sConsulta & "       DATEADD(hour,HORA, @Dia)as FECHA" & vbCrLf
sConsulta = sConsulta & "       ,ANYO" & vbCrLf
sConsulta = sConsulta & "       ,MES" & vbCrLf
sConsulta = sConsulta & "       ,DIA" & vbCrLf
sConsulta = sConsulta & "       ,HORA           " & vbCrLf
sConsulta = sConsulta & "       ,ENTORNO" & vbCrLf
sConsulta = sConsulta & "       ,PYME" & vbCrLf
sConsulta = sConsulta & "       ,max(activos_gs) as ACTIVOS_GS" & vbCrLf
sConsulta = sConsulta & "       , max(activos_web) as ACTIVOS_WEB" & vbCrLf
sConsulta = sConsulta & "       , max(activos_portal) as ACTIVOS_PORTAL" & vbCrLf
sConsulta = sConsulta & "   FROM  FSAL_USUARIOS_MINUTO WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   where  ANYO = DATEPART(""yy"",@Dia)" & vbCrLf
sConsulta = sConsulta & "           and mes = DATEPART(""mm"",@Dia)" & vbCrLf
sConsulta = sConsulta & "           and dia = DATEPART(""dd"",@Dia)" & vbCrLf
sConsulta = sConsulta & "           --and HORA= DATEPART(""hh"",@Dia)" & vbCrLf
sConsulta = sConsulta & "           and ENTORNO = @entorno" & vbCrLf
sConsulta = sConsulta & "           and isnull(PYME,'') = isnull(@Pyme,'')" & vbCrLf
sConsulta = sConsulta & "           and fecha >=@Desde" & vbCrLf
sConsulta = sConsulta & "           and FECHA <@Hasta" & vbCrLf
sConsulta = sConsulta & "   group by " & vbCrLf
sConsulta = sConsulta & "       ANYO" & vbCrLf
sConsulta = sConsulta & "       ,MES" & vbCrLf
sConsulta = sConsulta & "       ,DIA" & vbCrLf
sConsulta = sConsulta & "       ,HORA" & vbCrLf
sConsulta = sConsulta & "       ,ENTORNO" & vbCrLf
sConsulta = sConsulta & "       ,PYME           " & vbCrLf
sConsulta = sConsulta & "   ) ACTIVOS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- Total_GS" & vbCrLf
sConsulta = sConsulta & "(" & vbCrLf
sConsulta = sConsulta & "SELECT" & vbCrLf
sConsulta = sConsulta & "datepart(""yy"",FISERVER) as ANYO" & vbCrLf
sConsulta = sConsulta & ",datepart(""mm"",FISERVER) as MES" & vbCrLf
sConsulta = sConsulta & ",datepart(""dd"",FISERVER) as DIA" & vbCrLf
sConsulta = sConsulta & ",datepart(""hh"",FISERVER) as HORA" & vbCrLf
sConsulta = sConsulta & ",count( distinct usuario) AS TOTAL_GS" & vbCrLf
sConsulta = sConsulta & "from fsal_accesos WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "where " & vbCrLf
sConsulta = sConsulta & "   producto = 'FULLSTEP GS'" & vbCrLf
sConsulta = sConsulta & "   and fsal_entorno = @entorno" & vbCrLf
sConsulta = sConsulta & "   and isnull(fsal_pyme,'') = isnull(@pyme,'')" & vbCrLf
sConsulta = sConsulta & "group by " & vbCrLf
sConsulta = sConsulta & "datepart(""yy"",FISERVER)" & vbCrLf
sConsulta = sConsulta & ",datepart(""mm"",FISERVER)" & vbCrLf
sConsulta = sConsulta & ",datepart(""dd"",FISERVER)" & vbCrLf
sConsulta = sConsulta & ",datepart(""hh"",FISERVER)" & vbCrLf
sConsulta = sConsulta & ") USUARIOS_GS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ON ACTIVOS.ANYO = USUARIOS_GS.ANYO" & vbCrLf
sConsulta = sConsulta & "   AND ACTIVOS.MES = USUARIOS_GS.MES" & vbCrLf
sConsulta = sConsulta & "   AND ACTIVOS.DIA = USUARIOS_GS.DIA" & vbCrLf
sConsulta = sConsulta & "   AND ACTIVOS.HORA = USUARIOS_GS.HORA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- Total_WEB" & vbCrLf
sConsulta = sConsulta & "(" & vbCrLf
sConsulta = sConsulta & "SELECT" & vbCrLf
sConsulta = sConsulta & "datepart(""yy"",FISERVER) as ANYO" & vbCrLf
sConsulta = sConsulta & ",datepart(""mm"",FISERVER) as MES" & vbCrLf
sConsulta = sConsulta & ",datepart(""dd"",FISERVER) as DIA" & vbCrLf
sConsulta = sConsulta & ",datepart(""hh"",FISERVER) as HORA" & vbCrLf
sConsulta = sConsulta & ",count( distinct usuario) AS TOTAL_WEB" & vbCrLf
sConsulta = sConsulta & "from fsal_accesos WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "where " & vbCrLf
sConsulta = sConsulta & "   producto = 'FULLSTEP WEB'" & vbCrLf
sConsulta = sConsulta & "   and fsal_entorno = @entorno" & vbCrLf
sConsulta = sConsulta & "   and isnull(fsal_pyme,'') = isnull(@pyme,'')" & vbCrLf
sConsulta = sConsulta & "group by " & vbCrLf
sConsulta = sConsulta & "datepart(""yy"",FISERVER)" & vbCrLf
sConsulta = sConsulta & ",datepart(""mm"",FISERVER)" & vbCrLf
sConsulta = sConsulta & ",datepart(""dd"",FISERVER)" & vbCrLf
sConsulta = sConsulta & ",datepart(""hh"",FISERVER)" & vbCrLf
sConsulta = sConsulta & ") USUARIOS_WEB" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ON     ACTIVOS.ANYO  = USUARIOS_WEB.ANYO" & vbCrLf
sConsulta = sConsulta & "   AND ACTIVOS.MES = USUARIOS_WEB.MES" & vbCrLf
sConsulta = sConsulta & "   AND ACTIVOS.DIA = USUARIOS_WEB.DIA" & vbCrLf
sConsulta = sConsulta & "   AND ACTIVOS.HORA = USUARIOS_WEB.HORA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- Total_PORTAL" & vbCrLf
sConsulta = sConsulta & "(" & vbCrLf
sConsulta = sConsulta & "SELECT" & vbCrLf
sConsulta = sConsulta & "datepart(""yy"",FISERVER) as ANYO" & vbCrLf
sConsulta = sConsulta & ",datepart(""mm"",FISERVER) as MES" & vbCrLf
sConsulta = sConsulta & ",datepart(""dd"",FISERVER) as DIA" & vbCrLf
sConsulta = sConsulta & ",datepart(""hh"",FISERVER) as HORA" & vbCrLf
sConsulta = sConsulta & ",count( distinct usuario) AS TOTAL_PORTAL" & vbCrLf
sConsulta = sConsulta & "from fsal_accesos WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "where " & vbCrLf
sConsulta = sConsulta & "   producto = 'FULLSTEP PORTAL'" & vbCrLf
sConsulta = sConsulta & "   and fsal_entorno = @entorno" & vbCrLf
sConsulta = sConsulta & "   and isnull(fsal_pyme,'') = isnull(@pyme,'')" & vbCrLf
sConsulta = sConsulta & "group by " & vbCrLf
sConsulta = sConsulta & "datepart(""yy"",FISERVER)" & vbCrLf
sConsulta = sConsulta & ",datepart(""mm"",FISERVER)" & vbCrLf
sConsulta = sConsulta & ",datepart(""dd"",FISERVER)" & vbCrLf
sConsulta = sConsulta & ",datepart(""hh"",FISERVER)" & vbCrLf
sConsulta = sConsulta & ") USUARIOS_PORTAL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ON     ACTIVOS.ANYO  = USUARIOS_PORTAL.ANYO" & vbCrLf
sConsulta = sConsulta & "   AND ACTIVOS.MES = USUARIOS_PORTAL.MES" & vbCrLf
sConsulta = sConsulta & "   AND ACTIVOS.DIA = USUARIOS_PORTAL.DIA" & vbCrLf
sConsulta = sConsulta & "   AND ACTIVOS.HORA = USUARIOS_PORTAL.HORA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "where" & vbCrLf
sConsulta = sConsulta & "      (ACTIVOS.ACTIVOS_GS > 0 " & vbCrLf
sConsulta = sConsulta & "       OR ACTIVOS.ACTIVOS_WEB > 0" & vbCrLf
sConsulta = sConsulta & "       OR ACTIVOS.ACTIVOS_PORTAL > 0" & vbCrLf
sConsulta = sConsulta & "       OR ISNULL(USUARIOS_GS.TOTAL_GS,0) >0" & vbCrLf
sConsulta = sConsulta & "       OR ISNULL(USUARIOS_WEB.TOTAL_WEB,0) > 0" & vbCrLf
sConsulta = sConsulta & "       OR ISNULL(USUARIOS_PORTAL.TOTAL_PORTAL,0) >0" & vbCrLf
sConsulta = sConsulta & "      )" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "      AND " & vbCrLf
sConsulta = sConsulta & "      NOT EXISTS" & vbCrLf
sConsulta = sConsulta & "      (" & vbCrLf
sConsulta = sConsulta & "      SELECT FECHA FROM FSAL_USUARIOS_HORA WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "      WHERE FECHA = ACTIVOS.FECHA" & vbCrLf
sConsulta = sConsulta & "        AND ENTORNO =ACTIVOS.ENTORNO" & vbCrLf
sConsulta = sConsulta & "        AND isnull(PYME,'') =isnull(ACTIVOS.PYME,'')" & vbCrLf
sConsulta = sConsulta & "      )" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Actualizamos los registros que ya existan" & vbCrLf
sConsulta = sConsulta & "   UPDATE FSAL_USUARIOS_HORA" & vbCrLf
sConsulta = sConsulta & "   SET ACTIVOS_GS = REGISTROS_A_ACTUALIZAR.ACTIVOS_GS" & vbCrLf
sConsulta = sConsulta & "       ,ACTIVOS_WEB = REGISTROS_A_ACTUALIZAR.ACTIVOS_WEB" & vbCrLf
sConsulta = sConsulta & "       ,ACTIVOS_PORTAL = REGISTROS_A_ACTUALIZAR.ACTIVOS_PORTAL" & vbCrLf
sConsulta = sConsulta & "       ,TOTAL_GS = REGISTROS_A_ACTUALIZAR.TOTAL_GS" & vbCrLf
sConsulta = sConsulta & "       ,TOTAL_WEB = REGISTROS_A_ACTUALIZAR.TOTAL_WEB" & vbCrLf
sConsulta = sConsulta & "       ,TOTAL_PORTAL = REGISTROS_A_ACTUALIZAR.TOTAL_PORTAL" & vbCrLf
sConsulta = sConsulta & "      ,TRASPASO = 0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    from  FSAL_USUARIOS_HORA WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   inner join" & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   Select" & vbCrLf
sConsulta = sConsulta & "       ACTIVOS.FECHA," & vbCrLf
sConsulta = sConsulta & "       ACTIVOS.ANYO," & vbCrLf
sConsulta = sConsulta & "       ACTIVOS.MES," & vbCrLf
sConsulta = sConsulta & "       ACTIVOS.DIA," & vbCrLf
sConsulta = sConsulta & "       ACTIVOS.HORA," & vbCrLf
sConsulta = sConsulta & "       ACTIVOS.ENTORNO," & vbCrLf
sConsulta = sConsulta & "       ACTIVOS.PYME," & vbCrLf
sConsulta = sConsulta & "       ACTIVOS.ACTIVOS_GS," & vbCrLf
sConsulta = sConsulta & "       ACTIVOS.ACTIVOS_WEB," & vbCrLf
sConsulta = sConsulta & "       ACTIVOS.ACTIVOS_PORTAL," & vbCrLf
sConsulta = sConsulta & "       ISNULL(USUARIOS_GS.TOTAL_GS,0) AS TOTAL_GS," & vbCrLf
sConsulta = sConsulta & "       ISNULL(USUARIOS_WEB.TOTAL_WEB,0) AS TOTAL_WEB," & vbCrLf
sConsulta = sConsulta & "       ISNULL(USUARIOS_PORTAL.TOTAL_PORTAL,0) AS TOTAL_PORTAL," & vbCrLf
sConsulta = sConsulta & "       0 as traspaso" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   FROM (" & vbCrLf
sConsulta = sConsulta & "   SELECT" & vbCrLf
sConsulta = sConsulta & "       DATEADD(hour,HORA, @Dia)as FECHA" & vbCrLf
sConsulta = sConsulta & "       ,ANYO" & vbCrLf
sConsulta = sConsulta & "       ,MES" & vbCrLf
sConsulta = sConsulta & "       ,DIA" & vbCrLf
sConsulta = sConsulta & "       ,HORA           " & vbCrLf
sConsulta = sConsulta & "       ,ENTORNO" & vbCrLf
sConsulta = sConsulta & "       ,PYME" & vbCrLf
sConsulta = sConsulta & "       ,max(activos_gs) as ACTIVOS_GS" & vbCrLf
sConsulta = sConsulta & "       , max(activos_web) as ACTIVOS_WEB" & vbCrLf
sConsulta = sConsulta & "       , max(activos_portal) as ACTIVOS_PORTAL" & vbCrLf
sConsulta = sConsulta & "   FROM  FSAL_USUARIOS_MINUTO WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   where  ANYO = DATEPART(""yy"",@Dia)" & vbCrLf
sConsulta = sConsulta & "           and mes = DATEPART(""mm"",@Dia)" & vbCrLf
sConsulta = sConsulta & "           and dia = DATEPART(""dd"",@Dia)" & vbCrLf
sConsulta = sConsulta & "           --and HORA= DATEPART(""hh"",@Dia)" & vbCrLf
sConsulta = sConsulta & "           and ENTORNO = @entorno" & vbCrLf
sConsulta = sConsulta & "           and isnull(PYME,'') = isnull(@Pyme,'')" & vbCrLf
sConsulta = sConsulta & "           and fecha >=@Desde" & vbCrLf
sConsulta = sConsulta & "           and FECHA <@Hasta" & vbCrLf
sConsulta = sConsulta & "   group by " & vbCrLf
sConsulta = sConsulta & "       ANYO" & vbCrLf
sConsulta = sConsulta & "       ,MES" & vbCrLf
sConsulta = sConsulta & "       ,DIA" & vbCrLf
sConsulta = sConsulta & "       ,HORA" & vbCrLf
sConsulta = sConsulta & "       ,ENTORNO" & vbCrLf
sConsulta = sConsulta & "       ,PYME           " & vbCrLf
sConsulta = sConsulta & "   ) ACTIVOS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- Total_GS" & vbCrLf
sConsulta = sConsulta & "(" & vbCrLf
sConsulta = sConsulta & "SELECT" & vbCrLf
sConsulta = sConsulta & "datepart(""yy"",FISERVER) as ANYO" & vbCrLf
sConsulta = sConsulta & ",datepart(""mm"",FISERVER) as MES" & vbCrLf
sConsulta = sConsulta & ",datepart(""dd"",FISERVER) as DIA" & vbCrLf
sConsulta = sConsulta & ",datepart(""hh"",FISERVER) as HORA" & vbCrLf
sConsulta = sConsulta & ",count( distinct usuario) AS TOTAL_GS" & vbCrLf
sConsulta = sConsulta & "from fsal_accesos WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "where " & vbCrLf
sConsulta = sConsulta & "   producto = 'FULLSTEP GS'" & vbCrLf
sConsulta = sConsulta & "   and fsal_entorno = @entorno" & vbCrLf
sConsulta = sConsulta & "   and isnull(fsal_pyme,'') = isnull(@pyme,'')" & vbCrLf
sConsulta = sConsulta & "group by " & vbCrLf
sConsulta = sConsulta & "datepart(""yy"",FISERVER)" & vbCrLf
sConsulta = sConsulta & ",datepart(""mm"",FISERVER)" & vbCrLf
sConsulta = sConsulta & ",datepart(""dd"",FISERVER)" & vbCrLf
sConsulta = sConsulta & ",datepart(""hh"",FISERVER)" & vbCrLf
sConsulta = sConsulta & ") USUARIOS_GS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ON ACTIVOS.ANYO = USUARIOS_GS.ANYO" & vbCrLf
sConsulta = sConsulta & "   AND ACTIVOS.MES = USUARIOS_GS.MES" & vbCrLf
sConsulta = sConsulta & "   AND ACTIVOS.DIA = USUARIOS_GS.DIA" & vbCrLf
sConsulta = sConsulta & "   AND ACTIVOS.HORA = USUARIOS_GS.HORA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- Total_WEB" & vbCrLf
sConsulta = sConsulta & "(" & vbCrLf
sConsulta = sConsulta & "SELECT" & vbCrLf
sConsulta = sConsulta & "datepart(""yy"",FISERVER) as ANYO" & vbCrLf
sConsulta = sConsulta & ",datepart(""mm"",FISERVER) as MES" & vbCrLf
sConsulta = sConsulta & ",datepart(""dd"",FISERVER) as DIA" & vbCrLf
sConsulta = sConsulta & ",datepart(""hh"",FISERVER) as HORA" & vbCrLf
sConsulta = sConsulta & ",count( distinct usuario) AS TOTAL_WEB" & vbCrLf
sConsulta = sConsulta & "from fsal_accesos WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "where " & vbCrLf
sConsulta = sConsulta & "   producto = 'FULLSTEP WEB'" & vbCrLf
sConsulta = sConsulta & "   and fsal_entorno = @entorno" & vbCrLf
sConsulta = sConsulta & "   and isnull(fsal_pyme,'') = isnull(@pyme,'')" & vbCrLf
sConsulta = sConsulta & "group by " & vbCrLf
sConsulta = sConsulta & "datepart(""yy"",FISERVER)" & vbCrLf
sConsulta = sConsulta & ",datepart(""mm"",FISERVER)" & vbCrLf
sConsulta = sConsulta & ",datepart(""dd"",FISERVER)" & vbCrLf
sConsulta = sConsulta & ",datepart(""hh"",FISERVER)" & vbCrLf
sConsulta = sConsulta & ") USUARIOS_WEB" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ON     ACTIVOS.ANYO  = USUARIOS_WEB.ANYO" & vbCrLf
sConsulta = sConsulta & "   AND ACTIVOS.MES = USUARIOS_WEB.MES" & vbCrLf
sConsulta = sConsulta & "   AND ACTIVOS.DIA = USUARIOS_WEB.DIA" & vbCrLf
sConsulta = sConsulta & "   AND ACTIVOS.HORA = USUARIOS_WEB.HORA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- Total_PORTAL" & vbCrLf
sConsulta = sConsulta & "(" & vbCrLf
sConsulta = sConsulta & "SELECT" & vbCrLf
sConsulta = sConsulta & "datepart(""yy"",FISERVER) as ANYO" & vbCrLf
sConsulta = sConsulta & ",datepart(""mm"",FISERVER) as MES" & vbCrLf
sConsulta = sConsulta & ",datepart(""dd"",FISERVER) as DIA" & vbCrLf
sConsulta = sConsulta & ",datepart(""hh"",FISERVER) as HORA" & vbCrLf
sConsulta = sConsulta & ",count( distinct usuario) AS TOTAL_PORTAL" & vbCrLf
sConsulta = sConsulta & "from fsal_accesos WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "where " & vbCrLf
sConsulta = sConsulta & "   producto = 'FULLSTEP PORTAL'" & vbCrLf
sConsulta = sConsulta & "   and fsal_entorno = @entorno" & vbCrLf
sConsulta = sConsulta & "   and isnull(fsal_pyme,'') = isnull(@pyme,'')" & vbCrLf
sConsulta = sConsulta & "group by " & vbCrLf
sConsulta = sConsulta & "datepart(""yy"",FISERVER)" & vbCrLf
sConsulta = sConsulta & ",datepart(""mm"",FISERVER)" & vbCrLf
sConsulta = sConsulta & ",datepart(""dd"",FISERVER)" & vbCrLf
sConsulta = sConsulta & ",datepart(""hh"",FISERVER)" & vbCrLf
sConsulta = sConsulta & ") USUARIOS_PORTAL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ON     ACTIVOS.ANYO  = USUARIOS_PORTAL.ANYO" & vbCrLf
sConsulta = sConsulta & "   AND ACTIVOS.MES = USUARIOS_PORTAL.MES" & vbCrLf
sConsulta = sConsulta & "   AND ACTIVOS.DIA = USUARIOS_PORTAL.DIA" & vbCrLf
sConsulta = sConsulta & "   AND ACTIVOS.HORA = USUARIOS_PORTAL.HORA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "where" & vbCrLf
sConsulta = sConsulta & "      (ACTIVOS.ACTIVOS_GS > 0 " & vbCrLf
sConsulta = sConsulta & "       OR ACTIVOS.ACTIVOS_WEB > 0" & vbCrLf
sConsulta = sConsulta & "       OR ACTIVOS.ACTIVOS_PORTAL > 0" & vbCrLf
sConsulta = sConsulta & "       OR ISNULL(USUARIOS_GS.TOTAL_GS,0) >0" & vbCrLf
sConsulta = sConsulta & "       OR ISNULL(USUARIOS_WEB.TOTAL_WEB,0) > 0" & vbCrLf
sConsulta = sConsulta & "       OR ISNULL(USUARIOS_PORTAL.TOTAL_PORTAL,0) >0" & vbCrLf
sConsulta = sConsulta & "      )" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & ") REGISTROS_A_ACTUALIZAR" & vbCrLf
sConsulta = sConsulta & "ON  FSAL_USUARIOS_HORA.FECHA = REGISTROS_A_ACTUALIZAR.FECHA" & vbCrLf
sConsulta = sConsulta & "AND  FSAL_USUARIOS_HORA.ENTORNO = REGISTROS_A_ACTUALIZAR.ENTORNO" & vbCrLf
sConsulta = sConsulta & "AND isnull(FSAL_USUARIOS_HORA.PYME,'') = isnull(REGISTROS_A_ACTUALIZAR.PYME,'')" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
            
            
sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_ESTADISTICAS_ENTORNO_DIA]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_ESTADISTICAS_ENTORNO_DIA]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta


sConsulta = "CREATE PROCEDURE [dbo].[FSAL_ESTADISTICAS_ENTORNO_DIA]  " & vbCrLf
sConsulta = sConsulta & "   @Dia DATETIME," & vbCrLf
sConsulta = sConsulta & "   @entorno nvarchar(50)," & vbCrLf
sConsulta = sConsulta & "   @pyme nvarchar(50)" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "Declare @DiaSig as datetime" & vbCrLf
sConsulta = sConsulta & "set @Diasig = dateadd(day,1,@Dia);" & vbCrLf
sConsulta = sConsulta & "Declare @Hora as datetime;" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- Ahora se cargan los usuarios activos por Dia basandose en el m�ximo de usuarios de cada bloque de hora" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO FSAL_USUARIOS_DIA (FECHA,ANYO,MES,DIA,DIA_SEMANA,ENTORNO,PYME,ACTIVOS_GS,ACTIVOS_WEB,ACTIVOS_PORTAL,TOTAL_GS,TOTAL_WEB,TOTAL_PORTAL,TRASPASO) " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   Select" & vbCrLf
sConsulta = sConsulta & "       ACTIVOS.FECHA," & vbCrLf
sConsulta = sConsulta & "       ACTIVOS.ANYO," & vbCrLf
sConsulta = sConsulta & "       ACTIVOS.MES," & vbCrLf
sConsulta = sConsulta & "       ACTIVOS.DIA," & vbCrLf
sConsulta = sConsulta & "       datepart(""dw"",ACTIVOS.FECHA) as DIASEMANA," & vbCrLf
sConsulta = sConsulta & "       ACTIVOS.ENTORNO," & vbCrLf
sConsulta = sConsulta & "       ISNULL(ACTIVOS.PYME,'') as PYME," & vbCrLf
sConsulta = sConsulta & "       ACTIVOS.ACTIVOS_GS," & vbCrLf
sConsulta = sConsulta & "       ACTIVOS.ACTIVOS_WEB," & vbCrLf
sConsulta = sConsulta & "       ACTIVOS.ACTIVOS_PORTAL," & vbCrLf
sConsulta = sConsulta & "       ISNULL(USUARIOS_GS.TOTAL_GS,0) AS TOTAL_GS," & vbCrLf
sConsulta = sConsulta & "       ISNULL(USUARIOS_WEB.TOTAL_WEB,0) AS TOTAL_WEB," & vbCrLf
sConsulta = sConsulta & "       ISNULL(USUARIOS_PORTAL.TOTAL_PORTAL,0) AS TOTAL_PORTAL, " & vbCrLf
sConsulta = sConsulta & "       0 as TRASPASO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   FROM (" & vbCrLf
sConsulta = sConsulta & "   SELECT" & vbCrLf
sConsulta = sConsulta & "       Convert(datetime,convert(varchar(10),@Dia,121)) as FECHA" & vbCrLf
sConsulta = sConsulta & "       ,ANYO" & vbCrLf
sConsulta = sConsulta & "       ,MES" & vbCrLf
sConsulta = sConsulta & "       ,DIA" & vbCrLf
sConsulta = sConsulta & "       ,ENTORNO" & vbCrLf
sConsulta = sConsulta & "       ,PYME" & vbCrLf
sConsulta = sConsulta & "       ,max(activos_gs) as ACTIVOS_GS" & vbCrLf
sConsulta = sConsulta & "       , max(activos_web) as ACTIVOS_WEB" & vbCrLf
sConsulta = sConsulta & "       , max(activos_portal) as ACTIVOS_PORTAL" & vbCrLf
sConsulta = sConsulta & "   FROM  FSAL_USUARIOS_HORA WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   where  ANYO = DATEPART(""yy"",@Dia)" & vbCrLf
sConsulta = sConsulta & "           and mes = DATEPART(""mm"",@Dia)" & vbCrLf
sConsulta = sConsulta & "           and dia = DATEPART(""dd"",@Dia)" & vbCrLf
sConsulta = sConsulta & "           and ENTORNO = @entorno" & vbCrLf
sConsulta = sConsulta & "           and isnull(PYME,'') = isnull(@Pyme,'')" & vbCrLf
sConsulta = sConsulta & "   group by " & vbCrLf
sConsulta = sConsulta & "       ANYO" & vbCrLf
sConsulta = sConsulta & "       ,MES" & vbCrLf
sConsulta = sConsulta & "       ,DIA" & vbCrLf
sConsulta = sConsulta & "       ,ENTORNO" & vbCrLf
sConsulta = sConsulta & "       ,PYME           " & vbCrLf
sConsulta = sConsulta & "   ) ACTIVOS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- Total_GS" & vbCrLf
sConsulta = sConsulta & "(" & vbCrLf
sConsulta = sConsulta & "SELECT" & vbCrLf
sConsulta = sConsulta & "datepart(""yy"",FISERVER) as ANYO" & vbCrLf
sConsulta = sConsulta & ",datepart(""mm"",FISERVER) as MES" & vbCrLf
sConsulta = sConsulta & ",datepart(""dd"",FISERVER) as DIA" & vbCrLf
sConsulta = sConsulta & ",count( distinct usuario) AS TOTAL_GS" & vbCrLf
sConsulta = sConsulta & "from fsal_accesos WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "where " & vbCrLf
sConsulta = sConsulta & "   producto = 'FULLSTEP GS'" & vbCrLf
sConsulta = sConsulta & "   and fsal_entorno = @entorno" & vbCrLf
sConsulta = sConsulta & "   and isnull(fsal_pyme,'') = isnull(@pyme,'')" & vbCrLf
sConsulta = sConsulta & "group by " & vbCrLf
sConsulta = sConsulta & "datepart(""yy"",FISERVER)" & vbCrLf
sConsulta = sConsulta & ",datepart(""mm"",FISERVER)" & vbCrLf
sConsulta = sConsulta & ",datepart(""dd"",FISERVER)" & vbCrLf
sConsulta = sConsulta & ") USUARIOS_GS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ON ACTIVOS.ANYO = USUARIOS_GS.ANYO" & vbCrLf
sConsulta = sConsulta & "   AND ACTIVOS.MES = USUARIOS_GS.MES" & vbCrLf
sConsulta = sConsulta & "   AND ACTIVOS.DIA = USUARIOS_GS.DIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- Total_WEB" & vbCrLf
sConsulta = sConsulta & "(" & vbCrLf
sConsulta = sConsulta & "SELECT" & vbCrLf
sConsulta = sConsulta & "datepart(""yy"",FISERVER) as ANYO" & vbCrLf
sConsulta = sConsulta & ",datepart(""mm"",FISERVER) as MES" & vbCrLf
sConsulta = sConsulta & ",datepart(""dd"",FISERVER) as DIA" & vbCrLf
sConsulta = sConsulta & ",count( distinct usuario) AS TOTAL_WEB" & vbCrLf
sConsulta = sConsulta & "from fsal_accesos WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "where " & vbCrLf
sConsulta = sConsulta & "   producto = 'FULLSTEP WEB'" & vbCrLf
sConsulta = sConsulta & "   and fsal_entorno = @entorno" & vbCrLf
sConsulta = sConsulta & "   and isnull(fsal_pyme,'') = isnull(@pyme,'')" & vbCrLf
sConsulta = sConsulta & "group by " & vbCrLf
sConsulta = sConsulta & "datepart(""yy"",FISERVER)" & vbCrLf
sConsulta = sConsulta & ",datepart(""mm"",FISERVER)" & vbCrLf
sConsulta = sConsulta & ",datepart(""dd"",FISERVER)" & vbCrLf
sConsulta = sConsulta & ") USUARIOS_WEB" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ON     ACTIVOS.ANYO  = USUARIOS_WEB.ANYO" & vbCrLf
sConsulta = sConsulta & "   AND ACTIVOS.MES = USUARIOS_WEB.MES" & vbCrLf
sConsulta = sConsulta & "   AND ACTIVOS.DIA = USUARIOS_WEB.DIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- Total_PORTAL" & vbCrLf
sConsulta = sConsulta & "(" & vbCrLf
sConsulta = sConsulta & "SELECT" & vbCrLf
sConsulta = sConsulta & "datepart(""yy"",FISERVER) as ANYO" & vbCrLf
sConsulta = sConsulta & ",datepart(""mm"",FISERVER) as MES" & vbCrLf
sConsulta = sConsulta & ",datepart(""dd"",FISERVER) as DIA" & vbCrLf
sConsulta = sConsulta & ",count( distinct usuario) AS TOTAL_PORTAL" & vbCrLf
sConsulta = sConsulta & "from fsal_accesos WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "where " & vbCrLf
sConsulta = sConsulta & "   producto = 'FULLSTEP PORTAL'" & vbCrLf
sConsulta = sConsulta & "   and fsal_entorno = @entorno" & vbCrLf
sConsulta = sConsulta & "   and isnull(fsal_pyme,'') = isnull(@pyme,'')" & vbCrLf
sConsulta = sConsulta & "group by " & vbCrLf
sConsulta = sConsulta & "datepart(""yy"",FISERVER)" & vbCrLf
sConsulta = sConsulta & ",datepart(""mm"",FISERVER)" & vbCrLf
sConsulta = sConsulta & ",datepart(""dd"",FISERVER)" & vbCrLf
sConsulta = sConsulta & ") USUARIOS_PORTAL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ON     ACTIVOS.ANYO  = USUARIOS_PORTAL.ANYO" & vbCrLf
sConsulta = sConsulta & "   AND ACTIVOS.MES = USUARIOS_PORTAL.MES" & vbCrLf
sConsulta = sConsulta & "   AND ACTIVOS.DIA = USUARIOS_PORTAL.DIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "WHERE (ACTIVOS.ACTIVOS_GS > 0" & vbCrLf
sConsulta = sConsulta & "   OR ACTIVOS.ACTIVOS_WEB > 0" & vbCrLf
sConsulta = sConsulta & "   OR ACTIVOS.ACTIVOS_PORTAL > 0" & vbCrLf
sConsulta = sConsulta & "   OR ISNULL(USUARIOS_GS.TOTAL_GS,0) > 0" & vbCrLf
sConsulta = sConsulta & "   OR ISNULL(USUARIOS_WEB.TOTAL_WEB,0) > 0" & vbCrLf
sConsulta = sConsulta & "   OR ISNULL(USUARIOS_PORTAL.TOTAL_PORTAL,0) >0" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
sConsulta = sConsulta & "AND NOT EXISTS(" & vbCrLf
sConsulta = sConsulta & "   SELECT FECHA FROM FSAL_USUARIOS_DIA WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   WHERE FECHA = ACTIVOS.FECHA" & vbCrLf
sConsulta = sConsulta & "     AND ENTORNO = ACTIVOS.ENTORNO " & vbCrLf
sConsulta = sConsulta & "    AND ISNULL(PYME,'') = ISNULL(ACTIVOS.PYME,'')" & vbCrLf
sConsulta = sConsulta & ")" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- Y ahora actualizamos los registros que ya existen" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE FSAL_USUARIOS_DIA" & vbCrLf
sConsulta = sConsulta & "SET ACTIVOS_GS = REGISTROS_A_ACTUALIZAR.ACTIVOS_GS," & vbCrLf
sConsulta = sConsulta & "    ACTIVOS_WEB = REGISTROS_A_ACTUALIZAR.ACTIVOS_WEB," & vbCrLf
sConsulta = sConsulta & "    ACTIVOS_PORTAL = REGISTROS_A_ACTUALIZAR.ACTIVOS_PORTAL," & vbCrLf
sConsulta = sConsulta & "    TOTAL_GS= REGISTROS_A_ACTUALIZAR.TOTAL_GS," & vbCrLf
sConsulta = sConsulta & "    TOTAL_WEB= REGISTROS_A_ACTUALIZAR.TOTAL_WEB," & vbCrLf
sConsulta = sConsulta & "    TOTAL_PORTAL = REGISTROS_A_ACTUALIZAR.TOTAL_PORTAL, " & vbCrLf
sConsulta = sConsulta & "   traspaso = 0" & vbCrLf
sConsulta = sConsulta & "from  FSAL_USUARIOS_DIA WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "inner join " & vbCrLf
sConsulta = sConsulta & "(" & vbCrLf
sConsulta = sConsulta & "   Select" & vbCrLf
sConsulta = sConsulta & "       ACTIVOS.FECHA," & vbCrLf
sConsulta = sConsulta & "       ACTIVOS.ANYO," & vbCrLf
sConsulta = sConsulta & "       ACTIVOS.MES," & vbCrLf
sConsulta = sConsulta & "       ACTIVOS.DIA," & vbCrLf
sConsulta = sConsulta & "       datepart(""dw"",ACTIVOS.FECHA) as DIASEMANA," & vbCrLf
sConsulta = sConsulta & "       ACTIVOS.ENTORNO," & vbCrLf
sConsulta = sConsulta & "       ISNULL(ACTIVOS.PYME,'') as PYME," & vbCrLf
sConsulta = sConsulta & "       ACTIVOS.ACTIVOS_GS," & vbCrLf
sConsulta = sConsulta & "       ACTIVOS.ACTIVOS_WEB," & vbCrLf
sConsulta = sConsulta & "       ACTIVOS.ACTIVOS_PORTAL," & vbCrLf
sConsulta = sConsulta & "       ISNULL(USUARIOS_GS.TOTAL_GS,0) AS TOTAL_GS," & vbCrLf
sConsulta = sConsulta & "       ISNULL(USUARIOS_WEB.TOTAL_WEB,0) AS TOTAL_WEB," & vbCrLf
sConsulta = sConsulta & "       ISNULL(USUARIOS_PORTAL.TOTAL_PORTAL,0) AS TOTAL_PORTAL, " & vbCrLf
sConsulta = sConsulta & "       0 as TRASPASO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   FROM (" & vbCrLf
sConsulta = sConsulta & "   SELECT" & vbCrLf
sConsulta = sConsulta & "       Convert(datetime,convert(varchar(10),@Dia,121)) as FECHA" & vbCrLf
sConsulta = sConsulta & "       ,ANYO" & vbCrLf
sConsulta = sConsulta & "       ,MES" & vbCrLf
sConsulta = sConsulta & "       ,DIA" & vbCrLf
sConsulta = sConsulta & "       ,ENTORNO" & vbCrLf
sConsulta = sConsulta & "       ,PYME" & vbCrLf
sConsulta = sConsulta & "       ,max(activos_gs) as ACTIVOS_GS" & vbCrLf
sConsulta = sConsulta & "       , max(activos_web) as ACTIVOS_WEB" & vbCrLf
sConsulta = sConsulta & "       , max(activos_portal) as ACTIVOS_PORTAL" & vbCrLf
sConsulta = sConsulta & "   FROM  FSAL_USUARIOS_HORA WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   where  ANYO = DATEPART(""yy"",@Dia)" & vbCrLf
sConsulta = sConsulta & "           and mes = DATEPART(""mm"",@Dia)" & vbCrLf
sConsulta = sConsulta & "           and dia = DATEPART(""dd"",@Dia)" & vbCrLf
sConsulta = sConsulta & "           and ENTORNO = @entorno" & vbCrLf
sConsulta = sConsulta & "           and isnull(PYME,'') = isnull(@Pyme,'')" & vbCrLf
sConsulta = sConsulta & "   group by " & vbCrLf
sConsulta = sConsulta & "       ANYO" & vbCrLf
sConsulta = sConsulta & "       ,MES" & vbCrLf
sConsulta = sConsulta & "       ,DIA" & vbCrLf
sConsulta = sConsulta & "       ,ENTORNO" & vbCrLf
sConsulta = sConsulta & "       ,PYME           " & vbCrLf
sConsulta = sConsulta & "   ) ACTIVOS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- Total_GS" & vbCrLf
sConsulta = sConsulta & "(" & vbCrLf
sConsulta = sConsulta & "SELECT" & vbCrLf
sConsulta = sConsulta & "datepart(""yy"",FISERVER) as ANYO" & vbCrLf
sConsulta = sConsulta & ",datepart(""mm"",FISERVER) as MES" & vbCrLf
sConsulta = sConsulta & ",datepart(""dd"",FISERVER) as DIA" & vbCrLf
sConsulta = sConsulta & ",count( distinct usuario) AS TOTAL_GS" & vbCrLf
sConsulta = sConsulta & "from fsal_accesos WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "where " & vbCrLf
sConsulta = sConsulta & "   producto = 'FULLSTEP GS'" & vbCrLf
sConsulta = sConsulta & "   and fsal_entorno = @entorno" & vbCrLf
sConsulta = sConsulta & "   and isnull(fsal_pyme,'') = isnull(@pyme,'')" & vbCrLf
sConsulta = sConsulta & "group by " & vbCrLf
sConsulta = sConsulta & "datepart(""yy"",FISERVER)" & vbCrLf
sConsulta = sConsulta & ",datepart(""mm"",FISERVER)" & vbCrLf
sConsulta = sConsulta & ",datepart(""dd"",FISERVER)" & vbCrLf
sConsulta = sConsulta & ") USUARIOS_GS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ON ACTIVOS.ANYO = USUARIOS_GS.ANYO" & vbCrLf
sConsulta = sConsulta & "   AND ACTIVOS.MES = USUARIOS_GS.MES" & vbCrLf
sConsulta = sConsulta & "   AND ACTIVOS.DIA = USUARIOS_GS.DIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- Total_WEB" & vbCrLf
sConsulta = sConsulta & "(" & vbCrLf
sConsulta = sConsulta & "SELECT" & vbCrLf
sConsulta = sConsulta & "datepart(""yy"",FISERVER) as ANYO" & vbCrLf
sConsulta = sConsulta & ",datepart(""mm"",FISERVER) as MES" & vbCrLf
sConsulta = sConsulta & ",datepart(""dd"",FISERVER) as DIA" & vbCrLf
sConsulta = sConsulta & ",count( distinct usuario) AS TOTAL_WEB" & vbCrLf
sConsulta = sConsulta & "from fsal_accesos WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "where " & vbCrLf
sConsulta = sConsulta & "   producto = 'FULLSTEP WEB'" & vbCrLf
sConsulta = sConsulta & "   and fsal_entorno = @entorno" & vbCrLf
sConsulta = sConsulta & "   and isnull(fsal_pyme,'') = isnull(@pyme,'')" & vbCrLf
sConsulta = sConsulta & "group by " & vbCrLf
sConsulta = sConsulta & "datepart(""yy"",FISERVER)" & vbCrLf
sConsulta = sConsulta & ",datepart(""mm"",FISERVER)" & vbCrLf
sConsulta = sConsulta & ",datepart(""dd"",FISERVER)" & vbCrLf
sConsulta = sConsulta & ") USUARIOS_WEB" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ON     ACTIVOS.ANYO  = USUARIOS_WEB.ANYO" & vbCrLf
sConsulta = sConsulta & "   AND ACTIVOS.MES = USUARIOS_WEB.MES" & vbCrLf
sConsulta = sConsulta & "   AND ACTIVOS.DIA = USUARIOS_WEB.DIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- Total_PORTAL" & vbCrLf
sConsulta = sConsulta & "(" & vbCrLf
sConsulta = sConsulta & "SELECT" & vbCrLf
sConsulta = sConsulta & "datepart(""yy"",FISERVER) as ANYO" & vbCrLf
sConsulta = sConsulta & ",datepart(""mm"",FISERVER) as MES" & vbCrLf
sConsulta = sConsulta & ",datepart(""dd"",FISERVER) as DIA" & vbCrLf
sConsulta = sConsulta & ",count( distinct usuario) AS TOTAL_PORTAL" & vbCrLf
sConsulta = sConsulta & "from fsal_accesos WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "where " & vbCrLf
sConsulta = sConsulta & "   producto = 'FULLSTEP PORTAL'" & vbCrLf
sConsulta = sConsulta & "   and fsal_entorno = @entorno" & vbCrLf
sConsulta = sConsulta & "   and isnull(fsal_pyme,'') = isnull(@pyme,'')" & vbCrLf
sConsulta = sConsulta & "group by " & vbCrLf
sConsulta = sConsulta & "datepart(""yy"",FISERVER)" & vbCrLf
sConsulta = sConsulta & ",datepart(""mm"",FISERVER)" & vbCrLf
sConsulta = sConsulta & ",datepart(""dd"",FISERVER)" & vbCrLf
sConsulta = sConsulta & ") USUARIOS_PORTAL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ON     ACTIVOS.ANYO  = USUARIOS_PORTAL.ANYO" & vbCrLf
sConsulta = sConsulta & "   AND ACTIVOS.MES = USUARIOS_PORTAL.MES" & vbCrLf
sConsulta = sConsulta & "   AND ACTIVOS.DIA = USUARIOS_PORTAL.DIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "WHERE (ACTIVOS.ACTIVOS_GS > 0" & vbCrLf
sConsulta = sConsulta & "   OR ACTIVOS.ACTIVOS_WEB > 0" & vbCrLf
sConsulta = sConsulta & "   OR ACTIVOS.ACTIVOS_PORTAL > 0" & vbCrLf
sConsulta = sConsulta & "   OR ISNULL(USUARIOS_GS.TOTAL_GS,0) > 0" & vbCrLf
sConsulta = sConsulta & "   OR ISNULL(USUARIOS_WEB.TOTAL_WEB,0) > 0" & vbCrLf
sConsulta = sConsulta & "   OR ISNULL(USUARIOS_PORTAL.TOTAL_PORTAL,0) >0" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
sConsulta = sConsulta & ")" & vbCrLf
sConsulta = sConsulta & " REGISTROS_A_ACTUALIZAR" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ON FSAL_USUARIOS_DIA.FECHA = REGISTROS_A_ACTUALIZAR.FECHA" & vbCrLf
sConsulta = sConsulta & "AND FSAL_USUARIOS_DIA.ENTORNO = REGISTROS_A_ACTUALIZAR.ENTORNO" & vbCrLf
sConsulta = sConsulta & "AND isnull(FSAL_USUARIOS_DIA.PYME,'') = isnull(REGISTROS_A_ACTUALIZAR.pyme,'')" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- Fin de procesado de las horas" & vbCrLf
sConsulta = sConsulta & "-- Por �ltimo a�ado los distintos ususrios conectados hoy a las tablas de registros semanales, mensuales y anuales" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Nos aseguramos de utilizar un formato de semana europeo " & vbCrLf
sConsulta = sConsulta & "-- Solo afecta a la sesi�n por lo que no alteramos el formato por defecto del sevidor" & vbCrLf
sConsulta = sConsulta & "set datefirst 1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO FSAL_USUARIOS_DIS_SEMANA (FECHA,ANYO,MES,DIA,ENTORNO,PYME,PRODUCTO,USU,TRASPASO)" & vbCrLf
sConsulta = sConsulta & "          SELECT DISTINCT " & vbCrLf
sConsulta = sConsulta & "           convert(datetime,ltrim(datepart(""yy"",CONVERT(datetime,CONVERT (char(10), dateadd(d,-1*(datepart(dw,FISERVER)-1),FISERVER), 112)))) +'-'+ ltrim(datepart(""mm"",CONVERT(datetime,CONVERT (char(10), dateadd(d,-1*(datepart(dw,FISERVER)-1),FISERVER), 112))))+'-'+ltrim(datepart(""dd"",CONVERT(datetime,CONVERT (char(10), dateadd(d,-1*(datepart(dw,FISERVER)-1),FISERVER), 112))))) as FISERVER" & vbCrLf
sConsulta = sConsulta & "           ,datepart(""yy"",CONVERT(datetime,CONVERT (char(10), dateadd(d,-1*(datepart(dw,FISERVER)-1),FISERVER), 112))) AS ANYO" & vbCrLf
sConsulta = sConsulta & "           ,datepart(""mm"",CONVERT(datetime,CONVERT (char(10), dateadd(d,-1*(datepart(dw,FISERVER)-1),FISERVER), 112))) AS MES" & vbCrLf
sConsulta = sConsulta & "           ,datepart(""dd"",CONVERT(datetime,CONVERT (char(10), dateadd(d,-1*(datepart(dw,FISERVER)-1),FISERVER), 112))) AS DIA -- OJO!!!! Primer d�a de la semaan en formato europeo" & vbCrLf
sConsulta = sConsulta & "           , FSAL_entorno AS ENTORNO" & vbCrLf
sConsulta = sConsulta & "           , FSAL_pyme AS PYME" & vbCrLf
sConsulta = sConsulta & "           , producto AS PRODUCTO" & vbCrLf
sConsulta = sConsulta & "           ,usuario as USU" & vbCrLf
sConsulta = sConsulta & "           ,0" & vbCrLf
sConsulta = sConsulta & "            FROM FSAL_ACCESOS WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "          WHERE usuario <> ''" & vbCrLf
sConsulta = sConsulta & "          and CONVERT(datetime,CONVERT (char(10), FISERVER,112)) = CONVERT(datetime,CONVERT (char(10), dateadd(d,-1*(datepart(dw,FISERVER)-1),FISERVER), 112))" & vbCrLf
sConsulta = sConsulta & "          and NOT EXISTS ( SELECT * FROM FSAL_USUARIOS_DIS_SEMANA WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                           WHERE FSAL_USUARIOS_DIS_SEMANA.ANYO = datepart(""yy"",FSAL_ACCESOS.FISERVER)" & vbCrLf
sConsulta = sConsulta & "                               and FSAL_USUARIOS_DIS_SEMANA.MES = datepart(""mm"",FSAL_ACCESOS.FISERVER)" & vbCrLf
sConsulta = sConsulta & "                               and FSAL_USUARIOS_DIS_SEMANA.DIA = datepart(""dd"",CONVERT(datetime,CONVERT (char(10), dateadd(d,-1*(datepart(dw,FISERVER)-1),FISERVER), 112)))" & vbCrLf
sConsulta = sConsulta & "                               and FSAL_USUARIOS_DIS_SEMANA.ENTORNO = FSAL_ACCESOS.FSAL_ENTORNO" & vbCrLf
sConsulta = sConsulta & "                               and isnull(FSAL_USUARIOS_DIS_SEMANA.pyme,'') = isnull(FSAL_ACCESOS.FSAL_pyme,'')" & vbCrLf
sConsulta = sConsulta & "                               and FSAL_USUARIOS_DIS_SEMANA.producto = FSAL_ACCESOS.producto" & vbCrLf
sConsulta = sConsulta & "                               and FSAL_USUARIOS_DIS_SEMANA.usu = FSAL_ACCESOS.usuario)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO FSAL_USUARIOS_DIS_MES (ANYO,MES,ENTORNO,PYME,PRODUCTO,USU,TRASPASO)" & vbCrLf
sConsulta = sConsulta & "          SELECT DISTINCT " & vbCrLf
sConsulta = sConsulta & "           datepart(""yy"",FISERVER) AS ANYO" & vbCrLf
sConsulta = sConsulta & "           ,datepart(""mm"",FISERVER) AS MES" & vbCrLf
sConsulta = sConsulta & "           , FSAL_entorno AS ENTORNO" & vbCrLf
sConsulta = sConsulta & "           , FSAL_pyme AS PYME" & vbCrLf
sConsulta = sConsulta & "           , producto AS PRODUCTO" & vbCrLf
sConsulta = sConsulta & "           ,usuario as USU" & vbCrLf
sConsulta = sConsulta & "           ,0" & vbCrLf
sConsulta = sConsulta & "            FROM FSAL_ACCESOS WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "          WHERE usuario <> ''" & vbCrLf
sConsulta = sConsulta & "              and CONVERT(datetime,CONVERT (char(10), FISERVER,112)) = CONVERT(datetime,CONVERT (char(10), @Dia ,112))" & vbCrLf
sConsulta = sConsulta & "              and NOT EXISTS ( SELECT * FROM FSAL_USUARIOS_DIS_MES WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                           WHERE FSAL_USUARIOS_DIS_MES.ANYO = datepart(""yy"",FSAL_ACCESOS.FISERVER)" & vbCrLf
sConsulta = sConsulta & "                               and FSAL_USUARIOS_DIS_MES.MES = datepart(""mm"",FSAL_ACCESOS.FISERVER)" & vbCrLf
sConsulta = sConsulta & "                               and FSAL_USUARIOS_DIS_MES.ENTORNO = FSAL_ACCESOS.FSAL_ENTORNO" & vbCrLf
sConsulta = sConsulta & "                               and isnull(FSAL_USUARIOS_DIS_MES.pyme,'') = isnull(FSAL_ACCESOS.FSAL_pyme,'')" & vbCrLf
sConsulta = sConsulta & "                               and FSAL_USUARIOS_DIS_MES.producto = FSAL_ACCESOS.producto" & vbCrLf
sConsulta = sConsulta & "                               and FSAL_USUARIOS_DIS_MES.usu = FSAL_ACCESOS.usuario)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO FSAL_USUARIOS_DIS_ANYO (ANYO,ENTORNO,PYME,PRODUCTO,USU,TRASPASO)" & vbCrLf
sConsulta = sConsulta & "          SELECT DISTINCT " & vbCrLf
sConsulta = sConsulta & "           datepart(""yy"",FISERVER) AS ANYO" & vbCrLf
sConsulta = sConsulta & "           , FSAL_entorno AS ENTORNO" & vbCrLf
sConsulta = sConsulta & "           , FSAL_pyme AS PYME" & vbCrLf
sConsulta = sConsulta & "           , producto AS PRODUCTO" & vbCrLf
sConsulta = sConsulta & "           ,usuario as USU" & vbCrLf
sConsulta = sConsulta & "           ,0" & vbCrLf
sConsulta = sConsulta & "            FROM FSAL_ACCESOS WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "          WHERE usuario <> ''" & vbCrLf
sConsulta = sConsulta & "              and CONVERT(datetime,CONVERT (char(10), FISERVER,112)) = CONVERT(datetime,CONVERT (char(10), @Dia ,112))" & vbCrLf
sConsulta = sConsulta & "              and NOT EXISTS ( SELECT * FROM FSAL_USUARIOS_DIS_ANYO WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                           WHERE FSAL_USUARIOS_DIS_ANYO.ANYO = datepart(""yy"",FSAL_ACCESOS.FISERVER)" & vbCrLf
sConsulta = sConsulta & "                               and FSAL_USUARIOS_DIS_ANYO.ENTORNO = FSAL_ACCESOS.FSAL_ENTORNO" & vbCrLf
sConsulta = sConsulta & "                               and isnull(FSAL_USUARIOS_DIS_ANYO.pyme,'') = isnull(FSAL_ACCESOS.FSAL_pyme,'')" & vbCrLf
sConsulta = sConsulta & "                               and FSAL_USUARIOS_DIS_ANYO.producto = FSAL_ACCESOS.producto" & vbCrLf
sConsulta = sConsulta & "                               and FSAL_USUARIOS_DIS_ANYO.usu = FSAL_ACCESOS.usuario)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
            
            
sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_REGISTRAR_ACCESO_ASPX]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_REGISTRAR_ACCESO_ASPX]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta
            
            
sConsulta = "CREATE PROCEDURE [dbo].[FSAL_REGISTRAR_ACCESO_ASPX] @TIPO INT ,@PAGINA NVARCHAR(2000) ,@FPETICION VARCHAR(50), @FECHA varchar(50),@IDSESION VARCHAR(40),@USUCOD VARCHAR(50),@IPDIR VARCHAR(50),@CIACODGS VARCHAR(50)= NULL,@CIACOD VARCHAR(50)=NULL ,@PRODUCTO VARCHAR(50),@PAGINAORIGEN NVARCHAR(2000),@POSTBACK INT,@NAVEGADOR NVARCHAR(200)  AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @TSERVER INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @FISERVERDATE DATETIME" & vbCrLf
sConsulta = sConsulta & "-----" & vbCrLf
sConsulta = sConsulta & "DECLARE @FICLIRESPDATE DATETIME" & vbCrLf
sConsulta = sConsulta & "DECLARE @TRESPUESTA INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @TDOWNLOAD INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @ENTORNO VARCHAR(15)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT TOP 1 @ENTORNO=COD FROM FSAL_ENTORNOS WITH (NOLOCK) ORDER BY ID ASC" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @TIPO=1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @TSERVER = (SELECT DATEDIFF(MS, CAST(@FPETICION AS DATETIME),CAST(@FECHA AS DATETIME) ))" & vbCrLf
sConsulta = sConsulta & "INSERT INTO FSAL_ACCESOS(PAGINA,FSAL_ENTORNO,PRODUCTO, FISERVER,FFSERVER,TSERVER,PAGINAORIGEN,USUARIO, PROVE,IP,POSTBACK,HTTP_USER_AGENT) VALUES (@PAGINA,@ENTORNO,@PRODUCTO,CAST(@FPETICION AS DATETIME),CAST(@FECHA AS DATETIME),@TSERVER,@PAGINAORIGEN,ISNULL(@USUCOD,''),ISNULL(@CIACODGS,@CIACOD),@IPDIR,@POSTBACK,@NAVEGADOR)" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @TIPO=3" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @FISERVERDATE = (SELECT TOP 1 FFSERVER FROM FSAL_ACCESOS WITH (NOLOCK)WHERE FISERVER=CAST(@FPETICION AS DATETIME))" & vbCrLf
sConsulta = sConsulta & "SET @TSERVER = (SELECT DATEDIFF(MS, CAST(@FISERVERDATE AS DATETIME),CAST(@FECHA AS DATETIME) ))" & vbCrLf
sConsulta = sConsulta & "UPDATE FSAL_ACCESOS SET FICLIRESP= CAST(@FECHA AS DATETIME),TDOWNLOAD=@TSERVER WHERE FISERVER=CAST(@FPETICION AS DATETIME)" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "------" & vbCrLf
sConsulta = sConsulta & "IF @TIPO=4" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @FICLIRESPDATE = (SELECT TOP 1 FICLIRESP FROM FSAL_ACCESOS WITH (NOLOCK)WHERE FISERVER=CAST(@FPETICION AS DATETIME))" & vbCrLf
sConsulta = sConsulta & "SET @TRESPUESTA = (SELECT DATEDIFF(MS, CAST(@FICLIRESPDATE AS DATETIME),CAST(@FECHA AS DATETIME) ))" & vbCrLf
sConsulta = sConsulta & "SET @TSERVER =(SELECT TOP 1 TSERVER FROM FSAL_ACCESOS WITH (NOLOCK)WHERE FISERVER=CAST(@FPETICION AS DATETIME))" & vbCrLf
sConsulta = sConsulta & "SET @TDOWNLOAD =(SELECT TOP 1 TDOWNLOAD FROM FSAL_ACCESOS WITH (NOLOCK)WHERE FISERVER=CAST(@FPETICION AS DATETIME))" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE FSAL_ACCESOS SET FFCLIRESP= CAST (@FECHA AS DATETIME),TRESPUESTA=@TRESPUESTA ,TTOTAL=@TSERVER+@TDOWNLOAD+@TRESPUESTA WHERE FISERVER=CAST(@FPETICION AS DATETIME)" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

            


End Sub


Public Sub V_31900_8_Tablas_206()
    Dim sConsulta As String

    sConsulta = "ALTER TABLE PYMES ADD URLPORTAL NVARCHAR(255) NULL" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion31900_08_2006_A31900_08_2007() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim oADOConnection As ADODB.Connection
    Dim sConsulta As String

    On Error GoTo Error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Actualiza Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    frmProgreso.ProgressBar1.Refresh
    V_31900_8_Storeds_207
    
    sConsulta = "UPDATE VERSION SET NUM ='3.00.20.007'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103), PRODVER='31900.8'"
    ExecuteSQL gRDOCon, sConsulta
        
    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False
        
    CodigoDeActualizacion31900_08_2006_A31900_08_2007 = True
    Exit Function

Error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    CodigoDeActualizacion31900_08_2006_A31900_08_2007 = False
End Function

Public Sub V_31900_8_Storeds_207()
Dim sConsulta As String
        
sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSGS_RECEPCION_AUTOMATICA_ENTREGAS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSGS_RECEPCION_AUTOMATICA_ENTREGAS]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSGS_RECEPCION_AUTOMATICA_ENTREGAS] AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE @ORDEN_ID INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PEDIDO_ID INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ENTREGA_FECHA DATE" & vbCrLf
sConsulta = sConsulta & "DECLARE @ENTREGA_CANTIDAD FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ENTREGA_ID INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ENTREGA_ESTADO TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ENTREGA_NUM_PED_ERP VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @LINEA_PEDIDO_ID INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @LINEA_PEDIDO_CANT_PEDIDA FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @LINEA_PEDIDO_EST TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRECIO FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @MONPEDIDO NVARCHAR(10)" & vbCrLf
sConsulta = sConsulta & "DECLARE @RECEPTOR VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @ENTREGA_ANYO INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ENTREGA_NUM INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PEDIDO_NUM INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ENTREGA_EMPRESA INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @AUX_PEDIDO_ID INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @AUX_ENTREGA_FECHA DATE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @AUX_PEDIDO_RECEP INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @AUX_RECEP_FECHA DATETIME" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @CORRELATIVO INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @AUX_CORRELATIVO VARCHAR(50)   " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @GRABARENLOG TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @AUX_GRABARENLOG TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ORIGENGRABARENLOG TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ERP VARCHAR(4)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @AUX_LINEA_PEDIDO_RECEP INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @AUX_LOG_PEDIDO_RECEP INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @CANT_RECEPCIONADA FLOAT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @ESTADO_LINEA TINYINT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @AUX_ORDEN_ID INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @AUX_ENTREGA_ESTADO TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @SUMA_RECEP_CANT_ORDEN FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @SUMA_PEDIDA_CANT_ORDEN FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @AUX_ESTADO_ORDEN INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @AUX_ORDEN_EST_ID INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @HAY_TRANSACC TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @SALTO_ERROR TINYINT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @AUX_INTEGRA_PEDIDO_DIRECTO TINYINT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @ALBARAN NVARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "----Acumular SM" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRES5IMP INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRES0 NVARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRES4 NVARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRES3 NVARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRES2 NVARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRES1 NVARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @CC4 NVARCHAR(10)" & vbCrLf
sConsulta = sConsulta & "DECLARE @CC3 NVARCHAR(10)" & vbCrLf
sConsulta = sConsulta & "DECLARE @CC2 NVARCHAR(10)" & vbCrLf
sConsulta = sConsulta & "DECLARE @CC1 NVARCHAR(10)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @COMP FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @MON NVARCHAR(10)" & vbCrLf
sConsulta = sConsulta & "DECLARE @CAMBIO FLOAT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @COSTESLINEA FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @DESCUENTOSLINEA FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @COSTESORDEN FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @DESCUENTOSORDEN FLOAT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @IMPMODO TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @IMPORTELINEA FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @IMPORTEBRUTOORDEN FLOAT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--GRABARENLOG" & vbCrLf
sConsulta = sConsulta & "SET @GRABARENLOG = 0" & vbCrLf
sConsulta = sConsulta & "SET @AUX_GRABARENLOG=0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @GRABARENLOG = PARGEN_GEST.ACTIVLOG FROM PARGEN_GEST WITH(NOLOCK) WHERE ID=1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF (SELECT COUNT(1) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA =14 AND ACTIVA=1 AND NOT(SENTIDO=2))>0" & vbCrLf
sConsulta = sConsulta & "   SET @AUX_GRABARENLOG=1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF (@GRABARENLOG=1)    OR (@AUX_GRABARENLOG=1) " & vbCrLf
sConsulta = sConsulta & "BEGIN  " & vbCrLf
sConsulta = sConsulta & "   IF (@GRABARENLOG=1) AND (@AUX_GRABARENLOG=1)" & vbCrLf
sConsulta = sConsulta & "   BEGIN " & vbCrLf
sConsulta = sConsulta & "       SET @ORIGENGRABARENLOG = 2" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "   BEGIN " & vbCrLf
sConsulta = sConsulta & "       IF (@GRABARENLOG=1) " & vbCrLf
sConsulta = sConsulta & "           SET @ORIGENGRABARENLOG = 1" & vbCrLf
sConsulta = sConsulta & "       ELSE" & vbCrLf
sConsulta = sConsulta & "           SET @ORIGENGRABARENLOG = 0" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   SET @GRABARENLOG=1" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "--FIN GRABARENLOG" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--SISTEMAS CON INTEGRACI�N DE PEDIDOS DIRECTOS " & vbCrLf
sConsulta = sConsulta & "SET @AUX_INTEGRA_PEDIDO_DIRECTO=0" & vbCrLf
sConsulta = sConsulta & "IF (SELECT COUNT(1) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA =13 AND ACTIVA=1 AND NOT(SENTIDO=2))>0" & vbCrLf
sConsulta = sConsulta & "   SET @AUX_INTEGRA_PEDIDO_DIRECTO=1" & vbCrLf
sConsulta = sConsulta & "--FIN SISTEMAS CON INTEGRACI�N DE PEDIDOS DIRECTOS " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @AUX_INTEGRA_PEDIDO_DIRECTO=1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   DECLARE curPEDIDOS CURSOR LOCAL FOR     " & vbCrLf
sConsulta = sConsulta & "   SELECT DISTINCT O.ID, P.ID , LPE.FECHA, LPE.CANTIDAD , LPE.ID, LP.ID, LP.CANT_PED, O.ANYO, O.NUM, P.NUM, O.EMPRESA, O.EST ESTORDEN, O.NUM_PED_ERP" & vbCrLf
sConsulta = sConsulta & "   , LP.PREC_UP, O.MON, LP.COSTES, LP.DESCUENTOS, O.COSTES, O.DESCUENTOS, O.IMPORTE - O.COSTES + O.DESCUENTOS AS BRUTOORDEN, O.RECEPTOR" & vbCrLf
sConsulta = sConsulta & "   FROM ORDEN_ENTREGA O WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PEDIDO P WITH(NOLOCK) ON O.PEDIDO=P.ID" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN LINEAS_PEDIDO LP WITH(NOLOCK) ON  LP.ORDEN=O.ID" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN LINEAS_PEDIDO_ENTREGAS LPE WITH(NOLOCK) ON LPE.LINEA_PEDIDO=LP.ID" & vbCrLf
sConsulta = sConsulta & "   WHERE LP.IM_RECEPAUTO=1" & vbCrLf
sConsulta = sConsulta & "   AND CONVERT(DATE,LPE.FECHA,103)<=GETDATE()" & vbCrLf
sConsulta = sConsulta & "   AND ((O.ACEP_PROVE=1 AND O.EST>=3 AND O.EST<=5) OR (O.ACEP_PROVE=0 AND O.EST>=2 AND O.EST<=5))" & vbCrLf
sConsulta = sConsulta & "   AND LPE.ALBARAN IS NULL" & vbCrLf
sConsulta = sConsulta & "   AND 4=(SELECT LOG_GRAL.ESTADO FROM LOG_GRAL WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "       WHERE LOG_GRAL.ID =  (SELECT ISNULL(MAX(G.ID),0) FROM LOG_GRAL G WITH (NOLOCK)  " & vbCrLf
sConsulta = sConsulta & "               INNER JOIN LOG_ORDEN_ENTREGA LO WITH (NOLOCK) ON G.ID_TABLA=LO.ID  " & vbCrLf
sConsulta = sConsulta & "               WHERE (TABLA=13 OR TABLA=11) " & vbCrLf
sConsulta = sConsulta & "               AND G.ORIGEN=0 AND (LO.ACCION='I' OR LO.ACCION='U' OR LO.ACCION='R') " & vbCrLf
sConsulta = sConsulta & "               AND LO.ID_ORDEN_ENTREGA = O.ID)" & vbCrLf
sConsulta = sConsulta & "       )       " & vbCrLf
sConsulta = sConsulta & "   ORDER BY O.ID, P.ID, LPE.FECHA, LPE.ID" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   DECLARE curPEDIDOS CURSOR LOCAL FOR     " & vbCrLf
sConsulta = sConsulta & "   SELECT DISTINCT O.ID, P.ID , LPE.FECHA, LPE.CANTIDAD , LPE.ID, LP.ID, LP.CANT_PED, O.ANYO, O.NUM, P.NUM, O.EMPRESA, O.EST ESTORDEN, O.NUM_PED_ERP" & vbCrLf
sConsulta = sConsulta & "   , LP.PREC_UP, O.MON, LP.COSTES, LP.DESCUENTOS, O.COSTES, O.DESCUENTOS, O.IMPORTE - O.COSTES + O.DESCUENTOS AS BRUTOORDEN, O.RECEPTOR" & vbCrLf
sConsulta = sConsulta & "   FROM ORDEN_ENTREGA O WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PEDIDO P WITH(NOLOCK) ON O.PEDIDO=P.ID" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN LINEAS_PEDIDO LP WITH(NOLOCK) ON  LP.ORDEN=O.ID" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN LINEAS_PEDIDO_ENTREGAS LPE WITH(NOLOCK) ON LPE.LINEA_PEDIDO=LP.ID" & vbCrLf
sConsulta = sConsulta & "   WHERE LP.IM_RECEPAUTO=1" & vbCrLf
sConsulta = sConsulta & "   AND CONVERT(DATE,LPE.FECHA,103)<=GETDATE()" & vbCrLf
sConsulta = sConsulta & "   AND ((O.ACEP_PROVE=1 AND O.EST>=3 AND O.EST<=5) OR (O.ACEP_PROVE=0 AND O.EST>=2 AND O.EST<=5))" & vbCrLf
sConsulta = sConsulta & "   AND LPE.ALBARAN IS NULL" & vbCrLf
sConsulta = sConsulta & "   ORDER BY O.ID, P.ID, LPE.FECHA, LPE.ID" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "OPEN curPEDIDOS" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curPEDIDOS INTO @ORDEN_ID,@PEDIDO_ID, @ENTREGA_FECHA,@ENTREGA_CANTIDAD,@ENTREGA_ID" & vbCrLf
sConsulta = sConsulta & ",@LINEA_PEDIDO_ID,@LINEA_PEDIDO_CANT_PEDIDA,@ENTREGA_ANYO,@ENTREGA_NUM,@PEDIDO_NUM,@ENTREGA_EMPRESA,@ENTREGA_ESTADO" & vbCrLf
sConsulta = sConsulta & ",@ENTREGA_NUM_PED_ERP, @PRECIO, @MONPEDIDO, @COSTESLINEA, @DESCUENTOSLINEA, @COSTESORDEN, @DESCUENTOSORDEN,@IMPORTEBRUTOORDEN,@RECEPTOR" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @ALBARAN=N''" & vbCrLf
sConsulta = sConsulta & "SET @AUX_PEDIDO_ID = 0" & vbCrLf
sConsulta = sConsulta & "SET @AUX_ENTREGA_FECHA = '01/01/1900'" & vbCrLf
sConsulta = sConsulta & "SET @SALTO_ERROR =0" & vbCrLf
sConsulta = sConsulta & "SET @HAY_TRANSACC=0" & vbCrLf
sConsulta = sConsulta & "SET @AUX_ORDEN_ID=@ORDEN_ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN  " & vbCrLf
sConsulta = sConsulta & "   IF (@SALTO_ERROR=0)" & vbCrLf
sConsulta = sConsulta & "   BEGIN        " & vbCrLf
sConsulta = sConsulta & "       IF NOT(@AUX_ORDEN_ID=@ORDEN_ID)     " & vbCrLf
sConsulta = sConsulta & "       BEGIN       " & vbCrLf
sConsulta = sConsulta & "           SELECT @SUMA_RECEP_CANT_ORDEN =SUM(LINEAS_PEDIDO.CANT_REC), @SUMA_PEDIDA_CANT_ORDEN=SUM(LINEAS_PEDIDO.CANT_PED) " & vbCrLf
sConsulta = sConsulta & "           FROM LINEAS_PEDIDO WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "           WHERE LINEAS_PEDIDO.ORDEN = @AUX_ORDEN_ID" & vbCrLf
sConsulta = sConsulta & "           IF @SUMA_PEDIDA_CANT_ORDEN < @SUMA_RECEP_CANT_ORDEN" & vbCrLf
sConsulta = sConsulta & "           BEGIN" & vbCrLf
sConsulta = sConsulta & "               SET @AUX_ESTADO_ORDEN = 5" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "           ELSE" & vbCrLf
sConsulta = sConsulta & "           BEGIN" & vbCrLf
sConsulta = sConsulta & "               IF @SUMA_PEDIDA_CANT_ORDEN > @SUMA_RECEP_CANT_ORDEN" & vbCrLf
sConsulta = sConsulta & "                   SET @AUX_ESTADO_ORDEN = 5" & vbCrLf
sConsulta = sConsulta & "               ELSE" & vbCrLf
sConsulta = sConsulta & "                   SET @AUX_ESTADO_ORDEN = 6" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "           IF NOT(@AUX_ESTADO_ORDEN= @AUX_ENTREGA_ESTADO)" & vbCrLf
sConsulta = sConsulta & "           BEGIN       " & vbCrLf
sConsulta = sConsulta & "               IF @AUX_ESTADO_ORDEN=6" & vbCrLf
sConsulta = sConsulta & "               BEGIN                                                   " & vbCrLf
sConsulta = sConsulta & "                   IF (SELECT COUNT(1) FROM ORDEN_EST WITH (NOLOCK) WHERE PEDIDO =@AUX_PEDIDO_ID AND EST=5)=0" & vbCrLf
sConsulta = sConsulta & "                   BEGIN" & vbCrLf
sConsulta = sConsulta & "                       INSERT INTO ORDEN_EST (PEDIDO, ORDEN, EST, FECHA, PER) VALUES (@AUX_PEDIDO_ID,@AUX_ORDEN_ID,5,@AUX_RECEP_FECHA,NULL)" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               END" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "           INSERT INTO ORDEN_EST (PEDIDO,ORDEN,EST,FECHA,PER) VALUES (@AUX_PEDIDO_ID,@AUX_ORDEN_ID,@AUX_ESTADO_ORDEN,@AUX_RECEP_FECHA,NULL)" & vbCrLf
sConsulta = sConsulta & "           --Calidad: Sin With(No lock) pq es importante coger los ids correctos" & vbCrLf
sConsulta = sConsulta & "           SELECT @AUX_ORDEN_EST_ID=MAX(ID) FROM ORDEN_EST WHERE PEDIDO=@AUX_PEDIDO_ID" & vbCrLf
sConsulta = sConsulta & "           UPDATE ORDEN_ENTREGA SET EST =@AUX_ESTADO_ORDEN, ORDEN_EST =@AUX_ORDEN_EST_ID WHERE ID = @AUX_ORDEN_ID      " & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "           IF (@HAY_TRANSACC=1)" & vbCrLf
sConsulta = sConsulta & "           BEGIN" & vbCrLf
sConsulta = sConsulta & "               SET @HAY_TRANSACC =0" & vbCrLf
sConsulta = sConsulta & "               COMMIT TRAN" & vbCrLf
sConsulta = sConsulta & "           END " & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF NOT(@AUX_ORDEN_ID=@ORDEN_ID) OR NOT(@AUX_ENTREGA_FECHA=@ENTREGA_FECHA)" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       --Otro pedido o fecha -> es otra recepci�n. Otra transacci�n." & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "       IF (@HAY_TRANSACC=1)" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           COMMIT TRAN" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "       BEGIN TRAN" & vbCrLf
sConsulta = sConsulta & "       SET @HAY_TRANSACC=1" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "       SET @SALTO_ERROR =0" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "       SET @AUX_RECEP_FECHA = CONVERT(VARCHAR(19), GETDATE(), 120)" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "       IF @AUX_INTEGRA_PEDIDO_DIRECTO=1" & vbCrLf
sConsulta = sConsulta & "       BEGIN       " & vbCrLf
sConsulta = sConsulta & "           SET @ALBARAN = @ENTREGA_NUM_PED_ERP " & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "       ELSE        " & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           SET @ALBARAN= CONVERT(VARCHAR(4),@ENTREGA_ANYO) + '-' + CONVERT(VARCHAR(10),@PEDIDO_NUM) + '-' + CONVERT(VARCHAR(10),@ENTREGA_NUM)" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "       END     " & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "       IF (@CORRELATIVO IS NULL )" & vbCrLf
sConsulta = sConsulta & "           SET @CORRELATIVO=1" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "       IF NOT(@AUX_ORDEN_ID=@ORDEN_ID)" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           --Calidad: Sin With(No lock) pq es importante coger los ids correctos y el correlativo va a formar parte de ids" & vbCrLf
sConsulta = sConsulta & "           SELECT @AUX_CORRELATIVO=MAX(ALBARAN) FROM PEDIDO_RECEP WHERE ALBARAN LIKE @ALBARAN + '%'" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "           IF (@AUX_CORRELATIVO IS NULL)" & vbCrLf
sConsulta = sConsulta & "               SET @CORRELATIVO = 1" & vbCrLf
sConsulta = sConsulta & "           ELSE" & vbCrLf
sConsulta = sConsulta & "           BEGIN" & vbCrLf
sConsulta = sConsulta & "               SET @CORRELATIVO = CONVERT(INT,RIGHT(@AUX_CORRELATIVO,LEN(@AUX_CORRELATIVO)-LEN(@ALBARAN)-4))" & vbCrLf
sConsulta = sConsulta & "               " & vbCrLf
sConsulta = sConsulta & "               SET @CORRELATIVO = @CORRELATIVO + 1 " & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "       END         " & vbCrLf
sConsulta = sConsulta & "       IF @AUX_INTEGRA_PEDIDO_DIRECTO=1" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           IF MONTH(@ENTREGA_FECHA)<10" & vbCrLf
sConsulta = sConsulta & "               SET @ALBARAN= @ALBARAN + '0' + CONVERT(VARCHAR(1),MONTH(@ENTREGA_FECHA))" & vbCrLf
sConsulta = sConsulta & "           ELSE" & vbCrLf
sConsulta = sConsulta & "               SET @ALBARAN= @ALBARAN + CONVERT(VARCHAR(2),MONTH(@ENTREGA_FECHA))      " & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "       ELSE" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           IF MONTH(@ENTREGA_FECHA)<10" & vbCrLf
sConsulta = sConsulta & "               SET @ALBARAN= @ALBARAN + '-0' + CONVERT(VARCHAR(1),MONTH(@ENTREGA_FECHA))" & vbCrLf
sConsulta = sConsulta & "           ELSE" & vbCrLf
sConsulta = sConsulta & "               SET @ALBARAN= @ALBARAN + '-' + CONVERT(VARCHAR(2),MONTH(@ENTREGA_FECHA))" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "       SET @ALBARAN= @ALBARAN + RIGHT(CONVERT(VARCHAR(4),YEAR(@ENTREGA_FECHA)),2)" & vbCrLf
sConsulta = sConsulta & "               " & vbCrLf
sConsulta = sConsulta & "       SET @AUX_PEDIDO_ID = @PEDIDO_ID" & vbCrLf
sConsulta = sConsulta & "       SET @AUX_ORDEN_ID = @ORDEN_ID" & vbCrLf
sConsulta = sConsulta & "       SET @AUX_ENTREGA_FECHA=@ENTREGA_FECHA" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "       IF @CORRELATIVO<10" & vbCrLf
sConsulta = sConsulta & "           SET @ALBARAN= @ALBARAN + '0' + CONVERT(VARCHAR(1),@CORRELATIVO)" & vbCrLf
sConsulta = sConsulta & "       ELSE" & vbCrLf
sConsulta = sConsulta & "           SET @ALBARAN= @ALBARAN + CONVERT(VARCHAR(10),@CORRELATIVO)          " & vbCrLf
sConsulta = sConsulta & "                       " & vbCrLf
sConsulta = sConsulta & "       INSERT INTO PEDIDO_RECEP (PEDIDO,ORDEN,FECHA,ALBARAN,CORRECTO,COMENT,RECEPTOR,IM_BLOQUEO) " & vbCrLf
sConsulta = sConsulta & "       VALUES (@PEDIDO_ID,@ORDEN_ID,@AUX_RECEP_FECHA,@ALBARAN,1,NULL,@RECEPTOR,0)" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "       --Calidad: Sin With(No lock) pq es importante coger los ids correctos" & vbCrLf
sConsulta = sConsulta & "       SELECT @AUX_PEDIDO_RECEP=MAX(ID) FROM PEDIDO_RECEP WHERE PEDIDO=@PEDIDO_ID" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "       SET @CORRELATIVO = @CORRELATIVO + 1" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "       IF (@GRABARENLOG=1)" & vbCrLf
sConsulta = sConsulta & "       BEGIN " & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "           SET @ERP =(SELECT DISTINCT ERP = CASE WHEN EO.ERP IS NULL THEN ERP_SOCIEDAD.ERP ELSE EO.ERP END " & vbCrLf
sConsulta = sConsulta & "               FROM EMP WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "               LEFT JOIN ERP_SOCIEDAD WITH (NOLOCK) ON ERP_SOCIEDAD.SOCIEDAD = EMP.SOCIEDAD" & vbCrLf
sConsulta = sConsulta & "               LEFT JOIN (ERP_ORGCOMPRAS EO WITH(NOLOCK) INNER JOIN SOCIEDAD_ORGCOMPRAS SO WITH(NOLOCK) ON SO.ORGCOMPRAS=EO.ORGCOMPRAS) ON SO.SOCIEDAD = EMP.SOCIEDAD" & vbCrLf
sConsulta = sConsulta & "               WHERE EMP.ID = @ENTREGA_EMPRESA)    " & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "           INSERT INTO LOG_PEDIDO_RECEP (ACCION,ID_PEDIDO_RECEP,ID_PEDIDO,ID_ORDEN_ENTREGA,TIPO,FECHA,ALBARAN,CORRECTO,COMENT,ORIGEN,PER,USU,ERP) " & vbCrLf
sConsulta = sConsulta & "           VALUES ('I',@AUX_PEDIDO_RECEP,@PEDIDO_ID,@ORDEN_ID,0,@AUX_RECEP_FECHA,@ALBARAN,1,NULL,@ORIGENGRABARENLOG,NULL,NULL,@ERP)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "           --Calidad: Sin With(No lock) pq es importante coger los ids correctos" & vbCrLf
sConsulta = sConsulta & "           SELECT @AUX_LOG_PEDIDO_RECEP=MAX(ID) FROM LOG_PEDIDO_RECEP WHERE ID_PEDIDO_RECEP=@AUX_PEDIDO_RECEP          " & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "   IF (@SALTO_ERROR=0)" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "       INSERT INTO LINEAS_RECEP (ORDEN,PEDIDO_RECEP,LINEA,CANT) VALUES (@ORDEN_ID,@AUX_PEDIDO_RECEP,@LINEA_PEDIDO_ID,@ENTREGA_CANTIDAD )" & vbCrLf
sConsulta = sConsulta & "       --Calidad: Sin With(No lock) pq es importante coger los ids correctos" & vbCrLf
sConsulta = sConsulta & "       SELECT @AUX_LINEA_PEDIDO_RECEP=MAX(ID) FROM LINEAS_RECEP WHERE LINEA=@LINEA_PEDIDO_ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       ---------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "       --      Acumular SM     --          " & vbCrLf
sConsulta = sConsulta & "       ---------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "       DECLARE CURLPI CURSOR LOCAL FOR SELECT DISTINCT PRES5_IMP, PRES0, PRES1, ISNULL(PRES2,''), ISNULL(PRES3,''), ISNULL(PRES4,''), UON1, UON2, UON3, UON4, IMPMODO" & vbCrLf
sConsulta = sConsulta & "       FROM LINEAS_PED_IMPUTACION LPI WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PARGEN_SM WITH (NOLOCK) ON COMPR_ACUM=2 AND PRES5=LPI.PRES0" & vbCrLf
sConsulta = sConsulta & "       WHERE LPI.LINEA=@LINEA_PEDIDO_ID" & vbCrLf
sConsulta = sConsulta & "       --Acum. por linea IMPMODO=0" & vbCrLf
sConsulta = sConsulta & "       --Acum. por cabecera IMPMODO=1" & vbCrLf
sConsulta = sConsulta & "       -- Importe neto = (cant entrega * precio lin ped) + (coste lin ped * (%cant entrega respecto cant_ped)) - (descuento lin ped * (%cant entrega respecto cant_ped))" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "       --No se filtra por impmodo=1 pq se debe insertar en LINEAS_RECEP_IMPUTACION todas las recepciones y esta insert necesita datos de insert LINEAS_RECEP" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "       OPEN CURLPI" & vbCrLf
sConsulta = sConsulta & "       FETCH NEXT FROM CURLPI INTO @PRES5IMP, @PRES0, @PRES1, @PRES2, @PRES3, @PRES4, @CC1, @CC2, @CC3, @CC4, @IMPMODO" & vbCrLf
sConsulta = sConsulta & "               " & vbCrLf
sConsulta = sConsulta & "       WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           IF NOT @PRES0 IS NULL" & vbCrLf
sConsulta = sConsulta & "           BEGIN" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "               IF NOT @PRES5IMP IS NULL" & vbCrLf
sConsulta = sConsulta & "               BEGIN" & vbCrLf
sConsulta = sConsulta & "                   SELECT @COMP=ISNULL(COMP,0),@MON=ISNULL(MON,''),@CAMBIO=ISNULL(EQUIV,1) " & vbCrLf
sConsulta = sConsulta & "                   FROM PRES5_IMPORTES WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                   LEFT JOIN MON WITH (NOLOCK) ON PRES5_IMPORTES.MON=MON.COD  " & vbCrLf
sConsulta = sConsulta & "                   WHERE CERRADO=0 AND PRES5_IMPORTES.ID = @PRES5IMP" & vbCrLf
sConsulta = sConsulta & "               " & vbCrLf
sConsulta = sConsulta & "                   IF @MON=''  " & vbCrLf
sConsulta = sConsulta & "                   BEGIN                                               " & vbCrLf
sConsulta = sConsulta & "                       SELECT @MON=MONCEN FROM PARGEN_DEF WITH (NOLOCK) WHERE ID=1" & vbCrLf
sConsulta = sConsulta & "                       SET @CAMBIO=1" & vbCrLf
sConsulta = sConsulta & "                   END         " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                   SET @IMPORTELINEA = @ENTREGA_CANTIDAD * @PRECIO" & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                   SET @IMPORTELINEA = @IMPORTELINEA + (@COSTESLINEA * (@ENTREGA_CANTIDAD / @LINEA_PEDIDO_CANT_PEDIDA))" & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                   SET @IMPORTELINEA = @IMPORTELINEA - (@DESCUENTOSLINEA * (@ENTREGA_CANTIDAD / @LINEA_PEDIDO_CANT_PEDIDA))" & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                   SET @COMP = @COMP + (@IMPORTELINEA*@CAMBIO)" & vbCrLf
sConsulta = sConsulta & "                                                           " & vbCrLf
sConsulta = sConsulta & "                   -----------------------         " & vbCrLf
sConsulta = sConsulta & "                   INSERT INTO LINEAS_RECEP_IMPUTACION (LINEA_RECEP,UON1,UON2,UON3,UON4,PRES5_IMP,CAMBIO) " & vbCrLf
sConsulta = sConsulta & "                   VALUES (@AUX_LINEA_PEDIDO_RECEP, @CC1, @CC2, @CC3, @CC4, @PRES5IMP, @CAMBIO)" & vbCrLf
sConsulta = sConsulta & "                                                           " & vbCrLf
sConsulta = sConsulta & "                   IF @IMPMODO=1" & vbCrLf
sConsulta = sConsulta & "                   BEGIN                                   " & vbCrLf
sConsulta = sConsulta & "                       IF @IMPORTELINEA <> 0 " & vbCrLf
sConsulta = sConsulta & "                           UPDATE PRES5_IMPORTES SET COMP=@COMP WHERE ID=@PRES5IMP                                                 " & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                   IF @IMPMODO=0" & vbCrLf
sConsulta = sConsulta & "                   BEGIN" & vbCrLf
sConsulta = sConsulta & "                       SET @COMP = @COMP + (@COSTESORDEN * (@IMPORTELINEA / @IMPORTEBRUTOORDEN))" & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                       SET @COMP = @COMP - (@DESCUENTOSORDEN * (@IMPORTELINEA / @IMPORTEBRUTOORDEN))" & vbCrLf
sConsulta = sConsulta & "                       " & vbCrLf
sConsulta = sConsulta & "                       If @IMPORTELINEA <> 0 " & vbCrLf
sConsulta = sConsulta & "                           UPDATE PRES5_IMPORTES SET COMP=@COMP WHERE ID=@PRES5IMP                         " & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                   END                 " & vbCrLf
sConsulta = sConsulta & "               END" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "           FETCH NEXT FROM CURLPI INTO @PRES5IMP,@PRES0,@PRES1, @PRES2, @PRES3, @PRES4, @CC1, @CC2, @CC3, @CC4, @IMPMODO" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       CLOSE CURLPI" & vbCrLf
sConsulta = sConsulta & "       DEALLOCATE CURLPI       " & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "       ---------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "       --      Fin Acumular SM     --          " & vbCrLf
sConsulta = sConsulta & "       ---------------------------------------------------------------------------------------------------------           " & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "       IF (@GRABARENLOG=1)" & vbCrLf
sConsulta = sConsulta & "       BEGIN   " & vbCrLf
sConsulta = sConsulta & "           INSERT INTO LOG_LINEAS_RECEP (ID_LOG_PEDIDO_RECEP,ID_PEDIDO_RECEP,ID_ORDEN_ENTREGA,ID_LINEA_RECEP,ID_LINEA_PEDIDO,CANT,ACCION) " & vbCrLf
sConsulta = sConsulta & "           VALUES (@AUX_LOG_PEDIDO_RECEP,@AUX_PEDIDO_RECEP,@ORDEN_ID,@AUX_LINEA_PEDIDO_RECEP,@LINEA_PEDIDO_ID,@ENTREGA_CANTIDAD,'I')" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "       SELECT @CANT_RECEPCIONADA=SUM(CANT) FROM LINEAS_RECEP WITH (NOLOCK) WHERE LINEA =@LINEA_PEDIDO_ID" & vbCrLf
sConsulta = sConsulta & "       IF @CANT_RECEPCIONADA IS NULL " & vbCrLf
sConsulta = sConsulta & "           SET @CANT_RECEPCIONADA=0" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "       IF @CANT_RECEPCIONADA=0" & vbCrLf
sConsulta = sConsulta & "           SET @ESTADO_LINEA = 0" & vbCrLf
sConsulta = sConsulta & "       ELSE" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           IF @LINEA_PEDIDO_CANT_PEDIDA >=0" & vbCrLf
sConsulta = sConsulta & "           BEGIN" & vbCrLf
sConsulta = sConsulta & "               IF @LINEA_PEDIDO_CANT_PEDIDA>@CANT_RECEPCIONADA" & vbCrLf
sConsulta = sConsulta & "                   SET @ESTADO_LINEA = 2" & vbCrLf
sConsulta = sConsulta & "               ELSE    " & vbCrLf
sConsulta = sConsulta & "                   IF @LINEA_PEDIDO_CANT_PEDIDA=@CANT_RECEPCIONADA" & vbCrLf
sConsulta = sConsulta & "                       SET @ESTADO_LINEA = 3" & vbCrLf
sConsulta = sConsulta & "                   ELSE" & vbCrLf
sConsulta = sConsulta & "                   BEGIN" & vbCrLf
sConsulta = sConsulta & "                       IF @LINEA_PEDIDO_CANT_PEDIDA>0" & vbCrLf
sConsulta = sConsulta & "                       BEGIN" & vbCrLf
sConsulta = sConsulta & "                           --Tratamiento de error " & vbCrLf
sConsulta = sConsulta & "                           --  podr�a hacer rollback y continuar hasta q haya cambio de orden/pedido" & vbCrLf
sConsulta = sConsulta & "                           --  antes de otro begin tran" & vbCrLf
sConsulta = sConsulta & "                           SET @SALTO_ERROR= 1 " & vbCrLf
sConsulta = sConsulta & "                           SET @HAY_TRANSACC= 0                        " & vbCrLf
sConsulta = sConsulta & "                           ROLLBACK TRAN" & vbCrLf
sConsulta = sConsulta & "                       END" & vbCrLf
sConsulta = sConsulta & "                       ELSE" & vbCrLf
sConsulta = sConsulta & "                           SET @ESTADO_LINEA = 2" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "           END         " & vbCrLf
sConsulta = sConsulta & "           ELSE" & vbCrLf
sConsulta = sConsulta & "           BEGIN" & vbCrLf
sConsulta = sConsulta & "               IF @LINEA_PEDIDO_CANT_PEDIDA<@CANT_RECEPCIONADA" & vbCrLf
sConsulta = sConsulta & "                   SET @ESTADO_LINEA = 2" & vbCrLf
sConsulta = sConsulta & "               ELSE    " & vbCrLf
sConsulta = sConsulta & "                   IF @LINEA_PEDIDO_CANT_PEDIDA=@CANT_RECEPCIONADA" & vbCrLf
sConsulta = sConsulta & "                       SET @ESTADO_LINEA = 3" & vbCrLf
sConsulta = sConsulta & "                   ELSE" & vbCrLf
sConsulta = sConsulta & "                   BEGIN" & vbCrLf
sConsulta = sConsulta & "                       --Tratamiento de error " & vbCrLf
sConsulta = sConsulta & "                       --  podr�a hacer rollback y continuar hasta q haya cambio de orden/pedido" & vbCrLf
sConsulta = sConsulta & "                       --  antes de otro begin tran" & vbCrLf
sConsulta = sConsulta & "                       SET @SALTO_ERROR= 1 " & vbCrLf
sConsulta = sConsulta & "                       SET @HAY_TRANSACC= 0                        " & vbCrLf
sConsulta = sConsulta & "                       ROLLBACK TRAN" & vbCrLf
sConsulta = sConsulta & "                   END                 " & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   IF @SALTO_ERROR=0" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       SELECT @LINEA_PEDIDO_EST=ISNULL(EST,0) FROM LINEAS_PEDIDO WITH(NOLOCK) WHERE ID = @LINEA_PEDIDO_ID" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "       UPDATE LINEAS_PEDIDO SET EST = @ESTADO_LINEA,CANT_REC = @CANT_RECEPCIONADA WHERE ID = @LINEA_PEDIDO_ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       IF NOT(@LINEA_PEDIDO_EST = @ESTADO_LINEA)" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           INSERT INTO LINEAS_EST (PEDIDO,ORDEN,LINEA,EST,FECHA,PER) VALUES(@PEDIDO_ID,@ORDEN_ID,@LINEA_PEDIDO_ID,@ESTADO_LINEA,@AUX_RECEP_FECHA,NULL)" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "       UPDATE LINEAS_PEDIDO_ENTREGAS SET ALBARAN=@ALBARAN WHERE ID=@ENTREGA_ID" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   SET @AUX_ORDEN_ID=@ORDEN_ID" & vbCrLf
sConsulta = sConsulta & "   SET @AUX_ENTREGA_ESTADO=@ENTREGA_ESTADO" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   FETCH NEXT FROM curPEDIDOS INTO @ORDEN_ID,@PEDIDO_ID, @ENTREGA_FECHA,@ENTREGA_CANTIDAD,@ENTREGA_ID" & vbCrLf
sConsulta = sConsulta & "   ,@LINEA_PEDIDO_ID,@LINEA_PEDIDO_CANT_PEDIDA ,@ENTREGA_ANYO,@ENTREGA_NUM,@PEDIDO_NUM,@ENTREGA_EMPRESA,@ENTREGA_ESTADO" & vbCrLf
sConsulta = sConsulta & "   , @ENTREGA_NUM_PED_ERP, @PRECIO, @MONPEDIDO, @COSTESLINEA, @DESCUENTOSLINEA, @COSTESORDEN, @DESCUENTOSORDEN,@IMPORTEBRUTOORDEN,@RECEPTOR" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF (@SALTO_ERROR=0) AND (@HAY_TRANSACC =1)" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SELECT @SUMA_RECEP_CANT_ORDEN =SUM(LINEAS_PEDIDO.CANT_REC), @SUMA_PEDIDA_CANT_ORDEN=SUM(LINEAS_PEDIDO.CANT_PED) " & vbCrLf
sConsulta = sConsulta & "   FROM LINEAS_PEDIDO WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   WHERE LINEAS_PEDIDO.ORDEN = @AUX_ORDEN_ID" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   IF @SUMA_PEDIDA_CANT_ORDEN < @SUMA_RECEP_CANT_ORDEN" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       SET @AUX_ESTADO_ORDEN = 5" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       IF @SUMA_PEDIDA_CANT_ORDEN > @SUMA_RECEP_CANT_ORDEN" & vbCrLf
sConsulta = sConsulta & "           SET @AUX_ESTADO_ORDEN = 5" & vbCrLf
sConsulta = sConsulta & "       ELSE" & vbCrLf
sConsulta = sConsulta & "           SET @AUX_ESTADO_ORDEN = 6" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   IF NOT(@AUX_ESTADO_ORDEN= @AUX_ENTREGA_ESTADO)" & vbCrLf
sConsulta = sConsulta & "   BEGIN       " & vbCrLf
sConsulta = sConsulta & "       IF @AUX_ESTADO_ORDEN=6" & vbCrLf
sConsulta = sConsulta & "       BEGIN                       " & vbCrLf
sConsulta = sConsulta & "           IF (SELECT COUNT(1) FROM ORDEN_EST WITH (NOLOCK) WHERE PEDIDO =@AUX_PEDIDO_ID AND EST=5)=0" & vbCrLf
sConsulta = sConsulta & "           BEGIN" & vbCrLf
sConsulta = sConsulta & "               INSERT INTO ORDEN_EST (PEDIDO, ORDEN, EST, FECHA, PER) VALUES (@AUX_PEDIDO_ID,@AUX_ORDEN_ID,5,@AUX_RECEP_FECHA,NULL)" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO ORDEN_EST (PEDIDO,ORDEN,EST,FECHA,PER) VALUES (@AUX_PEDIDO_ID,@AUX_ORDEN_ID,@AUX_ESTADO_ORDEN,@AUX_RECEP_FECHA,NULL)" & vbCrLf
sConsulta = sConsulta & "   --Calidad: Sin With(No lock) pq es importante coger los ids correctos" & vbCrLf
sConsulta = sConsulta & "   SELECT @AUX_ORDEN_EST_ID=MAX(ID) FROM ORDEN_EST WHERE PEDIDO=@AUX_PEDIDO_ID" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   UPDATE ORDEN_ENTREGA SET EST =@AUX_ESTADO_ORDEN, ORDEN_EST =@AUX_ORDEN_EST_ID WHERE ID = @AUX_ORDEN_ID      " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @HAY_TRANSACC =0" & vbCrLf
sConsulta = sConsulta & "   COMMIT TRAN" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "    " & vbCrLf
sConsulta = sConsulta & "CLOSE curPEDIDOS" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curPEDIDOS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta



sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_DEVOLVER_ETAPA_ACTUAL]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_DEVOLVER_ETAPA_ACTUAL]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_DEVOLVER_ETAPA_ACTUAL] @INSTANCIA INT ,@IDI VARCHAR(50), @PER VARCHAR(50)=NULL ,@PROVE VARCHAR(50) =NULL, @BLOQUE INT=0, @GESTOR TINYINT=0, @RECEPTOR TINYINT=0 AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL= N' SELECT TOP 1 BLOQUE, ROL, VER_FLUJO, BLOQUE_DEN, ID, TRASLADADA, CUMPLIMENTADOR, BLOQ, COMO_ASIGNAR, PER_ROL, VER_DETALLE_PER, ORDEN, IEID, PERMITIR_TRASLADO, ESTADO FROM ('" & vbCrLf
sConsulta = sConsulta & "SET @SQL= @SQL + ' SELECT DISTINCT IB.BLOQUE,R.ID AS ROL,R.VER_FLUJO,PM_COPIA_BLOQUE_DEN.DEN AS BLOQUE_DEN ,IB.ID,IB.TRASLADADA,NULL AS CUMPLIMENTADOR,IB.BLOQ,R.COMO_ASIGNAR, R.PER PER_ROL, R.VER_DETALLE_PER,1 AS ORDEN ,NULL AS IEID, CRB.PERMITIR_TRASLADO, R.TIPO, INSTANCIA.ESTADO " & vbCrLf
sConsulta = sConsulta & "   FROM INSTANCIA_BLOQUE IB WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN INSTANCIA WITH (NOLOCK) ON IB.INSTANCIA= INSTANCIA.ID" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL_BLOQUE CRB WITH (NOLOCK)  ON IB.BLOQUE=CRB.BLOQUE" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL R WITH (NOLOCK)  ON CRB.ROL=R.ID AND R.INSTANCIA=@INSTANCIA" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN PM_COPIA_BLOQUE_DEN WITH (NOLOCK)  ON IB.BLOQUE=PM_COPIA_BLOQUE_DEN.BLOQUE AND PM_COPIA_BLOQUE_DEN.IDI=@IDI" & vbCrLf
sConsulta = sConsulta & "    WHERE IB.INSTANCIA=@INSTANCIA AND IB.ESTADO=1 AND IB.BLOQ<>1'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @GESTOR = 1" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + ' AND R.GESTOR_FACTURA=1'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @RECEPTOR = 1" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + ' AND R.RECEPTOR_FACTURA=1'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @GESTOR = 0 AND @RECEPTOR = 0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   IF @PER IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL + ' AND (R.PER=@PER OR R.PER IN (SELECT U.PER FROM USU U WITH (NOLOCK) WHERE U.FSWS_SUSTITUCION=@PER))'" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL + ' AND R.PROVE=@PROVE'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @BLOQUE <> 0" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + ' AND IB.BLOQUE=@BLOQUE'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @GESTOR = 0 AND @RECEPTOR = 0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   -- Es un rol de lista autoasignable" & vbCrLf
sConsulta = sConsulta & "   IF @PER IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL + '  " & vbCrLf
sConsulta = sConsulta & "       UNION" & vbCrLf
sConsulta = sConsulta & "       SELECT DISTINCT IB.BLOQUE,R.ID AS ROL,R.VER_FLUJO,PM_COPIA_BLOQUE_DEN.DEN AS BLOQUE_DEN ,IB.ID,IB.TRASLADADA,NULL AS CUMPLIMENTADOR,IB.BLOQ,R.COMO_ASIGNAR, R.PER PER_ROL, R.VER_DETALLE_PER,1 AS ORDEN ,NULL AS IEID, CRB.PERMITIR_TRASLADO, R.TIPO, INSTANCIA.ESTADO " & vbCrLf
sConsulta = sConsulta & "       FROM INSTANCIA_BLOQUE IB WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN INSTANCIA WITH (NOLOCK) ON IB.INSTANCIA= INSTANCIA.ID" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PM_COPIA_ROL_BLOQUE CRB WITH (NOLOCK)  ON IB.BLOQUE=CRB.BLOQUE" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PM_COPIA_ROL R WITH (NOLOCK)  ON CRB.ROL=R.ID AND R.INSTANCIA=@INSTANCIA AND R.PER IS NULL AND R.COMO_ASIGNAR=3" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PM_COPIA_PARTICIPANTES PT WITH (NOLOCK)  ON R.ID=PT.ROL AND (PT.PER=@PER OR PT.PER IN ( SELECT U.PER FROM USU U WITH (NOLOCK) WHERE U.FSWS_SUSTITUCION=@PER)) " & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN PM_COPIA_BLOQUE_DEN WITH (NOLOCK)  ON IB.BLOQUE=PM_COPIA_BLOQUE_DEN.BLOQUE AND PM_COPIA_BLOQUE_DEN.IDI=@IDI" & vbCrLf
sConsulta = sConsulta & "       WHERE IB.INSTANCIA=@INSTANCIA AND IB.ESTADO=1 AND IB.BLOQ<>1 '" & vbCrLf
sConsulta = sConsulta & "   IF @BLOQUE <> 0" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL + ' AND IB.BLOQUE=@BLOQUE'" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + '  " & vbCrLf
sConsulta = sConsulta & "   UNION" & vbCrLf
sConsulta = sConsulta & "   SELECT DISTINCT IB.BLOQUE,R.ID AS ROL,R.VER_FLUJO, PM_COPIA_BLOQUE_DEN.DEN AS BLOQUE_DEN ,IB.ID,IB.TRASLADADA,CASE WHEN IE.DESTINATARIO IS NULL THEN IE.DESTINATARIO_PROV ELSE IE.DESTINATARIO END AS CUMPLIMENTADOR ,IB.BLOQ,R.COMO_ASIGNAR, R.PER PER_ROL, R.VER_DETALLE_PER,2 AS ORDEN ,IE.ID AS IEID, CRB.PERMITIR_TRASLADO, R.TIPO, INSTANCIA.ESTADO " & vbCrLf
sConsulta = sConsulta & "   FROM INSTANCIA_BLOQUE IB WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN INSTANCIA WITH (NOLOCK) ON IB.INSTANCIA= INSTANCIA.ID" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL_BLOQUE CRB WITH (NOLOCK)  ON IB.BLOQUE=CRB.BLOQUE" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL R WITH (NOLOCK)  ON CRB.ROL=R.ID AND R.INSTANCIA=@INSTANCIA" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN PM_COPIA_BLOQUE_DEN WITH (NOLOCK)  ON IB.BLOQUE=PM_COPIA_BLOQUE_DEN.BLOQUE AND PM_COPIA_BLOQUE_DEN.IDI=@IDI" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN " & vbCrLf
sConsulta = sConsulta & "       (" & vbCrLf
sConsulta = sConsulta & "       SELECT MAX(ID) ID ,INSTANCIA,BLOQUE,ROL " & vbCrLf
sConsulta = sConsulta & "       FROM INSTANCIA_EST WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "       WHERE INSTANCIA_EST.INSTANCIA=@INSTANCIA AND (DESTINATARIO IS NOT NULL OR DESTINATARIO_PROV IS NOT NULL)  '" & vbCrLf
sConsulta = sConsulta & "       IF @PER IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "          SET @SQL=@SQL + '  AND INSTANCIA_EST.PER=@PER'" & vbCrLf
sConsulta = sConsulta & "       ELSE" & vbCrLf
sConsulta = sConsulta & "         SET @SQL=@SQL + '  AND INSTANCIA_EST.PROVE=@PROVE'" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + ' GROUP BY INSTANCIA,BLOQUE,ROL" & vbCrLf
sConsulta = sConsulta & "       ) T ON IB.INSTANCIA=T.INSTANCIA AND IB.ID=T.BLOQUE AND R.ID=T.ROL" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN INSTANCIA_EST IE WITH (NOLOCK)  ON T.ID=IE.ID AND IE.INSTANCIA=@INSTANCIA" & vbCrLf
sConsulta = sConsulta & "   WHERE IB.ESTADO=1 AND IB.BLOQ<>1 AND IB.INSTANCIA=@INSTANCIA AND IB.TRASLADADA=1 '" & vbCrLf
sConsulta = sConsulta & "   IF @BLOQUE <> 0" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL + ' AND IB.BLOQUE=@BLOQUE'" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + '  " & vbCrLf
sConsulta = sConsulta & "   UNION " & vbCrLf
sConsulta = sConsulta & "   SELECT DISTINCT IB.BLOQUE,R.ID AS ROL,R.VER_FLUJO, PM_COPIA_BLOQUE_DEN.DEN AS BLOQUE_DEN ,IB.ID,IB.TRASLADADA,CASE WHEN IE.DESTINATARIO IS NULL THEN IE.DESTINATARIO_PROV ELSE IE.DESTINATARIO END AS CUMPLIMENTADOR ,IB.BLOQ,R.COMO_ASIGNAR, R.PER PER_ROL, R.VER_DETALLE_PER,2 AS ORDEN ,IE.ID AS IEID, CRB.PERMITIR_TRASLADO, R.TIPO, INSTANCIA.ESTADO" & vbCrLf
sConsulta = sConsulta & "   FROM INSTANCIA_BLOQUE IB WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN INSTANCIA WITH (NOLOCK) ON IB.INSTANCIA= INSTANCIA.ID" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL_BLOQUE CRB WITH (NOLOCK)  ON IB.BLOQUE=CRB.BLOQUE" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL R WITH (NOLOCK)  ON CRB.ROL=R.ID AND R.INSTANCIA=@INSTANCIA" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN PM_COPIA_BLOQUE_DEN WITH (NOLOCK)  ON IB.BLOQUE=PM_COPIA_BLOQUE_DEN.BLOQUE AND PM_COPIA_BLOQUE_DEN.IDI=@IDI" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN " & vbCrLf
sConsulta = sConsulta & "       (" & vbCrLf
sConsulta = sConsulta & "       SELECT MAX(ID) ID ,INSTANCIA,BLOQUE,ROL " & vbCrLf
sConsulta = sConsulta & "       FROM INSTANCIA_EST WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "       WHERE INSTANCIA_EST.INSTANCIA=@INSTANCIA AND (DESTINATARIO IS NOT NULL OR DESTINATARIO_PROV IS NOT NULL)  '" & vbCrLf
sConsulta = sConsulta & "       IF @PER IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "          SET @SQL=@SQL + ' AND (INSTANCIA_EST.DESTINATARIO=@PER OR INSTANCIA_EST.DESTINATARIO IN (SELECT U.PER FROM USU U WITH (NOLOCK) WHERE U.FSWS_SUSTITUCION=@PER))  '" & vbCrLf
sConsulta = sConsulta & "       ELSE" & vbCrLf
sConsulta = sConsulta & "         SET @SQL=@SQL + ' AND INSTANCIA_EST.DESTINATARIO_PROV=@PROVE '" & vbCrLf
sConsulta = sConsulta & "     SET @SQL=@SQL + ' GROUP BY INSTANCIA,BLOQUE,ROL" & vbCrLf
sConsulta = sConsulta & "       ) T ON IB.INSTANCIA=T.INSTANCIA AND IB.ID=T.BLOQUE AND R.ID=T.ROL" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN INSTANCIA_EST IE WITH (NOLOCK) ON T.ID=IE.ID AND IE.INSTANCIA=@INSTANCIA'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + '  WHERE IB.ESTADO=1 AND IB.BLOQ<>1 AND IB.INSTANCIA=@INSTANCIA AND IB.TRASLADADA=1 '" & vbCrLf
sConsulta = sConsulta & "   IF @BLOQUE <> 0" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL + ' AND IB.BLOQUE=@BLOQUE'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' )AUX ORDER BY ORDEN ASC, TIPO ASC, IEID DESC '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA INT, @IDI VARCHAR(50), @PER VARCHAR(50), @PROVE VARCHAR(50), @BLOQUE INT', @INSTANCIA=@INSTANCIA,@IDI=@IDI,@PER=@PER,@PROVE=@PROVE,@BLOQUE=@BLOQUE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta



sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSEP_VALIDAR_INTEGRACION_ORDEN]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSEP_VALIDAR_INTEGRACION_ORDEN]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSEP_VALIDAR_INTEGRACION_ORDEN] @ORDEN INT AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @CONTROL INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @CONTROL=COUNT(*) FROM TABLAS_INTEGRACION_ERP" & vbCrLf
sConsulta = sConsulta & "WHERE (ACTIVA=1 AND SENTIDO IN (1,3) AND TABLA=12) OR (ACTIVA=1 AND SENTIDO IN (1,3) AND TABLA=14)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF NOT @CONTROL IS NULL AND @CONTROL>0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SELECT @CONTROL=COUNT(*) FROM TABLAS_INTEGRACION_ERP" & vbCrLf
sConsulta = sConsulta & "   WHERE (ACTIVA=1 AND SENTIDO IN (1,3) AND TABLA=11) OR (ACTIVA=1 AND SENTIDO IN (1,3) AND TABLA=13)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF NOT @CONTROL IS NULL AND @CONTROL>0" & vbCrLf
sConsulta = sConsulta & "      BEGIN" & vbCrLf
sConsulta = sConsulta & "      DECLARE @ESTADO INT" & vbCrLf
sConsulta = sConsulta & "      SELECT @ESTADO=LOG_GRAL.ESTADO FROM LOG_GRAL WITH (NOLOCK) WHERE LOG_GRAL.ID = " & vbCrLf
sConsulta = sConsulta & "          (SELECT isnull(MAX(G.ID),0) FROM LOG_GRAL G WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "              INNER JOIN LOG_ORDEN_ENTREGA LO WITH (NOLOCK) ON G.ID_TABLA=LO.ID " & vbCrLf
sConsulta = sConsulta & "          WHERE (TABLA=13 OR TABLA=11) AND G.ORIGEN IN (0,2) AND (LO.ACCION='I' OR LO.ACCION='U' OR LO.ACCION='R')" & vbCrLf
sConsulta = sConsulta & "              AND LO.ID_ORDEN_ENTREGA = @ORDEN)" & vbCrLf
sConsulta = sConsulta & "      IF @ESTADO IS NULL OR @ESTADO<>4" & vbCrLf
sConsulta = sConsulta & "          RETURN 0" & vbCrLf
sConsulta = sConsulta & "      ELSE" & vbCrLf
sConsulta = sConsulta & "          RETURN 1" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "      RETURN 1" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "      RETURN 1" & vbCrLf
ExecuteSQL gRDOCon, sConsulta



sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSEP_COMPROBAR_PROVE_ERP]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSEP_COMPROBAR_PROVE_ERP]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSEP_COMPROBAR_PROVE_ERP] (@EMP INTEGER) AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SELECT T.ACTIVA FROM TABLAS_INTEGRACION_ERP T WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN ERP_SOCIEDAD ES WITH(NOLOCK) ON T.ERP=ES.ERP " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN EMP E WITH(NOLOCK)  ON E.SOCIEDAD = ES.SOCIEDAD AND E.ERP=1 " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN TABLAS_INTEGRACION_ERP T2 WITH(NOLOCK) ON T2.ERP=T.ERP " & vbCrLf
sConsulta = sConsulta & "   WHERE T.TABLA=11 AND T.SENTIDO IN (1,3) AND T.ACTIVA=1 AND T2.TABLA=8 AND T2.ACTIVA=1 AND T2.TABLA_INTERMEDIA = 1" & vbCrLf
sConsulta = sConsulta & "   AND E.ID = @EMP" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta



End Sub
