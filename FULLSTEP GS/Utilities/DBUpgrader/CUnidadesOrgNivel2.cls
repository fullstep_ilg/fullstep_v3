VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CUnidadesOrgNivel2"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Collection" ,"CUnidadOrgNivel2"
Attribute VB_Ext_KEY = "Member0" ,"CUnidadOrgNivel2"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
    
End Enum


'local variable to hold collection
Private mCol As Collection
Private m_oConexion As CConexion

Friend Property Get Conexion() As CConexion
    Set Conexion = m_oConexion
End Property

Friend Property Set Conexion(ByVal con As CConexion)
    Set m_oConexion = con
End Property


Public Function Add(Cod As String, Den As String, CodUnidadOrgNivel1 As String, Optional ByVal varIndice As Variant) As CUnidadOrgNivel2
    'create a new object
    Dim objnewmember As CUnidadOrgNivel2
    Dim sCod As String
    Set objnewmember = New CUnidadOrgNivel2


    'set the properties passed into the method
    objnewmember.Cod = Cod
    objnewmember.Den = Den
    objnewmember.CodUnidadOrgNivel1 = CodUnidadOrgNivel1
    Set objnewmember.Conexion = m_oConexion
    
    If Not IsNull(varIndice) And Not IsMissing(varIndice) Then
        objnewmember.Indice = varIndice
        mCol.Add objnewmember, CStr(varIndice)
    Else
        sCod = CodUnidadOrgNivel1 & Mid$("                         ", 1, gLongitudesDeCodigos.giLongCodUON1 - Len(CodUnidadOrgNivel1))
        sCod = sCod & Cod & Mid$("                         ", 1, gLongitudesDeCodigos.giLongCodUON2 - Len(Cod))

        mCol.Add objnewmember, CStr(Cod)
    End If
    
    'return the object created
    Set Add = objnewmember
    Set objnewmember = Nothing


End Function

Public Property Get Item(vntIndexKey As Variant) As CUnidadOrgNivel2
Attribute Item.VB_UserMemId = 0
    On Error GoTo vacio
      Set Item = mCol(vntIndexKey)
      Exit Property
vacio:
      Set Item = Nothing
End Property



Public Property Get Count() As Long
    If mCol Is Nothing Then
        Count = 0
    Else
        Count = mCol.Count
    End If
End Property


Public Sub Remove(vntIndexKey As Variant)
    mCol.Remove vntIndexKey
End Sub


Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
     Set NewEnum = mCol.[_NewEnum]
End Property
Public Sub CargarTodasLasUnidadesOrgNivel2(Optional ByVal UON1 As Variant, Optional ByVal UON2 As Variant, Optional ByVal UON3 As Variant, Optional ByVal CodDep As Variant, Optional ByVal RUO As Boolean = False, Optional ByVal RDep As Boolean, Optional ByVal CarIniCod As String, Optional ByVal CarIniDen As String, Optional ByVal CoincidenciaTotal As Boolean, Optional ByVal OrdenadasPorDen As Boolean = False, Optional ByVal UsarIndice As Boolean = False, Optional ByVal bCargarEmpresas As Boolean = False, Optional ByVal sCarIniNif As String, Optional ByVal sCarIniRazon As String)

Dim rs As New ADODB.Recordset
Dim sConsulta1 As String
Dim sConsulta2 As String
Dim sConsulta3 As String
Dim sCad As String
Dim lIndice As Long
Dim oUON2 As CUnidadOrgNivel2
Dim oUnidadesNivel2 As CUnidadesOrgNivel2
Dim fldUon1Cod As ADODB.Field
Dim fldUon2Cod As ADODB.Field
Dim fldUON2Den As ADODB.Field
Dim fldEmpresa As ADODB.Field
Dim SQLSelectEmpresas As String
Dim SQLFromEmpresas As String
Dim SQLWhereEmpresas As String

    
If bCargarEmpresas Then
    SQLSelectEmpresas = ", UON2.EMPRESA, E.NIF, E.DEN, E.DIR, E.CP, E.POB, E.PAI, E.PROVI, E.ERP "
    SQLFromEmpresas = " LEFT JOIN EMP E ON  E.ID = UON2.EMPRESA "
    If sCarIniNif <> "" Then
        SQLWhereEmpresas = " AND E.NIF LIKE '" & sCarIniNif & "%'"
    End If
    If sCarIniRazon <> "" Then
        SQLWhereEmpresas = SQLWhereEmpresas & " AND E.DEN LIKE '" & sCarIniRazon & "%'"
    End If
    
Else
    SQLSelectEmpresas = ", UON2.EMPRESA "
    SQLFromEmpresas = ""

End If

    

If Not IsMissing(UON1) Then
    If IsEmpty(UON1) Then
        UON1 = Null
    End If
    If Trim(UON1) = "" Then
        UON1 = Null
    End If
Else
    UON1 = Null
End If

If Not IsMissing(UON2) Then
    If IsEmpty(UON2) Then
        UON2 = Null
    End If
    If Trim(UON2) = "" Then
        UON2 = Null
    End If
Else
    UON2 = Null
End If

If Not IsMissing(UON3) Then
    If IsEmpty(UON3) Then
        UON3 = Null
    End If
    If Trim(UON3) = "" Then
        UON3 = Null
    End If
Else
    UON3 = Null
End If


'********* Precondicion **************************************
If m_oConexion Is Nothing Then
    Err.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CGrupoDevolverTodasLasUnidadesOrgNivel1", "No se ha establecido la conexion"
    Exit Sub
End If
'*************************************************************
    
If Not RDep Then

    If RUO Then
    'Restriccion de unidad
        If IsNull(UON1) Then
            ' La persona pertence al nivel 0
            sConsulta1 = "SELECT DISTINCT UON2.COD AS UON2COD, UON2.DEN AS UON2DEN, UON2.UON1 AS UON1COD " & SQLSelectEmpresas & " FROM UON2 " & SQLFromEmpresas & " WHERE 1=1"
    
        Else ' Es del nivel 1 o 2 o 3
            If IsNull(UON2) Then
                'La persona pertenece al nivel 1
                sConsulta1 = "SELECT DISTINCT UON2.COD AS UON2COD, UON2.DEN AS UON2DEN, UON2.UON1 AS UON1COD  " & SQLSelectEmpresas & " FROM UON2 " & SQLFromEmpresas & "  "
                sConsulta1 = sConsulta1 & "WHERE UON2.UON1='" & DblQuote(CStr(UON1)) & "'"
            
            Else
                ' La persona pertence al nivel 2 o 3
                ' Solo cargar lasuya
                sConsulta1 = "SELECT DISTINCT UON2.COD AS UON2COD, UON2.DEN AS UON2DEN, UON2.UON1 AS UON1COD  " & SQLSelectEmpresas & " FROM UON2 " & SQLFromEmpresas & "  "
                sConsulta1 = sConsulta1 & "WHERE UON2.UON1='" & DblQuote(CStr(UON1)) & "' "
                CarIniCod = UON2
                CoincidenciaTotal = True
            End If
        End If
    Else
    ' No hay restricciones
        sConsulta1 = "SELECT DISTINCT UON2.COD AS UON2COD, UON2.DEN AS UON2DEN, UON2.UON1 AS UON1COD  " & SQLSelectEmpresas & " FROM UON2 " & SQLFromEmpresas & "  WHERE 1=1 "
    End If
    
If CarIniCod <> "" Then
    sCad = "1"
Else
    sCad = "0"
End If

If CarIniDen <> "" Then
    sCad = sCad & "1"
Else
    sCad = sCad & "0"
End If
    
    Select Case sCad
            
    Case "01"
            
            If CoincidenciaTotal Then
                sConsulta1 = sConsulta1 & "AND UON2.DEN = '" & DblQuote(CarIniDen) & "'"
            Else
                sConsulta1 = sConsulta1 & "AND UON2.DEN LIKE '" & DblQuote(CarIniDen) & "%'"
            End If
                      
    Case "10"
            If CoincidenciaTotal Then
                sConsulta1 = sConsulta1 & "AND UON2.COD = '" & DblQuote(CarIniCod) & "'"
            Else
                sConsulta1 = sConsulta1 & "AND UON2.COD LIKE '" & DblQuote(CarIniCod) & "%'"
            End If
                   
    Case "11"
            If CoincidenciaTotal Then
                sConsulta1 = sConsulta1 & "AND UON2.COD = '" & DblQuote(CarIniCod) & "'"
                sConsulta1 = sConsulta1 & "AND UON2.DEN = '" & DblQuote(CarIniDen) & "'"
            Else
                sConsulta1 = sConsulta1 & "AND UON2.COD LIKE '" & DblQuote(CarIniCod) & "%'"
                sConsulta1 = sConsulta1 & "AND UON2.DEN LIKE '" & DblQuote(CarIniDen) & "%'"
            End If
    End Select
    sConsulta1 = sConsulta1 & SQLWhereEmpresas
    
    If OrdenadasPorDen Then
        sConsulta1 = sConsulta1 & " ORDER BY UON2DEN"
    Else
        sConsulta1 = sConsulta1 & " ORDER BY UON2COD"
    End If
        
    rs.Open sConsulta1, m_oConexion.adoCon, adOpenForwardOnly, adLockReadOnly
      
    If rs.EOF Then
            
        rs.Close
        Set rs = Nothing
        Set mCol = Nothing
        Set mCol = New Collection
        Exit Sub
          
    Else
        
        Set mCol = Nothing
        Set mCol = New Collection
         
        Set fldUon1Cod = rs.Fields("UON1COD")
        Set fldUon2Cod = rs.Fields("UON2COD")
        Set fldUON2Den = rs.Fields("UON2DEN")
        Set fldEmpresa = rs.Fields("EMPRESA")
        
        
        If UsarIndice Then
            lIndice = 0
            Set oUON2 = New CUnidadOrgNivel2
            Set oUON2.Conexion = m_oConexion
            While Not rs.EOF
                oUON2.CodUnidadOrgNivel1 = fldUon1Cod.Value
                oUON2.Cod = fldUon2Cod.Value
                oUON2.Den = fldUON2Den.Value
                oUON2.IdEmpresa = fldEmpresa.Value
                If bCargarEmpresas And Not IsNull(fldEmpresa.Value) Then
                    Set oUON2.Empresa = New CEmpresa
                    With oUON2.Empresa
                        .ID = fldEmpresa.Value
                        .Nif = rs.Fields("NIF").Value
                        .Den = rs.Fields("DEN").Value
                        .CP = rs.Fields("CP").Value
                        .Poblacion = rs.Fields("POB").Value
                        .CodPais = rs.Fields("PAI").Value
                        .CodProvi = rs.Fields("PROVI").Value
                        .Direccion = rs.Fields("DIR").Value
                    End With
                End If
                
                mCol.Add oUON2, lIndice
                rs.MoveNext
                lIndice = lIndice + 1
                Set oUON2 = Nothing
                Set oUON2 = New CUnidadOrgNivel2
                Set oUON2.Conexion = m_oConexion
            Wend
        
        Else
            
            Set oUON2 = New CUnidadOrgNivel2
            Set oUON2.Conexion = m_oConexion
            While Not rs.EOF
                oUON2.CodUnidadOrgNivel1 = fldUon1Cod.Value
                oUON2.Cod = fldUon2Cod.Value
                oUON2.Den = fldUON2Den.Value
                oUON2.IdEmpresa = fldEmpresa.Value
                If bCargarEmpresas And Not IsNull(fldEmpresa.Value) Then
                    Set oUON2.Empresa = New CEmpresa
                    With oUON2.Empresa
                        .ID = fldEmpresa.Value
                        .Nif = rs.Fields("NIF").Value
                        .Den = rs.Fields("DEN").Value
                        .CP = rs.Fields("CP").Value
                        .Poblacion = rs.Fields("POB").Value
                        .CodPais = rs.Fields("PAI").Value
                        .CodProvi = rs.Fields("PROVI").Value
                        .Direccion = rs.Fields("DIR").Value
                    End With
                End If
                mCol.Add oUON2, CStr(fldUon1Cod.Value) & CStr(fldUon2Cod.Value)
                rs.MoveNext
                Set oUON2 = Nothing
                Set oUON2 = New CUnidadOrgNivel2
                Set oUON2.Conexion = m_oConexion
            Wend
        End If
        
        rs.Close
        Set rs = Nothing
        Exit Sub
          
    End If
    
End If 'not Rdep


'Rdepartamento solamente
If Not RUO Then
    ' Restriccion de departamento
    sConsulta2 = "SELECT DISTINCT UON2.COD AS UON2COD, UON2.DEN as UON2DEN ,UON2.UON1 AS UON1COD  " & SQLSelectEmpresas & " FROM UON2 " & SQLFromEmpresas & " ,UON2_DEP WHERE UON2.COD=UON2_DEP.UON2 AND UON2.UON1=UON2_DEP.UON1 AND UON2_DEP.DEP='" & DblQuote(CodDep) & "'"
    sConsulta3 = "SELECT DISTINCT UON2.COD AS UON2COD, UON2.DEN as UON2DEN ,UON2.UON1 AS UON1COD  " & SQLSelectEmpresas & " FROM UON2 " & SQLFromEmpresas & " UON3_DEP WHERE UON2.COD=UON3_DEP.UON2  AND UON2.UON1=UON3_DEP.UON1 AND UON3_DEP.DEP='" & DblQuote(CodDep) & "'"
End If

'Rdep y unidad organizativa
If RUO Then
    ' Restriccion de departamento
    If IsNull(UON1) Then
        'Es de nivel 0
        sConsulta2 = "SELECT DISTINCT UON2.COD AS UON2COD, UON2.DEN as UON2DEN,UON2.UON1 AS UON1COD  " & SQLSelectEmpresas & " FROM UON2 " & SQLFromEmpresas & " ,UON2_DEP WHERE UON2.COD=UON2_DEP.UON2 AND UON2.UON1=UON2_DEP.UON1 AND UON2_DEP.DEP='" & DblQuote(CodDep) & "'"
        sConsulta3 = "SELECT DISTINCT UON2.COD AS UON2COD, UON2.DEN as UON2DEN ,UON2.UON1 AS UON1COD  " & SQLSelectEmpresas & " FROM UON2 " & SQLFromEmpresas & " ,UON3_DEP WHERE UON2.COD=UON3_DEP.UON2 AND UON2.UON1=UON3_DEP.UON1 AND UON3_DEP.DEP='" & DblQuote(CodDep) & "'"
    Else
        If IsNull(UON2) Then
            'Es de nivel1
            sConsulta2 = "SELECT DISTINCT UON2.COD AS UON2COD, UON2.DEN as UON2DEN,UON2.UON1 AS UON1COD  " & SQLSelectEmpresas & " FROM UON2 " & SQLFromEmpresas & " ,UON2_DEP WHERE UON2.COD=UON2_DEP.UON2 AND UON2.UON1=UON2_DEP.UON1 AND UON2_DEP.DEP='" & DblQuote(CodDep) & "' AND UON2.UON1='" & DblQuote(UON1) & "'"
            sConsulta3 = "SELECT DISTINCT UON2.COD AS UON2COD, UON2.DEN as UON2DEN ,UON2.UON1 AS UON1COD  " & SQLSelectEmpresas & " FROM UON2 " & SQLFromEmpresas & " ,UON3_DEP WHERE UON2.COD=UON3_DEP.UON2 AND UON2.UON1=UON3_DEP.UON1 AND UON3_DEP.DEP='" & DblQuote(CodDep) & "' AND UON3_DEP.UON1='" & DblQuote(UON1) & "'"
        Else
            If IsNull(UON3) Then
                'Es de nivel 2
                sConsulta2 = "SELECT DISTINCT UON2.COD AS UON2COD, UON2.DEN as UON2DEN,UON2.UON1 AS UON1COD  " & SQLSelectEmpresas & " FROM UON2 " & SQLFromEmpresas & " ,UON2_DEP WHERE UON2.COD=UON2_DEP.UON2 AND UON2.UON1=UON2_DEP.UON1 AND UON2.COD=UON2_DEP.UON2 AND UON2_DEP.DEP='" & DblQuote(CodDep) & "' AND UON2.UON1='" & DblQuote(UON1) & "' AND UON2.COD='" & DblQuote(UON2) & "'"
                sConsulta3 = "SELECT DISTINCT UON2.COD AS UON2COD, UON2.DEN as UON2DEN ,UON2.UON1 AS UON1COD  " & SQLSelectEmpresas & " FROM UON2 " & SQLFromEmpresas & " ,UON3_DEP WHERE UON2.COD=UON3_DEP.UON2 AND UON2.UON1=UON3_DEP.UON1  AND UON2.COD=UON3_DEP.UON2 AND UON3_DEP.DEP='" & DblQuote(CodDep) & "' AND UON3_DEP.UON1='" & DblQuote(UON1) & "' AND UON3_DEP.UON2='" & DblQuote(UON2) & "'"
                
            Else
                'Es de nivel 3
                sConsulta2 = "SELECT DISTINCT UON2.COD AS UON2COD, UON2.DEN as UON2DEN,UON2.UON1 AS UON1COD  " & SQLSelectEmpresas & " FROM UON2 " & SQLFromEmpresas & " ,UON2_DEP WHERE UON2.COD=UON2_DEP.UON2 AND UON2.UON1=UON2_DEP.UON1 AND UON2_DEP.DEP='" & DblQuote(CodDep) & "' AND UON2.UON1='" & DblQuote(UON1) & "' AND UON2.COD='" & DblQuote(UON2) & "'"
                sConsulta3 = "SELECT DISTINCT UON2.COD AS UON2COD, UON2.DEN as UON2DEN ,UON2.UON1 AS UON1COD  " & SQLSelectEmpresas & " FROM UON2 " & SQLFromEmpresas & " ,UON3_DEP WHERE UON2.COD=UON3_DEP.UON2 AND UON2.UON1=UON3_DEP.UON1 AND UON3_DEP.DEP='" & DblQuote(CodDep) & "' AND UON3_DEP.UON1='" & DblQuote(UON1) & "' AND UON3_DEP.UON2='" & DblQuote(UON2) & "' AND UON3_DEP.DEP='" & DblQuote(CodDep) & "'"
            End If
        End If
    End If
End If

    Select Case sCad
            
    Case "01"
            
            If CoincidenciaTotal Then
                sConsulta1 = sConsulta1 & "AND UON2.DEN = '" & DblQuote(CarIniDen) & "'"
                sConsulta2 = sConsulta2 & "AND UON2.DEN = '" & DblQuote(CarIniDen) & "'"
                sConsulta3 = sConsulta3 & "AND UON2.DEN = '" & DblQuote(CarIniDen) & "'"
            Else
                sConsulta1 = sConsulta1 & "AND UON2.DEN LIKE '" & DblQuote(CarIniDen) & "%'"
                sConsulta2 = sConsulta2 & "AND UON2.DEN LIKE '" & DblQuote(CarIniDen) & "%'"
                sConsulta3 = sConsulta3 & "AND UON2.DEN LIKE '" & DblQuote(CarIniDen) & "%'"
            End If
                      
    Case "10"
            If CoincidenciaTotal Then
                sConsulta1 = sConsulta1 & "AND UON2.COD = '" & DblQuote(CarIniCod) & "'"
                sConsulta2 = sConsulta2 & "AND UON2.COD = '" & DblQuote(CarIniCod) & "'"
                sConsulta3 = sConsulta3 & "AND UON2.COD = '" & DblQuote(CarIniCod) & "'"
            Else
                sConsulta1 = sConsulta1 & "AND UON2.COD LIKE '" & DblQuote(CarIniCod) & "%'"
                sConsulta2 = sConsulta2 & "AND UON2.COD LIKE '" & DblQuote(CarIniCod) & "%'"
                sConsulta3 = sConsulta3 & "AND UON2.COD LIKE '" & DblQuote(CarIniCod) & "%'"
            End If
                   
    Case "11"
            If CoincidenciaTotal Then
                sConsulta1 = sConsulta1 & "AND UON2.COD = '" & DblQuote(CarIniCod) & "'"
                sConsulta1 = sConsulta1 & "AND UON2.DEN = '" & DblQuote(CarIniDen) & "'"
                sConsulta2 = sConsulta2 & "AND UON2.COD = '" & DblQuote(CarIniCod) & "'"
                sConsulta2 = sConsulta2 & "AND UON2.DEN = '" & DblQuote(CarIniDen) & "'"
                sConsulta3 = sConsulta3 & "AND UON2.COD = '" & DblQuote(CarIniCod) & "'"
                sConsulta3 = sConsulta3 & "AND UON2.DEN = '" & DblQuote(CarIniDen) & "'"
            Else
                sConsulta1 = sConsulta1 & "AND UON2.COD LIKE '" & DblQuote(CarIniCod) & "%'"
                sConsulta1 = sConsulta1 & "AND UON2.DEN LIKE '" & DblQuote(CarIniDen) & "%'"
                sConsulta2 = sConsulta2 & "AND UON2.COD LIKE '" & DblQuote(CarIniCod) & "%'"
                sConsulta2 = sConsulta2 & "AND UON2.DEN LIKE '" & DblQuote(CarIniDen) & "%'"
                sConsulta3 = sConsulta3 & "AND UON2.COD LIKE '" & DblQuote(CarIniCod) & "%'"
                sConsulta3 = sConsulta3 & "AND UON2.DEN LIKE '" & DblQuote(CarIniDen) & "%'"
            End If
    End Select
    
    'Para que no salga la UON ='  ' que es la utilizada para guardar las distribuciones vacias
    sConsulta1 = sConsulta1 & " AND UON2.COD >'" & "   " & "'"
    sConsulta2 = sConsulta2 & " AND UON2.COD >'" & "   " & "'"
    sConsulta3 = sConsulta3 & " AND UON2.COD >'" & "   " & "'"
    
    
    sConsulta1 = sConsulta1 & SQLWhereEmpresas
    sConsulta2 = sConsulta2 & SQLWhereEmpresas
    sConsulta3 = sConsulta3 & SQLWhereEmpresas
    
    If OrdenadasPorDen Then
        sConsulta2 = sConsulta2 & " ORDER BY UON2DEN"
        sConsulta3 = sConsulta3 & " ORDER BY UON2DEN"
    Else
        sConsulta2 = sConsulta2 & " ORDER BY UON2COD"
        sConsulta3 = sConsulta3 & " ORDER BY UON2COD"
    End If
        
    If UsarIndice Then
        Set oUnidadesNivel2 = New CUnidadesOrgNivel2
        Set oUON2.Conexion = m_oConexion
    End If
    
    rs.Open sConsulta2, m_oConexion.adoCon, adOpenForwardOnly, adLockReadOnly
      
    If rs.EOF Then
            
        rs.Close
        Set rs = Nothing
        Set mCol = Nothing
        Set mCol = New Collection
          
    Else
        
        Set mCol = Nothing
        Set mCol = New Collection
         
        Set fldUon1Cod = rs.Fields("UON1COD")
        Set fldUon2Cod = rs.Fields("UON2COD")
        Set fldUON2Den = rs.Fields("UON2DEN")
        Set fldEmpresa = rs.Fields("EMPRESA")
         
        If UsarIndice Then
            lIndice = 0
            Set oUON2 = New CUnidadOrgNivel2
            
            While Not rs.EOF
                oUON2.CodUnidadOrgNivel1 = fldUon1Cod.Value
                oUON2.Cod = fldUon2Cod.Value
                oUON2.Den = fldUON2Den.Value
                oUON2.IdEmpresa = fldEmpresa.Value
                If bCargarEmpresas And Not IsNull(fldEmpresa.Value) Then
                    Set oUON2.Empresa = New CEmpresa
                    With oUON2.Empresa
                        .ID = fldEmpresa.Value
                        .Nif = rs.Fields("NIF").Value
                        .Den = rs.Fields("DEN").Value
                        .CP = rs.Fields("CP").Value
                        .Poblacion = rs.Fields("POB").Value
                        .CodPais = rs.Fields("PAI").Value
                        .CodProvi = rs.Fields("PROVI").Value
                        .Direccion = rs.Fields("DIR").Value
                    End With
                End If
                mCol.Add oUON2, lIndice
                oUnidadesNivel2.Add CStr(fldUon1Cod.Value), CStr(fldUon2Cod.Value), ""
                rs.MoveNext
                lIndice = lIndice + 1
                Set oUON2 = Nothing
                Set oUON2 = New CUnidadOrgNivel2
            Wend
        
        Else
            
            Set oUON2 = New CUnidadOrgNivel2
            
            While Not rs.EOF
                oUON2.CodUnidadOrgNivel1 = fldUon1Cod.Value
                oUON2.Cod = fldUon2Cod.Value
                oUON2.Den = fldUON2Den.Value
                oUON2.IdEmpresa = fldEmpresa.Value
                If bCargarEmpresas And Not IsNull(fldEmpresa.Value) Then
                    Set oUON2.Empresa = New CEmpresa
                    With oUON2.Empresa
                        .ID = fldEmpresa.Value
                        .Nif = rs.Fields("NIF").Value
                        .Den = rs.Fields("DEN").Value
                        .CP = rs.Fields("CP").Value
                        .Poblacion = rs.Fields("POB").Value
                        .CodPais = rs.Fields("PAI").Value
                        .CodProvi = rs.Fields("PROVI").Value
                        .Direccion = rs.Fields("DIR").Value
                    End With
                End If
                mCol.Add oUON2, CStr(fldUon1Cod.Value) & CStr(fldUon2Cod.Value)
                rs.MoveNext
                Set oUON2 = Nothing
                Set oUON2 = New CUnidadOrgNivel2
            Wend
        End If
        
        rs.Close
        
          
    End If
    
    rs.Open sConsulta3, m_oConexion.adoCon, adOpenForwardOnly, adLockReadOnly
    
    If Not rs.EOF Then
    
        Set fldUon1Cod = rs.Fields("UON1COD")
        Set fldUon2Cod = rs.Fields("UON2COD")
        Set fldUON2Den = rs.Fields("UON2DEN")
        Set fldEmpresa = rs.Fields("EMPRESA")
        
        If UsarIndice Then
            Set oUON2 = Nothing
            Set oUON2 = New CUnidadOrgNivel2
            Set oUON2.Conexion = m_oConexion
            While Not rs.EOF
                If oUnidadesNivel2(CStr(fldUon1Cod.Value) & CStr(fldUon1Cod.Value)) Is Nothing Then
                    oUON2.CodUnidadOrgNivel1 = fldUon1Cod.Value
                    oUON2.Cod = fldUon2Cod.Value
                    oUON2.Den = fldUON2Den.Value
                    oUON2.IdEmpresa = fldEmpresa.Value
                    If bCargarEmpresas And Not IsNull(fldEmpresa.Value) Then
                        Set oUON2.Empresa = New CEmpresa
                        With oUON2.Empresa
                            .ID = fldEmpresa.Value
                            .Nif = rs.Fields("NIF").Value
                            .Den = rs.Fields("DEN").Value
                            .CP = rs.Fields("CP").Value
                            .Poblacion = rs.Fields("POB").Value
                            .CodPais = rs.Fields("PAI").Value
                            .CodProvi = rs.Fields("PROVI").Value
                            .Direccion = rs.Fields("DIR").Value
                        End With
                    End If
                    mCol.Add oUON2, lIndice
                    oUnidadesNivel2.Add fldUon1Cod.Value, fldUon2Cod.Value, ""
                End If
                rs.MoveNext
                lIndice = lIndice + 1
                Set oUON2 = Nothing
                Set oUON2 = New CUnidadOrgNivel2
                Set oUON2.Conexion = m_oConexion
            Wend
        
        Else
            
            Set oUON2 = New CUnidadOrgNivel2
            Set oUON2.Conexion = m_oConexion
            While Not rs.EOF
                If Me.Item(CStr(fldUon1Cod.Value) & CStr(fldUon2Cod.Value)) Is Nothing Then
                    oUON2.CodUnidadOrgNivel1 = fldUon1Cod.Value
                    oUON2.Cod = fldUon2Cod.Value
                    oUON2.Den = fldUON2Den.Value
                    oUON2.IdEmpresa = fldEmpresa.Value
                    If bCargarEmpresas And Not IsNull(fldEmpresa.Value) Then
                        Set oUON2.Empresa = New CEmpresa
                        With oUON2.Empresa
                            .ID = fldEmpresa.Value
                            .Nif = rs.Fields("NIF").Value
                            .Den = rs.Fields("DEN").Value
                            .CP = rs.Fields("CP").Value
                            .Poblacion = rs.Fields("POB").Value
                            .CodPais = rs.Fields("PAI").Value
                            .CodProvi = rs.Fields("PROVI").Value
                            .Direccion = rs.Fields("DIR").Value
                        End With
                    End If
                    mCol.Add oUON2, CStr(fldUon1Cod.Value) & CStr(fldUon2Cod.Value)
                End If
                rs.MoveNext
                Set oUON2 = Nothing
                Set oUON2 = New CUnidadOrgNivel2
                Set oUON2.Conexion = m_oConexion
            Wend
        End If
        
        Set fldUon1Cod = Nothing
        Set fldUon2Cod = Nothing
        Set fldUON2Den = Nothing
        Set fldEmpresa = Nothing
          
    End If
    rs.Close
    Set rs = Nothing
    
    Exit Sub
        
End Sub


Private Sub Class_Initialize()
     Set mCol = New Collection
End Sub


Private Sub Class_Terminate()
     Set mCol = Nothing
     Set m_oConexion = Nothing
End Sub

Public Function DevolverDenominacion(ByVal sCodUON1 As String, ByVal sCod As String) As String
'*****************************************************************************
'*** Descripci�n: Devuelve la denominaci�n (campo DEN) de la UO de nivel 2 ***
'***              (tabla UON2) cuyo c�digo corresponda con el especificado ***
'***              por los argumentos sCod (nivel 2) y sCodUON1 (nivel 1).  ***
'***                                                                       ***
'*** Par�metros : sCodUON1, sCod ::>> de tipo String, contienen el c�digo  ***
'***                                  de la UO de nivel 2 cuya             ***
'***                                  denominaci�n retornar� la funci�n.   ***
'***                                                                       ***
'*** Valor que devuelve: Un String que contendr� la denominaci�n de la UO  ***
'***                     cuyo c�digo se especifiquen en los argumentos.    ***
'*****************************************************************************
    Dim sConsulta As String
    Dim adoresAdo As ADODB.Recordset
    Dim sDenominacion As String
    
    
    sDenominacion = ""
    Set adoresAdo = New ADODB.Recordset
    sConsulta = "SELECT DEN FROM UON2 WHERE UON1='" & DblQuote(sCodUON1) & "' AND COD='" & DblQuote(sCod) & "'"
    adoresAdo.Open sConsulta, m_oConexion.adoCon, adOpenForwardOnly, adLockReadOnly
    If Not adoresAdo.EOF Then sDenominacion = adoresAdo.Fields.Item(0).Value
    adoresAdo.Close
    Set adoresAdo = Nothing
    DevolverDenominacion = sDenominacion
End Function

