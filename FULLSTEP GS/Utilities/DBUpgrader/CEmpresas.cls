VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CEmpresas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
   
End Enum
Private mCol As Collection
Private m_oConexion As CConexion


Friend Property Set Conexion(ByVal con As CConexion)
    Set m_oConexion = con
End Property
Friend Property Get Conexion() As CConexion
    Set Conexion = m_oConexion
End Property
Public Function Add(ByVal lId As Long, ByVal sNif As String, ByVal sDen As String, Optional ByVal vDireccion As Variant, Optional ByVal vCP As Variant, _
Optional ByVal vCodPais As Variant, Optional ByVal vPoblacion As Variant, Optional ByVal vCodProvi As Variant, Optional ByVal bERP As Boolean, Optional ByVal oConexion As CConexion) As CEmpresa

    
    'create a new object
    Dim objnewmember As CEmpresa
   
    Set objnewmember = New CEmpresa
    
    With objnewmember
    
        .ID = lId
        .Nif = sNif
        .Den = sDen
        
        If Not IsMissing(vPoblacion) Then
            .Poblacion = vPoblacion
        Else
            .Poblacion = Null
        End If
        
        If Not IsMissing(vDireccion) Then
            .Direccion = vDireccion
        Else
            .Direccion = Null
        End If
        
        If Not IsMissing(vCP) Then
            .CP = vCP
        Else
            .CP = Null
        End If
         
        If Not IsMissing(vCodProvi) Then
            .CodProvi = vCodProvi
        Else
            .CodProvi = Null
        End If
         
        If Not IsMissing(vCodPais) Then
            .CodPais = vCodPais
        Else
            .CodPais = Null
        End If
            
        .ERP = bERP
        Set .Conexion = oConexion
    End With
        
        
    
            
    mCol.Add objnewmember, CStr(lId)
    
    'return the object created
    Set Add = objnewmember
    Set objnewmember = Nothing

End Function

Public Property Get Item(vntIndexKey As Variant) As CEmpresa
On Error GoTo NoSeEncuentra:

    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property


Public Property Get Count() As Long

    If mCol Is Nothing Then
        Count = 0
    Else
         Count = mCol.Count
    End If

End Property


Public Sub Remove(vntIndexKey As Variant)
On Error GoTo NoSeEncuentra:
    mCol.Remove vntIndexKey
    Exit Sub
NoSeEncuentra:
    
End Sub


Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
     Set NewEnum = mCol.[_NewEnum]
End Property


Private Sub Class_Initialize()
  

    Set mCol = New Collection
End Sub


Private Sub Class_Terminate()
    'destroys collection when this class is terminated
    Set mCol = Nothing
    Set m_oConexion = Nothing
End Sub


Public Sub CargarTodasLasEmpresasDesde(Optional ByVal sCif As String, _
                                        Optional ByVal sCarIniDen As String, Optional ByVal sCodPais As String, _
                                        Optional ByVal sCodProvi As String, Optional ByVal bOrdenadosPorDen As Boolean = False, _
                                        Optional ByVal bUsarLike As Boolean, Optional ByVal bSoloERP As Boolean, _
                                        Optional ByVal sUON1 As String, Optional ByVal sUON2 As String, _
                                        Optional ByVal sUON3 As String, Optional ByVal lId As Long, Optional ByVal lOrder As Long)

Dim AdoRes As New ADODB.Recordset
Dim sConsulta As String
Dim sWhere As String
Dim sFrom As String

    sFrom = "SELECT DISTINCT E.ID, E.NIF, E.DEN, E.DIR, E.CP, E.POB, E.PAI, E.PROVI, E.ERP FROM EMP E "

    sWhere = " WHERE 1 = 1 "
    
    If sCarIniDen <> "" Then
        If bUsarLike Then
            sWhere = sWhere & " AND DEN LIKE '" & DblQuote(sCarIniDen) & "%'"
        Else
            sWhere = sWhere & " AND DEN = '" & DblQuote(sCarIniDen) & "'"
        End If
    End If
    
    If sCif <> "" Then
        If bUsarLike Then
            sWhere = sWhere & " AND NIF LIKE '" & DblQuote(sCif) & "%'"
        Else
            sWhere = sWhere & " AND NIF = '" & DblQuote(sCif) & "'"
        End If
    End If
    
    If sCodPais <> "" Then
        sWhere = sWhere & " AND PAI = '" & DblQuote(sCodPais) & "'"
    End If
    
    If sCodProvi <> "" Then
        sWhere = sWhere & " AND PAI = '" & DblQuote(sCodProvi) & "'"
    End If
    
    If bSoloERP Then
        sWhere = sWhere & " AND ERP = 1 "
    End If

    If lId <> 0 Then
        sWhere = sWhere & " AND E.ID = " & lId
    End If
    
    If sUON3 <> "" Then
        sConsulta = sConsulta & " INNER JOIN UON3 U3 ON U3.UON1='" & DblQuote(sUON1) & "' AND U3.UON2='" & DblQuote(sUON2) & "' AND U3.COD='" & DblQuote(sUON3) & "' AND E.ID = U3.EMPRESA AND EMPRESA IS NOT NULL" & sWhere
        sConsulta = sConsulta & " UNION " & sFrom & " INNER JOIN UON2 ON UON2.UON1='" & DblQuote(sUON1) & "' AND UON2.COD='" & DblQuote(sUON2) & "' AND E.ID = UON2.EMPRESA AND UON2.EMPRESA IS NOT NULL"
        sConsulta = sConsulta & " AND (SELECT COUNT(*) FROM UON3 WHERE UON1='" & DblQuote(sUON1) & "' AND UON2='" & DblQuote(sUON2) & "' AND COD = '" & DblQuote(sUON3) & "' AND EMPRESA IS NOT NULL)=0" & sWhere
        
        sConsulta = sConsulta & " UNION " & sFrom & " INNER JOIN UON3 ON UON3.UON1='" & DblQuote(sUON1) & "' AND UON3.UON2='" & DblQuote(sUON1) & "' AND E.ID = UON3.EMPRESA AND UON3.EMPRESA IS NOT NULL"
        sConsulta = sConsulta & " AND (SELECT COUNT(*) FROM UON3 WHERE UON1='" & DblQuote(sUON1) & "' AND UON2='" & DblQuote(sUON2) & "' AND COD = '" & DblQuote(sUON3) & "' AND EMPRESA IS NOT NULL)=0"
        sConsulta = sConsulta & " AND (SELECT COUNT(*) FROM UON2 WHERE UON1='" & DblQuote(sUON1) & "' AND COD='" & DblQuote(sUON2) & "' AND UON2.EMPRESA IS NOT NULL )<>0 " & sWhere
        
        sConsulta = sConsulta & " UNION " & sFrom & " INNER JOIN UON1 ON UON1.COD='" & DblQuote(sUON1) & "' AND UON1.EMPRESA=E.ID AND UON1.EMPRESA IS NOT NULL "
        sConsulta = sConsulta & " AND (SELECT COUNT(*) FROM UON2 WHERE UON1='" & DblQuote(sUON1) & "' AND COD='" & DblQuote(sUON2) & "' AND EMPRESA IS NOT NULL )=0 "
        sConsulta = sConsulta & " AND (SELECT COUNT(*) FROM UON3 WHERE UON1='" & DblQuote(sUON1) & "' AND UON2='" & DblQuote(sUON2) & "' AND COD = '" & DblQuote(sUON3) & "' AND EMPRESA IS NOT NULL)=0" & sWhere
        
        sConsulta = sConsulta & " UNION " & sFrom & " INNER JOIN UON2 ON UON2.UON1='" & DblQuote(sUON1) & "' AND UON2.EMPRESA=E.ID AND UON2.EMPRESA IS NOT NULL "
        sConsulta = sConsulta & " AND (SELECT COUNT(*) FROM UON2 WHERE UON1='" & DblQuote(sUON1) & "' AND COD='" & DblQuote(sUON2) & "' AND EMPRESA IS NOT NULL )=0 "
        sConsulta = sConsulta & " AND (SELECT COUNT(*) FROM UON3 WHERE UON1='" & DblQuote(sUON1) & "' AND UON2='" & DblQuote(sUON2) & "' AND COD = '" & DblQuote(sUON3) & "' AND EMPRESA IS NOT NULL)=0"
        sConsulta = sConsulta & " AND (SELECT COUNT(*) FROM UON1 WHERE COD='" & DblQuote(sUON1) & "' AND UON1.EMPRESA IS NOT NULL )<>0 " & sWhere
        
        sConsulta = sConsulta & " UNION " & sFrom & " INNER JOIN UON3 ON UON3.UON1='" & DblQuote(sUON1) & "' AND UON3.EMPRESA=E.ID AND UON3.EMPRESA IS NOT NULL "
        sConsulta = sConsulta & " AND (SELECT COUNT(*) FROM UON2 WHERE UON1='" & DblQuote(sUON1) & "' AND COD='" & DblQuote(sUON2) & "' AND EMPRESA IS NOT NULL )=0 "
        sConsulta = sConsulta & " AND (SELECT COUNT(*) FROM UON3 WHERE UON1='" & DblQuote(sUON1) & "' AND UON2='" & DblQuote(sUON2) & "' AND COD = '" & DblQuote(sUON3) & "' AND EMPRESA IS NOT NULL)=0"
        sConsulta = sConsulta & " AND (SELECT COUNT(*) FROM UON1 WHERE COD='" & DblQuote(sUON1) & "' AND UON1.EMPRESA IS NOT NULL )<>0 " & sWhere
        
        sConsulta = sFrom & sConsulta
        
    ElseIf sUON2 <> "" Then
        sConsulta = sConsulta & " INNER JOIN UON2 ON UON2.UON1='" & DblQuote(sUON1) & "' AND UON2.COD='" & DblQuote(sUON2) & "' AND E.ID = UON2.EMPRESA AND EMPRESA IS NOT NULL" & sWhere
        sConsulta = sConsulta & " UNION " & sFrom & " INNER JOIN UON1 ON UON1.COD='" & DblQuote(sUON1) & "' AND UON1.EMPRESA=E.ID AND UON1.EMPRESA IS NOT NULL "
        sConsulta = sConsulta & " AND (SELECT COUNT(*) FROM UON2 WHERE UON1='" & DblQuote(sUON1) & "' AND COD='" & DblQuote(sUON2) & "' AND EMPRESA IS NOT NULL )=0 " & sWhere
        
        sConsulta = sConsulta & " UNION " & sFrom & " INNER JOIN UON2 ON UON2.UON1='" & DblQuote(sUON1) & "' AND UON2.EMPRESA=E.ID  AND UON2.EMPRESA IS NOT NULL"
        sConsulta = sConsulta & " AND (SELECT COUNT(*) FROM UON2 WHERE UON1='" & DblQuote(sUON1) & "' AND COD='" & DblQuote(sUON2) & "' AND EMPRESA IS NOT NULL )=0 "
        sConsulta = sConsulta & " AND (SELECT COUNT(*) FROM UON1 WHERE UON1.COD='" & DblQuote(sUON1) & "' AND EMPRESA IS NOT NULL)<>0" & sWhere
        
        sConsulta = sConsulta & " UNION " & sFrom & " INNER JOIN UON3 ON UON3.UON1='" & DblQuote(sUON1) & "' AND UON3.EMPRESA=E.ID  AND UON3.EMPRESA IS NOT NULL"
        sConsulta = sConsulta & " AND (SELECT COUNT(*) FROM UON2 WHERE UON1='" & DblQuote(sUON1) & "' AND COD='" & DblQuote(sUON2) & "' AND EMPRESA IS NOT NULL )=0 "
        sConsulta = sConsulta & " AND (SELECT COUNT(*) FROM UON1 WHERE UON1.COD='" & DblQuote(sUON1) & "' AND EMPRESA IS NOT NULL)<>0" & sWhere
        
        sConsulta = sFrom & sConsulta
        
    ElseIf sUON1 <> "" Then
        sConsulta = sConsulta & " INNER JOIN UON1 ON UON1.COD='" & DblQuote(sUON1) & "' AND E.ID = UON1.EMPRESA"
        sConsulta = sFrom & sConsulta
    Else
        sConsulta = sFrom & sConsulta & sWhere
    End If

    Select Case lOrder
        Case 1
            sConsulta = sConsulta & " ORDER BY E.NIF"
        Case 2
            sConsulta = sConsulta & " ORDER BY E.DEN"
        Case Else
            sConsulta = sConsulta & " ORDER BY E.ID"
    End Select
    
    AdoRes.Open sConsulta, m_oConexion.adoCon, adOpenKeyset, adLockReadOnly
          
    If AdoRes.eof Then
            
        AdoRes.Close
        Set AdoRes = Nothing
        Set mCol = Nothing
        Set mCol = New Collection
        Exit Sub
          
    Else
        
        Set mCol = Nothing
        Set mCol = New Collection
        While Not AdoRes.eof
            Me.Add AdoRes.Fields("ID").Value, AdoRes.Fields("NIF").Value, AdoRes.Fields("DEN").Value, AdoRes.Fields("DIR").Value, AdoRes.Fields("CP").Value, AdoRes.Fields("PAI").Value, AdoRes.Fields("POB").Value, AdoRes.Fields("PROVI").Value, (AdoRes.Fields("ERP").Value = 1), m_oConexion
            AdoRes.MoveNext
        Wend
        
    End If
    
    AdoRes.Close
    Set AdoRes = Nothing
    
End Sub


