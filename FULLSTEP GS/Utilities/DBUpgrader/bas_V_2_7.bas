Attribute VB_Name = "bas_V_2_7"
Public Function CodigoDeActualizacion2_7_00A2_7_01() As Boolean
    Dim bTransaccionEnCurso As Boolean
    
    On Error GoTo error
    
    sConsulta = "BEGIN TRANSACTION"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    gRDOCon.Execute "SET XACT_ABORT OFF", rdExecDirect
    bTransaccionEnCurso = True
        
    '''MODIFICAR EL PROCEDIMIENTO ALMACENADO GMN1_COD
    
    frmProgreso.lblDetalle = "Modificamos la stored procedure GMN1_COD"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[GMN1_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[GMN1_COD]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearNuevoGMN1_COD, rdExecDirect
    
    '''MODIFICAR EL PROCEDIMIENTO ALMACENADO PROCE_COD
    
    frmProgreso.lblDetalle = "Modificamos la stored procedure PROCE_COD"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[PROCE_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[PROCE_COD]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearNuevoPROCE_COD, rdExecDirect
    
    '''MODIFICAR EL PROCEDIMIENTO ALMACENADO PROVE_COD
    
    frmProgreso.lblDetalle = "Modificamos la stored procedure PROVE_COD"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[PROVE_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[PROVE_COD]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearNuevoPROVE_COD, rdExecDirect
    
    
    '''PONER EL VALOR POR DEFECT A MAX_ADJUN

    sConsulta = "ALTER TABLE [dbo].[PARGEN_DEF] ADD CONSTRAINT"
    sConsulta = sConsulta & vbLf & "[DF_PARGEN_DEF_MAX_ADJUN] DEFAULT (0) FOR MAX_ADJUN"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE VERSION SET NUM ='2.7.01'" & ", FECHA='" & Format(Date, "mm-dd-yyyy") & "'"
    gRDOCon.Execute sConsulta, rdExecDirect
  
    gRDOCon.Execute "COMMIT TRANSACTION", rdExecDirect
    bTransaccionEnCurso = False

    CodigoDeActualizacion2_7_00A2_7_01 = True
    Exit Function
    
error:

    If bTransaccionEnCurso Then
        gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_7_00A2_7_01 = False

    
End Function


Public Function CodigoDeActualizacion2_7_01A2_7_02() As Boolean
    Dim bTransaccionEnCurso As Boolean
    
    On Error GoTo error
    
    gRDOCon.Execute "EXEC sp_dboption @dbname='" & frmPrincipal.txtBaseDatos & "', @optname='select into/bulkcopy', @optvalue='TRUE'"
    
    sConsulta = "BEGIN TRANSACTION"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    gRDOCon.Execute "SET XACT_ABORT OFF", rdExecDirect
    bTransaccionEnCurso = True
        
    '''MODIFICAR EL triger para la INSERT O UPDATE DE LA TABLA PAISES
    
    frmProgreso.lblDetalle = "Modificamos el trigger para la INSERT Y UPDATE de la tabla Paises"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[PAI_TG_INSUPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1) drop trigger [dbo].[PAI_TG_INSUPD]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearNuevoTriggerPais, rdExecDirect
    
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE VERSION SET NUM ='2.7.02'" & ", FECHA='" & Format(Date, "mm-dd-yyyy") & "'"
    gRDOCon.Execute sConsulta, rdExecDirect
  
    gRDOCon.Execute "COMMIT TRANSACTION", rdExecDirect
    bTransaccionEnCurso = False



    CodigoDeActualizacion2_7_01A2_7_02 = True
    Exit Function
    
error:

    If bTransaccionEnCurso Then
        gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_7_01A2_7_02 = False

    
End Function

Public Function CodigoDeActualizacion2_7_02A2_7_03() As Boolean
    Dim bTransaccionEnCurso As Boolean
    
    On Error GoTo error
        
    sConsulta = "BEGIN TRANSACTION"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    gRDOCon.Execute "SET XACT_ABORT OFF", rdExecDirect
    bTransaccionEnCurso = True
        
    '''Actualizamos el campo MAX_ADJUN 1 Mb.
    
    frmProgreso.lblDetalle = "Actualizamos el campo MAX_ADJUN 1 Mb."
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    gRDOCon.Execute "UPDATE PARGEN_DEF SET MAX_ADJUN=1048576", rdExecDirect
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE VERSION SET NUM ='2.7.03'" & ", FECHA='" & Format(Date, "mm-dd-yyyy") & "'"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    
    gRDOCon.Execute "COMMIT TRANSACTION", rdExecDirect
    bTransaccionEnCurso = False



    CodigoDeActualizacion2_7_02A2_7_03 = True
    Exit Function
    
error:

    If bTransaccionEnCurso Then
        gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_7_02A2_7_03 = False

    
End Function

Public Function CodigoDeActualizacion2_7_03A2_7_04() As Boolean
    Dim bTransaccionEnCurso As Boolean
    
    On Error GoTo error
        
    sConsulta = "BEGIN TRANSACTION"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    gRDOCon.Execute "SET XACT_ABORT OFF", rdExecDirect
    bTransaccionEnCurso = True
        
        
    '''MODIFICAR EL PROCEDIMIENTO ALMACENADO SP_DEVOLVER_ITEM_ESP_PORTAL
    
    frmProgreso.lblDetalle = "Creamos si no existe la stored procedure SP_DEVOLVER_ITEM_ESP_PORTAL"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[SP_DEVOLVER_ITEM_ESP_PORTAL]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[SP_DEVOLVER_ITEM_ESP_PORTAL]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearNuevoSP_DEVOLVER_ITEM_ESP_PORTAL, rdExecDirect
        
    '''MODIFICAR EL PROCEDIMIENTO ALMACENADO SP_DEVOLVER_PROCE_ESP_PORTAL
    
    frmProgreso.lblDetalle = "Creamos si no existe la stored procedure SP_DEVOLVER_PROCE_ESP_PORTAL"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[SP_DEVOLVER_PROCE_ESP_PORTAL]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[SP_DEVOLVER_PROCE_ESP_PORTAL]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearNuevoSP_DEVOLVER_PROCE_ESP_PORTAL, rdExecDirect
    
    '''MODIFICAR EL PROCEDIMIENTO ALMACENADO SP_DOWNLOAD_PROCE_ESP
    
    frmProgreso.lblDetalle = "Creamos si no existe la stored procedure SP_DOWNLOAD_PROCE_ESP"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[SP_DOWNLOAD_PROCE_ESP]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[SP_DOWNLOAD_PROCE_ESP]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearNuevoSP_DOWNLOAD_PROCE_ESP, rdExecDirect
        
    '''MODIFICAR EL PROCEDIMIENTO ALMACENADO SP_DOWNLOAD_PROCE_ITEM_ESP
    
    frmProgreso.lblDetalle = "Creamos si no existe la stored procedure SP_DOWNLOAD_PROCE_ITEM_ESP"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[SP_DOWNLOAD_PROCE_ITEM_ESP]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[SP_DOWNLOAD_PROCE_ITEM_ESP]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearNuevoSP_DOWNLOAD_PROCE_ITEM_ESP, rdExecDirect
        
    '''MODIFICAR EL PROCEDIMIENTO ALMACENADO SP_ESP_ITEM2
    
    frmProgreso.lblDetalle = "Creamos si no existe la stored procedure SP_ESP_ITEM2"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[SP_ESP_ITEM2]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[SP_ESP_ITEM2]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearNuevoSP_ESP_ITEM2, rdExecDirect
        
    '''MODIFICAR EL PROCEDIMIENTO ALMACENADO SP_ESP_PROCE2
    
    frmProgreso.lblDetalle = "Creamos si no existe la stored procedure SP_ESP_PROCE2"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[SP_ESP_PROCE2]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[SP_ESP_PROCE2]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearNuevoSP_ESP_PROCE2, rdExecDirect
        
    '''MODIFICAR EL PROCEDIMIENTO ALMACENADO SP_HAY_ESPECIFICACIONES
    
    frmProgreso.lblDetalle = "Creamos si no existe la stored procedure SP_HAY_ESPECIFICACIONES"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[SP_HAY_ESPECIFICACIONES]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[SP_HAY_ESPECIFICACIONES]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearNuevoSP_HAY_ESPECIFICACIONES, rdExecDirect
        
        
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE VERSION SET NUM ='2.7.04'" & ", FECHA='" & Format(Date, "mm-dd-yyyy") & "'"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    
    gRDOCon.Execute "COMMIT TRANSACTION", rdExecDirect
    bTransaccionEnCurso = False



    CodigoDeActualizacion2_7_03A2_7_04 = True
    Exit Function
    
error:

    If bTransaccionEnCurso Then
        gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_7_03A2_7_04 = False

    
End Function
Public Function CodigoDeActualizacion2_7_04A2_7_05() As Boolean
    Dim bTransaccionEnCurso As Boolean
    
    On Error GoTo error
        
    sConsulta = "BEGIN TRANSACTION"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    gRDOCon.Execute "SET XACT_ABORT OFF", rdExecDirect
    bTransaccionEnCurso = True
        
        
    '''MODIFICAR EL TRIGGER PROCE_OFE_TG_INS
    
    frmProgreso.lblDetalle = "Creamos si no existe el trigger PROCE_OFE_TG_INS"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[PROCE_OFE_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1) drop trigger [dbo].[PROCE_OFE_TG_INS]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearNuevoTriggerPROCE_OFE_TG_INS, rdExecDirect
        
        
        
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE VERSION SET NUM ='2.7.05'" & ", FECHA='" & Format(Date, "mm-dd-yyyy") & "'"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    
    gRDOCon.Execute "COMMIT TRANSACTION", rdExecDirect
    bTransaccionEnCurso = False



    CodigoDeActualizacion2_7_04A2_7_05 = True
    Exit Function
    
error:

    If bTransaccionEnCurso Then
        gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_7_04A2_7_05 = False

    
End Function

Public Function CodigoDeActualizacion2_7_05A2_7_06() As Boolean
    Dim bTransaccionEnCurso As Boolean
    
    On Error GoTo error
        
    sConsulta = "BEGIN TRANSACTION"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    gRDOCon.Execute "SET XACT_ABORT OFF", rdExecDirect
    bTransaccionEnCurso = True
        
        
    '''MODIFICAR EL TRIGGER PROCE_OFE_TG_INS
    
    frmProgreso.lblDetalle = "Creamos si no existe la foreign key FK_ITEM_OFE_PROCE_OFE"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[FK_ITEM_OFE_PROCE_OFE]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1) ALTER TABLE [dbo].[ITEM_OFE] DROP CONSTRAINT [FK_ITEM_OFE_PROCE_OFE]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute A�adirForeignKey_Item_Ofe_2_7_06, rdExecDirect
        
'''MODIFICAR EL STORED SP_MODIF_ITEM_OFE
        
    frmProgreso.lblDetalle = "Creamos si no existe la stored procedure SP_MODIF_ITEM_OFE"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[SP_MODIF_ITEM_OFE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[SP_MODIF_ITEM_OFE]"
    gRDOCon.Execute sConsulta, rdExecDirect
            
sConsulta = "CREATE PROCEDURE  SP_MODIF_ITEM_OFE (@PRECIO  AS float, @CANTMAX AS float,@ANYO SMALLINT,@GMN1 VARCHAR(50), @PROCE INT,@ITEM INT,@PROVE VARCHAR(50) ,@OFE INT,@WEB BIT = 0)  AS"
sConsulta = sConsulta & vbCrLf & "BEGIN "
sConsulta = sConsulta & vbCrLf & "   Update ITEM_OFE"
sConsulta = sConsulta & vbCrLf & "           SET PRECIO=@PRECIO, CANTMAX=@CANTMAX"
sConsulta = sConsulta & vbCrLf & "    WHERE ANYO=@ANYO"
sConsulta = sConsulta & vbCrLf & "         AND GMN1=@GMN1"
sConsulta = sConsulta & vbCrLf & "         AND PROCE=@PROCE"
sConsulta = sConsulta & vbCrLf & "         AND ITEM=@ITEM"
sConsulta = sConsulta & vbCrLf & "         AND PROVE=@PROVE"
sConsulta = sConsulta & vbCrLf & "         AND NUM=@OFE"
sConsulta = sConsulta & vbCrLf & "IF @WEB = 1"
sConsulta = sConsulta & vbCrLf & "    BEGIN"
sConsulta = sConsulta & vbCrLf & "    DECLARE @OFEWEB INT"
sConsulta = sConsulta & vbCrLf & "    SELECT @OFEWEB = OFEWEB FROM PROCE_OFE_WEB WHERE ANYO=@ANYO AND PROCE=@PROCE AND GMN1=@GMN1 AND PROVE=@PROVE AND OFE=@OFE"
sConsulta = sConsulta & vbCrLf & "             INSERT INTO ITEM_OFE_WEB VALUES (@ANYO,@GMN1,@PROCE,@ITEM,@PROVE,@OFEWEB, @PRECIO , @CANTMAX )"
sConsulta = sConsulta & vbCrLf & "    End"
sConsulta = sConsulta & vbCrLf & "End"
gRDOCon.Execute sConsulta, rdExecDirect
        
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE VERSION SET NUM ='2.7.06'" & ", FECHA='" & Format(Date, "mm-dd-yyyy") & "'"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    
    gRDOCon.Execute "COMMIT TRANSACTION", rdExecDirect
    bTransaccionEnCurso = False



    CodigoDeActualizacion2_7_05A2_7_06 = True
    Exit Function
    
error:

    If bTransaccionEnCurso Then
        gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_7_05A2_7_06 = False

    
End Function

Public Function CodigoDeActualizacion2_7_06A2_7_25() As Boolean
    Dim bTransaccionEnCurso As Boolean
    
    On Error GoTo error
        
    sConsulta = "BEGIN TRANSACTION"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    gRDOCon.Execute "SET XACT_ABORT OFF", rdExecDirect
    bTransaccionEnCurso = True
        
        
    'Nuevo campo SUBASTA en PARGEN_INTERNO
    sConsulta = "ALTER TABLE [dbo].[PARGEN_INTERNO] ADD SUBASTA [tinyint]  NOT NULL DEFAULT (0)"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    'Nuevo campo SUPERADA en PROCE_PROVE
    sConsulta = "ALTER TABLE [dbo].[PROCE_PROVE] ADD SUPERADA [tinyint]  NOT NULL DEFAULT (0)"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    'Nuevos campos SUBASTA,SUBASTAPROVE y SUBASTAPUJAS en PROCE
    sConsulta = "ALTER TABLE [dbo].[PROCE] ADD SUBASTA [tinyint]  NOT NULL DEFAULT (0)"
    gRDOCon.Execute sConsulta, rdExecDirect
    sConsulta = "ALTER TABLE [dbo].[PROCE] ADD SUBASTAPROVE [tinyint]  NOT NULL DEFAULT (0)"
    gRDOCon.Execute sConsulta, rdExecDirect
    sConsulta = "ALTER TABLE [dbo].[PROCE] ADD SUBASTAPUJAS [tinyint]  NOT NULL DEFAULT (0)"
    gRDOCon.Execute sConsulta, rdExecDirect

    'A�adir los siguientes registros en ACC
    frmProgreso.lblDetalle = "A�adir nuevos registros en la tabla ACC"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    
    sConsulta = "INSERT INTO ACC (ID) VALUES (12017)"
    gRDOCon.Execute sConsulta, rdExecDirect
    sConsulta = "INSERT INTO ACC (ID) VALUES (12018)"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    
    'A�adir los siguientes registros en ACC_DEPEN
    frmProgreso.lblDetalle = "A�adimos registros a ACC_DEPEN"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    
    sConsulta = "INSERT INTO ACC_DEPEN (ACC,EST,ACC_DEPEN,EST_DEPEN) VALUES (12000,0,12017,0)"
    gRDOCon.Execute sConsulta, rdExecDirect
    sConsulta = "INSERT INTO ACC_DEPEN (ACC,EST,ACC_DEPEN,EST_DEPEN) VALUES (12000,0,12018,0)"
    gRDOCon.Execute sConsulta, rdExecDirect
    sConsulta = "INSERT INTO ACC_DEPEN (ACC,EST,ACC_DEPEN,EST_DEPEN) VALUES (12003,1,12017,1)"
    gRDOCon.Execute sConsulta, rdExecDirect
    sConsulta = "INSERT INTO ACC_DEPEN (ACC,EST,ACC_DEPEN,EST_DEPEN) VALUES (12017,0,12003,0)"
    gRDOCon.Execute sConsulta, rdExecDirect
    sConsulta = "INSERT INTO ACC_DEPEN (ACC,EST,ACC_DEPEN,EST_DEPEN) VALUES (12017,0,12018,0)"
    gRDOCon.Execute sConsulta, rdExecDirect
    sConsulta = "INSERT INTO ACC_DEPEN (ACC,EST,ACC_DEPEN,EST_DEPEN) VALUES (12018,1,12017,1)"
    gRDOCon.Execute sConsulta, rdExecDirect
   
   
    'Modificar la tabla USU_ACC : Los usuarios que tengan permisos para modificar datos generales (12003) tendr�n que a�adir un registro tambi�n para permitir consultar las subastas (12017)
    frmProgreso.lblDetalle = "A�adimos nuevos registros en la tabla USU_ACC"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "INSERT INTO USU_ACC (USU,ACC) SELECT USU,12017 FROM USU_ACC WHERE ACC=12003"
    gRDOCon.Execute sConsulta

    'Nueva tabla OFEMIN
    frmProgreso.lblDetalle = "Creamos la tabla OFEMIN"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    gRDOCon.Execute CrearTablaOFEMIN, rdExecDirect


    'Nueva tabla PROVENOTIF
    frmProgreso.lblDetalle = "Creamos la tabla PROVENOTIF"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    gRDOCon.Execute CrearTablaPROVENOTIF, rdExecDirect
    
    'Nuevo trigger en la tabla PROCE :PROCE_TG_UPD
    frmProgreso.lblDetalle = "Insertamos el trigger para la UPDATE de la tabla Proce"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[PROCE_TG_UPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1) drop trigger [dbo].[PROCE_TG_UPD]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearNuevoTriggerPROCE_TG_UPD, rdExecDirect
    
    
    'Nuevo trigger en la tabla ITEM_OFE :ITEM_OFE_TG_UPD
    frmProgreso.lblDetalle = "Insertamos el trigger para la UPDATE de la tabla ITEM_OFE"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[ITEM_OFE_TG_UPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1) drop trigger [dbo].[ITEM_OFE_TG_UPD]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearNuevoTriggerITEM_OFE_TG_UPD, rdExecDirect


    'Nuevo trigger en la tabla PROCE_OFE :PROCE_OFE_TG_DEL
    frmProgreso.lblDetalle = "Insertamos el trigger para la DELETE de la tabla PROCE_OFE"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[PROCE_OFE_TG_DEL]') and OBJECTPROPERTY(id, N'IsTrigger') = 1) drop trigger [dbo].[PROCE_OFE_TG_DEL]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearNuevoTriggerPROCE_OFE_TG_DEL, rdExecDirect

    'Nuevo store procedure SP_DEVOLVER_PROCESOS_PROVE_WEB
    frmProgreso.lblDetalle = "Creamos el store procedure SP_DEVOLVER_PROCESOS_PROVE_WEB"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[SP_DEVOLVER_PROCESOS_PROVE_WEB]') and OBJECTPROPERTY(id, N'IsTrigger') = 1) drop procedure [dbo].[SP_DEVOLVER_PROCESOS_PROVE_WEB]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearSP_DEVOLVER_PROCESOS_PROVE_WEB, rdExecDirect

    'Nuevo store procedure SP_DEVOLVER_PROCESO_WEB
    frmProgreso.lblDetalle = "Creamos el store procedure SP_DEVOLVER_PROCESO_WEB"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[SP_DEVOLVER_PROCESO_WEB]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[SP_DEVOLVER_PROCESO_WEB]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearSP_DEVOLVER_PROCESO_WEB, rdExecDirect
    
    'Nuevo store procedure SP_DEVOLVER_OFERTA_PROVE_WEB
    frmProgreso.lblDetalle = "Creamos el store procedure SP_DEVOLVER_OFERTA_PROVE_WEB"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[SP_DEVOLVER_OFERTA_PROVE_WEB]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[SP_DEVOLVER_OFERTA_PROVE_WEB]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearSP_DEVOLVER_OFERTA_PROVE_WEB, rdExecDirect
    
    'Nuevo store procedure SP_DEVOLVER_PUJAS_ITEM_WEB
    frmProgreso.lblDetalle = "Creamos el store procedure SP_DEVOLVER_PUJAS_ITEM_WEB"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[SP_DEVOLVER_PUJAS_ITEM_WEB]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[SP_DEVOLVER_PUJAS_ITEM_WEB]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearSP_DEVOLVER_PUJAS_ITEM_WEB, rdExecDirect

    'Nueva tabla DETALLE_PUJAS
    frmProgreso.lblDetalle = "Creamos la tabla DETALLE_PUJAS"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    gRDOCon.Execute CrearTablaDETALLE_PUJAS, rdExecDirect
    
    'Modificar el store procedure SP_ITEMS
    frmProgreso.lblDetalle = "Modificamos el store procedure SP_ITEMS"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[SP_ITEMS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[SP_ITEMS]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearSP_ITEMS_2_7_25, rdExecDirect
            
        
        
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE VERSION SET NUM ='2.7.25'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    gRDOCon.Execute "COMMIT TRANSACTION", rdExecDirect
    bTransaccionEnCurso = False

    CodigoDeActualizacion2_7_06A2_7_25 = True
    Exit Function
    
error:

    If bTransaccionEnCurso Then
        gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_7_06A2_7_25 = False
End Function
Public Function CodigoDeActualizacion2_7_25A2_7_25_2() As Boolean
Dim bTransaccionEnCurso As Boolean
    
    On Error GoTo error
        
    sConsulta = "BEGIN TRANSACTION"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    gRDOCon.Execute "SET XACT_ABORT OFF", rdExecDirect
    bTransaccionEnCurso = True
        
    'Nuevos campos en USU
    frmProgreso.lblDetalle = "A�adimos campos a USU"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    gRDOCon.Execute A�adirCamposUSU_2_7_25, rdExecDirect
    
    'Modificar el trigger ITEM_OFE_TG_UPD para que saque bien la denominaci�n del item.
    sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ITEM_OFE_TG_UPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)"
    sConsulta = sConsulta & vbCrLf & "drop trigger [dbo].[ITEM_OFE_TG_UPD]"
    gRDOCon.Execute sConsulta, rdExecDirect
    sConsulta = ModificarTriggerITEM_OFE
    gRDOCon.Execute sConsulta, rdExecDirect
        
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE VERSION SET NUM ='2.7.25.2'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    gRDOCon.Execute "COMMIT TRANSACTION", rdExecDirect
    bTransaccionEnCurso = False

    CodigoDeActualizacion2_7_25A2_7_25_2 = True
    Exit Function
    
error:

    If bTransaccionEnCurso Then
        gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_7_25A2_7_25_2 = False
End Function

Public Function CodigoDeActualizacion2_7_25_2A2_7_50() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String

    On Error GoTo error
        
    sConsulta = "BEGIN TRANSACTION"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    gRDOCon.Execute "SET XACT_ABORT OFF", rdExecDirect
    bTransaccionEnCurso = True

    'Nueva tabla ART4_ESP
    frmProgreso.lblDetalle = "Nueva tabla ART4_ESP"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    gRDOCon.Execute CrearTablaART4_ESP, rdExecDirect
    
    'Nuevo trigger en ART4_ESP llamado ART4_ESP_TG_INSUPD
    frmProgreso.lblDetalle = "Nueva trigger ART4_ESP_TG_INSUPD en la tabla ART4_ESP"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[ART4_ESP_TG_INSUPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1) drop trigger [dbo].[ART4_ESP_TG_INSUPD]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearTriggerART4_ESP_TG_INSUPD, rdExecDirect
    
    'Modificaci�n de stored procedure ART4_COD
    frmProgreso.lblDetalle = "Modificamos la store procedure ART4_COD"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[ART4_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[ART4_COD]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute ModificarART4_COD_2_7_50, rdExecDirect
    
    'Modificaci�n de stored procedure GMN2_COD
    frmProgreso.lblDetalle = "Modificamos la store procedure GMN2_COD"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[GMN2_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[GMN2_COD]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute ModificarGMN2_COD_2_7_50, rdExecDirect
    
    'Modificaci�n de stored procedure GMN3_COD
    frmProgreso.lblDetalle = "Modificamos la store procedure GMN3_COD"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[GMN3_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[GMN3_COD]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute ModificarGMN3_COD_2_7_50, rdExecDirect
    
    'Modificaci�n de stored procedure GMN4_COD
    frmProgreso.lblDetalle = "Modificamos la store procedure GMN4_COD"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[GMN4_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[GMN4_COD]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute ModificarGMN4_COD_2_7_50, rdExecDirect
    
    'Nuevo campo ESP en ART4
    frmProgreso.lblDetalle = "Nueva campo ESP en ART4"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "ALTER TABLE [dbo].[ART4] ADD ESP [varchar] (500) NULL"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    'nuevos campos ARTIC_ADJESP,USAPRES1,USAPRES2,USAPRES3,USAPRES4 en PARGEN_GEST
    frmProgreso.lblDetalle = "Nuevos campos en PARGEN_GEST"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "ALTER TABLE [dbo].[PARGEN_GEST] ADD "
    sConsulta = sConsulta & vbCrLf & "    ARTIC_ADJESP [tinyint] NOT NULL DEFAULT (2),"
    sConsulta = sConsulta & vbCrLf & "    USAPRES1 [tinyint]  NULL DEFAULT (1),"
    sConsulta = sConsulta & vbCrLf & "    USAPRES2 [tinyint]  NULL DEFAULT (1),"
    sConsulta = sConsulta & vbCrLf & "    USAPRES3 [tinyint]  NULL DEFAULT (1),"
    sConsulta = sConsulta & vbCrLf & "    USAPRES4 [tinyint]  NULL DEFAULT (1)"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    'Nueva tabla PRES_3_CON1
    frmProgreso.lblDetalle = "Nueva tabla PRES_3_CON1"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    gRDOCon.Execute CrearTablaPRES_3_CON1, rdExecDirect
    
    'Nuevo trigger en PRES_3_CON1 llamado PRES_3_CON1_TG_INSUPD
    frmProgreso.lblDetalle = "Nueva trigger PRES_3_CON1_TG_INSUPD en la tabla PRES_3_CON1"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[PRES_3_CON1_TG_INSUPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1) drop trigger [dbo].[PRES_3_CON1_TG_INSUPD]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearTriggerPRES_3_CON1_TG_INSUPD, rdExecDirect

    'Crear la stored Procedure: PRES_3_CON1_COD
    frmProgreso.lblDetalle = "Creamos la store procedure PRES_3_CON1_COD"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[PRES_3_CON1_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[PRES_3_CON1_COD]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearPRES_3_CON1_COD, rdExecDirect
    
    'Nueva tabla PRES_3_CON2
    frmProgreso.lblDetalle = "Nueva tabla PRES_3_CON2"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    gRDOCon.Execute CrearTablaPRES_3_CON2, rdExecDirect
    
    'Nuevo trigger en PRES_3_CON2 llamado PRES_3_CON2_TG_INSUPD
    frmProgreso.lblDetalle = "Nueva trigger PRES_3_CON2_TG_INSUPD en la tabla PRES_3_CON2"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[PRES_3_CON2_TG_INSUPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1) drop trigger [dbo].[PRES_3_CON2_TG_INSUPD]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearTriggerPRES_3_CON2_TG_INSUPD, rdExecDirect
    
    'Crear la stored Procedure: PRES_3_CON2_COD
    frmProgreso.lblDetalle = "Creamos la store procedure PRES_3_CON2_COD"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[PRES_3_CON2_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[PRES_3_CON2_COD]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearPRES_3_CON2_COD, rdExecDirect
    
    'Nueva tabla PRES_3_CON3
    frmProgreso.lblDetalle = "Nueva tabla PRES_3_CON3"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    gRDOCon.Execute CrearTablaPPRES_3_CON3, rdExecDirect
    
    'Nuevo trigger en PRES_3_CON3 llamado PRES_3_CON3_TG_INSUPD
    frmProgreso.lblDetalle = "Nueva trigger PRES_3_CON3_TG_INSUPD en la tabla PRES_3_CON3"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[PRES_3_CON3_TG_INSUPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1) drop trigger [dbo].[PRES_3_CON3_TG_INSUPD]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearTriggerPRES_3_CON3_TG_INSUPD, rdExecDirect
    
    'Crea la stored Procedure: PRES_3_CON3_COD
    frmProgreso.lblDetalle = "Creamos la store procedure PRES_3_CON3_COD"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[PRES_3_CON3_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[PRES_3_CON3_COD]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearPRES_3_CON3_COD, rdExecDirect
    
    'Nueva tabla PRES_3_CON4
    frmProgreso.lblDetalle = "Nueva tabla PRES_3_CON4"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    gRDOCon.Execute CrearTablaPRES_3_CON4, rdExecDirect
    
    'Nuevo trigger en PRES_3_CON4 llamado PRES_3_CON4_TG_INSUPD
    frmProgreso.lblDetalle = "Nueva trigger PRES_3_CON4_TG_INSUPD en la tabla PRES_3_CON4"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[PRES_3_CON4_TG_INSUPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1) drop trigger [dbo].[PRES_3_CON4_TG_INSUPD]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearTriggerPRES_3_CON4_TG_INSUPD, rdExecDirect
    
    'Crear la stored Procedure: PRES_3_CON4_COD
    frmProgreso.lblDetalle = "Creamos la store procedure PRES_3_CON4_COD"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[PRES_3_CON4_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[PRES_3_CON4_COD]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearPRES_3_CON4_COD, rdExecDirect
    
    'Nueva tabla PRES_4_CON1
    frmProgreso.lblDetalle = "Nueva tabla PRES_4_CON1"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    gRDOCon.Execute CrearTablaPRES_4_CON1, rdExecDirect
    
    'Nuevo trigger en PRES_4_CON1 llamado PRES_4_CON1_TG_INSUPD
    frmProgreso.lblDetalle = "Nueva trigger PRES_4_CON1_TG_INSUPD en la tabla PRES_4_CON1"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[PRES_4_CON1_TG_INSUPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1) drop trigger [dbo].[PRES_4_CON1_TG_INSUPD]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearTriggerPRES_4_CON1_TG_INSUPD, rdExecDirect
    
    'Crea la stored Procedure: PRES_4_CON1_COD
    frmProgreso.lblDetalle = "Creamos la store procedure PRES_4_CON1_COD"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[PRES_4_CON1_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[PRES_4_CON1_COD]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearPRES_4_CON1_COD, rdExecDirect
    
    'Nueva tabla PRES_4_CON2
    frmProgreso.lblDetalle = "Nueva tabla PRES_4_CON2"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    gRDOCon.Execute CrearTablaPRES_4_CON2, rdExecDirect
    
    'Nuevo trigger en PRES_4_CON2 llamado PRES_4_CON2_TG_INSUPD
    frmProgreso.lblDetalle = "Nueva trigger PRES_4_CON2_TG_INSUPD en la tabla PRES_4_CON2"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[PRES_4_CON2_TG_INSUPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1) drop trigger [dbo].[PRES_4_CON2_TG_INSUPD]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearTriggerPRES_4_CON2_TG_INSUPD, rdExecDirect
    
    'Crea la stored Procedure: PRES_4_CON2_COD
    frmProgreso.lblDetalle = "Creamos la store procedure PRES_4_CON2_COD"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[PRES_4_CON2_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[PRES_4_CON2_COD]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearPRES_4_CON2_COD, rdExecDirect
    
    'Nueva tabla PRES_4_CON3
    frmProgreso.lblDetalle = "Nueva tabla PRES_4_CON3"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    gRDOCon.Execute CrearTablaPRES_4_CON3, rdExecDirect
    
    'Nuevo trigger en PRES_4_CON3 llamado PRES_4_CON3_TG_INSUPD
    frmProgreso.lblDetalle = "Nueva trigger PRES_4_CON3_TG_INSUPD en la tabla PRES_4_CON3"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[PRES_4_CON3_TG_INSUPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1) drop trigger [dbo].[PRES_4_CON3_TG_INSUPD]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearTriggerPRES_4_CON3_TG_INSUPD, rdExecDirect
    
    'Crea la stored Procedure: PRES_4_CON3_COD
    frmProgreso.lblDetalle = "Creamos la store procedure PRES_4_CON3_COD"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[PRES_4_CON3_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[PRES_4_CON3_COD]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearPRES_4_CON3_COD, rdExecDirect
    
    'Nueva tabla PRES_4_CON4
    frmProgreso.lblDetalle = "Nueva tabla PRES_4_CON4"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    gRDOCon.Execute CrearTablaPRES_4_CON4, rdExecDirect
    
    'Nuevo trigger en PRES_4_CON4 llamado PRES_4_CON4_TG_INSUPD
    frmProgreso.lblDetalle = "Nueva trigger PRES_4_CON4_TG_INSUPD en la tabla PRES_4_CON4"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[PRES_4_CON4_TG_INSUPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1) drop trigger [dbo].[PRES_4_CON4_TG_INSUPD]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearTriggerPRES_4_CON4_TG_INSUPD, rdExecDirect
    
    'Crea la stored Procedure: PRES_4_CON4_COD
    frmProgreso.lblDetalle = "Creamos la store procedure PRES_4_CON4_COD"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[PRES_4_CON4_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[PRES_4_CON4_COD]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearPRES_4_CON4_COD, rdExecDirect
    
    'nueva tabla ITEM_PRESCON3
    frmProgreso.lblDetalle = "Nueva tabla ITEM_PRESCON3"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    gRDOCon.Execute CrearTablaITEM_PRESCON3, rdExecDirect
    
    'Nueva tabla ITEM_PRESCON4
    frmProgreso.lblDetalle = "Nueva tabla ITEM_PRESCON4"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    gRDOCon.Execute CrearTablaITEM_PRESCON4, rdExecDirect
        
    'Modificaci�n de stored procedure GMN1_COD
    frmProgreso.lblDetalle = "Modificamos la store procedure GMN1_COD"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[GMN1_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[GMN1_COD]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute ModificarGMN1_COD_2_7_50, rdExecDirect
    
    'Modificaci�n del stored procedure PROCE_COD
    frmProgreso.lblDetalle = "Modificamos la store procedure PROCE_COD"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[PROCE_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[PROCE_COD]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute ModificarPROCE_COD_2_7_50, rdExecDirect

    'Se a�aden los siguientes registros a la tabla PARGEN_LIT
    sConsulta = "INSERT INTO PARGEN_LIT (ID,IDI,DEN) VALUES(24,'SPA', NULL)"
    gRDOCon.Execute sConsulta
    sConsulta = "INSERT INTO PARGEN_LIT (ID,IDI,DEN) VALUES(24,'ENG', NULL)"
    gRDOCon.Execute sConsulta
    sConsulta = "INSERT INTO PARGEN_LIT (ID,IDI,DEN) VALUES(24,'FRA', NULL)"
    gRDOCon.Execute sConsulta
    sConsulta = "INSERT INTO PARGEN_LIT (ID,IDI,DEN) VALUES(24,'GER', NULL)"
    gRDOCon.Execute sConsulta
    sConsulta = "INSERT INTO PARGEN_LIT (ID,IDI,DEN) VALUES(24,'POR', NULL)"
    gRDOCon.Execute sConsulta
    sConsulta = "INSERT INTO PARGEN_LIT (ID,IDI,DEN) VALUES(25,'SPA', NULL)"
    gRDOCon.Execute sConsulta
    sConsulta = "INSERT INTO PARGEN_LIT (ID,IDI,DEN) VALUES(25,'ENG', NULL)"
    gRDOCon.Execute sConsulta
    sConsulta = "INSERT INTO PARGEN_LIT (ID,IDI,DEN) VALUES(25,'FRA', NULL)"
    gRDOCon.Execute sConsulta
    sConsulta = "INSERT INTO PARGEN_LIT (ID,IDI,DEN) VALUES(25,'GER', NULL)"
    gRDOCon.Execute sConsulta
    sConsulta = "INSERT INTO PARGEN_LIT (ID,IDI,DEN) VALUES(25,'POR', NULL)"
    gRDOCon.Execute sConsulta
    sConsulta = "INSERT INTO PARGEN_LIT (ID,IDI,DEN) VALUES(26,'SPA', NULL)"
    gRDOCon.Execute sConsulta
    sConsulta = "INSERT INTO PARGEN_LIT (ID,IDI,DEN) VALUES(26,'ENG', NULL)"
    gRDOCon.Execute sConsulta
    sConsulta = "INSERT INTO PARGEN_LIT (ID,IDI,DEN) VALUES(26,'FRA', NULL)"
    gRDOCon.Execute sConsulta
    sConsulta = "INSERT INTO PARGEN_LIT (ID,IDI,DEN) VALUES(26,'GER', NULL)"
    gRDOCon.Execute sConsulta
    sConsulta = "INSERT INTO PARGEN_LIT (ID,IDI,DEN) VALUES(26,'POR', NULL)"
    gRDOCon.Execute sConsulta
    sConsulta = "INSERT INTO PARGEN_LIT (ID,IDI,DEN) VALUES(27,'SPA', NULL)"
    gRDOCon.Execute sConsulta
    sConsulta = "INSERT INTO PARGEN_LIT (ID,IDI,DEN) VALUES(27,'ENG', NULL)"
    gRDOCon.Execute sConsulta
    sConsulta = "INSERT INTO PARGEN_LIT (ID,IDI,DEN) VALUES(27,'FRA', NULL)"
    gRDOCon.Execute sConsulta
    sConsulta = "INSERT INTO PARGEN_LIT (ID,IDI,DEN) VALUES(27,'GER', NULL)"
    gRDOCon.Execute sConsulta
    sConsulta = "INSERT INTO PARGEN_LIT (ID,IDI,DEN) VALUES(27,'POR', NULL)"
    gRDOCon.Execute sConsulta
    sConsulta = "INSERT INTO PARGEN_LIT (ID,IDI,DEN) VALUES(28,'SPA', NULL)"
    gRDOCon.Execute sConsulta
    sConsulta = "INSERT INTO PARGEN_LIT (ID,IDI,DEN) VALUES(28,'ENG', NULL)"
    gRDOCon.Execute sConsulta
    sConsulta = "INSERT INTO PARGEN_LIT (ID,IDI,DEN) VALUES(28,'FRA', NULL)"
    gRDOCon.Execute sConsulta
    sConsulta = "INSERT INTO PARGEN_LIT (ID,IDI,DEN) VALUES(28,'GER', NULL)"
    gRDOCon.Execute sConsulta
    sConsulta = "INSERT INTO PARGEN_LIT (ID,IDI,DEN) VALUES(28,'POR', NULL)"
    gRDOCon.Execute sConsulta
    sConsulta = "INSERT INTO PARGEN_LIT (ID,IDI,DEN) VALUES(29,'SPA', NULL)"
    gRDOCon.Execute sConsulta
    sConsulta = "INSERT INTO PARGEN_LIT (ID,IDI,DEN) VALUES(29,'ENG', NULL)"
    gRDOCon.Execute sConsulta
    sConsulta = "INSERT INTO PARGEN_LIT (ID,IDI,DEN) VALUES(29,'FRA', NULL)"
    gRDOCon.Execute sConsulta
    sConsulta = "INSERT INTO PARGEN_LIT (ID,IDI,DEN) VALUES(29,'GER', NULL)"
    gRDOCon.Execute sConsulta
    sConsulta = "INSERT INTO PARGEN_LIT (ID,IDI,DEN) VALUES(29,'POR', NULL)"
    gRDOCon.Execute sConsulta
    sConsulta = "INSERT INTO PARGEN_LIT (ID,IDI,DEN) VALUES(30,'SPA', NULL)"
    gRDOCon.Execute sConsulta
    sConsulta = "INSERT INTO PARGEN_LIT (ID,IDI,DEN) VALUES(30,'ENG', NULL)"
    gRDOCon.Execute sConsulta
    sConsulta = "INSERT INTO PARGEN_LIT (ID,IDI,DEN) VALUES(30,'FRA', NULL)"
    gRDOCon.Execute sConsulta
    sConsulta = "INSERT INTO PARGEN_LIT (ID,IDI,DEN) VALUES(30,'GER', NULL)"
    gRDOCon.Execute sConsulta
    sConsulta = "INSERT INTO PARGEN_LIT (ID,IDI,DEN) VALUES(30,'POR', NULL)"
    gRDOCon.Execute sConsulta
    
    'Nuevos registros en la tabla ACC
    frmProgreso.lblDetalle = "Insertamos nuevos registros en la tabla ACC"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "INSERT INTO ACC (ID) VALUES (19200)"
    gRDOCon.Execute sConsulta, rdExecDirect
    sConsulta = "INSERT INTO ACC (ID) VALUES (19201)"
    gRDOCon.Execute sConsulta, rdExecDirect
    sConsulta = "INSERT INTO ACC (ID) VALUES (19300)"
    gRDOCon.Execute sConsulta, rdExecDirect
    sConsulta = "INSERT INTO ACC (ID) VALUES (19301)"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    'Se a�aden los siguientes registros a la tabla ACC_DEPEN:
    sConsulta = "INSERT INTO ACC_DEPEN (ACC,EST,ACC_DEPEN,EST_DEPEN) VALUES (19200,0,19201,0)"
    gRDOCon.Execute sConsulta, rdExecDirect
    sConsulta = "INSERT INTO ACC_DEPEN (ACC,EST,ACC_DEPEN,EST_DEPEN) VALUES (19201,1,19200,1)"
    gRDOCon.Execute sConsulta, rdExecDirect
    sConsulta = "INSERT INTO ACC_DEPEN (ACC,EST,ACC_DEPEN,EST_DEPEN) VALUES (19300,0,19301,0)"
    gRDOCon.Execute sConsulta, rdExecDirect
    sConsulta = "INSERT INTO ACC_DEPEN (ACC,EST,ACC_DEPEN,EST_DEPEN) VALUES (19301,1,19300,1)"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    'A�adir los siguientes registros en DIC
    sConsulta = "INSERT INTO [dbo].[DIC] VALUES (390,'PRESCONCEP31',15)"
    gRDOCon.Execute sConsulta, rdExecDirect
    sConsulta = "INSERT INTO [dbo].[DIC] VALUES (400,'PRESCONCEP32',15)"
    gRDOCon.Execute sConsulta, rdExecDirect
    sConsulta = "INSERT INTO [dbo].[DIC] VALUES (410,'PRESCONCEP33',15)"
    gRDOCon.Execute sConsulta, rdExecDirect
    sConsulta = "INSERT INTO [dbo].[DIC] VALUES (420,'PRESCONCEP34',15)"
    gRDOCon.Execute sConsulta, rdExecDirect
    sConsulta = "INSERT INTO [dbo].[DIC] VALUES (430,'PRESCONCEP41',15)"
    gRDOCon.Execute sConsulta, rdExecDirect
    sConsulta = "INSERT INTO [dbo].[DIC] VALUES (440,'PRESCONCEP42',15)"
    gRDOCon.Execute sConsulta, rdExecDirect
    sConsulta = "INSERT INTO [dbo].[DIC] VALUES (450,'PRESCONCEP43',15)"
    gRDOCon.Execute sConsulta, rdExecDirect
    sConsulta = "INSERT INTO [dbo].[DIC] VALUES (460,'PRESCONCEP44',15)"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    'Nueva tabla AR_ITEM_MES_PRESCON3_1
    frmProgreso.lblDetalle = "Nueva tabla AR_ITEM_MES_PRESCON3_1"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    gRDOCon.Execute CrearTablaAR_ITEM_MES_PRESCON3_1, rdExecDirect
    
    'Nueva tabla AR_ITEM_MES_PRESCON3_2
    frmProgreso.lblDetalle = "Nueva tabla AR_ITEM_MES_PRESCON3_2"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    gRDOCon.Execute CrearTablaAR_ITEM_MES_PRESCON3_2, rdExecDirect
    
    'Nueva tabla AR_ITEM_MES_PRESCON3_3
    frmProgreso.lblDetalle = "Nueva tabla AR_ITEM_MES_PRESCON3_3"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    gRDOCon.Execute CrearTablaAR_ITEM_MES_PRESCON3_3, rdExecDirect
    
    'Nueva tabla AR_ITEM_MES_PRESCON3_4
    frmProgreso.lblDetalle = "Nueva tabla AR_ITEM_MES_PRESCON3_4"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    gRDOCon.Execute CrearTablaAR_ITEM_MES_PRESCON3_4, rdExecDirect
    
    'Nueva tabla AR_ITEM_PRESCON3
    frmProgreso.lblDetalle = "Nueva tabla AR_ITEM_PRESCON3"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    gRDOCon.Execute CrearTablaAR_ITEM_PRESCON3, rdExecDirect
    
    'Nuevo store procedure SP_AR_ITEM_MES_PRESCON3_1
    frmProgreso.lblDetalle = "Creamos la store procedure SP_AR_ITEM_MES_PRESCON3_1"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[SP_AR_ITEM_MES_PRESCON3_1]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[SP_AR_ITEM_MES_PRESCON3_1]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearSP_AR_ITEM_MES_PRESCON3_1, rdExecDirect
    
    'Nuevo store procedure SP_AR_ITEM_MES_PRESCON3_2
    frmProgreso.lblDetalle = "Creamos la store procedure SP_AR_ITEM_MES_PRESCON3_2"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[SP_AR_ITEM_MES_PRESCON3_2]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[SP_AR_ITEM_MES_PRESCON3_2]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearSP_AR_ITEM_MES_PRESCON3_2, rdExecDirect
    
    'Nuevo store procedure SP_AR_ITEM_MES_PRESCON3_3
    frmProgreso.lblDetalle = "Creamos la store procedure SP_AR_ITEM_MES_PRESCON3_3"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[SP_AR_ITEM_MES_PRESCON3_3]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[SP_AR_ITEM_MES_PRESCON3_3]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearSP_AR_ITEM_MES_PRESCON3_3, rdExecDirect
    
    'Nuevo store procedure SP_AR_ITEM_MES_PRESCON3_4
    frmProgreso.lblDetalle = "Creamos la store procedure SP_AR_ITEM_MES_PRESCON3_4"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[SP_AR_ITEM_MES_PRESCON3_4]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[SP_AR_ITEM_MES_PRESCON3_4]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearSP_AR_ITEM_MES_PRESCON3_4, rdExecDirect
    
    'Nuevo store procedure SP_AR_ITEM_PRESCON3
    frmProgreso.lblDetalle = "Creamos la store procedure SP_AR_ITEM_PRESCON3"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[SP_AR_ITEM_PRESCON3]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[SP_AR_ITEM_PRESCON3]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearSP_AR_ITEM_PRESCON3, rdExecDirect
    
    'Nueva tabla AR_ITEM_MES_PRESCON4_1
    frmProgreso.lblDetalle = "Nueva tabla AR_ITEM_MES_PRESCON4_1"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    gRDOCon.Execute CrearTablaAR_ITEM_MES_PRESCON4_1, rdExecDirect
    
    'Nueva tabla AR_ITEM_MES_PRESCON4_2
    frmProgreso.lblDetalle = "Nueva tabla AR_ITEM_MES_PRESCON4_2"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    gRDOCon.Execute CrearTablaAR_ITEM_MES_PRESCON4_2, rdExecDirect
    
    'Nueva tabla AR_ITEM_MES_PRESCON4_3
    frmProgreso.lblDetalle = "Nueva tabla AR_ITEM_MES_PRESCON4_3"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    gRDOCon.Execute CrearTablaAR_ITEM_MES_PRESCON4_3, rdExecDirect
    
    'Nueva tabla AR_ITEM_MES_PRESCON4_4
    frmProgreso.lblDetalle = "Nueva tabla AR_ITEM_MES_PRESCON4_4"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    gRDOCon.Execute CrearTablaAR_ITEM_MES_PRESCON4_4, rdExecDirect
    
    'Nueva tabla AR_ITEM_PRESCON4
    frmProgreso.lblDetalle = "Nueva tabla AR_ITEM_PRESCON4"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    gRDOCon.Execute CrearTablaAR_ITEM_PRESCON4, rdExecDirect
    
    'Nuevo store procedure SP_AR_ITEM_MES_PRESCON4_1
    frmProgreso.lblDetalle = "Creamos la store procedure SP_AR_ITEM_MES_PRESCON4_1"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[SP_AR_ITEM_MES_PRESCON4_1]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[SP_AR_ITEM_MES_PRESCON4_1]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearSP_AR_ITEM_MES_PRESCON4_1, rdExecDirect
    
    'Nuevo store procedure SP_AR_ITEM_MES_PRESCON4_2
    frmProgreso.lblDetalle = "Creamos la store procedure SP_AR_ITEM_MES_PRESCON4_2"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[SP_AR_ITEM_MES_PRESCON4_2]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[SP_AR_ITEM_MES_PRESCON4_2]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearSP_AR_ITEM_MES_PRESCON4_2, rdExecDirect
    
    'Nuevo store procedure SP_AR_ITEM_MES_PRESCON4_3
    frmProgreso.lblDetalle = "Creamos la store procedure SP_AR_ITEM_MES_PRESCON4_3"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[SP_AR_ITEM_MES_PRESCON4_3]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[SP_AR_ITEM_MES_PRESCON4_3]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearSP_AR_ITEM_MES_PRESCON4_3, rdExecDirect
    
    'Nuevo store procedure SP_AR_ITEM_MES_PRESCON4_4
    frmProgreso.lblDetalle = "Creamos la store procedure SP_AR_ITEM_MES_PRESCON4_4"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[SP_AR_ITEM_MES_PRESCON4_4]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[SP_AR_ITEM_MES_PRESCON4_4]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearSP_AR_ITEM_MES_PRESCON4_4, rdExecDirect

    'Nuevo store procedure SP_AR_ITEM_PRESCON4
    frmProgreso.lblDetalle = "Creamos la store procedure SP_AR_ITEM_PRESCON4"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[SP_AR_ITEM_PRESCON4]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[SP_AR_ITEM_PRESCON4]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearSP_AR_ITEM_PRESCON4, rdExecDirect
    
    'Nuevos registros en la tabla ACC
    frmProgreso.lblDetalle = "Insertamos nuevos registros en la tabla ACC"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "INSERT INTO ACC (ID) VALUES (19400)"
    gRDOCon.Execute sConsulta, rdExecDirect
    sConsulta = "INSERT INTO ACC (ID) VALUES (19500)"
    gRDOCon.Execute sConsulta, rdExecDirect
    sConsulta = "INSERT INTO ACC (ID) VALUES (19600)"
    gRDOCon.Execute sConsulta, rdExecDirect
    sConsulta = "INSERT INTO ACC (ID) VALUES (19700)"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    'Se a�ade el campo URLPROVE a la tabla: PROVE
    frmProgreso.lblDetalle = "Nueva campo URLPROVE en PROVE"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "ALTER TABLE [dbo].[PROVE] ADD URLPROVE [varchar] (255) NULL"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    'Modificar el store procedure SP_ANYA_PROVE
    frmProgreso.lblDetalle = "Modificamos la store procedure SP_ANYA_PROVE"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[SP_ANYA_PROVE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[SP_ANYA_PROVE]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearSP_ANYA_PROVE_2_7_50, rdExecDirect
    
    'Modificar el store procedure SP_DEVOLVER_PROVE
    frmProgreso.lblDetalle = "Modificamos la store procedure SP_DEVOLVER_PROVE"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[SP_DEVOLVER_PROVE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[SP_DEVOLVER_PROVE]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearSP_DEVOLVER_PROVE_2_7_50, rdExecDirect
    
    'Nuevo campo USU en BLOQUEO_PROCE
    frmProgreso.lblDetalle = "Nueva campo USU en BLOQUEO_PROCE"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "ALTER TABLE [dbo].[BLOQUEO_PROCE] ADD USU [varchar] (" & gLongitudesDeCodigos.giLongCodUSU & ") NULL"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    'Nueva tabla GMN4_ACT
    frmProgreso.lblDetalle = "Nueva tabla GMN4_ACT"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    gRDOCon.Execute CrearTablaGMN4_ACT, rdExecDirect
    
    'Nuevo trigger en la tabla GMN4_ACT
    frmProgreso.lblDetalle = "Nueva trigger GMN4_ACT_TG_INSUPD en la tabla GMN4_ACT"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    gRDOCon.Execute CrearTriggerGMN4_ACT_TG_INSUPD, rdExecDirect
    
    'Nuevo campo CANT en ART4_ADJ
    frmProgreso.lblDetalle = "Nuevo campo CANT en ART4_ADJ"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "ALTER TABLE [dbo].[ART4_ADJ] ADD CANT [float] NULL"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    'Nuevo campo TFNO_MOVIL en la tabla CON
    frmProgreso.lblDetalle = "Nuevo campo TFNO_MOVIL en CON"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "ALTER TABLE [dbo].[CON] ADD TFNO_MOVIL [varchar] (20) NULL"
    gRDOCon.Execute sConsulta, rdExecDirect

    'Migraci�n de datos actuales: pasar todos los tel�fonos de la tabla CON
    'que empiecen por 6 al campo de tel�fono m�vil
    frmProgreso.lblDetalle = "Migraci�n de datos actuales: pasar todos los tel�fonos de la tabla CON que empiecen por 6 al campo de tel�fono m�vil"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "UPDATE CON SET TFNO=NULL,TFNO_MOVIL=TFNO WHERE TFNO LIKE '6%'"
    gRDOCon.Execute sConsulta, rdExecDirect
    sConsulta = "UPDATE CON SET TFNO2=NULL,TFNO_MOVIL=TFNO2 WHERE TFNO2 LIKE '6%'"
    gRDOCon.Execute sConsulta, rdExecDirect

    'Nuevo campo OBLPROVEMAT en PARGEN_GEST
    frmProgreso.lblDetalle = "Nuevo campo OBLPROVEMAT en PARGEN_GEST"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "ALTER TABLE [dbo].[PARGEN_GEST] ADD OBLPROVEMAT [tinyint] NOT NULL DEFAULT (0)"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    'Nuevos campos NUMOBJ y OBJNUEVOS en la tabla PROCE_PROVE
    frmProgreso.lblDetalle = "Nuevos campos NUMOBJ y OBJNUEVOS en PROCE_PROVE"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "ALTER TABLE [dbo].[PROCE_PROVE] ADD "
    sConsulta = sConsulta & vbCrLf & "NUMOBJ [int] NOT NULL DEFAULT (0),"
    sConsulta = sConsulta & vbCrLf & "OBJNUEVOS [tinyint] NOT NULL DEFAULT (0)"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    'Modificar el trigger ITEM_TG_UPD de la tabla ITEM
    frmProgreso.lblDetalle = "Modificar el trigger ITEM_TG_UPD de la tabla ITEM"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[ITEM_TG_UPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1) drop trigger [dbo].[ITEM_TG_UPD]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute ModificarTriggerITEM_TG_UPD_2_7_50, rdExecDirect
    
    'Modificar la store procedure NUM_PROCE_PUB
    frmProgreso.lblDetalle = "Modificamos la store procedure NUM_PROCE_PUB"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[NUM_PROCE_PUB]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[NUM_PROCE_PUB]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute ModificarNUM_PROCE_PUB_2_7_50, rdExecDirect
    
    'Modificar la store procedure SP_DEVOLVER_PROCESOS_PROVE_WEB
    frmProgreso.lblDetalle = "Modificamos la store procedure SP_DEVOLVER_PROCESOS_PROVE_WEB"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[SP_DEVOLVER_PROCESOS_PROVE_WEB]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[SP_DEVOLVER_PROCESOS_PROVE_WEB]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute ModificarSP_DEVOLVER_PROCESOS_PROVE_WEB_2_7_50, rdExecDirect
    
    'Nueva store procedure SP_MODIF_OBJ_PROVE_WEB
    frmProgreso.lblDetalle = "Nueva store procedure SP_MODIF_OBJ_PROVE_WEB"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[SP_MODIF_OBJ_PROVE_WEB]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[SP_MODIF_OBJ_PROVE_WEB]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearSP_MODIF_OBJ_PROVE_WEB, rdExecDirect
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE VERSION SET NUM ='2.7.50.0'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    gRDOCon.Execute "COMMIT TRANSACTION", rdExecDirect
    bTransaccionEnCurso = False

    CodigoDeActualizacion2_7_25_2A2_7_50 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
        gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_7_25_2A2_7_50 = False
End Function



Public Function CodigoDeActualizacion2_7_50A2_7_50_1() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String

    On Error GoTo error
        
    sConsulta = "BEGIN TRANSACTION"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    gRDOCon.Execute "SET XACT_ABORT OFF", rdExecDirect
    bTransaccionEnCurso = True

    'Modificamos el store procedure SP_CIERRE
    frmProgreso.lblDetalle = "Modificamos la store procedure SP_CIERRE"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[SP_CIERRE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[SP_CIERRE]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute ModificarSP_CIERRE_2_7_50, rdExecDirect
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE VERSION SET NUM ='2.7.50.1'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    gRDOCon.Execute "COMMIT TRANSACTION", rdExecDirect
    bTransaccionEnCurso = False

    CodigoDeActualizacion2_7_50A2_7_50_1 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
        gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_7_50A2_7_50_1 = False
End Function

Public Function CodigoDeActualizacion2_7_50_1A2_7_50_2() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String

    On Error GoTo error
        
    sConsulta = "BEGIN TRANSACTION"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    gRDOCon.Execute "SET XACT_ABORT OFF", rdExecDirect
    bTransaccionEnCurso = True

    'Modificamos el store procedure PROCE_TG_UPD
    frmProgreso.lblDetalle = "Corregimos el trigger PROCE_TG_UPD"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[PROCE_TG_UPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1) drop trigger [dbo].[PROCE_TG_UPD]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CorregirTriggerPROCE_TG_UPD, rdExecDirect
    
    'Modificamos el store procedure SP_ANYA_PROVE_CON
    frmProgreso.lblDetalle = "Modificamos la store procedure SP_ANYA_PROVE_CON"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[SP_ANYA_PROVE_CON]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[SP_ANYA_PROVE_CON]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute ModificarSP_ANYA_PROVE_CON_2_7_50, rdExecDirect
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE VERSION SET NUM ='2.7.50.2'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    gRDOCon.Execute "COMMIT TRANSACTION", rdExecDirect
    bTransaccionEnCurso = False

    CodigoDeActualizacion2_7_50_1A2_7_50_2 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
        gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_7_50_1A2_7_50_2 = False
End Function
Public Function CodigoDeActualizacion2_7_50_2A2_7_50_3() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String

    On Error GoTo error
        
    sConsulta = "BEGIN TRANSACTION"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    gRDOCon.Execute "SET XACT_ABORT OFF", rdExecDirect
    bTransaccionEnCurso = True

    'Creamos el trigger ITEM_OFE_TG_DEL
    frmProgreso.lblDetalle = "Creamos el trigger ITEM_OFE_TG_DEL"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[ITEM_OFE_TG_DEL]') and OBJECTPROPERTY(id, N'IsTrigger') = 1) drop trigger [dbo].[ITEM_OFE_TG_DEL]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearTriggerITEM_OFE_TG_DEL, rdExecDirect
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE VERSION SET NUM ='2.7.50.3'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    gRDOCon.Execute "COMMIT TRANSACTION", rdExecDirect
    bTransaccionEnCurso = False

    CodigoDeActualizacion2_7_50_2A2_7_50_3 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
        gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_7_50_2A2_7_50_3 = False
End Function

Public Function CodigoDeActualizacion2_7_50_3A2_7_50_4() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String

    On Error GoTo error
        
    sConsulta = "BEGIN TRANSACTION"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    gRDOCon.Execute "SET XACT_ABORT OFF", rdExecDirect
    bTransaccionEnCurso = True

    'Creamos la tabla WEBTEXTHELP
    frmProgreso.lblDetalle = "Creamos la tabla WEBTEXTHELP"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[WEBTEXTHELP]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)"
    sConsulta = sConsulta & " drop table [dbo].[WEBTEXTHELP]"
    gRDOCon.Execute sConsulta
    gRDOCon.Execute CrearTablaWEBTEXTHELP, rdExecDirect
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE VERSION SET NUM ='2.7.50.4'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    gRDOCon.Execute "COMMIT TRANSACTION", rdExecDirect
    bTransaccionEnCurso = False

    CodigoDeActualizacion2_7_50_3A2_7_50_4 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
        gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_7_50_3A2_7_50_4 = False
End Function


Public Function CodigoDeActualizacion2_7_50_4A2_7_50_5() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String

    On Error GoTo error
        
    sConsulta = "BEGIN TRANSACTION"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    gRDOCon.Execute "SET XACT_ABORT OFF", rdExecDirect
    bTransaccionEnCurso = True

    'Modificamos el trigger ITEM_OFE_TG_UPD
    frmProgreso.lblDetalle = "Modificamos el trigger ITEM_OFE_TG_UPD"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[ITEM_OFE_TG_UPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1) drop trigger [dbo].[ITEM_OFE_TG_UPD]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute ModificarITEM_OFE_TG_UPD_2_7_50_5, rdExecDirect
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE VERSION SET NUM ='2.7.50.5'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    gRDOCon.Execute "COMMIT TRANSACTION", rdExecDirect
    bTransaccionEnCurso = False

    CodigoDeActualizacion2_7_50_4A2_7_50_5 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
        gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_7_50_4A2_7_50_5 = False
End Function



Public Function CodigoDeActualizacion2_7_50_5A2_7_50_6() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String

    On Error GoTo error
        
    sConsulta = "BEGIN TRANSACTION"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    gRDOCon.Execute "SET XACT_ABORT OFF", rdExecDirect
    bTransaccionEnCurso = True

    'Eliminamos el trigger ITEM_OFE_TG_DEL
    frmProgreso.lblDetalle = "Eliminamos el trigger ITEM_OFE_TG_DEL"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[ITEM_OFE_TG_DEL]') and OBJECTPROPERTY(id, N'IsTrigger') = 1) drop trigger [dbo].[ITEM_OFE_TG_DEL]"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    'Modificar el trigger ITEM_TG_DEL de la delete de ITEM
    frmProgreso.lblDetalle = "Modificamos el trigger ITEM_TG_DEL"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[ITEM_TG_DEL]') and OBJECTPROPERTY(id, N'IsTrigger') = 1) drop trigger [dbo].[ITEM_TG_DEL]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute ModificarITEM_TG_DEL_2_7_50_6, rdExecDirect
    
    'Modificar el trigger ITEM_OFE_TG_UPD de la update en ITEM_OFE
    frmProgreso.lblDetalle = "Modificamos el trigger ITEM_OFE_TG_UPD"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[ITEM_OFE_TG_UPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1) drop trigger [dbo].[ITEM_OFE_TG_UPD]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute ModificarITEM_OFE_TG_UPD_2_7_50_6, rdExecDirect
    
    'Modificar el trigger PROCE_TG_UPD de la update en PROCE
    frmProgreso.lblDetalle = "Modificamos el trigger PROCE_TG_UPD"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[PROCE_TG_UPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1) drop trigger [dbo].[PROCE_TG_UPD]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute ModificarPROCE_TG_UPD_2_7_50_6, rdExecDirect
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE VERSION SET NUM ='2.7.50.6'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    gRDOCon.Execute "COMMIT TRANSACTION", rdExecDirect
    bTransaccionEnCurso = False

    CodigoDeActualizacion2_7_50_5A2_7_50_6 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
        gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_7_50_5A2_7_50_6 = False
End Function

Public Function CodigoDeActualizacion2_7_50_6A2_7_50_7() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String

    On Error GoTo error
        
    sConsulta = "BEGIN TRANSACTION"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    gRDOCon.Execute "SET XACT_ABORT OFF", rdExecDirect
    bTransaccionEnCurso = True

    
    'Modificamos el store procedure PRES_3_CON2_COD
    frmProgreso.lblDetalle = "Modificamos el store procedure PRES_3_CON2_COD"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[PRES_3_CON2_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[PRES_3_CON2_COD]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute ModificarPRES_3_CON2_COD_2_7_50_7, rdExecDirect
    
    'Modificamos el store procedure PRES_3_CON3_COD
    frmProgreso.lblDetalle = "Modificamos el store procedure PRES_3_CON3_COD"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[PRES_3_CON3_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[PRES_3_CON3_COD]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute ModificarPRES_3_CON3_COD_2_7_50_7, rdExecDirect
    
    'Modificamos el store procedure PRES_3_CON4_COD
    frmProgreso.lblDetalle = "Modificamos el store procedure PRES_3_CON4_COD"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[PRES_3_CON4_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[PRES_3_CON4_COD]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute ModificarPRES_3_CON4_COD_2_7_50_7, rdExecDirect
    
    
    'Modificamos el store procedure PRES_4_CON2_COD
    frmProgreso.lblDetalle = "Modificamos el store procedure PRES_4_CON2_COD"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[PRES_4_CON2_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[PRES_4_CON2_COD]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute ModificarPRES_4_CON2_COD_2_7_50_7, rdExecDirect
    
    'Modificamos el store procedure PRES_4_CON3_COD
    frmProgreso.lblDetalle = "Modificamos el store procedure PRES_4_CON3_COD"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[PRES_4_CON3_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[PRES_4_CON3_COD]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute ModificarPRES_4_CON3_COD_2_7_50_7, rdExecDirect
    
    'Modificamos el store procedure PRES_4_CON4_COD
    frmProgreso.lblDetalle = "Modificamos el store procedure PRES_4_CON4_COD"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[PRES_4_CON4_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[PRES_4_CON4_COD]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute ModificarPRES_4_CON4_COD_2_7_50_7, rdExecDirect
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE VERSION SET NUM ='2.7.50.7'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    gRDOCon.Execute "COMMIT TRANSACTION", rdExecDirect
    bTransaccionEnCurso = False

    CodigoDeActualizacion2_7_50_6A2_7_50_7 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
        gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_7_50_6A2_7_50_7 = False
End Function

Public Function CodigoDeActualizacion2_7_50_7A2_7_50_8() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String

    On Error GoTo error
        
    sConsulta = "BEGIN TRANSACTION"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    gRDOCon.Execute "SET XACT_ABORT OFF", rdExecDirect
    bTransaccionEnCurso = True

    'Borramos todos los registros de PROVE_ART_4 que no est�n en PROVE
    sConsulta = "DELETE FROM PROVE_ART4 WHERE PROVE NOT IN (SELECT COD FROM PROVE)"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    'Creamos la foreign key de PROVE_ART4 con PROVE
    sConsulta = "ALTER TABLE [dbo].[PROVE_ART4] ADD "
    sConsulta = sConsulta & vbLf & "CONSTRAINT [FK_PROVE_ART4_PROVE] FOREIGN KEY "
    sConsulta = sConsulta & vbLf & "("
    sConsulta = sConsulta & vbLf & "        [PROVE]"
    sConsulta = sConsulta & vbLf & "    ) REFERENCES [dbo].[PROVE]  ("
    sConsulta = sConsulta & vbLf & "        [COD]"
    sConsulta = sConsulta & vbLf & "    )"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    'Modificamos el store procedure PROVE_COD
    frmProgreso.lblDetalle = "Modificamos el store procedure PROVE_COD"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[PROVE_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[PROVE_COD]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute ModificarPROVE_COD_2_7_50_8, rdExecDirect
    
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE VERSION SET NUM ='2.7.50.8'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    gRDOCon.Execute "COMMIT TRANSACTION", rdExecDirect
    bTransaccionEnCurso = False

    CodigoDeActualizacion2_7_50_7A2_7_50_8 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
        gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_7_50_7A2_7_50_8 = False
End Function


Public Function CodigoDeActualizacion2_7_50_8A2_7_50_9() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String

    On Error GoTo error
        
    sConsulta = "BEGIN TRANSACTION"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    gRDOCon.Execute "SET XACT_ABORT OFF", rdExecDirect
    bTransaccionEnCurso = True

    
    'Modificamos el store procedure PROVE_COD
    frmProgreso.lblDetalle = "Modificamos el store procedure DEST_COD"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[DEST_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[DEST_COD]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute ModificarDEST_COD_2_8_50_9, rdExecDirect
    
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE VERSION SET NUM ='2.7.50.9'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    gRDOCon.Execute "COMMIT TRANSACTION", rdExecDirect
    bTransaccionEnCurso = False

    CodigoDeActualizacion2_7_50_8A2_7_50_9 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
        gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_7_50_8A2_7_50_9 = False
End Function


Public Function CodigoDeActualizacion2_7_50_9A2_7_51_0() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim sConsulta As String

    On Error GoTo error
        
    sConsulta = "BEGIN TRANSACTION"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    gRDOCon.Execute "SET XACT_ABORT OFF", rdExecDirect
    bTransaccionEnCurso = True

    'Modificar la SP PER_COD para que actualice la tabla FIJ
    frmProgreso.lblDetalle = "Modificamos el store procedure PER_COD"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[PER_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[PER_COD]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute ModificarPER_COD_2_7_51_0, rdExecDirect

    'A�adimos los campos NEP3 y NEP4 a la tabla PARGEN_INTERNO, tinyint 1, Default Value 4
    frmProgreso.lblDetalle = "A�adimos los campos NEP3 y NEP4 a la tabla PARGEN_INTERNO"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "ALTER TABLE dbo.[PARGEN_INTERNO] ADD NEP3 [tinyint] NOT NULL DEFAULT (4),"
    sConsulta = sConsulta & vbCrLf & " NEP4 [tinyint] NOT NULL DEFAULT (4)"
    gRDOCon.Execute sConsulta, rdExecDirect

    'A�adimos los campos OBLPRES3 y OBLPRES4 a la tabla PARGEN_GEST , tinyint 1, Default Value 0
    frmProgreso.lblDetalle = "A�adimos los campos OBLPRES3 y OBLPRES4 a la tabla PARGEN_GEST"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "ALTER TABLE dbo.[PARGEN_GEST] ADD OBLPres3 [tinyint] NOT NULL DEFAULT (0),"
    sConsulta = sConsulta & vbCrLf & " OBLPres4 [tinyint] NOT NULL DEFAULT (0)"
    gRDOCon.Execute sConsulta, rdExecDirect

    'A�adir el campo PROVEACT a la tabla ITEM
    frmProgreso.lblDetalle = "A�adimos el campo PROVEACT a la tabla ITEM"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "ALTER TABLE dbo.[ITEM] ADD PROVEACT [varchar](" & gLongitudesDeCodigos.giLongCodPROVE & ") NULL"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    'Todos los procesos de PROCE validados en apertura comprobar si existen en BLOQUEO_PROCE
    'y si no existen insertarlos
    frmProgreso.lblDetalle = "Actualizamos la tabla BLOQUEO_PROCE"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    If Not ActualizarBLOQUEO_PROCE_2_7_51_0 Then
        If bTransaccionEnCurso Then
            gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
        End If
        CodigoDeActualizacion2_7_50_9A2_7_51_0 = False
        Exit Function
    End If


    'Actualizamos el store procedure NUM_PROCE_PUB
    frmProgreso.lblDetalle = "Modificamos el store procedure NUM_PROCE_PUB"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[NUM_PROCE_PUB]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[NUM_PROCE_PUB]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute ModificarNUM_PROCE_PUB_2_7_51_0, rdExecDirect
    
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE VERSION SET NUM ='2.7.51.0'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    gRDOCon.Execute "COMMIT TRANSACTION", rdExecDirect
    bTransaccionEnCurso = False

    CodigoDeActualizacion2_7_50_9A2_7_51_0 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
        gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_7_50_9A2_7_51_0 = False
End Function


Private Function CrearNuevoGMN1_COD() As String
Dim sConsulta As String

sConsulta = "CREATE PROCEDURE GMN1_COD (@OLD VARCHAR(50),@NEW VARCHAR(50))  AS "
 sConsulta = sConsulta & vbCrLf & "DECLARE @VAR_COD VARCHAR(50) "
 sConsulta = sConsulta & vbCrLf & "SET XACT_ABORT ON "
 sConsulta = sConsulta & vbCrLf & "BEGIN TRANSACTION "
 sConsulta = sConsulta & vbCrLf & "BEGIN "
 sConsulta = sConsulta & vbCrLf & "DECLARE C CURSOR FOR SELECT COD FROM GMN1 WHERE COD=@OLD "
 sConsulta = sConsulta & vbCrLf & "OPEN C "
 sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM C INTO @VAR_COD "
 sConsulta = sConsulta & vbCrLf & "IF @@FETCH_STATUS=0 "
 sConsulta = sConsulta & vbCrLf & "BEGIN "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN1 NOCHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART1 NOCHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART1_ADJ NOCHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART2 NOCHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART2_ADJ NOCHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART3 NOCHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART3_ADJ NOCHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4 NOCHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4_ADJ NOCHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE BLOQUEO_PROCE NOCHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE COM_GMN1 NOCHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE COM_GMN2 NOCHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE COM_GMN3 NOCHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE COM_GMN4 NOCHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN2 NOCHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN3 NOCHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN4 NOCHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM NOCHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_ADJ NOCHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_ADJREU NOCHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_ESP NOCHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_OBJ NOCHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_OFE NOCHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_OFE_WEB NOCHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRES NOCHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_UON1 NOCHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_UON2 NOCHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_UON3 NOCHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON1 NOCHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON2 NOCHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON3 NOCHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON4 NOCHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY1 NOCHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY2 NOCHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY3 NOCHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY4 NOCHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART1 NOCHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART2 NOCHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART3 NOCHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART4 NOCHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON NOCHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESPROY NOCHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_PROVE NOCHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_MAT1 NOCHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_MAT2 NOCHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_MAT3 NOCHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_MAT4 NOCHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE NOCHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_ESP NOCHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_OFE_ADJUN NOCHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_OFE NOCHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_OFE_WEB_ADJUN NOCHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_OFE_WEB NOCHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_PER NOCHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_PROVE NOCHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_PROVE_PET NOCHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_PROVE_POT NOCHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN1 NOCHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN2 NOCHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN3 NOCHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN4 NOCHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE REU_PROCE NOCHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_PROCE NOCHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM NOCHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES NOCHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON1 NOCHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON2 NOCHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON3 NOCHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "UPDATE GMN1 SET COD=@NEW WHERE COD=@OLD "
 sConsulta = sConsulta & vbCrLf & "UPDATE ART1 SET GMN1=@NEW WHERE GMN1=@OLD "
 sConsulta = sConsulta & vbCrLf & "UPDATE ART1_ADJ SET GMN1=@NEW WHERE GMN1=@OLD "
 sConsulta = sConsulta & vbCrLf & "UPDATE ART2 SET GMN1=@NEW WHERE GMN1=@OLD "
 sConsulta = sConsulta & vbCrLf & "UPDATE ART2_ADJ SET GMN1=@NEW WHERE GMN1=@OLD "
 sConsulta = sConsulta & vbCrLf & "UPDATE ART3 SET GMN1=@NEW WHERE GMN1=@OLD "
 sConsulta = sConsulta & vbCrLf & "UPDATE ART3_ADJ SET GMN1=@NEW WHERE GMN1=@OLD "
 sConsulta = sConsulta & vbCrLf & "UPDATE ART4 SET GMN1=@NEW WHERE GMN1=@OLD "
 sConsulta = sConsulta & vbCrLf & "UPDATE ART4_ADJ SET GMN1=@NEW WHERE GMN1=@OLD "
 sConsulta = sConsulta & vbCrLf & "UPDATE BLOQUEO_PROCE SET GMN1=@NEW WHERE GMN1=@OLD "
 sConsulta = sConsulta & vbCrLf & "UPDATE COM_GMN1 SET GMN1=@NEW WHERE GMN1=@OLD "
 sConsulta = sConsulta & vbCrLf & "UPDATE COM_GMN2 SET GMN1=@NEW WHERE GMN1=@OLD "
 sConsulta = sConsulta & vbCrLf & "UPDATE COM_GMN3 SET GMN1=@NEW WHERE GMN1=@OLD "
 sConsulta = sConsulta & vbCrLf & "UPDATE COM_GMN4 SET GMN1=@NEW WHERE GMN1=@OLD "
 sConsulta = sConsulta & vbCrLf & "UPDATE GMN2 SET GMN1=@NEW WHERE GMN1=@OLD "
 sConsulta = sConsulta & vbCrLf & "UPDATE GMN3 SET GMN1=@NEW WHERE GMN1=@OLD "
 sConsulta = sConsulta & vbCrLf & "UPDATE GMN4 SET GMN1=@NEW WHERE GMN1=@OLD "
 sConsulta = sConsulta & vbCrLf & "UPDATE ITEM SET GMN1=@NEW WHERE GMN1=@OLD "
 sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_ADJ SET GMN1=@NEW WHERE GMN1=@OLD "
 sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_ADJREU SET GMN1=@NEW WHERE GMN1=@OLD "
 sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_ESP SET GMN1=@NEW WHERE GMN1=@OLD "
 sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_OBJ SET GMN1=@NEW WHERE GMN1=@OLD "
 sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_OFE SET GMN1=@NEW WHERE GMN1=@OLD "
 sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_OFE_WEB SET GMN1=@NEW WHERE GMN1=@OLD "
 sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_PRES SET GMN1=@NEW WHERE GMN1=@OLD "
 sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_UON1 SET GMN1=@NEW WHERE GMN1=@OLD "
 sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_UON2 SET GMN1=@NEW WHERE GMN1=@OLD "
 sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_UON3 SET GMN1=@NEW WHERE GMN1=@OLD "
 sConsulta = sConsulta & vbCrLf & "UPDATE PRES_MAT1 SET GMN1=@NEW WHERE GMN1=@OLD "
 sConsulta = sConsulta & vbCrLf & "UPDATE PRES_MAT2 SET GMN1=@NEW WHERE GMN1=@OLD "
 sConsulta = sConsulta & vbCrLf & "UPDATE PRES_MAT3 SET GMN1=@NEW WHERE GMN1=@OLD "
 sConsulta = sConsulta & vbCrLf & "UPDATE PRES_MAT4 SET GMN1=@NEW WHERE GMN1=@OLD "
 sConsulta = sConsulta & vbCrLf & "UPDATE PROCE SET GMN1=@NEW WHERE GMN1=@OLD "
 sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_ESP SET GMN1=@NEW WHERE GMN1=@OLD "
 sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_OFE_ADJUN SET GMN1=@NEW WHERE GMN1=@OLD "
 sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_OFE SET GMN1=@NEW WHERE GMN1=@OLD "
 sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_OFE_WEB_ADJUN SET GMN1=@NEW WHERE GMN1=@OLD "
 sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_OFE_WEB SET GMN1=@NEW WHERE GMN1=@OLD "
 sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_PER SET GMN1=@NEW WHERE GMN1=@OLD "
 sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_PROVE SET GMN1=@NEW WHERE GMN1=@OLD "
 sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_PROVE_PET SET GMN1=@NEW WHERE GMN1=@OLD "
 sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_PROVE_POT SET GMN1=@NEW WHERE GMN1=@OLD "
 sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_GMN1 SET GMN1=@NEW WHERE GMN1=@OLD "
 sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_GMN2 SET GMN1=@NEW WHERE GMN1=@OLD "
 sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_GMN3 SET GMN1=@NEW WHERE GMN1=@OLD "
 sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_GMN4 SET GMN1=@NEW WHERE GMN1=@OLD "
 sConsulta = sConsulta & vbCrLf & "UPDATE REU_PROCE SET GMN1=@NEW WHERE GMN1=@OLD "
 sConsulta = sConsulta & vbCrLf & "UPDATE AR_PROCE SET GMN1=@NEW WHERE GMN1=@OLD "
 sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM SET GMN1=@NEW WHERE GMN1=@OLD "
 sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES SET GMN1=@NEW WHERE GMN1=@OLD "
 sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_UON1 SET GMN1=@NEW WHERE GMN1=@OLD "
 sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_UON2 SET GMN1=@NEW WHERE GMN1=@OLD "
 sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_UON3 SET GMN1=@NEW WHERE GMN1=@OLD "
 sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON1 SET GMN1=@NEW WHERE GMN1=@OLD "
 sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON2 SET GMN1=@NEW WHERE GMN1=@OLD "
 sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON3 SET GMN1=@NEW WHERE GMN1=@OLD "
 sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON4 SET GMN1=@NEW WHERE GMN1=@OLD "
 sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY1 SET GMN1=@NEW WHERE GMN1=@OLD "
 sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY2 SET GMN1=@NEW WHERE GMN1=@OLD "
 sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY3 SET GMN1=@NEW WHERE GMN1=@OLD "
 sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY4 SET GMN1=@NEW WHERE GMN1=@OLD "
 sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_PROVE SET GMN1=@NEW WHERE GMN1=@OLD "
 sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_PRESCON SET GMN1=@NEW WHERE GMN1=@OLD "
 sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_PRESPROY SET GMN1=@NEW WHERE GMN1=@OLD "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN1 CHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART1 CHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART1_ADJ CHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART2 CHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART2_ADJ CHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART3 CHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART3_ADJ CHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4 CHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4_ADJ CHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE BLOQUEO_PROCE CHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE COM_GMN1 CHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE COM_GMN2 CHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE COM_GMN3 CHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE COM_GMN4 CHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN2 CHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN3 CHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN4 CHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM CHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_ADJ CHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_ADJREU CHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_ESP CHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_OBJ CHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_OFE CHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_OFE_WEB CHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRES CHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_UON1 CHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_UON2 CHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_UON3 CHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_MAT1 CHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_MAT2 CHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_MAT3 CHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_MAT4 CHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE CHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_ESP CHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_OFE_ADJUN CHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_OFE CHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_OFE_WEB_ADJUN CHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_OFE_WEB CHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_PER CHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_PROVE CHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_PROVE_PET CHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_PROVE_POT CHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN1 CHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN2 CHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN3 CHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN4 CHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE REU_PROCE CHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_PROCE CHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM CHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES CHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON1 CHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON2 CHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON3 CHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON1 CHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON2 CHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON3 CHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON4 CHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY1 CHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY2 CHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY3 CHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY4 CHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART1 CHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART2 CHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART3 CHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART4 CHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON CHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESPROY CHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_PROVE CHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "End "
 sConsulta = sConsulta & vbCrLf & "Close C "
 sConsulta = sConsulta & vbCrLf & "DEALLOCATE C "
 sConsulta = sConsulta & vbCrLf & "End "
 sConsulta = sConsulta & vbCrLf & "COMMIT TRANSACTION "
CrearNuevoGMN1_COD = sConsulta
End Function

Private Function CrearNuevoPROVE_COD() As String
Dim sConsulta As String
 sConsulta = "CREATE PROCEDURE PROVE_COD (@OLD VARCHAR(50),@NEW VARCHAR(50))  AS "
 sConsulta = sConsulta & vbCrLf & "DECLARE @VAR_COD VARCHAR(50) "
 sConsulta = sConsulta & vbCrLf & "SET XACT_ABORT ON "
 sConsulta = sConsulta & vbCrLf & "BEGIN TRANSACTION "
 sConsulta = sConsulta & vbCrLf & "BEGIN "
 sConsulta = sConsulta & vbCrLf & "DECLARE C CURSOR FOR SELECT COD FROM PROVE WHERE COD=@OLD "
 sConsulta = sConsulta & vbCrLf & "OPEN C "
 sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM C INTO @VAR_COD "
 sConsulta = sConsulta & vbCrLf & "IF @@FETCH_STATUS=0 "
 sConsulta = sConsulta & vbCrLf & "BEGIN "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE NOCHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE CON NOCHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART1_ADJ NOCHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART2_ADJ NOCHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART3_ADJ NOCHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4_ADJ NOCHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_ADJ NOCHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_ADJREU NOCHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_OFE NOCHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_OFE_WEB NOCHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_OFE_ADJUN NOCHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_OFE NOCHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_OFE_WEB_ADJUN NOCHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_OFE_WEB NOCHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_PROVE NOCHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_PROVE_PET NOCHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_PROVE_POT NOCHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_EQP NOCHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN1 NOCHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN2 NOCHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN3 NOCHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN4 NOCHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE USU NOCHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_PROVE NOCHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "UPDATE PROVE SET COD=@NEW WHERE COD=@OLD "
 sConsulta = sConsulta & vbCrLf & "UPDATE ART1_ADJ SET CODPROVE=@NEW WHERE CODPROVE=@OLD "
 sConsulta = sConsulta & vbCrLf & "UPDATE ART2_ADJ SET CODPROVE=@NEW WHERE CODPROVE=@OLD "
 sConsulta = sConsulta & vbCrLf & "UPDATE ART3_ADJ SET CODPROVE=@NEW WHERE CODPROVE=@OLD "
 sConsulta = sConsulta & vbCrLf & "UPDATE ART4_ADJ SET CODPROVE=@NEW WHERE CODPROVE=@OLD "
 sConsulta = sConsulta & vbCrLf & "UPDATE CON SET PROVE=@NEW WHERE PROVE=@OLD "
 sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_ADJ SET PROVE=@NEW WHERE PROVE=@OLD "
 sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_ADJREU SET PROVE=@NEW WHERE PROVE=@OLD "
 sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_OFE SET PROVE=@NEW WHERE PROVE=@OLD "
 sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_OFE_WEB SET PROVE=@NEW WHERE PROVE=@OLD "
 sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_OFE_ADJUN SET PROVE=@NEW WHERE PROVE=@OLD "
 sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_OFE SET PROVE=@NEW WHERE PROVE=@OLD "
 sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_OFE_WEB_ADJUN SET PROVE=@NEW WHERE PROVE=@OLD "
 sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_OFE_WEB SET PROVE=@NEW WHERE PROVE=@OLD "
 sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_PROVE SET PROVE=@NEW WHERE PROVE=@OLD "
 sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_PROVE_PET SET PROVE=@NEW WHERE PROVE=@OLD "
 sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_PROVE_POT SET PROVE=@NEW WHERE PROVE=@OLD "
 sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_EQP SET PROVE=@NEW WHERE PROVE=@OLD "
 sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_GMN1 SET PROVE=@NEW WHERE PROVE=@OLD "
 sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_GMN2 SET PROVE=@NEW WHERE PROVE=@OLD "
 sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_GMN3 SET PROVE=@NEW WHERE PROVE=@OLD "
 sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_GMN4 SET PROVE=@NEW WHERE PROVE=@OLD "
 sConsulta = sConsulta & vbCrLf & "UPDATE USU SET PROVE=@NEW WHERE PROVE=@OLD "
 sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_PROVE SET PROVE=@NEW WHERE PROVE=@OLD "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_PROVE CHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE CHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE CON CHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART1_ADJ CHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART2_ADJ CHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART3_ADJ CHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4_ADJ CHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_ADJ CHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_ADJREU CHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_OFE CHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_OFE_WEB CHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_OFE_ADJUN CHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_OFE CHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_OFE_WEB_ADJUN CHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_OFE_WEB CHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_PROVE CHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_PROVE_PET CHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_PROVE_POT CHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_EQP CHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN1 CHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN2 CHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN3 CHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN4 CHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "ALTER TABLE USU CHECK CONSTRAINT ALL "
 sConsulta = sConsulta & vbCrLf & "End "
 sConsulta = sConsulta & vbCrLf & "Close C "
 sConsulta = sConsulta & vbCrLf & "DEALLOCATE C "
 sConsulta = sConsulta & vbCrLf & "End "
 sConsulta = sConsulta & vbCrLf & "COMMIT TRANSACTION "
CrearNuevoPROVE_COD = sConsulta
End Function


Private Function CrearNuevoTriggerPais() As String

sConsulta = "CREATE TRIGGER PAI_TG_INSUPD ON dbo.PAI"
sConsulta = sConsulta & vbCrLf & "FOR INSERT, UPDATE"
sConsulta = sConsulta & vbCrLf & "AS"
sConsulta = sConsulta & vbCrLf & "DECLARE @COD VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "DECLARE @FSP_COD VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "DECLARE curTG_Pai_Ins CURSOR FOR SELECT COD,FSP_COD FROM INSERTED"
sConsulta = sConsulta & vbCrLf & "OPEN curTG_Pai_Ins"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM curTG_Pai_Ins INTO @COD,@FSP_COD"
sConsulta = sConsulta & vbCrLf & "WHILE @@FETCH_STATUS=0"
sConsulta = sConsulta & vbCrLf & "begin"
sConsulta = sConsulta & vbCrLf & "if update(fsp_cod)"
sConsulta = sConsulta & vbCrLf & "    begin"
sConsulta = sConsulta & vbCrLf & "    IF @FSP_COD IS NOT NULL"
sConsulta = sConsulta & vbCrLf & "        if (select count(*) from pai where fsp_Cod=@FSP_COD)>1"
sConsulta = sConsulta & vbCrLf & "            begin"
sConsulta = sConsulta & vbCrLf & "            RAISERROR ('C�digo del pais en el portal ya esta asignado a otro pais en esta compa��a.',16,1)"
sConsulta = sConsulta & vbCrLf & "            ROLLBACK TRANSACTION"
sConsulta = sConsulta & vbCrLf & "            BREAK"
sConsulta = sConsulta & vbCrLf & "            End"
sConsulta = sConsulta & vbCrLf & "    End"
sConsulta = sConsulta & vbCrLf & "UPDATE PAI SET FECACT=GETDATE() WHERE COD=@COD"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM curTG_Pai_Ins INTO @COD, @FSP_COD"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "Close curTG_Pai_Ins"
sConsulta = sConsulta & vbCrLf & "DEALLOCATE curTG_Pai_Ins"

CrearNuevoTriggerPais = sConsulta
End Function

Private Function CrearNuevoSP_DEVOLVER_ITEM_ESP_PORTAL() As String
Dim sConsulta As String


sConsulta = "CREATE PROCEDURE SP_DEVOLVER_ITEM_ESP_PORTAL @ANYO smallint, @GMN1 varchar(50), @PROCE smallint , @item smallint AS "
sConsulta = sConsulta & vbCrLf & "SELECT NOM,COM,ID,FECACT FROM  ITEM_ESP WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM "


CrearNuevoSP_DEVOLVER_ITEM_ESP_PORTAL = sConsulta

End Function

Private Function CrearNuevoSP_DEVOLVER_PROCE_ESP_PORTAL() As String
Dim sConsulta As String

sConsulta = "CREATE PROCEDURE SP_DEVOLVER_PROCE_ESP_PORTAL @ANYO smallint, @GMN1 varchar(50), @PROCE smallint AS "

sConsulta = sConsulta & vbCrLf & "SELECT NOM,COM,ID,FECACT FROM  PROCE_ESP WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE"

CrearNuevoSP_DEVOLVER_PROCE_ESP_PORTAL = sConsulta
End Function


Private Function CrearNuevoSP_DOWNLOAD_PROCE_ESP() As String
Dim sConsulta As String

sConsulta = "CREATE PROCEDURE SP_DOWNLOAD_PROCE_ESP @INIT INT, @OFFSET INT,@ANYO INT,@GMN1 VARCHAR(50),@PROCE INT,@ID INT  AS "
sConsulta = sConsulta & vbCrLf & " "
sConsulta = sConsulta & vbCrLf & "DECLARE @textpointer varbinary(16)  "
sConsulta = sConsulta & vbCrLf & "DECLARE @size int "
sConsulta = sConsulta & vbCrLf & "DECLARE @LOFFSET int "
sConsulta = sConsulta & vbCrLf & "DECLARE @ANTTEXTSIZE INT "
sConsulta = sConsulta & vbCrLf & " "
sConsulta = sConsulta & vbCrLf & "SELECT @ANTTEXTSIZE=@@TEXTSIZE "
sConsulta = sConsulta & vbCrLf & "SET nocount on "
sConsulta = sConsulta & vbCrLf & " "
sConsulta = sConsulta & vbCrLf & "SELECT @size=datalength(data), @textpointer = TEXTPTR(data)  FROM PROCE_ESP WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ID=@ID  "
sConsulta = sConsulta & vbCrLf & " "
sConsulta = sConsulta & vbCrLf & "if @INIT + @OFFSET > @SIZE "
sConsulta = sConsulta & vbCrLf & "    SET @LOFFSET = @SIZE-@INIT "
sConsulta = sConsulta & vbCrLf & "ELSE "
sConsulta = sConsulta & vbCrLf & "    BEGIN "
sConsulta = sConsulta & vbCrLf & "    SET @LOFFSET = @OFFSET +1 "
sConsulta = sConsulta & vbCrLf & "    END "
sConsulta = sConsulta & vbCrLf & " "
sConsulta = sConsulta & vbCrLf & " "
sConsulta = sConsulta & vbCrLf & "READTEXT proce_esp.data @textpointer @INIT  @LOFFSET "


CrearNuevoSP_DOWNLOAD_PROCE_ESP = sConsulta
End Function

Private Function CrearNuevoSP_DOWNLOAD_PROCE_ITEM_ESP() As String
Dim sConsulta As String

sConsulta = "CREATE PROCEDURE SP_DOWNLOAD_PROCE_ITEM_ESP @INIT INT, @OFFSET INT,@ANYO INT,@GMN1 VARCHAR(50),@PROCE INT,@ITEM int,@ID INT  AS "
sConsulta = sConsulta & vbCrLf & " "
sConsulta = sConsulta & vbCrLf & "DECLARE @textpointer varbinary(16)  "
sConsulta = sConsulta & vbCrLf & "DECLARE @size int "
sConsulta = sConsulta & vbCrLf & "DECLARE @LOFFSET int "
sConsulta = sConsulta & vbCrLf & "DECLARE @ANTTEXTSIZE INT "
sConsulta = sConsulta & vbCrLf & " "
sConsulta = sConsulta & vbCrLf & "SELECT @ANTTEXTSIZE=@@TEXTSIZE "
sConsulta = sConsulta & vbCrLf & "SET nocount on "
sConsulta = sConsulta & vbCrLf & " "
sConsulta = sConsulta & vbCrLf & "SELECT @size=datalength(data), @textpointer = TEXTPTR(data)  FROM ITEM_ESP WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM AND ID=@ID  "
sConsulta = sConsulta & vbCrLf & " "
sConsulta = sConsulta & vbCrLf & "if @INIT + @OFFSET > @SIZE "
sConsulta = sConsulta & vbCrLf & "    SET @LOFFSET = @SIZE-@INIT "
sConsulta = sConsulta & vbCrLf & "ELSE "
sConsulta = sConsulta & vbCrLf & "    BEGIN "
sConsulta = sConsulta & vbCrLf & "    SET @LOFFSET = @OFFSET +1 "
sConsulta = sConsulta & vbCrLf & "    END "
sConsulta = sConsulta & vbCrLf & " "
sConsulta = sConsulta & vbCrLf & " "
sConsulta = sConsulta & vbCrLf & "READTEXT item_esp.data @textpointer @INIT  @LOFFSET "
CrearNuevoSP_DOWNLOAD_PROCE_ITEM_ESP = sConsulta
End Function

Private Function CrearNuevoSP_ESP_ITEM2() As String
Dim sConsulta As String

sConsulta = "CREATE PROCEDURE  SP_ESP_ITEM2  (@ANYO INT,@GMN1 VARCHAR(50),@PROCE INT,@ITEM INT,@ID INT)  AS "
sConsulta = sConsulta & vbCrLf & " BEGIN "

sConsulta = sConsulta & vbCrLf & " select NOM, COM, FECACT,datalength(data) msize  FROM ITEM_ESP WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM AND ID=@ID "
sConsulta = sConsulta & vbCrLf & " Return "
sConsulta = sConsulta & vbCrLf & " End "


CrearNuevoSP_ESP_ITEM2 = sConsulta
End Function

Private Function CrearNuevoSP_ESP_PROCE2() As String
Dim sConsulta As String

sConsulta = "CREATE PROCEDURE  SP_ESP_PROCE2  (@ANYO INT,@GMN1 VARCHAR(50),@PROCE INT,@ID INT)  AS"
sConsulta = sConsulta & vbCrLf & " set nocount on"
sConsulta = sConsulta & vbCrLf & " select NOM, COM, FECACT,datalength(data) msize  FROM PROCE_ESP WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ID=@ID"
sConsulta = sConsulta & vbCrLf & " Return"

CrearNuevoSP_ESP_PROCE2 = sConsulta
End Function


Private Function CrearNuevoSP_HAY_ESPECIFICACIONES() As String
Dim sConsulta As String

sConsulta = "CREATE PROCEDURE SP_HAY_ESPECIFICACIONES @ANYO smallint, @PROCE smallint, @GMN1 varchar(50) AS"

sConsulta = sConsulta & vbCrLf & " DECLARE @SIHAY BIT"
sConsulta = sConsulta & vbCrLf & " SELECT @SIHAY=0"


sConsulta = sConsulta & vbCrLf & " SELECT @SIHAY = CASE WHEN ESP IS NULL OR ESP='' THEN 0 ELSE 1 END FROM PROCE WHERE ANYO=@ANYO AND COD=@PROCE AND GMN1=@GMN1"

sConsulta = sConsulta & vbCrLf & " IF @SIHAY = 0"
sConsulta = sConsulta & vbCrLf & "     if (select count(id) from proce_esp where ANYO=@ANYO and gmn1=@gmn1 and proce=@proce)>0"
sConsulta = sConsulta & vbCrLf & "         select @SIHAY = 1"

sConsulta = sConsulta & vbCrLf & " SELECT @SIHAY HAYESP"

CrearNuevoSP_HAY_ESPECIFICACIONES = sConsulta
End Function


Private Function CrearNuevoTriggerPROCE_OFE_TG_INS() As String
Dim sConsulta As String


sConsulta = "CREATE TRIGGER PROCE_OFE_TG_INS ON [PROCE_OFE]"
sConsulta = sConsulta & vbCrLf & "FOR INSERT"
sConsulta = sConsulta & vbCrLf & "AS"
sConsulta = sConsulta & vbCrLf & "DECLARE @ANYO AS INTEGER"
sConsulta = sConsulta & vbCrLf & "DECLARE @GMN1 AS VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "DECLARE @PROCE AS INTEGER"
sConsulta = sConsulta & vbCrLf & "DECLARE @PROVE AS VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "DECLARE @OFE AS INTEGER"
sConsulta = sConsulta & vbCrLf & "DECLARE @MAXOFE AS INT"
sConsulta = sConsulta & vbCrLf & "DECLARE curTG_PROCE_OFE_Ins CURSOR FOR SELECT ANYO,GMN1,PROCE,PROVE,OFE FROM INSERTED"
sConsulta = sConsulta & vbCrLf & "OPEN curTG_PROCE_OFE_Ins"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM curTG_PROCE_OFE_Ins INTO @ANYO,@GMN1,@PROCE,@PROVE,@OFE"
sConsulta = sConsulta & vbCrLf & "WHILE @@FETCH_STATUS=0"
sConsulta = sConsulta & vbCrLf & "BEGIN"
 
sConsulta = sConsulta & vbCrLf & "/* Si hab�a ofertas anteriores de ese proveedor, introducimos su precio como ofertas por defecto */"
sConsulta = sConsulta & vbCrLf & "SET @MAXOFE = ( SELECT MAX(OFE) FROM PROCE_OFE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE AND OFE<@OFE)"
  
sConsulta = sConsulta & vbCrLf & "IF (@MAXOFE > 0 )"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_OFE SET ULT=0 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE AND NUM=@MAXOFE"
sConsulta = sConsulta & vbCrLf & "INSERT INTO ITEM_OFE (ANYO,GMN1,PROCE,ITEM,PROVE,NUM,PRECIO,CANTMAX,ULT,FECACT)"
sConsulta = sConsulta & vbCrLf & "SELECT ANYO,GMN1,PROCE,ITEM,PROVE,@OFE,PRECIO,CANTMAX,1,FECACT"
sConsulta = sConsulta & vbCrLf & "From ITEM_OFE"
sConsulta = sConsulta & vbCrLf & "WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE AND NUM=@MAXOFE"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "Else"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "INSERT INTO ITEM_OFE (ANYO,GMN1,PROCE,ITEM,PROVE,NUM,PRECIO,CANTMAX,ULT,FECACT)"
sConsulta = sConsulta & vbCrLf & "SELECT @ANYO,@GMN1,@PROCE,ID,@PROVE,@OFE,NULL,NULL,1,NULL"
sConsulta = sConsulta & vbCrLf & "From Item"
sConsulta = sConsulta & vbCrLf & "WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "/* Actualizamos el campo OFE de PROCE_PROVE que indica si el proveedor ha presentado ofertas para ese proceso*/"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_PROVE SET OFE = 1  WHERE ANYO= @ANYO  AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE"
 
sConsulta = sConsulta & vbCrLf & "/* Si se han insertado ofertas, actualizamos el estado del proceso  si es que este era inferior a 7 */"
sConsulta = sConsulta & vbCrLf & "IF (SELECT PROCE.EST FROM PROCE WHERE ANYO=@ANYO  AND GMN1=@GMN1 AND COD=@PROCE) < 7  /* 7= Con ofertas */"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE SET EST = 7  WHERE ANYO= @ANYO  AND GMN1=@GMN1 AND COD=@PROCE"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM curTG_PROCE_OFE_Ins INTO @ANYO,@GMN1,@PROCE,@PROVE,@OFE"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "Close curTG_PROCE_OFE_Ins"
sConsulta = sConsulta & vbCrLf & "DEALLOCATE curTG_PROCE_OFE_Ins"

CrearNuevoTriggerPROCE_OFE_TG_INS = sConsulta
End Function



Private Function A�adirForeignKey_Item_Ofe_2_7_06() As String
    
    sConsulta = "ALTER TABLE [dbo].[ITEM_OFE] ADD CONSTRAINT"
    sConsulta = sConsulta & vbLf & "[FK_ITEM_OFE_PROCE_OFE] FOREIGN KEY"
    sConsulta = sConsulta & vbLf & "("
    sConsulta = sConsulta & vbLf & "[ANYO],"
    sConsulta = sConsulta & vbLf & "[GMN1],"
    sConsulta = sConsulta & vbLf & "[PROCE],"
    sConsulta = sConsulta & vbLf & "[PROVE],"
    sConsulta = sConsulta & vbLf & "[NUM]"
    sConsulta = sConsulta & vbLf & ") REFERENCES [dbo].[PROCE_OFE]"
    sConsulta = sConsulta & vbLf & "("
    sConsulta = sConsulta & vbLf & "[ANYO],"
    sConsulta = sConsulta & vbLf & "[GMN1],"
    sConsulta = sConsulta & vbLf & "[PROCE],"
    sConsulta = sConsulta & vbLf & "[PROVE],"
    sConsulta = sConsulta & vbLf & "[OFE]"
    sConsulta = sConsulta & vbLf & ")"
    
    
    A�adirForeignKey_Item_Ofe_2_7_06 = sConsulta
    
End Function




Private Function CrearTablaOFEMIN() As String
    Dim sConsulta As String
    Dim rdores As RDO.rdoResultset
    Dim rdoresb As RDO.rdoResultset
    
    Set rdores = gRDOCon.OpenResultset("SELECT LONGITUD FROM DIC WHERE NOMBRE = 'GMN1'", rdOpenForwardOnly, rdConcurReadOnly, rdExecDirect)
    Set rdoresb = gRDOCon.OpenResultset("SELECT LONGITUD FROM DIC WHERE NOMBRE = 'PROVE'", rdOpenForwardOnly, rdConcurReadOnly, rdExecDirect)
    
    sConsulta = "CREATE TABLE [dbo].[OFEMIN] ("
    sConsulta = sConsulta & vbCrLf & "[ANYO] [int] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[GMN1] [varchar] (" & CStr(rdores("LONGITUD")) & ") NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[PROCE] [int] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[PROVE] [varchar] (" & CStr(rdoresb("LONGITUD")) & ") NULL ,"
    sConsulta = sConsulta & vbCrLf & "[ITEM] [int] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[PRECIO] [float] NULL ,"
    sConsulta = sConsulta & vbCrLf & "[OFE] [smallint] NULL ,"
    sConsulta = sConsulta & vbCrLf & "[CAMBIO] [float] NULL"
    sConsulta = sConsulta & vbCrLf & ") ON [PRIMARY]"
    sConsulta = sConsulta & vbCrLf & "ALTER TABLE [dbo].[OFEMIN] WITH NOCHECK ADD"
    sConsulta = sConsulta & vbCrLf & "CONSTRAINT [PK_OFEMIN] PRIMARY KEY  NONCLUSTERED"
    sConsulta = sConsulta & vbCrLf & "("
    sConsulta = sConsulta & vbCrLf & "[ANYO],"
    sConsulta = sConsulta & vbCrLf & "[GMN1],"
    sConsulta = sConsulta & vbCrLf & "[PROCE],"
    sConsulta = sConsulta & vbCrLf & "[ITEM]"
    sConsulta = sConsulta & vbCrLf & ")  ON [PRIMARY]"

    rdores.Close
    Set rdores = Nothing
    rdoresb.Close
    Set rdoresb = Nothing
    
    CrearTablaOFEMIN = sConsulta
End Function

Private Function CrearTablaPROVENOTIF() As String
    Dim sConsulta As String
    Dim rdores As RDO.rdoResultset
    Dim rdoresb As RDO.rdoResultset
    
    Set rdores = gRDOCon.OpenResultset("SELECT LONGITUD FROM DIC WHERE NOMBRE = 'GMN1'", rdOpenForwardOnly, rdConcurReadOnly, rdExecDirect)
    Set rdoresb = gRDOCon.OpenResultset("SELECT LONGITUD FROM DIC WHERE NOMBRE = 'PROVE'", rdOpenForwardOnly, rdConcurReadOnly, rdExecDirect)
    
    
    sConsulta = "CREATE TABLE [dbo].[PROVENOTIF] ("
    sConsulta = sConsulta & vbCrLf & "[ID] [int] IDENTITY (1, 1) NOT NULL, "
    sConsulta = sConsulta & vbCrLf & "[ANYO] [int] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[GMN1] [varchar] (" & CStr(rdores("LONGITUD")) & ") NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[PROCE] [int] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[PROCEDEN] [varchar] (500) NULL ,"
    sConsulta = sConsulta & vbCrLf & "[ITEM] [int] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[ITEMDEN] [varchar] (500) NULL ,"
    sConsulta = sConsulta & vbCrLf & "[PROVE] [varchar] (" & CStr(rdoresb("LONGITUD")) & ") NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[PROVEDEN] [varchar] (500) NULL ,"
    sConsulta = sConsulta & vbCrLf & "[EMAIL] [varchar] (100) NULL ,"
    sConsulta = sConsulta & vbCrLf & "[CONTACTO] [varchar] (500) NULL"
    sConsulta = sConsulta & vbCrLf & ") ON [PRIMARY]"
    sConsulta = sConsulta & vbCrLf & "ALTER TABLE [dbo].[PROVENOTIF] WITH NOCHECK ADD"
    sConsulta = sConsulta & vbCrLf & "CONSTRAINT [PK_PROVENOTIF] PRIMARY KEY  NONCLUSTERED"
    sConsulta = sConsulta & vbCrLf & "("
    sConsulta = sConsulta & vbCrLf & "[ID]"
    sConsulta = sConsulta & vbCrLf & ")  ON [PRIMARY]"
    
    rdores.Close
    Set rdores = Nothing
    rdoresb.Close
    Set rdoresb = Nothing
    
    CrearTablaPROVENOTIF = sConsulta
End Function


Private Function CrearNuevoTriggerPROCE_TG_UPD() As String
    Dim sConsulta As String
    
    sConsulta = "CREATE TRIGGER PROCE_TG_UPD ON dbo.PROCE"
    sConsulta = sConsulta & vbCrLf & "FOR UPDATE"
    sConsulta = sConsulta & vbCrLf & "AS"
    sConsulta = sConsulta & vbCrLf & "DECLARE @ANYO INT,@GMN1 VARCHAR(50),@COD SMALLINT"
    sConsulta = sConsulta & vbCrLf & "DECLARE @ITEM INT"
    sConsulta = sConsulta & vbCrLf & "DECLARE @SUBASTA AS BIT"
    sConsulta = sConsulta & vbCrLf & "IF UPDATE(SUBASTA)"
    sConsulta = sConsulta & vbCrLf & "BEGIN"
    sConsulta = sConsulta & vbCrLf & "DECLARE curTG_PROCE_upd CURSOR FOR SELECT ANYO,GMN1,COD,SUBASTA FROM INSERTED"
    sConsulta = sConsulta & vbCrLf & "OPEN curTG_PROCE_upd"
    sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM curTG_PROCE_upd INTO @ANYO,@GMN1,@COD,@SUBASTA"
    sConsulta = sConsulta & vbCrLf & "WHILE @@FETCH_STATUS=0"
    sConsulta = sConsulta & vbCrLf & "BEGIN"
    sConsulta = sConsulta & vbCrLf & "DELETE FROM OFEMIN WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@COD"
    sConsulta = sConsulta & vbCrLf & "IF @SUBASTA=1"
    sConsulta = sConsulta & vbCrLf & "BEGIN"
    sConsulta = sConsulta & vbCrLf & "DECLARE curTG_ITEM CURSOR FOR SELECT ID FROM ITEM WHERE  ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@COD"
    sConsulta = sConsulta & vbCrLf & "OPEN curTG_ITEM"
    sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM curTG_ITEM INTO @ITEM"
    sConsulta = sConsulta & vbCrLf & "WHILE @@FETCH_STATUS=0"
    sConsulta = sConsulta & vbCrLf & "BEGIN"
    sConsulta = sConsulta & vbCrLf & "INSERT INTO OFEMIN (ANYO,GMN1,PROCE,ITEM,PROVE,PRECIO) VALUES (@ANYO,@GMN1,@COD, @ITEM,NULL,NULL)"
    sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM curTG_ITEM INTO @ITEM"
    sConsulta = sConsulta & vbCrLf & "End"
    sConsulta = sConsulta & vbCrLf & "Close curTG_ITEM"
    sConsulta = sConsulta & vbCrLf & "DEALLOCATE curTG_ITEM"
    sConsulta = sConsulta & vbCrLf & "     End"
    sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM curTG_PROCE_upd INTO @ANYO,@GMN1,@COD,@SUBASTA"
    sConsulta = sConsulta & vbCrLf & "End"
    sConsulta = sConsulta & vbCrLf & "Close curTG_PROCE_upd"
    sConsulta = sConsulta & vbCrLf & "DEALLOCATE curTG_PROCE_upd"
    sConsulta = sConsulta & vbCrLf & "End"

    CrearNuevoTriggerPROCE_TG_UPD = sConsulta
End Function

Private Function CrearNuevoTriggerITEM_OFE_TG_UPD() As String
    Dim sConsulta As String
    
sConsulta = "CREATE TRIGGER ITEM_OFE_TG_UPD ON [ITEM_OFE] "
sConsulta = sConsulta & vbCrLf & "FOR UPDATE "
sConsulta = sConsulta & vbCrLf & "AS "
sConsulta = sConsulta & vbCrLf & "DECLARE @ANYO INT,@GMN1 VARCHAR(50),@PROCE INT, @ITEM INT,@PROVE VARCHAR(50), @NUM INT,@ULT INT "
sConsulta = sConsulta & vbCrLf & "DECLARE @PRECIO AS FLOAT "
sConsulta = sConsulta & vbCrLf & "DECLARE @PROVE2 AS VARCHAR(50) "
sConsulta = sConsulta & vbCrLf & "DECLARE @CONEX  AS VARCHAR(100),@FSP_CIA AS VARCHAR(50) "
sConsulta = sConsulta & vbCrLf & "DECLARE @BD AS VARCHAR(100),@SERVER AS VARCHAR(100) "
sConsulta = sConsulta & vbCrLf & "DECLARE @EMAIL AS VARCHAR(100) ,@PROVEDEN AS VARCHAR(500) , @CONTACTO AS VARCHAR(500) "
sConsulta = sConsulta & vbCrLf & "DECLARE @PRECCOMP AS FLOAT,@PROVECOMP AS VARCHAR(50) "
sConsulta = sConsulta & vbCrLf & "DECLARE @OFE2 AS SMALLINT "
sConsulta = sConsulta & vbCrLf & "DECLARE @IDENT AS INTEGER "
sConsulta = sConsulta & vbCrLf & "DECLARE @OFEMIN AS INTEGER "
sConsulta = sConsulta & vbCrLf & "DECLARE @PROCEDEN VARCHAR(500),@ITEMDEN VARCHAR(500) "
sConsulta = sConsulta & vbCrLf & "DECLARE @CAMBIOMONCEN FLOAT "
sConsulta = sConsulta & vbCrLf & "DECLARE @NUMOFEMIN int ,@INST INTEGER "
sConsulta = sConsulta & vbCrLf & "DECLARE @EMAIL_EX AS VARCHAR(100),@CONTACTO_EX AS VARCHAR(500) "
sConsulta = sConsulta & vbCrLf & "DECLARE curTG_ITEM_OFE_Upd CURSOR FOR SELECT ANYO,GMN1,PROCE,ITEM,PROVE,NUM,ULT FROM INSERTED "
sConsulta = sConsulta & vbCrLf & "OPEN curTG_ITEM_OFE_Upd "
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM curTG_ITEM_OFE_Upd INTO @ANYO,@GMN1,@PROCE,@ITEM,@PROVE,@NUM,@ULT "
sConsulta = sConsulta & vbCrLf & "WHILE @@FETCH_STATUS=0 "
sConsulta = sConsulta & vbCrLf & "BEGIN "
sConsulta = sConsulta & vbCrLf & " IF (SELECT SUBASTA FROM PROCE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE) = 1 "
sConsulta = sConsulta & vbCrLf & " BEGIN "
sConsulta = sConsulta & vbCrLf & "     IF UPDATE(PRECIO) "
sConsulta = sConsulta & vbCrLf & "     BEGIN "
sConsulta = sConsulta & vbCrLf & "         SELECT @NUMOFEMIN = MAX(NUM) FROM ITEM_OFE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE = @PROCE AND PROVE=@PROVE AND ITEM=@ITEM AND NUM!=@NUM "
sConsulta = sConsulta & vbCrLf & "         SET @OFEMIN =(SELECT COUNT(*) FROM OFEMIN WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE AND ITEM=@ITEM AND OFE=@NUMOFEMIN) "
sConsulta = sConsulta & vbCrLf & "         IF @ULT=1 OR @OFEMIN>0  /*Si el precio se ha modificado en la �ltima oferta recalculamos el precio m�s bajo para ese item*/ "
sConsulta = sConsulta & vbCrLf & "         BEGIN "
sConsulta = sConsulta & vbCrLf & "             SELECT TOP 1 @PRECIO= A.PRECIO / (B.CAMBIO * C.CAMBIO), @PROVE2=A.PROVE,@OFE2=A.NUM "
sConsulta = sConsulta & vbCrLf & "                   FROM ITEM_OFE A INNER JOIN PROCE_OFE B ON A.ANYO=B.ANYO AND A.GMN1=B.GMN1 AND A.PROCE=B.PROCE AND A.PROVE=B.PROVE AND A.NUM=B.OFE"
sConsulta = sConsulta & vbCrLf & "                  INNER JOIN PROCE C ON A.ANYO=C.ANYO AND A.GMN1=C.GMN1 AND A.PROCE = C.COD"
sConsulta = sConsulta & vbCrLf & "             WHERE A.ANYO=@ANYO AND A.GMN1=@GMN1 AND A.PROCE=@PROCE AND A.ITEM=@ITEM AND A.ULT=1 AND A.PRECIO IS NOT NULL"
sConsulta = sConsulta & vbCrLf & "             ORDER BY A.PRECIO / (B.CAMBIO * C.CAMBIO) ASC "
sConsulta = sConsulta & vbCrLf & "              /*Si el nuevo precio calculado es m�s bajo y el proveedor es diferente del que antes ten�a el precio m�s bajo se actualiza OFEMIN*/ "
sConsulta = sConsulta & vbCrLf & "             SELECT @PRECCOMP=PRECIO,@PROVECOMP=PROVE FROM OFEMIN WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM "
sConsulta = sConsulta & vbCrLf & "             IF @PROVECOMP IS NULL "
sConsulta = sConsulta & vbCrLf & "             /*Si el precio y prov. es nulo no notifica pero actualiza ofemin*/ "
sConsulta = sConsulta & vbCrLf & "             BEGIN "
sConsulta = sConsulta & vbCrLf & "                 UPDATE OFEMIN SET PRECIO=@PRECIO,PROVE=@PROVE2,OFE=@OFE2 , CAMBIO=@CAMBIOMONCEN WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM "
sConsulta = sConsulta & vbCrLf & "                 if @PRECIO IS NOT NULL "
sConsulta = sConsulta & vbCrLf & "                     INSERT INTO DETALLE_PUJAS (ANYO,GMN1,PROCE,ITEM,ID,OFE,PROVE,FECHA,PRECIO) VALUES (@ANYO,@GMN1,@PROCE,@ITEM,1,@OFE2,@PROVE2,GETDATE(),@PRECIO) "
sConsulta = sConsulta & vbCrLf & "             End "
sConsulta = sConsulta & vbCrLf & "             ELSE IF  @PRECIO <= @PRECCOMP OR @OFEMIN>0 "
sConsulta = sConsulta & vbCrLf & "             BEGIN "
sConsulta = sConsulta & vbCrLf & "                 IF @PROVECOMP <>@PROVE2 /*Si el proveedor es diferente habr� que notificarle que su oferta ha sido pisada*/ "
sConsulta = sConsulta & vbCrLf & "                 BEGIN "
sConsulta = sConsulta & vbCrLf & "                     /*Obtenemos los datos del proveedor (Denominaci�n,persona de contacto y e-mail de la persona de contacto) para notificarle que su oferta "
sConsulta = sConsulta & vbCrLf & "                     ha sido pisada*/ "
sConsulta = sConsulta & vbCrLf & "                     UPDATE PROCE_PROVE SET SUPERADA = 1 WHERE ANYO= @ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVECOMP"
sConsulta = sConsulta & vbCrLf & "                     SELECT TOP 1 @EMAIL=CON.EMAIL,@PROVEDEN=PROVE.DEN,@CONTACTO = NOMCON + ' ' + APECON "
sConsulta = sConsulta & vbCrLf & "                     FROM PROVE,PROCE_PROVE_PET,CON WHERE PROVE.COD= PROCE_PROVE_PET.PROVE AND PROCE_PROVE_PET.PROVE= CON.PROVE AND "
sConsulta = sConsulta & vbCrLf & "                     PROCE_PROVE_PET.NOMCON=CON.NOM AND PROCE_PROVE_PET.APECON = CON.APE AND "
sConsulta = sConsulta & vbCrLf & "                     PROCE_PROVE_PET.TIPOPET=0 AND ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE.COD=@PROVECOMP ORDER BY PROCE_PROVE_PET.ID DESC "
sConsulta = sConsulta & vbCrLf & "                     SELECT @PROCEDEN=PROCE.DEN,@ITEMDEN=ITEM.DESCR FROM PROCE INNER JOIN ITEM ON PROCE.ANYO=ITEM.ANYO AND "
sConsulta = sConsulta & vbCrLf & "                     Proce.gmn1 = Item.gmn1 And Proce.COD = Item.Proce "
sConsulta = sConsulta & vbCrLf & "                     WHERE PROCE.ANYO=@ANYO AND PROCE.GMN1=@GMN1 AND PROCE.COD=@PROCE AND ITEM.ID=@ITEM "
sConsulta = sConsulta & vbCrLf & "                         /*Si la instalaci�n es de BuySite*/ "
sConsulta = sConsulta & vbCrLf & "                     SELECT @INST=INSTWEB FROM PARGEN_INTERNO "
sConsulta = sConsulta & vbCrLf & "                     IF @INST = 1 "
sConsulta = sConsulta & vbCrLf & "                     BEGIN "
sConsulta = sConsulta & vbCrLf & "                         /*Inserta en la tabla PROVENOTIF*/ "
sConsulta = sConsulta & vbCrLf & "                         IF @EMAIL IS NULL OR @CONTACTO IS NULL  /*Si el e-mail o el contacto son nulos saca de contactos alguno que no sea nulo*/ "
sConsulta = sConsulta & vbCrLf & "                         BEGIN "
sConsulta = sConsulta & vbCrLf & "                             SELECT TOP 1 @EMAIL_EX=CON.EMAIL,@CONTACTO_EX = NOMCON + ' ' + APECON FROM PROCE_PROVE_PET,CON  "
sConsulta = sConsulta & vbCrLf & "                             WHERE PROCE_PROVE_PET.PROVE= CON.PROVE AND PROCE_PROVE_PET.NOMCON=CON.NOM AND PROCE_PROVE_PET.APECON = CON.APE  "
sConsulta = sConsulta & vbCrLf & "                             AND PROCE_PROVE_PET.TIPOPET=0 AND ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROCE_PROVE_PET.PROVE=@PROVECOMP  "
sConsulta = sConsulta & vbCrLf & "                             AND CON.EMAIL IS NOT NULL AND NOMCON IS NOT NULL "
sConsulta = sConsulta & vbCrLf & "                             INSERT INTO PROVENOTIF (ANYO,GMN1,PROCE,PROCEDEN,ITEM,ITEMDEN,PROVE,PROVEDEN,EMAIL,CONTACTO) VALUES "
sConsulta = sConsulta & vbCrLf & "                             (@ANYO,@GMN1,@PROCE,@PROCEDEN,@ITEM,@ITEMDEN,@PROVE,@PROVEDEN,@EMAIL_EX,@CONTACTO_EX) "
sConsulta = sConsulta & vbCrLf & "                         END "
sConsulta = sConsulta & vbCrLf & "                         ELSE "
sConsulta = sConsulta & vbCrLf & "                             INSERT INTO PROVENOTIF (ANYO,GMN1,PROCE,PROCEDEN,ITEM,ITEMDEN,PROVE,PROVEDEN,EMAIL,CONTACTO) VALUES "
sConsulta = sConsulta & vbCrLf & "                             (@ANYO,@GMN1,@PROCE,@PROCEDEN,@ITEM,@ITEMDEN,@PROVE,@PROVEDEN,@EMAIL,@CONTACTO) "
sConsulta = sConsulta & vbCrLf & "                     END "
sConsulta = sConsulta & vbCrLf & "                     ELSE /*Si la instalaci�n es de Portal*/ "
sConsulta = sConsulta & vbCrLf & "                     BEGIN "
sConsulta = sConsulta & vbCrLf & "                         /*Obtenemos el servidor,BD y el identificador de la Cia del portal*/ "
sConsulta = sConsulta & vbCrLf & "                         SELECT @SERVER = FSP_SRV, @BD = FSP_BD , @FSP_CIA = FSP_CIA FROM PARGEN_PORT "
sConsulta = sConsulta & vbCrLf & "                         /*Ejecutamos el store procedure de notificaci�n que se encuentra en el portal.*/ "
sConsulta = sConsulta & vbCrLf & "                         SET @CONEX=@SERVER + '.' + @BD + '.dbo.SP_ACTUALIZAR_PEND_NOTIF' "
sConsulta = sConsulta & vbCrLf & "                         EXECUTE @CONEX @FSP_CIA,@ANYO,@GMN1,@PROCE,@ITEM,@PROVECOMP,@PROCEDEN,@ITEMDEN,@EMAIL,@PROVEDEN,@CONTACTO "
sConsulta = sConsulta & vbCrLf & "                     END "
sConsulta = sConsulta & vbCrLf & "                 END "
sConsulta = sConsulta & vbCrLf & "                 /*Actualizamos OFEMIN con el precio m�s bajo*/ "
sConsulta = sConsulta & vbCrLf & "                 UPDATE OFEMIN SET PRECIO=@PRECIO,PROVE=@PROVE2,OFE=@OFE2  , CAMBIO=@CAMBIOMONCEN WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM "
sConsulta = sConsulta & vbCrLf & "                     UPDATE PROCE_PROVE "
sConsulta = sConsulta & vbCrLf & "                             SET SUPERADA = 0"
sConsulta = sConsulta & vbCrLf & "                         FROM PROCE_PROVE A"
sConsulta = sConsulta & vbCrLf & "                      WHERE A.ANYO= @ANYO "
sConsulta = sConsulta & vbCrLf & "                            AND A.GMN1=@GMN1 "
sConsulta = sConsulta & vbCrLf & "                            AND A.PROCE=@PROCE "
sConsulta = sConsulta & vbCrLf & "                            AND A.PROVE=@PROVE2 "
sConsulta = sConsulta & vbCrLf & "                            AND NOT EXISTS (SELECT * FROM OFEMIN B WHERE A.ANYO=B.ANYO AND A.GMN1=B.GMN1 AND A.PROCE=B.PROCE AND A.PROVE=B.PROVE)"
sConsulta = sConsulta & vbCrLf & "                   /*Si no era la oferta m�nima borramos y sino insertamos en DETALLE_PUJAS*/ "
sConsulta = sConsulta & vbCrLf & "                 IF @OFEMIN >0 "
sConsulta = sConsulta & vbCrLf & "                     DELETE FROM DETALLE_PUJAS WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM AND ID=(SELECT MAX(ID) FROM DETALLE_PUJAS WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM) "
sConsulta = sConsulta & vbCrLf & "                 SET @IDENT = (SELECT MAX(ID) FROM DETALLE_PUJAS WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM ) "
sConsulta = sConsulta & vbCrLf & "                 IF (SELECT COUNT(*) FROM DETALLE_PUJAS WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM AND PROVE=@PROVE2 AND OFE=@OFE2  AND ID=@IDENT)=0 "
sConsulta = sConsulta & vbCrLf & "                 BEGIN "
sConsulta = sConsulta & vbCrLf & "                     IF @IDENT IS NULL "
sConsulta = sConsulta & vbCrLf & "                         SET @IDENT=1 "
sConsulta = sConsulta & vbCrLf & "                     Else "
sConsulta = sConsulta & vbCrLf & "                         SET @IDENT= @IDENT+1 "
sConsulta = sConsulta & vbCrLf & "                     if @PRECIO IS NOT NULL "
sConsulta = sConsulta & vbCrLf & "                             INSERT INTO DETALLE_PUJAS (ANYO,GMN1,PROCE,ITEM,ID,OFE,PROVE,FECHA,PRECIO) VALUES (@ANYO,@GMN1,@PROCE,@ITEM,@IDENT,@OFE2,@PROVE2,GETDATE(),@PRECIO) "
sConsulta = sConsulta & vbCrLf & "                 END "
sConsulta = sConsulta & vbCrLf & "             End "
sConsulta = sConsulta & vbCrLf & "         End "
sConsulta = sConsulta & vbCrLf & "     End "
sConsulta = sConsulta & vbCrLf & " End "
sConsulta = sConsulta & vbCrLf & " FETCH NEXT FROM curTG_ITEM_OFE_Upd INTO @ANYO,@GMN1,@PROCE,@ITEM,@PROVE, @NUM,@ULT "
sConsulta = sConsulta & vbCrLf & "End "
sConsulta = sConsulta & vbCrLf & "Close curTG_ITEM_OFE_Upd "
sConsulta = sConsulta & vbCrLf & "DEALLOCATE curTG_ITEM_OFE_Upd "
    
    CrearNuevoTriggerITEM_OFE_TG_UPD = sConsulta
End Function

Private Function CrearNuevoTriggerPROCE_OFE_TG_DEL() As String
    Dim sConsulta As String
    
 sConsulta = "CREATE TRIGGER PROCE_OFE_TG_DEL ON dbo.PROCE_OFE "
 sConsulta = sConsulta & vbCrLf & "FOR DELETE "
 sConsulta = sConsulta & vbCrLf & "AS "
 sConsulta = sConsulta & vbCrLf & "DECLARE @ANYO AS INTEGER "
 sConsulta = sConsulta & vbCrLf & "DECLARE @GMN1 AS VARCHAR(50) "
 sConsulta = sConsulta & vbCrLf & "DECLARE @PROCE AS INTEGER "
 sConsulta = sConsulta & vbCrLf & "DECLARE @PROVE AS VARCHAR(50) "
 sConsulta = sConsulta & vbCrLf & "DECLARE @MAXOFE AS INT "
 sConsulta = sConsulta & vbCrLf & "DECLARE @ITEM AS INTEGER "
 sConsulta = sConsulta & vbCrLf & "DECLARE @PRECIO AS FLOAT "
 sConsulta = sConsulta & vbCrLf & "DECLARE @PROVE2 AS VARCHAR(50) "
 sConsulta = sConsulta & vbCrLf & "DECLARE @OFE2 AS SMALLINT "
 sConsulta = sConsulta & vbCrLf & "DECLARE @IDENT AS INTEGER "
 sConsulta = sConsulta & vbCrLf & "DECLARE @CAMBIOMONCEN FLOAT "
 sConsulta = sConsulta & vbCrLf & "DECLARE curTG_PROCE_OFE_Del CURSOR FOR SELECT ANYO,GMN1,PROCE,PROVE FROM DELETED "
 sConsulta = sConsulta & vbCrLf & "OPEN curTG_PROCE_OFE_Del "
 sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM curTG_PROCE_OFE_Del INTO @ANYO,@GMN1,@PROCE,@PROVE "
 sConsulta = sConsulta & vbCrLf & "WHILE @@FETCH_STATUS=0 "
 sConsulta = sConsulta & vbCrLf & "BEGIN "
 sConsulta = sConsulta & vbCrLf & " IF (SELECT SUBASTA FROM PROCE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE) = 1 "
 sConsulta = sConsulta & vbCrLf & " BEGIN "
 sConsulta = sConsulta & vbCrLf & "     --Si existen precios en la oferta eliminada que eran m�nimas en la puja se deber�n recalcular los nuevos precios m�nimos "
 sConsulta = sConsulta & vbCrLf & "     IF (SELECT COUNT(*) FROM OFEMIN WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE = @PROCE AND PROVE=@PROVE)>0 "
 sConsulta = sConsulta & vbCrLf & "     BEGIN "
 sConsulta = sConsulta & vbCrLf & "         /*Obtiene todos los items para los cuales ese proveedor tiene la oferta m�nima*/ "
 sConsulta = sConsulta & vbCrLf & "         DECLARE C CURSOR FOR SELECT ITEM FROM OFEMIN WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE "
 sConsulta = sConsulta & vbCrLf & "         OPEN C "
 sConsulta = sConsulta & vbCrLf & "         FETCH NEXT FROM C INTO @ITEM "
 sConsulta = sConsulta & vbCrLf & "         WHILE @@FETCH_STATUS=0 "
 sConsulta = sConsulta & vbCrLf & "         BEGIN "
 sConsulta = sConsulta & vbCrLf & "             SELECT TOP 1 @PRECIO= A.PRECIO / (B.CAMBIO * C.CAMBIO), @PROVE2=A.PROVE,@OFE2=A.NUM"
 sConsulta = sConsulta & vbCrLf & "                   FROM ITEM_OFE A INNER JOIN PROCE_OFE B ON A.ANYO=B.ANYO AND A.GMN1=B.GMN1 AND A.PROCE=B.PROCE AND A.PROVE=B.PROVE AND A.NUM=B.OFE"
 sConsulta = sConsulta & vbCrLf & "                  INNER JOIN PROCE C ON A.ANYO=C.ANYO AND A.GMN1=C.GMN1 AND A.PROCE = C.COD"
 sConsulta = sConsulta & vbCrLf & "             WHERE A.ANYO=@ANYO AND A.GMN1=@GMN1 AND A.PROCE=@PROCE AND A.ITEM=@ITEM AND A.ULT=1 AND A.PRECIO IS NOT NULL"
 sConsulta = sConsulta & vbCrLf & "             ORDER BY A.PRECIO / (B.CAMBIO * C.CAMBIO) ASC"
 sConsulta = sConsulta & vbCrLf & "             /*Borramos de DETALLE_PUJAS el �ltimo registro*/ "
 sConsulta = sConsulta & vbCrLf & "             SELECT @IDENT =MAX(ID) FROM DETALLE_PUJAS WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM "
 sConsulta = sConsulta & vbCrLf & "             DELETE FROM DETALLE_PUJAS WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM AND ID=@IDENT "
 sConsulta = sConsulta & vbCrLf & "             /*Insertamos en DETALLE_PUJAS*/ "
 sConsulta = sConsulta & vbCrLf & "             IF @PRECIO IS NULL "
 sConsulta = sConsulta & vbCrLf & "             BEGIN "
 sConsulta = sConsulta & vbCrLf & "                 UPDATE OFEMIN SET PRECIO=NULL, PROVE=NULL, OFE=NULL WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE = @PROCE AND ITEM = @ITEM "
 sConsulta = sConsulta & vbCrLf & "                 DELETE FROM DETALLE_PUJAS WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM "
 sConsulta = sConsulta & vbCrLf & "             End "
 sConsulta = sConsulta & vbCrLf & "             Else "
 sConsulta = sConsulta & vbCrLf & "             BEGIN "
 sConsulta = sConsulta & vbCrLf & "                 /*Actualiza la tabla OFEMIN con la oferta m�nima para el proceso e item*/ "
 sConsulta = sConsulta & vbCrLf & "                 UPDATE OFEMIN SET PRECIO=@PRECIO,PROVE=@PROVE2,OFE=@OFE2 WHERE ANYO=@ANYO  AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM "
 sConsulta = sConsulta & vbCrLf & "                 INSERT INTO DETALLE_PUJAS (ANYO,GMN1,PROCE,ITEM,ID,OFE,PROVE,FECHA,PRECIO) VALUES (@ANYO,@GMN1,@PROCE,@ITEM,@IDENT,@OFE2,@PROVE2,GETDATE(),@PRECIO) "
 sConsulta = sConsulta & vbCrLf & "             End "
 sConsulta = sConsulta & vbCrLf & "             FETCH NEXT FROM C INTO @ITEM "
 sConsulta = sConsulta & vbCrLf & "         End "
 sConsulta = sConsulta & vbCrLf & "         Close C "
 sConsulta = sConsulta & vbCrLf & "         DEALLOCATE C "
 sConsulta = sConsulta & vbCrLf & "     End "
 sConsulta = sConsulta & vbCrLf & "     /* Si hab�a ofertas anteriores de ese proveedor, introducimos su precio como ofertas por defecto */ "
 sConsulta = sConsulta & vbCrLf & "     SET @MAXOFE = ( SELECT MAX(OFE) FROM PROCE_OFE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE ) "
 sConsulta = sConsulta & vbCrLf & "     IF (@MAXOFE IS NULL ) "
 sConsulta = sConsulta & vbCrLf & "     BEGIN "
 sConsulta = sConsulta & vbCrLf & "     /* Si se han insertado ofertas, actualizamos el estado del proceso  si es que este era inferior a 7 */ "
 sConsulta = sConsulta & vbCrLf & "         IF (SELECT PROCE.EST FROM PROCE WHERE ANYO=@ANYO  AND GMN1=@GMN1 AND COD=@PROCE) = 7  /* 7= Con ofertas */ "
 sConsulta = sConsulta & vbCrLf & "         BEGIN "
 sConsulta = sConsulta & vbCrLf & "             UPDATE PROCE SET EST = 6  WHERE ANYO= @ANYO  AND GMN1=@GMN1 AND COD=@PROCE "
 sConsulta = sConsulta & vbCrLf & "         End "
 sConsulta = sConsulta & vbCrLf & "     End "
 sConsulta = sConsulta & vbCrLf & " End "
 sConsulta = sConsulta & vbCrLf & " FETCH NEXT FROM curTG_PROCE_OFE_Del INTO @ANYO,@GMN1,@PROCE,@PROVE "
 sConsulta = sConsulta & vbCrLf & "End "
 sConsulta = sConsulta & vbCrLf & "Close curTG_PROCE_OFE_Del "
 sConsulta = sConsulta & vbCrLf & "DEALLOCATE curTG_PROCE_OFE_Del "
    
    CrearNuevoTriggerPROCE_OFE_TG_DEL = sConsulta
End Function

Private Function CrearSP_DEVOLVER_PROCESOS_PROVE_WEB() As String
    Dim sConsulta As String
    
 sConsulta = "CREATE PROCEDURE SP_DEVOLVER_PROCESOS_PROVE_WEB (@PROVE VARCHAR(50),@NORMAL SMALLINT OUTPUT ,@SUBASTA SMALLINT OUTPUT) AS "
 sConsulta = sConsulta & vbCrLf & "BEGIN "
 sConsulta = sConsulta & vbCrLf & "SET @NORMAL=(SELECT COUNT(*) FROM PROCE INNER JOIN PROCE_PROVE ON PROCE.ANYO= PROCE_PROVE.ANYO AND "
 sConsulta = sConsulta & vbCrLf & "PROCE.GMN1=PROCE_PROVE.GMN1 AND PROCE.COD=PROCE_PROVE.PROCE AND PROCE_PROVE.PROVE=@PROVE "
 sConsulta = sConsulta & vbCrLf & "AND PROCE_PROVE.PUB=1 AND PROCE.EST<11 AND SUBASTA=0) "
 sConsulta = sConsulta & vbCrLf & "SET @SUBASTA=(SELECT COUNT(*) FROM PROCE INNER JOIN PROCE_PROVE ON PROCE.ANYO= PROCE_PROVE.ANYO AND "
 sConsulta = sConsulta & vbCrLf & "PROCE.GMN1=PROCE_PROVE.GMN1 AND PROCE.COD=PROCE_PROVE.PROCE AND PROCE_PROVE.PROVE=@PROVE "
 sConsulta = sConsulta & vbCrLf & "AND PROCE_PROVE.PUB=1 AND PROCE.EST<11 AND SUBASTA=1) "
 sConsulta = sConsulta & vbCrLf & "/*Devuelve en 1� lugar los procesos normales y luego los abiertos en modo subasta*/ "
 sConsulta = sConsulta & vbCrLf & " "
 sConsulta = sConsulta & vbCrLf & "SELECT PROCE.ANYO,PROCE.GMN1,PROCE.COD,PROCE.DEN,PROCE.FECLIMOFE,PROCE_PROVE.OFE,PROCE.EST,PROCE_PROVE.SUPERADA, "
 sConsulta = sConsulta & vbCrLf & "PROCE.SUBASTA,PROCE.SUBASTAPROVE,PROCE.SUBASTAPUJAS,PROCE.MON FROM PROCE INNER JOIN PROCE_PROVE ON PROCE.ANYO= PROCE_PROVE.ANYO AND "
 sConsulta = sConsulta & vbCrLf & "PROCE.GMN1=PROCE_PROVE.GMN1 AND PROCE.COD=PROCE_PROVE.PROCE AND PROCE_PROVE.PROVE=@PROVE "
 sConsulta = sConsulta & vbCrLf & "AND PROCE_PROVE.PUB=1 AND PROCE.EST<11  ORDER BY SUBASTA "
 sConsulta = sConsulta & vbCrLf & "End "
' sConsulta = sConsulta & vbCrLf & "GO "
 
    CrearSP_DEVOLVER_PROCESOS_PROVE_WEB = sConsulta
End Function


Private Function CrearSP_DEVOLVER_PROCESO_WEB() As String
    Dim sConsulta As String
    
    sConsulta = "CREATE PROCEDURE SP_DEVOLVER_PROCESO_WEB (@ANYO INTEGER,@GMN1 VARCHAR(20),@PROCE INTEGER ,@ACTIVA SMALLINT OUTPUT) AS"
    sConsulta = sConsulta & vbCrLf & "DECLARE @SUBASTA AS TINYINT"
    sConsulta = sConsulta & vbCrLf & "DECLARE @FECLIMOFE AS DATETIME"
    sConsulta = sConsulta & vbCrLf & "DECLARE @HAYESP  AS BIT"
    sConsulta = sConsulta & vbCrLf & "BEGIN"
    sConsulta = sConsulta & vbCrLf & "SELECT @SUBASTA=SUBASTA , @FECLIMOFE =FECLIMOFE FROM PROCE WHERE"
    sConsulta = sConsulta & vbCrLf & "PROCE.GMN1=@GMN1 AND PROCE.COD=@PROCE AND PROCE.ANYO=@ANYO"
    sConsulta = sConsulta & vbCrLf & "IF @SUBASTA= 1 AND @FECLIMOFE <=GETDATE()"
    sConsulta = sConsulta & vbCrLf & "BEGIN"
    sConsulta = sConsulta & vbCrLf & "SET @ACTIVA=2"
    sConsulta = sConsulta & vbCrLf & "SELECT NULL AS NADA"
    sConsulta = sConsulta & vbCrLf & "End"
    sConsulta = sConsulta & vbCrLf & "Else"
    sConsulta = sConsulta & vbCrLf & "BEGIN"
    sConsulta = sConsulta & vbCrLf & "SELECT @HAYESP = CASE WHEN ESP IS NULL OR ESP='' THEN 0 ELSE 1 END FROM PROCE WHERE ANYO=@ANYO AND COD=@PROCE AND GMN1=@GMN1"
    sConsulta = sConsulta & vbCrLf & "IF @HAYESP = 0"
    sConsulta = sConsulta & vbCrLf & "BEGIN"
    sConsulta = sConsulta & vbCrLf & "if (select count(id) from proce_esp where ANYO=@ANYO and gmn1=@GMN1 and proce=@PROCE)>0"
    sConsulta = sConsulta & vbCrLf & "select @HAYESP = 1"
    sConsulta = sConsulta & vbCrLf & "End"
    sConsulta = sConsulta & vbCrLf & "IF @SUBASTA=1"
    sConsulta = sConsulta & vbCrLf & "SET @ACTIVA=1"
    sConsulta = sConsulta & vbCrLf & "Else"
    sConsulta = sConsulta & vbCrLf & "SET @ACTIVA=0"
    sConsulta = sConsulta & vbCrLf & "SELECT PROCE.ANYO,PROCE.GMN1,PROCE.COD,PROCE.GMN2,PROCE.GMN3,PROCE.GMN4,PROCE.ADJDIR,PROCE.PRES,PROCE.EST,"
    sConsulta = sConsulta & vbCrLf & "PROCE.FECAPE,PROCE.FECLIMOFE,PROCE.FECNEC,PROCE.FECPRES,PROCE.FECENVPET,PROCE.FECPROXREU,PROCE.FECULTREU,"
    sConsulta = sConsulta & vbCrLf & "PROCE.MON,PROCE.CAMBIO,PROCE.SOLICITUD,PROCE.ESP,"
    sConsulta = sConsulta & vbCrLf & "PROCE.SUBASTA,PROCE.SUBASTAPROVE,PROCE.SUBASTAPUJAS,HAYESP = @HAYESP"
    sConsulta = sConsulta & vbCrLf & "FROM PROCE WHERE"
    sConsulta = sConsulta & vbCrLf & "PROCE.GMN1=@GMN1 AND PROCE.COD=@PROCE AND PROCE.ANYO=@ANYO"
    sConsulta = sConsulta & vbCrLf & "End"
    sConsulta = sConsulta & vbCrLf & "Return"
    sConsulta = sConsulta & vbCrLf & "End"
    
    CrearSP_DEVOLVER_PROCESO_WEB = sConsulta
End Function

Private Function CrearSP_DEVOLVER_OFERTA_PROVE_WEB() As String
    Dim sConsulta As String
    
    sConsulta = "CREATE PROCEDURE SP_DEVOLVER_OFERTA_PROVE_WEB (@ANYO INTEGER,@GMN1 VARCHAR(20),@PROCE INTEGER ,@PROVE VARCHAR(50),"
    sConsulta = sConsulta & vbCrLf & "@ADJUDICABLE AS SMALLINT,@COMPARABLE AS SMALLINT) AS"
    sConsulta = sConsulta & vbCrLf & "BEGIN"
    sConsulta = sConsulta & vbCrLf & "IF @ADJUDICABLE=1"
    sConsulta = sConsulta & vbCrLf & "SELECT PROCE_OFE.ANYO,PROCE_OFE.GMN1,PROCE_OFE.PROCE,PROCE_OFE.PROVE,PROCE_OFE.OFE,PROCE_OFE.EST,PROCE_OFE.FECREC,"
    sConsulta = sConsulta & vbCrLf & "PROCE_OFE.FECVAL,PROCE_OFE.MON,PROCE_OFE.CAMBIO,PROCE_OFE.OBS,MON.DEN AS MONDEN"
    sConsulta = sConsulta & vbCrLf & "FROM PROCE_OFE AS PROCE_OFE INNER JOIN MON AS MON ON PROCE_OFE.MON=MON.COD"
    sConsulta = sConsulta & vbCrLf & "INNER JOIN OFEEST AS OFEEST ON PROCE_OFE.EST=OFEEST.COD AND OFEEST.ADJ= 1"
    sConsulta = sConsulta & vbCrLf & "WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE"
    sConsulta = sConsulta & vbCrLf & "AND OFE= ( SELECT MAX(OFE) FROM PROCE_OFE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE)"
    sConsulta = sConsulta & vbCrLf & "ELSE IF @COMPARABLE=1"
    sConsulta = sConsulta & vbCrLf & "SELECT PROCE_OFE.ANYO,PROCE_OFE.GMN1,PROCE_OFE.PROCE,PROCE_OFE.PROVE,PROCE_OFE.OFE,PROCE_OFE.EST,PROCE_OFE.FECREC,"
    sConsulta = sConsulta & vbCrLf & "PROCE_OFE.FECVAL,PROCE_OFE.MON,PROCE_OFE.CAMBIO,PROCE_OFE.OBS,MON.DEN AS MONDEN"
    sConsulta = sConsulta & vbCrLf & "FROM PROCE_OFE AS PROCE_OFE INNER JOIN MON AS MON ON PROCE_OFE.MON=MON.COD"
    sConsulta = sConsulta & vbCrLf & "INNER JOIN OFEEST AS OFEEST ON PROCE_OFE.EST=OFEEST.COD AND OFEEST.COMP= 1"
    sConsulta = sConsulta & vbCrLf & "WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE"
    sConsulta = sConsulta & vbCrLf & "AND OFE= ( SELECT MAX(OFE) FROM PROCE_OFE AS PROCE_OFE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE)"
    sConsulta = sConsulta & vbCrLf & "Else"
    sConsulta = sConsulta & vbCrLf & "SELECT PROCE_OFE.ANYO,PROCE_OFE.GMN1,PROCE_OFE.PROCE,PROCE_OFE.PROVE,PROCE_OFE.OFE,PROCE_OFE.EST,PROCE_OFE.FECREC,"
    sConsulta = sConsulta & vbCrLf & "PROCE_OFE.FECVAL,PROCE_OFE.MON,PROCE_OFE.CAMBIO,PROCE_OFE.OBS,MON.DEN AS MONDEN"
    sConsulta = sConsulta & vbCrLf & "FROM PROCE_OFE AS PROCE_OFE INNER JOIN MON AS MON ON PROCE_OFE.MON=MON.COD"
    sConsulta = sConsulta & vbCrLf & "WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE"
    sConsulta = sConsulta & vbCrLf & "AND OFE= ( SELECT MAX(OFE) FROM PROCE_OFE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE)"
    sConsulta = sConsulta & vbCrLf & "Return"
    sConsulta = sConsulta & vbCrLf & "End"
    
    CrearSP_DEVOLVER_OFERTA_PROVE_WEB = sConsulta
End Function

Private Function CrearSP_DEVOLVER_PUJAS_ITEM_WEB() As String
    Dim sConsulta As String
    
    sConsulta = "CREATE PROCEDURE SP_DEVOLVER_PUJAS_ITEM_WEB (@ANYO INTEGER,@GMN1 VARCHAR(20),@PROCE INTEGER ,@ITEM INTEGER) AS"
    sConsulta = sConsulta & vbCrLf & "BEGIN"
    sConsulta = sConsulta & vbCrLf & "SELECT DETALLE_PUJAS.FECHA,PROVE.COD + ' - ' + PROVE.DEN AS PROVE,DETALLE_PUJAS.PRECIO"
    sConsulta = sConsulta & vbCrLf & "FROM DETALLE_PUJAS INNER JOIN PROVE ON DETALLE_PUJAS.PROVE=PROVE.COD"
    sConsulta = sConsulta & vbCrLf & "WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM"
    sConsulta = sConsulta & vbCrLf & "ORDER BY FECHA DESC"
    sConsulta = sConsulta & vbCrLf & "End"
    
    CrearSP_DEVOLVER_PUJAS_ITEM_WEB = sConsulta
End Function

Private Function CrearTablaDETALLE_PUJAS() As String
    Dim sConsulta As String
    Dim rdores As RDO.rdoResultset
    Dim rdoresb As RDO.rdoResultset
    
    Set rdores = gRDOCon.OpenResultset("SELECT LONGITUD FROM DIC WHERE NOMBRE = 'GMN1'", rdOpenForwardOnly, rdConcurReadOnly, rdExecDirect)
    Set rdoresb = gRDOCon.OpenResultset("SELECT LONGITUD FROM DIC WHERE NOMBRE = 'PROVE'", rdOpenForwardOnly, rdConcurReadOnly, rdExecDirect)

    sConsulta = "CREATE TABLE [dbo].[DETALLE_PUJAS] ("
    sConsulta = sConsulta & vbCrLf & "[ANYO] [int] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[GMN1] [varchar] (" & CStr(rdores("LONGITUD")) & ") NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[PROCE] [int] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[ITEM] [int] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[ID] [int] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[OFE] [int] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[PROVE] [varchar] (" & CStr(rdoresb("LONGITUD")) & ") NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[FECHA] [datetime] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[PRECIO][Float] NOT NULL"
    sConsulta = sConsulta & vbCrLf & ") ON [PRIMARY]"
    sConsulta = sConsulta & vbCrLf & "ALTER TABLE [dbo].[DETALLE_PUJAS] WITH NOCHECK ADD"
    sConsulta = sConsulta & vbCrLf & "CONSTRAINT [PK_DETALLE_PUJAS] PRIMARY KEY  NONCLUSTERED"
    sConsulta = sConsulta & vbCrLf & "("
    sConsulta = sConsulta & vbCrLf & "[ANYO],"
    sConsulta = sConsulta & vbCrLf & "[GMN1],"
    sConsulta = sConsulta & vbCrLf & "[PROCE],"
    sConsulta = sConsulta & vbCrLf & "[ITEM],"
    sConsulta = sConsulta & vbCrLf & "[ID]"
    sConsulta = sConsulta & vbCrLf & ")  ON [PRIMARY]"

    rdores.Close
    Set rdores = Nothing
    rdoresb.Close
    Set rdoresb = Nothing
    
    CrearTablaDETALLE_PUJAS = sConsulta
End Function



Private Function CrearSP_ITEMS_2_7_25() As String
    Dim sConsulta As String
    
 sConsulta = "CREATE PROCEDURE  SP_ITEMS  (@ANYO INT,@GMN1 VARCHAR(50),@COD INT,@PROVEEDOR BIT,@CODMON varchar(50))  AS  "
 sConsulta = sConsulta & vbCrLf & "BEGIN  "
 sConsulta = sConsulta & vbCrLf & "SET CONCAT_NULL_YIELDS_NULL OFF  "
 sConsulta = sConsulta & vbCrLf & "DECLARE @EQUIV FLOAT  "
 sConsulta = sConsulta & vbCrLf & "SELECT @EQUIV= MON.EQUIV  FROM MON WHERE MON.COD=@CODMON  "
 sConsulta = sConsulta & vbCrLf & "    SELECT ITEM.ANYO,  "
 sConsulta = sConsulta & vbCrLf & "           ITEM.GMN1,  "
 sConsulta = sConsulta & vbCrLf & "           ITEM.PROCE,  "
 sConsulta = sConsulta & vbCrLf & "           ITEM.ID,  "
 sConsulta = sConsulta & vbCrLf & "           ITEM.GMN2,  "
 sConsulta = sConsulta & vbCrLf & "           ITEM.GMN3,  "
 sConsulta = sConsulta & vbCrLf & "           ITEM.GMN4,  "
 sConsulta = sConsulta & vbCrLf & "           ITEM.ART,  "
 sConsulta = sConsulta & vbCrLf & "           ITEM.DESCR + ART4.DEN AS DESCR,  "
 sConsulta = sConsulta & vbCrLf & "           ITEM.DEST,  "
 sConsulta = sConsulta & vbCrLf & "           ITEM.UNI,  "
 sConsulta = sConsulta & vbCrLf & "           ITEM.CANT,  "
 sConsulta = sConsulta & vbCrLf & "           ITEM.PREC, "
 sConsulta = sConsulta & vbCrLf & "           ITEM.PRES,  "
 sConsulta = sConsulta & vbCrLf & "           ITEM.PAG, "
 sConsulta = sConsulta & vbCrLf & "           ITEM.FECINI,  "
 sConsulta = sConsulta & vbCrLf & "           ITEM.FECFIN,  "
 sConsulta = sConsulta & vbCrLf & "           ITEM.ESP,  "
 sConsulta = sConsulta & vbCrLf & "           ITEM.CONF,  "
 sConsulta = sConsulta & vbCrLf & "           ITEM.OBJ,  "
 sConsulta = sConsulta & vbCrLf & "           ITEM.FECHAOBJ, "
 sConsulta = sConsulta & vbCrLf & "           CASE  WHEN NUMESP > 0 THEN 1 WHEN NOT ITEM.ESP  IS NULL THEN 1 ELSE 0 END ESPECADJ ,  "
 sConsulta = sConsulta & vbCrLf & "           ITEM.ANYO,  "
 sConsulta = sConsulta & vbCrLf & "           ITEM.GMN1,  "
 sConsulta = sConsulta & vbCrLf & "           ITEM.PROCE ,  "
 sConsulta = sConsulta & vbCrLf & "           round(OFEMIN.PRECIO*@EQUIV,5) AS MINPRECIO , "
 sConsulta = sConsulta & vbCrLf & "           CASE WHEN @PROVEEDOR=1 THEN OFEMIN.PROVE + ' - ' + PROVE.DEN ELSE NULL END MINPROVE "
 sConsulta = sConsulta & vbCrLf & "      FROM ((ITEM LEFT JOIN ART4  "
 sConsulta = sConsulta & vbCrLf & "                    ON ITEM.ART = ART4.COD  "
 sConsulta = sConsulta & vbCrLf & "                   AND ITEM.GMN4 = ART4.GMN4  "
 sConsulta = sConsulta & vbCrLf & "                   AND ITEM.GMN3 = ART4.GMN3  "
 sConsulta = sConsulta & vbCrLf & "                   AND ITEM.GMN2 = ART4.GMN2 "
 sConsulta = sConsulta & vbCrLf & "                   AND ITEM.GMN1 = ART4.GMN1) LEFT JOIN  "
 sConsulta = sConsulta & vbCrLf & "                                                (SELECT ITEM_ESP.ANYO,  "
 sConsulta = sConsulta & vbCrLf & "                                                        ITEM_ESP.GMN1,  "
 sConsulta = sConsulta & vbCrLf & "                                                        ITEM_ESP.PROCE,  "
 sConsulta = sConsulta & vbCrLf & "                                                        ITEM_ESP.ITEM,  "
 sConsulta = sConsulta & vbCrLf & "                                                        Count(ITEM_ESP.ID) NUMESP  "
 sConsulta = sConsulta & vbCrLf & "                                                   From ITEM_ESP  "
 sConsulta = sConsulta & vbCrLf & "                                                  WHERE ITEM_ESP.ANYO=@ANYO  "
 sConsulta = sConsulta & vbCrLf & "                                                    AND ITEM_ESP.GMN1=@GMN1  "
 sConsulta = sConsulta & vbCrLf & "                                                    AND ITEM_ESP.PROCE=@COD  "
 sConsulta = sConsulta & vbCrLf & "                                                GROUP BY ITEM_ESP.ANYO,  "
 sConsulta = sConsulta & vbCrLf & "                                                         ITEM_ESP.GMN1,  "
 sConsulta = sConsulta & vbCrLf & "                                                         ITEM_ESP.PROCE,  "
 sConsulta = sConsulta & vbCrLf & "                                                         ITEM_ESP.ITEM  "
 sConsulta = sConsulta & vbCrLf & "                                                 ) ITEMESP  "
 sConsulta = sConsulta & vbCrLf & "                                                ON ITEM.ID = ITEMESP.ITEM  "
 sConsulta = sConsulta & vbCrLf & "                                               AND ITEM.GMN1 = ITEMESP.GMN1 "
 sConsulta = sConsulta & vbCrLf & "                                               AND ITEM.PROCE = ITEMESP.PROCE "
 sConsulta = sConsulta & vbCrLf & "                                               AND ITEM.ANYO = ITEMESP.ANYO  "
 sConsulta = sConsulta & vbCrLf & "            ) "
 sConsulta = sConsulta & vbCrLf & "           LEFT JOIN OFEMIN  "
 sConsulta = sConsulta & vbCrLf & "             ON OFEMIN.ANYO=ITEM.ANYO  "
 sConsulta = sConsulta & vbCrLf & "            AND OFEMIN.GMN1=ITEM.GMN1  "
 sConsulta = sConsulta & vbCrLf & "            AND OFEMIN.PROCE=ITEM.PROCE  "
 sConsulta = sConsulta & vbCrLf & "            AND OFEMIN.ITEM=ITEM.ID "
 sConsulta = sConsulta & vbCrLf & "           LEFT JOIN PROVE  "
 sConsulta = sConsulta & vbCrLf & "             ON OFEMIN.PROVE=PROVE.COD "
 sConsulta = sConsulta & vbCrLf & "      WHERE ITEM.ANYO=@ANYO AND ITEM.GMN1=@GMN1 AND ITEM.PROCE=@COD  "
 sConsulta = sConsulta & vbCrLf & " "
 sConsulta = sConsulta & vbCrLf & "SET CONCAT_NULL_YIELDS_NULL ON  "
 sConsulta = sConsulta & vbCrLf & "Return  "
 sConsulta = sConsulta & vbCrLf & "End "
    
    CrearSP_ITEMS_2_7_25 = sConsulta
End Function


Private Function A�adirCamposUSU_2_7_25() As String
Dim sConsulta As String

sConsulta = "ALTER TABLE dbo.USU ADD "
sConsulta = sConsulta & vbCrLf & "    THOUSANFMT char(1) NULL,"
sConsulta = sConsulta & vbCrLf & "    DECIMALFMT char(1) NULL,"
sConsulta = sConsulta & vbCrLf & "    PRECISIONFMT smallint NOT NULL CONSTRAINT DF_USU_PRECISIONFMT DEFAULT 2,"
sConsulta = sConsulta & vbCrLf & "    DATEFMT varchar(50) NULL,"
sConsulta = sConsulta & vbCrLf & "    MOSTRARFMT tinyint NOT NULL CONSTRAINT DF_USU_MOSTRARFMT DEFAULT 1"

A�adirCamposUSU_2_7_25 = sConsulta

End Function

Private Function ModificarTriggerITEM_OFE() As String
Dim sConsulta As String

sConsulta = "CREATE TRIGGER ITEM_OFE_TG_UPD ON [ITEM_OFE]"
sConsulta = sConsulta & vbCrLf & " FOR UPDATE"
sConsulta = sConsulta & vbCrLf & " AS"
sConsulta = sConsulta & vbCrLf & " DECLARE @ANYO INT,@GMN1 VARCHAR(50),@PROCE INT, @ITEM INT,@PROVE VARCHAR(50), @NUM INT,@ULT INT "
sConsulta = sConsulta & vbCrLf & " DECLARE @PRECIO AS FLOAT"
sConsulta = sConsulta & vbCrLf & " DECLARE @PROVE2 AS VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & " DECLARE @CONEX  AS VARCHAR(100),@FSP_CIA AS VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & " DECLARE @BD AS VARCHAR(100),@SERVER AS VARCHAR(100)"
sConsulta = sConsulta & vbCrLf & " DECLARE @EMAIL AS VARCHAR(100) ,@PROVEDEN AS VARCHAR(500) , @CONTACTO AS VARCHAR(500)"
sConsulta = sConsulta & vbCrLf & " DECLARE @PRECCOMP AS FLOAT,@PROVECOMP AS VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & " DECLARE @OFE2 AS SMALLINT"
sConsulta = sConsulta & vbCrLf & " DECLARE @IDENT AS INTEGER"
sConsulta = sConsulta & vbCrLf & " DECLARE @OFEMIN AS INTEGER"
sConsulta = sConsulta & vbCrLf & " DECLARE @PROCEDEN VARCHAR(500),@ITEMDEN VARCHAR(500)"
sConsulta = sConsulta & vbCrLf & " DECLARE @CAMBIOMONCEN FLOAT"
sConsulta = sConsulta & vbCrLf & " DECLARE @NUMOFEMIN int ,@INST INTEGER"
sConsulta = sConsulta & vbCrLf & " DECLARE @EMAIL_EX AS VARCHAR(100),@CONTACTO_EX AS VARCHAR(500)"
sConsulta = sConsulta & vbCrLf & " DECLARE curTG_ITEM_OFE_Upd CURSOR FOR SELECT ANYO,GMN1,PROCE,ITEM,PROVE,NUM,ULT FROM INSERTED"
sConsulta = sConsulta & vbCrLf & " OPEN curTG_ITEM_OFE_Upd"
sConsulta = sConsulta & vbCrLf & " FETCH NEXT FROM curTG_ITEM_OFE_Upd INTO @ANYO,@GMN1,@PROCE,@ITEM,@PROVE,@NUM,@ULT"
sConsulta = sConsulta & vbCrLf & " WHILE @@FETCH_STATUS=0"
sConsulta = sConsulta & vbCrLf & " BEGIN"
 sConsulta = sConsulta & vbCrLf & " IF (SELECT SUBASTA FROM PROCE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE) = 1"
 sConsulta = sConsulta & vbCrLf & " BEGIN"
sConsulta = sConsulta & vbCrLf & "      IF UPDATE(PRECIO)"
sConsulta = sConsulta & vbCrLf & "      BEGIN"
sConsulta = sConsulta & vbCrLf & "          SELECT @NUMOFEMIN = MAX(NUM) FROM ITEM_OFE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE = @PROCE AND PROVE=@PROVE AND ITEM=@ITEM AND NUM!=@NUM"
sConsulta = sConsulta & vbCrLf & "          SET @OFEMIN =(SELECT COUNT(*) FROM OFEMIN WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE AND ITEM=@ITEM AND OFE=@NUMOFEMIN)"
sConsulta = sConsulta & vbCrLf & "          IF @ULT=1 OR @OFEMIN>0  /*Si el precio se ha modificado en la �ltima oferta recalculamos el precio m�s bajo para ese item*/"
sConsulta = sConsulta & vbCrLf & "          BEGIN"
sConsulta = sConsulta & vbCrLf & "              SELECT TOP 1 @PRECIO= A.PRECIO / (B.CAMBIO * C.CAMBIO), @PROVE2=A.PROVE,@OFE2=A.NUM"
sConsulta = sConsulta & vbCrLf & "                    FROM ITEM_OFE A INNER JOIN PROCE_OFE B ON A.ANYO=B.ANYO AND A.GMN1=B.GMN1 AND A.PROCE=B.PROCE AND A.PROVE=B.PROVE AND A.NUM=B.OFE"
sConsulta = sConsulta & vbCrLf & "                   INNER JOIN PROCE C ON A.ANYO=C.ANYO AND A.GMN1=C.GMN1 AND A.PROCE = C.COD"
sConsulta = sConsulta & vbCrLf & "              WHERE A.ANYO=@ANYO AND A.GMN1=@GMN1 AND A.PROCE=@PROCE AND A.ITEM=@ITEM AND A.ULT=1 AND A.PRECIO IS NOT NULL"
sConsulta = sConsulta & vbCrLf & "              ORDER BY A.PRECIO / (B.CAMBIO * C.CAMBIO) ASC"
sConsulta = sConsulta & vbCrLf & "               /*Si el nuevo precio calculado es m�s bajo y el proveedor es diferente del que antes ten�a el precio m�s bajo se actualiza OFEMIN*/"
sConsulta = sConsulta & vbCrLf & "              SELECT @PRECCOMP=PRECIO,@PROVECOMP=PROVE FROM OFEMIN WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM"
sConsulta = sConsulta & vbCrLf & "              IF @PROVECOMP IS NULL"
sConsulta = sConsulta & vbCrLf & "              /*Si el precio y prov. es nulo no notifica pero actualiza ofemin*/"
sConsulta = sConsulta & vbCrLf & "              BEGIN"
sConsulta = sConsulta & vbCrLf & "                  UPDATE OFEMIN SET PRECIO=@PRECIO,PROVE=@PROVE2,OFE=@OFE2 , CAMBIO=@CAMBIOMONCEN WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM"
sConsulta = sConsulta & vbCrLf & "                  if @PRECIO IS NOT NULL"
sConsulta = sConsulta & vbCrLf & "                      INSERT INTO DETALLE_PUJAS (ANYO,GMN1,PROCE,ITEM,ID,OFE,PROVE,FECHA,PRECIO) VALUES (@ANYO,@GMN1,@PROCE,@ITEM,1,@OFE2,@PROVE2,GETDATE(),@PRECIO)"
sConsulta = sConsulta & vbCrLf & "              End"
sConsulta = sConsulta & vbCrLf & "              ELSE IF  @PRECIO <= @PRECCOMP OR @OFEMIN>0"
sConsulta = sConsulta & vbCrLf & "              BEGIN"
sConsulta = sConsulta & vbCrLf & "                  IF @PROVECOMP <>@PROVE2 /*Si el proveedor es diferente habr� que notificarle que su oferta ha sido pisada*/"
sConsulta = sConsulta & vbCrLf & "                  BEGIN"
sConsulta = sConsulta & vbCrLf & "                      /*Obtenemos los datos del proveedor (Denominaci�n,persona de contacto y e-mail de la persona de contacto) para notificarle que su oferta"
sConsulta = sConsulta & vbCrLf & "                      ha sido pisada*/"
sConsulta = sConsulta & vbCrLf & "                      UPDATE PROCE_PROVE SET SUPERADA = 1 WHERE ANYO= @ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVECOMP"
sConsulta = sConsulta & vbCrLf & "                     SET CONCAT_NULL_YIELDS_NULL OFF"
sConsulta = sConsulta & vbCrLf & "            SELECT TOP 1 @EMAIL=CON.EMAIL,@PROVEDEN=PROVE.DEN,@CONTACTO = NOMCON + ' ' + APECON"
sConsulta = sConsulta & vbCrLf & "                      FROM PROVE,PROCE_PROVE_PET,CON WHERE PROVE.COD= PROCE_PROVE_PET.PROVE AND PROCE_PROVE_PET.PROVE= CON.PROVE AND"
sConsulta = sConsulta & vbCrLf & "                      PROCE_PROVE_PET.NOMCON=CON.NOM AND PROCE_PROVE_PET.APECON = CON.APE AND"
sConsulta = sConsulta & vbCrLf & "                      PROCE_PROVE_PET.TIPOPET=0 AND ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE.COD=@PROVECOMP ORDER BY PROCE_PROVE_PET.ID DESC"
sConsulta = sConsulta & vbCrLf & "                      SELECT @PROCEDEN=PROCE.DEN,@ITEMDEN=ITEM.DESCR + ART4.DEN FROM PROCE INNER JOIN ITEM ON PROCE.ANYO=ITEM.ANYO AND"
sConsulta = sConsulta & vbCrLf & "                      Proce.gmn1 = Item.gmn1 And Proce.COD = Item.Proce"
sConsulta = sConsulta & vbCrLf & "             LEFT JOIN ART4 ON ITEM.ART=ART4.COD AND ITEM.GMN1=ART4.GMN1 AND ITEM.GMN2=ART4.GMN2 AND ITEM.GMN3=ART4.GMN3 AND ITEM.GMN4=ART4.GMN4"
sConsulta = sConsulta & vbCrLf & "                      WHERE PROCE.ANYO=@ANYO AND PROCE.GMN1=@GMN1 AND PROCE.COD=@PROCE AND ITEM.ID=@ITEM"
sConsulta = sConsulta & vbCrLf & "                      SET CONCAT_NULL_YIELDS_NULL ON"
sConsulta = sConsulta & vbCrLf & "             /*Si la instalaci�n es de BuySite*/"
sConsulta = sConsulta & vbCrLf & "                      SELECT @INST=INSTWEB FROM PARGEN_INTERNO"
sConsulta = sConsulta & vbCrLf & "                      IF @INST = 1"
sConsulta = sConsulta & vbCrLf & "                      BEGIN"
sConsulta = sConsulta & vbCrLf & "                          /*Inserta en la tabla PROVENOTIF*/"
sConsulta = sConsulta & vbCrLf & "                          IF @EMAIL IS NULL OR @CONTACTO IS NULL  /*Si el e-mail o el contacto son nulos saca de contactos alguno que no sea nulo*/"
sConsulta = sConsulta & vbCrLf & "                          BEGIN"
sConsulta = sConsulta & vbCrLf & "                              SELECT TOP 1 @EMAIL_EX=CON.EMAIL,@CONTACTO_EX = NOM + ' ' + APE FROM CON"
sConsulta = sConsulta & vbCrLf & "                              WHERE CON.EMAIL IS NOT NULL AND NOM IS NOT NULL  AND PROVE=@PROVECOMP"
sConsulta = sConsulta & vbCrLf & "                            INSERT INTO PROVENOTIF (ANYO,GMN1,PROCE,PROCEDEN,ITEM,ITEMDEN,PROVE,PROVEDEN,EMAIL,CONTACTO) VALUES"
sConsulta = sConsulta & vbCrLf & "                              (@ANYO,@GMN1,@PROCE,@PROCEDEN,@ITEM,@ITEMDEN,@PROVE,@PROVEDEN,@EMAIL_EX,@CONTACTO_EX)"
sConsulta = sConsulta & vbCrLf & "                          End"
sConsulta = sConsulta & vbCrLf & "                          Else"
sConsulta = sConsulta & vbCrLf & "                              INSERT INTO PROVENOTIF (ANYO,GMN1,PROCE,PROCEDEN,ITEM,ITEMDEN,PROVE,PROVEDEN,EMAIL,CONTACTO) VALUES"
sConsulta = sConsulta & vbCrLf & "                              (@ANYO,@GMN1,@PROCE,@PROCEDEN,@ITEM,@ITEMDEN,@PROVECOMP,@PROVEDEN,@EMAIL,@CONTACTO)"
sConsulta = sConsulta & vbCrLf & "                      End"
sConsulta = sConsulta & vbCrLf & "                      ELSE /*Si la instalaci�n es de Portal*/"
sConsulta = sConsulta & vbCrLf & "                      BEGIN"
sConsulta = sConsulta & vbCrLf & "                          /*Obtenemos el servidor,BD y el identificador de la Cia del portal*/"
sConsulta = sConsulta & vbCrLf & "                          SELECT @SERVER = FSP_SRV, @BD = FSP_BD , @FSP_CIA = FSP_CIA FROM PARGEN_PORT"
sConsulta = sConsulta & vbCrLf & "                          /*Ejecutamos el store procedure de notificaci�n que se encuentra en el portal.*/"
sConsulta = sConsulta & vbCrLf & "                          SET @CONEX=@SERVER + '.' + @BD + '.dbo.SP_ACTUALIZAR_PEND_NOTIF'"
sConsulta = sConsulta & vbCrLf & "                          EXECUTE @CONEX @FSP_CIA,@ANYO,@GMN1,@PROCE,@ITEM,@PROVECOMP,@PROCEDEN,@ITEMDEN,@EMAIL,@PROVEDEN,@CONTACTO"
sConsulta = sConsulta & vbCrLf & "                      End"
sConsulta = sConsulta & vbCrLf & "                  End"
sConsulta = sConsulta & vbCrLf & "                  /*Actualizamos OFEMIN con el precio m�s bajo*/"
sConsulta = sConsulta & vbCrLf & "                  UPDATE OFEMIN SET PRECIO=@PRECIO,PROVE=@PROVE2,OFE=@OFE2  , CAMBIO=@CAMBIOMONCEN WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM"
sConsulta = sConsulta & vbCrLf & "                      Update PROCE_PROVE"
sConsulta = sConsulta & vbCrLf & "                              Set SUPERADA = 0"
sConsulta = sConsulta & vbCrLf & "                          FROM PROCE_PROVE A"
sConsulta = sConsulta & vbCrLf & "                       WHERE A.ANYO= @ANYO"
sConsulta = sConsulta & vbCrLf & "                             AND A.GMN1=@GMN1"
sConsulta = sConsulta & vbCrLf & "                             AND A.PROCE=@PROCE"
sConsulta = sConsulta & vbCrLf & "                             AND A.PROVE=@PROVE2"
sConsulta = sConsulta & vbCrLf & "                             AND NOT EXISTS (SELECT * FROM OFEMIN B WHERE A.ANYO=B.ANYO AND A.GMN1=B.GMN1 AND A.PROCE=B.PROCE AND A.PROVE=B.PROVE)"
sConsulta = sConsulta & vbCrLf & "                    /*Si no era la oferta m�nima borramos y sino insertamos en DETALLE_PUJAS*/"
sConsulta = sConsulta & vbCrLf & "                  IF @OFEMIN >0"
sConsulta = sConsulta & vbCrLf & "                      DELETE FROM DETALLE_PUJAS WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM AND ID=(SELECT MAX(ID) FROM DETALLE_PUJAS WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM)"
sConsulta = sConsulta & vbCrLf & "                  SET @IDENT = (SELECT MAX(ID) FROM DETALLE_PUJAS WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM )"
sConsulta = sConsulta & vbCrLf & "                  IF (SELECT COUNT(*) FROM DETALLE_PUJAS WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM AND PROVE=@PROVE2 AND OFE=@OFE2  AND ID=@IDENT)=0"
sConsulta = sConsulta & vbCrLf & "                  BEGIN"
sConsulta = sConsulta & vbCrLf & "                      IF @IDENT IS NULL"
sConsulta = sConsulta & vbCrLf & "                          SET @IDENT=1"
sConsulta = sConsulta & vbCrLf & "                      Else"
sConsulta = sConsulta & vbCrLf & "                          SET @IDENT= @IDENT+1"
sConsulta = sConsulta & vbCrLf & "                      if @PRECIO IS NOT NULL"
sConsulta = sConsulta & vbCrLf & "                              INSERT INTO DETALLE_PUJAS (ANYO,GMN1,PROCE,ITEM,ID,OFE,PROVE,FECHA,PRECIO) VALUES (@ANYO,@GMN1,@PROCE,@ITEM,@IDENT,@OFE2,@PROVE2,GETDATE(),@PRECIO)"
sConsulta = sConsulta & vbCrLf & "                  End"
sConsulta = sConsulta & vbCrLf & "              End"
sConsulta = sConsulta & vbCrLf & "          End"
sConsulta = sConsulta & vbCrLf & "      End"
sConsulta = sConsulta & vbCrLf & "  End"
sConsulta = sConsulta & vbCrLf & "  FETCH NEXT FROM curTG_ITEM_OFE_Upd INTO @ANYO,@GMN1,@PROCE,@ITEM,@PROVE, @NUM,@ULT"
sConsulta = sConsulta & vbCrLf & " End"
sConsulta = sConsulta & vbCrLf & " Close curTG_ITEM_OFE_Upd"
sConsulta = sConsulta & vbCrLf & " DEALLOCATE curTG_ITEM_OFE_Upd"

ModificarTriggerITEM_OFE = sConsulta


End Function


Private Function CrearTablaART4_ESP() As String
Dim sConsulta As String
    
sConsulta = "CREATE TABLE [dbo].[ART4_ESP] ("
sConsulta = sConsulta & vbCrLf & "[GMN1] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodGMN1) & ") NOT NULL ,"
sConsulta = sConsulta & vbCrLf & "[GMN2] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodGMN2) & ") NOT NULL ,"
sConsulta = sConsulta & vbCrLf & "[GMN3] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodGMN3) & ") NOT NULL ,"
sConsulta = sConsulta & vbCrLf & "[GMN4] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodGMN4) & ") NOT NULL ,"
sConsulta = sConsulta & vbCrLf & "[ART] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodART) & ") NOT NULL ,"
sConsulta = sConsulta & vbCrLf & "[ID] [smallint] NOT NULL ,"
sConsulta = sConsulta & vbCrLf & "[NOM] [varchar] (300) NOT NULL ,"
sConsulta = sConsulta & vbCrLf & "[COM] [varchar] (500) NULL ,"
sConsulta = sConsulta & vbCrLf & "[DATA] [image] NULL ,"
sConsulta = sConsulta & vbCrLf & "[FECACT] [datetime] NULL"
sConsulta = sConsulta & vbCrLf & ") ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE [dbo].[ART4_ESP] WITH NOCHECK ADD"
sConsulta = sConsulta & vbCrLf & "CONSTRAINT [PK_ART4_ESP] PRIMARY KEY  CLUSTERED"
sConsulta = sConsulta & vbCrLf & "("
sConsulta = sConsulta & vbCrLf & "[GMN1],"
sConsulta = sConsulta & vbCrLf & "[GMN2],"
sConsulta = sConsulta & vbCrLf & "[GMN3],"
sConsulta = sConsulta & vbCrLf & "[GMN4],"
sConsulta = sConsulta & vbCrLf & "[ART],"
sConsulta = sConsulta & vbCrLf & "[ID]"
sConsulta = sConsulta & vbCrLf & ")  ON [PRIMARY]"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE [dbo].[ART4_ESP] ADD"
sConsulta = sConsulta & vbCrLf & "CONSTRAINT [FK_ART4_ESP_ART4] FOREIGN KEY"
sConsulta = sConsulta & vbCrLf & "("
sConsulta = sConsulta & vbCrLf & "[GMN1],"
sConsulta = sConsulta & vbCrLf & "[GMN2],"
sConsulta = sConsulta & vbCrLf & "[GMN3],"
sConsulta = sConsulta & vbCrLf & "[GMN4],"
sConsulta = sConsulta & vbCrLf & "[ART]"
sConsulta = sConsulta & vbCrLf & ") REFERENCES [dbo].[ART4] ("
sConsulta = sConsulta & vbCrLf & "[GMN1],"
sConsulta = sConsulta & vbCrLf & "[GMN2],"
sConsulta = sConsulta & vbCrLf & "[GMN3],"
sConsulta = sConsulta & vbCrLf & "[GMN4],"
sConsulta = sConsulta & vbCrLf & "[COD]"
sConsulta = sConsulta & vbCrLf & ")"

CrearTablaART4_ESP = sConsulta

End Function

Private Function CrearTriggerART4_ESP_TG_INSUPD() As String
Dim sConsulta As String

sConsulta = "CREATE TRIGGER ART4_ESP_TG_INSUPD ON dbo.ART4_ESP"
sConsulta = sConsulta & vbCrLf & "FOR INSERT, UPDATE"
sConsulta = sConsulta & vbCrLf & "AS"
sConsulta = sConsulta & vbCrLf & "DECLARE @GMN1 VARCHAR(50),@GMN2 VARCHAR(50),@GMN3 VARCHAR(50),@GMN4 VARCHAR(50),@ART VARCHAR(50),@ID AS INT"
sConsulta = sConsulta & vbCrLf & "DECLARE curTG_ART4_ESP_Ins CURSOR FOR SELECT GMN1,GMN2,GMN3,GMN4,ART,ID FROM INSERTED"
sConsulta = sConsulta & vbCrLf & "OPEN curTG_ART4_ESP_Ins"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM curTG_ART4_ESP_Ins INTO @GMN1,@GMN2,@GMN3,@GMN4,@ART,@ID"
sConsulta = sConsulta & vbCrLf & "WHILE @@FETCH_STATUS=0"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "UPDATE ART4_ESP SET FECACT=GETDATE() WHERE GMN1=@GMN1  AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@GMN4 AND ART=@ART AND ID=@ID"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM curTG_ART4_ESP_Ins INTO @GMN1,@GMN2,@GMN3,@GMN4,@ART,@ID"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "Close curTG_ART4_ESP_Ins"
sConsulta = sConsulta & vbCrLf & "DEALLOCATE curTG_ART4_ESP_Ins"

CrearTriggerART4_ESP_TG_INSUPD = sConsulta
End Function

Private Function ModificarART4_COD_2_7_50() As String
Dim sConsulta As String

sConsulta = "CREATE PROCEDURE ART4_COD (@OLD VARCHAR(50),@NEW VARCHAR(50),@GMN1 VARCHAR(50),@GMN2 VARCHAR(50),@GMN3 VARCHAR(50),@GMN4 VARCHAR(50))  AS"
sConsulta = sConsulta & vbCrLf & "DECLARE @VAR_COD VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "SET XACT_ABORT ON"
sConsulta = sConsulta & vbCrLf & "BEGIN TRANSACTION"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "DECLARE C CURSOR FOR SELECT COD FROM ART4 WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@GMN4 AND COD=@OLD"
sConsulta = sConsulta & vbCrLf & "OPEN C"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM C INTO @VAR_COD"
sConsulta = sConsulta & vbCrLf & "IF @@FETCH_STATUS=0"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4_ADJ NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4_ESP NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "UPDATE ART4 SET COD=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@GMN4 AND COD=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART4_ADJ SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@GMN4 AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@GMN4 AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@GMN4 AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@GMN4 AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_UON1 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@GMN4 AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_UON2 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@GMN4 AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_UON3 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@GMN4 AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_ART4 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@GMN4 AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART4_ESP SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@GMN4 AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4_ADJ CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4_ESP CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "Close C"
sConsulta = sConsulta & vbCrLf & "DEALLOCATE C"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "COMMIT TRANSACTION"

ModificarART4_COD_2_7_50 = sConsulta
End Function


Private Function ModificarGMN2_COD_2_7_50() As String
Dim sConsulta As String

sConsulta = "CREATE PROCEDURE GMN2_COD (@OLD VARCHAR(50),@NEW VARCHAR(50),@GMN1 VARCHAR(50))  AS"
sConsulta = sConsulta & vbCrLf & "DECLARE @VAR_COD VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "SET XACT_ABORT ON"
sConsulta = sConsulta & vbCrLf & "BEGIN TRANSACTION"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "DECLARE C CURSOR FOR SELECT COD FROM GMN2 WHERE GMN1=@GMN1 AND COD=@OLD"
sConsulta = sConsulta & vbCrLf & "OPEN C"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM C INTO @VAR_COD"
sConsulta = sConsulta & vbCrLf & "IF @@FETCH_STATUS=0"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART2_ADJ NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART3_ADJ NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4_ADJ NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE COM_GMN2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE COM_GMN3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE COM_GMN4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_MAT2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_MAT3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_MAT4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_PROCE NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4_ESP NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "UPDATE GMN2 SET COD=@NEW WHERE GMN1=@GMN1 AND COD=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART2 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART2_ADJ SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART3 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART3_ADJ SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART4 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART4_ADJ SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE COM_GMN2 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE COM_GMN3 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE COM_GMN4 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE GMN3 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE GMN4 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_MAT2 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_MAT3 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_MAT4 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_GMN2 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_GMN3 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_GMN4 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_PROCE SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_UON1 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_UON2 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_UON3 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON1 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON2 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON3 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON4 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY1 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY2 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY3 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY4 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_ART2 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_ART3 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_ART4 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART4_ESP SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART2_ADJ CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART3_ADJ CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4_ADJ CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE COM_GMN2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE COM_GMN3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE COM_GMN4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_MAT2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_MAT3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_MAT4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_PROCE CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4_ESP CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "Close C"
sConsulta = sConsulta & vbCrLf & "DEALLOCATE C"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "COMMIT TRANSACTION"

ModificarGMN2_COD_2_7_50 = sConsulta

End Function


Private Function ModificarGMN3_COD_2_7_50() As String
Dim sConsulta As String

sConsulta = "CREATE PROCEDURE GMN3_COD (@OLD VARCHAR(50),@NEW VARCHAR(50),@GMN1 VARCHAR(50),@GMN2 VARCHAR(50))  AS"
sConsulta = sConsulta & vbCrLf & "DECLARE @VAR_COD VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "SET XACT_ABORT ON"
sConsulta = sConsulta & vbCrLf & "BEGIN TRANSACTION"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "DECLARE C CURSOR FOR SELECT COD FROM GMN3 WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND COD=@OLD"
sConsulta = sConsulta & vbCrLf & "OPEN C"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM C INTO @VAR_COD"
sConsulta = sConsulta & vbCrLf & "IF @@FETCH_STATUS=0"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART3_ADJ NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4_ADJ NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE COM_GMN3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE COM_GMN4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_MAT3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_MAT4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_PROCE NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4_ESP NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "UPDATE GMN3 SET COD=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND COD=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART3 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART3_ADJ SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART4 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART4_ADJ SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE COM_GMN3 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE COM_GMN4 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE GMN4 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_MAT3 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_MAT4 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_GMN3 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_GMN4 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_PROCE SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_UON1 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_UON2 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_UON3 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON3 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON4 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY3 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY4 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_ART3 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_ART4 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART4_ESP SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART3_ADJ CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4_ADJ CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE COM_GMN3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE COM_GMN4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_MAT3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_MAT4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_PROCE CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4_ESP CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "Close C"
sConsulta = sConsulta & vbCrLf & "DEALLOCATE C"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "COMMIT TRANSACTION"


ModificarGMN3_COD_2_7_50 = sConsulta
End Function

Private Function ModificarGMN4_COD_2_7_50() As String
Dim sConsulta As String

sConsulta = "CREATE PROCEDURE GMN4_COD (@OLD VARCHAR(50),@NEW VARCHAR(50),@GMN1 VARCHAR(50),@GMN2 VARCHAR(50),@GMN3 VARCHAR(50))  AS"
sConsulta = sConsulta & vbCrLf & "DECLARE @VAR_COD VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "SET XACT_ABORT ON"
sConsulta = sConsulta & vbCrLf & "BEGIN TRANSACTION"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "DECLARE C CURSOR FOR SELECT COD FROM GMN4 WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND COD=@OLD"
sConsulta = sConsulta & vbCrLf & "OPEN C"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM C INTO @VAR_COD"
sConsulta = sConsulta & vbCrLf & "IF @@FETCH_STATUS=0"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4_ADJ NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE COM_GMN4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_MAT4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_PROCE NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4_ESP NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "UPDATE GMN4 SET COD=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND COD=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART4 SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART4_ADJ SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE COM_GMN4 SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_MAT4 SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_GMN4 SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_PROCE SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_UON1 SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_UON2 SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_UON3 SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON1 SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON2 SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON3 SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON4 SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY1 SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY2 SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY3 SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY4 SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_ART4 SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART4_ESP  SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4_ADJ CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE COM_GMN4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_MAT4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_PROCE CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4_ESP CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "Close C"
sConsulta = sConsulta & vbCrLf & "DEALLOCATE C"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "COMMIT TRANSACTION"

ModificarGMN4_COD_2_7_50 = sConsulta
End Function

Private Function CrearTablaPRES_3_CON1() As String
Dim sConsulta As String

sConsulta = "CREATE TABLE [dbo].[PRES_3_CON1] ("
sConsulta = sConsulta & vbCrLf & "[COD] [varchar] (15) NOT NULL ,"
sConsulta = sConsulta & vbCrLf & "[DEN] [varchar] (100) NOT NULL ,"
sConsulta = sConsulta & vbCrLf & "[IMP] [float] NULL ,"
sConsulta = sConsulta & vbCrLf & "[OBJ] [float] NULL ,"
sConsulta = sConsulta & vbCrLf & "[FECACT] [datetime] NULL"
sConsulta = sConsulta & vbCrLf & ") ON [PRIMARY]"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE [dbo].[PRES_3_CON1] WITH NOCHECK ADD"
sConsulta = sConsulta & vbCrLf & "CONSTRAINT [PK_PRES_3_CON1] PRIMARY KEY  NONCLUSTERED"
sConsulta = sConsulta & vbCrLf & "("
sConsulta = sConsulta & vbCrLf & "[COD]"
sConsulta = sConsulta & vbCrLf & ")  ON [PRIMARY]"

CrearTablaPRES_3_CON1 = sConsulta
End Function

Private Function CrearTriggerPRES_3_CON1_TG_INSUPD() As String
Dim sConsulta As String

sConsulta = "CREATE TRIGGER PRES_3_CON1_TG_INSUPD ON dbo.PRES_3_CON1"
sConsulta = sConsulta & vbCrLf & "FOR INSERT,UPDATE"
sConsulta = sConsulta & vbCrLf & "AS"
sConsulta = sConsulta & vbCrLf & "DECLARE @COD VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "DECLARE curTG_PRES_3_CON1_Ins CURSOR FOR SELECT COD FROM INSERTED"
sConsulta = sConsulta & vbCrLf & "OPEN curTG_PRES_3_CON1_Ins"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM curTG_PRES_3_CON1_Ins INTO @COD"
sConsulta = sConsulta & vbCrLf & "WHILE @@FETCH_STATUS=0"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_3_CON1 SET FECACT=GETDATE() WHERE COD=@COD"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM curTG_PRES_3_CON1_Ins INTO @COD"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "Close curTG_PRES_3_CON1_Ins"
sConsulta = sConsulta & vbCrLf & "DEALLOCATE curTG_PRES_3_CON1_Ins"

CrearTriggerPRES_3_CON1_TG_INSUPD = sConsulta
End Function


Private Function CrearPRES_3_CON1_COD() As String
Dim sConsulta As String

sConsulta = "CREATE PROCEDURE PRES_3_CON1_COD (@OLD VARCHAR(50),@NEW VARCHAR(50))  AS"
sConsulta = sConsulta & vbCrLf & "DECLARE @VAR_COD VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "SET XACT_ABORT ON"
sConsulta = sConsulta & vbCrLf & "BEGIN TRANSACTION"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "DECLARE C CURSOR FOR SELECT COD FROM PRES_3_CON1 WHERE  COD=@OLD"
sConsulta = sConsulta & vbCrLf & "OPEN C"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM C INTO @VAR_COD"
sConsulta = sConsulta & vbCrLf & "IF @@FETCH_STATUS=0"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_3_CON1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_3_CON2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_3_CON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_3_CON4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_3_CON1 SET COD=@NEW WHERE  COD=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_3_CON2 SET PRES1=@NEW WHERE PRES1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_3_CON3 SET PRES1=@NEW WHERE  PRES1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_3_CON4 SET PRES1=@NEW WHERE  PRES1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_PRESCON3 SET PRES1=@NEW WHERE  PRES1=@OLD"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_3_CON1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_3_CON2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_3_CON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_3_CON4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "Close C"
sConsulta = sConsulta & vbCrLf & "DEALLOCATE C"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "COMMIT TRANSACTION"

CrearPRES_3_CON1_COD = sConsulta
End Function


Private Function CrearTablaPRES_3_CON2() As String
Dim sConsulta As String

sConsulta = "CREATE TABLE [dbo].[PRES_3_CON2] ("
sConsulta = sConsulta & vbCrLf & "[PRES1] [varchar] (15) NOT NULL ,"
sConsulta = sConsulta & vbCrLf & "[COD] [varchar] (15) NOT NULL ,"
sConsulta = sConsulta & vbCrLf & "[DEN] [varchar] (100) NOT NULL ,"
sConsulta = sConsulta & vbCrLf & "[IMP] [float] NULL ,"
sConsulta = sConsulta & vbCrLf & "[OBJ] [float] NULL ,"
sConsulta = sConsulta & vbCrLf & "[FECACT] [datetime] NULL"
sConsulta = sConsulta & vbCrLf & ") ON [PRIMARY]"

sConsulta = sConsulta & vbCrLf & "ALTER TABLE [dbo].[PRES_3_CON2] WITH NOCHECK ADD"
sConsulta = sConsulta & vbCrLf & "CONSTRAINT [PK_PRES_3_CON2] PRIMARY KEY  NONCLUSTERED"
sConsulta = sConsulta & vbCrLf & "("
sConsulta = sConsulta & vbCrLf & "[PRES1],"
sConsulta = sConsulta & vbCrLf & "[COD]"
sConsulta = sConsulta & vbCrLf & ")  ON [PRIMARY]"

sConsulta = sConsulta & vbCrLf & "ALTER TABLE [dbo].[PRES_3_CON2] ADD"
sConsulta = sConsulta & vbCrLf & "CONSTRAINT [FK_PRES_3_CON2_PRES_3_CON1] FOREIGN KEY"
sConsulta = sConsulta & vbCrLf & "("
sConsulta = sConsulta & vbCrLf & "[PRES1]"
sConsulta = sConsulta & vbCrLf & ") REFERENCES [dbo].[PRES_3_CON1] ("
sConsulta = sConsulta & vbCrLf & "[COD]"
sConsulta = sConsulta & vbCrLf & ")"

CrearTablaPRES_3_CON2 = sConsulta
End Function


Private Function CrearTriggerPRES_3_CON2_TG_INSUPD() As String
Dim sConsulta As String

sConsulta = "CREATE TRIGGER PRES_3_CON2_TG_INSUPD ON dbo.PRES_3_CON2"
sConsulta = sConsulta & vbCrLf & "FOR INSERT, UPDATE"
sConsulta = sConsulta & vbCrLf & "AS"
sConsulta = sConsulta & vbCrLf & "DECLARE @PRES1 VARCHAR(50), @COD VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "DECLARE curTG_PRES_3_CON2_Ins CURSOR FOR SELECT PRES1,COD FROM INSERTED"
sConsulta = sConsulta & vbCrLf & "OPEN curTG_PRES_3_CON2_Ins"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM curTG_PRES_3_CON2_Ins INTO @PRES1, @COD"
sConsulta = sConsulta & vbCrLf & "WHILE @@FETCH_STATUS=0"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_3_CON2 SET FECACT=GETDATE() WHERE PRES1=@PRES1 AND COD=@COD"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM curTG_PRES_3_CON2_Ins INTO @PRES1,@COD"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "Close curTG_PRES_3_CON2_Ins"
sConsulta = sConsulta & vbCrLf & "DEALLOCATE curTG_PRES_3_CON2_Ins"

CrearTriggerPRES_3_CON2_TG_INSUPD = sConsulta
End Function


Private Function CrearPRES_3_CON2_COD() As String
Dim sConsulta As String

sConsulta = "CREATE PROCEDURE PRES_3_CON2_COD (@OLD VARCHAR(50),@NEW VARCHAR(50),@PRES1 VARCHAR(50))  AS"
sConsulta = sConsulta & vbCrLf & "DECLARE @VAR_COD VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "SET XACT_ABORT ON"
sConsulta = sConsulta & vbCrLf & "BEGIN TRANSACTION"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "DECLARE C CURSOR FOR SELECT COD FROM PRES_3_CON2 WHERE  PRES1=@PRES1 AND COD=@OLD"
sConsulta = sConsulta & vbCrLf & "OPEN C"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM C INTO @VAR_COD"
sConsulta = sConsulta & vbCrLf & "IF @@FETCH_STATUS=0"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_3_CON1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_3_CON2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_3_CON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_3_CON4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_3_CON1 SET COD=@NEW WHERE  COD=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_3_CON2 SET COD=@NEW WHERE  PRES1=@PRES1 AND COD=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_3_CON3 SET PRES2=@NEW WHERE  PRES1=@PRES1 AND PRES2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_3_CON4 SET PRES2=@NEW WHERE  PRES1=@PRES1 AND PRES2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_PRESCON3 SET PRES2=@NEW WHERE PRES1=@PRES1 AND PRES2=@OLD"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_3_CON1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_3_CON2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_3_CON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_3_CON4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "Close C"
sConsulta = sConsulta & vbCrLf & "DEALLOCATE C"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "COMMIT TRANSACTION"

CrearPRES_3_CON2_COD = sConsulta

End Function


Private Function CrearTablaPPRES_3_CON3() As String
Dim sConsulta As String

sConsulta = "CREATE TABLE [dbo].[PRES_3_CON3] ("
sConsulta = sConsulta & vbCrLf & "[PRES1] [varchar] (15) NOT NULL ,"
sConsulta = sConsulta & vbCrLf & "[PRES2] [varchar] (15) NOT NULL ,"
sConsulta = sConsulta & vbCrLf & "[COD] [varchar] (15) NOT NULL ,"
sConsulta = sConsulta & vbCrLf & "[DEN] [varchar] (100) NOT NULL ,"
sConsulta = sConsulta & vbCrLf & "[IMP] [float] NULL ,"
sConsulta = sConsulta & vbCrLf & "[OBJ] [float] NULL ,"
sConsulta = sConsulta & vbCrLf & "[FECACT] [datetime] NULL"
sConsulta = sConsulta & vbCrLf & ") ON [PRIMARY]"

sConsulta = sConsulta & vbCrLf & "ALTER TABLE [dbo].[PRES_3_CON3] WITH NOCHECK ADD"
sConsulta = sConsulta & vbCrLf & "CONSTRAINT [PK_PRES_3_CON3] PRIMARY KEY  NONCLUSTERED"
sConsulta = sConsulta & vbCrLf & "("
sConsulta = sConsulta & vbCrLf & "[PRES1],"
sConsulta = sConsulta & vbCrLf & "[PRES2],"
sConsulta = sConsulta & vbCrLf & "[COD]"
sConsulta = sConsulta & vbCrLf & ")  ON [PRIMARY]"

sConsulta = sConsulta & vbCrLf & "ALTER TABLE [dbo].[PRES_3_CON3] ADD"
sConsulta = sConsulta & vbCrLf & "CONSTRAINT [FK_PRES_3_CON3_PRES_3_CON2] FOREIGN KEY"
sConsulta = sConsulta & vbCrLf & "("
sConsulta = sConsulta & vbCrLf & "[PRES1],"
sConsulta = sConsulta & vbCrLf & "[PRES2]"
sConsulta = sConsulta & vbCrLf & ") REFERENCES [dbo].[PRES_3_CON2] ("
sConsulta = sConsulta & vbCrLf & "[PRES1],"
sConsulta = sConsulta & vbCrLf & "[COD]"
sConsulta = sConsulta & vbCrLf & ")"

CrearTablaPPRES_3_CON3 = sConsulta

End Function


Private Function CrearTriggerPRES_3_CON3_TG_INSUPD() As String

Dim sConsulta As String

sConsulta = "CREATE TRIGGER PRES_3_CON3_TG_INSUPD ON dbo.PRES_3_CON3"
sConsulta = sConsulta & vbCrLf & "FOR INSERT, UPDATE"
sConsulta = sConsulta & vbCrLf & "AS"
sConsulta = sConsulta & vbCrLf & "DECLARE @PRES1 VARCHAR(50), @PRES2 VARCHAR(50), @COD VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "DECLARE curTG_PRES_3_CON3_Ins CURSOR FOR SELECT PRES1, PRES2, COD FROM INSERTED"
sConsulta = sConsulta & vbCrLf & "OPEN curTG_PRES_3_CON3_Ins"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM curTG_PRES_3_CON3_Ins INTO @PRES1, @PRES2, @COD"
sConsulta = sConsulta & vbCrLf & "WHILE @@FETCH_STATUS=0"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_3_CON3 SET FECACT=GETDATE() WHERE  PRES1=@PRES1 AND PRES2=@PRES2 AND COD=@COD"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM curTG_PRES_3_CON3_Ins INTO @PRES1, @PRES2, @COD"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "Close curTG_PRES_3_CON3_Ins"
sConsulta = sConsulta & vbCrLf & "DEALLOCATE curTG_PRES_3_CON3_Ins"

CrearTriggerPRES_3_CON3_TG_INSUPD = sConsulta
End Function


Private Function CrearPRES_3_CON3_COD() As String
Dim sConsulta As String

sConsulta = "CREATE PROCEDURE PRES_3_CON3_COD (@OLD VARCHAR(50),@NEW VARCHAR(50),@PRES1 VARCHAR(50),@PRES2 VARCHAR(50))  AS"
sConsulta = sConsulta & vbCrLf & "DECLARE @VAR_COD VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "SET XACT_ABORT ON"
sConsulta = sConsulta & vbCrLf & "BEGIN TRANSACTION"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "DECLARE C CURSOR FOR SELECT COD FROM PRES_3_CON3 WHERE  PRES1=@PRES1 AND PRES2=@PRES2 AND COD=@OLD"
sConsulta = sConsulta & vbCrLf & "OPEN C"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM C INTO @VAR_COD"
sConsulta = sConsulta & vbCrLf & "IF @@FETCH_STATUS=0"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_3_CON1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_3_CON2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_3_CON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_3_CON4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_3_CON1 SET COD=@NEW WHERE  COD=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_3_CON2 SET COD=@NEW WHERE  PRES1=@PRES1 AND COD=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_3_CON3 SET COD=@NEW WHERE  PRES1=@PRES1 AND PRES2=@PRES2 AND COD=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_3_CON4 SET PRES3=@NEW WHERE PRES1=@PRES1 AND PRES2=@PRES2 AND PRES3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_PRESCON3 SET PRES3=@NEW WHERE  PRES1=@PRES1 AND PRES2=@PRES2 AND PRES3=@OLD"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_3_CON1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_3_CON2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_3_CON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_3_CON4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "Close C"
sConsulta = sConsulta & vbCrLf & "DEALLOCATE C"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "COMMIT TRANSACTION"

CrearPRES_3_CON3_COD = sConsulta

End Function


Private Function CrearTablaPRES_3_CON4() As String
Dim sConsulta As String

sConsulta = "CREATE TABLE [dbo].[PRES_3_CON4] ("
sConsulta = sConsulta & vbCrLf & "[PRES1] [varchar] (15) NOT NULL ,"
sConsulta = sConsulta & vbCrLf & "[PRES2] [varchar] (15) NOT NULL ,"
sConsulta = sConsulta & vbCrLf & "[PRES3] [varchar] (15) NOT NULL ,"
sConsulta = sConsulta & vbCrLf & "[COD] [varchar] (15) NOT NULL ,"
sConsulta = sConsulta & vbCrLf & "[DEN] [varchar] (100) NOT NULL ,"
sConsulta = sConsulta & vbCrLf & "[IMP] [float] NULL ,"
sConsulta = sConsulta & vbCrLf & "[OBJ] [float] NULL ,"
sConsulta = sConsulta & vbCrLf & "[FECACT] [datetime] NULL"
sConsulta = sConsulta & vbCrLf & ") ON [PRIMARY]"

sConsulta = sConsulta & vbCrLf & "ALTER TABLE [dbo].[PRES_3_CON4] WITH NOCHECK ADD"
sConsulta = sConsulta & vbCrLf & "CONSTRAINT [PK_PRES_3_CON4] PRIMARY KEY  NONCLUSTERED"
sConsulta = sConsulta & vbCrLf & "("
sConsulta = sConsulta & vbCrLf & "[PRES1],"
sConsulta = sConsulta & vbCrLf & "[PRES2],"
sConsulta = sConsulta & vbCrLf & "[PRES3],"
sConsulta = sConsulta & vbCrLf & "[COD]"
sConsulta = sConsulta & vbCrLf & ")  ON [PRIMARY]"

sConsulta = sConsulta & vbCrLf & "ALTER TABLE [dbo].[PRES_3_CON4] ADD"
sConsulta = sConsulta & vbCrLf & "CONSTRAINT [FK_PRES_3_CON4_PRES_3_CON3] FOREIGN KEY"
sConsulta = sConsulta & vbCrLf & "("
sConsulta = sConsulta & vbCrLf & "[PRES1],"
sConsulta = sConsulta & vbCrLf & "[PRES2],"
sConsulta = sConsulta & vbCrLf & "[PRES3]"
sConsulta = sConsulta & vbCrLf & ") REFERENCES [dbo].[PRES_3_CON3] ("
sConsulta = sConsulta & vbCrLf & "[PRES1],"
sConsulta = sConsulta & vbCrLf & "[PRES2],"
sConsulta = sConsulta & vbCrLf & "[COD]"
sConsulta = sConsulta & vbCrLf & ")"

CrearTablaPRES_3_CON4 = sConsulta
End Function


Private Function CrearTriggerPRES_3_CON4_TG_INSUPD() As String
Dim sConsulta As String

sConsulta = "CREATE TRIGGER PRES_3_CON4_TG_INSUPD ON dbo.PRES_3_CON4"
sConsulta = sConsulta & vbCrLf & "FOR INSERT, UPDATE"
sConsulta = sConsulta & vbCrLf & "AS"
sConsulta = sConsulta & vbCrLf & "DECLARE @PRES1 VARCHAR(50), @PRES2 VARCHAR(50), @PRES3 VARCHAR(50), @COD VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "DECLARE curTG_PRES_3_CON4_Ins CURSOR FOR SELECT PRES1, PRES2, PRES3, COD FROM INSERTED"
sConsulta = sConsulta & vbCrLf & "OPEN curTG_PRES_3_CON4_Ins"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM curTG_PRES_3_CON4_Ins INTO @PRES1, @PRES2, @PRES3, @COD"
sConsulta = sConsulta & vbCrLf & "WHILE @@FETCH_STATUS=0"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_3_CON4 SET FECACT=GETDATE() WHERE  PRES1=@PRES1 AND @PRES2=@PRES2 AND PRES3=@PRES3 AND COD=@COD"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM curTG_PRES_3_CON4_Ins INTO @PRES1,@PRES2, @PRES3, @COD"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "Close curTG_PRES_3_CON4_Ins"
sConsulta = sConsulta & vbCrLf & "DEALLOCATE curTG_PRES_3_CON4_Ins"

CrearTriggerPRES_3_CON4_TG_INSUPD = sConsulta
End Function


Private Function CrearPRES_3_CON4_COD() As String
Dim sConsulta As String

sConsulta = "CREATE PROCEDURE PRES_3_CON4_COD (@OLD VARCHAR(50),@NEW VARCHAR(50),@PRES1 VARCHAR(50),@PRES2 VARCHAR(50),@PRES3 VARCHAR(50))  AS"
sConsulta = sConsulta & vbCrLf & "DECLARE @VAR_COD VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "SET XACT_ABORT ON"
sConsulta = sConsulta & vbCrLf & "BEGIN TRANSACTION"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "DECLARE C CURSOR FOR SELECT COD FROM PRES_3_CON4 WHERE PRES1=@PRES1 AND PRES2=@PRES2 AND PRES3=@PRES3 AND COD=@OLD"
sConsulta = sConsulta & vbCrLf & "OPEN C"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM C INTO @VAR_COD"
sConsulta = sConsulta & vbCrLf & "IF @@FETCH_STATUS=0"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_3_CON1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_3_CON2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_3_CON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_3_CON4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_3_CON1 SET COD=@NEW WHERE COD=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_3_CON2 SET COD=@NEW WHERE PRES1=@PRES1 AND COD=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_3_CON3 SET COD=@NEW WHERE PRES1=@PRES1 AND PRES2=@PRES2  AND COD=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_3_CON4 SET COD=@NEW WHERE PRES1=@PRES1 AND PRES2=@PRES2 AND PRES3=@PRES3 AND COD=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_PRESCON3 SET PRES4=@NEW WHERE PRES1=@PRES1 AND PRES2=@PRES2 AND PRES3=@PRES3 AND PRES4=@OLD"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_3_CON1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_3_CON2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_3_CON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_3_CON4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "Close C"
sConsulta = sConsulta & vbCrLf & "DEALLOCATE C"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "COMMIT TRANSACTION"

CrearPRES_3_CON4_COD = sConsulta

End Function

Private Function CrearTablaPRES_4_CON1() As String
Dim sConsulta As String

sConsulta = "CREATE TABLE [dbo].[PRES_4_CON1] ("
sConsulta = sConsulta & vbCrLf & "[COD] [varchar] (15) NOT NULL ,"
sConsulta = sConsulta & vbCrLf & "[DEN] [varchar] (100) NOT NULL ,"
sConsulta = sConsulta & vbCrLf & "[IMP] [float] NULL ,"
sConsulta = sConsulta & vbCrLf & "[OBJ] [float] NULL ,"
sConsulta = sConsulta & vbCrLf & "[FECACT] [datetime] NULL"
sConsulta = sConsulta & vbCrLf & ") ON [PRIMARY]"

sConsulta = sConsulta & vbCrLf & "ALTER TABLE [dbo].[PRES_4_CON1] WITH NOCHECK ADD"
sConsulta = sConsulta & vbCrLf & "CONSTRAINT [PK_PRES_4_CON1] PRIMARY KEY  NONCLUSTERED"
sConsulta = sConsulta & vbCrLf & "("
sConsulta = sConsulta & vbCrLf & "[COD]"
sConsulta = sConsulta & vbCrLf & ")  ON [PRIMARY]"

CrearTablaPRES_4_CON1 = sConsulta
End Function


Private Function CrearTriggerPRES_4_CON1_TG_INSUPD() As String
Dim sConsulta As String

sConsulta = "CREATE TRIGGER PRES_4_CON1_TG_INSUPD ON dbo.PRES_4_CON1"
sConsulta = sConsulta & vbCrLf & "FOR INSERT, UPDATE"
sConsulta = sConsulta & vbCrLf & "AS"
sConsulta = sConsulta & vbCrLf & "DECLARE @COD VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "DECLARE curTG_PRES_4_CON1_Ins CURSOR FOR SELECT COD FROM INSERTED"
sConsulta = sConsulta & vbCrLf & "OPEN curTG_PRES_4_CON1_Ins"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM curTG_PRES_4_CON1_Ins INTO @COD"
sConsulta = sConsulta & vbCrLf & "WHILE @@FETCH_STATUS=0"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_4_CON1 SET FECACT=GETDATE() WHERE  COD=@COD"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM curTG_PRES_4_CON1_Ins INTO @COD"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "Close curTG_PRES_4_CON1_Ins"
sConsulta = sConsulta & vbCrLf & "DEALLOCATE curTG_PRES_4_CON1_Ins"

CrearTriggerPRES_4_CON1_TG_INSUPD = sConsulta

End Function


Private Function CrearPRES_4_CON1_COD() As String
Dim sConsulta As String

sConsulta = "CREATE PROCEDURE PRES_4_CON1_COD (@OLD VARCHAR(50),@NEW VARCHAR(50))  AS"
sConsulta = sConsulta & vbCrLf & "DECLARE @VAR_COD VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "SET XACT_ABORT ON"
sConsulta = sConsulta & vbCrLf & "BEGIN TRANSACTION"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "DECLARE C CURSOR FOR SELECT COD FROM PRES_4_CON1 WHERE  COD=@OLD"
sConsulta = sConsulta & vbCrLf & "OPEN C"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM C INTO @VAR_COD"
sConsulta = sConsulta & vbCrLf & "IF @@FETCH_STATUS=0"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_4_CON1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_4_CON2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_4_CON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_4_CON4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_4_CON1 SET COD=@NEW WHERE  COD=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_4_CON2 SET PRES1=@NEW WHERE PRES1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_4_CON3 SET PRES1=@NEW WHERE  PRES1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_4_CON4 SET PRES1=@NEW WHERE  PRES1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_PRESCON4 SET PRES1=@NEW WHERE  PRES1=@OLD"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_4_CON1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_4_CON2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_4_CON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_4_CON4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "Close C"
sConsulta = sConsulta & vbCrLf & "DEALLOCATE C"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "COMMIT TRANSACTION"

CrearPRES_4_CON1_COD = sConsulta
End Function


Private Function CrearTablaPRES_4_CON2() As String
Dim sConsulta As String

sConsulta = "CREATE TABLE [dbo].[PRES_4_CON2] ("
sConsulta = sConsulta & vbCrLf & "[PRES1] [varchar] (15) NOT NULL ,"
sConsulta = sConsulta & vbCrLf & "[COD] [varchar] (15) NOT NULL ,"
sConsulta = sConsulta & vbCrLf & "[DEN] [varchar] (100) NOT NULL ,"
sConsulta = sConsulta & vbCrLf & "[IMP] [float] NULL ,"
sConsulta = sConsulta & vbCrLf & "[OBJ] [float] NULL ,"
sConsulta = sConsulta & vbCrLf & "[FECACT] [datetime] NULL"
sConsulta = sConsulta & vbCrLf & ") ON [PRIMARY]"

sConsulta = sConsulta & vbCrLf & "ALTER TABLE [dbo].[PRES_4_CON2] WITH NOCHECK ADD"
sConsulta = sConsulta & vbCrLf & "CONSTRAINT [PK_PRES_4_CON2] PRIMARY KEY  NONCLUSTERED"
sConsulta = sConsulta & vbCrLf & "("
sConsulta = sConsulta & vbCrLf & "[PRES1],"
sConsulta = sConsulta & vbCrLf & "[COD]"
sConsulta = sConsulta & vbCrLf & ")  ON [PRIMARY]"

sConsulta = sConsulta & vbCrLf & "ALTER TABLE [dbo].[PRES_4_CON2] ADD"
sConsulta = sConsulta & vbCrLf & "CONSTRAINT [FK_PRES_4_CON2_PRES_4_CON1] FOREIGN KEY"
sConsulta = sConsulta & vbCrLf & "("
sConsulta = sConsulta & vbCrLf & "[PRES1]"
sConsulta = sConsulta & vbCrLf & ") REFERENCES [dbo].[PRES_4_CON1] ("
sConsulta = sConsulta & vbCrLf & "[COD]"
sConsulta = sConsulta & vbCrLf & ")"

CrearTablaPRES_4_CON2 = sConsulta
End Function


Private Function CrearTriggerPRES_4_CON2_TG_INSUPD() As String
Dim sConsulta As String

sConsulta = "CREATE TRIGGER PRES_4_CON2_TG_INSUPD ON dbo.PRES_4_CON2"
sConsulta = sConsulta & vbCrLf & "FOR INSERT, UPDATE"
sConsulta = sConsulta & vbCrLf & "AS"
sConsulta = sConsulta & vbCrLf & "DECLARE @PRES1 VARCHAR(50), @COD VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "DECLARE curTG_PRES_4_CON2_Ins CURSOR FOR SELECT PRES1,COD FROM INSERTED"
sConsulta = sConsulta & vbCrLf & "OPEN curTG_PRES_4_CON2_Ins"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM curTG_PRES_4_CON2_Ins INTO @PRES1, @COD"
sConsulta = sConsulta & vbCrLf & "WHILE @@FETCH_STATUS=0"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_4_CON2 SET FECACT=GETDATE() WHERE PRES1=@PRES1 AND COD=@COD"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM curTG_PRES_4_CON2_Ins INTO @PRES1,@COD"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "Close curTG_PRES_4_CON2_Ins"
sConsulta = sConsulta & vbCrLf & "DEALLOCATE curTG_PRES_4_CON2_Ins"

CrearTriggerPRES_4_CON2_TG_INSUPD = sConsulta
End Function


Private Function CrearPRES_4_CON2_COD() As String
Dim sConsulta As String

sConsulta = "CREATE PROCEDURE PRES_4_CON2_COD (@OLD VARCHAR(50),@NEW VARCHAR(50),@PRES1 VARCHAR(50))  AS"
sConsulta = sConsulta & vbCrLf & "DECLARE @VAR_COD VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "SET XACT_ABORT ON"
sConsulta = sConsulta & vbCrLf & "BEGIN TRANSACTION"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "DECLARE C CURSOR FOR SELECT COD FROM PRES_4_CON2 WHERE  PRES1=@PRES1 AND COD=@OLD"
sConsulta = sConsulta & vbCrLf & "OPEN C"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM C INTO @VAR_COD"
sConsulta = sConsulta & vbCrLf & "IF @@FETCH_STATUS=0"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_4_CON1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_4_CON2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_4_CON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_4_CON4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_4_CON1 SET COD=@NEW WHERE  COD=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_4_CON2 SET COD=@NEW WHERE  PRES1=@PRES1 AND COD=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_4_CON3 SET PRES2=@NEW WHERE  PRES1=@PRES1 AND PRES2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_4_CON4 SET PRES2=@NEW WHERE  PRES1=@PRES1 AND PRES2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_PRESCON4 SET PRES2=@NEW WHERE PRES1=@PRES1 AND PRES2=@OLD"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_4_CON1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_4_CON2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_4_CON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_4_CON4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "Close C"
sConsulta = sConsulta & vbCrLf & "DEALLOCATE C"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "COMMIT TRANSACTION"

CrearPRES_4_CON2_COD = sConsulta
End Function

Private Function CrearTablaPRES_4_CON3() As String
Dim sConsulta As String

sConsulta = "CREATE TABLE [dbo].[PRES_4_CON3] ("
sConsulta = sConsulta & vbCrLf & "[PRES1] [varchar] (15) NOT NULL ,"
sConsulta = sConsulta & vbCrLf & "[PRES2] [varchar] (15) NOT NULL ,"
sConsulta = sConsulta & vbCrLf & "[COD] [varchar] (15) NOT NULL ,"
sConsulta = sConsulta & vbCrLf & "[DEN] [varchar] (100) NOT NULL ,"
sConsulta = sConsulta & vbCrLf & "[IMP] [float] NULL ,"
sConsulta = sConsulta & vbCrLf & "[OBJ] [float] NULL ,"
sConsulta = sConsulta & vbCrLf & "[FECACT] [datetime] NULL"
sConsulta = sConsulta & vbCrLf & ") ON [PRIMARY]"

sConsulta = sConsulta & vbCrLf & "ALTER TABLE [dbo].[PRES_4_CON3] WITH NOCHECK ADD"
sConsulta = sConsulta & vbCrLf & "CONSTRAINT [PK_PRES_4_CON3] PRIMARY KEY  NONCLUSTERED"
sConsulta = sConsulta & vbCrLf & "("
sConsulta = sConsulta & vbCrLf & "[PRES1],"
sConsulta = sConsulta & vbCrLf & "[PRES2],"
sConsulta = sConsulta & vbCrLf & "[COD]"
sConsulta = sConsulta & vbCrLf & ")  ON [PRIMARY]"

sConsulta = sConsulta & vbCrLf & "ALTER TABLE [dbo].[PRES_4_CON3] ADD"
sConsulta = sConsulta & vbCrLf & "CONSTRAINT [FK_PRES_4_CON3_PRES_4_CON2] FOREIGN KEY"
sConsulta = sConsulta & vbCrLf & "("
sConsulta = sConsulta & vbCrLf & "[PRES1],"
sConsulta = sConsulta & vbCrLf & "[PRES2]"
sConsulta = sConsulta & vbCrLf & ") REFERENCES [dbo].[PRES_4_CON2] ("
sConsulta = sConsulta & vbCrLf & "[PRES1],"
sConsulta = sConsulta & vbCrLf & "[COD]"
sConsulta = sConsulta & vbCrLf & ")"

CrearTablaPRES_4_CON3 = sConsulta

End Function

Private Function CrearTriggerPRES_4_CON3_TG_INSUPD() As String
Dim sConsulta As String

sConsulta = "CREATE TRIGGER PRES_4_CON3_TG_INSUPD ON dbo.PRES_4_CON3"
sConsulta = sConsulta & vbCrLf & "FOR INSERT, UPDATE"
sConsulta = sConsulta & vbCrLf & "AS"
sConsulta = sConsulta & vbCrLf & "DECLARE @PRES1 VARCHAR(50), @PRES2 VARCHAR(50), @COD VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "DECLARE curTG_PRES_4_CON3_Ins CURSOR FOR SELECT PRES1, PRES2, COD FROM INSERTED"
sConsulta = sConsulta & vbCrLf & "OPEN curTG_PRES_4_CON3_Ins"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM curTG_PRES_4_CON3_Ins INTO @PRES1, @PRES2, @COD"
sConsulta = sConsulta & vbCrLf & "WHILE @@FETCH_STATUS=0"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_4_CON3 SET FECACT=GETDATE() WHERE  PRES1=@PRES1 AND PRES2=@PRES2 AND COD=@COD"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM curTG_PRES_4_CON3_Ins INTO @PRES1, @PRES2, @COD"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "Close curTG_PRES_4_CON3_Ins"
sConsulta = sConsulta & vbCrLf & "DEALLOCATE curTG_PRES_4_CON3_Ins"

CrearTriggerPRES_4_CON3_TG_INSUPD = sConsulta
End Function

Private Function CrearPRES_4_CON3_COD() As String
Dim sConsulta As String

sConsulta = "CREATE PROCEDURE PRES_4_CON3_COD (@OLD VARCHAR(50),@NEW VARCHAR(50),@PRES1 VARCHAR(50),@PRES2 VARCHAR(50))  AS"
sConsulta = sConsulta & vbCrLf & "DECLARE @VAR_COD VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "SET XACT_ABORT ON"
sConsulta = sConsulta & vbCrLf & "BEGIN TRANSACTION"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "DECLARE C CURSOR FOR SELECT COD FROM PRES_4_CON3 WHERE  PRES1=@PRES1 AND PRES2=@PRES2 AND COD=@OLD"
sConsulta = sConsulta & vbCrLf & "OPEN C"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM C INTO @VAR_COD"
sConsulta = sConsulta & vbCrLf & "IF @@FETCH_STATUS=0"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_4_CON1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_4_CON2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_4_CON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_4_CON4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_4_CON1 SET COD=@NEW WHERE  COD=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_4_CON2 SET COD=@NEW WHERE  PRES1=@PRES1 AND COD=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_4_CON3 SET COD=@NEW WHERE  PRES1=@PRES1 AND PRES2=@PRES2 AND COD=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_4_CON4 SET PRES3=@NEW WHERE PRES1=@PRES1 AND PRES2=@PRES2 AND PRES3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_PRESCON4 SET PRES3=@NEW WHERE  PRES1=@PRES1 AND PRES2=@PRES2 AND PRES3=@OLD"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_4_CON1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_4_CON2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_4_CON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_4_CON4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "END"
sConsulta = sConsulta & vbCrLf & "CLOSE C"
sConsulta = sConsulta & vbCrLf & "DEALLOCATE C"
sConsulta = sConsulta & vbCrLf & "END"
sConsulta = sConsulta & vbCrLf & "COMMIT TRANSACTION"

CrearPRES_4_CON3_COD = sConsulta
End Function


Private Function CrearTablaPRES_4_CON4() As String
Dim sConsulta As String

sConsulta = "CREATE TABLE [dbo].[PRES_4_CON4] ("
sConsulta = sConsulta & vbCrLf & "[PRES1] [varchar] (15) NOT NULL ,"
sConsulta = sConsulta & vbCrLf & "[PRES2] [varchar] (15) NOT NULL ,"
sConsulta = sConsulta & vbCrLf & "[PRES3] [varchar] (15) NOT NULL ,"
sConsulta = sConsulta & vbCrLf & "[COD] [varchar] (15) NOT NULL ,"
sConsulta = sConsulta & vbCrLf & "[DEN] [varchar] (100) NOT NULL ,"
sConsulta = sConsulta & vbCrLf & "[IMP] [float] NULL ,"
sConsulta = sConsulta & vbCrLf & "[OBJ] [float] NULL ,"
sConsulta = sConsulta & vbCrLf & "[FECACT] [datetime] NULL"
sConsulta = sConsulta & vbCrLf & ") ON [PRIMARY]"

sConsulta = sConsulta & vbCrLf & "ALTER TABLE [dbo].[PRES_4_CON4] WITH NOCHECK ADD"
sConsulta = sConsulta & vbCrLf & "CONSTRAINT [PK_PRES_4_CON4] PRIMARY KEY  NONCLUSTERED"
sConsulta = sConsulta & vbCrLf & "("
sConsulta = sConsulta & vbCrLf & "[PRES1],"
sConsulta = sConsulta & vbCrLf & "[PRES2],"
sConsulta = sConsulta & vbCrLf & "[PRES3],"
sConsulta = sConsulta & vbCrLf & "[COD]"
sConsulta = sConsulta & vbCrLf & ")  ON [PRIMARY]"

sConsulta = sConsulta & vbCrLf & "ALTER TABLE [dbo].[PRES_4_CON4] ADD"
sConsulta = sConsulta & vbCrLf & "CONSTRAINT [FK_PRES_4_CON4_PRES_4_CON3] FOREIGN KEY"
sConsulta = sConsulta & vbCrLf & "("
sConsulta = sConsulta & vbCrLf & "[PRES1],"
sConsulta = sConsulta & vbCrLf & "[PRES2],"
sConsulta = sConsulta & vbCrLf & "[PRES3]"
sConsulta = sConsulta & vbCrLf & ") REFERENCES [dbo].[PRES_4_CON3] ("
sConsulta = sConsulta & vbCrLf & "[PRES1],"
sConsulta = sConsulta & vbCrLf & "[PRES2],"
sConsulta = sConsulta & vbCrLf & "[COD]"
sConsulta = sConsulta & vbCrLf & ")"

CrearTablaPRES_4_CON4 = sConsulta
End Function


Private Function CrearTriggerPRES_4_CON4_TG_INSUPD() As String
Dim sConsulta As String

sConsulta = "CREATE TRIGGER PRES_4_CON4_TG_INSUPD ON dbo.PRES_4_CON4"
sConsulta = sConsulta & vbCrLf & "FOR INSERT, UPDATE"
sConsulta = sConsulta & vbCrLf & "AS"
sConsulta = sConsulta & vbCrLf & "DECLARE @PRES1 VARCHAR(50), @PRES2 VARCHAR(50), @PRES3 VARCHAR(50), @COD VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "DECLARE curTG_PRES_4_CON4_Ins CURSOR FOR SELECT PRES1, PRES2, PRES3, COD FROM INSERTED"
sConsulta = sConsulta & vbCrLf & "OPEN curTG_PRES_4_CON4_Ins"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM curTG_PRES_4_CON4_Ins INTO @PRES1, @PRES2, @PRES3, @COD"
sConsulta = sConsulta & vbCrLf & "WHILE @@FETCH_STATUS=0"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_4_CON4 SET FECACT=GETDATE() WHERE  PRES1=@PRES1 AND @PRES2=@PRES2 AND PRES3=@PRES3 AND COD=@COD"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM curTG_PRES_4_CON4_Ins INTO @PRES1,@PRES2, @PRES3, @COD"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "Close curTG_PRES_4_CON4_Ins"
sConsulta = sConsulta & vbCrLf & "DEALLOCATE curTG_PRES_4_CON4_Ins"

CrearTriggerPRES_4_CON4_TG_INSUPD = sConsulta
End Function

Private Function CrearPRES_4_CON4_COD() As String
Dim sConsulta As String

sConsulta = "CREATE PROCEDURE PRES_4_CON4_COD (@OLD VARCHAR(50),@NEW VARCHAR(50),@PRES1 VARCHAR(50),@PRES2 VARCHAR(50),@PRES3 VARCHAR(50))  AS"
sConsulta = sConsulta & vbCrLf & "DECLARE @VAR_COD VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "SET XACT_ABORT ON"
sConsulta = sConsulta & vbCrLf & "BEGIN TRANSACTION"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "DECLARE C CURSOR FOR SELECT COD FROM PRES_4_CON4 WHERE PRES1=@PRES1 AND PRES2=@PRES2 AND PRES3=@PRES3 AND COD=@OLD"
sConsulta = sConsulta & vbCrLf & "OPEN C"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM C INTO @VAR_COD"
sConsulta = sConsulta & vbCrLf & "IF @@FETCH_STATUS=0"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_4_CON1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_4_CON2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_4_CON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_4_CON4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_4_CON1 SET COD=@NEW WHERE COD=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_4_CON2 SET COD=@NEW WHERE PRES1=@PRES1 AND COD=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_4_CON3 SET COD=@NEW WHERE PRES1=@PRES1 AND PRES2=@PRES2  AND COD=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_4_CON4 SET COD=@NEW WHERE PRES1=@PRES1 AND PRES2=@PRES2 AND PRES3=@PRES3 AND COD=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_PRESCON4 SET PRES4=@NEW WHERE PRES1=@PRES1 AND PRES2=@PRES2 AND PRES3=@PRES3 AND PRES4=@OLD"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_4_CON1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_4_CON2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_4_CON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_4_CON4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "Close C"
sConsulta = sConsulta & vbCrLf & "DEALLOCATE C"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "COMMIT TRANSACTION"

CrearPRES_4_CON4_COD = sConsulta
End Function


Private Function CrearTablaITEM_PRESCON3() As String
Dim sConsulta As String

sConsulta = "CREATE TABLE [dbo].[ITEM_PRESCON3] ("
sConsulta = sConsulta & vbCrLf & "[ANYO] [smallint] NOT NULL ,"
sConsulta = sConsulta & vbCrLf & "[GMN1] [varchar] (3) NOT NULL ,"
sConsulta = sConsulta & vbCrLf & "[PROCE] [smallint] NOT NULL ,"
sConsulta = sConsulta & vbCrLf & "[ITEM] [int] NOT NULL ,"
sConsulta = sConsulta & vbCrLf & "[PRES1] [varchar] (15) NOT NULL ,"
sConsulta = sConsulta & vbCrLf & "[PRES2] [varchar] (15) NULL ,"
sConsulta = sConsulta & vbCrLf & "[PRES3] [varchar] (15) NULL ,"
sConsulta = sConsulta & vbCrLf & "[PRES4] [varchar] (15) NULL"
sConsulta = sConsulta & vbCrLf & ") ON [PRIMARY]"

sConsulta = sConsulta & vbCrLf & "ALTER TABLE [dbo].[ITEM_PRESCON3] WITH NOCHECK ADD"
sConsulta = sConsulta & vbCrLf & "CONSTRAINT [PK_ITEM_PRESCON3] PRIMARY KEY  NONCLUSTERED"
sConsulta = sConsulta & vbCrLf & "("
sConsulta = sConsulta & vbCrLf & "[ANYO],"
sConsulta = sConsulta & vbCrLf & "[GMN1],"
sConsulta = sConsulta & vbCrLf & "[PROCE],"
sConsulta = sConsulta & vbCrLf & "[ITEM],"
sConsulta = sConsulta & vbCrLf & "[PRES1]"
sConsulta = sConsulta & vbCrLf & ")  ON [PRIMARY]"

sConsulta = sConsulta & vbCrLf & "ALTER TABLE [dbo].[ITEM_PRESCON3] ADD"
sConsulta = sConsulta & vbCrLf & "CONSTRAINT [FK_ITEM_PRESCON3_ITEM] FOREIGN KEY"
sConsulta = sConsulta & vbCrLf & "("
sConsulta = sConsulta & vbCrLf & "[ANYO],"
sConsulta = sConsulta & vbCrLf & "[GMN1],"
sConsulta = sConsulta & vbCrLf & "[PROCE],"
sConsulta = sConsulta & vbCrLf & "[Item]"
sConsulta = sConsulta & vbCrLf & ") REFERENCES [dbo].[ITEM] ("
sConsulta = sConsulta & vbCrLf & "[ANYO],"
sConsulta = sConsulta & vbCrLf & "[GMN1],"
sConsulta = sConsulta & vbCrLf & "[PROCE],"
sConsulta = sConsulta & vbCrLf & "[ID]"
sConsulta = sConsulta & vbCrLf & "),"
sConsulta = sConsulta & vbCrLf & "CONSTRAINT [FK_ITEM_PRESCON3_PRES_3_CON1] FOREIGN KEY"
sConsulta = sConsulta & vbCrLf & "("
sConsulta = sConsulta & vbCrLf & "[PRES1]"
sConsulta = sConsulta & vbCrLf & ") REFERENCES [dbo].[PRES_3_CON1] ("
sConsulta = sConsulta & vbCrLf & "[COD]"
sConsulta = sConsulta & vbCrLf & "),"
sConsulta = sConsulta & vbCrLf & "CONSTRAINT [FK_ITEM_PRESCON3_PRES_3_CON2] FOREIGN KEY"
sConsulta = sConsulta & vbCrLf & "("
sConsulta = sConsulta & vbCrLf & "[PRES1],"
sConsulta = sConsulta & vbCrLf & "[PRES2]"
sConsulta = sConsulta & vbCrLf & ") REFERENCES [dbo].[PRES_3_CON2] ("
sConsulta = sConsulta & vbCrLf & "[PRES1],"
sConsulta = sConsulta & vbCrLf & "[COD]"
sConsulta = sConsulta & vbCrLf & "),"
sConsulta = sConsulta & vbCrLf & "CONSTRAINT [FK_ITEM_PRESCON3_PRES_3_CON3] FOREIGN KEY"
sConsulta = sConsulta & vbCrLf & "("
sConsulta = sConsulta & vbCrLf & "[PRES1],"
sConsulta = sConsulta & vbCrLf & "[PRES2],"
sConsulta = sConsulta & vbCrLf & "[PRES3]"
sConsulta = sConsulta & vbCrLf & ") REFERENCES [dbo].[PRES_3_CON3] ("
sConsulta = sConsulta & vbCrLf & "[PRES1],"
sConsulta = sConsulta & vbCrLf & "[PRES2],"
sConsulta = sConsulta & vbCrLf & "[COD]"
sConsulta = sConsulta & vbCrLf & "),"
sConsulta = sConsulta & vbCrLf & "CONSTRAINT [FK_ITEM_PRESCON3_PRES_3_CON4] FOREIGN KEY"
sConsulta = sConsulta & vbCrLf & "("
sConsulta = sConsulta & vbCrLf & "[PRES1],"
sConsulta = sConsulta & vbCrLf & "[PRES2],"
sConsulta = sConsulta & vbCrLf & "[PRES3],"
sConsulta = sConsulta & vbCrLf & "[PRES4]"
sConsulta = sConsulta & vbCrLf & ") REFERENCES [dbo].[PRES_3_CON4] ("
sConsulta = sConsulta & vbCrLf & "[PRES1],"
sConsulta = sConsulta & vbCrLf & "[PRES2],"
sConsulta = sConsulta & vbCrLf & "[PRES3],"
sConsulta = sConsulta & vbCrLf & "[COD]"
sConsulta = sConsulta & vbCrLf & ")"

CrearTablaITEM_PRESCON3 = sConsulta
End Function


Private Function CrearTablaITEM_PRESCON4() As String
Dim sConsulta As String

sConsulta = "CREATE TABLE [dbo].[ITEM_PRESCON4] ("
sConsulta = sConsulta & vbCrLf & "[ANYO] [smallint] NOT NULL ,"
sConsulta = sConsulta & vbCrLf & "[GMN1] [varchar] (3) NOT NULL ,"
sConsulta = sConsulta & vbCrLf & "[PROCE] [smallint] NOT NULL ,"
sConsulta = sConsulta & vbCrLf & "[ITEM] [int] NOT NULL ,"
sConsulta = sConsulta & vbCrLf & "[PRES1] [varchar] (15) NOT NULL ,"
sConsulta = sConsulta & vbCrLf & "[PRES2] [varchar] (15) NULL ,"
sConsulta = sConsulta & vbCrLf & "[PRES3] [varchar] (15) NULL ,"
sConsulta = sConsulta & vbCrLf & "[PRES4] [varchar] (15) NULL"
sConsulta = sConsulta & vbCrLf & ") ON [PRIMARY]"

sConsulta = sConsulta & vbCrLf & "ALTER TABLE [dbo].[ITEM_PRESCON4] WITH NOCHECK ADD"
sConsulta = sConsulta & vbCrLf & "CONSTRAINT [PK_ITM_PRESCON4] PRIMARY KEY  NONCLUSTERED"
sConsulta = sConsulta & vbCrLf & "("
sConsulta = sConsulta & vbCrLf & "[ANYO],"
sConsulta = sConsulta & vbCrLf & "[GMN1],"
sConsulta = sConsulta & vbCrLf & "[PROCE],"
sConsulta = sConsulta & vbCrLf & "[ITEM],"
sConsulta = sConsulta & vbCrLf & "[PRES1]"
sConsulta = sConsulta & vbCrLf & ")  ON [PRIMARY]"

sConsulta = sConsulta & vbCrLf & "ALTER TABLE [dbo].[ITEM_PRESCON4] ADD"
sConsulta = sConsulta & vbCrLf & "CONSTRAINT [FK_ITEM_PRESCON4_ITEM] FOREIGN KEY"
sConsulta = sConsulta & vbCrLf & "("
sConsulta = sConsulta & vbCrLf & "[ANYO],"
sConsulta = sConsulta & vbCrLf & "[GMN1],"
sConsulta = sConsulta & vbCrLf & "[PROCE],"
sConsulta = sConsulta & vbCrLf & "[Item]"
sConsulta = sConsulta & vbCrLf & ") REFERENCES [dbo].[ITEM] ("
sConsulta = sConsulta & vbCrLf & "[ANYO],"
sConsulta = sConsulta & vbCrLf & "[GMN1],"
sConsulta = sConsulta & vbCrLf & "[PROCE],"
sConsulta = sConsulta & vbCrLf & "[ID]"
sConsulta = sConsulta & vbCrLf & "),"
sConsulta = sConsulta & vbCrLf & "CONSTRAINT [FK_ITEM_PRESCON4_PRES_4_CON1] FOREIGN KEY"
sConsulta = sConsulta & vbCrLf & "("
sConsulta = sConsulta & vbCrLf & "[PRES1]"
sConsulta = sConsulta & vbCrLf & ") REFERENCES [dbo].[PRES_4_CON1] ("
sConsulta = sConsulta & vbCrLf & "[COD]"
sConsulta = sConsulta & vbCrLf & "),"
sConsulta = sConsulta & vbCrLf & "CONSTRAINT [FK_ITEM_PRESCON4_PRES_4_CON2] FOREIGN KEY"
sConsulta = sConsulta & vbCrLf & "("
sConsulta = sConsulta & vbCrLf & "[PRES1],"
sConsulta = sConsulta & vbCrLf & "[PRES2]"
sConsulta = sConsulta & vbCrLf & ") REFERENCES [dbo].[PRES_4_CON2] ("
sConsulta = sConsulta & vbCrLf & "[PRES1],"
sConsulta = sConsulta & vbCrLf & "[COD]"
sConsulta = sConsulta & vbCrLf & "),"
sConsulta = sConsulta & vbCrLf & "CONSTRAINT [FK_ITEM_PRESCON4_PRES_4_CON3] FOREIGN KEY"
sConsulta = sConsulta & vbCrLf & "("
sConsulta = sConsulta & vbCrLf & "[PRES1],"
sConsulta = sConsulta & vbCrLf & "[PRES2],"
sConsulta = sConsulta & vbCrLf & "[PRES3]"
sConsulta = sConsulta & vbCrLf & ") REFERENCES [dbo].[PRES_4_CON3] ("
sConsulta = sConsulta & vbCrLf & "[PRES1],"
sConsulta = sConsulta & vbCrLf & "[PRES2],"
sConsulta = sConsulta & vbCrLf & "[COD]"
sConsulta = sConsulta & vbCrLf & "),"
sConsulta = sConsulta & vbCrLf & "CONSTRAINT [FK_ITEM_PRESCON4_PRES_4_CON4] FOREIGN KEY"
sConsulta = sConsulta & vbCrLf & "("
sConsulta = sConsulta & vbCrLf & "[PRES1],"
sConsulta = sConsulta & vbCrLf & "[PRES2],"
sConsulta = sConsulta & vbCrLf & "[PRES3],"
sConsulta = sConsulta & vbCrLf & "[PRES4]"
sConsulta = sConsulta & vbCrLf & ") REFERENCES [dbo].[PRES_4_CON4] ("
sConsulta = sConsulta & vbCrLf & "[PRES1],"
sConsulta = sConsulta & vbCrLf & "[PRES2],"
sConsulta = sConsulta & vbCrLf & "[PRES3],"
sConsulta = sConsulta & vbCrLf & "[COD]"
sConsulta = sConsulta & vbCrLf & ")"

CrearTablaITEM_PRESCON4 = sConsulta
End Function

Private Function ModificarGMN1_COD_2_7_50() As String
Dim sConsulta As String

sConsulta = "CREATE PROCEDURE GMN1_COD (@OLD VARCHAR(50),@NEW VARCHAR(50))  AS "
sConsulta = sConsulta & vbCrLf & "DECLARE @VAR_COD VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "SET XACT_ABORT ON"
sConsulta = sConsulta & vbCrLf & "BEGIN TRANSACTION"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "DECLARE C CURSOR FOR SELECT COD FROM GMN1 WHERE COD=@OLD"
sConsulta = sConsulta & vbCrLf & "OPEN C"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM C INTO @VAR_COD"
sConsulta = sConsulta & vbCrLf & "IF @@FETCH_STATUS=0"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART1_ADJ NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART2_ADJ NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART3_ADJ NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4_ADJ NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE BLOQUEO_PROCE NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE COM_GMN1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE COM_GMN2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE COM_GMN3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE COM_GMN4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_ADJ NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_ADJREU NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_ESP NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_OBJ NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_OFE NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_OFE_WEB NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRES NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_UON1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_UON2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_UON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESPROY NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_PROVE NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_MAT1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_MAT2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_MAT3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_MAT4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_ESP NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_OFE_ADJUN NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_OFE NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_OFE_WEB_ADJUN NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_OFE_WEB NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_PER NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_PROVE NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_PROVE_PET NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_PROVE_POT NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE REU_PROCE NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_PROCE NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4_ESP NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "UPDATE GMN1 SET COD=@NEW WHERE COD=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART1 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART1_ADJ SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART2 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART2_ADJ SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART3 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART3_ADJ SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART4 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART4_ADJ SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE BLOQUEO_PROCE SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE COM_GMN1 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE COM_GMN2 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE COM_GMN3 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE COM_GMN4 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE GMN2 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE GMN3 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE GMN4 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_ADJ SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_ADJREU SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_ESP SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_OBJ SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_OFE SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_OFE_WEB SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_PRES SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_UON1 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_UON2 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_UON3 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_MAT1 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_MAT2 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_MAT3 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_MAT4 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_ESP SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_OFE_ADJUN SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_OFE SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_OFE_WEB_ADJUN SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_OFE_WEB SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_PER SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_PROVE SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_PROVE_PET SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_PROVE_POT SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_GMN1 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_GMN2 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_GMN3 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_GMN4 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE REU_PROCE SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_PROCE SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_UON1 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_UON2 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_UON3 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON1 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON2 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON3 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON4 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY1 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY2 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY3 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY4 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_PROVE SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_PRESCON SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_PRESCON3 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_PRESCON4 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_PRESPROY SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART4_ESP  SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART1_ADJ CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART2_ADJ CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART3_ADJ CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4_ADJ CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE BLOQUEO_PROCE CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE COM_GMN1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE COM_GMN2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE COM_GMN3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE COM_GMN4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_ADJ CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_ADJREU CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_ESP CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_OBJ CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_OFE CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_OFE_WEB CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRES CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_UON1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_UON2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_UON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_MAT1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_MAT2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_MAT3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_MAT4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_ESP CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_OFE_ADJUN CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_OFE CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_OFE_WEB_ADJUN CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_OFE_WEB CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_PER CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_PROVE CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_PROVE_PET CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_PROVE_POT CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE REU_PROCE CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_PROCE CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESPROY CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_PROVE CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4_ESP CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "Close C"
sConsulta = sConsulta & vbCrLf & "DEALLOCATE C"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "COMMIT TRANSACTION"

ModificarGMN1_COD_2_7_50 = sConsulta
End Function



Private Function ModificarPROCE_COD_2_7_50() As String
Dim sConsulta As String

sConsulta = "CREATE PROCEDURE PROCE_COD (@OLD SMALLINT,@NEW SMALLINT,@ANYO INT,@GMN1 VARCHAR (3))  AS "
sConsulta = sConsulta & vbCrLf & "DECLARE @VAR_COD SMALLINT"
sConsulta = sConsulta & vbCrLf & "DECLARE @AN AS INT"
sConsulta = sConsulta & vbCrLf & "DECLARE @GMN AS VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "SET XACT_ABORT ON"
sConsulta = sConsulta & vbCrLf & "BEGIN TRANSACTION"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "DECLARE D CURSOR FOR SELECT ANYO,GMN1,COD FROM PROCE WHERE ANYO=@ANYO AND COD=@OLD AND GMN1=@GMN1"
sConsulta = sConsulta & vbCrLf & "OPEN D"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM D INTO @AN,@GMN,@VAR_COD"
sConsulta = sConsulta & vbCrLf & "IF @@FETCH_STATUS=0"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART1_ADJ NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART2_ADJ NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART3_ADJ NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4_ADJ NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_ADJ NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_ADJREU NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_ESP NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_OBJ NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_OFE NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_OFE_WEB NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRES NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESPROY NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_UON1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_UON2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_UON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_ESP NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_OFE_ADJUN NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_OFE NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_OFE_WEB_ADJUN NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_OFE_WEB NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_PER NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_PROVE NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_PROVE_PET NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_PROVE_POT NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE REU_PROCE NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE BLOQUEO_PROCE NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "UPDATE ART1_ADJ SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART2_ADJ SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART3_ADJ SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART4_ADJ SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_ADJ SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_ADJREU SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_ESP SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_OBJ SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_OFE SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_OFE_WEB SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_PRES SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_UON1 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_UON2 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_UON3 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_PRESCON SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_PRESCON3 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_PRESCON4 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_PRESPROY SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_ESP SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_OFE_ADJUN SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_OFE SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_OFE_WEB_ADJUN SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_OFE_WEB SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_PER SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_PROVE SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_PROVE_PET SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_PROVE_POT SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE REU_PROCE SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE SET COD=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE BLOQUEO_PROCE SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART1_ADJ CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART2_ADJ CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART3_ADJ CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4_ADJ CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_ADJ CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESPROY CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_ADJREU CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_ESP CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_OBJ CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_OFE CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_OFE_WEB CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRES CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_UON1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_UON2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_UON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_ESP CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_OFE_ADJUN CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_OFE CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_OFE_WEB_ADJUN CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_OFE_WEB CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_PER CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_PROVE CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_PROVE_PET CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_PROVE_POT CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE REU_PROCE CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE BLOQUEO_PROCE CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "Close D"
sConsulta = sConsulta & vbCrLf & "DEALLOCATE D"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "COMMIT TRANSACTION"

ModificarPROCE_COD_2_7_50 = sConsulta
End Function

Private Function CrearTablaAR_ITEM_MES_PRESCON3_1() As String
Dim sConsulta As String

sConsulta = "CREATE TABLE [dbo].[AR_ITEM_MES_PRESCON3_1] ("
sConsulta = sConsulta & vbCrLf & "[ANYO] [smallint] NOT NULL ,"
sConsulta = sConsulta & vbCrLf & "[GMN1] [varchar] (" & gLongitudesDeCodigos.giLongCodGMN1 & ") NOT NULL ,"
sConsulta = sConsulta & vbCrLf & "[PROCE] [smallint] NOT NULL ,"
sConsulta = sConsulta & vbCrLf & "[ITEM] [int] NOT NULL ,"
sConsulta = sConsulta & vbCrLf & "[ANYOADJ] [smallint] NOT NULL ,"
sConsulta = sConsulta & vbCrLf & "[MES] [smallint] NOT NULL ,"
sConsulta = sConsulta & vbCrLf & "[PRES1] [varchar] (15) NOT NULL ,"
sConsulta = sConsulta & vbCrLf & "[DENPRES1] [varchar] (100) NOT NULL ,"
sConsulta = sConsulta & vbCrLf & "[DENGMN1] [varchar] (100) NOT NULL ,"
sConsulta = sConsulta & vbCrLf & "[ADJDIR] [tinyint] NOT NULL ,"
sConsulta = sConsulta & vbCrLf & "[GMN2] [varchar] (" & gLongitudesDeCodigos.giLongCodGMN2 & ") NULL ,"
sConsulta = sConsulta & vbCrLf & "[GMN3] [varchar] (" & gLongitudesDeCodigos.giLongCodGMN3 & ") NULL ,"
sConsulta = sConsulta & vbCrLf & "[GMN4] [varchar] (" & gLongitudesDeCodigos.giLongCodGMN4 & ") NULL ,"
sConsulta = sConsulta & vbCrLf & "[DENGMN2] [varchar] (100) NULL ,"
sConsulta = sConsulta & vbCrLf & "[DENGMN3] [varchar] (100) NULL ,"
sConsulta = sConsulta & vbCrLf & "[DENGMN4] [varchar] (100) NULL ,"
sConsulta = sConsulta & vbCrLf & "[ART] [varchar] (" & gLongitudesDeCodigos.giLongCodART & ") NULL ,"
sConsulta = sConsulta & vbCrLf & "[DESCR] [varchar] (100) NULL ,"
sConsulta = sConsulta & vbCrLf & "[CANT] [float] NOT NULL ,"
sConsulta = sConsulta & vbCrLf & "[CANTPERIODO] [float] NOT NULL ,"
sConsulta = sConsulta & vbCrLf & "[UNI] [varchar] (" & gLongitudesDeCodigos.giLongCodUNI & ") NOT NULL ,"
sConsulta = sConsulta & vbCrLf & "[DENUNI] [varchar] (100) NOT NULL ,"
sConsulta = sConsulta & vbCrLf & "[DEST] [varchar] (4) NOT NULL ,"
sConsulta = sConsulta & vbCrLf & "[DENDEST] [varchar] (100) NOT NULL ,"
sConsulta = sConsulta & vbCrLf & "[PAG] [varchar] (" & gLongitudesDeCodigos.giLongCodPAG & ") NOT NULL ,"
sConsulta = sConsulta & vbCrLf & "[DENPAG] [varchar] (100) NOT NULL ,"
sConsulta = sConsulta & vbCrLf & "[FECINI] [datetime] NOT NULL ,"
sConsulta = sConsulta & vbCrLf & "[FECFIN] [datetime] NOT NULL ,"
sConsulta = sConsulta & vbCrLf & "[PRES] [float] NOT NULL ,"
sConsulta = sConsulta & vbCrLf & "[PREC] [float] NOT NULL"
sConsulta = sConsulta & vbCrLf & ") ON [PRIMARY]"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE [dbo].[AR_ITEM_MES_PRESCON3_1] WITH NOCHECK ADD"
sConsulta = sConsulta & vbCrLf & "CONSTRAINT [PK_AR_ITEM_MES_PRESCON3_1] PRIMARY KEY  CLUSTERED"
sConsulta = sConsulta & vbCrLf & "("
sConsulta = sConsulta & vbCrLf & "[ANYO],"
sConsulta = sConsulta & vbCrLf & "[GMN1],"
sConsulta = sConsulta & vbCrLf & "[PROCE],"
sConsulta = sConsulta & vbCrLf & "[ITEM],"
sConsulta = sConsulta & vbCrLf & "[ANYOADJ],"
sConsulta = sConsulta & vbCrLf & "[MES],"
sConsulta = sConsulta & vbCrLf & "[PRES1]"
sConsulta = sConsulta & vbCrLf & ")  ON [PRIMARY]"
    
CrearTablaAR_ITEM_MES_PRESCON3_1 = sConsulta
End Function


Private Function CrearTablaAR_ITEM_MES_PRESCON3_2() As String
Dim sConsulta As String

    sConsulta = "CREATE TABLE [dbo].[AR_ITEM_MES_PRESCON3_2] ("
    sConsulta = sConsulta & vbCrLf & "[ANYO] [smallint] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[GMN1] [varchar] (" & gLongitudesDeCodigos.giLongCodGMN1 & ") NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[PROCE] [smallint] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[ITEM] [int] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[ANYOADJ] [smallint] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[MES] [smallint] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[PRES1] [varchar] (15) NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[PRES2] [varchar] (15) NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[DENPRES1] [varchar] (100) NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[DENPRES2] [varchar] (100) NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[DENGMN1] [varchar] (100) NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[ADJDIR] [tinyint] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[GMN2] [varchar] (" & gLongitudesDeCodigos.giLongCodGMN2 & ") NULL ,"
    sConsulta = sConsulta & vbCrLf & "[GMN3] [varchar] (" & gLongitudesDeCodigos.giLongCodGMN3 & ") NULL ,"
    sConsulta = sConsulta & vbCrLf & "[GMN4] [varchar] (" & gLongitudesDeCodigos.giLongCodGMN4 & ") NULL ,"
    sConsulta = sConsulta & vbCrLf & "[DENGMN2] [varchar] (100) NULL ,"
    sConsulta = sConsulta & vbCrLf & "[DENGMN3] [varchar] (100) NULL ,"
    sConsulta = sConsulta & vbCrLf & "[DENGMN4] [varchar] (100) NULL ,"
    sConsulta = sConsulta & vbCrLf & "[ART] [varchar] (" & gLongitudesDeCodigos.giLongCodART & ") NULL ,"
    sConsulta = sConsulta & vbCrLf & "[DESCR] [varchar] (100) NULL ,"
    sConsulta = sConsulta & vbCrLf & "[CANT] [float] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[CANTPERIODO] [float] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[UNI] [varchar] (" & gLongitudesDeCodigos.giLongCodUNI & ") NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[DENUNI] [varchar] (100) NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[DEST] [varchar] (4) NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[DENDEST] [varchar] (100) NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[PAG] [varchar] (" & gLongitudesDeCodigos.giLongCodPAG & ") NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[DENPAG] [varchar] (100) NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[FECINI] [datetime] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[FECFIN] [datetime] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[PRES] [float] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[PREC] [float] NOT NULL"
    sConsulta = sConsulta & vbCrLf & ") ON [PRIMARY]"
    
    sConsulta = sConsulta & vbCrLf & "ALTER TABLE [dbo].[AR_ITEM_MES_PRESCON3_2] WITH NOCHECK ADD"
    sConsulta = sConsulta & vbCrLf & "CONSTRAINT [PK_AR_ITEM_MES_PRESCON3_2] PRIMARY KEY  CLUSTERED"
    sConsulta = sConsulta & vbCrLf & "("
    sConsulta = sConsulta & vbCrLf & "[ANYO],"
    sConsulta = sConsulta & vbCrLf & "[GMN1],"
    sConsulta = sConsulta & vbCrLf & "[PROCE],"
    sConsulta = sConsulta & vbCrLf & "[ITEM],"
    sConsulta = sConsulta & vbCrLf & "[ANYOADJ],"
    sConsulta = sConsulta & vbCrLf & "[MES],"
    sConsulta = sConsulta & vbCrLf & "[PRES1],"
    sConsulta = sConsulta & vbCrLf & "[PRES2]"
    sConsulta = sConsulta & vbCrLf & ")  ON [PRIMARY]"
    
    CrearTablaAR_ITEM_MES_PRESCON3_2 = sConsulta
End Function

Private Function CrearTablaAR_ITEM_MES_PRESCON3_3() As String
Dim sConsulta As String

    sConsulta = "CREATE TABLE [dbo].[AR_ITEM_MES_PRESCON3_3] ("
    sConsulta = sConsulta & vbCrLf & "[ANYO] [smallint] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[GMN1] [varchar] (" & gLongitudesDeCodigos.giLongCodGMN1 & ") NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[PROCE] [smallint] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[ITEM] [int] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[ANYOADJ] [smallint] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[MES] [smallint] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[PRES1] [varchar] (15) NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[PRES2] [varchar] (15) NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[PRES3] [varchar] (15) NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[DENPRES1] [varchar] (100) NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[DENPRES2] [varchar] (100) NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[DENPRES3] [varchar] (100) NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[DENGMN1] [varchar] (100) NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[ADJDIR] [tinyint] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[GMN2] [varchar] (" & gLongitudesDeCodigos.giLongCodGMN2 & ") NULL ,"
    sConsulta = sConsulta & vbCrLf & "[GMN3] [varchar] (" & gLongitudesDeCodigos.giLongCodGMN3 & ") NULL ,"
    sConsulta = sConsulta & vbCrLf & "[GMN4] [varchar] (" & gLongitudesDeCodigos.giLongCodGMN4 & ") NULL ,"
    sConsulta = sConsulta & vbCrLf & "[DENGMN2] [varchar] (100) NULL ,"
    sConsulta = sConsulta & vbCrLf & "[DENGMN3] [varchar] (100) NULL ,"
    sConsulta = sConsulta & vbCrLf & "[DENGMN4] [varchar] (100) NULL ,"
    sConsulta = sConsulta & vbCrLf & "[ART] [varchar] (" & gLongitudesDeCodigos.giLongCodART & ") NULL ,"
    sConsulta = sConsulta & vbCrLf & "[DESCR] [varchar] (100) NULL ,"
    sConsulta = sConsulta & vbCrLf & "[CANT] [float] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[CANTPERIODO] [float] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[UNI] [varchar] (" & gLongitudesDeCodigos.giLongCodUNI & ") NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[DENUNI] [varchar] (100) NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[DEST] [varchar] (4) NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[DENDEST] [varchar] (100) NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[PAG] [varchar] (" & gLongitudesDeCodigos.giLongCodPAG & ") NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[DENPAG] [varchar] (100) NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[FECINI] [datetime] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[FECFIN] [datetime] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[PRES] [float] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[PREC] [float] NOT NULL"
    sConsulta = sConsulta & vbCrLf & ") ON [PRIMARY]"

    sConsulta = sConsulta & vbCrLf & "ALTER TABLE [dbo].[AR_ITEM_MES_PRESCON3_3] WITH NOCHECK ADD"
    sConsulta = sConsulta & vbCrLf & "CONSTRAINT [PK_AR_ITEM_MES_PRESCON3_3] PRIMARY KEY  CLUSTERED"
    sConsulta = sConsulta & vbCrLf & "("
    sConsulta = sConsulta & vbCrLf & "[ANYO],"
    sConsulta = sConsulta & vbCrLf & "[GMN1],"
    sConsulta = sConsulta & vbCrLf & "[PROCE],"
    sConsulta = sConsulta & vbCrLf & "[ITEM],"
    sConsulta = sConsulta & vbCrLf & "[ANYOADJ],"
    sConsulta = sConsulta & vbCrLf & "[MES],"
    sConsulta = sConsulta & vbCrLf & "[PRES1],"
    sConsulta = sConsulta & vbCrLf & "[PRES2],"
    sConsulta = sConsulta & vbCrLf & "[PRES3]"
    sConsulta = sConsulta & vbCrLf & ")  ON [PRIMARY]"
    
    CrearTablaAR_ITEM_MES_PRESCON3_3 = sConsulta
End Function

Private Function CrearTablaAR_ITEM_MES_PRESCON3_4() As String
Dim sConsulta As String

    sConsulta = "CREATE TABLE [dbo].[AR_ITEM_MES_PRESCON3_4] ("
    sConsulta = sConsulta & vbCrLf & "[ANYO] [smallint] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[GMN1] [varchar] (" & gLongitudesDeCodigos.giLongCodGMN1 & ") NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[PROCE] [smallint] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[ITEM] [int] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[ANYOADJ] [smallint] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[MES] [smallint] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[PRES1] [varchar] (15) NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[PRES2] [varchar] (15) NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[PRES3] [varchar] (15) NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[PRES4] [varchar] (15) NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[DENPRES1] [varchar] (100)  NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[DENPRES2] [varchar] (100)  NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[DENPRES3] [varchar] (100)  NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[DENPRES4] [varchar] (100)  NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[DENGMN1] [varchar] (100)  NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[ADJDIR] [tinyint] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[GMN2] [varchar] (" & gLongitudesDeCodigos.giLongCodGMN2 & ")  NULL ,"
    sConsulta = sConsulta & vbCrLf & "[GMN3] [varchar] (" & gLongitudesDeCodigos.giLongCodGMN3 & ")  NULL ,"
    sConsulta = sConsulta & vbCrLf & "[GMN4] [varchar] (" & gLongitudesDeCodigos.giLongCodGMN4 & ")  NULL ,"
    sConsulta = sConsulta & vbCrLf & "[DENGMN2] [varchar] (100)  NULL ,"
    sConsulta = sConsulta & vbCrLf & "[DENGMN3] [varchar] (100)  NULL ,"
    sConsulta = sConsulta & vbCrLf & "[DENGMN4] [varchar] (100)  NULL ,"
    sConsulta = sConsulta & vbCrLf & "[ART] [varchar] (" & gLongitudesDeCodigos.giLongCodART & ")  NULL ,"
    sConsulta = sConsulta & vbCrLf & "[DESCR] [varchar] (100)  NULL ,"
    sConsulta = sConsulta & vbCrLf & "[CANT] [float] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[CANTPERIODO] [float] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[UNI] [varchar] (" & gLongitudesDeCodigos.giLongCodUNI & ")  NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[DENUNI] [varchar] (100)  NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[DEST] [varchar] (4)  NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[DENDEST] [varchar] (100)  NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[PAG] [varchar] (" & gLongitudesDeCodigos.giLongCodPAG & ")  NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[DENPAG] [varchar] (100)  NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[FECINI] [datetime] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[FECFIN] [datetime] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[PRES] [float] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[PREC] [float] NOT NULL"
    sConsulta = sConsulta & vbCrLf & ") ON [PRIMARY]"

    sConsulta = sConsulta & vbCrLf & "ALTER TABLE [dbo].[AR_ITEM_MES_PRESCON3_4] WITH NOCHECK ADD"
    sConsulta = sConsulta & vbCrLf & "CONSTRAINT [PK_AR_ITEM_MES_PRESCON3_4] PRIMARY KEY  CLUSTERED"
    sConsulta = sConsulta & vbCrLf & "("
    sConsulta = sConsulta & vbCrLf & "[ANYO],"
    sConsulta = sConsulta & vbCrLf & "[GMN1],"
    sConsulta = sConsulta & vbCrLf & "[PROCE],"
    sConsulta = sConsulta & vbCrLf & "[ITEM],"
    sConsulta = sConsulta & vbCrLf & "[ANYOADJ],"
    sConsulta = sConsulta & vbCrLf & "[MES],"
    sConsulta = sConsulta & vbCrLf & "[PRES1],"
    sConsulta = sConsulta & vbCrLf & "[PRES2],"
    sConsulta = sConsulta & vbCrLf & "[PRES3],"
    sConsulta = sConsulta & vbCrLf & "[PRES4]"
    sConsulta = sConsulta & vbCrLf & ")  ON [PRIMARY]"
    
    CrearTablaAR_ITEM_MES_PRESCON3_4 = sConsulta
End Function

Private Function CrearTablaAR_ITEM_PRESCON3() As String
    Dim sConsulta As String
    
    sConsulta = "CREATE TABLE [dbo].[AR_ITEM_PRESCON3] ("
    sConsulta = sConsulta & vbCrLf & "[ANYO] [smallint] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[GMN1] [varchar] (" & gLongitudesDeCodigos.giLongCodGMN1 & ")  NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[PROCE] [smallint] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[ITEM] [int] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[PRES1] [varchar] (15)  NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[PRES2] [varchar] (15)  NULL ,"
    sConsulta = sConsulta & vbCrLf & "[PRES3] [varchar] (15)  NULL ,"
    sConsulta = sConsulta & vbCrLf & "[PRES4] [varchar] (15)  NULL ,"
    sConsulta = sConsulta & vbCrLf & "[DENPRES1] [varchar] (100)  NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[DENPRES2] [varchar] (100)  NULL ,"
    sConsulta = sConsulta & vbCrLf & "[DENPRES3] [varchar] (100)  NULL ,"
    sConsulta = sConsulta & vbCrLf & "[DENPRES4] [varchar] (100)  NULL ,"
    sConsulta = sConsulta & vbCrLf & "[DENGMN1] [varchar] (100)  NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[ADJDIR] [tinyint] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[ART] [varchar] (" & gLongitudesDeCodigos.giLongCodART & ")  NULL ,"
    sConsulta = sConsulta & vbCrLf & "[DESCR] [varchar] (100)  NULL ,"
    sConsulta = sConsulta & vbCrLf & "[CANT] [float] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[PRES] [float] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[PREC] [float] NOT NULL"
    sConsulta = sConsulta & vbCrLf & ") ON [PRIMARY]"
    
    sConsulta = sConsulta & vbCrLf & "ALTER TABLE [dbo].[AR_ITEM_PRESCON3] WITH NOCHECK ADD"
    sConsulta = sConsulta & vbCrLf & "CONSTRAINT [PK_AR_ITEM_PRESCON3] PRIMARY KEY  CLUSTERED"
    sConsulta = sConsulta & vbCrLf & "("
    sConsulta = sConsulta & vbCrLf & "[ANYO],"
    sConsulta = sConsulta & vbCrLf & "[GMN1],"
    sConsulta = sConsulta & vbCrLf & "[PROCE],"
    sConsulta = sConsulta & vbCrLf & "[ITEM],"
    sConsulta = sConsulta & vbCrLf & "[PRES1]"
    sConsulta = sConsulta & vbCrLf & ")  ON [PRIMARY]"
    
    CrearTablaAR_ITEM_PRESCON3 = sConsulta
End Function

Private Function CrearSP_AR_ITEM_MES_PRESCON3_1() As String
Dim sConsulta As String

sConsulta = "CREATE   PROCEDURE  SP_AR_ITEM_MES_PRESCON3_1 (@ANYO SMALLINT,@GMN1 VARCHAR(50), @PROCE INT,@RES INT OUTPUT)  AS"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "DECLARE @FECINI AS DATETIME"
sConsulta = sConsulta & vbCrLf & "DECLARE @FECFIN AS DATETIME"
sConsulta = sConsulta & vbCrLf & "DECLARE @PRECADJ AS FLOAT"
sConsulta = sConsulta & vbCrLf & "DECLARE @CANTADJ AS FLOAT"
sConsulta = sConsulta & vbCrLf & "DECLARE @PRECAPE AS FLOAT"
sConsulta = sConsulta & vbCrLf & "DECLARE @DENGMN1 AS VARCHAR(100)"
sConsulta = sConsulta & vbCrLf & "DECLARE @DENGMN2 AS VARCHAR(100)"
sConsulta = sConsulta & vbCrLf & "DECLARE @DENGMN3 AS VARCHAR(100)"
sConsulta = sConsulta & vbCrLf & "DECLARE @DENGMN4 AS VARCHAR(100)"
sConsulta = sConsulta & vbCrLf & "DECLARE @PROCECAMBIO AS FLOAT"
sConsulta = sConsulta & vbCrLf & "DECLARE @ADJDIR AS BIT"
sConsulta = sConsulta & vbCrLf & "DECLARE @GMN2 AS VARCHAR (50)"
sConsulta = sConsulta & vbCrLf & "DECLARE @GMN3 AS VARCHAR (50)"
sConsulta = sConsulta & vbCrLf & "DECLARE @GMN4 AS VARCHAR (50)"
sConsulta = sConsulta & vbCrLf & "DECLARE @PORCEN AS FLOAT"
sConsulta = sConsulta & vbCrLf & "DECLARE @MESINI AS INTEGER"
sConsulta = sConsulta & vbCrLf & "DECLARE @TOTALDIAS AS FLOAT"
sConsulta = sConsulta & vbCrLf & "DECLARE @DIAS AS FLOAT"
sConsulta = sConsulta & vbCrLf & "DECLARE @PAG AS VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "DECLARE @DENPAG AS VARCHAR(100)"
sConsulta = sConsulta & vbCrLf & "DECLARE @UNI AS VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "DECLARE @DENUNI AS VARCHAR(100)"
sConsulta = sConsulta & vbCrLf & "DECLARE @DEST AS VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "DECLARE @DENDEST AS VARCHAR(100)"
sConsulta = sConsulta & vbCrLf & "DECLARE @ART AS VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "DECLARE @DESCR AS VARCHAR(100)"
sConsulta = sConsulta & vbCrLf & "DECLARE @CANT AS FLOAT"
sConsulta = sConsulta & vbCrLf & "DECLARE @FECHA AS DATETIME"
sConsulta = sConsulta & vbCrLf & "DECLARE @ITEM AS INTEGER"
sConsulta = sConsulta & vbCrLf & "DECLARE @PROVE AS VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "DECLARE @FEC AS DATETIME"
sConsulta = sConsulta & vbCrLf & "DECLARE @FECFINAL AS DATETIME"
sConsulta = sConsulta & vbCrLf & "DECLARE @ANYOINI AS INTEGER"
sConsulta = sConsulta & vbCrLf & "DECLARE @ANYOFIN AS INTEGER"
sConsulta = sConsulta & vbCrLf & "DECLARE @ANYOACTUAL AS INTEGER"
sConsulta = sConsulta & vbCrLf & "DECLARE @PRES1 AS VARCHAR (50)"
sConsulta = sConsulta & vbCrLf & "DECLARE @DENPRES1 AS VARCHAR(100)"
sConsulta = sConsulta & vbCrLf & "DECLARE @FECINICIAL AS DATETIME"
sConsulta = sConsulta & vbCrLf & "SET CONCAT_NULL_YIELDS_NULL OFF"
sConsulta = sConsulta & vbCrLf & "DECLARE Cur_PROCE CURSOR FORWARD_ONLY STATIC READ_ONLY FOR"
sConsulta = sConsulta & vbCrLf & "SELECT GMN1.DEN,PROCE.GMN2, PROCE.GMN3, PROCE.GMN4"
sConsulta = sConsulta & vbCrLf & ",GMN2.DEN, GMN3.DEN, GMN4.DEN, PROCE.CAMBIO,PROCE.ADJDIR"
sConsulta = sConsulta & vbCrLf & "From Proce"
sConsulta = sConsulta & vbCrLf & "INNER JOIN GMN1 ON PROCE.GMN1=GMN1.COD"
sConsulta = sConsulta & vbCrLf & "INNER JOIN GMN2 ON  PROCE.GMN1=GMN2.GMN1 AND PROCE.GMN2=GMN2.COD"
sConsulta = sConsulta & vbCrLf & "INNER JOIN GMN3 ON  PROCE.GMN1=GMN3.GMN1 AND  PROCE.GMN2=GMN3.GMN2 AND PROCE.GMN3=GMN3.COD"
sConsulta = sConsulta & vbCrLf & "INNER JOIN GMN4 ON  PROCE.GMN1=GMN4.GMN1 AND  PROCE.GMN2=GMN4.GMN2 AND PROCE.GMN3=GMN4.GMN3 AND PROCE.GMN4=GMN4.COD"
sConsulta = sConsulta & vbCrLf & "WHERE PROCE.ANYO=@ANYO AND PROCE.GMN1=@GMN1 AND PROCE.COD=@PROCE"
sConsulta = sConsulta & vbCrLf & "OPEN Cur_PROCE"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM cur_PROCE INTO @DENGMN1,@GMN2,@GMN3,@GMN4,@DENGMN2,@DENGMN3,@DENGMN4,@PROCECAMBIO,@ADJDIR"
sConsulta = sConsulta & vbCrLf & "IF @@FETCH_STATUS<>0 GOTO ERR_"
sConsulta = sConsulta & vbCrLf & "DECLARE Cur_ITEM_ADJ Cursor FORWARD_ONLY STATIC READ_ONLY FOR"
sConsulta = sConsulta & vbCrLf & "SELECT ITEM_ADJ.ITEM,ITEM_ADJ.PROVE,ITEM.FECINI,ITEM.FECFIN,(ITEM_OFE.PRECIO/(PROCE_OFE.CAMBIO*@PROCECAMBIO)) AS PRECADJ,(ITEM.CANT*(ITEM_ADJ.PORCEN/100)) AS CANTADJ,(ITEM.PREC/@PROCECAMBIO) AS PRECAPE"
sConsulta = sConsulta & vbCrLf & ",ITEM.PAG,PAG.DEN,ITEM.UNI,UNI.DEN,ITEM.DEST,DEST.DEN"
sConsulta = sConsulta & vbCrLf & ",ITEM.ART,ITEM.DESCR + ART4.DEN,ITEM.CANT,ITEM_PRESCON3.PRES1,PRES_3_CON1.DEN"
sConsulta = sConsulta & vbCrLf & "From ITEM_ADJ"
sConsulta = sConsulta & vbCrLf & "INNER JOIN ITEM ON ITEM_ADJ.ANYO=ITEM.ANYO AND ITEM_ADJ.GMN1=ITEM.GMN1 AND ITEM_ADJ.PROCE=ITEM.PROCE  AND ITEM_ADJ.ITEM=ITEM.ID"
sConsulta = sConsulta & vbCrLf & "INNER JOIN ITEM_PRESCON3 ON ITEM.ANYO=ITEM_PRESCON3.ANYO AND ITEM.GMN1=ITEM_PRESCON3.GMN1 AND ITEM.PROCE=ITEM_PRESCON3.PROCE AND ITEM.ID=ITEM_PRESCON3.ITEM"
sConsulta = sConsulta & vbCrLf & "INNER JOIN PRES_3_CON1 ON ITEM_PRESCON3.PRES1=PRES_3_CON1.COD"
sConsulta = sConsulta & vbCrLf & "INNER JOIN PROCE_OFE ON ITEM_ADJ.ANYO=PROCE_OFE.ANYO AND ITEM_ADJ.GMN1=PROCE_OFE.GMN1 AND ITEM_ADJ.PROCE=PROCE_OFE.PROCE AND ITEM_ADJ.OFE=PROCE_OFE.OFE AND ITEM_ADJ.PROVE=PROCE_OFE.PROVE"
sConsulta = sConsulta & vbCrLf & "INNER JOIN ITEM_OFE ON ITEM_ADJ.ANYO=ITEM_OFE.ANYO AND ITEM_OFE.GMN1=ITEM_OFE.GMN1 AND ITEM_ADJ.PROCE=ITEM_OFE.PROCE AND ITEM_ADJ.PROVE=ITEM_OFE.PROVE AND ITEM_ADJ.ITEM=ITEM_OFE.ITEM AND ITEM_ADJ.OFE=ITEM_OFE.NUM"
sConsulta = sConsulta & vbCrLf & "INNER JOIN UNI ON ITEM.UNI=UNI.COD"
sConsulta = sConsulta & vbCrLf & "INNER JOIN DEST ON ITEM.DEST=DEST.COD"
sConsulta = sConsulta & vbCrLf & "INNER JOIN PAG ON ITEM.PAG=PAG.COD"
sConsulta = sConsulta & vbCrLf & "LEFT JOIN ART4 ON ITEM.ART=ART4.COD AND ITEM.GMN1=ART4.GMN1 AND ITEM.GMN2=ART4.GMN2 AND ITEM.GMN3=ART4.GMN3 AND ITEM.GMN4=ART4.GMN4"
sConsulta = sConsulta & vbCrLf & "WHERE ITEM_ADJ.ANYO=@ANYO AND ITEM_ADJ.GMN1=@GMN1 AND ITEM_ADJ.PROCE=@PROCE AND ITEM_ADJ.PORCEN <> 0"
sConsulta = sConsulta & vbCrLf & "OPEN Cur_ITEM_ADJ"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM Cur_ITEM_ADJ INTO @ITEM,@PROVE,@FECINI,@FECFIN,@PRECADJ,@CANTADJ,@PRECAPE"
sConsulta = sConsulta & vbCrLf & ",@PAG,@DENPAG,@UNI,@DENUNI,@DEST,@DENDEST"
sConsulta = sConsulta & vbCrLf & ",@ART,@DESCR,@CANT,@PRES1,@DENPRES1"
sConsulta = sConsulta & vbCrLf & "WHILE @@FETCH_STATUS=0"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "/* Tenemos que calcular el porcentaje correspondiente a cada mes del ANYO.*/"
sConsulta = sConsulta & vbCrLf & "SET @TOTALDIAS = datediff(day,@FECINI, @FECFIN) + 1"
sConsulta = sConsulta & vbCrLf & "SET @FECINICIAL = @FECINI"
sConsulta = sConsulta & vbCrLf & "SET @FECFINAL = @FECFIN"
sConsulta = sConsulta & vbCrLf & "SET @MESINI = MONTH(@FECINICIAL)"
sConsulta = sConsulta & vbCrLf & "SET @FECHA = @FECINICIAL"
sConsulta = sConsulta & vbCrLf & "WHILE @MESINI < MONTH(@FECFINAL)"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "/* Calculamos la diferencia en d�as entre el mes inicial y el siguiente mes*/"
sConsulta = sConsulta & vbCrLf & "SET @DIAS = datediff(day,@FECHA,cast( str (month(@FECHA) +1) + '/1/' + str(year(@FECHA) ) as datetime))"
sConsulta = sConsulta & vbCrLf & "SET @PORCEN = @DIAS / @TOTALDIAS"
sConsulta = sConsulta & vbCrLf & "SET @FECHA = cast(str((month(@FECHA) +1)) + '/1/' + str(year(@FECHA)) as datetime)"
sConsulta = sConsulta & vbCrLf & "/*Miramos si hab�a alg�n resultado para ese mes*/"
sConsulta = sConsulta & vbCrLf & "IF (SELECT COUNT(*) FROM AR_ITEM_MES_PRESCON3_1 WHERE"
sConsulta = sConsulta & vbCrLf & "ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM AND ANYOADJ=YEAR(@FECINICIAL) AND MES=@MESINI AND PRES1=@PRES1)=0"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "INSERT INTO  AR_ITEM_MES_PRESCON3_1 ( ANYO,GMN1,PROCE,ITEM,ANYOADJ,MES,DENGMN1,ADJDIR,GMN2,GMN3,GMN4,DENGMN2,DENGMN3,DENGMN4"
sConsulta = sConsulta & vbCrLf & ",ART,DESCR,CANT"
sConsulta = sConsulta & vbCrLf & ",UNI,DENUNI,DEST,DENDEST,PAG,DENPAG,FECINI,FECFIN,PRES,PREC,CANTPERIODO,PRES1,DENPRES1)"
sConsulta = sConsulta & vbCrLf & "VALUES(@ANYO,@GMN1, @PROCE , @ITEM,YEAR(@FECINICIAL),@MESINI, @DENGMN1,@ADJDIR, @GMN2, @GMN3, @GMN4 ,@DENGMN2, @DENGMN3, @DENGMN4"
sConsulta = sConsulta & vbCrLf & ",@ART,@DESCR,@CANT"
sConsulta = sConsulta & vbCrLf & ",@UNI,@DENUNI,@DEST,@DENDEST,@PAG,@DENPAG,@FECINI,@FECFIN,@PRECAPE*@PORCEN*@CANTADJ,@PRECADJ*@PORCEN*@CANTADJ,@CANT*@PORCEN,@PRES1,@DENPRES1)"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "Else"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_1 SET PRES=PRES+@PRECAPE*@PORCEN*@CANTADJ, PREC=PREC+@PRECADJ*@PORCEN*@CANTADJ"
sConsulta = sConsulta & vbCrLf & "WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND MES=@MESINI AND ANYOADJ=YEAR(@FECINICIAL)  AND ITEM=@ITEM AND PRES1=@PRES1"
sConsulta = sConsulta & vbCrLf & "SET @MESINI=@MESINI + 1"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "/*Si el m�s es el mismo*/"
sConsulta = sConsulta & vbCrLf & "IF @MESINI = MONTH(@FECFINAL)"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "SET @DIAS = datediff(day,@FECHA,@FECFINAL) + 1"
sConsulta = sConsulta & vbCrLf & "SET @PORCEN = @DIAS/@TOTALDIAS"
sConsulta = sConsulta & vbCrLf & "/*Miramos si hab�a alg�n resultado para ese mes*/"
sConsulta = sConsulta & vbCrLf & "IF (SELECT COUNT(*) FROM AR_ITEM_MES_PRESCON3_1 WHERE"
sConsulta = sConsulta & vbCrLf & "ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM AND ANYOADJ=YEAR(@FECINICIAL) AND MES=@MESINI AND PRES1=@PRES1)=0"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "INSERT INTO  AR_ITEM_MES_PRESCON3_1 ( ANYO, GMN1, PROCE, ITEM, ANYOADJ, MES, DENGMN1, ADJDIR, GMN2, GMN3, GMN4, DENGMN2, DENGMN3,DENGMN4"
sConsulta = sConsulta & vbCrLf & ", ART, DESCR, CANT, UNI, DENUNI, DEST, DENDEST, PAG, DENPAG, FECINI, FECFIN, PRES, PREC, CANTPERIODO, PRES1, DENPRES1)"
sConsulta = sConsulta & vbCrLf & "VALUES(@ANYO,@GMN1, @PROCE , @ITEM,YEAR(@FECINICIAL),@MESINI, @DENGMN1,@ADJDIR, @GMN2, @GMN3, @GMN4 ,@DENGMN2, @DENGMN3, @DENGMN4"
sConsulta = sConsulta & vbCrLf & ",@ART,@DESCR,@CANT,@UNI,@DENUNI,@DEST,@DENDEST,@PAG,@DENPAG,@FECINI,@FECFIN, @PRECAPE*@PORCEN*@CANTADJ,@PRECADJ*@PORCEN*@CANTADJ,@CANT*@PORCEN,@PRES1,@DENPRES1)"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "Else"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_1 SET PRES=PRES+@PRECAPE*@PORCEN*@CANTADJ, PREC=PREC+@PRECADJ*@PORCEN*@CANTADJ"
sConsulta = sConsulta & vbCrLf & "WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND MES=@MESINI AND ANYOADJ=YEAR(@FECINICIAL)  AND ITEM=@ITEM AND PRES1=@PRES1"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM Cur_ITEM_ADJ INTO @ITEM,@PROVE,@FECINI,@FECFIN,@PRECADJ,@CANTADJ,@PRECAPE"
sConsulta = sConsulta & vbCrLf & ",@PAG,@DENPAG,@UNI,@DENUNI,@DEST,@DENDEST"
sConsulta = sConsulta & vbCrLf & ",@ART,@DESCR,@CANT,@PRES1,@DENPRES1"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "ERR_:"
sConsulta = sConsulta & vbCrLf & "Close Cur_PROCE"
sConsulta = sConsulta & vbCrLf & "DEALLOCATE Cur_PROCE"
sConsulta = sConsulta & vbCrLf & "Close Cur_ITEM_ADJ"
sConsulta = sConsulta & vbCrLf & "DEALLOCATE Cur_ITEM_ADJ"
sConsulta = sConsulta & vbCrLf & "SET CONCAT_NULL_YIELDS_NULL ON"
sConsulta = sConsulta & vbCrLf & "SET @RES=@@ERROR"
sConsulta = sConsulta & vbCrLf & "Return"
sConsulta = sConsulta & vbCrLf & "End"

CrearSP_AR_ITEM_MES_PRESCON3_1 = sConsulta

End Function

Private Function CrearSP_AR_ITEM_MES_PRESCON3_2() As String
Dim sConsulta As String

sConsulta = "CREATE   PROCEDURE  SP_AR_ITEM_MES_PRESCON3_2 (@ANYO SMALLINT,@GMN1 VARCHAR(50), @PROCE INT,@RES INT OUTPUT)  AS"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "DECLARE @FECINI AS DATETIME"
sConsulta = sConsulta & vbCrLf & "DECLARE @FECFIN AS DATETIME"
sConsulta = sConsulta & vbCrLf & "DECLARE @PRECADJ AS FLOAT"
sConsulta = sConsulta & vbCrLf & "DECLARE @CANTADJ AS FLOAT"
sConsulta = sConsulta & vbCrLf & "DECLARE @PRECAPE AS FLOAT"
sConsulta = sConsulta & vbCrLf & "DECLARE @DENGMN1 AS VARCHAR(100)"
sConsulta = sConsulta & vbCrLf & "DECLARE @DENGMN2 AS VARCHAR(100)"
sConsulta = sConsulta & vbCrLf & "DECLARE @DENGMN3 AS VARCHAR(100)"
sConsulta = sConsulta & vbCrLf & "DECLARE @DENGMN4 AS VARCHAR(100)"
sConsulta = sConsulta & vbCrLf & "DECLARE @PROCECAMBIO AS FLOAT"
sConsulta = sConsulta & vbCrLf & "DECLARE @ADJDIR AS BIT"
sConsulta = sConsulta & vbCrLf & "DECLARE @GMN2 AS VARCHAR (50)"
sConsulta = sConsulta & vbCrLf & "DECLARE @GMN3 AS VARCHAR (50)"
sConsulta = sConsulta & vbCrLf & "DECLARE @GMN4 AS VARCHAR (50)"
sConsulta = sConsulta & vbCrLf & "DECLARE @PORCEN AS FLOAT"
sConsulta = sConsulta & vbCrLf & "DECLARE @MESINI AS INTEGER"
sConsulta = sConsulta & vbCrLf & "DECLARE @TOTALDIAS AS FLOAT"
sConsulta = sConsulta & vbCrLf & "DECLARE @DIAS AS FLOAT"
sConsulta = sConsulta & vbCrLf & "DECLARE @PAG AS VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "DECLARE @DENPAG AS VARCHAR(100)"
sConsulta = sConsulta & vbCrLf & "DECLARE @UNI AS VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "DECLARE @DENUNI AS VARCHAR(100)"
sConsulta = sConsulta & vbCrLf & "DECLARE @DEST AS VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "DECLARE @DENDEST AS VARCHAR(100)"
sConsulta = sConsulta & vbCrLf & "DECLARE @ART AS VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "DECLARE @DESCR AS VARCHAR(100)"
sConsulta = sConsulta & vbCrLf & "DECLARE @CANT AS FLOAT"
sConsulta = sConsulta & vbCrLf & "DECLARE @FECHA AS DATETIME"
sConsulta = sConsulta & vbCrLf & "DECLARE @ITEM AS INTEGER"
sConsulta = sConsulta & vbCrLf & "DECLARE @PROVE AS VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "DECLARE @NEPC AS TINYINT /* Numero de niveles en la estructura de materiales*/"
sConsulta = sConsulta & vbCrLf & "DECLARE @FEC AS DATETIME"
sConsulta = sConsulta & vbCrLf & "DECLARE @FECFINAL AS DATETIME"
sConsulta = sConsulta & vbCrLf & "DECLARE @ANYOINI AS INTEGER"
sConsulta = sConsulta & vbCrLf & "DECLARE @ANYOFIN AS INTEGER"
sConsulta = sConsulta & vbCrLf & "DECLARE @ANYOACTUAL AS INTEGER"
sConsulta = sConsulta & vbCrLf & "DECLARE @PRES1 AS VARCHAR (50)"
sConsulta = sConsulta & vbCrLf & "DECLARE @DENPRES1 AS VARCHAR(100)"
sConsulta = sConsulta & vbCrLf & "DECLARE @PRES2 AS VARCHAR (50)"
sConsulta = sConsulta & vbCrLf & "DECLARE @DENPRES2 AS VARCHAR(100)"
sConsulta = sConsulta & vbCrLf & "DECLARE @FECINICIAL AS DATETIME"
sConsulta = sConsulta & vbCrLf & "SET CONCAT_NULL_YIELDS_NULL OFF"
sConsulta = sConsulta & vbCrLf & "DECLARE Cur_PROCE CURSOR FORWARD_ONLY STATIC READ_ONLY FOR"
sConsulta = sConsulta & vbCrLf & "SELECT GMN1.DEN,PROCE.GMN2, PROCE.GMN3, PROCE.GMN4"
sConsulta = sConsulta & vbCrLf & ",GMN2.DEN, GMN3.DEN, GMN4.DEN, PROCE.CAMBIO,PROCE.ADJDIR"
sConsulta = sConsulta & vbCrLf & "From Proce"
sConsulta = sConsulta & vbCrLf & "INNER JOIN GMN1 ON PROCE.GMN1=GMN1.COD"
sConsulta = sConsulta & vbCrLf & "INNER JOIN GMN2 ON  PROCE.GMN1=GMN2.GMN1 AND PROCE.GMN2=GMN2.COD"
sConsulta = sConsulta & vbCrLf & "INNER JOIN GMN3 ON  PROCE.GMN1=GMN3.GMN1 AND  PROCE.GMN2=GMN3.GMN2 AND PROCE.GMN3=GMN3.COD"
sConsulta = sConsulta & vbCrLf & "INNER JOIN GMN4 ON  PROCE.GMN1=GMN4.GMN1 AND  PROCE.GMN2=GMN4.GMN2 AND PROCE.GMN3=GMN4.GMN3 AND PROCE.GMN4=GMN4.COD"
sConsulta = sConsulta & vbCrLf & "WHERE PROCE.ANYO=@ANYO AND PROCE.GMN1=@GMN1 AND PROCE.COD=@PROCE"
sConsulta = sConsulta & vbCrLf & "OPEN Cur_PROCE"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM cur_PROCE INTO @DENGMN1,@GMN2,@GMN3,@GMN4,@DENGMN2,@DENGMN3,@DENGMN4,@PROCECAMBIO,@ADJDIR"
sConsulta = sConsulta & vbCrLf & "IF @@FETCH_STATUS<>0 GOTO ERR_"
sConsulta = sConsulta & vbCrLf & "DECLARE Cur_ITEM_ADJ Cursor FORWARD_ONLY STATIC READ_ONLY FOR"
sConsulta = sConsulta & vbCrLf & "SELECT ITEM_ADJ.ITEM,ITEM_ADJ.PROVE,ITEM.FECINI,ITEM.FECFIN,(ITEM_OFE.PRECIO/(PROCE_OFE.CAMBIO*@PROCECAMBIO)) AS PRECADJ,(ITEM.CANT*(ITEM_ADJ.PORCEN/100)) AS CANTADJ,(ITEM.PREC/@PROCECAMBIO) AS PRECAPE"
sConsulta = sConsulta & vbCrLf & ",ITEM.PAG,PAG.DEN,ITEM.UNI,UNI.DEN,ITEM.DEST,DEST.DEN"
sConsulta = sConsulta & vbCrLf & ",ITEM.ART,ITEM.DESCR + ART4.DEN,ITEM.CANT,ITEM_PRESCON3.PRES1,PRES_3_CON1.DEN,ITEM_PRESCON3.PRES2,PRES_3_CON2.DEN"
sConsulta = sConsulta & vbCrLf & "From ITEM_ADJ"
sConsulta = sConsulta & vbCrLf & "INNER JOIN ITEM ON ITEM_ADJ.ANYO=ITEM.ANYO AND ITEM_ADJ.GMN1=ITEM.GMN1 AND ITEM_ADJ.PROCE=ITEM.PROCE  AND ITEM_ADJ.ITEM=ITEM.ID"
sConsulta = sConsulta & vbCrLf & "INNER JOIN ITEM_PRESCON3 ON ITEM.ANYO=ITEM_PRESCON3.ANYO AND ITEM.GMN1=ITEM_PRESCON3.GMN1 AND ITEM.PROCE=ITEM_PRESCON3.PROCE AND ITEM.ID=ITEM_PRESCON3.ITEM AND ITEM_PRESCON3.PRES2 IS NOT NULL"
sConsulta = sConsulta & vbCrLf & "INNER JOIN PRES_3_CON1 ON ITEM_PRESCON3.PRES1=PRES_3_CON1.COD"
sConsulta = sConsulta & vbCrLf & "INNER JOIN PRES_3_CON2 ON ITEM_PRESCON3.PRES1=PRES_3_CON2.PRES1 AND ITEM_PRESCON3.PRES2=PRES_3_CON2.COD"
sConsulta = sConsulta & vbCrLf & "INNER JOIN PROCE_OFE ON ITEM_ADJ.ANYO=PROCE_OFE.ANYO AND ITEM_ADJ.GMN1=PROCE_OFE.GMN1 AND ITEM_ADJ.PROCE=PROCE_OFE.PROCE AND ITEM_ADJ.OFE=PROCE_OFE.OFE AND ITEM_ADJ.PROVE=PROCE_OFE.PROVE"
sConsulta = sConsulta & vbCrLf & "INNER JOIN ITEM_OFE ON ITEM_ADJ.ANYO=ITEM_OFE.ANYO AND ITEM_OFE.GMN1=ITEM_OFE.GMN1 AND ITEM_ADJ.PROCE=ITEM_OFE.PROCE AND ITEM_ADJ.PROVE=ITEM_OFE.PROVE AND ITEM_ADJ.ITEM=ITEM_OFE.ITEM AND ITEM_ADJ.OFE=ITEM_OFE.NUM"
sConsulta = sConsulta & vbCrLf & "INNER JOIN UNI ON ITEM.UNI=UNI.COD"
sConsulta = sConsulta & vbCrLf & "INNER JOIN DEST ON ITEM.DEST=DEST.COD"
sConsulta = sConsulta & vbCrLf & "INNER JOIN PAG ON ITEM.PAG=PAG.COD"
sConsulta = sConsulta & vbCrLf & "LEFT JOIN ART4 ON ITEM.ART=ART4.COD AND ITEM.GMN1=ART4.GMN1 AND ITEM.GMN2=ART4.GMN2 AND ITEM.GMN3=ART4.GMN3 AND ITEM.GMN4=ART4.GMN4"
sConsulta = sConsulta & vbCrLf & "WHERE ITEM_ADJ.ANYO=@ANYO AND ITEM_ADJ.GMN1=@GMN1 AND ITEM_ADJ.PROCE=@PROCE AND ITEM_ADJ.PORCEN <> 0"
sConsulta = sConsulta & vbCrLf & "OPEN Cur_ITEM_ADJ"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM Cur_ITEM_ADJ INTO @ITEM,@PROVE,@FECINI,@FECFIN,@PRECADJ,@CANTADJ,@PRECAPE"
sConsulta = sConsulta & vbCrLf & ",@PAG,@DENPAG,@UNI,@DENUNI,@DEST,@DENDEST"
sConsulta = sConsulta & vbCrLf & ",@ART,@DESCR,@CANT,@PRES1,@DENPRES1,@PRES2,@DENPRES2"
sConsulta = sConsulta & vbCrLf & "WHILE @@FETCH_STATUS=0"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "/* Tenemos que calcular el porcentaje correspondiente a cada mes del ANYO.*/"
sConsulta = sConsulta & vbCrLf & "SET @TOTALDIAS = datediff(day,@FECINI, @FECFIN) + 1"
sConsulta = sConsulta & vbCrLf & "SET @FECINICIAL = @FECINI"
sConsulta = sConsulta & vbCrLf & "SET @FECFINAL = @FECFIN"
sConsulta = sConsulta & vbCrLf & "SET @MESINI = MONTH(@FECINICIAL)"
sConsulta = sConsulta & vbCrLf & "SET @FECHA = @FECINICIAL"
sConsulta = sConsulta & vbCrLf & "WHILE @MESINI < MONTH(@FECFINAL)"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "/* Calculamos la diferencia en d�as entre el mes inicial y el siguiente mes*/"
sConsulta = sConsulta & vbCrLf & "SET @DIAS = datediff(day,@FECHA,cast( str (month(@FECHA) +1) + '/1/' + str(year(@FECHA) ) as datetime))"
sConsulta = sConsulta & vbCrLf & "SET @PORCEN = @DIAS / @TOTALDIAS"
sConsulta = sConsulta & vbCrLf & "SET @FECHA = cast(str((month(@FECHA) +1)) + '/1/' + str(year(@FECHA)) as datetime)"
sConsulta = sConsulta & vbCrLf & "/*Miramos si hab�a alg�n resultado PROYa ese mes*/"
sConsulta = sConsulta & vbCrLf & "IF (SELECT COUNT(*) FROM AR_ITEM_MES_PRESCON3_2 WHERE"
sConsulta = sConsulta & vbCrLf & "ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM AND ANYOADJ=YEAR(@FECINICIAL) AND MES=@MESINI AND PRES1=@PRES1 AND PRES2=@PRES2)=0"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "INSERT INTO  AR_ITEM_MES_PRESCON3_2 ( ANYO,GMN1,PROCE,ITEM,ANYOADJ,MES,DENGMN1,ADJDIR,GMN2,GMN3,GMN4,DENGMN2,DENGMN3,DENGMN4"
sConsulta = sConsulta & vbCrLf & ",ART,DESCR,CANT"
sConsulta = sConsulta & vbCrLf & ",UNI,DENUNI,DEST,DENDEST,PAG,DENPAG,FECINI,FECFIN,PRES,PREC,CANTPERIODO,PRES1,DENPRES1,PRES2,DENPRES2)"
sConsulta = sConsulta & vbCrLf & "VALUES(@ANYO,@GMN1, @PROCE , @ITEM,YEAR(@FECINICIAL),@MESINI, @DENGMN1,@ADJDIR, @GMN2, @GMN3, @GMN4 ,@DENGMN2, @DENGMN3, @DENGMN4"
sConsulta = sConsulta & vbCrLf & ",@ART,@DESCR,@CANT"
sConsulta = sConsulta & vbCrLf & ",@UNI,@DENUNI,@DEST,@DENDEST,@PAG,@DENPAG,@FECINI,@FECFIN,@PRECAPE*@PORCEN*@CANTADJ,@PRECADJ*@PORCEN*@CANTADJ,@CANT*@PORCEN,@PRES1,@DENPRES1,@PRES2,@DENPRES2)"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "Else"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_2 SET PRES=PRES+@PRECAPE*@PORCEN*@CANTADJ, PREC=PREC+@PRECADJ*@PORCEN*@CANTADJ"
sConsulta = sConsulta & vbCrLf & "WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND MES=@MESINI AND ANYOADJ=YEAR(@FECINICIAL)  AND ITEM=@ITEM AND PRES1=@PRES1 AND PRES2=@PRES2"
sConsulta = sConsulta & vbCrLf & "SET @MESINI=@MESINI + 1"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "/*Si el m�s es el mismo*/"
sConsulta = sConsulta & vbCrLf & "IF @MESINI = MONTH(@FECFINAL)"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "SET @DIAS = datediff(day,@FECHA,@FECFINAL) + 1"
sConsulta = sConsulta & vbCrLf & "SET @PORCEN = @DIAS/@TOTALDIAS"
sConsulta = sConsulta & vbCrLf & "/*Miramos si hab�a alg�n resultado PROYa ese mes*/"
sConsulta = sConsulta & vbCrLf & "IF (SELECT COUNT(*) FROM AR_ITEM_MES_PRESCON3_2 WHERE"
sConsulta = sConsulta & vbCrLf & "ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM AND ANYOADJ=YEAR(@FECINICIAL) AND MES=@MESINI AND PRES1=@PRES1 AND PRES2=@PRES2)=0"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "INSERT INTO  AR_ITEM_MES_PRESCON3_2 ( ANYO, GMN1, PROCE, ITEM, ANYOADJ, MES, DENGMN1, ADJDIR, GMN2, GMN3, GMN4, DENGMN2, DENGMN3,DENGMN4"
sConsulta = sConsulta & vbCrLf & ", ART, DESCR, CANT, UNI, DENUNI, DEST, DENDEST, PAG, DENPAG, FECINI, FECFIN, PRES, PREC, CANTPERIODO, PRES1, DENPRES1,PRES2,DENPRES2)"
sConsulta = sConsulta & vbCrLf & "VALUES(@ANYO,@GMN1, @PROCE , @ITEM,YEAR(@FECINICIAL),@MESINI, @DENGMN1,@ADJDIR, @GMN2, @GMN3, @GMN4 ,@DENGMN2, @DENGMN3, @DENGMN4"
sConsulta = sConsulta & vbCrLf & ",@ART,@DESCR,@CANT,@UNI,@DENUNI,@DEST,@DENDEST,@PAG,@DENPAG,@FECINI,@FECFIN, @PRECAPE*@PORCEN*@CANTADJ,@PRECADJ*@PORCEN*@CANTADJ,@CANT*@PORCEN,@PRES1,@DENPRES1,@PRES2,@DENPRES2)"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "Else"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_2 SET PRES=PRES+@PRECAPE*@PORCEN*@CANTADJ, PREC=PREC+@PRECADJ*@PORCEN*@CANTADJ"
sConsulta = sConsulta & vbCrLf & "WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND MES=@MESINI AND ANYOADJ=YEAR(@FECINICIAL)  AND ITEM=@ITEM AND PRES1=@PRES1 AND PRES2=@PRES2"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM Cur_ITEM_ADJ INTO @ITEM,@PROVE,@FECINI,@FECFIN,@PRECADJ,@CANTADJ,@PRECAPE"
sConsulta = sConsulta & vbCrLf & ",@PAG,@DENPAG,@UNI,@DENUNI,@DEST,@DENDEST"
sConsulta = sConsulta & vbCrLf & ",@ART,@DESCR,@CANT,@PRES1,@DENPRES1,@PRES2,@DENPRES2"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "ERR_:"
sConsulta = sConsulta & vbCrLf & "Close Cur_PROCE"
sConsulta = sConsulta & vbCrLf & "DEALLOCATE Cur_PROCE"
sConsulta = sConsulta & vbCrLf & "Close Cur_ITEM_ADJ"
sConsulta = sConsulta & vbCrLf & "DEALLOCATE Cur_ITEM_ADJ"
sConsulta = sConsulta & vbCrLf & "SET CONCAT_NULL_YIELDS_NULL ON"
sConsulta = sConsulta & vbCrLf & "SET @RES=@@ERROR"
sConsulta = sConsulta & vbCrLf & "Return"
sConsulta = sConsulta & vbCrLf & "End"

CrearSP_AR_ITEM_MES_PRESCON3_2 = sConsulta
End Function

Private Function CrearSP_AR_ITEM_MES_PRESCON3_3() As String
Dim sConsulta As String

sConsulta = "CREATE   PROCEDURE  SP_AR_ITEM_MES_PRESCON3_3 (@ANYO SMALLINT,@GMN1 VARCHAR(50), @PROCE INT,@RES INT OUTPUT)  AS"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "DECLARE @FECINI AS DATETIME"
sConsulta = sConsulta & vbCrLf & "DECLARE @FECFIN AS DATETIME"
sConsulta = sConsulta & vbCrLf & "DECLARE @PRECADJ AS FLOAT"
sConsulta = sConsulta & vbCrLf & "DECLARE @CANTADJ AS FLOAT"
sConsulta = sConsulta & vbCrLf & "DECLARE @PRECAPE AS FLOAT"
sConsulta = sConsulta & vbCrLf & "DECLARE @DENGMN1 AS VARCHAR(100)"
sConsulta = sConsulta & vbCrLf & "DECLARE @DENGMN2 AS VARCHAR(100)"
sConsulta = sConsulta & vbCrLf & "DECLARE @DENGMN3 AS VARCHAR(100)"
sConsulta = sConsulta & vbCrLf & "DECLARE @DENGMN4 AS VARCHAR(100)"
sConsulta = sConsulta & vbCrLf & "DECLARE @PROCECAMBIO AS FLOAT"
sConsulta = sConsulta & vbCrLf & "DECLARE @ADJDIR AS BIT"
sConsulta = sConsulta & vbCrLf & "DECLARE @GMN2 AS VARCHAR (50)"
sConsulta = sConsulta & vbCrLf & "DECLARE @GMN3 AS VARCHAR (50)"
sConsulta = sConsulta & vbCrLf & "DECLARE @GMN4 AS VARCHAR (50)"
sConsulta = sConsulta & vbCrLf & "DECLARE @PORCEN AS FLOAT"
sConsulta = sConsulta & vbCrLf & "DECLARE @MESINI AS INTEGER"
sConsulta = sConsulta & vbCrLf & "DECLARE @TOTALDIAS AS FLOAT"
sConsulta = sConsulta & vbCrLf & "DECLARE @DIAS AS FLOAT"
sConsulta = sConsulta & vbCrLf & "DECLARE @PAG AS VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "DECLARE @DENPAG AS VARCHAR(100)"
sConsulta = sConsulta & vbCrLf & "DECLARE @UNI AS VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "DECLARE @DENUNI AS VARCHAR(100)"
sConsulta = sConsulta & vbCrLf & "DECLARE @DEST AS VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "DECLARE @DENDEST AS VARCHAR(100)"
sConsulta = sConsulta & vbCrLf & "DECLARE @ART AS VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "DECLARE @DESCR AS VARCHAR(100)"
sConsulta = sConsulta & vbCrLf & "DECLARE @CANT AS FLOAT"
sConsulta = sConsulta & vbCrLf & "DECLARE @FECHA AS DATETIME"
sConsulta = sConsulta & vbCrLf & "DECLARE @ITEM AS INTEGER"
sConsulta = sConsulta & vbCrLf & "DECLARE @PROVE AS VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "DECLARE @NEPC AS TINYINT /* Numero de niveles en la estructura de materiales*/"
sConsulta = sConsulta & vbCrLf & "DECLARE @FEC AS DATETIME"
sConsulta = sConsulta & vbCrLf & "DECLARE @FECFINAL AS DATETIME"
sConsulta = sConsulta & vbCrLf & "DECLARE @ANYOINI AS INTEGER"
sConsulta = sConsulta & vbCrLf & "DECLARE @ANYOFIN AS INTEGER"
sConsulta = sConsulta & vbCrLf & "DECLARE @ANYOACTUAL AS INTEGER"
sConsulta = sConsulta & vbCrLf & "DECLARE @PRES1 AS VARCHAR (50)"
sConsulta = sConsulta & vbCrLf & "DECLARE @DENPRES1 AS VARCHAR(100)"
sConsulta = sConsulta & vbCrLf & "DECLARE @PRES2 AS VARCHAR (50)"
sConsulta = sConsulta & vbCrLf & "DECLARE @DENPRES2 AS VARCHAR(100)"
sConsulta = sConsulta & vbCrLf & "DECLARE @PRES3 AS VARCHAR (50)"
sConsulta = sConsulta & vbCrLf & "DECLARE @DENPRES3 AS VARCHAR(100)"
sConsulta = sConsulta & vbCrLf & "DECLARE @FECINICIAL AS DATETIME"
sConsulta = sConsulta & vbCrLf & "SET CONCAT_NULL_YIELDS_NULL OFF"
sConsulta = sConsulta & vbCrLf & "DECLARE Cur_PROCE CURSOR FORWARD_ONLY STATIC READ_ONLY FOR"
sConsulta = sConsulta & vbCrLf & "SELECT GMN1.DEN,PROCE.GMN2, PROCE.GMN3, PROCE.GMN4"
sConsulta = sConsulta & vbCrLf & ",GMN2.DEN, GMN3.DEN, GMN4.DEN, PROCE.CAMBIO,PROCE.ADJDIR"
sConsulta = sConsulta & vbCrLf & "From Proce"
sConsulta = sConsulta & vbCrLf & "INNER JOIN GMN1 ON PROCE.GMN1=GMN1.COD"
sConsulta = sConsulta & vbCrLf & "INNER JOIN GMN2 ON  PROCE.GMN1=GMN2.GMN1 AND PROCE.GMN2=GMN2.COD"
sConsulta = sConsulta & vbCrLf & "INNER JOIN GMN3 ON  PROCE.GMN1=GMN3.GMN1 AND  PROCE.GMN2=GMN3.GMN2 AND PROCE.GMN3=GMN3.COD"
sConsulta = sConsulta & vbCrLf & "INNER JOIN GMN4 ON  PROCE.GMN1=GMN4.GMN1 AND  PROCE.GMN2=GMN4.GMN2 AND PROCE.GMN3=GMN4.GMN3 AND PROCE.GMN4=GMN4.COD"
sConsulta = sConsulta & vbCrLf & "WHERE PROCE.ANYO=@ANYO AND PROCE.GMN1=@GMN1 AND PROCE.COD=@PROCE"
sConsulta = sConsulta & vbCrLf & "OPEN Cur_PROCE"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM cur_PROCE INTO @DENGMN1,@GMN2,@GMN3,@GMN4,@DENGMN2,@DENGMN3,@DENGMN4,@PROCECAMBIO,@ADJDIR"
sConsulta = sConsulta & vbCrLf & "IF @@FETCH_STATUS<>0 GOTO ERR_"
sConsulta = sConsulta & vbCrLf & "DECLARE Cur_ITEM_ADJ Cursor FORWARD_ONLY STATIC READ_ONLY FOR"
sConsulta = sConsulta & vbCrLf & "SELECT ITEM_ADJ.ITEM,ITEM_ADJ.PROVE,ITEM.FECINI,ITEM.FECFIN,(ITEM_OFE.PRECIO/(PROCE_OFE.CAMBIO*@PROCECAMBIO)) AS PRECADJ,(ITEM.CANT*(ITEM_ADJ.PORCEN/100)) AS CANTADJ,(ITEM.PREC/@PROCECAMBIO) AS PRECAPE"
sConsulta = sConsulta & vbCrLf & ",ITEM.PAG,PAG.DEN,ITEM.UNI,UNI.DEN,ITEM.DEST,DEST.DEN"
sConsulta = sConsulta & vbCrLf & ",ITEM.ART,ITEM.DESCR + ART4.DEN,ITEM.CANT,ITEM_PRESCON3.PRES1,PRES_3_CON1.DEN,ITEM_PRESCON3.PRES2,PRES_3_CON2.DEN,ITEM_PRESCON3.PRES3,PRES_3_CON3.DEN"
sConsulta = sConsulta & vbCrLf & "From ITEM_ADJ"
sConsulta = sConsulta & vbCrLf & "INNER JOIN ITEM ON ITEM_ADJ.ANYO=ITEM.ANYO AND ITEM_ADJ.GMN1=ITEM.GMN1 AND ITEM_ADJ.PROCE=ITEM.PROCE  AND ITEM_ADJ.ITEM=ITEM.ID"
sConsulta = sConsulta & vbCrLf & "INNER JOIN ITEM_PRESCON3 ON ITEM.ANYO=ITEM_PRESCON3.ANYO AND ITEM.GMN1=ITEM_PRESCON3.GMN1 AND ITEM.PROCE=ITEM_PRESCON3.PROCE AND ITEM.ID=ITEM_PRESCON3.ITEM AND ITEM_PRESCON3.PRES3 IS NOT NULL"
sConsulta = sConsulta & vbCrLf & "INNER JOIN PRES_3_CON1 ON ITEM_PRESCON3.PRES1=PRES_3_CON1.COD"
sConsulta = sConsulta & vbCrLf & "INNER JOIN PRES_3_CON2 ON ITEM_PRESCON3.PRES1=PRES_3_CON2.PRES1 AND ITEM_PRESCON3.PRES2=PRES_3_CON2.COD"
sConsulta = sConsulta & vbCrLf & "INNER JOIN PRES_3_CON3 ON ITEM_PRESCON3.PRES1=PRES_3_CON3.PRES1 AND ITEM_PRESCON3.PRES2=PRES_3_CON3.PRES2 AND ITEM_PRESCON3.PRES3 =PRES_3_CON3.COD"
sConsulta = sConsulta & vbCrLf & "INNER JOIN PROCE_OFE ON ITEM_ADJ.ANYO=PROCE_OFE.ANYO AND ITEM_ADJ.GMN1=PROCE_OFE.GMN1 AND ITEM_ADJ.PROCE=PROCE_OFE.PROCE AND ITEM_ADJ.OFE=PROCE_OFE.OFE AND ITEM_ADJ.PROVE=PROCE_OFE.PROVE"
sConsulta = sConsulta & vbCrLf & "INNER JOIN ITEM_OFE ON ITEM_ADJ.ANYO=ITEM_OFE.ANYO AND ITEM_OFE.GMN1=ITEM_OFE.GMN1 AND ITEM_ADJ.PROCE=ITEM_OFE.PROCE AND ITEM_ADJ.PROVE=ITEM_OFE.PROVE AND ITEM_ADJ.ITEM=ITEM_OFE.ITEM AND ITEM_ADJ.OFE=ITEM_OFE.NUM"
sConsulta = sConsulta & vbCrLf & "INNER JOIN UNI ON ITEM.UNI=UNI.COD"
sConsulta = sConsulta & vbCrLf & "INNER JOIN DEST ON ITEM.DEST=DEST.COD"
sConsulta = sConsulta & vbCrLf & "INNER JOIN PAG ON ITEM.PAG=PAG.COD"
sConsulta = sConsulta & vbCrLf & "LEFT JOIN ART4 ON ITEM.ART=ART4.COD AND ITEM.GMN1=ART4.GMN1 AND ITEM.GMN2=ART4.GMN2 AND ITEM.GMN3=ART4.GMN3 AND ITEM.GMN4=ART4.GMN4"
sConsulta = sConsulta & vbCrLf & "WHERE ITEM_ADJ.ANYO=@ANYO AND ITEM_ADJ.GMN1=@GMN1 AND ITEM_ADJ.PROCE=@PROCE AND ITEM_ADJ.PORCEN <> 0"
sConsulta = sConsulta & vbCrLf & "OPEN Cur_ITEM_ADJ"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM Cur_ITEM_ADJ INTO @ITEM,@PROVE,@FECINI,@FECFIN,@PRECADJ,@CANTADJ,@PRECAPE"
sConsulta = sConsulta & vbCrLf & ",@PAG,@DENPAG,@UNI,@DENUNI,@DEST,@DENDEST"
sConsulta = sConsulta & vbCrLf & ",@ART,@DESCR,@CANT,@PRES1,@DENPRES1,@PRES2,@DENPRES2,@PRES3,@DENPRES3"
sConsulta = sConsulta & vbCrLf & "WHILE @@FETCH_STATUS=0"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "/* Tenemos que calcular el porcentaje correspondiente a cada mes del ANYO.*/"
sConsulta = sConsulta & vbCrLf & "SET @TOTALDIAS = datediff(day,@FECINI, @FECFIN) + 1"
sConsulta = sConsulta & vbCrLf & "SET @FECINICIAL = @FECINI"
sConsulta = sConsulta & vbCrLf & "SET @FECFINAL = @FECFIN"
sConsulta = sConsulta & vbCrLf & "SET @MESINI = MONTH(@FECINICIAL)"
sConsulta = sConsulta & vbCrLf & "SET @FECHA = @FECINICIAL"
sConsulta = sConsulta & vbCrLf & "WHILE @MESINI < MONTH(@FECFINAL)"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "/* Calculamos la diferencia en d�as entre el mes inicial y el siguiente mes*/"
sConsulta = sConsulta & vbCrLf & "SET @DIAS = datediff(day,@FECHA,cast( str (month(@FECHA) +1) + '/1/' + str(year(@FECHA) ) as datetime))"
sConsulta = sConsulta & vbCrLf & "SET @PORCEN = @DIAS / @TOTALDIAS"
sConsulta = sConsulta & vbCrLf & "SET @FECHA = cast(str((month(@FECHA) +1)) + '/1/' + str(year(@FECHA)) as datetime)"
sConsulta = sConsulta & vbCrLf & "/*Miramos si hab�a alg�n resultado PRES a ese mes*/"
sConsulta = sConsulta & vbCrLf & "IF (SELECT COUNT(*) FROM AR_ITEM_MES_PRESCON3_3 WHERE"
sConsulta = sConsulta & vbCrLf & "ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM AND ANYOADJ=YEAR(@FECINICIAL) AND MES=@MESINI AND PRES1=@PRES1 AND PRES2=@PRES2 AND PRES3=@PRES3)=0"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "INSERT INTO  AR_ITEM_MES_PRESCON3_3 ( ANYO,GMN1,PROCE,ITEM,ANYOADJ,MES,DENGMN1,ADJDIR,GMN2,GMN3,GMN4,DENGMN2,DENGMN3,DENGMN4"
sConsulta = sConsulta & vbCrLf & ",ART,DESCR,CANT"
sConsulta = sConsulta & vbCrLf & ",UNI,DENUNI,DEST,DENDEST,PAG,DENPAG,FECINI,FECFIN,PRES,PREC,CANTPERIODO,PRES1,DENPRES1,PRES2,DENPRES2,PRES3,DENPRES3)"
sConsulta = sConsulta & vbCrLf & "VALUES(@ANYO,@GMN1, @PROCE , @ITEM,YEAR(@FECINICIAL),@MESINI, @DENGMN1,@ADJDIR, @GMN2, @GMN3, @GMN4 ,@DENGMN2, @DENGMN3, @DENGMN4"
sConsulta = sConsulta & vbCrLf & ",@ART,@DESCR,@CANT"
sConsulta = sConsulta & vbCrLf & ",@UNI,@DENUNI,@DEST,@DENDEST,@PAG,@DENPAG,@FECINI,@FECFIN,@PRECAPE*@PORCEN*@CANTADJ,@PRECADJ*@PORCEN*@CANTADJ,@CANT*@PORCEN,@PRES1,@DENPRES1,@PRES2,@DENPRES2,@PRES3,@DENPRES3)"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "Else"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_3 SET PRES=PRES+@PRECAPE*@PORCEN*@CANTADJ, PREC=PREC+@PRECADJ*@PORCEN*@CANTADJ"
sConsulta = sConsulta & vbCrLf & "WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND MES=@MESINI AND ANYOADJ=YEAR(@FECINICIAL)  AND ITEM=@ITEM AND PRES1=@PRES1 AND PRES2=@PRES2 AND PRES3=@PRES3"
sConsulta = sConsulta & vbCrLf & "SET @MESINI=@MESINI + 1"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "/*Si el m�s es el mismo*/"
sConsulta = sConsulta & vbCrLf & "IF @MESINI = MONTH(@FECFINAL)"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "SET @DIAS = datediff(day,@FECHA,@FECFINAL) + 1"
sConsulta = sConsulta & vbCrLf & "SET @PORCEN = @DIAS/@TOTALDIAS"
sConsulta = sConsulta & vbCrLf & "/*Miramos si hab�a alg�n resultado PRES a ese mes*/"
sConsulta = sConsulta & vbCrLf & "IF (SELECT COUNT(*) FROM AR_ITEM_MES_PRESCON3_3 WHERE"
sConsulta = sConsulta & vbCrLf & "ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM AND ANYOADJ=YEAR(@FECINICIAL) AND MES=@MESINI AND PRES1=@PRES1 AND PRES2=@PRES2 AND PRES3=@PRES3)=0"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "INSERT INTO  AR_ITEM_MES_PRESCON3_3 ( ANYO, GMN1, PROCE, ITEM, ANYOADJ, MES, DENGMN1, ADJDIR, GMN2, GMN3, GMN4, DENGMN2, DENGMN3,DENGMN4"
sConsulta = sConsulta & vbCrLf & ", ART, DESCR, CANT, UNI, DENUNI, DEST, DENDEST, PAG, DENPAG, FECINI, FECFIN, PRES, PREC, CANTPERIODO, PRES1, DENPRES1,PRES2,DENPRES2,PRES3,DENPRES3)"
sConsulta = sConsulta & vbCrLf & "VALUES(@ANYO,@GMN1, @PROCE , @ITEM,YEAR(@FECINICIAL),@MESINI, @DENGMN1,@ADJDIR, @GMN2, @GMN3, @GMN4 ,@DENGMN2, @DENGMN3, @DENGMN4"
sConsulta = sConsulta & vbCrLf & ",@ART,@DESCR,@CANT,@UNI,@DENUNI,@DEST,@DENDEST,@PAG,@DENPAG,@FECINI,@FECFIN, @PRECAPE*@PORCEN*@CANTADJ,@PRECADJ*@PORCEN*@CANTADJ,@CANT*@PORCEN,@PRES1,@DENPRES1,@PRES2,@DENPRES2,@PRES3,@DENPRES3)"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "Else"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_3 SET PRES=PRES+@PRECAPE*@PORCEN*@CANTADJ, PREC=PREC+@PRECADJ*@PORCEN*@CANTADJ"
sConsulta = sConsulta & vbCrLf & "WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND MES=@MESINI AND ANYOADJ=YEAR(@FECINICIAL)  AND ITEM=@ITEM AND PRES1=@PRES1 AND PRES2=@PRES2 AND PRES3=@PRES3"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM Cur_ITEM_ADJ INTO @ITEM,@PROVE,@FECINI,@FECFIN,@PRECADJ,@CANTADJ,@PRECAPE"
sConsulta = sConsulta & vbCrLf & ",@PAG,@DENPAG,@UNI,@DENUNI,@DEST,@DENDEST"
sConsulta = sConsulta & vbCrLf & ",@ART,@DESCR,@CANT,@PRES1,@DENPRES1,@PRES2,@DENPRES2,@PRES3,@DENPRES3"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "ERR_:"
sConsulta = sConsulta & vbCrLf & "Close Cur_PROCE"
sConsulta = sConsulta & vbCrLf & "DEALLOCATE Cur_PROCE"
sConsulta = sConsulta & vbCrLf & "Close Cur_ITEM_ADJ"
sConsulta = sConsulta & vbCrLf & "DEALLOCATE Cur_ITEM_ADJ"
sConsulta = sConsulta & vbCrLf & "SET CONCAT_NULL_YIELDS_NULL ON"
sConsulta = sConsulta & vbCrLf & "SET @RES=@@ERROR"
sConsulta = sConsulta & vbCrLf & "Return"
sConsulta = sConsulta & vbCrLf & "End"

CrearSP_AR_ITEM_MES_PRESCON3_3 = sConsulta
End Function

Private Function CrearSP_AR_ITEM_MES_PRESCON3_4()
Dim sConsulta As String

 sConsulta = "CREATE   PROCEDURE  SP_AR_ITEM_MES_PRESCON3_4 (@ANYO SMALLINT,@GMN1 VARCHAR(50), @PROCE INT,@RES INT OUTPUT)  AS"
 sConsulta = sConsulta & vbCrLf & "BEGIN"
 sConsulta = sConsulta & vbCrLf & "DECLARE @FECINI AS DATETIME"
 sConsulta = sConsulta & vbCrLf & "DECLARE @FECFIN AS DATETIME"
 sConsulta = sConsulta & vbCrLf & "DECLARE @PRECADJ AS FLOAT"
 sConsulta = sConsulta & vbCrLf & "DECLARE @CANTADJ AS FLOAT"
 sConsulta = sConsulta & vbCrLf & "DECLARE @PRECAPE AS FLOAT"
 sConsulta = sConsulta & vbCrLf & "DECLARE @DENGMN1 AS VARCHAR(100)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @DENGMN2 AS VARCHAR(100)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @DENGMN3 AS VARCHAR(100)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @DENGMN4 AS VARCHAR(100)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @PROCECAMBIO AS FLOAT"
 sConsulta = sConsulta & vbCrLf & "DECLARE @ADJDIR AS BIT"
 sConsulta = sConsulta & vbCrLf & "DECLARE @GMN2 AS VARCHAR (50)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @GMN3 AS VARCHAR (50)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @GMN4 AS VARCHAR (50)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @PORCEN AS FLOAT"
 sConsulta = sConsulta & vbCrLf & "DECLARE @MESINI AS INTEGER"
 sConsulta = sConsulta & vbCrLf & "DECLARE @TOTALDIAS AS FLOAT"
 sConsulta = sConsulta & vbCrLf & "DECLARE @DIAS AS FLOAT"
 sConsulta = sConsulta & vbCrLf & "DECLARE @PAG AS VARCHAR(50)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @DENPAG AS VARCHAR(100)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @UNI AS VARCHAR(50)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @DENUNI AS VARCHAR(100)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @DEST AS VARCHAR(50)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @DENDEST AS VARCHAR(100)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @ART AS VARCHAR(50)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @DESCR AS VARCHAR(100)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @CANT AS FLOAT"
 sConsulta = sConsulta & vbCrLf & "DECLARE @FECHA AS DATETIME"
 sConsulta = sConsulta & vbCrLf & "DECLARE @ITEM AS INTEGER"
 sConsulta = sConsulta & vbCrLf & "DECLARE @PROVE AS VARCHAR(50)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @FEC AS DATETIME"
 sConsulta = sConsulta & vbCrLf & "DECLARE @FECFINAL AS DATETIME"
 sConsulta = sConsulta & vbCrLf & "DECLARE @ANYOINI AS INTEGER"
 sConsulta = sConsulta & vbCrLf & "DECLARE @ANYOFIN AS INTEGER"
 sConsulta = sConsulta & vbCrLf & "DECLARE @ANYOACTUAL AS INTEGER"
 sConsulta = sConsulta & vbCrLf & "DECLARE @PRES1 AS VARCHAR (50)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @DENPRES1 AS VARCHAR(100)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @PRES2 AS VARCHAR (50)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @DENPRES2 AS VARCHAR(100)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @PRES3 AS VARCHAR (50)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @DENPRES3 AS VARCHAR(100)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @PRES4 AS VARCHAR (50)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @DENPRES4 AS VARCHAR(100)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @FECINICIAL AS DATETIME"
 sConsulta = sConsulta & vbCrLf & ""
 sConsulta = sConsulta & vbCrLf & "SET CONCAT_NULL_YIELDS_NULL OFF"
 sConsulta = sConsulta & vbCrLf & ""
 sConsulta = sConsulta & vbCrLf & "DECLARE Cur_PROCE CURSOR FORWARD_ONLY STATIC READ_ONLY FOR"
 sConsulta = sConsulta & vbCrLf & ""
 sConsulta = sConsulta & vbCrLf & "  "
 sConsulta = sConsulta & vbCrLf & "SELECT GMN1.DEN,PROCE.GMN2, PROCE.GMN3, PROCE.GMN4"
 sConsulta = sConsulta & vbCrLf & ",GMN2.DEN, GMN3.DEN, GMN4.DEN, PROCE.CAMBIO,PROCE.ADJDIR"
 sConsulta = sConsulta & vbCrLf & "From Proce"
 sConsulta = sConsulta & vbCrLf & "INNER JOIN GMN1 ON PROCE.GMN1=GMN1.COD"
 sConsulta = sConsulta & vbCrLf & "INNER JOIN GMN2 ON  PROCE.GMN1=GMN2.GMN1 AND PROCE.GMN2=GMN2.COD"
 sConsulta = sConsulta & vbCrLf & "INNER JOIN GMN3 ON  PROCE.GMN1=GMN3.GMN1 AND  PROCE.GMN2=GMN3.GMN2 AND PROCE.GMN3=GMN3.COD"
 sConsulta = sConsulta & vbCrLf & "INNER JOIN GMN4 ON  PROCE.GMN1=GMN4.GMN1 AND  PROCE.GMN2=GMN4.GMN2 AND PROCE.GMN3=GMN4.GMN3 AND PROCE.GMN4=GMN4.COD"
 sConsulta = sConsulta & vbCrLf & "WHERE PROCE.ANYO=@ANYO AND PROCE.GMN1=@GMN1 AND PROCE.COD=@PROCE"
 sConsulta = sConsulta & vbCrLf & "OPEN Cur_PROCE"
 sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM cur_PROCE INTO @DENGMN1,@GMN2,@GMN3,@GMN4,@DENGMN2,@DENGMN3,@DENGMN4,@PROCECAMBIO,@ADJDIR"
 sConsulta = sConsulta & vbCrLf & "IF @@FETCH_STATUS<>0 GOTO ERR_"
 sConsulta = sConsulta & vbCrLf & ""
 sConsulta = sConsulta & vbCrLf & ""
 sConsulta = sConsulta & vbCrLf & "DECLARE Cur_ITEM_ADJ Cursor FORWARD_ONLY STATIC READ_ONLY FOR"
 sConsulta = sConsulta & vbCrLf & ""
 sConsulta = sConsulta & vbCrLf & "SELECT ITEM_ADJ.ITEM,ITEM_ADJ.PROVE,ITEM.FECINI,ITEM.FECFIN,(ITEM_OFE.PRECIO/(PROCE_OFE.CAMBIO*@PROCECAMBIO)) AS PRECADJ,(ITEM.CANT*(ITEM_ADJ.PORCEN/100)) AS CANTADJ,(ITEM.PREC/@PROCECAMBIO) AS PRECAPE"
 sConsulta = sConsulta & vbCrLf & ",ITEM.PAG,PAG.DEN,ITEM.UNI,UNI.DEN,ITEM.DEST,DEST.DEN"
 sConsulta = sConsulta & vbCrLf & ",ITEM.ART,ITEM.DESCR + ART4.DEN,ITEM.CANT,ITEM_PRESCON3.PRES1,PRES_3_CON1.DEN,ITEM_PRESCON3.PRES2,PRES_3_CON2.DEN,ITEM_PRESCON3.PRES3,PRES_3_CON3.DEN, ITEM_PRESCON3.PRES4,PRES_3_CON4.DEN"
 sConsulta = sConsulta & vbCrLf & "From ITEM_ADJ"
 sConsulta = sConsulta & vbCrLf & "INNER JOIN ITEM ON ITEM_ADJ.ANYO=ITEM.ANYO AND ITEM_ADJ.GMN1=ITEM.GMN1 AND ITEM_ADJ.PROCE=ITEM.PROCE  AND ITEM_ADJ.ITEM=ITEM.ID"
 sConsulta = sConsulta & vbCrLf & "INNER JOIN ITEM_PRESCON3 ON ITEM.ANYO=ITEM_PRESCON3.ANYO AND ITEM.GMN1=ITEM_PRESCON3.GMN1 AND ITEM.PROCE=ITEM_PRESCON3.PROCE AND ITEM.ID=ITEM_PRESCON3.ITEM AND ITEM_PRESCON3.PRES4 IS NOT NULL"
 sConsulta = sConsulta & vbCrLf & "INNER JOIN PRES_3_CON1 ON ITEM_PRESCON3.PRES1=PRES_3_CON1.COD"
 sConsulta = sConsulta & vbCrLf & "INNER JOIN PRES_3_CON2 ON ITEM_PRESCON3.PRES1=PRES_3_CON2.PRES1 AND ITEM_PRESCON3.PRES2=PRES_3_CON2.COD"
 sConsulta = sConsulta & vbCrLf & "INNER JOIN PRES_3_CON3 ON ITEM_PRESCON3.PRES1=PRES_3_CON3.PRES1 AND ITEM_PRESCON3.PRES2=PRES_3_CON3.PRES2 AND ITEM_PRESCON3.PRES3 =PRES_3_CON3.COD"
 sConsulta = sConsulta & vbCrLf & "INNER JOIN PRES_3_CON4 ON ITEM_PRESCON3.PRES1=PRES_3_CON4.PRES1 AND ITEM_PRESCON3.PRES2=PRES_3_CON4.PRES2 AND ITEM_PRESCON3.PRES3 =PRES_3_CON4.PRES3 AND ITEM_PRESCON3.PRES4 = PRES_3_CON4.COD"
 sConsulta = sConsulta & vbCrLf & "INNER JOIN PROCE_OFE ON ITEM_ADJ.ANYO=PROCE_OFE.ANYO AND ITEM_ADJ.GMN1=PROCE_OFE.GMN1 AND ITEM_ADJ.PROCE=PROCE_OFE.PROCE AND ITEM_ADJ.OFE=PROCE_OFE.OFE AND ITEM_ADJ.PROVE=PROCE_OFE.PROVE"
 sConsulta = sConsulta & vbCrLf & "INNER JOIN ITEM_OFE ON ITEM_ADJ.ANYO=ITEM_OFE.ANYO AND ITEM_OFE.GMN1=ITEM_OFE.GMN1 AND ITEM_ADJ.PROCE=ITEM_OFE.PROCE AND ITEM_ADJ.PROVE=ITEM_OFE.PROVE AND ITEM_ADJ.ITEM=ITEM_OFE.ITEM AND ITEM_ADJ.OFE=ITEM_OFE.NUM"
 sConsulta = sConsulta & vbCrLf & "INNER JOIN UNI ON ITEM.UNI=UNI.COD"
 sConsulta = sConsulta & vbCrLf & "INNER JOIN DEST ON ITEM.DEST=DEST.COD"
 sConsulta = sConsulta & vbCrLf & "INNER JOIN PAG ON ITEM.PAG=PAG.COD"
 sConsulta = sConsulta & vbCrLf & "LEFT JOIN ART4 ON ITEM.ART=ART4.COD AND ITEM.GMN1=ART4.GMN1 AND ITEM.GMN2=ART4.GMN2 AND ITEM.GMN3=ART4.GMN3 AND ITEM.GMN4=ART4.GMN4"
 sConsulta = sConsulta & vbCrLf & "WHERE ITEM_ADJ.ANYO=@ANYO AND ITEM_ADJ.GMN1=@GMN1 AND ITEM_ADJ.PROCE=@PROCE AND ITEM_ADJ.PORCEN <> 0"
 sConsulta = sConsulta & vbCrLf & "                    "
 sConsulta = sConsulta & vbCrLf & "                    "
 sConsulta = sConsulta & vbCrLf & "OPEN Cur_ITEM_ADJ"
 sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM Cur_ITEM_ADJ INTO @ITEM,@PROVE,@FECINI,@FECFIN,@PRECADJ,@CANTADJ,@PRECAPE"
 sConsulta = sConsulta & vbCrLf & ",@PAG,@DENPAG,@UNI,@DENUNI,@DEST,@DENDEST"
 sConsulta = sConsulta & vbCrLf & ",@ART,@DESCR,@CANT,@PRES1,@DENPRES1,@PRES2,@DENPRES2,@PRES3,@DENPRES3,@PRES4,@DENPRES4"
 sConsulta = sConsulta & vbCrLf & "WHILE @@FETCH_STATUS=0"
 sConsulta = sConsulta & vbCrLf & "BEGIN"
 sConsulta = sConsulta & vbCrLf & " "
 sConsulta = sConsulta & vbCrLf & "/* Tenemos que calcular el porcentaje correspondiente a cada mes del ANYO.*/"
 sConsulta = sConsulta & vbCrLf & "SET @TOTALDIAS = datediff(day,@FECINI, @FECFIN) + 1"
 sConsulta = sConsulta & vbCrLf & "    "
 sConsulta = sConsulta & vbCrLf & "SET @FECINICIAL = @FECINI"
 sConsulta = sConsulta & vbCrLf & "SET @FECFINAL = @FECFIN"
 sConsulta = sConsulta & vbCrLf & "SET @MESINI = MONTH(@FECINICIAL)"
 sConsulta = sConsulta & vbCrLf & "SET @FECHA = @FECINICIAL"
 sConsulta = sConsulta & vbCrLf & "            "
 sConsulta = sConsulta & vbCrLf & "WHILE @MESINI < MONTH(@FECFINAL)"
 sConsulta = sConsulta & vbCrLf & "BEGIN"
 sConsulta = sConsulta & vbCrLf & "/* Calculamos la diferencia en d�as entre el mes inicial y el siguiente mes*/  "
 sConsulta = sConsulta & vbCrLf & "SET @DIAS = datediff(day,@FECHA,cast( str (month(@FECHA) +1) + '/1/' + str(year(@FECHA) ) as datetime))"
 sConsulta = sConsulta & vbCrLf & "SET @PORCEN = @DIAS / @TOTALDIAS"
 sConsulta = sConsulta & vbCrLf & "SET @FECHA = cast(str((month(@FECHA) +1)) + '/1/' + str(year(@FECHA)) as datetime)"
 sConsulta = sConsulta & vbCrLf & "  "
 sConsulta = sConsulta & vbCrLf & "/*Miramos si hab�a alg�n resultado PRESa ese mes*/"
 sConsulta = sConsulta & vbCrLf & "IF (SELECT COUNT(*) FROM AR_ITEM_MES_PRESCON3_4 WHERE"
 sConsulta = sConsulta & vbCrLf & "ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM AND ANYOADJ=YEAR(@FECINICIAL) AND MES=@MESINI AND PRES1=@PRES1 AND PRES2=@PRES2 AND PRES3=@PRES3 AND PRES4=@PRES4)=0"
 sConsulta = sConsulta & vbCrLf & "BEGIN"
 sConsulta = sConsulta & vbCrLf & "    "
 sConsulta = sConsulta & vbCrLf & "INSERT INTO  AR_ITEM_MES_PRESCON3_4 ( ANYO,GMN1,PROCE,ITEM,ANYOADJ,MES,DENGMN1,ADJDIR,GMN2,GMN3,GMN4,DENGMN2,DENGMN3,DENGMN4"
 sConsulta = sConsulta & vbCrLf & ",ART,DESCR,CANT"
 sConsulta = sConsulta & vbCrLf & ",UNI,DENUNI,DEST,DENDEST,PAG,DENPAG,FECINI,FECFIN,PRES,PREC,CANTPERIODO,PRES1,DENPRES1,PRES2,DENPRES2,PRES3,DENPRES3,PRES4,DENPRES4)"
 sConsulta = sConsulta & vbCrLf & " "
 sConsulta = sConsulta & vbCrLf & "VALUES(@ANYO,@GMN1, @PROCE , @ITEM,YEAR(@FECINICIAL),@MESINI, @DENGMN1,@ADJDIR, @GMN2, @GMN3, @GMN4 ,@DENGMN2, @DENGMN3, @DENGMN4"
 sConsulta = sConsulta & vbCrLf & ",@ART,@DESCR,@CANT"
 sConsulta = sConsulta & vbCrLf & ",@UNI,@DENUNI,@DEST,@DENDEST,@PAG,@DENPAG,@FECINI,@FECFIN,@PRECAPE*@PORCEN*@CANTADJ,@PRECADJ*@PORCEN*@CANTADJ,@CANT*@PORCEN,@PRES1,@DENPRES1,@PRES2,@DENPRES2,@PRES3,@DENPRES3,@PRES4,@DENPRES4)"
 sConsulta = sConsulta & vbCrLf & "    "
 sConsulta = sConsulta & vbCrLf & "End"
 sConsulta = sConsulta & vbCrLf & "Else"
 sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_4 SET PRES=PRES+@PRECAPE*@PORCEN*@CANTADJ, PREC=PREC+@PRECADJ*@PORCEN*@CANTADJ"
 sConsulta = sConsulta & vbCrLf & "WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND MES=@MESINI AND ANYOADJ=YEAR(@FECINICIAL)  AND ITEM=@ITEM AND PRES1=@PRES1 AND PRES2=@PRES2 AND PRES3=@PRES3 AND PRES4=@PRES4"
 sConsulta = sConsulta & vbCrLf & "  "
 sConsulta = sConsulta & vbCrLf & "SET @MESINI=@MESINI + 1"
 sConsulta = sConsulta & vbCrLf & "  "
 sConsulta = sConsulta & vbCrLf & "  "
 sConsulta = sConsulta & vbCrLf & "End"
 sConsulta = sConsulta & vbCrLf & ""
 sConsulta = sConsulta & vbCrLf & "/*Si el m�s es el mismo*/"
 sConsulta = sConsulta & vbCrLf & "IF @MESINI = MONTH(@FECFINAL)"
 sConsulta = sConsulta & vbCrLf & "BEGIN"
 sConsulta = sConsulta & vbCrLf & ""
 sConsulta = sConsulta & vbCrLf & "SET @DIAS = datediff(day,@FECHA,@FECFINAL) + 1"
 sConsulta = sConsulta & vbCrLf & "SET @PORCEN = @DIAS/@TOTALDIAS"
 sConsulta = sConsulta & vbCrLf & ""
 sConsulta = sConsulta & vbCrLf & "/*Miramos si hab�a alg�n resultado PRES a ese mes*/"
 sConsulta = sConsulta & vbCrLf & "IF (SELECT COUNT(*) FROM AR_ITEM_MES_PRESCON3_4 WHERE"
 sConsulta = sConsulta & vbCrLf & "ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM AND ANYOADJ=YEAR(@FECINICIAL) AND MES=@MESINI AND PRES1=@PRES1 AND PRES2=@PRES2 AND PRES3=@PRES3 AND PRES4=@PRES4)=0"
 sConsulta = sConsulta & vbCrLf & "BEGIN"
 sConsulta = sConsulta & vbCrLf & "    "
 sConsulta = sConsulta & vbCrLf & "INSERT INTO  AR_ITEM_MES_PRESCON3_4 ( ANYO, GMN1, PROCE, ITEM, ANYOADJ, MES, DENGMN1, ADJDIR, GMN2, GMN3, GMN4, DENGMN2, DENGMN3,DENGMN4"
 sConsulta = sConsulta & vbCrLf & ", ART, DESCR, CANT, UNI, DENUNI, DEST, DENDEST, PAG, DENPAG, FECINI, FECFIN, PRES, PREC, CANTPERIODO, PRES1, DENPRES1,PRES2,DENPRES2,PRES3,DENPRES3,PRES4,DENPRES4)"
 sConsulta = sConsulta & vbCrLf & "VALUES(@ANYO,@GMN1, @PROCE , @ITEM,YEAR(@FECINICIAL),@MESINI, @DENGMN1,@ADJDIR, @GMN2, @GMN3, @GMN4 ,@DENGMN2, @DENGMN3, @DENGMN4"
 sConsulta = sConsulta & vbCrLf & ",@ART,@DESCR,@CANT,@UNI,@DENUNI,@DEST,@DENDEST,@PAG,@DENPAG,@FECINI,@FECFIN, @PRECAPE*@PORCEN*@CANTADJ,@PRECADJ*@PORCEN*@CANTADJ,@CANT*@PORCEN,@PRES1,@DENPRES1,@PRES2,@DENPRES2,@PRES3,@DENPRES3,@PRES4,@DENPRES4)"
 sConsulta = sConsulta & vbCrLf & "    "
 sConsulta = sConsulta & vbCrLf & "End"
 sConsulta = sConsulta & vbCrLf & "Else"
 sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_4 SET PRES=PRES+@PRECAPE*@PORCEN*@CANTADJ, PREC=PREC+@PRECADJ*@PORCEN*@CANTADJ"
 sConsulta = sConsulta & vbCrLf & "WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND MES=@MESINI AND ANYOADJ=YEAR(@FECINICIAL)  AND ITEM=@ITEM AND PRES1=@PRES1 AND PRES2=@PRES2 AND PRES3=@PRES3 AND PRES4=@PRES4"
 sConsulta = sConsulta & vbCrLf & ""
 sConsulta = sConsulta & vbCrLf & ""
 sConsulta = sConsulta & vbCrLf & "End"
 sConsulta = sConsulta & vbCrLf & " "
 sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM Cur_ITEM_ADJ INTO @ITEM,@PROVE,@FECINI,@FECFIN,@PRECADJ,@CANTADJ,@PRECAPE"
 sConsulta = sConsulta & vbCrLf & ",@PAG,@DENPAG,@UNI,@DENUNI,@DEST,@DENDEST"
 sConsulta = sConsulta & vbCrLf & ",@ART,@DESCR,@CANT,@PRES1,@DENPRES1,@PRES2,@DENPRES2,@PRES3,@DENPRES3,@PRES4,@DENPRES4"
 sConsulta = sConsulta & vbCrLf & "End"
 sConsulta = sConsulta & vbCrLf & "ERR_:"
 sConsulta = sConsulta & vbCrLf & "Close Cur_PROCE"
 sConsulta = sConsulta & vbCrLf & "DEALLOCATE Cur_PROCE"
 sConsulta = sConsulta & vbCrLf & "Close Cur_ITEM_ADJ"
 sConsulta = sConsulta & vbCrLf & "DEALLOCATE Cur_ITEM_ADJ"
 sConsulta = sConsulta & vbCrLf & "    "
 sConsulta = sConsulta & vbCrLf & "SET CONCAT_NULL_YIELDS_NULL ON"
 sConsulta = sConsulta & vbCrLf & "SET @RES=@@ERROR"
 sConsulta = sConsulta & vbCrLf & "Return"
 sConsulta = sConsulta & vbCrLf & "End"

 CrearSP_AR_ITEM_MES_PRESCON3_4 = sConsulta
End Function

Private Function CrearSP_AR_ITEM_PRESCON3() As String
Dim sConsulta As String

 sConsulta = "CREATE PROCEDURE SP_AR_ITEM_PRESCON3 (@ANYO SMALLINT,@GMN1 VARCHAR(50), @PROCE INT,@RES INT OUTPUT)  AS"
 sConsulta = sConsulta & vbCrLf & "BEGIN"
 sConsulta = sConsulta & vbCrLf & "DECLARE @ITEM AS INTEGER"
 sConsulta = sConsulta & vbCrLf & "DECLARE @DENGMN1 AS VARCHAR(100)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @DENPRES1 AS VARCHAR(100)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @DENPRES2 AS VARCHAR(100)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @DENPRES3 AS VARCHAR(100)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @DENPRES4 AS VARCHAR(100)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @PROCECAMBIO AS FLOAT"
 sConsulta = sConsulta & vbCrLf & "DECLARE @ADJDIR AS BIT"
 sConsulta = sConsulta & vbCrLf & "DECLARE @PRES1 AS VARCHAR (50)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @PRES2 AS VARCHAR (50)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @PRES3 AS VARCHAR (50)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @PRES4 AS VARCHAR (50)"
 sConsulta = sConsulta & vbCrLf & ""
 sConsulta = sConsulta & vbCrLf & "SET CONCAT_NULL_YIELDS_NULL OFF"
 sConsulta = sConsulta & vbCrLf & ""
 sConsulta = sConsulta & vbCrLf & "DECLARE Cur_PROCE CURSOR FOR SELECT GMN1.DEN,PROCE.CAMBIO,PROCE.ADJDIR"
 sConsulta = sConsulta & vbCrLf & "From Proce, gmn1"
 sConsulta = sConsulta & vbCrLf & "WHERE PROCE.ANYO=@ANYO AND PROCE.GMN1=@GMN1 AND PROCE.COD=@PROCE"
 sConsulta = sConsulta & vbCrLf & "AND GMN1.COD=PROCE.GMN1"
 sConsulta = sConsulta & vbCrLf & "OPEN Cur_PROCE"
 sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM cur_PROCE INTO @DENGMN1,@PROCECAMBIO,@ADJDIR"
 sConsulta = sConsulta & vbCrLf & "IF @@FETCH_STATUS<>0 GOTO ERR_"
 sConsulta = sConsulta & vbCrLf & ""
 sConsulta = sConsulta & vbCrLf & ""
 sConsulta = sConsulta & vbCrLf & "DECLARE Cur_ITEM Cursor FOR SELECT DISTINCT ITEM_ADJ.ITEM,ITEM_PRESCON3.PRES1,PRES_3_CON1.DEN,ITEM_PRESCON3.PRES2,PRES_3_CON2.DEN,ITEM_PRESCON3.PRES3,PRES_3_CON3.DEN, ITEM_PRESCON3.PRES4,PRES_3_CON4.DEN"
 sConsulta = sConsulta & vbCrLf & "From ITEM_ADJ"
 sConsulta = sConsulta & vbCrLf & "INNER JOIN ITEM ON ITEM_ADJ.ANYO=ITEM.ANYO AND ITEM_ADJ.GMN1=ITEM.GMN1 AND ITEM_ADJ.PROCE=ITEM.PROCE  AND ITEM_ADJ.ITEM=ITEM.ID"
 sConsulta = sConsulta & vbCrLf & "INNER JOIN ITEM_PRESCON3 ON ITEM.ANYO=ITEM_PRESCON3.ANYO AND ITEM.GMN1=ITEM_PRESCON3.GMN1 AND ITEM.PROCE=ITEM_PRESCON3.PROCE AND ITEM.ID=ITEM_PRESCON3.ITEM AND ITEM_PRESCON3.PRES4 IS NOT NULL"
 sConsulta = sConsulta & vbCrLf & "INNER JOIN PRES_3_CON1 ON ITEM_PRESCON3.PRES1=PRES_3_CON1.COD"
 sConsulta = sConsulta & vbCrLf & "LEFT JOIN PRES_3_CON2 ON ITEM_PRESCON3.PRES1=PRES_3_CON2.PRES1 AND ITEM_PRESCON3.PRES2=PRES_3_CON2.COD"
 sConsulta = sConsulta & vbCrLf & "LEFT JOIN PRES_3_CON3 ON ITEM_PRESCON3.PRES1=PRES_3_CON3.PRES1 AND ITEM_PRESCON3.PRES2=PRES_3_CON3.PRES2 AND ITEM_PRESCON3.PRES3 =PRES_3_CON3.COD"
 sConsulta = sConsulta & vbCrLf & "LEFT JOIN PRES_3_CON4 ON ITEM_PRESCON3.PRES1=PRES_3_CON4.PRES1 AND ITEM_PRESCON3.PRES2=PRES_3_CON4.PRES2 AND ITEM_PRESCON3.PRES3 =PRES_3_CON4.PRES3 AND ITEM_PRESCON3.PRES4 = PRES_3_CON4.COD"
 sConsulta = sConsulta & vbCrLf & "INNER JOIN PROCE_OFE ON ITEM_ADJ.ANYO=PROCE_OFE.ANYO AND ITEM_ADJ.GMN1=PROCE_OFE.GMN1 AND ITEM_ADJ.PROCE=PROCE_OFE.PROCE AND ITEM_ADJ.OFE=PROCE_OFE.OFE AND ITEM_ADJ.PROVE=PROCE_OFE.PROVE"
 sConsulta = sConsulta & vbCrLf & "INNER JOIN ITEM_OFE ON ITEM_ADJ.ANYO=ITEM_OFE.ANYO AND ITEM_OFE.GMN1=ITEM_OFE.GMN1 AND ITEM_ADJ.PROCE=ITEM_OFE.PROCE AND ITEM_ADJ.PROVE=ITEM_OFE.PROVE AND ITEM_ADJ.ITEM=ITEM_OFE.ITEM AND ITEM_ADJ.OFE=ITEM_OFE.NUM"
 sConsulta = sConsulta & vbCrLf & "LEFT JOIN ART4 ON ITEM.ART=ART4.COD AND ITEM.GMN1=ART4.GMN1 AND ITEM.GMN2=ART4.GMN2 AND ITEM.GMN3=ART4.GMN3 AND ITEM.GMN4=ART4.GMN4"
 sConsulta = sConsulta & vbCrLf & "WHERE ITEM_ADJ.ANYO=@ANYO AND ITEM_ADJ.GMN1=@GMN1 AND ITEM_ADJ.PROCE=@PROCE AND ITEM_ADJ.PORCEN <> 0"
 sConsulta = sConsulta & vbCrLf & "OPEN Cur_ITEM"
 sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM Cur_ITEM INTO @ITEM,@PRES1,@DENPRES1,@PRES2,@DENPRES2,@PRES3,@DENPRES3,@PRES4,@DENPRES4"
 sConsulta = sConsulta & vbCrLf & "WHILE @@FETCH_STATUS=0"
 sConsulta = sConsulta & vbCrLf & ""
 sConsulta = sConsulta & vbCrLf & "BEGIN"
 sConsulta = sConsulta & vbCrLf & "INSERT INTO  AR_ITEM_PRESCON3 ( ANYO,GMN1,PROCE,ITEM,PRES1,PRES2,PRES3,PRES4,DENPRES1,DENPRES2,DENPRES3,DENPRES4"
 sConsulta = sConsulta & vbCrLf & ",DENGMN1,ADJDIR,ART,DESCR,CANT,PRES,PREC )    "
 sConsulta = sConsulta & vbCrLf & "SELECT  @ANYO , @GMN1, @PROCE , @ITEM, @PRES1,@PRES2,@PRES3,@PRES4,@DENPRES1,@DENPRES2, @DENPRES3, @DENPRES4"
 sConsulta = sConsulta & vbCrLf & ",@DENGMN1,@ADJDIR, ITEM.ART,ITEM.DESCR + ART4.DEN,ITEM.CANT"
 sConsulta = sConsulta & vbCrLf & ",(SELECT SUM((ITEM_ADJ.PORCEN/100)*ITEM.CANT*ITEM.PREC/@PROCECAMBIO)"
 sConsulta = sConsulta & vbCrLf & "AS PRES FROM ITEM_ADJ INNER JOIN ITEM"
 sConsulta = sConsulta & vbCrLf & "ON ITEM_ADJ.ANYO=ITEM.ANYO AND ITEM_ADJ.GMN1=ITEM.GMN1 AND ITEM_ADJ.PROCE=ITEM.PROCE AND ITEM_ADJ.ITEM=ITEM.ID AND ITEM_ADJ.ITEM=@ITEM"
 sConsulta = sConsulta & vbCrLf & "AND ITEM_ADJ.ANYO=@ANYO AND ITEM_ADJ.GMN1=@GMN1 AND ITEM_ADJ.PROCE=@PROCE AND ITEM_ADJ.PORCEN <> 0 )"
 sConsulta = sConsulta & vbCrLf & ",(SELECT SUM((ITEM_ADJ.PORCEN/100)*ITEM.CANT*ITEM_OFE.PRECIO/(PROCE_OFE.CAMBIO*@PROCECAMBIO))"
 sConsulta = sConsulta & vbCrLf & "AS PREC FROM ITEM_ADJ INNER JOIN ITEM"
 sConsulta = sConsulta & vbCrLf & "ON ITEM_ADJ.ANYO=ITEM.ANYO AND ITEM_ADJ.GMN1=ITEM.GMN1 AND ITEM_ADJ.PROCE=ITEM.PROCE AND ITEM_ADJ.ITEM=ITEM.ID AND ITEM_ADJ.ITEM=@ITEM"
 sConsulta = sConsulta & vbCrLf & "AND ITEM_ADJ.ANYO=@ANYO AND ITEM_ADJ.GMN1=@GMN1 AND ITEM_ADJ.PROCE=@PROCE AND ITEM_ADJ.PORCEN <> 0"
 sConsulta = sConsulta & vbCrLf & "INNER JOIN ITEM_OFE"
 sConsulta = sConsulta & vbCrLf & "ON ITEM_ADJ.ANYO=ITEM_OFE.ANYO AND ITEM_ADJ.GMN1=ITEM_OFE.GMN1 AND ITEM_ADJ.PROCE=ITEM_OFE.PROCE"
 sConsulta = sConsulta & vbCrLf & "AND ITEM_ADJ.OFE=ITEM_OFE.NUM AND ITEM_ADJ.PROVE=ITEM_OFE.PROVE"
 sConsulta = sConsulta & vbCrLf & "AND ITEM_ADJ.ITEM=ITEM_OFE.ITEM"
 sConsulta = sConsulta & vbCrLf & "INNER JOIN PROCE_OFE"
 sConsulta = sConsulta & vbCrLf & "ON ITEM_OFE.ANYO=PROCE_OFE.ANYO AND ITEM_OFE.GMN1=PROCE_OFE.GMN1 AND ITEM_OFE.PROCE=PROCE_OFE.PROCE AND ITEM_OFE.NUM=PROCE_OFE.OFE"
 sConsulta = sConsulta & vbCrLf & "AND ITEM_OFE.PROVE=PROCE_OFE.PROVE)"
 sConsulta = sConsulta & vbCrLf & "From Item"
 sConsulta = sConsulta & vbCrLf & "LEFT JOIN ART4 ON ITEM.ART=ART4.COD AND ITEM.GMN1=ART4.GMN1 AND ITEM.GMN2=ART4.GMN2 AND ITEM.GMN3=ART4.GMN3 AND ITEM.GMN4=ART4.GMN4"
 sConsulta = sConsulta & vbCrLf & "WHERE ITEM.ANYO=@ANYO AND ITEM.GMN1=@GMN1 AND ITEM.PROCE=@PROCE AND ITEM.ID=@ITEM"
 sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM Cur_ITEM INTO @ITEM,@PRES1,@DENPRES1,@PRES2,@DENPRES2,@PRES3,@DENPRES3,@PRES4,@DENPRES4"
 sConsulta = sConsulta & vbCrLf & "End"
 sConsulta = sConsulta & vbCrLf & ""
 sConsulta = sConsulta & vbCrLf & "ERR_:"
 sConsulta = sConsulta & vbCrLf & "Close Cur_PROCE"
 sConsulta = sConsulta & vbCrLf & "DEALLOCATE Cur_PROCE"
 sConsulta = sConsulta & vbCrLf & "Close Cur_ITEM"
 sConsulta = sConsulta & vbCrLf & "DEALLOCATE Cur_ITEM"
 sConsulta = sConsulta & vbCrLf & "SET CONCAT_NULL_YIELDS_NULL ON"
 sConsulta = sConsulta & vbCrLf & "SELECT @RES=@@ERROR"
 sConsulta = sConsulta & vbCrLf & "Return"
 sConsulta = sConsulta & vbCrLf & "End"

CrearSP_AR_ITEM_PRESCON3 = sConsulta

End Function


Private Function CrearSP_AR_ITEM_MES_PRESCON4_1() As String
Dim sConsulta As String

 sConsulta = "CREATE PROCEDURE SP_AR_ITEM_MES_PRESCON4_1 (@ANYO SMALLINT,@GMN1 VARCHAR(50), @PROCE INT,@RES INT OUTPUT)  AS"
 sConsulta = sConsulta & vbCrLf & "BEGIN"
 sConsulta = sConsulta & vbCrLf & "DECLARE @FECINI AS DATETIME"
 sConsulta = sConsulta & vbCrLf & "DECLARE @FECFIN AS DATETIME"
 sConsulta = sConsulta & vbCrLf & "DECLARE @PRECADJ AS FLOAT"
 sConsulta = sConsulta & vbCrLf & "DECLARE @CANTADJ AS FLOAT"
 sConsulta = sConsulta & vbCrLf & "DECLARE @PRECAPE AS FLOAT"
 sConsulta = sConsulta & vbCrLf & "DECLARE @DENGMN1 AS VARCHAR(100)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @DENGMN2 AS VARCHAR(100)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @DENGMN3 AS VARCHAR(100)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @DENGMN4 AS VARCHAR(100)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @PROCECAMBIO AS FLOAT"
 sConsulta = sConsulta & vbCrLf & "DECLARE @ADJDIR AS BIT"
 sConsulta = sConsulta & vbCrLf & "DECLARE @GMN2 AS VARCHAR (50)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @GMN3 AS VARCHAR (50)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @GMN4 AS VARCHAR (50)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @PORCEN AS FLOAT"
 sConsulta = sConsulta & vbCrLf & "DECLARE @MESINI AS INTEGER"
 sConsulta = sConsulta & vbCrLf & "DECLARE @TOTALDIAS AS FLOAT"
 sConsulta = sConsulta & vbCrLf & "DECLARE @DIAS AS FLOAT"
 sConsulta = sConsulta & vbCrLf & "DECLARE @PAG AS VARCHAR(50)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @DENPAG AS VARCHAR(100)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @UNI AS VARCHAR(50)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @DENUNI AS VARCHAR(100)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @DEST AS VARCHAR(50)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @DENDEST AS VARCHAR(100)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @ART AS VARCHAR(50)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @DESCR AS VARCHAR(100)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @CANT AS FLOAT"
 sConsulta = sConsulta & vbCrLf & "DECLARE @FECHA AS DATETIME"
 sConsulta = sConsulta & vbCrLf & "DECLARE @ITEM AS INTEGER"
 sConsulta = sConsulta & vbCrLf & "DECLARE @PROVE AS VARCHAR(50)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @FEC AS DATETIME"
 sConsulta = sConsulta & vbCrLf & "DECLARE @FECFINAL AS DATETIME"
 sConsulta = sConsulta & vbCrLf & "DECLARE @ANYOINI AS INTEGER"
 sConsulta = sConsulta & vbCrLf & "DECLARE @ANYOFIN AS INTEGER"
 sConsulta = sConsulta & vbCrLf & "DECLARE @ANYOACTUAL AS INTEGER"
 sConsulta = sConsulta & vbCrLf & "DECLARE @PRES1 AS VARCHAR (50)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @DENPRES1 AS VARCHAR(100)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @FECINICIAL AS DATETIME"
 sConsulta = sConsulta & vbCrLf & "SET CONCAT_NULL_YIELDS_NULL OFF"
 sConsulta = sConsulta & vbCrLf & "DECLARE Cur_PROCE CURSOR FORWARD_ONLY STATIC READ_ONLY FOR"
 sConsulta = sConsulta & vbCrLf & "SELECT GMN1.DEN,PROCE.GMN2, PROCE.GMN3, PROCE.GMN4"
 sConsulta = sConsulta & vbCrLf & ",GMN2.DEN, GMN3.DEN, GMN4.DEN, PROCE.CAMBIO,PROCE.ADJDIR"
 sConsulta = sConsulta & vbCrLf & "From Proce"
 sConsulta = sConsulta & vbCrLf & "INNER JOIN GMN1 ON PROCE.GMN1=GMN1.COD"
 sConsulta = sConsulta & vbCrLf & "INNER JOIN GMN2 ON  PROCE.GMN1=GMN2.GMN1 AND PROCE.GMN2=GMN2.COD"
 sConsulta = sConsulta & vbCrLf & "INNER JOIN GMN3 ON  PROCE.GMN1=GMN3.GMN1 AND  PROCE.GMN2=GMN3.GMN2 AND PROCE.GMN3=GMN3.COD"
 sConsulta = sConsulta & vbCrLf & "INNER JOIN GMN4 ON  PROCE.GMN1=GMN4.GMN1 AND  PROCE.GMN2=GMN4.GMN2 AND PROCE.GMN3=GMN4.GMN3 AND PROCE.GMN4=GMN4.COD"
 sConsulta = sConsulta & vbCrLf & "WHERE PROCE.ANYO=@ANYO AND PROCE.GMN1=@GMN1 AND PROCE.COD=@PROCE"
 sConsulta = sConsulta & vbCrLf & "OPEN Cur_PROCE"
 sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM cur_PROCE INTO @DENGMN1,@GMN2,@GMN3,@GMN4,@DENGMN2,@DENGMN3,@DENGMN4,@PROCECAMBIO,@ADJDIR"
 sConsulta = sConsulta & vbCrLf & "IF @@FETCH_STATUS<>0 GOTO ERR_"
 sConsulta = sConsulta & vbCrLf & "DECLARE Cur_ITEM_ADJ Cursor FORWARD_ONLY STATIC READ_ONLY FOR"
 sConsulta = sConsulta & vbCrLf & "SELECT ITEM_ADJ.ITEM,ITEM_ADJ.PROVE,ITEM.FECINI,ITEM.FECFIN,(ITEM_OFE.PRECIO/(PROCE_OFE.CAMBIO*@PROCECAMBIO)) AS PRECADJ,(ITEM.CANT*(ITEM_ADJ.PORCEN/100)) AS CANTADJ,(ITEM.PREC/@PROCECAMBIO) AS PRECAPE"
 sConsulta = sConsulta & vbCrLf & ",ITEM.PAG,PAG.DEN,ITEM.UNI,UNI.DEN,ITEM.DEST,DEST.DEN"
 sConsulta = sConsulta & vbCrLf & ",ITEM.ART,ITEM.DESCR + ART4.DEN,ITEM.CANT,ITEM_PRESCON4.PRES1,PRES_4_CON1.DEN"
 sConsulta = sConsulta & vbCrLf & "From ITEM_ADJ"
 sConsulta = sConsulta & vbCrLf & "INNER JOIN ITEM ON ITEM_ADJ.ANYO=ITEM.ANYO AND ITEM_ADJ.GMN1=ITEM.GMN1 AND ITEM_ADJ.PROCE=ITEM.PROCE  AND ITEM_ADJ.ITEM=ITEM.ID"
 sConsulta = sConsulta & vbCrLf & "INNER JOIN ITEM_PRESCON4 ON ITEM.ANYO=ITEM_PRESCON4.ANYO AND ITEM.GMN1=ITEM_PRESCON4.GMN1 AND ITEM.PROCE=ITEM_PRESCON4.PROCE AND ITEM.ID=ITEM_PRESCON4.ITEM"
 sConsulta = sConsulta & vbCrLf & "INNER JOIN PRES_4_CON1 ON ITEM_PRESCON4.PRES1=PRES_4_CON1.COD"
 sConsulta = sConsulta & vbCrLf & "INNER JOIN PROCE_OFE ON ITEM_ADJ.ANYO=PROCE_OFE.ANYO AND ITEM_ADJ.GMN1=PROCE_OFE.GMN1 AND ITEM_ADJ.PROCE=PROCE_OFE.PROCE AND ITEM_ADJ.OFE=PROCE_OFE.OFE AND ITEM_ADJ.PROVE=PROCE_OFE.PROVE"
 sConsulta = sConsulta & vbCrLf & "INNER JOIN ITEM_OFE ON ITEM_ADJ.ANYO=ITEM_OFE.ANYO AND ITEM_OFE.GMN1=ITEM_OFE.GMN1 AND ITEM_ADJ.PROCE=ITEM_OFE.PROCE AND ITEM_ADJ.PROVE=ITEM_OFE.PROVE AND ITEM_ADJ.ITEM=ITEM_OFE.ITEM AND ITEM_ADJ.OFE=ITEM_OFE.NUM"
 sConsulta = sConsulta & vbCrLf & "INNER JOIN UNI ON ITEM.UNI=UNI.COD"
 sConsulta = sConsulta & vbCrLf & "INNER JOIN DEST ON ITEM.DEST=DEST.COD"
 sConsulta = sConsulta & vbCrLf & "INNER JOIN PAG ON ITEM.PAG=PAG.COD"
 sConsulta = sConsulta & vbCrLf & "LEFT JOIN ART4 ON ITEM.ART=ART4.COD AND ITEM.GMN1=ART4.GMN1 AND ITEM.GMN2=ART4.GMN2 AND ITEM.GMN3=ART4.GMN3 AND ITEM.GMN4=ART4.GMN4"
 sConsulta = sConsulta & vbCrLf & "WHERE ITEM_ADJ.ANYO=@ANYO AND ITEM_ADJ.GMN1=@GMN1 AND ITEM_ADJ.PROCE=@PROCE AND ITEM_ADJ.PORCEN <> 0"
 sConsulta = sConsulta & vbCrLf & "OPEN Cur_ITEM_ADJ"
 sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM Cur_ITEM_ADJ INTO @ITEM,@PROVE,@FECINI,@FECFIN,@PRECADJ,@CANTADJ,@PRECAPE"
 sConsulta = sConsulta & vbCrLf & ",@PAG,@DENPAG,@UNI,@DENUNI,@DEST,@DENDEST"
 sConsulta = sConsulta & vbCrLf & ",@ART,@DESCR,@CANT,@PRES1,@DENPRES1"
 sConsulta = sConsulta & vbCrLf & "WHILE @@FETCH_STATUS=0"
 sConsulta = sConsulta & vbCrLf & "BEGIN"
 sConsulta = sConsulta & vbCrLf & "/* Tenemos que calcular el porcentaje correspondiente a cada mes del ANYO.*/"
 sConsulta = sConsulta & vbCrLf & "SET @TOTALDIAS = datediff(day,@FECINI, @FECFIN) + 1"
 sConsulta = sConsulta & vbCrLf & "SET @FECINICIAL = @FECINI"
 sConsulta = sConsulta & vbCrLf & "SET @FECFINAL = @FECFIN"
 sConsulta = sConsulta & vbCrLf & "SET @MESINI = MONTH(@FECINICIAL)"
 sConsulta = sConsulta & vbCrLf & "SET @FECHA = @FECINICIAL"
 sConsulta = sConsulta & vbCrLf & "WHILE @MESINI < MONTH(@FECFINAL)"
 sConsulta = sConsulta & vbCrLf & "BEGIN"
 sConsulta = sConsulta & vbCrLf & "/* Calculamos la diferencia en d�as entre el mes inicial y el siguiente mes*/  "
 sConsulta = sConsulta & vbCrLf & "SET @DIAS = datediff(day,@FECHA,cast( str (month(@FECHA) +1) + '/1/' + str(year(@FECHA) ) as datetime))"
 sConsulta = sConsulta & vbCrLf & "SET @PORCEN = @DIAS / @TOTALDIAS"
 sConsulta = sConsulta & vbCrLf & "SET @FECHA = cast(str((month(@FECHA) +1)) + '/1/' + str(year(@FECHA)) as datetime)"
 sConsulta = sConsulta & vbCrLf & "/*Miramos si hab�a alg�n resultado para ese mes*/"
 sConsulta = sConsulta & vbCrLf & "IF (SELECT COUNT(*) FROM AR_ITEM_MES_PRESCON4_1 WHERE"
 sConsulta = sConsulta & vbCrLf & "ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM AND ANYOADJ=YEAR(@FECINICIAL) AND MES=@MESINI AND PRES1=@PRES1)=0"
 sConsulta = sConsulta & vbCrLf & "BEGIN"
 sConsulta = sConsulta & vbCrLf & "INSERT INTO  AR_ITEM_MES_PRESCON4_1 ( ANYO,GMN1,PROCE,ITEM,ANYOADJ,MES,DENGMN1,ADJDIR,GMN2,GMN3,GMN4,DENGMN2,DENGMN3,DENGMN4"
 sConsulta = sConsulta & vbCrLf & ",ART,DESCR,CANT"
 sConsulta = sConsulta & vbCrLf & ",UNI,DENUNI,DEST,DENDEST,PAG,DENPAG,FECINI,FECFIN,PRES,PREC,CANTPERIODO,PRES1,DENPRES1)"
 sConsulta = sConsulta & vbCrLf & "VALUES(@ANYO,@GMN1, @PROCE , @ITEM,YEAR(@FECINICIAL),@MESINI, @DENGMN1,@ADJDIR, @GMN2, @GMN3, @GMN4 ,@DENGMN2, @DENGMN3, @DENGMN4"
 sConsulta = sConsulta & vbCrLf & ",@ART,@DESCR,@CANT"
 sConsulta = sConsulta & vbCrLf & ",@UNI,@DENUNI,@DEST,@DENDEST,@PAG,@DENPAG,@FECINI,@FECFIN,@PRECAPE*@PORCEN*@CANTADJ,@PRECADJ*@PORCEN*@CANTADJ,@CANT*@PORCEN,@PRES1,@DENPRES1)"
 sConsulta = sConsulta & vbCrLf & "End"
 sConsulta = sConsulta & vbCrLf & "Else"
 sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_1 SET PRES=PRES+@PRECAPE*@PORCEN*@CANTADJ, PREC=PREC+@PRECADJ*@PORCEN*@CANTADJ"
 sConsulta = sConsulta & vbCrLf & "WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND MES=@MESINI AND ANYOADJ=YEAR(@FECINICIAL)  AND ITEM=@ITEM AND PRES1=@PRES1"
 sConsulta = sConsulta & vbCrLf & "SET @MESINI=@MESINI + 1"
 sConsulta = sConsulta & vbCrLf & "End"
 sConsulta = sConsulta & vbCrLf & "/*Si el m�s es el mismo*/"
 sConsulta = sConsulta & vbCrLf & "IF @MESINI = MONTH(@FECFINAL)"
 sConsulta = sConsulta & vbCrLf & "BEGIN"
 sConsulta = sConsulta & vbCrLf & "SET @DIAS = datediff(day,@FECHA,@FECFINAL) + 1"
 sConsulta = sConsulta & vbCrLf & "SET @PORCEN = @DIAS/@TOTALDIAS"
 sConsulta = sConsulta & vbCrLf & "/*Miramos si hab�a alg�n resultado para ese mes*/"
 sConsulta = sConsulta & vbCrLf & "IF (SELECT COUNT(*) FROM AR_ITEM_MES_PRESCON4_1 WHERE"
 sConsulta = sConsulta & vbCrLf & ""
 sConsulta = sConsulta & vbCrLf & "ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM AND ANYOADJ=YEAR(@FECINICIAL) AND MES=@MESINI AND PRES1=@PRES1)=0"
 sConsulta = sConsulta & vbCrLf & "BEGIN"
 sConsulta = sConsulta & vbCrLf & "INSERT INTO  AR_ITEM_MES_PRESCON4_1 ( ANYO, GMN1, PROCE, ITEM, ANYOADJ, MES, DENGMN1, ADJDIR, GMN2, GMN3, GMN4, DENGMN2, DENGMN3,DENGMN4"
 sConsulta = sConsulta & vbCrLf & ", ART, DESCR, CANT, UNI, DENUNI, DEST, DENDEST, PAG, DENPAG, FECINI, FECFIN, PRES, PREC, CANTPERIODO, PRES1, DENPRES1)"
 sConsulta = sConsulta & vbCrLf & "VALUES(@ANYO,@GMN1, @PROCE , @ITEM,YEAR(@FECINICIAL),@MESINI, @DENGMN1,@ADJDIR, @GMN2, @GMN3, @GMN4 ,@DENGMN2, @DENGMN3, @DENGMN4"
 sConsulta = sConsulta & vbCrLf & ",@ART,@DESCR,@CANT,@UNI,@DENUNI,@DEST,@DENDEST,@PAG,@DENPAG,@FECINI,@FECFIN, @PRECAPE*@PORCEN*@CANTADJ,@PRECADJ*@PORCEN*@CANTADJ,@CANT*@PORCEN,@PRES1,@DENPRES1)"
 sConsulta = sConsulta & vbCrLf & "End"
 sConsulta = sConsulta & vbCrLf & "Else"
 sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_1 SET PRES=PRES+@PRECAPE*@PORCEN*@CANTADJ, PREC=PREC+@PRECADJ*@PORCEN*@CANTADJ"
 sConsulta = sConsulta & vbCrLf & "WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND MES=@MESINI AND ANYOADJ=YEAR(@FECINICIAL)  AND ITEM=@ITEM AND PRES1=@PRES1"
 sConsulta = sConsulta & vbCrLf & "End"
 sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM Cur_ITEM_ADJ INTO @ITEM,@PROVE,@FECINI,@FECFIN,@PRECADJ,@CANTADJ,@PRECAPE"
 sConsulta = sConsulta & vbCrLf & ",@PAG,@DENPAG,@UNI,@DENUNI,@DEST,@DENDEST"
 sConsulta = sConsulta & vbCrLf & ",@ART,@DESCR,@CANT,@PRES1,@DENPRES1"
 sConsulta = sConsulta & vbCrLf & "End"
 sConsulta = sConsulta & vbCrLf & "ERR_:"
 sConsulta = sConsulta & vbCrLf & "Close Cur_PROCE"
 sConsulta = sConsulta & vbCrLf & "DEALLOCATE Cur_PROCE"
 sConsulta = sConsulta & vbCrLf & "Close Cur_ITEM_ADJ"
 sConsulta = sConsulta & vbCrLf & "DEALLOCATE Cur_ITEM_ADJ"
 sConsulta = sConsulta & vbCrLf & "SET CONCAT_NULL_YIELDS_NULL ON"
 sConsulta = sConsulta & vbCrLf & "SET @RES=@@ERROR"
 sConsulta = sConsulta & vbCrLf & "Return"
 sConsulta = sConsulta & vbCrLf & "End"

CrearSP_AR_ITEM_MES_PRESCON4_1 = sConsulta
End Function

Private Function CrearSP_AR_ITEM_MES_PRESCON4_2() As String
Dim sConsulta As String

 sConsulta = "CREATE PROCEDURE SP_AR_ITEM_MES_PRESCON4_2 (@ANYO SMALLINT,@GMN1 VARCHAR(50), @PROCE INT,@RES INT OUTPUT)  AS"
 sConsulta = sConsulta & vbCrLf & "BEGIN"
 sConsulta = sConsulta & vbCrLf & "DECLARE @FECINI AS DATETIME"
 sConsulta = sConsulta & vbCrLf & "DECLARE @FECFIN AS DATETIME"
 sConsulta = sConsulta & vbCrLf & "DECLARE @PRECADJ AS FLOAT"
 sConsulta = sConsulta & vbCrLf & "DECLARE @CANTADJ AS FLOAT"
 sConsulta = sConsulta & vbCrLf & "DECLARE @PRECAPE AS FLOAT"
 sConsulta = sConsulta & vbCrLf & "DECLARE @DENGMN1 AS VARCHAR(100)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @DENGMN2 AS VARCHAR(100)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @DENGMN3 AS VARCHAR(100)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @DENGMN4 AS VARCHAR(100)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @PROCECAMBIO AS FLOAT"
 sConsulta = sConsulta & vbCrLf & "DECLARE @ADJDIR AS BIT"
 sConsulta = sConsulta & vbCrLf & "DECLARE @GMN2 AS VARCHAR (50)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @GMN3 AS VARCHAR (50)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @GMN4 AS VARCHAR (50)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @PORCEN AS FLOAT"
 sConsulta = sConsulta & vbCrLf & "DECLARE @MESINI AS INTEGER"
 sConsulta = sConsulta & vbCrLf & "DECLARE @TOTALDIAS AS FLOAT"
 sConsulta = sConsulta & vbCrLf & "DECLARE @DIAS AS FLOAT"
 sConsulta = sConsulta & vbCrLf & "DECLARE @PAG AS VARCHAR(50)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @DENPAG AS VARCHAR(100)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @UNI AS VARCHAR(50)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @DENUNI AS VARCHAR(100)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @DEST AS VARCHAR(50)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @DENDEST AS VARCHAR(100)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @ART AS VARCHAR(50)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @DESCR AS VARCHAR(100)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @CANT AS FLOAT"
 sConsulta = sConsulta & vbCrLf & "DECLARE @FECHA AS DATETIME"
 sConsulta = sConsulta & vbCrLf & "DECLARE @ITEM AS INTEGER"
 sConsulta = sConsulta & vbCrLf & "DECLARE @PROVE AS VARCHAR(50)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @NEPC AS TINYINT /* Numero de niveles en la estructura de materiales*/"
 sConsulta = sConsulta & vbCrLf & "DECLARE @FEC AS DATETIME"
 sConsulta = sConsulta & vbCrLf & "DECLARE @FECFINAL AS DATETIME"
 sConsulta = sConsulta & vbCrLf & "DECLARE @ANYOINI AS INTEGER"
 sConsulta = sConsulta & vbCrLf & "DECLARE @ANYOFIN AS INTEGER"
 sConsulta = sConsulta & vbCrLf & "DECLARE @ANYOACTUAL AS INTEGER"
 sConsulta = sConsulta & vbCrLf & "DECLARE @PRES1 AS VARCHAR (50)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @DENPRES1 AS VARCHAR(100)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @PRES2 AS VARCHAR (50)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @DENPRES2 AS VARCHAR(100)"
 sConsulta = sConsulta & vbCrLf & "--DECLARE @ANYOPROY AS INTEGER"
 sConsulta = sConsulta & vbCrLf & "DECLARE @FECINICIAL AS DATETIME"
 sConsulta = sConsulta & vbCrLf & ""
 sConsulta = sConsulta & vbCrLf & "SET CONCAT_NULL_YIELDS_NULL OFF"
 sConsulta = sConsulta & vbCrLf & ""
 sConsulta = sConsulta & vbCrLf & "DECLARE Cur_PROCE CURSOR FORWARD_ONLY STATIC READ_ONLY FOR"
 sConsulta = sConsulta & vbCrLf & ""
 sConsulta = sConsulta & vbCrLf & "  "
 sConsulta = sConsulta & vbCrLf & "SELECT GMN1.DEN,PROCE.GMN2, PROCE.GMN3, PROCE.GMN4"
 sConsulta = sConsulta & vbCrLf & ",GMN2.DEN, GMN3.DEN, GMN4.DEN, PROCE.CAMBIO,PROCE.ADJDIR"
 sConsulta = sConsulta & vbCrLf & "From Proce"
 sConsulta = sConsulta & vbCrLf & "INNER JOIN GMN1 ON PROCE.GMN1=GMN1.COD"
 sConsulta = sConsulta & vbCrLf & "INNER JOIN GMN2 ON  PROCE.GMN1=GMN2.GMN1 AND PROCE.GMN2=GMN2.COD"
 sConsulta = sConsulta & vbCrLf & "INNER JOIN GMN3 ON  PROCE.GMN1=GMN3.GMN1 AND  PROCE.GMN2=GMN3.GMN2 AND PROCE.GMN3=GMN3.COD"
 sConsulta = sConsulta & vbCrLf & "INNER JOIN GMN4 ON  PROCE.GMN1=GMN4.GMN1 AND  PROCE.GMN2=GMN4.GMN2 AND PROCE.GMN3=GMN4.GMN3 AND PROCE.GMN4=GMN4.COD"
 sConsulta = sConsulta & vbCrLf & "WHERE PROCE.ANYO=@ANYO AND PROCE.GMN1=@GMN1 AND PROCE.COD=@PROCE"
 sConsulta = sConsulta & vbCrLf & "OPEN Cur_PROCE"
 sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM cur_PROCE INTO @DENGMN1,@GMN2,@GMN3,@GMN4,@DENGMN2,@DENGMN3,@DENGMN4,@PROCECAMBIO,@ADJDIR"
 sConsulta = sConsulta & vbCrLf & "IF @@FETCH_STATUS<>0 GOTO ERR_"
 sConsulta = sConsulta & vbCrLf & ""
 sConsulta = sConsulta & vbCrLf & ""
 sConsulta = sConsulta & vbCrLf & "DECLARE Cur_ITEM_ADJ Cursor FORWARD_ONLY STATIC READ_ONLY FOR"
 sConsulta = sConsulta & vbCrLf & ""
 sConsulta = sConsulta & vbCrLf & "SELECT ITEM_ADJ.ITEM,ITEM_ADJ.PROVE,ITEM.FECINI,ITEM.FECFIN,(ITEM_OFE.PRECIO/(PROCE_OFE.CAMBIO*@PROCECAMBIO)) AS PRECADJ,(ITEM.CANT*(ITEM_ADJ.PORCEN/100)) AS CANTADJ,(ITEM.PREC/@PROCECAMBIO) AS PRECAPE"
 sConsulta = sConsulta & vbCrLf & ",ITEM.PAG,PAG.DEN,ITEM.UNI,UNI.DEN,ITEM.DEST,DEST.DEN"
 sConsulta = sConsulta & vbCrLf & ",ITEM.ART,ITEM.DESCR + ART4.DEN,ITEM.CANT,ITEM_PRESCON4.PRES1,PRES_4_CON1.DEN,ITEM_PRESCON4.PRES2,PRES_4_CON2.DEN"
 sConsulta = sConsulta & vbCrLf & "From ITEM_ADJ"
 sConsulta = sConsulta & vbCrLf & "INNER JOIN ITEM ON ITEM_ADJ.ANYO=ITEM.ANYO AND ITEM_ADJ.GMN1=ITEM.GMN1 AND ITEM_ADJ.PROCE=ITEM.PROCE  AND ITEM_ADJ.ITEM=ITEM.ID"
 sConsulta = sConsulta & vbCrLf & "INNER JOIN ITEM_PRESCON4 ON ITEM.ANYO=ITEM_PRESCON4.ANYO AND ITEM.GMN1=ITEM_PRESCON4.GMN1 AND ITEM.PROCE=ITEM_PRESCON4.PROCE AND ITEM.ID=ITEM_PRESCON4.ITEM AND ITEM_PRESCON4.PRES2 IS NOT NULL"
 sConsulta = sConsulta & vbCrLf & "INNER JOIN PRES_4_CON1 ON ITEM_PRESCON4.PRES1=PRES_4_CON1.COD"
 sConsulta = sConsulta & vbCrLf & "INNER JOIN PRES_4_CON2 ON ITEM_PRESCON4.PRES1=PRES_4_CON2.PRES1 AND ITEM_PRESCON4.PRES2=PRES_4_CON2.COD"
 sConsulta = sConsulta & vbCrLf & "INNER JOIN PROCE_OFE ON ITEM_ADJ.ANYO=PROCE_OFE.ANYO AND ITEM_ADJ.GMN1=PROCE_OFE.GMN1 AND ITEM_ADJ.PROCE=PROCE_OFE.PROCE AND ITEM_ADJ.OFE=PROCE_OFE.OFE AND ITEM_ADJ.PROVE=PROCE_OFE.PROVE"
 sConsulta = sConsulta & vbCrLf & "INNER JOIN ITEM_OFE ON ITEM_ADJ.ANYO=ITEM_OFE.ANYO AND ITEM_OFE.GMN1=ITEM_OFE.GMN1 AND ITEM_ADJ.PROCE=ITEM_OFE.PROCE AND ITEM_ADJ.PROVE=ITEM_OFE.PROVE AND ITEM_ADJ.ITEM=ITEM_OFE.ITEM AND ITEM_ADJ.OFE=ITEM_OFE.NUM"
 sConsulta = sConsulta & vbCrLf & "INNER JOIN UNI ON ITEM.UNI=UNI.COD"
 sConsulta = sConsulta & vbCrLf & "INNER JOIN DEST ON ITEM.DEST=DEST.COD"
 sConsulta = sConsulta & vbCrLf & "INNER JOIN PAG ON ITEM.PAG=PAG.COD"
 sConsulta = sConsulta & vbCrLf & "LEFT JOIN ART4 ON ITEM.ART=ART4.COD AND ITEM.GMN1=ART4.GMN1 AND ITEM.GMN2=ART4.GMN2 AND ITEM.GMN3=ART4.GMN3 AND ITEM.GMN4=ART4.GMN4"
 sConsulta = sConsulta & vbCrLf & "WHERE ITEM_ADJ.ANYO=@ANYO AND ITEM_ADJ.GMN1=@GMN1 AND ITEM_ADJ.PROCE=@PROCE AND ITEM_ADJ.PORCEN <> 0"
 sConsulta = sConsulta & vbCrLf & "                    "
 sConsulta = sConsulta & vbCrLf & ""
 sConsulta = sConsulta & vbCrLf & "                    "
 sConsulta = sConsulta & vbCrLf & "OPEN Cur_ITEM_ADJ"
 sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM Cur_ITEM_ADJ INTO @ITEM,@PROVE,@FECINI,@FECFIN,@PRECADJ,@CANTADJ,@PRECAPE"
 sConsulta = sConsulta & vbCrLf & ",@PAG,@DENPAG,@UNI,@DENUNI,@DEST,@DENDEST"
 sConsulta = sConsulta & vbCrLf & ",@ART,@DESCR,@CANT,@PRES1,@DENPRES1,@PRES2,@DENPRES2"
 sConsulta = sConsulta & vbCrLf & "WHILE @@FETCH_STATUS=0"
 sConsulta = sConsulta & vbCrLf & "BEGIN"
 sConsulta = sConsulta & vbCrLf & " "
 sConsulta = sConsulta & vbCrLf & "/* Tenemos que calcular el porcentaje correspondiente a cada mes del ANYO.*/"
 sConsulta = sConsulta & vbCrLf & "SET @TOTALDIAS = datediff(day,@FECINI, @FECFIN) + 1"
 sConsulta = sConsulta & vbCrLf & "    "
 sConsulta = sConsulta & vbCrLf & "SET @FECINICIAL = @FECINI"
 sConsulta = sConsulta & vbCrLf & "SET @FECFINAL = @FECFIN"
 sConsulta = sConsulta & vbCrLf & "SET @MESINI = MONTH(@FECINICIAL)"
 sConsulta = sConsulta & vbCrLf & "SET @FECHA = @FECINICIAL"
 sConsulta = sConsulta & vbCrLf & "            "
 sConsulta = sConsulta & vbCrLf & "WHILE @MESINI < MONTH(@FECFINAL)"
 sConsulta = sConsulta & vbCrLf & "BEGIN"
 sConsulta = sConsulta & vbCrLf & "/* Calculamos la diferencia en d�as entre el mes inicial y el siguiente mes*/  "
 sConsulta = sConsulta & vbCrLf & "SET @DIAS = datediff(day,@FECHA,cast( str (month(@FECHA) +1) + '/1/' + str(year(@FECHA) ) as datetime))"
 sConsulta = sConsulta & vbCrLf & "SET @PORCEN = @DIAS / @TOTALDIAS"
 sConsulta = sConsulta & vbCrLf & "SET @FECHA = cast(str((month(@FECHA) +1)) + '/1/' + str(year(@FECHA)) as datetime)"
 sConsulta = sConsulta & vbCrLf & ""
 sConsulta = sConsulta & vbCrLf & "/*Miramos si hab�a alg�n resultado PROYa ese mes*/"
 sConsulta = sConsulta & vbCrLf & "IF (SELECT COUNT(*) FROM AR_ITEM_MES_PRESCON4_2 WHERE"
 sConsulta = sConsulta & vbCrLf & "ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM AND ANYOADJ=YEAR(@FECINICIAL) AND MES=@MESINI AND PRES1=@PRES1 AND PRES2=@PRES2)=0"
 sConsulta = sConsulta & vbCrLf & "BEGIN"
 sConsulta = sConsulta & vbCrLf & "    "
 sConsulta = sConsulta & vbCrLf & "INSERT INTO  AR_ITEM_MES_PRESCON4_2 ( ANYO,GMN1,PROCE,ITEM,ANYOADJ,MES,DENGMN1,ADJDIR,GMN2,GMN3,GMN4,DENGMN2,DENGMN3,DENGMN4"
 sConsulta = sConsulta & vbCrLf & ",ART,DESCR,CANT"
 sConsulta = sConsulta & vbCrLf & ",UNI,DENUNI,DEST,DENDEST,PAG,DENPAG,FECINI,FECFIN,PRES,PREC,CANTPERIODO,PRES1,DENPRES1,PRES2,DENPRES2)"
 sConsulta = sConsulta & vbCrLf & " "
 sConsulta = sConsulta & vbCrLf & "VALUES(@ANYO,@GMN1, @PROCE , @ITEM,YEAR(@FECINICIAL),@MESINI, @DENGMN1,@ADJDIR, @GMN2, @GMN3, @GMN4 ,@DENGMN2, @DENGMN3, @DENGMN4"
 sConsulta = sConsulta & vbCrLf & ",@ART,@DESCR,@CANT"
 sConsulta = sConsulta & vbCrLf & ",@UNI,@DENUNI,@DEST,@DENDEST,@PAG,@DENPAG,@FECINI,@FECFIN,@PRECAPE*@PORCEN*@CANTADJ,@PRECADJ*@PORCEN*@CANTADJ,@CANT*@PORCEN,@PRES1,@DENPRES1,@PRES2,@DENPRES2)"
 sConsulta = sConsulta & vbCrLf & "    "
 sConsulta = sConsulta & vbCrLf & "End"
 sConsulta = sConsulta & vbCrLf & "Else"
 sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_2 SET PRES=PRES+@PRECAPE*@PORCEN*@CANTADJ, PREC=PREC+@PRECADJ*@PORCEN*@CANTADJ"
 sConsulta = sConsulta & vbCrLf & "WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND MES=@MESINI AND ANYOADJ=YEAR(@FECINICIAL)  AND ITEM=@ITEM AND PRES1=@PRES1 AND PRES2=@PRES2"
 sConsulta = sConsulta & vbCrLf & "  "
 sConsulta = sConsulta & vbCrLf & "SET @MESINI=@MESINI + 1"
 sConsulta = sConsulta & vbCrLf & "  "
 sConsulta = sConsulta & vbCrLf & "  "
 sConsulta = sConsulta & vbCrLf & "End"
 sConsulta = sConsulta & vbCrLf & ""
 sConsulta = sConsulta & vbCrLf & "/*Si el m�s es el mismo*/"
 sConsulta = sConsulta & vbCrLf & "IF @MESINI = MONTH(@FECFINAL)"
 sConsulta = sConsulta & vbCrLf & "BEGIN"
 sConsulta = sConsulta & vbCrLf & ""
 sConsulta = sConsulta & vbCrLf & "SET @DIAS = datediff(day,@FECHA,@FECFINAL) + 1"
 sConsulta = sConsulta & vbCrLf & "SET @PORCEN = @DIAS/@TOTALDIAS"
 sConsulta = sConsulta & vbCrLf & ""
 sConsulta = sConsulta & vbCrLf & "/*Miramos si hab�a alg�n resultado PROYa ese mes*/"
 sConsulta = sConsulta & vbCrLf & "IF (SELECT COUNT(*) FROM AR_ITEM_MES_PRESCON4_2 WHERE"
 sConsulta = sConsulta & vbCrLf & ""
 sConsulta = sConsulta & vbCrLf & "ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM AND ANYOADJ=YEAR(@FECINICIAL) AND MES=@MESINI AND PRES1=@PRES1 AND PRES2=@PRES2)=0"
 sConsulta = sConsulta & vbCrLf & "BEGIN"
 sConsulta = sConsulta & vbCrLf & "    "
 sConsulta = sConsulta & vbCrLf & "INSERT INTO  AR_ITEM_MES_PRESCON4_2 ( ANYO, GMN1, PROCE, ITEM, ANYOADJ, MES, DENGMN1, ADJDIR, GMN2, GMN3, GMN4, DENGMN2, DENGMN3,DENGMN4"
 sConsulta = sConsulta & vbCrLf & ", ART, DESCR, CANT, UNI, DENUNI, DEST, DENDEST, PAG, DENPAG, FECINI, FECFIN, PRES, PREC, CANTPERIODO, PRES1, DENPRES1,PRES2,DENPRES2)"
 sConsulta = sConsulta & vbCrLf & "VALUES(@ANYO,@GMN1, @PROCE , @ITEM,YEAR(@FECINICIAL),@MESINI, @DENGMN1,@ADJDIR, @GMN2, @GMN3, @GMN4 ,@DENGMN2, @DENGMN3, @DENGMN4"
 sConsulta = sConsulta & vbCrLf & ",@ART,@DESCR,@CANT,@UNI,@DENUNI,@DEST,@DENDEST,@PAG,@DENPAG,@FECINI,@FECFIN, @PRECAPE*@PORCEN*@CANTADJ,@PRECADJ*@PORCEN*@CANTADJ,@CANT*@PORCEN,@PRES1,@DENPRES1,@PRES2,@DENPRES2)"
 sConsulta = sConsulta & vbCrLf & "    "
 sConsulta = sConsulta & vbCrLf & "End"
 sConsulta = sConsulta & vbCrLf & "Else"
 sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_2 SET PRES=PRES+@PRECAPE*@PORCEN*@CANTADJ, PREC=PREC+@PRECADJ*@PORCEN*@CANTADJ"
 sConsulta = sConsulta & vbCrLf & "WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND MES=@MESINI AND ANYOADJ=YEAR(@FECINICIAL)  AND ITEM=@ITEM AND PRES1=@PRES1 AND PRES2=@PRES2"
 sConsulta = sConsulta & vbCrLf & ""
 sConsulta = sConsulta & vbCrLf & ""
 sConsulta = sConsulta & vbCrLf & "End"
 sConsulta = sConsulta & vbCrLf & " "
 sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM Cur_ITEM_ADJ INTO @ITEM,@PROVE,@FECINI,@FECFIN,@PRECADJ,@CANTADJ,@PRECAPE"
 sConsulta = sConsulta & vbCrLf & ",@PAG,@DENPAG,@UNI,@DENUNI,@DEST,@DENDEST"
 sConsulta = sConsulta & vbCrLf & ",@ART,@DESCR,@CANT,@PRES1,@DENPRES1,@PRES2,@DENPRES2"
 sConsulta = sConsulta & vbCrLf & "End"
 sConsulta = sConsulta & vbCrLf & "ERR_:"
 sConsulta = sConsulta & vbCrLf & "Close Cur_PROCE"
 sConsulta = sConsulta & vbCrLf & "DEALLOCATE Cur_PROCE"
 sConsulta = sConsulta & vbCrLf & "Close Cur_ITEM_ADJ"
 sConsulta = sConsulta & vbCrLf & "DEALLOCATE Cur_ITEM_ADJ"
 sConsulta = sConsulta & vbCrLf & "    "
 sConsulta = sConsulta & vbCrLf & "SET CONCAT_NULL_YIELDS_NULL ON"
 sConsulta = sConsulta & vbCrLf & "SET @RES=@@ERROR"
 sConsulta = sConsulta & vbCrLf & "Return"
 sConsulta = sConsulta & vbCrLf & "End"

CrearSP_AR_ITEM_MES_PRESCON4_2 = sConsulta
End Function

Private Function CrearSP_AR_ITEM_MES_PRESCON4_3() As String
Dim sConsulta As String

 sConsulta = "CREATE PROCEDURE SP_AR_ITEM_MES_PRESCON4_3 (@ANYO SMALLINT,@GMN1 VARCHAR(50), @PROCE INT,@RES INT OUTPUT)  AS"
 sConsulta = sConsulta & vbCrLf & "BEGIN"
 sConsulta = sConsulta & vbCrLf & "DECLARE @FECINI AS DATETIME"
 sConsulta = sConsulta & vbCrLf & "DECLARE @FECFIN AS DATETIME"
 sConsulta = sConsulta & vbCrLf & "DECLARE @PRECADJ AS FLOAT"
 sConsulta = sConsulta & vbCrLf & "DECLARE @CANTADJ AS FLOAT"
 sConsulta = sConsulta & vbCrLf & "DECLARE @PRECAPE AS FLOAT"
 sConsulta = sConsulta & vbCrLf & "DECLARE @DENGMN1 AS VARCHAR(100)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @DENGMN2 AS VARCHAR(100)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @DENGMN3 AS VARCHAR(100)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @DENGMN4 AS VARCHAR(100)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @PROCECAMBIO AS FLOAT"
 sConsulta = sConsulta & vbCrLf & "DECLARE @ADJDIR AS BIT"
 sConsulta = sConsulta & vbCrLf & "DECLARE @GMN2 AS VARCHAR (50)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @GMN3 AS VARCHAR (50)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @GMN4 AS VARCHAR (50)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @PORCEN AS FLOAT"
 sConsulta = sConsulta & vbCrLf & "DECLARE @MESINI AS INTEGER"
 sConsulta = sConsulta & vbCrLf & "DECLARE @TOTALDIAS AS FLOAT"
 sConsulta = sConsulta & vbCrLf & "DECLARE @DIAS AS FLOAT"
 sConsulta = sConsulta & vbCrLf & "DECLARE @PAG AS VARCHAR(50)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @DENPAG AS VARCHAR(100)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @UNI AS VARCHAR(50)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @DENUNI AS VARCHAR(100)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @DEST AS VARCHAR(50)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @DENDEST AS VARCHAR(100)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @ART AS VARCHAR(50)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @DESCR AS VARCHAR(100)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @CANT AS FLOAT"
 sConsulta = sConsulta & vbCrLf & "DECLARE @FECHA AS DATETIME"
 sConsulta = sConsulta & vbCrLf & "DECLARE @ITEM AS INTEGER"
 sConsulta = sConsulta & vbCrLf & "DECLARE @PROVE AS VARCHAR(50)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @NEPC AS TINYINT /* Numero de niveles en la estructura de materiales*/"
 sConsulta = sConsulta & vbCrLf & "DECLARE @FEC AS DATETIME"
 sConsulta = sConsulta & vbCrLf & "DECLARE @FECFINAL AS DATETIME"
 sConsulta = sConsulta & vbCrLf & "DECLARE @ANYOINI AS INTEGER"
 sConsulta = sConsulta & vbCrLf & "DECLARE @ANYOFIN AS INTEGER"
 sConsulta = sConsulta & vbCrLf & "DECLARE @ANYOACTUAL AS INTEGER"
 sConsulta = sConsulta & vbCrLf & "DECLARE @PRES1 AS VARCHAR (50)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @DENPRES1 AS VARCHAR(100)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @PRES2 AS VARCHAR (50)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @DENPRES2 AS VARCHAR(100)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @PRES3 AS VARCHAR (50)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @DENPRES3 AS VARCHAR(100)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @FECINICIAL AS DATETIME"
 sConsulta = sConsulta & vbCrLf & ""
 sConsulta = sConsulta & vbCrLf & "SET CONCAT_NULL_YIELDS_NULL OFF"
 sConsulta = sConsulta & vbCrLf & ""
 sConsulta = sConsulta & vbCrLf & "DECLARE Cur_PROCE CURSOR FORWARD_ONLY STATIC READ_ONLY FOR"
 sConsulta = sConsulta & vbCrLf & ""
 sConsulta = sConsulta & vbCrLf & "  "
 sConsulta = sConsulta & vbCrLf & "SELECT GMN1.DEN,PROCE.GMN2, PROCE.GMN3, PROCE.GMN4"
 sConsulta = sConsulta & vbCrLf & ",GMN2.DEN, GMN3.DEN, GMN4.DEN, PROCE.CAMBIO,PROCE.ADJDIR"
 sConsulta = sConsulta & vbCrLf & "From Proce"
 sConsulta = sConsulta & vbCrLf & "INNER JOIN GMN1 ON PROCE.GMN1=GMN1.COD"
 sConsulta = sConsulta & vbCrLf & "INNER JOIN GMN2 ON  PROCE.GMN1=GMN2.GMN1 AND PROCE.GMN2=GMN2.COD"
 sConsulta = sConsulta & vbCrLf & "INNER JOIN GMN3 ON  PROCE.GMN1=GMN3.GMN1 AND  PROCE.GMN2=GMN3.GMN2 AND PROCE.GMN3=GMN3.COD"
 sConsulta = sConsulta & vbCrLf & "INNER JOIN GMN4 ON  PROCE.GMN1=GMN4.GMN1 AND  PROCE.GMN2=GMN4.GMN2 AND PROCE.GMN3=GMN4.GMN3 AND PROCE.GMN4=GMN4.COD"
 sConsulta = sConsulta & vbCrLf & "WHERE PROCE.ANYO=@ANYO AND PROCE.GMN1=@GMN1 AND PROCE.COD=@PROCE"
 sConsulta = sConsulta & vbCrLf & "OPEN Cur_PROCE"
 sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM cur_PROCE INTO @DENGMN1,@GMN2,@GMN3,@GMN4,@DENGMN2,@DENGMN3,@DENGMN4,@PROCECAMBIO,@ADJDIR"
 sConsulta = sConsulta & vbCrLf & "IF @@FETCH_STATUS<>0 GOTO ERR_"
 sConsulta = sConsulta & vbCrLf & ""
 sConsulta = sConsulta & vbCrLf & ""
 sConsulta = sConsulta & vbCrLf & "DECLARE Cur_ITEM_ADJ Cursor FORWARD_ONLY STATIC READ_ONLY FOR"
 sConsulta = sConsulta & vbCrLf & ""
 sConsulta = sConsulta & vbCrLf & "SELECT ITEM_ADJ.ITEM,ITEM_ADJ.PROVE,ITEM.FECINI,ITEM.FECFIN,(ITEM_OFE.PRECIO/(PROCE_OFE.CAMBIO*@PROCECAMBIO)) AS PRECADJ,(ITEM.CANT*(ITEM_ADJ.PORCEN/100)) AS CANTADJ,(ITEM.PREC/@PROCECAMBIO) AS PRECAPE"
 sConsulta = sConsulta & vbCrLf & ",ITEM.PAG,PAG.DEN,ITEM.UNI,UNI.DEN,ITEM.DEST,DEST.DEN"
 sConsulta = sConsulta & vbCrLf & ",ITEM.ART,ITEM.DESCR + ART4.DEN,ITEM.CANT,ITEM_PRESCON4.PRES1,PRES_4_CON1.DEN,ITEM_PRESCON4.PRES2,PRES_4_CON2.DEN,ITEM_PRESCON4.PRES3,PRES_4_CON3.DEN"
 sConsulta = sConsulta & vbCrLf & "From ITEM_ADJ"
 sConsulta = sConsulta & vbCrLf & "INNER JOIN ITEM ON ITEM_ADJ.ANYO=ITEM.ANYO AND ITEM_ADJ.GMN1=ITEM.GMN1 AND ITEM_ADJ.PROCE=ITEM.PROCE  AND ITEM_ADJ.ITEM=ITEM.ID"
 sConsulta = sConsulta & vbCrLf & "INNER JOIN ITEM_PRESCON4 ON ITEM.ANYO=ITEM_PRESCON4.ANYO AND ITEM.GMN1=ITEM_PRESCON4.GMN1 AND ITEM.PROCE=ITEM_PRESCON4.PROCE AND ITEM.ID=ITEM_PRESCON4.ITEM AND ITEM_PRESCON4.PRES3 IS NOT NULL"
 sConsulta = sConsulta & vbCrLf & "INNER JOIN PRES_4_CON1 ON ITEM_PRESCON4.PRES1=PRES_4_CON1.COD"
 sConsulta = sConsulta & vbCrLf & "INNER JOIN PRES_4_CON2 ON ITEM_PRESCON4.PRES1=PRES_4_CON2.PRES1 AND ITEM_PRESCON4.PRES2=PRES_4_CON2.COD"
 sConsulta = sConsulta & vbCrLf & "INNER JOIN PRES_4_CON3 ON ITEM_PRESCON4.PRES1=PRES_4_CON3.PRES1 AND ITEM_PRESCON4.PRES2=PRES_4_CON3.PRES2 AND ITEM_PRESCON4.PRES3 =PRES_4_CON3.COD"
 sConsulta = sConsulta & vbCrLf & "INNER JOIN PROCE_OFE ON ITEM_ADJ.ANYO=PROCE_OFE.ANYO AND ITEM_ADJ.GMN1=PROCE_OFE.GMN1 AND ITEM_ADJ.PROCE=PROCE_OFE.PROCE AND ITEM_ADJ.OFE=PROCE_OFE.OFE AND ITEM_ADJ.PROVE=PROCE_OFE.PROVE"
 sConsulta = sConsulta & vbCrLf & "INNER JOIN ITEM_OFE ON ITEM_ADJ.ANYO=ITEM_OFE.ANYO AND ITEM_OFE.GMN1=ITEM_OFE.GMN1 AND ITEM_ADJ.PROCE=ITEM_OFE.PROCE AND ITEM_ADJ.PROVE=ITEM_OFE.PROVE AND ITEM_ADJ.ITEM=ITEM_OFE.ITEM AND ITEM_ADJ.OFE=ITEM_OFE.NUM"
 sConsulta = sConsulta & vbCrLf & "INNER JOIN UNI ON ITEM.UNI=UNI.COD"
 sConsulta = sConsulta & vbCrLf & "INNER JOIN DEST ON ITEM.DEST=DEST.COD"
 sConsulta = sConsulta & vbCrLf & "INNER JOIN PAG ON ITEM.PAG=PAG.COD"
 sConsulta = sConsulta & vbCrLf & "LEFT JOIN ART4 ON ITEM.ART=ART4.COD AND ITEM.GMN1=ART4.GMN1 AND ITEM.GMN2=ART4.GMN2 AND ITEM.GMN3=ART4.GMN3 AND ITEM.GMN4=ART4.GMN4"
 sConsulta = sConsulta & vbCrLf & "WHERE ITEM_ADJ.ANYO=@ANYO AND ITEM_ADJ.GMN1=@GMN1 AND ITEM_ADJ.PROCE=@PROCE AND ITEM_ADJ.PORCEN <> 0"
 sConsulta = sConsulta & vbCrLf & "                    "
 sConsulta = sConsulta & vbCrLf & "                    "
 sConsulta = sConsulta & vbCrLf & "OPEN Cur_ITEM_ADJ"
 sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM Cur_ITEM_ADJ INTO @ITEM,@PROVE,@FECINI,@FECFIN,@PRECADJ,@CANTADJ,@PRECAPE"
 sConsulta = sConsulta & vbCrLf & ",@PAG,@DENPAG,@UNI,@DENUNI,@DEST,@DENDEST"
 sConsulta = sConsulta & vbCrLf & ",@ART,@DESCR,@CANT,@PRES1,@DENPRES1,@PRES2,@DENPRES2,@PRES3,@DENPRES3"
 sConsulta = sConsulta & vbCrLf & "WHILE @@FETCH_STATUS=0"
 sConsulta = sConsulta & vbCrLf & "BEGIN"
 sConsulta = sConsulta & vbCrLf & " "
 sConsulta = sConsulta & vbCrLf & "/* Tenemos que calcular el porcentaje correspondiente a cada mes del ANYO.*/"
 sConsulta = sConsulta & vbCrLf & "SET @TOTALDIAS = datediff(day,@FECINI, @FECFIN) + 1"
 sConsulta = sConsulta & vbCrLf & "    "
 sConsulta = sConsulta & vbCrLf & "SET @FECINICIAL = @FECINI"
 sConsulta = sConsulta & vbCrLf & "SET @FECFINAL = @FECFIN"
 sConsulta = sConsulta & vbCrLf & "SET @MESINI = MONTH(@FECINICIAL)"
 sConsulta = sConsulta & vbCrLf & "SET @FECHA = @FECINICIAL"
 sConsulta = sConsulta & vbCrLf & ""
 sConsulta = sConsulta & vbCrLf & "            "
 sConsulta = sConsulta & vbCrLf & "WHILE @MESINI < MONTH(@FECFINAL)"
 sConsulta = sConsulta & vbCrLf & "BEGIN"
 sConsulta = sConsulta & vbCrLf & "/* Calculamos la diferencia en d�as entre el mes inicial y el siguiente mes*/  "
 sConsulta = sConsulta & vbCrLf & "SET @DIAS = datediff(day,@FECHA,cast( str (month(@FECHA) +1) + '/1/' + str(year(@FECHA) ) as datetime))"
 sConsulta = sConsulta & vbCrLf & "SET @PORCEN = @DIAS / @TOTALDIAS"
 sConsulta = sConsulta & vbCrLf & "SET @FECHA = cast(str((month(@FECHA) +1)) + '/1/' + str(year(@FECHA)) as datetime)"
 sConsulta = sConsulta & vbCrLf & "  "
 sConsulta = sConsulta & vbCrLf & "/*Miramos si hab�a alg�n resultado PRES a ese mes*/"
 sConsulta = sConsulta & vbCrLf & "IF (SELECT COUNT(*) FROM AR_ITEM_MES_PRESCON4_3 WHERE"
 sConsulta = sConsulta & vbCrLf & "ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM AND ANYOADJ=YEAR(@FECINICIAL) AND MES=@MESINI AND PRES1=@PRES1 AND PRES2=@PRES2 AND PRES3=@PRES3)=0"
 sConsulta = sConsulta & vbCrLf & "BEGIN"
 sConsulta = sConsulta & vbCrLf & "    "
 sConsulta = sConsulta & vbCrLf & "INSERT INTO  AR_ITEM_MES_PRESCON4_3 ( ANYO,GMN1,PROCE,ITEM,ANYOADJ,MES,DENGMN1,ADJDIR,GMN2,GMN3,GMN4,DENGMN2,DENGMN3,DENGMN4"
 sConsulta = sConsulta & vbCrLf & ",ART,DESCR,CANT"
 sConsulta = sConsulta & vbCrLf & ",UNI,DENUNI,DEST,DENDEST,PAG,DENPAG,FECINI,FECFIN,PRES,PREC,CANTPERIODO,PRES1,DENPRES1,PRES2,DENPRES2,PRES3,DENPRES3)"
 sConsulta = sConsulta & vbCrLf & " "
 sConsulta = sConsulta & vbCrLf & "VALUES(@ANYO,@GMN1, @PROCE , @ITEM,YEAR(@FECINICIAL),@MESINI, @DENGMN1,@ADJDIR, @GMN2, @GMN3, @GMN4 ,@DENGMN2, @DENGMN3, @DENGMN4"
 sConsulta = sConsulta & vbCrLf & ",@ART,@DESCR,@CANT"
 sConsulta = sConsulta & vbCrLf & ",@UNI,@DENUNI,@DEST,@DENDEST,@PAG,@DENPAG,@FECINI,@FECFIN,@PRECAPE*@PORCEN*@CANTADJ,@PRECADJ*@PORCEN*@CANTADJ,@CANT*@PORCEN,@PRES1,@DENPRES1,@PRES2,@DENPRES2,@PRES3,@DENPRES3)"
 sConsulta = sConsulta & vbCrLf & "    "
 sConsulta = sConsulta & vbCrLf & "End"
 sConsulta = sConsulta & vbCrLf & "Else"
 sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_3 SET PRES=PRES+@PRECAPE*@PORCEN*@CANTADJ, PREC=PREC+@PRECADJ*@PORCEN*@CANTADJ"
 sConsulta = sConsulta & vbCrLf & "WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND MES=@MESINI AND ANYOADJ=YEAR(@FECINICIAL)  AND ITEM=@ITEM AND PRES1=@PRES1 AND PRES2=@PRES2 AND PRES3=@PRES3"
 sConsulta = sConsulta & vbCrLf & "  "
 sConsulta = sConsulta & vbCrLf & "SET @MESINI=@MESINI + 1"
 sConsulta = sConsulta & vbCrLf & "  "
 sConsulta = sConsulta & vbCrLf & "  "
 sConsulta = sConsulta & vbCrLf & "End"
 sConsulta = sConsulta & vbCrLf & ""
 sConsulta = sConsulta & vbCrLf & "/*Si el m�s es el mismo*/"
 sConsulta = sConsulta & vbCrLf & "IF @MESINI = MONTH(@FECFINAL)"
 sConsulta = sConsulta & vbCrLf & "BEGIN"
 sConsulta = sConsulta & vbCrLf & ""
 sConsulta = sConsulta & vbCrLf & "SET @DIAS = datediff(day,@FECHA,@FECFINAL) + 1"
 sConsulta = sConsulta & vbCrLf & "SET @PORCEN = @DIAS/@TOTALDIAS"
 sConsulta = sConsulta & vbCrLf & ""
 sConsulta = sConsulta & vbCrLf & "/*Miramos si hab�a alg�n resultado PRES a ese mes*/"
 sConsulta = sConsulta & vbCrLf & "IF (SELECT COUNT(*) FROM AR_ITEM_MES_PRESCON4_3 WHERE"
 sConsulta = sConsulta & vbCrLf & "ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM AND ANYOADJ=YEAR(@FECINICIAL) AND MES=@MESINI AND PRES1=@PRES1 AND PRES2=@PRES2 AND PRES3=@PRES3)=0"
 sConsulta = sConsulta & vbCrLf & "BEGIN"
 sConsulta = sConsulta & vbCrLf & "    "
 sConsulta = sConsulta & vbCrLf & "INSERT INTO  AR_ITEM_MES_PRESCON4_3 ( ANYO, GMN1, PROCE, ITEM, ANYOADJ, MES, DENGMN1, ADJDIR, GMN2, GMN3, GMN4, DENGMN2, DENGMN3,DENGMN4"
 sConsulta = sConsulta & vbCrLf & ", ART, DESCR, CANT, UNI, DENUNI, DEST, DENDEST, PAG, DENPAG, FECINI, FECFIN, PRES, PREC, CANTPERIODO, PRES1, DENPRES1,PRES2,DENPRES2,PRES3,DENPRES3)"
 sConsulta = sConsulta & vbCrLf & "VALUES(@ANYO,@GMN1, @PROCE , @ITEM,YEAR(@FECINICIAL),@MESINI, @DENGMN1,@ADJDIR, @GMN2, @GMN3, @GMN4 ,@DENGMN2, @DENGMN3, @DENGMN4"
 sConsulta = sConsulta & vbCrLf & ",@ART,@DESCR,@CANT,@UNI,@DENUNI,@DEST,@DENDEST,@PAG,@DENPAG,@FECINI,@FECFIN, @PRECAPE*@PORCEN*@CANTADJ,@PRECADJ*@PORCEN*@CANTADJ,@CANT*@PORCEN,@PRES1,@DENPRES1,@PRES2,@DENPRES2,@PRES3,@DENPRES3)"
 sConsulta = sConsulta & vbCrLf & "    "
 sConsulta = sConsulta & vbCrLf & "End"
 sConsulta = sConsulta & vbCrLf & "Else"
 sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_3 SET PRES=PRES+@PRECAPE*@PORCEN*@CANTADJ, PREC=PREC+@PRECADJ*@PORCEN*@CANTADJ"
 sConsulta = sConsulta & vbCrLf & "WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND MES=@MESINI AND ANYOADJ=YEAR(@FECINICIAL)  AND ITEM=@ITEM AND PRES1=@PRES1 AND PRES2=@PRES2 AND PRES3=@PRES3"
 sConsulta = sConsulta & vbCrLf & ""
 sConsulta = sConsulta & vbCrLf & ""
 sConsulta = sConsulta & vbCrLf & "End"
 sConsulta = sConsulta & vbCrLf & " "
 sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM Cur_ITEM_ADJ INTO @ITEM,@PROVE,@FECINI,@FECFIN,@PRECADJ,@CANTADJ,@PRECAPE"
 sConsulta = sConsulta & vbCrLf & ",@PAG,@DENPAG,@UNI,@DENUNI,@DEST,@DENDEST"
 sConsulta = sConsulta & vbCrLf & ",@ART,@DESCR,@CANT,@PRES1,@DENPRES1,@PRES2,@DENPRES2,@PRES3,@DENPRES3"
 sConsulta = sConsulta & vbCrLf & "End"
 sConsulta = sConsulta & vbCrLf & "ERR_:"
 sConsulta = sConsulta & vbCrLf & "Close Cur_PROCE"
 sConsulta = sConsulta & vbCrLf & "DEALLOCATE Cur_PROCE"
 sConsulta = sConsulta & vbCrLf & "Close Cur_ITEM_ADJ"
 sConsulta = sConsulta & vbCrLf & "DEALLOCATE Cur_ITEM_ADJ"
 sConsulta = sConsulta & vbCrLf & "    "
 sConsulta = sConsulta & vbCrLf & "SET CONCAT_NULL_YIELDS_NULL ON"
 sConsulta = sConsulta & vbCrLf & "SET @RES=@@ERROR"
 sConsulta = sConsulta & vbCrLf & "Return"
 sConsulta = sConsulta & vbCrLf & "End"

CrearSP_AR_ITEM_MES_PRESCON4_3 = sConsulta
End Function

Private Function CrearSP_AR_ITEM_MES_PRESCON4_4() As String
Dim sConsulta As String

 sConsulta = "CREATE PROCEDURE SP_AR_ITEM_MES_PRESCON4_4 (@ANYO SMALLINT,@GMN1 VARCHAR(50), @PROCE INT,@RES INT OUTPUT)  AS"
 sConsulta = sConsulta & vbCrLf & "BEGIN"
 sConsulta = sConsulta & vbCrLf & "DECLARE @FECINI AS DATETIME"
 sConsulta = sConsulta & vbCrLf & "DECLARE @FECFIN AS DATETIME"
 sConsulta = sConsulta & vbCrLf & "DECLARE @PRECADJ AS FLOAT"
 sConsulta = sConsulta & vbCrLf & "DECLARE @CANTADJ AS FLOAT"
 sConsulta = sConsulta & vbCrLf & "DECLARE @PRECAPE AS FLOAT"
 sConsulta = sConsulta & vbCrLf & "DECLARE @DENGMN1 AS VARCHAR(100)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @DENGMN2 AS VARCHAR(100)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @DENGMN3 AS VARCHAR(100)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @DENGMN4 AS VARCHAR(100)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @PROCECAMBIO AS FLOAT"
 sConsulta = sConsulta & vbCrLf & "DECLARE @ADJDIR AS BIT"
 sConsulta = sConsulta & vbCrLf & "DECLARE @GMN2 AS VARCHAR (50)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @GMN3 AS VARCHAR (50)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @GMN4 AS VARCHAR (50)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @PORCEN AS FLOAT"
 sConsulta = sConsulta & vbCrLf & "DECLARE @MESINI AS INTEGER"
 sConsulta = sConsulta & vbCrLf & "DECLARE @TOTALDIAS AS FLOAT"
 sConsulta = sConsulta & vbCrLf & "DECLARE @DIAS AS FLOAT"
 sConsulta = sConsulta & vbCrLf & "DECLARE @PAG AS VARCHAR(50)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @DENPAG AS VARCHAR(100)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @UNI AS VARCHAR(50)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @DENUNI AS VARCHAR(100)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @DEST AS VARCHAR(50)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @DENDEST AS VARCHAR(100)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @ART AS VARCHAR(50)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @DESCR AS VARCHAR(100)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @CANT AS FLOAT"
 sConsulta = sConsulta & vbCrLf & "DECLARE @FECHA AS DATETIME"
 sConsulta = sConsulta & vbCrLf & "DECLARE @ITEM AS INTEGER"
 sConsulta = sConsulta & vbCrLf & "DECLARE @PROVE AS VARCHAR(50)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @FEC AS DATETIME"
 sConsulta = sConsulta & vbCrLf & "DECLARE @FECFINAL AS DATETIME"
 sConsulta = sConsulta & vbCrLf & "DECLARE @ANYOINI AS INTEGER"
 sConsulta = sConsulta & vbCrLf & "DECLARE @ANYOFIN AS INTEGER"
 sConsulta = sConsulta & vbCrLf & "DECLARE @ANYOACTUAL AS INTEGER"
 sConsulta = sConsulta & vbCrLf & "DECLARE @PRES1 AS VARCHAR (50)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @DENPRES1 AS VARCHAR(100)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @PRES2 AS VARCHAR (50)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @DENPRES2 AS VARCHAR(100)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @PRES3 AS VARCHAR (50)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @DENPRES3 AS VARCHAR(100)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @PRES4 AS VARCHAR (50)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @DENPRES4 AS VARCHAR(100)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @FECINICIAL AS DATETIME"
 sConsulta = sConsulta & vbCrLf & ""
 sConsulta = sConsulta & vbCrLf & "SET CONCAT_NULL_YIELDS_NULL OFF"
 sConsulta = sConsulta & vbCrLf & ""
 sConsulta = sConsulta & vbCrLf & "DECLARE Cur_PROCE CURSOR FORWARD_ONLY STATIC READ_ONLY FOR"
 sConsulta = sConsulta & vbCrLf & ""
 sConsulta = sConsulta & vbCrLf & "  "
 sConsulta = sConsulta & vbCrLf & "SELECT GMN1.DEN,PROCE.GMN2, PROCE.GMN3, PROCE.GMN4"
 sConsulta = sConsulta & vbCrLf & ",GMN2.DEN, GMN3.DEN, GMN4.DEN, PROCE.CAMBIO,PROCE.ADJDIR"
 sConsulta = sConsulta & vbCrLf & "From Proce"
 sConsulta = sConsulta & vbCrLf & "INNER JOIN GMN1 ON PROCE.GMN1=GMN1.COD"
 sConsulta = sConsulta & vbCrLf & "INNER JOIN GMN2 ON  PROCE.GMN1=GMN2.GMN1 AND PROCE.GMN2=GMN2.COD"
 sConsulta = sConsulta & vbCrLf & "INNER JOIN GMN3 ON  PROCE.GMN1=GMN3.GMN1 AND  PROCE.GMN2=GMN3.GMN2 AND PROCE.GMN3=GMN3.COD"
 sConsulta = sConsulta & vbCrLf & "INNER JOIN GMN4 ON  PROCE.GMN1=GMN4.GMN1 AND  PROCE.GMN2=GMN4.GMN2 AND PROCE.GMN3=GMN4.GMN3 AND PROCE.GMN4=GMN4.COD"
 sConsulta = sConsulta & vbCrLf & "WHERE PROCE.ANYO=@ANYO AND PROCE.GMN1=@GMN1 AND PROCE.COD=@PROCE"
 sConsulta = sConsulta & vbCrLf & "OPEN Cur_PROCE"
 sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM cur_PROCE INTO @DENGMN1,@GMN2,@GMN3,@GMN4,@DENGMN2,@DENGMN3,@DENGMN4,@PROCECAMBIO,@ADJDIR"
 sConsulta = sConsulta & vbCrLf & "IF @@FETCH_STATUS<>0 GOTO ERR_"
 sConsulta = sConsulta & vbCrLf & ""
 sConsulta = sConsulta & vbCrLf & ""
 sConsulta = sConsulta & vbCrLf & "DECLARE Cur_ITEM_ADJ Cursor FORWARD_ONLY STATIC READ_ONLY FOR"
 sConsulta = sConsulta & vbCrLf & ""
 sConsulta = sConsulta & vbCrLf & "SELECT ITEM_ADJ.ITEM,ITEM_ADJ.PROVE,ITEM.FECINI,ITEM.FECFIN,(ITEM_OFE.PRECIO/(PROCE_OFE.CAMBIO*@PROCECAMBIO)) AS PRECADJ,(ITEM.CANT*(ITEM_ADJ.PORCEN/100)) AS CANTADJ,(ITEM.PREC/@PROCECAMBIO) AS PRECAPE"
 sConsulta = sConsulta & vbCrLf & ",ITEM.PAG,PAG.DEN,ITEM.UNI,UNI.DEN,ITEM.DEST,DEST.DEN"
 sConsulta = sConsulta & vbCrLf & ",ITEM.ART,ITEM.DESCR + ART4.DEN,ITEM.CANT,ITEM_PRESCON4.PRES1,PRES_4_CON1.DEN,ITEM_PRESCON4.PRES2,PRES_4_CON2.DEN,ITEM_PRESCON4.PRES3,PRES_4_CON3.DEN, ITEM_PRESCON4.PRES4,PRES_4_CON4.DEN"
 sConsulta = sConsulta & vbCrLf & "From ITEM_ADJ"
 sConsulta = sConsulta & vbCrLf & "INNER JOIN ITEM ON ITEM_ADJ.ANYO=ITEM.ANYO AND ITEM_ADJ.GMN1=ITEM.GMN1 AND ITEM_ADJ.PROCE=ITEM.PROCE  AND ITEM_ADJ.ITEM=ITEM.ID"
 sConsulta = sConsulta & vbCrLf & "INNER JOIN ITEM_PRESCON4 ON ITEM.ANYO=ITEM_PRESCON4.ANYO AND ITEM.GMN1=ITEM_PRESCON4.GMN1 AND ITEM.PROCE=ITEM_PRESCON4.PROCE AND ITEM.ID=ITEM_PRESCON4.ITEM AND ITEM_PRESCON4.PRES4 IS NOT NULL"
 sConsulta = sConsulta & vbCrLf & "INNER JOIN PRES_4_CON1 ON ITEM_PRESCON4.PRES1=PRES_4_CON1.COD"
 sConsulta = sConsulta & vbCrLf & "INNER JOIN PRES_4_CON2 ON ITEM_PRESCON4.PRES1=PRES_4_CON2.PRES1 AND ITEM_PRESCON4.PRES2=PRES_4_CON2.COD"
 sConsulta = sConsulta & vbCrLf & "INNER JOIN PRES_4_CON3 ON ITEM_PRESCON4.PRES1=PRES_4_CON3.PRES1 AND ITEM_PRESCON4.PRES2=PRES_4_CON3.PRES2 AND ITEM_PRESCON4.PRES3 =PRES_4_CON3.COD"
 sConsulta = sConsulta & vbCrLf & "INNER JOIN PRES_4_CON4 ON ITEM_PRESCON4.PRES1=PRES_4_CON4.PRES1 AND ITEM_PRESCON4.PRES2=PRES_4_CON4.PRES2 AND ITEM_PRESCON4.PRES3 =PRES_4_CON4.PRES3 AND ITEM_PRESCON4.PRES4 = PRES_4_CON4.COD"
 sConsulta = sConsulta & vbCrLf & "INNER JOIN PROCE_OFE ON ITEM_ADJ.ANYO=PROCE_OFE.ANYO AND ITEM_ADJ.GMN1=PROCE_OFE.GMN1 AND ITEM_ADJ.PROCE=PROCE_OFE.PROCE AND ITEM_ADJ.OFE=PROCE_OFE.OFE AND ITEM_ADJ.PROVE=PROCE_OFE.PROVE"
 sConsulta = sConsulta & vbCrLf & "INNER JOIN ITEM_OFE ON ITEM_ADJ.ANYO=ITEM_OFE.ANYO AND ITEM_OFE.GMN1=ITEM_OFE.GMN1 AND ITEM_ADJ.PROCE=ITEM_OFE.PROCE AND ITEM_ADJ.PROVE=ITEM_OFE.PROVE AND ITEM_ADJ.ITEM=ITEM_OFE.ITEM AND ITEM_ADJ.OFE=ITEM_OFE.NUM"
 sConsulta = sConsulta & vbCrLf & "INNER JOIN UNI ON ITEM.UNI=UNI.COD"
 sConsulta = sConsulta & vbCrLf & "INNER JOIN DEST ON ITEM.DEST=DEST.COD"
 sConsulta = sConsulta & vbCrLf & "INNER JOIN PAG ON ITEM.PAG=PAG.COD"
 sConsulta = sConsulta & vbCrLf & "LEFT JOIN ART4 ON ITEM.ART=ART4.COD AND ITEM.GMN1=ART4.GMN1 AND ITEM.GMN2=ART4.GMN2 AND ITEM.GMN3=ART4.GMN3 AND ITEM.GMN4=ART4.GMN4"
 sConsulta = sConsulta & vbCrLf & "WHERE ITEM_ADJ.ANYO=@ANYO AND ITEM_ADJ.GMN1=@GMN1 AND ITEM_ADJ.PROCE=@PROCE AND ITEM_ADJ.PORCEN <> 0"
 sConsulta = sConsulta & vbCrLf & "                    "
 sConsulta = sConsulta & vbCrLf & "                    "
 sConsulta = sConsulta & vbCrLf & "OPEN Cur_ITEM_ADJ"
 sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM Cur_ITEM_ADJ INTO @ITEM,@PROVE,@FECINI,@FECFIN,@PRECADJ,@CANTADJ,@PRECAPE"
 sConsulta = sConsulta & vbCrLf & ",@PAG,@DENPAG,@UNI,@DENUNI,@DEST,@DENDEST"
 sConsulta = sConsulta & vbCrLf & ",@ART,@DESCR,@CANT,@PRES1,@DENPRES1,@PRES2,@DENPRES2,@PRES3,@DENPRES3,@PRES4,@DENPRES4"
 sConsulta = sConsulta & vbCrLf & "WHILE @@FETCH_STATUS=0"
 sConsulta = sConsulta & vbCrLf & "BEGIN"
 sConsulta = sConsulta & vbCrLf & " "
 sConsulta = sConsulta & vbCrLf & "/* Tenemos que calcular el porcentaje correspondiente a cada mes del ANYO.*/"
 sConsulta = sConsulta & vbCrLf & "SET @TOTALDIAS = datediff(day,@FECINI, @FECFIN) + 1"
 sConsulta = sConsulta & vbCrLf & "    "
 sConsulta = sConsulta & vbCrLf & "SET @FECINICIAL = @FECINI"
 sConsulta = sConsulta & vbCrLf & "SET @FECFINAL = @FECFIN"
 sConsulta = sConsulta & vbCrLf & "SET @MESINI = MONTH(@FECINICIAL)"
 sConsulta = sConsulta & vbCrLf & "SET @FECHA = @FECINICIAL"
 sConsulta = sConsulta & vbCrLf & "            "
 sConsulta = sConsulta & vbCrLf & "WHILE @MESINI < MONTH(@FECFINAL)"
 sConsulta = sConsulta & vbCrLf & "BEGIN"
 sConsulta = sConsulta & vbCrLf & "/* Calculamos la diferencia en d�as entre el mes inicial y el siguiente mes*/  "
 sConsulta = sConsulta & vbCrLf & "SET @DIAS = datediff(day,@FECHA,cast( str (month(@FECHA) +1) + '/1/' + str(year(@FECHA) ) as datetime))"
 sConsulta = sConsulta & vbCrLf & "SET @PORCEN = @DIAS / @TOTALDIAS"
 sConsulta = sConsulta & vbCrLf & "SET @FECHA = cast(str((month(@FECHA) +1)) + '/1/' + str(year(@FECHA)) as datetime)"
 sConsulta = sConsulta & vbCrLf & "  "
 sConsulta = sConsulta & vbCrLf & "/*Miramos si hab�a alg�n resultado PRESa ese mes*/"
 sConsulta = sConsulta & vbCrLf & "IF (SELECT COUNT(*) FROM AR_ITEM_MES_PRESCON4_4 WHERE"
 sConsulta = sConsulta & vbCrLf & "ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM AND ANYOADJ=YEAR(@FECINICIAL) AND MES=@MESINI AND PRES1=@PRES1 AND PRES2=@PRES2 AND PRES3=@PRES3 AND PRES4=@PRES4)=0"
 sConsulta = sConsulta & vbCrLf & "BEGIN"
 sConsulta = sConsulta & vbCrLf & "    "
 sConsulta = sConsulta & vbCrLf & "INSERT INTO  AR_ITEM_MES_PRESCON4_4 ( ANYO,GMN1,PROCE,ITEM,ANYOADJ,MES,DENGMN1,ADJDIR,GMN2,GMN3,GMN4,DENGMN2,DENGMN3,DENGMN4"
 sConsulta = sConsulta & vbCrLf & ",ART,DESCR,CANT"
 sConsulta = sConsulta & vbCrLf & ",UNI,DENUNI,DEST,DENDEST,PAG,DENPAG,FECINI,FECFIN,PRES,PREC,CANTPERIODO,PRES1,DENPRES1,PRES2,DENPRES2,PRES3,DENPRES3,PRES4,DENPRES4)"
 sConsulta = sConsulta & vbCrLf & " "
 sConsulta = sConsulta & vbCrLf & "VALUES(@ANYO,@GMN1, @PROCE , @ITEM,YEAR(@FECINICIAL),@MESINI, @DENGMN1,@ADJDIR, @GMN2, @GMN3, @GMN4 ,@DENGMN2, @DENGMN3, @DENGMN4"
 sConsulta = sConsulta & vbCrLf & ",@ART,@DESCR,@CANT"
 sConsulta = sConsulta & vbCrLf & ",@UNI,@DENUNI,@DEST,@DENDEST,@PAG,@DENPAG,@FECINI,@FECFIN,@PRECAPE*@PORCEN*@CANTADJ,@PRECADJ*@PORCEN*@CANTADJ,@CANT*@PORCEN,@PRES1,@DENPRES1,@PRES2,@DENPRES2,@PRES3,@DENPRES3,@PRES4,@DENPRES4)"
 sConsulta = sConsulta & vbCrLf & "    "
 sConsulta = sConsulta & vbCrLf & "End"
 sConsulta = sConsulta & vbCrLf & "Else"
 sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_4 SET PRES=PRES+@PRECAPE*@PORCEN*@CANTADJ, PREC=PREC+@PRECADJ*@PORCEN*@CANTADJ"
 sConsulta = sConsulta & vbCrLf & "WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND MES=@MESINI AND ANYOADJ=YEAR(@FECINICIAL)  AND ITEM=@ITEM AND PRES1=@PRES1 AND PRES2=@PRES2 AND PRES3=@PRES3 AND PRES4=@PRES4"
 sConsulta = sConsulta & vbCrLf & "  "
 sConsulta = sConsulta & vbCrLf & "SET @MESINI=@MESINI + 1"
 sConsulta = sConsulta & vbCrLf & "  "
 sConsulta = sConsulta & vbCrLf & "  "
 sConsulta = sConsulta & vbCrLf & "End"
 sConsulta = sConsulta & vbCrLf & ""
 sConsulta = sConsulta & vbCrLf & "/*Si el m�s es el mismo*/"
 sConsulta = sConsulta & vbCrLf & "IF @MESINI = MONTH(@FECFINAL)"
 sConsulta = sConsulta & vbCrLf & "BEGIN"
 sConsulta = sConsulta & vbCrLf & ""
 sConsulta = sConsulta & vbCrLf & "SET @DIAS = datediff(day,@FECHA,@FECFINAL) + 1"
 sConsulta = sConsulta & vbCrLf & "SET @PORCEN = @DIAS/@TOTALDIAS"
 sConsulta = sConsulta & vbCrLf & ""
 sConsulta = sConsulta & vbCrLf & "/*Miramos si hab�a alg�n resultado PRES a ese mes*/"
 sConsulta = sConsulta & vbCrLf & "IF (SELECT COUNT(*) FROM AR_ITEM_MES_PRESCON4_4 WHERE"
 sConsulta = sConsulta & vbCrLf & "ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM AND ANYOADJ=YEAR(@FECINICIAL) AND MES=@MESINI AND PRES1=@PRES1 AND PRES2=@PRES2 AND PRES3=@PRES3 AND PRES4=@PRES4)=0"
 sConsulta = sConsulta & vbCrLf & "BEGIN"
 sConsulta = sConsulta & vbCrLf & "    "
 sConsulta = sConsulta & vbCrLf & "INSERT INTO  AR_ITEM_MES_PRESCON4_4 ( ANYO, GMN1, PROCE, ITEM, ANYOADJ, MES, DENGMN1, ADJDIR, GMN2, GMN3, GMN4, DENGMN2, DENGMN3,DENGMN4"
 sConsulta = sConsulta & vbCrLf & ", ART, DESCR, CANT, UNI, DENUNI, DEST, DENDEST, PAG, DENPAG, FECINI, FECFIN, PRES, PREC, CANTPERIODO, PRES1, DENPRES1,PRES2,DENPRES2,PRES3,DENPRES3,PRES4,DENPRES4)"
 sConsulta = sConsulta & vbCrLf & "VALUES(@ANYO,@GMN1, @PROCE , @ITEM,YEAR(@FECINICIAL),@MESINI, @DENGMN1,@ADJDIR, @GMN2, @GMN3, @GMN4 ,@DENGMN2, @DENGMN3, @DENGMN4"
 sConsulta = sConsulta & vbCrLf & ",@ART,@DESCR,@CANT,@UNI,@DENUNI,@DEST,@DENDEST,@PAG,@DENPAG,@FECINI,@FECFIN, @PRECAPE*@PORCEN*@CANTADJ,@PRECADJ*@PORCEN*@CANTADJ,@CANT*@PORCEN,@PRES1,@DENPRES1,@PRES2,@DENPRES2,@PRES3,@DENPRES3,@PRES4,@DENPRES4)"
 sConsulta = sConsulta & vbCrLf & "    "
 sConsulta = sConsulta & vbCrLf & "End"
 sConsulta = sConsulta & vbCrLf & "Else"
 sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_4 SET PRES=PRES+@PRECAPE*@PORCEN*@CANTADJ, PREC=PREC+@PRECADJ*@PORCEN*@CANTADJ"
 sConsulta = sConsulta & vbCrLf & "WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND MES=@MESINI AND ANYOADJ=YEAR(@FECINICIAL)  AND ITEM=@ITEM AND PRES1=@PRES1 AND PRES2=@PRES2 AND PRES3=@PRES3 AND PRES4=@PRES4"
 sConsulta = sConsulta & vbCrLf & ""
 sConsulta = sConsulta & vbCrLf & ""
 sConsulta = sConsulta & vbCrLf & "End"
 sConsulta = sConsulta & vbCrLf & " "
 sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM Cur_ITEM_ADJ INTO @ITEM,@PROVE,@FECINI,@FECFIN,@PRECADJ,@CANTADJ,@PRECAPE"
 sConsulta = sConsulta & vbCrLf & ",@PAG,@DENPAG,@UNI,@DENUNI,@DEST,@DENDEST"
 sConsulta = sConsulta & vbCrLf & ",@ART,@DESCR,@CANT,@PRES1,@DENPRES1,@PRES2,@DENPRES2,@PRES3,@DENPRES3,@PRES4,@DENPRES4"
 sConsulta = sConsulta & vbCrLf & "End"
 sConsulta = sConsulta & vbCrLf & "ERR_:"
 sConsulta = sConsulta & vbCrLf & "Close Cur_PROCE"
 sConsulta = sConsulta & vbCrLf & "DEALLOCATE Cur_PROCE"
 sConsulta = sConsulta & vbCrLf & "Close Cur_ITEM_ADJ"
 sConsulta = sConsulta & vbCrLf & "DEALLOCATE Cur_ITEM_ADJ"
 sConsulta = sConsulta & vbCrLf & "    "
 sConsulta = sConsulta & vbCrLf & "SET CONCAT_NULL_YIELDS_NULL ON"
 sConsulta = sConsulta & vbCrLf & "SET @RES=@@ERROR"
 sConsulta = sConsulta & vbCrLf & "Return"
 sConsulta = sConsulta & vbCrLf & "End"

CrearSP_AR_ITEM_MES_PRESCON4_4 = sConsulta
End Function

Private Function CrearSP_AR_ITEM_PRESCON4() As String
Dim sConsulta As String

 sConsulta = "CREATE PROCEDURE SP_AR_ITEM_PRESCON4 (@ANYO SMALLINT,@GMN1 VARCHAR(50), @PROCE INT,@RES INT OUTPUT)  AS"
 sConsulta = sConsulta & vbCrLf & "BEGIN"
 sConsulta = sConsulta & vbCrLf & "DECLARE @ITEM AS INTEGER"
 sConsulta = sConsulta & vbCrLf & "DECLARE @DENGMN1 AS VARCHAR(100)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @DENPRES1 AS VARCHAR(100)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @DENPRES2 AS VARCHAR(100)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @DENPRES3 AS VARCHAR(100)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @DENPRES4 AS VARCHAR(100)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @PROCECAMBIO AS FLOAT"
 sConsulta = sConsulta & vbCrLf & "DECLARE @ADJDIR AS BIT"
 sConsulta = sConsulta & vbCrLf & "DECLARE @PRES1 AS VARCHAR (50)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @PRES2 AS VARCHAR (50)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @PRES3 AS VARCHAR (50)"
 sConsulta = sConsulta & vbCrLf & "DECLARE @PRES4 AS VARCHAR (50)"
 sConsulta = sConsulta & vbCrLf & ""
 sConsulta = sConsulta & vbCrLf & "SET CONCAT_NULL_YIELDS_NULL OFF"
 sConsulta = sConsulta & vbCrLf & ""
 sConsulta = sConsulta & vbCrLf & "DECLARE Cur_PROCE CURSOR FOR SELECT GMN1.DEN,PROCE.CAMBIO,PROCE.ADJDIR"
 sConsulta = sConsulta & vbCrLf & "From Proce, gmn1"
 sConsulta = sConsulta & vbCrLf & "WHERE PROCE.ANYO=@ANYO AND PROCE.GMN1=@GMN1 AND PROCE.COD=@PROCE"
 sConsulta = sConsulta & vbCrLf & "AND GMN1.COD=PROCE.GMN1"
 sConsulta = sConsulta & vbCrLf & "OPEN Cur_PROCE"
 sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM cur_PROCE INTO @DENGMN1,@PROCECAMBIO,@ADJDIR"
 sConsulta = sConsulta & vbCrLf & "IF @@FETCH_STATUS<>0 GOTO ERR_"
 sConsulta = sConsulta & vbCrLf & ""
 sConsulta = sConsulta & vbCrLf & ""
 sConsulta = sConsulta & vbCrLf & "DECLARE Cur_ITEM Cursor FOR SELECT DISTINCT ITEM_ADJ.ITEM,ITEM_PRESCON4.PRES1,PRES_4_CON1.DEN,ITEM_PRESCON4.PRES2,PRES_4_CON2.DEN,ITEM_PRESCON4.PRES3,PRES_4_CON3.DEN, ITEM_PRESCON4.PRES4,PRES_4_CON4.DEN"
 sConsulta = sConsulta & vbCrLf & "From ITEM_ADJ"
 sConsulta = sConsulta & vbCrLf & "INNER JOIN ITEM ON ITEM_ADJ.ANYO=ITEM.ANYO AND ITEM_ADJ.GMN1=ITEM.GMN1 AND ITEM_ADJ.PROCE=ITEM.PROCE  AND ITEM_ADJ.ITEM=ITEM.ID"
 sConsulta = sConsulta & vbCrLf & "INNER JOIN ITEM_PRESCON4 ON ITEM.ANYO=ITEM_PRESCON4.ANYO AND ITEM.GMN1=ITEM_PRESCON4.GMN1 AND ITEM.PROCE=ITEM_PRESCON4.PROCE AND ITEM.ID=ITEM_PRESCON4.ITEM AND ITEM_PRESCON4.PRES4 IS NOT NULL"
 sConsulta = sConsulta & vbCrLf & "INNER JOIN PRES_4_CON1 ON ITEM_PRESCON4.PRES1=PRES_4_CON1.COD"
 sConsulta = sConsulta & vbCrLf & "LEFT JOIN PRES_4_CON2 ON ITEM_PRESCON4.PRES1=PRES_4_CON2.PRES1 AND ITEM_PRESCON4.PRES2=PRES_4_CON2.COD"
 sConsulta = sConsulta & vbCrLf & "LEFT JOIN PRES_4_CON3 ON ITEM_PRESCON4.PRES1=PRES_4_CON3.PRES1 AND ITEM_PRESCON4.PRES2=PRES_4_CON3.PRES2 AND ITEM_PRESCON4.PRES3 =PRES_4_CON3.COD"
 sConsulta = sConsulta & vbCrLf & "LEFT JOIN PRES_4_CON4 ON ITEM_PRESCON4.PRES1=PRES_4_CON4.PRES1 AND ITEM_PRESCON4.PRES2=PRES_4_CON4.PRES2 AND ITEM_PRESCON4.PRES3 =PRES_4_CON4.PRES3 AND ITEM_PRESCON4.PRES4 = PRES_4_CON4.COD"
 sConsulta = sConsulta & vbCrLf & "INNER JOIN PROCE_OFE ON ITEM_ADJ.ANYO=PROCE_OFE.ANYO AND ITEM_ADJ.GMN1=PROCE_OFE.GMN1 AND ITEM_ADJ.PROCE=PROCE_OFE.PROCE AND ITEM_ADJ.OFE=PROCE_OFE.OFE AND ITEM_ADJ.PROVE=PROCE_OFE.PROVE"
 sConsulta = sConsulta & vbCrLf & "INNER JOIN ITEM_OFE ON ITEM_ADJ.ANYO=ITEM_OFE.ANYO AND ITEM_OFE.GMN1=ITEM_OFE.GMN1 AND ITEM_ADJ.PROCE=ITEM_OFE.PROCE AND ITEM_ADJ.PROVE=ITEM_OFE.PROVE AND ITEM_ADJ.ITEM=ITEM_OFE.ITEM AND ITEM_ADJ.OFE=ITEM_OFE.NUM"
 sConsulta = sConsulta & vbCrLf & "LEFT JOIN ART4 ON ITEM.ART=ART4.COD AND ITEM.GMN1=ART4.GMN1 AND ITEM.GMN2=ART4.GMN2 AND ITEM.GMN3=ART4.GMN3 AND ITEM.GMN4=ART4.GMN4"
 sConsulta = sConsulta & vbCrLf & "WHERE ITEM_ADJ.ANYO=@ANYO AND ITEM_ADJ.GMN1=@GMN1 AND ITEM_ADJ.PROCE=@PROCE AND ITEM_ADJ.PORCEN <> 0"
 sConsulta = sConsulta & vbCrLf & "OPEN Cur_ITEM"
 sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM Cur_ITEM INTO @ITEM,@PRES1,@DENPRES1,@PRES2,@DENPRES2,@PRES3,@DENPRES3,@PRES4,@DENPRES4"
 sConsulta = sConsulta & vbCrLf & "WHILE @@FETCH_STATUS=0"
 sConsulta = sConsulta & vbCrLf & ""
 sConsulta = sConsulta & vbCrLf & ""
 sConsulta = sConsulta & vbCrLf & "BEGIN"
 sConsulta = sConsulta & vbCrLf & "INSERT INTO  AR_ITEM_PRESCON4 ( ANYO,GMN1,PROCE,ITEM,PRES1,PRES2,PRES3,PRES4,DENPRES1,DENPRES2,DENPRES3,DENPRES4"
 sConsulta = sConsulta & vbCrLf & ",DENGMN1,ADJDIR,ART,DESCR,CANT,PRES,PREC )"
 sConsulta = sConsulta & vbCrLf & "    "
 sConsulta = sConsulta & vbCrLf & "SELECT  @ANYO , @GMN1, @PROCE , @ITEM, @PRES1,@PRES2,@PRES3,@PRES4,@DENPRES1,@DENPRES2, @DENPRES3, @DENPRES4"
 sConsulta = sConsulta & vbCrLf & ",@DENGMN1,@ADJDIR, ITEM.ART,ITEM.DESCR + ART4.DEN,ITEM.CANT"
 sConsulta = sConsulta & vbCrLf & ",(SELECT SUM((ITEM_ADJ.PORCEN/100)*ITEM.CANT*ITEM.PREC/@PROCECAMBIO)"
 sConsulta = sConsulta & vbCrLf & "AS PRES FROM ITEM_ADJ INNER JOIN ITEM"
 sConsulta = sConsulta & vbCrLf & "ON ITEM_ADJ.ANYO=ITEM.ANYO AND ITEM_ADJ.GMN1=ITEM.GMN1 AND ITEM_ADJ.PROCE=ITEM.PROCE AND ITEM_ADJ.ITEM=ITEM.ID AND ITEM_ADJ.ITEM=@ITEM"
 sConsulta = sConsulta & vbCrLf & "AND ITEM_ADJ.ANYO=@ANYO AND ITEM_ADJ.GMN1=@GMN1 AND ITEM_ADJ.PROCE=@PROCE AND ITEM_ADJ.PORCEN <> 0 )"
 sConsulta = sConsulta & vbCrLf & ",(SELECT SUM((ITEM_ADJ.PORCEN/100)*ITEM.CANT*ITEM_OFE.PRECIO/(PROCE_OFE.CAMBIO*@PROCECAMBIO))"
 sConsulta = sConsulta & vbCrLf & "AS PREC FROM ITEM_ADJ INNER JOIN ITEM"
 sConsulta = sConsulta & vbCrLf & "ON ITEM_ADJ.ANYO=ITEM.ANYO AND ITEM_ADJ.GMN1=ITEM.GMN1 AND ITEM_ADJ.PROCE=ITEM.PROCE AND ITEM_ADJ.ITEM=ITEM.ID AND ITEM_ADJ.ITEM=@ITEM"
 sConsulta = sConsulta & vbCrLf & "AND ITEM_ADJ.ANYO=@ANYO AND ITEM_ADJ.GMN1=@GMN1 AND ITEM_ADJ.PROCE=@PROCE AND ITEM_ADJ.PORCEN <> 0"
 sConsulta = sConsulta & vbCrLf & "INNER JOIN ITEM_OFE"
 sConsulta = sConsulta & vbCrLf & "ON ITEM_ADJ.ANYO=ITEM_OFE.ANYO AND ITEM_ADJ.GMN1=ITEM_OFE.GMN1 AND ITEM_ADJ.PROCE=ITEM_OFE.PROCE"
 sConsulta = sConsulta & vbCrLf & "AND ITEM_ADJ.OFE=ITEM_OFE.NUM AND ITEM_ADJ.PROVE=ITEM_OFE.PROVE"
 sConsulta = sConsulta & vbCrLf & "AND ITEM_ADJ.ITEM=ITEM_OFE.ITEM"
 sConsulta = sConsulta & vbCrLf & "INNER JOIN PROCE_OFE"
 sConsulta = sConsulta & vbCrLf & "ON ITEM_OFE.ANYO=PROCE_OFE.ANYO AND ITEM_OFE.GMN1=PROCE_OFE.GMN1 AND ITEM_OFE.PROCE=PROCE_OFE.PROCE AND ITEM_OFE.NUM=PROCE_OFE.OFE"
 sConsulta = sConsulta & vbCrLf & "AND ITEM_OFE.PROVE=PROCE_OFE.PROVE)"
 sConsulta = sConsulta & vbCrLf & "From Item"
 sConsulta = sConsulta & vbCrLf & "LEFT JOIN ART4 ON ITEM.ART=ART4.COD AND ITEM.GMN1=ART4.GMN1 AND ITEM.GMN2=ART4.GMN2 AND ITEM.GMN3=ART4.GMN3 AND ITEM.GMN4=ART4.GMN4"
 sConsulta = sConsulta & vbCrLf & "WHERE ITEM.ANYO=@ANYO AND ITEM.GMN1=@GMN1 AND ITEM.PROCE=@PROCE AND ITEM.ID=@ITEM"
 sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM Cur_ITEM INTO @ITEM,@PRES1,@DENPRES1,@PRES2,@DENPRES2,@PRES3,@DENPRES3,@PRES4,@DENPRES4"
 sConsulta = sConsulta & vbCrLf & "End"
 sConsulta = sConsulta & vbCrLf & ""
 sConsulta = sConsulta & vbCrLf & "ERR_:"
 sConsulta = sConsulta & vbCrLf & "Close Cur_PROCE"
 sConsulta = sConsulta & vbCrLf & "DEALLOCATE Cur_PROCE"
 sConsulta = sConsulta & vbCrLf & "Close Cur_ITEM"
 sConsulta = sConsulta & vbCrLf & "DEALLOCATE Cur_ITEM"
 sConsulta = sConsulta & vbCrLf & "SET CONCAT_NULL_YIELDS_NULL ON"
 sConsulta = sConsulta & vbCrLf & "SELECT @RES=@@ERROR"
 sConsulta = sConsulta & vbCrLf & "Return"
 sConsulta = sConsulta & vbCrLf & "End"

CrearSP_AR_ITEM_PRESCON4 = sConsulta
End Function

Private Function CrearTablaAR_ITEM_MES_PRESCON4_1() As String
Dim sConsulta As String

    sConsulta = "CREATE TABLE [dbo].[AR_ITEM_MES_PRESCON4_1] ("
    sConsulta = sConsulta & vbCrLf & "[ANYO] [smallint] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[GMN1] [varchar] (" & gLongitudesDeCodigos.giLongCodGMN1 & ")  NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[PROCE] [smallint] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[ITEM] [int] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[ANYOADJ] [smallint] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[MES] [smallint] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[PRES1] [varchar] (15)  NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[DENPRES1] [varchar] (100)  NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[DENGMN1] [varchar] (100)  NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[ADJDIR] [tinyint] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[GMN2] [varchar] (" & gLongitudesDeCodigos.giLongCodGMN2 & ")  NULL ,"
    sConsulta = sConsulta & vbCrLf & "[GMN3] [varchar] (" & gLongitudesDeCodigos.giLongCodGMN3 & ")  NULL ,"
    sConsulta = sConsulta & vbCrLf & "[GMN4] [varchar] (" & gLongitudesDeCodigos.giLongCodGMN4 & ")  NULL ,"
    sConsulta = sConsulta & vbCrLf & "[DENGMN2] [varchar] (100)  NULL ,"
    sConsulta = sConsulta & vbCrLf & "[DENGMN3] [varchar] (100)  NULL ,"
    sConsulta = sConsulta & vbCrLf & "[DENGMN4] [varchar] (100)  NULL ,"
    sConsulta = sConsulta & vbCrLf & "[ART] [varchar] (" & gLongitudesDeCodigos.giLongCodART & ")  NULL ,"
    sConsulta = sConsulta & vbCrLf & "[DESCR] [varchar] (100)  NULL ,"
    sConsulta = sConsulta & vbCrLf & "[CANT] [float] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[CANTPERIODO] [float] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[UNI] [varchar] (" & gLongitudesDeCodigos.giLongCodUNI & ")  NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[DENUNI] [varchar] (100)  NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[DEST] [varchar] (4)  NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[DENDEST] [varchar] (100)  NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[PAG] [varchar] (" & gLongitudesDeCodigos.giLongCodPAG & ")  NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[DENPAG] [varchar] (100)  NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[FECINI] [datetime] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[FECFIN] [datetime] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[PRES] [float] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[PREC] [float] NOT NULL"
    sConsulta = sConsulta & vbCrLf & ") ON [PRIMARY]"

    sConsulta = sConsulta & vbCrLf & "ALTER TABLE [dbo].[AR_ITEM_MES_PRESCON4_1] WITH NOCHECK ADD"
    sConsulta = sConsulta & vbCrLf & "CONSTRAINT [PK_AR_ITEM_MES_PRESCON4_1] PRIMARY KEY  CLUSTERED"
    sConsulta = sConsulta & vbCrLf & "("
    sConsulta = sConsulta & vbCrLf & "[ANYO],"
    sConsulta = sConsulta & vbCrLf & "[GMN1],"
    sConsulta = sConsulta & vbCrLf & "[PROCE],"
    sConsulta = sConsulta & vbCrLf & "[ITEM],"
    sConsulta = sConsulta & vbCrLf & "[ANYOADJ],"
    sConsulta = sConsulta & vbCrLf & "[MES],"
    sConsulta = sConsulta & vbCrLf & "[PRES1]"
    sConsulta = sConsulta & vbCrLf & ")  ON [PRIMARY]"

    CrearTablaAR_ITEM_MES_PRESCON4_1 = sConsulta
End Function

Private Function CrearTablaAR_ITEM_MES_PRESCON4_2() As String
Dim sConsulta As String

    sConsulta = "CREATE TABLE [dbo].[AR_ITEM_MES_PRESCON4_2] ("
    sConsulta = sConsulta & vbCrLf & "[ANYO] [smallint] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[GMN1] [varchar] (" & gLongitudesDeCodigos.giLongCodGMN1 & ")  NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[PROCE] [smallint] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[ITEM] [int] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[ANYOADJ] [smallint] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[MES] [smallint] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[PRES1] [varchar] (15)  NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[PRES2] [varchar] (15)  NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[DENPRES1] [varchar] (100)  NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[DENPRES2] [varchar] (100)  NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[DENGMN1] [varchar] (100)  NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[ADJDIR] [tinyint] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[GMN2] [varchar] (" & gLongitudesDeCodigos.giLongCodGMN2 & ")  NULL ,"
    sConsulta = sConsulta & vbCrLf & "[GMN3] [varchar] (" & gLongitudesDeCodigos.giLongCodGMN3 & ")  NULL ,"
    sConsulta = sConsulta & vbCrLf & "[GMN4] [varchar] (" & gLongitudesDeCodigos.giLongCodGMN4 & ")  NULL ,"
    sConsulta = sConsulta & vbCrLf & "[DENGMN2] [varchar] (100)  NULL ,"
    sConsulta = sConsulta & vbCrLf & "[DENGMN3] [varchar] (100)  NULL ,"
    sConsulta = sConsulta & vbCrLf & "[DENGMN4] [varchar] (100)  NULL ,"
    sConsulta = sConsulta & vbCrLf & "[ART] [varchar] (" & gLongitudesDeCodigos.giLongCodART & ")  NULL ,"
    sConsulta = sConsulta & vbCrLf & "[DESCR] [varchar] (100)  NULL ,"
    sConsulta = sConsulta & vbCrLf & "[CANT] [float] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[CANTPERIODO] [float] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[UNI] [varchar] (" & gLongitudesDeCodigos.giLongCodUNI & ")  NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[DENUNI] [varchar] (100)  NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[DEST] [varchar] (4)  NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[DENDEST] [varchar] (100)  NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[PAG] [varchar] (" & gLongitudesDeCodigos.giLongCodPAG & ")  NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[DENPAG] [varchar] (100)  NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[FECINI] [datetime] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[FECFIN] [datetime] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[PRES] [float] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[PREC] [float] NOT NULL"
    sConsulta = sConsulta & vbCrLf & ") ON [PRIMARY]"

    sConsulta = sConsulta & vbCrLf & "ALTER TABLE [dbo].[AR_ITEM_MES_PRESCON4_2] WITH NOCHECK ADD"
    sConsulta = sConsulta & vbCrLf & "CONSTRAINT [PK_AR_ITEM_MES_PRESCON4_2] PRIMARY KEY  CLUSTERED"
    sConsulta = sConsulta & vbCrLf & "("
    sConsulta = sConsulta & vbCrLf & "[ANYO],"
    sConsulta = sConsulta & vbCrLf & "[GMN1],"
    sConsulta = sConsulta & vbCrLf & "[PROCE],"
    sConsulta = sConsulta & vbCrLf & "[ITEM],"
    sConsulta = sConsulta & vbCrLf & "[ANYOADJ],"
    sConsulta = sConsulta & vbCrLf & "[MES],"
    sConsulta = sConsulta & vbCrLf & "[PRES1],"
    sConsulta = sConsulta & vbCrLf & "[PRES2]"
    sConsulta = sConsulta & vbCrLf & ")  ON [PRIMARY]"

    CrearTablaAR_ITEM_MES_PRESCON4_2 = sConsulta
End Function

Private Function CrearTablaAR_ITEM_MES_PRESCON4_3() As String
Dim sConsulta As String

    sConsulta = "CREATE TABLE [dbo].[AR_ITEM_MES_PRESCON4_3] ("
    sConsulta = sConsulta & vbCrLf & "[ANYO] [smallint] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[GMN1] [varchar] (" & gLongitudesDeCodigos.giLongCodGMN1 & ")  NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[PROCE] [smallint] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[ITEM] [int] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[ANYOADJ] [smallint] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[MES] [smallint] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[PRES1] [varchar] (15)  NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[PRES2] [varchar] (15)  NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[PRES3] [varchar] (15)  NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[DENPRES1] [varchar] (100)  NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[DENPRES2] [varchar] (100)  NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[DENPRES3] [varchar] (100)  NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[DENGMN1] [varchar] (100)  NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[ADJDIR] [tinyint] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[GMN2] [varchar] (" & gLongitudesDeCodigos.giLongCodGMN2 & ")  NULL ,"
    sConsulta = sConsulta & vbCrLf & "[GMN3] [varchar] (" & gLongitudesDeCodigos.giLongCodGMN3 & ")  NULL ,"
    sConsulta = sConsulta & vbCrLf & "[GMN4] [varchar] (" & gLongitudesDeCodigos.giLongCodGMN4 & ")  NULL ,"
    sConsulta = sConsulta & vbCrLf & "[DENGMN2] [varchar] (100)  NULL ,"
    sConsulta = sConsulta & vbCrLf & "[DENGMN3] [varchar] (100)  NULL ,"
    sConsulta = sConsulta & vbCrLf & "[DENGMN4] [varchar] (100)  NULL ,"
    sConsulta = sConsulta & vbCrLf & "[ART] [varchar] (" & gLongitudesDeCodigos.giLongCodART & ")  NULL ,"
    sConsulta = sConsulta & vbCrLf & "[DESCR] [varchar] (100)  NULL ,"
    sConsulta = sConsulta & vbCrLf & "[CANT] [float] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[CANTPERIODO] [float] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[UNI] [varchar] (" & gLongitudesDeCodigos.giLongCodUNI & ")  NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[DENUNI] [varchar] (100)  NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[DEST] [varchar] (4)  NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[DENDEST] [varchar] (100)  NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[PAG] [varchar] (" & gLongitudesDeCodigos.giLongCodPAG & ")  NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[DENPAG] [varchar] (100)  NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[FECINI] [datetime] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[FECFIN] [datetime] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[PRES] [float] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[PREC] [float] NOT NULL"
    sConsulta = sConsulta & vbCrLf & ") ON [PRIMARY]"

    sConsulta = sConsulta & vbCrLf & "ALTER TABLE [dbo].[AR_ITEM_MES_PRESCON4_3] WITH NOCHECK ADD"
    sConsulta = sConsulta & vbCrLf & "CONSTRAINT [PK_AR_ITEM_MES_PRESCON4_3] PRIMARY KEY  CLUSTERED"
    sConsulta = sConsulta & vbCrLf & "("
    sConsulta = sConsulta & vbCrLf & "[ANYO],"
    sConsulta = sConsulta & vbCrLf & "[GMN1],"
    sConsulta = sConsulta & vbCrLf & "[PROCE],"
    sConsulta = sConsulta & vbCrLf & "[ITEM],"
    sConsulta = sConsulta & vbCrLf & "[ANYOADJ],"
    sConsulta = sConsulta & vbCrLf & "[MES],"
    sConsulta = sConsulta & vbCrLf & "[PRES1],"
    sConsulta = sConsulta & vbCrLf & "[PRES2],"
    sConsulta = sConsulta & vbCrLf & "[PRES3]"
    sConsulta = sConsulta & vbCrLf & ")  ON [PRIMARY]"

    CrearTablaAR_ITEM_MES_PRESCON4_3 = sConsulta
End Function

Private Function CrearTablaAR_ITEM_MES_PRESCON4_4() As String
Dim sConsulta As String

    sConsulta = "CREATE TABLE [dbo].[AR_ITEM_MES_PRESCON4_4] ("
    sConsulta = sConsulta & vbCrLf & "[ANYO] [smallint] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[GMN1] [varchar] (" & gLongitudesDeCodigos.giLongCodGMN1 & ")  NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[PROCE] [smallint] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[ITEM] [int] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[ANYOADJ] [smallint] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[MES] [smallint] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[PRES1] [varchar] (15)  NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[PRES2] [varchar] (15)  NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[PRES3] [varchar] (15)  NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[PRES4] [varchar] (15)  NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[DENPRES1] [varchar] (100)  NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[DENPRES2] [varchar] (100)  NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[DENPRES3] [varchar] (100)  NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[DENPRES4] [varchar] (100)  NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[DENGMN1] [varchar] (100)  NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[ADJDIR] [tinyint] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[GMN2] [varchar] (" & gLongitudesDeCodigos.giLongCodGMN2 & ")  NULL ,"
    sConsulta = sConsulta & vbCrLf & "[GMN3] [varchar] (" & gLongitudesDeCodigos.giLongCodGMN3 & ")  NULL ,"
    sConsulta = sConsulta & vbCrLf & "[GMN4] [varchar] (" & gLongitudesDeCodigos.giLongCodGMN4 & ")  NULL ,"
    sConsulta = sConsulta & vbCrLf & "[DENGMN2] [varchar] (100)  NULL ,"
    sConsulta = sConsulta & vbCrLf & "[DENGMN3] [varchar] (100)  NULL ,"
    sConsulta = sConsulta & vbCrLf & "[DENGMN4] [varchar] (100)  NULL ,"
    sConsulta = sConsulta & vbCrLf & "[ART] [varchar] (" & gLongitudesDeCodigos.giLongCodART & ")  NULL ,"
    sConsulta = sConsulta & vbCrLf & "[DESCR] [varchar] (100)  NULL ,"
    sConsulta = sConsulta & vbCrLf & "[CANT] [float] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[CANTPERIODO] [float] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[UNI] [varchar] (" & gLongitudesDeCodigos.giLongCodUNI & ")  NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[DENUNI] [varchar] (100)  NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[DEST] [varchar] (4)  NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[DENDEST] [varchar] (100)  NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[PAG] [varchar] (" & gLongitudesDeCodigos.giLongCodPAG & ")  NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[DENPAG] [varchar] (100)  NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[FECINI] [datetime] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[FECFIN] [datetime] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[PRES] [float] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[PREC] [float] NOT NULL"
    sConsulta = sConsulta & vbCrLf & ") ON [PRIMARY]"

    sConsulta = sConsulta & vbCrLf & "ALTER TABLE [dbo].[AR_ITEM_MES_PRESCON4_4] WITH NOCHECK ADD"
    sConsulta = sConsulta & vbCrLf & "CONSTRAINT [PK_AR_ITEM_MES_PRESCON4_4] PRIMARY KEY  CLUSTERED"
    sConsulta = sConsulta & vbCrLf & "("
    sConsulta = sConsulta & vbCrLf & "[ANYO],"
    sConsulta = sConsulta & vbCrLf & "[GMN1],"
    sConsulta = sConsulta & vbCrLf & "[PROCE],"
    sConsulta = sConsulta & vbCrLf & "[ITEM],"
    sConsulta = sConsulta & vbCrLf & "[ANYOADJ],"
    sConsulta = sConsulta & vbCrLf & "[MES],"
    sConsulta = sConsulta & vbCrLf & "[PRES1],"
    sConsulta = sConsulta & vbCrLf & "[PRES2],"
    sConsulta = sConsulta & vbCrLf & "[PRES3],"
    sConsulta = sConsulta & vbCrLf & "[PRES4]"
    sConsulta = sConsulta & vbCrLf & ")  ON [PRIMARY]"
    
    CrearTablaAR_ITEM_MES_PRESCON4_4 = sConsulta
End Function

Private Function CrearTablaAR_ITEM_PRESCON4() As String
    Dim sConsulta As String
    
    sConsulta = "CREATE TABLE [dbo].[AR_ITEM_PRESCON4] ("
    sConsulta = sConsulta & vbCrLf & "[ANYO] [smallint] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[GMN1] [varchar] (" & gLongitudesDeCodigos.giLongCodGMN1 & ")  NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[PROCE] [smallint] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[ITEM] [int] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[PRES1] [varchar] (15)  NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[PRES2] [varchar] (15)  NULL ,"
    sConsulta = sConsulta & vbCrLf & "[PRES3] [varchar] (15)  NULL ,"
    sConsulta = sConsulta & vbCrLf & "[PRES4] [varchar] (15)  NULL ,"
    sConsulta = sConsulta & vbCrLf & "[DENPRES1] [varchar] (100)  NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[DENPRES2] [varchar] (100)  NULL ,"
    sConsulta = sConsulta & vbCrLf & "[DENPRES3] [varchar] (100)  NULL ,"
    sConsulta = sConsulta & vbCrLf & "[DENPRES4] [varchar] (100)  NULL ,"
    sConsulta = sConsulta & vbCrLf & "[DENGMN1] [varchar] (100)  NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[ADJDIR] [tinyint] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[ART] [varchar] (" & gLongitudesDeCodigos.giLongCodART & ")  NULL ,"
    sConsulta = sConsulta & vbCrLf & "[DESCR] [varchar] (100)  NULL ,"
    sConsulta = sConsulta & vbCrLf & "[CANT] [float] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[PRES] [float] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[PREC] [float] NOT NULL"
    sConsulta = sConsulta & vbCrLf & ") ON [PRIMARY]"

    sConsulta = sConsulta & vbCrLf & "ALTER TABLE [dbo].[AR_ITEM_PRESCON4] WITH NOCHECK ADD"
    sConsulta = sConsulta & vbCrLf & "CONSTRAINT [PK_AR_ITEM_PRESCON4] PRIMARY KEY  CLUSTERED"
    sConsulta = sConsulta & vbCrLf & "("
    sConsulta = sConsulta & vbCrLf & "[ANYO],"
    sConsulta = sConsulta & vbCrLf & "[GMN1],"
    sConsulta = sConsulta & vbCrLf & "[PROCE],"
    sConsulta = sConsulta & vbCrLf & "[ITEM],"
    sConsulta = sConsulta & vbCrLf & "[PRES1]"
    sConsulta = sConsulta & vbCrLf & ")  ON [PRIMARY]"
    
    CrearTablaAR_ITEM_PRESCON4 = sConsulta
End Function

Private Function CrearSP_ANYA_PROVE_2_7_50() As String
    Dim sConsulta As String
    
    sConsulta = "CREATE PROCEDURE SP_ANYA_PROVE (@COD VARCHAR(100),@DEN VARCHAR(100),@NIF VARCHAR(100),@PAI VARCHAR(50),@PROVI VARCHAR(50),@CP VARCHAR(50),@POB VARCHAR(100),@DIR VARCHAR(255),@MON VARCHAR(50),@FSP_COD VARCHAR(100),@PREM  INT,@URLPROVE VARCHAR(255))  AS"
    sConsulta = sConsulta & vbCrLf & "DECLARE @PAIG VARCHAR(50)"
    sConsulta = sConsulta & vbCrLf & "DECLARE @PROVIG VARCHAR(50)"
    sConsulta = sConsulta & vbCrLf & "DECLARE @MONG VARCHAR(50)"
    sConsulta = sConsulta & vbCrLf & "BEGIN"
    sConsulta = sConsulta & vbCrLf & "SET @PAIG=(SELECT TOP 1 COD FROM PAI WHERE FSP_COD=@PAI)"
    sConsulta = sConsulta & vbCrLf & "SET @PROVIG=(SELECT TOP 1 COD FROM PROVI WHERE FSP_PAI=@PAI AND FSP_COD=@PROVI)"
    sConsulta = sConsulta & vbCrLf & "SET @MONG=(SELECT TOP 1 COD FROM MON WHERE FSP_COD=@MON)"
    sConsulta = sConsulta & vbCrLf & "IF @PREM = 2"
    sConsulta = sConsulta & vbCrLf & "SET @PREM=1"
    sConsulta = sConsulta & vbCrLf & "Else"
    sConsulta = sConsulta & vbCrLf & "SET @PREM=0"
    sConsulta = sConsulta & vbCrLf & "IF ((SELECT COUNT(*) FROM PROVE WHERE COD=@COD)>0)"
    sConsulta = sConsulta & vbCrLf & "UPDATE PROVE SET DEN=@DEN,NIF=@NIF,PAI=@PAIG,PROVI=@PROVIG,CP=@CP,POB=@POB,DIR=@DIR,MON=@MONG,FSP_COD=@FSP_COD,PREMIUM=@PREM,ACTIVO=@PREM,URLPROVE=@URLPROVE WHERE COD=@COD"
    sConsulta = sConsulta & vbCrLf & "Else"
    sConsulta = sConsulta & vbCrLf & "INSERT INTO PROVE (COD,DEN,NIF,PAI,PROVI,CP,POB,DIR,MON,FSP_COD,PREMIUM,ACTIVO,URLPROVE) VALUES"
    sConsulta = sConsulta & vbCrLf & "(@COD,@DEN,@NIF,@PAIG,@PROVIG,@CP,@POB,@DIR,@MONG,@FSP_COD,@PREM,@PREM,@URLPROVE)"
    sConsulta = sConsulta & vbCrLf & "Return"
    sConsulta = sConsulta & vbCrLf & "End"
    
    CrearSP_ANYA_PROVE_2_7_50 = sConsulta
End Function

Private Function CrearSP_DEVOLVER_PROVE_2_7_50() As String
    Dim sConsulta As String

    sConsulta = "CREATE PROCEDURE  SP_DEVOLVER_PROVE (@COD VARCHAR(50))  AS"
    sConsulta = sConsulta & vbCrLf & "BEGIN"
    sConsulta = sConsulta & vbCrLf & "SELECT PROVE.COD,PROVE.DEN,PROVE.DIR,PROVE.CP,PROVE.POB,PAI.COD AS PAICOD"
    sConsulta = sConsulta & vbCrLf & ",PAI.DEN AS PAIDEN,PROVI.COD AS PROVICOD,PROVI.DEN AS PROVIDEN"
    sConsulta = sConsulta & vbCrLf & ",MON.COD AS MONCOD,MON.DEN AS MONDEN,PROVE.OBS,PROVE.NIF,PROVE.URLPROVE"
    sConsulta = sConsulta & vbCrLf & "From PROVE"
    sConsulta = sConsulta & vbCrLf & "LEFT JOIN PAI ON PROVE.PAI=PAI.COD"
    sConsulta = sConsulta & vbCrLf & "LEFT JOIN PROVI ON PROVE.PAI = PROVI.PAI"
    sConsulta = sConsulta & vbCrLf & "AND PROVE.PROVI=PROVI.COD"
    sConsulta = sConsulta & vbCrLf & "LEFT JOIN MON ON PROVE.MON=MON.COD"
    sConsulta = sConsulta & vbCrLf & "WHERE PROVE.COD =@COD"
    sConsulta = sConsulta & vbCrLf & "End"

    CrearSP_DEVOLVER_PROVE_2_7_50 = sConsulta
End Function

Private Function CrearTablaGMN4_ACT() As String
    Dim sConsulta As String
    
    sConsulta = "CREATE TABLE [dbo].[GMN4_ACT] ("
    sConsulta = sConsulta & vbCrLf & "[GMN1] [varchar] (" & gLongitudesDeCodigos.giLongCodGMN1 & ")  NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[GMN2] [varchar] (" & gLongitudesDeCodigos.giLongCodGMN2 & ")  NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[GMN3] [varchar] (" & gLongitudesDeCodigos.giLongCodGMN3 & ")  NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[GMN4] [varchar] (" & gLongitudesDeCodigos.giLongCodGMN4 & ")  NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[ACT1] [int] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[ACT2] [int] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[ACT3] [int] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[ACT4] [int] NOT NULL ,"
    sConsulta = sConsulta & vbCrLf & "[ACT5] [int] NULL ,"
    sConsulta = sConsulta & vbCrLf & "[DEN] [varchar] (100)  NULL ,"
    sConsulta = sConsulta & vbCrLf & "[FECACT] [datetime] NULL"
    sConsulta = sConsulta & vbCrLf & ") ON [PRIMARY]"

    sConsulta = sConsulta & vbCrLf & "ALTER TABLE [dbo].[GMN4_ACT] WITH NOCHECK ADD"
    sConsulta = sConsulta & vbCrLf & "CONSTRAINT [PK_GMN4_ACT] PRIMARY KEY  CLUSTERED"
    sConsulta = sConsulta & vbCrLf & "("
    sConsulta = sConsulta & vbCrLf & "[GMN1],"
    sConsulta = sConsulta & vbCrLf & "[GMN2],"
    sConsulta = sConsulta & vbCrLf & "[GMN3],"
    sConsulta = sConsulta & vbCrLf & "[GMN4]"
    sConsulta = sConsulta & vbCrLf & ")  ON [PRIMARY]"

    sConsulta = sConsulta & vbCrLf & "ALTER TABLE [dbo].[GMN4_ACT] ADD"
    sConsulta = sConsulta & vbCrLf & "CONSTRAINT [FK_GMN4_ACT_GMN4] FOREIGN KEY"
    sConsulta = sConsulta & vbCrLf & "("
    sConsulta = sConsulta & vbCrLf & "[GMN1],"
    sConsulta = sConsulta & vbCrLf & "[GMN2],"
    sConsulta = sConsulta & vbCrLf & "[GMN3],"
    sConsulta = sConsulta & vbCrLf & "[GMN4]"
    sConsulta = sConsulta & vbCrLf & ") REFERENCES [dbo].[GMN4] ("
    sConsulta = sConsulta & vbCrLf & "[GMN1],"
    sConsulta = sConsulta & vbCrLf & "[GMN2],"
    sConsulta = sConsulta & vbCrLf & "[GMN3],"
    sConsulta = sConsulta & vbCrLf & "[COD]"
    sConsulta = sConsulta & vbCrLf & ")"
    
    CrearTablaGMN4_ACT = sConsulta
End Function


Private Function CrearTriggerGMN4_ACT_TG_INSUPD() As String
    Dim sConsulta As String
    
    sConsulta = "CREATE TRIGGER [GMN4_ACT_TG_INSUPD] ON dbo.GMN4_ACT "
    sConsulta = sConsulta & vbCrLf & "FOR INSERT, UPDATE, DELETE"
    sConsulta = sConsulta & vbCrLf & "AS"
    sConsulta = sConsulta & vbCrLf & "DECLARE @GMN1 VARCHAR(50),@GMN2 VARCHAR(50),@GMN3 VARCHAR(50),@GMN4 VARCHAR(50)"
    sConsulta = sConsulta & vbCrLf & "DECLARE curTG_GMN4_ACT_Ins CURSOR FOR SELECT GMN1,GMN2,GMN3,GMN4 FROM INSERTED"
    sConsulta = sConsulta & vbCrLf & "OPEN curTG_GMN4_ACT_Ins"
    sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM curTG_GMN4_ACT_Ins INTO @GMN1,@GMN2,@GMN3,@GMN4"
    sConsulta = sConsulta & vbCrLf & "WHILE @@FETCH_STATUS=0"
    sConsulta = sConsulta & vbCrLf & "BEGIN"
    sConsulta = sConsulta & vbCrLf & "UPDATE GMN4_ACT SET FECACT=GETDATE() WHERE GMN1=@GMN1  AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@GMN4"
    sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM curTG_GMN4_ACT_Ins INTO @GMN1,@GMN2,@GMN3,@GMN4"
    sConsulta = sConsulta & vbCrLf & "End"
    sConsulta = sConsulta & vbCrLf & "Close curTG_GMN4_ACT_Ins"
    sConsulta = sConsulta & vbCrLf & "DEALLOCATE curTG_GMN4_ACT_Ins"
    
    CrearTriggerGMN4_ACT_TG_INSUPD = sConsulta
End Function

Private Function ModificarTriggerITEM_TG_UPD_2_7_50() As String
    Dim sConsulta As String
    
    sConsulta = "CREATE TRIGGER ITEM_TG_UPD ON dbo.ITEM"
    sConsulta = sConsulta & vbCrLf & "FOR UPDATE"
    sConsulta = sConsulta & vbCrLf & "AS"
    sConsulta = sConsulta & vbCrLf & "DECLARE @ANYO AS INTEGER"
    sConsulta = sConsulta & vbCrLf & "DECLARE @GMN1 AS VARCHAR(50)"
    sConsulta = sConsulta & vbCrLf & "DECLARE @PROCE AS INTEGER"
    sConsulta = sConsulta & vbCrLf & "DECLARE @ID AS INTEGER"
    sConsulta = sConsulta & vbCrLf & "DECLARE @GMN2 AS VARCHAR(50)"
    sConsulta = sConsulta & vbCrLf & "DECLARE @GMN3 AS VARCHAR(50)"
    sConsulta = sConsulta & vbCrLf & "DECLARE @GMN4 AS VARCHAR(50)"
    sConsulta = sConsulta & vbCrLf & "DECLARE @ART AS VARCHAR(50)"
    sConsulta = sConsulta & vbCrLf & "DECLARE @CANT AS FLOAT"
    sConsulta = sConsulta & vbCrLf & "DECLARE @FECINI AS DATETIME"
    sConsulta = sConsulta & vbCrLf & "DECLARE @EST AS INTEGER"
    sConsulta = sConsulta & vbCrLf & "DECLARE @OBJ AS FLOAT"
    sConsulta = sConsulta & vbCrLf & "DECLARE @OBJOLD AS FLOAT"
    sConsulta = sConsulta & vbCrLf & "DECLARE @FECNECMANUAL  AS TINYINT"
    sConsulta = sConsulta & vbCrLf & ""
    sConsulta = sConsulta & vbCrLf & "DECLARE curTG_ITEM_Upd CURSOR FOR SELECT ANYO,GMN1,PROCE,ID,GMN2,GMN3,GMN4,ART,CANT,FECINI,OBJ FROM INSERTED"
    sConsulta = sConsulta & vbCrLf & "OPEN curTG_ITEM_Upd"
    sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM curTG_ITEM_Upd INTO @ANYO,@GMN1,@PROCE,@ID,@GMN2,@GMN3,@GMN4,@ART,@CANT,@FECINI,@OBJ"
    sConsulta = sConsulta & vbCrLf & "WHILE @@FETCH_STATUS=0"
    sConsulta = sConsulta & vbCrLf & "BEGIN"
    sConsulta = sConsulta & vbCrLf & "SET @EST = (SELECT EST FROM PROCE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE)"
    sConsulta = sConsulta & vbCrLf & "SET @OBJOLD = (SELECT OBJ FROM DELETED WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ID=@ID)"
    sConsulta = sConsulta & vbCrLf & "SET  @FECNECMANUAL=(SELECT PROCE.FECNECMANUAL FROM PROCE  WHERE ANYO=@ANYO  AND GMN1=@GMN1 AND COD=@PROCE)"
    sConsulta = sConsulta & vbCrLf & ""
    sConsulta = sConsulta & vbCrLf & "IF @FECNECMANUAL=0"
    sConsulta = sConsulta & vbCrLf & "UPDATE PROCE SET FECNEC =(SELECT MIN(FECINI) FROM ITEM WHERE ANYO=@ANYO  AND GMN1=@GMN1 AND PROCE=@PROCE)"
    sConsulta = sConsulta & vbCrLf & "WHERE COD=@PROCE"
    sConsulta = sConsulta & vbCrLf & ""
    sConsulta = sConsulta & vbCrLf & "IF @OBJ IS NULL"
    sConsulta = sConsulta & vbCrLf & "BEGIN"
    sConsulta = sConsulta & vbCrLf & "/*Si estamos en estado 8 y borramos el �ltimoobjetivo entonces pasamos a estado 7*/"
    sConsulta = sConsulta & vbCrLf & "IF @EST = 8 AND ((SELECT COUNT(*) FROM ITEM WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND OBJ IS NOT NULL )=0) UPDATE PROCE SET EST=7 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE"
    sConsulta = sConsulta & vbCrLf & "IF @EST = 9  AND ((SELECT COUNT(*) FROM ITEM WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND OBJ IS NOT NULL )=0) UPDATE PROCE SET EST=10 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE"
    sConsulta = sConsulta & vbCrLf & "IF @OBJOLD IS NOT NULL AND ((SELECT COUNT(*) FROM ITEM WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND OBJ IS NOT NULL )=0) UPDATE PROCE_PROVE SET NUMOBJ=0,OBJNUEVOS=0 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE"
    sConsulta = sConsulta & vbCrLf & "End"
    sConsulta = sConsulta & vbCrLf & "Else"
    sConsulta = sConsulta & vbCrLf & "IF @OBJOLD IS NULL"
    sConsulta = sConsulta & vbCrLf & "BEGIN"
    sConsulta = sConsulta & vbCrLf & "IF @EST = 7 UPDATE PROCE SET EST=8 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE"
    sConsulta = sConsulta & vbCrLf & "IF @EST = 10 UPDATE PROCE SET EST=9 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE"
    sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_PROVE SET OBJNUEVOS=1,NUMOBJ=NUMOBJ+1 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE"
    sConsulta = sConsulta & vbCrLf & "End"
    sConsulta = sConsulta & vbCrLf & "Else"
    sConsulta = sConsulta & vbCrLf & "IF @OBJOLD <> @OBJ"
    sConsulta = sConsulta & vbCrLf & "BEGIN"
    sConsulta = sConsulta & vbCrLf & "IF @EST = 7 UPDATE PROCE SET EST=8 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE"
    sConsulta = sConsulta & vbCrLf & "IF @EST = 10 UPDATE PROCE SET EST=9 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE"
    sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_PROVE SET OBJNUEVOS=1,NUMOBJ=NUMOBJ+1 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE"
    sConsulta = sConsulta & vbCrLf & "End"
    sConsulta = sConsulta & vbCrLf & ""
    sConsulta = sConsulta & vbCrLf & "_FETCH:"
    sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM curTG_ITEM_Upd INTO @ANYO,@GMN1,@PROCE,@ID,@GMN2,@GMN3,@GMN4,@ART,@CANT,@FECINI,@OBJ"
    sConsulta = sConsulta & vbCrLf & "End"
    sConsulta = sConsulta & vbCrLf & "Close curTG_ITEM_Upd"
    sConsulta = sConsulta & vbCrLf & "DEALLOCATE curTG_ITEM_Upd"
    
    ModificarTriggerITEM_TG_UPD_2_7_50 = sConsulta
End Function

Private Function ModificarNUM_PROCE_PUB_2_7_50() As String
    Dim sConsulta As String
    
    sConsulta = "CREATE PROCEDURE  NUM_PROCE_PUB (@PROVE VARCHAR(50),@NUM INT OUTPUT,@NUMNUE INT OUTPUT,@NUMAREV INT OUTPUT)  AS"
    sConsulta = sConsulta & vbCrLf & "BEGIN"
    sConsulta = sConsulta & vbCrLf & ""
    sConsulta = sConsulta & vbCrLf & "SET @NUM = (SELECT COUNT(*) FROM PROCE_PROVE INNER JOIN PROCE ON PROCE_PROVE.ANYO=PROCE.ANYO"
    sConsulta = sConsulta & vbCrLf & " AND PROCE_PROVE.GMN1=PROCE.GMN1 AND PROCE_PROVE.PROCE=PROCE.COD AND PROCE_PROVE.PROVE=@PROVE"
    sConsulta = sConsulta & vbCrLf & " AND PROCE_PROVE.PUB=1 AND PROCE.EST<=10 )"
    sConsulta = sConsulta & vbCrLf & ""
    sConsulta = sConsulta & vbCrLf & ""
    sConsulta = sConsulta & vbCrLf & "SET @NUMNUE = (SELECT COUNT(*) FROM PROCE_PROVE INNER JOIN PROCE ON PROCE_PROVE.ANYO=PROCE.ANYO"
    sConsulta = sConsulta & vbCrLf & " AND PROCE_PROVE.GMN1=PROCE.GMN1 AND PROCE_PROVE.PROCE=PROCE.COD AND PROCE_PROVE.PROVE=@PROVE"
    sConsulta = sConsulta & vbCrLf & " AND PROCE_PROVE.PUB=1 AND PROCE.EST>=5 AND PROCE.EST<=6)"
    sConsulta = sConsulta & vbCrLf & ""
    sConsulta = sConsulta & vbCrLf & ""
    sConsulta = sConsulta & vbCrLf & "SET @NUMAREV = (SELECT COUNT(*)  FROM PROCE_PROVE INNER JOIN PROCE ON PROCE_PROVE.ANYO=PROCE.ANYO"
    sConsulta = sConsulta & vbCrLf & "AND PROCE_PROVE.GMN1=PROCE.GMN1 AND PROCE_PROVE.PROCE=PROCE.COD AND PROCE_PROVE.PROVE=@PROVE"
    sConsulta = sConsulta & vbCrLf & "AND PROCE_PROVE.PUB=1 AND PROCE_PROVE.OBJNUEVOS=1 AND PROCE.SUBASTA=0)"
    sConsulta = sConsulta & vbCrLf & ""
    sConsulta = sConsulta & vbCrLf & "Return"
    sConsulta = sConsulta & vbCrLf & "End"
    
    ModificarNUM_PROCE_PUB_2_7_50 = sConsulta
End Function

Private Function ModificarSP_DEVOLVER_PROCESOS_PROVE_WEB_2_7_50() As String
Dim sConsulta As String

sConsulta = "CREATE PROCEDURE SP_DEVOLVER_PROCESOS_PROVE_WEB (@PROVE VARCHAR(50),@NORMAL SMALLINT OUTPUT ,@SUBASTA SMALLINT OUTPUT) AS"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "SET @NORMAL=(SELECT COUNT(*) FROM PROCE INNER JOIN PROCE_PROVE ON PROCE.ANYO= PROCE_PROVE.ANYO AND"
sConsulta = sConsulta & vbCrLf & "PROCE.GMN1=PROCE_PROVE.GMN1 AND PROCE.COD=PROCE_PROVE.PROCE AND PROCE_PROVE.PROVE=@PROVE"
sConsulta = sConsulta & vbCrLf & "AND PROCE_PROVE.PUB=1 AND PROCE.EST<11 AND SUBASTA=0)"
sConsulta = sConsulta & vbCrLf & "SET @SUBASTA=(SELECT COUNT(*) FROM PROCE INNER JOIN PROCE_PROVE ON PROCE.ANYO= PROCE_PROVE.ANYO AND"
sConsulta = sConsulta & vbCrLf & "PROCE.GMN1=PROCE_PROVE.GMN1 AND PROCE.COD=PROCE_PROVE.PROCE AND PROCE_PROVE.PROVE=@PROVE"
sConsulta = sConsulta & vbCrLf & "AND PROCE_PROVE.PUB=1 AND PROCE.EST<11 AND SUBASTA=1)"
sConsulta = sConsulta & vbCrLf & "/*Devuelve en 1� lugar los procesos normales y luego los abiertos en modo subasta*/"
sConsulta = sConsulta & vbCrLf & "SELECT PROCE.ANYO,PROCE.GMN1,PROCE.COD,PROCE.DEN,PROCE.FECLIMOFE,PROCE_PROVE.OFE,PROCE.EST,PROCE_PROVE.SUPERADA,"
sConsulta = sConsulta & vbCrLf & "Proce.SUBASTA , Proce.SUBASTAPROVE, Proce.SUBASTAPUJAS, Proce.MON, PROCE_PROVE.NUMOBJ, PROCE_PROVE.OBJNUEVOS"
sConsulta = sConsulta & vbCrLf & "FROM PROCE INNER JOIN PROCE_PROVE ON PROCE.ANYO= PROCE_PROVE.ANYO AND"
sConsulta = sConsulta & vbCrLf & "PROCE.GMN1=PROCE_PROVE.GMN1 AND PROCE.COD=PROCE_PROVE.PROCE AND PROCE_PROVE.PROVE=@PROVE"
sConsulta = sConsulta & vbCrLf & "AND PROCE_PROVE.PUB=1 AND PROCE.EST<11  ORDER BY SUBASTA"
sConsulta = sConsulta & vbCrLf & "End"

ModificarSP_DEVOLVER_PROCESOS_PROVE_WEB_2_7_50 = sConsulta
End Function

Private Function CrearSP_MODIF_OBJ_PROVE_WEB() As String
Dim sConsulta As String

sConsulta = "CREATE PROCEDURE SP_MODIF_OBJ_PROVE_WEB (@ANYO SMALLINT,@GMN1 VARCHAR(50),@PROCE INTEGER ,"
sConsulta = sConsulta & vbCrLf & "@PROVE VARCHAR(50),@NUMOBJ INTEGER,@NUM INTEGER OUTPUT,@NUMNUE INTEGER OUTPUT,@NUMAREV INTEGER OUTPUT) AS"
sConsulta = sConsulta & vbCrLf & ""
sConsulta = sConsulta & vbCrLf & "DECLARE @NUMOBJ2 AS INTEGER"
sConsulta = sConsulta & vbCrLf & "DECLARE @INST AS INTEGER"
sConsulta = sConsulta & vbCrLf & ""
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & ""
sConsulta = sConsulta & vbCrLf & "SET @NUMOBJ2 = (SELECT NUMOBJ FROM PROCE_PROVE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE)"
sConsulta = sConsulta & vbCrLf & ""
sConsulta = sConsulta & vbCrLf & "IF @NUMOBJ2 <=@NUMOBJ"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_PROVE SET OBJNUEVOS=0 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE"
sConsulta = sConsulta & vbCrLf & "END"
sConsulta = sConsulta & vbCrLf & ""
sConsulta = sConsulta & vbCrLf & "SET @INST = (SELECT INSTWEB FROM PARGEN_INTERNO)"
sConsulta = sConsulta & vbCrLf & "IF @INST=2"
sConsulta = sConsulta & vbCrLf & "/*Ejecuta el store procedure que devuelve el n�mero de procesos nuevos, a revisar,etc...*/"
sConsulta = sConsulta & vbCrLf & "EXECUTE NUM_PROCE_PUB @PROVE,@NUM OUTPUT,@NUMNUE OUTPUT,@NUMAREV OUTPUT"
sConsulta = sConsulta & vbCrLf & ""
sConsulta = sConsulta & vbCrLf & "RETURN"
sConsulta = sConsulta & vbCrLf & "END"
 
CrearSP_MODIF_OBJ_PROVE_WEB = sConsulta
End Function


Private Function CorregirTriggerPROCE_TG_UPD() As String
    Dim sConsulta As String
    
    sConsulta = "CREATE TRIGGER PROCE_TG_UPD ON dbo.PROCE"
    sConsulta = sConsulta & vbCrLf & "FOR UPDATE"
    sConsulta = sConsulta & vbCrLf & "AS"
    sConsulta = sConsulta & vbCrLf & "DECLARE @ANYO INT,@GMN1 VARCHAR(50),@COD SMALLINT"
    sConsulta = sConsulta & vbCrLf & "DECLARE @ITEM INT"
    sConsulta = sConsulta & vbCrLf & "DECLARE @SUBASTA AS TINYINT"
    sConsulta = sConsulta & vbCrLf & "DECLARE @OLDSUBASTA AS TINYINT"
    sConsulta = sConsulta & vbCrLf & ""
    sConsulta = sConsulta & vbCrLf & ""
    sConsulta = sConsulta & vbCrLf & ""
    sConsulta = sConsulta & vbCrLf & "DECLARE curTG_PROCE_upd CURSOR FOR SELECT ANYO,GMN1,COD,SUBASTA FROM INSERTED"
    sConsulta = sConsulta & vbCrLf & "OPEN curTG_PROCE_upd"
    sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM curTG_PROCE_upd INTO @ANYO,@GMN1,@COD,@SUBASTA"
    sConsulta = sConsulta & vbCrLf & "WHILE @@FETCH_STATUS=0"
    sConsulta = sConsulta & vbCrLf & "BEGIN"
    sConsulta = sConsulta & vbCrLf & "  SELECT @OLDSUBASTA = SUBASTA FROM DELETED WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@COD"
    sConsulta = sConsulta & vbCrLf & "  IF @SUBASTA!=@OLDSUBASTA"
    sConsulta = sConsulta & vbCrLf & "      BEGIN"
    sConsulta = sConsulta & vbCrLf & "          DELETE FROM OFEMIN WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@COD"
    sConsulta = sConsulta & vbCrLf & "          IF @SUBASTA=1"
    sConsulta = sConsulta & vbCrLf & "              BEGIN"
    sConsulta = sConsulta & vbCrLf & "                  DECLARE curTG_ITEM CURSOR FOR SELECT ID FROM ITEM WHERE  ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@COD"
    sConsulta = sConsulta & vbCrLf & "                  OPEN curTG_ITEM"
    sConsulta = sConsulta & vbCrLf & "                  FETCH NEXT FROM curTG_ITEM INTO @ITEM"
    sConsulta = sConsulta & vbCrLf & "                  WHILE @@FETCH_STATUS=0"
    sConsulta = sConsulta & vbCrLf & "                      BEGIN"
    sConsulta = sConsulta & vbCrLf & "                          INSERT INTO OFEMIN (ANYO,GMN1,PROCE,ITEM,PROVE,PRECIO) VALUES (@ANYO,@GMN1,@COD, @ITEM,NULL,NULL)"
    sConsulta = sConsulta & vbCrLf & "                          FETCH NEXT FROM curTG_ITEM INTO @ITEM"
    sConsulta = sConsulta & vbCrLf & "                      End"
    sConsulta = sConsulta & vbCrLf & "                  Close curTG_ITEM"
    sConsulta = sConsulta & vbCrLf & "                  DEALLOCATE curTG_ITEM"
    sConsulta = sConsulta & vbCrLf & "              End"
    sConsulta = sConsulta & vbCrLf & "      END"
    sConsulta = sConsulta & vbCrLf & "  FETCH NEXT FROM curTG_PROCE_upd INTO @ANYO,@GMN1,@COD,@SUBASTA"
    sConsulta = sConsulta & vbCrLf & "End"
    sConsulta = sConsulta & vbCrLf & "Close curTG_PROCE_upd"
    sConsulta = sConsulta & vbCrLf & "DEALLOCATE curTG_PROCE_upd"
    
    CorregirTriggerPROCE_TG_UPD = sConsulta
End Function

Private Function ModificarSP_ANYA_PROVE_CON_2_7_50() As String
    Dim sConsulta As String
    
    sConsulta = "CREATE PROCEDURE SP_ANYA_PROVE_CON (@PROVE VARCHAR(100),@APE VARCHAR(100),@NOM VARCHAR(100),@DEP VARCHAR(100),@CAR VARCHAR(100),@TFNO VARCHAR(50),@TFNO2 VARCHAR(50),@FAX VARCHAR(50),@EMAIL VARCHAR(100),@NUM INT,@TFNO_MOVIL VARCHAR(50))  AS"
    sConsulta = sConsulta & vbCrLf & "DECLARE @IND INT"
    sConsulta = sConsulta & vbCrLf & "BEGIN"
    sConsulta = sConsulta & vbCrLf & "IF @NUM = 1"
    sConsulta = sConsulta & vbCrLf & "BEGIN"
    sConsulta = sConsulta & vbCrLf & "IF ((SELECT COUNT(*)  FROM CON WHERE PROVE=@PROVE AND PORT=1) > 0)"
    sConsulta = sConsulta & vbCrLf & "DELETE FROM CON WHERE PROVE=@PROVE AND PORT=1"
    sConsulta = sConsulta & vbCrLf & "End"
    sConsulta = sConsulta & vbCrLf & "SET @IND=(SELECT ISNULL(MAX(ID),0) FROM CON WHERE PROVE=@PROVE)"
    sConsulta = sConsulta & vbCrLf & "SET @IND=@IND+1"
    sConsulta = sConsulta & vbCrLf & "INSERT INTO CON (PROVE,ID,APE,NOM,DEP,CAR,TFNO,FAX,EMAIL,RECPET,TFNO2,PORT,TFNO_MOVIL) VALUES"
    sConsulta = sConsulta & vbCrLf & "(@PROVE,@IND,@APE,@NOM,@DEP,@CAR,@TFNO,@FAX,@EMAIL,1,@TFNO2,1,@TFNO_MOVIL)"
    sConsulta = sConsulta & vbCrLf & "Return"
    sConsulta = sConsulta & vbCrLf & "End"
    
    ModificarSP_ANYA_PROVE_CON_2_7_50 = sConsulta

End Function

Private Function CrearTriggerITEM_OFE_TG_DEL() As String
Dim sConsulta As String

sConsulta = "CREATE TRIGGER ITEM_OFE_TG_DEL ON [dbo].[ITEM_OFE] "
sConsulta = sConsulta & vbLf & "FOR DELETE"
sConsulta = sConsulta & vbLf & "AS"
sConsulta = sConsulta & vbLf & "DECLARE @SUPERADA AS TINYINT"
sConsulta = sConsulta & vbLf & "DELETE DETALLE_PUJAS  FROM DETALLE_PUJAS A INNER JOIN DELETED B"
sConsulta = sConsulta & vbLf & "ON A.ANYO=B.ANYO AND A.GMN1= B.GMN1 AND A.PROCE=B.PROCE AND A.ITEM=B.ITEM"
sConsulta = sConsulta & vbLf & "/*Si se hab�a superado la oferta para ese proveedor*/"
sConsulta = sConsulta & vbLf & "SELECT @SUPERADA =SUPERADA FROM PROCE_PROVE A INNER JOIN DELETED B ON A.ANYO=B.ANYO AND A.GMN1=B.GMN1 AND A.PROCE=B.PROCE AND A.PROVE=B.PROVE"
sConsulta = sConsulta & vbLf & "IF @SUPERADA=1"
sConsulta = sConsulta & vbLf & "UPDATE PROCE_PROVE SET SUPERADA = 0"
sConsulta = sConsulta & vbLf & "FROM PROCE_PROVE A  INNER JOIN DELETED C ON A.ANYO= C.ANYO AND A.GMN1=C.GMN1 AND A.PROCE=C.PROCE AND A.PROVE=C.PROVE"
sConsulta = sConsulta & vbLf & "AND NOT EXISTS (SELECT * FROM OFEMIN B WHERE A.ANYO=B.ANYO AND A.GMN1=B.GMN1 AND A.PROCE=B.PROCE AND A.PROVE=B.PROVE)"

CrearTriggerITEM_OFE_TG_DEL = sConsulta
End Function

Private Function CrearTablaWEBTEXTHELP() As String
Dim sConsulta As String

sConsulta = "CREATE TABLE [dbo].[WEBTEXTHELP] ("
sConsulta = sConsulta & vbLf & "[MODULO] [int] NOT NULL ,"
sConsulta = sConsulta & vbLf & "[ID] [int] NOT NULL ,"
sConsulta = sConsulta & vbLf & "[SPA] [text] NULL ,"
sConsulta = sConsulta & vbLf & "[ENG] [text] NULL"
sConsulta = sConsulta & vbLf & ") ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]"
sConsulta = sConsulta & vbLf & "ALTER TABLE [dbo].[WEBTEXTHELP] WITH NOCHECK ADD"
sConsulta = sConsulta & vbLf & "CONSTRAINT [PK_WEBTEXTHELP] PRIMARY KEY  CLUSTERED"
sConsulta = sConsulta & vbLf & "("
sConsulta = sConsulta & vbLf & "[MODULO],"
sConsulta = sConsulta & vbLf & "[ID]"
sConsulta = sConsulta & vbLf & ")  ON [PRIMARY]"

CrearTablaWEBTEXTHELP = sConsulta
End Function


Private Function ModificarITEM_OFE_TG_UPD_2_7_50_5() As String
Dim sConsulta As String

sConsulta = "CREATE TRIGGER ITEM_OFE_TG_UPD ON [ITEM_OFE]"
sConsulta = sConsulta & vbLf & "FOR UPDATE "
sConsulta = sConsulta & vbLf & "AS "
sConsulta = sConsulta & vbLf & "DECLARE @ANYO INT,@GMN1 VARCHAR(50),@PROCE INT, @ITEM INT,@PROVE VARCHAR(50), @NUM INT,@ULT INT "
sConsulta = sConsulta & vbLf & "DECLARE @PRECIO AS FLOAT "
sConsulta = sConsulta & vbLf & "DECLARE @PROVE2 AS VARCHAR(50) "
sConsulta = sConsulta & vbLf & "DECLARE @CONEX  AS VARCHAR(100),@FSP_CIA AS VARCHAR(50) "
sConsulta = sConsulta & vbLf & "DECLARE @BD AS VARCHAR(100),@SERVER AS VARCHAR(100) "
sConsulta = sConsulta & vbLf & "DECLARE @EMAIL AS VARCHAR(100) ,@PROVEDEN AS VARCHAR(500) , @CONTACTO AS VARCHAR(500) "
sConsulta = sConsulta & vbLf & "DECLARE @PRECCOMP AS FLOAT,@PROVECOMP AS VARCHAR(50) "
sConsulta = sConsulta & vbLf & "DECLARE @OFE2 AS SMALLINT "
sConsulta = sConsulta & vbLf & "DECLARE @IDENT AS INTEGER "
sConsulta = sConsulta & vbLf & "DECLARE @OFEMIN AS INTEGER "
sConsulta = sConsulta & vbLf & "DECLARE @PRIMERO AS INTEGER "
sConsulta = sConsulta & vbLf & "DECLARE @PROCEDEN VARCHAR(500),@ITEMDEN VARCHAR(500) "
sConsulta = sConsulta & vbLf & "DECLARE @CAMBIOMONCEN FLOAT "
sConsulta = sConsulta & vbLf & "DECLARE @INST INTEGER "
sConsulta = sConsulta & vbLf & "DECLARE @EMAIL_EX AS VARCHAR(100),@CONTACTO_EX AS VARCHAR(500) "
sConsulta = sConsulta & vbLf & "DECLARE curTG_ITEM_OFE_Upd CURSOR FOR SELECT ANYO,GMN1,PROCE,ITEM,PROVE,NUM,ULT FROM INSERTED "
sConsulta = sConsulta & vbLf & "OPEN curTG_ITEM_OFE_Upd "
sConsulta = sConsulta & vbLf & "FETCH NEXT FROM curTG_ITEM_OFE_Upd INTO @ANYO,@GMN1,@PROCE,@ITEM,@PROVE,@NUM,@ULT "
sConsulta = sConsulta & vbLf & "WHILE @@FETCH_STATUS=0 "
sConsulta = sConsulta & vbLf & "BEGIN "
sConsulta = sConsulta & vbLf & " IF (SELECT SUBASTA FROM PROCE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE) = 1 "
sConsulta = sConsulta & vbLf & " BEGIN "
sConsulta = sConsulta & vbLf & "     IF UPDATE(PRECIO) "
sConsulta = sConsulta & vbLf & "     BEGIN "
sConsulta = sConsulta & vbLf & "         SELECT @PRIMERO=COUNT(*) FROM OFEMIN WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE AND ITEM=@ITEM AND OFE=@NUM         "
sConsulta = sConsulta & vbLf & ""
sConsulta = sConsulta & vbLf & "         SET @OFEMIN =(SELECT COUNT(*) FROM OFEMIN WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE AND ITEM=@ITEM) "
sConsulta = sConsulta & vbLf & "         IF @ULT=1 OR @OFEMIN>0  OR @PRIMERO>0/*Si el precio se ha modificado en la �ltima oferta recalculamos el precio m�s bajo para ese item o era la m�nima*/ "
sConsulta = sConsulta & vbLf & "         BEGIN "
sConsulta = sConsulta & vbLf & "             SELECT TOP 1 @PRECIO= A.PRECIO / (B.CAMBIO * C.CAMBIO), @PROVE2=A.PROVE,@OFE2=A.NUM "
sConsulta = sConsulta & vbLf & "                   FROM ITEM_OFE A INNER JOIN PROCE_OFE B ON A.ANYO=B.ANYO AND A.GMN1=B.GMN1 AND A.PROCE=B.PROCE AND A.PROVE=B.PROVE AND A.NUM=B.OFE"
sConsulta = sConsulta & vbLf & "                  INNER JOIN PROCE C ON A.ANYO=C.ANYO AND A.GMN1=C.GMN1 AND A.PROCE = C.COD"
sConsulta = sConsulta & vbLf & "             WHERE A.ANYO=@ANYO AND A.GMN1=@GMN1 AND A.PROCE=@PROCE AND A.ITEM=@ITEM AND A.ULT=1 AND A.PRECIO IS NOT NULL"
sConsulta = sConsulta & vbLf & "             ORDER BY A.PRECIO / (B.CAMBIO * C.CAMBIO) ASC "
sConsulta = sConsulta & vbLf & "              /*Si el nuevo precio calculado es m�s bajo y el proveedor es diferente del que antes ten�a el precio m�s bajo se actualiza OFEMIN*/ "
sConsulta = sConsulta & vbLf & "             SELECT @PRECCOMP=PRECIO,@PROVECOMP=PROVE FROM OFEMIN WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM "
sConsulta = sConsulta & vbLf & "             IF @PROVECOMP IS NULL "
sConsulta = sConsulta & vbLf & "             /*Si el precio y prov. es nulo no notifica pero actualiza ofemin*/ "
sConsulta = sConsulta & vbLf & "             BEGIN "
sConsulta = sConsulta & vbLf & "                 UPDATE OFEMIN SET PRECIO=@PRECIO,PROVE=@PROVE2,OFE=@OFE2 , CAMBIO=@CAMBIOMONCEN WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM "
sConsulta = sConsulta & vbLf & "                 if @PRECIO IS NOT NULL "
sConsulta = sConsulta & vbLf & "                     INSERT INTO DETALLE_PUJAS (ANYO,GMN1,PROCE,ITEM,ID,OFE,PROVE,FECHA,PRECIO) VALUES (@ANYO,@GMN1,@PROCE,@ITEM,1,@OFE2,@PROVE2,GETDATE(),@PRECIO) "
sConsulta = sConsulta & vbLf & "             End "
sConsulta = sConsulta & vbLf & "             ELSE IF  @PRECIO <= @PRECCOMP OR @OFEMIN>0 OR @PRIMERO>0"
sConsulta = sConsulta & vbLf & "             BEGIN "
sConsulta = sConsulta & vbLf & "                 IF @PROVECOMP <>@PROVE2 /*Si el proveedor es diferente habr� que notificarle que su oferta ha sido pisada*/ "
sConsulta = sConsulta & vbLf & "                 BEGIN "
sConsulta = sConsulta & vbLf & "                     /*Obtenemos los datos del proveedor (Denominaci�n,persona de contacto y e-mail de la persona de contacto) para notificarle que su oferta "
sConsulta = sConsulta & vbLf & "                     ha sido pisada*/ "
sConsulta = sConsulta & vbLf & "                     UPDATE PROCE_PROVE SET SUPERADA = 1 WHERE ANYO= @ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVECOMP"
sConsulta = sConsulta & vbLf & "                    SET CONCAT_NULL_YIELDS_NULL OFF "
sConsulta = sConsulta & vbLf & "          SELECT TOP 1 @EMAIL=CON.EMAIL,@PROVEDEN=PROVE.DEN,@CONTACTO = NOMCON + ' ' + APECON "
sConsulta = sConsulta & vbLf & "                     FROM PROVE,PROCE_PROVE_PET,CON WHERE PROVE.COD= PROCE_PROVE_PET.PROVE AND PROCE_PROVE_PET.PROVE= CON.PROVE AND "
sConsulta = sConsulta & vbLf & "                     PROCE_PROVE_PET.NOMCON=CON.NOM AND PROCE_PROVE_PET.APECON = CON.APE AND "
sConsulta = sConsulta & vbLf & "                     PROCE_PROVE_PET.TIPOPET=0 AND ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE.COD=@PROVECOMP ORDER BY PROCE_PROVE_PET.ID DESC "
sConsulta = sConsulta & vbLf & "                     SELECT @PROCEDEN=PROCE.DEN,@ITEMDEN=ITEM.DESCR + ART4.DEN FROM PROCE INNER JOIN ITEM ON PROCE.ANYO=ITEM.ANYO AND "
sConsulta = sConsulta & vbLf & "                     Proce.gmn1 = Item.gmn1 And Proce.COD = Item.Proce "
sConsulta = sConsulta & vbLf & "           LEFT JOIN ART4 ON ITEM.ART=ART4.COD AND ITEM.GMN1=ART4.GMN1 AND ITEM.GMN2=ART4.GMN2 AND ITEM.GMN3=ART4.GMN3 AND ITEM.GMN4=ART4.GMN4 "
sConsulta = sConsulta & vbLf & "                     WHERE PROCE.ANYO=@ANYO AND PROCE.GMN1=@GMN1 AND PROCE.COD=@PROCE AND ITEM.ID=@ITEM "
sConsulta = sConsulta & vbLf & "                     SET CONCAT_NULL_YIELDS_NULL ON"
sConsulta = sConsulta & vbLf & "           /*Si la instalaci�n es de BuySite*/ "
sConsulta = sConsulta & vbLf & "                     SELECT @INST=INSTWEB FROM PARGEN_INTERNO "
sConsulta = sConsulta & vbLf & "                     IF @INST = 1 "
sConsulta = sConsulta & vbLf & "                     BEGIN "
sConsulta = sConsulta & vbLf & "                         /*Inserta en la tabla PROVENOTIF*/ "
sConsulta = sConsulta & vbLf & "                         IF @EMAIL IS NULL OR @CONTACTO IS NULL  /*Si el e-mail o el contacto son nulos saca de contactos alguno que no sea nulo*/ "
sConsulta = sConsulta & vbLf & "                         BEGIN "
sConsulta = sConsulta & vbLf & "                             SELECT TOP 1 @EMAIL_EX=CON.EMAIL,@CONTACTO_EX = NOMCON + ' ' + APECON FROM PROCE_PROVE_PET,CON  "
sConsulta = sConsulta & vbLf & "                             WHERE PROCE_PROVE_PET.PROVE= CON.PROVE AND PROCE_PROVE_PET.NOMCON=CON.NOM AND PROCE_PROVE_PET.APECON = CON.APE  "
sConsulta = sConsulta & vbLf & "                             AND PROCE_PROVE_PET.TIPOPET=0 AND ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROCE_PROVE_PET.PROVE=@PROVECOMP  "
sConsulta = sConsulta & vbLf & "                             AND CON.EMAIL IS NOT NULL AND NOMCON IS NOT NULL "
sConsulta = sConsulta & vbLf & "                             INSERT INTO PROVENOTIF (ANYO,GMN1,PROCE,PROCEDEN,ITEM,ITEMDEN,PROVE,PROVEDEN,EMAIL,CONTACTO) VALUES "
sConsulta = sConsulta & vbLf & "                             (@ANYO,@GMN1,@PROCE,@PROCEDEN,@ITEM,@ITEMDEN,@PROVE,@PROVEDEN,@EMAIL_EX,@CONTACTO_EX) "
sConsulta = sConsulta & vbLf & "                         END "
sConsulta = sConsulta & vbLf & "                         ELSE "
sConsulta = sConsulta & vbLf & "                             INSERT INTO PROVENOTIF (ANYO,GMN1,PROCE,PROCEDEN,ITEM,ITEMDEN,PROVE,PROVEDEN,EMAIL,CONTACTO) VALUES "
sConsulta = sConsulta & vbLf & "                             (@ANYO,@GMN1,@PROCE,@PROCEDEN,@ITEM,@ITEMDEN,@PROVE,@PROVEDEN,@EMAIL,@CONTACTO) "
sConsulta = sConsulta & vbLf & "                     END "
sConsulta = sConsulta & vbLf & "                     ELSE /*Si la instalaci�n es de Portal*/ "
sConsulta = sConsulta & vbLf & "                     BEGIN "
sConsulta = sConsulta & vbLf & "                         /*Obtenemos el servidor,BD y el identificador de la Cia del portal*/ "
sConsulta = sConsulta & vbLf & "                         SELECT @SERVER = FSP_SRV, @BD = FSP_BD , @FSP_CIA = FSP_CIA FROM PARGEN_PORT "
sConsulta = sConsulta & vbLf & "                         /*Ejecutamos el store procedure de notificaci�n que se encuentra en el portal.*/ "
sConsulta = sConsulta & vbLf & "                         SET @CONEX=@SERVER + '.' + @BD + '.dbo.SP_ACTUALIZAR_PEND_NOTIF' "
sConsulta = sConsulta & vbLf & "                         EXECUTE @CONEX @FSP_CIA,@ANYO,@GMN1,@PROCE,@ITEM,@PROVECOMP,@PROCEDEN,@ITEMDEN,@EMAIL,@PROVEDEN,@CONTACTO "
sConsulta = sConsulta & vbLf & "                     END "
sConsulta = sConsulta & vbLf & "                 END "
sConsulta = sConsulta & vbLf & "                 /*Actualizamos OFEMIN con el precio m�s bajo*/ "
sConsulta = sConsulta & vbLf & "                 UPDATE OFEMIN SET PRECIO=@PRECIO,PROVE=@PROVE2,OFE=@OFE2  , CAMBIO=@CAMBIOMONCEN "
sConsulta = sConsulta & vbLf & "                   WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM "
sConsulta = sConsulta & vbLf & "                     UPDATE PROCE_PROVE "
sConsulta = sConsulta & vbLf & "                             SET SUPERADA = 0"
sConsulta = sConsulta & vbLf & "                         FROM PROCE_PROVE A"
sConsulta = sConsulta & vbLf & "                      WHERE A.ANYO= @ANYO "
sConsulta = sConsulta & vbLf & "                            AND A.GMN1=@GMN1 "
sConsulta = sConsulta & vbLf & "                            AND A.PROCE=@PROCE "
sConsulta = sConsulta & vbLf & "                            AND A.PROVE=@PROVE2 "
sConsulta = sConsulta & vbLf & "                            AND NOT EXISTS (SELECT * FROM OFEMIN B WHERE A.ANYO=B.ANYO AND A.GMN1=B.GMN1 AND A.PROCE=B.PROCE AND A.PROVE=B.PROVE)"
sConsulta = sConsulta & vbLf & "                   /*Si no era la oferta m�nima borramos y sino insertamos en DETALLE_PUJAS*/ "
sConsulta = sConsulta & vbLf & "                 IF @OFEMIN >0 "
sConsulta = sConsulta & vbLf & "                     DELETE FROM DETALLE_PUJAS WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM AND ID=(SELECT MAX(ID) FROM DETALLE_PUJAS WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM) "
sConsulta = sConsulta & vbLf & "                 SET @IDENT = (SELECT MAX(ID) FROM DETALLE_PUJAS WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM ) "
sConsulta = sConsulta & vbLf & "                 IF (SELECT COUNT(*) FROM DETALLE_PUJAS WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM AND PROVE=@PROVE2 AND OFE=@OFE2  AND ID=@IDENT)=0 "
sConsulta = sConsulta & vbLf & "                 BEGIN "
sConsulta = sConsulta & vbLf & "                     IF @IDENT IS NULL "
sConsulta = sConsulta & vbLf & "                         SET @IDENT=1 "
sConsulta = sConsulta & vbLf & "                     Else "
sConsulta = sConsulta & vbLf & "                         SET @IDENT= @IDENT+1 "
sConsulta = sConsulta & vbLf & "                     if @PRECIO IS NOT NULL "
sConsulta = sConsulta & vbLf & "                             INSERT INTO DETALLE_PUJAS (ANYO,GMN1,PROCE,ITEM,ID,OFE,PROVE,FECHA,PRECIO) VALUES (@ANYO,@GMN1,@PROCE,@ITEM,@IDENT,@OFE2,@PROVE2,GETDATE(),@PRECIO) "
sConsulta = sConsulta & vbLf & "                 END "
sConsulta = sConsulta & vbLf & "             End "
sConsulta = sConsulta & vbLf & "         End "
sConsulta = sConsulta & vbLf & "     End "
sConsulta = sConsulta & vbLf & " End "
sConsulta = sConsulta & vbLf & " FETCH NEXT FROM curTG_ITEM_OFE_Upd INTO @ANYO,@GMN1,@PROCE,@ITEM,@PROVE, @NUM,@ULT "
sConsulta = sConsulta & vbLf & "End "
sConsulta = sConsulta & vbLf & "Close curTG_ITEM_OFE_Upd "
sConsulta = sConsulta & vbLf & "DEALLOCATE curTG_ITEM_OFE_Upd"

ModificarITEM_OFE_TG_UPD_2_7_50_5 = sConsulta
End Function

Private Function ModificarITEM_TG_DEL_2_7_50_6() As String
Dim sConsulta As String

sConsulta = "CREATE TRIGGER ITEM_TG_DEL ON dbo.ITEM"
sConsulta = sConsulta & vbLf & "FOR DELETE"
sConsulta = sConsulta & vbLf & "AS"
sConsulta = sConsulta & vbLf & "DECLARE @ANYO AS INTEGER"
sConsulta = sConsulta & vbLf & "DECLARE @GMN1 AS VARCHAR(50)"
sConsulta = sConsulta & vbLf & "DECLARE @PROCE AS INTEGER"
sConsulta = sConsulta & vbLf & "DECLARE @ID AS INTEGER"
sConsulta = sConsulta & vbLf & "DECLARE @FECINI AS DATETIME"
sConsulta = sConsulta & vbLf & "DECLARE @FECMIN AS DATETIME"
sConsulta = sConsulta & vbLf & "DECLARE @FECNECMANUAL  AS TINYINT"
sConsulta = sConsulta & vbLf & ""
sConsulta = sConsulta & vbLf & ""
sConsulta = sConsulta & vbLf & ""
sConsulta = sConsulta & vbLf & ""
sConsulta = sConsulta & vbLf & ""
sConsulta = sConsulta & vbLf & "DECLARE curTG_ITEM_Del CURSOR FOR SELECT ANYO,GMN1,PROCE,ID,FECINI FROM DELETED"
sConsulta = sConsulta & vbLf & "OPEN curTG_ITEM_Del"
sConsulta = sConsulta & vbLf & "FETCH NEXT FROM curTG_ITEM_Del INTO @ANYO,@GMN1,@PROCE,@ID,@FECINI"
sConsulta = sConsulta & vbLf & "WHILE @@FETCH_STATUS=0"
sConsulta = sConsulta & vbLf & "BEGIN"
sConsulta = sConsulta & vbLf & ""
sConsulta = sConsulta & vbLf & "SET  @FECNECMANUAL=(SELECT PROCE.FECNECMANUAL FROM PROCE  WHERE ANYO=@ANYO  AND GMN1=@GMN1 AND COD=@PROCE)"
sConsulta = sConsulta & vbLf & ""
sConsulta = sConsulta & vbLf & "IF @FECNECMANUAL=0"
sConsulta = sConsulta & vbLf & "UPDATE PROCE SET FECNEC =(SELECT MIN(FECINI) FROM ITEM WHERE  ANYO=@ANYO  AND GMN1=@GMN1 AND PROCE=@PROCE)"
sConsulta = sConsulta & vbLf & "WHERE COD=@PROCE"
sConsulta = sConsulta & vbLf & ""
sConsulta = sConsulta & vbLf & "/* Borramos su distribuci�n y sus especificaciones*/"
sConsulta = sConsulta & vbLf & "DELETE FROM ITEM_UON3 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ID"
sConsulta = sConsulta & vbLf & "DELETE FROM ITEM_UON2 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ID"
sConsulta = sConsulta & vbLf & "DELETE FROM ITEM_UON1 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ID"
sConsulta = sConsulta & vbLf & "DELETE FROM ITEM_ESP  WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ID"
sConsulta = sConsulta & vbLf & ""
sConsulta = sConsulta & vbLf & ""
sConsulta = sConsulta & vbLf & "/* Si se han eliminado todos los  items, actualizamos el estado del proceso  si es que este era inferior a 3*/"
sConsulta = sConsulta & vbLf & "IF (SELECT PROCE.EST FROM PROCE WHERE ANYO=@ANYO  AND GMN1=@GMN1 AND COD=@PROCE) = 2  /* 3= Validado */"
sConsulta = sConsulta & vbLf & " BEGIN"
sConsulta = sConsulta & vbLf & "  IF (SELECT COUNT(*) FROM ITEM WHERE ANYO=@ANYO  AND GMN1=@GMN1 AND PROCE=@PROCE) = 0  /* 3= Validado */"
sConsulta = sConsulta & vbLf & "   BEGIN"
sConsulta = sConsulta & vbLf & "    UPDATE PROCE SET EST =1  WHERE ANYO= @ANYO  AND GMN1=@GMN1 AND COD=@PROCE"
sConsulta = sConsulta & vbLf & "   End"
sConsulta = sConsulta & vbLf & " End"
sConsulta = sConsulta & vbLf & ""
sConsulta = sConsulta & vbLf & ""
sConsulta = sConsulta & vbLf & " IF (SELECT SUBASTA FROM PROCE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE) = 1 "
sConsulta = sConsulta & vbLf & " BEGIN "
sConsulta = sConsulta & vbLf & ""
sConsulta = sConsulta & vbLf & "DELETE DETALLE_PUJAS  "
sConsulta = sConsulta & vbLf & "  FROM DETALLE_PUJAS A "
sConsulta = sConsulta & vbLf & "     INNER JOIN DELETED B"
sConsulta = sConsulta & vbLf & "             ON A.ANYO=B.ANYO "
sConsulta = sConsulta & vbLf & "            AND A.GMN1= B.GMN1 "
sConsulta = sConsulta & vbLf & "            AND A.PROCE=B.PROCE "
sConsulta = sConsulta & vbLf & "            AND A.ITEM=B.ID"
sConsulta = sConsulta & vbLf & ""
sConsulta = sConsulta & vbLf & "IF (SELECT COUNT(*) FROM (SELECT DISTINCT PROVE "
sConsulta = sConsulta & vbLf & "                            FROM OFEMIN "
sConsulta = sConsulta & vbLf & "                           WHERE ANYO = @ANYO"
sConsulta = sConsulta & vbLf & "                             AND GMN1=@GMN1"
sConsulta = sConsulta & vbLf & "                             AND PROCE = @PROCE"
sConsulta = sConsulta & vbLf & "                             AND PROVE IS NOT NULL) A)>1 "
sConsulta = sConsulta & vbLf & "  UPDATE PROCE_PROVE"
sConsulta = sConsulta & vbLf & "     SET SUPERADA = 1"
sConsulta = sConsulta & vbLf & "   WHERE ANYO = @ANYO"
sConsulta = sConsulta & vbLf & "     AND GMN1 = @GMN1"
sConsulta = sConsulta & vbLf & "     AND PROCE = @PROCE"
sConsulta = sConsulta & vbLf & "ELSE"
sConsulta = sConsulta & vbLf & "  BEGIN"
sConsulta = sConsulta & vbLf & "    DECLARE @PROVE VARCHAR(50)"
sConsulta = sConsulta & vbLf & "    SELECT TOP 1 @PROVE = PROVE "
sConsulta = sConsulta & vbLf & "      FROM OFEMIN"
sConsulta = sConsulta & vbLf & "     WHERE ANYO=@ANYO"
sConsulta = sConsulta & vbLf & "       AND GMN1 = @GMN1"
sConsulta = sConsulta & vbLf & "       AND PROCE = @PROCE"
sConsulta = sConsulta & vbLf & "    UPDATE PROCE_PROVE"
sConsulta = sConsulta & vbLf & "       SET SUPERADA = CASE WHEN A.PROVE=@PROVE THEN 0 ELSE 1 END"
sConsulta = sConsulta & vbLf & "      FROM PROCE_PROVE A"
sConsulta = sConsulta & vbLf & "     WHERE A.ANYO=@ANYO"
sConsulta = sConsulta & vbLf & "       AND A.GMN1 = @GMN1"
sConsulta = sConsulta & vbLf & "       AND A.PROCE = @PROCE"
sConsulta = sConsulta & vbLf & "  END"
sConsulta = sConsulta & vbLf & "                 "
sConsulta = sConsulta & vbLf & ""
sConsulta = sConsulta & vbLf & "end"
sConsulta = sConsulta & vbLf & ""
sConsulta = sConsulta & vbLf & ""
sConsulta = sConsulta & vbLf & ""
sConsulta = sConsulta & vbLf & "FETCH NEXT FROM curTG_ITEM_Del INTO @ANYO,@GMN1,@PROCE,@ID,@FECINI"
sConsulta = sConsulta & vbLf & "End"
sConsulta = sConsulta & vbLf & "Close curTG_ITEM_Del"
sConsulta = sConsulta & vbLf & "DEALLOCATE curTG_ITEM_Del"

ModificarITEM_TG_DEL_2_7_50_6 = sConsulta
End Function

Private Function ModificarITEM_OFE_TG_UPD_2_7_50_6() As String
Dim sConsulta As String

sConsulta = "CREATE TRIGGER ITEM_OFE_TG_UPD ON [ITEM_OFE] "
sConsulta = sConsulta & vbLf & "FOR UPDATE "
sConsulta = sConsulta & vbLf & "AS "
sConsulta = sConsulta & vbLf & "DECLARE @ANYO INT,@GMN1 VARCHAR(50),@PROCE INT, @ITEM INT,@PROVE VARCHAR(50), @NUM INT,@ULT INT "
sConsulta = sConsulta & vbLf & "DECLARE @PRECIO AS FLOAT "
sConsulta = sConsulta & vbLf & "DECLARE @PROVE2 AS VARCHAR(50) "
sConsulta = sConsulta & vbLf & "DECLARE @CONEX  AS VARCHAR(100),@FSP_CIA AS VARCHAR(50) "
sConsulta = sConsulta & vbLf & "DECLARE @BD AS VARCHAR(100),@SERVER AS VARCHAR(100) "
sConsulta = sConsulta & vbLf & "DECLARE @EMAIL AS VARCHAR(100) ,@PROVEDEN AS VARCHAR(500) , @CONTACTO AS VARCHAR(500) "
sConsulta = sConsulta & vbLf & "DECLARE @PRECCOMP AS FLOAT,@PROVECOMP AS VARCHAR(50) "
sConsulta = sConsulta & vbLf & "DECLARE @OFE2 AS SMALLINT "
sConsulta = sConsulta & vbLf & "DECLARE @IDENT AS INTEGER "
sConsulta = sConsulta & vbLf & "DECLARE @OFEMIN AS INTEGER "
sConsulta = sConsulta & vbLf & "DECLARE @PRIMERO AS INTEGER "
sConsulta = sConsulta & vbLf & "DECLARE @PROCEDEN VARCHAR(500),@ITEMDEN VARCHAR(500) "
sConsulta = sConsulta & vbLf & "DECLARE @CAMBIOMONCEN FLOAT "
sConsulta = sConsulta & vbLf & "DECLARE @INST INTEGER "
sConsulta = sConsulta & vbLf & "DECLARE @EMAIL_EX AS VARCHAR(100),@CONTACTO_EX AS VARCHAR(500) "
sConsulta = sConsulta & vbLf & "DECLARE curTG_ITEM_OFE_Upd CURSOR FOR SELECT ANYO,GMN1,PROCE,ITEM,PROVE,NUM,ULT FROM INSERTED "
sConsulta = sConsulta & vbLf & "OPEN curTG_ITEM_OFE_Upd "
sConsulta = sConsulta & vbLf & "FETCH NEXT FROM curTG_ITEM_OFE_Upd INTO @ANYO,@GMN1,@PROCE,@ITEM,@PROVE,@NUM,@ULT "
sConsulta = sConsulta & vbLf & "WHILE @@FETCH_STATUS=0 "
sConsulta = sConsulta & vbLf & "BEGIN "
sConsulta = sConsulta & vbLf & " IF (SELECT SUBASTA FROM PROCE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE) = 1 "
sConsulta = sConsulta & vbLf & " BEGIN "
sConsulta = sConsulta & vbLf & "     IF UPDATE(PRECIO) "
sConsulta = sConsulta & vbLf & "     BEGIN "
sConsulta = sConsulta & vbLf & "         "
sConsulta = sConsulta & vbLf & "         SELECT @PRIMERO = COUNT(*) "
sConsulta = sConsulta & vbLf & "           FROM OFEMIN "
sConsulta = sConsulta & vbLf & "          WHERE ANYO=@ANYO "
sConsulta = sConsulta & vbLf & "            AND GMN1=@GMN1 "
sConsulta = sConsulta & vbLf & "            AND PROCE=@PROCE"
sConsulta = sConsulta & vbLf & "            AND PROVE=@PROVE "
sConsulta = sConsulta & vbLf & "            AND ITEM=@ITEM "
sConsulta = sConsulta & vbLf & "            AND OFE=@NUM"
sConsulta = sConsulta & vbLf & ""
sConsulta = sConsulta & vbLf & "         SELECT @OFEMIN = COUNT(*) "
sConsulta = sConsulta & vbLf & "           FROM OFEMIN "
sConsulta = sConsulta & vbLf & "          WHERE ANYO=@ANYO "
sConsulta = sConsulta & vbLf & "            AND GMN1=@GMN1 "
sConsulta = sConsulta & vbLf & "            AND PROCE=@PROCE "
sConsulta = sConsulta & vbLf & "            AND PROVE=@PROVE "
sConsulta = sConsulta & vbLf & "            AND ITEM=@ITEM"
sConsulta = sConsulta & vbLf & ""
sConsulta = sConsulta & vbLf & "         IF @ULT=1 OR @PRIMERO>0 or @OFEMIN>0 /*Si el precio se ha modificado en la �ltima oferta o era el precio que actualmente era el m�nimo, recalculamos el precio m�s bajo para ese item */ "
sConsulta = sConsulta & vbLf & "         BEGIN "
sConsulta = sConsulta & vbLf & "             SELECT TOP 1 @PRECIO= A.PRECIO / (B.CAMBIO * C.CAMBIO), @PROVE2=A.PROVE,@OFE2=A.NUM "
sConsulta = sConsulta & vbLf & "                   FROM ITEM_OFE A INNER JOIN PROCE_OFE B ON A.ANYO=B.ANYO AND A.GMN1=B.GMN1 AND A.PROCE=B.PROCE AND A.PROVE=B.PROVE AND A.NUM=B.OFE"
sConsulta = sConsulta & vbLf & "                  INNER JOIN PROCE C ON A.ANYO=C.ANYO AND A.GMN1=C.GMN1 AND A.PROCE = C.COD"
sConsulta = sConsulta & vbLf & "             WHERE A.ANYO=@ANYO AND A.GMN1=@GMN1 AND A.PROCE=@PROCE AND A.ITEM=@ITEM AND A.ULT=1 AND A.PRECIO IS NOT NULL"
sConsulta = sConsulta & vbLf & "             ORDER BY A.PRECIO / (B.CAMBIO * C.CAMBIO) ASC "
sConsulta = sConsulta & vbLf & "              /*Si el nuevo precio calculado es m�s bajo y el proveedor es diferente del que antes ten�a el precio m�s bajo se actualiza OFEMIN*/ "
sConsulta = sConsulta & vbLf & "             SELECT @PRECCOMP=PRECIO,@PROVECOMP=PROVE FROM OFEMIN WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM "
sConsulta = sConsulta & vbLf & "             IF @PROVECOMP IS NULL "
sConsulta = sConsulta & vbLf & "             /*Si el precio y prov. es nulo no notifica pero actualiza ofemin*/ "
sConsulta = sConsulta & vbLf & "             BEGIN "
sConsulta = sConsulta & vbLf & "                 UPDATE OFEMIN SET PRECIO=@PRECIO,PROVE=@PROVE2,OFE=@OFE2 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM "
sConsulta = sConsulta & vbLf & "                 UPDATE PROCE_PROVE SET SUPERADA = 1 WHERE ANYO= @ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE!=@PROVE2"
sConsulta = sConsulta & vbLf & "                 if @PRECIO IS NOT NULL "
sConsulta = sConsulta & vbLf & "                     INSERT INTO DETALLE_PUJAS (ANYO,GMN1,PROCE,ITEM,ID,OFE,PROVE,FECHA,PRECIO) VALUES (@ANYO,@GMN1,@PROCE,@ITEM,1,@OFE2,@PROVE2,GETDATE(),@PRECIO) "
sConsulta = sConsulta & vbLf & "                 if (select count(*) from ofemin where ANYO=@anyo and gmn1=@gmn1 and proce=@proce and prove!=@prove2)=0"
sConsulta = sConsulta & vbLf & "                     UPDATE PROCE_PROVE "
sConsulta = sConsulta & vbLf & "                             SET SUPERADA = 0"
sConsulta = sConsulta & vbLf & "                         FROM PROCE_PROVE A"
sConsulta = sConsulta & vbLf & "                      WHERE A.ANYO= @ANYO "
sConsulta = sConsulta & vbLf & "                            AND A.GMN1=@GMN1 "
sConsulta = sConsulta & vbLf & "                            AND A.PROCE=@PROCE "
sConsulta = sConsulta & vbLf & "                            AND A.PROVE=@PROVE2 "
sConsulta = sConsulta & vbLf & "             End "
sConsulta = sConsulta & vbLf & "             ELSE IF  @PRECIO <= @PRECCOMP OR @PRIMERO>0 OR @OFEMIN>0"
sConsulta = sConsulta & vbLf & "             BEGIN "
sConsulta = sConsulta & vbLf & "                 IF @PROVECOMP <>@PROVE2 /*Si el proveedor es diferente habr� que notificarle que su oferta ha sido pisada*/ "
sConsulta = sConsulta & vbLf & "                 BEGIN "
sConsulta = sConsulta & vbLf & "                     /*Obtenemos los datos del proveedor (Denominaci�n,persona de contacto y e-mail de la persona de contacto) para notificarle que su oferta "
sConsulta = sConsulta & vbLf & "                     ha sido pisada*/ "
sConsulta = sConsulta & vbLf & "                     UPDATE PROCE_PROVE SET SUPERADA = 1 WHERE ANYO= @ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVECOMP"
sConsulta = sConsulta & vbLf & "                     SET CONCAT_NULL_YIELDS_NULL OFF "
sConsulta = sConsulta & vbLf & "                     SELECT TOP 1 @EMAIL=CON.EMAIL,@PROVEDEN=PROVE.DEN,@CONTACTO = NOMCON + ' ' + APECON "
sConsulta = sConsulta & vbLf & "                       FROM PROVE,PROCE_PROVE_PET,CON "
sConsulta = sConsulta & vbLf & "                      WHERE PROVE.COD= PROCE_PROVE_PET.PROVE "
sConsulta = sConsulta & vbLf & "                        AND PROCE_PROVE_PET.PROVE= CON.PROVE "
sConsulta = sConsulta & vbLf & "                        AND PROCE_PROVE_PET.NOMCON=CON.NOM "
sConsulta = sConsulta & vbLf & "                        AND PROCE_PROVE_PET.APECON = CON.APE "
sConsulta = sConsulta & vbLf & "                        AND PROCE_PROVE_PET.TIPOPET=0 "
sConsulta = sConsulta & vbLf & "                        AND ANYO=@ANYO "
sConsulta = sConsulta & vbLf & "                        AND GMN1=@GMN1 "
sConsulta = sConsulta & vbLf & "                        AND PROCE=@PROCE "
sConsulta = sConsulta & vbLf & "                        AND PROVE.COD=@PROVECOMP "
sConsulta = sConsulta & vbLf & "                      ORDER BY PROCE_PROVE_PET.ID DESC "
sConsulta = sConsulta & vbLf & "                     SELECT @PROCEDEN=PROCE.DEN,@ITEMDEN=ITEM.DESCR + ART4.DEN "
sConsulta = sConsulta & vbLf & "                       FROM PROCE "
sConsulta = sConsulta & vbLf & "                           INNER JOIN ITEM "
sConsulta = sConsulta & vbLf & "                                   ON PROCE.ANYO=ITEM.ANYO "
sConsulta = sConsulta & vbLf & "                                  AND Proce.gmn1 = Item.gmn1 "
sConsulta = sConsulta & vbLf & "                                  And Proce.COD = Item.Proce "
sConsulta = sConsulta & vbLf & "                            LEFT JOIN ART4 "
sConsulta = sConsulta & vbLf & "                                   ON ITEM.ART=ART4.COD "
sConsulta = sConsulta & vbLf & "                                  AND ITEM.GMN1=ART4.GMN1 "
sConsulta = sConsulta & vbLf & "                                  AND ITEM.GMN2=ART4.GMN2  "
sConsulta = sConsulta & vbLf & "                                  AND ITEM.GMN3=ART4.GMN3 "
sConsulta = sConsulta & vbLf & "                                  AND ITEM.GMN4=ART4.GMN4  "
sConsulta = sConsulta & vbLf & "                      WHERE PROCE.ANYO=@ANYO "
sConsulta = sConsulta & vbLf & "                        AND PROCE.GMN1=@GMN1 "
sConsulta = sConsulta & vbLf & "                        AND PROCE.COD=@PROCE "
sConsulta = sConsulta & vbLf & "                        AND ITEM.ID=@ITEM "
sConsulta = sConsulta & vbLf & "                     SET CONCAT_NULL_YIELDS_NULL ON"
sConsulta = sConsulta & vbLf & "                     /*Si la instalaci�n es de BuySite*/ "
sConsulta = sConsulta & vbLf & "                     SELECT @INST=INSTWEB FROM PARGEN_INTERNO "
sConsulta = sConsulta & vbLf & "                     IF @INST = 1 "
sConsulta = sConsulta & vbLf & "                     BEGIN "
sConsulta = sConsulta & vbLf & "                         /*Inserta en la tabla PROVENOTIF*/ "
sConsulta = sConsulta & vbLf & "                         IF @EMAIL IS NULL OR @CONTACTO IS NULL  /*Si el e-mail o el contacto son nulos saca de contactos alguno que no sea nulo*/ "
sConsulta = sConsulta & vbLf & "                         BEGIN "
sConsulta = sConsulta & vbLf & "                             SELECT TOP 1 @EMAIL_EX=CON.EMAIL,@CONTACTO_EX = NOMCON + ' ' + APECON FROM PROCE_PROVE_PET,CON  "
sConsulta = sConsulta & vbLf & "                             WHERE PROCE_PROVE_PET.PROVE= CON.PROVE AND PROCE_PROVE_PET.NOMCON=CON.NOM AND PROCE_PROVE_PET.APECON = CON.APE  "
sConsulta = sConsulta & vbLf & "                             AND PROCE_PROVE_PET.TIPOPET=0 AND ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROCE_PROVE_PET.PROVE=@PROVECOMP  "
sConsulta = sConsulta & vbLf & "                             AND CON.EMAIL IS NOT NULL AND NOMCON IS NOT NULL "
sConsulta = sConsulta & vbLf & "                             INSERT INTO PROVENOTIF (ANYO,GMN1,PROCE,PROCEDEN,ITEM,ITEMDEN,PROVE,PROVEDEN,EMAIL,CONTACTO) VALUES "
sConsulta = sConsulta & vbLf & "                             (@ANYO,@GMN1,@PROCE,@PROCEDEN,@ITEM,@ITEMDEN,@PROVE,@PROVEDEN,@EMAIL_EX,@CONTACTO_EX) "
sConsulta = sConsulta & vbLf & "                         END "
sConsulta = sConsulta & vbLf & "                         ELSE "
sConsulta = sConsulta & vbLf & "                             INSERT INTO PROVENOTIF (ANYO,GMN1,PROCE,PROCEDEN,ITEM,ITEMDEN,PROVE,PROVEDEN,EMAIL,CONTACTO) VALUES "
sConsulta = sConsulta & vbLf & "                             (@ANYO,@GMN1,@PROCE,@PROCEDEN,@ITEM,@ITEMDEN,@PROVE,@PROVEDEN,@EMAIL,@CONTACTO) "
sConsulta = sConsulta & vbLf & "                     END "
sConsulta = sConsulta & vbLf & "                     ELSE /*Si la instalaci�n es de Portal*/ "
sConsulta = sConsulta & vbLf & "                     BEGIN "
sConsulta = sConsulta & vbLf & "                         /*Obtenemos el servidor,BD y el identificador de la Cia del portal*/ "
sConsulta = sConsulta & vbLf & "                         SELECT @SERVER = FSP_SRV, @BD = FSP_BD , @FSP_CIA = FSP_CIA FROM PARGEN_PORT "
sConsulta = sConsulta & vbLf & "                         /*Ejecutamos el store procedure de notificaci�n que se encuentra en el portal.*/ "
sConsulta = sConsulta & vbLf & "                         SET @CONEX=@SERVER + '.' + @BD + '.dbo.SP_ACTUALIZAR_PEND_NOTIF' "
sConsulta = sConsulta & vbLf & "                         EXECUTE @CONEX @FSP_CIA,@ANYO,@GMN1,@PROCE,@ITEM,@PROVECOMP,@PROCEDEN,@ITEMDEN,@EMAIL,@PROVEDEN,@CONTACTO "
sConsulta = sConsulta & vbLf & "                     END "
sConsulta = sConsulta & vbLf & "                 END "
sConsulta = sConsulta & vbLf & "                 /*Actualizamos OFEMIN con el precio m�s bajo*/ "
sConsulta = sConsulta & vbLf & "                 UPDATE OFEMIN SET PRECIO=@PRECIO,PROVE=@PROVE2,OFE=@OFE2  , CAMBIO=@CAMBIOMONCEN "
sConsulta = sConsulta & vbLf & "                   WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM "
sConsulta = sConsulta & vbLf & ""
sConsulta = sConsulta & vbLf & "          if (select count(*) from ofemin where ANYO=@anyo and gmn1=@gmn1 and proce=@proce and prove!=@prove2)=0"
sConsulta = sConsulta & vbLf & "                     UPDATE PROCE_PROVE "
sConsulta = sConsulta & vbLf & "                             SET SUPERADA = 0"
sConsulta = sConsulta & vbLf & "                         FROM PROCE_PROVE A"
sConsulta = sConsulta & vbLf & "                      WHERE A.ANYO= @ANYO "
sConsulta = sConsulta & vbLf & "                            AND A.GMN1=@GMN1 "
sConsulta = sConsulta & vbLf & "                            AND A.PROCE=@PROCE "
sConsulta = sConsulta & vbLf & "                            AND A.PROVE=@PROVE2 "
sConsulta = sConsulta & vbLf & "                   /*Si no era la oferta m�nima borramos y sino insertamos en DETALLE_PUJAS*/ "
sConsulta = sConsulta & vbLf & "                 IF @PRIMERO >0 OR @OFEMIN>0"
sConsulta = sConsulta & vbLf & "                     DELETE FROM DETALLE_PUJAS WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM AND ID=(SELECT MAX(ID) FROM DETALLE_PUJAS WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM) "
sConsulta = sConsulta & vbLf & "                 SET @IDENT = (SELECT MAX(ID) FROM DETALLE_PUJAS WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM ) "
sConsulta = sConsulta & vbLf & "                 IF (SELECT COUNT(*) FROM DETALLE_PUJAS WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM AND PROVE=@PROVE2 AND OFE=@OFE2  AND ID=@IDENT)=0 "
sConsulta = sConsulta & vbLf & "                 BEGIN "
sConsulta = sConsulta & vbLf & "                     IF @IDENT IS NULL "
sConsulta = sConsulta & vbLf & "                         SET @IDENT=1 "
sConsulta = sConsulta & vbLf & "                     Else "
sConsulta = sConsulta & vbLf & "                         SET @IDENT= @IDENT+1 "
sConsulta = sConsulta & vbLf & "                     if @PRECIO IS NOT NULL "
sConsulta = sConsulta & vbLf & "                             INSERT INTO DETALLE_PUJAS (ANYO,GMN1,PROCE,ITEM,ID,OFE,PROVE,FECHA,PRECIO) VALUES (@ANYO,@GMN1,@PROCE,@ITEM,@IDENT,@OFE2,@PROVE2,GETDATE(),@PRECIO) "
sConsulta = sConsulta & vbLf & "                 END "
sConsulta = sConsulta & vbLf & "             End "
sConsulta = sConsulta & vbLf & "         End "
sConsulta = sConsulta & vbLf & "     End "
sConsulta = sConsulta & vbLf & " End "
sConsulta = sConsulta & vbLf & " FETCH NEXT FROM curTG_ITEM_OFE_Upd INTO @ANYO,@GMN1,@PROCE,@ITEM,@PROVE, @NUM,@ULT "
sConsulta = sConsulta & vbLf & "End "
sConsulta = sConsulta & vbLf & "Close curTG_ITEM_OFE_Upd "
sConsulta = sConsulta & vbLf & "DEALLOCATE curTG_ITEM_OFE_Upd "

ModificarITEM_OFE_TG_UPD_2_7_50_6 = sConsulta
End Function

Private Function ModificarPROCE_TG_UPD_2_7_50_6() As String
Dim sConsulta As String

sConsulta = "CREATE TRIGGER PROCE_TG_UPD ON dbo.PROCE"
sConsulta = sConsulta & vbLf & "FOR UPDATE"
sConsulta = sConsulta & vbLf & "AS"
sConsulta = sConsulta & vbLf & "DECLARE @ANYO INT,@GMN1 VARCHAR(50),@COD SMALLINT"
sConsulta = sConsulta & vbLf & "DECLARE @ITEM INT"
sConsulta = sConsulta & vbLf & "DECLARE @SUBASTA AS TINYINT"
sConsulta = sConsulta & vbLf & "DECLARE @OLDSUBASTA AS TINYINT"
sConsulta = sConsulta & vbLf & ""
sConsulta = sConsulta & vbLf & ""
sConsulta = sConsulta & vbLf & ""
sConsulta = sConsulta & vbLf & "DECLARE curTG_PROCE_upd CURSOR FOR SELECT ANYO,GMN1,COD,SUBASTA FROM INSERTED"
sConsulta = sConsulta & vbLf & "OPEN curTG_PROCE_upd"
sConsulta = sConsulta & vbLf & "FETCH NEXT FROM curTG_PROCE_upd INTO @ANYO,@GMN1,@COD,@SUBASTA"
sConsulta = sConsulta & vbLf & "WHILE @@FETCH_STATUS=0"
sConsulta = sConsulta & vbLf & "BEGIN"
sConsulta = sConsulta & vbLf & "  SELECT @OLDSUBASTA = SUBASTA FROM DELETED WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@COD"
sConsulta = sConsulta & vbLf & "  IF @SUBASTA!=@OLDSUBASTA"
sConsulta = sConsulta & vbLf & "      BEGIN"
sConsulta = sConsulta & vbLf & "          DELETE FROM OFEMIN WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@COD"
sConsulta = sConsulta & vbLf & "          IF @SUBASTA=1"
sConsulta = sConsulta & vbLf & "              BEGIN"
sConsulta = sConsulta & vbLf & "                  DECLARE curTG_ITEM CURSOR FOR SELECT ID FROM ITEM WHERE  ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@COD"
sConsulta = sConsulta & vbLf & "                  OPEN curTG_ITEM"
sConsulta = sConsulta & vbLf & "                  FETCH NEXT FROM curTG_ITEM INTO @ITEM"
sConsulta = sConsulta & vbLf & "                  WHILE @@FETCH_STATUS=0"
sConsulta = sConsulta & vbLf & "                      BEGIN"
sConsulta = sConsulta & vbLf & "                          INSERT INTO OFEMIN (ANYO,GMN1,PROCE,ITEM,PROVE,PRECIO) VALUES (@ANYO,@GMN1,@COD, @ITEM,NULL,NULL)"
sConsulta = sConsulta & vbLf & "                          FETCH NEXT FROM curTG_ITEM INTO @ITEM"
sConsulta = sConsulta & vbLf & "                      End"
sConsulta = sConsulta & vbLf & "                  Close curTG_ITEM"
sConsulta = sConsulta & vbLf & "                  DEALLOCATE curTG_ITEM"
sConsulta = sConsulta & vbLf & "              End"
sConsulta = sConsulta & vbLf & "      END"
sConsulta = sConsulta & vbLf & "  FETCH NEXT FROM curTG_PROCE_upd INTO @ANYO,@GMN1,@COD,@SUBASTA"
sConsulta = sConsulta & vbLf & "End"
sConsulta = sConsulta & vbLf & "Close curTG_PROCE_upd"
sConsulta = sConsulta & vbLf & "DEALLOCATE curTG_PROCE_upd"

ModificarPROCE_TG_UPD_2_7_50_6 = sConsulta
End Function


Private Function ModificarPRES_3_CON2_COD_2_7_50_7() As String
Dim sConsulta As String

sConsulta = "CREATE PROCEDURE PRES_3_CON2_COD (@OLD VARCHAR(50),@NEW VARCHAR(50),@PRES1 VARCHAR(50))  AS"
sConsulta = sConsulta & vbCrLf & "DECLARE @VAR_COD VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "SET XACT_ABORT ON"
sConsulta = sConsulta & vbCrLf & "BEGIN TRANSACTION"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "DECLARE C CURSOR FOR SELECT COD FROM PRES_3_CON2 WHERE  PRES1=@PRES1 AND COD=@OLD"
sConsulta = sConsulta & vbCrLf & "OPEN C"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM C INTO @VAR_COD"
sConsulta = sConsulta & vbCrLf & "IF @@FETCH_STATUS=0"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_3_CON2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_3_CON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_3_CON4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_3_CON2 SET COD=@NEW WHERE  PRES1=@PRES1 AND COD=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_3_CON3 SET PRES2=@NEW WHERE  PRES1=@PRES1 AND PRES2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_3_CON4 SET PRES2=@NEW WHERE  PRES1=@PRES1 AND PRES2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_PRESCON3 SET PRES2=@NEW WHERE PRES1=@PRES1 AND PRES2=@OLD"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_3_CON2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_3_CON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_3_CON4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "Close C"
sConsulta = sConsulta & vbCrLf & "DEALLOCATE C"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "COMMIT TRANSACTION"

ModificarPRES_3_CON2_COD_2_7_50_7 = sConsulta
End Function

Private Function ModificarPRES_3_CON3_COD_2_7_50_7() As String
Dim sConsulta

sConsulta = "CREATE PROCEDURE PRES_3_CON3_COD (@OLD VARCHAR(50),@NEW VARCHAR(50),@PRES1 VARCHAR(50),@PRES2 VARCHAR(50))  AS"
sConsulta = sConsulta & vbCrLf & "DECLARE @VAR_COD VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "SET XACT_ABORT ON"
sConsulta = sConsulta & vbCrLf & "BEGIN TRANSACTION"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "DECLARE C CURSOR FOR SELECT COD FROM PRES_3_CON3 WHERE  PRES1=@PRES1 AND PRES2=@PRES2 AND COD=@OLD"
sConsulta = sConsulta & vbCrLf & "OPEN C"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM C INTO @VAR_COD"
sConsulta = sConsulta & vbCrLf & "IF @@FETCH_STATUS=0"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_3_CON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_3_CON4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_3_CON3 SET COD=@NEW WHERE  PRES1=@PRES1 AND PRES2=@PRES2 AND COD=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_3_CON4 SET PRES3=@NEW WHERE PRES1=@PRES1 AND PRES2=@PRES2 AND PRES3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_PRESCON3 SET PRES3=@NEW WHERE  PRES1=@PRES1 AND PRES2=@PRES2 AND PRES3=@OLD"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_3_CON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_3_CON4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "Close C"
sConsulta = sConsulta & vbCrLf & "DEALLOCATE C"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "COMMIT TRANSACTION"
 
ModificarPRES_3_CON3_COD_2_7_50_7 = sConsulta
End Function

Private Function ModificarPRES_3_CON4_COD_2_7_50_7() As String
Dim sConsulta As String

sConsulta = "CREATE PROCEDURE PRES_3_CON4_COD (@OLD VARCHAR(50),@NEW VARCHAR(50),@PRES1 VARCHAR(50),@PRES2 VARCHAR(50),@PRES3 VARCHAR(50))  AS"
sConsulta = sConsulta & vbCrLf & "DECLARE @VAR_COD VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "SET XACT_ABORT ON"
sConsulta = sConsulta & vbCrLf & "BEGIN TRANSACTION"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "DECLARE C CURSOR FOR SELECT COD FROM PRES_3_CON4 WHERE PRES1=@PRES1 AND PRES2=@PRES2 AND PRES3=@PRES3 AND COD=@OLD"
sConsulta = sConsulta & vbCrLf & "OPEN C"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM C INTO @VAR_COD"
sConsulta = sConsulta & vbCrLf & "IF @@FETCH_STATUS=0"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_3_CON4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_3_CON4 SET COD=@NEW WHERE PRES1=@PRES1 AND PRES2=@PRES2 AND PRES3=@PRES3 AND COD=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_PRESCON3 SET PRES4=@NEW WHERE PRES1=@PRES1 AND PRES2=@PRES2 AND PRES3=@PRES3 AND PRES4=@OLD"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_3_CON4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "Close C"
sConsulta = sConsulta & vbCrLf & "DEALLOCATE C"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "COMMIT TRANSACTION"

ModificarPRES_3_CON4_COD_2_7_50_7 = sConsulta
End Function


Private Function ModificarPRES_4_CON2_COD_2_7_50_7() As String
Dim sConsulta As String

sConsulta = "CREATE PROCEDURE PRES_4_CON2_COD (@OLD VARCHAR(50),@NEW VARCHAR(50),@PRES1 VARCHAR(50))  AS"
sConsulta = sConsulta & vbCrLf & "DECLARE @VAR_COD VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "SET XACT_ABORT ON"
sConsulta = sConsulta & vbCrLf & "BEGIN TRANSACTION"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "DECLARE C CURSOR FOR SELECT COD FROM PRES_4_CON2 WHERE  PRES1=@PRES1 AND COD=@OLD"
sConsulta = sConsulta & vbCrLf & "OPEN C"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM C INTO @VAR_COD"
sConsulta = sConsulta & vbCrLf & "IF @@FETCH_STATUS=0"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_4_CON2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_4_CON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_4_CON4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_4_CON2 SET COD=@NEW WHERE  PRES1=@PRES1 AND COD=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_4_CON3 SET PRES2=@NEW WHERE  PRES1=@PRES1 AND PRES2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_4_CON4 SET PRES2=@NEW WHERE  PRES1=@PRES1 AND PRES2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_PRESCON4 SET PRES2=@NEW WHERE PRES1=@PRES1 AND PRES2=@OLD"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_4_CON2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_4_CON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_4_CON4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "Close C"
sConsulta = sConsulta & vbCrLf & "DEALLOCATE C"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "COMMIT TRANSACTION"
 
ModificarPRES_4_CON2_COD_2_7_50_7 = sConsulta
End Function

Private Function ModificarPRES_4_CON3_COD_2_7_50_7() As String
Dim sConsulta As String

sConsulta = "CREATE PROCEDURE PRES_4_CON3_COD (@OLD VARCHAR(50),@NEW VARCHAR(50),@PRES1 VARCHAR(50),@PRES2 VARCHAR(50))  AS"
sConsulta = sConsulta & vbCrLf & "DECLARE @VAR_COD VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "SET XACT_ABORT ON"
sConsulta = sConsulta & vbCrLf & "BEGIN TRANSACTION"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "DECLARE C CURSOR FOR SELECT COD FROM PRES_4_CON3 WHERE  PRES1=@PRES1 AND PRES2=@PRES2 AND COD=@OLD"
sConsulta = sConsulta & vbCrLf & "OPEN C"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM C INTO @VAR_COD"
sConsulta = sConsulta & vbCrLf & "IF @@FETCH_STATUS=0"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_4_CON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_4_CON4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_4_CON3 SET COD=@NEW WHERE  PRES1=@PRES1 AND PRES2=@PRES2 AND COD=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_4_CON4 SET PRES3=@NEW WHERE PRES1=@PRES1 AND PRES2=@PRES2 AND PRES3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_PRESCON4 SET PRES3=@NEW WHERE  PRES1=@PRES1 AND PRES2=@PRES2 AND PRES3=@OLD"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_4_CON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_4_CON4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "Close C"
sConsulta = sConsulta & vbCrLf & "DEALLOCATE C"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "COMMIT TRANSACTION"
 
ModificarPRES_4_CON3_COD_2_7_50_7 = sConsulta
End Function

Private Function ModificarPRES_4_CON4_COD_2_7_50_7() As String
Dim sConsulta As String

sConsulta = "CREATE PROCEDURE PRES_4_CON4_COD (@OLD VARCHAR(50),@NEW VARCHAR(50),@PRES1 VARCHAR(50),@PRES2 VARCHAR(50),@PRES3 VARCHAR(50))  AS"
sConsulta = sConsulta & vbCrLf & "DECLARE @VAR_COD VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "SET XACT_ABORT ON"
sConsulta = sConsulta & vbCrLf & "BEGIN TRANSACTION"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "DECLARE C CURSOR FOR SELECT COD FROM PRES_4_CON4 WHERE PRES1=@PRES1 AND PRES2=@PRES2 AND PRES3=@PRES3 AND COD=@OLD"
sConsulta = sConsulta & vbCrLf & "OPEN C"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM C INTO @VAR_COD"
sConsulta = sConsulta & vbCrLf & "IF @@FETCH_STATUS=0"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_4_CON4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_4_CON4 SET COD=@NEW WHERE PRES1=@PRES1 AND PRES2=@PRES2 AND PRES3=@PRES3 AND COD=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_PRESCON4 SET PRES4=@NEW WHERE PRES1=@PRES1 AND PRES2=@PRES2 AND PRES3=@PRES3 AND PRES4=@OLD"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_4_CON4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "Close C"
sConsulta = sConsulta & vbCrLf & "DEALLOCATE C"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "COMMIT TRANSACTION"

ModificarPRES_4_CON4_COD_2_7_50_7 = sConsulta
End Function



Private Function ModificarPROVE_COD_2_7_50_8() As String
Dim sConsulta As String

sConsulta = "CREATE PROCEDURE PROVE_COD (@OLD VARCHAR(50),@NEW VARCHAR(50))  AS "
sConsulta = sConsulta & vbCrLf & "DECLARE @VAR_COD VARCHAR(50) "
sConsulta = sConsulta & vbCrLf & "SET XACT_ABORT ON "
sConsulta = sConsulta & vbCrLf & "BEGIN TRANSACTION "
sConsulta = sConsulta & vbCrLf & "BEGIN "
sConsulta = sConsulta & vbCrLf & "DECLARE C CURSOR FOR SELECT COD FROM PROVE WHERE COD=@OLD "
sConsulta = sConsulta & vbCrLf & "OPEN C "
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM C INTO @VAR_COD "
sConsulta = sConsulta & vbCrLf & "IF @@FETCH_STATUS=0 "
sConsulta = sConsulta & vbCrLf & "BEGIN "
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE NOCHECK CONSTRAINT ALL "
sConsulta = sConsulta & vbCrLf & "ALTER TABLE CON NOCHECK CONSTRAINT ALL "
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART1_ADJ NOCHECK CONSTRAINT ALL "
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART2_ADJ NOCHECK CONSTRAINT ALL "
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART3_ADJ NOCHECK CONSTRAINT ALL "
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4_ADJ NOCHECK CONSTRAINT ALL "
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_ADJ NOCHECK CONSTRAINT ALL "
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_ADJREU NOCHECK CONSTRAINT ALL "
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_OFE NOCHECK CONSTRAINT ALL "
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_OFE_WEB NOCHECK CONSTRAINT ALL "
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_OFE_ADJUN NOCHECK CONSTRAINT ALL "
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_OFE NOCHECK CONSTRAINT ALL "
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_OFE_WEB_ADJUN NOCHECK CONSTRAINT ALL "
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_OFE_WEB NOCHECK CONSTRAINT ALL "
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_PROVE NOCHECK CONSTRAINT ALL "
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_PROVE_PET NOCHECK CONSTRAINT ALL "
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_PROVE_POT NOCHECK CONSTRAINT ALL "
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_EQP NOCHECK CONSTRAINT ALL "
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN1 NOCHECK CONSTRAINT ALL "
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN2 NOCHECK CONSTRAINT ALL "
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN3 NOCHECK CONSTRAINT ALL "
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN4 NOCHECK CONSTRAINT ALL "
sConsulta = sConsulta & vbCrLf & "ALTER TABLE USU NOCHECK CONSTRAINT ALL "
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_PROVE NOCHECK CONSTRAINT ALL "
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART4 NOCHECK CONSTRAINT ALL "
sConsulta = sConsulta & vbCrLf & "UPDATE PROVE SET COD=@NEW WHERE COD=@OLD "
sConsulta = sConsulta & vbCrLf & "UPDATE ART1_ADJ SET CODPROVE=@NEW WHERE CODPROVE=@OLD "
sConsulta = sConsulta & vbCrLf & "UPDATE ART2_ADJ SET CODPROVE=@NEW WHERE CODPROVE=@OLD "
sConsulta = sConsulta & vbCrLf & "UPDATE ART3_ADJ SET CODPROVE=@NEW WHERE CODPROVE=@OLD "
sConsulta = sConsulta & vbCrLf & "UPDATE ART4_ADJ SET CODPROVE=@NEW WHERE CODPROVE=@OLD "
sConsulta = sConsulta & vbCrLf & "UPDATE CON SET PROVE=@NEW WHERE PROVE=@OLD "
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_ADJ SET PROVE=@NEW WHERE PROVE=@OLD "
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_ADJREU SET PROVE=@NEW WHERE PROVE=@OLD "
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_OFE SET PROVE=@NEW WHERE PROVE=@OLD "
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_OFE_WEB SET PROVE=@NEW WHERE PROVE=@OLD "
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_OFE_ADJUN SET PROVE=@NEW WHERE PROVE=@OLD "
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_OFE SET PROVE=@NEW WHERE PROVE=@OLD "
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_OFE_WEB_ADJUN SET PROVE=@NEW WHERE PROVE=@OLD "
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_OFE_WEB SET PROVE=@NEW WHERE PROVE=@OLD "
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_PROVE SET PROVE=@NEW WHERE PROVE=@OLD "
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_PROVE_PET SET PROVE=@NEW WHERE PROVE=@OLD "
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_PROVE_POT SET PROVE=@NEW WHERE PROVE=@OLD "
sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_EQP SET PROVE=@NEW WHERE PROVE=@OLD "
sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_GMN1 SET PROVE=@NEW WHERE PROVE=@OLD "
sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_GMN2 SET PROVE=@NEW WHERE PROVE=@OLD "
sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_GMN3 SET PROVE=@NEW WHERE PROVE=@OLD "
sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_GMN4 SET PROVE=@NEW WHERE PROVE=@OLD "
sConsulta = sConsulta & vbCrLf & "UPDATE USU SET PROVE=@NEW WHERE PROVE=@OLD "
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_PROVE SET PROVE=@NEW WHERE PROVE=@OLD "
sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_ART4 SET PROVE=@NEW WHERE PROVE=@OLD"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART4 CHECK CONSTRAINT ALL "
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_PROVE CHECK CONSTRAINT ALL "
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE CHECK CONSTRAINT ALL "
sConsulta = sConsulta & vbCrLf & "ALTER TABLE CON CHECK CONSTRAINT ALL "
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART1_ADJ CHECK CONSTRAINT ALL "
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART2_ADJ CHECK CONSTRAINT ALL "
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART3_ADJ CHECK CONSTRAINT ALL "
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4_ADJ CHECK CONSTRAINT ALL "
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_ADJ CHECK CONSTRAINT ALL "
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_ADJREU CHECK CONSTRAINT ALL "
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_OFE CHECK CONSTRAINT ALL "
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_OFE_WEB CHECK CONSTRAINT ALL "
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_OFE_ADJUN CHECK CONSTRAINT ALL "
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_OFE CHECK CONSTRAINT ALL "
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_OFE_WEB_ADJUN CHECK CONSTRAINT ALL "
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_OFE_WEB CHECK CONSTRAINT ALL "
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_PROVE CHECK CONSTRAINT ALL "
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_PROVE_PET CHECK CONSTRAINT ALL "
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_PROVE_POT CHECK CONSTRAINT ALL "
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_EQP CHECK CONSTRAINT ALL "
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN1 CHECK CONSTRAINT ALL "
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN2 CHECK CONSTRAINT ALL "
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN3 CHECK CONSTRAINT ALL "
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN4 CHECK CONSTRAINT ALL "
sConsulta = sConsulta & vbCrLf & "ALTER TABLE USU CHECK CONSTRAINT ALL "
sConsulta = sConsulta & vbCrLf & "End "
sConsulta = sConsulta & vbCrLf & "Close C "
sConsulta = sConsulta & vbCrLf & "DEALLOCATE C "
sConsulta = sConsulta & vbCrLf & "End "
sConsulta = sConsulta & vbCrLf & "COMMIT TRANSACTION"

ModificarPROVE_COD_2_7_50_8 = sConsulta
End Function

Private Function ModificarDEST_COD_2_8_50_9() As String
Dim sConsulta As String

sConsulta = "CREATE PROCEDURE DEST_COD (@OLD VARCHAR(50),@NEW VARCHAR(50))  AS "
sConsulta = sConsulta & vbCrLf & "DECLARE @VAR_COD VARCHAR(50) "
sConsulta = sConsulta & vbCrLf & "SET XACT_ABORT ON "
sConsulta = sConsulta & vbCrLf & "BEGIN TRANSACTION "
sConsulta = sConsulta & vbCrLf & "BEGIN "
sConsulta = sConsulta & vbCrLf & "DECLARE C CURSOR FOR SELECT COD FROM DEST WHERE COD=@OLD "
sConsulta = sConsulta & vbCrLf & "OPEN C "
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM C INTO @VAR_COD "
sConsulta = sConsulta & vbCrLf & "IF @@FETCH_STATUS=0 "
sConsulta = sConsulta & vbCrLf & "BEGIN "
sConsulta = sConsulta & vbCrLf & "ALTER TABLE DEST NOCHECK CONSTRAINT ALL "
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM NOCHECK CONSTRAINT ALL "
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART1_ADJ NOCHECK CONSTRAINT ALL "
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART2_ADJ NOCHECK CONSTRAINT ALL "
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART3_ADJ NOCHECK CONSTRAINT ALL "
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4_ADJ NOCHECK CONSTRAINT ALL "
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PARGEN_DEF NOCHECK CONSTRAINT ALL "
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM NOCHECK CONSTRAINT ALL "
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES NOCHECK CONSTRAINT ALL "
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON1 NOCHECK CONSTRAINT ALL "
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON2 NOCHECK CONSTRAINT ALL "
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON3 NOCHECK CONSTRAINT ALL "
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON1 NOCHECK CONSTRAINT ALL "
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON2 NOCHECK CONSTRAINT ALL "
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON3 NOCHECK CONSTRAINT ALL "
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON4 NOCHECK CONSTRAINT ALL "
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY1 NOCHECK CONSTRAINT ALL "
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY2 NOCHECK CONSTRAINT ALL "
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY3 NOCHECK CONSTRAINT ALL "
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY4 NOCHECK CONSTRAINT ALL "
sConsulta = sConsulta & vbCrLf & "IF (SELECT DESTDEF FROM PARGEN_DEF) = @OLD  "
sConsulta = sConsulta & vbCrLf & "  UPDATE PARGEN_DEF SET DESTDEF=@NEW "
sConsulta = sConsulta & vbCrLf & "UPDATE DEST SET COD=@NEW WHERE COD=@OLD "
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM SET DEST=@NEW WHERE DEST=@OLD "
sConsulta = sConsulta & vbCrLf & "UPDATE ART1_ADJ SET DEST=@NEW WHERE DEST=@OLD "
sConsulta = sConsulta & vbCrLf & "UPDATE ART2_ADJ SET DEST=@NEW WHERE DEST=@OLD "
sConsulta = sConsulta & vbCrLf & "UPDATE ART3_ADJ SET DEST=@NEW WHERE DEST=@OLD "
sConsulta = sConsulta & vbCrLf & "UPDATE ART4_ADJ SET DEST=@NEW WHERE DEST=@OLD "
sConsulta = sConsulta & vbCrLf & "UPDATE PARGEN_DEF SET DESTDEF=@NEW WHERE DESTDEF=@OLD "
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM SET DEST=@NEW WHERE DEST=@OLD "
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES SET DEST=@NEW WHERE DEST=@OLD "
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_UON1 SET DEST=@NEW WHERE DEST=@OLD "
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_UON2 SET DEST=@NEW WHERE DEST=@OLD "
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_UON3 SET DEST=@NEW WHERE DEST=@OLD "
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON1 SET DEST=@NEW WHERE DEST=@OLD "
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON2 SET DEST=@NEW WHERE DEST=@OLD "
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON3 SET DEST=@NEW WHERE DEST=@OLD "
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON4 SET DEST=@NEW WHERE DEST=@OLD "
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY1 SET DEST=@NEW WHERE DEST=@OLD "
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY2 SET DEST=@NEW WHERE DEST=@OLD "
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY3 SET DEST=@NEW WHERE DEST=@OLD "
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY4 SET DEST=@NEW WHERE DEST=@OLD "
sConsulta = sConsulta & vbCrLf & "ALTER TABLE DEST CHECK CONSTRAINT ALL "
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM CHECK CONSTRAINT ALL "
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART1_ADJ CHECK CONSTRAINT ALL "
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART2_ADJ CHECK CONSTRAINT ALL "
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART3_ADJ CHECK CONSTRAINT ALL "
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4_ADJ CHECK CONSTRAINT ALL "
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PARGEN_DEF CHECK CONSTRAINT ALL "
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM CHECK CONSTRAINT ALL "
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES CHECK CONSTRAINT ALL "
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON1 CHECK CONSTRAINT ALL "
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON2 CHECK CONSTRAINT ALL "
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON3 CHECK CONSTRAINT ALL "
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON1 CHECK CONSTRAINT ALL "
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON2 CHECK CONSTRAINT ALL "
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON3 CHECK CONSTRAINT ALL "
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON4 CHECK CONSTRAINT ALL "
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY1 CHECK CONSTRAINT ALL "
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY2 CHECK CONSTRAINT ALL "
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY3 CHECK CONSTRAINT ALL "
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY4 CHECK CONSTRAINT ALL "
sConsulta = sConsulta & vbCrLf & "End "
sConsulta = sConsulta & vbCrLf & "Close C "
sConsulta = sConsulta & vbCrLf & "DEALLOCATE C "
sConsulta = sConsulta & vbCrLf & "End "
sConsulta = sConsulta & vbCrLf & "COMMIT TRANSACTION "

ModificarDEST_COD_2_8_50_9 = sConsulta
End Function


Private Function ModificarPER_COD_2_7_51_0() As String
    Dim sConsulta As String
    
    sConsulta = "CREATE PROCEDURE PER_COD (@OLD VARCHAR(50),@NEW VARCHAR(50))  AS"
    sConsulta = sConsulta & vbCrLf & "DECLARE @VAR_COD VARCHAR(50)"
    sConsulta = sConsulta & vbCrLf & "SET XACT_ABORT ON"
    sConsulta = sConsulta & vbCrLf & "BEGIN TRANSACTION"
    sConsulta = sConsulta & vbCrLf & "BEGIN"
    sConsulta = sConsulta & vbCrLf & "DECLARE C CURSOR FOR SELECT COD FROM PER WHERE COD=@OLD"
    sConsulta = sConsulta & vbCrLf & "OPEN C"
    sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM C INTO @VAR_COD"
    sConsulta = sConsulta & vbCrLf & "IF @@FETCH_STATUS=0"
    sConsulta = sConsulta & vbCrLf & "BEGIN"
    sConsulta = sConsulta & vbCrLf & "ALTER TABLE PER NOCHECK CONSTRAINT ALL"
    sConsulta = sConsulta & vbCrLf & "ALTER TABLE USU NOCHECK CONSTRAINT ALL"
    sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_PER NOCHECK CONSTRAINT ALL"
    sConsulta = sConsulta & vbCrLf & "ALTER TABLE FIJ NOCHECK CONSTRAINT ALL"
    sConsulta = sConsulta & vbCrLf & "UPDATE PER SET COD=@NEW WHERE COD=@OLD"
    sConsulta = sConsulta & vbCrLf & "UPDATE USU SET PER=@NEW WHERE PER=@OLD"
    sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_PER SET PER=@NEW WHERE PER=@OLD"
    sConsulta = sConsulta & vbCrLf & "UPDATE FIJ SET PER=@NEW WHERE PER=@OLD"
    sConsulta = sConsulta & vbCrLf & "ALTER TABLE PER CHECK CONSTRAINT ALL"
    sConsulta = sConsulta & vbCrLf & "ALTER TABLE USU CHECK CONSTRAINT ALL"
    sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_PER CHECK CONSTRAINT ALL"
    sConsulta = sConsulta & vbCrLf & "ALTER TABLE FIJ CHECK CONSTRAINT ALL"
    sConsulta = sConsulta & vbCrLf & "End"
    sConsulta = sConsulta & vbCrLf & "Close C"
    sConsulta = sConsulta & vbCrLf & "DEALLOCATE C"
    sConsulta = sConsulta & vbCrLf & "End"
    sConsulta = sConsulta & vbCrLf & "COMMIT TRANSACTION"
    
    ModificarPER_COD_2_7_51_0 = sConsulta
End Function


Private Function ActualizarBLOQUEO_PROCE_2_7_51_0() As Boolean
    Dim rdores As rdoResultset
    Dim sConsulta As String
    Dim sSQL As String
    
    On Error GoTo error
    
    'Obtenemos los procesos de PROCE que ya han sido validados en la apertura
    'y para los que no hay registro en BLOQUEO_PROCE
    
'    sSQL = "SELECT ANYO,GMN1,COD FROM PROCE WHERE EST>=3 AND COD NOT IN"
'    sSQL = sSQL & " (SELECT PROCE FROM BLOQUEO_PROCE WHERE ANYO=PROCE.ANYO AND GMN1=PROCE.GMN1)"
'    Set rdores = gRDOCon.OpenResultset(sSQL, rdOpenForwardOnly, rdConcurReadOnly, rdExecDirect)
'
'    While Not rdores.EOF
'        sConsulta = "INSERT INTO BLOQUEO_PROCE (ANYO,GMN1,PROCE) VALUES (" & rdores("ANYO") & ",'" & rdores("GMN1") & "'," & rdores("COD") & ")"
'        gRDOCon.Execute sConsulta, rdExecDirect
'
'        rdores.MoveNext
'    Wend
'
'
'    rdores.Close
'    Set rdores = Nothing
    sSQL = "INSERT INTO BLOQUEO_PROCE (ANYO,GMN1,PROCE) SELECT P.ANYO,P.GMN1,P.COD FROM PROCE P WHERE EST>=3  and not exists (SELECT * FROM BLOQUEO_PROCE B WHERE B.ANYO=P.ANYO AND B.GMN1=P.GMN1 AND B.PROCE=P.COD)"
    gRDOCon.Execute sSQL
    
    
    ActualizarBLOQUEO_PROCE_2_7_51_0 = True
    Exit Function
    
error:
    ActualizarBLOQUEO_PROCE_2_7_51_0 = False
    If Not rdores Is Nothing Then
        rdores.Close
        Set rdores = Nothing
    End If
End Function

Private Function ModificarNUM_PROCE_PUB_2_7_51_0() As String
    Dim sConsulta As String
    
    sConsulta = "CREATE PROCEDURE  NUM_PROCE_PUB (@PROVE VARCHAR(50),@NUM INT OUTPUT,@NUMNUE INT OUTPUT,@NUMAREV INT OUTPUT)  AS"
    sConsulta = sConsulta & vbCrLf & "BEGIN"
    sConsulta = sConsulta & vbCrLf & ""
    sConsulta = sConsulta & vbCrLf & "SET @NUM = (SELECT COUNT(*) FROM PROCE_PROVE INNER JOIN PROCE ON PROCE_PROVE.ANYO=PROCE.ANYO"
    sConsulta = sConsulta & vbCrLf & " AND PROCE_PROVE.GMN1=PROCE.GMN1 AND PROCE_PROVE.PROCE=PROCE.COD AND PROCE_PROVE.PROVE=@PROVE"
    sConsulta = sConsulta & vbCrLf & " AND PROCE_PROVE.PUB=1 AND PROCE.EST<=10 )"
    sConsulta = sConsulta & vbCrLf & ""
    sConsulta = sConsulta & vbCrLf & ""
    sConsulta = sConsulta & vbCrLf & "SET @NUMNUE = (SELECT COUNT(*) FROM PROCE_PROVE INNER JOIN PROCE ON PROCE_PROVE.ANYO=PROCE.ANYO"
    sConsulta = sConsulta & vbCrLf & " AND PROCE_PROVE.GMN1=PROCE.GMN1 AND PROCE_PROVE.PROCE=PROCE.COD AND PROCE_PROVE.PROVE=@PROVE"
    sConsulta = sConsulta & vbCrLf & " AND PROCE_PROVE.PUB=1 AND PROCE.EST>=5 AND PROCE.EST<=6)"
    sConsulta = sConsulta & vbCrLf & ""
    sConsulta = sConsulta & vbCrLf & ""
    sConsulta = sConsulta & vbCrLf & "SET @NUMAREV = (SELECT COUNT(*)  FROM PROCE_PROVE INNER JOIN PROCE ON PROCE_PROVE.ANYO=PROCE.ANYO"
    sConsulta = sConsulta & vbCrLf & "AND PROCE_PROVE.GMN1=PROCE.GMN1 AND PROCE_PROVE.PROCE=PROCE.COD AND PROCE_PROVE.PROVE=@PROVE"
    sConsulta = sConsulta & vbCrLf & "AND PROCE_PROVE.PUB=1 AND PROCE_PROVE.OBJNUEVOS=1 AND PROCE.SUBASTA=0 AND PROCE.EST<=10)"
    sConsulta = sConsulta & vbCrLf & ""
    sConsulta = sConsulta & vbCrLf & "Return"
    sConsulta = sConsulta & vbCrLf & "End"

    ModificarNUM_PROCE_PUB_2_7_51_0 = sConsulta
End Function

Private Function ModificarSP_CIERRE_2_7_50() As String
Dim sConsulta As String

sConsulta = "CREATE PROCEDURE SP_CIERRE(@NEM TINYINT,@ANYO SMALLINT,@GMN1 VARCHAR(20),@PROCE INTEGER, @RES INTEGER OUTPUT) AS"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "EXECUTE SP_AR_PROCE @ANYO,@GMN1,@PROCE,@RES"
sConsulta = sConsulta & vbCrLf & "IF @RES<>0 RETURN"
sConsulta = sConsulta & vbCrLf & "EXECUTE SP_AR_ITEM @NEM,@ANYO,@GMN1,@PROCE,@RES"
sConsulta = sConsulta & vbCrLf & "IF @RES<>0 RETURN"
sConsulta = sConsulta & vbCrLf & "EXECUTE SP_AR_ITEM_MES @NEM,@ANYO,@GMN1,@PROCE,@RES"
sConsulta = sConsulta & vbCrLf & "IF @RES<>0 RETURN"
sConsulta = sConsulta & vbCrLf & "EXECUTE SP_AR_ITEM_MES_UON1 @NEM,@ANYO,@GMN1,@PROCE,@RES"
sConsulta = sConsulta & vbCrLf & "IF @RES<>0 RETURN"
sConsulta = sConsulta & vbCrLf & "EXECUTE SP_AR_ITEM_MES_UON2 @NEM,@ANYO,@GMN1,@PROCE,@RES"
sConsulta = sConsulta & vbCrLf & "IF @RES<>0 RETURN"
sConsulta = sConsulta & vbCrLf & "EXECUTE SP_AR_ITEM_MES_UON3 @NEM,@ANYO,@GMN1,@PROCE,@RES"
sConsulta = sConsulta & vbCrLf & "IF @RES<>0 RETURN"
sConsulta = sConsulta & vbCrLf & "EXECUTE SP_AR_ITEM_MES_PARCON1 @ANYO,@GMN1,@PROCE,@RES"
sConsulta = sConsulta & vbCrLf & "IF @RES<>0 RETURN"
sConsulta = sConsulta & vbCrLf & "EXECUTE SP_AR_ITEM_MES_PARCON2 @ANYO,@GMN1,@PROCE,@RES"
sConsulta = sConsulta & vbCrLf & "IF @RES<>0 RETURN"
sConsulta = sConsulta & vbCrLf & "EXECUTE SP_AR_ITEM_MES_PARCON3 @ANYO,@GMN1,@PROCE,@RES"
sConsulta = sConsulta & vbCrLf & "IF @RES<>0 RETURN"
sConsulta = sConsulta & vbCrLf & "EXECUTE SP_AR_ITEM_MES_PARCON4 @ANYO,@GMN1,@PROCE,@RES"
sConsulta = sConsulta & vbCrLf & "IF @RES<>0 RETURN"
sConsulta = sConsulta & vbCrLf & "EXECUTE SP_AR_ITEM_MES_PROY1 @ANYO,@GMN1,@PROCE,@RES"
sConsulta = sConsulta & vbCrLf & "IF @RES<>0 RETURN"
sConsulta = sConsulta & vbCrLf & "EXECUTE SP_AR_ITEM_MES_PROY2 @ANYO,@GMN1,@PROCE,@RES"
sConsulta = sConsulta & vbCrLf & "IF @RES<>0 RETURN"
sConsulta = sConsulta & vbCrLf & "EXECUTE SP_AR_ITEM_MES_PROY3 @ANYO,@GMN1,@PROCE,@RES"
sConsulta = sConsulta & vbCrLf & "IF @RES<>0 RETURN"
sConsulta = sConsulta & vbCrLf & "EXECUTE SP_AR_ITEM_MES_PROY4 @ANYO,@GMN1,@PROCE,@RES"
sConsulta = sConsulta & vbCrLf & "IF @RES<>0 RETURN"
sConsulta = sConsulta & vbCrLf & "EXECUTE SP_AR_ITEM_PROVE @ANYO,@GMN1,@PROCE,@RES"
sConsulta = sConsulta & vbCrLf & "IF @RES<>0 RETURN"
sConsulta = sConsulta & vbCrLf & "EXECUTE SP_AR_ITEM_MES_PRESCON3_1 @ANYO,@GMN1,@PROCE,@RES"
sConsulta = sConsulta & vbCrLf & "IF @RES<>0 RETURN"
sConsulta = sConsulta & vbCrLf & "EXECUTE SP_AR_ITEM_MES_PRESCON3_2 @ANYO,@GMN1,@PROCE,@RES"
sConsulta = sConsulta & vbCrLf & "IF @RES<>0 RETURN"
sConsulta = sConsulta & vbCrLf & "EXECUTE SP_AR_ITEM_MES_PRESCON3_3 @ANYO,@GMN1,@PROCE,@RES"
sConsulta = sConsulta & vbCrLf & "IF @RES<>0 RETURN"
sConsulta = sConsulta & vbCrLf & "EXECUTE SP_AR_ITEM_MES_PRESCON3_4 @ANYO,@GMN1,@PROCE,@RES"
sConsulta = sConsulta & vbCrLf & "IF @RES<>0 RETURN"
sConsulta = sConsulta & vbCrLf & "EXECUTE SP_AR_ITEM_MES_PRESCON4_1 @ANYO,@GMN1,@PROCE,@RES"
sConsulta = sConsulta & vbCrLf & "IF @RES<>0 RETURN"
sConsulta = sConsulta & vbCrLf & "EXECUTE SP_AR_ITEM_MES_PRESCON4_2 @ANYO,@GMN1,@PROCE,@RES"
sConsulta = sConsulta & vbCrLf & "IF @RES<>0 RETURN"
sConsulta = sConsulta & vbCrLf & "EXECUTE SP_AR_ITEM_MES_PRESCON4_3 @ANYO,@GMN1,@PROCE,@RES"
sConsulta = sConsulta & vbCrLf & "IF @RES<>0 RETURN"
sConsulta = sConsulta & vbCrLf & "EXECUTE SP_AR_ITEM_MES_PRESCON4_4 @ANYO,@GMN1,@PROCE,@RES"
sConsulta = sConsulta & vbCrLf & "IF @RES<>0 RETURN"
sConsulta = sConsulta & vbCrLf & "EXECUTE SP_AR_ITEM_PRESCON3 @ANYO,@GMN1,@PROCE,@RES"
sConsulta = sConsulta & vbCrLf & "IF @RES<>0 RETURN"
sConsulta = sConsulta & vbCrLf & "EXECUTE SP_AR_ITEM_PRESCON4 @ANYO,@GMN1,@PROCE,@RES"
sConsulta = sConsulta & vbCrLf & "Return"
sConsulta = sConsulta & vbCrLf & "End"

ModificarSP_CIERRE_2_7_50 = sConsulta
End Function


