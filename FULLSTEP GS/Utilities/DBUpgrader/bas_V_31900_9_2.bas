Attribute VB_Name = "bas_V_31900_9_2"
Option Explicit


Public Function CodigoDeActualizacion31900_09_2140_A31900_09_2141() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim oADOConnection As ADODB.Connection
    Dim sConsulta As String

    On Error GoTo Error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Actualiza Datos"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    frmProgreso.ProgressBar1.Refresh
    V_31900_9_Datos_2141

    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Actualiza Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    frmProgreso.ProgressBar1.Refresh
    V_31900_9_Storeds_2141

    sConsulta = "UPDATE VERSION SET NUM ='3.00.21.041'" & ", FECHA=CONVERT(datetime,'" & Format(Now, "mm-dd-yyyy hh:mm:ss") & "',110), PRODVER='31900.9'"
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False

    CodigoDeActualizacion31900_09_2140_A31900_09_2141 = True
    Exit Function

Error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If

    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    CodigoDeActualizacion31900_09_2140_A31900_09_2141 = False
End Function



Private Sub V_31900_9_Datos_2141()
    Dim sConsulta As String
sConsulta = "UPDATE LINEAS_PEDIDO_CD_IMPUESTO" & vbCrLf
sConsulta = sConsulta & "SET IMPUESTO = T.IMPP_ID" & vbCrLf
sConsulta = sConsulta & "FROM (SELECT IMP.ID,MAX(IMPPP.ID) IMPP_ID" & vbCrLf
sConsulta = sConsulta & "from  IMPUESTO_PP_VALOR IMPPP WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN IMPUESTO_PAIPROVI IMPPAI WITH(NOLOCK) ON IMPPAI.ID = IMPPP.IMPUESTO_PAIPROVI" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN IMPUESTO IMP WITH(NOLOCK) ON IMPPAI.IMPUESTO = IMP.ID" & vbCrLf
sConsulta = sConsulta & "GROUP BY IMP.ID) T" & vbCrLf
sConsulta = sConsulta & "WHERE LINEAS_PEDIDO_CD_IMPUESTO.IMPUESTO=T.ID" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "UPDATE LINEAS_PEDIDO_IMPUESTO" & vbCrLf
sConsulta = sConsulta & "SET IMPUESTO = T.IMPP_ID" & vbCrLf
sConsulta = sConsulta & "FROM (SELECT IMP.ID,MAX(IMPPP.ID) IMPP_ID" & vbCrLf
sConsulta = sConsulta & "from  IMPUESTO_PP_VALOR IMPPP WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN IMPUESTO_PAIPROVI IMPPAI WITH(NOLOCK) ON IMPPAI.ID = IMPPP.IMPUESTO_PAIPROVI" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN IMPUESTO IMP WITH(NOLOCK) ON IMPPAI.IMPUESTO = IMP.ID" & vbCrLf
sConsulta = sConsulta & "GROUP BY IMP.ID) T" & vbCrLf
sConsulta = sConsulta & "WHERE LINEAS_PEDIDO_IMPUESTO.IMPUESTO=T.ID" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "UPDATE ORDEN_ENTREGA_CD_IMPUESTO" & vbCrLf
sConsulta = sConsulta & "SET IMPUESTO = T.IMPP_ID" & vbCrLf
sConsulta = sConsulta & "FROM (SELECT IMP.ID,MAX(IMPPP.ID) IMPP_ID" & vbCrLf
sConsulta = sConsulta & "from  IMPUESTO_PP_VALOR IMPPP WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN IMPUESTO_PAIPROVI IMPPAI WITH(NOLOCK) ON IMPPAI.ID = IMPPP.IMPUESTO_PAIPROVI" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN IMPUESTO IMP WITH(NOLOCK) ON IMPPAI.IMPUESTO = IMP.ID" & vbCrLf
sConsulta = sConsulta & "GROUP BY IMP.ID) T" & vbCrLf
sConsulta = sConsulta & "WHERE ORDEN_ENTREGA_CD_IMPUESTO.IMPUESTO=T.ID" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

End Sub




Private Sub V_31900_9_Storeds_2141()
    Dim sConsulta As String

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_SAVE_INSTANCIA_EN_FILTRO]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_SAVE_INSTANCIA_EN_FILTRO]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_SAVE_INSTANCIA_EN_FILTRO]" & vbCrLf
sConsulta = sConsulta & "@INSTANCIA INT," & vbCrLf
sConsulta = sConsulta & "@INSERT TINYINT," & vbCrLf
sConsulta = sConsulta & "@QA TINYINT=0," & vbCrLf
sConsulta = sConsulta & "@CONTRATO TINYINT=0" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @INSTANCIA_L INT" & vbCrLf
sConsulta = sConsulta & "SET @INSTANCIA_L=@INSTANCIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Obtenemos el nombre de las tablas din?micas del filtro (falta a?adir el prefijo del idioma)." & vbCrLf
sConsulta = sConsulta & "DECLARE @TABLA NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "IF @QA=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @CONTRATO=0" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT @TABLA=F.NOM_TABLA FROM INSTANCIA I WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_GRUPO G WITH (NOLOCK) ON I.ID=G.INSTANCIA" & vbCrLf
sConsulta = sConsulta & "INNER JOIN FORM_GRUPO G2 WITH (NOLOCK) ON G.GRUPO_ORIGEN=G2.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_FILTROS F WITH (NOLOCK) ON G2.FORMULARIO=F.FORMULARIO" & vbCrLf
sConsulta = sConsulta & "WHERE I.ID = @INSTANCIA_L" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT @TABLA=F.NOM_TABLA FROM INSTANCIA I WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_GRUPO G WITH (NOLOCK) ON I.ID=G.INSTANCIA" & vbCrLf
sConsulta = sConsulta & "INNER JOIN FORM_GRUPO G2 WITH (NOLOCK) ON G.GRUPO_ORIGEN=G2.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_CONTR_FILTROS F WITH (NOLOCK) ON G2.FORMULARIO=F.FORMULARIO" & vbCrLf
sConsulta = sConsulta & "WHERE I.ID = @INSTANCIA_L" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT @TABLA=F.NOM_TABLA FROM INSTANCIA I WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_GRUPO G WITH (NOLOCK) ON I.ID=G.INSTANCIA" & vbCrLf
sConsulta = sConsulta & "INNER JOIN FORM_GRUPO G2 WITH (NOLOCK) ON G.GRUPO_ORIGEN=G2.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN QA_FILTROS F WITH (NOLOCK) ON G2.FORMULARIO=F.FORMULARIO" & vbCrLf
sConsulta = sConsulta & "WHERE I.ID = @INSTANCIA_L" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Obtenemos las longitudes de los materiales de la tabla DIC" & vbCrLf
sConsulta = sConsulta & "DECLARE @LEN_GMN1 INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @LEN_GMN2 INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @LEN_GMN3 INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @LEN_GMN4 INT" & vbCrLf
sConsulta = sConsulta & "SELECT @LEN_GMN1=DIC.LONGITUD, @LEN_GMN2=DIC2.LONGITUD,@LEN_GMN3=DIC3.LONGITUD, @LEN_GMN4=DIC4.LONGITUD FROM DIC WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN DIC DIC2 WITH (NOLOCK) ON DIC2.NOMBRE='GMN2'" & vbCrLf
sConsulta = sConsulta & "INNER JOIN DIC DIC3 WITH (NOLOCK) ON DIC3.NOMBRE='GMN3'" & vbCrLf
sConsulta = sConsulta & "INNER JOIN DIC DIC4 WITH (NOLOCK) ON DIC4.NOMBRE='GMN4'" & vbCrLf
sConsulta = sConsulta & "WHERE DIC.NOMBRE = 'GMN1'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Guardamos los IDs en una tabla temporal" & vbCrLf
sConsulta = sConsulta & "--Es lo ?nico que cambia de los 3 casos (QA, Solicitudes y Contratos)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLINSERT NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE #G1 (ID_CAMPO INT)" & vbCrLf
sConsulta = sConsulta & "SET @SQLINSERT = N'INSERT INTO #G1 SELECT ID_CAMPO FROM '" & vbCrLf
sConsulta = sConsulta & "IF @QA=0" & vbCrLf
sConsulta = sConsulta & "IF @CONTRATO = 0" & vbCrLf
sConsulta = sConsulta & "SET @SQLINSERT = @SQLINSERT + N'UPD_VISORES_PM WITH(NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQLINSERT = @SQLINSERT + N'UPD_VISORES_CONTR WITH(NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQLINSERT = @SQLINSERT + N'UPD_VISORES_QA WITH(NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "SET @SQLINSERT = @SQLINSERT + N'WHERE (ACCION=''I'' OR ACCION=''A'') AND (NOM_TABLA=@TABLA OR NOM_TABLA=@TABLA+''_DESGLOSE'') AND OK=0'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLINSERT, N'@TABLA NVARCHAR(100)', @TABLA=@TABLA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @LINEA INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @LINEA_ANT INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @CAMPO_ORIGEN INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @SUBTIPO INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @TIPO_CAMPO_GS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @VALOR_NUM FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @VALOR_TEXT NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @VALOR_FEC DATETIME" & vbCrLf
sConsulta = sConsulta & "DECLARE @VALOR_BOOL TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @VALOR_TEXT_DEN NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @VALOR_TEXT_COD NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @INTRO TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_ATRIB_GS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @TABLA_EXTERNA INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @CAMPO_DEF INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL_COD NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLCAMPOSINSERT NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @EJECUTARSQL TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @CAMPO_PADRE_DESGLOSE INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @CAMPO_PADRE_DESGLOSE_ANT INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @DENOMINACION NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @CAMPOTRATADO TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @EMPIEZAIDIOMA INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ACABADENOMINACION INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @EMPIEZADENOMINACION INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @SEPARADOR INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRESUPUESTOS NVARCHAR(400)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PORCENTAJE NVARCHAR(400)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SEPARADOR_DEN INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @SEPARADOR_PORCENTAJE INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDIOMA AS NVARCHAR(400)" & vbCrLf
sConsulta = sConsulta & "DECLARE curIDIOMAS CURSOR LOCAL FOR" & vbCrLf
sConsulta = sConsulta & "SELECT COD FROM IDIOMAS WITH(NOLOCK) WHERE APLICACION=1" & vbCrLf
sConsulta = sConsulta & "OPEN curIDIOMAS" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curIDIOMAS INTO @IDIOMA" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SQL=''" & vbCrLf
sConsulta = sConsulta & "SET @SQLCAMPOSINSERT=''" & vbCrLf
sConsulta = sConsulta & "SET @EJECUTARSQL=0" & vbCrLf
sConsulta = sConsulta & "SET @LINEA_ANT=1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @INSERT = 1 AND @CONTRATO = 1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'INSERT INTO ' + @IDIOMA + N'_CONTR_FILTRO1 VALUES (@INSTANCIA_L)'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA_L INT', @INSTANCIA_L=@INSTANCIA_L" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @INSERT = 1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SQLCAMPOSINSERT=N'INSERT INTO '+@IDIOMA+N'_'+@TABLA+N' (INSTANCIA,'" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N' VALUES (@INSTANCIA_L,'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=N' UPDATE ' + @IDIOMA + N'_' + @TABLA  + N' SET '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE curSOLICITUDES_CAMPOS CURSOR LOCAL FOR" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT CD.CAMPO_ORIGEN, CD.SUBTIPO, CD.TIPO_CAMPO_GS, C.VALOR_TEXT, C.VALOR_NUM," & vbCrLf
sConsulta = sConsulta & "C.VALOR_FEC, C.VALOR_BOOL, C.VALOR_TEXT_COD, C.VALOR_TEXT_DEN, CD.INTRO, CD.ID_ATRIB_GS," & vbCrLf
sConsulta = sConsulta & "CD.TABLA_EXTERNA, C.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "FROM COPIA_CAMPO_DEF CD WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_CAMPO C WITH(NOLOCK) ON CD.ID=C.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "INNER JOIN INSTANCIA I WITH(NOLOCK) ON C.INSTANCIA=I.ID AND C.NUM_VERSION=I.ULT_VERSION" & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_GRUPO G WITH (NOLOCK) ON I.ID=G.INSTANCIA" & vbCrLf
sConsulta = sConsulta & "INNER JOIN FORM_GRUPO G2 WITH (NOLOCK) ON G.GRUPO_ORIGEN=G2.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN FORMULARIO F WITH(NOLOCK) ON F.ID=G2.FORMULARIO" & vbCrLf
sConsulta = sConsulta & "INNER JOIN FORM_GRUPO FG WITH (NOLOCK) ON F.ID=FG.FORMULARIO" & vbCrLf
sConsulta = sConsulta & "INNER JOIN FORM_CAMPO FC WITH (NOLOCK) ON FG.ID = FC.GRUPO" & vbCrLf
sConsulta = sConsulta & "WHERE CD.ES_SUBCAMPO=0 AND CD.SUBTIPO<>9 AND (CD.TIPO_CAMPO_GS<>106 OR CD.TIPO_CAMPO_GS IS NULL)" & vbCrLf
sConsulta = sConsulta & "AND CD.CAMPO_ORIGEN = FC.ID AND (CD.SUBTIPO=FC.SUBTIPO" & vbCrLf
sConsulta = sConsulta & "OR (CD.SUBTIPO=5 AND FC.SUBTIPO IN (6,7)) OR (CD.SUBTIPO=6 AND FC.SUBTIPO=7))  --si la definici?n del campo ha cambiado de texto corto a texto medio o largo, o de texto medio a texto largo" & vbCrLf
sConsulta = sConsulta & "AND CD.CAMPO_ORIGEN NOT IN (SELECT ID_CAMPO FROM #G1)  -- campos que se han a?adido en el formulario pero no se ha hecho todavia ALTER en las tablas de filtros (BatchUpdates)" & vbCrLf
sConsulta = sConsulta & "AND I.ID=@INSTANCIA_L" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "OPEN curSOLICITUDES_CAMPOS" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curSOLICITUDES_CAMPOS INTO @CAMPO_ORIGEN, @SUBTIPO, @TIPO_CAMPO_GS, @VALOR_TEXT, @VALOR_NUM, @VALOR_FEC, @VALOR_BOOL, @VALOR_TEXT_COD, @VALOR_TEXT_DEN, @INTRO, @ID_ATRIB_GS,@TABLA_EXTERNA, @CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @EJECUTARSQL=1" & vbCrLf
sConsulta = sConsulta & "--Reemplazamos las comillas simples en valor_text y valor_text_den" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @VALOR_TEXT = SUBSTRING(@VALOR_TEXT,1,100)" & vbCrLf
sConsulta = sConsulta & "SET @VALOR_TEXT = REPLACE(@VALOR_TEXT,'''','''''')" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT_DEN IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SET @VALOR_TEXT_DEN = REPLACE(@VALOR_TEXT_DEN,'''','''''')" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "   IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "       SET @SQLCAMPOSINSERT = @SQLCAMPOSINSERT + 'C_' + CONVERT(NVARCHAR,@CAMPO_ORIGEN)  + ','" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "       SET @SQLCAMPOSINSERT = @SQLCAMPOSINSERT + N'C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N','" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = @SQL + 'C_' + CONVERT(NVARCHAR,@CAMPO_ORIGEN) + '='" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = @SQL +N'C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'='" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "----------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @SUBTIPO = 3 -- tipo fecha" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_FEC IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'CONVERT(DATETIME,''' + CONVERT(VARCHAR,@VALOR_FEC) + ''')'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @SUBTIPO = 4 -- tipo booleano" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_BOOL IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + CONVERT(NVARCHAR,@VALOR_BOOL)" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @SUBTIPO = 8 -- tipo archivo" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'''' + @VALOR_TEXT + N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @TIPO_CAMPO_GS IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @TABLA_EXTERNA IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @SUBTIPO=2 -- tipo num?rico" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_NUM IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + CONVERT(NVARCHAR,@VALOR_NUM)" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE    -- tipo texto o tipo lista texto" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1 AND @QA=0 AND @CONTRATO=0" & vbCrLf
sConsulta = sConsulta & "   SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "IF @INTRO=1 AND @VALOR_NUM IS NOT NULL --tipo lista texto" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @ID_ATRIB_GS IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @IDIOMA = 'SPA'" & vbCrLf
sConsulta = sConsulta & "SELECT @DENOMINACION=VALOR_TEXT_SPA" & vbCrLf
sConsulta = sConsulta & "FROM COPIA_CAMPO_VALOR_LISTA WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE CAMPO_DEF=CONVERT(VARCHAR,@CAMPO_DEF) AND ORDEN=CONVERT(VARCHAR,@VALOR_NUM)" & vbCrLf
sConsulta = sConsulta & "IF @IDIOMA = 'ENG'" & vbCrLf
sConsulta = sConsulta & "SELECT @DENOMINACION=VALOR_TEXT_ENG" & vbCrLf
sConsulta = sConsulta & "FROM COPIA_CAMPO_VALOR_LISTA WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE CAMPO_DEF=CONVERT(VARCHAR,@CAMPO_DEF) AND ORDEN=CONVERT(VARCHAR,@VALOR_NUM)" & vbCrLf
sConsulta = sConsulta & "IF @IDIOMA = 'GER'" & vbCrLf
sConsulta = sConsulta & "SELECT @DENOMINACION=VALOR_TEXT_GER" & vbCrLf
sConsulta = sConsulta & "FROM COPIA_CAMPO_VALOR_LISTA WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE CAMPO_DEF = CONVERT(VARCHAR,@CAMPO_DEF) AND ORDEN=CONVERT(VARCHAR,@VALOR_NUM)" & vbCrLf
sConsulta = sConsulta & "IF @IDIOMA = 'FRA'" & vbCrLf
sConsulta = sConsulta & "SELECT @DENOMINACION=VALOR_TEXT_FRA" & vbCrLf
sConsulta = sConsulta & "FROM COPIA_CAMPO_VALOR_LISTA WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE CAMPO_DEF = CONVERT(VARCHAR,@CAMPO_DEF) AND ORDEN=CONVERT(VARCHAR,@VALOR_NUM)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @DENOMINACION IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + '''' + replace(@DENOMINACION,'''','''''') + ''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+REPLACE(@DENOMINACION,'''','''''')+''''+N',CONVERT(NVARCHAR,'+CONVERT(NVARCHAR,@VALOR_NUM)+N')'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+REPLACE(@DENOMINACION,'''','''''')+''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=CONVERT(NVARCHAR,'+CONVERT(NVARCHAR,@VALOR_NUM)+N')'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + N'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + N'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + '''' + @VALOR_TEXT + ''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + N''''+@VALOR_TEXT+N''''+N','+N''''+CONVERT(NVARCHAR,@VALOR_NUM)+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + N''''+@VALOR_TEXT+N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+ CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N''''+CONVERT(NVARCHAR,@VALOR_NUM)+N''''" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE -- tipo texto" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + '''' + @VALOR_TEXT + ''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + N''''+@VALOR_TEXT+N''''+N','+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + N''''+@VALOR_TEXT+N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+ CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + N'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @SUBTIPO=2 -- tipo num?rico" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_NUM IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + CONVERT(NVARCHAR,@VALOR_NUM)" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE    -- tipo texto o tipo lista texto" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1 AND @QA=0 AND @CONTRATO=0" & vbCrLf
sConsulta = sConsulta & "   SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT_DEN IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + '''' + @VALOR_TEXT_DEN + ''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+@VALOR_TEXT_DEN+N''''+N','+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+@VALOR_TEXT_DEN+N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_' +CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE -- campo del sistema" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @CAMPOTRATADO = 0" & vbCrLf
sConsulta = sConsulta & "---------------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "-- Formas de pago, Monedas, Familia de materiales, Unidades, Destinos, Pa?ses y Provincias" & vbCrLf
sConsulta = sConsulta & "-- FORMATO: <Idioma>|<Texto>#<Idioma>|<Texto>" & vbCrLf
sConsulta = sConsulta & "-- FORMATO MATERIALES: cod. ultimo nivel (VALOR_TEXT) +  <Idioma>|<Texto>#<Idioma>|<Texto>" & vbCrLf
sConsulta = sConsulta & "------------------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "IF @TIPO_CAMPO_GS = 101 OR @TIPO_CAMPO_GS = 102 OR @TIPO_CAMPO_GS = 103 OR @TIPO_CAMPO_GS = 105 OR @TIPO_CAMPO_GS = 109 OR @TIPO_CAMPO_GS = 107 OR @TIPO_CAMPO_GS = 108" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1 AND @QA=0 AND @CONTRATO=0" & vbCrLf
sConsulta = sConsulta & "   SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+ N'_COD,'" & vbCrLf
sConsulta = sConsulta & "SET @CAMPOTRATADO = 1" & vbCrLf
sConsulta = sConsulta & "-- Materiales" & vbCrLf
sConsulta = sConsulta & "IF @TIPO_CAMPO_GS = 103" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "-- Al hacer Len de valor_text me est? quitando los espacios en blanco de la derecha" & vbCrLf
sConsulta = sConsulta & "IF LEN(@VALOR_TEXT + '#')-1 = @LEN_GMN1 + @LEN_GMN2 + @LEN_GMN3 + @LEN_GMN4" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'''' + RTRIM(RIGHT(@VALOR_TEXT,@LEN_GMN4)) + N' - '" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF LEN(@VALOR_TEXT + '#')-1 = @LEN_GMN1 + @LEN_GMN2 + @LEN_GMN3" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'''' + RTRIM(RIGHT(@VALOR_TEXT,@LEN_GMN3)) + N' - '" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF LEN(@VALOR_TEXT + '#')-1 = @LEN_GMN1 + @LEN_GMN2" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'''' + RTRIM(RIGHT(@VALOR_TEXT,@LEN_GMN2)) + N' - '" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'''' + RTRIM(RIGHT(@VALOR_TEXT,@LEN_GMN1)) + N' - '" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- Todos" & vbCrLf
sConsulta = sConsulta & "SET @EMPIEZAIDIOMA=CHARINDEX(@IDIOMA + '|',@VALOR_TEXT_DEN)" & vbCrLf
sConsulta = sConsulta & "IF (@EMPIEZAIDIOMA) > 0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @ACABADENOMINACION = CHARINDEX('#',@VALOR_TEXT_DEN,@EMPIEZAIDIOMA)" & vbCrLf
sConsulta = sConsulta & "SET @EMPIEZADENOMINACION = CHARINDEX('|',@VALOR_TEXT_DEN,@EMPIEZAIDIOMA)+1" & vbCrLf
sConsulta = sConsulta & "IF @TIPO_CAMPO_GS <> 103" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+''''" & vbCrLf
sConsulta = sConsulta & "IF @ACABADENOMINACION>0" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+SUBSTRING(@VALOR_TEXT_DEN,@EMPIEZADENOMINACION,@ACABADENOMINACION-@EMPIEZADENOMINACION) + ''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+RIGHT(@VALOR_TEXT_DEN,LEN(@VALOR_TEXT_DEN)-@EMPIEZADENOMINACION+1) + ''''" & vbCrLf
sConsulta = sConsulta & "IF @QA=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N','+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N',C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT IS NOT NULL AND @TIPO_CAMPO_GS = 103" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+N','+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "-- Proveedores, Departamentos, Organizaciones de Compras, Centros,Proveedor ERP" & vbCrLf
sConsulta = sConsulta & "-- FORMATO: C?digo (VALOR_TEXT) - Denominaci?n (VALOR_TEXT_DEN)" & vbCrLf
sConsulta = sConsulta & "--------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "IF @TIPO_CAMPO_GS = 11 OR @TIPO_CAMPO_GS = 100  OR @TIPO_CAMPO_GS = 122 OR @TIPO_CAMPO_GS = 123 OR @TIPO_CAMPO_GS = 124 OR @TIPO_CAMPO_GS = 143" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @CAMPOTRATADO = 1" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1 AND @QA=0 AND @CONTRATO=0" & vbCrLf
sConsulta = sConsulta & "   SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + '''' + @VALOR_TEXT + ' - ' + @VALOR_TEXT_DEN + ''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+@VALOR_TEXT+N' - '+@VALOR_TEXT_DEN+N''''+N','+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+@VALOR_TEXT+N' - '+@VALOR_TEXT_DEN+N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "-- Centros de coste, Partidas presupuestarias, Activos" & vbCrLf
sConsulta = sConsulta & "-- FORMATO: cod. ultimo nivel (VALOR_TEXT) +  <Idioma>|<Texto>#<Idioma>|<Texto>" & vbCrLf
sConsulta = sConsulta & "--------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "IF @TIPO_CAMPO_GS = 129 OR @TIPO_CAMPO_GS = 130 OR @TIPO_CAMPO_GS = 131 OR @TIPO_CAMPO_GS=132" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @CAMPOTRATADO=1" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1 AND @QA=0 AND @CONTRATO=0" & vbCrLf
sConsulta = sConsulta & "   SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SEPARADOR = CHARINDEX('#',@VALOR_TEXT,1)" & vbCrLf
sConsulta = sConsulta & "WHILE @SEPARADOR > 0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @VALOR_TEXT = RIGHT(@VALOR_TEXT,LEN(@VALOR_TEXT)-@SEPARADOR)" & vbCrLf
sConsulta = sConsulta & "SET @SEPARADOR = CHARINDEX('#',@VALOR_TEXT,1)" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SEPARADOR = CHARINDEX('|',@VALOR_TEXT,1)" & vbCrLf
sConsulta = sConsulta & "WHILE @SEPARADOR > 0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @VALOR_TEXT = RIGHT(@VALOR_TEXT,LEN(@VALOR_TEXT)-@SEPARADOR)" & vbCrLf
sConsulta = sConsulta & "SET @SEPARADOR = CHARINDEX('|',@VALOR_TEXT,1)" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + '''' + @VALOR_TEXT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Denominaci?n en el idioma" & vbCrLf
sConsulta = sConsulta & "SET @EMPIEZAIDIOMA=CHARINDEX(@IDIOMA + '|',@VALOR_TEXT_DEN)" & vbCrLf
sConsulta = sConsulta & "IF (@EMPIEZAIDIOMA) > 0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @ACABADENOMINACION = CHARINDEX('#',@VALOR_TEXT_DEN,@EMPIEZAIDIOMA)" & vbCrLf
sConsulta = sConsulta & "SET @EMPIEZADENOMINACION = CHARINDEX('|',@VALOR_TEXT_DEN,@EMPIEZAIDIOMA)+1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ACABADENOMINACION>0" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ' - ' + SUBSTRING(@VALOR_TEXT_DEN,@EMPIEZADENOMINACION,@ACABADENOMINACION-@EMPIEZADENOMINACION)" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ' - ' + RIGHT(@VALOR_TEXT_DEN,LEN(@VALOR_TEXT_DEN)-@EMPIEZADENOMINACION+1)" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+N','+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "-- Personas, Comprador" & vbCrLf
sConsulta = sConsulta & "---------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "IF @TIPO_CAMPO_GS = 115 OR @TIPO_CAMPO_GS = 144" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @CAMPOTRATADO = 1" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1 AND @QA=0 AND @CONTRATO=0" & vbCrLf
sConsulta = sConsulta & "   SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT_DEN IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + '''' + @VALOR_TEXT_DEN + ''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+@VALOR_TEXT_DEN+N''''+N','+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+@VALOR_TEXT_DEN+N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "-- Almacen" & vbCrLf
sConsulta = sConsulta & "---------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "IF @TIPO_CAMPO_GS = 125" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @CAMPOTRATADO = 1" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1 AND @QA=0 AND @CONTRATO=0" & vbCrLf
sConsulta = sConsulta & "   SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT_DEN IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + '''' + @VALOR_TEXT_DEN + ''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+@VALOR_TEXT_DEN+N''''+N','+N''''+CONVERT(NVARCHAR,@VALOR_NUM)+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+@VALOR_TEXT_DEN+N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N''''+CONVERT(NVARCHAR,@VALOR_NUM)+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "------------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "-- Cod. Art?culo (viejo), Cod. Art?culo (nuevo), Denominaci?n del art?culo y Unidades Organizativas" & vbCrLf
sConsulta = sConsulta & "------------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "IF @TIPO_CAMPO_GS = 104  OR @TIPO_CAMPO_GS = 118 OR @TIPO_CAMPO_GS = 119 OR @TIPO_CAMPO_GS = 121" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @CAMPOTRATADO = 1" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1 AND @QA=0 AND @CONTRATO=0" & vbCrLf
sConsulta = sConsulta & "   SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + '''' + @VALOR_TEXT + ''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+@VALOR_TEXT+N''''+N','+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+@VALOR_TEXT +N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "---------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "-- Presupuestos" & vbCrLf
sConsulta = sConsulta & "-- FORMATO: Ej 2005 - PC-002  (50.00%); 2005 - PC-001 (50.00%);" & vbCrLf
sConsulta = sConsulta & "--  VALOR_TEXT_DEN: 2005 - PC-002#2005 - PC-001" & vbCrLf
sConsulta = sConsulta & "--  VALOR_TEXT: 1_11_0.5#1_10_0.5" & vbCrLf
sConsulta = sConsulta & "---------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "IF @TIPO_CAMPO_GS = 110  OR @TIPO_CAMPO_GS = 111 OR @TIPO_CAMPO_GS = 112 OR @TIPO_CAMPO_GS = 113" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @CAMPOTRATADO = 1" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1 AND @QA=0 AND @CONTRATO=0" & vbCrLf
sConsulta = sConsulta & "   SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "SET @PRESUPUESTOS = ''" & vbCrLf
sConsulta = sConsulta & "SET @SEPARADOR_DEN = CHARINDEX('#',@VALOR_TEXT_DEN)" & vbCrLf
sConsulta = sConsulta & "WHILE @SEPARADOR_DEN > 0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @PRESUPUESTOS = @PRESUPUESTOS + SUBSTRING(@VALOR_TEXT_DEN,1,@SEPARADOR_DEN-1)" & vbCrLf
sConsulta = sConsulta & "SET @VALOR_TEXT_DEN = RIGHT(@VALOR_TEXT_DEN,LEN(@VALOR_TEXT_DEN)-@SEPARADOR_DEN)" & vbCrLf
sConsulta = sConsulta & "SET @PORCENTAJE = SUBSTRING(@VALOR_TEXT,1,CHARINDEX('#',@VALOR_TEXT)-1)" & vbCrLf
sConsulta = sConsulta & "SET @SEPARADOR_PORCENTAJE = 1" & vbCrLf
sConsulta = sConsulta & "WHILE @SEPARADOR_PORCENTAJE > 0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @PORCENTAJE = RIGHT(@PORCENTAJE,LEN(@PORCENTAJE)-@SEPARADOR_PORCENTAJE)" & vbCrLf
sConsulta = sConsulta & "SET @SEPARADOR_PORCENTAJE = CHARINDEX('_',@PORCENTAJE,@SEPARADOR_PORCENTAJE+1)" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "SET @PRESUPUESTOS = @PRESUPUESTOS + '(' + CONVERT(VARCHAR,CONVERT(DECIMAL(3,2),@PORCENTAJE)*100) + '%); '" & vbCrLf
sConsulta = sConsulta & "SET @VALOR_TEXT = RIGHT(@VALOR_TEXT,LEN(@VALOR_TEXT)-CHARINDEX('#',@VALOR_TEXT))" & vbCrLf
sConsulta = sConsulta & "SET @SEPARADOR_DEN = CHARINDEX('#',@VALOR_TEXT_DEN)" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "SET @PORCENTAJE = @VALOR_TEXT" & vbCrLf
sConsulta = sConsulta & "SET @SEPARADOR_PORCENTAJE = 1" & vbCrLf
sConsulta = sConsulta & "WHILE @SEPARADOR_PORCENTAJE > 0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @PORCENTAJE = RIGHT(@PORCENTAJE,LEN(@PORCENTAJE)-@SEPARADOR_PORCENTAJE)" & vbCrLf
sConsulta = sConsulta & "SET @SEPARADOR_PORCENTAJE = CHARINDEX('_',@PORCENTAJE,@SEPARADOR_PORCENTAJE+1)" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @PRESUPUESTOS = @PRESUPUESTOS + @VALOR_TEXT_DEN + '(' + CAST(CONVERT(FLOAT,@PORCENTAJE)*100 AS VARCHAR) + '%);'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @PRESUPUESTOS <> ''" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + '''' + @PRESUPUESTOS + ''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'''' + @PRESUPUESTOS + N'''' + N',' + N'''' + @VALOR_TEXT + N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'''' + @PRESUPUESTOS + N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL,'+N'C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CAMPOTRATADO = 0 -- Resto" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @SUBTIPO = 3 -- tipo fecha" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_FEC IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + '''' + @VALOR_FEC + ''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @SUBTIPO = 4 -- tipo booleano" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_BOOL IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + CONVERT(VARCHAR,@VALOR_BOOL)" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @SUBTIPO=2 -- tipo num?rico" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_NUM IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + CONVERT(VARCHAR,@VALOR_NUM)" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE    -- tipo texto" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1 AND @QA=0 AND @CONTRATO=0" & vbCrLf
sConsulta = sConsulta & "   SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + '''' + @VALOR_TEXT + ''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+@VALOR_TEXT+N''''+N','+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+''''+@VALOR_TEXT+''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+ N'_COD='+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+ N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ','" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curSOLICITUDES_CAMPOS INTO @CAMPO_ORIGEN, @SUBTIPO, @TIPO_CAMPO_GS, @VALOR_TEXT, @VALOR_NUM, @VALOR_FEC, @VALOR_BOOL, @VALOR_TEXT_COD, @VALOR_TEXT_DEN, @INTRO, @ID_ATRIB_GS,@TABLA_EXTERNA, @CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CLOSE curSOLICITUDES_CAMPOS" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curSOLICITUDES_CAMPOS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @EJECUTARSQL=1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @INSERT = 1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = LEFT(@SQLCAMPOSINSERT,LEN(@SQLCAMPOSINSERT)-1) + ') ' + LEFT(@SQL,LEN(@SQL)-1) + ')'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = LEFT(@SQL,LEN(@SQL)-1) + ' WHERE INSTANCIA = @INSTANCIA_L'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL=N'IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = ''dbo'' AND  TABLE_NAME='''+@IDIOMA+N'_'+@TABLA+''')) BEGIN '+@SQL+N' END'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA_L INT', @INSTANCIA_L=@INSTANCIA_L" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--*********************************" & vbCrLf
sConsulta = sConsulta & "--*********************************" & vbCrLf
sConsulta = sConsulta & "-- AHORA LOS CAMPOS DE LOS DESGLOSES (SI NO ES QA NI CONTRATO)" & vbCrLf
sConsulta = sConsulta & "--**********************************" & vbCrLf
sConsulta = sConsulta & "IF @QA=0 AND @CONTRATO=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SQL=''" & vbCrLf
sConsulta = sConsulta & "SET @SQLCAMPOSINSERT=''" & vbCrLf
sConsulta = sConsulta & "SET @EJECUTARSQL=0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @INSERT = 1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SQLCAMPOSINSERT=N'INSERT INTO '+@IDIOMA+N'_'+@TABLA+N'_DESGLOSE (FORM_INSTANCIA,LINEA,DESGLOSE,'" & vbCrLf
sConsulta = sConsulta & "SET @SQL=N' VALUES (@INSTANCIA_L,@LINEA,'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=N' UPDATE '+@IDIOMA+N'_'+@TABLA+N'_DESGLOSE SET '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE curSOLICITUDES_CAMPOS_DESGLOSE CURSOR LOCAL FOR" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT CLD.LINEA,CCD.CAMPO_ORIGEN,CCD.SUBTIPO,CCD.TIPO_CAMPO_GS,CLD.VALOR_TEXT,CLD.VALOR_NUM,CLD.VALOR_FEC,CLD.VALOR_BOOL," & vbCrLf
sConsulta = sConsulta & "CLD.VALOR_TEXT_COD,CLD.VALOR_TEXT_DEN,CCD.INTRO,CCD.ID_ATRIB_GS,CCD.TABLA_EXTERNA,CC.COPIA_CAMPO_DEF," & vbCrLf
sConsulta = sConsulta & "D.CAMPO_PADRE" & vbCrLf
sConsulta = sConsulta & "FROM COPIA_CAMPO_DEF CCD WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_CAMPO CC WITH(NOLOCK) ON CCD.ID=CC.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "INNER JOIN INSTANCIA I WITH(NOLOCK) ON CC.INSTANCIA=I.ID AND CC.NUM_VERSION=I.ULT_VERSION" & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_GRUPO G WITH (NOLOCK) ON I.ID=G.INSTANCIA" & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_LINEA_DESGLOSE CLD WITH(NOLOCK) ON CLD.CAMPO_HIJO=CC.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN FORM_GRUPO G2 WITH (NOLOCK) ON G.GRUPO_ORIGEN=G2.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN FORMULARIO F WITH(NOLOCK) ON F.ID=G2.FORMULARIO" & vbCrLf
sConsulta = sConsulta & "INNER JOIN FORM_GRUPO FG WITH (NOLOCK) ON F.ID=FG.FORMULARIO" & vbCrLf
sConsulta = sConsulta & "INNER JOIN FORM_CAMPO FC WITH (NOLOCK) ON FG.ID=FC.GRUPO" & vbCrLf
sConsulta = sConsulta & "INNER JOIN DESGLOSE D WITH(NOLOCK) ON D.CAMPO_HIJO=FC.ID" & vbCrLf
sConsulta = sConsulta & "WHERE CCD.ES_SUBCAMPO=1 AND CCD.SUBTIPO<>9 AND (CCD.TIPO_CAMPO_GS<>106 OR CCD.TIPO_CAMPO_GS IS NULL)" & vbCrLf
sConsulta = sConsulta & "AND CCD.CAMPO_ORIGEN=FC.ID" & vbCrLf
sConsulta = sConsulta & "AND (CCD.SUBTIPO=FC.SUBTIPO OR (CCD.SUBTIPO=5 AND FC.SUBTIPO IN (6,7)) OR (CCD.SUBTIPO=6 AND FC.SUBTIPO=7))--si la definici?n del campo ha cambiado de texto corto a texto medio o largo, o de texto medio a texto largo" & vbCrLf
sConsulta = sConsulta & "AND CCD.CAMPO_ORIGEN NOT IN (SELECT ID_CAMPO FROM #G1)  -- campos que se han a?adido en el formulario pero no se ha hecho todavia ALTER en las tablas de filtros (BatchUpdates)" & vbCrLf
sConsulta = sConsulta & "AND I.ID=@INSTANCIA_L" & vbCrLf
sConsulta = sConsulta & "ORDER BY CAMPO_PADRE,LINEA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "OPEN curSOLICITUDES_CAMPOS_DESGLOSE" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curSOLICITUDES_CAMPOS_DESGLOSE INTO @LINEA,@CAMPO_ORIGEN,@SUBTIPO,@TIPO_CAMPO_GS,@VALOR_TEXT," & vbCrLf
sConsulta = sConsulta & "@VALOR_NUM,@VALOR_FEC,@VALOR_BOOL,@VALOR_TEXT_COD,@VALOR_TEXT_DEN,@INTRO,@ID_ATRIB_GS,@TABLA_EXTERNA," & vbCrLf
sConsulta = sConsulta & "@CAMPO_DEF,@CAMPO_PADRE_DESGLOSE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @CAMPO_PADRE_DESGLOSE_ANT=@CAMPO_PADRE_DESGLOSE" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLAUX NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLCAMPOSINSERTAUX NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "SET @SQLAUX=@SQL" & vbCrLf
sConsulta = sConsulta & "SET @SQLCAMPOSINSERTAUX=@SQLCAMPOSINSERT" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N','" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @EJECUTARSQL=1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @LINEA<>@LINEA_ANT OR @CAMPO_PADRE_DESGLOSE<>@CAMPO_PADRE_DESGLOSE_ANT" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @EJECUTARSQL=1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @INSERT = 1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=LEFT(@SQLCAMPOSINSERT,LEN(@SQLCAMPOSINSERT)-1) + ') '+LEFT(@SQL,LEN(@SQL)-1)+')'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=LEFT(@SQL,LEN(@SQL)-1)+' WHERE FORM_INSTANCIA=@INSTANCIA_L AND DESGLOSE=@CAMPO_PADRE_DESGLOSE AND LINEA=@LINEA '" & vbCrLf
sConsulta = sConsulta & "SET @SQL=N'IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = ''dbo'' AND  TABLE_NAME='''+@IDIOMA+N'_'+@TABLA+N'_DESGLOSE'')) BEGIN '+@SQL+N' END'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA_L INT,@CAMPO_PADRE_DESGLOSE INT,@LINEA INT',@INSTANCIA_L=@INSTANCIA_L,@CAMPO_PADRE_DESGLOSE=@CAMPO_PADRE_DESGLOSE_ANT,@LINEA=@LINEA_ANT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQLAUX" & vbCrLf
sConsulta = sConsulta & "SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERTAUX" & vbCrLf
sConsulta = sConsulta & "SET @LINEA_ANT=@LINEA" & vbCrLf
sConsulta = sConsulta & "SET @CAMPO_PADRE_DESGLOSE_ANT=@CAMPO_PADRE_DESGLOSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N','" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "--Reemplazamos las comillas simples en valor_text y valor_text_den" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @VALOR_TEXT = SUBSTRING(@VALOR_TEXT,1,100)" & vbCrLf
sConsulta = sConsulta & "SET @VALOR_TEXT = REPLACE(@VALOR_TEXT,'''','''''')" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT_DEN IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SET @VALOR_TEXT_DEN = REPLACE(@VALOR_TEXT_DEN,'''','''''')" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT + N'C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N','" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'='" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "----------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "IF @SUBTIPO = 3 -- tipo fecha" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_FEC IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'CONVERT(DATETIME,''' + CONVERT(VARCHAR,@VALOR_FEC) + ''')'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @SUBTIPO = 4 -- tipo booleano" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_BOOL IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + CONVERT(NVARCHAR,@VALOR_BOOL)" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @SUBTIPO = 8 -- tipo archivo" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'''' + @VALOR_TEXT + N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @TIPO_CAMPO_GS IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @TABLA_EXTERNA IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @SUBTIPO=2 -- tipo num?rico" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_NUM IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + CONVERT(NVARCHAR,@VALOR_NUM)" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE    -- tipo texto o tipo lista texto" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "IF @INTRO=1 AND @VALOR_NUM IS NOT NULL --tipo lista texto" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @ID_ATRIB_GS IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @IDIOMA = 'SPA'" & vbCrLf
sConsulta = sConsulta & "SELECT @DENOMINACION=VALOR_TEXT_SPA" & vbCrLf
sConsulta = sConsulta & "FROM COPIA_CAMPO_VALOR_LISTA WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE CAMPO_DEF=CONVERT(VARCHAR,@CAMPO_DEF) AND ORDEN=CONVERT(VARCHAR,@VALOR_NUM)" & vbCrLf
sConsulta = sConsulta & "IF @IDIOMA = 'ENG'" & vbCrLf
sConsulta = sConsulta & "SELECT @DENOMINACION=VALOR_TEXT_ENG" & vbCrLf
sConsulta = sConsulta & "FROM COPIA_CAMPO_VALOR_LISTA WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE CAMPO_DEF=CONVERT(VARCHAR,@CAMPO_DEF) AND ORDEN=CONVERT(VARCHAR,@VALOR_NUM)" & vbCrLf
sConsulta = sConsulta & "IF @IDIOMA = 'GER'" & vbCrLf
sConsulta = sConsulta & "SELECT @DENOMINACION=VALOR_TEXT_GER" & vbCrLf
sConsulta = sConsulta & "FROM COPIA_CAMPO_VALOR_LISTA WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE CAMPO_DEF = CONVERT(VARCHAR,@CAMPO_DEF) AND ORDEN=CONVERT(VARCHAR,@VALOR_NUM)" & vbCrLf
sConsulta = sConsulta & "IF @IDIOMA = 'FRA'" & vbCrLf
sConsulta = sConsulta & "SELECT @DENOMINACION=VALOR_TEXT_FRA" & vbCrLf
sConsulta = sConsulta & "FROM COPIA_CAMPO_VALOR_LISTA WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE CAMPO_DEF = CONVERT(VARCHAR,@CAMPO_DEF) AND ORDEN=CONVERT(VARCHAR,@VALOR_NUM)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @DENOMINACION IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+REPLACE(@DENOMINACION,'''','''''')+''''+N',CONVERT(NVARCHAR,'+CONVERT(NVARCHAR,@VALOR_NUM)+N')'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+REPLACE(@DENOMINACION,'''','''''')+''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=CONVERT(NVARCHAR,'+CONVERT(NVARCHAR,@VALOR_NUM)+N')'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + N'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + N'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + N''''+@VALOR_TEXT+N''''+N','+N''''+CONVERT(NVARCHAR,@VALOR_NUM)+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + N''''+@VALOR_TEXT+N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+ CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N''''+CONVERT(NVARCHAR,@VALOR_NUM)+N''''" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE -- tipo texto" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + N''''+@VALOR_TEXT+N''''+N','+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + N''''+@VALOR_TEXT+N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+ CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + N'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @SUBTIPO=2 -- tipo num?rico" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_NUM IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + CONVERT(NVARCHAR,@VALOR_NUM)" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE    -- tipo texto o tipo lista texto" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT_DEN IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+@VALOR_TEXT_DEN+N''''+N','+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+@VALOR_TEXT_DEN+N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_' +CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE -- campo del sistema" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @CAMPOTRATADO = 0" & vbCrLf
sConsulta = sConsulta & "---------------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "-- Formas de pago, Monedas, Familia de materiales, Unidades, Destinos, Pa?ses y Provincias" & vbCrLf
sConsulta = sConsulta & "-- FORMATO: <Idioma>|<Texto>#<Idioma>|<Texto>" & vbCrLf
sConsulta = sConsulta & "-- FORMATO MATERIALES: cod. ultimo nivel (VALOR_TEXT) +  <Idioma>|<Texto>#<Idioma>|<Texto>" & vbCrLf
sConsulta = sConsulta & "------------------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "IF @TIPO_CAMPO_GS = 101 OR @TIPO_CAMPO_GS = 102 OR @TIPO_CAMPO_GS = 103 OR @TIPO_CAMPO_GS = 105 OR @TIPO_CAMPO_GS = 109 OR @TIPO_CAMPO_GS = 107 OR @TIPO_CAMPO_GS = 108" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+ N'_COD,'" & vbCrLf
sConsulta = sConsulta & "SET @CAMPOTRATADO = 1" & vbCrLf
sConsulta = sConsulta & "-- Materiales" & vbCrLf
sConsulta = sConsulta & "IF @TIPO_CAMPO_GS = 103" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "-- Al hacer Len de valor_text me est? quitando los espacios en blanco de la derecha" & vbCrLf
sConsulta = sConsulta & "IF LEN(@VALOR_TEXT + '#')-1 = @LEN_GMN1 + @LEN_GMN2 + @LEN_GMN3 + @LEN_GMN4" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'''' + RTRIM(RIGHT(@VALOR_TEXT,@LEN_GMN4)) + N' - '" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF LEN(@VALOR_TEXT + '#')-1 = @LEN_GMN1 + @LEN_GMN2 + @LEN_GMN3" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'''' + RTRIM(RIGHT(@VALOR_TEXT,@LEN_GMN3)) + N' - '" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF LEN(@VALOR_TEXT + '#')-1 = @LEN_GMN1 + @LEN_GMN2" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'''' + RTRIM(RIGHT(@VALOR_TEXT,@LEN_GMN2)) + N' - '" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'''' + RTRIM(RIGHT(@VALOR_TEXT,@LEN_GMN1)) + N' - '" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- Todos" & vbCrLf
sConsulta = sConsulta & "SET @EMPIEZAIDIOMA=CHARINDEX(@IDIOMA + '|',@VALOR_TEXT_DEN)" & vbCrLf
sConsulta = sConsulta & "IF (@EMPIEZAIDIOMA) > 0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @ACABADENOMINACION = CHARINDEX('#',@VALOR_TEXT_DEN,@EMPIEZAIDIOMA)" & vbCrLf
sConsulta = sConsulta & "SET @EMPIEZADENOMINACION = CHARINDEX('|',@VALOR_TEXT_DEN,@EMPIEZAIDIOMA)+1" & vbCrLf
sConsulta = sConsulta & "IF @TIPO_CAMPO_GS <> 103" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+''''" & vbCrLf
sConsulta = sConsulta & "IF @ACABADENOMINACION>0" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+SUBSTRING(@VALOR_TEXT_DEN,@EMPIEZADENOMINACION,@ACABADENOMINACION-@EMPIEZADENOMINACION) + ''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+RIGHT(@VALOR_TEXT_DEN,LEN(@VALOR_TEXT_DEN)-@EMPIEZADENOMINACION+1) + ''''" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N','+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N',C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT IS NOT NULL AND @TIPO_CAMPO_GS = 103" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+N','+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "-- Proveedores, Departamentos, Organizaciones de Compras, Centros" & vbCrLf
sConsulta = sConsulta & "-- FORMATO: C?digo (VALOR_TEXT) - Denominaci?n (VALOR_TEXT_DEN)" & vbCrLf
sConsulta = sConsulta & "--------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "IF @TIPO_CAMPO_GS = 11 OR @TIPO_CAMPO_GS = 100  OR @TIPO_CAMPO_GS = 122 OR @TIPO_CAMPO_GS = 123 OR @TIPO_CAMPO_GS = 124" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @CAMPOTRATADO = 1" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+@VALOR_TEXT+N' - '+@VALOR_TEXT_DEN+N''''+N','+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+@VALOR_TEXT+N' - '+@VALOR_TEXT_DEN+N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "-- Centros de coste, Partidas presupuestarias, Activos" & vbCrLf
sConsulta = sConsulta & "-- FORMATO: cod. ultimo nivel (VALOR_TEXT) +  <Idioma>|<Texto>#<Idioma>|<Texto>" & vbCrLf
sConsulta = sConsulta & "--------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "IF @TIPO_CAMPO_GS = 129 OR @TIPO_CAMPO_GS = 130 OR @TIPO_CAMPO_GS = 131 OR @TIPO_CAMPO_GS=132" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @CAMPOTRATADO=1" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SEPARADOR = CHARINDEX('#',@VALOR_TEXT,1)" & vbCrLf
sConsulta = sConsulta & "WHILE @SEPARADOR > 0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @VALOR_TEXT = RIGHT(@VALOR_TEXT,LEN(@VALOR_TEXT)-@SEPARADOR)" & vbCrLf
sConsulta = sConsulta & "SET @SEPARADOR = CHARINDEX('#',@VALOR_TEXT,1)" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SEPARADOR = CHARINDEX('|',@VALOR_TEXT,1)" & vbCrLf
sConsulta = sConsulta & "WHILE @SEPARADOR > 0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @VALOR_TEXT = RIGHT(@VALOR_TEXT,LEN(@VALOR_TEXT)-@SEPARADOR)" & vbCrLf
sConsulta = sConsulta & "SET @SEPARADOR = CHARINDEX('|',@VALOR_TEXT,1)" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + '''' + @VALOR_TEXT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Denominaci?n en el idioma" & vbCrLf
sConsulta = sConsulta & "SET @EMPIEZAIDIOMA=CHARINDEX(@IDIOMA + '|',@VALOR_TEXT_DEN)" & vbCrLf
sConsulta = sConsulta & "IF (@EMPIEZAIDIOMA) > 0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @ACABADENOMINACION = CHARINDEX('#',@VALOR_TEXT_DEN,@EMPIEZAIDIOMA)" & vbCrLf
sConsulta = sConsulta & "SET @EMPIEZADENOMINACION = CHARINDEX('|',@VALOR_TEXT_DEN,@EMPIEZAIDIOMA)+1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ACABADENOMINACION>0" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ' - ' + SUBSTRING(@VALOR_TEXT_DEN,@EMPIEZADENOMINACION,@ACABADENOMINACION-@EMPIEZADENOMINACION)" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ' - ' + RIGHT(@VALOR_TEXT_DEN,LEN(@VALOR_TEXT_DEN)-@EMPIEZADENOMINACION+1)" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+N','+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "-- Personas, Comprador" & vbCrLf
sConsulta = sConsulta & "---------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "IF @TIPO_CAMPO_GS = 115 OR @TIPO_CAMPO_GS=144" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @CAMPOTRATADO = 1" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT_DEN IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+@VALOR_TEXT_DEN+N''''+N','+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+@VALOR_TEXT_DEN+N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "-- Almacen" & vbCrLf
sConsulta = sConsulta & "---------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "IF @TIPO_CAMPO_GS = 125" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @CAMPOTRATADO = 1" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT_DEN IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+@VALOR_TEXT_DEN+N''''+N','+N''''+CONVERT(NVARCHAR,@VALOR_NUM)+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+@VALOR_TEXT_DEN+N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N''''+CONVERT(NVARCHAR,@VALOR_NUM)+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "------------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "-- Cod. Art?culo (viejo), Cod. Art?culo (nuevo), Denominaci?n del art?culo y Unidades Organizativas" & vbCrLf
sConsulta = sConsulta & "------------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "IF @TIPO_CAMPO_GS = 104  OR @TIPO_CAMPO_GS = 118 OR @TIPO_CAMPO_GS = 119 OR @TIPO_CAMPO_GS = 121" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @CAMPOTRATADO = 1" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+@VALOR_TEXT+N''''+N','+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+@VALOR_TEXT +N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "---------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "-- Presupuestos" & vbCrLf
sConsulta = sConsulta & "-- FORMATO: Ej 2005 - PC-002  (50.00%); 2005 - PC-001 (50.00%);" & vbCrLf
sConsulta = sConsulta & "--  VALOR_TEXT_DEN: 2005 - PC-002#2005 - PC-001" & vbCrLf
sConsulta = sConsulta & "--  VALOR_TEXT: 1_11_0.5#1_10_0.5" & vbCrLf
sConsulta = sConsulta & "---------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "IF @TIPO_CAMPO_GS = 110  OR @TIPO_CAMPO_GS = 111 OR @TIPO_CAMPO_GS = 112 OR @TIPO_CAMPO_GS = 113" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @CAMPOTRATADO = 1" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "SET @PRESUPUESTOS = ''" & vbCrLf
sConsulta = sConsulta & "SET @SEPARADOR_DEN = CHARINDEX('#',@VALOR_TEXT_DEN)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "WHILE @SEPARADOR_DEN > 0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @PRESUPUESTOS = @PRESUPUESTOS + SUBSTRING(@VALOR_TEXT_DEN,1,@SEPARADOR_DEN-1)" & vbCrLf
sConsulta = sConsulta & "SET @VALOR_TEXT_DEN = RIGHT(@VALOR_TEXT_DEN,LEN(@VALOR_TEXT_DEN)-@SEPARADOR_DEN)" & vbCrLf
sConsulta = sConsulta & "SET @PORCENTAJE = SUBSTRING(@VALOR_TEXT,1,CHARINDEX('#',@VALOR_TEXT)-1)" & vbCrLf
sConsulta = sConsulta & "SET @SEPARADOR_PORCENTAJE = 1" & vbCrLf
sConsulta = sConsulta & "WHILE @SEPARADOR_PORCENTAJE > 0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @PORCENTAJE = RIGHT(@PORCENTAJE,LEN(@PORCENTAJE)-@SEPARADOR_PORCENTAJE)" & vbCrLf
sConsulta = sConsulta & "SET @SEPARADOR_PORCENTAJE = CHARINDEX('_',@PORCENTAJE,@SEPARADOR_PORCENTAJE+1)" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "SET @PRESUPUESTOS = @PRESUPUESTOS + '(' + CONVERT(VARCHAR,CONVERT(DECIMAL(3,2),@PORCENTAJE)*100) + '%); '" & vbCrLf
sConsulta = sConsulta & "SET @VALOR_TEXT = RIGHT(@VALOR_TEXT,LEN(@VALOR_TEXT)-CHARINDEX('#',@VALOR_TEXT))" & vbCrLf
sConsulta = sConsulta & "SET @SEPARADOR_DEN = CHARINDEX('#',@VALOR_TEXT_DEN)" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "SET @PORCENTAJE = @VALOR_TEXT" & vbCrLf
sConsulta = sConsulta & "SET @SEPARADOR_PORCENTAJE = 1" & vbCrLf
sConsulta = sConsulta & "WHILE @SEPARADOR_PORCENTAJE > 0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @PORCENTAJE = RIGHT(@PORCENTAJE,LEN(@PORCENTAJE)-@SEPARADOR_PORCENTAJE)" & vbCrLf
sConsulta = sConsulta & "SET @SEPARADOR_PORCENTAJE = CHARINDEX('_',@PORCENTAJE,@SEPARADOR_PORCENTAJE+1)" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @PRESUPUESTOS = @PRESUPUESTOS + @VALOR_TEXT_DEN + '(' + CAST(CONVERT(FLOAT,@PORCENTAJE)*100 AS VARCHAR) + '%);'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @PRESUPUESTOS <> ''" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'''' + @PRESUPUESTOS + N'''' + N',' + N'''' + @VALOR_TEXT + N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'''' + @PRESUPUESTOS + N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL,'+N'C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CAMPOTRATADO = 0 -- Resto" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @SUBTIPO = 3 -- tipo fecha" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_FEC IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + '''' + @VALOR_FEC + ''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @SUBTIPO = 4 -- tipo booleano" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_BOOL IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + CONVERT(VARCHAR,@VALOR_BOOL)" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @SUBTIPO=2 -- tipo num?rico" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_NUM IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + CONVERT(VARCHAR,@VALOR_NUM)" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE    -- tipo texto" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+@VALOR_TEXT+N''''+N','+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+''''+@VALOR_TEXT+''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+ N'_COD='+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+ N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ','" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curSOLICITUDES_CAMPOS_DESGLOSE INTO @LINEA,@CAMPO_ORIGEN,@SUBTIPO,@TIPO_CAMPO_GS," & vbCrLf
sConsulta = sConsulta & "@VALOR_TEXT,@VALOR_NUM,@VALOR_FEC,@VALOR_BOOL,@VALOR_TEXT_COD,@VALOR_TEXT_DEN,@INTRO,@ID_ATRIB_GS," & vbCrLf
sConsulta = sConsulta & "@TABLA_EXTERNA,@CAMPO_DEF,@CAMPO_PADRE_DESGLOSE" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "CLOSE curSOLICITUDES_CAMPOS_DESGLOSE" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curSOLICITUDES_CAMPOS_DESGLOSE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @EJECUTARSQL=1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @INSERT = 1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = LEFT(@SQLCAMPOSINSERT,LEN(@SQLCAMPOSINSERT)-1) + ') ' + LEFT(@SQL,LEN(@SQL)-1) + ')'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = LEFT(@SQL,LEN(@SQL)-1) + ' WHERE FORM_INSTANCIA=@INSTANCIA_L AND DESGLOSE=@CAMPO_PADRE_DESGLOSE AND LINEA=@LINEA '" & vbCrLf
sConsulta = sConsulta & "SET @SQL=N'IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = ''dbo'' AND  TABLE_NAME='''+@IDIOMA+N'_'+@TABLA+N'_DESGLOSE'')) BEGIN '+@SQL+N' END'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA_L INT,@CAMPO_PADRE_DESGLOSE INT,@LINEA INT', @INSTANCIA_L=@INSTANCIA_L,@CAMPO_PADRE_DESGLOSE=@CAMPO_PADRE_DESGLOSE,@LINEA=@LINEA" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curIDIOMAS INTO @IDIOMA" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CLOSE curIDIOMAS" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curIDIOMAS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DROP TABLE #G1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSEP_GET_ORDENES_APROVISIONADOR]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSEP_GET_ORDENES_APROVISIONADOR]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSEP_GET_ORDENES_APROVISIONADOR" & vbCrLf
sConsulta = sConsulta & "   @NUMLINEAS INT OUTPUT," & vbCrLf
sConsulta = sConsulta & "   @APROVISIONADOR VARCHAR(50)," & vbCrLf
sConsulta = sConsulta & "   @EST INT=0," & vbCrLf
sConsulta = sConsulta & "   @PENDIENTESAPROBAR BIT=0," & vbCrLf
sConsulta = sConsulta & "   @PENDIENTESRECEPCIONAR BIT=0," & vbCrLf
sConsulta = sConsulta & "   @PERMISORECEPCIONAR_CC BIT=0," & vbCrLf
sConsulta = sConsulta & "   @VERENRECEPCIONPEDIDOS_CC BIT=0" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLQUERY NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLQUERYESTADOS NVARCHAR(2000) = ''" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLWHEREESTADOS NVARCHAR(2000) = ''" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLQUERYPENDIENTESAPROBAR NVARCHAR(2000) = ''" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLWHEREPENDIENTESAPROBAR NVARCHAR(2000) = ''" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLQUERYPENDIENTESRECEPCIONAR NVARCHAR(2000) = ''" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLWHEREPENDIENTESRECEPCIONAR NVARCHAR(2000) = ''" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLQUERYPENDIENTESRECEPCIONARCC NVARCHAR(2000) = ''" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLWHEREPENDIENTESRECEPCIONARCC NVARCHAR(2000) = ''" & vbCrLf
sConsulta = sConsulta & "DECLARE @IN NVARCHAR(20)='*2*3*4*5*'" & vbCrLf
sConsulta = sConsulta & "--AL MOSTRAR RECEPCIONES, No mostraremos los abonos" & vbCrLf
sConsulta = sConsulta & "DECLARE @ABONO NVARCHAR(10)" & vbCrLf
sConsulta = sConsulta & "SET @ABONO='*0*1*'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLORDERBY NVARCHAR(50) = '" & vbCrLf
sConsulta = sConsulta & "   ORDER BY OE.FECHA DESC'" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "--Si no hay ning?n filtro, no devolvemos ning?n valor" & vbCrLf
sConsulta = sConsulta & "IF @EST =0 AND @PENDIENTESAPROBAR = 0 AND @PENDIENTESRECEPCIONAR = 0" & vbCrLf
sConsulta = sConsulta & "   SET @SQLWHEREESTADOS = '" & vbCrLf
sConsulta = sConsulta & "   AND " & vbCrLf
sConsulta = sConsulta & "       (OE.EST=-1)'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @EST>0" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @SQLQUERYESTADOS = '" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PEDIDO PE WITH (NOLOCK) ON OE.PEDIDO=PE.ID'" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   SET @SQLWHEREESTADOS = '" & vbCrLf
sConsulta = sConsulta & "   AND " & vbCrLf
sConsulta = sConsulta & "       (PE.PER=@APROVISIONADOR" & vbCrLf
sConsulta = sConsulta & "       AND (OE.EST<>0 OR (@EST & 1)>0) AND (OE.EST<>1 OR (@EST & 2)>0) AND (OE.EST<>2 OR (@EST & 4)>0) AND (OE.EST<>3 OR (@EST & 8)>0)" & vbCrLf
sConsulta = sConsulta & "       AND (OE.EST<>4 OR (@EST & 16)>0) AND (OE.EST<>5 OR (@EST & 32)>0) AND (OE.EST<>6 OR (@EST & 64)>0)" & vbCrLf
sConsulta = sConsulta & "       AND (OE.EST<>20 OR (@EST & 128)>0) AND (OE.EST<>21 OR (@EST & 256)>0) AND (OE.EST<>22 OR (@EST & 512)>0))'" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "IF @PENDIENTESAPROBAR = 1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SQLQUERYPENDIENTESAPROBAR = '" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN LINEAS_PEDIDO LP WITH (NOLOCK) ON LP.ORDEN = OE.ID" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN CATALOG_LIN CL ON CL.ID=LP.LINCAT" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN CATN1 WITH (NOLOCK) ON CATN1.ID=CL.SEGURIDAD_CATN AND CL.SEGURIDAD_NIVEL=1" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN CATN2 WITH (NOLOCK) ON CATN2.ID=CL.SEGURIDAD_CATN AND CL.SEGURIDAD_NIVEL=2" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN CATN3 WITH (NOLOCK) ON CATN3.ID=CL.SEGURIDAD_CATN AND CL.SEGURIDAD_NIVEL=3" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN CATN4 WITH (NOLOCK) ON CATN4.ID=CL.SEGURIDAD_CATN AND CL.SEGURIDAD_NIVEL=4" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN CATN5 WITH (NOLOCK) ON CATN5.ID=CL.SEGURIDAD_CATN AND CL.SEGURIDAD_NIVEL=5" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN LINEAS_EST LE WITH (NOLOCK) ON LE.LINEA=LP.ID AND LE.PEDIDO=LP.PEDIDO AND LE.ORDEN=LP.ORDEN" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PER WITH (NOLOCK) ON PER.COD=@APROVISIONADOR" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN INSTANCIA I WITH (NOLOCK) ON I.ID=LP.INSTANCIA_APROB" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN INSTANCIA_BLOQUE IB WITH (NOLOCK) ON I.ID=IB.INSTANCIA AND IB.EN_PROCESO=0 AND IB. ESTADO=1 AND IB.BLOQ<>1" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL_BLOQUE RB WITH (NOLOCK) ON RB.BLOQUE=IB.BLOQUE AND IB.ESTADO=1 AND IB.BLOQ<>1 AND IB.TRASLADADA<>1" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL R WITH (NOLOCK)  ON RB.ROL=R.ID" & vbCrLf
sConsulta = sConsulta & "   AND ISNULL(R.UON1,'''') = CASE WHEN R.UON1 IS NULL  THEN '''' ELSE PER.UON1 END AND ISNULL(R.UON2,'''') = CASE WHEN R.UON2 IS NULL  THEN '''' ELSE PER.UON2 END AND ISNULL(R.UON3,'''') = CASE WHEN R.UON3 IS NULL  THEN '''' ELSE PER.UON3 END AND ISNULL(R.PER,'''') = CASE WHEN R.PER IS NULL  THEN '''' ELSE PER.COD END'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLWHEREPENDIENTESAPROBAR = '" & vbCrLf
sConsulta = sConsulta & "AND" & vbCrLf
sConsulta = sConsulta & "(" & vbCrLf
sConsulta = sConsulta & "(" & vbCrLf
sConsulta = sConsulta & "(LP.NIVEL_APROB=0 AND COALESCE(CATN1.APROBADOR,CATN2.APROBADOR,CATN3.APROBADOR,CATN4.APROBADOR,CATN5.APROBADOR )=@APROVISIONADOR)" & vbCrLf
sConsulta = sConsulta & "OR" & vbCrLf
sConsulta = sConsulta & "(LE.EST=1 AND LE.PER=@APROVISIONADOR)" & vbCrLf
sConsulta = sConsulta & ")" & vbCrLf
sConsulta = sConsulta & "AND" & vbCrLf
sConsulta = sConsulta & "(NOT LP.EST IS NULL AND NOT LP.APROBADOR IS NULL)" & vbCrLf
sConsulta = sConsulta & "AND" & vbCrLf
sConsulta = sConsulta & "LP.EST = 0  --> PDTES DE APROBAR" & vbCrLf
sConsulta = sConsulta & "AND" & vbCrLf
sConsulta = sConsulta & "OE.EST < 2" & vbCrLf
sConsulta = sConsulta & ")" & vbCrLf
sConsulta = sConsulta & "   OR" & vbCrLf
sConsulta = sConsulta & "      (" & vbCrLf
sConsulta = sConsulta & "           OE.EST < 2" & vbCrLf
sConsulta = sConsulta & "           AND" & vbCrLf
sConsulta = sConsulta & "           LP.EST = 0  --> PDTES DE APROBAR" & vbCrLf
sConsulta = sConsulta & "           AND" & vbCrLf
sConsulta = sConsulta & "           LP.NIVEL_APROB = 1  --> (NIVEL1->SOLICITUD)" & vbCrLf
sConsulta = sConsulta & "           AND" & vbCrLf
sConsulta = sConsulta & "           R.PER=@APROVISIONADOR" & vbCrLf
sConsulta = sConsulta & "      )'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @PENDIENTESRECEPCIONAR = 1" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @SQLWHEREPENDIENTESRECEPCIONAR = '" & vbCrLf
sConsulta = sConsulta & "       (PATINDEX(''%*'' + CONVERT(VARCHAR,OE.EST) + ''*%'', @IN)>0" & vbCrLf
sConsulta = sConsulta & "       AND (OE.EST<>2 OR OE.ACEP_PROVE =0)" & vbCrLf
sConsulta = sConsulta & "       AND PATINDEX(''%*'' + CONVERT(VARCHAR,OE.ABONO) + ''*%'', @ABONO)>0" & vbCrLf
sConsulta = sConsulta & "       AND (@APROVISIONADOR IS NULL OR OE.RECEPTOR=@APROVISIONADOR))'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @PERMISORECEPCIONAR_CC = 1 AND @VERENRECEPCIONPEDIDOS_CC = 1" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "       SET @SQLQUERYPENDIENTESRECEPCIONARCC = '" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN (SELECT DISTINCT ORDEN, ISNULL(UON1,'''')+ISNULL(UON2,'''')+ISNULL(UON3,'''')+ISNULL(UON4,'''') CCPEDIDO" & vbCrLf
sConsulta = sConsulta & "               FROM LINEAS_PEDIDO LP WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "               INNER JOIN LINEAS_PED_IMPUTACION LPI WITH (NOLOCK) ON LPI.LINEA = LP.ID) IMP" & vbCrLf
sConsulta = sConsulta & "               ON OE.ID = IMP.ORDEN'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       SET @SQLWHEREPENDIENTESRECEPCIONARCC = '" & vbCrLf
sConsulta = sConsulta & "       (PATINDEX(''%*'' + CONVERT(VARCHAR,OE.EST) + ''*%'', @IN)>0" & vbCrLf
sConsulta = sConsulta & "       AND (OE.EST<>2 OR OE.ACEP_PROVE =0)" & vbCrLf
sConsulta = sConsulta & "       AND PATINDEX(''%*'' + CONVERT(VARCHAR,OE.ABONO) + ''*%'', @ABONO)>0" & vbCrLf
sConsulta = sConsulta & "       AND (OE.RECEPTOR<>@APROVISIONADOR)" & vbCrLf
sConsulta = sConsulta & "       AND IMP.CCPEDIDO IN" & vbCrLf
sConsulta = sConsulta & "           (SELECT ISNULL(UON1,'''')+ISNULL(UON2,'''')+ISNULL(UON3,'''')+ISNULL(UON4,'''')" & vbCrLf
sConsulta = sConsulta & "           FROM USU_CC_IMPUTACION UI WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN USU U WITH (NOLOCK) ON U.COD = UI.USU" & vbCrLf
sConsulta = sConsulta & "           WHERE U.PER =@APROVISIONADOR))'" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "       SET @SQLWHEREPENDIENTESRECEPCIONAR = '" & vbCrLf
sConsulta = sConsulta & "       AND" & vbCrLf
sConsulta = sConsulta & "           (' + @SQLWHEREPENDIENTESRECEPCIONAR + ' OR ' + @SQLWHEREPENDIENTESRECEPCIONARCC + ')'" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "       SET @SQLWHEREPENDIENTESRECEPCIONAR = '" & vbCrLf
sConsulta = sConsulta & "       AND ' + @SQLWHEREPENDIENTESRECEPCIONAR" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "IF @NUMLINEAS>0" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET ROWCOUNT @NUMLINEAS" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   SET @SQLQUERY = 'SELECT DISTINCT OE.ID, OE.ANYO, PE.NUM NUMPEDIDO, OE.NUM NUMORDEN, OE.PROVE PROVECOD, P.DEN PROVEDEN, OE.EST" & vbCrLf
sConsulta = sConsulta & "       , OE.IMPORTE, OE.CAMBIO, OE.MON, OE.EMPRESA, E.DEN DENEMPRESA" & vbCrLf
sConsulta = sConsulta & "   FROM ORDEN_ENTREGA OE WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PEDIDO PE WITH (NOLOCK) ON OE.PEDIDO=PE.ID" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PROVE P WITH (NOLOCK) ON OE.PROVE=P.COD" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN EMP E WITH (NOLOCK) ON OE.EMPRESA=E.ID'" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   SET @SQLQUERY += @SQLQUERYPENDIENTESAPROBAR + @SQLQUERYPENDIENTESRECEPCIONARCC +" & vbCrLf
sConsulta = sConsulta & "       '" & vbCrLf
sConsulta = sConsulta & "       WHERE 1=1 ' +" & vbCrLf
sConsulta = sConsulta & "       @SQLWHEREESTADOS + @SQLWHEREPENDIENTESAPROBAR + @SQLWHEREPENDIENTESRECEPCIONAR" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQLQUERY," & vbCrLf
sConsulta = sConsulta & "                   N'@APROVISIONADOR VARCHAR(50), @EST INT, @IN NVARCHAR(20), @ABONO NVARCHAR(10)'," & vbCrLf
sConsulta = sConsulta & "                   @APROVISIONADOR = @APROVISIONADOR," & vbCrLf
sConsulta = sConsulta & "                   @EST = @EST," & vbCrLf
sConsulta = sConsulta & "                   @IN = @IN," & vbCrLf
sConsulta = sConsulta & "                   @ABONO = @ABONO" & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "   SET ROWCOUNT 0" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLQUERY = 'SELECT @NUMLINEAS=COUNT(*)" & vbCrLf
sConsulta = sConsulta & "FROM ORDEN_ENTREGA OE WITH (NOLOCK)'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLQUERY += @SQLQUERYESTADOS + @SQLQUERYPENDIENTESAPROBAR + @SQLQUERYPENDIENTESRECEPCIONARCC +" & vbCrLf
sConsulta = sConsulta & "   '" & vbCrLf
sConsulta = sConsulta & "   WHERE 1=1 ' +" & vbCrLf
sConsulta = sConsulta & "   @SQLWHEREESTADOS + @SQLWHEREPENDIENTESAPROBAR + @SQLWHEREPENDIENTESRECEPCIONAR" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLQUERY," & vbCrLf
sConsulta = sConsulta & "               N'@APROVISIONADOR VARCHAR(50), @EST INT, @IN NVARCHAR(20), @ABONO NVARCHAR(10), @NUMLINEAS INT OUTPUT'," & vbCrLf
sConsulta = sConsulta & "               @APROVISIONADOR = @APROVISIONADOR," & vbCrLf
sConsulta = sConsulta & "               @EST = @EST," & vbCrLf
sConsulta = sConsulta & "               @IN = @IN," & vbCrLf
sConsulta = sConsulta & "               @ABONO = @ABONO," & vbCrLf
sConsulta = sConsulta & "               @NUMLINEAS = @NUMLINEAS OUTPUT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSGS_INSERTAR_ART4_UON]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSGS_INSERTAR_ART4_UON]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSGS_INSERTAR_ART4_UON]   @ART VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON;" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Metemos las uo que tienen empresa con integracion E o E/S y las que tienen emp sin activar la integracion" & vbCrLf
sConsulta = sConsulta & "INSERT INTO ART4_UON (ART4,UON1,UON2,UON3)" & vbCrLf
sConsulta = sConsulta & "select DISTINCT @ART,uon1,uon2,uon3" & vbCrLf
sConsulta = sConsulta & "FROM" & vbCrLf
sConsulta = sConsulta & "(select u1.cod uon1,u2.cod uon2,u3.cod uon3,u4.cod uon4, coalesce(u4.empresa,u3.empresa,u2.empresa,u1.empresa) emp from uon1 u1 with (nolock)" & vbCrLf
sConsulta = sConsulta & "left join uon2 u2 with (nolock) on u1.cod=u2.uon1 and u1.BAJALOG=0 and u2.BAJALOG=0 and u2.EMPRESA is not null" & vbCrLf
sConsulta = sConsulta & "left join uon3 u3 with (nolock) on u1.cod=u3.uon1 and u2.cod=u3.uon2 and u3.BAJALOG=0 and u3.EMPRESA is not null" & vbCrLf
sConsulta = sConsulta & "left join uon4 u4 with (nolock) on u1.cod=u4.uon1 and u2.cod=u4.uon2 and u3.cod=u4.uon3 and u4.BAJALOG=0  and u4.EMPRESA is not null where u1.BAJALOG=0) empresa" & vbCrLf
sConsulta = sConsulta & "inner join emp with (nolock) on emp.id=empresa.emp" & vbCrLf
sConsulta = sConsulta & "inner join erp_sociedad e with (nolock) on e.SOCIEDAD=emp.SOCIEDAD" & vbCrLf
sConsulta = sConsulta & "inner join TABLAS_INTEGRACION_ERP ti  WITH(NOLOCK) on ti.ERP=e.ERP and TABLA=7 and coalesce(sentido,1)<>2" & vbCrLf
sConsulta = sConsulta & "order by uon1,uon2,uon3" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Metemos las que no tienen empresa" & vbCrLf
sConsulta = sConsulta & "INSERT INTO ART4_UON (ART4,UON1)" & vbCrLf
sConsulta = sConsulta & "select distinct @ART,cod from uon1  with (nolock)  WHERE NOT EXISTS (select * from (select u1.cod uon1,u2.cod uon2,u3.cod uon3,u4.cod uon4, coalesce(u4.empresa,u3.empresa,u2.empresa,u1.empresa) emp from uon1 u1 with (nolock)" & vbCrLf
sConsulta = sConsulta & "left join uon2 u2 with (nolock) on u1.cod=u2.uon1 and u1.BAJALOG=0 and u2.BAJALOG=0 and u2.EMPRESA is not null" & vbCrLf
sConsulta = sConsulta & "left join uon3 u3 with (nolock) on u1.cod=u3.uon1 and u2.cod=u3.uon2 and u3.BAJALOG=0 and u3.EMPRESA is not null" & vbCrLf
sConsulta = sConsulta & "left join uon4 u4 with (nolock) on u1.cod=u4.uon1 and u2.cod=u4.uon2 and u3.cod=u4.uon3 and u4.BAJALOG=0 and u4.EMPRESA is not null   where u1.BAJALOG=0) empresa" & vbCrLf
sConsulta = sConsulta & "inner join emp with (nolock) on emp.id=empresa.emp where uon1=uon1.cod)" & vbCrLf
sConsulta = sConsulta & "and bajalog=0 and cod not in (select uon1 from art4_uon  with (nolock) where art4=@ART)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO ART4_UON (ART4,UON1,uon2)" & vbCrLf
sConsulta = sConsulta & "select distinct @ART,uon1,cod from uon2 with (nolock) WHERE NOT EXISTS (select * from (select u1.cod uon1,u2.cod uon2,u3.cod uon3,u4.cod uon4, coalesce(u4.empresa,u3.empresa,u2.empresa,u1.empresa) emp from uon1 u1 with (nolock)" & vbCrLf
sConsulta = sConsulta & "left join uon2 u2 with (nolock) on u1.cod=u2.uon1 and u1.BAJALOG=0 and u2.BAJALOG=0 and u2.EMPRESA is not null" & vbCrLf
sConsulta = sConsulta & "left join uon3 u3 with (nolock) on u1.cod=u3.uon1 and u2.cod=u3.uon2 and u3.BAJALOG=0 and u3.EMPRESA is not null" & vbCrLf
sConsulta = sConsulta & "left join uon4 u4 with (nolock) on u1.cod=u4.uon1 and u2.cod=u4.uon2 and u3.cod=u4.uon3 and u4.BAJALOG=0 and u4.EMPRESA is not null   where u1.BAJALOG=0) empresa" & vbCrLf
sConsulta = sConsulta & "inner join emp with (nolock) on emp.id=empresa.emp where uon1=uon2.uon1 and coalesce(uon2,uon2.cod,'')=coalesce(uon2.cod,UON2,''))" & vbCrLf
sConsulta = sConsulta & "and bajalog=0 and not exists (select * from art4_uon with (nolock) where art4=@ART and uon1=uon2.uon1 and uon2=uon2.cod and uon3 is null)" & vbCrLf
sConsulta = sConsulta & "and not exists (select * from art4_uon with (nolock) where art4=@ART and uon1=uon2.uon1 and uon2 is null and uon3 is null)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO ART4_UON (ART4,UON1,uon2,UON3)" & vbCrLf
sConsulta = sConsulta & "select distinct @ART,uon1,UON2,cod from uon3 with (nolock) WHERE NOT EXISTS (select * from (select u1.cod uon1,u2.cod uon2,u3.cod uon3,u4.cod uon4, coalesce(u4.empresa,u3.empresa,u2.empresa,u1.empresa) emp from uon1 u1 with (nolock)" & vbCrLf
sConsulta = sConsulta & "left join uon2 u2 with (nolock) on u1.cod=u2.uon1 and u1.BAJALOG=0 and u2.BAJALOG=0 and u2.EMPRESA is not null" & vbCrLf
sConsulta = sConsulta & "left join uon3 u3 with (nolock) on u1.cod=u3.uon1 and u2.cod=u3.uon2 and u3.BAJALOG=0 and u3.EMPRESA is not null" & vbCrLf
sConsulta = sConsulta & "left join uon4 u4 with (nolock) on u1.cod=u4.uon1 and u2.cod=u4.uon2 and u3.cod=u4.uon3 and u4.BAJALOG=0 and u4.EMPRESA is not null   where u1.BAJALOG=0) empresa" & vbCrLf
sConsulta = sConsulta & "inner join emp with (nolock) on emp.id=empresa.emp where uon1=uon3.uon1 and coalesce(uon2,uon3.UON2,'')=coalesce(uon3.UON2,UON2,'') and coalesce(uon3,uon3.cod,'')=coalesce(uon3.cod,UON3,''))" & vbCrLf
sConsulta = sConsulta & "and bajalog=0 and not exists (select * from art4_uon with (nolock) where art4=@ART and uon1=uon3.uon1 and uon2=uon3.UON2 and uon3=uon3.cod)" & vbCrLf
sConsulta = sConsulta & "and not exists (select * from art4_uon with (nolock) where art4=@ART and uon1=uon3.uon1 and uon2 =uon3.uon2 and uon3 is null)" & vbCrLf
sConsulta = sConsulta & "and not exists (select * from art4_uon with (nolock) where art4=@ART and uon1=uon3.uon1 and uon2 is null and uon3 is null)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_GETWORKFLOW_PROVE]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_GETWORKFLOW_PROVE]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_GETWORKFLOW_PROVE] @PRV VARCHAR(50), @IDI VARCHAR(50), @TI INT =0 , @FEC  VARCHAR(10)=NULL, @INS INT=0, @FILAESTADO INT =0, @FORMATOFECHA VARCHAR(20)='' AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- Descripci?n: Carga los procesos que requieren la intervenci?n del usuario y el hist?rico de procesos en los que particip?." & vbCrLf
sConsulta = sConsulta & "-- Par?metros de entrada:@PRV: C?digo del proveedor" & vbCrLf
sConsulta = sConsulta & "--         @IDI: Idioma en el que se va a mostrar las denominaci" & vbCrLf
sConsulta = sConsulta & "--         @TI: Id de la solicitud" & vbCrLf
sConsulta = sConsulta & "--          @FEC: Fecha de alta de la solicitud" & vbCrLf
sConsulta = sConsulta & "--          @INS: Id de la instancia" & vbCrLf
sConsulta = sConsulta & "--          @FILAESTADO: Estado de las instancias" & vbCrLf
sConsulta = sConsulta & "--          @FORMATOFECHA: Formato de la fecha" & vbCrLf
sConsulta = sConsulta & "-- Par?metros de salida: Devuelve los procesos que requieren la intervenci?n del usuario y el hist?rico de procesos en los que particip?." & vbCrLf
sConsulta = sConsulta & "-- Tiempo m?ximo: 0 sg                         " & vbCrLf
sConsulta = sConsulta & "-- Llamada desde: El stored de portal FSPM_GETWORKFLOW_PROVE" & vbCrLf
sConsulta = sConsulta & "-- Revisado por MMV (05/11/2012)" & vbCrLf
sConsulta = sConsulta & "-------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SWHERE  AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLFROM AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLFROM1 AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLFROM2 AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLFROM3 AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLFROM4 AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SWHERE2  AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL2 AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @TIPOFECHA INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @TIPOFECHA=103" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @FORMATOFECHA='dd/mm/yyyy'" & vbCrLf
sConsulta = sConsulta & "SET @TIPOFECHA=103" & vbCrLf
sConsulta = sConsulta & "ELSE IF @FORMATOFECHA='dd/MM/yyyy'" & vbCrLf
sConsulta = sConsulta & "SET @TIPOFECHA=103" & vbCrLf
sConsulta = sConsulta & "ELSE IF @FORMATOFECHA='mm/dd/yyyy'" & vbCrLf
sConsulta = sConsulta & "SET @TIPOFECHA=101" & vbCrLf
sConsulta = sConsulta & "ELSE IF @FORMATOFECHA='dd-mm-yyyy'" & vbCrLf
sConsulta = sConsulta & "SET @TIPOFECHA=105" & vbCrLf
sConsulta = sConsulta & "ELSE IF @FORMATOFECHA='mm-dd-yyyy'" & vbCrLf
sConsulta = sConsulta & "SET @TIPOFECHA=110" & vbCrLf
sConsulta = sConsulta & "ELSE IF @FORMATOFECHA='mm.dd.yyyy'" & vbCrLf
sConsulta = sConsulta & "SET @TIPOFECHA=104" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SWHERE = ' WHERE TS.TIPO NOT IN (2,3,7)  AND I.NUM IS NOT NULL'" & vbCrLf
sConsulta = sConsulta & "IF @TI <> 0" & vbCrLf
sConsulta = sConsulta & "SET @SWHERE = @SWHERE +  ' AND I.SOLICITUD=@TI'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @FEC IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SET @SWHERE = @SWHERE + ' AND I.FEC_ALTA>=CONVERT(datetime,@FEC,103)'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @INS<>0" & vbCrLf
sConsulta = sConsulta & "SET @SWHERE = @SWHERE +  ' AND I.ID=@INS'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--------------------------------------------PROCESOS QUE REQUIEREN LA INTERVENCI?N DEL USUARIO (trasladados m?s los propios del rol)----------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'SELECT COUNT(*) AS PENDIENTES" & vbCrLf
sConsulta = sConsulta & "FROM (" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT I.ID" & vbCrLf
sConsulta = sConsulta & "FROM INSTANCIA I WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN SOLICITUD S WITH (NOLOCK) ON I.SOLICITUD=S.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN TIPO_SOLICITUDES TS WITH (NOLOCK) ON S.TIPO=TS.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN INSTANCIA_BLOQUE  WITH (NOLOCK) ON I.ID=INSTANCIA_BLOQUE.INSTANCIA AND INSTANCIA_BLOQUE.ESTADO=1 AND INSTANCIA_BLOQUE.BLOQ<>1 AND INSTANCIA_BLOQUE.TRASLADADA=0" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_COPIA_ROL_BLOQUE WITH (NOLOCK)  ON INSTANCIA_BLOQUE.BLOQUE=PM_COPIA_ROL_BLOQUE.BLOQUE" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_COPIA_ROL WITH (NOLOCK)  ON PM_COPIA_ROL_BLOQUE.ROL=PM_COPIA_ROL.ID AND PM_COPIA_ROL.PROVE=@PRV'" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL +  @SWHERE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ' UNION" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT I.ID" & vbCrLf
sConsulta = sConsulta & "FROM INSTANCIA I WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN SOLICITUD S WITH (NOLOCK) ON I.SOLICITUD=S.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN TIPO_SOLICITUDES TS WITH (NOLOCK) ON S.TIPO=TS.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN INSTANCIA_BLOQUE WITH (NOLOCK)  ON I.ID=INSTANCIA_BLOQUE.INSTANCIA AND INSTANCIA_BLOQUE.ESTADO=1 AND INSTANCIA_BLOQUE.BLOQ<>1 AND INSTANCIA_BLOQUE.TRASLADADA=1" & vbCrLf
sConsulta = sConsulta & "INNER JOIN (" & vbCrLf
sConsulta = sConsulta & "SELECT MAX(ID) ID, BLOQUE" & vbCrLf
sConsulta = sConsulta & "FROM INSTANCIA_EST" & vbCrLf
sConsulta = sConsulta & "GROUP BY BLOQUE" & vbCrLf
sConsulta = sConsulta & ") M ON INSTANCIA_BLOQUE.ID=M.BLOQUE" & vbCrLf
sConsulta = sConsulta & "INNER JOIN INSTANCIA_EST  WITH (NOLOCK) ON M.ID=INSTANCIA_EST.ID AND INSTANCIA_EST.DESTINATARIO_PROV =@PRV" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_COPIA_ROL R  WITH (NOLOCK) ON INSTANCIA_EST.ROL = R.ID'" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL +  @SWHERE + ' AND (I.ESTADO=2 OR I.ESTADO=3 OR I.ESTADO=4)) SUMA1 '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@PRV VARCHAR(50), @TI INT, @FEC VARCHAR(10), @INS  INT', @PRV = @PRV,@TI=@TI,@FEC=@FEC,@INS=@INS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "----------------------------------------------------PROCESOS EN LOS QUE PARTICIP? EL USUARIO--------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "-- 2.- En curso" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'SELECT COUNT(*) AS EN_CURSO" & vbCrLf
sConsulta = sConsulta & "FROM (" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT I.ID" & vbCrLf
sConsulta = sConsulta & "FROM INSTANCIA_EST WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN INSTANCIA I  WITH (NOLOCK) ON INSTANCIA_EST.INSTANCIA=I.ID AND INSTANCIA_EST.PROVE=@PRV AND INSTANCIA_EST.BLOQUE IS NOT NULL AND (I.ESTADO=2  OR I.ESTADO=3 OR I.ESTADO=4)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN SOLICITUD S WITH (NOLOCK) ON I.SOLICITUD=S.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN TIPO_SOLICITUDES TS WITH (NOLOCK) ON S.TIPO=TS.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_COPIA_ROL R  WITH (NOLOCK) ON INSTANCIA_EST.ROL = R.ID '" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL +  @SWHERE + ') SUMA2 '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@PRV VARCHAR(50), @TI INT, @FEC VARCHAR(10), @INS  INT', @PRV = @PRV,@TI=@TI,@FEC=@FEC,@INS=@INS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- 3.- Rechazadas" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'SELECT COUNT(*) AS RECHAZADAS" & vbCrLf
sConsulta = sConsulta & "FROM (" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT I.ID" & vbCrLf
sConsulta = sConsulta & "FROM INSTANCIA_EST WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN INSTANCIA I  WITH (NOLOCK) ON INSTANCIA_EST.INSTANCIA=I.ID AND INSTANCIA_EST.PROVE=@PRV AND INSTANCIA_EST.BLOQUE IS NOT NULL  AND I.ESTADO=6" & vbCrLf
sConsulta = sConsulta & "INNER JOIN SOLICITUD S WITH (NOLOCK) ON I.SOLICITUD=S.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN TIPO_SOLICITUDES TS WITH (NOLOCK) ON S.TIPO=TS.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_COPIA_ROL R  WITH (NOLOCK) ON INSTANCIA_EST.ROL = R.ID'" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL +  @SWHERE + ') SUMA3 '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@PRV VARCHAR(50), @TI INT, @FEC VARCHAR(10), @INS  INT', @PRV = @PRV,@TI=@TI,@FEC=@FEC,@INS=@INS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- 4.- Anuladas" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'SELECT COUNT(*) AS ANULADAS" & vbCrLf
sConsulta = sConsulta & "FROM (" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT I.ID" & vbCrLf
sConsulta = sConsulta & "FROM INSTANCIA_EST WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN INSTANCIA I  WITH (NOLOCK) ON INSTANCIA_EST.INSTANCIA=I.ID AND INSTANCIA_EST.PROVE=@PRV AND INSTANCIA_EST.BLOQUE IS NOT NULL  AND I.ESTADO=8" & vbCrLf
sConsulta = sConsulta & "INNER JOIN SOLICITUD S WITH (NOLOCK) ON I.SOLICITUD=S.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN TIPO_SOLICITUDES TS WITH (NOLOCK) ON S.TIPO=TS.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_COPIA_ROL R  WITH (NOLOCK) ON INSTANCIA_EST.ROL = R.ID'" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL +  @SWHERE + ' ) SUMA4 '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@PRV VARCHAR(50), @TI INT, @FEC VARCHAR(10), @INS  INT', @PRV = @PRV,@TI=@TI,@FEC=@FEC,@INS=@INS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- 4.- Finalizadas" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'SELECT COUNT(*) AS FINALIZADAS" & vbCrLf
sConsulta = sConsulta & "FROM (" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT I.ID" & vbCrLf
sConsulta = sConsulta & "FROM INSTANCIA_EST WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN INSTANCIA I  WITH (NOLOCK) ON INSTANCIA_EST.INSTANCIA=I.ID AND INSTANCIA_EST.PROVE=@PRV AND INSTANCIA_EST.BLOQUE IS NOT NULL  AND I.ESTADO>=100" & vbCrLf
sConsulta = sConsulta & "INNER JOIN SOLICITUD S WITH (NOLOCK) ON I.SOLICITUD=S.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN TIPO_SOLICITUDES TS WITH (NOLOCK) ON S.TIPO=TS.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_COPIA_ROL R  WITH (NOLOCK) ON INSTANCIA_EST.ROL = R.ID'" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL +  @SWHERE + ' ) SUMA5 '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@PRV VARCHAR(50), @TI INT, @FEC VARCHAR(10), @INS  INT', @PRV = @PRV,@TI=@TI,@FEC=@FEC,@INS=@INS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "----------------------------------------Devuelve las instancias que cumplen las condiciones:--------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--T?tulo: campo normal o atributo" & vbCrLf
sConsulta = sConsulta & "SET @SQLFROM2 =N'SELECT DISTINCT S.ID ID_SOLIC,S.COD,CASE WHEN C1.TITULO_FEC IS NULL THEN C1.TITULO_TEXT ELSE CONVERT(VARCHAR ,C1.TITULO_FEC,@TIPOFECHA) END DESCR, I.ID ID_INS,IE.FECHA," & vbCrLf
sConsulta = sConsulta & "CASE WHEN IE.PER IS NOT NULL THEN" & vbCrLf
sConsulta = sConsulta & "E.NOM + '' '' + E.APE" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "PR.DEN" & vbCrLf
sConsulta = sConsulta & "END NOM_USU," & vbCrLf
sConsulta = sConsulta & "D.DEN ORIGEN,I.ESTADO," & vbCrLf
sConsulta = sConsulta & "CASE WHEN M.PARALELO>1 THEN" & vbCrLf
sConsulta = sConsulta & "NULL" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "ISNULL(CB.DEN,'' '')" & vbCrLf
sConsulta = sConsulta & "END DENETAPA_ACTUAL, R.VER_DETALLE_PER, I.EN_PROCESO, TS.TIPO,C.ID CONTRATO,C.CODIGO CODIGO_CONTRATO" & vbCrLf
sConsulta = sConsulta & "FROM INSTANCIA I  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN SOLICITUD S WITH (NOLOCK)  ON I.SOLICITUD=S.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN TIPO_SOLICITUDES TS WITH (NOLOCK) ON S.TIPO=TS.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN" & vbCrLf
sConsulta = sConsulta & "(" & vbCrLf
sConsulta = sConsulta & "SELECT INSTANCIA, COUNT(*) AS PARALELO,MIN(BLOQUE) AS BLOQUE" & vbCrLf
sConsulta = sConsulta & "FROM INSTANCIA_BLOQUE  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE ESTADO=1 AND BLOQ<>1" & vbCrLf
sConsulta = sConsulta & "GROUP BY INSTANCIA" & vbCrLf
sConsulta = sConsulta & ") M ON I.ID=M.INSTANCIA" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN INSTANCIA_VISORES_PM C1 WITH (NOLOCK)  ON C1.INSTANCIA=I.ID" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN CONTRATO C WITH (NOLOCK) ON I.ID=C.INSTANCIA  '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @FILAESTADO=0 --Pendientes:" & vbCrLf
sConsulta = sConsulta & "begin" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL   + ' ORDER BY I.ID'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL= @SQLFROM2 + '" & vbCrLf
sConsulta = sConsulta & "INNER JOIN INSTANCIA_BLOQUE IB WITH (NOLOCK)  ON M.INSTANCIA= IB.INSTANCIA AND IB.ESTADO=1 AND IB.BLOQ<>1 AND IB.TRASLADADA=0" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_COPIA_ROL_BLOQUE CRB  WITH (NOLOCK) ON IB.BLOQUE=CRB.BLOQUE" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_COPIA_ROL R  WITH (NOLOCK) ON CRB.ROL=R.ID AND R.PROVE=@PRV" & vbCrLf
sConsulta = sConsulta & "INNER JOIN INSTANCIA_CAMINO  WITH (NOLOCK) ON INSTANCIA_BLOQUE_DESTINO=IB.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN INSTANCIA_EST IE  WITH (NOLOCK) ON INSTANCIA_CAMINO.INSTANCIA_EST=IE.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_COPIA_BLOQUE_DEN CB WITH (NOLOCK)  ON IB.BLOQUE=CB.BLOQUE AND CB.IDI= @IDI" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PROVE PR WITH (NOLOCK)  ON IE.PROVE=PR.COD" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PER E WITH (NOLOCK)  ON IE.PER=E.COD" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN DEP D  WITH (NOLOCK) ON E.DEP=D.COD'" & vbCrLf
sConsulta = sConsulta & "+ @SWHERE + ' AND (I.ESTADO=2 OR I.ESTADO=3 OR I.ESTADO=4)'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL= @SQL + ' UNION ' + @SQLFROM2 + '" & vbCrLf
sConsulta = sConsulta & "INNER JOIN INSTANCIA_BLOQUE IB WITH (NOLOCK)  ON M.INSTANCIA= IB.INSTANCIA AND IB.ESTADO=1 AND IB.BLOQ<>1 AND IB.TRASLADADA=1" & vbCrLf
sConsulta = sConsulta & "INNER JOIN" & vbCrLf
sConsulta = sConsulta & "(" & vbCrLf
sConsulta = sConsulta & "SELECT MAX(ID) ID, BLOQUE" & vbCrLf
sConsulta = sConsulta & "FROM INSTANCIA_EST" & vbCrLf
sConsulta = sConsulta & "GROUP BY BLOQUE" & vbCrLf
sConsulta = sConsulta & ") P ON IB.ID=P.BLOQUE" & vbCrLf
sConsulta = sConsulta & "INNER JOIN INSTANCIA_EST IE  WITH (NOLOCK) ON P.ID=IE.ID AND IE.DESTINATARIO_PROV =@PRV" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_COPIA_ROL R  WITH (NOLOCK) ON IE.ROL = R.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_COPIA_BLOQUE_DEN CB  WITH (NOLOCK) ON IB.BLOQUE=CB.BLOQUE AND CB.IDI=@IDI" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PROVE PR  WITH (NOLOCK) ON IE.PROVE=PR.COD" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PER E  WITH (NOLOCK) ON IE.PER=E.COD" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN DEP D  WITH (NOLOCK) ON E.DEP=D.COD'" & vbCrLf
sConsulta = sConsulta & "+ @SWHERE + ' AND (I.ESTADO=2 OR I.ESTADO=3 OR I.ESTADO=4)'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL   + ' ORDER BY I.ID'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL,N'@PRV VARCHAR(50),@IDI VARCHAR(20),@TI INT,@FEC VARCHAR(10),@INS INT,@TIPOFECHA INT',@PRV = @PRV,@IDI=@IDI,@TI=@TI,@FEC=@FEC,@INS=@INS,@TIPOFECHA=@TIPOFECHA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "end" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "begin" & vbCrLf
sConsulta = sConsulta & "IF  @FILAESTADO=2   --2 Rechazadas" & vbCrLf
sConsulta = sConsulta & "SET @SWHERE = @SWHERE + ' AND I.ESTADO=6'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "begin" & vbCrLf
sConsulta = sConsulta & "IF  @FILAESTADO=3   --3 Anuladas" & vbCrLf
sConsulta = sConsulta & "SET @SWHERE = @SWHERE + ' AND I.ESTADO=8'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "begin" & vbCrLf
sConsulta = sConsulta & "IF  @FILAESTADO=4   ---4 finalizadas" & vbCrLf
sConsulta = sConsulta & "SET @SWHERE = @SWHERE + ' AND I.ESTADO>=100'" & vbCrLf
sConsulta = sConsulta & "ELSE  --1 En curso" & vbCrLf
sConsulta = sConsulta & "SET @SWHERE = @SWHERE + ' AND (I.ESTADO=2 OR I.ESTADO=3 OR I.ESTADO=4)'" & vbCrLf
sConsulta = sConsulta & "end" & vbCrLf
sConsulta = sConsulta & "end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL= N'" & vbCrLf
sConsulta = sConsulta & "INNER JOIN INSTANCIA_BLOQUE IB  WITH (NOLOCK) ON M.INSTANCIA= IB.INSTANCIA AND IB.ESTADO=1 AND IB.BLOQ<>1" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PM_COPIA_BLOQUE_DEN CB  WITH (NOLOCK) ON IB.BLOQUE=CB.BLOQUE AND CB.IDI=@IDI" & vbCrLf
sConsulta = sConsulta & "INNER JOIN INSTANCIA_EST IE  WITH (NOLOCK) ON I.ID=IE.INSTANCIA AND IE.PROVE=@PRV AND IE.BLOQUE IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "AND IE.ID = (SELECT MAX(ID) FROM INSTANCIA_EST  WITH (NOLOCK) WHERE INSTANCIA_EST.INSTANCIA=I.ID  AND INSTANCIA_EST.PROVE=@PRV  AND INSTANCIA_EST.BLOQUE IS NOT NULL)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_COPIA_ROL R  WITH (NOLOCK) ON IE.ROL = R.ID" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PROVE PR WITH (NOLOCK)  ON IE.PROVE=PR.COD" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PER E WITH (NOLOCK)  ON IE.PER=E.COD" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN DEP D WITH (NOLOCK)  ON E.DEP=D.COD'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLFROM2 = @SQLFROM2 + @SQL  + @SWHERE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLFROM2,N'@PRV VARCHAR(50),@IDI VARCHAR(20),@TI INT,@FEC VARCHAR(10),@INS INT,@TIPOFECHA INT',@PRV = @PRV,@IDI=@IDI,@TI=@TI,@FEC=@FEC,@INS=@INS,@TIPOFECHA=@TIPOFECHA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSEP_INSERTAR_ORDEN_LINEA_PEDIDO]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSEP_INSERTAR_ORDEN_LINEA_PEDIDO]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSEP_INSERTAR_ORDEN_LINEA_PEDIDO] @CAMBIO Float, @PER VarChar," & vbCrLf
sConsulta = sConsulta & "@PROVE VarChar(20), @MON NVarChar(3), @PEDIDO Int," & vbCrLf
sConsulta = sConsulta & "@REFERENCIA VarChar(100), @OBSERVACIONES VarChar(2000)," & vbCrLf
sConsulta = sConsulta & "@EMPRESA Int, @RECEPTOR VarChar(20)," & vbCrLf
sConsulta = sConsulta & "@CODERP VarChar(100), @TIPOPEDIDO Int," & vbCrLf
sConsulta = sConsulta & "@DIRECCIONENVIOFACTURA Int, @IM_AUTOFACTURA TinyInt," & vbCrLf
sConsulta = sConsulta & "@IM_FACTRASRECEP TinyInt, @CESTA Int, @IDIOMA VARCHAR(3), @IMPORTE Float," & vbCrLf
sConsulta = sConsulta & "@ORDEN INT = 0 OUTPUT,@NUM_ORDEN INT = 0 OUTPUT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @COSTES FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @DCTOS FLOAT" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON;" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--DECLARE @IMPORTE FLOAT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--SELECT @IMPORTE=SUM(IMPORTE) FROM (" & vbCrLf
sConsulta = sConsulta & "--    SELECT SUM(CASE WHEN CE.TIPORECEPCION = 0 THEN ((CE.PREC * CE.CANT * CE.FC)  / 1) ELSE (ISNULL(CE.IMPORTE_PED/ 1,0)  )  END  )AS IMPORTE" & vbCrLf
sConsulta = sConsulta & "-- FROM  CESTA CE  WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "-- LEFT JOIN CATALOG_LIN CL WITH(NOLOCK) ON CE.LINEA = CL.ID AND CL.ID IS NULL" & vbCrLf
sConsulta = sConsulta & "-- WHERE CE.CESTACABECERAID = @CESTA AND CE.TIPO<>99) AUX" & vbCrLf
sConsulta = sConsulta & "--UNION SELECT (SUM(ISNULL(CE.PREC,CL.PRECIO_UC) * CE.CANT * CE.FC ) / @CAMBIO ) AS IMPORTE FROM CESTA CE INNER JOIN CATALOG_LIN CL ON CE.LINEA = CL.ID" & vbCrLf
sConsulta = sConsulta & "--WHERE CE.CESTACABECERAID = @CESTA AND CL.PROVE=@PROVE AND CE.MON =@MON AND CE.TIPO<>99" & vbCrLf
sConsulta = sConsulta & "--UNION SELECT SUM(ISNULL(CE.IMPORTE_PED,0)  / @CAMBIO ) AS IMPORTE  FROM  CESTA CE  WHERE CE.CESTACABECERAID = @CESTA AND CE.TIPO<>99 ) AUX" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO ORDEN_ENTREGA (PEDIDO, ANYO, PROVE, NUM, EST, TIPO, FECHA, IMPORTE, MON, CAMBIO, NUM_PED_ERP, OBS, EMPRESA, COD_PROVE_ERP, RECEPTOR, PAG, TIPOPEDIDO, FAC_DIR_ENVIO, IM_AUTOFACTURA)" & vbCrLf
sConsulta = sConsulta & "SELECT @PEDIDO, YEAR(GETDATE()), @PROVE, ISNULL(MAX(O.NUM),0)+1, 0, 1, GETDATE(), @IMPORTE, @MON, @CAMBIO, @REFERENCIA, @OBSERVACIONES, @EMPRESA, @CODERP, @RECEPTOR, PROVE.PAG, @TIPOPEDIDO, @DIRECCIONENVIOFACTURA, @IM_AUTOFACTURA" & vbCrLf
sConsulta = sConsulta & "FROM PROVE WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN (SELECT ANYO, NUM, PROVE FROM ORDEN_ENTREGA WITH(NOLOCK)) O ON O.ANYO=YEAR(GETDATE())" & vbCrLf
sConsulta = sConsulta & "WHERE PROVE.COD=@PROVE" & vbCrLf
sConsulta = sConsulta & "GROUP BY PROVE.PAG" & vbCrLf
sConsulta = sConsulta & "SELECT @ORDEN=SCOPE_IDENTITY()" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @NUM_ORDEN=OE.NUM FROM ORDEN_ENTREGA OE WITH (NOLOCK) WHERE ID=@ORDEN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_EMPRESA_COD_ERP SET ULTIMO=1 WHERE COD_ERP=@CODERP AND PROVE=@PROVE AND EMPRESA=@EMPRESA" & vbCrLf
sConsulta = sConsulta & "DELETE FROM PROCE_PEDIDO" & vbCrLf
sConsulta = sConsulta & "FROM PROCE_PEDIDO PR WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PEDIDO P WITH(NOLOCK) ON P.NUM=PR.PED_CESTA" & vbCrLf
sConsulta = sConsulta & "INNER JOIN ORDEN_ENTREGA O WITH(NOLOCK) ON O.NUM=PR.PED_ORDEN" & vbCrLf
sConsulta = sConsulta & "WHERE P.ID=@PEDIDO AND O.ID=@ORDEN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO ORDEN_ENTREGA_ADJUN(ORDEN, NOM, COM, DATA, DATASIZE)" & vbCrLf
sConsulta = sConsulta & "SELECT @ORDEN, NOM, COM, DATA, DATASIZE FROM CESTA_CABECERA_ADJUN CCA WITH (NOLOCK) WHERE CCA.CESTACABECERAID = @CESTA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO ORDEN_ENTREGA_ATRIB ([ORDEN]" & vbCrLf
sConsulta = sConsulta & ",[ATRIB]" & vbCrLf
sConsulta = sConsulta & ",[VALOR_NUM]" & vbCrLf
sConsulta = sConsulta & ",[VALOR_TEXT]" & vbCrLf
sConsulta = sConsulta & ",[VALOR_FEC]" & vbCrLf
sConsulta = sConsulta & ",[VALOR_BOOL]" & vbCrLf
sConsulta = sConsulta & ",[INTERNO]" & vbCrLf
sConsulta = sConsulta & ",[INTRO]" & vbCrLf
sConsulta = sConsulta & ",[MINNUM]" & vbCrLf
sConsulta = sConsulta & ",[MAXNUM]" & vbCrLf
sConsulta = sConsulta & ",[MINFEC]" & vbCrLf
sConsulta = sConsulta & ",[MAXFEC]" & vbCrLf
sConsulta = sConsulta & ",[ORD]" & vbCrLf
sConsulta = sConsulta & ",[OBLIGATORIO])" & vbCrLf
sConsulta = sConsulta & "SELECT @ORDEN, CCA.ATRIB, (CASE WHEN CCA.VALOR_NUM IS NULL THEN DF.VALOR_NUM ELSE CCA.VALOR_NUM END) AS VALOR_NUM," & vbCrLf
sConsulta = sConsulta & "(CASE WHEN CCA.VALOR_TEXT IS NULL THEN DF.VALOR_TEXT ELSE CCA.VALOR_TEXT END) AS VALOR_TEXT," & vbCrLf
sConsulta = sConsulta & "(CASE WHEN CCA.VALOR_FEC IS NULL THEN DF.VALOR_FEC ELSE CCA.VALOR_FEC END) AS VALOR_FEC," & vbCrLf
sConsulta = sConsulta & "(CASE WHEN CCA.VALOR_BOOL IS NULL THEN DF.VALOR_BOOL ELSE CCA.VALOR_BOOL END) AS VALOR_BOOL," & vbCrLf
sConsulta = sConsulta & "CCA.INTERNO, DF.INTRO, DF.MINNUM, DF.MAXNUM, DF.MINFEC, DF.MAXFEC, 1, CCA.OBLIGATORIO" & vbCrLf
sConsulta = sConsulta & "from CESTA_CABECERA_ATRIB CCA WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN DEF_ATRIB DF  WITH (NOLOCK) on CCA.ATRIB = DF.ID" & vbCrLf
sConsulta = sConsulta & "where CCA.CESTACABECERAID = @CESTA AND CCA.TIPO = 1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO ORDEN_ENTREGA_ATRIB_LISTA([ORDEN]" & vbCrLf
sConsulta = sConsulta & ",[ATRIB]" & vbCrLf
sConsulta = sConsulta & ",[NUMORDEN]" & vbCrLf
sConsulta = sConsulta & ",[VALOR_NUM]" & vbCrLf
sConsulta = sConsulta & ",[VALOR_TEXT]" & vbCrLf
sConsulta = sConsulta & ",[VALOR_FEC])" & vbCrLf
sConsulta = sConsulta & "SELECT CCA.ORDEN, CCA.ATRIB, DFL.ORDEN, DFL.VALOR_NUM AS VALOR_NUM , DFL.VALOR_TEXT,DFL.VALOR_FEC" & vbCrLf
sConsulta = sConsulta & "FROM ORDEN_ENTREGA_ATRIB CCA WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN DEF_ATRIB_LISTA DFL WITH (NOLOCK) ON CCA.ATRIB = DFL.ATRIB" & vbCrLf
sConsulta = sConsulta & "INNER JOIN DEF_ATRIB DF WITH (NOLOCK) ON DF.ID = DFL.ATRIB AND DF.INTRO=1" & vbCrLf
sConsulta = sConsulta & "WHERE CCA.ORDEN = @ORDEN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO ORDEN_ENTREGA_CD ([ORDEN]" & vbCrLf
sConsulta = sConsulta & ",[ATRIB]" & vbCrLf
sConsulta = sConsulta & ",[TIPO]" & vbCrLf
sConsulta = sConsulta & ",[OPERACION]" & vbCrLf
sConsulta = sConsulta & ",[VALOR]" & vbCrLf
sConsulta = sConsulta & ",[IMPORTE]" & vbCrLf
sConsulta = sConsulta & ",[FACTURAR]" & vbCrLf
sConsulta = sConsulta & ",[DESCR])" & vbCrLf
sConsulta = sConsulta & "SELECT @ORDEN, CCA.ATRIB," & vbCrLf
sConsulta = sConsulta & "CASE WHEN CCA.TIPO = 2 THEN 0 ELSE 1 END AS TIPO, CASE WHEN LEN(CCA.OPERACION) = 1  THEN 0 ELSE 1 END AS OPERACION, CASE WHEN LEN(CCA.OPERACION) = 1  THEN CCA.VALOR_NUM/@CAMBIO ELSE CCA.VALOR_NUM END, CCA.VALOR/@CAMBIO, 0, DF.DEN" & vbCrLf
sConsulta = sConsulta & "FROM CESTA_CABECERA_ATRIB CCA WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "left JOIN DEF_ATRIB DF WITH (NOLOCK) ON CCA.ATRIB = DF.ID" & vbCrLf
sConsulta = sConsulta & "where CCA.CESTACABECERAID = @CESTA AND (CCA.TIPO = 2 or CCA.TIPO = 3)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO ORDEN_ENTREGA_CD_LISTA([ID]" & vbCrLf
sConsulta = sConsulta & ",[NUMORDEN]" & vbCrLf
sConsulta = sConsulta & ",[VALOR_NUM])" & vbCrLf
sConsulta = sConsulta & "SELECT CCA.ID, DFL.ORDEN, DFL.VALOR_NUM AS VALOR_NUM" & vbCrLf
sConsulta = sConsulta & "FROM ORDEN_ENTREGA_CD CCA WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN DEF_ATRIB_LISTA DFL WITH (NOLOCK) ON CCA.ATRIB = DFL.ATRIB" & vbCrLf
sConsulta = sConsulta & "INNER JOIN DEF_ATRIB DF WITH (NOLOCK) ON DF.ID = DFL.ATRIB AND DF.INTRO=1" & vbCrLf
sConsulta = sConsulta & "WHERE CCA.ORDEN = @ORDEN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @COSTES=isnull(SUM(IMPORTE),0) FROM ORDEN_ENTREGA_CD WITH (NOLOCK) WHERE ORDEN=@ORDEN AND TIPO=0" & vbCrLf
sConsulta = sConsulta & "SELECT @DCTOS=isnull(SUM(IMPORTE),0) FROM ORDEN_ENTREGA_CD WITH (NOLOCK) WHERE ORDEN=@ORDEN AND TIPO=1" & vbCrLf
sConsulta = sConsulta & "UPDATE ORDEN_ENTREGA SET COSTES=@COSTES,DESCUENTOS=@DCTOS FROM ORDEN_ENTREGA O WITH (NOLOCK) WHERE ID=@ORDEN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO LINEAS_PEDIDO" & vbCrLf
sConsulta = sConsulta & "(PEDIDO, ORDEN, ART_INT, PROVE, UP, PREC_UP, CANT_PED, DEST, ANYO, GMN1, PROCE, ITEM," & vbCrLf
sConsulta = sConsulta & "FECEMISION, ENTREGA_OBL, FECENTREGA, EST, CANT_REC, CAT1, CAT2, CAT3, CAT4, CAT5," & vbCrLf
sConsulta = sConsulta & "NIVEL_APROB,OBS,FC,SOLICIT, DESCR_LIBRE, CAMPO1, VALOR1, CAMPO2, VALOR2,CESTA,OBSADJUN,LINCAT,ALMACEN, IM_FACTRASRECEP, NUM, PORCEN_DESVIO, TIPORECEPCION, IMPORTE_PED, SEGURIDAD_CATN, SEGURIDAD_NIVEL)" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT @PEDIDO, @ORDEN, CL.ART_INT," & vbCrLf
sConsulta = sConsulta & "CASE CE.TIPO WHEN 0 THEN CL.PROVE WHEN 1 THEN CE.PROVE END PROVE," & vbCrLf
sConsulta = sConsulta & "CE.UP, CASE CE.TIPORECEPCION WHEN 1 THEN NULL ELSE CE.PREC*CE.FC / @CAMBIO END IMPORTE,  CASE CE.TIPORECEPCION WHEN 1 THEN NULL ELSE CE.CANT END CANT_PED, CE.DEST," & vbCrLf
sConsulta = sConsulta & "CASE CE.TIPO WHEN 0 THEN CL.ANYO WHEN 1 THEN NULL END ANYO," & vbCrLf
sConsulta = sConsulta & "CASE CE.TIPO WHEN 0 THEN CL.GMN1 WHEN 1 THEN NULL END GMN1," & vbCrLf
sConsulta = sConsulta & "CASE CE.TIPO WHEN 0 THEN CL.PROCE WHEN 1 THEN NULL END PROCE," & vbCrLf
sConsulta = sConsulta & "CASE CE.TIPO WHEN 0 THEN CL.ITEM WHEN 1 THEN NULL END ITEM," & vbCrLf
sConsulta = sConsulta & "GETDATE(), ISNULL(CE.ENTREGA_OBL,0), CE.FECENTREGA, 22 EST, 0 CANT_REC," & vbCrLf
sConsulta = sConsulta & "CASE CE.TIPO WHEN 0 THEN CL.CAT1 WHEN 1 THEN CE.CAT1 END CAT1," & vbCrLf
sConsulta = sConsulta & "CASE CE.TIPO WHEN 0 THEN CL.CAT2 WHEN 1 THEN CE.CAT2 END CAT2," & vbCrLf
sConsulta = sConsulta & "CASE CE.TIPO WHEN 0 THEN CL.CAT3 WHEN 1 THEN CE.CAT3 END CAT3," & vbCrLf
sConsulta = sConsulta & "CASE CE.TIPO WHEN 0 THEN CL.CAT4 WHEN 1 THEN CE.CAT4 END CAT4," & vbCrLf
sConsulta = sConsulta & "CASE CE.TIPO WHEN 0 THEN CL.CAT5 WHEN 1 THEN CE.CAT5 END CAT5," & vbCrLf
sConsulta = sConsulta & "-1 NIVEL_APROB, CE.OBS,CE.FC," & vbCrLf
sConsulta = sConsulta & "CASE CE.TIPO WHEN 0 THEN CL.SOLICIT WHEN 1 THEN NULL END SOLICIT," & vbCrLf
sConsulta = sConsulta & "CASE CE.TIPO WHEN 0 THEN CASE WHEN A4.GENERICO=1 THEN CE.DESCR ELSE CL.DEN END WHEN 1 THEN CE.DESCR END DESCR_LIBRE," & vbCrLf
sConsulta = sConsulta & "CE.Campo1 , CE.VALOR1, CE.Campo2, CE.VALOR2, CE.ID, CE.OBSADJUN, CE.LINEA,CE.ALMACEN, @IM_FACTRASRECEP," & vbCrLf
sConsulta = sConsulta & "ROW_NUMBER() OVER (PARTITION BY CE.PROVE, CE.MON ORDER BY CE.ID), CL.PORCEN_DESVIO, CE.TIPORECEPCION, CE.IMPORTE_PED/@CAMBIO, CL.SEGURIDAD_CATN, SEGURIDAD_NIVEL" & vbCrLf
sConsulta = sConsulta & "FROM CESTA CE WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN CATALOG_LIN CL WITH(NOLOCK) ON CE.LINEA=CL.ID" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN ART4 A4 WITH (NOLOCK) ON A4.COD = CL.ART_INT" & vbCrLf
sConsulta = sConsulta & "INNER JOIN ORDEN_ENTREGA OE WITH(NOLOCK) ON OE.PROVE=CASE CE.TIPO WHEN 0 THEN CL.PROVE WHEN 1 THEN CE.PROVE END" & vbCrLf
sConsulta = sConsulta & "WHERE OE.PEDIDO=@PEDIDO AND OE.PROVE=@PROVE AND CE.CESTACABECERAID=@CESTA AND CE.TIPO<>99" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO LINEAS_PEDIDO_ATRIB([LINEA]" & vbCrLf
sConsulta = sConsulta & ",[ATRIB]" & vbCrLf
sConsulta = sConsulta & ",[VALOR_NUM]" & vbCrLf
sConsulta = sConsulta & ",[VALOR_TEXT]" & vbCrLf
sConsulta = sConsulta & ",[VALOR_FEC]" & vbCrLf
sConsulta = sConsulta & ",[VALOR_BOOL]" & vbCrLf
sConsulta = sConsulta & ",[INTERNO]" & vbCrLf
sConsulta = sConsulta & ",[INTRO]" & vbCrLf
sConsulta = sConsulta & ",[MINNUM]" & vbCrLf
sConsulta = sConsulta & ",[MAXNUM]" & vbCrLf
sConsulta = sConsulta & ",[MINFEC]" & vbCrLf
sConsulta = sConsulta & ",[MAXFEC]" & vbCrLf
sConsulta = sConsulta & ",[ORD]" & vbCrLf
sConsulta = sConsulta & ",[OBLIGATORIO])" & vbCrLf
sConsulta = sConsulta & "SELECT LP.ID, CCA.ATRIB, (CASE WHEN CCA.VALOR_NUM IS NULL THEN DF.VALOR_NUM ELSE CCA.VALOR_NUM END) AS VALOR_NUM," & vbCrLf
sConsulta = sConsulta & "(CASE WHEN CCA.VALOR_TEXT IS NULL THEN DF.VALOR_TEXT ELSE CCA.VALOR_TEXT END) AS VALOR_TEXT," & vbCrLf
sConsulta = sConsulta & "(CASE WHEN CCA.VALOR_FEC IS NULL THEN DF.VALOR_FEC ELSE CCA.VALOR_FEC END) AS VALOR_FEC," & vbCrLf
sConsulta = sConsulta & "(CASE WHEN CCA.VALOR_BOOL IS NULL THEN DF.VALOR_BOOL ELSE CCA.VALOR_BOOL END) AS VALOR_BOOL," & vbCrLf
sConsulta = sConsulta & "CCA.INTERNO, DF.INTRO, DF.MINNUM, DF.MAXNUM, DF.MINFEC, DF.MAXFEC, 1, CCA.OBLIGATORIO" & vbCrLf
sConsulta = sConsulta & "from LINEAS_PEDIDO LP WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN CESTA_ATRIB CCA  WITH (NOLOCK) on LP.CESTA = CCA.CESTA" & vbCrLf
sConsulta = sConsulta & "INNER JOIN DEF_ATRIB DF  WITH (NOLOCK) on CCA.ATRIB = DF.ID" & vbCrLf
sConsulta = sConsulta & "where LP.ORDEN = @ORDEN AND CCA.TIPO = 1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO LINEAS_PEDIDO_ADJUN(LINEA, NOM, COM, DATA, DATASIZE)" & vbCrLf
sConsulta = sConsulta & "SELECT LP.ID, CA.NOM, CA.COM, CA.DATA, CA.DATASIZE" & vbCrLf
sConsulta = sConsulta & "from LINEAS_PEDIDO LP WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN CESTA CT WITH (NOLOCK) on LP.CESTA = CT.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN CESTA_ADJUN CA  WITH (NOLOCK) on CT.ID = CA.CESTA_LINEA" & vbCrLf
sConsulta = sConsulta & "where CT.CESTACABECERAID = @CESTA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO LINEAS_PEDIDO_ATRIB_LISTA([LINEA]" & vbCrLf
sConsulta = sConsulta & ",[ATRIB]" & vbCrLf
sConsulta = sConsulta & ",[NUMORDEN]" & vbCrLf
sConsulta = sConsulta & ",[VALOR_NUM]" & vbCrLf
sConsulta = sConsulta & ",[VALOR_TEXT]" & vbCrLf
sConsulta = sConsulta & ",[VALOR_FEC])" & vbCrLf
sConsulta = sConsulta & "SELECT LPA.LINEA, LPA.ATRIB, DFL.ORDEN, DFL.VALOR_NUM, DFL.VALOR_TEXT,DFL.VALOR_FEC" & vbCrLf
sConsulta = sConsulta & "FROM LINEAS_PEDIDO_ATRIB LPA WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN LINEAS_PEDIDO LP WITH (NOLOCK)  on LPA.LINEA = LP.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN CESTA CT WITH (NOLOCK) on LP.CESTA = CT.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN DEF_ATRIB_LISTA DFL WITH (NOLOCK) ON LPA.ATRIB = DFL.ATRIB" & vbCrLf
sConsulta = sConsulta & "INNER JOIN DEF_ATRIB DF WITH (NOLOCK) ON DF.ID = DFL.ATRIB AND DF.INTRO=1" & vbCrLf
sConsulta = sConsulta & "where CT.CESTACABECERAID = @CESTA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO LINEAS_PEDIDO_CD ([LINEA]" & vbCrLf
sConsulta = sConsulta & ",[ATRIB]" & vbCrLf
sConsulta = sConsulta & ",[TIPO]" & vbCrLf
sConsulta = sConsulta & ",[OPERACION]" & vbCrLf
sConsulta = sConsulta & ",[VALOR]" & vbCrLf
sConsulta = sConsulta & ",[IMPORTE]" & vbCrLf
sConsulta = sConsulta & ",[FACTURAR]" & vbCrLf
sConsulta = sConsulta & ",[DESCR])" & vbCrLf
sConsulta = sConsulta & "SELECT LP.ID, CCA.ATRIB," & vbCrLf
sConsulta = sConsulta & "CASE WHEN CCA.TIPO = 2 THEN 0 ELSE 1 END AS TIPO, CASE WHEN LEN(CCA.OPERACION) = 1  THEN 0 ELSE 1 END AS OPERACION, CASE WHEN LEN(CCA.OPERACION) = 1  THEN CCA.VALOR_NUM/@CAMBIO ELSE CCA.VALOR_NUM END, CCA.VALOR/@CAMBIO, 0, DF.DEN" & vbCrLf
sConsulta = sConsulta & "from LINEAS_PEDIDO LP WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN CESTA CT WITH (NOLOCK) on LP.CESTA = CT.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN CESTA_ATRIB CCA  WITH (NOLOCK) on CT.ID = CCA.CESTA" & vbCrLf
sConsulta = sConsulta & "INNER JOIN DEF_ATRIB DF  WITH (NOLOCK) on CCA.ATRIB = DF.ID" & vbCrLf
sConsulta = sConsulta & "where CT.CESTACABECERAID = @CESTA AND (CCA.TIPO = 2 or CCA.TIPO = 3)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO LINEAS_PEDIDO_CD_LISTA([ID]" & vbCrLf
sConsulta = sConsulta & ",[NUMORDEN]" & vbCrLf
sConsulta = sConsulta & ",[VALOR_NUM])" & vbCrLf
sConsulta = sConsulta & "SELECT CCA.ID, DFL.ORDEN, DFL.VALOR_NUM AS VALOR_NUM" & vbCrLf
sConsulta = sConsulta & "FROM LINEAS_PEDIDO_CD CCA WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN DEF_ATRIB_LISTA DFL WITH (NOLOCK) ON CCA.ATRIB = DFL.ATRIB" & vbCrLf
sConsulta = sConsulta & "INNER JOIN DEF_ATRIB DF WITH (NOLOCK) ON DF.ID = DFL.ATRIB AND DF.INTRO=1" & vbCrLf
sConsulta = sConsulta & "INNER JOIN LINEAS_PEDIDO LP WITH (NOLOCK) ON CCA.LINEA = LP.ID" & vbCrLf
sConsulta = sConsulta & "WHERE LP.ORDEN = @ORDEN" & vbCrLf
sConsulta = sConsulta & "SET @COSTES=NULL" & vbCrLf
sConsulta = sConsulta & "SET @DCTOS=NULL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE LINEAS_PEDIDO SET COSTES=ISNULL(CD.IMPORTE,0) FROM LINEAS_PEDIDO L WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN (SELECT LINEA,SUM(IMPORTE) IMPORTE FROM LINEAS_PEDIDO_CD WITH (NOLOCK) WHERE TIPO=0 GROUP BY LINEA) CD ON L.ID=CD.LINEA" & vbCrLf
sConsulta = sConsulta & "   WHERE L.ORDEN=@ORDEN" & vbCrLf
sConsulta = sConsulta & "UPDATE LINEAS_PEDIDO SET DESCUENTOS=ISNULL(CD.IMPORTE,0) FROM LINEAS_PEDIDO L WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN (SELECT LINEA,SUM(IMPORTE) IMPORTE FROM LINEAS_PEDIDO_CD WITH (NOLOCK) WHERE TIPO=1 GROUP BY LINEA) CD ON L.ID=CD.LINEA" & vbCrLf
sConsulta = sConsulta & "   WHERE L.ORDEN=@ORDEN" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "select @ORDEN" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion31900_09_2141_A31900_09_2142() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim oADOConnection As ADODB.Connection
    Dim sConsulta As String

    On Error GoTo Error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Actualiza Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    frmProgreso.ProgressBar1.Refresh
    V_31900_9_Storeds_2142

    sConsulta = "UPDATE VERSION SET NUM ='3.00.21.042'" & ", FECHA=CONVERT(datetime,'" & Format(Now, "mm-dd-yyyy hh:mm:ss") & "',110), PRODVER='31900.9'"
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False

    CodigoDeActualizacion31900_09_2141_A31900_09_2142 = True
    Exit Function

Error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If

    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    CodigoDeActualizacion31900_09_2141_A31900_09_2142 = False
End Function

Private Sub V_31900_9_Storeds_2142()
    Dim sConsulta As String

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSGS_INSERTAR_ART4_UON]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSGS_INSERTAR_ART4_UON]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSGS_INSERTAR_ART4_UON]   @ART VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON;" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Metemos las uo que tienen empresa con integracion E o E/S y las que tienen emp sin activar la integracion" & vbCrLf
sConsulta = sConsulta & "INSERT INTO ART4_UON (ART4,UON1,UON2,UON3)" & vbCrLf
sConsulta = sConsulta & "select DISTINCT @ART,uon1,uon2,uon3" & vbCrLf
sConsulta = sConsulta & "FROM" & vbCrLf
sConsulta = sConsulta & "(select u1.cod uon1,u2.cod uon2,u3.cod uon3,u4.cod uon4, coalesce(u4.empresa,u3.empresa,u2.empresa,u1.empresa) emp from uon1 u1 with (nolock)" & vbCrLf
sConsulta = sConsulta & "left join uon2 u2 with (nolock) on u1.cod=u2.uon1 and u1.BAJALOG=0 and u2.BAJALOG=0 and u2.EMPRESA is not null" & vbCrLf
sConsulta = sConsulta & "left join uon3 u3 with (nolock) on u1.cod=u3.uon1 and u2.cod=u3.uon2 and u3.BAJALOG=0 and u3.EMPRESA is not null" & vbCrLf
sConsulta = sConsulta & "left join uon4 u4 with (nolock) on u1.cod=u4.uon1 and u2.cod=u4.uon2 and u3.cod=u4.uon3 and u4.BAJALOG=0  and u4.EMPRESA is not null where u1.BAJALOG=0) empresa" & vbCrLf
sConsulta = sConsulta & "inner join emp with (nolock) on emp.id=empresa.emp" & vbCrLf
sConsulta = sConsulta & "inner join erp_sociedad e with (nolock) on e.SOCIEDAD=emp.SOCIEDAD" & vbCrLf
sConsulta = sConsulta & "inner join TABLAS_INTEGRACION_ERP ti  WITH(NOLOCK) on ti.ERP=e.ERP and TABLA=7 and coalesce(sentido,1)<>2" & vbCrLf
sConsulta = sConsulta & "order by uon1,uon2,uon3" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Metemos las que no tienen empresa" & vbCrLf
sConsulta = sConsulta & "INSERT INTO ART4_UON (ART4,UON1)" & vbCrLf
sConsulta = sConsulta & "select distinct @ART,cod from uon1  with (nolock)  WHERE NOT EXISTS (select * from (select u1.cod uon1,u2.cod uon2,u3.cod uon3,u4.cod uon4, coalesce(u4.empresa,u3.empresa,u2.empresa,u1.empresa) emp from uon1 u1 with (nolock)" & vbCrLf
sConsulta = sConsulta & "left join uon2 u2 with (nolock) on u1.cod=u2.uon1 and u1.BAJALOG=0 and u2.BAJALOG=0 and u2.EMPRESA is not null" & vbCrLf
sConsulta = sConsulta & "left join uon3 u3 with (nolock) on u1.cod=u3.uon1 and u2.cod=u3.uon2 and u3.BAJALOG=0 and u3.EMPRESA is not null" & vbCrLf
sConsulta = sConsulta & "left join uon4 u4 with (nolock) on u1.cod=u4.uon1 and u2.cod=u4.uon2 and u3.cod=u4.uon3 and u4.BAJALOG=0 and u4.EMPRESA is not null   where u1.BAJALOG=0) empresa" & vbCrLf
sConsulta = sConsulta & "inner join emp with (nolock) on emp.id=empresa.emp inner join erp_sociedad e with (nolock) on e.SOCIEDAD=emp.SOCIEDAD" & vbCrLf
sConsulta = sConsulta & "inner join TABLAS_INTEGRACION_ERP ti  WITH(NOLOCK) on ti.ERP=e.ERP and TABLA=7 and coalesce(sentido,1)=2" & vbCrLf
sConsulta = sConsulta & "where uon1=uon1.cod)" & vbCrLf
sConsulta = sConsulta & "and bajalog=0 and cod not in (select uon1 from art4_uon  with (nolock) where art4=@ART)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO ART4_UON (ART4,UON1,uon2)" & vbCrLf
sConsulta = sConsulta & "select distinct @ART,uon1,cod from uon2 with (nolock) WHERE NOT EXISTS (select * from (select u1.cod uon1,u2.cod uon2,u3.cod uon3,u4.cod uon4, coalesce(u4.empresa,u3.empresa,u2.empresa,u1.empresa) emp from uon1 u1 with (nolock)" & vbCrLf
sConsulta = sConsulta & "left join uon2 u2 with (nolock) on u1.cod=u2.uon1 and u1.BAJALOG=0 and u2.BAJALOG=0 and u2.EMPRESA is not null" & vbCrLf
sConsulta = sConsulta & "left join uon3 u3 with (nolock) on u1.cod=u3.uon1 and u2.cod=u3.uon2 and u3.BAJALOG=0 and u3.EMPRESA is not null" & vbCrLf
sConsulta = sConsulta & "left join uon4 u4 with (nolock) on u1.cod=u4.uon1 and u2.cod=u4.uon2 and u3.cod=u4.uon3 and u4.BAJALOG=0 and u4.EMPRESA is not null   where u1.BAJALOG=0) empresa" & vbCrLf
sConsulta = sConsulta & "inner join emp with (nolock) on emp.id=empresa.emp inner join erp_sociedad e with (nolock) on e.SOCIEDAD=emp.SOCIEDAD" & vbCrLf
sConsulta = sConsulta & "inner join TABLAS_INTEGRACION_ERP ti  WITH(NOLOCK) on ti.ERP=e.ERP and TABLA=7 and coalesce(sentido,1)=2" & vbCrLf
sConsulta = sConsulta & "where uon1=uon2.uon1 and coalesce(uon2,uon2.cod,'')=coalesce(uon2.cod,UON2,''))" & vbCrLf
sConsulta = sConsulta & "and bajalog=0 and not exists (select * from art4_uon with (nolock) where art4=@ART and uon1=uon2.uon1 and uon2=uon2.cod and uon3 is null)" & vbCrLf
sConsulta = sConsulta & "and not exists (select * from art4_uon with (nolock) where art4=@ART and uon1=uon2.uon1 and uon2 is null and uon3 is null)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO ART4_UON (ART4,UON1,uon2,UON3)" & vbCrLf
sConsulta = sConsulta & "select distinct @ART,uon1,UON2,cod from uon3 with (nolock) WHERE NOT EXISTS (select * from (select u1.cod uon1,u2.cod uon2,u3.cod uon3,u4.cod uon4, coalesce(u4.empresa,u3.empresa,u2.empresa,u1.empresa) emp from uon1 u1 with (nolock)" & vbCrLf
sConsulta = sConsulta & "left join uon2 u2 with (nolock) on u1.cod=u2.uon1 and u1.BAJALOG=0 and u2.BAJALOG=0 and u2.EMPRESA is not null" & vbCrLf
sConsulta = sConsulta & "left join uon3 u3 with (nolock) on u1.cod=u3.uon1 and u2.cod=u3.uon2 and u3.BAJALOG=0 and u3.EMPRESA is not null" & vbCrLf
sConsulta = sConsulta & "left join uon4 u4 with (nolock) on u1.cod=u4.uon1 and u2.cod=u4.uon2 and u3.cod=u4.uon3 and u4.BAJALOG=0 and u4.EMPRESA is not null   where u1.BAJALOG=0) empresa" & vbCrLf
sConsulta = sConsulta & "inner join emp with (nolock) on emp.id=empresa.emp inner join erp_sociedad e with (nolock) on e.SOCIEDAD=emp.SOCIEDAD" & vbCrLf
sConsulta = sConsulta & "inner join TABLAS_INTEGRACION_ERP ti  WITH(NOLOCK) on ti.ERP=e.ERP and TABLA=7 and coalesce(sentido,1)=2" & vbCrLf
sConsulta = sConsulta & "where uon1=uon3.uon1 and coalesce(uon2,uon3.UON2,'')=coalesce(uon3.UON2,UON2,'') and coalesce(uon3,uon3.cod,'')=coalesce(uon3.cod,UON3,''))" & vbCrLf
sConsulta = sConsulta & "and bajalog=0 and not exists (select * from art4_uon with (nolock) where art4=@ART and uon1=uon3.uon1 and uon2=uon3.UON2 and uon3=uon3.cod)" & vbCrLf
sConsulta = sConsulta & "and not exists (select * from art4_uon with (nolock) where art4=@ART and uon1=uon3.uon1 and uon2 =uon3.uon2 and uon3 is null)" & vbCrLf
sConsulta = sConsulta & "and not exists (select * from art4_uon with (nolock) where art4=@ART and uon1=uon3.uon1 and uon2 is null and uon3 is null)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

End Sub


Public Function CodigoDeActualizacion31900_09_2142_A31900_09_2143() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim oADOConnection As ADODB.Connection
    Dim sConsulta As String

    On Error GoTo Error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Actualiza Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    frmProgreso.ProgressBar1.Refresh
    V_31900_9_Storeds_2143

    sConsulta = "UPDATE VERSION SET NUM ='3.00.21.043'" & ", FECHA=CONVERT(datetime,'" & Format(Now, "mm-dd-yyyy hh:mm:ss") & "',110), PRODVER='31900.9'"
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False

    CodigoDeActualizacion31900_09_2142_A31900_09_2143 = True
    Exit Function

Error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If

    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    CodigoDeActualizacion31900_09_2142_A31900_09_2143 = False
End Function

Private Sub V_31900_9_Storeds_2143()
    Dim sConsulta As String

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_BORRADO_PROCESO_MARCADOSENPEDIDO]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_BORRADO_PROCESO_MARCADOSENPEDIDO]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[SP_BORRADO_PROCESO_MARCADOSENPEDIDO] AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN1 VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCE INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @ACTIVLOG TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @INSTWEB TINYINT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @ADM VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @DAT VARCHAR(512)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @ADJUNPROCE INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @TEXTOFIN INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSP NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVE VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSP_COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @CIA_COMP INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @NUM INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @NUMNUE INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @NUMAREV INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @ACTIVLOG =(SELECT ACTIVLOG FROM PARGEN_GEST WITH (NOLOCK) WHERE ID=1)" & vbCrLf
sConsulta = sConsulta & "--TipoInstWeb.ConPortal = 2" & vbCrLf
sConsulta = sConsulta & "SET @INSTWEB =(SELECT INSTWEB FROM PARGEN_INTERNO WITH (NOLOCK) WHERE ID=1)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @ADM =(SELECT TOP 1 U.COD FROM USU U WITH(NOLOCK) INNER JOIN PERF P WITH(NOLOCK) ON P.ID=U.PERF AND P.ADM=1)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "--Solo para Gestamp -> Dato Fijo no se mete en Pargen_Xxx" & vbCrLf
sConsulta = sConsulta & "DECLARE @BORRADO_TRAS_HORAS INT" & vbCrLf
sConsulta = sConsulta & "SET @BORRADO_TRAS_HORAS =8" & vbCrLf
sConsulta = sConsulta & "--------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE CBORRADO CURSOR LOCAL FOR" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT P.ANYO, P.GMN1, P.COD, 'Anyo:' + CAST(P.ANYO AS VARCHAR(50)) + ' GMN1:' + P.GMN1 + ' Cod:' + CAST(P.COD AS VARCHAR(50))" & vbCrLf
sConsulta = sConsulta & "FROM PROCE P WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PROCE_ATRIBESP PA WITH(NOLOCK) ON PA.ANYO=P.ANYO AND PA.GMN1=P.GMN1 AND PA.PROCE=P.COD AND PA.VALOR_BOOL=0" & vbCrLf
sConsulta = sConsulta & "INNER JOIN DEF_ATRIB DA WITH(NOLOCK) ON PA.ATRIB=DA.ID AND DA.COD='PED_CON_AHORRO 0'" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PLANTILLA_ATRIB_ESP PLA WITH(NOLOCK) ON P.PLANTILLA=PLA.PLANT AND PLA.ATRIB=PA.ATRIB" & vbCrLf
sConsulta = sConsulta & "WHERE P.EST>=12" & vbCrLf
sConsulta = sConsulta & "AND DATEDIFF(HOUR,P.FECALTABD,GETDATE())>@BORRADO_TRAS_HORAS AND P.EST>=12" & vbCrLf
sConsulta = sConsulta & "OPEN CBORRADO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM CBORRADO INTO @ANYO,@GMN1,@PROCE ,@DAT" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   UPDATE LINEAS_PEDIDO SET ANYO=NULL, GMN1=NULL, PROCE=NULL WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @INSTWEB =2 --CON PORTAL" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "SELECT @FSP = FSP_SRV + '.' + FSP_BD + '.dbo.' FROM PARGEN_PORT WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'SELECT @CIA_COMP= ID FROM ' + @FSP + 'CIAS WITH (NOLOCK) WHERE COD=(SELECT FSP_CIA FROM PARGEN_PORT WITH (NOLOCK) WHERE ID=1)'" & vbCrLf
sConsulta = sConsulta & "       EXEC SP_EXECUTESQL @SQL, N'@CIA_COMP INT OUTPUT',@CIA_COMP=@CIA_COMP OUTPUT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       DECLARE CPORTAL CURSOR LOCAL FOR" & vbCrLf
sConsulta = sConsulta & "       SELECT DISTINCT PROCE_PROVE.PROVE AS PROVE ,PROVE.FSP_COD AS FSP_COD" & vbCrLf
sConsulta = sConsulta & "           FROM PROCE_PROVE WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PROVE WITH (NOLOCK) ON PROCE_PROVE.PROVE=PROVE.COD" & vbCrLf
sConsulta = sConsulta & "           WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PUB=1" & vbCrLf
sConsulta = sConsulta & "       OPEN CPORTAL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       FETCH NEXT FROM CPORTAL INTO @PROVE,@FSP_COD" & vbCrLf
sConsulta = sConsulta & "       WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "       BEGIN   " & vbCrLf
sConsulta = sConsulta & "           UPDATE PROCE_PROVE SET PUB=0 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE  AND PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "           EXEC NUM_PROCE_PUB @PROVE=@PROVE,@NUM=@NUM OUTPUT,@NUMNUE=@NUMNUE OUTPUT,@NUMAREV=@NUMAREV OUTPUT" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "           SET @SQL = N'EXEC ' +  @FSP + 'SP_ACT_PROCE_PUB @CIA_COMP=@CIA_COMP,@CIA_PROVE=@CIA_PROVE,@NUM_PUB=@NUM_PUB,@NUM_PUB_SINOFE=@NUM_PUB_SINOFE,@NUM_PUB_AREV=@NUM_PUB_AREV'" & vbCrLf
sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQL, N'@CIA_COMP SMALLINT,@CIA_PROVE VARCHAR(50), @NUM_PUB INT, @NUM_PUB_SINOFE INT,@NUM_PUB_AREV INT', @CIA_COMP=@CIA_COMP, @CIA_PROVE = @PROVE, @NUM_PUB = @NUM, @NUM_PUB_SINOFE=@NUMNUE, @NUM_PUB_AREV=@NUMAREV" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "           FETCH NEXT FROM CPORTAL INTO @PROVE,@FSP_COD" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "       CLOSE CPORTAL" & vbCrLf
sConsulta = sConsulta & "       DEALLOCATE CPORTAL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'EXEC ' +  @FSP + 'SP_ELIMINAR_PROCE @CIA_COMP=@CIA_COMP,@ANYO=@ANYO,@GMN1=@GMN1,@PROCE=@PROCE'" & vbCrLf
sConsulta = sConsulta & "       EXEC SP_EXECUTESQL @SQL, N'@CIA_COMP SMALLINT,@ANYO SMALLINT,@GMN1 VARCHAR(50),@PROCE INT', @CIA_COMP=@CIA_COMP, @ANYO = @ANYO, @GMN1 = @GMN1, @PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   --Hay que ir eliminando los datos de todas las tablas relacionadas" & vbCrLf
sConsulta = sConsulta & "   DELETE PROCE_ATRIBESP WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   DELETE PROCE_GRUPO_ATRIBESP WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   DELETE ITEM_ATRIBESP WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   DELETE ITEM_ATRIBESP_DEF WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   DELETE AR_ITEM_PROVE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   DELETE AR_ITEM_PRESCON3 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   DELETE AR_ITEM_PRESCON4 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   DELETE AR_ITEM_MES_UON1 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   DELETE AR_ITEM_MES_UON2 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   DELETE AR_ITEM_MES_UON3 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   DELETE AR_ITEM_MES_PRESCON4_1 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   DELETE AR_ITEM_MES_PRESCON4_2 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   DELETE AR_ITEM_MES_PRESCON4_3 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   DELETE AR_ITEM_MES_PRESCON4_4 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   DELETE AR_ITEM_MES_PRESCON3_1 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   DELETE AR_ITEM_MES_PRESCON3_2 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   DELETE AR_ITEM_MES_PRESCON3_3 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   DELETE AR_ITEM_MES_PRESCON3_4 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   DELETE AR_ITEM_MES_PARPROY1 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   DELETE AR_ITEM_MES_PARPROY2 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   DELETE AR_ITEM_MES_PARPROY3 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   DELETE AR_ITEM_MES_PARPROY4 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   DELETE AR_ITEM_MES_PARCON1 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   DELETE AR_ITEM_MES_PARCON2 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   DELETE AR_ITEM_MES_PARCON3 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   DELETE AR_ITEM_MES_PARCON4 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   DELETE AR_ITEM_MES WHERE ANYO=@ANYO AND GMN1_PROCE=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   DELETE AR_ITEM WHERE ANYO=@ANYO AND GMN1_PROCE=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   DELETE AR_ITEM_PROVE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   DELETE AR_PROCE_GMN4 WHERE ANYO=@ANYO AND GMN1_PROCE=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Borrado de tablas relacionadas para subastas" & vbCrLf
sConsulta = sConsulta & "SET @TEXTOFIN=(SELECT TEXTOFIN FROM PROCE_SUBASTA WITH (NOLOCK) WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE)" & vbCrLf
sConsulta = sConsulta & "IF @TEXTOFIN IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "       DELETE TEXTOS_MULTIIDIOMA WHERE ID=@TEXTOFIN" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   DECLARE CSUBASTA CURSOR LOCAL FOR SELECT DISTINCT ADJUN_PROCE FROM PROCE_SUBASTA_REGLAS WITH (NOLOCK) WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   OPEN CSUBASTA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   FETCH NEXT FROM CSUBASTA INTO @ADJUNPROCE" & vbCrLf
sConsulta = sConsulta & "   WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "   BEGIN   " & vbCrLf
sConsulta = sConsulta & "       IF (SELECT COUNT(1) AS NUMREG FROM PROCE_SUBASTA_REGLAS WITH (NOLOCK) WHERE ADJUN_PROCE=@ADJUNPROCE)>0" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           DELETE ADJUN_PROCE WHERE ID=@ADJUNPROCE" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "       FETCH NEXT FROM CSUBASTA INTO @ADJUNPROCE" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "   CLOSE CSUBASTA" & vbCrLf
sConsulta = sConsulta & "   DEALLOCATE CSUBASTA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   DELETE PROCE_SUBASTA_REGLAS WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   DELETE PROCE_SUBASTA WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   DELETE PROCE_SUB_EVENTOS WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   --Fin Borrado de tablas relacionadas para subastas" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   DELETE OFE_ITEM_ADJUN WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   DELETE OFE_ITEM_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   DELETE OFE_GR_ADJUN WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   DELETE OFE_GR_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   DELETE OFE_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   DELETE GRUPO_OFE_REU WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   DELETE GRUPO_OFE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   DELETE OFE_LEIDO WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   DELETE ITEM_PRESPROY WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM > 0" & vbCrLf
sConsulta = sConsulta & "   DELETE ITEM_PRESCON WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM > 0" & vbCrLf
sConsulta = sConsulta & "   DELETE ITEM_PRESCON3 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM > 0" & vbCrLf
sConsulta = sConsulta & "   DELETE ITEM_PRESCON4 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM > 0" & vbCrLf
sConsulta = sConsulta & "   DELETE ITEM_ESP WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM > 0" & vbCrLf
sConsulta = sConsulta & "   DELETE ITEM_OFE_WEB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE        " & vbCrLf
sConsulta = sConsulta & "   DELETE ITEM_ADJ WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   DELETE ITEM_PRESESC WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   DELETE ITEM_ADJREU WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   DELETE ITEM_OFE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   DELETE CONF_VISTAS_ITEM WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   DELETE CONF_VISTAS_ITEM_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   DELETE CONF_VISTAS_ITEM_PROVE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   DELETE ITEM_UON1 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   DELETE ITEM_UON2 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   DELETE ITEM_UON3 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   DELETE ITEM_OBJ WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   DELETE ITEM_PRES WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   DELETE ITEM WHERE ANYO=@ANYO AND GMN1_PROCE=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   DELETE OFE_GR_COSTESDESC WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   DELETE PROCE_PER WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   DELETE PROCE_ESP WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   DELETE PROCE_PROVE_PET WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   DELETE PROCE_PROVE_GRUPOS WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   DELETE PROCE_OFE_ADJUN WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   DELETE PROCE_OFE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   DELETE PROCE_OFE_WEB_ADJUN WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   DELETE PROCE_OFE_WEB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   DELETE AR_PROVE FROM AR_PROVE WITH(NOLOCK) INNER JOIN PROCE_PROVE WITH(NOLOCK) ON AR_PROVE.ANYO=PROCE_PROVE.ANYO AND AR_PROVE.GMN1=PROCE_PROVE.GMN1 AND AR_PROVE.PROCE=PROCE_PROVE.PROCE AND AR_PROVE.PROVE=PROCE_PROVE.PROVE WHERE AR_PROVE.ANYO=@ANYO AND AR_PROVE.GMN1=@GMN1 AND AR_PROVE.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   DELETE PROCE_PROVE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   DELETE REU_PROCE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   DELETE BLOQUEO_PROCE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "--Borrar registros de la tabla de proveedores potenciales eliminados correspondientes al proceso a eliminar" & vbCrLf
sConsulta = sConsulta & "   DELETE PROCE_PROVE_NO_POT WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "DELETE OFEMIN WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   DELETE CONF_VISTAS_GRUPO WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   DELETE CONF_VISTAS_GRUPO_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   DELETE CONF_VISTAS_GRUPO_PROVE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   DELETE PROCE_GR_PRES2 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   DELETE PROCE_GR_PRES3 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   DELETE PROCE_GR_PRES4 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   DELETE PROCE_GR_PRES1 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   DELETE PROCE_GR_UON1 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   DELETE PROCE_GR_UON2 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   DELETE PROCE_GR_UON3 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   DELETE PROCE_GR_ESC WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   DELETE PROCE_GRUPO_ESP WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   DELETE PROCE_GRUPO_DEF WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   DELETE CONF_VISTAS_ALL WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   DELETE CONF_VISTAS_ALL_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   DELETE CONF_VISTAS_ALL_PROVE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   DELETE CONF_VISTAS_PROCE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   DELETE CONF_VISTAS_PROCE_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   DELETE CONF_VISTAS_PROCE_PROVE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   DELETE PROCE_PRES2 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   DELETE PROCE_PRES3 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   DELETE PROCE_PRES4 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   DELETE PROCE_PRES1 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   DELETE PROCE_UON1 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   DELETE PROCE_UON2 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   DELETE PROCE_UON3 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   DELETE PROCE_ATRIB_POND FROM PROCE_ATRIB_POND WITH(NOLOCK) INNER JOIN PROCE_ATRIB WITH(NOLOCK) ON PROCE_ATRIB_POND.ATRIB_ID =PROCE_ATRIB.ID AND PROCE_ATRIB.ANYO=@ANYO AND PROCE_ATRIB.GMN1=@GMN1 AND PROCE_ATRIB.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   DELETE PROCE_ATRIB_LISTA FROM PROCE_ATRIB_LISTA WITH(NOLOCK) INNER JOIN PROCE_ATRIB WITH(NOLOCK) ON PROCE_ATRIB_LISTA.ATRIB_ID =PROCE_ATRIB.ID AND PROCE_ATRIB.ANYO=@ANYO AND PROCE_ATRIB.GMN1=@GMN1 AND PROCE_ATRIB.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   DELETE PROCE_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   DELETE AR_GRUPO FROM AR_GRUPO WITH(NOLOCK) INNER JOIN PROCE_GRUPO WITH(NOLOCK) ON AR_GRUPO.ANYO=PROCE_GRUPO.ANYO AND AR_GRUPO.GMN1=PROCE_GRUPO.GMN1 AND AR_GRUPO.PROCE=PROCE_GRUPO.PROCE AND AR_GRUPO.GRUPO=PROCE_GRUPO.ID WHERE AR_GRUPO.ANYO=@ANYO AND AR_GRUPO.GMN1=@GMN1 AND AR_GRUPO.PROCE=@PROCE  " & vbCrLf
sConsulta = sConsulta & "   DELETE PROCE_GRUPO WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   DELETE PROCE_DEF WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   DELETE CONF_VISTAS_PROCE_SOBRE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   DELETE SOBRE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   DELETE PROCE_NUMDEC WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   DELETE PROCE_PEDIDO WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE    " & vbCrLf
sConsulta = sConsulta & "   DELETE PROCE_ADJUN WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   DELETE PROCE_GMN4 WHERE ANYO= @ANYO AND GMN1_PROCE=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   DELETE PROCE_INT WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   DELETE AR_PROCE FROM AR_PROCE WITH(NOLOCK) INNER JOIN PROCE WITH(NOLOCK) ON AR_PROCE.ANYO=PROCE.ANYO AND AR_PROCE.GMN1=PROCE.GMN1 AND AR_PROCE.COD=PROCE.COD WHERE AR_PROCE.ANYO=@ANYO AND AR_PROCE.GMN1=@GMN1 AND AR_PROCE.COD=@PROCE  " & vbCrLf
sConsulta = sConsulta & "   DELETE PROCE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   IF @ACTIVLOG = 1" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       --AccionesSummit.accproceeli = 283      " & vbCrLf
sConsulta = sConsulta & "       INSERT INTO LOG (USU,ACC,DAT,FEC) VALUES (@ADM,283,@DAT,GETDATE())" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM CBORRADO INTO @ANYO,@GMN1,@PROCE ,@DAT" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "CLOSE CBORRADO" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE CBORRADO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSEP_CARGAR_ATRIBUTOS_LINEA_PEDIDO]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSEP_CARGAR_ATRIBUTOS_LINEA_PEDIDO]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSEP_CARGAR_ATRIBUTOS_LINEA_PEDIDO]" & vbCrLf
sConsulta = sConsulta & "@IDCODLINEA INT," & vbCrLf
sConsulta = sConsulta & "@IDTIPOPEDIDO INT," & vbCrLf
sConsulta = sConsulta & "@EMPRESA INT," & vbCrLf
sConsulta = sConsulta & "@INTEGRACION TINYINT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING1 NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "SELECT @SQLSTRING1 ='SELECT ID, COD,  DEN, TIPO, INTRO, INTERNO, OBLIGATORIO, VALORMINFEC,VALORMINNUM," & vbCrLf
sConsulta = sConsulta & "       VALORMAXFEC,VALORMAXNUM, VALOR_TEXT, VALOR_NUM, VALOR_FEC, VALOR_BOOL , VALNUM,VALTEXT,VALFEC,VALBOOL, MAX(INTEGRACION) INTEGRACION" & vbCrLf
sConsulta = sConsulta & "       FROM ('" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @SQLSTRING =" & vbCrLf
sConsulta = sConsulta & "case when CESTA.CAT5 is null then" & vbCrLf
sConsulta = sConsulta & "case when CESTA.CAT4 is null then" & vbCrLf
sConsulta = sConsulta & "case when CESTA.CAT3 is null then" & vbCrLf
sConsulta = sConsulta & "case when CESTA.CAT2 is null then  N'SELECT DF.ID ID, DF.COD COD,  DF.DEN DEN, DF.TIPO TIPO, DF.INTRO INTRO, CTN1.INTERNO INTERNO, CTN1.OBLIGATORIO OBLIGATORIO, DF.MINFEC VALORMINFEC,DF.MINNUM VALORMINNUM," & vbCrLf
sConsulta = sConsulta & "DF.MAXFEC VALORMAXFEC,DF.MAXNUM VALORMAXNUM, CTN1.VALOR_TEXT, CTN1.VALOR_NUM, CTN1.VALOR_FEC, CTN1.VALOR_BOOL , CA.VALOR_NUM AS VALNUM,CA.VALOR_TEXT AS VALTEXT,CA.VALOR_FEC AS VALFEC,CA.VALOR_BOOL AS VALBOOL, 0 AS INTEGRACION  FROM CATN1_ATRIB CTN1 WITH(NOLOCK) left join DEF_ATRIB DF WITH(NOLOCK) on CTN1.ATRIB = DF.ID LEFT OUTER JOIN CESTA_ATRIB CA WITH(NOLOCK) on CA.CESTA = '+ convert(VARCHAR, @IDCODLINEA)  +' AND CA.ATRIB = DF.ID  where CTN1.CATN1_ID = '+convert(nvarchar(255), CESTA.CAT1) + ' AND CTN1.AMBITO = 1 AND CTN1.TIPO = 1 '" & vbCrLf
sConsulta = sConsulta & "else  N'SELECT DF.ID ID, DF.COD COD,  DF.DEN DEN, DF.TIPO TIPO, DF.INTRO INTRO, CTN2.INTERNO INTERNO, CTN2.OBLIGATORIO OBLIGATORIO, DF.MINFEC VALORMINFEC,DF.MINNUM VALORMINNUM," & vbCrLf
sConsulta = sConsulta & "DF.MAXFEC VALORMAXFEC,DF.MAXNUM VALORMAXNUM, CTN2.VALOR_TEXT, CTN2.VALOR_NUM, CTN2.VALOR_FEC, CTN2.VALOR_BOOL , CA.VALOR_NUM AS VALNUM,CA.VALOR_TEXT AS VALTEXT,CA.VALOR_FEC AS VALFEC,CA.VALOR_BOOL AS VALBOOL, 0 AS INTEGRACION  FROM CATN2_ATRIB CTN2 WITH(NOLOCK) left join DEF_ATRIB DF WITH(NOLOCK) on CTN2.ATRIB = DF.ID LEFT OUTER JOIN CESTA_ATRIB CA WITH(NOLOCK) on CA.CESTA = '+ convert(VARCHAR, @IDCODLINEA)  +' AND CA.ATRIB = DF.ID  where CTN2.CATN2_ID = '+convert(nvarchar(255), CESTA.CAT2) + ' AND CTN2.AMBITO = 1 AND CTN2.TIPO = 1' end" & vbCrLf
sConsulta = sConsulta & "else  N'SELECT DF.ID ID, DF.COD COD,  DF.DEN DEN, DF.TIPO TIPO, DF.INTRO INTRO, CTN3.INTERNO INTERNO, CTN3.OBLIGATORIO OBLIGATORIO, DF.MINFEC VALORMINFEC,DF.MINNUM VALORMINNUM," & vbCrLf
sConsulta = sConsulta & "DF.MAXFEC VALORMAXFEC,DF.MAXNUM VALORMAXNUM, CTN3.VALOR_TEXT, CTN3.VALOR_NUM, CTN3.VALOR_FEC, CTN3.VALOR_BOOL , CA.VALOR_NUM AS VALNUM,CA.VALOR_TEXT AS VALTEXT,CA.VALOR_FEC AS VALFEC,CA.VALOR_BOOL AS VALBOOL, 0 AS INTEGRACION  FROM CATN3_ATRIB CTN3 WITH(NOLOCK) left join DEF_ATRIB DF WITH(NOLOCK) on CTN3.ATRIB = DF.ID LEFT OUTER JOIN CESTA_ATRIB CA WITH(NOLOCK) on CA.CESTA = '+ convert(VARCHAR, @IDCODLINEA)  +' AND CA.ATRIB = DF.ID  where CTN3.CATN3_ID = '+convert(nvarchar(255), CESTA.CAT3) + ' AND CTN3.AMBITO = 1 AND CTN3.TIPO = 1' end" & vbCrLf
sConsulta = sConsulta & "else  N'SELECT DF.ID ID, DF.COD COD,  DF.DEN DEN, DF.TIPO TIPO, DF.INTRO INTRO, CTN4.INTERNO INTERNO, CTN4.OBLIGATORIO OBLIGATORIO, DF.MINFEC VALORMINFEC,DF.MINNUM VALORMINNUM," & vbCrLf
sConsulta = sConsulta & "DF.MAXFEC VALORMAXFEC,DF.MAXNUM VALORMAXNUM, CTN4.VALOR_TEXT, CTN4.VALOR_NUM, CTN4.VALOR_FEC, CTN4.VALOR_BOOL , CA.VALOR_NUM AS VALNUM,CA.VALOR_TEXT AS VALTEXT,CA.VALOR_FEC AS VALFEC,CA.VALOR_BOOL AS VALBOOL, 0 AS INTEGRACION   FROM CATN4_ATRIB CTN4 WITH(NOLOCK) left join DEF_ATRIB DF WITH(NOLOCK) on CTN4.ATRIB = DF.ID LEFT OUTER JOIN CESTA_ATRIB CA WITH(NOLOCK) on CA.CESTA = '+ convert(VARCHAR, @IDCODLINEA)  +' AND CA.ATRIB = DF.ID  where CTN4.CATN4_ID = '+convert(nvarchar(255), CESTA.CAT4) + ' AND CTN4.AMBITO = 1 AND CTN4.TIPO = 1' end" & vbCrLf
sConsulta = sConsulta & "else  N'SELECT DF.ID ID, DF.COD COD,  DF.DEN DEN, DF.TIPO TIPO, DF.INTRO INTRO, CT1.INTERNO INTERNO, CT1.OBLIGATORIO OBLIGATORIO, DF.MINFEC VALORMINFEC,DF.MINNUM VALORMINNUM," & vbCrLf
sConsulta = sConsulta & "DF.MAXFEC VALORMAXFEC,DF.MAXNUM VALORMAXNUM, CT1.VALOR_TEXT, CT1.VALOR_NUM, CT1.VALOR_FEC, CT1.VALOR_BOOL , CA.VALOR_NUM AS VALNUM,CA.VALOR_TEXT AS VALTEXT,CA.VALOR_FEC AS VALFEC,CA.VALOR_BOOL AS VALBOOL, 0 AS INTEGRACION  FROM CATN5_ATRIB CT1 WITH(NOLOCK) left join DEF_ATRIB DF WITH(NOLOCK) on CT1.ATRIB = DF.ID LEFT OUTER JOIN CESTA_ATRIB CA WITH(NOLOCK) on CA.CESTA = '+ convert(VARCHAR, @IDCODLINEA)  +' AND CA.ATRIB = DF.ID  where CT1.CATN5_ID = '+convert(nvarchar(255), CESTA.CAT5) + ' AND CT1.AMBITO = 1 AND CT1.TIPO = 1' end" & vbCrLf
sConsulta = sConsulta & "FROM CESTA where ID = @IDCODLINEA;" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "If (@INTEGRACION = 1)" & vbCrLf
sConsulta = sConsulta & "   Begin" & vbCrLf
sConsulta = sConsulta & "        SET @SQLSTRING =  @SQLSTRING + N' UNION SELECT DF.ID ID, DF.COD COD,  DF.DEN DEN, DF.TIPO TIPO, DF.INTRO INTRO, 0 INTERNO, IA.OBLIGATORIO OBLIGATORIO, DF.MINFEC VALORMINFEC,DF.MINNUM VALORMINNUM," & vbCrLf
sConsulta = sConsulta & "           DF.MAXFEC VALORMAXFEC,DF.MAXNUM VALORMAXNUM, IA.VALOR_TEXT, IA.VALOR_NUM, IA.VALOR_FEC, IA.VALOR_BOOL, CA.VALOR_NUM AS VALNUM,CA.VALOR_TEXT AS VALTEXT,CA.VALOR_FEC AS VALFEC,CA.VALOR_BOOL AS VALBOOL , 1 AS INTEGRACION" & vbCrLf
sConsulta = sConsulta & "           from INT_ATRIB IA WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN DEF_ATRIB DF WITH (NOLOCK) ON IA.ATRIB=DF.ID" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN ERP ERP WITH(NOLOCK) on IA.ERP = ERP.COD" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN EMP EMP WITH(NOLOCK) on ERP.COD = EMP.ERP" & vbCrLf
sConsulta = sConsulta & "           LEFT OUTER JOIN CESTA_ATRIB CA WITH(NOLOCK) on CA.CESTA = '+ convert(VARCHAR, @IDCODLINEA)  +' AND CA.ATRIB = DF.ID" & vbCrLf
sConsulta = sConsulta & "           where EMP.ID = ' + convert(VARCHAR, @EMPRESA)  +  ' AND IA.PEDIDO = 1 and IA.AMBITO = 3" & vbCrLf
sConsulta = sConsulta & "           AND IA.ID not in (SELECT ATRIB from INT_ATRIB_TIPOPEDIDO IATP WITH (NOLOCK) where IA.ID=IATP.ATRIB)" & vbCrLf
sConsulta = sConsulta & "           UNION" & vbCrLf
sConsulta = sConsulta & "           SELECT DF.ID ID, DF.COD COD,  DF.DEN DEN, DF.TIPO TIPO, DF.INTRO INTRO, 0 INTERNO, IA.OBLIGATORIO OBLIGATORIO, DF.MINFEC VALORMINFEC,DF.MINNUM VALORMINNUM," & vbCrLf
sConsulta = sConsulta & "           DF.MAXFEC VALORMAXFEC,DF.MAXNUM VALORMAXNUM, IA.VALOR_TEXT, IA.VALOR_NUM, IA.VALOR_FEC, IA.VALOR_BOOL, CA.VALOR_NUM AS VALNUM,CA.VALOR_TEXT AS VALTEXT,CA.VALOR_FEC AS VALFEC,CA.VALOR_BOOL AS VALBOOL , 1 AS INTEGRACION" & vbCrLf
sConsulta = sConsulta & "           from INT_ATRIB IA WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN INT_ATRIB_TIPOPEDIDO IATP WITH (NOLOCK) ON IA.ID=IATP.ATRIB AND IATP.TIPOPEDIDO = '+ convert(VARCHAR, @IDTIPOPEDIDO)  +'" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN DEF_ATRIB DF WITH (NOLOCK) ON IA.ATRIB=DF.ID" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN ERP ERP WITH(NOLOCK) on IA.ERP = ERP.COD" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN EMP EMP WITH(NOLOCK) on ERP.COD = EMP.ERP" & vbCrLf
sConsulta = sConsulta & "           LEFT OUTER JOIN CESTA_ATRIB CA WITH(NOLOCK) on CA.CESTA = '+ convert(VARCHAR, @IDCODLINEA)  +' AND CA.ATRIB = DF.ID" & vbCrLf
sConsulta = sConsulta & "           where EMP.ID = ' + convert(VARCHAR, @EMPRESA)  +  ' AND IA.PEDIDO = 1 and IA.AMBITO = 3';" & vbCrLf
sConsulta = sConsulta & "   End" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING =  @SQLSTRING+ N' UNION SELECT DF.ID ID, DF.COD COD,  DF.DEN DEN, DF.TIPO TIPO, DF.INTRO INTRO, CLA.INTERNO INTERNO, CLA.OBLIGATORIO OBLIGATORIO, DF.MINFEC VALORMINFEC,DF.MINNUM VALORMINNUM," & vbCrLf
sConsulta = sConsulta & "DF.MAXFEC VALORMAXFEC,DF.MAXNUM VALORMAXNUM, CLA.VALOR_TEXT, CLA.VALOR_NUM, CLA.VALOR_FEC, CLA.VALOR_BOOL, CAT.VALOR_NUM AS VALNUM,CAT.VALOR_TEXT AS VALTEXT,CAT.VALOR_FEC AS VALFEC,CAT.VALOR_BOOL AS VALBOOL, 0 AS INTEGRACION" & vbCrLf
sConsulta = sConsulta & "FROM CESTA CA WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN CATALOG_LIN CL  WITH (NOLOCK) on CA.LINEA = CL.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN CATALOG_LIN_ATRIB CLA WITH (NOLOCK) on CL.ID = CLA.CATALOG_LIN_ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN DEF_ATRIB DF WITH (NOLOCK) ON CLA.ATRIB = DF.ID" & vbCrLf
sConsulta = sConsulta & "LEFT OUTER JOIN CESTA_ATRIB CAT WITH (NOLOCK) ON CAT.CESTA = '+ convert(VARCHAR, @IDCODLINEA)  +' AND CLA.ATRIB = CAT.ATRIB" & vbCrLf
sConsulta = sConsulta & "WHERE CA.ID = '+ convert(VARCHAR, @IDCODLINEA)  +'  AND CLA.TIPO = 1" & vbCrLf
sConsulta = sConsulta & ")A" & vbCrLf
sConsulta = sConsulta & "GROUP BY ID, COD,  DEN, TIPO, INTRO, INTERNO, OBLIGATORIO, VALORMINFEC,VALORMINNUM," & vbCrLf
sConsulta = sConsulta & "VALORMAXFEC,VALORMAXNUM, VALOR_TEXT, VALOR_NUM, VALOR_FEC, VALOR_BOOL , VALNUM,VALTEXT," & vbCrLf
sConsulta = sConsulta & "VALFEC,VALBOOL" & vbCrLf
sConsulta = sConsulta & "ORDER BY OBLIGATORIO DESC'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING =  @SQLSTRING1 + @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "EXECUTE sp_executesql @SQLSTRING, N'@IDTIPOPEDIDO INT', @IDTIPOPEDIDO = @IDTIPOPEDIDO;" & vbCrLf
sConsulta = sConsulta & "--END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSEP_CARGAR_ATRIBUTOS_LINEA_PEDIDO_INTEGRACION]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSEP_CARGAR_ATRIBUTOS_LINEA_PEDIDO_INTEGRACION]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSEP_CARGAR_ATRIBUTOS_LINEA_PEDIDO_INTEGRACION]" & vbCrLf
sConsulta = sConsulta & "@IDCODLINEA INT," & vbCrLf
sConsulta = sConsulta & "@IDTIPOPEDIDO INT," & vbCrLf
sConsulta = sConsulta & "@EMPRESA INT," & vbCrLf
sConsulta = sConsulta & "@INTEGRACION TINYINT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "If (@INTEGRACION = 1)" & vbCrLf
sConsulta = sConsulta & "Begin" & vbCrLf
sConsulta = sConsulta & "SELECT @SQLSTRING =  N' SELECT DF.ID ID, DF.COD COD,  DF.DEN DEN, DF.TIPO TIPO, DF.INTRO INTRO, 0 INTERNO, IA.OBLIGATORIO OBLIGATORIO, DF.MINFEC VALORMINFEC,DF.MINNUM VALORMINNUM," & vbCrLf
sConsulta = sConsulta & "DF.MAXFEC VALORMAXFEC,DF.MAXNUM VALORMAXNUM, IA.VALOR_TEXT, IA.VALOR_NUM, IA.VALOR_FEC, IA.VALOR_BOOL, CA.VALOR_NUM AS VALNUM,CA.VALOR_TEXT AS VALTEXT,CA.VALOR_FEC AS VALFEC,CA.VALOR_BOOL AS VALBOOL , 1 AS INTEGRACION" & vbCrLf
sConsulta = sConsulta & "from INT_ATRIB IA WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN DEF_ATRIB DF WITH (NOLOCK) ON IA.ATRIB=DF.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN ERP ERP WITH(NOLOCK) on IA.ERP = ERP.COD" & vbCrLf
sConsulta = sConsulta & "INNER JOIN EMP EMP WITH(NOLOCK) on ERP.COD = EMP.ERP" & vbCrLf
sConsulta = sConsulta & "LEFT OUTER JOIN CESTA_ATRIB CA WITH(NOLOCK) on CA.CESTA = '+ convert(VARCHAR, @IDCODLINEA)  +' AND CA.ATRIB = DF.ID" & vbCrLf
sConsulta = sConsulta & "where EMP.ID = ' + convert(VARCHAR, @EMPRESA)  +  ' AND IA.PEDIDO = 1 and IA.AMBITO = 3" & vbCrLf
sConsulta = sConsulta & "                  AND IA.ID not in (SELECT ATRIB from INT_ATRIB_TIPOPEDIDO IATP WITH (NOLOCK) where IA.ID=IATP.ATRIB)" & vbCrLf
sConsulta = sConsulta & "                  UNION" & vbCrLf
sConsulta = sConsulta & "                  SELECT DF.ID ID, DF.COD COD,  DF.DEN DEN, DF.TIPO TIPO, DF.INTRO INTRO, 0 INTERNO, IA.OBLIGATORIO OBLIGATORIO, DF.MINFEC VALORMINFEC,DF.MINNUM VALORMINNUM," & vbCrLf
sConsulta = sConsulta & "DF.MAXFEC VALORMAXFEC,DF.MAXNUM VALORMAXNUM, IA.VALOR_TEXT, IA.VALOR_NUM, IA.VALOR_FEC, IA.VALOR_BOOL, CA.VALOR_NUM AS VALNUM,CA.VALOR_TEXT AS VALTEXT,CA.VALOR_FEC AS VALFEC,CA.VALOR_BOOL AS VALBOOL , 1 AS INTEGRACION" & vbCrLf
sConsulta = sConsulta & "from INT_ATRIB IA WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN INT_ATRIB_TIPOPEDIDO IATP WITH (NOLOCK) ON IA.ID=IATP.ATRIB AND IATP.TIPOPEDIDO = '+ convert(VARCHAR, @IDTIPOPEDIDO)  +'" & vbCrLf
sConsulta = sConsulta & "INNER JOIN DEF_ATRIB DF WITH (NOLOCK) ON IA.ATRIB=DF.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN ERP ERP WITH(NOLOCK) on IA.ERP = ERP.COD" & vbCrLf
sConsulta = sConsulta & "INNER JOIN EMP EMP WITH(NOLOCK) on ERP.COD = EMP.ERP" & vbCrLf
sConsulta = sConsulta & "LEFT OUTER JOIN CESTA_ATRIB CA WITH(NOLOCK) on CA.CESTA = '+ convert(VARCHAR, @IDCODLINEA)  +' AND CA.ATRIB = DF.ID" & vbCrLf
sConsulta = sConsulta & "where EMP.ID = ' + convert(VARCHAR, @EMPRESA)  +  ' AND IA.PEDIDO = 1 and IA.AMBITO = 3';" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXECUTE sp_executesql @SQLSTRING, N'@IDTIPOPEDIDO INT', @IDTIPOPEDIDO = @IDTIPOPEDIDO;" & vbCrLf
sConsulta = sConsulta & "--END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSEP_CARGAR_COSTES_LINEA]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSEP_CARGAR_COSTES_LINEA]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSEP_CARGAR_COSTES_LINEA]" & vbCrLf
sConsulta = sConsulta & "@IDLINEA INT," & vbCrLf
sConsulta = sConsulta & "@IDTIPOPEDIDO INT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @SQLSTRING =" & vbCrLf
sConsulta = sConsulta & "case when CESTA.CAT5 is null then" & vbCrLf
sConsulta = sConsulta & "case when CESTA.CAT4 is null then" & vbCrLf
sConsulta = sConsulta & "case when CESTA.CAT3 is null then" & vbCrLf
sConsulta = sConsulta & "case when CESTA.CAT2 is null then  N'SELECT DF.ID ID, DF.COD COD,  DF.DEN DEN, DF.TIPO TIPO, DF.INTRO INTRO, CTN1.INTERNO INTERNO, CTN1.OBLIGATORIO OBLIGATORIO, DF.MINFEC VALORMINFEC,DF.MINNUM VALORMINNUM,CTN1.OPERACION, CTN1.TIPO_COSTE_DESCUENTO,CTN1.GRUPO_COSTE_DESCUENTO," & vbCrLf
sConsulta = sConsulta & "DF.MAXFEC VALORMAXFEC,DF.MAXNUM VALORMAXNUM, CTN1.VALOR_TEXT, CTN1.VALOR_NUM, CTN1.VALOR_FEC, CTN1.VALOR_BOOL, CA.CESTA as SELEC FROM CATN1_ATRIB CTN1 WITH(NOLOCK) left join DEF_ATRIB DF WITH(NOLOCK) on CTN1.ATRIB = DF.ID  LEFT OUTER JOIN CESTA_ATRIB CA WITH(NOLOCK) on CA.CESTA = '+ convert(VARCHAR, @IDLINEA) +'  AND CA.ATRIB = DF.ID where CTN1.CATN1_ID = '+convert(nvarchar(255), CESTA.CAT1) + ' AND CTN1.AMBITO = 1 AND CTN1.TIPO = 2'" & vbCrLf
sConsulta = sConsulta & "else  N'SELECT DF.ID ID, DF.COD COD,  DF.DEN DEN, DF.TIPO TIPO, DF.INTRO INTRO, CTN2.INTERNO INTERNO, CTN2.OBLIGATORIO OBLIGATORIO, DF.MINFEC VALORMINFEC,DF.MINNUM VALORMINNUM,CTN2.OPERACION,CTN2.TIPO_COSTE_DESCUENTO,CTN2.GRUPO_COSTE_DESCUENTO," & vbCrLf
sConsulta = sConsulta & "DF.MAXFEC VALORMAXFEC,DF.MAXNUM VALORMAXNUM, CTN2.VALOR_TEXT, CTN2.VALOR_NUM, CTN2.VALOR_FEC, CTN2.VALOR_BOOL, CA.CESTA as SELEC FROM CATN2_ATRIB CTN2 WITH(NOLOCK) left join DEF_ATRIB DF WITH(NOLOCK) on CTN2.ATRIB = DF.ID  LEFT OUTER JOIN CESTA_ATRIB CA WITH(NOLOCK) on CA.CESTA = '+ convert(VARCHAR, @IDLINEA) +'  AND CA.ATRIB = DF.ID where CTN2.CATN2_ID = '+convert(nvarchar(255), CESTA.CAT2) + ' AND CTN2.AMBITO = 1 AND CTN2.TIPO = 2' end" & vbCrLf
sConsulta = sConsulta & "else  N'SELECT DF.ID ID, DF.COD COD,  DF.DEN DEN, DF.TIPO TIPO, DF.INTRO INTRO, CTN3.INTERNO INTERNO, CTN3.OBLIGATORIO OBLIGATORIO, DF.MINFEC VALORMINFEC,DF.MINNUM VALORMINNUM,CTN3.OPERACION,CTN3.TIPO_COSTE_DESCUENTO,CTN3.GRUPO_COSTE_DESCUENTO," & vbCrLf
sConsulta = sConsulta & "DF.MAXFEC VALORMAXFEC,DF.MAXNUM VALORMAXNUM, CTN3.VALOR_TEXT, CTN3.VALOR_NUM, CTN3.VALOR_FEC, CTN3.VALOR_BOOL, CA.CESTA as SELEC FROM CATN3_ATRIB CTN3 WITH(NOLOCK) left join DEF_ATRIB DF WITH(NOLOCK) on CTN3.ATRIB = DF.ID LEFT OUTER JOIN CESTA_ATRIB CA WITH(NOLOCK) on CA.CESTA = '+ convert(VARCHAR, @IDLINEA) +'  AND CA.ATRIB = DF.ID where CTN3.CATN3_ID = '+convert(nvarchar(255), CESTA.CAT3) + ' AND CTN3.AMBITO = 1 AND CTN3.TIPO = 2 ' end" & vbCrLf
sConsulta = sConsulta & "else  N'SELECT DF.ID ID, DF.COD COD,  DF.DEN DEN, DF.TIPO TIPO, DF.INTRO INTRO, CTN4.INTERNO INTERNO, CTN4.OBLIGATORIO OBLIGATORIO, DF.MINFEC VALORMINFEC,DF.MINNUM VALORMINNUM,CTN4.OPERACION,CTN4.TIPO_COSTE_DESCUENTO,CTN4.GRUPO_COSTE_DESCUENTO," & vbCrLf
sConsulta = sConsulta & "DF.MAXFEC VALORMAXFEC,DF.MAXNUM VALORMAXNUM, CTN4.VALOR_TEXT, CTN4.VALOR_NUM, CTN4.VALOR_FEC, CTN4.VALOR_BOOL, CA.CESTA as SELEC FROM CATN4_ATRIB CTN4 WITH(NOLOCK) left join DEF_ATRIB DF WITH(NOLOCK) on CTN4.ATRIB = DF.ID LEFT OUTER JOIN CESTA_ATRIB CA WITH(NOLOCK) on CA.CESTA = '+ convert(VARCHAR, @IDLINEA) +'  AND CA.ATRIB = DF.ID where CTN4.CATN4_ID = '+convert(nvarchar(255), CESTA.CAT4) + ' AND CTN4.AMBITO = 1 AND CTN4.TIPO = 2 ' end" & vbCrLf
sConsulta = sConsulta & "else  N'SELECT DF.ID ID, DF.COD COD,  DF.DEN DEN, DF.TIPO TIPO, DF.INTRO INTRO, CT1.INTERNO INTERNO, CT1.OBLIGATORIO OBLIGATORIO, DF.MINFEC VALORMINFEC,DF.MINNUM VALORMINNUM,CT1.OPERACION , CT1.TIPO_COSTE_DESCUENTO,CT1.GRUPO_COSTE_DESCUENTO," & vbCrLf
sConsulta = sConsulta & "DF.MAXFEC VALORMAXFEC,DF.MAXNUM VALORMAXNUM, CT1.VALOR_TEXT, CT1.VALOR_NUM, CT1.VALOR_FEC, CT1.VALOR_BOOL, CA.CESTA as SELEC FROM CATN5_ATRIB CT1 WITH(NOLOCK) left join DEF_ATRIB DF WITH(NOLOCK) on CT1.ATRIB = DF.ID LEFT OUTER JOIN CESTA_ATRIB CA WITH(NOLOCK) on CA.CESTA = '+ convert(VARCHAR, @IDLINEA) +'  AND CA.ATRIB = DF.ID where CT1.CATN5_ID = '+convert(nvarchar(255), CESTA.CAT5) + ' AND CT1.AMBITO = 1 AND CT1.TIPO = 2' end" & vbCrLf
sConsulta = sConsulta & "FROM CESTA where ID = @IDLINEA;" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING =  @SQLSTRING+ N' UNION SELECT DF.ID ID, DF.COD COD,  DF.DEN DEN, DF.TIPO TIPO, DF.INTRO INTRO, CL.INTERNO INTERNO, CL.OBLIGATORIO OBLIGATORIO, DF.MINFEC VALORMINFEC,DF.MINNUM VALORMINNUM, CL.OPERACION, CL.TIPO_COSTE_DESCUENTO,CL.GRUPO_COSTE_DESCUENTO," & vbCrLf
sConsulta = sConsulta & "DF.MAXFEC VALORMAXFEC,DF.MAXNUM VALORMAXNUM, DF.VALOR_TEXT, DF.VALOR_NUM, DF.VALOR_FEC, DF.VALOR_BOOL, CA.CESTA as SELEC" & vbCrLf
sConsulta = sConsulta & "FROM  CATALOG_LIN_ATRIB CL WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN TIPOPEDIDO_ATRIB TP WITH (NOLOCK) ON TP.ID = CL.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN DEF_ATRIB DF WITH (NOLOCK) ON DF.ID=TP.ATRIB" & vbCrLf
sConsulta = sConsulta & "LEFT OUTER JOIN CESTA_ATRIB CA WITH (NOLOCK) on CA.CESTA = '+ convert(VARCHAR, @IDLINEA) +'  AND CA.ATRIB = DF.ID" & vbCrLf
sConsulta = sConsulta & "WHERE TP.TIPOPEDIDO_ID= @IDTIPOPEDIDO AND CL.TIPO = 2'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING =  @SQLSTRING+ N' UNION SELECT DF.ID ID, DF.COD COD,  DF.DEN DEN, DF.TIPO TIPO, DF.INTRO INTRO, CL.INTERNO INTERNO, CL.OBLIGATORIO OBLIGATORIO, DF.MINFEC VALORMINFEC,DF.MINNUM VALORMINNUM, CL.OPERACION, CL.TIPO_COSTE_DESCUENTO,CL.GRUPO_COSTE_DESCUENTO," & vbCrLf
sConsulta = sConsulta & "DF.MAXFEC VALORMAXFEC,DF.MAXNUM VALORMAXNUM, CL.VALOR_TEXT, CL.VALOR_NUM, CL.VALOR_FEC, CL.VALOR_BOOL, CA.CESTA as SELEC" & vbCrLf
sConsulta = sConsulta & "FROM CATALOG_LIN_ATRIB CL WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN DEF_ATRIB DF WITH (NOLOCK) ON DF.ID=CL.ATRIB" & vbCrLf
sConsulta = sConsulta & "INNER JOIN CESTA CT WITH (NOLOCK) ON CT.LINEA=CL.CATALOG_LIN_ID" & vbCrLf
sConsulta = sConsulta & "LEFT OUTER JOIN CESTA_ATRIB CA WITH (NOLOCK) on CA.CESTA = '+ convert(VARCHAR, @IDLINEA) +'  AND CA.ATRIB = DF.ID" & vbCrLf
sConsulta = sConsulta & "WHERE CT.ID = @IDLINEA AND CL.TIPO = 2" & vbCrLf
sConsulta = sConsulta & "ORDER BY TIPO_COSTE_DESCUENTO, GRUPO_COSTE_DESCUENTO'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXECUTE sp_executesql @SQLSTRING, N'@IDLINEA INT, @IDTIPOPEDIDO INT', @IDLINEA = @IDLINEA, @IDTIPOPEDIDO = @IDTIPOPEDIDO;" & vbCrLf
sConsulta = sConsulta & "--END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSEP_GENERAR_CESTA_FAVORITO]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSEP_GENERAR_CESTA_FAVORITO]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSEP_GENERAR_CESTA_FAVORITO] @ORDENID INT," & vbCrLf
sConsulta = sConsulta & "@CESTA INT = 0 OUTPUT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON;" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDLINEA INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDLINEAFAVORITO INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @FECHAEMISION as DATETIME" & vbCrLf
sConsulta = sConsulta & "DECLARE @MON AS VARCHAR(3)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @FECHAEMISION = CONVERT(DATETIME,CONVERT(VARCHAR,DAY(GETDATE())) + '/' + CONVERT(VARCHAR,MONTH(GETDATE())) + '/' + CONVERT(VARCHAR,YEAR(GETDATE())) ,103)" & vbCrLf
sConsulta = sConsulta & "SET @ANYO=YEAR(@FECHAEMISION)" & vbCrLf
sConsulta = sConsulta & "SELECT @MON = MON FROM FAVORITOS_ORDEN_ENTREGA WITH (NOLOCK) WHERE ID = @ORDENID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO CESTA_CABECERA ([PER]" & vbCrLf
sConsulta = sConsulta & "   ,[PROVE]" & vbCrLf
sConsulta = sConsulta & "   ,[MON]" & vbCrLf
sConsulta = sConsulta & "   ,[NUM_PED_ERP]" & vbCrLf
sConsulta = sConsulta & "   ,[EMP]" & vbCrLf
sConsulta = sConsulta & "   ,[COD_PROVE_ERP]" & vbCrLf
sConsulta = sConsulta & "   ,[RECEPTOR]" & vbCrLf
sConsulta = sConsulta & "   ,[TIPOPEDIDO]" & vbCrLf
sConsulta = sConsulta & "   ,[OBS]" & vbCrLf
sConsulta = sConsulta & "   ,[FAC_DIR_ENVIO]" & vbCrLf
sConsulta = sConsulta & "   ,[CAT]" & vbCrLf
sConsulta = sConsulta & "   ,[NIVEL])" & vbCrLf
sConsulta = sConsulta & "SELECT FOE.USU, FOE.PROVE,FOE.MON, FOE.NUM_PED_ERP, FOE.EMPRESA, FOE.COD_PROVE_ERP, FOE.RECEPTOR, FOE.TIPOPEDIDO, FOE.OBS, FOE.FAC_DIR_ENVIO, FOE.CAT, FOE.NIVEL" & vbCrLf
sConsulta = sConsulta & "FROM FAVORITOS_ORDEN_ENTREGA FOE WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE FOE.ID = @ORDENID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--SELECT @CESTA = MAX(ID) FROM CESTA_CABECERA WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "SELECT @CESTA=SCOPE_IDENTITY()" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO CESTA_CABECERA_ADJUN([CESTACABECERAID]" & vbCrLf
sConsulta = sConsulta & "   ,[NOM]" & vbCrLf
sConsulta = sConsulta & "   ,[COM]" & vbCrLf
sConsulta = sConsulta & "   ,[DATA]" & vbCrLf
sConsulta = sConsulta & "   ,[DATASIZE])" & vbCrLf
sConsulta = sConsulta & "SELECT @CESTA, ADJ.NOM, ADJ.COM, ADJ.DATA, ADJ.DATASIZE" & vbCrLf
sConsulta = sConsulta & "FROM FAVORITOS_ORDEN_ENTREGA_ADJUN ADJ WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE ADJ.ORDEN = @ORDENID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE Cur_CESTA Cursor FOR SELECT FLP.ID" & vbCrLf
sConsulta = sConsulta & "FROM FAVORITOS_LINEAS_PEDIDO FLP WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE FLP.ORDEN = @ORDENID" & vbCrLf
sConsulta = sConsulta & "OPEN Cur_CESTA" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM Cur_CESTA into @IDLINEA" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO CESTA ([PER]" & vbCrLf
sConsulta = sConsulta & "   ,[PROVE]" & vbCrLf
sConsulta = sConsulta & "   ,[LINEA]" & vbCrLf
sConsulta = sConsulta & "   ,[CAT1]" & vbCrLf
sConsulta = sConsulta & "   ,[CAT2]" & vbCrLf
sConsulta = sConsulta & "   ,[CAT3]" & vbCrLf
sConsulta = sConsulta & "   ,[CAT4]" & vbCrLf
sConsulta = sConsulta & "   ,[CAT5]" & vbCrLf
sConsulta = sConsulta & "   ,[DESCR]" & vbCrLf
sConsulta = sConsulta & "   ,[CANT]" & vbCrLf
sConsulta = sConsulta & "   ,[UP]" & vbCrLf
sConsulta = sConsulta & "   ,[PREC]" & vbCrLf
sConsulta = sConsulta & "   ,[TIPO]" & vbCrLf
sConsulta = sConsulta & "   ,[FC]" & vbCrLf
sConsulta = sConsulta & "   ,[DEST]" & vbCrLf
sConsulta = sConsulta & "   ,[ENTREGA_OBL]" & vbCrLf
sConsulta = sConsulta & "   ,[FECENTREGA]" & vbCrLf
sConsulta = sConsulta & "   ,[OBS]" & vbCrLf
sConsulta = sConsulta & "   ,[CAMPO1]" & vbCrLf
sConsulta = sConsulta & "   ,[VALOR1]" & vbCrLf
sConsulta = sConsulta & "   ,[CAMPO2]" & vbCrLf
sConsulta = sConsulta & "   ,[VALOR2]" & vbCrLf
sConsulta = sConsulta & "   ,[OBSADJUN]" & vbCrLf
sConsulta = sConsulta & "   ,[MON]" & vbCrLf
sConsulta = sConsulta & "   ,[ALMACEN]" & vbCrLf
sConsulta = sConsulta & "   ,[ACTIVO]" & vbCrLf
sConsulta = sConsulta & "   ,[CESTACABECERAID]" & vbCrLf
sConsulta = sConsulta & "   ,[TIPORECEPCION]" & vbCrLf
sConsulta = sConsulta & "   ,[IMPORTE_PED])" & vbCrLf
sConsulta = sConsulta & "SELECT FLP.USU, FLP.PROVE, FLP.LINEA, FLP.CAT1, FLP.CAT2, FLP.CAT3, FLP.CAT4, FLP.CAT5, FLP.DESCR_LIBRE, FLP.CANT_PED, FLP.UP, FLP.PREC_UP * MON.EQUIV, 0, FLP.FC," & vbCrLf
sConsulta = sConsulta & "FLP.DEST, FLP.ENTREGA_OBL, @FECHAEMISION, FLP.OBS, FLP.CAMPO1, FLP.VALOR1, FLP.CAMPO2, FLP.VALOR2, FLP.OBSADJUN, @MON, FLP.ALMACEN, FLP.ACTIVO," & vbCrLf
sConsulta = sConsulta & "@CESTA, FLP.TIPORECEPCION, FLP.IMPORTE_PED * MON.EQUIV" & vbCrLf
sConsulta = sConsulta & "FROM FAVORITOS_LINEAS_PEDIDO FLP WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN MON WITH(NOLOCK) ON MON.COD=@MON" & vbCrLf
sConsulta = sConsulta & "WHERE FLP.ID = @IDLINEA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @IDLINEAFAVORITO=SCOPE_IDENTITY()" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO CESTA_ADJUN([CESTA_LINEA]" & vbCrLf
sConsulta = sConsulta & ",[NOM]" & vbCrLf
sConsulta = sConsulta & ",[COM]" & vbCrLf
sConsulta = sConsulta & ",[DATA]" & vbCrLf
sConsulta = sConsulta & ",[DATASIZE])" & vbCrLf
sConsulta = sConsulta & "SELECT @IDLINEAFAVORITO, ADJ.NOM, ADJ.COM, ADJ.DATA, ADJ.DATASIZE" & vbCrLf
sConsulta = sConsulta & "FROM FAVORITOS_LINEAS_PEDIDO_ADJUN ADJ WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN FAVORITOS_LINEAS_PEDIDO FLP WITH(NOLOCK) on FLP.ORDEN = @ORDENID" & vbCrLf
sConsulta = sConsulta & "WHERE ADJ.LINEA = @IDLINEA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO CESTA_IMPUTACION([CESTA]" & vbCrLf
sConsulta = sConsulta & ",[ID]" & vbCrLf
sConsulta = sConsulta & ",[CENTRO_SM]" & vbCrLf
sConsulta = sConsulta & ",[PRES5_IMP])" & vbCrLf
sConsulta = sConsulta & "SELECT @IDLINEAFAVORITO, FI.ID, FI.CENTRO_SM, FI.PRES5_IMP" & vbCrLf
sConsulta = sConsulta & "FROM FAVORITOS_IMPUTACION FI WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN FAVORITOS_LINEAS_PEDIDO FLP WITH(NOLOCK) on FLP.ID = FI.FAVORITO AND FLP.ORDEN = @ORDENID" & vbCrLf
sConsulta = sConsulta & "WHERE FI.FAVORITO = @IDLINEA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM Cur_CESTA INTO @IDLINEA" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "Close Cur_CESTA" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE Cur_CESTA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @CESTA" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_DEVOLVER_ATRIB_ART_UON]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_DEVOLVER_ATRIB_ART_UON]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_DEVOLVER_ATRIB_ART_UON]" & vbCrLf
sConsulta = sConsulta & "@ART      VARCHAR(50)=NULL," & vbCrLf
sConsulta = sConsulta & "@UON1     VARCHAR(50)=NULL," & vbCrLf
sConsulta = sConsulta & "@UON2     VARCHAR(50)=NULL," & vbCrLf
sConsulta = sConsulta & "@UON3       VARCHAR(50)=NULL" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @UON1 IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       SELECT  AUA.ATRIB,DA.DEN,DA.TIPO,AUA.ART,AUA.VALOR_BOOL,AUA.VALOR_FEC,AUA.VALOR_NUM,AUA.VALOR_TEXT" & vbCrLf
sConsulta = sConsulta & "       ,AUA.UON1,AUA.UON2,AUA.UON3" & vbCrLf
sConsulta = sConsulta & "       FROM ART4_UON_ATRIB AUA WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN DEF_ATRIB DA WITH (NOLOCK) ON AUA.ATRIB=DA.ID" & vbCrLf
sConsulta = sConsulta & "       WHERE @ART=ART AND @UON1=UON1 AND (@UON2 IS NULL OR UON2=@UON2) AND (@UON3 IS NULL OR UON3=@UON3)" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       SELECT  AUA.ATRIB,DA.DEN,DA.TIPO,AUA.ART,AUA.VALOR_BOOL,AUA.VALOR_FEC,AUA.VALOR_NUM,AUA.VALOR_TEXT" & vbCrLf
sConsulta = sConsulta & "       ,AUA.UON1,AUA.UON2,AUA.UON3" & vbCrLf
sConsulta = sConsulta & "       FROM ART4_UON_ATRIB AUA WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN DEF_ATRIB DA WITH (NOLOCK) ON AUA.ATRIB=DA.ID" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN ART4_UON AU WITH (NOLOCK) ON AUA.ART=AU.ART4" & vbCrLf
sConsulta = sConsulta & "       AND AUA.UON1=AU.UON1 AND (AUA.UON2 IS NULL OR AU.UON2=AUA.UON2) AND (AUA.UON3 IS NULL OR AU.UON3=AUA.UON3)" & vbCrLf
sConsulta = sConsulta & "       WHERE @ART=ART" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_UON_ORGCOMPRAS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_UON_ORGCOMPRAS]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_UON_ORGCOMPRAS]" & vbCrLf
sConsulta = sConsulta & "@ORGCOMP  VARCHAR(100) = NULL," & vbCrLf
sConsulta = sConsulta & "@CENTRO  VARCHAR(100) = NULL" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT U3.UON1, U3.UON2, U3.COD UON3, U3.DEN" & vbCrLf
sConsulta = sConsulta & "FROM UON3 U3 WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN UON2 U2 WITH(NOLOCK) ON U3.UON2 = U2.COD AND U3.UON1 = U2.UON1" & vbCrLf
sConsulta = sConsulta & "INNER JOIN UON1 U1 WITH(NOLOCK) ON U2.UON1 = U1.COD" & vbCrLf
sConsulta = sConsulta & "WHERE ISNULL(U3.ORGCOMPRAS, ISNULL(U2.ORGCOMPRAS,U1.ORGCOMPRAS)) = @ORGCOMP" & vbCrLf
sConsulta = sConsulta & "AND ISNULL(U3.CENTROS,'') = ISNULL(@CENTRO,'')" & vbCrLf
sConsulta = sConsulta & "AND U3.BAJALOG = 0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UNION" & vbCrLf
sConsulta = sConsulta & "SELECT U2.UON1, U2.COD UON2, NULL UON3, U2.DEN" & vbCrLf
sConsulta = sConsulta & "FROM UON2 U2 WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN UON1 U1 WITH(NOLOCK) ON U2.UON1 = U1.COD" & vbCrLf
sConsulta = sConsulta & "WHERE ISNULL(U2.ORGCOMPRAS, U1.ORGCOMPRAS) = @ORGCOMP" & vbCrLf
sConsulta = sConsulta & "AND ISNULL(U2.CENTROS,'') = ISNULL(@CENTRO,'')" & vbCrLf
sConsulta = sConsulta & "AND U2.BAJALOG = 0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UNION" & vbCrLf
sConsulta = sConsulta & "SELECT U1.COD UON1, NULL UON2, NULL UON3, U1.DEN" & vbCrLf
sConsulta = sConsulta & "FROM UON1 U1 WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE U1.ORGCOMPRAS = @ORGCOMP" & vbCrLf
sConsulta = sConsulta & "AND ISNULL(U1.CENTROS,'') = ISNULL(@CENTRO,'')" & vbCrLf
sConsulta = sConsulta & "AND U1.BAJALOG = 0" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSWS_ARTICULOS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSWS_ARTICULOS]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSWS_ARTICULOS]" & vbCrLf
sConsulta = sConsulta & "@USU NVARCHAR(50)=NULL," & vbCrLf
sConsulta = sConsulta & "@RESTRICUONSOLUSUUON BIT=0," & vbCrLf
sConsulta = sConsulta & "@RESTRICUONSOLPERFUON BIT=0," & vbCrLf
sConsulta = sConsulta & "@COD VARCHAR(50)=NULL," & vbCrLf
sConsulta = sConsulta & "@DEN VARCHAR(2000)=NULL," & vbCrLf
sConsulta = sConsulta & "@GMN1 VARCHAR(50)=NULL," & vbCrLf
sConsulta = sConsulta & "@GMN2 VARCHAR(50)=NULL," & vbCrLf
sConsulta = sConsulta & "@GMN3 VARCHAR(50)=NULL," & vbCrLf
sConsulta = sConsulta & "@GMN4 VARCHAR(50)=NULL," & vbCrLf
sConsulta = sConsulta & "@COINCID TINYINT=1," & vbCrLf
sConsulta = sConsulta & "@PER VARCHAR(50)=NULL," & vbCrLf
sConsulta = sConsulta & "@FILAS INT=NULL," & vbCrLf
sConsulta = sConsulta & "@DESDEFILA INT=NULL," & vbCrLf
sConsulta = sConsulta & "@IDI VARCHAR(50)='SPA'," & vbCrLf
sConsulta = sConsulta & "@INSTANCIAMONEDA VARCHAR(3)='EUR'," & vbCrLf
sConsulta = sConsulta & "@CARGAR_ULT_ADJ TINYINT=0," & vbCrLf
sConsulta = sConsulta & "@GENERICOS TINYINT=2," & vbCrLf
sConsulta = sConsulta & "@CODORGCOMPRAS VARCHAR(4)=NULL," & vbCrLf
sConsulta = sConsulta & "@CODCENTRO VARCHAR(4)=NULL," & vbCrLf
sConsulta = sConsulta & "@CODINI VARCHAR(50)=NULL," & vbCrLf
sConsulta = sConsulta & "@CODULT VARCHAR(50)=NULL," & vbCrLf
sConsulta = sConsulta & "@CARGAR_CC TINYINT=0," & vbCrLf
sConsulta = sConsulta & "@CARGAR_ORG TINYINT=0," & vbCrLf
sConsulta = sConsulta & "@GASTO TINYINT=0," & vbCrLf
sConsulta = sConsulta & "@INVERSION TINYINT=0," & vbCrLf
sConsulta = sConsulta & "@GASTOINVERSION TINYINT=0," & vbCrLf
sConsulta = sConsulta & "@NOALMACENABLE TINYINT=0," & vbCrLf
sConsulta = sConsulta & "@ALMACENABLE TINYINT=0," & vbCrLf
sConsulta = sConsulta & "@ALMACENOPCIONAL TINYINT=0," & vbCrLf
sConsulta = sConsulta & "@RECEPCIONOBLIG TINYINT=0," & vbCrLf
sConsulta = sConsulta & "@NORECEPCIONABLE TINYINT=0," & vbCrLf
sConsulta = sConsulta & "@RECEPCIONOPCIONAL TINYINT=0," & vbCrLf
sConsulta = sConsulta & "@MATERIALESCATALOGADOS TINYINT=NULL," & vbCrLf
sConsulta = sConsulta & "@RESTRICMATUSU INT=0," & vbCrLf
sConsulta = sConsulta & "@FILTRARNEGOCIADOS INT=0," & vbCrLf
sConsulta = sConsulta & "@CODPROVEEDOR VARCHAR(50)=NULL," & vbCrLf
sConsulta = sConsulta & "@CONFPAGO AS TINYINT=0," & vbCrLf
sConsulta = sConsulta & "@USAR_ORG AS TINYINT=0," & vbCrLf
sConsulta = sConsulta & "@ACCESO_SM AS TINYINT=0," & vbCrLf
sConsulta = sConsulta & "@UON1 NVARCHAR(15)=NULL," & vbCrLf
sConsulta = sConsulta & "@UON2 NVARCHAR(15)=NULL," & vbCrLf
sConsulta = sConsulta & "@UON3 NVARCHAR(15)=NULL," & vbCrLf
sConsulta = sConsulta & "@UON4 NVARCHAR(15)=NULL," & vbCrLf
sConsulta = sConsulta & "@RESTRIC_PED_ART_UON_USU TINYINT=0," & vbCrLf
sConsulta = sConsulta & "@RESTRIC_PED_ART_UON_PERF TINYINT=0," & vbCrLf
sConsulta = sConsulta & "@RUON1 NVARCHAR(15)=NULL," & vbCrLf
sConsulta = sConsulta & "@RUON2 NVARCHAR(15)=NULL," & vbCrLf
sConsulta = sConsulta & "@RUON3 NVARCHAR(15)=NULL," & vbCrLf
sConsulta = sConsulta & "@ATRIB_UON TBATRIBUON READONLY" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @RESTRIC INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @MONCEN AS NVARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @EQUIV AS FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @EQUIVINS AS FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL2 AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL3 AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSELPAG AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLJOINPAG_P AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLJOINPAG_D AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PARAM AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PERF AS INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @RESTRIC = 0" & vbCrLf
sConsulta = sConsulta & "IF @PER IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SELECT @PERF=U.PERF,@RESTRIC=ISNULL(ISNULL(USU_ACC.ACC,PERF_ACC.ACC),0)" & vbCrLf
sConsulta = sConsulta & "FROM USU U WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN USU_ACC WITH(NOLOCK) ON USU_ACC.USU=U.COD AND USU_ACC.ACC=@RESTRICMATUSU" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PERF_ACC WITH(NOLOCK) ON PERF_ACC.PERF=U.PERF AND PERF_ACC.ACC=@RESTRICMATUSU" & vbCrLf
sConsulta = sConsulta & "WHERE U.PER=@PER AND U.BAJA=0" & vbCrLf
sConsulta = sConsulta & "IF @RESTRIC>0" & vbCrLf
sConsulta = sConsulta & "SET @RESTRIC=1" & vbCrLf
sConsulta = sConsulta & "if @DESDEFILA = 0" & vbCrLf
sConsulta = sConsulta & "SET @DESDEFILA = NULL" & vbCrLf
sConsulta = sConsulta & "if @FILAS=0" & vbCrLf
sConsulta = sConsulta & "SET @FILAS = 1000" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL2=''" & vbCrLf
sConsulta = sConsulta & "IF @RESTRICUONSOLUSUUON=1 OR @RESTRICUONSOLPERFUON=1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SQL2=N';WITH UONSRESTRICCION AS ('" & vbCrLf
sConsulta = sConsulta & "SET @SQL2=@SQL2+N'SELECT U.PERF,P.UON1,P.UON2,P.UON3 '" & vbCrLf
sConsulta = sConsulta & "SET @SQL2=@SQL2+N'FROM USU U WITH(NOLOCK) INNER JOIN PER P WITH(NOLOCK) ON P.COD=U.PER AND U.COD=@USU '" & vbCrLf
sConsulta = sConsulta & "IF @RESTRICUONSOLUSUUON=0" & vbCrLf
sConsulta = sConsulta & "SET @SQL2=@SQL2+N'AND U.COD IS NULL '" & vbCrLf
sConsulta = sConsulta & "SET @SQL2=@SQL2+N'UNION '" & vbCrLf
sConsulta = sConsulta & "SET @SQL2=@SQL2+N'SELECT U.PERF,PU.UON1,PU.UON2,PU.UON3 '" & vbCrLf
sConsulta = sConsulta & "SET @SQL2=@SQL2+N'FROM USU U WITH(NOLOCK) INNER JOIN PERF_UON PU WITH(NOLOCK) ON PU.PERF=U.PERF AND U.COD=@USU '" & vbCrLf
sConsulta = sConsulta & "IF @RESTRICUONSOLPERFUON=0" & vbCrLf
sConsulta = sConsulta & "SET @SQL2=@SQL2+N'AND U.COD IS NULL '" & vbCrLf
sConsulta = sConsulta & "SET @SQL2=@SQL2+N')'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL2=@SQL2+N'SELECT DISTINCT A.COD,A.DEN,A.GENERICO,A.CONCEPTO,A.ALMACENAR,A.RECEPCIONAR,A.GMN1,A.GMN2,A.GMN3,A.GMN4,A.UNI,G4.DEN_' + @IDI + ' DENGMN,UNI_DEN.DEN DENUNI,UNI.NUMDEC'" & vbCrLf
sConsulta = sConsulta & "SET @SQL2=@SQL2 + ',A.PRES5_NIV0, A.PRES5_NIV1,A.PRES5_NIV2,A.PRES5_NIV3, A.PRES5_NIV4,PERM_EP.PERMISO_EP,CATALOGADO.ESTA_CATALOGADO,'''' AS PREC_VIGENTE'" & vbCrLf
sConsulta = sConsulta & "SET @SQL2=@SQL2 + ',A.TIPORECEPCION'" & vbCrLf
sConsulta = sConsulta & "IF @RESTRIC_PED_ART_UON_USU =1 OR @RESTRIC_PED_ART_UON_PERF =1" & vbCrLf
sConsulta = sConsulta & "SET @SQL2=@SQL2 + ',PERMISO_EMISION.TIENE_PERMISO'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL2=@SQL2 + ',1 TIENE_PERMISO'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @MONCEN=(SELECT MONCEN FROM PARGEN_DEF WITH (NOLOCK))" & vbCrLf
sConsulta = sConsulta & "IF @CARGAR_ULT_ADJ=1 AND NOT(@FILTRARNEGOCIADOS=1 OR @FILTRARNEGOCIADOS=3)" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @EQUIV=(SELECT EQUIV FROM MON  WITH (NOLOCK)  WHERE COD=@MONCEN)" & vbCrLf
sConsulta = sConsulta & "SET @EQUIVINS=(SELECT EQUIV FROM MON WITH (NOLOCK) WHERE COD=@INSTANCIAMONEDA)" & vbCrLf
sConsulta = sConsulta & "SET @SQL2 = @SQL2 + ',(AA.PREC/@EQUIV*@EQUIVINS) PREC_ULT_ADJ,AA.CODPROVE CODPROV_ULT_ADJ,AA.DENPROVE DENPROV_ULT_ADJ'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF (@FILTRARNEGOCIADOS=1 OR @FILTRARNEGOCIADOS=3)" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "--Art4_adj. Guarda en moneda central. Pero lo q se ha de ver es precio en moneda proveedor" & vbCrLf
sConsulta = sConsulta & "SET @SQL2 = @SQL2 + ',NEGO.PREC*(NEGO.CAMBIO_OFE*NEGO.CAMBIO_PROCE) PREC_ULT_ADJ,NEGO.CODPROVE CODPROV_ULT_ADJ,NEGO.DENPROVE DENPROV_ULT_ADJ'" & vbCrLf
sConsulta = sConsulta & "SET @SQL2 = @SQL2 + ',NEGO.UNI AS UNI_ULT_ADJ, NEGO.DEN AS DENUNI_ULT_ADJ, NEGO.MON AS MON_ULT_ADJ, NEGO.DENMON AS DENMON_ULT_ADJ'" & vbCrLf
sConsulta = sConsulta & "--Junto con el proveedor viene la forma de pago. As? q con cada cambio de proveedor en pedidos negociados has de cambiar la forma pago , DE HABER este campo" & vbCrLf
sConsulta = sConsulta & "IF @CONFPAGO = 1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "--se cargar? con la forma de pago asociada al proveedor. Bien prove_erp o bien prove" & vbCrLf
sConsulta = sConsulta & "SET @SQL2 = @SQL2 + ',NEGO.PAG PAG_ULT_ADJ, NEGO.DENPAG DENPAG_ULT_ADJ'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLSELPAG = N''" & vbCrLf
sConsulta = sConsulta & "SET @SQLJOINPAG_P = N''" & vbCrLf
sConsulta = sConsulta & "SET @SQLJOINPAG_D = N''" & vbCrLf
sConsulta = sConsulta & "DECLARE @DESDEERP TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @EMPRESA INT" & vbCrLf
sConsulta = sConsulta & "SET @DESDEERP = 0" & vbCrLf
sConsulta = sConsulta & "SET @EMPRESA= NULL" & vbCrLf
sConsulta = sConsulta & "--Junto con el proveedor viene la forma de pago. As? q con cada cambio de proveedor en pedidos negociados has de cambiar la forma pago" & vbCrLf
sConsulta = sConsulta & "IF @CONFPAGO = 1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE @ERP INT" & vbCrLf
sConsulta = sConsulta & "--La carga de la forma de pago ser? similar a la que se realiza en la emisi?n de pedidos desde GS:" & vbCrLf
sConsulta = sConsulta & "--Cogido de FSWS_PROVES" & vbCrLf
sConsulta = sConsulta & "IF (@USAR_ORG=1 OR @ACCESO_SM=1)" & vbCrLf
sConsulta = sConsulta & "BEGIN --Si no usas org compras o partida presupuestaria, no hay de donde sacar empresa/erp" & vbCrLf
sConsulta = sConsulta & "IF (SELECT ISNULL(TRASPASO_PED_ERP,0) FROM PARGEN_INTERNO WITH (NOLOCK) WHERE ID=1)=1" & vbCrLf
sConsulta = sConsulta & "BEGIN   --Visto en GS q si NO se cumple esto no hay nada q hacer con ERP" & vbCrLf
sConsulta = sConsulta & "IF (SELECT COUNT(1) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA =13 AND ACTIVA=1 AND (SENTIDO=1 OR SENTIDO=3))>0" & vbCrLf
sConsulta = sConsulta & "BEGIN--Si la integraci?n de pedidos (en sentido FULLSTEP->ERP o FULLSTEP<->ERP) est? activada se cargar? por defecto la forma de pago" & vbCrLf
sConsulta = sConsulta & "--del proveedor en el ERP. Si este dato no est? indicado se tomar? la forma de pago del proveedor en FULLSTEP" & vbCrLf
sConsulta = sConsulta & "SET @ERP= NULL" & vbCrLf
sConsulta = sConsulta & "IF @ACCESO_SM=1 --si est? activado el SM se obtendr? la empresa de la partida presupuestaria seleccionada" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "--La partida es, por ejemplo:" & vbCrLf
sConsulta = sConsulta & "--  Uon1#Uon2..#Pres1|Pres2..." & vbCrLf
sConsulta = sConsulta & "--  Uon1#Pres1" & vbCrLf
sConsulta = sConsulta & "--Es decir dispongo en codigo del UON. As? pues se lo puedo pasar a este stored." & vbCrLf
sConsulta = sConsulta & "--Las empresas estan en UON" & vbCrLf
sConsulta = sConsulta & "SET @EMPRESA= 0" & vbCrLf
sConsulta = sConsulta & "SELECT @EMPRESA=ISNULL(U4.EMPRESA,0) FROM UON4 U4 WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE U4.UON1=@UON1 AND U4.UON2=@UON2 AND U4.UON3=@UON3 AND U4.COD=@UON4" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @EMPRESA=0" & vbCrLf
sConsulta = sConsulta & "SELECT @EMPRESA=ISNULL(U3.EMPRESA,0) FROM UON3 U3 WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE U3.UON1=@UON1 AND U3.UON2=@UON2 AND U3.COD=@UON3" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @EMPRESA=0" & vbCrLf
sConsulta = sConsulta & "SELECT @EMPRESA=ISNULL(U2.EMPRESA,0) FROM UON2 U2 WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE U2.UON1=@UON1 AND U2.COD=@UON2" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @EMPRESA=0" & vbCrLf
sConsulta = sConsulta & "SELECT @EMPRESA=ISNULL(U1.EMPRESA,0) FROM UON1 U1 WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE U1.COD=@UON1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @EMPRESA=0" & vbCrLf
sConsulta = sConsulta & "SET @EMPRESA= NULL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Muy bien, en frmpedidos de GS veo q la carga ERP se hace con la funci?n CERPInt.ObtenerCodERP" & vbCrLf
sConsulta = sConsulta & "--si le pasa una empresa, esta es la select" & vbCrLf
sConsulta = sConsulta & "SELECT @ERP=COD" & vbCrLf
sConsulta = sConsulta & "FROM ERP E WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN ERP_SOCIEDAD EO WITH (NOLOCK) ON EO.ERP=E.COD" & vbCrLf
sConsulta = sConsulta & "INNER JOIN EMP WITH(NOLOCK) ON EMP.SOCIEDAD=EO.SOCIEDAD" & vbCrLf
sConsulta = sConsulta & "WHERE EMP.ERP=1 AND EMP.ID =@EMPRESA" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @CODORGCOMPRAS IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "--Vamos a ver en GESTAMP, es una organizaci?n UNA empresa" & vbCrLf
sConsulta = sConsulta & "--Vamos a ver en FERROVIAL, es una organizaci?n VARIAS empresas" & vbCrLf
sConsulta = sConsulta & "--???Problema??? Pues comentado con Mertxe/Teresa/Encarni, lo q tiene sentido es lo de gestamp y Ferr lo hacen de" & vbCrLf
sConsulta = sConsulta & "--otra manera... nunca usa org. OK a top 1." & vbCrLf
sConsulta = sConsulta & "SELECT TOP 1 @EMPRESA=ID FROM EMP WITH(NOLOCK) WHERE ORG_COMPRAS=@CODORGCOMPRAS ORDER BY ID ASC" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Muy bien, en frmPedidos de GS veo q la carga ERP se hace con la funci?n CERPInt.ObtenerCodERP" & vbCrLf
sConsulta = sConsulta & "--si le pasa una org compra, esta es la select" & vbCrLf
sConsulta = sConsulta & "SELECT TOP 1 @ERP=COD FROM ERP E WITH (NOLOCK) INNER JOIN ERP_ORGCOMPRAS EO WITH (NOLOCK) ON EO.ERP=E.COD" & vbCrLf
sConsulta = sConsulta & "WHERE EO.ORGCOMPRAS =@CODORGCOMPRAS" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ERP IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN --integraci?n de pedidos del erp en concreto. Ver si esta activo o no y en sentido exportar o no." & vbCrLf
sConsulta = sConsulta & "IF (SELECT COUNT(1) FROM TABLAS_INTEGRACION_ERP TIE WITH (NOLOCK) WHERE TIE.ERP = @ERP AND TIE.ENTIDAD=13 AND ISNULL(TIE.ACTIVA,0)=1 AND ((ISNULL(TIE.SENTIDO,0)=1) OR (ISNULL(TIE.SENTIDO,0)=3)))>0" & vbCrLf
sConsulta = sConsulta & "BEGIN --integraci?n de pedidos del erp en concreto. Ver si esta activo o no y en sentido exportar o no. Lo esta." & vbCrLf
sConsulta = sConsulta & "SET @DESDEERP = 1" & vbCrLf
sConsulta = sConsulta & "SET @SQLJOINPAG_P = N' INNER JOIN PROVE WITH(NOLOCK) ON PROVE.COD=AA.CODPROVE '" & vbCrLf
sConsulta = sConsulta & "SET @SQLJOINPAG_P = @SQLJOINPAG_P + ' LEFT JOIN PROVE_ERP ERP WITH(NOLOCK) ON ERP.NIF=PROVE.NIF AND ERP.EMPRESA=@EMPRESA'" & vbCrLf
sConsulta = sConsulta & "SET @SQLJOINPAG_D = N' LEFT JOIN PAG_DEN PD WITH(NOLOCK) ON ISNULL(PROVE_ERP.PAG,PROVE.PAG)=PD.PAG AND PD.IDIOMA=@IDI'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN --integraci?n de pedidos del erp en concreto. Ver si esta activo o no y en sentido exportar o no. NO lo esta." & vbCrLf
sConsulta = sConsulta & "SET @SQLJOINPAG_P = N' INNER JOIN PROVE WITH(NOLOCK) ON PROVE.COD=AA.CODPROVE '" & vbCrLf
sConsulta = sConsulta & "SET @SQLJOINPAG_D = N' LEFT JOIN PAG_DEN PD WITH(NOLOCK) ON PROVE.PAG=PD.PAG AND PD.IDIOMA=@IDI'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN --integraci?n de pedidos del erp en concreto. Ver si esta activo o no y en sentido exportar o no. NO lo esta pq no se ha encontrado ERP." & vbCrLf
sConsulta = sConsulta & "SET @SQLJOINPAG_P = N' INNER JOIN PROVE WITH(NOLOCK) ON PROVE.COD=AA.CODPROVE '" & vbCrLf
sConsulta = sConsulta & "SET @SQLJOINPAG_D = N' LEFT JOIN PAG_DEN PD WITH(NOLOCK) ON PROVE.PAG=PD.PAG AND PD.IDIOMA=@IDI'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE --Si la integraci?n de pedidos (en sentido FULLSTEP->ERP o FULLSTEP<->ERP) no est? activada se cargar? por defecto la forma de pago del proveedor en FULLSTEP." & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SQLJOINPAG_P = N' INNER JOIN PROVE WITH(NOLOCK) ON PROVE.COD=AA.CODPROVE '" & vbCrLf
sConsulta = sConsulta & "SET @SQLJOINPAG_D = N' LEFT JOIN PAG_DEN PD WITH(NOLOCK) ON PROVE.PAG=PD.PAG AND PD.IDIOMA=@IDI'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE--Visto en GS q si NO se cumple esto no hay nada q hacer con ERP" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SQLJOINPAG_P = N' INNER JOIN PROVE WITH(NOLOCK) ON PROVE.COD=AA.CODPROVE '" & vbCrLf
sConsulta = sConsulta & "SET @SQLJOINPAG_D = N' LEFT JOIN PAG_DEN PD WITH(NOLOCK) ON PROVE.PAG=PD.PAG AND PD.IDIOMA=@IDI'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE --Si no usas org compras o partida presupuestaria, no hay de donde sacar empresa/erp" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SQLJOINPAG_P = N' INNER JOIN PROVE WITH(NOLOCK) ON PROVE.COD=AA.CODPROVE '" & vbCrLf
sConsulta = sConsulta & "SET @SQLJOINPAG_D = N' LEFT JOIN PAG_DEN PD WITH(NOLOCK) ON PROVE.PAG=PD.PAG AND PD.IDIOMA=@IDI'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--se cargar? con la forma de pago asociada al proveedor. Bien prove_erp o bien prove" & vbCrLf
sConsulta = sConsulta & "IF @DESDEERP=1 --prove_erp" & vbCrLf
sConsulta = sConsulta & "SET @SQLSELPAG = N', ISNULL(PROVE_ERP.PAG,PROVE.PAG) PAG, PD.DEN DENPAG'" & vbCrLf
sConsulta = sConsulta & "ELSE --prove" & vbCrLf
sConsulta = sConsulta & "SET @SQLSELPAG = N', PROVE.PAG PAG, PD.DEN DENPAG'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CODORGCOMPRAS IS NOT NULL OR @CARGAR_ORG=1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SQL2 =@SQL2 +  N',ISNULL(u3.orgcompras,isNull(u2.orgcompras,u1.orgcompras)) CODORGCOMPRAS,o.den DENORGCOMPRAS,isNull(u3.centros,isNull(u2.centros,u1.centros)) CODCENTRO,c.den DENCENTRO '" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CARGAR_CC = 1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SQL2 =@SQL2 +  N',ISNULL(A.PRES5_NIV4,isNull(A.PRES5_NIV3,isNull(A.PRES5_NIV2,isNull(A.PRES5_NIV1,A.PRES5_NIV0)))) CC '" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N' FROM ART4 A WITH (NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + N'INNER JOIN GMN4 G4 WITH (NOLOCK) ON A.GMN1= G4.GMN1 AND A.GMN2= G4.GMN2 AND A.GMN3= G4.GMN3 AND A.GMN4= G4.COD '" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + N'INNER JOIN UNI_DEN WITH (NOLOCK) ON UNI_DEN.UNI = A.UNI AND UNI_DEN.IDIOMA= @IDI '" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + N'INNER JOIN UNI WITH(NOLOCK) ON A.UNI = UNI.COD '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF (@RESTRICUONSOLUSUUON=1 OR @RESTRICUONSOLPERFUON=1) AND @RUON1 IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'INNER JOIN ART4_UON AUR WITH(NOLOCK) ON AUR.ART4=A.COD '" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'INNER JOIN UONSRESTRICCION UONS ON ((AUR.UON1=UONS.UON1 '" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'AND (UONS.UON2 IS NULL OR AUR.UON2=UONS.UON2) '" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'AND (UONS.UON3 IS NULL OR AUR.UON3=UONS.UON3)) OR AUR.UON1 IS NULL) '" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "IF @RUON1 IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'INNER JOIN ART4_UON AU2 WITH(NOLOCK) ON AU2.ART4=A.COD '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Pedidos negociados. La ultima adjudicaci?n solo para el proveedor seleccionado, no de todos como en el resto de casos" & vbCrLf
sConsulta = sConsulta & "IF @CARGAR_ULT_ADJ = 1 AND NOT(@FILTRARNEGOCIADOS=1 OR @FILTRARNEGOCIADOS=3)" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + '  LEFT JOIN ART4_ADJ AA WITH (NOLOCK) ON AA.ART = A.COD AND AA.ID = (SELECT MAX(ID) FROM ART4_ADJ WHERE ART4_ADJ.ART = A.COD) '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CODORGCOMPRAS IS NOT NULL OR @CARGAR_ORG=1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N' INNER JOIN  ART4_UON AU WITH (NOLOCK) ON A.COD = AU.ART4'" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL +' INNER JOIN UON1 u1 WITH (NOLOCK) ON u1.cod =au.uon1'" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL +' INNER JOIN UON2 u2 WITH (NOLOCK) ON u2.cod = au.uon2 and u2.uon1 = au.uon1'" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL +' INNER JOIN UON3 u3 WITH (NOLOCK) ON u3.cod = au.uon3 and u3.uon2 = au.uon2 and u3.uon1 = au.uon1'" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL +' INNER JOIN orgcompras o WITH (NOLOCK) on o.cod = isNull(u3.orgcompras,isNull(u2.orgcompras,u1.orgcompras))'" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL +' INNER JOIN Centros c  WITH (NOLOCK) on c.cod = isNull(u3.centros,isNull(u2.centros,u1.centros))'" & vbCrLf
sConsulta = sConsulta & "--Para filtrar por atributos de UON (OrgCompras)" & vbCrLf
sConsulta = sConsulta & "IF (@CODORGCOMPRAS IS NOT NULL) AND (EXISTS(SELECT 1 FROM @ATRIB_UON))" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   DECLARE @ATRIBOC INT" & vbCrLf
sConsulta = sConsulta & "   DECLARE @NUMOC INT" & vbCrLf
sConsulta = sConsulta & "   DECLARE @SNUMOC NVARCHAR(20)" & vbCrLf
sConsulta = sConsulta & "   SET @NUMOC = 1" & vbCrLf
sConsulta = sConsulta & "   DECLARE CATRIBUTOSOC CURSOR LOCAL FOR" & vbCrLf
sConsulta = sConsulta & "   SELECT ATRIB FROM @ATRIB_UON" & vbCrLf
sConsulta = sConsulta & "   OPEN CATRIBUTOSOC" & vbCrLf
sConsulta = sConsulta & "   FETCH NEXT FROM CATRIBUTOSOC INTO @ATRIBOC" & vbCrLf
sConsulta = sConsulta & "   WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       SET @SNUMOC = CONVERT(NVARCHAR(20),@NUMOC)" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = @SQL + ' LEFT JOIN ART4_UON_ATRIB AUA' + @SNUMOC + ' WITH (NOLOCK) ON AUA' + @SNUMOC + '.ART=AU.ART4 AND AUA' + @SNUMOC + '.UON1=AU.UON1 AND (AUA' + @SNUMOC + '.UON2 IS NULL OR AU.UON2=AUA' + @SNUMOC + '.UON2) AND (AUA' + @SNUMOC + '.UON3 IS NULL OR AU.UON3=AUA' + @SNUMOC + '.UON3) '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = @SQL + ' INNER JOIN @ATRIB_UON ATR' + @SNUMOC + '  WITH (NOLOCK) ON ATR'+@SNUMOC+'.ATRIB=AUA' + @SNUMOC + '.ATRIB AND ATR'+@SNUMOC+'.ATRIB='+CONVERT(NVARCHAR(20),@ATRIBOC)+ ' AND ATR'+@SNUMOC+'.UON1=AUA' + @SNUMOC + '.UON1 AND (ATR'+@SNUMOC+'.UON2='''' OR  AUA' + @SNUMOC + '.UON2=ATR'+@SNUMOC+'.UON2) AND (ATR'+@SNUMOC+'.UON3='''' OR  AUA' + @SNUMOC + '.UON3=ATR'+@SNUMOC+'.UON3) '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = @SQL + ' AND ISNULL(ATR'+@SNUMOC+'.VALOR_TEXT,'''')=ISNULL(AUA' + @SNUMOC + '.VALOR_TEXT,'''') AND ISNULL(ATR'+@SNUMOC+'.VALOR_NUM,'''')=ISNULL(AUA' + @SNUMOC + '.VALOR_NUM,'''') '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = @SQL + ' AND ISNULL(ATR'+@SNUMOC+'.VALOR_BOOL,'''')=ISNULL(AUA' + @SNUMOC + '.VALOR_BOOL,'''') AND ISNULL(ATR'+@SNUMOC+'.VALOR_FEC,'''')=ISNULL(AUA' + @SNUMOC + '.VALOR_FEC,'''') '" & vbCrLf
sConsulta = sConsulta & "       SET @NUMOC = @NUMOC + 1" & vbCrLf
sConsulta = sConsulta & "       FETCH NEXT FROM CATRIBUTOSOC INTO @ATRIBOC" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "   CLOSE CATRIBUTOSOC" & vbCrLf
sConsulta = sConsulta & "   DEALLOCATE CATRIBUTOSOC" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "-------------------------------------" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @RESTRIC = 1" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' INNER JOIN COM_GMN4 CG  WITH (NOLOCK)  on CG.COM = @PER AND A.GMN1 = CG.GMN1 AND A.GMN2 = CG.GMN2 AND A.GMN3 = CG.GMN3 AND A.GMN4 = CG.GMN4'" & vbCrLf
sConsulta = sConsulta & "--INICIO A?ADIDO PARA TAREA 1739---------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL + ' LEFT JOIN" & vbCrLf
sConsulta = sConsulta & "(SELECT COUNT(C.ART_INT) AS PERMISO_EP,C.ART_INT AS CODARTICULO" & vbCrLf
sConsulta = sConsulta & "FROM CATALOG_LIN C WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN VIEW_APROVISONADORES A WITH (NOLOCK) ON C.SEGURIDAD_CATN=A.CATN AND C.SEGURIDAD_NIVEL=A.NIVEL '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @PER IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL + 'WHERE A.PER=@PER '" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL + ' GROUP BY C.ART_INT" & vbCrLf
sConsulta = sConsulta & ") AS PERM_EP ON PERM_EP.CODARTICULO=A.COD '" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL + 'LEFT JOIN" & vbCrLf
sConsulta = sConsulta & "(SELECT COUNT(C.ART_INT) AS ESTA_CATALOGADO,C.ART_INT AS CODARTICULOCATALOGADO" & vbCrLf
sConsulta = sConsulta & "FROM CATALOG_LIN C WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "GROUP BY C.ART_INT" & vbCrLf
sConsulta = sConsulta & ") AS CATALOGADO ON CATALOGADO.CODARTICULOCATALOGADO=A.COD '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @RESTRIC_PED_ART_UON_USU =1 OR @RESTRIC_PED_ART_UON_PERF =1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ' LEFT JOIN(SELECT COUNT(A.COD) AS TIENE_PERMISO,A.COD CODARTICULO FROM( '" & vbCrLf
sConsulta = sConsulta & "IF @RESTRIC_PED_ART_UON_USU = 1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'SELECT ART4_UON.ART4 AS COD FROM ART4_UON WITH (NOLOCK) JOIN PER WITH (NOLOCK) ON ISNULL(PER.UON1,'''') = CASE WHEN PER.UON1 IS NULL  THEN '''' ELSE ART4_UON.UON1 END AND ISNULL(PER.UON2,'''') = CASE WHEN PER.UON2 IS NULL  THEN '''' ELSE ART4_UON.UON2 END AND ISNULL(PER.UON3,'''') = CASE WHEN PER.UON3 IS NULL  THEN '''' ELSE ART4_UON.UON3 END WHERE PER.COD=@PER" & vbCrLf
sConsulta = sConsulta & "UNION" & vbCrLf
sConsulta = sConsulta & "SELECT ART4.COD FROM ART4 WITH (NOLOCK) WHERE ART4.COD NOT IN (SELECT ART4_UON.ART4 FROM ART4_UON) '" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @RESTRIC_PED_ART_UON_PERF = 1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @RESTRIC_PED_ART_UON_USU = 1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ' UNION '" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL +" & vbCrLf
sConsulta = sConsulta & "' SELECT ART4_UON.ART4 AS COD FROM ART4_UON WITH (NOLOCK) JOIN PERF_UON WITH (NOLOCK) ON ISNULL(PERF_UON.UON1,'''') = CASE WHEN PERF_UON.UON1 IS NULL  THEN '''' ELSE ART4_UON.UON1 END AND ISNULL(PERF_UON.UON2,'''') = CASE WHEN PERF_UON.UON2 IS NULL  THEN '''' ELSE ART4_UON.UON2 END AND ISNULL(PERF_UON.UON3,'''') = CASE WHEN PERF_UON.UON3 IS NULL  THEN '''' ELSE ART4_UON.UON3 END WHERE PERF_UON.PERF =@PERF" & vbCrLf
sConsulta = sConsulta & "UNION" & vbCrLf
sConsulta = sConsulta & "SELECT ART4.COD FROM ART4 WITH (NOLOCK) WHERE ART4.COD NOT IN (SELECT ART4_UON.ART4 FROM ART4_UON) '" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "IF @RESTRIC_PED_ART_UON_USU =1 OR @RESTRIC_PED_ART_UON_PERF =1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ') A GROUP BY A.COD ) AS PERMISO_EMISION ON PERMISO_EMISION.CODARTICULO=A.COD '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--FIN A?ADIDO PARA TAREA 1739---------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- Tarea 2904 ------------------------------" & vbCrLf
sConsulta = sConsulta & "IF @CODPROVEEDOR IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @FILTRARNEGOCIADOS=1 --1- art negociado" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "--Negociado de prove dado" & vbCrLf
sConsulta = sConsulta & "--, no necesito mirar prove_gmn4 pq si esta negociado se supone q sera art manejado por prove" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' INNER JOIN (SELECT AA.ART,AA.PREC,AA.CODPROVE,AA.DENPROVE,AA.UNI,ISNULL(UD.DEN,'''') AS DEN, PO.MON MON" & vbCrLf
sConsulta = sConsulta & ", PO.CAMBIO CAMBIO_OFE, P.CAMBIO CAMBIO_PROCE, MD.DEN AS DENMON' + @SQLSELPAG" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' FROM ART4_ADJ AA WITH(NOLOCK)'" & vbCrLf
sConsulta = sConsulta & "--Art4_adj. Guarda en moneda central. Para saber el cambio en moneda de la adjudicaci?n me voy a proceso" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' INNER JOIN PROCE P WITH(NOLOCK) ON P.ANYO=AA.ANYO AND P.GMN1=AA.GMN1 AND P.COD=AA.PROCE'" & vbCrLf
sConsulta = sConsulta & "--Art4_adj. Guarda en moneda central. Para saber la moneda de la adjudicaci?n me voy a la ultima oferta" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' INNER JOIN PROCE_OFE PO WITH(NOLOCK) ON PO.ANYO=AA.ANYO AND PO.GMN1=AA.GMN1 AND PO.PROCE=AA.PROCE AND PO.PROVE=AA.CODPROVE'" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N'    AND PO.OFE = (SELECT MAX(OFE) FROM PROCE_OFE  WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN ITEM_OFE IO WITH(NOLOCK) ON PROCE_OFE.ANYO=IO.ANYO AND PROCE_OFE.GMN1=IO.GMN1 AND PROCE_OFE.PROCE=IO.PROCE AND PROCE_OFE.PROVE=IO.PROVE AND PROCE_OFE.OFE=IO.NUM" & vbCrLf
sConsulta = sConsulta & "INNER JOIN ITEM I WITH(NOLOCK) ON I.ANYO=IO.ANYO AND I.GMN1_PROCE=IO.GMN1 AND I.PROCE=IO.PROCE AND I.ART=AA.ART AND I.ID=IO.ITEM" & vbCrLf
sConsulta = sConsulta & "WHERE PROCE_OFE.ANYO=AA.ANYO AND PROCE_OFE.GMN1=AA.GMN1 AND PROCE_OFE.PROCE=AA.PROCE AND PROCE_OFE.PROVE=AA.CODPROVE)'" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' LEFT JOIN UNI_DEN UD WITH(NOLOCK) ON UD.UNI=AA.UNI AND UD.IDIOMA=@IDI'" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' INNER JOIN MON_DEN MD WITH(NOLOCK) ON PO.MON = MD.MON AND MD.IDIOMA =@IDI'" & vbCrLf
sConsulta = sConsulta & "--Proveedores de la org. compras" & vbCrLf
sConsulta = sConsulta & "IF @CODORGCOMPRAS IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N'  INNER JOIN PROVE WITH(NOLOCK) ON PROVE.COD=AA.CODPROVE '" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N'  INNER JOIN PROVE_ERP WITH(NOLOCK) ON PROVE.NIF = PROVE_ERP.NIF AND PROVE_ERP.BAJALOG=0'" & vbCrLf
sConsulta = sConsulta & "IF @DESDEERP=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL + ' AND PROVE_ERP.EMPRESA=ISNULL(@EMPRESA,PROVE_ERP.EMPRESA)'" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N'  INNER JOIN EMP WITH(NOLOCK) ON PROVE_ERP.EMPRESA = EMP.ID '" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N'  INNER JOIN SOCIEDAD_ORGCOMPRAS WITH(NOLOCK) ON EMP.SOCIEDAD=SOCIEDAD_ORGCOMPRAS.SOCIEDAD '" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N'    AND SOCIEDAD_ORGCOMPRAS.ORGCOMPRAS=@CODORGCOMPRAS'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + @SQLJOINPAG_P" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + @SQLJOINPAG_D" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' WHERE AA.ID = (SELECT MAX(ID) FROM ART4_ADJ  WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE ART4_ADJ.ART=AA.ART" & vbCrLf
sConsulta = sConsulta & "AND ART4_ADJ.FECINI<=GETDATE()" & vbCrLf
sConsulta = sConsulta & "AND ART4_ADJ.FECFIN>=GETDATE()" & vbCrLf
sConsulta = sConsulta & "AND ART4_ADJ.CODPROVE=@CODPROVEEDOR)'" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' ) NEGO ON NEGO.ART=A.COD'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "IF @FILTRARNEGOCIADOS=2 --2- art no negociado" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "--No negociado por nadie" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' LEFT JOIN (SELECT ID, ART'" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' FROM ART4_ADJ AA WITH(NOLOCK)'" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' WHERE AA.FECINI<=GETDATE() AND AA.FECFIN>=GETDATE()'" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' ) NEGO ON NEGO.ART=A.COD'" & vbCrLf
sConsulta = sConsulta & "--es del proveedor dado" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' INNER JOIN PROVE_GMN4 PG WITH(NOLOCK) ON PG.PROVE=@CODPROVEEDOR'" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' AND A.GMN1=PG.GMN1 AND A.GMN2=PG.GMN2 AND A.GMN3=PG.GMN3 AND A.GMN4=PG.GMN4'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @FILTRARNEGOCIADOS=3 --1- art negociado" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "--Negociado de prove dado" & vbCrLf
sConsulta = sConsulta & "--, no necesito mirar prove_gmn4 pq si esta negociado se supone q sera art manejado por prove" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' LEFT JOIN (SELECT AA.ART,AA.PREC,AA.CODPROVE,AA.DENPROVE,AA.UNI,ISNULL(UD.DEN,'''') AS DEN, PO.MON MON" & vbCrLf
sConsulta = sConsulta & ", PO.CAMBIO CAMBIO_OFE, P.CAMBIO CAMBIO_PROCE, MD.DEN AS DENMON' + @SQLSELPAG" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' FROM ART4_ADJ AA WITH(NOLOCK)'" & vbCrLf
sConsulta = sConsulta & "--Art4_adj. Guarda en moneda central. Para saber el cambio en moneda de la adjudicaci?n me voy a proceso" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' LEFT JOIN PROCE P WITH(NOLOCK) ON P.ANYO=AA.ANYO AND P.GMN1=AA.GMN1 AND P.COD=AA.PROCE'" & vbCrLf
sConsulta = sConsulta & "--Art4_adj. Guarda en moneda central. Para saber la moneda de la adjudicaci?n me voy a la ultima oferta" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' LEFT JOIN PROCE_OFE PO WITH(NOLOCK) ON PO.ANYO=AA.ANYO AND PO.GMN1=AA.GMN1 AND PO.PROCE=AA.PROCE AND PO.PROVE=AA.CODPROVE'" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N'    AND PO.OFE = (SELECT MAX(OFE) FROM PROCE_OFE  WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN ITEM_OFE IO WITH(NOLOCK) ON PROCE_OFE.ANYO=IO.ANYO AND PROCE_OFE.GMN1=IO.GMN1 AND PROCE_OFE.PROCE=IO.PROCE AND PROCE_OFE.PROVE=IO.PROVE AND PROCE_OFE.OFE=IO.NUM" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN ITEM I WITH(NOLOCK) ON I.ANYO=IO.ANYO AND I.GMN1_PROCE=IO.GMN1 AND I.PROCE=IO.PROCE AND I.ART=AA.ART AND I.ID=IO.ITEM" & vbCrLf
sConsulta = sConsulta & "WHERE PROCE_OFE.ANYO=AA.ANYO AND PROCE_OFE.GMN1=AA.GMN1 AND PROCE_OFE.PROCE=AA.PROCE AND PROCE_OFE.PROVE=AA.CODPROVE)'" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' LEFT JOIN UNI_DEN UD WITH(NOLOCK) ON UD.UNI=AA.UNI AND UD.IDIOMA=@IDI'" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' LEFT JOIN MON_DEN MD WITH(NOLOCK) ON PO.MON = MD.MON AND MD.IDIOMA =@IDI'" & vbCrLf
sConsulta = sConsulta & "--Proveedores de la org. compras" & vbCrLf
sConsulta = sConsulta & "IF @CODORGCOMPRAS IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N'  LEFT JOIN PROVE WITH(NOLOCK) ON PROVE.COD=AA.CODPROVE '" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N'  INNER JOIN PROVE_ERP WITH(NOLOCK) ON PROVE.NIF = PROVE_ERP.NIF AND PROVE_ERP.BAJALOG=0'" & vbCrLf
sConsulta = sConsulta & "IF @DESDEERP=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL + ' AND PROVE_ERP.EMPRESA=ISNULL(@EMPRESA,PROVE_ERP.EMPRESA)'" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N'  LEFT JOIN EMP WITH(NOLOCK) ON PROVE_ERP.EMPRESA = EMP.ID '" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N'  INNER JOIN SOCIEDAD_ORGCOMPRAS WITH(NOLOCK) ON EMP.SOCIEDAD=SOCIEDAD_ORGCOMPRAS.SOCIEDAD '" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N'    AND SOCIEDAD_ORGCOMPRAS.ORGCOMPRAS=@CODORGCOMPRAS'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + @SQLJOINPAG_P" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + @SQLJOINPAG_D" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' WHERE AA.ID = (SELECT MAX(ID) FROM ART4_ADJ  WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE ART4_ADJ.ART=AA.ART" & vbCrLf
sConsulta = sConsulta & "AND ART4_ADJ.FECINI<=GETDATE()" & vbCrLf
sConsulta = sConsulta & "AND ART4_ADJ.FECFIN>=GETDATE()" & vbCrLf
sConsulta = sConsulta & "AND ART4_ADJ.CODPROVE=@CODPROVEEDOR)'" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' ) NEGO ON NEGO.ART=A.COD'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @FILTRARNEGOCIADOS=0 --0- Todos" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "--es del proveedor dado" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' INNER JOIN PROVE_GMN4 PG WITH(NOLOCK) ON PG.PROVE=@CODPROVEEDOR'" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' AND A.GMN1=PG.GMN1 AND A.GMN2=PG.GMN2 AND A.GMN3=PG.GMN3 AND A.GMN4=PG.GMN4'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @FILTRARNEGOCIADOS=1 --1- art negociado" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "--Ultimo negociado del art" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' INNER JOIN (SELECT  AA.ART,AA.ID,AA.PREC,AA.CODPROVE,AA.DENPROVE,AA.UNI,ISNULL(UD.DEN,'''') AS DEN, PO.MON MON" & vbCrLf
sConsulta = sConsulta & ", PO.CAMBIO CAMBIO_OFE, P.CAMBIO CAMBIO_PROCE, MD.DEN AS DENMON' + @SQLSELPAG" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' FROM ART4_ADJ AA WITH(NOLOCK)'" & vbCrLf
sConsulta = sConsulta & "--Art4_adj. Guarda en moneda central. Para saber el cambio en moneda de la adjudicaci?n me voy a proceso" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' INNER JOIN PROCE P WITH(NOLOCK) ON P.ANYO=AA.ANYO AND P.GMN1=AA.GMN1 AND P.COD=AA.PROCE'" & vbCrLf
sConsulta = sConsulta & "--Art4_adj. Guarda en moneda central. Para saber la moneda de la adjudicaci?n me voy a la ultima oferta" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' INNER JOIN PROCE_OFE PO WITH(NOLOCK) ON PO.ANYO=AA.ANYO AND PO.GMN1=AA.GMN1 AND PO.PROCE=AA.PROCE AND PO.PROVE=AA.CODPROVE'" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N'    AND PO.OFE = (SELECT MAX(OFE) FROM PROCE_OFE  WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN ITEM_OFE IO WITH(NOLOCK) ON PROCE_OFE.ANYO=IO.ANYO AND PROCE_OFE.GMN1=IO.GMN1 AND PROCE_OFE.PROCE=IO.PROCE AND PROCE_OFE.PROVE=IO.PROVE AND PROCE_OFE.OFE=IO.NUM" & vbCrLf
sConsulta = sConsulta & "INNER JOIN ITEM I WITH(NOLOCK) ON I.ANYO=IO.ANYO AND I.GMN1_PROCE=IO.GMN1 AND I.PROCE=IO.PROCE AND I.ART=AA.ART AND I.ID=IO.ITEM" & vbCrLf
sConsulta = sConsulta & "WHERE PROCE_OFE.ANYO=AA.ANYO AND PROCE_OFE.GMN1=AA.GMN1 AND PROCE_OFE.PROCE=AA.PROCE AND PROCE_OFE.PROVE=AA.CODPROVE)'" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' LEFT JOIN UNI_DEN UD WITH(NOLOCK) ON UD.UNI=AA.UNI AND UD.IDIOMA=@IDI'" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' INNER JOIN MON_DEN MD WITH(NOLOCK) ON PO.MON = MD.MON AND MD.IDIOMA =@IDI'" & vbCrLf
sConsulta = sConsulta & "--Proveedores de la org. compras" & vbCrLf
sConsulta = sConsulta & "IF @CODORGCOMPRAS IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N'  INNER JOIN PROVE WITH(NOLOCK) ON PROVE.COD=AA.CODPROVE '" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N'  INNER JOIN PROVE_ERP WITH(NOLOCK) ON PROVE.NIF = PROVE_ERP.NIF AND PROVE_ERP.BAJALOG=0'" & vbCrLf
sConsulta = sConsulta & "IF @DESDEERP=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL + ' AND PROVE_ERP.EMPRESA=ISNULL(@EMPRESA,PROVE_ERP.EMPRESA)'" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N'  INNER JOIN EMP WITH(NOLOCK) ON PROVE_ERP.EMPRESA = EMP.ID '" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N'  INNER JOIN SOCIEDAD_ORGCOMPRAS WITH(NOLOCK) ON EMP.SOCIEDAD=SOCIEDAD_ORGCOMPRAS.SOCIEDAD '" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N'    AND SOCIEDAD_ORGCOMPRAS.ORGCOMPRAS=@CODORGCOMPRAS'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + @SQLJOINPAG_P" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + @SQLJOINPAG_D" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' WHERE AA.FECINI<=GETDATE() AND AA.FECFIN>=GETDATE()'" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' ) NEGO ON NEGO.ART=A.COD'" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' AND NEGO.ID = (SELECT MAX(ID) FROM ART4_ADJ WITH(NOLOCK)'" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N'  WHERE ART4_ADJ.ART = A.COD AND ART4_ADJ.FECINI<=GETDATE() AND ART4_ADJ.FECFIN>=GETDATE())'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "IF @FILTRARNEGOCIADOS=2 --2- art no negociado" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "--No negociado por nadie" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' LEFT JOIN (SELECT ID, ART'" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' FROM ART4_ADJ AA WITH(NOLOCK)'" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' WHERE AA.FECINI<=GETDATE() AND AA.FECFIN>=GETDATE()'" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' ) NEGO ON NEGO.ART=A.COD'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "IF @FILTRARNEGOCIADOS=3 --1 EXPRESS NEGOCIADOS O NO" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "--Ultimo negociado del art" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' LEFT JOIN (SELECT AA.ART,AA.ID,AA.PREC,AA.CODPROVE,AA.DENPROVE,AA.UNI,ISNULL(UD.DEN,'''') AS DEN, PO.MON MON" & vbCrLf
sConsulta = sConsulta & ", PO.CAMBIO CAMBIO_OFE, P.CAMBIO CAMBIO_PROCE, MD.DEN AS DENMON' + @SQLSELPAG" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' FROM ART4_ADJ AA WITH(NOLOCK)'" & vbCrLf
sConsulta = sConsulta & "--Art4_adj. Guarda en moneda central. Para saber el cambio en moneda de la adjudicaci?n me voy a proceso" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' LEFT JOIN PROCE P WITH(NOLOCK) ON P.ANYO=AA.ANYO AND P.GMN1=AA.GMN1 AND P.COD=AA.PROCE'" & vbCrLf
sConsulta = sConsulta & "--Art4_adj. Guarda en moneda central. Para saber la moneda de la adjudicaci?n me voy a la ultima oferta" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' LEFT JOIN PROCE_OFE PO WITH(NOLOCK) ON PO.ANYO=AA.ANYO AND PO.GMN1=AA.GMN1 AND PO.PROCE=AA.PROCE AND PO.PROVE=AA.CODPROVE'" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N'    AND PO.OFE = (SELECT MAX(OFE) FROM PROCE_OFE  WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN ITEM_OFE IO WITH(NOLOCK) ON PROCE_OFE.ANYO=IO.ANYO AND PROCE_OFE.GMN1=IO.GMN1 AND PROCE_OFE.PROCE=IO.PROCE AND PROCE_OFE.PROVE=IO.PROVE AND PROCE_OFE.OFE=IO.NUM" & vbCrLf
sConsulta = sConsulta & "INNER JOIN ITEM I WITH(NOLOCK) ON I.ANYO=IO.ANYO AND I.GMN1_PROCE=IO.GMN1 AND I.PROCE=IO.PROCE AND I.ART=AA.ART AND I.ID=IO.ITEM" & vbCrLf
sConsulta = sConsulta & "WHERE PROCE_OFE.ANYO=AA.ANYO AND PROCE_OFE.GMN1=AA.GMN1 AND PROCE_OFE.PROCE=AA.PROCE AND PROCE_OFE.PROVE=AA.CODPROVE)'" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' LEFT JOIN UNI_DEN UD WITH(NOLOCK) ON UD.UNI=AA.UNI AND UD.IDIOMA=@IDI'" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' INNER JOIN MON_DEN MD WITH(NOLOCK) ON PO.MON = MD.MON AND MD.IDIOMA =@IDI'" & vbCrLf
sConsulta = sConsulta & "--Proveedores de la org. compras" & vbCrLf
sConsulta = sConsulta & "IF @CODORGCOMPRAS IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N'  LEFT JOIN PROVE WITH(NOLOCK) ON PROVE.COD=AA.CODPROVE '" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N'  INNER JOIN PROVE_ERP WITH(NOLOCK) ON PROVE.NIF = PROVE_ERP.NIF AND PROVE_ERP.BAJALOG=0'" & vbCrLf
sConsulta = sConsulta & "IF @DESDEERP=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL + ' AND PROVE_ERP.EMPRESA=ISNULL(@EMPRESA,PROVE_ERP.EMPRESA)'" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N'  INNER JOIN EMP WITH(NOLOCK) ON PROVE_ERP.EMPRESA = EMP.ID '" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N'  INNER JOIN SOCIEDAD_ORGCOMPRAS WITH(NOLOCK) ON EMP.SOCIEDAD=SOCIEDAD_ORGCOMPRAS.SOCIEDAD '" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N'    AND SOCIEDAD_ORGCOMPRAS.ORGCOMPRAS=@CODORGCOMPRAS'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + @SQLJOINPAG_P" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + @SQLJOINPAG_D" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' WHERE AA.FECINI<=GETDATE() AND AA.FECFIN>=GETDATE()'" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' ) NEGO ON NEGO.ART=A.COD'" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' AND NEGO.ID = (SELECT MAX(ID) FROM ART4_ADJ WITH(NOLOCK)'" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N'  WHERE ART4_ADJ.ART = A.COD AND ART4_ADJ.FECINI<=GETDATE() AND ART4_ADJ.FECFIN>=GETDATE())'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "-- FIN Tarea 2904 --------------------------" & vbCrLf
sConsulta = sConsulta & "--Para filtrar por atributos de UON (UON)" & vbCrLf
sConsulta = sConsulta & "IF (@RUON1 IS NOT NULL) AND (EXISTS(SELECT 1 FROM @ATRIB_UON))" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   DECLARE @ATRIB INT" & vbCrLf
sConsulta = sConsulta & "   DECLARE @NUM INT" & vbCrLf
sConsulta = sConsulta & "   DECLARE @SNUM NVARCHAR(20)" & vbCrLf
sConsulta = sConsulta & "   SET @NUM = 1" & vbCrLf
sConsulta = sConsulta & "   DECLARE CATRIBUTOS CURSOR LOCAL FOR" & vbCrLf
sConsulta = sConsulta & "   SELECT ATRIB FROM @ATRIB_UON" & vbCrLf
sConsulta = sConsulta & "   OPEN CATRIBUTOS" & vbCrLf
sConsulta = sConsulta & "   FETCH NEXT FROM CATRIBUTOS INTO @ATRIB" & vbCrLf
sConsulta = sConsulta & "   WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       SET @SNUM = CONVERT(NVARCHAR(20),@NUM)" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = @SQL + ' LEFT JOIN ART4_UON_ATRIB AUA' + @SNUM + '  WITH(NOLOCK) ON AUA' + @SNUM + '.ART=AU2.ART4 AND AUA' + @SNUM + '.UON1=AU2.UON1 AND (AUA' + @SNUM + '.UON2 IS NULL OR AU2.UON2=AUA' + @SNUM + '.UON2) AND (AUA' + @SNUM + '.UON3 IS NULL OR AU2.UON3=AUA' + @SNUM + '.UON3) '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = @SQL + ' INNER JOIN @ATRIB_UON ATR' + @SNUM + '  WITH(NOLOCK)  ON ATR'+@SNUM+'.ATRIB=AUA' + @SNUM + '.ATRIB AND ATR'+@SNUM+'.ATRIB='+CONVERT(NVARCHAR(20),@ATRIB)+ ' AND ATR'+@SNUM+'.UON1=AUA' + @SNUM + '.UON1 AND (ATR'+@SNUM+'.UON2='''' OR  AUA' + @SNUM + '.UON2=ATR'+@SNUM+'.UON2) AND (ATR'+@SNUM+'.UON3='''' OR  AUA' + @SNUM + '.UON3=ATR'+@SNUM+'.UON3) '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = @SQL + ' AND ISNULL(ATR'+@SNUM+'.VALOR_TEXT,'''')=ISNULL(AUA' + @SNUM + '.VALOR_TEXT,'''') AND ISNULL(ATR'+@SNUM+'.VALOR_NUM,'''')=ISNULL(AUA' + @SNUM + '.VALOR_NUM,'''') '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = @SQL + ' AND ISNULL(ATR'+@SNUM+'.VALOR_BOOL,'''')=ISNULL(AUA' + @SNUM + '.VALOR_BOOL,'''') AND ISNULL(ATR'+@SNUM+'.VALOR_FEC,'''')=ISNULL(AUA' + @SNUM + '.VALOR_FEC,'''') '" & vbCrLf
sConsulta = sConsulta & "       SET @NUM = @NUM + 1" & vbCrLf
sConsulta = sConsulta & "       FETCH NEXT FROM CATRIBUTOS INTO @ATRIB" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "   CLOSE CATRIBUTOS" & vbCrLf
sConsulta = sConsulta & "   DEALLOCATE CATRIBUTOS" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "--------------------------------" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N' WHERE 1 = 1 '" & vbCrLf
sConsulta = sConsulta & "IF @RUON1 IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'AND (AU2.UON1=@RUON1 AND (@RUON2 IS NULL OR AU2.UON2=@RUON2) AND (@RUON3 IS NULL OR AU2.UON3=@RUON3)) '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @COD IS NOT NULL AND @COD<>''" & vbCrLf
sConsulta = sConsulta & "IF @COINCID=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' AND A.COD = @COD'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' AND A.COD LIKE @COD + ''%'''" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @DEN IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "IF @COINCID=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' AND A.DEN = @DEN'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' AND A.DEN LIKE @DEN + ''%'''" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @GMN1 IS NOT NULL AND @GMN1<>''" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' AND A.GMN1 = @GMN1'" & vbCrLf
sConsulta = sConsulta & "IF @GMN2 IS NOT NULL AND @GMN2<>''" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' AND A.GMN2 = @GMN2'" & vbCrLf
sConsulta = sConsulta & "IF @GMN3 IS NOT NULL AND @GMN3<>''" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' AND A.GMN3 = @GMN3'" & vbCrLf
sConsulta = sConsulta & "IF @GMN4 IS NOT NULL AND @GMN4<>''" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' AND A.GMN4 = @GMN4'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF (@CODORGCOMPRAS IS NOT NULL)" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N' and isNull(u3.orgcompras,isNull(u2.orgcompras,u1.orgcompras)) =@CODORGCOMPRAS'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF (@CODCENTRO IS NOT NULL)" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N' and isNull(u3.centros,isNull(u2.centros,u1.centros)) = @CODCENTRO'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CODINI IS NOT NULL AND (@COD IS NULL OR (@COD IS NOT NULL AND @COINCID<>1))" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SQL3=@SQL" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N'  AND A.COD" & vbCrLf
sConsulta = sConsulta & "NOT IN" & vbCrLf
sConsulta = sConsulta & "(SELECT TOP ' + CAST(ISNULL(@DESDEFILA,1)-1 AS NVARCHAR(10)) + N'   A.COD ' + @SQL3 + ' ORDER BY A.COD)'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @CODULT IS NOT NULL AND (@COD IS NULL OR (@COD IS NOT NULL AND @COINCID<>1))" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N'  AND A.COD > @CODULT'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "----------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "-- OTRAS CARACTERISTICAS" & vbCrLf
sConsulta = sConsulta & "----------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "--Gen?ricos" & vbCrLf
sConsulta = sConsulta & "IF @GENERICOS=0" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' AND A.GENERICO=1'" & vbCrLf
sConsulta = sConsulta & "IF @GENERICOS=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' AND A.GENERICO=0'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Concepto" & vbCrLf
sConsulta = sConsulta & "IF @GASTO=1 OR @INVERSION=1 OR @GASTOINVERSION=1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' AND A.CONCEPTO IN ('" & vbCrLf
sConsulta = sConsulta & "IF @GASTO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL + '0,'" & vbCrLf
sConsulta = sConsulta & "IF @INVERSION=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL + '1,'" & vbCrLf
sConsulta = sConsulta & "IF @GASTOINVERSION=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL + '2,'" & vbCrLf
sConsulta = sConsulta & "SET @SQL= LEFT(@SQL,LEN(@SQL)-1) + ')'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Almacenar" & vbCrLf
sConsulta = sConsulta & "IF @NOALMACENABLE=1 OR @ALMACENABLE=1 OR @ALMACENOPCIONAL=1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' AND A.ALMACENAR IN ('" & vbCrLf
sConsulta = sConsulta & "IF @NOALMACENABLE=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL + '0,'" & vbCrLf
sConsulta = sConsulta & "IF @ALMACENABLE=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL + '1,'" & vbCrLf
sConsulta = sConsulta & "IF @ALMACENOPCIONAL=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL + '2,'" & vbCrLf
sConsulta = sConsulta & "SET @SQL= LEFT(@SQL,LEN(@SQL)-1) + ')'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Recepcionar" & vbCrLf
sConsulta = sConsulta & "IF @RECEPCIONOBLIG=1 OR @NORECEPCIONABLE=1 OR @RECEPCIONOPCIONAL=1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' AND A.RECEPCIONAR IN ('" & vbCrLf
sConsulta = sConsulta & "IF @NORECEPCIONABLE=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL + '0,'" & vbCrLf
sConsulta = sConsulta & "IF @RECEPCIONOBLIG=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL + '1,'" & vbCrLf
sConsulta = sConsulta & "IF @RECEPCIONOPCIONAL=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL + '2,'" & vbCrLf
sConsulta = sConsulta & "SET @SQL= LEFT(@SQL,LEN(@SQL)-1) + ')'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Catalogado" & vbCrLf
sConsulta = sConsulta & "--INICIO A?ADIDO PARA TAREA 1739---------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "IF @MATERIALESCATALOGADOS=0" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL + ' AND CATALOGADO.ESTA_CATALOGADO IS NULL'" & vbCrLf
sConsulta = sConsulta & "--FIN A?ADIDO PARA TAREA 1739---------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "IF @MATERIALESCATALOGADOS=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL + ' AND CATALOGADO.ESTA_CATALOGADO IS NOT NULL'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- Tarea 2904 ------------------------------" & vbCrLf
sConsulta = sConsulta & "IF @FILTRARNEGOCIADOS=2 --No negociado por nadie" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL + ' AND (NEGO.ID IS NULL OR A.GENERICO=1)'" & vbCrLf
sConsulta = sConsulta & "-- FIN Tarea 2904 ------------------------------" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL2 + @SQL + N' ORDER BY A.COD'" & vbCrLf
sConsulta = sConsulta & "SET @PARAM = N'@COD VARCHAR(50)=NULL,@DEN VARCHAR(2000)=NULL,@GMN1 VARCHAR(50)=NULL,@GMN2 VARCHAR(50)=NULL,@GMN3 VARCHAR(50)=NULL,@GMN4 VARCHAR(50)=NULL,@COINCID TINYINT=1,@PER VARCHAR(50)=NULL,@GENERICOS TINYINT=0,@CODORGCOMPRAS VARCHAR(4),@CODCENTRO VARCHAR(4)=NULL,@CODINI VARCHAR(50),@CODULT VARCHAR(50),@EQUIV FLOAT,@EQUIVINS FLOAT,@GASTO TINYINT,@INVERSION TINYINT,@GASTOINVERSION TINYINT,@NOALMACENABLE TINYINT,@ALMACENABLE TINYINT,@ALMACENOPCIONAL TINYINT,@RECEPCIONOBLIG TINYINT,@NORECEPCIONABLE TINYINT, @RECEPCIONOPCIONAL TINYINT, @IDI VARCHAR(50), @CODPROVEEDOR VARCHAR(50)=NULL, @EMPRESA INT=NULL,@PERF INT,@RUON1 NVARCHAR(50),@RUON2 NVARCHAR(50),@RUON3 NVARCHAR(50),@USU NVARCHAR(50),@ATRIB_UON TBATRIBUON READONLY'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, @PARAM, @COD = @COD, @DEN=@DEN, @GMN1=@GMN1, @GMN2=@GMN2, @GMN3=@GMN3, @GMN4=@GMN4, @COINCID=@COINCID, @PER=@PER, @GENERICOS=@GENERICOS, @CODORGCOMPRAS=@CODORGCOMPRAS,@CODCENTRO=@CODCENTRO,@CODINI=@CODINI,@CODULT=@CODULT,@EQUIV=@EQUIV,@EQUIVINS=@EQUIVINS,@GASTO=@GASTO, @INVERSION=@INVERSION, @GASTOINVERSION=@GASTOINVERSION, @NOALMACENABLE=@NOALMACENABLE, @ALMACENABLE=@ALMACENABLE, @ALMACENOPCIONAL=@ALMACENOPCIONAL, @RECEPCIONOBLIG=@RECEPCIONOBLIG, @NORECEPCIONABLE=@NORECEPCIONABLE, @RECEPCIONOPCIONAL=@RECEPCIONOPCIONAL,@IDI=@IDI, @CODPROVEEDOR=@CODPROVEEDOR, @EMPRESA=@EMPRESA,@PERF=@PERF,@RUON1=@RUON1,@RUON2=@RUON2,@RUON3=@RUON3,@USU=@USU,@ATRIB_UON=@ATRIB_UON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSSM_DEVOLVER_PARTIDAS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSSM_DEVOLVER_PARTIDAS]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSSM_DEVOLVER_PARTIDAS] @PRES5 VARCHAR(20),@IDI VARCHAR(20),@USU VARCHAR(50)=NULL,@UON1 VARCHAR(10)=NULL,@UON2 VARCHAR(10)=NULL,@UON3 VARCHAR(10)=NULL,@UON4 VARCHAR(10)=NULL,@VIGENTES TINYINT=1,@FECHAINICIODESDE DATETIME=NULL,@FECHAFINHASTA DATETIME=NULL,@FORMATOFECHA AS VARCHAR(50)=NULL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-----------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "-- Descripci?n: Devuelve las partidas y las uons para mostrarlas en un treeview." & vbCrLf
sConsulta = sConsulta & "-- Par?metros de entrada: @PRES5: C?digo de la partida presupuetaria" & vbCrLf
sConsulta = sConsulta & "--         @IDI: Idioma del usuario" & vbCrLf
sConsulta = sConsulta & "--         @USU: C?digo de la persona" & vbCrLf
sConsulta = sConsulta & "--         @UON1: cod de la unidad organizativa de nivel 1" & vbCrLf
sConsulta = sConsulta & "--         @UON2: cod de la unidad organizativa de nivel 2" & vbCrLf
sConsulta = sConsulta & "--         @UON3: cod de la unidad organizativa de nivel 3" & vbCrLf
sConsulta = sConsulta & "--         @UON4: cod de la unidad organizativa de nivel 4" & vbCrLf
sConsulta = sConsulta & "--         @VIGENTES: Si se muestran las partidas vigentes o todas las partidas" & vbCrLf
sConsulta = sConsulta & "--         @FECHAINICIODESDE: Fecha de inicio desde para buscar partidas" & vbCrLf
sConsulta = sConsulta & "--         @FECHAFINHASTA: Fecha fin hasta para buscar partidas" & vbCrLf
sConsulta = sConsulta & "--         @FORMATOFECHA: Formato en el que llegan las fechas" & vbCrLf
sConsulta = sConsulta & "-- Par?metros de salida: las partidas y las uons." & vbCrLf
sConsulta = sConsulta & "-- Llamada desde: SMdatabaseServer --> Root.vb --> Partidas_Load; Tiempo m?ximo: 2 sg." & vbCrLf
sConsulta = sConsulta & "-- Revisado por ILG 20/10/2011" & vbCrLf
sConsulta = sConsulta & "-----------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL  AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLWHERE  AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @TIPOFECHA AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @NIVEL TINYINT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLWHERE= N' AND P5I.CERRADO = 0'" & vbCrLf
sConsulta = sConsulta & "SET @TIPOFECHA=103" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @FORMATOFECHA='dd/mm/yyyy'" & vbCrLf
sConsulta = sConsulta & "SET @TIPOFECHA=103" & vbCrLf
sConsulta = sConsulta & "ELSE IF @FORMATOFECHA='mm/dd/yyyy'" & vbCrLf
sConsulta = sConsulta & "SET @TIPOFECHA=101" & vbCrLf
sConsulta = sConsulta & "ELSE IF @FORMATOFECHA='dd-mm-yyyy'" & vbCrLf
sConsulta = sConsulta & "SET @TIPOFECHA=105" & vbCrLf
sConsulta = sConsulta & "ELSE IF @FORMATOFECHA='mm-dd-yyyy'" & vbCrLf
sConsulta = sConsulta & "SET @TIPOFECHA=110" & vbCrLf
sConsulta = sConsulta & "ELSE IF @FORMATOFECHA='mm.dd.yyyy'" & vbCrLf
sConsulta = sConsulta & "SET @TIPOFECHA=104" & vbCrLf
sConsulta = sConsulta & "----------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "IF @UON1 IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SET @SQLWHERE = @SQLWHERE + '   AND U.UON1 = @UON1'" & vbCrLf
sConsulta = sConsulta & "IF @UON2 IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SET @SQLWHERE = @SQLWHERE + '   AND U.UON2 = @UON2'" & vbCrLf
sConsulta = sConsulta & "IF @UON3 IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SET @SQLWHERE = @SQLWHERE + '   AND U.UON3 = @UON3'" & vbCrLf
sConsulta = sConsulta & "IF @UON4 IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SET @SQLWHERE = @SQLWHERE + '   AND U.UON4 = @UON4'" & vbCrLf
sConsulta = sConsulta & "IF @VIGENTES=1" & vbCrLf
sConsulta = sConsulta & "SET @SQLWHERE = @SQLWHERE + ' AND ((P5I.FECINI<GETDATE() OR P5I.FECINI IS NULL) AND (P5I.FECFIN>GETDATE() OR P5I.FECFIN IS NULL))'" & vbCrLf
sConsulta = sConsulta & "IF @FECHAINICIODESDE IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SET @SQLWHERE = @SQLWHERE + ' AND P5I.FECINI>=CONVERT(datetime,@FECHAINICIODESDE,@TIPOFECHA)'" & vbCrLf
sConsulta = sConsulta & "IF @FECHAFINHASTA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SET @SQLWHERE = @SQLWHERE + ' AND P5I.FECFIN<=CONVERT(datetime,@FECHAFINHASTA,@TIPOFECHA)'" & vbCrLf
sConsulta = sConsulta & "----------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @NIVEL=1 --por si acaso" & vbCrLf
sConsulta = sConsulta & "SELECT @NIVEL = IMP_NIVEL FROM PARGEN_SM WITH(NOLOCK) WHERE PRES5=@PRES5" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--creamos una tabla temporal para guardar las partidas" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE #PARTIDAS (UON1 VARCHAR(10) COLLATE DATABASE_DEFAULT, UON2 VARCHAR(10) COLLATE DATABASE_DEFAULT, UON3 VARCHAR(10) COLLATE DATABASE_DEFAULT, UON4 VARCHAR(10) COLLATE DATABASE_DEFAULT, COD VARCHAR(100) COLLATE DATABASE_DEFAULT, DEN VARCHAR(500) COLLATE DATABASE_DEFAULT, FECINI DATETIME, FECFIN DATETIME, VIGENTE TINYINT, NIVEL TINYINT, IMPUTABLE TINYINT)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @USU IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "---- NIVEL 1 ----" & vbCrLf
sConsulta = sConsulta & "SET @SQL=N'SELECT U.UON1, U.UON2, U.UON3, U.UON4,P.COD, P.COD + '' - '' + I.DEN AS DEN, P5I.FECINI, P5I.FECFIN, case when P5I.FECINI<GETDATE() AND P5I.FECFIN>GETDATE() then 1 else 0 end as VIGENTE, 1 as NIVEL '" & vbCrLf
sConsulta = sConsulta & "IF @NIVEL > 1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ',0 AS IMPUTABLE'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ',1 AS IMPUTABLE'" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ' FROM PRES5_NIV1 P WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PRES5_IDIOMAS I WITH(NOLOCK) ON P.PRES0=I.PRES0 AND P.COD=I.PRES1 AND I.PRES2 IS NULL AND I.PRES3 IS NULL AND I.PRES4 IS NULL AND P.BAJALOG=0" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PRES5_UON U WITH(NOLOCK) ON P.PRES0=U.PRES0 AND P.COD=U.PRES1 AND U.PRES2 IS NULL AND U.PRES3 IS NULL AND U.PRES4 IS NULL" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PRES5_IMPORTES P5I WITH(NOLOCK) ON P.PRES0=P5I.PRES0 AND P.COD=P5I.PRES1 AND P5I.PRES2 IS NULL AND P5I.PRES3 IS NULL AND P5I.PRES4 IS NULL" & vbCrLf
sConsulta = sConsulta & "WHERE P.PRES0 = @PRES5 AND I.IDIOMA=@IDI ' + @SQLWHERE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL=N'INSERT INTO #PARTIDAS (UON1,UON2,UON3,UON4,COD,DEN,FECINI,FECFIN,VIGENTE,NIVEL,IMPUTABLE) ' + @SQL" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@PRES5 NVARCHAR(20),@IDI NVARCHAR(20),@USU NVARCHAR(50),@UON1 AS VARCHAR(10),@UON2 AS VARCHAR(10),@UON3 AS VARCHAR(10),@UON4 AS VARCHAR(10),@VIGENTES TINYINT,@FECHAINICIODESDE DATETIME,@FECHAFINHASTA DATETIME,@TIPOFECHA INT', @PRES5=@PRES5,@IDI=@IDI,@USU=@USU,@UON1=@UON1,@UON2=@UON2,@UON3=@UON3,@UON4=@UON4,@VIGENTES=@VIGENTES,@FECHAINICIODESDE=@FECHAINICIODESDE,@FECHAFINHASTA=@FECHAFINHASTA,@TIPOFECHA=@TIPOFECHA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "---- NIVEL 2 ----" & vbCrLf
sConsulta = sConsulta & "IF @NIVEL > 1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SQL=N'SELECT U.UON1, U.UON2, U.UON3, U.UON4,P.PRES1 + ''|'' + P.COD, P.COD + '' - '' + I.DEN AS DEN, P5I.FECINI, P5I.FECFIN, case when P5I.FECINI<GETDATE() AND P5I.FECFIN>GETDATE() then 1 else 0 end as VIGENTE, 2 as NIVEL'" & vbCrLf
sConsulta = sConsulta & "IF @NIVEL > 2" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ',0 AS IMPUTABLE'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ',1 AS IMPUTABLE'" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ' FROM PRES5_NIV2 P WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PRES5_IDIOMAS I WITH(NOLOCK) ON P.PRES0=I.PRES0 AND P.PRES1=I.PRES1 AND P.COD=I.PRES2 AND I.PRES3 IS NULL AND I.PRES4 IS NULL AND P.BAJALOG=0" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PRES5_UON U WITH(NOLOCK) ON P.PRES0=U.PRES0 AND P.PRES1=U.PRES1 AND P.COD=U.PRES2 AND U.PRES3 IS NULL AND U.PRES4 IS NULL" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PRES5_IMPORTES P5I WITH(NOLOCK) ON P.PRES0=P5I.PRES0 AND P.PRES1=P5I.PRES1 AND P.COD=P5I.PRES2 AND P5I.PRES3 IS NULL AND P5I.PRES4 IS NULL" & vbCrLf
sConsulta = sConsulta & "WHERE P.PRES0 = @PRES5 AND I.IDIOMA=@IDI ' + @SQLWHERE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL='INSERT INTO #PARTIDAS (UON1,UON2,UON3,UON4,COD,DEN,FECINI,FECFIN,VIGENTE,NIVEL,IMPUTABLE) ' + @SQL" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@PRES5 NVARCHAR(20),@IDI NVARCHAR(20),@USU NVARCHAR(50),@UON1 AS VARCHAR(10),@UON2 AS VARCHAR(10),@UON3 AS VARCHAR(10),@UON4 AS VARCHAR(10),@VIGENTES TINYINT,@FECHAINICIODESDE DATETIME,@FECHAFINHASTA DATETIME,@TIPOFECHA INT', @PRES5=@PRES5,@IDI=@IDI,@USU=@USU,@UON1=@UON1,@UON2=@UON2,@UON3=@UON3,@UON4=@UON4,@VIGENTES=@VIGENTES,@FECHAINICIODESDE=@FECHAINICIODESDE,@FECHAFINHASTA=@FECHAFINHASTA,@TIPOFECHA=@TIPOFECHA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "---- NIVEL 3 ----" & vbCrLf
sConsulta = sConsulta & "IF @NIVEL > 2" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SQL=N'SELECT U.UON1, U.UON2, U.UON3, U.UON4,P.PRES1 + ''|'' + P.PRES2 + ''|'' + P.COD, P.COD + '' - '' + I.DEN AS DEN, P5I.FECINI, P5I.FECFIN, case when P5I.FECINI<GETDATE() AND P5I.FECFIN>GETDATE() then 1 else 0 end as VIGENTE, 3 as NIVEL'" & vbCrLf
sConsulta = sConsulta & "IF @NIVEL > 3" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ',0 AS IMPUTABLE'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ',1 AS IMPUTABLE'" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ' FROM PRES5_NIV3 P WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PRES5_IDIOMAS I WITH(NOLOCK) ON P.PRES0=I.PRES0 AND P.PRES1=I.PRES1 AND P.PRES2=I.PRES2 AND P.COD=I.PRES3 AND I.PRES4 IS NULL AND P.BAJALOG=0" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PRES5_UON U WITH(NOLOCK) ON P.PRES0=U.PRES0 AND P.PRES1=U.PRES1 AND P.PRES2=U.PRES2 AND P.COD=U.PRES3 AND U.PRES4 IS NULL" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PRES5_IMPORTES P5I WITH(NOLOCK) ON P.PRES0=P5I.PRES0 AND P.PRES1=P5I.PRES1 AND P.PRES2=P5I.PRES2 AND P.COD=P5I.PRES3 AND P5I.PRES4 IS NULL" & vbCrLf
sConsulta = sConsulta & "WHERE P.PRES0 = @PRES5 AND I.IDIOMA=@IDI ' + @SQLWHERE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL='INSERT INTO #PARTIDAS (UON1,UON2,UON3,UON4,COD,DEN,FECINI,FECFIN,VIGENTE,NIVEL,IMPUTABLE) ' + @SQL" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@PRES5 NVARCHAR(20),@IDI NVARCHAR(20),@USU NVARCHAR(50),@UON1 AS VARCHAR(10),@UON2 AS VARCHAR(10),@UON3 AS VARCHAR(10),@UON4 AS VARCHAR(10),@VIGENTES TINYINT,@FECHAINICIODESDE DATETIME,@FECHAFINHASTA DATETIME,@TIPOFECHA INT', @PRES5=@PRES5,@IDI=@IDI,@USU=@USU,@UON1=@UON1,@UON2=@UON2,@UON3=@UON3,@UON4=@UON4,@VIGENTES=@VIGENTES,@FECHAINICIODESDE=@FECHAINICIODESDE,@FECHAFINHASTA=@FECHAFINHASTA,@TIPOFECHA=@TIPOFECHA" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "---- NIVEL 4 ----" & vbCrLf
sConsulta = sConsulta & "IF @NIVEL > 3" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SQL=N'SELECT U.UON1, U.UON2, U.UON3, U.UON4,P.PRES1 + ''|'' + P.PRES2 + ''|'' + P.PRES3 + ''|'' + P.COD, P.COD + '' - '' + I.DEN AS DEN, P5I.FECINI, P5I.FECFIN, case when P5I.FECINI<GETDATE() AND P5I.FECFIN>GETDATE() then 1 else 0 end as VIGENTE, 4 as NIVEL, 1 AS IMPUTABLE FROM PRES5_NIV4 P WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PRES5_IDIOMAS I WITH(NOLOCK) ON P.PRES0=I.PRES0 AND P.PRES1=I.PRES1 AND P.PRES2=I.PRES2 AND P.PRES3=I.PRES3 AND P.COD=I.PRES4 AND P.BAJALOG=0" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PRES5_UON U WITH(NOLOCK) ON P.PRES0=U.PRES0 AND P.PRES1=U.PRES1 AND P.PRES2=U.PRES2 AND P.PRES3=U.PRES3 AND P.COD=U.PRES4" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PRES5_IMPORTES P5I WITH(NOLOCK) ON P.PRES0=P5I.PRES0 AND P.PRES1=P5I.PRES1 AND P.PRES2=P5I.PRES2 AND P.PRES3=P5I.PRES3 AND P.COD=P5I.PRES4" & vbCrLf
sConsulta = sConsulta & "WHERE P.PRES0 = @PRES5 AND I.IDIOMA=@IDI ' + @SQLWHERE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL='INSERT INTO #PARTIDAS (UON1,UON2,UON3,UON4,COD,DEN,FECINI,FECFIN,VIGENTE,NIVEL,IMPUTABLE) ' + @SQL" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@PRES5 NVARCHAR(20),@IDI NVARCHAR(20),@USU NVARCHAR(50),@UON1 AS VARCHAR(10),@UON2 AS VARCHAR(10),@UON3 AS VARCHAR(10),@UON4 AS VARCHAR(10),@VIGENTES TINYINT,@FECHAINICIODESDE DATETIME,@FECHAFINHASTA DATETIME,@TIPOFECHA INT', @PRES5=@PRES5,@IDI=@IDI,@USU=@USU,@UON1=@UON1,@UON2=@UON2,@UON3=@UON3,@UON4=@UON4,@VIGENTES=@VIGENTES,@FECHAINICIODESDE=@FECHAINICIODESDE,@FECHAFINHASTA=@FECHAFINHASTA,@TIPOFECHA=@TIPOFECHA" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "----------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT '###' as COD0,0 as ID,DEN, NULL as centro_sm FROM PARGEN_LIT WITH(NOLOCK) WHERE IDI='spa' and id = 1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "select distinct '###' as COD0, u.cod as uon1, '' as uon2, '' as uon3, '' as uon4, u.cod, u.cod + ' - ' + u.den as den, su.centro_sm  from uon1 u with(nolock)" & vbCrLf
sConsulta = sConsulta & "left join centro_sm_uon su with(nolock) on u.cod=su.uon1 and su.uon2 is null and su.uon3 is null and su.uon4 is null" & vbCrLf
sConsulta = sConsulta & "inner join #PARTIDAS p with(nolock) on u.cod = p.uon1" & vbCrLf
sConsulta = sConsulta & "where u.bajalog=0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "select distinct '###' as COD0,u.uon1,u.cod as uon2, '' as uon3, '' as uon4, u.cod,u.cod + ' - ' + u.den as den, su.centro_sm, null as fecini, null as fecfin, null as vigente, null as imputable from uon2 u with(nolock)" & vbCrLf
sConsulta = sConsulta & "left join centro_sm_uon su with(nolock) on u.uon1=su.uon1 and u.cod=su.uon2 and su.uon3 is null and su.uon4 is null" & vbCrLf
sConsulta = sConsulta & "inner join #PARTIDAS p with(nolock) on u.cod = p.uon2 and u.uon1 =p.uon1" & vbCrLf
sConsulta = sConsulta & "where bajalog=0" & vbCrLf
sConsulta = sConsulta & "UNION" & vbCrLf
sConsulta = sConsulta & "select '###' as COD0,p.uon1,'' as uon2, '' as uon3, '' as uon4, p.cod,p.den as den, 'PARTIDA' as centro_sm, fecini, fecfin, vigente, imputable" & vbCrLf
sConsulta = sConsulta & "from #PARTIDAS p with(nolock)" & vbCrLf
sConsulta = sConsulta & "where nivel = 1 and p.uon1 is not null and p.uon2 is null and p.uon3 is null and p.uon4 is null" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "select distinct '###' as COD0,u.uon1,u.uon2,u.cod as uon3, '' as uon4, u.cod,u.cod + ' - ' + u.den as den, su.centro_sm, null as fecini, null as fecfin, null as vigente, null as imputable from uon3 u with(nolock)" & vbCrLf
sConsulta = sConsulta & "left join centro_sm_uon su with(nolock) on u.uon1=su.uon1 and u.uon2=su.uon2 and u.cod=su.uon3 and su.uon4 is null" & vbCrLf
sConsulta = sConsulta & "inner join #PARTIDAS p with(nolock) on u.cod=p.uon3 and u.uon2=p.uon2 and u.uon1 =p.uon1" & vbCrLf
sConsulta = sConsulta & "where bajalog=0" & vbCrLf
sConsulta = sConsulta & "UNION" & vbCrLf
sConsulta = sConsulta & "select '###' as COD0,p.uon1,p.uon2 as uon2, '' as uon3, '' as uon4, p.cod,p.den as den, 'PARTIDA' as centro_sm, fecini, fecfin, vigente, imputable" & vbCrLf
sConsulta = sConsulta & "from #PARTIDAS p with(nolock)" & vbCrLf
sConsulta = sConsulta & "where nivel = 1 and p.uon1 is not null and p.uon2 is not null and p.uon3 is null and p.uon4 is null" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "select distinct '###' as COD0,u.uon1,u.uon2,u.uon3,u.cod as uon4, u.cod,u.cod + ' - ' + u.den as den, su.centro_sm, null as fecini, null as fecfin, null as vigente, null as imputable from uon4 u with(nolock)" & vbCrLf
sConsulta = sConsulta & "left join centro_sm_uon su with(nolock) on u.uon1=su.uon1 and u.uon2=su.uon2 and u.uon3=su.uon3 and u.cod=su.uon4" & vbCrLf
sConsulta = sConsulta & "inner join #PARTIDAS p with(nolock) on u.cod=p.uon4 and u.uon3=p.uon3 and u.uon2=p.uon2 and u.uon1=p.uon1" & vbCrLf
sConsulta = sConsulta & "where bajalog=0" & vbCrLf
sConsulta = sConsulta & "UNION" & vbCrLf
sConsulta = sConsulta & "select '###' as COD0,p.uon1,p.uon2 as uon2, p.uon3 as uon3, p.uon4 as uon4, p.cod,p.den as den, 'PARTIDA' as centro_sm, fecini, fecfin, vigente, imputable" & vbCrLf
sConsulta = sConsulta & "from #PARTIDAS p with(nolock)" & vbCrLf
sConsulta = sConsulta & "where nivel = 1 and p.uon1 is not null and p.uon2 is not null and p.uon3 is not null and p.uon4 is null" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SELECT DISTINCT COD0,UON1,UON2,UON3, UON4, COD,DEN,FECINI, FECFIN, VIGENTE, CENTRO_SM, IMPUTABLE" & vbCrLf
sConsulta = sConsulta & "   FROM (" & vbCrLf
sConsulta = sConsulta & "       select distinct  '###' as COD0, uon1, isnull(uon2,'') as uon2, isnull(uon3,'') as uon3, isnull(uon4,'') as uon4, cod, den, fecini, fecfin, vigente, 'PARTIDA' as centro_sm, IMPUTABLE from #PARTIDAS with(nolock)" & vbCrLf
sConsulta = sConsulta & "       where nivel = 1 and uon1 is not null and uon2 is not null and uon3 is not null and uon4 is not null" & vbCrLf
sConsulta = sConsulta & "       UNION" & vbCrLf
sConsulta = sConsulta & "       select distinct  '###' as COD0, uon1, isnull(uon2,'') as uon2, isnull(uon3,'') as uon3, isnull(uon4,'') as uon4, cod, den, fecini, fecfin, vigente, 'PARTIDA' as centro_sm, IMPUTABLE from #PARTIDAS with(nolock) where nivel = 2" & vbCrLf
sConsulta = sConsulta & "       UNION" & vbCrLf
sConsulta = sConsulta & "       select distinct  '###' as COD0, uon1, isnull(uon2,'') as uon2, isnull(uon3,'') as uon3, isnull(uon4,'') as uon4, cod, den, fecini, fecfin, vigente, 'PARTIDA' as centro_sm, IMPUTABLE from #PARTIDAS with(nolock) where nivel = 3" & vbCrLf
sConsulta = sConsulta & "       UNION" & vbCrLf
sConsulta = sConsulta & "       select distinct  '###' as COD0, uon1, isnull(uon2,'') as uon2, isnull(uon3,'') as uon3, isnull(uon4,'') as uon4, cod, den, fecini, fecfin, vigente, 'PARTIDA' as centro_sm, IMPUTABLE from #PARTIDAS with(nolock) where nivel = 4   " & vbCrLf
sConsulta = sConsulta & "   ) A" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SELECT DISTINCT COD0,UON1,UON2,UON3, UON4, COD,DEN,FECINI, FECFIN, VIGENTE, CENTRO_SM, IMPUTABLE" & vbCrLf
sConsulta = sConsulta & "   FROM (" & vbCrLf
sConsulta = sConsulta & "       select distinct  '###' as COD0, uon1, isnull(uon2,'') as uon2, isnull(uon3,'') as uon3, isnull(uon4,'') as uon4, cod, den, fecini, fecfin, vigente, 'PARTIDA' as centro_sm, IMPUTABLE from #PARTIDAS with(nolock) where nivel = 2" & vbCrLf
sConsulta = sConsulta & "       UNION" & vbCrLf
sConsulta = sConsulta & "       select distinct  '###' as COD0, uon1, isnull(uon2,'') as uon2, isnull(uon3,'') as uon3, isnull(uon4,'') as uon4, cod, den, fecini, fecfin, vigente, 'PARTIDA' as centro_sm, IMPUTABLE from #PARTIDAS with(nolock) where nivel = 3       " & vbCrLf
sConsulta = sConsulta & "       UNION" & vbCrLf
sConsulta = sConsulta & "       select distinct  '###' as COD0, uon1, isnull(uon2,'') as uon2, isnull(uon3,'') as uon3, isnull(uon4,'') as uon4, cod, den, fecini, fecfin, vigente, 'PARTIDA' as centro_sm, IMPUTABLE from #PARTIDAS with(nolock) where nivel = 4       " & vbCrLf
sConsulta = sConsulta & "   ) A " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SELECT DISTINCT COD0,UON1,UON2,UON3, UON4, COD,DEN,FECINI, FECFIN, VIGENTE, CENTRO_SM, IMPUTABLE" & vbCrLf
sConsulta = sConsulta & "   FROM (      " & vbCrLf
sConsulta = sConsulta & "       select distinct  '###' as COD0, uon1, isnull(uon2,'') as uon2, isnull(uon3,'') as uon3, isnull(uon4,'') as uon4, cod, den, fecini, fecfin, vigente, 'PARTIDA' as centro_sm, IMPUTABLE from #PARTIDAS with(nolock) where nivel = 3" & vbCrLf
sConsulta = sConsulta & "       UNION" & vbCrLf
sConsulta = sConsulta & "       select distinct  '###' as COD0, uon1, isnull(uon2,'') as uon2, isnull(uon3,'') as uon3, isnull(uon4,'') as uon4, cod, den, fecini, fecfin, vigente, 'PARTIDA' as centro_sm, IMPUTABLE from #PARTIDAS with(nolock) where nivel = 4   " & vbCrLf
sConsulta = sConsulta & "   ) A" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   select distinct  '###' as COD0, uon1, isnull(uon2,'') as uon2, isnull(uon3,'') as uon3, isnull(uon4,'') as uon4, cod, den, fecini, fecfin, vigente, 'PARTIDA' as centro_sm, IMPUTABLE from #PARTIDAS with(nolock) where nivel = 4" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "---- NIVEL 1 ----" & vbCrLf
sConsulta = sConsulta & "SET @SQL=N'SELECT U.UON1, U.UON2, U.UON3, U.UON4,P.COD, P.COD + '' - '' + I.DEN AS DEN, P5I.FECINI, P5I.FECFIN, case when P5I.FECINI<GETDATE() AND P5I.FECFIN>GETDATE() then 1 else 0 end as VIGENTE, 1 as NIVEL '" & vbCrLf
sConsulta = sConsulta & "IF @NIVEL > 1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ',0 AS IMPUTABLE'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ',1 AS IMPUTABLE'" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ' FROM PRES5_NIV1 P WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PRES5_IDIOMAS I WITH(NOLOCK) ON P.PRES0=I.PRES0 AND P.COD=I.PRES1 AND I.PRES2 IS NULL AND I.PRES3 IS NULL AND I.PRES4 IS NULL AND P.BAJALOG=0" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PRES5_UON U WITH(NOLOCK) ON P.PRES0=U.PRES0 AND P.COD=U.PRES1 AND U.PRES2 IS NULL AND U.PRES3 IS NULL AND U.PRES4 IS NULL" & vbCrLf
sConsulta = sConsulta & "INNER JOIN USU_CC_IMPUTACION CI WITH(NOLOCK) ON U.UON1=CI.UON1 AND ISNULL(U.UON2,0)=ISNULL(CI.UON2,0) AND ISNULL(U.UON3,0)=ISNULL(CI.UON3,0) AND ISNULL(U.UON4,0)=ISNULL(CI.UON4,0)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PRES5_IMPORTES P5I WITH(NOLOCK) ON P.PRES0=P5I.PRES0 AND P.COD=P5I.PRES1 AND P5I.PRES2 IS NULL AND P5I.PRES3 IS NULL AND P5I.PRES4 IS NULL" & vbCrLf
sConsulta = sConsulta & "WHERE P.PRES0 = @PRES5 AND I.IDIOMA=@IDI AND CI.USU=@USU' + @SQLWHERE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL=N'INSERT INTO #PARTIDAS (UON1,UON2,UON3,UON4,COD,DEN,FECINI,FECFIN,VIGENTE,NIVEL,IMPUTABLE) ' + @SQL" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@PRES5 NVARCHAR(20),@IDI NVARCHAR(20),@USU NVARCHAR(50),@UON1 AS VARCHAR(10),@UON2 AS VARCHAR(10),@UON3 AS VARCHAR(10),@UON4 AS VARCHAR(10),@VIGENTES TINYINT,@FECHAINICIODESDE DATETIME,@FECHAFINHASTA DATETIME,@TIPOFECHA INT', @PRES5=@PRES5,@IDI=@IDI,@USU=@USU,@UON1=@UON1,@UON2=@UON2,@UON3=@UON3,@UON4=@UON4,@VIGENTES=@VIGENTES,@FECHAINICIODESDE=@FECHAINICIODESDE,@FECHAFINHASTA=@FECHAFINHASTA,@TIPOFECHA=@TIPOFECHA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "---- NIVEL 2 ----" & vbCrLf
sConsulta = sConsulta & "IF @NIVEL > 1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SQL=N'SELECT U.UON1, U.UON2, U.UON3, U.UON4,P.PRES1 + ''|'' + P.COD, P.COD + '' - '' + I.DEN AS DEN, P5I.FECINI, P5I.FECFIN, case when P5I.FECINI<GETDATE() AND P5I.FECFIN>GETDATE() then 1 else 0 end as VIGENTE, 2 as NIVEL'" & vbCrLf
sConsulta = sConsulta & "IF @NIVEL > 2" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ',0 AS IMPUTABLE'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ',1 AS IMPUTABLE'" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ' FROM PRES5_NIV2 P WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PRES5_IDIOMAS I WITH(NOLOCK) ON P.PRES0=I.PRES0 AND P.PRES1=I.PRES1 AND P.COD=I.PRES2 AND I.PRES3 IS NULL AND I.PRES4 IS NULL AND P.BAJALOG=0" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PRES5_UON U WITH(NOLOCK) ON P.PRES0=U.PRES0 AND P.PRES1=U.PRES1 AND P.COD=U.PRES2 AND U.PRES3 IS NULL AND U.PRES4 IS NULL" & vbCrLf
sConsulta = sConsulta & "INNER JOIN USU_CC_IMPUTACION CI WITH(NOLOCK) ON U.UON1=CI.UON1 AND ISNULL(U.UON2,0)=ISNULL(CI.UON2,0) AND ISNULL(U.UON3,0)=ISNULL(CI.UON3,0) AND ISNULL(U.UON4,0)=ISNULL(CI.UON4,0)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PRES5_IMPORTES P5I WITH(NOLOCK) ON P.PRES0=P5I.PRES0 AND P.PRES1=P5I.PRES1 AND P.COD=P5I.PRES2 AND P5I.PRES3 IS NULL AND P5I.PRES4 IS NULL" & vbCrLf
sConsulta = sConsulta & "WHERE P.PRES0 = @PRES5 AND I.IDIOMA=@IDI AND CI.USU=@USU' + @SQLWHERE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL='INSERT INTO #PARTIDAS (UON1,UON2,UON3,UON4,COD,DEN,FECINI,FECFIN,VIGENTE,NIVEL,IMPUTABLE) ' + @SQL" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@PRES5 NVARCHAR(20),@IDI NVARCHAR(20),@USU NVARCHAR(50),@UON1 AS VARCHAR(10),@UON2 AS VARCHAR(10),@UON3 AS VARCHAR(10),@UON4 AS VARCHAR(10),@VIGENTES TINYINT,@FECHAINICIODESDE DATETIME,@FECHAFINHASTA DATETIME,@TIPOFECHA INT', @PRES5=@PRES5,@IDI=@IDI,@USU=@USU,@UON1=@UON1,@UON2=@UON2,@UON3=@UON3,@UON4=@UON4,@VIGENTES=@VIGENTES,@FECHAINICIODESDE=@FECHAINICIODESDE,@FECHAFINHASTA=@FECHAFINHASTA,@TIPOFECHA=@TIPOFECHA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "---- NIVEL 3 ----" & vbCrLf
sConsulta = sConsulta & "IF @NIVEL > 2" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SQL=N'SELECT U.UON1, U.UON2, U.UON3, U.UON4,P.PRES1 + ''|'' + P.PRES2 + ''|'' + P.COD, P.COD + '' - '' + I.DEN AS DEN, P5I.FECINI, P5I.FECFIN, case when P5I.FECINI<GETDATE() AND P5I.FECFIN>GETDATE() then 1 else 0 end as VIGENTE, 3 as NIVEL'" & vbCrLf
sConsulta = sConsulta & "IF @NIVEL > 3" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ',0 AS IMPUTABLE'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ',1 AS IMPUTABLE'" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ' FROM PRES5_NIV3 P WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PRES5_IDIOMAS I WITH(NOLOCK) ON P.PRES0=I.PRES0 AND P.PRES1=I.PRES1 AND P.PRES2=I.PRES2 AND P.COD=I.PRES3 AND I.PRES4 IS NULL AND P.BAJALOG=0" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PRES5_UON U WITH(NOLOCK) ON P.PRES0=U.PRES0 AND P.PRES1=U.PRES1 AND P.PRES2=U.PRES2 AND P.COD=U.PRES3 AND U.PRES4 IS NULL" & vbCrLf
sConsulta = sConsulta & "INNER JOIN USU_CC_IMPUTACION CI WITH(NOLOCK) ON U.UON1=CI.UON1 AND ISNULL(U.UON2,0)=ISNULL(CI.UON2,0) AND ISNULL(U.UON3,0)=ISNULL(CI.UON3,0) AND ISNULL(U.UON4,0)=ISNULL(CI.UON4,0)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PRES5_IMPORTES P5I WITH(NOLOCK) ON P.PRES0=P5I.PRES0 AND P.PRES1=P5I.PRES1 AND P.PRES2=P5I.PRES2 AND P.COD=P5I.PRES3 AND P5I.PRES4 IS NULL" & vbCrLf
sConsulta = sConsulta & "WHERE P.PRES0 = @PRES5 AND I.IDIOMA=@IDI AND CI.USU=@USU' + @SQLWHERE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL='INSERT INTO #PARTIDAS (UON1,UON2,UON3,UON4,COD,DEN,FECINI,FECFIN,VIGENTE,NIVEL,IMPUTABLE) ' + @SQL" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@PRES5 NVARCHAR(20),@IDI NVARCHAR(20),@USU NVARCHAR(50),@UON1 AS VARCHAR(10),@UON2 AS VARCHAR(10),@UON3 AS VARCHAR(10),@UON4 AS VARCHAR(10),@VIGENTES TINYINT,@FECHAINICIODESDE DATETIME,@FECHAFINHASTA DATETIME,@TIPOFECHA INT', @PRES5=@PRES5,@IDI=@IDI,@USU=@USU,@UON1=@UON1,@UON2=@UON2,@UON3=@UON3,@UON4=@UON4,@VIGENTES=@VIGENTES,@FECHAINICIODESDE=@FECHAINICIODESDE,@FECHAFINHASTA=@FECHAFINHASTA,@TIPOFECHA=@TIPOFECHA" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "---- NIVEL 4 ----" & vbCrLf
sConsulta = sConsulta & "IF @NIVEL > 3" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SQL=N'SELECT U.UON1, U.UON2, U.UON3, U.UON4,P.PRES1 + ''|'' + P.PRES2 + ''|'' + P.PRES3 + ''|'' + P.COD, P.COD + '' - '' + I.DEN AS DEN, P5I.FECINI, P5I.FECFIN, case when P5I.FECINI<GETDATE() AND P5I.FECFIN>GETDATE() then 1 else 0 end as VIGENTE, 4 as NIVEL, 1 AS IMPUTABLE FROM PRES5_NIV4 P WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PRES5_IDIOMAS I WITH(NOLOCK) ON P.PRES0=I.PRES0 AND P.PRES1=I.PRES1 AND P.PRES2=I.PRES2 AND P.PRES3=I.PRES3 AND P.COD=I.PRES4 AND P.BAJALOG=0" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PRES5_UON U WITH(NOLOCK) ON P.PRES0=U.PRES0 AND P.PRES1=U.PRES1 AND P.PRES2=U.PRES2 AND P.PRES3=U.PRES3 AND P.COD=U.PRES4" & vbCrLf
sConsulta = sConsulta & "INNER JOIN USU_CC_IMPUTACION CI WITH(NOLOCK) ON U.UON1=CI.UON1 AND ISNULL(U.UON2,0)=ISNULL(CI.UON2,0) AND ISNULL(U.UON3,0)=ISNULL(CI.UON3,0) AND ISNULL(U.UON4,0)=ISNULL(CI.UON4,0)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PRES5_IMPORTES P5I WITH(NOLOCK) ON P.PRES0=P5I.PRES0 AND P.PRES1=P5I.PRES1 AND P.PRES2=P5I.PRES2 AND P.PRES3=P5I.PRES3 AND P.COD=P5I.PRES4" & vbCrLf
sConsulta = sConsulta & "WHERE P.PRES0 = @PRES5 AND I.IDIOMA=@IDI AND CI.USU=@USU' + @SQLWHERE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL='INSERT INTO #PARTIDAS (UON1,UON2,UON3,UON4,COD,DEN,FECINI,FECFIN,VIGENTE,NIVEL,IMPUTABLE) ' + @SQL" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@PRES5 NVARCHAR(20),@IDI NVARCHAR(20),@USU NVARCHAR(50),@UON1 AS VARCHAR(10),@UON2 AS VARCHAR(10),@UON3 AS VARCHAR(10),@UON4 AS VARCHAR(10),@VIGENTES TINYINT,@FECHAINICIODESDE DATETIME,@FECHAFINHASTA DATETIME,@TIPOFECHA INT', @PRES5=@PRES5,@IDI=@IDI,@USU=@USU,@UON1=@UON1,@UON2=@UON2,@UON3=@UON3,@UON4=@UON4,@VIGENTES=@VIGENTES,@FECHAINICIODESDE=@FECHAINICIODESDE,@FECHAFINHASTA=@FECHAFINHASTA,@TIPOFECHA=@TIPOFECHA" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "----------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT '###' as COD0,0 as ID,DEN, NULL as centro_sm FROM PARGEN_LIT WITH(NOLOCK) WHERE IDI='spa' and id = 1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "select distinct '###' as COD0, u.cod as uon1, '' as uon2, '' as uon3, '' as uon4, u.cod, u.cod + ' - ' + u.den as den, su.centro_sm  from uon1 u with(nolock)" & vbCrLf
sConsulta = sConsulta & "inner join usu_cc_imputacion c with(nolock) on u.cod=c.uon1" & vbCrLf
sConsulta = sConsulta & "left join centro_sm_uon su with(nolock) on u.cod=su.uon1 and su.uon2 is null and su.uon3 is null and su.uon4 is null" & vbCrLf
sConsulta = sConsulta & "inner join #PARTIDAS p with(nolock) on u.cod = p.uon1" & vbCrLf
sConsulta = sConsulta & "where u.bajalog=0 and c.usu = @usu" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "select distinct '###' as COD0,u.uon1,u.cod as uon2, '' as uon3, '' as uon4, u.cod,u.cod + ' - ' + u.den as den, su.centro_sm, null as fecini, null as fecfin, null as vigente, null as imputable from uon2 u with(nolock)" & vbCrLf
sConsulta = sConsulta & "inner join usu_cc_imputacion c with(nolock) on u.cod=c.uon2 and u.uon1=c.uon1" & vbCrLf
sConsulta = sConsulta & "left join centro_sm_uon su with(nolock) on u.uon1=su.uon1 and u.cod=su.uon2 and su.uon3 is null and su.uon4 is null" & vbCrLf
sConsulta = sConsulta & "inner join #PARTIDAS p with(nolock) on u.cod = p.uon2 and u.uon1 =p.uon1" & vbCrLf
sConsulta = sConsulta & "where bajalog=0 and c.usu = @usu" & vbCrLf
sConsulta = sConsulta & "UNION" & vbCrLf
sConsulta = sConsulta & "select '###' as COD0,p.uon1,'' as uon2, '' as uon3, '' as uon4, p.cod,p.den as den, 'PARTIDA' as centro_sm, fecini, fecfin, vigente, imputable" & vbCrLf
sConsulta = sConsulta & "from #PARTIDAS p with(nolock)" & vbCrLf
sConsulta = sConsulta & "where nivel = 1 and p.uon1 is not null and p.uon2 is null and p.uon3 is null and p.uon4 is null" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "select distinct '###' as COD0,u.uon1,u.uon2,u.cod as uon3, '' as uon4, u.cod,u.cod + ' - ' + u.den as den, su.centro_sm, null as fecini, null as fecfin, null as vigente, null as imputable from uon3 u with(nolock)" & vbCrLf
sConsulta = sConsulta & "inner join usu_cc_imputacion c with(nolock) on u.cod=c.uon3 and u.uon2=c.uon2 and u.uon1=c.uon1" & vbCrLf
sConsulta = sConsulta & "left join centro_sm_uon su with(nolock) on u.uon1=su.uon1 and u.uon2=su.uon2 and u.cod=su.uon3 and su.uon4 is null" & vbCrLf
sConsulta = sConsulta & "inner join #PARTIDAS p with(nolock) on u.cod=p.uon3 and u.uon2=p.uon2 and u.uon1 =p.uon1" & vbCrLf
sConsulta = sConsulta & "where bajalog=0 and c.usu = @usu" & vbCrLf
sConsulta = sConsulta & "UNION" & vbCrLf
sConsulta = sConsulta & "select '###' as COD0,p.uon1,p.uon2 as uon2, '' as uon3, '' as uon4, p.cod,p.den as den, 'PARTIDA' as centro_sm, fecini, fecfin, vigente, imputable" & vbCrLf
sConsulta = sConsulta & "from #PARTIDAS p with(nolock)" & vbCrLf
sConsulta = sConsulta & "where nivel = 1 and p.uon1 is not null and p.uon2 is not null and p.uon3 is null and p.uon4 is null" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "select distinct '###' as COD0,u.uon1,u.uon2,u.uon3,u.cod as uon4, u.cod,u.cod + ' - ' + u.den as den, su.centro_sm, null as fecini, null as fecfin, null as vigente, null as imputable from uon4 u with(nolock)" & vbCrLf
sConsulta = sConsulta & "inner join usu_cc_imputacion c with(nolock) on u.cod=c.uon4 and u.uon3=c.uon3 and u.uon2=c.uon2 and u.uon1=c.uon1" & vbCrLf
sConsulta = sConsulta & "left join centro_sm_uon su with(nolock) on u.uon1=su.uon1 and u.uon2=su.uon2 and u.uon3=su.uon3 and u.cod=su.uon4" & vbCrLf
sConsulta = sConsulta & "inner join #PARTIDAS p with(nolock) on u.cod=p.uon4 and u.uon3=p.uon3 and u.uon2=p.uon2 and u.uon1=p.uon1" & vbCrLf
sConsulta = sConsulta & "where bajalog=0 and c.usu = @usu" & vbCrLf
sConsulta = sConsulta & "UNION" & vbCrLf
sConsulta = sConsulta & "select '###' as COD0,p.uon1,p.uon2 as uon2, p.uon3 as uon3, p.uon4 as uon4, p.cod,p.den as den, 'PARTIDA' as centro_sm, fecini, fecfin, vigente, imputable" & vbCrLf
sConsulta = sConsulta & "from #PARTIDAS p with(nolock)" & vbCrLf
sConsulta = sConsulta & "where nivel = 1 and p.uon1 is not null and p.uon2 is not null and p.uon3 is not null and p.uon4 is null" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SELECT DISTINCT COD0,UON1,UON2,UON3, UON4, COD,DEN,FECINI, FECFIN, VIGENTE, CENTRO_SM, IMPUTABLE" & vbCrLf
sConsulta = sConsulta & "   FROM (" & vbCrLf
sConsulta = sConsulta & "       select distinct  '###' as COD0, uon1, isnull(uon2,'') as uon2, isnull(uon3,'') as uon3, isnull(uon4,'') as uon4, cod, den, fecini, fecfin, vigente, 'PARTIDA' as centro_sm, IMPUTABLE from #PARTIDAS with(nolock)" & vbCrLf
sConsulta = sConsulta & "       where nivel = 1 and uon1 is not null and uon2 is not null and uon3 is not null and uon4 is not null" & vbCrLf
sConsulta = sConsulta & "       UNION" & vbCrLf
sConsulta = sConsulta & "       select distinct  '###' as COD0, uon1, isnull(uon2,'') as uon2, isnull(uon3,'') as uon3, isnull(uon4,'') as uon4, cod, den, fecini, fecfin, vigente, 'PARTIDA' as centro_sm, IMPUTABLE from #PARTIDAS with(nolock) where nivel = 2" & vbCrLf
sConsulta = sConsulta & "       UNION" & vbCrLf
sConsulta = sConsulta & "       select distinct  '###' as COD0, uon1, isnull(uon2,'') as uon2, isnull(uon3,'') as uon3, isnull(uon4,'') as uon4, cod, den, fecini, fecfin, vigente, 'PARTIDA' as centro_sm, IMPUTABLE from #PARTIDAS with(nolock) where nivel = 3" & vbCrLf
sConsulta = sConsulta & "       UNION" & vbCrLf
sConsulta = sConsulta & "       select distinct  '###' as COD0, uon1, isnull(uon2,'') as uon2, isnull(uon3,'') as uon3, isnull(uon4,'') as uon4, cod, den, fecini, fecfin, vigente, 'PARTIDA' as centro_sm, IMPUTABLE from #PARTIDAS with(nolock) where nivel = 4   " & vbCrLf
sConsulta = sConsulta & "   ) A     " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SELECT DISTINCT COD0,UON1,UON2,UON3, UON4, COD,DEN,FECINI, FECFIN, VIGENTE, CENTRO_SM, IMPUTABLE" & vbCrLf
sConsulta = sConsulta & "   FROM (" & vbCrLf
sConsulta = sConsulta & "       select distinct  '###' as COD0, uon1, isnull(uon2,'') as uon2, isnull(uon3,'') as uon3, isnull(uon4,'') as uon4, cod, den, fecini, fecfin, vigente, 'PARTIDA' as centro_sm, IMPUTABLE from #PARTIDAS with(nolock) where nivel = 2" & vbCrLf
sConsulta = sConsulta & "       UNION" & vbCrLf
sConsulta = sConsulta & "       select distinct  '###' as COD0, uon1, isnull(uon2,'') as uon2, isnull(uon3,'') as uon3, isnull(uon4,'') as uon4, cod, den, fecini, fecfin, vigente, 'PARTIDA' as centro_sm, IMPUTABLE from #PARTIDAS with(nolock) where nivel = 3" & vbCrLf
sConsulta = sConsulta & "       UNION" & vbCrLf
sConsulta = sConsulta & "       select distinct  '###' as COD0, uon1, isnull(uon2,'') as uon2, isnull(uon3,'') as uon3, isnull(uon4,'') as uon4, cod, den, fecini, fecfin, vigente, 'PARTIDA' as centro_sm, IMPUTABLE from #PARTIDAS with(nolock) where nivel = 4   " & vbCrLf
sConsulta = sConsulta & "   ) A" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   SELECT DISTINCT COD0,UON1,UON2,UON3, UON4, COD,DEN,FECINI, FECFIN, VIGENTE, CENTRO_SM, IMPUTABLE" & vbCrLf
sConsulta = sConsulta & "   FROM (      " & vbCrLf
sConsulta = sConsulta & "       select distinct  '###' as COD0, uon1, isnull(uon2,'') as uon2, isnull(uon3,'') as uon3, isnull(uon4,'') as uon4, cod, den, fecini, fecfin, vigente, 'PARTIDA' as centro_sm, IMPUTABLE from #PARTIDAS with(nolock) where nivel = 3" & vbCrLf
sConsulta = sConsulta & "       UNION" & vbCrLf
sConsulta = sConsulta & "       select distinct  '###' as COD0, uon1, isnull(uon2,'') as uon2, isnull(uon3,'') as uon3, isnull(uon4,'') as uon4, cod, den, fecini, fecfin, vigente, 'PARTIDA' as centro_sm, IMPUTABLE from #PARTIDAS with(nolock) where nivel = 4   " & vbCrLf
sConsulta = sConsulta & "   ) A" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   select distinct  '###' as COD0, uon1, isnull(uon2,'') as uon2, isnull(uon3,'') as uon3, isnull(uon4,'') as uon4, cod, den, fecini, fecfin, vigente, 'PARTIDA' as centro_sm, IMPUTABLE from #PARTIDAS with(nolock) where nivel = 4" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DROP TABLE #PARTIDAS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSGS_DESGLOSE_IMPUESTOS_ORDEN]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSGS_DESGLOSE_IMPUESTOS_ORDEN]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSGS_DESGLOSE_IMPUESTOS_ORDEN]" & vbCrLf
sConsulta = sConsulta & "   @IDORDEN INT," & vbCrLf
sConsulta = sConsulta & "   @IDIOMA varchar(20)" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       DECLARE @NETO FLOAT" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "       --CALCULO DE IMPORTE NETO DE LINEA" & vbCrLf
sConsulta = sConsulta & "       SELECT @NETO=SUM(NETO.IMPORTE) FROM(" & vbCrLf
sConsulta = sConsulta & "           SELECT isnull(PREC_UP*CANT_PED,0) IMPORTE" & vbCrLf
sConsulta = sConsulta & "           FROM LINEAS_PEDIDO LP WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "           WHERE ORDEN=@IDORDEN" & vbCrLf
sConsulta = sConsulta & "           UNION" & vbCrLf
sConsulta = sConsulta & "           SELECT CASE WHEN TIPO = 0 THEN IMPORTE ELSE -IMPORTE END IMPORTE" & vbCrLf
sConsulta = sConsulta & "           FROM LINEAS_PEDIDO_CD LPCD WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN LINEAS_PEDIDO LP WITH(NOLOCK) ON LP.ID=LPCD.LINEA" & vbCrLf
sConsulta = sConsulta & "           WHERE ORDEN=@IDORDEN" & vbCrLf
sConsulta = sConsulta & "       )NETO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       SELECT SUM(BI) BI,TIPO,DEN" & vbCrLf
sConsulta = sConsulta & "       FROM(" & vbCrLf
sConsulta = sConsulta & "           --linea" & vbCrLf
sConsulta = sConsulta & "           SELECT CASE WHEN LP.TIPORECEPCION=0 THEN PREC_UP*CANT_PED ELSE LP.IMPORTE_PED END BI,ID.DEN, LPI.VALOR TIPO" & vbCrLf
sConsulta = sConsulta & "           FROM LINEAS_PEDIDO LP WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN LINEAS_PEDIDO_IMPUESTO LPI WITH(NOLOCK) ON LPI.LINEA =LP.ID" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN IMPUESTO_PP_VALOR IMPPP WITH(NOLOCK) ON IMPPP.ID= LPI.IMPUESTO" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN IMPUESTO_PAIPROVI IMPPAI WITH(NOLOCK) ON IMPPP.IMPUESTO_PAIPROVI = IMPPAI.ID" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN IMPUESTO I WITH(NOLOCK) ON IMPPAI.IMPUESTO = I.ID" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN IMPUESTO_DEN ID WITH(NOLOCK) ON ID.IMPUESTO =I.ID" & vbCrLf
sConsulta = sConsulta & "           WHERE LP.Orden = @IDORDEN AND ID.IDIOMA =@IDIOMA" & vbCrLf
sConsulta = sConsulta & "           UNION" & vbCrLf
sConsulta = sConsulta & "           --coste de linea + y +%" & vbCrLf
sConsulta = sConsulta & "           SELECT LPCD.IMPORTE  AS BI, ID.DEN,LPCDI.VALOR TIPO" & vbCrLf
sConsulta = sConsulta & "           FROM LINEAS_PEDIDO_CD LPCD WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN LINEAS_PEDIDO LP WITH(NOLOCK) ON LP.ID = LPCD.LINEA" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN LINEAS_PEDIDO_CD_IMPUESTO LPCDI WITH(NOLOCK) ON LPCD.LINEA =LPCDI.LINEA AND LPCD.ID=LPCDI.COSTE" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN IMPUESTO_PP_VALOR IMPPP WITH(NOLOCK) ON IMPPP.ID= LPCDI.IMPUESTO" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN IMPUESTO_PAIPROVI IMPPAI WITH(NOLOCK) ON IMPPP.IMPUESTO_PAIPROVI = IMPPAI.ID" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN IMPUESTO I WITH(NOLOCK) ON IMPPAI.IMPUESTO = I.ID" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN IMPUESTO_DEN ID WITH(NOLOCK) ON ID.IMPUESTO =I.ID" & vbCrLf
sConsulta = sConsulta & "           WHERE LP.ORDEN = @IDORDEN AND TIPO = 0 AND ID.IDIOMA =@IDIOMA" & vbCrLf
sConsulta = sConsulta & "           UNION" & vbCrLf
sConsulta = sConsulta & "           --Descuento linea- y -%" & vbCrLf
sConsulta = sConsulta & "           SELECT -LPCD.IMPORTE AS BI, ID.DEN,LPI.VALOR TIPO" & vbCrLf
sConsulta = sConsulta & "           FROM LINEAS_PEDIDO_CD LPCD WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN LINEAS_PEDIDO LP WITH(NOLOCK) ON LP.ID = LPCD.LINEA" & vbCrLf
sConsulta = sConsulta & "           LEFT JOIN LINEAS_PEDIDO_IMPUESTO LPI WITH(NOLOCK) ON LPI.LINEA =LP.ID" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN IMPUESTO_PP_VALOR IMPPP WITH(NOLOCK) ON IMPPP.ID= LPI.IMPUESTO" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN IMPUESTO_PAIPROVI IMPPAI WITH(NOLOCK) ON IMPPP.IMPUESTO_PAIPROVI = IMPPAI.ID" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN IMPUESTO I WITH(NOLOCK) ON IMPPAI.IMPUESTO = I.ID" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN IMPUESTO_DEN ID WITH(NOLOCK) ON ID.IMPUESTO =I.ID" & vbCrLf
sConsulta = sConsulta & "           WHERE LP.ORDEN = @IDORDEN AND TIPO = 1 AND ID.IDIOMA =@IDIOMA" & vbCrLf
sConsulta = sConsulta & "           UNION" & vbCrLf
sConsulta = sConsulta & "           --coste general +" & vbCrLf
sConsulta = sConsulta & "           SELECT OECD.IMPORTE BI,ID.DEN, OECDI.VALOR TIPO" & vbCrLf
sConsulta = sConsulta & "           FROM ORDEN_ENTREGA OE WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN ORDEN_ENTREGA_CD OECD WITH(NOLOCK) ON OE.ID=OECD.ORDEN" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN ORDEN_ENTREGA_CD_IMPUESTO OECDI WITH(NOLOCK) ON OECDI.ORDEN = OE.ID AND OECDI.COSTE=OECD.ID" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN IMPUESTO_PP_VALOR IMPPP WITH(NOLOCK) ON IMPPP.ID= OECDI.IMPUESTO" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN IMPUESTO_PAIPROVI IMPPAI WITH(NOLOCK) ON IMPPP.IMPUESTO_PAIPROVI = IMPPAI.ID" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN IMPUESTO I WITH(NOLOCK) ON IMPPAI.IMPUESTO = I.ID" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN IMPUESTO_DEN ID WITH(NOLOCK) ON ID.IMPUESTO =I.ID" & vbCrLf
sConsulta = sConsulta & "           WHERE OE.ID = @IDORDEN AND OECD.TIPO = 0 AND ID.IDIOMA =@IDIOMA" & vbCrLf
sConsulta = sConsulta & "           UNION" & vbCrLf
sConsulta = sConsulta & "           ---DESCUENTO GENERAL % SOBRE IMPORTE DE LINEA" & vbCrLf
sConsulta = sConsulta & "           SELECT CASE LP.TIPORECEPCION WHEN 0 THEN -(LP.PREC_UP*LP.CANT_PED)*(OECD.VALOR/100) ELSE -LP.IMPORTE_PED*(OECD.VALOR/100)END  BI,ID.DEN," & vbCrLf
sConsulta = sConsulta & "                       LPI.VALOR TIPO" & vbCrLf
sConsulta = sConsulta & "           FROM LINEAS_PEDIDO LP WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN  ORDEN_ENTREGA OE WITH(NOLOCK) ON LP.ORDEN =OE.ID" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN LINEAS_PEDIDO_IMPUESTO LPI WITH(NOLOCK) ON LPI.LINEA =LP.ID" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN ORDEN_ENTREGA_CD OECD WITH(NOLOCK) ON OE.ID=OECD.ORDEN" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN IMPUESTO_PP_VALOR IMPPP WITH(NOLOCK) ON IMPPP.ID= LPI.IMPUESTO" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN IMPUESTO_PAIPROVI IMPPAI WITH(NOLOCK) ON IMPPP.IMPUESTO_PAIPROVI = IMPPAI.ID" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN IMPUESTO I WITH(NOLOCK) ON IMPPAI.IMPUESTO = I.ID" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN IMPUESTO_DEN ID WITH(NOLOCK) ON ID.IMPUESTO =I.ID" & vbCrLf
sConsulta = sConsulta & "           WHERE OE.ID = @IDORDEN AND OECD.TIPO = 1 AND OECD.OPERACION =1 AND ID.IDIOMA =@IDIOMA" & vbCrLf
sConsulta = sConsulta & "           UNION" & vbCrLf
sConsulta = sConsulta & "           ---DESCUENTO GENERAL % SOBRE COSTES" & vbCrLf
sConsulta = sConsulta & "           SELECT -LPC.IMPORTE*(OECD.VALOR/100) BI,ID.DEN, LPI.VALOR TIPO" & vbCrLf
sConsulta = sConsulta & "           FROM LINEAS_PEDIDO LP WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN  ORDEN_ENTREGA OE WITH(NOLOCK) ON LP.ORDEN =OE.ID" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN LINEAS_PEDIDO_IMPUESTO LPI WITH(NOLOCK) ON LPI.LINEA =LP.ID" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN LINEAS_PEDIDO_CD LPC ON LPC.LINEA =LPI.LINEA AND LPC.TIPO=0" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN ORDEN_ENTREGA_CD OECD WITH(NOLOCK) ON OE.ID=OECD.ORDEN" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN IMPUESTO_PP_VALOR IMPPP WITH(NOLOCK) ON IMPPP.ID= LPI.IMPUESTO" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN IMPUESTO_PAIPROVI IMPPAI WITH(NOLOCK) ON IMPPP.IMPUESTO_PAIPROVI = IMPPAI.ID" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN IMPUESTO I WITH(NOLOCK) ON IMPPAI.IMPUESTO = I.ID" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN IMPUESTO_DEN ID WITH(NOLOCK) ON ID.IMPUESTO =I.ID" & vbCrLf
sConsulta = sConsulta & "           WHERE OE.ID = @IDORDEN AND OECD.TIPO = 1 AND OECD.OPERACION =1 AND ID.IDIOMA =@IDIOMA" & vbCrLf
sConsulta = sConsulta & "           UNION" & vbCrLf
sConsulta = sConsulta & "           ---descuento general % sobre descuentos" & vbCrLf
sConsulta = sConsulta & "           SELECT LPD.IMPORTE*(OECD.VALOR/100) BI,ID.DEN,LPI.VALOR TIPO" & vbCrLf
sConsulta = sConsulta & "           FROM LINEAS_PEDIDO LP WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN  ORDEN_ENTREGA OE WITH(NOLOCK) ON LP.ORDEN =OE.ID" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN LINEAS_PEDIDO_IMPUESTO LPI WITH(NOLOCK) ON LPI.LINEA =LP.ID" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN LINEAS_PEDIDO_CD LPD ON LPD.LINEA = LPI.LINEA AND LPD.TIPO=1" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN ORDEN_ENTREGA_CD OECD WITH(NOLOCK) ON OE.ID=OECD.ORDEN" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN IMPUESTO_PP_VALOR IMPPP WITH(NOLOCK) ON IMPPP.ID= LPI.IMPUESTO" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN IMPUESTO_PAIPROVI IMPPAI WITH(NOLOCK) ON IMPPP.IMPUESTO_PAIPROVI = IMPPAI.ID" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN IMPUESTO I WITH(NOLOCK) ON IMPPAI.IMPUESTO = I.ID" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN IMPUESTO_DEN ID WITH(NOLOCK) ON ID.IMPUESTO =I.ID" & vbCrLf
sConsulta = sConsulta & "           WHERE OE.ID = @IDORDEN AND OECD.TIPO = 1 AND OECD.OPERACION =1 AND ID.IDIOMA =@IDIOMA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "           UNION" & vbCrLf
sConsulta = sConsulta & "           --DESCUENTO GENERAL IMPORTE, PRORRATEADO EN COSTES DE LINEA" & vbCrLf
sConsulta = sConsulta & "           SELECT -oecd.importe*NETOLINEA.IMPORTE/@NETO BI,ID.DEN," & vbCrLf
sConsulta = sConsulta & "                       LPI.VALOR TIPO" & vbCrLf
sConsulta = sConsulta & "           FROM LINEAS_PEDIDO LP WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN  ORDEN_ENTREGA OE WITH(NOLOCK) ON LP.ORDEN =OE.ID" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN LINEAS_PEDIDO_IMPUESTO LPI WITH(NOLOCK) ON LPI.LINEA =LP.ID" & vbCrLf
sConsulta = sConsulta & "           LEFT JOIN LINEAS_PEDIDO_CD LPD ON LPD.LINEA = LPI.LINEA AND LPD.TIPO=1" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN ORDEN_ENTREGA_CD OECD WITH(NOLOCK) ON OE.ID=OECD.ORDEN" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN IMPUESTO_PP_VALOR IMPPP WITH(NOLOCK) ON IMPPP.ID= LPI.IMPUESTO" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN IMPUESTO_PAIPROVI IMPPAI WITH(NOLOCK) ON IMPPP.IMPUESTO_PAIPROVI = IMPPAI.ID" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN IMPUESTO I WITH(NOLOCK) ON IMPPAI.IMPUESTO = I.ID" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN IMPUESTO_DEN ID WITH(NOLOCK) ON ID.IMPUESTO =I.ID" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN(" & vbCrLf
sConsulta = sConsulta & "               SELECT LP.ID LINEA,isnull(PREC_UP*CANT_PED,0) IMPORTE" & vbCrLf
sConsulta = sConsulta & "               FROM LINEAS_PEDIDO LP WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "               WHERE ORDEN=@IDORDEN" & vbCrLf
sConsulta = sConsulta & "               UNION" & vbCrLf
sConsulta = sConsulta & "               SELECT LP.ID LINEA, CASE WHEN TIPO = 0 THEN IMPORTE ELSE -IMPORTE END IMPORTE" & vbCrLf
sConsulta = sConsulta & "               FROM LINEAS_PEDIDO_CD LPCD WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "               INNER JOIN LINEAS_PEDIDO LP WITH(NOLOCK) ON LP.ID=LPCD.LINEA" & vbCrLf
sConsulta = sConsulta & "               WHERE ORDEN=@IDORDEN" & vbCrLf
sConsulta = sConsulta & "           )NETOLINEA ON NETOLINEA.LINEA=LP.ID" & vbCrLf
sConsulta = sConsulta & "           WHERE OE.ID = @IDORDEN AND OECD.TIPO = 1 AND OECD.OPERACION =0 AND ID.IDIOMA =@IDIOMA" & vbCrLf
sConsulta = sConsulta & "       ) DESGLOSE" & vbCrLf
sConsulta = sConsulta & "       GROUP BY DESGLOSE.DEN,DESGLOSE.TIPO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSP_DEVOLVER_IMPUESTOS_CD_LINEA]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSP_DEVOLVER_IMPUESTOS_CD_LINEA]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSP_DEVOLVER_IMPUESTOS_CD_LINEA](" & vbCrLf
sConsulta = sConsulta & "@LINEA INT," & vbCrLf
sConsulta = sConsulta & "@IDIOMA VARCHAR(3)" & vbCrLf
sConsulta = sConsulta & ")  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT OE.ANYO,OE.NUM CESTA,OE.PEDIDO,LP.NUM,LP.ART_INT CODART , DA.COD CODCOSTE, DA.DESCR DENCOSTE,LPCD.TIPO,CONVERT(VARCHAR,LPCD.OPERACION) OPERACION,ID.DEN IMPUESTO,LPCDI.VALOR" & vbCrLf
sConsulta = sConsulta & "FROM LINEAS_PEDIDO LP WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN ORDEN_ENTREGA OE WITH(NOLOCK) ON OE.ID=LP.ORDEN" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN LINEAS_PEDIDO_CD LPCD WITH(NOLOCK) ON LPCD.LINEA = LP.ID" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN LINEAS_PEDIDO_CD_IMPUESTO LPCDI WITH(NOLOCK) ON LPCDI.LINEA = LP.ID AND LPCD.ID=LPCDI.COSTE" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN IMPUESTO_PP_VALOR IMPPP WITH(NOLOCK) ON IMPPP.ID=LPCDI.IMPUESTO" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN IMPUESTO_PAIPROVI IMPPAI WITH(NOLOCK) ON IMPPP.IMPUESTO_PAIPROVI = IMPPAI.ID" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN IMPUESTO I WITH(NOLOCK) ON IMPPAI.IMPUESTO = I.ID" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN IMPUESTO_DEN ID WITH(NOLOCK)ON ID.IMPUESTO =I.ID" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN DEF_ATRIB DA WITH(NOLOCK) ON DA.ID=LPCD.ATRIB" & vbCrLf
sConsulta = sConsulta & "WHERE LPCDI.LINEA=@LINEA AND IDIOMA=@IDIOMA" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSP_DEVOLVER_IMPUESTOS_CD_ORDEN]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSP_DEVOLVER_IMPUESTOS_CD_ORDEN]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSP_DEVOLVER_IMPUESTOS_CD_ORDEN](" & vbCrLf
sConsulta = sConsulta & "   @ORDEN INT," & vbCrLf
sConsulta = sConsulta & "   @IDIOMA VARCHAR(3)" & vbCrLf
sConsulta = sConsulta & ")  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT OE.ANYO,OE.NUM CESTA,OE.PEDIDO,DA.COD  CODCOSTE,DA.DESCR DENCOSTE, ID.DEN IMPUESTO,OECDI.VALOR" & vbCrLf
sConsulta = sConsulta & "FROM ORDEN_ENTREGA_CD OECD WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN ORDEN_ENTREGA OE WITH(NOLOCK) ON OECD.ORDEN = OE.ID" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN ORDEN_ENTREGA_CD_IMPUESTO OECDI WITH(NOLOCK) ON  OECDI.ORDEN=OE.ID AND OECD.ID=OECDI.COSTE" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN IMPUESTO_PP_VALOR IMPPP WITH(NOLOCK) ON IMPPP.ID=OECDI.IMPUESTO" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN IMPUESTO_PAIPROVI IMPPAI WITH(NOLOCK) ON IMPPP.IMPUESTO_PAIPROVI = IMPPAI.ID" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN IMPUESTO I WITH(NOLOCK) ON IMPPAI.IMPUESTO = I.ID" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN IMPUESTO_DEN ID WITH(NOLOCK)ON I.ID=ID.IMPUESTO" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN DEF_ATRIB DA WITH(NOLOCK)ON DA.ID=OECD.ATRIB" & vbCrLf
sConsulta = sConsulta & "WHERE OECDI.ORDEN=@ORDEN AND IDIOMA=@IDIOMA" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSP_DEVOLVER_IMPUESTOS_LINEA]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSP_DEVOLVER_IMPUESTOS_LINEA]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSP_DEVOLVER_IMPUESTOS_LINEA](" & vbCrLf
sConsulta = sConsulta & "@LINEA INT," & vbCrLf
sConsulta = sConsulta & "@IDIOMA VARCHAR(3)" & vbCrLf
sConsulta = sConsulta & ")  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT OE.ANYO,OE.NUM CESTA,OE.PEDIDO,LP.NUM,LP.ART_INT CODART,ID.DEN IMPUESTO,LPI.VALOR" & vbCrLf
sConsulta = sConsulta & "FROM LINEAS_PEDIDO LP WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN ORDEN_ENTREGA OE WITH(NOLOCK) ON OE.ID=LP.ORDEN" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN LINEAS_PEDIDO_IMPUESTO LPI WITH(NOLOCK) ON LPI.LINEA = LP.ID" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN IMPUESTO_PP_VALOR IMPPP WITH(NOLOCK) ON IMPPP.ID=LPI.IMPUESTO" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN IMPUESTO_PAIPROVI IMPPAI WITH(NOLOCK) ON IMPPP.IMPUESTO_PAIPROVI = IMPPAI.ID" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN IMPUESTO I WITH(NOLOCK) ON IMPPAI.IMPUESTO = I.ID" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN IMPUESTO_DEN ID WITH(NOLOCK)ON ID.IMPUESTO =I.ID" & vbCrLf
sConsulta = sConsulta & "WHERE LPI.LINEA=@LINEA AND IDIOMA=@IDIOMA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSEP_REEMITIR_ORDEN]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSEP_REEMITIR_ORDEN]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSEP_REEMITIR_ORDEN] @ORDEN INT, @APROVISIONADOR varchar(50) AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @PEDIDO int" & vbCrLf
sConsulta = sConsulta & "SELECT @PEDIDO = PEDIDO  FROM ORDEN_ENTREGA WITH (NOLOCK) WHERE ID = @ORDEN" & vbCrLf
sConsulta = sConsulta & "DELETE FROM LINEAS_EST   FROM LINEAS_EST LE    INNER JOIN LINEAS_PEDIDO LP WITH (NOLOCK)  ON LE.LINEA = LP.ID WHERE LP.ORDEN = @ORDEN   AND LP.EST = 20" & vbCrLf
sConsulta = sConsulta & "DELETE FROM LINEAS_PEDIDO_ADJUN WHERE LINEA IN (SELECT ID FROM LINEAS_PEDIDO WITH (NOLOCK) WHERE ORDEN = @ORDEN AND EST = 20)" & vbCrLf
sConsulta = sConsulta & "DELETE FROM LINEAS_PED_IMPUTACION WHERE LINEA IN (SELECT ID FROM LINEAS_PEDIDO WITH (NOLOCK) WHERE ORDEN = @ORDEN AND EST = 20)" & vbCrLf
sConsulta = sConsulta & "DELETE FROM LINEAS_PEDIDO_ATRIB_LISTA WHERE LINEA IN (SELECT ID FROM LINEAS_PEDIDO WITH (NOLOCK) WHERE ORDEN = @ORDEN AND EST = 20)" & vbCrLf
sConsulta = sConsulta & "DELETE FROM LINEAS_PEDIDO_ATRIB WHERE LINEA IN (SELECT ID FROM LINEAS_PEDIDO WITH (NOLOCK) WHERE ORDEN = @ORDEN AND EST = 20)" & vbCrLf
sConsulta = sConsulta & "DELETE FROM LINEAS_PEDIDO_CD_IMPUESTO WHERE LINEA IN (SELECT ID FROM LINEAS_PEDIDO WITH (NOLOCK) WHERE ORDEN = @ORDEN AND EST = 20)" & vbCrLf
sConsulta = sConsulta & "DELETE FROM LINEAS_PEDIDO_CD_LISTA FROM LINEAS_PEDIDO_CD_LISTA LPCDL" & vbCrLf
sConsulta = sConsulta & "                               INNER JOIN LINEAS_PEDIDO_CD LPCD WITH (NOLOCK)  ON LPCDL.ID = LPCD.ID" & vbCrLf
sConsulta = sConsulta & "                               WHERE LPCD.LINEA IN (SELECT ID FROM LINEAS_PEDIDO WITH (NOLOCK) WHERE ORDEN = @ORDEN AND EST = 20)" & vbCrLf
sConsulta = sConsulta & "DELETE FROM LINEAS_PEDIDO_CD WHERE LINEA IN (SELECT ID FROM LINEAS_PEDIDO WITH (NOLOCK) WHERE ORDEN = @ORDEN AND EST = 20)" & vbCrLf
sConsulta = sConsulta & "DELETE FROM LINEAS_PEDIDO_IMPUESTO WHERE LINEA IN (SELECT ID FROM LINEAS_PEDIDO WITH (NOLOCK) WHERE ORDEN = @ORDEN AND EST = 20)" & vbCrLf
sConsulta = sConsulta & "DELETE FROM LINEAS_PEDIDO_ENTREGAS WHERE LINEA_PEDIDO IN (SELECT ID FROM LINEAS_PEDIDO WITH (NOLOCK) WHERE ORDEN = @ORDEN AND EST = 20)" & vbCrLf
sConsulta = sConsulta & "DELETE FROM LINEAS_PEDIDO_FACTURA WHERE LINEA_PEDIDO IN (SELECT ID FROM LINEAS_PEDIDO WITH (NOLOCK) WHERE ORDEN = @ORDEN AND EST = 20)" & vbCrLf
sConsulta = sConsulta & "DELETE FROM LINEAS_PEDIDO_REGISTRO WHERE LINEA IN (SELECT ID FROM LINEAS_PEDIDO WITH (NOLOCK) WHERE ORDEN = @ORDEN AND EST = 20)" & vbCrLf
sConsulta = sConsulta & "DELETE FROM LINEAS_PEDIDO WHERE ORDEN = @ORDEN   AND EST = 20" & vbCrLf
sConsulta = sConsulta & "DECLARE @IMPORTE float" & vbCrLf
sConsulta = sConsulta & "SELECT @IMPORTE = SUM((PREC_UP*CANT_PED)+ISNULL(COSTES,0)-ISNULL(DESCUENTOS,0))   FROM LINEAS_PEDIDO WITH (NOLOCK) WHERE ORDEN = @ORDEN" & vbCrLf
sConsulta = sConsulta & "UPDATE LINEAS_PEDIDO   SET NIVEL_APROB=-1 WHERE ORDEN = @ORDEN" & vbCrLf
sConsulta = sConsulta & "UPDATE ORDEN_ENTREGA   SET IMPORTE = @IMPORTE,  EST = 2  FROM ORDEN_ENTREGA OE WITH (NOLOCK) INNER JOIN LINEAS_PEDIDO LP WITH (NOLOCK)  ON OE.ID = LP.ORDEN" & vbCrLf
sConsulta = sConsulta & "WHERE oe.id = @ORDEN" & vbCrLf
sConsulta = sConsulta & "INSERT INTO ORDEN_EST (PEDIDO, ORDEN, EST, FECHA, PER)  VALUES (@PEDIDO, @ORDEN, 2, GETDATE(),@APROVISIONADOR)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE LINEAS_PEDIDO SET NUM=ROW" & vbCrLf
sConsulta = sConsulta & "FROM (" & vbCrLf
sConsulta = sConsulta & "SELECT ID AS ID_LIN, NUM, ROW_NUMBER() OVER (ORDER BY NUM) AS ROW" & vbCrLf
sConsulta = sConsulta & "FROM LINEAS_PEDIDO WITH(NOLOCK) WHERE ORDEN=@ORDEN" & vbCrLf
sConsulta = sConsulta & ") AS TABLA" & vbCrLf
sConsulta = sConsulta & "WHERE LINEAS_PEDIDO.ID=TABLA.ID_LIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_GETWORKFLOW]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_GETWORKFLOW]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_GETWORKFLOW] @IDI AS VARCHAR(20), @USU AS VARCHAR(50),@TIPO AS INT=NULL" & vbCrLf
sConsulta = sConsulta & ",@FECHA AS VARCHAR(10)=NULL,@ORIGEN AS VARCHAR(50)=NULL,@FILAESTADO INT=NULL" & vbCrLf
sConsulta = sConsulta & ", @FORMATOFECHA AS VARCHAR(50)=NULL,@IMPORTEDESDE AS  FLOAT=NULL" & vbCrLf
sConsulta = sConsulta & ",@IMPORTEHASTA AS  FLOAT=NULL ,@NOFILTROS AS  TINYINT=0  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL1 AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL2 AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL3 AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL  AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SWHERE  AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLFROM AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL11 AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL12 AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL1COMBO  AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL2COMBO  AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL3COMBO  AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL11COMBO  AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SWHERECOMBO  AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLFROMCOMBO AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @REUSAR  AS TINYINT" & vbCrLf
sConsulta = sConsulta & "SET @REUSAR=0" & vbCrLf
sConsulta = sConsulta & "IF @NOFILTROS=1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @REUSAR=1" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @TIPOFECHA AS INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @TIPOFECHA=103" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @FORMATOFECHA='dd/mm/yyyy'" & vbCrLf
sConsulta = sConsulta & "SET @TIPOFECHA=103" & vbCrLf
sConsulta = sConsulta & "ELSE IF @FORMATOFECHA='mm/dd/yyyy'" & vbCrLf
sConsulta = sConsulta & "SET @TIPOFECHA=101" & vbCrLf
sConsulta = sConsulta & "ELSE IF @FORMATOFECHA='dd-mm-yyyy'" & vbCrLf
sConsulta = sConsulta & "SET @TIPOFECHA=105" & vbCrLf
sConsulta = sConsulta & "ELSE IF @FORMATOFECHA='mm-dd-yyyy'" & vbCrLf
sConsulta = sConsulta & "SET @TIPOFECHA=110" & vbCrLf
sConsulta = sConsulta & "ELSE IF @FORMATOFECHA='mm.dd.yyyy'" & vbCrLf
sConsulta = sConsulta & "SET @TIPOFECHA=104" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SWHERE = ' WHERE I.ESTADO=2'   --Las instancias anuladas y rechazadas no aparecen!!!!" & vbCrLf
sConsulta = sConsulta & "SET @SWHERECOMBO= ' WHERE I.ESTADO=2'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @TIPO <> 0 AND @TIPO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SWHERE = @SWHERE +  ' AND I.SOLICITUD= @TIPO'" & vbCrLf
sConsulta = sConsulta & "SET @REUSAR=0" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @FECHA IS NOT NULL AND @FECHA<>''" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SWHERE = @SWHERE + '  AND I.FEC_ALTA>= CONVERT(datetime,''' + @FECHA + ''',@TIPOFECHA)'" & vbCrLf
sConsulta = sConsulta & "SET @REUSAR=0" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "IF @ORIGEN IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SWHERE = @SWHERE + ' AND DEP.COD=@ORIGEN'" & vbCrLf
sConsulta = sConsulta & "SET @REUSAR=0" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "IF @IMPORTEDESDE IS NOT NULL AND @IMPORTEDESDE<>0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SWHERE = @SWHERE + ' AND I.IMPORTE>=@IMPORTEDESDE'" & vbCrLf
sConsulta = sConsulta & "SET @REUSAR=0" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF  @IMPORTEHASTA IS NOT NULL AND @IMPORTEHASTA<>0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SWHERE = @SWHERE + ' AND I.IMPORTE<=@IMPORTEHASTA'" & vbCrLf
sConsulta = sConsulta & "SET @REUSAR=0" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "SET @SWHERE = @SWHERE + ' AND I.NUM IS NOT NULL AND IB.EN_PROCESO=0 '" & vbCrLf
sConsulta = sConsulta & "SET @SWHERECOMBO=@SWHERECOMBO + ' AND I.NUM IS NOT NULL AND IB.EN_PROCESO=0 '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLFROM =  ' FROM INSTANCIA I WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN SOLICITUD S WITH (NOLOCK)  ON I.SOLICITUD=S.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN TIPO_SOLICITUDES TS WITH (NOLOCK)  ON TS.ID=S.TIPO" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN INSTANCIA_VISORES_PM C1  WITH (NOLOCK) ON C1.INSTANCIA=I.ID" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN INSTANCIA_INT INT WITH (NOLOCK) ON INT.ID = I.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PER PETPER WITH (NOLOCK) ON PETPER.COD = I.PETICIONARIO '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLFROMCOMBO = ' FROM INSTANCIA I WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN SOLICITUD S WITH (NOLOCK)  ON I.SOLICITUD=S.ID  '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--------------------------------------------------------------------------------- Pendientes de tratar ----------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "SET @SQL1COMBO = N'SELECT DISTINCT S.ID AS ID_SOLIC,S.COD,S.DEN_' + @IDI + ' AS DEN '" & vbCrLf
sConsulta = sConsulta & "SET @SQL1COMBO = @SQL1COMBO + @SQLFROMCOMBO + '" & vbCrLf
sConsulta = sConsulta & "INNER JOIN INSTANCIA_BLOQUE IB WITH (NOLOCK)  ON I.ID=IB.INSTANCIA AND IB.ESTADO=1 AND IB.BLOQ<>1" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_COPIA_ROL_BLOQUE RB WITH (NOLOCK)  ON IB.BLOQUE=RB.BLOQUE" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_COPIA_ROL R WITH (NOLOCK)  ON RB.ROL=R.ID AND R.PER=@USU" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN" & vbCrLf
sConsulta = sConsulta & "(" & vbCrLf
sConsulta = sConsulta & "SELECT CB.INSTANCIA,MAX(TIPO) TIPO" & vbCrLf
sConsulta = sConsulta & "FROM PM_COPIA_BLOQUE CB WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN INSTANCIA_BLOQUE IB WITH (NOLOCK) ON IB.ESTADO=1 AND IB.BLOQUE=CB.ID AND IB.INSTANCIA=CB.INSTANCIA" & vbCrLf
sConsulta = sConsulta & "GROUP BY CB.INSTANCIA" & vbCrLf
sConsulta = sConsulta & ") TIPOPET ON TIPOPET.INSTANCIA=I.ID '" & vbCrLf
sConsulta = sConsulta & "SET @SQL1COMBO = @SQL1COMBO + @SWHERECOMBO + ' AND  IB.TRASLADADA=0 '" & vbCrLf
sConsulta = sConsulta & "SET @SQL1COMBO = @SQL1COMBO + ' AND ( I.PETICIONARIO=''' + @USU + ''' OR TIPOPET.TIPO<>1)'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL1=N'" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT S.ID AS ID_SOLIC,S.COD, C1.TITULO_FEC, C1.TITULO_TEXT,I.ID AS ID_INS" & vbCrLf
sConsulta = sConsulta & ", MR.FECHA , M.PARALELO,CB.DEN AS DEN_ETAPA_ACTUAL" & vbCrLf
sConsulta = sConsulta & ", DEP.DEN  AS ORIGEN,R.PER AS USUARIO, P1.NOM AS NOM_USU , P1.APE  AS APE_USU,REEMITIDAS.NUM AS NUM_REEMISIONES, I.EN_PROCESO AS EN_PROCESO,RA.ROL AS CONSULTA, R.VER_DETALLE_PER VER_DETALLE_PER, I.PETICIONARIO PET,  PETPER.NOM AS NOM_PET , PETPER.APE APE_PET,I.IMPORTE,TS.TIPO,I.MON" & vbCrLf
sConsulta = sConsulta & ",RAA.ACCION AS ACCION_APROBAR,RAR.ACCION AS ACCION_RECHAZAR,IB.BLOQUE,I.ERROR_VALIDACION,I.EST_VALIDACION, FECHA_VALIDACION, VER_FLUJO, KO, ERROR '" & vbCrLf
sConsulta = sConsulta & "SET @SQL1 = @SQL1 + @SQLFROM + '" & vbCrLf
sConsulta = sConsulta & "INNER JOIN INSTANCIA_BLOQUE IB WITH (NOLOCK)  ON I.ID=IB.INSTANCIA AND IB.ESTADO=1 AND IB.BLOQ<>1" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_COPIA_ROL_BLOQUE RB WITH (NOLOCK)  ON IB.BLOQUE=RB.BLOQUE" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_COPIA_ROL R WITH (NOLOCK)  ON RB.ROL=R.ID" & vbCrLf
sConsulta = sConsulta & "   AND ( (R.PER=@USU AND (SELECT ISNULL(U.FSWS_SUSTITUCION,'''') FROM USU U WITH (NOLOCK) WHERE U.PER=@USU) = '''')" & vbCrLf
sConsulta = sConsulta & "           OR R.PER IN ( SELECT U.PER FROM USU U WITH (NOLOCK) WHERE U.FSWS_SUSTITUCION=@USU))" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PM_COPIA_ROL_ACCION RA WITH (NOLOCK) ON RA.ROL=R.ID AND RA.BLOQUE=RB.BLOQUE" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN" & vbCrLf
sConsulta = sConsulta & "(" & vbCrLf
sConsulta = sConsulta & "SELECT INSTANCIA_BLOQUE_DESTINO,MAX(FECHA) AS FECHA" & vbCrLf
sConsulta = sConsulta & "FROM INSTANCIA_CAMINO WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "GROUP BY INSTANCIA_BLOQUE_DESTINO" & vbCrLf
sConsulta = sConsulta & ")" & vbCrLf
sConsulta = sConsulta & "MR ON IB.ID=MR.INSTANCIA_BLOQUE_DESTINO" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PER P1 WITH (NOLOCK)  ON R.PER=P1.COD" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN DEP WITH (NOLOCK)  ON P1.DEP=DEP.COD" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN" & vbCrLf
sConsulta = sConsulta & "(" & vbCrLf
sConsulta = sConsulta & "SELECT INSTANCIA,COUNT(*) AS PARALELO" & vbCrLf
sConsulta = sConsulta & "FROM INSTANCIA_BLOQUE WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE ESTADO=1 AND BLOQ<>1" & vbCrLf
sConsulta = sConsulta & "GROUP BY INSTANCIA" & vbCrLf
sConsulta = sConsulta & ") M ON I.ID=M.INSTANCIA" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PM_COPIA_BLOQUE_DEN CB WITH (NOLOCK)  ON IB.BLOQUE=CB.BLOQUE AND IDI=@IDI" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN" & vbCrLf
sConsulta = sConsulta & "(" & vbCrLf
sConsulta = sConsulta & "SELECT IB.INSTANCIA,COUNT(*) AS NUM" & vbCrLf
sConsulta = sConsulta & "FROM INSTANCIA_BLOQUE IB  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_COPIA_BLOQUE CB WITH (NOLOCK)  ON IB.BLOQUE=CB.ID" & vbCrLf
sConsulta = sConsulta & "WHERE IB.ESTADO<>1 AND CB.TIPO=1" & vbCrLf
sConsulta = sConsulta & "GROUP BY IB.INSTANCIA" & vbCrLf
sConsulta = sConsulta & ") REEMITIDAS ON I.ID=REEMITIDAS.INSTANCIA" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN" & vbCrLf
sConsulta = sConsulta & "(" & vbCrLf
sConsulta = sConsulta & "SELECT CB.INSTANCIA,MAX(TIPO) TIPO" & vbCrLf
sConsulta = sConsulta & "FROM PM_COPIA_BLOQUE CB WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN INSTANCIA_BLOQUE IB WITH (NOLOCK) ON IB.ESTADO=1 AND IB.BLOQUE=CB.ID AND IB.INSTANCIA=CB.INSTANCIA" & vbCrLf
sConsulta = sConsulta & "GROUP BY CB.INSTANCIA" & vbCrLf
sConsulta = sConsulta & ") TIPOPET ON TIPOPET.INSTANCIA=I.ID" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PM_COPIA_ROL_ACCION RAA WITH (NOLOCK) ON RAA.ROL=R.ID AND RAA.BLOQUE=RB.BLOQUE AND RAA.APROBAR=1" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PM_COPIA_ROL_ACCION RAR WITH (NOLOCK) ON RAR.ROL=R.ID AND RAR.BLOQUE=RB.BLOQUE AND RAR.RECHAZAR=1'" & vbCrLf
sConsulta = sConsulta & "SET @SQL1=@SQL1 + @SWHERE + ' AND  IB.TRASLADADA=0 '" & vbCrLf
sConsulta = sConsulta & "--Las rechazadas temporalmente tampoco" & vbCrLf
sConsulta = sConsulta & "--a menos que se sea el peticionario que inicio la instancia" & vbCrLf
sConsulta = sConsulta & "--           ? se haya rechazado a una etapa no peticionario" & vbCrLf
sConsulta = sConsulta & "SET @SQL1 = @SQL1 + ' AND (       ( I.PETICIONARIO=''' + @USU + ''' AND (SELECT ISNULL(U.FSWS_SUSTITUCION,'''') FROM USU U WITH (NOLOCK) WHERE U.PER=@USU) = '''')" & vbCrLf
sConsulta = sConsulta & "       OR I.PETICIONARIO IN ( SELECT U.PER FROM USU U WITH (NOLOCK) WHERE U.FSWS_SUSTITUCION=@USU) OR TIPOPET.TIPO<>1)'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "----------------------------------------------------------------------------Las que se le han trasladado ---------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "SET @SQL11COMBO= N'SELECT DISTINCT S.ID AS ID_SOLIC,S.COD,S.DEN_' + @IDI + ' AS DEN '" & vbCrLf
sConsulta = sConsulta & "SET @SQL11COMBO=@SQL11COMBO + @SQLFROMCOMBO  + '" & vbCrLf
sConsulta = sConsulta & "INNER JOIN INSTANCIA_BLOQUE IB  WITH (NOLOCK) ON I.ID= IB.INSTANCIA AND IB.ESTADO=1 AND IB.BLOQ<>1" & vbCrLf
sConsulta = sConsulta & "INNER JOIN" & vbCrLf
sConsulta = sConsulta & "(" & vbCrLf
sConsulta = sConsulta & "SELECT MAX(ID) ID ,INSTANCIA,BLOQUE" & vbCrLf
sConsulta = sConsulta & "FROM INSTANCIA_EST WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE DESTINATARIO IS NOT NULL OR DESTINATARIO_PROV IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "GROUP BY INSTANCIA,BLOQUE" & vbCrLf
sConsulta = sConsulta & ") T ON I.ID=T.INSTANCIA AND IB.ID=T.BLOQUE" & vbCrLf
sConsulta = sConsulta & "INNER JOIN INSTANCIA_EST IE WITH (NOLOCK)  ON T.ID=IE.ID  AND ( IE.DESTINATARIO=@USU OR IE.DESTINATARIO IN ( SELECT U.PER FROM USU U WITH (NOLOCK) WHERE U.FSWS_SUSTITUCION=@USU)) '" & vbCrLf
sConsulta = sConsulta & "SET @SQL11COMBO=@SQL11COMBO + @SWHERECOMBO + ' AND  IB.TRASLADADA=1  ORDER BY S.COD'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL11= N'" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT S.ID AS ID_SOLIC,S.COD,C1.TITULO_FEC, C1.TITULO_TEXT,I.ID AS ID_INS" & vbCrLf
sConsulta = sConsulta & ",IE.FECHA , M.PARALELO, CB.DEN AS DENETAPA_ACTUAL" & vbCrLf
sConsulta = sConsulta & ", DEP.DEN  AS ORIGEN,IE.PER AS USUARIO, PER.NOM AS NOM_USU, PER.APE  AS APE_USU,REEMITIDAS.NUM AS NUM_REEMISIONES, I.EN_PROCESO AS EN_PROCESO, 0 AS CONSULTA, CR.VER_DETALLE_PER VER_DETALLE_PER, I.PETICIONARIO PET,  PETPER.NOM  NOM_PET ,  PETPER.APE APE_PET,I.IMPORTE,TS.TIPO,I.MON" & vbCrLf
sConsulta = sConsulta & ",0 AS ACCION_APROBAR,0 AS ACCION_RECHAZAR,IB.BLOQUE,I.ERROR_VALIDACION,I.EST_VALIDACION, FECHA_VALIDACION, VER_FLUJO, KO, ERROR '" & vbCrLf
sConsulta = sConsulta & "SET @SQL11=@SQL11 + @SQLFROM  + '" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN (" & vbCrLf
sConsulta = sConsulta & "SELECT IB.INSTANCIA,COUNT(*) AS NUM" & vbCrLf
sConsulta = sConsulta & "FROM INSTANCIA_BLOQUE IB  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_COPIA_BLOQUE CB WITH (NOLOCK)  ON IB.BLOQUE=CB.ID" & vbCrLf
sConsulta = sConsulta & "WHERE IB.ESTADO<>1 AND CB.TIPO=1" & vbCrLf
sConsulta = sConsulta & "GROUP BY IB.INSTANCIA" & vbCrLf
sConsulta = sConsulta & ") REEMITIDAS ON I.ID=REEMITIDAS.INSTANCIA" & vbCrLf
sConsulta = sConsulta & "INNER JOIN" & vbCrLf
sConsulta = sConsulta & "(" & vbCrLf
sConsulta = sConsulta & "SELECT INSTANCIA, COUNT(*) AS PARALELO,MIN(BLOQUE) AS BLOQUE" & vbCrLf
sConsulta = sConsulta & "FROM INSTANCIA_BLOQUE  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE ESTADO=1 AND BLOQ<>1" & vbCrLf
sConsulta = sConsulta & "GROUP BY INSTANCIA" & vbCrLf
sConsulta = sConsulta & ") M ON I.ID=M.INSTANCIA" & vbCrLf
sConsulta = sConsulta & "INNER JOIN INSTANCIA_BLOQUE IB  WITH (NOLOCK) ON M.INSTANCIA= IB.INSTANCIA AND IB.ESTADO=1 AND IB.BLOQ<>1" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PM_COPIA_BLOQUE_DEN CB  WITH (NOLOCK) ON IB.BLOQUE=CB.BLOQUE AND IDI=@IDI" & vbCrLf
sConsulta = sConsulta & "INNER JOIN" & vbCrLf
sConsulta = sConsulta & "(" & vbCrLf
sConsulta = sConsulta & "SELECT MAX(ID) ID ,INSTANCIA,BLOQUE" & vbCrLf
sConsulta = sConsulta & "FROM INSTANCIA_EST WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE DESTINATARIO IS NOT NULL OR DESTINATARIO_PROV IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "GROUP BY INSTANCIA,BLOQUE" & vbCrLf
sConsulta = sConsulta & ") T ON I.ID=T.INSTANCIA AND IB.ID=T.BLOQUE" & vbCrLf
sConsulta = sConsulta & "INNER JOIN INSTANCIA_EST IE WITH (NOLOCK)  ON T.ID=IE.ID  AND ( (IE.DESTINATARIO=@USU AND (SELECT ISNULL(U.FSWS_SUSTITUCION,'''') FROM USU U WITH (NOLOCK) WHERE U.PER=@USU) = '''')" & vbCrLf
sConsulta = sConsulta & "   OR IE.DESTINATARIO IN ( SELECT U.PER FROM USU U WITH (NOLOCK) WHERE U.FSWS_SUSTITUCION=@USU))" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_COPIA_ROL CR WITH (NOLOCK) ON IE.ROL= CR.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PER WITH (NOLOCK)  ON IE.PER=PER.COD" & vbCrLf
sConsulta = sConsulta & "INNER JOIN DEP WITH (NOLOCK)  ON PER.DEP=DEP.COD'" & vbCrLf
sConsulta = sConsulta & "SET @SQL11= @SQL11 + @SWHERE + ' AND IB.TRASLADADA=1 '" & vbCrLf
sConsulta = sConsulta & "SET @SQL11= @SQL11 + ' ORDER BY I.ID'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--04/02/2009" & vbCrLf
sConsulta = sConsulta & "--Ha sido trasladada a proveedor y ha sido devuelta por el proveedor POR esto  IE.PER es nulo e IE.ROL es nulo." & vbCrLf
sConsulta = sConsulta & "--Se cambia FSPM_DEVOLUCION_INSTANCIA para que cumplimente ROL. Sigue PER a nulo." & vbCrLf
sConsulta = sConsulta & "SET @SQL12= N'" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT S.ID AS ID_SOLIC,S.COD,C1.TITULO_FEC, C1.TITULO_TEXT,I.ID AS ID_INS" & vbCrLf
sConsulta = sConsulta & ",IE.FECHA , M.PARALELO, CB.DEN AS DENETAPA_ACTUAL" & vbCrLf
sConsulta = sConsulta & ", DEP.DEN  AS ORIGEN,IE.DESTINATARIO AS USUARIO, PER.NOM AS NOM_USU, PER.APE  AS APE_USU,REEMITIDAS.NUM AS NUM_REEMISIONES, I.EN_PROCESO AS EN_PROCESO, 0 AS CONSULTA, CR.VER_DETALLE_PER VER_DETALLE_PER, I.PETICIONARIO PET,  PETPER.NOM  NOM_PET ,  PETPER.APE APE_PET,I.IMPORTE,TS.TIPO,I.MON" & vbCrLf
sConsulta = sConsulta & ",0 AS ACCION_APROBAR,0 AS ACCION_RECHAZAR,IB.BLOQUE,I.ERROR_VALIDACION,I.EST_VALIDACION, FECHA_VALIDACION, VER_FLUJO, KO, ERROR '" & vbCrLf
sConsulta = sConsulta & "SET @SQL12=@SQL12 + @SQLFROM  + '" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN (" & vbCrLf
sConsulta = sConsulta & "SELECT IB.INSTANCIA,COUNT(*) AS NUM" & vbCrLf
sConsulta = sConsulta & "FROM INSTANCIA_BLOQUE IB  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_COPIA_BLOQUE CB WITH (NOLOCK)  ON IB.BLOQUE=CB.ID" & vbCrLf
sConsulta = sConsulta & "WHERE IB.ESTADO<>1 AND CB.TIPO=1" & vbCrLf
sConsulta = sConsulta & "GROUP BY IB.INSTANCIA" & vbCrLf
sConsulta = sConsulta & ") REEMITIDAS ON I.ID=REEMITIDAS.INSTANCIA" & vbCrLf
sConsulta = sConsulta & "INNER JOIN" & vbCrLf
sConsulta = sConsulta & "(" & vbCrLf
sConsulta = sConsulta & "SELECT INSTANCIA, COUNT(*) AS PARALELO,MIN(BLOQUE) AS BLOQUE" & vbCrLf
sConsulta = sConsulta & "FROM INSTANCIA_BLOQUE  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE ESTADO=1 AND BLOQ<>1" & vbCrLf
sConsulta = sConsulta & "GROUP BY INSTANCIA" & vbCrLf
sConsulta = sConsulta & ") M ON I.ID=M.INSTANCIA" & vbCrLf
sConsulta = sConsulta & "INNER JOIN INSTANCIA_BLOQUE IB  WITH (NOLOCK) ON M.INSTANCIA= IB.INSTANCIA AND IB.ESTADO=1 AND IB.BLOQ<>1" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PM_COPIA_BLOQUE_DEN CB  WITH (NOLOCK) ON IB.BLOQUE=CB.BLOQUE AND IDI=@IDI" & vbCrLf
sConsulta = sConsulta & "INNER JOIN" & vbCrLf
sConsulta = sConsulta & "(" & vbCrLf
sConsulta = sConsulta & "SELECT MAX(ID) ID ,INSTANCIA,BLOQUE" & vbCrLf
sConsulta = sConsulta & "FROM INSTANCIA_EST WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE DESTINATARIO IS NOT NULL OR DESTINATARIO_PROV IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "GROUP BY INSTANCIA,BLOQUE" & vbCrLf
sConsulta = sConsulta & ") T ON I.ID=T.INSTANCIA AND IB.ID=T.BLOQUE" & vbCrLf
sConsulta = sConsulta & "INNER JOIN INSTANCIA_EST IE WITH (NOLOCK)  ON T.ID=IE.ID AND IE.PROVE IS NOT NULL AND IE.PER IS NULL" & vbCrLf
sConsulta = sConsulta & "   AND ( (IE.DESTINATARIO=@USU AND (SELECT ISNULL(U.FSWS_SUSTITUCION,'''') FROM USU U WITH (NOLOCK) WHERE U.PER=@USU) = '''') OR IE.DESTINATARIO IN ( SELECT U.PER FROM USU U WITH (NOLOCK) WHERE U.FSWS_SUSTITUCION=@USU))" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_COPIA_ROL CR WITH (NOLOCK) ON IE.ROL = CR.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PER WITH (NOLOCK)  ON IE.DESTINATARIO=PER.COD" & vbCrLf
sConsulta = sConsulta & "INNER JOIN DEP WITH (NOLOCK)  ON PER.DEP=DEP.COD'" & vbCrLf
sConsulta = sConsulta & "SET @SQL12= @SQL12 + @SWHERE + ' AND IB.TRASLADADA=1 '" & vbCrLf
sConsulta = sConsulta & "SET @SQL12= @SQL12 + ' ORDER BY I.ID'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "---------------------------------------------------------------------- Las de una lista de autoasignacion  ----------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "SET @SQL3COMBO=N'SELECT DISTINCT S.ID AS ID_SOLIC,S.COD,S.DEN_' + @IDI + ' AS DEN '" & vbCrLf
sConsulta = sConsulta & "SET @SQL3COMBO=@SQL3COMBO + @SQLFROMCOMBO  + '" & vbCrLf
sConsulta = sConsulta & "INNER JOIN INSTANCIA_BLOQUE IB WITH (NOLOCK)  ON I.ID=IB.INSTANCIA AND IB.ESTADO=1 AND IB.BLOQ<>1" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_COPIA_ROL_BLOQUE RB WITH (NOLOCK)  ON IB.BLOQUE=RB.BLOQUE" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_COPIA_ROL R WITH (NOLOCK)  ON RB.ROL=R.ID AND R.PER IS NULL AND (R.COMO_ASIGNAR=3 OR R.COMO_ASIGNAR=4)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_COPIA_PARTICIPANTES PT WITH (NOLOCK)  ON PT.ROL=R.ID AND PT.PER=@USU '" & vbCrLf
sConsulta = sConsulta & "SET @SQL3COMBO=@SQL3COMBO + @SWHERECOMBO + ' AND  IB.TRASLADADA=0 '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL3=N'" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT S.ID AS ID_SOLIC,S.COD,C1.TITULO_FEC ,C1.TITULO_TEXT,I.ID AS ID_INS" & vbCrLf
sConsulta = sConsulta & ", MR.FECHA , M.PARALELO,CB.DEN AS DENETAPA_ACTUAL" & vbCrLf
sConsulta = sConsulta & ", DEP.DEN  AS ORIGEN,R.PER AS USUARIO, PER.NOM  AS NOM_USU  , PER.APE  AS APE_USU,REEMITIDAS.NUM AS NUM_REEMISIONES, I.EN_PROCESO AS EN_PROCESO, 0 AS CONSULTA, R.VER_DETALLE_PER VER_DETALLE_PER, I.PETICIONARIO PET,  PETPER.NOM  NOM_PET, PETPER.APE APE_PET,I.IMPORTE,TS.TIPO,I.MON" & vbCrLf
sConsulta = sConsulta & ",RAA.ACCION AS ACCION_APROBAR,RAR.ACCION AS ACCION_RECHAZAR,IB.BLOQUE,I.ERROR_VALIDACION,I.EST_VALIDACION, FECHA_VALIDACION, VER_FLUJO, KO, ERROR '" & vbCrLf
sConsulta = sConsulta & "SET @SQL3 = @SQL3 + @SQLFROM + '" & vbCrLf
sConsulta = sConsulta & "INNER JOIN INSTANCIA_BLOQUE IB WITH (NOLOCK)  ON I.ID=IB.INSTANCIA AND IB.ESTADO=1 AND IB.BLOQ<>1" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_COPIA_ROL_BLOQUE RB WITH (NOLOCK)  ON IB.BLOQUE=RB.BLOQUE" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_COPIA_ROL R WITH (NOLOCK)  ON RB.ROL=R.ID AND R.PER IS NULL AND (R.COMO_ASIGNAR=3 OR R.COMO_ASIGNAR=4)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_COPIA_PARTICIPANTES PT WITH (NOLOCK)  ON PT.ROL=R.ID AND ( (PT.PER=@USU AND (SELECT ISNULL(U.FSWS_SUSTITUCION,'''') FROM USU U WITH (NOLOCK) WHERE U.PER=@USU) = '''')" & vbCrLf
sConsulta = sConsulta & "   OR PT.PER IN ( SELECT U.PER FROM USU U WITH (NOLOCK) WHERE U.FSWS_SUSTITUCION=@USU))" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN" & vbCrLf
sConsulta = sConsulta & "(" & vbCrLf
sConsulta = sConsulta & "SELECT INSTANCIA_BLOQUE_DESTINO,MAX(FECHA) AS FECHA" & vbCrLf
sConsulta = sConsulta & "FROM INSTANCIA_CAMINO  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "GROUP BY INSTANCIA_BLOQUE_DESTINO" & vbCrLf
sConsulta = sConsulta & ") MR ON IB.ID=MR.INSTANCIA_BLOQUE_DESTINO" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PER WITH (NOLOCK)  ON R.PER=PER.COD" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN DEP WITH (NOLOCK)  ON PER.DEP=DEP.COD" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN" & vbCrLf
sConsulta = sConsulta & "(" & vbCrLf
sConsulta = sConsulta & "SELECT INSTANCIA,COUNT(*) AS PARALELO" & vbCrLf
sConsulta = sConsulta & "FROM INSTANCIA_BLOQUE  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE ESTADO=1 AND BLOQ<>1" & vbCrLf
sConsulta = sConsulta & "GROUP BY INSTANCIA" & vbCrLf
sConsulta = sConsulta & ") M ON I.ID=M.INSTANCIA" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PM_COPIA_BLOQUE_DEN CB WITH (NOLOCK)  ON IB.BLOQUE=CB.BLOQUE AND IDI=@IDI" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN" & vbCrLf
sConsulta = sConsulta & "(" & vbCrLf
sConsulta = sConsulta & "SELECT IB.INSTANCIA,COUNT(*) AS NUM" & vbCrLf
sConsulta = sConsulta & "FROM INSTANCIA_BLOQUE IB  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_COPIA_BLOQUE CB WITH (NOLOCK)  ON IB.BLOQUE=CB.ID" & vbCrLf
sConsulta = sConsulta & "WHERE IB.ESTADO<>1 AND CB.TIPO=1" & vbCrLf
sConsulta = sConsulta & "GROUP BY IB.INSTANCIA" & vbCrLf
sConsulta = sConsulta & ") REEMITIDAS ON I.ID=REEMITIDAS.INSTANCIA" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PM_COPIA_ROL_ACCION RAA WITH (NOLOCK) ON RAA.ROL=R.ID AND RAA.BLOQUE=RB.BLOQUE AND RAA.APROBAR=1" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PM_COPIA_ROL_ACCION RAR WITH (NOLOCK) ON RAR.ROL=R.ID AND RAR.BLOQUE=RB.BLOQUE AND RAR.RECHAZAR=1'" & vbCrLf
sConsulta = sConsulta & "SET @SQL3=@SQL3 + @SWHERE + ' AND  IB.TRASLADADA=0 '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "------------------------------------------------------------------------- Esperando devoluci?n -------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "SET @SQL2COMBO=N'SELECT DISTINCT S.ID AS ID_SOLIC,S.COD,S.DEN_' + @IDI + ' AS DEN '" & vbCrLf
sConsulta = sConsulta & "SET @SQL2COMBO=@SQL2COMBO + @SQLFROMCOMBO  + '" & vbCrLf
sConsulta = sConsulta & "INNER JOIN" & vbCrLf
sConsulta = sConsulta & "(" & vbCrLf
sConsulta = sConsulta & "SELECT INSTANCIA, COUNT(*) AS PARALELO,MIN(BLOQUE) AS BLOQUE" & vbCrLf
sConsulta = sConsulta & "FROM INSTANCIA_BLOQUE  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE ESTADO=1 AND BLOQ<>1" & vbCrLf
sConsulta = sConsulta & "GROUP BY INSTANCIA" & vbCrLf
sConsulta = sConsulta & ") M ON I.ID=M.INSTANCIA" & vbCrLf
sConsulta = sConsulta & "INNER JOIN INSTANCIA_BLOQUE IB WITH (NOLOCK)  ON M.INSTANCIA= IB.INSTANCIA AND M.BLOQUE=IB.BLOQUE AND IB.ESTADO=1 AND IB.BLOQ<>1" & vbCrLf
sConsulta = sConsulta & "INNER JOIN" & vbCrLf
sConsulta = sConsulta & "(" & vbCrLf
sConsulta = sConsulta & "SELECT MAX(IE1.ID) ID,IE1.INSTANCIA,IE1.BLOQUE" & vbCrLf
sConsulta = sConsulta & "FROM INSTANCIA_EST IE1 WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE (IE1.ACCION=3 OR IE1.ACCION=4)  AND (IE1.DESTINATARIO IS NOT NULL OR IE1.DESTINATARIO_PROV IS NOT NULL) and IE1.PER=@USU" & vbCrLf
sConsulta = sConsulta & "and not exists (select 1 from INSTANCIA_EST IE2 WITH (NOLOCK) where IE1.INSTANCIA = IE2.INSTANCIA" & vbCrLf
sConsulta = sConsulta & "and IE2.DESTINATARIO=@USU and (IE2.ACCION=3 or IE2.ACCION=4) and IE2.ID > IE1.ID)" & vbCrLf
sConsulta = sConsulta & "GROUP BY IE1.INSTANCIA,IE1.BLOQUE" & vbCrLf
sConsulta = sConsulta & ") T ON I.ID=T.INSTANCIA AND IB.ID=T.BLOQUE" & vbCrLf
sConsulta = sConsulta & "INNER JOIN INSTANCIA_EST IE WITH (NOLOCK)  ON T.ID=IE.ID AND IE.PER=@USU AND (IE.ACCION = 3 OR IE.ACCION = 4) '" & vbCrLf
sConsulta = sConsulta & "SET @SQL2COMBO=@SQL2COMBO + @SWHERECOMBO + ' AND IB.TRASLADADA=1   ORDER BY S.COD'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL2=N'SELECT DISTINCT S.ID AS ID_SOLIC,S.COD, C1.TITULO_FEC , C1.TITULO_TEXT ,I.ID AS ID_INS" & vbCrLf
sConsulta = sConsulta & ",IE.FECHA, M.PARALELO,CB.DEN AS DENETAPA_ACTUAL" & vbCrLf
sConsulta = sConsulta & ", DEP.DEN  AS ORIGEN,IE.PER AS USUARIO, PER.NOM NOM_USU , PER.APE  AS APE_USU,REEMITIDAS.NUM AS NUM_REEMISIONES, I.EN_PROCESO AS EN_PROCESO, 0 AS CONSULTA, CR.VER_DETALLE_PER VER_DETALLE_PER,I.PETICIONARIO PET,  PETPER.NOM NOM_PET , PETPER.APE APE_PET,I.IMPORTE,TS.TIPO,I.MON" & vbCrLf
sConsulta = sConsulta & ",0 AS ACCION_APROBAR,0 AS ACCION_RECHAZAR,IB.BLOQUE,I.ERROR_VALIDACION,I.EST_VALIDACION, FECHA_VALIDACION, VER_FLUJO, KO, ERROR '" & vbCrLf
sConsulta = sConsulta & "SET @SQL2 = @SQL2 + @SQLFROM + '" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN" & vbCrLf
sConsulta = sConsulta & "(" & vbCrLf
sConsulta = sConsulta & "SELECT IB.INSTANCIA,COUNT(*) AS NUM" & vbCrLf
sConsulta = sConsulta & "FROM INSTANCIA_BLOQUE IB  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_COPIA_BLOQUE CB WITH (NOLOCK)  ON IB.BLOQUE=CB.ID" & vbCrLf
sConsulta = sConsulta & "WHERE IB.ESTADO<>1 AND CB.TIPO=1" & vbCrLf
sConsulta = sConsulta & "GROUP BY IB.INSTANCIA" & vbCrLf
sConsulta = sConsulta & ") REEMITIDAS ON I.ID=REEMITIDAS.INSTANCIA" & vbCrLf
sConsulta = sConsulta & "INNER JOIN" & vbCrLf
sConsulta = sConsulta & "(" & vbCrLf
sConsulta = sConsulta & "SELECT INSTANCIA, COUNT(*) AS PARALELO,MIN(BLOQUE) AS BLOQUE" & vbCrLf
sConsulta = sConsulta & "FROM INSTANCIA_BLOQUE  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE ESTADO=1 AND BLOQ<>1" & vbCrLf
sConsulta = sConsulta & "GROUP BY INSTANCIA" & vbCrLf
sConsulta = sConsulta & ") M ON I.ID=M.INSTANCIA" & vbCrLf
sConsulta = sConsulta & "INNER JOIN INSTANCIA_BLOQUE IB WITH (NOLOCK)  ON M.INSTANCIA= IB.INSTANCIA AND M.BLOQUE=IB.BLOQUE AND IB.ESTADO=1 AND IB.BLOQ<>1" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PM_COPIA_BLOQUE_DEN CB WITH (NOLOCK)  ON IB.BLOQUE=CB.BLOQUE AND IDI=@IDI" & vbCrLf
sConsulta = sConsulta & "INNER JOIN" & vbCrLf
sConsulta = sConsulta & "(" & vbCrLf
sConsulta = sConsulta & "SELECT MAX(IE1.ID) ID,IE1.INSTANCIA,IE1.BLOQUE" & vbCrLf
sConsulta = sConsulta & "FROM INSTANCIA_EST IE1 WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE (IE1.ACCION=3 OR IE1.ACCION=4)  AND (IE1.DESTINATARIO IS NOT NULL OR IE1.DESTINATARIO_PROV IS NOT NULL) and ( ( IE1.PER=@USU and ( IE1.PER=@USU" & vbCrLf
sConsulta = sConsulta & "       AND (SELECT ISNULL(U.FSWS_SUSTITUCION,'''') FROM USU U WITH (NOLOCK) WHERE U.PER=@USU) = '''')  OR IE1.PER IN ( SELECT U.PER FROM USU U WITH (NOLOCK) WHERE U.FSWS_SUSTITUCION=@USU) ))" & vbCrLf
sConsulta = sConsulta & "and not exists (select 1 from INSTANCIA_EST IE2 WITH (NOLOCK) where IE1.INSTANCIA = IE2.INSTANCIA" & vbCrLf
sConsulta = sConsulta & "and ( ( IE2.DESTINATARIO=@USU and ( IE2.DESTINATARIO=@USU AND (SELECT ISNULL(U.FSWS_SUSTITUCION,'''') FROM USU U WITH (NOLOCK) WHERE U.PER=@USU) = ''''))" & vbCrLf
sConsulta = sConsulta & "           OR IE2.DESTINATARIO IN ( SELECT U.PER FROM USU U WITH (NOLOCK) WHERE U.FSWS_SUSTITUCION=@USU) ) and (IE2.ACCION=3 or IE2.ACCION=4) and IE2.ID > IE1.ID)" & vbCrLf
sConsulta = sConsulta & "GROUP BY IE1.INSTANCIA,IE1.BLOQUE" & vbCrLf
sConsulta = sConsulta & ") T ON I.ID=T.INSTANCIA AND IB.ID=T.BLOQUE" & vbCrLf
sConsulta = sConsulta & "INNER JOIN INSTANCIA_EST IE WITH (NOLOCK)  ON T.ID=IE.ID AND ( ( IE.PER=@USU and ( IE.PER=@USU AND (SELECT ISNULL(U.FSWS_SUSTITUCION,'''') FROM USU U WITH (NOLOCK) WHERE U.PER=@USU) = '''') )" & vbCrLf
sConsulta = sConsulta & "   OR IE.PER IN ( SELECT U.PER FROM USU U WITH (NOLOCK) WHERE U.FSWS_SUSTITUCION=@USU) ) AND (IE.ACCION = 3 OR IE.ACCION = 4)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_COPIA_ROL CR WITH (NOLOCK) ON IE.ROL = CR.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PER  WITH (NOLOCK)  ON IE.PER=PER.COD" & vbCrLf
sConsulta = sConsulta & "INNER JOIN DEP WITH (NOLOCK)  ON PER.DEP=DEP.COD'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL2 = @SQL2 +  @SWHERE + ' AND IB.TRASLADADA=1   ORDER BY I.ID'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @FILAESTADO=1 --Pendientes de tratar, tanto las que les corresponde al rol como las que se le ha trasladado y tiene que cumplimentar:" & vbCrLf
sConsulta = sConsulta & "begin  --No cabe en 1 sola select .Inserto en tablas temporales" & vbCrLf
sConsulta = sConsulta & "SET @REUSAR=0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE #GRID_WORKFLOW4 (ID_SOLIC INT, COD VARCHAR(50)  ,TITULO_FEC DATETIME, TITULO_TEXT VARCHAR(500) , ID_INS INT, FECHA DATETIME, PARALELO INT , DENETAPA_ACTUAL VARCHAR(500) ,ORIGEN VARCHAR(500) , USUARIO VARCHAR(50) , NOM_USU VARCHAR(500) , APE_USUARIO  VARCHAR(500), NUM_REEMISIONES INT, EN_PROCESO INT, CONSULTA FLOAT, VER_DETALLE_PER INT,PET VARCHAR(20),NOM_PET VARCHAR(200),APE_PET VARCHAR(200), IMPORTE FLOAT,TIPO_SOLIC INT,MON VARCHAR(3),ACCION_APROBAR INT,ACCION_RECHAZAR INT,BLOQUE INT,ERROR_VALIDACION VARCHAR(2000),EST_VALIDACION TINYINT, FECHA_VALIDACION DATETIME, VER_FLUJO TINYINT, KO TINYINT, ERROR VARCHAR(500))" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N' INSERT INTO #GRID_WORKFLOW4 ' + @SQL1" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@USU VARCHAR(50), @TIPO INT, @FECHA VARCHAR(10), @ORIGEN  VARCHAR(50),@IDI VARCHAR(50),@TIPOFECHA INT', @USU = @USU,@TIPO=@TIPO,@FECHA=@FECHA,@ORIGEN=@ORIGEN,@IDI=@IDI,@TIPOFECHA=@TIPOFECHA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE #GRID_WORKFLOW5 (ID_SOLIC INT, COD VARCHAR(50)  ,TITULO_FEC DATETIME, TITULO_TEXT VARCHAR(500) , ID_INS INT, FECHA DATETIME, PARALELO INT , DENETAPA_ACTUAL VARCHAR(500) ,ORIGEN VARCHAR(500) , USUARIO VARCHAR(50) , NOM_USU VARCHAR(500) , APE_USUARIO  VARCHAR(500), NUM_REEMISIONES INT, EN_PROCESO INT, CONSULTA FLOAT, VER_DETALLE_PER INT,PET VARCHAR(20),NOM_PET VARCHAR(200),APE_PET VARCHAR(200), IMPORTE FLOAT,TIPO_SOLIC INT,MON VARCHAR(3),ACCION_APROBAR INT,ACCION_RECHAZAR INT,BLOQUE INT,ERROR_VALIDACION VARCHAR(2000),EST_VALIDACION TINYINT, FECHA_VALIDACION DATETIME, VER_FLUJO TINYINT, KO TINYINT, ERROR VARCHAR(500))" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N' INSERT INTO #GRID_WORKFLOW5 ' + @SQL3" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@USU VARCHAR(50), @TIPO INT, @FECHA VARCHAR(10), @ORIGEN  VARCHAR(50),@IDI VARCHAR(50),@TIPOFECHA INT', @USU = @USU,@TIPO=@TIPO,@FECHA=@FECHA,@ORIGEN=@ORIGEN,@IDI=@IDI,@TIPOFECHA=@TIPOFECHA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE #GRID_TITULO (ID_SOLIC INT, COD VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS,TITULO_FEC DATETIME, TITULO_TEXT VARCHAR(500) COLLATE SQL_Latin1_General_CP1_CI_AS, ID_INS INT, FECHA DATETIME, PARALELO INT, DENETAPA_ACTUAL VARCHAR(500) COLLATE SQL_Latin1_General_CP1_CI_AS,ORIGEN VARCHAR(500) COLLATE SQL_Latin1_General_CP1_CI_AS, USUARIO VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS, NOM_USU VARCHAR(500) COLLATE SQL_Latin1_General_CP1_CI_AS,APE_USU VARCHAR(500) COLLATE SQL_Latin1_General_CP1_CI_AS, NUM_REEMISIONES INT, EN_PROCESO INT, CONSULTA FLOAT,VER_DETALLE_PER INT,PET VARCHAR(20),NOM_PET VARCHAR(200),APE_PET VARCHAR(200),IMPORTE FLOAT,TIPO_SOLIC INT,MON VARCHAR(3),ACCION_APROBAR INT,ACCION_RECHAZAR INT,BLOQUE INT,ERROR_VALIDACION VARCHAR(2000),EST_VALIDACION TINYINT, FECHA_VALIDACION DATETIME, VER_FLUJO TINYINT, KO TINYINT, ERROR VARCHAR(500))" & vbCrLf
sConsulta = sConsulta & "SET @SQL=N' INSERT INTO #GRID_TITULO SELECT * FROM #GRID_WORKFLOW4 UNION SELECT * FROM #GRID_WORKFLOW5 ORDER BY ID_INS'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@USU VARCHAR(50), @TIPO INT, @FECHA VARCHAR(10), @ORIGEN  VARCHAR(50),@IDI VARCHAR(50),@TIPOFECHA INT', @USU = @USU,@TIPO=@TIPO,@FECHA=@FECHA,@ORIGEN=@ORIGEN,@IDI=@IDI,@TIPOFECHA=@TIPOFECHA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL ='" & vbCrLf
sConsulta = sConsulta & "SELECT T.ID_SOLIC,T.COD, CASE WHEN T.TITULO_FEC IS NULL THEN T.TITULO_TEXT ELSE CONVERT(VARCHAR ,T.TITULO_FEC,103) END DESCR, T.ID_INS, T.FECHA" & vbCrLf
sConsulta = sConsulta & ", CASE WHEN T.PARALELO>1 THEN NULL ELSE  ISNULL(T.DENETAPA_ACTUAL,'' '') END AS DENETAPA_ACTUAL" & vbCrLf
sConsulta = sConsulta & ", T.USUARIO,  T.NOM_USU  + '' '' + T.APE_USU  AS NOM_USU" & vbCrLf
sConsulta = sConsulta & ", T.ORIGEN, T.NUM_REEMISIONES" & vbCrLf
sConsulta = sConsulta & ", T.EN_PROCESO,CASE WHEN T.CONSULTA IS NULL THEN 1 ELSE 0 END AS CONSULTA, T.VER_DETALLE_PER, T.PET, T.NOM_PET + '' '' + T.APE_PET NOM_PET,T.IMPORTE,T.TIPO_SOLIC,T.MON" & vbCrLf
sConsulta = sConsulta & ",ISNULL(T.ACCION_APROBAR,0) AS ACCION_APROBAR, ISNULL(T.ACCION_RECHAZAR,0) AS ACCION_RECHAZAR,T.BLOQUE,ERROR_VALIDACION,EST_VALIDACION, FECHA_VALIDACION, VER_FLUJO, KO, ERROR" & vbCrLf
sConsulta = sConsulta & "FROM #GRID_TITULO T  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "ORDER BY ID_INS'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@USU VARCHAR(50), @TIPO INT, @FECHA VARCHAR(10), @ORIGEN  VARCHAR(50),@IDI VARCHAR(50),@TIPOFECHA INT', @USU = @USU,@TIPO=@TIPO,@FECHA=@FECHA,@ORIGEN=@ORIGEN,@IDI=@IDI,@TIPOFECHA=@TIPOFECHA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DROP TABLE #GRID_WORKFLOW4" & vbCrLf
sConsulta = sConsulta & "DROP TABLE #GRID_WORKFLOW5" & vbCrLf
sConsulta = sConsulta & "DROP TABLE #GRID_TITULO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "begin" & vbCrLf
sConsulta = sConsulta & "IF @FILAESTADO=2 --Esperando devoluci?n:" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @REUSAR=0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE #GRID_TITULO2 (ID_SOLIC INT, COD VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS,TITULO_FEC DATETIME, TITULO_TEXT VARCHAR(500) COLLATE SQL_Latin1_General_CP1_CI_AS, ID_INS INT, FECHA DATETIME, PARALELO INT, DENETAPA_ACTUAL VARCHAR(500) COLLATE SQL_Latin1_General_CP1_CI_AS,ORIGEN VARCHAR(500) COLLATE SQL_Latin1_General_CP1_CI_AS, USUARIO VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS, NOM_USU VARCHAR(500) COLLATE SQL_Latin1_General_CP1_CI_AS,APE_USU VARCHAR(500) COLLATE SQL_Latin1_General_CP1_CI_AS, NUM_REEMISIONES INT, EN_PROCESO INT, CONSULTA FLOAT,VER_DETALLE_PER INT,PET VARCHAR(20),NOM_PET VARCHAR(200),APE_PET VARCHAR(200),IMPORTE FLOAT,TIPO_SOLIC INT,MON VARCHAR(3),ACCION_APROBAR INT,ACCION_RECHAZAR INT,BLOQUE INT,ERROR_VALIDACION VARCHAR(2000),EST_VALIDACION TINYINT, FECHA_VALIDACION DATETIME, VER_FLUJO TINYINT, KO TINYINT, ERROR VARCHAR(500))" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N' INSERT INTO #GRID_TITULO2 ' + @SQL2" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@USU VARCHAR(50), @TIPO INT, @FECHA VARCHAR(10), @ORIGEN  VARCHAR(50),@IDI VARCHAR(50),@TIPOFECHA INT,@IMPORTEDESDE FLOAT,@IMPORTEHASTA FLOAT', @USU = @USU,@TIPO=@TIPO,@FECHA=@FECHA,@ORIGEN=@ORIGEN,@IDI=@IDI,@TIPOFECHA=@TIPOFECHA,@IMPORTEDESDE=@IMPORTEDESDE,@IMPORTEHASTA=@IMPORTEHASTA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL ='" & vbCrLf
sConsulta = sConsulta & "SELECT T.ID_SOLIC,T.COD, CASE WHEN T.TITULO_FEC IS NULL THEN T.TITULO_TEXT ELSE CONVERT(VARCHAR ,T.TITULO_FEC,103) END DESCR, T.ID_INS, T.FECHA" & vbCrLf
sConsulta = sConsulta & ", CASE WHEN T.PARALELO>1 THEN NULL ELSE  ISNULL(T.DENETAPA_ACTUAL,'' '') END AS DENETAPA_ACTUAL" & vbCrLf
sConsulta = sConsulta & ", T.USUARIO,  T.NOM_USU  + '' '' + T.APE_USU  AS NOM_USU" & vbCrLf
sConsulta = sConsulta & ", T.ORIGEN, T.NUM_REEMISIONES" & vbCrLf
sConsulta = sConsulta & ", T.EN_PROCESO,CASE WHEN T.CONSULTA IS NULL THEN 1 ELSE 0 END AS CONSULTA, T.VER_DETALLE_PER, T.PET, T.NOM_PET + '' '' + T.APE_PET NOM_PET,T.IMPORTE,T.TIPO_SOLIC,T.MON" & vbCrLf
sConsulta = sConsulta & ",T.ACCION_APROBAR,T.ACCION_RECHAZAR,T.BLOQUE,ERROR_VALIDACION,EST_VALIDACION, FECHA_VALIDACION, VER_FLUJO, KO, ERROR" & vbCrLf
sConsulta = sConsulta & "FROM #GRID_TITULO2 T  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "ORDER BY ID_INS'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@USU VARCHAR(50), @TIPO INT, @FECHA VARCHAR(10), @ORIGEN  VARCHAR(50),@IDI VARCHAR(50),@TIPOFECHA INT,@IMPORTEDESDE FLOAT,@IMPORTEHASTA FLOAT', @USU = @USU,@TIPO=@TIPO,@FECHA=@FECHA,@ORIGEN=@ORIGEN,@IDI=@IDI,@TIPOFECHA=@TIPOFECHA,@IMPORTEDESDE=@IMPORTEDESDE,@IMPORTEHASTA=@IMPORTEHASTA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DROP TABLE #GRID_TITULO2" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ELSE --Todas" & vbCrLf
sConsulta = sConsulta & "begin  --No cabe en 1 sola select .Inserto en tablas temporales:" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE #GRID_WORKFLOW (ID_SOLIC INT, COD VARCHAR(50)  ,TITULO_FEC DATETIME, TITULO_TEXT VARCHAR(500) , ID_INS INT, FECHA DATETIME, PARALELO INT , DENETAPA_ACTUAL VARCHAR(500) ,ORIGEN VARCHAR(500) , USUARIO VARCHAR(50) , NOM_USU VARCHAR(500) , APE_USUARIO  VARCHAR(500), NUM_REEMISIONES INT, EN_PROCESO INT, CONSULTA FLOAT, VER_DETALLE_PER INT,PET VARCHAR(20),NOM_PET VARCHAR(200),APE_PET VARCHAR(200), IMPORTE FLOAT,TIPO_SOLIC INT,MON VARCHAR(3),ACCION_APROBAR INT,ACCION_RECHAZAR INT,BLOQUE INT,ERROR_VALIDACION VARCHAR(2000),EST_VALIDACION TINYINT, FECHA_VALIDACION DATETIME, VER_FLUJO TINYINT, KO TINYINT, ERROR VARCHAR(500))" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N' INSERT INTO #GRID_WORKFLOW ' + @SQL1" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@USU VARCHAR(50), @TIPO INT, @FECHA VARCHAR(10), @ORIGEN  VARCHAR(50),@IDI VARCHAR(50),@TIPOFECHA INT,@IMPORTEDESDE FLOAT,@IMPORTEHASTA FLOAT', @USU = @USU,@TIPO=@TIPO,@FECHA=@FECHA,@ORIGEN=@ORIGEN,@IDI=@IDI,@TIPOFECHA=@TIPOFECHA,@IMPORTEDESDE=@IMPORTEDESDE,@IMPORTEHASTA=@IMPORTEHASTA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE #GRID_WORKFLOW2 (ID_SOLIC INT, COD VARCHAR(50)  ,TITULO_FEC DATETIME, TITULO_TEXT VARCHAR(500) , ID_INS INT, FECHA DATETIME, PARALELO INT , DENETAPA_ACTUAL VARCHAR(500) ,ORIGEN VARCHAR(500) , USUARIO VARCHAR(50) , NOM_USU VARCHAR(500) , APE_USUARIO  VARCHAR(500), NUM_REEMISIONES INT, EN_PROCESO INT, CONSULTA FLOAT, VER_DETALLE_PER INT,PET VARCHAR(20),NOM_PET VARCHAR(200),APE_PET VARCHAR(200), IMPORTE FLOAT,TIPO_SOLIC INT,MON VARCHAR(3),ACCION_APROBAR INT,ACCION_RECHAZAR INT,BLOQUE INT,ERROR_VALIDACION VARCHAR(2000),EST_VALIDACION TINYINT, FECHA_VALIDACION DATETIME, VER_FLUJO TINYINT, KO TINYINT, ERROR VARCHAR(500))" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N' INSERT INTO #GRID_WORKFLOW2 ' + @SQL2" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@USU VARCHAR(50), @TIPO INT, @FECHA VARCHAR(10), @ORIGEN  VARCHAR(50),@IDI VARCHAR(50),@TIPOFECHA INT,@IMPORTEDESDE FLOAT,@IMPORTEHASTA FLOAT', @USU = @USU,@TIPO=@TIPO,@FECHA=@FECHA,@ORIGEN=@ORIGEN,@IDI=@IDI,@TIPOFECHA=@TIPOFECHA,@IMPORTEDESDE=@IMPORTEDESDE,@IMPORTEHASTA=@IMPORTEHASTA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE #GRID_WORKFLOW3 (ID_SOLIC INT, COD VARCHAR(50)  ,TITULO_FEC DATETIME, TITULO_TEXT VARCHAR(500) , ID_INS INT, FECHA DATETIME, PARALELO INT , DENETAPA_ACTUAL VARCHAR(500) ,ORIGEN VARCHAR(500) , USUARIO VARCHAR(50) , NOM_USU VARCHAR(500) , APE_USUARIO  VARCHAR(500), NUM_REEMISIONES INT, EN_PROCESO INT, CONSULTA FLOAT, VER_DETALLE_PER INT,PET VARCHAR(20),NOM_PET VARCHAR(200),APE_PET VARCHAR(200), IMPORTE FLOAT,TIPO_SOLIC INT,MON VARCHAR(3),ACCION_APROBAR INT,ACCION_RECHAZAR INT,BLOQUE INT,ERROR_VALIDACION VARCHAR(2000),EST_VALIDACION TINYINT, FECHA_VALIDACION DATETIME, VER_FLUJO TINYINT, KO TINYINT, ERROR VARCHAR(500))" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N' INSERT INTO #GRID_WORKFLOW3 ' + @SQL3" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@USU VARCHAR(50), @TIPO INT, @FECHA VARCHAR(10), @ORIGEN  VARCHAR(50),@IDI VARCHAR(50),@TIPOFECHA INT,@IMPORTEDESDE FLOAT,@IMPORTEHASTA FLOAT', @USU = @USU,@TIPO=@TIPO,@FECHA=@FECHA,@ORIGEN=@ORIGEN,@IDI=@IDI,@TIPOFECHA=@TIPOFECHA,@IMPORTEDESDE=@IMPORTEDESDE,@IMPORTEHASTA=@IMPORTEHASTA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE #GRID_WORKFLOW11 (ID_SOLIC INT, COD VARCHAR(50)  ,TITULO_FEC DATETIME, TITULO_TEXT VARCHAR(500) , ID_INS INT, FECHA DATETIME, PARALELO INT , DENETAPA_ACTUAL VARCHAR(500) ,ORIGEN VARCHAR(500) , USUARIO VARCHAR(50) , NOM_USU VARCHAR(500) , APE_USUARIO  VARCHAR(500), NUM_REEMISIONES INT, EN_PROCESO INT, CONSULTA FLOAT, VER_DETALLE_PER INT,PET VARCHAR(20),NOM_PET VARCHAR(200),APE_PET VARCHAR(200), IMPORTE FLOAT,TIPO_SOLIC INT,MON VARCHAR(3),ACCION_APROBAR INT,ACCION_RECHAZAR INT,BLOQUE INT,ERROR_VALIDACION VARCHAR(2000),EST_VALIDACION TINYINT, FECHA_VALIDACION DATETIME, VER_FLUJO TINYINT, KO TINYINT, ERROR VARCHAR(500))" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N' INSERT INTO #GRID_WORKFLOW11 ' + @SQL11" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@USU VARCHAR(50), @TIPO INT, @FECHA VARCHAR(10), @ORIGEN  VARCHAR(50),@IDI VARCHAR(50),@TIPOFECHA INT,@IMPORTEDESDE FLOAT,@IMPORTEHASTA FLOAT', @USU = @USU,@TIPO=@TIPO,@FECHA=@FECHA,@ORIGEN=@ORIGEN,@IDI=@IDI,@TIPOFECHA=@TIPOFECHA,@IMPORTEDESDE=@IMPORTEDESDE,@IMPORTEHASTA=@IMPORTEHASTA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N' INSERT INTO #GRID_WORKFLOW11 ' + @SQL12" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@USU VARCHAR(50), @TIPO INT, @FECHA VARCHAR(10), @ORIGEN  VARCHAR(50),@IDI VARCHAR(50),@TIPOFECHA INT,@IMPORTEDESDE FLOAT,@IMPORTEHASTA FLOAT', @USU = @USU,@TIPO=@TIPO,@FECHA=@FECHA,@ORIGEN=@ORIGEN,@IDI=@IDI,@TIPOFECHA=@TIPOFECHA,@IMPORTEDESDE=@IMPORTEDESDE,@IMPORTEHASTA=@IMPORTEHASTA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE #GRID_TITULO3 (ID_SOLIC INT, COD VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS,TITULO_FEC DATETIME, TITULO_TEXT VARCHAR(500) COLLATE SQL_Latin1_General_CP1_CI_AS, ID_INS INT, FECHA DATETIME, PARALELO INT, DENETAPA_ACTUAL VARCHAR(500) COLLATE SQL_Latin1_General_CP1_CI_AS,ORIGEN VARCHAR(500) COLLATE SQL_Latin1_General_CP1_CI_AS, USUARIO VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS, NOM_USU VARCHAR(500) COLLATE SQL_Latin1_General_CP1_CI_AS,APE_USU VARCHAR(500) COLLATE SQL_Latin1_General_CP1_CI_AS, NUM_REEMISIONES INT, EN_PROCESO INT, CONSULTA FLOAT,VER_DETALLE_PER INT,PET VARCHAR(20),NOM_PET VARCHAR(200),APE_PET VARCHAR(200),IMPORTE FLOAT,TIPO_SOLIC INT,MON VARCHAR(3),ACCION_APROBAR INT,ACCION_RECHAZAR INT,BLOQUE INT,ERROR_VALIDACION VARCHAR(2000),EST_VALIDACION TINYINT, FECHA_VALIDACION DATETIME, VER_FLUJO TINYINT, KO TINYINT, ERROR VARCHAR(500))" & vbCrLf
sConsulta = sConsulta & "SET @SQL=N' INSERT INTO #GRID_TITULO3 SELECT * FROM #GRID_WORKFLOW WITH (NOLOCK) UNION SELECT * FROM #GRID_WORKFLOW2 WITH (NOLOCK) UNION SELECT * FROM #GRID_WORKFLOW3 WITH (NOLOCK) UNION SELECT * FROM #GRID_WORKFLOW11 WITH (NOLOCK) ORDER BY ID_INS'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@USU VARCHAR(50), @TIPO INT, @FECHA VARCHAR(10), @ORIGEN  VARCHAR(50),@IDI VARCHAR(50),@FORMATOFECHA VARCHAR(50),@IMPORTEDESDE FLOAT,@IMPORTEHASTA FLOAT', @USU = @USU,@TIPO=@TIPO,@FECHA=@FECHA,@ORIGEN=@ORIGEN,@IDI=@IDI,@FORMATOFECHA=@FORMATOFECHA,@IMPORTEDESDE=@IMPORTEDESDE,@IMPORTEHASTA=@IMPORTEHASTA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL ='" & vbCrLf
sConsulta = sConsulta & "SELECT T.ID_SOLIC,T.COD, CASE WHEN T.TITULO_FEC IS NULL THEN T.TITULO_TEXT ELSE CONVERT(VARCHAR ,T.TITULO_FEC,103) END DESCR, T.ID_INS, T.FECHA" & vbCrLf
sConsulta = sConsulta & ", CASE WHEN T.PARALELO>1 THEN NULL ELSE  ISNULL(T.DENETAPA_ACTUAL,'' '') END AS DENETAPA_ACTUAL" & vbCrLf
sConsulta = sConsulta & ", T.USUARIO,  T.NOM_USU  + '' '' + T.APE_USU  AS NOM_USU" & vbCrLf
sConsulta = sConsulta & ", T.ORIGEN, T.NUM_REEMISIONES" & vbCrLf
sConsulta = sConsulta & ", T.EN_PROCESO,CASE WHEN T.CONSULTA IS NULL THEN 1 ELSE 0 END AS CONSULTA, T.VER_DETALLE_PER, T.PET, T.NOM_PET + '' '' + T.APE_PET NOM_PET,T.IMPORTE,T.TIPO_SOLIC,T.MON" & vbCrLf
sConsulta = sConsulta & ",ISNULL(T.ACCION_APROBAR,0) AS ACCION_APROBAR, ISNULL(T.ACCION_RECHAZAR,0) AS ACCION_RECHAZAR,T.BLOQUE,ERROR_VALIDACION,EST_VALIDACION, FECHA_VALIDACION, VER_FLUJO, KO, ERROR" & vbCrLf
sConsulta = sConsulta & "FROM #GRID_TITULO3 T  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE T.EN_PROCESO = 0" & vbCrLf
sConsulta = sConsulta & "ORDER BY ID_INS'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@USU VARCHAR(50), @TIPO INT, @FECHA VARCHAR(10), @ORIGEN  VARCHAR(50),@IDI VARCHAR(50),@TIPOFECHA INT,@IMPORTEDESDE FLOAT,@IMPORTEHASTA FLOAT', @USU = @USU,@TIPO=@TIPO,@FECHA=@FECHA,@ORIGEN=@ORIGEN,@IDI=@IDI,@TIPOFECHA=@TIPOFECHA,@IMPORTEDESDE=@IMPORTEDESDE,@IMPORTEHASTA=@IMPORTEHASTA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @REUSAR=1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SQL ='SELECT NULL AS ID, NULL AS COD, NULL AS DEN  UNION" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT T.ID_SOLIC AS ID,T.COD,S.DEN_' + @IDI + ' AS DEN" & vbCrLf
sConsulta = sConsulta & "FROM #GRID_TITULO3 T  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN SOLICITUD S WITH (NOLOCK) ON T.ID_SOLIC=S.ID" & vbCrLf
sConsulta = sConsulta & "ORDER BY T.COD'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@IDI VARCHAR(50)',@IDI=@IDI" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF  @NOFILTROS = 1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DELETE #GRID_WORKFLOW" & vbCrLf
sConsulta = sConsulta & "DELETE #GRID_WORKFLOW2" & vbCrLf
sConsulta = sConsulta & "DELETE #GRID_WORKFLOW3" & vbCrLf
sConsulta = sConsulta & "DELETE #GRID_WORKFLOW11" & vbCrLf
sConsulta = sConsulta & "DELETE #GRID_TITULO3" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N' INSERT INTO #GRID_WORKFLOW (ID_SOLIC, COD, TITULO_TEXT) ' + @SQL1COMBO" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@USU VARCHAR(50) ,@IDI VARCHAR(50)', @USU = @USU,@IDI=@IDI" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N' INSERT INTO #GRID_WORKFLOW2 (ID_SOLIC, COD, TITULO_TEXT)' + @SQL2COMBO" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@USU VARCHAR(50) ,@IDI VARCHAR(50)', @USU = @USU,@IDI=@IDI" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N' INSERT INTO #GRID_WORKFLOW3 (ID_SOLIC, COD, TITULO_TEXT)' + @SQL3COMBO" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@USU VARCHAR(50) ,@IDI VARCHAR(50)', @USU = @USU,@IDI=@IDI" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N' INSERT INTO #GRID_WORKFLOW11 (ID_SOLIC, COD, TITULO_TEXT)' + @SQL11COMBO" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@USU VARCHAR(50) ,@IDI VARCHAR(50)', @USU = @USU,@IDI=@IDI" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL=N' INSERT INTO #GRID_TITULO3 (ID_SOLIC, COD, TITULO_TEXT)" & vbCrLf
sConsulta = sConsulta & "SELECT ID_SOLIC, COD, TITULO_TEXT FROM #GRID_WORKFLOW WITH (NOLOCK) UNION SELECT ID_SOLIC, COD, TITULO_TEXT FROM #GRID_WORKFLOW2 WITH (NOLOCK) UNION SELECT ID_SOLIC, COD, TITULO_TEXT FROM #GRID_WORKFLOW3 WITH (NOLOCK) UNION SELECT ID_SOLIC, COD, TITULO_TEXT FROM #GRID_WORKFLOW11 WITH (NOLOCK) ORDER BY COD'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL ='SELECT NULL AS ID, NULL AS COD, NULL AS DEN" & vbCrLf
sConsulta = sConsulta & "UNION SELECT DISTINCT T.ID_SOLIC AS ID,T.COD,T.TITULO_TEXT AS DEN FROM #GRID_TITULO3 T  WITH (NOLOCK) ORDER BY T.COD'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @REUSAR=1" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DROP TABLE #GRID_WORKFLOW" & vbCrLf
sConsulta = sConsulta & "DROP TABLE #GRID_WORKFLOW2" & vbCrLf
sConsulta = sConsulta & "DROP TABLE #GRID_WORKFLOW3" & vbCrLf
sConsulta = sConsulta & "DROP TABLE #GRID_WORKFLOW11" & vbCrLf
sConsulta = sConsulta & "DROP TABLE #GRID_TITULO3" & vbCrLf
sConsulta = sConsulta & "end" & vbCrLf
sConsulta = sConsulta & "end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @NOFILTROS=1 AND @REUSAR=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE #GRID_WORKFLOWCOMBO (ID_SOLIC INT, COD VARCHAR(50), DEN VARCHAR(500) )" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE #GRID_WORKFLOW2COMBO(ID_SOLIC INT, COD VARCHAR(50), DEN VARCHAR(500) )" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE #GRID_WORKFLOW3COMBO (ID_SOLIC INT, COD VARCHAR(50), DEN VARCHAR(500) )" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE #GRID_WORKFLOW11COMBO(ID_SOLIC INT, COD VARCHAR(50), DEN VARCHAR(500) )" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE #GRID_TITULO3COMBO (ID_SOLIC INT, COD VARCHAR(50), DEN VARCHAR(500) COLLATE SQL_Latin1_General_CP1_CI_AS)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N' INSERT INTO #GRID_WORKFLOWCOMBO (ID_SOLIC, COD, DEN) ' + @SQL1COMBO" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@USU VARCHAR(50) ,@IDI VARCHAR(50)', @USU = @USU,@IDI=@IDI" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N' INSERT INTO #GRID_WORKFLOW2COMBO (ID_SOLIC, COD, DEN)' + @SQL2COMBO" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@USU VARCHAR(50) ,@IDI VARCHAR(50)', @USU = @USU,@IDI=@IDI" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N' INSERT INTO #GRID_WORKFLOW3COMBO (ID_SOLIC, COD, DEN)' + @SQL3COMBO" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@USU VARCHAR(50) ,@IDI VARCHAR(50)', @USU = @USU,@IDI=@IDI" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N' INSERT INTO #GRID_WORKFLOW11COMBO (ID_SOLIC, COD, DEN)' + @SQL11COMBO" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@USU VARCHAR(50) ,@IDI VARCHAR(50)', @USU = @USU,@IDI=@IDI" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL=N' INSERT INTO #GRID_TITULO3COMBO" & vbCrLf
sConsulta = sConsulta & "SELECT * FROM #GRID_WORKFLOWCOMBO WITH (NOLOCK) UNION SELECT * FROM #GRID_WORKFLOW2COMBO WITH (NOLOCK) UNION SELECT* FROM #GRID_WORKFLOW3COMBO WITH (NOLOCK) UNION SELECT * FROM #GRID_WORKFLOW11COMBO WITH (NOLOCK) ORDER BY COD'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL ='SELECT NULL AS ID, NULL AS COD, NULL AS DEN" & vbCrLf
sConsulta = sConsulta & "UNION SELECT DISTINCT T.ID_SOLIC AS ID,T.COD,T.DEN FROM #GRID_TITULO3COMBO T  WITH (NOLOCK) ORDER BY T.COD'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DROP TABLE #GRID_WORKFLOWCOMBO" & vbCrLf
sConsulta = sConsulta & "DROP TABLE #GRID_WORKFLOW2COMBO" & vbCrLf
sConsulta = sConsulta & "DROP TABLE #GRID_WORKFLOW3COMBO" & vbCrLf
sConsulta = sConsulta & "DROP TABLE #GRID_WORKFLOW11COMBO" & vbCrLf
sConsulta = sConsulta & "DROP TABLE #GRID_TITULO3COMBO" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta


sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSEP_CARGAR_ATRIBUTOS_PEDIDO]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSEP_CARGAR_ATRIBUTOS_PEDIDO]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSEP_CARGAR_ATRIBUTOS_PEDIDO]" & vbCrLf
sConsulta = sConsulta & "@IDPEDIDO INT," & vbCrLf
sConsulta = sConsulta & "@IDTIPOPEDIDO INT," & vbCrLf
sConsulta = sConsulta & "@EMPRESA INT," & vbCrLf
sConsulta = sConsulta & "@INTEGRACION TINYINT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING1 NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "SELECT @SQLSTRING1 = N'SELECT ID, COD,  DEN, TIPO, INTRO, INTERNO, OBLIGATORIO, VALORMINFEC,VALORMINNUM," & vbCrLf
sConsulta = sConsulta & "       VALORMAXFEC,VALORMAXNUM, VALOR_TEXT, VALOR_NUM, VALOR_FEC, VALOR_BOOL , VALNUM,VALTEXT,VALFEC,VALBOOL, MAX(INTEGRACION) INTEGRACION" & vbCrLf
sConsulta = sConsulta & "       FROM ('" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @SQLSTRING =" & vbCrLf
sConsulta = sConsulta & "case when CESTA.CAT5 is null then" & vbCrLf
sConsulta = sConsulta & "case when CESTA.CAT4 is null then" & vbCrLf
sConsulta = sConsulta & "case when CESTA.CAT3 is null then" & vbCrLf
sConsulta = sConsulta & "case when CESTA.CAT2 is null then  N'SELECT DF.ID ID, DF.COD COD,  DF.DEN DEN, DF.TIPO TIPO, DF.INTRO INTRO, CTN1.INTERNO INTERNO, CTN1.OBLIGATORIO OBLIGATORIO, DF.MINFEC VALORMINFEC,DF.MINNUM VALORMINNUM," & vbCrLf
sConsulta = sConsulta & "DF.MAXFEC VALORMAXFEC,DF.MAXNUM VALORMAXNUM, CTN1.VALOR_TEXT, CTN1.VALOR_NUM, CTN1.VALOR_FEC, CTN1.VALOR_BOOL, CA.VALOR_NUM AS VALNUM,CA.VALOR_TEXT AS VALTEXT,CA.VALOR_FEC AS VALFEC,CA.VALOR_BOOL AS VALBOOL, 0 AS INTEGRACION  FROM CATN1_ATRIB CTN1 WITH (NOLOCK) left join DEF_ATRIB DF WITH (NOLOCK) on CTN1.ATRIB = DF.ID  LEFT OUTER JOIN CESTA_CABECERA_ATRIB CA WITH (NOLOCK) on CA.CESTACABECERAID = '+ convert(VARCHAR, @IDPEDIDO)  +' AND CA.ATRIB = DF.ID  where CTN1.CATN1_ID = '+convert(nvarchar(255), CESTA.CAT1) + ' AND CTN1.AMBITO = 0 AND CTN1.TIPO = 1'" & vbCrLf
sConsulta = sConsulta & "else  N'SELECT DF.ID ID, DF.COD COD,  DF.DEN DEN, DF.TIPO TIPO, DF.INTRO INTRO, CTN2.INTERNO INTERNO, CTN2.OBLIGATORIO OBLIGATORIO, DF.MINFEC VALORMINFEC,DF.MINNUM VALORMINNUM," & vbCrLf
sConsulta = sConsulta & "DF.MAXFEC VALORMAXFEC,DF.MAXNUM VALORMAXNUM, CTN2.VALOR_TEXT, CTN2.VALOR_NUM, CTN2.VALOR_FEC, CTN2.VALOR_BOOL, CA.VALOR_NUM AS VALNUM,CA.VALOR_TEXT AS VALTEXT,CA.VALOR_FEC AS VALFEC,CA.VALOR_BOOL AS VALBOOL, 0 AS INTEGRACION  FROM CATN2_ATRIB CTN2 WITH (NOLOCK) left join DEF_ATRIB DF WITH (NOLOCK) on CTN2.ATRIB = DF.ID  LEFT OUTER JOIN CESTA_CABECERA_ATRIB CA WITH (NOLOCK) on CA.CESTACABECERAID = '+ convert(VARCHAR, @IDPEDIDO)  +' AND CA.ATRIB = DF.ID  where CTN2.CATN2_ID = '+convert(nvarchar(255), CESTA.CAT2) + ' AND CTN2.AMBITO = 0 AND CTN2.TIPO = 1' end" & vbCrLf
sConsulta = sConsulta & "else  N'SELECT DF.ID ID, DF.COD COD,  DF.DEN DEN, DF.TIPO TIPO, DF.INTRO INTRO, CTN3.INTERNO INTERNO, CTN3.OBLIGATORIO OBLIGATORIO, DF.MINFEC VALORMINFEC,DF.MINNUM VALORMINNUM," & vbCrLf
sConsulta = sConsulta & "DF.MAXFEC VALORMAXFEC,DF.MAXNUM VALORMAXNUM, CTN3.VALOR_TEXT, CTN3.VALOR_NUM, CTN3.VALOR_FEC, CTN3.VALOR_BOOL, CA.VALOR_NUM AS VALNUM,CA.VALOR_TEXT AS VALTEXT,CA.VALOR_FEC AS VALFEC,CA.VALOR_BOOL AS VALBOOL, 0 AS INTEGRACION  FROM CATN3_ATRIB CTN3 WITH (NOLOCK) left join DEF_ATRIB DF WITH (NOLOCK) on CTN3.ATRIB = DF.ID  LEFT OUTER JOIN CESTA_CABECERA_ATRIB CA WITH (NOLOCK) on CA.CESTACABECERAID = '+ convert(VARCHAR, @IDPEDIDO)  +' AND CA.ATRIB = DF.ID  where CTN3.CATN3_ID = '+convert(nvarchar(255), CESTA.CAT3) + ' AND CTN3.AMBITO = 0 AND CTN3.TIPO = 1' end" & vbCrLf
sConsulta = sConsulta & "else  N'SELECT DF.ID ID, DF.COD COD,  DF.DEN DEN, DF.TIPO TIPO, DF.INTRO INTRO, CTN4.INTERNO INTERNO, CTN4.OBLIGATORIO OBLIGATORIO, DF.MINFEC VALORMINFEC,DF.MINNUM VALORMINNUM," & vbCrLf
sConsulta = sConsulta & "DF.MAXFEC VALORMAXFEC,DF.MAXNUM VALORMAXNUM, CTN4.VALOR_TEXT, CTN4.VALOR_NUM, CTN4.VALOR_FEC, CTN4.VALOR_BOOL, CA.VALOR_NUM AS VALNUM,CA.VALOR_TEXT AS VALTEXT,CA.VALOR_FEC AS VALFEC,CA.VALOR_BOOL AS VALBOOL, 0 AS INTEGRACION  FROM CATN4_ATRIB CTN4 WITH (NOLOCK) left join DEF_ATRIB DF WITH (NOLOCK) on CTN4.ATRIB = DF.ID  LEFT OUTER JOIN CESTA_CABECERA_ATRIB CA WITH (NOLOCK) on CA.CESTACABECERAID = '+ convert(VARCHAR, @IDPEDIDO)  +' AND CA.ATRIB = DF.ID  where CTN4.CATN4_ID = '+convert(nvarchar(255), CESTA.CAT4) + ' AND CTN4.AMBITO = 0 AND CTN4.TIPO = 1' end" & vbCrLf
sConsulta = sConsulta & "else  N'SELECT DF.ID ID, DF.COD COD,  DF.DEN DEN, DF.TIPO TIPO, DF.INTRO INTRO, CT1.INTERNO INTERNO, CT1.OBLIGATORIO OBLIGATORIO, DF.MINFEC VALORMINFEC,DF.MINNUM VALORMINNUM," & vbCrLf
sConsulta = sConsulta & "DF.MAXFEC VALORMAXFEC,DF.MAXNUM VALORMAXNUM, CT1.VALOR_TEXT, CT1.VALOR_NUM, CT1.VALOR_FEC, CT1.VALOR_BOOL, CA.VALOR_NUM AS VALNUM,CA.VALOR_TEXT AS VALTEXT,CA.VALOR_FEC AS VALFEC,CA.VALOR_BOOL AS VALBOOL, 0 AS INTEGRACION FROM CATN5_ATRIB CT1 WITH (NOLOCK) left join DEF_ATRIB DF WITH (NOLOCK) on CT1.ATRIB = DF.ID LEFT OUTER JOIN CESTA_CABECERA_ATRIB CA WITH (NOLOCK) on CA.CESTACABECERAID = '+ convert(VARCHAR, @IDPEDIDO)  +' AND CA.ATRIB = DF.ID where CT1.CATN5_ID = '+convert(nvarchar(255), CESTA.CAT5) + ' AND CT1.AMBITO = 0 AND CT1.TIPO = 1' end" & vbCrLf
sConsulta = sConsulta & "FROM CESTA where CESTACABECERAID = @IDPEDIDO;" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "If (@INTEGRACION = 1)" & vbCrLf
sConsulta = sConsulta & "   Begin" & vbCrLf
sConsulta = sConsulta & "       SET @SQLSTRING =  @SQLSTRING + N' UNION SELECT DF.ID ID, DF.COD COD,  DF.DEN DEN, DF.TIPO TIPO, DF.INTRO INTRO, 0 INTERNO, IA.OBLIGATORIO OBLIGATORIO, DF.MINFEC VALORMINFEC,DF.MINNUM VALORMINNUM," & vbCrLf
sConsulta = sConsulta & "           DF.MAXFEC VALORMAXFEC,DF.MAXNUM VALORMAXNUM, IA.VALOR_TEXT, IA.VALOR_NUM, IA.VALOR_FEC, IA.VALOR_BOOL, CA.VALOR_NUM AS VALNUM,CA.VALOR_TEXT AS VALTEXT,CA.VALOR_FEC AS VALFEC,CA.VALOR_BOOL AS VALBOOL, 1 AS INTEGRACION" & vbCrLf
sConsulta = sConsulta & "           from INT_ATRIB IA WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN DEF_ATRIB DF WITH (NOLOCK) ON IA.ATRIB=DF.ID" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN ERP ERP WITH(NOLOCK) on IA.ERP = ERP.COD" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN EMP EMP WITH(NOLOCK) on ERP.COD = EMP.ERP" & vbCrLf
sConsulta = sConsulta & "           LEFT OUTER JOIN CESTA_CABECERA_ATRIB CA WITH (NOLOCK) on CA.CESTACABECERAID = '+ convert(VARCHAR, @IDPEDIDO)  +' AND CA.ATRIB = DF.ID" & vbCrLf
sConsulta = sConsulta & "           where EMP.ID = ' + convert(VARCHAR, @EMPRESA)  +  ' AND IA.PEDIDO = 1 and (IA.AMBITO = 1 or IA.AMBITO = 2)" & vbCrLf
sConsulta = sConsulta & "           AND IA.ID not in (SELECT ATRIB from INT_ATRIB_TIPOPEDIDO IATP WITH (NOLOCK) where IA.ID=IATP.ATRIB)" & vbCrLf
sConsulta = sConsulta & "           UNION" & vbCrLf
sConsulta = sConsulta & "           SELECT DF.ID ID, DF.COD COD,  DF.DEN DEN, DF.TIPO TIPO, DF.INTRO INTRO, 0 INTERNO, IA.OBLIGATORIO OBLIGATORIO, DF.MINFEC VALORMINFEC,DF.MINNUM VALORMINNUM," & vbCrLf
sConsulta = sConsulta & "           DF.MAXFEC VALORMAXFEC,DF.MAXNUM VALORMAXNUM, IA.VALOR_TEXT, IA.VALOR_NUM, IA.VALOR_FEC, IA.VALOR_BOOL, CA.VALOR_NUM AS VALNUM,CA.VALOR_TEXT AS VALTEXT,CA.VALOR_FEC AS VALFEC,CA.VALOR_BOOL AS VALBOOL, 1 AS INTEGRACION" & vbCrLf
sConsulta = sConsulta & "           from INT_ATRIB IA WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN INT_ATRIB_TIPOPEDIDO IATP WITH (NOLOCK) ON IA.ID=IATP.ATRIB AND IATP.TIPOPEDIDO = @IDTIPOPEDIDO" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN DEF_ATRIB DF WITH (NOLOCK) ON IA.ATRIB=DF.ID" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN ERP ERP WITH(NOLOCK) on IA.ERP = ERP.COD" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN EMP EMP WITH(NOLOCK) on ERP.COD = EMP.ERP" & vbCrLf
sConsulta = sConsulta & "           LEFT OUTER JOIN CESTA_CABECERA_ATRIB CA WITH (NOLOCK) on CA.CESTACABECERAID = '+ convert(VARCHAR, @IDPEDIDO)  +' AND CA.ATRIB = DF.ID" & vbCrLf
sConsulta = sConsulta & "           where EMP.ID = ' + convert(VARCHAR, @EMPRESA)  +  ' AND IA.PEDIDO = 1 and (IA.AMBITO = 1 or IA.AMBITO = 2)';" & vbCrLf
sConsulta = sConsulta & "   End" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING =  @SQLSTRING+ N' UNION SELECT DF.ID ID, DF.COD COD,  DF.DEN DEN, DF.TIPO TIPO, DF.INTRO INTRO, TP.INTERNO INTERNO, TP.OBLIGATORIO OBLIGATORIO, DF.MINFEC VALORMINFEC,DF.MINNUM VALORMINNUM," & vbCrLf
sConsulta = sConsulta & "DF.MAXFEC VALORMAXFEC,DF.MAXNUM VALORMAXNUM, TP.VALOR_TEXT, TP.VALOR_NUM, TP.VALOR_FEC, TP.VALOR_BOOL, CA.VALOR_NUM AS VALNUM,CA.VALOR_TEXT AS VALTEXT,CA.VALOR_FEC AS VALFEC,CA.VALOR_BOOL AS VALBOOL, 0 AS INTEGRACION" & vbCrLf
sConsulta = sConsulta & "FROM TIPOPEDIDO_ATRIB TP WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN DEF_ATRIB DF WITH (NOLOCK) ON DF.ID=TP.ATRIB" & vbCrLf
sConsulta = sConsulta & "LEFT OUTER JOIN CESTA_CABECERA_ATRIB CA WITH (NOLOCK) on CA.CESTACABECERAID = '+ convert(VARCHAR, @IDPEDIDO)  +' AND CA.ATRIB = DF.ID" & vbCrLf
sConsulta = sConsulta & "WHERE TP.TIPOPEDIDO_ID= @IDTIPOPEDIDO  AND DF.TIPO = 1" & vbCrLf
sConsulta = sConsulta & ")A" & vbCrLf
sConsulta = sConsulta & "GROUP BY ID, COD,  DEN, TIPO, INTRO, INTERNO, OBLIGATORIO, VALORMINFEC,VALORMINNUM," & vbCrLf
sConsulta = sConsulta & "VALORMAXFEC,VALORMAXNUM, VALOR_TEXT, VALOR_NUM, VALOR_FEC, VALOR_BOOL , VALNUM,VALTEXT," & vbCrLf
sConsulta = sConsulta & "VALFEC,VALBOOL'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING =  @SQLSTRING1 + @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXECUTE sp_executesql @SQLSTRING, N'@IDTIPOPEDIDO INT', @IDTIPOPEDIDO = @IDTIPOPEDIDO;" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta



sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSEP_CARGAR_ATRIBUTOS_PEDIDO_INTEGRACION]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSEP_CARGAR_ATRIBUTOS_PEDIDO_INTEGRACION]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSEP_CARGAR_ATRIBUTOS_PEDIDO_INTEGRACION]" & vbCrLf
sConsulta = sConsulta & "@IDPEDIDO INT," & vbCrLf
sConsulta = sConsulta & "@IDTIPOPEDIDO INT," & vbCrLf
sConsulta = sConsulta & "@EMPRESA INT," & vbCrLf
sConsulta = sConsulta & "@INTEGRACION TINYINT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "If (@INTEGRACION = 1)" & vbCrLf
sConsulta = sConsulta & "   Begin" & vbCrLf
sConsulta = sConsulta & "        SET @SQLSTRING =   N'  SELECT DF.ID ID, DF.COD COD,  DF.DEN DEN, DF.TIPO TIPO, DF.INTRO INTRO, 0 INTERNO, IA.OBLIGATORIO OBLIGATORIO, DF.MINFEC VALORMINFEC,DF.MINNUM VALORMINNUM," & vbCrLf
sConsulta = sConsulta & "DF.MAXFEC VALORMAXFEC,DF.MAXNUM VALORMAXNUM, IA.VALOR_TEXT, IA.VALOR_NUM, IA.VALOR_FEC, IA.VALOR_BOOL, CA.VALOR_NUM AS VALNUM,CA.VALOR_TEXT AS VALTEXT,CA.VALOR_FEC AS VALFEC,CA.VALOR_BOOL AS VALBOOL, 1 AS INTEGRACION" & vbCrLf
sConsulta = sConsulta & "from INT_ATRIB IA WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN DEF_ATRIB DF WITH (NOLOCK) ON IA.ATRIB=DF.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN ERP ERP WITH(NOLOCK) on IA.ERP = ERP.COD" & vbCrLf
sConsulta = sConsulta & "INNER JOIN EMP EMP WITH(NOLOCK) on ERP.COD = EMP.ERP" & vbCrLf
sConsulta = sConsulta & "LEFT OUTER JOIN CESTA_CABECERA_ATRIB CA WITH(NOLOCK) on CA.CESTACABECERAID = '+ convert(VARCHAR, @IDPEDIDO)  +' AND CA.ATRIB = DF.ID" & vbCrLf
sConsulta = sConsulta & "where EMP.ID = ' + convert(VARCHAR, @EMPRESA)  +  ' AND IA.PEDIDO = 1 and (IA.AMBITO = 1 or IA.AMBITO = 2)" & vbCrLf
sConsulta = sConsulta & "                  AND IA.ID not in (SELECT ATRIB from INT_ATRIB_TIPOPEDIDO IATP WITH (NOLOCK) where IA.ID=IATP.ATRIB)" & vbCrLf
sConsulta = sConsulta & "                  UNION" & vbCrLf
sConsulta = sConsulta & "                  SELECT DF.ID ID, DF.COD COD,  DF.DEN DEN, DF.TIPO TIPO, DF.INTRO INTRO, 0 INTERNO, IA.OBLIGATORIO OBLIGATORIO, DF.MINFEC VALORMINFEC,DF.MINNUM VALORMINNUM," & vbCrLf
sConsulta = sConsulta & "DF.MAXFEC VALORMAXFEC,DF.MAXNUM VALORMAXNUM, IA.VALOR_TEXT, IA.VALOR_NUM, IA.VALOR_FEC, IA.VALOR_BOOL, CA.VALOR_NUM AS VALNUM,CA.VALOR_TEXT AS VALTEXT,CA.VALOR_FEC AS VALFEC,CA.VALOR_BOOL AS VALBOOL, 1 AS INTEGRACION" & vbCrLf
sConsulta = sConsulta & "from INT_ATRIB IA WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN INT_ATRIB_TIPOPEDIDO IATP WITH (NOLOCK) ON IA.ID=IATP.ATRIB AND IATP.TIPOPEDIDO = @IDTIPOPEDIDO" & vbCrLf
sConsulta = sConsulta & "INNER JOIN DEF_ATRIB DF WITH (NOLOCK) ON IA.ATRIB=DF.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN ERP ERP WITH(NOLOCK) on IA.ERP = ERP.COD" & vbCrLf
sConsulta = sConsulta & "INNER JOIN EMP EMP WITH(NOLOCK) on ERP.COD = EMP.ERP" & vbCrLf
sConsulta = sConsulta & "LEFT OUTER JOIN CESTA_CABECERA_ATRIB CA WITH(NOLOCK) on CA.CESTACABECERAID = '+ convert(VARCHAR, @IDPEDIDO)  +' AND CA.ATRIB = DF.ID" & vbCrLf
sConsulta = sConsulta & "where EMP.ID = ' + convert(VARCHAR, @EMPRESA)  +  ' AND IA.PEDIDO = 1 and (IA.AMBITO = 1 or IA.AMBITO = 2)';" & vbCrLf
sConsulta = sConsulta & "   End" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "EXECUTE sp_executesql @SQLSTRING, N'@IDTIPOPEDIDO INT', @IDTIPOPEDIDO = @IDTIPOPEDIDO;" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

End Sub



Public Function CodigoDeActualizacion31900_09_2143_A31900_09_2144() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim oADOConnection As ADODB.Connection
    Dim sConsulta As String

    On Error GoTo Error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Actualiza Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    frmProgreso.ProgressBar1.Refresh
    V_31900_9_Storeds_2144

    sConsulta = "UPDATE VERSION SET NUM ='3.00.21.044'" & ", FECHA=CONVERT(datetime,'" & Format(Now, "mm-dd-yyyy hh:mm:ss") & "',110), PRODVER='31900.9'"
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False

    CodigoDeActualizacion31900_09_2143_A31900_09_2144 = True
    Exit Function

Error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If

    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    CodigoDeActualizacion31900_09_2143_A31900_09_2144 = False
End Function

Private Sub V_31900_9_Storeds_2144()
    Dim sConsulta As String

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSEP_DEVOLVER_LINEAS_DE_ORDEN]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSEP_DEVOLVER_LINEAS_DE_ORDEN]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSEP_DEVOLVER_LINEAS_DE_ORDEN](@APROBADOR VARCHAR(50)=null, @ORDENID INTEGER = 0, @SOLICIT int = null ,@IDI varchar(20) , @MOSTRARCINTERNO tinyint = null, @PRES0 TINYINT=0, @PER VARCHAR(50)='')  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @PER<>''" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @ORDENID=0" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "IF @APROBADOR IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT LP.ID AS LINEAID, LP.ART_INT CODART, ISNULL(LP.DESCR_LIBRE,ITEM.DESCR)AS DENART," & vbCrLf
sConsulta = sConsulta & "LP.ART_INT + ' ' + ITEM.DESCR + LP.DESCR_LIBRE  AS ARTICULO, PA.COD_EXT ART_EXT," & vbCrLf
sConsulta = sConsulta & "ITEM.GMN1 + ART4.GMN1 GMN1, ITEM.GMN2 + ART4.GMN2 GMN2, ITEM.GMN3 + ART4.GMN3 GMN3, ITEM.GMN4 + ART4.GMN4 GMN4," & vbCrLf
sConsulta = sConsulta & "LP.PROVE, LP.DEST AS DESTINO, LP.UP AS UNIDAD, LP.PREC_UP AS PRECIOUNITARIO, LP.CANT_PED AS CANTIDAD," & vbCrLf
sConsulta = sConsulta & "LP.CANT_REC, ITEM_ADJ.CANT_ADJ / LP.FC AS CANTADJUDICADA, ITEM_ADJ.CANT_PED / LP.FC AS CANTCONSUMIDA," & vbCrLf
sConsulta = sConsulta & "CASE WHEN LP.TIPORECEPCION=0 THEN LP.PREC_UP * LP.CANT_PED ELSE LP.IMPORTE_PED END IMPORTE, LP.EST AS APROBADA, LP.FECEMISION AS FECHA," & vbCrLf
sConsulta = sConsulta & "GESTIONABLE=CASE WHEN APROB_LIM.PER= @APROBADOR   THEN 1 WHEN (APROB_LIM.PER IS NULL AND SEGURIDAD.APROB_TIPO1=@APROBADOR ) THEN 1  ELSE 0 END," & vbCrLf
sConsulta = sConsulta & "LP.CAT1, LP.CAT2, LP.CAT3, LP.CAT4, LP.CAT5, LP.OBS, COALESCE(C5.COD, C4.COD, C3.COD, C2.COD, C1.COD) AS CATEGORIA," & vbCrLf
sConsulta = sConsulta & "'(' + C1.COD + ') ' + C1.DEN + ISNULL(' - (' + C2.COD + ') ' + C2.DEN, '') + ISNULL(' - (' + C3.COD + ') ' + C3.DEN, '') + ISNULL(' - (' + C4.COD + ') ' + C4.DEN, '') + ISNULL(' - (' + C5.COD + ') ' + C5.DEN, '') AS CATEGORIARAMA," & vbCrLf
sConsulta = sConsulta & "ITEM.ANYO ANYOPROCE, ITEM.GMN1_PROCE, ITEM.PROCE CODPROCE, LE.COMENT, LPA.ADJUNTOS," & vbCrLf
sConsulta = sConsulta & "CASE WHEN @MOSTRARCINTERNO IS NULL OR @MOSTRARCINTERNO=1 OR CC1.INTERNO=0 THEN LP.VALOR1 ELSE NULL END VALOR1," & vbCrLf
sConsulta = sConsulta & "CASE WHEN @MOSTRARCINTERNO IS NULL OR @MOSTRARCINTERNO=1 OR CC2.INTERNO=0 THEN LP.VALOR2 ELSE NULL END VALOR2," & vbCrLf
sConsulta = sConsulta & "CC1.DEN AS DENCAMPO1, CC2.DEN AS DENCAMPO2, PROCE.DEN AS DENPROCE, LP.OBSADJUN, LP.FECENTREGA, LP.ENTREGA_OBL," & vbCrLf
sConsulta = sConsulta & "LP.PEDIDOABIERTO, LP.ORDEN, ISNULL(FRA.FACTURAS,0) AS TIENE_FRAS, ISNULL(PAG.PAGOS,0) AS TIENE_PAGOS," & vbCrLf
sConsulta = sConsulta & "CASE WHEN C5.COD IS NULL THEN" & vbCrLf
sConsulta = sConsulta & "CASE WHEN C4.COD IS NULL THEN" & vbCrLf
sConsulta = sConsulta & "CASE WHEN C3.COD IS NULL THEN" & vbCrLf
sConsulta = sConsulta & "CASE WHEN C2.COD IS NULL THEN" & vbCrLf
sConsulta = sConsulta & "CASE WHEN ISNULL(C1_ADJ.CAT1,0) = 0 THEN CASE WHEN ISNULL(C1.OBS,'0') = '0' THEN 0 ELSE 1 END ELSE 1 END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "CASE WHEN ISNULL(C2_ADJ.CAT1,0) = 0 THEN CASE WHEN ISNULL(C2.OBS,'0') = '0' THEN 0 ELSE 1 END ELSE 1 END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "CASE WHEN ISNULL(C3_ADJ.CAT1,0) = 0 THEN CASE WHEN ISNULL(C3.OBS,'0') = '0' THEN 0 ELSE 1 END ELSE 1 END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "CASE WHEN ISNULL(C4_ADJ.CAT1,0) = 0 THEN CASE WHEN ISNULL(C4.OBS,'0') = '0' THEN 0 ELSE 1 END ELSE 1 END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "CASE WHEN ISNULL(C5_ADJ.CAT1,0) = 0 THEN CASE WHEN ISNULL(C5.OBS,'0') = '0' THEN 0 ELSE 1 END ELSE 1 END" & vbCrLf
sConsulta = sConsulta & "END INFO_ADICIONAL," & vbCrLf
sConsulta = sConsulta & "UD.DEN AS UNIDEN," & vbCrLf
sConsulta = sConsulta & "CASE @IDI WHEN 'ENG' THEN D.DEN_ENG WHEN 'GER' THEN D.DEN_GER WHEN 'FRA' THEN D.DEN_FRA ELSE D.DEN_SPA END AS DESTINODEN," & vbCrLf
sConsulta = sConsulta & "LP.NUM,ISNULL(LPPLAN.HAYPLANES ,0) HAYPLANES,LP.PUB_PLAN_ENTREGA,ISNULL(CS.HAYCOSTES,0) HAYCOSTES," & vbCrLf
sConsulta = sConsulta & "ISNULL(DS.HAYDESCUENTOS,0) HAYDESCUENTOS,LP.COSTES,LP.DESCUENTOS,LP.TIPORECEPCION" & vbCrLf
sConsulta = sConsulta & "FROM LINEAS_PEDIDO LP WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN UNI U WITH (NOLOCK) ON U.COD=LP.UP" & vbCrLf
sConsulta = sConsulta & "INNER JOIN DEST D WITH (NOLOCK) ON D.COD=LP.DEST" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN APROB_LIM WITH (NOLOCK) ON APROB_LIM.ID=LP.APROB_LIM" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN SEGURIDAD WITH (NOLOCK) ON SEGURIDAD.ID=LP.SEGURIDAD" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN ITEM WITH (NOLOCK) ON ITEM.ANYO=LP.ANYO AND ITEM.GMN1_PROCE=LP.GMN1 AND ITEM.PROCE=LP.PROCE AND ITEM.ID=LP.ITEM" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN ART4 WITH (NOLOCK) ON ART4.COD=LP.ART_INT" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN ITEM_ADJ WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "ON ITEM_ADJ.ANYO=LP.ANYO" & vbCrLf
sConsulta = sConsulta & "AND ITEM_ADJ.GMN1=LP.GMN1" & vbCrLf
sConsulta = sConsulta & "AND ITEM_ADJ.PROCE=LP.PROCE" & vbCrLf
sConsulta = sConsulta & "AND ITEM_ADJ.ITEM=LP.ITEM" & vbCrLf
sConsulta = sConsulta & "AND ITEM_ADJ.PROVE=LP.PROVE" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PROVE_ART4 PA WITH (NOLOCK) ON PA.PROVE=LP.PROVE AND PA.ART=LP.ART_INT" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN CATN1 C1 WITH (NOLOCK) ON C1.ID=LP.CAT1" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN CATN2 C2 WITH (NOLOCK) ON C2.ID=LP.CAT2" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN CATN3 C3 WITH (NOLOCK) ON C3.ID=LP.CAT3" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN CATN4 C4 WITH (NOLOCK) ON C4.ID=LP.CAT4" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN CATN5 C5 WITH (NOLOCK) ON C5.ID=LP.CAT5" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN LINEAS_EST LE WITH (NOLOCK) ON LE.ID=LP.LINEAS_EST" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN" & vbCrLf
sConsulta = sConsulta & "(SELECT COUNT(*) AS ADJUNTOS, LINEA FROM LINEAS_PEDIDO_ADJUN WITH (NOLOCK) GROUP BY LINEAS_PEDIDO_ADJUN.LINEA) LPA ON LPA.LINEA=LP.ID" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN CATALOG_CAMPOS AS CC1 WITH (NOLOCK) ON CC1.ID=LP.CAMPO1 AND CC1.IDIOMA=@IDI" & vbCrLf
sConsulta = sConsulta & "AND CC1.INTERNO =  CASE WHEN @MOSTRARCINTERNO IS NULL OR @MOSTRARCINTERNO=1 THEN CC1.INTERNO ELSE 0 END" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN CATALOG_CAMPOS AS CC2 WITH (NOLOCK) ON CC2.ID=LP.CAMPO2 AND CC2.IDIOMA=@IDI" & vbCrLf
sConsulta = sConsulta & "AND CC2.INTERNO = CASE WHEN @MOSTRARCINTERNO IS NULL OR @MOSTRARCINTERNO=1 THEN CC2.INTERNO ELSE 0 END" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PROCE WITH (NOLOCK) ON PROCE.ANYO=LP.ANYO AND PROCE.GMN1=LP.GMN1 AND PROCE.COD=LP.PROCE" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN (SELECT COUNT(*) AS FACTURAS, LPF.LINEA_PEDIDO" & vbCrLf
sConsulta = sConsulta & "FROM LINEAS_PEDIDO_FACTURA LPF WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN LIN_FACTURA LF WITH (NOLOCK) ON LF.LINEA=LPF.LINEA_FACTURA" & vbCrLf
sConsulta = sConsulta & "GROUP BY LPF.LINEA_PEDIDO) FRA ON FRA.LINEA_PEDIDO = LP.ID" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN (SELECT COUNT(*) AS PAGOS, LPF.LINEA_PEDIDO FROM LINEAS_PEDIDO_FACTURA LPF WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN LIN_FACTURA LF WITH (NOLOCK) ON LF.LINEA = LPF.LINEA_FACTURA" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PAGO PAGO WITH (NOLOCK) ON PAGO.ID_FACTURA = LF.FACTURA" & vbCrLf
sConsulta = sConsulta & "GROUP BY LPF.LINEA_PEDIDO) PAG ON PAG.LINEA_PEDIDO = LP.ID" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN (SELECT DISTINCT CAT1 FROM CATN1_ADJUN WITH (NOLOCK)) C1_ADJ ON C1_ADJ.CAT1=LP.CAT1" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN (SELECT DISTINCT CAT1, CAT2 FROM CATN2_ADJUN WITH (NOLOCK)) C2_ADJ ON LP.CAT1 = C2_ADJ.CAT1 AND LP.CAT2 = C2_ADJ.CAT2" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN (SELECT DISTINCT CAT1, CAT2, CAT3 FROM CATN3_ADJUN WITH (NOLOCK)) C3_ADJ ON LP.CAT1 = C3_ADJ.CAT1 AND LP.CAT2 = C3_ADJ.CAT2 AND LP.CAT3 = C3_ADJ.CAT3" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN (SELECT DISTINCT CAT1, CAT2, CAT3, CAT4 FROM CATN4_ADJUN WITH (NOLOCK)) C4_ADJ ON LP.CAT1 = C4_ADJ.CAT1 AND LP.CAT2 = C4_ADJ.CAT2 AND LP.CAT3 = C4_ADJ.CAT3 AND LP.CAT4 = C4_ADJ.CAT4" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN (SELECT DISTINCT CAT1, CAT2, CAT3, CAT4, CAT5 FROM CATN5_ADJUN WITH (NOLOCK)) C5_ADJ ON LP.CAT1 = C5_ADJ.CAT1 AND LP.CAT2 = C5_ADJ.CAT2 AND LP.CAT3 = C5_ADJ.CAT3 AND LP.CAT4 = C5_ADJ.CAT4 AND LP.CAT5 = C5_ADJ.CAT5" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN UNI_DEN UD WITH (NOLOCK) ON UD.UNI = U.COD AND UD.IDIOMA = @IDI" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PEDIDO P WITH (NOLOCK) ON P.ID = LP.PEDIDO AND CASE WHEN @PER='' THEN '' ELSE P.PER END = CASE WHEN @PER='' THEN '' ELSE @PER END" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN (SELECT COUNT(1) AS HAYPLANES,LINEA_PEDIDO FROM LINEAS_PEDIDO_ENTREGAS LPE WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "GROUP BY LPE.LINEA_PEDIDO) LPPLAN ON LP.ID= LPPLAN.LINEA_PEDIDO" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN (SELECT COUNT(ID) HAYCOSTES,LPCD.LINEA FROM LINEAS_PEDIDO_CD LPCD WITH (NOLOCK) WHERE LPCD.TIPO=0 GROUP BY LPCD.LINEA) CS ON CS.LINEA=LP.ID" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN (SELECT COUNT(ID) HAYDESCUENTOS,LPCD.LINEA FROM LINEAS_PEDIDO_CD LPCD WITH (NOLOCK) WHERE LPCD.TIPO=1 GROUP BY LPCD.LINEA) DS ON DS.LINEA=LP.ID" & vbCrLf
sConsulta = sConsulta & "WHERE CASE WHEN @ORDENID = 0 THEN -1 ELSE LP.ORDEN END" & vbCrLf
sConsulta = sConsulta & "= CASE WHEN @ORDENID = 0 THEN -1 ELSE @ORDENID END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = N'SELECT DISTINCT LP.ID AS LINEAID, LP.ART_INT CODART" & vbCrLf
sConsulta = sConsulta & ", I.GMN1 + ART4.GMN1 AS GMN1, I.GMN2 + ART4.GMN2 AS GMN2, I.GMN3 + ART4.GMN3 AS GMN3, I.GMN4 + ART4.GMN4 AS GMN4" & vbCrLf
sConsulta = sConsulta & ", LP.PROVE, ISNULL(LP.DESCR_LIBRE,I.DESCR)AS DENART" & vbCrLf
sConsulta = sConsulta & ", PA.COD_EXT AS ART_EXT, LP.DEST AS DESTINO, LP.UP AS UNIDAD, LP.PREC_UP AS PRECIOUNITARIO, LP.CANT_PED AS CANTIDAD" & vbCrLf
sConsulta = sConsulta & ", LP.CANT_REC, IA.CANT_ADJ * LP.FC AS CANTADJUDICADA, CASE WHEN LP.TIPORECEPCION=0 THEN LP.PREC_UP * LP.CANT_PED ELSE LP.IMPORTE_PED END IMPORTE, ISNULL(LP.EST,1) AS APROBADA" & vbCrLf
sConsulta = sConsulta & ", LP.FECEMISION AS FECHA, LP.FECENTREGA, ISNULL(LP.EST,1) AS EST, LP.ENTREGA_OBL, LP.FECENTREGAPROVE" & vbCrLf
sConsulta = sConsulta & ", LP.CAT1, LP.CAT2, LP.CAT3, LP.CAT4, LP.CAT5, LP.OBS, COALESCE(C5.COD, C4.COD, C3.COD, C2.COD, C1.COD) AS CATEGORIA" & vbCrLf
sConsulta = sConsulta & ", ''('' + C1.COD + '') '' + C1.DEN + ISNULL('' - ('' + C2.COD + '') '' + C2.DEN, '''') + ISNULL('' - ('' + C3.COD + '') '' + C3.DEN, '''') + ISNULL('' - ('' + C4.COD + '') '' + C4.DEN, '''') + ISNULL('' - ('' + C5.COD + '') '' + C5.DEN, '''') AS CATEGORIARAMA" & vbCrLf
sConsulta = sConsulta & ", C1.DEN AS CAT1DEN, C2.DEN AS CAT2DEN, C3.DEN AS CAT3DEN, C4.DEN AS CAT4DEN, C5.DEN AS CAT5DEN, LE.COMENT" & vbCrLf
sConsulta = sConsulta & ", I.ANYO AS ANYOPROCE, I.GMN1_PROCE, I.PROCE AS CODPROCE, I.ID AS ITEM, LPA.ADJUNTOS" & vbCrLf
sConsulta = sConsulta & ", CASE WHEN @MOSTRARCINTERNO IS NULL OR @MOSTRARCINTERNO=1 OR CC1.INTERNO=0 THEN LP.VALOR1 ELSE NULL END AS VALOR1" & vbCrLf
sConsulta = sConsulta & ", CASE WHEN @MOSTRARCINTERNO IS NULL OR @MOSTRARCINTERNO=1 OR CC2.INTERNO=0 THEN LP.VALOR2 ELSE NULL END AS VALOR2" & vbCrLf
sConsulta = sConsulta & ", CC1.DEN AS DENCAMPO1, CC2.DEN AS DENCAMPO2, PROCE.DEN AS DENPROCE, LP.OBSADJUN, LP.PEDIDOABIERTO, LP.ORDEN" & vbCrLf
sConsulta = sConsulta & ", ISNULL(FRA.FACTURAS,0) AS TIENE_FRAS, ISNULL(PAG.PAGOS,0) AS TIENE_PAGOS" & vbCrLf
sConsulta = sConsulta & ", LP.ALMACEN, AL.DEN AS ALMACENDEN, ISNULL(ART4.ALMACENAR,0) AS ALMACENABLE" & vbCrLf
sConsulta = sConsulta & ", LP.ACTIVO, ACT.COD AS ACTIVO_COD, ACTDEN.DEN AS ACTIVO_DEN, ISNULL(ART4.CONCEPTO,2) AS CONCEPTO" & vbCrLf
sConsulta = sConsulta & ", CASE WHEN C5.COD IS NULL THEN" & vbCrLf
sConsulta = sConsulta & "CASE WHEN C4.COD IS NULL THEN" & vbCrLf
sConsulta = sConsulta & "CASE WHEN C3.COD IS NULL THEN" & vbCrLf
sConsulta = sConsulta & "CASE WHEN C2.COD IS NULL THEN" & vbCrLf
sConsulta = sConsulta & "CASE WHEN ISNULL(C1_ADJ.CAT1,0) = 0 THEN CASE WHEN ISNULL(C1.OBS,''0'') = ''0'' THEN 0 ELSE 1 END ELSE 1 END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "CASE WHEN ISNULL(C2_ADJ.CAT1,0) = 0 THEN CASE WHEN ISNULL(C2.OBS,''0'') = ''0'' THEN 0 ELSE 1 END ELSE 1 END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "CASE WHEN ISNULL(C3_ADJ.CAT1,0) = 0 THEN CASE WHEN ISNULL(C3.OBS,''0'') = ''0'' THEN 0 ELSE 1 END ELSE 1 END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "CASE WHEN ISNULL(C4_ADJ.CAT1,0) = 0 THEN CASE WHEN ISNULL(C4.OBS,''0'') = ''0'' THEN 0 ELSE 1 END ELSE 1 END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "CASE WHEN ISNULL(C5_ADJ.CAT1,0) = 0 THEN CASE WHEN ISNULL(C5.OBS,''0'') = ''0'' THEN 0 ELSE 1 END ELSE 1 END" & vbCrLf
sConsulta = sConsulta & "END AS INFO_ADICIONAL," & vbCrLf
sConsulta = sConsulta & "UD.DEN AS UNIDEN" & vbCrLf
sConsulta = sConsulta & ",D.DEN_' + @IDI + ' AS DESTDEN," & vbCrLf
sConsulta = sConsulta & "LP.NUM,ISNULL(LPPLAN.HAYPLANES ,0) HAYPLANES,LP.PUB_PLAN_ENTREGA,ISNULL(CS.HAYCOSTES,0) HAYCOSTES," & vbCrLf
sConsulta = sConsulta & "ISNULL(DS.HAYDESCUENTOS,0) HAYDESCUENTOS,LP.COSTES,LP.DESCUENTOS,LP.TIPORECEPCION '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @PRES0=1" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N' ,CSU.CENTRO_SM,CSD.DEN AS CCOSTE_DEN,ISNULL(LPI.PRES4,ISNULL(LPI.PRES3,ISNULL(LPI.PRES2,LPI.PRES1))) AS PRES,PRI.DEN AS PP_DEN '" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N' FROM LINEAS_PEDIDO LP  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN UNI U WITH (NOLOCK) ON U.COD=LP.UP" & vbCrLf
sConsulta = sConsulta & "INNER JOIN DEST D WITH (NOLOCK) ON D.COD=LP.DEST" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN ITEM I WITH (NOLOCK) ON I.ANYO=LP.ANYO AND I.GMN1_PROCE=LP.GMN1 AND I.PROCE=LP.PROCE AND I.ID=LP.ITEM" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN ART4 WITH (NOLOCK) ON ART4.COD=LP.ART_INT" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PROVE_ART4 PA WITH (NOLOCK) ON PA.PROVE=LP.PROVE AND PA.ART=LP.ART_INT" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN ITEM_ADJ IA WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "ON IA.ANYO=LP.ANYO" & vbCrLf
sConsulta = sConsulta & "AND IA.GMN1=LP.GMN1" & vbCrLf
sConsulta = sConsulta & "AND IA.PROCE=LP.PROCE" & vbCrLf
sConsulta = sConsulta & "AND IA.ITEM=LP.ITEM" & vbCrLf
sConsulta = sConsulta & "AND IA.PROVE=LP.PROVE" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN CATN1 C1 WITH (NOLOCK) ON C1.ID=LP.CAT1" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN CATN2 C2 WITH (NOLOCK) ON C2.ID=LP.CAT2" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN CATN3 C3 WITH (NOLOCK) ON C3.ID=LP.CAT3" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN CATN4 C4 WITH (NOLOCK) ON C4.ID=LP.CAT4" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN CATN5 C5 WITH (NOLOCK) ON C5.ID=LP.CAT5" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN LINEAS_EST LE WITH (NOLOCK) ON LE.ID=LP.LINEAS_EST" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN" & vbCrLf
sConsulta = sConsulta & "(SELECT COUNT(*) AS ADJUNTOS, LINEA" & vbCrLf
sConsulta = sConsulta & "FROM LINEAS_PEDIDO_ADJUN WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "GROUP BY LINEAS_PEDIDO_ADJUN.LINEA) LPA ON LPA.LINEA=LP.ID" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN CATALOG_CAMPOS AS CC1 WITH (NOLOCK) ON CC1.ID=LP.CAMPO1 AND CC1.IDIOMA=@IDI" & vbCrLf
sConsulta = sConsulta & "AND CC1.INTERNO = CASE WHEN @MOSTRARCINTERNO IS NULL OR @MOSTRARCINTERNO=1 THEN CC1.INTERNO ELSE 0 END" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN CATALOG_CAMPOS AS CC2 WITH (NOLOCK) ON CC2.ID=LP.CAMPO2 AND CC2.IDIOMA=@IDI" & vbCrLf
sConsulta = sConsulta & "AND CC2.INTERNO = CASE WHEN @MOSTRARCINTERNO IS NULL OR @MOSTRARCINTERNO=1 THEN CC2.INTERNO ELSE 0 END" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PROCE WITH (NOLOCK) ON PROCE.ANYO=LP.ANYO AND PROCE.GMN1=LP.GMN1 AND PROCE.COD=LP.PROCE" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN (SELECT COUNT(*) AS FACTURAS, LPF.LINEA_PEDIDO" & vbCrLf
sConsulta = sConsulta & "FROM LINEAS_PEDIDO_FACTURA LPF WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN LIN_FACTURA LF WITH (NOLOCK) ON LF.LINEA = LPF.LINEA_FACTURA" & vbCrLf
sConsulta = sConsulta & "GROUP BY LPF.LINEA_PEDIDO) FRA ON FRA.LINEA_PEDIDO = LP.ID" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN (SELECT COUNT(*) AS PAGOS, LPF.LINEA_PEDIDO" & vbCrLf
sConsulta = sConsulta & "FROM LINEAS_PEDIDO_FACTURA LPF WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN LIN_FACTURA LF WITH (NOLOCK) ON LF.LINEA = LPF.LINEA_FACTURA" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PAGO PAGO WITH (NOLOCK) ON PAGO.ID_FACTURA = LF.FACTURA" & vbCrLf
sConsulta = sConsulta & "GROUP BY LPF.LINEA_PEDIDO) PAG ON PAG.LINEA_PEDIDO = LP.ID" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN ALMACEN AL WITH (NOLOCK) ON AL.ID=LP.ALMACEN" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN ACTIVO ACT WITH (NOLOCK) ON ACT.ID=LP.ACTIVO" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN ACTIVO_DEN ACTDEN WITH (NOLOCK) ON ACTDEN.ACTIVO=LP.ACTIVO AND ACTDEN.IDI=@IDI" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN (SELECT DISTINCT CAT1 FROM CATN1_ADJUN WITH (NOLOCK)) C1_ADJ ON LP.CAT1 = C1_ADJ.CAT1" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN (SELECT DISTINCT CAT1, CAT2 FROM CATN2_ADJUN WITH (NOLOCK)) C2_ADJ ON LP.CAT1 = C2_ADJ.CAT1 AND LP.CAT2 = C2_ADJ.CAT2" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN (SELECT DISTINCT CAT1, CAT2, CAT3 FROM CATN3_ADJUN WITH (NOLOCK)) C3_ADJ ON LP.CAT1 = C3_ADJ.CAT1 AND LP.CAT2 = C3_ADJ.CAT2 AND LP.CAT3 = C3_ADJ.CAT3" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN (SELECT DISTINCT CAT1, CAT2, CAT3, CAT4 FROM CATN4_ADJUN WITH (NOLOCK)) C4_ADJ ON LP.CAT1 = C4_ADJ.CAT1 AND LP.CAT2 = C4_ADJ.CAT2 AND LP.CAT3 = C4_ADJ.CAT3 AND LP.CAT4 = C4_ADJ.CAT4" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN (SELECT DISTINCT CAT1, CAT2, CAT3, CAT4, CAT5 FROM CATN5_ADJUN WITH (NOLOCK)) C5_ADJ ON LP.CAT1 = C5_ADJ.CAT1 AND LP.CAT2 = C5_ADJ.CAT2 AND LP.CAT3 = C5_ADJ.CAT3 AND LP.CAT4 = C5_ADJ.CAT4 AND LP.CAT5 = C5_ADJ.CAT5" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN UNI_DEN UD WITH (NOLOCK) ON UD.UNI = U.COD AND UD.IDIOMA = @IDI" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PEDIDO P WITH (NOLOCK) ON P.ID = LP.PEDIDO AND CASE WHEN @PER='''' THEN '''' ELSE P.PER END = CASE WHEN @PER='''' THEN '''' ELSE @PER END  '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N' LEFT JOIN (SELECT COUNT(1) AS HAYPLANES,LINEA_PEDIDO FROM LINEAS_PEDIDO_ENTREGAS LPE WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       GROUP BY LPE.LINEA_PEDIDO) LPPLAN ON LP.ID= LPPLAN.LINEA_PEDIDO" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN (SELECT COUNT(ID) HAYCOSTES,LPCD.LINEA FROM LINEAS_PEDIDO_CD LPCD WITH (NOLOCK) WHERE LPCD.TIPO=0 GROUP BY LPCD.LINEA) CS ON CS.LINEA=LP.ID" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN (SELECT COUNT(ID) HAYDESCUENTOS,LPCD.LINEA FROM LINEAS_PEDIDO_CD LPCD WITH (NOLOCK) WHERE LPCD.TIPO=1 GROUP BY LPCD.LINEA) DS ON DS.LINEA=LP.ID '" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   IF @PRES0=1" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = @SQL + N' LEFT JOIN LINEAS_PED_IMPUTACION LPI WITH (NOLOCK) ON LPI.LINEA=LP.ID" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN CENTRO_SM_UON CSU WITH (NOLOCK) ON ISNULL(CSU.UON1,'''')=ISNULL(LPI.UON1,'''') AND ISNULL(CSU.UON2,'''')=ISNULL(LPI.UON2,'''') AND" & vbCrLf
sConsulta = sConsulta & "          ISNULL(CSU.UON3,'''')=ISNULL(LPI.UON3,'''') AND ISNULL(CSU.UON4,'''')=ISNULL(LPI.UON4,'''')" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN CENTRO_SM_DEN CSD WITH (NOLOCK) ON CSD.CENTRO_SM=CSU.CENTRO_SM AND CSD.IDI=@IDI" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN PRES5_IDIOMAS PRI WITH (NOLOCK) ON ISNULL(PRI.PRES0,'''')=ISNULL(LPI.PRES0,'''') AND ISNULL(PRI.PRES1,'''')=ISNULL(LPI.PRES1,'''') AND" & vbCrLf
sConsulta = sConsulta & "          ISNULL(PRI.PRES2,'''')=ISNULL(LPI.PRES2,'''') AND ISNULL(PRI.PRES3,'''')=ISNULL(LPI.PRES3,'''') AND ISNULL(PRI.PRES4,'''')=ISNULL(LPI.PRES4,'''')'" & vbCrLf
sConsulta = sConsulta & "               " & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N' WHERE CASE WHEN @ORDENID = 0 THEN -1 ELSE LP.ORDEN END" & vbCrLf
sConsulta = sConsulta & "           = CASE WHEN @ORDENID = 0 THEN -1 ELSE @ORDENID END" & vbCrLf
sConsulta = sConsulta & "       AND CASE WHEN LP.SOLICIT IS NULL AND @SOLICIT IS NULL THEN -1 ELSE LP.SOLICIT END" & vbCrLf
sConsulta = sConsulta & "           = CASE WHEN LP.SOLICIT IS NULL AND @SOLICIT IS NULL THEN -1 ELSE ISNULL(@SOLICIT,LP.SOLICIT) END'" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL,N'@ORDENID INTEGER, @SOLICIT INT, @IDI NVARCHAR(20), @MOSTRARCINTERNO TINYINT, @PER NVARCHAR(50)',@ORDENID=@ORDENID,@SOLICIT=@SOLICIT,@IDI=@IDI,@MOSTRARCINTERNO=@MOSTRARCINTERNO,@PER=@PER" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSGSW_DEVOLVER_DATOSADJUDICACION]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSGSW_DEVOLVER_DATOSADJUDICACION]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSGSW_DEVOLVER_DATOSADJUDICACION]    @ANYO INT, @GMN1 VARCHAR(50), @PROCE VARCHAR(50) AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @USAR_ART4_UON INT=0" & vbCrLf
sConsulta = sConsulta & "DECLARE @TRASPASO_PLANIF INT=0" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PARAM AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @USAR_ART4_UON=USAR_ART4_UON, @TRASPASO_PLANIF=TRASPASO_ART_PLANIF  FROM PARGEN_INTERNO WITH(NOLOCK) WHERE ID=1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL=N'" & vbCrLf
sConsulta = sConsulta & "DECLARE @TABLA_PROCE_INT AS TABLE (ID INT, ANYO INT, GMN1 NVARCHAR(50), PROCE INT, PROVE NVARCHAR(50), GRUPO NVARCHAR(10), ESTADO_ADJ INT, EMPRESA INT)" & vbCrLf
sConsulta = sConsulta & "INSERT INTO @TABLA_PROCE_INT SELECT ID, ANYO, GMN1,PROCE, PROVE, GRUPO, ESTADO_ADJ, EMPRESA FROM PROCE_INT WITH(NOLOCK) WHERE BAJA=0 AND ANYO=' + CAST(@ANYO AS NVARCHAR(4))  + ' AND GMN1=''' + @GMN1 + ''' AND PROCE=' + @PROCE  +' AND ITEM IS NULL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "select I.ANYO,I.GMN1_PROCE GMN1, I.PROCE,I.ID ITEM,ISNULL(A2.COD,I.ART) ART,I.GRUPO INTO #ITEMS_T from ITEM I WITH (NOLOCK) LEFT JOIN ART4 A2 WITH(NOLOCK) ON A2.ART=I.ART" & vbCrLf
sConsulta = sConsulta & "WHERE  i.anyo=' + CAST(@ANYO AS NVARCHAR(4)) +' and i.gmn1_proce=''' + @GMN1 + ''' and i.proce=' + @PROCE +'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT EMP.ID AS EMPCOD,EMP.NIF,EMP.DEN EMPDEN,EMP.ERP, PROVE.COD AS PROVECOD, PROVE.DEN AS PROVEDEN,PG.ID GRUPOID,PG.COD GRUPOCOD, PG.DEN GRUPODEN ," & vbCrLf
sConsulta = sConsulta & "PRI_E.ESTADO_ADJ EST_EMP,PRI_PV.ESTADO_ADJ EST_PROVE,PRI_G.ESTADO_ADJ EST_GRUPO" & vbCrLf
sConsulta = sConsulta & "FROM ITEM_ADJ WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN #ITEMS_T Z ON ITEM_ADJ.ANYO=Z.ANYO AND ITEM_ADJ.GMN1=Z.GMN1 AND ITEM_ADJ.PROCE=Z.PROCE AND ITEM_ADJ.ITEM=Z.ITEM" & vbCrLf
sConsulta = sConsulta & "INNER JOIN" & vbCrLf
sConsulta = sConsulta & "(select distinct iu.item,iu.uon1, null uon2,null uon3,u.empresa" & vbCrLf
sConsulta = sConsulta & "from item_uon1 iu WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "inner join uon1 u WITH(NOLOCK) on iu.uon1 = u.cod  where IU.ANYO=' + CAST(@ANYO AS NVARCHAR(4)) +' AND IU.GMN1=''' + @GMN1 + ''' AND IU.PROCE=' + @PROCE +'" & vbCrLf
sConsulta = sConsulta & "Union" & vbCrLf
sConsulta = sConsulta & "select distinct iu.item,iu.uon1, IU.uon2 ,null uon3,ISNULL(u1.empresa,u2.empresa) as empresa" & vbCrLf
sConsulta = sConsulta & "from item_uon2 iu WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "inner join uon2 u2 WITH(NOLOCK) on iu.uon1 = u2.uon1 and iu.uon2=u2.cod" & vbCrLf
sConsulta = sConsulta & "inner join uon1 u1 WITH(NOLOCK) on u1.cod = u2.uon1  where IU.ANYO=' + CAST(@ANYO AS NVARCHAR(4)) +' AND IU.GMN1=''' + @GMN1 + ''' AND IU.PROCE=' + @PROCE +'" & vbCrLf
sConsulta = sConsulta & "Union select distinct iu.item,iu.uon1, IU.uon2 ,IU.uon3,ISNULL(u3.empresa,ISNULL(u2.empresa,u1.empresa)) as empresa" & vbCrLf
sConsulta = sConsulta & "from item_uon3 iu WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "inner join uon3 u3 WITH(NOLOCK) on iu.uon1 = u3.uon1 and iu.uon2=u3.uon2 and iu.uon3=u3.cod" & vbCrLf
sConsulta = sConsulta & "inner join uon2 u2 WITH(NOLOCK) on u3.uon1 = u2.uon1 and u3.uon2=u2.cod" & vbCrLf
sConsulta = sConsulta & "inner join uon1 u1 WITH(NOLOCK) on u1.cod = u3.uon1  where IU.ANYO=' + CAST(@ANYO AS NVARCHAR(4)) +' AND IU.GMN1=''' + @GMN1 + ''' AND IU.PROCE=' + @PROCE +'" & vbCrLf
sConsulta = sConsulta & ") IT_EMP ON IT_EMP.ITEM=ITEM_ADJ.ITEM" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INNER JOIN ART4_UON WITH (NOLOCK) ON ART4_UON.ART4 =Z.ART AND ART4_UON.UON1=IT_EMP.UON1 AND COALESCE(ART4_UON.UON2,IT_EMP.UON2,'''')=COALESCE(IT_EMP.UON2,ART4_UON.UON2,'''')" & vbCrLf
sConsulta = sConsulta & "AND COALESCE(ART4_UON.UON3,IT_EMP.UON3,'''')=COALESCE(IT_EMP.UON3,ART4_UON.UON3,'''')'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @TRASPASO_PLANIF >0" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + N' INNER JOIN ART4_UON_ATRIB AUA WITH(NOLOCK) ON AUA.ART=ART4_UON.ART4 AND ART4_UON.UON1=AUA.UON1 AND COALESCE(ART4_UON.UON2,AUA.UON2,'''')=COALESCE(AUA.UON2,ART4_UON.UON2,'''') AND COALESCE(ART4_UON.UON3,AUA.UON3,'''')=COALESCE(AUA.UON3,ART4_UON.UON3,'''') AND ATRIB=' + CAST(@TRASPASO_PLANIF AS VARCHAR(10)) +' AND VALOR_BOOL=1 '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + N'INNER JOIN PROVE WITH(NOLOCK) ON ITEM_ADJ.PROVE=PROVE.COD" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PROCE_GRUPO PG WITH (NOLOCK) ON Z.ANYO=PG.ANYO AND Z.GMN1 =PG.GMN1 AND Z.PROCE=PG.PROCE AND Z.GRUPO=PG.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN EMP WITH(NOLOCK) ON IT_EMP.EMPRESA=EMP.ID --AND ERP=1  --erp=1 = SI tiene integraci?n (ERP=0 NO tiene integracion)" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN @TABLA_PROCE_INT PRI_E ON PRI_E.ANYO =ITEM_ADJ.ANYO AND PRI_E.GMN1=ITEM_ADJ.GMN1 AND PRI_E.PROCE=ITEM_ADJ.PROCE AND PRI_E.EMPRESA=EMP.ID AND PRI_E.PROVE IS NULL AND PRI_E.GRUPO IS NULL" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN @TABLA_PROCE_INT PRI_PV ON PRI_PV.ANYO =ITEM_ADJ.ANYO AND PRI_PV.GMN1=ITEM_ADJ.GMN1 AND PRI_PV.PROCE=ITEM_ADJ.PROCE AND PRI_PV.PROVE=ITEM_ADJ.PROVE AND PRI_PV.EMPRESA=EMP.ID AND PRI_PV.GRUPO IS NULL" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN @TABLA_PROCE_INT PRI_G ON PRI_G.ANYO =ITEM_ADJ.ANYO AND PRI_G.GMN1=ITEM_ADJ.GMN1 AND PRI_G.PROCE=ITEM_ADJ.PROCE AND PRI_G.PROVE=ITEM_ADJ.PROVE AND PRI_G.GRUPO=PG.ID AND PRI_G.EMPRESA=EMP.ID" & vbCrLf
sConsulta = sConsulta & "WHERE ITEM_ADJ.ANYO=' + CAST(@ANYO AS NVARCHAR(4)) +' AND ITEM_ADJ.GMN1=''' + @GMN1 + ''' AND ITEM_ADJ.PROCE=' + @PROCE +' and ITEM_ADJ.PORCEN>0" & vbCrLf
sConsulta = sConsulta & "GROUP BY PROVE.COD, PROVE.DEN, EMP.ID,EMP.DEN,EMP.NIF,EMP.ERP,PG.ID,PG.COD,PG.DEN,PRI_PV.ESTADO_ADJ ,PRI_G.ESTADO_ADJ,PRI_E.ESTADO_ADJ'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--SET @PARAM =N'' + CAST(@ANYO AS NVARCHAR(4)) +' INT, ''' + @GMN1 + ''' VARCHAR(50), ' + @PROCE +' VARCHAR(50),@TRASPASO_PLANIF INT'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL--,@PARAM,' + CAST(@ANYO AS NVARCHAR(4)) +'=' + CAST(@ANYO AS NVARCHAR(4)) +',''' + @GMN1 + '''=''' + @GMN1 + ''',' + @PROCE +'=' + @PROCE +',@TRASPASO_PLANIF =@TRASPASO_PLANIF" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSGSW_DEVOLVER_ITEMSGRUPO]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSGSW_DEVOLVER_ITEMSGRUPO]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSGSW_DEVOLVER_ITEMSGRUPO] @ANYO INT, @GMN1 VARCHAR(50), @PROCE INT, @CODGRUPO VARCHAR(20),@PROVE VARCHAR(50),@IDEMP INT AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @EST INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PARAM AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @ERP INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ESCALADOS INT=0" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDGRUPO INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @USAR_ART4_UON INT=0" & vbCrLf
sConsulta = sConsulta & "DECLARE @TRASPASO_PLANIF INT=0" & vbCrLf
sConsulta = sConsulta & "DECLARE @USAR_ORGCOMPRAS INT =0" & vbCrLf
sConsulta = sConsulta & "DECLARE @DISTR AS BIT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO_L INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN1_L VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCE_L INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @CODGRUPO_L VARCHAR(20)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVE_L VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDEMP_L INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @ANYO_L =@ANYO" & vbCrLf
sConsulta = sConsulta & "SET @GMN1_L =@GMN1" & vbCrLf
sConsulta = sConsulta & "SET @PROCE_L =@PROCE" & vbCrLf
sConsulta = sConsulta & "SET @CODGRUPO_L =@CODGRUPO" & vbCrLf
sConsulta = sConsulta & "SET @PROVE_L =@PROVE" & vbCrLf
sConsulta = sConsulta & "SET @IDEMP_L =@IDEMP" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @ESCALADOS=COUNT(ID) FROM PROCE_GRUPO PG WITH (NOLOCK) WHERE PG.ANYO=@ANYO_L  AND PG.GMN1=@GMN1_L AND PG.PROCE=@PROCE_L AND PG.COD=@CODGRUPO_L AND PG.ESCALADOS=1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @EST=EST FROM PROCE P WITH (NOLOCK) WHERE ANYO =@ANYO_L AND COD=@PROCE_L AND GMN1=@GMN1_L" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @ERP=ES.ERP FROM ERP_SOCIEDAD ES WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN EMP E WITH (NOLOCK) ON E.SOCIEDAD=ES.SOCIEDAD AND E.ID=@IDEMP_L" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @IDGRUPO=ID FROM PROCE_GRUPO PG WITH (NOLOCK) WHERE PG.ANYO=@ANYO_L AND PG.GMN1=@GMN1_L AND PG.PROCE=@PROCE_L AND PG.COD=@CODGRUPO_L" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @USAR_ART4_UON=USAR_ART4_UON, @TRASPASO_PLANIF=TRASPASO_ART_PLANIF, @USAR_ORGCOMPRAS=USAR_ORGCOMPRAS  FROM PARGEN_INTERNO WITH(NOLOCK) WHERE ID=1" & vbCrLf
sConsulta = sConsulta & "SELECT @DISTR=(CASE WHEN Z.NUM=0 THEN 0 ELSE 1 END) FROM (SELECT COUNT(PROVEP) NUM FROM PROVE_REL PR WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN GS_PROVETREL GP WITH (NOLOCK) ON PR.PROVETREL=GP.ID AND PR.PROVEP=@PROVE_L AND GP.PEDIDO=1) Z" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL='SELECT A.*, '" & vbCrLf
sConsulta = sConsulta & "IF @USAR_ORGCOMPRAS =1" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + 'CASE WHEN A.ACCION=''D'' THEN IT_CEN.CENTRO ELSE CASE WHEN A.NUM_CENTROS=1 THEN COALESCE(CENTRO_LIA,it_cen.CENTRO,'''') ELSE '''' END END CENTROS,'''' CENTRO '" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + ''''' CENTROS,'''' AS CENTRO '" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + 'FROM (SELECT DISTINCT '" & vbCrLf
sConsulta = sConsulta & "IF @DISTR=1" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + ' COALESCE(LIA2.ID,LIA1.ID) LIA_ID,'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + ' LIA1.ID LIA_ID,'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' I.ID ID_ITEM,I.ART CODART,I.DESCR DENART,I.UNI'" & vbCrLf
sConsulta = sConsulta & "if  @ESCALADOS>0" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + ',PGE.ID ESC_ID,PGE.INICIO,PGE.FIN'" & vbCrLf
sConsulta = sConsulta & "else" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + ',IA.CANT_ADJ*IT_EMP.PORCEN CANT_ADJ'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ',IOF.PREC_VALIDO PREC_ADJ,P.MON,'" & vbCrLf
sConsulta = sConsulta & "IF @USAR_ORGCOMPRAS =1  --Si el ?ltimo traspaso es un borrado, lee lo que haya en el proceso. Si no (era una I), lee lo de log_item_adj" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + 'COUNT(IT_CEN.CENTRO) NUM_CENTROS ,'" & vbCrLf
sConsulta = sConsulta & "-- ELSE" & vbCrLf
sConsulta = sConsulta & "--SET @SQL=@SQL + ''''' CENTROS,'''' AS CENTRO,'" & vbCrLf
sConsulta = sConsulta & "IF @DISTR=1" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + ' COALESCE(LIA2.CENTRO,LIA1.CENTRO) CENTRO_LIA, COALESCE(LIA2.ACCION,LIA1.ACCION) ACCION" & vbCrLf
sConsulta = sConsulta & "   ,CASE WHEN COALESCE(LIA2.ACCION,LIA1.ACCION) = ''D'' AND LG.ESTADO=4  THEN NULL ELSE LG.ESTADO END AS ESTADOINT" & vbCrLf
sConsulta = sConsulta & "   ,CASE WHEN COALESCE(LIA2.ACCION,LIA1.ACCION) = ''D'' AND LG.ESTADO=4 THEN NULL ELSE LG.FECENV END FECENV" & vbCrLf
sConsulta = sConsulta & "   ,CASE WHEN COALESCE(LIA2.ACCION,LIA1.ACCION) = ''D'' AND LG.ESTADO=4 THEN NULL ELSE LIA1.USU  END USU'" & vbCrLf
sConsulta = sConsulta & "else" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + ' LIA1.CENTRO CENTRO_LIA,LIA1.ACCION ACCION ,CASE WHEN LIA1.ACCION = ''D'' AND LG.ESTADO=4  THEN NULL ELSE LG.ESTADO END AS ESTADOINT" & vbCrLf
sConsulta = sConsulta & "   ,CASE WHEN LIA1.ACCION= ''D'' AND LG.ESTADO=4 THEN NULL ELSE LG.FECENV END FECENV" & vbCrLf
sConsulta = sConsulta & "   ,CASE WHEN LIA1.ACCION = ''D'' AND LG.ESTADO=4 THEN NULL ELSE LIA1.USU  END USU'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ', I.FECINI FECINI_SUM,I.FECFIN FECFIN_SUM,PE.COD_ERP PROVE_ERP,PE.DEN_ERP," & vbCrLf
sConsulta = sConsulta & "COALESCE(IT_ATRIB1.INT_ATRIB_ID,IAE.ATRIB,PGA.ATRIB,PATR.ATRIB,PA.ATRIB,DA.ID) ATRIBID," & vbCrLf
sConsulta = sConsulta & "DA.COD ATRIBCOD,DA.DEN ATRIBDEN," & vbCrLf
sConsulta = sConsulta & "COALESCE(IT_ATRIB1.VALOR_TEXT,IAE.VALOR_TEXT,PGA.VALOR_TEXT,PATR.VALOR_TEXT,OA.VALOR_TEXT,OGATR.VALOR_TEXT,OATR.VALOR_TEXT,IAT.VALOR_TEXT) ATRIBVALOR_TEXT ," & vbCrLf
sConsulta = sConsulta & "COALESCE(IT_ATRIB1.VALOR_NUM ,IAE.VALOR_NUM,PGA.VALOR_NUM,PATR.VALOR_NUM,OA.VALOR_NUM,OGATR.VALOR_NUM,OATR.VALOR_NUM,IAT.VALOR_NUM) ATRIBVALOR_NUM," & vbCrLf
sConsulta = sConsulta & "COALESCE(IT_ATRIB1.VALOR_FEC ,IAE.VALOR_FEC,PGA.VALOR_FEC,PATR.VALOR_FEC,OA.VALOR_FEC,OGATR.VALOR_FEC,OATR.VALOR_FEC,IAT.VALOR_FEC) ATRIBVALOR_FEC," & vbCrLf
sConsulta = sConsulta & "COALESCE(IT_ATRIB1.VALOR_BOOL,IAE.VALOR_BOOL,PGA.VALOR_BOOL,PATR.VALOR_BOOL,OA.VALOR_BOOL,OGATR.VALOR_BOOL,OATR.VALOR_BOOL,IAT.VALOR_BOOL) ATRIBVALOR_BOOL," & vbCrLf
sConsulta = sConsulta & "DA.TIPO ATRIBTIPO,IAT.OBLIGATORIO ATRIBOBLIGATORIO,'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @USAR_ORGCOMPRAS=1" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + 'IT_EMP.UON1 UON1,IT_EMP.UON2 UON2,IT_EMP.UON3 UON3, '" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + 'NULL AS UON1,NULL AS UON2, NULL AS UON3,'" & vbCrLf
sConsulta = sConsulta & "IF @DISTR=1" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + '1 DISTRIBUIDOR'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + '0 DISTRIBUIDOR'" & vbCrLf
sConsulta = sConsulta & "IF @ESCALADOS>0" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + ',1 ESCALADOS,PG.ESCALAUNI '" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + ',0 ESCALADOS '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--------FROM--------------------------------------------" & vbCrLf
sConsulta = sConsulta & "--Parte de los art centrales, meto en una temporal para agilizar" & vbCrLf
sConsulta = sConsulta & "SELECT I.ANYO,I.GMN1_PROCE,I.PROCE,I.ID ,ISNULL(A2.COD,I.ART) ART,I.EST,I.GRUPO,ISNULL(A2.DEN,I.DESCR) DESCR,I.UNI,I.FECINI,I.FECFIN INTO #ITEMS_T from ITEM I WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                           LEFT JOIN ART4 A2 WITH(NOLOCK) ON A2.ART=I.ART WHERE  I.ANYO=@ANYO_L AND I.GMN1_PROCE=@GMN1_L AND I.PROCE=@PROCE_L AND I.GRUPO=@IDGRUPO" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' from #ITEMS_T I '" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' INNER JOIN  PROCE P WITH (NOLOCK) ON I.ANYO=P.ANYO AND I.GMN1_PROCE=P.GMN1 AND I.PROCE=P.COD '" & vbCrLf
sConsulta = sConsulta & "IF @EST=11 --Si el proceso esta parcialmente cerrado" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + ' AND I.EST=1 '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ESCALADOS>0 --Recoge los datos de adjudicaciones de las tablas de escalados" & vbCrLf
sConsulta = sConsulta & "   SET @SQL =@SQL + 'INNER JOIN ITEM_ADJESC IA WITH (NOLOCK) ON IA.ANYO=I.ANYO AND IA.GMN1=I.GMN1_PROCE AND IA.PROCE=I.PROCE AND IA.ITEM=I.ID AND IA.PROVE=@PROVE_L" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PROCE_GR_ESC PGE WITH (NOLOCK) ON PGE.ANYO=I.ANYO AND PGE.GMN1=I.GMN1_PROCE AND PGE.PROCE=I.PROCE AND PGE.GRUPO =PG.ID AND PGE.ID=IA.ESC '" & vbCrLf
sConsulta = sConsulta & "ELSE -- Recoge la adjudicaci?n" & vbCrLf
sConsulta = sConsulta & "   SET @SQL =@SQL + 'INNER JOIN ITEM_ADJ IA WITH (NOLOCK) ON I.ANYO=IA.ANYO AND I.GMN1_PROCE=IA.GMN1 AND I.PROCE=IA.PROCE AND I.ID=IA.ITEM AND IA.PROVE=@PROVE_L '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "---Obtiene las empresas a las que se ha distribuido la compra" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL + 'INNER JOIN" & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   SELECT DISTINCT IT_EMP_AUX.ANYO,IT_EMP_AUX.GMN1,IT_EMP_AUX.PROCE,ITEM,UON1,UON2,UON3, IT_EMP_AUX.EMPRESA, IT_EMP_AUX.PORCEN, IT_EMP_AUX.CENTRO , LIA.ID IT_EMP_LIAID" & vbCrLf
sConsulta = sConsulta & "   FROM (" & vbCrLf
sConsulta = sConsulta & "       SELECT DISTINCT IU.ANYO,IU.GMN1,IU.PROCE,iu.ITEM,IU.UON1, IU.UON2, IU.UON3,ISNULL(u3.empresa,ISNULL(u2.empresa,u1.empresa)) as empresa,iu.porcen,ISNULL(U3.CENTROS,ISNULL(U2.CENTROS,U1.CENTROS)) CENTRO" & vbCrLf
sConsulta = sConsulta & "       FROM VW_DIST_ITEM IU WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN ART4_UON AU WITH(NOLOCK) ON IU.UON1=AU.UON1 AND COALESCE(AU.UON2,IU.UON2,'''')=COALESCE(IU.UON2,AU.UON2,'''') AND COALESCE(AU.UON3,IU.UON3,'''')=COALESCE(IU.UON3,AU.UON3,'''')" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN ITEM I WITH(NOLOCK) ON I.ANYO=IU.ANYO AND I.GMN1_PROCE=IU.GMN1 AND I.PROCE=IU.PROCE AND I.ID=IU.ITEM AND I.ART=AU.ART4" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN UON1 U1 WITH(NOLOCK) ON AU.UON1=U1.cod" & vbCrLf
sConsulta = sConsulta & "       left JOIN UON2 U2 WITH(NOLOCK) ON AU.UON1=U2.UON1 AND COALESCE(U2.COD,AU.UON2,'''')=COALESCE(AU.UON2,U2.COD,'''')" & vbCrLf
sConsulta = sConsulta & "       left JOIN UON3 U3 WITH(NOLOCK) ON AU.UON1=U3.UON1 AND COALESCE(U3.UON2,AU.UON2,'''')=COALESCE(AU.UON2,U3.UON2,'''') AND COALESCE(U3.COD,AU.UON3,'''')=COALESCE(AU.UON3,U3.COD,'''')" & vbCrLf
sConsulta = sConsulta & "       where IU.ANYO=@ANYO_L AND IU.GMN1=@GMN1_L AND IU.PROCE=@PROCE_L" & vbCrLf
sConsulta = sConsulta & "       ) IT_EMP_AUX" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN LOG_ITEM_ADJ LIA WITH (NOLOCK) ON (LIA.ORIGEN=0 OR LIA.ORIGEN=2) AND LIA.ULTIMO=1" & vbCrLf
sConsulta = sConsulta & "   AND LIA.ANYO=IT_EMP_AUX.ANYO AND LIA.GMN1_PROCE=IT_EMP_AUX.GMN1 AND LIA.PROCE=IT_EMP_AUX.PROCE AND LIA.ID_ITEM=IT_EMP_AUX.ITEM AND LIA.PROVEPADRE=@PROVE_L" & vbCrLf
sConsulta = sConsulta & "   WHERE" & vbCrLf
sConsulta = sConsulta & "   CASE WHEN LIA.ID IS NULL THEN ISNULL(IT_EMP_AUX.CENTRO,'''') ELSE case when LIA.CENTRO is null or lia.centro='''' then IT_EMP_AUX.CENTRO else  LIA.CENTRO end  END = ISNULL(IT_EMP_AUX.CENTRO,'''')" & vbCrLf
sConsulta = sConsulta & "   ) IT_EMP ON IT_EMP.ITEM=IA.ITEM AND IT_EMP.EMPRESA=@IDEMP_L" & vbCrLf
sConsulta = sConsulta & "   '" & vbCrLf
sConsulta = sConsulta & "--Si los art?culos est?n vinculados a las unidades organizativas, solamente aparecen las empresas a las que pertenece el item (unidades organizativas comunes entre el art?culo y la distribuci?n del item)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' INNER JOIN ART4_UON ARTUON WITH(NOLOCK) ON ARTUON.UON1=IT_EMP.UON1" & vbCrLf
sConsulta = sConsulta & "AND COALESCE(ARTUON.UON2,IT_EMP.UON2,'''')=COALESCE(IT_EMP.UON2,ARTUON.UON2,'''')" & vbCrLf
sConsulta = sConsulta & "AND COALESCE(ARTUON.UON3,IT_EMP.UON3,'''')=COALESCE(IT_EMP.UON3,ARTUON.UON3,'''')" & vbCrLf
sConsulta = sConsulta & "AND ARTUON.ART4=I.ART '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @USAR_ORGCOMPRAS=1" & vbCrLf
sConsulta = sConsulta & "begin" & vbCrLf
sConsulta = sConsulta & "   SET @SQL =@SQL + 'LEFT JOIN(" & vbCrLf
sConsulta = sConsulta & "   select distinct au.art4,au.uon1,aU.uon2,aU.uon3,ISNULL(u3.empresa,ISNULL(u2.empresa,u1.empresa)) as empresa,ISNULL(u3.CENTROS,ISNULL(u2.CENTROS,u1.CENTROS)) CENTRO" & vbCrLf
sConsulta = sConsulta & "   from art4_uon au WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   inner join uon1 u1 WITH(NOLOCK) on u1.cod=AU.uon1" & vbCrLf
sConsulta = sConsulta & "   LEFT join uon2 u2 WITH(NOLOCK) on AU.uon1=u2.uon1 and AU.uon2=u2.cod" & vbCrLf
sConsulta = sConsulta & "   LEFT join uon3 u3 WITH(NOLOCK) on au.uon1=u3.uon1 and au.uon2=u3.uon2 and au.uon3=u3.cod'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @SQL =@SQL + ') IT_CEN ON I.ART=IT_CEN.ART4 AND IT_CEN.EMPRESA=@IDEMP_L" & vbCrLf
sConsulta = sConsulta & "   AND IT_CEN.UON1=IT_EMP.UON1 AND COALESCE(IT_CEN.UON2,IT_EMP.uon2,artuon.UON2)=COALESCE(IT_EMP.uon2,artuon.UON2,IT_CEN.UON2)" & vbCrLf
sConsulta = sConsulta & "   AND COALESCE(IT_CEN.UON3,IT_EMP.uon3,artuon.UON3)=COALESCE(IT_EMP.uon3,artuon.UON3,IT_CEN.UON3)'" & vbCrLf
sConsulta = sConsulta & "end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @TRASPASO_PLANIF>0" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL+ ' INNER JOIN ART4_UON_ATRIB AUA WITH (NOLOCK) ON ARTUON.ART4=AUA.ART" & vbCrLf
sConsulta = sConsulta & "   AND AUA.UON1=ARTUON.UON1 AND COALESCE(AUA.UON2,ARTUON.UON2,'''')=COALESCE(ARTUON.UON2,AUA.UON2,'''')" & vbCrLf
sConsulta = sConsulta & "   AND COALESCE(AUA.UON3,ARTUON.UON3,'''')=COALESCE(ARTUON.UON3,AUA.UON3,'''')" & vbCrLf
sConsulta = sConsulta & "   AND AUA.ATRIB=@TRASPASO_PLANIF AND AUA.VALOR_BOOL=1  '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ESCALADOS>0 --Recoge las ofertas de la tabla de escalados" & vbCrLf
sConsulta = sConsulta & "   SET @SQL =@SQL + 'INNER JOIN ITEM_OFEESC IOF WITH (NOLOCK) ON IA.ANYO =IOF.ANYO AND IA.GMN1 =IOF.GMN1 AND IA.PROCE =IOF.PROCE AND IA.ITEM =IOF.ITEM AND IA.PROVE=IOF.PROVE AND IA.OFE =IOF.OFE AND IOF.ESC=PGE.ID '" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + 'INNER JOIN ITEM_OFE IOF WITH (NOLOCK) ON IA.ANYO =IOF.ANYO AND IA.GMN1 =IOF.GMN1 AND IA.PROCE =IOF.PROCE AND IA.ITEM =IOF.ITEM AND IA.PROVE=IOF.PROVE AND IA.OFE =IOF.NUM '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "---Datos de integraci?n:" & vbCrLf
sConsulta = sConsulta & "IF @DISTR=1  --Con distribuidores" & vbCrLf
sConsulta = sConsulta & "   SET @SQL= @SQL + ' LEFT JOIN LOG_ITEM_ADJ LIA2 WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   ON I.ANYO=LIA2.ANYO AND I.GMN1_PROCE=LIA2.GMN1_PROCE AND I.PROCE =LIA2.PROCE AND LIA2.PROVEPADRE=@PROVE_L AND LIA2.PROVE<>@PROVE_L  AND I.ID=LIA2.ID_ITEM AND (LIA2.ORIGEN=0 OR LIA2.ORIGEN=2) AND LIA2.ULTIMO=1'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL= @SQL + '" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN LOG_ITEM_ADJ LIA1 WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "ON I.ANYO=LIA1.ANYO AND I.GMN1_PROCE=LIA1.GMN1_PROCE AND I.PROCE =LIA1.PROCE AND LIA1.PROVEPADRE=@PROVE_L AND LIA1.PROVE=@PROVE_L AND I.ID=LIA1.ID_ITEM AND (LIA1.ORIGEN=0 OR LIA1.ORIGEN=2) AND LIA1.ULTIMO=1'" & vbCrLf
sConsulta = sConsulta & "IF @DISTR=1  --Con distribuidores" & vbCrLf
sConsulta = sConsulta & "   SET @SQL= @SQL + ' LEFT JOIN LOG_GRAL LG WITH (NOLOCK) ON LG.ID_TABLA=COALESCE(LIA2.ID,LIA1.ID) AND LG.TABLA=9" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN PROVE_ERP PE WITH (NOLOCK) ON COALESCE(LIA2.PROVE_ERP,LIA1.PROVE_ERP)=PE.COD_ERP AND PE.EMPRESA=@IDEMP_L'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   SET @SQL= @SQL + ' LEFT JOIN LOG_GRAL LG WITH (NOLOCK) ON LG.ID_TABLA=LIA1.ID AND LG.TABLA=9" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN PROVE_ERP PE WITH (NOLOCK) ON LIA1.PROVE_ERP=PE.COD_ERP AND PE.EMPRESA=@IDEMP_L'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL + ' LEFT JOIN INT_ATRIB IAT WITH(NOLOCK) ON IAT.ADJUDICACION=1 AND IAT.AMBITO=3 AND IAT.ERP=@ERP" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN DEF_ATRIB DA WITH (NOLOCK) ON DA.ID=IAT.ATRIB  AND IAT.ADJUDICACION=1 AND IAT.AMBITO=3" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN ITEM_ATRIBESP IAE WITH (NOLOCK) ON IAE.ANYO=@ANYO_L AND IAE.GMN1=@GMN1_L AND IAE.PROCE=@PROCE_L AND IAE.ATRIB=IAT.ATRIB AND IAE.ITEM=I.ID" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PROCE_GRUPO_ATRIBESP PGA WITH (NOLOCK) ON PGA.ANYO=@ANYO_L AND PGA.GMN1=@GMN1_L AND PGA.PROCE=@PROCE_L AND PGA.ATRIB=IAT.ATRIB AND PGA.GRUPO=@IDGRUPO" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PROCE_ATRIBESP PATR WITH(NOLOCK) ON PATR.ANYO=@ANYO_L AND PATR.GMN1=@GMN1_L AND PATR.PROCE=@PROCE_L AND PATR.ATRIB=IAT.ATRIB" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN (SELECT TOP 1 OFE,ANYO,GMN1,PROCE,PROVE FROM ITEM_ADJ WITH (NOLOCK) WHERE ANYO=@ANYO_L AND GMN1=@GMN1_L AND PROCE=@PROCE_L AND PROVE=@PROVE_L) IAD ON IAD.ANYO=@ANYO_L  AND IAD.GMN1=@GMN1_L AND IAD.PROCE=@PROCE_L AND IAD.PROVE=@PROVE_L" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PROCE_ATRIB PA WITH (NOLOCK) ON PA.ANYO=@ANYO_L AND PA.GMN1=@GMN1_L AND PA.PROCE=@PROCE_L AND PA.AMBITO=3 AND DA.ID=PA.ATRIB AND (PA.GRUPO=@IDGRUPO OR PA.GRUPO IS NULL)" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN OFE_ITEM_ATRIB OA WITH (NOLOCK) ON OA.ANYO=@ANYO_L AND OA.GMN1=@GMN1_L AND OA.PROCE=@PROCE_L AND OA.ATRIB_ID =PA.ID AND OA.PROVE=@PROVE_L AND OA.OFE=IAD.OFE AND OA.ITEM=I.ID" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN OFE_GR_ATRIB OGATR WITH(NOLOCK) ON OGATR.ANYO=@ANYO_L AND OGATR.GMN1=@GMN1_L AND OGATR.PROCE=@PROCE_L AND OGATR.ATRIB_ID =PA.ID AND OGATR.PROVE=@PROVE_L AND OGATR.OFE=IAD.OFE AND OGATR.GRUPO=I.GRUPO" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN OFE_ATRIB OATR WITH(NOLOCK) ON OATR.ANYO=@ANYO_L AND OATR.GMN1=@GMN1_L AND OATR.PROCE=@PROCE_L AND OATR.ATRIB_ID =PA.ID AND OATR.PROVE=@PROVE_L AND OATR.OFE=IAD.OFE" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN" & vbCrLf
sConsulta = sConsulta & "   (SELECT DA.ID INT_ATRIB_ID,LIAA.ID_LOG_ITEM_ADJ , DA.ID, DA.COD,DA.DEN," & vbCrLf
sConsulta = sConsulta & "   DA.TIPO,IA.INTRO,IA.OBLIGATORIO,LIAA.VALOR_TEXT ,LIAA.VALOR_NUM,LIAA.VALOR_FEC,LIAA.VALOR_BOOL" & vbCrLf
sConsulta = sConsulta & "   FROM INT_ATRIB IA WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN DEF_ATRIB DA WITH (NOLOCK) ON DA.ID=IA.ATRIB AND IA.ADJUDICACION=1 AND IA.AMBITO=3" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN LOG_ITEM_ADJ_ATRIB LIAA WITH (NOLOCK) ON LIAA.ATRIB=IA.ATRIB where IA.ERP=@ERP" & vbCrLf
sConsulta = sConsulta & "   ) IT_ATRIB1 ON IT_ATRIB1.INT_ATRIB_ID=IAT.ATRIB AND'" & vbCrLf
sConsulta = sConsulta & "IF @DISTR=1" & vbCrLf
sConsulta = sConsulta & "   SET @SQL =@SQL + ' COALESCE(LIA2.ID,LIA1.ID)=IT_ATRIB1.ID_LOG_ITEM_ADJ WHERE P.ANYO=@ANYO_L AND P.GMN1=@GMN1_L AND P.COD=@PROCE_L" & vbCrLf
sConsulta = sConsulta & "   AND CASE WHEN COALESCE(LIA2.ID,LIA1.ID) IS NULL THEN 1 ELSE COALESCE(LIA2.ID,LIA1.ID) END = ISNULL(IT_EMP.IT_EMP_LIAID,1)" & vbCrLf
sConsulta = sConsulta & "   GROUP BY LIA1.ID,LIA2.ID'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   SET @SQL =@SQL + ' LIA1.ID=IT_ATRIB1.ID_LOG_ITEM_ADJ WHERE P.ANYO=@ANYO_L AND P.GMN1=@GMN1_L AND P.COD=@PROCE_L" & vbCrLf
sConsulta = sConsulta & "   AND CASE WHEN LIA1.ID IS NULL THEN 1 ELSE LIA1.ID END = ISNULL(IT_EMP.IT_EMP_LIAID,1)" & vbCrLf
sConsulta = sConsulta & "   GROUP BY LIA1.ID'" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' ,I.ID,I.ART ,I.DESCR ,I.UNI,'" & vbCrLf
sConsulta = sConsulta & "if  @ESCALADOS>0" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + ' PGE.ID ,PGE.INICIO,PGE.FIN,'" & vbCrLf
sConsulta = sConsulta & "else" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + 'IA.CANT_ADJ,IT_EMP.PORCEN,'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' IOF.PREC_VALIDO ,P.MON, LG.ESTADO , LG.FECENV, '" & vbCrLf
sConsulta = sConsulta & "IF @DISTR=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' LIA1.USU, COALESCE(LIA2.USU,LIA1.USU) ,COALESCE(LIA2.ACCION,LIA1.ACCION),COALESCE(LIA2.CENTRO,LIA1.CENTRO)'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + ' LIA1.USU,LIA1.ACCION,LIA1.CENTRO'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' , I.FECINI ,I.FECFIN ,PE.COD_ERP,PE.DEN_ERP," & vbCrLf
sConsulta = sConsulta & "IT_ATRIB1.INT_ATRIB_ID,IAE.ATRIB,PGA.ATRIB,PATR.ATRIB,PA.ATRIB,DA.ID," & vbCrLf
sConsulta = sConsulta & "DA.COD ,DA.DEN ," & vbCrLf
sConsulta = sConsulta & "COALESCE(IT_ATRIB1.VALOR_TEXT,IAE.VALOR_TEXT,PGA.VALOR_TEXT,PATR.VALOR_TEXT,OA.VALOR_TEXT,OGATR.VALOR_TEXT,OATR.VALOR_TEXT,IAT.VALOR_TEXT)  ," & vbCrLf
sConsulta = sConsulta & "COALESCE(IT_ATRIB1.VALOR_NUM ,IAE.VALOR_NUM,PGA.VALOR_NUM,PATR.VALOR_NUM,OA.VALOR_NUM,OGATR.VALOR_NUM,OATR.VALOR_NUM,IAT.VALOR_NUM) ," & vbCrLf
sConsulta = sConsulta & "COALESCE(IT_ATRIB1.VALOR_FEC ,IAE.VALOR_FEC,PGA.VALOR_FEC,PATR.VALOR_FEC,OA.VALOR_FEC,OGATR.VALOR_FEC,OATR.VALOR_FEC,IAT.VALOR_FEC) ," & vbCrLf
sConsulta = sConsulta & "COALESCE(IT_ATRIB1.VALOR_BOOL,IAE.VALOR_BOOL,PGA.VALOR_BOOL,PATR.VALOR_BOOL,OA.VALOR_BOOL,OGATR.VALOR_BOOL,OATR.VALOR_BOOL,IAT.VALOR_BOOL) ," & vbCrLf
sConsulta = sConsulta & "DA.TIPO ,IAT.OBLIGATORIO'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @USAR_ORGCOMPRAS=1" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + ',IT_EMP.UON1 ,IT_EMP.UON2 ,IT_EMP.UON3  '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ESCALADOS>0" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + ',PG.ESCALAUNI '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' ) A '" & vbCrLf
sConsulta = sConsulta & "IF @USAR_ORGCOMPRAS=1" & vbCrLf
sConsulta = sConsulta & "begin" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + 'LEFT JOIN(" & vbCrLf
sConsulta = sConsulta & "   select distinct au.art4,au.uon1,aU.uon2,aU.uon3,ISNULL(u3.empresa,ISNULL(u2.empresa,u1.empresa)) as empresa,ISNULL(u3.CENTROS,ISNULL(u2.CENTROS,u1.CENTROS)) CENTRO" & vbCrLf
sConsulta = sConsulta & "   from art4_uon au WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   inner join uon3 u3 WITH(NOLOCK) on au.uon1=u3.uon1 and au.uon2=u3.uon2 and au.uon3=u3.cod" & vbCrLf
sConsulta = sConsulta & "   inner join uon2 u2 WITH(NOLOCK) on u3.uon1=u2.uon1 and u3.uon2=u2.cod" & vbCrLf
sConsulta = sConsulta & "   inner join uon1 u1 WITH(NOLOCK) on u1.cod=u3.uon1'" & vbCrLf
sConsulta = sConsulta & "   IF @TRASPASO_PLANIF>0" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+ ' INNER JOIN ART4_UON_ATRIB AUA with (nolock) ON au.ART4=AUA.ART" & vbCrLf
sConsulta = sConsulta & "       AND AUA.UON1=au.UON1 AND ISNULL(AUA.UON2,'''')=ISNULL(au.UON2,'''')" & vbCrLf
sConsulta = sConsulta & "       AND ISNULL(AUA.UON3, '''')=ISNULL(au.UON3,'''')" & vbCrLf
sConsulta = sConsulta & "       AND AUA.ATRIB=@TRASPASO_PLANIF AND AUA.VALOR_BOOL=1  '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL =@SQL + ') IT_CEN ON A.CODART=IT_CEN.ART4 AND IT_CEN.EMPRESA=@IDEMP_L AND IT_CEN.UON1=A.UON1 AND IT_CEN.UON2=ISNULL(A.uon2,IT_CEN.UON2) AND ISNULL(A.uon3,IT_CEN.UON3) =IT_CEN.UON3 '" & vbCrLf
sConsulta = sConsulta & "end" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' ORDER BY A.ID_ITEM'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @PARAM =N'@ANYO_L INT, @GMN1_L VARCHAR(50), @PROCE_L VARCHAR(50), @CODGRUPO_L VARCHAR(20),@PROVE_L VARCHAR(50),@IDEMP_L INT,@ERP INT,@IDGRUPO INT,@TRASPASO_PLANIF INT'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL ,@PARAM,@ANYO_L=@ANYO_L,@GMN1_L=@GMN1_L,@PROCE_L=@PROCE_L,@CODGRUPO_L=@CODGRUPO_L,@PROVE_L=@PROVE_L,@IDEMP_L=@IDEMP_L,@ERP=@ERP ,@IDGRUPO=@IDGRUPO,@TRASPASO_PLANIF=@TRASPASO_PLANIF" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta


sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_ROL_PARTICIPANTES]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_ROL_PARTICIPANTES]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_ROL_PARTICIPANTES] @BLOQUE INT, @ROL INT, @INSTANCIA INT,@IDI VARCHAR(50), @ID_FAVORITA INT=0, @LIS_PET INT=0  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @INSTANCIA<>0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF" & vbCrLf
sConsulta = sConsulta & "SELECT R.DEN RDEN,R.ID ROL,R.TIPO,R.RESTRINGIR, CASE WHEN R.PER IS NULL THEN R.PROVE ELSE R.PER END PART," & vbCrLf
sConsulta = sConsulta & "CASE WHEN R.PER IS NOT NULL THEN" & vbCrLf
sConsulta = sConsulta & "PER.NOM + ' ' + PER.APE+ ' (' + PER.EMAIL + ')'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "(CASE WHEN R.PROVE IS NULL THEN" & vbCrLf
sConsulta = sConsulta & "NULL" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "PROVE.DEN" & vbCrLf
sConsulta = sConsulta & "END)" & vbCrLf
sConsulta = sConsulta & "END PARTNOM, NULL AS ID_CONTACTO" & vbCrLf
sConsulta = sConsulta & "FROM PM_COPIA_ROL R WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PER WITH (NOLOCK)  ON PER.COD=R.PER" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PROVE WITH (NOLOCK)  ON PROVE.COD=R.PROVE" & vbCrLf
sConsulta = sConsulta & "WHERE R.INSTANCIA=@INSTANCIA AND R.BLOQUE=@BLOQUE AND R.ROL=@ROL AND R.COMO_ASIGNAR=2" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT RB.ROL,RB.BLOQUE,B.DEN BDEN" & vbCrLf
sConsulta = sConsulta & "FROM PM_COPIA_ROL R WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_COPIA_ROL_BLOQUE RB WITH (NOLOCK)  ON RB.ROL= R.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_COPIA_BLOQUE_DEN B WITH (NOLOCK)  ON  B.BLOQUE=RB.BLOQUE" & vbCrLf
sConsulta = sConsulta & "WHERE R.INSTANCIA=@INSTANCIA AND R.BLOQUE=@BLOQUE AND R.ROL=@ROL AND R.COMO_ASIGNAR=2 AND B.IDI=@IDI" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @CNT INTEGER" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @CNT=COUNT(*) FROM PM_PARTICIPANTES_FAV WITH(NOLOCK) WHERE SOLICIT_FAV=@ID_FAVORITA" & vbCrLf
sConsulta = sConsulta & "IF  @CNT = 0" & vbCrLf
sConsulta = sConsulta & "       SET @ID_FAVORITA = 0" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'SELECT DISTINCT R.DEN RDEN, R.ID ROL, R.TIPO, R.RESTRINGIR, '" & vbCrLf
sConsulta = sConsulta & "IF @ID_FAVORITA <> 0" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = @SQL + 'CASE WHEN PF.PER IS NULL THEN PF.PROVE" & vbCrLf
sConsulta = sConsulta & "                       ELSE  PF.PER END PART," & vbCrLf
sConsulta = sConsulta & "                           CASE WHEN PF.PER IS NOT NULL THEN PER.NOM + '' '' + PER.APE+ '' ('' + PER.EMAIL + '')''" & vbCrLf
sConsulta = sConsulta & "                           ELSE" & vbCrLf
sConsulta = sConsulta & "                               (CASE WHEN PF.PROVE IS NULL THEN" & vbCrLf
sConsulta = sConsulta & "                                   NULL" & vbCrLf
sConsulta = sConsulta & "                               ELSE" & vbCrLf
sConsulta = sConsulta & "                                   PROVE.DEN + '' ('' + LEFT(b.EmailsConcatenados , LEN(b.EmailsConcatenados )-1) + '')''" & vbCrLf
sConsulta = sConsulta & "                               END)" & vbCrLf
sConsulta = sConsulta & "                       END PARTNOM, a.ContactosConcatenados ID_CONTACTO'" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = @SQL + 'CASE WHEN R.PER IS NULL THEN R.PROVE" & vbCrLf
sConsulta = sConsulta & "                       ELSE R.PER END PART," & vbCrLf
sConsulta = sConsulta & "                           CASE WHEN R.PER IS NOT NULL THEN PER.NOM + '' '' + PER.APE+ '' ('' + PER.EMAIL + '')''" & vbCrLf
sConsulta = sConsulta & "                           ELSE" & vbCrLf
sConsulta = sConsulta & "                               (CASE WHEN R.PROVE IS NULL THEN" & vbCrLf
sConsulta = sConsulta & "                                   NULL" & vbCrLf
sConsulta = sConsulta & "                               ELSE" & vbCrLf
sConsulta = sConsulta & "                                   PROVE.DEN + '' ('' + CON.EMAIL + '')''" & vbCrLf
sConsulta = sConsulta & "                               END)" & vbCrLf
sConsulta = sConsulta & "                       END PARTNOM, CON.ID ID_CONTACTO '" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   IF @LIS_PET =0" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = @SQL + ' FROM PM_ROL R WITH (NOLOCK)'" & vbCrLf
sConsulta = sConsulta & "   ELSE --Lista de Peticionarios" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = @SQL + ' FROM PM_ROL R WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PM_ROL_BLOQUE RB WITH (NOLOCK)  ON RB.ROL= R.ID AND RB.BLOQUE=@BLOQUE'" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   IF @ID_FAVORITA <> 0" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = @SQL + ' LEFT JOIN PM_PARTICIPANTES_FAV PF WITH (NOLOCK) ON R.ID=PF.ROL" & vbCrLf
sConsulta = sConsulta & "                           CROSS APPLY" & vbCrLf
sConsulta = sConsulta & "                           (" & vbCrLf
sConsulta = sConsulta & "                           SELECT CON + ''|''" & vbCrLf
sConsulta = sConsulta & "                           FROM PM_PARTICIPANTES_CON_FAV pcf WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                           WHERE pf.ID = pcf.PARTICIPANTES_FAV" & vbCrLf
sConsulta = sConsulta & "                           FOR XML PATH('''')" & vbCrLf
sConsulta = sConsulta & "                           ) a (ContactosConcatenados)" & vbCrLf
sConsulta = sConsulta & "                           " & vbCrLf
sConsulta = sConsulta & "                           CROSS APPLY" & vbCrLf
sConsulta = sConsulta & "                           (" & vbCrLf
sConsulta = sConsulta & "                           SELECT CON.EMAIL + '',''" & vbCrLf
sConsulta = sConsulta & "                           FROM PM_PARTICIPANTES_CON_FAV PCF WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                           LEFT JOIN CON WITH(NOLOCK) ON CON.ID = PCF.CON AND CON.PROVE = PF.PROVE" & vbCrLf
sConsulta = sConsulta & "                           WHERE PF.ID = PCF.PARTICIPANTES_FAV" & vbCrLf
sConsulta = sConsulta & "                           FOR XML PATH('''')" & vbCrLf
sConsulta = sConsulta & "                           ) b (EmailsConcatenados)" & vbCrLf
sConsulta = sConsulta & "                           " & vbCrLf
sConsulta = sConsulta & "                           LEFT JOIN PER WITH (NOLOCK)  ON PER.COD = PF.PER" & vbCrLf
sConsulta = sConsulta & "                           LEFT JOIN PROVE WITH (NOLOCK)  ON PROVE.COD=PF.PROVE '" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = @SQL + ' LEFT JOIN PER WITH (NOLOCK)  ON PER.COD = R.PER" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN PROVE WITH (NOLOCK)  ON PROVE.COD=R.PROVE" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN CON WITH (NOLOCK)  ON PROVE.COD=CON.PROVE AND CON.ID=R.CON'" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "   IF @LIS_PET =0" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = @SQL + ' WHERE R.BLOQUE_ASIGNA=@BLOQUE AND R.ROL_ASIGNA=@ROL AND R.COMO_ASIGNAR=2'" & vbCrLf
sConsulta = sConsulta & "   ELSE --Lista de Peticionarios" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = @SQL + ' WHERE R.TIPO=4 AND R.RESTRINGIR=7 AND R.ID=@ROL'    " & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "   IF @ID_FAVORITA <> 0" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = @SQL + 'AND SOLICIT_FAV=@ID_FAVORITA" & vbCrLf
sConsulta = sConsulta & "       GROUP BY R.BLOQUE_ASIGNA, R.DEN, R.ID, R.TIPO, R.RESTRINGIR, PF.PER, PER.NOM, PER.APE, PER.EMAIL, PF.PROVE, PROVE.DEN, a.ContactosConcatenados, b.EmailsConcatenados'" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL,N'@BLOQUE INT, @ROL INT, @ID_FAVORITA INT',@BLOQUE=@BLOQUE, @ROL=@ROL, @ID_FAVORITA=@ID_FAVORITA" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "IF @LIS_PET =0" & vbCrLf
sConsulta = sConsulta & "       SELECT RB.ROL,RB.BLOQUE,B.DEN BDEN" & vbCrLf
sConsulta = sConsulta & "       FROM PM_ROL R  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PM_ROL_BLOQUE RB WITH (NOLOCK)  ON RB.ROL= R.ID" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PM_BLOQUE_DEN B WITH (NOLOCK)  ON  B.BLOQUE=RB.BLOQUE" & vbCrLf
sConsulta = sConsulta & "       WHERE R.BLOQUE_ASIGNA=@BLOQUE AND R.ROL_ASIGNA=@ROL AND R.COMO_ASIGNAR=2 AND B.IDI=@IDI" & vbCrLf
sConsulta = sConsulta & "ELSE --Lista de Peticionarios" & vbCrLf
sConsulta = sConsulta & "       SELECT RB.ROL,RB.BLOQUE,B.DEN BDEN" & vbCrLf
sConsulta = sConsulta & "       FROM PM_ROL R  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PM_ROL_BLOQUE RB WITH (NOLOCK)  ON RB.ROL= R.ID" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PM_BLOQUE_DEN B WITH (NOLOCK)  ON  B.BLOQUE=RB.BLOQUE" & vbCrLf
sConsulta = sConsulta & "       WHERE RB.BLOQUE=@BLOQUE AND R.TIPO=4 AND R.RESTRINGIR=7 AND B.IDI=@IDI AND R.ID=@ROL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @LIS_PET=1" & vbCrLf
sConsulta = sConsulta & "       SELECT PER FROM PM_PARTICIPANTES WHERE ROL=@ROL     " & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSEP_CAMBIAR_ESTADO_RECEPCION]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSEP_CAMBIAR_ESTADO_RECEPCION]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSEP_CAMBIAR_ESTADO_RECEPCION @ORDEN int, @PER varchar(40), @CERRAR tinyint" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @CERRARSOLICITADOPORUSUARIO INT = @CERRAR" & vbCrLf
sConsulta = sConsulta & "IF @CERRAR=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   declare @linea bigint" & vbCrLf
sConsulta = sConsulta & "   declare @tiporec smallint" & vbCrLf
sConsulta = sConsulta & "   declare @cont int=0, @numlin int=0" & vbCrLf
sConsulta = sConsulta & "   declare @cant_ped float,@cant_rec float,@imp_ped float" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   declare UNCURSOR cursor" & vbCrLf
sConsulta = sConsulta & "   for" & vbCrLf
sConsulta = sConsulta & "       select ID,TIPORECEPCION,CANT_PED,CANT_REC,IMPORTE_PED from LINEAS_PEDIDO lp with(nolock) WHERE ORDEN=@ORDEN" & vbCrLf
sConsulta = sConsulta & "   open UNCURSOR" & vbCrLf
sConsulta = sConsulta & "   fetch UNCURSOR into @linea, @tiporec,@cant_ped,@cant_rec,@imp_ped" & vbCrLf
sConsulta = sConsulta & "   while @@fetch_status = 0" & vbCrLf
sConsulta = sConsulta & "   begin" & vbCrLf
sConsulta = sConsulta & "       SET @numlin=@numlin+1" & vbCrLf
sConsulta = sConsulta & "       IF @tiporec=0" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           IF @cant_ped=@cant_rec SET @cont=@cont+1" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "       ELSE" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           DECLARE @IMP_REC FLOAT" & vbCrLf
sConsulta = sConsulta & "           SELECT @IMP_REC=SUM(LINEAS_RECEP.IMPORTE)" & vbCrLf
sConsulta = sConsulta & "               FROM LINEAS_RECEP WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "               GROUP BY LINEA" & vbCrLf
sConsulta = sConsulta & "               HAVING LINEA = @linea" & vbCrLf
sConsulta = sConsulta & "           IF @imp_ped=@IMP_REC SET @cont=@cont+1" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "       fetch UNCURSOR into @linea, @tiporec,@cant_ped,@cant_rec,@imp_ped" & vbCrLf
sConsulta = sConsulta & "   end" & vbCrLf
sConsulta = sConsulta & "   close UNCURSOR" & vbCrLf
sConsulta = sConsulta & "   deallocate UNCURSOR" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @cont=@numlin SET @CERRAR=1" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- @APROBPROVEREQUERIDA -> APROBACION DEL PROVEEDOR REQUERIDA" & vbCrLf
sConsulta = sConsulta & "-- @TIPO -> ORIGEN DEL PEDIDO: GS (0), EP (1) O ERP (2)" & vbCrLf
sConsulta = sConsulta & "DECLARE @ESTACT INT, @PEDIDO INT, @APROBPROVEREQUERIDA INT, @TIPO INT" & vbCrLf
sConsulta = sConsulta & "SELECT @ESTACT=EST, @PEDIDO=PEDIDO, @APROBPROVEREQUERIDA=ACEP_PROVE, @TIPO = TIPO FROM ORDEN_ENTREGA WITH (NOLOCK) WHERE ID=@ORDEN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CERRAR=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @ESTACT=3 OR @ESTACT=4 OR (@ESTACT = 2 AND @TIPO = 0 AND @APROBPROVEREQUERIDA = 0)" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "UPDATE ORDEN_ENTREGA SET EST=5 WHERE ID=@ORDEN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO ORDEN_EST (PEDIDO, ORDEN, EST, FECHA, PER)" & vbCrLf
sConsulta = sConsulta & "VALUES (@PEDIDO, @ORDEN, 5, GETDATE(), @PER)" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "UPDATE ORDEN_ENTREGA SET EST=6, INCORRECTA=0 WHERE ID=@ORDEN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Si es el usuario el que ha solicitado cerrar, a?adimos estado parcialmente recibido (no importa que ya haya previamente este estado, es para saber que hemos pasado por registro de recepci?n + cierre)" & vbCrLf
sConsulta = sConsulta & "IF @CERRARSOLICITADOPORUSUARIO = 1" & vbCrLf
sConsulta = sConsulta & "INSERT INTO ORDEN_EST (PEDIDO, ORDEN, EST, FECHA, PER)" & vbCrLf
sConsulta = sConsulta & "VALUES (@PEDIDO, @ORDEN, 5, GETDATE(), @PER)" & vbCrLf
sConsulta = sConsulta & "--Luego a?adimos estado cerrado" & vbCrLf
sConsulta = sConsulta & "INSERT INTO ORDEN_EST (PEDIDO, ORDEN, EST, FECHA, PER)" & vbCrLf
sConsulta = sConsulta & "VALUES (@PEDIDO, @ORDEN, 6, GETDATE(), @PER)" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF" & vbCrLf
sConsulta = sConsulta & "SET ANSI_NULLS ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSEP_REGISTRAR_CANTIDAD]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSEP_REGISTRAR_CANTIDAD]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSEP_REGISTRAR_CANTIDAD] @ORDEN int, @RECEP int, @LINEA int, @CANT float, @ACTIVO int, @PER varchar(40),@LOG tinyint,@IMPORTE float=null AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_LINEAS_RECEP INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @TIPORECEP smallint" & vbCrLf
sConsulta = sConsulta & "SELECT @TIPORECEP = TIPORECEPCION" & vbCrLf
sConsulta = sConsulta & "FROM LINEAS_PEDIDO WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE ID = @LINEA" & vbCrLf
sConsulta = sConsulta & "IF @TIPORECEP=1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   DECLARE @IMPORTE_REC FLOAT" & vbCrLf
sConsulta = sConsulta & "   SELECT @IMPORTE_REC=SUM(LINEAS_RECEP.IMPORTE)" & vbCrLf
sConsulta = sConsulta & "       FROM LINEAS_RECEP WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       GROUP BY LINEA" & vbCrLf
sConsulta = sConsulta & "       HAVING LINEA=@LINEA" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO LINEAS_RECEP (ORDEN,PEDIDO_RECEP,LINEA,CANT,IMPORTE )" & vbCrLf
sConsulta = sConsulta & "VALUES(@ORDEN,@RECEP,@LINEA,@CANT,@IMPORTE )" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @ID_LINEAS_RECEP=SCOPE_IDENTITY()" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @ESTANTIGUO smallint" & vbCrLf
sConsulta = sConsulta & "DECLARE @ESTNUEVO smallint" & vbCrLf
sConsulta = sConsulta & "SELECT @ESTANTIGUO = EST" & vbCrLf
sConsulta = sConsulta & "FROM LINEAS_PEDIDO WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE ID = @LINEA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @TIPORECEP=0" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       UPDATE LINEAS_PEDIDO" & vbCrLf
sConsulta = sConsulta & "          SET CANT_REC = CANT_REC + @CANT," & vbCrLf
sConsulta = sConsulta & "              EST = CASE WHEN CANT_PED = CANT_REC + @CANT THEN 3 ELSE CASE WHEN @CANT>0 THEN 2 ELSE EST END END," & vbCrLf
sConsulta = sConsulta & "              ACTIVO = @ACTIVO" & vbCrLf
sConsulta = sConsulta & "         FROM LINEAS_PEDIDO WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "        WHERE ID = @LINEA" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       UPDATE LINEAS_PEDIDO" & vbCrLf
sConsulta = sConsulta & "          SET EST = CASE WHEN IMPORTE_PED = @IMPORTE_REC + @IMPORTE THEN 3 ELSE CASE WHEN @IMPORTE>0 THEN 2 ELSE EST END END," & vbCrLf
sConsulta = sConsulta & "              ACTIVO = @ACTIVO" & vbCrLf
sConsulta = sConsulta & "         FROM LINEAS_PEDIDO WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "        WHERE ID = @LINEA" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @PEDIDO int" & vbCrLf
sConsulta = sConsulta & "SELECT @PEDIDO = PEDIDO, @ESTNUEVO = EST" & vbCrLf
sConsulta = sConsulta & "FROM LINEAS_PEDIDO WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE ID = @LINEA" & vbCrLf
sConsulta = sConsulta & "IF @ESTANTIGUO!=@ESTNUEVO" & vbCrLf
sConsulta = sConsulta & "INSERT INTO LINEAS_EST (PEDIDO, ORDEN, LINEA,EST,FECHA,PER)  VALUES (@PEDIDO, @ORDEN, @LINEA, @ESTNUEVO, GETDATE(),@PER)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF NOT @LOG IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_LOG_PEDIDO_RECEP INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @ID_LOG_PEDIDO_RECEP=LPR.ID FROM LOG_PEDIDO_RECEP LPR WITH(NOLOCK) WHERE LPR.ID_PEDIDO_RECEP=@RECEP" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF NOT @ID_LOG_PEDIDO_RECEP IS NULL" & vbCrLf
sConsulta = sConsulta & "INSERT INTO LOG_LINEAS_RECEP (ID_LOG_PEDIDO_RECEP,ID_PEDIDO_RECEP,ID_ORDEN_ENTREGA,ID_LINEA_RECEP,ID_LINEA_PEDIDO,CANT,ACCION,IMPORTE)" & vbCrLf
sConsulta = sConsulta & "VALUES(@ID_LOG_PEDIDO_RECEP,@RECEP,@ORDEN,@ID_LINEAS_RECEP,@LINEA,@CANT,'I',@IMPORTE)" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_ELIMINAR_SOLICITUDES_INCOMPLETAS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_ELIMINAR_SOLICITUDES_INCOMPLETAS]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_ELIMINAR_SOLICITUDES_INCOMPLETAS] AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--PM" & vbCrLf
sConsulta = sConsulta & "DECLARE  CINSTANCIAS CURSOR LOCAL FOR SELECT ID FROM INSTANCIA WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE NUM IS NULL  AND ESTADO<200 AND EN_PROCESO=0 AND NOT EXISTS (SELECT DISTINCT I.SOLICIT FROM ITEM I WITH(NOLOCK) WHERE I.SOLICIT = INSTANCIA.ID)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "OPEN CINSTANCIAS" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM CINSTANCIAS INTO @ID" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM PM_INSTANCIAS_PENDIENTES WHERE INSTANCIA = @ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM PM_COPIA_ENLACE_EXTRAPOINTS FROM PM_COPIA_ENLACE_EXTRAPOINTS E WITH(NOLOCK) INNER JOIN PM_COPIA_ENLACE CE WITH (NOLOCK) ON E.ENLACE=CE.ID INNER JOIN PM_COPIA_BLOQUE B WITH (NOLOCK) ON CE.BLOQUE_ORIGEN=B.ID WHERE B.INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM PM_COPIA_ENLACE_CONDICION FROM PM_COPIA_ENLACE_CONDICION E WITH(NOLOCK) INNER JOIN PM_COPIA_ENLACE CE WITH (NOLOCK) ON E.ENLACE=CE.ID INNER JOIN PM_COPIA_BLOQUE B WITH (NOLOCK) ON CE.BLOQUE_ORIGEN=B.ID WHERE B.INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM PM_COPIA_ENLACE_CONDICION FROM PM_COPIA_ENLACE_CONDICION E WITH(NOLOCK) INNER JOIN PM_COPIA_ENLACE  CE WITH (NOLOCK) ON E.ENLACE=CE.ID INNER JOIN PM_COPIA_BLOQUE B WITH (NOLOCK) ON CE.BLOQUE_DESTINO=B.ID WHERE B.INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM PM_COPIA_CONF_CUMP_BLOQUE_LISTA FROM PM_COPIA_CONF_CUMP_BLOQUE_LISTA C WITH(NOLOCK) INNER JOIN COPIA_CAMPO_DEF CC  WITH (NOLOCK) ON C.CAMPO=CC.ID INNER JOIN COPIA_GRUPO G WITH (NOLOCK) ON CC.GRUPO=G.ID WHERE G.INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM PM_COPIA_CONF_CUMP_BLOQUE_LISTA FROM PM_COPIA_CONF_CUMP_BLOQUE_LISTA C WITH(NOLOCK) INNER JOIN PM_COPIA_ROL R WITH (NOLOCK) ON C.ROL=R.ID WHERE R.INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM PM_COPIA_CONF_CUMP_BLOQUE_LISTA FROM PM_COPIA_CONF_CUMP_BLOQUE_LISTA C INNER JOIN PM_COPIA_BLOQUE B WITH (NOLOCK) ON C.BLOQUE=B.ID WHERE B.INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM PM_COPIA_CONF_CUMP_BLOQUE FROM PM_COPIA_CONF_CUMP_BLOQUE C WITH(NOLOCK) INNER JOIN COPIA_CAMPO_DEF CC  WITH (NOLOCK) ON C.CAMPO=CC.ID INNER JOIN COPIA_GRUPO G WITH (NOLOCK) ON CC.GRUPO=G.ID WHERE G.INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM PM_COPIA_CONF_CUMP_BLOQUE FROM PM_COPIA_CONF_CUMP_BLOQUE C WITH(NOLOCK) INNER JOIN PM_COPIA_ROL R WITH (NOLOCK) ON C.ROL=R.ID WHERE R.INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM PM_COPIA_CONF_CUMP_BLOQUE FROM PM_COPIA_CONF_CUMP_BLOQUE C WITH(NOLOCK) INNER JOIN PM_COPIA_BLOQUE B WITH (NOLOCK) ON C.BLOQUE=B.ID WHERE B.INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM PM_COPIA_CONF_CUMP_BLOQUE_COND FROM PM_COPIA_CONF_CUMP_BLOQUE_COND C WITH(NOLOCK) INNER JOIN COPIA_CAMPO_DEF CC  WITH (NOLOCK) ON C.CAMPO=CC.ID INNER JOIN COPIA_GRUPO G WITH (NOLOCK) ON CC.GRUPO=G.ID WHERE G.INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM PM_COPIA_CONF_CUMP_BLOQUE_COND FROM PM_COPIA_CONF_CUMP_BLOQUE_COND C WITH(NOLOCK) INNER JOIN PM_COPIA_ROL R WITH (NOLOCK) ON C.ROL=R.ID WHERE R.INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM PM_COPIA_CONF_CUMP_BLOQUE_COND FROM PM_COPIA_CONF_CUMP_BLOQUE_COND C WITH(NOLOCK) INNER JOIN PM_COPIA_BLOQUE B WITH (NOLOCK) ON C.BLOQUE=B.ID WHERE B.INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM PM_COPIA_ACCION_CONDICIONES FROM PM_COPIA_ACCION_CONDICIONES C WITH(NOLOCK) INNER JOIN PM_COPIA_ACCION_PRECOND A WITH (NOLOCK) ON C.ACCION_PRECOND=A.ID INNER JOIN PM_COPIA_ACCIONES CA WITH (NOLOCK) ON A.ACCION=CA.ID INNER JOIN PM_COPIA_BLOQUE B WITH (NOLOCK) ON CA.BLOQUE=B.ID WHERE B.INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM PM_COPIA_ACCION_PRECOND_DEN FROM PM_COPIA_ACCION_PRECOND_DEN C WITH(NOLOCK) INNER JOIN PM_COPIA_ACCION_PRECOND A WITH (NOLOCK) ON C.ACCION_PRECOND=A.ID INNER JOIN PM_COPIA_ACCIONES CA WITH (NOLOCK) ON A.ACCION=CA.ID INNER JOIN PM_COPIA_BLOQUE B WITH (NOLOCK) ON CA.BLOQUE=B.ID WHERE B.INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM PM_COPIA_ACCION_PRECOND FROM PM_COPIA_ACCION_PRECOND A WITH(NOLOCK) INNER JOIN PM_COPIA_ACCIONES CA WITH (NOLOCK) ON A.ACCION=CA.ID INNER JOIN PM_COPIA_BLOQUE B WITH (NOLOCK) ON CA.BLOQUE=B.ID WHERE B.INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM PM_COPIA_ROL_ACCION FROM PM_COPIA_ROL_ACCION R WITH(NOLOCK) INNER JOIN PM_COPIA_BLOQUE B WITH (NOLOCK) ON R.BLOQUE=B.ID WHERE B.INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM PM_COPIA_ROL_BLOQUE FROM PM_COPIA_ROL_BLOQUE R WITH(NOLOCK) INNER JOIN PM_COPIA_BLOQUE B WITH (NOLOCK) ON R.BLOQUE=B.ID WHERE B.INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM PM_COPIA_PARTICIPANTES  FROM PM_COPIA_PARTICIPANTES P  WITH(NOLOCK) INNER JOIN PM_COPIA_ROL R WITH (NOLOCK) ON P.ROL=R.ID WHERE R.INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM INSTANCIA_CAMINO FROM INSTANCIA_CAMINO I WITH(NOLOCK) INNER JOIN INSTANCIA_BLOQUE B WITH (NOLOCK) ON I.INSTANCIA_BLOQUE_ORIGEN=B.ID WHERE INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM PM_COPIA_ENLACE FROM PM_COPIA_ENLACE E WITH(NOLOCK) INNER JOIN PM_COPIA_BLOQUE B WITH (NOLOCK) ON E.BLOQUE_ORIGEN=B.ID WHERE B.INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM PM_COPIA_ENLACE FROM PM_COPIA_ENLACE E WITH(NOLOCK) INNER JOIN PM_COPIA_BLOQUE B WITH (NOLOCK) ON E.BLOQUE_DESTINO=B.ID WHERE B.INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM PM_COPIA_ACCIONES_DEN FROM PM_COPIA_ACCIONES_DEN  A WITH(NOLOCK) INNER JOIN PM_COPIA_ACCIONES C WITH (NOLOCK) ON A.ACCION=C.ID INNER JOIN PM_COPIA_BLOQUE B WITH (NOLOCK) ON C.BLOQUE=B.ID WHERE B.INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM PM_COPIA_ACCIONES FROM PM_COPIA_ACCIONES A WITH(NOLOCK) INNER JOIN PM_COPIA_BLOQUE B WITH (NOLOCK) ON A.BLOQUE=B.ID WHERE B.INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM PM_COPIA_ROL_LISPER_DEN FROM PM_COPIA_ROL_LISPER_DEN L WITH(NOLOCK) INNER JOIN PM_COPIA_ROL R WITH (NOLOCK) ON L.ROL=R.ID WHERE INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM PM_COPIA_ROL_LISPER FROM PM_COPIA_ROL_LISPER L WITH(NOLOCK) INNER JOIN PM_COPIA_ROL R WITH (NOLOCK) ON L.ROL=R.ID WHERE INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE PM_COPIA_CONDICIONES_BLOQUEO_CONDICIONES FROM PM_COPIA_CONDICIONES_BLOQUEO_CONDICIONES CBC WITH(NOLOCK) INNER JOIN PM_COPIA_CONDICIONES_BLOQUEO CB WITH (NOLOCK) ON CBC.ID_COD = CB.ID WHERE CB.INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE PM_COPIA_CONDICIONES_BLOQUEO WHERE INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE PM_COPIA_BLOQUEO_ETAPA FROM PM_COPIA_BLOQUEO_ETAPA BE WITH(NOLOCK) INNER JOIN PM_COPIA_BLOQUE B WITH (NOLOCK) ON BE.IDBLOQUE = B.ID WHERE INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM PM_COPIA_BLOQUE_DEN FROM PM_COPIA_BLOQUE_DEN C WITH(NOLOCK) INNER JOIN PM_COPIA_BLOQUE B WITH (NOLOCK) ON C.BLOQUE=B.ID WHERE B.INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM INSTANCIA_ASIG_COMP WHERE INSTANCIA = @ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Elimina las lineas de desglose de campos de la instancia" & vbCrLf
sConsulta = sConsulta & "DELETE FROM COPIA_LINEA_DESGLOSE_ADJUN FROM COPIA_LINEA_DESGLOSE_ADJUN D WITH(NOLOCK) INNER JOIN COPIA_CAMPO WITH (NOLOCK) ON D.CAMPO_PADRE=COPIA_CAMPO.ID WHERE COPIA_CAMPO.INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM COPIA_LINEA_DESGLOSE FROM COPIA_LINEA_DESGLOSE D WITH(NOLOCK) INNER JOIN COPIA_CAMPO WITH (NOLOCK) ON D.CAMPO_PADRE=COPIA_CAMPO.ID WHERE COPIA_CAMPO.INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM COPIA_LINEA_DESGLOSE FROM COPIA_LINEA_DESGLOSE D WITH(NOLOCK) INNER JOIN COPIA_CAMPO WITH (NOLOCK) ON D.CAMPO_HIJO=COPIA_CAMPO.ID WHERE COPIA_CAMPO.INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Elimina de COPIA_LINEA la relaci?n entre las l?neas de la solicitud en PM y en el ERP" & vbCrLf
sConsulta = sConsulta & "DELETE FROM COPIA_LINEA WHERE INSTANCIA = @ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Elimina los desgloses de campos de la instancia" & vbCrLf
sConsulta = sConsulta & "DELETE FROM COPIA_DESGLOSE FROM COPIA_DESGLOSE D WITH(NOLOCK) INNER JOIN COPIA_CAMPO WITH (NOLOCK) ON D.CAMPO_PADRE=COPIA_CAMPO.ID WHERE COPIA_CAMPO.INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM COPIA_DESGLOSE FROM COPIA_DESGLOSE D WITH(NOLOCK) INNER JOIN COPIA_CAMPO WITH (NOLOCK) ON D.CAMPO_HIJO=COPIA_CAMPO.ID WHERE COPIA_CAMPO.INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Elimina los adjuntos:" & vbCrLf
sConsulta = sConsulta & "DELETE FROM COPIA_CAMPO_ADJUN FROM COPIA_CAMPO_ADJUN C WITH(NOLOCK) INNER JOIN COPIA_CAMPO WITH (NOLOCK) ON C.CAMPO=COPIA_CAMPO.ID WHERE INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Elimina el campo:" & vbCrLf
sConsulta = sConsulta & "DELETE FROM COPIA_CAMPO WHERE INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Elimina la definici?n del campo:" & vbCrLf
sConsulta = sConsulta & "DELETE FROM COPIA_CAMPO_VALOR_LISTA FROM COPIA_CAMPO_VALOR_LISTA C WITH(NOLOCK) INNER JOIN COPIA_CAMPO_DEF WITH (NOLOCK) ON C.CAMPO_DEF=COPIA_CAMPO_DEF.ID INNER JOIN COPIA_GRUPO WITH (NOLOCK) ON COPIA_CAMPO_DEF.GRUPO=COPIA_GRUPO.ID WHERE COPIA_GRUPO.INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM INSTANCIA_EST WHERE INSTANCIA = @ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM PM_COPIA_ROL_CON FROM PM_COPIA_ROL_CON RC WITH(NOLOCK) INNER JOIN PM_COPIA_ROL R WITH(NOLOCK) ON RC.ID_ROL=R.ID WHERE R.INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM PM_COPIA_ROL WHERE INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM INSTANCIA_BLOQUE WHERE INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM PM_COPIA_BLOQUE WHERE INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM COPIA_CAMPO_DEF FROM COPIA_CAMPO_DEF C WITH(NOLOCK) INNER JOIN COPIA_GRUPO WITH (NOLOCK) ON C.GRUPO=COPIA_GRUPO.ID AND COPIA_GRUPO.INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM COPIA_GRUPO WHERE INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM VERSION_INSTANCIA WHERE INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM INSTANCIA_VISORES_PM WHERE INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM INSTANCIA_INT WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM INSTANCIA_LINEAS_VINCULAR WHERE INSTANCIAVINCULADA=@ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM INSTANCIA_TIEMPO_PROC WHERE INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Eliminamos las instancias de las tablas din?micas de filtros" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(400)" & vbCrLf
sConsulta = sConsulta & "DECLARE @NOM_TABLA NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @NOM_TABLA=NOM_TABLA FROM PM_FILTROS PF WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN SOLICITUD S WITH (NOLOCK) ON PF.FORMULARIO=S.FORMULARIO" & vbCrLf
sConsulta = sConsulta & "INNER JOIN INSTANCIA I WITH(NOLOCK) ON S.ID = I.SOLICITUD WHERE I.ID = @ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDIOMA AS NVARCHAR(400)" & vbCrLf
sConsulta = sConsulta & "DECLARE curIDIOMAS CURSOR LOCAL FOR" & vbCrLf
sConsulta = sConsulta & "SELECT COD FROM IDIOMAS WITH(NOLOCK) WHERE APLICACION=1" & vbCrLf
sConsulta = sConsulta & "OPEN curIDIOMAS" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curIDIOMAS INTO @IDIOMA" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "      SET @SQL = N'DELETE FROM ' + @IDIOMA + '_' + @NOM_TABLA + '_DESGLOSE WHERE FORM_INSTANCIA = @ID'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID INT', @ID=@ID" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'DELETE FROM ' + @IDIOMA + '_' + @NOM_TABLA + ' WHERE INSTANCIA = @ID'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID INT', @ID=@ID" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curIDIOMAS INTO @IDIOMA" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "CLOSE curIDIOMAS" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curIDIOMAS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "---------------------------" & vbCrLf
sConsulta = sConsulta & "--Descontamos del solicitado de las partidas lo que se hab?a solicitado para esta instancia." & vbCrLf
sConsulta = sConsulta & "---------------------------" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRES0 NVARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRES1 NVARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRES2 NVARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRES3 NVARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRES4 NVARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SOLICITADO FLOAT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE CURCAMPOS CURSOR FOR" & vbCrLf
sConsulta = sConsulta & "SELECT PRES0,PRES1,PRES2,PRES3,PRES4,SOLICITADO" & vbCrLf
sConsulta = sConsulta & "FROM INSTANCIA_PRES5 WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE INSTANCIA = @ID" & vbCrLf
sConsulta = sConsulta & "OPEN CURCAMPOS" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM CURCAMPOS INTO @PRES0, @PRES1,@PRES2,@PRES3,@PRES4,@SOLICITADO" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES5_IMPORTES SET SOLICITADO = SOLICITADO - @SOLICITADO" & vbCrLf
sConsulta = sConsulta & "WHERE CERRADO=0 AND PRES0 = @PRES0 AND PRES1 = @PRES1" & vbCrLf
sConsulta = sConsulta & "AND ISNULL(PRES2,'') = ISNULL(@PRES2,'')" & vbCrLf
sConsulta = sConsulta & "AND ISNULL(PRES3,'') = ISNULL(@PRES3,'')" & vbCrLf
sConsulta = sConsulta & "AND ISNULL(PRES4,'') = ISNULL(@PRES4,'')" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM CURCAMPOS INTO @PRES0, @PRES1,@PRES2,@PRES3,@PRES4,@SOLICITADO" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "CLOSE CURCAMPOS" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE CURCAMPOS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE INSTANCIA_PRES5 WHERE INSTANCIA = @ID" & vbCrLf
sConsulta = sConsulta & "---------------------------" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM INSTANCIA_ADICIONAL WHERE INSTANCIA = @ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM INSTANCIA_ADICIONAL_PROVE WHERE INSTANCIA = @ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM INSTANCIA WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM CINSTANCIAS INTO @ID" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "CLOSE CINSTANCIAS" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE CINSTANCIAS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_SUSTITUIR_PER]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_SUSTITUIR_PER]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_SUSTITUIR_PER] @ROL INT, @SUSTITUTO VARCHAR(20), @TIPO_SUSTITUCION INT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- Solicitudes Finalizadas" & vbCrLf
sConsulta = sConsulta & "if @TIPO_SUSTITUCION = 1" & vbCrLf
sConsulta = sConsulta & "begin" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "update INSTANCIA" & vbCrLf
sConsulta = sConsulta & "set PETICIONARIO = @SUSTITUTO" & vbCrLf
sConsulta = sConsulta & "from INSTANCIA I WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "inner join PM_COPIA_ROL CR WITH (NOLOCK) on I.ID = CR.INSTANCIA and I.PETICIONARIO = CR.PER" & vbCrLf
sConsulta = sConsulta & "where CR.TIPO = 4" & vbCrLf
sConsulta = sConsulta & "and CR.ROL_ORIGEN = @ROL" & vbCrLf
sConsulta = sConsulta & "and I.ESTADO >= 100" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "update VERSION_INSTANCIA" & vbCrLf
sConsulta = sConsulta & "set PER_MODIF = @SUSTITUTO" & vbCrLf
sConsulta = sConsulta & "from VERSION_INSTANCIA VI WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "inner join INSTANCIA_EST IE WITH (NOLOCK) on IE.INSTANCIA = VI.INSTANCIA" & vbCrLf
sConsulta = sConsulta & "inner join INSTANCIA_CAMINO IC WITH (NOLOCK) on IC.INSTANCIA_EST = IE.ID AND IC.VERSION = VI.NUM_VERSION" & vbCrLf
sConsulta = sConsulta & "inner join PM_COPIA_ROL CR WITH (NOLOCK) on VI.INSTANCIA = CR.INSTANCIA and VI.PER_MODIF = CR.PER AND CR.ID = IC.ROL" & vbCrLf
sConsulta = sConsulta & "inner join INSTANCIA I WITH (NOLOCK) on VI.INSTANCIA = I.ID" & vbCrLf
sConsulta = sConsulta & "where CR.ROL_ORIGEN = @ROL" & vbCrLf
sConsulta = sConsulta & "and I.ESTADO >= 100" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "update INSTANCIA_EST" & vbCrLf
sConsulta = sConsulta & "set PER = @SUSTITUTO" & vbCrLf
sConsulta = sConsulta & "from INSTANCIA_EST IE WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "inner join PM_COPIA_ROL CR WITH (NOLOCK) on IE.INSTANCIA = CR.INSTANCIA and IE.ROL = CR.ID" & vbCrLf
sConsulta = sConsulta & "inner join INSTANCIA I WITH (NOLOCK) on IE.INSTANCIA = I.ID" & vbCrLf
sConsulta = sConsulta & "where CR.ROL_ORIGEN = @ROL" & vbCrLf
sConsulta = sConsulta & "and I.ESTADO >= 100" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "update PM_COPIA_ROL" & vbCrLf
sConsulta = sConsulta & "set PER = @SUSTITUTO, SUSTITUTO = @SUSTITUTO" & vbCrLf
sConsulta = sConsulta & "from PM_COPIA_ROL CR WITH (NOLOCK) inner join INSTANCIA I WITH (NOLOCK) on CR.INSTANCIA = I.ID" & vbCrLf
sConsulta = sConsulta & "where ROL_ORIGEN = @ROL" & vbCrLf
sConsulta = sConsulta & "and I.ESTADO >= 100" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- Solicitudes en curso" & vbCrLf
sConsulta = sConsulta & "if @TIPO_SUSTITUCION = 2" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "begin" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "update INSTANCIA" & vbCrLf
sConsulta = sConsulta & "set PETICIONARIO = @SUSTITUTO" & vbCrLf
sConsulta = sConsulta & "from INSTANCIA I WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "inner join PM_COPIA_ROL CR WITH (NOLOCK) on I.ID = CR.INSTANCIA and I.PETICIONARIO = CR.PER" & vbCrLf
sConsulta = sConsulta & "where CR.TIPO = 4" & vbCrLf
sConsulta = sConsulta & "and CR.ROL_ORIGEN = @ROL" & vbCrLf
sConsulta = sConsulta & "and I.ESTADO = 2" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "update VERSION_INSTANCIA" & vbCrLf
sConsulta = sConsulta & "set PER_MODIF = @SUSTITUTO" & vbCrLf
sConsulta = sConsulta & "from VERSION_INSTANCIA VI WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "inner join INSTANCIA_EST IE WITH (NOLOCK) on IE.INSTANCIA = VI.INSTANCIA" & vbCrLf
sConsulta = sConsulta & "inner join INSTANCIA_CAMINO IC WITH (NOLOCK) on IC.INSTANCIA_EST = IE.ID AND IC.VERSION = VI.NUM_VERSION" & vbCrLf
sConsulta = sConsulta & "inner join PM_COPIA_ROL CR WITH (NOLOCK) on VI.INSTANCIA = CR.INSTANCIA and VI.PER_MODIF = CR.PER AND CR.ID = IC.ROL" & vbCrLf
sConsulta = sConsulta & "inner join INSTANCIA I WITH (NOLOCK) on VI.INSTANCIA = I.ID" & vbCrLf
sConsulta = sConsulta & "where CR.ROL_ORIGEN = @ROL" & vbCrLf
sConsulta = sConsulta & "and I.ESTADO = 2" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "update INSTANCIA_EST" & vbCrLf
sConsulta = sConsulta & "set PER = @SUSTITUTO" & vbCrLf
sConsulta = sConsulta & "from INSTANCIA_EST IE WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "inner join PM_COPIA_ROL CR WITH (NOLOCK) on IE.INSTANCIA = CR.INSTANCIA and IE.ROL = CR.ID" & vbCrLf
sConsulta = sConsulta & "inner join INSTANCIA I WITH (NOLOCK) on IE.INSTANCIA = I.ID" & vbCrLf
sConsulta = sConsulta & "where CR.ROL_ORIGEN = @ROL" & vbCrLf
sConsulta = sConsulta & "and I.ESTADO = 2" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "update PM_COPIA_ROL" & vbCrLf
sConsulta = sConsulta & "set PER = @SUSTITUTO, SUSTITUTO = @SUSTITUTO" & vbCrLf
sConsulta = sConsulta & "from PM_COPIA_ROL CR WITH (NOLOCK) inner join INSTANCIA I WITH (NOLOCK) on CR.INSTANCIA = I.ID" & vbCrLf
sConsulta = sConsulta & "where ROL_ORIGEN = @ROL" & vbCrLf
sConsulta = sConsulta & "and I.ESTADO = 2" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE PM_INSTANCIAS_PENDIENTES" & vbCrLf
sConsulta = sConsulta & "SET PER=@SUSTITUTO" & vbCrLf
sConsulta = sConsulta & "       FROM PM_INSTANCIAS_PENDIENTES PIP WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PM_COPIA_ROL CR WITH (NOLOCK) ON PIP.ROL=CR.ID" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN INSTANCIA I WITH (NOLOCK) ON CR.INSTANCIA = I.ID" & vbCrLf
sConsulta = sConsulta & "WHERE ROL_ORIGEN = @ROL" & vbCrLf
sConsulta = sConsulta & "AND I.ESTADO = 2" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- Todas las solicitudes" & vbCrLf
sConsulta = sConsulta & "else" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "begin" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "update INSTANCIA" & vbCrLf
sConsulta = sConsulta & "set PETICIONARIO = @SUSTITUTO" & vbCrLf
sConsulta = sConsulta & "from INSTANCIA I WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "inner join PM_COPIA_ROL CR WITH (NOLOCK) on I.ID = CR.INSTANCIA and I.PETICIONARIO = CR.PER" & vbCrLf
sConsulta = sConsulta & "--      where CR.TIPO in (1,3,4)" & vbCrLf
sConsulta = sConsulta & "where CR.TIPO = 4" & vbCrLf
sConsulta = sConsulta & "and CR.ROL_ORIGEN = @ROL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "update VERSION_INSTANCIA" & vbCrLf
sConsulta = sConsulta & "set PER_MODIF = @SUSTITUTO" & vbCrLf
sConsulta = sConsulta & "from VERSION_INSTANCIA VI WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "inner join INSTANCIA_EST IE WITH (NOLOCK) on IE.INSTANCIA = VI.INSTANCIA" & vbCrLf
sConsulta = sConsulta & "inner join INSTANCIA_CAMINO IC WITH (NOLOCK) on IC.INSTANCIA_EST = IE.ID AND IC.VERSION = VI.NUM_VERSION" & vbCrLf
sConsulta = sConsulta & "inner join PM_COPIA_ROL CR WITH (NOLOCK) on VI.INSTANCIA = CR.INSTANCIA and VI.PER_MODIF = CR.PER AND CR.ID = IC.ROL" & vbCrLf
sConsulta = sConsulta & "inner join INSTANCIA I WITH (NOLOCK) on VI.INSTANCIA = I.ID" & vbCrLf
sConsulta = sConsulta & "where CR.ROL_ORIGEN = @ROL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "update INSTANCIA_EST" & vbCrLf
sConsulta = sConsulta & "set PER = @SUSTITUTO" & vbCrLf
sConsulta = sConsulta & "from INSTANCIA_EST IE WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "inner join PM_COPIA_ROL CR WITH (NOLOCK) on IE.INSTANCIA = CR.INSTANCIA and IE.ROL = CR.ID" & vbCrLf
sConsulta = sConsulta & "inner join INSTANCIA I WITH (NOLOCK) on IE.INSTANCIA = I.ID" & vbCrLf
sConsulta = sConsulta & "where CR.ROL_ORIGEN = @ROL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "update PM_COPIA_ROL" & vbCrLf
sConsulta = sConsulta & "set PER = @SUSTITUTO, SUSTITUTO = @SUSTITUTO" & vbCrLf
sConsulta = sConsulta & "from PM_COPIA_ROL CR WITH (NOLOCK) inner join INSTANCIA I WITH (NOLOCK) on CR.INSTANCIA = I.ID" & vbCrLf
sConsulta = sConsulta & "where ROL_ORIGEN = @ROL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- Solicitudes en curso. Finalizadas no tendran registro pero de tener NO tocar." & vbCrLf
sConsulta = sConsulta & "UPDATE PM_INSTANCIAS_PENDIENTES" & vbCrLf
sConsulta = sConsulta & "SET PER=@SUSTITUTO" & vbCrLf
sConsulta = sConsulta & "       FROM PM_INSTANCIAS_PENDIENTES PIP WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PM_COPIA_ROL CR WITH (NOLOCK) ON PIP.ROL=CR.ID" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN INSTANCIA I WITH (NOLOCK) ON CR.INSTANCIA = I.ID" & vbCrLf
sConsulta = sConsulta & "WHERE ROL_ORIGEN = @ROL" & vbCrLf
sConsulta = sConsulta & "AND I.ESTADO = 2" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSEP_GET_ORDENES_APROVISIONADOR]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSEP_GET_ORDENES_APROVISIONADOR]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSEP_GET_ORDENES_APROVISIONADOR" & vbCrLf
sConsulta = sConsulta & "   @NUMLINEAS INT OUTPUT," & vbCrLf
sConsulta = sConsulta & "   @APROVISIONADOR VARCHAR(50)," & vbCrLf
sConsulta = sConsulta & "   @EST INT=0," & vbCrLf
sConsulta = sConsulta & "   @PENDIENTESAPROBAR BIT=0," & vbCrLf
sConsulta = sConsulta & "   @PENDIENTESRECEPCIONAR BIT=0," & vbCrLf
sConsulta = sConsulta & "   @PERMISORECEPCIONAR_CC BIT=0," & vbCrLf
sConsulta = sConsulta & "   @VERENRECEPCIONPEDIDOS_CC BIT=0" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLQUERY NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLQUERYESTADOS NVARCHAR(2000) = ''" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLWHEREESTADOS NVARCHAR(2000) = ''" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLQUERYPENDIENTESAPROBAR NVARCHAR(2000) = ''" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLWHEREPENDIENTESAPROBAR NVARCHAR(2000) = ''" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLQUERYPENDIENTESRECEPCIONAR NVARCHAR(2000) = ''" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLWHEREPENDIENTESRECEPCIONAR NVARCHAR(2000) = ''" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLQUERYPENDIENTESRECEPCIONARCC NVARCHAR(2000) = ''" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLWHEREPENDIENTESRECEPCIONARCC NVARCHAR(2000) = ''" & vbCrLf
sConsulta = sConsulta & "DECLARE @IN NVARCHAR(20)='*2*3*4*5*'" & vbCrLf
sConsulta = sConsulta & "--AL MOSTRAR RECEPCIONES, No mostraremos los abonos" & vbCrLf
sConsulta = sConsulta & "DECLARE @ABONO NVARCHAR(10)" & vbCrLf
sConsulta = sConsulta & "SET @ABONO='*0*1*'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLORDERBY NVARCHAR(50) = '" & vbCrLf
sConsulta = sConsulta & "ORDER BY OE.FECHA DESC'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Si no hay ning?n filtro, no devolvemos ning?n valor" & vbCrLf
sConsulta = sConsulta & "IF @EST =0 AND @PENDIENTESAPROBAR = 0 AND @PENDIENTESRECEPCIONAR = 0" & vbCrLf
sConsulta = sConsulta & "SET @SQLWHEREESTADOS = '" & vbCrLf
sConsulta = sConsulta & "AND" & vbCrLf
sConsulta = sConsulta & "(OE.EST=-1)'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @EST>0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SQLQUERYESTADOS = '" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PEDIDO PE WITH (NOLOCK) ON OE.PEDIDO=PE.ID'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLWHEREESTADOS = '" & vbCrLf
sConsulta = sConsulta & "AND" & vbCrLf
sConsulta = sConsulta & "(PE.PER=@APROVISIONADOR" & vbCrLf
sConsulta = sConsulta & "AND (OE.EST<>0 OR (@EST & 1)>0) AND (OE.EST<>1 OR (@EST & 2)>0) AND (OE.EST<>2 OR (@EST & 4)>0) AND (OE.EST<>3 OR (@EST & 8)>0)" & vbCrLf
sConsulta = sConsulta & "AND (OE.EST<>4 OR (@EST & 16)>0) AND (OE.EST<>5 OR (@EST & 32)>0) AND (OE.EST<>6 OR (@EST & 64)>0)" & vbCrLf
sConsulta = sConsulta & "AND (OE.EST<>20 OR (@EST & 128)>0) AND (OE.EST<>21 OR (@EST & 256)>0) AND (OE.EST<>22 OR (@EST & 512)>0))'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @PENDIENTESAPROBAR = 1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SQLQUERYPENDIENTESAPROBAR = '" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN LINEAS_PEDIDO LP WITH (NOLOCK) ON LP.ORDEN = OE.ID" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN CATALOG_LIN CL ON CL.ID=LP.LINCAT" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN CATN1 WITH (NOLOCK) ON CATN1.ID=CL.SEGURIDAD_CATN AND CL.SEGURIDAD_NIVEL=1" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN CATN2 WITH (NOLOCK) ON CATN2.ID=CL.SEGURIDAD_CATN AND CL.SEGURIDAD_NIVEL=2" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN CATN3 WITH (NOLOCK) ON CATN3.ID=CL.SEGURIDAD_CATN AND CL.SEGURIDAD_NIVEL=3" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN CATN4 WITH (NOLOCK) ON CATN4.ID=CL.SEGURIDAD_CATN AND CL.SEGURIDAD_NIVEL=4" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN CATN5 WITH (NOLOCK) ON CATN5.ID=CL.SEGURIDAD_CATN AND CL.SEGURIDAD_NIVEL=5" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN LINEAS_EST LE WITH (NOLOCK) ON LE.LINEA=LP.ID AND LE.PEDIDO=LP.PEDIDO AND LE.ORDEN=LP.ORDEN" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PER WITH (NOLOCK) ON PER.COD=@APROVISIONADOR" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN INSTANCIA I WITH (NOLOCK) ON I.ID=LP.INSTANCIA_APROB" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN INSTANCIA_BLOQUE IB WITH (NOLOCK) ON I.ID=IB.INSTANCIA AND IB.EN_PROCESO=0 AND IB. ESTADO=1 AND IB.BLOQ<>1" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL_BLOQUE RB WITH (NOLOCK) ON RB.BLOQUE=IB.BLOQUE AND IB.ESTADO=1 AND IB.BLOQ<>1 AND IB.TRASLADADA<>1" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL R WITH (NOLOCK)  ON RB.ROL=R.ID" & vbCrLf
sConsulta = sConsulta & "   AND ISNULL(R.UON1,'''') = CASE WHEN R.UON1 IS NULL  THEN '''' ELSE PER.UON1 END AND ISNULL(R.UON2,'''') = CASE WHEN R.UON2 IS NULL  THEN '''' ELSE PER.UON2 END AND ISNULL(R.UON3,'''') = CASE WHEN R.UON3 IS NULL  THEN '''' ELSE PER.UON3 END AND ISNULL(R.PER,'''') = CASE WHEN R.PER IS NULL  THEN '''' ELSE PER.COD END'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLWHEREPENDIENTESAPROBAR = '" & vbCrLf
sConsulta = sConsulta & "AND" & vbCrLf
sConsulta = sConsulta & "(" & vbCrLf
sConsulta = sConsulta & "(" & vbCrLf
sConsulta = sConsulta & "(LP.NIVEL_APROB=0 AND COALESCE(CATN1.APROBADOR,CATN2.APROBADOR,CATN3.APROBADOR,CATN4.APROBADOR,CATN5.APROBADOR )=@APROVISIONADOR)" & vbCrLf
sConsulta = sConsulta & "OR" & vbCrLf
sConsulta = sConsulta & "(LE.EST=1 AND LE.PER=@APROVISIONADOR)" & vbCrLf
sConsulta = sConsulta & ")" & vbCrLf
sConsulta = sConsulta & "AND" & vbCrLf
sConsulta = sConsulta & "(NOT LP.EST IS NULL AND NOT LP.APROBADOR IS NULL)" & vbCrLf
sConsulta = sConsulta & "AND" & vbCrLf
sConsulta = sConsulta & "LP.EST = 0  --> PDTES DE APROBAR" & vbCrLf
sConsulta = sConsulta & "AND" & vbCrLf
sConsulta = sConsulta & "OE.EST < 2" & vbCrLf
sConsulta = sConsulta & ")" & vbCrLf
sConsulta = sConsulta & "   OR" & vbCrLf
sConsulta = sConsulta & "      (" & vbCrLf
sConsulta = sConsulta & "           OE.EST < 2" & vbCrLf
sConsulta = sConsulta & "           AND" & vbCrLf
sConsulta = sConsulta & "           LP.EST = 0  --> PDTES DE APROBAR" & vbCrLf
sConsulta = sConsulta & "           AND" & vbCrLf
sConsulta = sConsulta & "           LP.NIVEL_APROB = 1  --> (NIVEL1->SOLICITUD)" & vbCrLf
sConsulta = sConsulta & "      )'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @PENDIENTESRECEPCIONAR = 1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SQLWHEREPENDIENTESRECEPCIONAR = '" & vbCrLf
sConsulta = sConsulta & "(PATINDEX(''%*'' + CONVERT(VARCHAR,OE.EST) + ''*%'', @IN)>0" & vbCrLf
sConsulta = sConsulta & "AND (OE.EST<>2 OR OE.ACEP_PROVE =0)" & vbCrLf
sConsulta = sConsulta & "AND PATINDEX(''%*'' + CONVERT(VARCHAR,OE.ABONO) + ''*%'', @ABONO)>0" & vbCrLf
sConsulta = sConsulta & "AND (@APROVISIONADOR IS NULL OR OE.RECEPTOR=@APROVISIONADOR))'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @PERMISORECEPCIONAR_CC = 1 AND @VERENRECEPCIONPEDIDOS_CC = 1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SQLQUERYPENDIENTESRECEPCIONARCC = '" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN (SELECT DISTINCT ORDEN, ISNULL(UON1,'''')+ISNULL(UON2,'''')+ISNULL(UON3,'''')+ISNULL(UON4,'''') CCPEDIDO" & vbCrLf
sConsulta = sConsulta & "FROM LINEAS_PEDIDO LP WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN LINEAS_PED_IMPUTACION LPI WITH (NOLOCK) ON LPI.LINEA = LP.ID) IMP" & vbCrLf
sConsulta = sConsulta & "ON OE.ID = IMP.ORDEN'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLWHEREPENDIENTESRECEPCIONARCC = '" & vbCrLf
sConsulta = sConsulta & "(PATINDEX(''%*'' + CONVERT(VARCHAR,OE.EST) + ''*%'', @IN)>0" & vbCrLf
sConsulta = sConsulta & "AND (OE.EST<>2 OR OE.ACEP_PROVE =0)" & vbCrLf
sConsulta = sConsulta & "AND PATINDEX(''%*'' + CONVERT(VARCHAR,OE.ABONO) + ''*%'', @ABONO)>0" & vbCrLf
sConsulta = sConsulta & "AND (OE.RECEPTOR<>@APROVISIONADOR)" & vbCrLf
sConsulta = sConsulta & "AND IMP.CCPEDIDO IN" & vbCrLf
sConsulta = sConsulta & "(SELECT ISNULL(UON1,'''')+ISNULL(UON2,'''')+ISNULL(UON3,'''')+ISNULL(UON4,'''')" & vbCrLf
sConsulta = sConsulta & "FROM USU_CC_IMPUTACION UI WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN USU U WITH (NOLOCK) ON U.COD = UI.USU" & vbCrLf
sConsulta = sConsulta & "WHERE U.PER =@APROVISIONADOR))'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLWHEREPENDIENTESRECEPCIONAR = '" & vbCrLf
sConsulta = sConsulta & "AND" & vbCrLf
sConsulta = sConsulta & "(' + @SQLWHEREPENDIENTESRECEPCIONAR + ' OR ' + @SQLWHEREPENDIENTESRECEPCIONARCC + ')'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQLWHEREPENDIENTESRECEPCIONAR = '" & vbCrLf
sConsulta = sConsulta & "AND ' + @SQLWHEREPENDIENTESRECEPCIONAR" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @NUMLINEAS>0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET ROWCOUNT @NUMLINEAS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLQUERY = 'SELECT DISTINCT OE.ID, OE.ANYO, PE.NUM NUMPEDIDO, OE.NUM NUMORDEN, OE.PROVE PROVECOD, P.DEN PROVEDEN, OE.EST" & vbCrLf
sConsulta = sConsulta & ", OE.IMPORTE, OE.CAMBIO, OE.MON, OE.EMPRESA, E.DEN DENEMPRESA" & vbCrLf
sConsulta = sConsulta & "FROM ORDEN_ENTREGA OE WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PEDIDO PE WITH (NOLOCK) ON OE.PEDIDO=PE.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PROVE P WITH (NOLOCK) ON OE.PROVE=P.COD" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN EMP E WITH (NOLOCK) ON OE.EMPRESA=E.ID'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLQUERY += @SQLQUERYPENDIENTESAPROBAR + @SQLQUERYPENDIENTESRECEPCIONARCC +" & vbCrLf
sConsulta = sConsulta & "'" & vbCrLf
sConsulta = sConsulta & "WHERE 1=1 ' +" & vbCrLf
sConsulta = sConsulta & "@SQLWHEREESTADOS + @SQLWHEREPENDIENTESAPROBAR + @SQLWHEREPENDIENTESRECEPCIONAR" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLQUERY," & vbCrLf
sConsulta = sConsulta & "N'@APROVISIONADOR VARCHAR(50), @EST INT, @IN NVARCHAR(20), @ABONO NVARCHAR(10)'," & vbCrLf
sConsulta = sConsulta & "@APROVISIONADOR = @APROVISIONADOR," & vbCrLf
sConsulta = sConsulta & "@EST = @EST," & vbCrLf
sConsulta = sConsulta & "@IN = @IN," & vbCrLf
sConsulta = sConsulta & "@ABONO = @ABONO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET ROWCOUNT 0" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLQUERY = 'SELECT @NUMLINEAS=COUNT(DISTINCT OE.ID)" & vbCrLf
sConsulta = sConsulta & "FROM ORDEN_ENTREGA OE WITH (NOLOCK)'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLQUERY += @SQLQUERYESTADOS + @SQLQUERYPENDIENTESAPROBAR + @SQLQUERYPENDIENTESRECEPCIONARCC +" & vbCrLf
sConsulta = sConsulta & "'" & vbCrLf
sConsulta = sConsulta & "WHERE 1=1 ' +" & vbCrLf
sConsulta = sConsulta & "@SQLWHEREESTADOS + @SQLWHEREPENDIENTESAPROBAR + @SQLWHEREPENDIENTESRECEPCIONAR" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLQUERY," & vbCrLf
sConsulta = sConsulta & "N'@APROVISIONADOR VARCHAR(50), @EST INT, @IN NVARCHAR(20), @ABONO NVARCHAR(10), @NUMLINEAS INT OUTPUT'," & vbCrLf
sConsulta = sConsulta & "@APROVISIONADOR = @APROVISIONADOR," & vbCrLf
sConsulta = sConsulta & "@EST = @EST," & vbCrLf
sConsulta = sConsulta & "@IN = @IN," & vbCrLf
sConsulta = sConsulta & "@ABONO = @ABONO," & vbCrLf
sConsulta = sConsulta & "@NUMLINEAS = @NUMLINEAS OUTPUT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta


sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSEP_CARGAR_IMPUESTOS_ATRIBUTO]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSEP_CARGAR_IMPUESTOS_ATRIBUTO]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSEP_CARGAR_IMPUESTOS_ATRIBUTO @IDATRIB INT, @IDIOMA VARCHAR(3), @IDLINEA INT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT IMPPP.ID, IMPDEN.DEN, IMPPP.VALOR, IMP.COD" & vbCrLf
sConsulta = sConsulta & "FROM CATALOG_LIN_ATRIB CLA WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN IMPUESTO_CATALOG_LIN_ATRIB ICL WITH (NOLOCK) ON CLA.ID = ICL.CATALOG_LIN_ATRIB" & vbCrLf
sConsulta = sConsulta & "left JOIN IMPUESTO_PP_VALOR IMPPP WITH (NOLOCK) ON ICL.IMPUESTO_PP_VALOR = IMPPP.ID" & vbCrLf
sConsulta = sConsulta & "left JOIN IMPUESTO_PAIPROVI IMPPAI WITH (NOLOCK) ON IMPPP.IMPUESTO_PAIPROVI = IMPPAI.ID" & vbCrLf
sConsulta = sConsulta & "left JOIN IMPUESTO IMP WITH (NOLOCK) ON IMPPAI.IMPUESTO = IMP.ID" & vbCrLf
sConsulta = sConsulta & "left JOIN IMPUESTO_DEN IMPDEN WITH (NOLOCK) ON IMP.ID = IMPDEN.IMPUESTO AND IMPDEN.IDIOMA = @IDIOMA" & vbCrLf
sConsulta = sConsulta & "left JOIN CESTA C WITH (NOLOCK) ON CLA.CATALOG_LIN_ID=C.LINEA" & vbCrLf
sConsulta = sConsulta & "WHERE CLA.ATRIB = @IDATRIB AND C.ID=@IDLINEA" & vbCrLf
sConsulta = sConsulta & "AND IMPPP.VIGENTE = 1" & vbCrLf
sConsulta = sConsulta & "ORDER BY IMP.ID" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta


End Sub

Public Function CodigoDeActualizacion31900_09_2144_A31900_09_2145() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim oADOConnection As ADODB.Connection
    Dim sConsulta As String

    On Error GoTo Error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Actualiza Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    frmProgreso.ProgressBar1.Refresh
    V_31900_9_Storeds_2145

    sConsulta = "UPDATE VERSION SET NUM ='3.00.21.045'" & ", FECHA=CONVERT(datetime,'" & Format(Now, "mm-dd-yyyy hh:mm:ss") & "',110), PRODVER='31900.9'"
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False

    CodigoDeActualizacion31900_09_2144_A31900_09_2145 = True
    Exit Function

Error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If

    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    CodigoDeActualizacion31900_09_2144_A31900_09_2145 = False
End Function

Private Sub V_31900_9_Storeds_2145()
    Dim sConsulta As String

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSEP_REGISTRAR_CANTIDAD]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSEP_REGISTRAR_CANTIDAD]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSEP_REGISTRAR_CANTIDAD] @ORDEN int, @RECEP int, @LINEA int, @CANT float, @ACTIVO int, @PER varchar(40),@LOG tinyint,@IMPORTE float=null AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_LINEAS_RECEP INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @TIPORECEP smallint" & vbCrLf
sConsulta = sConsulta & "SELECT @TIPORECEP = TIPORECEPCION" & vbCrLf
sConsulta = sConsulta & "FROM LINEAS_PEDIDO WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE ID = @LINEA" & vbCrLf
sConsulta = sConsulta & "IF @TIPORECEP=1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   DECLARE @IMPORTE_REC FLOAT=0" & vbCrLf
sConsulta = sConsulta & "   SELECT @IMPORTE_REC=SUM(LINEAS_RECEP.IMPORTE)" & vbCrLf
sConsulta = sConsulta & "       FROM LINEAS_RECEP WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       GROUP BY LINEA" & vbCrLf
sConsulta = sConsulta & "       HAVING LINEA=@LINEA" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO LINEAS_RECEP (ORDEN,PEDIDO_RECEP,LINEA,CANT,IMPORTE )" & vbCrLf
sConsulta = sConsulta & "VALUES(@ORDEN,@RECEP,@LINEA,@CANT,@IMPORTE )" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @ID_LINEAS_RECEP=SCOPE_IDENTITY()" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @ESTANTIGUO smallint" & vbCrLf
sConsulta = sConsulta & "DECLARE @ESTNUEVO smallint" & vbCrLf
sConsulta = sConsulta & "SELECT @ESTANTIGUO = EST" & vbCrLf
sConsulta = sConsulta & "FROM LINEAS_PEDIDO WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE ID = @LINEA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @TIPORECEP=0" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       UPDATE LINEAS_PEDIDO" & vbCrLf
sConsulta = sConsulta & "          SET CANT_REC = CANT_REC + @CANT," & vbCrLf
sConsulta = sConsulta & "              EST = CASE WHEN CANT_PED = CANT_REC + @CANT THEN 3 ELSE CASE WHEN @CANT>0 THEN 2 ELSE EST END END," & vbCrLf
sConsulta = sConsulta & "              ACTIVO = @ACTIVO" & vbCrLf
sConsulta = sConsulta & "         FROM LINEAS_PEDIDO WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "        WHERE ID = @LINEA" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       UPDATE LINEAS_PEDIDO" & vbCrLf
sConsulta = sConsulta & "          SET EST = CASE WHEN IMPORTE_PED = @IMPORTE_REC + @IMPORTE THEN 3 ELSE CASE WHEN @IMPORTE>0 THEN 2 ELSE EST END END," & vbCrLf
sConsulta = sConsulta & "              ACTIVO = @ACTIVO" & vbCrLf
sConsulta = sConsulta & "         FROM LINEAS_PEDIDO WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "        WHERE ID = @LINEA" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @PEDIDO int" & vbCrLf
sConsulta = sConsulta & "SELECT @PEDIDO = PEDIDO, @ESTNUEVO = EST" & vbCrLf
sConsulta = sConsulta & "FROM LINEAS_PEDIDO WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE ID = @LINEA" & vbCrLf
sConsulta = sConsulta & "IF @ESTANTIGUO!=@ESTNUEVO" & vbCrLf
sConsulta = sConsulta & "INSERT INTO LINEAS_EST (PEDIDO, ORDEN, LINEA,EST,FECHA,PER)  VALUES (@PEDIDO, @ORDEN, @LINEA, @ESTNUEVO, GETDATE(),@PER)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF NOT @LOG IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_LOG_PEDIDO_RECEP INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @ID_LOG_PEDIDO_RECEP=LPR.ID FROM LOG_PEDIDO_RECEP LPR WITH(NOLOCK) WHERE LPR.ID_PEDIDO_RECEP=@RECEP" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF NOT @ID_LOG_PEDIDO_RECEP IS NULL" & vbCrLf
sConsulta = sConsulta & "INSERT INTO LOG_LINEAS_RECEP (ID_LOG_PEDIDO_RECEP,ID_PEDIDO_RECEP,ID_ORDEN_ENTREGA,ID_LINEA_RECEP,ID_LINEA_PEDIDO,CANT,ACCION,IMPORTE)" & vbCrLf
sConsulta = sConsulta & "VALUES(@ID_LOG_PEDIDO_RECEP,@RECEP,@ORDEN,@ID_LINEAS_RECEP,@LINEA,@CANT,'I',@IMPORTE)" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_SAVE_INSTANCIA_EN_FILTRO]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_SAVE_INSTANCIA_EN_FILTRO]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_SAVE_INSTANCIA_EN_FILTRO]" & vbCrLf
sConsulta = sConsulta & "@INSTANCIA INT," & vbCrLf
sConsulta = sConsulta & "@INSERT TINYINT," & vbCrLf
sConsulta = sConsulta & "@QA TINYINT=0," & vbCrLf
sConsulta = sConsulta & "@CONTRATO TINYINT=0" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @INSTANCIA_L INT" & vbCrLf
sConsulta = sConsulta & "SET @INSTANCIA_L=@INSTANCIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Obtenemos el nombre de las tablas din?micas del filtro (falta a?adir el prefijo del idioma)." & vbCrLf
sConsulta = sConsulta & "DECLARE @TABLA NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "IF @QA=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @CONTRATO=0" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT @TABLA=F.NOM_TABLA FROM INSTANCIA I WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_GRUPO G WITH (NOLOCK) ON I.ID=G.INSTANCIA" & vbCrLf
sConsulta = sConsulta & "INNER JOIN FORM_GRUPO G2 WITH (NOLOCK) ON G.GRUPO_ORIGEN=G2.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_FILTROS F WITH (NOLOCK) ON G2.FORMULARIO=F.FORMULARIO" & vbCrLf
sConsulta = sConsulta & "WHERE I.ID = @INSTANCIA_L" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT @TABLA=F.NOM_TABLA FROM INSTANCIA I WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_GRUPO G WITH (NOLOCK) ON I.ID=G.INSTANCIA" & vbCrLf
sConsulta = sConsulta & "INNER JOIN FORM_GRUPO G2 WITH (NOLOCK) ON G.GRUPO_ORIGEN=G2.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_CONTR_FILTROS F WITH (NOLOCK) ON G2.FORMULARIO=F.FORMULARIO" & vbCrLf
sConsulta = sConsulta & "WHERE I.ID = @INSTANCIA_L" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT @TABLA=F.NOM_TABLA FROM INSTANCIA I WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_GRUPO G WITH (NOLOCK) ON I.ID=G.INSTANCIA" & vbCrLf
sConsulta = sConsulta & "INNER JOIN FORM_GRUPO G2 WITH (NOLOCK) ON G.GRUPO_ORIGEN=G2.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN QA_FILTROS F WITH (NOLOCK) ON G2.FORMULARIO=F.FORMULARIO" & vbCrLf
sConsulta = sConsulta & "WHERE I.ID = @INSTANCIA_L" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Obtenemos las longitudes de los materiales de la tabla DIC" & vbCrLf
sConsulta = sConsulta & "DECLARE @LEN_GMN1 INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @LEN_GMN2 INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @LEN_GMN3 INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @LEN_GMN4 INT" & vbCrLf
sConsulta = sConsulta & "SELECT @LEN_GMN1=DIC.LONGITUD, @LEN_GMN2=DIC2.LONGITUD,@LEN_GMN3=DIC3.LONGITUD, @LEN_GMN4=DIC4.LONGITUD FROM DIC WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN DIC DIC2 WITH (NOLOCK) ON DIC2.NOMBRE='GMN2'" & vbCrLf
sConsulta = sConsulta & "INNER JOIN DIC DIC3 WITH (NOLOCK) ON DIC3.NOMBRE='GMN3'" & vbCrLf
sConsulta = sConsulta & "INNER JOIN DIC DIC4 WITH (NOLOCK) ON DIC4.NOMBRE='GMN4'" & vbCrLf
sConsulta = sConsulta & "WHERE DIC.NOMBRE = 'GMN1'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Guardamos los IDs en una tabla temporal" & vbCrLf
sConsulta = sConsulta & "--Es lo ?nico que cambia de los 3 casos (QA, Solicitudes y Contratos)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLINSERT NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE #G1 (ID_CAMPO INT)" & vbCrLf
sConsulta = sConsulta & "SET @SQLINSERT = N'INSERT INTO #G1 SELECT ID_CAMPO FROM '" & vbCrLf
sConsulta = sConsulta & "IF @QA=0" & vbCrLf
sConsulta = sConsulta & "IF @CONTRATO = 0" & vbCrLf
sConsulta = sConsulta & "SET @SQLINSERT = @SQLINSERT + N'UPD_VISORES_PM WITH(NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQLINSERT = @SQLINSERT + N'UPD_VISORES_CONTR WITH(NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQLINSERT = @SQLINSERT + N'UPD_VISORES_QA WITH(NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "SET @SQLINSERT = @SQLINSERT + N'WHERE (ACCION=''I'' OR ACCION=''A'') AND (NOM_TABLA=@TABLA OR NOM_TABLA=@TABLA+''_DESGLOSE'') AND OK=0'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLINSERT, N'@TABLA NVARCHAR(100)', @TABLA=@TABLA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @LINEA INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @LINEA_ANT INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @CAMPO_ORIGEN INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @SUBTIPO INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @TIPO_CAMPO_GS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @VALOR_NUM FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @VALOR_TEXT NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @VALOR_FEC DATETIME" & vbCrLf
sConsulta = sConsulta & "DECLARE @VALOR_BOOL TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @VALOR_TEXT_DEN NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @VALOR_TEXT_COD NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @INTRO TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_ATRIB_GS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @TABLA_EXTERNA INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @CAMPO_DEF INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL_COD NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLCAMPOSINSERT NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @EJECUTARSQL TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @CAMPO_PADRE_DESGLOSE INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @CAMPO_PADRE_DESGLOSE_ANT INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @DENOMINACION NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @CAMPOTRATADO TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @EMPIEZAIDIOMA INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ACABADENOMINACION INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @EMPIEZADENOMINACION INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @SEPARADOR INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRESUPUESTOS NVARCHAR(400)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PORCENTAJE NVARCHAR(400)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SEPARADOR_DEN INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @SEPARADOR_PORCENTAJE INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDIOMA AS NVARCHAR(400)" & vbCrLf
sConsulta = sConsulta & "DECLARE curIDIOMAS CURSOR LOCAL FOR" & vbCrLf
sConsulta = sConsulta & "SELECT COD FROM IDIOMAS WITH(NOLOCK) WHERE APLICACION=1" & vbCrLf
sConsulta = sConsulta & "OPEN curIDIOMAS" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curIDIOMAS INTO @IDIOMA" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SQL=''" & vbCrLf
sConsulta = sConsulta & "SET @SQLCAMPOSINSERT=''" & vbCrLf
sConsulta = sConsulta & "SET @EJECUTARSQL=0" & vbCrLf
sConsulta = sConsulta & "SET @LINEA_ANT=1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @INSERT = 1 AND @CONTRATO = 1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'INSERT INTO ' + @IDIOMA + N'_CONTR_FILTRO1 VALUES (@INSTANCIA_L)'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA_L INT', @INSTANCIA_L=@INSTANCIA_L" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @INSERT = 1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SQLCAMPOSINSERT=N'INSERT INTO '+@IDIOMA+N'_'+@TABLA+N' (INSTANCIA,'" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N' VALUES (@INSTANCIA_L,'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=N' UPDATE ' + @IDIOMA + N'_' + @TABLA  + N' SET '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE curSOLICITUDES_CAMPOS CURSOR LOCAL FOR" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT CD.CAMPO_ORIGEN, CD.SUBTIPO, CD.TIPO_CAMPO_GS, C.VALOR_TEXT, C.VALOR_NUM," & vbCrLf
sConsulta = sConsulta & "C.VALOR_FEC, C.VALOR_BOOL, C.VALOR_TEXT_COD, C.VALOR_TEXT_DEN, CD.INTRO, CD.ID_ATRIB_GS," & vbCrLf
sConsulta = sConsulta & "CD.TABLA_EXTERNA, C.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "FROM COPIA_CAMPO_DEF CD WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_CAMPO C WITH(NOLOCK) ON CD.ID=C.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "INNER JOIN INSTANCIA I WITH(NOLOCK) ON C.INSTANCIA=I.ID AND C.NUM_VERSION=I.ULT_VERSION" & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_GRUPO G WITH (NOLOCK) ON I.ID=G.INSTANCIA" & vbCrLf
sConsulta = sConsulta & "INNER JOIN FORM_GRUPO G2 WITH (NOLOCK) ON G.GRUPO_ORIGEN=G2.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN FORMULARIO F WITH(NOLOCK) ON F.ID=G2.FORMULARIO" & vbCrLf
sConsulta = sConsulta & "INNER JOIN FORM_GRUPO FG WITH (NOLOCK) ON F.ID=FG.FORMULARIO" & vbCrLf
sConsulta = sConsulta & "INNER JOIN FORM_CAMPO FC WITH (NOLOCK) ON FG.ID = FC.GRUPO" & vbCrLf
sConsulta = sConsulta & "WHERE CD.ES_SUBCAMPO=0 AND CD.SUBTIPO<>9 AND (CD.TIPO_CAMPO_GS<>106 OR CD.TIPO_CAMPO_GS IS NULL)" & vbCrLf
sConsulta = sConsulta & "AND CD.CAMPO_ORIGEN = FC.ID AND (CD.SUBTIPO=FC.SUBTIPO" & vbCrLf
sConsulta = sConsulta & "OR (CD.SUBTIPO=5 AND FC.SUBTIPO IN (6,7)) OR (CD.SUBTIPO=6 AND FC.SUBTIPO=7))  --si la definici?n del campo ha cambiado de texto corto a texto medio o largo, o de texto medio a texto largo" & vbCrLf
sConsulta = sConsulta & "AND CD.CAMPO_ORIGEN NOT IN (SELECT ID_CAMPO FROM #G1)  -- campos que se han a?adido en el formulario pero no se ha hecho todavia ALTER en las tablas de filtros (BatchUpdates)" & vbCrLf
sConsulta = sConsulta & "AND I.ID=@INSTANCIA_L" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "OPEN curSOLICITUDES_CAMPOS" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curSOLICITUDES_CAMPOS INTO @CAMPO_ORIGEN, @SUBTIPO, @TIPO_CAMPO_GS, @VALOR_TEXT, @VALOR_NUM, @VALOR_FEC, @VALOR_BOOL, @VALOR_TEXT_COD, @VALOR_TEXT_DEN, @INTRO, @ID_ATRIB_GS,@TABLA_EXTERNA, @CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @EJECUTARSQL=1" & vbCrLf
sConsulta = sConsulta & "--Reemplazamos las comillas simples en valor_text y valor_text_den" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @VALOR_TEXT = SUBSTRING(@VALOR_TEXT,1,100)" & vbCrLf
sConsulta = sConsulta & "SET @VALOR_TEXT = REPLACE(@VALOR_TEXT,'''','''''')" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT_DEN IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SET @VALOR_TEXT_DEN = REPLACE(@VALOR_TEXT_DEN,'''','''''')" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "   IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "       SET @SQLCAMPOSINSERT = @SQLCAMPOSINSERT + 'C_' + CONVERT(NVARCHAR,@CAMPO_ORIGEN)  + ','" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "       SET @SQLCAMPOSINSERT = @SQLCAMPOSINSERT + N'C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N','" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = @SQL + 'C_' + CONVERT(NVARCHAR,@CAMPO_ORIGEN) + '='" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = @SQL +N'C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'='" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "----------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @SUBTIPO = 3 -- tipo fecha" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_FEC IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'CONVERT(DATETIME,''' + CONVERT(VARCHAR,@VALOR_FEC) + ''')'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @SUBTIPO = 4 -- tipo booleano" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_BOOL IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + CONVERT(NVARCHAR,@VALOR_BOOL)" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @SUBTIPO = 8 -- tipo archivo" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'''' + @VALOR_TEXT + N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @TIPO_CAMPO_GS IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @TABLA_EXTERNA IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @SUBTIPO=2 -- tipo num?rico" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_NUM IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + CONVERT(NVARCHAR,@VALOR_NUM)" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE    -- tipo texto o tipo lista texto" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1 AND @QA=0 AND @CONTRATO=0" & vbCrLf
sConsulta = sConsulta & "   SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "IF @INTRO=1 AND @VALOR_NUM IS NOT NULL --tipo lista texto" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @ID_ATRIB_GS IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @IDIOMA = 'SPA'" & vbCrLf
sConsulta = sConsulta & "SELECT @DENOMINACION=VALOR_TEXT_SPA" & vbCrLf
sConsulta = sConsulta & "FROM COPIA_CAMPO_VALOR_LISTA WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE CAMPO_DEF=CONVERT(VARCHAR,@CAMPO_DEF) AND ORDEN=CONVERT(VARCHAR,@VALOR_NUM)" & vbCrLf
sConsulta = sConsulta & "IF @IDIOMA = 'ENG'" & vbCrLf
sConsulta = sConsulta & "SELECT @DENOMINACION=VALOR_TEXT_ENG" & vbCrLf
sConsulta = sConsulta & "FROM COPIA_CAMPO_VALOR_LISTA WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE CAMPO_DEF=CONVERT(VARCHAR,@CAMPO_DEF) AND ORDEN=CONVERT(VARCHAR,@VALOR_NUM)" & vbCrLf
sConsulta = sConsulta & "IF @IDIOMA = 'GER'" & vbCrLf
sConsulta = sConsulta & "SELECT @DENOMINACION=VALOR_TEXT_GER" & vbCrLf
sConsulta = sConsulta & "FROM COPIA_CAMPO_VALOR_LISTA WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE CAMPO_DEF = CONVERT(VARCHAR,@CAMPO_DEF) AND ORDEN=CONVERT(VARCHAR,@VALOR_NUM)" & vbCrLf
sConsulta = sConsulta & "IF @IDIOMA = 'FRA'" & vbCrLf
sConsulta = sConsulta & "SELECT @DENOMINACION=VALOR_TEXT_FRA" & vbCrLf
sConsulta = sConsulta & "FROM COPIA_CAMPO_VALOR_LISTA WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE CAMPO_DEF = CONVERT(VARCHAR,@CAMPO_DEF) AND ORDEN=CONVERT(VARCHAR,@VALOR_NUM)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @DENOMINACION IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + '''' + replace(@DENOMINACION,'''','''''') + ''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+REPLACE(@DENOMINACION,'''','''''')+''''+N',CONVERT(NVARCHAR,'+CONVERT(NVARCHAR,@VALOR_NUM)+N')'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+REPLACE(@DENOMINACION,'''','''''')+''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=CONVERT(NVARCHAR,'+CONVERT(NVARCHAR,@VALOR_NUM)+N')'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + N'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + N'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + '''' + @VALOR_TEXT + ''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + N''''+@VALOR_TEXT+N''''+N','+N''''+CONVERT(NVARCHAR,@VALOR_NUM)+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + N''''+@VALOR_TEXT+N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+ CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N''''+CONVERT(NVARCHAR,@VALOR_NUM)+N''''" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE -- tipo texto" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + '''' + @VALOR_TEXT + ''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + N''''+@VALOR_TEXT+N''''+N','+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + N''''+@VALOR_TEXT+N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+ CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + N'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @SUBTIPO=2 -- tipo num?rico" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_NUM IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + CONVERT(NVARCHAR,@VALOR_NUM)" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE    -- tipo texto o tipo lista texto" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1 AND @QA=0 AND @CONTRATO=0" & vbCrLf
sConsulta = sConsulta & "   SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT_DEN IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + '''' + @VALOR_TEXT_DEN + ''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+@VALOR_TEXT_DEN+N''''+N','+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+@VALOR_TEXT_DEN+N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_' +CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE -- campo del sistema" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @CAMPOTRATADO = 0" & vbCrLf
sConsulta = sConsulta & "---------------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "-- Formas de pago, Monedas, Familia de materiales, Unidades, Destinos, Pa?ses y Provincias" & vbCrLf
sConsulta = sConsulta & "-- FORMATO: <Idioma>|<Texto>#<Idioma>|<Texto>" & vbCrLf
sConsulta = sConsulta & "-- FORMATO MATERIALES: cod. ultimo nivel (VALOR_TEXT) +  <Idioma>|<Texto>#<Idioma>|<Texto>" & vbCrLf
sConsulta = sConsulta & "------------------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "IF @TIPO_CAMPO_GS = 101 OR @TIPO_CAMPO_GS = 102 OR @TIPO_CAMPO_GS = 103 OR @TIPO_CAMPO_GS = 105 OR @TIPO_CAMPO_GS = 109 OR @TIPO_CAMPO_GS = 107 OR @TIPO_CAMPO_GS = 108" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1 AND @QA=0 AND @CONTRATO=0" & vbCrLf
sConsulta = sConsulta & "   SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+ N'_COD,'" & vbCrLf
sConsulta = sConsulta & "SET @CAMPOTRATADO = 1" & vbCrLf
sConsulta = sConsulta & "-- Materiales" & vbCrLf
sConsulta = sConsulta & "IF @TIPO_CAMPO_GS = 103" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "-- Al hacer Len de valor_text me est? quitando los espacios en blanco de la derecha" & vbCrLf
sConsulta = sConsulta & "IF LEN(@VALOR_TEXT + '#')-1 = @LEN_GMN1 + @LEN_GMN2 + @LEN_GMN3 + @LEN_GMN4" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'''' + RTRIM(RIGHT(@VALOR_TEXT,@LEN_GMN4)) + N' - '" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF LEN(@VALOR_TEXT + '#')-1 = @LEN_GMN1 + @LEN_GMN2 + @LEN_GMN3" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'''' + RTRIM(RIGHT(@VALOR_TEXT,@LEN_GMN3)) + N' - '" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF LEN(@VALOR_TEXT + '#')-1 = @LEN_GMN1 + @LEN_GMN2" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'''' + RTRIM(RIGHT(@VALOR_TEXT,@LEN_GMN2)) + N' - '" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'''' + RTRIM(RIGHT(@VALOR_TEXT,@LEN_GMN1)) + N' - '" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- Todos" & vbCrLf
sConsulta = sConsulta & "SET @EMPIEZAIDIOMA=CHARINDEX(@IDIOMA + '|',@VALOR_TEXT_DEN)" & vbCrLf
sConsulta = sConsulta & "IF (@EMPIEZAIDIOMA) > 0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @ACABADENOMINACION = CHARINDEX('#',@VALOR_TEXT_DEN,@EMPIEZAIDIOMA)" & vbCrLf
sConsulta = sConsulta & "SET @EMPIEZADENOMINACION = CHARINDEX('|',@VALOR_TEXT_DEN,@EMPIEZAIDIOMA)+1" & vbCrLf
sConsulta = sConsulta & "IF @TIPO_CAMPO_GS <> 103" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+''''" & vbCrLf
sConsulta = sConsulta & "IF @ACABADENOMINACION>0" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+SUBSTRING(@VALOR_TEXT_DEN,@EMPIEZADENOMINACION,@ACABADENOMINACION-@EMPIEZADENOMINACION) + ''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+RIGHT(@VALOR_TEXT_DEN,LEN(@VALOR_TEXT_DEN)-@EMPIEZADENOMINACION+1) + ''''" & vbCrLf
sConsulta = sConsulta & "IF @QA=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N','+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N',C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT IS NOT NULL AND @TIPO_CAMPO_GS = 103" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+N','+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "-- Proveedores, Departamentos, Organizaciones de Compras, Centros,Proveedor ERP" & vbCrLf
sConsulta = sConsulta & "-- FORMATO: C?digo (VALOR_TEXT) - Denominaci?n (VALOR_TEXT_DEN)" & vbCrLf
sConsulta = sConsulta & "--------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "IF @TIPO_CAMPO_GS = 11 OR @TIPO_CAMPO_GS = 100  OR @TIPO_CAMPO_GS = 122 OR @TIPO_CAMPO_GS = 123 OR @TIPO_CAMPO_GS = 124 OR @TIPO_CAMPO_GS = 143" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @CAMPOTRATADO = 1" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1 AND @QA=0 AND @CONTRATO=0" & vbCrLf
sConsulta = sConsulta & "   SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + '''' + @VALOR_TEXT + ' - ' + @VALOR_TEXT_DEN + ''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+@VALOR_TEXT+N' - '+@VALOR_TEXT_DEN+N''''+N','+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+@VALOR_TEXT+N' - '+@VALOR_TEXT_DEN+N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "-- Centros de coste, Partidas presupuestarias, Activos" & vbCrLf
sConsulta = sConsulta & "-- FORMATO: cod. ultimo nivel (VALOR_TEXT) +  <Idioma>|<Texto>#<Idioma>|<Texto>" & vbCrLf
sConsulta = sConsulta & "--------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "IF @TIPO_CAMPO_GS = 129 OR @TIPO_CAMPO_GS = 130 OR @TIPO_CAMPO_GS = 131 OR @TIPO_CAMPO_GS=132" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @CAMPOTRATADO=1" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1 AND @QA=0 AND @CONTRATO=0" & vbCrLf
sConsulta = sConsulta & "   SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SEPARADOR = CHARINDEX('#',@VALOR_TEXT,1)" & vbCrLf
sConsulta = sConsulta & "WHILE @SEPARADOR > 0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @VALOR_TEXT = RIGHT(@VALOR_TEXT,LEN(@VALOR_TEXT)-@SEPARADOR)" & vbCrLf
sConsulta = sConsulta & "SET @SEPARADOR = CHARINDEX('#',@VALOR_TEXT,1)" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SEPARADOR = CHARINDEX('|',@VALOR_TEXT,1)" & vbCrLf
sConsulta = sConsulta & "WHILE @SEPARADOR > 0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @VALOR_TEXT = RIGHT(@VALOR_TEXT,LEN(@VALOR_TEXT)-@SEPARADOR)" & vbCrLf
sConsulta = sConsulta & "SET @SEPARADOR = CHARINDEX('|',@VALOR_TEXT,1)" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + '''' + @VALOR_TEXT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Denominaci?n en el idioma" & vbCrLf
sConsulta = sConsulta & "SET @EMPIEZAIDIOMA=CHARINDEX(@IDIOMA + '|',@VALOR_TEXT_DEN)" & vbCrLf
sConsulta = sConsulta & "IF (@EMPIEZAIDIOMA) > 0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @ACABADENOMINACION = CHARINDEX('#',@VALOR_TEXT_DEN,@EMPIEZAIDIOMA)" & vbCrLf
sConsulta = sConsulta & "SET @EMPIEZADENOMINACION = CHARINDEX('|',@VALOR_TEXT_DEN,@EMPIEZAIDIOMA)+1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ACABADENOMINACION>0" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ' - ' + SUBSTRING(@VALOR_TEXT_DEN,@EMPIEZADENOMINACION,@ACABADENOMINACION-@EMPIEZADENOMINACION)" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ' - ' + RIGHT(@VALOR_TEXT_DEN,LEN(@VALOR_TEXT_DEN)-@EMPIEZADENOMINACION+1)" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+N','+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "-- Personas, Comprador" & vbCrLf
sConsulta = sConsulta & "---------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "IF @TIPO_CAMPO_GS = 115 OR @TIPO_CAMPO_GS = 144" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @CAMPOTRATADO = 1" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1 AND @QA=0 AND @CONTRATO=0" & vbCrLf
sConsulta = sConsulta & "   SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT_DEN IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + '''' + @VALOR_TEXT_DEN + ''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+@VALOR_TEXT_DEN+N''''+N','+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+@VALOR_TEXT_DEN+N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "-- Almacen" & vbCrLf
sConsulta = sConsulta & "---------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "IF @TIPO_CAMPO_GS = 125" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @CAMPOTRATADO = 1" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1 AND @QA=0 AND @CONTRATO=0" & vbCrLf
sConsulta = sConsulta & "   SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT_DEN IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + '''' + @VALOR_TEXT_DEN + ''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+@VALOR_TEXT_DEN+N''''+N','+N''''+CONVERT(NVARCHAR,@VALOR_NUM)+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+@VALOR_TEXT_DEN+N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N''''+CONVERT(NVARCHAR,@VALOR_NUM)+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "------------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "-- Cod. Art?culo (viejo), Cod. Art?culo (nuevo), Denominaci?n del art?culo y Unidades Organizativas" & vbCrLf
sConsulta = sConsulta & "------------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "IF @TIPO_CAMPO_GS = 104  OR @TIPO_CAMPO_GS = 118 OR @TIPO_CAMPO_GS = 119 OR @TIPO_CAMPO_GS = 121" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @CAMPOTRATADO = 1" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1 AND @QA=0 AND @CONTRATO=0" & vbCrLf
sConsulta = sConsulta & "   SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + '''' + @VALOR_TEXT + ''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+@VALOR_TEXT+N''''+N','+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+@VALOR_TEXT +N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "---------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "-- Presupuestos" & vbCrLf
sConsulta = sConsulta & "-- FORMATO: Ej 2005 - PC-002  (50.00%); 2005 - PC-001 (50.00%);" & vbCrLf
sConsulta = sConsulta & "--  VALOR_TEXT_DEN: 2005 - PC-002#2005 - PC-001" & vbCrLf
sConsulta = sConsulta & "--  VALOR_TEXT: 1_11_0.5#1_10_0.5" & vbCrLf
sConsulta = sConsulta & "---------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "IF @TIPO_CAMPO_GS = 110  OR @TIPO_CAMPO_GS = 111 OR @TIPO_CAMPO_GS = 112 OR @TIPO_CAMPO_GS = 113" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @CAMPOTRATADO = 1" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1 AND @QA=0 AND @CONTRATO=0" & vbCrLf
sConsulta = sConsulta & "   SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "SET @PRESUPUESTOS = ''" & vbCrLf
sConsulta = sConsulta & "SET @SEPARADOR_DEN = CHARINDEX('#',@VALOR_TEXT_DEN)" & vbCrLf
sConsulta = sConsulta & "WHILE @SEPARADOR_DEN > 0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @PRESUPUESTOS = @PRESUPUESTOS + SUBSTRING(@VALOR_TEXT_DEN,1,@SEPARADOR_DEN-1)" & vbCrLf
sConsulta = sConsulta & "SET @VALOR_TEXT_DEN = RIGHT(@VALOR_TEXT_DEN,LEN(@VALOR_TEXT_DEN)-@SEPARADOR_DEN)" & vbCrLf
sConsulta = sConsulta & "SET @PORCENTAJE = SUBSTRING(@VALOR_TEXT,1,CHARINDEX('#',@VALOR_TEXT)-1)" & vbCrLf
sConsulta = sConsulta & "SET @SEPARADOR_PORCENTAJE = 1" & vbCrLf
sConsulta = sConsulta & "WHILE @SEPARADOR_PORCENTAJE > 0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @PORCENTAJE = RIGHT(@PORCENTAJE,LEN(@PORCENTAJE)-@SEPARADOR_PORCENTAJE)" & vbCrLf
sConsulta = sConsulta & "SET @SEPARADOR_PORCENTAJE = CHARINDEX('_',@PORCENTAJE,@SEPARADOR_PORCENTAJE+1)" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "SET @PRESUPUESTOS = @PRESUPUESTOS + '(' + CONVERT(VARCHAR,CONVERT(DECIMAL(3,2),@PORCENTAJE)*100) + '%); '" & vbCrLf
sConsulta = sConsulta & "SET @VALOR_TEXT = RIGHT(@VALOR_TEXT,LEN(@VALOR_TEXT)-CHARINDEX('#',@VALOR_TEXT))" & vbCrLf
sConsulta = sConsulta & "SET @SEPARADOR_DEN = CHARINDEX('#',@VALOR_TEXT_DEN)" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "SET @PORCENTAJE = @VALOR_TEXT" & vbCrLf
sConsulta = sConsulta & "SET @SEPARADOR_PORCENTAJE = 1" & vbCrLf
sConsulta = sConsulta & "WHILE @SEPARADOR_PORCENTAJE > 0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @PORCENTAJE = RIGHT(@PORCENTAJE,LEN(@PORCENTAJE)-@SEPARADOR_PORCENTAJE)" & vbCrLf
sConsulta = sConsulta & "SET @SEPARADOR_PORCENTAJE = CHARINDEX('_',@PORCENTAJE,@SEPARADOR_PORCENTAJE+1)" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @PRESUPUESTOS = @PRESUPUESTOS + @VALOR_TEXT_DEN + '(' + CAST(CONVERT(FLOAT,@PORCENTAJE)*100 AS VARCHAR) + '%);'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @PRESUPUESTOS <> ''" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + '''' + @PRESUPUESTOS + ''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'''' + @PRESUPUESTOS + N'''' + N',' + N'''' + @VALOR_TEXT + N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'''' + @PRESUPUESTOS + N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL,'+N'C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CAMPOTRATADO = 0 -- Resto" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @SUBTIPO = 3 -- tipo fecha" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_FEC IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + '''' + @VALOR_FEC + ''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @SUBTIPO = 4 -- tipo booleano" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_BOOL IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + CONVERT(VARCHAR,@VALOR_BOOL)" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @SUBTIPO=2 -- tipo num?rico" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_NUM IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + CONVERT(VARCHAR,@VALOR_NUM)" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE    -- tipo texto" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1 AND @QA=0 AND @CONTRATO=0" & vbCrLf
sConsulta = sConsulta & "   SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + '''' + @VALOR_TEXT + ''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+@VALOR_TEXT+N''''+N','+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+''''+@VALOR_TEXT+''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+ N'_COD='+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+ N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ','" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curSOLICITUDES_CAMPOS INTO @CAMPO_ORIGEN, @SUBTIPO, @TIPO_CAMPO_GS, @VALOR_TEXT, @VALOR_NUM, @VALOR_FEC, @VALOR_BOOL, @VALOR_TEXT_COD, @VALOR_TEXT_DEN, @INTRO, @ID_ATRIB_GS,@TABLA_EXTERNA, @CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CLOSE curSOLICITUDES_CAMPOS" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curSOLICITUDES_CAMPOS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @EJECUTARSQL=1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @INSERT = 1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = LEFT(@SQLCAMPOSINSERT,LEN(@SQLCAMPOSINSERT)-1) + ') ' + LEFT(@SQL,LEN(@SQL)-1) + ')'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = LEFT(@SQL,LEN(@SQL)-1) + ' WHERE INSTANCIA = @INSTANCIA_L'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL=N'IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = ''dbo'' AND  TABLE_NAME='''+@IDIOMA+N'_'+@TABLA+''')) BEGIN '+@SQL+N' END'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA_L INT', @INSTANCIA_L=@INSTANCIA_L" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--*********************************" & vbCrLf
sConsulta = sConsulta & "--*********************************" & vbCrLf
sConsulta = sConsulta & "-- AHORA LOS CAMPOS DE LOS DESGLOSES (SI NO ES QA NI CONTRATO)" & vbCrLf
sConsulta = sConsulta & "--**********************************" & vbCrLf
sConsulta = sConsulta & "IF @QA=0 AND @CONTRATO=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SQL=''" & vbCrLf
sConsulta = sConsulta & "SET @SQLCAMPOSINSERT=''" & vbCrLf
sConsulta = sConsulta & "SET @EJECUTARSQL=0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Pongo insert a 1 para que siempre inserte, ya que si de una etapa a otra se crean nuevas lineas de desglose s?lo estar?a haciendo update de las lineas existentes" & vbCrLf
sConsulta = sConsulta & "--Ahora elimino todas las l?neas y vuelvo a insertar las que haya en ese momento, as? tambi?n controlo si se elimina alguna l?nea" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLDELETE NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @INSERT_ORIGINAL TINYINT" & vbCrLf
sConsulta = sConsulta & "SET @SQLDELETE=''" & vbCrLf
sConsulta = sConsulta & "SET @INSERT_ORIGINAL=@INSERT" & vbCrLf
sConsulta = sConsulta & "SET @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQLDELETE='DELETE '+@IDIOMA+N'_'+@TABLA+N'_DESGLOSE WHERE FORM_INSTANCIA=@INSTANCIA_L'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLDELETE, N'@INSTANCIA_L INT',@INSTANCIA_L=@INSTANCIA_L" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @INSERT = 1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SQLCAMPOSINSERT=N'INSERT INTO '+@IDIOMA+N'_'+@TABLA+N'_DESGLOSE (FORM_INSTANCIA,LINEA,DESGLOSE,'" & vbCrLf
sConsulta = sConsulta & "SET @SQL=N' VALUES (@INSTANCIA_L,@LINEA,'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=N' UPDATE '+@IDIOMA+N'_'+@TABLA+N'_DESGLOSE SET '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE curSOLICITUDES_CAMPOS_DESGLOSE CURSOR LOCAL FOR" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT CLD.LINEA,CCD.CAMPO_ORIGEN,CCD.SUBTIPO,CCD.TIPO_CAMPO_GS,CLD.VALOR_TEXT,CLD.VALOR_NUM,CLD.VALOR_FEC,CLD.VALOR_BOOL," & vbCrLf
sConsulta = sConsulta & "CLD.VALOR_TEXT_COD,CLD.VALOR_TEXT_DEN,CCD.INTRO,CCD.ID_ATRIB_GS,CCD.TABLA_EXTERNA,CC.COPIA_CAMPO_DEF," & vbCrLf
sConsulta = sConsulta & "D.CAMPO_PADRE" & vbCrLf
sConsulta = sConsulta & "FROM COPIA_CAMPO_DEF CCD WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_CAMPO CC WITH(NOLOCK) ON CCD.ID=CC.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "INNER JOIN INSTANCIA I WITH(NOLOCK) ON CC.INSTANCIA=I.ID AND CC.NUM_VERSION=I.ULT_VERSION" & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_GRUPO G WITH (NOLOCK) ON I.ID=G.INSTANCIA" & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_LINEA_DESGLOSE CLD WITH(NOLOCK) ON CLD.CAMPO_HIJO=CC.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN FORM_GRUPO G2 WITH (NOLOCK) ON G.GRUPO_ORIGEN=G2.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN FORMULARIO F WITH(NOLOCK) ON F.ID=G2.FORMULARIO" & vbCrLf
sConsulta = sConsulta & "INNER JOIN FORM_GRUPO FG WITH (NOLOCK) ON F.ID=FG.FORMULARIO" & vbCrLf
sConsulta = sConsulta & "INNER JOIN FORM_CAMPO FC WITH (NOLOCK) ON FG.ID=FC.GRUPO" & vbCrLf
sConsulta = sConsulta & "INNER JOIN DESGLOSE D WITH(NOLOCK) ON D.CAMPO_HIJO=FC.ID" & vbCrLf
sConsulta = sConsulta & "WHERE CCD.ES_SUBCAMPO=1 AND CCD.SUBTIPO<>9 AND (CCD.TIPO_CAMPO_GS<>106 OR CCD.TIPO_CAMPO_GS IS NULL)" & vbCrLf
sConsulta = sConsulta & "AND CCD.CAMPO_ORIGEN=FC.ID" & vbCrLf
sConsulta = sConsulta & "AND (CCD.SUBTIPO=FC.SUBTIPO OR (CCD.SUBTIPO=5 AND FC.SUBTIPO IN (6,7)) OR (CCD.SUBTIPO=6 AND FC.SUBTIPO=7))--si la definici?n del campo ha cambiado de texto corto a texto medio o largo, o de texto medio a texto largo" & vbCrLf
sConsulta = sConsulta & "AND CCD.CAMPO_ORIGEN NOT IN (SELECT ID_CAMPO FROM #G1)  -- campos que se han a?adido en el formulario pero no se ha hecho todavia ALTER en las tablas de filtros (BatchUpdates)" & vbCrLf
sConsulta = sConsulta & "AND I.ID=@INSTANCIA_L" & vbCrLf
sConsulta = sConsulta & "ORDER BY CAMPO_PADRE,LINEA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "OPEN curSOLICITUDES_CAMPOS_DESGLOSE" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curSOLICITUDES_CAMPOS_DESGLOSE INTO @LINEA,@CAMPO_ORIGEN,@SUBTIPO,@TIPO_CAMPO_GS,@VALOR_TEXT," & vbCrLf
sConsulta = sConsulta & "@VALOR_NUM,@VALOR_FEC,@VALOR_BOOL,@VALOR_TEXT_COD,@VALOR_TEXT_DEN,@INTRO,@ID_ATRIB_GS,@TABLA_EXTERNA," & vbCrLf
sConsulta = sConsulta & "@CAMPO_DEF,@CAMPO_PADRE_DESGLOSE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @CAMPO_PADRE_DESGLOSE_ANT=@CAMPO_PADRE_DESGLOSE" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLAUX NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLCAMPOSINSERTAUX NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "SET @SQLAUX=@SQL" & vbCrLf
sConsulta = sConsulta & "SET @SQLCAMPOSINSERTAUX=@SQLCAMPOSINSERT" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N','" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @EJECUTARSQL=1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @LINEA<>@LINEA_ANT OR @CAMPO_PADRE_DESGLOSE<>@CAMPO_PADRE_DESGLOSE_ANT" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @EJECUTARSQL=1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @INSERT = 1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=LEFT(@SQLCAMPOSINSERT,LEN(@SQLCAMPOSINSERT)-1) + ') '+LEFT(@SQL,LEN(@SQL)-1)+')'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=LEFT(@SQL,LEN(@SQL)-1)+' WHERE FORM_INSTANCIA=@INSTANCIA_L AND DESGLOSE=@CAMPO_PADRE_DESGLOSE AND LINEA=@LINEA '" & vbCrLf
sConsulta = sConsulta & "SET @SQL=N'IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = ''dbo'' AND  TABLE_NAME='''+@IDIOMA+N'_'+@TABLA+N'_DESGLOSE'')) BEGIN '+@SQL+N' END'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA_L INT,@CAMPO_PADRE_DESGLOSE INT,@LINEA INT',@INSTANCIA_L=@INSTANCIA_L,@CAMPO_PADRE_DESGLOSE=@CAMPO_PADRE_DESGLOSE_ANT,@LINEA=@LINEA_ANT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQLAUX" & vbCrLf
sConsulta = sConsulta & "SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERTAUX" & vbCrLf
sConsulta = sConsulta & "SET @LINEA_ANT=@LINEA" & vbCrLf
sConsulta = sConsulta & "SET @CAMPO_PADRE_DESGLOSE_ANT=@CAMPO_PADRE_DESGLOSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N','" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "--Reemplazamos las comillas simples en valor_text y valor_text_den" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @VALOR_TEXT = SUBSTRING(@VALOR_TEXT,1,100)" & vbCrLf
sConsulta = sConsulta & "SET @VALOR_TEXT = REPLACE(@VALOR_TEXT,'''','''''')" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT_DEN IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SET @VALOR_TEXT_DEN = REPLACE(@VALOR_TEXT_DEN,'''','''''')" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT + N'C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N','" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'='" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "----------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "IF @SUBTIPO = 3 -- tipo fecha" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_FEC IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'CONVERT(DATETIME,''' + CONVERT(VARCHAR,@VALOR_FEC) + ''')'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @SUBTIPO = 4 -- tipo booleano" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_BOOL IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + CONVERT(NVARCHAR,@VALOR_BOOL)" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @SUBTIPO = 8 -- tipo archivo" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'''' + @VALOR_TEXT + N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @TIPO_CAMPO_GS IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @TABLA_EXTERNA IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @SUBTIPO=2 -- tipo num?rico" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_NUM IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + CONVERT(NVARCHAR,@VALOR_NUM)" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE    -- tipo texto o tipo lista texto" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "IF @INTRO=1 AND @VALOR_NUM IS NOT NULL --tipo lista texto" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @ID_ATRIB_GS IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @IDIOMA = 'SPA'" & vbCrLf
sConsulta = sConsulta & "SELECT @DENOMINACION=VALOR_TEXT_SPA" & vbCrLf
sConsulta = sConsulta & "FROM COPIA_CAMPO_VALOR_LISTA WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE CAMPO_DEF=CONVERT(VARCHAR,@CAMPO_DEF) AND ORDEN=CONVERT(VARCHAR,@VALOR_NUM)" & vbCrLf
sConsulta = sConsulta & "IF @IDIOMA = 'ENG'" & vbCrLf
sConsulta = sConsulta & "SELECT @DENOMINACION=VALOR_TEXT_ENG" & vbCrLf
sConsulta = sConsulta & "FROM COPIA_CAMPO_VALOR_LISTA WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE CAMPO_DEF=CONVERT(VARCHAR,@CAMPO_DEF) AND ORDEN=CONVERT(VARCHAR,@VALOR_NUM)" & vbCrLf
sConsulta = sConsulta & "IF @IDIOMA = 'GER'" & vbCrLf
sConsulta = sConsulta & "SELECT @DENOMINACION=VALOR_TEXT_GER" & vbCrLf
sConsulta = sConsulta & "FROM COPIA_CAMPO_VALOR_LISTA WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE CAMPO_DEF = CONVERT(VARCHAR,@CAMPO_DEF) AND ORDEN=CONVERT(VARCHAR,@VALOR_NUM)" & vbCrLf
sConsulta = sConsulta & "IF @IDIOMA = 'FRA'" & vbCrLf
sConsulta = sConsulta & "SELECT @DENOMINACION=VALOR_TEXT_FRA" & vbCrLf
sConsulta = sConsulta & "FROM COPIA_CAMPO_VALOR_LISTA WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE CAMPO_DEF = CONVERT(VARCHAR,@CAMPO_DEF) AND ORDEN=CONVERT(VARCHAR,@VALOR_NUM)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @DENOMINACION IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+REPLACE(@DENOMINACION,'''','''''')+''''+N',CONVERT(NVARCHAR,'+CONVERT(NVARCHAR,@VALOR_NUM)+N')'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+REPLACE(@DENOMINACION,'''','''''')+''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=CONVERT(NVARCHAR,'+CONVERT(NVARCHAR,@VALOR_NUM)+N')'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + N'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + N'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + N''''+@VALOR_TEXT+N''''+N','+N''''+CONVERT(NVARCHAR,@VALOR_NUM)+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + N''''+@VALOR_TEXT+N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+ CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N''''+CONVERT(NVARCHAR,@VALOR_NUM)+N''''" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE -- tipo texto" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + N''''+@VALOR_TEXT+N''''+N','+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + N''''+@VALOR_TEXT+N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+ CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + N'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @SUBTIPO=2 -- tipo num?rico" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_NUM IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + CONVERT(NVARCHAR,@VALOR_NUM)" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE    -- tipo texto o tipo lista texto" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT_DEN IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+@VALOR_TEXT_DEN+N''''+N','+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+@VALOR_TEXT_DEN+N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_' +CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE -- campo del sistema" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @CAMPOTRATADO = 0" & vbCrLf
sConsulta = sConsulta & "---------------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "-- Formas de pago, Monedas, Familia de materiales, Unidades, Destinos, Pa?ses y Provincias" & vbCrLf
sConsulta = sConsulta & "-- FORMATO: <Idioma>|<Texto>#<Idioma>|<Texto>" & vbCrLf
sConsulta = sConsulta & "-- FORMATO MATERIALES: cod. ultimo nivel (VALOR_TEXT) +  <Idioma>|<Texto>#<Idioma>|<Texto>" & vbCrLf
sConsulta = sConsulta & "------------------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "IF @TIPO_CAMPO_GS = 101 OR @TIPO_CAMPO_GS = 102 OR @TIPO_CAMPO_GS = 103 OR @TIPO_CAMPO_GS = 105 OR @TIPO_CAMPO_GS = 109 OR @TIPO_CAMPO_GS = 107 OR @TIPO_CAMPO_GS = 108" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+ N'_COD,'" & vbCrLf
sConsulta = sConsulta & "SET @CAMPOTRATADO = 1" & vbCrLf
sConsulta = sConsulta & "-- Materiales" & vbCrLf
sConsulta = sConsulta & "IF @TIPO_CAMPO_GS = 103" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "-- Al hacer Len de valor_text me est? quitando los espacios en blanco de la derecha" & vbCrLf
sConsulta = sConsulta & "IF LEN(@VALOR_TEXT + '#')-1 = @LEN_GMN1 + @LEN_GMN2 + @LEN_GMN3 + @LEN_GMN4" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'''' + RTRIM(RIGHT(@VALOR_TEXT,@LEN_GMN4)) + N' - '" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF LEN(@VALOR_TEXT + '#')-1 = @LEN_GMN1 + @LEN_GMN2 + @LEN_GMN3" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'''' + RTRIM(RIGHT(@VALOR_TEXT,@LEN_GMN3)) + N' - '" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF LEN(@VALOR_TEXT + '#')-1 = @LEN_GMN1 + @LEN_GMN2" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'''' + RTRIM(RIGHT(@VALOR_TEXT,@LEN_GMN2)) + N' - '" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'''' + RTRIM(RIGHT(@VALOR_TEXT,@LEN_GMN1)) + N' - '" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- Todos" & vbCrLf
sConsulta = sConsulta & "SET @EMPIEZAIDIOMA=CHARINDEX(@IDIOMA + '|',@VALOR_TEXT_DEN)" & vbCrLf
sConsulta = sConsulta & "IF (@EMPIEZAIDIOMA) > 0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @ACABADENOMINACION = CHARINDEX('#',@VALOR_TEXT_DEN,@EMPIEZAIDIOMA)" & vbCrLf
sConsulta = sConsulta & "SET @EMPIEZADENOMINACION = CHARINDEX('|',@VALOR_TEXT_DEN,@EMPIEZAIDIOMA)+1" & vbCrLf
sConsulta = sConsulta & "IF @TIPO_CAMPO_GS <> 103" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+''''" & vbCrLf
sConsulta = sConsulta & "IF @ACABADENOMINACION>0" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+SUBSTRING(@VALOR_TEXT_DEN,@EMPIEZADENOMINACION,@ACABADENOMINACION-@EMPIEZADENOMINACION) + ''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+RIGHT(@VALOR_TEXT_DEN,LEN(@VALOR_TEXT_DEN)-@EMPIEZADENOMINACION+1) + ''''" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N','+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N',C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT IS NOT NULL AND @TIPO_CAMPO_GS = 103" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+N','+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "-- Proveedores, Departamentos, Organizaciones de Compras, Centros" & vbCrLf
sConsulta = sConsulta & "-- FORMATO: C?digo (VALOR_TEXT) - Denominaci?n (VALOR_TEXT_DEN)" & vbCrLf
sConsulta = sConsulta & "--------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "IF @TIPO_CAMPO_GS = 11 OR @TIPO_CAMPO_GS = 100  OR @TIPO_CAMPO_GS = 122 OR @TIPO_CAMPO_GS = 123 OR @TIPO_CAMPO_GS = 124" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @CAMPOTRATADO = 1" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+@VALOR_TEXT+N' - '+@VALOR_TEXT_DEN+N''''+N','+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+@VALOR_TEXT+N' - '+@VALOR_TEXT_DEN+N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "-- Centros de coste, Partidas presupuestarias, Activos" & vbCrLf
sConsulta = sConsulta & "-- FORMATO: cod. ultimo nivel (VALOR_TEXT) +  <Idioma>|<Texto>#<Idioma>|<Texto>" & vbCrLf
sConsulta = sConsulta & "--------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "IF @TIPO_CAMPO_GS = 129 OR @TIPO_CAMPO_GS = 130 OR @TIPO_CAMPO_GS = 131 OR @TIPO_CAMPO_GS=132" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @CAMPOTRATADO=1" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SEPARADOR = CHARINDEX('#',@VALOR_TEXT,1)" & vbCrLf
sConsulta = sConsulta & "WHILE @SEPARADOR > 0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @VALOR_TEXT = RIGHT(@VALOR_TEXT,LEN(@VALOR_TEXT)-@SEPARADOR)" & vbCrLf
sConsulta = sConsulta & "SET @SEPARADOR = CHARINDEX('#',@VALOR_TEXT,1)" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SEPARADOR = CHARINDEX('|',@VALOR_TEXT,1)" & vbCrLf
sConsulta = sConsulta & "WHILE @SEPARADOR > 0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @VALOR_TEXT = RIGHT(@VALOR_TEXT,LEN(@VALOR_TEXT)-@SEPARADOR)" & vbCrLf
sConsulta = sConsulta & "SET @SEPARADOR = CHARINDEX('|',@VALOR_TEXT,1)" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + '''' + @VALOR_TEXT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Denominaci?n en el idioma" & vbCrLf
sConsulta = sConsulta & "SET @EMPIEZAIDIOMA=CHARINDEX(@IDIOMA + '|',@VALOR_TEXT_DEN)" & vbCrLf
sConsulta = sConsulta & "IF (@EMPIEZAIDIOMA) > 0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @ACABADENOMINACION = CHARINDEX('#',@VALOR_TEXT_DEN,@EMPIEZAIDIOMA)" & vbCrLf
sConsulta = sConsulta & "SET @EMPIEZADENOMINACION = CHARINDEX('|',@VALOR_TEXT_DEN,@EMPIEZAIDIOMA)+1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ACABADENOMINACION>0" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ' - ' + SUBSTRING(@VALOR_TEXT_DEN,@EMPIEZADENOMINACION,@ACABADENOMINACION-@EMPIEZADENOMINACION)" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ' - ' + RIGHT(@VALOR_TEXT_DEN,LEN(@VALOR_TEXT_DEN)-@EMPIEZADENOMINACION+1)" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+N','+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "-- Personas, Comprador" & vbCrLf
sConsulta = sConsulta & "---------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "IF @TIPO_CAMPO_GS = 115 OR @TIPO_CAMPO_GS=144" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @CAMPOTRATADO = 1" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT_DEN IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+@VALOR_TEXT_DEN+N''''+N','+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+@VALOR_TEXT_DEN+N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "-- Almacen" & vbCrLf
sConsulta = sConsulta & "---------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "IF @TIPO_CAMPO_GS = 125" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @CAMPOTRATADO = 1" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT_DEN IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+@VALOR_TEXT_DEN+N''''+N','+N''''+CONVERT(NVARCHAR,@VALOR_NUM)+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+@VALOR_TEXT_DEN+N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N''''+CONVERT(NVARCHAR,@VALOR_NUM)+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "------------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "-- Cod. Art?culo (viejo), Cod. Art?culo (nuevo), Denominaci?n del art?culo y Unidades Organizativas" & vbCrLf
sConsulta = sConsulta & "------------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "IF @TIPO_CAMPO_GS = 104  OR @TIPO_CAMPO_GS = 118 OR @TIPO_CAMPO_GS = 119 OR @TIPO_CAMPO_GS = 121" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @CAMPOTRATADO = 1" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+@VALOR_TEXT+N''''+N','+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+@VALOR_TEXT +N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "---------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "-- Presupuestos" & vbCrLf
sConsulta = sConsulta & "-- FORMATO: Ej 2005 - PC-002  (50.00%); 2005 - PC-001 (50.00%);" & vbCrLf
sConsulta = sConsulta & "--  VALOR_TEXT_DEN: 2005 - PC-002#2005 - PC-001" & vbCrLf
sConsulta = sConsulta & "--  VALOR_TEXT: 1_11_0.5#1_10_0.5" & vbCrLf
sConsulta = sConsulta & "---------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "IF @TIPO_CAMPO_GS = 110  OR @TIPO_CAMPO_GS = 111 OR @TIPO_CAMPO_GS = 112 OR @TIPO_CAMPO_GS = 113" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @CAMPOTRATADO = 1" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "SET @PRESUPUESTOS = ''" & vbCrLf
sConsulta = sConsulta & "SET @SEPARADOR_DEN = CHARINDEX('#',@VALOR_TEXT_DEN)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "WHILE @SEPARADOR_DEN > 0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @PRESUPUESTOS = @PRESUPUESTOS + SUBSTRING(@VALOR_TEXT_DEN,1,@SEPARADOR_DEN-1)" & vbCrLf
sConsulta = sConsulta & "SET @VALOR_TEXT_DEN = RIGHT(@VALOR_TEXT_DEN,LEN(@VALOR_TEXT_DEN)-@SEPARADOR_DEN)" & vbCrLf
sConsulta = sConsulta & "SET @PORCENTAJE = SUBSTRING(@VALOR_TEXT,1,CHARINDEX('#',@VALOR_TEXT)-1)" & vbCrLf
sConsulta = sConsulta & "SET @SEPARADOR_PORCENTAJE = 1" & vbCrLf
sConsulta = sConsulta & "WHILE @SEPARADOR_PORCENTAJE > 0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @PORCENTAJE = RIGHT(@PORCENTAJE,LEN(@PORCENTAJE)-@SEPARADOR_PORCENTAJE)" & vbCrLf
sConsulta = sConsulta & "SET @SEPARADOR_PORCENTAJE = CHARINDEX('_',@PORCENTAJE,@SEPARADOR_PORCENTAJE+1)" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "SET @PRESUPUESTOS = @PRESUPUESTOS + '(' + CONVERT(VARCHAR,CONVERT(DECIMAL(3,2),@PORCENTAJE)*100) + '%); '" & vbCrLf
sConsulta = sConsulta & "SET @VALOR_TEXT = RIGHT(@VALOR_TEXT,LEN(@VALOR_TEXT)-CHARINDEX('#',@VALOR_TEXT))" & vbCrLf
sConsulta = sConsulta & "SET @SEPARADOR_DEN = CHARINDEX('#',@VALOR_TEXT_DEN)" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "SET @PORCENTAJE = @VALOR_TEXT" & vbCrLf
sConsulta = sConsulta & "SET @SEPARADOR_PORCENTAJE = 1" & vbCrLf
sConsulta = sConsulta & "WHILE @SEPARADOR_PORCENTAJE > 0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @PORCENTAJE = RIGHT(@PORCENTAJE,LEN(@PORCENTAJE)-@SEPARADOR_PORCENTAJE)" & vbCrLf
sConsulta = sConsulta & "SET @SEPARADOR_PORCENTAJE = CHARINDEX('_',@PORCENTAJE,@SEPARADOR_PORCENTAJE+1)" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @PRESUPUESTOS = @PRESUPUESTOS + @VALOR_TEXT_DEN + '(' + CAST(CONVERT(FLOAT,@PORCENTAJE)*100 AS VARCHAR) + '%);'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @PRESUPUESTOS <> ''" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'''' + @PRESUPUESTOS + N'''' + N',' + N'''' + @VALOR_TEXT + N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'''' + @PRESUPUESTOS + N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL,'+N'C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CAMPOTRATADO = 0 -- Resto" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @SUBTIPO = 3 -- tipo fecha" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_FEC IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + '''' + @VALOR_FEC + ''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @SUBTIPO = 4 -- tipo booleano" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_BOOL IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + CONVERT(VARCHAR,@VALOR_BOOL)" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @SUBTIPO=2 -- tipo num?rico" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_NUM IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + CONVERT(VARCHAR,@VALOR_NUM)" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE    -- tipo texto" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+@VALOR_TEXT+N''''+N','+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+''''+@VALOR_TEXT+''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+ N'_COD='+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+ N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ','" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curSOLICITUDES_CAMPOS_DESGLOSE INTO @LINEA,@CAMPO_ORIGEN,@SUBTIPO,@TIPO_CAMPO_GS," & vbCrLf
sConsulta = sConsulta & "@VALOR_TEXT,@VALOR_NUM,@VALOR_FEC,@VALOR_BOOL,@VALOR_TEXT_COD,@VALOR_TEXT_DEN,@INTRO,@ID_ATRIB_GS," & vbCrLf
sConsulta = sConsulta & "@TABLA_EXTERNA,@CAMPO_DEF,@CAMPO_PADRE_DESGLOSE" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "CLOSE curSOLICITUDES_CAMPOS_DESGLOSE" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curSOLICITUDES_CAMPOS_DESGLOSE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @EJECUTARSQL=1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @INSERT = 1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = LEFT(@SQLCAMPOSINSERT,LEN(@SQLCAMPOSINSERT)-1) + ') ' + LEFT(@SQL,LEN(@SQL)-1) + ')'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = LEFT(@SQL,LEN(@SQL)-1) + ' WHERE FORM_INSTANCIA=@INSTANCIA_L AND DESGLOSE=@CAMPO_PADRE_DESGLOSE AND LINEA=@LINEA '" & vbCrLf
sConsulta = sConsulta & "SET @SQL=N'IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = ''dbo'' AND  TABLE_NAME='''+@IDIOMA+N'_'+@TABLA+N'_DESGLOSE'')) BEGIN '+@SQL+N' END'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA_L INT,@CAMPO_PADRE_DESGLOSE INT,@LINEA INT', @INSTANCIA_L=@INSTANCIA_L,@CAMPO_PADRE_DESGLOSE=@CAMPO_PADRE_DESGLOSE,@LINEA=@LINEA" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "SET @INSERT=@INSERT_ORIGINAL" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curIDIOMAS INTO @IDIOMA" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CLOSE curIDIOMAS" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curIDIOMAS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DROP TABLE #G1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion31900_09_2145_A31900_09_2146() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim oADOConnection As ADODB.Connection
    Dim sConsulta As String

    On Error GoTo Error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Actualiza Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    frmProgreso.ProgressBar1.Refresh
    V_31900_9_Storeds_2146

    sConsulta = "UPDATE VERSION SET NUM ='3.00.21.046'" & ", FECHA=CONVERT(datetime,'" & Format(Now, "mm-dd-yyyy hh:mm:ss") & "',110), PRODVER='31900.9'"
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False

    CodigoDeActualizacion31900_09_2145_A31900_09_2146 = True
    Exit Function

Error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If

    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    CodigoDeActualizacion31900_09_2145_A31900_09_2146 = False
End Function

Private Sub V_31900_9_Storeds_2146()
    Dim sConsulta As String

    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSEP_ACTUALIZAR_IMPUTACION_LINEAS_PEDIDO]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSEP_ACTUALIZAR_IMPUTACION_LINEAS_PEDIDO]" & vbCrLf
    gRDOCon.QueryTimeout = 7200
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "CREATE PROCEDURE [dbo].[FSEP_ACTUALIZAR_IMPUTACION_LINEAS_PEDIDO]  " & vbCrLf
sConsulta = sConsulta & "   @ORDEN INT   " & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @LINEA INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE Cur_PEDIDOS Cursor FOR select DISTINCT LP.ID" & vbCrLf
sConsulta = sConsulta & "FROM LINEAS_PEDIDO LP  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN LINEAS_PED_IMPUTACION LPI WITH (NOLOCK) ON LP.ID=LPI.LINEA" & vbCrLf
sConsulta = sConsulta & "WHERE LP.ORDEN = @ORDEN" & vbCrLf
sConsulta = sConsulta & "  OPEN Cur_PEDIDOS" & vbCrLf
sConsulta = sConsulta & "  FETCH NEXT FROM Cur_PEDIDOS into @LINEA" & vbCrLf
sConsulta = sConsulta & "  WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   DECLARE @UON1 VARCHAR(50), @UON2 VARCHAR(50), @UON3 VARCHAR(50), @UON4 VARCHAR(50), @PRES0 VARCHAR(500), @PRES1 VARCHAR(500), @PRES2 VARCHAR(500), @PRES3 VARCHAR(500)," & vbCrLf
sConsulta = sConsulta & "      @PRES4 VARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "   DECLARE @CAMBIO FLOAT, @P5I INT,@IMPORTETOTALLINEA FLOAT" & vbCrLf
sConsulta = sConsulta & "   DECLARE @IMPCOSTELINEA FLOAT, @IMPCOSTECD FLOAT, @IMPCOSTECABECERA FLOAT, @CDLINEA FLOAT, @IMPORTELINEA FLOAT, @DLINEA FLOAT," & vbCrLf
sConsulta = sConsulta & "   @CDLINEATOTAL FLOAT, @IMPORTECABE FLOAT, @CDCABE FLOAT, @IMPORTELINEAS FLOAT, @IMPORTETOTALLINEAS FLOAT, @DCABE FLOAT, @IMPLINEA FLOAT, @SUMA FLOAT, @SUMAIMPU FLOAT" & vbCrLf
sConsulta = sConsulta & "   DECLARE @ACUMULARIMP BIT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   select DISTINCT @UON1=LPI.UON1,@UON2=LPI.UON2, @UON3=LPI.UON3, @UON4=LPI.UON4, @PRES0=LPI.PRES0, @PRES1=LPI.PRES1, @PRES2=LPI.PRES2, @PRES3=LPI.PRES3, @PRES4=LPI.PRES4" & vbCrLf
sConsulta = sConsulta & "   FROM LINEAS_PED_IMPUTACION LPI WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   WHERE LPI.LINEA=@LINEA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   --impuesto costes lineas" & vbCrLf
sConsulta = sConsulta & "   select @IMPCOSTELINEA=SUM(LPCD.IMPORTE * (LPCDI.VALOR / 100) )" & vbCrLf
sConsulta = sConsulta & "   FROM LINEAS_PEDIDO LP WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN LINEAS_PEDIDO_CD LPCD WITH(NOLOCK) ON LP.ID = LPCD.LINEA" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN LINEAS_PEDIDO_CD_IMPUESTO LPCDI   WITH(NOLOCK) ON LPCD.ID = LPCDI.COSTE" & vbCrLf
sConsulta = sConsulta & "   WHERE LP.ORDEN=@ORDEN AND LP.ID=@LINEA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   --costes descuentos LINEA" & vbCrLf
sConsulta = sConsulta & "   select @CDLINEA=SUM(case when LPCD.TIPO = 0 then LPCD.IMPORTE else LPCD.IMPORTE * -1 end  )" & vbCrLf
sConsulta = sConsulta & "   FROM LINEAS_PEDIDO LP WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN LINEAS_PEDIDO_CD LPCD WITH(NOLOCK) ON LP.ID = LPCD.LINEA" & vbCrLf
sConsulta = sConsulta & "   WHERE  LP.ORDEN=@ORDEN AND LP.ID=@LINEA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   --IMPORTE TOTAL LINEA" & vbCrLf
sConsulta = sConsulta & "   SELECT @IMPORTELINEA = case when LP.TIPORECEPCION = 0 THEN (LP.PREC_UP * LP.CANT_PED * LP.FC) ELSE  LP.IMPORTE_PED END" & vbCrLf
sConsulta = sConsulta & "   FROM LINEAS_PEDIDO LP WITH(NOLOCK) WHERE  LP.ORDEN=@ORDEN AND LP.ID=@LINEA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   --IMPORTE TOTAL LINEAS" & vbCrLf
sConsulta = sConsulta & "   SELECT @IMPORTELINEAS = SUM(case when LP.TIPORECEPCION = 0 THEN (LP.PREC_UP * LP.CANT_PED * LP.FC) ELSE  LP.IMPORTE_PED END)" & vbCrLf
sConsulta = sConsulta & "   FROM LINEAS_PEDIDO LP WITH(NOLOCK) WHERE LP.ORDEN=@ORDEN " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   --costes descuentos LINEA TOTAL" & vbCrLf
sConsulta = sConsulta & "   select @CDLINEATOTAL=SUM(case when LPCD.TIPO = 0 then LPCD.IMPORTE else LPCD.IMPORTE * -1 end  )" & vbCrLf
sConsulta = sConsulta & "   FROM LINEAS_PEDIDO LP WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN LINEAS_PEDIDO_CD LPCD WITH(NOLOCK) ON LP.ID = LPCD.LINEA" & vbCrLf
sConsulta = sConsulta & "   WHERE LP.ORDEN=@ORDEN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   --descuentos LINEA TOTAL" & vbCrLf
sConsulta = sConsulta & "   select @DLINEA=SUM(case when LPCD.TIPO = 0 then 0 else LPCD.IMPORTE end  )" & vbCrLf
sConsulta = sConsulta & "   FROM LINEAS_PEDIDO LP WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN LINEAS_PEDIDO_CD LPCD WITH(NOLOCK) ON LP.ID = LPCD.LINEA" & vbCrLf
sConsulta = sConsulta & "   WHERE LP.ORDEN=@ORDEN AND LP.ID=@LINEA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SELECT @IMPORTETOTALLINEAS = ISNULL(@IMPORTELINEAS, 0) + ISNULL(@CDLINEATOTAL, 0)" & vbCrLf
sConsulta = sConsulta & "   SELECT @IMPORTETOTALLINEA = ISNULL(@IMPORTELINEA, 0) + ISNULL(@CDLINEA, 0)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SELECT @DCABE=SUM(case when OECD.TIPO = 0 then 0 else CASE WHEN LEN(OECD.OPERACION) = 1 THEN (@IMPORTETOTALLINEA * OECD.VALOR) / @IMPORTETOTALLINEAS ELSE OECD.VALOR END end ) " & vbCrLf
sConsulta = sConsulta & "   FROM ORDEN_ENTREGA OE" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN ORDEN_ENTREGA_CD OECD WITH(NOLOCK) ON OE.ID=OECD.ORDEN" & vbCrLf
sConsulta = sConsulta & "   WHERE OE.ID=@ORDEN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   --impuesto lineas" & vbCrLf
sConsulta = sConsulta & "   SELECT @IMPLINEA=SUM((@IMPORTELINEA - @DLINEA - @DCABE) * (LPI.VALOR / 100) )" & vbCrLf
sConsulta = sConsulta & "   FROM LINEAS_PEDIDO LP WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN LINEAS_PEDIDO_IMPUESTO LPI WITH(NOLOCK) ON LP.ID = LPI.LINEA" & vbCrLf
sConsulta = sConsulta & "   WHERE LP.ORDEN=@ORDEN AND LP.ID=@LINEA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   --IMPUESTOS CABECERA" & vbCrLf
sConsulta = sConsulta & "   SELECT @IMPCOSTECABECERA=(@IMPORTETOTALLINEA * SUM(OECD.IMPORTE * (OECDI.VALOR / 100) ) / @IMPORTETOTALLINEAS)" & vbCrLf
sConsulta = sConsulta & "   FROM ORDEN_ENTREGA OE" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN ORDEN_ENTREGA_CD OECD WITH(NOLOCK) ON OE.ID=OECD.ORDEN" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN ORDEN_ENTREGA_CD_IMPUESTO OECDI WITH(NOLOCK) ON OECD.ID = OECDI.COSTE" & vbCrLf
sConsulta = sConsulta & "   WHERE OE.ID=@ORDEN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   --COSTES DESCUENTOS CABECERA" & vbCrLf
sConsulta = sConsulta & "   SELECT @CDCABE=(ISNULL(@IMPORTETOTALLINEA, 0) * SUM(case when OECD.TIPO = 0 then ISNULL(OECD.IMPORTE,0) else ISNULL(OECD.IMPORTE, 0) * -1 end ) / @IMPORTETOTALLINEAS)" & vbCrLf
sConsulta = sConsulta & "   FROM ORDEN_ENTREGA OE" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN ORDEN_ENTREGA_CD OECD WITH(NOLOCK) ON OE.ID=OECD.ORDEN" & vbCrLf
sConsulta = sConsulta & "   WHERE OE.ID=@ORDEN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SELECT @SUMAIMPU= ISNULL(@IMPORTELINEA,0)+ISNULL(@CDCABE,0)+ISNULL(@IMPCOSTECABECERA,0)+ISNULL(@CDLINEA,0)+ISNULL(@IMPCOSTELINEA,0)+ISNULL(@IMPLINEA,0)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SELECT @SUMA= ISNULL(@IMPORTELINEA,0)+ISNULL(@CDCABE,0)+ISNULL(@CDLINEA,0)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SELECT @CAMBIO= M.EQUIV, @P5I=P5I.ID FROM MON M WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PRES5_IMPORTES P5I WITH (NOLOCK) ON P5I.MON = M.COD" & vbCrLf
sConsulta = sConsulta & "   WHERE P5I.CERRADO=0 AND P5I.PRES0=@PRES0 AND P5I.PRES1=@PRES1" & vbCrLf
sConsulta = sConsulta & "   AND (P5I.PRES2 IS NULL AND @PRES2 IS NULL OR P5I.PRES2=@PRES2)" & vbCrLf
sConsulta = sConsulta & "   AND (P5I.PRES3 IS NULL AND @PRES3 IS NULL OR P5I.PRES3=@PRES3)" & vbCrLf
sConsulta = sConsulta & "   AND (P5I.PRES4 IS NULL AND @PRES4 IS NULL OR P5I.PRES4=@PRES4)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @P5I IS NULL" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           SET @CAMBIO = 1" & vbCrLf
sConsulta = sConsulta & "           SELECT @P5I=P5I.ID " & vbCrLf
sConsulta = sConsulta & "           FROM PRES5_IMPORTES P5I WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "           WHERE P5I.CERRADO=0 AND P5I.PRES0=@PRES0 AND P5I.PRES1=@PRES1" & vbCrLf
sConsulta = sConsulta & "           AND (P5I.PRES2 IS NULL AND @PRES2 IS NULL OR P5I.PRES2=@PRES2)" & vbCrLf
sConsulta = sConsulta & "           AND (P5I.PRES3 IS NULL AND @PRES3 IS NULL OR P5I.PRES3=@PRES3)" & vbCrLf
sConsulta = sConsulta & "           AND (P5I.PRES4 IS NULL AND @PRES4 IS NULL OR P5I.PRES4=@PRES4)" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "   SELECT @ACUMULARIMP= PS.ACUMULARIMP FROM PARGEN_SM PS WITH(NOLOCK) WHERE PS.PRES5=@PRES0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   UPDATE LINEAS_PED_IMPUTACION  SET CAMBIO = ISNULL(@CAMBIO,1.0) WHERE LINEA=@LINEA" & vbCrLf
sConsulta = sConsulta & "   --Sumar al comprometido" & vbCrLf
sConsulta = sConsulta & "   IF @ACUMULARIMP=0" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           UPDATE LINEAS_PEDIDO SET SM_IMPORTECOMPR=@SUMA WHERE ID=@LINEA" & vbCrLf
sConsulta = sConsulta & "           UPDATE PRES5_IMPORTES SET COMP=ISNULL(COMP,0) + ((@SUMA) * LP.FC * ISNULL(@CAMBIO,1.0))" & vbCrLf
sConsulta = sConsulta & "           FROM LINEAS_PEDIDO LP WITH(NOLOCK) WHERE LP.ORDEN=@ORDEN AND LP.ID=@LINEA AND PRES5_IMPORTES.ID=@P5I" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           UPDATE LINEAS_PEDIDO SET SM_IMPORTECOMPRIMPUESTOS=@SUMAIMPU WHERE ID=@LINEA" & vbCrLf
sConsulta = sConsulta & "           UPDATE PRES5_IMPORTES SET COMP=ISNULL(COMP,0) + ((@SUMAIMPU) * LP.FC * ISNULL(@CAMBIO,1.0))" & vbCrLf
sConsulta = sConsulta & "           FROM LINEAS_PEDIDO LP WITH(NOLOCK) WHERE LP.ORDEN=@ORDEN AND LP.ID=@LINEA AND PRES5_IMPORTES.ID=@P5I" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--select @sumA SUMA, @SUMAIMPU SUMAIMPU, @linea LINEA,@IMPORTELINEA IMPORTELIN,@CDCABE CDCABE--" & vbCrLf
sConsulta = sConsulta & "--,@IMPCOSTECABECERA IMPOCOSTECABE,@CDLINEA CDLINEA,@IMPCOSTELINEA IMPOCOSTELINA,@IMPLINEA IMPLINEA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   FETCH NEXT FROM Cur_PEDIDOS INTO @LINEA" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "Close Cur_PEDIDOS" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE Cur_PEDIDOS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
    gRDOCon.QueryTimeout = 7200
    ExecuteSQL gRDOCon, sConsulta
 
   sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSEP_ACTUALIZAR_LINEAS_PED_IMPUTACION]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSEP_ACTUALIZAR_LINEAS_PED_IMPUTACION]" & vbCrLf
    gRDOCon.QueryTimeout = 7200
    ExecuteSQL gRDOCon, sConsulta
    sConsulta = "CREATE PROCEDURE [dbo].[FSEP_ACTUALIZAR_LINEAS_PED_IMPUTACION]  " & vbCrLf
sConsulta = sConsulta & "   @ORDEN INT, @MODOIMP INT, " & vbCrLf
sConsulta = sConsulta & "   @LINEA INT, @PEDIDO INT, @UON1 VARCHAR(50), @UON2 VARCHAR(50), @UON3 VARCHAR(50), @UON4 VARCHAR(50), @PRES0 VARCHAR(500), @PRES1 VARCHAR(500), @PRES2 VARCHAR(500), @PRES3 VARCHAR(500)," & vbCrLf
sConsulta = sConsulta & "   @PRES4 VARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @CAMBIO FLOAT, @P5I INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @IMPCOSTELINEA FLOAT, @IMPCOSTECD FLOAT, @IMPCOSTECABECERA FLOAT, @CDLINEA FLOAT, @IMPORTELINEA FLOAT, @DLINEA FLOAT, @IMPORTETOTALLINEA FLOAT," & vbCrLf
sConsulta = sConsulta & "@CDLINEATOTAL FLOAT, @IMPORTECABE FLOAT, @CDCABE FLOAT, @IMPORTELINEAS FLOAT, @IMPORTETOTALLINEAS FLOAT, @DCABE FLOAT, @IMPLINEA FLOAT, @SUMA FLOAT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--impuesto costes lineas" & vbCrLf
sConsulta = sConsulta & "select @IMPCOSTELINEA=SUM(LPCD.IMPORTE * (LPCDI.VALOR / 100) )" & vbCrLf
sConsulta = sConsulta & "FROM LINEAS_PEDIDO LP WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "INNER JOIN LINEAS_PEDIDO_CD LPCD WITH(NOLOCK) ON LP.ID = LPCD.LINEA" & vbCrLf
sConsulta = sConsulta & "INNER JOIN LINEAS_PEDIDO_CD_IMPUESTO LPCDI  WITH(NOLOCK) ON LPCD.ID = LPCDI.COSTE" & vbCrLf
sConsulta = sConsulta & "WHERE LP.PEDIDO=@PEDIDO AND LP.ORDEN=@ORDEN AND LP.CESTA=@LINEA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--costes descuentos LINEA" & vbCrLf
sConsulta = sConsulta & "select @CDLINEA=SUM(case when LPCD.TIPO = 0 then LPCD.IMPORTE else LPCD.IMPORTE * -1 end  )" & vbCrLf
sConsulta = sConsulta & "FROM LINEAS_PEDIDO LP WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "INNER JOIN LINEAS_PEDIDO_CD LPCD WITH(NOLOCK) ON LP.ID = LPCD.LINEA" & vbCrLf
sConsulta = sConsulta & "WHERE LP.PEDIDO=@PEDIDO AND LP.ORDEN=@ORDEN AND LP.CESTA=@LINEA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--IMPORTE TOTAL LINEA" & vbCrLf
sConsulta = sConsulta & "SELECT @IMPORTELINEA = case when LP.TIPORECEPCION = 0 THEN (LP.PREC_UP * LP.CANT_PED) ELSE  LP.IMPORTE_PED END" & vbCrLf
sConsulta = sConsulta & "FROM LINEAS_PEDIDO LP WITH(NOLOCK) WHERE LP.PEDIDO=@PEDIDO AND LP.ORDEN=@ORDEN AND LP.CESTA=@LINEA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--IMPORTE TOTAL LINEAS" & vbCrLf
sConsulta = sConsulta & "SELECT @IMPORTELINEAS = SUM(case when LP.TIPORECEPCION = 0 THEN (LP.PREC_UP * LP.CANT_PED) ELSE  LP.IMPORTE_PED END)" & vbCrLf
sConsulta = sConsulta & "FROM LINEAS_PEDIDO LP WITH(NOLOCK) WHERE LP.PEDIDO=@PEDIDO AND LP.ORDEN=@ORDEN " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--costes descuentos LINEA TOTAL" & vbCrLf
sConsulta = sConsulta & "select @CDLINEATOTAL=SUM(case when LPCD.TIPO = 0 then LPCD.IMPORTE else LPCD.IMPORTE * -1 end  )" & vbCrLf
sConsulta = sConsulta & "FROM LINEAS_PEDIDO LP WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "INNER JOIN LINEAS_PEDIDO_CD LPCD WITH(NOLOCK) ON LP.ID = LPCD.LINEA" & vbCrLf
sConsulta = sConsulta & "WHERE LP.PEDIDO=@PEDIDO AND LP.ORDEN=@ORDEN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--descuentos LINEA TOTAL" & vbCrLf
sConsulta = sConsulta & "select @DLINEA=SUM(case when LPCD.TIPO = 0 then 0 else LPCD.IMPORTE end  )" & vbCrLf
sConsulta = sConsulta & "FROM LINEAS_PEDIDO LP WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "INNER JOIN LINEAS_PEDIDO_CD LPCD WITH(NOLOCK) ON LP.ID = LPCD.LINEA" & vbCrLf
sConsulta = sConsulta & "WHERE LP.PEDIDO=@PEDIDO AND LP.ORDEN=@ORDEN AND LP.CESTA=@LINEA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @IMPORTETOTALLINEAS = ISNULL(@IMPORTELINEAS, 0) + ISNULL(@CDLINEATOTAL, 0)" & vbCrLf
sConsulta = sConsulta & "SELECT @IMPORTETOTALLINEA = ISNULL(@IMPORTELINEA, 0) + ISNULL(@CDLINEA, 0)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @DCABE=SUM(case when OECD.TIPO = 0 then 0 else CASE WHEN LEN(OECD.OPERACION) = 1 THEN (@IMPORTETOTALLINEA * OECD.VALOR) / @IMPORTETOTALLINEAS ELSE OECD.VALOR END end ) " & vbCrLf
sConsulta = sConsulta & "FROM ORDEN_ENTREGA OE" & vbCrLf
sConsulta = sConsulta & "INNER JOIN ORDEN_ENTREGA_CD OECD WITH(NOLOCK) ON OE.ID=OECD.ORDEN" & vbCrLf
sConsulta = sConsulta & "WHERE OE.ID=@ORDEN AND OE.PEDIDO = @PEDIDO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--impuesto lineas" & vbCrLf
sConsulta = sConsulta & "SELECT @IMPLINEA=SUM((@IMPORTELINEA - @DLINEA - @DCABE) * (LPI.VALOR / 100) )" & vbCrLf
sConsulta = sConsulta & "FROM LINEAS_PEDIDO LP WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "INNER JOIN LINEAS_PEDIDO_IMPUESTO LPI WITH(NOLOCK) ON LP.ID = LPI.LINEA" & vbCrLf
sConsulta = sConsulta & "WHERE LP.PEDIDO=@PEDIDO AND LP.ORDEN=@ORDEN AND LP.CESTA=@LINEA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--IMPUESTOS CABECERA" & vbCrLf
sConsulta = sConsulta & "SELECT @IMPCOSTECABECERA=(@IMPORTETOTALLINEA * SUM(OECD.IMPORTE * (OECDI.VALOR / 100) ) / @IMPORTETOTALLINEAS)" & vbCrLf
sConsulta = sConsulta & "FROM ORDEN_ENTREGA OE" & vbCrLf
sConsulta = sConsulta & "INNER JOIN ORDEN_ENTREGA_CD OECD WITH(NOLOCK) ON OE.ID=OECD.ORDEN" & vbCrLf
sConsulta = sConsulta & "INNER JOIN ORDEN_ENTREGA_CD_IMPUESTO OECDI WITH(NOLOCK) ON OECD.ID = OECDI.COSTE" & vbCrLf
sConsulta = sConsulta & "WHERE OE.ID=@ORDEN AND OE.PEDIDO = @PEDIDO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--COSTES DESCUENTOS CABECERA" & vbCrLf
sConsulta = sConsulta & "SELECT @CDCABE=(@IMPORTETOTALLINEA * SUM(case when OECD.TIPO = 0 then OECD.IMPORTE else OECD.IMPORTE * -1 end ) / @IMPORTETOTALLINEAS)" & vbCrLf
sConsulta = sConsulta & "FROM ORDEN_ENTREGA OE" & vbCrLf
sConsulta = sConsulta & "INNER JOIN ORDEN_ENTREGA_CD OECD WITH(NOLOCK) ON OE.ID=OECD.ORDEN" & vbCrLf
sConsulta = sConsulta & "WHERE OE.ID=@ORDEN AND OE.PEDIDO = @PEDIDO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF (@MODOIMP = 1) " & vbCrLf
sConsulta = sConsulta & " BEGIN" & vbCrLf
sConsulta = sConsulta & "   SELECT @SUMA= ISNULL(@IMPORTELINEA, 0)+ISNULL(@CDCABE, 0)+ISNULL(@IMPCOSTECABECERA, 0)+ISNULL(@CDLINEA, 0)+ISNULL(@IMPCOSTELINEA, 0)+ISNULL(@IMPLINEA, 0)" & vbCrLf
sConsulta = sConsulta & " END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & " BEGIN" & vbCrLf
sConsulta = sConsulta & "   SELECT @SUMA= ISNULL(@IMPORTELINEA, 0)+ISNULL(@CDCABE, 0)+ISNULL(@CDLINEA, 0)" & vbCrLf
sConsulta = sConsulta & " END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @CAMBIO= M.EQUIV, @P5I=P5I.ID FROM MON M WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PRES5_IMPORTES P5I WITH (NOLOCK) ON P5I.MON = M.COD" & vbCrLf
sConsulta = sConsulta & "WHERE P5I.CERRADO=0 AND P5I.PRES0=@PRES0 AND P5I.PRES1=@PRES1" & vbCrLf
sConsulta = sConsulta & "AND (P5I.PRES2 IS NULL AND @PRES2 IS NULL OR P5I.PRES2=@PRES2)" & vbCrLf
sConsulta = sConsulta & "AND (P5I.PRES3 IS NULL AND @PRES3 IS NULL OR P5I.PRES3=@PRES3)" & vbCrLf
sConsulta = sConsulta & "AND (P5I.PRES4 IS NULL AND @PRES4 IS NULL OR P5I.PRES4=@PRES4)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @P5I IS NULL" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       SET @CAMBIO = 1" & vbCrLf
sConsulta = sConsulta & "       SELECT @P5I=P5I.ID " & vbCrLf
sConsulta = sConsulta & "       FROM PRES5_IMPORTES P5I WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       WHERE P5I.CERRADO=0 AND P5I.PRES0=@PRES0 AND P5I.PRES1=@PRES1" & vbCrLf
sConsulta = sConsulta & "       AND (P5I.PRES2 IS NULL AND @PRES2 IS NULL OR P5I.PRES2=@PRES2)" & vbCrLf
sConsulta = sConsulta & "       AND (P5I.PRES3 IS NULL AND @PRES3 IS NULL OR P5I.PRES3=@PRES3)" & vbCrLf
sConsulta = sConsulta & "       AND (P5I.PRES4 IS NULL AND @PRES4 IS NULL OR P5I.PRES4=@PRES4)" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO LINEAS_PED_IMPUTACION(LINEA,UON1,UON2,UON3,UON4,PRES5_IMP,PRES0,PRES1,PRES2,PRES3,PRES4,CAMBIO)" & vbCrLf
sConsulta = sConsulta & "SELECT LP.ID,@UON1,@UON2,@UON3,@UON4,@P5I,@PRES0,@PRES1,@PRES2,@PRES3,@PRES4,@CAMBIO" & vbCrLf
sConsulta = sConsulta & "FROM LINEAS_PEDIDO LP WITH(NOLOCK) WHERE LP.PEDIDO=@PEDIDO AND LP.ORDEN=@ORDEN AND LP.CESTA=@LINEA" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES5_IMPORTES SET COMP=ISNULL(COMP,0) + ((@SUMA) * LP.FC * ISNULL(@CAMBIO,1.0))" & vbCrLf
sConsulta = sConsulta & "FROM LINEAS_PEDIDO LP WITH(NOLOCK) WHERE LP.PEDIDO=@PEDIDO AND LP.ORDEN=@ORDEN AND LP.CESTA=@LINEA AND PRES5_IMPORTES.ID=@P5I" & vbCrLf
sConsulta = sConsulta & "IF (@MODOIMP = 1) " & vbCrLf
sConsulta = sConsulta & " BEGIN" & vbCrLf
sConsulta = sConsulta & "   UPDATE LINEAS_PEDIDO SET SM_IMPORTECOMPRIMPUESTOS=@SUMA WHERE PEDIDO=@PEDIDO AND ORDEN=@ORDEN AND CESTA=@LINEA" & vbCrLf
sConsulta = sConsulta & " END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & " BEGIN" & vbCrLf
sConsulta = sConsulta & "   UPDATE LINEAS_PEDIDO SET SM_IMPORTECOMPR=@SUMA WHERE PEDIDO=@PEDIDO AND ORDEN=@ORDEN AND CESTA=@LINEA" & vbCrLf
sConsulta = sConsulta & " END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & ""

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSEP_INSERTAR_LINEAS_PED_IMPUTACION]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSEP_INSERTAR_LINEAS_PED_IMPUTACION]" & vbCrLf
    gRDOCon.QueryTimeout = 7200
    ExecuteSQL gRDOCon, sConsulta
sConsulta = "CREATE PROCEDURE [dbo].[FSEP_INSERTAR_LINEAS_PED_IMPUTACION]  " & vbCrLf
sConsulta = sConsulta & "@ORDEN INT, @MODOIMP INT," & vbCrLf
sConsulta = sConsulta & "@LINEA INT, @PEDIDO INT, @UON1 VARCHAR(50), @UON2 VARCHAR(50), @UON3 VARCHAR(50), @UON4 VARCHAR(50), @PRES0 VARCHAR(500), @PRES1 VARCHAR(500), @PRES2 VARCHAR(500), @PRES3 VARCHAR(500)," & vbCrLf
sConsulta = sConsulta & "@PRES4 VARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO LINEAS_PED_IMPUTACION(LINEA,UON1,UON2,UON3,UON4,PRES5_IMP,PRES0,PRES1,PRES2,PRES3,PRES4,CAMBIO)" & vbCrLf
sConsulta = sConsulta & "SELECT LP.ID,@UON1,@UON2,@UON3,@UON4,P5I.ID,@PRES0,@PRES1,@PRES2,@PRES3,@PRES4, NULL" & vbCrLf
sConsulta = sConsulta & "FROM LINEAS_PEDIDO LP WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PRES5_IMPORTES P5I WITH(NOLOCK) ON P5I.PRES0=@PRES0 AND P5I.PRES1=@PRES1" & vbCrLf
sConsulta = sConsulta & "AND (P5I.PRES2 IS NULL AND @PRES2 IS NULL OR P5I.PRES2=@PRES2)" & vbCrLf
sConsulta = sConsulta & "AND (P5I.PRES3 IS NULL AND @PRES3 IS NULL OR P5I.PRES3=@PRES3)" & vbCrLf
sConsulta = sConsulta & "AND (P5I.PRES4 IS NULL AND @PRES4 IS NULL OR P5I.PRES4=@PRES4)" & vbCrLf
sConsulta = sConsulta & "WHERE LP.PEDIDO=@PEDIDO AND LP.ORDEN=@ORDEN AND LP.CESTA=@LINEA AND P5I.CERRADO=0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta
 
sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSSM_DEVOLVER_PARTIDAS]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSSM_DEVOLVER_PARTIDAS]" & vbCrLf
    gRDOCon.QueryTimeout = 7200
    ExecuteSQL gRDOCon, sConsulta
sConsulta = "CREATE PROCEDURE [dbo].[FSSM_DEVOLVER_PARTIDAS] @PRES5 VARCHAR(20),@IDI VARCHAR(20),@USU VARCHAR(50)=NULL,@UON1 VARCHAR(10)=NULL,@UON2 VARCHAR(10)=NULL,@UON3 VARCHAR(10)=NULL,@UON4 VARCHAR(10)=NULL,@VIGENTES TINYINT=1,@FECHAINICIODESDE DATETIME=NULL,@FECHAFINHASTA DATETIME=NULL,@FORMATOFECHA AS VARCHAR(50)=NULL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-----------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "-- Descripci�n: Devuelve las partidas y las uons para mostrarlas en un treeview." & vbCrLf
sConsulta = sConsulta & "-- Par�metros de entrada: @PRES5: C�digo de la partida presupuetaria" & vbCrLf
sConsulta = sConsulta & "--         @IDI: Idioma del usuario" & vbCrLf
sConsulta = sConsulta & "--         @USU: C�digo de la persona" & vbCrLf
sConsulta = sConsulta & "--         @UON1: cod de la unidad organizativa de nivel 1" & vbCrLf
sConsulta = sConsulta & "--         @UON2: cod de la unidad organizativa de nivel 2" & vbCrLf
sConsulta = sConsulta & "--         @UON3: cod de la unidad organizativa de nivel 3" & vbCrLf
sConsulta = sConsulta & "--         @UON4: cod de la unidad organizativa de nivel 4" & vbCrLf
sConsulta = sConsulta & "--         @VIGENTES: Si se muestran las partidas vigentes o todas las partidas" & vbCrLf
sConsulta = sConsulta & "--         @FECHAINICIODESDE: Fecha de inicio desde para buscar partidas" & vbCrLf
sConsulta = sConsulta & "--         @FECHAFINHASTA: Fecha fin hasta para buscar partidas" & vbCrLf
sConsulta = sConsulta & "--         @FORMATOFECHA: Formato en el que llegan las fechas" & vbCrLf
sConsulta = sConsulta & "-- Par�metros de salida: las partidas y las uons." & vbCrLf
sConsulta = sConsulta & "-- Llamada desde: SMdatabaseServer --> Root.vb --> Partidas_Load; Tiempo m�ximo: 2 sg." & vbCrLf
sConsulta = sConsulta & "-- Revisado por ILG 20/10/2011" & vbCrLf
sConsulta = sConsulta & "-----------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL  AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLWHERE  AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @TIPOFECHA AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @NIVEL TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL_PR_UON AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLWHERE= N' AND P5I.CERRADO = 0'" & vbCrLf
sConsulta = sConsulta & "SET @TIPOFECHA=103" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @FORMATOFECHA='dd/mm/yyyy'" & vbCrLf
sConsulta = sConsulta & "   SET @TIPOFECHA=103" & vbCrLf
sConsulta = sConsulta & "ELSE IF @FORMATOFECHA='mm/dd/yyyy'" & vbCrLf
sConsulta = sConsulta & "   SET @TIPOFECHA=101" & vbCrLf
sConsulta = sConsulta & "ELSE IF @FORMATOFECHA='dd-mm-yyyy'" & vbCrLf
sConsulta = sConsulta & "   SET @TIPOFECHA=105" & vbCrLf
sConsulta = sConsulta & "ELSE IF @FORMATOFECHA='mm-dd-yyyy'" & vbCrLf
sConsulta = sConsulta & "   SET @TIPOFECHA=110" & vbCrLf
sConsulta = sConsulta & "ELSE IF @FORMATOFECHA='mm.dd.yyyy'" & vbCrLf
sConsulta = sConsulta & "   SET @TIPOFECHA=104" & vbCrLf
sConsulta = sConsulta & "----------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "IF @UON1 IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQLWHERE = @SQLWHERE + '   AND U.UON1 = @UON1'" & vbCrLf
sConsulta = sConsulta & "IF @UON2 IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQLWHERE = @SQLWHERE + '   AND U.UON2 = @UON2'" & vbCrLf
sConsulta = sConsulta & "IF @UON3 IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQLWHERE = @SQLWHERE + '   AND U.UON3 = @UON3'" & vbCrLf
sConsulta = sConsulta & "IF @UON4 IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQLWHERE = @SQLWHERE + '   AND U.UON4 = @UON4'" & vbCrLf
sConsulta = sConsulta & "IF @VIGENTES=1" & vbCrLf
sConsulta = sConsulta & "   SET @SQLWHERE = @SQLWHERE + ' AND ((P5I.FECINI<GETDATE() OR P5I.FECINI IS NULL) AND (P5I.FECFIN>GETDATE() OR P5I.FECFIN IS NULL))'" & vbCrLf
sConsulta = sConsulta & "IF @FECHAINICIODESDE IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQLWHERE = @SQLWHERE + ' AND P5I.FECINI>=CONVERT(datetime,@FECHAINICIODESDE,@TIPOFECHA)'" & vbCrLf
sConsulta = sConsulta & "IF @FECHAFINHASTA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQLWHERE = @SQLWHERE + ' AND P5I.FECFIN<=CONVERT(datetime,@FECHAFINHASTA,@TIPOFECHA)'" & vbCrLf
sConsulta = sConsulta & "----------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @NIVEL=1 --por si acaso" & vbCrLf
sConsulta = sConsulta & "SELECT @NIVEL = IMP_NIVEL FROM PARGEN_SM WITH(NOLOCK) WHERE PRES5=@PRES5" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--creamos una tabla temporal para guardar las partidas" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE #PARTIDAS (UON1 VARCHAR(10) COLLATE DATABASE_DEFAULT, UON2 VARCHAR(10) COLLATE DATABASE_DEFAULT, UON3 VARCHAR(10) COLLATE DATABASE_DEFAULT, UON4 VARCHAR(10) COLLATE DATABASE_DEFAULT, COD VARCHAR(100) COLLATE DATABASE_DEFAULT, DEN VARCHAR(500) COLLATE DATABASE_DEFAULT, FECINI DATETIME, FECFIN DATETIME, VIGENTE TINYINT, NIVEL TINYINT, IMPUTABLE TINYINT)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @NIVEL = 1" & vbCrLf
sConsulta = sConsulta & "   SET @SQL_PR_UON = N' AND U.PRES2 IS NULL AND U.PRES3 IS NULL AND U.PRES4 IS NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE   " & vbCrLf
sConsulta = sConsulta & "   IF @NIVEL = 2" & vbCrLf
sConsulta = sConsulta & "       SET @SQL_PR_UON = N' AND U.PRES3 IS NULL AND U.PRES4 IS NULL'" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "       IF @NIVEL = 3" & vbCrLf
sConsulta = sConsulta & "           SET @SQL_PR_UON = N' AND U.PRES4 IS NULL'" & vbCrLf
sConsulta = sConsulta & "       ELSE" & vbCrLf
sConsulta = sConsulta & "           SET @SQL_PR_UON = N''" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @USU IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   ---- NIVEL 1 ----" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=N'SELECT DISTINCT U.UON1, U.UON2, U.UON3, U.UON4,P.COD, P.COD + '' - '' + I.DEN AS DEN, P5I.FECINI, P5I.FECFIN, case when P5I.FECINI<GETDATE() AND P5I.FECFIN>GETDATE() then 1 else 0 end as VIGENTE, 1 as NIVEL '" & vbCrLf
sConsulta = sConsulta & "   IF @NIVEL > 1" & vbCrLf
sConsulta = sConsulta & "      SET @SQL = @SQL + ',0 AS IMPUTABLE'" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "      SET @SQL = @SQL + ',1 AS IMPUTABLE'" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + ' FROM PRES5_NIV1 P WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "      INNER JOIN PRES5_IDIOMAS I WITH(NOLOCK) ON P.PRES0=I.PRES0 AND P.COD=I.PRES1 AND I.PRES2 IS NULL AND I.PRES3 IS NULL AND I.PRES4 IS NULL AND P.BAJALOG=0" & vbCrLf
sConsulta = sConsulta & "      INNER JOIN PRES5_UON U WITH(NOLOCK) ON P.PRES0=U.PRES0 AND P.COD=U.PRES1' + @SQL_PR_UON" & vbCrLf
sConsulta = sConsulta & "  " & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + ' " & vbCrLf
sConsulta = sConsulta & "      INNER JOIN PRES5_IMPORTES P5I WITH(NOLOCK) ON P.PRES0=P5I.PRES0 AND P.COD=P5I.PRES1 AND P5I.PRES2 IS NULL AND P5I.PRES3 IS NULL AND P5I.PRES4 IS NULL" & vbCrLf
sConsulta = sConsulta & "      WHERE P.PRES0 = @PRES5 AND I.IDIOMA=@IDI ' + @SQLWHERE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=N'INSERT INTO #PARTIDAS (UON1,UON2,UON3,UON4,COD,DEN,FECINI,FECFIN,VIGENTE,NIVEL,IMPUTABLE) ' + @SQL" & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL, N'@PRES5 NVARCHAR(20),@IDI NVARCHAR(20),@USU NVARCHAR(50),@UON1 AS VARCHAR(10),@UON2 AS VARCHAR(10),@UON3 AS VARCHAR(10),@UON4 AS VARCHAR(10),@VIGENTES TINYINT,@FECHAINICIODESDE DATETIME,@FECHAFINHASTA DATETIME,@TIPOFECHA INT', @PRES5=@PRES5,@IDI=@IDI,@USU=@USU,@UON1=@UON1,@UON2=@UON2,@UON3=@UON3,@UON4=@UON4,@VIGENTES=@VIGENTES,@FECHAINICIODESDE=@FECHAINICIODESDE,@FECHAFINHASTA=@FECHAFINHASTA,@TIPOFECHA=@TIPOFECHA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   ---- NIVEL 2 ----" & vbCrLf
sConsulta = sConsulta & "   IF @NIVEL > 1" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "      SET @SQL=N'SELECT DISTINCT U.UON1, U.UON2, U.UON3, U.UON4,P.PRES1 + ''|'' + P.COD, P.COD + '' - '' + I.DEN AS DEN, P5I.FECINI, P5I.FECFIN, case when P5I.FECINI<GETDATE() AND P5I.FECFIN>GETDATE() then 1 else 0 end as VIGENTE, 2 as NIVEL'" & vbCrLf
sConsulta = sConsulta & "      IF @NIVEL > 2" & vbCrLf
sConsulta = sConsulta & "           SET @SQL = @SQL + ',0 AS IMPUTABLE'" & vbCrLf
sConsulta = sConsulta & "      ELSE" & vbCrLf
sConsulta = sConsulta & "           SET @SQL = @SQL + ',1 AS IMPUTABLE'" & vbCrLf
sConsulta = sConsulta & "      SET @SQL = @SQL + ' FROM PRES5_NIV2 P WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "      INNER JOIN PRES5_IDIOMAS I WITH(NOLOCK) ON P.PRES0=I.PRES0 AND P.PRES1=I.PRES1 AND P.COD=I.PRES2 AND I.PRES3 IS NULL AND I.PRES4 IS NULL AND P.BAJALOG=0" & vbCrLf
sConsulta = sConsulta & "      INNER JOIN PRES5_UON U WITH(NOLOCK) ON P.PRES0=U.PRES0 AND P.PRES1=U.PRES1 AND P.COD=U.PRES2' + @SQL_PR_UON" & vbCrLf
sConsulta = sConsulta & "  " & vbCrLf
sConsulta = sConsulta & "      SET @SQL = @SQL + ' " & vbCrLf
sConsulta = sConsulta & "      INNER JOIN PRES5_IMPORTES P5I WITH(NOLOCK) ON P.PRES0=P5I.PRES0 AND P.PRES1=P5I.PRES1 AND P.COD=P5I.PRES2 AND P5I.PRES3 IS NULL AND P5I.PRES4 IS NULL" & vbCrLf
sConsulta = sConsulta & "      WHERE P.PRES0 = @PRES5 AND I.IDIOMA=@IDI ' + @SQLWHERE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "      SET @SQL='INSERT INTO #PARTIDAS (UON1,UON2,UON3,UON4,COD,DEN,FECINI,FECFIN,VIGENTE,NIVEL,IMPUTABLE) ' + @SQL" & vbCrLf
sConsulta = sConsulta & "      EXEC SP_EXECUTESQL @SQL, N'@PRES5 NVARCHAR(20),@IDI NVARCHAR(20),@USU NVARCHAR(50),@UON1 AS VARCHAR(10),@UON2 AS VARCHAR(10),@UON3 AS VARCHAR(10),@UON4 AS VARCHAR(10),@VIGENTES TINYINT,@FECHAINICIODESDE DATETIME,@FECHAFINHASTA DATETIME,@TIPOFECHA INT', @PRES5=@PRES5,@IDI=@IDI,@USU=@USU,@UON1=@UON1,@UON2=@UON2,@UON3=@UON3,@UON4=@UON4,@VIGENTES=@VIGENTES,@FECHAINICIODESDE=@FECHAINICIODESDE,@FECHAFINHASTA=@FECHAFINHASTA,@TIPOFECHA=@TIPOFECHA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   ---- NIVEL 3 ----" & vbCrLf
sConsulta = sConsulta & "   IF @NIVEL > 2" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "      SET @SQL=N'SELECT DISTINCT U.UON1, U.UON2, U.UON3, U.UON4,P.PRES1 + ''|'' + P.PRES2 + ''|'' + P.COD, P.COD + '' - '' + I.DEN AS DEN, P5I.FECINI, P5I.FECFIN, case when P5I.FECINI<GETDATE() AND P5I.FECFIN>GETDATE() then 1 else 0 end as VIGENTE, 3 as NIVEL'" & vbCrLf
sConsulta = sConsulta & "      IF @NIVEL > 3" & vbCrLf
sConsulta = sConsulta & "           SET @SQL = @SQL + ',0 AS IMPUTABLE'" & vbCrLf
sConsulta = sConsulta & "      ELSE" & vbCrLf
sConsulta = sConsulta & "           SET @SQL = @SQL + ',1 AS IMPUTABLE'" & vbCrLf
sConsulta = sConsulta & "      SET @SQL = @SQL + ' FROM PRES5_NIV3 P WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "      INNER JOIN PRES5_IDIOMAS I WITH(NOLOCK) ON P.PRES0=I.PRES0 AND P.PRES1=I.PRES1 AND P.PRES2=I.PRES2 AND P.COD=I.PRES3 AND I.PRES4 IS NULL AND P.BAJALOG=0" & vbCrLf
sConsulta = sConsulta & "      INNER JOIN PRES5_UON U WITH(NOLOCK) ON P.PRES0=U.PRES0 AND P.PRES1=U.PRES1 AND P.PRES2=U.PRES2 AND P.COD=U.PRES3' + @SQL_PR_UON" & vbCrLf
sConsulta = sConsulta & "  " & vbCrLf
sConsulta = sConsulta & "     SET @SQL = @SQL + ' " & vbCrLf
sConsulta = sConsulta & "      INNER JOIN PRES5_IMPORTES P5I WITH(NOLOCK) ON P.PRES0=P5I.PRES0 AND P.PRES1=P5I.PRES1 AND P.PRES2=P5I.PRES2 AND P.COD=P5I.PRES3 AND P5I.PRES4 IS NULL" & vbCrLf
sConsulta = sConsulta & "      WHERE P.PRES0 = @PRES5 AND I.IDIOMA=@IDI ' + @SQLWHERE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "      SET @SQL='INSERT INTO #PARTIDAS (UON1,UON2,UON3,UON4,COD,DEN,FECINI,FECFIN,VIGENTE,NIVEL,IMPUTABLE) ' + @SQL" & vbCrLf
sConsulta = sConsulta & "      EXEC SP_EXECUTESQL @SQL, N'@PRES5 NVARCHAR(20),@IDI NVARCHAR(20),@USU NVARCHAR(50),@UON1 AS VARCHAR(10),@UON2 AS VARCHAR(10),@UON3 AS VARCHAR(10),@UON4 AS VARCHAR(10),@VIGENTES TINYINT,@FECHAINICIODESDE DATETIME,@FECHAFINHASTA DATETIME,@TIPOFECHA INT', @PRES5=@PRES5,@IDI=@IDI,@USU=@USU,@UON1=@UON1,@UON2=@UON2,@UON3=@UON3,@UON4=@UON4,@VIGENTES=@VIGENTES,@FECHAINICIODESDE=@FECHAINICIODESDE,@FECHAFINHASTA=@FECHAFINHASTA,@TIPOFECHA=@TIPOFECHA" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   ---- NIVEL 4 ----" & vbCrLf
sConsulta = sConsulta & "   IF @NIVEL > 3" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "      SET @SQL=N'SELECT U.UON1, U.UON2, U.UON3, U.UON4,P.PRES1 + ''|'' + P.PRES2 + ''|'' + P.PRES3 + ''|'' + P.COD, P.COD + '' - '' + I.DEN AS DEN, P5I.FECINI, P5I.FECFIN, case when P5I.FECINI<GETDATE() AND P5I.FECFIN>GETDATE() then 1 else 0 end as VIGENTE, 4 as NIVEL, 1 AS IMPUTABLE FROM PRES5_NIV4 P WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "      INNER JOIN PRES5_IDIOMAS I WITH(NOLOCK) ON P.PRES0=I.PRES0 AND P.PRES1=I.PRES1 AND P.PRES2=I.PRES2 AND P.PRES3=I.PRES3 AND P.COD=I.PRES4 AND P.BAJALOG=0" & vbCrLf
sConsulta = sConsulta & "      INNER JOIN PRES5_UON U WITH(NOLOCK) ON P.PRES0=U.PRES0 AND P.PRES1=U.PRES1 AND P.PRES2=U.PRES2 AND P.PRES3=U.PRES3 AND P.COD=U.PRES4" & vbCrLf
sConsulta = sConsulta & "      INNER JOIN PRES5_IMPORTES P5I WITH(NOLOCK) ON P.PRES0=P5I.PRES0 AND P.PRES1=P5I.PRES1 AND P.PRES2=P5I.PRES2 AND P.PRES3=P5I.PRES3 AND P.COD=P5I.PRES4" & vbCrLf
sConsulta = sConsulta & "      WHERE P.PRES0 = @PRES5 AND I.IDIOMA=@IDI ' + @SQLWHERE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "      SET @SQL='INSERT INTO #PARTIDAS (UON1,UON2,UON3,UON4,COD,DEN,FECINI,FECFIN,VIGENTE,NIVEL,IMPUTABLE) ' + @SQL" & vbCrLf
sConsulta = sConsulta & "      EXEC SP_EXECUTESQL @SQL, N'@PRES5 NVARCHAR(20),@IDI NVARCHAR(20),@USU NVARCHAR(50),@UON1 AS VARCHAR(10),@UON2 AS VARCHAR(10),@UON3 AS VARCHAR(10),@UON4 AS VARCHAR(10),@VIGENTES TINYINT,@FECHAINICIODESDE DATETIME,@FECHAFINHASTA DATETIME,@TIPOFECHA INT', @PRES5=@PRES5,@IDI=@IDI,@USU=@USU,@UON1=@UON1,@UON2=@UON2,@UON3=@UON3,@UON4=@UON4,@VIGENTES=@VIGENTES,@FECHAINICIODESDE=@FECHAINICIODESDE,@FECHAFINHASTA=@FECHAFINHASTA,@TIPOFECHA=@TIPOFECHA" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   ----------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SELECT '###' as COD0,0 as ID,DEN, NULL as centro_sm FROM PARGEN_LIT WITH(NOLOCK) WHERE IDI='spa' and id = 1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   select distinct '###' as COD0, u.cod as uon1, '' as uon2, '' as uon3, '' as uon4, u.cod, u.cod + ' - ' + u.den as den, su.centro_sm  from uon1 u with(nolock)" & vbCrLf
sConsulta = sConsulta & "   left join centro_sm_uon su with(nolock) on u.cod=su.uon1 and su.uon2 is null and su.uon3 is null and su.uon4 is null" & vbCrLf
sConsulta = sConsulta & "   inner join #PARTIDAS p with(nolock) on u.cod = p.uon1" & vbCrLf
sConsulta = sConsulta & "   where u.bajalog=0 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   select distinct '###' as COD0,u.uon1,u.cod as uon2, '' as uon3, '' as uon4, u.cod,u.cod + ' - ' + u.den as den, su.centro_sm, null as fecini, null as fecfin, null as vigente, null as imputable from uon2 u with(nolock)" & vbCrLf
sConsulta = sConsulta & "   left join centro_sm_uon su with(nolock) on u.uon1=su.uon1 and u.cod=su.uon2 and su.uon3 is null and su.uon4 is null" & vbCrLf
sConsulta = sConsulta & "   inner join #PARTIDAS p with(nolock) on u.cod = p.uon2 and u.uon1 =p.uon1" & vbCrLf
sConsulta = sConsulta & "   where bajalog=0 " & vbCrLf
sConsulta = sConsulta & "   UNION" & vbCrLf
sConsulta = sConsulta & "   select '###' as COD0,p.uon1,'' as uon2, '' as uon3, '' as uon4, p.cod,p.den as den, 'PARTIDA' as centro_sm, fecini, fecfin, vigente, imputable" & vbCrLf
sConsulta = sConsulta & "   from #PARTIDAS p with(nolock)" & vbCrLf
sConsulta = sConsulta & "   where nivel = 1 and p.uon1 is not null and p.uon2 is null and p.uon3 is null and p.uon4 is null" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   select distinct '###' as COD0,u.uon1,u.uon2,u.cod as uon3, '' as uon4, u.cod,u.cod + ' - ' + u.den as den, su.centro_sm, null as fecini, null as fecfin, null as vigente, null as imputable from uon3 u with(nolock)" & vbCrLf
sConsulta = sConsulta & "   left join centro_sm_uon su with(nolock) on u.uon1=su.uon1 and u.uon2=su.uon2 and u.cod=su.uon3 and su.uon4 is null" & vbCrLf
sConsulta = sConsulta & "   inner join #PARTIDAS p with(nolock) on u.cod=p.uon3 and u.uon2=p.uon2 and u.uon1 =p.uon1" & vbCrLf
sConsulta = sConsulta & "   where bajalog=0 " & vbCrLf
sConsulta = sConsulta & "   UNION" & vbCrLf
sConsulta = sConsulta & "   select '###' as COD0,p.uon1,p.uon2 as uon2, '' as uon3, '' as uon4, p.cod,p.den as den, 'PARTIDA' as centro_sm, fecini, fecfin, vigente, imputable" & vbCrLf
sConsulta = sConsulta & "   from #PARTIDAS p with(nolock)" & vbCrLf
sConsulta = sConsulta & "   where nivel = 1 and p.uon1 is not null and p.uon2 is not null and p.uon3 is null and p.uon4 is null" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   select distinct '###' as COD0,u.uon1,u.uon2,u.uon3,u.cod as uon4, u.cod,u.cod + ' - ' + u.den as den, su.centro_sm, null as fecini, null as fecfin, null as vigente, null as imputable from uon4 u with(nolock)" & vbCrLf
sConsulta = sConsulta & "   left join centro_sm_uon su with(nolock) on u.uon1=su.uon1 and u.uon2=su.uon2 and u.uon3=su.uon3 and u.cod=su.uon4" & vbCrLf
sConsulta = sConsulta & "   inner join #PARTIDAS p with(nolock) on u.cod=p.uon4 and u.uon3=p.uon3 and u.uon2=p.uon2 and u.uon1=p.uon1" & vbCrLf
sConsulta = sConsulta & "   where bajalog=0 " & vbCrLf
sConsulta = sConsulta & "   UNION" & vbCrLf
sConsulta = sConsulta & "   select '###' as COD0,p.uon1,p.uon2 as uon2, p.uon3 as uon3, '' as uon4, p.cod,p.den as den, 'PARTIDA' as centro_sm, fecini, fecfin, vigente, imputable" & vbCrLf
sConsulta = sConsulta & "   from #PARTIDAS p with(nolock)" & vbCrLf
sConsulta = sConsulta & "   where nivel = 1 and p.uon1 is not null and p.uon2 is not null and p.uon3 is not null and p.uon4 is null" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   select distinct  '###' as COD0, uon1, isnull(uon2,'') as uon2, isnull(uon3,'') as uon3, isnull(uon4,'') as uon4, cod, den, fecini, fecfin, vigente, 'PARTIDA' as centro_sm, IMPUTABLE from #PARTIDAS with(nolock)" & vbCrLf
sConsulta = sConsulta & "   where nivel = 1 and uon1 is not null and uon2 is not null and uon3 is not null and uon4 is not null" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   select distinct  '###' as COD0, uon1, isnull(uon2,'') as uon2, isnull(uon3,'') as uon3, isnull(uon4,'') as uon4, cod, den, fecini, fecfin, vigente, 'PARTIDA' as centro_sm, IMPUTABLE from #PARTIDAS with(nolock) where nivel = 2" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   select distinct  '###' as COD0, uon1, isnull(uon2,'') as uon2, isnull(uon3,'') as uon3, isnull(uon4,'') as uon4, cod, den, fecini, fecfin, vigente, 'PARTIDA' as centro_sm, IMPUTABLE from #PARTIDAS with(nolock) where nivel = 3" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   select distinct  '###' as COD0, uon1, isnull(uon2,'') as uon2, isnull(uon3,'') as uon3, isnull(uon4,'') as uon4, cod, den, fecini, fecfin, vigente, 'PARTIDA' as centro_sm, IMPUTABLE from #PARTIDAS with(nolock) where nivel = 4" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   ---- NIVEL 1 ----" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=N'SELECT DISTINCT U.UON1, U.UON2, U.UON3, U.UON4,P.COD, P.COD + '' - '' + I.DEN AS DEN, P5I.FECINI, P5I.FECFIN, case when P5I.FECINI<GETDATE() AND P5I.FECFIN>GETDATE() then 1 else 0 end as VIGENTE, 1 as NIVEL '" & vbCrLf
sConsulta = sConsulta & "   IF @NIVEL > 1" & vbCrLf
sConsulta = sConsulta & "      SET @SQL = @SQL + ',0 AS IMPUTABLE'" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "      SET @SQL = @SQL + ',1 AS IMPUTABLE'" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + ' FROM PRES5_NIV1 P WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "      INNER JOIN PRES5_IDIOMAS I WITH(NOLOCK) ON P.PRES0=I.PRES0 AND P.COD=I.PRES1 AND I.PRES2 IS NULL AND I.PRES3 IS NULL AND I.PRES4 IS NULL AND P.BAJALOG=0'" & vbCrLf
sConsulta = sConsulta & "  " & vbCrLf
sConsulta = sConsulta & "  SET @SQL = @SQL + ' INNER JOIN PRES5_UON U WITH(NOLOCK) ON P.PRES0=U.PRES0 AND P.COD=U.PRES1' + @SQL_PR_UON" & vbCrLf
sConsulta = sConsulta & "  " & vbCrLf
sConsulta = sConsulta & "  SET @SQL = @SQL + ' INNER JOIN USU_CC_IMPUTACION CI WITH(NOLOCK) ON U.UON1=CI.UON1 AND ISNULL(U.UON2,0)=ISNULL(CI.UON2,0) AND ISNULL(U.UON3,0)=ISNULL(CI.UON3,0) AND ISNULL(U.UON4,0)=ISNULL(CI.UON4,0)" & vbCrLf
sConsulta = sConsulta & "      INNER JOIN PRES5_IMPORTES P5I WITH(NOLOCK) ON P.PRES0=P5I.PRES0 AND P.COD=P5I.PRES1 AND P5I.PRES2 IS NULL AND P5I.PRES3 IS NULL AND P5I.PRES4 IS NULL" & vbCrLf
sConsulta = sConsulta & "      WHERE P.PRES0 = @PRES5 AND I.IDIOMA=@IDI AND CI.USU=@USU' + @SQLWHERE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=N'INSERT INTO #PARTIDAS (UON1,UON2,UON3,UON4,COD,DEN,FECINI,FECFIN,VIGENTE,NIVEL,IMPUTABLE) ' + @SQL" & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL, N'@PRES5 NVARCHAR(20),@IDI NVARCHAR(20),@USU NVARCHAR(50),@UON1 AS VARCHAR(10),@UON2 AS VARCHAR(10),@UON3 AS VARCHAR(10),@UON4 AS VARCHAR(10),@VIGENTES TINYINT,@FECHAINICIODESDE DATETIME,@FECHAFINHASTA DATETIME,@TIPOFECHA INT', @PRES5=@PRES5,@IDI=@IDI,@USU=@USU,@UON1=@UON1,@UON2=@UON2,@UON3=@UON3,@UON4=@UON4,@VIGENTES=@VIGENTES,@FECHAINICIODESDE=@FECHAINICIODESDE,@FECHAFINHASTA=@FECHAFINHASTA,@TIPOFECHA=@TIPOFECHA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   ---- NIVEL 2 ----" & vbCrLf
sConsulta = sConsulta & "   IF @NIVEL > 1" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "      SET @SQL=N'SELECT DISTINCT U.UON1, U.UON2, U.UON3, U.UON4,P.PRES1 + ''|'' + P.COD, P.COD + '' - '' + I.DEN AS DEN, P5I.FECINI, P5I.FECFIN, case when P5I.FECINI<GETDATE() AND P5I.FECFIN>GETDATE() then 1 else 0 end as VIGENTE, 2 as NIVEL'" & vbCrLf
sConsulta = sConsulta & "      IF @NIVEL > 2" & vbCrLf
sConsulta = sConsulta & "           SET @SQL = @SQL + ',0 AS IMPUTABLE'" & vbCrLf
sConsulta = sConsulta & "      ELSE" & vbCrLf
sConsulta = sConsulta & "           SET @SQL = @SQL + ',1 AS IMPUTABLE'" & vbCrLf
sConsulta = sConsulta & "      SET @SQL = @SQL + ' FROM PRES5_NIV2 P WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "      INNER JOIN PRES5_IDIOMAS I WITH(NOLOCK) ON P.PRES0=I.PRES0 AND P.PRES1=I.PRES1 AND P.COD=I.PRES2 AND I.PRES3 IS NULL AND I.PRES4 IS NULL AND P.BAJALOG=0'" & vbCrLf
sConsulta = sConsulta & "  " & vbCrLf
sConsulta = sConsulta & "  SET @SQL = @SQL + ' INNER JOIN PRES5_UON U WITH(NOLOCK) ON P.PRES0=U.PRES0 AND P.PRES1=U.PRES1 AND P.COD=U.PRES2' + @SQL_PR_UON" & vbCrLf
sConsulta = sConsulta & "  " & vbCrLf
sConsulta = sConsulta & "  SET @SQL = @SQL + ' INNER JOIN USU_CC_IMPUTACION CI WITH(NOLOCK) ON U.UON1=CI.UON1 AND ISNULL(U.UON2,0)=ISNULL(CI.UON2,0) AND ISNULL(U.UON3,0)=ISNULL(CI.UON3,0) AND ISNULL(U.UON4,0)=ISNULL(CI.UON4,0)" & vbCrLf
sConsulta = sConsulta & "      INNER JOIN PRES5_IMPORTES P5I WITH(NOLOCK) ON P.PRES0=P5I.PRES0 AND P.PRES1=P5I.PRES1 AND P.COD=P5I.PRES2 AND P5I.PRES3 IS NULL AND P5I.PRES4 IS NULL" & vbCrLf
sConsulta = sConsulta & "      WHERE P.PRES0 = @PRES5 AND I.IDIOMA=@IDI AND CI.USU=@USU' + @SQLWHERE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "      SET @SQL='INSERT INTO #PARTIDAS (UON1,UON2,UON3,UON4,COD,DEN,FECINI,FECFIN,VIGENTE,NIVEL,IMPUTABLE) ' + @SQL" & vbCrLf
sConsulta = sConsulta & "      EXEC SP_EXECUTESQL @SQL, N'@PRES5 NVARCHAR(20),@IDI NVARCHAR(20),@USU NVARCHAR(50),@UON1 AS VARCHAR(10),@UON2 AS VARCHAR(10),@UON3 AS VARCHAR(10),@UON4 AS VARCHAR(10),@VIGENTES TINYINT,@FECHAINICIODESDE DATETIME,@FECHAFINHASTA DATETIME,@TIPOFECHA INT', @PRES5=@PRES5,@IDI=@IDI,@USU=@USU,@UON1=@UON1,@UON2=@UON2,@UON3=@UON3,@UON4=@UON4,@VIGENTES=@VIGENTES,@FECHAINICIODESDE=@FECHAINICIODESDE,@FECHAFINHASTA=@FECHAFINHASTA,@TIPOFECHA=@TIPOFECHA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   ---- NIVEL 3 ----" & vbCrLf
sConsulta = sConsulta & "   IF @NIVEL > 2" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "      SET @SQL=N'SELECT DISTINCT U.UON1, U.UON2, U.UON3, U.UON4,P.PRES1 + ''|'' + P.PRES2 + ''|'' + P.COD, P.COD + '' - '' + I.DEN AS DEN, P5I.FECINI, P5I.FECFIN, case when P5I.FECINI<GETDATE() AND P5I.FECFIN>GETDATE() then 1 else 0 end as VIGENTE, 3 as NIVEL'" & vbCrLf
sConsulta = sConsulta & "      IF @NIVEL > 3" & vbCrLf
sConsulta = sConsulta & "           SET @SQL = @SQL + ',0 AS IMPUTABLE'" & vbCrLf
sConsulta = sConsulta & "      ELSE" & vbCrLf
sConsulta = sConsulta & "           SET @SQL = @SQL + ',1 AS IMPUTABLE'" & vbCrLf
sConsulta = sConsulta & "      SET @SQL = @SQL + ' FROM PRES5_NIV3 P WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "      INNER JOIN PRES5_IDIOMAS I WITH(NOLOCK) ON P.PRES0=I.PRES0 AND P.PRES1=I.PRES1 AND P.PRES2=I.PRES2 AND P.COD=I.PRES3 AND I.PRES4 IS NULL AND P.BAJALOG=0'" & vbCrLf
sConsulta = sConsulta & "  " & vbCrLf
sConsulta = sConsulta & "  SET @SQL = @SQL + ' INNER JOIN PRES5_UON U WITH(NOLOCK) ON P.PRES0=U.PRES0 AND P.PRES1=U.PRES1 AND P.PRES2=U.PRES2 AND P.COD=U.PRES3' + @SQL_PR_UON" & vbCrLf
sConsulta = sConsulta & "  " & vbCrLf
sConsulta = sConsulta & "  SET @SQL = @SQL + ' INNER JOIN USU_CC_IMPUTACION CI WITH(NOLOCK) ON U.UON1=CI.UON1 AND ISNULL(U.UON2,0)=ISNULL(CI.UON2,0) AND ISNULL(U.UON3,0)=ISNULL(CI.UON3,0) AND ISNULL(U.UON4,0)=ISNULL(CI.UON4,0)" & vbCrLf
sConsulta = sConsulta & "      INNER JOIN PRES5_IMPORTES P5I WITH(NOLOCK) ON P.PRES0=P5I.PRES0 AND P.PRES1=P5I.PRES1 AND P.PRES2=P5I.PRES2 AND P.COD=P5I.PRES3 AND P5I.PRES4 IS NULL" & vbCrLf
sConsulta = sConsulta & "      WHERE P.PRES0 = @PRES5 AND I.IDIOMA=@IDI AND CI.USU=@USU' + @SQLWHERE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "      SET @SQL='INSERT INTO #PARTIDAS (UON1,UON2,UON3,UON4,COD,DEN,FECINI,FECFIN,VIGENTE,NIVEL,IMPUTABLE) ' + @SQL" & vbCrLf
sConsulta = sConsulta & "      EXEC SP_EXECUTESQL @SQL, N'@PRES5 NVARCHAR(20),@IDI NVARCHAR(20),@USU NVARCHAR(50),@UON1 AS VARCHAR(10),@UON2 AS VARCHAR(10),@UON3 AS VARCHAR(10),@UON4 AS VARCHAR(10),@VIGENTES TINYINT,@FECHAINICIODESDE DATETIME,@FECHAFINHASTA DATETIME,@TIPOFECHA INT', @PRES5=@PRES5,@IDI=@IDI,@USU=@USU,@UON1=@UON1,@UON2=@UON2,@UON3=@UON3,@UON4=@UON4,@VIGENTES=@VIGENTES,@FECHAINICIODESDE=@FECHAINICIODESDE,@FECHAFINHASTA=@FECHAFINHASTA,@TIPOFECHA=@TIPOFECHA" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   ---- NIVEL 4 ----" & vbCrLf
sConsulta = sConsulta & "   IF @NIVEL > 3" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "      SET @SQL=N'SELECT U.UON1, U.UON2, U.UON3, U.UON4,P.PRES1 + ''|'' + P.PRES2 + ''|'' + P.PRES3 + ''|'' + P.COD, P.COD + '' - '' + I.DEN AS DEN, P5I.FECINI, P5I.FECFIN, case when P5I.FECINI<GETDATE() AND P5I.FECFIN>GETDATE() then 1 else 0 end as VIGENTE, 4 as NIVEL, 1 AS IMPUTABLE FROM PRES5_NIV4 P WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "      INNER JOIN PRES5_IDIOMAS I WITH(NOLOCK) ON P.PRES0=I.PRES0 AND P.PRES1=I.PRES1 AND P.PRES2=I.PRES2 AND P.PRES3=I.PRES3 AND P.COD=I.PRES4 AND P.BAJALOG=0" & vbCrLf
sConsulta = sConsulta & "      INNER JOIN PRES5_UON U WITH(NOLOCK) ON P.PRES0=U.PRES0 AND P.PRES1=U.PRES1 AND P.PRES2=U.PRES2 AND P.PRES3=U.PRES3 AND P.COD=U.PRES4" & vbCrLf
sConsulta = sConsulta & "      INNER JOIN USU_CC_IMPUTACION CI WITH(NOLOCK) ON U.UON1=CI.UON1 AND ISNULL(U.UON2,0)=ISNULL(CI.UON2,0) AND ISNULL(U.UON3,0)=ISNULL(CI.UON3,0) AND ISNULL(U.UON4,0)=ISNULL(CI.UON4,0)" & vbCrLf
sConsulta = sConsulta & "      INNER JOIN PRES5_IMPORTES P5I WITH(NOLOCK) ON P.PRES0=P5I.PRES0 AND P.PRES1=P5I.PRES1 AND P.PRES2=P5I.PRES2 AND P.PRES3=P5I.PRES3 AND P.COD=P5I.PRES4" & vbCrLf
sConsulta = sConsulta & "      WHERE P.PRES0 = @PRES5 AND I.IDIOMA=@IDI AND CI.USU=@USU' + @SQLWHERE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "      SET @SQL='INSERT INTO #PARTIDAS (UON1,UON2,UON3,UON4,COD,DEN,FECINI,FECFIN,VIGENTE,NIVEL,IMPUTABLE) ' + @SQL" & vbCrLf
sConsulta = sConsulta & "      EXEC SP_EXECUTESQL @SQL, N'@PRES5 NVARCHAR(20),@IDI NVARCHAR(20),@USU NVARCHAR(50),@UON1 AS VARCHAR(10),@UON2 AS VARCHAR(10),@UON3 AS VARCHAR(10),@UON4 AS VARCHAR(10),@VIGENTES TINYINT,@FECHAINICIODESDE DATETIME,@FECHAFINHASTA DATETIME,@TIPOFECHA INT', @PRES5=@PRES5,@IDI=@IDI,@USU=@USU,@UON1=@UON1,@UON2=@UON2,@UON3=@UON3,@UON4=@UON4,@VIGENTES=@VIGENTES,@FECHAINICIODESDE=@FECHAINICIODESDE,@FECHAFINHASTA=@FECHAFINHASTA,@TIPOFECHA=@TIPOFECHA" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   ----------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SELECT '###' as COD0,0 as ID,DEN, NULL as centro_sm FROM PARGEN_LIT WITH(NOLOCK) WHERE IDI='spa' and id = 1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   select distinct '###' as COD0, u.cod as uon1, '' as uon2, '' as uon3, '' as uon4, u.cod, u.cod + ' - ' + u.den as den, su.centro_sm  from uon1 u with(nolock)" & vbCrLf
sConsulta = sConsulta & "   inner join usu_cc_imputacion c with(nolock) on u.cod=c.uon1" & vbCrLf
sConsulta = sConsulta & "   left join centro_sm_uon su with(nolock) on u.cod=su.uon1 and su.uon2 is null and su.uon3 is null and su.uon4 is null" & vbCrLf
sConsulta = sConsulta & "   inner join #PARTIDAS p with(nolock) on u.cod = p.uon1" & vbCrLf
sConsulta = sConsulta & "   where u.bajalog=0 and c.usu = @usu" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   select distinct '###' as COD0,u.uon1,u.cod as uon2, '' as uon3, '' as uon4, u.cod,u.cod + ' - ' + u.den as den, su.centro_sm, null as fecini, null as fecfin, null as vigente, null as imputable from uon2 u with(nolock)" & vbCrLf
sConsulta = sConsulta & "   inner join usu_cc_imputacion c with(nolock) on u.cod=c.uon2 and u.uon1=c.uon1" & vbCrLf
sConsulta = sConsulta & "   left join centro_sm_uon su with(nolock) on u.uon1=su.uon1 and u.cod=su.uon2 and su.uon3 is null and su.uon4 is null" & vbCrLf
sConsulta = sConsulta & "   inner join #PARTIDAS p with(nolock) on u.cod = p.uon2 and u.uon1 =p.uon1" & vbCrLf
sConsulta = sConsulta & "   where bajalog=0 and c.usu = @usu" & vbCrLf
sConsulta = sConsulta & "   UNION" & vbCrLf
sConsulta = sConsulta & "   select '###' as COD0,p.uon1,'' as uon2, '' as uon3, '' as uon4, p.cod,p.den as den, 'PARTIDA' as centro_sm, fecini, fecfin, vigente, imputable" & vbCrLf
sConsulta = sConsulta & "   from #PARTIDAS p with(nolock)" & vbCrLf
sConsulta = sConsulta & "   where nivel = 1 and p.uon1 is not null and p.uon2 is null and p.uon3 is null and p.uon4 is null" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   select distinct '###' as COD0,u.uon1,u.uon2,u.cod as uon3, '' as uon4, u.cod,u.cod + ' - ' + u.den as den, su.centro_sm, null as fecini, null as fecfin, null as vigente, null as imputable from uon3 u with(nolock)" & vbCrLf
sConsulta = sConsulta & "   inner join usu_cc_imputacion c with(nolock) on u.cod=c.uon3 and u.uon2=c.uon2 and u.uon1=c.uon1" & vbCrLf
sConsulta = sConsulta & "   left join centro_sm_uon su with(nolock) on u.uon1=su.uon1 and u.uon2=su.uon2 and u.cod=su.uon3 and su.uon4 is null" & vbCrLf
sConsulta = sConsulta & "   inner join #PARTIDAS p with(nolock) on u.cod=p.uon3 and u.uon2=p.uon2 and u.uon1 =p.uon1" & vbCrLf
sConsulta = sConsulta & "   where bajalog=0 and c.usu = @usu" & vbCrLf
sConsulta = sConsulta & "   UNION" & vbCrLf
sConsulta = sConsulta & "   select '###' as COD0,p.uon1,p.uon2 as uon2, '' as uon3, '' as uon4, p.cod,p.den as den, 'PARTIDA' as centro_sm, fecini, fecfin, vigente, imputable" & vbCrLf
sConsulta = sConsulta & "   from #PARTIDAS p with(nolock)" & vbCrLf
sConsulta = sConsulta & "   where nivel = 1 and p.uon1 is not null and p.uon2 is not null and p.uon3 is null and p.uon4 is null" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   select distinct '###' as COD0,u.uon1,u.uon2,u.uon3,u.cod as uon4, u.cod,u.cod + ' - ' + u.den as den, su.centro_sm, null as fecini, null as fecfin, null as vigente, null as imputable from uon4 u with(nolock)" & vbCrLf
sConsulta = sConsulta & "   inner join usu_cc_imputacion c with(nolock) on u.cod=c.uon4 and u.uon3=c.uon3 and u.uon2=c.uon2 and u.uon1=c.uon1" & vbCrLf
sConsulta = sConsulta & "   left join centro_sm_uon su with(nolock) on u.uon1=su.uon1 and u.uon2=su.uon2 and u.uon3=su.uon3 and u.cod=su.uon4" & vbCrLf
sConsulta = sConsulta & "   inner join #PARTIDAS p with(nolock) on u.cod=p.uon4 and u.uon3=p.uon3 and u.uon2=p.uon2 and u.uon1=p.uon1" & vbCrLf
sConsulta = sConsulta & "   where bajalog=0 and c.usu = @usu" & vbCrLf
sConsulta = sConsulta & "   UNION" & vbCrLf
sConsulta = sConsulta & "   select '###' as COD0,p.uon1,p.uon2 as uon2, p.uon3 as uon3, '' as uon4, p.cod,p.den as den, 'PARTIDA' as centro_sm, fecini, fecfin, vigente, imputable" & vbCrLf
sConsulta = sConsulta & "   from #PARTIDAS p with(nolock)" & vbCrLf
sConsulta = sConsulta & "   where nivel = 1 and p.uon1 is not null and p.uon2 is not null and p.uon3 is not null and p.uon4 is null" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   select distinct  '###' as COD0, uon1, isnull(uon2,'') as uon2, isnull(uon3,'') as uon3, isnull(uon4,'') as uon4, cod, den, fecini, fecfin, vigente, 'PARTIDA' as centro_sm, IMPUTABLE from #PARTIDAS with(nolock)" & vbCrLf
sConsulta = sConsulta & "   where nivel = 1 and uon1 is not null and uon2 is not null and uon3 is not null and uon4 is not null" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   select distinct  '###' as COD0, uon1, isnull(uon2,'') as uon2, isnull(uon3,'') as uon3, isnull(uon4,'') as uon4, cod, den, fecini, fecfin, vigente, 'PARTIDA' as centro_sm, IMPUTABLE from #PARTIDAS with(nolock) where nivel = 2" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   select distinct  '###' as COD0, uon1, isnull(uon2,'') as uon2, isnull(uon3,'') as uon3, isnull(uon4,'') as uon4, cod, den, fecini, fecfin, vigente, 'PARTIDA' as centro_sm, IMPUTABLE from #PARTIDAS with(nolock) where nivel = 3" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   select distinct  '###' as COD0, uon1, isnull(uon2,'') as uon2, isnull(uon3,'') as uon3, isnull(uon4,'') as uon4, cod, den, fecini, fecfin, vigente, 'PARTIDA' as centro_sm, IMPUTABLE from #PARTIDAS with(nolock) where nivel = 4" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DROP TABLE #PARTIDAS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSGSW_DEVOLVER_ITEMSGRUPO]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSGSW_DEVOLVER_ITEMSGRUPO]" & vbCrLf
    gRDOCon.QueryTimeout = 7200
    ExecuteSQL gRDOCon, sConsulta
sConsulta = "CREATE PROCEDURE [dbo].[FSGSW_DEVOLVER_ITEMSGRUPO] @ANYO INT, @GMN1 VARCHAR(50), @PROCE INT, @CODGRUPO VARCHAR(20),@PROVE VARCHAR(50),@IDEMP INT AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @EST INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PARAM AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @ERP INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ESCALADOS INT=0" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDGRUPO INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @USAR_ART4_UON INT=0" & vbCrLf
sConsulta = sConsulta & "DECLARE @TRASPASO_PLANIF INT=0" & vbCrLf
sConsulta = sConsulta & "DECLARE @USAR_ORGCOMPRAS INT =0" & vbCrLf
sConsulta = sConsulta & "DECLARE @DISTR AS BIT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO_L INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN1_L VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCE_L INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @CODGRUPO_L VARCHAR(20)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVE_L VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDEMP_L INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @ANYO_L =@ANYO" & vbCrLf
sConsulta = sConsulta & "SET @GMN1_L =@GMN1" & vbCrLf
sConsulta = sConsulta & "SET @PROCE_L =@PROCE" & vbCrLf
sConsulta = sConsulta & "SET @CODGRUPO_L =@CODGRUPO" & vbCrLf
sConsulta = sConsulta & "SET @PROVE_L =@PROVE" & vbCrLf
sConsulta = sConsulta & "SET @IDEMP_L =@IDEMP" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @ESCALADOS=COUNT(ID) FROM PROCE_GRUPO PG WITH (NOLOCK) WHERE PG.ANYO=@ANYO_L  AND PG.GMN1=@GMN1_L AND PG.PROCE=@PROCE_L AND PG.COD=@CODGRUPO_L AND PG.ESCALADOS=1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @EST=EST FROM PROCE P WITH (NOLOCK) WHERE ANYO =@ANYO_L AND COD=@PROCE_L AND GMN1=@GMN1_L" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @ERP=ES.ERP FROM ERP_SOCIEDAD ES WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN EMP E WITH (NOLOCK) ON E.SOCIEDAD=ES.SOCIEDAD AND E.ID=@IDEMP_L" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @IDGRUPO=ID FROM PROCE_GRUPO PG WITH (NOLOCK) WHERE PG.ANYO=@ANYO_L AND PG.GMN1=@GMN1_L AND PG.PROCE=@PROCE_L AND PG.COD=@CODGRUPO_L" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @USAR_ART4_UON=USAR_ART4_UON, @TRASPASO_PLANIF=TRASPASO_ART_PLANIF, @USAR_ORGCOMPRAS=USAR_ORGCOMPRAS  FROM PARGEN_INTERNO WITH(NOLOCK) WHERE ID=1" & vbCrLf
sConsulta = sConsulta & "SELECT @DISTR=(CASE WHEN Z.NUM=0 THEN 0 ELSE 1 END) FROM (SELECT COUNT(PROVEP) NUM FROM PROVE_REL PR WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN GS_PROVETREL GP WITH (NOLOCK) ON PR.PROVETREL=GP.ID AND PR.PROVEP=@PROVE_L AND GP.PEDIDO=1) Z" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL='SELECT A.*, '" & vbCrLf
sConsulta = sConsulta & "IF @USAR_ORGCOMPRAS =1" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + 'CASE WHEN A.ACCION=''D'' THEN IT_CEN.CENTRO ELSE CASE WHEN A.NUM_CENTROS=1 THEN COALESCE(CENTRO_LIA,it_cen.CENTRO,'''') ELSE '''' END END CENTROS,'''' CENTRO '" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + ''''' CENTROS,'''' AS CENTRO '" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + 'FROM (SELECT DISTINCT '" & vbCrLf
sConsulta = sConsulta & "IF @DISTR=1" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + ' COALESCE(LIA2.ID,LIA1.ID) LIA_ID,'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + ' LIA1.ID LIA_ID,'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' I.ID ID_ITEM,I.ART CODART,I.DESCR DENART,I.UNI'" & vbCrLf
sConsulta = sConsulta & "if  @ESCALADOS>0" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + ',PGE.ID ESC_ID,PGE.INICIO,PGE.FIN'" & vbCrLf
sConsulta = sConsulta & "else" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + ',IA.CANT_ADJ*IT_EMP.PORCEN CANT_ADJ'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ',IOF.PREC_VALIDO PREC_ADJ,P.MON,'" & vbCrLf
sConsulta = sConsulta & "IF @USAR_ORGCOMPRAS =1  --Si el ?ltimo traspaso es un borrado, lee lo que haya en el proceso. Si no (era una I), lee lo de log_item_adj" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + 'COUNT(IT_CEN.CENTRO) NUM_CENTROS ,'" & vbCrLf
sConsulta = sConsulta & "-- ELSE" & vbCrLf
sConsulta = sConsulta & "--SET @SQL=@SQL + ''''' CENTROS,'''' AS CENTRO,'" & vbCrLf
sConsulta = sConsulta & "IF @DISTR=1" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + ' COALESCE(LIA2.CENTRO,LIA1.CENTRO) CENTRO_LIA, COALESCE(LIA2.ACCION,LIA1.ACCION) ACCION" & vbCrLf
sConsulta = sConsulta & "   ,CASE WHEN COALESCE(LIA2.ACCION,LIA1.ACCION) = ''D'' AND LG.ESTADO=4  THEN NULL ELSE LG.ESTADO END AS ESTADOINT" & vbCrLf
sConsulta = sConsulta & "   ,CASE WHEN COALESCE(LIA2.ACCION,LIA1.ACCION) = ''D'' AND LG.ESTADO=4 THEN NULL ELSE LG.FECENV END FECENV" & vbCrLf
sConsulta = sConsulta & "   ,CASE WHEN COALESCE(LIA2.ACCION,LIA1.ACCION) = ''D'' AND LG.ESTADO=4 THEN NULL ELSE LIA1.USU  END USU'" & vbCrLf
sConsulta = sConsulta & "else" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + ' LIA1.CENTRO CENTRO_LIA,LIA1.ACCION ACCION ,CASE WHEN LIA1.ACCION = ''D'' AND LG.ESTADO=4  THEN NULL ELSE LG.ESTADO END AS ESTADOINT" & vbCrLf
sConsulta = sConsulta & "   ,CASE WHEN LIA1.ACCION= ''D'' AND LG.ESTADO=4 THEN NULL ELSE LG.FECENV END FECENV" & vbCrLf
sConsulta = sConsulta & "   ,CASE WHEN LIA1.ACCION = ''D'' AND LG.ESTADO=4 THEN NULL ELSE LIA1.USU  END USU'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ', I.FECINI FECINI_SUM,I.FECFIN FECFIN_SUM,PE.COD_ERP PROVE_ERP,PE.DEN_ERP," & vbCrLf
sConsulta = sConsulta & "COALESCE(IT_ATRIB1.INT_ATRIB_ID,IAE.ATRIB,PGA.ATRIB,PATR.ATRIB,PA.ATRIB,DA.ID) ATRIBID," & vbCrLf
sConsulta = sConsulta & "DA.COD ATRIBCOD,DA.DEN ATRIBDEN," & vbCrLf
sConsulta = sConsulta & "COALESCE(IT_ATRIB1.VALOR_TEXT,IAE.VALOR_TEXT,PGA.VALOR_TEXT,PATR.VALOR_TEXT,OA.VALOR_TEXT,OGATR.VALOR_TEXT,OATR.VALOR_TEXT,IAT.VALOR_TEXT) ATRIBVALOR_TEXT ," & vbCrLf
sConsulta = sConsulta & "COALESCE(IT_ATRIB1.VALOR_NUM ,IAE.VALOR_NUM,PGA.VALOR_NUM,PATR.VALOR_NUM,OA.VALOR_NUM,OGATR.VALOR_NUM,OATR.VALOR_NUM,IAT.VALOR_NUM) ATRIBVALOR_NUM," & vbCrLf
sConsulta = sConsulta & "COALESCE(IT_ATRIB1.VALOR_FEC ,IAE.VALOR_FEC,PGA.VALOR_FEC,PATR.VALOR_FEC,OA.VALOR_FEC,OGATR.VALOR_FEC,OATR.VALOR_FEC,IAT.VALOR_FEC) ATRIBVALOR_FEC," & vbCrLf
sConsulta = sConsulta & "COALESCE(IT_ATRIB1.VALOR_BOOL,IAE.VALOR_BOOL,PGA.VALOR_BOOL,PATR.VALOR_BOOL,OA.VALOR_BOOL,OGATR.VALOR_BOOL,OATR.VALOR_BOOL,IAT.VALOR_BOOL) ATRIBVALOR_BOOL," & vbCrLf
sConsulta = sConsulta & "DA.TIPO ATRIBTIPO,IAT.OBLIGATORIO ATRIBOBLIGATORIO,'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @USAR_ORGCOMPRAS=1" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + 'IT_EMP.UON1 UON1,IT_EMP.UON2 UON2,IT_EMP.UON3 UON3, '" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + 'NULL AS UON1,NULL AS UON2, NULL AS UON3,'" & vbCrLf
sConsulta = sConsulta & "IF @DISTR=1" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + '1 DISTRIBUIDOR'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + '0 DISTRIBUIDOR'" & vbCrLf
sConsulta = sConsulta & "IF @ESCALADOS>0" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + ',1 ESCALADOS,PG.ESCALAUNI '" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + ',0 ESCALADOS '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--------FROM--------------------------------------------" & vbCrLf
sConsulta = sConsulta & "--Parte de los art centrales, meto en una temporal para agilizar" & vbCrLf
sConsulta = sConsulta & "SELECT I.ANYO,I.GMN1_PROCE,I.PROCE,I.ID ,ISNULL(A2.COD,I.ART) ART,I.EST,I.GRUPO,ISNULL(A2.DEN,I.DESCR) DESCR,I.UNI,I.FECINI,I.FECFIN INTO #ITEMS_T from ITEM I WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                           LEFT JOIN ART4 A2 WITH(NOLOCK) ON A2.ART=I.ART WHERE  I.ANYO=@ANYO_L AND I.GMN1_PROCE=@GMN1_L AND I.PROCE=@PROCE_L AND I.GRUPO=@IDGRUPO" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' from #ITEMS_T I '" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' INNER JOIN  PROCE P WITH (NOLOCK) ON I.ANYO=P.ANYO AND I.GMN1_PROCE=P.GMN1 AND I.PROCE=P.COD '" & vbCrLf
sConsulta = sConsulta & "IF @EST=11 --Si el proceso esta parcialmente cerrado" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + ' AND I.EST=1 '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ESCALADOS>0 --Recoge los datos de adjudicaciones de las tablas de escalados" & vbCrLf
sConsulta = sConsulta & "   SET @SQL =@SQL + 'INNER JOIN ITEM_ADJESC IA WITH (NOLOCK) ON IA.ANYO=I.ANYO AND IA.GMN1=I.GMN1_PROCE AND IA.PROCE=I.PROCE AND IA.ITEM=I.ID AND IA.PROVE=@PROVE_L" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PROCE_GR_ESC PGE WITH (NOLOCK) ON PGE.ANYO=I.ANYO AND PGE.GMN1=I.GMN1_PROCE AND PGE.PROCE=I.PROCE AND PGE.GRUPO =PG.ID AND PGE.ID=IA.ESC '" & vbCrLf
sConsulta = sConsulta & "ELSE -- Recoge la adjudicaci?n" & vbCrLf
sConsulta = sConsulta & "   SET @SQL =@SQL + 'INNER JOIN ITEM_ADJ IA WITH (NOLOCK) ON I.ANYO=IA.ANYO AND I.GMN1_PROCE=IA.GMN1 AND I.PROCE=IA.PROCE AND I.ID=IA.ITEM AND IA.PROVE=@PROVE_L '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "---Obtiene las empresas a las que se ha distribuido la compra" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL + 'INNER JOIN" & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   SELECT DISTINCT IT_EMP_AUX.ANYO,IT_EMP_AUX.GMN1,IT_EMP_AUX.PROCE,ITEM,UON1,UON2,UON3, IT_EMP_AUX.EMPRESA, IT_EMP_AUX.PORCEN, IT_EMP_AUX.CENTRO , LIA.ID IT_EMP_LIAID" & vbCrLf
sConsulta = sConsulta & "   FROM (" & vbCrLf
sConsulta = sConsulta & "       SELECT DISTINCT IU.ANYO,IU.GMN1,IU.PROCE,iu.ITEM,IU.UON1, IU.UON2, IU.UON3,ISNULL(u3.empresa,ISNULL(u2.empresa,u1.empresa)) as empresa,iu.porcen,ISNULL(U3.CENTROS,ISNULL(U2.CENTROS,U1.CENTROS)) CENTRO" & vbCrLf
sConsulta = sConsulta & "       FROM VW_DIST_ITEM IU WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN ART4_UON AU WITH(NOLOCK) ON IU.UON1=AU.UON1 AND COALESCE(AU.UON2,IU.UON2,'''')=COALESCE(IU.UON2,AU.UON2,'''') AND COALESCE(AU.UON3,IU.UON3,'''')=COALESCE(IU.UON3,AU.UON3,'''')" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN ITEM I WITH(NOLOCK) ON I.ANYO=IU.ANYO AND I.GMN1_PROCE=IU.GMN1 AND I.PROCE=IU.PROCE AND I.ID=IU.ITEM AND I.ART=AU.ART4" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN UON1 U1 WITH(NOLOCK) ON AU.UON1=U1.cod" & vbCrLf
sConsulta = sConsulta & "       left JOIN UON2 U2 WITH(NOLOCK) ON AU.UON1=U2.UON1 AND COALESCE(U2.COD,AU.UON2,'''')=COALESCE(AU.UON2,U2.COD,'''')" & vbCrLf
sConsulta = sConsulta & "       left JOIN UON3 U3 WITH(NOLOCK) ON AU.UON1=U3.UON1 AND COALESCE(U3.UON2,AU.UON2,'''')=COALESCE(AU.UON2,U3.UON2,'''') AND COALESCE(U3.COD,AU.UON3,'''')=COALESCE(AU.UON3,U3.COD,'''')" & vbCrLf
sConsulta = sConsulta & "       where IU.ANYO=@ANYO_L AND IU.GMN1=@GMN1_L AND IU.PROCE=@PROCE_L" & vbCrLf
sConsulta = sConsulta & "       ) IT_EMP_AUX" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN LOG_ITEM_ADJ LIA WITH (NOLOCK) ON (LIA.ORIGEN=0 OR LIA.ORIGEN=2) AND LIA.ULTIMO=1" & vbCrLf
sConsulta = sConsulta & "   AND LIA.ANYO=IT_EMP_AUX.ANYO AND LIA.GMN1_PROCE=IT_EMP_AUX.GMN1 AND LIA.PROCE=IT_EMP_AUX.PROCE AND LIA.ID_ITEM=IT_EMP_AUX.ITEM AND LIA.PROVEPADRE=@PROVE_L" & vbCrLf
sConsulta = sConsulta & "   WHERE" & vbCrLf
sConsulta = sConsulta & "   CASE WHEN LIA.ID IS NULL THEN ISNULL(IT_EMP_AUX.CENTRO,'''') ELSE case when LIA.CENTRO is null or lia.centro='''' then ISNULL(IT_EMP_AUX.CENTRO,'''') else  LIA.CENTRO end  END = ISNULL(IT_EMP_AUX.CENTRO,'''')" & vbCrLf
sConsulta = sConsulta & "   ) IT_EMP ON IT_EMP.ITEM=IA.ITEM AND IT_EMP.EMPRESA=@IDEMP_L" & vbCrLf
sConsulta = sConsulta & "   '" & vbCrLf
sConsulta = sConsulta & "--Si los art?culos est?n vinculados a las unidades organizativas, solamente aparecen las empresas a las que pertenece el item (unidades organizativas comunes entre el art?culo y la distribuci?n del item)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' INNER JOIN ART4_UON ARTUON WITH(NOLOCK) ON ARTUON.UON1=IT_EMP.UON1" & vbCrLf
sConsulta = sConsulta & "AND COALESCE(ARTUON.UON2,IT_EMP.UON2,'''')=COALESCE(IT_EMP.UON2,ARTUON.UON2,'''')" & vbCrLf
sConsulta = sConsulta & "AND COALESCE(ARTUON.UON3,IT_EMP.UON3,'''')=COALESCE(IT_EMP.UON3,ARTUON.UON3,'''')" & vbCrLf
sConsulta = sConsulta & "AND ARTUON.ART4=I.ART '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @USAR_ORGCOMPRAS=1" & vbCrLf
sConsulta = sConsulta & "begin" & vbCrLf
sConsulta = sConsulta & "   SET @SQL =@SQL + 'LEFT JOIN(" & vbCrLf
sConsulta = sConsulta & "   select distinct au.art4,au.uon1,aU.uon2,aU.uon3,ISNULL(u3.empresa,ISNULL(u2.empresa,u1.empresa)) as empresa,ISNULL(u3.CENTROS,ISNULL(u2.CENTROS,u1.CENTROS)) CENTRO" & vbCrLf
sConsulta = sConsulta & "   from art4_uon au WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   inner join uon1 u1 WITH(NOLOCK) on u1.cod=AU.uon1" & vbCrLf
sConsulta = sConsulta & "   LEFT join uon2 u2 WITH(NOLOCK) on AU.uon1=u2.uon1 and AU.uon2=u2.cod" & vbCrLf
sConsulta = sConsulta & "   LEFT join uon3 u3 WITH(NOLOCK) on au.uon1=u3.uon1 and au.uon2=u3.uon2 and au.uon3=u3.cod'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @SQL =@SQL + ') IT_CEN ON I.ART=IT_CEN.ART4 AND IT_CEN.EMPRESA=@IDEMP_L" & vbCrLf
sConsulta = sConsulta & "   AND IT_CEN.UON1=IT_EMP.UON1 AND COALESCE(IT_CEN.UON2,IT_EMP.uon2,artuon.UON2)=COALESCE(IT_EMP.uon2,artuon.UON2,IT_CEN.UON2)" & vbCrLf
sConsulta = sConsulta & "   AND COALESCE(IT_CEN.UON3,IT_EMP.uon3,artuon.UON3)=COALESCE(IT_EMP.uon3,artuon.UON3,IT_CEN.UON3)'" & vbCrLf
sConsulta = sConsulta & "end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @TRASPASO_PLANIF>0" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL+ ' INNER JOIN ART4_UON_ATRIB AUA WITH (NOLOCK) ON ARTUON.ART4=AUA.ART" & vbCrLf
sConsulta = sConsulta & "   AND AUA.UON1=ARTUON.UON1 AND COALESCE(AUA.UON2,ARTUON.UON2,'''')=COALESCE(ARTUON.UON2,AUA.UON2,'''')" & vbCrLf
sConsulta = sConsulta & "   AND COALESCE(AUA.UON3,ARTUON.UON3,'''')=COALESCE(ARTUON.UON3,AUA.UON3,'''')" & vbCrLf
sConsulta = sConsulta & "   AND AUA.ATRIB=@TRASPASO_PLANIF AND AUA.VALOR_BOOL=1  '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ESCALADOS>0 --Recoge las ofertas de la tabla de escalados" & vbCrLf
sConsulta = sConsulta & "   SET @SQL =@SQL + 'INNER JOIN ITEM_OFEESC IOF WITH (NOLOCK) ON IA.ANYO =IOF.ANYO AND IA.GMN1 =IOF.GMN1 AND IA.PROCE =IOF.PROCE AND IA.ITEM =IOF.ITEM AND IA.PROVE=IOF.PROVE AND IA.OFE =IOF.OFE AND IOF.ESC=PGE.ID '" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + 'INNER JOIN ITEM_OFE IOF WITH (NOLOCK) ON IA.ANYO =IOF.ANYO AND IA.GMN1 =IOF.GMN1 AND IA.PROCE =IOF.PROCE AND IA.ITEM =IOF.ITEM AND IA.PROVE=IOF.PROVE AND IA.OFE =IOF.NUM '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "---Datos de integraci?n:" & vbCrLf
sConsulta = sConsulta & "IF @DISTR=1  --Con distribuidores" & vbCrLf
sConsulta = sConsulta & "   SET @SQL= @SQL + ' LEFT JOIN LOG_ITEM_ADJ LIA2 WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   ON I.ANYO=LIA2.ANYO AND I.GMN1_PROCE=LIA2.GMN1_PROCE AND I.PROCE =LIA2.PROCE AND LIA2.PROVEPADRE=@PROVE_L AND LIA2.PROVE<>@PROVE_L  AND I.ID=LIA2.ID_ITEM AND (LIA2.ORIGEN=0 OR LIA2.ORIGEN=2) AND LIA2.ULTIMO=1'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL= @SQL + '" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN LOG_ITEM_ADJ LIA1 WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "ON I.ANYO=LIA1.ANYO AND I.GMN1_PROCE=LIA1.GMN1_PROCE AND I.PROCE =LIA1.PROCE AND LIA1.PROVEPADRE=@PROVE_L AND LIA1.PROVE=@PROVE_L AND I.ID=LIA1.ID_ITEM AND (LIA1.ORIGEN=0 OR LIA1.ORIGEN=2) AND LIA1.ULTIMO=1'" & vbCrLf
sConsulta = sConsulta & "IF @DISTR=1  --Con distribuidores" & vbCrLf
sConsulta = sConsulta & "   SET @SQL= @SQL + ' LEFT JOIN LOG_GRAL LG WITH (NOLOCK) ON LG.ID_TABLA=COALESCE(LIA2.ID,LIA1.ID) AND LG.TABLA=9" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN PROVE_ERP PE WITH (NOLOCK) ON COALESCE(LIA2.PROVE_ERP,LIA1.PROVE_ERP)=PE.COD_ERP AND PE.EMPRESA=@IDEMP_L'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   SET @SQL= @SQL + ' LEFT JOIN LOG_GRAL LG WITH (NOLOCK) ON LG.ID_TABLA=LIA1.ID AND LG.TABLA=9" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN PROVE_ERP PE WITH (NOLOCK) ON LIA1.PROVE_ERP=PE.COD_ERP AND PE.EMPRESA=@IDEMP_L'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL + ' LEFT JOIN INT_ATRIB IAT WITH(NOLOCK) ON IAT.ADJUDICACION=1 AND IAT.AMBITO=3 AND IAT.ERP=@ERP" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN DEF_ATRIB DA WITH (NOLOCK) ON DA.ID=IAT.ATRIB  AND IAT.ADJUDICACION=1 AND IAT.AMBITO=3" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN ITEM_ATRIBESP IAE WITH (NOLOCK) ON IAE.ANYO=@ANYO_L AND IAE.GMN1=@GMN1_L AND IAE.PROCE=@PROCE_L AND IAE.ATRIB=IAT.ATRIB AND IAE.ITEM=I.ID" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PROCE_GRUPO_ATRIBESP PGA WITH (NOLOCK) ON PGA.ANYO=@ANYO_L AND PGA.GMN1=@GMN1_L AND PGA.PROCE=@PROCE_L AND PGA.ATRIB=IAT.ATRIB AND PGA.GRUPO=@IDGRUPO" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PROCE_ATRIBESP PATR WITH(NOLOCK) ON PATR.ANYO=@ANYO_L AND PATR.GMN1=@GMN1_L AND PATR.PROCE=@PROCE_L AND PATR.ATRIB=IAT.ATRIB" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN (SELECT TOP 1 OFE,ANYO,GMN1,PROCE,PROVE FROM ITEM_ADJ WITH (NOLOCK) WHERE ANYO=@ANYO_L AND GMN1=@GMN1_L AND PROCE=@PROCE_L AND PROVE=@PROVE_L) IAD ON IAD.ANYO=@ANYO_L  AND IAD.GMN1=@GMN1_L AND IAD.PROCE=@PROCE_L AND IAD.PROVE=@PROVE_L" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PROCE_ATRIB PA WITH (NOLOCK) ON PA.ANYO=@ANYO_L AND PA.GMN1=@GMN1_L AND PA.PROCE=@PROCE_L AND PA.AMBITO=3 AND DA.ID=PA.ATRIB AND (PA.GRUPO=@IDGRUPO OR PA.GRUPO IS NULL)" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN OFE_ITEM_ATRIB OA WITH (NOLOCK) ON OA.ANYO=@ANYO_L AND OA.GMN1=@GMN1_L AND OA.PROCE=@PROCE_L AND OA.ATRIB_ID =PA.ID AND OA.PROVE=@PROVE_L AND OA.OFE=IAD.OFE AND OA.ITEM=I.ID" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN OFE_GR_ATRIB OGATR WITH(NOLOCK) ON OGATR.ANYO=@ANYO_L AND OGATR.GMN1=@GMN1_L AND OGATR.PROCE=@PROCE_L AND OGATR.ATRIB_ID =PA.ID AND OGATR.PROVE=@PROVE_L AND OGATR.OFE=IAD.OFE AND OGATR.GRUPO=I.GRUPO" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN OFE_ATRIB OATR WITH(NOLOCK) ON OATR.ANYO=@ANYO_L AND OATR.GMN1=@GMN1_L AND OATR.PROCE=@PROCE_L AND OATR.ATRIB_ID =PA.ID AND OATR.PROVE=@PROVE_L AND OATR.OFE=IAD.OFE" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN" & vbCrLf
sConsulta = sConsulta & "   (SELECT DA.ID INT_ATRIB_ID,LIAA.ID_LOG_ITEM_ADJ , DA.ID, DA.COD,DA.DEN," & vbCrLf
sConsulta = sConsulta & "   DA.TIPO,IA.INTRO,IA.OBLIGATORIO,LIAA.VALOR_TEXT ,LIAA.VALOR_NUM,LIAA.VALOR_FEC,LIAA.VALOR_BOOL" & vbCrLf
sConsulta = sConsulta & "   FROM INT_ATRIB IA WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN DEF_ATRIB DA WITH (NOLOCK) ON DA.ID=IA.ATRIB AND IA.ADJUDICACION=1 AND IA.AMBITO=3" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN LOG_ITEM_ADJ_ATRIB LIAA WITH (NOLOCK) ON LIAA.ATRIB=IA.ATRIB where IA.ERP=@ERP" & vbCrLf
sConsulta = sConsulta & "   ) IT_ATRIB1 ON IT_ATRIB1.INT_ATRIB_ID=IAT.ATRIB AND'" & vbCrLf
sConsulta = sConsulta & "IF @DISTR=1" & vbCrLf
sConsulta = sConsulta & "   SET @SQL =@SQL + ' COALESCE(LIA2.ID,LIA1.ID)=IT_ATRIB1.ID_LOG_ITEM_ADJ WHERE P.ANYO=@ANYO_L AND P.GMN1=@GMN1_L AND P.COD=@PROCE_L" & vbCrLf
sConsulta = sConsulta & "   AND CASE WHEN COALESCE(LIA2.ID,LIA1.ID) IS NULL THEN 1 ELSE COALESCE(LIA2.ID,LIA1.ID) END = ISNULL(IT_EMP.IT_EMP_LIAID,1)" & vbCrLf
sConsulta = sConsulta & "   GROUP BY LIA1.ID,LIA2.ID'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   SET @SQL =@SQL + ' LIA1.ID=IT_ATRIB1.ID_LOG_ITEM_ADJ WHERE P.ANYO=@ANYO_L AND P.GMN1=@GMN1_L AND P.COD=@PROCE_L" & vbCrLf
sConsulta = sConsulta & "   AND CASE WHEN LIA1.ID IS NULL THEN 1 ELSE LIA1.ID END = ISNULL(IT_EMP.IT_EMP_LIAID,1)" & vbCrLf
sConsulta = sConsulta & "   GROUP BY LIA1.ID'" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' ,I.ID,I.ART ,I.DESCR ,I.UNI,'" & vbCrLf
sConsulta = sConsulta & "if  @ESCALADOS>0" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + ' PGE.ID ,PGE.INICIO,PGE.FIN,'" & vbCrLf
sConsulta = sConsulta & "else" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + 'IA.CANT_ADJ,IT_EMP.PORCEN,'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' IOF.PREC_VALIDO ,P.MON, LG.ESTADO , LG.FECENV, '" & vbCrLf
sConsulta = sConsulta & "IF @DISTR=1" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + ' LIA1.USU, COALESCE(LIA2.USU,LIA1.USU) ,COALESCE(LIA2.ACCION,LIA1.ACCION),COALESCE(LIA2.CENTRO,LIA1.CENTRO)'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + ' LIA1.USU,LIA1.ACCION,LIA1.CENTRO'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' , I.FECINI ,I.FECFIN ,PE.COD_ERP,PE.DEN_ERP," & vbCrLf
sConsulta = sConsulta & "IT_ATRIB1.INT_ATRIB_ID,IAE.ATRIB,PGA.ATRIB,PATR.ATRIB,PA.ATRIB,DA.ID," & vbCrLf
sConsulta = sConsulta & "DA.COD ,DA.DEN ," & vbCrLf
sConsulta = sConsulta & "COALESCE(IT_ATRIB1.VALOR_TEXT,IAE.VALOR_TEXT,PGA.VALOR_TEXT,PATR.VALOR_TEXT,OA.VALOR_TEXT,OGATR.VALOR_TEXT,OATR.VALOR_TEXT,IAT.VALOR_TEXT)  ," & vbCrLf
sConsulta = sConsulta & "COALESCE(IT_ATRIB1.VALOR_NUM ,IAE.VALOR_NUM,PGA.VALOR_NUM,PATR.VALOR_NUM,OA.VALOR_NUM,OGATR.VALOR_NUM,OATR.VALOR_NUM,IAT.VALOR_NUM) ," & vbCrLf
sConsulta = sConsulta & "COALESCE(IT_ATRIB1.VALOR_FEC ,IAE.VALOR_FEC,PGA.VALOR_FEC,PATR.VALOR_FEC,OA.VALOR_FEC,OGATR.VALOR_FEC,OATR.VALOR_FEC,IAT.VALOR_FEC) ," & vbCrLf
sConsulta = sConsulta & "COALESCE(IT_ATRIB1.VALOR_BOOL,IAE.VALOR_BOOL,PGA.VALOR_BOOL,PATR.VALOR_BOOL,OA.VALOR_BOOL,OGATR.VALOR_BOOL,OATR.VALOR_BOOL,IAT.VALOR_BOOL) ," & vbCrLf
sConsulta = sConsulta & "DA.TIPO ,IAT.OBLIGATORIO'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @USAR_ORGCOMPRAS=1" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + ',IT_EMP.UON1 ,IT_EMP.UON2 ,IT_EMP.UON3  '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ESCALADOS>0" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + ',PG.ESCALAUNI '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' ) A '" & vbCrLf
sConsulta = sConsulta & "IF @USAR_ORGCOMPRAS=1" & vbCrLf
sConsulta = sConsulta & "begin" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + 'LEFT JOIN(" & vbCrLf
sConsulta = sConsulta & "   select distinct au.art4,au.uon1,aU.uon2,aU.uon3,ISNULL(u3.empresa,ISNULL(u2.empresa,u1.empresa)) as empresa,ISNULL(u3.CENTROS,ISNULL(u2.CENTROS,u1.CENTROS)) CENTRO" & vbCrLf
sConsulta = sConsulta & "   from art4_uon au WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   inner join uon3 u3 WITH(NOLOCK) on au.uon1=u3.uon1 and au.uon2=u3.uon2 and au.uon3=u3.cod" & vbCrLf
sConsulta = sConsulta & "   inner join uon2 u2 WITH(NOLOCK) on u3.uon1=u2.uon1 and u3.uon2=u2.cod" & vbCrLf
sConsulta = sConsulta & "   inner join uon1 u1 WITH(NOLOCK) on u1.cod=u3.uon1'" & vbCrLf
sConsulta = sConsulta & "   IF @TRASPASO_PLANIF>0" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+ ' INNER JOIN ART4_UON_ATRIB AUA with (nolock) ON au.ART4=AUA.ART" & vbCrLf
sConsulta = sConsulta & "       AND AUA.UON1=au.UON1 AND ISNULL(AUA.UON2,'''')=ISNULL(au.UON2,'''')" & vbCrLf
sConsulta = sConsulta & "       AND ISNULL(AUA.UON3, '''')=ISNULL(au.UON3,'''')" & vbCrLf
sConsulta = sConsulta & "       AND AUA.ATRIB=@TRASPASO_PLANIF AND AUA.VALOR_BOOL=1  '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL =@SQL + ') IT_CEN ON A.CODART=IT_CEN.ART4 AND IT_CEN.EMPRESA=@IDEMP_L AND IT_CEN.UON1=A.UON1 AND IT_CEN.UON2=ISNULL(A.uon2,IT_CEN.UON2) AND ISNULL(A.uon3,IT_CEN.UON3) =IT_CEN.UON3 '" & vbCrLf
sConsulta = sConsulta & "end" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' ORDER BY A.ID_ITEM'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @PARAM =N'@ANYO_L INT, @GMN1_L VARCHAR(50), @PROCE_L VARCHAR(50), @CODGRUPO_L VARCHAR(20),@PROVE_L VARCHAR(50),@IDEMP_L INT,@ERP INT,@IDGRUPO INT,@TRASPASO_PLANIF INT'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL ,@PARAM,@ANYO_L=@ANYO_L,@GMN1_L=@GMN1_L,@PROCE_L=@PROCE_L,@CODGRUPO_L=@CODGRUPO_L,@PROVE_L=@PROVE_L,@IDEMP_L=@IDEMP_L,@ERP=@ERP ,@IDGRUPO=@IDGRUPO,@TRASPASO_PLANIF=@TRASPASO_PLANIF" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSEP_VALIDAR_LIMITES]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSEP_VALIDAR_LIMITES]" & vbCrLf
    gRDOCon.QueryTimeout = 7200
    ExecuteSQL gRDOCon, sConsulta
sConsulta = "CREATE PROCEDURE [dbo].[FSEP_VALIDAR_LIMITES](" & vbCrLf
sConsulta = sConsulta & "   @PEDIDO AS INTEGER,@PER AS VARCHAR(50)=''," & vbCrLf
sConsulta = sConsulta & "   @ID_TIPOPEDIDO AS INT," & vbCrLf
sConsulta = sConsulta & "   @ID_EMP AS INT, " & vbCrLf
sConsulta = sConsulta & "   @ESTADO_ORDEN INT OUTPUT)  " & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ORDEN AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @SEGURIDAD_CATN AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @SEGURIDAD_NIVEL AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @IMPORTE AS FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @NIVEL AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @SUMA AS FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @TABLA_IMPORTES_SEGURIDADES TABLE (CAT INT,NIVEL INT,IMPORTE FLOAT)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "/*Se recorre las ?rdenes de entrega*/" & vbCrLf
sConsulta = sConsulta & "DECLARE C1 CURSOR FOR SELECT ID FROM ORDEN_ENTREGA WHERE PEDIDO=@PEDIDO" & vbCrLf
sConsulta = sConsulta & "OPEN C1" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C1 INTO @ORDEN" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "/*Cursor para las lineas de pedido de categorias  en el stored FSEP_VALIDAR_ADJUDICACION */" & vbCrLf
sConsulta = sConsulta & "DECLARE C2 CURSOR FOR  SELECT DISTINCT CL.SEGURIDAD_CATN,CL.SEGURIDAD_NIVEL FROM LINEAS_PEDIDO LP WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN CATALOG_LIN CL WITH (NOLOCK) ON CL.ID=LP.LINCAT" & vbCrLf
sConsulta = sConsulta & "WHERE LP.PEDIDO=@PEDIDO AND LP.ORDEN=@ORDEN AND LP.EST=22" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "OPEN C2" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C2 INTO @SEGURIDAD_CATN,@SEGURIDAD_NIVEL" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @PER='' --Si @PER viene en blanco es que viene de la pagina de aprobacion y tendremos que buscar el aprovisionador" & vbCrLf
sConsulta = sConsulta & "SELECT @PER=PEDIDO.PER" & vbCrLf
sConsulta = sConsulta & "FROM ORDEN_ENTREGA WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PEDIDO WITH (NOLOCK) ON PEDIDO.ID=ORDEN_ENTREGA.PEDIDO" & vbCrLf
sConsulta = sConsulta & "WHERE ORDEN_ENTREGA.ID=@ORDEN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "/* Sacamos el importe l?mite del aprovisionador*/" & vbCrLf
sConsulta = sConsulta & "SELECT @IMPORTE=IMPORTE  FROM (SELECT MAX(IMPORTE) IMPORTE FROM VIEW_APROVISONADORES WHERE PER=@PER AND CATN=@SEGURIDAD_CATN AND NIVEL=@SEGURIDAD_NIVEL ) APROVISIONADORES" & vbCrLf
sConsulta = sConsulta & "/* Calculamos la suma de los importes de las l?neas de esa seguridad para ver si se pasa */" & vbCrLf
sConsulta = sConsulta & "SELECT @SUMA=(CASE WHEN LP.TIPORECEPCION=0 THEN SUM(LP.PREC_UP * LP.CANT_PED) ELSE SUM(LP.IMPORTE_PED) END" & vbCrLf
sConsulta = sConsulta & "               - SUM(LP.DESCUENTOS) " & vbCrLf
sConsulta = sConsulta & "               - ISNULL(OECD.IMPORTE_D_C,0)) * (1+(ISNULL(LPI.PORCENTAJE_IMPUESTOS,0)/100))" & vbCrLf
sConsulta = sConsulta & "           + SUM(ISNULL(LPCD.IMPORTE_C,0)) " & vbCrLf
sConsulta = sConsulta & "           + ISNULL(OECD.IMPORTE_C_C,0)" & vbCrLf
sConsulta = sConsulta & "FROM LINEAS_PEDIDO LP WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN CATALOG_LIN CL WITH (NOLOCK) ON CL.ID=LP.LINCAT" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN (SELECT LPCD.LINEA, SUM(CASE WHEN LPCD.TIPO=0 THEN IMPORTE * (1 + (ISNULL(LPCDI.VALOR,0)/100)) END) IMPORTE_C, SUM(CASE WHEN LPCD.TIPO=1 THEN IMPORTE END) IMPORTE_D" & vbCrLf
sConsulta = sConsulta & "FROM  LINEAS_PEDIDO_CD LPCD WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN (SELECT LINEA,COSTE,SUM(VALOR) VALOR FROM LINEAS_PEDIDO_CD_IMPUESTO WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "GROUP BY LINEA,COSTE) LPCDI ON LPCD.ID=LPCDI.COSTE AND LPCD.LINEA=LPCDI.LINEA" & vbCrLf
sConsulta = sConsulta & "GROUP BY LPCD.LINEA) LPCD" & vbCrLf
sConsulta = sConsulta & "ON LPCD.LINEA =LP.ID" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN (SELECT LINEA, SUM(VALOR) PORCENTAJE_IMPUESTOS" & vbCrLf
sConsulta = sConsulta & "FROM  LINEAS_PEDIDO_IMPUESTO LPI WITH (NOLOCK) GROUP BY LINEA) LPI" & vbCrLf
sConsulta = sConsulta & "ON LPI.LINEA =LP.ID" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN ORDEN_ENTREGA OE WITH (NOLOCK) ON LP.ORDEN=OE.ID" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN (SELECT  OECD.ORDEN ,SUM(CASE WHEN OECD.TIPO=0 THEN IMPORTE * (1 + (OECDI.VALOR/100)) END) IMPORTE_C_C, SUM(CASE WHEN OECD.TIPO=1 THEN IMPORTE END) IMPORTE_D_C" & vbCrLf
sConsulta = sConsulta & "FROM  ORDEN_ENTREGA_CD OECD WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN (SELECT ORDEN,COSTE,SUM(VALOR) VALOR FROM ORDEN_ENTREGA_CD_IMPUESTO WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "GROUP BY ORDEN,COSTE) OECDI ON OECD.ID=OECDI.COSTE AND OECD.ORDEN =OECDI.ORDEN" & vbCrLf
sConsulta = sConsulta & "GROUP BY OECD.ORDEN) OECD" & vbCrLf
sConsulta = sConsulta & "ON OE.ID=OECD.ORDEN" & vbCrLf
sConsulta = sConsulta & "WHERE LP.ORDEN=@ORDEN AND (LP.EST=22 OR LP.EST=23) AND CL.SEGURIDAD_CATN=@SEGURIDAD_CATN AND CL.SEGURIDAD_NIVEL=@SEGURIDAD_NIVEL" & vbCrLf
sConsulta = sConsulta & "GROUP BY OECD.IMPORTE_D_C,LPI.PORCENTAJE_IMPUESTOS,OECD.IMPORTE_C_C,LP.TIPORECEPCION" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO @TABLA_IMPORTES_SEGURIDADES VALUES(@SEGURIDAD_CATN,@SEGURIDAD_NIVEL,@SUMA)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @SUMA>@IMPORTE" & vbCrLf
sConsulta = sConsulta & "UPDATE LINEAS_PEDIDO SET NIVEL_APROB=1,EST=0,APROBADOR=NULL" & vbCrLf
sConsulta = sConsulta & "FROM LINEAS_PEDIDO" & vbCrLf
sConsulta = sConsulta & "INNER JOIN CATALOG_LIN ON CATALOG_LIN.ID=LINEAS_PEDIDO.LINCAT" & vbCrLf
sConsulta = sConsulta & "WHERE LINEAS_PEDIDO.ORDEN=@ORDEN AND LINEAS_PEDIDO.PEDIDO=@PEDIDO  AND CATALOG_LIN.SEGURIDAD_CATN=@SEGURIDAD_CATN" & vbCrLf
sConsulta = sConsulta & "AND CATALOG_LIN.SEGURIDAD_NIVEL =@SEGURIDAD_NIVEL AND LINEAS_PEDIDO.EST=22" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "UPDATE LINEAS_PEDIDO SET EST=1,APROBADOR=@PER,NIVEL_APROB=-1" & vbCrLf
sConsulta = sConsulta & "FROM LINEAS_PEDIDO WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN CATALOG_LIN WITH(NOLOCK) ON CATALOG_LIN.ID=LINEAS_PEDIDO.LINCAT" & vbCrLf
sConsulta = sConsulta & "WHERE LINEAS_PEDIDO.PEDIDO=@PEDIDO AND LINEAS_PEDIDO.ORDEN=@ORDEN" & vbCrLf
sConsulta = sConsulta & "AND CATALOG_LIN.SEGURIDAD_CATN=@SEGURIDAD_CATN" & vbCrLf
sConsulta = sConsulta & "AND CATALOG_LIN.SEGURIDAD_NIVEL =@SEGURIDAD_NIVEL" & vbCrLf
sConsulta = sConsulta & "AND LINEAS_PEDIDO.EST=22" & vbCrLf
sConsulta = sConsulta & "--Inserto en LINEAS_EST las lineas de pedido quedan pendientes de aprobar, las aprobadas se insertan en un trigger de LINEAS_PEDIDO" & vbCrLf
sConsulta = sConsulta & "INSERT INTO LINEAS_EST(PEDIDO,ORDEN,LINEA,EST,FECHA,PER) SELECT LINEAS_PEDIDO.PEDIDO,LINEAS_PEDIDO.ORDEN,LINEAS_PEDIDO.ID,LINEAS_PEDIDO.EST,GETDATE(),LINEAS_PEDIDO.APROBADOR" & vbCrLf
sConsulta = sConsulta & "FROM LINEAS_PEDIDO WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN CATALOG_LIN WITH(NOLOCK) ON CATALOG_LIN.ID=LINEAS_PEDIDO.LINCAT" & vbCrLf
sConsulta = sConsulta & "WHERE PEDIDO=@PEDIDO AND ORDEN=@ORDEN AND CATALOG_LIN.SEGURIDAD_CATN=@SEGURIDAD_CATN" & vbCrLf
sConsulta = sConsulta & "AND CATALOG_LIN.SEGURIDAD_NIVEL =@SEGURIDAD_NIVEL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C2 INTO @SEGURIDAD_CATN,@SEGURIDAD_NIVEL" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "CLOSE  C2" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C2" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C1 INTO @ORDEN" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "CLOSE  C1" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Una vez realizadas las validaciones de importe de aprovisionamiento, para aquellas lineas de pedido" & vbCrLf
sConsulta = sConsulta & "--que sobrepasen el importe de aprovisionamiento sacaremos la solicitud que les corresponda para iniciar el flujo," & vbCrLf
sConsulta = sConsulta & "--para ello obtendremos la seguridad de estas lineas(su categoria y nivel en la que esta configurada ls seguridad)" & vbCrLf
sConsulta = sConsulta & "--y comprobaremos junto con el tipo de pedido y la empresa, que solicitud le pertenece" & vbCrLf
sConsulta = sConsulta & "DECLARE @TABLA_SEGURIDADES TABLE (CAT INT,NIVEL INT,TIPO_PEDIDO INT,EMP INT)" & vbCrLf
sConsulta = sConsulta & "DECLARE @NUM_SEGURIDADES INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @NUM_SOLICITUDES INT" & vbCrLf
sConsulta = sConsulta & "INSERT INTO @TABLA_SEGURIDADES" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT CL.SEGURIDAD_CATN,CL.SEGURIDAD_NIVEL,@ID_TIPOPEDIDO,@ID_EMP" & vbCrLf
sConsulta = sConsulta & "FROM LINEAS_PEDIDO LP WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN CATALOG_LIN CL WITH(NOLOCK) ON LP.LINCAT=CL.ID" & vbCrLf
sConsulta = sConsulta & "WHERE LP.PEDIDO=@PEDIDO AND LP.NIVEL_APROB=1 AND LP.EST=0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @NUM_SEGURIDADES= @@ROWCOUNT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Devuelvo las solicitudes(y su importe) de las que se tendra que iniciar un flujo de aprobacion(llamada al web service)" & vbCrLf
sConsulta = sConsulta & "SELECT SOLICITUD,TIS.IMPORTE,TIS.CAT,TIS.NIVEL" & vbCrLf
sConsulta = sConsulta & "FROM CATN_TIPOPEDIDO_EMP_SOLICITUD CTES WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN @TABLA_SEGURIDADES TS ON CTES.CAT=TS.CAT AND CTES.NIVEL =TS.NIVEL AND CTES.TIPOPEDIDO=TS.TIPO_PEDIDO AND CTES.EMP=TS.EMP" & vbCrLf
sConsulta = sConsulta & "INNER JOIN @TABLA_IMPORTES_SEGURIDADES TIS ON TS.CAT=TIS.CAT AND TS.NIVEL=TIS.NIVEL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @NUM_SOLICITUDES= @@ROWCOUNT" & vbCrLf
sConsulta = sConsulta & "IF @NUM_SEGURIDADES > @NUM_SOLICITUDES" & vbCrLf
sConsulta = sConsulta & "--Pongo como emitidas al proveedor(EST=1) a aquellas lineas que se pasen de importe pero" & vbCrLf
sConsulta = sConsulta & "--que no tengan configurado una solicitud para realizar un flujo de aprobacion" & vbCrLf
sConsulta = sConsulta & "UPDATE LINEAS_PEDIDO SET NIVEL_APROB=-1,EST=1,APROBADOR=@PER" & vbCrLf
sConsulta = sConsulta & "FROM LINEAS_PEDIDO LP WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN CATALOG_LIN CL WITH(NOLOCK) ON CL.ID=LP.LINCAT" & vbCrLf
sConsulta = sConsulta & "INNER JOIN( SELECT TS.CAT,TS.NIVEL,TS.TIPO_PEDIDO,TS.EMP FROM @TABLA_SEGURIDADES TS" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN CATN_TIPOPEDIDO_EMP_SOLICITUD CTES WITH(NOLOCK) ON" & vbCrLf
sConsulta = sConsulta & "CTES.CAT=TS.CAT AND CTES.NIVEL =TS.NIVEL" & vbCrLf
sConsulta = sConsulta & "AND CTES.TIPOPEDIDO=@ID_TIPOPEDIDO  AND CTES.EMP=@ID_EMP" & vbCrLf
sConsulta = sConsulta & "WHERE CTES.CAT IS NULL) TS ON CL.SEGURIDAD_CATN=TS.CAT AND CL.SEGURIDAD_NIVEL=TS.NIVEL AND TS.EMP=@ID_EMP AND TS.TIPO_PEDIDO=@ID_TIPOPEDIDO" & vbCrLf
sConsulta = sConsulta & "WHERE LP.PEDIDO=@PEDIDO AND LP.NIVEL_APROB=1 AND LP.EST=0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF (SELECT COUNT(ID) FROM LINEAS_PEDIDO WITH(NOLOCK) WHERE ORDEN=@ORDEN AND PEDIDO=@PEDIDO AND EST=0)>0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO ORDEN_EST(PEDIDO,ORDEN,EST,FECHA,PER,COMENT,FECACT)" & vbCrLf
sConsulta = sConsulta & "       VALUES(@PEDIDO,@ORDEN,0,GETDATE(),@PER,NULL,GETDATE())" & vbCrLf
sConsulta = sConsulta & "       SET @ESTADO_ORDEN=0" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   IF(SELECT COUNT(ID) FROM LINEAS_PEDIDO WITH(NOLOCK) WHERE ORDEN=@ORDEN AND PEDIDO=@PEDIDO AND EST<>1)=0" & vbCrLf
sConsulta = sConsulta & "   --Todas las lineas son para emitir" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       UPDATE ORDEN_ENTREGA SET EST=2 WHERE ID=@ORDEN" & vbCrLf
sConsulta = sConsulta & "       SET @ESTADO_ORDEN=2" & vbCrLf
sConsulta = sConsulta & "       INSERT INTO ORDEN_EST(PEDIDO,ORDEN,EST,FECHA,PER,COMENT,FECACT)" & vbCrLf
sConsulta = sConsulta & "       VALUES(@PEDIDO,@ORDEN,2,GETDATE(),@PER,NULL,GETDATE())" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       UPDATE LINEAS_PEDIDO SET CESTA = NULL,LINCAT=NULL" & vbCrLf
sConsulta = sConsulta & "       FROM LINEAS_PEDIDO LP WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN ORDEN_ENTREGA OE WITH(NOLOCK) ON LP.ORDEN=OE.ID" & vbCrLf
sConsulta = sConsulta & "       WHERE LP.PEDIDO=@PEDIDO AND OE.EST=2" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       UPDATE CESTA SET TIPO=0 WHERE TIPO=99 AND PER=@PER" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       IF (SELECT COUNT(ID) FROM LINEAS_PEDIDO WITH(NOLOCK) WHERE ORDEN=@ORDEN AND PEDIDO=@PEDIDO AND EST=20)>0" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           UPDATE ORDEN_ENTREGA SET EST=1 WHERE ID=@ORDEN" & vbCrLf
sConsulta = sConsulta & "           INSERT INTO ORDEN_EST(PEDIDO,ORDEN,EST,FECHA,PER,COMENT,FECACT)" & vbCrLf
sConsulta = sConsulta & "           VALUES(@PEDIDO,@ORDEN,1,GETDATE(),@PER,NULL,GETDATE())" & vbCrLf
sConsulta = sConsulta & "           SET @ESTADO_ORDEN=1" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "   RETURN @ESTADO_ORDEN" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO ORDEN_COMUNIC (PEDIDO, ORDEN, APE, NOM, EMAILDIR, TIPO, FECHA, WEB, EMAIL, IMP)" & vbCrLf
sConsulta = sConsulta & "SELECT @PEDIDO,@ORDEN, C.APE, C.NOM, C.EMAIL ,0, getdate(), 1,0,0" & vbCrLf
sConsulta = sConsulta & "FROM CON C WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PROVE P WITH(NOLOCK) ON P.COD=C.PROVE" & vbCrLf
sConsulta = sConsulta & "INNER JOIN ORDEN_ENTREGA OE WITH(NOLOCK) ON OE.ID=@ORDEN AND OE.PROVE=P.COD" & vbCrLf
sConsulta = sConsulta & "WHERE C.APROV=1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Devuelvo las solicitudes(y su importe) de las que se tendra que iniciar un flujo de aprobacion(llamada al web service)" & vbCrLf
sConsulta = sConsulta & "SELECT SOLICITUD,TIS.IMPORTE,TIS.CAT,TIS.NIVEL" & vbCrLf
sConsulta = sConsulta & "FROM CATN_TIPOPEDIDO_EMP_SOLICITUD CTES WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN @TABLA_SEGURIDADES TS ON CTES.CAT=TS.CAT AND CTES.NIVEL =TS.NIVEL AND CTES.TIPOPEDIDO=TS.TIPO_PEDIDO AND CTES.EMP=TS.EMP" & vbCrLf
sConsulta = sConsulta & "INNER JOIN @TABLA_IMPORTES_SEGURIDADES TIS ON TS.CAT =TIS.CAT AND TS.NIVEL=TIS.NIVEL" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion31900_09_2146_A31900_09_2147() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim oADOConnection As ADODB.Connection
    Dim sConsulta As String

    On Error GoTo Error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Actualiza Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    frmProgreso.ProgressBar1.Refresh
    V_31900_9_Storeds_2147

    sConsulta = "UPDATE VERSION SET NUM ='3.00.21.047'" & ", FECHA=CONVERT(datetime,'" & Format(Now, "mm-dd-yyyy hh:mm:ss") & "',110), PRODVER='31900.9'"
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False

    CodigoDeActualizacion31900_09_2146_A31900_09_2147 = True
    Exit Function

Error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If

    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    CodigoDeActualizacion31900_09_2146_A31900_09_2147 = False
End Function

Private Sub V_31900_9_Storeds_2147()
    Dim sConsulta As String

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSWS_ARTICULOS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSWS_ARTICULOS]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSWS_ARTICULOS]" & vbCrLf
sConsulta = sConsulta & "@USU NVARCHAR(50)=NULL," & vbCrLf
sConsulta = sConsulta & "@RESTRICUONSOLUSUUON BIT=0," & vbCrLf
sConsulta = sConsulta & "@RESTRICUONSOLPERFUON BIT=0," & vbCrLf
sConsulta = sConsulta & "@COD VARCHAR(50)=NULL," & vbCrLf
sConsulta = sConsulta & "@DEN VARCHAR(2000)=NULL," & vbCrLf
sConsulta = sConsulta & "@GMN1 VARCHAR(50)=NULL," & vbCrLf
sConsulta = sConsulta & "@GMN2 VARCHAR(50)=NULL," & vbCrLf
sConsulta = sConsulta & "@GMN3 VARCHAR(50)=NULL," & vbCrLf
sConsulta = sConsulta & "@GMN4 VARCHAR(50)=NULL," & vbCrLf
sConsulta = sConsulta & "@COINCID TINYINT=1," & vbCrLf
sConsulta = sConsulta & "@PER VARCHAR(50)=NULL," & vbCrLf
sConsulta = sConsulta & "@FILAS INT=NULL," & vbCrLf
sConsulta = sConsulta & "@DESDEFILA INT=NULL," & vbCrLf
sConsulta = sConsulta & "@IDI VARCHAR(50)='SPA'," & vbCrLf
sConsulta = sConsulta & "@INSTANCIAMONEDA VARCHAR(3)='EUR'," & vbCrLf
sConsulta = sConsulta & "@CARGAR_ULT_ADJ TINYINT=0," & vbCrLf
sConsulta = sConsulta & "@GENERICOS TINYINT=2," & vbCrLf
sConsulta = sConsulta & "@CODORGCOMPRAS VARCHAR(4)=NULL," & vbCrLf
sConsulta = sConsulta & "@CODCENTRO VARCHAR(4)=NULL," & vbCrLf
sConsulta = sConsulta & "@CODINI VARCHAR(50)=NULL," & vbCrLf
sConsulta = sConsulta & "@CODULT VARCHAR(50)=NULL," & vbCrLf
sConsulta = sConsulta & "@CARGAR_CC TINYINT=0," & vbCrLf
sConsulta = sConsulta & "@CARGAR_ORG TINYINT=0," & vbCrLf
sConsulta = sConsulta & "@GASTO TINYINT=0," & vbCrLf
sConsulta = sConsulta & "@INVERSION TINYINT=0," & vbCrLf
sConsulta = sConsulta & "@GASTOINVERSION TINYINT=0," & vbCrLf
sConsulta = sConsulta & "@NOALMACENABLE TINYINT=0," & vbCrLf
sConsulta = sConsulta & "@ALMACENABLE TINYINT=0," & vbCrLf
sConsulta = sConsulta & "@ALMACENOPCIONAL TINYINT=0," & vbCrLf
sConsulta = sConsulta & "@RECEPCIONOBLIG TINYINT=0," & vbCrLf
sConsulta = sConsulta & "@NORECEPCIONABLE TINYINT=0," & vbCrLf
sConsulta = sConsulta & "@RECEPCIONOPCIONAL TINYINT=0," & vbCrLf
sConsulta = sConsulta & "@MATERIALESCATALOGADOS TINYINT=NULL," & vbCrLf
sConsulta = sConsulta & "@RESTRICMATUSU INT=0," & vbCrLf
sConsulta = sConsulta & "@FILTRARNEGOCIADOS INT=0," & vbCrLf
sConsulta = sConsulta & "@CODPROVEEDOR VARCHAR(50)=NULL," & vbCrLf
sConsulta = sConsulta & "@CONFPAGO AS TINYINT=0," & vbCrLf
sConsulta = sConsulta & "@USAR_ORG AS TINYINT=0," & vbCrLf
sConsulta = sConsulta & "@ACCESO_SM AS TINYINT=0," & vbCrLf
sConsulta = sConsulta & "@UON1 NVARCHAR(15)=NULL," & vbCrLf
sConsulta = sConsulta & "@UON2 NVARCHAR(15)=NULL," & vbCrLf
sConsulta = sConsulta & "@UON3 NVARCHAR(15)=NULL," & vbCrLf
sConsulta = sConsulta & "@UON4 NVARCHAR(15)=NULL," & vbCrLf
sConsulta = sConsulta & "@RESTRIC_PED_ART_UON_USU TINYINT=0," & vbCrLf
sConsulta = sConsulta & "@RESTRIC_PED_ART_UON_PERF TINYINT=0," & vbCrLf
sConsulta = sConsulta & "@RUON1 NVARCHAR(15)=NULL," & vbCrLf
sConsulta = sConsulta & "@RUON2 NVARCHAR(15)=NULL," & vbCrLf
sConsulta = sConsulta & "@RUON3 NVARCHAR(15)=NULL," & vbCrLf
sConsulta = sConsulta & "@ATRIB_UON TBATRIBUON READONLY" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @RESTRIC INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @MONCEN AS NVARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @EQUIV AS FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @EQUIVINS AS FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL2 AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL3 AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSELPAG AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLJOINPAG_P AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLJOINPAG_D AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PARAM AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PERF AS INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @RESTRIC = 0" & vbCrLf
sConsulta = sConsulta & "IF @PER IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SELECT @PERF=U.PERF,@RESTRIC=ISNULL(ISNULL(USU_ACC.ACC,PERF_ACC.ACC),0)" & vbCrLf
sConsulta = sConsulta & "FROM USU U WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN USU_ACC WITH(NOLOCK) ON USU_ACC.USU=U.COD AND USU_ACC.ACC=@RESTRICMATUSU" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PERF_ACC WITH(NOLOCK) ON PERF_ACC.PERF=U.PERF AND PERF_ACC.ACC=@RESTRICMATUSU" & vbCrLf
sConsulta = sConsulta & "WHERE U.PER=@PER AND U.BAJA=0" & vbCrLf
sConsulta = sConsulta & "IF @RESTRIC>0" & vbCrLf
sConsulta = sConsulta & "SET @RESTRIC=1" & vbCrLf
sConsulta = sConsulta & "if @DESDEFILA = 0" & vbCrLf
sConsulta = sConsulta & "SET @DESDEFILA = NULL" & vbCrLf
sConsulta = sConsulta & "if @FILAS=0" & vbCrLf
sConsulta = sConsulta & "SET @FILAS = 1000" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL2=''" & vbCrLf
sConsulta = sConsulta & "IF @RESTRICUONSOLUSUUON=1 OR @RESTRICUONSOLPERFUON=1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SQL2=N';WITH UONSRESTRICCION AS ('" & vbCrLf
sConsulta = sConsulta & "SET @SQL2=@SQL2+N'SELECT U.PERF,P.UON1,P.UON2,P.UON3 '" & vbCrLf
sConsulta = sConsulta & "SET @SQL2=@SQL2+N'FROM USU U WITH(NOLOCK) INNER JOIN PER P WITH(NOLOCK) ON P.COD=U.PER AND U.COD=@USU '" & vbCrLf
sConsulta = sConsulta & "IF @RESTRICUONSOLUSUUON=0" & vbCrLf
sConsulta = sConsulta & "SET @SQL2=@SQL2+N'AND U.COD IS NULL '" & vbCrLf
sConsulta = sConsulta & "SET @SQL2=@SQL2+N'UNION '" & vbCrLf
sConsulta = sConsulta & "SET @SQL2=@SQL2+N'SELECT U.PERF,PU.UON1,PU.UON2,PU.UON3 '" & vbCrLf
sConsulta = sConsulta & "SET @SQL2=@SQL2+N'FROM USU U WITH(NOLOCK) INNER JOIN PERF_UON PU WITH(NOLOCK) ON PU.PERF=U.PERF AND U.COD=@USU '" & vbCrLf
sConsulta = sConsulta & "IF @RESTRICUONSOLPERFUON=0" & vbCrLf
sConsulta = sConsulta & "SET @SQL2=@SQL2+N'AND U.COD IS NULL '" & vbCrLf
sConsulta = sConsulta & "SET @SQL2=@SQL2+N')'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL2=@SQL2+N'SELECT DISTINCT A.COD,A.DEN,A.GENERICO,A.CONCEPTO,A.ALMACENAR,A.RECEPCIONAR,A.GMN1,A.GMN2,A.GMN3,A.GMN4,A.UNI,G4.DEN_' + @IDI + ' DENGMN,UNI_DEN.DEN DENUNI,UNI.NUMDEC'" & vbCrLf
sConsulta = sConsulta & "SET @SQL2=@SQL2 + ',A.PRES5_NIV0, A.PRES5_NIV1,A.PRES5_NIV2,A.PRES5_NIV3, A.PRES5_NIV4,PERM_EP.PERMISO_EP,CATALOGADO.ESTA_CATALOGADO,'''' AS PREC_VIGENTE'" & vbCrLf
sConsulta = sConsulta & "SET @SQL2=@SQL2 + ',A.TIPORECEPCION'" & vbCrLf
sConsulta = sConsulta & "IF @RESTRIC_PED_ART_UON_USU =1 OR @RESTRIC_PED_ART_UON_PERF =1" & vbCrLf
sConsulta = sConsulta & "SET @SQL2=@SQL2 + ',PERMISO_EMISION.TIENE_PERMISO'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL2=@SQL2 + ',1 TIENE_PERMISO'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @MONCEN=(SELECT MONCEN FROM PARGEN_DEF WITH (NOLOCK))" & vbCrLf
sConsulta = sConsulta & "IF @CARGAR_ULT_ADJ=1 AND NOT(@FILTRARNEGOCIADOS=1 OR @FILTRARNEGOCIADOS=3)" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @EQUIV=(SELECT EQUIV FROM MON  WITH (NOLOCK)  WHERE COD=@MONCEN)" & vbCrLf
sConsulta = sConsulta & "SET @EQUIVINS=(SELECT EQUIV FROM MON WITH (NOLOCK) WHERE COD=@INSTANCIAMONEDA)" & vbCrLf
sConsulta = sConsulta & "SET @SQL2 = @SQL2 + ',(AA.PREC/@EQUIV*@EQUIVINS) PREC_ULT_ADJ,AA.CODPROVE CODPROV_ULT_ADJ,AA.DENPROVE DENPROV_ULT_ADJ'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF (@FILTRARNEGOCIADOS=1 OR @FILTRARNEGOCIADOS=3)" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "--Art4_adj. Guarda en moneda central. Pero lo q se ha de ver es precio en moneda proveedor" & vbCrLf
sConsulta = sConsulta & "SET @SQL2 = @SQL2 + ',NEGO.PREC*(NEGO.CAMBIO_OFE*NEGO.CAMBIO_PROCE) PREC_ULT_ADJ,NEGO.CODPROVE CODPROV_ULT_ADJ,NEGO.DENPROVE DENPROV_ULT_ADJ'" & vbCrLf
sConsulta = sConsulta & "SET @SQL2 = @SQL2 + ',NEGO.UNI AS UNI_ULT_ADJ, NEGO.DEN AS DENUNI_ULT_ADJ, NEGO.MON AS MON_ULT_ADJ, NEGO.DENMON AS DENMON_ULT_ADJ'" & vbCrLf
sConsulta = sConsulta & "--Junto con el proveedor viene la forma de pago. As? q con cada cambio de proveedor en pedidos negociados has de cambiar la forma pago , DE HABER este campo" & vbCrLf
sConsulta = sConsulta & "IF @CONFPAGO = 1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "--se cargar? con la forma de pago asociada al proveedor. Bien prove_erp o bien prove" & vbCrLf
sConsulta = sConsulta & "SET @SQL2 = @SQL2 + ',NEGO.PAG PAG_ULT_ADJ, NEGO.DENPAG DENPAG_ULT_ADJ'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLSELPAG = N''" & vbCrLf
sConsulta = sConsulta & "SET @SQLJOINPAG_P = N''" & vbCrLf
sConsulta = sConsulta & "SET @SQLJOINPAG_D = N''" & vbCrLf
sConsulta = sConsulta & "DECLARE @DESDEERP TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @EMPRESA INT" & vbCrLf
sConsulta = sConsulta & "SET @DESDEERP = 0" & vbCrLf
sConsulta = sConsulta & "SET @EMPRESA= NULL" & vbCrLf
sConsulta = sConsulta & "--Junto con el proveedor viene la forma de pago. As? q con cada cambio de proveedor en pedidos negociados has de cambiar la forma pago" & vbCrLf
sConsulta = sConsulta & "IF @CONFPAGO = 1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE @ERP INT" & vbCrLf
sConsulta = sConsulta & "--La carga de la forma de pago ser? similar a la que se realiza en la emisi?n de pedidos desde GS:" & vbCrLf
sConsulta = sConsulta & "--Cogido de FSWS_PROVES" & vbCrLf
sConsulta = sConsulta & "IF (@USAR_ORG=1 OR @ACCESO_SM=1)" & vbCrLf
sConsulta = sConsulta & "BEGIN --Si no usas org compras o partida presupuestaria, no hay de donde sacar empresa/erp" & vbCrLf
sConsulta = sConsulta & "IF (SELECT ISNULL(TRASPASO_PED_ERP,0) FROM PARGEN_INTERNO WITH (NOLOCK) WHERE ID=1)=1" & vbCrLf
sConsulta = sConsulta & "BEGIN   --Visto en GS q si NO se cumple esto no hay nada q hacer con ERP" & vbCrLf
sConsulta = sConsulta & "IF (SELECT COUNT(1) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA =13 AND ACTIVA=1 AND (SENTIDO=1 OR SENTIDO=3))>0" & vbCrLf
sConsulta = sConsulta & "BEGIN--Si la integraci?n de pedidos (en sentido FULLSTEP->ERP o FULLSTEP<->ERP) est? activada se cargar? por defecto la forma de pago" & vbCrLf
sConsulta = sConsulta & "--del proveedor en el ERP. Si este dato no est? indicado se tomar? la forma de pago del proveedor en FULLSTEP" & vbCrLf
sConsulta = sConsulta & "SET @ERP= NULL" & vbCrLf
sConsulta = sConsulta & "IF @ACCESO_SM=1 --si est? activado el SM se obtendr? la empresa de la partida presupuestaria seleccionada" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "--La partida es, por ejemplo:" & vbCrLf
sConsulta = sConsulta & "--  Uon1#Uon2..#Pres1|Pres2..." & vbCrLf
sConsulta = sConsulta & "--  Uon1#Pres1" & vbCrLf
sConsulta = sConsulta & "--Es decir dispongo en codigo del UON. As? pues se lo puedo pasar a este stored." & vbCrLf
sConsulta = sConsulta & "--Las empresas estan en UON" & vbCrLf
sConsulta = sConsulta & "SET @EMPRESA= 0" & vbCrLf
sConsulta = sConsulta & "SELECT @EMPRESA=ISNULL(U4.EMPRESA,0) FROM UON4 U4 WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE U4.UON1=@UON1 AND U4.UON2=@UON2 AND U4.UON3=@UON3 AND U4.COD=@UON4" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @EMPRESA=0" & vbCrLf
sConsulta = sConsulta & "SELECT @EMPRESA=ISNULL(U3.EMPRESA,0) FROM UON3 U3 WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE U3.UON1=@UON1 AND U3.UON2=@UON2 AND U3.COD=@UON3" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @EMPRESA=0" & vbCrLf
sConsulta = sConsulta & "SELECT @EMPRESA=ISNULL(U2.EMPRESA,0) FROM UON2 U2 WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE U2.UON1=@UON1 AND U2.COD=@UON2" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @EMPRESA=0" & vbCrLf
sConsulta = sConsulta & "SELECT @EMPRESA=ISNULL(U1.EMPRESA,0) FROM UON1 U1 WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE U1.COD=@UON1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @EMPRESA=0" & vbCrLf
sConsulta = sConsulta & "SET @EMPRESA= NULL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Muy bien, en frmpedidos de GS veo q la carga ERP se hace con la funci?n CERPInt.ObtenerCodERP" & vbCrLf
sConsulta = sConsulta & "--si le pasa una empresa, esta es la select" & vbCrLf
sConsulta = sConsulta & "SELECT @ERP=COD" & vbCrLf
sConsulta = sConsulta & "FROM ERP E WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN ERP_SOCIEDAD EO WITH (NOLOCK) ON EO.ERP=E.COD" & vbCrLf
sConsulta = sConsulta & "INNER JOIN EMP WITH(NOLOCK) ON EMP.SOCIEDAD=EO.SOCIEDAD" & vbCrLf
sConsulta = sConsulta & "WHERE EMP.ERP=1 AND EMP.ID =@EMPRESA" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @CODORGCOMPRAS IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "--Vamos a ver en GESTAMP, es una organizaci?n UNA empresa" & vbCrLf
sConsulta = sConsulta & "--Vamos a ver en FERROVIAL, es una organizaci?n VARIAS empresas" & vbCrLf
sConsulta = sConsulta & "--???Problema??? Pues comentado con Mertxe/Teresa/Encarni, lo q tiene sentido es lo de gestamp y Ferr lo hacen de" & vbCrLf
sConsulta = sConsulta & "--otra manera... nunca usa org. OK a top 1." & vbCrLf
sConsulta = sConsulta & "SELECT TOP 1 @EMPRESA=ID FROM EMP WITH(NOLOCK) WHERE ORG_COMPRAS=@CODORGCOMPRAS ORDER BY ID ASC" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Muy bien, en frmPedidos de GS veo q la carga ERP se hace con la funci?n CERPInt.ObtenerCodERP" & vbCrLf
sConsulta = sConsulta & "--si le pasa una org compra, esta es la select" & vbCrLf
sConsulta = sConsulta & "SELECT TOP 1 @ERP=COD FROM ERP E WITH (NOLOCK) INNER JOIN ERP_ORGCOMPRAS EO WITH (NOLOCK) ON EO.ERP=E.COD" & vbCrLf
sConsulta = sConsulta & "WHERE EO.ORGCOMPRAS =@CODORGCOMPRAS" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ERP IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN --integraci?n de pedidos del erp en concreto. Ver si esta activo o no y en sentido exportar o no." & vbCrLf
sConsulta = sConsulta & "IF (SELECT COUNT(1) FROM TABLAS_INTEGRACION_ERP TIE WITH (NOLOCK) WHERE TIE.ERP = @ERP AND TIE.ENTIDAD=13 AND ISNULL(TIE.ACTIVA,0)=1 AND ((ISNULL(TIE.SENTIDO,0)=1) OR (ISNULL(TIE.SENTIDO,0)=3)))>0" & vbCrLf
sConsulta = sConsulta & "BEGIN --integraci?n de pedidos del erp en concreto. Ver si esta activo o no y en sentido exportar o no. Lo esta." & vbCrLf
sConsulta = sConsulta & "SET @DESDEERP = 1" & vbCrLf
sConsulta = sConsulta & "SET @SQLJOINPAG_P = N' INNER JOIN PROVE WITH(NOLOCK) ON PROVE.COD=AA.CODPROVE '" & vbCrLf
sConsulta = sConsulta & "SET @SQLJOINPAG_P = @SQLJOINPAG_P + ' LEFT JOIN PROVE_ERP ERP WITH(NOLOCK) ON ERP.NIF=PROVE.NIF AND ERP.EMPRESA=@EMPRESA'" & vbCrLf
sConsulta = sConsulta & "SET @SQLJOINPAG_D = N' LEFT JOIN PAG_DEN PD WITH(NOLOCK) ON ISNULL(PROVE_ERP.PAG,PROVE.PAG)=PD.PAG AND PD.IDIOMA=@IDI'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN --integraci?n de pedidos del erp en concreto. Ver si esta activo o no y en sentido exportar o no. NO lo esta." & vbCrLf
sConsulta = sConsulta & "SET @SQLJOINPAG_P = N' INNER JOIN PROVE WITH(NOLOCK) ON PROVE.COD=AA.CODPROVE '" & vbCrLf
sConsulta = sConsulta & "SET @SQLJOINPAG_D = N' LEFT JOIN PAG_DEN PD WITH(NOLOCK) ON PROVE.PAG=PD.PAG AND PD.IDIOMA=@IDI'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN --integraci?n de pedidos del erp en concreto. Ver si esta activo o no y en sentido exportar o no. NO lo esta pq no se ha encontrado ERP." & vbCrLf
sConsulta = sConsulta & "SET @SQLJOINPAG_P = N' INNER JOIN PROVE WITH(NOLOCK) ON PROVE.COD=AA.CODPROVE '" & vbCrLf
sConsulta = sConsulta & "SET @SQLJOINPAG_D = N' LEFT JOIN PAG_DEN PD WITH(NOLOCK) ON PROVE.PAG=PD.PAG AND PD.IDIOMA=@IDI'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE --Si la integraci?n de pedidos (en sentido FULLSTEP->ERP o FULLSTEP<->ERP) no est? activada se cargar? por defecto la forma de pago del proveedor en FULLSTEP." & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SQLJOINPAG_P = N' INNER JOIN PROVE WITH(NOLOCK) ON PROVE.COD=AA.CODPROVE '" & vbCrLf
sConsulta = sConsulta & "SET @SQLJOINPAG_D = N' LEFT JOIN PAG_DEN PD WITH(NOLOCK) ON PROVE.PAG=PD.PAG AND PD.IDIOMA=@IDI'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE--Visto en GS q si NO se cumple esto no hay nada q hacer con ERP" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SQLJOINPAG_P = N' INNER JOIN PROVE WITH(NOLOCK) ON PROVE.COD=AA.CODPROVE '" & vbCrLf
sConsulta = sConsulta & "SET @SQLJOINPAG_D = N' LEFT JOIN PAG_DEN PD WITH(NOLOCK) ON PROVE.PAG=PD.PAG AND PD.IDIOMA=@IDI'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE --Si no usas org compras o partida presupuestaria, no hay de donde sacar empresa/erp" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SQLJOINPAG_P = N' INNER JOIN PROVE WITH(NOLOCK) ON PROVE.COD=AA.CODPROVE '" & vbCrLf
sConsulta = sConsulta & "SET @SQLJOINPAG_D = N' LEFT JOIN PAG_DEN PD WITH(NOLOCK) ON PROVE.PAG=PD.PAG AND PD.IDIOMA=@IDI'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--se cargar? con la forma de pago asociada al proveedor. Bien prove_erp o bien prove" & vbCrLf
sConsulta = sConsulta & "IF @DESDEERP=1 --prove_erp" & vbCrLf
sConsulta = sConsulta & "SET @SQLSELPAG = N', ISNULL(PROVE_ERP.PAG,PROVE.PAG) PAG, PD.DEN DENPAG'" & vbCrLf
sConsulta = sConsulta & "ELSE --prove" & vbCrLf
sConsulta = sConsulta & "SET @SQLSELPAG = N', PROVE.PAG PAG, PD.DEN DENPAG'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CODORGCOMPRAS IS NOT NULL OR @CARGAR_ORG=1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SQL2 =@SQL2 +  N',ISNULL(u3.orgcompras,isNull(u2.orgcompras,u1.orgcompras)) CODORGCOMPRAS,o.den DENORGCOMPRAS,isNull(u3.centros,isNull(u2.centros,u1.centros)) CODCENTRO,c.den DENCENTRO '" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CARGAR_CC = 1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SQL2 =@SQL2 +  N',ISNULL(A.PRES5_NIV4,isNull(A.PRES5_NIV3,isNull(A.PRES5_NIV2,isNull(A.PRES5_NIV1,A.PRES5_NIV0)))) CC '" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N' FROM ART4 A WITH (NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + N'INNER JOIN GMN4 G4 WITH (NOLOCK) ON A.GMN1= G4.GMN1 AND A.GMN2= G4.GMN2 AND A.GMN3= G4.GMN3 AND A.GMN4= G4.COD '" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + N'INNER JOIN UNI_DEN WITH (NOLOCK) ON UNI_DEN.UNI = A.UNI AND UNI_DEN.IDIOMA= @IDI '" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + N'INNER JOIN UNI WITH(NOLOCK) ON A.UNI = UNI.COD '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF (@RESTRICUONSOLUSUUON=1 OR @RESTRICUONSOLPERFUON=1) AND @RUON1 IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'INNER JOIN ART4_UON AUR WITH(NOLOCK) ON AUR.ART4=A.COD '" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'INNER JOIN UONSRESTRICCION UONS ON ((AUR.UON1=UONS.UON1 '" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'AND (UONS.UON2 IS NULL OR AUR.UON2=UONS.UON2) '" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'AND (UONS.UON3 IS NULL OR AUR.UON3=UONS.UON3)) OR AUR.UON1 IS NULL) '" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "IF @RUON1 IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'INNER JOIN ART4_UON AU2 WITH(NOLOCK) ON AU2.ART4=A.COD '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Pedidos negociados. La ultima adjudicaci?n solo para el proveedor seleccionado, no de todos como en el resto de casos" & vbCrLf
sConsulta = sConsulta & "IF @CARGAR_ULT_ADJ = 1 AND NOT(@FILTRARNEGOCIADOS=1 OR @FILTRARNEGOCIADOS=3)" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + '  LEFT JOIN ART4_ADJ AA WITH (NOLOCK) ON AA.ART = A.COD AND AA.ID = (SELECT MAX(ID) FROM ART4_ADJ WHERE ART4_ADJ.ART = A.COD) '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CODORGCOMPRAS IS NOT NULL OR @CARGAR_ORG=1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N' INNER JOIN  ART4_UON AU WITH (NOLOCK) ON A.COD = AU.ART4'" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL +' INNER JOIN UON1 u1 WITH (NOLOCK) ON u1.cod =au.uon1'" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL +' INNER JOIN UON2 u2 WITH (NOLOCK) ON u2.cod = au.uon2 and u2.uon1 = au.uon1'" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL +' INNER JOIN UON3 u3 WITH (NOLOCK) ON u3.cod = au.uon3 and u3.uon2 = au.uon2 and u3.uon1 = au.uon1'" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL +' INNER JOIN orgcompras o WITH (NOLOCK) on o.cod = isNull(u3.orgcompras,isNull(u2.orgcompras,u1.orgcompras))'" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL +' INNER JOIN Centros c  WITH (NOLOCK) on c.cod = isNull(u3.centros,isNull(u2.centros,u1.centros))'" & vbCrLf
sConsulta = sConsulta & "--Para filtrar por atributos de UON (OrgCompras)" & vbCrLf
sConsulta = sConsulta & "IF (@CODORGCOMPRAS IS NOT NULL) AND (EXISTS(SELECT 1 FROM @ATRIB_UON))" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   DECLARE @ATRIBOC INT" & vbCrLf
sConsulta = sConsulta & "   DECLARE @NUMOC INT" & vbCrLf
sConsulta = sConsulta & "   DECLARE @SNUMOC NVARCHAR(20)" & vbCrLf
sConsulta = sConsulta & "   SET @NUMOC = 1" & vbCrLf
sConsulta = sConsulta & "   DECLARE CATRIBUTOSOC CURSOR LOCAL FOR" & vbCrLf
sConsulta = sConsulta & "   SELECT ATRIB FROM @ATRIB_UON" & vbCrLf
sConsulta = sConsulta & "   OPEN CATRIBUTOSOC" & vbCrLf
sConsulta = sConsulta & "   FETCH NEXT FROM CATRIBUTOSOC INTO @ATRIBOC" & vbCrLf
sConsulta = sConsulta & "   WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       SET @SNUMOC = CONVERT(NVARCHAR(20),@NUMOC)" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = @SQL + ' LEFT JOIN ART4_UON_ATRIB AUA' + @SNUMOC + ' WITH (NOLOCK) ON AUA' + @SNUMOC + '.ART=AU.ART4 AND AUA' + @SNUMOC + '.UON1=AU.UON1 AND (AUA' + @SNUMOC + '.UON2 IS NULL OR AU.UON2=AUA' + @SNUMOC + '.UON2) AND (AUA' + @SNUMOC + '.UON3 IS NULL OR AU.UON3=AUA' + @SNUMOC + '.UON3) '" & vbCrLf
sConsulta = sConsulta & "       --CALIDAD Es sin WITH (NOLOCK) pq da error de ponerlo. No es una tabla es un parametro." & vbCrLf
sConsulta = sConsulta & "       SET @SQL = @SQL + ' INNER JOIN @ATRIB_UON ATR' + @SNUMOC + ' ON ATR'+@SNUMOC+'.ATRIB=AUA' + @SNUMOC + '.ATRIB AND ATR'+@SNUMOC+'.ATRIB='+CONVERT(NVARCHAR(20),@ATRIBOC)+ ' AND ATR'+@SNUMOC+'.UON1=AUA' + @SNUMOC + '.UON1 AND (ATR'+@SNUMOC+'.UON2='''' OR  AUA' + @SNUMOC + '.UON2=ATR'+@SNUMOC+'.UON2) AND (ATR'+@SNUMOC+'.UON3='''' OR  AUA' + @SNUMOC + '.UON3=ATR'+@SNUMOC+'.UON3) '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = @SQL + ' AND ISNULL(ATR'+@SNUMOC+'.VALOR_TEXT,'''')=ISNULL(AUA' + @SNUMOC + '.VALOR_TEXT,'''') AND ISNULL(ATR'+@SNUMOC+'.VALOR_NUM,'''')=ISNULL(AUA' + @SNUMOC + '.VALOR_NUM,'''') '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = @SQL + ' AND ISNULL(ATR'+@SNUMOC+'.VALOR_BOOL,'''')=ISNULL(AUA' + @SNUMOC + '.VALOR_BOOL,'''') AND ISNULL(ATR'+@SNUMOC+'.VALOR_FEC,'''')=ISNULL(AUA' + @SNUMOC + '.VALOR_FEC,'''') '" & vbCrLf
sConsulta = sConsulta & "       SET @NUMOC = @NUMOC + 1" & vbCrLf
sConsulta = sConsulta & "       FETCH NEXT FROM CATRIBUTOSOC INTO @ATRIBOC" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "   CLOSE CATRIBUTOSOC" & vbCrLf
sConsulta = sConsulta & "   DEALLOCATE CATRIBUTOSOC" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "-------------------------------------" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @RESTRIC = 1" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' INNER JOIN COM_GMN4 CG  WITH (NOLOCK)  on CG.COM = @PER AND A.GMN1 = CG.GMN1 AND A.GMN2 = CG.GMN2 AND A.GMN3 = CG.GMN3 AND A.GMN4 = CG.GMN4'" & vbCrLf
sConsulta = sConsulta & "--INICIO A?ADIDO PARA TAREA 1739---------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL + ' LEFT JOIN" & vbCrLf
sConsulta = sConsulta & "(SELECT COUNT(C.ART_INT) AS PERMISO_EP,C.ART_INT AS CODARTICULO" & vbCrLf
sConsulta = sConsulta & "FROM CATALOG_LIN C WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN VIEW_APROVISONADORES A WITH (NOLOCK) ON C.SEGURIDAD_CATN=A.CATN AND C.SEGURIDAD_NIVEL=A.NIVEL '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @PER IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL + 'WHERE A.PER=@PER '" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL + ' GROUP BY C.ART_INT" & vbCrLf
sConsulta = sConsulta & ") AS PERM_EP ON PERM_EP.CODARTICULO=A.COD '" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL + 'LEFT JOIN" & vbCrLf
sConsulta = sConsulta & "(SELECT COUNT(C.ART_INT) AS ESTA_CATALOGADO,C.ART_INT AS CODARTICULOCATALOGADO" & vbCrLf
sConsulta = sConsulta & "FROM CATALOG_LIN C WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "GROUP BY C.ART_INT" & vbCrLf
sConsulta = sConsulta & ") AS CATALOGADO ON CATALOGADO.CODARTICULOCATALOGADO=A.COD '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @RESTRIC_PED_ART_UON_USU =1 OR @RESTRIC_PED_ART_UON_PERF =1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ' LEFT JOIN(SELECT COUNT(A.COD) AS TIENE_PERMISO,A.COD CODARTICULO FROM( '" & vbCrLf
sConsulta = sConsulta & "IF @RESTRIC_PED_ART_UON_USU = 1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'SELECT ART4_UON.ART4 AS COD FROM ART4_UON WITH (NOLOCK) JOIN PER WITH (NOLOCK) ON ISNULL(PER.UON1,'''') = CASE WHEN PER.UON1 IS NULL  THEN '''' ELSE ART4_UON.UON1 END AND ISNULL(PER.UON2,'''') = CASE WHEN PER.UON2 IS NULL  THEN '''' ELSE ART4_UON.UON2 END AND ISNULL(PER.UON3,'''') = CASE WHEN PER.UON3 IS NULL  THEN '''' ELSE ART4_UON.UON3 END WHERE PER.COD=@PER" & vbCrLf
sConsulta = sConsulta & "UNION" & vbCrLf
sConsulta = sConsulta & "SELECT ART4.COD FROM ART4 WITH (NOLOCK) WHERE ART4.COD NOT IN (SELECT ART4_UON.ART4 FROM ART4_UON) '" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @RESTRIC_PED_ART_UON_PERF = 1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @RESTRIC_PED_ART_UON_USU = 1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ' UNION '" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL +" & vbCrLf
sConsulta = sConsulta & "' SELECT ART4_UON.ART4 AS COD FROM ART4_UON WITH (NOLOCK) JOIN PERF_UON WITH (NOLOCK) ON ISNULL(PERF_UON.UON1,'''') = CASE WHEN PERF_UON.UON1 IS NULL  THEN '''' ELSE ART4_UON.UON1 END AND ISNULL(PERF_UON.UON2,'''') = CASE WHEN PERF_UON.UON2 IS NULL  THEN '''' ELSE ART4_UON.UON2 END AND ISNULL(PERF_UON.UON3,'''') = CASE WHEN PERF_UON.UON3 IS NULL  THEN '''' ELSE ART4_UON.UON3 END WHERE PERF_UON.PERF =@PERF" & vbCrLf
sConsulta = sConsulta & "UNION" & vbCrLf
sConsulta = sConsulta & "SELECT ART4.COD FROM ART4 WITH (NOLOCK) WHERE ART4.COD NOT IN (SELECT ART4_UON.ART4 FROM ART4_UON) '" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "IF @RESTRIC_PED_ART_UON_USU =1 OR @RESTRIC_PED_ART_UON_PERF =1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ') A GROUP BY A.COD ) AS PERMISO_EMISION ON PERMISO_EMISION.CODARTICULO=A.COD '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--FIN A?ADIDO PARA TAREA 1739---------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- Tarea 2904 ------------------------------" & vbCrLf
sConsulta = sConsulta & "IF @CODPROVEEDOR IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @FILTRARNEGOCIADOS=1 --1- art negociado" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "--Negociado de prove dado" & vbCrLf
sConsulta = sConsulta & "--, no necesito mirar prove_gmn4 pq si esta negociado se supone q sera art manejado por prove" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' INNER JOIN (SELECT AA.ART,AA.PREC,AA.CODPROVE,AA.DENPROVE,AA.UNI,ISNULL(UD.DEN,'''') AS DEN, PO.MON MON" & vbCrLf
sConsulta = sConsulta & ", PO.CAMBIO CAMBIO_OFE, P.CAMBIO CAMBIO_PROCE, MD.DEN AS DENMON' + @SQLSELPAG" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' FROM ART4_ADJ AA WITH(NOLOCK)'" & vbCrLf
sConsulta = sConsulta & "--Art4_adj. Guarda en moneda central. Para saber el cambio en moneda de la adjudicaci?n me voy a proceso" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' INNER JOIN PROCE P WITH(NOLOCK) ON P.ANYO=AA.ANYO AND P.GMN1=AA.GMN1 AND P.COD=AA.PROCE'" & vbCrLf
sConsulta = sConsulta & "--Art4_adj. Guarda en moneda central. Para saber la moneda de la adjudicaci?n me voy a la ultima oferta" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' INNER JOIN PROCE_OFE PO WITH(NOLOCK) ON PO.ANYO=AA.ANYO AND PO.GMN1=AA.GMN1 AND PO.PROCE=AA.PROCE AND PO.PROVE=AA.CODPROVE'" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N'    AND PO.OFE = (SELECT MAX(OFE) FROM PROCE_OFE  WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN ITEM_OFE IO WITH(NOLOCK) ON PROCE_OFE.ANYO=IO.ANYO AND PROCE_OFE.GMN1=IO.GMN1 AND PROCE_OFE.PROCE=IO.PROCE AND PROCE_OFE.PROVE=IO.PROVE AND PROCE_OFE.OFE=IO.NUM" & vbCrLf
sConsulta = sConsulta & "INNER JOIN ITEM I WITH(NOLOCK) ON I.ANYO=IO.ANYO AND I.GMN1_PROCE=IO.GMN1 AND I.PROCE=IO.PROCE AND I.ART=AA.ART AND I.ID=IO.ITEM" & vbCrLf
sConsulta = sConsulta & "WHERE PROCE_OFE.ANYO=AA.ANYO AND PROCE_OFE.GMN1=AA.GMN1 AND PROCE_OFE.PROCE=AA.PROCE AND PROCE_OFE.PROVE=AA.CODPROVE)'" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' LEFT JOIN UNI_DEN UD WITH(NOLOCK) ON UD.UNI=AA.UNI AND UD.IDIOMA=@IDI'" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' INNER JOIN MON_DEN MD WITH(NOLOCK) ON PO.MON = MD.MON AND MD.IDIOMA =@IDI'" & vbCrLf
sConsulta = sConsulta & "--Proveedores de la org. compras" & vbCrLf
sConsulta = sConsulta & "IF @CODORGCOMPRAS IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N'  INNER JOIN PROVE WITH(NOLOCK) ON PROVE.COD=AA.CODPROVE '" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N'  INNER JOIN PROVE_ERP WITH(NOLOCK) ON PROVE.NIF = PROVE_ERP.NIF AND PROVE_ERP.BAJALOG=0'" & vbCrLf
sConsulta = sConsulta & "IF @DESDEERP=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL + ' AND PROVE_ERP.EMPRESA=ISNULL(@EMPRESA,PROVE_ERP.EMPRESA)'" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N'  INNER JOIN EMP WITH(NOLOCK) ON PROVE_ERP.EMPRESA = EMP.ID '" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N'  INNER JOIN SOCIEDAD_ORGCOMPRAS WITH(NOLOCK) ON EMP.SOCIEDAD=SOCIEDAD_ORGCOMPRAS.SOCIEDAD '" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N'    AND SOCIEDAD_ORGCOMPRAS.ORGCOMPRAS=@CODORGCOMPRAS'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + @SQLJOINPAG_P" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + @SQLJOINPAG_D" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' WHERE AA.ID = (SELECT MAX(ID) FROM ART4_ADJ  WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE ART4_ADJ.ART=AA.ART" & vbCrLf
sConsulta = sConsulta & "AND ART4_ADJ.FECINI<=GETDATE()" & vbCrLf
sConsulta = sConsulta & "AND ART4_ADJ.FECFIN>=GETDATE()" & vbCrLf
sConsulta = sConsulta & "AND ART4_ADJ.CODPROVE=@CODPROVEEDOR)'" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' ) NEGO ON NEGO.ART=A.COD'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "IF @FILTRARNEGOCIADOS=2 --2- art no negociado" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "--No negociado por nadie" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' LEFT JOIN (SELECT ID, ART'" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' FROM ART4_ADJ AA WITH(NOLOCK)'" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' WHERE AA.FECINI<=GETDATE() AND AA.FECFIN>=GETDATE()'" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' ) NEGO ON NEGO.ART=A.COD'" & vbCrLf
sConsulta = sConsulta & "--es del proveedor dado" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' INNER JOIN PROVE_GMN4 PG WITH(NOLOCK) ON PG.PROVE=@CODPROVEEDOR'" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' AND A.GMN1=PG.GMN1 AND A.GMN2=PG.GMN2 AND A.GMN3=PG.GMN3 AND A.GMN4=PG.GMN4'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @FILTRARNEGOCIADOS=3 --1- art negociado" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "--Negociado de prove dado" & vbCrLf
sConsulta = sConsulta & "--, no necesito mirar prove_gmn4 pq si esta negociado se supone q sera art manejado por prove" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' LEFT JOIN (SELECT AA.ART,AA.PREC,AA.CODPROVE,AA.DENPROVE,AA.UNI,ISNULL(UD.DEN,'''') AS DEN, PO.MON MON" & vbCrLf
sConsulta = sConsulta & ", PO.CAMBIO CAMBIO_OFE, P.CAMBIO CAMBIO_PROCE, MD.DEN AS DENMON' + @SQLSELPAG" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' FROM ART4_ADJ AA WITH(NOLOCK)'" & vbCrLf
sConsulta = sConsulta & "--Art4_adj. Guarda en moneda central. Para saber el cambio en moneda de la adjudicaci?n me voy a proceso" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' LEFT JOIN PROCE P WITH(NOLOCK) ON P.ANYO=AA.ANYO AND P.GMN1=AA.GMN1 AND P.COD=AA.PROCE'" & vbCrLf
sConsulta = sConsulta & "--Art4_adj. Guarda en moneda central. Para saber la moneda de la adjudicaci?n me voy a la ultima oferta" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' LEFT JOIN PROCE_OFE PO WITH(NOLOCK) ON PO.ANYO=AA.ANYO AND PO.GMN1=AA.GMN1 AND PO.PROCE=AA.PROCE AND PO.PROVE=AA.CODPROVE'" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N'    AND PO.OFE = (SELECT MAX(OFE) FROM PROCE_OFE  WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN ITEM_OFE IO WITH(NOLOCK) ON PROCE_OFE.ANYO=IO.ANYO AND PROCE_OFE.GMN1=IO.GMN1 AND PROCE_OFE.PROCE=IO.PROCE AND PROCE_OFE.PROVE=IO.PROVE AND PROCE_OFE.OFE=IO.NUM" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN ITEM I WITH(NOLOCK) ON I.ANYO=IO.ANYO AND I.GMN1_PROCE=IO.GMN1 AND I.PROCE=IO.PROCE AND I.ART=AA.ART AND I.ID=IO.ITEM" & vbCrLf
sConsulta = sConsulta & "WHERE PROCE_OFE.ANYO=AA.ANYO AND PROCE_OFE.GMN1=AA.GMN1 AND PROCE_OFE.PROCE=AA.PROCE AND PROCE_OFE.PROVE=AA.CODPROVE)'" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' LEFT JOIN UNI_DEN UD WITH(NOLOCK) ON UD.UNI=AA.UNI AND UD.IDIOMA=@IDI'" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' LEFT JOIN MON_DEN MD WITH(NOLOCK) ON PO.MON = MD.MON AND MD.IDIOMA =@IDI'" & vbCrLf
sConsulta = sConsulta & "--Proveedores de la org. compras" & vbCrLf
sConsulta = sConsulta & "IF @CODORGCOMPRAS IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N'  LEFT JOIN PROVE WITH(NOLOCK) ON PROVE.COD=AA.CODPROVE '" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N'  INNER JOIN PROVE_ERP WITH(NOLOCK) ON PROVE.NIF = PROVE_ERP.NIF AND PROVE_ERP.BAJALOG=0'" & vbCrLf
sConsulta = sConsulta & "IF @DESDEERP=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL + ' AND PROVE_ERP.EMPRESA=ISNULL(@EMPRESA,PROVE_ERP.EMPRESA)'" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N'  LEFT JOIN EMP WITH(NOLOCK) ON PROVE_ERP.EMPRESA = EMP.ID '" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N'  INNER JOIN SOCIEDAD_ORGCOMPRAS WITH(NOLOCK) ON EMP.SOCIEDAD=SOCIEDAD_ORGCOMPRAS.SOCIEDAD '" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N'    AND SOCIEDAD_ORGCOMPRAS.ORGCOMPRAS=@CODORGCOMPRAS'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + @SQLJOINPAG_P" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + @SQLJOINPAG_D" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' WHERE AA.ID = (SELECT MAX(ID) FROM ART4_ADJ  WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE ART4_ADJ.ART=AA.ART" & vbCrLf
sConsulta = sConsulta & "AND ART4_ADJ.FECINI<=GETDATE()" & vbCrLf
sConsulta = sConsulta & "AND ART4_ADJ.FECFIN>=GETDATE()" & vbCrLf
sConsulta = sConsulta & "AND ART4_ADJ.CODPROVE=@CODPROVEEDOR)'" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' ) NEGO ON NEGO.ART=A.COD'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @FILTRARNEGOCIADOS=0 --0- Todos" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "--es del proveedor dado" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' INNER JOIN PROVE_GMN4 PG WITH(NOLOCK) ON PG.PROVE=@CODPROVEEDOR'" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' AND A.GMN1=PG.GMN1 AND A.GMN2=PG.GMN2 AND A.GMN3=PG.GMN3 AND A.GMN4=PG.GMN4'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @FILTRARNEGOCIADOS=1 --1- art negociado" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "--Ultimo negociado del art" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' INNER JOIN (SELECT  AA.ART,AA.ID,AA.PREC,AA.CODPROVE,AA.DENPROVE,AA.UNI,ISNULL(UD.DEN,'''') AS DEN, PO.MON MON" & vbCrLf
sConsulta = sConsulta & ", PO.CAMBIO CAMBIO_OFE, P.CAMBIO CAMBIO_PROCE, MD.DEN AS DENMON' + @SQLSELPAG" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' FROM ART4_ADJ AA WITH(NOLOCK)'" & vbCrLf
sConsulta = sConsulta & "--Art4_adj. Guarda en moneda central. Para saber el cambio en moneda de la adjudicaci?n me voy a proceso" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' INNER JOIN PROCE P WITH(NOLOCK) ON P.ANYO=AA.ANYO AND P.GMN1=AA.GMN1 AND P.COD=AA.PROCE'" & vbCrLf
sConsulta = sConsulta & "--Art4_adj. Guarda en moneda central. Para saber la moneda de la adjudicaci?n me voy a la ultima oferta" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' INNER JOIN PROCE_OFE PO WITH(NOLOCK) ON PO.ANYO=AA.ANYO AND PO.GMN1=AA.GMN1 AND PO.PROCE=AA.PROCE AND PO.PROVE=AA.CODPROVE'" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N'    AND PO.OFE = (SELECT MAX(OFE) FROM PROCE_OFE  WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN ITEM_OFE IO WITH(NOLOCK) ON PROCE_OFE.ANYO=IO.ANYO AND PROCE_OFE.GMN1=IO.GMN1 AND PROCE_OFE.PROCE=IO.PROCE AND PROCE_OFE.PROVE=IO.PROVE AND PROCE_OFE.OFE=IO.NUM" & vbCrLf
sConsulta = sConsulta & "INNER JOIN ITEM I WITH(NOLOCK) ON I.ANYO=IO.ANYO AND I.GMN1_PROCE=IO.GMN1 AND I.PROCE=IO.PROCE AND I.ART=AA.ART AND I.ID=IO.ITEM" & vbCrLf
sConsulta = sConsulta & "WHERE PROCE_OFE.ANYO=AA.ANYO AND PROCE_OFE.GMN1=AA.GMN1 AND PROCE_OFE.PROCE=AA.PROCE AND PROCE_OFE.PROVE=AA.CODPROVE)'" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' LEFT JOIN UNI_DEN UD WITH(NOLOCK) ON UD.UNI=AA.UNI AND UD.IDIOMA=@IDI'" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' INNER JOIN MON_DEN MD WITH(NOLOCK) ON PO.MON = MD.MON AND MD.IDIOMA =@IDI'" & vbCrLf
sConsulta = sConsulta & "--Proveedores de la org. compras" & vbCrLf
sConsulta = sConsulta & "IF @CODORGCOMPRAS IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N'  INNER JOIN PROVE WITH(NOLOCK) ON PROVE.COD=AA.CODPROVE '" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N'  INNER JOIN PROVE_ERP WITH(NOLOCK) ON PROVE.NIF = PROVE_ERP.NIF AND PROVE_ERP.BAJALOG=0'" & vbCrLf
sConsulta = sConsulta & "IF @DESDEERP=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL + ' AND PROVE_ERP.EMPRESA=ISNULL(@EMPRESA,PROVE_ERP.EMPRESA)'" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N'  INNER JOIN EMP WITH(NOLOCK) ON PROVE_ERP.EMPRESA = EMP.ID '" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N'  INNER JOIN SOCIEDAD_ORGCOMPRAS WITH(NOLOCK) ON EMP.SOCIEDAD=SOCIEDAD_ORGCOMPRAS.SOCIEDAD '" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N'    AND SOCIEDAD_ORGCOMPRAS.ORGCOMPRAS=@CODORGCOMPRAS'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + @SQLJOINPAG_P" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + @SQLJOINPAG_D" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' WHERE AA.FECINI<=GETDATE() AND AA.FECFIN>=GETDATE()'" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' ) NEGO ON NEGO.ART=A.COD'" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' AND NEGO.ID = (SELECT MAX(ID) FROM ART4_ADJ WITH(NOLOCK)'" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N'  WHERE ART4_ADJ.ART = A.COD AND ART4_ADJ.FECINI<=GETDATE() AND ART4_ADJ.FECFIN>=GETDATE())'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "IF @FILTRARNEGOCIADOS=2 --2- art no negociado" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "--No negociado por nadie" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' LEFT JOIN (SELECT ID, ART'" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' FROM ART4_ADJ AA WITH(NOLOCK)'" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' WHERE AA.FECINI<=GETDATE() AND AA.FECFIN>=GETDATE()'" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' ) NEGO ON NEGO.ART=A.COD'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "IF @FILTRARNEGOCIADOS=3 --1 EXPRESS NEGOCIADOS O NO" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "--Ultimo negociado del art" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' LEFT JOIN (SELECT AA.ART,AA.ID,AA.PREC,AA.CODPROVE,AA.DENPROVE,AA.UNI,ISNULL(UD.DEN,'''') AS DEN, PO.MON MON" & vbCrLf
sConsulta = sConsulta & ", PO.CAMBIO CAMBIO_OFE, P.CAMBIO CAMBIO_PROCE, MD.DEN AS DENMON' + @SQLSELPAG" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' FROM ART4_ADJ AA WITH(NOLOCK)'" & vbCrLf
sConsulta = sConsulta & "--Art4_adj. Guarda en moneda central. Para saber el cambio en moneda de la adjudicaci?n me voy a proceso" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' LEFT JOIN PROCE P WITH(NOLOCK) ON P.ANYO=AA.ANYO AND P.GMN1=AA.GMN1 AND P.COD=AA.PROCE'" & vbCrLf
sConsulta = sConsulta & "--Art4_adj. Guarda en moneda central. Para saber la moneda de la adjudicaci?n me voy a la ultima oferta" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' LEFT JOIN PROCE_OFE PO WITH(NOLOCK) ON PO.ANYO=AA.ANYO AND PO.GMN1=AA.GMN1 AND PO.PROCE=AA.PROCE AND PO.PROVE=AA.CODPROVE'" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N'    AND PO.OFE = (SELECT MAX(OFE) FROM PROCE_OFE  WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN ITEM_OFE IO WITH(NOLOCK) ON PROCE_OFE.ANYO=IO.ANYO AND PROCE_OFE.GMN1=IO.GMN1 AND PROCE_OFE.PROCE=IO.PROCE AND PROCE_OFE.PROVE=IO.PROVE AND PROCE_OFE.OFE=IO.NUM" & vbCrLf
sConsulta = sConsulta & "INNER JOIN ITEM I WITH(NOLOCK) ON I.ANYO=IO.ANYO AND I.GMN1_PROCE=IO.GMN1 AND I.PROCE=IO.PROCE AND I.ART=AA.ART AND I.ID=IO.ITEM" & vbCrLf
sConsulta = sConsulta & "WHERE PROCE_OFE.ANYO=AA.ANYO AND PROCE_OFE.GMN1=AA.GMN1 AND PROCE_OFE.PROCE=AA.PROCE AND PROCE_OFE.PROVE=AA.CODPROVE)'" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' LEFT JOIN UNI_DEN UD WITH(NOLOCK) ON UD.UNI=AA.UNI AND UD.IDIOMA=@IDI'" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' INNER JOIN MON_DEN MD WITH(NOLOCK) ON PO.MON = MD.MON AND MD.IDIOMA =@IDI'" & vbCrLf
sConsulta = sConsulta & "--Proveedores de la org. compras" & vbCrLf
sConsulta = sConsulta & "IF @CODORGCOMPRAS IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N'  LEFT JOIN PROVE WITH(NOLOCK) ON PROVE.COD=AA.CODPROVE '" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N'  INNER JOIN PROVE_ERP WITH(NOLOCK) ON PROVE.NIF = PROVE_ERP.NIF AND PROVE_ERP.BAJALOG=0'" & vbCrLf
sConsulta = sConsulta & "IF @DESDEERP=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL + ' AND PROVE_ERP.EMPRESA=ISNULL(@EMPRESA,PROVE_ERP.EMPRESA)'" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N'  INNER JOIN EMP WITH(NOLOCK) ON PROVE_ERP.EMPRESA = EMP.ID '" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N'  INNER JOIN SOCIEDAD_ORGCOMPRAS WITH(NOLOCK) ON EMP.SOCIEDAD=SOCIEDAD_ORGCOMPRAS.SOCIEDAD '" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N'    AND SOCIEDAD_ORGCOMPRAS.ORGCOMPRAS=@CODORGCOMPRAS'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + @SQLJOINPAG_P" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + @SQLJOINPAG_D" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' WHERE AA.FECINI<=GETDATE() AND AA.FECFIN>=GETDATE()'" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' ) NEGO ON NEGO.ART=A.COD'" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' AND NEGO.ID = (SELECT MAX(ID) FROM ART4_ADJ WITH(NOLOCK)'" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N'  WHERE ART4_ADJ.ART = A.COD AND ART4_ADJ.FECINI<=GETDATE() AND ART4_ADJ.FECFIN>=GETDATE())'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "-- FIN Tarea 2904 --------------------------" & vbCrLf
sConsulta = sConsulta & "--Para filtrar por atributos de UON (UON)" & vbCrLf
sConsulta = sConsulta & "IF (@RUON1 IS NOT NULL) AND (EXISTS(SELECT 1 FROM @ATRIB_UON))" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   DECLARE @ATRIB INT" & vbCrLf
sConsulta = sConsulta & "   DECLARE @NUM INT" & vbCrLf
sConsulta = sConsulta & "   DECLARE @SNUM NVARCHAR(20)" & vbCrLf
sConsulta = sConsulta & "   SET @NUM = 1" & vbCrLf
sConsulta = sConsulta & "   DECLARE CATRIBUTOS CURSOR LOCAL FOR" & vbCrLf
sConsulta = sConsulta & "   SELECT ATRIB FROM @ATRIB_UON" & vbCrLf
sConsulta = sConsulta & "   OPEN CATRIBUTOS" & vbCrLf
sConsulta = sConsulta & "   FETCH NEXT FROM CATRIBUTOS INTO @ATRIB" & vbCrLf
sConsulta = sConsulta & "   WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       SET @SNUM = CONVERT(NVARCHAR(20),@NUM)" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = @SQL + ' LEFT JOIN ART4_UON_ATRIB AUA' + @SNUM + '  WITH(NOLOCK) ON AUA' + @SNUM + '.ART=AU2.ART4 AND AUA' + @SNUM + '.UON1=AU2.UON1 AND (AUA' + @SNUM + '.UON2 IS NULL OR AU2.UON2=AUA' + @SNUM + '.UON2) AND (AUA' + @SNUM + '.UON3 IS NULL OR AU2.UON3=AUA' + @SNUM + '.UON3) '" & vbCrLf
sConsulta = sConsulta & "       --CALIDAD Es sin WITH (NOLOCK) pq da error de ponerlo. No es una tabla es un parametro." & vbCrLf
sConsulta = sConsulta & "       SET @SQL = @SQL + ' INNER JOIN @ATRIB_UON ATR' + @SNUM + ' ON ATR'+@SNUM+'.ATRIB=AUA' + @SNUM + '.ATRIB AND ATR'+@SNUM+'.ATRIB='+CONVERT(NVARCHAR(20),@ATRIB)+ ' AND ATR'+@SNUM+'.UON1=AUA' + @SNUM + '.UON1 AND (ATR'+@SNUM+'.UON2='''' OR  AUA' + @SNUM + '.UON2=ATR'+@SNUM+'.UON2) AND (ATR'+@SNUM+'.UON3='''' OR  AUA' + @SNUM + '.UON3=ATR'+@SNUM+'.UON3) '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = @SQL + ' AND ISNULL(ATR'+@SNUM+'.VALOR_TEXT,'''')=ISNULL(AUA' + @SNUM + '.VALOR_TEXT,'''') AND ISNULL(ATR'+@SNUM+'.VALOR_NUM,'''')=ISNULL(AUA' + @SNUM + '.VALOR_NUM,'''') '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = @SQL + ' AND ISNULL(ATR'+@SNUM+'.VALOR_BOOL,'''')=ISNULL(AUA' + @SNUM + '.VALOR_BOOL,'''') AND ISNULL(ATR'+@SNUM+'.VALOR_FEC,'''')=ISNULL(AUA' + @SNUM + '.VALOR_FEC,'''') '" & vbCrLf
sConsulta = sConsulta & "       SET @NUM = @NUM + 1" & vbCrLf
sConsulta = sConsulta & "       FETCH NEXT FROM CATRIBUTOS INTO @ATRIB" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "   CLOSE CATRIBUTOS" & vbCrLf
sConsulta = sConsulta & "   DEALLOCATE CATRIBUTOS" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "--------------------------------" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N' WHERE 1 = 1 '" & vbCrLf
sConsulta = sConsulta & "IF @RUON1 IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'AND (AU2.UON1=@RUON1 AND (@RUON2 IS NULL OR AU2.UON2=@RUON2) AND (@RUON3 IS NULL OR AU2.UON3=@RUON3)) '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @COD IS NOT NULL AND @COD<>''" & vbCrLf
sConsulta = sConsulta & "IF @COINCID=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' AND A.COD = @COD'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' AND A.COD LIKE @COD + ''%'''" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @DEN IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "IF @COINCID=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' AND A.DEN = @DEN'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' AND A.DEN LIKE @DEN + ''%'''" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @GMN1 IS NOT NULL AND @GMN1<>''" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' AND A.GMN1 = @GMN1'" & vbCrLf
sConsulta = sConsulta & "IF @GMN2 IS NOT NULL AND @GMN2<>''" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' AND A.GMN2 = @GMN2'" & vbCrLf
sConsulta = sConsulta & "IF @GMN3 IS NOT NULL AND @GMN3<>''" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' AND A.GMN3 = @GMN3'" & vbCrLf
sConsulta = sConsulta & "IF @GMN4 IS NOT NULL AND @GMN4<>''" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' AND A.GMN4 = @GMN4'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF (@CODORGCOMPRAS IS NOT NULL)" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N' and isNull(u3.orgcompras,isNull(u2.orgcompras,u1.orgcompras)) =@CODORGCOMPRAS'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF (@CODCENTRO IS NOT NULL)" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N' and isNull(u3.centros,isNull(u2.centros,u1.centros)) = @CODCENTRO'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CODINI IS NOT NULL AND (@COD IS NULL OR (@COD IS NOT NULL AND @COINCID<>1))" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SQL3=@SQL" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N'  AND A.COD" & vbCrLf
sConsulta = sConsulta & "NOT IN" & vbCrLf
sConsulta = sConsulta & "(SELECT TOP ' + CAST(ISNULL(@DESDEFILA,1)-1 AS NVARCHAR(10)) + N'   A.COD ' + @SQL3 + ' ORDER BY A.COD)'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @CODULT IS NOT NULL AND (@COD IS NULL OR (@COD IS NOT NULL AND @COINCID<>1))" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N'  AND A.COD > @CODULT'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "----------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "-- OTRAS CARACTERISTICAS" & vbCrLf
sConsulta = sConsulta & "----------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "--Gen?ricos" & vbCrLf
sConsulta = sConsulta & "IF @GENERICOS=0" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' AND A.GENERICO=1'" & vbCrLf
sConsulta = sConsulta & "IF @GENERICOS=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' AND A.GENERICO=0'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Concepto" & vbCrLf
sConsulta = sConsulta & "IF @GASTO=1 OR @INVERSION=1 OR @GASTOINVERSION=1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' AND A.CONCEPTO IN ('" & vbCrLf
sConsulta = sConsulta & "IF @GASTO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL + '0,'" & vbCrLf
sConsulta = sConsulta & "IF @INVERSION=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL + '1,'" & vbCrLf
sConsulta = sConsulta & "IF @GASTOINVERSION=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL + '2,'" & vbCrLf
sConsulta = sConsulta & "SET @SQL= LEFT(@SQL,LEN(@SQL)-1) + ')'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Almacenar" & vbCrLf
sConsulta = sConsulta & "IF @NOALMACENABLE=1 OR @ALMACENABLE=1 OR @ALMACENOPCIONAL=1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' AND A.ALMACENAR IN ('" & vbCrLf
sConsulta = sConsulta & "IF @NOALMACENABLE=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL + '0,'" & vbCrLf
sConsulta = sConsulta & "IF @ALMACENABLE=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL + '1,'" & vbCrLf
sConsulta = sConsulta & "IF @ALMACENOPCIONAL=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL + '2,'" & vbCrLf
sConsulta = sConsulta & "SET @SQL= LEFT(@SQL,LEN(@SQL)-1) + ')'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Recepcionar" & vbCrLf
sConsulta = sConsulta & "IF @RECEPCIONOBLIG=1 OR @NORECEPCIONABLE=1 OR @RECEPCIONOPCIONAL=1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' AND A.RECEPCIONAR IN ('" & vbCrLf
sConsulta = sConsulta & "IF @NORECEPCIONABLE=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL + '0,'" & vbCrLf
sConsulta = sConsulta & "IF @RECEPCIONOBLIG=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL + '1,'" & vbCrLf
sConsulta = sConsulta & "IF @RECEPCIONOPCIONAL=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL + '2,'" & vbCrLf
sConsulta = sConsulta & "SET @SQL= LEFT(@SQL,LEN(@SQL)-1) + ')'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Catalogado" & vbCrLf
sConsulta = sConsulta & "--INICIO A?ADIDO PARA TAREA 1739---------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "IF @MATERIALESCATALOGADOS=0" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL + ' AND CATALOGADO.ESTA_CATALOGADO IS NULL'" & vbCrLf
sConsulta = sConsulta & "--FIN A?ADIDO PARA TAREA 1739---------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "IF @MATERIALESCATALOGADOS=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL + ' AND CATALOGADO.ESTA_CATALOGADO IS NOT NULL'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- Tarea 2904 ------------------------------" & vbCrLf
sConsulta = sConsulta & "IF @FILTRARNEGOCIADOS=2 --No negociado por nadie" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL + ' AND (NEGO.ID IS NULL OR A.GENERICO=1)'" & vbCrLf
sConsulta = sConsulta & "-- FIN Tarea 2904 ------------------------------" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL2 + @SQL + N' ORDER BY A.COD'" & vbCrLf
sConsulta = sConsulta & "SET @PARAM = N'@COD VARCHAR(50)=NULL,@DEN VARCHAR(2000)=NULL,@GMN1 VARCHAR(50)=NULL,@GMN2 VARCHAR(50)=NULL,@GMN3 VARCHAR(50)=NULL,@GMN4 VARCHAR(50)=NULL,@COINCID TINYINT=1,@PER VARCHAR(50)=NULL,@GENERICOS TINYINT=0,@CODORGCOMPRAS VARCHAR(4),@CODCENTRO VARCHAR(4)=NULL,@CODINI VARCHAR(50),@CODULT VARCHAR(50),@EQUIV FLOAT,@EQUIVINS FLOAT,@GASTO TINYINT,@INVERSION TINYINT,@GASTOINVERSION TINYINT,@NOALMACENABLE TINYINT,@ALMACENABLE TINYINT,@ALMACENOPCIONAL TINYINT,@RECEPCIONOBLIG TINYINT,@NORECEPCIONABLE TINYINT, @RECEPCIONOPCIONAL TINYINT, @IDI VARCHAR(50), @CODPROVEEDOR VARCHAR(50)=NULL, @EMPRESA INT=NULL,@PERF INT,@RUON1 NVARCHAR(50),@RUON2 NVARCHAR(50),@RUON3 NVARCHAR(50),@USU NVARCHAR(50),@ATRIB_UON TBATRIBUON READONLY'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, @PARAM, @COD = @COD, @DEN=@DEN, @GMN1=@GMN1, @GMN2=@GMN2, @GMN3=@GMN3, @GMN4=@GMN4, @COINCID=@COINCID, @PER=@PER, @GENERICOS=@GENERICOS, @CODORGCOMPRAS=@CODORGCOMPRAS,@CODCENTRO=@CODCENTRO,@CODINI=@CODINI,@CODULT=@CODULT,@EQUIV=@EQUIV,@EQUIVINS=@EQUIVINS,@GASTO=@GASTO, @INVERSION=@INVERSION, @GASTOINVERSION=@GASTOINVERSION, @NOALMACENABLE=@NOALMACENABLE, @ALMACENABLE=@ALMACENABLE, @ALMACENOPCIONAL=@ALMACENOPCIONAL, @RECEPCIONOBLIG=@RECEPCIONOBLIG, @NORECEPCIONABLE=@NORECEPCIONABLE, @RECEPCIONOPCIONAL=@RECEPCIONOPCIONAL,@IDI=@IDI, @CODPROVEEDOR=@CODPROVEEDOR, @EMPRESA=@EMPRESA,@PERF=@PERF,@RUON1=@RUON1,@RUON2=@RUON2,@RUON3=@RUON3,@USU=@USU,@ATRIB_UON=@ATRIB_UON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_SAVE_INSTANCIA_EN_FILTRO]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_SAVE_INSTANCIA_EN_FILTRO]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_SAVE_INSTANCIA_EN_FILTRO]" & vbCrLf
sConsulta = sConsulta & "@INSTANCIA INT," & vbCrLf
sConsulta = sConsulta & "@INSERT TINYINT," & vbCrLf
sConsulta = sConsulta & "@QA TINYINT=0," & vbCrLf
sConsulta = sConsulta & "@CONTRATO TINYINT=0" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @INSTANCIA_L INT" & vbCrLf
sConsulta = sConsulta & "SET @INSTANCIA_L=@INSTANCIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Obtenemos el nombre de las tablas din?micas del filtro (falta a?adir el prefijo del idioma)." & vbCrLf
sConsulta = sConsulta & "DECLARE @TABLA NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "IF @QA=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @CONTRATO=0" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT @TABLA=F.NOM_TABLA FROM INSTANCIA I WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_GRUPO G WITH (NOLOCK) ON I.ID=G.INSTANCIA" & vbCrLf
sConsulta = sConsulta & "INNER JOIN FORM_GRUPO G2 WITH (NOLOCK) ON G.GRUPO_ORIGEN=G2.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_FILTROS F WITH (NOLOCK) ON G2.FORMULARIO=F.FORMULARIO" & vbCrLf
sConsulta = sConsulta & "WHERE I.ID = @INSTANCIA_L" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT @TABLA=F.NOM_TABLA FROM INSTANCIA I WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_GRUPO G WITH (NOLOCK) ON I.ID=G.INSTANCIA" & vbCrLf
sConsulta = sConsulta & "INNER JOIN FORM_GRUPO G2 WITH (NOLOCK) ON G.GRUPO_ORIGEN=G2.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_CONTR_FILTROS F WITH (NOLOCK) ON G2.FORMULARIO=F.FORMULARIO" & vbCrLf
sConsulta = sConsulta & "WHERE I.ID = @INSTANCIA_L" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT @TABLA=F.NOM_TABLA FROM INSTANCIA I WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_GRUPO G WITH (NOLOCK) ON I.ID=G.INSTANCIA" & vbCrLf
sConsulta = sConsulta & "INNER JOIN FORM_GRUPO G2 WITH (NOLOCK) ON G.GRUPO_ORIGEN=G2.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN QA_FILTROS F WITH (NOLOCK) ON G2.FORMULARIO=F.FORMULARIO" & vbCrLf
sConsulta = sConsulta & "WHERE I.ID = @INSTANCIA_L" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Obtenemos las longitudes de los materiales de la tabla DIC" & vbCrLf
sConsulta = sConsulta & "DECLARE @LEN_GMN1 INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @LEN_GMN2 INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @LEN_GMN3 INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @LEN_GMN4 INT" & vbCrLf
sConsulta = sConsulta & "SELECT @LEN_GMN1=DIC.LONGITUD, @LEN_GMN2=DIC2.LONGITUD,@LEN_GMN3=DIC3.LONGITUD, @LEN_GMN4=DIC4.LONGITUD FROM DIC WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN DIC DIC2 WITH (NOLOCK) ON DIC2.NOMBRE='GMN2'" & vbCrLf
sConsulta = sConsulta & "INNER JOIN DIC DIC3 WITH (NOLOCK) ON DIC3.NOMBRE='GMN3'" & vbCrLf
sConsulta = sConsulta & "INNER JOIN DIC DIC4 WITH (NOLOCK) ON DIC4.NOMBRE='GMN4'" & vbCrLf
sConsulta = sConsulta & "WHERE DIC.NOMBRE = 'GMN1'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Guardamos los IDs en una tabla temporal" & vbCrLf
sConsulta = sConsulta & "--Es lo ?nico que cambia de los 3 casos (QA, Solicitudes y Contratos)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLINSERT NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE #G1 (ID_CAMPO INT)" & vbCrLf
sConsulta = sConsulta & "SET @SQLINSERT = N'INSERT INTO #G1 SELECT ID_CAMPO FROM '" & vbCrLf
sConsulta = sConsulta & "IF @QA=0" & vbCrLf
sConsulta = sConsulta & "IF @CONTRATO = 0" & vbCrLf
sConsulta = sConsulta & "SET @SQLINSERT = @SQLINSERT + N'UPD_VISORES_PM WITH(NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQLINSERT = @SQLINSERT + N'UPD_VISORES_CONTR WITH(NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQLINSERT = @SQLINSERT + N'UPD_VISORES_QA WITH(NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "SET @SQLINSERT = @SQLINSERT + N'WHERE (ACCION=''I'' OR ACCION=''A'') AND (NOM_TABLA=@TABLA OR NOM_TABLA=@TABLA+''_DESGLOSE'') AND OK=0'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLINSERT, N'@TABLA NVARCHAR(100)', @TABLA=@TABLA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @LINEA INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @LINEA_ANT INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @CAMPO_ORIGEN INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @SUBTIPO INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @TIPO_CAMPO_GS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @VALOR_NUM FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @VALOR_TEXT NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @VALOR_FEC DATETIME" & vbCrLf
sConsulta = sConsulta & "DECLARE @VALOR_BOOL TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @VALOR_TEXT_DEN NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @VALOR_TEXT_COD NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @INTRO TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_ATRIB_GS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @TABLA_EXTERNA INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @CAMPO_DEF INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL_COD NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLCAMPOSINSERT NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @EJECUTARSQL TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @CAMPO_PADRE_DESGLOSE INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @CAMPO_PADRE_DESGLOSE_ANT INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @DENOMINACION NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @CAMPOTRATADO TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @EMPIEZAIDIOMA INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ACABADENOMINACION INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @EMPIEZADENOMINACION INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @SEPARADOR INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRESUPUESTOS NVARCHAR(800)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PORCENTAJE NVARCHAR(400)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SEPARADOR_DEN INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @SEPARADOR_PORCENTAJE INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDIOMA AS NVARCHAR(400)" & vbCrLf
sConsulta = sConsulta & "DECLARE curIDIOMAS CURSOR LOCAL FOR" & vbCrLf
sConsulta = sConsulta & "SELECT COD FROM IDIOMAS WITH(NOLOCK) WHERE APLICACION=1" & vbCrLf
sConsulta = sConsulta & "OPEN curIDIOMAS" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curIDIOMAS INTO @IDIOMA" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SQL=''" & vbCrLf
sConsulta = sConsulta & "SET @SQLCAMPOSINSERT=''" & vbCrLf
sConsulta = sConsulta & "SET @EJECUTARSQL=0" & vbCrLf
sConsulta = sConsulta & "SET @LINEA_ANT=1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @INSERT = 1 AND @CONTRATO = 1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'INSERT INTO ' + @IDIOMA + N'_CONTR_FILTRO1 VALUES (@INSTANCIA_L)'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA_L INT', @INSTANCIA_L=@INSTANCIA_L" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @INSERT = 1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SQLCAMPOSINSERT=N'INSERT INTO '+@IDIOMA+N'_'+@TABLA+N' (INSTANCIA,'" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N' VALUES (@INSTANCIA_L,'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=N' UPDATE ' + @IDIOMA + N'_' + @TABLA  + N' SET '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE curSOLICITUDES_CAMPOS CURSOR LOCAL FOR" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT CD.CAMPO_ORIGEN, CD.SUBTIPO, CD.TIPO_CAMPO_GS, C.VALOR_TEXT, C.VALOR_NUM," & vbCrLf
sConsulta = sConsulta & "C.VALOR_FEC, C.VALOR_BOOL, C.VALOR_TEXT_COD, C.VALOR_TEXT_DEN, CD.INTRO, CD.ID_ATRIB_GS," & vbCrLf
sConsulta = sConsulta & "CD.TABLA_EXTERNA, C.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "FROM COPIA_CAMPO_DEF CD WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_CAMPO C WITH(NOLOCK) ON CD.ID=C.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "INNER JOIN INSTANCIA I WITH(NOLOCK) ON C.INSTANCIA=I.ID AND C.NUM_VERSION=I.ULT_VERSION" & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_GRUPO G WITH (NOLOCK) ON I.ID=G.INSTANCIA" & vbCrLf
sConsulta = sConsulta & "INNER JOIN FORM_GRUPO G2 WITH (NOLOCK) ON G.GRUPO_ORIGEN=G2.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN FORMULARIO F WITH(NOLOCK) ON F.ID=G2.FORMULARIO" & vbCrLf
sConsulta = sConsulta & "INNER JOIN FORM_GRUPO FG WITH (NOLOCK) ON F.ID=FG.FORMULARIO" & vbCrLf
sConsulta = sConsulta & "INNER JOIN FORM_CAMPO FC WITH (NOLOCK) ON FG.ID = FC.GRUPO" & vbCrLf
sConsulta = sConsulta & "WHERE CD.ES_SUBCAMPO=0 AND CD.SUBTIPO<>9 AND (CD.TIPO_CAMPO_GS<>106 OR CD.TIPO_CAMPO_GS IS NULL)" & vbCrLf
sConsulta = sConsulta & "AND CD.CAMPO_ORIGEN = FC.ID AND (CD.SUBTIPO=FC.SUBTIPO" & vbCrLf
sConsulta = sConsulta & "OR (CD.SUBTIPO=5 AND FC.SUBTIPO IN (6,7)) OR (CD.SUBTIPO=6 AND FC.SUBTIPO=7))  --si la definici?n del campo ha cambiado de texto corto a texto medio o largo, o de texto medio a texto largo" & vbCrLf
sConsulta = sConsulta & "AND CD.CAMPO_ORIGEN NOT IN (SELECT ID_CAMPO FROM #G1)  -- campos que se han a?adido en el formulario pero no se ha hecho todavia ALTER en las tablas de filtros (BatchUpdates)" & vbCrLf
sConsulta = sConsulta & "AND I.ID=@INSTANCIA_L" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "OPEN curSOLICITUDES_CAMPOS" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curSOLICITUDES_CAMPOS INTO @CAMPO_ORIGEN, @SUBTIPO, @TIPO_CAMPO_GS, @VALOR_TEXT, @VALOR_NUM, @VALOR_FEC, @VALOR_BOOL, @VALOR_TEXT_COD, @VALOR_TEXT_DEN, @INTRO, @ID_ATRIB_GS,@TABLA_EXTERNA, @CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @EJECUTARSQL=1" & vbCrLf
sConsulta = sConsulta & "--Reemplazamos las comillas simples en valor_text y valor_text_den" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @VALOR_TEXT = REPLACE(@VALOR_TEXT,'''','''''')" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT_DEN IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SET @VALOR_TEXT_DEN = REPLACE(@VALOR_TEXT_DEN,'''','''''')" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "   IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "       SET @SQLCAMPOSINSERT = @SQLCAMPOSINSERT + 'C_' + CONVERT(NVARCHAR,@CAMPO_ORIGEN)  + ','" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "       SET @SQLCAMPOSINSERT = @SQLCAMPOSINSERT + N'C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N','" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = @SQL + 'C_' + CONVERT(NVARCHAR,@CAMPO_ORIGEN) + '='" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = @SQL +N'C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'='" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "----------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @SUBTIPO = 3 -- tipo fecha" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_FEC IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'CONVERT(DATETIME,''' + CONVERT(VARCHAR,@VALOR_FEC) + ''')'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @SUBTIPO = 4 -- tipo booleano" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_BOOL IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + CONVERT(NVARCHAR,@VALOR_BOOL)" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @SUBTIPO = 8 -- tipo archivo" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'''' + @VALOR_TEXT + N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @TIPO_CAMPO_GS IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @TABLA_EXTERNA IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @SUBTIPO=2 -- tipo num?rico" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_NUM IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + CONVERT(NVARCHAR,@VALOR_NUM)" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE    -- tipo texto o tipo lista texto" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1 AND @QA=0 AND @CONTRATO=0" & vbCrLf
sConsulta = sConsulta & "   SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "IF @INTRO=1 AND @VALOR_NUM IS NOT NULL --tipo lista texto" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @ID_ATRIB_GS IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @IDIOMA = 'SPA'" & vbCrLf
sConsulta = sConsulta & "SELECT @DENOMINACION=VALOR_TEXT_SPA" & vbCrLf
sConsulta = sConsulta & "FROM COPIA_CAMPO_VALOR_LISTA WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE CAMPO_DEF=CONVERT(VARCHAR,@CAMPO_DEF) AND ORDEN=CONVERT(VARCHAR,@VALOR_NUM)" & vbCrLf
sConsulta = sConsulta & "IF @IDIOMA = 'ENG'" & vbCrLf
sConsulta = sConsulta & "SELECT @DENOMINACION=VALOR_TEXT_ENG" & vbCrLf
sConsulta = sConsulta & "FROM COPIA_CAMPO_VALOR_LISTA WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE CAMPO_DEF=CONVERT(VARCHAR,@CAMPO_DEF) AND ORDEN=CONVERT(VARCHAR,@VALOR_NUM)" & vbCrLf
sConsulta = sConsulta & "IF @IDIOMA = 'GER'" & vbCrLf
sConsulta = sConsulta & "SELECT @DENOMINACION=VALOR_TEXT_GER" & vbCrLf
sConsulta = sConsulta & "FROM COPIA_CAMPO_VALOR_LISTA WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE CAMPO_DEF = CONVERT(VARCHAR,@CAMPO_DEF) AND ORDEN=CONVERT(VARCHAR,@VALOR_NUM)" & vbCrLf
sConsulta = sConsulta & "IF @IDIOMA = 'FRA'" & vbCrLf
sConsulta = sConsulta & "SELECT @DENOMINACION=VALOR_TEXT_FRA" & vbCrLf
sConsulta = sConsulta & "FROM COPIA_CAMPO_VALOR_LISTA WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE CAMPO_DEF = CONVERT(VARCHAR,@CAMPO_DEF) AND ORDEN=CONVERT(VARCHAR,@VALOR_NUM)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @DENOMINACION IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + '''' + replace(@DENOMINACION,'''','''''') + ''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+REPLACE(@DENOMINACION,'''','''''')+''''+N',CONVERT(NVARCHAR,'+CONVERT(NVARCHAR,@VALOR_NUM)+N')'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+REPLACE(@DENOMINACION,'''','''''')+''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=CONVERT(NVARCHAR,'+CONVERT(NVARCHAR,@VALOR_NUM)+N')'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + N'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + N'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + '''' + @VALOR_TEXT + ''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + N''''+@VALOR_TEXT+N''''+N','+N''''+CONVERT(NVARCHAR,@VALOR_NUM)+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + N''''+@VALOR_TEXT+N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+ CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N''''+CONVERT(NVARCHAR,@VALOR_NUM)+N''''" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE -- tipo texto" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + '''' + @VALOR_TEXT + ''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + N''''+@VALOR_TEXT+N''''+N','+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + N''''+@VALOR_TEXT+N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+ CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + N'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @SUBTIPO=2 -- tipo num?rico" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_NUM IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + CONVERT(NVARCHAR,@VALOR_NUM)" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE    -- tipo texto o tipo lista texto" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1 AND @QA=0 AND @CONTRATO=0" & vbCrLf
sConsulta = sConsulta & "   SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT_DEN IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + '''' + @VALOR_TEXT_DEN + ''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+@VALOR_TEXT_DEN+N''''+N','+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+@VALOR_TEXT_DEN+N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_' +CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE -- campo del sistema" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @CAMPOTRATADO = 0" & vbCrLf
sConsulta = sConsulta & "---------------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "-- Formas de pago, Monedas, Familia de materiales, Unidades, Destinos, Pa?ses y Provincias" & vbCrLf
sConsulta = sConsulta & "-- FORMATO: <Idioma>|<Texto>#<Idioma>|<Texto>" & vbCrLf
sConsulta = sConsulta & "-- FORMATO MATERIALES: cod. ultimo nivel (VALOR_TEXT) +  <Idioma>|<Texto>#<Idioma>|<Texto>" & vbCrLf
sConsulta = sConsulta & "------------------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "IF @TIPO_CAMPO_GS = 101 OR @TIPO_CAMPO_GS = 102 OR @TIPO_CAMPO_GS = 103 OR @TIPO_CAMPO_GS = 105 OR @TIPO_CAMPO_GS = 109 OR @TIPO_CAMPO_GS = 107 OR @TIPO_CAMPO_GS = 108" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1 AND @QA=0 AND @CONTRATO=0" & vbCrLf
sConsulta = sConsulta & "   SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+ N'_COD,'" & vbCrLf
sConsulta = sConsulta & "SET @CAMPOTRATADO = 1" & vbCrLf
sConsulta = sConsulta & "-- Materiales" & vbCrLf
sConsulta = sConsulta & "IF @TIPO_CAMPO_GS = 103" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "-- Al hacer Len de valor_text me est? quitando los espacios en blanco de la derecha" & vbCrLf
sConsulta = sConsulta & "IF LEN(@VALOR_TEXT + '#')-1 = @LEN_GMN1 + @LEN_GMN2 + @LEN_GMN3 + @LEN_GMN4" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'''' + RTRIM(RIGHT(@VALOR_TEXT,@LEN_GMN4)) + N' - '" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF LEN(@VALOR_TEXT + '#')-1 = @LEN_GMN1 + @LEN_GMN2 + @LEN_GMN3" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'''' + RTRIM(RIGHT(@VALOR_TEXT,@LEN_GMN3)) + N' - '" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF LEN(@VALOR_TEXT + '#')-1 = @LEN_GMN1 + @LEN_GMN2" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'''' + RTRIM(RIGHT(@VALOR_TEXT,@LEN_GMN2)) + N' - '" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'''' + RTRIM(RIGHT(@VALOR_TEXT,@LEN_GMN1)) + N' - '" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- Todos" & vbCrLf
sConsulta = sConsulta & "SET @EMPIEZAIDIOMA=CHARINDEX(@IDIOMA + '|',@VALOR_TEXT_DEN)" & vbCrLf
sConsulta = sConsulta & "IF (@EMPIEZAIDIOMA) > 0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @ACABADENOMINACION = CHARINDEX('#',@VALOR_TEXT_DEN,@EMPIEZAIDIOMA)" & vbCrLf
sConsulta = sConsulta & "SET @EMPIEZADENOMINACION = CHARINDEX('|',@VALOR_TEXT_DEN,@EMPIEZAIDIOMA)+1" & vbCrLf
sConsulta = sConsulta & "IF @TIPO_CAMPO_GS <> 103" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+''''" & vbCrLf
sConsulta = sConsulta & "IF @ACABADENOMINACION>0" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+SUBSTRING(@VALOR_TEXT_DEN,@EMPIEZADENOMINACION,@ACABADENOMINACION-@EMPIEZADENOMINACION) + ''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+RIGHT(@VALOR_TEXT_DEN,LEN(@VALOR_TEXT_DEN)-@EMPIEZADENOMINACION+1) + ''''" & vbCrLf
sConsulta = sConsulta & "IF @QA=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N','+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N',C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT IS NOT NULL AND @TIPO_CAMPO_GS = 103" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+N','+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "-- Proveedores, Departamentos, Organizaciones de Compras, Centros,Proveedor ERP" & vbCrLf
sConsulta = sConsulta & "-- FORMATO: C?digo (VALOR_TEXT) - Denominaci?n (VALOR_TEXT_DEN)" & vbCrLf
sConsulta = sConsulta & "--------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "IF @TIPO_CAMPO_GS = 11 OR @TIPO_CAMPO_GS = 100  OR @TIPO_CAMPO_GS = 122 OR @TIPO_CAMPO_GS = 123 OR @TIPO_CAMPO_GS = 124 OR @TIPO_CAMPO_GS = 143" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @CAMPOTRATADO = 1" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1 AND @QA=0 AND @CONTRATO=0" & vbCrLf
sConsulta = sConsulta & "   SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + '''' + @VALOR_TEXT + ' - ' + @VALOR_TEXT_DEN + ''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+@VALOR_TEXT+N' - '+@VALOR_TEXT_DEN+N''''+N','+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+@VALOR_TEXT+N' - '+@VALOR_TEXT_DEN+N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "-- Centros de coste, Partidas presupuestarias, Activos" & vbCrLf
sConsulta = sConsulta & "-- FORMATO: cod. ultimo nivel (VALOR_TEXT) +  <Idioma>|<Texto>#<Idioma>|<Texto>" & vbCrLf
sConsulta = sConsulta & "--------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "IF @TIPO_CAMPO_GS = 129 OR @TIPO_CAMPO_GS = 130 OR @TIPO_CAMPO_GS = 131 OR @TIPO_CAMPO_GS=132" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @CAMPOTRATADO=1" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1 AND @QA=0 AND @CONTRATO=0" & vbCrLf
sConsulta = sConsulta & "   SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SEPARADOR = CHARINDEX('#',@VALOR_TEXT,1)" & vbCrLf
sConsulta = sConsulta & "WHILE @SEPARADOR > 0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @VALOR_TEXT = RIGHT(@VALOR_TEXT,LEN(@VALOR_TEXT)-@SEPARADOR)" & vbCrLf
sConsulta = sConsulta & "SET @SEPARADOR = CHARINDEX('#',@VALOR_TEXT,1)" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SEPARADOR = CHARINDEX('|',@VALOR_TEXT,1)" & vbCrLf
sConsulta = sConsulta & "WHILE @SEPARADOR > 0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @VALOR_TEXT = RIGHT(@VALOR_TEXT,LEN(@VALOR_TEXT)-@SEPARADOR)" & vbCrLf
sConsulta = sConsulta & "SET @SEPARADOR = CHARINDEX('|',@VALOR_TEXT,1)" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + '''' + @VALOR_TEXT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Denominaci?n en el idioma" & vbCrLf
sConsulta = sConsulta & "SET @EMPIEZAIDIOMA=CHARINDEX(@IDIOMA + '|',@VALOR_TEXT_DEN)" & vbCrLf
sConsulta = sConsulta & "IF (@EMPIEZAIDIOMA) > 0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @ACABADENOMINACION = CHARINDEX('#',@VALOR_TEXT_DEN,@EMPIEZAIDIOMA)" & vbCrLf
sConsulta = sConsulta & "SET @EMPIEZADENOMINACION = CHARINDEX('|',@VALOR_TEXT_DEN,@EMPIEZAIDIOMA)+1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ACABADENOMINACION>0" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ' - ' + SUBSTRING(@VALOR_TEXT_DEN,@EMPIEZADENOMINACION,@ACABADENOMINACION-@EMPIEZADENOMINACION)" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ' - ' + RIGHT(@VALOR_TEXT_DEN,LEN(@VALOR_TEXT_DEN)-@EMPIEZADENOMINACION+1)" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+N','+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "-- Personas, Comprador" & vbCrLf
sConsulta = sConsulta & "---------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "IF @TIPO_CAMPO_GS = 115 OR @TIPO_CAMPO_GS = 144" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @CAMPOTRATADO = 1" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1 AND @QA=0 AND @CONTRATO=0" & vbCrLf
sConsulta = sConsulta & "   SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT_DEN IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + '''' + @VALOR_TEXT_DEN + ''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+@VALOR_TEXT_DEN+N''''+N','+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+@VALOR_TEXT_DEN+N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "-- Almacen" & vbCrLf
sConsulta = sConsulta & "---------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "IF @TIPO_CAMPO_GS = 125" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @CAMPOTRATADO = 1" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1 AND @QA=0 AND @CONTRATO=0" & vbCrLf
sConsulta = sConsulta & "   SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT_DEN IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + '''' + @VALOR_TEXT_DEN + ''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+@VALOR_TEXT_DEN+N''''+N','+N''''+CONVERT(NVARCHAR,@VALOR_NUM)+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+@VALOR_TEXT_DEN+N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N''''+CONVERT(NVARCHAR,@VALOR_NUM)+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "------------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "-- Cod. Art?culo (viejo), Cod. Art?culo (nuevo), Denominaci?n del art?culo y Unidades Organizativas" & vbCrLf
sConsulta = sConsulta & "------------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "IF @TIPO_CAMPO_GS = 104  OR @TIPO_CAMPO_GS = 118 OR @TIPO_CAMPO_GS = 119 OR @TIPO_CAMPO_GS = 121" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @CAMPOTRATADO = 1" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1 AND @QA=0 AND @CONTRATO=0" & vbCrLf
sConsulta = sConsulta & "   SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + '''' + @VALOR_TEXT + ''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+@VALOR_TEXT+N''''+N','+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+@VALOR_TEXT +N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "---------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "-- Presupuestos" & vbCrLf
sConsulta = sConsulta & "-- FORMATO: Ej 2005 - PC-002  (50.00%); 2005 - PC-001 (50.00%);" & vbCrLf
sConsulta = sConsulta & "--  VALOR_TEXT_DEN: 2005 - PC-002#2005 - PC-001" & vbCrLf
sConsulta = sConsulta & "--  VALOR_TEXT: 1_11_0.5#1_10_0.5" & vbCrLf
sConsulta = sConsulta & "---------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "IF @TIPO_CAMPO_GS = 110  OR @TIPO_CAMPO_GS = 111 OR @TIPO_CAMPO_GS = 112 OR @TIPO_CAMPO_GS = 113" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @CAMPOTRATADO = 1" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1 AND @QA=0 AND @CONTRATO=0" & vbCrLf
sConsulta = sConsulta & "   SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "SET @PRESUPUESTOS = ''" & vbCrLf
sConsulta = sConsulta & "SET @SEPARADOR_DEN = CHARINDEX('#',@VALOR_TEXT_DEN)" & vbCrLf
sConsulta = sConsulta & "WHILE @SEPARADOR_DEN > 0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @PRESUPUESTOS = @PRESUPUESTOS + SUBSTRING(@VALOR_TEXT_DEN,1,@SEPARADOR_DEN-1)" & vbCrLf
sConsulta = sConsulta & "SET @VALOR_TEXT_DEN = RIGHT(@VALOR_TEXT_DEN,LEN(@VALOR_TEXT_DEN)-@SEPARADOR_DEN)" & vbCrLf
sConsulta = sConsulta & "SET @PORCENTAJE = SUBSTRING(@VALOR_TEXT,1,CHARINDEX('#',@VALOR_TEXT)-1)" & vbCrLf
sConsulta = sConsulta & "SET @SEPARADOR_PORCENTAJE = 1" & vbCrLf
sConsulta = sConsulta & "WHILE @SEPARADOR_PORCENTAJE > 0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @PORCENTAJE = RIGHT(@PORCENTAJE,LEN(@PORCENTAJE)-@SEPARADOR_PORCENTAJE)" & vbCrLf
sConsulta = sConsulta & "SET @SEPARADOR_PORCENTAJE = CHARINDEX('_',@PORCENTAJE,@SEPARADOR_PORCENTAJE+1)" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "SET @PRESUPUESTOS = @PRESUPUESTOS + '(' + CONVERT(VARCHAR,CONVERT(DECIMAL(3,2),@PORCENTAJE)*100) + '%); '" & vbCrLf
sConsulta = sConsulta & "SET @VALOR_TEXT = RIGHT(@VALOR_TEXT,LEN(@VALOR_TEXT)-CHARINDEX('#',@VALOR_TEXT))" & vbCrLf
sConsulta = sConsulta & "SET @SEPARADOR_DEN = CHARINDEX('#',@VALOR_TEXT_DEN)" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "SET @PORCENTAJE = @VALOR_TEXT" & vbCrLf
sConsulta = sConsulta & "SET @SEPARADOR_PORCENTAJE = 1" & vbCrLf
sConsulta = sConsulta & "WHILE @SEPARADOR_PORCENTAJE > 0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @PORCENTAJE = RIGHT(@PORCENTAJE,LEN(@PORCENTAJE)-@SEPARADOR_PORCENTAJE)" & vbCrLf
sConsulta = sConsulta & "SET @SEPARADOR_PORCENTAJE = CHARINDEX('_',@PORCENTAJE,@SEPARADOR_PORCENTAJE+1)" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @PRESUPUESTOS = @PRESUPUESTOS + @VALOR_TEXT_DEN + '(' + CAST(CONVERT(FLOAT,@PORCENTAJE)*100 AS VARCHAR) + '%);'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @PRESUPUESTOS <> ''" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + '''' + @PRESUPUESTOS + ''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'''' + @PRESUPUESTOS + N'''' + N',' + N'''' + @VALOR_TEXT + N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'''' + @PRESUPUESTOS + N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL,'+N'C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CAMPOTRATADO = 0 -- Resto" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @SUBTIPO = 3 -- tipo fecha" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_FEC IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + '''' + @VALOR_FEC + ''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @SUBTIPO = 4 -- tipo booleano" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_BOOL IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + CONVERT(VARCHAR,@VALOR_BOOL)" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @SUBTIPO=2 -- tipo num?rico" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_NUM IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + CONVERT(VARCHAR,@VALOR_NUM)" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE    -- tipo texto" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1 AND @QA=0 AND @CONTRATO=0" & vbCrLf
sConsulta = sConsulta & "   SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + '''' + @VALOR_TEXT + ''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+@VALOR_TEXT+N''''+N','+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+''''+@VALOR_TEXT+''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+ N'_COD='+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+ N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ','" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curSOLICITUDES_CAMPOS INTO @CAMPO_ORIGEN, @SUBTIPO, @TIPO_CAMPO_GS, @VALOR_TEXT, @VALOR_NUM, @VALOR_FEC, @VALOR_BOOL, @VALOR_TEXT_COD, @VALOR_TEXT_DEN, @INTRO, @ID_ATRIB_GS,@TABLA_EXTERNA, @CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CLOSE curSOLICITUDES_CAMPOS" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curSOLICITUDES_CAMPOS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @EJECUTARSQL=1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @INSERT = 1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = LEFT(@SQLCAMPOSINSERT,LEN(@SQLCAMPOSINSERT)-1) + ') ' + LEFT(@SQL,LEN(@SQL)-1) + ')'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = LEFT(@SQL,LEN(@SQL)-1) + ' WHERE INSTANCIA = @INSTANCIA_L'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL=N'IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = ''dbo'' AND  TABLE_NAME='''+@IDIOMA+N'_'+@TABLA+''')) BEGIN '+@SQL+N' END'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA_L INT', @INSTANCIA_L=@INSTANCIA_L" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--*********************************" & vbCrLf
sConsulta = sConsulta & "--*********************************" & vbCrLf
sConsulta = sConsulta & "-- AHORA LOS CAMPOS DE LOS DESGLOSES (SI NO ES QA NI CONTRATO)" & vbCrLf
sConsulta = sConsulta & "--**********************************" & vbCrLf
sConsulta = sConsulta & "IF @QA=0 AND @CONTRATO=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SQL=''" & vbCrLf
sConsulta = sConsulta & "SET @SQLCAMPOSINSERT=''" & vbCrLf
sConsulta = sConsulta & "SET @EJECUTARSQL=0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Pongo insert a 1 para que siempre inserte, ya que si de una etapa a otra se crean nuevas lineas de desglose s?lo estar?a haciendo update de las lineas existentes" & vbCrLf
sConsulta = sConsulta & "--Ahora elimino todas las l?neas y vuelvo a insertar las que haya en ese momento, as? tambi?n controlo si se elimina alguna l?nea" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLDELETE NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @INSERT_ORIGINAL TINYINT" & vbCrLf
sConsulta = sConsulta & "SET @SQLDELETE=''" & vbCrLf
sConsulta = sConsulta & "SET @INSERT_ORIGINAL=@INSERT" & vbCrLf
sConsulta = sConsulta & "SET @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQLDELETE='DELETE '+@IDIOMA+N'_'+@TABLA+N'_DESGLOSE WHERE FORM_INSTANCIA=@INSTANCIA_L'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLDELETE, N'@INSTANCIA_L INT',@INSTANCIA_L=@INSTANCIA_L" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @INSERT = 1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SQLCAMPOSINSERT=N'INSERT INTO '+@IDIOMA+N'_'+@TABLA+N'_DESGLOSE (FORM_INSTANCIA,LINEA,DESGLOSE,'" & vbCrLf
sConsulta = sConsulta & "SET @SQL=N' VALUES (@INSTANCIA_L,@LINEA,'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=N' UPDATE '+@IDIOMA+N'_'+@TABLA+N'_DESGLOSE SET '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE curSOLICITUDES_CAMPOS_DESGLOSE CURSOR LOCAL FOR" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT CLD.LINEA,CCD.CAMPO_ORIGEN,CCD.SUBTIPO,CCD.TIPO_CAMPO_GS,CLD.VALOR_TEXT,CLD.VALOR_NUM,CLD.VALOR_FEC,CLD.VALOR_BOOL," & vbCrLf
sConsulta = sConsulta & "CLD.VALOR_TEXT_COD,CLD.VALOR_TEXT_DEN,CCD.INTRO,CCD.ID_ATRIB_GS,CCD.TABLA_EXTERNA,CC.COPIA_CAMPO_DEF," & vbCrLf
sConsulta = sConsulta & "D.CAMPO_PADRE" & vbCrLf
sConsulta = sConsulta & "FROM COPIA_CAMPO_DEF CCD WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_CAMPO CC WITH(NOLOCK) ON CCD.ID=CC.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "INNER JOIN INSTANCIA I WITH(NOLOCK) ON CC.INSTANCIA=I.ID AND CC.NUM_VERSION=I.ULT_VERSION" & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_GRUPO G WITH (NOLOCK) ON I.ID=G.INSTANCIA" & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_LINEA_DESGLOSE CLD WITH(NOLOCK) ON CLD.CAMPO_HIJO=CC.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN FORM_GRUPO G2 WITH (NOLOCK) ON G.GRUPO_ORIGEN=G2.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN FORMULARIO F WITH(NOLOCK) ON F.ID=G2.FORMULARIO" & vbCrLf
sConsulta = sConsulta & "INNER JOIN FORM_GRUPO FG WITH (NOLOCK) ON F.ID=FG.FORMULARIO" & vbCrLf
sConsulta = sConsulta & "INNER JOIN FORM_CAMPO FC WITH (NOLOCK) ON FG.ID=FC.GRUPO" & vbCrLf
sConsulta = sConsulta & "INNER JOIN DESGLOSE D WITH(NOLOCK) ON D.CAMPO_HIJO=FC.ID" & vbCrLf
sConsulta = sConsulta & "WHERE CCD.ES_SUBCAMPO=1 AND CCD.SUBTIPO<>9 AND (CCD.TIPO_CAMPO_GS<>106 OR CCD.TIPO_CAMPO_GS IS NULL)" & vbCrLf
sConsulta = sConsulta & "AND CCD.CAMPO_ORIGEN=FC.ID" & vbCrLf
sConsulta = sConsulta & "AND (CCD.SUBTIPO=FC.SUBTIPO OR (CCD.SUBTIPO=5 AND FC.SUBTIPO IN (6,7)) OR (CCD.SUBTIPO=6 AND FC.SUBTIPO=7))--si la definici?n del campo ha cambiado de texto corto a texto medio o largo, o de texto medio a texto largo" & vbCrLf
sConsulta = sConsulta & "AND CCD.CAMPO_ORIGEN NOT IN (SELECT ID_CAMPO FROM #G1)  -- campos que se han a?adido en el formulario pero no se ha hecho todavia ALTER en las tablas de filtros (BatchUpdates)" & vbCrLf
sConsulta = sConsulta & "AND I.ID=@INSTANCIA_L" & vbCrLf
sConsulta = sConsulta & "ORDER BY CAMPO_PADRE,LINEA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "OPEN curSOLICITUDES_CAMPOS_DESGLOSE" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curSOLICITUDES_CAMPOS_DESGLOSE INTO @LINEA,@CAMPO_ORIGEN,@SUBTIPO,@TIPO_CAMPO_GS,@VALOR_TEXT," & vbCrLf
sConsulta = sConsulta & "@VALOR_NUM,@VALOR_FEC,@VALOR_BOOL,@VALOR_TEXT_COD,@VALOR_TEXT_DEN,@INTRO,@ID_ATRIB_GS,@TABLA_EXTERNA," & vbCrLf
sConsulta = sConsulta & "@CAMPO_DEF,@CAMPO_PADRE_DESGLOSE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @CAMPO_PADRE_DESGLOSE_ANT=@CAMPO_PADRE_DESGLOSE" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLAUX NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLCAMPOSINSERTAUX NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "SET @SQLAUX=@SQL" & vbCrLf
sConsulta = sConsulta & "SET @SQLCAMPOSINSERTAUX=@SQLCAMPOSINSERT" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N','" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @EJECUTARSQL=1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @LINEA<>@LINEA_ANT OR @CAMPO_PADRE_DESGLOSE<>@CAMPO_PADRE_DESGLOSE_ANT" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @EJECUTARSQL=1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @INSERT = 1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=LEFT(@SQLCAMPOSINSERT,LEN(@SQLCAMPOSINSERT)-1) + ') '+LEFT(@SQL,LEN(@SQL)-1)+')'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=LEFT(@SQL,LEN(@SQL)-1)+' WHERE FORM_INSTANCIA=@INSTANCIA_L AND DESGLOSE=@CAMPO_PADRE_DESGLOSE AND LINEA=@LINEA '" & vbCrLf
sConsulta = sConsulta & "SET @SQL=N'IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = ''dbo'' AND  TABLE_NAME='''+@IDIOMA+N'_'+@TABLA+N'_DESGLOSE'')) BEGIN '+@SQL+N' END'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA_L INT,@CAMPO_PADRE_DESGLOSE INT,@LINEA INT',@INSTANCIA_L=@INSTANCIA_L,@CAMPO_PADRE_DESGLOSE=@CAMPO_PADRE_DESGLOSE_ANT,@LINEA=@LINEA_ANT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQLAUX" & vbCrLf
sConsulta = sConsulta & "SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERTAUX" & vbCrLf
sConsulta = sConsulta & "SET @LINEA_ANT=@LINEA" & vbCrLf
sConsulta = sConsulta & "SET @CAMPO_PADRE_DESGLOSE_ANT=@CAMPO_PADRE_DESGLOSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N','" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "--Reemplazamos las comillas simples en valor_text y valor_text_den" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @VALOR_TEXT = REPLACE(@VALOR_TEXT,'''','''''')" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT_DEN IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SET @VALOR_TEXT_DEN = REPLACE(@VALOR_TEXT_DEN,'''','''''')" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT + N'C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N','" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'='" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "----------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "IF @SUBTIPO = 3 -- tipo fecha" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_FEC IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'CONVERT(DATETIME,''' + CONVERT(VARCHAR,@VALOR_FEC) + ''')'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @SUBTIPO = 4 -- tipo booleano" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_BOOL IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + CONVERT(NVARCHAR,@VALOR_BOOL)" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @SUBTIPO = 8 -- tipo archivo" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'''' + @VALOR_TEXT + N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @TIPO_CAMPO_GS IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @TABLA_EXTERNA IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @SUBTIPO=2 -- tipo num?rico" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_NUM IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + CONVERT(NVARCHAR,@VALOR_NUM)" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE    -- tipo texto o tipo lista texto" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "IF @INTRO=1 AND @VALOR_NUM IS NOT NULL --tipo lista texto" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @ID_ATRIB_GS IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @IDIOMA = 'SPA'" & vbCrLf
sConsulta = sConsulta & "SELECT @DENOMINACION=VALOR_TEXT_SPA" & vbCrLf
sConsulta = sConsulta & "FROM COPIA_CAMPO_VALOR_LISTA WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE CAMPO_DEF=CONVERT(VARCHAR,@CAMPO_DEF) AND ORDEN=CONVERT(VARCHAR,@VALOR_NUM)" & vbCrLf
sConsulta = sConsulta & "IF @IDIOMA = 'ENG'" & vbCrLf
sConsulta = sConsulta & "SELECT @DENOMINACION=VALOR_TEXT_ENG" & vbCrLf
sConsulta = sConsulta & "FROM COPIA_CAMPO_VALOR_LISTA WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE CAMPO_DEF=CONVERT(VARCHAR,@CAMPO_DEF) AND ORDEN=CONVERT(VARCHAR,@VALOR_NUM)" & vbCrLf
sConsulta = sConsulta & "IF @IDIOMA = 'GER'" & vbCrLf
sConsulta = sConsulta & "SELECT @DENOMINACION=VALOR_TEXT_GER" & vbCrLf
sConsulta = sConsulta & "FROM COPIA_CAMPO_VALOR_LISTA WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE CAMPO_DEF = CONVERT(VARCHAR,@CAMPO_DEF) AND ORDEN=CONVERT(VARCHAR,@VALOR_NUM)" & vbCrLf
sConsulta = sConsulta & "IF @IDIOMA = 'FRA'" & vbCrLf
sConsulta = sConsulta & "SELECT @DENOMINACION=VALOR_TEXT_FRA" & vbCrLf
sConsulta = sConsulta & "FROM COPIA_CAMPO_VALOR_LISTA WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE CAMPO_DEF = CONVERT(VARCHAR,@CAMPO_DEF) AND ORDEN=CONVERT(VARCHAR,@VALOR_NUM)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @DENOMINACION IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+REPLACE(@DENOMINACION,'''','''''')+''''+N',CONVERT(NVARCHAR,'+CONVERT(NVARCHAR,@VALOR_NUM)+N')'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+REPLACE(@DENOMINACION,'''','''''')+''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=CONVERT(NVARCHAR,'+CONVERT(NVARCHAR,@VALOR_NUM)+N')'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + N'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + N'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + N''''+@VALOR_TEXT+N''''+N','+N''''+CONVERT(NVARCHAR,@VALOR_NUM)+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + N''''+@VALOR_TEXT+N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+ CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N''''+CONVERT(NVARCHAR,@VALOR_NUM)+N''''" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE -- tipo texto" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + N''''+@VALOR_TEXT+N''''+N','+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + N''''+@VALOR_TEXT+N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+ CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + N'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @SUBTIPO=2 -- tipo num?rico" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_NUM IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + CONVERT(NVARCHAR,@VALOR_NUM)" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE    -- tipo texto o tipo lista texto" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT_DEN IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+@VALOR_TEXT_DEN+N''''+N','+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+@VALOR_TEXT_DEN+N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_' +CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE -- campo del sistema" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @CAMPOTRATADO = 0" & vbCrLf
sConsulta = sConsulta & "---------------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "-- Formas de pago, Monedas, Familia de materiales, Unidades, Destinos, Pa?ses y Provincias" & vbCrLf
sConsulta = sConsulta & "-- FORMATO: <Idioma>|<Texto>#<Idioma>|<Texto>" & vbCrLf
sConsulta = sConsulta & "-- FORMATO MATERIALES: cod. ultimo nivel (VALOR_TEXT) +  <Idioma>|<Texto>#<Idioma>|<Texto>" & vbCrLf
sConsulta = sConsulta & "------------------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "IF @TIPO_CAMPO_GS = 101 OR @TIPO_CAMPO_GS = 102 OR @TIPO_CAMPO_GS = 103 OR @TIPO_CAMPO_GS = 105 OR @TIPO_CAMPO_GS = 109 OR @TIPO_CAMPO_GS = 107 OR @TIPO_CAMPO_GS = 108" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+ N'_COD,'" & vbCrLf
sConsulta = sConsulta & "SET @CAMPOTRATADO = 1" & vbCrLf
sConsulta = sConsulta & "-- Materiales" & vbCrLf
sConsulta = sConsulta & "IF @TIPO_CAMPO_GS = 103" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "-- Al hacer Len de valor_text me est? quitando los espacios en blanco de la derecha" & vbCrLf
sConsulta = sConsulta & "IF LEN(@VALOR_TEXT + '#')-1 = @LEN_GMN1 + @LEN_GMN2 + @LEN_GMN3 + @LEN_GMN4" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'''' + RTRIM(RIGHT(@VALOR_TEXT,@LEN_GMN4)) + N' - '" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF LEN(@VALOR_TEXT + '#')-1 = @LEN_GMN1 + @LEN_GMN2 + @LEN_GMN3" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'''' + RTRIM(RIGHT(@VALOR_TEXT,@LEN_GMN3)) + N' - '" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF LEN(@VALOR_TEXT + '#')-1 = @LEN_GMN1 + @LEN_GMN2" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'''' + RTRIM(RIGHT(@VALOR_TEXT,@LEN_GMN2)) + N' - '" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'''' + RTRIM(RIGHT(@VALOR_TEXT,@LEN_GMN1)) + N' - '" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- Todos" & vbCrLf
sConsulta = sConsulta & "SET @EMPIEZAIDIOMA=CHARINDEX(@IDIOMA + '|',@VALOR_TEXT_DEN)" & vbCrLf
sConsulta = sConsulta & "IF (@EMPIEZAIDIOMA) > 0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @ACABADENOMINACION = CHARINDEX('#',@VALOR_TEXT_DEN,@EMPIEZAIDIOMA)" & vbCrLf
sConsulta = sConsulta & "SET @EMPIEZADENOMINACION = CHARINDEX('|',@VALOR_TEXT_DEN,@EMPIEZAIDIOMA)+1" & vbCrLf
sConsulta = sConsulta & "IF @TIPO_CAMPO_GS <> 103" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+''''" & vbCrLf
sConsulta = sConsulta & "IF @ACABADENOMINACION>0" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+SUBSTRING(@VALOR_TEXT_DEN,@EMPIEZADENOMINACION,@ACABADENOMINACION-@EMPIEZADENOMINACION) + ''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+RIGHT(@VALOR_TEXT_DEN,LEN(@VALOR_TEXT_DEN)-@EMPIEZADENOMINACION+1) + ''''" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N','+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N',C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT IS NOT NULL AND @TIPO_CAMPO_GS = 103" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+N','+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "-- Proveedores, Departamentos, Organizaciones de Compras, Centros" & vbCrLf
sConsulta = sConsulta & "-- FORMATO: C?digo (VALOR_TEXT) - Denominaci?n (VALOR_TEXT_DEN)" & vbCrLf
sConsulta = sConsulta & "--------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "IF @TIPO_CAMPO_GS = 11 OR @TIPO_CAMPO_GS = 100  OR @TIPO_CAMPO_GS = 122 OR @TIPO_CAMPO_GS = 123 OR @TIPO_CAMPO_GS = 124" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @CAMPOTRATADO = 1" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+@VALOR_TEXT+N' - '+@VALOR_TEXT_DEN+N''''+N','+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+@VALOR_TEXT+N' - '+@VALOR_TEXT_DEN+N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "-- Centros de coste, Partidas presupuestarias, Activos" & vbCrLf
sConsulta = sConsulta & "-- FORMATO: cod. ultimo nivel (VALOR_TEXT) +  <Idioma>|<Texto>#<Idioma>|<Texto>" & vbCrLf
sConsulta = sConsulta & "--------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "IF @TIPO_CAMPO_GS = 129 OR @TIPO_CAMPO_GS = 130 OR @TIPO_CAMPO_GS = 131 OR @TIPO_CAMPO_GS=132" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @CAMPOTRATADO=1" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SEPARADOR = CHARINDEX('#',@VALOR_TEXT,1)" & vbCrLf
sConsulta = sConsulta & "WHILE @SEPARADOR > 0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @VALOR_TEXT = RIGHT(@VALOR_TEXT,LEN(@VALOR_TEXT)-@SEPARADOR)" & vbCrLf
sConsulta = sConsulta & "SET @SEPARADOR = CHARINDEX('#',@VALOR_TEXT,1)" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SEPARADOR = CHARINDEX('|',@VALOR_TEXT,1)" & vbCrLf
sConsulta = sConsulta & "WHILE @SEPARADOR > 0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @VALOR_TEXT = RIGHT(@VALOR_TEXT,LEN(@VALOR_TEXT)-@SEPARADOR)" & vbCrLf
sConsulta = sConsulta & "SET @SEPARADOR = CHARINDEX('|',@VALOR_TEXT,1)" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + '''' + @VALOR_TEXT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Denominaci?n en el idioma" & vbCrLf
sConsulta = sConsulta & "SET @EMPIEZAIDIOMA=CHARINDEX(@IDIOMA + '|',@VALOR_TEXT_DEN)" & vbCrLf
sConsulta = sConsulta & "IF (@EMPIEZAIDIOMA) > 0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @ACABADENOMINACION = CHARINDEX('#',@VALOR_TEXT_DEN,@EMPIEZAIDIOMA)" & vbCrLf
sConsulta = sConsulta & "SET @EMPIEZADENOMINACION = CHARINDEX('|',@VALOR_TEXT_DEN,@EMPIEZAIDIOMA)+1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ACABADENOMINACION>0" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ' - ' + SUBSTRING(@VALOR_TEXT_DEN,@EMPIEZADENOMINACION,@ACABADENOMINACION-@EMPIEZADENOMINACION)" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ' - ' + RIGHT(@VALOR_TEXT_DEN,LEN(@VALOR_TEXT_DEN)-@EMPIEZADENOMINACION+1)" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+N','+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "-- Personas, Comprador" & vbCrLf
sConsulta = sConsulta & "---------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "IF @TIPO_CAMPO_GS = 115 OR @TIPO_CAMPO_GS=144" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @CAMPOTRATADO = 1" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT_DEN IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+@VALOR_TEXT_DEN+N''''+N','+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+@VALOR_TEXT_DEN+N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "-- Almacen" & vbCrLf
sConsulta = sConsulta & "---------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "IF @TIPO_CAMPO_GS = 125" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @CAMPOTRATADO = 1" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT_DEN IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+@VALOR_TEXT_DEN+N''''+N','+N''''+CONVERT(NVARCHAR,@VALOR_NUM)+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+@VALOR_TEXT_DEN+N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N''''+CONVERT(NVARCHAR,@VALOR_NUM)+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "------------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "-- Cod. Art?culo (viejo), Cod. Art?culo (nuevo), Denominaci?n del art?culo y Unidades Organizativas" & vbCrLf
sConsulta = sConsulta & "------------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "IF @TIPO_CAMPO_GS = 104  OR @TIPO_CAMPO_GS = 118 OR @TIPO_CAMPO_GS = 119 OR @TIPO_CAMPO_GS = 121" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @CAMPOTRATADO = 1" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+@VALOR_TEXT+N''''+N','+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+@VALOR_TEXT +N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "---------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "-- Presupuestos" & vbCrLf
sConsulta = sConsulta & "-- FORMATO: Ej 2005 - PC-002  (50.00%); 2005 - PC-001 (50.00%);" & vbCrLf
sConsulta = sConsulta & "--  VALOR_TEXT_DEN: 2005 - PC-002#2005 - PC-001" & vbCrLf
sConsulta = sConsulta & "--  VALOR_TEXT: 1_11_0.5#1_10_0.5" & vbCrLf
sConsulta = sConsulta & "---------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "IF @TIPO_CAMPO_GS = 110  OR @TIPO_CAMPO_GS = 111 OR @TIPO_CAMPO_GS = 112 OR @TIPO_CAMPO_GS = 113" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @CAMPOTRATADO = 1" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "SET @PRESUPUESTOS = ''" & vbCrLf
sConsulta = sConsulta & "SET @SEPARADOR_DEN = CHARINDEX('#',@VALOR_TEXT_DEN)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "WHILE @SEPARADOR_DEN > 0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @PRESUPUESTOS = @PRESUPUESTOS + SUBSTRING(@VALOR_TEXT_DEN,1,@SEPARADOR_DEN-1)" & vbCrLf
sConsulta = sConsulta & "SET @VALOR_TEXT_DEN = RIGHT(@VALOR_TEXT_DEN,LEN(@VALOR_TEXT_DEN)-@SEPARADOR_DEN)" & vbCrLf
sConsulta = sConsulta & "SET @PORCENTAJE = SUBSTRING(@VALOR_TEXT,1,CHARINDEX('#',@VALOR_TEXT)-1)" & vbCrLf
sConsulta = sConsulta & "SET @SEPARADOR_PORCENTAJE = 1" & vbCrLf
sConsulta = sConsulta & "WHILE @SEPARADOR_PORCENTAJE > 0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @PORCENTAJE = RIGHT(@PORCENTAJE,LEN(@PORCENTAJE)-@SEPARADOR_PORCENTAJE)" & vbCrLf
sConsulta = sConsulta & "SET @SEPARADOR_PORCENTAJE = CHARINDEX('_',@PORCENTAJE,@SEPARADOR_PORCENTAJE+1)" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "SET @PRESUPUESTOS = @PRESUPUESTOS + '(' + CONVERT(VARCHAR,CONVERT(DECIMAL(3,2),@PORCENTAJE)*100) + '%); '" & vbCrLf
sConsulta = sConsulta & "SET @VALOR_TEXT = RIGHT(@VALOR_TEXT,LEN(@VALOR_TEXT)-CHARINDEX('#',@VALOR_TEXT))" & vbCrLf
sConsulta = sConsulta & "SET @SEPARADOR_DEN = CHARINDEX('#',@VALOR_TEXT_DEN)" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "SET @PORCENTAJE = @VALOR_TEXT" & vbCrLf
sConsulta = sConsulta & "SET @SEPARADOR_PORCENTAJE = 1" & vbCrLf
sConsulta = sConsulta & "WHILE @SEPARADOR_PORCENTAJE > 0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @PORCENTAJE = RIGHT(@PORCENTAJE,LEN(@PORCENTAJE)-@SEPARADOR_PORCENTAJE)" & vbCrLf
sConsulta = sConsulta & "SET @SEPARADOR_PORCENTAJE = CHARINDEX('_',@PORCENTAJE,@SEPARADOR_PORCENTAJE+1)" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @PRESUPUESTOS = @PRESUPUESTOS + @VALOR_TEXT_DEN + '(' + CAST(CONVERT(FLOAT,@PORCENTAJE)*100 AS VARCHAR) + '%);'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @PRESUPUESTOS <> ''" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'''' + @PRESUPUESTOS + N'''' + N',' + N'''' + @VALOR_TEXT + N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'''' + @PRESUPUESTOS + N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL,'+N'C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CAMPOTRATADO = 0 -- Resto" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @SUBTIPO = 3 -- tipo fecha" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_FEC IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + '''' + @VALOR_FEC + ''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @SUBTIPO = 4 -- tipo booleano" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_BOOL IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + CONVERT(VARCHAR,@VALOR_BOOL)" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @SUBTIPO=2 -- tipo num?rico" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_NUM IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + CONVERT(VARCHAR,@VALOR_NUM)" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE    -- tipo texto" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+@VALOR_TEXT+N''''+N','+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+''''+@VALOR_TEXT+''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+ N'_COD='+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+ N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ','" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curSOLICITUDES_CAMPOS_DESGLOSE INTO @LINEA,@CAMPO_ORIGEN,@SUBTIPO,@TIPO_CAMPO_GS," & vbCrLf
sConsulta = sConsulta & "@VALOR_TEXT,@VALOR_NUM,@VALOR_FEC,@VALOR_BOOL,@VALOR_TEXT_COD,@VALOR_TEXT_DEN,@INTRO,@ID_ATRIB_GS," & vbCrLf
sConsulta = sConsulta & "@TABLA_EXTERNA,@CAMPO_DEF,@CAMPO_PADRE_DESGLOSE" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "CLOSE curSOLICITUDES_CAMPOS_DESGLOSE" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curSOLICITUDES_CAMPOS_DESGLOSE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @EJECUTARSQL=1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @INSERT = 1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = LEFT(@SQLCAMPOSINSERT,LEN(@SQLCAMPOSINSERT)-1) + ') ' + LEFT(@SQL,LEN(@SQL)-1) + ')'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = LEFT(@SQL,LEN(@SQL)-1) + ' WHERE FORM_INSTANCIA=@INSTANCIA_L AND DESGLOSE=@CAMPO_PADRE_DESGLOSE AND LINEA=@LINEA '" & vbCrLf
sConsulta = sConsulta & "SET @SQL=N'IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = ''dbo'' AND  TABLE_NAME='''+@IDIOMA+N'_'+@TABLA+N'_DESGLOSE'')) BEGIN '+@SQL+N' END'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA_L INT,@CAMPO_PADRE_DESGLOSE INT,@LINEA INT', @INSTANCIA_L=@INSTANCIA_L,@CAMPO_PADRE_DESGLOSE=@CAMPO_PADRE_DESGLOSE,@LINEA=@LINEA" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "SET @INSERT=@INSERT_ORIGINAL" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curIDIOMAS INTO @IDIOMA" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CLOSE curIDIOMAS" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curIDIOMAS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DROP TABLE #G1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion31900_09_2147_A31900_09_2148() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim oADOConnection As ADODB.Connection
    Dim sConsulta As String

    On Error GoTo Error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Actualiza Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    frmProgreso.ProgressBar1.Refresh
    V_31900_9_Storeds_2148

    sConsulta = "UPDATE VERSION SET NUM ='3.00.21.048'" & ", FECHA=CONVERT(datetime,'" & Format(Now, "mm-dd-yyyy hh:mm:ss") & "',110), PRODVER='31900.9'"
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False

    CodigoDeActualizacion31900_09_2147_A31900_09_2148 = True
    Exit Function

Error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If

    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    CodigoDeActualizacion31900_09_2147_A31900_09_2148 = False
End Function

Private Sub V_31900_9_Storeds_2148()
    Dim sConsulta As String

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_SAVE_INSTANCIA_EN_FILTRO]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_SAVE_INSTANCIA_EN_FILTRO]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_SAVE_INSTANCIA_EN_FILTRO]" & vbCrLf
sConsulta = sConsulta & "@INSTANCIA INT," & vbCrLf
sConsulta = sConsulta & "@INSERT TINYINT," & vbCrLf
sConsulta = sConsulta & "@QA TINYINT=0," & vbCrLf
sConsulta = sConsulta & "@CONTRATO TINYINT=0" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @INSTANCIA_L INT" & vbCrLf
sConsulta = sConsulta & "SET @INSTANCIA_L=@INSTANCIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Obtenemos el nombre de las tablas din?micas del filtro (falta a?adir el prefijo del idioma)." & vbCrLf
sConsulta = sConsulta & "DECLARE @TABLA NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "IF @QA=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @CONTRATO=0" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT @TABLA=F.NOM_TABLA FROM INSTANCIA I WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_GRUPO G WITH (NOLOCK) ON I.ID=G.INSTANCIA" & vbCrLf
sConsulta = sConsulta & "INNER JOIN FORM_GRUPO G2 WITH (NOLOCK) ON G.GRUPO_ORIGEN=G2.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_FILTROS F WITH (NOLOCK) ON G2.FORMULARIO=F.FORMULARIO" & vbCrLf
sConsulta = sConsulta & "WHERE I.ID = @INSTANCIA_L" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT @TABLA=F.NOM_TABLA FROM INSTANCIA I WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_GRUPO G WITH (NOLOCK) ON I.ID=G.INSTANCIA" & vbCrLf
sConsulta = sConsulta & "INNER JOIN FORM_GRUPO G2 WITH (NOLOCK) ON G.GRUPO_ORIGEN=G2.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_CONTR_FILTROS F WITH (NOLOCK) ON G2.FORMULARIO=F.FORMULARIO" & vbCrLf
sConsulta = sConsulta & "WHERE I.ID = @INSTANCIA_L" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT @TABLA=F.NOM_TABLA FROM INSTANCIA I WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_GRUPO G WITH (NOLOCK) ON I.ID=G.INSTANCIA" & vbCrLf
sConsulta = sConsulta & "INNER JOIN FORM_GRUPO G2 WITH (NOLOCK) ON G.GRUPO_ORIGEN=G2.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN QA_FILTROS F WITH (NOLOCK) ON G2.FORMULARIO=F.FORMULARIO" & vbCrLf
sConsulta = sConsulta & "WHERE I.ID = @INSTANCIA_L" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Obtenemos las longitudes de los materiales de la tabla DIC" & vbCrLf
sConsulta = sConsulta & "DECLARE @LEN_GMN1 INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @LEN_GMN2 INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @LEN_GMN3 INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @LEN_GMN4 INT" & vbCrLf
sConsulta = sConsulta & "SELECT @LEN_GMN1=DIC.LONGITUD, @LEN_GMN2=DIC2.LONGITUD,@LEN_GMN3=DIC3.LONGITUD, @LEN_GMN4=DIC4.LONGITUD FROM DIC WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN DIC DIC2 WITH (NOLOCK) ON DIC2.NOMBRE='GMN2'" & vbCrLf
sConsulta = sConsulta & "INNER JOIN DIC DIC3 WITH (NOLOCK) ON DIC3.NOMBRE='GMN3'" & vbCrLf
sConsulta = sConsulta & "INNER JOIN DIC DIC4 WITH (NOLOCK) ON DIC4.NOMBRE='GMN4'" & vbCrLf
sConsulta = sConsulta & "WHERE DIC.NOMBRE = 'GMN1'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Guardamos los IDs en una tabla temporal" & vbCrLf
sConsulta = sConsulta & "--Es lo ?nico que cambia de los 3 casos (QA, Solicitudes y Contratos)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLINSERT NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE #G1 (ID_CAMPO INT)" & vbCrLf
sConsulta = sConsulta & "SET @SQLINSERT = N'INSERT INTO #G1 SELECT ID_CAMPO FROM '" & vbCrLf
sConsulta = sConsulta & "IF @QA=0" & vbCrLf
sConsulta = sConsulta & "IF @CONTRATO = 0" & vbCrLf
sConsulta = sConsulta & "SET @SQLINSERT = @SQLINSERT + N'UPD_VISORES_PM WITH(NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQLINSERT = @SQLINSERT + N'UPD_VISORES_CONTR WITH(NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQLINSERT = @SQLINSERT + N'UPD_VISORES_QA WITH(NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "SET @SQLINSERT = @SQLINSERT + N'WHERE (ACCION=''I'' OR ACCION=''A'') AND (NOM_TABLA=@TABLA OR NOM_TABLA=@TABLA+''_DESGLOSE'') AND OK=0'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLINSERT, N'@TABLA NVARCHAR(100)', @TABLA=@TABLA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @LINEA INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @LINEA_ANT INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @CAMPO_ORIGEN INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @SUBTIPO INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @TIPO_CAMPO_GS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @VALOR_NUM FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @VALOR_TEXT NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @VALOR_FEC DATETIME" & vbCrLf
sConsulta = sConsulta & "DECLARE @VALOR_BOOL TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @VALOR_TEXT_DEN NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @VALOR_TEXT_COD NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @INTRO TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_ATRIB_GS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @TABLA_EXTERNA INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @CAMPO_DEF INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL_COD NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLCAMPOSINSERT NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @EJECUTARSQL TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @CAMPO_PADRE_DESGLOSE INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @CAMPO_PADRE_DESGLOSE_ANT INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @DENOMINACION NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @CAMPOTRATADO TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @EMPIEZAIDIOMA INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ACABADENOMINACION INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @EMPIEZADENOMINACION INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @SEPARADOR INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRESUPUESTOS NVARCHAR(800)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PORCENTAJE NVARCHAR(400)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SEPARADOR_DEN INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @SEPARADOR_PORCENTAJE INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDIOMA AS NVARCHAR(400)" & vbCrLf
sConsulta = sConsulta & "DECLARE curIDIOMAS CURSOR LOCAL FOR" & vbCrLf
sConsulta = sConsulta & "SELECT COD FROM IDIOMAS WITH(NOLOCK) WHERE APLICACION=1" & vbCrLf
sConsulta = sConsulta & "OPEN curIDIOMAS" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curIDIOMAS INTO @IDIOMA" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SQL=''" & vbCrLf
sConsulta = sConsulta & "SET @SQLCAMPOSINSERT=''" & vbCrLf
sConsulta = sConsulta & "SET @EJECUTARSQL=0" & vbCrLf
sConsulta = sConsulta & "SET @LINEA_ANT=1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @INSERT = 1 AND @CONTRATO = 1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'INSERT INTO ' + @IDIOMA + N'_CONTR_FILTRO1 VALUES (@INSTANCIA_L)'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA_L INT', @INSTANCIA_L=@INSTANCIA_L" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @INSERT = 1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SQLCAMPOSINSERT=N'INSERT INTO '+@IDIOMA+N'_'+@TABLA+N' (INSTANCIA,'" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N' VALUES (@INSTANCIA_L,'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=N' UPDATE ' + @IDIOMA + N'_' + @TABLA  + N' SET '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE curSOLICITUDES_CAMPOS CURSOR LOCAL FOR" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT CD.CAMPO_ORIGEN, CD.SUBTIPO, CD.TIPO_CAMPO_GS, C.VALOR_TEXT, C.VALOR_NUM," & vbCrLf
sConsulta = sConsulta & "C.VALOR_FEC, C.VALOR_BOOL, C.VALOR_TEXT_COD, C.VALOR_TEXT_DEN, CD.INTRO, CD.ID_ATRIB_GS," & vbCrLf
sConsulta = sConsulta & "CD.TABLA_EXTERNA, C.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "FROM COPIA_CAMPO_DEF CD WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_CAMPO C WITH(NOLOCK) ON CD.ID=C.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "INNER JOIN INSTANCIA I WITH(NOLOCK) ON C.INSTANCIA=I.ID AND C.NUM_VERSION=I.ULT_VERSION" & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_GRUPO G WITH (NOLOCK) ON I.ID=G.INSTANCIA" & vbCrLf
sConsulta = sConsulta & "INNER JOIN FORM_GRUPO G2 WITH (NOLOCK) ON G.GRUPO_ORIGEN=G2.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN FORMULARIO F WITH(NOLOCK) ON F.ID=G2.FORMULARIO" & vbCrLf
sConsulta = sConsulta & "INNER JOIN FORM_GRUPO FG WITH (NOLOCK) ON F.ID=FG.FORMULARIO" & vbCrLf
sConsulta = sConsulta & "INNER JOIN FORM_CAMPO FC WITH (NOLOCK) ON FG.ID = FC.GRUPO" & vbCrLf
sConsulta = sConsulta & "WHERE CD.ES_SUBCAMPO=0 AND CD.SUBTIPO<>9 AND (CD.TIPO_CAMPO_GS<>106 OR CD.TIPO_CAMPO_GS IS NULL)" & vbCrLf
sConsulta = sConsulta & "AND CD.CAMPO_ORIGEN = FC.ID AND (CD.SUBTIPO=FC.SUBTIPO" & vbCrLf
sConsulta = sConsulta & "OR (CD.SUBTIPO=5 AND FC.SUBTIPO IN (6,7)) OR (CD.SUBTIPO=6 AND FC.SUBTIPO=7))  --si la definici?n del campo ha cambiado de texto corto a texto medio o largo, o de texto medio a texto largo" & vbCrLf
sConsulta = sConsulta & "AND CD.CAMPO_ORIGEN NOT IN (SELECT ID_CAMPO FROM #G1)  -- campos que se han a?adido en el formulario pero no se ha hecho todavia ALTER en las tablas de filtros (BatchUpdates)" & vbCrLf
sConsulta = sConsulta & "AND I.ID=@INSTANCIA_L" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "OPEN curSOLICITUDES_CAMPOS" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curSOLICITUDES_CAMPOS INTO @CAMPO_ORIGEN, @SUBTIPO, @TIPO_CAMPO_GS, @VALOR_TEXT, @VALOR_NUM, @VALOR_FEC, @VALOR_BOOL, @VALOR_TEXT_COD, @VALOR_TEXT_DEN, @INTRO, @ID_ATRIB_GS,@TABLA_EXTERNA, @CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @EJECUTARSQL=1" & vbCrLf
sConsulta = sConsulta & "--Reemplazamos las comillas simples en valor_text y valor_text_den" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @VALOR_TEXT = REPLACE(@VALOR_TEXT,'''','''''')" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT_DEN IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SET @VALOR_TEXT_DEN = REPLACE(@VALOR_TEXT_DEN,'''','''''')" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "   IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "       SET @SQLCAMPOSINSERT = @SQLCAMPOSINSERT + 'C_' + CONVERT(NVARCHAR,@CAMPO_ORIGEN)  + ','" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "       SET @SQLCAMPOSINSERT = @SQLCAMPOSINSERT + N'C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N','" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = @SQL + 'C_' + CONVERT(NVARCHAR,@CAMPO_ORIGEN) + '='" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = @SQL +N'C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'='" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "----------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @SUBTIPO = 3 -- tipo fecha" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_FEC IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'CONVERT(DATETIME,''' + CONVERT(VARCHAR,@VALOR_FEC) + ''')'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @SUBTIPO = 4 -- tipo booleano" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_BOOL IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + CONVERT(NVARCHAR,@VALOR_BOOL)" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @SUBTIPO = 8 -- tipo archivo" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'''' + @VALOR_TEXT + N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @TIPO_CAMPO_GS IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @TABLA_EXTERNA IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @SUBTIPO=2 -- tipo num?rico" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_NUM IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + CONVERT(NVARCHAR,@VALOR_NUM)" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE    -- tipo texto o tipo lista texto" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1 AND @QA=0 AND @CONTRATO=0" & vbCrLf
sConsulta = sConsulta & "   SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "IF @INTRO=1 AND @VALOR_NUM IS NOT NULL --tipo lista texto" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @ID_ATRIB_GS IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @IDIOMA = 'SPA'" & vbCrLf
sConsulta = sConsulta & "SELECT @DENOMINACION=VALOR_TEXT_SPA" & vbCrLf
sConsulta = sConsulta & "FROM COPIA_CAMPO_VALOR_LISTA WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE CAMPO_DEF=CONVERT(VARCHAR,@CAMPO_DEF) AND ORDEN=CONVERT(VARCHAR,@VALOR_NUM)" & vbCrLf
sConsulta = sConsulta & "IF @IDIOMA = 'ENG'" & vbCrLf
sConsulta = sConsulta & "SELECT @DENOMINACION=VALOR_TEXT_ENG" & vbCrLf
sConsulta = sConsulta & "FROM COPIA_CAMPO_VALOR_LISTA WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE CAMPO_DEF=CONVERT(VARCHAR,@CAMPO_DEF) AND ORDEN=CONVERT(VARCHAR,@VALOR_NUM)" & vbCrLf
sConsulta = sConsulta & "IF @IDIOMA = 'GER'" & vbCrLf
sConsulta = sConsulta & "SELECT @DENOMINACION=VALOR_TEXT_GER" & vbCrLf
sConsulta = sConsulta & "FROM COPIA_CAMPO_VALOR_LISTA WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE CAMPO_DEF = CONVERT(VARCHAR,@CAMPO_DEF) AND ORDEN=CONVERT(VARCHAR,@VALOR_NUM)" & vbCrLf
sConsulta = sConsulta & "IF @IDIOMA = 'FRA'" & vbCrLf
sConsulta = sConsulta & "SELECT @DENOMINACION=VALOR_TEXT_FRA" & vbCrLf
sConsulta = sConsulta & "FROM COPIA_CAMPO_VALOR_LISTA WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE CAMPO_DEF = CONVERT(VARCHAR,@CAMPO_DEF) AND ORDEN=CONVERT(VARCHAR,@VALOR_NUM)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @DENOMINACION IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + '''' + replace(@DENOMINACION,'''','''''') + ''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+REPLACE(@DENOMINACION,'''','''''')+''''+N',CONVERT(NVARCHAR,'+CONVERT(NVARCHAR,@VALOR_NUM)+N')'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+REPLACE(@DENOMINACION,'''','''''')+''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=CONVERT(NVARCHAR,'+CONVERT(NVARCHAR,@VALOR_NUM)+N')'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + N'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + N'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + '''' + @VALOR_TEXT + ''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + N''''+@VALOR_TEXT+N''''+N','+N''''+CONVERT(NVARCHAR,@VALOR_NUM)+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + N''''+@VALOR_TEXT+N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+ CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N''''+CONVERT(NVARCHAR,@VALOR_NUM)+N''''" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE -- tipo texto" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + '''' + @VALOR_TEXT + ''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + N''''+@VALOR_TEXT+N''''+N','+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + N''''+@VALOR_TEXT+N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+ CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + N'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @SUBTIPO=2 -- tipo num?rico" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_NUM IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + CONVERT(NVARCHAR,@VALOR_NUM)" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE    -- tipo texto o tipo lista texto" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1 AND @QA=0 AND @CONTRATO=0" & vbCrLf
sConsulta = sConsulta & "   SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT_DEN IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + '''' + @VALOR_TEXT_DEN + ''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+@VALOR_TEXT_DEN+N''''+N','+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+@VALOR_TEXT_DEN+N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_' +CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE -- campo del sistema" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @CAMPOTRATADO = 0" & vbCrLf
sConsulta = sConsulta & "---------------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "-- Formas de pago, Monedas, Familia de materiales, Unidades, Destinos, Pa?ses y Provincias" & vbCrLf
sConsulta = sConsulta & "-- FORMATO: <Idioma>|<Texto>#<Idioma>|<Texto>" & vbCrLf
sConsulta = sConsulta & "-- FORMATO MATERIALES: cod. ultimo nivel (VALOR_TEXT) +  <Idioma>|<Texto>#<Idioma>|<Texto>" & vbCrLf
sConsulta = sConsulta & "------------------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "IF @TIPO_CAMPO_GS = 101 OR @TIPO_CAMPO_GS = 102 OR @TIPO_CAMPO_GS = 103 OR @TIPO_CAMPO_GS = 105 OR @TIPO_CAMPO_GS = 109 OR @TIPO_CAMPO_GS = 107 OR @TIPO_CAMPO_GS = 108" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1 AND @QA=0 AND @CONTRATO=0" & vbCrLf
sConsulta = sConsulta & "   SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+ N'_COD,'" & vbCrLf
sConsulta = sConsulta & "SET @CAMPOTRATADO = 1" & vbCrLf
sConsulta = sConsulta & "-- Materiales" & vbCrLf
sConsulta = sConsulta & "IF @TIPO_CAMPO_GS = 103" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "-- Al hacer Len de valor_text me est? quitando los espacios en blanco de la derecha" & vbCrLf
sConsulta = sConsulta & "IF LEN(@VALOR_TEXT + '#')-1 = @LEN_GMN1 + @LEN_GMN2 + @LEN_GMN3 + @LEN_GMN4" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'''' + RTRIM(RIGHT(@VALOR_TEXT,@LEN_GMN4)) + N' - '" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF LEN(@VALOR_TEXT + '#')-1 = @LEN_GMN1 + @LEN_GMN2 + @LEN_GMN3" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'''' + RTRIM(RIGHT(@VALOR_TEXT,@LEN_GMN3)) + N' - '" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF LEN(@VALOR_TEXT + '#')-1 = @LEN_GMN1 + @LEN_GMN2" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'''' + RTRIM(RIGHT(@VALOR_TEXT,@LEN_GMN2)) + N' - '" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'''' + RTRIM(RIGHT(@VALOR_TEXT,@LEN_GMN1)) + N' - '" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- Todos" & vbCrLf
sConsulta = sConsulta & "SET @EMPIEZAIDIOMA=CHARINDEX(@IDIOMA + '|',@VALOR_TEXT_DEN)" & vbCrLf
sConsulta = sConsulta & "IF (@EMPIEZAIDIOMA) > 0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @ACABADENOMINACION = CHARINDEX('#',@VALOR_TEXT_DEN,@EMPIEZAIDIOMA)" & vbCrLf
sConsulta = sConsulta & "SET @EMPIEZADENOMINACION = CHARINDEX('|',@VALOR_TEXT_DEN,@EMPIEZAIDIOMA)+1" & vbCrLf
sConsulta = sConsulta & "IF @TIPO_CAMPO_GS <> 103" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+''''" & vbCrLf
sConsulta = sConsulta & "IF @ACABADENOMINACION>0" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+SUBSTRING(@VALOR_TEXT_DEN,@EMPIEZADENOMINACION,@ACABADENOMINACION-@EMPIEZADENOMINACION) + ''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+RIGHT(@VALOR_TEXT_DEN,LEN(@VALOR_TEXT_DEN)-@EMPIEZADENOMINACION+1) + ''''" & vbCrLf
sConsulta = sConsulta & "IF @QA=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N','+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N',C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT IS NOT NULL AND @TIPO_CAMPO_GS = 103" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+N','+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "-- Proveedores, Departamentos, Organizaciones de Compras, Centros,Proveedor ERP" & vbCrLf
sConsulta = sConsulta & "-- FORMATO: C?digo (VALOR_TEXT) - Denominaci?n (VALOR_TEXT_DEN)" & vbCrLf
sConsulta = sConsulta & "--------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "IF @TIPO_CAMPO_GS = 11 OR @TIPO_CAMPO_GS = 100  OR @TIPO_CAMPO_GS = 122 OR @TIPO_CAMPO_GS = 123 OR @TIPO_CAMPO_GS = 124 OR @TIPO_CAMPO_GS = 143" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @CAMPOTRATADO = 1" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1 AND @QA=0 AND @CONTRATO=0" & vbCrLf
sConsulta = sConsulta & "   SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + '''' + @VALOR_TEXT + ' - ' + @VALOR_TEXT_DEN + ''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+@VALOR_TEXT+N' - '+@VALOR_TEXT_DEN+N''''+N','+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+@VALOR_TEXT+N' - '+@VALOR_TEXT_DEN+N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "-- Centros de coste, Partidas presupuestarias, Activos" & vbCrLf
sConsulta = sConsulta & "-- FORMATO: cod. ultimo nivel (VALOR_TEXT) +  <Idioma>|<Texto>#<Idioma>|<Texto>" & vbCrLf
sConsulta = sConsulta & "--------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "IF @TIPO_CAMPO_GS = 129 OR @TIPO_CAMPO_GS = 130 OR @TIPO_CAMPO_GS = 131 OR @TIPO_CAMPO_GS=132" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @CAMPOTRATADO=1" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1 AND @QA=0 AND @CONTRATO=0" & vbCrLf
sConsulta = sConsulta & "   SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SEPARADOR = CHARINDEX('#',@VALOR_TEXT,1)" & vbCrLf
sConsulta = sConsulta & "WHILE @SEPARADOR > 0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @VALOR_TEXT = RIGHT(@VALOR_TEXT,LEN(@VALOR_TEXT)-@SEPARADOR)" & vbCrLf
sConsulta = sConsulta & "SET @SEPARADOR = CHARINDEX('#',@VALOR_TEXT,1)" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SEPARADOR = CHARINDEX('|',@VALOR_TEXT,1)" & vbCrLf
sConsulta = sConsulta & "WHILE @SEPARADOR > 0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @VALOR_TEXT = RIGHT(@VALOR_TEXT,LEN(@VALOR_TEXT)-@SEPARADOR)" & vbCrLf
sConsulta = sConsulta & "SET @SEPARADOR = CHARINDEX('|',@VALOR_TEXT,1)" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + '''' + @VALOR_TEXT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Denominaci?n en el idioma" & vbCrLf
sConsulta = sConsulta & "SET @EMPIEZAIDIOMA=CHARINDEX(@IDIOMA + '|',@VALOR_TEXT_DEN)" & vbCrLf
sConsulta = sConsulta & "IF (@EMPIEZAIDIOMA) > 0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @ACABADENOMINACION = CHARINDEX('#',@VALOR_TEXT_DEN,@EMPIEZAIDIOMA)" & vbCrLf
sConsulta = sConsulta & "SET @EMPIEZADENOMINACION = CHARINDEX('|',@VALOR_TEXT_DEN,@EMPIEZAIDIOMA)+1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ACABADENOMINACION>0" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ' - ' + SUBSTRING(@VALOR_TEXT_DEN,@EMPIEZADENOMINACION,@ACABADENOMINACION-@EMPIEZADENOMINACION)" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ' - ' + RIGHT(@VALOR_TEXT_DEN,LEN(@VALOR_TEXT_DEN)-@EMPIEZADENOMINACION+1)" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+N','+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "-- Personas, Comprador" & vbCrLf
sConsulta = sConsulta & "---------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "IF @TIPO_CAMPO_GS = 115 OR @TIPO_CAMPO_GS = 144" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @CAMPOTRATADO = 1" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1 AND @QA=0 AND @CONTRATO=0" & vbCrLf
sConsulta = sConsulta & "   SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT_DEN IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + '''' + @VALOR_TEXT_DEN + ''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+@VALOR_TEXT_DEN+N''''+N','+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+@VALOR_TEXT_DEN+N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "-- Almacen" & vbCrLf
sConsulta = sConsulta & "---------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "IF @TIPO_CAMPO_GS = 125" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @CAMPOTRATADO = 1" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1 AND @QA=0 AND @CONTRATO=0" & vbCrLf
sConsulta = sConsulta & "   SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT_DEN IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + '''' + @VALOR_TEXT_DEN + ''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+@VALOR_TEXT_DEN+N''''+N','+N''''+CONVERT(NVARCHAR,@VALOR_NUM)+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+@VALOR_TEXT_DEN+N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N''''+CONVERT(NVARCHAR,@VALOR_NUM)+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "------------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "-- Cod. Art?culo (viejo), Cod. Art?culo (nuevo), Denominaci?n del art?culo y Unidades Organizativas" & vbCrLf
sConsulta = sConsulta & "------------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "IF @TIPO_CAMPO_GS = 104  OR @TIPO_CAMPO_GS = 118 OR @TIPO_CAMPO_GS = 119 OR @TIPO_CAMPO_GS = 121" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @CAMPOTRATADO = 1" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1 AND @QA=0 AND @CONTRATO=0" & vbCrLf
sConsulta = sConsulta & "   SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + '''' + @VALOR_TEXT + ''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+@VALOR_TEXT+N''''+N','+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+@VALOR_TEXT +N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "---------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "-- Presupuestos" & vbCrLf
sConsulta = sConsulta & "-- FORMATO: Ej 2005 - PC-002  (50.00%); 2005 - PC-001 (50.00%);" & vbCrLf
sConsulta = sConsulta & "--  VALOR_TEXT_DEN: 2005 - PC-002#2005 - PC-001" & vbCrLf
sConsulta = sConsulta & "--  VALOR_TEXT: 1_11_0.5#1_10_0.5" & vbCrLf
sConsulta = sConsulta & "---------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "IF @TIPO_CAMPO_GS = 110  OR @TIPO_CAMPO_GS = 111 OR @TIPO_CAMPO_GS = 112 OR @TIPO_CAMPO_GS = 113" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @CAMPOTRATADO = 1" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1 AND @QA=0 AND @CONTRATO=0" & vbCrLf
sConsulta = sConsulta & "   SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "SET @PRESUPUESTOS = ''" & vbCrLf
sConsulta = sConsulta & "SET @SEPARADOR_DEN = CHARINDEX('#',@VALOR_TEXT_DEN)" & vbCrLf
sConsulta = sConsulta & "WHILE @SEPARADOR_DEN > 0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @PRESUPUESTOS = @PRESUPUESTOS + SUBSTRING(@VALOR_TEXT_DEN,1,@SEPARADOR_DEN-1)" & vbCrLf
sConsulta = sConsulta & "SET @VALOR_TEXT_DEN = RIGHT(@VALOR_TEXT_DEN,LEN(@VALOR_TEXT_DEN)-@SEPARADOR_DEN)" & vbCrLf
sConsulta = sConsulta & "SET @PORCENTAJE = SUBSTRING(@VALOR_TEXT,1,CHARINDEX('#',@VALOR_TEXT)-1)" & vbCrLf
sConsulta = sConsulta & "SET @SEPARADOR_PORCENTAJE = 1" & vbCrLf
sConsulta = sConsulta & "WHILE @SEPARADOR_PORCENTAJE > 0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @PORCENTAJE = RIGHT(@PORCENTAJE,LEN(@PORCENTAJE)-@SEPARADOR_PORCENTAJE)" & vbCrLf
sConsulta = sConsulta & "SET @SEPARADOR_PORCENTAJE = CHARINDEX('_',@PORCENTAJE,@SEPARADOR_PORCENTAJE+1)" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "SET @PRESUPUESTOS = @PRESUPUESTOS + '(' + CONVERT(VARCHAR,CONVERT(DECIMAL(3,2),@PORCENTAJE)*100) + '%); '" & vbCrLf
sConsulta = sConsulta & "SET @VALOR_TEXT = RIGHT(@VALOR_TEXT,LEN(@VALOR_TEXT)-CHARINDEX('#',@VALOR_TEXT))" & vbCrLf
sConsulta = sConsulta & "SET @SEPARADOR_DEN = CHARINDEX('#',@VALOR_TEXT_DEN)" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "SET @PORCENTAJE = @VALOR_TEXT" & vbCrLf
sConsulta = sConsulta & "SET @SEPARADOR_PORCENTAJE = 1" & vbCrLf
sConsulta = sConsulta & "WHILE @SEPARADOR_PORCENTAJE > 0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @PORCENTAJE = RIGHT(@PORCENTAJE,LEN(@PORCENTAJE)-@SEPARADOR_PORCENTAJE)" & vbCrLf
sConsulta = sConsulta & "SET @SEPARADOR_PORCENTAJE = CHARINDEX('_',@PORCENTAJE,@SEPARADOR_PORCENTAJE+1)" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @PRESUPUESTOS = @PRESUPUESTOS + @VALOR_TEXT_DEN + '(' + CAST(CONVERT(FLOAT,@PORCENTAJE)*100 AS VARCHAR) + '%);'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @PRESUPUESTOS <> ''" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + '''' + @PRESUPUESTOS + ''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'''' + @PRESUPUESTOS + N'''' + N',' + N'''' + @VALOR_TEXT + N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'''' + @PRESUPUESTOS + N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL,'+N'C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CAMPOTRATADO = 0 -- Resto" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @SUBTIPO = 3 -- tipo fecha" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_FEC IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + '''' + @VALOR_FEC + ''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @SUBTIPO = 4 -- tipo booleano" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_BOOL IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + CONVERT(VARCHAR,@VALOR_BOOL)" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @SUBTIPO=2 -- tipo num?rico" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_NUM IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + CONVERT(VARCHAR,@VALOR_NUM)" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE    -- tipo texto" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1 AND @QA=0 AND @CONTRATO=0" & vbCrLf
sConsulta = sConsulta & "   SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + '''' + @VALOR_TEXT + ''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+@VALOR_TEXT+N''''+N','+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+''''+@VALOR_TEXT+''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+ N'_COD='+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+ N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ','" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curSOLICITUDES_CAMPOS INTO @CAMPO_ORIGEN, @SUBTIPO, @TIPO_CAMPO_GS, @VALOR_TEXT, @VALOR_NUM, @VALOR_FEC, @VALOR_BOOL, @VALOR_TEXT_COD, @VALOR_TEXT_DEN, @INTRO, @ID_ATRIB_GS,@TABLA_EXTERNA, @CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CLOSE curSOLICITUDES_CAMPOS" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curSOLICITUDES_CAMPOS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @EJECUTARSQL=1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @INSERT = 1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = LEFT(@SQLCAMPOSINSERT,LEN(@SQLCAMPOSINSERT)-1) + ') ' + LEFT(@SQL,LEN(@SQL)-1) + ')'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = LEFT(@SQL,LEN(@SQL)-1) + ' WHERE INSTANCIA = @INSTANCIA_L'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL=N'IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = ''dbo'' AND  TABLE_NAME='''+@IDIOMA+N'_'+@TABLA+''')) BEGIN '+@SQL+N' END'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA_L INT', @INSTANCIA_L=@INSTANCIA_L" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--*********************************" & vbCrLf
sConsulta = sConsulta & "--*********************************" & vbCrLf
sConsulta = sConsulta & "-- AHORA LOS CAMPOS DE LOS DESGLOSES (SI NO ES QA NI CONTRATO)" & vbCrLf
sConsulta = sConsulta & "--**********************************" & vbCrLf
sConsulta = sConsulta & "IF @QA=0 AND @CONTRATO=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SQL=''" & vbCrLf
sConsulta = sConsulta & "SET @SQLCAMPOSINSERT=''" & vbCrLf
sConsulta = sConsulta & "SET @EJECUTARSQL=0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Pongo insert a 1 para que siempre inserte, ya que si de una etapa a otra se crean nuevas lineas de desglose s?lo estar?a haciendo update de las lineas existentes" & vbCrLf
sConsulta = sConsulta & "--Ahora elimino todas las l?neas y vuelvo a insertar las que haya en ese momento, as? tambi?n controlo si se elimina alguna l?nea" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLDELETE NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @INSERT_ORIGINAL TINYINT" & vbCrLf
sConsulta = sConsulta & "SET @SQLDELETE=''" & vbCrLf
sConsulta = sConsulta & "SET @INSERT_ORIGINAL=@INSERT" & vbCrLf
sConsulta = sConsulta & "SET @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQLDELETE='DELETE '+@IDIOMA+N'_'+@TABLA+N'_DESGLOSE WHERE FORM_INSTANCIA=@INSTANCIA_L'" & vbCrLf
sConsulta = sConsulta & "SET @SQLDELETE=N'IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = ''dbo'' AND  TABLE_NAME='''+@IDIOMA+N'_'+@TABLA+N'_DESGLOSE'')) BEGIN '+@SQLDELETE+N' END'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLDELETE, N'@INSTANCIA_L INT',@INSTANCIA_L=@INSTANCIA_L" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @INSERT = 1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SQLCAMPOSINSERT=N'INSERT INTO '+@IDIOMA+N'_'+@TABLA+N'_DESGLOSE (FORM_INSTANCIA,LINEA,DESGLOSE,'" & vbCrLf
sConsulta = sConsulta & "SET @SQL=N' VALUES (@INSTANCIA_L,@LINEA,'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=N' UPDATE '+@IDIOMA+N'_'+@TABLA+N'_DESGLOSE SET '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE curSOLICITUDES_CAMPOS_DESGLOSE CURSOR LOCAL FOR" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT CLD.LINEA,CCD.CAMPO_ORIGEN,CCD.SUBTIPO,CCD.TIPO_CAMPO_GS,CLD.VALOR_TEXT,CLD.VALOR_NUM,CLD.VALOR_FEC,CLD.VALOR_BOOL," & vbCrLf
sConsulta = sConsulta & "CLD.VALOR_TEXT_COD,CLD.VALOR_TEXT_DEN,CCD.INTRO,CCD.ID_ATRIB_GS,CCD.TABLA_EXTERNA,CC.COPIA_CAMPO_DEF," & vbCrLf
sConsulta = sConsulta & "D.CAMPO_PADRE" & vbCrLf
sConsulta = sConsulta & "FROM COPIA_CAMPO_DEF CCD WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_CAMPO CC WITH(NOLOCK) ON CCD.ID=CC.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "INNER JOIN INSTANCIA I WITH(NOLOCK) ON CC.INSTANCIA=I.ID AND CC.NUM_VERSION=I.ULT_VERSION" & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_GRUPO G WITH (NOLOCK) ON I.ID=G.INSTANCIA" & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_LINEA_DESGLOSE CLD WITH(NOLOCK) ON CLD.CAMPO_HIJO=CC.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN FORM_GRUPO G2 WITH (NOLOCK) ON G.GRUPO_ORIGEN=G2.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN FORMULARIO F WITH(NOLOCK) ON F.ID=G2.FORMULARIO" & vbCrLf
sConsulta = sConsulta & "INNER JOIN FORM_GRUPO FG WITH (NOLOCK) ON F.ID=FG.FORMULARIO" & vbCrLf
sConsulta = sConsulta & "INNER JOIN FORM_CAMPO FC WITH (NOLOCK) ON FG.ID=FC.GRUPO" & vbCrLf
sConsulta = sConsulta & "INNER JOIN DESGLOSE D WITH(NOLOCK) ON D.CAMPO_HIJO=FC.ID" & vbCrLf
sConsulta = sConsulta & "WHERE CCD.ES_SUBCAMPO=1 AND CCD.SUBTIPO<>9 AND (CCD.TIPO_CAMPO_GS<>106 OR CCD.TIPO_CAMPO_GS IS NULL)" & vbCrLf
sConsulta = sConsulta & "AND CCD.CAMPO_ORIGEN=FC.ID" & vbCrLf
sConsulta = sConsulta & "AND (CCD.SUBTIPO=FC.SUBTIPO OR (CCD.SUBTIPO=5 AND FC.SUBTIPO IN (6,7)) OR (CCD.SUBTIPO=6 AND FC.SUBTIPO=7))--si la definici?n del campo ha cambiado de texto corto a texto medio o largo, o de texto medio a texto largo" & vbCrLf
sConsulta = sConsulta & "AND CCD.CAMPO_ORIGEN NOT IN (SELECT ID_CAMPO FROM #G1)  -- campos que se han a?adido en el formulario pero no se ha hecho todavia ALTER en las tablas de filtros (BatchUpdates)" & vbCrLf
sConsulta = sConsulta & "AND I.ID=@INSTANCIA_L" & vbCrLf
sConsulta = sConsulta & "ORDER BY CAMPO_PADRE,LINEA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "OPEN curSOLICITUDES_CAMPOS_DESGLOSE" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curSOLICITUDES_CAMPOS_DESGLOSE INTO @LINEA,@CAMPO_ORIGEN,@SUBTIPO,@TIPO_CAMPO_GS,@VALOR_TEXT," & vbCrLf
sConsulta = sConsulta & "@VALOR_NUM,@VALOR_FEC,@VALOR_BOOL,@VALOR_TEXT_COD,@VALOR_TEXT_DEN,@INTRO,@ID_ATRIB_GS,@TABLA_EXTERNA," & vbCrLf
sConsulta = sConsulta & "@CAMPO_DEF,@CAMPO_PADRE_DESGLOSE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @CAMPO_PADRE_DESGLOSE_ANT=@CAMPO_PADRE_DESGLOSE" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLAUX NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLCAMPOSINSERTAUX NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "SET @SQLAUX=@SQL" & vbCrLf
sConsulta = sConsulta & "SET @SQLCAMPOSINSERTAUX=@SQLCAMPOSINSERT" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N','" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @EJECUTARSQL=1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @LINEA<>@LINEA_ANT OR @CAMPO_PADRE_DESGLOSE<>@CAMPO_PADRE_DESGLOSE_ANT" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @EJECUTARSQL=1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @INSERT = 1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=LEFT(@SQLCAMPOSINSERT,LEN(@SQLCAMPOSINSERT)-1) + ') '+LEFT(@SQL,LEN(@SQL)-1)+')'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=LEFT(@SQL,LEN(@SQL)-1)+' WHERE FORM_INSTANCIA=@INSTANCIA_L AND DESGLOSE=@CAMPO_PADRE_DESGLOSE AND LINEA=@LINEA '" & vbCrLf
sConsulta = sConsulta & "SET @SQL=N'IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = ''dbo'' AND  TABLE_NAME='''+@IDIOMA+N'_'+@TABLA+N'_DESGLOSE'')) BEGIN '+@SQL+N' END'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA_L INT,@CAMPO_PADRE_DESGLOSE INT,@LINEA INT',@INSTANCIA_L=@INSTANCIA_L,@CAMPO_PADRE_DESGLOSE=@CAMPO_PADRE_DESGLOSE_ANT,@LINEA=@LINEA_ANT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQLAUX" & vbCrLf
sConsulta = sConsulta & "SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERTAUX" & vbCrLf
sConsulta = sConsulta & "SET @LINEA_ANT=@LINEA" & vbCrLf
sConsulta = sConsulta & "SET @CAMPO_PADRE_DESGLOSE_ANT=@CAMPO_PADRE_DESGLOSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N','" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "--Reemplazamos las comillas simples en valor_text y valor_text_den" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @VALOR_TEXT = REPLACE(@VALOR_TEXT,'''','''''')" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT_DEN IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SET @VALOR_TEXT_DEN = REPLACE(@VALOR_TEXT_DEN,'''','''''')" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT + N'C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N','" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'='" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "----------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "IF @SUBTIPO = 3 -- tipo fecha" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_FEC IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'CONVERT(DATETIME,''' + CONVERT(VARCHAR,@VALOR_FEC) + ''')'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @SUBTIPO = 4 -- tipo booleano" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_BOOL IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + CONVERT(NVARCHAR,@VALOR_BOOL)" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @SUBTIPO = 8 -- tipo archivo" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'''' + @VALOR_TEXT + N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @TIPO_CAMPO_GS IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @TABLA_EXTERNA IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @SUBTIPO=2 -- tipo num?rico" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_NUM IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + CONVERT(NVARCHAR,@VALOR_NUM)" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE    -- tipo texto o tipo lista texto" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "IF @INTRO=1 AND @VALOR_NUM IS NOT NULL --tipo lista texto" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @ID_ATRIB_GS IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @IDIOMA = 'SPA'" & vbCrLf
sConsulta = sConsulta & "SELECT @DENOMINACION=VALOR_TEXT_SPA" & vbCrLf
sConsulta = sConsulta & "FROM COPIA_CAMPO_VALOR_LISTA WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE CAMPO_DEF=CONVERT(VARCHAR,@CAMPO_DEF) AND ORDEN=CONVERT(VARCHAR,@VALOR_NUM)" & vbCrLf
sConsulta = sConsulta & "IF @IDIOMA = 'ENG'" & vbCrLf
sConsulta = sConsulta & "SELECT @DENOMINACION=VALOR_TEXT_ENG" & vbCrLf
sConsulta = sConsulta & "FROM COPIA_CAMPO_VALOR_LISTA WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE CAMPO_DEF=CONVERT(VARCHAR,@CAMPO_DEF) AND ORDEN=CONVERT(VARCHAR,@VALOR_NUM)" & vbCrLf
sConsulta = sConsulta & "IF @IDIOMA = 'GER'" & vbCrLf
sConsulta = sConsulta & "SELECT @DENOMINACION=VALOR_TEXT_GER" & vbCrLf
sConsulta = sConsulta & "FROM COPIA_CAMPO_VALOR_LISTA WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE CAMPO_DEF = CONVERT(VARCHAR,@CAMPO_DEF) AND ORDEN=CONVERT(VARCHAR,@VALOR_NUM)" & vbCrLf
sConsulta = sConsulta & "IF @IDIOMA = 'FRA'" & vbCrLf
sConsulta = sConsulta & "SELECT @DENOMINACION=VALOR_TEXT_FRA" & vbCrLf
sConsulta = sConsulta & "FROM COPIA_CAMPO_VALOR_LISTA WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE CAMPO_DEF = CONVERT(VARCHAR,@CAMPO_DEF) AND ORDEN=CONVERT(VARCHAR,@VALOR_NUM)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @DENOMINACION IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+REPLACE(@DENOMINACION,'''','''''')+''''+N',CONVERT(NVARCHAR,'+CONVERT(NVARCHAR,@VALOR_NUM)+N')'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+REPLACE(@DENOMINACION,'''','''''')+''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=CONVERT(NVARCHAR,'+CONVERT(NVARCHAR,@VALOR_NUM)+N')'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + N'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + N'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + N''''+@VALOR_TEXT+N''''+N','+N''''+CONVERT(NVARCHAR,@VALOR_NUM)+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + N''''+@VALOR_TEXT+N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+ CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N''''+CONVERT(NVARCHAR,@VALOR_NUM)+N''''" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE -- tipo texto" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + N''''+@VALOR_TEXT+N''''+N','+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + N''''+@VALOR_TEXT+N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+ CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + N'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @SUBTIPO=2 -- tipo num?rico" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_NUM IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + CONVERT(NVARCHAR,@VALOR_NUM)" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE    -- tipo texto o tipo lista texto" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT_DEN IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+@VALOR_TEXT_DEN+N''''+N','+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+@VALOR_TEXT_DEN+N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_' +CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE -- campo del sistema" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @CAMPOTRATADO = 0" & vbCrLf
sConsulta = sConsulta & "---------------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "-- Formas de pago, Monedas, Familia de materiales, Unidades, Destinos, Pa?ses y Provincias" & vbCrLf
sConsulta = sConsulta & "-- FORMATO: <Idioma>|<Texto>#<Idioma>|<Texto>" & vbCrLf
sConsulta = sConsulta & "-- FORMATO MATERIALES: cod. ultimo nivel (VALOR_TEXT) +  <Idioma>|<Texto>#<Idioma>|<Texto>" & vbCrLf
sConsulta = sConsulta & "------------------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "IF @TIPO_CAMPO_GS = 101 OR @TIPO_CAMPO_GS = 102 OR @TIPO_CAMPO_GS = 103 OR @TIPO_CAMPO_GS = 105 OR @TIPO_CAMPO_GS = 109 OR @TIPO_CAMPO_GS = 107 OR @TIPO_CAMPO_GS = 108" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+ N'_COD,'" & vbCrLf
sConsulta = sConsulta & "SET @CAMPOTRATADO = 1" & vbCrLf
sConsulta = sConsulta & "-- Materiales" & vbCrLf
sConsulta = sConsulta & "IF @TIPO_CAMPO_GS = 103" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "-- Al hacer Len de valor_text me est? quitando los espacios en blanco de la derecha" & vbCrLf
sConsulta = sConsulta & "IF LEN(@VALOR_TEXT + '#')-1 = @LEN_GMN1 + @LEN_GMN2 + @LEN_GMN3 + @LEN_GMN4" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'''' + RTRIM(RIGHT(@VALOR_TEXT,@LEN_GMN4)) + N' - '" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF LEN(@VALOR_TEXT + '#')-1 = @LEN_GMN1 + @LEN_GMN2 + @LEN_GMN3" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'''' + RTRIM(RIGHT(@VALOR_TEXT,@LEN_GMN3)) + N' - '" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF LEN(@VALOR_TEXT + '#')-1 = @LEN_GMN1 + @LEN_GMN2" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'''' + RTRIM(RIGHT(@VALOR_TEXT,@LEN_GMN2)) + N' - '" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'''' + RTRIM(RIGHT(@VALOR_TEXT,@LEN_GMN1)) + N' - '" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- Todos" & vbCrLf
sConsulta = sConsulta & "SET @EMPIEZAIDIOMA=CHARINDEX(@IDIOMA + '|',@VALOR_TEXT_DEN)" & vbCrLf
sConsulta = sConsulta & "IF (@EMPIEZAIDIOMA) > 0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @ACABADENOMINACION = CHARINDEX('#',@VALOR_TEXT_DEN,@EMPIEZAIDIOMA)" & vbCrLf
sConsulta = sConsulta & "SET @EMPIEZADENOMINACION = CHARINDEX('|',@VALOR_TEXT_DEN,@EMPIEZAIDIOMA)+1" & vbCrLf
sConsulta = sConsulta & "IF @TIPO_CAMPO_GS <> 103" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+''''" & vbCrLf
sConsulta = sConsulta & "IF @ACABADENOMINACION>0" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+SUBSTRING(@VALOR_TEXT_DEN,@EMPIEZADENOMINACION,@ACABADENOMINACION-@EMPIEZADENOMINACION) + ''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+RIGHT(@VALOR_TEXT_DEN,LEN(@VALOR_TEXT_DEN)-@EMPIEZADENOMINACION+1) + ''''" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N','+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N',C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT IS NOT NULL AND @TIPO_CAMPO_GS = 103" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+N','+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "-- Proveedores, Departamentos, Organizaciones de Compras, Centros" & vbCrLf
sConsulta = sConsulta & "-- FORMATO: C?digo (VALOR_TEXT) - Denominaci?n (VALOR_TEXT_DEN)" & vbCrLf
sConsulta = sConsulta & "--------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "IF @TIPO_CAMPO_GS = 11 OR @TIPO_CAMPO_GS = 100  OR @TIPO_CAMPO_GS = 122 OR @TIPO_CAMPO_GS = 123 OR @TIPO_CAMPO_GS = 124" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @CAMPOTRATADO = 1" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+@VALOR_TEXT+N' - '+@VALOR_TEXT_DEN+N''''+N','+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+@VALOR_TEXT+N' - '+@VALOR_TEXT_DEN+N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "-- Centros de coste, Partidas presupuestarias, Activos" & vbCrLf
sConsulta = sConsulta & "-- FORMATO: cod. ultimo nivel (VALOR_TEXT) +  <Idioma>|<Texto>#<Idioma>|<Texto>" & vbCrLf
sConsulta = sConsulta & "--------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "IF @TIPO_CAMPO_GS = 129 OR @TIPO_CAMPO_GS = 130 OR @TIPO_CAMPO_GS = 131 OR @TIPO_CAMPO_GS=132" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @CAMPOTRATADO=1" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SEPARADOR = CHARINDEX('#',@VALOR_TEXT,1)" & vbCrLf
sConsulta = sConsulta & "WHILE @SEPARADOR > 0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @VALOR_TEXT = RIGHT(@VALOR_TEXT,LEN(@VALOR_TEXT)-@SEPARADOR)" & vbCrLf
sConsulta = sConsulta & "SET @SEPARADOR = CHARINDEX('#',@VALOR_TEXT,1)" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SEPARADOR = CHARINDEX('|',@VALOR_TEXT,1)" & vbCrLf
sConsulta = sConsulta & "WHILE @SEPARADOR > 0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @VALOR_TEXT = RIGHT(@VALOR_TEXT,LEN(@VALOR_TEXT)-@SEPARADOR)" & vbCrLf
sConsulta = sConsulta & "SET @SEPARADOR = CHARINDEX('|',@VALOR_TEXT,1)" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + '''' + @VALOR_TEXT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Denominaci?n en el idioma" & vbCrLf
sConsulta = sConsulta & "SET @EMPIEZAIDIOMA=CHARINDEX(@IDIOMA + '|',@VALOR_TEXT_DEN)" & vbCrLf
sConsulta = sConsulta & "IF (@EMPIEZAIDIOMA) > 0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @ACABADENOMINACION = CHARINDEX('#',@VALOR_TEXT_DEN,@EMPIEZAIDIOMA)" & vbCrLf
sConsulta = sConsulta & "SET @EMPIEZADENOMINACION = CHARINDEX('|',@VALOR_TEXT_DEN,@EMPIEZAIDIOMA)+1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ACABADENOMINACION>0" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ' - ' + SUBSTRING(@VALOR_TEXT_DEN,@EMPIEZADENOMINACION,@ACABADENOMINACION-@EMPIEZADENOMINACION)" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ' - ' + RIGHT(@VALOR_TEXT_DEN,LEN(@VALOR_TEXT_DEN)-@EMPIEZADENOMINACION+1)" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+N','+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "-- Personas, Comprador" & vbCrLf
sConsulta = sConsulta & "---------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "IF @TIPO_CAMPO_GS = 115 OR @TIPO_CAMPO_GS=144" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @CAMPOTRATADO = 1" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT_DEN IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+@VALOR_TEXT_DEN+N''''+N','+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+@VALOR_TEXT_DEN+N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "-- Almacen" & vbCrLf
sConsulta = sConsulta & "---------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "IF @TIPO_CAMPO_GS = 125" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @CAMPOTRATADO = 1" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT_DEN IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+@VALOR_TEXT_DEN+N''''+N','+N''''+CONVERT(NVARCHAR,@VALOR_NUM)+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+@VALOR_TEXT_DEN+N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N''''+CONVERT(NVARCHAR,@VALOR_NUM)+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "------------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "-- Cod. Art?culo (viejo), Cod. Art?culo (nuevo), Denominaci?n del art?culo y Unidades Organizativas" & vbCrLf
sConsulta = sConsulta & "------------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "IF @TIPO_CAMPO_GS = 104  OR @TIPO_CAMPO_GS = 118 OR @TIPO_CAMPO_GS = 119 OR @TIPO_CAMPO_GS = 121" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @CAMPOTRATADO = 1" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+@VALOR_TEXT+N''''+N','+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+@VALOR_TEXT +N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "---------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "-- Presupuestos" & vbCrLf
sConsulta = sConsulta & "-- FORMATO: Ej 2005 - PC-002  (50.00%); 2005 - PC-001 (50.00%);" & vbCrLf
sConsulta = sConsulta & "--  VALOR_TEXT_DEN: 2005 - PC-002#2005 - PC-001" & vbCrLf
sConsulta = sConsulta & "--  VALOR_TEXT: 1_11_0.5#1_10_0.5" & vbCrLf
sConsulta = sConsulta & "---------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "IF @TIPO_CAMPO_GS = 110  OR @TIPO_CAMPO_GS = 111 OR @TIPO_CAMPO_GS = 112 OR @TIPO_CAMPO_GS = 113" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @CAMPOTRATADO = 1" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "SET @PRESUPUESTOS = ''" & vbCrLf
sConsulta = sConsulta & "SET @SEPARADOR_DEN = CHARINDEX('#',@VALOR_TEXT_DEN)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "WHILE @SEPARADOR_DEN > 0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @PRESUPUESTOS = @PRESUPUESTOS + SUBSTRING(@VALOR_TEXT_DEN,1,@SEPARADOR_DEN-1)" & vbCrLf
sConsulta = sConsulta & "SET @VALOR_TEXT_DEN = RIGHT(@VALOR_TEXT_DEN,LEN(@VALOR_TEXT_DEN)-@SEPARADOR_DEN)" & vbCrLf
sConsulta = sConsulta & "SET @PORCENTAJE = SUBSTRING(@VALOR_TEXT,1,CHARINDEX('#',@VALOR_TEXT)-1)" & vbCrLf
sConsulta = sConsulta & "SET @SEPARADOR_PORCENTAJE = 1" & vbCrLf
sConsulta = sConsulta & "WHILE @SEPARADOR_PORCENTAJE > 0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @PORCENTAJE = RIGHT(@PORCENTAJE,LEN(@PORCENTAJE)-@SEPARADOR_PORCENTAJE)" & vbCrLf
sConsulta = sConsulta & "SET @SEPARADOR_PORCENTAJE = CHARINDEX('_',@PORCENTAJE,@SEPARADOR_PORCENTAJE+1)" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "SET @PRESUPUESTOS = @PRESUPUESTOS + '(' + CONVERT(VARCHAR,CONVERT(DECIMAL(3,2),@PORCENTAJE)*100) + '%); '" & vbCrLf
sConsulta = sConsulta & "SET @VALOR_TEXT = RIGHT(@VALOR_TEXT,LEN(@VALOR_TEXT)-CHARINDEX('#',@VALOR_TEXT))" & vbCrLf
sConsulta = sConsulta & "SET @SEPARADOR_DEN = CHARINDEX('#',@VALOR_TEXT_DEN)" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "SET @PORCENTAJE = @VALOR_TEXT" & vbCrLf
sConsulta = sConsulta & "SET @SEPARADOR_PORCENTAJE = 1" & vbCrLf
sConsulta = sConsulta & "WHILE @SEPARADOR_PORCENTAJE > 0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @PORCENTAJE = RIGHT(@PORCENTAJE,LEN(@PORCENTAJE)-@SEPARADOR_PORCENTAJE)" & vbCrLf
sConsulta = sConsulta & "SET @SEPARADOR_PORCENTAJE = CHARINDEX('_',@PORCENTAJE,@SEPARADOR_PORCENTAJE+1)" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @PRESUPUESTOS = @PRESUPUESTOS + @VALOR_TEXT_DEN + '(' + CAST(CONVERT(FLOAT,@PORCENTAJE)*100 AS VARCHAR) + '%);'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @PRESUPUESTOS <> ''" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'''' + @PRESUPUESTOS + N'''' + N',' + N'''' + @VALOR_TEXT + N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'''' + @PRESUPUESTOS + N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL,'+N'C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CAMPOTRATADO = 0 -- Resto" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @SUBTIPO = 3 -- tipo fecha" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_FEC IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + '''' + @VALOR_FEC + ''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @SUBTIPO = 4 -- tipo booleano" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_BOOL IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + CONVERT(VARCHAR,@VALOR_BOOL)" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @SUBTIPO=2 -- tipo num?rico" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_NUM IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + CONVERT(VARCHAR,@VALOR_NUM)" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE    -- tipo texto" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+@VALOR_TEXT+N''''+N','+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+''''+@VALOR_TEXT+''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+ N'_COD='+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+ N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ','" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curSOLICITUDES_CAMPOS_DESGLOSE INTO @LINEA,@CAMPO_ORIGEN,@SUBTIPO,@TIPO_CAMPO_GS," & vbCrLf
sConsulta = sConsulta & "@VALOR_TEXT,@VALOR_NUM,@VALOR_FEC,@VALOR_BOOL,@VALOR_TEXT_COD,@VALOR_TEXT_DEN,@INTRO,@ID_ATRIB_GS," & vbCrLf
sConsulta = sConsulta & "@TABLA_EXTERNA,@CAMPO_DEF,@CAMPO_PADRE_DESGLOSE" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "CLOSE curSOLICITUDES_CAMPOS_DESGLOSE" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curSOLICITUDES_CAMPOS_DESGLOSE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @EJECUTARSQL=1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @INSERT = 1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = LEFT(@SQLCAMPOSINSERT,LEN(@SQLCAMPOSINSERT)-1) + ') ' + LEFT(@SQL,LEN(@SQL)-1) + ')'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = LEFT(@SQL,LEN(@SQL)-1) + ' WHERE FORM_INSTANCIA=@INSTANCIA_L AND DESGLOSE=@CAMPO_PADRE_DESGLOSE AND LINEA=@LINEA '" & vbCrLf
sConsulta = sConsulta & "SET @SQL=N'IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = ''dbo'' AND  TABLE_NAME='''+@IDIOMA+N'_'+@TABLA+N'_DESGLOSE'')) BEGIN '+@SQL+N' END'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA_L INT,@CAMPO_PADRE_DESGLOSE INT,@LINEA INT', @INSTANCIA_L=@INSTANCIA_L,@CAMPO_PADRE_DESGLOSE=@CAMPO_PADRE_DESGLOSE,@LINEA=@LINEA" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "SET @INSERT=@INSERT_ORIGINAL" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curIDIOMAS INTO @IDIOMA" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CLOSE curIDIOMAS" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curIDIOMAS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DROP TABLE #G1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion31900_09_2148_A31900_09_2149() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim oADOConnection As ADODB.Connection
    Dim sConsulta As String

    On Error GoTo Error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Actualiza Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    frmProgreso.ProgressBar1.Refresh
    V_31900_9_Storeds_2149

    sConsulta = "UPDATE VERSION SET NUM ='3.00.21.049'" & ", FECHA=CONVERT(datetime,'" & Format(Now, "mm-dd-yyyy hh:mm:ss") & "',110), PRODVER='31900.9'"
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False

    CodigoDeActualizacion31900_09_2148_A31900_09_2149 = True
    Exit Function

Error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If

    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    CodigoDeActualizacion31900_09_2148_A31900_09_2149 = False
End Function

Private Sub V_31900_9_Storeds_2149()
    Dim sConsulta As String

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usu_cod]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[usu_cod]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[USU_COD] (@OLD VARCHAR(50),@NEW VARCHAR(50),@PWD VARCHAR(512),@FEC_USU DATETIME,@ADM VARCHAR(100))  AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT ON" & vbCrLf
sConsulta = sConsulta & "--BEGIN TRANSACTION" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR LOCAL FOR SELECT COD FROM USU WITH (NOLOCK) WHERE COD=@OLD" & vbCrLf
sConsulta = sConsulta & "OPEN C" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C INTO @VAR_COD" & vbCrLf
sConsulta = sConsulta & "IF @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU_ACC NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU_LIS NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_SOBRE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTA_INICIAL_PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTA_INICIAL_GRUPO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LOG NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_CONTR NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE SOBRE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PARINS_DEF NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PARINS_DOT NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PARINS_GEST NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PARINS_PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FAVORITOS_ORDEN_ENTREGA NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FAVORITOS_ORDEN_ENTREGA_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FAVORITOS_LINEAS_PEDIDO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FAVORITOS_LINEAS_PEDIDO_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_NUMDEC NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE VAR_CAL_USU NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CERTIFICADO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE NOCONFORMIDAD NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_CAMPOS_MATERIAL_QA NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_CAMPOS_CERTIFICADOS NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISOR NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_SOLICITUDES_FAV  NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_FAVORITOS  NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_FAVORITOS NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_FAVORITOS NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_FAVORITOS NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_FAVORITOS NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU_PRES5 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU_CC_CONTROL NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU_CC_IMPUTACION NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU_DEST NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU_UNQA NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE VAR_CAL_MANUAL NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU_PANELCALIDAD NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PERSONALIZACION_USU NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OBJETIVO_SUELO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU_PWD NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FAVORITOS_IMPUTACION NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES5_HIST NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "--CN" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_CAT NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_GRUPO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_GRUPO_USU NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_MEN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_MEN_USU NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_OPT_MEN_USU NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_RESP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_USU NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_USU_NOTIF NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ESCENARIO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FILTRO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE VISTA NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CARPETA NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE USU SET COD=@NEW,PWD=@PWD,FEC_USU=@FEC_USU,ADM=@ADM WHERE COD=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE USU_ACC SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE USU_LIS SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET USUVAL=@NEW WHERE USUVAL=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET USUVALSELPROVE=@NEW WHERE USUVALSELPROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET USUVALCIERRE=@NEW WHERE USUVALCIERRE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET USUAPER=@NEW WHERE USUAPER=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_PROCE SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_PROCE_PROVE SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_PROCE_ATRIB SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_PROCE_SOBRE SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_GRUPO SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_GRUPO_PROVE SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_GRUPO_ATRIB SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ITEM SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ITEM_PROVE SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ITEM_ATRIB SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ALL SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ALL_PROVE SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ALL_ATRIB SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTA_INICIAL_PROCE SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTA_INICIAL_GRUPO SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_CONTR SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE SOBRE SET USUAPER=@NEW WHERE USUAPER=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PARINS_DEF SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PARINS_DOT SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PARINS_GEST SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PARINS_PROCE SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE FAVORITOS_ORDEN_ENTREGA SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE FAVORITOS_ORDEN_ENTREGA_ADJUN SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE FAVORITOS_LINEAS_PEDIDO SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE FAVORITOS_LINEAS_PEDIDO_ADJUN SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_NUMDEC SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE VAR_CAL_USU SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CERTIFICADO SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE NOCONFORMIDAD SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE NOCONFORMIDAD SET REVISOR=@NEW WHERE REVISOR=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_CAMPOS_MATERIAL_QA SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_CAMPOS_CERTIFICADOS SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISOR SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PM_SOLICITUDES_FAV SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_FAVORITOS SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES1_FAVORITOS SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES2_FAVORITOS SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES3_FAVORITOS SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES4_FAVORITOS SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE USU_PRES5 SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE USU_CC_CONTROL SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE USU_CC_IMPUTACION SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE USU_DEST SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE USU_UNQA SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE VAR_CAL_MANUAL SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE USU_PANELCALIDAD SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PERSONALIZACION_USU SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OBJETIVO_SUELO SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE USU_PWD SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE FAVORITOS_IMPUTACION SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES5_HIST SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES5_HIST SET USU_COMENT=@NEW WHERE USU_COMENT=@OLD" & vbCrLf
sConsulta = sConsulta & "--CN" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_CAT SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_GRUPO SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_GRUPO_USU SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_MEN SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_MEN_USU SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_OPT_MEN_USU SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_RESP SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_RESP SET USUCITADO=@NEW WHERE USUCITADO=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_USU SET COD=@NEW WHERE COD=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_USU_NOTIF SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE ESCENARIO SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE FILTRO SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE VISTA SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CARPETA SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU_ACC CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU_LIS CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_SOBRE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTA_INICIAL_PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTA_INICIAL_GRUPO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LOG CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_CONTR CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE SOBRE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PARINS_DEF CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PARINS_DOT CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PARINS_GEST CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PARINS_PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FAVORITOS_ORDEN_ENTREGA CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FAVORITOS_ORDEN_ENTREGA_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FAVORITOS_LINEAS_PEDIDO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FAVORITOS_LINEAS_PEDIDO_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_NUMDEC CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE VAR_CAL_USU CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CERTIFICADO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE NOCONFORMIDAD CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_CAMPOS_MATERIAL_QA CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_CAMPOS_CERTIFICADOS CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISOR CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_SOLICITUDES_FAV CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_FAVORITOS CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_FAVORITOS CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_FAVORITOS CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_FAVORITOS CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_FAVORITOS CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU_PRES5 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU_CC_CONTROL CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU_CC_IMPUTACION CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU_DEST CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU_UNQA CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE VAR_CAL_MANUAL CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU_PANELCALIDAD CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PERSONALIZACION_USU CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OBJETIVO_SUELO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU_PWD CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FAVORITOS_IMPUTACION CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES5_HIST CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "--CN" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_CAT CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_GRUPO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_GRUPO_USU CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_MEN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_MEN_USU CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_OPT_MEN_USU CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_RESP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_USU CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_USU_NOTIF CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ESCENARIO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FILTRO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE VISTA CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CARPETA CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close C" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT OFF" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

End Sub

