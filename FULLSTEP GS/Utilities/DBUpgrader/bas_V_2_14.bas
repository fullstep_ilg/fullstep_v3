Attribute VB_Name = "bas_V_2_14"
Option Explicit
    
Public Function CodigoDeActualizacion2_12_05_11A2_14_00_00() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim bBool As Boolean

On Error GoTo error
    

sConsulta = "BEGIN TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = True
ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

frmProgreso.lblDetalle = "Modificamos tablas"
frmProgreso.lblDetalle.Refresh
frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_14_Tablas_0



frmProgreso.lblDetalle = "Modificamos storeds"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_14_Storeds_0


sConsulta = "UPDATE VERSION SET NUM ='2.14.00.00'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "COMMIT TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = False

CodigoDeActualizacion2_12_05_11A2_14_00_00 = True
Exit Function
    
error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_12_05_11A2_14_00_00 = False

End Function
Private Sub V_2_14_Tablas_0()
Dim sConsulta As String


sConsulta = "ALTER TABLE dbo.CATALOG_CAMPOS ADD" & vbCrLf
sConsulta = sConsulta & "   INTERNO tinyint NOT NULL CONSTRAINT DF_CATALOG_CAMPOS_INTERNO DEFAULT (0)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PRES_ART]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)" & vbCrLf
sConsulta = sConsulta & "drop table [dbo].[PRES_ART]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TABLE [dbo].[PRES_ART] (" & vbCrLf
sConsulta = sConsulta & "   [ANYO] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [GMN1] [varchar] (" & gLongitudesDeCodigos.giLongCodGMN1 & ") NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [GMN2] [varchar] (" & gLongitudesDeCodigos.giLongCodGMN2 & ") NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [GMN3] [varchar] (" & gLongitudesDeCodigos.giLongCodGMN3 & ") NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [GMN4] [varchar] (" & gLongitudesDeCodigos.giLongCodGMN4 & ") NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [COD] [varchar] (" & gLongitudesDeCodigos.giLongCodART & ") NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [CANT] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PRES] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [OBJ] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [FECACT] [datetime] NULL " & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

If frmProgreso.ProgressBar1.Value >= frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

sConsulta = "ALTER TABLE [dbo].[PRES_ART] WITH NOCHECK ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [PK_PRES_ART] PRIMARY KEY  CLUSTERED " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [ANYO]," & vbCrLf
sConsulta = sConsulta & "       [GMN1]," & vbCrLf
sConsulta = sConsulta & "       [GMN2]," & vbCrLf
sConsulta = sConsulta & "       [GMN3]," & vbCrLf
sConsulta = sConsulta & "       [GMN4]," & vbCrLf
sConsulta = sConsulta & "       [COD]" & vbCrLf
sConsulta = sConsulta & "   )  ON [PRIMARY] " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[PRES_ART] ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [FK_PRES_ART_ART4] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [GMN1]," & vbCrLf
sConsulta = sConsulta & "       [GMN2]," & vbCrLf
sConsulta = sConsulta & "       [GMN3]," & vbCrLf
sConsulta = sConsulta & "       [GMN4]," & vbCrLf
sConsulta = sConsulta & "       [COD]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[ART4] (" & vbCrLf
sConsulta = sConsulta & "       [GMN1]," & vbCrLf
sConsulta = sConsulta & "       [GMN2]," & vbCrLf
sConsulta = sConsulta & "       [GMN3]," & vbCrLf
sConsulta = sConsulta & "       [GMN4]," & vbCrLf
sConsulta = sConsulta & "       [COD]" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE dbo.PRES_MAT4 ADD" & vbCrLf
sConsulta = sConsulta & "   PRESDIRECTA tinyint NOT NULL CONSTRAINT DF_PRES_MAT4_PRESDIRECTA DEFAULT (0)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

If frmProgreso.ProgressBar1.Value >= frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

sConsulta = "UPDATE PRES_MAT4 SET PRESDIRECTA =1"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PRES_ART_TG_INSUPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PRES_ART_TG_INSUPD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PRES_ART_TG_DEL]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PRES_ART_TG_DEL]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TRIGGER PRES_ART_TG_INSUPD ON dbo.PRES_ART" & vbCrLf
sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN1 VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN2 VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN3 VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN4 VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @COUNT INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_PRES_ART_Ins CURSOR FOR SELECT ANYO,GMN1,GMN2,GMN3,GMN4,COD FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_PRES_ART_Ins" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PRES_ART_Ins INTO @ANYO,@GMN1,@GMN2,@GMN3,@GMN4,@COD" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & " IF UPDATE(PRES)  OR UPDATE(OBJ)  OR UPDATE(CANT) " & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "     SELECT @COUNT=COUNT(*) FROM PRES_MAT4 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@GMN4" & vbCrLf
sConsulta = sConsulta & "     IF @COUNT=0 " & vbCrLf
sConsulta = sConsulta & "       INSERT INTO PRES_MAT4 ( ANYO,GMN1,GMN2,GMN3,GMN4,PRES,OBJ,PRESDIRECTA)  SELECT @ANYO,@GMN1,@GMN2,@GMN3,@GMN4,SUM(PRES*CANT) ,SUM(OBJ*CANT),0 FROM PRES_ART WHERE ANYO=@ANYO AND GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@GMN4" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "     ELSE" & vbCrLf
sConsulta = sConsulta & "           UPDATE PRES_MAT4 SET PRES=(SELECT SUM(PRES*CANT) FROM PRES_ART WHERE ANYO=@ANYO AND GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@GMN4)  ," & vbCrLf
sConsulta = sConsulta & "                         OBJ=(SELECT SUM(OBJ*CANT) FROM PRES_ART WHERE ANYO=@ANYO AND GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@GMN4) ," & vbCrLf
sConsulta = sConsulta & "                         PRESDIRECTA=0" & vbCrLf
sConsulta = sConsulta & "                                     WHERE ANYO=@ANYO AND GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3  AND GMN4=@GMN4" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES_ART SET FECACT=GETDATE() WHERE ANYO=@ANYO AND GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@GMN4 AND COD=@COD" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PRES_ART_Ins INTO @ANYO,@GMN1,@GMN2,@GMN3,@GMN4,@COD" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_PRES_ART_Ins" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_PRES_ART_Ins" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TRIGGER PRES_ART_TG_DEL ON [dbo].[PRES_ART] " & vbCrLf
sConsulta = sConsulta & "FOR  DELETE " & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN1 VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN2 VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN3 VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN4 VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRES FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @OBJ FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @CANT FLOAT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_PRES_ART_Del CURSOR FOR SELECT ANYO,GMN1,GMN2,GMN3,GMN4,COD,PRES,OBJ,CANT FROM DELETED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_PRES_ART_Del" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PRES_ART_Del INTO @ANYO,@GMN1,@GMN2,@GMN3,@GMN4,@COD,@PRES,@OBJ,@CANT" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "  UPDATE PRES_MAT4 SET PRES=(PRES-(@PRES*@CANT)), OBJ=(OBJ-(@OBJ*@CANT))  WHERE ANYO=@ANYO AND GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3  AND GMN4=@GMN4" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & " FETCH NEXT FROM curTG_PRES_ART_Del INTO @ANYO,@GMN1,@GMN2,@GMN3,@GMN4,@COD,@PRES,@OBJ,@CANT" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "Close curTG_PRES_ART_Del" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_PRES_ART_Del" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

If frmProgreso.ProgressBar1.Value >= frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PRES_MAT4_TG_INSUPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PRES_MAT4_TG_INSUPD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PRES_MAT1_TG_INSUPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PRES_MAT1_TG_INSUPD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PRES_MAT2_TG_INSUPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PRES_MAT2_TG_INSUPD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PRES_MAT3_TG_INSUPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PRES_MAT3_TG_INSUPD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

If frmProgreso.ProgressBar1.Value >= frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

sConsulta = "CREATE TRIGGER PRES_MAT4_TG_INSUPD ON dbo.PRES_MAT4" & vbCrLf
sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO INT,@GMN1 VARCHAR(50),@GMN2 VARCHAR(50),@GMN3 VARCHAR(50),@GMN4 VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @COUNT INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_PRES_MAT4_Ins CURSOR FOR SELECT ANYO,GMN1,GMN2,GMN3,GMN4 FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_PRES_MAT4_Ins" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PRES_MAT4_Ins INTO @ANYO,@GMN1,@GMN2,@GMN3,@GMN4" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF UPDATE(PRES)  OR UPDATE(OBJ)" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "     SELECT @COUNT=COUNT(*) FROM PRES_MAT3 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3" & vbCrLf
sConsulta = sConsulta & "      IF @COUNT=0 " & vbCrLf
sConsulta = sConsulta & "   INSERT INTO PRES_MAT3 ( ANYO,GMN1,GMN2,GMN3,PRES,OBJ)  SELECT @ANYO,@GMN1,@GMN2,@GMN3,SUM(PRES) ,SUM(OBJ) FROM PRES_MAT4 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "      ELSE" & vbCrLf
sConsulta = sConsulta & "       UPDATE PRES_MAT3 SET PRES=(SELECT SUM(PRES) FROM PRES_MAT4 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3)  ," & vbCrLf
sConsulta = sConsulta & "                     OBJ=(SELECT SUM(OBJ) FROM PRES_MAT4 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3) " & vbCrLf
sConsulta = sConsulta & "                                     WHERE ANYO=@ANYO AND GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    SELECT @COUNT=COUNT(*) FROM PRES_MAT2 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND GMN2=@GMN2" & vbCrLf
sConsulta = sConsulta & "    IF @COUNT=0 " & vbCrLf
sConsulta = sConsulta & "   INSERT INTO PRES_MAT2 ( ANYO,GMN1,GMN2,PRES,OBJ)  SELECT @ANYO,@GMN1,@GMN2,SUM(PRES) ,SUM(OBJ) FROM PRES_MAT3 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND GMN2=@GMN2" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    ELSE" & vbCrLf
sConsulta = sConsulta & "       UPDATE PRES_MAT2 SET PRES=(SELECT SUM(PRES) FROM PRES_MAT3 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND GMN2=@GMN2)  ," & vbCrLf
sConsulta = sConsulta & "                     OBJ=(SELECT SUM(OBJ) FROM PRES_MAT3 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND GMN2=@GMN2 ) " & vbCrLf
sConsulta = sConsulta & "                                     WHERE ANYO=@ANYO AND GMN1=@GMN1 AND GMN2=@GMN2 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    SELECT @COUNT=COUNT(*) FROM PRES_MAT1 WHERE ANYO=@ANYO AND GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "    IF @COUNT=0 " & vbCrLf
sConsulta = sConsulta & "   INSERT INTO PRES_MAT1 ( ANYO,GMN1,PRES,OBJ)  SELECT @ANYO,@GMN1,SUM(PRES) ,SUM(OBJ) FROM PRES_MAT2 WHERE ANYO=@ANYO AND GMN1=@GMN1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    ELSE" & vbCrLf
sConsulta = sConsulta & "       UPDATE PRES_MAT1 SET PRES=(SELECT SUM(PRES) FROM PRES_MAT2 WHERE ANYO=@ANYO AND GMN1=@GMN1)  , OBJ=(SELECT SUM(OBJ) FROM PRES_MAT2 WHERE ANYO=@ANYO AND GMN1=@GMN1 ) " & vbCrLf
sConsulta = sConsulta & "                                     WHERE ANYO=@ANYO AND GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & " END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES_MAT4 SET FECACT=GETDATE() WHERE ANYO=@ANYO AND GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@GMN4" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PRES_MAT4_Ins INTO @ANYO,@GMN1,@GMN2,@GMN3,@GMN4" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_PRES_MAT4_Ins" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_PRES_MAT4_Ins" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

Dim iAnyo As Integer
iAnyo = Year(Date)
sConsulta = "INSERT INTO PRES_ART (ANYO,GMN1,GMN2,GMN3,GMN4,COD,CANT) SELECT " & iAnyo & ", GMN1,GMN2,GMN3,GMN4,COD,CANT FROM ART4 WHERE CANT IS NOT NULL"
ExecuteSQL gRDOCon, sConsulta

If frmProgreso.ProgressBar1.Value >= frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

sConsulta = "ALTER TABLE dbo.ART4 DROP COLUMN CANT" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "UPDATE PRES_MAT1 SET FECACT=GETDATE() WHERE ANYO=" & iAnyo & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = "UPDATE PRES_MAT2 SET FECACT=GETDATE() WHERE ANYO=" & iAnyo & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = "UPDATE PRES_MAT3 SET FECACT=GETDATE() WHERE ANYO=" & iAnyo & vbCrLf
ExecuteSQL gRDOCon, sConsulta

If frmProgreso.ProgressBar1.Value >= frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

sConsulta = "CREATE TRIGGER PRES_MAT1_TG_INSUPD ON [PRES_MAT1]" & vbCrLf
sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO INT,@GMN1 VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_PRES_MAT1_Ins CURSOR FOR SELECT ANYO,GMN1 FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_PRES_MAT1_Ins" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PRES_MAT1_Ins INTO @ANYO,@GMN1" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES_MAT1 SET FECACT=GETDATE() WHERE ANYO=@ANYO AND GMN1=@GMN1" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PRES_MAT1_Ins INTO @ANYO,@GMN1" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_PRES_MAT1_Ins" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_PRES_MAT1_Ins" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TRIGGER PRES_MAT2_TG_INSUPD ON [PRES_MAT2]" & vbCrLf
sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO INT,@GMN1 VARCHAR(50),@GMN2 VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_PRES_MAT2_Ins CURSOR FOR SELECT ANYO,GMN1,GMN2 FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_PRES_MAT2_Ins" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PRES_MAT2_Ins INTO @ANYO,@GMN1,@GMN2" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES_MAT2 SET FECACT=GETDATE() WHERE ANYO=@ANYO AND GMN1=@GMN1 AND GMN2=@GMN2" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PRES_MAT2_Ins INTO @ANYO,@GMN1,@GMN2" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_PRES_MAT2_Ins" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_PRES_MAT2_Ins" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & ""
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TRIGGER PRES_MAT3_TG_INSUPD ON dbo.PRES_MAT3" & vbCrLf
sConsulta = sConsulta & "FOR INSERT, UPDATE, DELETE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO INT,@GMN1 VARCHAR(50),@GMN2 VARCHAR(50),@GMN3 VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_PRES_MAT3_Ins CURSOR FOR SELECT ANYO,GMN1,GMN2,GMN3 FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_PRES_MAT3_Ins" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PRES_MAT3_Ins INTO @ANYO,@GMN1,@GMN2,@GMN3" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES_MAT3 SET FECACT=GETDATE() WHERE ANYO=@ANYO AND GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PRES_MAT3_Ins INTO @ANYO,@GMN1,@GMN2,@GMN3" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_PRES_MAT3_Ins" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_PRES_MAT3_Ins" & vbCrLf
sConsulta = sConsulta & ""
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE dbo.PARGEN_GEST ADD" & vbCrLf
sConsulta = sConsulta & "   PRES_PLANIFICADO tinyint NOT NULL CONSTRAINT DF_PARGEN_GEST_PRES_PLANIFICADO DEFAULT (0)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE dbo.PARINS_GEST ADD" & vbCrLf
sConsulta = sConsulta & "   PRES_PLANIFICADO tinyint NOT NULL CONSTRAINT DF_PARINS_GEST_PRES_PLANIFICADO DEFAULT (0)," & vbCrLf
sConsulta = sConsulta & "   TIPOACTA tinyint NOT NULL CONSTRAINT DF_PARINS_GEST_TIPOACTA DEFAULT (0)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE dbo.PARGEN_DOT ADD" & vbCrLf
sConsulta = sConsulta & "   ACTADOT2 varchar(255) NULL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


End Sub


Private Sub V_2_14_Storeds_0()
Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_DEVOLVER_LINEAS_DE_ORDEN]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_DEVOLVER_LINEAS_DE_ORDEN]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSEP_DEVOLVER_LINEAS_DE_ORDEN(@APROBADOR VARCHAR(50), @ORDENID INTEGER, @SOLICIT int = null ,@IDI varchar(20) = 'SPA', @MOSTRARCINTERNO tinyint = null)  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(1500)" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if @APROBADOR IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT LP.ID AS LINEAID," & vbCrLf
sConsulta = sConsulta & "                LP.ART_INT + ITEM.ART CODART," & vbCrLf
sConsulta = sConsulta & "                ART4.DEN + ITEM.DESCR  + ART_AUX.DEN + LP.DESCR_LIBRE DENART," & vbCrLf
sConsulta = sConsulta & "                LP.ART_INT + ITEM.ART + ' ' + ART4.DEN + ITEM.DESCR  + ART_AUX.DEN + + LP.DESCR_LIBRE AS ARTICULO," & vbCrLf
sConsulta = sConsulta & "                PA.COD_EXT ART_EXT," & vbCrLf
sConsulta = sConsulta & "                LP.GMN1," & vbCrLf
sConsulta = sConsulta & "                LP.GMN2," & vbCrLf
sConsulta = sConsulta & "                LP.GMN3," & vbCrLf
sConsulta = sConsulta & "                LP.GMN4," & vbCrLf
sConsulta = sConsulta & "              LP.PROVE," & vbCrLf
sConsulta = sConsulta & "                LP.DEST AS DESTINO," & vbCrLf
sConsulta = sConsulta & "                LP.UP AS UNIDAD," & vbCrLf
sConsulta = sConsulta & "                LP.PREC_UP AS PRECIOUNITARIO," & vbCrLf
sConsulta = sConsulta & "                LP.CANT_PED AS CANTIDAD," & vbCrLf
sConsulta = sConsulta & "                LP.CANT_REC AS CANTIDAD_REC," & vbCrLf
sConsulta = sConsulta & "                ITEM_ADJ.CANT_ADJ / LP.FC AS CANTADJUDICADA," & vbCrLf
sConsulta = sConsulta & "                ITEM_ADJ.CANT_PED / LP.FC AS CANTCONSUMIDA," & vbCrLf
sConsulta = sConsulta & "                LP.PREC_UP * LP.CANT_PED IMPORTE," & vbCrLf
sConsulta = sConsulta & "                LP.EST AS APROBADA," & vbCrLf
sConsulta = sConsulta & "                LP.FECEMISION AS FECHA," & vbCrLf
sConsulta = sConsulta & "                GESTIONABLE=CASE WHEN APROB_LIM.PER= @APROBADOR   THEN 1 WHEN (APROB_LIM.PER IS NULL AND SEGURIDAD.APROB_TIPO1=@APROBADOR ) THEN 1  ELSE 0 END," & vbCrLf
sConsulta = sConsulta & "                LP.CAT1," & vbCrLf
sConsulta = sConsulta & "                LP.CAT2," & vbCrLf
sConsulta = sConsulta & "                LP.CAT3," & vbCrLf
sConsulta = sConsulta & "                LP.CAT4," & vbCrLf
sConsulta = sConsulta & "                LP.CAT5," & vbCrLf
sConsulta = sConsulta & "                LP.OBS," & vbCrLf
sConsulta = sConsulta & "                case when c5.cod is null then " & vbCrLf
sConsulta = sConsulta & "                        case when c4.cod is null then " & vbCrLf
sConsulta = sConsulta & "                             case when c3.cod is null then" & vbCrLf
sConsulta = sConsulta & "                                  case when c2.cod is null then" & vbCrLf
sConsulta = sConsulta & "                                       c1.cod" & vbCrLf
sConsulta = sConsulta & "                                  else" & vbCrLf
sConsulta = sConsulta & "                                  c2.cod" & vbCrLf
sConsulta = sConsulta & "                                  end" & vbCrLf
sConsulta = sConsulta & "                             else" & vbCrLf
sConsulta = sConsulta & "                             c3.cod" & vbCrLf
sConsulta = sConsulta & "                             end" & vbCrLf
sConsulta = sConsulta & "                        else" & vbCrLf
sConsulta = sConsulta & "                        c4.cod" & vbCrLf
sConsulta = sConsulta & "                        end" & vbCrLf
sConsulta = sConsulta & "                    else" & vbCrLf
sConsulta = sConsulta & "                    c5.cod" & vbCrLf
sConsulta = sConsulta & "                    end CATEGORIA," & vbCrLf
sConsulta = sConsulta & "                case when c5.cod is null then " & vbCrLf
sConsulta = sConsulta & "                        case when c4.cod is null then " & vbCrLf
sConsulta = sConsulta & "                             case when c3.cod is null then" & vbCrLf
sConsulta = sConsulta & "                                  case when c2.cod is null then" & vbCrLf
sConsulta = sConsulta & "                                       '(' + c1.cod + ') ' + c1.den" & vbCrLf
sConsulta = sConsulta & "                                  else" & vbCrLf
sConsulta = sConsulta & "                                  '(' + c1.cod + ') ' + c1.den + ' - (' + c2.cod + ') ' + c2.den" & vbCrLf
sConsulta = sConsulta & "                                  end" & vbCrLf
sConsulta = sConsulta & "                             else" & vbCrLf
sConsulta = sConsulta & "                             '(' + c1.cod + ') ' + c1.den + ' - (' + c2.cod + ') ' + c2.den + ' - (' + c3.cod + ') ' + c3.den" & vbCrLf
sConsulta = sConsulta & "                             end" & vbCrLf
sConsulta = sConsulta & "                        else" & vbCrLf
sConsulta = sConsulta & "                        '(' + c1.cod + ') ' + c1.den + ' - (' + c2.cod + ') ' + c2.den + ' - (' + c3.cod + ') ' + c3.den + ' - (' + c4.cod + ') ' + c4.den" & vbCrLf
sConsulta = sConsulta & "                        end" & vbCrLf
sConsulta = sConsulta & "                    else" & vbCrLf
sConsulta = sConsulta & "                    '(' + c1.cod + ') ' + c1.den + ' - (' + c2.cod + ') ' + c2.den + ' - (' + c3.cod + ') ' + c3.den + ' - (' + c4.cod + ') ' + c4.den + ' - (' + c5.cod + ') ' + c5.den" & vbCrLf
sConsulta = sConsulta & "                    end CATEGORIARAMA," & vbCrLf
sConsulta = sConsulta & "                ITEM.ANYO ANYOPROCE," & vbCrLf
sConsulta = sConsulta & "                ITEM.PROCE CODPROCE," & vbCrLf
sConsulta = sConsulta & "                LE.COMENT,LPA.ADJUNTOS," & vbCrLf
sConsulta = sConsulta & "                CASE WHEN @MOSTRARCINTERNO IS NULL OR @MOSTRARCINTERNO=1 OR CC1.INTERNO=0 THEN LP.VALOR1 ELSE NULL END VALOR1," & vbCrLf
sConsulta = sConsulta & "                CASE WHEN @MOSTRARCINTERNO IS NULL OR @MOSTRARCINTERNO=1 OR CC2.INTERNO=0 THEN LP.VALOR2 ELSE NULL END VALOR2," & vbCrLf
sConsulta = sConsulta & "                CC1.DEN AS DENCAMPO1,CC2.DEN AS DENCAMPO2, PROCE.DEN AS DENPROCE,LP.OBSADJUN,LP.FECENTREGA," & vbCrLf
sConsulta = sConsulta & "                DEST.DEN AS DESTINODEN " & vbCrLf
sConsulta = sConsulta & "FROM LINEAS_PEDIDO LP" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN APROB_LIM" & vbCrLf
sConsulta = sConsulta & "            ON LP.APROB_LIM=APROB_LIM.ID" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN SEGURIDAD " & vbCrLf
sConsulta = sConsulta & "            ON LP.SEGURIDAD=SEGURIDAD.ID" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN ITEM " & vbCrLf
sConsulta = sConsulta & "            ON LP.ANYO = ITEM.ANYO" & vbCrLf
sConsulta = sConsulta & "           AND LP.GMN1=ITEM.GMN1 " & vbCrLf
sConsulta = sConsulta & "           AND LP.PROCE=ITEM.PROCE " & vbCrLf
sConsulta = sConsulta & "           AND LP.ITEM=ITEM.ID" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN ART4 " & vbCrLf
sConsulta = sConsulta & "            ON LP.GMN1=ART4.GMN1 " & vbCrLf
sConsulta = sConsulta & "           AND LP.GMN2=ART4.GMN2 " & vbCrLf
sConsulta = sConsulta & "           AND LP.GMN3=ART4.GMN3 " & vbCrLf
sConsulta = sConsulta & "           AND LP.GMN4=ART4.GMN4 " & vbCrLf
sConsulta = sConsulta & "           AND LP.ART_INT=ART4.COD" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN ITEM_ADJ " & vbCrLf
sConsulta = sConsulta & "            ON LP.ITEM=ITEM_ADJ.ITEM " & vbCrLf
sConsulta = sConsulta & "           AND LP.ANYO = ITEM_ADJ.ANYO" & vbCrLf
sConsulta = sConsulta & "           AND LP.GMN1=ITEM_ADJ.GMN1 " & vbCrLf
sConsulta = sConsulta & "           AND LP.PROCE=ITEM_ADJ.PROCE " & vbCrLf
sConsulta = sConsulta & "           AND LP.PROVE=ITEM_ADJ.PROVE" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN ART4 ART_AUX " & vbCrLf
sConsulta = sConsulta & "            ON ITEM.ART=ART_AUX.COD " & vbCrLf
sConsulta = sConsulta & "           AND ART_AUX.GMN1=ITEM.GMN1 " & vbCrLf
sConsulta = sConsulta & "           AND ART_AUX.GMN2=ITEM.GMN2 " & vbCrLf
sConsulta = sConsulta & "           AND ART_AUX.GMN3=ITEM.GMN3 " & vbCrLf
sConsulta = sConsulta & "           AND ART_AUX.GMN4=ITEM.GMN4 " & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN PROVE_ART4 PA" & vbCrLf
sConsulta = sConsulta & "            ON LP.PROVE = PA.PROVE" & vbCrLf
sConsulta = sConsulta & "           AND LP.GMN1 = PA.GMN1" & vbCrLf
sConsulta = sConsulta & "           AND LP.GMN2 = PA.GMN2" & vbCrLf
sConsulta = sConsulta & "           AND LP.GMN3 = PA.GMN3" & vbCrLf
sConsulta = sConsulta & "           AND LP.GMN4 = PA.GMN4" & vbCrLf
sConsulta = sConsulta & "           AND CASE WHEN LP.ART_INT IS NULL THEN ITEM.ART ELSE LP.ART_INT END = PA.ART" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN CATN1 C1" & vbCrLf
sConsulta = sConsulta & "            ON LP.CAT1 = C1.ID" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN CATN2 C2" & vbCrLf
sConsulta = sConsulta & "            ON LP.CAT2 = C2.ID" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN CATN3 C3" & vbCrLf
sConsulta = sConsulta & "            ON LP.CAT3 = C3.ID" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN CATN4 C4" & vbCrLf
sConsulta = sConsulta & "            ON LP.CAT4 = C4.ID" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN CATN5 C5" & vbCrLf
sConsulta = sConsulta & "            ON LP.CAT5 = C5.ID" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN LINEAS_EST LE" & vbCrLf
sConsulta = sConsulta & "            ON LP.LINEAS_EST = LE.ID" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN " & vbCrLf
sConsulta = sConsulta & "   (SELECT COUNT(*) AS ADJUNTOS,LINEA  FROM LINEAS_PEDIDO_ADJUN GROUP BY LINEAS_PEDIDO_ADJUN.LINEA) LPA ON LP.ID= LPA.LINEA " & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN CATALOG_CAMPOS AS CC1 " & vbCrLf
sConsulta = sConsulta & "            ON CC1.ID=LP.CAMPO1 AND CC1.IDIOMA=@IDI" & vbCrLf
sConsulta = sConsulta & "           AND CC1.INTERNO =  CASE WHEN @MOSTRARCINTERNO IS NULL OR @MOSTRARCINTERNO=1 THEN CC1.INTERNO ELSE 0 END" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN CATALOG_CAMPOS AS CC2 " & vbCrLf
sConsulta = sConsulta & "            ON CC2.ID=LP.CAMPO2 AND CC2.IDIOMA=@IDI" & vbCrLf
sConsulta = sConsulta & "           AND CC2.INTERNO = CASE WHEN @MOSTRARCINTERNO IS NULL OR @MOSTRARCINTERNO=1 THEN CC2.INTERNO ELSE 0 END" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN PROCE " & vbCrLf
sConsulta = sConsulta & "       ON LP.ANYO=PROCE.ANYO AND LP.GMN1=PROCE.GMN1 AND LP.PROCE=PROCE.COD" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN DEST" & vbCrLf
sConsulta = sConsulta & "       ON LP.DEST=DEST.COD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "WHERE LP.ORDEN=@ORDENID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT LP.ID LINEAID," & vbCrLf
sConsulta = sConsulta & "                LP.ART_INT + I.ART CODART," & vbCrLf
sConsulta = sConsulta & "                LP.GMN1," & vbCrLf
sConsulta = sConsulta & "                LP.GMN2," & vbCrLf
sConsulta = sConsulta & "                LP.GMN3," & vbCrLf
sConsulta = sConsulta & "                LP.GMN4," & vbCrLf
sConsulta = sConsulta & "               LP.PROVE," & vbCrLf
sConsulta = sConsulta & "                ART4.DEN + I.DESCR  + ART_AUX.DEN + + LP.DESCR_LIBRE DENART," & vbCrLf
sConsulta = sConsulta & "                PA.COD_EXT ART_EXT," & vbCrLf
sConsulta = sConsulta & "                LP.DEST DESTINO," & vbCrLf
sConsulta = sConsulta & "                D.DEN DESTDEN," & vbCrLf
sConsulta = sConsulta & "                LP.UP UNIDAD," & vbCrLf
sConsulta = sConsulta & "                U.DEN UNIDEN," & vbCrLf
sConsulta = sConsulta & "                LP.PREC_UP PRECIOUNITARIO," & vbCrLf
sConsulta = sConsulta & "                LP.CANT_PED CANTIDAD," & vbCrLf
sConsulta = sConsulta & "                LP.CANT_REC CANT_REC," & vbCrLf
sConsulta = sConsulta & "                IA.CANT_ADJ * LP.FC AS CANTADJUDICADA," & vbCrLf
sConsulta = sConsulta & "                LP.PREC_UP * LP.CANT_PED IMPORTE," & vbCrLf
sConsulta = sConsulta & "                LP.EST AS APROBADA," & vbCrLf
sConsulta = sConsulta & "                LP.FECEMISION FECHA," & vbCrLf
sConsulta = sConsulta & "                LP.FECENTREGA FECENTREGA," & vbCrLf
sConsulta = sConsulta & "                LP.EST,LP.ENTREGA_OBL,LP.FECENTREGAPROVE," & vbCrLf
sConsulta = sConsulta & "                LP.CAT1," & vbCrLf
sConsulta = sConsulta & "                LP.CAT2," & vbCrLf
sConsulta = sConsulta & "                LP.CAT3," & vbCrLf
sConsulta = sConsulta & "                LP.CAT4," & vbCrLf
sConsulta = sConsulta & "                LP.CAT5," & vbCrLf
sConsulta = sConsulta & "                LP.OBS," & vbCrLf
sConsulta = sConsulta & "                case when c5.cod is null then " & vbCrLf
sConsulta = sConsulta & "                        case when c4.cod is null then " & vbCrLf
sConsulta = sConsulta & "                             case when c3.cod is null then" & vbCrLf
sConsulta = sConsulta & "                                  case when c2.cod is null then" & vbCrLf
sConsulta = sConsulta & "                                       c1.cod" & vbCrLf
sConsulta = sConsulta & "                                  else" & vbCrLf
sConsulta = sConsulta & "                                  c2.cod" & vbCrLf
sConsulta = sConsulta & "                                  end" & vbCrLf
sConsulta = sConsulta & "                             else" & vbCrLf
sConsulta = sConsulta & "                             c3.cod" & vbCrLf
sConsulta = sConsulta & "                             end" & vbCrLf
sConsulta = sConsulta & "                        else" & vbCrLf
sConsulta = sConsulta & "                        c4.cod" & vbCrLf
sConsulta = sConsulta & "                        end" & vbCrLf
sConsulta = sConsulta & "                    else" & vbCrLf
sConsulta = sConsulta & "                    c5.cod" & vbCrLf
sConsulta = sConsulta & "                    end CATEGORIA," & vbCrLf
sConsulta = sConsulta & "                case when c5.cod is null then " & vbCrLf
sConsulta = sConsulta & "                        case when c4.cod is null then " & vbCrLf
sConsulta = sConsulta & "                             case when c3.cod is null then" & vbCrLf
sConsulta = sConsulta & "                                  case when c2.cod is null then" & vbCrLf
sConsulta = sConsulta & "                                       '(' + c1.cod + ') ' + c1.den" & vbCrLf
sConsulta = sConsulta & "                                  else" & vbCrLf
sConsulta = sConsulta & "                                  '(' + c1.cod + ') ' + c1.den + ' - (' + c2.cod + ') ' + c2.den" & vbCrLf
sConsulta = sConsulta & "                                  end" & vbCrLf
sConsulta = sConsulta & "                             else" & vbCrLf
sConsulta = sConsulta & "                             '(' + c1.cod + ') ' + c1.den + ' - (' + c2.cod + ') ' + c2.den + ' - (' + c3.cod + ') ' + c3.den" & vbCrLf
sConsulta = sConsulta & "                             end" & vbCrLf
sConsulta = sConsulta & "                        else" & vbCrLf
sConsulta = sConsulta & "                        '(' + c1.cod + ') ' + c1.den + ' - (' + c2.cod + ') ' + c2.den + ' - (' + c3.cod + ') ' + c3.den + ' - (' + c4.cod + ') ' + c4.den" & vbCrLf
sConsulta = sConsulta & "                        end" & vbCrLf
sConsulta = sConsulta & "                    else" & vbCrLf
sConsulta = sConsulta & "                    '(' + c1.cod + ') ' + c1.den + ' - (' + c2.cod + ') ' + c2.den + ' - (' + c3.cod + ') ' + c3.den + ' - (' + c4.cod + ') ' + c4.den + ' - (' + c5.cod + ') ' + c5.den" & vbCrLf
sConsulta = sConsulta & "                    end CATEGORIARAMA," & vbCrLf
sConsulta = sConsulta & "               " & vbCrLf
sConsulta = sConsulta & "                        " & vbCrLf
sConsulta = sConsulta & "                C1.DEN CAT1DEN," & vbCrLf
sConsulta = sConsulta & "                C2.DEN CAT2DEN," & vbCrLf
sConsulta = sConsulta & "                C3.DEN CAT3DEN," & vbCrLf
sConsulta = sConsulta & "                C4.DEN CAT4DEN," & vbCrLf
sConsulta = sConsulta & "                C5.DEN CAT5DEN," & vbCrLf
sConsulta = sConsulta & "                LE.COMENT," & vbCrLf
sConsulta = sConsulta & "                I.ANYO ANYOPROCE," & vbCrLf
sConsulta = sConsulta & "                I.PROCE CODPROCE," & vbCrLf
sConsulta = sConsulta & "                I.ID ITEM,LPA.ADJUNTOS," & vbCrLf
sConsulta = sConsulta & "                CASE WHEN @MOSTRARCINTERNO IS NULL OR @MOSTRARCINTERNO=1 OR CC1.INTERNO=0 THEN LP.VALOR1 ELSE NULL END VALOR1," & vbCrLf
sConsulta = sConsulta & "                CASE WHEN @MOSTRARCINTERNO IS NULL OR @MOSTRARCINTERNO=1 OR CC2.INTERNO=0 THEN LP.VALOR2 ELSE NULL END VALOR2," & vbCrLf
sConsulta = sConsulta & "                CC1.DEN AS DENCAMPO1,CC2.DEN AS DENCAMPO2,PROCE.DEN AS DENPROCE,LP.OBSADJUN,LP.FECENTREGA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & " FROM LINEAS_PEDIDO LP" & vbCrLf
sConsulta = sConsulta & "     INNER JOIN DEST D" & vbCrLf
sConsulta = sConsulta & "             ON LP.DEST = D.COD" & vbCrLf
sConsulta = sConsulta & "     INNER JOIN UNI U" & vbCrLf
sConsulta = sConsulta & "             ON LP.UP = U.COD" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN ITEM I " & vbCrLf
sConsulta = sConsulta & "             ON LP.ANYO = I.ANYO" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN1=I.GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND LP.PROCE=I.PROCE " & vbCrLf
sConsulta = sConsulta & "            AND LP.ITEM=I.ID" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN PROVE_ART4 PA" & vbCrLf
sConsulta = sConsulta & "             ON LP.PROVE = PA.PROVE" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN1 = PA.GMN1" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN2 = PA.GMN2" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN3 = PA.GMN3" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN4 = PA.GMN4" & vbCrLf
sConsulta = sConsulta & "            AND CASE WHEN LP.ART_INT IS NULL THEN I.ART ELSE LP.ART_INT END = PA.ART" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN ART4 " & vbCrLf
sConsulta = sConsulta & "             ON LP.GMN1=ART4.GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN2=ART4.GMN2 " & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN3=ART4.GMN3 " & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN4=ART4.GMN4 " & vbCrLf
sConsulta = sConsulta & "            AND LP.ART_INT=ART4.COD" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN ITEM_ADJ IA " & vbCrLf
sConsulta = sConsulta & "             ON LP.ITEM = IA.ITEM " & vbCrLf
sConsulta = sConsulta & "            AND LP.ANYO = IA.ANYO" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN1 = IA.GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND LP.PROCE= IA.PROCE " & vbCrLf
sConsulta = sConsulta & "            AND LP.PROVE= IA.PROVE" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN ART4 ART_AUX " & vbCrLf
sConsulta = sConsulta & "             ON ART_AUX.COD=I.ART " & vbCrLf
sConsulta = sConsulta & "            AND ART_AUX.GMN1=I.GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND ART_AUX.GMN2=I.GMN2 " & vbCrLf
sConsulta = sConsulta & "            AND ART_AUX.GMN3=I.GMN3 " & vbCrLf
sConsulta = sConsulta & "            AND ART_AUX.GMN4=I.GMN4" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN CATN1 C1" & vbCrLf
sConsulta = sConsulta & "             ON LP.CAT1 = C1.ID" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN CATN2 C2" & vbCrLf
sConsulta = sConsulta & "             ON LP.CAT2 = C2.ID" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN CATN3 C3" & vbCrLf
sConsulta = sConsulta & "             ON LP.CAT3 = C3.ID" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN CATN4 C4" & vbCrLf
sConsulta = sConsulta & "             ON LP.CAT4 = C4.ID" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN CATN5 C5" & vbCrLf
sConsulta = sConsulta & "             ON LP.CAT5 = C5.ID" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN LINEAS_EST LE" & vbCrLf
sConsulta = sConsulta & "             ON LP.LINEAS_EST = LE.ID" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN " & vbCrLf
sConsulta = sConsulta & "   (SELECT COUNT(*) AS ADJUNTOS,LINEA  FROM LINEAS_PEDIDO_ADJUN GROUP BY LINEAS_PEDIDO_ADJUN.LINEA) LPA ON LP.ID= LPA.LINEA " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN CATALOG_CAMPOS AS CC1 " & vbCrLf
sConsulta = sConsulta & "             ON CC1.ID=LP.CAMPO1 AND CC1.IDIOMA=@IDI" & vbCrLf
sConsulta = sConsulta & "            AND CC1.INTERNO = CASE WHEN @MOSTRARCINTERNO IS NULL OR @MOSTRARCINTERNO=1 THEN CC1.INTERNO ELSE 0 END" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN CATALOG_CAMPOS AS CC2 " & vbCrLf
sConsulta = sConsulta & "            ON CC2.ID=LP.CAMPO2 AND CC2.IDIOMA=@IDI" & vbCrLf
sConsulta = sConsulta & "           AND CC2.INTERNO = CASE WHEN @MOSTRARCINTERNO IS NULL OR @MOSTRARCINTERNO=1 THEN CC2.INTERNO ELSE 0 END" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN PROCE " & vbCrLf
sConsulta = sConsulta & "       ON LP.ANYO=PROCE.ANYO AND LP.GMN1=PROCE.GMN1 AND LP.PROCE=PROCE.COD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & " WHERE LP.ORDEN=@ORDENID" & vbCrLf
sConsulta = sConsulta & "   AND CASE WHEN LP.SOLICIT IS NULL AND @SOLICIT IS NULL THEN -1 ELSE LP.SOLICIT END " & vbCrLf
sConsulta = sConsulta & "       = CASE WHEN LP.SOLICIT IS NULL AND @SOLICIT IS NULL THEN -1 ELSE ISNULL(@SOLICIT,LP.SOLICIT) END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_CAMBIARREUNION]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_CAMBIARREUNION]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE    PROCEDURE SP_CAMBIARREUNION (@FECHAANTERIOR DATETIME,@FECHANUEVA DATETIME,@RES INT OUTPUT)  AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE @REF AS VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & " SET @REF = (SELECT REF FROM REU WHERE FECHA=@FECHAANTERIOR)" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & " INSERT INTO REU (FECHA,REF) VALUES (@FECHANUEVA,@REF)" & vbCrLf
sConsulta = sConsulta & "           IF @@ERROR <> 0  GOTO ERR_" & vbCrLf
sConsulta = sConsulta & "  " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE REU_PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "declare @DIF int" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "set @dif = datediff(minute,@fechaanterior,@fechanueva)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE REU_PROCE SET FECHA=@FECHANUEVA," & vbCrLf
sConsulta = sConsulta & "    HORA=dateadd(minute,@dif,Convert(datetime,(Convert(varchar(10),@FECHAANTERIOR,110) + ' ' + Convert(varchar(8),HORA,108)),110))" & vbCrLf
sConsulta = sConsulta & " WHERE FECHA=@FECHAANTERIOR" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET FECULTREU=@FECHANUEVA  " & vbCrLf
sConsulta = sConsulta & "  FROM PROCE P INNER JOIN REU_PROCE RP ON P.ANYO = RP.ANYO AND P.GMN1 = RP.GMN1 AND P.COD = RP.PROCE" & vbCrLf
sConsulta = sConsulta & " WHERE RP.FECHA = @FECHANUEVA" & vbCrLf
sConsulta = sConsulta & "   AND P.FECULTREU=@FECHAANTERIOR" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM REU WHERE FECHA=@FECHAANTERIOR" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE REU_PROCE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "  " & vbCrLf
sConsulta = sConsulta & "ERR_: " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & " SET @RES = @@ERROR" & vbCrLf
sConsulta = sConsulta & " RETURN " & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ART4_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[ART4_COD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE ART4_COD (@OLD VARCHAR(50),@NEW VARCHAR(50),@GMN1 VARCHAR(50),@GMN2 VARCHAR(50),@GMN3 VARCHAR(50),@GMN4 VARCHAR(50))  AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT ON" & vbCrLf
sConsulta = sConsulta & "BEGIN TRANSACTION" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR FOR SELECT COD FROM ART4 WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@GMN4 AND COD=@OLD" & vbCrLf
sConsulta = sConsulta & "OPEN C" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C INTO @VAR_COD" & vbCrLf
sConsulta = sConsulta & "IF @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ADJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ESP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATALOG_LIN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_PEDIDO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART_UP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FAVORITOS_LINEAS_PEDIDO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_ART NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "-----" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4 SET COD=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@GMN4 AND COD=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4_ADJ SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@GMN4 AND ART=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@GMN4 AND ART=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART4 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@GMN4 AND ART=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4_ESP SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@GMN4 AND ART=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4_ATRIB SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@GMN4 AND ART=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CATALOG_LIN SET ART_INT=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@GMN4 AND ART_INT=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE LINEAS_PEDIDO SET ART_INT=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@GMN4 AND ART_INT=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART4_ATRIB SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@GMN4 AND ART=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART_UP SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@GMN4 AND ART=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART4_ADJUN SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@GMN4 AND ART=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE FAVORITOS_LINEAS_PEDIDO SET ART_INT=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@GMN4 AND ART_INT=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES_ART SET COD=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@GMN4 AND COD=@OLD" & vbCrLf
sConsulta = sConsulta & "----" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ADJ CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ESP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATALOG_LIN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_PEDIDO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART_UP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FAVORITOS_LINEAS_PEDIDO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_ART CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "----" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close C" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT OFF" & vbCrLf
sConsulta = sConsulta & "COMMIT TRANSACTION" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GMN1_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[GMN1_COD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE GMN1_COD (@OLD VARCHAR(50),@NEW VARCHAR(50),@INT_ORIGEN VARCHAR(1),@USUARIO VARCHAR(50))  AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_DEN VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT ON" & vbCrLf
sConsulta = sConsulta & "BEGIN TRANSACTION" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR FOR SELECT COD,DEN FROM GMN1 WHERE COD=@OLD" & vbCrLf
sConsulta = sConsulta & "OPEN C" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C INTO @VAR_COD,@VAR_DEN" & vbCrLf
sConsulta = sConsulta & "IF @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART1_ADJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART2_ADJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART3_ADJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ADJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE DETALLE_PUJAS NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ADJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ADJREU NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ESP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OBJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OFE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OFE_WEB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRES NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESCON NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESPROY NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESCON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESCON4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFEMIN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_GRUPO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_ESP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_WEB_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_WEB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PER NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_PET NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_POT NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVENOTIF NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE REU_PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ESP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4_ACT NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATALOG_LIN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_PEDIDO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART_UP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_DEF NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO_DEF NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO_ESP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_GR_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_GR_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ITEM_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ITEM_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_SOBRE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GRUPO_OFE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GRUPO_OFE_REU NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USAR_GR_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_CONTR NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE SOBRE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FAVORITOS_LINEAS_PEDIDO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_NUMDEC NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_ART NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN1 SET COD=@NEW WHERE COD=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART1_ADJ SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART2_ADJ SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART3_ADJ SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4_ADJ SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE BLOQUEO_PROCE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE COM_GMN1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE COM_GMN2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE COM_GMN3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE COM_GMN4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE DETALLE_PUJAS SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_ADJ SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_ADJREU SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_ESP SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_OBJ SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_OFE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_OFE_WEB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_PRES SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_PRESCON SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_PRESCON3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_PRESCON4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_PRESPROY SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_UON1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_UON2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_UON3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFEMIN SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES_MAT1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES_MAT2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES_MAT3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES_MAT4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_ESP SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE_ADJUN SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE_WEB_ADJUN SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE_WEB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PER SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE_PET SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE_POT SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_GMN1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_GMN2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_GMN3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_GMN4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVENOTIF SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE REU_PROCE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_PROCE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_PROVE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_GRUPO SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_UON1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_UON2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_UON3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_PRESCON3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_PRESCON4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_PROVE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4_ESP  SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN4_ACT SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART4_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN4_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4_ATRIB  SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CATALOG_LIN  SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE LINEAS_PEDIDO  SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART_UP  SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_DEF SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GRUPO SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GRUPO_DEF SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GRUPO_ESP SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PRES1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PRES2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PRES3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PRES4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_PRES1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_PRES2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_PRES3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_PRES4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_UON1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_UON2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_UON3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_UON1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_UON2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_UON3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_GR_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_GR_ADJUN SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_ITEM_ADJUN SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_ITEM_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ALL SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ALL_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ALL_PROVE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_GRUPO SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_GRUPO_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_GRUPO_PROVE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ITEM SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ITEM_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ITEM_PROVE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_PROCE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_PROCE_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_PROCE_PROVE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_PROCE_SOBRE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GRUPO_OFE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GRUPO_OFE_REU SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE USAR_GR_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_CONTR SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE SOBRE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART4_ADJUN SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE FAVORITOS_LINEAS_PEDIDO SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_NUMDEC SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES_ART SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART1_ADJ CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART2_ADJ CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART3_ADJ CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ADJ CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE DETALLE_PUJAS CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ADJ CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ADJREU CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ESP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OBJ CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OFE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OFE_WEB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRES CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESCON CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESCON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESCON4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESPROY CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFEMIN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_ESP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_WEB_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_WEB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PER CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_PET CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_POT CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVENOTIF CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE REU_PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_GRUPO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ESP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4_ACT CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATALOG_LIN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_PEDIDO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART_UP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_DEF CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO_DEF CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO_ESP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_GR_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_GR_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ITEM_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ITEM_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_SOBRE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GRUPO_OFE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GRUPO_OFE_REU CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USAR_GR_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_CONTR CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE SOBRE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FAVORITOS_LINEAS_PEDIDO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_NUMDEC CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_ART CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "--INTEGRACION" & vbCrLf
sConsulta = sConsulta & "IF @INT_ORIGEN <> '' " & vbCrLf
sConsulta = sConsulta & "INSERT INTO LOG_GMN (ACCION,GMN1,DEN,COD_NEW,ORIGEN,USU) VALUES ('C',@VAR_COD,@VAR_DEN,@NEW,@INT_ORIGEN,@USUARIO)" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close C" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "COMMIT TRANSACTION" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT OFF" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GMN2_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[GMN2_COD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE GMN2_COD (@OLD VARCHAR(50),@NEW VARCHAR(50),@GMN1 VARCHAR(50),@INT_ORIGEN VARCHAR(1),@USUARIO VARCHAR(50))  AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_DEN VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT ON" & vbCrLf
sConsulta = sConsulta & "BEGIN TRANSACTION" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR FOR SELECT COD,DEN FROM GMN2 WHERE GMN1=@GMN1 AND COD=@OLD" & vbCrLf
sConsulta = sConsulta & "OPEN C" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C INTO @VAR_COD,@VAR_DEN" & vbCrLf
sConsulta = sConsulta & "IF @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART2_ADJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART3_ADJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ADJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ESP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4_ACT NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATALOG_LIN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_PEDIDO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART_UP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FAVORITOS_LINEAS_PEDIDO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_ART NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "----" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN2 SET COD=@NEW WHERE GMN1=@GMN1 AND COD=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART2 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART2_ADJ SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART3 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART3_ADJ SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4_ADJ SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE COM_GMN2 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE COM_GMN3 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE COM_GMN4 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN3 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN4 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES_MAT2 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES_MAT3 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES_MAT4 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_GMN2 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_GMN3 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_GMN4 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_PROCE SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART2 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART3 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART4 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4_ESP SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN4_ACT SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4_ATRIB SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN4_ATRIB SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART4_ATRIB SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CATALOG_LIN SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE LINEAS_PEDIDO SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART_UP SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART4_ADJUN SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE FAVORITOS_LINEAS_PEDIDO SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES_ART SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "----" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART2_ADJ CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART3_ADJ CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ADJ CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ESP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4_ACT CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATALOG_LIN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_PEDIDO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART_UP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FAVORITOS_LINEAS_PEDIDO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_ART CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "--INTEGRACION" & vbCrLf
sConsulta = sConsulta & "IF @INT_ORIGEN <> '' " & vbCrLf
sConsulta = sConsulta & "INSERT INTO LOG_GMN (ACCION,GMN1,GMN2,DEN,COD_NEW,ORIGEN,USU) VALUES ('C',@GMN1,@VAR_COD,@VAR_DEN,@NEW,@INT_ORIGEN,@USUARIO)" & vbCrLf
sConsulta = sConsulta & "----" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close C" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT OFF" & vbCrLf
sConsulta = sConsulta & "COMMIT TRANSACTION" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GMN3_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[GMN3_COD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE GMN3_COD (@OLD VARCHAR(50),@NEW VARCHAR(50),@GMN1 VARCHAR(50),@GMN2 VARCHAR(50),@INT_ORIGEN VARCHAR(1),@USUARIO VARCHAR(50))  AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_DEN VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT ON" & vbCrLf
sConsulta = sConsulta & "BEGIN TRANSACTION" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR FOR SELECT COD,DEN FROM GMN3 WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND COD=@OLD" & vbCrLf
sConsulta = sConsulta & "OPEN C" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C INTO @VAR_COD,@VAR_DEN" & vbCrLf
sConsulta = sConsulta & "IF @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART3_ADJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ADJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ESP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4_ACT NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATALOG_LIN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_PEDIDO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART_UP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FAVORITOS_LINEAS_PEDIDO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_ART NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "----" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN3 SET COD=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND COD=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART3 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART3_ADJ SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4_ADJ SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE COM_GMN3 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE COM_GMN4 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN4 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES_MAT3 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES_MAT4 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_GMN3 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_GMN4 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_PROCE SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART3 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART4 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4_ESP SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN4_ACT SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4_ATRIB SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN4_ATRIB SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CATALOG_LIN SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE LINEAS_PEDIDO SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART_UP SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART4_ATRIB SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART4_ADJUN SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE FAVORITOS_LINEAS_PEDIDO SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES_ART SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "----" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART3_ADJ CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ADJ CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ESP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4_ACT CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATALOG_LIN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_PEDIDO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART_UP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FAVORITOS_LINEAS_PEDIDO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_ART CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "--INTEGRACION" & vbCrLf
sConsulta = sConsulta & "IF @INT_ORIGEN <> '' " & vbCrLf
sConsulta = sConsulta & "INSERT INTO LOG_GMN (ACCION,GMN1,GMN2,GMN3,DEN,COD_NEW,ORIGEN,USU) VALUES ('C',@GMN1,@GMN2,@VAR_COD,@VAR_DEN,@NEW,@INT_ORIGEN,@USUARIO)" & vbCrLf
sConsulta = sConsulta & "----" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close C" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "COMMIT TRANSACTION" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT OFF" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GMN4_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[GMN4_COD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE GMN4_COD (@OLD VARCHAR(50),@NEW VARCHAR(50),@GMN1 VARCHAR(50),@GMN2 VARCHAR(50),@GMN3 VARCHAR(50),@INT_ORIGEN VARCHAR(1),@USUARIO VARCHAR(50))  AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_DEN VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT ON" & vbCrLf
sConsulta = sConsulta & "BEGIN TRANSACTION" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR FOR SELECT COD,DEN FROM GMN4 WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND COD=@OLD" & vbCrLf
sConsulta = sConsulta & "OPEN C" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C INTO @VAR_COD,@VAR_DEN" & vbCrLf
sConsulta = sConsulta & "IF @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ADJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ESP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4_ACT NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATALOG_LIN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_PEDIDO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART_UP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FAVORITOS_LINEAS_PEDIDO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_ART NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "----" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN4 SET COD=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND COD=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4 SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4_ADJ SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE COM_GMN4 SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES_MAT4 SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_GMN4 SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_PROCE SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART4 SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4_ESP SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN4_ACT SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART4_ATRIB SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN4_ATRIB SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4_ATRIB SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CATALOG_LIN SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE LINEAS_PEDIDO SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART_UP SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART4_ADJUN SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE FAVORITOS_LINEAS_PEDIDO SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES_ART SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD" & vbCrLf
sConsulta = sConsulta & "----" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ADJ CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ESP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4_ACT CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATALOG_LIN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_PEDIDO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART_UP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FAVORITOS_LINEAS_PEDIDO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_ART CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "--INTEGRACION" & vbCrLf
sConsulta = sConsulta & "IF @INT_ORIGEN <> '' " & vbCrLf
sConsulta = sConsulta & "INSERT INTO LOG_GMN (ACCION,GMN1,GMN2,GMN3,GMN4,DEN,COD_NEW,ORIGEN,USU) VALUES ('C',@GMN1,@GMN2,@GMN3,@VAR_COD,@VAR_DEN,@NEW,@INT_ORIGEN,@USUARIO)" & vbCrLf
sConsulta = sConsulta & "----" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close C" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT OFF" & vbCrLf
sConsulta = sConsulta & "COMMIT TRANSACTION" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion2_14_00_00A2_14_00_01() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim oADOCon As ADODB.Connection
Dim adores As ADODB.Recordset
Dim dFechaHora As Date

On Error GoTo error
    
    Set oADOCon = New ADODB.Connection
    oADOCon.Open "Provider=SQLOLEDB.1;Auto Translate=FALSE;Server=" & gServer & ";Database=" & gBD & ";", gUID, gPWD
    oADOCon.CursorLocation = adUseClient
    oADOCon.CommandTimeout = 120

    oADOCon.Execute "BEGIN TRANSACTION"
    oADOCon.Execute "SET XACT_ABORT OFF"
    bTransaccionEnCurso = True
    
    frmProgreso.lblDetalle = "Actualizar fechas de reuni�n"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
        
    sConsulta = ""
    sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PROCE_TG_INSUPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
    sConsulta = sConsulta & "drop trigger [dbo].[PROCE_TG_INSUPD]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = ""
    sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PROCE_TG_UPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
    sConsulta = sConsulta & "drop trigger [dbo].[PROCE_TG_UPD]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = ""
    sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PROCE_TG_UPD_DATVAR]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
    sConsulta = sConsulta & "drop trigger [dbo].[PROCE_TG_UPD_DATVAR]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = ""
    sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ITEM_ADJREU_TG_INSUPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
    sConsulta = sConsulta & "drop trigger [dbo].[ITEM_ADJREU_TG_INSUPD]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = ""
    sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ITEM_OBJ_TG_INSUPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
    sConsulta = sConsulta & "drop trigger [dbo].[ITEM_OBJ_TG_INSUPD]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = ""
    sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ITEM_PRES_TG_INSUPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
    sConsulta = sConsulta & "drop trigger [dbo].[ITEM_PRES_TG_INSUPD]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    oADOCon.Execute "ALTER TABLE REU NOCHECK CONSTRAINT ALL"
    oADOCon.Execute "ALTER TABLE REU_PROCE NOCHECK CONSTRAINT ALL"
    oADOCon.Execute "ALTER TABLE PROCE NOCHECK CONSTRAINT ALL"
    oADOCon.Execute "ALTER TABLE AR_PROCE NOCHECK CONSTRAINT ALL"
    oADOCon.Execute "ALTER TABLE LOG_ADJ NOCHECK CONSTRAINT ALL"
    oADOCon.Execute "ALTER TABLE ITEM_ADJREU NOCHECK CONSTRAINT ALL"
    oADOCon.Execute "ALTER TABLE ITEM_OBJ NOCHECK CONSTRAINT ALL"
    oADOCon.Execute "ALTER TABLE ITEM_PRES NOCHECK CONSTRAINT ALL"
    oADOCon.Execute "ALTER TABLE GRUPO_OFE_REU NOCHECK CONSTRAINT ALL"

    Set adores = New ADODB.Recordset
    adores.Open "SELECT FECHA,MIN(HORA) H FROM REU_PROCE GROUP BY FECHA ORDER BY FECHA", oADOCon, adOpenForwardOnly, adLockReadOnly
    While Not adores.EOF
        frmProgreso.lblDetalle = "Actualizar fechas de reuni�n"
        frmProgreso.lblDetalle.Refresh
        If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
        frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

        dFechaHora = CDate(FormatDateTime(adores("FECHA").Value, vbShortDate) & " " & FormatDateTime(adores("H").Value, vbShortTime))
        sConsulta = "UPDATE REU SET FECHA=" & DateToSQLTimeDate(dFechaHora) & " WHERE FECHA = " & DateToSQLTimeDate(adores("FECHA").Value)
        oADOCon.Execute sConsulta
        sConsulta = "UPDATE REU_PROCE SET FECHA=" & DateToSQLTimeDate(dFechaHora) & " WHERE FECHA = " & DateToSQLTimeDate(adores("FECHA").Value)
        oADOCon.Execute sConsulta
        sConsulta = "UPDATE PROCE SET FECULTREU=" & DateToSQLTimeDate(dFechaHora) & " WHERE FECULTREU = " & DateToSQLTimeDate(adores("FECHA").Value) & " AND ADJDIR=0"
        oADOCon.Execute sConsulta
        sConsulta = "UPDATE AR_PROCE SET FECULTREU=" & DateToSQLTimeDate(dFechaHora) & " WHERE FECULTREU = " & DateToSQLTimeDate(adores("FECHA").Value) & " AND ADJDIR=0"
        oADOCon.Execute sConsulta
        sConsulta = "UPDATE LOG_ADJ SET FECULTREU=" & DateToSQLTimeDate(dFechaHora) & " WHERE FECULTREU = " & DateToSQLTimeDate(adores("FECHA").Value) & " AND ADJDIR=0"
        oADOCon.Execute sConsulta
        sConsulta = "UPDATE ITEM_ADJREU SET FECREU=" & DateToSQLTimeDate(dFechaHora) & " WHERE FECREU = " & DateToSQLTimeDate(adores("FECHA").Value)
        oADOCon.Execute sConsulta
        sConsulta = "UPDATE ITEM_OBJ SET FECREU=" & DateToSQLTimeDate(dFechaHora) & " WHERE FECREU = " & DateToSQLTimeDate(adores("FECHA").Value)
        oADOCon.Execute sConsulta
        sConsulta = "UPDATE ITEM_PRES SET FECREU=" & DateToSQLTimeDate(dFechaHora) & " WHERE FECREU = " & DateToSQLTimeDate(adores("FECHA").Value)
        oADOCon.Execute sConsulta
        sConsulta = "UPDATE GRUPO_OFE_REU SET FECREU=" & DateToSQLTimeDate(dFechaHora) & " WHERE FECREU = " & DateToSQLTimeDate(adores("FECHA").Value)
        oADOCon.Execute sConsulta
        
        adores.MoveNext
    Wend
    adores.Close
    Set adores = Nothing
    
    oADOCon.Execute "ALTER TABLE REU CHECK CONSTRAINT ALL"
    oADOCon.Execute "ALTER TABLE REU_PROCE CHECK CONSTRAINT ALL"
    oADOCon.Execute "ALTER TABLE PROCE CHECK CONSTRAINT ALL"
    oADOCon.Execute "ALTER TABLE AR_PROCE CHECK CONSTRAINT ALL"
    oADOCon.Execute "ALTER TABLE LOG_ADJ CHECK CONSTRAINT ALL"
    oADOCon.Execute "ALTER TABLE ITEM_ADJREU CHECK CONSTRAINT ALL"
    oADOCon.Execute "ALTER TABLE ITEM_OBJ CHECK CONSTRAINT ALL"
    oADOCon.Execute "ALTER TABLE ITEM_PRES CHECK CONSTRAINT ALL"
    oADOCon.Execute "ALTER TABLE GRUPO_OFE_REU CHECK CONSTRAINT ALL"
    
    sConsulta = "CREATE TRIGGER PROCE_TG_INSUPD ON dbo.PROCE" & vbCrLf
    sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
    sConsulta = sConsulta & "AS" & vbCrLf
    sConsulta = sConsulta & "DECLARE @ANYO INT,@GMN1 VARCHAR(50),@COD SMALLINT" & vbCrLf
    sConsulta = sConsulta & "DECLARE curTG_PROCE_Ins CURSOR FOR SELECT ANYO,GMN1,COD FROM INSERTED" & vbCrLf
    sConsulta = sConsulta & "OPEN curTG_PROCE_Ins" & vbCrLf
    sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_Ins INTO @ANYO,@GMN1,@COD" & vbCrLf
    sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
    sConsulta = sConsulta & "BEGIN" & vbCrLf
    sConsulta = sConsulta & "UPDATE PROCE SET FECACT=GETDATE() WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@COD" & vbCrLf
    sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_Ins INTO @ANYO,@GMN1,@COD" & vbCrLf
    sConsulta = sConsulta & "End" & vbCrLf
    sConsulta = sConsulta & "Close curTG_PROCE_Ins" & vbCrLf
    sConsulta = sConsulta & "DEALLOCATE curTG_PROCE_Ins" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    oADOCon.Execute sConsulta

    sConsulta = "CREATE TRIGGER PROCE_TG_UPD ON dbo.PROCE" & vbCrLf
    sConsulta = sConsulta & "FOR UPDATE" & vbCrLf
    sConsulta = sConsulta & "AS" & vbCrLf
    sConsulta = sConsulta & "DECLARE @ANYO INT,@GMN1 VARCHAR(50),@COD SMALLINT" & vbCrLf
    sConsulta = sConsulta & "DECLARE @CALCPEND TINYINT, @CALCPEND_ANT TINYINT" & vbCrLf
    sConsulta = sConsulta & "DECLARE curTG_PROCE_Upd CURSOR LOCAL FOR SELECT I.ANYO,I.GMN1,I.COD,I.CALCPEND,D.CALCPEND FROM INSERTED I" & vbCrLf
    sConsulta = sConsulta & "     INNER JOIN DELETED D ON I.ANYO=D.ANYO AND I.GMN1=D.GMN1 AND I.COD=D.COD" & vbCrLf
    sConsulta = sConsulta & "OPEN curTG_PROCE_Upd" & vbCrLf
    sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_Upd INTO @ANYO,@GMN1,@COD,@CALCPEND,@CALCPEND_ANT" & vbCrLf
    sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
    sConsulta = sConsulta & "BEGIN" & vbCrLf
    sConsulta = sConsulta & "   IF UPDATE(CALCPEND)" & vbCrLf
    sConsulta = sConsulta & "   IF @CALCPEND = 0 AND @CALCPEND_ANT=1" & vbCrLf
    sConsulta = sConsulta & "                  UPDATE PROCE SET CALCFEC=GETDATE() WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@COD" & vbCrLf
    sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_Upd INTO @ANYO,@GMN1,@COD,@CALCPEND,@CALCPEND_ANT" & vbCrLf
    sConsulta = sConsulta & "End" & vbCrLf
    sConsulta = sConsulta & "Close curTG_PROCE_Upd" & vbCrLf
    sConsulta = sConsulta & "DEALLOCATE curTG_PROCE_Upd" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    oADOCon.Execute sConsulta

    sConsulta = "CREATE TRIGGER PROCE_TG_UPD_DATVAR ON dbo.PROCE" & vbCrLf
    sConsulta = sConsulta & "FOR UPDATE" & vbCrLf
    sConsulta = sConsulta & "AS" & vbCrLf
    sConsulta = sConsulta & "DECLARE @ANYO SMALLINT,@GMN1 VARCHAR(5),@PROCE SMALLINT,@DATO SMALLINT" & vbCrLf
    sConsulta = sConsulta & "DECLARE @DESTOLD VARCHAR(20),@DESTNEW VARCHAR(20)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @PAGOLD VARCHAR(20),@PAGNEW VARCHAR(20)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FECINIOLD DATETIME,@FECININEW DATETIME" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FECFINOLD DATETIME,@FECFINNEW DATETIME" & vbCrLf
    sConsulta = sConsulta & "DECLARE @PROVEACTOLD VARCHAR(50)DECLARE @PROVEACTNEW VARCHAR(50)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SOLICITOLD INT,@SOLICITNEW INT" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "DECLARE curTG_PROCE_Upd_DATVAR CURSOR LOCAL FOR " & vbCrLf
    sConsulta = sConsulta & "SELECT I.ANYO,I.GMN1,I.COD,D.DEST,I.DEST,D.PAG,I.PAG,D.FECINI,I.FECINI,D.FECFIN,I.FECFIN,D.PROVEACT,I.PROVEACT,D.SOLICIT,I.SOLICIT" & vbCrLf
    sConsulta = sConsulta & "FROM DELETED D " & vbCrLf
    sConsulta = sConsulta & "INNER JOIN INSERTED I" & vbCrLf
    sConsulta = sConsulta & "ON D.ANYO=I.ANYO AND D.GMN1=I.GMN1 AND D.COD=I.COD" & vbCrLf
    sConsulta = sConsulta & "OPEN curTG_PROCE_Upd_DATVAR" & vbCrLf
    sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_Upd_DATVAR INTO @ANYO,@GMN1,@PROCE, @DESTOLD,@DESTNEW,@PAGOLD,@PAGNEW,@FECINIOLD,@FECININEW,@FECFINOLD,@FECFINNEW,@PROVEACTOLD,@PROVEACTNEW,@SOLICITOLD,@SOLICITNEW" & vbCrLf
    sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
    sConsulta = sConsulta & "BEGIN" & vbCrLf
    sConsulta = sConsulta & "--DEST" & vbCrLf
    sConsulta = sConsulta & "IF ISNULL(@DESTOLD,'')<>ISNULL(@DESTNEW,'')" & vbCrLf
    sConsulta = sConsulta & "BEGIN" & vbCrLf
    sConsulta = sConsulta & "         SELECT @DATO=DEST FROM PROCE_DEF WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
    sConsulta = sConsulta & "         IF @DATO=1 --Definido a nivel de proceso" & vbCrLf
    sConsulta = sConsulta & "         BEGIN" & vbCrLf
    sConsulta = sConsulta & "       UPDATE ITEM SET DEST=@DESTNEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
    sConsulta = sConsulta & "         End" & vbCrLf
    sConsulta = sConsulta & "End" & vbCrLf
    sConsulta = sConsulta & "--PAG" & vbCrLf
    sConsulta = sConsulta & "IF ISNULL(@PAGOLD,'')<>ISNULL(@PAGNEW,'')" & vbCrLf
    sConsulta = sConsulta & "BEGIN" & vbCrLf
    sConsulta = sConsulta & "         SELECT @DATO=PAG FROM PROCE_DEF WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
    sConsulta = sConsulta & "         IF @DATO=1" & vbCrLf
    sConsulta = sConsulta & "         BEGIN" & vbCrLf
    sConsulta = sConsulta & "       UPDATE ITEM SET PAG=@PAGNEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
    sConsulta = sConsulta & "         End" & vbCrLf
    sConsulta = sConsulta & "End" & vbCrLf
    sConsulta = sConsulta & "--FECINI" & vbCrLf
    sConsulta = sConsulta & "IF ISNULL(@FECINIOLD,'')<>ISNULL(@FECININEW,'')" & vbCrLf
    sConsulta = sConsulta & "BEGIN" & vbCrLf
    sConsulta = sConsulta & "         SELECT @DATO=FECSUM FROM PROCE_DEF WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
    sConsulta = sConsulta & "         IF @DATO=1" & vbCrLf
    sConsulta = sConsulta & "         BEGIN" & vbCrLf
    sConsulta = sConsulta & "       UPDATE ITEM SET FECINI=@FECININEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
    sConsulta = sConsulta & "         End" & vbCrLf
    sConsulta = sConsulta & "End" & vbCrLf
    sConsulta = sConsulta & "--FECFIN" & vbCrLf
    sConsulta = sConsulta & "IF ISNULL(@FECFINOLD,'')<>ISNULL(@FECFINNEW,'')" & vbCrLf
    sConsulta = sConsulta & "BEGIN" & vbCrLf
    sConsulta = sConsulta & "         SELECT @DATO=FECSUM FROM PROCE_DEF WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
    sConsulta = sConsulta & "         IF @DATO=1" & vbCrLf
    sConsulta = sConsulta & "         BEGIN" & vbCrLf
    sConsulta = sConsulta & "       UPDATE ITEM SET FECFIN=@FECFINNEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
    sConsulta = sConsulta & "         End" & vbCrLf
    sConsulta = sConsulta & "End" & vbCrLf
    sConsulta = sConsulta & "--PROVEACT" & vbCrLf
    sConsulta = sConsulta & "IF ISNULL(@PROVEACTOLD,'')<>ISNULL(@PROVEACTNEW,'')" & vbCrLf
    sConsulta = sConsulta & "BEGIN" & vbCrLf
    sConsulta = sConsulta & "         SELECT @DATO=PROVE FROM PROCE_DEF WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
    sConsulta = sConsulta & "         IF @DATO=1" & vbCrLf
    sConsulta = sConsulta & "         BEGIN" & vbCrLf
    sConsulta = sConsulta & "       UPDATE ITEM SET PROVEACT=@PROVEACTNEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
    sConsulta = sConsulta & "         End" & vbCrLf
    sConsulta = sConsulta & "End" & vbCrLf
    sConsulta = sConsulta & "--SOLICIT" & vbCrLf
    sConsulta = sConsulta & "IF ISNULL(@SOLICITOLD,'')<>ISNULL(@SOLICITNEW,'')" & vbCrLf
    sConsulta = sConsulta & "BEGIN" & vbCrLf
    sConsulta = sConsulta & "         SELECT @DATO=SOLICIT FROM PROCE_DEF WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
    sConsulta = sConsulta & "         IF @DATO=1 --Definido a nivel de proceso" & vbCrLf
    sConsulta = sConsulta & "         BEGIN" & vbCrLf
    sConsulta = sConsulta & "       UPDATE ITEM SET SOLICIT=@SOLICITNEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
    sConsulta = sConsulta & "         End" & vbCrLf
    sConsulta = sConsulta & "End" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_Upd_DATVAR INTO @ANYO,@GMN1,@PROCE, @DESTOLD,@DESTNEW,@PAGOLD,@PAGNEW,@FECINIOLD,@FECININEW,@FECFINOLD,@FECFINNEW,@PROVEACTOLD,@PROVEACTNEW,@SOLICITOLD,@SOLICITNEW" & vbCrLf
    sConsulta = sConsulta & "End" & vbCrLf
    sConsulta = sConsulta & "Close curTG_PROCE_Upd_DATVAR" & vbCrLf
    sConsulta = sConsulta & "DEALLOCATE curTG_PROCE_Upd_DATVAR" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    oADOCon.Execute sConsulta
    
    sConsulta = "CREATE TRIGGER ITEM_ADJREU_TG_INSUPD ON [ITEM_ADJREU]" & vbCrLf
    sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
    sConsulta = sConsulta & "AS" & vbCrLf
    sConsulta = sConsulta & "DECLARE @ANYO INT,@GMN1 VARCHAR(50),@PROCE INT, @ITEM INT,@PROVE VARCHAR(50), @FECREU DATETIME" & vbCrLf
    sConsulta = sConsulta & "DECLARE curTG_ITEM_ADJREU_InsUpd CURSOR FOR SELECT ANYO,GMN1,PROCE,ITEM,PROVE,FECREU FROM INSERTED" & vbCrLf
    sConsulta = sConsulta & "OPEN curTG_ITEM_ADJREU_InsUpd" & vbCrLf
    sConsulta = sConsulta & "FETCH NEXT FROM curTG_ITEM_ADJREU_InsUpd INTO @ANYO,@GMN1,@PROCE,@ITEM,@PROVE,@FECREU" & vbCrLf
    sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
    sConsulta = sConsulta & "BEGIN" & vbCrLf
    sConsulta = sConsulta & "UPDATE ITEM_ADJREU SET FECACT=GETDATE() WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM AND PROVE=@PROVE AND FECREU=@FECREU" & vbCrLf
    sConsulta = sConsulta & "FETCH NEXT FROM curTG_ITEM_ADJREU_InsUpd INTO @ANYO,@GMN1,@PROCE,@ITEM,@PROVE, @FECREU" & vbCrLf
    sConsulta = sConsulta & "End" & vbCrLf
    sConsulta = sConsulta & "Close curTG_ITEM_ADJREU_InsUpd" & vbCrLf
    sConsulta = sConsulta & "DEALLOCATE curTG_ITEM_ADJREU_InsUpd" & vbCrLf
    sConsulta = sConsulta & ""
    oADOCon.Execute sConsulta
    
    sConsulta = "CREATE TRIGGER ITEM_OBJ_TG_INSUPD ON [ITEM_OBJ]" & vbCrLf
    sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
    sConsulta = sConsulta & "AS" & vbCrLf
    sConsulta = sConsulta & "DECLARE @ANYO AS INTEGER" & vbCrLf
    sConsulta = sConsulta & "DECLARE @GMN1 AS VARCHAR(50)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @PROCE AS INTEGER" & vbCrLf
    sConsulta = sConsulta & "DECLARE @ITEM AS INTEGER" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FECREU AS DATETIME" & vbCrLf
    sConsulta = sConsulta & "DECLARE @OBJ AS FLOAT" & vbCrLf
    sConsulta = sConsulta & "DECLARE curTG_ITEM_OBJ_Ins CURSOR FOR SELECT ANYO,GMN1,PROCE,ITEM,FECREU,OBJ FROM INSERTED" & vbCrLf
    sConsulta = sConsulta & "OPEN curTG_ITEM_OBJ_Ins" & vbCrLf
    sConsulta = sConsulta & "FETCH NEXT FROM curTG_ITEM_OBJ_Ins INTO @ANYO,@GMN1,@PROCE,@ITEM,@FECREU,@OBJ" & vbCrLf
    sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
    sConsulta = sConsulta & "BEGIN" & vbCrLf
    sConsulta = sConsulta & "/* Si no hay ning�na reunion posterior entonces ponemos este como actual del item */" & vbCrLf
    sConsulta = sConsulta & "IF (SELECT COUNT(*) FROM REU_PROCE WHERE  ANYO=@ANYO AND PROCE=@PROCE AND GMN1=@GMN1 AND FECHA > @FECREU AND RES>0) = 0" & vbCrLf
    sConsulta = sConsulta & "UPDATE  ITEM  SET OBJ=@OBJ,FECHAOBJ=@FECREU  WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ID=@ITEM" & vbCrLf
    sConsulta = sConsulta & "FETCH NEXT FROM curTG_ITEM_OBJ_Ins INTO @ANYO,@GMN1,@PROCE,@ITEM,@FECREU,@OBJ" & vbCrLf
    sConsulta = sConsulta & "End" & vbCrLf
    sConsulta = sConsulta & "Close curTG_ITEM_OBJ_Ins" & vbCrLf
    sConsulta = sConsulta & "DEALLOCATE curTG_ITEM_OBJ_Ins" & vbCrLf
    sConsulta = sConsulta & ""
    oADOCon.Execute sConsulta
    
    sConsulta = "CREATE TRIGGER ITEM_PRES_TG_INSUPD ON [ITEM_PRES]" & vbCrLf
    sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
    sConsulta = sConsulta & "AS" & vbCrLf
    sConsulta = sConsulta & "DECLARE @ANYO AS INTEGER" & vbCrLf
    sConsulta = sConsulta & "DECLARE @GMN1 AS VARCHAR(50)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @PROCE AS INTEGER" & vbCrLf
    sConsulta = sConsulta & "DECLARE @ITEM AS INTEGER" & vbCrLf
    sConsulta = sConsulta & "DECLARE @FECREU AS DATETIME" & vbCrLf
    sConsulta = sConsulta & "DECLARE @PRES  AS FLOAT" & vbCrLf
    sConsulta = sConsulta & "DECLARE curTG_ITEM_PRES_Ins CURSOR FOR SELECT ANYO,GMN1,PROCE,ITEM,FECREU,PRES FROM INSERTED" & vbCrLf
    sConsulta = sConsulta & "OPEN curTG_ITEM_PRES_Ins" & vbCrLf
    sConsulta = sConsulta & "FETCH NEXT FROM curTG_ITEM_PRES_Ins INTO @ANYO,@GMN1,@PROCE,@ITEM,@FECREU,@PRES" & vbCrLf
    sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
    sConsulta = sConsulta & "BEGIN" & vbCrLf
    sConsulta = sConsulta & "/* Si no hay ninguna reuni�n posterior fijado para ese item, entonces ponemos este como actual del item */" & vbCrLf
    sConsulta = sConsulta & "IF (SELECT COUNT(*) FROM REU_PROCE WHERE  ANYO=@ANYO AND PROCE=@PROCE AND GMN1=@GMN1 AND FECHA > @FECREU AND RES>0) = 0" & vbCrLf
    sConsulta = sConsulta & "UPDATE  ITEM  SET PREC=@PRES, PRES=(@PRES*CANT)  WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ID=@ITEM" & vbCrLf
    sConsulta = sConsulta & "   " & vbCrLf
    sConsulta = sConsulta & "FETCH NEXT FROM curTG_ITEM_PRES_Ins INTO @ANYO,@GMN1,@PROCE,@ITEM,@FECREU,@PRES" & vbCrLf
    sConsulta = sConsulta & "End" & vbCrLf
    sConsulta = sConsulta & "Close curTG_ITEM_PRES_Ins" & vbCrLf
    sConsulta = sConsulta & "DEALLOCATE curTG_ITEM_PRES_Ins" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    oADOCon.Execute sConsulta
    
oADOCon.Execute "UPDATE VERSION SET NUM ='2.14.00.01'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"

oADOCon.Execute "COMMIT TRANSACTION"
bTransaccionEnCurso = False

oADOCon.Close
Set oADOCon = Nothing


CodigoDeActualizacion2_14_00_00A2_14_00_01 = True
Exit Function
    
error:
    
    MsgBox oADOCon.Errors(0).Description, vbCritical + vbOKOnly
    If bTransaccionEnCurso Then oADOCon.Execute "ROLLBACK TRANSACTION"
    CodigoDeActualizacion2_14_00_00A2_14_00_01 = False

End Function

Public Function CodigoDeActualizacion2_14_00_01A2_14_00_02() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim bBool As Boolean

On Error GoTo error
    

sConsulta = "BEGIN TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = True
ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

frmProgreso.lblDetalle = "Eliminamos trigger"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

    sConsulta = ""
    sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[REU_PROCE_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
    sConsulta = sConsulta & "drop trigger [dbo].[REU_PROCE_TG_INS]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta


frmProgreso.lblDetalle = "Modificamos storeds"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_14_Storeds_2


sConsulta = "UPDATE VERSION SET NUM ='2.14.00.02'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "COMMIT TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = False

CodigoDeActualizacion2_14_00_01A2_14_00_02 = True
Exit Function
    
error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_14_00_01A2_14_00_02 = False

End Function

Public Function CodigoDeActualizacion2_14_00_02A2_14_00_03() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim bBool As Boolean

On Error GoTo error
    

sConsulta = "BEGIN TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = True
ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

frmProgreso.lblDetalle = "Creamos tablas nuevas para integraci�n de GMN"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_14_Tablas_03


sConsulta = "UPDATE VERSION SET NUM ='2.14.00.03'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "COMMIT TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = False

CodigoDeActualizacion2_14_00_02A2_14_00_03 = True
Exit Function
    
error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_14_00_02A2_14_00_03 = False

End Function


Public Function CodigoDeActualizacion2_14_00_03A2_14_00_04() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim bBool As Boolean

On Error GoTo error
    

sConsulta = "BEGIN TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = True
ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

frmProgreso.lblDetalle = "Modificamos campos para pedidos libres"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1



V_2_14_Tablas_04

frmProgreso.lblDetalle = "Actualizamos triggers y storeds para las vistas"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_14_Triggers_04


sConsulta = "UPDATE VERSION SET NUM ='2.14.00.04'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "COMMIT TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = False

CodigoDeActualizacion2_14_00_03A2_14_00_04 = True
Exit Function
    
error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_14_00_03A2_14_00_04 = False

End Function

Public Function CodigoDeActualizacion2_14_00_04A2_14_00_05() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim bBool As Boolean

On Error GoTo error
    

sConsulta = "BEGIN TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = True
ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

frmProgreso.lblDetalle = "Modificamos campos para LOGS"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
'LO QUITO PORQUE M�S ADELANTE LO VUELVE A PONER A 800
'sConsulta = "ALTER TABLE LOG_ADJ ALTER COLUMN ESP VARCHAR(4000)"
'ExecuteSQL gRDOCon, sConsulta

sConsulta = "UPDATE VERSION SET NUM ='2.14.00.05'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "COMMIT TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = False

CodigoDeActualizacion2_14_00_04A2_14_00_05 = True
Exit Function
    
error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_14_00_04A2_14_00_05 = False

End Function


Private Sub V_2_14_Storeds_2()
Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_CAMBIARREUNION]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_CAMBIARREUNION]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE    PROCEDURE SP_CAMBIARREUNION (@FECHAANTERIOR DATETIME,@FECHANUEVA DATETIME,@RES INT OUTPUT, @TRATADA tinyint = 1)  AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE @REF AS VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & " SET @REF = (SELECT REF FROM REU WHERE FECHA=@FECHAANTERIOR)" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & " INSERT INTO REU (FECHA,REF) VALUES (@FECHANUEVA,@REF)" & vbCrLf
sConsulta = sConsulta & "           IF @@ERROR <> 0  GOTO ERR_" & vbCrLf
sConsulta = sConsulta & "  " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE REU_PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "declare @DIF int" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "set @dif = datediff(minute,@fechaanterior,@fechanueva)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE REU_PROCE SET FECHA=@FECHANUEVA," & vbCrLf
sConsulta = sConsulta & "    HORA=dateadd(minute,@dif,Convert(datetime,(Convert(varchar(10),@FECHAANTERIOR,110) + ' ' + Convert(varchar(8),HORA,108)),110))" & vbCrLf
sConsulta = sConsulta & " WHERE FECHA=@FECHAANTERIOR" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @TRATADA = 1" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET FECULTREU=@FECHANUEVA  " & vbCrLf
sConsulta = sConsulta & "  FROM PROCE P INNER JOIN REU_PROCE RP ON P.ANYO = RP.ANYO AND P.GMN1 = RP.GMN1 AND P.COD = RP.PROCE" & vbCrLf
sConsulta = sConsulta & " WHERE RP.FECHA = @FECHANUEVA" & vbCrLf
sConsulta = sConsulta & "   AND P.FECULTREU=@FECHAANTERIOR" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM REU WHERE FECHA=@FECHAANTERIOR" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE REU_PROCE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "  " & vbCrLf
sConsulta = sConsulta & "ERR_: " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & " SET @RES = @@ERROR" & vbCrLf
sConsulta = sConsulta & " RETURN " & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub
Private Sub V_2_14_Tablas_03()
Dim sConsulta As String
Dim lMaxLongGMN As Long

lMaxLongGMN = gLongitudesDeCodigos.giLongCodGMN1

If gLongitudesDeCodigos.giLongCodGMN2 > lMaxLongGMN Then
    lMaxLongGMN = gLongitudesDeCodigos.giLongCodGMN2
End If
If gLongitudesDeCodigos.giLongCodGMN3 > lMaxLongGMN Then
    lMaxLongGMN = gLongitudesDeCodigos.giLongCodGMN3
End If
If gLongitudesDeCodigos.giLongCodGMN4 > lMaxLongGMN Then
    lMaxLongGMN = gLongitudesDeCodigos.giLongCodGMN4
End If

sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE [dbo].[LOG_GMN] (" & vbCrLf
sConsulta = sConsulta & "   [ID] [int] IDENTITY (1, 1) NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [ACCION] [varchar] (1) NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [GMN1] [varchar] (" & gLongitudesDeCodigos.giLongCodGMN1 & ") NULL ," & vbCrLf
sConsulta = sConsulta & "   [GMN2] [varchar] (" & gLongitudesDeCodigos.giLongCodGMN2 & ") NULL ," & vbCrLf
sConsulta = sConsulta & "   [GMN3] [varchar] (" & gLongitudesDeCodigos.giLongCodGMN3 & ") NULL ," & vbCrLf
sConsulta = sConsulta & "   [GMN4] [varchar] (" & gLongitudesDeCodigos.giLongCodGMN4 & ") NULL ," & vbCrLf
sConsulta = sConsulta & "   [DEN] [varchar] (100) NULL ," & vbCrLf
sConsulta = sConsulta & "   [COD_NEW] [varchar] (" & lMaxLongGMN & ") NULL ," & vbCrLf
sConsulta = sConsulta & "   [ORIGEN] [tinyint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [USU] [varchar] (" & gLongitudesDeCodigos.giLongCodUSU & ") NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [FECACT] [datetime] NULL " & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[LOG_GMN] WITH NOCHECK ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [PK_LOG_GMN] PRIMARY KEY  CLUSTERED " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )  ON [PRIMARY] " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & " CREATE  INDEX [IX_LOG_GMN_01] ON [dbo].[LOG_GMN]([GMN1], [GMN2], [GMN3], [GMN4]) WITH  FILLFACTOR = 80 ON [PRIMARY]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER TG_LOG_GMN_INS ON dbo.LOG_GMN" & vbCrLf
sConsulta = sConsulta & "FOR INSERT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "    INSERT INTO LOG_GRAL" & vbCrLf
sConsulta = sConsulta & "       (ID_TABLA,TABLA,ORIGEN,FECACT)" & vbCrLf
sConsulta = sConsulta & "       SELECT L.ID,15, L.ORIGEN,CASE WHEN L.FECACT IS NULL THEN GETDATE() ELSE L.FECACT END" & vbCrLf
sConsulta = sConsulta & "       FROM INSERTED I" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN LOG_GMN L ON L.ID = I.ID" & vbCrLf
sConsulta = sConsulta & "    UPDATE LOG_GMN " & vbCrLf
sConsulta = sConsulta & "        SET FECACT = CASE WHEN L.FECACT IS NULL THEN GETDATE() ELSE L.FECACT END" & vbCrLf
sConsulta = sConsulta & "        FROM INSERTED I" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN LOG_GMN L ON L.ID = I.ID" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[LOG_GRAL_TG_DEL]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[LOG_GRAL_TG_DEL]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER LOG_GRAL_TG_DEL ON [dbo].[LOG_GRAL] " & vbCrLf
sConsulta = sConsulta & "FOR DELETE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_TABLA  INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @TABLA INT" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_LOG_GRAL_Del CURSOR FOR SELECT ID_TABLA,TABLA FROM DELETED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_LOG_GRAL_Del" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_LOG_GRAL_Del INTO @ID_TABLA,@TABLA" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @TABLA = 15 " & vbCrLf
sConsulta = sConsulta & "DELETE FROM LOG_GMN WHERE ID = @ID_TABLA" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @TABLA = 12 OR @TABLA = 14 " & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & " DELETE FROM LOG_LINEAS_RECEP WHERE ID_LOG_PEDIDO_RECEP = @ID_TABLA" & vbCrLf
sConsulta = sConsulta & " DELETE FROM LOG_PEDIDO_RECEP WHERE ID= @ID_TABLA " & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @TABLA = 11 OR @TABLA = 13 " & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & " DELETE FROM LOG_LINEAS_PEDIDO WHERE ID_LOG_ORDEN_ENTREGA = @ID_TABLA" & vbCrLf
sConsulta = sConsulta & " DELETE FROM LOG_ORDEN_ENTREGA WHERE ID= @ID_TABLA " & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @TABLA = 9" & vbCrLf
sConsulta = sConsulta & " DELETE FROM LOG_ADJ WHERE ID = @ID_TABLA" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @TABLA = 8" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DELETE FROM LOG_CON WHERE ID_LOG_PROVE = @ID_TABLA" & vbCrLf
sConsulta = sConsulta & "DELETE FROM LOG_PROVE WHERE ID = @ID_TABLA AND ID_LOG_CON IS NULL" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @TABLA = 7" & vbCrLf
sConsulta = sConsulta & "DELETE FROM LOG_ART4 WHERE ID = @ID_TABLA" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @TABLA = 6" & vbCrLf
sConsulta = sConsulta & "DELETE FROM LOG_UNI WHERE ID = @ID_TABLA" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @TABLA = 5" & vbCrLf
sConsulta = sConsulta & "DELETE FROM LOG_DEST WHERE ID = @ID_TABLA" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @TABLA = 4" & vbCrLf
sConsulta = sConsulta & "DELETE FROM LOG_PAG WHERE ID = @ID_TABLA" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @TABLA = 3" & vbCrLf
sConsulta = sConsulta & "DELETE FROM LOG_PROVI WHERE ID = @ID_TABLA" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @TABLA = 2" & vbCrLf
sConsulta = sConsulta & "DELETE FROM LOG_PAI WHERE ID = @ID_TABLA" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @TABLA = 1" & vbCrLf
sConsulta = sConsulta & "DELETE FROM LOG_MON WHERE ID = @ID_TABLA" & vbCrLf
sConsulta = sConsulta & "IF @TABLA = 101" & vbCrLf
sConsulta = sConsulta & "begin" & vbCrLf
sConsulta = sConsulta & "DELETE FROM LOG_PROVE WHERE ID_LOG_CON = @ID_TABLA" & vbCrLf
sConsulta = sConsulta & "DELETE FROM LOG_CON WHERE ID = @ID_TABLA AND ID_LOG_PROVE IS NULL" & vbCrLf
sConsulta = sConsulta & "end" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_LOG_GRAL_Del INTO @ID_TABLA,@TABLA" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_LOG_GRAL_Del" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_LOG_GRAL_Del" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO TABLAS_INTEGRACION (ID,TABLA,ACTIVA) VALUES (15,15,0)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub


Private Sub V_2_14_Triggers_04()
Dim sConsulta As String

sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PLANTILLA_CONF_VISTAS_PROCE_ATRIB_TG_DEL]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PLANTILLA_CONF_VISTAS_PROCE_ATRIB_TG_DEL]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PLANTILLA_CONF_VISTAS_GRUPO_ATRIB_TG_DEL]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PLANTILLA_CONF_VISTAS_GRUPO_ATRIB_TG_DEL]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PLANTILLA_CONF_VISTAS_ALL_ATRIB_TG_DEL]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PLANTILLA_CONF_VISTAS_ALL_ATRIB_TG_DEL]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CONF_VISTAS_PROCE_ATRIB_TG_DEL]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[CONF_VISTAS_PROCE_ATRIB_TG_DEL]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CONF_VISTAS_GRUPO_ATRIB_TG_DEL]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[CONF_VISTAS_GRUPO_ATRIB_TG_DEL]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CONF_VISTAS_ALL_ATRIB_TG_DEL]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[CONF_VISTAS_ALL_ATRIB_TG_DEL]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ITEM_OFE_TG_UPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[ITEM_OFE_TG_UPD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PROCE_ATRIB_TG_DEL]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PROCE_ATRIB_TG_DEL]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ATR_MODIFICAR]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[ATR_MODIFICAR]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ATR_ELIMINAR]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[ATR_ELIMINAR]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ELIMINAR_GRUPO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_ELIMINAR_GRUPO]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE ATR_MODIFICAR  @TABLA SMALLINT,@ID INTEGER,@DEN VARCHAR(100)=NULL,@TIPO SMALLINT=NULL, @PREFBAJO TINYINT , @INTRO TINYINT, @MINNUM FLOAT=NULL,@MINFEC DATETIME=NULL,@MAXNUM FLOAT=NULL,@MAXFEC DATETIME=NULL,@ATRIB INTEGER=NULL,@INTERNO  TINYINT=NULL,@AMBITO TINYINT=NULL,@OBLIG  TINYINT=NULL,@OP_PREC VARCHAR(2)=NULL,@APLIC_PREC VARCHAR(50)=NULL,@DEF_PREC TINYINT=NULL,@ANYO VARCHAR(50)=NULL,@GMN1 VARCHAR(50)=NULL,@PROCE VARCHAR(50)=NULL,@GRUPO VARCHAR(50)=NULL , @FECACT DATETIME OUTPUT,@ORDEN SMALLINT=NULL OUTPUT,@PLANT INTEGER=NULL AS " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @POND AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @AMBITOOLD AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @COUNT AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @APLI_PREC_V AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @ORDEN_V AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @ATRIB_PREC AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @USAR_PREC AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRIMERO AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @TIPOOLD AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @VISTAS AS TINYINT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @TABLA=0  " & vbCrLf
sConsulta = sConsulta & "     BEGIN --@tabla=0" & vbCrLf
sConsulta = sConsulta & "            SELECT   @POND=POND,@TIPOOLD=TIPO  FROM DEF_ATRIB  WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "            IF @POND = 1 AND @TIPO<> 2 and @TIPO<>3  " & vbCrLf
sConsulta = sConsulta & "                         SET @POND = 0" & vbCrLf
sConsulta = sConsulta & "            ELSE " & vbCrLf
sConsulta = sConsulta & "                          begin" & vbCrLf
sConsulta = sConsulta & "                          IF @POND = 2 AND (@TIPO=4 OR @INTRO=0)" & vbCrLf
sConsulta = sConsulta & "                                       SET @POND= 0" & vbCrLf
sConsulta = sConsulta & "                          ELSE" & vbCrLf
sConsulta = sConsulta & "                                        IF  @POND = 3 AND @TIPO<> 2 " & vbCrLf
sConsulta = sConsulta & "                                                   SET @POND= 0" & vbCrLf
sConsulta = sConsulta & "                                        ELSE" & vbCrLf
sConsulta = sConsulta & "                                                    IF @POND =5 AND @TIPO<>4" & vbCrLf
sConsulta = sConsulta & "                                                            begin" & vbCrLf
sConsulta = sConsulta & "                                                            SET @POND= 0" & vbCrLf
sConsulta = sConsulta & "                                                            end" & vbCrLf
sConsulta = sConsulta & "                           end" & vbCrLf
sConsulta = sConsulta & "            IF @TIPOOLD <> @TIPO " & vbCrLf
sConsulta = sConsulta & "                  begin" & vbCrLf
sConsulta = sConsulta & "                  IF @TIPOOLD=2" & vbCrLf
sConsulta = sConsulta & "                        BEGIN" & vbCrLf
sConsulta = sConsulta & "                        UPDATE PROCE_ATRIB SET OP_PREC=NULL, APLIC_PREC=NULL, DEF_PREC=NULL WHERE ATRIB= @ID" & vbCrLf
sConsulta = sConsulta & "                        UPDATE PLANTILLA_ATRIB SET OP_PREC=NULL, APLIC_PREC=NULL, DEF_PREC=NULL WHERE ATRIB= @ID" & vbCrLf
sConsulta = sConsulta & "                        END " & vbCrLf
sConsulta = sConsulta & "                  end" & vbCrLf
sConsulta = sConsulta & "            UPDATE DEF_ATRIB SET DEN=@DEN,TIPO=@TIPO,PREFBAJO=@PREFBAJO,POND=@POND,INTRO=@INTRO,MAXNUM=CAST(@MAXNUM AS VARCHAR(15)) ,MINNUM=CAST(@MINNUM AS VARCHAR(15)),MAXFEC=CONVERT(VARCHAR(10),@MAXFEC,111),MINFEC=CONVERT(VARCHAR(10),@MINFEC,111) WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "            SELECT @FECACT=FECACT FROM DEF_ATRIB WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "      END --@tabla=0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @TABLA=1" & vbCrLf
sConsulta = sConsulta & "    BEGIN -- @tabla=1" & vbCrLf
sConsulta = sConsulta & "       IF @APLIC_PREC IS not  NULL  AND @ORDEN is  NULL " & vbCrLf
sConsulta = sConsulta & "                 BEGIN --@orden=null" & vbCrLf
sConsulta = sConsulta & "                 SELECT @ATRIB_PREC =ATRIB  FROM PROCE_ATRIB WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "                 SELECT @ORDEN=ORDEN FROM PROCE_ATRIB WHERE  ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND APLIC_PREC = @APLIC_PREC AND" & vbCrLf
sConsulta = sConsulta & "                    OP_PREC=@OP_PREC AND ATRIB =@ATRIB_PREC" & vbCrLf
sConsulta = sConsulta & "                      " & vbCrLf
sConsulta = sConsulta & "                 SET @APLI_PREC_V=@APLIC_PREC" & vbCrLf
sConsulta = sConsulta & "                 SET @ORDEN_V= 0" & vbCrLf
sConsulta = sConsulta & "                 SET @PRIMERO=0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                 WHILE @ORDEN_V=0 and @APLI_PREC_V<=3" & vbCrLf
sConsulta = sConsulta & "                         BEGIN -- while" & vbCrLf
sConsulta = sConsulta & "                             SELECT  @ORDEN_V = MAX(ORDEN) FROM PROCE_ATRIB WHERE  ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND APLIC_PREC = @APLI_PREC_V" & vbCrLf
sConsulta = sConsulta & "                             IF @ORDEN_V IS NULL" & vbCrLf
sConsulta = sConsulta & "                               begin --@ORDEN_V =null" & vbCrLf
sConsulta = sConsulta & "                                 IF @APLIC_PREC=3" & vbCrLf
sConsulta = sConsulta & "                                     begin" & vbCrLf
sConsulta = sConsulta & "                                       SET @ORDEN_V=1 " & vbCrLf
sConsulta = sConsulta & "                                       SET @PRIMERO=1" & vbCrLf
sConsulta = sConsulta & "                                     end" & vbCrLf
sConsulta = sConsulta & "                                 ELSE                   " & vbCrLf
sConsulta = sConsulta & "                                   begin" & vbCrLf
sConsulta = sConsulta & "                                      SET @ORDEN_V= 0" & vbCrLf
sConsulta = sConsulta & "                                      SET @APLI_PREC_V=@APLI_PREC_V + 1" & vbCrLf
sConsulta = sConsulta & "                                   end" & vbCrLf
sConsulta = sConsulta & "                               end  --@ORDEN_V =null" & vbCrLf
sConsulta = sConsulta & "                         END -- while" & vbCrLf
sConsulta = sConsulta & "                if @ORDEN_V IS NULL  SET @ORDEN_V=0" & vbCrLf
sConsulta = sConsulta & "                      begin" & vbCrLf
sConsulta = sConsulta & "                      if @PRIMERO=1" & vbCrLf
sConsulta = sConsulta & "                              UPDATE PROCE_ATRIB SET ORDEN=1 WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "                     else" & vbCrLf
sConsulta = sConsulta & "                              UPDATE PROCE_ATRIB SET ORDEN=@ORDEN_V+1 WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "                     end" & vbCrLf
sConsulta = sConsulta & "                UPDATE PROCE_ATRIB SET ORDEN = ORDEN +1 WHERE  APLIC_PREC < @APLIC_PREC AND ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ID<>@ID" & vbCrLf
sConsulta = sConsulta & "               SET @ORDEN=@ORDEN_V +1" & vbCrLf
sConsulta = sConsulta & "              " & vbCrLf
sConsulta = sConsulta & "             END --@orden=null" & vbCrLf
sConsulta = sConsulta & "       SELECT @POND=POND,@AMBITOOLD=AMBITO,@USAR_PREC=USAR_PREC FROM PROCE_ATRIB WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "       IF @INTRO=0 AND @POND=2 SET @POND=0" & vbCrLf
sConsulta = sConsulta & "       IF @DEF_PREC=1 SET @USAR_PREC=1" & vbCrLf
sConsulta = sConsulta & "       UPDATE PROCE_ATRIB SET PREFBAJO=@PREFBAJO,INTRO=@INTRO,MAXNUM=@MAXNUM  ," & vbCrLf
sConsulta = sConsulta & "                  MINNUM=@MINNUM,MAXFEC=CONVERT(VARCHAR(10),@MAXFEC,111)," & vbCrLf
sConsulta = sConsulta & "                  MINFEC=CONVERT(VARCHAR(10),@MINFEC,111),INTERNO=@INTERNO,AMBITO=@AMBITO,OBLIG=@OBLIG," & vbCrLf
sConsulta = sConsulta & "                  OP_PREC=@OP_PREC,APLIC_PREC=@APLIC_PREC,USAR_PREC=@USAR_PREC," & vbCrLf
sConsulta = sConsulta & "                  DEF_PREC=@DEF_PREC,POND=@POND WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "       SELECT @FECACT=FECACT FROM PROCE_ATRIB WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "       IF   @ATRIB IS NOT  NULL" & vbCrLf
sConsulta = sConsulta & "             BEGIN --@atrib not null" & vbCrLf
sConsulta = sConsulta & "                    SELECT @POND=POND FROM DEF_ATRIB WHERE ID=@ATRIB" & vbCrLf
sConsulta = sConsulta & "                    IF @INTRO=0 AND @POND=2 SET @POND=0" & vbCrLf
sConsulta = sConsulta & "                       UPDATE DEF_ATRIB SET PREFBAJO=@PREFBAJO,INTRO=@INTRO,MAXNUM=@MAXNUM ," & vbCrLf
sConsulta = sConsulta & "                           MINNUM=@MINNUM,MAXFEC=CONVERT(VARCHAR(10),@MAXFEC,111)," & vbCrLf
sConsulta = sConsulta & "                           MINFEC=CONVERT(VARCHAR(10),@MINFEC,111),POND=@POND WHERE ID=@ATRIB" & vbCrLf
sConsulta = sConsulta & "             END--@atrib not null" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "       IF @AMBITO<>@AMBITOOLD" & vbCrLf
sConsulta = sConsulta & "               BEGIN --@ambito<>ambitoold" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                     IF @AMBITOOLD<3 " & vbCrLf
sConsulta = sConsulta & "                          begin" & vbCrLf
sConsulta = sConsulta & "                             IF (SELECT count(*) FROM CONF_VISTAS_PROCE_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE  AND ATRIB =@ID AND POS IS NOT NULL AND POS<>0)>0" & vbCrLf
sConsulta = sConsulta & "                                    SET @VISTAS=1" & vbCrLf
sConsulta = sConsulta & "                          end" & vbCrLf
sConsulta = sConsulta & "                     ELSE" & vbCrLf
sConsulta = sConsulta & "                          begin" & vbCrLf
sConsulta = sConsulta & "                           IF (SELECT count(*) FROM CONF_VISTAS_GRUPO_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE  AND ATRIB =@ID AND POSICION IS NOT NULL AND POSICION<>0)>0 " & vbCrLf
sConsulta = sConsulta & "                                     SET @VISTAS=1" & vbCrLf
sConsulta = sConsulta & "                           IF (SELECT count(*) FROM CONF_VISTAS_ALL_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE  AND ATRIB =@ID AND POSICION IS NOT NULL AND POSICION<>0)>0 " & vbCrLf
sConsulta = sConsulta & "                                     SET @VISTAS=1" & vbCrLf
sConsulta = sConsulta & "                          end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                       IF (SELECT PLANTILLA_VISTAS FROM PROCE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE)=1 AND @VISTAS=1" & vbCrLf
sConsulta = sConsulta & "                           begin" & vbCrLf
sConsulta = sConsulta & "                           UPDATE PROCE SET PLANTILLA_VISTAS=0 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE  " & vbCrLf
sConsulta = sConsulta & "                           end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                     SELECT @COUNT=COUNT(*) FROM PROCE_OFE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                     IF  @AMBITOOLD>@AMBITO" & vbCrLf
sConsulta = sConsulta & "                              IF @AMBITOOLD=3" & vbCrLf
sConsulta = sConsulta & "                                     begin --old=3" & vbCrLf
sConsulta = sConsulta & "                                           IF @COUNT > 0" & vbCrLf
sConsulta = sConsulta & "                                                begin" & vbCrLf
sConsulta = sConsulta & "                                                DELETE OFE_ITEM_ATRIB WHERE  ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ATRIB_ID=@ID" & vbCrLf
sConsulta = sConsulta & "                                                end" & vbCrLf
sConsulta = sConsulta & "                                           DELETE CONF_VISTAS_GRUPO_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ATRIB=@ID" & vbCrLf
sConsulta = sConsulta & "                                           DELETE CONF_VISTAS_ALL_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE  AND ATRIB=@ID" & vbCrLf
sConsulta = sConsulta & "                                           DELETE CONF_VISTAS_ITEM_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE  AND ATRIB=@ID" & vbCrLf
sConsulta = sConsulta & "                                           INSERT INTO CONF_VISTAS_PROCE_ATRIB (ANYO,GMN1,PROCE,USU,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                                    SELECT @ANYO,@GMN1,@PROCE,USU,VISTA,@ID FROM  CONF_VISTAS_PROCE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "                                           IF @VISTAS=1" & vbCrLf
sConsulta = sConsulta & "                                            begin                                                              " & vbCrLf
sConsulta = sConsulta & "                                                  IF @GRUPO IS NULL" & vbCrLf
sConsulta = sConsulta & "                                                    BEGIN" & vbCrLf
sConsulta = sConsulta & "                                                        UPDATE CONF_VISTAS_GRUPO_ATRIB SET POSICION=NULL,VISIBLE=0,FILA=0 WHERE  ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "                                                        UPDATE CONF_VISTAS_GRUPO SET " & vbCrLf
sConsulta = sConsulta & "                                                              PRECIO_PROV_POS=(case when VI.USU is null then 2 else vi.PRECIO_PROV_POS end), PRECIO_PROV_WIDTH=(case when VI.USU is null then 1600 else vi.PRECIO_PROV_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "                                                              ,PRECIO_PROV_VISIBLE=(case when VI.USU is null then 1 else vi.PRECIO_PROV_VISIBLE end), PRECIO_PROV_LEVEL=(case when VI.USU is null then 0 else vi.PRECIO_PROV_LEVEL end)" & vbCrLf
sConsulta = sConsulta & "                                                              ,ADJ_PROV_POS=(case when VI.USU is null then 9 else vi.ADJ_PROV_POS end), ADJ_PROV_WIDTH=(case when VI.USU is null then 700 else vi.ADJ_PROV_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "                                                              ,ADJ_PROV_VISIBLE=(case when VI.USU is null then 1 else vi.ADJ_PROV_VISIBLE end), ADJ_PROV_LEVEL=(case when VI.USU is null then 1 else vi.ADJ_PROV_LEVEL end)" & vbCrLf
sConsulta = sConsulta & "                                                              ,CANT_PROV_POS=(case when VI.USU is null then 8 else vi.CANT_PROV_POS end), CANT_PROV_WIDTH=(case when VI.USU is null then 900 else vi.CANT_PROV_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "                                                              ,CANT_PROV_VISIBLE=(case when VI.USU is null then 1 else vi.CANT_PROV_VISIBLE end), CANT_PROV_LEVEL=(case when VI.USU is null then 1 else vi.CANT_PROV_LEVEL end)" & vbCrLf
sConsulta = sConsulta & "                                                               ,IMP_ADJ_POS=(case when VI.USU is null then 1 else vi.IMP_ADJ_POS end), IMP_ADJ_WIDTH=(case when VI.USU is null then 500 else vi.IMP_ADJ_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "                                                              ,IMP_ADJ_VISIBLE=(case when VI.USU is null then 0 else vi.IMP_ADJ_VISIBLE end), IMP_ADJ_LEVEL=(case when VI.USU is null then 0 else vi.IMP_ADJ_LEVEL end)" & vbCrLf
sConsulta = sConsulta & "                                                              ,GRUPOS_WIDTH=(case when VI.USU is null then 1600 else vi.GRUPOS_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "                                                               FROM CONF_VISTAS_GRUPO V  LEFT JOIN conf_vista_inicial_grupo VI ON VI.ES_GR=1 AND V.USU=VI.USU  WHERE V.ANYO=@ANYO AND V.GMN1=@GMN1 AND V.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "                                                    END" & vbCrLf
sConsulta = sConsulta & "                                                  ELSE" & vbCrLf
sConsulta = sConsulta & "                                                    BEGIN      " & vbCrLf
sConsulta = sConsulta & "                                                          UPDATE CONF_VISTAS_GRUPO_ATRIB SET POSICION=NULL,VISIBLE=0,FILA=0 WHERE  ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "                                                          UPDATE CONF_VISTAS_GRUPO SET " & vbCrLf
sConsulta = sConsulta & "                                                                PRECIO_PROV_POS=(case when VI.USU is null then 2 else vi.PRECIO_PROV_POS end), PRECIO_PROV_WIDTH=(case when VI.USU is null then 1600 else vi.PRECIO_PROV_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "                                                                 ,PRECIO_PROV_VISIBLE=(case when VI.USU is null then 1 else vi.PRECIO_PROV_VISIBLE end), PRECIO_PROV_LEVEL=(case when VI.USU is null then 0 else vi.PRECIO_PROV_LEVEL end)" & vbCrLf
sConsulta = sConsulta & "                                                                 ,ADJ_PROV_POS=(case when VI.USU is null then 9 else vi.ADJ_PROV_POS end), ADJ_PROV_WIDTH=(case when VI.USU is null then 700 else vi.ADJ_PROV_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "                                                                 ,ADJ_PROV_VISIBLE=(case when VI.USU is null then 1 else vi.ADJ_PROV_VISIBLE end), ADJ_PROV_LEVEL=(case when VI.USU is null then 1 else vi.ADJ_PROV_LEVEL end)" & vbCrLf
sConsulta = sConsulta & "                                                                 ,CANT_PROV_POS=(case when VI.USU is null then 8 else vi.CANT_PROV_POS end), CANT_PROV_WIDTH=(case when VI.USU is null then 900 else vi.CANT_PROV_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "                                                                  ,CANT_PROV_VISIBLE=(case when VI.USU is null then 1 else vi.CANT_PROV_VISIBLE end), CANT_PROV_LEVEL=(case when VI.USU is null then 1 else vi.CANT_PROV_LEVEL end)" & vbCrLf
sConsulta = sConsulta & "                                                                 ,IMP_ADJ_POS=(case when VI.USU is null then 1 else vi.IMP_ADJ_POS end), IMP_ADJ_WIDTH=(case when VI.USU is null then 500 else vi.IMP_ADJ_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "                                                                 ,IMP_ADJ_VISIBLE=(case when VI.USU is null then 0 else vi.IMP_ADJ_VISIBLE end), IMP_ADJ_LEVEL=(case when VI.USU is null then 0 else vi.IMP_ADJ_LEVEL end)" & vbCrLf
sConsulta = sConsulta & "                                                                 ,GRUPOS_WIDTH=(case when VI.USU is null then 1600 else vi.GRUPOS_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "                                                                  FROM CONF_VISTAS_GRUPO V  LEFT JOIN conf_vista_inicial_grupo VI ON VI.ES_GR=1 AND V.USU=VI.USU  WHERE V.ANYO=@ANYO AND V.GMN1=@GMN1 AND V.PROCE=@PROCE AND V.GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "                                                     END" & vbCrLf
sConsulta = sConsulta & "                                                   UPDATE CONF_VISTAS_ALL_ATRIB SET POSICION=NULL,VISIBLE=0,FILA=0 WHERE  ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "                                                   UPDATE CONF_VISTAS_ALL SET " & vbCrLf
sConsulta = sConsulta & "                             PRECIO_PROV_POS=(case when VI.USU is null then 2 else vi.PRECIO_PROV_POS end), PRECIO_PROV_WIDTH=(case when VI.USU is null then 1600 else vi.PRECIO_PROV_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "                        ,PRECIO_PROV_VISIBLE=(case when VI.USU is null then 1 else vi.PRECIO_PROV_VISIBLE end), PRECIO_PROV_LEVEL=(case when VI.USU is null then 0 else vi.PRECIO_PROV_LEVEL end)" & vbCrLf
sConsulta = sConsulta & "                       ,ADJ_PROV_POS=(case when VI.USU is null then 9 else vi.ADJ_PROV_POS end), ADJ_PROV_WIDTH=(case when VI.USU is null then 700 else vi.ADJ_PROV_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "                        ,ADJ_PROV_VISIBLE=(case when VI.USU is null then 1 else vi.ADJ_PROV_VISIBLE end), ADJ_PROV_LEVEL=(case when VI.USU is null then 1 else vi.ADJ_PROV_LEVEL end)" & vbCrLf
sConsulta = sConsulta & "                        ,CANT_PROV_POS=(case when VI.USU is null then 8 else vi.CANT_PROV_POS end), CANT_PROV_WIDTH=(case when VI.USU is null then 900 else vi.CANT_PROV_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "                        ,CANT_PROV_VISIBLE=(case when VI.USU is null then 1 else vi.CANT_PROV_VISIBLE end), CANT_PROV_LEVEL=(case when VI.USU is null then 1 else vi.CANT_PROV_LEVEL end)" & vbCrLf
sConsulta = sConsulta & "                        ,IMP_ADJ_POS=(case when VI.USU is null then 1 else vi.IMP_ADJ_POS end), IMP_ADJ_WIDTH=(case when VI.USU is null then 500 else vi.IMP_ADJ_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "                        ,IMP_ADJ_VISIBLE=(case when VI.USU is null then 0 else vi.IMP_ADJ_VISIBLE end), IMP_ADJ_LEVEL=(case when VI.USU is null then 0 else vi.IMP_ADJ_LEVEL end)" & vbCrLf
sConsulta = sConsulta & "                        ,GRUPOS_WIDTH=(case when VI.USU is null then 1600 else vi.GRUPOS_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "                     FROM CONF_VISTAS_ALL V  LEFT JOIN conf_vista_inicial_grupo VI ON VI.ES_GR=0 AND V.USU=VI.USU  WHERE V.ANYO=@ANYO AND V.GMN1=@GMN1 AND V.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "                                            end -- vista" & vbCrLf
sConsulta = sConsulta & "                                     end  --old=3" & vbCrLf
sConsulta = sConsulta & "                              ELSE --ser�a old=2" & vbCrLf
sConsulta = sConsulta & "                                     begin" & vbCrLf
sConsulta = sConsulta & "                                          IF @COUNT>0 " & vbCrLf
sConsulta = sConsulta & "                                              BEGIN" & vbCrLf
sConsulta = sConsulta & "                                              IF @GRUPO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                                                   DELETE OFE_GR_ATRIB WHERE  ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ATRIB_ID=@ID AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "                                              ELSE" & vbCrLf
sConsulta = sConsulta & "                                                   DELETE OFE_GR_ATRIB WHERE  ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ATRIB_ID=@ID" & vbCrLf
sConsulta = sConsulta & "                                              END " & vbCrLf
sConsulta = sConsulta & "                                          IF @VISTAS=1 " & vbCrLf
sConsulta = sConsulta & "                                              BEGIN                  " & vbCrLf
sConsulta = sConsulta & "                                              UPDATE CONF_VISTAS_PROCE_ATRIB SET POS=NULL,VISIBLE=0 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "                                              UPDATE CONF_VISTAS_PROCE SET " & vbCrLf
sConsulta = sConsulta & "               CONSUMIDO_POS=(case when VI.USU is null then 1 else vi.CONSUMIDO_POS end), CONSUMIDO_WIDTH=(case when VI.USU is null then 850 else vi.CONSUMIDO_WIDTH end),CONSUMIDO_VISIBLE=(case when VI.USU is null then 1 else vi.CONSUMIDO_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "               ,IMP_POS=(case when VI.USU is null then 2 else vi.IMP_POS end), IMP_WIDTH=(case when VI.USU is null then 850 else vi.IMP_WIDTH end),IMP_VISIBLE=(case when VI.USU is null then 0 else vi.IMP_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "               ,ADJ_POS=(case when VI.USU is null then 3 else vi.ADJ_POS end), ADJ_WIDTH=(case when VI.USU is null then 850 else vi.ADJ_WIDTH end),ADJ_VISIBLE=(case when VI.USU is null then 1 else vi.ADJ_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "               ,AHORR_ADJ_POS=(case when VI.USU is null then 5 else vi.AHORR_ADJ_POS end), AHORR_ADJ_WIDTH=(case when VI.USU is null then 850 else vi.AHORR_ADJ_WIDTH end),AHORR_ADJ_VISIBLE=(case when VI.USU is null then 1 else vi.AHORR_ADJ_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "               ,AHORR_ADJ_PORCEN_POS=(case when VI.USU is null then 4 else vi.AHORR_ADJ_PORCEN_POS end), AHORR_ADJ_PORCEN_WIDTH=(case when VI.USU is null then 850 else vi.AHORR_ADJ_PORCEN_WIDTH end),AHORR_ADJ_PORCEN_VISIBLE=(case when VI.USU is null then 0 else vi.AHORR_ADJ_PORCEN_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "               ,AHORR_OFE_POS=(case when VI.USU is null then 7 else vi.AHORR_OFE_POS end), AHORR_OFE_WIDTH=(case when VI.USU is null then 850 else vi.AHORR_OFE_WIDTH end),AHORR_OFE_VISIBLE=(case when VI.USU is null then 1 else vi.AHORR_OFE_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "               ,AHORR_OFE_PORCEN_POS=(case when VI.USU is null then 6 else vi.AHORR_OFE_PORCEN_POS end), AHORR_OFE_PORCEN_WIDTH=(case when VI.USU is null then 850 else vi.AHORR_OFE_PORCEN_WIDTH end),AHORR_OFE_PORCEN_VISIBLE=(case when VI.USU is null then 0 else vi.AHORR_OFE_PORCEN_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "               ,GRUPO_WIDTH=(case when VI.USU is null then 2600 else vi.GRUPO_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "               FROM CONF_VISTAS_PROCE V LEFT JOIN CONF_VISTA_INICIAL_PROCE VI ON V.USU=VI.USU WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "                                              END             " & vbCrLf
sConsulta = sConsulta & "                                     end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                     ELSE  --old < ambito" & vbCrLf
sConsulta = sConsulta & "                                IF @AMBITOOLD=1" & vbCrLf
sConsulta = sConsulta & "                                     BEGIN --old=1" & vbCrLf
sConsulta = sConsulta & "                                           IF @COUNT>0" & vbCrLf
sConsulta = sConsulta & "                                                   begin" & vbCrLf
sConsulta = sConsulta & "                                                   DELETE OFE_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ATRIB_ID=@ID            " & vbCrLf
sConsulta = sConsulta & "                                                   end" & vbCrLf
sConsulta = sConsulta & "                                          IF @AMBITO = 3" & vbCrLf
sConsulta = sConsulta & "                                                   begin --ambito=3" & vbCrLf
sConsulta = sConsulta & "                                                          DELETE CONF_VISTAS_PROCE_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ATRIB=@ID" & vbCrLf
sConsulta = sConsulta & "                                                            --pasa de ambito 1 a ambito 3, el @grupo es siempre null, para todos los grupos" & vbCrLf
sConsulta = sConsulta & "                                                            INSERT INTO CONF_VISTAS_GRUPO_ATRIB (ANYO,GMN1,PROCE,GRUPO,USU,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                                                    SELECT @ANYO,@GMN1,@PROCE,GRUPO,USU,VISTA,@ID FROM  CONF_VISTAS_GRUPO WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE         " & vbCrLf
sConsulta = sConsulta & "                                                            INSERT INTO CONF_VISTAS_ITEM_ATRIB (ANYO,GMN1,PROCE,GRUPO,USU,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                                                   SELECT @ANYO,@GMN1,@PROCE,GRUPO,USU,VISTA,@ID FROM  CONF_VISTAS_ITEM WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "                                                            INSERT INTO CONF_VISTAS_ALL_ATRIB (ANYO,GMN1,PROCE,USU,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                                                   SELECT @ANYO,@GMN1,@PROCE,USU,VISTA,@ID FROM  CONF_VISTAS_ALL WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE              " & vbCrLf
sConsulta = sConsulta & "                                                  end--ambito=3" & vbCrLf
sConsulta = sConsulta & "                                           IF @VISTAS=1" & vbCrLf
sConsulta = sConsulta & "                                               begin                               " & vbCrLf
sConsulta = sConsulta & "                                                   UPDATE CONF_VISTAS_PROCE_ATRIB SET POS=NULL,VISIBLE=0 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "                                                   UPDATE CONF_VISTAS_PROCE SET " & vbCrLf
sConsulta = sConsulta & "               CONSUMIDO_POS=(case when VI.USU is null then 1 else vi.CONSUMIDO_POS end), CONSUMIDO_WIDTH=(case when VI.USU is null then 850 else vi.CONSUMIDO_WIDTH end),CONSUMIDO_VISIBLE=(case when VI.USU is null then 1 else vi.CONSUMIDO_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "               ,IMP_POS=(case when VI.USU is null then 2 else vi.IMP_POS end), IMP_WIDTH=(case when VI.USU is null then 850 else vi.IMP_WIDTH end),IMP_VISIBLE=(case when VI.USU is null then 0 else vi.IMP_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "               ,ADJ_POS=(case when VI.USU is null then 3 else vi.ADJ_POS end), ADJ_WIDTH=(case when VI.USU is null then 850 else vi.ADJ_WIDTH end),ADJ_VISIBLE=(case when VI.USU is null then 1 else vi.ADJ_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "               ,AHORR_ADJ_POS=(case when VI.USU is null then 5 else vi.AHORR_ADJ_POS end), AHORR_ADJ_WIDTH=(case when VI.USU is null then 850 else vi.AHORR_ADJ_WIDTH end),AHORR_ADJ_VISIBLE=(case when VI.USU is null then 1 else vi.AHORR_ADJ_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "               ,AHORR_ADJ_PORCEN_POS=(case when VI.USU is null then 4 else vi.AHORR_ADJ_PORCEN_POS end), AHORR_ADJ_PORCEN_WIDTH=(case when VI.USU is null then 850 else vi.AHORR_ADJ_PORCEN_WIDTH end),AHORR_ADJ_PORCEN_VISIBLE=(case when VI.USU is null then 0 else vi.AHORR_ADJ_PORCEN_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "               ,AHORR_OFE_POS=(case when VI.USU is null then 7 else vi.AHORR_OFE_POS end), AHORR_OFE_WIDTH=(case when VI.USU is null then 850 else vi.AHORR_OFE_WIDTH end),AHORR_OFE_VISIBLE=(case when VI.USU is null then 1 else vi.AHORR_OFE_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "               ,AHORR_OFE_PORCEN_POS=(case when VI.USU is null then 6 else vi.AHORR_OFE_PORCEN_POS end), AHORR_OFE_PORCEN_WIDTH=(case when VI.USU is null then 850 else vi.AHORR_OFE_PORCEN_WIDTH end),AHORR_OFE_PORCEN_VISIBLE=(case when VI.USU is null then 0 else vi.AHORR_OFE_PORCEN_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "               ,GRUPO_WIDTH=(case when VI.USU is null then 2600 else vi.GRUPO_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "               FROM CONF_VISTAS_PROCE V LEFT JOIN CONF_VISTA_INICIAL_PROCE VI ON V.USU=VI.USU WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "                                                end --vistas" & vbCrLf
sConsulta = sConsulta & "                                     END --old=1" & vbCrLf
sConsulta = sConsulta & "                                ELSE " & vbCrLf
sConsulta = sConsulta & "                                     BEGIN  --old es 2, nuevo es 3" & vbCrLf
sConsulta = sConsulta & "                                           IF @COUNT>0" & vbCrLf
sConsulta = sConsulta & "                                               begin" & vbCrLf
sConsulta = sConsulta & "                                               IF @GRUPO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                                                   begin" & vbCrLf
sConsulta = sConsulta & "                                                    DELETE OFE_GR_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ATRIB_ID=@ID AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "                                                    end" & vbCrLf
sConsulta = sConsulta & "                                                ELSE" & vbCrLf
sConsulta = sConsulta & "                                                     begin" & vbCrLf
sConsulta = sConsulta & "                                                     DELETE OFE_GR_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ATRIB_ID=@ID " & vbCrLf
sConsulta = sConsulta & "                                                     end" & vbCrLf
sConsulta = sConsulta & "                                               end" & vbCrLf
sConsulta = sConsulta & "                                           DELETE CONF_VISTAS_PROCE_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ATRIB=@ID" & vbCrLf
sConsulta = sConsulta & "                                           IF @GRUPO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                                                   begin" & vbCrLf
sConsulta = sConsulta & "                                                    INSERT INTO CONF_VISTAS_GRUPO_ATRIB (ANYO,GMN1,PROCE,GRUPO,USU,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                                         SELECT @ANYO,@GMN1,@PROCE,@GRUPO,USU,VISTA,@ID FROM  CONF_VISTAS_GRUPO WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO " & vbCrLf
sConsulta = sConsulta & "                                                    INSERT INTO CONF_VISTAS_ITEM_ATRIB (ANYO,GMN1,PROCE,GRUPO,USU,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                                        SELECT @ANYO,@GMN1,@PROCE,@GRUPO,USU,VISTA,@ID FROM  CONF_VISTAS_ITEM WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO " & vbCrLf
sConsulta = sConsulta & "                                                    end" & vbCrLf
sConsulta = sConsulta & "                                           ELSE" & vbCrLf
sConsulta = sConsulta & "                                                     begin" & vbCrLf
sConsulta = sConsulta & "                                                     INSERT INTO CONF_VISTAS_GRUPO_ATRIB (ANYO,GMN1,PROCE,GRUPO,USU,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                                          SELECT @ANYO,@GMN1,@PROCE,GRUPO,USU,VISTA,@ID FROM  CONF_VISTAS_GRUPO WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "                                                     INSERT INTO CONF_VISTAS_ITEM_ATRIB (ANYO,GMN1,PROCE,GRUPO,USU,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                                          SELECT @ANYO,@GMN1,@PROCE,GRUPO,USU,VISTA,@ID FROM  CONF_VISTAS_ITEM WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "                                                     end" & vbCrLf
sConsulta = sConsulta & "                                           INSERT INTO CONF_VISTAS_ALL_ATRIB (ANYO,GMN1,PROCE,USU,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                                  SELECT @ANYO,@GMN1,@PROCE,USU,VISTA,@ID FROM  CONF_VISTAS_ALL WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "                                           IF @VISTAS=1" & vbCrLf
sConsulta = sConsulta & "                                               begin" & vbCrLf
sConsulta = sConsulta & "                                                   UPDATE CONF_VISTAS_PROCE_ATRIB SET POS=NULL,VISIBLE=0 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "                          UPDATE CONF_VISTAS_PROCE SET " & vbCrLf
sConsulta = sConsulta & "                              CONSUMIDO_POS=(case when VI.USU is null then 1 else vi.CONSUMIDO_POS end), CONSUMIDO_WIDTH=(case when VI.USU is null then 850 else vi.CONSUMIDO_WIDTH end),CONSUMIDO_VISIBLE=(case when VI.USU is null then 1 else vi.CONSUMIDO_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "                                                           ,IMP_POS=(case when VI.USU is null then 2 else vi.IMP_POS end), IMP_WIDTH=(case when VI.USU is null then 850 else vi.IMP_WIDTH end),IMP_VISIBLE=(case when VI.USU is null then 0 else vi.IMP_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "                                                           ,ADJ_POS=(case when VI.USU is null then 3 else vi.ADJ_POS end), ADJ_WIDTH=(case when VI.USU is null then 850 else vi.ADJ_WIDTH end),ADJ_VISIBLE=(case when VI.USU is null then 1 else vi.ADJ_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "                                                           ,AHORR_ADJ_POS=(case when VI.USU is null then 5 else vi.AHORR_ADJ_POS end), AHORR_ADJ_WIDTH=(case when VI.USU is null then 850 else vi.AHORR_ADJ_WIDTH end),AHORR_ADJ_VISIBLE=(case when VI.USU is null then 1 else vi.AHORR_ADJ_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "                                                           ,AHORR_ADJ_PORCEN_POS=(case when VI.USU is null then 4 else vi.AHORR_ADJ_PORCEN_POS end), AHORR_ADJ_PORCEN_WIDTH=(case when VI.USU is null then 850 else vi.AHORR_ADJ_PORCEN_WIDTH end),AHORR_ADJ_PORCEN_VISIBLE=(case when VI.USU is null then 0 else vi.AHORR_ADJ_PORCEN_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "                                                           ,AHORR_OFE_POS=(case when VI.USU is null then 7 else vi.AHORR_OFE_POS end), AHORR_OFE_WIDTH=(case when VI.USU is null then 850 else vi.AHORR_OFE_WIDTH end),AHORR_OFE_VISIBLE=(case when VI.USU is null then 1 else vi.AHORR_OFE_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "                                                          ,AHORR_OFE_PORCEN_POS=(case when VI.USU is null then 6 else vi.AHORR_OFE_PORCEN_POS end), AHORR_OFE_PORCEN_WIDTH=(case when VI.USU is null then 850 else vi.AHORR_OFE_PORCEN_WIDTH end),AHORR_OFE_PORCEN_VISIBLE=(case when VI.USU is null then 0 else vi.AHORR_OFE_PORCEN_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "                                                           ,GRUPO_WIDTH=(case when VI.USU is null then 2600 else vi.GRUPO_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "                                                           FROM CONF_VISTAS_PROCE V LEFT JOIN CONF_VISTA_INICIAL_PROCE VI ON V.USU=VI.USU WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "                                              end --vistas" & vbCrLf
sConsulta = sConsulta & "                                     END--old es 2" & vbCrLf
sConsulta = sConsulta & "               END --@ambito<>ambitoold" & vbCrLf
sConsulta = sConsulta & "         END -- @tabla=1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @TABLA=2" & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "          IF @APLIC_PREC IS not  NULL  AND @ORDEN is  NULL " & vbCrLf
sConsulta = sConsulta & "                 BEGIN" & vbCrLf
sConsulta = sConsulta & "                     SELECT @ATRIB_PREC =ATRIB  FROM PLANTILLA_ATRIB WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "                     SELECT @ORDEN=ORDEN FROM PLANTILLA_ATRIB WHERE PLANT=@PLANT AND APLIC_PREC = @APLIC_PREC AND OP_PREC=@OP_PREC AND ATRIB =@ATRIB_PREC" & vbCrLf
sConsulta = sConsulta & "                     IF @ORDEN IS NULL" & vbCrLf
sConsulta = sConsulta & "                         BEGIN" & vbCrLf
sConsulta = sConsulta & "                           SET @APLI_PREC_V=@APLIC_PREC" & vbCrLf
sConsulta = sConsulta & "                           SET @ORDEN_V= 0" & vbCrLf
sConsulta = sConsulta & "                           WHILE @APLI_PREC_V < 3  AND @ORDEN_V= 0" & vbCrLf
sConsulta = sConsulta & "                                 BEGIN" & vbCrLf
sConsulta = sConsulta & "                                     SELECT  @ORDEN_V = MAX(ORDEN) FROM PLANTILLA_ATRIB WHERE  PLANT=@PLANT AND APLIC_PREC = @APLI_PREC_V" & vbCrLf
sConsulta = sConsulta & "                                      IF @ORDEN_V IS NULL" & vbCrLf
sConsulta = sConsulta & "                                         begin" & vbCrLf
sConsulta = sConsulta & "                                              SET @ORDEN_V= 0" & vbCrLf
sConsulta = sConsulta & "                                              SET @APLI_PREC_V=@APLI_PREC_V + 1" & vbCrLf
sConsulta = sConsulta & "                                         end" & vbCrLf
sConsulta = sConsulta & "                                END" & vbCrLf
sConsulta = sConsulta & "                          UPDATE PLANTILLA_ATRIB SET ORDEN = ORDEN +1 WHERE  APLIC_PREC < @APLIC_PREC AND PLANT=@PLANT" & vbCrLf
sConsulta = sConsulta & "                          SET @ORDEN=@ORDEN_V +1" & vbCrLf
sConsulta = sConsulta & "                        END" & vbCrLf
sConsulta = sConsulta & "                   ELSE" & vbCrLf
sConsulta = sConsulta & "            begin" & vbCrLf
sConsulta = sConsulta & "                        IF @APLIC_PREC IS not  NULL   " & vbCrLf
sConsulta = sConsulta & "       begin" & vbCrLf
sConsulta = sConsulta & "                            UPDATE PLANTILLA_ATRIB SET ORDEN = ORDEN +1 WHERE  APLIC_PREC < @APLIC_PREC AND PLANT=@PLANT" & vbCrLf
sConsulta = sConsulta & "       end" & vbCrLf
sConsulta = sConsulta & "           end" & vbCrLf
sConsulta = sConsulta & "                 END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "           SELECT @POND=POND, @AMBITOOLD=AMBITO FROM PLANTILLA_ATRIB WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "           IF @INTRO=0 AND @POND=2 SET @POND=0" & vbCrLf
sConsulta = sConsulta & "               UPDATE PLANTILLA_ATRIB SET PREFBAJO=@PREFBAJO,INTRO=@INTRO,MAXNUM=@MAXNUM  ," & vbCrLf
sConsulta = sConsulta & "                    MINNUM=@MINNUM,MAXFEC=CONVERT(VARCHAR(10),@MAXFEC,111)," & vbCrLf
sConsulta = sConsulta & "                    MINFEC=CONVERT(VARCHAR(10),@MINFEC,111),INTERNO=@INTERNO,AMBITO=@AMBITO,OBLIG=@OBLIG," & vbCrLf
sConsulta = sConsulta & "                    OP_PREC=@OP_PREC,APLIC_PREC=@APLIC_PREC," & vbCrLf
sConsulta = sConsulta & "                    DEF_PREC=@DEF_PREC,POND=@POND,ORDEN=@ORDEN WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "          SELECT @FECACT=FECACT FROM PLANTILLA_ATRIB WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "          IF   @ATRIB IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                BEGIN" & vbCrLf
sConsulta = sConsulta & "                    SELECT @POND=POND FROM DEF_ATRIB WHERE ID=@ATRIB" & vbCrLf
sConsulta = sConsulta & "                    IF @INTRO=0 AND @POND=2 SET @POND=0" & vbCrLf
sConsulta = sConsulta & "                        UPDATE DEF_ATRIB SET PREFBAJO=@PREFBAJO,INTRO=@INTRO,MAXNUM=CAST(@MAXNUM AS VARCHAR(15)) ," & vbCrLf
sConsulta = sConsulta & "                            MINNUM=CAST(@MINNUM AS VARCHAR(15)),MAXFEC=CONVERT(VARCHAR(10),@MAXFEC,111)," & vbCrLf
sConsulta = sConsulta & "                            MINFEC=CONVERT(VARCHAR(10),@MINFEC,111),POND=@POND WHERE ID=@ATRIB" & vbCrLf
sConsulta = sConsulta & "                END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "     IF @AMBITO<>@AMBITOOLD" & vbCrLf
sConsulta = sConsulta & "       BEGIN --@ambito<>ambitoold           " & vbCrLf
sConsulta = sConsulta & "           IF @AMBITOOLD<3 " & vbCrLf
sConsulta = sConsulta & "               begin" & vbCrLf
sConsulta = sConsulta & "                IF (SELECT count(*) FROM PLANTILLA_CONF_VISTAS_PROCE_ATRIB WHERE PLANT=@PLANT AND ATRIB =@ID AND POS IS NOT NULL AND POS<>0)>0 SET @VISTAS=1" & vbCrLf
sConsulta = sConsulta & "              end" & vbCrLf
sConsulta = sConsulta & "           ELSE" & vbCrLf
sConsulta = sConsulta & "              begin" & vbCrLf
sConsulta = sConsulta & "                IF (SELECT count(*) FROM PLANTILLA_CONF_VISTAS_GRUPO_ATRIB WHERE PLANT=@PLANT AND ATRIB =@ID AND POSICION IS NOT NULL AND POSICION<>0)>0 SET @VISTAS=1" & vbCrLf
sConsulta = sConsulta & "                IF (SELECT count(*) FROM PLANTILLA_CONF_VISTAS_ALL_ATRIB WHERE PLANT=@PLANT AND ATRIB =@ID AND POSICION IS NOT NULL AND POSICION<>0)>0 SET @VISTAS=1" & vbCrLf
sConsulta = sConsulta & "              end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "            IF  @AMBITOOLD>@AMBITO" & vbCrLf
sConsulta = sConsulta & "             begin" & vbCrLf
sConsulta = sConsulta & "                IF @AMBITOOLD=3" & vbCrLf
sConsulta = sConsulta & "                   begin --old=3" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                         DELETE PLANTILLA_CONF_VISTAS_GRUPO_ATRIB WHERE PLANT=@PLANT AND ATRIB=@ID" & vbCrLf
sConsulta = sConsulta & "                         DELETE PLANTILLA_CONF_VISTAS_ALL_ATRIB WHERE PLANT=@PLANT AND ATRIB=@ID" & vbCrLf
sConsulta = sConsulta & "                                              " & vbCrLf
sConsulta = sConsulta & "                         INSERT INTO PLANTILLA_CONF_VISTAS_PROCE_ATRIB (PLANT,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                SELECT @PLANT,VISTA,@ID FROM  PLANTILLA_CONF_VISTAS_PROCE WHERE PLANT=@PLANT " & vbCrLf
sConsulta = sConsulta & "                         IF @VISTAS=1" & vbCrLf
sConsulta = sConsulta & "                         begin" & vbCrLf
sConsulta = sConsulta & "               UPDATE PLANTILLA_CONF_VISTAS_ALL_ATRIB SET POSICION=NULL,VISIBLE=0,FILA=0 WHERE PLANT=@PLANT" & vbCrLf
sConsulta = sConsulta & "               UPDATE PLANTILLA_CONF_VISTAS_ALL SET PRECIO_PROV_POS=2 , PRECIO_PROV_WIDTH=1600 ,PRECIO_PROV_VISIBLE=1, PRECIO_PROV_LEVEL=0," & vbCrLf
sConsulta = sConsulta & "                    ADJ_PROV_POS=9, ADJ_PROV_WIDTH=700, ADJ_PROV_VISIBLE=1, ADJ_PROV_LEVEL=1,CANT_PROV_POS=8 , CANT_PROV_WIDTH=900 ,CANT_PROV_VISIBLE=1, CANT_PROV_LEVEL=1 ," & vbCrLf
sConsulta = sConsulta & "                    IMP_ADJ_POS=1, IMP_ADJ_WIDTH=500 ,IMP_ADJ_VISIBLE=0, IMP_ADJ_LEVEL=0 , GRUPOS_WIDTH=1600  WHERE PLANT=@PLANT" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "              IF @GRUPO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "       begin" & vbCrLf
sConsulta = sConsulta & "                  UPDATE PLANTILLA_CONF_VISTAS_GRUPO_ATRIB SET POSICION=NULL,VISIBLE=0,FILA=0 WHERE PLANT=@PLANT AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "                  UPDATE PLANTILLA_CONF_VISTAS_GRUPO SET PRECIO_PROV_POS=2 , PRECIO_PROV_WIDTH=1600 ,PRECIO_PROV_VISIBLE=1, PRECIO_PROV_LEVEL=0," & vbCrLf
sConsulta = sConsulta & "                    ADJ_PROV_POS=9, ADJ_PROV_WIDTH=700, ADJ_PROV_VISIBLE=1, ADJ_PROV_LEVEL=1,  CANT_PROV_POS=8 , CANT_PROV_WIDTH=900 ,CANT_PROV_VISIBLE=1, CANT_PROV_LEVEL=1 ," & vbCrLf
sConsulta = sConsulta & "                    IMP_ADJ_POS=1, IMP_ADJ_WIDTH=500 ,IMP_ADJ_VISIBLE=0, IMP_ADJ_LEVEL=0 , GRUPOS_WIDTH=1600 WHERE PLANT=@PLANT  AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "       end" & vbCrLf
sConsulta = sConsulta & "              ELSE" & vbCrLf
sConsulta = sConsulta & "       begin" & vbCrLf
sConsulta = sConsulta & "                  UPDATE PLANTILLA_CONF_VISTAS_GRUPO_ATRIB SET POSICION=NULL,VISIBLE=0,FILA=0 WHERE PLANT=@PLANT " & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "                  UPDATE PLANTILLA_CONF_VISTAS_GRUPO SET PRECIO_PROV_POS=2 , PRECIO_PROV_WIDTH=1600 ,PRECIO_PROV_VISIBLE=1, PRECIO_PROV_LEVEL=0," & vbCrLf
sConsulta = sConsulta & "                    ADJ_PROV_POS=9, ADJ_PROV_WIDTH=700, ADJ_PROV_VISIBLE=1, ADJ_PROV_LEVEL=1,  CANT_PROV_POS=8 , CANT_PROV_WIDTH=900 ,CANT_PROV_VISIBLE=1, CANT_PROV_LEVEL=1 ," & vbCrLf
sConsulta = sConsulta & "                    IMP_ADJ_POS=1, IMP_ADJ_WIDTH=500 ,IMP_ADJ_VISIBLE=0, IMP_ADJ_LEVEL=0 , GRUPOS_WIDTH=1600 WHERE PLANT=@PLANT" & vbCrLf
sConsulta = sConsulta & "       end" & vbCrLf
sConsulta = sConsulta & "                          end" & vbCrLf
sConsulta = sConsulta & "                   end  --old=3" & vbCrLf
sConsulta = sConsulta & "   ELSE --Ser�a ambitoold=2" & vbCrLf
sConsulta = sConsulta & "        begin" & vbCrLf
sConsulta = sConsulta & "                          if @VISTAS=1 " & vbCrLf
sConsulta = sConsulta & "                          begin" & vbCrLf
sConsulta = sConsulta & "       UPDATE PLANTILLA_CONF_VISTAS_PROCE_ATRIB SET POS=NULL,VISIBLE=0 WHERE PLANT=@PLANT" & vbCrLf
sConsulta = sConsulta & "       UPDATE PLANTILLA_CONF_VISTAS_PROCE SET CONSUMIDO_POS=1, CONSUMIDO_WIDTH=850,CONSUMIDO_VISIBLE=1,IMP_POS=2, IMP_WIDTH=850, IMP_VISIBLE=0, ADJ_POS=3, ADJ_WIDTH=850," & vbCrLf
sConsulta = sConsulta & "               ADJ_VISIBLE=1, AHORR_ADJ_POS=5, AHORR_ADJ_WIDTH=850, AHORR_ADJ_VISIBLE=1, AHORR_ADJ_PORCEN_POS=4, AHORR_ADJ_PORCEN_WIDTH=850 ," & vbCrLf
sConsulta = sConsulta & "               AHORR_ADJ_PORCEN_VISIBLE=0, AHORR_OFE_POS=7, AHORR_OFE_WIDTH=850,AHORR_OFE_VISIBLE=1," & vbCrLf
sConsulta = sConsulta & "               AHORR_OFE_PORCEN_POS= 6, AHORR_OFE_PORCEN_WIDTH=850 ,AHORR_OFE_PORCEN_VISIBLE=0,GRUPO_WIDTH=2600 WHERE PLANT=@PLANT" & vbCrLf
sConsulta = sConsulta & "                         end" & vbCrLf
sConsulta = sConsulta & "        end" & vbCrLf
sConsulta = sConsulta & "            end --@AMBITOOLD>@AMBITO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "           ELSE  --old < ambito" & vbCrLf
sConsulta = sConsulta & "             IF @AMBITOOLD=1" & vbCrLf
sConsulta = sConsulta & "                  begin --old=1" & vbCrLf
sConsulta = sConsulta & "       IF @AMBITO = 3" & vbCrLf
sConsulta = sConsulta & "               begin --ambito=3" & vbCrLf
sConsulta = sConsulta & "                               DELETE PLANTILLA_CONF_VISTAS_PROCE_ATRIB WHERE PLANT=@PLANT AND ATRIB=@ID" & vbCrLf
sConsulta = sConsulta & "                               IF @GRUPO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                                      INSERT INTO PLANTILLA_CONF_VISTAS_GRUPO_ATRIB (PLANT,GRUPO,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                             SELECT @PLANT,@GRUPO,VISTA,@ID FROM  PLANTILLA_CONF_VISTAS_GRUPO WHERE PLANT=@PLANT AND GRUPO=@GRUPO " & vbCrLf
sConsulta = sConsulta & "                               ELSE" & vbCrLf
sConsulta = sConsulta & "                                      INSERT INTO PLANTILLA_CONF_VISTAS_GRUPO_ATRIB (PLANT,GRUPO,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                             SELECT @PLANT,GRUPO,VISTA,@ID FROM  PLANTILLA_CONF_VISTAS_GRUPO WHERE PLANT=@PLANT         " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                               INSERT INTO PLANTILLA_CONF_VISTAS_ALL_ATRIB (PLANT,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                      SELECT @PLANT,VISTA,@ID FROM PLANTILLA_CONF_VISTAS_ALL WHERE PLANT=@PLANT " & vbCrLf
sConsulta = sConsulta & "           end" & vbCrLf
sConsulta = sConsulta & "                            IF @VISTAS=1" & vbCrLf
sConsulta = sConsulta & "                            begin" & vbCrLf
sConsulta = sConsulta & "                UPDATE PLANTILLA_CONF_VISTAS_PROCE_ATRIB SET POS=NULL,VISIBLE=0 WHERE PLANT=@PLANT" & vbCrLf
sConsulta = sConsulta & "            UPDATE PLANTILLA_CONF_VISTAS_PROCE SET CONSUMIDO_POS=1, CONSUMIDO_WIDTH=850,CONSUMIDO_VISIBLE=1,IMP_POS=2, IMP_WIDTH=850, IMP_VISIBLE=0, ADJ_POS=3, ADJ_WIDTH=850," & vbCrLf
sConsulta = sConsulta & "               ADJ_VISIBLE=1, AHORR_ADJ_POS=5, AHORR_ADJ_WIDTH=850, AHORR_ADJ_VISIBLE=1, AHORR_ADJ_PORCEN_POS=4, AHORR_ADJ_PORCEN_WIDTH=850 ," & vbCrLf
sConsulta = sConsulta & "               AHORR_ADJ_PORCEN_VISIBLE=0, AHORR_OFE_POS=7, AHORR_OFE_WIDTH=850,AHORR_OFE_VISIBLE=1," & vbCrLf
sConsulta = sConsulta & "               AHORR_OFE_PORCEN_POS= 6, AHORR_OFE_PORCEN_WIDTH=850 ,AHORR_OFE_PORCEN_VISIBLE=0,GRUPO_WIDTH=2600 WHERE PLANT=@PLANT" & vbCrLf
sConsulta = sConsulta & "                            end" & vbCrLf
sConsulta = sConsulta & "                 end --old=1" & vbCrLf
sConsulta = sConsulta & "             ELSE " & vbCrLf
sConsulta = sConsulta & "                 BEGIN  --old es 2, nuevo es 3" & vbCrLf
sConsulta = sConsulta & "                      DELETE PLANTILLA_CONF_VISTAS_PROCE_ATRIB WHERE PLANT=@PLANT AND ATRIB=@ID" & vbCrLf
sConsulta = sConsulta & "                      IF @GRUPO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "       begin" & vbCrLf
sConsulta = sConsulta & "                             INSERT INTO PLANTILLA_CONF_VISTAS_GRUPO_ATRIB (PLANT,GRUPO,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                    SELECT @PLANT,@GRUPO,VISTA,@ID FROM  PLANTILLA_CONF_VISTAS_GRUPO WHERE PLANT=@PLANT AND GRUPO=@GRUPO " & vbCrLf
sConsulta = sConsulta & "                          end" & vbCrLf
sConsulta = sConsulta & "                      ELSE" & vbCrLf
sConsulta = sConsulta & "                begin" & vbCrLf
sConsulta = sConsulta & "                             INSERT INTO PLANTILLA_CONF_VISTAS_GRUPO_ATRIB (PLANT,GRUPO,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                    SELECT @PLANT,GRUPO,VISTA,@ID FROM  PLANTILLA_CONF_VISTAS_GRUPO WHERE PLANT=@PLANT " & vbCrLf
sConsulta = sConsulta & "                          end" & vbCrLf
sConsulta = sConsulta & "                      INSERT INTO PLANTILLA_CONF_VISTAS_ALL_ATRIB (PLANT,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                             SELECT @PLANT,VISTA,@ID FROM PLANTILLA_CONF_VISTAS_ALL WHERE PLANT=@PLANT " & vbCrLf
sConsulta = sConsulta & "                      IF @VISTAS=1" & vbCrLf
sConsulta = sConsulta & "                       begin                       " & vbCrLf
sConsulta = sConsulta & "                         UPDATE PLANTILLA_CONF_VISTAS_PROCE_ATRIB SET POS=NULL,VISIBLE=0 WHERE PLANT=@PLANT" & vbCrLf
sConsulta = sConsulta & "                 UPDATE PLANTILLA_CONF_VISTAS_PROCE SET CONSUMIDO_POS=1, CONSUMIDO_WIDTH=850,CONSUMIDO_VISIBLE=1,IMP_POS=2, IMP_WIDTH=850, IMP_VISIBLE=0, ADJ_POS=3, ADJ_WIDTH=850," & vbCrLf
sConsulta = sConsulta & "               ADJ_VISIBLE=1, AHORR_ADJ_POS=5, AHORR_ADJ_WIDTH=850, AHORR_ADJ_VISIBLE=1, AHORR_ADJ_PORCEN_POS=4, AHORR_ADJ_PORCEN_WIDTH=850 ," & vbCrLf
sConsulta = sConsulta & "               AHORR_ADJ_PORCEN_VISIBLE=0, AHORR_OFE_POS=7, AHORR_OFE_WIDTH=850,AHORR_OFE_VISIBLE=1," & vbCrLf
sConsulta = sConsulta & "               AHORR_OFE_PORCEN_POS= 6, AHORR_OFE_PORCEN_WIDTH=850 ,AHORR_OFE_PORCEN_VISIBLE=0,GRUPO_WIDTH=2600 WHERE PLANT=@PLANT" & vbCrLf
sConsulta = sConsulta & "                       end" & vbCrLf
sConsulta = sConsulta & "                END--old es 2" & vbCrLf
sConsulta = sConsulta & "       END --@ambito<>ambitoold" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE ATR_ELIMINAR  @TABLA SMALLINT,@ATRIB INTEGER  AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO SMALLINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN1 VARCHAR(10)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCE SMALLINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @GRUPO VARCHAR(10)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PLANT SMALLINT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @TABLA = 0" & vbCrLf
sConsulta = sConsulta & "     BEGIN" & vbCrLf
sConsulta = sConsulta & "      DELETE FROM PROVE_ART4_ATRIB  WHERE ATRIB=@ATRIB" & vbCrLf
sConsulta = sConsulta & "      " & vbCrLf
sConsulta = sConsulta & "      DELETE  FROM ART4_ATRIB  WHERE ATRIB=@ATRIB" & vbCrLf
sConsulta = sConsulta & "      " & vbCrLf
sConsulta = sConsulta & "      DELETE FROM GMN4_ATRIB  WHERE ATRIB=@ATRIB" & vbCrLf
sConsulta = sConsulta & "      " & vbCrLf
sConsulta = sConsulta & "      DELETE FROM PLANTILLA_ATRIB_LISTA  WHERE ATRIB_ID=@ATRIB" & vbCrLf
sConsulta = sConsulta & "      " & vbCrLf
sConsulta = sConsulta & "      DELETE  FROM PLANTILLA_ATRIB_POND  WHERE ATRIB_ID=@ATRIB" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "      DELETE FROM PLANTILLA_CONF_VISTAS_PROCE_ATRIB  FROM PLANTILLA_CONF_VISTAS_PROCE_ATRIB INNER JOIN PLANTILLA_ATRIB" & vbCrLf
sConsulta = sConsulta & "             ON PLANTILLA_CONF_VISTAS_PROCE_ATRIB.ATRIB=PLANTILLA_ATRIB.ID AND PLANTILLA_ATRIB. ATRIB=@ATRIB" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "      DELETE FROM PLANTILLA_CONF_VISTAS_GRUPO_ATRIB  FROM PLANTILLA_CONF_VISTAS_GRUPO_ATRIB INNER JOIN PLANTILLA_ATRIB" & vbCrLf
sConsulta = sConsulta & "             ON PLANTILLA_CONF_VISTAS_GRUPO_ATRIB.ATRIB=PLANTILLA_ATRIB.ID AND PLANTILLA_ATRIB. ATRIB=@ATRIB" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "      DELETE FROM PLANTILLA_CONF_VISTAS_ALL_ATRIB  FROM PLANTILLA_CONF_VISTAS_ALL_ATRIB INNER JOIN PLANTILLA_ATRIB" & vbCrLf
sConsulta = sConsulta & "             ON PLANTILLA_CONF_VISTAS_ALL_ATRIB.ATRIB=PLANTILLA_ATRIB.ID AND PLANTILLA_ATRIB. ATRIB=@ATRIB" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "      DELETE FROM PLANTILLA_ATRIB  WHERE ATRIB=@ATRIB" & vbCrLf
sConsulta = sConsulta & "          " & vbCrLf
sConsulta = sConsulta & "      DELETE FROM DEF_ATRIB_LISTA WHERE ATRIB=@ATRIB" & vbCrLf
sConsulta = sConsulta & "         " & vbCrLf
sConsulta = sConsulta & "      DELETE FROM DEF_ATRIB_POND WHERE ATRIB=@ATRIB" & vbCrLf
sConsulta = sConsulta & "      " & vbCrLf
sConsulta = sConsulta & "       DELETE   FROM DEF_ATRIB  WHERE ID=@ATRIB" & vbCrLf
sConsulta = sConsulta & "    END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "         IF @TABLA=1" & vbCrLf
sConsulta = sConsulta & "              BEGIN" & vbCrLf
sConsulta = sConsulta & "           ---Si existe el atributo en la pesta�a de ALL" & vbCrLf
sConsulta = sConsulta & "           DECLARE C_Vistas CURSOR LOCAL FOR SELECT DISTINCT ANYO,GMN1,PROCE FROM CONF_VISTAS_ALL_ATRIB WHERE  ATRIB=@ATRIB AND POSICION IS NOT NULL AND POSICION<>0" & vbCrLf
sConsulta = sConsulta & "           OPEN C_Vistas" & vbCrLf
sConsulta = sConsulta & "           FETCH NEXT FROM C_Vistas INTO @ANYO,@GMN1,@PROCE" & vbCrLf
sConsulta = sConsulta & "           WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "           BEGIN" & vbCrLf
sConsulta = sConsulta & "           UPDATE CONF_VISTAS_ALL_ATRIB SET POSICION=NULL,VISIBLE=0,FILA=0 WHERE  ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "           UPDATE CONF_VISTAS_ALL SET " & vbCrLf
sConsulta = sConsulta & "                PRECIO_PROV_POS=(case when VI.USU is null then 2 else vi.PRECIO_PROV_POS end), PRECIO_PROV_WIDTH=(case when VI.USU is null then 1600 else vi.PRECIO_PROV_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "                ,PRECIO_PROV_VISIBLE=(case when VI.USU is null then 1 else vi.PRECIO_PROV_VISIBLE end), PRECIO_PROV_LEVEL=(case when VI.USU is null then 0 else vi.PRECIO_PROV_LEVEL end)" & vbCrLf
sConsulta = sConsulta & "                ,ADJ_PROV_POS=(case when VI.USU is null then 9 else vi.ADJ_PROV_POS end), ADJ_PROV_WIDTH=(case when VI.USU is null then 700 else vi.ADJ_PROV_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "                ,ADJ_PROV_VISIBLE=(case when VI.USU is null then 1 else vi.ADJ_PROV_VISIBLE end), ADJ_PROV_LEVEL=(case when VI.USU is null then 1 else vi.ADJ_PROV_LEVEL end)" & vbCrLf
sConsulta = sConsulta & "                ,CANT_PROV_POS=(case when VI.USU is null then 8 else vi.CANT_PROV_POS end), CANT_PROV_WIDTH=(case when VI.USU is null then 900 else vi.CANT_PROV_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "                ,CANT_PROV_VISIBLE=(case when VI.USU is null then 1 else vi.CANT_PROV_VISIBLE end), CANT_PROV_LEVEL=(case when VI.USU is null then 1 else vi.CANT_PROV_LEVEL end)" & vbCrLf
sConsulta = sConsulta & "                ,IMP_ADJ_POS=(case when VI.USU is null then 1 else vi.IMP_ADJ_POS end), IMP_ADJ_WIDTH=(case when VI.USU is null then 500 else vi.IMP_ADJ_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "                ,IMP_ADJ_VISIBLE=(case when VI.USU is null then 0 else vi.IMP_ADJ_VISIBLE end), IMP_ADJ_LEVEL=(case when VI.USU is null then 0 else vi.IMP_ADJ_LEVEL end)" & vbCrLf
sConsulta = sConsulta & "                ,GRUPOS_WIDTH=(case when VI.USU is null then 1600 else vi.GRUPOS_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "                FROM CONF_VISTAS_ALL V  LEFT JOIN conf_vista_inicial_grupo VI ON VI.ES_GR=0 AND V.USU=VI.USU  WHERE V.ANYO=@ANYO AND V.GMN1=@GMN1 AND V.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "           IF (SELECT PLANTILLA_VISTAS FROM PROCE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE)=1 " & vbCrLf
sConsulta = sConsulta & "                   begin" & vbCrLf
sConsulta = sConsulta & "                      UPDATE PROCE SET PLANTILLA_VISTAS=0 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE                  " & vbCrLf
sConsulta = sConsulta & "                   end" & vbCrLf
sConsulta = sConsulta & "           FETCH NEXT FROM C_Vistas INTO @ANYO,@GMN1,@PROCE" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "          Close C_Vistas" & vbCrLf
sConsulta = sConsulta & "          DEALLOCATE C_Vistas" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "          DELETE FROM CONF_VISTAS_ALL_ATRIB  WHERE ATRIB=@ATRIB" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "          ---Si existe el atributo en la pesta�a de Grupo" & vbCrLf
sConsulta = sConsulta & "           DECLARE C_Vistas2 CURSOR LOCAL FOR SELECT DISTINCT ANYO,GMN1,PROCE,GRUPO FROM CONF_VISTAS_GRUPO_ATRIB WHERE  ATRIB=@ATRIB AND POSICION IS NOT NULL AND POSICION<>0" & vbCrLf
sConsulta = sConsulta & "           OPEN C_Vistas2" & vbCrLf
sConsulta = sConsulta & "           FETCH NEXT FROM C_Vistas2 INTO @ANYO,@GMN1,@PROCE,@GRUPO" & vbCrLf
sConsulta = sConsulta & "           WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "           BEGIN" & vbCrLf
sConsulta = sConsulta & "           UPDATE CONF_VISTAS_GRUPO_ATRIB SET POSICION=NULL,VISIBLE=0,FILA=0 WHERE  ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "           UPDATE CONF_VISTAS_GRUPO SET " & vbCrLf
sConsulta = sConsulta & "                PRECIO_PROV_POS=(case when VI.USU is null then 2 else vi.PRECIO_PROV_POS end), PRECIO_PROV_WIDTH=(case when VI.USU is null then 1600 else vi.PRECIO_PROV_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "                ,PRECIO_PROV_VISIBLE=(case when VI.USU is null then 1 else vi.PRECIO_PROV_VISIBLE end), PRECIO_PROV_LEVEL=(case when VI.USU is null then 0 else vi.PRECIO_PROV_LEVEL end)" & vbCrLf
sConsulta = sConsulta & "                ,ADJ_PROV_POS=(case when VI.USU is null then 9 else vi.ADJ_PROV_POS end), ADJ_PROV_WIDTH=(case when VI.USU is null then 700 else vi.ADJ_PROV_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "                ,ADJ_PROV_VISIBLE=(case when VI.USU is null then 1 else vi.ADJ_PROV_VISIBLE end), ADJ_PROV_LEVEL=(case when VI.USU is null then 1 else vi.ADJ_PROV_LEVEL end)" & vbCrLf
sConsulta = sConsulta & "                ,CANT_PROV_POS=(case when VI.USU is null then 8 else vi.CANT_PROV_POS end), CANT_PROV_WIDTH=(case when VI.USU is null then 900 else vi.CANT_PROV_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "                ,CANT_PROV_VISIBLE=(case when VI.USU is null then 1 else vi.CANT_PROV_VISIBLE end), CANT_PROV_LEVEL=(case when VI.USU is null then 1 else vi.CANT_PROV_LEVEL end)" & vbCrLf
sConsulta = sConsulta & "                ,IMP_ADJ_POS=(case when VI.USU is null then 1 else vi.IMP_ADJ_POS end), IMP_ADJ_WIDTH=(case when VI.USU is null then 500 else vi.IMP_ADJ_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "                ,IMP_ADJ_VISIBLE=(case when VI.USU is null then 0 else vi.IMP_ADJ_VISIBLE end), IMP_ADJ_LEVEL=(case when VI.USU is null then 0 else vi.IMP_ADJ_LEVEL end)" & vbCrLf
sConsulta = sConsulta & "                ,GRUPOS_WIDTH=(case when VI.USU is null then 1600 else vi.GRUPOS_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "                FROM CONF_VISTAS_GRUPO V  LEFT JOIN conf_vista_inicial_grupo VI ON VI.ES_GR=1 AND V.USU=VI.USU  WHERE V.ANYO=@ANYO AND V.GMN1=@GMN1 AND V.PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "            IF (SELECT PLANTILLA_VISTAS FROM PROCE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE)=1 " & vbCrLf
sConsulta = sConsulta & "                   begin" & vbCrLf
sConsulta = sConsulta & "                      UPDATE PROCE SET PLANTILLA_VISTAS=0 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE                  " & vbCrLf
sConsulta = sConsulta & "                   end" & vbCrLf
sConsulta = sConsulta & "            FETCH NEXT FROM C_Vistas2 INTO @ANYO,@GMN1,@PROCE,@GRUPO" & vbCrLf
sConsulta = sConsulta & "            END" & vbCrLf
sConsulta = sConsulta & "          Close C_Vistas2" & vbCrLf
sConsulta = sConsulta & "          DEALLOCATE C_Vistas2" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "          DELETE FROM CONF_VISTAS_GRUPO_ATRIB WHERE ATRIB=@ATRIB" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "          ---Si existe el atributo en la pesta�a de PROCE" & vbCrLf
sConsulta = sConsulta & "           DECLARE C_Vistas3 CURSOR LOCAL FOR SELECT DISTINCT ANYO,GMN1,PROCE FROM CONF_VISTAS_PROCE_ATRIB WHERE  ATRIB=@ATRIB AND POS IS NOT NULL AND POS<>0" & vbCrLf
sConsulta = sConsulta & "           OPEN C_Vistas3" & vbCrLf
sConsulta = sConsulta & "           FETCH NEXT FROM C_Vistas3 INTO @ANYO,@GMN1,@PROCE" & vbCrLf
sConsulta = sConsulta & "           WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "               BEGIN" & vbCrLf
sConsulta = sConsulta & "           UPDATE CONF_VISTAS_PROCE_ATRIB SET POS=NULL,VISIBLE=0 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "           UPDATE CONF_VISTAS_PROCE SET " & vbCrLf
sConsulta = sConsulta & "           CONSUMIDO_POS=(case when VI.USU is null then 1 else vi.CONSUMIDO_POS end), CONSUMIDO_WIDTH=(case when VI.USU is null then 850 else vi.CONSUMIDO_WIDTH end),CONSUMIDO_VISIBLE=(case when VI.USU is null then 1 else vi.CONSUMIDO_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "           ,IMP_POS=(case when VI.USU is null then 2 else vi.IMP_POS end), IMP_WIDTH=(case when VI.USU is null then 850 else vi.IMP_WIDTH end),IMP_VISIBLE=(case when VI.USU is null then 0 else vi.IMP_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "           ,ADJ_POS=(case when VI.USU is null then 3 else vi.ADJ_POS end), ADJ_WIDTH=(case when VI.USU is null then 850 else vi.ADJ_WIDTH end),ADJ_VISIBLE=(case when VI.USU is null then 1 else vi.ADJ_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "           ,AHORR_ADJ_POS=(case when VI.USU is null then 5 else vi.AHORR_ADJ_POS end), AHORR_ADJ_WIDTH=(case when VI.USU is null then 850 else vi.AHORR_ADJ_WIDTH end),AHORR_ADJ_VISIBLE=(case when VI.USU is null then 1 else vi.AHORR_ADJ_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "           ,AHORR_ADJ_PORCEN_POS=(case when VI.USU is null then 4 else vi.AHORR_ADJ_PORCEN_POS end), AHORR_ADJ_PORCEN_WIDTH=(case when VI.USU is null then 850 else vi.AHORR_ADJ_PORCEN_WIDTH end),AHORR_ADJ_PORCEN_VISIBLE=(case when VI.USU is null then 0 else vi.AHORR_ADJ_PORCEN_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "           ,AHORR_OFE_POS=(case when VI.USU is null then 7 else vi.AHORR_OFE_POS end), AHORR_OFE_WIDTH=(case when VI.USU is null then 850 else vi.AHORR_OFE_WIDTH end),AHORR_OFE_VISIBLE=(case when VI.USU is null then 1 else vi.AHORR_OFE_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "           ,AHORR_OFE_PORCEN_POS=(case when VI.USU is null then 6 else vi.AHORR_OFE_PORCEN_POS end), AHORR_OFE_PORCEN_WIDTH=(case when VI.USU is null then 850 else vi.AHORR_OFE_PORCEN_WIDTH end),AHORR_OFE_PORCEN_VISIBLE=(case when VI.USU is null then 0 else vi.AHORR_OFE_PORCEN_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "           ,GRUPO_WIDTH=(case when VI.USU is null then 2600 else vi.GRUPO_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "           FROM CONF_VISTAS_PROCE V LEFT JOIN CONF_VISTA_INICIAL_PROCE VI ON V.USU=VI.USU WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "           IF (SELECT PLANTILLA_VISTAS FROM PROCE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE)=1 " & vbCrLf
sConsulta = sConsulta & "                   begin" & vbCrLf
sConsulta = sConsulta & "                      UPDATE PROCE SET PLANTILLA_VISTAS=0 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE                  " & vbCrLf
sConsulta = sConsulta & "                   end" & vbCrLf
sConsulta = sConsulta & "           FETCH NEXT FROM C_Vistas3 INTO @ANYO,@GMN1,@PROCE" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "          Close C_Vistas3" & vbCrLf
sConsulta = sConsulta & "          DEALLOCATE C_Vistas3" & vbCrLf
sConsulta = sConsulta & "          DELETE FROM CONF_VISTAS_PROCE_ATRIB WHERE ATRIB=@ATRIB" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "            DELETE FROM CONF_VISTAS_ITEM_ATRIB WHERE ATRIB=@ATRIB" & vbCrLf
sConsulta = sConsulta & "            DELETE FROM OFE_ATRIB WHERE ATRIB_ID=@ATRIB" & vbCrLf
sConsulta = sConsulta & "            DELETE FROM OFE_GR_ATRIB WHERE ATRIB_ID=@ATRIB" & vbCrLf
sConsulta = sConsulta & "            DELETE FROM OFE_ITEM_ATRIB WHERE ATRIB_ID=@ATRIB" & vbCrLf
sConsulta = sConsulta & "            DELETE FROM PROCE_ATRIB_POND WHERE ATRIB_ID=@ATRIB" & vbCrLf
sConsulta = sConsulta & "            DELETE FROM PROCE_ATRIB_LISTA WHERE ATRIB_ID=@ATRIB" & vbCrLf
sConsulta = sConsulta & "            DELETE FROM PROCE_ATRIB WHERE ID=@ATRIB" & vbCrLf
sConsulta = sConsulta & "               END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "         ELSE" & vbCrLf
sConsulta = sConsulta & "             IF @TABLA=2" & vbCrLf
sConsulta = sConsulta & "                 BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       --Si existe el atributo en la pesta�a de ALL" & vbCrLf
sConsulta = sConsulta & "        DECLARE C_Vistas CURSOR LOCAL FOR SELECT PLANT FROM PLANTILLA_CONF_VISTAS_ALL_ATRIB WHERE  ATRIB=@ATRIB AND POSICION IS NOT NULL AND POSICION<>0" & vbCrLf
sConsulta = sConsulta & "        OPEN C_Vistas" & vbCrLf
sConsulta = sConsulta & "        FETCH NEXT FROM C_Vistas INTO @PLANT" & vbCrLf
sConsulta = sConsulta & "        WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "               BEGIN" & vbCrLf
sConsulta = sConsulta & "               UPDATE PLANTILLA_CONF_VISTAS_ALL_ATRIB SET POSICION=NULL,VISIBLE=0,FILA=0 WHERE PLANT=@PLANT" & vbCrLf
sConsulta = sConsulta & "               UPDATE PLANTILLA_CONF_VISTAS_ALL SET " & vbCrLf
sConsulta = sConsulta & "                    PRECIO_PROV_POS=2 , PRECIO_PROV_WIDTH=1600 ,PRECIO_PROV_VISIBLE=1, PRECIO_PROV_LEVEL=0," & vbCrLf
sConsulta = sConsulta & "                    ADJ_PROV_POS=9, ADJ_PROV_WIDTH=700, ADJ_PROV_VISIBLE=1, ADJ_PROV_LEVEL=1," & vbCrLf
sConsulta = sConsulta & "                    CANT_PROV_POS=8 , CANT_PROV_WIDTH=900 ,CANT_PROV_VISIBLE=1, CANT_PROV_LEVEL=1 ," & vbCrLf
sConsulta = sConsulta & "                    IMP_ADJ_POS=1, IMP_ADJ_WIDTH=500 ,IMP_ADJ_VISIBLE=0, IMP_ADJ_LEVEL=0 , GRUPOS_WIDTH=1600 WHERE PLANT=@PLANT" & vbCrLf
sConsulta = sConsulta & "                                 FETCH NEXT FROM C_Vistas INTO @PLANT" & vbCrLf
sConsulta = sConsulta & "                       END" & vbCrLf
sConsulta = sConsulta & "       Close C_Vistas" & vbCrLf
sConsulta = sConsulta & "       DEALLOCATE C_Vistas" & vbCrLf
sConsulta = sConsulta & "       DELETE FROM PLANTILLA_CONF_VISTAS_ALL_ATRIB  WHERE ATRIB=@ATRIB" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "       --Si existe el atributo en la pesta�a de grupo" & vbCrLf
sConsulta = sConsulta & "       DECLARE C_Vistas2 CURSOR LOCAL FOR SELECT PLANT FROM PLANTILLA_CONF_VISTAS_GRUPO_ATRIB WHERE  ATRIB=@ATRIB AND POSICION IS NOT NULL AND POSICION<>0" & vbCrLf
sConsulta = sConsulta & "        OPEN C_Vistas2" & vbCrLf
sConsulta = sConsulta & "        FETCH NEXT FROM C_Vistas2 INTO @PLANT" & vbCrLf
sConsulta = sConsulta & "        WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "               BEGIN" & vbCrLf
sConsulta = sConsulta & "               UPDATE PLANTILLA_CONF_VISTAS_GRUPO_ATRIB SET POSICION=NULL,VISIBLE=0,FILA=0 WHERE PLANT=@PLANT" & vbCrLf
sConsulta = sConsulta & "               UPDATE PLANTILLA_CONF_VISTAS_GRUPO SET " & vbCrLf
sConsulta = sConsulta & "                    PRECIO_PROV_POS=2 , PRECIO_PROV_WIDTH=1600 ,PRECIO_PROV_VISIBLE=1, PRECIO_PROV_LEVEL=0," & vbCrLf
sConsulta = sConsulta & "                    ADJ_PROV_POS=9, ADJ_PROV_WIDTH=700, ADJ_PROV_VISIBLE=1, ADJ_PROV_LEVEL=1," & vbCrLf
sConsulta = sConsulta & "                    CANT_PROV_POS=8 , CANT_PROV_WIDTH=900 ,CANT_PROV_VISIBLE=1, CANT_PROV_LEVEL=1 ," & vbCrLf
sConsulta = sConsulta & "                    IMP_ADJ_POS=1, IMP_ADJ_WIDTH=500 ,IMP_ADJ_VISIBLE=0, IMP_ADJ_LEVEL=0 , GRUPOS_WIDTH=1600 WHERE PLANT=@PLANT" & vbCrLf
sConsulta = sConsulta & "                                 FETCH NEXT FROM C_Vistas2 INTO @PLANT" & vbCrLf
sConsulta = sConsulta & "                       END" & vbCrLf
sConsulta = sConsulta & "       Close C_Vistas2" & vbCrLf
sConsulta = sConsulta & "       DEALLOCATE C_Vistas2" & vbCrLf
sConsulta = sConsulta & "                           DELETE FROM PLANTILLA_CONF_VISTAS_GRUPO_ATRIB WHERE ATRIB=@ATRIB" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       ---Si existe el atributo en la pesta�a de PROCE" & vbCrLf
sConsulta = sConsulta & "            DECLARE C_Vistas3 CURSOR LOCAL FOR SELECT PLANT FROM PLANTILLA_CONF_VISTAS_PROCE_ATRIB WHERE  ATRIB=@ATRIB AND POS IS NOT NULL AND POS<>0" & vbCrLf
sConsulta = sConsulta & "        OPEN C_Vistas3" & vbCrLf
sConsulta = sConsulta & "        FETCH NEXT FROM C_Vistas3 INTO @PLANT" & vbCrLf
sConsulta = sConsulta & "        WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "                   BEGIN" & vbCrLf
sConsulta = sConsulta & "               UPDATE PLANTILLA_CONF_VISTAS_PROCE_ATRIB SET POS=NULL,VISIBLE=0 WHERE PLANT=@PLANT" & vbCrLf
sConsulta = sConsulta & "               UPDATE PLANTILLA_CONF_VISTAS_PROCE SET " & vbCrLf
sConsulta = sConsulta & "               CONSUMIDO_POS=1, CONSUMIDO_WIDTH=850,CONSUMIDO_VISIBLE=1,IMP_POS=2, IMP_WIDTH=850, IMP_VISIBLE=0" & vbCrLf
sConsulta = sConsulta & "               , ADJ_POS=3, ADJ_WIDTH=850,ADJ_VISIBLE=1,AHORR_ADJ_POS=5, AHORR_ADJ_WIDTH=850, AHORR_ADJ_VISIBLE=1" & vbCrLf
sConsulta = sConsulta & "               , AHORR_ADJ_PORCEN_POS=4, AHORR_ADJ_PORCEN_WIDTH=850 ,AHORR_ADJ_PORCEN_VISIBLE=0" & vbCrLf
sConsulta = sConsulta & "               , AHORR_OFE_POS=7, AHORR_OFE_WIDTH=850,AHORR_OFE_VISIBLE=1" & vbCrLf
sConsulta = sConsulta & "               , AHORR_OFE_PORCEN_POS= 6, AHORR_OFE_PORCEN_WIDTH=850 ,AHORR_OFE_PORCEN_VISIBLE=0,GRUPO_WIDTH=2600 WHERE PLANT=@PLANT" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "                                   FETCH NEXT FROM C_Vistas3 INTO @PLANT" & vbCrLf
sConsulta = sConsulta & "                          END " & vbCrLf
sConsulta = sConsulta & "         Close C_Vistas3" & vbCrLf
sConsulta = sConsulta & "         DEALLOCATE C_Vistas3" & vbCrLf
sConsulta = sConsulta & "                  DELETE FROM PLANTILLA_CONF_VISTAS_PROCE_ATRIB WHERE ATRIB=@ATRIB" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "        DELETE FROM PLANTILLA_ATRIB_POND WHERE ATRIB_ID=@ATRIB" & vbCrLf
sConsulta = sConsulta & "                            DELETE FROM PLANTILLA_ATRIB_LISTA WHERE ATRIB_ID=@ATRIB" & vbCrLf
sConsulta = sConsulta & "                            DELETE FROM PLANTILLA_ATRIB WHERE ID=@ATRIB" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                 END" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE SP_ELIMINAR_GRUPO (@ANYO SMALLINT,@GMN1 VARCHAR(50),@PROCE SMALLINT,@GRUPO VARCHAR(50))  AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ITEM INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @RESTVISTAS AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCEVISTAS AS INT" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  SET @PROCEVISTAS=0" & vbCrLf
sConsulta = sConsulta & "  SET @RESTVISTAS=0" & vbCrLf
sConsulta = sConsulta & "  SELECT @RESTVISTAS= COUNT(CONF_VISTAS_PROCE_ATRIB.ATRIB) FROM CONF_VISTAS_PROCE_ATRIB INNER JOIN PROCE_ATRIB " & vbCrLf
sConsulta = sConsulta & "          ON CONF_VISTAS_PROCE_ATRIB.ATRIB =PROCE_ATRIB.ID WHERE PROCE_ATRIB.ANYO=@ANYO AND PROCE_ATRIB.GMN1=@GMN1 AND PROCE_ATRIB.PROCE=@PROCE AND PROCE_ATRIB.GRUPO=@GRUPO " & vbCrLf
sConsulta = sConsulta & "        AND CONF_VISTAS_PROCE_ATRIB.POS IS NOT NULL AND CONF_VISTAS_PROCE_ATRIB.POS<>0" & vbCrLf
sConsulta = sConsulta & "  IF @RESTVISTAS>0" & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "           SET @PROCEVISTAS=1" & vbCrLf
sConsulta = sConsulta & "           UPDATE CONF_VISTAS_PROCE_ATRIB SET POS=NULL,VISIBLE=0 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "           UPDATE CONF_VISTAS_PROCE SET " & vbCrLf
sConsulta = sConsulta & "           CONSUMIDO_POS=(case when VI.USU is null then 1 else vi.CONSUMIDO_POS end), CONSUMIDO_WIDTH=(case when VI.USU is null then 850 else vi.CONSUMIDO_WIDTH end),CONSUMIDO_VISIBLE=(case when VI.USU is null then 1 else vi.CONSUMIDO_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "           ,IMP_POS=(case when VI.USU is null then 2 else vi.IMP_POS end), IMP_WIDTH=(case when VI.USU is null then 850 else vi.IMP_WIDTH end),IMP_VISIBLE=(case when VI.USU is null then 0 else vi.IMP_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "           ,ADJ_POS=(case when VI.USU is null then 3 else vi.ADJ_POS end), ADJ_WIDTH=(case when VI.USU is null then 850 else vi.ADJ_WIDTH end),ADJ_VISIBLE=(case when VI.USU is null then 1 else vi.ADJ_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "           ,AHORR_ADJ_POS=(case when VI.USU is null then 5 else vi.AHORR_ADJ_POS end), AHORR_ADJ_WIDTH=(case when VI.USU is null then 850 else vi.AHORR_ADJ_WIDTH end),AHORR_ADJ_VISIBLE=(case when VI.USU is null then 1 else vi.AHORR_ADJ_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "           ,AHORR_ADJ_PORCEN_POS=(case when VI.USU is null then 4 else vi.AHORR_ADJ_PORCEN_POS end), AHORR_ADJ_PORCEN_WIDTH=(case when VI.USU is null then 850 else vi.AHORR_ADJ_PORCEN_WIDTH end),AHORR_ADJ_PORCEN_VISIBLE=(case when VI.USU is null then 0 else vi.AHORR_ADJ_PORCEN_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "           ,AHORR_OFE_POS=(case when VI.USU is null then 7 else vi.AHORR_OFE_POS end), AHORR_OFE_WIDTH=(case when VI.USU is null then 850 else vi.AHORR_OFE_WIDTH end),AHORR_OFE_VISIBLE=(case when VI.USU is null then 1 else vi.AHORR_OFE_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "           ,AHORR_OFE_PORCEN_POS=(case when VI.USU is null then 6 else vi.AHORR_OFE_PORCEN_POS end), AHORR_OFE_PORCEN_WIDTH=(case when VI.USU is null then 850 else vi.AHORR_OFE_PORCEN_WIDTH end),AHORR_OFE_PORCEN_VISIBLE=(case when VI.USU is null then 0 else vi.AHORR_OFE_PORCEN_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "           ,GRUPO_WIDTH=(case when VI.USU is null then 2600 else vi.GRUPO_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "           FROM CONF_VISTAS_PROCE V LEFT JOIN CONF_VISTA_INICIAL_PROCE VI ON V.USU=VI.USU WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "      end" & vbCrLf
sConsulta = sConsulta & "   DELETE CONF_VISTAS_PROCE_ATRIB FROM CONF_VISTAS_PROCE_ATRIB" & vbCrLf
sConsulta = sConsulta & "         INNER JOIN PROCE_ATRIB ON CONF_VISTAS_PROCE_ATRIB.ATRIB =PROCE_ATRIB.ID AND PROCE_ATRIB.ANYO=@ANYO AND PROCE_ATRIB.GMN1=@GMN1 AND PROCE_ATRIB.PROCE=@PROCE AND PROCE_ATRIB.GRUPO=@GRUPO " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   DELETE FROM CONF_VISTAS_GRUPO_PROVE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "   DELETE FROM CONF_VISTAS_GRUPO_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "   DELETE FROM CONF_VISTAS_GRUPO WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  SET @RESTVISTAS=0" & vbCrLf
sConsulta = sConsulta & "  SELECT @RESTVISTAS= COUNT(CONF_VISTAS_ALL_ATRIB.ATRIB) FROM CONF_VISTAS_ALL_ATRIB INNER JOIN PROCE_ATRIB " & vbCrLf
sConsulta = sConsulta & "          ON CONF_VISTAS_ALL_ATRIB.ATRIB =PROCE_ATRIB.ID WHERE PROCE_ATRIB.ANYO=@ANYO AND PROCE_ATRIB.GMN1=@GMN1 AND PROCE_ATRIB.PROCE=@PROCE AND PROCE_ATRIB.GRUPO=@GRUPO " & vbCrLf
sConsulta = sConsulta & "        AND CONF_VISTAS_ALL_ATRIB.POSICION IS NOT NULL AND CONF_VISTAS_ALL_ATRIB.POSICION<>0" & vbCrLf
sConsulta = sConsulta & "  IF @RESTVISTAS>0" & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "           SET @PROCEVISTAS=1   " & vbCrLf
sConsulta = sConsulta & "           UPDATE CONF_VISTAS_ALL_ATRIB SET POSICION=NULL,VISIBLE=0,FILA=0 WHERE  ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "           UPDATE CONF_VISTAS_ALL SET " & vbCrLf
sConsulta = sConsulta & "                PRECIO_PROV_POS=(case when VI.USU is null then 2 else vi.PRECIO_PROV_POS end), PRECIO_PROV_WIDTH=(case when VI.USU is null then 1600 else vi.PRECIO_PROV_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "                ,PRECIO_PROV_VISIBLE=(case when VI.USU is null then 1 else vi.PRECIO_PROV_VISIBLE end), PRECIO_PROV_LEVEL=(case when VI.USU is null then 0 else vi.PRECIO_PROV_LEVEL end)" & vbCrLf
sConsulta = sConsulta & "                ,ADJ_PROV_POS=(case when VI.USU is null then 9 else vi.ADJ_PROV_POS end), ADJ_PROV_WIDTH=(case when VI.USU is null then 700 else vi.ADJ_PROV_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "                ,ADJ_PROV_VISIBLE=(case when VI.USU is null then 1 else vi.ADJ_PROV_VISIBLE end), ADJ_PROV_LEVEL=(case when VI.USU is null then 1 else vi.ADJ_PROV_LEVEL end)" & vbCrLf
sConsulta = sConsulta & "                ,CANT_PROV_POS=(case when VI.USU is null then 8 else vi.CANT_PROV_POS end), CANT_PROV_WIDTH=(case when VI.USU is null then 900 else vi.CANT_PROV_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "                ,CANT_PROV_VISIBLE=(case when VI.USU is null then 1 else vi.CANT_PROV_VISIBLE end), CANT_PROV_LEVEL=(case when VI.USU is null then 1 else vi.CANT_PROV_LEVEL end)" & vbCrLf
sConsulta = sConsulta & "                ,IMP_ADJ_POS=(case when VI.USU is null then 1 else vi.IMP_ADJ_POS end), IMP_ADJ_WIDTH=(case when VI.USU is null then 500 else vi.IMP_ADJ_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "                ,IMP_ADJ_VISIBLE=(case when VI.USU is null then 0 else vi.IMP_ADJ_VISIBLE end), IMP_ADJ_LEVEL=(case when VI.USU is null then 0 else vi.IMP_ADJ_LEVEL end)" & vbCrLf
sConsulta = sConsulta & "                ,GRUPOS_WIDTH=(case when VI.USU is null then 1600 else vi.GRUPOS_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "                FROM CONF_VISTAS_ALL V  LEFT JOIN conf_vista_inicial_grupo VI ON VI.ES_GR=0 AND V.USU=VI.USU  WHERE V.ANYO=@ANYO AND V.GMN1=@GMN1 AND V.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "      end" & vbCrLf
sConsulta = sConsulta & "   DELETE CONF_VISTAS_ALL_ATRIB FROM CONF_VISTAS_ALL_ATRIB" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN PROCE_ATRIB ON CONF_VISTAS_ALL_ATRIB.ATRIB =PROCE_ATRIB.ID AND PROCE_ATRIB.ANYO=@ANYO AND PROCE_ATRIB.GMN1=@GMN1 AND PROCE_ATRIB.PROCE=@PROCE AND PROCE_ATRIB.GRUPO=@GRUPO " & vbCrLf
sConsulta = sConsulta & "   IF @PROCEVISTAS=1   " & vbCrLf
sConsulta = sConsulta & "       begin" & vbCrLf
sConsulta = sConsulta & "           IF (SELECT PLANTILLA_VISTAS FROM PROCE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE)=1 " & vbCrLf
sConsulta = sConsulta & "                   begin" & vbCrLf
sConsulta = sConsulta & "                      UPDATE PROCE SET PLANTILLA_VISTAS=0 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE                  " & vbCrLf
sConsulta = sConsulta & "                   end" & vbCrLf
sConsulta = sConsulta & "       end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   DELETE FROM PROCE_GR_PRES1 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "   DELETE FROM PROCE_GR_PRES2 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "   DELETE FROM PROCE_GR_PRES3 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "   DELETE FROM PROCE_GR_PRES4 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "   DELETE FROM PROCE_GR_UON1 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "   DELETE FROM PROCE_GR_UON2 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "   DELETE FROM PROCE_GR_UON3 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "   DELETE FROM PROCE_GRUPO_ESP WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "   DELETE FROM CONF_VISTAS_ITEM WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "   DELETE FROM CONF_VISTAS_ITEM_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "   DELETE FROM CONF_VISTAS_ITEM_PROVE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "   DELETE FROM USAR_GR_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   DECLARE C CURSOR FOR SELECT ID FROM ITEM WHERE ANYO=@ANYO AND PROCE=@PROCE AND GMN1=@GMN1 AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "   OPEN C" & vbCrLf
sConsulta = sConsulta & "   FETCH NEXT FROM C INTO @ITEM" & vbCrLf
sConsulta = sConsulta & "   WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       DELETE FROM OFE_ITEM_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM" & vbCrLf
sConsulta = sConsulta & "       DELETE FROM OFEMIN WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM" & vbCrLf
sConsulta = sConsulta & "       DELETE FROM OFE_ITEM_ADJUN WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM" & vbCrLf
sConsulta = sConsulta & "       DELETE FROM ITEM_OFE_WEB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM" & vbCrLf
sConsulta = sConsulta & "       DELETE FROM ITEM_ADJ WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM" & vbCrLf
sConsulta = sConsulta & "       DELETE FROM ITEM_ADJREU WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM" & vbCrLf
sConsulta = sConsulta & "       DELETE FROM ITEM_OFE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM" & vbCrLf
sConsulta = sConsulta & "       DELETE FROM ITEM_OBJ WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM" & vbCrLf
sConsulta = sConsulta & "       DELETE FROM ITEM_PRES WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM" & vbCrLf
sConsulta = sConsulta & "       DELETE FROM ITEM_PRESCON WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM" & vbCrLf
sConsulta = sConsulta & "       DELETE FROM ITEM_PRESCON3 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM" & vbCrLf
sConsulta = sConsulta & "       DELETE FROM ITEM_PRESCON4 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM" & vbCrLf
sConsulta = sConsulta & "       DELETE FROM ITEM_PRESPROY WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM" & vbCrLf
sConsulta = sConsulta & "       DELETE FROM ITEM_UON1 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM" & vbCrLf
sConsulta = sConsulta & "       DELETE FROM ITEM_UON2 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM" & vbCrLf
sConsulta = sConsulta & "       DELETE FROM ITEM_UON3 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM" & vbCrLf
sConsulta = sConsulta & "       DELETE FROM ITEM_ESP WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM" & vbCrLf
sConsulta = sConsulta & "       DELETE FROM ITEM WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ID=@ITEM" & vbCrLf
sConsulta = sConsulta & "   FETCH NEXT FROM C INTO @ITEM" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "   CLOSE C" & vbCrLf
sConsulta = sConsulta & "   DEALLOCATE C" & vbCrLf
sConsulta = sConsulta & "   DELETE FROM OFE_GR_ADJUN WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "   DELETE FROM OFE_GR_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "   DELETE FROM GRUPO_OFE_REU WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "   DELETE FROM GRUPO_OFE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "   DELETE PROCE_ATRIB_POND FROM PROCE_ATRIB_POND" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PROCE_ATRIB ON PROCE_ATRIB_POND.ATRIB_ID =PROCE_ATRIB.ID AND PROCE_ATRIB.ANYO=@ANYO AND PROCE_ATRIB.GMN1=@GMN1 AND PROCE_ATRIB.PROCE=@PROCE AND PROCE_ATRIB.GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "   DELETE PROCE_ATRIB_LISTA FROM PROCE_ATRIB_LISTA" & vbCrLf
sConsulta = sConsulta & "      INNER JOIN PROCE_ATRIB ON PROCE_ATRIB_LISTA.ATRIB_ID =PROCE_ATRIB.ID AND PROCE_ATRIB.ANYO=@ANYO AND PROCE_ATRIB.GMN1=@GMN1 AND PROCE_ATRIB.PROCE=@PROCE  AND PROCE_ATRIB.GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "   DELETE FROM PROCE_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   DELETE FROM PROCE_GRUPO_DEF WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "   DELETE FROM PROCE_GRUPO WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND COD=@GRUPO" & vbCrLf
sConsulta = sConsulta & "Return" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER ITEM_OFE_TG_UPD ON dbo.ITEM_OFE " & vbCrLf
sConsulta = sConsulta & "FOR UPDATE " & vbCrLf
sConsulta = sConsulta & "AS " & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN1 VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCE INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ITEM INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRECIO AS FLOAT " & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVEINS varchar(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @OFEINS smallint" & vbCrLf
sConsulta = sConsulta & "DECLARE @ULTINS smallint" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVENEW AS VARCHAR(50) " & vbCrLf
sConsulta = sConsulta & "DECLARE @OFENEW AS SMALLINT " & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_ITEM_OFE_Upd CURSOR FOR SELECT ANYO,GMN1,PROCE,ITEM,PROVE,NUM, ULT FROM INSERTED " & vbCrLf
sConsulta = sConsulta & "OPEN curTG_ITEM_OFE_Upd " & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_ITEM_OFE_Upd INTO @ANYO,@GMN1,@PROCE,@ITEM,@PROVEINS,@OFEINS,@ULTINS" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0 " & vbCrLf
sConsulta = sConsulta & "BEGIN " & vbCrLf
sConsulta = sConsulta & " IF (SELECT SUBASTA FROM PROCE_DEF WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE) = 1 AND" & vbCrLf
sConsulta = sConsulta & "    (SELECT ENVIADA FROM PROCE_OFE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE = @PROVEINS AND OFE = @OFEINS) =1" & vbCrLf
sConsulta = sConsulta & " BEGIN " & vbCrLf
sConsulta = sConsulta & "     IF UPDATE(PRECIO) " & vbCrLf
sConsulta = sConsulta & "     BEGIN " & vbCrLf
sConsulta = sConsulta & "        --Obtenemos el precio mas bajo ofertado para ese ITEM" & vbCrLf
sConsulta = sConsulta & "        set @precio = null" & vbCrLf
sConsulta = sConsulta & "        set @provenew = null" & vbCrLf
sConsulta = sConsulta & "        SELECT TOP 1 @PRECIO= A.PRECIO / (B.CAMBIO * C.CAMBIO), @PROVENEW=A.PROVE,@OFENEW=A.NUM " & vbCrLf
sConsulta = sConsulta & "          FROM ITEM_OFE A " & vbCrLf
sConsulta = sConsulta & "                   INNER JOIN PROCE_OFE B " & vbCrLf
sConsulta = sConsulta & "                           ON A.ANYO=B.ANYO " & vbCrLf
sConsulta = sConsulta & "                          AND A.GMN1=B.GMN1 " & vbCrLf
sConsulta = sConsulta & "                          AND A.PROCE=B.PROCE " & vbCrLf
sConsulta = sConsulta & "                          AND A.PROVE=B.PROVE " & vbCrLf
sConsulta = sConsulta & "                          AND A.NUM=B.OFE" & vbCrLf
sConsulta = sConsulta & "                   INNER JOIN PROCE C " & vbCrLf
sConsulta = sConsulta & "                           ON A.ANYO=C.ANYO " & vbCrLf
sConsulta = sConsulta & "                          AND A.GMN1=C.GMN1 " & vbCrLf
sConsulta = sConsulta & "                          AND A.PROCE = C.COD" & vbCrLf
sConsulta = sConsulta & "                   INNER JOIN PROCE_PROVE PP" & vbCrLf
sConsulta = sConsulta & "                           ON A.ANYO = PP.ANYO" & vbCrLf
sConsulta = sConsulta & "                          AND A.GMN1 = PP.GMN1" & vbCrLf
sConsulta = sConsulta & "                          AND A.PROCE = PP.PROCE" & vbCrLf
sConsulta = sConsulta & "                          AND A.PROVE = PP.PROVE" & vbCrLf
sConsulta = sConsulta & "                          " & vbCrLf
sConsulta = sConsulta & "         WHERE A.ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "           AND A.GMN1=@GMN1  " & vbCrLf
sConsulta = sConsulta & "           AND A.PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "           AND A.ITEM=@ITEM " & vbCrLf
sConsulta = sConsulta & "           AND A.ULT=1 " & vbCrLf
sConsulta = sConsulta & "           AND A.PRECIO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "           AND B.ENVIADA = 1" & vbCrLf
sConsulta = sConsulta & "           AND PP.PUB = 1" & vbCrLf
sConsulta = sConsulta & "      ORDER BY A.PRECIO / (B.CAMBIO * C.CAMBIO) ASC " & vbCrLf
sConsulta = sConsulta & "      DECLARE @ULTOFE smallint" & vbCrLf
sConsulta = sConsulta & "      IF @ULTINS=1 AND @PROVEINS=@PROVENEW AND @OFEINS=@OFENEW" & vbCrLf
sConsulta = sConsulta & "          set @ULTOFE=1" & vbCrLf
sConsulta = sConsulta & "      ELSE" & vbCrLf
sConsulta = sConsulta & "          set @ULTOFE=0" & vbCrLf
sConsulta = sConsulta & "         " & vbCrLf
sConsulta = sConsulta & "      --Actualizamos OFEMIN con los datos de la oferta m�s baja                       " & vbCrLf
sConsulta = sConsulta & "      UPDATE OFEMIN " & vbCrLf
sConsulta = sConsulta & "         SET PRECIO=@PRECIO," & vbCrLf
sConsulta = sConsulta & "             PROVE=@PROVENEW," & vbCrLf
sConsulta = sConsulta & "             OFE=@OFENEW," & vbCrLf
sConsulta = sConsulta & "             ULTOFE = @ULTOFE" & vbCrLf
sConsulta = sConsulta & "       WHERE ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "         AND GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "         AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "         AND ITEM=@ITEM " & vbCrLf
sConsulta = sConsulta & "      EXEC SP_CALCULAR_DETALLE_PUJAS @ANYO, @GMN1, @PROCE, @ITEM" & vbCrLf
sConsulta = sConsulta & "    END" & vbCrLf
sConsulta = sConsulta & "  END  " & vbCrLf
sConsulta = sConsulta & " FETCH NEXT FROM curTG_ITEM_OFE_Upd INTO @ANYO,@GMN1,@PROCE,@ITEM,@PROVEINS,@OFEINS,@ULTINS" & vbCrLf
sConsulta = sConsulta & "End " & vbCrLf
sConsulta = sConsulta & "Close curTG_ITEM_OFE_Upd " & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_ITEM_OFE_Upd" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER PROCE_ATRIB_TG_DEL ON dbo.PROCE_ATRIB" & vbCrLf
sConsulta = sConsulta & "FOR DELETE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @GRUPO VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN1 VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCE INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @COUNT INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @APLIC_PREC INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @DEF_PREC INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @ORDEN INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @AMBITO INTEGER" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_PROCE_ATRIBS_DEL CURSOR LOCAL FOR SELECT ID,ANYO,GMN1,PROCE,GRUPO,APLIC_PREC,DEF_PREC,ORDEN_ATRIB,AMBITO FROM DELETED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_PROCE_ATRIBS_DEL" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_ATRIBS_DEL INTO @ID,@ANYO,@GMN1,@PROCE,@GRUPO,@APLIC_PREC,@DEF_PREC,@ORDEN,@AMBITO" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "    " & vbCrLf
sConsulta = sConsulta & "IF @APLIC_PREC = 1" & vbCrLf
sConsulta = sConsulta & " begin" & vbCrLf
sConsulta = sConsulta & "   IF @GRUPO IS NULL" & vbCrLf
sConsulta = sConsulta & "        DELETE USAR_GR_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ATRIB =@ID " & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "        DELETE USAR_GR_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ATRIB =@ID AND GRUPO =@GRUPO         " & vbCrLf
sConsulta = sConsulta & " end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @GRUPO IS NULL" & vbCrLf
sConsulta = sConsulta & "   UPDATE PROCE_ATRIB SET ORDEN_ATRIB=ORDEN_ATRIB - 1 WHERE  ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "   AND GRUPO IS NULL  AND ORDEN_ATRIB > @ORDEN" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   UPDATE PROCE_ATRIB SET ORDEN_ATRIB=ORDEN_ATRIB - 1 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   AND GRUPO =@GRUPO AND ORDEN_ATRIB > @ORDEN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_ATRIBS_DEL INTO @ID,@ANYO,@GMN1,@PROCE,@GRUPO,@APLIC_PREC,@DEF_PREC,@ORDEN,@AMBITO" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_PROCE_ATRIBS_DEL" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_PROCE_ATRIBS_DEL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PRES_ART_TG_INSUPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PRES_ART_TG_INSUPD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PRES_ART_TG_DEL]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PRES_ART_TG_DEL]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PRES_ART_TG_UPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PRES_ART_TG_UPD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PRES_ART_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PRES_ART_TG_INS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER PRES_ART_TG_DEL ON [dbo].[PRES_ART] " & vbCrLf
sConsulta = sConsulta & "FOR  DELETE " & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN1 VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN2 VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN3 VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN4 VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRES FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @OBJ FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @CANT FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @DIRECTA AS INTEGER" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_PRES_ART_Del CURSOR FOR SELECT ANYO,GMN1,GMN2,GMN3,GMN4,COD,PRES,OBJ,CANT FROM DELETED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_PRES_ART_Del" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PRES_ART_Del INTO @ANYO,@GMN1,@GMN2,@GMN3,@GMN4,@COD,@PRES,@OBJ,@CANT" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  SET @DIRECTA=(SELECT PRESDIRECTA FROM PRES_MAT4 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3  AND GMN4=@GMN4)" & vbCrLf
sConsulta = sConsulta & "  IF @DIRECTA=0" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "     IF @CANT IS NULL " & vbCrLf
sConsulta = sConsulta & "       SET  @CANT=0" & vbCrLf
sConsulta = sConsulta & "     IF @PRES IS NULL " & vbCrLf
sConsulta = sConsulta & "       SET  @PRES=0" & vbCrLf
sConsulta = sConsulta & "     IF @OBJ IS NULL " & vbCrLf
sConsulta = sConsulta & "       SET  @OBJ=0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "     UPDATE PRES_MAT4 SET PRES=(PRES-(@PRES*@CANT)), OBJ=(OBJ-(@OBJ*@CANT))  WHERE ANYO=@ANYO AND GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3  AND GMN4=@GMN4" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & " FETCH NEXT FROM curTG_PRES_ART_Del INTO @ANYO,@GMN1,@GMN2,@GMN3,@GMN4,@COD,@PRES,@OBJ,@CANT" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "Close curTG_PRES_ART_Del" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_PRES_ART_Del" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER PRES_ART_TG_UPD ON dbo.PRES_ART" & vbCrLf
sConsulta = sConsulta & "FOR UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN1 VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN2 VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN3 VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN4 VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @DIRECTA INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @CANTOLD FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @CANTNEW FLOAT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_PRES_ART_Upd CURSOR FOR SELECT ANYO,GMN1,GMN2,GMN3,GMN4,COD,CANT FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_PRES_ART_Upd" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PRES_ART_Upd INTO @ANYO,@GMN1,@GMN2,@GMN3,@GMN4,@COD,@CANTNEW" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF UPDATE(PRES)  OR UPDATE(OBJ)  OR UPDATE (CANT)" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @DIRECTA=(SELECT PRESDIRECTA FROM PRES_MAT4 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3  AND GMN4=@GMN4)" & vbCrLf
sConsulta = sConsulta & "         IF @DIRECTA=0" & vbCrLf
sConsulta = sConsulta & "       begin" & vbCrLf
sConsulta = sConsulta & "                UPDATE PRES_MAT4 SET PRES=(SELECT SUM(PRES*CANT) FROM PRES_ART WHERE ANYO=@ANYO AND GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@GMN4)  ," & vbCrLf
sConsulta = sConsulta & "                      OBJ=(SELECT SUM(OBJ*CANT) FROM PRES_ART WHERE ANYO=@ANYO AND GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@GMN4) ," & vbCrLf
sConsulta = sConsulta & "                      PRESDIRECTA=0 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3  AND GMN4=@GMN4" & vbCrLf
sConsulta = sConsulta & "       end" & vbCrLf
sConsulta = sConsulta & "         ELSE" & vbCrLf
sConsulta = sConsulta & "       begin" & vbCrLf
sConsulta = sConsulta & "            SET @CANTOLD=(SELECT CANT FROM DELETED)" & vbCrLf
sConsulta = sConsulta & "            IF @CANTNEW=@CANTOLD" & vbCrLf
sConsulta = sConsulta & "           begin" & vbCrLf
sConsulta = sConsulta & "                      UPDATE PRES_MAT4 SET PRES=(SELECT SUM(PRES*CANT) FROM PRES_ART WHERE ANYO=@ANYO AND GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@GMN4)  ," & vbCrLf
sConsulta = sConsulta & "                      OBJ=(SELECT SUM(OBJ*CANT) FROM PRES_ART WHERE ANYO=@ANYO AND GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@GMN4) ," & vbCrLf
sConsulta = sConsulta & "                      PRESDIRECTA=0 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3  AND GMN4=@GMN4" & vbCrLf
sConsulta = sConsulta & "           end" & vbCrLf
sConsulta = sConsulta & "       end" & vbCrLf
sConsulta = sConsulta & " END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & " UPDATE PRES_ART SET FECACT=GETDATE() WHERE ANYO=@ANYO AND GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@GMN4 AND COD=@COD" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PRES_ART_Upd INTO @ANYO,@GMN1,@GMN2,@GMN3,@GMN4,@COD,@CANTNEW" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_PRES_ART_Upd" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_PRES_ART_Upd" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER PRES_ART_TG_INS ON dbo.PRES_ART" & vbCrLf
sConsulta = sConsulta & "FOR INSERT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN1 VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN2 VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN3 VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN4 VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @COUNT INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @DIRECTA INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRES FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @OBJ FLOAT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_PRES_ART_Ins CURSOR FOR SELECT ANYO,GMN1,GMN2,GMN3,GMN4,COD,PRES,OBJ FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_PRES_ART_Ins" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PRES_ART_Ins INTO @ANYO,@GMN1,@GMN2,@GMN3,@GMN4,@COD,@PRES,@OBJ" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "    SELECT @COUNT=COUNT(*) FROM PRES_MAT4 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@GMN4" & vbCrLf
sConsulta = sConsulta & "     IF @COUNT=0 " & vbCrLf
sConsulta = sConsulta & "   begin" & vbCrLf
sConsulta = sConsulta & "     INSERT INTO PRES_MAT4 ( ANYO,GMN1,GMN2,GMN3,GMN4,PRES,OBJ,PRESDIRECTA)  SELECT @ANYO,@GMN1,@GMN2,@GMN3,@GMN4,SUM(PRES*CANT) ,SUM(OBJ*CANT),0 FROM PRES_ART WHERE ANYO=@ANYO AND GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@GMN4" & vbCrLf
sConsulta = sConsulta & "   end" & vbCrLf
sConsulta = sConsulta & "     ELSE" & vbCrLf
sConsulta = sConsulta & "   begin" & vbCrLf
sConsulta = sConsulta & "    SET @DIRECTA=(SELECT PRESDIRECTA FROM PRES_MAT4 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3  AND GMN4=@GMN4)" & vbCrLf
sConsulta = sConsulta & "    IF @DIRECTA=0 or (@DIRECTA=1 AND (@PRES IS NOT NULL OR @OBJ IS NOT NULL))" & vbCrLf
sConsulta = sConsulta & "                      begin" & vbCrLf
sConsulta = sConsulta & "       UPDATE PRES_MAT4 SET PRES=(SELECT SUM(PRES*CANT) FROM PRES_ART WHERE ANYO=@ANYO AND GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@GMN4)  ," & vbCrLf
sConsulta = sConsulta & "                          OBJ=(SELECT SUM(OBJ*CANT) FROM PRES_ART WHERE ANYO=@ANYO AND GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@GMN4) ," & vbCrLf
sConsulta = sConsulta & "                          PRESDIRECTA=0 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3  AND GMN4=@GMN4" & vbCrLf
sConsulta = sConsulta & "                     end" & vbCrLf
sConsulta = sConsulta & "            end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   UPDATE PRES_ART SET FECACT=GETDATE() WHERE ANYO=@ANYO AND GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@GMN4 AND COD=@COD" & vbCrLf
sConsulta = sConsulta & "   FETCH NEXT FROM curTG_PRES_ART_Ins INTO @ANYO,@GMN1,@GMN2,@GMN3,@GMN4,@COD,@PRES,@OBJ" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "Close curTG_PRES_ART_Ins" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_PRES_ART_Ins" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Private Sub V_2_14_Tablas_04()
Dim sConsulta As String

sConsulta = ""
sConsulta = sConsulta & "ALTER TABLE LINEAS_PEDIDO ALTER COLUMN DESCR_LIBRE VARCHAR(200) NULL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LOG_LINEAS_PEDIDO ALTER COLUMN DESCR_LIBRE VARCHAR(200) NULL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion2_14_00_05A2_14_00_06() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim bBool As Boolean

On Error GoTo error
    

sConsulta = "BEGIN TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = True
ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

frmProgreso.lblDetalle = "Modificamos PROCE_PROVE_PET"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

sConsulta = "UPDATE PROCE_PROVE_PET SET FECHAREU = PROCE.FECULTREU "
sConsulta = sConsulta & " FROM PROCE_PROVE_PET INNER JOIN PROCE ON"
sConsulta = sConsulta & " PROCE_PROVE_PET.anyo = Proce.anyo And PROCE_PROVE_PET.gmn1 = Proce.gmn1 And PROCE_PROVE_PET.Proce = Proce.COD"
sConsulta = sConsulta & " Where Proce.ADJDIR = 0 And PROCE_PROVE_PET.TIPOPET = 3"
sConsulta = sConsulta & " AND PROCE_PROVE_PET.FECHAREU=CONVERT(DATETIME,CONVERT(VARCHAR,DAY(PROCE.FECULTREU)) + '/' + CONVERT(VARCHAR,MONTH(PROCE.FECULTREU)) + '/' + CONVERT(VARCHAR,YEAR(PROCE.FECULTREU)) ,103)"
ExecuteSQL gRDOCon, sConsulta

frmProgreso.lblDetalle = "Actualizamos storeds"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_14_Storeds_06


sConsulta = "UPDATE VERSION SET NUM ='2.14.00.06'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "COMMIT TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = False

CodigoDeActualizacion2_14_00_05A2_14_00_06 = True
Exit Function
    
error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_14_00_05A2_14_00_06 = False

End Function


Private Sub V_2_14_Storeds_06()


Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ITEMS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_ITEMS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE  SP_ITEMS  @ANYO INT,@GMN1 VARCHAR(50),@PROCE INT,@GRUPO VARCHAR(50)=null, @PROVE varchar(50), @OFE smallint, @INTERNO TINYINT = NULL, @TODOS TINYINT = 0, @MON varchar(50) = null  AS " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF " & vbCrLf
sConsulta = sConsulta & "set NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @CAM FLOAT " & vbCrLf
sConsulta = sConsulta & "DECLARE @EQUIVPROC FLOAT  " & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCEEST smallint" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @TIPOATRIB tinyint" & vbCrLf
sConsulta = sConsulta & "DECLARE @CODATRIB varchar(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDPROCEATRIB int" & vbCrLf
sConsulta = sConsulta & "DECLARE @PARAMDEF NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @ALTER NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @INSERT NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "SELECT @EQUIVPROC = CAMBIO, @PROCEEST =EST " & vbCrLf
sConsulta = sConsulta & "  FROM PROCE " & vbCrLf
sConsulta = sConsulta & " WHERE ANYO=@ANYO" & vbCrLf
sConsulta = sConsulta & "   AND GMN1=@GMN1" & vbCrLf
sConsulta = sConsulta & "   AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "SET @CAM =( SELECT CAMBIO" & vbCrLf
sConsulta = sConsulta & "             FROM PROCE_OFE" & vbCrLf
sConsulta = sConsulta & "            WHERE ANYO=@ANYO" & vbCrLf
sConsulta = sConsulta & "              AND GMN1=@GMN1" & vbCrLf
sConsulta = sConsulta & "              AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "              AND PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "              AND OFE=@OFE)" & vbCrLf
sConsulta = sConsulta & "IF @CAM IS NULL" & vbCrLf
sConsulta = sConsulta & "  SELECT @CAM =  EQUIV / @EQUIVPROC" & vbCrLf
sConsulta = sConsulta & "    FROM MON " & vbCrLf
sConsulta = sConsulta & "   WHERE COD = @MON" & vbCrLf
sConsulta = sConsulta & "declare C1 CURSOR LOCAL FOR" & vbCrLf
sConsulta = sConsulta & "SELECT PA.ID, DA.COD, DA.TIPO" & vbCrLf
sConsulta = sConsulta & " FROM PROCE_ATRIB PA" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN DEF_ATRIB DA" & vbCrLf
sConsulta = sConsulta & "               ON PA.ATRIB = DA.ID" & vbCrLf
sConsulta = sConsulta & "WHERE PA.ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "  AND PA.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "  AND PA.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "  AND ISNULL(PA.GRUPO,'#') = CASE WHEN PA.GRUPO IS NULL THEN ISNULL(PA.GRUPO,'#') ELSE @GRUPO END" & vbCrLf
sConsulta = sConsulta & "  AND PA.AMBITO = 3" & vbCrLf
sConsulta = sConsulta & "  AND PA.INTERNO = CASE WHEN @INTERNO IS NULL THEN PA.INTERNO ELSE @INTERNO END" & vbCrLf
sConsulta = sConsulta & "ORDER BY DA.COD" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE #ATRIBS (ATANYO int NULL ," & vbCrLf
If gCollation = "" Then
    sConsulta = sConsulta & "   ATGMN1 varchar (50) NULL ," & vbCrLf
Else
    sConsulta = sConsulta & "   ATGMN1 varchar (50) COLLATE " & gCollation & " NULL ," & vbCrLf
End If
sConsulta = sConsulta & "   ATPROCE int NULL ," & vbCrLf
sConsulta = sConsulta & "   ATITEM int NULL " & vbCrLf
sConsulta = sConsulta & ") " & vbCrLf
sConsulta = sConsulta & "SET @INSERT = 'INSERT INTO #ATRIBS (ATANYO, ATGMN1, ATPROCE, ATITEM, '" & vbCrLf
sConsulta = sConsulta & "SET @SQL='SELECT ANYO, GMN1,  PROCE, ITEM, '" & vbCrLf
sConsulta = sConsulta & "set @ALTER = 'ALTER TABLE #ATRIBS ADD '" & vbCrLf
sConsulta = sConsulta & "OPEN C1" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C1 INTO @IDPROCEATRIB, @CODATRIB, @TIPOATRIB" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS = 0" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    " & vbCrLf
sConsulta = sConsulta & "    SET @INSERT = @INSERT + '[AT_' + @CODATRIB + '],'" & vbCrLf
sConsulta = sConsulta & "    IF @TIPOATRIB = 1 " & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "        SET @ALTER = @ALTER  + '[AT_' + @CODATRIB + '] VARCHAR(800), '" & vbCrLf
sConsulta = sConsulta & "        SET @SQL = @SQL + 'MAX(case IOA.ATRIB_ID when ' + cast(@IDPROCEATRIB as varchar(100)) + ' THEN VALOR_TEXT else NULL end) as [' + @CODATRIB + '],'" & vbCrLf
sConsulta = sConsulta & "      END " & vbCrLf
sConsulta = sConsulta & "    ELSE IF @TIPOATRIB = 2" & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "        SET @ALTER = @ALTER  + '[AT_' + @CODATRIB + '] FLOAT, '" & vbCrLf
sConsulta = sConsulta & "        SET @SQL = @SQL + 'MAX(case IOA.ATRIB_ID when ' + cast(@IDPROCEATRIB as varchar(100)) + ' THEN VALOR_NUM else NULL end) as [' + @CODATRIB + '],'" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "    ELSE IF @TIPOATRIB = 3" & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "        SET @ALTER = @ALTER  + '[AT_' + @CODATRIB + '] DATETIME, '" & vbCrLf
sConsulta = sConsulta & "        SET @SQL = @SQL + 'MAX(case IOA.ATRIB_ID when ' + cast(@IDPROCEATRIB as varchar(100)) + ' THEN VALOR_FEC else NULL end) as [' + @CODATRIB + '],'" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "    ELSE " & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "        SET @ALTER = @ALTER + '[AT_' + @CODATRIB + '] TINYINT, '" & vbCrLf
sConsulta = sConsulta & "        SET @SQL = @SQL + 'MAX(case IOA.ATRIB_ID when ' + cast(@IDPROCEATRIB as varchar(100)) + ' THEN VALOR_BOOL else NULL end) as [' + @CODATRIB + '],'" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "    FETCH NEXT FROM C1 INTO @IDPROCEATRIB, @CODATRIB, @TIPOATRIB" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "if @INSERT != 'INSERT INTO #ATRIBS (ATANYO, ATGMN1, ATPROCE, ATITEM, '" & vbCrLf
sConsulta = sConsulta & "  begin" & vbCrLf
sConsulta = sConsulta & "    set @SQL = LEFT(@SQL,LEN(@SQL)-1)" & vbCrLf
sConsulta = sConsulta & "    set @INSERT = LEFT(@INSERT,LEN(@INSERT)-1)" & vbCrLf
sConsulta = sConsulta & "    set @ALTER = LEFT(@ALTER,LEN(@ALTER)-1)" & vbCrLf
sConsulta = sConsulta & "    EXEC SP_EXECUTESQL  @ALTER " & vbCrLf
sConsulta = sConsulta & "    SET @SQL = @INSERT + ')" & vbCrLf
sConsulta = sConsulta & "    ' + @SQL + '" & vbCrLf
sConsulta = sConsulta & "      FROM OFE_ITEM_ATRIB IOA" & vbCrLf
sConsulta = sConsulta & "     WHERE IOA.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "       AND IOA.GMN1 = @GMN1  " & vbCrLf
sConsulta = sConsulta & "       AND IOA.PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "       AND IOA.PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "       AND IOA.OFE = @OFE" & vbCrLf
sConsulta = sConsulta & "    GROUP BY ANYO, GMN1,  PROCE, ITEM'" & vbCrLf
sConsulta = sConsulta & "    SET @PARAMDEF = '@ANYO INT, @GMN1 VARCHAR(50), @PROCE INT,@GRUPO VARCHAR(50), @PROVE varchar(50), @OFE smallint'" & vbCrLf
sConsulta = sConsulta & "    EXEC SP_EXECUTESQL  @SQL, @PARAMDEF, @ANYO = @ANYO, @GMN1 = @GMN1, @PROCE = @PROCE, @GRUPO = @GRUPO, @PROVE = @PROVE, @OFE = @OFE" & vbCrLf
sConsulta = sConsulta & "  end" & vbCrLf
sConsulta = sConsulta & "--   Se devuelve siempre en la moneda de la oferta" & vbCrLf
sConsulta = sConsulta & "SELECT I.ANYO, I.GMN1, I.PROCE, I.GRUPO, I.ID, I.GMN2, I.GMN3, I.GMN4, I.ART, I.DESCR + A.DEN AS DESCR, " & vbCrLf
sConsulta = sConsulta & "       I.DEST, I.UNI,UNI.DEN UNIDEN, I.CANT, I.PREC*@CAM as PREC, I.PRES*@CAM as PRES," & vbCrLf
sConsulta = sConsulta & "       I.PAG, PAG.DEN DENPAG, I.FECINI, I.FECFIN, I.ESP, I.CONF, I.EST," & vbCrLf
sConsulta = sConsulta & "       I.OBJ*@CAM AS OBJ , I.FECHAOBJ, " & vbCrLf
sConsulta = sConsulta & "       O.PROVE MINPROVE, P2.DEN MINPROVEDEN, O.PRECIO*@EQUIVPROC*@CAM MINPRECIO," & vbCrLf
sConsulta = sConsulta & "       CASE WHEN @PROVE = O.PROVE THEN 1 ELSE 0 END PROPIA," & vbCrLf
sConsulta = sConsulta & "       CASE  WHEN NUMESP > 0 THEN 1 WHEN NOT I.ESP  IS NULL THEN 1 ELSE 0 END ESPECADJ ," & vbCrLf
sConsulta = sConsulta & "       ITO.PRECIO, ITO.CANTMAX, ITO.ULT, ITO.PRECIO2, ITO.PRECIO3,ITO.USAR,ITO.FECACT," & vbCrLf
sConsulta = sConsulta & "       IA.NUMADJUN, ITO.COMENT1, ITO.COMENT2, ITO.COMENT3, @EQUIVPROC*@CAM CAMBIO, cast(ITO.OBSADJUN as varchar(2000)) OBSADJUN, @EQUIVPROC EQUIVPROC, ATR.*" & vbCrLf
sConsulta = sConsulta & "  FROM ITEM I" & vbCrLf
sConsulta = sConsulta & "        LEFT JOIN ART4 A" & vbCrLf
sConsulta = sConsulta & "               ON I.ART = A.COD " & vbCrLf
sConsulta = sConsulta & "              AND I.GMN4 = A.GMN4 " & vbCrLf
sConsulta = sConsulta & "              AND I.GMN3 = A.GMN3 " & vbCrLf
sConsulta = sConsulta & "              AND I.GMN2 = A.GMN2" & vbCrLf
sConsulta = sConsulta & "              AND I.GMN1 = A.GMN1" & vbCrLf
sConsulta = sConsulta & "        LEFT JOIN ITEM_OFE ITO" & vbCrLf
sConsulta = sConsulta & "               ON I.ANYO = ITO.ANYO" & vbCrLf
sConsulta = sConsulta & "              AND I.GMN1 = ITO.GMN1" & vbCrLf
sConsulta = sConsulta & "              AND I.PROCE = ITO.PROCE" & vbCrLf
sConsulta = sConsulta & "              AND I.ID = ITO.ITEM" & vbCrLf
sConsulta = sConsulta & "              AND @PROVE = ITO.PROVE" & vbCrLf
sConsulta = sConsulta & "              AND @OFE = ITO.NUM" & vbCrLf
sConsulta = sConsulta & "        LEFT JOIN (OFEMIN O " & vbCrLf
sConsulta = sConsulta & "                     INNER JOIN PROVE P2 " & vbCrLf
sConsulta = sConsulta & "                             ON O.PROVE = P2.COD" & vbCrLf
sConsulta = sConsulta & "                   )" & vbCrLf
sConsulta = sConsulta & "               ON I.ANYO = O.ANYO" & vbCrLf
sConsulta = sConsulta & "              AND I.GMN1 = O.GMN1" & vbCrLf
sConsulta = sConsulta & "              AND I.PROCE = O.PROCE" & vbCrLf
sConsulta = sConsulta & "              AND I.ID = O.ITEM" & vbCrLf
sConsulta = sConsulta & "               " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "        LEFT JOIN #ATRIBS ATR" & vbCrLf
sConsulta = sConsulta & "               ON I.ANYO = ATR.ATANYO" & vbCrLf
sConsulta = sConsulta & "              AND I.GMN1 = ATR.ATGMN1" & vbCrLf
sConsulta = sConsulta & "              AND I.PROCE = ATR.ATPROCE" & vbCrLf
sConsulta = sConsulta & "              AND I.ID = ATR.ATITEM        LEFT JOIN (SELECT IE.ANYO, IE.GMN1, IE.PROCE, IE.ITEM, COUNT(IE.ID) NUMESP " & vbCrLf
sConsulta = sConsulta & "                     FROM ITEM_ESP IE" & vbCrLf
sConsulta = sConsulta & "                    WHERE IE.ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "                      AND IE.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "                      AND IE.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "                 GROUP BY IE.ANYO, IE.GMN1, IE.PROCE, IE.ITEM) IE" & vbCrLf
sConsulta = sConsulta & "               ON I.ID = IE.ITEM " & vbCrLf
sConsulta = sConsulta & "              AND I.GMN1 = IE.GMN1" & vbCrLf
sConsulta = sConsulta & "              AND I.PROCE = IE.PROCE" & vbCrLf
sConsulta = sConsulta & "              AND I.ANYO = IE.ANYO " & vbCrLf
sConsulta = sConsulta & "        LEFT JOIN (SELECT IA.ANYO, IA.GMN1, IA.PROCE, IA.ITEM, COUNT(IA.ID) NUMADJUN" & vbCrLf
sConsulta = sConsulta & "                     FROM OFE_ITEM_ADJUN IA" & vbCrLf
sConsulta = sConsulta & "                    WHERE IA.ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "                      AND IA.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "                      AND IA.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "                      AND IA.PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "                      AND IA.OFE = @OFE" & vbCrLf
sConsulta = sConsulta & "                 GROUP BY IA.ANYO, IA.GMN1, IA.PROCE, IA.ITEM) IA" & vbCrLf
sConsulta = sConsulta & "               ON I.ID = IA.ITEM " & vbCrLf
sConsulta = sConsulta & "              AND I.GMN1 = IA.GMN1" & vbCrLf
sConsulta = sConsulta & "              AND I.PROCE = IA.PROCE" & vbCrLf
sConsulta = sConsulta & "              AND I.ANYO = IA.ANYO " & vbCrLf
sConsulta = sConsulta & "        LEFT JOIN PAG " & vbCrLf
sConsulta = sConsulta & "               ON I.PAG = PAG.COD" & vbCrLf
sConsulta = sConsulta & "        LEFT JOIN UNI " & vbCrLf
sConsulta = sConsulta & "               ON I.UNI = UNI.COD" & vbCrLf
sConsulta = sConsulta & "WHERE I.ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "  AND I.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "  AND I.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "  AND I.CONF=1" & vbCrLf
sConsulta = sConsulta & "  AND I.GRUPO=case when @GRUPO is null then i.grupo else @GRUPO END" & vbCrLf
sConsulta = sConsulta & "  AND CASE WHEN I.EST IS NULL THEN -1 ELSE I.EST END = CASE WHEN @PROCEEST=11 THEN CASE WHEN @TODOS=1 THEN I.EST ELSE 0 END ELSE -1 END" & vbCrLf
sConsulta = sConsulta & "ORDER BY I.ART,DESCR" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[RPT_PRES3_PROCE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[RPT_PRES3_PROCE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE RPT_PRES3_PROCE  (@PRES1 VARCHAR(50)) AS" & vbCrLf
sConsulta = sConsulta & "-- Para otro cliente que no es huf estas variables deben ser apr�metros" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRES2 VARCHAR(50),@PRES3 VARCHAR(50),@PRES4 VARCHAR(50),@UON1 VARCHAR(50),@UON2 VARCHAR(50),@UON3 VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @UONS NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @UON3  IS NOT NULL AND @UON3<>''" & vbCrLf
sConsulta = sConsulta & "    SET @UONS = ' AND PN.UON1=''' + @UON1 + ''' AND PN.UON2= ''' + @UON2 + ''' AND PN.UON3=''' + @UON3 + ''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "        IF @UON2  IS NOT NULL AND @UON2<>''" & vbCrLf
sConsulta = sConsulta & "                   SET @UONS = ' AND PN.UON1=''' + @UON1 + ''' AND PN.UON2= ''' + @UON2 + '''  AND UON3 IS NULL'" & vbCrLf
sConsulta = sConsulta & "        ELSE" & vbCrLf
sConsulta = sConsulta & "                IF @UON1  IS NOT NULL AND @UON1<>''" & vbCrLf
sConsulta = sConsulta & "                          SET @UONS = ' AND PN.UON1=''' + @UON1 + ''' AND UON2 IS NULL AND UON3 IS NULL'" & vbCrLf
sConsulta = sConsulta & "                ELSE" & vbCrLf
sConsulta = sConsulta & "                          SET @UONS = ' AND PN.UON1 IS NULL AND PN.UON2 IS NULL AND PN.UON3 IS NULL'" & vbCrLf
sConsulta = sConsulta & "IF @PRES4 <>'' AND @PRES4 IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "          SET @SQL='SELECT DISTINCT PN.PRES1 P1, PN.PRES2 P2, PN.PRES3 P3, PN.COD P4,PROCE.ANYO,PROCE.GMN1,PROCE.COD,PN.DEN PDEN,PROCE.DEN,PROCE.EST,PROCE.FECAPE FROM ITEM_PRESCON3" & vbCrLf
sConsulta = sConsulta & "                       INNER JOIN PROCE ON PROCE.ANYO =ITEM_PRESCON3.ANYO AND PROCE.GMN1=ITEM_PRESCON3.GMN1 AND PROCE.COD=ITEM_PRESCON3.PROCE " & vbCrLf
sConsulta = sConsulta & "                       INNER JOIN PRES3_NIV4 PN ON ITEM_PRESCON3.PRES4 = PN.ID  AND PN.PRES1=''' + @PRES1 + '''  AND PN.PRES2=''' + @PRES2 + '''  AND PN.PRES3=''' + @PRES3 + '''  AND PN.COD=''' + @PRES4 + '''' + @UONS + ' ORDER BY P1,P2,P3,P4,PROCE.ANYO,PROCE.GMN1,PROCE.COD'                        " & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "        IF @PRES3 <>''   AND @PRES3  IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                  SET @SQL='SELECT DISTINCT PN.PRES1 P1, PN.PRES2 P2, PN.COD P3, NULL P4,PROCE.ANYO,PROCE.GMN1,PROCE.COD,PN.DEN PDEN,PROCE.DEN,PROCE.EST,PROCE.FECAPE FROM ITEM_PRESCON3 " & vbCrLf
sConsulta = sConsulta & "                             INNER JOIN PROCE ON PROCE.ANYO =ITEM_PRESCON3.ANYO AND PROCE.GMN1=ITEM_PRESCON3.GMN1 AND PROCE.COD=ITEM_PRESCON3.PROCE " & vbCrLf
sConsulta = sConsulta & "                             INNER JOIN PRES3_NIV3 PN ON ITEM_PRESCON3.PRES3 = PN.ID AND PN.PRES1=''' + @PRES1 + '''  AND PN.PRES2=''' + @PRES2 + '''  AND PN.COD=''' + @PRES3 + '''' + @UONS + ' UNION " & vbCrLf
sConsulta = sConsulta & "                            SELECT DISTINCT PN.PRES1 P1, PN.PRES2 P2, PN.PRES3 P3, PN.COD P4,PROCE.ANYO,PROCE.GMN1,PROCE.COD,PN.DEN PDEN,PROCE.DEN,PROCE.EST,PROCE.FECAPE FROM ITEM_PRESCON3" & vbCrLf
sConsulta = sConsulta & "                             INNER JOIN PROCE ON PROCE.ANYO =ITEM_PRESCON3.ANYO AND PROCE.GMN1=ITEM_PRESCON3.GMN1 AND PROCE.COD=ITEM_PRESCON3.PROCE " & vbCrLf
sConsulta = sConsulta & "                             INNER JOIN PRES3_NIV4 PN ON ITEM_PRESCON3.PRES4 = PN.ID  AND PN.PRES1=''' + @PRES1 + '''  AND PN.PRES2=''' + @PRES2 + '''  AND PN.PRES3=''' + @PRES3 + '''' + @UONS + ' ORDER BY P1,P2,P3,P4,PROCE.ANYO,PROCE.GMN1,PROCE.COD'" & vbCrLf
sConsulta = sConsulta & "        ELSE" & vbCrLf
sConsulta = sConsulta & "                IF @PRES2 <>'' AND @PRES2  IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                       SET @SQL='SELECT DISTINCT PN.PRES1 P1, PN.COD P2,NULL P3 ,NULL P4,PROCE.ANYO,PROCE.GMN1,PROCE.COD,PN.DEN PDEN,PROCE.DEN,PROCE.EST,PROCE.FECAPE FROM ITEM_PRESCON3 " & vbCrLf
sConsulta = sConsulta & "                            INNER JOIN PROCE ON PROCE.ANYO =ITEM_PRESCON3.ANYO AND PROCE.GMN1=ITEM_PRESCON3.GMN1 AND PROCE.COD=ITEM_PRESCON3.PROCE " & vbCrLf
sConsulta = sConsulta & "                            INNER JOIN PRES3_NIV2  PN ON ITEM_PRESCON3.PRES2 = PN.ID AND PN.PRES1=''' + @PRES1 + '''  AND PN.COD=''' + @PRES2 + ''''  + @UONS + ' UNION " & vbCrLf
sConsulta = sConsulta & "                         SELECT DISTINCT PN.PRES1 P1, PN.PRES2 P2, PN.COD P3, NULL P4,PROCE.ANYO,PROCE.GMN1,PROCE.COD,PN.DEN PDEN,PROCE.DEN,PROCE.EST,PROCE.FECAPE FROM ITEM_PRESCON3 " & vbCrLf
sConsulta = sConsulta & "                            INNER JOIN PROCE ON PROCE.ANYO =ITEM_PRESCON3.ANYO AND PROCE.GMN1=ITEM_PRESCON3.GMN1 AND PROCE.COD=ITEM_PRESCON3.PROCE " & vbCrLf
sConsulta = sConsulta & "                            INNER JOIN PRES3_NIV3 PN ON ITEM_PRESCON3.PRES3 = PN.ID AND PN.PRES1=''' + @PRES1 + '''  AND PN.PRES2=''' + @PRES2 + ''''  + @UONS + ' UNION " & vbCrLf
sConsulta = sConsulta & "                         SELECT DISTINCT PN.PRES1 P1, PN.PRES2 P2, PN.PRES3 P3, PN.COD P4,PROCE.ANYO,PROCE.GMN1,PROCE.COD,PN.DEN PDEN,PROCE.DEN,PROCE.EST,PROCE.FECAPE FROM ITEM_PRESCON3" & vbCrLf
sConsulta = sConsulta & "                            INNER JOIN PROCE ON PROCE.ANYO =ITEM_PRESCON3.ANYO AND PROCE.GMN1=ITEM_PRESCON3.GMN1 AND PROCE.COD=ITEM_PRESCON3.PROCE " & vbCrLf
sConsulta = sConsulta & "                            INNER JOIN PRES3_NIV4 PN ON ITEM_PRESCON3.PRES4 = PN.ID  AND PN.PRES1=''' + @PRES1 + '''  AND PN.PRES2=''' + @PRES2 + ''''  + @UONS + ' ORDER BY P1,P2,P3,P4,PROCE.ANYO,PROCE.GMN1,PROCE.COD'" & vbCrLf
sConsulta = sConsulta & "                ELSE" & vbCrLf
sConsulta = sConsulta & "                      IF @PRES1 <>'' AND @PRES1  IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                       SET @SQL='SELECT DISTINCT PN.COD P1, NULL P2, NULL P3, NULL P4,PROCE.ANYO,PROCE.GMN1,PROCE.COD,PN.DEN PDEN,PROCE.DEN,PROCE.EST,PROCE.FECAPE FROM ITEM_PRESCON3 " & vbCrLf
sConsulta = sConsulta & "                              INNER JOIN PROCE ON PROCE.ANYO =ITEM_PRESCON3.ANYO AND PROCE.GMN1=ITEM_PRESCON3.GMN1 AND PROCE.COD=ITEM_PRESCON3.PROCE " & vbCrLf
sConsulta = sConsulta & "                              INNER JOIN PRES3_NIV1 PN ON ITEM_PRESCON3.PRES1 = PN.ID AND PN.COD=''' + @PRES1 + '''' + @UONS + ' UNION " & vbCrLf
sConsulta = sConsulta & "                         SELECT DISTINCT PN.PRES1 P1, PN.COD P2,NULL P3 ,NULL P4,PROCE.ANYO,PROCE.GMN1,PROCE.COD,PN.DEN PDEN,PROCE.DEN,PROCE.EST,PROCE.FECAPE FROM ITEM_PRESCON3 " & vbCrLf
sConsulta = sConsulta & "                            INNER JOIN PROCE ON PROCE.ANYO =ITEM_PRESCON3.ANYO AND PROCE.GMN1=ITEM_PRESCON3.GMN1 AND PROCE.COD=ITEM_PRESCON3.PROCE " & vbCrLf
sConsulta = sConsulta & "                            INNER JOIN PRES3_NIV2  PN ON ITEM_PRESCON3.PRES2 = PN.ID AND PN.PRES1=''' + @PRES1 + ''''  + @UONS + ' UNION " & vbCrLf
sConsulta = sConsulta & "                         SELECT DISTINCT PN.PRES1 P1, PN.PRES2 P2, PN.COD P3, NULL P4,PROCE.ANYO,PROCE.GMN1,PROCE.COD,PN.DEN PDEN,PROCE.DEN,PROCE.EST,PROCE.FECAPE FROM ITEM_PRESCON3 " & vbCrLf
sConsulta = sConsulta & "                            INNER JOIN PROCE ON PROCE.ANYO =ITEM_PRESCON3.ANYO AND PROCE.GMN1=ITEM_PRESCON3.GMN1 AND PROCE.COD=ITEM_PRESCON3.PROCE " & vbCrLf
sConsulta = sConsulta & "                            INNER JOIN PRES3_NIV3 PN ON ITEM_PRESCON3.PRES3 = PN.ID AND PN.PRES1=''' + @PRES1 + ''''  + @UONS + ' UNION " & vbCrLf
sConsulta = sConsulta & "                         SELECT DISTINCT PN.PRES1 P1, PN.PRES2 P2, PN.PRES3 P3, PN.COD P4,PROCE.ANYO,PROCE.GMN1,PROCE.COD,PN.DEN  PDEN,PROCE.DEN,PROCE.EST,PROCE.FECAPE FROM ITEM_PRESCON3" & vbCrLf
sConsulta = sConsulta & "                            INNER JOIN PROCE ON PROCE.ANYO =ITEM_PRESCON3.ANYO AND PROCE.GMN1=ITEM_PRESCON3.GMN1 AND PROCE.COD=ITEM_PRESCON3.PROCE " & vbCrLf
sConsulta = sConsulta & "                            INNER JOIN PRES3_NIV4 PN ON ITEM_PRESCON3.PRES4 = PN.ID  AND PN.PRES1=''' + @PRES1 + ''''  + @UONS + ' ORDER BY P1,P2,P3,P4,PROCE.ANYO,PROCE.GMN1,PROCE.COD'" & vbCrLf
sConsulta = sConsulta & "                      ELSE" & vbCrLf
sConsulta = sConsulta & "                       SET @SQL='SELECT DISTINCT PN.COD P1, NULL P2, NULL P3, NULL P4,PROCE.ANYO,PROCE.GMN1,PROCE.COD,PN.DEN PDEN,PROCE.DEN,PROCE.EST,PROCE.FECAPE FROM ITEM_PRESCON3 " & vbCrLf
sConsulta = sConsulta & "                              INNER JOIN PROCE ON PROCE.ANYO =ITEM_PRESCON3.ANYO AND PROCE.GMN1=ITEM_PRESCON3.GMN1 AND PROCE.COD=ITEM_PRESCON3.PROCE " & vbCrLf
sConsulta = sConsulta & "                              INNER JOIN PRES3_NIV1 PN ON ITEM_PRESCON3.PRES1 = PN.ID'  + @UONS + ' UNION " & vbCrLf
sConsulta = sConsulta & "                         SELECT DISTINCT PN.PRES1 P1, PN.COD P2,NULL P3 ,NULL P4,PROCE.ANYO,PROCE.GMN1,PROCE.COD,PN.DEN PDEN,PROCE.DEN,PROCE.EST,PROCE.FECAPE FROM ITEM_PRESCON3 " & vbCrLf
sConsulta = sConsulta & "                            INNER JOIN PROCE ON PROCE.ANYO =ITEM_PRESCON3.ANYO AND PROCE.GMN1=ITEM_PRESCON3.GMN1 AND PROCE.COD=ITEM_PRESCON3.PROCE " & vbCrLf
sConsulta = sConsulta & "                            INNER JOIN PRES3_NIV2  PN ON ITEM_PRESCON3.PRES2 = PN.ID'   + @UONS + ' UNION " & vbCrLf
sConsulta = sConsulta & "                         SELECT DISTINCT PN.PRES1 P1, PN.PRES2 P2, PN.COD P3, NULL P4,PROCE.ANYO,PROCE.GMN1,PROCE.COD,PN.DEN PDEN,PROCE.DEN,PROCE.EST,PROCE.FECAPE FROM ITEM_PRESCON3 " & vbCrLf
sConsulta = sConsulta & "                            INNER JOIN PROCE ON PROCE.ANYO =ITEM_PRESCON3.ANYO AND PROCE.GMN1=ITEM_PRESCON3.GMN1 AND PROCE.COD=ITEM_PRESCON3.PROCE " & vbCrLf
sConsulta = sConsulta & "                            INNER JOIN PRES3_NIV3 PN ON ITEM_PRESCON3.PRES3 = PN.ID' + @UONS + ' UNION " & vbCrLf
sConsulta = sConsulta & "                         SELECT DISTINCT PN.PRES1 P1, PN.PRES2 P2, PN.PRES3 P3, PN.COD P4,PROCE.ANYO,PROCE.GMN1,PROCE.COD,PN.DEN  PDEN,PROCE.DEN,PROCE.EST,PROCE.FECAPE FROM ITEM_PRESCON3" & vbCrLf
sConsulta = sConsulta & "                            INNER JOIN PROCE ON PROCE.ANYO =ITEM_PRESCON3.ANYO AND PROCE.GMN1=ITEM_PRESCON3.GMN1 AND PROCE.COD=ITEM_PRESCON3.PROCE " & vbCrLf
sConsulta = sConsulta & "                            INNER JOIN PRES3_NIV4 PN ON ITEM_PRESCON3.PRES4 = PN.ID' + @UONS + ' ORDER BY P1,P2,P3,P4,PROCE.ANYO,PROCE.GMN1,PROCE.COD'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[RPT_PRES4_PROCE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[RPT_PRES4_PROCE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE RPT_PRES4_PROCE  (@PRES1 VARCHAR(50),@PRES2 VARCHAR(50)) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRES3 VARCHAR(50),@PRES4 VARCHAR(50),@UON1 VARCHAR(50),@UON2 VARCHAR(50),@UON3 VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @UONS NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @UON3 <>'' AND @UON3 IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "    SET @UONS = ' AND PN.UON1=''' + @UON1 + ''' AND PN.UON2= ''' + @UON2 + ''' AND PN.UON3=''' + @UON3 + ''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "        IF @UON2 <>'' AND @UON1 IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @UONS = ' AND PN.UON1=''' + @UON1 + ''' AND PN.UON2= ''' + @UON2 + '''  AND UON3 IS NULL'" & vbCrLf
sConsulta = sConsulta & "        ELSE" & vbCrLf
sConsulta = sConsulta & "                IF @UON1 <>'' AND @UON1 IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                          SET @UONS = ' AND PN.UON1=''' + @UON1 + ''' AND UON2 IS NULL AND UON3 IS NULL'" & vbCrLf
sConsulta = sConsulta & "                ELSE" & vbCrLf
sConsulta = sConsulta & "                          SET @UONS = ' AND PN.UON1 IS NULL AND PN.UON2 IS NULL AND PN.UON3 IS NULL'" & vbCrLf
sConsulta = sConsulta & "IF @PRES4 <>''   AND @PRES4 IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "          SET @SQL='SELECT DISTINCT PN.PRES1 P1, PN.PRES2 P2, PN.PRES3 P3, PN.COD P4,PROCE.ANYO,PROCE.GMN1,PROCE.COD,PN.DEN PDEN,PROCE.DEN,PROCE.EST,PROCE.FECAPE FROM ITEM_PRESCON4" & vbCrLf
sConsulta = sConsulta & "                       INNER JOIN PROCE ON PROCE.ANYO =ITEM_PRESCON4.ANYO AND PROCE.GMN1=ITEM_PRESCON4.GMN1 AND PROCE.COD=ITEM_PRESCON4.PROCE " & vbCrLf
sConsulta = sConsulta & "                       INNER JOIN PRES4_NIV4 PN ON ITEM_PRESCON4.PRES4 = PN.ID  AND PN.PRES1=''' + @PRES1 + '''  AND PN.PRES2=''' + @PRES2 + '''  AND PN.PRES3=''' + @PRES3 + '''  AND PN.COD=''' + @PRES4 + '''' + @UONS + ' ORDER BY P1,P2,P3,P4,PROCE.ANYO,PROCE.GMN1,PROCE.COD'                        " & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "        IF @PRES3 <>''   AND @PRES3 IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                  SET @SQL='SELECT DISTINCT PN.PRES1 P1, PN.PRES2 P2, PN.COD P3, NULL P4,PROCE.ANYO,PROCE.GMN1,PROCE.COD,PN.DEN PDEN,PROCE.DEN,PROCE.EST,PROCE.FECAPE FROM ITEM_PRESCON4" & vbCrLf
sConsulta = sConsulta & "                             INNER JOIN PROCE ON PROCE.ANYO =ITEM_PRESCON4.ANYO AND PROCE.GMN1=ITEM_PRESCON4.GMN1 AND PROCE.COD=ITEM_PRESCON4.PROCE " & vbCrLf
sConsulta = sConsulta & "                             INNER JOIN PRES4_NIV3 PN ON ITEM_PRESCON4.PRES3 = PN.ID AND PN.PRES1=''' + @PRES1 + '''  AND PN.PRES2=''' + @PRES2 + '''  AND PN.COD=''' + @PRES3 + '''' + @UONS + ' UNION " & vbCrLf
sConsulta = sConsulta & "                            SELECT DISTINCT PN.PRES1 P1, PN.PRES2 P2, PN.PRES3 P3, PN.COD P4,PROCE.ANYO,PROCE.GMN1,PROCE.COD,PN.DEN PDEN,PROCE.DEN,PROCE.EST,PROCE.FECAPE FROM ITEM_PRESCON4" & vbCrLf
sConsulta = sConsulta & "                             INNER JOIN PROCE ON PROCE.ANYO =ITEM_PRESCON4.ANYO AND PROCE.GMN1=ITEM_PRESCON4.GMN1 AND PROCE.COD=ITEM_PRESCON4.PROCE " & vbCrLf
sConsulta = sConsulta & "                             INNER JOIN PRES4_NIV4 PN ON ITEM_PRESCON4.PRES4 = PN.ID  AND PN.PRES1=''' + @PRES1 + '''  AND PN.PRES2=''' + @PRES2 + '''  AND PN.PRES3=''' + @PRES3 + '''' + @UONS + ' ORDER BY P1,P2,P3,P4,PROCE.ANYO,PROCE.GMN1,PROCE.COD'" & vbCrLf
sConsulta = sConsulta & "        ELSE" & vbCrLf
sConsulta = sConsulta & "                IF @PRES2 <>'' AND @PRES2 IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                       SET @SQL='SELECT DISTINCT PN.PRES1 P1, PN.COD P2,NULL P3 ,NULL P4,PROCE.ANYO,PROCE.GMN1,PROCE.COD,PN.DEN PDEN,PROCE.DEN,PROCE.EST,PROCE.FECAPE FROM ITEM_PRESCON4" & vbCrLf
sConsulta = sConsulta & "                            INNER JOIN PROCE ON PROCE.ANYO =ITEM_PRESCON4.ANYO AND PROCE.GMN1=ITEM_PRESCON4.GMN1 AND PROCE.COD=ITEM_PRESCON4.PROCE " & vbCrLf
sConsulta = sConsulta & "                            INNER JOIN PRES4_NIV2  PN ON ITEM_PRESCON4.PRES2 = PN.ID AND PN.PRES1=''' + @PRES1 + '''  AND PN.COD=''' + @PRES2 + '''' + @UONS + ' UNION " & vbCrLf
sConsulta = sConsulta & "                         SELECT DISTINCT PN.PRES1 P1, PN.PRES2 P2, PN.COD P3, NULL P4,PROCE.ANYO,PROCE.GMN1,PROCE.COD,PN.DEN PDEN,PROCE.DEN,PROCE.EST,PROCE.FECAPE FROM ITEM_PRESCON4" & vbCrLf
sConsulta = sConsulta & "                            INNER JOIN PROCE ON PROCE.ANYO =ITEM_PRESCON4.ANYO AND PROCE.GMN1=ITEM_PRESCON4.GMN1 AND PROCE.COD=ITEM_PRESCON4.PROCE " & vbCrLf
sConsulta = sConsulta & "                            INNER JOIN PRES4_NIV3 PN ON ITEM_PRESCON4.PRES3 = PN.ID AND PN.PRES1=''' + @PRES1 + '''  AND PN.PRES2=''' + @PRES2 + '''' + @UONS + ' UNION " & vbCrLf
sConsulta = sConsulta & "                         SELECT DISTINCT PN.PRES1 P1, PN.PRES2 P2, PN.PRES3 P3, PN.COD P4,PROCE.ANYO,PROCE.GMN1,PROCE.COD,PN.DEN PDEN,PROCE.DEN,PROCE.EST,PROCE.FECAPE FROM ITEM_PRESCON4" & vbCrLf
sConsulta = sConsulta & "                            INNER JOIN PROCE ON PROCE.ANYO =ITEM_PRESCON4.ANYO AND PROCE.GMN1=ITEM_PRESCON4.GMN1 AND PROCE.COD=ITEM_PRESCON4.PROCE " & vbCrLf
sConsulta = sConsulta & "                            INNER JOIN PRES4_NIV4 PN ON ITEM_PRESCON4.PRES4 = PN.ID  AND PN.PRES1=''' + @PRES1 + '''  AND PN.PRES2=''' + @PRES2 + '''' + @UONS + ' ORDER BY P1,P2,P3,P4,PROCE.ANYO,PROCE.GMN1,PROCE.COD'" & vbCrLf
sConsulta = sConsulta & "                ELSE" & vbCrLf
sConsulta = sConsulta & "                IF @PRES1 <>'' AND @PRES1 IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                       SET @SQL='SELECT DISTINCT PN.COD P1, NULL P2, NULL P3, NULL P4,PROCE.ANYO,PROCE.GMN1,PROCE.COD,PN.DEN PDEN,PROCE.DEN,PROCE.EST,PROCE.FECAPE FROM ITEM_PRESCON4" & vbCrLf
sConsulta = sConsulta & "                              INNER JOIN PROCE ON PROCE.ANYO =ITEM_PRESCON4.ANYO AND PROCE.GMN1=ITEM_PRESCON4.GMN1 AND PROCE.COD=ITEM_PRESCON4.PROCE " & vbCrLf
sConsulta = sConsulta & "                              INNER JOIN PRES4_NIV1 PN ON ITEM_PRESCON4.PRES1 = PN.ID AND PN.COD=''' + @PRES1 + '''' + @UONS + ' UNION " & vbCrLf
sConsulta = sConsulta & "                         SELECT DISTINCT PN.PRES1 P1, PN.COD P2,NULL P3 ,NULL P4,PROCE.ANYO,PROCE.GMN1,PROCE.COD,PN.DEN PDEN,PROCE.DEN,PROCE.EST,PROCE.FECAPE FROM ITEM_PRESCON4" & vbCrLf
sConsulta = sConsulta & "                            INNER JOIN PROCE ON PROCE.ANYO =ITEM_PRESCON4.ANYO AND PROCE.GMN1=ITEM_PRESCON4.GMN1 AND PROCE.COD=ITEM_PRESCON4.PROCE " & vbCrLf
sConsulta = sConsulta & "                            INNER JOIN PRES4_NIV2  PN ON ITEM_PRESCON4.PRES2 = PN.ID AND PN.PRES1=''' + @PRES1 + '''' + @UONS + ' UNION " & vbCrLf
sConsulta = sConsulta & "                         SELECT DISTINCT PN.PRES1 P1, PN.PRES2 P2, PN.COD P3, NULL P4,PROCE.ANYO,PROCE.GMN1,PROCE.COD,PN.DEN PDEN,PROCE.DEN,PROCE.EST,PROCE.FECAPE FROM ITEM_PRESCON4" & vbCrLf
sConsulta = sConsulta & "                            INNER JOIN PROCE ON PROCE.ANYO =ITEM_PRESCON4.ANYO AND PROCE.GMN1=ITEM_PRESCON4.GMN1 AND PROCE.COD=ITEM_PRESCON4.PROCE " & vbCrLf
sConsulta = sConsulta & "                            INNER JOIN PRES4_NIV3 PN ON ITEM_PRESCON4.PRES3 = PN.ID AND PN.PRES1=''' + @PRES1 + '''' + @UONS + ' UNION " & vbCrLf
sConsulta = sConsulta & "                         SELECT DISTINCT PN.PRES1 P1, PN.PRES2 P2, PN.PRES3 P3, PN.COD P4,PROCE.ANYO,PROCE.GMN1,PROCE.COD,PN.DEN  PDEN,PROCE.DEN,PROCE.EST,PROCE.FECAPE FROM ITEM_PRESCON4" & vbCrLf
sConsulta = sConsulta & "                            INNER JOIN PROCE ON PROCE.ANYO =ITEM_PRESCON4.ANYO AND PROCE.GMN1=ITEM_PRESCON4.GMN1 AND PROCE.COD=ITEM_PRESCON4.PROCE " & vbCrLf
sConsulta = sConsulta & "                            INNER JOIN PRES4_NIV4 PN ON ITEM_PRESCON4.PRES4 = PN.ID  AND PN.PRES1=''' + @PRES1 + '''' + @UONS + ' ORDER BY P1,P2,P3,P4,PROCE.ANYO,PROCE.GMN1,PROCE.COD'" & vbCrLf
sConsulta = sConsulta & "                ELSE" & vbCrLf
sConsulta = sConsulta & "                       SET @SQL='SELECT DISTINCT PN.COD P1, NULL P2, NULL P3, NULL P4,PROCE.ANYO,PROCE.GMN1,PROCE.COD,PN.DEN PDEN,PROCE.DEN,PROCE.EST,PROCE.FECAPE FROM ITEM_PRESCON4" & vbCrLf
sConsulta = sConsulta & "                              INNER JOIN PROCE ON PROCE.ANYO =ITEM_PRESCON4.ANYO AND PROCE.GMN1=ITEM_PRESCON4.GMN1 AND PROCE.COD=ITEM_PRESCON4.PROCE " & vbCrLf
sConsulta = sConsulta & "                              INNER JOIN PRES4_NIV1 PN ON ITEM_PRESCON4.PRES1 = PN.ID' + @UONS + ' UNION " & vbCrLf
sConsulta = sConsulta & "                         SELECT DISTINCT PN.PRES1 P1, PN.COD P2,NULL P3 ,NULL P4,PROCE.ANYO,PROCE.GMN1,PROCE.COD,PN.DEN PDEN,PROCE.DEN,PROCE.EST,PROCE.FECAPE FROM ITEM_PRESCON4" & vbCrLf
sConsulta = sConsulta & "                            INNER JOIN PROCE ON PROCE.ANYO =ITEM_PRESCON4.ANYO AND PROCE.GMN1=ITEM_PRESCON4.GMN1 AND PROCE.COD=ITEM_PRESCON4.PROCE " & vbCrLf
sConsulta = sConsulta & "                            INNER JOIN PRES4_NIV2  PN ON ITEM_PRESCON4.PRES2 = PN.ID'+ @UONS + ' UNION " & vbCrLf
sConsulta = sConsulta & "                         SELECT DISTINCT PN.PRES1 P1, PN.PRES2 P2, PN.COD P3, NULL P4,PROCE.ANYO,PROCE.GMN1,PROCE.COD,PN.DEN PDEN,PROCE.DEN,PROCE.EST,PROCE.FECAPE FROM ITEM_PRESCON4" & vbCrLf
sConsulta = sConsulta & "                            INNER JOIN PROCE ON PROCE.ANYO =ITEM_PRESCON4.ANYO AND PROCE.GMN1=ITEM_PRESCON4.GMN1 AND PROCE.COD=ITEM_PRESCON4.PROCE " & vbCrLf
sConsulta = sConsulta & "                            INNER JOIN PRES4_NIV3 PN ON ITEM_PRESCON4.PRES3 = PN.ID'  + @UONS + ' UNION " & vbCrLf
sConsulta = sConsulta & "                         SELECT DISTINCT PN.PRES1 P1, PN.PRES2 P2, PN.PRES3 P3, PN.COD P4,PROCE.ANYO,PROCE.GMN1,PROCE.COD,PN.DEN  PDEN,PROCE.DEN,PROCE.EST,PROCE.FECAPE FROM ITEM_PRESCON4" & vbCrLf
sConsulta = sConsulta & "                            INNER JOIN PROCE ON PROCE.ANYO =ITEM_PRESCON4.ANYO AND PROCE.GMN1=ITEM_PRESCON4.GMN1 AND PROCE.COD=ITEM_PRESCON4.PROCE " & vbCrLf
sConsulta = sConsulta & "                            INNER JOIN PRES4_NIV4 PN ON ITEM_PRESCON4.PRES4 = PN.ID' + @UONS + ' ORDER BY P1,P2,P3,P4,PROCE.ANYO,PROCE.GMN1,PROCE.COD'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion2_14_00_06A2_14_00_07() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim bBool As Boolean

On Error GoTo error
    

sConsulta = "BEGIN TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = True
ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

frmProgreso.lblDetalle = "Modificamos REU_PROCE"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_14_Tablas_7


sConsulta = "UPDATE VERSION SET NUM ='2.14.00.07'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "COMMIT TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = False

CodigoDeActualizacion2_14_00_06A2_14_00_07 = True
Exit Function
    
error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_14_00_06A2_14_00_07 = False

End Function


Private Sub V_2_14_Tablas_7()
Dim sConsulta As String


'Esto hay que hacerlo por si acaso, por un bug en SQL Server 7 para mas info leer
'BUG: ALTER COLUMN on Column With Auto-Stats Returns Error Message 1903
sConsulta = "DECLARE @STAT VARCHAR(200)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "if exists(select * from sysindexes where id = object_id(N'[dbo].[REU_PROCE]') and name like '_WA_Sys_COMEN_%')" & vbCrLf
sConsulta = sConsulta & "begin" & vbCrLf
sConsulta = sConsulta & "select @STAT=NAME from sysindexes where id = object_id(N'[dbo].[REU_PROCE]') and name like '_WA_Sys_COMEN_%'" & vbCrLf
sConsulta = sConsulta & "SET @SQL= 'drop statistics [dbo].[REU_PROCE].' + @STAT" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

If frmProgreso.ProgressBar1.Value >= frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

sConsulta = "ALTER TABLE dbo.REU_PROCE ALTER COLUMN" & vbCrLf
sConsulta = sConsulta & "   COMEN varchar(4000) NULL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
End Sub

Public Function CodigoDeActualizacion2_14_00_07A2_14_00_08() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim bBool As Boolean

On Error GoTo error
    

sConsulta = "BEGIN TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = True
ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

frmProgreso.lblDetalle = "Tabla de centros de coste (Ficosa)"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_14_Tablas_8


sConsulta = "UPDATE VERSION SET NUM ='2.14.00.08'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "COMMIT TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = False

CodigoDeActualizacion2_14_00_07A2_14_00_08 = True
Exit Function
    
error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_14_00_07A2_14_00_08 = False

End Function



Private Sub V_2_14_Tablas_8()

Dim sConsulta As String
Dim oRes As rdo.rdoResultset


    sConsulta = "select * from dbo.sysobjects where id = object_id(N'[dbo].[CENTROS_COSTE]') and OBJECTPROPERTY(id, N'IsTable')=1"
    Set oRes = gRDOCon.OpenResultset(sConsulta, rdOpenForwardOnly, rdConcurReadOnly, rdExecDirect)
    If oRes.EOF Then
        sConsulta = ""
        sConsulta = sConsulta & "CREATE TABLE [dbo].[CENTROS_COSTE] (" & vbCrLf
        sConsulta = sConsulta & " [COD] [varchar] (100) NOT NULL ," & vbCrLf
        sConsulta = sConsulta & " [DEN] [varchar] (100) NULL " & vbCrLf
        sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
        ExecuteSQL gRDOCon, sConsulta
        sConsulta = ""
        sConsulta = sConsulta & " " & vbCrLf
        sConsulta = sConsulta & "ALTER TABLE [dbo].[CENTROS_COSTE] WITH NOCHECK ADD " & vbCrLf
        sConsulta = sConsulta & " CONSTRAINT [PK_CENTROS_COSTE] PRIMARY KEY  CLUSTERED " & vbCrLf
        sConsulta = sConsulta & " (" & vbCrLf
        sConsulta = sConsulta & "  [COD]" & vbCrLf
        sConsulta = sConsulta & " )  ON [PRIMARY] " & vbCrLf
        ExecuteSQL gRDOCon, sConsulta
        
        sConsulta = "INSERT INTO CENTROS_COSTE VALUES('100','Almac�n')" & vbCrLf
        ExecuteSQL gRDOCon, sConsulta
        
        sConsulta = "INSERT INTO CENTROS_COSTE VALUES('103','Aprovisionamiento')" & vbCrLf
        ExecuteSQL gRDOCon, sConsulta
        
        sConsulta = "INSERT INTO CENTROS_COSTE VALUES('105','Departamento financiero')" & vbCrLf
        ExecuteSQL gRDOCon, sConsulta
        
        sConsulta = "INSERT INTO CENTROS_COSTE VALUES('106','�rea de sistemas')" & vbCrLf
        ExecuteSQL gRDOCon, sConsulta
        
        sConsulta = "INSERT INTO CENTROS_COSTE VALUES('107','Comercial')" & vbCrLf
        ExecuteSQL gRDOCon, sConsulta
        
        sConsulta = "INSERT INTO CENTROS_COSTE VALUES('108','Compras')" & vbCrLf
        ExecuteSQL gRDOCon, sConsulta
        
        sConsulta = "INSERT INTO CENTROS_COSTE VALUES('109','Control de Calidad')" & vbCrLf
        ExecuteSQL gRDOCon, sConsulta
        
        sConsulta = "INSERT INTO CENTROS_COSTE VALUES('110','Direcci�n general')" & vbCrLf
        ExecuteSQL gRDOCon, sConsulta
        
        sConsulta = "INSERT INTO CENTROS_COSTE VALUES('111','Administraci�n')" & vbCrLf
        ExecuteSQL gRDOCon, sConsulta
        
        sConsulta = "INSERT INTO CENTROS_COSTE VALUES('112','Ingenier�a de procesos')" & vbCrLf
        ExecuteSQL gRDOCon, sConsulta
        
        sConsulta = "INSERT INTO CENTROS_COSTE VALUES('114','Log�stica')" & vbCrLf
        ExecuteSQL gRDOCon, sConsulta
        
        sConsulta = "INSERT INTO CENTROS_COSTE VALUES('115','Mantenimiento')" & vbCrLf
        ExecuteSQL gRDOCon, sConsulta
        
        sConsulta = "INSERT INTO CENTROS_COSTE VALUES('116','Oficina t�cnica')" & vbCrLf
        ExecuteSQL gRDOCon, sConsulta
        
        sConsulta = "INSERT INTO CENTROS_COSTE VALUES('117','Recursos humanos')" & vbCrLf
        ExecuteSQL gRDOCon, sConsulta
        
        sConsulta = "INSERT INTO CENTROS_COSTE VALUES('118','Servicio m�dico')" & vbCrLf
        ExecuteSQL gRDOCon, sConsulta
        
        sConsulta = "INSERT INTO CENTROS_COSTE VALUES('119','Trabajos exteriores')" & vbCrLf
        ExecuteSQL gRDOCon, sConsulta
        
        sConsulta = "INSERT INTO CENTROS_COSTE VALUES('121','Producci�n')" & vbCrLf
        ExecuteSQL gRDOCon, sConsulta
        
        sConsulta = "INSERT INTO CENTROS_COSTE VALUES('122','Laboratorio')" & vbCrLf
        ExecuteSQL gRDOCon, sConsulta
        
        sConsulta = "INSERT INTO CENTROS_COSTE VALUES('123','Desplazados')" & vbCrLf
        ExecuteSQL gRDOCon, sConsulta
        
        sConsulta = "INSERT INTO CENTROS_COSTE VALUES('124','Taller de moldes')" & vbCrLf
        ExecuteSQL gRDOCon, sConsulta
        
        sConsulta = "INSERT INTO CENTROS_COSTE VALUES('220','Inyecci�n de pl�sticos')" & vbCrLf
        ExecuteSQL gRDOCon, sConsulta
        
        sConsulta = "INSERT INTO CENTROS_COSTE VALUES('230','Inyecci�n de zamak')" & vbCrLf
        ExecuteSQL gRDOCon, sConsulta
        
        sConsulta = "INSERT INTO CENTROS_COSTE VALUES('270','Inyecci�n de aluminio')" & vbCrLf
        ExecuteSQL gRDOCon, sConsulta
        
        sConsulta = "INSERT INTO CENTROS_COSTE VALUES('280','Pintura')" & vbCrLf
        ExecuteSQL gRDOCon, sConsulta
        
        sConsulta = "INSERT INTO CENTROS_COSTE VALUES('290','Montaje')" & vbCrLf
        ExecuteSQL gRDOCon, sConsulta
        
        sConsulta = "INSERT INTO CENTROS_COSTE VALUES('590','Embalaje')" & vbCrLf
        ExecuteSQL gRDOCon, sConsulta
    End If
    oRes.Close
    Set oRes = Nothing



End Sub


Public Function CodigoDeActualizacion2_14_00_08A2_14_00_09() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim bBool As Boolean

On Error GoTo error
    

sConsulta = "BEGIN TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = True
ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

frmProgreso.lblDetalle = "Actualizamos tablas, FECALTA"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_14_Tablas_9

frmProgreso.lblDetalle = "Actualizamos triggers, FECALTA"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_14_Triggers_9

frmProgreso.lblDetalle = "Actualizamos storeds"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_14_Storeds_9


sConsulta = "UPDATE VERSION SET NUM ='2.14.00.09'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "COMMIT TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = False

CodigoDeActualizacion2_14_00_08A2_14_00_09 = True
Exit Function
    
error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_14_00_08A2_14_00_09 = False

End Function

Private Sub V_2_14_Tablas_9()
Dim sConsulta As String

sConsulta = "ALTER TABLE dbo.PROVE ADD FECALTA datetime NULL"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE dbo.USU ADD FECALTA datetime NULL"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE dbo.PROVE_ESP ADD FECALTA datetime NULL"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE dbo.PROVE_ART4_ADJUN ADD FECALTA datetime NULL"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE dbo.PROCE_ESP ADD FECALTA datetime NULL"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE dbo.PROCE_GRUPO_ESP ADD FECALTA datetime NULL"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE dbo.ITEM_ESP ADD FECALTA datetime NULL"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE dbo.ADJUN ADD FECALTA datetime NULL"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE dbo.CATALOG_LIN_ESP ADD FECALTA datetime NULL"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE dbo.CATALOG_LIN ADD FECHA_PUB datetime NULL"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE dbo.PROVE_ESP_PORTAL ADD DATASIZE integer NULL"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE dbo.ART4_ESP ADD DATASIZE integer NULL"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE dbo.DEF_ATRIB ADD DESCR NVARCHAR(800) NULL"
ExecuteSQL gRDOCon, sConsulta

End Sub

Private Sub V_2_14_Triggers_9()
Dim sConsulta As String

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PROVE_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PROVE_TG_INS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TRIGGER PROVE_TG_INS ON [dbo].[PROVE] " & vbCrLf
sConsulta = sConsulta & "FOR INSERT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_Prove_Inserta CURSOR FOR SELECT COD FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_Prove_Inserta" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_Prove_Inserta INTO @COD" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE SET FECALTA=GETDATE() WHERE COD=@COD" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_Prove_Inserta INTO @COD" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_Prove_Inserta" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_Prove_Inserta" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USU_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[USU_TG_INS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE   TRIGGER USU_TG_INS ON dbo.USU" & vbCrLf
sConsulta = sConsulta & "FOR INSERT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDIOMA VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "SELECT @IDIOMA = IDIOMA FROM PARGEN_DEF" & vbCrLf
sConsulta = sConsulta & "update USU" & vbCrLf
sConsulta = sConsulta & "   SET IDIOMA = @IDIOMA," & vbCrLf
sConsulta = sConsulta & "   FECALTA=GETDATE()" & vbCrLf
sConsulta = sConsulta & "  FROM usu u inner join inserted i on u.cod = i.cod" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PROVE_ESP_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PROVE_ESP_TG_INS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TRIGGER PROVE_ESP_TG_INS ON [dbo].[PROVE_ESP] " & vbCrLf
sConsulta = sConsulta & "FOR INSERT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVE AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @ESP AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_PROVE_ESP_Insertar CURSOR FOR SELECT PROVE,ID FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_PROVE_ESP_Insertar" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROVE_ESP_Insertar INTO @PROVE,@ESP" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ESP SET FECALTA=GETDATE() WHERE PROVE=@PROVE AND ID=@ESP" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROVE_ESP_Insertar INTO @PROVE,@ESP" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_PROVE_ESP_Insertar" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_PROVE_ESP_Insertar" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PROVE_ART4_ADJUN_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PROVE_ART4_ADJUN_TG_INS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TRIGGER PROVE_ART4_ADJUN_TG_INS ON [dbo].[PROVE_ART4_ADJUN] " & vbCrLf
sConsulta = sConsulta & "FOR INSERT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVE AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN1 AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN2 AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN3 AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN4 AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @ART AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @ESP AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_PROVE_ESP_Insertar CURSOR LOCAL FOR SELECT PROVE,GMN1,GMN2,GMN3,GMN4,ART,ID FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_PROVE_ESP_Insertar" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROVE_ESP_Insertar INTO @PROVE,@GMN1,@GMN2,@GMN3,@GMN4,@ART,@ESP" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART4_ADJUN SET FECALTA=GETDATE() WHERE PROVE=@PROVE AND GMN1=@GMN1 AND GMN2=@GMN2 AND" & vbCrLf
sConsulta = sConsulta & "          GMN3=@GMN3 AND GMN4=@GMN4 AND ART=@ART AND  ID=@ESP" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROVE_ESP_Insertar INTO @PROVE,@GMN1,@GMN2,@GMN3,@GMN4,@ART,@ESP" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_PROVE_ESP_Insertar" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_PROVE_ESP_Insertar" & vbCrLf
sConsulta = sConsulta & ""
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PROCE_ESP_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PROCE_ESP_TG_INS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TRIGGER PROCE_ESP_TG_INS ON [dbo].[PROCE_ESP] " & vbCrLf
sConsulta = sConsulta & "FOR INSERT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN1 AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCE AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @ESP AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_PROCE_ESP_Insertar CURSOR FOR SELECT ANYO,GMN1,PROCE,ID FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_PROCE_ESP_Insertar" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_ESP_Insertar INTO @ANYO,@GMN1,@PROCE,@ESP" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_ESP SET FECALTA=GETDATE() WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ID=@ESP" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_ESP_Insertar INTO @ANYO,@GMN1,@PROCE,@ESP" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_PROCE_ESP_Insertar" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_PROCE_ESP_Insertar" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PROCE_GRUPO_ESP_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PROCE_GRUPO_ESP_TG_INS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TRIGGER PROCE_GRUPO_ESP_TG_INS ON dbo.PROCE_GRUPO_ESP" & vbCrLf
sConsulta = sConsulta & "FOR INSERT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO INT,@GMN1 VARCHAR(50),@PROCE SMALLINT,@GRUPO VARCHAR(50),@ID SMALLINT" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_PROCE_GRUPO_ESP_Insertar CURSOR FOR SELECT ANYO,GMN1,PROCE,GRUPO,ID FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_PROCE_GRUPO_ESP_Insertar" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_GRUPO_ESP_Insertar INTO @ANYO,@GMN1,@PROCE,@GRUPO,@ID" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GRUPO_ESP SET FECALTA=GETDATE() WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO AND ID=@ID" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_GRUPO_ESP_Insertar INTO @ANYO,@GMN1,@PROCE,@GRUPO,@ID" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_PROCE_GRUPO_ESP_Insertar" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_PROCE_GRUPO_ESP_Insertar" & vbCrLf
sConsulta = sConsulta & ""
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ITEM_ESP_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[ITEM_ESP_TG_INS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TRIGGER ITEM_ESP_TG_INS ON dbo.ITEM_ESP" & vbCrLf
sConsulta = sConsulta & "FOR INSERT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN1 AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCE AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @ITEM AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @ESP AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_ITEM_ESP_Insertar CURSOR FOR SELECT ANYO,GMN1,PROCE,ITEM,ID FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_ITEM_ESP_Insertar" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_ITEM_ESP_Insertar INTO @ANYO,@GMN1,@PROCE,@ITEM,@ESP" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_ESP SET FECALTA=GETDATE() WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM AND ID=@ESP" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_ITEM_ESP_Insertar INTO @ANYO,@GMN1,@PROCE,@ITEM,@ESP" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_ITEM_ESP_Insertar" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_ITEM_ESP_Insertar" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & ""
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CATALOG_LIN_ESP_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[CATALOG_LIN_ESP_TG_INS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TRIGGER CATALOG_LIN_ESP_TG_INS ON dbo.CATALOG_LIN_ESP" & vbCrLf
sConsulta = sConsulta & "FOR INSERT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @LIN INT,@ID AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_LIN_ESP_Insertar CURSOR LOCAL FOR SELECT LINEA,ID FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_LIN_ESP_Insertar" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_LIN_ESP_Insertar INTO @LIN,@ID" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "UPDATE CATALOG_LIN_ESP SET FECALTA=GETDATE() WHERE LINEA=@LIN AND ID=@ID" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_LIN_ESP_Insertar INTO @LIN,@ID" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_LIN_ESP_Insertar" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_LIN_ESP_Insertar" & vbCrLf
sConsulta = sConsulta & ""
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ADJUN_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[ADJUN_TG_INS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TRIGGER ADJUN_TG_INS ON dbo.ADJUN" & vbCrLf
sConsulta = sConsulta & "FOR INSERT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID SMALLINT" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_ADJUN_Ins CURSOR FOR SELECT ID FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_ADJUN_Ins" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_ADJUN_Ins INTO @ID" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "UPDATE ADJUN SET FECALTA=GETDATE() WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_ADJUN_Ins INTO @ID" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_ADJUN_Ins" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_ADJUN_Ins" & vbCrLf
sConsulta = sConsulta & ""
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CATALOG_LIN_TG_UPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[CATALOG_LIN_TG_UPD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE  TRIGGER CATALOG_LIN_TG_UPD ON dbo.CATALOG_LIN" & vbCrLf
sConsulta = sConsulta & "FOR UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @PUB INTEGER" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF UPDATE (PUB)" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  DECLARE curTG_CATALOGLIN_Upd CURSOR FOR SELECT ID,PUB FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "  OPEN curTG_CATALOGLIN_Upd" & vbCrLf
sConsulta = sConsulta & "  FETCH NEXT FROM curTG_CATALOGLIN_Upd INTO @ID,@PUB" & vbCrLf
sConsulta = sConsulta & "  WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "      IF @PUB=1" & vbCrLf
sConsulta = sConsulta & "       UPDATE CATALOG_LIN SET FECHA_PUB=GETDATE() WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "    " & vbCrLf
sConsulta = sConsulta & "  FETCH NEXT FROM curTG_CATALOGLIN_Upd INTO @ID,@PUB" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "  Close curTG_CATALOGLIN_Upd" & vbCrLf
sConsulta = sConsulta & "  DEALLOCATE curTG_CATALOGLIN_Upd" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Private Sub V_2_14_Storeds_9()
Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ANYA_PROVE_ESP]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_ANYA_PROVE_ESP]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_ANYA_PROVE_ESP (@PROVE VARCHAR(100),@ID SMALLINT,@NOM VARCHAR(300),@COM VARCHAR(500),@FECHA DATETIME,@DATASIZE INT)  AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "INSERT INTO PROVE_ESP_PORTAL (PROVE,ID,NOM,COM,FECHA,DATASIZE) VALUES (@PROVE,@ID,@NOM,@COM,@FECHA,@DATASIZE)" & vbCrLf
sConsulta = sConsulta & "Return" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_INSERTAR_ADJUNTO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_INSERTAR_ADJUNTO]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_INSERTAR_ADJUNTO (@NOM VARCHAR(300),@COM VARCHAR(500),@FECACT DATETIME, " & vbCrLf
sConsulta = sConsulta & "                                @COD_PROVE_CIA VARCHAR(200), @ESPEC INT,@DATASIZE INT) AS" & vbCrLf
sConsulta = sConsulta & "/**************************************************************************************************/" & vbCrLf
sConsulta = sConsulta & "/******       Descripci�n : Inserta un archivo adjunto insertado desde el PS.                ******/" & vbCrLf
sConsulta = sConsulta & "/******                                                                 ******/" & vbCrLf
sConsulta = sConsulta & "/******       Par�metros :   @NOM >> El nombre del fichero adjunto                           ******/" & vbCrLf
sConsulta = sConsulta & "/******                              @COM >> Comentarios sobre el fichero adjunto            ******/" & vbCrLf
sConsulta = sConsulta & "/******                              @FECACT >> Fecha de actualizaci�n                       ******/" & vbCrLf
sConsulta = sConsulta & "/******                              @COD_PROVE_CIA >> Codigo del proveedor en el GS         ******/" & vbCrLf
sConsulta = sConsulta & "/******                              @ESPEC >> El identificador del archivo                  ******/" & vbCrLf
sConsulta = sConsulta & "/******                                                                                         ******/" & vbCrLf
sConsulta = sConsulta & "/**************************************************************************************************/" & vbCrLf
sConsulta = sConsulta & "IF @NOM IS NOT NULL  --Para que no falle al hacer insert  " & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   IF (SELECT COUNT(*) FROM PROVE_ESP_PORTAL WHERE PROVE=@COD_PROVE_CIA AND ID=@ESPEC)=0" & vbCrLf
sConsulta = sConsulta & "      INSERT INTO PROVE_ESP_PORTAL (PROVE,ID,NOM,COM,FECHA,DATASIZE) VALUES (@COD_PROVE_CIA,@ESPEC,@NOM, @COM, @FECACT,@DATASIZE)" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ATR_INSERTAR]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[ATR_INSERTAR]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE ATR_INSERTAR  @TABLA SMALLINT,@COD VARCHAR(50),@DEN VARCHAR(100),@TIPO SMALLINT,@PREFBAJO TINYINT,@INTRO TINYINT , @MINNUM FLOAT,@MINFEC DATETIME,@MAXNUM FLOAT,@MAXFEC DATETIME,@ATRIB INTEGER,@ANYO SMALLINT,@GMN1 VARCHAR(50),@PROCE SMALLINT,@PLANTILLA  VARCHAR(50),@GRUPO VARCHAR(50),@INTERNO  TINYINT,@AMBITO TINYINT,@OBLIG TINYINT,@OP_PREC VARCHAR(2),@APLIC_PREC TINYINT,@DEF_PREC TINYINT=NULL,@MOD_ATRIB TINYINT,@INS INT OUTPUT ,@FECACT DATETIME OUTPUT,@PONDE INT OUTPUT, @DESCR NVARCHAR(4000)=NULL  AS   " & vbCrLf
sConsulta = sConsulta & "DECLARE @ID AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @POND AS integer" & vbCrLf
sConsulta = sConsulta & "DECLARE @OP_POND AS VARCHAR(2)" & vbCrLf
sConsulta = sConsulta & "DECLARE @POND_SI AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @POND_NO AS FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @NUM AS FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ORDEN AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @VALOR_NUM AS FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @VALOR_TEXT AS VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @VALOR_FEC AS DATETIME" & vbCrLf
sConsulta = sConsulta & "DECLARE @VALOR_POND AS FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @DESDE_NUM AS FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @HASTA_NUM AS FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @DESDE_FEC AS DATETIME" & vbCrLf
sConsulta = sConsulta & "DECLARE @HASTA_FEC AS DATETIME" & vbCrLf
sConsulta = sConsulta & "DECLARE @ATRIB_ID AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @ORDENPLANT AS SMALLINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_PLANT AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @INTRO_D AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ORDENATRIB AS INTEGER" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @TABLA=0" & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO DEF_ATRIB(COD,DEN,TIPO,PREFBAJO,INTRO,MAXNUM,MINNUM,MAXFEC,MINFEC,DESCR)" & vbCrLf
sConsulta = sConsulta & "   VALUES(@COD,@DEN,@TIPO,@PREFBAJO,@INTRO,@MAXNUM,@MINNUM,@MAXFEC,@MINFEC,@DESCR) " & vbCrLf
sConsulta = sConsulta & "   SELECT @INS=ID,@PONDE=POND,@FECACT=FECACT FROM DEF_ATRIB WHERE ID=(SELECT MAX(ID)  FROM DEF_ATRIB )" & vbCrLf
sConsulta = sConsulta & "    END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "     IF @TABLA = 1" & vbCrLf
sConsulta = sConsulta & "             BEGIN" & vbCrLf
sConsulta = sConsulta & "              IF @MOD_ATRIB=1" & vbCrLf
sConsulta = sConsulta & "                  UPDATE DEF_ATRIB SET PREFBAJO=@PREFBAJO,INTRO=@INTRO,MAXNUM=@MAXNUM" & vbCrLf
sConsulta = sConsulta & "                                      ,MINNUM=@MINNUM,MAXFEC=CONVERT(VARCHAR(10),@MAXFEC,111),MINFEC=CONVERT(VARCHAR(10),@MINFEC,111) , DESCR = @DESCR" & vbCrLf
sConsulta = sConsulta & "                                       WHERE ID=@ATRIB" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "              SELECT @INTRO_D=INTRO,@POND=POND,@OP_POND=FORMULA,@NUM=NUM,@POND_SI=POND_SI,@POND_NO=POND_NO" & vbCrLf
sConsulta = sConsulta & "                                FROM DEF_ATRIB WHERE ID=@ATRIB" & vbCrLf
sConsulta = sConsulta & "                         IF @INTRO_D<>@INTRO" & vbCrLf
sConsulta = sConsulta & "                                 IF @INTRO=0 AND @POND=2" & vbCrLf
sConsulta = sConsulta & "                                      SET @POND=0" & vbCrLf
sConsulta = sConsulta & "            IF @GRUPO IS NULL " & vbCrLf
sConsulta = sConsulta & "               SELECT @ORDENATRIB=MAX(ORDEN_ATRIB) FROM PROCE_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO IS NULL" & vbCrLf
sConsulta = sConsulta & "            ELSE" & vbCrLf
sConsulta = sConsulta & "               SELECT @ORDENATRIB=MAX(ORDEN_ATRIB) FROM PROCE_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO " & vbCrLf
sConsulta = sConsulta & "                IF @ORDENATRIB IS NULL " & vbCrLf
sConsulta = sConsulta & "                   SET @ORDENATRIB=1" & vbCrLf
sConsulta = sConsulta & "            ELSE" & vbCrLf
sConsulta = sConsulta & "               SET @ORDENATRIB=@ORDENATRIB +1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                         INSERT INTO PROCE_ATRIB(ANYO,GMN1,PROCE,GRUPO,ATRIB,INTERNO,INTRO,PREFBAJO,MAXNUM,MINNUM" & vbCrLf
sConsulta = sConsulta & "                                          ,MAXFEC,MINFEC,AMBITO,OBLIG,OP_PREC,APLIC_PREC" & vbCrLf
sConsulta = sConsulta & "                                         ,DEF_PREC,POND,OP_POND,NUM_POND,POND_SI,POND_NO,ORDEN_ATRIB)" & vbCrLf
sConsulta = sConsulta & "                             VALUES(@ANYO,@GMN1,@PROCE,@GRUPO,@ATRIB,@INTERNO,@INTRO,@PREFBAJO,@MAXNUM  ,@MINNUM" & vbCrLf
sConsulta = sConsulta & "                                        ,CONVERT(VARCHAR(10),@MAXFEC,111),CONVERT(VARCHAR(10),@MINFEC,111),@AMBITO,@OBLIG,@OP_PREC ,@APLIC_PREC" & vbCrLf
sConsulta = sConsulta & "                                        ,@DEF_PREC,@POND,@OP_POND,@NUM,@POND_SI,@POND_NO,@ORDENATRIB)" & vbCrLf
sConsulta = sConsulta & "                        SELECT @INS=ID,@FECACT=FECACT FROM PROCE_ATRIB WHERE ID=(SELECT MAX(ID)  FROM PROCE_ATRIB )" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "                         IF @INTRO = 1 " & vbCrLf
sConsulta = sConsulta & "                                 INSERT INTO PROCE_ATRIB_LISTA (ATRIB_ID, ORDEN, VALOR_NUM,VALOR_TEXT,VALOR_FEC,VALOR_POND) " & vbCrLf
sConsulta = sConsulta & "                                     SELECT @INS, ORDEN, VALOR_NUM,VALOR_TEXT,VALOR_FEC,VALOR_POND FROM DEF_ATRIB_LISTA WHERE ATRIB=@ATRIB" & vbCrLf
sConsulta = sConsulta & "                         INSERT INTO PROCE_ATRIB_POND (ATRIB_ID, ID, DESDE_NUM,HASTA_NUM,DESDE_FEC,HASTA_FEC,VALOR_POND) " & vbCrLf
sConsulta = sConsulta & "                                     SELECT @INS, ID, DESDE_NUM,HASTA_NUM,DESDE_FEC,HASTA_FEC,VALOR_POND FROM DEF_ATRIB_POND WHERE ATRIB=@ATRIB" & vbCrLf
sConsulta = sConsulta & "                            " & vbCrLf
sConsulta = sConsulta & "                         SET @PONDE =@POND          " & vbCrLf
sConsulta = sConsulta & "             END  " & vbCrLf
sConsulta = sConsulta & "      ELSE" & vbCrLf
sConsulta = sConsulta & "                 IF @TABLA=2" & vbCrLf
sConsulta = sConsulta & "                       BEGIN" & vbCrLf
sConsulta = sConsulta & "                            IF @MOD_ATRIB=1" & vbCrLf
sConsulta = sConsulta & "                                       UPDATE DEF_ATRIB SET PREFBAJO=@PREFBAJO,INTRO=@INTRO,MAXNUM=@MAXNUM " & vbCrLf
sConsulta = sConsulta & "                                        ,MINNUM=@MINNUM,MAXFEC=CONVERT(VARCHAR(10),@MAXFEC,111),MINFEC=CONVERT(VARCHAR(10)" & vbCrLf
sConsulta = sConsulta & "                                        ,@MINFEC,111), DESCR = @DESCR WHERE ID=@ATRIB" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "                                        SELECT @INTRO_D=INTRO,@POND=POND,@OP_POND=FORMULA,@NUM=NUM,@POND_SI=POND_SI,@POND_NO=POND_NO" & vbCrLf
sConsulta = sConsulta & "                                            FROM DEF_ATRIB WHERE ID=@ATRIB" & vbCrLf
sConsulta = sConsulta & "                                        IF @INTRO_D<>@INTRO" & vbCrLf
sConsulta = sConsulta & "                                              IF @INTRO=0 AND @POND=2" & vbCrLf
sConsulta = sConsulta & "                                                    SET @POND=0" & vbCrLf
sConsulta = sConsulta & "                   IF @GRUPO IS NULL " & vbCrLf
sConsulta = sConsulta & "                        SELECT @ORDENATRIB=MAX(ORDEN_ATRIB) FROM PLANTILLA_ATRIB WHERE PLANT=@PLANTILLA AND GRUPO IS NULL" & vbCrLf
sConsulta = sConsulta & "                   ELSE" & vbCrLf
sConsulta = sConsulta & "                        SELECT @ORDENATRIB=MAX(ORDEN_ATRIB) FROM PLANTILLA_ATRIB WHERE PLANT=@PLANTILLA AND GRUPO=@GRUPO " & vbCrLf
sConsulta = sConsulta & "                   IF @ORDENATRIB IS NULL " & vbCrLf
sConsulta = sConsulta & "                        SET @ORDENATRIB=1" & vbCrLf
sConsulta = sConsulta & "                   ELSE" & vbCrLf
sConsulta = sConsulta & "                        SET @ORDENATRIB=@ORDENATRIB +1" & vbCrLf
sConsulta = sConsulta & "                                        INSERT INTO PLANTILLA_ATRIB(PLANT,GRUPO,ATRIB,INTERNO,INTRO,PREFBAJO,MAXNUM,MINNUM" & vbCrLf
sConsulta = sConsulta & "                                            ,MAXFEC,MINFEC,AMBITO,OBLIG,OP_PREC,APLIC_PREC" & vbCrLf
sConsulta = sConsulta & "                                            ,DEF_PREC,POND,OP_POND,NUM_POND,POND_SI,POND_NO,ORDEN_ATRIB)" & vbCrLf
sConsulta = sConsulta & "                                        VALUES(@PLANTILLA,@GRUPO,@ATRIB,@INTERNO,@INTRO,@PREFBAJO,@MAXNUM  ,@MINNUM" & vbCrLf
sConsulta = sConsulta & "                                           ,CONVERT(VARCHAR(10),@MAXFEC,111),CONVERT(VARCHAR(10),@MINFEC,111),@AMBITO,@OBLIG,@OP_PREC ,@APLIC_PREC" & vbCrLf
sConsulta = sConsulta & "                                           ,@DEF_PREC,@POND,@OP_POND,@NUM,@POND_SI,@POND_NO,@ORDENATRIB)" & vbCrLf
sConsulta = sConsulta & "                                        SELECT @INS=ID,@FECACT=FECACT FROM PLANTILLA_ATRIB WHERE ID=(SELECT MAX(ID)  FROM PLANTILLA_ATRIB )" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "                                         IF @INTRO = 1 " & vbCrLf
sConsulta = sConsulta & "                                             INSERT INTO PLANTILLA_ATRIB_LISTA (ATRIB_ID, ORDEN, VALOR_NUM,VALOR_TEXT,VALOR_FEC,VALOR_POND) " & vbCrLf
sConsulta = sConsulta & "                                                SELECT @INS, ORDEN, VALOR_NUM,VALOR_TEXT,VALOR_FEC,VALOR_POND FROM DEF_ATRIB_LISTA WHERE ATRIB=@ATRIB" & vbCrLf
sConsulta = sConsulta & "                                        INSERT INTO PLANTILLA_ATRIB_POND (ATRIB_ID, ID, DESDE_NUM,HASTA_NUM,DESDE_FEC,HASTA_FEC,VALOR_POND) " & vbCrLf
sConsulta = sConsulta & "                                                SELECT @INS, ID, DESDE_NUM,HASTA_NUM,DESDE_FEC,HASTA_FEC,VALOR_POND FROM DEF_ATRIB_POND WHERE ATRIB=@ATRIB" & vbCrLf
sConsulta = sConsulta & "                                        SET @PONDE =@POND" & vbCrLf
sConsulta = sConsulta & "                  END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ATR_MODIFICAR]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[ATR_MODIFICAR]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE ATR_MODIFICAR  @TABLA SMALLINT,@ID INTEGER,@DEN VARCHAR(100)=NULL,@TIPO SMALLINT=NULL, @PREFBAJO TINYINT , @INTRO TINYINT, @MINNUM FLOAT=NULL,@MINFEC DATETIME=NULL,@MAXNUM FLOAT=NULL,@MAXFEC DATETIME=NULL,@ATRIB INTEGER=NULL,@INTERNO  TINYINT=NULL,@AMBITO TINYINT=NULL,@OBLIG  TINYINT=NULL,@OP_PREC VARCHAR(2)=NULL,@APLIC_PREC VARCHAR(50)=NULL,@DEF_PREC TINYINT=NULL,@ANYO VARCHAR(50)=NULL,@GMN1 VARCHAR(50)=NULL,@PROCE VARCHAR(50)=NULL,@GRUPO VARCHAR(50)=NULL , @FECACT DATETIME OUTPUT,@ORDEN SMALLINT=NULL OUTPUT,@PLANT INTEGER=NULL , @DESCR NVARCHAR(4000)=NULL AS " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @POND AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @AMBITOOLD AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @COUNT AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @APLI_PREC_V AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @ORDEN_V AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @ATRIB_PREC AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @USAR_PREC AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRIMERO AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @TIPOOLD AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @VISTAS AS TINYINT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @TABLA=0  " & vbCrLf
sConsulta = sConsulta & "     BEGIN --@tabla=0" & vbCrLf
sConsulta = sConsulta & "            SELECT   @POND=POND,@TIPOOLD=TIPO  FROM DEF_ATRIB  WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "            IF @POND = 1 AND @TIPO<> 2 and @TIPO<>3  " & vbCrLf
sConsulta = sConsulta & "                         SET @POND = 0" & vbCrLf
sConsulta = sConsulta & "            ELSE " & vbCrLf
sConsulta = sConsulta & "                          begin" & vbCrLf
sConsulta = sConsulta & "                          IF @POND = 2 AND (@TIPO=4 OR @INTRO=0)" & vbCrLf
sConsulta = sConsulta & "                                       SET @POND= 0" & vbCrLf
sConsulta = sConsulta & "                          ELSE" & vbCrLf
sConsulta = sConsulta & "                                        IF  @POND = 3 AND @TIPO<> 2 " & vbCrLf
sConsulta = sConsulta & "                                                   SET @POND= 0" & vbCrLf
sConsulta = sConsulta & "                                        ELSE" & vbCrLf
sConsulta = sConsulta & "                                                    IF @POND =5 AND @TIPO<>4" & vbCrLf
sConsulta = sConsulta & "                                                            begin" & vbCrLf
sConsulta = sConsulta & "                                                            SET @POND= 0" & vbCrLf
sConsulta = sConsulta & "                                                            end" & vbCrLf
sConsulta = sConsulta & "                           end" & vbCrLf
sConsulta = sConsulta & "            IF @TIPOOLD <> @TIPO " & vbCrLf
sConsulta = sConsulta & "                  begin" & vbCrLf
sConsulta = sConsulta & "                  IF @TIPOOLD=2" & vbCrLf
sConsulta = sConsulta & "                        BEGIN" & vbCrLf
sConsulta = sConsulta & "                        UPDATE PROCE_ATRIB SET OP_PREC=NULL, APLIC_PREC=NULL, DEF_PREC=NULL WHERE ATRIB= @ID" & vbCrLf
sConsulta = sConsulta & "                        UPDATE PLANTILLA_ATRIB SET OP_PREC=NULL, APLIC_PREC=NULL, DEF_PREC=NULL WHERE ATRIB= @ID" & vbCrLf
sConsulta = sConsulta & "                        END " & vbCrLf
sConsulta = sConsulta & "                  end" & vbCrLf
sConsulta = sConsulta & "            UPDATE DEF_ATRIB SET DEN=@DEN,TIPO=@TIPO,PREFBAJO=@PREFBAJO,POND=@POND,INTRO=@INTRO,MAXNUM=CAST(@MAXNUM AS VARCHAR(15)) ,MINNUM=CAST(@MINNUM AS VARCHAR(15)),MAXFEC=CONVERT(VARCHAR(10),@MAXFEC,111),MINFEC=CONVERT(VARCHAR(10),@MINFEC,111) , DESCR=@DESCR WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "            SELECT @FECACT=FECACT FROM DEF_ATRIB WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "      END --@tabla=0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @TABLA=1" & vbCrLf
sConsulta = sConsulta & "    BEGIN -- @tabla=1" & vbCrLf
sConsulta = sConsulta & "       IF @APLIC_PREC IS not  NULL  AND @ORDEN is  NULL " & vbCrLf
sConsulta = sConsulta & "                 BEGIN --@orden=null" & vbCrLf
sConsulta = sConsulta & "                 SELECT @ATRIB_PREC =ATRIB  FROM PROCE_ATRIB WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "                 SELECT @ORDEN=ORDEN FROM PROCE_ATRIB WHERE  ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND APLIC_PREC = @APLIC_PREC AND" & vbCrLf
sConsulta = sConsulta & "                    OP_PREC=@OP_PREC AND ATRIB =@ATRIB_PREC" & vbCrLf
sConsulta = sConsulta & "                      " & vbCrLf
sConsulta = sConsulta & "                 SET @APLI_PREC_V=@APLIC_PREC" & vbCrLf
sConsulta = sConsulta & "                 SET @ORDEN_V= 0" & vbCrLf
sConsulta = sConsulta & "                 SET @PRIMERO=0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                 WHILE @ORDEN_V=0 and @APLI_PREC_V<=3" & vbCrLf
sConsulta = sConsulta & "                         BEGIN -- while" & vbCrLf
sConsulta = sConsulta & "                             SELECT  @ORDEN_V = MAX(ORDEN) FROM PROCE_ATRIB WHERE  ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND APLIC_PREC = @APLI_PREC_V" & vbCrLf
sConsulta = sConsulta & "                             IF @ORDEN_V IS NULL" & vbCrLf
sConsulta = sConsulta & "                               begin --@ORDEN_V =null" & vbCrLf
sConsulta = sConsulta & "                                 IF @APLIC_PREC=3" & vbCrLf
sConsulta = sConsulta & "                                     begin" & vbCrLf
sConsulta = sConsulta & "                                       SET @ORDEN_V=1 " & vbCrLf
sConsulta = sConsulta & "                                       SET @PRIMERO=1" & vbCrLf
sConsulta = sConsulta & "                                     end" & vbCrLf
sConsulta = sConsulta & "                                 ELSE                   " & vbCrLf
sConsulta = sConsulta & "                                   begin" & vbCrLf
sConsulta = sConsulta & "                                      SET @ORDEN_V= 0" & vbCrLf
sConsulta = sConsulta & "                                      SET @APLI_PREC_V=@APLI_PREC_V + 1" & vbCrLf
sConsulta = sConsulta & "                                   end" & vbCrLf
sConsulta = sConsulta & "                               end  --@ORDEN_V =null" & vbCrLf
sConsulta = sConsulta & "                         END -- while" & vbCrLf
sConsulta = sConsulta & "                if @ORDEN_V IS NULL  SET @ORDEN_V=0" & vbCrLf
sConsulta = sConsulta & "                      begin" & vbCrLf
sConsulta = sConsulta & "                      if @PRIMERO=1" & vbCrLf
sConsulta = sConsulta & "                              UPDATE PROCE_ATRIB SET ORDEN=1 WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "                     else" & vbCrLf
sConsulta = sConsulta & "                              UPDATE PROCE_ATRIB SET ORDEN=@ORDEN_V+1 WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "                     end" & vbCrLf
sConsulta = sConsulta & "                UPDATE PROCE_ATRIB SET ORDEN = ORDEN +1 WHERE  APLIC_PREC < @APLIC_PREC AND ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ID<>@ID" & vbCrLf
sConsulta = sConsulta & "               SET @ORDEN=@ORDEN_V +1" & vbCrLf
sConsulta = sConsulta & "              " & vbCrLf
sConsulta = sConsulta & "             END --@orden=null" & vbCrLf
sConsulta = sConsulta & "       SELECT @POND=POND,@AMBITOOLD=AMBITO,@USAR_PREC=USAR_PREC FROM PROCE_ATRIB WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "       IF @INTRO=0 AND @POND=2 SET @POND=0" & vbCrLf
sConsulta = sConsulta & "       IF @DEF_PREC=1 SET @USAR_PREC=1" & vbCrLf
sConsulta = sConsulta & "       UPDATE PROCE_ATRIB SET PREFBAJO=@PREFBAJO,INTRO=@INTRO,MAXNUM=@MAXNUM  ," & vbCrLf
sConsulta = sConsulta & "                  MINNUM=@MINNUM,MAXFEC=CONVERT(VARCHAR(10),@MAXFEC,111)," & vbCrLf
sConsulta = sConsulta & "                  MINFEC=CONVERT(VARCHAR(10),@MINFEC,111),INTERNO=@INTERNO,AMBITO=@AMBITO,OBLIG=@OBLIG," & vbCrLf
sConsulta = sConsulta & "                  OP_PREC=@OP_PREC,APLIC_PREC=@APLIC_PREC,USAR_PREC=@USAR_PREC," & vbCrLf
sConsulta = sConsulta & "                  DEF_PREC=@DEF_PREC,POND=@POND WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "       SELECT @FECACT=FECACT FROM PROCE_ATRIB WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "       IF   @ATRIB IS NOT  NULL" & vbCrLf
sConsulta = sConsulta & "             BEGIN --@atrib not null" & vbCrLf
sConsulta = sConsulta & "                    SELECT @POND=POND FROM DEF_ATRIB WHERE ID=@ATRIB" & vbCrLf
sConsulta = sConsulta & "                    IF @INTRO=0 AND @POND=2 SET @POND=0" & vbCrLf
sConsulta = sConsulta & "                       UPDATE DEF_ATRIB SET PREFBAJO=@PREFBAJO,INTRO=@INTRO,MAXNUM=@MAXNUM ," & vbCrLf
sConsulta = sConsulta & "                           MINNUM=@MINNUM,MAXFEC=CONVERT(VARCHAR(10),@MAXFEC,111)," & vbCrLf
sConsulta = sConsulta & "                           MINFEC=CONVERT(VARCHAR(10),@MINFEC,111),POND=@POND WHERE ID=@ATRIB" & vbCrLf
sConsulta = sConsulta & "             END--@atrib not null" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "       IF @AMBITO<>@AMBITOOLD" & vbCrLf
sConsulta = sConsulta & "               BEGIN --@ambito<>ambitoold" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                     IF @AMBITOOLD<3 " & vbCrLf
sConsulta = sConsulta & "                          begin" & vbCrLf
sConsulta = sConsulta & "                             IF (SELECT count(*) FROM CONF_VISTAS_PROCE_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE  AND ATRIB =@ID AND POS IS NOT NULL AND POS<>0)>0" & vbCrLf
sConsulta = sConsulta & "                                    SET @VISTAS=1" & vbCrLf
sConsulta = sConsulta & "                          end" & vbCrLf
sConsulta = sConsulta & "                     ELSE" & vbCrLf
sConsulta = sConsulta & "                          begin" & vbCrLf
sConsulta = sConsulta & "                           IF (SELECT count(*) FROM CONF_VISTAS_GRUPO_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE  AND ATRIB =@ID AND POSICION IS NOT NULL AND POSICION<>0)>0 " & vbCrLf
sConsulta = sConsulta & "                                     SET @VISTAS=1" & vbCrLf
sConsulta = sConsulta & "                           IF (SELECT count(*) FROM CONF_VISTAS_ALL_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE  AND ATRIB =@ID AND POSICION IS NOT NULL AND POSICION<>0)>0 " & vbCrLf
sConsulta = sConsulta & "                                     SET @VISTAS=1" & vbCrLf
sConsulta = sConsulta & "                          end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                       IF (SELECT PLANTILLA_VISTAS FROM PROCE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE)=1 AND @VISTAS=1" & vbCrLf
sConsulta = sConsulta & "                           begin" & vbCrLf
sConsulta = sConsulta & "                           UPDATE PROCE SET PLANTILLA_VISTAS=0 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE  " & vbCrLf
sConsulta = sConsulta & "                           end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                     SELECT @COUNT=COUNT(*) FROM PROCE_OFE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                     IF  @AMBITOOLD>@AMBITO" & vbCrLf
sConsulta = sConsulta & "                              IF @AMBITOOLD=3" & vbCrLf
sConsulta = sConsulta & "                                     begin --old=3" & vbCrLf
sConsulta = sConsulta & "                                           IF @COUNT > 0" & vbCrLf
sConsulta = sConsulta & "                                                begin" & vbCrLf
sConsulta = sConsulta & "                                                DELETE OFE_ITEM_ATRIB WHERE  ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ATRIB_ID=@ID" & vbCrLf
sConsulta = sConsulta & "                                                end" & vbCrLf
sConsulta = sConsulta & "                                           DELETE CONF_VISTAS_GRUPO_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ATRIB=@ID" & vbCrLf
sConsulta = sConsulta & "                                           DELETE CONF_VISTAS_ALL_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE  AND ATRIB=@ID" & vbCrLf
sConsulta = sConsulta & "                                           DELETE CONF_VISTAS_ITEM_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE  AND ATRIB=@ID" & vbCrLf
sConsulta = sConsulta & "                                           INSERT INTO CONF_VISTAS_PROCE_ATRIB (ANYO,GMN1,PROCE,USU,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                                    SELECT @ANYO,@GMN1,@PROCE,USU,VISTA,@ID FROM  CONF_VISTAS_PROCE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "                                           IF @VISTAS=1" & vbCrLf
sConsulta = sConsulta & "                                            begin                                                              " & vbCrLf
sConsulta = sConsulta & "                                                  IF @GRUPO IS NULL" & vbCrLf
sConsulta = sConsulta & "                                                    BEGIN" & vbCrLf
sConsulta = sConsulta & "                                                        UPDATE CONF_VISTAS_GRUPO_ATRIB SET POSICION=NULL,VISIBLE=0,FILA=0 WHERE  ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "                                                        UPDATE CONF_VISTAS_GRUPO SET " & vbCrLf
sConsulta = sConsulta & "                                                              PRECIO_PROV_POS=(case when VI.USU is null then 2 else vi.PRECIO_PROV_POS end), PRECIO_PROV_WIDTH=(case when VI.USU is null then 1600 else vi.PRECIO_PROV_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "                                                              ,PRECIO_PROV_VISIBLE=(case when VI.USU is null then 1 else vi.PRECIO_PROV_VISIBLE end), PRECIO_PROV_LEVEL=(case when VI.USU is null then 0 else vi.PRECIO_PROV_LEVEL end)" & vbCrLf
sConsulta = sConsulta & "                                                              ,ADJ_PROV_POS=(case when VI.USU is null then 9 else vi.ADJ_PROV_POS end), ADJ_PROV_WIDTH=(case when VI.USU is null then 700 else vi.ADJ_PROV_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "                                                              ,ADJ_PROV_VISIBLE=(case when VI.USU is null then 1 else vi.ADJ_PROV_VISIBLE end), ADJ_PROV_LEVEL=(case when VI.USU is null then 1 else vi.ADJ_PROV_LEVEL end)" & vbCrLf
sConsulta = sConsulta & "                                                              ,CANT_PROV_POS=(case when VI.USU is null then 8 else vi.CANT_PROV_POS end), CANT_PROV_WIDTH=(case when VI.USU is null then 900 else vi.CANT_PROV_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "                                                              ,CANT_PROV_VISIBLE=(case when VI.USU is null then 1 else vi.CANT_PROV_VISIBLE end), CANT_PROV_LEVEL=(case when VI.USU is null then 1 else vi.CANT_PROV_LEVEL end)" & vbCrLf
sConsulta = sConsulta & "                                                               ,IMP_ADJ_POS=(case when VI.USU is null then 1 else vi.IMP_ADJ_POS end), IMP_ADJ_WIDTH=(case when VI.USU is null then 500 else vi.IMP_ADJ_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "                                                              ,IMP_ADJ_VISIBLE=(case when VI.USU is null then 0 else vi.IMP_ADJ_VISIBLE end), IMP_ADJ_LEVEL=(case when VI.USU is null then 0 else vi.IMP_ADJ_LEVEL end)" & vbCrLf
sConsulta = sConsulta & "                                                              ,GRUPOS_WIDTH=(case when VI.USU is null then 1600 else vi.GRUPOS_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "                                                               FROM CONF_VISTAS_GRUPO V  LEFT JOIN conf_vista_inicial_grupo VI ON VI.ES_GR=1 AND V.USU=VI.USU  WHERE V.ANYO=@ANYO AND V.GMN1=@GMN1 AND V.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "                                                    END" & vbCrLf
sConsulta = sConsulta & "                                                  ELSE" & vbCrLf
sConsulta = sConsulta & "                                                    BEGIN      " & vbCrLf
sConsulta = sConsulta & "                                                          UPDATE CONF_VISTAS_GRUPO_ATRIB SET POSICION=NULL,VISIBLE=0,FILA=0 WHERE  ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "                                                          UPDATE CONF_VISTAS_GRUPO SET " & vbCrLf
sConsulta = sConsulta & "                                                                PRECIO_PROV_POS=(case when VI.USU is null then 2 else vi.PRECIO_PROV_POS end), PRECIO_PROV_WIDTH=(case when VI.USU is null then 1600 else vi.PRECIO_PROV_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "                                                                 ,PRECIO_PROV_VISIBLE=(case when VI.USU is null then 1 else vi.PRECIO_PROV_VISIBLE end), PRECIO_PROV_LEVEL=(case when VI.USU is null then 0 else vi.PRECIO_PROV_LEVEL end)" & vbCrLf
sConsulta = sConsulta & "                                                                 ,ADJ_PROV_POS=(case when VI.USU is null then 9 else vi.ADJ_PROV_POS end), ADJ_PROV_WIDTH=(case when VI.USU is null then 700 else vi.ADJ_PROV_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "                                                                 ,ADJ_PROV_VISIBLE=(case when VI.USU is null then 1 else vi.ADJ_PROV_VISIBLE end), ADJ_PROV_LEVEL=(case when VI.USU is null then 1 else vi.ADJ_PROV_LEVEL end)" & vbCrLf
sConsulta = sConsulta & "                                                                 ,CANT_PROV_POS=(case when VI.USU is null then 8 else vi.CANT_PROV_POS end), CANT_PROV_WIDTH=(case when VI.USU is null then 900 else vi.CANT_PROV_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "                                                                  ,CANT_PROV_VISIBLE=(case when VI.USU is null then 1 else vi.CANT_PROV_VISIBLE end), CANT_PROV_LEVEL=(case when VI.USU is null then 1 else vi.CANT_PROV_LEVEL end)" & vbCrLf
sConsulta = sConsulta & "                                                                 ,IMP_ADJ_POS=(case when VI.USU is null then 1 else vi.IMP_ADJ_POS end), IMP_ADJ_WIDTH=(case when VI.USU is null then 500 else vi.IMP_ADJ_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "                                                                 ,IMP_ADJ_VISIBLE=(case when VI.USU is null then 0 else vi.IMP_ADJ_VISIBLE end), IMP_ADJ_LEVEL=(case when VI.USU is null then 0 else vi.IMP_ADJ_LEVEL end)" & vbCrLf
sConsulta = sConsulta & "                                                                 ,GRUPOS_WIDTH=(case when VI.USU is null then 1600 else vi.GRUPOS_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "                                                                  FROM CONF_VISTAS_GRUPO V  LEFT JOIN conf_vista_inicial_grupo VI ON VI.ES_GR=1 AND V.USU=VI.USU  WHERE V.ANYO=@ANYO AND V.GMN1=@GMN1 AND V.PROCE=@PROCE AND V.GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "                                                     END" & vbCrLf
sConsulta = sConsulta & "                                                   UPDATE CONF_VISTAS_ALL_ATRIB SET POSICION=NULL,VISIBLE=0,FILA=0 WHERE  ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "                                                   UPDATE CONF_VISTAS_ALL SET " & vbCrLf
sConsulta = sConsulta & "                             PRECIO_PROV_POS=(case when VI.USU is null then 2 else vi.PRECIO_PROV_POS end), PRECIO_PROV_WIDTH=(case when VI.USU is null then 1600 else vi.PRECIO_PROV_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "                        ,PRECIO_PROV_VISIBLE=(case when VI.USU is null then 1 else vi.PRECIO_PROV_VISIBLE end), PRECIO_PROV_LEVEL=(case when VI.USU is null then 0 else vi.PRECIO_PROV_LEVEL end)" & vbCrLf
sConsulta = sConsulta & "                       ,ADJ_PROV_POS=(case when VI.USU is null then 9 else vi.ADJ_PROV_POS end), ADJ_PROV_WIDTH=(case when VI.USU is null then 700 else vi.ADJ_PROV_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "                        ,ADJ_PROV_VISIBLE=(case when VI.USU is null then 1 else vi.ADJ_PROV_VISIBLE end), ADJ_PROV_LEVEL=(case when VI.USU is null then 1 else vi.ADJ_PROV_LEVEL end)" & vbCrLf
sConsulta = sConsulta & "                        ,CANT_PROV_POS=(case when VI.USU is null then 8 else vi.CANT_PROV_POS end), CANT_PROV_WIDTH=(case when VI.USU is null then 900 else vi.CANT_PROV_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "                        ,CANT_PROV_VISIBLE=(case when VI.USU is null then 1 else vi.CANT_PROV_VISIBLE end), CANT_PROV_LEVEL=(case when VI.USU is null then 1 else vi.CANT_PROV_LEVEL end)" & vbCrLf
sConsulta = sConsulta & "                        ,IMP_ADJ_POS=(case when VI.USU is null then 1 else vi.IMP_ADJ_POS end), IMP_ADJ_WIDTH=(case when VI.USU is null then 500 else vi.IMP_ADJ_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "                        ,IMP_ADJ_VISIBLE=(case when VI.USU is null then 0 else vi.IMP_ADJ_VISIBLE end), IMP_ADJ_LEVEL=(case when VI.USU is null then 0 else vi.IMP_ADJ_LEVEL end)" & vbCrLf
sConsulta = sConsulta & "                        ,GRUPOS_WIDTH=(case when VI.USU is null then 1600 else vi.GRUPOS_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "                     FROM CONF_VISTAS_ALL V  LEFT JOIN conf_vista_inicial_grupo VI ON VI.ES_GR=0 AND V.USU=VI.USU  WHERE V.ANYO=@ANYO AND V.GMN1=@GMN1 AND V.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "                                            end -- vista" & vbCrLf
sConsulta = sConsulta & "                                     end  --old=3" & vbCrLf
sConsulta = sConsulta & "                              ELSE --ser�a old=2" & vbCrLf
sConsulta = sConsulta & "                                     begin" & vbCrLf
sConsulta = sConsulta & "                                          IF @COUNT>0 " & vbCrLf
sConsulta = sConsulta & "                                              BEGIN" & vbCrLf
sConsulta = sConsulta & "                                              IF @GRUPO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                                                   DELETE OFE_GR_ATRIB WHERE  ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ATRIB_ID=@ID AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "                                              ELSE" & vbCrLf
sConsulta = sConsulta & "                                                   DELETE OFE_GR_ATRIB WHERE  ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ATRIB_ID=@ID" & vbCrLf
sConsulta = sConsulta & "                                              END " & vbCrLf
sConsulta = sConsulta & "                                          IF @VISTAS=1 " & vbCrLf
sConsulta = sConsulta & "                                              BEGIN                  " & vbCrLf
sConsulta = sConsulta & "                                              UPDATE CONF_VISTAS_PROCE_ATRIB SET POS=NULL,VISIBLE=0 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "                                              UPDATE CONF_VISTAS_PROCE SET " & vbCrLf
sConsulta = sConsulta & "               CONSUMIDO_POS=(case when VI.USU is null then 1 else vi.CONSUMIDO_POS end), CONSUMIDO_WIDTH=(case when VI.USU is null then 850 else vi.CONSUMIDO_WIDTH end),CONSUMIDO_VISIBLE=(case when VI.USU is null then 1 else vi.CONSUMIDO_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "               ,IMP_POS=(case when VI.USU is null then 2 else vi.IMP_POS end), IMP_WIDTH=(case when VI.USU is null then 850 else vi.IMP_WIDTH end),IMP_VISIBLE=(case when VI.USU is null then 0 else vi.IMP_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "               ,ADJ_POS=(case when VI.USU is null then 3 else vi.ADJ_POS end), ADJ_WIDTH=(case when VI.USU is null then 850 else vi.ADJ_WIDTH end),ADJ_VISIBLE=(case when VI.USU is null then 1 else vi.ADJ_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "               ,AHORR_ADJ_POS=(case when VI.USU is null then 5 else vi.AHORR_ADJ_POS end), AHORR_ADJ_WIDTH=(case when VI.USU is null then 850 else vi.AHORR_ADJ_WIDTH end),AHORR_ADJ_VISIBLE=(case when VI.USU is null then 1 else vi.AHORR_ADJ_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "               ,AHORR_ADJ_PORCEN_POS=(case when VI.USU is null then 4 else vi.AHORR_ADJ_PORCEN_POS end), AHORR_ADJ_PORCEN_WIDTH=(case when VI.USU is null then 850 else vi.AHORR_ADJ_PORCEN_WIDTH end),AHORR_ADJ_PORCEN_VISIBLE=(case when VI.USU is null then 0 else vi.AHORR_ADJ_PORCEN_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "               ,AHORR_OFE_POS=(case when VI.USU is null then 7 else vi.AHORR_OFE_POS end), AHORR_OFE_WIDTH=(case when VI.USU is null then 850 else vi.AHORR_OFE_WIDTH end),AHORR_OFE_VISIBLE=(case when VI.USU is null then 1 else vi.AHORR_OFE_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "               ,AHORR_OFE_PORCEN_POS=(case when VI.USU is null then 6 else vi.AHORR_OFE_PORCEN_POS end), AHORR_OFE_PORCEN_WIDTH=(case when VI.USU is null then 850 else vi.AHORR_OFE_PORCEN_WIDTH end),AHORR_OFE_PORCEN_VISIBLE=(case when VI.USU is null then 0 else vi.AHORR_OFE_PORCEN_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "               ,GRUPO_WIDTH=(case when VI.USU is null then 2600 else vi.GRUPO_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "               FROM CONF_VISTAS_PROCE V LEFT JOIN CONF_VISTA_INICIAL_PROCE VI ON V.USU=VI.USU WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "                                              END             " & vbCrLf
sConsulta = sConsulta & "                                     end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                     ELSE  --old < ambito" & vbCrLf
sConsulta = sConsulta & "                                IF @AMBITOOLD=1" & vbCrLf
sConsulta = sConsulta & "                                     BEGIN --old=1" & vbCrLf
sConsulta = sConsulta & "                                           IF @COUNT>0" & vbCrLf
sConsulta = sConsulta & "                                                   begin" & vbCrLf
sConsulta = sConsulta & "                                                   DELETE OFE_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ATRIB_ID=@ID            " & vbCrLf
sConsulta = sConsulta & "                                                   end" & vbCrLf
sConsulta = sConsulta & "                                          IF @AMBITO = 3" & vbCrLf
sConsulta = sConsulta & "                                                   begin --ambito=3" & vbCrLf
sConsulta = sConsulta & "                                                          DELETE CONF_VISTAS_PROCE_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ATRIB=@ID" & vbCrLf
sConsulta = sConsulta & "                                                            --pasa de ambito 1 a ambito 3, el @grupo es siempre null, para todos los grupos" & vbCrLf
sConsulta = sConsulta & "                                                            INSERT INTO CONF_VISTAS_GRUPO_ATRIB (ANYO,GMN1,PROCE,GRUPO,USU,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                                                    SELECT @ANYO,@GMN1,@PROCE,GRUPO,USU,VISTA,@ID FROM  CONF_VISTAS_GRUPO WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE         " & vbCrLf
sConsulta = sConsulta & "                                                            INSERT INTO CONF_VISTAS_ITEM_ATRIB (ANYO,GMN1,PROCE,GRUPO,USU,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                                                   SELECT @ANYO,@GMN1,@PROCE,GRUPO,USU,VISTA,@ID FROM  CONF_VISTAS_ITEM WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "                                                            INSERT INTO CONF_VISTAS_ALL_ATRIB (ANYO,GMN1,PROCE,USU,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                                                   SELECT @ANYO,@GMN1,@PROCE,USU,VISTA,@ID FROM  CONF_VISTAS_ALL WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE              " & vbCrLf
sConsulta = sConsulta & "                                                  end--ambito=3" & vbCrLf
sConsulta = sConsulta & "                                           IF @VISTAS=1" & vbCrLf
sConsulta = sConsulta & "                                               begin                               " & vbCrLf
sConsulta = sConsulta & "                                                   UPDATE CONF_VISTAS_PROCE_ATRIB SET POS=NULL,VISIBLE=0 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "                                                   UPDATE CONF_VISTAS_PROCE SET " & vbCrLf
sConsulta = sConsulta & "               CONSUMIDO_POS=(case when VI.USU is null then 1 else vi.CONSUMIDO_POS end), CONSUMIDO_WIDTH=(case when VI.USU is null then 850 else vi.CONSUMIDO_WIDTH end),CONSUMIDO_VISIBLE=(case when VI.USU is null then 1 else vi.CONSUMIDO_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "               ,IMP_POS=(case when VI.USU is null then 2 else vi.IMP_POS end), IMP_WIDTH=(case when VI.USU is null then 850 else vi.IMP_WIDTH end),IMP_VISIBLE=(case when VI.USU is null then 0 else vi.IMP_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "               ,ADJ_POS=(case when VI.USU is null then 3 else vi.ADJ_POS end), ADJ_WIDTH=(case when VI.USU is null then 850 else vi.ADJ_WIDTH end),ADJ_VISIBLE=(case when VI.USU is null then 1 else vi.ADJ_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "               ,AHORR_ADJ_POS=(case when VI.USU is null then 5 else vi.AHORR_ADJ_POS end), AHORR_ADJ_WIDTH=(case when VI.USU is null then 850 else vi.AHORR_ADJ_WIDTH end),AHORR_ADJ_VISIBLE=(case when VI.USU is null then 1 else vi.AHORR_ADJ_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "               ,AHORR_ADJ_PORCEN_POS=(case when VI.USU is null then 4 else vi.AHORR_ADJ_PORCEN_POS end), AHORR_ADJ_PORCEN_WIDTH=(case when VI.USU is null then 850 else vi.AHORR_ADJ_PORCEN_WIDTH end),AHORR_ADJ_PORCEN_VISIBLE=(case when VI.USU is null then 0 else vi.AHORR_ADJ_PORCEN_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "               ,AHORR_OFE_POS=(case when VI.USU is null then 7 else vi.AHORR_OFE_POS end), AHORR_OFE_WIDTH=(case when VI.USU is null then 850 else vi.AHORR_OFE_WIDTH end),AHORR_OFE_VISIBLE=(case when VI.USU is null then 1 else vi.AHORR_OFE_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "               ,AHORR_OFE_PORCEN_POS=(case when VI.USU is null then 6 else vi.AHORR_OFE_PORCEN_POS end), AHORR_OFE_PORCEN_WIDTH=(case when VI.USU is null then 850 else vi.AHORR_OFE_PORCEN_WIDTH end),AHORR_OFE_PORCEN_VISIBLE=(case when VI.USU is null then 0 else vi.AHORR_OFE_PORCEN_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "               ,GRUPO_WIDTH=(case when VI.USU is null then 2600 else vi.GRUPO_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "               FROM CONF_VISTAS_PROCE V LEFT JOIN CONF_VISTA_INICIAL_PROCE VI ON V.USU=VI.USU WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "                                                end --vistas" & vbCrLf
sConsulta = sConsulta & "                                     END --old=1" & vbCrLf
sConsulta = sConsulta & "                                ELSE " & vbCrLf
sConsulta = sConsulta & "                                     BEGIN  --old es 2, nuevo es 3" & vbCrLf
sConsulta = sConsulta & "                                           IF @COUNT>0" & vbCrLf
sConsulta = sConsulta & "                                               begin" & vbCrLf
sConsulta = sConsulta & "                                               IF @GRUPO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                                                   begin" & vbCrLf
sConsulta = sConsulta & "                                                    DELETE OFE_GR_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ATRIB_ID=@ID AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "                                                    end" & vbCrLf
sConsulta = sConsulta & "                                                ELSE" & vbCrLf
sConsulta = sConsulta & "                                                     begin" & vbCrLf
sConsulta = sConsulta & "                                                     DELETE OFE_GR_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ATRIB_ID=@ID " & vbCrLf
sConsulta = sConsulta & "                                                     end" & vbCrLf
sConsulta = sConsulta & "                                               end" & vbCrLf
sConsulta = sConsulta & "                                           DELETE CONF_VISTAS_PROCE_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ATRIB=@ID" & vbCrLf
sConsulta = sConsulta & "                                           IF @GRUPO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                                                   begin" & vbCrLf
sConsulta = sConsulta & "                                                    INSERT INTO CONF_VISTAS_GRUPO_ATRIB (ANYO,GMN1,PROCE,GRUPO,USU,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                                         SELECT @ANYO,@GMN1,@PROCE,@GRUPO,USU,VISTA,@ID FROM  CONF_VISTAS_GRUPO WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO " & vbCrLf
sConsulta = sConsulta & "                                                    INSERT INTO CONF_VISTAS_ITEM_ATRIB (ANYO,GMN1,PROCE,GRUPO,USU,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                                        SELECT @ANYO,@GMN1,@PROCE,@GRUPO,USU,VISTA,@ID FROM  CONF_VISTAS_ITEM WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO " & vbCrLf
sConsulta = sConsulta & "                                                    end" & vbCrLf
sConsulta = sConsulta & "                                           ELSE" & vbCrLf
sConsulta = sConsulta & "                                                     begin" & vbCrLf
sConsulta = sConsulta & "                                                     INSERT INTO CONF_VISTAS_GRUPO_ATRIB (ANYO,GMN1,PROCE,GRUPO,USU,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                                          SELECT @ANYO,@GMN1,@PROCE,GRUPO,USU,VISTA,@ID FROM  CONF_VISTAS_GRUPO WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "                                                     INSERT INTO CONF_VISTAS_ITEM_ATRIB (ANYO,GMN1,PROCE,GRUPO,USU,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                                          SELECT @ANYO,@GMN1,@PROCE,GRUPO,USU,VISTA,@ID FROM  CONF_VISTAS_ITEM WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "                                                     end" & vbCrLf
sConsulta = sConsulta & "                                           INSERT INTO CONF_VISTAS_ALL_ATRIB (ANYO,GMN1,PROCE,USU,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                                  SELECT @ANYO,@GMN1,@PROCE,USU,VISTA,@ID FROM  CONF_VISTAS_ALL WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "                                           IF @VISTAS=1" & vbCrLf
sConsulta = sConsulta & "                                               begin" & vbCrLf
sConsulta = sConsulta & "                                                   UPDATE CONF_VISTAS_PROCE_ATRIB SET POS=NULL,VISIBLE=0 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "                          UPDATE CONF_VISTAS_PROCE SET " & vbCrLf
sConsulta = sConsulta & "                              CONSUMIDO_POS=(case when VI.USU is null then 1 else vi.CONSUMIDO_POS end), CONSUMIDO_WIDTH=(case when VI.USU is null then 850 else vi.CONSUMIDO_WIDTH end),CONSUMIDO_VISIBLE=(case when VI.USU is null then 1 else vi.CONSUMIDO_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "                                                           ,IMP_POS=(case when VI.USU is null then 2 else vi.IMP_POS end), IMP_WIDTH=(case when VI.USU is null then 850 else vi.IMP_WIDTH end),IMP_VISIBLE=(case when VI.USU is null then 0 else vi.IMP_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "                                                           ,ADJ_POS=(case when VI.USU is null then 3 else vi.ADJ_POS end), ADJ_WIDTH=(case when VI.USU is null then 850 else vi.ADJ_WIDTH end),ADJ_VISIBLE=(case when VI.USU is null then 1 else vi.ADJ_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "                                                           ,AHORR_ADJ_POS=(case when VI.USU is null then 5 else vi.AHORR_ADJ_POS end), AHORR_ADJ_WIDTH=(case when VI.USU is null then 850 else vi.AHORR_ADJ_WIDTH end),AHORR_ADJ_VISIBLE=(case when VI.USU is null then 1 else vi.AHORR_ADJ_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "                                                           ,AHORR_ADJ_PORCEN_POS=(case when VI.USU is null then 4 else vi.AHORR_ADJ_PORCEN_POS end), AHORR_ADJ_PORCEN_WIDTH=(case when VI.USU is null then 850 else vi.AHORR_ADJ_PORCEN_WIDTH end),AHORR_ADJ_PORCEN_VISIBLE=(case when VI.USU is null then 0 else vi.AHORR_ADJ_PORCEN_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "                                                           ,AHORR_OFE_POS=(case when VI.USU is null then 7 else vi.AHORR_OFE_POS end), AHORR_OFE_WIDTH=(case when VI.USU is null then 850 else vi.AHORR_OFE_WIDTH end),AHORR_OFE_VISIBLE=(case when VI.USU is null then 1 else vi.AHORR_OFE_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "                                                          ,AHORR_OFE_PORCEN_POS=(case when VI.USU is null then 6 else vi.AHORR_OFE_PORCEN_POS end), AHORR_OFE_PORCEN_WIDTH=(case when VI.USU is null then 850 else vi.AHORR_OFE_PORCEN_WIDTH end),AHORR_OFE_PORCEN_VISIBLE=(case when VI.USU is null then 0 else vi.AHORR_OFE_PORCEN_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "                                                           ,GRUPO_WIDTH=(case when VI.USU is null then 2600 else vi.GRUPO_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "                                                           FROM CONF_VISTAS_PROCE V LEFT JOIN CONF_VISTA_INICIAL_PROCE VI ON V.USU=VI.USU WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "                                              end --vistas" & vbCrLf
sConsulta = sConsulta & "                                     END--old es 2" & vbCrLf
sConsulta = sConsulta & "               END --@ambito<>ambitoold" & vbCrLf
sConsulta = sConsulta & "         END -- @tabla=1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @TABLA=2" & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "          IF @APLIC_PREC IS not  NULL  AND @ORDEN is  NULL " & vbCrLf
sConsulta = sConsulta & "                 BEGIN" & vbCrLf
sConsulta = sConsulta & "                     SELECT @ATRIB_PREC =ATRIB  FROM PLANTILLA_ATRIB WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "                     SELECT @ORDEN=ORDEN FROM PLANTILLA_ATRIB WHERE PLANT=@PLANT AND APLIC_PREC = @APLIC_PREC AND OP_PREC=@OP_PREC AND ATRIB =@ATRIB_PREC" & vbCrLf
sConsulta = sConsulta & "                     IF @ORDEN IS NULL" & vbCrLf
sConsulta = sConsulta & "                         BEGIN" & vbCrLf
sConsulta = sConsulta & "                           SET @APLI_PREC_V=@APLIC_PREC" & vbCrLf
sConsulta = sConsulta & "                           SET @ORDEN_V= 0" & vbCrLf
sConsulta = sConsulta & "                           WHILE @APLI_PREC_V < 3  AND @ORDEN_V= 0" & vbCrLf
sConsulta = sConsulta & "                                 BEGIN" & vbCrLf
sConsulta = sConsulta & "                                     SELECT  @ORDEN_V = MAX(ORDEN) FROM PLANTILLA_ATRIB WHERE  PLANT=@PLANT AND APLIC_PREC = @APLI_PREC_V" & vbCrLf
sConsulta = sConsulta & "                                      IF @ORDEN_V IS NULL" & vbCrLf
sConsulta = sConsulta & "                                         begin" & vbCrLf
sConsulta = sConsulta & "                                              SET @ORDEN_V= 0" & vbCrLf
sConsulta = sConsulta & "                                              SET @APLI_PREC_V=@APLI_PREC_V + 1" & vbCrLf
sConsulta = sConsulta & "                                         end" & vbCrLf
sConsulta = sConsulta & "                                END" & vbCrLf
sConsulta = sConsulta & "                          UPDATE PLANTILLA_ATRIB SET ORDEN = ORDEN +1 WHERE  APLIC_PREC < @APLIC_PREC AND PLANT=@PLANT" & vbCrLf
sConsulta = sConsulta & "                          SET @ORDEN=@ORDEN_V +1" & vbCrLf
sConsulta = sConsulta & "                        END" & vbCrLf
sConsulta = sConsulta & "                   ELSE" & vbCrLf
sConsulta = sConsulta & "            begin" & vbCrLf
sConsulta = sConsulta & "                        IF @APLIC_PREC IS not  NULL   " & vbCrLf
sConsulta = sConsulta & "       begin" & vbCrLf
sConsulta = sConsulta & "                            UPDATE PLANTILLA_ATRIB SET ORDEN = ORDEN +1 WHERE  APLIC_PREC < @APLIC_PREC AND PLANT=@PLANT" & vbCrLf
sConsulta = sConsulta & "       end" & vbCrLf
sConsulta = sConsulta & "           end" & vbCrLf
sConsulta = sConsulta & "                 END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "           SELECT @POND=POND, @AMBITOOLD=AMBITO FROM PLANTILLA_ATRIB WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "           IF @INTRO=0 AND @POND=2 SET @POND=0" & vbCrLf
sConsulta = sConsulta & "               UPDATE PLANTILLA_ATRIB SET PREFBAJO=@PREFBAJO,INTRO=@INTRO,MAXNUM=@MAXNUM  ," & vbCrLf
sConsulta = sConsulta & "                    MINNUM=@MINNUM,MAXFEC=CONVERT(VARCHAR(10),@MAXFEC,111)," & vbCrLf
sConsulta = sConsulta & "                    MINFEC=CONVERT(VARCHAR(10),@MINFEC,111),INTERNO=@INTERNO,AMBITO=@AMBITO,OBLIG=@OBLIG," & vbCrLf
sConsulta = sConsulta & "                    OP_PREC=@OP_PREC,APLIC_PREC=@APLIC_PREC," & vbCrLf
sConsulta = sConsulta & "                    DEF_PREC=@DEF_PREC,POND=@POND,ORDEN=@ORDEN WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "          SELECT @FECACT=FECACT FROM PLANTILLA_ATRIB WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "          IF   @ATRIB IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                BEGIN" & vbCrLf
sConsulta = sConsulta & "                    SELECT @POND=POND FROM DEF_ATRIB WHERE ID=@ATRIB" & vbCrLf
sConsulta = sConsulta & "                    IF @INTRO=0 AND @POND=2 SET @POND=0" & vbCrLf
sConsulta = sConsulta & "                        UPDATE DEF_ATRIB SET PREFBAJO=@PREFBAJO,INTRO=@INTRO,MAXNUM=CAST(@MAXNUM AS VARCHAR(15)) ," & vbCrLf
sConsulta = sConsulta & "                            MINNUM=CAST(@MINNUM AS VARCHAR(15)),MAXFEC=CONVERT(VARCHAR(10),@MAXFEC,111)," & vbCrLf
sConsulta = sConsulta & "                            MINFEC=CONVERT(VARCHAR(10),@MINFEC,111),POND=@POND, DESCR=@DESCR WHERE ID=@ATRIB" & vbCrLf
sConsulta = sConsulta & "                END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "     IF @AMBITO<>@AMBITOOLD" & vbCrLf
sConsulta = sConsulta & "       BEGIN --@ambito<>ambitoold           " & vbCrLf
sConsulta = sConsulta & "           IF @AMBITOOLD<3 " & vbCrLf
sConsulta = sConsulta & "               begin" & vbCrLf
sConsulta = sConsulta & "                IF (SELECT count(*) FROM PLANTILLA_CONF_VISTAS_PROCE_ATRIB WHERE PLANT=@PLANT AND ATRIB =@ID AND POS IS NOT NULL AND POS<>0)>0 SET @VISTAS=1" & vbCrLf
sConsulta = sConsulta & "              end" & vbCrLf
sConsulta = sConsulta & "           ELSE" & vbCrLf
sConsulta = sConsulta & "              begin" & vbCrLf
sConsulta = sConsulta & "                IF (SELECT count(*) FROM PLANTILLA_CONF_VISTAS_GRUPO_ATRIB WHERE PLANT=@PLANT AND ATRIB =@ID AND POSICION IS NOT NULL AND POSICION<>0)>0 SET @VISTAS=1" & vbCrLf
sConsulta = sConsulta & "                IF (SELECT count(*) FROM PLANTILLA_CONF_VISTAS_ALL_ATRIB WHERE PLANT=@PLANT AND ATRIB =@ID AND POSICION IS NOT NULL AND POSICION<>0)>0 SET @VISTAS=1" & vbCrLf
sConsulta = sConsulta & "              end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "            IF  @AMBITOOLD>@AMBITO" & vbCrLf
sConsulta = sConsulta & "             begin" & vbCrLf
sConsulta = sConsulta & "                IF @AMBITOOLD=3" & vbCrLf
sConsulta = sConsulta & "                   begin --old=3" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                         DELETE PLANTILLA_CONF_VISTAS_GRUPO_ATRIB WHERE PLANT=@PLANT AND ATRIB=@ID" & vbCrLf
sConsulta = sConsulta & "                         DELETE PLANTILLA_CONF_VISTAS_ALL_ATRIB WHERE PLANT=@PLANT AND ATRIB=@ID" & vbCrLf
sConsulta = sConsulta & "                                              " & vbCrLf
sConsulta = sConsulta & "                         INSERT INTO PLANTILLA_CONF_VISTAS_PROCE_ATRIB (PLANT,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                SELECT @PLANT,VISTA,@ID FROM  PLANTILLA_CONF_VISTAS_PROCE WHERE PLANT=@PLANT " & vbCrLf
sConsulta = sConsulta & "                         IF @VISTAS=1" & vbCrLf
sConsulta = sConsulta & "                         begin" & vbCrLf
sConsulta = sConsulta & "               UPDATE PLANTILLA_CONF_VISTAS_ALL_ATRIB SET POSICION=NULL,VISIBLE=0,FILA=0 WHERE PLANT=@PLANT" & vbCrLf
sConsulta = sConsulta & "               UPDATE PLANTILLA_CONF_VISTAS_ALL SET PRECIO_PROV_POS=2 , PRECIO_PROV_WIDTH=1600 ,PRECIO_PROV_VISIBLE=1, PRECIO_PROV_LEVEL=0," & vbCrLf
sConsulta = sConsulta & "                    ADJ_PROV_POS=9, ADJ_PROV_WIDTH=700, ADJ_PROV_VISIBLE=1, ADJ_PROV_LEVEL=1,CANT_PROV_POS=8 , CANT_PROV_WIDTH=900 ,CANT_PROV_VISIBLE=1, CANT_PROV_LEVEL=1 ," & vbCrLf
sConsulta = sConsulta & "                    IMP_ADJ_POS=1, IMP_ADJ_WIDTH=500 ,IMP_ADJ_VISIBLE=0, IMP_ADJ_LEVEL=0 , GRUPOS_WIDTH=1600  WHERE PLANT=@PLANT" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "              IF @GRUPO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "       begin" & vbCrLf
sConsulta = sConsulta & "                  UPDATE PLANTILLA_CONF_VISTAS_GRUPO_ATRIB SET POSICION=NULL,VISIBLE=0,FILA=0 WHERE PLANT=@PLANT AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "                  UPDATE PLANTILLA_CONF_VISTAS_GRUPO SET PRECIO_PROV_POS=2 , PRECIO_PROV_WIDTH=1600 ,PRECIO_PROV_VISIBLE=1, PRECIO_PROV_LEVEL=0," & vbCrLf
sConsulta = sConsulta & "                    ADJ_PROV_POS=9, ADJ_PROV_WIDTH=700, ADJ_PROV_VISIBLE=1, ADJ_PROV_LEVEL=1,  CANT_PROV_POS=8 , CANT_PROV_WIDTH=900 ,CANT_PROV_VISIBLE=1, CANT_PROV_LEVEL=1 ," & vbCrLf
sConsulta = sConsulta & "                    IMP_ADJ_POS=1, IMP_ADJ_WIDTH=500 ,IMP_ADJ_VISIBLE=0, IMP_ADJ_LEVEL=0 , GRUPOS_WIDTH=1600 WHERE PLANT=@PLANT  AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "       end" & vbCrLf
sConsulta = sConsulta & "              ELSE" & vbCrLf
sConsulta = sConsulta & "       begin" & vbCrLf
sConsulta = sConsulta & "                  UPDATE PLANTILLA_CONF_VISTAS_GRUPO_ATRIB SET POSICION=NULL,VISIBLE=0,FILA=0 WHERE PLANT=@PLANT " & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "                  UPDATE PLANTILLA_CONF_VISTAS_GRUPO SET PRECIO_PROV_POS=2 , PRECIO_PROV_WIDTH=1600 ,PRECIO_PROV_VISIBLE=1, PRECIO_PROV_LEVEL=0," & vbCrLf
sConsulta = sConsulta & "                    ADJ_PROV_POS=9, ADJ_PROV_WIDTH=700, ADJ_PROV_VISIBLE=1, ADJ_PROV_LEVEL=1,  CANT_PROV_POS=8 , CANT_PROV_WIDTH=900 ,CANT_PROV_VISIBLE=1, CANT_PROV_LEVEL=1 ," & vbCrLf
sConsulta = sConsulta & "                    IMP_ADJ_POS=1, IMP_ADJ_WIDTH=500 ,IMP_ADJ_VISIBLE=0, IMP_ADJ_LEVEL=0 , GRUPOS_WIDTH=1600 WHERE PLANT=@PLANT" & vbCrLf
sConsulta = sConsulta & "       end" & vbCrLf
sConsulta = sConsulta & "                          end" & vbCrLf
sConsulta = sConsulta & "                   end  --old=3" & vbCrLf
sConsulta = sConsulta & "   ELSE --Ser�a ambitoold=2" & vbCrLf
sConsulta = sConsulta & "        begin" & vbCrLf
sConsulta = sConsulta & "                          if @VISTAS=1 " & vbCrLf
sConsulta = sConsulta & "                          begin" & vbCrLf
sConsulta = sConsulta & "       UPDATE PLANTILLA_CONF_VISTAS_PROCE_ATRIB SET POS=NULL,VISIBLE=0 WHERE PLANT=@PLANT" & vbCrLf
sConsulta = sConsulta & "       UPDATE PLANTILLA_CONF_VISTAS_PROCE SET CONSUMIDO_POS=1, CONSUMIDO_WIDTH=850,CONSUMIDO_VISIBLE=1,IMP_POS=2, IMP_WIDTH=850, IMP_VISIBLE=0, ADJ_POS=3, ADJ_WIDTH=850," & vbCrLf
sConsulta = sConsulta & "               ADJ_VISIBLE=1, AHORR_ADJ_POS=5, AHORR_ADJ_WIDTH=850, AHORR_ADJ_VISIBLE=1, AHORR_ADJ_PORCEN_POS=4, AHORR_ADJ_PORCEN_WIDTH=850 ," & vbCrLf
sConsulta = sConsulta & "               AHORR_ADJ_PORCEN_VISIBLE=0, AHORR_OFE_POS=7, AHORR_OFE_WIDTH=850,AHORR_OFE_VISIBLE=1," & vbCrLf
sConsulta = sConsulta & "               AHORR_OFE_PORCEN_POS= 6, AHORR_OFE_PORCEN_WIDTH=850 ,AHORR_OFE_PORCEN_VISIBLE=0,GRUPO_WIDTH=2600 WHERE PLANT=@PLANT" & vbCrLf
sConsulta = sConsulta & "                         end" & vbCrLf
sConsulta = sConsulta & "        end" & vbCrLf
sConsulta = sConsulta & "            end --@AMBITOOLD>@AMBITO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "           ELSE  --old < ambito" & vbCrLf
sConsulta = sConsulta & "             IF @AMBITOOLD=1" & vbCrLf
sConsulta = sConsulta & "                  begin --old=1" & vbCrLf
sConsulta = sConsulta & "       IF @AMBITO = 3" & vbCrLf
sConsulta = sConsulta & "               begin --ambito=3" & vbCrLf
sConsulta = sConsulta & "                               DELETE PLANTILLA_CONF_VISTAS_PROCE_ATRIB WHERE PLANT=@PLANT AND ATRIB=@ID" & vbCrLf
sConsulta = sConsulta & "                               IF @GRUPO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                                      INSERT INTO PLANTILLA_CONF_VISTAS_GRUPO_ATRIB (PLANT,GRUPO,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                             SELECT @PLANT,@GRUPO,VISTA,@ID FROM  PLANTILLA_CONF_VISTAS_GRUPO WHERE PLANT=@PLANT AND GRUPO=@GRUPO " & vbCrLf
sConsulta = sConsulta & "                               ELSE" & vbCrLf
sConsulta = sConsulta & "                                      INSERT INTO PLANTILLA_CONF_VISTAS_GRUPO_ATRIB (PLANT,GRUPO,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                             SELECT @PLANT,GRUPO,VISTA,@ID FROM  PLANTILLA_CONF_VISTAS_GRUPO WHERE PLANT=@PLANT         " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                               INSERT INTO PLANTILLA_CONF_VISTAS_ALL_ATRIB (PLANT,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                      SELECT @PLANT,VISTA,@ID FROM PLANTILLA_CONF_VISTAS_ALL WHERE PLANT=@PLANT " & vbCrLf
sConsulta = sConsulta & "           end" & vbCrLf
sConsulta = sConsulta & "                            IF @VISTAS=1" & vbCrLf
sConsulta = sConsulta & "                            begin" & vbCrLf
sConsulta = sConsulta & "                UPDATE PLANTILLA_CONF_VISTAS_PROCE_ATRIB SET POS=NULL,VISIBLE=0 WHERE PLANT=@PLANT" & vbCrLf
sConsulta = sConsulta & "            UPDATE PLANTILLA_CONF_VISTAS_PROCE SET CONSUMIDO_POS=1, CONSUMIDO_WIDTH=850,CONSUMIDO_VISIBLE=1,IMP_POS=2, IMP_WIDTH=850, IMP_VISIBLE=0, ADJ_POS=3, ADJ_WIDTH=850," & vbCrLf
sConsulta = sConsulta & "               ADJ_VISIBLE=1, AHORR_ADJ_POS=5, AHORR_ADJ_WIDTH=850, AHORR_ADJ_VISIBLE=1, AHORR_ADJ_PORCEN_POS=4, AHORR_ADJ_PORCEN_WIDTH=850 ," & vbCrLf
sConsulta = sConsulta & "               AHORR_ADJ_PORCEN_VISIBLE=0, AHORR_OFE_POS=7, AHORR_OFE_WIDTH=850,AHORR_OFE_VISIBLE=1," & vbCrLf
sConsulta = sConsulta & "               AHORR_OFE_PORCEN_POS= 6, AHORR_OFE_PORCEN_WIDTH=850 ,AHORR_OFE_PORCEN_VISIBLE=0,GRUPO_WIDTH=2600 WHERE PLANT=@PLANT" & vbCrLf
sConsulta = sConsulta & "                            end" & vbCrLf
sConsulta = sConsulta & "                 end --old=1" & vbCrLf
sConsulta = sConsulta & "             ELSE " & vbCrLf
sConsulta = sConsulta & "                 BEGIN  --old es 2, nuevo es 3" & vbCrLf
sConsulta = sConsulta & "                      DELETE PLANTILLA_CONF_VISTAS_PROCE_ATRIB WHERE PLANT=@PLANT AND ATRIB=@ID" & vbCrLf
sConsulta = sConsulta & "                      IF @GRUPO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "       begin" & vbCrLf
sConsulta = sConsulta & "                             INSERT INTO PLANTILLA_CONF_VISTAS_GRUPO_ATRIB (PLANT,GRUPO,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                    SELECT @PLANT,@GRUPO,VISTA,@ID FROM  PLANTILLA_CONF_VISTAS_GRUPO WHERE PLANT=@PLANT AND GRUPO=@GRUPO " & vbCrLf
sConsulta = sConsulta & "                          end" & vbCrLf
sConsulta = sConsulta & "                      ELSE" & vbCrLf
sConsulta = sConsulta & "                begin" & vbCrLf
sConsulta = sConsulta & "                             INSERT INTO PLANTILLA_CONF_VISTAS_GRUPO_ATRIB (PLANT,GRUPO,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                    SELECT @PLANT,GRUPO,VISTA,@ID FROM  PLANTILLA_CONF_VISTAS_GRUPO WHERE PLANT=@PLANT " & vbCrLf
sConsulta = sConsulta & "                          end" & vbCrLf
sConsulta = sConsulta & "                      INSERT INTO PLANTILLA_CONF_VISTAS_ALL_ATRIB (PLANT,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                             SELECT @PLANT,VISTA,@ID FROM PLANTILLA_CONF_VISTAS_ALL WHERE PLANT=@PLANT " & vbCrLf
sConsulta = sConsulta & "                      IF @VISTAS=1" & vbCrLf
sConsulta = sConsulta & "                       begin                       " & vbCrLf
sConsulta = sConsulta & "                         UPDATE PLANTILLA_CONF_VISTAS_PROCE_ATRIB SET POS=NULL,VISIBLE=0 WHERE PLANT=@PLANT" & vbCrLf
sConsulta = sConsulta & "                 UPDATE PLANTILLA_CONF_VISTAS_PROCE SET CONSUMIDO_POS=1, CONSUMIDO_WIDTH=850,CONSUMIDO_VISIBLE=1,IMP_POS=2, IMP_WIDTH=850, IMP_VISIBLE=0, ADJ_POS=3, ADJ_WIDTH=850," & vbCrLf
sConsulta = sConsulta & "               ADJ_VISIBLE=1, AHORR_ADJ_POS=5, AHORR_ADJ_WIDTH=850, AHORR_ADJ_VISIBLE=1, AHORR_ADJ_PORCEN_POS=4, AHORR_ADJ_PORCEN_WIDTH=850 ," & vbCrLf
sConsulta = sConsulta & "               AHORR_ADJ_PORCEN_VISIBLE=0, AHORR_OFE_POS=7, AHORR_OFE_WIDTH=850,AHORR_OFE_VISIBLE=1," & vbCrLf
sConsulta = sConsulta & "               AHORR_OFE_PORCEN_POS= 6, AHORR_OFE_PORCEN_WIDTH=850 ,AHORR_OFE_PORCEN_VISIBLE=0,GRUPO_WIDTH=2600 WHERE PLANT=@PLANT" & vbCrLf
sConsulta = sConsulta & "                       end" & vbCrLf
sConsulta = sConsulta & "                END--old es 2" & vbCrLf
sConsulta = sConsulta & "       END --@ambito<>ambitoold" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ATR_CARGAR_PROCE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[ATR_CARGAR_PROCE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE ATR_CARGAR_PROCE  (@PROCE SMALLINT,@ANYO AS INTEGER,@GMN1 AS VARCHAR(50), @ALL AS TINYINT,@GRUPO VARCHAR(50)=NULL,@ORDENADO SMALLINT=NULL)  AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @NUMORD  VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & " if  @ORDENADO is null " & vbCrLf
sConsulta = sConsulta & "     set @NUMORD='4,29'" & vbCrLf
sConsulta = sConsulta & "else" & vbCrLf
sConsulta = sConsulta & "     if  @ORDENADO=0" & vbCrLf
sConsulta = sConsulta & "      set  @NUMORD=11" & vbCrLf
sConsulta = sConsulta & "     else" & vbCrLf
sConsulta = sConsulta & "        if  @ORDENADO=1" & vbCrLf
sConsulta = sConsulta & "               set  @NUMORD=25" & vbCrLf
sConsulta = sConsulta & "        else" & vbCrLf
sConsulta = sConsulta & "        if  @ORDENADO=2" & vbCrLf
sConsulta = sConsulta & "            set @NUMORD=26" & vbCrLf
sConsulta = sConsulta & "       else" & vbCrLf
sConsulta = sConsulta & "           if  @ORDENADO=3" & vbCrLf
sConsulta = sConsulta & "                set @NUMORD=24" & vbCrLf
sConsulta = sConsulta & "          else" & vbCrLf
sConsulta = sConsulta & "                if  @ORDENADO=5" & vbCrLf
sConsulta = sConsulta & "                   set   @NUMORD=5" & vbCrLf
sConsulta = sConsulta & "               else" & vbCrLf
sConsulta = sConsulta & "                   if  @ORDENADO=6" & vbCrLf
sConsulta = sConsulta & "                      set   @NUMORD='6,8'" & vbCrLf
sConsulta = sConsulta & "                   else" & vbCrLf
sConsulta = sConsulta & "                        if  @ORDENADO=7" & vbCrLf
sConsulta = sConsulta & "                             set   @NUMORD='7,9'" & vbCrLf
sConsulta = sConsulta & "                       else" & vbCrLf
sConsulta = sConsulta & "                             if  @ORDENADO=8" & vbCrLf
sConsulta = sConsulta & "                                set @NUMORD=5" & vbCrLf
sConsulta = sConsulta & "                             else" & vbCrLf
sConsulta = sConsulta & "                                  if  @ORDENADO=10" & vbCrLf
sConsulta = sConsulta & "                                      set @NUMORD=12" & vbCrLf
sConsulta = sConsulta & "                                  else" & vbCrLf
sConsulta = sConsulta & "                                       if  @ORDENADO=11" & vbCrLf
sConsulta = sConsulta & "                                          set @NUMORD=13" & vbCrLf
sConsulta = sConsulta & "                                       else" & vbCrLf
sConsulta = sConsulta & "                                           if  @ORDENADO=12" & vbCrLf
sConsulta = sConsulta & "                                               set @NUMORD=10" & vbCrLf
sConsulta = sConsulta & "                                           else" & vbCrLf
sConsulta = sConsulta & "                                                if  @ORDENADO=13" & vbCrLf
sConsulta = sConsulta & "                                                    set @NUMORD=14" & vbCrLf
sConsulta = sConsulta & "                                                else" & vbCrLf
sConsulta = sConsulta & "                                                     if  @ORDENADO=14" & vbCrLf
sConsulta = sConsulta & "                                                        set  @NUMORD=15" & vbCrLf
sConsulta = sConsulta & "                                                     else" & vbCrLf
sConsulta = sConsulta & "                                                         if  @ORDENADO=15" & vbCrLf
sConsulta = sConsulta & "                                                             set @NUMORD=16" & vbCrLf
sConsulta = sConsulta & "                                                        else" & vbCrLf
sConsulta = sConsulta & "                                                             set @NUMORD=1" & vbCrLf
sConsulta = sConsulta & "IF @GRUPO IS NULL" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=' SELECT PROCE_ATRIB.ID,PROCE_ATRIB.ATRIB,PROCE_ATRIB.PROCE,PROCE_ATRIB.GRUPO,PROCE_ATRIB.INTRO,PROCE_ATRIB.MINNUM,PROCE_ATRIB.MAXNUM,PROCE_ATRIB.MINFEC,PROCE_ATRIB.MAXFEC,PROCE_ATRIB.PREFBAJO,' " & vbCrLf
sConsulta = sConsulta & "         + '  PROCE_ATRIB.INTERNO,PROCE_ATRIB.AMBITO,PROCE_ATRIB.OBLIG,PROCE_ATRIB.OP_PREC,PROCE_ATRIB.APLIC_PREC,PROCE_ATRIB.DEF_PREC, PROCE_ATRIB.POND,PROCE_ATRIB.OP_POND,PROCE_ATRIB.NUM_POND,PROCE_ATRIB.POND_SI,PROCE_ATRIB.POND_NO,' " & vbCrLf
sConsulta = sConsulta & "        + '  PROCE_ATRIB.GMN1,PROCE_ATRIB.ANYO,DEF_ATRIB.TIPO,DEF_ATRIB.COD,DEF_ATRIB.DEN,PROCE_ATRIB.FECACT,PROCE_ATRIB.ORDEN,PROCE_ATRIB.ORDEN_ATRIB, DEF_ATRIB.DESCR' " & vbCrLf
sConsulta = sConsulta & "         + '  FROM PROCE_ATRIB INNER JOIN DEF_ATRIB ON PROCE_ATRIB.ATRIB=DEF_ATRIB.ID' " & vbCrLf
sConsulta = sConsulta & "         + '  WHERE PROCE_ATRIB.PROCE=' +  CAST(@PROCE AS VARCHAR) + '  AND  PROCE_ATRIB.GRUPO IS NULL AND  PROCE_ATRIB.ANYO=' +  CAST(@ANYO AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "         + ' AND PROCE_ATRIB.GMN1= '''  + @GMN1 + ''' ORDER BY ' + CAST( @NUMORD AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "      IF  @ALL = 1" & vbCrLf
sConsulta = sConsulta & "                    SET @SQL=' SELECT PROCE_ATRIB.ID,PROCE_ATRIB.ATRIB,PROCE_ATRIB.PROCE,PROCE_ATRIB.GRUPO,PROCE_ATRIB.INTRO,PROCE_ATRIB.MINNUM,PROCE_ATRIB.MAXNUM,PROCE_ATRIB.MINFEC,PROCE_ATRIB.MAXFEC,PROCE_ATRIB.PREFBAJO,' " & vbCrLf
sConsulta = sConsulta & "                        + '  PROCE_ATRIB.INTERNO,PROCE_ATRIB.AMBITO,PROCE_ATRIB.OBLIG,PROCE_ATRIB.OP_PREC,PROCE_ATRIB.APLIC_PREC,PROCE_ATRIB.DEF_PREC, PROCE_ATRIB.POND,PROCE_ATRIB.OP_POND,PROCE_ATRIB.NUM_POND,PROCE_ATRIB.POND_SI,PROCE_ATRIB.POND_NO,' " & vbCrLf
sConsulta = sConsulta & "                        + '  PROCE_ATRIB.GMN1,PROCE_ATRIB.ANYO,DEF_ATRIB.TIPO,DEF_ATRIB.COD,DEF_ATRIB.DEN,PROCE_ATRIB.FECACT,PROCE_ATRIB.ORDEN,PROCE_ATRIB.ORDEN_ATRIB, DEF_ATRIB.DESCR' " & vbCrLf
sConsulta = sConsulta & "                        + '  FROM PROCE_ATRIB INNER JOIN DEF_ATRIB ON PROCE_ATRIB.ATRIB=DEF_ATRIB.ID' " & vbCrLf
sConsulta = sConsulta & "                        + '  WHERE PROCE_ATRIB.PROCE=' +  CAST(@PROCE AS VARCHAR) + '  AND  PROCE_ATRIB.ANYO=' +  CAST(@ANYO AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                        + ' AND PROCE_ATRIB.GMN1= '''  + @GMN1 + ''' ORDER BY ' + CAST( @NUMORD AS VARCHAR)    " & vbCrLf
sConsulta = sConsulta & "       ELSE" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "             SET @SQL=' SELECT PROCE_ATRIB.ID,PROCE_ATRIB.ATRIB,PROCE_ATRIB.PROCE,PROCE_ATRIB.GRUPO,PROCE_ATRIB.INTRO,PROCE_ATRIB.MINNUM,PROCE_ATRIB.MAXNUM,PROCE_ATRIB.MINFEC,PROCE_ATRIB.MAXFEC,PROCE_ATRIB.PREFBAJO,' " & vbCrLf
sConsulta = sConsulta & "                        + '  PROCE_ATRIB.INTERNO,PROCE_ATRIB.AMBITO,PROCE_ATRIB.OBLIG,PROCE_ATRIB.OP_PREC,PROCE_ATRIB.APLIC_PREC,PROCE_ATRIB.DEF_PREC, PROCE_ATRIB.POND,PROCE_ATRIB.OP_POND,PROCE_ATRIB.NUM_POND,PROCE_ATRIB.POND_SI,PROCE_ATRIB.POND_NO,' " & vbCrLf
sConsulta = sConsulta & "                        + '  PROCE_ATRIB.GMN1,PROCE_ATRIB.ANYO,DEF_ATRIB.TIPO,DEF_ATRIB.COD,DEF_ATRIB.DEN,PROCE_ATRIB.FECACT,PROCE_ATRIB.ORDEN,PROCE_ATRIB.ORDEN_ATRIB, DEF_ATRIB.DESCR' " & vbCrLf
sConsulta = sConsulta & "                        + '  FROM PROCE_ATRIB INNER JOIN DEF_ATRIB ON PROCE_ATRIB.ATRIB=DEF_ATRIB.ID' " & vbCrLf
sConsulta = sConsulta & "                        + '  WHERE PROCE_ATRIB.PROCE=' +  CAST(@PROCE AS VARCHAR) + '  AND  PROCE_ATRIB.ANYO=' +  CAST(@ANYO AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                        + ' AND PROCE_ATRIB.GMN1= '''  + @GMN1 + '''  AND  PROCE_ATRIB.GRUPO=''' +@GRUPO+ ''' ORDER BY ' + CAST( @NUMORD AS VARCHAR)   " & vbCrLf
sConsulta = sConsulta & " EXEC SP_EXECUTESQL @STMT=@SQL" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_DEVOLVER_PROCE_ATRIB]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_DEVOLVER_PROCE_ATRIB]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_DEVOLVER_PROCE_ATRIB @ANYO smallint, @GMN1 varchar(50), @PROCE smallint, @AMBITO tinyint, @GRUPO varchar(50)=null, @CONLISTA tinyint = 1, @INTERNO tinyint= null, @ATRIB int = null AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CONLISTA = 1" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "if @AMBITO = 1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT PA.ATRIB, PA.ID, DA.COD, DA.DEN, PA.INTRO, DA.TIPO, PA.AMBITO," & vbCrLf
sConsulta = sConsulta & "       PA.MINNUM, PA.MINFEC," & vbCrLf
sConsulta = sConsulta & "       PA.MAXNUM, PA.MAXFEC," & vbCrLf
sConsulta = sConsulta & "       DAL.VALOR_TEXT DALVALOR_TEXT," & vbCrLf
sConsulta = sConsulta & "       DAL.VALOR_NUM DALVALOR_NUM," & vbCrLf
sConsulta = sConsulta & "       DAL.VALOR_FEC DALVALOR_FEC," & vbCrLf
sConsulta = sConsulta & "       PA.INTERNO,PA.POND,PA.DEF_PREC,PA.PREFBAJO," & vbCrLf
sConsulta = sConsulta & "       PA.OBLIG,PA.APLIC_PREC,PA.OP_PREC,PA.GRUPO,PA.USAR_PREC,PA.ORDEN," & vbCrLf
sConsulta = sConsulta & "       PA.OP_POND,PA.NUM_POND,PA.POND_SI,PA.POND_NO, DA.DESCR" & vbCrLf
sConsulta = sConsulta & " FROM PROCE_ATRIB PA" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN DEF_ATRIB DA" & vbCrLf
sConsulta = sConsulta & "               ON PA.ATRIB = DA.ID" & vbCrLf
sConsulta = sConsulta & "        LEFT JOIN PROCE_ATRIB_LISTA DAL" & vbCrLf
sConsulta = sConsulta & "               ON PA.ID = DAL.ATRIB_ID" & vbCrLf
sConsulta = sConsulta & "WHERE PA.ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "  AND PA.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "  AND PA.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "  AND PA.AMBITO = 1" & vbCrLf
sConsulta = sConsulta & "  AND PA.INTERNO = CASE WHEN @INTERNO IS NULL THEN PA.INTERNO ELSE @INTERNO END" & vbCrLf
sConsulta = sConsulta & "  AND DA.ID = CASE WHEN @ATRIB IS NULL THEN DA.ID ELSE @ATRIB END" & vbCrLf
sConsulta = sConsulta & "ORDER BY PA.GRUPO,PA.ORDEN_ATRIB" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ELSE IF @AMBITO = 2" & vbCrLf
sConsulta = sConsulta & "  IF @GRUPO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "    SELECT PA.ATRIB, PA.ID, DA.COD, DA.DEN, PA.INTRO, DA.TIPO, PA.AMBITO," & vbCrLf
sConsulta = sConsulta & "           PA.MINNUM, PA.MINFEC," & vbCrLf
sConsulta = sConsulta & "           PA.MAXNUM, PA.MAXFEC," & vbCrLf
sConsulta = sConsulta & "           DAL.VALOR_TEXT DALVALOR_TEXT," & vbCrLf
sConsulta = sConsulta & "           DAL.VALOR_NUM DALVALOR_NUM," & vbCrLf
sConsulta = sConsulta & "           DAL.VALOR_FEC DALVALOR_FEC,   " & vbCrLf
sConsulta = sConsulta & "           PA.INTERNO,PA.POND,PA.DEF_PREC,PA.PREFBAJO," & vbCrLf
sConsulta = sConsulta & "           PA.OBLIG,PA.APLIC_PREC,PA.OP_PREC,PA.GRUPO,PA.USAR_PREC,PA.ORDEN," & vbCrLf
sConsulta = sConsulta & "           PA.OP_POND,PA.NUM_POND,PA.POND_SI,PA.POND_NO, DA.DESCR" & vbCrLf
sConsulta = sConsulta & "     FROM PROCE_ATRIB PA" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN DEF_ATRIB DA" & vbCrLf
sConsulta = sConsulta & "                   ON PA.ATRIB = DA.ID" & vbCrLf
sConsulta = sConsulta & "            LEFT JOIN PROCE_ATRIB_LISTA DAL" & vbCrLf
sConsulta = sConsulta & "                   ON PA.ID = DAL.ATRIB_ID" & vbCrLf
sConsulta = sConsulta & "    WHERE PA.ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "      AND PA.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "      AND PA.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "      AND ISNULL(PA.GRUPO,'#') = CASE WHEN PA.GRUPO IS NULL THEN ISNULL(PA.GRUPO,'#') ELSE @GRUPO END" & vbCrLf
sConsulta = sConsulta & "     AND PA.AMBITO = 2" & vbCrLf
sConsulta = sConsulta & "     AND PA.INTERNO = CASE WHEN @INTERNO IS NULL THEN PA.INTERNO ELSE @INTERNO END" & vbCrLf
sConsulta = sConsulta & "     AND DA.ID = CASE WHEN @ATRIB IS NULL THEN DA.ID ELSE @ATRIB END" & vbCrLf
sConsulta = sConsulta & "    ORDER BY PA.GRUPO,PA.ORDEN_ATRIB" & vbCrLf
sConsulta = sConsulta & "  ELSE" & vbCrLf
sConsulta = sConsulta & "    SELECT PA.ATRIB, PA.ID, DA.COD, DA.DEN, PA.INTRO, DA.TIPO, PA.AMBITO," & vbCrLf
sConsulta = sConsulta & "           PA.MINNUM, PA.MINFEC," & vbCrLf
sConsulta = sConsulta & "           PA.MAXNUM, PA.MAXFEC," & vbCrLf
sConsulta = sConsulta & "           DAL.VALOR_TEXT DALVALOR_TEXT," & vbCrLf
sConsulta = sConsulta & "           DAL.VALOR_NUM DALVALOR_NUM," & vbCrLf
sConsulta = sConsulta & "           DAL.VALOR_FEC DALVALOR_FEC,   " & vbCrLf
sConsulta = sConsulta & "           PA.INTERNO,PA.POND,PA.DEF_PREC,PA.PREFBAJO," & vbCrLf
sConsulta = sConsulta & "           PA.OBLIG,PA.APLIC_PREC,PA.OP_PREC,PA.GRUPO,PA.USAR_PREC,PA.ORDEN," & vbCrLf
sConsulta = sConsulta & "           PA.OP_POND,PA.NUM_POND,PA.POND_SI,PA.POND_NO, DA.DESCR" & vbCrLf
sConsulta = sConsulta & "     FROM PROCE_ATRIB PA" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN DEF_ATRIB DA" & vbCrLf
sConsulta = sConsulta & "                   ON PA.ATRIB = DA.ID" & vbCrLf
sConsulta = sConsulta & "            LEFT JOIN PROCE_ATRIB_LISTA DAL" & vbCrLf
sConsulta = sConsulta & "                   ON PA.ID = DAL.ATRIB_ID" & vbCrLf
sConsulta = sConsulta & "    WHERE PA.ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "      AND PA.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "      AND PA.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "      AND PA.AMBITO = 2" & vbCrLf
sConsulta = sConsulta & "      AND PA.INTERNO = CASE WHEN @INTERNO IS NULL THEN PA.INTERNO ELSE @INTERNO END" & vbCrLf
sConsulta = sConsulta & "      AND DA.ID = CASE WHEN @ATRIB IS NULL THEN DA.ID ELSE @ATRIB END" & vbCrLf
sConsulta = sConsulta & "    ORDER BY PA.GRUPO,PA.ORDEN_ATRIB" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ELSE IF @AMBITO = 3" & vbCrLf
sConsulta = sConsulta & "  IF @GRUPO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "    SELECT PA.ATRIB, PA.ID, DA.COD, DA.DEN, PA.INTRO, DA.TIPO,  PA.AMBITO," & vbCrLf
sConsulta = sConsulta & "           PA.MINNUM, PA.MINFEC," & vbCrLf
sConsulta = sConsulta & "           PA.MAXNUM, PA.MAXFEC," & vbCrLf
sConsulta = sConsulta & "           DAL.VALOR_TEXT DALVALOR_TEXT," & vbCrLf
sConsulta = sConsulta & "           DAL.VALOR_NUM DALVALOR_NUM," & vbCrLf
sConsulta = sConsulta & "           DAL.VALOR_FEC DALVALOR_FEC," & vbCrLf
sConsulta = sConsulta & "           PA.INTERNO,PA.POND,PA.DEF_PREC,PA.PREFBAJO," & vbCrLf
sConsulta = sConsulta & "           PA.OBLIG,PA.APLIC_PREC,PA.OP_PREC,PA.GRUPO,PA.USAR_PREC,PA.ORDEN," & vbCrLf
sConsulta = sConsulta & "           PA.OP_POND,PA.NUM_POND,PA.POND_SI,PA.POND_NO, DA.DESCR" & vbCrLf
sConsulta = sConsulta & "     FROM PROCE_ATRIB PA" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN DEF_ATRIB DA" & vbCrLf
sConsulta = sConsulta & "                   ON PA.ATRIB = DA.ID" & vbCrLf
sConsulta = sConsulta & "            LEFT JOIN PROCE_ATRIB_LISTA DAL" & vbCrLf
sConsulta = sConsulta & "                   ON PA.ID = DAL.ATRIB_ID" & vbCrLf
sConsulta = sConsulta & "    WHERE PA.ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "      AND PA.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "      AND PA.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "      AND ISNULL(PA.GRUPO,'#') = CASE WHEN PA.GRUPO IS NULL THEN ISNULL(PA.GRUPO,'#') ELSE @GRUPO END" & vbCrLf
sConsulta = sConsulta & "      AND PA.AMBITO = 3" & vbCrLf
sConsulta = sConsulta & "      AND PA.INTERNO = CASE WHEN @INTERNO IS NULL THEN PA.INTERNO ELSE @INTERNO END" & vbCrLf
sConsulta = sConsulta & "      AND DA.ID = CASE WHEN @ATRIB IS NULL THEN DA.ID ELSE @ATRIB END" & vbCrLf
sConsulta = sConsulta & "     ORDER BY PA.GRUPO,PA.ORDEN_ATRIB" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  ELSE" & vbCrLf
sConsulta = sConsulta & "   SELECT PA.ATRIB, PA.ID, DA.COD, DA.DEN, PA.INTRO, DA.TIPO,  PA.AMBITO," & vbCrLf
sConsulta = sConsulta & "                PA.MINNUM, PA.MINFEC," & vbCrLf
sConsulta = sConsulta & "                PA.MAXNUM, PA.MAXFEC," & vbCrLf
sConsulta = sConsulta & "                NULL DALVALOR_TEXT," & vbCrLf
sConsulta = sConsulta & "                NULL DALVALOR_NUM," & vbCrLf
sConsulta = sConsulta & "                NULL DALVALOR_FEC," & vbCrLf
sConsulta = sConsulta & "                PA.INTERNO,PA.POND,PA.DEF_PREC,PA.PREFBAJO," & vbCrLf
sConsulta = sConsulta & "                PA.OBLIG,PA.APLIC_PREC,PA.OP_PREC,PA.GRUPO,PA.USAR_PREC,PA.ORDEN," & vbCrLf
sConsulta = sConsulta & "                PA.OP_POND,PA.NUM_POND,PA.POND_SI,PA.POND_NO, DA.DESCR" & vbCrLf
sConsulta = sConsulta & "       FROM PROCE_ATRIB PA" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN DEF_ATRIB DA" & vbCrLf
sConsulta = sConsulta & "                   ON PA.ATRIB = DA.ID" & vbCrLf
sConsulta = sConsulta & "       WHERE PA.ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "         AND PA.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "         AND PA.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "         AND PA.AMBITO = 3" & vbCrLf
sConsulta = sConsulta & "               AND PA.INTERNO = CASE WHEN @INTERNO IS NULL THEN PA.INTERNO ELSE @INTERNO END" & vbCrLf
sConsulta = sConsulta & "               AND DA.ID = CASE WHEN @ATRIB IS NULL THEN DA.ID ELSE @ATRIB END" & vbCrLf
sConsulta = sConsulta & "          ORDER BY PA.GRUPO,PA.ORDEN_ATRIB" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "if @AMBITO = 1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT PA.ATRIB, PA.ID, DA.COD, DA.DEN, PA.INTRO, DA.TIPO,  PA.AMBITO," & vbCrLf
sConsulta = sConsulta & "       PA.MINNUM, PA.MINFEC," & vbCrLf
sConsulta = sConsulta & "       PA.MAXNUM, PA.MAXFEC," & vbCrLf
sConsulta = sConsulta & "       NULL DALVALOR_TEXT," & vbCrLf
sConsulta = sConsulta & "       NULL DALVALOR_NUM," & vbCrLf
sConsulta = sConsulta & "       NULL DALVALOR_FEC," & vbCrLf
sConsulta = sConsulta & "       PA.INTERNO,PA.POND,PA.DEF_PREC,PA.PREFBAJO," & vbCrLf
sConsulta = sConsulta & "       PA.OBLIG,PA.APLIC_PREC,PA.OP_PREC,PA.GRUPO,PA.USAR_PREC,PA.ORDEN," & vbCrLf
sConsulta = sConsulta & "       PA.OP_POND,PA.NUM_POND,PA.POND_SI,PA.POND_NO, DA.DESCR" & vbCrLf
sConsulta = sConsulta & " FROM PROCE_ATRIB PA" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN DEF_ATRIB DA" & vbCrLf
sConsulta = sConsulta & "               ON PA.ATRIB = DA.ID" & vbCrLf
sConsulta = sConsulta & "WHERE PA.ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "  AND PA.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "  AND PA.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "  AND PA.AMBITO = 1" & vbCrLf
sConsulta = sConsulta & "  AND PA.INTERNO = CASE WHEN @INTERNO IS NULL THEN PA.INTERNO ELSE @INTERNO END" & vbCrLf
sConsulta = sConsulta & "  AND DA.ID = CASE WHEN @ATRIB IS NULL THEN DA.ID ELSE @ATRIB END" & vbCrLf
sConsulta = sConsulta & "ORDER BY PA.GRUPO,PA.ORDEN_ATRIB" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ELSE IF @AMBITO = 2" & vbCrLf
sConsulta = sConsulta & "  IF @GRUPO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "    SELECT PA.ATRIB, PA.ID, DA.COD, DA.DEN, PA.INTRO, DA.TIPO,  PA.AMBITO," & vbCrLf
sConsulta = sConsulta & "           PA.MINNUM, PA.MINFEC," & vbCrLf
sConsulta = sConsulta & "           PA.MAXNUM, PA.MAXFEC," & vbCrLf
sConsulta = sConsulta & "           NULL DALVALOR_TEXT," & vbCrLf
sConsulta = sConsulta & "           NULL DALVALOR_NUM," & vbCrLf
sConsulta = sConsulta & "           NULL DALVALOR_FEC,   " & vbCrLf
sConsulta = sConsulta & "           PA.INTERNO,PA.POND,PA.DEF_PREC,PA.PREFBAJO," & vbCrLf
sConsulta = sConsulta & "           PA.OBLIG,PA.APLIC_PREC,PA.OP_PREC,PA.GRUPO,PA.USAR_PREC,PA.ORDEN," & vbCrLf
sConsulta = sConsulta & "           PA.OP_POND,PA.NUM_POND,PA.POND_SI,PA.POND_NO, DA.DESCR" & vbCrLf
sConsulta = sConsulta & "     FROM PROCE_ATRIB PA" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN DEF_ATRIB DA" & vbCrLf
sConsulta = sConsulta & "                   ON PA.ATRIB = DA.ID" & vbCrLf
sConsulta = sConsulta & "    WHERE PA.ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "      AND PA.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "      AND PA.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "      AND ISNULL(PA.GRUPO,'#') = CASE WHEN PA.GRUPO IS NULL THEN ISNULL(PA.GRUPO,'#') ELSE @GRUPO END" & vbCrLf
sConsulta = sConsulta & "      AND PA.AMBITO = 2" & vbCrLf
sConsulta = sConsulta & "      AND PA.INTERNO = CASE WHEN @INTERNO IS NULL THEN PA.INTERNO ELSE @INTERNO END" & vbCrLf
sConsulta = sConsulta & "      AND DA.ID = CASE WHEN @ATRIB IS NULL THEN DA.ID ELSE @ATRIB END" & vbCrLf
sConsulta = sConsulta & "    ORDER BY PA.GRUPO,PA.ORDEN_ATRIB" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  ELSE" & vbCrLf
sConsulta = sConsulta & "    SELECT PA.ATRIB, PA.ID, DA.COD, DA.DEN, PA.INTRO, DA.TIPO,  PA.AMBITO," & vbCrLf
sConsulta = sConsulta & "           PA.MINNUM, PA.MINFEC," & vbCrLf
sConsulta = sConsulta & "           PA.MAXNUM, PA.MAXFEC," & vbCrLf
sConsulta = sConsulta & "           NULL DALVALOR_TEXT," & vbCrLf
sConsulta = sConsulta & "           NULL DALVALOR_NUM," & vbCrLf
sConsulta = sConsulta & "           NULL DALVALOR_FEC,   " & vbCrLf
sConsulta = sConsulta & "           PA.INTERNO,PA.POND,PA.DEF_PREC,PA.PREFBAJO," & vbCrLf
sConsulta = sConsulta & "           PA.OBLIG,PA.APLIC_PREC,PA.OP_PREC,PA.GRUPO,PA.USAR_PREC,PA.ORDEN," & vbCrLf
sConsulta = sConsulta & "           PA.OP_POND,PA.NUM_POND,PA.POND_SI,PA.POND_NO, DA.DESCR" & vbCrLf
sConsulta = sConsulta & "     FROM PROCE_ATRIB PA" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN DEF_ATRIB DA" & vbCrLf
sConsulta = sConsulta & "                   ON PA.ATRIB = DA.ID" & vbCrLf
sConsulta = sConsulta & "    WHERE PA.ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "      AND PA.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "      AND PA.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "      AND PA.AMBITO = 2" & vbCrLf
sConsulta = sConsulta & "      AND PA.INTERNO = CASE WHEN @INTERNO IS NULL THEN PA.INTERNO ELSE @INTERNO END" & vbCrLf
sConsulta = sConsulta & "      AND DA.ID = CASE WHEN @ATRIB IS NULL THEN DA.ID ELSE @ATRIB END" & vbCrLf
sConsulta = sConsulta & "    ORDER BY PA.GRUPO,PA.ORDEN_ATRIB" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "ELSE IF @AMBITO = 3" & vbCrLf
sConsulta = sConsulta & "  IF @GRUPO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "    SELECT PA.ATRIB, PA.ID, DA.COD, DA.DEN, PA.INTRO, DA.TIPO,  PA.AMBITO," & vbCrLf
sConsulta = sConsulta & "           PA.MINNUM, PA.MINFEC," & vbCrLf
sConsulta = sConsulta & "           PA.MAXNUM, PA.MAXFEC," & vbCrLf
sConsulta = sConsulta & "           NULL DALVALOR_TEXT," & vbCrLf
sConsulta = sConsulta & "           NULL DALVALOR_NUM," & vbCrLf
sConsulta = sConsulta & "           NULL DALVALOR_FEC," & vbCrLf
sConsulta = sConsulta & "           PA.INTERNO,PA.POND,PA.DEF_PREC,PA.PREFBAJO," & vbCrLf
sConsulta = sConsulta & "           PA.OBLIG,PA.APLIC_PREC,PA.OP_PREC,PA.GRUPO,PA.USAR_PREC,PA.ORDEN," & vbCrLf
sConsulta = sConsulta & "           PA.OP_POND,PA.NUM_POND,PA.POND_SI,PA.POND_NO, DA.DESCR" & vbCrLf
sConsulta = sConsulta & "     FROM PROCE_ATRIB PA" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN DEF_ATRIB DA" & vbCrLf
sConsulta = sConsulta & "                   ON PA.ATRIB = DA.ID" & vbCrLf
sConsulta = sConsulta & "    WHERE PA.ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "      AND PA.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "      AND PA.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "      AND ISNULL(PA.GRUPO,'#') = CASE WHEN PA.GRUPO IS NULL THEN ISNULL(PA.GRUPO,'#') ELSE @GRUPO END" & vbCrLf
sConsulta = sConsulta & "      AND PA.AMBITO = 3" & vbCrLf
sConsulta = sConsulta & "      AND PA.INTERNO = CASE WHEN @INTERNO IS NULL THEN PA.INTERNO ELSE @INTERNO END" & vbCrLf
sConsulta = sConsulta & "      AND DA.ID = CASE WHEN @ATRIB IS NULL THEN DA.ID ELSE @ATRIB END" & vbCrLf
sConsulta = sConsulta & "    ORDER BY PA.GRUPO,PA.ORDEN_ATRIB" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "   SELECT PA.ATRIB, PA.ID, DA.COD, DA.DEN, PA.INTRO, DA.TIPO,  PA.AMBITO," & vbCrLf
sConsulta = sConsulta & "                PA.MINNUM, PA.MINFEC," & vbCrLf
sConsulta = sConsulta & "                PA.MAXNUM, PA.MAXFEC," & vbCrLf
sConsulta = sConsulta & "                NULL DALVALOR_TEXT," & vbCrLf
sConsulta = sConsulta & "                NULL DALVALOR_NUM," & vbCrLf
sConsulta = sConsulta & "                NULL DALVALOR_FEC," & vbCrLf
sConsulta = sConsulta & "                PA.INTERNO,PA.POND,PA.DEF_PREC,PA.PREFBAJO," & vbCrLf
sConsulta = sConsulta & "                PA.OBLIG,PA.APLIC_PREC,PA.OP_PREC,PA.GRUPO,PA.USAR_PREC,PA.ORDEN," & vbCrLf
sConsulta = sConsulta & "                PA.OP_POND,PA.NUM_POND,PA.POND_SI,PA.POND_NO, DA.DESCR" & vbCrLf
sConsulta = sConsulta & "       FROM PROCE_ATRIB PA" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN DEF_ATRIB DA" & vbCrLf
sConsulta = sConsulta & "                   ON PA.ATRIB = DA.ID" & vbCrLf
sConsulta = sConsulta & "       WHERE PA.ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "         AND PA.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "         AND PA.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "         AND PA.AMBITO = 3" & vbCrLf
sConsulta = sConsulta & "               AND PA.INTERNO = CASE WHEN @INTERNO IS NULL THEN PA.INTERNO ELSE @INTERNO END" & vbCrLf
sConsulta = sConsulta & "               AND DA.ID = CASE WHEN @ATRIB IS NULL THEN DA.ID ELSE @ATRIB END" & vbCrLf
sConsulta = sConsulta & "           ORDER BY PA.GRUPO,PA.ORDEN_ATRIB" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion2_14_00_09A2_14_00_10() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim sFSP As String
Dim oADOConnection As ADODB.Connection
Dim adores As ADODB.Recordset
Dim cm As ADODB.Command
Dim par As ADODB.Parameter
Dim iResult As Long
Dim bBool As Boolean
Dim bConPortal As Boolean

On Error GoTo error
    

    Set oADOConnection = New ADODB.Connection
    oADOConnection.Open "Provider=SQLOLEDB.1;Auto Translate=FALSE;Server=" & gServer & ";Database=" & gBD & ";", gUID, gPWD
    oADOConnection.CursorLocation = adUseClient
    oADOConnection.CommandTimeout = 120

    Set adores = New ADODB.Recordset
    adores.Open "SELECT INSTWEB FROM PARGEN_INTERNO", oADOConnection, adOpenForwardOnly, adLockReadOnly
    If adores(0).Value = 2 Then
        bConPortal = True
    Else
        bConPortal = False
    End If
    adores.Close
    
    If bConPortal Then
        
        V_2_14_Storeds_10
        
        oADOConnection.Execute "BEGIN DISTRIBUTED TRAN"
        oADOConnection.Execute "SET XACT_ABORT ON"
        bTransaccionEnCurso = True
    
        'Ejecutamos la estored que actuliza PROVE_ESP_PORTAL.DATASIZE
        Set cm = New ADODB.Command
        Set cm.ActiveConnection = oADOConnection
        
        cm.CommandType = adCmdStoredProc
        cm.CommandText = "SP_PROVE_ESP_PORTAL_TEMP"
    
        cm.Execute
        
        If oADOConnection.Errors.Count > 0 Then GoTo error
        Set cm = Nothing
        
        sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_PROVE_ESP_PORTAL_TEMP]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
        sConsulta = sConsulta & "drop procedure [dbo].[SP_PROVE_ESP_PORTAL_TEMP]" & vbCrLf
        ExecuteSQL gRDOCon, sConsulta
        
        sConsulta = "UPDATE ART4_ESP SET DATASIZE =datalength(DATA)"
        oADOConnection.Execute sConsulta
        
        sConsulta = "UPDATE VERSION SET NUM ='2.14.00.10'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
        oADOConnection.Execute sConsulta
    
        oADOConnection.Execute "COMMIT TRAN"
        oADOConnection.Execute "SET XACT_ABORT OFF"
        bTransaccionEnCurso = True
        CodigoDeActualizacion2_14_00_09A2_14_00_10 = True
    Else
    
        oADOConnection.Execute "BEGIN TRANSACTION"
        oADOConnection.Execute "SET XACT_ABORT ON"
        bTransaccionEnCurso = True
            
        sConsulta = "UPDATE ART4_ESP SET DATASIZE =datalength(DATA)"
        oADOConnection.Execute sConsulta
        
        sConsulta = "UPDATE VERSION SET NUM ='2.14.00.10'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
        oADOConnection.Execute sConsulta
    
        oADOConnection.Execute "COMMIT TRANSACTION"
        oADOConnection.Execute "SET XACT_ABORT OFF"
        bTransaccionEnCurso = True
        CodigoDeActualizacion2_14_00_09A2_14_00_10 = True
    End If
    Set adores = Nothing
    
TERMINAR:
On Error Resume Next
    If Not adores Is Nothing Then
        adores.Close
        Set adores = Nothing
    End If
    oADOConnection.Close
    Set oADOConnection = Nothing



Exit Function
    
error:
    
    MsgBox oADOConnection.Errors(0).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_14_00_09A2_14_00_10 = False
    
    Resume TERMINAR
End Function

Private Sub V_2_14_Storeds_10()
Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_PROVE_ESP_PORTAL_TEMP]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_PROVE_ESP_PORTAL_TEMP]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_PROVE_ESP_PORTAL_TEMP  AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVE AS VARCHAR(50),@ID AS INT,@FSP_COD AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SIZE AS FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PAR AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SERVER AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @BD AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @SERVER=FSP_SRV,@BD=FSP_BD FROM PARGEN_PORT WHERE ID=1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE C_Cal CURSOR LOCAL FOR  SELECT PROVE_ESP_PORTAL.PROVE,PROVE_ESP_PORTAL.ID,PROVE.FSP_COD" & vbCrLf
sConsulta = sConsulta & "FROM PROVE_ESP_PORTAL INNER JOIN  PROVE ON PROVE_ESP_PORTAL.PROVE=PROVE.COD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "OPEN C_Cal" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C_Cal INTO @PROVE,@ID,@FSP_COD" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "     SET @SQL='SELECT @SIZ=datalength(CIAS_ESP.DATA)  FROM ' + @SERVER + '.' + @BD + '.dbo.CIAS AS CIAS INNER JOIN ' + @SERVER + '.' + @BD + " & vbCrLf
sConsulta = sConsulta & "        '.dbo.CIAS_ESP AS CIAS_ESP ON CIAS_ESP.CIA=CIAS.ID WHERE CIAS.COD=''' + @FSP_COD +'''  AND CIAS_ESP.ID=' + CAST(@ID AS VARCHAR(50))" & vbCrLf
sConsulta = sConsulta & "     SET @PAR='@SIZ FLOAT OUTPUT'" & vbCrLf
sConsulta = sConsulta & "     EXEC SP_EXECUTESQL @SQL, @PAR, @SIZ=@SIZE OUTPUT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "     UPDATE PROVE_ESP_PORTAL SET DATASIZE= @SIZE WHERE PROVE=@PROVE AND ID=@ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "      FETCH NEXT FROM C_Cal INTO @PROVE,@ID,@FSP_COD" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "CLOSE C_Cal " & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C_Cal" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


End Sub

Public Function CodigoDeActualizacion2_14_00_10A2_14_00_11() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim bBool As Boolean

On Error GoTo error
    

sConsulta = "BEGIN TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = True
ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

frmProgreso.lblDetalle = "Actualizamos tablas"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_14_Tablas_11

frmProgreso.lblDetalle = "Actualizamos storeds"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_14_Storeds_11

frmProgreso.lblDetalle = "Actualizamos triggers"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_14_Triggers_11

sConsulta = "UPDATE VERSION SET NUM ='2.14.00.11'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "COMMIT TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = False

CodigoDeActualizacion2_14_00_10A2_14_00_11 = True
Exit Function
    
error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_14_00_10A2_14_00_11 = False
End Function

Private Sub V_2_14_Tablas_11()
Dim sConsulta As String

sConsulta = "ALTER TABLE dbo.DEF_ATRIB ALTER COLUMN" & vbCrLf
sConsulta = sConsulta & "   DEN varchar(50) NOT NULL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta



End Sub
Private Sub V_2_14_Storeds_11()
Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ATR_CARGAR_PLANTILLA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[ATR_CARGAR_PLANTILLA]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE ATR_CARGAR_PLANTILLA  (@PLANTILLA INT,@GRUPO VARCHAR(50)=NULL)  AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @GRUPO IS NULL" & vbCrLf
sConsulta = sConsulta & "                    SELECT PLANTILLA_ATRIB.ID,PLANTILLA_ATRIB.ATRIB,PLANTILLA_ATRIB.PLANT,PLANTILLA_ATRIB.GRUPO,PLANTILLA_ATRIB.INTRO,PLANTILLA_ATRIB.MINNUM,PLANTILLA_ATRIB.MAXNUM,PLANTILLA_ATRIB.MINFEC,PLANTILLA_ATRIB.MAXFEC,PLANTILLA_ATRIB.PREFBAJO," & vbCrLf
sConsulta = sConsulta & "                            PLANTILLA_ATRIB.INTERNO,PLANTILLA_ATRIB.AMBITO,PLANTILLA_ATRIB.OBLIG,PLANTILLA_ATRIB.OP_PREC,PLANTILLA_ATRIB.APLIC_PREC,PLANTILLA_ATRIB.DEF_PREC," & vbCrLf
sConsulta = sConsulta & "                            PLANTILLA_ATRIB.POND,PLANTILLA_ATRIB.OP_POND,PLANTILLA_ATRIB.NUM_POND,PLANTILLA_ATRIB.POND_SI,PLANTILLA_ATRIB.POND_NO," & vbCrLf
sConsulta = sConsulta & "                            DEF_ATRIB.COD,DEF_ATRIB.DEN,DEF_ATRIB.TIPO,PLANTILLA_ATRIB.FECACT,PLANTILLA_ATRIB.ORDEN, DEF_ATRIB.DESCR" & vbCrLf
sConsulta = sConsulta & "                            FROM PLANTILLA_ATRIB INNER JOIN DEF_ATRIB ON PLANTILLA_ATRIB.ATRIB=DEF_ATRIB.ID" & vbCrLf
sConsulta = sConsulta & "                            WHERE PLANTILLA_ATRIB.PLANT=@PLANTILLA  AND  PLANTILLA_ATRIB.GRUPO IS NULL   ORDER BY PLANTILLA_ATRIB.ORDEN_ATRIB" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "                    SELECT PLANTILLA_ATRIB.ID,PLANTILLA_ATRIB.ATRIB,PLANTILLA_ATRIB.PLANT,PLANTILLA_ATRIB.GRUPO,PLANTILLA_ATRIB.INTRO,PLANTILLA_ATRIB.MINNUM,PLANTILLA_ATRIB.MAXNUM,PLANTILLA_ATRIB.MINFEC,PLANTILLA_ATRIB.MAXFEC,PLANTILLA_ATRIB.PREFBAJO," & vbCrLf
sConsulta = sConsulta & "                            PLANTILLA_ATRIB.INTERNO,PLANTILLA_ATRIB.AMBITO,PLANTILLA_ATRIB.OBLIG,PLANTILLA_ATRIB.OP_PREC,PLANTILLA_ATRIB.APLIC_PREC,PLANTILLA_ATRIB.DEF_PREC," & vbCrLf
sConsulta = sConsulta & "                            PLANTILLA_ATRIB.POND,PLANTILLA_ATRIB.OP_POND,PLANTILLA_ATRIB.NUM_POND,PLANTILLA_ATRIB.POND_SI,PLANTILLA_ATRIB.POND_NO," & vbCrLf
sConsulta = sConsulta & "                            DEF_ATRIB.COD,DEF_ATRIB.DEN,DEF_ATRIB.TIPO,PLANTILLA_ATRIB.FECACT,PLANTILLA_ATRIB.ORDEN, DEF_ATRIB.DESCR" & vbCrLf
sConsulta = sConsulta & "                            FROM PLANTILLA_ATRIB INNER JOIN DEF_ATRIB ON PLANTILLA_ATRIB.ATRIB=DEF_ATRIB.ID" & vbCrLf
sConsulta = sConsulta & "                            WHERE PLANTILLA_ATRIB.PLANT=@PLANTILLA  AND   (PLANTILLA_ATRIB.GRUPO=@GRUPO OR PLANTILLA_ATRIB.GRUPO IS NULL)" & vbCrLf
sConsulta = sConsulta & "                               ORDER BY PLANTILLA_ATRIB.GRUPO, PLANTILLA_ATRIB.ORDEN_ATRIB" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_DEVOLVER_PROCESOS_PROVE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_DEVOLVER_PROCESOS_PROVE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_DEVOLVER_PROCESOS_PROVE (@PROVE VARCHAR(50),@NORMAL SMALLINT OUTPUT ,@SUBASTA SMALLINT OUTPUT) AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @PISUBASTA TINYINT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @PISUBASTA = (SELECT SUBASTA FROM PARGEN_INTERNO)" & vbCrLf
sConsulta = sConsulta & "SET @NORMAL=(SELECT COUNT(*) " & vbCrLf
sConsulta = sConsulta & "               FROM PROCE " & vbCrLf
sConsulta = sConsulta & "                    INNER JOIN PROCE_PROVE " & vbCrLf
sConsulta = sConsulta & "                            ON PROCE.ANYO= PROCE_PROVE.ANYO " & vbCrLf
sConsulta = sConsulta & "                           AND PROCE.GMN1=PROCE_PROVE.GMN1 " & vbCrLf
sConsulta = sConsulta & "                           AND PROCE.COD=PROCE_PROVE.PROCE " & vbCrLf
sConsulta = sConsulta & "                           AND PROCE_PROVE.PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "                           AND PROCE_PROVE.PUB=1 " & vbCrLf
sConsulta = sConsulta & "                           AND PROCE.EST<=11 " & vbCrLf
sConsulta = sConsulta & "                    INNER JOIN PROCE_DEF PD" & vbCrLf
sConsulta = sConsulta & "                            ON PROCE.ANYO = PD.ANYO" & vbCrLf
sConsulta = sConsulta & "                           AND PROCE.GMN1 = PD.GMN1" & vbCrLf
sConsulta = sConsulta & "                           AND PROCE.COD = PD.PROCE" & vbCrLf
sConsulta = sConsulta & "              WHERE PD.SUBASTA=CASE WHEN @PISUBASTA=1 THEN 0 ELSE PD.SUBASTA END)" & vbCrLf
sConsulta = sConsulta & "IF @PISUBASTA=1 " & vbCrLf
sConsulta = sConsulta & "    SET @SUBASTA=(SELECT COUNT(*) " & vbCrLf
sConsulta = sConsulta & "                FROM PROCE " & vbCrLf
sConsulta = sConsulta & "                     INNER JOIN PROCE_PROVE " & vbCrLf
sConsulta = sConsulta & "                             ON PROCE.ANYO= PROCE_PROVE.ANYO " & vbCrLf
sConsulta = sConsulta & "                            AND PROCE.GMN1=PROCE_PROVE.GMN1 " & vbCrLf
sConsulta = sConsulta & "                            AND PROCE.COD=PROCE_PROVE.PROCE " & vbCrLf
sConsulta = sConsulta & "                            AND PROCE_PROVE.PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "                            AND PROCE_PROVE.PUB=1 " & vbCrLf
sConsulta = sConsulta & "                            AND PROCE.EST<=11 " & vbCrLf
sConsulta = sConsulta & "                     INNER JOIN PROCE_DEF PD" & vbCrLf
sConsulta = sConsulta & "                             ON PROCE.ANYO = PD.ANYO" & vbCrLf
sConsulta = sConsulta & "                            AND PROCE.GMN1 = PD.GMN1" & vbCrLf
sConsulta = sConsulta & "                            AND PROCE.COD = PD.PROCE" & vbCrLf
sConsulta = sConsulta & "               WHERE PD.SUBASTA=1)" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   SET @SUBASTA = 0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "/*Devuelve en 1� lugar los procesos normales y luego los abiertos en modo subasta*/" & vbCrLf
sConsulta = sConsulta & "SELECT PROCE.ANYO,PROCE.GMN1,PROCE.COD,PROCE.DEN,PROCE.FECAPE,PROCE.FECLIMOFE,PROCE_PROVE.OFE,PROCE.EST,PROCE_PROVE.SUPERADA," & vbCrLf
sConsulta = sConsulta & "       PROCE.FECINISUB, PD.SUBASTA,PD.SUBASTAPROVE,PD.SUBASTAPUJAS,PROCE.MON,PROCE_PROVE.NUMOBJ,PROCE_PROVE.OBJNUEVOS " & vbCrLf
sConsulta = sConsulta & "  FROM PROCE " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PROCE_PROVE " & vbCrLf
sConsulta = sConsulta & "                   ON PROCE.ANYO= PROCE_PROVE.ANYO " & vbCrLf
sConsulta = sConsulta & "                  AND PROCE.GMN1=PROCE_PROVE.GMN1 " & vbCrLf
sConsulta = sConsulta & "                  AND PROCE.COD=PROCE_PROVE.PROCE " & vbCrLf
sConsulta = sConsulta & "                  AND PROCE_PROVE.PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "                  AND PROCE_PROVE.PUB=1 " & vbCrLf
sConsulta = sConsulta & "                  AND PROCE.EST<=11  " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PROCE_DEF PD" & vbCrLf
sConsulta = sConsulta & "                   ON PROCE.ANYO = PD.ANYO" & vbCrLf
sConsulta = sConsulta & "                  AND PROCE.GMN1 = PD.GMN1" & vbCrLf
sConsulta = sConsulta & "                  AND PROCE.COD = PD.PROCE" & vbCrLf
sConsulta = sConsulta & "ORDER BY PD.SUBASTA" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_CARGAR_CONFIG_PROCE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_CARGAR_CONFIG_PROCE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_CARGAR_CONFIG_PROCE @ANYO INT, @GMN1 VARCHAR(50), @PROCE INT,@DEFDEST tinyint  = null OUTPUT, @DEFPAG tinyint = null OUTPUT," & vbCrLf
sConsulta = sConsulta & "                                        @DEFFECSUM tinyint  = null OUTPUT, @DEFPROCEESP tinyint  = null OUTPUT, @DEFGRUPOESP  tinyint = null OUTPUT, @DEFITEMESP tinyint = null OUTPUT, " & vbCrLf
sConsulta = sConsulta & "                                        @DEFOFEADJUN tinyint = null OUTPUT, @DEFGRUPOADJUN tinyint = null OUTPUT, @DEFITEMADJUN tinyint = null OUTPUT," & vbCrLf
sConsulta = sConsulta & "                                        @DEFSUBASTA tinyint = null OUTPUT, @DEFSUVERPROVE tinyint = null OUTPUT, @DEFSUVERPUJAS tinyint = null OUTPUT, @DEFSUMINUTOS int = null OUTPUT," & vbCrLf
sConsulta = sConsulta & "                                        @DEFPRECALTER tinyint = null OUTPUT, @DEFCANTMAX tinyint = null OUTPUT, @HAYATRIBUTOS AS tinyint = null OUTPUT, " & vbCrLf
sConsulta = sConsulta & "                                        @CAMBIARMON tinyint = 1 OUTPUT, @ADMINPUB tinyint=null OUTPUT, @NUMMAXOFE int = 0 OUTPUT, @PUBLICARFINSUM tinyint = null OUTPUT AS" & vbCrLf
sConsulta = sConsulta & "set nocount on" & vbCrLf
sConsulta = sConsulta & "DECLARE @PISUBASTA TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCEEST INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @PISUBASTA = (SELECT SUBASTA FROM PARGEN_INTERNO)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @PROCEEST = EST " & vbCrLf
sConsulta = sConsulta & "  FROM PROCE " & vbCrLf
sConsulta = sConsulta & " WHERE ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "   AND GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "   AND COD = @PROCE" & vbCrLf
sConsulta = sConsulta & "SELECT @DEFDEST=PD.DEST, @DEFPAG=PD.PAG, @DEFFECSUM=PD.FECSUM, @DEFPROCEESP=PD.PROCE_ESP, @DEFGRUPOESP = PD.GRUPO_ESP, @DEFITEMESP  = PD.ITEM_ESP, " & vbCrLf
sConsulta = sConsulta & "       @DEFOFEADJUN = PD.OFE_ADJUN, @DEFGRUPOADJUN=PD.GRUPO_ADJUN, @DEFITEMADJUN= PD.ITEM_ADJUN, @DEFCANTMAX=PD.SOLCANTMAX, @DEFPRECALTER = PD.PRECALTER, " & vbCrLf
sConsulta = sConsulta & "       @DEFSUBASTA = CASE WHEN @PISUBASTA=1 THEN PD.SUBASTA ELSE 0 END, @DEFSUVERPROVE = PD.SUBASTAPROVE, @DEFSUVERPUJAS=PD.SUBASTAPUJAS, @DEFSUMINUTOS = PD.SUBASTAESPERA," & vbCrLf
sConsulta = sConsulta & "       @HAYATRIBUTOS = ISNULL(PA.ATRIB,0),@CAMBIARMON=PD.CAMBIARMON, @ADMINPUB = ADMIN_PUB, @NUMMAXOFE = PD.MAX_OFE , @PUBLICARFINSUM = PD.PUBLICARFINSUM" & vbCrLf
sConsulta = sConsulta & "  FROM PROCE_DEF PD" & vbCrLf
sConsulta = sConsulta & "          LEFT JOIN (SELECT ANYO, GMN1, PROCE, COUNT(ATRIB ) ATRIB" & vbCrLf
sConsulta = sConsulta & "                        FROM PROCE_ATRIB" & vbCrLf
sConsulta = sConsulta & "                       WHERE ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "                         AND GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "                         AND PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "                         AND AMBITO = 1" & vbCrLf
sConsulta = sConsulta & "                    GROUP BY ANYO, GMN1, PROCE) PA" & vbCrLf
sConsulta = sConsulta & "                  ON PD.ANYO = PA.ANYO" & vbCrLf
sConsulta = sConsulta & "                 AND PD.GMN1 = PA.GMN1" & vbCrLf
sConsulta = sConsulta & "                 AND PD.PROCE = PA.PROCE " & vbCrLf
sConsulta = sConsulta & " WHERE PD.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "   AND PD.GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "   AND PD.PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "  " & vbCrLf
sConsulta = sConsulta & "select PG.anyo, PG.gmn1, PG.proce, PG.COD , PG.DEN, COUNT(PA.ATRIB) ATRIB, S.SOBRE, S.USUAPER, S.EST, S.FECAPE, S.FECEST, S.OBS," & vbCrLf
sConsulta = sConsulta & "       sum(case dato when 'ESP' THEN 1 else 0 end) as ESP," & vbCrLf
sConsulta = sConsulta & "       sum(case dato when 'PRES1' THEN 1 ELSE 0 END) AS PRES1," & vbCrLf
sConsulta = sConsulta & "       sum(case dato when 'PRES2' THEN 1 ELSE 0 END) AS PRES2," & vbCrLf
sConsulta = sConsulta & "       sum(case dato when 'PRESANU1' THEN 1 ELSE 0 END) AS PRESANU1," & vbCrLf
sConsulta = sConsulta & "       sum(case dato when 'PRESANU2' THEN 1 ELSE 0 END) AS PRESANU2," & vbCrLf
sConsulta = sConsulta & "       sum(case dato when 'DIST' THEN 1 ELSE 0 END) AS DIST," & vbCrLf
sConsulta = sConsulta & "       sum(case dato when 'PROVE' THEN 1 ELSE 0 END) AS PROVE," & vbCrLf
sConsulta = sConsulta & "       sum(case dato when 'FECSUM' THEN 1 ELSE 0 END) AS FECSUM," & vbCrLf
sConsulta = sConsulta & "       sum(case dato when 'PAG' THEN 1 ELSE 0 END) AS PAG," & vbCrLf
sConsulta = sConsulta & "       sum(case dato when 'DEST' THEN 1 ELSE 0 END) AS DEST," & vbCrLf
sConsulta = sConsulta & "       ISNULL(IT.NUMITEMS,0) NUMITEMS" & vbCrLf
sConsulta = sConsulta & "  from PROCE_GRUPO PG " & vbCrLf
sConsulta = sConsulta & "          LEFT JOIN PROCE_GRUPO_DEF PGD" & vbCrLf
sConsulta = sConsulta & "                  ON PG.ANYO = PGD.ANYO" & vbCrLf
sConsulta = sConsulta & "                 AND PG.GMN1 = PGD.GMN1" & vbCrLf
sConsulta = sConsulta & "                 AND PG.PROCE = PGD.PROCE" & vbCrLf
sConsulta = sConsulta & "                 AND PG.COD = PGD.GRUPO" & vbCrLf
sConsulta = sConsulta & "           LEFT JOIN (SELECT ANYO, GMN1, PROCE, ISNULL(GRUPO,'#T#') GRUPO, ATRIB " & vbCrLf
sConsulta = sConsulta & "                        FROM PROCE_ATRIB " & vbCrLf
sConsulta = sConsulta & "                       WHERE ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "                         AND GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "                         AND PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "                         AND AMBITO = 2) PA" & vbCrLf
sConsulta = sConsulta & "                  ON PG.ANYO = PA.ANYO" & vbCrLf
sConsulta = sConsulta & "                 AND PG.GMN1 = PA.GMN1" & vbCrLf
sConsulta = sConsulta & "                 AND PG.PROCE = PA.PROCE" & vbCrLf
sConsulta = sConsulta & "                 AND PG.COD = CASE WHEN PA.GRUPO ='#T#' THEN PG.COD ELSE PA.GRUPO END" & vbCrLf
sConsulta = sConsulta & "          LEFT JOIN (SELECT I.GRUPO, count(I.id) NUMITEMS" & vbCrLf
sConsulta = sConsulta & "                        FROM ITEM I " & vbCrLf
sConsulta = sConsulta & "                      WHERE I.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "                        AND I.GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "                        AND I.PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "                        AND I.CONF = 1" & vbCrLf
sConsulta = sConsulta & "                        AND CASE WHEN I.EST IS NULL THEN -1 ELSE I.EST END = CASE WHEN @PROCEEST=11 THEN 0 ELSE -1 END" & vbCrLf
sConsulta = sConsulta & "                     GROUP BY I.GRUPO) IT" & vbCrLf
sConsulta = sConsulta & "                  ON PG.COD = IT.GRUPO" & vbCrLf
sConsulta = sConsulta & "           LEFT JOIN SOBRE S" & vbCrLf
sConsulta = sConsulta & "                  ON PG.ANYO = S.ANYO" & vbCrLf
sConsulta = sConsulta & "                 AND PG.GMN1 = S.GMN1" & vbCrLf
sConsulta = sConsulta & "                 AND PG.PROCE = S.PROCE" & vbCrLf
sConsulta = sConsulta & "                 AND PG.SOBRE = S.SOBRE" & vbCrLf
sConsulta = sConsulta & "where PG.anyo = @ANYO" & vbCrLf
sConsulta = sConsulta & "  and PG.gmn1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "  and PG.proce = @PROCE" & vbCrLf
sConsulta = sConsulta & "  and isnull(pg.cerrado,0) = 0" & vbCrLf
sConsulta = sConsulta & "group by PG.anyo, PG.gmn1, PG.proce, PG.cod, PG.DEN, IT.NUMITEMS, S.SOBRE, S.USUAPER, S.EST, S.FECAPE, S.FECEST, S.OBS" & vbCrLf
sConsulta = sConsulta & "ORDER BY S.SOBRE,PG.COD" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_DEVOLVER_PROCESO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_DEVOLVER_PROCESO]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_DEVOLVER_PROCESO (@ANYO INTEGER,@GMN1 VARCHAR(20),@PROCE INTEGER ,@ACTIVA SMALLINT OUTPUT) AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @SUBASTA AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @FECLIMOFE AS DATETIME" & vbCrLf
sConsulta = sConsulta & "DECLARE @HAYESP  AS BIT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ENVIOASINCRONO AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PISUBASTA TINYINT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  SELECT @ENVIOASINCRONO = ENVIO_ASINCRONO, @PISUBASTA= SUBASTA FROM PARGEN_INTERNO WHERE ID = 1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  SELECT @SUBASTA=CASE WHEN @PISUBASTA=1 THEN PD.SUBASTA ELSE 0 END, @FECLIMOFE =FECLIMOFE " & vbCrLf
sConsulta = sConsulta & "    FROM PROCE " & vbCrLf
sConsulta = sConsulta & "        INNER JOIN PROCE_DEF PD" & vbCrLf
sConsulta = sConsulta & "                ON PROCE.ANYO = PD.ANYO" & vbCrLf
sConsulta = sConsulta & "               AND PROCE.GMN1 = PD.GMN1" & vbCrLf
sConsulta = sConsulta & "               AND PROCE.COD = PD.PROCE" & vbCrLf
sConsulta = sConsulta & "   WHERE PROCE.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "     AND PROCE.COD=@PROCE " & vbCrLf
sConsulta = sConsulta & "     AND PROCE.ANYO=@ANYO" & vbCrLf
sConsulta = sConsulta & "  " & vbCrLf
sConsulta = sConsulta & "  SELECT @HAYESP = CASE WHEN ESP IS NULL OR ESP='' THEN 0 ELSE 1 END " & vbCrLf
sConsulta = sConsulta & "    FROM PROCE " & vbCrLf
sConsulta = sConsulta & "   WHERE ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "     AND COD=@PROCE " & vbCrLf
sConsulta = sConsulta & "     AND GMN1=@GMN1" & vbCrLf
sConsulta = sConsulta & "  IF @HAYESP = 0" & vbCrLf
sConsulta = sConsulta & "     BEGIN   " & vbCrLf
sConsulta = sConsulta & "       if (select count(id) " & vbCrLf
sConsulta = sConsulta & "             from proce_esp " & vbCrLf
sConsulta = sConsulta & "            where aNYo=@ANYO " & vbCrLf
sConsulta = sConsulta & "              and gmn1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "              and proce=@PROCE)>0" & vbCrLf
sConsulta = sConsulta & "         select @HAYESP = 1" & vbCrLf
sConsulta = sConsulta & "     END" & vbCrLf
sConsulta = sConsulta & "  IF @SUBASTA= 1 AND @FECLIMOFE <=GETDATE() " & vbCrLf
sConsulta = sConsulta & "      SET @ACTIVA=2" & vbCrLf
sConsulta = sConsulta & "  ELSE " & vbCrLf
sConsulta = sConsulta & "    IF @SUBASTA=1 " & vbCrLf
sConsulta = sConsulta & "      SET @ACTIVA=1" & vbCrLf
sConsulta = sConsulta & "    ELSE" & vbCrLf
sConsulta = sConsulta & "      SET @ACTIVA=0" & vbCrLf
sConsulta = sConsulta & "  SELECT PROCE.ANYO,PROCE.GMN1,PROCE.COD,PROCE.DEN,PROCE.GMN2,PROCE.GMN3,PROCE.GMN4,PROCE.ADJDIR,PROCE.PRES,PROCE.EST," & vbCrLf
sConsulta = sConsulta & "         PROCE.FECAPE,PROCE.FECLIMOFE,PROCE.FECNEC,PROCE.FECPRES,PROCE.FECENVPET,PROCE.FECPROXREU,PROCE.FECULTREU," & vbCrLf
sConsulta = sConsulta & "         PROCE.MON,MON.DEN DENMON, PROCE.CAMBIO,PROCE.SOLICITUD,PROCE.ESP,PROCE.DEST,PROCE.PAG, PAG.DEN DENPAG, PROCE.FECINI,PROCE.FECFIN," & vbCrLf
sConsulta = sConsulta & "         PROCE.FECINISUB,PROCE.COM, CASE WHEN @PISUBASTA=1 THEN PD.SUBASTA ELSE 0 END SUBASTA,PD.SUBASTAPROVE,PD.SUBASTAPUJAS, @HAYESP HAYESP , @ENVIOASINCRONO ENVIO_ASINCRONO" & vbCrLf
sConsulta = sConsulta & "    FROM PROCE " & vbCrLf
sConsulta = sConsulta & "          INNER JOIN PROCE_DEF PD" & vbCrLf
sConsulta = sConsulta & "                  ON PROCE.ANYO = PD.ANYO" & vbCrLf
sConsulta = sConsulta & "                 AND PROCE.GMN1 = PD.GMN1" & vbCrLf
sConsulta = sConsulta & "                 AND PROCE.COD = PD.PROCE" & vbCrLf
sConsulta = sConsulta & "           LEFT JOIN PAG" & vbCrLf
sConsulta = sConsulta & "                  ON PROCE.PAG = PAG.COD" & vbCrLf
sConsulta = sConsulta & "           LEFT JOIN MON" & vbCrLf
sConsulta = sConsulta & "                  ON PROCE.MON = MON.COD" & vbCrLf
sConsulta = sConsulta & "   WHERE PROCE.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "     AND PROCE.COD=@PROCE " & vbCrLf
sConsulta = sConsulta & "     AND PROCE.ANYO=@ANYO" & vbCrLf
sConsulta = sConsulta & "RETURN" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Private Sub V_2_14_Triggers_11()
Dim sConsulta As String

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CATALOG_LIN_TG_UPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[CATALOG_LIN_TG_UPD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE  TRIGGER CATALOG_LIN_TG_UPD ON dbo.CATALOG_LIN" & vbCrLf
sConsulta = sConsulta & "FOR UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @PUB INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @PUBOLD INTEGER" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  DECLARE curTG_CATALOGLIN_Upd CURSOR LOCAL FOR SELECT I.ID,I.PUB PUB,D.PUB PUBOLD FROM INSERTED I  INNER JOIN DELETED D ON I.ID=D.ID" & vbCrLf
sConsulta = sConsulta & "  OPEN curTG_CATALOGLIN_Upd" & vbCrLf
sConsulta = sConsulta & "  FETCH NEXT FROM curTG_CATALOGLIN_Upd INTO @ID,@PUB,@PUBOLD" & vbCrLf
sConsulta = sConsulta & "  WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "       IF UPDATE (PUB)" & vbCrLf
sConsulta = sConsulta & "             BEGIN" & vbCrLf
sConsulta = sConsulta & "             IF @PUB=1 AND @PUBOLD=0" & vbCrLf
sConsulta = sConsulta & "                 UPDATE CATALOG_LIN SET FECHA_PUB=GETDATE() WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "            END" & vbCrLf
sConsulta = sConsulta & "      FETCH NEXT FROM curTG_CATALOGLIN_Upd INTO @ID,@PUB,@PUBOLD" & vbCrLf
sConsulta = sConsulta & "    END" & vbCrLf
sConsulta = sConsulta & "  Close curTG_CATALOGLIN_Upd" & vbCrLf
sConsulta = sConsulta & "  DEALLOCATE curTG_CATALOGLIN_Upd" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub


Public Function CodigoDeActualizacion2_14_00_11A2_14_00_12() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim bBool As Boolean

On Error GoTo error
    

sConsulta = "BEGIN TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = True
ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

frmProgreso.lblDetalle = "Actualizamos tablas"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_14_Tablas_12


sConsulta = "UPDATE VERSION SET NUM ='2.14.00.12'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "COMMIT TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = False

CodigoDeActualizacion2_14_00_11A2_14_00_12 = True
Exit Function
    
error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_14_00_11A2_14_00_12 = False
End Function


Private Sub V_2_14_Tablas_12()
Dim sConsulta As String

'Campos de especificaciones de 4000 a 800 caracteres:
'PROCE:
sConsulta = "ALTER TABLE dbo.PROCE ALTER COLUMN" & vbCrLf
sConsulta = sConsulta & "   ESP varchar(800) NULL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

If frmProgreso.ProgressBar1.Value >= frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1


'PROCE_GRUPO
sConsulta = "DECLARE @STAT VARCHAR(200)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "if exists(select * from sysindexes where id = object_id(N'[dbo].[PROCE_GRUPO]') and name like '_WA_Sys_ESP_%')" & vbCrLf
sConsulta = sConsulta & "begin" & vbCrLf
sConsulta = sConsulta & "select @STAT=NAME from sysindexes where id = object_id(N'[dbo].[PROCE_GRUPO]') and name like '_WA_Sys_ESP_%'" & vbCrLf
sConsulta = sConsulta & "SET @SQL= 'drop statistics [dbo].[PROCE_GRUPO].' + @STAT" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE dbo.PROCE_GRUPO ALTER COLUMN" & vbCrLf
sConsulta = sConsulta & "   ESP varchar(800) NULL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

If frmProgreso.ProgressBar1.Value >= frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1


'PROVE_ART4
sConsulta = "DECLARE @STAT VARCHAR(200)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "if exists(select * from sysindexes where id = object_id(N'[dbo].[PROVE_ART4]') and name like '_WA_Sys_OBSADJUN_%')" & vbCrLf
sConsulta = sConsulta & "begin" & vbCrLf
sConsulta = sConsulta & "select @STAT=NAME from sysindexes where id = object_id(N'[dbo].[PROVE_ART4]') and name like '_WA_Sys_OBSADJUN_%'" & vbCrLf
sConsulta = sConsulta & "SET @SQL= 'drop statistics [dbo].[PROVE_ART4].' + @STAT" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE dbo.PROVE_ART4 ALTER COLUMN" & vbCrLf
sConsulta = sConsulta & "   OBSADJUN varchar(2000) NULL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

If frmProgreso.ProgressBar1.Value >= frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

'LOG_ART4
sConsulta = "DECLARE @STAT VARCHAR(200)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "if exists(select * from sysindexes where id = object_id(N'[dbo].[LOG_ART4]') and name like '_WA_Sys_ESP_%')" & vbCrLf
sConsulta = sConsulta & "begin" & vbCrLf
sConsulta = sConsulta & "select @STAT=NAME from sysindexes where id = object_id(N'[dbo].[LOG_ART4]') and name like '_WA_Sys_ESP_%'" & vbCrLf
sConsulta = sConsulta & "SET @SQL= 'drop statistics [dbo].[LOG_ART4].' + @STAT" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE dbo.LOG_ART4 ALTER COLUMN" & vbCrLf
sConsulta = sConsulta & "   ESP varchar(800) NULL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

If frmProgreso.ProgressBar1.Value >= frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

'ART4
sConsulta = "DECLARE @STAT VARCHAR(200)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "if exists(select * from sysindexes where id = object_id(N'[dbo].[ART4]') and name like '_WA_Sys_ESP_%')" & vbCrLf
sConsulta = sConsulta & "begin" & vbCrLf
sConsulta = sConsulta & "select @STAT=NAME from sysindexes where id = object_id(N'[dbo].[ART4]') and name like '_WA_Sys_ESP_%'" & vbCrLf
sConsulta = sConsulta & "SET @SQL= 'drop statistics [dbo].[ART4].' + @STAT" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE dbo.ART4 ALTER COLUMN" & vbCrLf
sConsulta = sConsulta & "   ESP varchar(800) NULL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

'DEF_ATRIB
If frmProgreso.ProgressBar1.Value >= frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

sConsulta = "ALTER TABLE dbo.DEF_ATRIB ALTER COLUMN" & vbCrLf
sConsulta = sConsulta & "   DESCR NVARCHAR(800) NULL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

If frmProgreso.ProgressBar1.Value >= frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

'Pasa el campo ESP de la tabla ITEM de text a varchar:
sConsulta = " ALTER TABLE dbo.ITEM" & vbCrLf
sConsulta = sConsulta & "   DROP CONSTRAINT FK_ITEM_ART4"
ExecuteSQL gRDOCon, sConsulta

sConsulta = " ALTER TABLE dbo.ITEM" & vbCrLf
sConsulta = sConsulta & "   DROP CONSTRAINT FK_ITEM_UNI"
ExecuteSQL gRDOCon, sConsulta

sConsulta = " ALTER TABLE dbo.ITEM" & vbCrLf
sConsulta = sConsulta & "   DROP CONSTRAINT FK_ITEM_PAG"
ExecuteSQL gRDOCon, sConsulta

sConsulta = " ALTER TABLE dbo.ITEM" & vbCrLf
sConsulta = sConsulta & "   DROP CONSTRAINT FK_ITEM_DEST"
ExecuteSQL gRDOCon, sConsulta

sConsulta = " ALTER TABLE dbo.ITEM" & vbCrLf
sConsulta = sConsulta & "   DROP CONSTRAINT FK_ITEM_SOLICIT"
ExecuteSQL gRDOCon, sConsulta

sConsulta = " ALTER TABLE dbo.ITEM" & vbCrLf
sConsulta = sConsulta & "   DROP CONSTRAINT FK_ITEM_PROVE"
ExecuteSQL gRDOCon, sConsulta

sConsulta = " ALTER TABLE dbo.ITEM" & vbCrLf
sConsulta = sConsulta & "   DROP CONSTRAINT FK_ITEM_PROCE_GRUPO"
ExecuteSQL gRDOCon, sConsulta

sConsulta = " ALTER TABLE dbo.ITEM" & vbCrLf
sConsulta = sConsulta & "   DROP CONSTRAINT FK_ITEM_PROCE"
ExecuteSQL gRDOCon, sConsulta

sConsulta = " ALTER TABLE dbo.ITEM" & vbCrLf
sConsulta = sConsulta & "   DROP CONSTRAINT DF_ITEM_CONF"
ExecuteSQL gRDOCon, sConsulta

sConsulta = " CREATE TABLE dbo.Tmp_ITEM" & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   ANYO smallint NOT NULL," & vbCrLf
sConsulta = sConsulta & "   PROCE smallint NOT NULL," & vbCrLf
sConsulta = sConsulta & "   GMN1 varchar(" & gLongitudesDeCodigos.giLongCodGMN1 & ") NOT NULL," & vbCrLf
sConsulta = sConsulta & "   ID int NOT NULL," & vbCrLf
sConsulta = sConsulta & "   GMN2 varchar(" & gLongitudesDeCodigos.giLongCodGMN2 & ") NOT NULL," & vbCrLf
sConsulta = sConsulta & "   GMN3 varchar(" & gLongitudesDeCodigos.giLongCodGMN3 & ") NOT NULL," & vbCrLf
sConsulta = sConsulta & "   GMN4 varchar(" & gLongitudesDeCodigos.giLongCodGMN4 & ") NOT NULL," & vbCrLf
sConsulta = sConsulta & "   ART varchar(" & gLongitudesDeCodigos.giLongCodART & ") NULL," & vbCrLf
sConsulta = sConsulta & "   DESCR varchar(200) NULL," & vbCrLf
sConsulta = sConsulta & "   DEST varchar(" & gLongitudesDeCodigos.giLongCodDEST & ") NOT NULL," & vbCrLf
sConsulta = sConsulta & "   UNI varchar(" & gLongitudesDeCodigos.giLongCodUNI & ") NOT NULL," & vbCrLf
sConsulta = sConsulta & "   PAG varchar(" & gLongitudesDeCodigos.giLongCodPAG & ") NOT NULL," & vbCrLf
sConsulta = sConsulta & "   CANT float NOT NULL," & vbCrLf
sConsulta = sConsulta & "   PREC float NULL," & vbCrLf
sConsulta = sConsulta & "   PRES float NULL," & vbCrLf
sConsulta = sConsulta & "   FECINI datetime NOT NULL," & vbCrLf
sConsulta = sConsulta & "   FECFIN datetime NOT NULL," & vbCrLf
sConsulta = sConsulta & "   OBJ float NULL," & vbCrLf
sConsulta = sConsulta & "   ESP varchar(800) NULL," & vbCrLf
sConsulta = sConsulta & "   CONF tinyint NOT NULL," & vbCrLf
sConsulta = sConsulta & "   FECACT datetime NULL," & vbCrLf
sConsulta = sConsulta & "   FECHAOBJ datetime NULL," & vbCrLf
sConsulta = sConsulta & "   PROVEACT varchar(" & gLongitudesDeCodigos.giLongCodPROVE & ") NULL," & vbCrLf
sConsulta = sConsulta & "   EST tinyint NULL," & vbCrLf
sConsulta = sConsulta & "   GRUPO varchar(" & gLongitudesDeCodigos.giLongCodGRUPO & ") NULL," & vbCrLf
sConsulta = sConsulta & "   LIMITE float NULL," & vbCrLf
sConsulta = sConsulta & "   SOLICIT int NULL" & vbCrLf
sConsulta = sConsulta & "   )  ON [PRIMARY]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = " ALTER TABLE dbo.Tmp_ITEM ADD CONSTRAINT" & vbCrLf
sConsulta = sConsulta & "   DF_ITEM_CONF DEFAULT (1) FOR CONF"
ExecuteSQL gRDOCon, sConsulta

sConsulta = " IF EXISTS(SELECT * FROM dbo.ITEM)" & vbCrLf
sConsulta = sConsulta & "    EXEC('INSERT INTO dbo.Tmp_ITEM (ANYO, PROCE, GMN1, ID, GMN2, GMN3, GMN4, ART, DESCR, DEST, UNI, PAG, CANT, PREC, PRES, FECINI, FECFIN, OBJ, ESP, CONF, FECACT, FECHAOBJ, PROVEACT, EST, GRUPO, LIMITE, SOLICIT)" & vbCrLf
sConsulta = sConsulta & "       SELECT ANYO, PROCE, GMN1, ID, GMN2, GMN3, GMN4, ART, DESCR, DEST, UNI, PAG, CANT, PREC, PRES, FECINI, FECFIN, OBJ, CONVERT(varchar(800), ESP), CONF, FECACT, FECHAOBJ, PROVEACT, EST, GRUPO, LIMITE, SOLICIT FROM dbo.ITEM TABLOCKX')" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = " ALTER TABLE dbo.AR_ITEM_PROVE" & vbCrLf
sConsulta = sConsulta & "   DROP CONSTRAINT FK_AR_ITEM_PROVE_ITEM"
ExecuteSQL gRDOCon, sConsulta

sConsulta = " ALTER TABLE dbo.ITEM_UON3" & vbCrLf
sConsulta = sConsulta & "   DROP CONSTRAINT FK_ITEM_UON3_ITEM"
ExecuteSQL gRDOCon, sConsulta

sConsulta = " ALTER TABLE dbo.ITEM_UON2" & vbCrLf
sConsulta = sConsulta & "   DROP CONSTRAINT FK_ITEM_UON2_ITEM"
ExecuteSQL gRDOCon, sConsulta

sConsulta = " ALTER TABLE dbo.AR_ITEM_PRESCON4" & vbCrLf
sConsulta = sConsulta & "   DROP CONSTRAINT FK_AR_ITEM_PRESCON4_ITEM"
ExecuteSQL gRDOCon, sConsulta

sConsulta = " ALTER TABLE dbo.ITEM_UON1" & vbCrLf
sConsulta = sConsulta & "   DROP CONSTRAINT FK_ITEM_UON1_ITEM"
ExecuteSQL gRDOCon, sConsulta

sConsulta = " ALTER TABLE dbo.ITEM_PRESPROY" & vbCrLf
sConsulta = sConsulta & "   DROP CONSTRAINT FK_ITEM_PRESPROY_ITEM"
ExecuteSQL gRDOCon, sConsulta

sConsulta = " ALTER TABLE dbo.AR_ITEM_PRESCON3" & vbCrLf
sConsulta = sConsulta & "   DROP CONSTRAINT FK_AR_ITEM_PRESCON3_ITEM"
ExecuteSQL gRDOCon, sConsulta

sConsulta = " ALTER TABLE dbo.AR_ITEM_MES_UON3" & vbCrLf
sConsulta = sConsulta & "   DROP CONSTRAINT FK_AR_ITEM_MES_UON3_ITEM"
ExecuteSQL gRDOCon, sConsulta

sConsulta = " ALTER TABLE dbo.ITEM_PRESCON4" & vbCrLf
sConsulta = sConsulta & "   DROP CONSTRAINT FK_ITEM_PRESCON4_ITEM"
ExecuteSQL gRDOCon, sConsulta

sConsulta = " ALTER TABLE dbo.AR_ITEM_MES_UON2" & vbCrLf
sConsulta = sConsulta & "   DROP CONSTRAINT FK_AR_ITEM_MES_UON2_ITEM"
ExecuteSQL gRDOCon, sConsulta

sConsulta = " ALTER TABLE dbo.AR_ITEM_MES_UON1" & vbCrLf
sConsulta = sConsulta & "   DROP CONSTRAINT FK_AR_ITEM_MES_UON1_ITEM"
ExecuteSQL gRDOCon, sConsulta

sConsulta = " ALTER TABLE dbo.ITEM_PRESCON3" & vbCrLf
sConsulta = sConsulta & "   DROP CONSTRAINT FK_ITEM_PRESCON3_ITEM"
ExecuteSQL gRDOCon, sConsulta

sConsulta = " ALTER TABLE dbo.ITEM_PRESCON" & vbCrLf
sConsulta = sConsulta & "   DROP CONSTRAINT FK_ITEM_PRESCON_ITEM"
ExecuteSQL gRDOCon, sConsulta

sConsulta = " ALTER TABLE dbo.AR_ITEM_MES_PRESCON4_4" & vbCrLf
sConsulta = sConsulta & "   DROP CONSTRAINT FK_AR_ITEM_MES_PRESCON4_4_ITEM"
ExecuteSQL gRDOCon, sConsulta

sConsulta = " ALTER TABLE dbo.AR_ITEM_MES_PRESCON4_3" & vbCrLf
sConsulta = sConsulta & "   DROP CONSTRAINT FK_AR_ITEM_MES_PRESCON4_3_ITEM"
ExecuteSQL gRDOCon, sConsulta

sConsulta = " ALTER TABLE dbo.ITEM_PRES" & vbCrLf
sConsulta = sConsulta & "   DROP CONSTRAINT FK_ITEM_PRES_ITEM"
ExecuteSQL gRDOCon, sConsulta

sConsulta = " ALTER TABLE dbo.AR_ITEM_MES_PRESCON4_2" & vbCrLf
sConsulta = sConsulta & "   DROP CONSTRAINT FK_AR_ITEM_MES_PRESCON4_2_ITEM"
ExecuteSQL gRDOCon, sConsulta

sConsulta = " ALTER TABLE dbo.ITEM_OFE" & vbCrLf
sConsulta = sConsulta & "   DROP CONSTRAINT FK_ITEM_OFE_ITEM"
ExecuteSQL gRDOCon, sConsulta

sConsulta = " ALTER TABLE dbo.AR_ITEM_MES_PRESCON4_1" & vbCrLf
sConsulta = sConsulta & "   DROP CONSTRAINT FK_AR_ITEM_MES_PRESCON4_1_ITEM"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE dbo.ITEM_OBJ" & vbCrLf
sConsulta = sConsulta & "   DROP CONSTRAINT FK_ITEM_OBJ_ITEM" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE dbo.ITEM_ESP" & vbCrLf
sConsulta = sConsulta & "   DROP CONSTRAINT FK_ITEM_ESP_ITEM" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE dbo.AR_ITEM_MES_PRESCON3_4" & vbCrLf
sConsulta = sConsulta & "   DROP CONSTRAINT FK_AR_ITEM_MES_PRESCON3_4_ITEM" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE dbo.AR_ITEM_MES_PRESCON3_3" & vbCrLf
sConsulta = sConsulta & "   DROP CONSTRAINT FK_AR_ITEM_MES_PRESCON3_3_ITEM" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE dbo.AR_ITEM_MES_PRESCON3_2" & vbCrLf
sConsulta = sConsulta & "   DROP CONSTRAINT FK_AR_ITEM_MES_PRESCON3_2_ITEM" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE dbo.AR_ITEM_MES_PRESCON3_1" & vbCrLf
sConsulta = sConsulta & "   DROP CONSTRAINT FK_AR_ITEM_MES_PRESCON3_1_ITEM" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE dbo.AR_ITEM_MES_PARPROY4" & vbCrLf
sConsulta = sConsulta & "   DROP CONSTRAINT FK_AR_ITEM_MES_PARPROY4_ITEM" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE dbo.AR_ITEM_MES_PARPROY3" & vbCrLf
sConsulta = sConsulta & "   DROP CONSTRAINT FK_AR_ITEM_MES_PARPROY3_ITEM" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE dbo.AR_ITEM_MES_PARPROY2" & vbCrLf
sConsulta = sConsulta & "   DROP CONSTRAINT FK_AR_ITEM_MES_PARPROY2_ITEM" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE dbo.AR_ITEM_MES_PARPROY1" & vbCrLf
sConsulta = sConsulta & "   DROP CONSTRAINT FK_AR_ITEM_MES_PARPROY1_ITEM" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE dbo.AR_ITEM_MES_PARCON4" & vbCrLf
sConsulta = sConsulta & "   DROP CONSTRAINT FK_AR_ITEM_MES_PARCON4_ITEM" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE dbo.AR_ITEM_MES_PARCON3" & vbCrLf
sConsulta = sConsulta & "   DROP CONSTRAINT FK_AR_ITEM_MES_PARCON3_ITEM" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE dbo.AR_ITEM_MES_PARCON2" & vbCrLf
sConsulta = sConsulta & "   DROP CONSTRAINT FK_AR_ITEM_MES_PARCON2_ITEM" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE dbo.AR_ITEM_MES_PARCON1" & vbCrLf
sConsulta = sConsulta & "   DROP CONSTRAINT FK_AR_ITEM_MES_PARCON1_ITEM" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE dbo.AR_ITEM_MES" & vbCrLf
sConsulta = sConsulta & "   DROP CONSTRAINT FK_AR_ITEM_MES_ITEM" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE dbo.AR_ITEM" & vbCrLf
sConsulta = sConsulta & "   DROP CONSTRAINT FK_AR_ITEM_ITEM" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE dbo.OFE_ITEM_ATRIB" & vbCrLf
sConsulta = sConsulta & "   DROP CONSTRAINT FK_OFE_ITEM_ATRIB_ITEM" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "DROP TABLE dbo.ITEM" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXECUTE sp_rename N'dbo.Tmp_ITEM', N'ITEM', 'OBJECT'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE CLUSTERED INDEX TEMP_PK_ITEM ON dbo.ITEM" & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   ANYO," & vbCrLf
sConsulta = sConsulta & "   GMN1," & vbCrLf
sConsulta = sConsulta & "   PROCE," & vbCrLf
sConsulta = sConsulta & "   ID" & vbCrLf
sConsulta = sConsulta & "   ) WITH FILLFACTOR = 90 ON [PRIMARY]" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE dbo.ITEM ADD CONSTRAINT" & vbCrLf
sConsulta = sConsulta & "   PK_ITEM PRIMARY KEY NONCLUSTERED " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   ANYO," & vbCrLf
sConsulta = sConsulta & "   GMN1," & vbCrLf
sConsulta = sConsulta & "   PROCE," & vbCrLf
sConsulta = sConsulta & "   ID" & vbCrLf
sConsulta = sConsulta & "   ) WITH FILLFACTOR = 90 ON [PRIMARY]" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE NONCLUSTERED INDEX IX_ITEM_01 ON dbo.ITEM" & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   ANYO," & vbCrLf
sConsulta = sConsulta & "   GMN1," & vbCrLf
sConsulta = sConsulta & "   GMN2," & vbCrLf
sConsulta = sConsulta & "   GMN3," & vbCrLf
sConsulta = sConsulta & "   GMN4," & vbCrLf
sConsulta = sConsulta & "   ART" & vbCrLf
sConsulta = sConsulta & "   ) WITH FILLFACTOR = 90 ON [PRIMARY]" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE NONCLUSTERED INDEX IX_ITEM_03 ON dbo.ITEM" & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   DEST" & vbCrLf
sConsulta = sConsulta & "   ) WITH FILLFACTOR = 90 ON [PRIMARY]" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE NONCLUSTERED INDEX IX_ITEM_04 ON dbo.ITEM" & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   UNI" & vbCrLf
sConsulta = sConsulta & "   ) WITH FILLFACTOR = 90 ON [PRIMARY]" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE NONCLUSTERED INDEX IX_ITEM_05 ON dbo.ITEM" & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   PAG" & vbCrLf
sConsulta = sConsulta & "   ) WITH FILLFACTOR = 90 ON [PRIMARY]" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE NONCLUSTERED INDEX IX_ITEM_02 ON dbo.ITEM" & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   GMN1," & vbCrLf
sConsulta = sConsulta & "   GMN2," & vbCrLf
sConsulta = sConsulta & "   GMN3," & vbCrLf
sConsulta = sConsulta & "   GMN4," & vbCrLf
sConsulta = sConsulta & "   ART" & vbCrLf
sConsulta = sConsulta & "   ) WITH FILLFACTOR = 90 ON [PRIMARY]" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE NONCLUSTERED INDEX IX_ITEM_06 ON dbo.ITEM" & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   ANYO," & vbCrLf
sConsulta = sConsulta & "   GMN1," & vbCrLf
sConsulta = sConsulta & "   PROCE," & vbCrLf
sConsulta = sConsulta & "   GRUPO" & vbCrLf
sConsulta = sConsulta & "   ) WITH FILLFACTOR = 90 ON [PRIMARY]" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE dbo.ITEM WITH NOCHECK ADD CONSTRAINT" & vbCrLf
sConsulta = sConsulta & "   FK_ITEM_PROCE FOREIGN KEY" & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   ANYO," & vbCrLf
sConsulta = sConsulta & "   GMN1," & vbCrLf
sConsulta = sConsulta & "   PROCE" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES dbo.PROCE" & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   ANYO," & vbCrLf
sConsulta = sConsulta & "   GMN1," & vbCrLf
sConsulta = sConsulta & "   COD" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE dbo.ITEM WITH NOCHECK ADD CONSTRAINT" & vbCrLf
sConsulta = sConsulta & "   FK_ITEM_PROCE_GRUPO FOREIGN KEY" & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   ANYO," & vbCrLf
sConsulta = sConsulta & "   GMN1," & vbCrLf
sConsulta = sConsulta & "   PROCE," & vbCrLf
sConsulta = sConsulta & "   GRUPO" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES dbo.PROCE_GRUPO" & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   ANYO," & vbCrLf
sConsulta = sConsulta & "   GMN1," & vbCrLf
sConsulta = sConsulta & "   PROCE," & vbCrLf
sConsulta = sConsulta & "   COD" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE dbo.ITEM WITH NOCHECK ADD CONSTRAINT" & vbCrLf
sConsulta = sConsulta & "   FK_ITEM_PROVE FOREIGN KEY" & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   PROVEACT" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES dbo.PROVE" & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   COD" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE dbo.ITEM WITH NOCHECK ADD CONSTRAINT" & vbCrLf
sConsulta = sConsulta & "   FK_ITEM_SOLICIT FOREIGN KEY" & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   SOLICIT" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES dbo.SOLICIT" & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   ID" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE dbo.ITEM WITH NOCHECK ADD CONSTRAINT" & vbCrLf
sConsulta = sConsulta & "   FK_ITEM_DEST FOREIGN KEY" & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   DEST" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES dbo.DEST" & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   COD" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "ALTER TABLE dbo.ITEM WITH NOCHECK ADD CONSTRAINT" & vbCrLf
sConsulta = sConsulta & "   FK_ITEM_PAG FOREIGN KEY" & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   PAG" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES dbo.PAG" & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   COD" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE dbo.ITEM WITH NOCHECK ADD CONSTRAINT" & vbCrLf
sConsulta = sConsulta & "   FK_ITEM_UNI FOREIGN KEY" & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   UNI" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES dbo.UNI" & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   COD" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE dbo.ITEM WITH NOCHECK ADD CONSTRAINT" & vbCrLf
sConsulta = sConsulta & "   FK_ITEM_ART4 FOREIGN KEY" & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   GMN1," & vbCrLf
sConsulta = sConsulta & "   GMN2," & vbCrLf
sConsulta = sConsulta & "   GMN3," & vbCrLf
sConsulta = sConsulta & "   GMN4," & vbCrLf
sConsulta = sConsulta & "   ART" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES dbo.ART4" & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   GMN1," & vbCrLf
sConsulta = sConsulta & "   GMN2," & vbCrLf
sConsulta = sConsulta & "   GMN3," & vbCrLf
sConsulta = sConsulta & "   GMN4," & vbCrLf
sConsulta = sConsulta & "   COD" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TRIGGER ITEM_TG_DEL ON dbo.ITEM" & vbCrLf
sConsulta = sConsulta & "FOR DELETE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN1 AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCE AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @FECINI AS DATETIME" & vbCrLf
sConsulta = sConsulta & "DECLARE @FECMIN AS DATETIME" & vbCrLf
sConsulta = sConsulta & "DECLARE @FECNECMANUAL  AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_ITEM_Del CURSOR FOR SELECT ANYO,GMN1,PROCE,ID,FECINI FROM DELETED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_ITEM_Del" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_ITEM_Del INTO @ANYO,@GMN1,@PROCE,@ID,@FECINI" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET  @FECNECMANUAL=(SELECT PROCE.FECNECMANUAL FROM PROCE  WHERE ANYO=@ANYO  AND GMN1=@GMN1 AND COD=@PROCE)" & vbCrLf
sConsulta = sConsulta & "IF @FECNECMANUAL=0" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET FECNEC =(SELECT MIN(FECINI) FROM ITEM WHERE  ANYO=@ANYO  AND GMN1=@GMN1 AND PROCE=@PROCE)" & vbCrLf
sConsulta = sConsulta & "WHERE COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "/* Si se han eliminado todos los  items, actualizamos el estado del proceso  si es que este era inferior a 3*/" & vbCrLf
sConsulta = sConsulta & "IF (SELECT PROCE.EST FROM PROCE WHERE ANYO=@ANYO  AND GMN1=@GMN1 AND COD=@PROCE) = 2  /* 3= Validado */" & vbCrLf
sConsulta = sConsulta & " BEGIN" & vbCrLf
sConsulta = sConsulta & "  IF (SELECT COUNT(*) FROM ITEM WHERE ANYO=@ANYO  AND GMN1=@GMN1 AND PROCE=@PROCE) = 0  /* 3= Validado */" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "    UPDATE PROCE SET EST =1  WHERE ANYO= @ANYO  AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "   End" & vbCrLf
sConsulta = sConsulta & " End" & vbCrLf
sConsulta = sConsulta & " IF (SELECT SUBASTA FROM PROCE_DEF WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE) = 1 " & vbCrLf
sConsulta = sConsulta & " BEGIN " & vbCrLf
sConsulta = sConsulta & "   DELETE DETALLE_PUJAS  " & vbCrLf
sConsulta = sConsulta & "     FROM DETALLE_PUJAS A " & vbCrLf
sConsulta = sConsulta & "     INNER JOIN DELETED B" & vbCrLf
sConsulta = sConsulta & "             ON A.ANYO=B.ANYO " & vbCrLf
sConsulta = sConsulta & "            AND A.GMN1= B.GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND A.PROCE=B.PROCE " & vbCrLf
sConsulta = sConsulta & "            AND A.ITEM=B.ID" & vbCrLf
sConsulta = sConsulta & "    WHERE A.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "      AND A.GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "      AND A.PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "   IF (SELECT COUNT(*) FROM (SELECT DISTINCT PROVE " & vbCrLf
sConsulta = sConsulta & "                               FROM OFEMIN " & vbCrLf
sConsulta = sConsulta & "                              WHERE ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "                                AND GMN1=@GMN1" & vbCrLf
sConsulta = sConsulta & "                                AND PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "                                AND PROVE IS NOT NULL) A)>1 " & vbCrLf
sConsulta = sConsulta & "     UPDATE PROCE_PROVE" & vbCrLf
sConsulta = sConsulta & "        SET SUPERADA = 1" & vbCrLf
sConsulta = sConsulta & "      WHERE ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "        AND GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "        AND PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "  ELSE" & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "      DECLARE @PROVE VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "      SELECT TOP 1 @PROVE = PROVE " & vbCrLf
sConsulta = sConsulta & "        FROM OFEMIN" & vbCrLf
sConsulta = sConsulta & "       WHERE ANYO=@ANYO" & vbCrLf
sConsulta = sConsulta & "         AND GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "         AND PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "      UPDATE PROCE_PROVE" & vbCrLf
sConsulta = sConsulta & "         SET SUPERADA = CASE WHEN A.PROVE=@PROVE THEN 0 ELSE 1 END" & vbCrLf
sConsulta = sConsulta & "        FROM PROCE_PROVE A" & vbCrLf
sConsulta = sConsulta & "       WHERE A.ANYO=@ANYO" & vbCrLf
sConsulta = sConsulta & "         AND A.GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "         AND A.PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "    END" & vbCrLf
sConsulta = sConsulta & "end" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_ITEM_Del INTO @ANYO,@GMN1,@PROCE,@ID,@FECINI" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_ITEM_Del" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_ITEM_Del" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE TRIGGER ITEM_TG_INS_DATVAR ON dbo.ITEM " & vbCrLf
sConsulta = sConsulta & "FOR INSERT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO SMALLINT,@GMN1 VARCHAR(5),@PROCE SMALLINT,@ITEM INTEGER,@GRUPO VARCHAR(5),@DATO VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @DEFPROVE SMALLINT,@DEFDISTUON SMALLINT,@DEFSOLICIT SMALLINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @DEFPRESANU1 SMALLINT,@DEFPRESANU2 SMALLINT,@DEFPRES1 SMALLINT,@DEFPRES2 SMALLINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVE VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @UON1 VARCHAR(5),@UON2 VARCHAR(5),@UON3 VARCHAR(5),@PORCEN FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYOPRES SMALLINT,@PRES1 VARCHAR(50),@PRES2 VARCHAR(50),@PRES3 VARCHAR(50),@PRES4 VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SUMA AS INTEGER,@SOLICIT INTEGER" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_ITEM_Ins_DATVAR CURSOR LOCAL FOR SELECT ANYO,GMN1,PROCE,ID,GRUPO FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_ITEM_Ins_DATVAR" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_ITEM_Ins_DATVAR INTO @ANYO,@GMN1,@PROCE,@ITEM,@GRUPO" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SELECT @DEFPROVE=PROVE,@DEFDISTUON=DIST" & vbCrLf
sConsulta = sConsulta & "    ,@DEFPRESANU1=PRESANU1,@DEFPRESANU2=PRESANU2,@DEFPRES1=PRES1,@DEFPRES2=PRES2" & vbCrLf
sConsulta = sConsulta & "   FROM PROCE_DEF WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- DEST,PAG Y FECSUM no es necesario actualizarlas porque al no poder" & vbCrLf
sConsulta = sConsulta & "-- introducir valores nulos en ITEM se hace desde la clase" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  --SOLICIT" & vbCrLf
sConsulta = sConsulta & "   IF @DEFSOLICIT=1 --Definido a nivel de proceso" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "       SELECT @SOLICIT=SOLICIT FROM PROCE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "           UPDATE ITEM SET SOLICIT=@SOLICIT WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ID=@ITEM" & vbCrLf
sConsulta = sConsulta & "        END" & vbCrLf
sConsulta = sConsulta & "   IF @DEFSOLICIT=2 --Definido a nivel de grupo" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "       SELECT @DATO=DATO FROM PROCE_GRUPO_DEF WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO AND DATO='SOLICIT'" & vbCrLf
sConsulta = sConsulta & "           IF @DATO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "               SELECT @SOLICIT=SOLICIT FROM PROCE_GRUPO WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND COD=@GRUPO" & vbCrLf
sConsulta = sConsulta & "           UPDATE ITEM SET SOLICIT=@SOLICIT WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ID=@ITEM" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "        END  " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   --PROVEACT" & vbCrLf
sConsulta = sConsulta & "   IF @DEFPROVE=1 --Definido a nivel de proceso" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "       SELECT @PROVE=PROVEACT FROM PROCE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "           UPDATE ITEM SET PROVEACT=@PROVE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ID=@ITEM" & vbCrLf
sConsulta = sConsulta & "        END" & vbCrLf
sConsulta = sConsulta & "   IF @DEFPROVE=2 --Definido a nivel de grupo" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "       SELECT @DATO=DATO FROM PROCE_GRUPO_DEF WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO AND DATO='PROVE'" & vbCrLf
sConsulta = sConsulta & "           IF @DATO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "               SELECT @PROVE=PROVEACT FROM PROCE_GRUPO WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND COD=@GRUPO" & vbCrLf
sConsulta = sConsulta & "           UPDATE ITEM SET PROVEACT=@PROVE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ID=@ITEM" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "        END  " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   --DISTUON" & vbCrLf
sConsulta = sConsulta & "   IF @DEFDISTUON=1 --Definido a nivel de proceso" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "       --UON1" & vbCrLf
sConsulta = sConsulta & "           INSERT INTO ITEM_UON1(ITEM,ANYO,GMN1,PROCE,UON1,PORCEN)" & vbCrLf
sConsulta = sConsulta & "           SELECT @ITEM,ANYO,GMN1,PROCE,UON1,PORCEN" & vbCrLf
sConsulta = sConsulta & "       FROM PROCE_UON1" & vbCrLf
sConsulta = sConsulta & "       WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "       --UON2" & vbCrLf
sConsulta = sConsulta & "           INSERT INTO ITEM_UON2(ITEM,ANYO,GMN1,PROCE,UON1,UON2,PORCEN)" & vbCrLf
sConsulta = sConsulta & "           SELECT @ITEM,ANYO,GMN1,PROCE,UON1,UON2,PORCEN" & vbCrLf
sConsulta = sConsulta & "       FROM PROCE_UON2" & vbCrLf
sConsulta = sConsulta & "       WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "       --UON3" & vbCrLf
sConsulta = sConsulta & "           INSERT INTO ITEM_UON3(ITEM,ANYO,GMN1,PROCE,UON1,UON2,UON3,PORCEN)" & vbCrLf
sConsulta = sConsulta & "           SELECT @ITEM,ANYO,GMN1,PROCE,UON1,UON2,UON3,PORCEN" & vbCrLf
sConsulta = sConsulta & "       FROM PROCE_UON3" & vbCrLf
sConsulta = sConsulta & "       WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "        END" & vbCrLf
sConsulta = sConsulta & "   IF @DEFDISTUON=2 --Definido a nivel de grupo" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "       SELECT @DATO=DATO FROM PROCE_GRUPO_DEF WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO AND DATO='DIST'" & vbCrLf
sConsulta = sConsulta & "           IF @DATO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "           --UON1" & vbCrLf
sConsulta = sConsulta & "           INSERT INTO ITEM_UON1(ITEM,ANYO,GMN1,PROCE,UON1,PORCEN)" & vbCrLf
sConsulta = sConsulta & "               SELECT @ITEM,ANYO,GMN1,PROCE,UON1,PORCEN" & vbCrLf
sConsulta = sConsulta & "           FROM PROCE_GR_UON1" & vbCrLf
sConsulta = sConsulta & "           WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "           --UON2" & vbCrLf
sConsulta = sConsulta & "           INSERT INTO ITEM_UON2(ITEM,ANYO,GMN1,PROCE,UON1,UON2,PORCEN)" & vbCrLf
sConsulta = sConsulta & "               SELECT @ITEM,ANYO,GMN1,PROCE,UON1,UON2,PORCEN" & vbCrLf
sConsulta = sConsulta & "           FROM PROCE_GR_UON2" & vbCrLf
sConsulta = sConsulta & "           WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "           --UON3" & vbCrLf
sConsulta = sConsulta & "           INSERT INTO ITEM_UON3(ITEM,ANYO,GMN1,PROCE,UON1,UON2,UON3,PORCEN)" & vbCrLf
sConsulta = sConsulta & "               SELECT @ITEM,ANYO,GMN1,PROCE,UON1,UON2,UON3,PORCEN" & vbCrLf
sConsulta = sConsulta & "           FROM PROCE_GR_UON3" & vbCrLf
sConsulta = sConsulta & "           WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "        END  " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   --PRESANU1" & vbCrLf
sConsulta = sConsulta & "   IF @DEFPRESANU1=1 --Definido a nivel de proceso" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           INSERT INTO ITEM_PRESPROY(ITEM,ANYO,GMN1,PROCE,PRES1,PRES2,PRES3,PRES4,PORCEN)" & vbCrLf
sConsulta = sConsulta & "           SELECT @ITEM,ANYO,GMN1,PROCE,PRES1,PRES2,PRES3,PRES4,PORCEN" & vbCrLf
sConsulta = sConsulta & "       FROM PROCE_PRES1" & vbCrLf
sConsulta = sConsulta & "       WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "        END" & vbCrLf
sConsulta = sConsulta & "   IF @DEFPRESANU1=2 --Definido a nivel de grupo" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "       SELECT @DATO=DATO FROM PROCE_GRUPO_DEF WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO AND DATO='PRESANU1'" & vbCrLf
sConsulta = sConsulta & "           IF @DATO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "           INSERT INTO ITEM_PRESPROY(ITEM,ANYO,GMN1,PROCE,PRES1,PRES2,PRES3,PRES4,PORCEN)" & vbCrLf
sConsulta = sConsulta & "               SELECT @ITEM,ANYO,GMN1,PROCE,PRES1,PRES2,PRES3,PRES4,PORCEN" & vbCrLf
sConsulta = sConsulta & "           FROM PROCE_GR_PRES1" & vbCrLf
sConsulta = sConsulta & "           WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "        END  " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   --PRESANU2" & vbCrLf
sConsulta = sConsulta & "   IF @DEFPRESANU2=1 --Definido a nivel de proceso" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           INSERT INTO ITEM_PRESCON(ITEM,ANYO,GMN1,PROCE,PRES1,PRES2,PRES3,PRES4,PORCEN)" & vbCrLf
sConsulta = sConsulta & "           SELECT @ITEM,ANYO,GMN1,PROCE,PRES1,PRES2,PRES3,PRES4,PORCEN" & vbCrLf
sConsulta = sConsulta & "       FROM PROCE_PRES2" & vbCrLf
sConsulta = sConsulta & "       WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "        END" & vbCrLf
sConsulta = sConsulta & "   IF @DEFPRESANU2=2 --Definido a nivel de grupo" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "       SELECT @DATO=DATO FROM PROCE_GRUPO_DEF WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO AND DATO='PRESANU2'" & vbCrLf
sConsulta = sConsulta & "           IF @DATO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "           INSERT INTO ITEM_PRESCON(ITEM,ANYO,GMN1,PROCE,PRES1,PRES2,PRES3,PRES4,PORCEN)" & vbCrLf
sConsulta = sConsulta & "               SELECT @ITEM,ANYO,GMN1,PROCE,PRES1,PRES2,PRES3,PRES4,PORCEN" & vbCrLf
sConsulta = sConsulta & "           FROM PROCE_GR_PRES2" & vbCrLf
sConsulta = sConsulta & "           WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO        " & vbCrLf
sConsulta = sConsulta & "   END  " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   --PRES1" & vbCrLf
sConsulta = sConsulta & "   IF @DEFPRES1=1 --Definido a nivel de proceso" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           INSERT INTO ITEM_PRESCON3(ITEM,ANYO,GMN1,PROCE,PRES1,PRES2,PRES3,PRES4,PORCEN)" & vbCrLf
sConsulta = sConsulta & "           SELECT @ITEM,ANYO,GMN1,PROCE,PRES1,PRES2,PRES3,PRES4,PORCEN" & vbCrLf
sConsulta = sConsulta & "       FROM PROCE_PRES3" & vbCrLf
sConsulta = sConsulta & "       WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "        END" & vbCrLf
sConsulta = sConsulta & "   IF @DEFPRES1=2 --Definido a nivel de grupo" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "       SELECT @DATO=DATO FROM PROCE_GRUPO_DEF WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO AND DATO='PRES1'" & vbCrLf
sConsulta = sConsulta & "           IF @DATO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "           INSERT INTO ITEM_PRESCON3(ITEM,ANYO,GMN1,PROCE,PRES1,PRES2,PRES3,PRES4,PORCEN)" & vbCrLf
sConsulta = sConsulta & "               SELECT @ITEM,ANYO,GMN1,PROCE,PRES1,PRES2,PRES3,PRES4,PORCEN" & vbCrLf
sConsulta = sConsulta & "           FROM PROCE_GR_PRES3" & vbCrLf
sConsulta = sConsulta & "           WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO        " & vbCrLf
sConsulta = sConsulta & "   END  " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   --PRES2" & vbCrLf
sConsulta = sConsulta & "   IF @DEFPRES2=1 --Definido a nivel de proceso" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           INSERT INTO ITEM_PRESCON4(ITEM,ANYO,GMN1,PROCE,PRES1,PRES2,PRES3,PRES4,PORCEN)" & vbCrLf
sConsulta = sConsulta & "           SELECT @ITEM,ANYO,GMN1,PROCE,PRES1,PRES2,PRES3,PRES4,PORCEN" & vbCrLf
sConsulta = sConsulta & "       FROM PROCE_PRES4" & vbCrLf
sConsulta = sConsulta & "       WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "        END" & vbCrLf
sConsulta = sConsulta & "   IF @DEFPRES2=2 --Definido a nivel de grupo" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "       SELECT @DATO=DATO FROM PROCE_GRUPO_DEF WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO AND DATO='PRES2'" & vbCrLf
sConsulta = sConsulta & "           IF @DATO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "           INSERT INTO ITEM_PRESCON4(ITEM,ANYO,GMN1,PROCE,PRES1,PRES2,PRES3,PRES4,PORCEN)" & vbCrLf
sConsulta = sConsulta & "               SELECT @ITEM,ANYO,GMN1,PROCE,PRES1,PRES2,PRES3,PRES4,PORCEN" & vbCrLf
sConsulta = sConsulta & "           FROM PROCE_GR_PRES4" & vbCrLf
sConsulta = sConsulta & "           WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO        " & vbCrLf
sConsulta = sConsulta & "        END  " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   --�ESP?" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_ITEM_Ins_DATVAR INTO @ANYO,@GMN1,@PROCE,@ITEM,@GRUPO" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_ITEM_Ins_DATVAR" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_ITEM_Ins_DATVAR" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE TRIGGER ITEM_TG_UPD ON dbo.ITEM" & vbCrLf
sConsulta = sConsulta & "FOR UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN1 AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCE AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN2 AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN3 AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN4 AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @ART AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @CANT AS FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @FECINI AS DATETIME" & vbCrLf
sConsulta = sConsulta & "DECLARE @FECFIN AS DATETIME" & vbCrLf
sConsulta = sConsulta & "DECLARE @EST AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @OBJ AS FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @OBJOLD AS FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @FECNECMANUAL  AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_ITEM_Upd CURSOR FOR SELECT ANYO,GMN1,PROCE,ID,GMN2,GMN3,GMN4,ART,CANT,FECINI,FECFIN,OBJ FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_ITEM_Upd" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_ITEM_Upd INTO @ANYO,@GMN1,@PROCE,@ID,@GMN2,@GMN3,@GMN4,@ART,@CANT,@FECINI,@FECFIN,@OBJ" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET  @FECNECMANUAL=(SELECT PROCE.FECNECMANUAL FROM PROCE  WHERE ANYO=@ANYO  AND GMN1=@GMN1 AND COD=@PROCE)" & vbCrLf
sConsulta = sConsulta & "IF @FECNECMANUAL=0" & vbCrLf
sConsulta = sConsulta & "  UPDATE PROCE SET FECNEC =(SELECT MIN(FECINI) FROM ITEM WHERE ANYO=@ANYO  AND GMN1=@GMN1 AND PROCE=@PROCE)" & vbCrLf
sConsulta = sConsulta & "   WHERE COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF UPDATE(OBJ)" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @EST = (SELECT EST FROM PROCE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE)" & vbCrLf
sConsulta = sConsulta & "SET @OBJOLD = (SELECT OBJ FROM DELETED WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ID=@ID)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @OBJ IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "/*Si estamos en estado 8 y borramos el �ltimoobjetivo entonces pasamos a estado 7*/" & vbCrLf
sConsulta = sConsulta & "IF @EST = 8 AND ((SELECT COUNT(*) FROM ITEM WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND OBJ IS NOT NULL )=0) UPDATE PROCE SET EST=7 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "IF @EST = 9  AND ((SELECT COUNT(*) FROM ITEM WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND OBJ IS NOT NULL )=0) UPDATE PROCE SET EST=10 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "IF @OBJOLD IS NOT NULL AND ((SELECT COUNT(*) FROM ITEM WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND OBJ IS NOT NULL )=0) UPDATE PROCE_PROVE SET NUMOBJ=0,OBJNUEVOS=0 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Else" & vbCrLf
sConsulta = sConsulta & "IF @OBJOLD IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @EST = 7 UPDATE PROCE SET EST=8 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "IF @EST = 10 UPDATE PROCE SET EST=9 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE SET OBJNUEVOS=1,NUMOBJ=NUMOBJ+1 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Else" & vbCrLf
sConsulta = sConsulta & "IF @OBJOLD <> @OBJ" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @EST = 7 UPDATE PROCE SET EST=8 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "IF @EST = 10 UPDATE PROCE SET EST=9 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE SET OBJNUEVOS=1,NUMOBJ=NUMOBJ+1 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "_FETCH:" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_ITEM_Upd INTO @ANYO,@GMN1,@PROCE,@ID,@GMN2,@GMN3,@GMN4,@ART,@CANT,@FECINI,@FECFIN,@OBJ" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_ITEM_Upd" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_ITEM_Upd" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE TRIGGER ITEM_TG_INSUPD ON dbo.ITEM" & vbCrLf
sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO INT,@GMN1 VARCHAR(50),@PROCE INT, @ID INT" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_ITEM_InsUpd CURSOR FOR SELECT ANYO,GMN1,PROCE,ID FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_ITEM_InsUpd" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_ITEM_InsUpd INTO @ANYO,@GMN1,@PROCE,@ID" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM SET FECACT=GETDATE() WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ID=@ID" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_ITEM_InsUpd INTO @ANYO,@GMN1,@PROCE,@ID" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_ITEM_InsUpd" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_ITEM_InsUpd" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE TRIGGER ITEM_TG_INS ON dbo.ITEM" & vbCrLf
sConsulta = sConsulta & "FOR INSERT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN1 AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCE AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN2 AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN3 AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN4 AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @ART AS VARCHAR(20)" & vbCrLf
sConsulta = sConsulta & "DECLARE @CANT AS FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @FECINI AS DATETIME" & vbCrLf
sConsulta = sConsulta & "DECLARE @ESTADOPROCESO AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @FECNECMANUAL  AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_ITEM_Ins CURSOR FOR SELECT ANYO,GMN1,PROCE,ID,GMN2,GMN3,GMN4,ART,CANT,FECINI FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_ITEM_Ins" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_ITEM_Ins INTO @ANYO,@GMN1,@PROCE,@ID,@GMN2,@GMN3,@GMN4,@ART,@CANT,@FECINI" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SELECT @ESTADOPROCESO=PROCE.EST, @FECNECMANUAL=PROCE.FECNECMANUAL FROM PROCE  WHERE ANYO=@ANYO  AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "/* SET @ESTADOPROCESO=  (SELECT PROCE.EST FROM PROCE WHERE ANYO=@ANYO  AND GMN1=@GMN1 AND COD=@PROCE)*/" & vbCrLf
sConsulta = sConsulta & "/* Si se han insertado items, actualizamos el estado del proceso  si es que este era inferior a 2*/" & vbCrLf
sConsulta = sConsulta & "IF @ESTADOPROCESO < 2  /* 3= Validado */" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET EST = 2  WHERE ANYO= @ANYO  AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "Else" & vbCrLf
sConsulta = sConsulta & "IF @ESTADOPROCESO >=7" & vbCrLf
sConsulta = sConsulta & "INSERT INTO ITEM_OFE (ANYO,GMN1,PROCE,ITEM,PROVE,NUM,ULT)  " & vbCrLf
sConsulta = sConsulta & "   SELECT @ANYO,@GMN1,@PROCE,@ID,PROVE,OFE,ULT From PROCE_OFE" & vbCrLf
sConsulta = sConsulta & "   WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "IF @FECNECMANUAL=0" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET FECNEC =(SELECT MIN(FECINI) FROM ITEM WHERE ANYO= @ANYO  AND GMN1=@GMN1 AND PROCE=@PROCE)" & vbCrLf
sConsulta = sConsulta & "WHERE COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "_FETCH:" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_ITEM_Ins INTO @ANYO,@GMN1,@PROCE,@ID,@GMN2,@GMN3,@GMN4,@ART,@CANT,@FECINI" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_ITEM_Ins" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_ITEM_Ins" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE dbo.OFE_ITEM_ATRIB WITH NOCHECK ADD CONSTRAINT" & vbCrLf
sConsulta = sConsulta & "   FK_OFE_ITEM_ATRIB_ITEM FOREIGN KEY" & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   ANYO," & vbCrLf
sConsulta = sConsulta & "   GMN1," & vbCrLf
sConsulta = sConsulta & "   PROCE," & vbCrLf
sConsulta = sConsulta & "   ITEM" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES dbo.ITEM" & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   ANYO," & vbCrLf
sConsulta = sConsulta & "   GMN1," & vbCrLf
sConsulta = sConsulta & "   PROCE," & vbCrLf
sConsulta = sConsulta & "   ID" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE dbo.AR_ITEM WITH NOCHECK ADD CONSTRAINT" & vbCrLf
sConsulta = sConsulta & "   FK_AR_ITEM_ITEM FOREIGN KEY" & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   ANYO," & vbCrLf
sConsulta = sConsulta & "   GMN1," & vbCrLf
sConsulta = sConsulta & "   PROCE," & vbCrLf
sConsulta = sConsulta & "   ITEM" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES dbo.ITEM" & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   ANYO," & vbCrLf
sConsulta = sConsulta & "   GMN1," & vbCrLf
sConsulta = sConsulta & "   PROCE," & vbCrLf
sConsulta = sConsulta & "   ID" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE dbo.AR_ITEM_MES WITH NOCHECK ADD CONSTRAINT" & vbCrLf
sConsulta = sConsulta & "   FK_AR_ITEM_MES_ITEM FOREIGN KEY" & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   ANYO," & vbCrLf
sConsulta = sConsulta & "   GMN1," & vbCrLf
sConsulta = sConsulta & "   PROCE," & vbCrLf
sConsulta = sConsulta & "   ITEM" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES dbo.ITEM" & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   ANYO," & vbCrLf
sConsulta = sConsulta & "   GMN1," & vbCrLf
sConsulta = sConsulta & "   PROCE," & vbCrLf
sConsulta = sConsulta & "   ID" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE dbo.AR_ITEM_MES_PARCON1 WITH NOCHECK ADD CONSTRAINT" & vbCrLf
sConsulta = sConsulta & "   FK_AR_ITEM_MES_PARCON1_ITEM FOREIGN KEY" & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   ANYO," & vbCrLf
sConsulta = sConsulta & "   GMN1," & vbCrLf
sConsulta = sConsulta & "   PROCE," & vbCrLf
sConsulta = sConsulta & "   ITEM" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES dbo.ITEM" & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   ANYO," & vbCrLf
sConsulta = sConsulta & "   GMN1," & vbCrLf
sConsulta = sConsulta & "   PROCE," & vbCrLf
sConsulta = sConsulta & "   ID" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE dbo.AR_ITEM_MES_PARCON2 WITH NOCHECK ADD CONSTRAINT" & vbCrLf
sConsulta = sConsulta & "   FK_AR_ITEM_MES_PARCON2_ITEM FOREIGN KEY" & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   ANYO," & vbCrLf
sConsulta = sConsulta & "   GMN1," & vbCrLf
sConsulta = sConsulta & "   PROCE," & vbCrLf
sConsulta = sConsulta & "   ITEM" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES dbo.ITEM" & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   ANYO," & vbCrLf
sConsulta = sConsulta & "   GMN1," & vbCrLf
sConsulta = sConsulta & "   PROCE," & vbCrLf
sConsulta = sConsulta & "   ID" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE dbo.AR_ITEM_MES_PARCON3 WITH NOCHECK ADD CONSTRAINT" & vbCrLf
sConsulta = sConsulta & "   FK_AR_ITEM_MES_PARCON3_ITEM FOREIGN KEY" & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   ANYO," & vbCrLf
sConsulta = sConsulta & "   GMN1," & vbCrLf
sConsulta = sConsulta & "   PROCE," & vbCrLf
sConsulta = sConsulta & "   ITEM" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES dbo.ITEM" & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   ANYO," & vbCrLf
sConsulta = sConsulta & "   GMN1," & vbCrLf
sConsulta = sConsulta & "   PROCE," & vbCrLf
sConsulta = sConsulta & "   ID" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE dbo.AR_ITEM_MES_PARCON4 WITH NOCHECK ADD CONSTRAINT" & vbCrLf
sConsulta = sConsulta & "   FK_AR_ITEM_MES_PARCON4_ITEM FOREIGN KEY" & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   ANYO," & vbCrLf
sConsulta = sConsulta & "   GMN1," & vbCrLf
sConsulta = sConsulta & "   PROCE," & vbCrLf
sConsulta = sConsulta & "   ITEM" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES dbo.ITEM" & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   ANYO," & vbCrLf
sConsulta = sConsulta & "   GMN1," & vbCrLf
sConsulta = sConsulta & "   PROCE," & vbCrLf
sConsulta = sConsulta & "   ID" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE dbo.AR_ITEM_MES_PARPROY1 WITH NOCHECK ADD CONSTRAINT" & vbCrLf
sConsulta = sConsulta & "   FK_AR_ITEM_MES_PARPROY1_ITEM FOREIGN KEY" & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   ANYO," & vbCrLf
sConsulta = sConsulta & "   GMN1," & vbCrLf
sConsulta = sConsulta & "   PROCE," & vbCrLf
sConsulta = sConsulta & "   ITEM" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES dbo.ITEM" & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   ANYO," & vbCrLf
sConsulta = sConsulta & "   GMN1," & vbCrLf
sConsulta = sConsulta & "   PROCE," & vbCrLf
sConsulta = sConsulta & "   ID" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE dbo.AR_ITEM_MES_PARPROY2 WITH NOCHECK ADD CONSTRAINT" & vbCrLf
sConsulta = sConsulta & "   FK_AR_ITEM_MES_PARPROY2_ITEM FOREIGN KEY" & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   ANYO," & vbCrLf
sConsulta = sConsulta & "   GMN1," & vbCrLf
sConsulta = sConsulta & "   PROCE," & vbCrLf
sConsulta = sConsulta & "   ITEM" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES dbo.ITEM" & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   ANYO," & vbCrLf
sConsulta = sConsulta & "   GMN1," & vbCrLf
sConsulta = sConsulta & "   PROCE," & vbCrLf
sConsulta = sConsulta & "   ID" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE dbo.AR_ITEM_MES_PARPROY3 WITH NOCHECK ADD CONSTRAINT" & vbCrLf
sConsulta = sConsulta & "   FK_AR_ITEM_MES_PARPROY3_ITEM FOREIGN KEY" & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   ANYO," & vbCrLf
sConsulta = sConsulta & "   GMN1," & vbCrLf
sConsulta = sConsulta & "   PROCE," & vbCrLf
sConsulta = sConsulta & "   ITEM" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES dbo.ITEM" & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   ANYO," & vbCrLf
sConsulta = sConsulta & "   GMN1," & vbCrLf
sConsulta = sConsulta & "   PROCE," & vbCrLf
sConsulta = sConsulta & "   ID" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE dbo.AR_ITEM_MES_PARPROY4 WITH NOCHECK ADD CONSTRAINT" & vbCrLf
sConsulta = sConsulta & "   FK_AR_ITEM_MES_PARPROY4_ITEM FOREIGN KEY" & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   ANYO," & vbCrLf
sConsulta = sConsulta & "   GMN1," & vbCrLf
sConsulta = sConsulta & "   PROCE," & vbCrLf
sConsulta = sConsulta & "   ITEM" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES dbo.ITEM" & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   ANYO," & vbCrLf
sConsulta = sConsulta & "   GMN1," & vbCrLf
sConsulta = sConsulta & "   PROCE," & vbCrLf
sConsulta = sConsulta & "   ID" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE dbo.AR_ITEM_MES_PRESCON3_1 WITH NOCHECK ADD CONSTRAINT" & vbCrLf
sConsulta = sConsulta & "   FK_AR_ITEM_MES_PRESCON3_1_ITEM FOREIGN KEY" & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   ANYO," & vbCrLf
sConsulta = sConsulta & "   GMN1," & vbCrLf
sConsulta = sConsulta & "   PROCE," & vbCrLf
sConsulta = sConsulta & "   ITEM" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES dbo.ITEM" & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   ANYO," & vbCrLf
sConsulta = sConsulta & "   GMN1," & vbCrLf
sConsulta = sConsulta & "   PROCE," & vbCrLf
sConsulta = sConsulta & "   ID" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE dbo.AR_ITEM_MES_PRESCON3_2 WITH NOCHECK ADD CONSTRAINT" & vbCrLf
sConsulta = sConsulta & "   FK_AR_ITEM_MES_PRESCON3_2_ITEM FOREIGN KEY" & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   ANYO," & vbCrLf
sConsulta = sConsulta & "   GMN1," & vbCrLf
sConsulta = sConsulta & "   PROCE," & vbCrLf
sConsulta = sConsulta & "   ITEM" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES dbo.ITEM" & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   ANYO," & vbCrLf
sConsulta = sConsulta & "   GMN1," & vbCrLf
sConsulta = sConsulta & "   PROCE," & vbCrLf
sConsulta = sConsulta & "   ID" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE dbo.AR_ITEM_MES_PRESCON3_3 WITH NOCHECK ADD CONSTRAINT" & vbCrLf
sConsulta = sConsulta & "   FK_AR_ITEM_MES_PRESCON3_3_ITEM FOREIGN KEY" & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   ANYO," & vbCrLf
sConsulta = sConsulta & "   GMN1," & vbCrLf
sConsulta = sConsulta & "   PROCE," & vbCrLf
sConsulta = sConsulta & "   ITEM" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES dbo.ITEM" & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   ANYO," & vbCrLf
sConsulta = sConsulta & "   GMN1," & vbCrLf
sConsulta = sConsulta & "   PROCE," & vbCrLf
sConsulta = sConsulta & "   ID" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE dbo.AR_ITEM_MES_PRESCON3_4 WITH NOCHECK ADD CONSTRAINT" & vbCrLf
sConsulta = sConsulta & "   FK_AR_ITEM_MES_PRESCON3_4_ITEM FOREIGN KEY" & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   ANYO," & vbCrLf
sConsulta = sConsulta & "   GMN1," & vbCrLf
sConsulta = sConsulta & "   PROCE," & vbCrLf
sConsulta = sConsulta & "   ITEM" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES dbo.ITEM" & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   ANYO," & vbCrLf
sConsulta = sConsulta & "   GMN1," & vbCrLf
sConsulta = sConsulta & "   PROCE," & vbCrLf
sConsulta = sConsulta & "   ID" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE dbo.ITEM_ESP WITH NOCHECK ADD CONSTRAINT" & vbCrLf
sConsulta = sConsulta & "   FK_ITEM_ESP_ITEM FOREIGN KEY" & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   ANYO," & vbCrLf
sConsulta = sConsulta & "   GMN1," & vbCrLf
sConsulta = sConsulta & "   PROCE," & vbCrLf
sConsulta = sConsulta & "   ITEM" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES dbo.ITEM" & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   ANYO," & vbCrLf
sConsulta = sConsulta & "   GMN1," & vbCrLf
sConsulta = sConsulta & "   PROCE," & vbCrLf
sConsulta = sConsulta & "   ID" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE dbo.ITEM_OBJ WITH NOCHECK ADD CONSTRAINT" & vbCrLf
sConsulta = sConsulta & "   FK_ITEM_OBJ_ITEM FOREIGN KEY" & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   ANYO," & vbCrLf
sConsulta = sConsulta & "   GMN1," & vbCrLf
sConsulta = sConsulta & "   PROCE," & vbCrLf
sConsulta = sConsulta & "   ITEM" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES dbo.ITEM" & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   ANYO," & vbCrLf
sConsulta = sConsulta & "   GMN1," & vbCrLf
sConsulta = sConsulta & "   PROCE," & vbCrLf
sConsulta = sConsulta & "   ID" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE dbo.AR_ITEM_MES_PRESCON4_1 WITH NOCHECK ADD CONSTRAINT" & vbCrLf
sConsulta = sConsulta & "   FK_AR_ITEM_MES_PRESCON4_1_ITEM FOREIGN KEY" & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   ANYO," & vbCrLf
sConsulta = sConsulta & "   GMN1," & vbCrLf
sConsulta = sConsulta & "   PROCE," & vbCrLf
sConsulta = sConsulta & "   ITEM" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES dbo.ITEM" & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   ANYO," & vbCrLf
sConsulta = sConsulta & "   GMN1," & vbCrLf
sConsulta = sConsulta & "   PROCE," & vbCrLf
sConsulta = sConsulta & "   ID" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE dbo.ITEM_OFE WITH NOCHECK ADD CONSTRAINT" & vbCrLf
sConsulta = sConsulta & "   FK_ITEM_OFE_ITEM FOREIGN KEY" & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   ANYO," & vbCrLf
sConsulta = sConsulta & "   GMN1," & vbCrLf
sConsulta = sConsulta & "   PROCE," & vbCrLf
sConsulta = sConsulta & "   ITEM" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES dbo.ITEM" & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   ANYO," & vbCrLf
sConsulta = sConsulta & "   GMN1," & vbCrLf
sConsulta = sConsulta & "   PROCE," & vbCrLf
sConsulta = sConsulta & "   ID" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE dbo.AR_ITEM_MES_PRESCON4_2 WITH NOCHECK ADD CONSTRAINT" & vbCrLf
sConsulta = sConsulta & "   FK_AR_ITEM_MES_PRESCON4_2_ITEM FOREIGN KEY" & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   ANYO," & vbCrLf
sConsulta = sConsulta & "   GMN1," & vbCrLf
sConsulta = sConsulta & "   PROCE," & vbCrLf
sConsulta = sConsulta & "   ITEM" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES dbo.ITEM" & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   ANYO," & vbCrLf
sConsulta = sConsulta & "   GMN1," & vbCrLf
sConsulta = sConsulta & "   PROCE," & vbCrLf
sConsulta = sConsulta & "   ID" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE dbo.ITEM_PRES WITH NOCHECK ADD CONSTRAINT" & vbCrLf
sConsulta = sConsulta & "   FK_ITEM_PRES_ITEM FOREIGN KEY" & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   ANYO," & vbCrLf
sConsulta = sConsulta & "   GMN1," & vbCrLf
sConsulta = sConsulta & "   PROCE," & vbCrLf
sConsulta = sConsulta & "   ITEM" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES dbo.ITEM" & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   ANYO," & vbCrLf
sConsulta = sConsulta & "   GMN1," & vbCrLf
sConsulta = sConsulta & "   PROCE," & vbCrLf
sConsulta = sConsulta & "   ID" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE dbo.AR_ITEM_MES_PRESCON4_3 WITH NOCHECK ADD CONSTRAINT" & vbCrLf
sConsulta = sConsulta & "   FK_AR_ITEM_MES_PRESCON4_3_ITEM FOREIGN KEY" & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   ANYO," & vbCrLf
sConsulta = sConsulta & "   GMN1," & vbCrLf
sConsulta = sConsulta & "   PROCE," & vbCrLf
sConsulta = sConsulta & "   ITEM" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES dbo.ITEM" & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   ANYO," & vbCrLf
sConsulta = sConsulta & "   GMN1," & vbCrLf
sConsulta = sConsulta & "   PROCE," & vbCrLf
sConsulta = sConsulta & "   ID" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE dbo.AR_ITEM_MES_PRESCON4_4 WITH NOCHECK ADD CONSTRAINT" & vbCrLf
sConsulta = sConsulta & "   FK_AR_ITEM_MES_PRESCON4_4_ITEM FOREIGN KEY" & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   ANYO," & vbCrLf
sConsulta = sConsulta & "   GMN1," & vbCrLf
sConsulta = sConsulta & "   PROCE," & vbCrLf
sConsulta = sConsulta & "   ITEM" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES dbo.ITEM" & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   ANYO," & vbCrLf
sConsulta = sConsulta & "   GMN1," & vbCrLf
sConsulta = sConsulta & "   PROCE," & vbCrLf
sConsulta = sConsulta & "   ID" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE dbo.ITEM_PRESCON WITH NOCHECK ADD CONSTRAINT" & vbCrLf
sConsulta = sConsulta & "   FK_ITEM_PRESCON_ITEM FOREIGN KEY" & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   ANYO," & vbCrLf
sConsulta = sConsulta & "   GMN1," & vbCrLf
sConsulta = sConsulta & "   PROCE," & vbCrLf
sConsulta = sConsulta & "   ITEM" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES dbo.ITEM" & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   ANYO," & vbCrLf
sConsulta = sConsulta & "   GMN1," & vbCrLf
sConsulta = sConsulta & "   PROCE," & vbCrLf
sConsulta = sConsulta & "   ID" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE dbo.ITEM_PRESCON3 WITH NOCHECK ADD CONSTRAINT" & vbCrLf
sConsulta = sConsulta & "   FK_ITEM_PRESCON3_ITEM FOREIGN KEY" & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   ANYO," & vbCrLf
sConsulta = sConsulta & "   GMN1," & vbCrLf
sConsulta = sConsulta & "   PROCE," & vbCrLf
sConsulta = sConsulta & "   ITEM" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES dbo.ITEM" & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   ANYO," & vbCrLf
sConsulta = sConsulta & "   GMN1," & vbCrLf
sConsulta = sConsulta & "   PROCE," & vbCrLf
sConsulta = sConsulta & "   ID" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE dbo.AR_ITEM_MES_UON1 WITH NOCHECK ADD CONSTRAINT" & vbCrLf
sConsulta = sConsulta & "   FK_AR_ITEM_MES_UON1_ITEM FOREIGN KEY" & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   ANYO," & vbCrLf
sConsulta = sConsulta & "   GMN1," & vbCrLf
sConsulta = sConsulta & "   PROCE," & vbCrLf
sConsulta = sConsulta & "   ITEM" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES dbo.ITEM" & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   ANYO," & vbCrLf
sConsulta = sConsulta & "   GMN1," & vbCrLf
sConsulta = sConsulta & "   PROCE," & vbCrLf
sConsulta = sConsulta & "   ID" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE dbo.AR_ITEM_MES_UON2 WITH NOCHECK ADD CONSTRAINT" & vbCrLf
sConsulta = sConsulta & "   FK_AR_ITEM_MES_UON2_ITEM FOREIGN KEY" & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   ANYO," & vbCrLf
sConsulta = sConsulta & "   GMN1," & vbCrLf
sConsulta = sConsulta & "   PROCE," & vbCrLf
sConsulta = sConsulta & "   ITEM" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES dbo.ITEM" & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   ANYO," & vbCrLf
sConsulta = sConsulta & "   GMN1," & vbCrLf
sConsulta = sConsulta & "   PROCE," & vbCrLf
sConsulta = sConsulta & "   ID" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE dbo.ITEM_PRESCON4 WITH NOCHECK ADD CONSTRAINT" & vbCrLf
sConsulta = sConsulta & "   FK_ITEM_PRESCON4_ITEM FOREIGN KEY" & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   ANYO," & vbCrLf
sConsulta = sConsulta & "   GMN1," & vbCrLf
sConsulta = sConsulta & "   PROCE," & vbCrLf
sConsulta = sConsulta & "   ITEM" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES dbo.ITEM" & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   ANYO," & vbCrLf
sConsulta = sConsulta & "   GMN1," & vbCrLf
sConsulta = sConsulta & "   PROCE," & vbCrLf
sConsulta = sConsulta & "   ID" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE dbo.AR_ITEM_MES_UON3 WITH NOCHECK ADD CONSTRAINT" & vbCrLf
sConsulta = sConsulta & "   FK_AR_ITEM_MES_UON3_ITEM FOREIGN KEY" & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   ANYO," & vbCrLf
sConsulta = sConsulta & "   GMN1," & vbCrLf
sConsulta = sConsulta & "   PROCE," & vbCrLf
sConsulta = sConsulta & "   ITEM" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES dbo.ITEM" & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   ANYO," & vbCrLf
sConsulta = sConsulta & "   GMN1," & vbCrLf
sConsulta = sConsulta & "   PROCE," & vbCrLf
sConsulta = sConsulta & "   ID" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE dbo.AR_ITEM_PRESCON3 WITH NOCHECK ADD CONSTRAINT" & vbCrLf
sConsulta = sConsulta & "   FK_AR_ITEM_PRESCON3_ITEM FOREIGN KEY" & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   ANYO," & vbCrLf
sConsulta = sConsulta & "   GMN1," & vbCrLf
sConsulta = sConsulta & "   PROCE," & vbCrLf
sConsulta = sConsulta & "   ITEM" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES dbo.ITEM" & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   ANYO," & vbCrLf
sConsulta = sConsulta & "   GMN1," & vbCrLf
sConsulta = sConsulta & "   PROCE," & vbCrLf
sConsulta = sConsulta & "   ID" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE dbo.ITEM_PRESPROY WITH NOCHECK ADD CONSTRAINT" & vbCrLf
sConsulta = sConsulta & "   FK_ITEM_PRESPROY_ITEM FOREIGN KEY" & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   ANYO," & vbCrLf
sConsulta = sConsulta & "   GMN1," & vbCrLf
sConsulta = sConsulta & "   PROCE," & vbCrLf
sConsulta = sConsulta & "   ITEM" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES dbo.ITEM" & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   ANYO," & vbCrLf
sConsulta = sConsulta & "   GMN1," & vbCrLf
sConsulta = sConsulta & "   PROCE," & vbCrLf
sConsulta = sConsulta & "   ID" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE dbo.ITEM_UON1 WITH NOCHECK ADD CONSTRAINT" & vbCrLf
sConsulta = sConsulta & "   FK_ITEM_UON1_ITEM FOREIGN KEY" & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   ANYO," & vbCrLf
sConsulta = sConsulta & "   GMN1," & vbCrLf
sConsulta = sConsulta & "   PROCE," & vbCrLf
sConsulta = sConsulta & "   ITEM" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES dbo.ITEM" & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   ANYO," & vbCrLf
sConsulta = sConsulta & "   GMN1," & vbCrLf
sConsulta = sConsulta & "   PROCE," & vbCrLf
sConsulta = sConsulta & "   ID" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE dbo.AR_ITEM_PRESCON4 WITH NOCHECK ADD CONSTRAINT" & vbCrLf
sConsulta = sConsulta & "   FK_AR_ITEM_PRESCON4_ITEM FOREIGN KEY" & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   ANYO," & vbCrLf
sConsulta = sConsulta & "   GMN1," & vbCrLf
sConsulta = sConsulta & "   PROCE," & vbCrLf
sConsulta = sConsulta & "   ITEM" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES dbo.ITEM" & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   ANYO," & vbCrLf
sConsulta = sConsulta & "   GMN1," & vbCrLf
sConsulta = sConsulta & "   PROCE," & vbCrLf
sConsulta = sConsulta & "   ID" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE dbo.ITEM_UON2 WITH NOCHECK ADD CONSTRAINT" & vbCrLf
sConsulta = sConsulta & "   FK_ITEM_UON2_ITEM FOREIGN KEY" & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   ANYO," & vbCrLf
sConsulta = sConsulta & "   GMN1," & vbCrLf
sConsulta = sConsulta & "   PROCE," & vbCrLf
sConsulta = sConsulta & "   ITEM" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES dbo.ITEM" & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   ANYO," & vbCrLf
sConsulta = sConsulta & "   GMN1," & vbCrLf
sConsulta = sConsulta & "   PROCE," & vbCrLf
sConsulta = sConsulta & "   ID" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE dbo.ITEM_UON3 WITH NOCHECK ADD CONSTRAINT" & vbCrLf
sConsulta = sConsulta & "   FK_ITEM_UON3_ITEM FOREIGN KEY" & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   ANYO," & vbCrLf
sConsulta = sConsulta & "   GMN1," & vbCrLf
sConsulta = sConsulta & "   PROCE," & vbCrLf
sConsulta = sConsulta & "   ITEM" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES dbo.ITEM" & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   ANYO," & vbCrLf
sConsulta = sConsulta & "   GMN1," & vbCrLf
sConsulta = sConsulta & "   PROCE," & vbCrLf
sConsulta = sConsulta & "   ID" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE dbo.AR_ITEM_PROVE WITH NOCHECK ADD CONSTRAINT" & vbCrLf
sConsulta = sConsulta & "   FK_AR_ITEM_PROVE_ITEM FOREIGN KEY" & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   ANYO," & vbCrLf
sConsulta = sConsulta & "   GMN1," & vbCrLf
sConsulta = sConsulta & "   PROCE," & vbCrLf
sConsulta = sConsulta & "   ITEM" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES dbo.ITEM" & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   ANYO," & vbCrLf
sConsulta = sConsulta & "   GMN1," & vbCrLf
sConsulta = sConsulta & "   PROCE," & vbCrLf
sConsulta = sConsulta & "   ID" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

If frmProgreso.ProgressBar1.Value >= frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

'LOG_ADJ
sConsulta = "ALTER TABLE LOG_ADJ ALTER COLUMN ESP VARCHAR(800)"
ExecuteSQL gRDOCon, sConsulta

If frmProgreso.ProgressBar1.Value >= frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

'Pasa el campo ESP de LOG_ITEM_ADJ de text a varchar(800)
sConsulta = "CREATE TABLE dbo.Tmp_LOG_ITEM_ADJ" & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   ID int NOT NULL IDENTITY (1, 1)," & vbCrLf
sConsulta = sConsulta & "   ID_LOG_ADJ int NOT NULL," & vbCrLf
sConsulta = sConsulta & "   ID_ITEM int NOT NULL," & vbCrLf
sConsulta = sConsulta & "   ART varchar(" & gLongitudesDeCodigos.giLongCodART & ") NULL," & vbCrLf
sConsulta = sConsulta & "   DESCR varchar(200) NULL," & vbCrLf
sConsulta = sConsulta & "   DEST varchar(" & gLongitudesDeCodigos.giLongCodDEST & ") NOT NULL," & vbCrLf
sConsulta = sConsulta & "   UNI varchar(" & gLongitudesDeCodigos.giLongCodUNI & ") NOT NULL," & vbCrLf
sConsulta = sConsulta & "   PAG varchar(" & gLongitudesDeCodigos.giLongCodPAG & ") NOT NULL," & vbCrLf
sConsulta = sConsulta & "   CANT float NOT NULL," & vbCrLf
sConsulta = sConsulta & "   CANT_ADJ float NOT NULL," & vbCrLf
sConsulta = sConsulta & "   PREC_VALIDO_ADJ float NOT NULL," & vbCrLf
sConsulta = sConsulta & "   PREC_ADJ float NOT NULL," & vbCrLf
sConsulta = sConsulta & "   PRES float NULL," & vbCrLf
sConsulta = sConsulta & "   FECINI datetime NOT NULL," & vbCrLf
sConsulta = sConsulta & "   FECFIN datetime NOT NULL," & vbCrLf
sConsulta = sConsulta & "   OBJ float NULL," & vbCrLf
sConsulta = sConsulta & "   FECOBJ datetime NULL," & vbCrLf
sConsulta = sConsulta & "   ESP varchar(800) NULL," & vbCrLf
sConsulta = sConsulta & "   ORIGEN varchar(1) NOT NULL," & vbCrLf
sConsulta = sConsulta & "   USU varchar(" & gLongitudesDeCodigos.giLongCodUSU & ") NOT NULL," & vbCrLf
sConsulta = sConsulta & "   FECACT datetime NULL," & vbCrLf
sConsulta = sConsulta & "   SOLICIT int NULL" & vbCrLf
sConsulta = sConsulta & "   )  ON [PRIMARY]" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET IDENTITY_INSERT dbo.Tmp_LOG_ITEM_ADJ ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF EXISTS(SELECT * FROM dbo.LOG_ITEM_ADJ)" & vbCrLf
sConsulta = sConsulta & "    EXEC('INSERT INTO dbo.Tmp_LOG_ITEM_ADJ (ID, ID_LOG_ADJ, ID_ITEM, ART, DESCR, DEST, UNI, PAG, CANT, CANT_ADJ, PREC_VALIDO_ADJ, PREC_ADJ, PRES, FECINI, FECFIN, OBJ, FECOBJ, ESP, ORIGEN, USU, FECACT, SOLICIT)" & vbCrLf
sConsulta = sConsulta & "       SELECT ID, ID_LOG_ADJ, ID_ITEM, ART, DESCR, DEST, UNI, PAG, CANT, CANT_ADJ, PREC_VALIDO_ADJ, PREC_ADJ, PRES, FECINI, FECFIN, OBJ, FECOBJ, CONVERT(varchar(800), ESP), ORIGEN, USU, FECACT, SOLICIT FROM dbo.LOG_ITEM_ADJ TABLOCKX')" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET IDENTITY_INSERT dbo.Tmp_LOG_ITEM_ADJ OFF" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DROP TABLE dbo.LOG_ITEM_ADJ" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXECUTE sp_rename N'dbo.Tmp_LOG_ITEM_ADJ', N'LOG_ITEM_ADJ', 'OBJECT'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE dbo.LOG_ITEM_ADJ ADD CONSTRAINT" & vbCrLf
sConsulta = sConsulta & "   PK_LOG_ITEM_ADJ PRIMARY KEY CLUSTERED " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   ID" & vbCrLf
sConsulta = sConsulta & "   ) ON [PRIMARY]" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE NONCLUSTERED INDEX IX_LOG_ITEM_ADJ_01 ON dbo.LOG_ITEM_ADJ" & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "   ID_LOG_ADJ" & vbCrLf
sConsulta = sConsulta & "   ) ON [PRIMARY]" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TRIGGER TG_LOG_ITEM_ADJ_INS ON dbo.LOG_ITEM_ADJ" & vbCrLf
sConsulta = sConsulta & "FOR INSERT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "    UPDATE LOG_ITEM_ADJ " & vbCrLf
sConsulta = sConsulta & "        SET FECACT = GETDATE() FROM INSERTED I" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN LOG_ITEM_ADJ L ON L.ID = I.ID" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion2_14_00_12A2_14_00_13() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim bBool As Boolean

On Error GoTo error
    

sConsulta = "BEGIN TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = True
ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

frmProgreso.lblDetalle = "Actualizamos storeds"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_14_Storeds_13


sConsulta = "UPDATE VERSION SET NUM ='2.14.00.13'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "COMMIT TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = False

CodigoDeActualizacion2_14_00_12A2_14_00_13 = True
Exit Function
    
error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_14_00_12A2_14_00_13 = False
End Function


Private Sub V_2_14_Storeds_13()
Dim sConsulta As String
sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_DEVOLVER_ADJUNTOS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_DEVOLVER_ADJUNTOS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE SP_DEVOLVER_ADJUNTOS @ANYO smallint, @GMN1 VARCHAR(50), @PROCE int , @GRUPO varchar(50) = null, @ITEM int = null, @PROVE VARCHAR(50), @OFE INT, @TODOS tinyint = 0 AS " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @TODOS = 1" & vbCrLf
sConsulta = sConsulta & "    IF @GRUPO IS NULL" & vbCrLf
sConsulta = sConsulta & "    SELECT 3 AMBITO, I.GRUPO GRUPO, OIA.ITEM ITEM, OIA.NOM AS NOM, " & vbCrLf
sConsulta = sConsulta & "           OIA.COM AS COM,  " & vbCrLf
sConsulta = sConsulta & "           A.ID AS ID,  " & vbCrLf
sConsulta = sConsulta & "           OIA.DATASIZE ," & vbCrLf
sConsulta = sConsulta & "           OIA.IDPORTAL  " & vbCrLf
sConsulta = sConsulta & "      FROM OFE_ITEM_ADJUN OIA" & vbCrLf
sConsulta = sConsulta & "            INNER JOIN ADJUN A " & vbCrLf
sConsulta = sConsulta & "                    ON OIA.ID = A.ID  " & vbCrLf
sConsulta = sConsulta & "            INNER JOIN ITEM I" & vbCrLf
sConsulta = sConsulta & "                    ON OIA.PROCE = I.PROCE" & vbCrLf
sConsulta = sConsulta & "                   AND OIA.ANYO = I.ANYO" & vbCrLf
sConsulta = sConsulta & "                   AND OIA.GMN1 = I.GMN1" & vbCrLf
sConsulta = sConsulta & "                   AND OIA.ITEM = I.ID " & vbCrLf
sConsulta = sConsulta & "     WHERE OIA.ANYO = @anyo " & vbCrLf
sConsulta = sConsulta & "       AND OIA.GMN1 = @GMN1   " & vbCrLf
sConsulta = sConsulta & "       AND OIA.PROCE = @PROCE  " & vbCrLf
sConsulta = sConsulta & "       AND OIA.PROVE = @PROVE " & vbCrLf
sConsulta = sConsulta & "       AND OIA.OFE = @OFE" & vbCrLf
sConsulta = sConsulta & "    UNION" & vbCrLf
sConsulta = sConsulta & "      SELECT 1 AMBITO, NULL GRUPO, NULL ITEM, POA.NOM AS NOM, " & vbCrLf
sConsulta = sConsulta & "             POA.COM AS COM,  " & vbCrLf
sConsulta = sConsulta & "             A.ID AS ID,  " & vbCrLf
sConsulta = sConsulta & "             POA.DATASIZE ," & vbCrLf
sConsulta = sConsulta & "             POA.IDPORTAL  " & vbCrLf
sConsulta = sConsulta & "        FROM PROCE_OFE_ADJUN POA " & vbCrLf
sConsulta = sConsulta & "              INNER JOIN ADJUN A " & vbCrLf
sConsulta = sConsulta & "                      ON POA.ID = A.ID  " & vbCrLf
sConsulta = sConsulta & "       WHERE POA.ANYO=@anyo " & vbCrLf
sConsulta = sConsulta & "         AND POA.GMN1=@GMN1   " & vbCrLf
sConsulta = sConsulta & "         AND POA.PROCE=@PROCE  " & vbCrLf
sConsulta = sConsulta & "         AND POA.PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "         AND POA.OFE=@OFE" & vbCrLf
sConsulta = sConsulta & "    UNION" & vbCrLf
sConsulta = sConsulta & "      SELECT 2 AMBITO, OGA.GRUPO GRUPO, NULL ITEM, OGA.NOM AS NOM, " & vbCrLf
sConsulta = sConsulta & "             OGA.COM AS COM,  " & vbCrLf
sConsulta = sConsulta & "             A.ID AS ID,  " & vbCrLf
sConsulta = sConsulta & "             OGA.DATASIZE ," & vbCrLf
sConsulta = sConsulta & "             OGA.IDPORTAL  " & vbCrLf
sConsulta = sConsulta & "        FROM OFE_GR_ADJUN OGA" & vbCrLf
sConsulta = sConsulta & "              INNER JOIN ADJUN A " & vbCrLf
sConsulta = sConsulta & "                      ON OGA.ID = A.ID  " & vbCrLf
sConsulta = sConsulta & "       WHERE ANYO = @anyo " & vbCrLf
sConsulta = sConsulta & "         AND GMN1 = @GMN1   " & vbCrLf
sConsulta = sConsulta & "         AND PROCE = @PROCE  " & vbCrLf
sConsulta = sConsulta & "         AND PROVE = @PROVE " & vbCrLf
sConsulta = sConsulta & "         AND OFE = @OFE" & vbCrLf
sConsulta = sConsulta & "    ELSE" & vbCrLf
sConsulta = sConsulta & "    SELECT 3 AMBITO, I.GRUPO GRUPO, OIA.ITEM ITEM, OIA.NOM AS NOM, " & vbCrLf
sConsulta = sConsulta & "           OIA.COM AS COM,  " & vbCrLf
sConsulta = sConsulta & "           A.ID AS ID,  " & vbCrLf
sConsulta = sConsulta & "           OIA.DATASIZE ," & vbCrLf
sConsulta = sConsulta & "           OIA.IDPORTAL  " & vbCrLf
sConsulta = sConsulta & "      FROM OFE_ITEM_ADJUN OIA" & vbCrLf
sConsulta = sConsulta & "            INNER JOIN ADJUN A " & vbCrLf
sConsulta = sConsulta & "                    ON OIA.ID = A.ID  " & vbCrLf
sConsulta = sConsulta & "            INNER JOIN ITEM I" & vbCrLf
sConsulta = sConsulta & "                    ON OIA.PROCE = I.PROCE" & vbCrLf
sConsulta = sConsulta & "                   AND OIA.ANYO = I.ANYO" & vbCrLf
sConsulta = sConsulta & "                   AND OIA.GMN1 = I.GMN1" & vbCrLf
sConsulta = sConsulta & "                   AND OIA.ITEM = I.ID " & vbCrLf
sConsulta = sConsulta & "     WHERE OIA.ANYO = @anyo " & vbCrLf
sConsulta = sConsulta & "       AND OIA.GMN1 = @GMN1   " & vbCrLf
sConsulta = sConsulta & "       AND OIA.PROCE = @PROCE  " & vbCrLf
sConsulta = sConsulta & "       AND OIA.PROVE = @PROVE " & vbCrLf
sConsulta = sConsulta & "       AND OIA.OFE = @OFE" & vbCrLf
sConsulta = sConsulta & "       AND I.GRUPO = @GRUPO" & vbCrLf
sConsulta = sConsulta & "    UNION" & vbCrLf
sConsulta = sConsulta & "      SELECT 2 AMBITO, OGA.GRUPO GRUPO, NULL ITEM, OGA.NOM AS NOM, " & vbCrLf
sConsulta = sConsulta & "             OGA.COM AS COM,  " & vbCrLf
sConsulta = sConsulta & "             A.ID AS ID,  " & vbCrLf
sConsulta = sConsulta & "             OGA.DATASIZE ," & vbCrLf
sConsulta = sConsulta & "             OGA.IDPORTAL  " & vbCrLf
sConsulta = sConsulta & "        FROM OFE_GR_ADJUN OGA" & vbCrLf
sConsulta = sConsulta & "              INNER JOIN ADJUN A " & vbCrLf
sConsulta = sConsulta & "                      ON OGA.ID = A.ID  " & vbCrLf
sConsulta = sConsulta & "       WHERE ANYO = @anyo " & vbCrLf
sConsulta = sConsulta & "         AND GMN1 = @GMN1   " & vbCrLf
sConsulta = sConsulta & "         AND PROCE = @PROCE  " & vbCrLf
sConsulta = sConsulta & "         AND PROVE = @PROVE " & vbCrLf
sConsulta = sConsulta & "         AND OFE = @OFE" & vbCrLf
sConsulta = sConsulta & "         AND OGA.GRUPO = @GRUPO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @ITEM IS NULL" & vbCrLf
sConsulta = sConsulta & "  IF @GRUPO IS NULL" & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "      SELECT POA.NOM AS NOM, " & vbCrLf
sConsulta = sConsulta & "             POA.COM AS COM,  " & vbCrLf
sConsulta = sConsulta & "             A.ID AS ID,  " & vbCrLf
sConsulta = sConsulta & "             POA.DATASIZE ," & vbCrLf
sConsulta = sConsulta & "             POA.IDPORTAL  " & vbCrLf
sConsulta = sConsulta & "        FROM PROCE_OFE_ADJUN POA " & vbCrLf
sConsulta = sConsulta & "              INNER JOIN ADJUN A " & vbCrLf
sConsulta = sConsulta & "                      ON POA.ID = A.ID  " & vbCrLf
sConsulta = sConsulta & "       WHERE POA.ANYO=@anyo " & vbCrLf
sConsulta = sConsulta & "         AND POA.GMN1=@GMN1   " & vbCrLf
sConsulta = sConsulta & "         AND POA.PROCE=@PROCE  " & vbCrLf
sConsulta = sConsulta & "         AND POA.PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "         AND POA.OFE=@OFE" & vbCrLf
sConsulta = sConsulta & "    END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  ELSE" & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "      SELECT OGA.NOM AS NOM, " & vbCrLf
sConsulta = sConsulta & "             OGA.COM AS COM,  " & vbCrLf
sConsulta = sConsulta & "             A.ID AS ID,  " & vbCrLf
sConsulta = sConsulta & "             OGA.DATASIZE ," & vbCrLf
sConsulta = sConsulta & "             OGA.IDPORTAL  " & vbCrLf
sConsulta = sConsulta & "        FROM OFE_GR_ADJUN OGA" & vbCrLf
sConsulta = sConsulta & "              INNER JOIN ADJUN A " & vbCrLf
sConsulta = sConsulta & "                      ON OGA.ID = A.ID  " & vbCrLf
sConsulta = sConsulta & "       WHERE ANYO = @anyo " & vbCrLf
sConsulta = sConsulta & "         AND GMN1 = @GMN1   " & vbCrLf
sConsulta = sConsulta & "         AND PROCE = @PROCE  " & vbCrLf
sConsulta = sConsulta & "         AND GRUPO = @GRUPO" & vbCrLf
sConsulta = sConsulta & "         AND PROVE = @PROVE " & vbCrLf
sConsulta = sConsulta & "         AND OFE = @OFE" & vbCrLf
sConsulta = sConsulta & "    END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    SELECT OIA.NOM AS NOM, " & vbCrLf
sConsulta = sConsulta & "           OIA.COM AS COM,  " & vbCrLf
sConsulta = sConsulta & "           A.ID AS ID,  " & vbCrLf
sConsulta = sConsulta & "           OIA.DATASIZE ," & vbCrLf
sConsulta = sConsulta & "           OIA.IDPORTAL  " & vbCrLf
sConsulta = sConsulta & "      FROM OFE_ITEM_ADJUN OIA" & vbCrLf
sConsulta = sConsulta & "            INNER JOIN ADJUN A " & vbCrLf
sConsulta = sConsulta & "                    ON OIA.ID = A.ID  " & vbCrLf
sConsulta = sConsulta & "     WHERE ANYO = @anyo " & vbCrLf
sConsulta = sConsulta & "       AND GMN1 = @GMN1   " & vbCrLf
sConsulta = sConsulta & "       AND PROCE = @PROCE  " & vbCrLf
sConsulta = sConsulta & "       AND ITEM = @ITEM" & vbCrLf
sConsulta = sConsulta & "       AND PROVE = @PROVE " & vbCrLf
sConsulta = sConsulta & "       AND OFE = @OFE" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_SOLICITUD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_SOLICITUD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSEP_SOLICITUD @ID int OUTPUT, @DEN varchar (50), @DESCR varchar (500), @IMPORTE float, @MON varchar (50), @PROVEEDORES varchar (300)  ," & vbCrLf
sConsulta = sConsulta & "@CARGO varchar (100), @FECNEC datetime, @DEST varchar (50), @PER varchar (50), @NOM varchar (50)  ," & vbCrLf
sConsulta = sConsulta & "@APE varchar (100), @EMAIL varchar (100), @TFNO varchar (20), @FAX varchar (20), @DEP varchar (100)  ," & vbCrLf
sConsulta = sConsulta & "@ESTADO tinyint, @COMPRADOR varchar (50) AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @CAMBIO float" & vbCrLf
sConsulta = sConsulta & "SELECT @CAMBIO = EQUIV " & vbCrLf
sConsulta = sConsulta & "  from MON " & vbCrLf
sConsulta = sConsulta & " WHERE COD = @MON" & vbCrLf
sConsulta = sConsulta & "DECLARE @ESTADOANTERIOR tinyint" & vbCrLf
sConsulta = sConsulta & "IF @ID =0" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    INSERT INTO SOLICIT (DEN, DESCR, IMPORTE, MON, CAMBIO, PROVEEDORES,CARGO, FECNEC, DEST, PER, ESTADO, COMPRADOR,FECALTA) " & vbCrLf
sConsulta = sConsulta & "    VALUES (@DEN, @DESCR, @IMPORTE, @MON, @CAMBIO, @PROVEEDORES, @CARGO, @FECNEC, @DEST, @PER, @ESTADO , @COMPRADOR,GETDATE())" & vbCrLf
sConsulta = sConsulta & "    SET @ID = (SELECT MAX(ID) FROM SOLICIT)" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    SELECT @ESTADOANTERIOR = ESTADO FROM SOLICIT WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "    UPDATE SOLICIT" & vbCrLf
sConsulta = sConsulta & "       SET DEN=@DEN, DESCR= @DESCR, IMPORTE = @IMPORTE, MON=@MON, CAMBIO=@CAMBIO, PROVEEDORES=@PROVEEDORES, CARGO=@CARGO," & vbCrLf
sConsulta = sConsulta & "           FECNEC=@FECNEC, DEST=@DEST, PER=@PER,ESTADO=@ESTADO, COMPRADOR=@COMPRADOR" & vbCrLf
sConsulta = sConsulta & "     WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "    IF @ESTADOANTERIOR!=@ESTADO " & vbCrLf
sConsulta = sConsulta & "      INSERT INTO SOLICIT_EST (SOLICIT, PER, FECHA, EST) VALUES (@ID, @PER, GETDATE(), @ESTADO)" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion2_14_00_13A2_14_00_14() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim bBool As Boolean

On Error GoTo error
    

sConsulta = "BEGIN TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = True
ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

frmProgreso.lblDetalle = "Actualizamos storeds"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_14_Storeds_14


sConsulta = "UPDATE VERSION SET NUM ='2.14.00.14'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "COMMIT TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = False

CodigoDeActualizacion2_14_00_13A2_14_00_14 = True
Exit Function
    
error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_14_00_13A2_14_00_14 = False
End Function


Private Sub V_2_14_Storeds_14()
Dim sConsulta As String
sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ATR_MODIFICAR]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[ATR_MODIFICAR]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE ATR_MODIFICAR  @TABLA SMALLINT,@ID INTEGER,@DEN VARCHAR(100)=NULL,@TIPO SMALLINT=NULL, @PREFBAJO TINYINT , @INTRO TINYINT, @MINNUM FLOAT=NULL,@MINFEC DATETIME=NULL,@MAXNUM FLOAT=NULL,@MAXFEC DATETIME=NULL,@ATRIB INTEGER=NULL,@INTERNO  TINYINT=NULL,@AMBITO TINYINT=NULL,@OBLIG  TINYINT=NULL,@OP_PREC VARCHAR(2)=NULL,@APLIC_PREC VARCHAR(50)=NULL,@DEF_PREC TINYINT=NULL,@ANYO VARCHAR(50)=NULL,@GMN1 VARCHAR(50)=NULL,@PROCE VARCHAR(50)=NULL,@GRUPO VARCHAR(50)=NULL , @FECACT DATETIME OUTPUT,@ORDEN SMALLINT=NULL OUTPUT,@PLANT INTEGER=NULL , @DESCR NVARCHAR(4000)=NULL AS " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @POND AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @AMBITOOLD AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @COUNT AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @APLI_PREC_V AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @ORDEN_V AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @ATRIB_PREC AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @USAR_PREC AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRIMERO AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @TIPOOLD AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @VISTAS AS TINYINT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @TABLA=0  " & vbCrLf
sConsulta = sConsulta & "     BEGIN --@tabla=0" & vbCrLf
sConsulta = sConsulta & "            SELECT   @POND=POND,@TIPOOLD=TIPO  FROM DEF_ATRIB  WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "            IF @POND = 1 AND @TIPO<> 2 and @TIPO<>3  " & vbCrLf
sConsulta = sConsulta & "                         SET @POND = 0" & vbCrLf
sConsulta = sConsulta & "            ELSE " & vbCrLf
sConsulta = sConsulta & "                          begin" & vbCrLf
sConsulta = sConsulta & "                          IF @POND = 2 AND (@TIPO=4 OR @INTRO=0)" & vbCrLf
sConsulta = sConsulta & "                                       SET @POND= 0" & vbCrLf
sConsulta = sConsulta & "                          ELSE" & vbCrLf
sConsulta = sConsulta & "                                        IF  @POND = 3 AND @TIPO<> 2 " & vbCrLf
sConsulta = sConsulta & "                                                   SET @POND= 0" & vbCrLf
sConsulta = sConsulta & "                                        ELSE" & vbCrLf
sConsulta = sConsulta & "                                                    IF @POND =5 AND @TIPO<>4" & vbCrLf
sConsulta = sConsulta & "                                                            begin" & vbCrLf
sConsulta = sConsulta & "                                                            SET @POND= 0" & vbCrLf
sConsulta = sConsulta & "                                                            end" & vbCrLf
sConsulta = sConsulta & "                           end" & vbCrLf
sConsulta = sConsulta & "            IF @TIPOOLD <> @TIPO " & vbCrLf
sConsulta = sConsulta & "                  begin" & vbCrLf
sConsulta = sConsulta & "                  IF @TIPOOLD=2" & vbCrLf
sConsulta = sConsulta & "                        BEGIN" & vbCrLf
sConsulta = sConsulta & "                        UPDATE PROCE_ATRIB SET OP_PREC=NULL, APLIC_PREC=NULL, DEF_PREC=NULL WHERE ATRIB= @ID" & vbCrLf
sConsulta = sConsulta & "                        UPDATE PLANTILLA_ATRIB SET OP_PREC=NULL, APLIC_PREC=NULL, DEF_PREC=NULL WHERE ATRIB= @ID" & vbCrLf
sConsulta = sConsulta & "                        END " & vbCrLf
sConsulta = sConsulta & "                  end" & vbCrLf
sConsulta = sConsulta & "            UPDATE DEF_ATRIB SET DEN=@DEN,TIPO=@TIPO,PREFBAJO=@PREFBAJO,POND=@POND,INTRO=@INTRO,MAXNUM=CAST(@MAXNUM AS VARCHAR(15)) ,MINNUM=CAST(@MINNUM AS VARCHAR(15)),MAXFEC=CONVERT(VARCHAR(10),@MAXFEC,111),MINFEC=CONVERT(VARCHAR(10),@MINFEC,111) , DESCR=@DESCR WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "            SELECT @FECACT=FECACT FROM DEF_ATRIB WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "      END --@tabla=0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @TABLA=1" & vbCrLf
sConsulta = sConsulta & "    BEGIN -- @tabla=1" & vbCrLf
sConsulta = sConsulta & "       IF @APLIC_PREC IS not  NULL  AND @ORDEN is  NULL " & vbCrLf
sConsulta = sConsulta & "                 BEGIN --@orden=null" & vbCrLf
sConsulta = sConsulta & "                 SELECT @ATRIB_PREC =ATRIB  FROM PROCE_ATRIB WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "                 SELECT @ORDEN=ORDEN FROM PROCE_ATRIB WHERE  ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND APLIC_PREC = @APLIC_PREC AND" & vbCrLf
sConsulta = sConsulta & "                    OP_PREC=@OP_PREC AND ATRIB =@ATRIB_PREC" & vbCrLf
sConsulta = sConsulta & "                      " & vbCrLf
sConsulta = sConsulta & "                 SET @APLI_PREC_V=@APLIC_PREC" & vbCrLf
sConsulta = sConsulta & "                 SET @ORDEN_V= 0" & vbCrLf
sConsulta = sConsulta & "                 SET @PRIMERO=0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                 WHILE @ORDEN_V=0 and @APLI_PREC_V<=3" & vbCrLf
sConsulta = sConsulta & "                         BEGIN -- while" & vbCrLf
sConsulta = sConsulta & "                             SELECT  @ORDEN_V = MAX(ORDEN) FROM PROCE_ATRIB WHERE  ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND APLIC_PREC = @APLI_PREC_V" & vbCrLf
sConsulta = sConsulta & "                             IF @ORDEN_V IS NULL" & vbCrLf
sConsulta = sConsulta & "                               begin --@ORDEN_V =null" & vbCrLf
sConsulta = sConsulta & "                                 IF @APLIC_PREC=3" & vbCrLf
sConsulta = sConsulta & "                                     begin" & vbCrLf
sConsulta = sConsulta & "                                       SET @ORDEN_V=1 " & vbCrLf
sConsulta = sConsulta & "                                       SET @PRIMERO=1" & vbCrLf
sConsulta = sConsulta & "                                     end" & vbCrLf
sConsulta = sConsulta & "                                 ELSE                   " & vbCrLf
sConsulta = sConsulta & "                                   begin" & vbCrLf
sConsulta = sConsulta & "                                      SET @ORDEN_V= 0" & vbCrLf
sConsulta = sConsulta & "                                      SET @APLI_PREC_V=@APLI_PREC_V + 1" & vbCrLf
sConsulta = sConsulta & "                                   end" & vbCrLf
sConsulta = sConsulta & "                               end  --@ORDEN_V =null" & vbCrLf
sConsulta = sConsulta & "                         END -- while" & vbCrLf
sConsulta = sConsulta & "                if @ORDEN_V IS NULL  SET @ORDEN_V=0" & vbCrLf
sConsulta = sConsulta & "                      begin" & vbCrLf
sConsulta = sConsulta & "                      if @PRIMERO=1" & vbCrLf
sConsulta = sConsulta & "                              UPDATE PROCE_ATRIB SET ORDEN=1 WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "                     else" & vbCrLf
sConsulta = sConsulta & "                              UPDATE PROCE_ATRIB SET ORDEN=@ORDEN_V+1 WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "                     end" & vbCrLf
sConsulta = sConsulta & "                UPDATE PROCE_ATRIB SET ORDEN = ORDEN +1 WHERE  APLIC_PREC < @APLIC_PREC AND ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ID<>@ID" & vbCrLf
sConsulta = sConsulta & "               SET @ORDEN=@ORDEN_V +1" & vbCrLf
sConsulta = sConsulta & "              " & vbCrLf
sConsulta = sConsulta & "             END --@orden=null" & vbCrLf
sConsulta = sConsulta & "       SELECT @POND=POND,@AMBITOOLD=AMBITO,@USAR_PREC=USAR_PREC FROM PROCE_ATRIB WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "       IF @INTRO=0 AND @POND=2 SET @POND=0" & vbCrLf
sConsulta = sConsulta & "       IF @DEF_PREC=1 SET @USAR_PREC=1" & vbCrLf
sConsulta = sConsulta & "       UPDATE PROCE_ATRIB SET PREFBAJO=@PREFBAJO,INTRO=@INTRO,MAXNUM=@MAXNUM  ," & vbCrLf
sConsulta = sConsulta & "                  MINNUM=@MINNUM,MAXFEC=CONVERT(VARCHAR(10),@MAXFEC,111)," & vbCrLf
sConsulta = sConsulta & "                  MINFEC=CONVERT(VARCHAR(10),@MINFEC,111),INTERNO=@INTERNO,AMBITO=@AMBITO,OBLIG=@OBLIG," & vbCrLf
sConsulta = sConsulta & "                  OP_PREC=@OP_PREC,APLIC_PREC=@APLIC_PREC,USAR_PREC=@USAR_PREC," & vbCrLf
sConsulta = sConsulta & "                  DEF_PREC=@DEF_PREC,POND=@POND WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "       SELECT @FECACT=FECACT FROM PROCE_ATRIB WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "       IF   @ATRIB IS NOT  NULL" & vbCrLf
sConsulta = sConsulta & "             BEGIN --@atrib not null" & vbCrLf
sConsulta = sConsulta & "                    SELECT @POND=POND FROM DEF_ATRIB WHERE ID=@ATRIB" & vbCrLf
sConsulta = sConsulta & "                    IF @INTRO=0 AND @POND=2 SET @POND=0" & vbCrLf
sConsulta = sConsulta & "                       UPDATE DEF_ATRIB SET PREFBAJO=@PREFBAJO,INTRO=@INTRO,MAXNUM=@MAXNUM ," & vbCrLf
sConsulta = sConsulta & "                           MINNUM=@MINNUM,MAXFEC=CONVERT(VARCHAR(10),@MAXFEC,111)," & vbCrLf
sConsulta = sConsulta & "                           MINFEC=CONVERT(VARCHAR(10),@MINFEC,111),POND=@POND WHERE ID=@ATRIB" & vbCrLf
sConsulta = sConsulta & "             END--@atrib not null" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "       IF @AMBITO<>@AMBITOOLD" & vbCrLf
sConsulta = sConsulta & "               BEGIN --@ambito<>ambitoold" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                     IF @AMBITOOLD<3 " & vbCrLf
sConsulta = sConsulta & "                          begin" & vbCrLf
sConsulta = sConsulta & "                             IF (SELECT count(*) FROM CONF_VISTAS_PROCE_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE  AND ATRIB =@ID AND POS IS NOT NULL AND POS<>0)>0" & vbCrLf
sConsulta = sConsulta & "                                    SET @VISTAS=1" & vbCrLf
sConsulta = sConsulta & "                          end" & vbCrLf
sConsulta = sConsulta & "                     ELSE" & vbCrLf
sConsulta = sConsulta & "                          begin" & vbCrLf
sConsulta = sConsulta & "                           IF (SELECT count(*) FROM CONF_VISTAS_GRUPO_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE  AND ATRIB =@ID AND POSICION IS NOT NULL AND POSICION<>0)>0 " & vbCrLf
sConsulta = sConsulta & "                                     SET @VISTAS=1" & vbCrLf
sConsulta = sConsulta & "                           IF (SELECT count(*) FROM CONF_VISTAS_ALL_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE  AND ATRIB =@ID AND POSICION IS NOT NULL AND POSICION<>0)>0 " & vbCrLf
sConsulta = sConsulta & "                                     SET @VISTAS=1" & vbCrLf
sConsulta = sConsulta & "                          end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                       IF (SELECT PLANTILLA_VISTAS FROM PROCE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE)=1 AND @VISTAS=1" & vbCrLf
sConsulta = sConsulta & "                           begin" & vbCrLf
sConsulta = sConsulta & "                           UPDATE PROCE SET PLANTILLA_VISTAS=0 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE  " & vbCrLf
sConsulta = sConsulta & "                           end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                     SELECT @COUNT=COUNT(*) FROM PROCE_OFE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                     IF  @AMBITOOLD>@AMBITO" & vbCrLf
sConsulta = sConsulta & "                              IF @AMBITOOLD=3" & vbCrLf
sConsulta = sConsulta & "                                     begin --old=3" & vbCrLf
sConsulta = sConsulta & "                                           IF @COUNT > 0" & vbCrLf
sConsulta = sConsulta & "                                                begin" & vbCrLf
sConsulta = sConsulta & "                                                DELETE OFE_ITEM_ATRIB WHERE  ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ATRIB_ID=@ID" & vbCrLf
sConsulta = sConsulta & "                                                end" & vbCrLf
sConsulta = sConsulta & "                                           DELETE CONF_VISTAS_GRUPO_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ATRIB=@ID" & vbCrLf
sConsulta = sConsulta & "                                           DELETE CONF_VISTAS_ALL_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE  AND ATRIB=@ID" & vbCrLf
sConsulta = sConsulta & "                                           DELETE CONF_VISTAS_ITEM_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE  AND ATRIB=@ID" & vbCrLf
sConsulta = sConsulta & "                                           INSERT INTO CONF_VISTAS_PROCE_ATRIB (ANYO,GMN1,PROCE,USU,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                                    SELECT @ANYO,@GMN1,@PROCE,USU,VISTA,@ID FROM  CONF_VISTAS_PROCE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "                                           IF @VISTAS=1" & vbCrLf
sConsulta = sConsulta & "                                            begin                                                              " & vbCrLf
sConsulta = sConsulta & "                                                  IF @GRUPO IS NULL" & vbCrLf
sConsulta = sConsulta & "                                                    BEGIN" & vbCrLf
sConsulta = sConsulta & "                                                        UPDATE CONF_VISTAS_GRUPO_ATRIB SET POSICION=NULL,VISIBLE=0,FILA=0 WHERE  ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "                                                        UPDATE CONF_VISTAS_GRUPO SET " & vbCrLf
sConsulta = sConsulta & "                                                              PRECIO_PROV_POS=(case when VI.USU is null then 2 else vi.PRECIO_PROV_POS end), PRECIO_PROV_WIDTH=(case when VI.USU is null then 1600 else vi.PRECIO_PROV_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "                                                              ,PRECIO_PROV_VISIBLE=(case when VI.USU is null then 1 else vi.PRECIO_PROV_VISIBLE end), PRECIO_PROV_LEVEL=(case when VI.USU is null then 0 else vi.PRECIO_PROV_LEVEL end)" & vbCrLf
sConsulta = sConsulta & "                                                              ,ADJ_PROV_POS=(case when VI.USU is null then 9 else vi.ADJ_PROV_POS end), ADJ_PROV_WIDTH=(case when VI.USU is null then 700 else vi.ADJ_PROV_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "                                                              ,ADJ_PROV_VISIBLE=(case when VI.USU is null then 1 else vi.ADJ_PROV_VISIBLE end), ADJ_PROV_LEVEL=(case when VI.USU is null then 1 else vi.ADJ_PROV_LEVEL end)" & vbCrLf
sConsulta = sConsulta & "                                                              ,CANT_PROV_POS=(case when VI.USU is null then 8 else vi.CANT_PROV_POS end), CANT_PROV_WIDTH=(case when VI.USU is null then 900 else vi.CANT_PROV_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "                                                              ,CANT_PROV_VISIBLE=(case when VI.USU is null then 1 else vi.CANT_PROV_VISIBLE end), CANT_PROV_LEVEL=(case when VI.USU is null then 1 else vi.CANT_PROV_LEVEL end)" & vbCrLf
sConsulta = sConsulta & "                                                               ,IMP_ADJ_POS=(case when VI.USU is null then 1 else vi.IMP_ADJ_POS end), IMP_ADJ_WIDTH=(case when VI.USU is null then 500 else vi.IMP_ADJ_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "                                                              ,IMP_ADJ_VISIBLE=(case when VI.USU is null then 0 else vi.IMP_ADJ_VISIBLE end), IMP_ADJ_LEVEL=(case when VI.USU is null then 0 else vi.IMP_ADJ_LEVEL end)" & vbCrLf
sConsulta = sConsulta & "                                                              ,GRUPOS_WIDTH=(case when VI.USU is null then 1600 else vi.GRUPOS_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "                                                               FROM CONF_VISTAS_GRUPO V  LEFT JOIN conf_vista_inicial_grupo VI ON VI.ES_GR=1 AND V.USU=VI.USU  WHERE V.ANYO=@ANYO AND V.GMN1=@GMN1 AND V.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "                                                    END" & vbCrLf
sConsulta = sConsulta & "                                                  ELSE" & vbCrLf
sConsulta = sConsulta & "                                                    BEGIN      " & vbCrLf
sConsulta = sConsulta & "                                                          UPDATE CONF_VISTAS_GRUPO_ATRIB SET POSICION=NULL,VISIBLE=0,FILA=0 WHERE  ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "                                                          UPDATE CONF_VISTAS_GRUPO SET " & vbCrLf
sConsulta = sConsulta & "                                                                PRECIO_PROV_POS=(case when VI.USU is null then 2 else vi.PRECIO_PROV_POS end), PRECIO_PROV_WIDTH=(case when VI.USU is null then 1600 else vi.PRECIO_PROV_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "                                                                 ,PRECIO_PROV_VISIBLE=(case when VI.USU is null then 1 else vi.PRECIO_PROV_VISIBLE end), PRECIO_PROV_LEVEL=(case when VI.USU is null then 0 else vi.PRECIO_PROV_LEVEL end)" & vbCrLf
sConsulta = sConsulta & "                                                                 ,ADJ_PROV_POS=(case when VI.USU is null then 9 else vi.ADJ_PROV_POS end), ADJ_PROV_WIDTH=(case when VI.USU is null then 700 else vi.ADJ_PROV_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "                                                                 ,ADJ_PROV_VISIBLE=(case when VI.USU is null then 1 else vi.ADJ_PROV_VISIBLE end), ADJ_PROV_LEVEL=(case when VI.USU is null then 1 else vi.ADJ_PROV_LEVEL end)" & vbCrLf
sConsulta = sConsulta & "                                                                 ,CANT_PROV_POS=(case when VI.USU is null then 8 else vi.CANT_PROV_POS end), CANT_PROV_WIDTH=(case when VI.USU is null then 900 else vi.CANT_PROV_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "                                                                  ,CANT_PROV_VISIBLE=(case when VI.USU is null then 1 else vi.CANT_PROV_VISIBLE end), CANT_PROV_LEVEL=(case when VI.USU is null then 1 else vi.CANT_PROV_LEVEL end)" & vbCrLf
sConsulta = sConsulta & "                                                                 ,IMP_ADJ_POS=(case when VI.USU is null then 1 else vi.IMP_ADJ_POS end), IMP_ADJ_WIDTH=(case when VI.USU is null then 500 else vi.IMP_ADJ_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "                                                                 ,IMP_ADJ_VISIBLE=(case when VI.USU is null then 0 else vi.IMP_ADJ_VISIBLE end), IMP_ADJ_LEVEL=(case when VI.USU is null then 0 else vi.IMP_ADJ_LEVEL end)" & vbCrLf
sConsulta = sConsulta & "                                                                 ,GRUPOS_WIDTH=(case when VI.USU is null then 1600 else vi.GRUPOS_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "                                                                  FROM CONF_VISTAS_GRUPO V  LEFT JOIN conf_vista_inicial_grupo VI ON VI.ES_GR=1 AND V.USU=VI.USU  WHERE V.ANYO=@ANYO AND V.GMN1=@GMN1 AND V.PROCE=@PROCE AND V.GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "                                                     END" & vbCrLf
sConsulta = sConsulta & "                                                   UPDATE CONF_VISTAS_ALL_ATRIB SET POSICION=NULL,VISIBLE=0,FILA=0 WHERE  ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "                                                   UPDATE CONF_VISTAS_ALL SET " & vbCrLf
sConsulta = sConsulta & "                             PRECIO_PROV_POS=(case when VI.USU is null then 2 else vi.PRECIO_PROV_POS end), PRECIO_PROV_WIDTH=(case when VI.USU is null then 1600 else vi.PRECIO_PROV_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "                        ,PRECIO_PROV_VISIBLE=(case when VI.USU is null then 1 else vi.PRECIO_PROV_VISIBLE end), PRECIO_PROV_LEVEL=(case when VI.USU is null then 0 else vi.PRECIO_PROV_LEVEL end)" & vbCrLf
sConsulta = sConsulta & "                       ,ADJ_PROV_POS=(case when VI.USU is null then 9 else vi.ADJ_PROV_POS end), ADJ_PROV_WIDTH=(case when VI.USU is null then 700 else vi.ADJ_PROV_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "                        ,ADJ_PROV_VISIBLE=(case when VI.USU is null then 1 else vi.ADJ_PROV_VISIBLE end), ADJ_PROV_LEVEL=(case when VI.USU is null then 1 else vi.ADJ_PROV_LEVEL end)" & vbCrLf
sConsulta = sConsulta & "                        ,CANT_PROV_POS=(case when VI.USU is null then 8 else vi.CANT_PROV_POS end), CANT_PROV_WIDTH=(case when VI.USU is null then 900 else vi.CANT_PROV_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "                        ,CANT_PROV_VISIBLE=(case when VI.USU is null then 1 else vi.CANT_PROV_VISIBLE end), CANT_PROV_LEVEL=(case when VI.USU is null then 1 else vi.CANT_PROV_LEVEL end)" & vbCrLf
sConsulta = sConsulta & "                        ,IMP_ADJ_POS=(case when VI.USU is null then 1 else vi.IMP_ADJ_POS end), IMP_ADJ_WIDTH=(case when VI.USU is null then 500 else vi.IMP_ADJ_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "                        ,IMP_ADJ_VISIBLE=(case when VI.USU is null then 0 else vi.IMP_ADJ_VISIBLE end), IMP_ADJ_LEVEL=(case when VI.USU is null then 0 else vi.IMP_ADJ_LEVEL end)" & vbCrLf
sConsulta = sConsulta & "                        ,GRUPOS_WIDTH=(case when VI.USU is null then 1600 else vi.GRUPOS_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "                     FROM CONF_VISTAS_ALL V  LEFT JOIN conf_vista_inicial_grupo VI ON VI.ES_GR=0 AND V.USU=VI.USU  WHERE V.ANYO=@ANYO AND V.GMN1=@GMN1 AND V.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "                                            end -- vista" & vbCrLf
sConsulta = sConsulta & "                                     end  --old=3" & vbCrLf
sConsulta = sConsulta & "                              ELSE --ser�a old=2" & vbCrLf
sConsulta = sConsulta & "                                     begin" & vbCrLf
sConsulta = sConsulta & "                                          IF @COUNT>0 " & vbCrLf
sConsulta = sConsulta & "                                              BEGIN" & vbCrLf
sConsulta = sConsulta & "                                              IF @GRUPO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                                                   DELETE OFE_GR_ATRIB WHERE  ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ATRIB_ID=@ID AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "                                              ELSE" & vbCrLf
sConsulta = sConsulta & "                                                   DELETE OFE_GR_ATRIB WHERE  ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ATRIB_ID=@ID" & vbCrLf
sConsulta = sConsulta & "                                              END " & vbCrLf
sConsulta = sConsulta & "                                          IF @VISTAS=1 " & vbCrLf
sConsulta = sConsulta & "                                              BEGIN                  " & vbCrLf
sConsulta = sConsulta & "                                              UPDATE CONF_VISTAS_PROCE_ATRIB SET POS=NULL,VISIBLE=0 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "                                              UPDATE CONF_VISTAS_PROCE SET " & vbCrLf
sConsulta = sConsulta & "               CONSUMIDO_POS=(case when VI.USU is null then 1 else vi.CONSUMIDO_POS end), CONSUMIDO_WIDTH=(case when VI.USU is null then 850 else vi.CONSUMIDO_WIDTH end),CONSUMIDO_VISIBLE=(case when VI.USU is null then 1 else vi.CONSUMIDO_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "               ,IMP_POS=(case when VI.USU is null then 2 else vi.IMP_POS end), IMP_WIDTH=(case when VI.USU is null then 850 else vi.IMP_WIDTH end),IMP_VISIBLE=(case when VI.USU is null then 0 else vi.IMP_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "               ,ADJ_POS=(case when VI.USU is null then 3 else vi.ADJ_POS end), ADJ_WIDTH=(case when VI.USU is null then 850 else vi.ADJ_WIDTH end),ADJ_VISIBLE=(case when VI.USU is null then 1 else vi.ADJ_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "               ,AHORR_ADJ_POS=(case when VI.USU is null then 5 else vi.AHORR_ADJ_POS end), AHORR_ADJ_WIDTH=(case when VI.USU is null then 850 else vi.AHORR_ADJ_WIDTH end),AHORR_ADJ_VISIBLE=(case when VI.USU is null then 1 else vi.AHORR_ADJ_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "               ,AHORR_ADJ_PORCEN_POS=(case when VI.USU is null then 4 else vi.AHORR_ADJ_PORCEN_POS end), AHORR_ADJ_PORCEN_WIDTH=(case when VI.USU is null then 850 else vi.AHORR_ADJ_PORCEN_WIDTH end),AHORR_ADJ_PORCEN_VISIBLE=(case when VI.USU is null then 0 else vi.AHORR_ADJ_PORCEN_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "               ,AHORR_OFE_POS=(case when VI.USU is null then 7 else vi.AHORR_OFE_POS end), AHORR_OFE_WIDTH=(case when VI.USU is null then 850 else vi.AHORR_OFE_WIDTH end),AHORR_OFE_VISIBLE=(case when VI.USU is null then 1 else vi.AHORR_OFE_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "               ,AHORR_OFE_PORCEN_POS=(case when VI.USU is null then 6 else vi.AHORR_OFE_PORCEN_POS end), AHORR_OFE_PORCEN_WIDTH=(case when VI.USU is null then 850 else vi.AHORR_OFE_PORCEN_WIDTH end),AHORR_OFE_PORCEN_VISIBLE=(case when VI.USU is null then 0 else vi.AHORR_OFE_PORCEN_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "               ,GRUPO_WIDTH=(case when VI.USU is null then 2600 else vi.GRUPO_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "               FROM CONF_VISTAS_PROCE V LEFT JOIN CONF_VISTA_INICIAL_PROCE VI ON V.USU=VI.USU WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "                                              END             " & vbCrLf
sConsulta = sConsulta & "                                     end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                     ELSE  --old < ambito" & vbCrLf
sConsulta = sConsulta & "                                IF @AMBITOOLD=1" & vbCrLf
sConsulta = sConsulta & "                                     BEGIN --old=1" & vbCrLf
sConsulta = sConsulta & "                                           IF @COUNT>0" & vbCrLf
sConsulta = sConsulta & "                                                   begin" & vbCrLf
sConsulta = sConsulta & "                                                   DELETE OFE_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ATRIB_ID=@ID            " & vbCrLf
sConsulta = sConsulta & "                                                   end" & vbCrLf
sConsulta = sConsulta & "                                          IF @AMBITO = 3" & vbCrLf
sConsulta = sConsulta & "                                                   begin --ambito=3" & vbCrLf
sConsulta = sConsulta & "                                                          DELETE CONF_VISTAS_PROCE_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ATRIB=@ID" & vbCrLf
sConsulta = sConsulta & "                                                            --pasa de ambito 1 a ambito 3, el @grupo es siempre null, para todos los grupos" & vbCrLf
sConsulta = sConsulta & "                                                            INSERT INTO CONF_VISTAS_GRUPO_ATRIB (ANYO,GMN1,PROCE,GRUPO,USU,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                                                    SELECT @ANYO,@GMN1,@PROCE,GRUPO,USU,VISTA,@ID FROM  CONF_VISTAS_GRUPO WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE         " & vbCrLf
sConsulta = sConsulta & "                                                            INSERT INTO CONF_VISTAS_ITEM_ATRIB (ANYO,GMN1,PROCE,GRUPO,USU,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                                                   SELECT @ANYO,@GMN1,@PROCE,GRUPO,USU,VISTA,@ID FROM  CONF_VISTAS_ITEM WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "                                                            INSERT INTO CONF_VISTAS_ALL_ATRIB (ANYO,GMN1,PROCE,USU,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                                                   SELECT @ANYO,@GMN1,@PROCE,USU,VISTA,@ID FROM  CONF_VISTAS_ALL WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE              " & vbCrLf
sConsulta = sConsulta & "                                                  end--ambito=3" & vbCrLf
sConsulta = sConsulta & "                                           IF @VISTAS=1" & vbCrLf
sConsulta = sConsulta & "                                               begin                               " & vbCrLf
sConsulta = sConsulta & "                                                   UPDATE CONF_VISTAS_PROCE_ATRIB SET POS=NULL,VISIBLE=0 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "                                                   UPDATE CONF_VISTAS_PROCE SET " & vbCrLf
sConsulta = sConsulta & "               CONSUMIDO_POS=(case when VI.USU is null then 1 else vi.CONSUMIDO_POS end), CONSUMIDO_WIDTH=(case when VI.USU is null then 850 else vi.CONSUMIDO_WIDTH end),CONSUMIDO_VISIBLE=(case when VI.USU is null then 1 else vi.CONSUMIDO_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "               ,IMP_POS=(case when VI.USU is null then 2 else vi.IMP_POS end), IMP_WIDTH=(case when VI.USU is null then 850 else vi.IMP_WIDTH end),IMP_VISIBLE=(case when VI.USU is null then 0 else vi.IMP_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "               ,ADJ_POS=(case when VI.USU is null then 3 else vi.ADJ_POS end), ADJ_WIDTH=(case when VI.USU is null then 850 else vi.ADJ_WIDTH end),ADJ_VISIBLE=(case when VI.USU is null then 1 else vi.ADJ_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "               ,AHORR_ADJ_POS=(case when VI.USU is null then 5 else vi.AHORR_ADJ_POS end), AHORR_ADJ_WIDTH=(case when VI.USU is null then 850 else vi.AHORR_ADJ_WIDTH end),AHORR_ADJ_VISIBLE=(case when VI.USU is null then 1 else vi.AHORR_ADJ_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "               ,AHORR_ADJ_PORCEN_POS=(case when VI.USU is null then 4 else vi.AHORR_ADJ_PORCEN_POS end), AHORR_ADJ_PORCEN_WIDTH=(case when VI.USU is null then 850 else vi.AHORR_ADJ_PORCEN_WIDTH end),AHORR_ADJ_PORCEN_VISIBLE=(case when VI.USU is null then 0 else vi.AHORR_ADJ_PORCEN_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "               ,AHORR_OFE_POS=(case when VI.USU is null then 7 else vi.AHORR_OFE_POS end), AHORR_OFE_WIDTH=(case when VI.USU is null then 850 else vi.AHORR_OFE_WIDTH end),AHORR_OFE_VISIBLE=(case when VI.USU is null then 1 else vi.AHORR_OFE_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "               ,AHORR_OFE_PORCEN_POS=(case when VI.USU is null then 6 else vi.AHORR_OFE_PORCEN_POS end), AHORR_OFE_PORCEN_WIDTH=(case when VI.USU is null then 850 else vi.AHORR_OFE_PORCEN_WIDTH end),AHORR_OFE_PORCEN_VISIBLE=(case when VI.USU is null then 0 else vi.AHORR_OFE_PORCEN_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "               ,GRUPO_WIDTH=(case when VI.USU is null then 2600 else vi.GRUPO_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "               FROM CONF_VISTAS_PROCE V LEFT JOIN CONF_VISTA_INICIAL_PROCE VI ON V.USU=VI.USU WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "                                                end --vistas" & vbCrLf
sConsulta = sConsulta & "                                     END --old=1" & vbCrLf
sConsulta = sConsulta & "                                ELSE " & vbCrLf
sConsulta = sConsulta & "                                     BEGIN  --old es 2, nuevo es 3" & vbCrLf
sConsulta = sConsulta & "                                           IF @COUNT>0" & vbCrLf
sConsulta = sConsulta & "                                               begin" & vbCrLf
sConsulta = sConsulta & "                                               IF @GRUPO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                                                   begin" & vbCrLf
sConsulta = sConsulta & "                                                    DELETE OFE_GR_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ATRIB_ID=@ID AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "                                                    end" & vbCrLf
sConsulta = sConsulta & "                                                ELSE" & vbCrLf
sConsulta = sConsulta & "                                                     begin" & vbCrLf
sConsulta = sConsulta & "                                                     DELETE OFE_GR_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ATRIB_ID=@ID " & vbCrLf
sConsulta = sConsulta & "                                                     end" & vbCrLf
sConsulta = sConsulta & "                                               end" & vbCrLf
sConsulta = sConsulta & "                                           DELETE CONF_VISTAS_PROCE_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ATRIB=@ID" & vbCrLf
sConsulta = sConsulta & "                                           IF @GRUPO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                                                   begin" & vbCrLf
sConsulta = sConsulta & "                                                    INSERT INTO CONF_VISTAS_GRUPO_ATRIB (ANYO,GMN1,PROCE,GRUPO,USU,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                                         SELECT @ANYO,@GMN1,@PROCE,@GRUPO,USU,VISTA,@ID FROM  CONF_VISTAS_GRUPO WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO " & vbCrLf
sConsulta = sConsulta & "                                                    INSERT INTO CONF_VISTAS_ITEM_ATRIB (ANYO,GMN1,PROCE,GRUPO,USU,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                                        SELECT @ANYO,@GMN1,@PROCE,@GRUPO,USU,VISTA,@ID FROM  CONF_VISTAS_ITEM WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO " & vbCrLf
sConsulta = sConsulta & "                                                    end" & vbCrLf
sConsulta = sConsulta & "                                           ELSE" & vbCrLf
sConsulta = sConsulta & "                                                     begin" & vbCrLf
sConsulta = sConsulta & "                                                     INSERT INTO CONF_VISTAS_GRUPO_ATRIB (ANYO,GMN1,PROCE,GRUPO,USU,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                                          SELECT @ANYO,@GMN1,@PROCE,GRUPO,USU,VISTA,@ID FROM  CONF_VISTAS_GRUPO WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "                                                     INSERT INTO CONF_VISTAS_ITEM_ATRIB (ANYO,GMN1,PROCE,GRUPO,USU,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                                          SELECT @ANYO,@GMN1,@PROCE,GRUPO,USU,VISTA,@ID FROM  CONF_VISTAS_ITEM WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "                                                     end" & vbCrLf
sConsulta = sConsulta & "                                           INSERT INTO CONF_VISTAS_ALL_ATRIB (ANYO,GMN1,PROCE,USU,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                                  SELECT @ANYO,@GMN1,@PROCE,USU,VISTA,@ID FROM  CONF_VISTAS_ALL WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "                                           IF @VISTAS=1" & vbCrLf
sConsulta = sConsulta & "                                               begin" & vbCrLf
sConsulta = sConsulta & "                                                   UPDATE CONF_VISTAS_PROCE_ATRIB SET POS=NULL,VISIBLE=0 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "                          UPDATE CONF_VISTAS_PROCE SET " & vbCrLf
sConsulta = sConsulta & "                              CONSUMIDO_POS=(case when VI.USU is null then 1 else vi.CONSUMIDO_POS end), CONSUMIDO_WIDTH=(case when VI.USU is null then 850 else vi.CONSUMIDO_WIDTH end),CONSUMIDO_VISIBLE=(case when VI.USU is null then 1 else vi.CONSUMIDO_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "                                                           ,IMP_POS=(case when VI.USU is null then 2 else vi.IMP_POS end), IMP_WIDTH=(case when VI.USU is null then 850 else vi.IMP_WIDTH end),IMP_VISIBLE=(case when VI.USU is null then 0 else vi.IMP_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "                                                           ,ADJ_POS=(case when VI.USU is null then 3 else vi.ADJ_POS end), ADJ_WIDTH=(case when VI.USU is null then 850 else vi.ADJ_WIDTH end),ADJ_VISIBLE=(case when VI.USU is null then 1 else vi.ADJ_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "                                                           ,AHORR_ADJ_POS=(case when VI.USU is null then 5 else vi.AHORR_ADJ_POS end), AHORR_ADJ_WIDTH=(case when VI.USU is null then 850 else vi.AHORR_ADJ_WIDTH end),AHORR_ADJ_VISIBLE=(case when VI.USU is null then 1 else vi.AHORR_ADJ_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "                                                           ,AHORR_ADJ_PORCEN_POS=(case when VI.USU is null then 4 else vi.AHORR_ADJ_PORCEN_POS end), AHORR_ADJ_PORCEN_WIDTH=(case when VI.USU is null then 850 else vi.AHORR_ADJ_PORCEN_WIDTH end),AHORR_ADJ_PORCEN_VISIBLE=(case when VI.USU is null then 0 else vi.AHORR_ADJ_PORCEN_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "                                                           ,AHORR_OFE_POS=(case when VI.USU is null then 7 else vi.AHORR_OFE_POS end), AHORR_OFE_WIDTH=(case when VI.USU is null then 850 else vi.AHORR_OFE_WIDTH end),AHORR_OFE_VISIBLE=(case when VI.USU is null then 1 else vi.AHORR_OFE_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "                                                          ,AHORR_OFE_PORCEN_POS=(case when VI.USU is null then 6 else vi.AHORR_OFE_PORCEN_POS end), AHORR_OFE_PORCEN_WIDTH=(case when VI.USU is null then 850 else vi.AHORR_OFE_PORCEN_WIDTH end),AHORR_OFE_PORCEN_VISIBLE=(case when VI.USU is null then 0 else vi.AHORR_OFE_PORCEN_VISIBLE end)" & vbCrLf
sConsulta = sConsulta & "                                                           ,GRUPO_WIDTH=(case when VI.USU is null then 2600 else vi.GRUPO_WIDTH end)" & vbCrLf
sConsulta = sConsulta & "                                                           FROM CONF_VISTAS_PROCE V LEFT JOIN CONF_VISTA_INICIAL_PROCE VI ON V.USU=VI.USU WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "                                              end --vistas" & vbCrLf
sConsulta = sConsulta & "                                     END--old es 2" & vbCrLf
sConsulta = sConsulta & "               END --@ambito<>ambitoold" & vbCrLf
sConsulta = sConsulta & "         END -- @tabla=1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @TABLA=2" & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "          IF @APLIC_PREC IS not  NULL  AND @ORDEN is  NULL " & vbCrLf
sConsulta = sConsulta & "                 BEGIN" & vbCrLf
sConsulta = sConsulta & "                     SELECT @ATRIB_PREC =ATRIB  FROM PLANTILLA_ATRIB WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "                     SELECT @ORDEN=ORDEN FROM PLANTILLA_ATRIB WHERE PLANT=@PLANT AND APLIC_PREC = @APLIC_PREC AND OP_PREC=@OP_PREC AND ATRIB =@ATRIB_PREC" & vbCrLf
sConsulta = sConsulta & "                     IF @ORDEN IS NULL" & vbCrLf
sConsulta = sConsulta & "                         BEGIN" & vbCrLf
sConsulta = sConsulta & "                           SET @APLI_PREC_V=@APLIC_PREC" & vbCrLf
sConsulta = sConsulta & "                           SET @ORDEN_V= 0" & vbCrLf
sConsulta = sConsulta & "                           WHILE @APLI_PREC_V < 3  AND @ORDEN_V= 0" & vbCrLf
sConsulta = sConsulta & "                                 BEGIN" & vbCrLf
sConsulta = sConsulta & "                                     SELECT  @ORDEN_V = MAX(ORDEN) FROM PLANTILLA_ATRIB WHERE  PLANT=@PLANT AND APLIC_PREC = @APLI_PREC_V" & vbCrLf
sConsulta = sConsulta & "                                      IF @ORDEN_V IS NULL" & vbCrLf
sConsulta = sConsulta & "                                         begin" & vbCrLf
sConsulta = sConsulta & "                                              SET @ORDEN_V= 0" & vbCrLf
sConsulta = sConsulta & "                                              SET @APLI_PREC_V=@APLI_PREC_V + 1" & vbCrLf
sConsulta = sConsulta & "                                         end" & vbCrLf
sConsulta = sConsulta & "                                END" & vbCrLf
sConsulta = sConsulta & "                          UPDATE PLANTILLA_ATRIB SET ORDEN = ORDEN +1 WHERE  APLIC_PREC < @APLIC_PREC AND PLANT=@PLANT" & vbCrLf
sConsulta = sConsulta & "                          SET @ORDEN=@ORDEN_V +1" & vbCrLf
sConsulta = sConsulta & "                        END" & vbCrLf
sConsulta = sConsulta & "                   ELSE" & vbCrLf
sConsulta = sConsulta & "            begin" & vbCrLf
sConsulta = sConsulta & "                        IF @APLIC_PREC IS not  NULL   " & vbCrLf
sConsulta = sConsulta & "       begin" & vbCrLf
sConsulta = sConsulta & "                            UPDATE PLANTILLA_ATRIB SET ORDEN = ORDEN +1 WHERE  APLIC_PREC < @APLIC_PREC AND PLANT=@PLANT" & vbCrLf
sConsulta = sConsulta & "       end" & vbCrLf
sConsulta = sConsulta & "           end" & vbCrLf
sConsulta = sConsulta & "                 END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "           SELECT @POND=POND, @AMBITOOLD=AMBITO FROM PLANTILLA_ATRIB WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "           IF @INTRO=0 AND @POND=2 SET @POND=0" & vbCrLf
sConsulta = sConsulta & "           IF @DEF_PREC=1 SET @USAR_PREC=1" & vbCrLf
sConsulta = sConsulta & "               UPDATE PLANTILLA_ATRIB SET PREFBAJO=@PREFBAJO,INTRO=@INTRO,MAXNUM=@MAXNUM  ," & vbCrLf
sConsulta = sConsulta & "                    MINNUM=@MINNUM,MAXFEC=CONVERT(VARCHAR(10),@MAXFEC,111)," & vbCrLf
sConsulta = sConsulta & "                    MINFEC=CONVERT(VARCHAR(10),@MINFEC,111),INTERNO=@INTERNO,AMBITO=@AMBITO,OBLIG=@OBLIG," & vbCrLf
sConsulta = sConsulta & "                    OP_PREC=@OP_PREC,APLIC_PREC=@APLIC_PREC,USAR_PREC=@USAR_PREC," & vbCrLf
sConsulta = sConsulta & "                    DEF_PREC=@DEF_PREC,POND=@POND,ORDEN=@ORDEN WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "          SELECT @FECACT=FECACT FROM PLANTILLA_ATRIB WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "          IF   @ATRIB IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                BEGIN" & vbCrLf
sConsulta = sConsulta & "                    SELECT @POND=POND FROM DEF_ATRIB WHERE ID=@ATRIB" & vbCrLf
sConsulta = sConsulta & "                    IF @INTRO=0 AND @POND=2 SET @POND=0" & vbCrLf
sConsulta = sConsulta & "                        UPDATE DEF_ATRIB SET PREFBAJO=@PREFBAJO,INTRO=@INTRO,MAXNUM=CAST(@MAXNUM AS VARCHAR(15)) ," & vbCrLf
sConsulta = sConsulta & "                            MINNUM=CAST(@MINNUM AS VARCHAR(15)),MAXFEC=CONVERT(VARCHAR(10),@MAXFEC,111)," & vbCrLf
sConsulta = sConsulta & "                            MINFEC=CONVERT(VARCHAR(10),@MINFEC,111),POND=@POND, DESCR=@DESCR WHERE ID=@ATRIB" & vbCrLf
sConsulta = sConsulta & "                END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "     IF @AMBITO<>@AMBITOOLD" & vbCrLf
sConsulta = sConsulta & "       BEGIN --@ambito<>ambitoold           " & vbCrLf
sConsulta = sConsulta & "           IF @AMBITOOLD<3 " & vbCrLf
sConsulta = sConsulta & "               begin" & vbCrLf
sConsulta = sConsulta & "                IF (SELECT count(*) FROM PLANTILLA_CONF_VISTAS_PROCE_ATRIB WHERE PLANT=@PLANT AND ATRIB =@ID AND POS IS NOT NULL AND POS<>0)>0 SET @VISTAS=1" & vbCrLf
sConsulta = sConsulta & "              end" & vbCrLf
sConsulta = sConsulta & "           ELSE" & vbCrLf
sConsulta = sConsulta & "              begin" & vbCrLf
sConsulta = sConsulta & "                IF (SELECT count(*) FROM PLANTILLA_CONF_VISTAS_GRUPO_ATRIB WHERE PLANT=@PLANT AND ATRIB =@ID AND POSICION IS NOT NULL AND POSICION<>0)>0 SET @VISTAS=1" & vbCrLf
sConsulta = sConsulta & "                IF (SELECT count(*) FROM PLANTILLA_CONF_VISTAS_ALL_ATRIB WHERE PLANT=@PLANT AND ATRIB =@ID AND POSICION IS NOT NULL AND POSICION<>0)>0 SET @VISTAS=1" & vbCrLf
sConsulta = sConsulta & "              end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "            IF  @AMBITOOLD>@AMBITO" & vbCrLf
sConsulta = sConsulta & "             begin" & vbCrLf
sConsulta = sConsulta & "                IF @AMBITOOLD=3" & vbCrLf
sConsulta = sConsulta & "                   begin --old=3" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                         DELETE PLANTILLA_CONF_VISTAS_GRUPO_ATRIB WHERE PLANT=@PLANT AND ATRIB=@ID" & vbCrLf
sConsulta = sConsulta & "                         DELETE PLANTILLA_CONF_VISTAS_ALL_ATRIB WHERE PLANT=@PLANT AND ATRIB=@ID" & vbCrLf
sConsulta = sConsulta & "                                              " & vbCrLf
sConsulta = sConsulta & "                         INSERT INTO PLANTILLA_CONF_VISTAS_PROCE_ATRIB (PLANT,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                SELECT @PLANT,VISTA,@ID FROM  PLANTILLA_CONF_VISTAS_PROCE WHERE PLANT=@PLANT " & vbCrLf
sConsulta = sConsulta & "                         IF @VISTAS=1" & vbCrLf
sConsulta = sConsulta & "                         begin" & vbCrLf
sConsulta = sConsulta & "               UPDATE PLANTILLA_CONF_VISTAS_ALL_ATRIB SET POSICION=NULL,VISIBLE=0,FILA=0 WHERE PLANT=@PLANT" & vbCrLf
sConsulta = sConsulta & "               UPDATE PLANTILLA_CONF_VISTAS_ALL SET PRECIO_PROV_POS=2 , PRECIO_PROV_WIDTH=1600 ,PRECIO_PROV_VISIBLE=1, PRECIO_PROV_LEVEL=0," & vbCrLf
sConsulta = sConsulta & "                    ADJ_PROV_POS=9, ADJ_PROV_WIDTH=700, ADJ_PROV_VISIBLE=1, ADJ_PROV_LEVEL=1,CANT_PROV_POS=8 , CANT_PROV_WIDTH=900 ,CANT_PROV_VISIBLE=1, CANT_PROV_LEVEL=1 ," & vbCrLf
sConsulta = sConsulta & "                    IMP_ADJ_POS=1, IMP_ADJ_WIDTH=500 ,IMP_ADJ_VISIBLE=0, IMP_ADJ_LEVEL=0 , GRUPOS_WIDTH=1600  WHERE PLANT=@PLANT" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "              IF @GRUPO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "       begin" & vbCrLf
sConsulta = sConsulta & "                  UPDATE PLANTILLA_CONF_VISTAS_GRUPO_ATRIB SET POSICION=NULL,VISIBLE=0,FILA=0 WHERE PLANT=@PLANT AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "                  UPDATE PLANTILLA_CONF_VISTAS_GRUPO SET PRECIO_PROV_POS=2 , PRECIO_PROV_WIDTH=1600 ,PRECIO_PROV_VISIBLE=1, PRECIO_PROV_LEVEL=0," & vbCrLf
sConsulta = sConsulta & "                    ADJ_PROV_POS=9, ADJ_PROV_WIDTH=700, ADJ_PROV_VISIBLE=1, ADJ_PROV_LEVEL=1,  CANT_PROV_POS=8 , CANT_PROV_WIDTH=900 ,CANT_PROV_VISIBLE=1, CANT_PROV_LEVEL=1 ," & vbCrLf
sConsulta = sConsulta & "                    IMP_ADJ_POS=1, IMP_ADJ_WIDTH=500 ,IMP_ADJ_VISIBLE=0, IMP_ADJ_LEVEL=0 , GRUPOS_WIDTH=1600 WHERE PLANT=@PLANT  AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "       end" & vbCrLf
sConsulta = sConsulta & "              ELSE" & vbCrLf
sConsulta = sConsulta & "       begin" & vbCrLf
sConsulta = sConsulta & "                  UPDATE PLANTILLA_CONF_VISTAS_GRUPO_ATRIB SET POSICION=NULL,VISIBLE=0,FILA=0 WHERE PLANT=@PLANT " & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "                  UPDATE PLANTILLA_CONF_VISTAS_GRUPO SET PRECIO_PROV_POS=2 , PRECIO_PROV_WIDTH=1600 ,PRECIO_PROV_VISIBLE=1, PRECIO_PROV_LEVEL=0," & vbCrLf
sConsulta = sConsulta & "                    ADJ_PROV_POS=9, ADJ_PROV_WIDTH=700, ADJ_PROV_VISIBLE=1, ADJ_PROV_LEVEL=1,  CANT_PROV_POS=8 , CANT_PROV_WIDTH=900 ,CANT_PROV_VISIBLE=1, CANT_PROV_LEVEL=1 ," & vbCrLf
sConsulta = sConsulta & "                    IMP_ADJ_POS=1, IMP_ADJ_WIDTH=500 ,IMP_ADJ_VISIBLE=0, IMP_ADJ_LEVEL=0 , GRUPOS_WIDTH=1600 WHERE PLANT=@PLANT" & vbCrLf
sConsulta = sConsulta & "       end" & vbCrLf
sConsulta = sConsulta & "                          end" & vbCrLf
sConsulta = sConsulta & "                   end  --old=3" & vbCrLf
sConsulta = sConsulta & "   ELSE --Ser�a ambitoold=2" & vbCrLf
sConsulta = sConsulta & "        begin" & vbCrLf
sConsulta = sConsulta & "                          if @VISTAS=1 " & vbCrLf
sConsulta = sConsulta & "                          begin" & vbCrLf
sConsulta = sConsulta & "       UPDATE PLANTILLA_CONF_VISTAS_PROCE_ATRIB SET POS=NULL,VISIBLE=0 WHERE PLANT=@PLANT" & vbCrLf
sConsulta = sConsulta & "       UPDATE PLANTILLA_CONF_VISTAS_PROCE SET CONSUMIDO_POS=1, CONSUMIDO_WIDTH=850,CONSUMIDO_VISIBLE=1,IMP_POS=2, IMP_WIDTH=850, IMP_VISIBLE=0, ADJ_POS=3, ADJ_WIDTH=850," & vbCrLf
sConsulta = sConsulta & "               ADJ_VISIBLE=1, AHORR_ADJ_POS=5, AHORR_ADJ_WIDTH=850, AHORR_ADJ_VISIBLE=1, AHORR_ADJ_PORCEN_POS=4, AHORR_ADJ_PORCEN_WIDTH=850 ," & vbCrLf
sConsulta = sConsulta & "               AHORR_ADJ_PORCEN_VISIBLE=0, AHORR_OFE_POS=7, AHORR_OFE_WIDTH=850,AHORR_OFE_VISIBLE=1," & vbCrLf
sConsulta = sConsulta & "               AHORR_OFE_PORCEN_POS= 6, AHORR_OFE_PORCEN_WIDTH=850 ,AHORR_OFE_PORCEN_VISIBLE=0,GRUPO_WIDTH=2600 WHERE PLANT=@PLANT" & vbCrLf
sConsulta = sConsulta & "                         end" & vbCrLf
sConsulta = sConsulta & "        end" & vbCrLf
sConsulta = sConsulta & "            end --@AMBITOOLD>@AMBITO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "           ELSE  --old < ambito" & vbCrLf
sConsulta = sConsulta & "             IF @AMBITOOLD=1" & vbCrLf
sConsulta = sConsulta & "                  begin --old=1" & vbCrLf
sConsulta = sConsulta & "       IF @AMBITO = 3" & vbCrLf
sConsulta = sConsulta & "               begin --ambito=3" & vbCrLf
sConsulta = sConsulta & "                               DELETE PLANTILLA_CONF_VISTAS_PROCE_ATRIB WHERE PLANT=@PLANT AND ATRIB=@ID" & vbCrLf
sConsulta = sConsulta & "                               IF @GRUPO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                                      INSERT INTO PLANTILLA_CONF_VISTAS_GRUPO_ATRIB (PLANT,GRUPO,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                             SELECT @PLANT,@GRUPO,VISTA,@ID FROM  PLANTILLA_CONF_VISTAS_GRUPO WHERE PLANT=@PLANT AND GRUPO=@GRUPO " & vbCrLf
sConsulta = sConsulta & "                               ELSE" & vbCrLf
sConsulta = sConsulta & "                                      INSERT INTO PLANTILLA_CONF_VISTAS_GRUPO_ATRIB (PLANT,GRUPO,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                             SELECT @PLANT,GRUPO,VISTA,@ID FROM  PLANTILLA_CONF_VISTAS_GRUPO WHERE PLANT=@PLANT         " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                               INSERT INTO PLANTILLA_CONF_VISTAS_ALL_ATRIB (PLANT,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                      SELECT @PLANT,VISTA,@ID FROM PLANTILLA_CONF_VISTAS_ALL WHERE PLANT=@PLANT " & vbCrLf
sConsulta = sConsulta & "           end" & vbCrLf
sConsulta = sConsulta & "                            IF @VISTAS=1" & vbCrLf
sConsulta = sConsulta & "                            begin" & vbCrLf
sConsulta = sConsulta & "                UPDATE PLANTILLA_CONF_VISTAS_PROCE_ATRIB SET POS=NULL,VISIBLE=0 WHERE PLANT=@PLANT" & vbCrLf
sConsulta = sConsulta & "            UPDATE PLANTILLA_CONF_VISTAS_PROCE SET CONSUMIDO_POS=1, CONSUMIDO_WIDTH=850,CONSUMIDO_VISIBLE=1,IMP_POS=2, IMP_WIDTH=850, IMP_VISIBLE=0, ADJ_POS=3, ADJ_WIDTH=850," & vbCrLf
sConsulta = sConsulta & "               ADJ_VISIBLE=1, AHORR_ADJ_POS=5, AHORR_ADJ_WIDTH=850, AHORR_ADJ_VISIBLE=1, AHORR_ADJ_PORCEN_POS=4, AHORR_ADJ_PORCEN_WIDTH=850 ," & vbCrLf
sConsulta = sConsulta & "               AHORR_ADJ_PORCEN_VISIBLE=0, AHORR_OFE_POS=7, AHORR_OFE_WIDTH=850,AHORR_OFE_VISIBLE=1," & vbCrLf
sConsulta = sConsulta & "               AHORR_OFE_PORCEN_POS= 6, AHORR_OFE_PORCEN_WIDTH=850 ,AHORR_OFE_PORCEN_VISIBLE=0,GRUPO_WIDTH=2600 WHERE PLANT=@PLANT" & vbCrLf
sConsulta = sConsulta & "                            end" & vbCrLf
sConsulta = sConsulta & "                 end --old=1" & vbCrLf
sConsulta = sConsulta & "             ELSE " & vbCrLf
sConsulta = sConsulta & "                 BEGIN  --old es 2, nuevo es 3" & vbCrLf
sConsulta = sConsulta & "                      DELETE PLANTILLA_CONF_VISTAS_PROCE_ATRIB WHERE PLANT=@PLANT AND ATRIB=@ID" & vbCrLf
sConsulta = sConsulta & "                      IF @GRUPO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "       begin" & vbCrLf
sConsulta = sConsulta & "                             INSERT INTO PLANTILLA_CONF_VISTAS_GRUPO_ATRIB (PLANT,GRUPO,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                    SELECT @PLANT,@GRUPO,VISTA,@ID FROM  PLANTILLA_CONF_VISTAS_GRUPO WHERE PLANT=@PLANT AND GRUPO=@GRUPO " & vbCrLf
sConsulta = sConsulta & "                          end" & vbCrLf
sConsulta = sConsulta & "                      ELSE" & vbCrLf
sConsulta = sConsulta & "                begin" & vbCrLf
sConsulta = sConsulta & "                             INSERT INTO PLANTILLA_CONF_VISTAS_GRUPO_ATRIB (PLANT,GRUPO,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                    SELECT @PLANT,GRUPO,VISTA,@ID FROM  PLANTILLA_CONF_VISTAS_GRUPO WHERE PLANT=@PLANT " & vbCrLf
sConsulta = sConsulta & "                          end" & vbCrLf
sConsulta = sConsulta & "                      INSERT INTO PLANTILLA_CONF_VISTAS_ALL_ATRIB (PLANT,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                             SELECT @PLANT,VISTA,@ID FROM PLANTILLA_CONF_VISTAS_ALL WHERE PLANT=@PLANT " & vbCrLf
sConsulta = sConsulta & "                      IF @VISTAS=1" & vbCrLf
sConsulta = sConsulta & "                       begin                       " & vbCrLf
sConsulta = sConsulta & "                         UPDATE PLANTILLA_CONF_VISTAS_PROCE_ATRIB SET POS=NULL,VISIBLE=0 WHERE PLANT=@PLANT" & vbCrLf
sConsulta = sConsulta & "                 UPDATE PLANTILLA_CONF_VISTAS_PROCE SET CONSUMIDO_POS=1, CONSUMIDO_WIDTH=850,CONSUMIDO_VISIBLE=1,IMP_POS=2, IMP_WIDTH=850, IMP_VISIBLE=0, ADJ_POS=3, ADJ_WIDTH=850," & vbCrLf
sConsulta = sConsulta & "               ADJ_VISIBLE=1, AHORR_ADJ_POS=5, AHORR_ADJ_WIDTH=850, AHORR_ADJ_VISIBLE=1, AHORR_ADJ_PORCEN_POS=4, AHORR_ADJ_PORCEN_WIDTH=850 ," & vbCrLf
sConsulta = sConsulta & "               AHORR_ADJ_PORCEN_VISIBLE=0, AHORR_OFE_POS=7, AHORR_OFE_WIDTH=850,AHORR_OFE_VISIBLE=1," & vbCrLf
sConsulta = sConsulta & "               AHORR_OFE_PORCEN_POS= 6, AHORR_OFE_PORCEN_WIDTH=850 ,AHORR_OFE_PORCEN_VISIBLE=0,GRUPO_WIDTH=2600 WHERE PLANT=@PLANT" & vbCrLf
sConsulta = sConsulta & "                       end" & vbCrLf
sConsulta = sConsulta & "                END--old es 2" & vbCrLf
sConsulta = sConsulta & "       END --@ambito<>ambitoold" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub



'*******************************************************************************
'*** DESDE LA VER. 50 ES LA NUEVA 2.14 DE SEPTIEMBRE DE 2004
'*******************************************************************************
Public Function CodigoDeActualizacion2_14_00_49A2_14_00_50() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim bBool As Boolean

On Error GoTo error
    

sConsulta = "BEGIN TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = True
ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

frmProgreso.lblDetalle = "Modificamos tablas"
frmProgreso.lblDetalle.Refresh
frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_14_Tablas_50

frmProgreso.lblDetalle = "Modificamos triggers"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_14_Triggers_50

frmProgreso.lblDetalle = "Modificamos storeds"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_14_Storeds_50


sConsulta = "UPDATE VERSION SET NUM ='2.14.00.50'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "COMMIT TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = False

CodigoDeActualizacion2_14_00_49A2_14_00_50 = True
Exit Function
    
error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_14_00_49A2_14_00_50 = False

End Function


Private Sub V_2_14_Tablas_50()
Dim sConsulta As String


sConsulta = "ALTER TABLE dbo.PARGEN_INTERNO ADD" & vbCrLf
sConsulta = sConsulta & "   TRASPASO_ADJ_ERP tinyint NOT NULL CONSTRAINT DF_PARGEN_INTERNO_TRASPASO_ADJ_ERP DEFAULT (0)," & vbCrLf
sConsulta = sConsulta & "   ACTIVAR_COD_PROVE_ERP tinyint NOT NULL CONSTRAINT DF_PARGEN_INTERNO_ACTIVAR_COD_PROVE_ERP DEFAULT (0)," & vbCrLf
sConsulta = sConsulta & "   TRASPASO_PED_ERP tinyint NOT NULL CONSTRAINT DF_PARGEN_INTERNO_TRASPASO_PED_ERP DEFAULT (0)," & vbCrLf
sConsulta = sConsulta & "   TRASLADO_APROV_ERP tinyint NOT NULL CONSTRAINT DF_PARGEN_INTERNO_TRASLADO_APROV_ERP DEFAULT (0)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TABLE [dbo].[EMP] (" & vbCrLf
sConsulta = sConsulta & "   [ID] [int] IDENTITY (1, 1) NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [NIF] [varchar] (20) NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [DEN] [varchar] (100)NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [DIR] [varchar] (255) NULL ," & vbCrLf
sConsulta = sConsulta & "   [CP] [varchar] (20) NULL ," & vbCrLf
sConsulta = sConsulta & "   [POB] [varchar] (100) NULL ," & vbCrLf
sConsulta = sConsulta & "   [PAI] [varchar] (" & gLongitudesDeCodigos.giLongCodPAI & ") NULL ," & vbCrLf
sConsulta = sConsulta & "   [PROVI] [varchar] (" & gLongitudesDeCodigos.giLongCodPROVI & ") NULL ," & vbCrLf
sConsulta = sConsulta & "   [ERP] [tinyint] NOT NULL CONSTRAINT [DF_EMP_ERP] DEFAULT (0)," & vbCrLf
sConsulta = sConsulta & "   [FECACT] [datetime] NULL " & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE [dbo].[EMP] WITH NOCHECK ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [PK_EMP] PRIMARY KEY  CLUSTERED " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )  ON [PRIMARY] " & vbCrLf
ExecuteSQL gRDOCon, sConsulta

If frmProgreso.ProgressBar1.Value >= frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

sConsulta = "ALTER TABLE dbo.LOG_ADJ ADD" & vbCrLf
sConsulta = sConsulta & "   EMPRESA int NULL," & vbCrLf
sConsulta = sConsulta & "   PROVE_ERP varchar(100) NULL," & vbCrLf
sConsulta = sConsulta & "   FECCIERRE datetime NULL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE  INDEX [IX_LOG_ADJ] ON [dbo].[LOG_ADJ]([ANYO], [GMN1], [COD], [EMPRESA], [PROVE_ERP]) ON [PRIMARY]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE dbo.PROCE ADD" & vbCrLf
sConsulta = sConsulta & "   ADJ_ERP tinyint NOT NULL CONSTRAINT DF_PROCE_ADJ_ERP DEFAULT (0)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE dbo.LOG_ORDEN_ENTREGA ADD" & vbCrLf
sConsulta = sConsulta & "   EMPRESA int NULL," & vbCrLf
sConsulta = sConsulta & "   COD_PROVE_ERP varchar(100) NULL," & vbCrLf
sConsulta = sConsulta & "   RECEPTOR varchar(" & gLongitudesDeCodigos.giLongCodPER & ") NULL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE dbo.ORDEN_ENTREGA ADD" & vbCrLf
sConsulta = sConsulta & "   PED_ERP tinyint NOT NULL CONSTRAINT DF_ORDEN_ENTREGA_PED_ERP DEFAULT (0)," & vbCrLf
sConsulta = sConsulta & "   EMPRESA int NULL," & vbCrLf
sConsulta = sConsulta & "   COD_PROVE_ERP varchar(100) NULL," & vbCrLf
sConsulta = sConsulta & "   RECEPTOR varchar(" & gLongitudesDeCodigos.giLongCodPER & ") NULL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE [dbo].[ORDEN_ENTREGA] ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [FK_ORDEN_ENTREGA_EMP] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [EMPRESA]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[EMP] (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE [dbo].[ORDEN_ENTREGA] ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [FK_ORDEN_ENTREGA_PER] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [RECEPTOR]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[PER] (" & vbCrLf
sConsulta = sConsulta & "       [COD]" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE dbo.PARGEN_GEST ADD" & vbCrLf
sConsulta = sConsulta & "   ACTCAMPO1 tinyint NOT NULL CONSTRAINT DF_PARGEN_GEST_ACTCAMPO1 DEFAULT (1)," & vbCrLf
sConsulta = sConsulta & "   ACTCAMPO2 tinyint NOT NULL CONSTRAINT DF_PARGEN_GEST_ACTCAMPO2 DEFAULT (1)," & vbCrLf
sConsulta = sConsulta & "   ACTCAMPO3 tinyint NOT NULL CONSTRAINT DF_PARGEN_GEST_ACTCAMPO3 DEFAULT (1)," & vbCrLf
sConsulta = sConsulta & "   ACTCAMPO4 tinyint NOT NULL CONSTRAINT DF_PARGEN_GEST_ACTCAMPO4 DEFAULT (1)," & vbCrLf
sConsulta = sConsulta & "   OBL_DIST_PEDIR tinyint NOT NULL CONSTRAINT DF_PARGEN_GEST_OBLDISTCOMPRAPEDIR DEFAULT (0)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE dbo.PARGEN_DOT ADD" & vbCrLf
sConsulta = sConsulta & "   PEDNOTIFACEP_HTML varchar(255) NULL," & vbCrLf
sConsulta = sConsulta & "   PEDNOTIFACEP_TXT varchar(255) NULL," & vbCrLf
sConsulta = sConsulta & "   PEDNOTIFSUBJECT varchar(255) NULL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TABLE [dbo].[PROVE_EMPRESA_COD_ERP] (" & vbCrLf
sConsulta = sConsulta & "   [PROVE] [varchar] (" & gLongitudesDeCodigos.giLongCodPROVE & ") NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [COD_ERP] [nvarchar] (100) NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [EMPRESA] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [ULTIMO] [tinyint] NOT NULL CONSTRAINT [DF_PROVE_EMPRESA_COD_ERP_ULTIMO] DEFAULT (0)" & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE [dbo].[PROVE_EMPRESA_COD_ERP] WITH NOCHECK ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [PK_PROVE_UON_COD_ERP] PRIMARY KEY  CLUSTERED " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [PROVE]," & vbCrLf
sConsulta = sConsulta & "       [COD_ERP]," & vbCrLf
sConsulta = sConsulta & "       [EMPRESA]" & vbCrLf
sConsulta = sConsulta & "   )  ON [PRIMARY] " & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "ALTER TABLE [dbo].[PROVE_EMPRESA_COD_ERP] ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [FK_PROVE_EMPRESA_COD_ERP_EMP] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [EMPRESA]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[EMP] (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "CREATE TABLE [dbo].[PROVE_ERP] (" & vbCrLf
sConsulta = sConsulta & "   [COD_ERP] [nvarchar] (100) NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [EMPRESA] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [DEN_ERP] [nvarchar] (100) NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [NIF] [nvarchar] (20) NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [CAMPO1] [nvarchar] (100) NULL ," & vbCrLf
sConsulta = sConsulta & "   [CAMPO2] [nvarchar] (100) NULL ," & vbCrLf
sConsulta = sConsulta & "   [CAMPO3] [nvarchar] (100) NULL ," & vbCrLf
sConsulta = sConsulta & "   [CAMPO4] [nvarchar] (100) NULL ," & vbCrLf
sConsulta = sConsulta & "   [BAJALOG] [tinyint] NOT NULL " & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE [dbo].[PROVE_ERP] WITH NOCHECK ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [PK_PROVE_ERP] PRIMARY KEY  CLUSTERED " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [COD_ERP]," & vbCrLf
sConsulta = sConsulta & "       [EMPRESA]" & vbCrLf
sConsulta = sConsulta & "   )  ON [PRIMARY] " & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE [dbo].[PROVE_ERP] ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PROVE_ERP_BAJALOG] DEFAULT (0) FOR [BAJALOG]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE [dbo].[PROVE_ERP] ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [FK_PROVE_ERP_EMP] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [EMPRESA]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[EMP] (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "SET QUOTED_IDENTIFIER ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = "CREATE TRIGGER PROVE_ERP_TG_INS ON dbo.PROVE_ERP " & vbCrLf
sConsulta = sConsulta & "FOR INSERT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO PROVE_EMPRESA_COD_ERP (PROVE, COD_ERP, EMPRESA) SELECT  P.COD,I.COD_ERP, I.EMPRESA FROM INSERTED I INNER JOIN PROVE P ON I.NIF = P.NIF" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE dbo.FAVORITOS_ORDEN_ENTREGA ADD" & vbCrLf
sConsulta = sConsulta & "   EMPRESA int NULL," & vbCrLf
sConsulta = sConsulta & "   COD_PROVE_ERP varchar(100) NULL," & vbCrLf
sConsulta = sConsulta & "   RECEPTOR varchar(" & gLongitudesDeCodigos.giLongCodPER & ") NULL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE [dbo].[FAVORITOS_ORDEN_ENTREGA] ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [FK_FAVORITOS_ORDEN_ENTREGA_EMP] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [EMPRESA]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[EMP] (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE [dbo].[FAVORITOS_ORDEN_ENTREGA] ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [FK_FAVORITOS_ORDEN_ENTREGA_PER] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [RECEPTOR]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[PER] (" & vbCrLf
sConsulta = sConsulta & "       [COD]" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE dbo.UON1 ADD" & vbCrLf
sConsulta = sConsulta & "   EMPRESA int NULL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE [dbo].[UON1] ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [FK_UON1_EMP] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [EMPRESA]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[EMP] (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE dbo.UON2 ADD" & vbCrLf
sConsulta = sConsulta & "   EMPRESA int NULL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE [dbo].[UON2] ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [FK_UON2_EMP] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [EMPRESA]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[EMP] (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE dbo.UON3 ADD" & vbCrLf
sConsulta = sConsulta & "   EMPRESA int NULL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE [dbo].[UON3] ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [FK_UON3_EMP] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [EMPRESA]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[EMP] (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE dbo.USU ADD" & vbCrLf
sConsulta = sConsulta & "   PED_OTRAS_EMP int NULL CONSTRAINT [DF_USU_PED_OTRAS_EMP] DEFAULT (0)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "INSERT INTO PARGEN_LIT (ID,IDI,DEN) VALUES(32,'ENG',NULL)"
ExecuteSQL gRDOCon, sConsulta
sConsulta = "INSERT INTO PARGEN_LIT (ID,IDI,DEN) VALUES(32,'SPA',NULL)"
ExecuteSQL gRDOCon, sConsulta
sConsulta = "INSERT INTO PARGEN_LIT (ID,IDI,DEN) VALUES(33,'ENG',NULL)"
ExecuteSQL gRDOCon, sConsulta
sConsulta = "INSERT INTO PARGEN_LIT (ID,IDI,DEN) VALUES(33,'SPA',NULL)"
ExecuteSQL gRDOCon, sConsulta
sConsulta = "INSERT INTO PARGEN_LIT (ID,IDI,DEN) VALUES(34,'ENG',NULL)"
ExecuteSQL gRDOCon, sConsulta
sConsulta = "INSERT INTO PARGEN_LIT (ID,IDI,DEN) VALUES(34,'SPA',NULL)"
ExecuteSQL gRDOCon, sConsulta
sConsulta = "INSERT INTO PARGEN_LIT (ID,IDI,DEN) VALUES(35,'ENG',NULL)"
ExecuteSQL gRDOCon, sConsulta
sConsulta = "INSERT INTO PARGEN_LIT (ID,IDI,DEN) VALUES(35,'SPA',NULL)"
ExecuteSQL gRDOCon, sConsulta

End Sub

Private Sub V_2_14_Triggers_50()
Dim sConsulta As String

sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ORDEN_EST_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[ORDEN_EST_TG_INS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TRIGGER ORDEN_EST_TG_INS ON dbo.ORDEN_EST " & vbCrLf
sConsulta = sConsulta & "FOR INSERT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_ORDEN_EST_Ins CURSOR FOR SELECT EST,ID,ORDEN,PER,COMENT,EXTERNO FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_ORDEN_EST_Ins" & vbCrLf
sConsulta = sConsulta & "DECLARE @EST AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_ORDEN AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @SENTIDO AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PER AS VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @USU AS VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @EXTERNO AS BIT" & vbCrLf
sConsulta = sConsulta & "DECLARE @TIPO AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ACTIVA AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ACTIVLOG AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ORIGEN AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @COMENT AS VARCHAR(255)" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_LOG_ORDEN_ENTREGA AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVE AS VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PAG AS VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @INTEGRACION AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ACCION AS VARCHAR(2)" & vbCrLf
sConsulta = sConsulta & "DECLARE @TRASPASO_ERP AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ERP  AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @NOGRABAR AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @TRASLADO_APROV_ERP AS TINYINT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_ORDEN_EST_Ins INTO @EST,@ID,@ID_ORDEN,@PER,@COMENT,@EXTERNO" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @EXTERNO IS NULL" & vbCrLf
sConsulta = sConsulta & "  -- si el cambio de estado de la orden est� originada en FSGS o en EP o en PORTAL (EXTERNO = NULL) tenemos que grabar en las tablas de LOG para luego exportar los mvtos" & vbCrLf
sConsulta = sConsulta & " BEGIN" & vbCrLf
sConsulta = sConsulta & "  IF (@EST = 2 OR @EST = 3 OR @EST = 4 OR @EST = 20 OR @EST = 21) " & vbCrLf
sConsulta = sConsulta & "  -- Si es uno de los estados que nos interesan para grabar en LOG_ORDEN_ENTREGA (emitido,aceptado,rechazado,anulado,en camino)" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "-- Recuperamos el tipo de orden de entrega para ver si est� activada su integraci�n" & vbCrLf
sConsulta = sConsulta & "     SELECT  @TIPO=TIPO,@PROVE=PROVE FROM ORDEN_ENTREGA WHERE ORDEN_ENTREGA.ID = @ID_ORDEN" & vbCrLf
sConsulta = sConsulta & "     SELECT  @ACTIVLOG = ACTIVLOG FROM PARGEN_GEST WHERE ID = 1" & vbCrLf
sConsulta = sConsulta & "     SELECT  @INTEGRACION =INTEGRACION, @TRASLADO_APROV_ERP=TRASLADO_APROV_ERP FROM PARGEN_INTERNO WHERE ID = 1" & vbCrLf
sConsulta = sConsulta & "     IF @TIPO = 0 AND @EST = 2  --Emisi�n de pedidos directos" & vbCrLf
sConsulta = sConsulta & "   SELECT  @TRASPASO_ERP= TRASPASO_PED_ERP FROM PARGEN_INTERNO WHERE ID = 1" & vbCrLf
sConsulta = sConsulta & "     ELSE" & vbCrLf
sConsulta = sConsulta & "   SET @TRASPASO_ERP=0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "     IF @INTEGRACION = 0" & vbCrLf
sConsulta = sConsulta & "        SET @ACTIVA = 0" & vbCrLf
sConsulta = sConsulta & "     ELSE " & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "        IF @TIPO = 0 --Pedidos directos" & vbCrLf
sConsulta = sConsulta & "           SELECT @ACTIVA=ACTIVA,@SENTIDO=SENTIDO FROM TABLAS_INTEGRACION WHERE ID = 13" & vbCrLf
sConsulta = sConsulta & "         ELSE" & vbCrLf
sConsulta = sConsulta & "           SELECT @ACTIVA=ACTIVA,@SENTIDO=SENTIDO FROM TABLAS_INTEGRACION WHERE ID = 11" & vbCrLf
sConsulta = sConsulta & "        END" & vbCrLf
sConsulta = sConsulta & "         IF (@ACTIVA = 1 AND (@SENTIDO=3 OR @SENTIDO=1)) OR @ACTIVLOG = 1 OR @TRASPASO_ERP=1 OR @TRASLADO_APROV_ERP = 1" & vbCrLf
sConsulta = sConsulta & "         -- si el sentido es = 2  (solo entrada) o no est� activa la integraci�n  no grabamos en log" & vbCrLf
sConsulta = sConsulta & "         BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @NOGRABAR=0" & vbCrLf
sConsulta = sConsulta & "   IF @TRASPASO_ERP=1  OR @TRASLADO_APROV_ERP = 1 --emisi�n de pedidos directos y de aprovisionamiento" & vbCrLf
sConsulta = sConsulta & "     begin" & vbCrLf
sConsulta = sConsulta & "       SET @ERP=1" & vbCrLf
sConsulta = sConsulta & "       IF @ACTIVLOG = 1 AND @ACTIVA = 1 " & vbCrLf
sConsulta = sConsulta & "          -- Integracion y log de actividad" & vbCrLf
sConsulta = sConsulta & "                    SET @ORIGEN = 1" & vbCrLf
sConsulta = sConsulta & "       ELSE" & vbCrLf
sConsulta = sConsulta & "           IF @ACTIVLOG = 1" & vbCrLf
sConsulta = sConsulta & "               SET @ORIGEN = 1     " & vbCrLf
sConsulta = sConsulta & "           ELSE " & vbCrLf
sConsulta = sConsulta & "               SET @NOGRABAR=1" & vbCrLf
sConsulta = sConsulta & "     end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "     begin" & vbCrLf
sConsulta = sConsulta & "               IF @ACTIVLOG = 1 AND @ACTIVA = 1 " & vbCrLf
sConsulta = sConsulta & "                           -- Integracion y log de actividad" & vbCrLf
sConsulta = sConsulta & "                  SET @ORIGEN = 2" & vbCrLf
sConsulta = sConsulta & "              ELSE" & vbCrLf
sConsulta = sConsulta & "                 IF @ACTIVLOG = 0" & vbCrLf
sConsulta = sConsulta & "                      -- solo log de actividad" & vbCrLf
sConsulta = sConsulta & "                         SET @ORIGEN = 0" & vbCrLf
sConsulta = sConsulta & "                   ELSE" & vbCrLf
sConsulta = sConsulta & "                         -- solo integraci�n" & vbCrLf
sConsulta = sConsulta & "             SET @ORIGEN = 1" & vbCrLf
sConsulta = sConsulta & "      end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "            IF @PER IS NULL" & vbCrLf
sConsulta = sConsulta & "               SET @USU = NULL" & vbCrLf
sConsulta = sConsulta & "            ELSE" & vbCrLf
sConsulta = sConsulta & "               SET @USU = (SELECT COD FROM USU WHERE PER = @PER)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   -- la acci�n ser� E - Cambio de estado o I Creaci�n orden de entrega)" & vbCrLf
sConsulta = sConsulta & "            SET @PAG = (SELECT PAG FROM PROVE WHERE COD = @PROVE)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @EST = 2  " & vbCrLf
sConsulta = sConsulta & "         SET @ACCION='I'" & vbCrLf
sConsulta = sConsulta & "   ELSE " & vbCrLf
sConsulta = sConsulta & "     BEGIN" & vbCrLf
sConsulta = sConsulta & "          IF @EST=20 " & vbCrLf
sConsulta = sConsulta & "       SET @ACCION='D'" & vbCrLf
sConsulta = sConsulta & "                     ELSE" & vbCrLf
sConsulta = sConsulta & "       SET @ACCION='E'" & vbCrLf
sConsulta = sConsulta & "     END" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "            IF @NOGRABAR=0" & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "               INSERT INTO LOG_ORDEN_ENTREGA (ACCION,ID_ORDEN_ENTREGA,ID_ORDEN_EST,ID_PEDIDO,ANYO,PROVE,NUM_PEDIDO,NUM_ORDEN,NUMEXT,TIPO,FECHA,IMPORTE,PAG,EST,INCORRECTA,COMENT,ORIGEN,PER,USU,MON,CAMBIO,EMPRESA,RECEPTOR,COD_PROVE_ERP)" & vbCrLf
sConsulta = sConsulta & "                     SELECT @ACCION,@ID_ORDEN,@ID,PEDIDO,ORDEN_ENTREGA.ANYO,PROVE,PEDIDO.NUM,ORDEN_ENTREGA.NUM,NUMEXT,ORDEN_ENTREGA.TIPO,ORDEN_ENTREGA.FECHA,IMPORTE,@PAG,@EST,INCORRECTA,@COMENT,@ORIGEN,@PER,@USU, MON,CAMBIO,EMPRESA,RECEPTOR,COD_PROVE_ERP FROM ORDEN_ENTREGA INNER JOIN PEDIDO ON PEDIDO.ID = ORDEN_ENTREGA.PEDIDO WHERE ORDEN_ENTREGA.ID = @ID_ORDEN" & vbCrLf
sConsulta = sConsulta & "               SET @ID_LOG_ORDEN_ENTREGA = (SELECT MAX(ID) FROM LOG_ORDEN_ENTREGA)" & vbCrLf
sConsulta = sConsulta & "                   -- En el caso de Emisi�n de Pedido Directo, todav�a no estar�n creadas las lineas de pedido, se hace en el trigger LINEAS_PEDIDO_TG_INS" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "               INSERT INTO LOG_LINEAS_PEDIDO (ACCION,ID_LOG_ORDEN_ENTREGA,ID_LINEAS_PEDIDO,ID_ORDEN_EST,ART_INT,UP,PREC_UP,CANT_PED,DEST,ANYO,GMN1,GMN2,GMN3,GMN4,PROCE,ITEM,ENTREGA_OBL,FECENTREGA,FECENTREGAPROVE,EST,CAT1,CAT2,CAT3,CAT4,CAT5,OBS,FC)" & vbCrLf
sConsulta = sConsulta & "                      SELECT @ACCION,@ID_LOG_ORDEN_ENTREGA,ID,@ID,ART_INT,UP,PREC_UP,CANT_PED,DEST,ANYO,GMN1,GMN2,GMN3,GMN4,PROCE,ITEM,ENTREGA_OBL,FECENTREGA,FECENTREGAPROVE,EST,CAT1,CAT2,CAT3,CAT4,CAT5,OBS,FC FROM LINEAS_PEDIDO WHERE  ORDEN = @ID_ORDEN" & vbCrLf
sConsulta = sConsulta & "     END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @ERP=1 " & vbCrLf
sConsulta = sConsulta & "     BEGIN" & vbCrLf
sConsulta = sConsulta & "            INSERT INTO LOG_ORDEN_ENTREGA (ACCION,ID_ORDEN_ENTREGA,ID_ORDEN_EST,ID_PEDIDO,ANYO,PROVE,NUM_PEDIDO,NUM_ORDEN,NUMEXT,TIPO,FECHA,IMPORTE,PAG,EST,INCORRECTA,COMENT,ORIGEN,PER,USU,MON,CAMBIO,EMPRESA,RECEPTOR,COD_PROVE_ERP)" & vbCrLf
sConsulta = sConsulta & "            SELECT @ACCION,@ID_ORDEN,@ID,PEDIDO,ORDEN_ENTREGA.ANYO,PROVE,PEDIDO.NUM,ORDEN_ENTREGA.NUM,NUMEXT,ORDEN_ENTREGA.TIPO,ORDEN_ENTREGA.FECHA,IMPORTE,@PAG,@EST,INCORRECTA,@COMENT,0,@PER,@USU, MON,CAMBIO,EMPRESA,RECEPTOR,COD_PROVE_ERP FROM ORDEN_ENTREGA INNER JOIN PEDIDO ON PEDIDO.ID = ORDEN_ENTREGA.PEDIDO WHERE ORDEN_ENTREGA.ID = @ID_ORDEN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "            SET @ID_LOG_ORDEN_ENTREGA = (SELECT MAX(ID) FROM LOG_ORDEN_ENTREGA)" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "                       INSERT INTO LOG_LINEAS_PEDIDO (ACCION,ID_LOG_ORDEN_ENTREGA,ID_LINEAS_PEDIDO,ID_ORDEN_EST,ART_INT,UP,PREC_UP,CANT_PED,DEST,ANYO,GMN1,GMN2,GMN3,GMN4,PROCE,ITEM,ENTREGA_OBL,FECENTREGA,FECENTREGAPROVE,EST,CAT1,CAT2,CAT3,CAT4,CAT5,OBS,FC)" & vbCrLf
sConsulta = sConsulta & "                             SELECT @ACCION,@ID_LOG_ORDEN_ENTREGA,ID,@ID,ART_INT,UP,PREC_UP,CANT_PED,DEST,ANYO,GMN1,GMN2,GMN3,GMN4,PROCE,ITEM,ENTREGA_OBL,FECENTREGA,FECENTREGAPROVE,EST,CAT1,CAT2,CAT3,CAT4,CAT5,OBS,FC FROM LINEAS_PEDIDO WHERE  ORDEN = @ID_ORDEN" & vbCrLf
sConsulta = sConsulta & "     END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "         END --IF ACTIVA" & vbCrLf
sConsulta = sConsulta & "     END -- IF EST" & vbCrLf
sConsulta = sConsulta & "  END -- IF EXTERNO" & vbCrLf
sConsulta = sConsulta & "UPDATE ORDEN_ENTREGA SET ORDEN_EST = @ID WHERE ORDEN_ENTREGA.ID = @ID_ORDEN" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_ORDEN_EST_Ins INTO @EST,@ID,@ID_ORDEN,@PER,@COMENT,@EXTERNO" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "Close curTG_ORDEN_EST_Ins" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_ORDEN_EST_Ins" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PROVE_EMPRESA_COD_ERP_INSUPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PROVE_EMPRESA_COD_ERP_INSUPD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "SET QUOTED_IDENTIFIER ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TRIGGER PROVE_EMPRESA_COD_ERP_INSUPD ON [dbo].[PROVE_EMPRESA_COD_ERP] " & vbCrLf
sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVE VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @EMPRESA INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @COD_ERP VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @ULTIMO INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF UPDATE(ULTIMO)" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "     DECLARE curTG_ERP_Ins CURSOR FOR SELECT ULTIMO,EMPRESA,PROVE,COD_ERP FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "     OPEN curTG_ERP_Ins" & vbCrLf
sConsulta = sConsulta & "     FETCH NEXT FROM curTG_ERP_Ins INTO @ULTIMO,@EMPRESA,@PROVE,@COD_ERP" & vbCrLf
sConsulta = sConsulta & "     WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "          BEGIN" & vbCrLf
sConsulta = sConsulta & "        IF @ULTIMO=1   " & vbCrLf
sConsulta = sConsulta & "            UPDATE PROVE_EMPRESA_COD_ERP SET ULTIMO=0 WHERE  COD_ERP<>@COD_ERP  AND EMPRESA=@EMPRESA AND PROVE=@PROVE AND ULTIMO=1" & vbCrLf
sConsulta = sConsulta & "        FETCH NEXT FROM curTG_ERP_Ins INTO @ULTIMO,@EMPRESA,@PROVE,@COD_ERP" & vbCrLf
sConsulta = sConsulta & "          END" & vbCrLf
sConsulta = sConsulta & "     Close curTG_ERP_Ins" & vbCrLf
sConsulta = sConsulta & "     DEALLOCATE curTG_ERP_Ins" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[LINEAS_PEDIDO_TG_UPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[LINEAS_PEDIDO_TG_UPD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TRIGGER LINEAS_PEDIDO_TG_UPD ON dbo.LINEAS_PEDIDO" & vbCrLf
sConsulta = sConsulta & "FOR UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ORDEN AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ORDEN_EST AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @FECENTREGAPROVE AS DATETIME" & vbCrLf
sConsulta = sConsulta & "DECLARE @ESTADO AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @EXTERNO AS BIT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ESTNEW AS SMALLINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ESTOLD AS SMALLINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @APROBADOR AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PEDIDO AS INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_LINEAS_PEDIDO_Upd CURSOR LOCAL FOR SELECT I.ID,I.ORDEN,I.FECENTREGAPROVE,I.EST,I.APROBADOR,I.PEDIDO,D.EST FROM INSERTED I INNER JOIN DELETED D ON I.ID=D.ID" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_LINEAS_PEDIDO_Upd" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_LINEAS_PEDIDO_Upd INTO @ID,@ORDEN,@FECENTREGAPROVE,@ESTNEW,@APROBADOR,@PEDIDO,@ESTOLD" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "  IF UPDATE (FECENTREGAPROVE)" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "     SELECT @ORDEN_EST=ORDEN_EST FROM ORDEN_ENTREGA WHERE ID = @ORDEN" & vbCrLf
sConsulta = sConsulta & "     SET @ESTADO = (SELECT TOP 1 ESTADO FROM LOG_GRAL INNER JOIN LOG_ORDEN_ENTREGA ON LOG_GRAL.ID_TABLA = LOG_ORDEN_ENTREGA.ID WHERE LOG_ORDEN_ENTREGA.ID_ORDEN_EST = @ORDEN_EST AND (TABLA =11 OR TABLA=13) AND LOG_ORDEN_ENTREGA.ORIGEN<>3)" & vbCrLf
sConsulta = sConsulta & "      IF @ESTADO = 0" & vbCrLf
sConsulta = sConsulta & "      BEGIN" & vbCrLf
sConsulta = sConsulta & "        UPDATE LOG_LINEAS_PEDIDO SET FECENTREGAPROVE = @FECENTREGAPROVE WHERE ID_ORDEN_EST = @ORDEN_EST AND ID_LINEAS_PEDIDO = @ID" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "-- FIN INTEGRACION" & vbCrLf
sConsulta = sConsulta & "  IF UPDATE(EST)" & vbCrLf
sConsulta = sConsulta & "     IF @ESTNEW <> @ESTOLD" & vbCrLf
sConsulta = sConsulta & "      IF  (@ESTNEW=1 AND @ESTOLD<>21) OR @ESTNEW=200" & vbCrLf
sConsulta = sConsulta & "                 INSERT INTO LINEAS_EST (PEDIDO,ORDEN,LINEA,EST,FECHA,PER) VALUES (@PEDIDO,@ORDEN,@ID,@ESTNEW,GETDATE(),@APROBADOR)" & vbCrLf
sConsulta = sConsulta & "UPDATE LINEAS_PEDIDO SET FECACT=GETDATE() WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_LINEAS_PEDIDO_Upd INTO @ID,@ORDEN,@FECENTREGAPROVE,@ESTNEW,@APROBADOR,@PEDIDO,@ESTOLD" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_LINEAS_PEDIDO_Upd" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_LINEAS_PEDIDO_Upd" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
End Sub


Private Sub V_2_14_Storeds_50()
Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_DEVOLVER_EMPRESAS_PERSONA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_DEVOLVER_EMPRESAS_PERSONA]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSEP_DEVOLVER_EMPRESAS_PERSONA @PER VARCHAR(50), @ID INT OUTPUT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @UON1 VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @UON2 VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @UON3 VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "select @UON1 = uon1, @UON2 = uon2, @UON3 = uon3 from per where cod = @PER" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF (SELECT PED_OTRAS_EMP FROM USU WHERE PER = @PER) = 1" & vbCrLf
sConsulta = sConsulta & "  IF @UON3 IS NULL" & vbCrLf
sConsulta = sConsulta & "    IF @UON2 IS NULL" & vbCrLf
sConsulta = sConsulta & "      BEGIN" & vbCrLf
sConsulta = sConsulta & "      SELECT @ID = EMPRESA FROM UON1 WHERE COD = @UON1" & vbCrLf
sConsulta = sConsulta & "      SELECT ID, NIF, DEN, DIR, CP, POB, PAI, PROVI, ERP FROM EMP" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "    ELSE" & vbCrLf
sConsulta = sConsulta & "      BEGIN" & vbCrLf
sConsulta = sConsulta & "      SELECT @ID = EMPRESA FROM UON2 WHERE UON1 = @UON1 AND COD = @UON2" & vbCrLf
sConsulta = sConsulta & "      SELECT ID, NIF, DEN, DIR, CP, POB, PAI, PROVI, ERP FROM EMP" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "  ELSE" & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "    SELECT @ID = EMPRESA FROM UON3 WHERE UON1 = @UON1 AND UON2 = @UON2 AND COD = @UON3" & vbCrLf
sConsulta = sConsulta & "    SELECT ID, NIF, DEN, DIR, CP, POB, PAI, PROVI, ERP FROM EMP" & vbCrLf
sConsulta = sConsulta & "    END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  IF @UON3 IS NULL" & vbCrLf
sConsulta = sConsulta & "    IF @UON2 IS NULL" & vbCrLf
sConsulta = sConsulta & "      BEGIN" & vbCrLf
sConsulta = sConsulta & "      SELECT @ID = EMPRESA FROM UON1 WHERE COD = @UON1" & vbCrLf
sConsulta = sConsulta & "      SELECT ID, NIF, DEN, DIR, CP, POB, PAI, PROVI, ERP FROM (" & vbCrLf
sConsulta = sConsulta & "      SELECT DISTINCT EMPRESA FROM (" & vbCrLf
sConsulta = sConsulta & "      SELECT EMPRESA FROM UON1 WHERE COD = @UON1" & vbCrLf
sConsulta = sConsulta & "      UNION" & vbCrLf
sConsulta = sConsulta & "      SELECT EMPRESA FROM UON2 WHERE UON1 = @UON1" & vbCrLf
sConsulta = sConsulta & "      UNION" & vbCrLf
sConsulta = sConsulta & "      SELECT EMPRESA FROM UON3 WHERE UON1 = @UON1) U)DU " & vbCrLf
sConsulta = sConsulta & "        INNER JOIN EMP E" & vbCrLf
sConsulta = sConsulta & "                ON DU.EMPRESA = E.ID" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "    ELSE" & vbCrLf
sConsulta = sConsulta & "      BEGIN" & vbCrLf
sConsulta = sConsulta & "      SELECT @ID = EMPRESA FROM UON2 WHERE UON1 = @UON1 AND COD = @UON2" & vbCrLf
sConsulta = sConsulta & "      SELECT ID, NIF, DEN, DIR, CP, POB, PAI, PROVI, ERP FROM (" & vbCrLf
sConsulta = sConsulta & "      SELECT DISTINCT EMPRESA FROM (" & vbCrLf
sConsulta = sConsulta & "      SELECT EMPRESA FROM UON2 WHERE UON1 = @UON1 AND COD = @UON2" & vbCrLf
sConsulta = sConsulta & "      UNION" & vbCrLf
sConsulta = sConsulta & "      SELECT EMPRESA FROM UON3 WHERE UON1 = @UON1 AND UON2 = @UON2) U)DU " & vbCrLf
sConsulta = sConsulta & "        INNER JOIN EMP E" & vbCrLf
sConsulta = sConsulta & "                ON DU.EMPRESA = E.ID" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "  ELSE" & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "    SELECT @ID = EMPRESA FROM UON3 WHERE UON1 = @UON1 AND UON2 = @UON2 AND COD = @UON3" & vbCrLf
sConsulta = sConsulta & "    SELECT E.ID, E.NIF, E.DEN, E.DIR, E.CP, E.POB, E.PAI, E.PROVI, E.ERP " & vbCrLf
sConsulta = sConsulta & "      FROM EMP E" & vbCrLf
sConsulta = sConsulta & "         INNER JOIN UON3 U3" & vbCrLf
sConsulta = sConsulta & "                 ON E.ID = U3.EMPRESA" & vbCrLf
sConsulta = sConsulta & "     WHERE UON1 = @UON1 AND UON2 = @UON2 AND COD = @UON3" & vbCrLf
sConsulta = sConsulta & "    END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_DEVOLVER_RECEPTORES]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_DEVOLVER_RECEPTORES]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSEP_DEVOLVER_RECEPTORES @PER VARCHAR(50) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "declare @UON1 VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "declare @UON2 VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "declare @UON3 VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF (SELECT DEST_OTRAS_UO FROM USU WHERE PER = @PER)=1" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    select @UON1 = uon1, @UON2 = uon2, @UON3 = uon3 from per where cod = @PER" & vbCrLf
sConsulta = sConsulta & "    IF @UON3 IS NULL" & vbCrLf
sConsulta = sConsulta & "      IF @UON2 IS NULL" & vbCrLf
sConsulta = sConsulta & "        SELECT P.* FROM PER P" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN USU U" & vbCrLf
sConsulta = sConsulta & "                   ON P.COD = U.PER" & vbCrLf
sConsulta = sConsulta & "         WHERE U.FSEP = 1" & vbCrLf
sConsulta = sConsulta & "           AND U.FSEPTIPO IN (1,3)" & vbCrLf
sConsulta = sConsulta & "           AND P.UON1 = @UON1" & vbCrLf
sConsulta = sConsulta & "      ELSE" & vbCrLf
sConsulta = sConsulta & "        SELECT P.* FROM PER P" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN USU U" & vbCrLf
sConsulta = sConsulta & "                   ON P.COD = U.PER" & vbCrLf
sConsulta = sConsulta & "         WHERE U.FSEP = 1" & vbCrLf
sConsulta = sConsulta & "           AND U.FSEPTIPO IN (1,3)" & vbCrLf
sConsulta = sConsulta & "           AND P.UON1 = @UON1" & vbCrLf
sConsulta = sConsulta & "           AND P.UON2 = @UON2" & vbCrLf
sConsulta = sConsulta & "    ELSE" & vbCrLf
sConsulta = sConsulta & "      SELECT P.* FROM PER P" & vbCrLf
sConsulta = sConsulta & "         INNER JOIN USU U" & vbCrLf
sConsulta = sConsulta & "                 ON P.COD = U.PER" & vbCrLf
sConsulta = sConsulta & "       WHERE U.FSEP = 1" & vbCrLf
sConsulta = sConsulta & "         AND U.FSEPTIPO IN (1,3)" & vbCrLf
sConsulta = sConsulta & "         AND P.UON1 = @UON1" & vbCrLf
sConsulta = sConsulta & "         AND P.UON2 = @UON2" & vbCrLf
sConsulta = sConsulta & "         AND P.UON3 = @UON3" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  SELECT P.* FROM PER P" & vbCrLf
sConsulta = sConsulta & "     INNER JOIN USU U" & vbCrLf
sConsulta = sConsulta & "             ON P.COD = U.PER" & vbCrLf
sConsulta = sConsulta & "   WHERE U.FSEP = 1" & vbCrLf
sConsulta = sConsulta & "     AND U.FSEPTIPO IN (1,3)" & vbCrLf
sConsulta = sConsulta & "  " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_VALIDAR_USUARIO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_VALIDAR_USUARIO]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE  FSEP_VALIDAR_USUARIO (@USU VARCHAR(80),@PWD VARCHAR(255),@BLOQUEO INTEGER)  AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE @NUM_INTENTOS AS INTEGER" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @NUMARTICULOS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @IMPORTETOTAL FLOAT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @NUMARTICULOS = COUNT(*) , @IMPORTETOTAL = ISNULL(SUM(isnull(CANT,0)*isnull(PREC,0)*isnull(FC,1)),0)" & vbCrLf
sConsulta = sConsulta & "  FROM CESTA " & vbCrLf
sConsulta = sConsulta & "    INNER JOIN USU " & vbCrLf
sConsulta = sConsulta & "            ON USU.PER = CESTA.PER" & vbCrLf
sConsulta = sConsulta & " WHERE USU.COD = @USU" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "If @BLOQUEO > 0 " & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    SET @NUM_INTENTOS = (SELECT USU.BLOQ FROM USU WHERE USU.COD=@USU AND USU.PWD=@PWD AND USU.BLOQ<@BLOQUEO)" & vbCrLf
sConsulta = sConsulta & "    IF (@NUM_INTENTOS IS NULL)" & vbCrLf
sConsulta = sConsulta & "       UPDATE USU SET BLOQ=BLOQ+1 WHERE USU.COD=@USU" & vbCrLf
sConsulta = sConsulta & "    ELSE" & vbCrLf
sConsulta = sConsulta & "      BEGIN" & vbCrLf
sConsulta = sConsulta & "        IF @NUM_INTENTOS < @BLOQUEO" & vbCrLf
sConsulta = sConsulta & "          BEGIN" & vbCrLf
sConsulta = sConsulta & "            SELECT USU.COD, PER.COD AS PER, PER.NOM, USU.THOUSANFMT, USU.DECIMALFMT, USU.PRECISIONFMT, " & vbCrLf
sConsulta = sConsulta & "                   USU.MOSTRARFMT,USU.DATEFMT,FSEP,USU.MON,USU.FSEPTIPO,MON.EQUIV EQUIV,USU.TIPOEMAIL,USU.IDIOMA," & vbCrLf
sConsulta = sConsulta & "                   USU.DEST,USU.DEST_OTRAS_UO,USU.SOL_COMPRA, MOSTIMGART, MOSTATRIB, MOSTCANTMIN, MOSTCODPROVE, MOSTCODART ," & vbCrLf
sConsulta = sConsulta & "                   @NUMARTICULOS NUMARTICULOS, @IMPORTETOTAL IMPORTETOTAL, ORDEN, DIREC, PEDLIBRE,PED_OTRAS_EMP" & vbCrLf
sConsulta = sConsulta & "              FROM USU INNER JOIN PER ON USU.PER=PER.COD" & vbCrLf
sConsulta = sConsulta & "                       INNER JOIN MON ON USU.MON =MON.COD" & vbCrLf
sConsulta = sConsulta & "             WHERE USU.COD =@USU AND USU.PWD=@PWD AND FSEP>0" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "            UPDATE USU SET BLOQ=0 WHERE USU.COD=@USU" & vbCrLf
sConsulta = sConsulta & "          END" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "  END " & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "    SELECT USU.COD, PER.COD AS PER, PER.NOM, USU.THOUSANFMT, USU.DECIMALFMT, USU.PRECISIONFMT, " & vbCrLf
sConsulta = sConsulta & "    USU.MOSTRARFMT,USU.DATEFMT,FSEP,USU.MON,USU.FSEPTIPO,MON.EQUIV EQUIV, USU.TIPOEMAIL, USU.IDIOMA," & vbCrLf
sConsulta = sConsulta & "    USU.DEST,USU.DEST_OTRAS_UO,USU.SOL_COMPRA, MOSTIMGART, MOSTATRIB, MOSTCANTMIN, MOSTCODPROVE, MOSTCODART ," & vbCrLf
sConsulta = sConsulta & "    @NUMARTICULOS NUMARTICULOS, @IMPORTETOTAL IMPORTETOTAL, ORDEN, DIREC, PEDLIBRE,PED_OTRAS_EMP" & vbCrLf
sConsulta = sConsulta & "     From USU INNER JOIN PER ON USU.PER=PER.COD" & vbCrLf
sConsulta = sConsulta & "     inner join MON ON USU.MON =MON.COD" & vbCrLf
sConsulta = sConsulta & "    WHERE USU.COD =@USU AND USU.PWD=@PWD AND FSEP>0" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ACTUALIZAR_PROV_GS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_ACTUALIZAR_PROV_GS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_ACTUALIZAR_PROV_GS (@COD_PROVE_CIA VARCHAR(40),@CIAS_DEN VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                                @CIAS_NIF VARCHAR(20),@CIAS_PAI VARCHAR(20),@CIAS_PROVI VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                                @CIAS_CP VARCHAR(20),@CIAS_POB VARCHAR(100),@CIAS_DIR VARCHAR(255)," & vbCrLf
sConsulta = sConsulta & "                                @CIAS_MON VARCHAR(20),@CIAS_URLCIA VARCHAR(255)) AS" & vbCrLf
sConsulta = sConsulta & "/*******************************************************************************************************************************/" & vbCrLf
sConsulta = sConsulta & "/******       Descripci�n :  Actualiza los datos del proveedor de GS que se han modificado desde el PS    ******/" & vbCrLf
sConsulta = sConsulta & "/******                                                                                                  ******/" & vbCrLf
sConsulta = sConsulta & "/******       Par�metros :     @COD_PROVE_CIA >> Codigo del proveedor en el GS                                    ******/" & vbCrLf
sConsulta = sConsulta & "/******            @CIAS_DEN >> Denominacion del proveedor                          ******/" & vbCrLf
sConsulta = sConsulta & "/******            @CIAS_NIF >> NIF del proveedor                          ******/" & vbCrLf
sConsulta = sConsulta & "/******            @CIAS_PAI >> Pais del proveedor                                                                     ******/" & vbCrLf
sConsulta = sConsulta & "/******            @CIAS_PROVI >> Provincia del proveedor                                                        ******/" & vbCrLf
sConsulta = sConsulta & "/******            @CIAS_CP >> Codigo Postal de la poblacion del proveedor                              ******/" & vbCrLf
sConsulta = sConsulta & "/******            @CIAS_POB >> Poblacion del proveedor                                                          ******/" & vbCrLf
sConsulta = sConsulta & "/******            @CIAS_DIR >> Direccion del proveedor                                                            ******/" & vbCrLf
sConsulta = sConsulta & "/******            @CIAS_MON >> Moneda con la trabaja el proveedor                                        ******/" & vbCrLf
sConsulta = sConsulta & "/******            @CIAS_URLCIA >> Direccion web del proveedor                                              ******/" & vbCrLf
sConsulta = sConsulta & "/******                                                                        ******/" & vbCrLf
sConsulta = sConsulta & "/********************************************************************************************************************************/" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE @PAIS VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @MON VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROV VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @INT TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @SENTIDO TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ACTIVAR_COD_PROVE_ERP TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @OLDNIF VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE  @ACTIVA BIT" & vbCrLf
sConsulta = sConsulta & "SET @PAIS = (SELECT COD FROM PAI WHERE FSP_COD=@CIAS_PAI)" & vbCrLf
sConsulta = sConsulta & "SET @PROV = (SELECT COD FROM PROVI WHERE  FSP_PAI=@CIAS_PAI and  FSP_COD=@CIAS_PROVI)" & vbCrLf
sConsulta = sConsulta & "SET @MON = (SELECT COD FROM MON WHERE FSP_COD=@CIAS_MON)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @INT =INTEGRACION, @ACTIVAR_COD_PROVE_ERP=ACTIVAR_COD_PROVE_ERP FROM PARGEN_INTERNO WHERE ID=1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ACTIVAR_COD_PROVE_ERP = 1" & vbCrLf
sConsulta = sConsulta & "  SELECT @OLDNIF = NIF FROM PROVE WHERE COD = @COD_PROVE_CIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- Actualizamos los datos del proveedor de GS" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE SET DEN=@CIAS_DEN ,DIR=@CIAS_DIR ,CP=@CIAS_CP ,POB= @CIAS_POB ,PAI= @PAIS," & vbCrLf
sConsulta = sConsulta & "                     PROVI=@PROV, MON=@MON,NIF=@CIAS_NIF,URLPROVE=@CIAS_URLCIA" & vbCrLf
sConsulta = sConsulta & "                 WHERE COD=@COD_PROVE_CIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ACTIVAR_COD_PROVE_ERP = 1" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    IF @OLDNIF <> @CIAS_NIF " & vbCrLf
sConsulta = sConsulta & "      DELETE FROM PROVE_EMPRESA_COD_ERP FROM PROVE_EMPRESA_COD_ERP PECE INNER JOIN PROVE_ERP PE ON PECE.COD_ERP = PE.COD_ERP AND PECE.EMPRESA = PE.EMPRESA  WHERE PE.NIF = @OLDNIF AND PECE.PROVE = @COD_PROVE_CIA" & vbCrLf
sConsulta = sConsulta & "    IF @CIAS_NIF IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "      INSERT INTO PROVE_EMPRESA_COD_ERP (PROVE, COD_ERP, EMPRESA) SELECT  P.COD,I.COD_ERP, I.EMPRESA FROM PROVE_ERP I INNER JOIN PROVE P ON I.NIF = P.NIF WHERE P.COD=@COD_PROVE_CIA AND I.NIF =@CIAS_NIF AND NOT EXISTS (SELECT * FROM PROVE_EMPRESA_COD_ERP PECE WHERE P.COD=PECE.PROVE AND I.COD_ERP = PECE.COD_ERP AND I.EMPRESA = PECE.EMPRESA)" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "/*************************************************************************************************************************************************************/" & vbCrLf
sConsulta = sConsulta & "/******       Integraci�n :  Si est� activada la integraci�n y en concreto la entidad PROVE en sentido salida o entrada/salida          ******/" & vbCrLf
sConsulta = sConsulta & "/******            grabamos una fila en la tabla LOG_PROVE con acci�n = U, origen = Integraci�n y el usuario a blancos ******/" & vbCrLf
sConsulta = sConsulta & "/*************************************************************************************************************************************************************/" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @INT = 1 " & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SELECT @ACTIVA=ACTIVA,@SENTIDO=SENTIDO FROM TABLAS_INTEGRACION WHERE ID=8" & vbCrLf
sConsulta = sConsulta & "IF @ACTIVA = 1 AND (@SENTIDO = 1 OR @SENTIDO=3)" & vbCrLf
sConsulta = sConsulta & "INSERT INTO LOG_PROVE (ACCION,COD,DEN,DIR,CP,POB,PAI,PROVI,MON,NIF,URLPROVE,ORIGEN,USU) VALUES ('U',@COD_PROVE_CIA,@CIAS_DEN,@CIAS_DIR,@CIAS_CP,@CIAS_POB,@PAIS,@PROV,@MON,@CIAS_NIF,@CIAS_URLCIA,0,'')" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PER_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[PER_COD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE PER_COD (@OLD VARCHAR(50),@NEW VARCHAR(50))  AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @EQP VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT ON" & vbCrLf
sConsulta = sConsulta & "BEGIN TRANSACTION" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE D CURSOR FOR SELECT COD,EQP FROM PER WHERE COD=@OLD" & vbCrLf
sConsulta = sConsulta & "OPEN D" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM D INTO @VAR_COD,@EQP" & vbCrLf
sConsulta = sConsulta & "IF @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "/*Ejecuta el store procedure COM_COD*/" & vbCrLf
sConsulta = sConsulta & "IF NOT (@EQP) IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "EXECUTE COM_COD @OLD,@NEW,@EQP" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PER NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FIJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PER NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE APROB_LIM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CAT_PER NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_EST NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE NOTIF_TIPO1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE NOTIF_TIPO2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ORDEN_EST NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PEDIDO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE SEGURIDAD NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE SOLICIT NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE SOLICIT_EST NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE SOLICIT_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE SOLICIT_ASIG_COMP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE SOLICIT_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ORDEN_ENTREGA NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE PER SET COD=@NEW WHERE COD=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE USU SET PER=@NEW WHERE PER=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE FIJ SET PER=@NEW WHERE PER=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PER SET PER=@NEW WHERE PER=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE APROB_LIM SET PER=@NEW WHERE PER=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CAT_PER SET PER=@NEW WHERE PER=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE LINEAS_EST SET PER=@NEW WHERE PER=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE NOTIF_TIPO1 SET NOTIF=@NEW WHERE NOTIF=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE NOTIF_TIPO2 SET PER=@NEW WHERE PER=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ORDEN_EST SET PER=@NEW WHERE PER=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PEDIDO SET PER=@NEW WHERE PER=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE SEGURIDAD SET APROB_TIPO1=@NEW WHERE APROB_TIPO1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE SOLICIT SET PER=@NEW WHERE PER=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE SOLICIT SET COMPRADOR=@NEW WHERE COMPRADOR=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE SOLICIT_EST SET PER=@NEW WHERE PER=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE SOLICIT_ADJUN SET PER=@NEW WHERE PER=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE SOLICIT_ASIG_COMP SET PER=@NEW WHERE PER=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE SOLICIT_ASIG_COMP SET COM=@NEW WHERE COM=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE SOLICIT_ADJUN SET PER=@NEW WHERE PER=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ORDEN_ENTREGA SET RECEPTOR=@NEW WHERE RECEPTOR=@OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PER CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FIJ CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PER CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE APROB_LIM CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CAT_PER CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_EST CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE NOTIF_TIPO1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE NOTIF_TIPO2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ORDEN_EST CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PEDIDO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE SEGURIDAD CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE SOLICIT CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE SOLICIT_EST CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE SOLICIT_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE SOLICIT_ASIG_COMP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE SOLICIT_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ORDEN_ENTREGA CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close D" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE D" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT OFF" & vbCrLf
sConsulta = sConsulta & "COMMIT TRANSACTION" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


End Sub

Public Function CodigoDeActualizacion2_14_00_50A2_14_00_51() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim bBool As Boolean

On Error GoTo error
    

sConsulta = "BEGIN TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = True
ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

frmProgreso.lblDetalle = "Modificamos tablas"
frmProgreso.lblDetalle.Refresh
frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_14_Tablas_51


frmProgreso.lblDetalle = "Modificamos storeds"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_14_Storeds_51


sConsulta = "UPDATE VERSION SET NUM ='2.14.00.51'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "COMMIT TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = False

CodigoDeActualizacion2_14_00_50A2_14_00_51 = True
Exit Function
    
error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_14_00_50A2_14_00_51 = False

End Function


Private Sub V_2_14_Tablas_51()
Dim sConsulta As String

sConsulta = "ALTER TABLE dbo.PROVE_EMPRESA_COD_ERP ADD CONSTRAINT" & vbCrLf
sConsulta = sConsulta & " FK_PROVE_EMPRESA_COD_ERP_PROVE FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & " (" & vbCrLf
sConsulta = sConsulta & "PROVE" & vbCrLf
sConsulta = sConsulta & ") REFERENCES dbo.PROVE" & vbCrLf
sConsulta = sConsulta & " (" & vbCrLf
sConsulta = sConsulta & "COD" & vbCrLf
sConsulta = sConsulta & ")" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE dbo.PROVE_EMPRESA_COD_ERP ADD CONSTRAINT" & vbCrLf
sConsulta = sConsulta & " FK_PROVE_EMPRESA_COD_ERP_PROVE_ERP FOREIGN KEY" & vbCrLf
sConsulta = sConsulta & " (" & vbCrLf
sConsulta = sConsulta & "COD_ERP," & vbCrLf
sConsulta = sConsulta & "EMPRESA" & vbCrLf
sConsulta = sConsulta & ") REFERENCES dbo.PROVE_ERP" & vbCrLf
sConsulta = sConsulta & " (" & vbCrLf
sConsulta = sConsulta & "COD_ERP," & vbCrLf
sConsulta = sConsulta & "EMPRESA" & vbCrLf
sConsulta = sConsulta & ")" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE dbo.PROCE_PROVE ADD" & vbCrLf
sConsulta = sConsulta & "   NO_OFE tinyint NOT NULL CONSTRAINT DF_PROCE_PROVE_NO_OFE DEFAULT (0)," & vbCrLf
sConsulta = sConsulta & "   FEC_NO_OFE datetime NULL," & vbCrLf
sConsulta = sConsulta & "   MOTIVO_NO_OFE varchar(4000) NULL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Private Sub V_2_14_Storeds_51()
Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_DEVOLVER_TODAS_ORDENES]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_DEVOLVER_TODAS_ORDENES]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSEP_DEVOLVER_TODAS_ORDENES @MAXLINEAS integer, @DESDEID varchar(50)=null, @APROBADOR varchar(50) = null,@APROVISIONADOR varchar(50) = null, @ARTINT varchar(50) = null, @ARTDEN varchar(500) = null, @ARTEXT varchar(500) = null, @DESDEFECHA varchar(10) = null, @HASTAFECHA varchar(10) = null, @PROVECOD varchar(50) = null, @PROVEDEN varchar(500) = null,@NUMPEDPROVE varchar(100) = null,@ANYO int = null,@PEDIDO int = null,@ORDEN int = null,@EST int  = null, @ID int = null ,@RECINCORRECTO int = null ,@COINCIDENCIA int =null,@REFERENCIA varchar(120) = null ,@CAMPO1 int = null ,@CAMPO2 int = null, @VALORCAMPO1 varchar(150) = null, @VALORCAMPO2 varchar(150)=null , @CATN1 int = null, @CATN2 int = null, @CATN3 int = null, @CATN4 int = null , @CATN5 int = null, @EMPRESA INT = NULL, @RECEPTOR VARCHAR(100) = NULL, @COD_ERP  VARCHAR(100) = NULL, @ENTIDAD VARCHAR(100) = 'ORDEN', @TIPO TINYINT = 1 AS " & vbCrLf
sConsulta = sConsulta & "declare @SQL nvarchar(4000)" & vbCrLf
sConsulta = sConsulta & "declare @PARAM nvarchar(1000)" & vbCrLf
sConsulta = sConsulta & "declare @SQL1 nvarchar(4000)" & vbCrLf
sConsulta = sConsulta & "declare @SQL2 nvarchar(4000)" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ENTIDAD = 'PROVE'" & vbCrLf
sConsulta = sConsulta & "  SET @SQL = N'SELECT DISTINCT P.COD, P.DEN , P.NIF, P.DIR, P.CP, P.PAI, P.POB, P.PROVI'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  IF @ENTIDAD = 'EMPRESA'" & vbCrLf
sConsulta = sConsulta & "    SET @SQL = N'SELECT DISTINCT E.ID, E.NIF, E.DEN, E.DIR, E.CP, E.POB, E.PROVI, E.PAI, PAI.DEN PAIDEN, PROVI.DEN PROVIDEN '" & vbCrLf
sConsulta = sConsulta & "  ELSE" & vbCrLf
sConsulta = sConsulta & "    SET @SQL = N'SELECT TOP ' + CONVERT(varchar,@MAXLINEAS) + ' CONVERT(VARCHAR,OE.EST) + LEFT(''00000000'',8-LEN(CONVERT(VARCHAR,OE.ID))) + CONVERT(VARCHAR,OE.ID), " & vbCrLf
sConsulta = sConsulta & "       OE.ID, OE.ANYO, PE.NUM NUMPEDIDO, OE.NUM NUMORDEN, OE.PROVE PROVECOD, P.DEN PROVEDEN, P.NIF," & vbCrLf
sConsulta = sConsulta & "       OE.NUMEXT, OE.FECHA FECHA, OE.EST EST, OE.IMPORTE IMPORTE, " & vbCrLf
sConsulta = sConsulta & "       P1.APE + '', '' + P1.NOM APROVISIONADOR,P1.APE,P1.NOM,OE.OBS," & vbCrLf
sConsulta = sConsulta & "       OES.COMENT, OES.FECHA FECEST,OE.IMPORTE,OE.PEDIDO,OE.TIPO, OE.INCORRECTA, PE.PER,OE.REFERENCIA,T3.ADJUNTOS, CP.COMENT COMENTPROVE, CP.EST ESTCOMENT,OE.MON, OE.EMPRESA, E.DEN DENEMPRESA, E.NIF NIFEMPRESA, OE.COD_PROVE_ERP, OE.RECEPTOR ,PER.NOM + '' '' + PER.APE NOMRECEPTOR, PER.TFNO TFNORECEPTOR, PER.EMAIL EMAILRECEPTOR, ISNULL(PER.UON3,ISNULL(PER.UON2,PER.UON1)) UONRECEPTOR, ISNULL(UON3.DEN,ISNULL(UON2.DEN,UON1.DEN)) DENUONRECEPTOR, PE.TIPO'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL= @SQL + N'" & vbCrLf
sConsulta = sConsulta & "  FROM ORDEN_ENTREGA OE" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN PROVE P" & vbCrLf
sConsulta = sConsulta & "            ON OE.PROVE = P.COD" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN PEDIDO PE" & vbCrLf
sConsulta = sConsulta & "            ON OE.PEDIDO = PE.ID '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ENTIDAD = 'EMPRESA'" & vbCrLf
sConsulta = sConsulta & "  SET @SQL= @SQL + N'" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN (EMP E LEFT JOIN PAI ON E.PAI = PAI.COD LEFT JOIN PROVI ON E.PAI = PROVI.PAI AND E.PROVI = PROVI.COD) ON OE.EMPRESA = E.ID'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  SET @SQL= @SQL + N'" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN EMP E ON OE.EMPRESA = E.ID" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN (PER LEFT JOIN UON1 ON PER.UON1 = UON1.COD LEFT JOIN UON2 ON PER.UON1 = UON2.UON1 AND PER.UON2 = UON2.COD LEFT JOIN UON3 ON PER.UON1 = UON3.UON1 AND PER.UON2 = UON3.UON2 AND PER.UON3 = UON3.COD) ON OE.RECEPTOR = PER.COD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN (SELECT OE.ID, OE.PEDIDO, OE.ORDEN, OE.EST, OE.COMENT " & vbCrLf
sConsulta = sConsulta & "                  FROM ORDEN_EST OE " & vbCrLf
sConsulta = sConsulta & "                        INNER JOIN (SELECT PEDIDO, ORDEN, MAX(ID) ID" & vbCrLf
sConsulta = sConsulta & "                                      FROM ORDEN_EST  WHERE EST IN (3, 21) " & vbCrLf
sConsulta = sConsulta & "                                    GROUP BY PEDIDO, ORDEN ) MO " & vbCrLf
sConsulta = sConsulta & "                                ON MO.ID = OE.ID) CP" & vbCrLf
sConsulta = sConsulta & "            ON OE.ID = CP.ORDEN" & vbCrLf
sConsulta = sConsulta & "'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @APROBADOR IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN (SELECT DISTINCT ORDEN , S.APROB_TIPO1 PER" & vbCrLf
sConsulta = sConsulta & "                           FROM LINEAS_PEDIDO LP " & vbCrLf
sConsulta = sConsulta & "                                    INNER JOIN SEGURIDAD S" & vbCrLf
sConsulta = sConsulta & "                                            ON LP.SEGURIDAD = S.ID " & vbCrLf
sConsulta = sConsulta & "                                           AND LP.NIVEL_APROB = 0) T1" & vbCrLf
sConsulta = sConsulta & "            ON OE.ID = T1.ORDEN" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN (SELECT DISTINCT ORDEN , AL.PER PER" & vbCrLf
sConsulta = sConsulta & "                            FROM LINEAS_PEDIDO LP " & vbCrLf
sConsulta = sConsulta & "                                     INNER JOIN APROB_LIM AL" & vbCrLf
sConsulta = sConsulta & "                                             ON LP.APROB_LIM = AL.ID) T2" & vbCrLf
sConsulta = sConsulta & "            ON OE.ID = T2.ORDEN '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN PER P1" & vbCrLf
sConsulta = sConsulta & "            ON PE.PER = P1.COD" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN ORDEN_EST OES" & vbCrLf
sConsulta = sConsulta & "            ON OE.ORDEN_EST = OES.ID'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "    LEFT JOIN (SELECT COUNT(*) AS ADJUNTOS,ORDEN  FROM orden_entrega_adjun GROUP BY orden_entrega_adjun.ORDEN) T3 ON OE.ID= T3.ORDEN '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if @ARTINT is not null or @ARTDEN is not null or @ARTEXT is not null or @CATN1 is not null or @CATN2 is not null or @CATN3 is not null or @CATN4 is not null or @CATN5 is not null or @CAMPO1 is not null or @CAMPO2 is not null or @VALORCAMPO1 is not null or @VALORCAMPO2 is not null" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "   set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN (SELECT DISTINCT ORDEN" & vbCrLf
sConsulta = sConsulta & "                  FROM LINEAS_PEDIDO LP" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN ITEM I " & vbCrLf
sConsulta = sConsulta & "             ON LP.ANYO = I.ANYO" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN1=I.GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND LP.PROCE=I.PROCE " & vbCrLf
sConsulta = sConsulta & "            AND LP.ITEM=I.ID'" & vbCrLf
sConsulta = sConsulta & "      IF @ARTDEN is NOT NULL " & vbCrLf
sConsulta = sConsulta & "         set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN ART4 " & vbCrLf
sConsulta = sConsulta & "             ON LP.GMN1=ART4.GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN2=ART4.GMN2 " & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN3=ART4.GMN3 " & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN4=ART4.GMN4 " & vbCrLf
sConsulta = sConsulta & "            AND LP.ART_INT=ART4.COD" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN ART4 ART_AUX " & vbCrLf
sConsulta = sConsulta & "             ON ART_AUX.COD=I.ART " & vbCrLf
sConsulta = sConsulta & "            AND ART_AUX.GMN1=I.GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND ART_AUX.GMN2=I.GMN2 " & vbCrLf
sConsulta = sConsulta & "            AND ART_AUX.GMN3=I.GMN3 " & vbCrLf
sConsulta = sConsulta & "            AND ART_AUX.GMN4=I.GMN4'" & vbCrLf
sConsulta = sConsulta & "      IF @ARTEXT is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN PROVE_ART4 PA" & vbCrLf
sConsulta = sConsulta & "             ON LP.PROVE = PA.PROVE" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN1 = PA.GMN1" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN2 = PA.GMN2" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN3 = PA.GMN3" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN4 = PA.GMN4" & vbCrLf
sConsulta = sConsulta & "            AND CASE WHEN LP.ART_INT IS NULL THEN I.ART ELSE LP.ART_INT END = PA.ART'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @TIPO = 1            " & vbCrLf
sConsulta = sConsulta & "   set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                 WHERE PE.TIPO=1'" & vbCrLf
sConsulta = sConsulta & "   ELSE IF @TIPO= 0" & vbCrLf
sConsulta = sConsulta & "   set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                 WHERE PE.TIPO=0'" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "   set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                 WHERE 1=1'" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   IF @CAMPO1 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQL = @SQL + N'   AND LP.CAMPO1 = @CAMPO1 '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @CAMPO2 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQL = @SQL + N'   AND LP.CAMPO2 = @CAMPO2 '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @VALORCAMPO1 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQL = @SQL + N'   AND LP.VALOR1 = @VALORCAMPO1 '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @VALORCAMPO2 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQL = @SQL + N'   AND LP.VALOR2 = @VALORCAMPO2 '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @CATN1 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQL = @SQL + N'   AND LP.CAT1 = @CATN1 '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @CATN2 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQL = @SQL + N'   AND LP.CAT2 = @CATN2 '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @CATN3 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQL = @SQL + N'   AND LP.CAT3 = @CATN3 '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @CATN4 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQL = @SQL + N'   AND LP.CAT4 = @CATN4 '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @CATN5 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQL = @SQL + N'   AND LP.CAT5 = @CATN5 '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  IF @ARTINT is NOT NULL " & vbCrLf
sConsulta = sConsulta & "     begin " & vbCrLf
sConsulta = sConsulta & "   IF @COINCIDENCIA=1 " & vbCrLf
sConsulta = sConsulta & "          set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                   AND (LP.ART_INT + I.ART like ''% ''  + @ARTINT + '' %''' +" & vbCrLf
sConsulta = sConsulta & "          ' OR LP.ART_INT + I.ART like @ARTINT  + '' %''' +" & vbCrLf
sConsulta = sConsulta & "          ' OR LP.ART_INT + I.ART like ''% '' + @ARTINT  ' + " & vbCrLf
sConsulta = sConsulta & "     ' OR LP.ART_INT + I.ART=@ARTINT)'" & vbCrLf
sConsulta = sConsulta & "        ELSE" & vbCrLf
sConsulta = sConsulta & "      set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                   AND LP.ART_INT + I.ART like + ''%'' + @ARTINT + ''%'' '" & vbCrLf
sConsulta = sConsulta & "     end" & vbCrLf
sConsulta = sConsulta & "   IF @ARTDEN is NOT NULL " & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "      IF @COINCIDENCIA=1 " & vbCrLf
sConsulta = sConsulta & "         set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                   AND (ART4.DEN + I.DESCR  + ART_AUX.DEN + LP.DESCR_LIBRE like ''% ''  + @ARTDEN + '' %''' + " & vbCrLf
sConsulta = sConsulta & "          ' OR ART4.DEN + I.DESCR  + ART_AUX.DEN + LP.DESCR_LIBRE like @ARTDEN + '' %''' +" & vbCrLf
sConsulta = sConsulta & "          ' OR ART4.DEN + I.DESCR  + ART_AUX.DEN + LP.DESCR_LIBRE like ''% '' + @ARTDEN ' + " & vbCrLf
sConsulta = sConsulta & "     ' OR ART4.DEN + I.DESCR  + ART_AUX.DEN + LP.DESCR_LIBRE =@ARTDEN)'" & vbCrLf
sConsulta = sConsulta & "      ELSE" & vbCrLf
sConsulta = sConsulta & "         set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                   AND ART4.DEN + I.DESCR  + ART_AUX.DEN + LP.DESCR_LIBRE like + ''%'' + @ARTDEN + ''%'''" & vbCrLf
sConsulta = sConsulta & "    END" & vbCrLf
sConsulta = sConsulta & "   IF @ARTEXT is NOT NULL" & vbCrLf
sConsulta = sConsulta & "     BEGIN" & vbCrLf
sConsulta = sConsulta & "       IF @COINCIDENCIA=1 " & vbCrLf
sConsulta = sConsulta & "         set @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                   AND (PA.COD_EXT like ''% ''  + @ARTEXT + '' %''' + " & vbCrLf
sConsulta = sConsulta & "          ' OR PA.COD_EXT like @ARTEXT + '' %''' +" & vbCrLf
sConsulta = sConsulta & "          ' OR PA.COD_EXT like ''% '' + @ARTEXT ' + " & vbCrLf
sConsulta = sConsulta & "     ' OR PA.COD_EXT =@ARTEXT)'" & vbCrLf
sConsulta = sConsulta & "       ELSE" & vbCrLf
sConsulta = sConsulta & "         set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                   AND PA.COD_EXT like + ''%'' + @ARTEXT + ''%'''  " & vbCrLf
sConsulta = sConsulta & "     END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   set  @SQL = @SQL + N') LP" & vbCrLf
sConsulta = sConsulta & "            ON OE.ID = LP.ORDEN" & vbCrLf
sConsulta = sConsulta & "'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "   IF @TIPO = 1            " & vbCrLf
sConsulta = sConsulta & "   set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                 WHERE PE.TIPO=1'" & vbCrLf
sConsulta = sConsulta & "   ELSE IF @TIPO= 0" & vbCrLf
sConsulta = sConsulta & "   set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                 WHERE PE.TIPO=0'" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "   set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                 WHERE 1=1'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if @REFERENCIA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.REFERENCIA = @REFERENCIA '" & vbCrLf
sConsulta = sConsulta & "if @DESDEFECHA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.FECHA >= CONVERT(datetime,@DESDEFECHA,103)'" & vbCrLf
sConsulta = sConsulta & "if @HASTAFECHA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.FECHA <= CONVERT(datetime,@HASTAFECHA,103)'" & vbCrLf
sConsulta = sConsulta & "if @PROVECOD IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.PROVE = @PROVECOD'" & vbCrLf
sConsulta = sConsulta & "if @NUMPEDPROVE IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.NUMEXT = @NUMPEDPROVE '" & vbCrLf
sConsulta = sConsulta & "if @ORDEN IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.NUM = @ORDEN '" & vbCrLf
sConsulta = sConsulta & "if @ANYO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.ANYO = @ANYO '" & vbCrLf
sConsulta = sConsulta & "if @PEDIDO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND PE.NUM = @PEDIDO '" & vbCrLf
sConsulta = sConsulta & "if @PROVEDEN IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND P.DEN  LIKE @PROVEDEN + ''%'' '" & vbCrLf
sConsulta = sConsulta & "IF @APROBADOR IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND (T1.PER = @APROBADOR " & vbCrLf
sConsulta = sConsulta & "                           OR T2.PER = @APROBADOR) '" & vbCrLf
sConsulta = sConsulta & "if @APROVISIONADOR IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND PE.PER = @APROVISIONADOR '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @EMPRESA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.EMPRESA = @EMPRESA '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @RECEPTOR IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.RECEPTOR = @RECEPTOR '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @COD_ERP IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.COD_PROVE_ERP = @COD_ERP '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "declare @IN varchar(500)" & vbCrLf
sConsulta = sConsulta & "SET @IN = '('" & vbCrLf
sConsulta = sConsulta & "IF @EST & 1 = 1" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '0,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 2 = 2" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '1,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 4 = 4" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '2,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 8 = 8" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '3,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 16 = 16" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '4,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 32 = 32" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '5,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 64 = 64" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '6,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 128 = 128" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '20,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 256 = 256" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '21,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 512 = 512" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '22,'" & vbCrLf
sConsulta = sConsulta & "SET @SQL1 = ''" & vbCrLf
sConsulta = sConsulta & "SET @SQL2 = ''" & vbCrLf
sConsulta = sConsulta & "IF @IN!='('" & vbCrLf
sConsulta = sConsulta & "     set @SQL1=' OE.EST IN ' + LEFT(@IN,LEN(@IN)-1) + ') '" & vbCrLf
sConsulta = sConsulta & "IF @RECINCORRECTO IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "  IF @RECINCORRECTO=1" & vbCrLf
sConsulta = sConsulta & "   set @SQL2 = ' OE.INCORRECTA=1'  " & vbCrLf
sConsulta = sConsulta & "IF @SQL1<>'' and @SQL2<>'' " & vbCrLf
sConsulta = sConsulta & "   SET @SQL= @SQL + ' AND (' + @SQL1 + ' OR ' + @SQL2 + ')'" & vbCrLf
sConsulta = sConsulta & "ELSE " & vbCrLf
sConsulta = sConsulta & "if @SQL1!=''" & vbCrLf
sConsulta = sConsulta & "   SET @SQL= @SQL + ' AND ' + @SQL1 " & vbCrLf
sConsulta = sConsulta & "else" & vbCrLf
sConsulta = sConsulta & "if @SQL2!=''" & vbCrLf
sConsulta = sConsulta & "   SET @SQL= @SQL + ' AND ' + @SQL2" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "if @ID IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + '" & vbCrLf
sConsulta = sConsulta & "   AND OE.ID=@ID '" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "if @DESDEID IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + '" & vbCrLf
sConsulta = sConsulta & "   AND CONVERT(VARCHAR,OE.EST) + LEFT(''00000000'',8-LEN(CONVERT(VARCHAR,OE.ID))) + CONVERT(VARCHAR,OE.ID)>=@DESDEID '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ENTIDAD = 'PROVE'" & vbCrLf
sConsulta = sConsulta & "  SET @SQL =  @SQL + N'" & vbCrLf
sConsulta = sConsulta & "    ORDER BY P.COD '" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  IF @ENTIDAD = 'EMPRESA'" & vbCrLf
sConsulta = sConsulta & "    SET @SQL =  @SQL + N'" & vbCrLf
sConsulta = sConsulta & "    ORDER BY E.NIF '" & vbCrLf
sConsulta = sConsulta & "  ELSE" & vbCrLf
sConsulta = sConsulta & "    set @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "    order by CONVERT(VARCHAR,OE.EST) + LEFT(''00000000'',8-LEN(CONVERT(VARCHAR,OE.ID))) + CONVERT(VARCHAR,OE.ID)'" & vbCrLf
sConsulta = sConsulta & "SET @PARAM = N'@APROBADOR varchar(50), @APROVISIONADOR varchar(50), @DESDEFECHA varchar(10), @HASTAFECHA varchar(10), @ARTINT varchar(50), @ARTDEN varchar(500), @ARTEXT varchar(500), @PROVECOD varchar(50), @PROVEDEN varchar(500), @NUMPEDPROVE varchar(100),@ORDEN int, @ANYO int, @PEDIDO int, @DESDEID varchar(50), @EST int, @ID int,@REFERENCIA varchar(120) , @CAMPO1 int ,@CAMPO2 int, @VALORCAMPO1 varchar(150) ,@VALORCAMPO2 varchar(150) ,@CATN1 int ,@CATN2 int ,@CATN3 int, @CATN4 int , @CATN5 int, @EMPRESA INT, @RECEPTOR VARCHAR(100), @COD_ERP VARCHAR(100) '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, @PARAM, @APROBADOR= @APROBADOR, @APROVISIONADOR = @APROVISIONADOR, @DESDEFECHA=@DESDEFECHA, @HASTAFECHA=@HASTAFECHA, @ARTINT=@ARTINT, @ARTDEN=@ARTDEN, @ARTEXT =@ARTEXT , @PROVECOD=@PROVECOD, @PROVEDEN=@PROVEDEN, @NUMPEDPROVE=@NUMPEDPROVE, @ANYO=@ANYO, @ORDEN=@ORDEN, @PEDIDO = @PEDIDO,@DESDEID=@DESDEID, @EST=@EST, @ID = @ID, @REFERENCIA=@REFERENCIA , @CAMPO1=@CAMPO1 , @CAMPO2=@CAMPO2 ,@VALORCAMPO1=@VALORCAMPO1 , @VALORCAMPO2=@VALORCAMPO2 , @CATN1=@CATN1 , @CATN2=@CATN2 ,@CATN3=@CATN3 ,@CATN4=@CATN4, @CATN5=@CATN5 , @EMPRESA = @EMPRESA, @RECEPTOR = @RECEPTOR, @COD_ERP = @COD_ERP" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_DEVOLVER_TODAS_ID_ORDENES]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_DEVOLVER_TODAS_ID_ORDENES]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSEP_DEVOLVER_TODAS_ID_ORDENES @MAXLINEAS integer, @APROBADOR varchar(50) =null,@APROVISIONADOR varchar(50)  =null,@ARTINT varchar(50)  =null, @ARTDEN varchar(500)  =null, @ARTEXT varchar(500)  =null, @DESDEFECHA varchar(10)  =null, @HASTAFECHA varchar(10)  =null, @PROVECOD varchar(50)  =null,@PROVEDEN varchar(500)  =null,@NUMPEDPROVE varchar(100)  =null,@ANYO int  =null,@PEDIDO int  =null,@ORDEN int  =null,@EST int  =null ,@COINCIDENCIA int =null,@REFERENCIA varchar(120)  =null,@CAMPO1 int=null ,@CAMPO2 int=null, @VALORCAMPO1 varchar(150)=null,@VALORCAMPO2 varchar(150)=null,@CATN1 int = NULL, @CATN2 int = NULL, @CATN3 int = NULL, @CATN4 int = NULL ,@CATN5 int = NULL, @EMPRESA INT = NULL, @RECEPTOR VARCHAR(100) = NULL, @COD_ERP  VARCHAR(100) = NULL, @TIPO TINYINT = 1  AS" & vbCrLf
sConsulta = sConsulta & "declare @SQL nvarchar(4000)" & vbCrLf
sConsulta = sConsulta & "declare @SQL2 nvarchar(4000)" & vbCrLf
sConsulta = sConsulta & "declare @PARAM nvarchar(1000)" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
sConsulta = sConsulta & "set  @SQL = N'" & vbCrLf
sConsulta = sConsulta & "SELECT CONVERT(VARCHAR,OE.EST) + LEFT(''00000000'',8-LEN(CONVERT(VARCHAR,OE.ID))) + CONVERT(VARCHAR,OE.ID)" & vbCrLf
sConsulta = sConsulta & "  FROM ORDEN_ENTREGA OE" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN PROVE P" & vbCrLf
sConsulta = sConsulta & "            ON OE.PROVE = P.COD" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN PEDIDO PE" & vbCrLf
sConsulta = sConsulta & "            ON OE.PEDIDO = PE.ID '" & vbCrLf
sConsulta = sConsulta & "IF @APROBADOR IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN (SELECT DISTINCT ORDEN , S.APROB_TIPO1 PER" & vbCrLf
sConsulta = sConsulta & "                           FROM LINEAS_PEDIDO LP " & vbCrLf
sConsulta = sConsulta & "                                    INNER JOIN SEGURIDAD S" & vbCrLf
sConsulta = sConsulta & "                                            ON LP.SEGURIDAD = S.ID " & vbCrLf
sConsulta = sConsulta & "                                           AND LP.NIVEL_APROB = 0) T1" & vbCrLf
sConsulta = sConsulta & "            ON OE.ID = T1.ORDEN" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN (SELECT DISTINCT ORDEN , AL.PER PER" & vbCrLf
sConsulta = sConsulta & "                            FROM LINEAS_PEDIDO LP " & vbCrLf
sConsulta = sConsulta & "                                     INNER JOIN APROB_LIM AL" & vbCrLf
sConsulta = sConsulta & "                                             ON LP.APROB_LIM = AL.ID) T2" & vbCrLf
sConsulta = sConsulta & "            ON OE.ID = T2.ORDEN '" & vbCrLf
sConsulta = sConsulta & "set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN PER P1" & vbCrLf
sConsulta = sConsulta & "            ON PE.PER = P1.COD" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN ORDEN_EST OES" & vbCrLf
sConsulta = sConsulta & "            ON OE.ORDEN_EST = OES.ID'" & vbCrLf
sConsulta = sConsulta & "if @ARTINT is not null or @ARTDEN is not null or @ARTEXT is not null or @CATN1 is not null or @CATN2 is not null or @CATN3 is not null or @CATN4 is not null or @CATN5 is not null or @CAMPO1 is not null or @CAMPO2 is not null or @VALORCAMPO1 is not null or @VALORCAMPO2 is not null" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "   set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN (SELECT DISTINCT ORDEN" & vbCrLf
sConsulta = sConsulta & "                  FROM LINEAS_PEDIDO LP" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN ITEM I " & vbCrLf
sConsulta = sConsulta & "             ON LP.ANYO = I.ANYO" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN1=I.GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND LP.PROCE=I.PROCE " & vbCrLf
sConsulta = sConsulta & "            AND LP.ITEM=I.ID'" & vbCrLf
sConsulta = sConsulta & "      IF @ARTDEN is NOT NULL " & vbCrLf
sConsulta = sConsulta & "         set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN ART4 " & vbCrLf
sConsulta = sConsulta & "             ON LP.GMN1=ART4.GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN2=ART4.GMN2 " & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN3=ART4.GMN3 " & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN4=ART4.GMN4 " & vbCrLf
sConsulta = sConsulta & "            AND LP.ART_INT=ART4.COD" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN ART4 ART_AUX " & vbCrLf
sConsulta = sConsulta & "             ON ART_AUX.COD=I.ART " & vbCrLf
sConsulta = sConsulta & "            AND ART_AUX.GMN1=I.GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND ART_AUX.GMN2=I.GMN2 " & vbCrLf
sConsulta = sConsulta & "            AND ART_AUX.GMN3=I.GMN3 " & vbCrLf
sConsulta = sConsulta & "            AND ART_AUX.GMN4=I.GMN4'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "      IF @ARTEXT is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN PROVE_ART4 PA" & vbCrLf
sConsulta = sConsulta & "             ON LP.PROVE = PA.PROVE" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN1 = PA.GMN1" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN2 = PA.GMN2" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN3 = PA.GMN3" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN4 = PA.GMN4" & vbCrLf
sConsulta = sConsulta & "            AND CASE WHEN LP.ART_INT IS NULL THEN I.ART ELSE LP.ART_INT END = PA.ART'             " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @TIPO = 1            " & vbCrLf
sConsulta = sConsulta & "   set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                 WHERE PE.TIPO=1'" & vbCrLf
sConsulta = sConsulta & "   ELSE IF @TIPO= 0" & vbCrLf
sConsulta = sConsulta & "   set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                 WHERE PE.TIPO=0'" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "   set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                 WHERE 1=1'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @CAMPO1 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQL = @SQL + N'   AND LP.CAMPO1 = @CAMPO1 '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @CAMPO2 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQL = @SQL + N'   AND LP.CAMPO2 = @CAMPO2 '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @VALORCAMPO1 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQL = @SQL + N'   AND LP.VALOR1 = @VALORCAMPO1 '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @VALORCAMPO2 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQL = @SQL + N'   AND LP.VALOR2 = @VALORCAMPO2 '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @CATN1 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQL = @SQL + N'   AND LP.CAT1 = @CATN1 '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @CATN2 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQL = @SQL + N'   AND LP.CAT2 = @CATN2 '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @CATN3 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQL = @SQL + N'   AND LP.CAT3 = @CATN3 '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @CATN4 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQL = @SQL + N'   AND LP.CAT4 = @CATN4 '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @CATN5 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQL = @SQL + N'   AND LP.CAT5 = @CATN5 '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @EMPRESA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "      SET @SQL = @SQL + N'   AND OE.EMPRESA = @EMPRESA '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @ARTINT is NOT NULL " & vbCrLf
sConsulta = sConsulta & "     begin " & vbCrLf
sConsulta = sConsulta & "   IF @COINCIDENCIA=1 " & vbCrLf
sConsulta = sConsulta & "          set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                   AND (LP.ART_INT + I.ART like ''% ''  + @ARTINT + '' %''' +" & vbCrLf
sConsulta = sConsulta & "          ' OR LP.ART_INT + I.ART like @ARTINT  + '' %''' +" & vbCrLf
sConsulta = sConsulta & "          ' OR LP.ART_INT + I.ART like ''% '' + @ARTINT  ' + " & vbCrLf
sConsulta = sConsulta & "     ' OR LP.ART_INT + I.ART=@ARTINT)'" & vbCrLf
sConsulta = sConsulta & "        ELSE" & vbCrLf
sConsulta = sConsulta & "      set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                   AND LP.ART_INT + I.ART like + ''%'' + @ARTINT + ''%'' '" & vbCrLf
sConsulta = sConsulta & "     end" & vbCrLf
sConsulta = sConsulta & "   IF @ARTDEN is NOT NULL " & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "      IF @COINCIDENCIA=1 " & vbCrLf
sConsulta = sConsulta & "         set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                   AND (ART4.DEN + I.DESCR  + ART_AUX.DEN + LP.DESCR_LIBRE like ''% ''  + @ARTDEN + '' %''' + " & vbCrLf
sConsulta = sConsulta & "          ' OR ART4.DEN + I.DESCR  + ART_AUX.DEN + LP.DESCR_LIBRE like @ARTDEN + '' %''' +" & vbCrLf
sConsulta = sConsulta & "          ' OR ART4.DEN + I.DESCR  + ART_AUX.DEN + LP.DESCR_LIBRE like ''% '' + @ARTDEN ' + " & vbCrLf
sConsulta = sConsulta & "     ' OR ART4.DEN + I.DESCR  + ART_AUX.DEN + LP.DESCR_LIBRE =@ARTDEN)'" & vbCrLf
sConsulta = sConsulta & "      ELSE" & vbCrLf
sConsulta = sConsulta & "         set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                   AND ART4.DEN + I.DESCR  + ART_AUX.DEN + LP.DESCR_LIBRE like + ''%'' + @ARTDEN + ''%'''" & vbCrLf
sConsulta = sConsulta & "    END" & vbCrLf
sConsulta = sConsulta & "   IF @ARTEXT is NOT NULL" & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "       IF @COINCIDENCIA=1 " & vbCrLf
sConsulta = sConsulta & "         set @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                   AND (PA.COD_EXT like ''% ''  + @ARTEXT + '' %''' + " & vbCrLf
sConsulta = sConsulta & "          ' OR PA.COD_EXT like @ARTEXT + '' %''' +" & vbCrLf
sConsulta = sConsulta & "          ' OR PA.COD_EXT like ''% '' + @ARTEXT ' + " & vbCrLf
sConsulta = sConsulta & "     ' OR PA.COD_EXT =@ARTEXT)'" & vbCrLf
sConsulta = sConsulta & "       ELSE" & vbCrLf
sConsulta = sConsulta & "         set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                   AND PA.COD_EXT like + ''%'' + @ARTEXT + ''%'''  " & vbCrLf
sConsulta = sConsulta & "     END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   set  @SQL = @SQL + N') LP" & vbCrLf
sConsulta = sConsulta & "            ON OE.ID = LP.ORDEN" & vbCrLf
sConsulta = sConsulta & "'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "        " & vbCrLf
sConsulta = sConsulta & "   IF @TIPO = 1            " & vbCrLf
sConsulta = sConsulta & "   set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                 WHERE PE.TIPO=1'" & vbCrLf
sConsulta = sConsulta & "   ELSE IF @TIPO= 0" & vbCrLf
sConsulta = sConsulta & "   set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                 WHERE PE.TIPO=0'" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "   set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                 WHERE 1=1'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if @DESDEFECHA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.FECHA >= CONVERT(datetime,@DESDEFECHA,103)'" & vbCrLf
sConsulta = sConsulta & "if @HASTAFECHA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.FECHA <= CONVERT(datetime,@HASTAFECHA,103)'" & vbCrLf
sConsulta = sConsulta & "if @PROVECOD IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.PROVE = @PROVECOD'" & vbCrLf
sConsulta = sConsulta & "if @NUMPEDPROVE IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.NUMEXT = @NUMPEDPROVE '" & vbCrLf
sConsulta = sConsulta & "if @ORDEN IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.NUM = @ORDEN '" & vbCrLf
sConsulta = sConsulta & "if @ANYO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.ANYO = @ANYO '" & vbCrLf
sConsulta = sConsulta & "if @REFERENCIA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.REFERENCIA = @REFERENCIA '" & vbCrLf
sConsulta = sConsulta & "if @PEDIDO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND PE.NUM = @PEDIDO '" & vbCrLf
sConsulta = sConsulta & "if @PROVEDEN IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND P.DEN  LIKE @PROVEDEN + ''%'' '" & vbCrLf
sConsulta = sConsulta & "IF @APROBADOR IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND (T1.PER = @APROBADOR " & vbCrLf
sConsulta = sConsulta & "                           OR T2.PER = @APROBADOR) '" & vbCrLf
sConsulta = sConsulta & "if @APROVISIONADOR IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND PE.PER = @APROVISIONADOR '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @EMPRESA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.EMPRESA = @EMPRESA '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @RECEPTOR IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.RECEPTOR = @RECEPTOR '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @COD_ERP IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.COD_PROVE_ERP = @COD_ERP '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "declare @IN varchar(500)" & vbCrLf
sConsulta = sConsulta & "SET @IN = '('" & vbCrLf
sConsulta = sConsulta & "IF @EST & 1 = 1" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '0,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 2 = 2" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '1,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 4 = 4" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '2,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 8 = 8" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '3,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 16 = 16" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '4,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 32 = 32" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '5,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 64 = 64" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '6,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 128 = 128" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '20,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 256 = 256" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '21,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 512 = 512" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '22,'" & vbCrLf
sConsulta = sConsulta & "IF @IN!='('" & vbCrLf
sConsulta = sConsulta & "   set @SQL = @SQL + '" & vbCrLf
sConsulta = sConsulta & "   AND OE.EST IN ' + LEFT(@IN,LEN(@IN)-1) + ') '" & vbCrLf
sConsulta = sConsulta & "set @SQL = @SQL + '" & vbCrLf
sConsulta = sConsulta & "order by CONVERT(VARCHAR,OE.EST) + LEFT(''00000000'',8-LEN(CONVERT(VARCHAR,OE.ID))) + CONVERT(VARCHAR,OE.ID)'" & vbCrLf
sConsulta = sConsulta & "SET @SQL2 = '" & vbCrLf
sConsulta = sConsulta & "create table #IDS (id varchar(200))" & vbCrLf
sConsulta = sConsulta & "set nocount on" & vbCrLf
sConsulta = sConsulta & "DECLARE C1 CURSOR FOR " & vbCrLf
sConsulta = sConsulta & "' + @SQL + '" & vbCrLf
sConsulta = sConsulta & "DECLARE @miID varchar(200)" & vbCrLf
sConsulta = sConsulta & "declare @cont int" & vbCrLf
sConsulta = sConsulta & "set @cont=1" & vbCrLf
sConsulta = sConsulta & "OPEN C1" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C1 INTO @miID" & vbCrLf
sConsulta = sConsulta & "while @@fetch_status=0" & vbCrLf
sConsulta = sConsulta & "begin" & vbCrLf
sConsulta = sConsulta & "if @cont = 1" & vbCrLf
sConsulta = sConsulta & " insert into #IDS values (@miID)" & vbCrLf
sConsulta = sConsulta & "set @cont = @cont + 1        " & vbCrLf
sConsulta = sConsulta & "     " & vbCrLf
sConsulta = sConsulta & "if @cont = ' + CONVERT(VARCHAR,@MAXLINEAS) + ' + 1" & vbCrLf
sConsulta = sConsulta & " set @cont = 1" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C1 INTO @miID" & vbCrLf
sConsulta = sConsulta & "end" & vbCrLf
sConsulta = sConsulta & "CLOSE C1" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C1" & vbCrLf
sConsulta = sConsulta & "set nocount off" & vbCrLf
sConsulta = sConsulta & "select * from #ids" & vbCrLf
sConsulta = sConsulta & "'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @PARAM = N'@APROBADOR varchar(50), @APROVISIONADOR varchar(50), @DESDEFECHA varchar(10), @HASTAFECHA varchar(10), @ARTINT varchar(50), @ARTDEN varchar(500), @ARTEXT varchar(500), @PROVECOD varchar(50), @PROVEDEN varchar(500), @NUMPEDPROVE varchar(100),@ORDEN int, @ANYO int, @PEDIDO int, @EST int,@REFERENCIA varchar(120), @CAMPO1 int ,@CAMPO2 int, @VALORCAMPO1 varchar(150) ,@VALORCAMPO2 varchar(150) ,@CATN1 int ,@CATN2 int ,@CATN3 int, @CATN4 int , @CATN5 int, @EMPRESA INT, @RECEPTOR VARCHAR(100), @COD_ERP VARCHAR(100)'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL2, @PARAM, @APROBADOR= @APROBADOR, @APROVISIONADOR = @APROVISIONADOR, @DESDEFECHA=@DESDEFECHA, @HASTAFECHA=@HASTAFECHA, @ARTINT=@ARTINT, @ARTDEN=@ARTDEN, @ARTEXT=@ARTEXT, @PROVECOD=@PROVECOD, @PROVEDEN=@PROVEDEN, @NUMPEDPROVE=@NUMPEDPROVE, @ANYO=@ANYO, @ORDEN=@ORDEN, @PEDIDO = @PEDIDO, @EST=@EST,@REFERENCIA=@REFERENCIA , @CAMPO1=@CAMPO1 , @CAMPO2=@CAMPO2 ,@VALORCAMPO1=@VALORCAMPO1 , @VALORCAMPO2=@VALORCAMPO2 , @CATN1=@CATN1 , @CATN2=@CATN2 ,@CATN3=@CATN3 ,@CATN4=@CATN4, @CATN5=@CATN5, @EMPRESA = @EMPRESA, @RECEPTOR = @RECEPTOR, @COD_ERP = @COD_ERP" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_APROB_DEVOLVER_ORDEN]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_APROB_DEVOLVER_ORDEN]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSEP_APROB_DEVOLVER_ORDEN(@ID INTEGER, @APROBADOR VARCHAR(50))  AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SELECT ORDEN_ENTREGA.ID AS ORDENID,ORDEN_ENTREGA.ANYO AS ANYO" & vbCrLf
sConsulta = sConsulta & ",PEDIDO.NUM AS NUMPEDIDO" & vbCrLf
sConsulta = sConsulta & ",ORDEN_ENTREGA.NUM AS NUMORDEN" & vbCrLf
sConsulta = sConsulta & ",ORDEN_ENTREGA.REFERENCIA AS REFERENCIA" & vbCrLf
sConsulta = sConsulta & ",ORDEN_ENTREGA.PROVE + ' ' + PROVE.DEN AS PROVE" & vbCrLf
sConsulta = sConsulta & ",ORDEN_ENTREGA.PROVE PROVECOD" & vbCrLf
sConsulta = sConsulta & ",PROVE.DEN PROVEDEN" & vbCrLf
sConsulta = sConsulta & ",ORDEN_ENTREGA.FECHA" & vbCrLf
sConsulta = sConsulta & ",ORDEN_ENTREGA.EST" & vbCrLf
sConsulta = sConsulta & ",ORDEN_ENTREGA.IMPORTE " & vbCrLf
sConsulta = sConsulta & ",PER.APE + ',' + PER.NOM AS APROVISIONADOR" & vbCrLf
sConsulta = sConsulta & ",ORDEN_ENTREGA.OBS" & vbCrLf
sConsulta = sConsulta & ",case WHEN LP.NIVEL_APROB=0 THEN 1 WHEN LP.NIVEL_APROB > 0 THEN 2 END  MOTIVO" & vbCrLf
sConsulta = sConsulta & ",COUNT(ORDEN_ENTREGA_ADJUN.ID)  ARCH" & vbCrLf
sConsulta = sConsulta & ", ORDEN_ENTREGA.EMPRESA, E.DEN DENEMPRESA, E.NIF NIFEMPRESA, ORDEN_ENTREGA.COD_PROVE_ERP, ORDEN_ENTREGA.RECEPTOR ,R.NOM + ' ' + R.APE NOMRECEPTOR, R.TFNO TFNORECEPTOR, R.EMAIL EMAILRECEPTOR, ISNULL(R.UON3,ISNULL(R.UON2,R.UON1)) UONRECEPTOR, ISNULL(UON3.DEN,ISNULL(UON2.DEN,UON1.DEN)) DENUONRECEPTOR" & vbCrLf
sConsulta = sConsulta & "FROM ORDEN_ENTREGA  " & vbCrLf
sConsulta = sConsulta & "INNER JOIN PEDIDO ON PEDIDO.ID=ORDEN_ENTREGA.PEDIDO AND ORDEN_ENTREGA.ID=@ID" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PER ON PER.COD=PEDIDO.PER" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PROVE ON PROVE.COD=ORDEN_ENTREGA.PROVE AND ORDEN_ENTREGA.ID=@ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN (" & vbCrLf
sConsulta = sConsulta & "     SELECT ORDEN,MIN(NIVEL_APROB) NIVEL_APROB FROM LINEAS_PEDIDO L " & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN SEGURIDAD S ON S.ID= L.SEGURIDAD AND L.NIVEL_APROB=0 AND L.EST=0" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN APROB_LIM AP ON AP.SEGURIDAD=L.SEGURIDAD AND AP.ID=L.APROB_LIM AND AP.NIVEL=L.NIVEL_APROB" & vbCrLf
sConsulta = sConsulta & "      WHERE L.ORDEN=@ID AND L.EST=0  AND (AP.PER=@APROBADOR OR S.APROB_TIPO1=@APROBADOR) GROUP BY ORDEN) LP" & vbCrLf
sConsulta = sConsulta & " ON LP.ORDEN=ORDEN_ENTREGA.ID" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN ORDEN_ENTREGA_ADJUN ON ORDEN_ENTREGA.ID=ORDEN_ENTREGA_ADJUN.ORDEN" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN EMP E ON ORDEN_ENTREGA.EMPRESA = E.ID" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN (PER R LEFT JOIN UON1 ON R.UON1 = UON1.COD LEFT JOIN UON2 ON R.UON1 = UON2.UON1 AND R.UON2 = UON2.COD LEFT JOIN UON3 ON R.UON1 = UON3.UON1 AND R.UON2 = UON3.UON2 AND R.UON3 = UON3.COD) ON ORDEN_ENTREGA.RECEPTOR = R.COD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "GROUP BY    ORDEN_ENTREGA.ID ,ORDEN_ENTREGA.ANYO " & vbCrLf
sConsulta = sConsulta & ",PEDIDO.NUM " & vbCrLf
sConsulta = sConsulta & ",ORDEN_ENTREGA.NUM " & vbCrLf
sConsulta = sConsulta & ",ORDEN_ENTREGA.REFERENCIA " & vbCrLf
sConsulta = sConsulta & ",ORDEN_ENTREGA.PROVE + ' ' + PROVE.DEN " & vbCrLf
sConsulta = sConsulta & ",ORDEN_ENTREGA.PROVE " & vbCrLf
sConsulta = sConsulta & ",PROVE.DEN " & vbCrLf
sConsulta = sConsulta & ",ORDEN_ENTREGA.FECHA" & vbCrLf
sConsulta = sConsulta & ",ORDEN_ENTREGA.EST" & vbCrLf
sConsulta = sConsulta & ",ORDEN_ENTREGA.IMPORTE " & vbCrLf
sConsulta = sConsulta & ",PER.APE + ',' + PER.NOM " & vbCrLf
sConsulta = sConsulta & ",ORDEN_ENTREGA.OBS,LP.NIVEL_APROB" & vbCrLf
sConsulta = sConsulta & ", ORDEN_ENTREGA.EMPRESA, E.DEN , E.NIF , ORDEN_ENTREGA.COD_PROVE_ERP, ORDEN_ENTREGA.RECEPTOR ,R.NOM + ' ' + R.APE , R.TFNO , R.EMAIL , ISNULL(R.UON3,ISNULL(R.UON2,R.UON1)) , ISNULL(UON3.DEN,ISNULL(UON2.DEN,UON1.DEN)) " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_APROB_DEVOLVER_ORDENES]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_APROB_DEVOLVER_ORDENES]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSEP_APROB_DEVOLVER_ORDENES(@APROBADOR VARCHAR(50), @MAXLINEAS INTEGER, @DESDEID INTEGER," & vbCrLf
sConsulta = sConsulta & "@ANYO INTEGER, @NUMPEDIDO INTEGER, @ORDEN INTEGER, @APROBADOS INTEGER,@DENEGADOS INTEGER,@CODPROVE VARCHAR(50),@NUMPEDIDOPROVE VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & ",@DESDEFECHA VARCHAR(10), @APROVISIONADOR VARCHAR(50), @REFERENCIA varchar(120) = null ,@CAMPO1 int = null ,@CAMPO2 int = null, " & vbCrLf
sConsulta = sConsulta & "@VALORCAMPO1 varchar(150) = null, @VALORCAMPO2 varchar(150)=null , @CATN1 int = null, @CATN2 int = null, @CATN3 int = null, @CATN4 int = null , @CATN5 int = null ," & vbCrLf
sConsulta = sConsulta & " @EMPRESA INT = NULL, @RECEPTOR VARCHAR(100) = NULL, @COD_ERP  VARCHAR(100) = NULL, @HASTAFECHA VARCHAR(10))  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @INDICE INTEGER" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING= 'SELECT DISTINCT OE.ID AS ORDENID" & vbCrLf
sConsulta = sConsulta & ",OE.ANYO AS ANYO" & vbCrLf
sConsulta = sConsulta & ",PEDIDO.NUM AS NUMPEDIDO" & vbCrLf
sConsulta = sConsulta & ",OE.NUM AS NUMORDEN" & vbCrLf
sConsulta = sConsulta & ",OE.REFERENCIA AS REFERENCIA" & vbCrLf
sConsulta = sConsulta & ",OE.PROVE + '' '' + PROVE.DEN AS PROVE" & vbCrLf
sConsulta = sConsulta & ",OE.PROVE AS PROVECOD" & vbCrLf
sConsulta = sConsulta & ",PROVE.DEN AS PROVEDEN" & vbCrLf
sConsulta = sConsulta & ",OE.FECHA" & vbCrLf
sConsulta = sConsulta & ",OE.EST" & vbCrLf
sConsulta = sConsulta & ",OE.IMPORTE " & vbCrLf
sConsulta = sConsulta & ",PER.APE + '','' + PER.NOM AS APROVISIONADOR" & vbCrLf
sConsulta = sConsulta & ",OE.OBS" & vbCrLf
sConsulta = sConsulta & ",1 MOTIVO" & vbCrLf
sConsulta = sConsulta & ",COUNT(ORDEN_ENTREGA_ADJUN.ID)  ARCH" & vbCrLf
sConsulta = sConsulta & ", OE.EMPRESA, E.DEN DENEMPRESA, E.NIF NIFEMPRESA, OE.COD_PROVE_ERP, OE.RECEPTOR ,R.NOM + '' '' + R.APE NOMRECEPTOR, R.TFNO TFNORECEPTOR, R.EMAIL EMAILRECEPTOR, ISNULL(R.UON3,ISNULL(R.UON2,R.UON1)) UONRECEPTOR, ISNULL(UON3.DEN,ISNULL(UON2.DEN,UON1.DEN)) DENUONRECEPTOR" & vbCrLf
sConsulta = sConsulta & "FROM LINEAS_PEDIDO " & vbCrLf
sConsulta = sConsulta & "INNER JOIN SEGURIDAD ON LINEAS_PEDIDO.SEGURIDAD=SEGURIDAD.ID AND SEGURIDAD.APROB_TIPO1='''+ @APROBADOR +''' AND LINEAS_PEDIDO.NIVEL_APROB=0'" & vbCrLf
sConsulta = sConsulta & "IF @APROBADOS=0 AND @DENEGADOS <> 0" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND LINEAS_PEDIDO.EST IN (0,20)' " & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & " IF @APROBADOS<>0 AND @DENEGADOS=0" & vbCrLf
sConsulta = sConsulta & "     SET @SQLSTRING= @SQLSTRING + ' AND LINEAS_PEDIDO.EST IN (0,1,2,3)'" & vbCrLf
sConsulta = sConsulta & " ELSE            " & vbCrLf
sConsulta = sConsulta & "     IF @APROBADOS=0 AND @DENEGADOS=0" & vbCrLf
sConsulta = sConsulta & "         SET @SQLSTRING= @SQLSTRING + ' AND LINEAS_PEDIDO.EST =0 '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING= @SQLSTRING + ' INNER JOIN ORDEN_ENTREGA OE ON LINEAS_PEDIDO.ORDEN=OE.ID'" & vbCrLf
sConsulta = sConsulta & "IF @ORDEN<>0" & vbCrLf
sConsulta = sConsulta & "  SET @SQLSTRING= @SQLSTRING + ' AND OE.NUM=' + CONVERT(VARCHAR,@ORDEN ) " & vbCrLf
sConsulta = sConsulta & "if @REFERENCIA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQLSTRING = @SQLSTRING + N'   AND OE.REFERENCIA = ''' + @REFERENCIA + ''''" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING= @SQLSTRING + ' INNER JOIN PEDIDO ON OE.PEDIDO=PEDIDO.ID '" & vbCrLf
sConsulta = sConsulta & "IF @NUMPEDIDO <>0 " & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND PEDIDO.NUM=' +CONVERT(VARCHAR,@NUMPEDIDO )" & vbCrLf
sConsulta = sConsulta & "IF @APROVISIONADOR <>''" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND PEDIDO.PER=''' + @APROVISIONADOR +''' '" & vbCrLf
sConsulta = sConsulta & "if @ANYO<>0" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING=@SQLSTRING + ' AND PEDIDO.ANYO =' + CONVERT(VARCHAR,@ANYO) " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING=@SQLSTRING + '  INNER JOIN PROVE ON OE.PROVE=PROVE.COD INNER JOIN PER ON PER.COD=PEDIDO.PER '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING=@SQLSTRING + '  LEFT JOIN ORDEN_ENTREGA_ADJUN ON OE.ID=ORDEN_ENTREGA_ADJUN.ORDEN '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING=@SQLSTRING + ' LEFT JOIN EMP E ON OE.EMPRESA = E.ID" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN (PER R LEFT JOIN UON1 ON R.UON1 = UON1.COD LEFT JOIN UON2 ON R.UON1 = UON2.UON1 AND R.UON2 = UON2.COD LEFT JOIN UON3 ON R.UON1 = UON3.UON1 AND R.UON2 = UON3.UON2 AND R.UON3 = UON3.COD) ON OE.RECEPTOR = R.COD'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING=@SQLSTRING + ' WHERE 1=1'" & vbCrLf
sConsulta = sConsulta & "IF @CAMPO1 is NOT NULL and @VALORCAMPO1 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQLSTRING = @SQLSTRING + ' AND LINEAS_PEDIDO.CAMPO1 = ' + CONVERT(VARCHAR,@CAMPO1) + N'   AND LINEAS_PEDIDO.VALOR1 = ''' + @VALORCAMPO1 +''''" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CAMPO2 is NOT NULL and @VALORCAMPO2 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQLSTRING = @SQLSTRING + ' AND LINEAS_PEDIDO.CAMPO2 = ' + CONVERT(VARCHAR,@CAMPO2) + N'   AND LINEAS_PEDIDO.VALOR2 = ''' + @VALORCAMPO2 + ''''" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CATN1 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQLSTRING = @SQLSTRING + N'   AND LINEAS_PEDIDO.CAT1 = ' + CONVERT(VARCHAR,@CATN1)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CATN2 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQLSTRING = @SQLSTRING + N'   AND LINEAS_PEDIDO.CAT2 = ' + CONVERT(VARCHAR,@CATN2) " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CATN3 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQLSTRING = @SQLSTRING + N'   AND LINEAS_PEDIDO.CAT3 = ' + CONVERT(VARCHAR,@CATN3) " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CATN4 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQLSTRING = @SQLSTRING + N'   AND LINEAS_PEDIDO.CAT4 = ' + CONVERT(VARCHAR,@CATN4) " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CATN5 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQLSTRING = @SQLSTRING + N'   AND LINEAS_PEDIDO.CAT5 = ' + CONVERT(VARCHAR,@CATN5) " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @DESDEID <> 0 " & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND OE.ID >=' + CONVERT(VARCHAR,@DESDEID) " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CODPROVE <>'' " & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND OE.PROVE =''' + @CODPROVE + ''''" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @NUMPEDIDOPROVE <> ''" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND OE.NUMEXT = ''' + @NUMPEDIDOPROVE + ''''" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @DESDEFECHA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND OE.FECHA >=CONVERT(DATETIME,''' + @DESDEFECHA + ''',103) '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if @HASTAFECHA  IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQLSTRING = @SQLSTRING + N'   AND OE.FECHA <= CONVERT(datetime,''' + @HASTAFECHA + ''',103)'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @EMPRESA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQLSTRING = @SQLSTRING + N'   AND OE.EMPRESA=' +CONVERT(VARCHAR,@EMPRESA )" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @RECEPTOR IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQLSTRING = @SQLSTRING + N'   AND OE.RECEPTOR  = ''' + @RECEPTOR + '''' " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @COD_ERP IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQLSTRING = @SQLSTRING + N'   AND OE.COD_PROVE_ERP = ''' +  @COD_ERP + ''''" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' GROUP BY OE.ID, OE.ANYO ,PEDIDO.NUM ,OE.NUM " & vbCrLf
sConsulta = sConsulta & ",OE.REFERENCIA ,OE.PROVE + '' '' + PROVE.DEN ,OE.PROVE ,PROVE.DEN " & vbCrLf
sConsulta = sConsulta & ",OE.FECHA ,OE.EST ,OE.IMPORTE   ,PER.APE + '','' + PER.NOM ,OE.OBS,  OE.EMPRESA, E.DEN , E.NIF , OE.COD_PROVE_ERP, OE.RECEPTOR ,R.NOM + '' '' + R.APE , R.TFNO , R.EMAIL , ISNULL(R.UON3,ISNULL(R.UON2,R.UON1)) , ISNULL(UON3.DEN,ISNULL(UON2.DEN,UON1.DEN))  '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING= @SQLSTRING + ' UNION SELECT DISTINCT '" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING= @SQLSTRING + ' OE.ID AS ORDENID,OE.ANYO AS ANYO" & vbCrLf
sConsulta = sConsulta & ",PEDIDO.NUM AS NUMPEDIDO" & vbCrLf
sConsulta = sConsulta & ",OE.NUM AS NUMORDEN" & vbCrLf
sConsulta = sConsulta & ",OE.REFERENCIA " & vbCrLf
sConsulta = sConsulta & ",OE.PROVE + '' '' + PROVE.DEN AS PROVE" & vbCrLf
sConsulta = sConsulta & ",OE.PROVE AS PROVECOD" & vbCrLf
sConsulta = sConsulta & ",PROVE.DEN AS PROVEDEN" & vbCrLf
sConsulta = sConsulta & ",OE.FECHA" & vbCrLf
sConsulta = sConsulta & ",OE.EST" & vbCrLf
sConsulta = sConsulta & ",OE.IMPORTE " & vbCrLf
sConsulta = sConsulta & ",PER.APE + '','' + PER.NOM AS APROVISIONADOR" & vbCrLf
sConsulta = sConsulta & ",OE.OBS" & vbCrLf
sConsulta = sConsulta & ",2 MOTIVO" & vbCrLf
sConsulta = sConsulta & ",COUNT(ORDEN_ENTREGA_ADJUN.ID)  ARCH" & vbCrLf
sConsulta = sConsulta & ", OE.EMPRESA, E.DEN DENEMPRESA, E.NIF NIFEMPRESA, OE.COD_PROVE_ERP, OE.RECEPTOR ,R.NOM + '' '' + R.APE NOMRECEPTOR, R.TFNO TFNORECEPTOR, R.EMAIL EMAILRECEPTOR, ISNULL(R.UON3,ISNULL(R.UON2,R.UON1)) UONRECEPTOR, ISNULL(UON3.DEN,ISNULL(UON2.DEN,UON1.DEN)) DENUONRECEPTOR" & vbCrLf
sConsulta = sConsulta & "FROM LINEAS_PEDIDO INNER JOIN APROB_LIM" & vbCrLf
sConsulta = sConsulta & "ON LINEAS_PEDIDO.APROB_LIM=APROB_LIM.ID AND APROB_LIM.PER='''+ @APROBADOR +''''" & vbCrLf
sConsulta = sConsulta & "IF @APROBADOS=0 AND @DENEGADOS <> 0" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND LINEAS_PEDIDO.EST IN (0,20)' " & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & " IF @APROBADOS<>0 AND @DENEGADOS=0" & vbCrLf
sConsulta = sConsulta & "     SET @SQLSTRING= @SQLSTRING + ' AND LINEAS_PEDIDO.EST IN (0,1,2,3)'" & vbCrLf
sConsulta = sConsulta & " ELSE" & vbCrLf
sConsulta = sConsulta & "     IF @APROBADOS=0 AND @DENEGADOS=0    " & vbCrLf
sConsulta = sConsulta & "         SET @SQLSTRING= @SQLSTRING + ' AND LINEAS_PEDIDO.EST =0 '" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING= @SQLSTRING + ' INNER JOIN ORDEN_ENTREGA OE ON LINEAS_PEDIDO.ORDEN=OE.ID '" & vbCrLf
sConsulta = sConsulta & "IF @ORDEN<>0" & vbCrLf
sConsulta = sConsulta & "  SET @SQLSTRING= @SQLSTRING + ' AND OE.NUM=' + CONVERT(VARCHAR,@ORDEN ) " & vbCrLf
sConsulta = sConsulta & "if @REFERENCIA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQLSTRING = @SQLSTRING + N'   AND OE.REFERENCIA = ''' + @REFERENCIA + ''''" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING= @SQLSTRING + ' INNER JOIN PEDIDO ON OE.PEDIDO=PEDIDO.ID '" & vbCrLf
sConsulta = sConsulta & "IF @NUMPEDIDO <>0 " & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND PEDIDO.NUM=' +CONVERT(VARCHAR,@NUMPEDIDO )" & vbCrLf
sConsulta = sConsulta & "IF @APROVISIONADOR <>''" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND PEDIDO.PER=''' + @APROVISIONADOR +''' '" & vbCrLf
sConsulta = sConsulta & "if @ANYO<>0 " & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING=@SQLSTRING + ' AND PEDIDO.ANYO =' + CONVERT(VARCHAR,@ANYO) " & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING=@SQLSTRING + '  LEFT JOIN ORDEN_ENTREGA_ADJUN ON OE.ID=ORDEN_ENTREGA_ADJUN.ORDEN '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING=@SQLSTRING + ' INNER JOIN PROVE ON OE.PROVE=PROVE.COD INNER JOIN PER ON PER.COD=PEDIDO.PER '" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING=@SQLSTRING + ' LEFT JOIN EMP E ON OE.EMPRESA = E.ID" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN (PER R LEFT JOIN UON1 ON R.UON1 = UON1.COD LEFT JOIN UON2 ON R.UON1 = UON2.UON1 AND R.UON2 = UON2.COD LEFT JOIN UON3 ON R.UON1 = UON3.UON1 AND R.UON2 = UON3.UON2 AND R.UON3 = UON3.COD) ON OE.RECEPTOR = R.COD '" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING=@SQLSTRING + '  WHERE NOT EXISTS (SELECT* FROM LINEAS_PEDIDO LP  INNER JOIN SEGURIDAD ON LP.SEGURIDAD=SEGURIDAD.ID AND SEGURIDAD.APROB_TIPO1='''+ @APROBADOR +''' AND LP.NIVEL_APROB=0 AND LP.EST=0 AND LP.ID=LINEAS_PEDIDO.ID)'" & vbCrLf
sConsulta = sConsulta & "IF @CAMPO1 is NOT NULL and @VALORCAMPO1 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQLSTRING = @SQLSTRING + ' AND LINEAS_PEDIDO.CAMPO1 = ' + CONVERT(VARCHAR,@CAMPO1) + N'   AND LINEAS_PEDIDO.VALOR1 = ''' + @VALORCAMPO1 +''''" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CAMPO2 is NOT NULL and @VALORCAMPO2 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQLSTRING = @SQLSTRING + ' AND LINEAS_PEDIDO.CAMPO2 = ' + CONVERT(VARCHAR,@CAMPO2) + N'   AND LINEAS_PEDIDO.VALOR2 = ''' + @VALORCAMPO2 + ''''" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CATN1 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQLSTRING = @SQLSTRING + N'   AND LINEAS_PEDIDO.CAT1 = ' + CONVERT(VARCHAR,@CATN1) " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CATN2 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQLSTRING = @SQLSTRING + N'   AND LINEAS_PEDIDO.CAT2 = ' + CONVERT(VARCHAR,@CATN2) " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CATN3 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQLSTRING = @SQLSTRING + N'   AND LINEAS_PEDIDO.CAT3 = ' + CONVERT(VARCHAR,@CATN3) " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CATN4 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQLSTRING = @SQLSTRING + N'   AND LINEAS_PEDIDO.CAT4 = ' + CONVERT(VARCHAR,@CATN4) " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CATN5 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQLSTRING = @SQLSTRING + N'   AND LINEAS_PEDIDO.CAT5 = ' + CONVERT(VARCHAR,@CATN5) " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @DESDEID <> 0 " & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND OE.ID >=' + CONVERT(VARCHAR,@DESDEID) " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CODPROVE <>''" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND OE.PROVE =''' + @CODPROVE + ''''" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @NUMPEDIDOPROVE <> ''" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND OE.NUMEXT = ''' + @NUMPEDIDOPROVE + ''''" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @DESDEFECHA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND OE.FECHA >=CONVERT(DATETIME,''' + @DESDEFECHA + ''',103) '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if @HASTAFECHA  IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQLSTRING = @SQLSTRING + N'   AND OE.FECHA <= CONVERT(datetime,''' + @HASTAFECHA + ''',103)'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @EMPRESA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQLSTRING = @SQLSTRING + N'   AND OE.EMPRESA=' +CONVERT(VARCHAR,@EMPRESA )" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @RECEPTOR IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQLSTRING = @SQLSTRING + N'   AND OE.RECEPTOR  = ''' + @RECEPTOR + '''' " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @COD_ERP IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQLSTRING = @SQLSTRING + N'   AND OE.COD_PROVE_ERP = ''' +  @COD_ERP + ''''" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' GROUP BY OE.ID, OE.ANYO ,PEDIDO.NUM ,OE.NUM " & vbCrLf
sConsulta = sConsulta & ",OE.REFERENCIA ,OE.PROVE + '' '' + PROVE.DEN ,OE.PROVE ,PROVE.DEN " & vbCrLf
sConsulta = sConsulta & ",OE.FECHA ,OE.EST ,OE.IMPORTE ,PER.APE + '','' + PER.NOM " & vbCrLf
sConsulta = sConsulta & ",OE.OBS, OE.EMPRESA, E.DEN , E.NIF , OE.COD_PROVE_ERP, OE.RECEPTOR ,R.NOM + '' '' + R.APE , R.TFNO , R.EMAIL , ISNULL(R.UON3,ISNULL(R.UON2,R.UON1)) , ISNULL(UON3.DEN,ISNULL(UON2.DEN,UON1.DEN))  '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--PRINT @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "SET ROWCOUNT @MAXLINEAS" & vbCrLf
sConsulta = sConsulta & "EXECUTE sp_executesql @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "SET ROWCOUNT 0" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_DEVOLVER_PROCESOS_PROVE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_DEVOLVER_PROCESOS_PROVE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_DEVOLVER_PROCESOS_PROVE (@PROVE VARCHAR(50),@NORMAL SMALLINT OUTPUT ,@SUBASTA SMALLINT OUTPUT) AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @PISUBASTA TINYINT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @PISUBASTA = (SELECT SUBASTA FROM PARGEN_INTERNO)" & vbCrLf
sConsulta = sConsulta & "SET @NORMAL=(SELECT COUNT(*) " & vbCrLf
sConsulta = sConsulta & "               FROM PROCE " & vbCrLf
sConsulta = sConsulta & "                    INNER JOIN PROCE_PROVE " & vbCrLf
sConsulta = sConsulta & "                            ON PROCE.ANYO= PROCE_PROVE.ANYO " & vbCrLf
sConsulta = sConsulta & "                           AND PROCE.GMN1=PROCE_PROVE.GMN1 " & vbCrLf
sConsulta = sConsulta & "                           AND PROCE.COD=PROCE_PROVE.PROCE " & vbCrLf
sConsulta = sConsulta & "                           AND PROCE_PROVE.PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "                           AND PROCE_PROVE.PUB=1 " & vbCrLf
sConsulta = sConsulta & "                           AND PROCE.EST<=11 " & vbCrLf
sConsulta = sConsulta & "                    INNER JOIN PROCE_DEF PD" & vbCrLf
sConsulta = sConsulta & "                            ON PROCE.ANYO = PD.ANYO" & vbCrLf
sConsulta = sConsulta & "                           AND PROCE.GMN1 = PD.GMN1" & vbCrLf
sConsulta = sConsulta & "                           AND PROCE.COD = PD.PROCE" & vbCrLf
sConsulta = sConsulta & "              WHERE PD.SUBASTA=CASE WHEN @PISUBASTA=1 THEN 0 ELSE PD.SUBASTA END)" & vbCrLf
sConsulta = sConsulta & "IF @PISUBASTA=1 " & vbCrLf
sConsulta = sConsulta & "    SET @SUBASTA=(SELECT COUNT(*) " & vbCrLf
sConsulta = sConsulta & "                FROM PROCE " & vbCrLf
sConsulta = sConsulta & "                     INNER JOIN PROCE_PROVE " & vbCrLf
sConsulta = sConsulta & "                             ON PROCE.ANYO= PROCE_PROVE.ANYO " & vbCrLf
sConsulta = sConsulta & "                            AND PROCE.GMN1=PROCE_PROVE.GMN1 " & vbCrLf
sConsulta = sConsulta & "                            AND PROCE.COD=PROCE_PROVE.PROCE " & vbCrLf
sConsulta = sConsulta & "                            AND PROCE_PROVE.PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "                            AND PROCE_PROVE.PUB=1 " & vbCrLf
sConsulta = sConsulta & "                            AND PROCE.EST<=11 " & vbCrLf
sConsulta = sConsulta & "                     INNER JOIN PROCE_DEF PD" & vbCrLf
sConsulta = sConsulta & "                             ON PROCE.ANYO = PD.ANYO" & vbCrLf
sConsulta = sConsulta & "                            AND PROCE.GMN1 = PD.GMN1" & vbCrLf
sConsulta = sConsulta & "                            AND PROCE.COD = PD.PROCE" & vbCrLf
sConsulta = sConsulta & "               WHERE PD.SUBASTA=1)" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   SET @SUBASTA = 0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "/*Devuelve en 1� lugar los procesos normales y luego los abiertos en modo subasta*/" & vbCrLf
sConsulta = sConsulta & "SELECT PROCE.ANYO,PROCE.GMN1,PROCE.COD,PROCE.DEN,PROCE.FECAPE,PROCE.FECLIMOFE,PROCE_PROVE.OFE,PROCE.EST,PROCE_PROVE.SUPERADA," & vbCrLf
sConsulta = sConsulta & "       PROCE.FECINISUB, PD.SUBASTA,PD.SUBASTAPROVE,PD.SUBASTAPUJAS,PROCE.MON,PROCE_PROVE.NUMOBJ,PROCE_PROVE.OBJNUEVOS , PROCE_PROVE.NO_OFE, PROCE_PROVE.COM, PER.NOM, PER.APE" & vbCrLf
sConsulta = sConsulta & "  FROM PROCE " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PROCE_PROVE " & vbCrLf
sConsulta = sConsulta & "                   ON PROCE.ANYO= PROCE_PROVE.ANYO " & vbCrLf
sConsulta = sConsulta & "                  AND PROCE.GMN1=PROCE_PROVE.GMN1 " & vbCrLf
sConsulta = sConsulta & "                  AND PROCE.COD=PROCE_PROVE.PROCE " & vbCrLf
sConsulta = sConsulta & "                  AND PROCE_PROVE.PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "                  AND PROCE_PROVE.PUB=1 " & vbCrLf
sConsulta = sConsulta & "                  AND PROCE.EST<=11  " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PROCE_DEF PD" & vbCrLf
sConsulta = sConsulta & "                   ON PROCE.ANYO = PD.ANYO" & vbCrLf
sConsulta = sConsulta & "                  AND PROCE.GMN1 = PD.GMN1" & vbCrLf
sConsulta = sConsulta & "                  AND PROCE.COD = PD.PROCE" & vbCrLf
sConsulta = sConsulta & "            LEFT JOIN PER " & vbCrLf
sConsulta = sConsulta & "                   ON PROCE_PROVE.COM = PER.COD" & vbCrLf
sConsulta = sConsulta & "ORDER BY PD.SUBASTA" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_DEVOLVER_OFERTA_PROVE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_DEVOLVER_OFERTA_PROVE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_DEVOLVER_OFERTA_PROVE @ANYO INTEGER,@GMN1 VARCHAR(20),@PROCE INTEGER ,@PROVE VARCHAR(50), @OFE smallint = null AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @LASTOFEENV smallint" & vbCrLf
sConsulta = sConsulta & "DECLARE @LASTOFE smallint" & vbCrLf
sConsulta = sConsulta & "DECLARE @NUMOFEENVIADAS smallint" & vbCrLf
sConsulta = sConsulta & "DECLARE @FECHAULTOFE datetime" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCEEST smallint" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "SELECT @PROCEEST = EST " & vbCrLf
sConsulta = sConsulta & "  FROM PROCE " & vbCrLf
sConsulta = sConsulta & " WHERE ANYO = @ANYO " & vbCrLf
sConsulta = sConsulta & "   AND GMN1 = @GMN1 " & vbCrLf
sConsulta = sConsulta & "   AND COD = @PROCE" & vbCrLf
sConsulta = sConsulta & "SELECT @NUMOFEENVIADAS  = COUNT(OFE)" & vbCrLf
sConsulta = sConsulta & "  FROM PROCE_OFE " & vbCrLf
sConsulta = sConsulta & " WHERE ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "   AND GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "   AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "   AND PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "   AND ENVIADA = 1" & vbCrLf
sConsulta = sConsulta & "SELECT @LASTOFEENV = MAX(OFE)" & vbCrLf
sConsulta = sConsulta & "  FROM PROCE_OFE " & vbCrLf
sConsulta = sConsulta & " WHERE ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "   AND GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "   AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "   AND PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "   AND ENVIADA = 1" & vbCrLf
sConsulta = sConsulta & "select @FECHAULTOFE = FECREC" & vbCrLf
sConsulta = sConsulta & "  FROM PROCE_OFE " & vbCrLf
sConsulta = sConsulta & " WHERE ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "   AND GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "   AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "   AND PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "   AND OFE = @LASTOFEENV" & vbCrLf
sConsulta = sConsulta & "IF @OFE IS NULL" & vbCrLf
sConsulta = sConsulta & "  IF EXISTS (SELECT * " & vbCrLf
sConsulta = sConsulta & "               FROM PROCE_OFE " & vbCrLf
sConsulta = sConsulta & "              WHERE ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "                AND GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "                AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "                AND PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "                AND OFE = -1)" & vbCrLf
sConsulta = sConsulta & "    SET @LASTOFE = -1" & vbCrLf
sConsulta = sConsulta & "  ELSE" & vbCrLf
sConsulta = sConsulta & "    SELECT @LASTOFE = MAX(OFE)" & vbCrLf
sConsulta = sConsulta & "      FROM PROCE_OFE" & vbCrLf
sConsulta = sConsulta & "     WHERE ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "       AND GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "       AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "       AND PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @LASTOFE =@OFE" & vbCrLf
sConsulta = sConsulta & "DECLARE @TOTALADJUNTOS int" & vbCrLf
sConsulta = sConsulta & "SET @TOTALADJUNTOS =(SELECT ISNULL(SUM(DATASIZE) ,0)" & vbCrLf
sConsulta = sConsulta & "                       FROM PROCE_OFE_ADJUN" & vbCrLf
sConsulta = sConsulta & "                      WHERE ANYO  = @ANYO" & vbCrLf
sConsulta = sConsulta & "                        AND GMN1  = @GMN1 " & vbCrLf
sConsulta = sConsulta & "                        AND PROCE = @PROCE " & vbCrLf
sConsulta = sConsulta & "                        AND PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "                        AND OFE   = @LASTOFE)" & vbCrLf
sConsulta = sConsulta & "SET @TOTALADJUNTOS = @TOTALADJUNTOS +" & vbCrLf
sConsulta = sConsulta & "(SELECT ISNULL(SUM(DATASIZE) ,0)" & vbCrLf
sConsulta = sConsulta & "   FROM OFE_GR_ADJUN" & vbCrLf
sConsulta = sConsulta & "  WHERE ANYO  = @ANYO" & vbCrLf
sConsulta = sConsulta & "    AND GMN1  = @GMN1 " & vbCrLf
sConsulta = sConsulta & "    AND PROCE = @PROCE " & vbCrLf
sConsulta = sConsulta & "    AND PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "    AND OFE   = @LASTOFE)" & vbCrLf
sConsulta = sConsulta & "SET @TOTALADJUNTOS = @TOTALADJUNTOS +" & vbCrLf
sConsulta = sConsulta & "(SELECT ISNULL(SUM(DATASIZE) ,0)" & vbCrLf
sConsulta = sConsulta & "   FROM OFE_ITEM_ADJUN" & vbCrLf
sConsulta = sConsulta & "  WHERE ANYO  = @ANYO" & vbCrLf
sConsulta = sConsulta & "    AND GMN1  = @GMN1 " & vbCrLf
sConsulta = sConsulta & "    AND PROCE = @PROCE " & vbCrLf
sConsulta = sConsulta & "    AND PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "    AND OFE   = @LASTOFE)" & vbCrLf
sConsulta = sConsulta & "DECLARE @IMPORTEOFERTA float" & vbCrLf
sConsulta = sConsulta & "SELECT @IMPORTEOFERTA = SUM(ISNULL(IO.PRECIO,0) * ISNULL(I.CANT,0)) " & vbCrLf
sConsulta = sConsulta & "  FROM ITEM_OFE IO" & vbCrLf
sConsulta = sConsulta & "         INNER JOIN ITEM I" & vbCrLf
sConsulta = sConsulta & "                 ON IO.PROCE = I.PROCE" & vbCrLf
sConsulta = sConsulta & "                AND IO.GMN1 = I.GMN1" & vbCrLf
sConsulta = sConsulta & "                AND IO.ANYO = I.ANYO" & vbCrLf
sConsulta = sConsulta & "                AND IO.ITEM =I.ID" & vbCrLf
sConsulta = sConsulta & " WHERE IO.ANYO  = @ANYO" & vbCrLf
sConsulta = sConsulta & "   AND IO.GMN1  = @GMN1 " & vbCrLf
sConsulta = sConsulta & "   AND IO.PROCE = @PROCE " & vbCrLf
sConsulta = sConsulta & "   AND IO.PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "   AND IO.NUM   = @LASTOFE" & vbCrLf
sConsulta = sConsulta & "   AND I.CONF=1" & vbCrLf
sConsulta = sConsulta & "   AND CASE WHEN I.EST IS NULL THEN -1 ELSE I.EST END = CASE WHEN @PROCEEST=11 THEN 0 ELSE -1 END" & vbCrLf
sConsulta = sConsulta & "SELECT PO.ANYO, PO.GMN1, PO.PROCE, PO.PROVE, PO.OFE, PO.EST, PO.FECREC," & vbCrLf
sConsulta = sConsulta & "       PO.FECVAL, PO.MON, MON.DEN MONDEN, PO.CAMBIO, PO.OBS , @LASTOFEENV LASTOFEENV, " & vbCrLf
sConsulta = sConsulta & "       @FECHAULTOFE FECHAULTOFE, @NUMOFEENVIADAS NUMOFEENVIADAS, ENVIADA ESTULTOFE,PO.FECACT, @TOTALADJUNTOS TOTALADJUNTOS, @IMPORTEOFERTA IMPORTEOFERTA, PO.OBSADJUN" & vbCrLf
sConsulta = sConsulta & "  FROM PROCE_OFE PO " & vbCrLf
sConsulta = sConsulta & "    INNER JOIN MON " & vbCrLf
sConsulta = sConsulta & "            ON PO.MON = MON.COD" & vbCrLf
sConsulta = sConsulta & " WHERE PO.ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "   AND PO.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "   AND PO.PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "   AND PO.PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "   AND PO.OFE = @LASTOFE" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ANYA_PROVE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_ANYA_PROVE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_ANYA_PROVE (@COD VARCHAR(100),@DEN VARCHAR(100),@NIF VARCHAR(100),@PAI VARCHAR(50),@PROVI VARCHAR(50),@CP VARCHAR(50),@POB VARCHAR(100),@DIR VARCHAR(255),@MON VARCHAR(50),@FSP_COD VARCHAR(100),@PREM  INT,@URLPROVE VARCHAR(255))  AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @PAIG VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVIG VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @MONG VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @ACTIVA BIT" & vbCrLf
sConsulta = sConsulta & "DECLARE @INT TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @SENTIDO TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ACTIVAR_COD_PROVE_ERP TINYINT" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @PAIG=(SELECT TOP 1 COD FROM PAI WHERE FSP_COD=@PAI)" & vbCrLf
sConsulta = sConsulta & "SET @PROVIG=(SELECT TOP 1 COD FROM PROVI WHERE FSP_PAI=@PAI AND FSP_COD=@PROVI)" & vbCrLf
sConsulta = sConsulta & "SET @MONG=(SELECT TOP 1 COD FROM MON WHERE FSP_COD=@MON)" & vbCrLf
sConsulta = sConsulta & "IF @PREM = 2 " & vbCrLf
sConsulta = sConsulta & "SET @PREM=1" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @PREM=0" & vbCrLf
sConsulta = sConsulta & "/**************************************************************************************************************************************************************************/" & vbCrLf
sConsulta = sConsulta & "/******       integraci�n :  Primero miramos si est� activada : a nivel gral y en concreto la entidad PROVE en sentido salida o entrada/salida   ******/" & vbCrLf
sConsulta = sConsulta & "/******                              grabaremos en la tabla LOG_PROVE (origen = 0 y el usuario a blancos                                                                  ******/" & vbCrLf
sConsulta = sConsulta & "/*********************************************************************************************************************************************************************/" & vbCrLf
sConsulta = sConsulta & "SELECT @INT = INTEGRACION, @ACTIVAR_COD_PROVE_ERP = ACTIVAR_COD_PROVE_ERP FROM PARGEN_INTERNO WHERE ID=1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @INT = 1 " & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & " SELECT @ACTIVA=ACTIVA,@SENTIDO=SENTIDO FROM TABLAS_INTEGRACION WHERE ID=8" & vbCrLf
sConsulta = sConsulta & " IF @ACTIVA = 1 AND (@SENTIDO = 1 OR @SENTIDO=3)" & vbCrLf
sConsulta = sConsulta & "    SET @INT = 1" & vbCrLf
sConsulta = sConsulta & " ELSE" & vbCrLf
sConsulta = sConsulta & "   SET @INT = 0" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "IF ((SELECT COUNT(*) FROM PROVE WHERE COD=@COD)>0)" & vbCrLf
sConsulta = sConsulta & "begin" & vbCrLf
sConsulta = sConsulta & " UPDATE PROVE SET DEN=@DEN,NIF=@NIF,PAI=@PAIG,PROVI=@PROVIG,CP=@CP,POB=@POB,DIR=@DIR,MON=@MONG,FSP_COD=@FSP_COD,PREMIUM=@PREM,ACTIVO=@PREM,URLPROVE=@URLPROVE WHERE COD=@COD" & vbCrLf
sConsulta = sConsulta & " IF @INT = 1" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO LOG_PROVE (ACCION,COD,DEN,NIF,PAI,PROVI,CP,POB,DIR,MON,URLPROVE,ORIGEN,USU) VALUES ('U',@COD,@DEN,@NIF,@PAIG,@PROVIG,@CP,@POB,@DIR,@MONG,@URLPROVE,0,'')" & vbCrLf
sConsulta = sConsulta & "end" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "begin" & vbCrLf
sConsulta = sConsulta & " INSERT INTO PROVE (COD,DEN,NIF,PAI,PROVI,CP,POB,DIR,MON,FSP_COD,PREMIUM,ACTIVO,URLPROVE) VALUES " & vbCrLf
sConsulta = sConsulta & " (@COD,@DEN,@NIF,@PAIG,@PROVIG,@CP,@POB,@DIR,@MONG,@FSP_COD,@PREM,@PREM,@URLPROVE)" & vbCrLf
sConsulta = sConsulta & " if @INT = 1" & vbCrLf
sConsulta = sConsulta & " INSERT INTO LOG_PROVE (ACCION,COD,DEN,NIF,PAI,PROVI,CP,POB,DIR,MON,URLPROVE,ORIGEN,USU) VALUES ('I',@COD,@DEN,@NIF,@PAIG,@PROVIG,@CP,@POB,@DIR,@MONG,@URLPROVE,0,'')" & vbCrLf
sConsulta = sConsulta & "end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ACTIVAR_COD_PROVE_ERP = 1 AND @NIF IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "  INSERT INTO PROVE_EMPRESA_COD_ERP (PROVE, COD_ERP, EMPRESA) SELECT  P.COD,I.COD_ERP, I.EMPRESA FROM PROVE_ERP I INNER JOIN PROVE P ON I.NIF = P.NIF WHERE P.COD=@COD AND I.NIF =@NIF AND NOT EXISTS (SELECT * FROM PROVE_EMPRESA_COD_ERP PECE WHERE P.COD=PECE.PROVE AND I.COD_ERP = PECE.COD_ERP AND I.EMPRESA = PECE.EMPRESA)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "Return" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub


