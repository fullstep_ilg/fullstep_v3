VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CPaises"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Collection" ,"CPais"
Attribute VB_Ext_KEY = "Member0" ,"CPais"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit
''' *** Clase: CPaises
''' *** Creacion: 1/09/1998 (Javier Arana)
''' *** Ultima revision: 14/01/1999 (Alfredo Magallon)

''' Variables que almacenan las propiedades de los objetos

Private mCol As Collection
Private m_bEOF As Boolean
Private m_oConexion As CConexion
Private m_bPortal As Boolean

' Variables para el control de cambios del Log e Integraci�n
''' Control de errores


Private Enum TipoDeError
    ConexionNoEstablecida = 613
End Enum

Friend Property Let Portal(ByVal Data As Boolean)
    
    Let m_bPortal = Data
    
End Property
Public Sub CargarTodosLosPaises(Optional ByVal CaracteresInicialesCod As String, Optional ByVal CaracteresInicialesDen As String, Optional ByVal CoincidenciaTotal As Boolean = False, Optional ByVal OrdenadosPorDen As Boolean = False, Optional ByVal OrdenadosPorMoneda As Boolean = False, Optional ByVal UsarIndice As Boolean = False, Optional ByVal MostrarEstadoIntegracion As Boolean = False, Optional ByVal OrdenadosPorEstadoInt As Boolean = False)

    'ado  Dim rdores As rdoResultset
    Dim rs As ADODB.Recordset
    Dim fldPaCod As ADODB.Field
    Dim fldPaDen As ADODB.Field
    Dim fldPaId As ADODB.Field
    Dim fldMonCod As ADODB.Field
    Dim fldMonDen As ADODB.Field
    Dim fldFecAct As ADODB.Field
    Dim fldMonId As ADODB.Field
    Dim fldEstadoInt As ADODB.Field   'Estado integraci�n
    Dim sConsulta As String
    Dim lIndice As Long
    Dim sFSP As String
    Dim v_Estado As Variant
    
    
    ''' * Objetivo: Cargar la coleccion de paises
    ''' *           Cuando se trabaja con integraci�n, desde el mantenimiento se visualiza el estado de integraci�n
    ''' *           en el sistema receptor de las modificaciones originadas en FSGS.
    ''' *           Buscamos por coincidencia de c�digo (COD) cuando la acci�n es distinta a un cambio de c�digo (C)
    ''' *           y por coincidencia en c�digo nuevo (COD_NEW) cuando la acci�n es un cambio de c�digo
   
    ''' Precondicion
    
    If m_oConexion Is Nothing Then
        Err.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CDivisiones.CargarTodasLasdivisiones", "No se ha establecido la conexion"
        Exit Sub
    End If
    
    ''' Generacion del SQL a partir de los parametros
    
    
    sConsulta = "SELECT PAI.COD AS PAICOD,PAI.DEN AS PAIDEN,PAI.MON AS PAIMON,PAI.FECACT AS PAIFECACT,MON.COD AS MONCOD, MON.DEN AS MONDEN FROM PAI LEFT JOIN MON ON PAI.MON=MON.COD"

    
    If CaracteresInicialesCod = "" And CaracteresInicialesDen = "" Then
            
    Else
        If Not (CaracteresInicialesCod = "") And Not (CaracteresInicialesDen = "") Then
            
            If CoincidenciaTotal Then
                
                If m_bPortal Then
                    sConsulta = sConsulta & " WHERE PAI.COD ='" & DblQuote(CaracteresInicialesCod) & "'"
                    sConsulta = sConsulta & " AND PAI.DEN='" & DblQuote(CaracteresInicialesDen) & "'"
                Else
                    sConsulta = sConsulta & " WHERE PAI.COD ='" & DblQuote(CaracteresInicialesCod) & "'"
                    sConsulta = sConsulta & " AND PAI.DEN='" & DblQuote(CaracteresInicialesDen) & "'"
                End If
            Else
                If m_bPortal Then
                    sConsulta = sConsulta & " WHERE PAI.COD LIKE '" & DblQuote(CaracteresInicialesCod) & "%'"
                    sConsulta = sConsulta & " PAI.DEN LIKE '" & DblQuote(CaracteresInicialesDen) & "%'"
                Else
                    sConsulta = sConsulta & " WHERE PAI.COD LIKE '" & DblQuote(CaracteresInicialesCod) & "%'"
                    sConsulta = sConsulta & " PAI.DEN LIKE '" & DblQuote(CaracteresInicialesDen) & "%'"
                End If
            End If
                        
        Else
            
            If Not (CaracteresInicialesCod = "") Then
                
                If CoincidenciaTotal Then
                
                    If m_bPortal Then
                        sConsulta = sConsulta & " WHERE PAI.COD ='" & DblQuote(CaracteresInicialesCod) & "'"
                    Else
                        sConsulta = sConsulta & " WHERE PAI.COD ='" & DblQuote(CaracteresInicialesCod) & "'"
                    End If
                Else
                    
                    If m_bPortal Then
                        sConsulta = sConsulta & " WHERE PAI.COD LIKE '" & DblQuote(CaracteresInicialesCod) & "%'"
                    Else
                        sConsulta = sConsulta & " WHERE PAI.COD LIKE '" & DblQuote(CaracteresInicialesCod) & "%'"
                    End If
                End If
                
            Else
                
                If CoincidenciaTotal Then
                
                    If m_bPortal Then
                        sConsulta = sConsulta & " WHERE PAI.DEN='" & DblQuote(CaracteresInicialesDen) & "'"
                    Else
                        sConsulta = sConsulta & " WHERE PAI.DEN='" & DblQuote(CaracteresInicialesDen) & "'"
                    End If
                    
                Else
                    
                    If m_bPortal Then
                        sConsulta = sConsulta & " WHERE PAI.DEN LIKE '" & DblQuote(CaracteresInicialesDen) & "%'"
                    
                    Else
                    
                       sConsulta = sConsulta & " WHERE PAI.DEN LIKE '" & DblQuote(CaracteresInicialesDen) & "%'"
                    
                    End If
                    
                End If
                
            End If
        
        End If
               
    End If
    
    If MostrarEstadoIntegracion Then
        sConsulta = sConsulta & " GROUP BY PAI.COD,PAI.DEN,PAI.MON,PAI.FECACT,MON.COD,MON.DEN "
    End If
    
    If OrdenadosPorDen Then
        sConsulta = sConsulta & " ORDER BY PAIDEN"
    Else
        If OrdenadosPorMoneda Then
            sConsulta = sConsulta & " ORDER BY MONCOD"
        Else
            If OrdenadosPorEstadoInt Then
                sConsulta = sConsulta & " ORDER BY ESTADO"
            Else
                sConsulta = sConsulta & " ORDER BY PAICOD"
            End If
        End If
    End If
          
    ''' Creacion del Resultset
    
    Set rs = New ADODB.Recordset
    rs.Open sConsulta, m_oConexion.adoCon, adOpenForwardOnly, adLockReadOnly
    
    If rs.eof Then
            
        rs.Close
        Set rs = Nothing
        Set mCol = Nothing
        Set mCol = New Collection
        Exit Sub
          
    Else
        
        ''' Carga del Resultset en la coleccion
        
        Set mCol = Nothing
        Set mCol = New Collection
        
        If m_bPortal Then
            Set fldPaId = rs.Fields("ID")   '0
            Set fldPaCod = rs.Fields("PAICOD") '1
            Set fldPaDen = rs.Fields("PAIDEN") '2
            Set fldMonCod = rs.Fields("MONCOD")   '4
            Set fldMonDen = rs.Fields("MONDEN")   '5
            Set fldMonId = rs.Fields("MONID")  '7
        
            If UsarIndice Then
                
                lIndice = 0
            
                While Not rs.eof
                
                    Me.Add fldPaCod.Value, fldPaDen.Value, NullToStr(fldMonCod.Value), NullToStr(fldMonDen.Value), lIndice, fldPaId.Value, fldMonId.Value
                    rs.MoveNext
                    lIndice = lIndice + 1
                 
                Wend
                
            Else
            
                While Not rs.eof
                
                    Me.Add fldPaCod.Value, fldPaDen.Value, NullToStr(fldMonCod.Value), NullToStr(fldMonDen.Value), , fldPaId.Value, NullToDbl0(fldMonId.Value)
                    rs.MoveNext
                    
                Wend
                
            End If
            Set fldPaId = Nothing
            Set fldPaCod = Nothing
            Set fldPaDen = Nothing
            Set fldMonCod = Nothing
            Set fldMonDen = Nothing
            Set fldMonId = Nothing
            
        Else
            Set fldPaCod = rs.Fields("PAICOD") '1
            Set fldPaDen = rs.Fields("PAIDEN") '2
            Set fldMonCod = rs.Fields("MONCOD")   '4
            Set fldMonDen = rs.Fields("MONDEN")   '5
            Set fldFecAct = rs.Fields("PAIFECACT")
            If MostrarEstadoIntegracion Then
                Set fldEstadoInt = rs.Fields("ESTADO")
            End If

            If UsarIndice Then
                
                lIndice = 0
            
                While Not rs.eof
                    If MostrarEstadoIntegracion Then
                        v_Estado = fldEstadoInt.Value
                    Else
                        v_Estado = Null
                    End If

                    Me.Add fldPaCod.Value, fldPaDen.Value, NullToStr(fldMonCod.Value), NullToStr(fldMonDen.Value), lIndice, , , fldFecAct.Value, v_Estado
                    
                    rs.MoveNext
                    lIndice = lIndice + 1
                    
                Wend
                
            Else
            
                While Not rs.eof
                    If MostrarEstadoIntegracion Then
                        v_Estado = fldEstadoInt.Value
                    Else
                        v_Estado = Null
                    End If

                    Me.Add fldPaCod.Value, fldPaDen.Value, NullToStr(fldMonCod.Value), NullToStr(fldMonDen.Value), , , , fldFecAct, v_Estado
                    
                    rs.MoveNext
                    
                Wend
                
            End If
            Set fldPaCod = Nothing
            Set fldPaDen = Nothing
            Set fldMonCod = Nothing
            Set fldMonDen = Nothing
            Set fldFecAct = Nothing
            Set fldEstadoInt = Nothing
          
        End If
        
        rs.Close
        Set rs = Nothing
    
        
    End If
    
End Sub
Public Function Add(ByVal Cod As String, ByVal Den As String, Optional ByVal MonCod As Variant, Optional ByVal MonDen As Variant, Optional ByVal varIndice As Variant, Optional ByVal PaiId As Integer, Optional ByVal MonId As Integer, Optional ByVal FecAct As Variant, Optional ByVal varEstado As Variant) As CPais
    
    ''' * Objetivo: Anyadir un pais a la coleccion
    ''' * Recibe: Datos del pais
    ''' * Devuelve: Pais anyadido
        
    Dim oPais As CPais
    
    ''' Creacion de objeto pais
    
    Set oPais = New CPais
    
    ''' Paso de los parametros al nuevo objeto pais
    
    oPais.Cod = Cod
    oPais.Den = Den
    If Not IsMissing(FecAct) Then
        oPais.FecAct = FecAct
    End If
    oPais.EstadoIntegracion = varEstado 'Estado integracion
    
    If Not IsMissing(PaiId) Then
        oPais.ID = PaiId
    End If
    
    
    Set oPais.Conexion = m_oConexion
    oPais.Portal = m_bPortal
    
    
    ''' Anyadir el objeto pais a la coleccion
    ''' Si no se especifica indice, se anyade al final
    
    If Not IsMissing(varIndice) And Not IsNull(varIndice) Then
    
        oPais.Indice = varIndice
        mCol.Add oPais, CStr(varIndice)
        
    Else
        mCol.Add oPais, Cod
    End If
    
    Set Add = oPais
    
    Set oPais = Nothing

End Function
Public Property Get Item(vntIndexKey As Variant) As CPais

    ''' * Objetivo: Recuperar un pais de la coleccion
    ''' * Recibe: Indice del pais a recuperar
    ''' * Devuelve: Pais correspondiente
    
On Error GoTo NoSeEncuentra:

    ''' Devolvemos el item de la coleccion privada
    
    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:

    Set Item = Nothing
    
End Property

Friend Property Set Conexion(ByVal con As CConexion)

    ''' * Objetivo: Dar valor a la variable privada Conexion
    ''' * Recibe: Conexion
    ''' * Devuelve: Nada
    
    Set m_oConexion = con
    
End Property
Friend Property Get Conexion() As CConexion

    ''' * Objetivo: Devolver variable privada Conexion
    ''' * Recibe: Nada
    ''' * Devuelve: Conexion
    
    Set Conexion = m_oConexion
    
End Property


Public Property Get Count() As Long

    ''' * Objetivo: Devolver variable privada Count
    ''' * Recibe: Nada
    ''' * Devuelve: Numero de elementos de la coleccion

    If mCol Is Nothing Then
        Count = 0
    Else
        Count = mCol.Count
    End If

End Property

Public Property Get eof() As Boolean

    ''' * Objetivo: Devolver variable privada EOF
    ''' * Recibe: Nada
    ''' * Devuelve: Variable privada EOF
    
    eof = m_bEOF
    
End Property
Public Sub Remove(vntIndexKey As Variant)
   
    ''' * Objetivo: Eliminar una moneda de la coleccion
    ''' * Recibe: Indice de la moneda a eliminar
    ''' * Devuelve: Nada
    
    mCol.Remove vntIndexKey
    
End Sub
Private Sub Class_Initialize()

    ''' * Objetivo: Crear la coleccion
    
    '*-*'
    
    Set mCol = New Collection
    
End Sub
Private Sub Class_Terminate()

    ''' * Objetivo: Limpiar memoria
    
    Set mCol = Nothing
    Set m_oConexion = Nothing

End Sub
Public Function DevolverPais(ByVal Cod As String) As CPais

    ''' ! Pendiente de revision, segun tema de combos, etc.
    
    Dim oPais As CPais
    Dim iIndice As Integer
    On Error Resume Next
    
    For iIndice = 1 To mCol.Count
        
        Set oPais = mCol.Item(iIndice)
        If oPais.Cod = Cod Then
            Set DevolverPais = oPais
            Set oPais = Nothing
            Exit Function
        End If
    
    Next
    
    Set oPais = Nothing
    Set DevolverPais = Nothing

End Function
Public Sub CargarTodosLosPaisesDesde(ByVal NumMaximo As Integer, Optional ByVal CaracteresInicialesCod As String, Optional ByVal CaracteresInicialesDen As String, Optional ByVal OrdenadosPorDen As Boolean, Optional ByVal UsarIndice As Boolean)

    ''' * Objetivo: Cargar los paises hasta un numero maximo
    ''' * Objetivo: desde un codigo o una denominacion determinadas
    
    'ado  Dim rdores As rdoResultset
    Dim rs As ADODB.Recordset
    Dim fldPaCod As ADODB.Field
    Dim fldPaDen As ADODB.Field
    Dim fldPaId As ADODB.Field
    Dim fldMonCod As ADODB.Field
    Dim fldMonDen As ADODB.Field
    Dim fldMonId As ADODB.Field
    Dim sConsulta As String
    Dim lIndice As Long
    Dim sFSP As String
    
    ''' Precondicion
    
    If m_oConexion Is Nothing Then
    
        Err.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CDestinos.CargarTodosLosDestinos", "No se ha establecido la conexion"
        Exit Sub
        
    End If
    
   
   
    ''' Generacion del SQL a partir de los parametros
           
    If CaracteresInicialesCod = "" And CaracteresInicialesDen = "" Then
        sConsulta = "SELECT TOP " & NumMaximo & " PAI.COD AS PAICOD,PAI.DEN AS PAIDEN,PAI.MON AS PAIMON,PAI.FECACT AS PAIFECACT,MON.COD AS MONCOD, MON.DEN AS MONDEN,MON.FECACT AS MONFECACT FROM PAI LEFT JOIN MON ON PAI.MON=MON.COD"
              
    Else
        
        sConsulta = "SELECT TOP " & NumMaximo & " PAI.COD AS PAICOD,PAI.DEN AS PAIDEN,PAI.MON AS PAIMON,PAI.FECACT AS PAIFECACT,MON.COD AS MONCOD, MON.DEN AS MONDEN,MON.FECACT AS MONFECACT FROM PAI LEFT JOIN MON ON PAI.MON=MON.COD WHERE"
        
        If Not (CaracteresInicialesCod = "") And Not (CaracteresInicialesDen = "") Then
                
            sConsulta = sConsulta & " PAI.COD >='" & DblQuote(CaracteresInicialesCod) & "'"
            sConsulta = sConsulta & " AND PAI.DEN >='" & DblQuote(CaracteresInicialesDen) & "'"
                    
        Else
            
            If Not (CaracteresInicialesCod = "") Then
                sConsulta = sConsulta & " PAI.COD >='" & DblQuote(CaracteresInicialesCod) & "'"
            Else
                sConsulta = sConsulta & " PAI.DEN >='" & DblQuote(CaracteresInicialesDen) & "'"
            End If
        
        End If
               
    End If
    
    If OrdenadosPorDen Then
        sConsulta = sConsulta & " ORDER BY PAIDEN"
    Else
        sConsulta = sConsulta & " ORDER BY PAICOD"
    End If
          
    ''' Crear el Recordset
    
    'ado  Set rdores = m_oConexion.rdoSummitCon.OpenResultset(sConsulta, rdOpenForwardOnly, rdConcurReadOnly, rdExecDirect)
    Set rs = New ADODB.Recordset
    rs.Open sConsulta, m_oConexion.adoCon, adOpenForwardOnly, adLockReadOnly
    
    If rs.eof Then
            
        rs.Close
        Set rs = Nothing
        Set mCol = Nothing
        Set mCol = New Collection
        
        Exit Sub
          
    Else
        
        ''' Llenar la coleccion a partir del Resultset
        ''' teniendo en cuenta el maximo
        
        Set mCol = Nothing
        Set mCol = New Collection
         
        If m_bPortal Then
            Set fldPaId = rs.Fields("ID")   '0
            Set fldPaCod = rs.Fields("PAICOD") '1
            Set fldPaDen = rs.Fields("PAIDEN") '2
            Set fldMonCod = rs.Fields("MONCOD")   '4
            Set fldMonDen = rs.Fields("MONDEN")   '5
            Set fldMonId = rs.Fields("MONID")  '7
            
            If UsarIndice Then
                
                lIndice = 0
                        
                While Not rs.eof
                    'ado  Me.Add rdores("PAICOD"), rdores("PAIDEN"), rdores("MONCOD"), rdores("MONDEN"), lIndice, rdores("ID"), rdores("MONID")
                    Me.Add fldPaCod.Value, fldPaDen.Value, fldMonCod.Value, fldMonDen.Value, lIndice, fldPaId.Value, fldMonId.Value
                    rs.MoveNext
                    lIndice = lIndice + 1
                Wend
                
                If Not rs.eof Then
                    m_bEOF = False
                Else
                    m_bEOF = True
                End If
        
            Else
                                
                While Not rs.eof
                    'ado  Me.Add rdores("PAICOD"), rdores("PAIDEN"), NullToStr(rdores("MONCOD")), NullToStr(rdores("MONDEN")), , rdores("ID"), NullToDbl0(rdores("MONID"))
                    Me.Add fldPaCod.Value, fldPaDen.Value, NullToStr(fldMonCod.Value), NullToStr(fldMonDen.Value), , fldPaId.Value, NullToDbl0(fldMonId.Value)
                    rs.MoveNext
                Wend
                
                If Not rs.eof Then
                    m_bEOF = False
                Else
                    m_bEOF = True
                End If
                
            End If
        Else
            Set fldPaCod = rs.Fields("PAICOD") '1
            Set fldPaDen = rs.Fields("PAIDEN") '2
            Set fldMonCod = rs.Fields("MONCOD")   '4
            Set fldMonDen = rs.Fields("MONDEN")   '5
            If UsarIndice Then
                
                lIndice = 0
                        
                While Not rs.eof
                    'ado  Me.Add rdores("PAICOD"), rdores("PAIDEN"), rdores("MONCOD"), rdores("MONDEN"), lIndice
                    Me.Add fldPaCod.Value, fldPaDen.Value, fldMonCod.Value, fldMonDen.Value, lIndice
                    rs.MoveNext
                    lIndice = lIndice + 1
                Wend
                
                If Not rs.eof Then
                    m_bEOF = False
                Else
                    m_bEOF = True
                End If
        
            Else
                                
                While Not rs.eof
                    'ado  Me.Add rdores("PAICOD"), rdores("PAIDEN"), NullToStr(rdores("MONCOD")), NullToStr(rdores("MONDEN"))
                    Me.Add fldPaCod.Value, fldPaDen.Value, NullToStr(fldMonCod.Value), NullToStr(fldMonDen.Value)
                    rs.MoveNext
                    
                Wend
                
                If Not rs.eof Then
                    m_bEOF = False
                Else
                    m_bEOF = True
                End If
                
            End If
        
        End If
        rs.Close
        Set rs = Nothing
        Set fldPaId = Nothing
        Set fldPaCod = Nothing
        Set fldPaDen = Nothing
        Set fldMonCod = Nothing
        Set fldMonDen = Nothing
        Set fldMonId = Nothing

    End If
    
End Sub
Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"

    ''' ! Pendiente de revision, objetivo desconocido
    
    Set NewEnum = mCol.[_NewEnum]

End Property

