VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CUnidadOrgNivel2"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
'********************* CUnidadOrgNivel2 **********************************
'*             Autor : Javier Arana
'*             Creada : 28/7/98
'****************************************************************

Option Explicit


Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
   
End Enum

Private m_oConexion As CConexion
Private mvarIndice As Long
Private m_sCod As String 'local copy
Private mvarDen As String 'local copy
Private m_sCodUnidadOrgNivel1 As String
Private mvarUnidadesOrgNivel3 As CUnidadesOrgNivel3
Private mvarRecordset As ADODB.Recordset
Private mvarbPresupuestos As Boolean
Private m_bPresAsig As Boolean

Private m_vidEmpresa As Variant
Private m_oEmpresa As CEmpresa
Private m_vFecAct As Variant
'*************** Propiedades de la clase ******

Friend Property Set Conexion(ByVal con As CConexion)
Set m_oConexion = con
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = m_oConexion
End Property


Public Property Get IdEmpresa() As Variant
    IdEmpresa = m_vidEmpresa
End Property

Public Property Let IdEmpresa(ByVal vEmpr As Variant)
    m_vidEmpresa = vEmpr
End Property
Public Property Set Empresa(ByVal oEmp As CEmpresa)
Set m_oEmpresa = oEmp
End Property
Public Property Get Empresa() As CEmpresa
Set Empresa = m_oEmpresa
End Property
Public Property Get FecAct() As Variant
    FecAct = m_vFecAct
End Property

Public Property Let FecAct(ByVal vFecAct As Variant)
    m_vFecAct = vFecAct
End Property


Public Property Let ConPresup(ByVal vData As Boolean)
    mvarbPresupuestos = vData
End Property

Public Property Get ConPresup() As Boolean
    ConPresup = mvarbPresupuestos
End Property

Public Property Let PresAsig(ByVal bAsig As Boolean)
    m_bPresAsig = bAsig
End Property

Public Property Get PresAsig() As Boolean
    PresAsig = m_bPresAsig
End Property


Public Property Get Indice() As Long
    Indice = mvarIndice

End Property

Public Property Let Indice(ByVal lInd As Long)
    mvarIndice = lInd
End Property
'Public Property Set UnidadesOrgNivel3(ByVal vData As CUnidadesOrgNivel3)
'    Set mvarUnidadesOrgNivel3 = vData
'End Property


'Public Property Get UnidadesOrgNivel3() As CUnidadesOrgNivel3
'        Set UnidadesOrgNivel3 = mvarUnidadesOrgNivel3
'End Property
Public Property Let CodUnidadOrgNivel1(ByVal vCod As String)
    Let m_sCodUnidadOrgNivel1 = vCod
End Property
Public Property Get CodUnidadOrgNivel1() As String
    CodUnidadOrgNivel1 = m_sCodUnidadOrgNivel1
End Property
Public Property Let Den(ByVal vData As String)
    mvarDen = vData
End Property


Public Property Get Den() As String
    Den = mvarDen
End Property



Public Property Let Cod(ByVal vData As String)
    m_sCod = vData
End Property


Public Property Get Cod() As String
    Cod = m_sCod
End Property



Public Sub CargarTodasLasUnidadesOrgNivel3(Optional ByVal CodPersona As Variant, Optional ByVal CarIniCod As String, Optional ByVal CarIniDen As String, Optional ByVal CoincidenciaTotal As Boolean, Optional ByVal OrdenadasPorDen As Boolean = False, Optional ByVal UsarIndice As Boolean = False)

Dim rs As New ADODB.Recordset
Dim sConsulta As String
Dim sCad As String
Dim lIndice As Long
Dim fldUon1Cod As ADODB.Field
Dim fldUon2Cod As ADODB.Field
Dim fldUon3Cod As ADODB.Field
Dim fldUON3Den As ADODB.Field

'********* Precondicion **************************************
If m_oConexion Is Nothing Then
    Err.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CGrupoDevolverTodasLasUnidadesOrgNivel1", "No se ha establecido la conexion"
    Exit Sub
End If
'*************************************************************

If CarIniCod <> "" Then
    sCad = "1"
Else
    sCad = "0"
End If

If CarIniDen <> "" Then
    sCad = sCad & "1"
Else
    sCad = sCad & "0"
End If

If Not IsMissing(CodPersona) And Not IsNull(CodPersona) Then
    sConsulta = "SELECT UON2 as UON2COD ,UON1 as UON1COD ,UON3.COD AS UON3COD, UON3.DEN as UON3DEN FROM UON3 JOIN PER ON UON3.COD=PER.UON3 WHERE UON3.UON2='" & DblQuote(m_sCod) & "' AND PER.COD='" & DblQuote(CodPersona) & "'"
Else
    sConsulta = "SELECT UON2 as UON2COD ,UON1 as UON1COD ,UON3.COD AS UON3COD, UON3.DEN as UON3DEN FROM UON3 WHERE UON3.UON2='" & DblQuote(m_sCod) & "'"
End If

    Select Case sCad
            
    Case "01"
            
            If CoincidenciaTotal Then
                sConsulta = sConsulta & "AND UON3DEN = '" & DblQuote(CarIniDen) & "'"
            Else
                sConsulta = sConsulta & "AND UON3DEN LIKE '" & DblQuote(CarIniDen) & "%'"
            End If
                      
    Case "10"
            If CoincidenciaTotal Then
                sConsulta = sConsulta & "AND UON3COD = '" & DblQuote(CarIniCod) & "'"
            Else
                sConsulta = sConsulta & "AND UON3COD LIKE '" & DblQuote(CarIniCod) & "%'"
            End If
                   
    Case "11"
            If CoincidenciaTotal Then
                sConsulta = sConsulta & "AND UON3COD = '" & DblQuote(CarIniCod) & "'"
                sConsulta = sConsulta & "AND UON3DEN = '" & DblQuote(CarIniDen) & "'"
            Else
                sConsulta = sConsulta & "AND UON3COD LIKE '" & DblQuote(CarIniCod) & "%'"
                sConsulta = sConsulta & "AND UON3DEN LIKE '" & DblQuote(CarIniDen) & "%'"
            End If
    End Select

    
If OrdenadasPorDen Then
    sConsulta = sConsulta & " ORDER BY UON3DEN"
Else
    sConsulta = sConsulta & " ORDER BY UON3COD"
End If

rs.Open sConsulta, m_oConexion.adoCon, adOpenForwardOnly, adLockReadOnly
      
If rs.eof Then
        
    rs.Close
    Set rs = Nothing
    Set mvarUnidadesOrgNivel3 = Nothing
    Set mvarUnidadesOrgNivel3 = New CUnidadesOrgNivel3
    Set mvarUnidadesOrgNivel3.Conexion = m_oConexion
    Exit Sub
      
Else
    
    Set mvarUnidadesOrgNivel3 = Nothing
    Set mvarUnidadesOrgNivel3 = New CUnidadesOrgNivel3
    Set mvarUnidadesOrgNivel3.Conexion = m_oConexion
     
    Set fldUon1Cod = rs.Fields("UON1COD")
    Set fldUon2Cod = rs.Fields("UON2COD")
    Set fldUon3Cod = rs.Fields("UON3COD")
    Set fldUON3Den = rs.Fields("UON3DEN")
    
    If UsarIndice Then
        lIndice = 0
        While Not rs.eof
            mvarUnidadesOrgNivel3.Add fldUon3Cod.Value, fldUON3Den.Value, fldUon1Cod.Value, fldUon2Cod.Value, lIndice
            rs.MoveNext
            lIndice = lIndice + 1
        Wend
    Else
        While Not rs.eof
            mvarUnidadesOrgNivel3.Add fldUon3Cod.Value, fldUON3Den.Value, fldUon1Cod.Value, fldUon2Cod.Value
            rs.MoveNext
        Wend
    End If
    
    rs.Close
    Set rs = Nothing
    Set fldUon1Cod = Nothing
    Set fldUon2Cod = Nothing
    Set fldUon3Cod = Nothing
    Set fldUON3Den = Nothing
    Exit Sub
      
End If
        
End Sub


Private Sub Class_Terminate()

'Set mvarUnidadesOrgNivel3 = Nothing
Set m_oConexion = Nothing

End Sub



Public Function CargarEmpresa() As CEmpresa
Dim sConsulta As String
Dim adoRS As ADODB.Recordset

sConsulta = "SELECT E.* FROM EMP E INNER JOIN UON2 U2 ON E.ID=U2.EMPRESA AND U2.UON1 = '" & DblQuote(m_sCodUnidadOrgNivel1) & "' AND U2.COD='" & DblQuote(m_sCod) & "'"
Set adoRS = New ADODB.Recordset
adoRS.Open sConsulta, m_oConexion.adoCon, adOpenForwardOnly
If adoRS.eof Then
    m_vidEmpresa = Null
    adoRS.Close
    Set m_oEmpresa = Nothing
Else
    Set m_oEmpresa = New CEmpresa
    
    With m_oEmpresa
        Set .Conexion = m_oConexion
        
        .ID = adoRS.Fields("ID").Value
        m_vidEmpresa = .ID
        .Den = adoRS.Fields("DEN").Value
        .Nif = adoRS.Fields("NIF").Value
        .CP = adoRS.Fields("CP").Value
        .CodPais = adoRS.Fields("PAI").Value
        .CodProvi = adoRS.Fields("PROVI").Value
        .Direccion = adoRS.Fields("DIR").Value
        .Poblacion = adoRS.Fields("POB").Value
        .ERP = (adoRS.Fields("ERP").Value = 1)
    End With
    adoRS.Close
    
End If
Set adoRS = Nothing

Set CargarEmpresa = m_oEmpresa
End Function

Public Function ImposibleEliminar() As Boolean

Dim sConsulta As String
Dim adoRS As ADODB.Recordset


sConsulta = "if (select erp from emp where id = " & m_vidEmpresa & ") = 1" & vbCrLf
sConsulta = sConsulta & "    SELECT COUNT(*) NUM FROM (" & vbCrLf
sConsulta = sConsulta & "SELECT EMPRESA FROM UON1 WHERE EMPRESA = " & m_vidEmpresa & "" & vbCrLf
sConsulta = sConsulta & "UNION SELECT EMPRESA FROM UON2 WHERE EMPRESA = " & m_vidEmpresa & " AND UON1 <>'" & DblQuote(m_sCodUnidadOrgNivel1) & "' AND COD <>'" & DblQuote(m_sCod) & "'" & vbCrLf
sConsulta = sConsulta & "UNION SELECT EMPRESA FROM UON3 WHERE EMPRESA = " & m_vidEmpresa & " ) u" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SELECT 0 NUM" & vbCrLf

ImposibleEliminar = False
Set adoRS = New ADODB.Recordset
adoRS.Open sConsulta, m_oConexion.adoCon, adOpenForwardOnly

If adoRS.eof Then
    adoRS.Close
Else
    If adoRS.Fields("NUM").Value > 0 Then
        ImposibleEliminar = True
    End If
    adoRS.Close
End If
Set adoRS = Nothing

End Function

Public Function FinalizarEdicionModificando() As Boolean

Dim sConsulta As String
Dim adoRS As ADODB.Recordset
m_oConexion.adoCon.Execute "BEGIN TRANSACTION"
m_oConexion.adoCon.Execute "SET XACT_ABORT OFF"
With m_oEmpresa
    
    sConsulta = "INSERT INTO EMP (NIF, DEN, DIR, CP,POB, PAI, PROVI, ERP) VALUES ('" & DblQuote(.Nif) & "','" & DblQuote(.Den) & "','" & DblQuote(.Direccion) & "'," _
              & StrToSQLNULL(.CP) & "," & StrToSQLNULL(.Poblacion) & "," & StrToSQLNULL(.CodPais) & "," & StrToSQLNULL(.CodProvi) & "," & BooleanToSQLBinary(.ERP) & ")"
    m_oConexion.adoCon.Execute sConsulta
    Set adoRS = New ADODB.Recordset
    sConsulta = "SELECT MAX(ID) ID FROM EMP"
    adoRS.Open sConsulta, m_oConexion.adoCon, adOpenForwardOnly
    .ID = adoRS.Fields("ID").Value
    adoRS.Close
    Set adoRS = Nothing

    sConsulta = "UPDATE UON2 SET EMPRESA = " & m_oEmpresa.ID & " WHERE UON1= '" & DblQuote(m_sCodUnidadOrgNivel1) & "' AND COD = '" & DblQuote(m_sCod) & "'"
    m_oConexion.adoCon.Execute sConsulta
    sConsulta = "UPDATE ORDEN_ENTREGA SET EMPRESA = " & m_oEmpresa.ID & " WHERE EMPRESA IS NULL"
    m_oConexion.adoCon.Execute sConsulta
End With

m_oConexion.adoCon.Execute "COMMIT TRANSACTION"


FinalizarEdicionModificando = True
End Function


