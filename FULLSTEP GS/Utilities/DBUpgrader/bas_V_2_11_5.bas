Attribute VB_Name = "bas_V_2_11_5"
Option Explicit
Dim oDMOBD As SQLDMO.Database
Dim miBugMS As Integer

Public Function CodigoDeActualizacion2_11_00_06A2_11_50_00() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim bBool As Boolean

On Error GoTo error
    

sConsulta = "BEGIN TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = True
ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

frmProgreso.lblDetalle = "Creamos tablas"
frmProgreso.lblDetalle.Refresh
frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_11_5_Tablas

frmProgreso.lblDetalle = "Modificamos tablas"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_11_5_TablasModif

V_2_11_5_Triggers

frmProgreso.lblDetalle = "Modificamos stored procedures"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_11_5_Storeds

sConsulta = "UPDATE VERSION SET NUM ='2.11.50.00'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "COMMIT TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = False

CodigoDeActualizacion2_11_00_06A2_11_50_00 = True
Exit Function
    
error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_11_00_06A2_11_50_00 = False

End Function



Private Sub V_2_11_5_Tablas()
Dim sConsulta As String


sConsulta = "CREATE TABLE [dbo].[CONF_VISTA_INICIAL_PROCE] (" & vbCrLf
sConsulta = sConsulta & "   [USU] [varchar] (" & gLongitudesDeCodigos.giLongCodUSU & ") NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [CONSUMIDO_POS] [smallint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [CONSUMIDO_VISIBLE] [tinyint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [CONSUMIDO_WIDTH] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [ADJ_POS] [smallint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [ADJ_VISIBLE] [tinyint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [ADJ_WIDTH] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [AHORR_ADJ_POS] [smallint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [AHORR_ADJ_VISIBLE] [tinyint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [AHORR_ADJ_WIDTH] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [AHORR_ADJ_PORCEN_POS] [smallint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [AHORR_ADJ_PORCEN_VISIBLE] [tinyint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [AHORR_ADJ_PORCEN_WIDTH] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [IMP_POS] [smallint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [IMP_VISIBLE] [tinyint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [IMP_WIDTH] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [AHORR_OFE_POS] [smallint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [AHORR_OFE_VISIBLE] [tinyint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [AHORR_OFE_WIDTH] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [AHORR_OFE_PORCEN_POS] [smallint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [AHORR_OFE_PORCEN_VISIBLE] [tinyint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [AHORR_OFE_PORCEN_WIDTH] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [OCULTAR_GR_CERRADOS] [tinyint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [EXCLUIR_GR_CERRADOS_RESULT] [tinyint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [TIPOVISION] [tinyint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [OCULTAR_NOADJ] [tinyint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [OCULTAR_PROV_SIN_OFE] [tinyint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [GRUPO_WIDTH] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [ANCHO_FILA] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [GRUPO0_WIDTH] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [COD_WIDTH] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [DEN_WIDTH] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [DEC_RESULT] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [DEC_PORCEN] [int] NOT NULL " & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[CONF_VISTA_INICIAL_PROCE] WITH NOCHECK ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [PK_CONF_VISTA_INICIAL_PROCE] PRIMARY KEY  CLUSTERED " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [USU]" & vbCrLf
sConsulta = sConsulta & "   )  ON [PRIMARY] " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[CONF_VISTA_INICIAL_PROCE] WITH NOCHECK ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_CONF_VISTA_INICIAL_PROCE_CONSUMIDO_VISIBLE] DEFAULT (1) FOR [CONSUMIDO_VISIBLE]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_CONF_VISTA_INICIAL_PROCE_ADJ_VISIBLE] DEFAULT (1) FOR [ADJ_VISIBLE]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_CONF_VISTA_INICIAL_PROCE_AHORR_ADJ_VISIBLE] DEFAULT (1) FOR [AHORR_ADJ_VISIBLE]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_CONF_VISTA_INICIAL_PROCE_AHORR_ADJ_PORCEN_VISIBLE] DEFAULT (0) FOR [AHORR_ADJ_PORCEN_VISIBLE]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_CONF_VISTA_INICIAL_PROCE_IMP_VISIBLE] DEFAULT (0) FOR [IMP_VISIBLE]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_CONF_VISTA_INICIAL_PROCE_AHORR_OFE_VISIBLE] DEFAULT (0) FOR [AHORR_OFE_VISIBLE]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_CONF_VISTA_INICIAL_PROCE_AHORR_OFE_PORCEN_VISIBLE] DEFAULT (0) FOR [AHORR_OFE_PORCEN_VISIBLE]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_CONF_VISTA_INICIAL_PROCE_OCULTAR_GR_CERRADOS] DEFAULT (0) FOR [OCULTAR_GR_CERRADOS]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_CONF_VISTA_INICIAL_PROCE_EXCLUIR_GR_CERRADOS_RESULT] DEFAULT (0) FOR [EXCLUIR_GR_CERRADOS_RESULT]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_CONF_VISTA_INICIAL_PROCE_TIPOVISION] DEFAULT (0) FOR [TIPOVISION]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_CONF_VISTA_INICIAL_PROCE_OCULTAR_NOADJ] DEFAULT (0) FOR [OCULTAR_NOADJ]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_CONF_VISTA_INICIAL_PROCE_OCULTAR_PROV_SIN_OFE] DEFAULT (0) FOR [OCULTAR_PROV_SIN_OFE]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_CONF_VISTA_INICIAL_PROCE_DEC_RESULT] DEFAULT (2) FOR [DEC_RESULT]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_CONF_VISTA_INICIAL_PROCE_DEC_PORCEN] DEFAULT (2) FOR [DEC_PORCEN]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

'---
sConsulta = "CREATE TABLE [dbo].[CONF_VISTA_INICIAL_GRUPO] (" & vbCrLf
sConsulta = sConsulta & "   [USU] [varchar] (" & gLongitudesDeCodigos.giLongCodUSU & ") NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [ES_GR] [tinyint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [DESCR_WIDTH] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [ANYO_POS] [smallint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [ANYO_VISIBLE] [tinyint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [ANYO_WIDTH] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [ANYO_LEVEL] [tinyint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PROV_POS] [smallint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PROV_VISIBLE] [tinyint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PROV_WIDTH] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PROV_LEVEL] [tinyint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PRESUNI_POS] [smallint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PRESUNI_VISIBLE] [tinyint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PRESUNI_WIDTH] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PRESUNI_LEVEL] [tinyint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [OBJ_POS] [smallint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [OBJ_VISIBLE] [tinyint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [OBJ_WIDTH] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [OBJ_LEVEL] [tinyint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [ADJ_POS] [smallint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [ADJ_VISIBLE] [tinyint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [ADJ_WIDTH] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [ADJ_LEVEL] [tinyint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [CANT_POS] [smallint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [CANT_VISIBLE] [tinyint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [CANT_WIDTH] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [CANT_LEVEL] [tinyint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [IMP_POS] [smallint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [IMP_VISIBLE] [tinyint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [IMP_WIDTH] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [IMP_LEVEL] [tinyint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [AHORRO_IMP_POS] [smallint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [AHORRO_IMP_VISIBLE] [tinyint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [AHORRO_IMP_WIDTH] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [AHORRO_IMP_LEVEL] [tinyint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [AHORRO_PORCEN_POS] [smallint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [AHORRO_PORCEN_VISIBLE] [tinyint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [AHORRO_PORCEN_WIDTH] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [AHORRO_PORCEN_LEVEL] [tinyint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [OCULTAR_IT_CERRADOS] [tinyint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [EXCLUIR_IT_CERRADOS_RESULT] [tinyint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [OCULTAR_NOADJ] [tinyint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [OCULTAR_FILA2] [tinyint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [OCULTAR_PROV_SIN_OFE] [tinyint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [GRUPO0_WIDTH] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [GRUPOS_WIDTH] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [IMP_ADJ_VISIBLE] [tinyint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [IMP_ADJ_WIDTH] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [ANCHO_FILA] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PRECIO_PROV_POS] [smallint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PRECIO_PROV_VISIBLE] [tinyint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PRECIO_PROV_WIDTH] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PRECIO_PROV_LEVEL] [tinyint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [CANT_PROV_POS] [smallint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [CANT_PROV_VISIBLE] [tinyint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [CANT_PROV_WIDTH] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [CANT_PROV_LEVEL] [tinyint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [ADJ_PROV_POS] [smallint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [ADJ_PROV_VISIBLE] [tinyint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [ADJ_PROV_WIDTH] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [ADJ_PROV_LEVEL] [tinyint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [IMP_ADJ_POS] [smallint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [IMP_ADJ_LEVEL] [tinyint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [DEC_RESULT] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [DEC_PORCEN] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [DEC_PRECIOS] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [DEC_CANT] [int] NOT NULL " & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[CONF_VISTA_INICIAL_GRUPO] WITH NOCHECK ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [PK_CONF_VISTA_INICIAL_GRUPO] PRIMARY KEY  CLUSTERED " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [USU]," & vbCrLf
sConsulta = sConsulta & "       [ES_GR]" & vbCrLf
sConsulta = sConsulta & "   )  ON [PRIMARY] " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[CONF_VISTA_INICIAL_GRUPO] WITH NOCHECK ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_CONF_VISTA_INICIAL_GR_ANYO_VISIBLE] DEFAULT (1) FOR [ANYO_VISIBLE]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_CONF_VISTA_INICIAL_GR_PROV_VISIBLE] DEFAULT (1) FOR [PROV_VISIBLE]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_CONF_VISTA_INICIAL_GR_PRESUNI_VISIBLE] DEFAULT (1) FOR [PRESUNI_VISIBLE]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_CONF_VISTA_INICIAL_GR_OBJ_VISIBLE] DEFAULT (1) FOR [OBJ_VISIBLE]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_CONF_VISTA_INICIAL_GR_ADJ_VISIBLE] DEFAULT (1) FOR [ADJ_VISIBLE]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_CONF_VISTA_INICIAL_GR_CANT_VISIBLE] DEFAULT (1) FOR [CANT_VISIBLE]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_CONF_VISTA_INICIAL_GR_IMP_VISIBLE] DEFAULT (1) FOR [IMP_VISIBLE]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_CONF_VISTA_INICIAL_GR_AHORRO_IMP_VISIBLE] DEFAULT (1) FOR [AHORRO_IMP_VISIBLE]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_CONF_VISTA_INICIAL_GR_AHORRO_PORCEN_VISIBLE] DEFAULT (1) FOR [AHORRO_PORCEN_VISIBLE]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_CONF_VISTA_INICIAL_GR_OCULTAR_IT_CERRADOS] DEFAULT (0) FOR [OCULTAR_IT_CERRADOS]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_CONF_VISTA_INICIAL_GR_EXCLUIR_IT_CERRADOS_RESULT] DEFAULT (0) FOR [EXCLUIR_IT_CERRADOS_RESULT]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_CONF_VISTA_INICIAL_GR_OCULTAR_NOADJ] DEFAULT (0) FOR [OCULTAR_NOADJ]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_CONF_VISTA_INICIAL_GR_OCULTAR_FILA2] DEFAULT (0) FOR [OCULTAR_FILA2]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_CONF_VISTA_INICIAL_GR_OCULTAR_PROV_SIN_OFE] DEFAULT (0) FOR [OCULTAR_PROV_SIN_OFE]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_CONF_VISTA_INICIAL_GR_IMP_ADJ_VISIBLE] DEFAULT (0) FOR [IMP_ADJ_VISIBLE]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_CONF_VISTA_INICIAL_GR_PRECIO_PROV_VISIBLE] DEFAULT (1) FOR [PRECIO_PROV_VISIBLE]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_CONF_VISTA_INICIAL_GR_PRECIO_PROV_LEVEL] DEFAULT (0) FOR [PRECIO_PROV_LEVEL]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_CONF_VISTA_INICIAL_GR_CANT_PROV_VISIBLE] DEFAULT (1) FOR [CANT_PROV_VISIBLE]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_CONF_VISTA_INICIAL_GR_CANT_PROV_LEVEL] DEFAULT (1) FOR [CANT_PROV_LEVEL]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_CONF_VISTA_INICIAL_GR_ADJ_PROV_VISIBLE] DEFAULT (1) FOR [ADJ_PROV_VISIBLE]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_CONF_VISTA_INICIAL_GR_ADJ_PROV_LEVEL] DEFAULT (1) FOR [ADJ_PROV_LEVEL]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_CONF_VISTA_INICIAL_GR_IMP_ADJ_LEVEL] DEFAULT (0) FOR [IMP_ADJ_LEVEL]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_CONF_VISTA_INICIAL_GRUPO_DEC_RESULT] DEFAULT (2) FOR [DEC_RESULT]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_CONF_VISTA_INICIAL_GRUPO_DEC_PORCEN] DEFAULT (2) FOR [DEC_PORCEN]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_CONF_VISTA_INICIAL_GRUPO_DEC_PRECIOS] DEFAULT (2) FOR [DEC_PRECIOS]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_CONF_VISTA_INICIAL_GRUPO_DEC_CANT] DEFAULT (2) FOR [DEC_CANT]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


End Sub

Private Sub V_2_11_5_TablasModif()
Dim sConsulta As String

'Tabla PROVE_aRT4
sConsulta = "  ALTER TABLE [dbo].[PROVE_ART4] ADD "
sConsulta = sConsulta & " [THUMBNAIL] [image] NULL"
ExecuteSQL gRDOCon, sConsulta

'Tabla ITEM
sConsulta = "  ALTER TABLE [dbo].[ITEM] ALTER COLUMN [DESCR] [varchar] (200) NULL"
ExecuteSQL gRDOCon, sConsulta

'Tabla ART4
sConsulta = "  ALTER TABLE [dbo].[ART4] ALTER COLUMN [DEN] [varchar] (200) NOT NULL"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "  ALTER TABLE [dbo].[CONF_VISTAS_PROCE] ADD"
sConsulta = sConsulta & " [ANCHO_FILA] [float] NULL,"
sConsulta = sConsulta & " [GRUPO0_WIDTH] [float] NULL,"
sConsulta = sConsulta & " [COD_WIDTH] [float] NULL,"
sConsulta = sConsulta & " [DEN_WIDTH] [float] NULL,"
sConsulta = sConsulta & " [DEC_RESULT] [int] NOT NULL CONSTRAINT [DF_CONF_VISTAS_PROCE_DEC_RESULT] DEFAULT 2,"
sConsulta = sConsulta & " [DEC_PORCEN] [int] NOT NULL CONSTRAINT [DF_CONF_VISTAS_PROCE_DEC_PORCEN] DEFAULT 2"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "  ALTER TABLE [dbo].[CONF_VISTAS_GRUPO] ADD"
sConsulta = sConsulta & " [ANCHO_FILA] [float] NULL,"
sConsulta = sConsulta & " [PRECIO_PROV_POS] [smallint] NULL,"
sConsulta = sConsulta & " [PRECIO_PROV_VISIBLE] [tinyint] NOT NULL CONSTRAINT [DF_CONF_VISTAS_GRUPO_PRECIO_PROV_VISIBLE] DEFAULT 1,"
sConsulta = sConsulta & " [PRECIO_PROV_WIDTH] [float] NULL,"
sConsulta = sConsulta & " [PRECIO_PROV_LEVEL] [tinyint] NOT NULL CONSTRAINT [DF_CONF_VISTAS_GRUPO_PRECIO_PROV_LEVEL] DEFAULT 0,"
sConsulta = sConsulta & " [CANT_PROV_POS] [smallint] NULL,"
sConsulta = sConsulta & " [CANT_PROV_VISIBLE] [tinyint] NOT NULL CONSTRAINT [DF_CONF_VISTAS_GRUPO_CANT_PROV_VISIBLE] DEFAULT 1,"
sConsulta = sConsulta & " [CANT_PROV_WIDTH] [float] NULL,"
sConsulta = sConsulta & " [CANT_PROV_LEVEL] [tinyint] NOT NULL CONSTRAINT [DF_CONF_VISTAS_GRUPO_CANT_PROV_LEVEL] DEFAULT 1,"
sConsulta = sConsulta & " [ADJ_PROV_POS] [smallint] NULL,"
sConsulta = sConsulta & " [ADJ_PROV_VISIBLE] [tinyint] NOT NULL CONSTRAINT [DF_CONF_VISTAS_GRUPO_ADJ_PROV_VISIBLE] DEFAULT 1,"
sConsulta = sConsulta & " [ADJ_PROV_WIDTH] [float] NULL,"
sConsulta = sConsulta & " [ADJ_PROV_LEVEL] [tinyint] NOT NULL CONSTRAINT [DF_CONF_VISTAS_GRUPO_ADJ_PROV_LEVEL] DEFAULT 1,"
sConsulta = sConsulta & " [IMP_ADJ_POS] [smallint] NULL,"
sConsulta = sConsulta & " [IMP_ADJ_LEVEL] [tinyint] NOT NULL CONSTRAINT [DF_CONF_VISTAS_GRUPO_IMP_ADJ_LEVEL] DEFAULT 0,"
sConsulta = sConsulta & " [DEC_RESULT] [int] NOT NULL CONSTRAINT [DF_CONF_VISTAS_GRUPO_DEC_RESULT] DEFAULT 2,"
sConsulta = sConsulta & " [DEC_PORCEN] [int] NOT NULL CONSTRAINT [DF_CONF_VISTAS_GRUPO_DEC_PORCEN] DEFAULT 2,"
sConsulta = sConsulta & " [DEC_PRECIOS] [int] NOT NULL CONSTRAINT [DF_CONF_VISTAS_GRUPO_DEC_PRECIOS] DEFAULT 2,"
sConsulta = sConsulta & " [DEC_CANT] [int] NOT NULL CONSTRAINT [DF_CONF_VISTAS_GRUPO_DEC_CANT] DEFAULT 2"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "  ALTER TABLE [dbo].[CONF_VISTAS_ALL] ADD"
sConsulta = sConsulta & " [ANCHO_FILA] [float] NULL,"
sConsulta = sConsulta & " [PRECIO_PROV_POS] [smallint] NULL,"
sConsulta = sConsulta & " [PRECIO_PROV_VISIBLE] [tinyint] NOT NULL CONSTRAINT [DF_CONF_VISTAS_ALL_PRECIO_PROV_VISIBLE] DEFAULT 1,"
sConsulta = sConsulta & " [PRECIO_PROV_WIDTH] [float] NULL,"
sConsulta = sConsulta & " [PRECIO_PROV_LEVEL] [tinyint] NOT NULL CONSTRAINT [DF_CONF_VISTAS_ALL_PRECIO_PROV_LEVEL] DEFAULT 0,"
sConsulta = sConsulta & " [CANT_PROV_POS] [smallint] NULL,"
sConsulta = sConsulta & " [CANT_PROV_VISIBLE] [tinyint] NOT NULL CONSTRAINT [DF_CONF_VISTAS_ALL_CANT_PROV_VISIBLE] DEFAULT 1,"
sConsulta = sConsulta & " [CANT_PROV_WIDTH] [float] NULL,"
sConsulta = sConsulta & " [CANT_PROV_LEVEL] [tinyint] NOT NULL CONSTRAINT [DF_CONF_VISTAS_ALL_CANT_PROV_LEVEL] DEFAULT 1,"
sConsulta = sConsulta & " [ADJ_PROV_POS] [smallint] NULL,"
sConsulta = sConsulta & " [ADJ_PROV_VISIBLE] [tinyint] NOT NULL CONSTRAINT [DF_CONF_VISTAS_ALL_ADJ_PROV_VISIBLE] DEFAULT 1,"
sConsulta = sConsulta & " [ADJ_PROV_WIDTH] [float] NULL,"
sConsulta = sConsulta & " [ADJ_PROV_LEVEL] [tinyint] NOT NULL CONSTRAINT [DF_CONF_VISTAS_ALL_ADJ_PROV_LEVEL] DEFAULT 1,"
sConsulta = sConsulta & " [IMP_ADJ_POS] [smallint] NULL,"
sConsulta = sConsulta & " [IMP_ADJ_LEVEL] [tinyint] NOT NULL CONSTRAINT [DF_CONF_VISTAS_ALL_IMP_ADJ_LEVEL] DEFAULT 0,"
sConsulta = sConsulta & " [DEC_RESULT] [int] NOT NULL CONSTRAINT [DF_CONF_VISTAS_ALL_DEC_RESULT] DEFAULT 2,"
sConsulta = sConsulta & " [DEC_PORCEN] [int] NOT NULL CONSTRAINT [DF_CONF_VISTAS_ALL_DEC_PORCEN] DEFAULT 2,"
sConsulta = sConsulta & " [DEC_PRECIOS] [int] NOT NULL CONSTRAINT [DF_CONF_VISTAS_ALL_DEC_PRECIOS] DEFAULT 2,"
sConsulta = sConsulta & " [DEC_CANT] [int] NOT NULL CONSTRAINT [DF_CONF_VISTAS_ALL_DEC_CANT] DEFAULT 2"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "  ALTER TABLE [dbo].[CONF_VISTAS_GRUPO_ATRIB] ADD"
sConsulta = sConsulta & " [POSICION] [smallint] NULL,"
sConsulta = sConsulta & " [FILA] [tinyint] NOT NULL  CONSTRAINT [DF_CONF_VISTAS_GRUPO_ATRIB_FILA] DEFAULT 0"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "  ALTER TABLE [dbo].[CONF_VISTAS_ALL_ATRIB] ADD"
sConsulta = sConsulta & " [POSICION] [smallint] NULL,"
sConsulta = sConsulta & " [FILA] [tinyint] NOT NULL  CONSTRAINT [DF_CONF_VISTAS_GRUPO_ALL_FILA] DEFAULT 0"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "  ALTER TABLE [dbo].[CONF_VISTAS_ITEM] ADD"
sConsulta = sConsulta & " [ANCHO_FILA] [float] NULL,"
sConsulta = sConsulta & " [DEC_RESULT] [int] NOT NULL CONSTRAINT [DF_CONF_VISTAS_ITEM_DEC_RESULT] DEFAULT 2,"
sConsulta = sConsulta & " [DEC_PORCEN] [int] NOT NULL CONSTRAINT [DF_CONF_VISTAS_ITEM_DEC_PORCEN] DEFAULT 2,"
sConsulta = sConsulta & " [DEC_PRECIOS] [int] NOT NULL CONSTRAINT [DF_CONF_VISTAS_ITEM_DEC_PRECIOS] DEFAULT 2,"
sConsulta = sConsulta & " [DEC_CANT] [int] NOT NULL CONSTRAINT [DF_CONF_VISTAS_ITEM_DEC_CANT] DEFAULT 2"
ExecuteSQL gRDOCon, sConsulta



End Sub

Private Sub V_2_11_5_Triggers()
Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PROCE_OFE_TG_DEL]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PROCE_OFE_TG_DEL]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TRIGGER PROCE_OFE_TG_DEL ON dbo.PROCE_OFE " & vbCrLf
sConsulta = sConsulta & "FOR DELETE " & vbCrLf
sConsulta = sConsulta & "AS " & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO AS INTEGER " & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN1 AS VARCHAR(50) " & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCE AS INTEGER " & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVE AS VARCHAR(50) " & vbCrLf
sConsulta = sConsulta & "DECLARE @MAXOFE AS INT " & vbCrLf
sConsulta = sConsulta & "DECLARE @ITEM AS INTEGER " & vbCrLf
sConsulta = sConsulta & "DECLARE @PRECIO AS FLOAT " & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVE2 AS VARCHAR(50) " & vbCrLf
sConsulta = sConsulta & "DECLARE @OFE2 AS SMALLINT " & vbCrLf
sConsulta = sConsulta & "DECLARE @IDENT AS INTEGER " & vbCrLf
sConsulta = sConsulta & "DECLARE @CAMBIOMONCEN FLOAT " & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_PROCE_OFE_Del CURSOR FOR SELECT ANYO,GMN1,PROCE,PROVE FROM DELETED " & vbCrLf
sConsulta = sConsulta & "OPEN curTG_PROCE_OFE_Del " & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_OFE_Del INTO @ANYO,@GMN1,@PROCE,@PROVE " & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0 " & vbCrLf
sConsulta = sConsulta & "BEGIN " & vbCrLf
sConsulta = sConsulta & " IF (SELECT SUBASTA FROM PROCE_DEF WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE) = 1 " & vbCrLf
sConsulta = sConsulta & " BEGIN " & vbCrLf
sConsulta = sConsulta & "     --Si existen precios en la oferta eliminada que eran m�nimas en la puja se deber�n recalcular los nuevos precios m�nimos " & vbCrLf
sConsulta = sConsulta & "         /*Obtiene todos los items para los cuales ese proveedor tiene la oferta m�nima*/ " & vbCrLf
sConsulta = sConsulta & "         DECLARE C CURSOR FOR SELECT ITEM FROM OFEMIN WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "         OPEN C " & vbCrLf
sConsulta = sConsulta & "         FETCH NEXT FROM C INTO @ITEM " & vbCrLf
sConsulta = sConsulta & "         WHILE @@FETCH_STATUS=0 " & vbCrLf
sConsulta = sConsulta & "         BEGIN " & vbCrLf
sConsulta = sConsulta & "             SELECT TOP 1 @PRECIO= A.PRECIO / (B.CAMBIO * C.CAMBIO), @PROVE2=A.PROVE,@OFE2=A.NUM" & vbCrLf
sConsulta = sConsulta & "                   FROM ITEM_OFE A INNER JOIN PROCE_OFE B ON A.ANYO=B.ANYO AND A.GMN1=B.GMN1 AND A.PROCE=B.PROCE AND A.PROVE=B.PROVE AND A.NUM=B.OFE" & vbCrLf
sConsulta = sConsulta & "                  INNER JOIN PROCE C ON A.ANYO=C.ANYO AND A.GMN1=C.GMN1 AND A.PROCE = C.COD" & vbCrLf
sConsulta = sConsulta & "             WHERE A.ANYO=@ANYO AND A.GMN1=@GMN1 AND A.PROCE=@PROCE AND A.ITEM=@ITEM AND A.ULT=1 AND A.PRECIO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "             ORDER BY A.PRECIO / (B.CAMBIO * C.CAMBIO) ASC" & vbCrLf
sConsulta = sConsulta & "             IF @PRECIO IS NULL " & vbCrLf
sConsulta = sConsulta & "             BEGIN " & vbCrLf
sConsulta = sConsulta & "                 UPDATE OFEMIN SET PRECIO=NULL, PROVE=NULL, OFE=NULL WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE = @PROCE AND ITEM = @ITEM " & vbCrLf
sConsulta = sConsulta & "             End " & vbCrLf
sConsulta = sConsulta & "             Else " & vbCrLf
sConsulta = sConsulta & "             BEGIN " & vbCrLf
sConsulta = sConsulta & "                 /*Actualiza la tabla OFEMIN con la oferta m�nima para el proceso e item*/ " & vbCrLf
sConsulta = sConsulta & "                 UPDATE OFEMIN SET PRECIO=@PRECIO,PROVE=@PROVE2,OFE=@OFE2, ULTOFE=0 WHERE ANYO=@ANYO  AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM " & vbCrLf
sConsulta = sConsulta & "             End " & vbCrLf
sConsulta = sConsulta & "             FETCH NEXT FROM C INTO @ITEM " & vbCrLf
sConsulta = sConsulta & "         End " & vbCrLf
sConsulta = sConsulta & "         Close C " & vbCrLf
sConsulta = sConsulta & "         DEALLOCATE C " & vbCrLf
sConsulta = sConsulta & "     EXEC SP_CALCULAR_DETALLE_PUJAS @ANYO, @GMN1, @PROCE" & vbCrLf
sConsulta = sConsulta & " END" & vbCrLf
sConsulta = sConsulta & "     SET @MAXOFE = ( SELECT MAX(OFE) FROM PROCE_OFE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ENVIADA=1 AND OFE <> -1 ) " & vbCrLf
sConsulta = sConsulta & "     IF (@MAXOFE IS NULL ) " & vbCrLf
sConsulta = sConsulta & "     BEGIN " & vbCrLf
sConsulta = sConsulta & "     /* Si se han insertado ofertas, actualizamos el estado del proceso  si es que este era inferior a 7 */ " & vbCrLf
sConsulta = sConsulta & "         IF (SELECT PROCE.EST FROM PROCE WHERE ANYO=@ANYO  AND GMN1=@GMN1 AND COD=@PROCE) = 7  /* 7= Con ofertas */ " & vbCrLf
sConsulta = sConsulta & "         BEGIN " & vbCrLf
sConsulta = sConsulta & "             UPDATE PROCE SET EST = 6  WHERE ANYO= @ANYO  AND GMN1=@GMN1 AND COD=@PROCE " & vbCrLf
sConsulta = sConsulta & "         End " & vbCrLf
sConsulta = sConsulta & "     End " & vbCrLf
sConsulta = sConsulta & " FETCH NEXT FROM curTG_PROCE_OFE_Del INTO @ANYO,@GMN1,@PROCE,@PROVE " & vbCrLf
sConsulta = sConsulta & "End " & vbCrLf
sConsulta = sConsulta & "Close curTG_PROCE_OFE_Del " & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_PROCE_OFE_Del " & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PROCE_OFE_TG_INSUPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PROCE_OFE_TG_INSUPD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TRIGGER PROCE_OFE_TG_INSUPD ON [PROCE_OFE]" & vbCrLf
sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE SET FECACT = GETDATE()" & vbCrLf
sConsulta = sConsulta & "  FROM PROCE_OFE PO" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN INSERTED I" & vbCrLf
sConsulta = sConsulta & "                ON PO.GMN1=I.GMN1" & vbCrLf
sConsulta = sConsulta & "               AND PO.PROCE = I.PROCE" & vbCrLf
sConsulta = sConsulta & "               AND PO.ANYO = I.ANYO" & vbCrLf
sConsulta = sConsulta & "               AND PO.PROVE = I.PROVE" & vbCrLf
sConsulta = sConsulta & "               AND PO.OFE = I.OFE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub
Private Sub V_2_11_5_Storeds()
Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_VALIDAR_OFERTA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_VALIDAR_OFERTA]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "CREATE PROCEDURE SP_VALIDAR_OFERTA @ANYO SMALLINT,@GMN1 VARCHAR(50), @PROCE INT,@PROVE VARCHAR(50), @OFE INT , @ERROR TINYINT OUTPUT , @NEWOFE INT =NULL OUTPUT AS" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @OFEATRIB BIT" & vbCrLf
sConsulta = sConsulta & "DECLARE @OFEGRUPOATRIB BIT" & vbCrLf
sConsulta = sConsulta & "DECLARE @OFEITEMATRIB BIT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @ERROR = 0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @PAID INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @AMBITO SMALLINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @GRUPO VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @TIPO SMALLINT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE CATRIBS CURSOR LOCAL FORWARD_ONLY  FOR " & vbCrLf
sConsulta = sConsulta & "SELECT PA.ID, AMBITO, GRUPO, TIPO" & vbCrLf
sConsulta = sConsulta & "  FROM PROCE_ATRIB PA" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN DEF_ATRIB DA" & vbCrLf
sConsulta = sConsulta & "           ON PA.ATRIB = DA.ID" & vbCrLf
sConsulta = sConsulta & " WHERE ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "   AND GMN1= @GMN1" & vbCrLf
sConsulta = sConsulta & "   AND PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "   AND INTERNO = 0" & vbCrLf
sConsulta = sConsulta & "   AND OBLIG = 1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "OPEN CATRIBS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM CATRIBS INTO @PAID, @AMBITO, @GRUPO,  @TIPO" & vbCrLf
sConsulta = sConsulta & "SET @OFEATRIB=0" & vbCrLf
sConsulta = sConsulta & "SET @OFEGRUPOATRIB=0" & vbCrLf
sConsulta = sConsulta & "SET @OFEITEMATRIB=0" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0 AND @OFEATRIB=0 AND @OFEGRUPOATRIB=0 AND @OFEITEMATRIB=0" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    IF @AMBITO = 1" & vbCrLf
sConsulta = sConsulta & "      BEGIN" & vbCrLf
sConsulta = sConsulta & "        SET @OFEATRIB =1" & vbCrLf
sConsulta = sConsulta & "   SELECT @OFEATRIB = CASE " & vbCrLf
sConsulta = sConsulta & "                  WHEN CASE @TIPO " & vbCrLf
sConsulta = sConsulta & "                          WHEN 1 THEN CASE WHEN VALOR_TEXT ='' THEN NULL ELSE VALOR_TEXT END " & vbCrLf
sConsulta = sConsulta & "                          WHEN 2 THEN CAST(VALOR_NUM AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                          WHEN 3 THEN CAST(VALOR_FEC AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                          WHEN 4 THEN CAST(VALOR_BOOL AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                       END IS NULL " & vbCrLf
sConsulta = sConsulta & "                  THEN 1 " & vbCrLf
sConsulta = sConsulta & "                  ELSE 0 " & vbCrLf
sConsulta = sConsulta & "               END" & vbCrLf
sConsulta = sConsulta & "          FROM OFE_ATRIB" & vbCrLf
sConsulta = sConsulta & "         WHERE ANYO= @ANYO" & vbCrLf
sConsulta = sConsulta & "           AND GMN1= @GMN1" & vbCrLf
sConsulta = sConsulta & "           AND PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "           AND PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "           AND OFE = @OFE" & vbCrLf
sConsulta = sConsulta & "           AND ATRIB_ID = @PAID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "    IF @AMBITO = 2" & vbCrLf
sConsulta = sConsulta & "      BEGIN" & vbCrLf
sConsulta = sConsulta & "        SET @OFEGRUPOATRIB =0" & vbCrLf
sConsulta = sConsulta & "        IF @GRUPO IS NULL " & vbCrLf
sConsulta = sConsulta & "          BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "          IF (SELECT COUNT(*) " & vbCrLf
sConsulta = sConsulta & "                FROM OFE_GR_ATRIB " & vbCrLf
sConsulta = sConsulta & "               WHERE ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "                 AND GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "                 AND PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "                 AND PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "                 AND OFE = @OFE" & vbCrLf
sConsulta = sConsulta & "                 AND ATRIB_ID = @PAID" & vbCrLf
sConsulta = sConsulta & "                 AND CASE @TIPO " & vbCrLf
sConsulta = sConsulta & "                          WHEN 1 THEN CASE WHEN VALOR_TEXT ='' THEN NULL ELSE VALOR_TEXT END " & vbCrLf
sConsulta = sConsulta & "                          WHEN 2 THEN CAST(VALOR_NUM AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                          WHEN 3 THEN CAST(VALOR_FEC AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                          WHEN 4 THEN CAST(VALOR_BOOL AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                       END IS NOT NULL) <> (SELECT COUNT(*) " & vbCrLf
sConsulta = sConsulta & "                 FROM PROCE_GRUPO " & vbCrLf
sConsulta = sConsulta & "                WHERE ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "                  AND GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "                  AND PROCE = @PROCE)" & vbCrLf
sConsulta = sConsulta & "             SET @OFEGRUPOATRIB = 1" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "         ELSE" & vbCrLf
sConsulta = sConsulta & "          IF (SELECT COUNT(*) " & vbCrLf
sConsulta = sConsulta & "                FROM OFE_GR_ATRIB " & vbCrLf
sConsulta = sConsulta & "               WHERE ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "                 AND GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "                 AND PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "                 AND GRUPO = @GRUPO" & vbCrLf
sConsulta = sConsulta & "                 AND PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "                 AND OFE = @OFE" & vbCrLf
sConsulta = sConsulta & "                 AND ATRIB_ID = @PAID" & vbCrLf
sConsulta = sConsulta & "                 AND CASE @TIPO " & vbCrLf
sConsulta = sConsulta & "                          WHEN 1 THEN CASE WHEN VALOR_TEXT ='' THEN NULL ELSE VALOR_TEXT END " & vbCrLf
sConsulta = sConsulta & "                          WHEN 2 THEN CAST(VALOR_NUM AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                          WHEN 3 THEN CAST(VALOR_FEC AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                          WHEN 4 THEN CAST(VALOR_BOOL AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                       END IS NOT NULL)  =0 " & vbCrLf
sConsulta = sConsulta & "             SET @OFEGRUPOATRIB = 1" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "    IF @AMBITO = 3" & vbCrLf
sConsulta = sConsulta & "      BEGIN" & vbCrLf
sConsulta = sConsulta & "        SET @OFEITEMATRIB =0" & vbCrLf
sConsulta = sConsulta & "        IF @GRUPO IS NULL " & vbCrLf
sConsulta = sConsulta & "          BEGIN" & vbCrLf
sConsulta = sConsulta & "            IF (SELECT COUNT(*) " & vbCrLf
sConsulta = sConsulta & "                  FROM OFE_ITEM_ATRIB " & vbCrLf
sConsulta = sConsulta & "                 WHERE ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "                   AND GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "                   AND PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "                   AND PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "                   AND OFE = @OFE" & vbCrLf
sConsulta = sConsulta & "                   AND ATRIB_ID = @PAID" & vbCrLf
sConsulta = sConsulta & "                   AND CASE @TIPO " & vbCrLf
sConsulta = sConsulta & "                          WHEN 1 THEN CASE WHEN VALOR_TEXT ='' THEN NULL ELSE VALOR_TEXT END " & vbCrLf
sConsulta = sConsulta & "                          WHEN 2 THEN CAST(VALOR_NUM AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                          WHEN 3 THEN CAST(VALOR_FEC AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                          WHEN 4 THEN CAST(VALOR_BOOL AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                       END IS NOT NULL)  <>" & vbCrLf
sConsulta = sConsulta & "                (SELECT COUNT(*) " & vbCrLf
sConsulta = sConsulta & "                   FROM ITEM " & vbCrLf
sConsulta = sConsulta & "                  WHERE ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "                    AND GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "                    AND PROCE = @PROCE                    " & vbCrLf
sConsulta = sConsulta & "                    AND CONF = 1)" & vbCrLf
sConsulta = sConsulta & "                 SET @OFEITEMATRIB = 1" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "        ELSE" & vbCrLf
sConsulta = sConsulta & "           begin" & vbCrLf
sConsulta = sConsulta & "            IF (SELECT COUNT(*) " & vbCrLf
sConsulta = sConsulta & "                  FROM OFE_ITEM_ATRIB OIA" & vbCrLf
sConsulta = sConsulta & "                        INNER JOIN ITEM I" & vbCrLf
sConsulta = sConsulta & "                                ON OIA.ANYO = I.ANYO" & vbCrLf
sConsulta = sConsulta & "                               AND OIA.GMN1 = I.GMN1" & vbCrLf
sConsulta = sConsulta & "                               AND OIA.PROCE = I.PROCE" & vbCrLf
sConsulta = sConsulta & "                               AND OIA.ITEM = I.ID" & vbCrLf
sConsulta = sConsulta & "                 WHERE OIA.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "                   AND OIA.GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "                   AND OIA.PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "                   AND I.GRUPO = @GRUPO" & vbCrLf
sConsulta = sConsulta & "                   AND OIA.PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "                   AND OIA.OFE = @OFE" & vbCrLf
sConsulta = sConsulta & "                   AND OIA.ATRIB_ID = @PAID" & vbCrLf
sConsulta = sConsulta & "                   AND CASE @TIPO " & vbCrLf
sConsulta = sConsulta & "                          WHEN 1 THEN CASE WHEN VALOR_TEXT ='' THEN NULL ELSE VALOR_TEXT END " & vbCrLf
sConsulta = sConsulta & "                          WHEN 2 THEN CAST(VALOR_NUM AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                          WHEN 3 THEN CAST(VALOR_FEC AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                          WHEN 4 THEN CAST(VALOR_BOOL AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                       END IS NOT NULL)  <>" & vbCrLf
sConsulta = sConsulta & "                (SELECT COUNT(*) " & vbCrLf
sConsulta = sConsulta & "                   FROM ITEM " & vbCrLf
sConsulta = sConsulta & "                  WHERE ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "                    AND GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "                    AND PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "                    AND GRUPO = @GRUPO" & vbCrLf
sConsulta = sConsulta & "                    AND CONF = 1)" & vbCrLf
sConsulta = sConsulta & "                 SET @OFEITEMATRIB = 1" & vbCrLf
sConsulta = sConsulta & "             end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "      FETCH NEXT FROM CATRIBS INTO @PAID, @AMBITO, @GRUPO,  @TIPO" & vbCrLf
sConsulta = sConsulta & "    END" & vbCrLf
sConsulta = sConsulta & "    " & vbCrLf
sConsulta = sConsulta & "CLOSE CATRIBS" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE CATRIBS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @OFEATRIB = 1" & vbCrLf
sConsulta = sConsulta & "  SET @ERROR = 2" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @OFEGRUPOATRIB = 1" & vbCrLf
sConsulta = sConsulta & "  SET @ERROR = 3" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @OFEITEMATRIB = 1" & vbCrLf
sConsulta = sConsulta & "  SET @ERROR = 4" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ERROR = 0 " & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    UPDATE PROCE_OFE SET ULT= 0, LEIDO = 1  WHERE ANYO = @ANYO AND GMN1 = @GMN1 AND PROCE = @PROCE AND PROVE = @PROVE AND ULT = 1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    DECLARE @NUMOFEWEB AS int" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    SELECT @NEWOFE = ISNULL(MAX(OFE),0) + 1 FROM PROCE_OFE WHERE ANYO = @ANYO AND GMN1 = @GMN1 AND PROCE = @PROCE AND PROVE = @PROVE AND OFE != -1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO PROCE_OFE(ANYO, GMN1, PROCE, PROVE, OFE, EST, FECREC, FECVAL, MON, CAMBIO, OBS, ULT, FECACT, LEIDO, CERRADO, ENVIADA, OBSADJUN)" & vbCrLf
sConsulta = sConsulta & "SELECT ANYO, GMN1, PROCE, PROVE, @NEWOFE, EST, FECREC, FECVAL, MON, CAMBIO, OBS, 1, FECACT, LEIDO, CERRADO, 1, OBSADJUN" & vbCrLf
sConsulta = sConsulta & "  FROM PROCE_OFE" & vbCrLf
sConsulta = sConsulta & "WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE = @PROCE AND PROVE = @PROVE AND OFE = -1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_ITEM_ADJUN WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE= @PROCE and PROVE=@PROVE AND OFE = @NEWOFE" & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_ITEM_ATRIB WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE= @PROCE and PROVE=@PROVE AND OFE = @NEWOFE" & vbCrLf
sConsulta = sConsulta & "DELETE FROM ITEM_OFE WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE= @PROCE and PROVE=@PROVE AND NUM = @NEWOFE" & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_GR_ATRIB  WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE= @PROCE and PROVE=@PROVE AND OFE = @NEWOFE" & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_GR_ADJUN WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE= @PROCE and PROVE=@PROVE AND OFE = @NEWOFE" & vbCrLf
sConsulta = sConsulta & "DELETE FROM GRUPO_OFE WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE= @PROCE and PROVE=@PROVE AND OFE = @NEWOFE" & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_ATRIB  WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE= @PROCE and PROVE=@PROVE AND OFE = @NEWOFE" & vbCrLf
sConsulta = sConsulta & "DELETE FROM PROCE_OFE_ADJUN WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE= @PROCE and PROVE=@PROVE AND OFE = @NEWOFE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO GRUPO_OFE(ANYO, GMN1, PROCE, GRUPO, PROVE, OFE, CONSUMIDO, ADJUDICADO, AHORRADO, AHORRADO_PORCEN, ABIERTO, IMPORTE, AHORRO_OFE, AHORRO_OFE_PORCEN, FECACT, OBSADJUN)" & vbCrLf
sConsulta = sConsulta & "SELECT ANYO, GMN1, PROCE, GRUPO, PROVE, @NEWOFE, CONSUMIDO, ADJUDICADO, AHORRADO, AHORRADO_PORCEN, ABIERTO, IMPORTE, AHORRO_OFE, AHORRO_OFE_PORCEN, FECACT, OBSADJUN" & vbCrLf
sConsulta = sConsulta & "  FROM GRUPO_OFE" & vbCrLf
sConsulta = sConsulta & "WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE = @PROCE AND PROVE = @PROVE AND OFE = -1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO ITEM_OFE(ANYO, GMN1, PROCE, ITEM, PROVE, NUM, PRECIO, CANTMAX, ULT, PRECIO2, PRECIO3, USAR, PREC_VALIDO, FECACT, OBSADJUN, COMENT1, COMENT2, COMENT3)" & vbCrLf
sConsulta = sConsulta & "SELECT ANYO, GMN1, PROCE, ITEM, PROVE, @NEWOFE, PRECIO, CANTMAX, ULT, PRECIO2, PRECIO3, USAR, PREC_VALIDO, FECACT, OBSADJUN, COMENT1, COMENT2, COMENT3" & vbCrLf
sConsulta = sConsulta & "  FROM ITEM_OFE" & vbCrLf
sConsulta = sConsulta & "WHERE  ANYO = @ANYO AND GMN1=@GMN1 AND PROCE = @PROCE AND PROVE = @PROVE AND NUM = -1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO PROCE_OFE_ADJUN(ANYO, GMN1, PROCE, PROVE, OFE, ID, NOM, COM, IDPORTAL, DATASIZE)" & vbCrLf
sConsulta = sConsulta & "SELECT ANYO, GMN1, PROCE, PROVE, @NEWOFE, ID, NOM, COM, IDPORTAL, DATASIZE" & vbCrLf
sConsulta = sConsulta & "  FROM PROCE_OFE_ADJUN" & vbCrLf
sConsulta = sConsulta & "WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE = @PROCE AND PROVE = @PROVE AND OFE = -1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO OFE_ITEM_ADJUN(ANYO, GMN1, PROCE, ITEM, PROVE, OFE, ID, NOM, COM, IDPORTAL, DATASIZE)" & vbCrLf
sConsulta = sConsulta & "SELECT ANYO, GMN1, PROCE, ITEM, PROVE, @NEWOFE, ID, NOM, COM, IDPORTAL, DATASIZE" & vbCrLf
sConsulta = sConsulta & "  FROM OFE_ITEM_ADJUN" & vbCrLf
sConsulta = sConsulta & "WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE = @PROCE AND PROVE = @PROVE AND OFE = -1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO OFE_GR_ADJUN(ANYO, GMN1, PROCE, GRUPO, PROVE, OFE, ID, NOM, COM, IDPORTAL, DATASIZE)" & vbCrLf
sConsulta = sConsulta & "SELECT ANYO, GMN1, PROCE, GRUPO, PROVE, @NEWOFE, ID, NOM, COM, IDPORTAL, DATASIZE" & vbCrLf
sConsulta = sConsulta & "  FROM OFE_GR_ADJUN" & vbCrLf
sConsulta = sConsulta & " WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE = @PROCE AND PROVE = @PROVE AND OFE = -1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO OFE_GR_ATRIB(ANYO, GMN1, PROCE, GRUPO, PROVE, OFE, ATRIB_ID, VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL, VALOR_POND, FECACT)" & vbCrLf
sConsulta = sConsulta & "SELECT ANYO, GMN1, PROCE, GRUPO, PROVE, @NEWOFE, ATRIB_ID, VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL, VALOR_POND, FECACT" & vbCrLf
sConsulta = sConsulta & "  FROM OFE_GR_ATRIB" & vbCrLf
sConsulta = sConsulta & "WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE = @PROCE AND PROVE = @PROVE AND OFE = -1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO OFE_ITEM_ATRIB(ANYO, GMN1, PROCE, ITEM, PROVE, OFE, ATRIB_ID, VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL, VALOR_POND, FECACT)" & vbCrLf
sConsulta = sConsulta & "SELECT ANYO, GMN1, PROCE, ITEM, PROVE, @NEWOFE, ATRIB_ID, VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL, VALOR_POND, FECACT" & vbCrLf
sConsulta = sConsulta & "  FROM OFE_ITEM_ATRIB" & vbCrLf
sConsulta = sConsulta & "WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE = @PROCE AND PROVE = @PROVE AND OFE = -1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO OFE_ATRIB(ANYO, GMN1, PROCE, PROVE, OFE, ATRIB_ID, VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL, VALOR_POND, FECACT)" & vbCrLf
sConsulta = sConsulta & "SELECT ANYO, GMN1, PROCE, PROVE, @NEWOFE, ATRIB_ID, VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL, VALOR_POND, FECACT" & vbCrLf
sConsulta = sConsulta & "  FROM OFE_ATRIB " & vbCrLf
sConsulta = sConsulta & "WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE = @PROCE AND PROVE = @PROVE AND OFE = -1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_ITEM_ADJUN WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE= @PROCE and PROVE=@PROVE AND OFE = -1" & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_ITEM_ATRIB WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE= @PROCE and PROVE=@PROVE AND OFE = -1" & vbCrLf
sConsulta = sConsulta & "DELETE FROM ITEM_OFE WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE= @PROCE and PROVE=@PROVE AND NUM = -1" & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_GR_ATRIB  WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE= @PROCE and PROVE=@PROVE AND OFE = -1" & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_GR_ADJUN WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE= @PROCE and PROVE=@PROVE AND OFE = -1" & vbCrLf
sConsulta = sConsulta & "DELETE FROM GRUPO_OFE WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE= @PROCE and PROVE=@PROVE AND OFE = -1" & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_ATRIB  WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE= @PROCE and PROVE=@PROVE AND OFE = -1" & vbCrLf
sConsulta = sConsulta & "DELETE FROM PROCE_OFE_ADJUN WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE= @PROCE and PROVE=@PROVE AND OFE = -1" & vbCrLf
sConsulta = sConsulta & "DELETE FROM PROCE_OFE WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE= @PROCE and PROVE=@PROVE AND OFE = -1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE SET OFE = 1  WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE = @PROCE AND PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    SELECT @NUMOFEWEB = isnull(MAX(OFEWEB),0)+1 FROM PROCE_OFE_WEB WHERE ANYO = @ANYO AND GMN1 = @GMN1 AND PROCE = @PROCE AND PROVE = @PROVE " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    INSERT INTO PROCE_OFE_WEB (ANYO, GMN1, PROCE, PROVE, OFEWEB, OFE, FECREC, FECVAL, MON)" & vbCrLf
sConsulta = sConsulta & "    SELECT ANYO, GMN1, PROCE, PROVE, @NUMOFEWEB, OFE, FECREC,FECVAL,MON " & vbCrLf
sConsulta = sConsulta & "      FROM PROCE_OFE " & vbCrLf
sConsulta = sConsulta & "     WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE = @PROCE AND PROVE = @PROVE AND OFE = @NEWOFE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    INSERT INTO ITEM_OFE_WEB (ANYO, GMN1, PROCE, ITEM, PROVE, NUM, PRECIO, CANTMAX)" & vbCrLf
sConsulta = sConsulta & "    SELECT ANYO, GMN1, PROCE, ITEM, PROVE, @NUMOFEWEB, PRECIO, CANTMAX" & vbCrLf
sConsulta = sConsulta & "      FROM ITEM_OFE" & vbCrLf
sConsulta = sConsulta & "     WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE = @PROCE AND PROVE = @PROVE AND NUM = @NEWOFE" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    INSERT INTO PROCE_OFE_WEB_ADJUN (ANYO, GMN1, PROCE, PROVE, OFEWEB, ID, NOM, COM, IDPORTAL)" & vbCrLf
sConsulta = sConsulta & "    SELECT ANYO, GMN1, PROCE, PROVE, @NUMOFEWEB, ID, NOM, COM, IDPORTAL" & vbCrLf
sConsulta = sConsulta & "      FROM PROCE_OFE_ADJUN" & vbCrLf
sConsulta = sConsulta & "     WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE = @PROCE AND PROVE = @PROVE AND OFE = @NEWOFE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    UPDATE ITEM_OFE SET ULT = 0 WHERE ANYO = @ANYO AND GMN1 = @GMN1 AND PROCE =@PROCE AND PROVE=@PROVE AND NUM!=@NEWOFE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    DECLARE @ITEM INT" & vbCrLf
sConsulta = sConsulta & "    DECLARE C1 CURSOR LOCAL FOR SELECT ID FROM ITEM WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "    OPEN C1" & vbCrLf
sConsulta = sConsulta & "    " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    FETCH NEXT FROM C1 INTO @ITEM" & vbCrLf
sConsulta = sConsulta & "    WHILE @@FETCH_STATUS = 0" & vbCrLf
sConsulta = sConsulta & "      BEGIN" & vbCrLf
sConsulta = sConsulta & "        UPDATE ITEM_OFE SET ULT = 1 ,PRECIO = PRECIO" & vbCrLf
sConsulta = sConsulta & "          FROM ITEM_OFE " & vbCrLf
sConsulta = sConsulta & "         WHERE ANYO = @ANYO AND GMN1 = @GMN1 AND PROCE = @PROCE AND PROVE = @PROVE AND ITEM=@ITEM AND NUM = @NEWOFE" & vbCrLf
sConsulta = sConsulta & "        FETCH NEXT FROM C1 INTO @ITEM" & vbCrLf
sConsulta = sConsulta & "      END " & vbCrLf
sConsulta = sConsulta & "    CLOSE C1" & vbCrLf
sConsulta = sConsulta & "    DEALLOCATE C1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion2_11_50_00A2_11_50_01() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim bBool As Boolean

On Error GoTo error
    

'sConsulta = "BEGIN TRANSACTION"
'ExecuteSQL gRDOCon, sConsulta
'bTransaccionEnCurso = True
'ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

frmProgreso.lblDetalle = "Modificamos tablas"
frmProgreso.lblDetalle.Refresh
frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

If V_2_11_5_Update_01 Then
    CodigoDeActualizacion2_11_50_00A2_11_50_01 = True
Else
    CodigoDeActualizacion2_11_50_00A2_11_50_01 = False
End If

frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1


'sConsulta = "COMMIT TRANSACTION"
'ExecuteSQL gRDOCon, sConsulta
'bTransaccionEnCurso = False

'CodigoDeActualizacion2_11_50_00A2_11_50_01 = True
Exit Function
    
error:

    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_11_50_00A2_11_50_01 = False

End Function


Private Function V_2_11_5_Update_01() As Boolean
Dim sConsulta As String
Dim oADOConnection As ADODB.Connection
Dim adores As ADODB.Recordset
Dim adores2 As ADODB.Recordset
Dim adocom As ADODB.Command
Dim oParam As ADODB.Parameter
Dim iResultado As Integer

On Error GoTo error:

    Set oADOConnection = New ADODB.Connection
    oADOConnection.Open "Provider=SQLOLEDB.1;Auto Translate=FALSE;Server=" & gServer & ";Database=" & gBD & ";", gUID, gPWD
    oADOConnection.CursorLocation = adUseClient
    oADOConnection.CommandTimeout = 120

    oADOConnection.Execute "BEGIN TRANSACTION"
    oADOConnection.Execute "SET XACT_ABORT OFF"

    Set adores = New ADODB.Recordset
    
    sConsulta = "SELECT count(*) FROM PRES1_NIV1 WHERE FECACT IS NULL"
    adores.Open sConsulta, oADOConnection, adOpenForwardOnly, adLockReadOnly
    If Not adores.EOF Then
        If adores(0).Value > 0 Then
            sConsulta = "UPDATE PRES1_NIV1 SET FECACT=GETDATE() WHERE  FECACT IS NULL"
            oADOConnection.Execute sConsulta
        End If
    End If
    adores.Close

    sConsulta = "SELECT count(*) FROM PRES1_NIV2 WHERE FECACT IS NULL"
    adores.Open sConsulta, oADOConnection, adOpenForwardOnly, adLockReadOnly
    If Not adores.EOF Then
        If adores(0).Value > 0 Then
            sConsulta = "UPDATE PRES1_NIV2 SET FECACT=GETDATE() WHERE  FECACT IS NULL"
            oADOConnection.Execute sConsulta
        End If
    End If
    adores.Close

    sConsulta = "SELECT count(*) FROM PRES1_NIV3 WHERE FECACT IS NULL"
    adores.Open sConsulta, oADOConnection, adOpenForwardOnly, adLockReadOnly
    If Not adores.EOF Then
        If adores(0).Value > 0 Then
            sConsulta = "UPDATE PRES1_NIV3 SET FECACT=GETDATE() WHERE  FECACT IS NULL"
            oADOConnection.Execute sConsulta
        End If
    End If
    adores.Close

    sConsulta = "SELECT count(*) FROM PRES1_NIV4 WHERE FECACT IS NULL"
    adores.Open sConsulta, oADOConnection, adOpenForwardOnly, adLockReadOnly
    If Not adores.EOF Then
        If adores(0).Value > 0 Then
            sConsulta = "UPDATE PRES1_NIV4 SET FECACT=GETDATE() WHERE  FECACT IS NULL"
            oADOConnection.Execute sConsulta
        End If
    End If
    adores.Close
'   PRESUPUESTOS TIPO 2
    sConsulta = "SELECT count(*) FROM PRES2_NIV1 WHERE FECACT IS NULL"
    adores.Open sConsulta, oADOConnection, adOpenForwardOnly, adLockReadOnly
    If Not adores.EOF Then
        If adores(0).Value > 0 Then
            sConsulta = "UPDATE PRES2_NIV1 SET FECACT=GETDATE() WHERE  FECACT IS NULL"
            oADOConnection.Execute sConsulta
        End If
    End If
    adores.Close

    sConsulta = "SELECT count(*) FROM PRES2_NIV2 WHERE FECACT IS NULL"
    adores.Open sConsulta, oADOConnection, adOpenForwardOnly, adLockReadOnly
    If Not adores.EOF Then
        If adores(0).Value > 0 Then
            sConsulta = "UPDATE PRES2_NIV2 SET FECACT=GETDATE() WHERE  FECACT IS NULL"
            oADOConnection.Execute sConsulta
        End If
    End If
    adores.Close

    sConsulta = "SELECT count(*) FROM PRES2_NIV3 WHERE FECACT IS NULL"
    adores.Open sConsulta, oADOConnection, adOpenForwardOnly, adLockReadOnly
    If Not adores.EOF Then
        If adores(0).Value > 0 Then
            sConsulta = "UPDATE PRES2_NIV3 SET FECACT=GETDATE() WHERE  FECACT IS NULL"
            oADOConnection.Execute sConsulta
        End If
    End If
    adores.Close

    sConsulta = "SELECT count(*) FROM PRES2_NIV4 WHERE FECACT IS NULL"
    adores.Open sConsulta, oADOConnection, adOpenForwardOnly, adLockReadOnly
    If Not adores.EOF Then
        If adores(0).Value > 0 Then
            sConsulta = "UPDATE PRES2_NIV4 SET FECACT=GETDATE() WHERE  FECACT IS NULL"
            oADOConnection.Execute sConsulta
        End If
    End If
    adores.Close
    'PRESUPUESTOS 3
    sConsulta = "SELECT count(*) FROM PRES3_NIV1 WHERE FECACT IS NULL"
    adores.Open sConsulta, oADOConnection, adOpenForwardOnly, adLockReadOnly
    If Not adores.EOF Then
        If adores(0).Value > 0 Then
            sConsulta = "UPDATE PRES3_NIV1 SET FECACT=GETDATE() WHERE  FECACT IS NULL"
            oADOConnection.Execute sConsulta
        End If
    End If
    adores.Close

    sConsulta = "SELECT count(*) FROM PRES3_NIV2 WHERE FECACT IS NULL"
    adores.Open sConsulta, oADOConnection, adOpenForwardOnly, adLockReadOnly
    If Not adores.EOF Then
        If adores(0).Value > 0 Then
            sConsulta = "UPDATE PRES3_NIV2 SET FECACT=GETDATE() WHERE  FECACT IS NULL"
            oADOConnection.Execute sConsulta
        End If
    End If
    adores.Close

    sConsulta = "SELECT count(*) FROM PRES3_NIV3 WHERE FECACT IS NULL"
    adores.Open sConsulta, oADOConnection, adOpenForwardOnly, adLockReadOnly
    If Not adores.EOF Then
        If adores(0).Value > 0 Then
            sConsulta = "UPDATE PRES3_NIV3 SET FECACT=GETDATE() WHERE  FECACT IS NULL"
            oADOConnection.Execute sConsulta
        End If
    End If
    adores.Close

    sConsulta = "SELECT count(*) FROM PRES3_NIV4 WHERE FECACT IS NULL"
    adores.Open sConsulta, oADOConnection, adOpenForwardOnly, adLockReadOnly
    If Not adores.EOF Then
        If adores(0).Value > 0 Then
            sConsulta = "UPDATE PRES3_NIV4 SET FECACT=GETDATE() WHERE  FECACT IS NULL"
            oADOConnection.Execute sConsulta
        End If
    End If
    adores.Close
    'PRESUPUESTOS 4
    sConsulta = "SELECT count(*) FROM PRES4_NIV1 WHERE FECACT IS NULL"
    adores.Open sConsulta, oADOConnection, adOpenForwardOnly, adLockReadOnly
    If Not adores.EOF Then
        If adores(0).Value > 0 Then
            sConsulta = "UPDATE PRES4_NIV1 SET FECACT=GETDATE() WHERE  FECACT IS NULL"
            oADOConnection.Execute sConsulta
        End If
    End If
    adores.Close

    sConsulta = "SELECT count(*) FROM PRES4_NIV2 WHERE FECACT IS NULL"
    adores.Open sConsulta, oADOConnection, adOpenForwardOnly, adLockReadOnly
    If Not adores.EOF Then
        If adores(0).Value > 0 Then
            sConsulta = "UPDATE PRES4_NIV2 SET FECACT=GETDATE() WHERE  FECACT IS NULL"
            oADOConnection.Execute sConsulta
        End If
    End If
    adores.Close

    sConsulta = "SELECT count(*) FROM PRES4_NIV3 WHERE FECACT IS NULL"
    adores.Open sConsulta, oADOConnection, adOpenForwardOnly, adLockReadOnly
    If Not adores.EOF Then
        If adores(0).Value > 0 Then
            sConsulta = "UPDATE PRES4_NIV3 SET FECACT=GETDATE() WHERE  FECACT IS NULL"
            oADOConnection.Execute sConsulta
        End If
    End If
    adores.Close

    sConsulta = "SELECT count(*) FROM PRES4_NIV4 WHERE FECACT IS NULL"
    adores.Open sConsulta, oADOConnection, adOpenForwardOnly, adLockReadOnly
    If Not adores.EOF Then
        If adores(0).Value > 0 Then
            sConsulta = "UPDATE PRES4_NIV4 SET FECACT=GETDATE() WHERE  FECACT IS NULL"
            oADOConnection.Execute sConsulta
        End If
    End If
    adores.Close
    
    'VISTAS
    sConsulta = "UPDATE CONF_VISTAS_ALL SET ANCHO_FILA=480 WHERE ANCHO_FILA IS NULL"
    oADOConnection.Execute sConsulta
    
    sConsulta = "UPDATE CONF_VISTAS_ALL SET PRECIO_PROV_WIDTH=1600 WHERE PRECIO_PROV_WIDTH IS NULL"
    oADOConnection.Execute sConsulta
    
    sConsulta = "UPDATE CONF_VISTAS_ALL SET CANT_PROV_WIDTH=900 WHERE CANT_PROV_WIDTH IS NULL"
    oADOConnection.Execute sConsulta
    
    sConsulta = "UPDATE CONF_VISTAS_ALL SET ADJ_PROV_WIDTH=700 WHERE ADJ_PROV_WIDTH IS NULL"
    oADOConnection.Execute sConsulta
    
    sConsulta = "UPDATE CONF_VISTAS_ITEM SET ANCHO_FILA=269.8583 WHERE ANCHO_FILA IS NULL"
    oADOConnection.Execute sConsulta

    sConsulta = "UPDATE VERSION SET NUM ='2.11.50.01'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    oADOConnection.Execute sConsulta

    oADOConnection.Execute "COMMIT TRANSACTION"
    V_2_11_5_Update_01 = True
        
Final:
    On Error Resume Next
    adores.Close
    Set adores = Nothing
    oADOConnection.Close
    Set oADOConnection = Nothing

    Exit Function


error:
    MsgBox oADOConnection.Errors(0).NativeError & "  " & oADOConnection.Errors(0).Description, vbCritical, "FULLSTEP"
    oADOConnection.Execute "ROLLBACK TRANSACTION"
    V_2_11_5_Update_01 = False
    Resume Final
End Function

Public Function CodigoDeActualizacion2_11_50_01A2_11_50_02() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim bBool As Boolean

On Error GoTo error
    

sConsulta = "BEGIN TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = True
ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

frmProgreso.lblDetalle = "Modificamos tablas"
frmProgreso.lblDetalle.Refresh
frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_11_5_TablasModif_02

frmProgreso.lblDetalle = "Modificamos stored procedures 1"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_11_5_Storeds1_02

frmProgreso.lblDetalle = "Modificamos stored procedures 2"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_11_5_Storeds2_02

frmProgreso.lblDetalle = "Modificamos stored procedures 3"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_11_5_Storeds3_02

frmProgreso.lblDetalle = "Modificamos vistas"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_11_5_Updated_02

sConsulta = "UPDATE VERSION SET NUM ='2.11.50.02'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "COMMIT TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = False

CodigoDeActualizacion2_11_50_01A2_11_50_02 = True
Exit Function
    
error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_11_50_01A2_11_50_02 = False

End Function

Public Function CodigoDeActualizacion2_11_50_02A2_11_50_03() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim bBool As Boolean

On Error GoTo error
    

sConsulta = "BEGIN TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = True
ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

frmProgreso.lblDetalle = "Modificamos stored de traspaso de ofertas"
frmProgreso.lblDetalle.Refresh
frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_11_5_Storeds_03


sConsulta = "UPDATE VERSION SET NUM ='2.11.50.03'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "COMMIT TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = False

CodigoDeActualizacion2_11_50_02A2_11_50_03 = True
Exit Function
    
error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_11_50_02A2_11_50_03 = False

End Function

Private Sub V_2_11_5_Storeds_03()
Dim sConsulta As String

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_TRASPASAR_OFERTA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_TRASPASAR_OFERTA]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE SP_TRASPASAR_OFERTA @ID int , @NUM INT OUTPUT, @NUMNUE INT OUTPUT, @NUMAREV INT OUTPUT, @ERROR SMALLINT = NULL OUTPUT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @NEWOFE INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @NUMOFEWEB AS int" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN1 VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCE INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVE VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @MON VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @ADJUN INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDADJUN INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDGS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @GRUPO VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @ITEM INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @NUMOBJ INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @NUMOBJGS INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @ERROR = 0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @ANYO = ANYO, @GMN1= GMN1, @PROCE = PROCE, @PROVE = PROVE, @MON = MON, @NUMOBJ = NUMOBJ" & vbCrLf
sConsulta = sConsulta & "  FROM P_PROCE_OFE " & vbCrLf
sConsulta = sConsulta & " WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @NUMOBJGS = (SELECT NUMOBJ FROM PROCE_PROVE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE)" & vbCrLf
sConsulta = sConsulta & "IF @NUMOBJGS  <=@NUMOBJ" & vbCrLf
sConsulta = sConsulta & "  UPDATE PROCE_PROVE SET OBJNUEVOS=0 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "/*" & vbCrLf
sConsulta = sConsulta & "DECLARE @EQUIV FLOAT " & vbCrLf
sConsulta = sConsulta & "SELECT @EQUIV = (MON.EQUIV / PROCE.CAMBIO) " & vbCrLf
sConsulta = sConsulta & "  FROM PROCE " & vbCrLf
sConsulta = sConsulta & "     INNER JOIN MON " & vbCrLf
sConsulta = sConsulta & "             ON PROCE.ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "            AND PROCE.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND PROCE.COD=@PROCE  " & vbCrLf
sConsulta = sConsulta & "            AND MON.COD=@MON " & vbCrLf
sConsulta = sConsulta & "*/" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @SUBASTA INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @SUBASTA = SUBASTA " & vbCrLf
sConsulta = sConsulta & "  FROM PROCE_DEF " & vbCrLf
sConsulta = sConsulta & " WHERE ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "   AND GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "   AND PROCE=@PROCE  " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @SUBASTA = 1" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    DECLARE @OFE INT" & vbCrLf
sConsulta = sConsulta & "    SET @OFE = ( SELECT MAX(OFE) FROM PROCE_OFE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE AND OFE!=-1)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    IF @OFE IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "      BEGIN" & vbCrLf
sConsulta = sConsulta & "        INSERT INTO P_OFE_GR_ATRIB (ID, GRUPO, ATRIB_ID,  VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL)" & vbCrLf
sConsulta = sConsulta & "        SELECT @ID, O.GRUPO, O.ATRIB_ID, O.VALOR_NUM, O.VALOR_TEXT, O.VALOR_FEC, O.VALOR_BOOL" & vbCrLf
sConsulta = sConsulta & "          FROM OFE_GR_ATRIB O" & vbCrLf
sConsulta = sConsulta & "         WHERE O.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "           AND O.GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "           AND O.PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "           AND O.PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "           AND O.OFE = @OFE" & vbCrLf
sConsulta = sConsulta & "           AND NOT EXISTS (SELECT * FROM P_OFE_GR_ATRIB T WHERE T.ID = @ID AND T.GRUPO = O.GRUPO AND T.ATRIB_ID = O.ATRIB_ID)" & vbCrLf
sConsulta = sConsulta & "        IF @@ERROR!=0 GOTO ELIMINAR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "        INSERT INTO P_OFE_ITEM_ATRIB (ID, GRUPO, ITEM, ATRIB_ID,  VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL)" & vbCrLf
sConsulta = sConsulta & "        SELECT @ID, I.GRUPO, IA.ITEM, ATRIB_ID, VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL" & vbCrLf
sConsulta = sConsulta & "          FROM OFE_ITEM_ATRIB IA" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN ITEM I" & vbCrLf
sConsulta = sConsulta & "                   ON I.ANYO = IA.ANYO" & vbCrLf
sConsulta = sConsulta & "                  AND I.GMN1 = IA.GMN1" & vbCrLf
sConsulta = sConsulta & "                  AND I.PROCE = IA.PROCE" & vbCrLf
sConsulta = sConsulta & "                  AND I.ID = IA.ITEM" & vbCrLf
sConsulta = sConsulta & "         WHERE IA.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "           AND IA.GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "           AND IA.PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "           AND IA.PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "           AND IA.OFE = @OFE" & vbCrLf
sConsulta = sConsulta & "           AND NOT EXISTS (SELECT * FROM P_OFE_ITEM_ATRIB T WHERE T.ID = @ID AND T.ATRIB_ID = IA.ATRIB_ID AND T.ITEM = IA.ITEM)" & vbCrLf
sConsulta = sConsulta & "        IF @@ERROR!=0 GOTO ELIMINAR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "        INSERT INTO P_OFE_ATRIB (ID, ATRIB_ID,  VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL)" & vbCrLf
sConsulta = sConsulta & "        SELECT @ID, O.ATRIB_ID, O.VALOR_NUM, O.VALOR_TEXT, O.VALOR_FEC, O.VALOR_BOOL" & vbCrLf
sConsulta = sConsulta & "          FROM OFE_ATRIB O" & vbCrLf
sConsulta = sConsulta & "         WHERE O.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "           AND O.GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "           AND O.PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "           AND O.PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "           AND O.OFE = @OFE" & vbCrLf
sConsulta = sConsulta & "           AND NOT EXISTS (SELECT * FROM P_OFE_ATRIB T WHERE T.ID = @ID AND T.ATRIB_ID = O.ATRIB_ID)" & vbCrLf
sConsulta = sConsulta & "        IF @@ERROR!=0 GOTO ELIMINAR_" & vbCrLf
sConsulta = sConsulta & "      END " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "      DECLARE @OFEATRIB BIT" & vbCrLf
sConsulta = sConsulta & "      DECLARE @OFEGRUPOATRIB BIT" & vbCrLf
sConsulta = sConsulta & "      DECLARE @OFEITEMATRIB BIT" & vbCrLf
sConsulta = sConsulta & "      DECLARE @PAID INT" & vbCrLf
sConsulta = sConsulta & "      DECLARE @AMBITO SMALLINT" & vbCrLf
sConsulta = sConsulta & "      DECLARE @TIPO SMALLINT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "      DECLARE CATRIBS CURSOR LOCAL FORWARD_ONLY  FOR " & vbCrLf
sConsulta = sConsulta & "      SELECT PA.ID, AMBITO, GRUPO, TIPO" & vbCrLf
sConsulta = sConsulta & "        FROM PROCE_ATRIB PA" & vbCrLf
sConsulta = sConsulta & "         INNER JOIN DEF_ATRIB DA" & vbCrLf
sConsulta = sConsulta & "                 ON PA.ATRIB = DA.ID" & vbCrLf
sConsulta = sConsulta & "       WHERE ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "         AND GMN1= @GMN1" & vbCrLf
sConsulta = sConsulta & "         AND PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "         AND INTERNO = 0" & vbCrLf
sConsulta = sConsulta & "         AND OBLIG = 1" & vbCrLf
sConsulta = sConsulta & "      " & vbCrLf
sConsulta = sConsulta & "      OPEN CATRIBS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "      FETCH NEXT FROM CATRIBS INTO @PAID, @AMBITO, @GRUPO,  @TIPO" & vbCrLf
sConsulta = sConsulta & "      SET @OFEATRIB=0" & vbCrLf
sConsulta = sConsulta & "      SET @OFEGRUPOATRIB=0" & vbCrLf
sConsulta = sConsulta & "      SET @OFEITEMATRIB=0" & vbCrLf
sConsulta = sConsulta & "      WHILE @@FETCH_STATUS=0 AND @OFEATRIB=0 AND @OFEGRUPOATRIB=0 AND @OFEITEMATRIB=0" & vbCrLf
sConsulta = sConsulta & "        BEGIN" & vbCrLf
sConsulta = sConsulta & "          IF @AMBITO = 1" & vbCrLf
sConsulta = sConsulta & "            BEGIN" & vbCrLf
sConsulta = sConsulta & "              SET @OFEATRIB =1" & vbCrLf
sConsulta = sConsulta & "              SELECT @OFEATRIB = CASE " & vbCrLf
sConsulta = sConsulta & "                        WHEN CASE @TIPO " & vbCrLf
sConsulta = sConsulta & "                                WHEN 1 THEN CASE WHEN VALOR_TEXT ='' THEN NULL ELSE VALOR_TEXT END " & vbCrLf
sConsulta = sConsulta & "                                WHEN 2 THEN CAST(VALOR_NUM AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                                WHEN 3 THEN CAST(VALOR_FEC AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                                WHEN 4 THEN CAST(VALOR_BOOL AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                             END IS NULL " & vbCrLf
sConsulta = sConsulta & "                        THEN 1 " & vbCrLf
sConsulta = sConsulta & "                        ELSE 0 " & vbCrLf
sConsulta = sConsulta & "                     END" & vbCrLf
sConsulta = sConsulta & "                FROM P_OFE_ATRIB" & vbCrLf
sConsulta = sConsulta & "               WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "                 AND ATRIB_ID = @PAID" & vbCrLf
sConsulta = sConsulta & "            END" & vbCrLf
sConsulta = sConsulta & "          IF @AMBITO = 2" & vbCrLf
sConsulta = sConsulta & "            BEGIN" & vbCrLf
sConsulta = sConsulta & "              SET @OFEGRUPOATRIB =0" & vbCrLf
sConsulta = sConsulta & "              IF @GRUPO IS NULL " & vbCrLf
sConsulta = sConsulta & "                BEGIN" & vbCrLf
sConsulta = sConsulta & "                IF (SELECT COUNT(*) " & vbCrLf
sConsulta = sConsulta & "                      FROM P_OFE_GR_ATRIB " & vbCrLf
sConsulta = sConsulta & "                     WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "                       AND ATRIB_ID = @PAID" & vbCrLf
sConsulta = sConsulta & "                       AND CASE @TIPO " & vbCrLf
sConsulta = sConsulta & "                                WHEN 1 THEN CASE WHEN VALOR_TEXT ='' THEN NULL ELSE VALOR_TEXT END " & vbCrLf
sConsulta = sConsulta & "                                WHEN 2 THEN CAST(VALOR_NUM AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                                WHEN 3 THEN CAST(VALOR_FEC AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                                WHEN 4 THEN CAST(VALOR_BOOL AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                             END IS NOT NULL) <> (SELECT COUNT(*) " & vbCrLf
sConsulta = sConsulta & "                       FROM PROCE_GRUPO " & vbCrLf
sConsulta = sConsulta & "                      WHERE ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "                        AND GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "                        AND PROCE = @PROCE)" & vbCrLf
sConsulta = sConsulta & "                   SET @OFEGRUPOATRIB = 1" & vbCrLf
sConsulta = sConsulta & "                 END" & vbCrLf
sConsulta = sConsulta & "               ELSE" & vbCrLf
sConsulta = sConsulta & "                IF (SELECT COUNT(*) " & vbCrLf
sConsulta = sConsulta & "                      FROM P_OFE_GR_ATRIB " & vbCrLf
sConsulta = sConsulta & "                     WHERE GRUPO = @GRUPO" & vbCrLf
sConsulta = sConsulta & "                       AND ATRIB_ID = @PAID" & vbCrLf
sConsulta = sConsulta & "                       AND CASE @TIPO " & vbCrLf
sConsulta = sConsulta & "                                WHEN 1 THEN CASE WHEN VALOR_TEXT ='' THEN NULL ELSE VALOR_TEXT END " & vbCrLf
sConsulta = sConsulta & "                                WHEN 2 THEN CAST(VALOR_NUM AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                                WHEN 3 THEN CAST(VALOR_FEC AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                                WHEN 4 THEN CAST(VALOR_BOOL AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                             END IS NOT NULL)  =0 " & vbCrLf
sConsulta = sConsulta & "                       SET @OFEGRUPOATRIB = 1" & vbCrLf
sConsulta = sConsulta & "            END" & vbCrLf
sConsulta = sConsulta & "          IF @AMBITO = 3" & vbCrLf
sConsulta = sConsulta & "            BEGIN" & vbCrLf
sConsulta = sConsulta & "              SET @OFEITEMATRIB =0" & vbCrLf
sConsulta = sConsulta & "              IF @GRUPO IS NULL " & vbCrLf
sConsulta = sConsulta & "                BEGIN" & vbCrLf
sConsulta = sConsulta & "                  IF (SELECT COUNT(*) " & vbCrLf
sConsulta = sConsulta & "                        FROM P_OFE_ITEM_ATRIB " & vbCrLf
sConsulta = sConsulta & "                       WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "                         AND ATRIB_ID = @PAID" & vbCrLf
sConsulta = sConsulta & "                         AND CASE @TIPO " & vbCrLf
sConsulta = sConsulta & "                                WHEN 1 THEN CASE WHEN VALOR_TEXT ='' THEN NULL ELSE VALOR_TEXT END " & vbCrLf
sConsulta = sConsulta & "                                WHEN 2 THEN CAST(VALOR_NUM AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                                WHEN 3 THEN CAST(VALOR_FEC AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                                WHEN 4 THEN CAST(VALOR_BOOL AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                     END IS NOT NULL)  <>" & vbCrLf
sConsulta = sConsulta & "                      (SELECT COUNT(*) " & vbCrLf
sConsulta = sConsulta & "                         FROM ITEM " & vbCrLf
sConsulta = sConsulta & "                        WHERE ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "                          AND GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "                          AND PROCE = @PROCE                    " & vbCrLf
sConsulta = sConsulta & "                          AND CONF = 1)" & vbCrLf
sConsulta = sConsulta & "                       SET @OFEITEMATRIB = 1" & vbCrLf
sConsulta = sConsulta & "                 END" & vbCrLf
sConsulta = sConsulta & "              ELSE" & vbCrLf
sConsulta = sConsulta & "                 begin" & vbCrLf
sConsulta = sConsulta & "                  IF (SELECT COUNT(*) " & vbCrLf
sConsulta = sConsulta & "                        FROM P_OFE_ITEM_ATRIB" & vbCrLf
sConsulta = sConsulta & "                       WHERE GRUPO = @GRUPO" & vbCrLf
sConsulta = sConsulta & "                         AND ID = @ID" & vbCrLf
sConsulta = sConsulta & "                         AND ATRIB_ID = @PAID" & vbCrLf
sConsulta = sConsulta & "                         AND CASE @TIPO " & vbCrLf
sConsulta = sConsulta & "                                WHEN 1 THEN CASE WHEN VALOR_TEXT ='' THEN NULL ELSE VALOR_TEXT END " & vbCrLf
sConsulta = sConsulta & "                                WHEN 2 THEN CAST(VALOR_NUM AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                                WHEN 3 THEN CAST(VALOR_FEC AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                                WHEN 4 THEN CAST(VALOR_BOOL AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                             END IS NOT NULL)  <>" & vbCrLf
sConsulta = sConsulta & "                      (SELECT COUNT(*) " & vbCrLf
sConsulta = sConsulta & "                         FROM ITEM " & vbCrLf
sConsulta = sConsulta & "                        WHERE ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "                          AND GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "                          AND PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "                          AND GRUPO = @GRUPO" & vbCrLf
sConsulta = sConsulta & "                          AND CONF = 1)" & vbCrLf
sConsulta = sConsulta & "                       SET @OFEITEMATRIB = 1" & vbCrLf
sConsulta = sConsulta & "                   end" & vbCrLf
sConsulta = sConsulta & "            END" & vbCrLf
sConsulta = sConsulta & "            FETCH NEXT FROM CATRIBS INTO @PAID, @AMBITO, @GRUPO,  @TIPO" & vbCrLf
sConsulta = sConsulta & "          END" & vbCrLf
sConsulta = sConsulta & "      CLOSE CATRIBS" & vbCrLf
sConsulta = sConsulta & "      DEALLOCATE CATRIBS" & vbCrLf
sConsulta = sConsulta & "      IF @OFEATRIB = 1" & vbCrLf
sConsulta = sConsulta & "        SET @ERROR = 2" & vbCrLf
sConsulta = sConsulta & "      IF @OFEGRUPOATRIB = 1" & vbCrLf
sConsulta = sConsulta & "        SET @ERROR = 3" & vbCrLf
sConsulta = sConsulta & "      IF @OFEITEMATRIB = 1" & vbCrLf
sConsulta = sConsulta & "        SET @ERROR = 4" & vbCrLf
sConsulta = sConsulta & "      IF @ERROR>0" & vbCrLf
sConsulta = sConsulta & "        GOTO ELIMINAR_" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "DECLARE  @ESTWEB VARCHAR(3) " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "SELECT @ESTWEB=ESTWEB FROM PARGEN_DEF INNER JOIN PARGEN_GEST ON PARGEN_DEF.ID=PARGEN_GEST.ID  " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN TRANSACTION" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE SET ULT= 0, LEIDO = 1  WHERE ANYO = @ANYO AND GMN1 = @GMN1 AND PROCE = @PROCE AND PROVE = @PROVE AND ULT = 1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @NEWOFE = ISNULL(MAX(OFE),0) + 1 FROM PROCE_OFE WHERE ANYO = @ANYO AND GMN1 = @GMN1 AND PROCE = @PROCE AND PROVE = @PROVE AND OFE != -1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO PROCE_OFE(ANYO, GMN1, PROCE, PROVE, OFE, EST, FECREC, FECVAL, MON, CAMBIO, OBS, LEIDO, ULT, ENVIADA, OBSADJUN)" & vbCrLf
sConsulta = sConsulta & "SELECT @ANYO, @GMN1, @PROCE, @PROVE, @NEWOFE, @ESTWEB, FECREC, FECVAL, MON, CAMBIO, OBS, 0, 1, 1, OBSADJUN" & vbCrLf
sConsulta = sConsulta & "  FROM P_PROCE_OFE" & vbCrLf
sConsulta = sConsulta & "WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE GRUPO_OFE " & vbCrLf
sConsulta = sConsulta & "   SET OBSADJUN = PGO.OBSADJUN" & vbCrLf
sConsulta = sConsulta & "  FROM GRUPO_OFE GO" & vbCrLf
sConsulta = sConsulta & "      INNER JOIN P_GRUPO_OFE PGO" & vbCrLf
sConsulta = sConsulta & "              ON GO.GRUPO = PGO.GRUPO" & vbCrLf
sConsulta = sConsulta & " WHERE GO.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "   AND GO.GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "   AND GO.PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "   AND GO.PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "   AND GO.OFE = @NEWOFE" & vbCrLf
sConsulta = sConsulta & "   AND PGO.ID = @ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_OFE" & vbCrLf
sConsulta = sConsulta & "   SET PRECIO = PIO.PRECIO," & vbCrLf
sConsulta = sConsulta & "       CANTMAX = PIO.CANTMAX," & vbCrLf
sConsulta = sConsulta & "       PRECIO2 = PIO.PRECIO2," & vbCrLf
sConsulta = sConsulta & "       PRECIO3 = PIO.PRECIO3," & vbCrLf
sConsulta = sConsulta & "       PREC_VALIDO = PIO.PRECIO," & vbCrLf
sConsulta = sConsulta & "       OBSADJUN = PIO.OBSADJUN," & vbCrLf
sConsulta = sConsulta & "       COMENT1 = PIO.COMENT1," & vbCrLf
sConsulta = sConsulta & "       COMENT2 = PIO.COMENT2," & vbCrLf
sConsulta = sConsulta & "       COMENT3 = PIO.COMENT3" & vbCrLf
sConsulta = sConsulta & "  FROM ITEM_OFE IO" & vbCrLf
sConsulta = sConsulta & "     INNER JOIN P_ITEM_OFE PIO" & vbCrLf
sConsulta = sConsulta & "             ON IO.ITEM = PIO.ITEM" & vbCrLf
sConsulta = sConsulta & " WHERE IO.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "   AND IO.GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "   AND IO.PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "   AND IO.PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "   AND IO.NUM = @NEWOFE" & vbCrLf
sConsulta = sConsulta & "   AND PIO.ID = @ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_ITEM_ADJUN " & vbCrLf
sConsulta = sConsulta & "  FROM OFE_ITEM_ADJUN R" & vbCrLf
sConsulta = sConsulta & " WHERE R.ANYO = @ANYO " & vbCrLf
sConsulta = sConsulta & "   AND R.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "   AND R.PROCE= @PROCE " & vbCrLf
sConsulta = sConsulta & "   AND R.PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "   AND R.OFE IN (@NEWOFE,-1)" & vbCrLf
sConsulta = sConsulta & "   AND EXISTS (SELECT * FROM P_OFE_ITEM_ADJUN P " & vbCrLf
sConsulta = sConsulta & "                WHERE P.ID  = @ID " & vbCrLf
sConsulta = sConsulta & "                  AND P.ITEM = R.ITEM" & vbCrLf
sConsulta = sConsulta & "                  AND P.IDGS = R.ID)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_ITEM_ATRIB " & vbCrLf
sConsulta = sConsulta & "  FROM OFE_ITEM_ATRIB R" & vbCrLf
sConsulta = sConsulta & " WHERE R.ANYO = @ANYO " & vbCrLf
sConsulta = sConsulta & "   AND R.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "   AND R.PROCE= @PROCE " & vbCrLf
sConsulta = sConsulta & "   AND R.PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "   AND R.OFE IN (@NEWOFE,-1)" & vbCrLf
sConsulta = sConsulta & "   AND EXISTS (SELECT * FROM P_OFE_ITEM_ATRIB P " & vbCrLf
sConsulta = sConsulta & "                WHERE P.ID  = @ID " & vbCrLf
sConsulta = sConsulta & "                  AND P.ITEM = R.ITEM" & vbCrLf
sConsulta = sConsulta & "                  AND P.ATRIB_ID = R.ATRIB_ID)" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_GR_ATRIB " & vbCrLf
sConsulta = sConsulta & "  FROM OFE_GR_ATRIB R" & vbCrLf
sConsulta = sConsulta & " WHERE R.ANYO = @ANYO " & vbCrLf
sConsulta = sConsulta & "   AND R.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "   AND R.PROCE= @PROCE " & vbCrLf
sConsulta = sConsulta & "   AND R.PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "   AND R.OFE IN (@NEWOFE,-1)" & vbCrLf
sConsulta = sConsulta & "   AND EXISTS (SELECT * FROM P_OFE_GR_ATRIB P " & vbCrLf
sConsulta = sConsulta & "                WHERE P.ID  = @ID " & vbCrLf
sConsulta = sConsulta & "                  AND CAST(P.GRUPO AS VARCHAR(50)) = CAST(R.GRUPO AS VARCHAR(50))" & vbCrLf
sConsulta = sConsulta & "                  AND P.ATRIB_ID = R.ATRIB_ID)" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_GR_ADJUN " & vbCrLf
sConsulta = sConsulta & "  FROM OFE_GR_ADJUN R" & vbCrLf
sConsulta = sConsulta & " WHERE R.ANYO = @ANYO " & vbCrLf
sConsulta = sConsulta & "   AND R.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "   AND R.PROCE= @PROCE " & vbCrLf
sConsulta = sConsulta & "   AND R.PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "   AND R.OFE IN (@NEWOFE,-1)" & vbCrLf
sConsulta = sConsulta & "   AND EXISTS (SELECT * FROM P_OFE_GR_ADJUN P " & vbCrLf
sConsulta = sConsulta & "                WHERE P.ID  = @ID " & vbCrLf
sConsulta = sConsulta & "                  AND P.GRUPO = R.GRUPO" & vbCrLf
sConsulta = sConsulta & "                  AND P.IDGS = R.ID)" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_ATRIB " & vbCrLf
sConsulta = sConsulta & "  FROM OFE_ATRIB R" & vbCrLf
sConsulta = sConsulta & " WHERE R.ANYO = @ANYO " & vbCrLf
sConsulta = sConsulta & "   AND R.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "   AND R.PROCE= @PROCE " & vbCrLf
sConsulta = sConsulta & "   AND R.PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "   AND R.OFE IN (@NEWOFE,-1)" & vbCrLf
sConsulta = sConsulta & "   AND EXISTS (SELECT * FROM P_OFE_ATRIB P " & vbCrLf
sConsulta = sConsulta & "                WHERE P.ID  = @ID " & vbCrLf
sConsulta = sConsulta & "                  AND P.ATRIB_ID = R.ATRIB_ID)" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM PROCE_OFE_ADJUN " & vbCrLf
sConsulta = sConsulta & "  FROM PROCE_OFE_ADJUN R" & vbCrLf
sConsulta = sConsulta & " WHERE R.ANYO = @ANYO " & vbCrLf
sConsulta = sConsulta & "   AND R.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "   AND R.PROCE= @PROCE " & vbCrLf
sConsulta = sConsulta & "   AND R.PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "   AND R.OFE IN (@NEWOFE,-1)" & vbCrLf
sConsulta = sConsulta & "   AND EXISTS (SELECT * FROM P_PROCE_OFE_ADJUN P " & vbCrLf
sConsulta = sConsulta & "                WHERE P.ID  = @ID " & vbCrLf
sConsulta = sConsulta & "                  AND P.IDGS = R.ID)" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM PROCE_OFE_ADJUN WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE= @PROCE and PROVE=@PROVE AND OFE = -1" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_GR_ADJUN WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE= @PROCE and PROVE=@PROVE AND OFE = -1" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_ITEM_ADJUN WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE= @PROCE and PROVE=@PROVE AND OFE = -1" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_ATRIB WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE= @PROCE and PROVE=@PROVE AND OFE = -1" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_GR_ATRIB WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE= @PROCE and PROVE=@PROVE AND OFE = -1" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_ITEM_ATRIB WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE= @PROCE and PROVE=@PROVE AND OFE = -1" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM ITEM_OFE WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE= @PROCE and PROVE=@PROVE AND NUM = -1" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM GRUPO_OFE WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE= @PROCE and PROVE=@PROVE AND OFE = -1" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM PROCE_OFE WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE= @PROCE and PROVE=@PROVE AND OFE = -1" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR LOCAL FOR" & vbCrLf
sConsulta = sConsulta & "SELECT ADJUN, IDGS FROM P_PROCE_OFE_ADJUN WHERE ID=@ID AND IDGS IS NULL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "OPEN C" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C INTO @ADJUN, @IDGS" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    SELECT @IDADJUN = isnull(MAX(ID),0) + 1 FROM ADJUN " & vbCrLf
sConsulta = sConsulta & "    INSERT INTO ADJUN (ID, WEB) VALUES (@IDADJUN, 1) " & vbCrLf
sConsulta = sConsulta & "    UPDATE P_PROCE_OFE_ADJUN SET IDGS = @IDADJUN WHERE ID = @ID AND ADJUN = @ADJUN" & vbCrLf
sConsulta = sConsulta & "    IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "    FETCH NEXT FROM C INTO @ADJUN, @IDGS" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "CLOSE C" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO PROCE_OFE_ADJUN(ANYO, GMN1, PROCE, PROVE, OFE, ID, NOM, COM, IDPORTAL, DATASIZE)" & vbCrLf
sConsulta = sConsulta & "SELECT @ANYO, @GMN1, @PROCE, @PROVE, @NEWOFE, POA.IDGS, POA.NOM, POA.COM, POA.IDPORTAL, POA.DATASIZE" & vbCrLf
sConsulta = sConsulta & "  FROM P_PROCE_OFE_ADJUN POA" & vbCrLf
sConsulta = sConsulta & "         INNER JOIN ADJUN A" & vbCrLf
sConsulta = sConsulta & "                 ON POA.IDGS = A.ID" & vbCrLf
sConsulta = sConsulta & "WHERE POA.ID = @ID" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR LOCAL FOR" & vbCrLf
sConsulta = sConsulta & "SELECT ADJUN, IDGS, GRUPO FROM P_OFE_GR_ADJUN WHERE ID=@ID AND IDGS IS NULL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "OPEN C" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C INTO @ADJUN, @IDGS, @GRUPO" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    SELECT @IDADJUN = isnull(MAX(ID),0) + 1 FROM ADJUN " & vbCrLf
sConsulta = sConsulta & "    INSERT INTO ADJUN (ID, WEB) VALUES (@IDADJUN, 1) " & vbCrLf
sConsulta = sConsulta & "    UPDATE P_OFE_GR_ADJUN SET IDGS = @IDADJUN WHERE ID = @ID AND ADJUN = @ADJUN AND GRUPO = @GRUPO" & vbCrLf
sConsulta = sConsulta & "    IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "    FETCH NEXT FROM C INTO @ADJUN, @IDGS, @GRUPO" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "CLOSE C" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO OFE_GR_ADJUN(ANYO, GMN1, PROCE, GRUPO, PROVE, OFE, ID, NOM, COM, IDPORTAL, DATASIZE)" & vbCrLf
sConsulta = sConsulta & "SELECT @ANYO, @GMN1, @PROCE, OGA.GRUPO, @PROVE, @NEWOFE, OGA.IDGS, OGA.NOM, OGA.COM, OGA.IDPORTAL, OGA.DATASIZE" & vbCrLf
sConsulta = sConsulta & "  FROM P_OFE_GR_ADJUN OGA" & vbCrLf
sConsulta = sConsulta & "         INNER JOIN ADJUN A" & vbCrLf
sConsulta = sConsulta & "                 ON OGA.IDGS = A.ID" & vbCrLf
sConsulta = sConsulta & " WHERE OGA.ID = @ID" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR LOCAL FOR" & vbCrLf
sConsulta = sConsulta & "SELECT ADJUN, IDGS, ITEM FROM P_OFE_ITEM_ADJUN WHERE ID=@ID AND IDGS IS NULL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "OPEN C" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C INTO @ADJUN, @IDGS, @ITEM" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    SELECT @IDADJUN = isnull(MAX(ID),0) + 1 FROM ADJUN " & vbCrLf
sConsulta = sConsulta & "    INSERT INTO ADJUN (ID, WEB) VALUES (@IDADJUN, 1) " & vbCrLf
sConsulta = sConsulta & "    UPDATE P_OFE_ITEM_ADJUN SET IDGS = @IDADJUN WHERE ID = @ID AND ADJUN = @ADJUN AND ITEM = @ITEM" & vbCrLf
sConsulta = sConsulta & "    IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "    FETCH NEXT FROM C INTO @ADJUN, @IDGS, @ITEM" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "CLOSE C" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO OFE_ITEM_ADJUN(ANYO, GMN1, PROCE, ITEM, PROVE, OFE, ID, NOM, COM, IDPORTAL, DATASIZE)" & vbCrLf
sConsulta = sConsulta & "SELECT @ANYO, @GMN1, @PROCE, OIA.ITEM, @PROVE, @NEWOFE, OIA.IDGS, OIA.NOM, OIA.COM, OIA.IDPORTAL, OIA.DATASIZE" & vbCrLf
sConsulta = sConsulta & "  FROM P_OFE_ITEM_ADJUN OIA" & vbCrLf
sConsulta = sConsulta & "         INNER JOIN ADJUN A" & vbCrLf
sConsulta = sConsulta & "                 ON OIA.IDGS = A.ID" & vbCrLf
sConsulta = sConsulta & "WHERE OIA.ID = @ID" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO OFE_GR_ATRIB(ANYO, GMN1, PROCE, GRUPO, PROVE, OFE, ATRIB_ID, VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL)" & vbCrLf
sConsulta = sConsulta & "SELECT @ANYO, @GMN1, @PROCE, GRUPO, @PROVE, @NEWOFE, ATRIB_ID, VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL" & vbCrLf
sConsulta = sConsulta & "  FROM P_OFE_GR_ATRIB" & vbCrLf
sConsulta = sConsulta & " WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO OFE_ITEM_ATRIB(ANYO, GMN1, PROCE, ITEM, PROVE, OFE, ATRIB_ID, VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL)" & vbCrLf
sConsulta = sConsulta & "SELECT @ANYO, @GMN1, @PROCE, ITEM, @PROVE, @NEWOFE, ATRIB_ID, VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL" & vbCrLf
sConsulta = sConsulta & "  FROM P_OFE_ITEM_ATRIB" & vbCrLf
sConsulta = sConsulta & " WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO OFE_ATRIB(ANYO, GMN1, PROCE, PROVE, OFE, ATRIB_ID, VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL)" & vbCrLf
sConsulta = sConsulta & "SELECT @ANYO, @GMN1, @PROCE, @PROVE, @NEWOFE, ATRIB_ID, VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL" & vbCrLf
sConsulta = sConsulta & "  FROM P_OFE_ATRIB " & vbCrLf
sConsulta = sConsulta & " WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE SET OFE = 1  WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE = @PROCE AND PROVE=@PROVE AND OFE = 0" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @NUMOFEWEB = isnull(MAX(OFEWEB),0)+1 FROM PROCE_OFE_WEB WHERE ANYO = @ANYO AND GMN1 = @GMN1 AND PROCE = @PROCE AND PROVE = @PROVE " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO PROCE_OFE_WEB (ANYO, GMN1, PROCE, PROVE, OFEWEB, OFE, FECREC, FECVAL, MON)" & vbCrLf
sConsulta = sConsulta & "SELECT ANYO, GMN1, PROCE, PROVE, @NUMOFEWEB, OFE, FECREC,FECVAL,MON " & vbCrLf
sConsulta = sConsulta & "FROM PROCE_OFE " & vbCrLf
sConsulta = sConsulta & "WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE = @PROCE AND PROVE = @PROVE AND OFE = @NEWOFE" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO ITEM_OFE_WEB (ANYO, GMN1, PROCE, ITEM, PROVE, NUM, PRECIO, CANTMAX)" & vbCrLf
sConsulta = sConsulta & "SELECT ANYO, GMN1, PROCE, ITEM, PROVE, @NUMOFEWEB, PRECIO, CANTMAX" & vbCrLf
sConsulta = sConsulta & "FROM ITEM_OFE" & vbCrLf
sConsulta = sConsulta & "WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE = @PROCE AND PROVE = @PROVE AND NUM = @NEWOFE" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO PROCE_OFE_WEB_ADJUN (ANYO, GMN1, PROCE, PROVE, OFEWEB, ID, NOM, COM, IDPORTAL)" & vbCrLf
sConsulta = sConsulta & "SELECT ANYO, GMN1, PROCE, PROVE, @NUMOFEWEB, ID, NOM, COM, IDPORTAL" & vbCrLf
sConsulta = sConsulta & "FROM PROCE_OFE_ADJUN" & vbCrLf
sConsulta = sConsulta & "WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE = @PROCE AND PROVE = @PROVE AND OFE = @NEWOFE" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_OFE SET ULT = 0 WHERE ANYO = @ANYO AND GMN1 = @GMN1 AND PROCE =@PROCE AND PROVE=@PROVE AND NUM!=@NEWOFE" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "COMMIT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "GOTO ELIMINAR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ERROR_:" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ROLLBACK" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ELIMINAR_:" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC NUM_PROCE_PUB @PROVE =@PROVE ,@NUM = @NUM OUTPUT,@NUMNUE = @NUMNUE OUTPUT ,@NUMAREV = @NUMAREV OUTPUT " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM P_PROCE_OFE WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM P_GRUPO_OFE WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM P_ITEM_OFE WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM P_OFE_ATRIB WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM P_OFE_GR_ATRIB WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM P_OFE_ITEM_ATRIB WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM P_PROCE_OFE_ADJUN WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM P_OFE_GR_ADJUN WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM P_OFE_ITEM_ADJUN WHERE ID=@ID" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Private Sub V_2_11_5_TablasModif_02()
Dim sConsulta As String

'Tabla PARGEN_INTERNO
sConsulta = "  ALTER TABLE [dbo].[PARGEN_INTERNO] ADD [ENVIO_ASINCRONO] [tinyint] NOT NULL CONSTRAINT [DF_PARGEN_INTERNO_ENVIO_ASINCRONO] DEFAULT 1"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "  ALTER TABLE [dbo].[LOG_ART4] ALTER COLUMN [DEN] [varchar] (200) NULL"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "  ALTER TABLE [dbo].[LOG_ITEM_ADJ] ALTER COLUMN [DESCR] [varchar] (200) NULL"
ExecuteSQL gRDOCon, sConsulta

'TABLAS NUEVAS

sConsulta = "CREATE TABLE [dbo].[P_PROCE_OFE] (" & vbCrLf
sConsulta = sConsulta & "   [ID] [int] IDENTITY (1, 1) NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [ANYO] [smallint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [GMN1] [varchar] (" & gLongitudesDeCodigos.giLongCodGMN1 & ") NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [PROCE] [smallint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [PROVE] [varchar] (" & gLongitudesDeCodigos.giLongCodPROVE & ") NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [CIA_PROVE] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [USU] [varchar] (" & gLongitudesDeCodigos.giLongCodUSU & ") NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [FECREC] [datetime] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [FECVAL] [datetime] NULL ," & vbCrLf
sConsulta = sConsulta & "   [MON] [varchar] (" & gLongitudesDeCodigos.giLongCodMON & ") NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [CAMBIO] [float] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [OBS] [text] NULL ," & vbCrLf
sConsulta = sConsulta & "   [OBSADJUN] [text] NULL ," & vbCrLf
sConsulta = sConsulta & "   [NUMOBJ] [int] NULL " & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE [dbo].[P_PROCE_OFE] WITH NOCHECK ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [PK_P_PROCE_OFE] PRIMARY KEY  CLUSTERED " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )  ON [PRIMARY] " & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "CREATE TABLE [dbo].[P_GRUPO_OFE] (" & vbCrLf
sConsulta = sConsulta & "   [ID] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [GRUPO] [varchar] (" & gLongitudesDeCodigos.giLongCodGRUPO & ") NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [OBSADJUN] [text] NULL " & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE [dbo].[P_GRUPO_OFE] WITH NOCHECK ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [PK_P_GRUPO_OFE] PRIMARY KEY  CLUSTERED " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [ID]," & vbCrLf
sConsulta = sConsulta & "       [GRUPO]" & vbCrLf
sConsulta = sConsulta & "   )  ON [PRIMARY] " & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "CREATE TABLE [dbo].[P_ITEM_OFE] (" & vbCrLf
sConsulta = sConsulta & "   [ID] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [ITEM] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [GRUPO] [varchar] (" & gLongitudesDeCodigos.giLongCodGRUPO & ") NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [PRECIO] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [CANTMAX] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PRECIO2] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PRECIO3] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [COMENT1] [varchar] (2000) NULL ," & vbCrLf
sConsulta = sConsulta & "   [COMENT2] [varchar] (2000) NULL ," & vbCrLf
sConsulta = sConsulta & "   [COMENT3] [varchar] (2000) NULL ," & vbCrLf
sConsulta = sConsulta & "   [OBSADJUN] [text] NULL ," & vbCrLf
sConsulta = sConsulta & "   [CANT] [float] NULL " & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE [dbo].[P_ITEM_OFE] WITH NOCHECK ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [PK_P_ITEM_OFE] PRIMARY KEY  CLUSTERED " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [ID]," & vbCrLf
sConsulta = sConsulta & "       [ITEM]" & vbCrLf
sConsulta = sConsulta & "   )  ON [PRIMARY] " & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TABLE [dbo].[P_OFE_ATRIB] (" & vbCrLf
sConsulta = sConsulta & "   [ID] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [ATRIB_ID] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [VALOR_NUM] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [VALOR_TEXT] [varchar] (800) NULL ," & vbCrLf
sConsulta = sConsulta & "   [VALOR_FEC] [datetime] NULL ," & vbCrLf
sConsulta = sConsulta & "   [VALOR_BOOL] [tinyint] NULL " & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE [dbo].[P_OFE_ATRIB] WITH NOCHECK ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [PK_P_OFE_ATRIB] PRIMARY KEY  CLUSTERED " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [ID]," & vbCrLf
sConsulta = sConsulta & "       [ATRIB_ID]" & vbCrLf
sConsulta = sConsulta & "   )  ON [PRIMARY] " & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "CREATE TABLE [dbo].[P_OFE_GR_ATRIB] (" & vbCrLf
sConsulta = sConsulta & "   [ID] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [GRUPO] [varchar] (" & gLongitudesDeCodigos.giLongCodGRUPO & ") NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [ATRIB_ID] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [VALOR_NUM] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [VALOR_TEXT] [varchar] (800) NULL ," & vbCrLf
sConsulta = sConsulta & "   [VALOR_FEC] [datetime] NULL ," & vbCrLf
sConsulta = sConsulta & "   [VALOR_BOOL] [tinyint] NULL " & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE [dbo].[P_OFE_GR_ATRIB] WITH NOCHECK ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [PK_P_OFE_GR_ATRIB] PRIMARY KEY  CLUSTERED " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [ID]," & vbCrLf
sConsulta = sConsulta & "       [GRUPO]," & vbCrLf
sConsulta = sConsulta & "       [ATRIB_ID]" & vbCrLf
sConsulta = sConsulta & "   )  ON [PRIMARY] " & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "CREATE TABLE [dbo].[P_OFE_ITEM_ATRIB] (" & vbCrLf
sConsulta = sConsulta & "   [ID] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [ITEM] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [ATRIB_ID] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [GRUPO] [varchar] (" & gLongitudesDeCodigos.giLongCodGRUPO & ") NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [VALOR_NUM] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [VALOR_TEXT] [varchar] (800) NULL ," & vbCrLf
sConsulta = sConsulta & "   [VALOR_FEC] [datetime] NULL ," & vbCrLf
sConsulta = sConsulta & "   [VALOR_BOOL] [tinyint] NULL " & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE [dbo].[P_OFE_ITEM_ATRIB] WITH NOCHECK ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [PK_P_OFE_ITEM_ATRIB] PRIMARY KEY  CLUSTERED " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [ID]," & vbCrLf
sConsulta = sConsulta & "       [ITEM]," & vbCrLf
sConsulta = sConsulta & "       [ATRIB_ID]" & vbCrLf
sConsulta = sConsulta & "   )  ON [PRIMARY] " & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TABLE [dbo].[P_PROCE_OFE_ADJUN] (" & vbCrLf
sConsulta = sConsulta & "   [ID] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [ADJUN] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [NOM] [varchar] (300) NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [COM] [varchar] (500) NULL ," & vbCrLf
sConsulta = sConsulta & "   [IDPORTAL] [int] NULL ," & vbCrLf
sConsulta = sConsulta & "   [IDGS] [int] NULL ," & vbCrLf
sConsulta = sConsulta & "   [DATASIZE] [int] NULL " & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE [dbo].[P_PROCE_OFE_ADJUN] WITH NOCHECK ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [PK_P_PROCE_OFE_ADJUN] PRIMARY KEY  CLUSTERED " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [ID]," & vbCrLf
sConsulta = sConsulta & "       [ADJUN]" & vbCrLf
sConsulta = sConsulta & "   )  ON [PRIMARY] " & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TABLE [dbo].[P_OFE_GR_ADJUN] (" & vbCrLf
sConsulta = sConsulta & "   [ID] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [GRUPO] [varchar] (" & gLongitudesDeCodigos.giLongCodGRUPO & ") NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [ADJUN] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [NOM] [varchar] (300) NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [COM] [varchar] (500) NULL ," & vbCrLf
sConsulta = sConsulta & "   [IDPORTAL] [int] NULL ," & vbCrLf
sConsulta = sConsulta & "   [IDGS] [int] NULL ," & vbCrLf
sConsulta = sConsulta & "   [DATASIZE] [int] NULL " & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE [dbo].[P_OFE_GR_ADJUN] WITH NOCHECK ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [PK_P_OFE_GR_ADJUN] PRIMARY KEY  CLUSTERED " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [ID]," & vbCrLf
sConsulta = sConsulta & "       [GRUPO]," & vbCrLf
sConsulta = sConsulta & "       [ADJUN]" & vbCrLf
sConsulta = sConsulta & "   )  ON [PRIMARY] " & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "CREATE TABLE [dbo].[P_OFE_ITEM_ADJUN] (" & vbCrLf
sConsulta = sConsulta & "   [ID] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [ITEM] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [ADJUN] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [NOM] [varchar] (300) NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [COM] [varchar] (500) NULL ," & vbCrLf
sConsulta = sConsulta & "   [IDPORTAL] [int] NULL ," & vbCrLf
sConsulta = sConsulta & "   [IDGS] [int] NULL ," & vbCrLf
sConsulta = sConsulta & "   [DATASIZE] [int] NULL " & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE [dbo].[P_OFE_ITEM_ADJUN] WITH NOCHECK ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [PK_P_OFE_ITEM_ADJUN] PRIMARY KEY  CLUSTERED " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [ID]," & vbCrLf
sConsulta = sConsulta & "       [ITEM]," & vbCrLf
sConsulta = sConsulta & "       [ADJUN]" & vbCrLf
sConsulta = sConsulta & "   )  ON [PRIMARY] " & vbCrLf
ExecuteSQL gRDOCon, sConsulta



End Sub



Private Sub V_2_11_5_Storeds1_02()
Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_DEVOLVER_ATRIBUTOS_OBLIGATORIOS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_DEVOLVER_ATRIBUTOS_OBLIGATORIOS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_DEVOLVER_ATRIBUTOS_OBLIGATORIOS @ANYO INT, @GMN1 varchar(50), @PROCE int" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--SET @CATRIBS = CURSOR" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--FORWARD_ONLY STATIC FOR" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT PA.ID, AMBITO, GRUPO, TIPO" & vbCrLf
sConsulta = sConsulta & "  FROM PROCE_ATRIB PA" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN DEF_ATRIB DA" & vbCrLf
sConsulta = sConsulta & "           ON PA.ATRIB = DA.ID" & vbCrLf
sConsulta = sConsulta & " WHERE ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "   AND GMN1= @GMN1" & vbCrLf
sConsulta = sConsulta & "   AND PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "   AND INTERNO = 0" & vbCrLf
sConsulta = sConsulta & "   AND OBLIG = 1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--OPEN @CATRIBS" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_DEVOLVER_ESP]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_DEVOLVER_ESP]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_DEVOLVER_ESP @ANYO INT, @GMN1 VARCHAR(20), @PROCE INT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT ANYO, GMN1, PROCE, NULL GRUPO, NULL ITEM,  ID, NOM, COM, FECACT, DATALENGTH(DATA) TAMANYO FROM PROCE_ESP WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "UNION" & vbCrLf
sConsulta = sConsulta & "SELECT ANYO, GMN1, PROCE, GRUPO, NULL ITEM,  ID, NOM, COM, FECACT, DATALENGTH(DATA) TAMANYO FROM PROCE_GRUPO_ESP WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "UNION" & vbCrLf
sConsulta = sConsulta & "SELECT IE.ANYO, IE.GMN1, IE.PROCE, I.GRUPO, IE.ITEM, IE.ID, IE.NOM, IE.COM, IE.FECACT, DATALENGTH(IE.DATA) TAMANYO " & vbCrLf
sConsulta = sConsulta & "  FROM  ITEM_ESP IE" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN ITEM I" & vbCrLf
sConsulta = sConsulta & "           ON IE.ANYO = I.ANYO" & vbCrLf
sConsulta = sConsulta & "          AND IE.GMN1 = I.GMN1" & vbCrLf
sConsulta = sConsulta & "          AND IE.PROCE = I.PROCE" & vbCrLf
sConsulta = sConsulta & "          AND IE.ITEM = I.ID" & vbCrLf
sConsulta = sConsulta & " WHERE IE.ANYO=@ANYO AND IE.GMN1=@GMN1 AND IE.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_DEVOLVER_GRUPO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_DEVOLVER_GRUPO]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "CREATE PROCEDURE SP_DEVOLVER_GRUPO @ANYO INT, @GMN1 VARCHAR(20),@PROCE INT ,@GRUPO VARCHAR(20), @PROVE VARCHAR(50) = null, @OFE int = NULL, @MON VARCHAR(50) = NULL AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @HAYESP TINYINT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @HAYATRIB TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCEEST smallint" & vbCrLf
sConsulta = sConsulta & "DECLARE @EQUIVPROC FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @CAM FLOAT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @EQUIVPROC = CAMBIO,@PROCEEST = EST" & vbCrLf
sConsulta = sConsulta & "  FROM PROCE " & vbCrLf
sConsulta = sConsulta & " WHERE ANYO=@ANYO" & vbCrLf
sConsulta = sConsulta & "   AND GMN1=@GMN1" & vbCrLf
sConsulta = sConsulta & "   AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @CAM =( SELECT CAMBIO" & vbCrLf
sConsulta = sConsulta & "             FROM PROCE_OFE" & vbCrLf
sConsulta = sConsulta & "            WHERE ANYO=@ANYO" & vbCrLf
sConsulta = sConsulta & "              AND GMN1=@GMN1" & vbCrLf
sConsulta = sConsulta & "              AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "              AND PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "              AND OFE=@OFE)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CAM IS NULL" & vbCrLf
sConsulta = sConsulta & "  SELECT @CAM =  EQUIV / @EQUIVPROC" & vbCrLf
sConsulta = sConsulta & "    FROM MON " & vbCrLf
sConsulta = sConsulta & "   WHERE COD = @MON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @HAYESP = CASE WHEN ESP IS NULL OR ESP='' THEN 0 ELSE 1 END " & vbCrLf
sConsulta = sConsulta & "  FROM PROCE_GRUPO" & vbCrLf
sConsulta = sConsulta & " WHERE ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "   AND GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "   AND PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "   AND COD = @GRUPO" & vbCrLf
sConsulta = sConsulta & "IF @HAYESP = 0" & vbCrLf
sConsulta = sConsulta & "  IF (SELECT count(id) " & vbCrLf
sConsulta = sConsulta & "        FROM PROCE_GRUPO_ESP" & vbCrLf
sConsulta = sConsulta & "       WHERE ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "         and GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "         and PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "         AND GRUPO = @GRUPO)>0" & vbCrLf
sConsulta = sConsulta & "    SET @HAYESP = 1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @HAYATRIB = 0" & vbCrLf
sConsulta = sConsulta & "IF (SELECT COUNT(*)" & vbCrLf
sConsulta = sConsulta & "      FROM PROCE_ATRIB" & vbCrLf
sConsulta = sConsulta & "     WHERE ANYO  = @ANYO" & vbCrLf
sConsulta = sConsulta & "       and GMN1  = @GMN1 " & vbCrLf
sConsulta = sConsulta & "       and PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "       AND AMBITO = 2" & vbCrLf
sConsulta = sConsulta & "       and ISNULL(GRUPO, @GRUPO) = @GRUPO)>0" & vbCrLf
sConsulta = sConsulta & "  SET @HAYATRIB = 1  " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "DECLARE @IMPORTEOFERTA float" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if @PROVE IS NOT NULL AND @OFE IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SELECT @IMPORTEOFERTA = SUM(ISNULL(IO.PRECIO,0) * ISNULL(I.CANT,0)) " & vbCrLf
sConsulta = sConsulta & "  FROM ITEM_OFE IO" & vbCrLf
sConsulta = sConsulta & "         INNER JOIN ITEM I" & vbCrLf
sConsulta = sConsulta & "                 ON IO.PROCE = I.PROCE" & vbCrLf
sConsulta = sConsulta & "                AND IO.GMN1 = I.GMN1" & vbCrLf
sConsulta = sConsulta & "                AND IO.ANYO = I.ANYO" & vbCrLf
sConsulta = sConsulta & "                AND IO.ITEM =I.ID" & vbCrLf
sConsulta = sConsulta & " WHERE IO.ANYO  = @ANYO" & vbCrLf
sConsulta = sConsulta & "   AND IO.GMN1  = @GMN1 " & vbCrLf
sConsulta = sConsulta & "   AND IO.PROCE = @PROCE " & vbCrLf
sConsulta = sConsulta & "   AND IO.PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "   AND IO.NUM   = @OFE" & vbCrLf
sConsulta = sConsulta & "   AND I.GRUPO = @GRUPO" & vbCrLf
sConsulta = sConsulta & "   AND I.CONF=1" & vbCrLf
sConsulta = sConsulta & "   AND CASE WHEN I.EST IS NULL THEN -1 ELSE I.EST END = CASE WHEN @PROCEEST=11 THEN 0 ELSE -1 END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT PG.COD, PG.DEN, PG.DESCR, PG.DEST, PG.PAG, PAG.DEN DENPAG, PG.FECINI, PG.FECFIN, PG.ESP, @HAYESP HAYESP, @HAYATRIB HAYATRIB, @IMPORTEOFERTA/(@EQUIVPROC*@CAM)  IMPORTEOFERTA, GO.OBSADJUN" & vbCrLf
sConsulta = sConsulta & "  FROM PROCE_GRUPO PG" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN PAG" & vbCrLf
sConsulta = sConsulta & "               ON PG.PAG = PAG.COD" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN GRUPO_OFE GO" & vbCrLf
sConsulta = sConsulta & "              ON PG.ANYO = GO.ANYO" & vbCrLf
sConsulta = sConsulta & "             AND PG.GMN1 = GO.GMN1" & vbCrLf
sConsulta = sConsulta & "             AND PG.PROCE = GO.PROCE" & vbCrLf
sConsulta = sConsulta & "             AND PG.COD = GO.GRUPO" & vbCrLf
sConsulta = sConsulta & "             AND @PROVE = GO.PROVE" & vbCrLf
sConsulta = sConsulta & "             AND @OFE = GO.OFE" & vbCrLf
sConsulta = sConsulta & " WHERE PG.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "   AND PG.GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "   AND PG.PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "   AND PG.COD = @GRUPO" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_DEVOLVER_OFERTA_PROVE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_DEVOLVER_OFERTA_PROVE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_DEVOLVER_OFERTA_PROVE @ANYO INTEGER,@GMN1 VARCHAR(20),@PROCE INTEGER ,@PROVE VARCHAR(50), @OFE smallint = null AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @LASTOFEENV smallint" & vbCrLf
sConsulta = sConsulta & "DECLARE @LASTOFE smallint" & vbCrLf
sConsulta = sConsulta & "DECLARE @NUMOFEENVIADAS smallint" & vbCrLf
sConsulta = sConsulta & "DECLARE @FECHAULTOFE datetime" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCEEST smallint" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "SELECT @PROCEEST = EST " & vbCrLf
sConsulta = sConsulta & "  FROM PROCE " & vbCrLf
sConsulta = sConsulta & " WHERE ANYO = @ANYO " & vbCrLf
sConsulta = sConsulta & "   AND GMN1 = @GMN1 " & vbCrLf
sConsulta = sConsulta & "   AND COD = @PROCE" & vbCrLf
sConsulta = sConsulta & "SELECT @NUMOFEENVIADAS  = COUNT(OFE)" & vbCrLf
sConsulta = sConsulta & "  FROM PROCE_OFE " & vbCrLf
sConsulta = sConsulta & " WHERE ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "   AND GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "   AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "   AND PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "   AND ENVIADA = 1" & vbCrLf
sConsulta = sConsulta & "SELECT @LASTOFEENV = MAX(OFE)" & vbCrLf
sConsulta = sConsulta & "  FROM PROCE_OFE " & vbCrLf
sConsulta = sConsulta & " WHERE ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "   AND GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "   AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "   AND PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "   AND ENVIADA = 1" & vbCrLf
sConsulta = sConsulta & "select @FECHAULTOFE = FECREC" & vbCrLf
sConsulta = sConsulta & "  FROM PROCE_OFE " & vbCrLf
sConsulta = sConsulta & " WHERE ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "   AND GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "   AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "   AND PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "   AND OFE = @LASTOFEENV" & vbCrLf
sConsulta = sConsulta & "IF @OFE IS NULL" & vbCrLf
sConsulta = sConsulta & "  IF EXISTS (SELECT * " & vbCrLf
sConsulta = sConsulta & "               FROM PROCE_OFE " & vbCrLf
sConsulta = sConsulta & "              WHERE ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "                AND GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "                AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "                AND PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "                AND OFE = -1)" & vbCrLf
sConsulta = sConsulta & "    SET @LASTOFE = -1" & vbCrLf
sConsulta = sConsulta & "  ELSE" & vbCrLf
sConsulta = sConsulta & "    SELECT @LASTOFE = MAX(OFE)" & vbCrLf
sConsulta = sConsulta & "      FROM PROCE_OFE" & vbCrLf
sConsulta = sConsulta & "     WHERE ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "       AND GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "       AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "       AND PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @LASTOFE =@OFE" & vbCrLf
sConsulta = sConsulta & "DECLARE @TOTALADJUNTOS int" & vbCrLf
sConsulta = sConsulta & "SET @TOTALADJUNTOS =(SELECT ISNULL(SUM(DATASIZE) ,0)" & vbCrLf
sConsulta = sConsulta & "                       FROM PROCE_OFE_ADJUN" & vbCrLf
sConsulta = sConsulta & "                      WHERE ANYO  = @ANYO" & vbCrLf
sConsulta = sConsulta & "                        AND GMN1  = @GMN1 " & vbCrLf
sConsulta = sConsulta & "                        AND PROCE = @PROCE " & vbCrLf
sConsulta = sConsulta & "                        AND PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "                        AND OFE   = @LASTOFE)" & vbCrLf
sConsulta = sConsulta & "SET @TOTALADJUNTOS = @TOTALADJUNTOS +" & vbCrLf
sConsulta = sConsulta & "(SELECT ISNULL(SUM(DATASIZE) ,0)" & vbCrLf
sConsulta = sConsulta & "   FROM OFE_GR_ADJUN" & vbCrLf
sConsulta = sConsulta & "  WHERE ANYO  = @ANYO" & vbCrLf
sConsulta = sConsulta & "    AND GMN1  = @GMN1 " & vbCrLf
sConsulta = sConsulta & "    AND PROCE = @PROCE " & vbCrLf
sConsulta = sConsulta & "    AND PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "    AND OFE   = @LASTOFE)" & vbCrLf
sConsulta = sConsulta & "SET @TOTALADJUNTOS = @TOTALADJUNTOS +" & vbCrLf
sConsulta = sConsulta & "(SELECT ISNULL(SUM(DATASIZE) ,0)" & vbCrLf
sConsulta = sConsulta & "   FROM OFE_ITEM_ADJUN" & vbCrLf
sConsulta = sConsulta & "  WHERE ANYO  = @ANYO" & vbCrLf
sConsulta = sConsulta & "    AND GMN1  = @GMN1 " & vbCrLf
sConsulta = sConsulta & "    AND PROCE = @PROCE " & vbCrLf
sConsulta = sConsulta & "    AND PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "    AND OFE   = @LASTOFE)" & vbCrLf
sConsulta = sConsulta & "DECLARE @IMPORTEOFERTA float" & vbCrLf
sConsulta = sConsulta & "SELECT @IMPORTEOFERTA = SUM(ISNULL(IO.PRECIO,0) * ISNULL(I.CANT,0)) " & vbCrLf
sConsulta = sConsulta & "  FROM ITEM_OFE IO" & vbCrLf
sConsulta = sConsulta & "         INNER JOIN ITEM I" & vbCrLf
sConsulta = sConsulta & "                 ON IO.PROCE = I.PROCE" & vbCrLf
sConsulta = sConsulta & "                AND IO.GMN1 = I.GMN1" & vbCrLf
sConsulta = sConsulta & "                AND IO.ANYO = I.ANYO" & vbCrLf
sConsulta = sConsulta & "                AND IO.ITEM =I.ID" & vbCrLf
sConsulta = sConsulta & " WHERE IO.ANYO  = @ANYO" & vbCrLf
sConsulta = sConsulta & "   AND IO.GMN1  = @GMN1 " & vbCrLf
sConsulta = sConsulta & "   AND IO.PROCE = @PROCE " & vbCrLf
sConsulta = sConsulta & "   AND IO.PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "   AND IO.NUM   = @LASTOFE" & vbCrLf
sConsulta = sConsulta & "   AND I.CONF=1" & vbCrLf
sConsulta = sConsulta & "   AND CASE WHEN I.EST IS NULL THEN -1 ELSE I.EST END = CASE WHEN @PROCEEST=11 THEN 0 ELSE -1 END" & vbCrLf
sConsulta = sConsulta & "SELECT PO.ANYO, PO.GMN1, PO.PROCE, PO.PROVE, PO.OFE, PO.EST, PO.FECREC," & vbCrLf
sConsulta = sConsulta & "       PO.FECVAL, PO.MON, MON.DEN MONDEN, PO.CAMBIO, PO.OBS , @LASTOFEENV LASTOFEENV, " & vbCrLf
sConsulta = sConsulta & "       @FECHAULTOFE FECHAULTOFE, @NUMOFEENVIADAS NUMOFEENVIADAS, ENVIADA ESTULTOFE,PO.FECACT, @TOTALADJUNTOS TOTALADJUNTOS, @IMPORTEOFERTA IMPORTEOFERTA, PO.OBSADJUN" & vbCrLf
sConsulta = sConsulta & "  FROM PROCE_OFE PO " & vbCrLf
sConsulta = sConsulta & "    INNER JOIN MON " & vbCrLf
sConsulta = sConsulta & "            ON PO.MON = MON.COD" & vbCrLf
sConsulta = sConsulta & " WHERE ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "   AND GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "   AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "   AND PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "   AND OFE = @LASTOFE" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub
Private Sub V_2_11_5_Storeds2_02()
Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ITEMS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_ITEMS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE  SP_ITEMS  @ANYO INT,@GMN1 VARCHAR(50),@PROCE INT,@GRUPO VARCHAR(50)=null, @PROVE varchar(50), @OFE smallint, @INTERNO TINYINT = NULL, @TODOS TINYINT = 0, @MON varchar(50) = null  AS " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF " & vbCrLf
sConsulta = sConsulta & "set NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @CAM FLOAT " & vbCrLf
sConsulta = sConsulta & "DECLARE @EQUIVPROC FLOAT  " & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCEEST smallint" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @TIPOATRIB tinyint" & vbCrLf
sConsulta = sConsulta & "DECLARE @CODATRIB varchar(500)" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDPROCEATRIB int" & vbCrLf
sConsulta = sConsulta & "DECLARE @PARAMDEF NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @ALTER NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @INSERT NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SELECT @EQUIVPROC = CAMBIO, @PROCEEST =EST " & vbCrLf
sConsulta = sConsulta & "  FROM PROCE " & vbCrLf
sConsulta = sConsulta & " WHERE ANYO=@ANYO" & vbCrLf
sConsulta = sConsulta & "   AND GMN1=@GMN1" & vbCrLf
sConsulta = sConsulta & "   AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "SET @CAM =( SELECT CAMBIO" & vbCrLf
sConsulta = sConsulta & "             FROM PROCE_OFE" & vbCrLf
sConsulta = sConsulta & "            WHERE ANYO=@ANYO" & vbCrLf
sConsulta = sConsulta & "              AND GMN1=@GMN1" & vbCrLf
sConsulta = sConsulta & "              AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "              AND PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "              AND OFE=@OFE)" & vbCrLf
sConsulta = sConsulta & "IF @CAM IS NULL" & vbCrLf
sConsulta = sConsulta & "  SELECT @CAM =  EQUIV / @EQUIVPROC" & vbCrLf
sConsulta = sConsulta & "    FROM MON " & vbCrLf
sConsulta = sConsulta & "   WHERE COD = @MON" & vbCrLf
sConsulta = sConsulta & "declare C1 CURSOR LOCAL FOR" & vbCrLf
sConsulta = sConsulta & "SELECT PA.ID, DA.COD, DA.TIPO" & vbCrLf
sConsulta = sConsulta & " FROM PROCE_ATRIB PA" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN DEF_ATRIB DA" & vbCrLf
sConsulta = sConsulta & "               ON PA.ATRIB = DA.ID" & vbCrLf
sConsulta = sConsulta & "WHERE PA.ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "  AND PA.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "  AND PA.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "  AND ISNULL(PA.GRUPO,'#') = CASE WHEN PA.GRUPO IS NULL THEN ISNULL(PA.GRUPO,'#') ELSE @GRUPO END" & vbCrLf
sConsulta = sConsulta & "  AND PA.AMBITO = 3" & vbCrLf
sConsulta = sConsulta & "  AND PA.INTERNO = CASE WHEN @INTERNO IS NULL THEN PA.INTERNO ELSE @INTERNO END" & vbCrLf
sConsulta = sConsulta & "ORDER BY DA.COD" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE #ATRIBS (ATANYO int NULL ," & vbCrLf
If gCollation = "" Then
    sConsulta = sConsulta & "   ATGMN1 varchar (50) NULL ," & vbCrLf
Else
    sConsulta = sConsulta & "   ATGMN1 varchar (50) COLLATE " & gCollation & " NULL ," & vbCrLf
End If
sConsulta = sConsulta & "   ATPROCE int NULL ," & vbCrLf
sConsulta = sConsulta & "   ATITEM int NULL " & vbCrLf
sConsulta = sConsulta & ") " & vbCrLf
sConsulta = sConsulta & "SET @INSERT = 'INSERT INTO #ATRIBS (ATANYO, ATGMN1, ATPROCE, ATITEM, '" & vbCrLf
sConsulta = sConsulta & "SET @SQL='SELECT ANYO, GMN1,  PROCE, ITEM, '" & vbCrLf
sConsulta = sConsulta & "set @ALTER = 'ALTER TABLE #ATRIBS ADD '" & vbCrLf
sConsulta = sConsulta & "OPEN C1" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C1 INTO @IDPROCEATRIB, @CODATRIB, @TIPOATRIB" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS = 0" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    " & vbCrLf
sConsulta = sConsulta & "    SET @INSERT = @INSERT + '[AT_' + @CODATRIB + '],'" & vbCrLf
sConsulta = sConsulta & "    IF @TIPOATRIB = 1 " & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "        SET @ALTER = @ALTER  + '[AT_' + @CODATRIB + '] VARCHAR(800), '" & vbCrLf
sConsulta = sConsulta & "        SET @SQL = @SQL + 'MAX(case IOA.ATRIB_ID when ' + cast(@IDPROCEATRIB as varchar(100)) + ' THEN VALOR_TEXT else NULL end) as [' + @CODATRIB + '],'" & vbCrLf
sConsulta = sConsulta & "      END " & vbCrLf
sConsulta = sConsulta & "    ELSE IF @TIPOATRIB = 2" & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "        SET @ALTER = @ALTER  + '[AT_' + @CODATRIB + '] FLOAT, '" & vbCrLf
sConsulta = sConsulta & "        SET @SQL = @SQL + 'MAX(case IOA.ATRIB_ID when ' + cast(@IDPROCEATRIB as varchar(100)) + ' THEN VALOR_NUM else NULL end) as [' + @CODATRIB + '],'" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "    ELSE IF @TIPOATRIB = 3" & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "        SET @ALTER = @ALTER  + '[AT_' + @CODATRIB + '] DATETIME, '" & vbCrLf
sConsulta = sConsulta & "        SET @SQL = @SQL + 'MAX(case IOA.ATRIB_ID when ' + cast(@IDPROCEATRIB as varchar(100)) + ' THEN VALOR_FEC else NULL end) as [' + @CODATRIB + '],'" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "    ELSE " & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "        SET @ALTER = @ALTER + '[AT_' + @CODATRIB + '] TINYINT, '" & vbCrLf
sConsulta = sConsulta & "        SET @SQL = @SQL + 'MAX(case IOA.ATRIB_ID when ' + cast(@IDPROCEATRIB as varchar(100)) + ' THEN VALOR_BOOL else NULL end) as [' + @CODATRIB + '],'" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "    FETCH NEXT FROM C1 INTO @IDPROCEATRIB, @CODATRIB, @TIPOATRIB" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "if @INSERT != 'INSERT INTO #ATRIBS (ATANYO, ATGMN1, ATPROCE, ATITEM, '" & vbCrLf
sConsulta = sConsulta & "  begin" & vbCrLf
sConsulta = sConsulta & "    set @SQL = LEFT(@SQL,LEN(@SQL)-1)" & vbCrLf
sConsulta = sConsulta & "    set @INSERT = LEFT(@INSERT,LEN(@INSERT)-1)" & vbCrLf
sConsulta = sConsulta & "    set @ALTER = LEFT(@ALTER,LEN(@ALTER)-1)" & vbCrLf
sConsulta = sConsulta & "    EXEC SP_EXECUTESQL  @ALTER " & vbCrLf
sConsulta = sConsulta & "    SET @SQL = @INSERT + ')" & vbCrLf
sConsulta = sConsulta & "    ' + @SQL + '" & vbCrLf
sConsulta = sConsulta & "      FROM OFE_ITEM_ATRIB IOA" & vbCrLf
sConsulta = sConsulta & "     WHERE IOA.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "       AND IOA.GMN1 = @GMN1  " & vbCrLf
sConsulta = sConsulta & "       AND IOA.PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "       AND IOA.PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "       AND IOA.OFE = @OFE" & vbCrLf
sConsulta = sConsulta & "    GROUP BY ANYO, GMN1,  PROCE, ITEM'" & vbCrLf
sConsulta = sConsulta & "    SET @PARAMDEF = '@ANYO INT, @GMN1 VARCHAR(50), @PROCE INT,@GRUPO VARCHAR(50), @PROVE varchar(50), @OFE smallint'" & vbCrLf
sConsulta = sConsulta & "    EXEC SP_EXECUTESQL  @SQL, @PARAMDEF, @ANYO = @ANYO, @GMN1 = @GMN1, @PROCE = @PROCE, @GRUPO = @GRUPO, @PROVE = @PROVE, @OFE = @OFE" & vbCrLf
sConsulta = sConsulta & "  end" & vbCrLf
sConsulta = sConsulta & "--   Se devuelve siempre en la moneda de la oferta" & vbCrLf
sConsulta = sConsulta & "SELECT I.ANYO, I.GMN1, I.PROCE, I.GRUPO, I.ID, I.GMN2, I.GMN3, I.GMN4, I.ART, I.DESCR + A.DEN AS DESCR, " & vbCrLf
sConsulta = sConsulta & "       I.DEST, I.UNI,UNI.DEN UNIDEN, I.CANT, I.PREC*@CAM as PREC, I.PRES*@CAM as PRES," & vbCrLf
sConsulta = sConsulta & "       I.PAG, PAG.DEN DENPAG, I.FECINI, I.FECFIN, I.ESP, I.CONF, I.EST," & vbCrLf
sConsulta = sConsulta & "       I.OBJ*@CAM AS OBJ , I.FECHAOBJ, " & vbCrLf
sConsulta = sConsulta & "       O.PROVE MINPROVE, P2.DEN MINPROVEDEN, O.PRECIO*@EQUIVPROC*@CAM MINPRECIO," & vbCrLf
sConsulta = sConsulta & "       CASE WHEN @PROVE = O.PROVE THEN 1 ELSE 0 END PROPIA," & vbCrLf
sConsulta = sConsulta & "       CASE  WHEN NUMESP > 0 THEN 1 WHEN NOT I.ESP  IS NULL THEN 1 ELSE 0 END ESPECADJ ," & vbCrLf
sConsulta = sConsulta & "       ITO.PRECIO, ITO.CANTMAX, ITO.ULT, ITO.PRECIO2, ITO.PRECIO3,ITO.USAR,ITO.FECACT," & vbCrLf
sConsulta = sConsulta & "       IA.NUMADJUN, ITO.COMENT1, ITO.COMENT2, ITO.COMENT3, @EQUIVPROC*@CAM CAMBIO, cast(ITO.OBSADJUN as varchar(2000)) OBSADJUN, @EQUIVPROC EQUIVPROC, ATR.*" & vbCrLf
sConsulta = sConsulta & "  FROM ITEM I" & vbCrLf
sConsulta = sConsulta & "        LEFT JOIN ART4 A" & vbCrLf
sConsulta = sConsulta & "               ON I.ART = A.COD " & vbCrLf
sConsulta = sConsulta & "              AND I.GMN4 = A.GMN4 " & vbCrLf
sConsulta = sConsulta & "              AND I.GMN3 = A.GMN3 " & vbCrLf
sConsulta = sConsulta & "              AND I.GMN2 = A.GMN2" & vbCrLf
sConsulta = sConsulta & "              AND I.GMN1 = A.GMN1" & vbCrLf
sConsulta = sConsulta & "        LEFT JOIN ITEM_OFE ITO" & vbCrLf
sConsulta = sConsulta & "               ON I.ANYO = ITO.ANYO" & vbCrLf
sConsulta = sConsulta & "              AND I.GMN1 = ITO.GMN1" & vbCrLf
sConsulta = sConsulta & "              AND I.PROCE = ITO.PROCE" & vbCrLf
sConsulta = sConsulta & "              AND I.ID = ITO.ITEM" & vbCrLf
sConsulta = sConsulta & "              AND @PROVE = ITO.PROVE" & vbCrLf
sConsulta = sConsulta & "              AND @OFE = ITO.NUM" & vbCrLf
sConsulta = sConsulta & "        LEFT JOIN (OFEMIN O " & vbCrLf
sConsulta = sConsulta & "                     INNER JOIN PROVE P2 " & vbCrLf
sConsulta = sConsulta & "                             ON O.PROVE = P2.COD" & vbCrLf
sConsulta = sConsulta & "                   )" & vbCrLf
sConsulta = sConsulta & "               ON I.ANYO = O.ANYO" & vbCrLf
sConsulta = sConsulta & "              AND I.GMN1 = O.GMN1" & vbCrLf
sConsulta = sConsulta & "              AND I.PROCE = O.PROCE" & vbCrLf
sConsulta = sConsulta & "              AND I.ID = O.ITEM" & vbCrLf
sConsulta = sConsulta & "               " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "        LEFT JOIN #ATRIBS ATR" & vbCrLf
sConsulta = sConsulta & "               ON I.ANYO = ATR.ATANYO" & vbCrLf
sConsulta = sConsulta & "              AND I.GMN1 = ATR.ATGMN1" & vbCrLf
sConsulta = sConsulta & "              AND I.PROCE = ATR.ATPROCE" & vbCrLf
sConsulta = sConsulta & "              AND I.ID = ATR.ATITEM        LEFT JOIN (SELECT IE.ANYO, IE.GMN1, IE.PROCE, IE.ITEM, COUNT(IE.ID) NUMESP " & vbCrLf
sConsulta = sConsulta & "                     FROM ITEM_ESP IE" & vbCrLf
sConsulta = sConsulta & "                    WHERE IE.ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "                      AND IE.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "                      AND IE.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "                 GROUP BY IE.ANYO, IE.GMN1, IE.PROCE, IE.ITEM) IE" & vbCrLf
sConsulta = sConsulta & "               ON I.ID = IE.ITEM " & vbCrLf
sConsulta = sConsulta & "              AND I.GMN1 = IE.GMN1" & vbCrLf
sConsulta = sConsulta & "              AND I.PROCE = IE.PROCE" & vbCrLf
sConsulta = sConsulta & "              AND I.ANYO = IE.ANYO " & vbCrLf
sConsulta = sConsulta & "        LEFT JOIN (SELECT IA.ANYO, IA.GMN1, IA.PROCE, IA.ITEM, COUNT(IA.ID) NUMADJUN" & vbCrLf
sConsulta = sConsulta & "                     FROM OFE_ITEM_ADJUN IA" & vbCrLf
sConsulta = sConsulta & "                    WHERE IA.ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "                      AND IA.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "                      AND IA.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "                      AND IA.PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "                      AND IA.OFE = @OFE" & vbCrLf
sConsulta = sConsulta & "                 GROUP BY IA.ANYO, IA.GMN1, IA.PROCE, IA.ITEM) IA" & vbCrLf
sConsulta = sConsulta & "               ON I.ID = IA.ITEM " & vbCrLf
sConsulta = sConsulta & "              AND I.GMN1 = IA.GMN1" & vbCrLf
sConsulta = sConsulta & "              AND I.PROCE = IA.PROCE" & vbCrLf
sConsulta = sConsulta & "              AND I.ANYO = IA.ANYO " & vbCrLf
sConsulta = sConsulta & "        LEFT JOIN PAG " & vbCrLf
sConsulta = sConsulta & "               ON I.PAG = PAG.COD" & vbCrLf
sConsulta = sConsulta & "        LEFT JOIN UNI " & vbCrLf
sConsulta = sConsulta & "               ON I.UNI = UNI.COD" & vbCrLf
sConsulta = sConsulta & "WHERE I.ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "  AND I.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "  AND I.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "  AND I.CONF=1" & vbCrLf
sConsulta = sConsulta & "  AND I.GRUPO=case when @GRUPO is null then i.grupo else @GRUPO END" & vbCrLf
sConsulta = sConsulta & "  AND CASE WHEN I.EST IS NULL THEN -1 ELSE I.EST END = CASE WHEN @PROCEEST=11 THEN CASE WHEN @TODOS=1 THEN I.EST ELSE 0 END ELSE -1 END" & vbCrLf
sConsulta = sConsulta & "ORDER BY I.ART,DESCR" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_DEVOLVER_PROCESO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_DEVOLVER_PROCESO]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "CREATE PROCEDURE SP_DEVOLVER_PROCESO (@ANYO INTEGER,@GMN1 VARCHAR(20),@PROCE INTEGER ,@ACTIVA SMALLINT OUTPUT) AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @SUBASTA AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @FECLIMOFE AS DATETIME" & vbCrLf
sConsulta = sConsulta & "DECLARE @HAYESP  AS BIT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ENVIOASINCRONO AS TINYINT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  SELECT @ENVIOASINCRONO = ENVIO_ASINCRONO FROM PARGEN_INTERNO WHERE ID = 1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  SELECT @SUBASTA=PD.SUBASTA , @FECLIMOFE =FECLIMOFE " & vbCrLf
sConsulta = sConsulta & "    FROM PROCE " & vbCrLf
sConsulta = sConsulta & "        INNER JOIN PROCE_DEF PD" & vbCrLf
sConsulta = sConsulta & "                ON PROCE.ANYO = PD.ANYO" & vbCrLf
sConsulta = sConsulta & "               AND PROCE.GMN1 = PD.GMN1" & vbCrLf
sConsulta = sConsulta & "               AND PROCE.COD = PD.PROCE" & vbCrLf
sConsulta = sConsulta & "   WHERE PROCE.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "     AND PROCE.COD=@PROCE " & vbCrLf
sConsulta = sConsulta & "     AND PROCE.ANYO=@ANYO" & vbCrLf
sConsulta = sConsulta & "  " & vbCrLf
sConsulta = sConsulta & "  SELECT @HAYESP = CASE WHEN ESP IS NULL OR ESP='' THEN 0 ELSE 1 END " & vbCrLf
sConsulta = sConsulta & "    FROM PROCE " & vbCrLf
sConsulta = sConsulta & "   WHERE ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "     AND COD=@PROCE " & vbCrLf
sConsulta = sConsulta & "     AND GMN1=@GMN1" & vbCrLf
sConsulta = sConsulta & "  IF @HAYESP = 0" & vbCrLf
sConsulta = sConsulta & "     BEGIN   " & vbCrLf
sConsulta = sConsulta & "       if (select count(id) " & vbCrLf
sConsulta = sConsulta & "             from proce_esp " & vbCrLf
sConsulta = sConsulta & "            where aNYo=@ANYO " & vbCrLf
sConsulta = sConsulta & "              and gmn1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "              and proce=@PROCE)>0" & vbCrLf
sConsulta = sConsulta & "         select @HAYESP = 1" & vbCrLf
sConsulta = sConsulta & "     END" & vbCrLf
sConsulta = sConsulta & "  IF @SUBASTA= 1 AND @FECLIMOFE <=GETDATE() " & vbCrLf
sConsulta = sConsulta & "      SET @ACTIVA=2" & vbCrLf
sConsulta = sConsulta & "  ELSE " & vbCrLf
sConsulta = sConsulta & "    IF @SUBASTA=1 " & vbCrLf
sConsulta = sConsulta & "      SET @ACTIVA=1" & vbCrLf
sConsulta = sConsulta & "    ELSE" & vbCrLf
sConsulta = sConsulta & "      SET @ACTIVA=0" & vbCrLf
sConsulta = sConsulta & "  SELECT PROCE.ANYO,PROCE.GMN1,PROCE.COD,PROCE.DEN,PROCE.GMN2,PROCE.GMN3,PROCE.GMN4,PROCE.ADJDIR,PROCE.PRES,PROCE.EST," & vbCrLf
sConsulta = sConsulta & "         PROCE.FECAPE,PROCE.FECLIMOFE,PROCE.FECNEC,PROCE.FECPRES,PROCE.FECENVPET,PROCE.FECPROXREU,PROCE.FECULTREU," & vbCrLf
sConsulta = sConsulta & "         PROCE.MON,MON.DEN DENMON, PROCE.CAMBIO,PROCE.SOLICITUD,PROCE.ESP,PROCE.DEST,PROCE.PAG, PAG.DEN DENPAG, PROCE.FECINI,PROCE.FECFIN," & vbCrLf
sConsulta = sConsulta & "         PROCE.FECINISUB,PROCE.COM, PD.SUBASTA,PD.SUBASTAPROVE,PD.SUBASTAPUJAS, @HAYESP HAYESP , @ENVIOASINCRONO ENVIO_ASINCRONO" & vbCrLf
sConsulta = sConsulta & "    FROM PROCE " & vbCrLf
sConsulta = sConsulta & "          INNER JOIN PROCE_DEF PD" & vbCrLf
sConsulta = sConsulta & "                  ON PROCE.ANYO = PD.ANYO" & vbCrLf
sConsulta = sConsulta & "                 AND PROCE.GMN1 = PD.GMN1" & vbCrLf
sConsulta = sConsulta & "                 AND PROCE.COD = PD.PROCE" & vbCrLf
sConsulta = sConsulta & "           LEFT JOIN PAG" & vbCrLf
sConsulta = sConsulta & "                  ON PROCE.PAG = PAG.COD" & vbCrLf
sConsulta = sConsulta & "           LEFT JOIN MON" & vbCrLf
sConsulta = sConsulta & "                  ON PROCE.MON = MON.COD" & vbCrLf
sConsulta = sConsulta & "   WHERE PROCE.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "     AND PROCE.COD=@PROCE " & vbCrLf
sConsulta = sConsulta & "     AND PROCE.ANYO=@ANYO" & vbCrLf
sConsulta = sConsulta & "RETURN" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_TRASPASAR_OFERTA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_TRASPASAR_OFERTA]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "CREATE PROCEDURE SP_TRASPASAR_OFERTA @ID int , @NUM INT OUTPUT, @NUMNUE INT OUTPUT, @NUMAREV INT OUTPUT, @ERROR SMALLINT = NULL OUTPUT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @NEWOFE INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @NUMOFEWEB AS int" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN1 VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCE INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVE VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @MON VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @ADJUN INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDADJUN INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDGS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @GRUPO VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @ITEM INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @NUMOBJ INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @NUMOBJGS INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @ERROR = 0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @ANYO = ANYO, @GMN1= GMN1, @PROCE = PROCE, @PROVE = PROVE, @MON = MON, @NUMOBJ = NUMOBJ" & vbCrLf
sConsulta = sConsulta & "  FROM P_PROCE_OFE " & vbCrLf
sConsulta = sConsulta & " WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @NUMOBJGS = (SELECT NUMOBJ FROM PROCE_PROVE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE)" & vbCrLf
sConsulta = sConsulta & "IF @NUMOBJGS  <=@NUMOBJ" & vbCrLf
sConsulta = sConsulta & "  UPDATE PROCE_PROVE SET OBJNUEVOS=0 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "/*" & vbCrLf
sConsulta = sConsulta & "DECLARE @EQUIV FLOAT " & vbCrLf
sConsulta = sConsulta & "SELECT @EQUIV = (MON.EQUIV / PROCE.CAMBIO) " & vbCrLf
sConsulta = sConsulta & "  FROM PROCE " & vbCrLf
sConsulta = sConsulta & "     INNER JOIN MON " & vbCrLf
sConsulta = sConsulta & "             ON PROCE.ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "            AND PROCE.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND PROCE.COD=@PROCE  " & vbCrLf
sConsulta = sConsulta & "            AND MON.COD=@MON " & vbCrLf
sConsulta = sConsulta & "*/" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @SUBASTA INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @SUBASTA = SUBASTA " & vbCrLf
sConsulta = sConsulta & "  FROM PROCE_DEF " & vbCrLf
sConsulta = sConsulta & " WHERE ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "   AND GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "   AND PROCE=@PROCE  " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @SUBASTA = 1" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    DECLARE @OFE INT" & vbCrLf
sConsulta = sConsulta & "    SET @OFE = ( SELECT MAX(OFE) FROM PROCE_OFE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE AND OFE!=-1)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    IF @OFE IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "      BEGIN" & vbCrLf
sConsulta = sConsulta & "        INSERT INTO P_OFE_GR_ATRIB (ID, GRUPO, ATRIB_ID,  VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL)" & vbCrLf
sConsulta = sConsulta & "        SELECT @ID, O.GRUPO, O.ATRIB_ID, O.VALOR_NUM, O.VALOR_TEXT, O.VALOR_FEC, O.VALOR_BOOL" & vbCrLf
sConsulta = sConsulta & "          FROM OFE_GR_ATRIB O" & vbCrLf
sConsulta = sConsulta & "         WHERE O.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "           AND O.GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "           AND O.PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "           AND O.PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "           AND O.OFE = @OFE" & vbCrLf
sConsulta = sConsulta & "           AND NOT EXISTS (SELECT * FROM P_OFE_GR_ATRIB T WHERE T.ID = @ID AND T.GRUPO = O.GRUPO AND T.ATRIB_ID = O.ATRIB_ID)" & vbCrLf
sConsulta = sConsulta & "        IF @@ERROR!=0 GOTO ELIMINAR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "        INSERT INTO P_OFE_ITEM_ATRIB (ID, GRUPO, ITEM, ATRIB_ID,  VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL)" & vbCrLf
sConsulta = sConsulta & "        SELECT @ID, I.GRUPO, IA.ITEM, ATRIB_ID, VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL" & vbCrLf
sConsulta = sConsulta & "          FROM OFE_ITEM_ATRIB IA" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN ITEM I" & vbCrLf
sConsulta = sConsulta & "                   ON I.ANYO = IA.ANYO" & vbCrLf
sConsulta = sConsulta & "                  AND I.GMN1 = IA.GMN1" & vbCrLf
sConsulta = sConsulta & "                  AND I.PROCE = IA.PROCE" & vbCrLf
sConsulta = sConsulta & "                  AND I.ID = IA.ITEM" & vbCrLf
sConsulta = sConsulta & "         WHERE IA.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "           AND IA.GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "           AND IA.PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "           AND IA.PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "           AND IA.OFE = @OFE" & vbCrLf
sConsulta = sConsulta & "           AND NOT EXISTS (SELECT * FROM P_OFE_ITEM_ATRIB T WHERE T.ID = @ID AND T.ATRIB_ID = IA.ATRIB_ID AND T.ITEM = IA.ITEM)" & vbCrLf
sConsulta = sConsulta & "        IF @@ERROR!=0 GOTO ELIMINAR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "        INSERT INTO P_OFE_ATRIB (ID, ATRIB_ID,  VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL)" & vbCrLf
sConsulta = sConsulta & "        SELECT @ID, O.ATRIB_ID, O.VALOR_NUM, O.VALOR_TEXT, O.VALOR_FEC, O.VALOR_BOOL" & vbCrLf
sConsulta = sConsulta & "          FROM OFE_ATRIB O" & vbCrLf
sConsulta = sConsulta & "         WHERE O.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "           AND O.GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "           AND O.PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "           AND O.PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "           AND O.OFE = @OFE" & vbCrLf
sConsulta = sConsulta & "           AND NOT EXISTS (SELECT * FROM P_OFE_ATRIB T WHERE T.ID = @ID AND T.ATRIB_ID = O.ATRIB_ID)" & vbCrLf
sConsulta = sConsulta & "        IF @@ERROR!=0 GOTO ELIMINAR_" & vbCrLf
sConsulta = sConsulta & "      END " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "      DECLARE @OFEATRIB BIT" & vbCrLf
sConsulta = sConsulta & "      DECLARE @OFEGRUPOATRIB BIT" & vbCrLf
sConsulta = sConsulta & "      DECLARE @OFEITEMATRIB BIT" & vbCrLf
sConsulta = sConsulta & "      DECLARE @PAID INT" & vbCrLf
sConsulta = sConsulta & "      DECLARE @AMBITO SMALLINT" & vbCrLf
sConsulta = sConsulta & "      DECLARE @TIPO SMALLINT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "      DECLARE CATRIBS CURSOR LOCAL FORWARD_ONLY  FOR " & vbCrLf
sConsulta = sConsulta & "      SELECT PA.ID, AMBITO, GRUPO, TIPO" & vbCrLf
sConsulta = sConsulta & "        FROM PROCE_ATRIB PA" & vbCrLf
sConsulta = sConsulta & "         INNER JOIN DEF_ATRIB DA" & vbCrLf
sConsulta = sConsulta & "                 ON PA.ATRIB = DA.ID" & vbCrLf
sConsulta = sConsulta & "       WHERE ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "         AND GMN1= @GMN1" & vbCrLf
sConsulta = sConsulta & "         AND PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "         AND INTERNO = 0" & vbCrLf
sConsulta = sConsulta & "         AND OBLIG = 1" & vbCrLf
sConsulta = sConsulta & "      " & vbCrLf
sConsulta = sConsulta & "      OPEN CATRIBS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "      FETCH NEXT FROM CATRIBS INTO @PAID, @AMBITO, @GRUPO,  @TIPO" & vbCrLf
sConsulta = sConsulta & "      SET @OFEATRIB=0" & vbCrLf
sConsulta = sConsulta & "      SET @OFEGRUPOATRIB=0" & vbCrLf
sConsulta = sConsulta & "      SET @OFEITEMATRIB=0" & vbCrLf
sConsulta = sConsulta & "      WHILE @@FETCH_STATUS=0 AND @OFEATRIB=0 AND @OFEGRUPOATRIB=0 AND @OFEITEMATRIB=0" & vbCrLf
sConsulta = sConsulta & "        BEGIN" & vbCrLf
sConsulta = sConsulta & "          IF @AMBITO = 1" & vbCrLf
sConsulta = sConsulta & "            BEGIN" & vbCrLf
sConsulta = sConsulta & "              SET @OFEATRIB =1" & vbCrLf
sConsulta = sConsulta & "              SELECT @OFEATRIB = CASE " & vbCrLf
sConsulta = sConsulta & "                        WHEN CASE @TIPO " & vbCrLf
sConsulta = sConsulta & "                                WHEN 1 THEN CASE WHEN VALOR_TEXT ='' THEN NULL ELSE VALOR_TEXT END " & vbCrLf
sConsulta = sConsulta & "                                WHEN 2 THEN CAST(VALOR_NUM AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                                WHEN 3 THEN CAST(VALOR_FEC AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                                WHEN 4 THEN CAST(VALOR_BOOL AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                             END IS NULL " & vbCrLf
sConsulta = sConsulta & "                        THEN 1 " & vbCrLf
sConsulta = sConsulta & "                        ELSE 0 " & vbCrLf
sConsulta = sConsulta & "                     END" & vbCrLf
sConsulta = sConsulta & "                FROM P_OFE_ATRIB" & vbCrLf
sConsulta = sConsulta & "               WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "                 AND ATRIB_ID = @PAID" & vbCrLf
sConsulta = sConsulta & "            END" & vbCrLf
sConsulta = sConsulta & "          IF @AMBITO = 2" & vbCrLf
sConsulta = sConsulta & "            BEGIN" & vbCrLf
sConsulta = sConsulta & "              SET @OFEGRUPOATRIB =0" & vbCrLf
sConsulta = sConsulta & "              IF @GRUPO IS NULL " & vbCrLf
sConsulta = sConsulta & "                BEGIN" & vbCrLf
sConsulta = sConsulta & "                IF (SELECT COUNT(*) " & vbCrLf
sConsulta = sConsulta & "                      FROM P_OFE_GR_ATRIB " & vbCrLf
sConsulta = sConsulta & "                     WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "                       AND ATRIB_ID = @PAID" & vbCrLf
sConsulta = sConsulta & "                       AND CASE @TIPO " & vbCrLf
sConsulta = sConsulta & "                                WHEN 1 THEN CASE WHEN VALOR_TEXT ='' THEN NULL ELSE VALOR_TEXT END " & vbCrLf
sConsulta = sConsulta & "                                WHEN 2 THEN CAST(VALOR_NUM AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                                WHEN 3 THEN CAST(VALOR_FEC AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                                WHEN 4 THEN CAST(VALOR_BOOL AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                             END IS NOT NULL) <> (SELECT COUNT(*) " & vbCrLf
sConsulta = sConsulta & "                       FROM PROCE_GRUPO " & vbCrLf
sConsulta = sConsulta & "                      WHERE ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "                        AND GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "                        AND PROCE = @PROCE)" & vbCrLf
sConsulta = sConsulta & "                   SET @OFEGRUPOATRIB = 1" & vbCrLf
sConsulta = sConsulta & "                 END" & vbCrLf
sConsulta = sConsulta & "               ELSE" & vbCrLf
sConsulta = sConsulta & "                IF (SELECT COUNT(*) " & vbCrLf
sConsulta = sConsulta & "                      FROM P_OFE_GR_ATRIB " & vbCrLf
sConsulta = sConsulta & "                     WHERE GRUPO = @GRUPO" & vbCrLf
sConsulta = sConsulta & "                       AND ATRIB_ID = @PAID" & vbCrLf
sConsulta = sConsulta & "                       AND CASE @TIPO " & vbCrLf
sConsulta = sConsulta & "                                WHEN 1 THEN CASE WHEN VALOR_TEXT ='' THEN NULL ELSE VALOR_TEXT END " & vbCrLf
sConsulta = sConsulta & "                                WHEN 2 THEN CAST(VALOR_NUM AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                                WHEN 3 THEN CAST(VALOR_FEC AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                                WHEN 4 THEN CAST(VALOR_BOOL AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                             END IS NOT NULL)  =0 " & vbCrLf
sConsulta = sConsulta & "                       SET @OFEGRUPOATRIB = 1" & vbCrLf
sConsulta = sConsulta & "            END" & vbCrLf
sConsulta = sConsulta & "          IF @AMBITO = 3" & vbCrLf
sConsulta = sConsulta & "            BEGIN" & vbCrLf
sConsulta = sConsulta & "              SET @OFEITEMATRIB =0" & vbCrLf
sConsulta = sConsulta & "              IF @GRUPO IS NULL " & vbCrLf
sConsulta = sConsulta & "                BEGIN" & vbCrLf
sConsulta = sConsulta & "                  IF (SELECT COUNT(*) " & vbCrLf
sConsulta = sConsulta & "                        FROM P_OFE_ITEM_ATRIB " & vbCrLf
sConsulta = sConsulta & "                       WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "                         AND ATRIB_ID = @PAID" & vbCrLf
sConsulta = sConsulta & "                         AND CASE @TIPO " & vbCrLf
sConsulta = sConsulta & "                                WHEN 1 THEN CASE WHEN VALOR_TEXT ='' THEN NULL ELSE VALOR_TEXT END " & vbCrLf
sConsulta = sConsulta & "                                WHEN 2 THEN CAST(VALOR_NUM AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                                WHEN 3 THEN CAST(VALOR_FEC AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                                WHEN 4 THEN CAST(VALOR_BOOL AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                     END IS NOT NULL)  <>" & vbCrLf
sConsulta = sConsulta & "                      (SELECT COUNT(*) " & vbCrLf
sConsulta = sConsulta & "                         FROM ITEM " & vbCrLf
sConsulta = sConsulta & "                        WHERE ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "                          AND GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "                          AND PROCE = @PROCE                    " & vbCrLf
sConsulta = sConsulta & "                          AND CONF = 1)" & vbCrLf
sConsulta = sConsulta & "                       SET @OFEITEMATRIB = 1" & vbCrLf
sConsulta = sConsulta & "                 END" & vbCrLf
sConsulta = sConsulta & "              ELSE" & vbCrLf
sConsulta = sConsulta & "                 begin" & vbCrLf
sConsulta = sConsulta & "                  IF (SELECT COUNT(*) " & vbCrLf
sConsulta = sConsulta & "                        FROM P_OFE_ITEM_ATRIB" & vbCrLf
sConsulta = sConsulta & "                       WHERE GRUPO = @GRUPO" & vbCrLf
sConsulta = sConsulta & "                         AND ID = @ID" & vbCrLf
sConsulta = sConsulta & "                         AND ATRIB_ID = @PAID" & vbCrLf
sConsulta = sConsulta & "                         AND CASE @TIPO " & vbCrLf
sConsulta = sConsulta & "                                WHEN 1 THEN CASE WHEN VALOR_TEXT ='' THEN NULL ELSE VALOR_TEXT END " & vbCrLf
sConsulta = sConsulta & "                                WHEN 2 THEN CAST(VALOR_NUM AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                                WHEN 3 THEN CAST(VALOR_FEC AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                                WHEN 4 THEN CAST(VALOR_BOOL AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                             END IS NOT NULL)  <>" & vbCrLf
sConsulta = sConsulta & "                      (SELECT COUNT(*) " & vbCrLf
sConsulta = sConsulta & "                         FROM ITEM " & vbCrLf
sConsulta = sConsulta & "                        WHERE ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "                          AND GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "                          AND PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "                          AND GRUPO = @GRUPO" & vbCrLf
sConsulta = sConsulta & "                          AND CONF = 1)" & vbCrLf
sConsulta = sConsulta & "                       SET @OFEITEMATRIB = 1" & vbCrLf
sConsulta = sConsulta & "                   end" & vbCrLf
sConsulta = sConsulta & "            END" & vbCrLf
sConsulta = sConsulta & "            FETCH NEXT FROM CATRIBS INTO @PAID, @AMBITO, @GRUPO,  @TIPO" & vbCrLf
sConsulta = sConsulta & "          END" & vbCrLf
sConsulta = sConsulta & "      CLOSE CATRIBS" & vbCrLf
sConsulta = sConsulta & "      DEALLOCATE CATRIBS" & vbCrLf
sConsulta = sConsulta & "      IF @OFEATRIB = 1" & vbCrLf
sConsulta = sConsulta & "        SET @ERROR = 2" & vbCrLf
sConsulta = sConsulta & "      IF @OFEGRUPOATRIB = 1" & vbCrLf
sConsulta = sConsulta & "        SET @ERROR = 3" & vbCrLf
sConsulta = sConsulta & "      IF @OFEITEMATRIB = 1" & vbCrLf
sConsulta = sConsulta & "        SET @ERROR = 4" & vbCrLf
sConsulta = sConsulta & "      IF @ERROR>0" & vbCrLf
sConsulta = sConsulta & "        GOTO ELIMINAR_" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "DECLARE  @ESTWEB VARCHAR(3) " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "SELECT @ESTWEB=ESTWEB FROM PARGEN_DEF INNER JOIN PARGEN_GEST ON PARGEN_DEF.ID=PARGEN_GEST.ID  " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN TRANSACTION" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE SET ULT= 0, LEIDO = 1  WHERE ANYO = @ANYO AND GMN1 = @GMN1 AND PROCE = @PROCE AND PROVE = @PROVE AND ULT = 1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @NEWOFE = ISNULL(MAX(OFE),0) + 1 FROM PROCE_OFE WHERE ANYO = @ANYO AND GMN1 = @GMN1 AND PROCE = @PROCE AND PROVE = @PROVE " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO PROCE_OFE(ANYO, GMN1, PROCE, PROVE, OFE, EST, FECREC, FECVAL, MON, CAMBIO, OBS, LEIDO, ULT, ENVIADA, OBSADJUN)" & vbCrLf
sConsulta = sConsulta & "SELECT @ANYO, @GMN1, @PROCE, @PROVE, @NEWOFE, @ESTWEB, FECREC, FECVAL, MON, CAMBIO, OBS, 0, 1, 1, OBSADJUN" & vbCrLf
sConsulta = sConsulta & "  FROM P_PROCE_OFE" & vbCrLf
sConsulta = sConsulta & "WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE GRUPO_OFE " & vbCrLf
sConsulta = sConsulta & "   SET OBSADJUN = PGO.OBSADJUN" & vbCrLf
sConsulta = sConsulta & "  FROM GRUPO_OFE GO" & vbCrLf
sConsulta = sConsulta & "      INNER JOIN P_GRUPO_OFE PGO" & vbCrLf
sConsulta = sConsulta & "              ON GO.GRUPO = PGO.GRUPO" & vbCrLf
sConsulta = sConsulta & " WHERE GO.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "   AND GO.GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "   AND GO.PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "   AND GO.PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "   AND GO.OFE = @NEWOFE" & vbCrLf
sConsulta = sConsulta & "   AND PGO.ID = @ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_OFE" & vbCrLf
sConsulta = sConsulta & "   SET PRECIO = PIO.PRECIO," & vbCrLf
sConsulta = sConsulta & "       CANTMAX = PIO.CANTMAX," & vbCrLf
sConsulta = sConsulta & "       PRECIO2 = PIO.PRECIO2," & vbCrLf
sConsulta = sConsulta & "       PRECIO3 = PIO.PRECIO3," & vbCrLf
sConsulta = sConsulta & "       PREC_VALIDO = PIO.PRECIO," & vbCrLf
sConsulta = sConsulta & "       OBSADJUN = PIO.OBSADJUN," & vbCrLf
sConsulta = sConsulta & "       COMENT1 = PIO.COMENT1," & vbCrLf
sConsulta = sConsulta & "       COMENT2 = PIO.COMENT2," & vbCrLf
sConsulta = sConsulta & "       COMENT3 = PIO.COMENT3" & vbCrLf
sConsulta = sConsulta & "  FROM ITEM_OFE IO" & vbCrLf
sConsulta = sConsulta & "     INNER JOIN P_ITEM_OFE PIO" & vbCrLf
sConsulta = sConsulta & "             ON IO.ITEM = PIO.ITEM" & vbCrLf
sConsulta = sConsulta & " WHERE IO.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "   AND IO.GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "   AND IO.PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "   AND IO.PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "   AND IO.NUM = @NEWOFE" & vbCrLf
sConsulta = sConsulta & "   AND PIO.ID = @ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_ITEM_ADJUN " & vbCrLf
sConsulta = sConsulta & "  FROM OFE_ITEM_ADJUN R" & vbCrLf
sConsulta = sConsulta & " WHERE R.ANYO = @ANYO " & vbCrLf
sConsulta = sConsulta & "   AND R.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "   AND R.PROCE= @PROCE " & vbCrLf
sConsulta = sConsulta & "   AND R.PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "   AND R.OFE IN (@NEWOFE,-1)" & vbCrLf
sConsulta = sConsulta & "   AND EXISTS (SELECT * FROM P_OFE_ITEM_ADJUN P " & vbCrLf
sConsulta = sConsulta & "                WHERE P.ID  = @ID " & vbCrLf
sConsulta = sConsulta & "                  AND P.ITEM = R.ITEM" & vbCrLf
sConsulta = sConsulta & "                  AND P.IDGS = R.ID)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_ITEM_ATRIB " & vbCrLf
sConsulta = sConsulta & "  FROM OFE_ITEM_ATRIB R" & vbCrLf
sConsulta = sConsulta & " WHERE R.ANYO = @ANYO " & vbCrLf
sConsulta = sConsulta & "   AND R.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "   AND R.PROCE= @PROCE " & vbCrLf
sConsulta = sConsulta & "   AND R.PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "   AND R.OFE IN (@NEWOFE,-1)" & vbCrLf
sConsulta = sConsulta & "   AND EXISTS (SELECT * FROM P_OFE_ITEM_ATRIB P " & vbCrLf
sConsulta = sConsulta & "                WHERE P.ID  = @ID " & vbCrLf
sConsulta = sConsulta & "                  AND P.ITEM = R.ITEM" & vbCrLf
sConsulta = sConsulta & "                  AND P.ATRIB_ID = R.ATRIB_ID)" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_GR_ATRIB " & vbCrLf
sConsulta = sConsulta & "  FROM OFE_GR_ATRIB R" & vbCrLf
sConsulta = sConsulta & " WHERE R.ANYO = @ANYO " & vbCrLf
sConsulta = sConsulta & "   AND R.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "   AND R.PROCE= @PROCE " & vbCrLf
sConsulta = sConsulta & "   AND R.PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "   AND R.OFE IN (@NEWOFE,-1)" & vbCrLf
sConsulta = sConsulta & "   AND EXISTS (SELECT * FROM P_OFE_GR_ATRIB P " & vbCrLf
sConsulta = sConsulta & "                WHERE P.ID  = @ID " & vbCrLf
sConsulta = sConsulta & "                  AND CAST(P.GRUPO AS VARCHAR(50)) = CAST(R.GRUPO AS VARCHAR(50))" & vbCrLf
sConsulta = sConsulta & "                  AND P.ATRIB_ID = R.ATRIB_ID)" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_GR_ADJUN " & vbCrLf
sConsulta = sConsulta & "  FROM OFE_GR_ADJUN R" & vbCrLf
sConsulta = sConsulta & " WHERE R.ANYO = @ANYO " & vbCrLf
sConsulta = sConsulta & "   AND R.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "   AND R.PROCE= @PROCE " & vbCrLf
sConsulta = sConsulta & "   AND R.PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "   AND R.OFE IN (@NEWOFE,-1)" & vbCrLf
sConsulta = sConsulta & "   AND EXISTS (SELECT * FROM P_OFE_GR_ADJUN P " & vbCrLf
sConsulta = sConsulta & "                WHERE P.ID  = @ID " & vbCrLf
sConsulta = sConsulta & "                  AND P.GRUPO = R.GRUPO" & vbCrLf
sConsulta = sConsulta & "                  AND P.IDGS = R.ID)" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_ATRIB " & vbCrLf
sConsulta = sConsulta & "  FROM OFE_ATRIB R" & vbCrLf
sConsulta = sConsulta & " WHERE R.ANYO = @ANYO " & vbCrLf
sConsulta = sConsulta & "   AND R.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "   AND R.PROCE= @PROCE " & vbCrLf
sConsulta = sConsulta & "   AND R.PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "   AND R.OFE IN (@NEWOFE,-1)" & vbCrLf
sConsulta = sConsulta & "   AND EXISTS (SELECT * FROM P_OFE_ATRIB P " & vbCrLf
sConsulta = sConsulta & "                WHERE P.ID  = @ID " & vbCrLf
sConsulta = sConsulta & "                  AND P.ATRIB_ID = R.ATRIB_ID)" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM PROCE_OFE_ADJUN " & vbCrLf
sConsulta = sConsulta & "  FROM PROCE_OFE_ADJUN R" & vbCrLf
sConsulta = sConsulta & " WHERE R.ANYO = @ANYO " & vbCrLf
sConsulta = sConsulta & "   AND R.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "   AND R.PROCE= @PROCE " & vbCrLf
sConsulta = sConsulta & "   AND R.PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "   AND R.OFE IN (@NEWOFE,-1)" & vbCrLf
sConsulta = sConsulta & "   AND EXISTS (SELECT * FROM P_PROCE_OFE_ADJUN P " & vbCrLf
sConsulta = sConsulta & "                WHERE P.ID  = @ID " & vbCrLf
sConsulta = sConsulta & "                  AND P.IDGS = R.ID)" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM ITEM_OFE WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE= @PROCE and PROVE=@PROVE AND NUM = -1" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM GRUPO_OFE WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE= @PROCE and PROVE=@PROVE AND OFE = -1" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM PROCE_OFE WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE= @PROCE and PROVE=@PROVE AND OFE = -1" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR LOCAL FOR" & vbCrLf
sConsulta = sConsulta & "SELECT ADJUN, IDGS FROM P_PROCE_OFE_ADJUN WHERE ID=@ID AND IDGS IS NULL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "OPEN C" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C INTO @ADJUN, @IDGS" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    SELECT @IDADJUN = isnull(MAX(ID),0) + 1 FROM ADJUN " & vbCrLf
sConsulta = sConsulta & "    INSERT INTO ADJUN (ID, WEB) VALUES (@IDADJUN, 1) " & vbCrLf
sConsulta = sConsulta & "    UPDATE P_PROCE_OFE_ADJUN SET IDGS = @IDADJUN WHERE ID = @ID AND ADJUN = @ADJUN" & vbCrLf
sConsulta = sConsulta & "    IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "    FETCH NEXT FROM C INTO @ADJUN, @IDGS" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "CLOSE C" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO PROCE_OFE_ADJUN(ANYO, GMN1, PROCE, PROVE, OFE, ID, NOM, COM, IDPORTAL, DATASIZE)" & vbCrLf
sConsulta = sConsulta & "SELECT @ANYO, @GMN1, @PROCE, @PROVE, @NEWOFE, POA.IDGS, POA.NOM, POA.COM, POA.IDPORTAL, POA.DATASIZE" & vbCrLf
sConsulta = sConsulta & "  FROM P_PROCE_OFE_ADJUN POA" & vbCrLf
sConsulta = sConsulta & "         INNER JOIN ADJUN A" & vbCrLf
sConsulta = sConsulta & "                 ON POA.IDGS = A.ID" & vbCrLf
sConsulta = sConsulta & "WHERE POA.ID = @ID" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR LOCAL FOR" & vbCrLf
sConsulta = sConsulta & "SELECT ADJUN, IDGS, GRUPO FROM P_OFE_GR_ADJUN WHERE ID=@ID AND IDGS IS NULL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "OPEN C" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C INTO @ADJUN, @IDGS, @GRUPO" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    SELECT @IDADJUN = isnull(MAX(ID),0) + 1 FROM ADJUN " & vbCrLf
sConsulta = sConsulta & "    INSERT INTO ADJUN (ID, WEB) VALUES (@IDADJUN, 1) " & vbCrLf
sConsulta = sConsulta & "    UPDATE P_OFE_GR_ADJUN SET IDGS = @IDADJUN WHERE ID = @ID AND ADJUN = @ADJUN AND GRUPO = @GRUPO" & vbCrLf
sConsulta = sConsulta & "    IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "    FETCH NEXT FROM C INTO @ADJUN, @IDGS, @GRUPO" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "CLOSE C" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO OFE_GR_ADJUN(ANYO, GMN1, PROCE, GRUPO, PROVE, OFE, ID, NOM, COM, IDPORTAL, DATASIZE)" & vbCrLf
sConsulta = sConsulta & "SELECT @ANYO, @GMN1, @PROCE, OGA.GRUPO, @PROVE, @NEWOFE, OGA.IDGS, OGA.NOM, OGA.COM, OGA.IDPORTAL, OGA.DATASIZE" & vbCrLf
sConsulta = sConsulta & "  FROM P_OFE_GR_ADJUN OGA" & vbCrLf
sConsulta = sConsulta & "         INNER JOIN ADJUN A" & vbCrLf
sConsulta = sConsulta & "                 ON OGA.IDGS = A.ID" & vbCrLf
sConsulta = sConsulta & " WHERE OGA.ID = @ID" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR LOCAL FOR" & vbCrLf
sConsulta = sConsulta & "SELECT ADJUN, IDGS, ITEM FROM P_OFE_ITEM_ADJUN WHERE ID=@ID AND IDGS IS NULL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "OPEN C" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C INTO @ADJUN, @IDGS, @ITEM" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    SELECT @IDADJUN = isnull(MAX(ID),0) + 1 FROM ADJUN " & vbCrLf
sConsulta = sConsulta & "    INSERT INTO ADJUN (ID, WEB) VALUES (@IDADJUN, 1) " & vbCrLf
sConsulta = sConsulta & "    UPDATE P_OFE_ITEM_ADJUN SET IDGS = @IDADJUN WHERE ID = @ID AND ADJUN = @ADJUN AND ITEM = @ITEM" & vbCrLf
sConsulta = sConsulta & "    IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "    FETCH NEXT FROM C INTO @ADJUN, @IDGS, @ITEM" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "CLOSE C" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO OFE_ITEM_ADJUN(ANYO, GMN1, PROCE, ITEM, PROVE, OFE, ID, NOM, COM, IDPORTAL, DATASIZE)" & vbCrLf
sConsulta = sConsulta & "SELECT @ANYO, @GMN1, @PROCE, OIA.ITEM, @PROVE, @NEWOFE, OIA.IDGS, OIA.NOM, OIA.COM, OIA.IDPORTAL, OIA.DATASIZE" & vbCrLf
sConsulta = sConsulta & "  FROM P_OFE_ITEM_ADJUN OIA" & vbCrLf
sConsulta = sConsulta & "         INNER JOIN ADJUN A" & vbCrLf
sConsulta = sConsulta & "                 ON OIA.IDGS = A.ID" & vbCrLf
sConsulta = sConsulta & "WHERE OIA.ID = @ID" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO OFE_GR_ATRIB(ANYO, GMN1, PROCE, GRUPO, PROVE, OFE, ATRIB_ID, VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL)" & vbCrLf
sConsulta = sConsulta & "SELECT @ANYO, @GMN1, @PROCE, GRUPO, @PROVE, @NEWOFE, ATRIB_ID, VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL" & vbCrLf
sConsulta = sConsulta & "  FROM P_OFE_GR_ATRIB" & vbCrLf
sConsulta = sConsulta & " WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO OFE_ITEM_ATRIB(ANYO, GMN1, PROCE, ITEM, PROVE, OFE, ATRIB_ID, VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL)" & vbCrLf
sConsulta = sConsulta & "SELECT @ANYO, @GMN1, @PROCE, ITEM, @PROVE, @NEWOFE, ATRIB_ID, VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL" & vbCrLf
sConsulta = sConsulta & "  FROM P_OFE_ITEM_ATRIB" & vbCrLf
sConsulta = sConsulta & " WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO OFE_ATRIB(ANYO, GMN1, PROCE, PROVE, OFE, ATRIB_ID, VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL)" & vbCrLf
sConsulta = sConsulta & "SELECT @ANYO, @GMN1, @PROCE, @PROVE, @NEWOFE, ATRIB_ID, VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL" & vbCrLf
sConsulta = sConsulta & "  FROM P_OFE_ATRIB " & vbCrLf
sConsulta = sConsulta & " WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE SET OFE = 1  WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE = @PROCE AND PROVE=@PROVE AND OFE = 0" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @NUMOFEWEB = isnull(MAX(OFEWEB),0)+1 FROM PROCE_OFE_WEB WHERE ANYO = @ANYO AND GMN1 = @GMN1 AND PROCE = @PROCE AND PROVE = @PROVE " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO PROCE_OFE_WEB (ANYO, GMN1, PROCE, PROVE, OFEWEB, OFE, FECREC, FECVAL, MON)" & vbCrLf
sConsulta = sConsulta & "SELECT ANYO, GMN1, PROCE, PROVE, @NUMOFEWEB, OFE, FECREC,FECVAL,MON " & vbCrLf
sConsulta = sConsulta & "FROM PROCE_OFE " & vbCrLf
sConsulta = sConsulta & "WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE = @PROCE AND PROVE = @PROVE AND OFE = @NEWOFE" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO ITEM_OFE_WEB (ANYO, GMN1, PROCE, ITEM, PROVE, NUM, PRECIO, CANTMAX)" & vbCrLf
sConsulta = sConsulta & "SELECT ANYO, GMN1, PROCE, ITEM, PROVE, @NUMOFEWEB, PRECIO, CANTMAX" & vbCrLf
sConsulta = sConsulta & "FROM ITEM_OFE" & vbCrLf
sConsulta = sConsulta & "WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE = @PROCE AND PROVE = @PROVE AND NUM = @NEWOFE" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO PROCE_OFE_WEB_ADJUN (ANYO, GMN1, PROCE, PROVE, OFEWEB, ID, NOM, COM, IDPORTAL)" & vbCrLf
sConsulta = sConsulta & "SELECT ANYO, GMN1, PROCE, PROVE, @NUMOFEWEB, ID, NOM, COM, IDPORTAL" & vbCrLf
sConsulta = sConsulta & "FROM PROCE_OFE_ADJUN" & vbCrLf
sConsulta = sConsulta & "WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE = @PROCE AND PROVE = @PROVE AND OFE = @NEWOFE" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_OFE SET ULT = 0 WHERE ANYO = @ANYO AND GMN1 = @GMN1 AND PROCE =@PROCE AND PROVE=@PROVE AND NUM!=@NEWOFE" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "COMMIT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "GOTO ELIMINAR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ERROR_:" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ROLLBACK" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ELIMINAR_:" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC NUM_PROCE_PUB @PROVE =@PROVE ,@NUM = @NUM OUTPUT,@NUMNUE = @NUMNUE OUTPUT ,@NUMAREV = @NUMAREV OUTPUT " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM P_PROCE_OFE WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM P_GRUPO_OFE WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM P_ITEM_OFE WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM P_OFE_ATRIB WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM P_OFE_GR_ATRIB WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM P_OFE_ITEM_ATRIB WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM P_PROCE_OFE_ADJUN WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM P_OFE_GR_ADJUN WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM P_OFE_ITEM_ADJUN WHERE ID=@ID" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Private Sub V_2_11_5_Storeds3_02()
Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_NUM_ARTICULOS_CATALOGO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_NUM_ARTICULOS_CATALOGO]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSEP_NUM_ARTICULOS_CATALOGO(@COD VARCHAR(40), @DEN VARCHAR(300), @PROVE VARCHAR(300)," & vbCrLf
sConsulta = sConsulta & "@CATN1 INTEGER, @CATN2 INTEGER, @CATN3 INTEGER, @CATN4 INTEGER, " & vbCrLf
sConsulta = sConsulta & "@CATN5 INTEGER, @PRECIOMAX FLOAT,@PER VARCHAR(50),@MAXLINEAS INTEGER,@COINCIDENCIA INTEGER=NULL)  AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PARAM NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING='" & vbCrLf
sConsulta = sConsulta & "SELECT catalog_lin.art_int + ITEM.ART +  convert(varchar,catalog_lin.ID)  " & vbCrLf
sConsulta = sConsulta & "  FROM CATALOG_LIN INNER JOIN PROVE ON CATALOG_LIN.PROVE=PROVE.COD'" & vbCrLf
sConsulta = sConsulta & "IF @PROVE is not null" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' " & vbCrLf
sConsulta = sConsulta & "   AND PROVE.COD = @PROVE'" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING= @SQLSTRING + ' " & vbCrLf
sConsulta = sConsulta & "          INNER JOIN APROB_LIM " & vbCrLf
sConsulta = sConsulta & "                  ON CATALOG_LIN.SEGURIDAD=APROB_LIM.SEGURIDAD " & vbCrLf
sConsulta = sConsulta & "                 AND APROB_LIM.PER=@PER'" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING= @SQLSTRING + ' " & vbCrLf
sConsulta = sConsulta & " LEFT JOIN ITEM ON CATALOG_LIN.ANYO=ITEM.ANYO AND CATALOG_LIN.PROCE=ITEM.PROCE AND CATALOG_LIN.GMN1=ITEM.GMN1 AND CATALOG_LIN.ITEM=ITEM.ID'" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING= @SQLSTRING + ' " & vbCrLf
sConsulta = sConsulta & " LEFT JOIN ART4 ON CATALOG_LIN.GMN1=ART4.GMN1 AND CATALOG_LIN.GMN2=ART4.GMN2 AND CATALOG_LIN.GMN3=ART4.GMN3 AND " & vbCrLf
sConsulta = sConsulta & " CATALOG_LIN.GMN4=ART4.GMN4 AND CATALOG_LIN.ART_INT=ART4.COD '" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING= @SQLSTRING + '" & vbCrLf
sConsulta = sConsulta & " LEFT JOIN ART4 ART_AUX ON ITEM.GMN1=ART_AUX.GMN1 AND ITEM.GMN2=ART_AUX.GMN2 AND ITEM.GMN3=ART_AUX.GMN3 AND ITEM.GMN4=ART_AUX.GMN4 AND ITEM.ART=ART_AUX.COD '" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING= @SQLSTRING +'  " & vbCrLf
sConsulta = sConsulta & " WHERE PUB=1  AND BAJALOG = 0 '" & vbCrLf
sConsulta = sConsulta & "IF @COD is not null" & vbCrLf
sConsulta = sConsulta & " BEGIN" & vbCrLf
sConsulta = sConsulta & "  IF @COINCIDENCIA=0 " & vbCrLf
sConsulta = sConsulta & "   SET @SQLSTRING= @SQLSTRING +' AND (CATALOG_LIN.ART_INT + ITEM.ART LIKE ''% ''  +  @COD  + '' %''' +" & vbCrLf
sConsulta = sConsulta & "   ' OR CATALOG_LIN.ART_INT + ITEM.ART LIKE @COD  + '' %''' +" & vbCrLf
sConsulta = sConsulta & "   ' OR CATALOG_LIN.ART_INT + ITEM.ART LIKE ''% '' + @COD  ' + " & vbCrLf
sConsulta = sConsulta & "   ' OR CATALOG_LIN.ART_INT + ITEM.ART=@COD)'" & vbCrLf
sConsulta = sConsulta & "  ELSE" & vbCrLf
sConsulta = sConsulta & "   SET @SQLSTRING= @SQLSTRING +' AND CATALOG_LIN.ART_INT + ITEM.ART LIKE  + ''%'' + @COD + ''%'''" & vbCrLf
sConsulta = sConsulta & " END" & vbCrLf
sConsulta = sConsulta & "IF @DEN is not null" & vbCrLf
sConsulta = sConsulta & " BEGIN" & vbCrLf
sConsulta = sConsulta & "   IF @COINCIDENCIA=0 " & vbCrLf
sConsulta = sConsulta & "   SET @SQLSTRING = @SQLSTRING + ' AND (ITEM.DESCR + ART4.DEN + ART_AUX.DEN LIKE ''% ''  +  @DEN  + '' %''' +" & vbCrLf
sConsulta = sConsulta & "   ' OR ITEM.DESCR + ART4.DEN + ART_AUX.DEN LIKE @DEN  + '' %''' +" & vbCrLf
sConsulta = sConsulta & "   ' OR ITEM.DESCR + ART4.DEN + ART_AUX.DEN LIKE ''% '' + @DEN ' + " & vbCrLf
sConsulta = sConsulta & "   ' OR ITEM.DESCR + ART4.DEN + ART_AUX.DEN=@DEN )'" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "        SET @SQLSTRING = @SQLSTRING + ' AND ITEM.DESCR + ART4.DEN + ART_AUX.DEN LIKE ''%'' + @DEN + ''%'''" & vbCrLf
sConsulta = sConsulta & " END" & vbCrLf
sConsulta = sConsulta & "IF @CATN1 is not null" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING = @SQLSTRING + ' " & vbCrLf
sConsulta = sConsulta & "   AND CATALOG_LIN.CAT1=@CATN1'" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "IF @CATN2 is not null" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING = @SQLSTRING + ' " & vbCrLf
sConsulta = sConsulta & "   AND CATALOG_LIN.CAT2=@CATN2'" & vbCrLf
sConsulta = sConsulta & "IF @CATN3 is not null" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING = @SQLSTRING + ' " & vbCrLf
sConsulta = sConsulta & "   AND CATALOG_LIN.CAT3=@CATN3'" & vbCrLf
sConsulta = sConsulta & "IF @CATN4 is not null" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING = @SQLSTRING + ' " & vbCrLf
sConsulta = sConsulta & "   AND CATALOG_LIN.CAT4=@CATN4'" & vbCrLf
sConsulta = sConsulta & "IF @CATN5 is not null" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING = @SQLSTRING + '" & vbCrLf
sConsulta = sConsulta & "   AND CATALOG_LIN.CAT5=@CATN5'" & vbCrLf
sConsulta = sConsulta & "IF @PRECIOMAX is not null" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING = @SQLSTRING + '" & vbCrLf
sConsulta = sConsulta & "   AND CATALOG_LIN.PRECIO_UC<=@PRECIOMAX'" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING = @SQLSTRING + '" & vbCrLf
sConsulta = sConsulta & "ORDER BY catalog_lin.art_int + ITEM.ART +  convert(varchar,catalog_lin.ID) '" & vbCrLf
sConsulta = sConsulta & "SET @PARAM = '@COD VARCHAR(40), @DEN VARCHAR(300), @PROVE VARCHAR(300)," & vbCrLf
sConsulta = sConsulta & "@CATN1 INTEGER, @CATN2 INTEGER, @CATN3 INTEGER, @CATN4 INTEGER, " & vbCrLf
sConsulta = sConsulta & "@CATN5 INTEGER, @PRECIOMAX FLOAT,@PER VARCHAR(50)'" & vbCrLf
sConsulta = sConsulta & "SET @SQL = '" & vbCrLf
sConsulta = sConsulta & "create table #IDS (id varchar(200))" & vbCrLf
sConsulta = sConsulta & "set nocount on" & vbCrLf
sConsulta = sConsulta & "DECLARE C1 CURSOR FOR " & vbCrLf
sConsulta = sConsulta & "' + @SQLSTRING + '" & vbCrLf
sConsulta = sConsulta & "DECLARE @miID varchar(200)" & vbCrLf
sConsulta = sConsulta & "declare @cont int" & vbCrLf
sConsulta = sConsulta & "set @cont=1" & vbCrLf
sConsulta = sConsulta & "OPEN C1" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C1 INTO @miID" & vbCrLf
sConsulta = sConsulta & "while @@fetch_status=0" & vbCrLf
sConsulta = sConsulta & "begin" & vbCrLf
sConsulta = sConsulta & "if @cont = 1" & vbCrLf
sConsulta = sConsulta & " insert into #IDS values (@miID)" & vbCrLf
sConsulta = sConsulta & "set @cont = @cont + 1        " & vbCrLf
sConsulta = sConsulta & "     " & vbCrLf
sConsulta = sConsulta & "if @cont = ' + CONVERT(VARCHAR,@MAXLINEAS) + ' + 1" & vbCrLf
sConsulta = sConsulta & " set @cont = 1" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C1 INTO @miID" & vbCrLf
sConsulta = sConsulta & "end" & vbCrLf
sConsulta = sConsulta & "CLOSE C1" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C1" & vbCrLf
sConsulta = sConsulta & "set nocount off" & vbCrLf
sConsulta = sConsulta & "select * from #ids" & vbCrLf
sConsulta = sConsulta & "'" & vbCrLf
sConsulta = sConsulta & "EXECUTE sp_executesql @SQL,@PARAM," & vbCrLf
sConsulta = sConsulta & "@COD=@COD, " & vbCrLf
sConsulta = sConsulta & "@DEN=@DEN, " & vbCrLf
sConsulta = sConsulta & "@PROVE=@PROVE," & vbCrLf
sConsulta = sConsulta & "@CATN1=@CATN1," & vbCrLf
sConsulta = sConsulta & "@CATN2=@CATN2, " & vbCrLf
sConsulta = sConsulta & "@CATN3=@CATN3," & vbCrLf
sConsulta = sConsulta & "@CATN4=@CATN4," & vbCrLf
sConsulta = sConsulta & "@CATN5=@CATN5, " & vbCrLf
sConsulta = sConsulta & "@PRECIOMAX=@PRECIOMAX, " & vbCrLf
sConsulta = sConsulta & "@PER=@PER" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_DEVOLVER_ARTICULOS_CATALOGO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_DEVOLVER_ARTICULOS_CATALOGO]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSEP_DEVOLVER_ARTICULOS_CATALOGO(@COD VARCHAR(40) = NULL, @DEN VARCHAR(300) = NULL, @PROVE VARCHAR(300) = NULL," & vbCrLf
sConsulta = sConsulta & "@CATN1 INTEGER = NULL, @CATN2 INTEGER = NULL, @CATN3 INTEGER = NULL, @CATN4 INTEGER = NULL, " & vbCrLf
sConsulta = sConsulta & "@CATN5 INTEGER = NULL, @ID varchar(200),@PRECIOMAX FLOAT = NULL,@PER VARCHAR(50)," & vbCrLf
sConsulta = sConsulta & "@MAXLINEAS INTEGER,@COINCIDENCIA INTEGER=NULL)  AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PARAM NVARCHAR(1000)" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING='" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT TOP ' + CONVERT(VARCHAR,@MAXLINEAS) +'  " & vbCrLf
sConsulta = sConsulta & "CATALOG_LIN.ID,CATALOG_LIN.ANYO,CATALOG_LIN.PROCE,CATALOG_LIN.ITEM," & vbCrLf
sConsulta = sConsulta & "CATALOG_LIN.PROVE,ART_INT + ITEM.ART COD_ITEM," & vbCrLf
sConsulta = sConsulta & "CASE WHEN ART_AUX.DEN IS NULL THEN ART4.DEN ELSE ART_AUX.DEN END + ITEM.DESCR ART_DEN," & vbCrLf
sConsulta = sConsulta & "CATALOG_LIN.DEST,UC,PROVE_ART4.COD_EXT," & vbCrLf
sConsulta = sConsulta & "CANT_ADJ,PRECIO_UC,UP_DEF,FC_DEF," & vbCrLf
sConsulta = sConsulta & "CANT_MIN_DEF,PUB,FECHA_DESPUB,CAT1,CAT2,CAT3,CAT4,CAT5,CATALOG_LIN.SEGURIDAD," & vbCrLf
sConsulta = sConsulta & "PROVE.DEN  AS PROVEDEN," & vbCrLf
sConsulta = sConsulta & "CATALOG_LIN.GMN1,CATALOG_LIN.GMN2,CATALOG_LIN.GMN3,CATALOG_LIN.GMN4,APROB_LIM.MOD_DEST," & vbCrLf
sConsulta = sConsulta & "CATALOG_LIN.ART_INT + ITEM.ART  + CONVERT(varchar,CATALOG_LIN.ID) '" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING= @SQLSTRING + ' " & vbCrLf
sConsulta = sConsulta & " FROM CATALOG_LIN " & vbCrLf
sConsulta = sConsulta & "         INNER JOIN PROVE " & vbCrLf
sConsulta = sConsulta & "            ON CATALOG_LIN.PROVE=PROVE.COD" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "LEFT JOIN ITEM " & vbCrLf
sConsulta = sConsulta & "       ON CATALOG_LIN.GMN1=ITEM.GMN1 " & vbCrLf
sConsulta = sConsulta & "      AND CATALOG_LIN.ANYO=ITEM.ANYO " & vbCrLf
sConsulta = sConsulta & "      AND CATALOG_LIN.PROCE=ITEM.PROCE " & vbCrLf
sConsulta = sConsulta & "      AND CATALOG_LIN.ITEM=ITEM.ID" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN ART4 " & vbCrLf
sConsulta = sConsulta & "       ON CATALOG_LIN.GMN1=ART4.GMN1 " & vbCrLf
sConsulta = sConsulta & "      AND CATALOG_LIN.GMN2=ART4.GMN2 " & vbCrLf
sConsulta = sConsulta & "      AND CATALOG_LIN.GMN3=ART4.GMN3 " & vbCrLf
sConsulta = sConsulta & "      AND CATALOG_LIN.GMN4=ART4.GMN4 " & vbCrLf
sConsulta = sConsulta & "      AND CATALOG_LIN.ART_INT + ITEM.ART=ART4.COD " & vbCrLf
sConsulta = sConsulta & "LEFT JOIN ART4 ART_AUX " & vbCrLf
sConsulta = sConsulta & "       ON ITEM.GMN1=ART_AUX.GMN1 " & vbCrLf
sConsulta = sConsulta & "      AND ITEM.GMN2=ART_AUX.GMN2 " & vbCrLf
sConsulta = sConsulta & "      AND ITEM.GMN3=ART_AUX.GMN3 " & vbCrLf
sConsulta = sConsulta & "      AND ITEM.GMN4=ART_AUX.GMN4 " & vbCrLf
sConsulta = sConsulta & "      AND ITEM.ART=ART_AUX.COD " & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PROVE_ART4 " & vbCrLf
sConsulta = sConsulta & "       ON CATALOG_LIN.GMN1=PROVE_ART4.GMN1 " & vbCrLf
sConsulta = sConsulta & "      AND CATALOG_LIN.GMN2=PROVE_ART4.GMN2 " & vbCrLf
sConsulta = sConsulta & "      AND CATALOG_LIN.GMN3=PROVE_ART4.GMN3 " & vbCrLf
sConsulta = sConsulta & "      AND CATALOG_LIN.GMN4=PROVE_ART4.GMN4 " & vbCrLf
sConsulta = sConsulta & "      AND CASE WHEN CATALOG_LIN.ART_INT IS NULL THEN ITEM.ART ELSE CATALOG_LIN.ART_INT  END =PROVE_ART4.ART " & vbCrLf
sConsulta = sConsulta & "      AND CATALOG_LIN.PROVE=PROVE_ART4.PROVE" & vbCrLf
sConsulta = sConsulta & "'" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING= @SQLSTRING + ' " & vbCrLf
sConsulta = sConsulta & "          INNER JOIN APROB_LIM " & vbCrLf
sConsulta = sConsulta & "                  ON CATALOG_LIN.SEGURIDAD=APROB_LIM.SEGURIDAD " & vbCrLf
sConsulta = sConsulta & "                 AND APROB_LIM.PER=@PER'" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING= @SQLSTRING +'  " & vbCrLf
sConsulta = sConsulta & " WHERE PUB=1" & vbCrLf
sConsulta = sConsulta & "   AND BAJALOG = 0" & vbCrLf
sConsulta = sConsulta & "'" & vbCrLf
sConsulta = sConsulta & "IF @PROVE is not null" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' " & vbCrLf
sConsulta = sConsulta & "           AND PROVE.COD = @PROVE'" & vbCrLf
sConsulta = sConsulta & "IF @COD is not null" & vbCrLf
sConsulta = sConsulta & " BEGIN" & vbCrLf
sConsulta = sConsulta & "  IF @COINCIDENCIA=0 " & vbCrLf
sConsulta = sConsulta & "   SET @SQLSTRING= @SQLSTRING +' AND (CATALOG_LIN.ART_INT + ITEM.ART LIKE ''% ''  +  @COD  + '' %''' +" & vbCrLf
sConsulta = sConsulta & "   ' OR CATALOG_LIN.ART_INT + ITEM.ART LIKE @COD  + '' %''' +" & vbCrLf
sConsulta = sConsulta & "   ' OR CATALOG_LIN.ART_INT + ITEM.ART LIKE ''% '' + @COD  ' + " & vbCrLf
sConsulta = sConsulta & "   ' OR CATALOG_LIN.ART_INT + ITEM.ART =@COD)'" & vbCrLf
sConsulta = sConsulta & "  ELSE" & vbCrLf
sConsulta = sConsulta & "   SET @SQLSTRING= @SQLSTRING +' AND CATALOG_LIN.ART_INT + ITEM.ART LIKE + ''%'' + @COD + ''%'''" & vbCrLf
sConsulta = sConsulta & " END" & vbCrLf
sConsulta = sConsulta & "IF @DEN is not null" & vbCrLf
sConsulta = sConsulta & " BEGIN" & vbCrLf
sConsulta = sConsulta & "   IF @COINCIDENCIA=0 " & vbCrLf
sConsulta = sConsulta & "   SET @SQLSTRING = @SQLSTRING + ' AND (ITEM.DESCR + ART4.DEN + ART_AUX.DEN LIKE ''% ''  +  @DEN  + '' %''' +" & vbCrLf
sConsulta = sConsulta & "   ' OR ITEM.DESCR + ART4.DEN + ART_AUX.DEN LIKE @DEN  + '' %''' +" & vbCrLf
sConsulta = sConsulta & "   ' OR ITEM.DESCR + ART4.DEN + ART_AUX.DEN LIKE ''% '' + @DEN ' + " & vbCrLf
sConsulta = sConsulta & "   ' OR ITEM.DESCR + ART4.DEN + ART_AUX.DEN=@DEN)'" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "        SET @SQLSTRING = @SQLSTRING + ' AND ITEM.DESCR + ART4.DEN + ART_AUX.DEN LIKE + ''%'' + @DEN + ''%'''" & vbCrLf
sConsulta = sConsulta & " END" & vbCrLf
sConsulta = sConsulta & "IF @CATN1 is not null" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING = @SQLSTRING + ' " & vbCrLf
sConsulta = sConsulta & "   AND CATALOG_LIN.CAT1=@CATN1'" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "IF @CATN2 is not null" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING = @SQLSTRING + ' " & vbCrLf
sConsulta = sConsulta & "   AND CATALOG_LIN.CAT2=@CATN2'" & vbCrLf
sConsulta = sConsulta & "IF @CATN3 is not null" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING = @SQLSTRING + ' " & vbCrLf
sConsulta = sConsulta & "   AND CATALOG_LIN.CAT3=@CATN3'" & vbCrLf
sConsulta = sConsulta & "IF @CATN4 is not null" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING = @SQLSTRING + ' " & vbCrLf
sConsulta = sConsulta & "   AND CATALOG_LIN.CAT4=@CATN4'" & vbCrLf
sConsulta = sConsulta & "IF @CATN5 is not null" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING = @SQLSTRING + '" & vbCrLf
sConsulta = sConsulta & "   AND CATALOG_LIN.CAT5=@CATN5'" & vbCrLf
sConsulta = sConsulta & "IF @PRECIOMAX is not null" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING = @SQLSTRING + '" & vbCrLf
sConsulta = sConsulta & "   AND CATALOG_LIN.PRECIO_UC<=@PRECIOMAX'" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING = @SQLSTRING + '" & vbCrLf
sConsulta = sConsulta & "   AND isnull(catalog_lin.art_int,'''') + isnull(ITEM.ART,'''') +  convert(varchar,isnull(catalog_lin.ID,'''')) >=@ID'" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING = @SQLSTRING + '" & vbCrLf
sConsulta = sConsulta & "ORDER BY CATALOG_LIN.ART_INT + ITEM.ART  + CONVERT(varchar,CATALOG_LIN.ID)'" & vbCrLf
sConsulta = sConsulta & "SET @PARAM = '@COD VARCHAR(40), @DEN VARCHAR(300), @PROVE VARCHAR(300)," & vbCrLf
sConsulta = sConsulta & "@CATN1 INTEGER, @CATN2 INTEGER, @CATN3 INTEGER, @CATN4 INTEGER, " & vbCrLf
sConsulta = sConsulta & "@CATN5 INTEGER, @PRECIOMAX FLOAT,@PER VARCHAR(50),@ID VARCHAR(200)'" & vbCrLf
sConsulta = sConsulta & "EXECUTE sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "@COD=@COD, " & vbCrLf
sConsulta = sConsulta & "@DEN=@DEN, " & vbCrLf
sConsulta = sConsulta & "@PROVE=@PROVE," & vbCrLf
sConsulta = sConsulta & "@CATN1=@CATN1," & vbCrLf
sConsulta = sConsulta & "@CATN2=@CATN2, " & vbCrLf
sConsulta = sConsulta & "@CATN3=@CATN3," & vbCrLf
sConsulta = sConsulta & "@CATN4=@CATN4," & vbCrLf
sConsulta = sConsulta & "@CATN5=@CATN5, " & vbCrLf
sConsulta = sConsulta & "@PRECIOMAX=@PRECIOMAX, " & vbCrLf
sConsulta = sConsulta & "@PER=@PER," & vbCrLf
sConsulta = sConsulta & "@ID=@ID" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_DEVOLVER_TODAS_ID_ORDENES]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_DEVOLVER_TODAS_ID_ORDENES]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSEP_DEVOLVER_TODAS_ID_ORDENES @MAXLINEAS integer, @APROBADOR varchar(50) =null,@APROVISIONADOR varchar(50)  =null,@ARTINT varchar(50)  =null, @ARTDEN varchar(500)  =null, @ARTEXT varchar(500)  =null, @DESDEFECHA varchar(10)  =null, @HASTAFECHA varchar(10)  =null, @PROVECOD varchar(50)  =null,@PROVEDEN varchar(500)  =null,@NUMPEDPROVE varchar(100)  =null,@ANYO int  =null,@PEDIDO int  =null,@ORDEN int  =null,@EST int  =null ,@COINCIDENCIA int =null  AS" & vbCrLf
sConsulta = sConsulta & "declare @SQL nvarchar(4000)" & vbCrLf
sConsulta = sConsulta & "declare @SQL2 nvarchar(4000)" & vbCrLf
sConsulta = sConsulta & "declare @PARAM nvarchar(500)" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
sConsulta = sConsulta & "set  @SQL = N'" & vbCrLf
sConsulta = sConsulta & "SELECT CONVERT(VARCHAR,OE.EST) + LEFT(''00000000'',8-LEN(CONVERT(VARCHAR,OE.ID))) + CONVERT(VARCHAR,OE.ID)" & vbCrLf
sConsulta = sConsulta & "  FROM ORDEN_ENTREGA OE" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN PROVE P" & vbCrLf
sConsulta = sConsulta & "            ON OE.PROVE = P.COD" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN PEDIDO PE" & vbCrLf
sConsulta = sConsulta & "            ON OE.PEDIDO = PE.ID '" & vbCrLf
sConsulta = sConsulta & "IF @APROBADOR IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN (SELECT DISTINCT ORDEN , S.APROB_TIPO1 PER" & vbCrLf
sConsulta = sConsulta & "                           FROM LINEAS_PEDIDO LP " & vbCrLf
sConsulta = sConsulta & "                                    INNER JOIN SEGURIDAD S" & vbCrLf
sConsulta = sConsulta & "                                            ON LP.SEGURIDAD = S.ID ) T1" & vbCrLf
sConsulta = sConsulta & "            ON OE.ID = T1.ORDEN" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN (SELECT DISTINCT ORDEN , AL.PER PER" & vbCrLf
sConsulta = sConsulta & "                            FROM LINEAS_PEDIDO LP " & vbCrLf
sConsulta = sConsulta & "                                     INNER JOIN APROB_LIM AL" & vbCrLf
sConsulta = sConsulta & "                                             ON LP.APROB_LIM = AL.ID) T2" & vbCrLf
sConsulta = sConsulta & "            ON OE.ID = T2.ORDEN '" & vbCrLf
sConsulta = sConsulta & "set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN PER P1" & vbCrLf
sConsulta = sConsulta & "            ON PE.PER = P1.COD" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN ORDEN_EST OES" & vbCrLf
sConsulta = sConsulta & "            ON OE.ORDEN_EST = OES.ID'" & vbCrLf
sConsulta = sConsulta & "if @ARTINT is not null or @ARTDEN is not null or @ARTEXT is not null" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "   set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN (SELECT DISTINCT ORDEN" & vbCrLf
sConsulta = sConsulta & "                  FROM LINEAS_PEDIDO LP" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN ITEM I " & vbCrLf
sConsulta = sConsulta & "             ON LP.ANYO = I.ANYO" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN1=I.GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND LP.PROCE=I.PROCE " & vbCrLf
sConsulta = sConsulta & "            AND LP.ITEM=I.ID'" & vbCrLf
sConsulta = sConsulta & "      IF @ARTDEN is NOT NULL " & vbCrLf
sConsulta = sConsulta & "         set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN ART4 " & vbCrLf
sConsulta = sConsulta & "             ON LP.GMN1=ART4.GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN2=ART4.GMN2 " & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN3=ART4.GMN3 " & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN4=ART4.GMN4 " & vbCrLf
sConsulta = sConsulta & "            AND LP.ART_INT=ART4.COD" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN ART4 ART_AUX " & vbCrLf
sConsulta = sConsulta & "             ON ART_AUX.COD=I.ART " & vbCrLf
sConsulta = sConsulta & "            AND ART_AUX.GMN1=I.GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND ART_AUX.GMN2=I.GMN2 " & vbCrLf
sConsulta = sConsulta & "            AND ART_AUX.GMN3=I.GMN3 " & vbCrLf
sConsulta = sConsulta & "            AND ART_AUX.GMN4=I.GMN4'" & vbCrLf
sConsulta = sConsulta & "      IF @ARTEXT is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN PROVE_ART4 PA" & vbCrLf
sConsulta = sConsulta & "             ON LP.PROVE = PA.PROVE" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN1 = PA.GMN1" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN2 = PA.GMN2" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN3 = PA.GMN3" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN4 = PA.GMN4" & vbCrLf
sConsulta = sConsulta & "            AND CASE WHEN LP.ART_INT IS NULL THEN I.ART ELSE LP.ART_INT END = PA.ART" & vbCrLf
sConsulta = sConsulta & "'" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "   set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                 WHERE 1=1'" & vbCrLf
sConsulta = sConsulta & "   IF @ARTINT is NOT NULL " & vbCrLf
sConsulta = sConsulta & "     begin " & vbCrLf
sConsulta = sConsulta & "   IF @COINCIDENCIA=1 " & vbCrLf
sConsulta = sConsulta & "          set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                   AND (LP.ART_INT + I.ART like ''% ''  + @ARTINT + '' %''' +" & vbCrLf
sConsulta = sConsulta & "          ' OR LP.ART_INT + I.ART like @ARTINT  + '' %''' +" & vbCrLf
sConsulta = sConsulta & "          ' OR LP.ART_INT + I.ART like ''% '' + @ARTINT  ' + " & vbCrLf
sConsulta = sConsulta & "     ' OR LP.ART_INT + I.ART=@ARTINT)'" & vbCrLf
sConsulta = sConsulta & "        ELSE" & vbCrLf
sConsulta = sConsulta & "      set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                   AND LP.ART_INT + I.ART like + ''%'' + @ARTINT + ''%'' '" & vbCrLf
sConsulta = sConsulta & "     end" & vbCrLf
sConsulta = sConsulta & "   IF @ARTDEN is NOT NULL " & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "      IF @COINCIDENCIA=1 " & vbCrLf
sConsulta = sConsulta & "         set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                   AND (ART4.DEN + I.DESCR  + ART_AUX.DEN like ''% ''  + @ARTDEN + '' %''' + " & vbCrLf
sConsulta = sConsulta & "          ' OR ART4.DEN + I.DESCR  + ART_AUX.DEN like @ARTDEN + '' %''' +" & vbCrLf
sConsulta = sConsulta & "          ' OR ART4.DEN + I.DESCR  + ART_AUX.DEN like ''% '' + @ARTDEN ' + " & vbCrLf
sConsulta = sConsulta & "     ' OR ART4.DEN + I.DESCR  + ART_AUX.DEN =@ARTDEN)'" & vbCrLf
sConsulta = sConsulta & "      ELSE" & vbCrLf
sConsulta = sConsulta & "         set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                   AND ART4.DEN + I.DESCR  + ART_AUX.DEN like + ''%'' + @ARTDEN + ''%'''" & vbCrLf
sConsulta = sConsulta & "    END" & vbCrLf
sConsulta = sConsulta & "    IF @ARTEXT is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                   AND PA.COD_EXT like @ARTEXT + ''%''" & vbCrLf
sConsulta = sConsulta & "'" & vbCrLf
sConsulta = sConsulta & "   set  @SQL = @SQL + N') LP" & vbCrLf
sConsulta = sConsulta & "            ON OE.ID = LP.ORDEN" & vbCrLf
sConsulta = sConsulta & "'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "        " & vbCrLf
sConsulta = sConsulta & "set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "              WHERE PE.TIPO=1'" & vbCrLf
sConsulta = sConsulta & "if @DESDEFECHA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.FECHA >= CONVERT(datetime,@DESDEFECHA,103)'" & vbCrLf
sConsulta = sConsulta & "if @HASTAFECHA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.FECHA <= CONVERT(datetime,@HASTAFECHA,103)'" & vbCrLf
sConsulta = sConsulta & "if @PROVECOD IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.PROVE = @PROVECOD'" & vbCrLf
sConsulta = sConsulta & "if @NUMPEDPROVE IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.NUMEXT = @NUMPEDPROVE '" & vbCrLf
sConsulta = sConsulta & "if @ORDEN IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.NUM = @ORDEN '" & vbCrLf
sConsulta = sConsulta & "if @ANYO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.ANYO = @ANYO '" & vbCrLf
sConsulta = sConsulta & "if @PEDIDO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND PE.NUM = @PEDIDO '" & vbCrLf
sConsulta = sConsulta & "if @PROVEDEN IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND P.DEN  LIKE @PROVEDEN + ''%'' '" & vbCrLf
sConsulta = sConsulta & "IF @APROBADOR IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND (T1.PER = @APROBADOR " & vbCrLf
sConsulta = sConsulta & "                           OR T2.PER = @APROBADOR) '" & vbCrLf
sConsulta = sConsulta & "if @APROVISIONADOR IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND PE.PER = @APROVISIONADOR '" & vbCrLf
sConsulta = sConsulta & "declare @IN varchar(500)" & vbCrLf
sConsulta = sConsulta & "SET @IN = '('" & vbCrLf
sConsulta = sConsulta & "IF @EST & 1 = 1" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '0,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 2 = 2" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '1,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 4 = 4" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '2,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 8 = 8" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '3,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 16 = 16" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '4,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 32 = 32" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '5,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 64 = 64" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '6,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 128 = 128" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '20,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 256 = 256" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '21,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 512 = 512" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '22,'" & vbCrLf
sConsulta = sConsulta & "IF @IN!='('" & vbCrLf
sConsulta = sConsulta & "   set @SQL = @SQL + '" & vbCrLf
sConsulta = sConsulta & "   AND OE.EST IN ' + LEFT(@IN,LEN(@IN)-1) + ') '" & vbCrLf
sConsulta = sConsulta & "set @SQL = @SQL + '" & vbCrLf
sConsulta = sConsulta & "order by CONVERT(VARCHAR,OE.EST) + LEFT(''00000000'',8-LEN(CONVERT(VARCHAR,OE.ID))) + CONVERT(VARCHAR,OE.ID)'" & vbCrLf
sConsulta = sConsulta & "SET @SQL2 = '" & vbCrLf
sConsulta = sConsulta & "create table #IDS (id varchar(200))" & vbCrLf
sConsulta = sConsulta & "set nocount on" & vbCrLf
sConsulta = sConsulta & "DECLARE C1 CURSOR FOR " & vbCrLf
sConsulta = sConsulta & "' + @SQL + '" & vbCrLf
sConsulta = sConsulta & "DECLARE @miID varchar(200)" & vbCrLf
sConsulta = sConsulta & "declare @cont int" & vbCrLf
sConsulta = sConsulta & "set @cont=1" & vbCrLf
sConsulta = sConsulta & "OPEN C1" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C1 INTO @miID" & vbCrLf
sConsulta = sConsulta & "while @@fetch_status=0" & vbCrLf
sConsulta = sConsulta & "begin" & vbCrLf
sConsulta = sConsulta & "if @cont = 1" & vbCrLf
sConsulta = sConsulta & " insert into #IDS values (@miID)" & vbCrLf
sConsulta = sConsulta & "set @cont = @cont + 1        " & vbCrLf
sConsulta = sConsulta & "     " & vbCrLf
sConsulta = sConsulta & "if @cont = ' + CONVERT(VARCHAR,@MAXLINEAS) + ' + 1" & vbCrLf
sConsulta = sConsulta & " set @cont = 1" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C1 INTO @miID" & vbCrLf
sConsulta = sConsulta & "end" & vbCrLf
sConsulta = sConsulta & "CLOSE C1" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C1" & vbCrLf
sConsulta = sConsulta & "set nocount off" & vbCrLf
sConsulta = sConsulta & "select * from #ids" & vbCrLf
sConsulta = sConsulta & "'" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF" & vbCrLf
sConsulta = sConsulta & "SET @PARAM = N'@APROBADOR varchar(50), @APROVISIONADOR varchar(50), @DESDEFECHA varchar(10), @HASTAFECHA varchar(10), @ARTINT varchar(50), @ARTDEN varchar(500), @ARTEXT varchar(500), @PROVECOD varchar(50), @PROVEDEN varchar(500), @NUMPEDPROVE varchar(100),@ORDEN int, @ANYO int, @PEDIDO int, @EST int'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL2, @PARAM, @APROBADOR= @APROBADOR, @APROVISIONADOR = @APROVISIONADOR, @DESDEFECHA=@DESDEFECHA, @HASTAFECHA=@HASTAFECHA, @ARTINT=@ARTINT, @ARTDEN=@ARTDEN, @ARTEXT=@ARTEXT, @PROVECOD=@PROVECOD, @PROVEDEN=@PROVEDEN, @NUMPEDPROVE=@NUMPEDPROVE, @ANYO=@ANYO, @ORDEN=@ORDEN, @PEDIDO = @PEDIDO, @EST=@EST" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_DEVOLVER_TODAS_ORDENES]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_DEVOLVER_TODAS_ORDENES]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSEP_DEVOLVER_TODAS_ORDENES @MAXLINEAS integer, @DESDEID varchar(50)=null, @APROBADOR varchar(50) = null,@APROVISIONADOR varchar(50) = null, @ARTINT varchar(50) = null, @ARTDEN varchar(500) = null, @ARTEXT varchar(500) = null, @DESDEFECHA varchar(10) = null, @HASTAFECHA varchar(10) = null, @PROVECOD varchar(50) = null, @PROVEDEN varchar(500) = null,@NUMPEDPROVE varchar(100) = null,@ANYO int = null,@PEDIDO int = null,@ORDEN int = null,@EST int  = null, @ID int = null ,@RECINCORRECTO int = null ,@COINCIDENCIA int =null AS " & vbCrLf
sConsulta = sConsulta & "declare @SQL nvarchar(4000)" & vbCrLf
sConsulta = sConsulta & "declare @PARAM nvarchar(500)" & vbCrLf
sConsulta = sConsulta & "declare @SQL1 nvarchar(4000)" & vbCrLf
sConsulta = sConsulta & "declare @SQL2 nvarchar(4000)" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF" & vbCrLf
sConsulta = sConsulta & "set  @SQL = N'" & vbCrLf
sConsulta = sConsulta & "SELECT TOP ' + CONVERT(varchar,@MAXLINEAS) + ' CONVERT(VARCHAR,OE.EST) + LEFT(''00000000'',8-LEN(CONVERT(VARCHAR,OE.ID))) + CONVERT(VARCHAR,OE.ID), " & vbCrLf
sConsulta = sConsulta & "       OE.ID, OE.ANYO, PE.NUM NUMPEDIDO, OE.NUM NUMORDEN, OE.PROVE PROVECOD, P.DEN PROVEDEN, " & vbCrLf
sConsulta = sConsulta & "       OE.NUMEXT, OE.FECHA FECHA, OE.EST EST, OE.IMPORTE IMPORTE, " & vbCrLf
sConsulta = sConsulta & "       P1.APE + '', '' + P1.NOM APROVISIONADOR,P1.APE,P1.NOM," & vbCrLf
sConsulta = sConsulta & "       OES.COMENT, OES.FECHA FECEST,OE.IMPORTE,OE.PEDIDO,OE.TIPO, OE.INCORRECTA, PE.PER" & vbCrLf
sConsulta = sConsulta & "  FROM ORDEN_ENTREGA OE" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN PROVE P" & vbCrLf
sConsulta = sConsulta & "            ON OE.PROVE = P.COD" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN PEDIDO PE" & vbCrLf
sConsulta = sConsulta & "            ON OE.PEDIDO = PE.ID '" & vbCrLf
sConsulta = sConsulta & "IF @APROBADOR IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN (SELECT DISTINCT ORDEN , S.APROB_TIPO1 PER" & vbCrLf
sConsulta = sConsulta & "                           FROM LINEAS_PEDIDO LP " & vbCrLf
sConsulta = sConsulta & "                                    INNER JOIN SEGURIDAD S" & vbCrLf
sConsulta = sConsulta & "                                            ON LP.SEGURIDAD = S.ID ) T1" & vbCrLf
sConsulta = sConsulta & "            ON OE.ID = T1.ORDEN" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN (SELECT DISTINCT ORDEN , AL.PER PER" & vbCrLf
sConsulta = sConsulta & "                            FROM LINEAS_PEDIDO LP " & vbCrLf
sConsulta = sConsulta & "                                     INNER JOIN APROB_LIM AL" & vbCrLf
sConsulta = sConsulta & "                                             ON LP.APROB_LIM = AL.ID) T2" & vbCrLf
sConsulta = sConsulta & "            ON OE.ID = T2.ORDEN '" & vbCrLf
sConsulta = sConsulta & "set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN PER P1" & vbCrLf
sConsulta = sConsulta & "            ON PE.PER = P1.COD" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN ORDEN_EST OES" & vbCrLf
sConsulta = sConsulta & "            ON OE.ORDEN_EST = OES.ID'" & vbCrLf
sConsulta = sConsulta & "if @ARTINT is not null or @ARTDEN is not null or @ARTEXT is not null" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "   set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN (SELECT DISTINCT ORDEN" & vbCrLf
sConsulta = sConsulta & "                  FROM LINEAS_PEDIDO LP" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN ITEM I " & vbCrLf
sConsulta = sConsulta & "             ON LP.ANYO = I.ANYO" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN1=I.GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND LP.PROCE=I.PROCE " & vbCrLf
sConsulta = sConsulta & "            AND LP.ITEM=I.ID'" & vbCrLf
sConsulta = sConsulta & "      IF @ARTDEN is NOT NULL " & vbCrLf
sConsulta = sConsulta & "         set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN ART4 " & vbCrLf
sConsulta = sConsulta & "             ON LP.GMN1=ART4.GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN2=ART4.GMN2 " & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN3=ART4.GMN3 " & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN4=ART4.GMN4 " & vbCrLf
sConsulta = sConsulta & "            AND LP.ART_INT=ART4.COD" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN ART4 ART_AUX " & vbCrLf
sConsulta = sConsulta & "             ON ART_AUX.COD=I.ART " & vbCrLf
sConsulta = sConsulta & "            AND ART_AUX.GMN1=I.GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND ART_AUX.GMN2=I.GMN2 " & vbCrLf
sConsulta = sConsulta & "            AND ART_AUX.GMN3=I.GMN3 " & vbCrLf
sConsulta = sConsulta & "            AND ART_AUX.GMN4=I.GMN4'" & vbCrLf
sConsulta = sConsulta & "      IF @ARTEXT is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN PROVE_ART4 PA" & vbCrLf
sConsulta = sConsulta & "             ON LP.PROVE = PA.PROVE" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN1 = PA.GMN1" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN2 = PA.GMN2" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN3 = PA.GMN3" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN4 = PA.GMN4" & vbCrLf
sConsulta = sConsulta & "            AND CASE WHEN LP.ART_INT IS NULL THEN I.ART ELSE LP.ART_INT END = PA.ART" & vbCrLf
sConsulta = sConsulta & "'" & vbCrLf
sConsulta = sConsulta & "set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & " WHERE 1=1'" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & " IF @ARTINT is NOT NULL " & vbCrLf
sConsulta = sConsulta & "     begin " & vbCrLf
sConsulta = sConsulta & "   IF @COINCIDENCIA=1 " & vbCrLf
sConsulta = sConsulta & "          set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                   AND (LP.ART_INT + I.ART like ''% ''  + @ARTINT + '' %''' +" & vbCrLf
sConsulta = sConsulta & "          ' OR LP.ART_INT + I.ART like @ARTINT  + '' %''' +" & vbCrLf
sConsulta = sConsulta & "          ' OR LP.ART_INT + I.ART like ''% '' + @ARTINT  ' + " & vbCrLf
sConsulta = sConsulta & "     ' OR LP.ART_INT + I.ART=@ARTINT)'" & vbCrLf
sConsulta = sConsulta & "        ELSE" & vbCrLf
sConsulta = sConsulta & "      set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                   AND LP.ART_INT + I.ART like + ''%'' + @ARTINT + ''%'' '" & vbCrLf
sConsulta = sConsulta & "     end" & vbCrLf
sConsulta = sConsulta & "   IF @ARTDEN is NOT NULL " & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "      IF @COINCIDENCIA=1 " & vbCrLf
sConsulta = sConsulta & "         set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                   AND (ART4.DEN + I.DESCR  + ART_AUX.DEN like ''% ''  + @ARTDEN + '' %''' + " & vbCrLf
sConsulta = sConsulta & "          ' OR ART4.DEN + I.DESCR  + ART_AUX.DEN like @ARTDEN + '' %''' +" & vbCrLf
sConsulta = sConsulta & "          ' OR ART4.DEN + I.DESCR  + ART_AUX.DEN like ''% '' + @ARTDEN ' + " & vbCrLf
sConsulta = sConsulta & "     ' OR ART4.DEN + I.DESCR  + ART_AUX.DEN=@ARTDEN)'" & vbCrLf
sConsulta = sConsulta & "      ELSE" & vbCrLf
sConsulta = sConsulta & "         set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                   AND ART4.DEN + I.DESCR  + ART_AUX.DEN like + ''%'' + @ARTDEN + ''%'''" & vbCrLf
sConsulta = sConsulta & "    END" & vbCrLf
sConsulta = sConsulta & "   IF @ARTEXT is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                   AND PA.COD_EXT like @ARTEXT + ''%''" & vbCrLf
sConsulta = sConsulta & "'" & vbCrLf
sConsulta = sConsulta & "   set  @SQL = @SQL + N') LP" & vbCrLf
sConsulta = sConsulta & "            ON OE.ID = LP.ORDEN" & vbCrLf
sConsulta = sConsulta & "'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "   set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                 WHERE PE.TIPO=1'" & vbCrLf
sConsulta = sConsulta & "        " & vbCrLf
sConsulta = sConsulta & "if @DESDEFECHA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.FECHA >= CONVERT(datetime,@DESDEFECHA,103)'" & vbCrLf
sConsulta = sConsulta & "if @HASTAFECHA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.FECHA <= CONVERT(datetime,@HASTAFECHA,103)'" & vbCrLf
sConsulta = sConsulta & "if @PROVECOD IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.PROVE = @PROVECOD'" & vbCrLf
sConsulta = sConsulta & "if @NUMPEDPROVE IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.NUMEXT = @NUMPEDPROVE '" & vbCrLf
sConsulta = sConsulta & "if @ORDEN IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.NUM = @ORDEN '" & vbCrLf
sConsulta = sConsulta & "if @ANYO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.ANYO = @ANYO '" & vbCrLf
sConsulta = sConsulta & "if @PEDIDO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND PE.NUM = @PEDIDO '" & vbCrLf
sConsulta = sConsulta & "if @PROVEDEN IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND P.DEN  LIKE @PROVEDEN + ''%'' '" & vbCrLf
sConsulta = sConsulta & "IF @APROBADOR IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND (T1.PER = @APROBADOR " & vbCrLf
sConsulta = sConsulta & "                           OR T2.PER = @APROBADOR) '" & vbCrLf
sConsulta = sConsulta & "if @APROVISIONADOR IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND PE.PER = @APROVISIONADOR '" & vbCrLf
sConsulta = sConsulta & "declare @IN varchar(500)" & vbCrLf
sConsulta = sConsulta & "SET @IN = '('" & vbCrLf
sConsulta = sConsulta & "IF @EST & 1 = 1" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '0,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 2 = 2" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '1,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 4 = 4" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '2,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 8 = 8" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '3,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 16 = 16" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '4,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 32 = 32" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '5,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 64 = 64" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '6,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 128 = 128" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '20,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 256 = 256" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '21,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 512 = 512" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '22,'" & vbCrLf
sConsulta = sConsulta & "SET @SQL1 = ''" & vbCrLf
sConsulta = sConsulta & "SET @SQL2 = ''" & vbCrLf
sConsulta = sConsulta & "IF @IN!='('" & vbCrLf
sConsulta = sConsulta & "   --set @SQL = @SQL + '" & vbCrLf
sConsulta = sConsulta & "   --AND OE.EST IN ' + LEFT(@IN,LEN(@IN)-1) + ') '" & vbCrLf
sConsulta = sConsulta & "     set @SQL1=' OE.EST IN ' + LEFT(@IN,LEN(@IN)-1) + ') '" & vbCrLf
sConsulta = sConsulta & "IF @RECINCORRECTO IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "  IF @RECINCORRECTO=1" & vbCrLf
sConsulta = sConsulta & "   --set @SQL = @SQL + '" & vbCrLf
sConsulta = sConsulta & "   --AND OE.INCORRECTA=1'" & vbCrLf
sConsulta = sConsulta & "   set @SQL2 = ' OE.INCORRECTA=1'  " & vbCrLf
sConsulta = sConsulta & "IF @SQL1<>'' and @SQL2<>'' " & vbCrLf
sConsulta = sConsulta & "   SET @SQL= @SQL + ' AND (' + @SQL1 + ' OR ' + @SQL2 + ')'" & vbCrLf
sConsulta = sConsulta & "ELSE " & vbCrLf
sConsulta = sConsulta & "if @SQL1!=''" & vbCrLf
sConsulta = sConsulta & "   SET @SQL= @SQL + ' AND ' + @SQL1 " & vbCrLf
sConsulta = sConsulta & "else" & vbCrLf
sConsulta = sConsulta & "if @SQL2!=''" & vbCrLf
sConsulta = sConsulta & "   SET @SQL= @SQL + ' AND ' + @SQL2" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "if @ID IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + '" & vbCrLf
sConsulta = sConsulta & "   AND OE.ID=@ID '" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "if @DESDEID IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + '" & vbCrLf
sConsulta = sConsulta & "   AND CONVERT(VARCHAR,OE.EST) + LEFT(''00000000'',8-LEN(CONVERT(VARCHAR,OE.ID))) + CONVERT(VARCHAR,OE.ID)>=@DESDEID '" & vbCrLf
sConsulta = sConsulta & "set @SQL = @SQL + '" & vbCrLf
sConsulta = sConsulta & "order by CONVERT(VARCHAR,OE.EST) + LEFT(''00000000'',8-LEN(CONVERT(VARCHAR,OE.ID))) + CONVERT(VARCHAR,OE.ID)'" & vbCrLf
sConsulta = sConsulta & "SET @PARAM = N'@APROBADOR varchar(50), @APROVISIONADOR varchar(50), @DESDEFECHA varchar(10), @HASTAFECHA varchar(10), @ARTINT varchar(50), @ARTDEN varchar(500), @ARTEXT varchar(500), @PROVECOD varchar(50), @PROVEDEN varchar(500), @NUMPEDPROVE varchar(100),@ORDEN int, @ANYO int, @PEDIDO int, @DESDEID varchar(50), @EST int, @ID int'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, @PARAM, @APROBADOR= @APROBADOR, @APROVISIONADOR = @APROVISIONADOR, @DESDEFECHA=@DESDEFECHA, @HASTAFECHA=@HASTAFECHA, @ARTINT=@ARTINT, @ARTDEN=@ARTDEN, @ARTEXT =@ARTEXT , @PROVECOD=@PROVECOD, @PROVEDEN=@PROVEDEN, @NUMPEDPROVE=@NUMPEDPROVE, @ANYO=@ANYO, @ORDEN=@ORDEN, @PEDIDO = @PEDIDO,@DESDEID=@DESDEID, @EST=@EST, @ID = @ID" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


End Sub

Private Function V_2_11_5_Updated_02() As Boolean
Dim sConsulta As String
Dim rdores As rdoResultset
Dim iResultado As Integer



    
    
    sConsulta = "SELECT P.ANYO,P.GMN1,P.COD FROM PROCE P WHERE P.EST > 6 AND P.EST < 11 AND "
    sConsulta = sConsulta & " NOT EXISTS (SELECT * FROM PROCE_OFE PO WHERE P.ANYO=PO.ANYO AND P.GMN1=PO.GMN1 AND P.COD=PO.PROCE AND PO.OFE >0)"
    Set rdores = gRDOCon.OpenResultset(sConsulta, rdOpenForwardOnly, rdConcurReadOnly)
    While Not rdores.EOF
            sConsulta = "UPDATE PROCE SET EST=6 WHERE  ANYO= " & rdores("ANYO").Value & " AND GMN1='" & DblQuote(rdores("GMN1").Value) & "' AND COD=" & rdores("COD").Value
            ExecuteSQL gRDOCon, sConsulta
            rdores.MoveNext
    Wend
    rdores.Close
    Set rdores = Nothing
    
    sConsulta = "SELECT ANYO,GMN1,PROCE, GRUPO,USU,VISTA FROM CONF_VISTAS_GRUPO WHERE "
    sConsulta = sConsulta & " CANT_PROV_POS <=0 OR ADJ_PROV_POS <=0 OR PRECIO_PROV_POS <= 0 OR IMP_ADJ_POS <= 0"
    Set rdores = gRDOCon.OpenResultset(sConsulta, rdOpenForwardOnly, rdConcurReadOnly)
    While Not rdores.EOF
    
        sConsulta = "DELETE FROM CONF_VISTAS_GRUPO_ATRIB WHERE ANYO= " & rdores("ANYO").Value & " AND GMN1='" & DblQuote(rdores("GMN1").Value) & "' AND PROCE=" & rdores("PROCE").Value & " AND GRUPO='" & DblQuote(rdores("GRUPO").Value) & "' AND USU='" & DblQuote(rdores("USU").Value) & "' AND VISTA=" & rdores("VISTA").Value
        ExecuteSQL gRDOCon, sConsulta
        sConsulta = "DELETE FROM CONF_VISTAS_GRUPO_PROVE WHERE ANYO= " & rdores("ANYO").Value & " AND GMN1='" & DblQuote(rdores("GMN1").Value) & "' AND PROCE=" & rdores("PROCE").Value & " AND GRUPO='" & DblQuote(rdores("GRUPO").Value) & "' AND USU='" & DblQuote(rdores("USU").Value) & "' AND VISTA=" & rdores("VISTA").Value
        ExecuteSQL gRDOCon, sConsulta
        sConsulta = "DELETE FROM CONF_VISTAS_GRUPO WHERE ANYO= " & rdores("ANYO").Value & " AND GMN1='" & DblQuote(rdores("GMN1").Value) & "' AND PROCE=" & rdores("PROCE").Value & " AND GRUPO='" & DblQuote(rdores("GRUPO").Value) & "' AND USU='" & DblQuote(rdores("USU").Value) & "' AND VISTA=" & rdores("VISTA").Value
        ExecuteSQL gRDOCon, sConsulta
    Wend
    rdores.Close
    Set rdores = Nothing

    sConsulta = "SELECT ANYO,GMN1,PROCE, USU,VISTA FROM CONF_VISTAS_ALL WHERE"
    sConsulta = sConsulta & " CANT_PROV_POS <=0 OR ADJ_PROV_POS <=0 OR PRECIO_PROV_POS <= 0 OR IMP_ADJ_POS <= 0"
    Set rdores = gRDOCon.OpenResultset(sConsulta, rdOpenForwardOnly, rdConcurReadOnly)
    While Not rdores.EOF
    
        sConsulta = "DELETE FROM CONF_VISTAS_ALL_ATRIB WHERE ANYO= " & rdores("ANYO").Value & " AND GMN1='" & DblQuote(rdores("GMN1").Value) & "' AND PROCE=" & rdores("PROCE").Value & " AND USU='" & DblQuote(rdores("USU").Value) & "' AND VISTA=" & rdores("VISTA").Value
        ExecuteSQL gRDOCon, sConsulta
        sConsulta = "DELETE FROM CONF_VISTAS_ALL_PROVE WHERE ANYO= " & rdores("ANYO").Value & " AND GMN1='" & DblQuote(rdores("GMN1").Value) & "' AND PROCE=" & rdores("PROCE").Value & " AND USU='" & DblQuote(rdores("USU").Value) & "' AND VISTA=" & rdores("VISTA").Value
        ExecuteSQL gRDOCon, sConsulta
        sConsulta = "DELETE FROM CONF_VISTAS_ALL WHERE ANYO= " & rdores("ANYO").Value & " AND GMN1='" & DblQuote(rdores("GMN1").Value) & "' AND PROCE=" & rdores("PROCE").Value & " AND USU='" & DblQuote(rdores("USU").Value) & "' AND VISTA=" & rdores("VISTA").Value
        ExecuteSQL gRDOCon, sConsulta
    Wend
    rdores.Close
    Set rdores = Nothing

    
    sConsulta = "DELETE FROM CONF_VISTA_INICIAL_GRUPO WHERE CANT_PROV_POS <=0 OR ADJ_PROV_POS <=0 OR PRECIO_PROV_POS <= 0 OR IMP_ADJ_POS <= 0"
    ExecuteSQL gRDOCon, sConsulta

    
End Function
Public Function CodigoDeActualizacion2_11_50_03A2_11_50_04() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim bBool As Boolean

On Error GoTo error
    

sConsulta = "BEGIN TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = True
ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

frmProgreso.lblDetalle = "Modificamos TABLAS"
frmProgreso.lblDetalle.Refresh
frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_11_5_TablasModif_04

frmProgreso.lblDetalle = "Modificamos stored de traspaso de ofertas"
frmProgreso.lblDetalle.Refresh
frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_11_5_Storeds_04

frmProgreso.lblDetalle = "Modificamos triggers"
frmProgreso.lblDetalle.Refresh
frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_11_5_Triggers_04


sConsulta = "UPDATE VERSION SET NUM ='2.11.50.04'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "COMMIT TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = False

CodigoDeActualizacion2_11_50_03A2_11_50_04 = True
Exit Function
    
error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_11_50_03A2_11_50_04 = False

End Function

Private Sub V_2_11_5_Storeds_04()
Dim sConsulta As String

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_DEVOLVER_PROCESOS_A_DESPUB]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_DEVOLVER_PROCESOS_A_DESPUB]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_DEVOLVER_PROCESOS_A_DESPUB(@EQP VARCHAR(50), @COM VARCHAR(50),@DESDES INT ,@DESADJ INT,@RESPADJ TINYINT,@RESPPUB TINYINT) AS" & vbCrLf
sConsulta = sConsulta & "IF @RESPADJ=0  AND @RESPPUB=0" & vbCrLf
sConsulta = sConsulta & "   if @DESADJ=0 " & vbCrLf
sConsulta = sConsulta & "            SELECT PROCE.ANYO,PROCE.GMN1,PROCE.COD,PROCE.DEN,PROCE.FECLIMOFE,PROCE.ADJDIR," & vbCrLf
sConsulta = sConsulta & "            PROCE.FECPRES,PROCE.EQP,PROCE.COM,PROCE.USUAPER," & vbCrLf
sConsulta = sConsulta & "           Proce.FECNEC , Proce.GMN2, Proce.GMN3, Proce.GMN4, Proce.EST,PROCE.FECAPE,PROCE_DEF.SUBASTA,sum(PROCE_PROVE.PUB) as PUB" & vbCrLf
sConsulta = sConsulta & "           From Proce" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PROCE_DEF ON PROCE.ANYO=PROCE_DEF.ANYO AND PROCE.GMN1=PROCE_DEF.GMN1 AND PROCE.COD=PROCE_DEF.PROCE" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PROCE_PROVE ON" & vbCrLf
sConsulta = sConsulta & "           Proce.anyo = PROCE_PROVE.anyo And Proce.gmn1 = PROCE_PROVE.gmn1 And Proce.COD = PROCE_PROVE.Proce" & vbCrLf
sConsulta = sConsulta & "           AND PROCE_PROVE.EQP=@EQP AND PROCE_PROVE.COM=@COM" & vbCrLf
sConsulta = sConsulta & "     " & vbCrLf
sConsulta = sConsulta & "           WHERE (PROCE_PROVE.PUB=1 AND convert(char,getdate(),111) <=convert(char,PROCE.FECLIMOFE,111) AND convert(char,dateadd(day, @DESDES   ,PROCE.FECLIMOFE),111)<=convert(char,getdate(),111))" & vbCrLf
sConsulta = sConsulta & "    " & vbCrLf
sConsulta = sConsulta & "           AND PROCE.EST>=5 AND PROCE.EST<12" & vbCrLf
sConsulta = sConsulta & "           GROUP BY PROCE.ANYO,PROCE.GMN1,PROCE.COD,PROCE.DEN,PROCE.FECLIMOFE,PROCE.ADJDIR," & vbCrLf
sConsulta = sConsulta & "            PROCE.FECPRES,PROCE.EQP,PROCE.COM,PROCE.USUAPER," & vbCrLf
sConsulta = sConsulta & "           Proce.FECNEC , Proce.GMN2, Proce.GMN3, Proce.GMN4, Proce.EST,PROCE.FECAPE,PROCE_DEF.SUBASTA" & vbCrLf
sConsulta = sConsulta & "           ORDER BY PROCE.FECLIMOFE" & vbCrLf
sConsulta = sConsulta & "   else" & vbCrLf
sConsulta = sConsulta & "        if @DESDES=0" & vbCrLf
sConsulta = sConsulta & "            SELECT PROCE.ANYO,PROCE.GMN1,PROCE.COD,PROCE.DEN,PROCE.FECLIMOFE,PROCE.ADJDIR," & vbCrLf
sConsulta = sConsulta & "            PROCE.FECPRES,PROCE.EQP,PROCE.COM,PROCE.USUAPER," & vbCrLf
sConsulta = sConsulta & "           Proce.FECNEC , Proce.GMN2, Proce.GMN3, Proce.GMN4, Proce.EST,PROCE.FECAPE,PROCE_DEF.SUBASTA,sum(PROCE_PROVE.PUB) as PUB" & vbCrLf
sConsulta = sConsulta & "           From Proce" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PROCE_DEF ON PROCE.ANYO=PROCE_DEF.ANYO AND PROCE.GMN1=PROCE_DEF.GMN1 AND PROCE.COD=PROCE_DEF.PROCE" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PROCE_PROVE ON" & vbCrLf
sConsulta = sConsulta & "           Proce.anyo = PROCE_PROVE.anyo And Proce.gmn1 = PROCE_PROVE.gmn1 And Proce.COD = PROCE_PROVE.Proce" & vbCrLf
sConsulta = sConsulta & "           AND PROCE_PROVE.EQP=@EQP AND PROCE_PROVE.COM=@COM" & vbCrLf
sConsulta = sConsulta & "     " & vbCrLf
sConsulta = sConsulta & "           WHERE     " & vbCrLf
sConsulta = sConsulta & "            (convert(char,getdate(),111)<=PROCE.FECPRES AND dateadd(day, @DESADJ   ,PROCE.FECPRES)<=convert(char,getdate(),111))" & vbCrLf
sConsulta = sConsulta & "    " & vbCrLf
sConsulta = sConsulta & "           AND PROCE.EST>=5 AND PROCE.EST<12" & vbCrLf
sConsulta = sConsulta & "           GROUP BY PROCE.ANYO,PROCE.GMN1,PROCE.COD,PROCE.DEN,PROCE.FECLIMOFE,PROCE.ADJDIR," & vbCrLf
sConsulta = sConsulta & "            PROCE.FECPRES,PROCE.EQP,PROCE.COM,PROCE.USUAPER," & vbCrLf
sConsulta = sConsulta & "           Proce.FECNEC , Proce.GMN2, Proce.GMN3, Proce.GMN4, Proce.EST,PROCE.FECAPE ,PROCE_DEF.SUBASTA" & vbCrLf
sConsulta = sConsulta & "           ORDER BY PROCE.FECLIMOFE" & vbCrLf
sConsulta = sConsulta & "        else" & vbCrLf
sConsulta = sConsulta & "            SELECT PROCE.ANYO,PROCE.GMN1,PROCE.COD,PROCE.DEN,PROCE.FECLIMOFE,PROCE.ADJDIR," & vbCrLf
sConsulta = sConsulta & "            PROCE.FECPRES,PROCE.EQP,PROCE.COM,PROCE.USUAPER," & vbCrLf
sConsulta = sConsulta & "           Proce.FECNEC , Proce.GMN2, Proce.GMN3, Proce.GMN4, Proce.EST,PROCE.FECAPE,PROCE_DEF.SUBASTA,sum(PROCE_PROVE.PUB) as PUB" & vbCrLf
sConsulta = sConsulta & "           From Proce" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PROCE_DEF ON PROCE.ANYO=PROCE_DEF.ANYO AND PROCE.GMN1=PROCE_DEF.GMN1 AND PROCE.COD=PROCE_DEF.PROCE" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PROCE_PROVE ON" & vbCrLf
sConsulta = sConsulta & "           Proce.anyo = PROCE_PROVE.anyo And Proce.gmn1 = PROCE_PROVE.gmn1 And Proce.COD = PROCE_PROVE.Proce" & vbCrLf
sConsulta = sConsulta & "           AND PROCE_PROVE.EQP=@EQP AND PROCE_PROVE.COM=@COM" & vbCrLf
sConsulta = sConsulta & "     " & vbCrLf
sConsulta = sConsulta & "           WHERE ((PROCE_PROVE.PUB=1 AND convert(char,getdate(),111) <=convert(char,PROCE.FECLIMOFE,111) AND convert(char,dateadd(day, @DESDES   ,PROCE.FECLIMOFE),111)<=convert(char,getdate(),111))" & vbCrLf
sConsulta = sConsulta & "     " & vbCrLf
sConsulta = sConsulta & "           OR (convert(char,getdate(),111)<=PROCE.FECPRES AND dateadd(day, @DESADJ   ,PROCE.FECPRES)<=convert(char,getdate(),111)))" & vbCrLf
sConsulta = sConsulta & "    " & vbCrLf
sConsulta = sConsulta & "           AND PROCE.EST>=5 AND PROCE.EST<12" & vbCrLf
sConsulta = sConsulta & "           GROUP BY PROCE.ANYO,PROCE.GMN1,PROCE.COD,PROCE.DEN,PROCE.FECLIMOFE,PROCE.ADJDIR," & vbCrLf
sConsulta = sConsulta & "            PROCE.FECPRES,PROCE.EQP,PROCE.COM,PROCE.USUAPER," & vbCrLf
sConsulta = sConsulta & "           Proce.FECNEC , Proce.GMN2, Proce.GMN3, Proce.GMN4, Proce.EST,PROCE.FECAPE ,PROCE_DEF.SUBASTA" & vbCrLf
sConsulta = sConsulta & "           ORDER BY PROCE.FECLIMOFE" & vbCrLf
sConsulta = sConsulta & "  " & vbCrLf
sConsulta = sConsulta & "Else" & vbCrLf
sConsulta = sConsulta & "  IF @RESPADJ=0  AND @RESPPUB=1" & vbCrLf
sConsulta = sConsulta & "    if @DESADJ=0" & vbCrLf
sConsulta = sConsulta & "        SELECT PROCE.ANYO,PROCE.GMN1,PROCE.COD,PROCE.DEN,PROCE.FECLIMOFE,PROCE.ADJDIR," & vbCrLf
sConsulta = sConsulta & "           PROCE.FECPRES,PROCE.EQP,PROCE.COM,PROCE.USUAPER," & vbCrLf
sConsulta = sConsulta & "           Proce.FECNEC , Proce.GMN2, Proce.GMN3, Proce.GMN4, Proce.EST,PROCE.FECAPE,PROCE_DEF.SUBASTA,sum(PROCE_PROVE.PUB) as PUB" & vbCrLf
sConsulta = sConsulta & "           From Proce" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PROCE_DEF ON PROCE.ANYO=PROCE_DEF.ANYO AND PROCE.GMN1=PROCE_DEF.GMN1 AND PROCE.COD=PROCE_DEF.PROCE" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PROCE_PROVE ON" & vbCrLf
sConsulta = sConsulta & "           Proce.anyo = PROCE_PROVE.anyo And Proce.gmn1 = PROCE_PROVE.gmn1 And Proce.COD = PROCE_PROVE.Proce" & vbCrLf
sConsulta = sConsulta & "           AND PROCE_PROVE.EQP=@EQP AND PROCE_PROVE.COM=@COM" & vbCrLf
sConsulta = sConsulta & "    " & vbCrLf
sConsulta = sConsulta & "           WHERE PROCE_PROVE.PUB=1 AND convert(char,getdate(),111) <= convert(char,PROCE.FECLIMOFE,111) AND convert(char,dateadd(day, @DESDES   ,PROCE.FECLIMOFE),111)<=convert(char,getdate(),111)" & vbCrLf
sConsulta = sConsulta & "           AND PROCE.EQP=@EQP AND PROCE.COM=@COM" & vbCrLf
sConsulta = sConsulta & "     " & vbCrLf
sConsulta = sConsulta & "           AND PROCE.EST>=5 AND PROCE.EST<12" & vbCrLf
sConsulta = sConsulta & "           GROUP BY PROCE.ANYO,PROCE.GMN1,PROCE.COD,PROCE.DEN,PROCE.FECLIMOFE,PROCE.ADJDIR," & vbCrLf
sConsulta = sConsulta & "            PROCE.FECPRES,PROCE.EQP,PROCE.COM,PROCE.USUAPER," & vbCrLf
sConsulta = sConsulta & "           Proce.FECNEC , Proce.GMN2, Proce.GMN3, Proce.GMN4, Proce.EST,PROCE.FECAPE,PROCE_DEF.SUBASTA" & vbCrLf
sConsulta = sConsulta & "           ORDER BY PROCE.FECLIMOFE" & vbCrLf
sConsulta = sConsulta & "    ELSE" & vbCrLf
sConsulta = sConsulta & "          SELECT PROCE.ANYO,PROCE.GMN1,PROCE.COD,PROCE.DEN,PROCE.FECLIMOFE,PROCE.ADJDIR," & vbCrLf
sConsulta = sConsulta & "             PROCE.FECPRES,PROCE.EQP,PROCE.COM,PROCE.USUAPER," & vbCrLf
sConsulta = sConsulta & "             Proce.FECNEC , Proce.GMN2, Proce.GMN3, Proce.GMN4, Proce.EST,PROCE.FECAPE,PROCE_DEF.SUBASTA,sum(PROCE_PROVE.PUB) as PUB" & vbCrLf
sConsulta = sConsulta & "             From Proce" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PROCE_DEF ON PROCE.ANYO=PROCE_DEF.ANYO AND PROCE.GMN1=PROCE_DEF.GMN1 AND PROCE.COD=PROCE_DEF.PROCE" & vbCrLf
sConsulta = sConsulta & "             INNER JOIN PROCE_PROVE ON" & vbCrLf
sConsulta = sConsulta & "             Proce.anyo = PROCE_PROVE.anyo And Proce.gmn1 = PROCE_PROVE.gmn1 And Proce.COD = PROCE_PROVE.Proce" & vbCrLf
sConsulta = sConsulta & "             AND PROCE_PROVE.EQP=@EQP AND PROCE_PROVE.COM=@COM" & vbCrLf
sConsulta = sConsulta & "    " & vbCrLf
sConsulta = sConsulta & "             WHERE ((PROCE_PROVE.PUB=1 AND convert(char,getdate(),111) <=convert(char,PROCE.FECLIMOFE,111) AND convert(char,dateadd(day, @DESDES   ,PROCE.FECLIMOFE),111)<=convert(char,getdate(),111)" & vbCrLf
sConsulta = sConsulta & "             AND PROCE.EQP=@EQP AND PROCE.COM=@COM)" & vbCrLf
sConsulta = sConsulta & "     " & vbCrLf
sConsulta = sConsulta & "             OR (convert(char,getdate(),111)<=PROCE.FECPRES AND dateadd(day,@DESADJ   ,PROCE.FECPRES)<=convert(char,getdate(),111)))" & vbCrLf
sConsulta = sConsulta & "    " & vbCrLf
sConsulta = sConsulta & "             AND PROCE.EST>=5 AND PROCE.EST<12" & vbCrLf
sConsulta = sConsulta & "               GROUP BY PROCE.ANYO,PROCE.GMN1,PROCE.COD,PROCE.DEN,PROCE.FECLIMOFE,PROCE.ADJDIR," & vbCrLf
sConsulta = sConsulta & "            PROCE.FECPRES,PROCE.EQP,PROCE.COM,PROCE.USUAPER," & vbCrLf
sConsulta = sConsulta & "           Proce.FECNEC , Proce.GMN2, Proce.GMN3, Proce.GMN4, Proce.EST,PROCE.FECAPE, PROCE_DEF.SUBASTA" & vbCrLf
sConsulta = sConsulta & "             ORDER BY PROCE.FECLIMOFE" & vbCrLf
sConsulta = sConsulta & "  Else" & vbCrLf
sConsulta = sConsulta & "    IF @RESPADJ=1  AND @RESPPUB=0" & vbCrLf
sConsulta = sConsulta & "       IF @DESDES=0" & vbCrLf
sConsulta = sConsulta & "         SELECT PROCE.ANYO,PROCE.GMN1,PROCE.COD,PROCE.DEN,PROCE.FECLIMOFE,PROCE.ADJDIR," & vbCrLf
sConsulta = sConsulta & "           PROCE.FECPRES,PROCE.EQP,PROCE.COM,PROCE.USUAPER," & vbCrLf
sConsulta = sConsulta & "           Proce.FECNEC , Proce.GMN2, Proce.GMN3, Proce.GMN4, Proce.EST,PROCE.FECAPE,PROCE_DEF.SUBASTA,sum(PROCE_PROVE.PUB) as PUB" & vbCrLf
sConsulta = sConsulta & "           From Proce" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PROCE_DEF ON PROCE.ANYO=PROCE_DEF.ANYO AND PROCE.GMN1=PROCE_DEF.GMN1 AND PROCE.COD=PROCE_DEF.PROCE" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PROCE_PROVE ON" & vbCrLf
sConsulta = sConsulta & "           Proce.anyo = PROCE_PROVE.anyo And Proce.gmn1 = PROCE_PROVE.gmn1 And Proce.COD = PROCE_PROVE.Proce" & vbCrLf
sConsulta = sConsulta & "           AND PROCE_PROVE.EQP=@EQP AND PROCE_PROVE.COM=@COM" & vbCrLf
sConsulta = sConsulta & "         " & vbCrLf
sConsulta = sConsulta & "           WHERE convert(char,getdate(),111)<=PROCE.FECPRES AND dateadd(day, @DESADJ   ,PROCE.FECPRES)<=convert(char,getdate(),111)" & vbCrLf
sConsulta = sConsulta & "           AND PROCE.EQP=@EQP AND PROCE.COM=@COM" & vbCrLf
sConsulta = sConsulta & "    " & vbCrLf
sConsulta = sConsulta & "           AND PROCE.EST>=5 AND PROCE.EST<12" & vbCrLf
sConsulta = sConsulta & "           GROUP BY PROCE.ANYO,PROCE.GMN1,PROCE.COD,PROCE.DEN,PROCE.FECLIMOFE,PROCE.ADJDIR," & vbCrLf
sConsulta = sConsulta & "            PROCE.FECPRES,PROCE.EQP,PROCE.COM,PROCE.USUAPER," & vbCrLf
sConsulta = sConsulta & "           Proce.FECNEC , Proce.GMN2, Proce.GMN3, Proce.GMN4, Proce.EST,PROCE.FECAPE, PROCE_DEF.SUBASTA" & vbCrLf
sConsulta = sConsulta & "           ORDER BY PROCE.FECLIMOFE" & vbCrLf
sConsulta = sConsulta & "       ELSE" & vbCrLf
sConsulta = sConsulta & "           SELECT PROCE.ANYO,PROCE.GMN1,PROCE.COD,PROCE.DEN,PROCE.FECLIMOFE,PROCE.ADJDIR," & vbCrLf
sConsulta = sConsulta & "           PROCE.FECPRES,PROCE.EQP,PROCE.COM,PROCE.USUAPER," & vbCrLf
sConsulta = sConsulta & "           Proce.FECNEC , Proce.GMN2, Proce.GMN3, Proce.GMN4, Proce.EST,PROCE.FECAPE,PROCE_DEF.SUBASTA,sum(PROCE_PROVE.PUB) as PUB" & vbCrLf
sConsulta = sConsulta & "           From Proce" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PROCE_DEF ON PROCE.ANYO=PROCE_DEF.ANYO AND PROCE.GMN1=PROCE_DEF.GMN1 AND PROCE.COD=PROCE_DEF.PROCE" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PROCE_PROVE ON" & vbCrLf
sConsulta = sConsulta & "           Proce.anyo = PROCE_PROVE.anyo And Proce.gmn1 = PROCE_PROVE.gmn1 And Proce.COD = PROCE_PROVE.Proce" & vbCrLf
sConsulta = sConsulta & "           AND PROCE_PROVE.EQP=@EQP AND PROCE_PROVE.COM=@COM" & vbCrLf
sConsulta = sConsulta & "         " & vbCrLf
sConsulta = sConsulta & "           WHERE ( (PROCE_PROVE.PUB=1 AND convert(char,getdate(),111)<=convert(char,PROCE.FECLIMOFE,111) AND convert(char,dateadd(day, @DESDES   ,PROCE.FECLIMOFE),111)<=convert(char,getdate(),111) )" & vbCrLf
sConsulta = sConsulta & "         " & vbCrLf
sConsulta = sConsulta & "           OR (convert(char,getdate(),111)<=PROCE.FECPRES AND dateadd(day, @DESADJ   ,PROCE.FECPRES)<=convert(char,getdate(),111)" & vbCrLf
sConsulta = sConsulta & "           AND PROCE.EQP=@EQP AND PROCE.COM=@COM))" & vbCrLf
sConsulta = sConsulta & "    " & vbCrLf
sConsulta = sConsulta & "           AND PROCE.EST>=5 AND PROCE.EST<12" & vbCrLf
sConsulta = sConsulta & "           GROUP BY PROCE.ANYO,PROCE.GMN1,PROCE.COD,PROCE.DEN,PROCE.FECLIMOFE,PROCE.ADJDIR," & vbCrLf
sConsulta = sConsulta & "            PROCE.FECPRES,PROCE.EQP,PROCE.COM,PROCE.USUAPER," & vbCrLf
sConsulta = sConsulta & "           Proce.FECNEC , Proce.GMN2, Proce.GMN3, Proce.GMN4, Proce.EST,PROCE.FECAPE, PROCE_DEF.SUBASTA" & vbCrLf
sConsulta = sConsulta & "           ORDER BY PROCE.FECLIMOFE" & vbCrLf
sConsulta = sConsulta & "Else" & vbCrLf
sConsulta = sConsulta & "    IF @RESPADJ=1  AND @RESPPUB=1" & vbCrLf
sConsulta = sConsulta & "  " & vbCrLf
sConsulta = sConsulta & "     SELECT  PROCE.ANYO,PROCE.GMN1,PROCE.COD,PROCE.DEN,PROCE.FECLIMOFE,PROCE.ADJDIR," & vbCrLf
sConsulta = sConsulta & "     PROCE.FECPRES,PROCE.EQP,PROCE.COM,PROCE.USUAPER," & vbCrLf
sConsulta = sConsulta & "     Proce.FECNEC , Proce.GMN2, Proce.GMN3, Proce.GMN4, Proce.EST,PROCE.FECAPE,PROCE_DEF.SUBASTA,sum(PROCE_PROVE.PUB) as PUB" & vbCrLf
sConsulta = sConsulta & "     From Proce" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PROCE_DEF ON PROCE.ANYO=PROCE_DEF.ANYO AND PROCE.GMN1=PROCE_DEF.GMN1 AND PROCE.COD=PROCE_DEF.PROCE" & vbCrLf
sConsulta = sConsulta & "     INNER JOIN PROCE_PROVE ON" & vbCrLf
sConsulta = sConsulta & "     Proce.anyo = PROCE_PROVE.anyo And Proce.gmn1 = PROCE_PROVE.gmn1 And Proce.COD = PROCE_PROVE.Proce" & vbCrLf
sConsulta = sConsulta & "     AND PROCE_PROVE.EQP=@EQP AND PROCE_PROVE.COM=@COM" & vbCrLf
sConsulta = sConsulta & "    " & vbCrLf
sConsulta = sConsulta & "     WHERE ((PROCE_PROVE.PUB=1 AND convert(char,getdate(),111)<=convert(char,PROCE.FECLIMOFE,111) AND convert(char,dateadd(day, @DESDES   ,PROCE.FECLIMOFE),111)<=convert(char,getdate(),111))" & vbCrLf
sConsulta = sConsulta & "     " & vbCrLf
sConsulta = sConsulta & "     OR (convert(char,getdate(),111)<=PROCE.FECPRES AND dateadd(day, @DESADJ   ,PROCE.FECPRES)<=convert(char,getdate(),111)))" & vbCrLf
sConsulta = sConsulta & "     AND PROCE.EQP=@EQP AND PROCE.COM=@COM" & vbCrLf
sConsulta = sConsulta & "     AND PROCE.EST>=5 AND PROCE.EST<12" & vbCrLf
sConsulta = sConsulta & "      GROUP BY PROCE.ANYO,PROCE.GMN1,PROCE.COD,PROCE.DEN,PROCE.FECLIMOFE,PROCE.ADJDIR," & vbCrLf
sConsulta = sConsulta & "            PROCE.FECPRES,PROCE.EQP,PROCE.COM,PROCE.USUAPER," & vbCrLf
sConsulta = sConsulta & "           Proce.FECNEC , Proce.GMN2, Proce.GMN3, Proce.GMN4, Proce.EST,PROCE.FECAPE, PROCE_DEF.SUBASTA" & vbCrLf
sConsulta = sConsulta & "      ORDER BY PROCE.FECLIMOFE" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_TRASPASAR_OFERTA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_TRASPASAR_OFERTA]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_TRASPASAR_OFERTA @ID int , @NUM INT OUTPUT, @NUMNUE INT OUTPUT, @NUMAREV INT OUTPUT, @ERROR SMALLINT = NULL OUTPUT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @NEWOFE INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @NUMOFEWEB AS int" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN1 VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCE INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVE VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @MON VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @ADJUN INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDADJUN INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDGS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @GRUPO VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @ITEM INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @NUMOBJ INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @NUMOBJGS INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @ERROR = 0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @ANYO = ANYO, @GMN1= GMN1, @PROCE = PROCE, @PROVE = PROVE, @MON = MON, @NUMOBJ = NUMOBJ" & vbCrLf
sConsulta = sConsulta & "  FROM P_PROCE_OFE " & vbCrLf
sConsulta = sConsulta & " WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @NUMOBJGS = (SELECT NUMOBJ FROM PROCE_PROVE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE)" & vbCrLf
sConsulta = sConsulta & "IF @NUMOBJGS  <=@NUMOBJ" & vbCrLf
sConsulta = sConsulta & "  UPDATE PROCE_PROVE SET OBJNUEVOS=0 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "/*" & vbCrLf
sConsulta = sConsulta & "DECLARE @EQUIV FLOAT " & vbCrLf
sConsulta = sConsulta & "SELECT @EQUIV = (MON.EQUIV / PROCE.CAMBIO) " & vbCrLf
sConsulta = sConsulta & "  FROM PROCE " & vbCrLf
sConsulta = sConsulta & "     INNER JOIN MON " & vbCrLf
sConsulta = sConsulta & "             ON PROCE.ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "            AND PROCE.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND PROCE.COD=@PROCE  " & vbCrLf
sConsulta = sConsulta & "            AND MON.COD=@MON " & vbCrLf
sConsulta = sConsulta & "*/" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @SUBASTA INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @SUBASTA = SUBASTA " & vbCrLf
sConsulta = sConsulta & "  FROM PROCE_DEF " & vbCrLf
sConsulta = sConsulta & " WHERE ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "   AND GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "   AND PROCE=@PROCE  " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @SUBASTA = 1" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    DECLARE @OFE INT" & vbCrLf
sConsulta = sConsulta & "    SET @OFE = ( SELECT MAX(OFE) FROM PROCE_OFE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE AND OFE!=-1)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    IF @OFE IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "      BEGIN" & vbCrLf
sConsulta = sConsulta & "        INSERT INTO P_OFE_GR_ATRIB (ID, GRUPO, ATRIB_ID,  VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL)" & vbCrLf
sConsulta = sConsulta & "        SELECT @ID, O.GRUPO, O.ATRIB_ID, O.VALOR_NUM, O.VALOR_TEXT, O.VALOR_FEC, O.VALOR_BOOL" & vbCrLf
sConsulta = sConsulta & "          FROM OFE_GR_ATRIB O" & vbCrLf
sConsulta = sConsulta & "         WHERE O.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "           AND O.GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "           AND O.PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "           AND O.PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "           AND O.OFE = @OFE" & vbCrLf
sConsulta = sConsulta & "           AND NOT EXISTS (SELECT * FROM P_OFE_GR_ATRIB T WHERE T.ID = @ID AND T.GRUPO = O.GRUPO AND T.ATRIB_ID = O.ATRIB_ID)" & vbCrLf
sConsulta = sConsulta & "        IF @@ERROR!=0 GOTO ERRORSINTRAN_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "        INSERT INTO P_OFE_ITEM_ATRIB (ID, GRUPO, ITEM, ATRIB_ID,  VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL)" & vbCrLf
sConsulta = sConsulta & "        SELECT @ID, I.GRUPO, IA.ITEM, ATRIB_ID, VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL" & vbCrLf
sConsulta = sConsulta & "          FROM OFE_ITEM_ATRIB IA" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN ITEM I" & vbCrLf
sConsulta = sConsulta & "                   ON I.ANYO = IA.ANYO" & vbCrLf
sConsulta = sConsulta & "                  AND I.GMN1 = IA.GMN1" & vbCrLf
sConsulta = sConsulta & "                  AND I.PROCE = IA.PROCE" & vbCrLf
sConsulta = sConsulta & "                  AND I.ID = IA.ITEM" & vbCrLf
sConsulta = sConsulta & "         WHERE IA.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "           AND IA.GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "           AND IA.PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "           AND IA.PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "           AND IA.OFE = @OFE" & vbCrLf
sConsulta = sConsulta & "           AND NOT EXISTS (SELECT * FROM P_OFE_ITEM_ATRIB T WHERE T.ID = @ID AND T.ATRIB_ID = IA.ATRIB_ID AND T.ITEM = IA.ITEM)" & vbCrLf
sConsulta = sConsulta & "        IF @@ERROR!=0 GOTO ERRORSINTRAN_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "        INSERT INTO P_OFE_ATRIB (ID, ATRIB_ID,  VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL)" & vbCrLf
sConsulta = sConsulta & "        SELECT @ID, O.ATRIB_ID, O.VALOR_NUM, O.VALOR_TEXT, O.VALOR_FEC, O.VALOR_BOOL" & vbCrLf
sConsulta = sConsulta & "          FROM OFE_ATRIB O" & vbCrLf
sConsulta = sConsulta & "         WHERE O.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "           AND O.GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "           AND O.PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "           AND O.PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "           AND O.OFE = @OFE" & vbCrLf
sConsulta = sConsulta & "           AND NOT EXISTS (SELECT * FROM P_OFE_ATRIB T WHERE T.ID = @ID AND T.ATRIB_ID = O.ATRIB_ID)" & vbCrLf
sConsulta = sConsulta & "        IF @@ERROR!=0 GOTO ERRORSINTRAN_" & vbCrLf
sConsulta = sConsulta & "      END " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "      DECLARE @OFEATRIB BIT" & vbCrLf
sConsulta = sConsulta & "      DECLARE @OFEGRUPOATRIB BIT" & vbCrLf
sConsulta = sConsulta & "      DECLARE @OFEITEMATRIB BIT" & vbCrLf
sConsulta = sConsulta & "      DECLARE @PAID INT" & vbCrLf
sConsulta = sConsulta & "      DECLARE @AMBITO SMALLINT" & vbCrLf
sConsulta = sConsulta & "      DECLARE @TIPO SMALLINT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "      DECLARE CATRIBS CURSOR LOCAL FORWARD_ONLY  FOR " & vbCrLf
sConsulta = sConsulta & "      SELECT PA.ID, AMBITO, GRUPO, TIPO" & vbCrLf
sConsulta = sConsulta & "        FROM PROCE_ATRIB PA" & vbCrLf
sConsulta = sConsulta & "         INNER JOIN DEF_ATRIB DA" & vbCrLf
sConsulta = sConsulta & "                 ON PA.ATRIB = DA.ID" & vbCrLf
sConsulta = sConsulta & "       WHERE ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "         AND GMN1= @GMN1" & vbCrLf
sConsulta = sConsulta & "         AND PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "         AND INTERNO = 0" & vbCrLf
sConsulta = sConsulta & "         AND OBLIG = 1" & vbCrLf
sConsulta = sConsulta & "      " & vbCrLf
sConsulta = sConsulta & "      OPEN CATRIBS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "      FETCH NEXT FROM CATRIBS INTO @PAID, @AMBITO, @GRUPO,  @TIPO" & vbCrLf
sConsulta = sConsulta & "      SET @OFEATRIB=0" & vbCrLf
sConsulta = sConsulta & "      SET @OFEGRUPOATRIB=0" & vbCrLf
sConsulta = sConsulta & "      SET @OFEITEMATRIB=0" & vbCrLf
sConsulta = sConsulta & "      WHILE @@FETCH_STATUS=0 AND @OFEATRIB=0 AND @OFEGRUPOATRIB=0 AND @OFEITEMATRIB=0" & vbCrLf
sConsulta = sConsulta & "        BEGIN" & vbCrLf
sConsulta = sConsulta & "          IF @AMBITO = 1" & vbCrLf
sConsulta = sConsulta & "            BEGIN" & vbCrLf
sConsulta = sConsulta & "              SET @OFEATRIB =1" & vbCrLf
sConsulta = sConsulta & "              SELECT @OFEATRIB = CASE " & vbCrLf
sConsulta = sConsulta & "                        WHEN CASE @TIPO " & vbCrLf
sConsulta = sConsulta & "                                WHEN 1 THEN CASE WHEN VALOR_TEXT ='' THEN NULL ELSE VALOR_TEXT END " & vbCrLf
sConsulta = sConsulta & "                                WHEN 2 THEN CAST(VALOR_NUM AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                                WHEN 3 THEN CAST(VALOR_FEC AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                                WHEN 4 THEN CAST(VALOR_BOOL AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                             END IS NULL " & vbCrLf
sConsulta = sConsulta & "                        THEN 1 " & vbCrLf
sConsulta = sConsulta & "                        ELSE 0 " & vbCrLf
sConsulta = sConsulta & "                     END" & vbCrLf
sConsulta = sConsulta & "                FROM P_OFE_ATRIB" & vbCrLf
sConsulta = sConsulta & "               WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "                 AND ATRIB_ID = @PAID" & vbCrLf
sConsulta = sConsulta & "            END" & vbCrLf
sConsulta = sConsulta & "          IF @AMBITO = 2" & vbCrLf
sConsulta = sConsulta & "            BEGIN" & vbCrLf
sConsulta = sConsulta & "              SET @OFEGRUPOATRIB =0" & vbCrLf
sConsulta = sConsulta & "              IF @GRUPO IS NULL " & vbCrLf
sConsulta = sConsulta & "                BEGIN" & vbCrLf
sConsulta = sConsulta & "                IF (SELECT COUNT(*) " & vbCrLf
sConsulta = sConsulta & "                      FROM P_OFE_GR_ATRIB " & vbCrLf
sConsulta = sConsulta & "                     WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "                       AND ATRIB_ID = @PAID" & vbCrLf
sConsulta = sConsulta & "                       AND CASE @TIPO " & vbCrLf
sConsulta = sConsulta & "                                WHEN 1 THEN CASE WHEN VALOR_TEXT ='' THEN NULL ELSE VALOR_TEXT END " & vbCrLf
sConsulta = sConsulta & "                                WHEN 2 THEN CAST(VALOR_NUM AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                                WHEN 3 THEN CAST(VALOR_FEC AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                                WHEN 4 THEN CAST(VALOR_BOOL AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                             END IS NOT NULL) <> (SELECT COUNT(*) " & vbCrLf
sConsulta = sConsulta & "                       FROM PROCE_GRUPO " & vbCrLf
sConsulta = sConsulta & "                      WHERE ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "                        AND GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "                        AND PROCE = @PROCE)" & vbCrLf
sConsulta = sConsulta & "                   SET @OFEGRUPOATRIB = 1" & vbCrLf
sConsulta = sConsulta & "                 END" & vbCrLf
sConsulta = sConsulta & "               ELSE" & vbCrLf
sConsulta = sConsulta & "                IF (SELECT COUNT(*) " & vbCrLf
sConsulta = sConsulta & "                      FROM P_OFE_GR_ATRIB " & vbCrLf
sConsulta = sConsulta & "                     WHERE GRUPO = @GRUPO" & vbCrLf
sConsulta = sConsulta & "                       AND ATRIB_ID = @PAID" & vbCrLf
sConsulta = sConsulta & "                       AND CASE @TIPO " & vbCrLf
sConsulta = sConsulta & "                                WHEN 1 THEN CASE WHEN VALOR_TEXT ='' THEN NULL ELSE VALOR_TEXT END " & vbCrLf
sConsulta = sConsulta & "                                WHEN 2 THEN CAST(VALOR_NUM AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                                WHEN 3 THEN CAST(VALOR_FEC AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                                WHEN 4 THEN CAST(VALOR_BOOL AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                             END IS NOT NULL)  =0 " & vbCrLf
sConsulta = sConsulta & "                       SET @OFEGRUPOATRIB = 1" & vbCrLf
sConsulta = sConsulta & "            END" & vbCrLf
sConsulta = sConsulta & "          IF @AMBITO = 3" & vbCrLf
sConsulta = sConsulta & "            BEGIN" & vbCrLf
sConsulta = sConsulta & "              SET @OFEITEMATRIB =0" & vbCrLf
sConsulta = sConsulta & "              IF @GRUPO IS NULL " & vbCrLf
sConsulta = sConsulta & "                BEGIN" & vbCrLf
sConsulta = sConsulta & "                  IF (SELECT COUNT(*) " & vbCrLf
sConsulta = sConsulta & "                        FROM P_OFE_ITEM_ATRIB " & vbCrLf
sConsulta = sConsulta & "                       WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "                         AND ATRIB_ID = @PAID" & vbCrLf
sConsulta = sConsulta & "                         AND CASE @TIPO " & vbCrLf
sConsulta = sConsulta & "                                WHEN 1 THEN CASE WHEN VALOR_TEXT ='' THEN NULL ELSE VALOR_TEXT END " & vbCrLf
sConsulta = sConsulta & "                                WHEN 2 THEN CAST(VALOR_NUM AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                                WHEN 3 THEN CAST(VALOR_FEC AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                                WHEN 4 THEN CAST(VALOR_BOOL AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                     END IS NOT NULL)  <>" & vbCrLf
sConsulta = sConsulta & "                      (SELECT COUNT(*) " & vbCrLf
sConsulta = sConsulta & "                         FROM ITEM " & vbCrLf
sConsulta = sConsulta & "                        WHERE ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "                          AND GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "                          AND PROCE = @PROCE                    " & vbCrLf
sConsulta = sConsulta & "                          AND CONF = 1)" & vbCrLf
sConsulta = sConsulta & "                       SET @OFEITEMATRIB = 1" & vbCrLf
sConsulta = sConsulta & "                 END" & vbCrLf
sConsulta = sConsulta & "              ELSE" & vbCrLf
sConsulta = sConsulta & "                 begin" & vbCrLf
sConsulta = sConsulta & "                  IF (SELECT COUNT(*) " & vbCrLf
sConsulta = sConsulta & "                        FROM P_OFE_ITEM_ATRIB" & vbCrLf
sConsulta = sConsulta & "                       WHERE GRUPO = @GRUPO" & vbCrLf
sConsulta = sConsulta & "                         AND ID = @ID" & vbCrLf
sConsulta = sConsulta & "                         AND ATRIB_ID = @PAID" & vbCrLf
sConsulta = sConsulta & "                         AND CASE @TIPO " & vbCrLf
sConsulta = sConsulta & "                                WHEN 1 THEN CASE WHEN VALOR_TEXT ='' THEN NULL ELSE VALOR_TEXT END " & vbCrLf
sConsulta = sConsulta & "                                WHEN 2 THEN CAST(VALOR_NUM AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                                WHEN 3 THEN CAST(VALOR_FEC AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                                WHEN 4 THEN CAST(VALOR_BOOL AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                             END IS NOT NULL)  <>" & vbCrLf
sConsulta = sConsulta & "                      (SELECT COUNT(*) " & vbCrLf
sConsulta = sConsulta & "                         FROM ITEM " & vbCrLf
sConsulta = sConsulta & "                        WHERE ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "                          AND GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "                          AND PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "                          AND GRUPO = @GRUPO" & vbCrLf
sConsulta = sConsulta & "                          AND CONF = 1)" & vbCrLf
sConsulta = sConsulta & "                       SET @OFEITEMATRIB = 1" & vbCrLf
sConsulta = sConsulta & "                   end" & vbCrLf
sConsulta = sConsulta & "            END" & vbCrLf
sConsulta = sConsulta & "            FETCH NEXT FROM CATRIBS INTO @PAID, @AMBITO, @GRUPO,  @TIPO" & vbCrLf
sConsulta = sConsulta & "          END" & vbCrLf
sConsulta = sConsulta & "      CLOSE CATRIBS" & vbCrLf
sConsulta = sConsulta & "      DEALLOCATE CATRIBS" & vbCrLf
sConsulta = sConsulta & "      IF @OFEATRIB = 1" & vbCrLf
sConsulta = sConsulta & "        SET @ERROR = 2" & vbCrLf
sConsulta = sConsulta & "      IF @OFEGRUPOATRIB = 1" & vbCrLf
sConsulta = sConsulta & "        SET @ERROR = 3" & vbCrLf
sConsulta = sConsulta & "      IF @OFEITEMATRIB = 1" & vbCrLf
sConsulta = sConsulta & "        SET @ERROR = 4" & vbCrLf
sConsulta = sConsulta & "      IF @ERROR>0" & vbCrLf
sConsulta = sConsulta & "        GOTO ELIMINAR_" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "DECLARE  @ESTWEB VARCHAR(3) " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "SELECT @ESTWEB=ESTWEB FROM PARGEN_DEF INNER JOIN PARGEN_GEST ON PARGEN_DEF.ID=PARGEN_GEST.ID  " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN TRANSACTION" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE SET ULT= 0, LEIDO = 1  WHERE ANYO = @ANYO AND GMN1 = @GMN1 AND PROCE = @PROCE AND PROVE = @PROVE AND ULT = 1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @NEWOFE = ISNULL(MAX(OFE),0) + 1 FROM PROCE_OFE WHERE ANYO = @ANYO AND GMN1 = @GMN1 AND PROCE = @PROCE AND PROVE = @PROVE AND OFE != -1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO PROCE_OFE(ANYO, GMN1, PROCE, PROVE, OFE, EST, FECREC, FECVAL, MON, CAMBIO, OBS, LEIDO, ULT, ENVIADA, OBSADJUN)" & vbCrLf
sConsulta = sConsulta & "SELECT @ANYO, @GMN1, @PROCE, @PROVE, @NEWOFE, @ESTWEB, FECREC, FECVAL, MON, CAMBIO, OBS, 0, 1, 1, OBSADJUN" & vbCrLf
sConsulta = sConsulta & "  FROM P_PROCE_OFE" & vbCrLf
sConsulta = sConsulta & "WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE GRUPO_OFE " & vbCrLf
sConsulta = sConsulta & "   SET OBSADJUN = PGO.OBSADJUN" & vbCrLf
sConsulta = sConsulta & "  FROM GRUPO_OFE GO" & vbCrLf
sConsulta = sConsulta & "      INNER JOIN P_GRUPO_OFE PGO" & vbCrLf
sConsulta = sConsulta & "              ON GO.GRUPO = PGO.GRUPO" & vbCrLf
sConsulta = sConsulta & " WHERE GO.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "   AND GO.GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "   AND GO.PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "   AND GO.PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "   AND GO.OFE = @NEWOFE" & vbCrLf
sConsulta = sConsulta & "   AND PGO.ID = @ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_OFE" & vbCrLf
sConsulta = sConsulta & "   SET PRECIO = PIO.PRECIO," & vbCrLf
sConsulta = sConsulta & "       CANTMAX = PIO.CANTMAX," & vbCrLf
sConsulta = sConsulta & "       PRECIO2 = PIO.PRECIO2," & vbCrLf
sConsulta = sConsulta & "       PRECIO3 = PIO.PRECIO3," & vbCrLf
sConsulta = sConsulta & "       PREC_VALIDO = PIO.PRECIO," & vbCrLf
sConsulta = sConsulta & "       OBSADJUN = PIO.OBSADJUN," & vbCrLf
sConsulta = sConsulta & "       COMENT1 = PIO.COMENT1," & vbCrLf
sConsulta = sConsulta & "       COMENT2 = PIO.COMENT2," & vbCrLf
sConsulta = sConsulta & "       COMENT3 = PIO.COMENT3" & vbCrLf
sConsulta = sConsulta & "  FROM ITEM_OFE IO" & vbCrLf
sConsulta = sConsulta & "     INNER JOIN P_ITEM_OFE PIO" & vbCrLf
sConsulta = sConsulta & "             ON IO.ITEM = PIO.ITEM" & vbCrLf
sConsulta = sConsulta & " WHERE IO.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "   AND IO.GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "   AND IO.PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "   AND IO.PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "   AND IO.NUM = @NEWOFE" & vbCrLf
sConsulta = sConsulta & "   AND PIO.ID = @ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_ITEM_ADJUN " & vbCrLf
sConsulta = sConsulta & "  FROM OFE_ITEM_ADJUN R" & vbCrLf
sConsulta = sConsulta & " WHERE R.ANYO = @ANYO " & vbCrLf
sConsulta = sConsulta & "   AND R.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "   AND R.PROCE= @PROCE " & vbCrLf
sConsulta = sConsulta & "   AND R.PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "   AND R.OFE IN (@NEWOFE,-1)" & vbCrLf
sConsulta = sConsulta & "   AND EXISTS (SELECT * FROM P_OFE_ITEM_ADJUN P " & vbCrLf
sConsulta = sConsulta & "                WHERE P.ID  = @ID " & vbCrLf
sConsulta = sConsulta & "                  AND P.ITEM = R.ITEM" & vbCrLf
sConsulta = sConsulta & "                  AND P.IDGS = R.ID)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_ITEM_ATRIB " & vbCrLf
sConsulta = sConsulta & "  FROM OFE_ITEM_ATRIB R" & vbCrLf
sConsulta = sConsulta & " WHERE R.ANYO = @ANYO " & vbCrLf
sConsulta = sConsulta & "   AND R.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "   AND R.PROCE= @PROCE " & vbCrLf
sConsulta = sConsulta & "   AND R.PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "   AND R.OFE IN (@NEWOFE,-1)" & vbCrLf
sConsulta = sConsulta & "   AND EXISTS (SELECT * FROM P_OFE_ITEM_ATRIB P " & vbCrLf
sConsulta = sConsulta & "                WHERE P.ID  = @ID " & vbCrLf
sConsulta = sConsulta & "                  AND P.ITEM = R.ITEM" & vbCrLf
sConsulta = sConsulta & "                  AND P.ATRIB_ID = R.ATRIB_ID)" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_GR_ATRIB " & vbCrLf
sConsulta = sConsulta & "  FROM OFE_GR_ATRIB R" & vbCrLf
sConsulta = sConsulta & " WHERE R.ANYO = @ANYO " & vbCrLf
sConsulta = sConsulta & "   AND R.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "   AND R.PROCE= @PROCE " & vbCrLf
sConsulta = sConsulta & "   AND R.PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "   AND R.OFE IN (@NEWOFE,-1)" & vbCrLf
sConsulta = sConsulta & "   AND EXISTS (SELECT * FROM P_OFE_GR_ATRIB P " & vbCrLf
sConsulta = sConsulta & "                WHERE P.ID  = @ID " & vbCrLf
sConsulta = sConsulta & "                  AND CAST(P.GRUPO AS VARCHAR(50)) = CAST(R.GRUPO AS VARCHAR(50))" & vbCrLf
sConsulta = sConsulta & "                  AND P.ATRIB_ID = R.ATRIB_ID)" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_GR_ADJUN " & vbCrLf
sConsulta = sConsulta & "  FROM OFE_GR_ADJUN R" & vbCrLf
sConsulta = sConsulta & " WHERE R.ANYO = @ANYO " & vbCrLf
sConsulta = sConsulta & "   AND R.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "   AND R.PROCE= @PROCE " & vbCrLf
sConsulta = sConsulta & "   AND R.PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "   AND R.OFE IN (@NEWOFE,-1)" & vbCrLf
sConsulta = sConsulta & "   AND EXISTS (SELECT * FROM P_OFE_GR_ADJUN P " & vbCrLf
sConsulta = sConsulta & "                WHERE P.ID  = @ID " & vbCrLf
sConsulta = sConsulta & "                  AND P.GRUPO = R.GRUPO" & vbCrLf
sConsulta = sConsulta & "                  AND P.IDGS = R.ID)" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_ATRIB " & vbCrLf
sConsulta = sConsulta & "  FROM OFE_ATRIB R" & vbCrLf
sConsulta = sConsulta & " WHERE R.ANYO = @ANYO " & vbCrLf
sConsulta = sConsulta & "   AND R.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "   AND R.PROCE= @PROCE " & vbCrLf
sConsulta = sConsulta & "   AND R.PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "   AND R.OFE IN (@NEWOFE,-1)" & vbCrLf
sConsulta = sConsulta & "   AND EXISTS (SELECT * FROM P_OFE_ATRIB P " & vbCrLf
sConsulta = sConsulta & "                WHERE P.ID  = @ID " & vbCrLf
sConsulta = sConsulta & "                  AND P.ATRIB_ID = R.ATRIB_ID)" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM PROCE_OFE_ADJUN " & vbCrLf
sConsulta = sConsulta & "  FROM PROCE_OFE_ADJUN R" & vbCrLf
sConsulta = sConsulta & " WHERE R.ANYO = @ANYO " & vbCrLf
sConsulta = sConsulta & "   AND R.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "   AND R.PROCE= @PROCE " & vbCrLf
sConsulta = sConsulta & "   AND R.PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "   AND R.OFE IN (@NEWOFE,-1)" & vbCrLf
sConsulta = sConsulta & "   AND EXISTS (SELECT * FROM P_PROCE_OFE_ADJUN P " & vbCrLf
sConsulta = sConsulta & "                WHERE P.ID  = @ID " & vbCrLf
sConsulta = sConsulta & "                  AND P.IDGS = R.ID)" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM PROCE_OFE_ADJUN WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE= @PROCE and PROVE=@PROVE AND OFE = -1" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_GR_ADJUN WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE= @PROCE and PROVE=@PROVE AND OFE = -1" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_ITEM_ADJUN WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE= @PROCE and PROVE=@PROVE AND OFE = -1" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_ATRIB WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE= @PROCE and PROVE=@PROVE AND OFE = -1" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_GR_ATRIB WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE= @PROCE and PROVE=@PROVE AND OFE = -1" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_ITEM_ATRIB WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE= @PROCE and PROVE=@PROVE AND OFE = -1" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM ITEM_OFE WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE= @PROCE and PROVE=@PROVE AND NUM = -1" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM GRUPO_OFE WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE= @PROCE and PROVE=@PROVE AND OFE = -1" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM PROCE_OFE WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE= @PROCE and PROVE=@PROVE AND OFE = -1" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR LOCAL FOR" & vbCrLf
sConsulta = sConsulta & "SELECT ADJUN, IDGS FROM P_PROCE_OFE_ADJUN WHERE ID=@ID AND IDGS IS NULL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "OPEN C" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C INTO @ADJUN, @IDGS" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    SELECT @IDADJUN = isnull(MAX(ID),0) + 1 FROM ADJUN " & vbCrLf
sConsulta = sConsulta & "    INSERT INTO ADJUN (ID, WEB) VALUES (@IDADJUN, 1) " & vbCrLf
sConsulta = sConsulta & "    UPDATE P_PROCE_OFE_ADJUN SET IDGS = @IDADJUN WHERE ID = @ID AND ADJUN = @ADJUN" & vbCrLf
sConsulta = sConsulta & "    IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "    FETCH NEXT FROM C INTO @ADJUN, @IDGS" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "CLOSE C" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO PROCE_OFE_ADJUN(ANYO, GMN1, PROCE, PROVE, OFE, ID, NOM, COM, IDPORTAL, DATASIZE)" & vbCrLf
sConsulta = sConsulta & "SELECT @ANYO, @GMN1, @PROCE, @PROVE, @NEWOFE, POA.IDGS, POA.NOM, POA.COM, POA.IDPORTAL, POA.DATASIZE" & vbCrLf
sConsulta = sConsulta & "  FROM P_PROCE_OFE_ADJUN POA" & vbCrLf
sConsulta = sConsulta & "         INNER JOIN ADJUN A" & vbCrLf
sConsulta = sConsulta & "                 ON POA.IDGS = A.ID" & vbCrLf
sConsulta = sConsulta & "WHERE POA.ID = @ID" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR LOCAL FOR" & vbCrLf
sConsulta = sConsulta & "SELECT ADJUN, IDGS, GRUPO FROM P_OFE_GR_ADJUN WHERE ID=@ID AND IDGS IS NULL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "OPEN C" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C INTO @ADJUN, @IDGS, @GRUPO" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    SELECT @IDADJUN = isnull(MAX(ID),0) + 1 FROM ADJUN " & vbCrLf
sConsulta = sConsulta & "    INSERT INTO ADJUN (ID, WEB) VALUES (@IDADJUN, 1) " & vbCrLf
sConsulta = sConsulta & "    UPDATE P_OFE_GR_ADJUN SET IDGS = @IDADJUN WHERE ID = @ID AND ADJUN = @ADJUN AND GRUPO = @GRUPO" & vbCrLf
sConsulta = sConsulta & "    IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "    FETCH NEXT FROM C INTO @ADJUN, @IDGS, @GRUPO" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "CLOSE C" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO OFE_GR_ADJUN(ANYO, GMN1, PROCE, GRUPO, PROVE, OFE, ID, NOM, COM, IDPORTAL, DATASIZE)" & vbCrLf
sConsulta = sConsulta & "SELECT @ANYO, @GMN1, @PROCE, OGA.GRUPO, @PROVE, @NEWOFE, OGA.IDGS, OGA.NOM, OGA.COM, OGA.IDPORTAL, OGA.DATASIZE" & vbCrLf
sConsulta = sConsulta & "  FROM P_OFE_GR_ADJUN OGA" & vbCrLf
sConsulta = sConsulta & "         INNER JOIN ADJUN A" & vbCrLf
sConsulta = sConsulta & "                 ON OGA.IDGS = A.ID" & vbCrLf
sConsulta = sConsulta & " WHERE OGA.ID = @ID" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR LOCAL FOR" & vbCrLf
sConsulta = sConsulta & "SELECT ADJUN, IDGS, ITEM FROM P_OFE_ITEM_ADJUN WHERE ID=@ID AND IDGS IS NULL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "OPEN C" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C INTO @ADJUN, @IDGS, @ITEM" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    SELECT @IDADJUN = isnull(MAX(ID),0) + 1 FROM ADJUN " & vbCrLf
sConsulta = sConsulta & "    INSERT INTO ADJUN (ID, WEB) VALUES (@IDADJUN, 1) " & vbCrLf
sConsulta = sConsulta & "    UPDATE P_OFE_ITEM_ADJUN SET IDGS = @IDADJUN WHERE ID = @ID AND ADJUN = @ADJUN AND ITEM = @ITEM" & vbCrLf
sConsulta = sConsulta & "    IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "    FETCH NEXT FROM C INTO @ADJUN, @IDGS, @ITEM" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "CLOSE C" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO OFE_ITEM_ADJUN(ANYO, GMN1, PROCE, ITEM, PROVE, OFE, ID, NOM, COM, IDPORTAL, DATASIZE)" & vbCrLf
sConsulta = sConsulta & "SELECT @ANYO, @GMN1, @PROCE, OIA.ITEM, @PROVE, @NEWOFE, OIA.IDGS, OIA.NOM, OIA.COM, OIA.IDPORTAL, OIA.DATASIZE" & vbCrLf
sConsulta = sConsulta & "  FROM P_OFE_ITEM_ADJUN OIA" & vbCrLf
sConsulta = sConsulta & "         INNER JOIN ADJUN A" & vbCrLf
sConsulta = sConsulta & "                 ON OIA.IDGS = A.ID" & vbCrLf
sConsulta = sConsulta & "WHERE OIA.ID = @ID" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO OFE_GR_ATRIB(ANYO, GMN1, PROCE, GRUPO, PROVE, OFE, ATRIB_ID, VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL)" & vbCrLf
sConsulta = sConsulta & "SELECT @ANYO, @GMN1, @PROCE, GRUPO, @PROVE, @NEWOFE, ATRIB_ID, VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL" & vbCrLf
sConsulta = sConsulta & "  FROM P_OFE_GR_ATRIB" & vbCrLf
sConsulta = sConsulta & " WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO OFE_ITEM_ATRIB(ANYO, GMN1, PROCE, ITEM, PROVE, OFE, ATRIB_ID, VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL)" & vbCrLf
sConsulta = sConsulta & "SELECT @ANYO, @GMN1, @PROCE, ITEM, @PROVE, @NEWOFE, ATRIB_ID, VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL" & vbCrLf
sConsulta = sConsulta & "  FROM P_OFE_ITEM_ATRIB" & vbCrLf
sConsulta = sConsulta & " WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO OFE_ATRIB(ANYO, GMN1, PROCE, PROVE, OFE, ATRIB_ID, VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL)" & vbCrLf
sConsulta = sConsulta & "SELECT @ANYO, @GMN1, @PROCE, @PROVE, @NEWOFE, ATRIB_ID, VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL" & vbCrLf
sConsulta = sConsulta & "  FROM P_OFE_ATRIB " & vbCrLf
sConsulta = sConsulta & " WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE SET OFE = 1  WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE = @PROCE AND PROVE=@PROVE AND OFE = 0" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @NUMOFEWEB = isnull(MAX(OFEWEB),0)+1 FROM PROCE_OFE_WEB WHERE ANYO = @ANYO AND GMN1 = @GMN1 AND PROCE = @PROCE AND PROVE = @PROVE " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO PROCE_OFE_WEB (ANYO, GMN1, PROCE, PROVE, OFEWEB, OFE, FECREC, FECVAL, MON)" & vbCrLf
sConsulta = sConsulta & "SELECT ANYO, GMN1, PROCE, PROVE, @NUMOFEWEB, OFE, FECREC,FECVAL,MON " & vbCrLf
sConsulta = sConsulta & "FROM PROCE_OFE " & vbCrLf
sConsulta = sConsulta & "WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE = @PROCE AND PROVE = @PROVE AND OFE = @NEWOFE" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO ITEM_OFE_WEB (ANYO, GMN1, PROCE, ITEM, PROVE, NUM, PRECIO, CANTMAX)" & vbCrLf
sConsulta = sConsulta & "SELECT ANYO, GMN1, PROCE, ITEM, PROVE, @NUMOFEWEB, PRECIO, CANTMAX" & vbCrLf
sConsulta = sConsulta & "FROM ITEM_OFE" & vbCrLf
sConsulta = sConsulta & "WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE = @PROCE AND PROVE = @PROVE AND NUM = @NEWOFE" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO PROCE_OFE_WEB_ADJUN (ANYO, GMN1, PROCE, PROVE, OFEWEB, ID, NOM, COM, IDPORTAL)" & vbCrLf
sConsulta = sConsulta & "SELECT ANYO, GMN1, PROCE, PROVE, @NUMOFEWEB, ID, NOM, COM, IDPORTAL" & vbCrLf
sConsulta = sConsulta & "FROM PROCE_OFE_ADJUN" & vbCrLf
sConsulta = sConsulta & "WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE = @PROCE AND PROVE = @PROVE AND OFE = @NEWOFE" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_OFE SET ULT = 0 WHERE ANYO = @ANYO AND GMN1 = @GMN1 AND PROCE =@PROCE AND PROVE=@PROVE AND NUM!=@NEWOFE" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "COMMIT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "GOTO ELIMINAR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ERROR_:" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @ERROR = 100" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ROLLBACK" & vbCrLf
sConsulta = sConsulta & "GOTO ELIMINAR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ERRORSINTRAN_:" & vbCrLf
sConsulta = sConsulta & "SET @ERROR = 200" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ELIMINAR_:" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC NUM_PROCE_PUB @PROVE =@PROVE ,@NUM = @NUM OUTPUT,@NUMNUE = @NUMNUE OUTPUT ,@NUMAREV = @NUMAREV OUTPUT " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM P_PROCE_OFE WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM P_GRUPO_OFE WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM P_ITEM_OFE WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM P_OFE_ATRIB WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM P_OFE_GR_ATRIB WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM P_OFE_ITEM_ATRIB WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM P_PROCE_OFE_ADJUN WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM P_OFE_GR_ADJUN WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM P_OFE_ITEM_ADJUN WHERE ID=@ID" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Private Sub V_2_11_5_TablasModif_04()
Dim sConsulta As String

'_Moneda del usuario del EP
sConsulta = "  UPDATE USU SET MON = (SELECT MONCEN FROM PARGEN_DEF) WHERE USU.FSEP=1"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "  ALTER TABLE [dbo].[CONF_VISTAS_ITEM] ADD"
sConsulta = sConsulta & " [IMP_OFE_POS] [smallint] NULL,"
sConsulta = sConsulta & " [IMP_OFE_VISIBLE] [tinyint] NOT NULL CONSTRAINT [DF_CONF_VISTAS_ITEM_IMP_OFE_VISIBLE] DEFAULT 1,"
sConsulta = sConsulta & " [IMP_OFE_WIDTH] [float] NULL,"
sConsulta = sConsulta & " [AHORRO_OFE_POS] [smallint] NULL,"
sConsulta = sConsulta & " [AHORRO_OFE_VISIBLE] [tinyint] NOT NULL CONSTRAINT [DF_CONF_VISTAS_ITEM_AHORRO_OFE_VISIBLE] DEFAULT 1,"
sConsulta = sConsulta & " [AHORRO_OFE_WIDTH] [float] NULL,"
sConsulta = sConsulta & " [AHORRO_OFE_PORCEN_POS] [smallint] NULL,"
sConsulta = sConsulta & " [AHORRO_OFE_PORCEN_VISIBLE] [tinyint] NOT NULL CONSTRAINT [DF_CONF_VISTAS_ITEM_AHORRO_OFE_PORCEN_VISIBLE] DEFAULT 1,"
sConsulta = sConsulta & " [AHORRO_OFE_PORCEN_WIDTH] [float] NULL"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "  ALTER TABLE [dbo].[ORDEN_ENTREGA] ADD"
sConsulta = sConsulta & " [MON] [varchar] (" & gLongitudesDeCodigos.giLongCodMON & ") NULL,"
sConsulta = sConsulta & " [CAMBIO] [float] NULL"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "  UPDATE ORDEN_ENTREGA SET MON = (SELECT MONCEN FROM PARGEN_DEF),CAMBIO=1 WHERE ORDEN_ENTREGA.TIPO=0"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "  UPDATE LOG_ORDEN_ENTREGA SET MON = (SELECT MONCEN FROM PARGEN_DEF),CAMBIO=1 WHERE LOG_ORDEN_ENTREGA.TIPO=0"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE [dbo].[PROCE_OFE] CHECK CONSTRAINT ALL"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE [dbo].[USU] CHECK CONSTRAINT ALL"
ExecuteSQL gRDOCon, sConsulta

End Sub

Private Sub V_2_11_5_Triggers_04()
Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ORDEN_EST_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[ORDEN_EST_TG_INS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TRIGGER ORDEN_EST_TG_INS ON dbo.ORDEN_EST " & vbCrLf
sConsulta = sConsulta & "FOR INSERT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @EST AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_ORDEN AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @SENTIDO AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PER AS VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @USU AS VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @EXTERNO AS BIT" & vbCrLf
sConsulta = sConsulta & "DECLARE @TIPO AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ACTIVA AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ACTIVLOG AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ORIGEN AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @COMENT AS VARCHAR(255)" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_LOG_ORDEN_ENTREGA AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVE AS VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PAG AS VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @INTEGRACION AS TINYINT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_ORDEN_EST_Ins CURSOR LOCAL FOR SELECT EST,ID,ORDEN,PER,COMENT,EXTERNO FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_ORDEN_EST_Ins" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_ORDEN_EST_Ins INTO @EST,@ID,@ID_ORDEN,@PER,@COMENT,@EXTERNO" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @EXTERNO IS NULL" & vbCrLf
sConsulta = sConsulta & "  -- si el cambio de estado de la orden est� originada en FSGS o en EP o en PORTAL (EXTERNO = NULL) tenemos que grabar en las tablas de LOG para luego exportar los mvtos" & vbCrLf
sConsulta = sConsulta & " BEGIN" & vbCrLf
sConsulta = sConsulta & "  IF (@EST = 2 OR @EST = 3 OR @EST = 4 OR @EST = 20 OR @EST = 21) " & vbCrLf
sConsulta = sConsulta & "  -- Si es uno de los estados que nos interesan para grabar en LOG_ORDEN_ENTREGA (emitido,aceptado,rechazado,anulado,en camino)" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "-- Recuperamos el tipo de orden de entrega para ver si est� activada su integraci�n" & vbCrLf
sConsulta = sConsulta & "     SELECT  @TIPO=TIPO,@PROVE=PROVE FROM ORDEN_ENTREGA WHERE ORDEN_ENTREGA.ID = @ID_ORDEN" & vbCrLf
sConsulta = sConsulta & "     SELECT  @ACTIVLOG = ACTIVLOG FROM PARGEN_GEST WHERE ID = 1" & vbCrLf
sConsulta = sConsulta & "     SELECT  @INTEGRACION =INTEGRACION FROM PARGEN_INTERNO WHERE ID = 1" & vbCrLf
sConsulta = sConsulta & "     IF @INTEGRACION = 0" & vbCrLf
sConsulta = sConsulta & "        SET @ACTIVA = 0" & vbCrLf
sConsulta = sConsulta & "     ELSE " & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "        IF @TIPO = 0 --Pedidos directos" & vbCrLf
sConsulta = sConsulta & "           SELECT @ACTIVA=ACTIVA,@SENTIDO=SENTIDO FROM TABLAS_INTEGRACION WHERE ID = 13" & vbCrLf
sConsulta = sConsulta & "         ELSE" & vbCrLf
sConsulta = sConsulta & "           SELECT @ACTIVA=ACTIVA,@SENTIDO=SENTIDO FROM TABLAS_INTEGRACION WHERE ID = 11" & vbCrLf
sConsulta = sConsulta & "        END" & vbCrLf
sConsulta = sConsulta & "         IF (@ACTIVA = 1 AND (@SENTIDO=3 OR @SENTIDO=1)) OR @ACTIVLOG = 1" & vbCrLf
sConsulta = sConsulta & "         -- si el sentido es = 2  (solo entrada) o no est� activa la integraci�n  no grabamos en log" & vbCrLf
sConsulta = sConsulta & "         BEGIN" & vbCrLf
sConsulta = sConsulta & "            IF @ACTIVLOG = 1 AND @ACTIVA = 1 " & vbCrLf
sConsulta = sConsulta & "               -- Integracion y log de activida" & vbCrLf
sConsulta = sConsulta & "               SET @ORIGEN = 2" & vbCrLf
sConsulta = sConsulta & "            ELSE" & vbCrLf
sConsulta = sConsulta & "                IF @ACTIVLOG = 0" & vbCrLf
sConsulta = sConsulta & "                   -- solo log de actividad" & vbCrLf
sConsulta = sConsulta & "                   SET @ORIGEN = 0" & vbCrLf
sConsulta = sConsulta & "                ELSE" & vbCrLf
sConsulta = sConsulta & "                   -- las dos" & vbCrLf
sConsulta = sConsulta & "                   SET @ORIGEN = 1" & vbCrLf
sConsulta = sConsulta & "            IF @PER IS NULL" & vbCrLf
sConsulta = sConsulta & "               SET @USU = NULL" & vbCrLf
sConsulta = sConsulta & "            ELSE" & vbCrLf
sConsulta = sConsulta & "          SET @USU = (SELECT COD FROM USU WHERE PER = @PER)" & vbCrLf
sConsulta = sConsulta & "   -- la acci�n ser� E - Cambio de estado o I Creaci�n orden de entrega)" & vbCrLf
sConsulta = sConsulta & "            SET @PAG = (SELECT PAG FROM PROVE WHERE COD = @PROVE)" & vbCrLf
sConsulta = sConsulta & "            INSERT INTO LOG_ORDEN_ENTREGA (ACCION,ID_ORDEN_ENTREGA,ID_ORDEN_EST,ID_PEDIDO,ANYO,PROVE,NUM_PEDIDO,NUM_ORDEN,NUMEXT,TIPO,FECHA,IMPORTE,PAG,EST,INCORRECTA,COMENT,ORIGEN,PER,USU,MON,CAMBIO)" & vbCrLf
sConsulta = sConsulta & "                SELECT CASE WHEN @EST = 2  THEN 'I' ELSE 'E' END,@ID_ORDEN,@ID,PEDIDO,ORDEN_ENTREGA.ANYO,PROVE,PEDIDO.NUM,ORDEN_ENTREGA.NUM,NUMEXT,ORDEN_ENTREGA.TIPO,ORDEN_ENTREGA.FECHA,IMPORTE,@PAG,@EST,INCORRECTA,@COMENT,@ORIGEN,@PER,@USU, MON,CAMBIO FROM ORDEN_ENTREGA INNER JOIN PEDIDO ON PEDIDO.ID = ORDEN_ENTREGA.PEDIDO WHERE ORDEN_ENTREGA.ID = @ID_ORDEN" & vbCrLf
sConsulta = sConsulta & "            SET @ID_LOG_ORDEN_ENTREGA = (SELECT MAX(ID) FROM LOG_ORDEN_ENTREGA)" & vbCrLf
sConsulta = sConsulta & "            -- En el caso de Emisi�n de Pedido Directo, todav�a no estar�n creadas las lineas de pedido, se hace en el trigger LINEAS_PEDIDO_TG_INS" & vbCrLf
sConsulta = sConsulta & "            INSERT INTO LOG_LINEAS_PEDIDO (ACCION,ID_LOG_ORDEN_ENTREGA,ID_LINEAS_PEDIDO,ID_ORDEN_EST,ART_INT,UP,PREC_UP,CANT_PED,DEST,ANYO,GMN1,GMN2,GMN3,GMN4,PROCE,ITEM,ENTREGA_OBL,FECENTREGA,FECENTREGAPROVE,EST,CAT1,CAT2,CAT3,CAT4,CAT5,OBS,FC)" & vbCrLf
sConsulta = sConsulta & "                SELECT CASE WHEN @EST = 2  THEN 'I' ELSE 'E'  END,@ID_LOG_ORDEN_ENTREGA,ID,@ID,ART_INT,UP,PREC_UP,CANT_PED,DEST,ANYO,GMN1,GMN2,GMN3,GMN4,PROCE,ITEM,ENTREGA_OBL,FECENTREGA,FECENTREGAPROVE,EST,CAT1,CAT2,CAT3,CAT4,CAT5,OBS,FC FROM LINEAS_PEDIDO WHERE  ORDEN = @ID_ORDEN" & vbCrLf
sConsulta = sConsulta & "         END --IF ACTIVA" & vbCrLf
sConsulta = sConsulta & "     END -- IF EST" & vbCrLf
sConsulta = sConsulta & "  END -- IF EXTERNO" & vbCrLf
sConsulta = sConsulta & "UPDATE ORDEN_ENTREGA SET ORDEN_EST = @ID WHERE ORDEN_ENTREGA.ID = @ID_ORDEN" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_ORDEN_EST_Ins INTO @EST,@ID,@ID_ORDEN,@PER,@COMENT,@EXTERNO" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "Close curTG_ORDEN_EST_Ins" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_ORDEN_EST_Ins" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & ""
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[MON_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[MON_COD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE MON_COD (@OLD VARCHAR(50),@NEW VARCHAR(50))  AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT ON" & vbCrLf
sConsulta = sConsulta & "BEGIN TRANSACTION" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR FOR SELECT COD FROM MON WHERE COD=@OLD" & vbCrLf
sConsulta = sConsulta & "OPEN C" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C INTO @VAR_COD" & vbCrLf
sConsulta = sConsulta & "IF @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE MON NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PAI NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_WEB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PARGEN_DEF NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ORDEN_ENTREGA NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE MON SET COD=@NEW WHERE COD=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PAI SET MON=@NEW WHERE MON=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE SET MON=@NEW WHERE MON=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET MON=@NEW WHERE MON=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE SET MON=@NEW WHERE MON=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE_WEB SET MON=@NEW WHERE MON=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PARGEN_DEF SET MONCEN=@NEW WHERE MONCEN=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE USU SET MON=@NEW WHERE MON=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ORDEN_ENTREGA SET MON=@NEW WHERE MON=@OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE MON CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PAI CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_WEB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PARGEN_DEF CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ORDEN_ENTREGA CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close C" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT OFF" & vbCrLf
sConsulta = sConsulta & "COMMIT TRANSACTION" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion2_11_50_04A2_11_50_05() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim bBool As Boolean

On Error GoTo error
    

sConsulta = "BEGIN TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = True
ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

frmProgreso.lblDetalle = "Modificamos TABLAS"
frmProgreso.lblDetalle.Refresh
frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_11_5_TablasModif_05

frmProgreso.lblDetalle = "Modificamos stored de traspaso de ofertas"
frmProgreso.lblDetalle.Refresh
frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_11_5_Storeds_05

sConsulta = "UPDATE VERSION SET NUM ='2.11.50.05'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "COMMIT TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = False

CodigoDeActualizacion2_11_50_04A2_11_50_05 = True
Exit Function
    
error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_11_50_04A2_11_50_05 = False

End Function
Public Function CodigoDeActualizacion2_11_50_05A2_11_50_06() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim bBool As Boolean

On Error GoTo error
    

sConsulta = "BEGIN TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = True
ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

frmProgreso.lblDetalle = "Modificamos triggers"
frmProgreso.lblDetalle.Refresh
frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_11_5_TriggersModif_06

sConsulta = "UPDATE VERSION SET NUM ='2.11.50.06'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "COMMIT TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = False

CodigoDeActualizacion2_11_50_05A2_11_50_06 = True
Exit Function
    
error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_11_50_05A2_11_50_06 = False

End Function

Public Function CodigoDeActualizacion2_11_50_06A2_11_50_07() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim bBool As Boolean

On Error GoTo error
    

sConsulta = "BEGIN TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = True
ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

frmProgreso.lblDetalle = "Modificamos triggers"
frmProgreso.lblDetalle.Refresh
frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_11_5_Storeds_07

sConsulta = "UPDATE VERSION SET NUM ='2.11.50.07'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "COMMIT TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = False

CodigoDeActualizacion2_11_50_06A2_11_50_07 = True
Exit Function
    
error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_11_50_06A2_11_50_07 = False

End Function

Private Sub V_2_11_5_TablasModif_05()
Dim sConsulta As String

   
sConsulta = "  ALTER TABLE [dbo].[PROVE] ALTER COLUMN  [OBS] [varchar] (4000) NULL "
ExecuteSQL gRDOCon, sConsulta

sConsulta = "  ALTER TABLE [dbo].[LOG_PROVE] ALTER COLUMN  [OBS] [varchar] (4000) NULL "
ExecuteSQL gRDOCon, sConsulta

End Sub

Private Sub V_2_11_5_Storeds_05()
Dim sConsulta As String

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ATR_MODIFICAR]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[ATR_MODIFICAR]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE ATR_MODIFICAR  @TABLA SMALLINT,@ID INTEGER,@DEN VARCHAR(100)=NULL,@TIPO SMALLINT=NULL, @PREFBAJO TINYINT , @INTRO TINYINT, @MINNUM FLOAT=NULL,@MINFEC DATETIME=NULL,@MAXNUM FLOAT=NULL,@MAXFEC DATETIME=NULL,@ATRIB INTEGER=NULL,@INTERNO  TINYINT=NULL,@AMBITO TINYINT=NULL,@OBLIG  TINYINT=NULL,@OP_PREC VARCHAR(2)=NULL,@APLIC_PREC VARCHAR(50)=NULL,@DEF_PREC TINYINT=NULL,@ANYO VARCHAR(50)=NULL,@GMN1 VARCHAR(50)=NULL,@PROCE VARCHAR(50)=NULL,@GRUPO VARCHAR(50)=NULL , @FECACT DATETIME OUTPUT,@ORDEN SMALLINT=NULL OUTPUT,@PLANT VARCHAR(50)=NULL AS " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @POND AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @AMBITOOLD AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @COUNT AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @APLI_PREC_V AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @ORDEN_V AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @ATRIB_PREC AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @USAR_PREC AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRIMERO AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @TIPOOLD AS INTEGER" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @TABLA=0  " & vbCrLf
sConsulta = sConsulta & "     BEGIN --@tabla=0" & vbCrLf
sConsulta = sConsulta & "            SELECT   @POND=POND,@TIPOOLD=TIPO  FROM DEF_ATRIB  WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "            IF @POND = 1 AND @TIPO<> 2 and @TIPO<>3  " & vbCrLf
sConsulta = sConsulta & "                         SET @POND = 0" & vbCrLf
sConsulta = sConsulta & "            ELSE " & vbCrLf
sConsulta = sConsulta & "                          IF @POND = 2 AND (@TIPO=4 OR @INTRO=0)" & vbCrLf
sConsulta = sConsulta & "                                       SET @POND= 0" & vbCrLf
sConsulta = sConsulta & "                          ELSE" & vbCrLf
sConsulta = sConsulta & "                                        IF  @POND = 3 AND @TIPO<> 2 " & vbCrLf
sConsulta = sConsulta & "                                                   SET @POND= 0" & vbCrLf
sConsulta = sConsulta & "                                        ELSE" & vbCrLf
sConsulta = sConsulta & "                                                    IF @POND =5 AND @TIPO<>4" & vbCrLf
sConsulta = sConsulta & "                                                            SET @POND= 0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "            IF @TIPOOLD <> @TIPO " & vbCrLf
sConsulta = sConsulta & "                  IF @TIPOOLD=2" & vbCrLf
sConsulta = sConsulta & "                        BEGIN" & vbCrLf
sConsulta = sConsulta & "                        UPDATE PROCE_ATRIB SET OP_PREC=NULL, APLIC_PREC=NULL, DEF_PREC=NULL WHERE ATRIB= @ID" & vbCrLf
sConsulta = sConsulta & "                        UPDATE PLANTILLA_ATRIB SET OP_PREC=NULL, APLIC_PREC=NULL, DEF_PREC=NULL WHERE ATRIB= @ID" & vbCrLf
sConsulta = sConsulta & "                        END " & vbCrLf
sConsulta = sConsulta & "            UPDATE DEF_ATRIB SET DEN=@DEN,TIPO=@TIPO,PREFBAJO=@PREFBAJO,POND=@POND,INTRO=@INTRO,MAXNUM=CAST(@MAXNUM AS VARCHAR(15)) ,MINNUM=CAST(@MINNUM AS VARCHAR(15)),MAXFEC=CONVERT(VARCHAR(10),@MAXFEC,111),MINFEC=CONVERT(VARCHAR(10),@MINFEC,111) WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "            SELECT @FECACT=FECACT FROM DEF_ATRIB WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "      END --@tabla=0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @TABLA=1" & vbCrLf
sConsulta = sConsulta & "    BEGIN -- @tabla=1" & vbCrLf
sConsulta = sConsulta & "       IF @APLIC_PREC IS not  NULL  AND @ORDEN is  NULL " & vbCrLf
sConsulta = sConsulta & "                 BEGIN --@orden=null" & vbCrLf
sConsulta = sConsulta & "                 SELECT @ATRIB_PREC =ATRIB  FROM PROCE_ATRIB WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "                 SELECT @ORDEN=ORDEN FROM PROCE_ATRIB WHERE  ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND APLIC_PREC = @APLIC_PREC AND" & vbCrLf
sConsulta = sConsulta & "                    OP_PREC=@OP_PREC AND ATRIB =@ATRIB_PREC" & vbCrLf
sConsulta = sConsulta & "                      " & vbCrLf
sConsulta = sConsulta & "                 SET @APLI_PREC_V=@APLIC_PREC" & vbCrLf
sConsulta = sConsulta & "                 SET @ORDEN_V= 0" & vbCrLf
sConsulta = sConsulta & "                 SET @PRIMERO=0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                 WHILE @ORDEN_V=0 and @APLI_PREC_V<=3" & vbCrLf
sConsulta = sConsulta & "                         BEGIN -- while" & vbCrLf
sConsulta = sConsulta & "                             SELECT  @ORDEN_V = MAX(ORDEN) FROM PROCE_ATRIB WHERE  ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND APLIC_PREC = @APLI_PREC_V" & vbCrLf
sConsulta = sConsulta & "                             IF @ORDEN_V IS NULL" & vbCrLf
sConsulta = sConsulta & "                               begin --@ORDEN_V =null" & vbCrLf
sConsulta = sConsulta & "                                 IF @APLIC_PREC=3" & vbCrLf
sConsulta = sConsulta & "                                     begin" & vbCrLf
sConsulta = sConsulta & "                                       SET @ORDEN_V=1 " & vbCrLf
sConsulta = sConsulta & "                                       SET @PRIMERO=1" & vbCrLf
sConsulta = sConsulta & "                                     end" & vbCrLf
sConsulta = sConsulta & "                                 ELSE                   " & vbCrLf
sConsulta = sConsulta & "                                   begin" & vbCrLf
sConsulta = sConsulta & "                                      SET @ORDEN_V= 0" & vbCrLf
sConsulta = sConsulta & "                                      SET @APLI_PREC_V=@APLI_PREC_V + 1" & vbCrLf
sConsulta = sConsulta & "                                   end" & vbCrLf
sConsulta = sConsulta & "                               end  --@ORDEN_V =null" & vbCrLf
sConsulta = sConsulta & "                         END -- while" & vbCrLf
sConsulta = sConsulta & "                if @ORDEN_V IS NULL  SET @ORDEN_V=0" & vbCrLf
sConsulta = sConsulta & "                      if @PRIMERO=1" & vbCrLf
sConsulta = sConsulta & "                              UPDATE PROCE_ATRIB SET ORDEN=1 WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "                     else" & vbCrLf
sConsulta = sConsulta & "                              UPDATE PROCE_ATRIB SET ORDEN=@ORDEN_V+1 WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "                UPDATE PROCE_ATRIB SET ORDEN = ORDEN +1 WHERE  APLIC_PREC < @APLIC_PREC AND ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ID<>@ID" & vbCrLf
sConsulta = sConsulta & "               SET @ORDEN=@ORDEN_V +1" & vbCrLf
sConsulta = sConsulta & "              " & vbCrLf
sConsulta = sConsulta & "             END --@orden=null" & vbCrLf
sConsulta = sConsulta & "       SELECT @POND=POND,@AMBITOOLD=AMBITO,@USAR_PREC=USAR_PREC FROM PROCE_ATRIB WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "       IF @INTRO=0 AND @POND=2 SET @POND=0" & vbCrLf
sConsulta = sConsulta & "       IF @DEF_PREC=1 SET @USAR_PREC=1" & vbCrLf
sConsulta = sConsulta & "       UPDATE PROCE_ATRIB SET PREFBAJO=@PREFBAJO,INTRO=@INTRO,MAXNUM=@MAXNUM  ," & vbCrLf
sConsulta = sConsulta & "                  MINNUM=@MINNUM,MAXFEC=CONVERT(VARCHAR(10),@MAXFEC,111)," & vbCrLf
sConsulta = sConsulta & "                  MINFEC=CONVERT(VARCHAR(10),@MINFEC,111),INTERNO=@INTERNO,AMBITO=@AMBITO,OBLIG=@OBLIG," & vbCrLf
sConsulta = sConsulta & "                  OP_PREC=@OP_PREC,APLIC_PREC=@APLIC_PREC,USAR_PREC=@USAR_PREC," & vbCrLf
sConsulta = sConsulta & "                  DEF_PREC=@DEF_PREC,POND=@POND WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "       SELECT @FECACT=FECACT FROM PROCE_ATRIB WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "       IF   @ATRIB IS NOT  NULL" & vbCrLf
sConsulta = sConsulta & "             BEGIN --@atrib not null" & vbCrLf
sConsulta = sConsulta & "                    SELECT @POND=POND FROM DEF_ATRIB WHERE ID=@ATRIB" & vbCrLf
sConsulta = sConsulta & "                    IF @INTRO=0 AND @POND=2 SET @POND=0" & vbCrLf
sConsulta = sConsulta & "                       UPDATE DEF_ATRIB SET PREFBAJO=@PREFBAJO,INTRO=@INTRO,MAXNUM=@MAXNUM ," & vbCrLf
sConsulta = sConsulta & "                           MINNUM=@MINNUM,MAXFEC=CONVERT(VARCHAR(10),@MAXFEC,111)," & vbCrLf
sConsulta = sConsulta & "                           MINFEC=CONVERT(VARCHAR(10),@MINFEC,111),POND=@POND WHERE ID=@ATRIB" & vbCrLf
sConsulta = sConsulta & "             END--@atrib not null" & vbCrLf
sConsulta = sConsulta & "--           END estaba mal" & vbCrLf
sConsulta = sConsulta & "       IF @AMBITO<>@AMBITOOLD" & vbCrLf
sConsulta = sConsulta & "               BEGIN --@ambito<>ambitoold" & vbCrLf
sConsulta = sConsulta & "                     SELECT @COUNT=COUNT(*) FROM PROCE_OFE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "                     IF @COUNT > 0" & vbCrLf
sConsulta = sConsulta & "                          IF  @AMBITOOLD>@AMBITO" & vbCrLf
sConsulta = sConsulta & "                               IF @AMBITOOLD=3" & vbCrLf
sConsulta = sConsulta & "                                     begin --old=3" & vbCrLf
sConsulta = sConsulta & "                                           DELETE OFE_ITEM_ATRIB WHERE  ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ATRIB_ID=@ID" & vbCrLf
sConsulta = sConsulta & "                                           DELETE CONF_VISTAS_GRUPO_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ATRIB=@ID" & vbCrLf
sConsulta = sConsulta & "                                           DELETE CONF_VISTAS_ALL_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE  AND ATRIB=@ID" & vbCrLf
sConsulta = sConsulta & "                                           DELETE CONF_VISTAS_ITEM_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE  AND ATRIB=@ID" & vbCrLf
sConsulta = sConsulta & "                                           INSERT INTO CONF_VISTAS_PROCE_ATRIB (ANYO,GMN1,PROCE,USU,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                              SELECT @ANYO,@GMN1,@PROCE,USU,VISTA,@ID FROM  CONF_VISTAS_PROCE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "                                     end  --old=3" & vbCrLf
sConsulta = sConsulta & "                               ELSE --ser�a old=2" & vbCrLf
sConsulta = sConsulta & "                                           DELETE OFE_GR_ATRIB WHERE  ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ATRIB_ID=@ID AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                          ELSE  --old < ambito" & vbCrLf
sConsulta = sConsulta & "                                 IF @AMBITOOLD=1" & vbCrLf
sConsulta = sConsulta & "                                     BEGIN --old=1" & vbCrLf
sConsulta = sConsulta & "                                        IF @AMBITO=2" & vbCrLf
sConsulta = sConsulta & "                                                INSERT INTO OFE_GR_ATRIB (ANYO, GMN1, PROCE,GRUPO,PROVE,OFE,ATRIB_ID,VALOR_NUM,VALOR_TEXT," & vbCrLf
sConsulta = sConsulta & "                                                      VALOR_FEC,VALOR_BOOL,VALOR_POND) " & vbCrLf
sConsulta = sConsulta & "                                                      SELECT DISTINCT OFE_ATRIB.ANYO, OFE_ATRIB.GMN1, OFE_ATRIB.PROCE,PROCE_ATRIB.GRUPO" & vbCrLf
sConsulta = sConsulta & "                                                                   ,OFE_ATRIB.PROVE,OFE_ATRIB.OFE,OFE_ATRIB.ATRIB_ID,OFE_ATRIB.VALOR_NUM,OFE_ATRIB.VALOR_TEXT" & vbCrLf
sConsulta = sConsulta & "                                                                   ,OFE_ATRIB.VALOR_FEC,OFE_ATRIB.VALOR_BOOL,OFE_ATRIB.VALOR_POND " & vbCrLf
sConsulta = sConsulta & "                                                     FROM OFE_ATRIB " & vbCrLf
sConsulta = sConsulta & "                                                     INNER JOIN PROCE_ATRIB ON  OFE_ATRIB.ANYO=PROCE_ATRIB.ANYO" & vbCrLf
sConsulta = sConsulta & "                                                                 AND OFE_ATRIB.GMN1=PROCE_ATRIB.GMN1 AND OFE_ATRIB.PROCE =PROCE_ATRIB.PROCE " & vbCrLf
sConsulta = sConsulta & "                                                                 AND PROCE_ATRIB.GRUPO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                                                      WHERE OFE_ATRIB.ATRIB_ID=@ID AND OFE_ATRIB.ANYO=@ANYO AND OFE_ATRIB.GMN1=@GMN1 AND OFE_ATRIB.PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "                                        DELETE OFE_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ATRIB_ID=@ID" & vbCrLf
sConsulta = sConsulta & "                                        IF @AMBITO = 3" & vbCrLf
sConsulta = sConsulta & "                                                  begin --ambito=3" & vbCrLf
sConsulta = sConsulta & "                                                  DELETE CONF_VISTAS_PROCE_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ATRIB=@ID" & vbCrLf
sConsulta = sConsulta & "                                                  IF @GRUPO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                                                        INSERT INTO CONF_VISTAS_GRUPO_ATRIB (ANYO,GMN1,PROCE,GRUPO,USU,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                                          SELECT @ANYO,@GMN1,@PROCE,@GRUPO,USU,VISTA,@ID FROM  CONF_VISTAS_GRUPO WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO " & vbCrLf
sConsulta = sConsulta & "                                                  ELSE" & vbCrLf
sConsulta = sConsulta & "                                                        INSERT INTO CONF_VISTAS_GRUPO_ATRIB (ANYO,GMN1,PROCE,GRUPO,USU,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                                          SELECT @ANYO,@GMN1,@PROCE,GRUPO,USU,VISTA,@ID FROM  CONF_VISTAS_GRUPO WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE         " & vbCrLf
sConsulta = sConsulta & "                                                 INSERT INTO CONF_VISTAS_ALL_ATRIB (ANYO,GMN1,PROCE,USU,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                                     SELECT @ANYO,@GMN1,@PROCE,USU,VISTA,@ID FROM  CONF_VISTAS_ALL WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "                                                 IF @GRUPO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                                                     INSERT INTO CONF_VISTAS_ITEM_ATRIB (ANYO,GMN1,PROCE,GRUPO,USU,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                                         SELECT @ANYO,@GMN1,@PROCE,@GRUPO,USU,VISTA,@ID FROM  CONF_VISTAS_ITEM WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO " & vbCrLf
sConsulta = sConsulta & "                                                  ELSE" & vbCrLf
sConsulta = sConsulta & "                                                     INSERT INTO CONF_VISTAS_ITEM_ATRIB (ANYO,GMN1,PROCE,GRUPO,USU,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                                         SELECT @ANYO,@GMN1,@PROCE,GRUPO,USU,VISTA,@ID FROM  CONF_VISTAS_ITEM WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                                                 end--ambito=3" & vbCrLf
sConsulta = sConsulta & "                                     END --old=1" & vbCrLf
sConsulta = sConsulta & "                                ELSE " & vbCrLf
sConsulta = sConsulta & "                                    BEGIN  --old es 2" & vbCrLf
sConsulta = sConsulta & "                                      DELETE OFE_GR_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ATRIB_ID=@ID AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "                                      DELETE CONF_VISTAS_PROCE_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ATRIB=@ID" & vbCrLf
sConsulta = sConsulta & "                                      IF @GRUPO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                                                 INSERT INTO CONF_VISTAS_GRUPO_ATRIB (ANYO,GMN1,PROCE,GRUPO,USU,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                                       SELECT @ANYO,@GMN1,@PROCE,@GRUPO,USU,VISTA,@ID FROM  CONF_VISTAS_GRUPO WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO " & vbCrLf
sConsulta = sConsulta & "                                      ELSE" & vbCrLf
sConsulta = sConsulta & "                                                INSERT INTO CONF_VISTAS_GRUPO_ATRIB (ANYO,GMN1,PROCE,GRUPO,USU,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                                       SELECT @ANYO,@GMN1,@PROCE,GRUPO,USU,VISTA,@ID FROM  CONF_VISTAS_GRUPO WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "                                      INSERT INTO CONF_VISTAS_ALL_ATRIB (ANYO,GMN1,PROCE,USU,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                           SELECT @ANYO,@GMN1,@PROCE,USU,VISTA,@ID FROM  CONF_VISTAS_ALL WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "                                      IF @GRUPO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                                                 INSERT INTO CONF_VISTAS_ITEM_ATRIB (ANYO,GMN1,PROCE,GRUPO,USU,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                                      SELECT @ANYO,@GMN1,@PROCE,@GRUPO,USU,VISTA,@ID FROM  CONF_VISTAS_ITEM WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO " & vbCrLf
sConsulta = sConsulta & "                                      ELSE" & vbCrLf
sConsulta = sConsulta & "                                                 INSERT INTO CONF_VISTAS_ITEM_ATRIB (ANYO,GMN1,PROCE,GRUPO,USU,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                                      SELECT @ANYO,@GMN1,@PROCE,GRUPO,USU,VISTA,@ID FROM  CONF_VISTAS_ITEM WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                                    END--old es 2" & vbCrLf
sConsulta = sConsulta & "               END --@ambito<>ambitoold" & vbCrLf
sConsulta = sConsulta & "         END -- @tabla=1" & vbCrLf
sConsulta = sConsulta & "IF @TABLA=2" & vbCrLf
sConsulta = sConsulta & "          BEGIN" & vbCrLf
sConsulta = sConsulta & "          IF @APLIC_PREC IS not  NULL  AND @ORDEN is  NULL " & vbCrLf
sConsulta = sConsulta & "                 BEGIN" & vbCrLf
sConsulta = sConsulta & "                     SELECT @ATRIB_PREC =ATRIB  FROM PLANTILLA_ATRIB WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "                     SELECT @ORDEN=ORDEN FROM PLANTILLA_ATRIB WHERE PLANT=@PLANT AND APLIC_PREC = @APLIC_PREC AND OP_PREC=@OP_PREC AND ATRIB =@ATRIB_PREC" & vbCrLf
sConsulta = sConsulta & "                     IF @ORDEN IS NULL" & vbCrLf
sConsulta = sConsulta & "                         BEGIN" & vbCrLf
sConsulta = sConsulta & "                           SET @APLI_PREC_V=@APLIC_PREC" & vbCrLf
sConsulta = sConsulta & "                           SET @ORDEN_V= 0" & vbCrLf
sConsulta = sConsulta & "                           WHILE @APLI_PREC_V < 3  AND @ORDEN_V= 0" & vbCrLf
sConsulta = sConsulta & "                                 BEGIN" & vbCrLf
sConsulta = sConsulta & "                                     SELECT  @ORDEN_V = MAX(ORDEN) FROM PLANTILLA_ATRIB WHERE  PLANT=@PLANT AND APLIC_PREC = @APLI_PREC_V" & vbCrLf
sConsulta = sConsulta & "                                      IF @ORDEN_V IS NULL" & vbCrLf
sConsulta = sConsulta & "                                         begin" & vbCrLf
sConsulta = sConsulta & "                                              SET @ORDEN_V= 0" & vbCrLf
sConsulta = sConsulta & "                                              SET @APLI_PREC_V=@APLI_PREC_V + 1" & vbCrLf
sConsulta = sConsulta & "                                         end" & vbCrLf
sConsulta = sConsulta & "                                END" & vbCrLf
sConsulta = sConsulta & "                          UPDATE PLANTILLA_ATRIB SET ORDEN = ORDEN +1 WHERE  APLIC_PREC < @APLIC_PREC AND PLANT=@PLANT" & vbCrLf
sConsulta = sConsulta & "                          SET @ORDEN=@ORDEN_V +1" & vbCrLf
sConsulta = sConsulta & "                        END" & vbCrLf
sConsulta = sConsulta & "                   else" & vbCrLf
sConsulta = sConsulta & "                      IF @APLIC_PREC IS not  NULL   " & vbCrLf
sConsulta = sConsulta & "                            UPDATE PLANTILLA_ATRIB SET ORDEN = ORDEN +1 WHERE  APLIC_PREC < @APLIC_PREC AND PLANT=@PLANT" & vbCrLf
sConsulta = sConsulta & "                 END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "           SELECT @POND=POND FROM PLANTILLA_ATRIB WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "           IF @INTRO=0 AND @POND=2 SET @POND=0" & vbCrLf
sConsulta = sConsulta & "               UPDATE PLANTILLA_ATRIB SET PREFBAJO=@PREFBAJO,INTRO=@INTRO,MAXNUM=@MAXNUM  ," & vbCrLf
sConsulta = sConsulta & "                    MINNUM=@MINNUM,MAXFEC=CONVERT(VARCHAR(10),@MAXFEC,111)," & vbCrLf
sConsulta = sConsulta & "                    MINFEC=CONVERT(VARCHAR(10),@MINFEC,111),INTERNO=@INTERNO,AMBITO=@AMBITO,OBLIG=@OBLIG," & vbCrLf
sConsulta = sConsulta & "                    OP_PREC=@OP_PREC,APLIC_PREC=@APLIC_PREC," & vbCrLf
sConsulta = sConsulta & "                    DEF_PREC=@DEF_PREC,POND=@POND,ORDEN=@ORDEN WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "          SELECT @FECACT=FECACT FROM PLANTILLA_ATRIB WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "          IF   @ATRIB IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                BEGIN" & vbCrLf
sConsulta = sConsulta & "                    SELECT @POND=POND FROM DEF_ATRIB WHERE ID=@ATRIB" & vbCrLf
sConsulta = sConsulta & "                    IF @INTRO=0 AND @POND=2 SET @POND=0" & vbCrLf
sConsulta = sConsulta & "                        UPDATE DEF_ATRIB SET PREFBAJO=@PREFBAJO,INTRO=@INTRO,MAXNUM=CAST(@MAXNUM AS VARCHAR(15)) ," & vbCrLf
sConsulta = sConsulta & "                            MINNUM=CAST(@MINNUM AS VARCHAR(15)),MAXFEC=CONVERT(VARCHAR(10),@MAXFEC,111)," & vbCrLf
sConsulta = sConsulta & "                            MINFEC=CONVERT(VARCHAR(10),@MINFEC,111),POND=@POND WHERE ID=@ATRIB" & vbCrLf
sConsulta = sConsulta & "                END" & vbCrLf
sConsulta = sConsulta & "          END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ATR_INSERTAR_LISTA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[ATR_INSERTAR_LISTA]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE ATR_INSERTAR_LISTA  @TIPO SMALLINT,@ATRIB INTEGER,@ORDEN SMALLINT,@VALORPOND FLOAT,@ENDEFINICION INTEGER,@VALORTEXT VARCHAR(100)=NULL, @VALORNUM FLOAT=NULL, @VALORFEC DATETIME=NULL AS " & vbCrLf
sConsulta = sConsulta & "DECLARE @INTRO AS SMALLINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ATRIBDEF AS INTEGER" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @TIPO = 1" & vbCrLf
sConsulta = sConsulta & "  BEGIN " & vbCrLf
sConsulta = sConsulta & "      SELECT @ATRIBDEF=ATRIB FROM PROCE_ATRIB WHERE ID= @ATRIB" & vbCrLf
sConsulta = sConsulta & "      INSERT INTO PROCE_ATRIB_LISTA (ATRIB_ID, ORDEN,VALOR_NUM,VALOR_TEXT,VALOR_FEC,VALOR_POND) " & vbCrLf
sConsulta = sConsulta & "      VALUES (@ATRIB, @ORDEN, cast(@VALORNUM AS VARCHAR(15)), @VALORTEXT, CONVERT( VARCHAR(10),@VALORFEC ,111),cast(@VALORPOND AS VARCHAR(15)) )" & vbCrLf
sConsulta = sConsulta & "  END    " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @TIPO = 2" & vbCrLf
sConsulta = sConsulta & "   BEGIN " & vbCrLf
sConsulta = sConsulta & "      SELECT @ATRIBDEF=ATRIB FROM PLANTILLA_ATRIB WHERE ID= @ATRIB" & vbCrLf
sConsulta = sConsulta & "      INSERT INTO PLANTILLA_ATRIB_LISTA (ATRIB_ID, ORDEN,VALOR_NUM,VALOR_TEXT,VALOR_FEC,VALOR_POND) " & vbCrLf
sConsulta = sConsulta & "      VALUES  (@ATRIB, @ORDEN, cast(@VALORNUM AS VARCHAR(15)), @VALORTEXT, CONVERT( VARCHAR(10),@VALORFEC ,111),cast(@VALORPOND AS VARCHAR(15)))" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @TIPO = 0 " & vbCrLf
sConsulta = sConsulta & "       INSERT INTO  DEF_ATRIB_LISTA ( ATRIB, ORDEN, VALOR_NUM, VALOR_TEXT, VALOR_FEC,VALOR_POND) " & vbCrLf
sConsulta = sConsulta & "           VALUES (@ATRIB, @ORDEN, cast(@VALORNUM AS VARCHAR(15)), @VALORTEXT, CONVERT( VARCHAR(10),@VALORFEC ,111),cast(@VALORPOND AS VARCHAR(15)) )" & vbCrLf
sConsulta = sConsulta & "IF @TIPO <> 0 AND @ENDEFINICION=1" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       SELECT @INTRO=INTRO FROM DEF_ATRIB WHERE ID=@ATRIBDEF" & vbCrLf
sConsulta = sConsulta & "       IF @INTRO=1 " & vbCrLf
sConsulta = sConsulta & "           INSERT INTO  DEF_ATRIB_LISTA ( ATRIB, ORDEN, VALOR_NUM, VALOR_TEXT, VALOR_FEC,VALOR_POND) " & vbCrLf
sConsulta = sConsulta & "               VALUES (@ATRIBDEF, @ORDEN, cast(@VALORNUM AS VARCHAR(15)), @VALORTEXT, CONVERT( VARCHAR(10),@VALORFEC ,111),cast(@VALORPOND AS VARCHAR(15)) )" & vbCrLf
sConsulta = sConsulta & "   END    " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Private Sub V_2_11_5_TriggersModif_06()
Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PROVE_GMN4_DEL]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PROVE_GMN4_DEL]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE TRIGGER PROVE_GMN4_DEL ON dbo.PROVE_GMN4" & vbCrLf
sConsulta = sConsulta & "FOR DELETE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN1 VARCHAR(50),@GMN2 VARCHAR(50),@GMN3 VARCHAR(50),@GMN4 VARCHAR(50),@PROVE VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_PROVE_GMN4_Del CURSOR FOR SELECT GMN1,GMN2,GMN3,GMN4,PROVE FROM DELETED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_PROVE_GMN4_Del" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROVE_GMN4_Del INTO @GMN1,@GMN2,@GMN3,@GMN4,@PROVE" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DELETE FROM PROVE_ART4_ATRIB WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@GMN4 AND PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "DELETE FROM PROVE_ART4 WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@GMN4 AND PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROVE_GMN4_Del INTO @GMN1,@GMN2,@GMN3,@GMN4,@PROVE" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_PROVE_GMN4_Del" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_PROVE_GMN4_Del" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Private Sub V_2_11_5_Storeds_07()
Dim sConsulta As String

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_SOLICITUD_DATOS_MENSAJE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_SOLICITUD_DATOS_MENSAJE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_SOLICITUD_DATOS_MENSAJE " & vbCrLf
sConsulta = sConsulta & "@ID INT," & vbCrLf
sConsulta = sConsulta & "@TYPE TINYINT, " & vbCrLf
sConsulta = sConsulta & "@DESCR_BREVE VarChar(50) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@DESCR_DET VarChar( 500) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@FECHA_ALTA DateTIME OUTPUT," & vbCrLf
sConsulta = sConsulta & "@FECHA_NECESIDAD DateTIME OUTPUT," & vbCrLf
sConsulta = sConsulta & "@IMPORTE FLOAT OUTPUT," & vbCrLf
sConsulta = sConsulta & "@MON_COD VarChar (50) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@MON_DEN VarChar(100) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@APROBADOR VarChar(150) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@APROBADOR_COD VarChar( 50) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@APROBADOR_NOM VarChar(50) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@APROBADOR_APE VarChar(100) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@APROBADOR_TFNO VarChar(20) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@APROBADOR_EMAIL VarChar(100) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@APROBADOR_FAX VarChar(20) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@APROBADOR_UON varChar(250) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@APROBADOR_DEP_COD VarChar(50) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@APROBADOR_DEP_DEN varChar(100) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@COMPRADOR_COD VarChar( 50) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@COMPRADOR_NOM VarChar(50) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@COMPRADOR_APE VarChar(100) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@COMPRADOR_TFNO VarChar(20) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@COMPRADOR_EMAIL VarChar(100) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@COMPRADOR_FAX VarChar(20) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@COMPRADOR_UON varChar(250) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@COMPRADOR_DEP_COD VarChar(50) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@COMPRADOR_DEP_DEN varChar(100) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@PET_COD VarChar(50) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@PET_NOM VarChar(50) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@PET_APE VarChar(100) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@PET_TFNO varchar(20) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@PET_EMAIL VarChar(100) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@PET_FAX VarChar(20) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@PET_UON VarChar(250) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@PET_DEP_COD VarChar(50) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@PET_DEP_DEN VarChar(100) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@CARGO VarChar(100) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@PROVEEDORES varChar(300) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@DEST_COD VarChar(50) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@DEST_DEN VarChar(100) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@DEST_DIR VarChar(255) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@DEST_POB VarChar(100) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@DEST_CP VarChar(20) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@DEST_PROVI_COD VarChar(50) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@DEST_PROVI_DEN VarChar(100) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@DEST_PAI_COD VarChar(50) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@DEST_PAI_DEN VarChar(100) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@COMENTARIO varChar(500) OUTPUT" & vbCrLf
sConsulta = sConsulta & "AS   " & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "/* " & vbCrLf
sConsulta = sConsulta & "   Par�metros de entrada:  ID  : Identificador de la solicitud" & vbCrLf
sConsulta = sConsulta & "               TYPE    :   " & vbCrLf
sConsulta = sConsulta & "                       0 -> Asignaci�n a comprador" & vbCrLf
sConsulta = sConsulta & "                       1 -> Aprobaci�n a peticionario" & vbCrLf
sConsulta = sConsulta & "                       2 -> Rechazo" & vbCrLf
sConsulta = sConsulta & "                       3 -> Anulaci�n          " & vbCrLf
sConsulta = sConsulta & "                       4 -> Cierre" & vbCrLf
sConsulta = sConsulta & "                       5 -> Reapertura" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "*/" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @TYPE=0" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "   SELECT " & vbCrLf
sConsulta = sConsulta & "   @DESCR_BREVE=S.DEN," & vbCrLf
sConsulta = sConsulta & "   @DESCR_DET =S.DESCR," & vbCrLf
sConsulta = sConsulta & "   @FECHA_ALTA = S.FECALTA," & vbCrLf
sConsulta = sConsulta & "   @FECHA_NECESIDAD=S.FECNEC," & vbCrLf
sConsulta = sConsulta & "   @IMPORTE =S.IMPORTE," & vbCrLf
sConsulta = sConsulta & "   @MON_COD =S.MON," & vbCrLf
sConsulta = sConsulta & "   @MON_DEN =M.DEN," & vbCrLf
sConsulta = sConsulta & "   @APROBADOR = " & vbCrLf
sConsulta = sConsulta & "   case when SEST.PER IS NULL THEN" & vbCrLf
sConsulta = sConsulta & "       (select usu from adm)       " & vbCrLf
sConsulta = sConsulta & "       ELSE" & vbCrLf
sConsulta = sConsulta & "       P2.NOM + ' ' + P2.APE" & vbCrLf
sConsulta = sConsulta & "   end," & vbCrLf
sConsulta = sConsulta & "   @APROBADOR_COD = " & vbCrLf
sConsulta = sConsulta & "   case when SEST.PER IS NULL THEN" & vbCrLf
sConsulta = sConsulta & "       (select usu from adm)       " & vbCrLf
sConsulta = sConsulta & "       ELSE" & vbCrLf
sConsulta = sConsulta & "       SEST.PER            " & vbCrLf
sConsulta = sConsulta & "       END," & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   @APROBADOR_NOM = P2.NOM," & vbCrLf
sConsulta = sConsulta & "   @APROBADOR_APE = P2.APE," & vbCrLf
sConsulta = sConsulta & "   @APROBADOR_TFNO =P2.TFNO," & vbCrLf
sConsulta = sConsulta & "   @APROBADOR_EMAIL = P2.EMAIL," & vbCrLf
sConsulta = sConsulta & "   @APROBADOR_FAX =P2.FAX," & vbCrLf
sConsulta = sConsulta & "   @APROBADOR_UON = " & vbCrLf
sConsulta = sConsulta & "   case when U23.cod is null then" & vbCrLf
sConsulta = sConsulta & "       case when U22.cod is null then" & vbCrLf
sConsulta = sConsulta & "                       CASE WHEN U21.COD IS NULL THEN" & vbCrLf
sConsulta = sConsulta & "                ' '" & vbCrLf
sConsulta = sConsulta & "                               ELSE" & vbCrLf
sConsulta = sConsulta & "                                     U21.cod + ' - ' + U21.den" & vbCrLf
sConsulta = sConsulta & "                                END" & vbCrLf
sConsulta = sConsulta & "                  ELSE" & vbCrLf
sConsulta = sConsulta & "           U21.COD + ' - ' + U22.COD + ' - ' + U22.DEN                        " & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "       U21.COD + ' - ' + U22.COD + ' - ' + U23.COD + ' - '  + U23.DEN" & vbCrLf
sConsulta = sConsulta & "   END," & vbCrLf
sConsulta = sConsulta & "   @APROBADOR_DEP_COD=P2.DEP," & vbCrLf
sConsulta = sConsulta & "   @APROBADOR_DEP_DEN = D2.DEN," & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   @COMPRADOR_COD = S.COMPRADOR," & vbCrLf
sConsulta = sConsulta & "   @COMPRADOR_NOM = P.NOM," & vbCrLf
sConsulta = sConsulta & "   @COMPRADOR_APE= P.APE," & vbCrLf
sConsulta = sConsulta & "   @COMPRADOR_TFNO =P.TFNO," & vbCrLf
sConsulta = sConsulta & "   @COMPRADOR_EMAIL= P.EMAIL," & vbCrLf
sConsulta = sConsulta & "   @COMPRADOR_FAX =P.FAX," & vbCrLf
sConsulta = sConsulta & "   @COMPRADOR_DEP_COD=P.DEP," & vbCrLf
sConsulta = sConsulta & "   @COMPRADOR_DEP_DEN=D.DEN," & vbCrLf
sConsulta = sConsulta & "   @COMPRADOR_UON= " & vbCrLf
sConsulta = sConsulta & "   case when U3.cod is null then" & vbCrLf
sConsulta = sConsulta & "       case when U2.cod is null then" & vbCrLf
sConsulta = sConsulta & "                       CASE WHEN U1.COD IS NULL THEN" & vbCrLf
sConsulta = sConsulta & "                ' '" & vbCrLf
sConsulta = sConsulta & "                               ELSE" & vbCrLf
sConsulta = sConsulta & "                                     U1.cod + ' - ' + U1.den" & vbCrLf
sConsulta = sConsulta & "                                END" & vbCrLf
sConsulta = sConsulta & "                  ELSE" & vbCrLf
sConsulta = sConsulta & "           U1.COD + ' - ' + U2.COD + ' - ' + U2.DEN                        " & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "       U1.COD + ' - ' + U2.COD + ' - ' + U3.COD + ' - '  + U3.DEN" & vbCrLf
sConsulta = sConsulta & "   END," & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   @PET_COD = S.PER," & vbCrLf
sConsulta = sConsulta & "   @PET_NOM = PET.NOM," & vbCrLf
sConsulta = sConsulta & "   @PET_APE = PET.APE," & vbCrLf
sConsulta = sConsulta & "   @PET_TFNO =PET.TFNO," & vbCrLf
sConsulta = sConsulta & "   @PET_EMAIL = PET.EMAIL," & vbCrLf
sConsulta = sConsulta & "   @PET_FAX = PET.FAX," & vbCrLf
sConsulta = sConsulta & "   @PET_UON = " & vbCrLf
sConsulta = sConsulta & "   case when U_PET3.cod is null then" & vbCrLf
sConsulta = sConsulta & "       case when U_PET2.cod is null then" & vbCrLf
sConsulta = sConsulta & "                       CASE WHEN U_PET1.COD IS NULL THEN" & vbCrLf
sConsulta = sConsulta & "                ' '" & vbCrLf
sConsulta = sConsulta & "                               ELSE" & vbCrLf
sConsulta = sConsulta & "                                     U_PET1.cod + ' - ' + U_PET1.den" & vbCrLf
sConsulta = sConsulta & "                                END" & vbCrLf
sConsulta = sConsulta & "                  ELSE" & vbCrLf
sConsulta = sConsulta & "           U_PET1.COD + ' - ' + U_PET2.COD + ' - ' + U_PET2.DEN                        " & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "       U_PET1.COD + ' - ' + U_PET2.COD + ' - ' + U_PET3.COD + ' - '  + U_PET3.DEN" & vbCrLf
sConsulta = sConsulta & "   END," & vbCrLf
sConsulta = sConsulta & "   @PET_DEP_COD= PET.DEP," & vbCrLf
sConsulta = sConsulta & "   @PET_DEP_DEN = PET_D.DEN," & vbCrLf
sConsulta = sConsulta & "   @CARGO = S.CARGO," & vbCrLf
sConsulta = sConsulta & "   @PROVEEDORES = S.PROVEEDORES," & vbCrLf
sConsulta = sConsulta & "   @DEST_COD= S.DEST," & vbCrLf
sConsulta = sConsulta & "   @DEST_DEN = DEST.DEN," & vbCrLf
sConsulta = sConsulta & "   @DEST_DIR =DEST.DIR," & vbCrLf
sConsulta = sConsulta & "   @DEST_POB =DEST.POB," & vbCrLf
sConsulta = sConsulta & "   @DEST_CP = DEST.CP," & vbCrLf
sConsulta = sConsulta & "   @DEST_PROVI_COD = DEST.PROVI," & vbCrLf
sConsulta = sConsulta & "   @DEST_PROVI_DEN = PROVI.DEN," & vbCrLf
sConsulta = sConsulta & "   @DEST_PAI_COD = DEST.PAI," & vbCrLf
sConsulta = sConsulta & "   @DEST_PAI_DEN = PAI.DEN," & vbCrLf
sConsulta = sConsulta & "   @COMENTARIO = SEST.COMENT" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   FROM SOLICIT S" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN SOLICIT_ASIG_COMP SEST ON S.ID=SEST.SOLICIT AND S.ID=@ID " & vbCrLf
sConsulta = sConsulta & "   AND SEST.ID IN (SELECT MAX (ID) FROM SOLICIT_ASIG_COMP WHERE SOLICIT=@ID) " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN MON M ON S.MON=M.COD" & vbCrLf
sConsulta = sConsulta & "   AND S.ID=@ID" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PER P ON S.COMPRADOR=P.COD" & vbCrLf
sConsulta = sConsulta & "   AND S.ID=@ID" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN DEP D ON P.DEP=D.COD" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN UON1 U1 ON U1.COD=P.UON1" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN UON2 U2 ON U2.UON1=P.UON1 AND P.UON2=U2.COD" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN UON3 U3 ON U3.UON1=P.UON1 AND P.UON2=U3.UON2 AND P.UON3=U3.COD" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN PER P2 ON SEST.PER=P2.COD" & vbCrLf
sConsulta = sConsulta & "   AND S.ID=@ID" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN DEP D2 ON P2.DEP=D2.COD" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN UON1 U21 ON U21.COD=P2.UON1" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN UON2 U22 ON U22.UON1=P2.UON1 AND P2.UON2=U22.COD" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN UON3 U23 ON U23.UON1=P2.UON1 AND P2.UON2=U23.UON2 AND P2.UON3=U23.COD" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PER PET ON S.PER=PET.COD AND S.ID=@ID" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN DEP PET_D ON P.DEP=PET_D.COD" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN UON1 U_PET1 ON U_PET1.COD=PET.UON1" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN UON2 U_PET2 ON U_PET2.UON1=PET.UON1 AND PET.UON2=U_PET2.COD" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN UON3 U_PET3 ON U_PET3.UON1=PET.UON1 AND PET.UON2=U_PET3.UON2 AND PET.UON3=U_PET3.COD" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN DEST ON S.DEST=DEST.COD" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN PAI ON DEST.PAI=PAI.COD" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN PROVI ON PROVI.PAI=PAI.COD AND PROVI.COD=DEST.PROVI" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT " & vbCrLf
sConsulta = sConsulta & "@DESCR_BREVE=S.DEN," & vbCrLf
sConsulta = sConsulta & "@DESCR_DET =S.DESCR," & vbCrLf
sConsulta = sConsulta & "@FECHA_ALTA = S.FECALTA," & vbCrLf
sConsulta = sConsulta & "@FECHA_NECESIDAD=S.FECNEC," & vbCrLf
sConsulta = sConsulta & "@IMPORTE =S.IMPORTE," & vbCrLf
sConsulta = sConsulta & "@MON_COD =S.MON," & vbCrLf
sConsulta = sConsulta & "@MON_DEN =M.DEN," & vbCrLf
sConsulta = sConsulta & "@APROBADOR = " & vbCrLf
sConsulta = sConsulta & "case when SEST.PER IS NULL THEN" & vbCrLf
sConsulta = sConsulta & "   (select usu from adm)       " & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "   P2.NOM + ' ' + P2.APE" & vbCrLf
sConsulta = sConsulta & "end," & vbCrLf
sConsulta = sConsulta & "@APROBADOR_COD = " & vbCrLf
sConsulta = sConsulta & "case when SEST.PER IS NULL THEN" & vbCrLf
sConsulta = sConsulta & "   (select usu from adm)       " & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "   SEST.PER            " & vbCrLf
sConsulta = sConsulta & "   END," & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "@APROBADOR_NOM = P2.NOM," & vbCrLf
sConsulta = sConsulta & "@APROBADOR_APE = P2.APE," & vbCrLf
sConsulta = sConsulta & "@APROBADOR_TFNO =P2.TFNO," & vbCrLf
sConsulta = sConsulta & "@APROBADOR_EMAIL = P2.EMAIL," & vbCrLf
sConsulta = sConsulta & "@APROBADOR_FAX =P2.FAX," & vbCrLf
sConsulta = sConsulta & "@APROBADOR_UON = " & vbCrLf
sConsulta = sConsulta & "case when U23.cod is null then" & vbCrLf
sConsulta = sConsulta & "   case when U22.cod is null then" & vbCrLf
sConsulta = sConsulta & "                   CASE WHEN U21.COD IS NULL THEN" & vbCrLf
sConsulta = sConsulta & "            ' '" & vbCrLf
sConsulta = sConsulta & "                            ELSE" & vbCrLf
sConsulta = sConsulta & "                                  U21.cod + ' - ' + U21.den" & vbCrLf
sConsulta = sConsulta & "                             END" & vbCrLf
sConsulta = sConsulta & "               ELSE" & vbCrLf
sConsulta = sConsulta & "       U21.COD + ' - ' + U22.COD + ' - ' + U22.DEN                        " & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   U21.COD + ' - ' + U22.COD + ' - ' + U23.COD + ' - '  + U23.DEN" & vbCrLf
sConsulta = sConsulta & "END," & vbCrLf
sConsulta = sConsulta & "@APROBADOR_DEP_COD=P2.DEP," & vbCrLf
sConsulta = sConsulta & "@APROBADOR_DEP_DEN = D2.DEN," & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "@COMPRADOR_COD = S.COMPRADOR," & vbCrLf
sConsulta = sConsulta & "@COMPRADOR_NOM = P.NOM," & vbCrLf
sConsulta = sConsulta & "@COMPRADOR_APE= P.APE," & vbCrLf
sConsulta = sConsulta & "@COMPRADOR_TFNO =P.TFNO," & vbCrLf
sConsulta = sConsulta & "@COMPRADOR_EMAIL= P.EMAIL," & vbCrLf
sConsulta = sConsulta & "@COMPRADOR_FAX =P.FAX," & vbCrLf
sConsulta = sConsulta & "@COMPRADOR_DEP_COD=P.DEP," & vbCrLf
sConsulta = sConsulta & "@COMPRADOR_DEP_DEN=D.DEN," & vbCrLf
sConsulta = sConsulta & "@COMPRADOR_UON= " & vbCrLf
sConsulta = sConsulta & "case when U3.cod is null then" & vbCrLf
sConsulta = sConsulta & "   case when U2.cod is null then" & vbCrLf
sConsulta = sConsulta & "                   CASE WHEN U1.COD IS NULL THEN" & vbCrLf
sConsulta = sConsulta & "            ' '" & vbCrLf
sConsulta = sConsulta & "                            ELSE" & vbCrLf
sConsulta = sConsulta & "                                  U1.cod + ' - ' + U1.den" & vbCrLf
sConsulta = sConsulta & "                             END" & vbCrLf
sConsulta = sConsulta & "               ELSE" & vbCrLf
sConsulta = sConsulta & "       U1.COD + ' - ' + U2.COD + ' - ' + U2.DEN                        " & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   U1.COD + ' - ' + U2.COD + ' - ' + U3.COD + ' - '  + U3.DEN" & vbCrLf
sConsulta = sConsulta & "END," & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "@PET_COD = S.PER," & vbCrLf
sConsulta = sConsulta & "@PET_NOM = PET.NOM," & vbCrLf
sConsulta = sConsulta & "@PET_APE = PET.APE," & vbCrLf
sConsulta = sConsulta & "@PET_TFNO =PET.TFNO," & vbCrLf
sConsulta = sConsulta & "@PET_EMAIL = PET.EMAIL," & vbCrLf
sConsulta = sConsulta & "@PET_FAX = PET.FAX," & vbCrLf
sConsulta = sConsulta & "@PET_UON = " & vbCrLf
sConsulta = sConsulta & "case when U_PET3.cod is null then" & vbCrLf
sConsulta = sConsulta & "   case when U_PET2.cod is null then" & vbCrLf
sConsulta = sConsulta & "                   CASE WHEN U_PET1.COD IS NULL THEN" & vbCrLf
sConsulta = sConsulta & "            ' '" & vbCrLf
sConsulta = sConsulta & "                            ELSE" & vbCrLf
sConsulta = sConsulta & "                                  U_PET1.cod + ' - ' + U_PET1.den" & vbCrLf
sConsulta = sConsulta & "                             END" & vbCrLf
sConsulta = sConsulta & "               ELSE" & vbCrLf
sConsulta = sConsulta & "       U_PET1.COD + ' - ' + U_PET2.COD + ' - ' + U_PET2.DEN                        " & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   U_PET1.COD + ' - ' + U_PET2.COD + ' - ' + U_PET3.COD + ' - '  + U_PET3.DEN" & vbCrLf
sConsulta = sConsulta & "END," & vbCrLf
sConsulta = sConsulta & "@PET_DEP_COD= PET.DEP," & vbCrLf
sConsulta = sConsulta & "@PET_DEP_DEN = PET_D.DEN," & vbCrLf
sConsulta = sConsulta & "@CARGO = S.CARGO," & vbCrLf
sConsulta = sConsulta & "@PROVEEDORES = S.PROVEEDORES," & vbCrLf
sConsulta = sConsulta & "@DEST_COD= S.DEST," & vbCrLf
sConsulta = sConsulta & "@DEST_DEN = DEST.DEN," & vbCrLf
sConsulta = sConsulta & "@DEST_DIR =DEST.DIR," & vbCrLf
sConsulta = sConsulta & "@DEST_POB =DEST.POB," & vbCrLf
sConsulta = sConsulta & "@DEST_CP = DEST.CP," & vbCrLf
sConsulta = sConsulta & "@DEST_PROVI_COD = DEST.PROVI," & vbCrLf
sConsulta = sConsulta & "@DEST_PROVI_DEN = PROVI.DEN," & vbCrLf
sConsulta = sConsulta & "@DEST_PAI_COD = DEST.PAI," & vbCrLf
sConsulta = sConsulta & "@DEST_PAI_DEN = PAI.DEN," & vbCrLf
sConsulta = sConsulta & "@COMENTARIO = SEST.COMENT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "FROM SOLICIT S" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN SOLICIT_EST SEST ON S.ID=SEST.SOLICIT AND S.ID=@ID AND SEST.ID IN (SELECT MAX(ID) FROM SOLICIT_EST WHERE SOLICIT=@ID)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INNER JOIN MON M ON S.MON=M.COD" & vbCrLf
sConsulta = sConsulta & "AND S.ID=@ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PER P ON S.COMPRADOR=P.COD" & vbCrLf
sConsulta = sConsulta & "AND S.ID=@ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN DEP D ON P.DEP=D.COD" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN UON1 U1 ON U1.COD=P.UON1" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN UON2 U2 ON U2.UON1=P.UON1 AND P.UON2=U2.COD" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN UON3 U3 ON U3.UON1=P.UON1 AND P.UON2=U3.UON2 AND P.UON3=U3.COD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PER P2 ON SEST.PER=P2.COD" & vbCrLf
sConsulta = sConsulta & "AND S.ID=@ID" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN DEP D2 ON P2.DEP=D2.COD" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN UON1 U21 ON U21.COD=P2.UON1" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN UON2 U22 ON U22.UON1=P2.UON1 AND P2.UON2=U22.COD" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN UON3 U23 ON U23.UON1=P2.UON1 AND P2.UON2=U23.UON2 AND P2.UON3=U23.COD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PER PET ON S.PER=PET.COD AND S.ID=@ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN DEP PET_D ON P.DEP=PET_D.COD" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN UON1 U_PET1 ON U_PET1.COD=PET.UON1" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN UON2 U_PET2 ON U_PET2.UON1=PET.UON1 AND PET.UON2=U_PET2.COD" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN UON3 U_PET3 ON U_PET3.UON1=PET.UON1 AND PET.UON2=U_PET3.UON2 AND PET.UON3=U_PET3.COD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN DEST ON S.DEST=DEST.COD" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PAI ON DEST.PAI=PAI.COD" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PROVI ON PROVI.PAI=PAI.COD AND PROVI.COD=DEST.PROVI" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_DEVOLVER_ADJUNTOS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_DEVOLVER_ADJUNTOS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_DEVOLVER_ADJUNTOS @ANYO smallint, @GMN1 VARCHAR(50), @PROCE int , @GRUPO varchar(50) = null, @ITEM int = null, @PROVE VARCHAR(50), @OFE INT AS " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ITEM IS NULL" & vbCrLf
sConsulta = sConsulta & "  IF @GRUPO IS NULL" & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "      SELECT POA.NOM AS NOM, " & vbCrLf
sConsulta = sConsulta & "             POA.COM AS COM,  " & vbCrLf
sConsulta = sConsulta & "             A.ID AS ID,  " & vbCrLf
sConsulta = sConsulta & "             POA.DATASIZE ," & vbCrLf
sConsulta = sConsulta & "             POA.IDPORTAL  " & vbCrLf
sConsulta = sConsulta & "        FROM PROCE_OFE_ADJUN POA " & vbCrLf
sConsulta = sConsulta & "              INNER JOIN ADJUN A " & vbCrLf
sConsulta = sConsulta & "                      ON POA.ID = A.ID  " & vbCrLf
sConsulta = sConsulta & "       WHERE POA.ANYO=@anyo " & vbCrLf
sConsulta = sConsulta & "         AND POA.GMN1=@GMN1   " & vbCrLf
sConsulta = sConsulta & "         AND POA.PROCE=@PROCE  " & vbCrLf
sConsulta = sConsulta & "         AND POA.PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "         AND POA.OFE=@OFE" & vbCrLf
sConsulta = sConsulta & "UNION" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SELECT OGA.NOM AS NOM, " & vbCrLf
sConsulta = sConsulta & "             OGA.COM AS COM,  " & vbCrLf
sConsulta = sConsulta & "             A.ID AS ID,  " & vbCrLf
sConsulta = sConsulta & "             OGA.DATASIZE ," & vbCrLf
sConsulta = sConsulta & "             OGA.IDPORTAL  " & vbCrLf
sConsulta = sConsulta & "        FROM OFE_GR_ADJUN OGA" & vbCrLf
sConsulta = sConsulta & "              INNER JOIN ADJUN A " & vbCrLf
sConsulta = sConsulta & "                      ON OGA.ID = A.ID  " & vbCrLf
sConsulta = sConsulta & "       WHERE ANYO = @anyo " & vbCrLf
sConsulta = sConsulta & "         AND GMN1 = @GMN1   " & vbCrLf
sConsulta = sConsulta & "         AND PROCE = @PROCE  " & vbCrLf
sConsulta = sConsulta & "         AND PROVE = @PROVE " & vbCrLf
sConsulta = sConsulta & "         AND OFE = @OFE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       UNION" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "       SELECT OIA.NOM AS NOM, " & vbCrLf
sConsulta = sConsulta & "           OIA.COM AS COM,  " & vbCrLf
sConsulta = sConsulta & "           A.ID AS ID,  " & vbCrLf
sConsulta = sConsulta & "           OIA.DATASIZE ," & vbCrLf
sConsulta = sConsulta & "           OIA.IDPORTAL  " & vbCrLf
sConsulta = sConsulta & "      FROM OFE_ITEM_ADJUN OIA" & vbCrLf
sConsulta = sConsulta & "            INNER JOIN ADJUN A " & vbCrLf
sConsulta = sConsulta & "                    ON OIA.ID = A.ID  " & vbCrLf
sConsulta = sConsulta & "     WHERE ANYO = @anyo " & vbCrLf
sConsulta = sConsulta & "       AND GMN1 = @GMN1   " & vbCrLf
sConsulta = sConsulta & "       AND PROCE = @PROCE  " & vbCrLf
sConsulta = sConsulta & "       AND PROVE = @PROVE " & vbCrLf
sConsulta = sConsulta & "       AND OFE = @OFE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    END" & vbCrLf
sConsulta = sConsulta & "  ELSE" & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "      SELECT OGA.NOM AS NOM, " & vbCrLf
sConsulta = sConsulta & "             OGA.COM AS COM,  " & vbCrLf
sConsulta = sConsulta & "             A.ID AS ID,  " & vbCrLf
sConsulta = sConsulta & "             OGA.DATASIZE ," & vbCrLf
sConsulta = sConsulta & "             OGA.IDPORTAL  " & vbCrLf
sConsulta = sConsulta & "        FROM OFE_GR_ADJUN OGA" & vbCrLf
sConsulta = sConsulta & "              INNER JOIN ADJUN A " & vbCrLf
sConsulta = sConsulta & "                      ON OGA.ID = A.ID  " & vbCrLf
sConsulta = sConsulta & "       WHERE ANYO = @anyo " & vbCrLf
sConsulta = sConsulta & "         AND GMN1 = @GMN1   " & vbCrLf
sConsulta = sConsulta & "         AND PROCE = @PROCE  " & vbCrLf
sConsulta = sConsulta & "         AND GRUPO = @GRUPO" & vbCrLf
sConsulta = sConsulta & "         AND PROVE = @PROVE " & vbCrLf
sConsulta = sConsulta & "         AND OFE = @OFE" & vbCrLf
sConsulta = sConsulta & "    END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    SELECT OIA.NOM AS NOM, " & vbCrLf
sConsulta = sConsulta & "           OIA.COM AS COM,  " & vbCrLf
sConsulta = sConsulta & "           A.ID AS ID,  " & vbCrLf
sConsulta = sConsulta & "           OIA.DATASIZE ," & vbCrLf
sConsulta = sConsulta & "           OIA.IDPORTAL  " & vbCrLf
sConsulta = sConsulta & "      FROM OFE_ITEM_ADJUN OIA" & vbCrLf
sConsulta = sConsulta & "            INNER JOIN ADJUN A " & vbCrLf
sConsulta = sConsulta & "                    ON OIA.ID = A.ID  " & vbCrLf
sConsulta = sConsulta & "     WHERE ANYO = @anyo " & vbCrLf
sConsulta = sConsulta & "       AND GMN1 = @GMN1   " & vbCrLf
sConsulta = sConsulta & "       AND PROCE = @PROCE  " & vbCrLf
sConsulta = sConsulta & "       AND ITEM = @ITEM" & vbCrLf
sConsulta = sConsulta & "       AND PROVE = @PROVE " & vbCrLf
sConsulta = sConsulta & "       AND OFE = @OFE" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion2_11_50_07A2_11_50_08() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim bBool As Boolean

On Error GoTo error
    

sConsulta = "BEGIN TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = True
ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

frmProgreso.lblDetalle = "Modificamos stored procedures"
frmProgreso.lblDetalle.Refresh
frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_11_5_Storeds_08

sConsulta = "UPDATE VERSION SET NUM ='2.11.50.08'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "COMMIT TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = False

CodigoDeActualizacion2_11_50_07A2_11_50_08 = True
Exit Function
    
error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_11_50_07A2_11_50_08 = False

End Function

Public Function CodigoDeActualizacion2_11_50_08A2_11_50_09() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim bBool As Boolean

On Error GoTo error
    

sConsulta = "BEGIN TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = True
ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

frmProgreso.lblDetalle = "Modificamos stored procedures"
frmProgreso.lblDetalle.Refresh
frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_11_5_Storeds_09

sConsulta = "UPDATE VERSION SET NUM ='2.11.50.09'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "COMMIT TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = False

CodigoDeActualizacion2_11_50_08A2_11_50_09 = True
Exit Function
    
error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_11_50_08A2_11_50_09 = False

End Function

Public Function CodigoDeActualizacion2_11_50_09A2_11_50_10() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim bBool As Boolean

On Error GoTo error
    

sConsulta = "BEGIN TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = True
ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

frmProgreso.lblDetalle = "Modificamos tablas"
frmProgreso.lblDetalle.Refresh
frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_11_5_Tablas_10

frmProgreso.lblDetalle = "Modificamos stored procedures"
frmProgreso.lblDetalle.Refresh
frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_11_5_Storeds_10

sConsulta = "UPDATE VERSION SET NUM ='2.11.50.10'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "COMMIT TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = False

CodigoDeActualizacion2_11_50_09A2_11_50_10 = True
Exit Function
    
error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_11_50_09A2_11_50_10 = False

End Function

Public Function CodigoDeActualizacion2_11_50_10A2_11_50_11() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim bBool As Boolean

On Error GoTo error
    

sConsulta = "BEGIN TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = True
ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

frmProgreso.lblDetalle = "Modificamos stored procedures"
frmProgreso.lblDetalle.Refresh
frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_11_5_Storeds_11

sConsulta = "UPDATE VERSION SET NUM ='2.11.50.11'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "COMMIT TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = False

CodigoDeActualizacion2_11_50_10A2_11_50_11 = True
Exit Function
    
error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_11_50_10A2_11_50_11 = False

End Function

Private Sub V_2_11_5_Storeds_08()
Dim sConsulta As String

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_DEVOLVER_ADJUNTOS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_DEVOLVER_ADJUNTOS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_DEVOLVER_ADJUNTOS @ANYO smallint, @GMN1 VARCHAR(50), @PROCE int , @GRUPO varchar(50) = null, @ITEM int = null, @PROVE VARCHAR(50), @OFE INT AS " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ITEM IS NULL" & vbCrLf
sConsulta = sConsulta & "  IF @GRUPO IS NULL" & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "      SELECT POA.NOM AS NOM, " & vbCrLf
sConsulta = sConsulta & "             POA.COM AS COM,  " & vbCrLf
sConsulta = sConsulta & "             A.ID AS ID,  " & vbCrLf
sConsulta = sConsulta & "             POA.DATASIZE ," & vbCrLf
sConsulta = sConsulta & "             POA.IDPORTAL  " & vbCrLf
sConsulta = sConsulta & "        FROM PROCE_OFE_ADJUN POA " & vbCrLf
sConsulta = sConsulta & "              INNER JOIN ADJUN A " & vbCrLf
sConsulta = sConsulta & "                      ON POA.ID = A.ID  " & vbCrLf
sConsulta = sConsulta & "       WHERE POA.ANYO=@anyo " & vbCrLf
sConsulta = sConsulta & "         AND POA.GMN1=@GMN1   " & vbCrLf
sConsulta = sConsulta & "         AND POA.PROCE=@PROCE  " & vbCrLf
sConsulta = sConsulta & "         AND POA.PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "         AND POA.OFE=@OFE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    END" & vbCrLf
sConsulta = sConsulta & "  ELSE" & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "      SELECT OGA.NOM AS NOM, " & vbCrLf
sConsulta = sConsulta & "             OGA.COM AS COM,  " & vbCrLf
sConsulta = sConsulta & "             A.ID AS ID,  " & vbCrLf
sConsulta = sConsulta & "             OGA.DATASIZE ," & vbCrLf
sConsulta = sConsulta & "             OGA.IDPORTAL  " & vbCrLf
sConsulta = sConsulta & "        FROM OFE_GR_ADJUN OGA" & vbCrLf
sConsulta = sConsulta & "              INNER JOIN ADJUN A " & vbCrLf
sConsulta = sConsulta & "                      ON OGA.ID = A.ID  " & vbCrLf
sConsulta = sConsulta & "       WHERE ANYO = @anyo " & vbCrLf
sConsulta = sConsulta & "         AND GMN1 = @GMN1   " & vbCrLf
sConsulta = sConsulta & "         AND PROCE = @PROCE  " & vbCrLf
sConsulta = sConsulta & "         AND GRUPO = @GRUPO" & vbCrLf
sConsulta = sConsulta & "         AND PROVE = @PROVE " & vbCrLf
sConsulta = sConsulta & "         AND OFE = @OFE" & vbCrLf
sConsulta = sConsulta & "    END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    SELECT OIA.NOM AS NOM, " & vbCrLf
sConsulta = sConsulta & "           OIA.COM AS COM,  " & vbCrLf
sConsulta = sConsulta & "           A.ID AS ID,  " & vbCrLf
sConsulta = sConsulta & "           OIA.DATASIZE ," & vbCrLf
sConsulta = sConsulta & "           OIA.IDPORTAL  " & vbCrLf
sConsulta = sConsulta & "      FROM OFE_ITEM_ADJUN OIA" & vbCrLf
sConsulta = sConsulta & "            INNER JOIN ADJUN A " & vbCrLf
sConsulta = sConsulta & "                    ON OIA.ID = A.ID  " & vbCrLf
sConsulta = sConsulta & "     WHERE ANYO = @anyo " & vbCrLf
sConsulta = sConsulta & "       AND GMN1 = @GMN1   " & vbCrLf
sConsulta = sConsulta & "       AND PROCE = @PROCE  " & vbCrLf
sConsulta = sConsulta & "       AND ITEM = @ITEM" & vbCrLf
sConsulta = sConsulta & "       AND PROVE = @PROVE " & vbCrLf
sConsulta = sConsulta & "       AND OFE = @OFE" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_DEVOLVER_ADJUNTOS_COMPARATIVA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_DEVOLVER_ADJUNTOS_COMPARATIVA]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_DEVOLVER_ADJUNTOS_COMPARATIVA @ANYO smallint, @GMN1 VARCHAR(50), @PROCE int , @PROVE VARCHAR(50), @OFE INT AS " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT POA.NOM AS NOM, POA.COM AS COM," & vbCrLf
sConsulta = sConsulta & "             A.ID AS ID, " & vbCrLf
sConsulta = sConsulta & "             POA.DATASIZE ," & vbCrLf
sConsulta = sConsulta & "             POA.IDPORTAL  " & vbCrLf
sConsulta = sConsulta & "        FROM PROCE_OFE_ADJUN POA " & vbCrLf
sConsulta = sConsulta & "              INNER JOIN ADJUN A " & vbCrLf
sConsulta = sConsulta & "                      ON POA.ID = A.ID  " & vbCrLf
sConsulta = sConsulta & "       WHERE POA.ANYO=@anyo " & vbCrLf
sConsulta = sConsulta & "         AND POA.GMN1=@GMN1   " & vbCrLf
sConsulta = sConsulta & "         AND POA.PROCE=@PROCE  " & vbCrLf
sConsulta = sConsulta & "         AND POA.PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "         AND POA.OFE=@OFE" & vbCrLf
sConsulta = sConsulta & "UNION" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SELECT OGA.NOM AS NOM, " & vbCrLf
sConsulta = sConsulta & "             OGA.COM AS COM,  " & vbCrLf
sConsulta = sConsulta & "             A.ID AS ID,  " & vbCrLf
sConsulta = sConsulta & "             OGA.DATASIZE ," & vbCrLf
sConsulta = sConsulta & "             OGA.IDPORTAL  " & vbCrLf
sConsulta = sConsulta & "        FROM OFE_GR_ADJUN OGA" & vbCrLf
sConsulta = sConsulta & "              INNER JOIN ADJUN A " & vbCrLf
sConsulta = sConsulta & "                      ON OGA.ID = A.ID  " & vbCrLf
sConsulta = sConsulta & "       WHERE ANYO = @anyo " & vbCrLf
sConsulta = sConsulta & "         AND GMN1 = @GMN1   " & vbCrLf
sConsulta = sConsulta & "         AND PROCE = @PROCE  " & vbCrLf
sConsulta = sConsulta & "         AND PROVE = @PROVE " & vbCrLf
sConsulta = sConsulta & "         AND OFE = @OFE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       UNION" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       SELECT OIA.NOM AS NOM, " & vbCrLf
sConsulta = sConsulta & "           OIA.COM AS COM, " & vbCrLf
sConsulta = sConsulta & "           A.ID AS ID,  " & vbCrLf
sConsulta = sConsulta & "           OIA.DATASIZE ," & vbCrLf
sConsulta = sConsulta & "           OIA.IDPORTAL  " & vbCrLf
sConsulta = sConsulta & "      FROM OFE_ITEM_ADJUN OIA" & vbCrLf
sConsulta = sConsulta & "            INNER JOIN ADJUN A " & vbCrLf
sConsulta = sConsulta & "                    ON OIA.ID = A.ID  " & vbCrLf
sConsulta = sConsulta & "     WHERE ANYO = @anyo" & vbCrLf
sConsulta = sConsulta & "       AND GMN1 = @GMN1   " & vbCrLf
sConsulta = sConsulta & "       AND PROCE = @PROCE  " & vbCrLf
sConsulta = sConsulta & "       AND PROVE = @PROVE " & vbCrLf
sConsulta = sConsulta & "       AND OFE = @OFE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Private Sub V_2_11_5_Storeds_09()
Dim sConsulta As String

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_DEVOLVER_GRUPO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_DEVOLVER_GRUPO]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE SP_DEVOLVER_GRUPO @ANYO INT, @GMN1 VARCHAR(20),@PROCE INT ,@GRUPO VARCHAR(20), @PROVE VARCHAR(50) = null, @OFE int = NULL, @MON VARCHAR(50) = NULL AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @HAYESP TINYINT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @HAYATRIB TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCEEST smallint" & vbCrLf
sConsulta = sConsulta & "DECLARE @PCAMBIO FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @POCAMBIO FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @MONOFE VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @PCAMBIO = CAMBIO,@PROCEEST = EST" & vbCrLf
sConsulta = sConsulta & "  FROM PROCE " & vbCrLf
sConsulta = sConsulta & " WHERE ANYO=@ANYO" & vbCrLf
sConsulta = sConsulta & "   AND GMN1=@GMN1" & vbCrLf
sConsulta = sConsulta & "   AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @POCAMBIO =NULL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @POCAMBIO = CAMBIO, @MONOFE = MON" & vbCrLf
sConsulta = sConsulta & "  FROM PROCE_OFE" & vbCrLf
sConsulta = sConsulta & " WHERE ANYO=@ANYO" & vbCrLf
sConsulta = sConsulta & "   AND GMN1=@GMN1" & vbCrLf
sConsulta = sConsulta & "   AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   AND PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "   AND OFE=@OFE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @CAMAPLIC FLOAT" & vbCrLf
sConsulta = sConsulta & "--CAMBIO A APLICAR A LOS PRECIOS. SI LA MONEDA QUE SE PASA ES LA MISMA QUE EN LA QUE ESTAN ALMACENADOS EL CAMBIO SERA 1 SINO SERA LA EQUIVALENCIA DE LA NUEVA MONEDA * @CAMCENTRAL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @CAMBIO FLOAT" & vbCrLf
sConsulta = sConsulta & "SELECT @CAMBIO  =  EQUIV " & vbCrLf
sConsulta = sConsulta & "  FROM MON " & vbCrLf
sConsulta = sConsulta & " WHERE COD = @MON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @MON = @MONOFE" & vbCrLf
sConsulta = sConsulta & "  SET @CAMAPLIC = 1" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  SET @CAMAPLIC =  (@CAMBIO / (@POCAMBIO*@PCAMBIO))" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--EN DEFINITIVA, EL TOTAL SE DEVOLVERA EN LA MONEDA QUE SE PASA A LA STORED" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @HAYESP = CASE WHEN ESP IS NULL OR ESP='' THEN 0 ELSE 1 END " & vbCrLf
sConsulta = sConsulta & "  FROM PROCE_GRUPO" & vbCrLf
sConsulta = sConsulta & " WHERE ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "   AND GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "   AND PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "   AND COD = @GRUPO" & vbCrLf
sConsulta = sConsulta & "IF @HAYESP = 0" & vbCrLf
sConsulta = sConsulta & "  IF (SELECT count(id) " & vbCrLf
sConsulta = sConsulta & "        FROM PROCE_GRUPO_ESP" & vbCrLf
sConsulta = sConsulta & "       WHERE ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "         and GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "         and PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "         AND GRUPO = @GRUPO)>0" & vbCrLf
sConsulta = sConsulta & "    SET @HAYESP = 1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @HAYATRIB = 0" & vbCrLf
sConsulta = sConsulta & "IF (SELECT COUNT(*)" & vbCrLf
sConsulta = sConsulta & "      FROM PROCE_ATRIB" & vbCrLf
sConsulta = sConsulta & "     WHERE ANYO  = @ANYO" & vbCrLf
sConsulta = sConsulta & "       and GMN1  = @GMN1 " & vbCrLf
sConsulta = sConsulta & "       and PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "       AND AMBITO = 2" & vbCrLf
sConsulta = sConsulta & "       and ISNULL(GRUPO, @GRUPO) = @GRUPO)>0" & vbCrLf
sConsulta = sConsulta & "  SET @HAYATRIB = 1  " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "DECLARE @IMPORTEOFERTA float" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if @PROVE IS NOT NULL AND @OFE IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SELECT @IMPORTEOFERTA = SUM(ISNULL(IO.PRECIO,0) * ISNULL(I.CANT,0)) " & vbCrLf
sConsulta = sConsulta & "  FROM ITEM_OFE IO" & vbCrLf
sConsulta = sConsulta & "         INNER JOIN ITEM I" & vbCrLf
sConsulta = sConsulta & "                 ON IO.PROCE = I.PROCE" & vbCrLf
sConsulta = sConsulta & "                AND IO.GMN1 = I.GMN1" & vbCrLf
sConsulta = sConsulta & "                AND IO.ANYO = I.ANYO" & vbCrLf
sConsulta = sConsulta & "                AND IO.ITEM =I.ID" & vbCrLf
sConsulta = sConsulta & " WHERE IO.ANYO  = @ANYO" & vbCrLf
sConsulta = sConsulta & "   AND IO.GMN1  = @GMN1 " & vbCrLf
sConsulta = sConsulta & "   AND IO.PROCE = @PROCE " & vbCrLf
sConsulta = sConsulta & "   AND IO.PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "   AND IO.NUM   = @OFE" & vbCrLf
sConsulta = sConsulta & "   AND I.GRUPO = @GRUPO" & vbCrLf
sConsulta = sConsulta & "   AND I.CONF=1" & vbCrLf
sConsulta = sConsulta & "   AND CASE WHEN I.EST IS NULL THEN -1 ELSE I.EST END = CASE WHEN @PROCEEST=11 THEN 0 ELSE -1 END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT PG.COD, PG.DEN, PG.DESCR, PG.DEST, PG.PAG, PAG.DEN DENPAG, PG.FECINI, PG.FECFIN, PG.ESP, @HAYESP HAYESP, @HAYATRIB HAYATRIB, @IMPORTEOFERTA IMPORTEBASE, @IMPORTEOFERTA * @CAMAPLIC IMPORTEOFERTA, GO.OBSADJUN" & vbCrLf
sConsulta = sConsulta & "  FROM PROCE_GRUPO PG" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN PAG" & vbCrLf
sConsulta = sConsulta & "               ON PG.PAG = PAG.COD" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN GRUPO_OFE GO" & vbCrLf
sConsulta = sConsulta & "              ON PG.ANYO = GO.ANYO" & vbCrLf
sConsulta = sConsulta & "             AND PG.GMN1 = GO.GMN1" & vbCrLf
sConsulta = sConsulta & "             AND PG.PROCE = GO.PROCE" & vbCrLf
sConsulta = sConsulta & "             AND PG.COD = GO.GRUPO" & vbCrLf
sConsulta = sConsulta & "             AND @PROVE = GO.PROVE" & vbCrLf
sConsulta = sConsulta & "             AND @OFE = GO.OFE" & vbCrLf
sConsulta = sConsulta & " WHERE PG.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "   AND PG.GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "   AND PG.PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "   AND PG.COD = @GRUPO" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Private Sub V_2_11_5_Tablas_10()
Dim sConsulta As String

sConsulta = ""

sConsulta = "ALTER TABLE DBO.P_PROCE_OFE ADD ADJUN TINYINT "
ExecuteSQL gRDOCon, sConsulta
sConsulta = "ALTER TABLE DBO.P_GRUPO_OFE ADD ADJUN TINYINT "
ExecuteSQL gRDOCon, sConsulta
sConsulta = "ALTER TABLE DBO.P_ITEM_OFE ADD ADJUN TINYINT "
ExecuteSQL gRDOCon, sConsulta



End Sub

Private Sub V_2_11_5_Storeds_10()
Dim sConsulta As String

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ANYA_PROVE_CON]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_ANYA_PROVE_CON]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_CARGAR_CONFIG_PROCE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_CARGAR_CONFIG_PROCE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_DEVOLVER_GRUPO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_DEVOLVER_GRUPO]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_DEVOLVER_OFE_ATRIB]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_DEVOLVER_OFE_ATRIB]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_TRASPASAR_OFERTA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_TRASPASAR_OFERTA]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE SP_ANYA_PROVE_CON (@PROVE VARCHAR(100),@APE VARCHAR(100),@NOM VARCHAR(100),@DEP VARCHAR(100),@CAR VARCHAR(100),@TFNO VARCHAR(50),@TFNO2 VARCHAR(50),@FAX VARCHAR(50),@EMAIL VARCHAR(100),@NUM INT,@TFNO_MOVIL VARCHAR(50), @ID_PORT INT,@IDI VARCHAR(20), @TIPOEMAIL TINYINT)  AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @IND INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ACTIVA BIT" & vbCrLf
sConsulta = sConsulta & "DECLARE @INT TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @SENTIDO TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_CUR AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_LOG_CON AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @APE_CUR VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @NOM_CUR VARCHAR(20)" & vbCrLf
sConsulta = sConsulta & "DECLARE @TFNO_CUR VARCHAR(20)" & vbCrLf
sConsulta = sConsulta & "DECLARE @TFNO2_CUR VARCHAR(20)" & vbCrLf
sConsulta = sConsulta & "DECLARE @TFNO_MOVIL_CUR VARCHAR(20)" & vbCrLf
sConsulta = sConsulta & "DECLARE @RECPET_CUR TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @APROV_CUR TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @SUBASTA_CUR TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @DEP_CUR VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @CAR_CUR VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @FAX_CUR VARCHAR(20)" & vbCrLf
sConsulta = sConsulta & "DECLARE @EMAIL_CUR VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "/*******************************************************************************************************************************************************************************************/" & vbCrLf
sConsulta = sConsulta & "/******       INTEGRACION :  Primero miramos a ver si est� activada : a nivel gral y en concreto la entidad PROVE en sentido salida o entrada/salida          ******/" & vbCrLf
sConsulta = sConsulta & "/****************************************************************************************************************************************************************************************/" & vbCrLf
sConsulta = sConsulta & "SET @INT = (SELECT INTEGRACION FROM PARGEN_INTERNO WHERE ID=1)" & vbCrLf
sConsulta = sConsulta & "IF @INT = 1 " & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & " SELECT @ACTIVA=ACTIVA,@SENTIDO=SENTIDO FROM TABLAS_INTEGRACION WHERE ID=8" & vbCrLf
sConsulta = sConsulta & " IF @ACTIVA = 1 AND (@SENTIDO = 1 OR @SENTIDO=3)" & vbCrLf
sConsulta = sConsulta & "    SET @INT = 1" & vbCrLf
sConsulta = sConsulta & " ELSE" & vbCrLf
sConsulta = sConsulta & "   SET @INT = 0" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "/*************************************************************************************************************************************************************************************/" & vbCrLf
sConsulta = sConsulta & "/******      Integraci�n : Por cada contacto que se vaya a borrar grabamos una fila en la tabla LOG_CON y su correspondiente LOG_PROVE             ******/" & vbCrLf
sConsulta = sConsulta & "/******                          Recorremos los contactos a borrar con un cursor  Cur_CON, recuperamos, insertamos en LOG_CON y recuperamos el ID      *****/    " & vbCrLf
sConsulta = sConsulta & "/******                          Las modificaciones sobre la tabla PROVE no nos afectan                                                                                                        *****/                                                                       " & vbCrLf
sConsulta = sConsulta & "/*******************************************************************************************************************************************************************************/" & vbCrLf
sConsulta = sConsulta & "IF @NUM = 1 AND @INT = 1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & " DECLARE Cur_CON Cursor FOR SELECT ID,APE,NOM,DEP,CAR,TFNO,FAX,EMAIL,RECPET,TFNO2,TFNO_MOVIL,APROV,SUBASTA FROM CON WHERE PROVE =@PROVE AND PORT =1" & vbCrLf
sConsulta = sConsulta & " OPEN Cur_CON" & vbCrLf
sConsulta = sConsulta & " FETCH NEXT FROM Cur_CON INTO @ID_CUR,@APE_CUR,@NOM_CUR,@DEP_CUR,@CAR_CUR,@TFNO_CUR,@FAX_CUR,@EMAIL_CUR,@RECPET_CUR,@TFNO2_CUR,@TFNO_MOVIL_CUR,@APROV_CUR,@SUBASTA_CUR" & vbCrLf
sConsulta = sConsulta & " WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & " BEGIN " & vbCrLf
sConsulta = sConsulta & "/* Grabamos en LOG_con y LOG_PROVE   */ " & vbCrLf
sConsulta = sConsulta & "   INSERT INTO LOG_CON (ACCION,PROVE,ID_CON,APE,NOM,DEP,CAR,TFNO,FAX,EMAIL,RECPET,TFNO2,TFNO_MOVIL,APROV,SUBASTA,ORIGEN,USU) VALUES" & vbCrLf
sConsulta = sConsulta & "   ( 'D',@PROVE,@ID_CUR,@APE_CUR,@NOM_CUR,@DEP_CUR,@CAR_CUR,@TFNO_CUR,@FAX_CUR,@EMAIL_CUR,@RECPET_CUR,@TFNO2_CUR,@TFNO_MOVIL_CUR,@APROV_CUR,@SUBASTA_CUR,0,'') " & vbCrLf
sConsulta = sConsulta & "   SET @ID_LOG_CON = (SELECT MAX(ID) FROM LOG_CON)" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO LOG_PROVE (ACCION,ID_LOG_CON,COD,DEN,DIR,CP,POB,PAI,PROVI,MON,NIF,URLPROVE,ORIGEN,USU) SELECT 'R',@ID_LOG_CON,COD,DEN,DIR,CP,POB,PAI,PROVI,MON,NIF,URLPROVE,0,''  FROM PROVE WHERE COD =@PROVE" & vbCrLf
sConsulta = sConsulta & " FETCH NEXT FROM Cur_CON INTO  @ID_CUR,@APE_CUR,@NOM_CUR,@DEP_CUR,@CAR_CUR,@TFNO_CUR,@FAX_CUR,@EMAIL_CUR,@RECPET_CUR,@TFNO2_CUR,@TFNO_MOVIL_CUR,@APROV_CUR,@SUBASTA_CUR" & vbCrLf
sConsulta = sConsulta & " End" & vbCrLf
sConsulta = sConsulta & " Close Cur_CON" & vbCrLf
sConsulta = sConsulta & " DEALLOCATE Cur_CON" & vbCrLf
sConsulta = sConsulta & " END" & vbCrLf
sConsulta = sConsulta & "/************************************************ fin trat integraci�n  PARA DELETES **********************************************************************************************/" & vbCrLf
sConsulta = sConsulta & "IF @NUM = 1 " & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF ((SELECT COUNT(*)  FROM CON WHERE PROVE=@PROVE AND PORT=1) > 0)" & vbCrLf
sConsulta = sConsulta & "DELETE FROM CON WHERE PROVE=@PROVE AND PORT=1" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "SET @IND=(SELECT ISNULL(MAX(ID),0) FROM CON WHERE PROVE=@PROVE)" & vbCrLf
sConsulta = sConsulta & "SET @IND=@IND+1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO CON (PROVE,ID,APE,NOM,DEP,CAR,TFNO,FAX,EMAIL,RECPET,TFNO2,PORT,TFNO_MOVIL,ID_PORT,IDI,TIPOEMAIL) VALUES " & vbCrLf
sConsulta = sConsulta & "(@PROVE,@IND,@APE,@NOM,@DEP,@CAR,@TFNO,@FAX,@EMAIL,1,@TFNO2,1,@TFNO_MOVIL,@ID_PORT, @IDI, @TIPOEMAIL)" & vbCrLf
sConsulta = sConsulta & "/*************************************************************************************************************************************************************************************/" & vbCrLf
sConsulta = sConsulta & "/******      Integraci�n : Por cada contacto que se vaya a INSERTAR grabamos una fila en la tabla LOG_CON y su correspondiente LOG_PROVE     ******/" & vbCrLf
sConsulta = sConsulta & "/***********************************************************************************************************************************************************************************/" & vbCrLf
sConsulta = sConsulta & " IF @INT = 1" & vbCrLf
sConsulta = sConsulta & " BEGIN" & vbCrLf
sConsulta = sConsulta & "       INSERT INTO LOG_CON (ACCION,PROVE,ID_CON,APE,NOM,DEP,CAR,TFNO,FAX,EMAIL,RECPET,TFNO2,TFNO_MOVIL,ORIGEN,USU)" & vbCrLf
sConsulta = sConsulta & "             VALUES('I',@PROVE,@IND,@APE,@NOM,@DEP,@CAR,@TFNO,@FAX,@EMAIL,1,@TFNO2,@TFNO_MOVIL,0,'')" & vbCrLf
sConsulta = sConsulta & "       SET @ID_LOG_CON = (SELECT MAX(ID) FROM LOG_CON)" & vbCrLf
sConsulta = sConsulta & "       INSERT INTO LOG_PROVE (ACCION,ID_LOG_CON,COD,DEN,DIR,CP,POB,PAI,PROVI,MON,NIF,URLPROVE,ORIGEN,USU) SELECT 'R',@ID_LOG_CON,COD,DEN,DIR,CP,POB,PAI,PROVI,MON,NIF,URLPROVE,0,''  FROM PROVE WHERE COD =@PROVE" & vbCrLf
sConsulta = sConsulta & " END" & vbCrLf
sConsulta = sConsulta & "Return" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE SP_CARGAR_CONFIG_PROCE @ANYO INT, @GMN1 VARCHAR(50), @PROCE INT,@DEFDEST tinyint  = null OUTPUT, @DEFPAG tinyint = null OUTPUT," & vbCrLf
sConsulta = sConsulta & "                                        @DEFFECSUM tinyint  = null OUTPUT, @DEFPROCEESP tinyint  = null OUTPUT, @DEFGRUPOESP  tinyint = null OUTPUT, @DEFITEMESP tinyint = null OUTPUT, " & vbCrLf
sConsulta = sConsulta & "                                        @DEFOFEADJUN tinyint = null OUTPUT, @DEFGRUPOADJUN tinyint = null OUTPUT, @DEFITEMADJUN tinyint = null OUTPUT," & vbCrLf
sConsulta = sConsulta & "                                        @DEFSUBASTA tinyint = null OUTPUT, @DEFSUVERPROVE tinyint = null OUTPUT, @DEFSUVERPUJAS tinyint = null OUTPUT, @DEFSUMINUTOS int = null OUTPUT," & vbCrLf
sConsulta = sConsulta & "                                        @DEFPRECALTER tinyint = null OUTPUT, @DEFCANTMAX tinyint = null OUTPUT, @HAYATRIBUTOS AS tinyint = null OUTPUT, " & vbCrLf
sConsulta = sConsulta & "                                        @CAMBIARMON tinyint = 1 OUTPUT, @ADMINPUB tinyint=null OUTPUT, @NUMMAXOFE int = 0 OUTPUT AS" & vbCrLf
sConsulta = sConsulta & "set nocount on" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCEEST INT" & vbCrLf
sConsulta = sConsulta & "SELECT @PROCEEST = EST " & vbCrLf
sConsulta = sConsulta & "  FROM PROCE " & vbCrLf
sConsulta = sConsulta & " WHERE ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "   AND GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "   AND COD = @PROCE" & vbCrLf
sConsulta = sConsulta & "SELECT @DEFDEST=PD.DEST, @DEFPAG=PD.PAG, @DEFFECSUM=PD.FECSUM, @DEFPROCEESP=PD.PROCE_ESP, @DEFGRUPOESP = PD.GRUPO_ESP, @DEFITEMESP  = PD.ITEM_ESP, " & vbCrLf
sConsulta = sConsulta & "       @DEFOFEADJUN = PD.OFE_ADJUN, @DEFGRUPOADJUN=PD.GRUPO_ADJUN, @DEFITEMADJUN= PD.ITEM_ADJUN, @DEFCANTMAX=PD.SOLCANTMAX, @DEFPRECALTER = PD.PRECALTER, " & vbCrLf
sConsulta = sConsulta & "       @DEFSUBASTA = PD.SUBASTA, @DEFSUVERPROVE = PD.SUBASTAPROVE, @DEFSUVERPUJAS=PD.SUBASTAPUJAS, @DEFSUMINUTOS = PD.SUBASTAESPERA," & vbCrLf
sConsulta = sConsulta & "       @HAYATRIBUTOS = ISNULL(PA.ATRIB,0),@CAMBIARMON=PD.CAMBIARMON, @ADMINPUB = ADMIN_PUB, @NUMMAXOFE = PD.MAX_OFE " & vbCrLf
sConsulta = sConsulta & "  FROM PROCE_DEF PD" & vbCrLf
sConsulta = sConsulta & "          LEFT JOIN (SELECT ANYO, GMN1, PROCE, COUNT(ATRIB ) ATRIB" & vbCrLf
sConsulta = sConsulta & "                        FROM PROCE_ATRIB" & vbCrLf
sConsulta = sConsulta & "                       WHERE ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "                         AND GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "                         AND PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "                         AND AMBITO = 1" & vbCrLf
sConsulta = sConsulta & "                         AND INTERNO = 0" & vbCrLf
sConsulta = sConsulta & "                    GROUP BY ANYO, GMN1, PROCE) PA" & vbCrLf
sConsulta = sConsulta & "                  ON PD.ANYO = PA.ANYO" & vbCrLf
sConsulta = sConsulta & "                 AND PD.GMN1 = PA.GMN1" & vbCrLf
sConsulta = sConsulta & "                 AND PD.PROCE = PA.PROCE " & vbCrLf
sConsulta = sConsulta & " WHERE PD.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "   AND PD.GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "   AND PD.PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "  " & vbCrLf
sConsulta = sConsulta & "select PG.anyo, PG.gmn1, PG.proce, PG.COD , PG.DEN, COUNT(PA.ATRIB) ATRIB, S.SOBRE, S.USUAPER, S.EST, S.FECAPE, S.FECEST, S.OBS," & vbCrLf
sConsulta = sConsulta & "       sum(case dato when 'ESP' THEN 1 else 0 end) as ESP," & vbCrLf
sConsulta = sConsulta & "       sum(case dato when 'PRES1' THEN 1 ELSE 0 END) AS PRES1," & vbCrLf
sConsulta = sConsulta & "       sum(case dato when 'PRES2' THEN 1 ELSE 0 END) AS PRES2," & vbCrLf
sConsulta = sConsulta & "       sum(case dato when 'PRESANU1' THEN 1 ELSE 0 END) AS PRESANU1," & vbCrLf
sConsulta = sConsulta & "       sum(case dato when 'PRESANU2' THEN 1 ELSE 0 END) AS PRESANU2," & vbCrLf
sConsulta = sConsulta & "       sum(case dato when 'DIST' THEN 1 ELSE 0 END) AS DIST," & vbCrLf
sConsulta = sConsulta & "       sum(case dato when 'PROVE' THEN 1 ELSE 0 END) AS PROVE," & vbCrLf
sConsulta = sConsulta & "       sum(case dato when 'FECSUM' THEN 1 ELSE 0 END) AS FECSUM," & vbCrLf
sConsulta = sConsulta & "       sum(case dato when 'PAG' THEN 1 ELSE 0 END) AS PAG," & vbCrLf
sConsulta = sConsulta & "       sum(case dato when 'DEST' THEN 1 ELSE 0 END) AS DEST," & vbCrLf
sConsulta = sConsulta & "       ISNULL(IT.NUMITEMS,0) NUMITEMS" & vbCrLf
sConsulta = sConsulta & "  from PROCE_GRUPO PG " & vbCrLf
sConsulta = sConsulta & "          LEFT JOIN PROCE_GRUPO_DEF PGD" & vbCrLf
sConsulta = sConsulta & "                  ON PG.ANYO = PGD.ANYO" & vbCrLf
sConsulta = sConsulta & "                 AND PG.GMN1 = PGD.GMN1" & vbCrLf
sConsulta = sConsulta & "                 AND PG.PROCE = PGD.PROCE" & vbCrLf
sConsulta = sConsulta & "                 AND PG.COD = PGD.GRUPO" & vbCrLf
sConsulta = sConsulta & "           LEFT JOIN (SELECT ANYO, GMN1, PROCE, ISNULL(GRUPO,'#T#') GRUPO, ATRIB " & vbCrLf
sConsulta = sConsulta & "                        FROM PROCE_ATRIB " & vbCrLf
sConsulta = sConsulta & "                       WHERE ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "                         AND GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "                         AND PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "                         AND AMBITO = 2" & vbCrLf
sConsulta = sConsulta & "                         AND INTERNO = 0) PA" & vbCrLf
sConsulta = sConsulta & "                  ON PG.ANYO = PA.ANYO" & vbCrLf
sConsulta = sConsulta & "                 AND PG.GMN1 = PA.GMN1" & vbCrLf
sConsulta = sConsulta & "                 AND PG.PROCE = PA.PROCE" & vbCrLf
sConsulta = sConsulta & "                 AND PG.COD = CASE WHEN PA.GRUPO ='#T#' THEN PG.COD ELSE PA.GRUPO END" & vbCrLf
sConsulta = sConsulta & "          LEFT JOIN (SELECT I.GRUPO, count(I.id) NUMITEMS" & vbCrLf
sConsulta = sConsulta & "                        FROM ITEM I " & vbCrLf
sConsulta = sConsulta & "                      WHERE I.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "                        AND I.GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "                        AND I.PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "                        AND I.CONF = 1" & vbCrLf
sConsulta = sConsulta & "                        AND CASE WHEN I.EST IS NULL THEN -1 ELSE I.EST END = CASE WHEN @PROCEEST=11 THEN 0 ELSE -1 END" & vbCrLf
sConsulta = sConsulta & "                     GROUP BY I.GRUPO) IT" & vbCrLf
sConsulta = sConsulta & "                  ON PG.COD = IT.GRUPO" & vbCrLf
sConsulta = sConsulta & "           LEFT JOIN SOBRE S" & vbCrLf
sConsulta = sConsulta & "                  ON PG.ANYO = S.ANYO" & vbCrLf
sConsulta = sConsulta & "                 AND PG.GMN1 = S.GMN1" & vbCrLf
sConsulta = sConsulta & "                 AND PG.PROCE = S.PROCE" & vbCrLf
sConsulta = sConsulta & "                 AND PG.SOBRE = S.SOBRE" & vbCrLf
sConsulta = sConsulta & "where PG.anyo = @ANYO" & vbCrLf
sConsulta = sConsulta & "  and PG.gmn1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "  and PG.proce = @PROCE" & vbCrLf
sConsulta = sConsulta & "  and isnull(pg.cerrado,0) = 0" & vbCrLf
sConsulta = sConsulta & "group by PG.anyo, PG.gmn1, PG.proce, PG.cod, PG.DEN, IT.NUMITEMS, S.SOBRE, S.USUAPER, S.EST, S.FECAPE, S.FECEST, S.OBS" & vbCrLf
sConsulta = sConsulta & "ORDER BY S.SOBRE,PG.COD" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE SP_DEVOLVER_GRUPO @ANYO INT, @GMN1 VARCHAR(20),@PROCE INT ,@GRUPO VARCHAR(20), @PROVE VARCHAR(50) = null, @OFE int = NULL, @MON VARCHAR(50) = NULL AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @HAYESP TINYINT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @HAYATRIB TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCEEST smallint" & vbCrLf
sConsulta = sConsulta & "DECLARE @PCAMBIO FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @POCAMBIO FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @MONOFE VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @PCAMBIO = CAMBIO,@PROCEEST = EST" & vbCrLf
sConsulta = sConsulta & "  FROM PROCE " & vbCrLf
sConsulta = sConsulta & " WHERE ANYO=@ANYO" & vbCrLf
sConsulta = sConsulta & "   AND GMN1=@GMN1" & vbCrLf
sConsulta = sConsulta & "   AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @POCAMBIO =NULL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @POCAMBIO = CAMBIO, @MONOFE = MON" & vbCrLf
sConsulta = sConsulta & "  FROM PROCE_OFE" & vbCrLf
sConsulta = sConsulta & " WHERE ANYO=@ANYO" & vbCrLf
sConsulta = sConsulta & "   AND GMN1=@GMN1" & vbCrLf
sConsulta = sConsulta & "   AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   AND PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "   AND OFE=@OFE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @CAMAPLIC FLOAT" & vbCrLf
sConsulta = sConsulta & "--CAMBIO A APLICAR A LOS PRECIOS. SI LA MONEDA QUE SE PASA ES LA MISMA QUE EN LA QUE ESTAN ALMACENADOS EL CAMBIO SERA 1 SINO SERA LA EQUIVALENCIA DE LA NUEVA MONEDA * @CAMCENTRAL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @CAMBIO FLOAT" & vbCrLf
sConsulta = sConsulta & "SELECT @CAMBIO  =  EQUIV " & vbCrLf
sConsulta = sConsulta & "  FROM MON " & vbCrLf
sConsulta = sConsulta & " WHERE COD = @MON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @MON = @MONOFE" & vbCrLf
sConsulta = sConsulta & "  SET @CAMAPLIC = 1" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  SET @CAMAPLIC =  (@CAMBIO / (@POCAMBIO*@PCAMBIO))" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--EN DEFINITIVA, EL TOTAL SE DEVOLVERA EN LA MONEDA QUE SE PASA A LA STORED" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @HAYESP = CASE WHEN ESP IS NULL OR ESP='' THEN 0 ELSE 1 END " & vbCrLf
sConsulta = sConsulta & "  FROM PROCE_GRUPO" & vbCrLf
sConsulta = sConsulta & " WHERE ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "   AND GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "   AND PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "   AND COD = @GRUPO" & vbCrLf
sConsulta = sConsulta & "IF @HAYESP = 0" & vbCrLf
sConsulta = sConsulta & "  IF (SELECT count(id) " & vbCrLf
sConsulta = sConsulta & "        FROM PROCE_GRUPO_ESP" & vbCrLf
sConsulta = sConsulta & "       WHERE ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "         and GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "         and PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "         AND GRUPO = @GRUPO)>0" & vbCrLf
sConsulta = sConsulta & "    SET @HAYESP = 1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @HAYATRIB = 0" & vbCrLf
sConsulta = sConsulta & "IF (SELECT COUNT(*)" & vbCrLf
sConsulta = sConsulta & "      FROM PROCE_ATRIB" & vbCrLf
sConsulta = sConsulta & "     WHERE ANYO  = @ANYO" & vbCrLf
sConsulta = sConsulta & "       and GMN1  = @GMN1 " & vbCrLf
sConsulta = sConsulta & "       and PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "       AND AMBITO = 2" & vbCrLf
sConsulta = sConsulta & "       AND INTERNO = 0" & vbCrLf
sConsulta = sConsulta & "       and ISNULL(GRUPO, @GRUPO) = @GRUPO)>0" & vbCrLf
sConsulta = sConsulta & "  SET @HAYATRIB = 1  " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "DECLARE @IMPORTEOFERTA float" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if @PROVE IS NOT NULL AND @OFE IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SELECT @IMPORTEOFERTA = SUM(ISNULL(IO.PRECIO,0) * ISNULL(I.CANT,0)) " & vbCrLf
sConsulta = sConsulta & "  FROM ITEM_OFE IO" & vbCrLf
sConsulta = sConsulta & "         INNER JOIN ITEM I" & vbCrLf
sConsulta = sConsulta & "                 ON IO.PROCE = I.PROCE" & vbCrLf
sConsulta = sConsulta & "                AND IO.GMN1 = I.GMN1" & vbCrLf
sConsulta = sConsulta & "                AND IO.ANYO = I.ANYO" & vbCrLf
sConsulta = sConsulta & "                AND IO.ITEM =I.ID" & vbCrLf
sConsulta = sConsulta & " WHERE IO.ANYO  = @ANYO" & vbCrLf
sConsulta = sConsulta & "   AND IO.GMN1  = @GMN1 " & vbCrLf
sConsulta = sConsulta & "   AND IO.PROCE = @PROCE " & vbCrLf
sConsulta = sConsulta & "   AND IO.PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "   AND IO.NUM   = @OFE" & vbCrLf
sConsulta = sConsulta & "   AND I.GRUPO = @GRUPO" & vbCrLf
sConsulta = sConsulta & "   AND I.CONF=1" & vbCrLf
sConsulta = sConsulta & "   AND CASE WHEN I.EST IS NULL THEN -1 ELSE I.EST END = CASE WHEN @PROCEEST=11 THEN 0 ELSE -1 END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT PG.COD, PG.DEN, PG.DESCR, PG.DEST, PG.PAG, PAG.DEN DENPAG, PG.FECINI, PG.FECFIN, PG.ESP, @HAYESP HAYESP, @HAYATRIB HAYATRIB, @IMPORTEOFERTA IMPORTEBASE, @IMPORTEOFERTA * @CAMAPLIC IMPORTEOFERTA, GO.OBSADJUN" & vbCrLf
sConsulta = sConsulta & "  FROM PROCE_GRUPO PG" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN PAG" & vbCrLf
sConsulta = sConsulta & "               ON PG.PAG = PAG.COD" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN GRUPO_OFE GO" & vbCrLf
sConsulta = sConsulta & "              ON PG.ANYO = GO.ANYO" & vbCrLf
sConsulta = sConsulta & "             AND PG.GMN1 = GO.GMN1" & vbCrLf
sConsulta = sConsulta & "             AND PG.PROCE = GO.PROCE" & vbCrLf
sConsulta = sConsulta & "             AND PG.COD = GO.GRUPO" & vbCrLf
sConsulta = sConsulta & "             AND @PROVE = GO.PROVE" & vbCrLf
sConsulta = sConsulta & "             AND @OFE = GO.OFE" & vbCrLf
sConsulta = sConsulta & " WHERE PG.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "   AND PG.GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "   AND PG.PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "   AND PG.COD = @GRUPO" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE SP_DEVOLVER_OFE_ATRIB @ANYO smallint, @GMN1 varchar(50), @PROCE smallint, @PROVE varchar(50), @AMBITO tinyint, @GRUPO varchar(50)=null, @OFE smallint, @MON VARCHAR(50) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @CAM FLOAT " & vbCrLf
sConsulta = sConsulta & "DECLARE @EQUIVPROC FLOAT  " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @EQUIVPROC = CAMBIO" & vbCrLf
sConsulta = sConsulta & "  FROM PROCE " & vbCrLf
sConsulta = sConsulta & " WHERE ANYO=@ANYO" & vbCrLf
sConsulta = sConsulta & "   AND GMN1=@GMN1" & vbCrLf
sConsulta = sConsulta & "   AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "SET @CAM =( SELECT CAMBIO" & vbCrLf
sConsulta = sConsulta & "             FROM PROCE_OFE" & vbCrLf
sConsulta = sConsulta & "            WHERE ANYO=@ANYO" & vbCrLf
sConsulta = sConsulta & "              AND GMN1=@GMN1" & vbCrLf
sConsulta = sConsulta & "              AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "              AND PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "              AND OFE=@OFE)" & vbCrLf
sConsulta = sConsulta & "IF @CAM IS NULL" & vbCrLf
sConsulta = sConsulta & "  SELECT @CAM =  EQUIV / @EQUIVPROC" & vbCrLf
sConsulta = sConsulta & "    FROM MON " & vbCrLf
sConsulta = sConsulta & "   WHERE COD = @MON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if @AMBITO = 1" & vbCrLf
sConsulta = sConsulta & "  SELECT * , @EQUIVPROC*@CAM CAMBIO" & vbCrLf
sConsulta = sConsulta & "    FROM OFE_ATRIB  OA " & vbCrLf
sConsulta = sConsulta & "   WHERE OA.ANYO= @ANYO " & vbCrLf
sConsulta = sConsulta & "     AND OA.GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "     AND OA.PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "     AND OA.PROVE = @PROVE " & vbCrLf
sConsulta = sConsulta & "     AND OA.OFE = @OFE" & vbCrLf
sConsulta = sConsulta & "ELSE IF @AMBITO = 2" & vbCrLf
sConsulta = sConsulta & "  SELECT * , @EQUIVPROC*@CAM CAMBIO" & vbCrLf
sConsulta = sConsulta & "    FROM OFE_GR_ATRIB OA " & vbCrLf
sConsulta = sConsulta & "   WHERE OA.ANYO= @ANYO " & vbCrLf
sConsulta = sConsulta & "     AND OA.GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "     AND OA.PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "     AND OA.PROVE = @PROVE " & vbCrLf
sConsulta = sConsulta & "     AND OA.GRUPO = @GRUPO " & vbCrLf
sConsulta = sConsulta & "     AND OA.OFE = @OFE" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE SP_TRASPASAR_OFERTA @ID int , @NUM INT OUTPUT, @NUMNUE INT OUTPUT, @NUMAREV INT OUTPUT, @ERROR SMALLINT = NULL OUTPUT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @NEWOFE INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @NUMOFEWEB AS int" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN1 VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCE INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVE VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @MON VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @ADJUN INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDADJUN INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDGS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @GRUPO VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @ITEM INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @NUMOBJ INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @NUMOBJGS INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @ERROR = 0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @ANYO = ANYO, @GMN1= GMN1, @PROCE = PROCE, @PROVE = PROVE, @MON = MON, @NUMOBJ = NUMOBJ" & vbCrLf
sConsulta = sConsulta & "  FROM P_PROCE_OFE " & vbCrLf
sConsulta = sConsulta & " WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @NUMOBJGS = (SELECT NUMOBJ FROM PROCE_PROVE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE)" & vbCrLf
sConsulta = sConsulta & "IF @NUMOBJGS  <=@NUMOBJ" & vbCrLf
sConsulta = sConsulta & "  UPDATE PROCE_PROVE SET OBJNUEVOS=0 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "/*" & vbCrLf
sConsulta = sConsulta & "DECLARE @EQUIV FLOAT " & vbCrLf
sConsulta = sConsulta & "SELECT @EQUIV = (MON.EQUIV / PROCE.CAMBIO) " & vbCrLf
sConsulta = sConsulta & "  FROM PROCE " & vbCrLf
sConsulta = sConsulta & "     INNER JOIN MON " & vbCrLf
sConsulta = sConsulta & "             ON PROCE.ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "            AND PROCE.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND PROCE.COD=@PROCE  " & vbCrLf
sConsulta = sConsulta & "            AND MON.COD=@MON " & vbCrLf
sConsulta = sConsulta & "*/" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @SUBASTA INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @SUBASTA = SUBASTA " & vbCrLf
sConsulta = sConsulta & "  FROM PROCE_DEF " & vbCrLf
sConsulta = sConsulta & " WHERE ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "   AND GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "   AND PROCE=@PROCE  " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @SUBASTA = 1" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    DECLARE @OFE INT" & vbCrLf
sConsulta = sConsulta & "    SET @OFE = ( SELECT MAX(OFE) FROM PROCE_OFE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE AND OFE!=-1)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    IF @OFE IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "      BEGIN" & vbCrLf
sConsulta = sConsulta & "        INSERT INTO P_OFE_GR_ATRIB (ID, GRUPO, ATRIB_ID,  VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL)" & vbCrLf
sConsulta = sConsulta & "        SELECT @ID, O.GRUPO, O.ATRIB_ID, O.VALOR_NUM, O.VALOR_TEXT, O.VALOR_FEC, O.VALOR_BOOL" & vbCrLf
sConsulta = sConsulta & "          FROM OFE_GR_ATRIB O" & vbCrLf
sConsulta = sConsulta & "         WHERE O.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "           AND O.GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "           AND O.PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "           AND O.PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "           AND O.OFE = @OFE" & vbCrLf
sConsulta = sConsulta & "           AND NOT EXISTS (SELECT * FROM P_OFE_GR_ATRIB T WHERE T.ID = @ID AND T.GRUPO = O.GRUPO AND T.ATRIB_ID = O.ATRIB_ID)" & vbCrLf
sConsulta = sConsulta & "        IF @@ERROR!=0 GOTO ERRORSINTRAN_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "        INSERT INTO P_OFE_ITEM_ATRIB (ID, GRUPO, ITEM, ATRIB_ID,  VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL)" & vbCrLf
sConsulta = sConsulta & "        SELECT @ID, I.GRUPO, IA.ITEM, ATRIB_ID, VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL" & vbCrLf
sConsulta = sConsulta & "          FROM OFE_ITEM_ATRIB IA" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN ITEM I" & vbCrLf
sConsulta = sConsulta & "                   ON I.ANYO = IA.ANYO" & vbCrLf
sConsulta = sConsulta & "                  AND I.GMN1 = IA.GMN1" & vbCrLf
sConsulta = sConsulta & "                  AND I.PROCE = IA.PROCE" & vbCrLf
sConsulta = sConsulta & "                  AND I.ID = IA.ITEM" & vbCrLf
sConsulta = sConsulta & "         WHERE IA.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "           AND IA.GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "           AND IA.PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "           AND IA.PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "           AND IA.OFE = @OFE" & vbCrLf
sConsulta = sConsulta & "           AND NOT EXISTS (SELECT * FROM P_OFE_ITEM_ATRIB T WHERE T.ID = @ID AND T.ATRIB_ID = IA.ATRIB_ID AND T.ITEM = IA.ITEM)" & vbCrLf
sConsulta = sConsulta & "        IF @@ERROR!=0 GOTO ERRORSINTRAN_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "        INSERT INTO P_OFE_ATRIB (ID, ATRIB_ID,  VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL)" & vbCrLf
sConsulta = sConsulta & "        SELECT @ID, O.ATRIB_ID, O.VALOR_NUM, O.VALOR_TEXT, O.VALOR_FEC, O.VALOR_BOOL" & vbCrLf
sConsulta = sConsulta & "          FROM OFE_ATRIB O" & vbCrLf
sConsulta = sConsulta & "         WHERE O.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "           AND O.GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "           AND O.PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "           AND O.PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "           AND O.OFE = @OFE" & vbCrLf
sConsulta = sConsulta & "           AND NOT EXISTS (SELECT * FROM P_OFE_ATRIB T WHERE T.ID = @ID AND T.ATRIB_ID = O.ATRIB_ID)" & vbCrLf
sConsulta = sConsulta & "        IF @@ERROR!=0 GOTO ERRORSINTRAN_" & vbCrLf
sConsulta = sConsulta & "      END " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "      DECLARE @OFEATRIB BIT" & vbCrLf
sConsulta = sConsulta & "      DECLARE @OFEGRUPOATRIB BIT" & vbCrLf
sConsulta = sConsulta & "      DECLARE @OFEITEMATRIB BIT" & vbCrLf
sConsulta = sConsulta & "      DECLARE @PAID INT" & vbCrLf
sConsulta = sConsulta & "      DECLARE @AMBITO SMALLINT" & vbCrLf
sConsulta = sConsulta & "      DECLARE @TIPO SMALLINT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "      DECLARE CATRIBS CURSOR LOCAL FORWARD_ONLY  FOR " & vbCrLf
sConsulta = sConsulta & "      SELECT PA.ID, AMBITO, GRUPO, TIPO" & vbCrLf
sConsulta = sConsulta & "        FROM PROCE_ATRIB PA" & vbCrLf
sConsulta = sConsulta & "         INNER JOIN DEF_ATRIB DA" & vbCrLf
sConsulta = sConsulta & "                 ON PA.ATRIB = DA.ID" & vbCrLf
sConsulta = sConsulta & "       WHERE ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "         AND GMN1= @GMN1" & vbCrLf
sConsulta = sConsulta & "         AND PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "         AND INTERNO = 0" & vbCrLf
sConsulta = sConsulta & "         AND OBLIG = 1" & vbCrLf
sConsulta = sConsulta & "      " & vbCrLf
sConsulta = sConsulta & "      OPEN CATRIBS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "      FETCH NEXT FROM CATRIBS INTO @PAID, @AMBITO, @GRUPO,  @TIPO" & vbCrLf
sConsulta = sConsulta & "      SET @OFEATRIB=0" & vbCrLf
sConsulta = sConsulta & "      SET @OFEGRUPOATRIB=0" & vbCrLf
sConsulta = sConsulta & "      SET @OFEITEMATRIB=0" & vbCrLf
sConsulta = sConsulta & "      WHILE @@FETCH_STATUS=0 AND @OFEATRIB=0 AND @OFEGRUPOATRIB=0 AND @OFEITEMATRIB=0" & vbCrLf
sConsulta = sConsulta & "        BEGIN" & vbCrLf
sConsulta = sConsulta & "          IF @AMBITO = 1" & vbCrLf
sConsulta = sConsulta & "            BEGIN" & vbCrLf
sConsulta = sConsulta & "              SET @OFEATRIB =1" & vbCrLf
sConsulta = sConsulta & "              SELECT @OFEATRIB = CASE " & vbCrLf
sConsulta = sConsulta & "                        WHEN CASE @TIPO " & vbCrLf
sConsulta = sConsulta & "                                WHEN 1 THEN CASE WHEN VALOR_TEXT ='' THEN NULL ELSE VALOR_TEXT END " & vbCrLf
sConsulta = sConsulta & "                                WHEN 2 THEN CAST(VALOR_NUM AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                                WHEN 3 THEN CAST(VALOR_FEC AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                                WHEN 4 THEN CAST(VALOR_BOOL AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                             END IS NULL " & vbCrLf
sConsulta = sConsulta & "                        THEN 1 " & vbCrLf
sConsulta = sConsulta & "                        ELSE 0 " & vbCrLf
sConsulta = sConsulta & "                     END" & vbCrLf
sConsulta = sConsulta & "                FROM P_OFE_ATRIB" & vbCrLf
sConsulta = sConsulta & "               WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "                 AND ATRIB_ID = @PAID" & vbCrLf
sConsulta = sConsulta & "            END" & vbCrLf
sConsulta = sConsulta & "          IF @AMBITO = 2" & vbCrLf
sConsulta = sConsulta & "            BEGIN" & vbCrLf
sConsulta = sConsulta & "              SET @OFEGRUPOATRIB =0" & vbCrLf
sConsulta = sConsulta & "              IF @GRUPO IS NULL " & vbCrLf
sConsulta = sConsulta & "                BEGIN" & vbCrLf
sConsulta = sConsulta & "                IF (SELECT COUNT(*) " & vbCrLf
sConsulta = sConsulta & "                      FROM P_OFE_GR_ATRIB " & vbCrLf
sConsulta = sConsulta & "                     WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "                       AND ATRIB_ID = @PAID" & vbCrLf
sConsulta = sConsulta & "                       AND CASE @TIPO " & vbCrLf
sConsulta = sConsulta & "                                WHEN 1 THEN CASE WHEN VALOR_TEXT ='' THEN NULL ELSE VALOR_TEXT END " & vbCrLf
sConsulta = sConsulta & "                                WHEN 2 THEN CAST(VALOR_NUM AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                                WHEN 3 THEN CAST(VALOR_FEC AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                                WHEN 4 THEN CAST(VALOR_BOOL AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                             END IS NOT NULL) <> (SELECT COUNT(*) " & vbCrLf
sConsulta = sConsulta & "                       FROM PROCE_GRUPO " & vbCrLf
sConsulta = sConsulta & "                      WHERE ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "                        AND GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "                        AND PROCE = @PROCE)" & vbCrLf
sConsulta = sConsulta & "                   SET @OFEGRUPOATRIB = 1" & vbCrLf
sConsulta = sConsulta & "                 END" & vbCrLf
sConsulta = sConsulta & "               ELSE" & vbCrLf
sConsulta = sConsulta & "                IF (SELECT COUNT(*) " & vbCrLf
sConsulta = sConsulta & "                      FROM P_OFE_GR_ATRIB " & vbCrLf
sConsulta = sConsulta & "                     WHERE GRUPO = @GRUPO" & vbCrLf
sConsulta = sConsulta & "                       AND ATRIB_ID = @PAID" & vbCrLf
sConsulta = sConsulta & "                       AND CASE @TIPO " & vbCrLf
sConsulta = sConsulta & "                                WHEN 1 THEN CASE WHEN VALOR_TEXT ='' THEN NULL ELSE VALOR_TEXT END " & vbCrLf
sConsulta = sConsulta & "                                WHEN 2 THEN CAST(VALOR_NUM AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                                WHEN 3 THEN CAST(VALOR_FEC AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                                WHEN 4 THEN CAST(VALOR_BOOL AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                             END IS NOT NULL)  =0 " & vbCrLf
sConsulta = sConsulta & "                       SET @OFEGRUPOATRIB = 1" & vbCrLf
sConsulta = sConsulta & "            END" & vbCrLf
sConsulta = sConsulta & "          IF @AMBITO = 3" & vbCrLf
sConsulta = sConsulta & "            BEGIN" & vbCrLf
sConsulta = sConsulta & "              SET @OFEITEMATRIB =0" & vbCrLf
sConsulta = sConsulta & "              IF @GRUPO IS NULL " & vbCrLf
sConsulta = sConsulta & "                BEGIN" & vbCrLf
sConsulta = sConsulta & "                  IF (SELECT COUNT(*) " & vbCrLf
sConsulta = sConsulta & "                        FROM P_OFE_ITEM_ATRIB " & vbCrLf
sConsulta = sConsulta & "                       WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "                         AND ATRIB_ID = @PAID" & vbCrLf
sConsulta = sConsulta & "                         AND CASE @TIPO " & vbCrLf
sConsulta = sConsulta & "                                WHEN 1 THEN CASE WHEN VALOR_TEXT ='' THEN NULL ELSE VALOR_TEXT END " & vbCrLf
sConsulta = sConsulta & "                                WHEN 2 THEN CAST(VALOR_NUM AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                                WHEN 3 THEN CAST(VALOR_FEC AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                                WHEN 4 THEN CAST(VALOR_BOOL AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                     END IS NOT NULL)  <>" & vbCrLf
sConsulta = sConsulta & "                      (SELECT COUNT(*) " & vbCrLf
sConsulta = sConsulta & "                         FROM ITEM " & vbCrLf
sConsulta = sConsulta & "                        WHERE ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "                          AND GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "                          AND PROCE = @PROCE                    " & vbCrLf
sConsulta = sConsulta & "                          AND CONF = 1)" & vbCrLf
sConsulta = sConsulta & "                       SET @OFEITEMATRIB = 1" & vbCrLf
sConsulta = sConsulta & "                 END" & vbCrLf
sConsulta = sConsulta & "              ELSE" & vbCrLf
sConsulta = sConsulta & "                 begin" & vbCrLf
sConsulta = sConsulta & "                  IF (SELECT COUNT(*) " & vbCrLf
sConsulta = sConsulta & "                        FROM P_OFE_ITEM_ATRIB" & vbCrLf
sConsulta = sConsulta & "                       WHERE GRUPO = @GRUPO" & vbCrLf
sConsulta = sConsulta & "                         AND ID = @ID" & vbCrLf
sConsulta = sConsulta & "                         AND ATRIB_ID = @PAID" & vbCrLf
sConsulta = sConsulta & "                         AND CASE @TIPO " & vbCrLf
sConsulta = sConsulta & "                                WHEN 1 THEN CASE WHEN VALOR_TEXT ='' THEN NULL ELSE VALOR_TEXT END " & vbCrLf
sConsulta = sConsulta & "                                WHEN 2 THEN CAST(VALOR_NUM AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                                WHEN 3 THEN CAST(VALOR_FEC AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                                WHEN 4 THEN CAST(VALOR_BOOL AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                             END IS NOT NULL)  <>" & vbCrLf
sConsulta = sConsulta & "                      (SELECT COUNT(*) " & vbCrLf
sConsulta = sConsulta & "                         FROM ITEM " & vbCrLf
sConsulta = sConsulta & "                        WHERE ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "                          AND GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "                          AND PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "                          AND GRUPO = @GRUPO" & vbCrLf
sConsulta = sConsulta & "                          AND CONF = 1)" & vbCrLf
sConsulta = sConsulta & "                       SET @OFEITEMATRIB = 1" & vbCrLf
sConsulta = sConsulta & "                   end" & vbCrLf
sConsulta = sConsulta & "            END" & vbCrLf
sConsulta = sConsulta & "            FETCH NEXT FROM CATRIBS INTO @PAID, @AMBITO, @GRUPO,  @TIPO" & vbCrLf
sConsulta = sConsulta & "          END" & vbCrLf
sConsulta = sConsulta & "      CLOSE CATRIBS" & vbCrLf
sConsulta = sConsulta & "      DEALLOCATE CATRIBS" & vbCrLf
sConsulta = sConsulta & "      IF @OFEATRIB = 1" & vbCrLf
sConsulta = sConsulta & "        SET @ERROR = 2" & vbCrLf
sConsulta = sConsulta & "      IF @OFEGRUPOATRIB = 1" & vbCrLf
sConsulta = sConsulta & "        SET @ERROR = 3" & vbCrLf
sConsulta = sConsulta & "      IF @OFEITEMATRIB = 1" & vbCrLf
sConsulta = sConsulta & "        SET @ERROR = 4" & vbCrLf
sConsulta = sConsulta & "      IF @ERROR>0" & vbCrLf
sConsulta = sConsulta & "        GOTO ELIMINAR_" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "DECLARE  @ESTWEB VARCHAR(3) " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "SELECT @ESTWEB=ESTWEB FROM PARGEN_DEF INNER JOIN PARGEN_GEST ON PARGEN_DEF.ID=PARGEN_GEST.ID  " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN TRANSACTION" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE SET ULT= 0, LEIDO = 1  WHERE ANYO = @ANYO AND GMN1 = @GMN1 AND PROCE = @PROCE AND PROVE = @PROVE AND ULT = 1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @NEWOFE = ISNULL(MAX(OFE),0) + 1 FROM PROCE_OFE WHERE ANYO = @ANYO AND GMN1 = @GMN1 AND PROCE = @PROCE AND PROVE = @PROVE AND OFE != -1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO PROCE_OFE(ANYO, GMN1, PROCE, PROVE, OFE, EST, FECREC, FECVAL, MON, CAMBIO, OBS, LEIDO, ULT, ENVIADA, OBSADJUN)" & vbCrLf
sConsulta = sConsulta & "SELECT @ANYO, @GMN1, @PROCE, @PROVE, @NEWOFE, @ESTWEB, FECREC, FECVAL, MON, CAMBIO, OBS, 0, 1, 1, OBSADJUN" & vbCrLf
sConsulta = sConsulta & "  FROM P_PROCE_OFE" & vbCrLf
sConsulta = sConsulta & "WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE GRUPO_OFE " & vbCrLf
sConsulta = sConsulta & "   SET OBSADJUN = PGO.OBSADJUN" & vbCrLf
sConsulta = sConsulta & "  FROM GRUPO_OFE GO" & vbCrLf
sConsulta = sConsulta & "      INNER JOIN P_GRUPO_OFE PGO" & vbCrLf
sConsulta = sConsulta & "              ON GO.GRUPO = PGO.GRUPO" & vbCrLf
sConsulta = sConsulta & " WHERE GO.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "   AND GO.GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "   AND GO.PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "   AND GO.PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "   AND GO.OFE = @NEWOFE" & vbCrLf
sConsulta = sConsulta & "   AND PGO.ID = @ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_OFE" & vbCrLf
sConsulta = sConsulta & "   SET PRECIO = PIO.PRECIO," & vbCrLf
sConsulta = sConsulta & "       CANTMAX = PIO.CANTMAX," & vbCrLf
sConsulta = sConsulta & "       PRECIO2 = PIO.PRECIO2," & vbCrLf
sConsulta = sConsulta & "       PRECIO3 = PIO.PRECIO3," & vbCrLf
sConsulta = sConsulta & "       PREC_VALIDO = PIO.PRECIO," & vbCrLf
sConsulta = sConsulta & "       OBSADJUN = PIO.OBSADJUN," & vbCrLf
sConsulta = sConsulta & "       COMENT1 = PIO.COMENT1," & vbCrLf
sConsulta = sConsulta & "       COMENT2 = PIO.COMENT2," & vbCrLf
sConsulta = sConsulta & "       COMENT3 = PIO.COMENT3" & vbCrLf
sConsulta = sConsulta & "  FROM ITEM_OFE IO" & vbCrLf
sConsulta = sConsulta & "     INNER JOIN P_ITEM_OFE PIO" & vbCrLf
sConsulta = sConsulta & "             ON IO.ITEM = PIO.ITEM" & vbCrLf
sConsulta = sConsulta & " WHERE IO.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "   AND IO.GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "   AND IO.PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "   AND IO.PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "   AND IO.NUM = @NEWOFE" & vbCrLf
sConsulta = sConsulta & "   AND PIO.ID = @ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_ITEM_ADJUN " & vbCrLf
sConsulta = sConsulta & "  FROM OFE_ITEM_ADJUN R" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN P_ITEM_OFE PIO" & vbCrLf
sConsulta = sConsulta & "               ON PIO.ID = @ID" & vbCrLf
sConsulta = sConsulta & "              AND R.ITEM = PIO.ITEM" & vbCrLf
sConsulta = sConsulta & " WHERE R.ANYO = @ANYO " & vbCrLf
sConsulta = sConsulta & "   AND R.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "   AND R.PROCE= @PROCE " & vbCrLf
sConsulta = sConsulta & "   AND R.PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "   AND R.OFE IN (@NEWOFE,-1)" & vbCrLf
sConsulta = sConsulta & "   AND PIO.ADJUN = 1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_ITEM_ATRIB " & vbCrLf
sConsulta = sConsulta & "  FROM OFE_ITEM_ATRIB R" & vbCrLf
sConsulta = sConsulta & " WHERE R.ANYO = @ANYO " & vbCrLf
sConsulta = sConsulta & "   AND R.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "   AND R.PROCE= @PROCE " & vbCrLf
sConsulta = sConsulta & "   AND R.PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "   AND R.OFE IN (@NEWOFE,-1)" & vbCrLf
sConsulta = sConsulta & "   AND EXISTS (SELECT * FROM P_OFE_ITEM_ATRIB P " & vbCrLf
sConsulta = sConsulta & "                WHERE P.ID  = @ID " & vbCrLf
sConsulta = sConsulta & "                  AND P.ITEM = R.ITEM" & vbCrLf
sConsulta = sConsulta & "                  AND P.ATRIB_ID = R.ATRIB_ID)" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_GR_ATRIB " & vbCrLf
sConsulta = sConsulta & "  FROM OFE_GR_ATRIB R" & vbCrLf
sConsulta = sConsulta & " WHERE R.ANYO = @ANYO " & vbCrLf
sConsulta = sConsulta & "   AND R.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "   AND R.PROCE= @PROCE " & vbCrLf
sConsulta = sConsulta & "   AND R.PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "   AND R.OFE IN (@NEWOFE,-1)" & vbCrLf
sConsulta = sConsulta & "   AND EXISTS (SELECT * FROM P_OFE_GR_ATRIB P " & vbCrLf
sConsulta = sConsulta & "                WHERE P.ID  = @ID " & vbCrLf
sConsulta = sConsulta & "                  AND CAST(P.GRUPO AS VARCHAR(50)) = CAST(R.GRUPO AS VARCHAR(50))" & vbCrLf
sConsulta = sConsulta & "                  AND P.ATRIB_ID = R.ATRIB_ID)" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_GR_ADJUN " & vbCrLf
sConsulta = sConsulta & "  FROM OFE_GR_ADJUN R" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN P_GRUPO_OFE PGO" & vbCrLf
sConsulta = sConsulta & "            ON PGO.ID = @ID" & vbCrLf
sConsulta = sConsulta & "           AND PGO.GRUPO = R.GRUPO" & vbCrLf
sConsulta = sConsulta & " WHERE R.ANYO = @ANYO " & vbCrLf
sConsulta = sConsulta & "   AND R.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "   AND R.PROCE= @PROCE " & vbCrLf
sConsulta = sConsulta & "   AND R.PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "   AND R.OFE IN (@NEWOFE,-1)" & vbCrLf
sConsulta = sConsulta & "   AND PGO.ADJUN = 1" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_ATRIB " & vbCrLf
sConsulta = sConsulta & "  FROM OFE_ATRIB R" & vbCrLf
sConsulta = sConsulta & " WHERE R.ANYO = @ANYO " & vbCrLf
sConsulta = sConsulta & "   AND R.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "   AND R.PROCE= @PROCE " & vbCrLf
sConsulta = sConsulta & "   AND R.PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "   AND R.OFE IN (@NEWOFE,-1)" & vbCrLf
sConsulta = sConsulta & "   AND EXISTS (SELECT * FROM P_OFE_ATRIB P " & vbCrLf
sConsulta = sConsulta & "                WHERE P.ID  = @ID " & vbCrLf
sConsulta = sConsulta & "                  AND P.ATRIB_ID = R.ATRIB_ID)" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF (SELECT ADJUN FROM P_PROCE_OFE WHERE ID = @ID) = 1" & vbCrLf
sConsulta = sConsulta & "DELETE FROM PROCE_OFE_ADJUN " & vbCrLf
sConsulta = sConsulta & "  FROM PROCE_OFE_ADJUN R" & vbCrLf
sConsulta = sConsulta & " WHERE R.ANYO = @ANYO " & vbCrLf
sConsulta = sConsulta & "   AND R.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "   AND R.PROCE= @PROCE " & vbCrLf
sConsulta = sConsulta & "   AND R.PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "   AND R.OFE IN (@NEWOFE,-1)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM PROCE_OFE_ADJUN WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE= @PROCE and PROVE=@PROVE AND OFE = -1" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_GR_ADJUN WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE= @PROCE and PROVE=@PROVE AND OFE = -1" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_ITEM_ADJUN WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE= @PROCE and PROVE=@PROVE AND OFE = -1" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_ATRIB WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE= @PROCE and PROVE=@PROVE AND OFE = -1" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_GR_ATRIB WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE= @PROCE and PROVE=@PROVE AND OFE = -1" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_ITEM_ATRIB WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE= @PROCE and PROVE=@PROVE AND OFE = -1" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM ITEM_OFE WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE= @PROCE and PROVE=@PROVE AND NUM = -1" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM GRUPO_OFE WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE= @PROCE and PROVE=@PROVE AND OFE = -1" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM PROCE_OFE WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE= @PROCE and PROVE=@PROVE AND OFE = -1" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR LOCAL FOR" & vbCrLf
sConsulta = sConsulta & "SELECT ADJUN, IDGS FROM P_PROCE_OFE_ADJUN WHERE ID=@ID AND IDGS IS NULL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "OPEN C" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C INTO @ADJUN, @IDGS" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    SELECT @IDADJUN = isnull(MAX(ID),0) + 1 FROM ADJUN " & vbCrLf
sConsulta = sConsulta & "    INSERT INTO ADJUN (ID, WEB) VALUES (@IDADJUN, 1) " & vbCrLf
sConsulta = sConsulta & "    UPDATE P_PROCE_OFE_ADJUN SET IDGS = @IDADJUN WHERE ID = @ID AND ADJUN = @ADJUN" & vbCrLf
sConsulta = sConsulta & "    IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "    FETCH NEXT FROM C INTO @ADJUN, @IDGS" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "CLOSE C" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO PROCE_OFE_ADJUN(ANYO, GMN1, PROCE, PROVE, OFE, ID, NOM, COM, IDPORTAL, DATASIZE)" & vbCrLf
sConsulta = sConsulta & "SELECT @ANYO, @GMN1, @PROCE, @PROVE, @NEWOFE, POA.IDGS, POA.NOM, POA.COM, POA.IDPORTAL, POA.DATASIZE" & vbCrLf
sConsulta = sConsulta & "  FROM P_PROCE_OFE_ADJUN POA" & vbCrLf
sConsulta = sConsulta & "         INNER JOIN ADJUN A" & vbCrLf
sConsulta = sConsulta & "                 ON POA.IDGS = A.ID" & vbCrLf
sConsulta = sConsulta & "WHERE POA.ID = @ID" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR LOCAL FOR" & vbCrLf
sConsulta = sConsulta & "SELECT ADJUN, IDGS, GRUPO FROM P_OFE_GR_ADJUN WHERE ID=@ID AND IDGS IS NULL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "OPEN C" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C INTO @ADJUN, @IDGS, @GRUPO" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    SELECT @IDADJUN = isnull(MAX(ID),0) + 1 FROM ADJUN " & vbCrLf
sConsulta = sConsulta & "    INSERT INTO ADJUN (ID, WEB) VALUES (@IDADJUN, 1) " & vbCrLf
sConsulta = sConsulta & "    UPDATE P_OFE_GR_ADJUN SET IDGS = @IDADJUN WHERE ID = @ID AND ADJUN = @ADJUN AND GRUPO = @GRUPO" & vbCrLf
sConsulta = sConsulta & "    IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "    FETCH NEXT FROM C INTO @ADJUN, @IDGS, @GRUPO" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "CLOSE C" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO OFE_GR_ADJUN(ANYO, GMN1, PROCE, GRUPO, PROVE, OFE, ID, NOM, COM, IDPORTAL, DATASIZE)" & vbCrLf
sConsulta = sConsulta & "SELECT @ANYO, @GMN1, @PROCE, OGA.GRUPO, @PROVE, @NEWOFE, OGA.IDGS, OGA.NOM, OGA.COM, OGA.IDPORTAL, OGA.DATASIZE" & vbCrLf
sConsulta = sConsulta & "  FROM P_OFE_GR_ADJUN OGA" & vbCrLf
sConsulta = sConsulta & "         INNER JOIN ADJUN A" & vbCrLf
sConsulta = sConsulta & "                 ON OGA.IDGS = A.ID" & vbCrLf
sConsulta = sConsulta & " WHERE OGA.ID = @ID" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR LOCAL FOR" & vbCrLf
sConsulta = sConsulta & "SELECT ADJUN, IDGS, ITEM FROM P_OFE_ITEM_ADJUN WHERE ID=@ID AND IDGS IS NULL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "OPEN C" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C INTO @ADJUN, @IDGS, @ITEM" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    SELECT @IDADJUN = isnull(MAX(ID),0) + 1 FROM ADJUN " & vbCrLf
sConsulta = sConsulta & "    INSERT INTO ADJUN (ID, WEB) VALUES (@IDADJUN, 1) " & vbCrLf
sConsulta = sConsulta & "    UPDATE P_OFE_ITEM_ADJUN SET IDGS = @IDADJUN WHERE ID = @ID AND ADJUN = @ADJUN AND ITEM = @ITEM" & vbCrLf
sConsulta = sConsulta & "    IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "    FETCH NEXT FROM C INTO @ADJUN, @IDGS, @ITEM" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "CLOSE C" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO OFE_ITEM_ADJUN(ANYO, GMN1, PROCE, ITEM, PROVE, OFE, ID, NOM, COM, IDPORTAL, DATASIZE)" & vbCrLf
sConsulta = sConsulta & "SELECT @ANYO, @GMN1, @PROCE, OIA.ITEM, @PROVE, @NEWOFE, OIA.IDGS, OIA.NOM, OIA.COM, OIA.IDPORTAL, OIA.DATASIZE" & vbCrLf
sConsulta = sConsulta & "  FROM P_OFE_ITEM_ADJUN OIA" & vbCrLf
sConsulta = sConsulta & "         INNER JOIN ADJUN A" & vbCrLf
sConsulta = sConsulta & "                 ON OIA.IDGS = A.ID" & vbCrLf
sConsulta = sConsulta & "WHERE OIA.ID = @ID" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO OFE_GR_ATRIB(ANYO, GMN1, PROCE, GRUPO, PROVE, OFE, ATRIB_ID, VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL)" & vbCrLf
sConsulta = sConsulta & "SELECT @ANYO, @GMN1, @PROCE, GRUPO, @PROVE, @NEWOFE, ATRIB_ID, VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL" & vbCrLf
sConsulta = sConsulta & "  FROM P_OFE_GR_ATRIB" & vbCrLf
sConsulta = sConsulta & " WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO OFE_ITEM_ATRIB(ANYO, GMN1, PROCE, ITEM, PROVE, OFE, ATRIB_ID, VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL)" & vbCrLf
sConsulta = sConsulta & "SELECT @ANYO, @GMN1, @PROCE, ITEM, @PROVE, @NEWOFE, ATRIB_ID, VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL" & vbCrLf
sConsulta = sConsulta & "  FROM P_OFE_ITEM_ATRIB" & vbCrLf
sConsulta = sConsulta & " WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO OFE_ATRIB(ANYO, GMN1, PROCE, PROVE, OFE, ATRIB_ID, VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL)" & vbCrLf
sConsulta = sConsulta & "SELECT @ANYO, @GMN1, @PROCE, @PROVE, @NEWOFE, ATRIB_ID, VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL" & vbCrLf
sConsulta = sConsulta & "  FROM P_OFE_ATRIB " & vbCrLf
sConsulta = sConsulta & " WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE SET OFE = 1  WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE = @PROCE AND PROVE=@PROVE AND OFE = 0" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @NUMOFEWEB = isnull(MAX(OFEWEB),0)+1 FROM PROCE_OFE_WEB WHERE ANYO = @ANYO AND GMN1 = @GMN1 AND PROCE = @PROCE AND PROVE = @PROVE " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO PROCE_OFE_WEB (ANYO, GMN1, PROCE, PROVE, OFEWEB, OFE, FECREC, FECVAL, MON)" & vbCrLf
sConsulta = sConsulta & "SELECT ANYO, GMN1, PROCE, PROVE, @NUMOFEWEB, OFE, FECREC,FECVAL,MON " & vbCrLf
sConsulta = sConsulta & "FROM PROCE_OFE " & vbCrLf
sConsulta = sConsulta & "WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE = @PROCE AND PROVE = @PROVE AND OFE = @NEWOFE" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO ITEM_OFE_WEB (ANYO, GMN1, PROCE, ITEM, PROVE, NUM, PRECIO, CANTMAX)" & vbCrLf
sConsulta = sConsulta & "SELECT ANYO, GMN1, PROCE, ITEM, PROVE, @NUMOFEWEB, PRECIO, CANTMAX" & vbCrLf
sConsulta = sConsulta & "FROM ITEM_OFE" & vbCrLf
sConsulta = sConsulta & "WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE = @PROCE AND PROVE = @PROVE AND NUM = @NEWOFE" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO PROCE_OFE_WEB_ADJUN (ANYO, GMN1, PROCE, PROVE, OFEWEB, ID, NOM, COM, IDPORTAL)" & vbCrLf
sConsulta = sConsulta & "SELECT ANYO, GMN1, PROCE, PROVE, @NUMOFEWEB, ID, NOM, COM, IDPORTAL" & vbCrLf
sConsulta = sConsulta & "FROM PROCE_OFE_ADJUN" & vbCrLf
sConsulta = sConsulta & "WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE = @PROCE AND PROVE = @PROVE AND OFE = @NEWOFE" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_OFE SET ULT = 0 WHERE ANYO = @ANYO AND GMN1 = @GMN1 AND PROCE =@PROCE AND PROVE=@PROVE AND NUM!=@NEWOFE" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "COMMIT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "GOTO ELIMINAR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ERROR_:" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @ERROR = 100" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ROLLBACK" & vbCrLf
sConsulta = sConsulta & "GOTO ELIMINAR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ERRORSINTRAN_:" & vbCrLf
sConsulta = sConsulta & "SET @ERROR = 200" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ELIMINAR_:" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC NUM_PROCE_PUB @PROVE =@PROVE ,@NUM = @NUM OUTPUT,@NUMNUE = @NUMNUE OUTPUT ,@NUMAREV = @NUMAREV OUTPUT " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM P_PROCE_OFE WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM P_GRUPO_OFE WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM P_ITEM_OFE WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM P_OFE_ATRIB WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM P_OFE_GR_ATRIB WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM P_OFE_ITEM_ATRIB WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM P_PROCE_OFE_ADJUN WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM P_OFE_GR_ADJUN WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM P_OFE_ITEM_ADJUN WHERE ID=@ID" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta



End Sub

Private Sub V_2_11_5_Storeds_11()
Dim sConsulta As String

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_MODIF_MON_OFE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_MODIF_MON_OFE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_NUEVA_OFERTA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_NUEVA_OFERTA]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE  SP_MODIF_MON_OFE @ANYO SMALLINT,@GMN1 VARCHAR(50), @PROCE INT,@PROVE VARCHAR(50) ,@OFE INT,@CAMANT FLOAT, @CAMBIO FLOAT AS " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   UPDATE ITEM_OFE " & vbCrLf
sConsulta = sConsulta & "           SET PRECIO=(PRECIO/@CAMANT)*@CAMBIO, " & vbCrLf
sConsulta = sConsulta & "               PREC_VALIDO=(PREC_VALIDO/@CAMANT)*@CAMBIO, " & vbCrLf
sConsulta = sConsulta & "               PRECIO2= (PRECIO2/@CAMANT)*@CAMBIO, " & vbCrLf
sConsulta = sConsulta & "               PRECIO3 = (PRECIO3/@CAMANT)*@CAMBIO" & vbCrLf
sConsulta = sConsulta & "     WHERE ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "         AND GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "         AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "         AND PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "         AND NUM=@OFE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   UPDATE OFE_ATRIB " & vbCrLf
sConsulta = sConsulta & "           SET VALOR_NUM=(VALOR_NUM/@CAMANT)*@CAMBIO" & vbCrLf
sConsulta = sConsulta & "   FROM OFE_ATRIB INNER JOIN PROCE_ATRIB ON OFE_ATRIB.ATRIB_ID=PROCE_ATRIB.ID AND PROCE_ATRIB.OP_PREC IN ('+','-')" & vbCrLf
sConsulta = sConsulta & "    WHERE OFE_ATRIB.ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "         AND OFE_ATRIB.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "         AND OFE_ATRIB.PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "         AND OFE_ATRIB.PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "         AND OFE_ATRIB.OFE=@OFE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   UPDATE OFE_GR_ATRIB " & vbCrLf
sConsulta = sConsulta & "           SET VALOR_NUM=(VALOR_NUM/@CAMANT)*@CAMBIO" & vbCrLf
sConsulta = sConsulta & "   FROM OFE_GR_ATRIB INNER JOIN PROCE_ATRIB ON OFE_GR_ATRIB.ATRIB_ID=PROCE_ATRIB.ID AND PROCE_ATRIB.OP_PREC IN ('+','-')" & vbCrLf
sConsulta = sConsulta & "    WHERE OFE_GR_ATRIB.ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "         AND OFE_GR_ATRIB.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "         AND OFE_GR_ATRIB.PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "         AND OFE_GR_ATRIB.PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "         AND OFE_GR_ATRIB.OFE=@OFE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   UPDATE OFE_ITEM_ATRIB " & vbCrLf
sConsulta = sConsulta & "           SET VALOR_NUM=(VALOR_NUM/@CAMANT)*@CAMBIO" & vbCrLf
sConsulta = sConsulta & "   FROM OFE_ITEM_ATRIB INNER JOIN PROCE_ATRIB ON OFE_ITEM_ATRIB.ATRIB_ID=PROCE_ATRIB.ID AND PROCE_ATRIB.OP_PREC IN ('+','-')" & vbCrLf
sConsulta = sConsulta & "    WHERE OFE_ITEM_ATRIB.ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "         AND OFE_ITEM_ATRIB.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "         AND OFE_ITEM_ATRIB.PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "         AND OFE_ITEM_ATRIB.PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "         AND OFE_ITEM_ATRIB.OFE=@OFE" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE SP_NUEVA_OFERTA @ANYO int, @GMN1 varchar(50), @PROCE int, @PROVE varchar(50), @OFE int, @ULT SMALLINT,@MONNEW VARCHAR(50),@CAMBIONEW FLOAT  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @MAXOFE INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @CAMBIOOLD FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @MONOLD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "--  ESTA SP SOLO SE PUEDE LLAMAR DESDE EL TRIGGER DE INSERCION DE PROCE_OFE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @OFE=-1" & vbCrLf
sConsulta = sConsulta & "    SET @MAXOFE = ( SELECT MAX(OFE) FROM PROCE_OFE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE)" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "    SET @MAXOFE = ( SELECT MAX(OFE) FROM PROCE_OFE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE AND OFE < @OFE)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF (@MAXOFE > 0 ) --NO ES LA PRIMERA OFERTA QUE METE EL PROVEEDOR AS� QUE COPIAMOS LA ANTERIOR" & vbCrLf
sConsulta = sConsulta & "      BEGIN" & vbCrLf
sConsulta = sConsulta & "         --    Leo la moneda de la oferta que voy a copiar" & vbCrLf
sConsulta = sConsulta & "         SELECT @MONOLD=MON,@CAMBIOOLD=CAMBIO FROM PROCE_OFE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE AND OFE =@MAXOFE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "        INSERT INTO PROCE_OFE_ADJUN (ANYO, GMN1, PROCE, PROVE, OFE, ID,NOM, COM, IDPORTAL,DATASIZE)" & vbCrLf
sConsulta = sConsulta & "        SELECT ANYO, GMN1, PROCE, PROVE, @OFE, ID,NOM ,COM, IDPORTAL,DATASIZE" & vbCrLf
sConsulta = sConsulta & "          FROM PROCE_OFE_ADJUN" & vbCrLf
sConsulta = sConsulta & "         WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE = @PROVE AND OFE =@MAXOFE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "        INSERT INTO OFE_ATRIB (ANYO,GMN1,PROCE,PROVE,OFE,ATRIB_ID,VALOR_NUM ,VALOR_TEXT ,VALOR_FEC ,VALOR_BOOL ,VALOR_POND)" & vbCrLf
sConsulta = sConsulta & "        SELECT ANYO,GMN1,PROCE,PROVE,@OFE,ATRIB_ID,VALOR_NUM ,VALOR_TEXT ,VALOR_FEC ,VALOR_BOOL ,VALOR_POND " & vbCrLf
sConsulta = sConsulta & "          FROM OFE_ATRIB " & vbCrLf
sConsulta = sConsulta & "         WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE = @PROVE AND OFE =@MAXOFE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "        INSERT INTO GRUPO_OFE (ANYO,GMN1,PROCE,GRUPO,PROVE,OFE,OBSADJUN)" & vbCrLf
sConsulta = sConsulta & "        SELECT ANYO,GMN1,PROCE,GRUPO,PROVE,@OFE,OBSADJUN" & vbCrLf
sConsulta = sConsulta & "          FROM GRUPO_OFE" & vbCrLf
sConsulta = sConsulta & "         WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE AND OFE=@MAXOFE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "        INSERT INTO OFE_GR_ADJUN (ANYO,GMN1,PROCE,GRUPO,PROVE,OFE,ID,NOM ,COM ,IDPORTAL,DATASIZE)" & vbCrLf
sConsulta = sConsulta & "        SELECT ANYO,GMN1,PROCE,GRUPO,PROVE,@OFE,ID,NOM ,COM ,IDPORTAL,DATASIZE" & vbCrLf
sConsulta = sConsulta & "          FROM OFE_GR_ADJUN" & vbCrLf
sConsulta = sConsulta & "         WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE = @PROVE AND OFE =@MAXOFE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "        INSERT INTO OFE_GR_ATRIB (ANYO,GMN1,PROCE,GRUPO,PROVE,OFE,ATRIB_ID,VALOR_NUM ,VALOR_TEXT ,VALOR_FEC ,VALOR_BOOL ,VALOR_POND )" & vbCrLf
sConsulta = sConsulta & "        SELECT ANYO,GMN1,PROCE,GRUPO,PROVE,@OFE,ATRIB_ID,VALOR_NUM ,VALOR_TEXT ,VALOR_FEC ,VALOR_BOOL ,VALOR_POND " & vbCrLf
sConsulta = sConsulta & "          FROM OFE_GR_ATRIB" & vbCrLf
sConsulta = sConsulta & "         WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE AND OFE=@MAXOFE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "        IF @ULT = 1 " & vbCrLf
sConsulta = sConsulta & "               UPDATE ITEM_OFE SET ULT=0 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE AND NUM=@MAXOFE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--        IF @MONOLD = @MONNEW " & vbCrLf
sConsulta = sConsulta & "            INSERT INTO ITEM_OFE (ANYO,GMN1,PROCE,ITEM,PROVE,NUM,PRECIO,PRECIO2,PRECIO3,USAR,CANTMAX,OBSADJUN,ULT,COMENT1,COMENT2,COMENT3)" & vbCrLf
sConsulta = sConsulta & "            SELECT ANYO,GMN1,PROCE,ITEM,PROVE,@OFE,PRECIO,PRECIO2,PRECIO3,USAR,CANTMAX,OBSADJUN,@ULT,COMENT1,COMENT2,COMENT3" & vbCrLf
sConsulta = sConsulta & "             FROM ITEM_OFE" & vbCrLf
sConsulta = sConsulta & "             WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE AND NUM=@MAXOFE" & vbCrLf
sConsulta = sConsulta & "/*        ELSE" & vbCrLf
sConsulta = sConsulta & "            INSERT INTO ITEM_OFE (ANYO,GMN1,PROCE,ITEM,PROVE,NUM,PRECIO,PRECIO2,PRECIO3,USAR,CANTMAX,OBSADJUN,ULT)" & vbCrLf
sConsulta = sConsulta & "            SELECT ANYO,GMN1,PROCE,ITEM,PROVE,@OFE,((PRECIO/@CAMBIOOLD)*@CAMBIONEW),((PRECIO2/@CAMBIOOLD)*@CAMBIONEW),((PRECIO3/@CAMBIOOLD)*@CAMBIONEW),USAR,CANTMAX,OBSADJUN,@ULT" & vbCrLf
sConsulta = sConsulta & "             FROM ITEM_OFE" & vbCrLf
sConsulta = sConsulta & "             WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE AND NUM=@MAXOFE" & vbCrLf
sConsulta = sConsulta & "*/" & vbCrLf
sConsulta = sConsulta & "        INSERT INTO OFE_ITEM_ADJUN (ANYO,GMN1,PROCE,ITEM,PROVE,OFE,ID,NOM ,COM ,IDPORTAL,DATASIZE)" & vbCrLf
sConsulta = sConsulta & "        SELECT ANYO,GMN1,PROCE,ITEM,PROVE,@OFE,ID,NOM ,COM ,IDPORTAL,DATASIZE" & vbCrLf
sConsulta = sConsulta & "          FROM OFE_ITEM_ADJUN" & vbCrLf
sConsulta = sConsulta & "         WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE AND OFE=@MAXOFE" & vbCrLf
sConsulta = sConsulta & "        " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "        INSERT INTO OFE_ITEM_ATRIB (ANYO,GMN1,PROCE,ITEM,PROVE,OFE,ATRIB_ID,VALOR_NUM ,VALOR_TEXT ,VALOR_FEC ,VALOR_BOOL ,VALOR_POND)" & vbCrLf
sConsulta = sConsulta & "        SELECT ANYO,GMN1,PROCE,ITEM,PROVE,@OFE,ATRIB_ID,VALOR_NUM ,VALOR_TEXT ,VALOR_FEC ,VALOR_BOOL ,VALOR_POND" & vbCrLf
sConsulta = sConsulta & "          FROM OFE_ITEM_ATRIB" & vbCrLf
sConsulta = sConsulta & "         WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE AND OFE=@MAXOFE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "        IF @MONOLD <> @MONNEW " & vbCrLf
sConsulta = sConsulta & "            EXEC SP_MODIF_MON_OFE @ANYO, @GMN1, @PROCE, @PROVE, @OFE, @CAMBIOOLD,@CAMBIONEW" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    ELSE" & vbCrLf
sConsulta = sConsulta & "      BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "        INSERT INTO GRUPO_OFE (ANYO,GMN1,PROCE,GRUPO,PROVE,OFE)" & vbCrLf
sConsulta = sConsulta & "        SELECT @ANYO,@GMN1,@PROCE,COD, @PROVE,@OFE" & vbCrLf
sConsulta = sConsulta & "          FROM PROCE_GRUPO" & vbCrLf
sConsulta = sConsulta & "         WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "        INSERT INTO ITEM_OFE (ANYO,GMN1,PROCE,ITEM,PROVE,NUM,PRECIO,PRECIO2,PRECIO3,CANTMAX,ULT,FECACT)" & vbCrLf
sConsulta = sConsulta & "        SELECT @ANYO,@GMN1,@PROCE,ID,@PROVE,@OFE,NULL,NULL,NULL,NULL,@ULT,NULL" & vbCrLf
sConsulta = sConsulta & "          FROM ITEM" & vbCrLf
sConsulta = sConsulta & "         WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_CALCULAR_PREC_VALIDO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_CALCULAR_PREC_VALIDO]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_CALCULAR_PREC_VALIDO @ANYO int, @GMN1 varchar(50), @PROCE int, @PROVE varchar(50) = NULL output, @OFE int = NULL output AS" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @GRUPO varchar(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @ITEM INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @VALOR FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @OP varchar(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PARAM NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "IF @PROVE IS NULL" & vbCrLf
sConsulta = sConsulta & "  DECLARE CUR_PROVE CURSOR LOCAL FORWARD_ONLY FOR " & vbCrLf
sConsulta = sConsulta & "   SELECT PROVE,OFE FROM PROCE_OFE WHERE ANYO=@ANYO AND GMN1 =@GMN1 AND PROCE = @PROCE AND ULT = 1 AND ENVIADA = 1" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  IF @OFE IS NULL" & vbCrLf
sConsulta = sConsulta & "    DECLARE CUR_PROVE CURSOR LOCAL FORWARD_ONLY FOR" & vbCrLf
sConsulta = sConsulta & "      SELECT @PROVE PROVE, OFE" & vbCrLf
sConsulta = sConsulta & "        FROM PROCE_OFE" & vbCrLf
sConsulta = sConsulta & "       WHERE ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "         AND GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "         AND PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "         AND PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "         AND ULT = 1 " & vbCrLf
sConsulta = sConsulta & "         AND ENVIADA = 1" & vbCrLf
sConsulta = sConsulta & "  ELSE" & vbCrLf
sConsulta = sConsulta & "    DECLARE CUR_PROVE CURSOR LOCAL FORWARD_ONLY FOR" & vbCrLf
sConsulta = sConsulta & "      SELECT @PROVE PROVE, @OFE OFE" & vbCrLf
sConsulta = sConsulta & "OPEN CUR_PROVE" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM CUR_PROVE INTO @PROVE, @OFE" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_OFE SET PREC_VALIDO = CASE USAR WHEN 1 THEN PRECIO WHEN 2 THEN PRECIO2 WHEN 3 THEN PRECIO3 END" & vbCrLf
sConsulta = sConsulta & " WHERE ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "   AND GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "   AND PROCE =@PROCE" & vbCrLf
sConsulta = sConsulta & "   AND PROVE =@PROVE" & vbCrLf
sConsulta = sConsulta & "   AND NUM = @OFE" & vbCrLf
sConsulta = sConsulta & "DECLARE CUR_ITEMS_OFE CURSOR LOCAL FORWARD_ONLY  FOR" & vbCrLf
sConsulta = sConsulta & "SELECT ID ITEM, OP_PREC, VALOR_NUM FROM " & vbCrLf
sConsulta = sConsulta & "  (SELECT IO.ID, PA.OP_PREC, OA.VALOR_NUM, PA.ORDEN" & vbCrLf
sConsulta = sConsulta & "     FROM ITEM IO" & vbCrLf
sConsulta = sConsulta & "          CROSS JOIN OFE_ATRIB OA" & vbCrLf
sConsulta = sConsulta & "          INNER JOIN PROCE_ATRIB PA" & vbCrLf
sConsulta = sConsulta & "                  ON OA.ANYO = PA.ANYO" & vbCrLf
sConsulta = sConsulta & "                 AND OA.GMN1 = PA.GMN1" & vbCrLf
sConsulta = sConsulta & "                 AND OA.PROCE = PA.PROCE" & vbCrLf
sConsulta = sConsulta & "                 AND OA.ATRIB_ID = PA.ID" & vbCrLf
sConsulta = sConsulta & "                 AND OA.PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "                 AND OA.OFE = @OFE" & vbCrLf
sConsulta = sConsulta & "   WHERE IO.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "     AND IO.GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "     AND IO.PROCE =@PROCE" & vbCrLf
sConsulta = sConsulta & "     AND OA.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "     AND OA.GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "     AND OA.PROCE =@PROCE" & vbCrLf
sConsulta = sConsulta & "     AND OA.PROVE =@PROVE" & vbCrLf
sConsulta = sConsulta & "     AND OA.OFE = @OFE" & vbCrLf
sConsulta = sConsulta & "     AND PA.APLIC_PREC = 3" & vbCrLf
sConsulta = sConsulta & "     AND PA.USAR_PREC = 1" & vbCrLf
sConsulta = sConsulta & "   UNION" & vbCrLf
sConsulta = sConsulta & "SELECT  IO.ID, PA.OP_PREC, OGA.VALOR_NUM, PA.ORDEN" & vbCrLf
sConsulta = sConsulta & "  FROM ITEM IO" & vbCrLf
sConsulta = sConsulta & "        CROSS JOIN OFE_GR_ATRIB OGA" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN PROCE_ATRIB PA" & vbCrLf
sConsulta = sConsulta & "                ON OGA.ANYO = PA.ANYO" & vbCrLf
sConsulta = sConsulta & "               AND OGA.GMN1 = PA.GMN1" & vbCrLf
sConsulta = sConsulta & "               AND OGA.PROCE = PA.PROCE" & vbCrLf
sConsulta = sConsulta & "               AND OGA.ATRIB_ID = PA.ID" & vbCrLf
sConsulta = sConsulta & "               AND OGA.PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "               AND OGA.OFE = @OFE" & vbCrLf
sConsulta = sConsulta & " WHERE IO.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "   AND IO.GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "   AND IO.PROCE =@PROCE" & vbCrLf
sConsulta = sConsulta & "   AND IO.GRUPO = OGA.GRUPO" & vbCrLf
sConsulta = sConsulta & "   AND OGA.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "   AND OGA.GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "   AND OGA.PROCE =@PROCE" & vbCrLf
sConsulta = sConsulta & "   AND OGA.PROVE =@PROVE" & vbCrLf
sConsulta = sConsulta & "   AND OGA.OFE = @OFE" & vbCrLf
sConsulta = sConsulta & "   AND PA.APLIC_PREC = 3" & vbCrLf
sConsulta = sConsulta & "   AND PA.USAR_PREC = 1" & vbCrLf
sConsulta = sConsulta & "UNION" & vbCrLf
sConsulta = sConsulta & "    SELECT  IO.ITEM, PA.OP_PREC , OIA.VALOR_NUM , PA.ORDEN" & vbCrLf
sConsulta = sConsulta & "      FROM ITEM_OFE IO" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN OFE_ITEM_ATRIB OIA" & vbCrLf
sConsulta = sConsulta & "                ON IO.ANYO = OIA.ANYO" & vbCrLf
sConsulta = sConsulta & "               AND IO.GMN1 = OIA.GMN1" & vbCrLf
sConsulta = sConsulta & "               AND IO.PROCE = OIA.PROCE" & vbCrLf
sConsulta = sConsulta & "               AND IO.ITEM = OIA.ITEM" & vbCrLf
sConsulta = sConsulta & "               AND IO.PROVE = OIA.PROVE" & vbCrLf
sConsulta = sConsulta & "               AND IO.NUM = OIA.OFE" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN PROCE_ATRIB PA" & vbCrLf
sConsulta = sConsulta & "                ON OIA.ANYO = PA.ANYO" & vbCrLf
sConsulta = sConsulta & "               AND OIA.GMN1 = PA.GMN1" & vbCrLf
sConsulta = sConsulta & "               AND OIA.PROCE = PA.PROCE" & vbCrLf
sConsulta = sConsulta & "               AND OIA.ATRIB_ID = PA.ID" & vbCrLf
sConsulta = sConsulta & "               AND OIA.PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "               AND OIA.OFE = @OFE" & vbCrLf
sConsulta = sConsulta & "     WHERE OIA.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "       AND OIA.GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "       AND OIA.PROCE =@PROCE" & vbCrLf
sConsulta = sConsulta & "       AND OIA.PROVE =@PROVE" & vbCrLf
sConsulta = sConsulta & "       AND OIA.OFE = @OFE" & vbCrLf
sConsulta = sConsulta & "       AND PA.AMBITO = 3" & vbCrLf
sConsulta = sConsulta & "       AND PA.APLIC_PREC = 3" & vbCrLf
sConsulta = sConsulta & "       AND PA.USAR_PREC = 1" & vbCrLf
sConsulta = sConsulta & ") PORITEM" & vbCrLf
sConsulta = sConsulta & "ORDER BY ORDEN" & vbCrLf
sConsulta = sConsulta & "OPEN CUR_ITEMS_OFE" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM CUR_ITEMS_OFE INTO @ITEM, @OP, @VALOR" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    SELECT  @SQL='" & vbCrLf
sConsulta = sConsulta & "       UPDATE ITEM_OFE " & vbCrLf
sConsulta = sConsulta & "          SET PREC_VALIDO=IO.PREC_VALIDO ' + " & vbCrLf
sConsulta = sConsulta & "        CASE WHEN @OP IN ('*','/') THEN @OP + ' isnull(@VALOR,1) ' " & vbCrLf
sConsulta = sConsulta & "        WHEN @OP IN ('+','-') THEN @OP + ' isnull(@VALOR,0) ' " & vbCrLf
sConsulta = sConsulta & "        WHEN @OP='+%' THEN '+  IO.PREC_VALIDO * isnull(@VALOR,0)/100 ' " & vbCrLf
sConsulta = sConsulta & "        WHEN @OP='-%' THEN '-  IO.PREC_VALIDO * isnull(@VALOR,0)/100 ' " & vbCrLf
sConsulta = sConsulta & "        END  + '" & vbCrLf
sConsulta = sConsulta & "         FROM ITEM_OFE IO" & vbCrLf
sConsulta = sConsulta & "        WHERE IO.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "          AND IO.GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "          AND IO.PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "          AND IO.ITEM = @ITEM" & vbCrLf
sConsulta = sConsulta & "          AND IO.PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "          AND IO.NUM = @OFE'" & vbCrLf
sConsulta = sConsulta & "    SET @PARAM ='@ANYO INT, @GMN1 VARCHAR(50), @PROCE INT, @ITEM INT, @PROVE VARCHAR(50), @OFE INT, @VALOR FLOAT'" & vbCrLf
sConsulta = sConsulta & "    EXEC SP_EXECUTESQL @SQL, @PARAM , @ANYO=@ANYO, @GMN1=@GMN1, @PROCE = @PROCE, @ITEM = @ITEM, @PROVE = @PROVE, @OFE =@OFE, @VALOR =@VALOR" & vbCrLf
sConsulta = sConsulta & "    FETCH NEXT FROM CUR_ITEMS_OFE INTO @ITEM, @OP, @VALOR" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "CLOSE CUR_ITEMS_OFE" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE CUR_ITEMS_OFE" & vbCrLf
sConsulta = sConsulta & "  SELECT I.GRUPO GRUPO, SUM(I.PRES * PO.CAMBIO) PRES INTO #TMP" & vbCrLf
sConsulta = sConsulta & "                      FROM ITEM I" & vbCrLf
sConsulta = sConsulta & "                             INNER JOIN ITEM_OFE IO " & vbCrLf
sConsulta = sConsulta & "                                     ON IO.ANYO = I.ANYO" & vbCrLf
sConsulta = sConsulta & "                                    AND IO.GMN1 = I.GMN1 AND IO.PROCE = I.PROCE" & vbCrLf
sConsulta = sConsulta & "                                    AND IO.ITEM = I.ID" & vbCrLf
sConsulta = sConsulta & "                             INNER JOIN PROCE_OFE PO " & vbCrLf
sConsulta = sConsulta & "                                     ON IO.ANYO = PO.ANYO" & vbCrLf
sConsulta = sConsulta & "                                    AND IO.GMN1 = PO.GMN1 AND IO.PROCE = PO.PROCE" & vbCrLf
sConsulta = sConsulta & "                                    AND IO.PROVE =PO.PROVE AND IO.NUM=PO.OFE" & vbCrLf
sConsulta = sConsulta & "                     WHERE IO.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "                       AND IO.GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "                       AND IO.PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "                       AND IO.PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "                       AND IO.NUM = @OFE AND I.CONF=1" & vbCrLf
sConsulta = sConsulta & "                  GROUP BY I.GRUPO" & vbCrLf
sConsulta = sConsulta & "  UPDATE GRUPO_OFE " & vbCrLf
sConsulta = sConsulta & "     SET   ABIERTO = PRES" & vbCrLf
sConsulta = sConsulta & "    FROM GRUPO_OFE GRO" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN #TMP AS PP " & vbCrLf
sConsulta = sConsulta & "               ON GRO.GRUPO = PP.GRUPO" & vbCrLf
sConsulta = sConsulta & "   WHERE ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "     AND GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "     AND PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "     AND PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "     AND OFE = @OFE " & vbCrLf
sConsulta = sConsulta & "  DROP TABLE #TMP" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM CUR_PROVE INTO @PROVE, @OFE" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "CLOSE CUR_PROVE" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE CUR_PROVE" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_CALCULAR_PREC_VALIDO_REU]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_CALCULAR_PREC_VALIDO_REU]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "CREATE PROCEDURE SP_CALCULAR_PREC_VALIDO_REU @ANYO int, @GMN1 varchar(50), @PROCE int, @FECREU datetime, @PROVE varchar(50) = NULL output, @OFE int = NULL output AS" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @GRUPO varchar(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @ITEM INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @VALOR FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @OP varchar(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PARAM NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @RES TINYINT" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "SELECT @RES=RES FROM REU_PROCE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND FECHA=@FECREU" & vbCrLf
sConsulta = sConsulta & "IF @RES=0  --Si la REU no tiene resultados inserto los registros para la �ltima oferta en GRUPO_OFE_REU si no existen ya" & vbCrLf
sConsulta = sConsulta & "  INSERT INTO GRUPO_OFE_REU (ANYO, GMN1, PROCE, GRUPO, PROVE,OFE,FECREU)" & vbCrLf
sConsulta = sConsulta & "    SELECT GO.ANYO, GO.GMN1, GO.PROCE, GO.GRUPO, GO.PROVE, GO.OFE, @FECREU" & vbCrLf
sConsulta = sConsulta & "    FROM GRUPO_OFE GO" & vbCrLf
sConsulta = sConsulta & "      INNER JOIN PROCE_OFE PO" & vbCrLf
sConsulta = sConsulta & "            ON GO.ANYO = PO.ANYO" & vbCrLf
sConsulta = sConsulta & "           AND GO.GMN1 = PO.GMN1" & vbCrLf
sConsulta = sConsulta & "           AND GO.PROCE = PO.PROCE" & vbCrLf
sConsulta = sConsulta & "           AND GO.PROVE = PO.PROVE" & vbCrLf
sConsulta = sConsulta & "           AND GO.OFE = PO.OFE          " & vbCrLf
sConsulta = sConsulta & "      WHERE GO.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "      AND GO.GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "      AND GO.PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "      AND NOT EXISTS (SELECT * " & vbCrLf
sConsulta = sConsulta & "                      FROM GRUPO_OFE_REU GOR " & vbCrLf
sConsulta = sConsulta & "                     WHERE GO.ANYO = GOR.ANYO " & vbCrLf
sConsulta = sConsulta & "                      AND GO.GMN1 = GOR.GMN1 " & vbCrLf
sConsulta = sConsulta & "                      AND GO.PROCE = GOR.PROCE " & vbCrLf
sConsulta = sConsulta & "                      AND GO.PROVE = GOR.PROVE " & vbCrLf
sConsulta = sConsulta & "                      AND GOR.FECREU=@FECREU)" & vbCrLf
sConsulta = sConsulta & "     AND PO.ENVIADA = 1 AND PO.ULT=1" & vbCrLf
sConsulta = sConsulta & "IF @PROVE IS NULL" & vbCrLf
sConsulta = sConsulta & "  DECLARE CUR_PROVE CURSOR LOCAL FORWARD_ONLY FOR " & vbCrLf
sConsulta = sConsulta & "   SELECT  GOR.PROVE,max(GOR.OFE ) OFE" & vbCrLf
sConsulta = sConsulta & "     FROM GRUPO_OFE_REU  GOR" & vbCrLf
sConsulta = sConsulta & "   WHERE GOR.ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "     AND GOR.GMN1 =@GMN1 " & vbCrLf
sConsulta = sConsulta & "     AND GOR.PROCE = @PROCE " & vbCrLf
sConsulta = sConsulta & "     AND GOR.FECREU = @FECREU" & vbCrLf
sConsulta = sConsulta & "   GROUP BY GOR.PROVE" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  IF @OFE IS NULL" & vbCrLf
sConsulta = sConsulta & "    DECLARE CUR_PROVE CURSOR LOCAL FORWARD_ONLY FOR" & vbCrLf
sConsulta = sConsulta & "      SELECT  @PROVE PROVE, MAX(GOR.OFE) OFE" & vbCrLf
sConsulta = sConsulta & "     FROM GRUPO_OFE_REU  GOR" & vbCrLf
sConsulta = sConsulta & "   WHERE GOR.ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "     AND GOR.GMN1 =@GMN1 " & vbCrLf
sConsulta = sConsulta & "     AND GOR.PROCE = @PROCE " & vbCrLf
sConsulta = sConsulta & "     AND GOR.FECREU = @FECREU" & vbCrLf
sConsulta = sConsulta & "         AND GOR.PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "   GROUP BY PROVE" & vbCrLf
sConsulta = sConsulta & "  ELSE" & vbCrLf
sConsulta = sConsulta & "    DECLARE CUR_PROVE CURSOR LOCAL FORWARD_ONLY FOR" & vbCrLf
sConsulta = sConsulta & "      SELECT @PROVE PROVE, @OFE OFE" & vbCrLf
sConsulta = sConsulta & "OPEN CUR_PROVE" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM CUR_PROVE INTO @PROVE, @OFE" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_OFE SET PREC_VALIDO = CASE USAR WHEN 1 THEN PRECIO WHEN 2 THEN PRECIO2 WHEN 3 THEN PRECIO3 END" & vbCrLf
sConsulta = sConsulta & " WHERE ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "   AND GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "   AND PROCE =@PROCE" & vbCrLf
sConsulta = sConsulta & "   AND PROVE =@PROVE" & vbCrLf
sConsulta = sConsulta & "   AND NUM = @OFE" & vbCrLf
sConsulta = sConsulta & "DECLARE CUR_ITEMS_OFE CURSOR LOCAL FORWARD_ONLY  FOR" & vbCrLf
sConsulta = sConsulta & "SELECT ID ITEM, OP_PREC, VALOR_NUM FROM " & vbCrLf
sConsulta = sConsulta & "  (SELECT IO.ID, PA.OP_PREC, OA.VALOR_NUM, PA.ORDEN" & vbCrLf
sConsulta = sConsulta & "     FROM ITEM IO" & vbCrLf
sConsulta = sConsulta & "          CROSS JOIN OFE_ATRIB OA" & vbCrLf
sConsulta = sConsulta & "          INNER JOIN PROCE_ATRIB PA" & vbCrLf
sConsulta = sConsulta & "                  ON OA.ANYO = PA.ANYO" & vbCrLf
sConsulta = sConsulta & "                 AND OA.GMN1 = PA.GMN1" & vbCrLf
sConsulta = sConsulta & "                 AND OA.PROCE = PA.PROCE" & vbCrLf
sConsulta = sConsulta & "                 AND OA.ATRIB_ID = PA.ID" & vbCrLf
sConsulta = sConsulta & "                 AND OA.PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "                 AND OA.OFE = @OFE" & vbCrLf
sConsulta = sConsulta & "   WHERE IO.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "     AND IO.GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "     AND IO.PROCE =@PROCE" & vbCrLf
sConsulta = sConsulta & "     AND OA.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "     AND OA.GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "     AND OA.PROCE =@PROCE" & vbCrLf
sConsulta = sConsulta & "     AND OA.PROVE =@PROVE" & vbCrLf
sConsulta = sConsulta & "     AND OA.OFE = @OFE" & vbCrLf
sConsulta = sConsulta & "     AND PA.APLIC_PREC = 3" & vbCrLf
sConsulta = sConsulta & "     AND PA.USAR_PREC = 1" & vbCrLf
sConsulta = sConsulta & "   UNION" & vbCrLf
sConsulta = sConsulta & "SELECT  IO.ID, PA.OP_PREC, OGA.VALOR_NUM, PA.ORDEN" & vbCrLf
sConsulta = sConsulta & "  FROM ITEM IO" & vbCrLf
sConsulta = sConsulta & "        CROSS JOIN OFE_GR_ATRIB OGA" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN PROCE_ATRIB PA" & vbCrLf
sConsulta = sConsulta & "                ON OGA.ANYO = PA.ANYO" & vbCrLf
sConsulta = sConsulta & "               AND OGA.GMN1 = PA.GMN1" & vbCrLf
sConsulta = sConsulta & "               AND OGA.PROCE = PA.PROCE" & vbCrLf
sConsulta = sConsulta & "               AND OGA.ATRIB_ID = PA.ID" & vbCrLf
sConsulta = sConsulta & "               AND OGA.PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "               AND OGA.OFE = @OFE" & vbCrLf
sConsulta = sConsulta & " WHERE IO.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "   AND IO.GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "   AND IO.PROCE =@PROCE" & vbCrLf
sConsulta = sConsulta & "   AND IO.GRUPO = OGA.GRUPO" & vbCrLf
sConsulta = sConsulta & "   AND OGA.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "   AND OGA.GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "   AND OGA.PROCE =@PROCE" & vbCrLf
sConsulta = sConsulta & "   AND OGA.PROVE =@PROVE" & vbCrLf
sConsulta = sConsulta & "   AND OGA.OFE = @OFE" & vbCrLf
sConsulta = sConsulta & "   AND PA.APLIC_PREC = 3" & vbCrLf
sConsulta = sConsulta & "   AND PA.USAR_PREC = 1" & vbCrLf
sConsulta = sConsulta & "UNION" & vbCrLf
sConsulta = sConsulta & "    SELECT  IO.ITEM, PA.OP_PREC , OIA.VALOR_NUM , PA.ORDEN" & vbCrLf
sConsulta = sConsulta & "      FROM ITEM_OFE IO" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN OFE_ITEM_ATRIB OIA" & vbCrLf
sConsulta = sConsulta & "                ON IO.ANYO = OIA.ANYO" & vbCrLf
sConsulta = sConsulta & "               AND IO.GMN1 = OIA.GMN1" & vbCrLf
sConsulta = sConsulta & "               AND IO.PROCE = OIA.PROCE" & vbCrLf
sConsulta = sConsulta & "               AND IO.ITEM = OIA.ITEM" & vbCrLf
sConsulta = sConsulta & "               AND IO.PROVE = OIA.PROVE" & vbCrLf
sConsulta = sConsulta & "               AND IO.NUM = OIA.OFE" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN PROCE_ATRIB PA" & vbCrLf
sConsulta = sConsulta & "                ON OIA.ANYO = PA.ANYO" & vbCrLf
sConsulta = sConsulta & "               AND OIA.GMN1 = PA.GMN1" & vbCrLf
sConsulta = sConsulta & "               AND OIA.PROCE = PA.PROCE" & vbCrLf
sConsulta = sConsulta & "               AND OIA.ATRIB_ID = PA.ID" & vbCrLf
sConsulta = sConsulta & "               AND OIA.PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "               AND OIA.OFE = @OFE" & vbCrLf
sConsulta = sConsulta & "     WHERE OIA.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "       AND OIA.GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "       AND OIA.PROCE =@PROCE" & vbCrLf
sConsulta = sConsulta & "       AND OIA.PROVE =@PROVE" & vbCrLf
sConsulta = sConsulta & "       AND OIA.OFE = @OFE" & vbCrLf
sConsulta = sConsulta & "       AND PA.AMBITO = 3" & vbCrLf
sConsulta = sConsulta & "       AND PA.APLIC_PREC = 3" & vbCrLf
sConsulta = sConsulta & "       AND PA.USAR_PREC = 1" & vbCrLf
sConsulta = sConsulta & ") PORITEM" & vbCrLf
sConsulta = sConsulta & "ORDER BY ORDEN" & vbCrLf
sConsulta = sConsulta & "OPEN CUR_ITEMS_OFE" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM CUR_ITEMS_OFE INTO @ITEM, @OP, @VALOR" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    SELECT  @SQL='" & vbCrLf
sConsulta = sConsulta & "       UPDATE ITEM_OFE " & vbCrLf
sConsulta = sConsulta & "          SET PREC_VALIDO=IO.PREC_VALIDO ' + " & vbCrLf
sConsulta = sConsulta & "        CASE WHEN @OP IN ('*','/') THEN @OP + ' isnull(@VALOR,1) ' " & vbCrLf
sConsulta = sConsulta & "        WHEN @OP IN ('+','-') THEN @OP + ' isnull(@VALOR,0) ' " & vbCrLf
sConsulta = sConsulta & "        WHEN @OP='+%' THEN '+  IO.PREC_VALIDO * isnull(@VALOR,0)/100 ' " & vbCrLf
sConsulta = sConsulta & "        WHEN @OP='-%' THEN '-  IO.PREC_VALIDO * isnull(@VALOR,0)/100 ' " & vbCrLf
sConsulta = sConsulta & "        END  + '" & vbCrLf
sConsulta = sConsulta & "         FROM ITEM_OFE IO" & vbCrLf
sConsulta = sConsulta & "        WHERE IO.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "          AND IO.GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "          AND IO.PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "          AND IO.ITEM = @ITEM" & vbCrLf
sConsulta = sConsulta & "          AND IO.PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "          AND IO.NUM = @OFE'" & vbCrLf
sConsulta = sConsulta & "    SET @PARAM ='@ANYO INT, @GMN1 VARCHAR(50), @PROCE INT, @ITEM INT, @PROVE VARCHAR(50), @OFE INT, @VALOR FLOAT'" & vbCrLf
sConsulta = sConsulta & "    EXEC SP_EXECUTESQL @SQL, @PARAM , @ANYO=@ANYO, @GMN1=@GMN1, @PROCE = @PROCE, @ITEM = @ITEM, @PROVE = @PROVE, @OFE =@OFE, @VALOR =@VALOR" & vbCrLf
sConsulta = sConsulta & "    FETCH NEXT FROM CUR_ITEMS_OFE INTO @ITEM, @OP, @VALOR" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "CLOSE CUR_ITEMS_OFE" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE CUR_ITEMS_OFE" & vbCrLf
sConsulta = sConsulta & "  SELECT I.GRUPO GRUPO,  SUM(IP.PRES/PO.CAMBIO) PRES INTO #TMP" & vbCrLf
sConsulta = sConsulta & "                      FROM ITEM I" & vbCrLf
sConsulta = sConsulta & "                             INNER JOIN ITEM_OFE IO " & vbCrLf
sConsulta = sConsulta & "                                     ON IO.ANYO = I.ANYO" & vbCrLf
sConsulta = sConsulta & "                                    AND IO.GMN1 = I.GMN1" & vbCrLf
sConsulta = sConsulta & "                                    AND IO.PROCE = I.PROCE" & vbCrLf
sConsulta = sConsulta & "                                    AND IO.ITEM = I.ID" & vbCrLf
sConsulta = sConsulta & "                             INNER JOIN ITEM_PRES IP" & vbCrLf
sConsulta = sConsulta & "                                     ON I.ANYO = IP.ANYO" & vbCrLf
sConsulta = sConsulta & "                                    AND I.GMN1 = IP.GMN1" & vbCrLf
sConsulta = sConsulta & "                                    AND I.PROCE = IP.PROCE" & vbCrLf
sConsulta = sConsulta & "                                    AND I.ID = IP.ITEM" & vbCrLf
sConsulta = sConsulta & "                                    AND @FECREU = IP.FECREU" & vbCrLf
sConsulta = sConsulta & "                             INNER JOIN PROCE_OFE PO" & vbCrLf
sConsulta = sConsulta & "                                     ON IO.ANYO = PO.ANYO" & vbCrLf
sConsulta = sConsulta & "                                    AND IO.GMN1 = PO.GMN1" & vbCrLf
sConsulta = sConsulta & "                                    AND IO.PROCE = PO.PROCE" & vbCrLf
sConsulta = sConsulta & "                                    AND IO.PROVE =PO.PROVE" & vbCrLf
sConsulta = sConsulta & "                                    AND IO.NUM=PO.OFE" & vbCrLf
sConsulta = sConsulta & "                     WHERE IO.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "                       AND IO.GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "                       AND IO.PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "                       AND IO.PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "                       AND IO.NUM = @OFE " & vbCrLf
sConsulta = sConsulta & "                  GROUP BY I.GRUPO" & vbCrLf
sConsulta = sConsulta & "  UPDATE GRUPO_OFE_REU" & vbCrLf
sConsulta = sConsulta & "     SET   ABIERTO = PRES" & vbCrLf
sConsulta = sConsulta & "    FROM GRUPO_OFE_REU GRO" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN #TMP AS PP " & vbCrLf
sConsulta = sConsulta & "               ON GRO.GRUPO = PP.GRUPO" & vbCrLf
sConsulta = sConsulta & "   WHERE ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "     AND GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "     AND PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "     AND PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "     AND OFE = @OFE " & vbCrLf
sConsulta = sConsulta & "     AND FECREU = @FECREU" & vbCrLf
sConsulta = sConsulta & "  DROP TABLE #TMP" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM CUR_PROVE INTO @PROVE, @OFE" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "CLOSE CUR_PROVE" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE CUR_PROVE" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


End Sub


Public Function CodigoDeActualizacion2_11_50_11A2_11_50_12() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim bBool As Boolean

Dim sConexion As String
Dim sServer As String
Dim sBD As String
Dim sUsu As String
Dim sPwd As String
Dim intCont  As Integer
Dim oSQLServer As SQLDMO.SQLServer

On Error GoTo error
    

sConsulta = "BEGIN TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = True
ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

frmProgreso.lblDetalle = "Modificamos stored procedures"
frmProgreso.lblDetalle.Refresh
frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_11_5_Storeds_12

'ELIMINAMOS EL JOB DE CADUCIDAD PARA QUE LO CREE DE NUEVO CON EL NUEVO HORARIO

sConexion = gRDOCon.Connect
sServer = gServer

sBD = gBD
sUsu = gUID
sPwd = gPWD
    
Set oSQLServer = New SQLDMO.SQLServer
oSQLServer.Connect sServer, sUsu, sPwd  'Se conecta

For intCont = 1 To oSQLServer.JobServer.Jobs.Count
    If UCase(oSQLServer.JobServer.Jobs.Item(intCont).Name) = UCase("CADU_" & sBD) Then
        oSQLServer.JobServer.Jobs.Item(intCont).Remove
        Exit For
    End If
Next intCont

oSQLServer.Disconnect
Set oSQLServer = Nothing


'FIN ELIMINACION JOB



sConsulta = "UPDATE VERSION SET NUM ='2.11.50.12'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "COMMIT TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = False

CodigoDeActualizacion2_11_50_11A2_11_50_12 = True
Exit Function
    
error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_11_50_11A2_11_50_12 = False

End Function

Private Sub V_2_11_5_Storeds_12()
Dim sConsulta As String

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_CADUCIDAD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_CADUCIDAD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE SP_CADUCIDAD AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCE AS INT,@ANYO AS INT,@GMN1 AS VARCHAR(3),@PROVE AS VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @INSTWEB AS INT,@SERVER AS VARCHAR(50),@BD AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @NUM AS INT,@NUMNUE AS INT,@NUMAREV AS INT,@CODPORTAL AS VARCHAR(20),@ID AS SMALLINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @CONEX  AS VARCHAR(100),@FSP_CIA AS VARCHAR(20),@CONEX2 AS VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @LINEA AS FLOAT,@APROV AS INT,@CERRAR AS INT" & vbCrLf
sConsulta = sConsulta & "declare @FECHA AS DATETIME" & vbCrLf
sConsulta = sConsulta & "declare @FECHAPUB AS DATETIME" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @FECHAPUB=getdate()" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @FECHA = CONVERT(DATETIME,CONVERT(VARCHAR,DAY(GETDATE())) + '/' + CONVERT(VARCHAR,MONTH(GETDATE())) + '/' + CONVERT(VARCHAR,YEAR(GETDATE())) ,103)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @INSTWEB=(SELECT INSTWEB FROM PARGEN_INTERNO)" & vbCrLf
sConsulta = sConsulta & "IF @INSTWEB<>0 " & vbCrLf
sConsulta = sConsulta & "  IF @INSTWEB=2" & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "      SET @SERVER=(SELECT FSP_SRV FROM PARGEN_PORT)" & vbCrLf
sConsulta = sConsulta & "      SET @BD=(SELECT FSP_BD FROM PARGEN_PORT)" & vbCrLf
sConsulta = sConsulta & "      SET @FSP_CIA =(SELECT FSP_CIA FROM PARGEN_PORT WHERE ID=1)" & vbCrLf
sConsulta = sConsulta & "      SET @CONEX2= @SERVER + '.' + @BD + '.dbo.SP_SELECT_ID'" & vbCrLf
sConsulta = sConsulta & "      EXEC @ID = @CONEX2  @FSP_CIA" & vbCrLf
sConsulta = sConsulta & "    END" & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR FOR " & vbCrLf
sConsulta = sConsulta & " SELECT PROCE_PROVE.PROCE,PROCE_PROVE.ANYO,PROCE_PROVE.GMN1,PROCE_PROVE.PROVE " & vbCrLf
sConsulta = sConsulta & "   FROM PROCE_PROVE " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PROCE " & vbCrLf
sConsulta = sConsulta & "                   ON PROCE_PROVE.PROCE= PROCE.COD " & vbCrLf
sConsulta = sConsulta & "                  AND PROCE_PROVE.ANYO=PROCE.ANYO " & vbCrLf
sConsulta = sConsulta & "                  AND PROCE_PROVE.GMN1=PROCE.GMN1 " & vbCrLf
sConsulta = sConsulta & "  WHERE PROCE_PROVE.PUB = 1 " & vbCrLf
sConsulta = sConsulta & "    AND PROCE.FECLIMOFE <= @FECHAPUB" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "OPEN C " & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C INTO @PROCE,@ANYO,@GMN1,@PROVE" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    UPDATE PROCE_PROVE SET PUB=0,FECPUB=NULL WHERE PROCE=@PROCE  AND ANYO= @ANYO AND GMN1= @GMN1 AND PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "    IF  @INSTWEB=2" & vbCrLf
sConsulta = sConsulta & "      BEGIN" & vbCrLf
sConsulta = sConsulta & "        SET @CODPORTAL=(SELECT FSP_COD FROM PROVE WHERE COD = @PROVE)" & vbCrLf
sConsulta = sConsulta & "        EXECUTE NUM_PROCE_PUB @PROVE,@NUM OUTPUT,@NUMNUE OUTPUT,@NUMAREV OUTPUT" & vbCrLf
sConsulta = sConsulta & "        IF @NUM IS NULL" & vbCrLf
sConsulta = sConsulta & "          SET @NUM=0" & vbCrLf
sConsulta = sConsulta & "        IF @NUMNUE IS NULL" & vbCrLf
sConsulta = sConsulta & "          SET @NUMNUE=0" & vbCrLf
sConsulta = sConsulta & "        IF @NUMAREV IS NULL" & vbCrLf
sConsulta = sConsulta & "          SET @NUMAREV=0" & vbCrLf
sConsulta = sConsulta & "        SET @CONEX=@SERVER + '.' + @BD + '.dbo.SP_ACT_PROCE_PUB'" & vbCrLf
sConsulta = sConsulta & "        EXECUTE @CONEX @ID,@CODPORTAL,@NUM,@NUMNUE,@NUMAREV" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "    FETCH NEXT FROM C INTO @PROCE,@ANYO,@GMN1,@PROVE" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "CLOSE C" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @APROV=(SELECT APROVISION FROM PARGEN_INTERNO) " & vbCrLf
sConsulta = sConsulta & "IF @APROV<>0" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    SET @CERRAR=1" & vbCrLf
sConsulta = sConsulta & "    DECLARE D CURSOR FOR SELECT ID FROM CATALOG_LIN  WHERE PUB = 1 AND FECHA_DESPUB <= @FECHA" & vbCrLf
sConsulta = sConsulta & "    OPEN D" & vbCrLf
sConsulta = sConsulta & "    FETCH NEXT FROM D INTO @LINEA" & vbCrLf
sConsulta = sConsulta & "    WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "      BEGIN" & vbCrLf
sConsulta = sConsulta & "        UPDATE CATALOG_LIN SET PUB=0,FECHA_DESPUB=NULL WHERE ID=@LINEA" & vbCrLf
sConsulta = sConsulta & "        FETCH NEXT FROM D INTO @LINEA" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "    CLOSE D" & vbCrLf
sConsulta = sConsulta & "    DEALLOCATE D" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub


Public Function CodigoDeActualizacion2_11_50_12A2_11_50_13() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim bBool As Boolean

Dim sConexion As String
Dim sServer As String
Dim sBD As String
Dim sUsu As String
Dim sPwd As String
Dim intCont  As Integer
Dim oSQLServer As SQLDMO.SQLServer

On Error GoTo error
    

sConsulta = "BEGIN TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = True
ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

frmProgreso.lblDetalle = "Eliminamos atributos vacios"
frmProgreso.lblDetalle.Refresh
frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_11_5_Atributos_Vacios_13

frmProgreso.lblDetalle = "Modificamos storeds"
frmProgreso.lblDetalle.Refresh
frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_11_5_Storeds_13



sConsulta = "UPDATE VERSION SET NUM ='2.11.50.13'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "COMMIT TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = False

CodigoDeActualizacion2_11_50_12A2_11_50_13 = True
Exit Function
    
error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_11_50_12A2_11_50_13 = False

End Function

Private Sub V_2_11_5_Atributos_Vacios_13()
Dim sConsulta As String

sConsulta = ""
sConsulta = sConsulta & "DELETE FROM OFE_ATRIB " & vbCrLf
sConsulta = sConsulta & "WHERE VALOR_TEXT IS NULL" & vbCrLf
sConsulta = sConsulta & " AND VALOR_NUM IS NULL" & vbCrLf
sConsulta = sConsulta & " AND VALOR_FEC IS NULL" & vbCrLf
sConsulta = sConsulta & " AND VALOR_BOOL IS NULL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "DELETE FROM OFE_GR_ATRIB " & vbCrLf
sConsulta = sConsulta & "WHERE VALOR_TEXT IS NULL" & vbCrLf
sConsulta = sConsulta & " AND VALOR_NUM IS NULL" & vbCrLf
sConsulta = sConsulta & " AND VALOR_FEC IS NULL" & vbCrLf
sConsulta = sConsulta & " AND VALOR_BOOL IS NULL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "DELETE FROM OFE_ITEM_ATRIB " & vbCrLf
sConsulta = sConsulta & "WHERE VALOR_TEXT IS NULL" & vbCrLf
sConsulta = sConsulta & " AND VALOR_NUM IS NULL" & vbCrLf
sConsulta = sConsulta & " AND VALOR_FEC IS NULL" & vbCrLf
sConsulta = sConsulta & " AND VALOR_BOOL IS NULL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Private Sub V_2_11_5_Storeds_13()
Dim sConsulta As String


sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_TRASPASAR_OFERTA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_TRASPASAR_OFERTA]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE SP_TRASPASAR_OFERTA @ID int , @NUM INT OUTPUT, @NUMNUE INT OUTPUT, @NUMAREV INT OUTPUT, @ERROR SMALLINT = NULL OUTPUT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @NEWOFE INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @NUMOFEWEB AS int" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN1 VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCE INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVE VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @MON VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @ADJUN INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDADJUN INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDGS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @GRUPO VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @ITEM INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @NUMOBJ INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @NUMOBJGS INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @ERROR = 0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @ANYO = ANYO, @GMN1= GMN1, @PROCE = PROCE, @PROVE = PROVE, @MON = MON, @NUMOBJ = NUMOBJ" & vbCrLf
sConsulta = sConsulta & "  FROM P_PROCE_OFE " & vbCrLf
sConsulta = sConsulta & " WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @NUMOBJGS = (SELECT NUMOBJ FROM PROCE_PROVE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE)" & vbCrLf
sConsulta = sConsulta & "IF @NUMOBJGS  <=@NUMOBJ" & vbCrLf
sConsulta = sConsulta & "  UPDATE PROCE_PROVE SET OBJNUEVOS=0 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "/*" & vbCrLf
sConsulta = sConsulta & "DECLARE @EQUIV FLOAT " & vbCrLf
sConsulta = sConsulta & "SELECT @EQUIV = (MON.EQUIV / PROCE.CAMBIO) " & vbCrLf
sConsulta = sConsulta & "  FROM PROCE " & vbCrLf
sConsulta = sConsulta & "     INNER JOIN MON " & vbCrLf
sConsulta = sConsulta & "             ON PROCE.ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "            AND PROCE.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND PROCE.COD=@PROCE  " & vbCrLf
sConsulta = sConsulta & "            AND MON.COD=@MON " & vbCrLf
sConsulta = sConsulta & "*/" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @SUBASTA INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @SUBASTA = SUBASTA " & vbCrLf
sConsulta = sConsulta & "  FROM PROCE_DEF " & vbCrLf
sConsulta = sConsulta & " WHERE ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "   AND GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "   AND PROCE=@PROCE  " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @SUBASTA = 1" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    DECLARE @OFE INT" & vbCrLf
sConsulta = sConsulta & "    SET @OFE = ( SELECT MAX(OFE) FROM PROCE_OFE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE AND OFE!=-1)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    IF @OFE IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "      BEGIN" & vbCrLf
sConsulta = sConsulta & "        INSERT INTO P_OFE_GR_ATRIB (ID, GRUPO, ATRIB_ID,  VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL)" & vbCrLf
sConsulta = sConsulta & "        SELECT @ID, O.GRUPO, O.ATRIB_ID, O.VALOR_NUM, O.VALOR_TEXT, O.VALOR_FEC, O.VALOR_BOOL" & vbCrLf
sConsulta = sConsulta & "          FROM OFE_GR_ATRIB O" & vbCrLf
sConsulta = sConsulta & "         WHERE O.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "           AND O.GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "           AND O.PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "           AND O.PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "           AND O.OFE = @OFE" & vbCrLf
sConsulta = sConsulta & "           AND NOT EXISTS (SELECT * FROM P_OFE_GR_ATRIB T WHERE T.ID = @ID AND T.GRUPO = O.GRUPO AND T.ATRIB_ID = O.ATRIB_ID)" & vbCrLf
sConsulta = sConsulta & "        IF @@ERROR!=0 GOTO ERRORSINTRAN_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "        INSERT INTO P_OFE_ITEM_ATRIB (ID, GRUPO, ITEM, ATRIB_ID,  VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL)" & vbCrLf
sConsulta = sConsulta & "        SELECT @ID, I.GRUPO, IA.ITEM, ATRIB_ID, VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL" & vbCrLf
sConsulta = sConsulta & "          FROM OFE_ITEM_ATRIB IA" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN ITEM I" & vbCrLf
sConsulta = sConsulta & "                   ON I.ANYO = IA.ANYO" & vbCrLf
sConsulta = sConsulta & "                  AND I.GMN1 = IA.GMN1" & vbCrLf
sConsulta = sConsulta & "                  AND I.PROCE = IA.PROCE" & vbCrLf
sConsulta = sConsulta & "                  AND I.ID = IA.ITEM" & vbCrLf
sConsulta = sConsulta & "         WHERE IA.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "           AND IA.GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "           AND IA.PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "           AND IA.PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "           AND IA.OFE = @OFE" & vbCrLf
sConsulta = sConsulta & "           AND NOT EXISTS (SELECT * FROM P_OFE_ITEM_ATRIB T WHERE T.ID = @ID AND T.ATRIB_ID = IA.ATRIB_ID AND T.ITEM = IA.ITEM)" & vbCrLf
sConsulta = sConsulta & "        IF @@ERROR!=0 GOTO ERRORSINTRAN_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "        INSERT INTO P_OFE_ATRIB (ID, ATRIB_ID,  VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL)" & vbCrLf
sConsulta = sConsulta & "        SELECT @ID, O.ATRIB_ID, O.VALOR_NUM, O.VALOR_TEXT, O.VALOR_FEC, O.VALOR_BOOL" & vbCrLf
sConsulta = sConsulta & "          FROM OFE_ATRIB O" & vbCrLf
sConsulta = sConsulta & "         WHERE O.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "           AND O.GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "           AND O.PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "           AND O.PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "           AND O.OFE = @OFE" & vbCrLf
sConsulta = sConsulta & "           AND NOT EXISTS (SELECT * FROM P_OFE_ATRIB T WHERE T.ID = @ID AND T.ATRIB_ID = O.ATRIB_ID)" & vbCrLf
sConsulta = sConsulta & "        IF @@ERROR!=0 GOTO ERRORSINTRAN_" & vbCrLf
sConsulta = sConsulta & "      END " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "      DECLARE @OFEATRIB BIT" & vbCrLf
sConsulta = sConsulta & "      DECLARE @OFEGRUPOATRIB BIT" & vbCrLf
sConsulta = sConsulta & "      DECLARE @OFEITEMATRIB BIT" & vbCrLf
sConsulta = sConsulta & "      DECLARE @PAID INT" & vbCrLf
sConsulta = sConsulta & "      DECLARE @AMBITO SMALLINT" & vbCrLf
sConsulta = sConsulta & "      DECLARE @TIPO SMALLINT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "      DECLARE CATRIBS CURSOR LOCAL FORWARD_ONLY  FOR " & vbCrLf
sConsulta = sConsulta & "      SELECT PA.ID, AMBITO, GRUPO, TIPO" & vbCrLf
sConsulta = sConsulta & "        FROM PROCE_ATRIB PA" & vbCrLf
sConsulta = sConsulta & "         INNER JOIN DEF_ATRIB DA" & vbCrLf
sConsulta = sConsulta & "                 ON PA.ATRIB = DA.ID" & vbCrLf
sConsulta = sConsulta & "       WHERE ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "         AND GMN1= @GMN1" & vbCrLf
sConsulta = sConsulta & "         AND PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "         AND INTERNO = 0" & vbCrLf
sConsulta = sConsulta & "         AND OBLIG = 1" & vbCrLf
sConsulta = sConsulta & "      " & vbCrLf
sConsulta = sConsulta & "      OPEN CATRIBS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "      FETCH NEXT FROM CATRIBS INTO @PAID, @AMBITO, @GRUPO,  @TIPO" & vbCrLf
sConsulta = sConsulta & "      SET @OFEATRIB=0" & vbCrLf
sConsulta = sConsulta & "      SET @OFEGRUPOATRIB=0" & vbCrLf
sConsulta = sConsulta & "      SET @OFEITEMATRIB=0" & vbCrLf
sConsulta = sConsulta & "      WHILE @@FETCH_STATUS=0 AND @OFEATRIB=0 AND @OFEGRUPOATRIB=0 AND @OFEITEMATRIB=0" & vbCrLf
sConsulta = sConsulta & "        BEGIN" & vbCrLf
sConsulta = sConsulta & "          IF @AMBITO = 1" & vbCrLf
sConsulta = sConsulta & "            BEGIN" & vbCrLf
sConsulta = sConsulta & "              SET @OFEATRIB =1" & vbCrLf
sConsulta = sConsulta & "              SELECT @OFEATRIB = CASE " & vbCrLf
sConsulta = sConsulta & "                        WHEN CASE @TIPO " & vbCrLf
sConsulta = sConsulta & "                                WHEN 1 THEN CASE WHEN VALOR_TEXT ='' THEN NULL ELSE VALOR_TEXT END " & vbCrLf
sConsulta = sConsulta & "                                WHEN 2 THEN CAST(VALOR_NUM AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                                WHEN 3 THEN CAST(VALOR_FEC AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                                WHEN 4 THEN CAST(VALOR_BOOL AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                             END IS NULL " & vbCrLf
sConsulta = sConsulta & "                        THEN 1 " & vbCrLf
sConsulta = sConsulta & "                        ELSE 0 " & vbCrLf
sConsulta = sConsulta & "                     END" & vbCrLf
sConsulta = sConsulta & "                FROM P_OFE_ATRIB" & vbCrLf
sConsulta = sConsulta & "               WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "                 AND ATRIB_ID = @PAID" & vbCrLf
sConsulta = sConsulta & "            END" & vbCrLf
sConsulta = sConsulta & "          IF @AMBITO = 2" & vbCrLf
sConsulta = sConsulta & "            BEGIN" & vbCrLf
sConsulta = sConsulta & "              SET @OFEGRUPOATRIB =0" & vbCrLf
sConsulta = sConsulta & "              IF @GRUPO IS NULL " & vbCrLf
sConsulta = sConsulta & "                BEGIN" & vbCrLf
sConsulta = sConsulta & "                IF (SELECT COUNT(*) " & vbCrLf
sConsulta = sConsulta & "                      FROM P_OFE_GR_ATRIB " & vbCrLf
sConsulta = sConsulta & "                     WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "                       AND ATRIB_ID = @PAID" & vbCrLf
sConsulta = sConsulta & "                       AND CASE @TIPO " & vbCrLf
sConsulta = sConsulta & "                                WHEN 1 THEN CASE WHEN VALOR_TEXT ='' THEN NULL ELSE VALOR_TEXT END " & vbCrLf
sConsulta = sConsulta & "                                WHEN 2 THEN CAST(VALOR_NUM AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                                WHEN 3 THEN CAST(VALOR_FEC AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                                WHEN 4 THEN CAST(VALOR_BOOL AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                             END IS NOT NULL) <> (SELECT COUNT(*) " & vbCrLf
sConsulta = sConsulta & "                       FROM PROCE_GRUPO " & vbCrLf
sConsulta = sConsulta & "                      WHERE ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "                        AND GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "                        AND PROCE = @PROCE)" & vbCrLf
sConsulta = sConsulta & "                   SET @OFEGRUPOATRIB = 1" & vbCrLf
sConsulta = sConsulta & "                 END" & vbCrLf
sConsulta = sConsulta & "               ELSE" & vbCrLf
sConsulta = sConsulta & "                IF (SELECT COUNT(*) " & vbCrLf
sConsulta = sConsulta & "                      FROM P_OFE_GR_ATRIB " & vbCrLf
sConsulta = sConsulta & "                     WHERE GRUPO = @GRUPO" & vbCrLf
sConsulta = sConsulta & "                       AND ATRIB_ID = @PAID" & vbCrLf
sConsulta = sConsulta & "                       AND CASE @TIPO " & vbCrLf
sConsulta = sConsulta & "                                WHEN 1 THEN CASE WHEN VALOR_TEXT ='' THEN NULL ELSE VALOR_TEXT END " & vbCrLf
sConsulta = sConsulta & "                                WHEN 2 THEN CAST(VALOR_NUM AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                                WHEN 3 THEN CAST(VALOR_FEC AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                                WHEN 4 THEN CAST(VALOR_BOOL AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                             END IS NOT NULL)  =0 " & vbCrLf
sConsulta = sConsulta & "                       SET @OFEGRUPOATRIB = 1" & vbCrLf
sConsulta = sConsulta & "            END" & vbCrLf
sConsulta = sConsulta & "          IF @AMBITO = 3" & vbCrLf
sConsulta = sConsulta & "            BEGIN" & vbCrLf
sConsulta = sConsulta & "              SET @OFEITEMATRIB =0" & vbCrLf
sConsulta = sConsulta & "              IF @GRUPO IS NULL " & vbCrLf
sConsulta = sConsulta & "                BEGIN" & vbCrLf
sConsulta = sConsulta & "                  IF (SELECT COUNT(*) " & vbCrLf
sConsulta = sConsulta & "                        FROM P_OFE_ITEM_ATRIB " & vbCrLf
sConsulta = sConsulta & "                       WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "                         AND ATRIB_ID = @PAID" & vbCrLf
sConsulta = sConsulta & "                         AND CASE @TIPO " & vbCrLf
sConsulta = sConsulta & "                                WHEN 1 THEN CASE WHEN VALOR_TEXT ='' THEN NULL ELSE VALOR_TEXT END " & vbCrLf
sConsulta = sConsulta & "                                WHEN 2 THEN CAST(VALOR_NUM AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                                WHEN 3 THEN CAST(VALOR_FEC AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                                WHEN 4 THEN CAST(VALOR_BOOL AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                     END IS NOT NULL)  <>" & vbCrLf
sConsulta = sConsulta & "                      (SELECT COUNT(*) " & vbCrLf
sConsulta = sConsulta & "                         FROM ITEM " & vbCrLf
sConsulta = sConsulta & "                        WHERE ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "                          AND GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "                          AND PROCE = @PROCE                    " & vbCrLf
sConsulta = sConsulta & "                          AND CONF = 1)" & vbCrLf
sConsulta = sConsulta & "                       SET @OFEITEMATRIB = 1" & vbCrLf
sConsulta = sConsulta & "                 END" & vbCrLf
sConsulta = sConsulta & "              ELSE" & vbCrLf
sConsulta = sConsulta & "                 begin" & vbCrLf
sConsulta = sConsulta & "                  IF (SELECT COUNT(*) " & vbCrLf
sConsulta = sConsulta & "                        FROM P_OFE_ITEM_ATRIB" & vbCrLf
sConsulta = sConsulta & "                       WHERE GRUPO = @GRUPO" & vbCrLf
sConsulta = sConsulta & "                         AND ID = @ID" & vbCrLf
sConsulta = sConsulta & "                         AND ATRIB_ID = @PAID" & vbCrLf
sConsulta = sConsulta & "                         AND CASE @TIPO " & vbCrLf
sConsulta = sConsulta & "                                WHEN 1 THEN CASE WHEN VALOR_TEXT ='' THEN NULL ELSE VALOR_TEXT END " & vbCrLf
sConsulta = sConsulta & "                                WHEN 2 THEN CAST(VALOR_NUM AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                                WHEN 3 THEN CAST(VALOR_FEC AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                                WHEN 4 THEN CAST(VALOR_BOOL AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                             END IS NOT NULL)  <>" & vbCrLf
sConsulta = sConsulta & "                      (SELECT COUNT(*) " & vbCrLf
sConsulta = sConsulta & "                         FROM ITEM " & vbCrLf
sConsulta = sConsulta & "                        WHERE ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "                          AND GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "                          AND PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "                          AND GRUPO = @GRUPO" & vbCrLf
sConsulta = sConsulta & "                          AND CONF = 1)" & vbCrLf
sConsulta = sConsulta & "                       SET @OFEITEMATRIB = 1" & vbCrLf
sConsulta = sConsulta & "                   end" & vbCrLf
sConsulta = sConsulta & "            END" & vbCrLf
sConsulta = sConsulta & "            FETCH NEXT FROM CATRIBS INTO @PAID, @AMBITO, @GRUPO,  @TIPO" & vbCrLf
sConsulta = sConsulta & "          END" & vbCrLf
sConsulta = sConsulta & "      CLOSE CATRIBS" & vbCrLf
sConsulta = sConsulta & "      DEALLOCATE CATRIBS" & vbCrLf
sConsulta = sConsulta & "      IF @OFEATRIB = 1" & vbCrLf
sConsulta = sConsulta & "        SET @ERROR = 2" & vbCrLf
sConsulta = sConsulta & "      IF @OFEGRUPOATRIB = 1" & vbCrLf
sConsulta = sConsulta & "        SET @ERROR = 3" & vbCrLf
sConsulta = sConsulta & "      IF @OFEITEMATRIB = 1" & vbCrLf
sConsulta = sConsulta & "        SET @ERROR = 4" & vbCrLf
sConsulta = sConsulta & "      IF @ERROR>0" & vbCrLf
sConsulta = sConsulta & "        GOTO ELIMINAR_" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "DECLARE  @ESTWEB VARCHAR(3) " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "SELECT @ESTWEB=ESTWEB FROM PARGEN_DEF INNER JOIN PARGEN_GEST ON PARGEN_DEF.ID=PARGEN_GEST.ID  " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN TRANSACTION" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE SET ULT= 0, LEIDO = 1  WHERE ANYO = @ANYO AND GMN1 = @GMN1 AND PROCE = @PROCE AND PROVE = @PROVE AND ULT = 1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @NEWOFE = ISNULL(MAX(OFE),0) + 1 FROM PROCE_OFE WHERE ANYO = @ANYO AND GMN1 = @GMN1 AND PROCE = @PROCE AND PROVE = @PROVE AND OFE != -1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO PROCE_OFE(ANYO, GMN1, PROCE, PROVE, OFE, EST, FECREC, FECVAL, MON, CAMBIO, OBS, LEIDO, ULT, ENVIADA, OBSADJUN)" & vbCrLf
sConsulta = sConsulta & "SELECT @ANYO, @GMN1, @PROCE, @PROVE, @NEWOFE, @ESTWEB, FECREC, FECVAL, MON, CAMBIO, OBS, 0, 1, 1, OBSADJUN" & vbCrLf
sConsulta = sConsulta & "  FROM P_PROCE_OFE" & vbCrLf
sConsulta = sConsulta & "WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE GRUPO_OFE " & vbCrLf
sConsulta = sConsulta & "   SET OBSADJUN = PGO.OBSADJUN" & vbCrLf
sConsulta = sConsulta & "  FROM GRUPO_OFE GO" & vbCrLf
sConsulta = sConsulta & "      INNER JOIN P_GRUPO_OFE PGO" & vbCrLf
sConsulta = sConsulta & "              ON GO.GRUPO = PGO.GRUPO" & vbCrLf
sConsulta = sConsulta & " WHERE GO.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "   AND GO.GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "   AND GO.PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "   AND GO.PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "   AND GO.OFE = @NEWOFE" & vbCrLf
sConsulta = sConsulta & "   AND PGO.ID = @ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_OFE" & vbCrLf
sConsulta = sConsulta & "   SET PRECIO = PIO.PRECIO," & vbCrLf
sConsulta = sConsulta & "       CANTMAX = PIO.CANTMAX," & vbCrLf
sConsulta = sConsulta & "       PRECIO2 = PIO.PRECIO2," & vbCrLf
sConsulta = sConsulta & "       PRECIO3 = PIO.PRECIO3," & vbCrLf
sConsulta = sConsulta & "       PREC_VALIDO = PIO.PRECIO," & vbCrLf
sConsulta = sConsulta & "       OBSADJUN = PIO.OBSADJUN," & vbCrLf
sConsulta = sConsulta & "       COMENT1 = PIO.COMENT1," & vbCrLf
sConsulta = sConsulta & "       COMENT2 = PIO.COMENT2," & vbCrLf
sConsulta = sConsulta & "       COMENT3 = PIO.COMENT3" & vbCrLf
sConsulta = sConsulta & "  FROM ITEM_OFE IO" & vbCrLf
sConsulta = sConsulta & "     INNER JOIN P_ITEM_OFE PIO" & vbCrLf
sConsulta = sConsulta & "             ON IO.ITEM = PIO.ITEM" & vbCrLf
sConsulta = sConsulta & " WHERE IO.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "   AND IO.GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "   AND IO.PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "   AND IO.PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "   AND IO.NUM = @NEWOFE" & vbCrLf
sConsulta = sConsulta & "   AND PIO.ID = @ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_ITEM_ADJUN " & vbCrLf
sConsulta = sConsulta & "  FROM OFE_ITEM_ADJUN R" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN P_ITEM_OFE PIO" & vbCrLf
sConsulta = sConsulta & "               ON PIO.ID = @ID" & vbCrLf
sConsulta = sConsulta & "              AND R.ITEM = PIO.ITEM" & vbCrLf
sConsulta = sConsulta & " WHERE R.ANYO = @ANYO " & vbCrLf
sConsulta = sConsulta & "   AND R.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "   AND R.PROCE= @PROCE " & vbCrLf
sConsulta = sConsulta & "   AND R.PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "   AND R.OFE IN (@NEWOFE,-1)" & vbCrLf
sConsulta = sConsulta & "   AND PIO.ADJUN = 1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_ITEM_ATRIB " & vbCrLf
sConsulta = sConsulta & "  FROM OFE_ITEM_ATRIB R" & vbCrLf
sConsulta = sConsulta & " WHERE R.ANYO = @ANYO " & vbCrLf
sConsulta = sConsulta & "   AND R.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "   AND R.PROCE= @PROCE " & vbCrLf
sConsulta = sConsulta & "   AND R.PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "   AND R.OFE IN (@NEWOFE,-1)" & vbCrLf
sConsulta = sConsulta & "   AND EXISTS (SELECT * FROM P_OFE_ITEM_ATRIB P " & vbCrLf
sConsulta = sConsulta & "                WHERE P.ID  = @ID " & vbCrLf
sConsulta = sConsulta & "                  AND P.ITEM = R.ITEM" & vbCrLf
sConsulta = sConsulta & "                  AND P.ATRIB_ID = R.ATRIB_ID)" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_GR_ATRIB " & vbCrLf
sConsulta = sConsulta & "  FROM OFE_GR_ATRIB R" & vbCrLf
sConsulta = sConsulta & " WHERE R.ANYO = @ANYO " & vbCrLf
sConsulta = sConsulta & "   AND R.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "   AND R.PROCE= @PROCE " & vbCrLf
sConsulta = sConsulta & "   AND R.PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "   AND R.OFE IN (@NEWOFE,-1)" & vbCrLf
sConsulta = sConsulta & "   AND EXISTS (SELECT * FROM P_OFE_GR_ATRIB P " & vbCrLf
sConsulta = sConsulta & "                WHERE P.ID  = @ID " & vbCrLf
sConsulta = sConsulta & "                  AND CAST(P.GRUPO AS VARCHAR(50)) = CAST(R.GRUPO AS VARCHAR(50))" & vbCrLf
sConsulta = sConsulta & "                  AND P.ATRIB_ID = R.ATRIB_ID)" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_GR_ADJUN " & vbCrLf
sConsulta = sConsulta & "  FROM OFE_GR_ADJUN R" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN P_GRUPO_OFE PGO" & vbCrLf
sConsulta = sConsulta & "            ON PGO.ID = @ID" & vbCrLf
sConsulta = sConsulta & "           AND PGO.GRUPO = R.GRUPO" & vbCrLf
sConsulta = sConsulta & " WHERE R.ANYO = @ANYO " & vbCrLf
sConsulta = sConsulta & "   AND R.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "   AND R.PROCE= @PROCE " & vbCrLf
sConsulta = sConsulta & "   AND R.PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "   AND R.OFE IN (@NEWOFE,-1)" & vbCrLf
sConsulta = sConsulta & "   AND PGO.ADJUN = 1" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_ATRIB " & vbCrLf
sConsulta = sConsulta & "  FROM OFE_ATRIB R" & vbCrLf
sConsulta = sConsulta & " WHERE R.ANYO = @ANYO " & vbCrLf
sConsulta = sConsulta & "   AND R.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "   AND R.PROCE= @PROCE " & vbCrLf
sConsulta = sConsulta & "   AND R.PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "   AND R.OFE IN (@NEWOFE,-1)" & vbCrLf
sConsulta = sConsulta & "   AND EXISTS (SELECT * FROM P_OFE_ATRIB P " & vbCrLf
sConsulta = sConsulta & "                WHERE P.ID  = @ID " & vbCrLf
sConsulta = sConsulta & "                  AND P.ATRIB_ID = R.ATRIB_ID)" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF (SELECT ADJUN FROM P_PROCE_OFE WHERE ID = @ID) = 1" & vbCrLf
sConsulta = sConsulta & "DELETE FROM PROCE_OFE_ADJUN " & vbCrLf
sConsulta = sConsulta & "  FROM PROCE_OFE_ADJUN R" & vbCrLf
sConsulta = sConsulta & " WHERE R.ANYO = @ANYO " & vbCrLf
sConsulta = sConsulta & "   AND R.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "   AND R.PROCE= @PROCE " & vbCrLf
sConsulta = sConsulta & "   AND R.PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "   AND R.OFE IN (@NEWOFE,-1)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM PROCE_OFE_ADJUN WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE= @PROCE and PROVE=@PROVE AND OFE = -1" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_GR_ADJUN WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE= @PROCE and PROVE=@PROVE AND OFE = -1" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_ITEM_ADJUN WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE= @PROCE and PROVE=@PROVE AND OFE = -1" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_ATRIB WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE= @PROCE and PROVE=@PROVE AND OFE = -1" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_GR_ATRIB WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE= @PROCE and PROVE=@PROVE AND OFE = -1" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_ITEM_ATRIB WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE= @PROCE and PROVE=@PROVE AND OFE = -1" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM ITEM_OFE WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE= @PROCE and PROVE=@PROVE AND NUM = -1" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM GRUPO_OFE WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE= @PROCE and PROVE=@PROVE AND OFE = -1" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM PROCE_OFE WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE= @PROCE and PROVE=@PROVE AND OFE = -1" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR LOCAL FOR" & vbCrLf
sConsulta = sConsulta & "SELECT ADJUN, IDGS FROM P_PROCE_OFE_ADJUN WHERE ID=@ID AND IDGS IS NULL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "OPEN C" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C INTO @ADJUN, @IDGS" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    SELECT @IDADJUN = isnull(MAX(ID),0) + 1 FROM ADJUN " & vbCrLf
sConsulta = sConsulta & "    INSERT INTO ADJUN (ID, WEB) VALUES (@IDADJUN, 1) " & vbCrLf
sConsulta = sConsulta & "    UPDATE P_PROCE_OFE_ADJUN SET IDGS = @IDADJUN WHERE ID = @ID AND ADJUN = @ADJUN" & vbCrLf
sConsulta = sConsulta & "    IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "    FETCH NEXT FROM C INTO @ADJUN, @IDGS" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "CLOSE C" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO PROCE_OFE_ADJUN(ANYO, GMN1, PROCE, PROVE, OFE, ID, NOM, COM, IDPORTAL, DATASIZE)" & vbCrLf
sConsulta = sConsulta & "SELECT @ANYO, @GMN1, @PROCE, @PROVE, @NEWOFE, POA.IDGS, POA.NOM, POA.COM, POA.IDPORTAL, POA.DATASIZE" & vbCrLf
sConsulta = sConsulta & "  FROM P_PROCE_OFE_ADJUN POA" & vbCrLf
sConsulta = sConsulta & "         INNER JOIN ADJUN A" & vbCrLf
sConsulta = sConsulta & "                 ON POA.IDGS = A.ID" & vbCrLf
sConsulta = sConsulta & "WHERE POA.ID = @ID" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR LOCAL FOR" & vbCrLf
sConsulta = sConsulta & "SELECT ADJUN, IDGS, GRUPO FROM P_OFE_GR_ADJUN WHERE ID=@ID AND IDGS IS NULL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "OPEN C" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C INTO @ADJUN, @IDGS, @GRUPO" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    SELECT @IDADJUN = isnull(MAX(ID),0) + 1 FROM ADJUN " & vbCrLf
sConsulta = sConsulta & "    INSERT INTO ADJUN (ID, WEB) VALUES (@IDADJUN, 1) " & vbCrLf
sConsulta = sConsulta & "    UPDATE P_OFE_GR_ADJUN SET IDGS = @IDADJUN WHERE ID = @ID AND ADJUN = @ADJUN AND GRUPO = @GRUPO" & vbCrLf
sConsulta = sConsulta & "    IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "    FETCH NEXT FROM C INTO @ADJUN, @IDGS, @GRUPO" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "CLOSE C" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO OFE_GR_ADJUN(ANYO, GMN1, PROCE, GRUPO, PROVE, OFE, ID, NOM, COM, IDPORTAL, DATASIZE)" & vbCrLf
sConsulta = sConsulta & "SELECT @ANYO, @GMN1, @PROCE, OGA.GRUPO, @PROVE, @NEWOFE, OGA.IDGS, OGA.NOM, OGA.COM, OGA.IDPORTAL, OGA.DATASIZE" & vbCrLf
sConsulta = sConsulta & "  FROM P_OFE_GR_ADJUN OGA" & vbCrLf
sConsulta = sConsulta & "         INNER JOIN ADJUN A" & vbCrLf
sConsulta = sConsulta & "                 ON OGA.IDGS = A.ID" & vbCrLf
sConsulta = sConsulta & " WHERE OGA.ID = @ID" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR LOCAL FOR" & vbCrLf
sConsulta = sConsulta & "SELECT ADJUN, IDGS, ITEM FROM P_OFE_ITEM_ADJUN WHERE ID=@ID AND IDGS IS NULL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "OPEN C" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C INTO @ADJUN, @IDGS, @ITEM" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    SELECT @IDADJUN = isnull(MAX(ID),0) + 1 FROM ADJUN " & vbCrLf
sConsulta = sConsulta & "    INSERT INTO ADJUN (ID, WEB) VALUES (@IDADJUN, 1) " & vbCrLf
sConsulta = sConsulta & "    UPDATE P_OFE_ITEM_ADJUN SET IDGS = @IDADJUN WHERE ID = @ID AND ADJUN = @ADJUN AND ITEM = @ITEM" & vbCrLf
sConsulta = sConsulta & "    IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "    FETCH NEXT FROM C INTO @ADJUN, @IDGS, @ITEM" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "CLOSE C" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO OFE_ITEM_ADJUN(ANYO, GMN1, PROCE, ITEM, PROVE, OFE, ID, NOM, COM, IDPORTAL, DATASIZE)" & vbCrLf
sConsulta = sConsulta & "SELECT @ANYO, @GMN1, @PROCE, OIA.ITEM, @PROVE, @NEWOFE, OIA.IDGS, OIA.NOM, OIA.COM, OIA.IDPORTAL, OIA.DATASIZE" & vbCrLf
sConsulta = sConsulta & "  FROM P_OFE_ITEM_ADJUN OIA" & vbCrLf
sConsulta = sConsulta & "         INNER JOIN ADJUN A" & vbCrLf
sConsulta = sConsulta & "                 ON OIA.IDGS = A.ID" & vbCrLf
sConsulta = sConsulta & "WHERE OIA.ID = @ID" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO OFE_GR_ATRIB(ANYO, GMN1, PROCE, GRUPO, PROVE, OFE, ATRIB_ID, VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL)" & vbCrLf
sConsulta = sConsulta & "SELECT @ANYO, @GMN1, @PROCE, GRUPO, @PROVE, @NEWOFE, ATRIB_ID, VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL" & vbCrLf
sConsulta = sConsulta & "  FROM P_OFE_GR_ATRIB" & vbCrLf
sConsulta = sConsulta & " WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "   AND NOT (VALOR_NUM IS NULL" & vbCrLf
sConsulta = sConsulta & "        AND VALOR_TEXT IS NULL" & vbCrLf
sConsulta = sConsulta & "        AND VALOR_FEC IS NULL" & vbCrLf
sConsulta = sConsulta & "        AND VALOR_BOOL IS NULL)" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO OFE_ITEM_ATRIB(ANYO, GMN1, PROCE, ITEM, PROVE, OFE, ATRIB_ID, VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL)" & vbCrLf
sConsulta = sConsulta & "SELECT @ANYO, @GMN1, @PROCE, ITEM, @PROVE, @NEWOFE, ATRIB_ID, VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL" & vbCrLf
sConsulta = sConsulta & "  FROM P_OFE_ITEM_ATRIB" & vbCrLf
sConsulta = sConsulta & " WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "   AND NOT (VALOR_NUM IS NULL" & vbCrLf
sConsulta = sConsulta & "        AND VALOR_TEXT IS NULL" & vbCrLf
sConsulta = sConsulta & "        AND VALOR_FEC IS NULL" & vbCrLf
sConsulta = sConsulta & "        AND VALOR_BOOL IS NULL)" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO OFE_ATRIB(ANYO, GMN1, PROCE, PROVE, OFE, ATRIB_ID, VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL)" & vbCrLf
sConsulta = sConsulta & "SELECT @ANYO, @GMN1, @PROCE, @PROVE, @NEWOFE, ATRIB_ID, VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL" & vbCrLf
sConsulta = sConsulta & "  FROM P_OFE_ATRIB " & vbCrLf
sConsulta = sConsulta & " WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "   AND NOT (VALOR_NUM IS NULL" & vbCrLf
sConsulta = sConsulta & "        AND VALOR_TEXT IS NULL" & vbCrLf
sConsulta = sConsulta & "        AND VALOR_FEC IS NULL" & vbCrLf
sConsulta = sConsulta & "        AND VALOR_BOOL IS NULL)" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE SET OFE = 1  WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE = @PROCE AND PROVE=@PROVE AND OFE = 0" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @NUMOFEWEB = isnull(MAX(OFEWEB),0)+1 FROM PROCE_OFE_WEB WHERE ANYO = @ANYO AND GMN1 = @GMN1 AND PROCE = @PROCE AND PROVE = @PROVE " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO PROCE_OFE_WEB (ANYO, GMN1, PROCE, PROVE, OFEWEB, OFE, FECREC, FECVAL, MON)" & vbCrLf
sConsulta = sConsulta & "SELECT ANYO, GMN1, PROCE, PROVE, @NUMOFEWEB, OFE, FECREC,FECVAL,MON " & vbCrLf
sConsulta = sConsulta & "FROM PROCE_OFE " & vbCrLf
sConsulta = sConsulta & "WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE = @PROCE AND PROVE = @PROVE AND OFE = @NEWOFE" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO ITEM_OFE_WEB (ANYO, GMN1, PROCE, ITEM, PROVE, NUM, PRECIO, CANTMAX)" & vbCrLf
sConsulta = sConsulta & "SELECT ANYO, GMN1, PROCE, ITEM, PROVE, @NUMOFEWEB, PRECIO, CANTMAX" & vbCrLf
sConsulta = sConsulta & "FROM ITEM_OFE" & vbCrLf
sConsulta = sConsulta & "WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE = @PROCE AND PROVE = @PROVE AND NUM = @NEWOFE" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO PROCE_OFE_WEB_ADJUN (ANYO, GMN1, PROCE, PROVE, OFEWEB, ID, NOM, COM, IDPORTAL)" & vbCrLf
sConsulta = sConsulta & "SELECT ANYO, GMN1, PROCE, PROVE, @NUMOFEWEB, ID, NOM, COM, IDPORTAL" & vbCrLf
sConsulta = sConsulta & "FROM PROCE_OFE_ADJUN" & vbCrLf
sConsulta = sConsulta & "WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE = @PROCE AND PROVE = @PROVE AND OFE = @NEWOFE" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_OFE SET ULT = 0 WHERE ANYO = @ANYO AND GMN1 = @GMN1 AND PROCE =@PROCE AND PROVE=@PROVE AND NUM!=@NEWOFE" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "COMMIT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "GOTO ELIMINAR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ERROR_:" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @ERROR = 100" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ROLLBACK" & vbCrLf
sConsulta = sConsulta & "GOTO ELIMINAR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ERRORSINTRAN_:" & vbCrLf
sConsulta = sConsulta & "SET @ERROR = 200" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ELIMINAR_:" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC NUM_PROCE_PUB @PROVE =@PROVE ,@NUM = @NUM OUTPUT,@NUMNUE = @NUMNUE OUTPUT ,@NUMAREV = @NUMAREV OUTPUT " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM P_PROCE_OFE WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM P_GRUPO_OFE WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM P_ITEM_OFE WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM P_OFE_ATRIB WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM P_OFE_GR_ATRIB WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM P_OFE_ITEM_ATRIB WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM P_PROCE_OFE_ADJUN WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM P_OFE_GR_ADJUN WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM P_OFE_ITEM_ADJUN WHERE ID=@ID" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub
