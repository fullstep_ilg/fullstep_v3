Attribute VB_Name = "basUtilidades"
Public Type LongitudesDeCodigos

    giLongCodART As Integer
    giLongCodCAL As Integer
    giLongCodCOM As Integer
    giLongCodCON As Integer
    giLongCodDEP As Integer
    giLongCodDEST As Integer
    giLongCodEQP As Integer
    giLongCodGMN1 As Integer
    giLongCodGMN2 As Integer
    giLongCodGMN3 As Integer
    giLongCodGMN4 As Integer
    giLongCodMON As Integer
    giLongCodOFEEST As Integer
    giLongCodPAG As Integer
    giLongCodPAI As Integer
    giLongCodPER As Integer
    giLongCodPERF As Integer
    giLongCodPRESCON1 As Integer
    giLongCodPRESCON2 As Integer
    giLongCodPRESCON3 As Integer
    giLongCodPRESCON4 As Integer
    giLongCodPRESPROY1 As Integer
    giLongCodPRESPROY2 As Integer
    giLongCodPRESPROY3 As Integer
    giLongCodPRESPROY4 As Integer
    giLongCodPROVE As Integer
    giLongCodPROVI As Integer
    giLongCodROL As Integer
    giLongCodUNI As Integer
    giLongCodUON1 As Integer
    giLongCodUON2 As Integer
    giLongCodUON3 As Integer
    giLongCodUSU As Integer
    giLongCodACT As Integer
    giLongCodSUBACT1 As Integer
    giLongCodSUBACT2 As Integer
    giLongCodSUBACT3 As Integer
    giLongCodSUBACT4 As Integer
    giLongCodPRESCONCEP31 As Integer
    giLongCodPRESCONCEP32 As Integer
    giLongCodPRESCONCEP33 As Integer
    giLongCodPRESCONCEP34 As Integer
    giLongCodPRESCONCEP41 As Integer
    giLongCodPRESCONCEP42 As Integer
    giLongCodPRESCONCEP43 As Integer
    giLongCodPRESCONCEP44 As Integer
    giLongCodCAT1 As Integer
    giLongCodCAT2 As Integer
    giLongCodCAT3 As Integer
    giLongCodCAT4 As Integer
    giLongCodCAT5 As Integer
    giLongCodDIC As Integer
    giLongCodGRUPO As Integer
    giLongCodCONTR As Integer 'Codigo de contrato
    giLongCodATRIB As Integer
    giLongCodPAR1 As Integer
    giLongCodPAR2 As Integer
    giLongCodPAR3 As Integer
    giLongCodPAR4 As Integer
    giLongCodVarCal As Integer
    
    giLongDenArt As Integer
    giLongCodPres5_0 As Integer
    giLongCodPres5_1 As Integer
    giLongCodPres5_2 As Integer
    giLongCodPres5_3 As Integer
    giLongCodPres5_4 As Integer
    giLongCodViaPago As Integer
    giLongCodUON4 As Integer
    
    giLongActivo As Integer
    giLongCentroCoste As Integer
    
    giFSGATareCod As Integer
    giFSGACatTareCod As Integer
    giFSGAProyCod As Integer
End Type

Public Const ClaveSQLtod = "aldj�w45u9'2JIHO^h+'31t4mV� H hfo fdGAAFKWETJ�51FSjD>GH18071998"
Public Const ClaveOBJtod = "jldfadf40'8946jnm, E�.Z -QS-    2e  -cy..0.qqvj,opry,j,18071998"
Public Const ClaveADMpar = "pruiopjanjup9858 99NTB=TBFS FI S.^�*^P?�afad19068jGIuyg18071998"
Public Const ClaveADMimp = "ljkdagga60TYIOGOIguIugh9p87tygp54854edWQ$""`=H�jl�ni�n�18071998"
Public Const ClaveUSUpar = "agkag�m=,5^**^P*43u9ihoHPGH)�(YIPGBITaDNGFGNDNK�D  ip&?18071998"
Public Const ClaveUSUimp = "h+hlL_^P*:K*M)(YN7HGN7tngn)GFNGNAagag8u998gd\=\]\]]\|||18071998"

Public Function DblQuote(ByVal StrToDblQuote As String) As String
    Dim QPos As Long
    Dim pos As Long
    
    Dim TmpDblQuote As String
    
    TmpDblQuote = ""
    pos = 1
    
    Do While Len(StrToDblQuote) > 0
    
        QPos = InStr(pos, StrToDblQuote, "'")
        
        If QPos > 0 Then
            
            TmpDblQuote = TmpDblQuote & Mid(StrToDblQuote, pos, QPos - 1) & "''"
            StrToDblQuote = Right(StrToDblQuote, Len(StrToDblQuote) - QPos)
            
        Else
        
            TmpDblQuote = TmpDblQuote & StrToDblQuote
            Exit Do
            
        End If
            
    Loop
    
    DblQuote = TmpDblQuote
    
End Function
Function DateToSQLDate(DateThatCanBeEmpty) As Variant


If IsEmpty(DateThatCanBeEmpty) Then
    DateToSQLDate = "NULL"
Else
    If IsNull(DateThatCanBeEmpty) Then
        DateToSQLDate = "NULL"
    Else
        If DateThatCanBeEmpty = "" Then
            DateToSQLDate = "NULL"
        Else
            DateToSQLDate = "'" & Format(DateThatCanBeEmpty, "mm-dd-yyyy") & "'"
        End If
    End If
End If

End Function
Function DateToSQLTimeDate(DateThatCanBeEmpty) As Variant


If IsEmpty(DateThatCanBeEmpty) Then
    DateToSQLTimeDate = "NULL"
Else
    If IsNull(DateThatCanBeEmpty) Then
        DateToSQLTimeDate = "NULL"
    Else
        If DateThatCanBeEmpty = "" Then
            DateToSQLTimeDate = "NULL"
        Else
            DateToSQLTimeDate = "Convert(datetime,'" & Format(DateThatCanBeEmpty, "mm-dd-yyyy hh:mm:ss") & "',110)"
        End If
    End If
End If

End Function

Public Sub Reabrir(ByVal anyo As Integer, ByVal gmn1 As String, ByVal Proce As String)
Dim sConsulta As String
Dim rdores As rdoResultset

sConsulta = "SELECT ANYO,GMN1,COD,EST,ESTULT,FECCIERRE,USUVALCIERRE FROM PROCE WHERE ANYO=" & anyo & " AND GMN1='" & DblQuote(gmn1) & "' AND COD=" & Proce
    
Set rdores = gRDOCon.OpenResultset(sConsulta, rdOpenKeyset, rdConcurRowVer)

If rdores.eof Then
    rdores.Close
    Set rdores = Nothing
    Exit Sub
End If
            
    'Tengo que recuperar el �ltimo estado del proceso
    rdores.Edit
    rdores("EST") = rdores("ESTULT")
    rdores.Update
    rdores.Close
    Set rdores = Nothing
        
    'Ahora borro la informaci�n de la tabla de resultados
    sConsulta = "DELETE FROM AR_PROCE WHERE ANYO=" & anyo & " AND GMN1='" & DblQuote(gmn1) & "' AND COD=" & Proce
    gRDOCon.Execute sConsulta, rdExecDirect
        
    sConsulta = "DELETE FROM AR_ITEM WHERE ANYO=" & anyo & " AND GMN1='" & DblQuote(gmn1) & "' AND PROCE=" & Proce & " AND ITEM > 0"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    sConsulta = "DELETE FROM AR_ITEM_MES WHERE ANYO=" & anyo & " AND GMN1='" & DblQuote(gmn1) & "' AND PROCE=" & Proce & " AND ITEM > 0"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    sConsulta = "DELETE FROM AR_ITEM_MES_UON1 WHERE ANYO=" & anyo & " AND GMN1='" & DblQuote(gmn1) & "' AND PROCE=" & Proce & " AND ITEM > 0"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    sConsulta = "DELETE FROM AR_ITEM_MES_UON2 WHERE ANYO=" & anyo & " AND GMN1='" & DblQuote(gmn1) & "' AND PROCE=" & Proce & " AND ITEM > 0"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    sConsulta = "DELETE FROM AR_ITEM_MES_UON3 WHERE ANYO=" & anyo & " AND GMN1='" & DblQuote(gmn1) & "' AND PROCE=" & Proce & " AND ITEM > 0"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    sConsulta = "DELETE FROM AR_ITEM_PROVE WHERE ANYO=" & anyo & " AND GMN1='" & DblQuote(gmn1) & "' AND PROCE=" & Proce & " AND ITEM > 0"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    sConsulta = "DELETE FROM ART4_ADJ WHERE ANYO=" & anyo & " AND GMN1='" & DblQuote(gmn1) & "' AND PROCE=" & Proce
    gRDOCon.Execute sConsulta, rdExecDirect
        
    
Exit Sub
        
Error:
    
    
End Sub

Public Function DblToSQLFloat(DblToConvert) As String
    Dim SeekPos As Long
    Dim pos As Long
    
    Dim TmpFloat As String
    Dim StrToConvert As String
    
    Dim sDecimal As String
    Dim sThousand As String
        
    If IsEmpty(DblToConvert) Then
        DblToSQLFloat = "NULL"
        Exit Function
    End If
    
    If IsNull(DblToConvert) Then
        DblToSQLFloat = "NULL"
        Exit Function
    End If
    
    If DblToConvert = "" Then
        DblToSQLFloat = "NULL"
        Exit Function
    End If
    
    StrToConvert = CStr(DblToConvert)
    
    sThousand = "."
    
    sDecimal = ","
    
    TmpFloat = ""
    pos = 1
    
    Do While Len(StrToConvert) > 0
    
        SeekPos = InStr(pos, StrToConvert, sThousand)
        
        If SeekPos > 0 Then
            
            TmpFloat = TmpFloat & Mid(StrToConvert, pos, SeekPos - 1)
            StrToConvert = Right(StrToConvert, Len(StrToConvert) - SeekPos)
            
        Else
        
            TmpFloat = TmpFloat & StrToConvert
            Exit Do
            
        End If
            
    Loop
    
    DblToSQLFloat = TmpFloat

    StrToConvert = DblToSQLFloat
    TmpFloat = ""
    pos = 1
    
    Do While Len(StrToConvert) > 0
    
        SeekPos = InStr(pos, StrToConvert, sDecimal)
        
        If SeekPos > 0 Then
            
            TmpFloat = TmpFloat & Mid(StrToConvert, pos, SeekPos - 1) & "."
            StrToConvert = Right(StrToConvert, Len(StrToConvert) - SeekPos)
            
        Else
        
            TmpFloat = TmpFloat & StrToConvert
            Exit Do
            
        End If
            
    Loop
    
    DblToSQLFloat = TmpFloat

End Function
Public Function NullToDbl0(ByVal ValueThatCanBeNull As Variant) As Variant
  
    If IsNull(ValueThatCanBeNull) Then
        NullToDbl0 = 0
    Else
        NullToDbl0 = ValueThatCanBeNull
    End If

End Function



Function StrToSQLNULL(StrThatCanBeEmpty) As Variant
    
    If IsEmpty(StrThatCanBeEmpty) Then
        StrToSQLNULL = "NULL"
    Else
        If IsNull(StrThatCanBeEmpty) Then
            StrToSQLNULL = "NULL"
        Else
            If StrThatCanBeEmpty = "" Then
                StrToSQLNULL = "NULL"
            Else
                StrToSQLNULL = "'" & DblQuote(CStr(StrThatCanBeEmpty)) & "'"
            End If
        End If
    End If
    
End Function

Public Function NullToStr(ByVal ValueThatCanBeNull As Variant) As String
  
    If IsNull(ValueThatCanBeNull) Then
        NullToStr = ""
    Else
        NullToStr = ValueThatCanBeNull
    End If

End Function

Public Function DevolverLongitudesDeCodigos() As LongitudesDeCodigos

    Dim rdores As rdoResultset
    Dim vLongitudesDeCodigos As LongitudesDeCodigos
    
    vLongitudesDeCodigos.giLongCodART = 20
    vLongitudesDeCodigos.giLongCodCAL = 3
    vLongitudesDeCodigos.giLongCodCOM = 20
    vLongitudesDeCodigos.giLongCodCON = 3
    vLongitudesDeCodigos.giLongCodDEP = 3
    vLongitudesDeCodigos.giLongCodDEST = 4
    vLongitudesDeCodigos.giLongCodEQP = 3
    vLongitudesDeCodigos.giLongCodGMN1 = 3
    vLongitudesDeCodigos.giLongCodGMN2 = 3
    vLongitudesDeCodigos.giLongCodGMN3 = 3
    vLongitudesDeCodigos.giLongCodGMN4 = 3
    vLongitudesDeCodigos.giLongCodMON = 3
    vLongitudesDeCodigos.giLongCodOFEEST = 3
    vLongitudesDeCodigos.giLongCodPAG = 3
    vLongitudesDeCodigos.giLongCodPAI = 3
    vLongitudesDeCodigos.giLongCodPER = 20
    vLongitudesDeCodigos.giLongCodPERF = 3
    vLongitudesDeCodigos.giLongCodPRESCON1 = 15
    vLongitudesDeCodigos.giLongCodPRESCON2 = 15
    vLongitudesDeCodigos.giLongCodPRESCON3 = 15
    vLongitudesDeCodigos.giLongCodPRESCON4 = 15
    vLongitudesDeCodigos.giLongCodPRESPROY1 = 15
    vLongitudesDeCodigos.giLongCodPRESPROY2 = 15
    vLongitudesDeCodigos.giLongCodPRESPROY3 = 15
    vLongitudesDeCodigos.giLongCodPRESPROY4 = 15
    vLongitudesDeCodigos.giLongCodPROVE = 20
    vLongitudesDeCodigos.giLongCodPROVI = 3
    vLongitudesDeCodigos.giLongCodROL = 3
    vLongitudesDeCodigos.giLongCodUNI = 3
    vLongitudesDeCodigos.giLongCodUON1 = 3
    vLongitudesDeCodigos.giLongCodUON2 = 3
    vLongitudesDeCodigos.giLongCodUON3 = 3
    vLongitudesDeCodigos.giLongCodUSU = 20
    vLongitudesDeCodigos.giLongCodACT = 20
    vLongitudesDeCodigos.giLongCodSUBACT1 = 20
    vLongitudesDeCodigos.giLongCodSUBACT2 = 20
    vLongitudesDeCodigos.giLongCodSUBACT3 = 20
    vLongitudesDeCodigos.giLongCodSUBACT4 = 20
    vLongitudesDeCodigos.giLongCodPRESCONCEP31 = 15
    vLongitudesDeCodigos.giLongCodPRESCONCEP32 = 15
    vLongitudesDeCodigos.giLongCodPRESCONCEP33 = 15
    vLongitudesDeCodigos.giLongCodPRESCONCEP34 = 15
    vLongitudesDeCodigos.giLongCodPRESCONCEP41 = 15
    vLongitudesDeCodigos.giLongCodPRESCONCEP42 = 15
    vLongitudesDeCodigos.giLongCodPRESCONCEP43 = 15
    vLongitudesDeCodigos.giLongCodPRESCONCEP44 = 15
    vLongitudesDeCodigos.giLongCodCAT1 = 20
    vLongitudesDeCodigos.giLongCodCAT2 = 20
    vLongitudesDeCodigos.giLongCodCAT3 = 20
    vLongitudesDeCodigos.giLongCodCAT4 = 20
    vLongitudesDeCodigos.giLongCodCAT5 = 20
    vLongitudesDeCodigos.giLongCodDIC = 3
    vLongitudesDeCodigos.giLongCodGRUPO = 3
    vLongitudesDeCodigos.giLongCodVarCal = 5
'no en bd
    vLongitudesDeCodigos.giLongCodCONTR = 10
    vLongitudesDeCodigos.giLongCodATRIB = 20
    vLongitudesDeCodigos.giLongCodPAR1 = 15
    vLongitudesDeCodigos.giLongCodPAR2 = 15
    vLongitudesDeCodigos.giLongCodPAR3 = 15
    vLongitudesDeCodigos.giLongCodPAR4 = 15
    
    
    vLongitudesDeCodigos.giLongCodPres5_0 = 15
    vLongitudesDeCodigos.giLongCodPres5_1 = 15
    vLongitudesDeCodigos.giLongCodPres5_2 = 15
    vLongitudesDeCodigos.giLongCodPres5_3 = 15
    vLongitudesDeCodigos.giLongCodPres5_4 = 15
    vLongitudesDeCodigos.giLongCodViaPago = 4
    vLongitudesDeCodigos.giLongCodUON4 = 3
    vLongitudesDeCodigos.giLongActivo = 20
    vLongitudesDeCodigos.giLongCentroCoste = 4
    
    vLongitudesDeCodigos.giFSGATareCod = 10
    vLongitudesDeCodigos.giFSGACatTareCod = 10
    vLongitudesDeCodigos.giFSGAProyCod = 10
    
    Set rdores = gRDOCon.OpenResultset("SELECT LONGITUD,NOMBRE FROM DIC ORDER BY ID", rdOpenForwardOnly, rdConcurReadOnly, rdExecDirect)
    
    While Not rdores.eof
    
    Select Case rdores(1).Value
    Case "ART"
        vLongitudesDeCodigos.giLongCodART = rdores(0).Value
    Case "CAL"
        vLongitudesDeCodigos.giLongCodCAL = rdores(0).Value
    Case "COM"
        vLongitudesDeCodigos.giLongCodCOM = rdores(0).Value
    Case "CON"
        vLongitudesDeCodigos.giLongCodCON = rdores(0).Value
    Case "DEP"
        vLongitudesDeCodigos.giLongCodDEP = rdores(0).Value
    Case "DEST"
        vLongitudesDeCodigos.giLongCodDEST = rdores(0).Value
    Case "EQP"
        vLongitudesDeCodigos.giLongCodEQP = rdores(0).Value
    Case "GMN1"
        vLongitudesDeCodigos.giLongCodGMN1 = rdores(0).Value
    Case "GMN2"
        vLongitudesDeCodigos.giLongCodGMN2 = rdores(0).Value
    Case "GMN3"
        vLongitudesDeCodigos.giLongCodGMN3 = rdores(0).Value
    Case "GMN4"
        vLongitudesDeCodigos.giLongCodGMN4 = rdores(0).Value
    Case "MON"
        vLongitudesDeCodigos.giLongCodMON = rdores(0).Value
    Case "OFEEST"
        vLongitudesDeCodigos.giLongCodOFEEST = rdores(0).Value
    Case "PAG"
        vLongitudesDeCodigos.giLongCodPAG = rdores(0).Value
    Case "PAI"
        vLongitudesDeCodigos.giLongCodPAI = rdores(0).Value
    Case "PER"
        vLongitudesDeCodigos.giLongCodPER = rdores(0).Value
    Case "PERF"
        vLongitudesDeCodigos.giLongCodPERF = rdores(0).Value
    Case "PRESCON1"
        vLongitudesDeCodigos.giLongCodPRESCON1 = rdores(0).Value
    Case "PRESCON2"
        vLongitudesDeCodigos.giLongCodPRESCON2 = rdores(0).Value
    Case "PRESCON3"
        vLongitudesDeCodigos.giLongCodPRESCON3 = rdores(0).Value
    Case "PRESCON4"
        vLongitudesDeCodigos.giLongCodPRESCON4 = rdores(0).Value
    Case "PRESPROY1"
        vLongitudesDeCodigos.giLongCodPRESPROY1 = rdores(0).Value
    Case "PRESPROY2"
        vLongitudesDeCodigos.giLongCodPRESPROY2 = rdores(0).Value
    Case "PRESPROY3"
        vLongitudesDeCodigos.giLongCodPRESPROY3 = rdores(0).Value
    Case "PRESPROY4"
        vLongitudesDeCodigos.giLongCodPRESPROY4 = rdores(0).Value
    Case "PROVE"
        vLongitudesDeCodigos.giLongCodPROVE = rdores(0).Value
    Case "PROVI"
        vLongitudesDeCodigos.giLongCodPROVI = rdores(0).Value
    Case "ROL"
        vLongitudesDeCodigos.giLongCodROL = rdores(0).Value
    Case "UNI"
        vLongitudesDeCodigos.giLongCodUNI = rdores(0).Value
    Case "UON1"
        vLongitudesDeCodigos.giLongCodUON1 = rdores(0).Value
    Case "UON2"
        vLongitudesDeCodigos.giLongCodUON2 = rdores(0).Value
    Case "UON3"
        vLongitudesDeCodigos.giLongCodUON3 = rdores(0).Value
    Case "USU"
        vLongitudesDeCodigos.giLongCodUSU = rdores(0).Value
    Case "ACT"
        vLongitudesDeCodigos.giLongCodACT = rdores(0).Value
    Case "SUBACT1"
        vLongitudesDeCodigos.giLongCodSUBACT1 = rdores(0).Value
    Case "SUBACT2"
        vLongitudesDeCodigos.giLongCodSUBACT2 = rdores(0).Value
    Case "SUBACT3"
        vLongitudesDeCodigos.giLongCodSUBACT3 = rdores(0).Value
    Case "SUBACT4"
        vLongitudesDeCodigos.giLongCodSUBACT4 = rdores(0).Value
    Case "PRESCONCEP31"
        vLongitudesDeCodigos.giLongCodPRESCONCEP31 = rdores(0).Value
    Case "PRESCONCEP32"
        vLongitudesDeCodigos.giLongCodPRESCONCEP32 = rdores(0).Value
    Case "PRESCONCEP33"
        vLongitudesDeCodigos.giLongCodPRESCONCEP33 = rdores(0).Value
    Case "PRESCONCEP34"
        vLongitudesDeCodigos.giLongCodPRESCONCEP34 = rdores(0).Value
    Case "PRESCONCEP41"
        vLongitudesDeCodigos.giLongCodPRESCONCEP41 = rdores(0).Value
    Case "PRESCONCEP42"
        vLongitudesDeCodigos.giLongCodPRESCONCEP42 = rdores(0).Value
    Case "PRESCONCEP43"
        vLongitudesDeCodigos.giLongCodPRESCONCEP43 = rdores(0).Value
    Case "PRESCONCEP44"
        vLongitudesDeCodigos.giLongCodPRESCONCEP44 = rdores(0).Value
    Case "CAT1"
        vLongitudesDeCodigos.giLongCodCAT1 = rdores(0).Value
    Case "CAT2"
        vLongitudesDeCodigos.giLongCodCAT2 = rdores(0).Value
    Case "CAT3"
        vLongitudesDeCodigos.giLongCodCAT3 = rdores(0).Value
    Case "CAT4"
        vLongitudesDeCodigos.giLongCodCAT4 = rdores(0).Value
    Case "CAT5"
        vLongitudesDeCodigos.giLongCodCAT5 = rdores(0).Value
    Case "GRUPO"
        vLongitudesDeCodigos.giLongCodGRUPO = rdores(0).Value
    Case "DIC"
        vLongitudesDeCodigos.giLongCodDIC = rdores(0).Value
    Case "CONTR"
        vLongitudesDeCodigos.giLongCodCONTR = rdores(0).Value
    Case "VARCAL"
        vLongitudesDeCodigos.giLongCodVarCal = rdores(0).Value
       
    Case "PRES5_0"
        vLongitudesDeCodigos.giLongCodPres5_0 = rdores(0).Value
    Case "PRES5_1"
        vLongitudesDeCodigos.giLongCodPres5_1 = rdores(0).Value
    Case "PRES5_2"
        vLongitudesDeCodigos.giLongCodPres5_2 = rdores(0).Value
    Case "PRES5_3"
        vLongitudesDeCodigos.giLongCodPres5_3 = rdores(0).Value
    Case "PRES5_4"
        vLongitudesDeCodigos.giLongCodPres5_4 = rdores(0).Value
    Case "VIA_PAG"
        vLongitudesDeCodigos.giLongCodViaPago = rdores(0).Value
    Case "UON4"
        vLongitudesDeCodigos.giLongCodUON4 = rdores(0).Value
    Case "ACTIVO"
        vLongitudesDeCodigos.giLongActivo = rdores(0).Value
    Case "CENTROCOSTE"
        vLongitudesDeCodigos.giLongCentroCoste = rdores(0).Value
        
    Case "FSGA_TARE_COD"
        vLongitudesDeCodigos.giFSGATareCod = rdores(0).Value
    Case "FSGA_CAT_TARE_COD"
        vLongitudesDeCodigos.giFSGACatTareCod = rdores(0).Value
    Case "FSGA_PROY_COD"
        vLongitudesDeCodigos.giFSGAProyCod = rdores(0).Value
        
    End Select
    
    rdores.MoveNext
    Wend
    rdores.Close
    Set rdores = Nothing
        

    DevolverLongitudesDeCodigos = vLongitudesDeCodigos
        
End Function



Public Sub ExecuteSQL(ByRef rdoCon As rdoConnection, ByVal sql As String)
'On Error GoTo errorExecuteRDO
#If ADISCO Then
    Open App.Path & "\sql.sql" For Append As #1
    Print #1, sql
    Print #1, "GO"
    Close #1
#Else
    If rdoCon.QueryTimeout < 400 And rdoCon.QueryTimeout <> 0 Then
        rdoCon.QueryTimeout = 400
    End If
    
    rdoCon.Execute sql, rdExecDirect
#End If

If frmProgreso.ProgressBar1.Value >= frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
DoEvents

'Exit Sub
'
'errorExecuteRDO:
'
'Dim i
'
'For i = 0 To rdoErrors.Count - 1
'    MsgBox rdoErrors.Item(i).Description
'Next i
'
End Sub

Public Sub ExecuteADOSQL(ByRef ADOCon As ADODB.Connection, ByVal sql As String)
#If ADISCO Then
    Open App.Path & "\sql.sql" For Append As #1
    Print #1, sql
    Print #1, "GO"
    Close #1
#Else
    ADOCon.CommandTimeout = 400
    ADOCon.Execute sql
#End If

If frmProgreso.ProgressBar1.Value >= frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
DoEvents
End Sub



'Public Sub ObtenerVersionYCollation()
'Dim oSrv As SQLDMO.SQLServer
'
'On Error Resume Next
'
'Set oSrv = New SQLDMO.SQLServer
'oSrv.Connect gServer, gUID, gPWD  'Se conecta
'
'gSQLVersion = oSrv.VersionMajor
'
'oSrv.Close
'Set oSrv = Nothing
'
'If gSQLVersion <= 7 Then
'    gCollation = ""
'Else
'    gCollation = "SQL_Latin1_General_CP1_CI_AS"
'End If
'
'
'End Sub
'

Public Function NumTransaccionesAbiertas(oCon As RDO.rdoConnection) As Integer

Dim sConsulta As String
Dim rdores As RDO.rdoResultset

sConsulta = "SELECT @@TRANCOUNT NUMTRAN"

    
Set rdores = oCon.OpenResultset(sConsulta, rdOpenKeyset, rdConcurRowVer)

NumTransaccionesAbiertas = rdores("NUMTRAN").Value

rdores.Close
Set rdores = Nothing

End Function

Public Function NumTransaccionesAbiertasADO(oCon As ADODB.Connection) As Integer

Dim sConsulta As String
Dim adoRS As ADODB.Recordset

sConsulta = "SELECT @@TRANCOUNT NUMTRAN"

    
Set adoRS = New ADODB.Recordset

adoRS.Open sConsulta, oCon, adOpenForwardOnly

NumTransaccionesAbiertasADO = adoRS("NUMTRAN").Value

adoRS.Close
Set adoRS = Nothing

End Function


Public Function BooleanToSQLBinary(ByVal b As Variant) As Variant
    If IsNull(b) Then
        BooleanToSQLBinary = "NULL"
    Else
        If b Then
            BooleanToSQLBinary = 1
        Else
            BooleanToSQLBinary = 0
        End If
    End If
    
End Function

Public Function EncriptarAES_319008(ByVal Seed As String, ByVal dato As String, ByVal Encriptando As Boolean, ByVal Fecha As Variant _
, ByVal Quien As Integer, Optional ByVal Tipo As Integer) As String
    Dim fechahoracrypt As Date
    Dim diacrypt
    Dim oCrypt2 As cCryptAES_319008
    
    If IsMissing(Fecha) Then
        Fecha = DateSerial(1974, 3, 5)
    Else
        If Fecha = "" Then
            Fecha = DateSerial(1974, 3, 5)
        End If
    End If
        
    fechahoracrypt = Fecha
    
    Set oCrypt2 = New cCryptAES_319008
    
    oCrypt2.InBuffer = dato
    
    If Quien = 0 Then
        oCrypt2.Codigo = Left(ClaveSQLtod, 6)
    ElseIf Quien = 1 Then
        diacrypt = Day(Fecha)
        
        If IsMissing(Tipo) Then 'CUsuario.IBaseDatos_CambiarCodigo
            If diacrypt Mod 2 = 0 Then
                oCrypt2.Codigo = Seed & Left(ClaveUSUpar, 6)
            Else
                oCrypt2.Codigo = Seed & Left(ClaveUSUimp, 6)
            End If
        Else
            If diacrypt Mod 2 = 0 Then
                If Tipo = 1 Then
                    oCrypt2.Codigo = Seed & Left(ClaveADMpar, 6)
                Else
                    oCrypt2.Codigo = Seed & Left(ClaveUSUpar, 6)
                End If
            Else
                If Tipo = 1 Then
                    oCrypt2.Codigo = Seed & Left(ClaveADMimp, 6)
                Else
                    oCrypt2.Codigo = Seed & Left(ClaveUSUimp, 6)
                End If
            End If
        End If
    Else
        oCrypt2.Codigo = Seed & Left(ClaveOBJtod, 6)
    End If
        
    oCrypt2.Fecha = fechahoracrypt
    
    'Datos
    If Encriptando Then
        oCrypt2.Encrypt
    Else
        oCrypt2.Decrypt
    End If
    
    EncriptarAES_319008 = oCrypt2.OutBuffer
                            
    Set oCrypt2 = Nothing
End Function

Public Function EncriptarAccesoFSWS_319008(ByVal sCadena As String, ByVal sDato As String, ByVal Encriptando As Boolean) As String
    Dim oCrypt2 As cCryptAES_319008
        
    Set oCrypt2 = New cCryptAES_319008
    
    oCrypt2.InBuffer = sDato
    If Len(sCadena) Mod 2 = 0 Then
        oCrypt2.Codigo = "agkag�"
    Else
        oCrypt2.Codigo = "h+hlL_"
    End If
    oCrypt2.Fecha = DateSerial(1974, 3, 5)
    
    If Encriptando Then
        oCrypt2.Encrypt
    Else
        oCrypt2.Decrypt
    End If
            
    EncriptarAccesoFSWS_319008 = oCrypt2.OutBuffer
    
    Set oCrypt2 = Nothing
End Function
