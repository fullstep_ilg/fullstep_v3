Attribute VB_Name = "bas_V_2_10_5"
Option Explicit
Dim oDMOBD As SQLDMO.Database


Public Function CodigoDeActualizacion2_10_00_15A2_10_50_0() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String

Dim sConexion As String
Dim sServer As String
Dim sBD As String
Dim sUsu As String
Dim sPwd As String

Dim oSQLServer As SQLDMO.SQLServer
sConexion = gRDOCon.Connect
sServer = gServer

sBD = gBD
sUsu = gUID
sPwd = gPWD
    
Set oSQLServer = New SQLDMO.SQLServer
oSQLServer.Connect sServer, sUsu, sPwd  'Se conecta


Set oDMOBD = oSQLServer.Databases(sBD)

On Error GoTo 0
    

gRDOCon.QueryTimeout = 120


sConsulta = "BEGIN TRANSACTION"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "SET XACT_ABORT OFF"
ExecuteSQL gRDOCon, sConsulta

bTransaccionEnCurso = True

'Creamos las nuevas FKs
frmProgreso.lblDetalle = "Se actualizar�n los defaults de PARGEN_GEST"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value > 9900 Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_10_5_Defaults_1
Set oDMOBD = Nothing

oSQLServer.Close
Set oSQLServer = Nothing

'Creamos tablas nuevas
frmProgreso.lblDetalle = "Se actualizar� la estructura de tablas a la versi�n 2.10.5"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value > 9900 Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_10_5_Crear_Tablas_1

'Modificamos tablas viejas
frmProgreso.lblDetalle = "Creaci�n de tablas de la versi�n 2.10.5"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value > 9900 Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_10_5_Actualizar_Tablas_1

frmProgreso.lblDetalle = "Se generar�n los triggers de la versi�n 2.10.5"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value > 9900 Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_10_5_Triggers_1

'Creamos los nuevos storeds
frmProgreso.lblDetalle = "Se generar�n las stored procedure de la versi�n 2.10.5"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value > 9900 Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
V_2_10_5_Storeds_1

'Creamos los nuevos storeds
frmProgreso.lblDetalle = "Se generar�n los triggers de la versi�n 2.10.5"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value > 9900 Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
V_2_10_5_Storeds_2

sConsulta = "UPDATE VERSION SET NUM ='2.10.50.00'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "COMMIT TRANSACTION"

ExecuteSQL gRDOCon, sConsulta

bTransaccionEnCurso = False

CodigoDeActualizacion2_10_00_15A2_10_50_0 = True


Exit Function
    
error:
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    If bTransaccionEnCurso Then
        sConsulta = "ROLLBACK TRANSACTION"
        ExecuteSQL gRDOCon, sConsulta
    End If
    
    CodigoDeActualizacion2_10_00_15A2_10_50_0 = False

End Function

Private Sub V_2_10_5_Crear_Tablas_1()
Dim sConsulta As String

'Tabla SOLICIT
sConsulta = "CREATE TABLE [dbo].[SOLICIT] (" & vbCrLf
sConsulta = sConsulta & "   [ID] [int] IDENTITY (1, 1) NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [DEN] [varchar] (50) NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [DESCR] [varchar] (500) NULL ," & vbCrLf
sConsulta = sConsulta & "   [IMPORTE] [float] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [MON] [varchar] (" & gLongitudesDeCodigos.giLongCodMON & ") NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [CAMBIO] [float] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [PROVEEDORES] [varchar] (300) NULL ," & vbCrLf
sConsulta = sConsulta & "   [CARGO] [varchar] (100) NULL ," & vbCrLf
sConsulta = sConsulta & "   [FECNEC] [datetime] NULL ," & vbCrLf
sConsulta = sConsulta & "   [DEST] [varchar] (" & gLongitudesDeCodigos.giLongCodDEST & ") NULL ," & vbCrLf
sConsulta = sConsulta & "   [PER] [varchar] (" & gLongitudesDeCodigos.giLongCodPER & ") NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [ESTADO] [tinyint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [COMPRADOR] [varchar] (" & gLongitudesDeCodigos.giLongCodCOM & ") NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [FECALTA] [datetime] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [SOLICIT_EST] [int] NULL ," & vbCrLf
sConsulta = sConsulta & "   [FECACT] [datetime] NULL " & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE [dbo].[SOLICIT] WITH NOCHECK ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [PK_SOLICIT] PRIMARY KEY  CLUSTERED " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )  ON [PRIMARY] " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 
sConsulta = "ALTER TABLE [dbo].[SOLICIT] ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [FK_SOLICIT_PER] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [PER]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[PER] (" & vbCrLf
sConsulta = sConsulta & "       [COD]" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
  
sConsulta = "CREATE TRIGGER SOLICIT_TG_INSUPD ON dbo.SOLICIT " & vbCrLf
sConsulta = sConsulta & "FOR UPDATE, INSERT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF NOT UPDATE(FECACT)" & vbCrLf
sConsulta = sConsulta & "  UPDATE SOLICIT" & vbCrLf
sConsulta = sConsulta & "     SET FECACT = GETDATE()" & vbCrLf
sConsulta = sConsulta & "    FROM SOLICIT S" & vbCrLf
sConsulta = sConsulta & "      INNER JOIN INSERTED I " & vbCrLf
sConsulta = sConsulta & "              ON S.ID = I.ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 
'Tabla SOLICIT_ADJUN
sConsulta = "CREATE TABLE [dbo].[SOLICIT_ADJUN] (" & vbCrLf
sConsulta = sConsulta & "   [ID] [int] IDENTITY (1, 1) NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [SOLICIT] [int] NULL ," & vbCrLf
sConsulta = sConsulta & "   [FECHA] [datetime] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [NOM] [varchar] (300) NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [COM] [varchar] (500) NULL ," & vbCrLf
sConsulta = sConsulta & "   [PER] [varchar] (" & gLongitudesDeCodigos.giLongCodPER & ") NULL ," & vbCrLf
sConsulta = sConsulta & "   [DATA] [image] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [FECACT] [datetime] NULL " & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "ALTER TABLE [dbo].[SOLICIT_ADJUN] WITH NOCHECK ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [PK_SOLICIT_ADJUN] PRIMARY KEY  CLUSTERED " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )  ON [PRIMARY] " & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "ALTER TABLE [dbo].[SOLICIT_ADJUN] ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [FK_SOLICIT_ADJUN_PER] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [PER]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[PER] (" & vbCrLf
sConsulta = sConsulta & "       [COD]" & vbCrLf
sConsulta = sConsulta & "   )," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [FK_SOLICIT_ADJUN_SOLICIT] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [SOLICIT]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[SOLICIT] (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 
sConsulta = ""
sConsulta = sConsulta & "CREATE TRIGGER SOLICIT_ADJUN_TG_INSUPD ON dbo.SOLICIT_ADJUN " & vbCrLf
sConsulta = sConsulta & "FOR UPDATE,INSERT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF NOT UPDATE(FECACT)" & vbCrLf
sConsulta = sConsulta & "  UPDATE SOLICIT_ADJUN" & vbCrLf
sConsulta = sConsulta & "     SET FECACT = GETDATE()" & vbCrLf
sConsulta = sConsulta & "    FROM SOLICIT_ADJUN SA" & vbCrLf
sConsulta = sConsulta & "      INNER JOIN INSERTED I " & vbCrLf
sConsulta = sConsulta & "              ON SA.ID = I.ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

'Tabla SOLICIT_ASIG_COMP
sConsulta = "CREATE TABLE [dbo].[SOLICIT_ASIG_COMP] (" & vbCrLf
sConsulta = sConsulta & "   [ID] [int] IDENTITY (1, 1) NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [SOLICIT] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [PER] [varchar] (" & gLongitudesDeCodigos.giLongCodPER & ") NULL ," & vbCrLf
sConsulta = sConsulta & "   [COM] [varchar] (" & gLongitudesDeCodigos.giLongCodCOM & ") NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [COMENT] [varchar] (500) NULL ," & vbCrLf
sConsulta = sConsulta & "   [FECHA] [datetime] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [FECACT] [datetime] NULL " & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 
sConsulta = ""
sConsulta = sConsulta & "ALTER TABLE [dbo].[SOLICIT_ASIG_COMP] WITH NOCHECK ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [PK_SOLICIT_ASIG_COMP] PRIMARY KEY  CLUSTERED " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )  ON [PRIMARY] " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 
sConsulta = ""
sConsulta = sConsulta & "ALTER TABLE [dbo].[SOLICIT_ASIG_COMP] ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [FK_SOLICIT_ASIG_COMP_PER] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [PER]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[PER] (" & vbCrLf
sConsulta = sConsulta & "       [COD]" & vbCrLf
sConsulta = sConsulta & "   )," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [FK_SOLICIT_ASIG_COMP_SOLICIT] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [SOLICIT]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[SOLICIT] (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

'Tabla SOLICIT_EST
sConsulta = "CREATE TABLE [dbo].[SOLICIT_EST] (" & vbCrLf
sConsulta = sConsulta & "   [ID] [int] IDENTITY (1, 1) NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [SOLICIT] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [PER] [varchar] (" & gLongitudesDeCodigos.giLongCodPER & ") NULL ," & vbCrLf
sConsulta = sConsulta & "   [COMENT] [varchar] (500) NULL ," & vbCrLf
sConsulta = sConsulta & "   [FECHA] [datetime] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [EST] [tinyint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [FECACT] [datetime] NULL " & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "ALTER TABLE [dbo].[SOLICIT_EST] WITH NOCHECK ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [PK_SOLICIT_EST] PRIMARY KEY  CLUSTERED " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )  ON [PRIMARY] " & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "ALTER TABLE [dbo].[SOLICIT_EST] ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [FK_SOLICIT_EST_PER] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [PER]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[PER] (" & vbCrLf
sConsulta = sConsulta & "       [COD]" & vbCrLf
sConsulta = sConsulta & "   )," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [FK_SOLICIT_EST_SOLICIT] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [SOLICIT]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[SOLICIT] (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE TRIGGER SOLICIT_EST_TG_INSUPD ON dbo.SOLICIT_EST " & vbCrLf
sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  UPDATE SOLICIT_EST" & vbCrLf
sConsulta = sConsulta & "     SET FECACT = GETDATE()" & vbCrLf
sConsulta = sConsulta & "    FROM SOLICIT_EST SE" & vbCrLf
sConsulta = sConsulta & "      INNER JOIN INSERTED I " & vbCrLf
sConsulta = sConsulta & "              ON SE.ID = I.ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE TRIGGER SOLICIT_EST_TG_INS ON dbo.SOLICIT_EST " & vbCrLf
sConsulta = sConsulta & "FOR INSERT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  UPDATE SOLICIT" & vbCrLf
sConsulta = sConsulta & "     SET SOLICIT_EST = I.ID" & vbCrLf
sConsulta = sConsulta & "    FROM SOLICIT S " & vbCrLf
sConsulta = sConsulta & "      INNER JOIN INSERTED I " & vbCrLf
sConsulta = sConsulta & "              ON S.ID = I.SOLICIT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

'Inserto en la tabla DIC
sConsulta = "INSERT INTO DIC (ID,NOMBRE,LONGITUD) VALUES (470,'CONTR',10)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

'Tabla CONTR y CONTR_PLANTILLAS
sConsulta = "CREATE TABLE [dbo].[CONTR] (" & vbCrLf
sConsulta = sConsulta & "   [COD] [varchar] (" & gLongitudesDeCodigos.giLongCodCONTR & ") NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [DEN] [varchar] (100) NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [FECACT] [datetime] NULL " & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE TABLE [dbo].[CONTR_PLANTILLAS] (" & vbCrLf
sConsulta = sConsulta & "   [CONTRATO] [varchar] (" & gLongitudesDeCodigos.giLongCodCONTR & ") NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [IDI] [varchar] (3) NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [PLANTILLA] [varchar] (255) NULL " & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 
sConsulta = ""
sConsulta = sConsulta & "ALTER TABLE [dbo].[CONTR] WITH NOCHECK ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [PK_CONTR] PRIMARY KEY  CLUSTERED " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [COD]" & vbCrLf
sConsulta = sConsulta & "   )  ON [PRIMARY] " & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "ALTER TABLE [dbo].[CONTR_PLANTILLAS] WITH NOCHECK ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [PK_CONTR_PLANTILLAS] PRIMARY KEY  CLUSTERED " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [CONTRATO]," & vbCrLf
sConsulta = sConsulta & "       [IDI]" & vbCrLf
sConsulta = sConsulta & "   )  ON [PRIMARY] " & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "ALTER TABLE [dbo].[CONTR_PLANTILLAS] ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [FK_CONTR_PLANTILLAS_CONTR] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [CONTRATO]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[CONTR] (" & vbCrLf
sConsulta = sConsulta & "       [COD]" & vbCrLf
sConsulta = sConsulta & "   )," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [FK_CONTR_PLANTILLAS_IDIOMAS] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [IDI]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[IDIOMAS] (" & vbCrLf
sConsulta = sConsulta & "       [COD]" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE TRIGGER CONTR_TG_INSUPD ON dbo.CONTR" & vbCrLf
sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_CONTR_Ins CURSOR LOCAL  FOR SELECT COD FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_CONTR_Ins" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_CONTR_Ins INTO @COD" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "UPDATE CONTR SET FECACT=GETDATE() WHERE COD=@COD" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_CONTR_Ins INTO @COD" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_CONTR_Ins" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_CONTR_Ins" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

'Tabla PROCE_CONTR
sConsulta = "CREATE TABLE [dbo].[PROCE_CONTR] (" & vbCrLf
sConsulta = sConsulta & "   [ID] [int] IDENTITY (1, 1) NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [ANYO] [smallint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [GMN1] [varchar] (" & gLongitudesDeCodigos.giLongCodGMN1 & ") NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [PROCE] [smallint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [PROVE] [varchar] (" & gLongitudesDeCodigos.giLongCodPROVE & ") NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [NOM] [varchar] (200) NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [EXT] [varchar] (3) NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [DATA] [image] NULL ," & vbCrLf
sConsulta = sConsulta & "   [USU] [varchar] (" & gLongitudesDeCodigos.giLongCodUSU & ") NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [FECACT] [datetime] NULL " & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "ALTER TABLE [dbo].[PROCE_CONTR] WITH NOCHECK ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [PK_PROCE_CONTR] PRIMARY KEY  CLUSTERED " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )  ON [PRIMARY] " & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "ALTER TABLE [dbo].[PROCE_CONTR] WITH NOCHECK ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [UC_PROCE_CONTR] UNIQUE  NONCLUSTERED " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [ANYO]," & vbCrLf
sConsulta = sConsulta & "       [GMN1]," & vbCrLf
sConsulta = sConsulta & "       [PROCE]," & vbCrLf
sConsulta = sConsulta & "       [PROVE]," & vbCrLf
sConsulta = sConsulta & "       [NOM]" & vbCrLf
sConsulta = sConsulta & "   )  ON [PRIMARY] " & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "ALTER TABLE [dbo].[PROCE_CONTR] ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [FK_PROCE_CONTR_PROCE_PROVE] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [ANYO]," & vbCrLf
sConsulta = sConsulta & "       [GMN1]," & vbCrLf
sConsulta = sConsulta & "       [PROCE]," & vbCrLf
sConsulta = sConsulta & "       [PROVE]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[PROCE_PROVE] (" & vbCrLf
sConsulta = sConsulta & "       [ANYO]," & vbCrLf
sConsulta = sConsulta & "       [GMN1]," & vbCrLf
sConsulta = sConsulta & "       [PROCE]," & vbCrLf
sConsulta = sConsulta & "       [PROVE]" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE TRIGGER PROCE_CONTR_TG_INSUPD ON dbo.PROCE_CONTR" & vbCrLf
sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID INT" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_PROCE_CONTR_Ins CURSOR LOCAL FOR SELECT ID FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_PROCE_CONTR_Ins" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_CONTR_Ins INTO @ID" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_CONTR SET FECACT=GETDATE() WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_CONTR_Ins INTO @ID" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_PROCE_CONTR_Ins" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_PROCE_CONTR_Ins" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Private Function V_2_10_5_Actualizar_Tablas_1()
Dim sConsulta As String
Dim iEntidadesInt As Integer

'Tablas WEBTEXT y FSEPTEXT
sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[WEBTEXT]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)" & vbCrLf
sConsulta = sConsulta & "drop table [dbo].[WEBTEXT]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TABLE [dbo].[WEBTEXT](" & vbCrLf
sConsulta = sConsulta & "   [MODULO] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [ID] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [OBS] [text] NULL ," & vbCrLf
sConsulta = sConsulta & "   [TEXT_SPA] [varchar] (2000) NULL ," & vbCrLf
sConsulta = sConsulta & "   [TEXT_ENG] [varchar] (2000) NULL " & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE [dbo].[WEBTEXT] WITH NOCHECK ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [PK_WEBTEXT] PRIMARY KEY  CLUSTERED " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [MODULO]," & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   ) WITH  FILLFACTOR = 90  ON [PRIMARY] " & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[WEBFSEPTEXT]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)" & vbCrLf
sConsulta = sConsulta & "drop table [dbo].[WEBFSEPTEXT]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 
sConsulta = "CREATE TABLE [dbo].[WEBFSEPTEXT] (" & vbCrLf
sConsulta = sConsulta & "   [MODULO] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [ID] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [OBS] [text] NULL ," & vbCrLf
sConsulta = sConsulta & "   [TEXT_SPA] [varchar] (2000) NULL ," & vbCrLf
sConsulta = sConsulta & "   [TEXT_ENG] [varchar] (2000) NULL " & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 
sConsulta = "ALTER TABLE [dbo].[WEBFSEPTEXT] WITH NOCHECK ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [PK_WEBFSEPTEXT] PRIMARY KEY  CLUSTERED " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [MODULO]," & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   ) WITH  FILLFACTOR = 90  ON [PRIMARY] " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 
'Tabla PROCE_GRUPO_DEF
sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CK_PROCE_GRUPO_DEF]') )" & vbCrLf
sConsulta = sConsulta & "  ALTER TABLE [dbo].[PROCE_GRUPO_DEF] DROP CONSTRAINT CK_PROCE_GRUPO_DEF" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE [dbo].[PROCE_GRUPO_DEF] WITH NOCHECK ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [CK_PROCE_GRUPO_DEF] CHECK ([DATO] = 'ESP' or ([DATO] = 'PRES2' or ([DATO] = 'PRES1' or ([DATO] = 'PRESANU2' or ([DATO] = 'PRESANU1' or ([DATO] = 'DIST' or ([DATO] = 'PROVE' or ([DATO] = 'FECSUM' or ([DATO] = 'PAG' or [DATO] = 'DEST' or [DATO] = 'SOLICIT')))))))))" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

'Tabla PROCE_GRUPO
sConsulta = "  ALTER TABLE PROCE_GRUPO ADD SOLICIT INT NULL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE [dbo].[PROCE_GRUPO] ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [FK_PROCE_GRUPO_SOLICIT] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [SOLICIT]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[SOLICIT] (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

'Tabla PROCE_DEF
sConsulta = "  ALTER TABLE [dbo].[PROCE_DEF] ADD [SOLICIT] [tinyint] NOT NULL CONSTRAINT [DF_PROCE_DEF_SOLICIT] DEFAULT 3"
ExecuteSQL gRDOCon, sConsulta

'Tabla PROCE
sConsulta = "  ALTER TABLE [dbo].[PROCE] ADD [SOLICIT] [int] NULL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "  ALTER TABLE [dbo].[PROCE] ADD"
sConsulta = sConsulta & " CONSTRAINT [FK_PROCE_SOLICIT] FOREIGN KEY"
sConsulta = sConsulta & "     ("
sConsulta = sConsulta & "         [SOLICIT]"
sConsulta = sConsulta & "     ) REFERENCES [dbo].[SOLICIT] ("
sConsulta = sConsulta & "         [ID]"
sConsulta = sConsulta & "     )"
ExecuteSQL gRDOCon, sConsulta

'Tabla PLANTILLA_GR_DEF
sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CK_PLANTILLA_GR_DEF]') )" & vbCrLf
sConsulta = sConsulta & "  ALTER TABLE [dbo].[PLANTILLA_GR_DEF] DROP CONSTRAINT CK_PLANTILLA_GR_DEF" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE [dbo].[PLANTILLA_GR_DEF] WITH NOCHECK ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [CK_PLANTILLA_GR_DEF] CHECK ([DATO] = 'ESP' or ([DATO] = 'PRES2' or ([DATO] = 'PRES1' or ([DATO] = 'PRESANU2' or ([DATO] = 'PRESANU1' or ([DATO] = 'DIST' or ([DATO] = 'PROVE' or ([DATO] = 'FECSUM' or ([DATO] = 'PAG' or ([DATO] = 'DEST' or [DATO] = 'SOLICIT'))))))))))"
ExecuteSQL gRDOCon, sConsulta

'Tabla PLANTILLA
sConsulta = "  ALTER TABLE [dbo].[PLANTILLA] ADD [SOLICIT] [tinyint] NOT NULL CONSTRAINT [DF_PLANTILLA_SOLICIT] DEFAULT 0"
ExecuteSQL gRDOCon, sConsulta

'Tabla PARGEN_INTERNO
sConsulta = "  ALTER TABLE [dbo].[PARGEN_INTERNO] ADD [SOL_COMPRA] [tinyint] NOT NULL CONSTRAINT [DF_PARGEN_INTERNO_SOL_COMPRA] DEFAULT 0"
ExecuteSQL gRDOCon, sConsulta

'Tabla PARGEN_GEST
sConsulta = "  ALTER TABLE [dbo].[PARGEN_GEST] ADD "
sConsulta = sConsulta & " [SOLICIPATH] [varchar] (255) NULL,"
sConsulta = sConsulta & " [SOLICIT] [tinyint] NOT NULL CONSTRAINT [DF_PARGEN_GEST_SOLICIT] DEFAULT 3"
ExecuteSQL gRDOCon, sConsulta

'Tabla LINEAS_PEDIDO
sConsulta = "  ALTER TABLE [dbo].[LINEAS_PEDIDO] ADD "
sConsulta = sConsulta & " [SOLICIT] [INT] NULL"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "  ALTER TABLE [dbo].[LINEAS_PEDIDO] ADD"
sConsulta = sConsulta & " CONSTRAINT [FK_LINEAS_PEDIDO_SOLICIT] FOREIGN KEY"
sConsulta = sConsulta & "     ("
sConsulta = sConsulta & "         [SOLICIT]"
sConsulta = sConsulta & "     ) REFERENCES [dbo].[SOLICIT] ("
sConsulta = sConsulta & "         [ID]"
sConsulta = sConsulta & "     )"
ExecuteSQL gRDOCon, sConsulta

'Tabla ITEM
sConsulta = "  ALTER TABLE [dbo].[ITEM] ADD "
sConsulta = sConsulta & " [SOLICIT] [INT] NULL"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "  ALTER TABLE [dbo].[ITEM] ADD"
sConsulta = sConsulta & " CONSTRAINT [FK_ITEM_SOLICIT] FOREIGN KEY"
sConsulta = sConsulta & "     ("
sConsulta = sConsulta & "         [SOLICIT]"
sConsulta = sConsulta & "     ) REFERENCES [dbo].[SOLICIT] ("
sConsulta = sConsulta & "         [ID]"
sConsulta = sConsulta & "     )"
ExecuteSQL gRDOCon, sConsulta

'Tabla CATALOG_LIN
sConsulta = "  ALTER TABLE [dbo].[CATALOG_LIN] ADD "
sConsulta = sConsulta & " [SOLICIT] [INT] NULL"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "  ALTER TABLE [dbo].[CATALOG_LIN] ADD"
sConsulta = sConsulta & " CONSTRAINT [FK_CATALOG_LIN_SOLICIT] FOREIGN KEY"
sConsulta = sConsulta & "     ("
sConsulta = sConsulta & "         [SOLICIT]"
sConsulta = sConsulta & "     ) REFERENCES [dbo].[SOLICIT] ("
sConsulta = sConsulta & "         [ID]"
sConsulta = sConsulta & "     )"
ExecuteSQL gRDOCon, sConsulta

'Tabla TMP_CATALOGO
sConsulta = "  ALTER TABLE TMP_CATALOGO ADD SOLICIT INT NULL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Function

Private Function V_2_10_5_Triggers_1()
Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PROCE_GRUPO_TG_UPD_DATVAR]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PROCE_GRUPO_TG_UPD_DATVAR]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TRIGGER PROCE_GRUPO_TG_UPD_DATVAR ON dbo.PROCE_GRUPO" & vbCrLf
sConsulta = sConsulta & "FOR UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO SMALLINT,@GMN1 VARCHAR(5),@PROCE SMALLINT,@GRUPO VARCHAR(20),@DATO VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @DESTOLD VARCHAR(20),@DESTNEW VARCHAR(20)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PAGOLD VARCHAR(20),@PAGNEW VARCHAR(20)" & vbCrLf
sConsulta = sConsulta & "DECLARE @FECINIOLD DATETIME,@FECININEW DATETIME" & vbCrLf
sConsulta = sConsulta & "DECLARE @FECFINOLD DATETIME,@FECFINNEW DATETIME" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVEACTOLD VARCHAR(50), @PROVEACTNEW VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SOLICITOLD INT ,@SOLICITNEW INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_PROCE_GRUPO_Upd_DATVAR CURSOR LOCAL FOR " & vbCrLf
sConsulta = sConsulta & "       SELECT I.ANYO,I.GMN1,I.PROCE,I.COD,D.DEST,I.DEST,D.PAG,I.PAG,D.FECINI,I.FECINI,D.FECFIN,I.FECFIN,D.PROVEACT,I.PROVEACT ,D.SOLICIT,I.SOLICIT" & vbCrLf
sConsulta = sConsulta & "       FROM DELETED D INNER JOIN INSERTED I ON D.ANYO=I.ANYO AND D.GMN1=I.GMN1 AND D.PROCE=I.PROCE AND D.COD=I.COD" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_PROCE_GRUPO_Upd_DATVAR" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_GRUPO_Upd_DATVAR INTO @ANYO,@GMN1,@PROCE,@GRUPO,@DESTOLD,@DESTNEW,@PAGOLD,@PAGNEW,@FECINIOLD,@FECININEW,@FECFINOLD,@FECFINNEW,@PROVEACTOLD,@PROVEACTNEW, @SOLICITOLD ,@SOLICITNEW " & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "--DEST" & vbCrLf
sConsulta = sConsulta & "IF ISNULL(@DESTOLD,'')<>ISNULL(@DESTNEW,'')" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "         SELECT @DATO=DATO FROM PROCE_GRUPO_DEF WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO AND DATO='DEST'" & vbCrLf
sConsulta = sConsulta & "         IF @DATO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "         BEGIN" & vbCrLf
sConsulta = sConsulta & "       UPDATE ITEM SET DEST=@DESTNEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "         End" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "--PAG" & vbCrLf
sConsulta = sConsulta & "IF ISNULL(@PAGOLD,'')<>ISNULL(@PAGNEW,'')" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "         SELECT @DATO=DATO FROM PROCE_GRUPO_DEF WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO AND DATO='PAG'" & vbCrLf
sConsulta = sConsulta & "         IF @DATO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "         BEGIN" & vbCrLf
sConsulta = sConsulta & "       UPDATE ITEM SET PAG=@PAGNEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "         End" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "--FECINI" & vbCrLf
sConsulta = sConsulta & "IF ISNULL(@FECINIOLD,'')<>ISNULL(@FECININEW,'')" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "         SELECT @DATO=DATO FROM PROCE_GRUPO_DEF WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO AND DATO='FECSUM'" & vbCrLf
sConsulta = sConsulta & "         IF @DATO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "         BEGIN" & vbCrLf
sConsulta = sConsulta & "       UPDATE ITEM SET FECINI=@FECININEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "         End" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "--FECFIN" & vbCrLf
sConsulta = sConsulta & "IF ISNULL(@FECFINOLD,'')<>ISNULL(@FECFINNEW,'')" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "         SELECT @DATO=DATO FROM PROCE_GRUPO_DEF WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO AND DATO='FECSUM'" & vbCrLf
sConsulta = sConsulta & "         IF @DATO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "         BEGIN" & vbCrLf
sConsulta = sConsulta & "       UPDATE ITEM SET FECFIN=@FECFINNEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "         End" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "--PROVEACT" & vbCrLf
sConsulta = sConsulta & "IF ISNULL(@PROVEACTOLD,'')<>ISNULL(@PROVEACTNEW,'')" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "         SELECT @DATO=DATO FROM PROCE_GRUPO_DEF WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO AND DATO='PROVE'" & vbCrLf
sConsulta = sConsulta & "         IF @DATO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "         BEGIN" & vbCrLf
sConsulta = sConsulta & "       UPDATE ITEM SET PROVEACT=@PROVEACTNEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "         End" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "--SOLICIT" & vbCrLf
sConsulta = sConsulta & "IF ISNULL(@SOLICITOLD,'')<>ISNULL(@SOLICITNEW,'')" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "         SELECT @DATO=DATO FROM PROCE_GRUPO_DEF WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO AND DATO='SOLICIT'" & vbCrLf
sConsulta = sConsulta & "         IF @DATO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "         BEGIN" & vbCrLf
sConsulta = sConsulta & "       UPDATE ITEM SET SOLICIT=@SOLICITNEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "         End" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_GRUPO_Upd_DATVAR INTO @ANYO,@GMN1,@PROCE,@GRUPO,@DESTOLD,@DESTNEW,@PAGOLD,@PAGNEW,@FECINIOLD,@FECININEW,@FECFINOLD,@FECFINNEW,@PROVEACTOLD,@PROVEACTNEW, @SOLICITOLD ,@SOLICITNEW " & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_PROCE_GRUPO_Upd_DATVAR" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_PROCE_GRUPO_Upd_DATVAR" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PROCE_TG_UPD_DATVAR]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PROCE_TG_UPD_DATVAR]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TRIGGER PROCE_TG_UPD_DATVAR ON dbo.PROCE" & vbCrLf
sConsulta = sConsulta & "FOR UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO SMALLINT,@GMN1 VARCHAR(5),@PROCE SMALLINT,@DATO SMALLINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @DESTOLD VARCHAR(20),@DESTNEW VARCHAR(20)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PAGOLD VARCHAR(20),@PAGNEW VARCHAR(20)" & vbCrLf
sConsulta = sConsulta & "DECLARE @FECINIOLD DATETIME,@FECININEW DATETIME" & vbCrLf
sConsulta = sConsulta & "DECLARE @FECFINOLD DATETIME,@FECFINNEW DATETIME" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVEACTOLD VARCHAR(50)DECLARE @PROVEACTNEW VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SOLICITOLD INT,@SOLICITNEW INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_PROCE_Upd_DATVAR CURSOR LOCAL FOR " & vbCrLf
sConsulta = sConsulta & "SELECT I.ANYO,I.GMN1,I.COD,D.DEST,I.DEST,D.PAG,I.PAG,D.FECINI,I.FECINI,D.FECFIN,I.FECFIN,D.PROVEACT,I.PROVEACT,D.SOLICIT,I.SOLICIT" & vbCrLf
sConsulta = sConsulta & "FROM DELETED D " & vbCrLf
sConsulta = sConsulta & "INNER JOIN INSERTED I" & vbCrLf
sConsulta = sConsulta & "ON D.ANYO=I.ANYO AND D.GMN1=I.GMN1 AND D.COD=I.COD" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_PROCE_Upd_DATVAR" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_Upd_DATVAR INTO @ANYO,@GMN1,@PROCE, @DESTOLD,@DESTNEW,@PAGOLD,@PAGNEW,@FECINIOLD,@FECININEW,@FECFINOLD,@FECFINNEW,@PROVEACTOLD,@PROVEACTNEW,@SOLICITOLD,@SOLICITNEW" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "--DEST" & vbCrLf
sConsulta = sConsulta & "IF ISNULL(@DESTOLD,'')<>ISNULL(@DESTNEW,'')" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "         SELECT @DATO=DEST FROM PROCE_DEF WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "         IF @DATO=1 --Definido a nivel de proceso" & vbCrLf
sConsulta = sConsulta & "         BEGIN" & vbCrLf
sConsulta = sConsulta & "       UPDATE ITEM SET DEST=@DESTNEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "         End" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "--PAG" & vbCrLf
sConsulta = sConsulta & "IF ISNULL(@PAGOLD,'')<>ISNULL(@PAGNEW,'')" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "         SELECT @DATO=PAG FROM PROCE_DEF WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "         IF @DATO=1" & vbCrLf
sConsulta = sConsulta & "         BEGIN" & vbCrLf
sConsulta = sConsulta & "       UPDATE ITEM SET PAG=@PAGNEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "         End" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "--FECINI" & vbCrLf
sConsulta = sConsulta & "IF ISNULL(@FECINIOLD,'')<>ISNULL(@FECININEW,'')" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "         SELECT @DATO=FECSUM FROM PROCE_DEF WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "         IF @DATO=1" & vbCrLf
sConsulta = sConsulta & "         BEGIN" & vbCrLf
sConsulta = sConsulta & "       UPDATE ITEM SET FECINI=@FECININEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "         End" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "--FECFIN" & vbCrLf
sConsulta = sConsulta & "IF ISNULL(@FECFINOLD,'')<>ISNULL(@FECFINNEW,'')" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "         SELECT @DATO=FECSUM FROM PROCE_DEF WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "         IF @DATO=1" & vbCrLf
sConsulta = sConsulta & "         BEGIN" & vbCrLf
sConsulta = sConsulta & "       UPDATE ITEM SET FECFIN=@FECFINNEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "         End" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "--PROVEACT" & vbCrLf
sConsulta = sConsulta & "IF ISNULL(@PROVEACTOLD,'')<>ISNULL(@PROVEACTNEW,'')" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "         SELECT @DATO=PROVE FROM PROCE_DEF WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "         IF @DATO=1" & vbCrLf
sConsulta = sConsulta & "         BEGIN" & vbCrLf
sConsulta = sConsulta & "       UPDATE ITEM SET PROVEACT=@PROVEACTNEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "         End" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "--SOLICIT" & vbCrLf
sConsulta = sConsulta & "IF ISNULL(@SOLICITOLD,'')<>ISNULL(@SOLICITNEW,'')" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "         SELECT @DATO=SOLICIT FROM PROCE_DEF WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "         IF @DATO=1 --Definido a nivel de proceso" & vbCrLf
sConsulta = sConsulta & "         BEGIN" & vbCrLf
sConsulta = sConsulta & "       UPDATE ITEM SET SOLICIT=@SOLICITNEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "         End" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_Upd_DATVAR INTO @ANYO,@GMN1,@PROCE, @DESTOLD,@DESTNEW,@PAGOLD,@PAGNEW,@FECINIOLD,@FECININEW,@FECFINOLD,@FECFINNEW,@PROVEACTOLD,@PROVEACTNEW,@SOLICITOLD,@SOLICITNEW" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_PROCE_Upd_DATVAR" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_PROCE_Upd_DATVAR" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ITEM_TG_INS_DATVAR]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[ITEM_TG_INS_DATVAR]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TRIGGER ITEM_TG_INS_DATVAR ON ITEM " & vbCrLf
sConsulta = sConsulta & "FOR INSERT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO SMALLINT,@GMN1 VARCHAR(5),@PROCE SMALLINT,@ITEM INTEGER,@GRUPO VARCHAR(5),@DATO VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @DEFPROVE SMALLINT,@DEFDISTUON SMALLINT,@DEFSOLICIT SMALLINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @DEFPRESANU1 SMALLINT,@DEFPRESANU2 SMALLINT,@DEFPRES1 SMALLINT,@DEFPRES2 SMALLINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVE VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @UON1 VARCHAR(5),@UON2 VARCHAR(5),@UON3 VARCHAR(5),@PORCEN FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYOPRES SMALLINT,@PRES1 VARCHAR(50),@PRES2 VARCHAR(50),@PRES3 VARCHAR(50),@PRES4 VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SUMA AS INTEGER,@SOLICIT INTEGER" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_ITEM_Ins_DATVAR CURSOR LOCAL FOR SELECT ANYO,GMN1,PROCE,ID,GRUPO FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_ITEM_Ins_DATVAR" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_ITEM_Ins_DATVAR INTO @ANYO,@GMN1,@PROCE,@ITEM,@GRUPO" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SELECT @DEFPROVE=PROVE,@DEFDISTUON=DIST" & vbCrLf
sConsulta = sConsulta & "    ,@DEFPRESANU1=PRESANU1,@DEFPRESANU2=PRESANU2,@DEFPRES1=PRES1,@DEFPRES2=PRES2" & vbCrLf
sConsulta = sConsulta & "   FROM PROCE_DEF WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- DEST,PAG Y FECSUM no es necesario actualizarlas porque al no poder" & vbCrLf
sConsulta = sConsulta & "-- introducir valores nulos en ITEM se hace desde la clase" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  --SOLICIT" & vbCrLf
sConsulta = sConsulta & "   IF @DEFSOLICIT=1 --Definido a nivel de proceso" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "       SELECT @SOLICIT=SOLICIT FROM PROCE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "           UPDATE ITEM SET SOLICIT=@SOLICIT WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ID=@ITEM" & vbCrLf
sConsulta = sConsulta & "        END" & vbCrLf
sConsulta = sConsulta & "   IF @DEFSOLICIT=2 --Definido a nivel de grupo" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "       SELECT @DATO=DATO FROM PROCE_GRUPO_DEF WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO AND DATO='SOLICIT'" & vbCrLf
sConsulta = sConsulta & "           IF @DATO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "               SELECT @SOLICIT=SOLICIT FROM PROCE_GRUPO WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND COD=@GRUPO" & vbCrLf
sConsulta = sConsulta & "           UPDATE ITEM SET SOLICIT=@SOLICIT WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ID=@ITEM" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "        END  " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   --PROVEACT" & vbCrLf
sConsulta = sConsulta & "   IF @DEFPROVE=1 --Definido a nivel de proceso" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "       SELECT @PROVE=PROVEACT FROM PROCE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "           UPDATE ITEM SET PROVEACT=@PROVE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ID=@ITEM" & vbCrLf
sConsulta = sConsulta & "        END" & vbCrLf
sConsulta = sConsulta & "   IF @DEFPROVE=2 --Definido a nivel de grupo" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "       SELECT @DATO=DATO FROM PROCE_GRUPO_DEF WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO AND DATO='PROVE'" & vbCrLf
sConsulta = sConsulta & "           IF @DATO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "               SELECT @PROVE=PROVEACT FROM PROCE_GRUPO WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND COD=@GRUPO" & vbCrLf
sConsulta = sConsulta & "           UPDATE ITEM SET PROVEACT=@PROVE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ID=@ITEM" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "        END  " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   --DISTUON" & vbCrLf
sConsulta = sConsulta & "   IF @DEFDISTUON=1 --Definido a nivel de proceso" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "       --UON1" & vbCrLf
sConsulta = sConsulta & "           INSERT INTO ITEM_UON1(ITEM,ANYO,GMN1,PROCE,UON1,PORCEN)" & vbCrLf
sConsulta = sConsulta & "           SELECT @ITEM,ANYO,GMN1,PROCE,UON1,PORCEN" & vbCrLf
sConsulta = sConsulta & "       FROM PROCE_UON1" & vbCrLf
sConsulta = sConsulta & "       WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "       --UON2" & vbCrLf
sConsulta = sConsulta & "           INSERT INTO ITEM_UON2(ITEM,ANYO,GMN1,PROCE,UON1,UON2,PORCEN)" & vbCrLf
sConsulta = sConsulta & "           SELECT @ITEM,ANYO,GMN1,PROCE,UON1,UON2,PORCEN" & vbCrLf
sConsulta = sConsulta & "       FROM PROCE_UON2" & vbCrLf
sConsulta = sConsulta & "       WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "       --UON3" & vbCrLf
sConsulta = sConsulta & "           INSERT INTO ITEM_UON3(ITEM,ANYO,GMN1,PROCE,UON1,UON2,UON3,PORCEN)" & vbCrLf
sConsulta = sConsulta & "           SELECT @ITEM,ANYO,GMN1,PROCE,UON1,UON2,UON3,PORCEN" & vbCrLf
sConsulta = sConsulta & "       FROM PROCE_UON3" & vbCrLf
sConsulta = sConsulta & "       WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "        END" & vbCrLf
sConsulta = sConsulta & "   IF @DEFDISTUON=2 --Definido a nivel de grupo" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "       SELECT @DATO=DATO FROM PROCE_GRUPO_DEF WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO AND DATO='DIST'" & vbCrLf
sConsulta = sConsulta & "           IF @DATO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "           --UON1" & vbCrLf
sConsulta = sConsulta & "           INSERT INTO ITEM_UON1(ITEM,ANYO,GMN1,PROCE,UON1,PORCEN)" & vbCrLf
sConsulta = sConsulta & "               SELECT @ITEM,ANYO,GMN1,PROCE,UON1,PORCEN" & vbCrLf
sConsulta = sConsulta & "           FROM PROCE_GR_UON1" & vbCrLf
sConsulta = sConsulta & "           WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "           --UON2" & vbCrLf
sConsulta = sConsulta & "           INSERT INTO ITEM_UON2(ITEM,ANYO,GMN1,PROCE,UON1,UON2,PORCEN)" & vbCrLf
sConsulta = sConsulta & "               SELECT @ITEM,ANYO,GMN1,PROCE,UON1,UON2,PORCEN" & vbCrLf
sConsulta = sConsulta & "           FROM PROCE_GR_UON2" & vbCrLf
sConsulta = sConsulta & "           WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "           --UON3" & vbCrLf
sConsulta = sConsulta & "           INSERT INTO ITEM_UON3(ITEM,ANYO,GMN1,PROCE,UON1,UON2,UON3,PORCEN)" & vbCrLf
sConsulta = sConsulta & "               SELECT @ITEM,ANYO,GMN1,PROCE,UON1,UON2,UON3,PORCEN" & vbCrLf
sConsulta = sConsulta & "           FROM PROCE_GR_UON3" & vbCrLf
sConsulta = sConsulta & "           WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "        END  " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   --PRESANU1" & vbCrLf
sConsulta = sConsulta & "   IF @DEFPRESANU1=1 --Definido a nivel de proceso" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           INSERT INTO ITEM_PRESPROY(ITEM,ANYO,GMN1,PROCE,PRES1,PRES2,PRES3,PRES4,PORCEN)" & vbCrLf
sConsulta = sConsulta & "           SELECT @ITEM,ANYO,GMN1,PROCE,PRES1,PRES2,PRES3,PRES4,PORCEN" & vbCrLf
sConsulta = sConsulta & "       FROM PROCE_PRES1" & vbCrLf
sConsulta = sConsulta & "       WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "        END" & vbCrLf
sConsulta = sConsulta & "   IF @DEFPRESANU1=2 --Definido a nivel de grupo" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "       SELECT @DATO=DATO FROM PROCE_GRUPO_DEF WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO AND DATO='PRESANU1'" & vbCrLf
sConsulta = sConsulta & "           IF @DATO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "           INSERT INTO ITEM_PRESPROY(ITEM,ANYO,GMN1,PROCE,PRES1,PRES2,PRES3,PRES4,PORCEN)" & vbCrLf
sConsulta = sConsulta & "               SELECT @ITEM,ANYO,GMN1,PROCE,PRES1,PRES2,PRES3,PRES4,PORCEN" & vbCrLf
sConsulta = sConsulta & "           FROM PROCE_GR_PRES1" & vbCrLf
sConsulta = sConsulta & "           WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "        END  " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   --PRESANU2" & vbCrLf
sConsulta = sConsulta & "   IF @DEFPRESANU2=1 --Definido a nivel de proceso" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           INSERT INTO ITEM_PRESCON(ITEM,ANYO,GMN1,PROCE,PRES1,PRES2,PRES3,PRES4,PORCEN)" & vbCrLf
sConsulta = sConsulta & "           SELECT @ITEM,ANYO,GMN1,PROCE,PRES1,PRES2,PRES3,PRES4,PORCEN" & vbCrLf
sConsulta = sConsulta & "       FROM PROCE_PRES2" & vbCrLf
sConsulta = sConsulta & "       WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "        END" & vbCrLf
sConsulta = sConsulta & "   IF @DEFPRESANU2=2 --Definido a nivel de grupo" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "       SELECT @DATO=DATO FROM PROCE_GRUPO_DEF WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO AND DATO='PRESANU2'" & vbCrLf
sConsulta = sConsulta & "           IF @DATO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "           INSERT INTO ITEM_PRESCON(ITEM,ANYO,GMN1,PROCE,PRES1,PRES2,PRES3,PRES4,PORCEN)" & vbCrLf
sConsulta = sConsulta & "               SELECT @ITEM,ANYO,GMN1,PROCE,PRES1,PRES2,PRES3,PRES4,PORCEN" & vbCrLf
sConsulta = sConsulta & "           FROM PROCE_GR_PRES2" & vbCrLf
sConsulta = sConsulta & "           WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO        " & vbCrLf
sConsulta = sConsulta & "   END  " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   --PRES1" & vbCrLf
sConsulta = sConsulta & "   IF @DEFPRES1=1 --Definido a nivel de proceso" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           INSERT INTO ITEM_PRESCON3(ITEM,ANYO,GMN1,PROCE,PRES1,PRES2,PRES3,PRES4,PORCEN)" & vbCrLf
sConsulta = sConsulta & "           SELECT @ITEM,ANYO,GMN1,PROCE,PRES1,PRES2,PRES3,PRES4,PORCEN" & vbCrLf
sConsulta = sConsulta & "       FROM PROCE_PRES3" & vbCrLf
sConsulta = sConsulta & "       WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "        END" & vbCrLf
sConsulta = sConsulta & "   IF @DEFPRES1=2 --Definido a nivel de grupo" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "       SELECT @DATO=DATO FROM PROCE_GRUPO_DEF WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO AND DATO='PRES1'" & vbCrLf
sConsulta = sConsulta & "           IF @DATO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "           INSERT INTO ITEM_PRESCON3(ITEM,ANYO,GMN1,PROCE,PRES1,PRES2,PRES3,PRES4,PORCEN)" & vbCrLf
sConsulta = sConsulta & "               SELECT @ITEM,ANYO,GMN1,PROCE,PRES1,PRES2,PRES3,PRES4,PORCEN" & vbCrLf
sConsulta = sConsulta & "           FROM PROCE_GR_PRES3" & vbCrLf
sConsulta = sConsulta & "           WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO        " & vbCrLf
sConsulta = sConsulta & "   END  " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   --PRES2" & vbCrLf
sConsulta = sConsulta & "   IF @DEFPRES2=1 --Definido a nivel de proceso" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           INSERT INTO ITEM_PRESCON4(ITEM,ANYO,GMN1,PROCE,PRES1,PRES2,PRES3,PRES4,PORCEN)" & vbCrLf
sConsulta = sConsulta & "           SELECT @ITEM,ANYO,GMN1,PROCE,PRES1,PRES2,PRES3,PRES4,PORCEN" & vbCrLf
sConsulta = sConsulta & "       FROM PROCE_PRES4" & vbCrLf
sConsulta = sConsulta & "       WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "        END" & vbCrLf
sConsulta = sConsulta & "   IF @DEFPRES2=2 --Definido a nivel de grupo" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "       SELECT @DATO=DATO FROM PROCE_GRUPO_DEF WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO AND DATO='PRES2'" & vbCrLf
sConsulta = sConsulta & "           IF @DATO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "           INSERT INTO ITEM_PRESCON4(ITEM,ANYO,GMN1,PROCE,PRES1,PRES2,PRES3,PRES4,PORCEN)" & vbCrLf
sConsulta = sConsulta & "               SELECT @ITEM,ANYO,GMN1,PROCE,PRES1,PRES2,PRES3,PRES4,PORCEN" & vbCrLf
sConsulta = sConsulta & "           FROM PROCE_GR_PRES4" & vbCrLf
sConsulta = sConsulta & "           WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO        " & vbCrLf
sConsulta = sConsulta & "        END  " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   --�ESP?" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_ITEM_Ins_DATVAR INTO @ANYO,@GMN1,@PROCE,@ITEM,@GRUPO" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_ITEM_Ins_DATVAR" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_ITEM_Ins_DATVAR" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Function


Private Function V_2_10_5_Defaults_1()
Dim sConsulta As String

EliminarDefaults gRDOCon, oDMOBD, "PARGEN_GEST"

sConsulta = "ALTER TABLE [dbo].[PARGEN_GEST] WITH NOCHECK ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_VOL_MAX_ADJ_DIR] DEFAULT (0) FOR [VOL_MAX_ADJ_DIR]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_OBLDIST] DEFAULT (0) FOR [OBLDIST]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_OBLPP] DEFAULT (0) FOR [OBLPP]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_OBLPC] DEFAULT (0) FOR [OBLPC]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_ADJORDEN] DEFAULT (0) FOR [ADJORDEN]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_DIAREU] DEFAULT (1) FOR [DIAREU]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_PERIODICIREU] DEFAULT (7) FOR [PERIODICIREU]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_TIEMPOREU] DEFAULT (30) FOR [TIEMPOREU]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_COMIENZOREU] DEFAULT (1 / 1 / 2000) FOR [COMIENZOREU]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_CRITEORDREU1] DEFAULT (1) FOR [CRITEORDREU1]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_CRITEORDREU2] DEFAULT (2) FOR [CRITEORDREU2]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_CONVSOLORESP] DEFAULT (1) FOR [CONVSOLORESP]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_OBLPROVEEQP] DEFAULT (0) FOR [OBLPROVEEQP]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_SENT_ORD_CAL1] DEFAULT (1) FOR [SENT_ORD_CAL1]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_SENT_ORD_CAL2] DEFAULT (1) FOR [SENT_ORD_CAL2]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_SENT_ORD_CAL3] DEFAULT (1) FOR [SENT_ORD_CAL3]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_PERMADJSINHOM] DEFAULT (0) FOR [PERMADJSINHOM]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_PERMITEMSINCOD] DEFAULT (0) FOR [PERMITEMSINCOD]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_MOSTRARMAIL] DEFAULT (0) FOR [MOSTRARMAIL]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_LOGPREBLOQ] DEFAULT (0) FOR [LOGPREBLOQ]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_ACTIVLOG] DEFAULT (0) FOR [ACTIVLOG]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_TIPOCLTEMAIL] DEFAULT (0) FOR [TIPOCLTEMAIL]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_BUZPER] DEFAULT (30) FOR [BUZPER]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_BUZLEC] DEFAULT (2) FOR [BUZLEC]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_ARTIC_ADJESP] DEFAULT (2) FOR [ARTIC_ADJESP]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_USAPRES1] DEFAULT (1) FOR [USAPRES1]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_USAPRES2] DEFAULT (1) FOR [USAPRES2]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_USAPRES3] DEFAULT (1) FOR [USAPRES3]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_USAPRES4] DEFAULT (1) FOR [USAPRES4]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_OBLPROVEMAT] DEFAULT (0) FOR [OBLPROVEMAT]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_OBLPRES3] DEFAULT (0) FOR [OBLPres3]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_OBLPRES4] DEFAULT (0) FOR [OBLPres4]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_OBLHOMPEDIDOS] DEFAULT (0) FOR [OBLHOMPEDIDOS]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_ELIMLINCATALOG] DEFAULT (0) FOR [ELIMLINCATALOG]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_OBLHOMPEDDIR] DEFAULT (0) FOR [OBLHOMPEDDIR]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_DEST] DEFAULT (3) FOR [DEST]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_PAG] DEFAULT (3) FOR [PAG]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_FECSUM] DEFAULT (3) FOR [FECSUM]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_PROVE] DEFAULT (3) FOR [PROVE]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_DIST] DEFAULT (3) FOR [DIST]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_PRESANU1] DEFAULT (3) FOR [PRESANU1]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_PRESANU2] DEFAULT (3) FOR [PRESANU2]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_PRES1] DEFAULT (3) FOR [PRES1]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_PRES2] DEFAULT (3) FOR [PRES2]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_PROCE_ESP] DEFAULT (1) FOR [PROCE_ESP]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_GRUPO_ESP] DEFAULT (0) FOR [GRUPO_ESP]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_ITEM_ESP] DEFAULT (1) FOR [ITEM_ESP]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_PERIODOAVISODES] DEFAULT (30) FOR [PERIODOAVISODES]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_PERIODOAVISOADJ] DEFAULT (30) FOR [PERIODOAVISOADJ]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_AVISODES] DEFAULT (0) FOR [AVISODES]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_AVISOSDJ] DEFAULT (0) FOR [AVISOADJ]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_SUBASTA] DEFAULT (0) FOR [SUBASTA]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_SUBASTA_PROVE] DEFAULT (0) FOR [SUBASTA_PROVE]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_SUBASTA_PUJAS] DEFAULT (0) FOR [SUBASTA_PUJAS]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_SUBASTA_ESPERA] DEFAULT (0) FOR [SUBASTA_ESPERA]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_SOLCANTMAX] DEFAULT (1) FOR [SOLCANTMAX]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_PRECALTER] DEFAULT (0) FOR [PRECALTER]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_PROCE_POND] DEFAULT (0) FOR [PROCE_POND]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_USA_POND] DEFAULT (0) FOR [USAPOND]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_OFE_ADJUN] DEFAULT (1) FOR [OFE_ADJUN]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_GRUPO_ADJUN] DEFAULT (0) FOR [GRUPO_ADJUN]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_ITEM_ADJUN] DEFAULT (0) FOR [ITEM_ADJUN]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_SUBASTA_ACTENV] DEFAULT (0) FOR [SUBASTA_ACTENV]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_SUBASTA_ATIEMPO] DEFAULT (0) FOR [SUBASTA_ATIEMPO]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_PRESDISTAUT] DEFAULT (0) FOR [PRESDISTAUT]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_NIVDIST] DEFAULT (0) FOR [NIVDIST]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_NIVPP] DEFAULT (0) FOR [NIVPP]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_NIVPC] DEFAULT (0) FOR [NIVPC]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_NIVPRES3] DEFAULT (0) FOR [NIVPRES3]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_NIVPRES4] DEFAULT (0) FOR [NIVPRES4]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_SUBASTA_ENVREC] DEFAULT (0) FOR [SUBASTA_ENVREC]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PARGEN_GEST_SUBASTA_ENVCIE] DEFAULT (0) FOR [SUBASTA_ENVCIE]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF__PARGEN_GEST_ACUSERECIBO] DEFAULT (0) FOR [ACUSERECIBO]" & vbCrLf

ExecuteSQL gRDOCon, sConsulta

End Function


Private Function V_2_10_5_Storeds_1()
Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_ANULAR_SOLICITUD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_ANULAR_SOLICITUD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSEP_ANULAR_SOLICITUD @ID int, @PER varchar(50), @MOTIVO varchar(500) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO SOLICIT_EST(SOLICIT,PER, COMENT, FECHA,EST)" & vbCrLf
sConsulta = sConsulta & "VALUES (@ID, @PER, @MOTIVO, GETDATE(), 5)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE SOLICIT" & vbCrLf
sConsulta = sConsulta & "   SET ESTADO = 5" & vbCrLf
sConsulta = sConsulta & " WHERE ID = @ID" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_CERRAR_SOLICITUD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_CERRAR_SOLICITUD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE FSEP_CERRAR_SOLICITUD @ID int, @PER varchar(50), @MOTIVO varchar(500) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO SOLICIT_EST(SOLICIT,PER, COMENT, FECHA,EST)" & vbCrLf
sConsulta = sConsulta & "VALUES (@ID, @PER, @MOTIVO, GETDATE(), 6)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE SOLICIT" & vbCrLf
sConsulta = sConsulta & "   SET ESTADO = 6" & vbCrLf
sConsulta = sConsulta & " WHERE ID = @ID" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_DEVOLVER_ADJUNTOS_SOLICIT]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_DEVOLVER_ADJUNTOS_SOLICIT]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSEP_DEVOLVER_ADJUNTOS_SOLICIT @SOLICIT int AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT ID, SOLICIT, FECHA, SA.NOM, SA.COM, P.NOM PERNOM, P.APE PERAPE, SA.PER, DATALENGTH(DATA) TAMANO " & vbCrLf
sConsulta = sConsulta & "  FROM SOLICIT_ADJUN SA" & vbCrLf
sConsulta = sConsulta & "        LEFT JOIN PER P" & vbCrLf
sConsulta = sConsulta & "                ON SA.PER = P.COD" & vbCrLf
sConsulta = sConsulta & " WHERE SOLICIT = @SOLICIT" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_DEVOLVER_ARTICULOS_SOLICIT]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_DEVOLVER_ARTICULOS_SOLICIT]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSEP_DEVOLVER_ARTICULOS_SOLICIT @ID int AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT case when c5.cod is null then " & vbCrLf
sConsulta = sConsulta & "                     case when c4.cod is null then " & vbCrLf
sConsulta = sConsulta & "                          case when c3.cod is null then" & vbCrLf
sConsulta = sConsulta & "                               case when c2.cod is null then" & vbCrLf
sConsulta = sConsulta & "                                    c1.cod " & vbCrLf
sConsulta = sConsulta & "                               else" & vbCrLf
sConsulta = sConsulta & "                               c1.cod + ' - ' + c2.cod " & vbCrLf
sConsulta = sConsulta & "                               end" & vbCrLf
sConsulta = sConsulta & "                          else" & vbCrLf
sConsulta = sConsulta & "                          c1.cod + ' - ' + c2.cod + ' - ' + c3.cod " & vbCrLf
sConsulta = sConsulta & "                          end" & vbCrLf
sConsulta = sConsulta & "                     else" & vbCrLf
sConsulta = sConsulta & "                     c1.cod + ' - ' + c2.cod + ' - ' + c3.cod + ' - ' + c4.cod" & vbCrLf
sConsulta = sConsulta & "                     end" & vbCrLf
sConsulta = sConsulta & "                 else" & vbCrLf
sConsulta = sConsulta & "                 c1.cod + ' - ' + c2.cod + ' - ' + c3.cod + ' - ' + c4.cod + ' - ' + c5.cod" & vbCrLf
sConsulta = sConsulta & "                 end CAT," & vbCrLf
sConsulta = sConsulta & "                 case when c5.cod is null then " & vbCrLf
sConsulta = sConsulta & "                         case when c4.cod is null then " & vbCrLf
sConsulta = sConsulta & "                              case when c3.cod is null then" & vbCrLf
sConsulta = sConsulta & "                                   case when c2.cod is null then" & vbCrLf
sConsulta = sConsulta & "                                        c1.cod" & vbCrLf
sConsulta = sConsulta & "                                   else" & vbCrLf
sConsulta = sConsulta & "                                   c2.cod" & vbCrLf
sConsulta = sConsulta & "                                   end" & vbCrLf
sConsulta = sConsulta & "                              else" & vbCrLf
sConsulta = sConsulta & "                              c3.cod" & vbCrLf
sConsulta = sConsulta & "                              end" & vbCrLf
sConsulta = sConsulta & "                         else" & vbCrLf
sConsulta = sConsulta & "                         c4.cod" & vbCrLf
sConsulta = sConsulta & "                         end" & vbCrLf
sConsulta = sConsulta & "                     else" & vbCrLf
sConsulta = sConsulta & "                     c5.cod" & vbCrLf
sConsulta = sConsulta & "                 end ULTCAT," & vbCrLf
sConsulta = sConsulta & "                 case when c5.cod is null then " & vbCrLf
sConsulta = sConsulta & "                         case when c4.cod is null then " & vbCrLf
sConsulta = sConsulta & "                              case when c3.cod is null then" & vbCrLf
sConsulta = sConsulta & "                                   case when c2.cod is null then" & vbCrLf
sConsulta = sConsulta & "                                        '(' + c1.cod + ') ' + c1.den" & vbCrLf
sConsulta = sConsulta & "                                   else" & vbCrLf
sConsulta = sConsulta & "                                   '(' + c1.cod + ') ' + c1.den + ' - (' + c2.cod + ') ' + c2.den" & vbCrLf
sConsulta = sConsulta & "                                   end" & vbCrLf
sConsulta = sConsulta & "                              else" & vbCrLf
sConsulta = sConsulta & "                              '(' + c1.cod + ') ' + c1.den + ' - (' + c2.cod + ') ' + c2.den + ' - (' + c3.cod + ') ' + c3.den" & vbCrLf
sConsulta = sConsulta & "                              end" & vbCrLf
sConsulta = sConsulta & "                         else" & vbCrLf
sConsulta = sConsulta & "                         '(' + c1.cod + ') ' + c1.den + ' - (' + c2.cod + ') ' + c2.den + ' - (' + c3.cod + ') ' + c3.den + ' - (' + c4.cod + ') ' + c4.den" & vbCrLf
sConsulta = sConsulta & "                         end" & vbCrLf
sConsulta = sConsulta & "                     else" & vbCrLf
sConsulta = sConsulta & "                     '(' + c1.cod + ') ' + c1.den + ' - (' + c2.cod + ') ' + c2.den + ' - (' + c3.cod + ') ' + c3.den + ' - (' + c4.cod + ') ' + c4.den + ' - (' + c5.cod + ') ' + c5.den" & vbCrLf
sConsulta = sConsulta & "                 end CATEGORIARAMA," & vbCrLf
sConsulta = sConsulta & "                 CL.ART_INT + I.ART CODART," & vbCrLf
sConsulta = sConsulta & "                 A4.DEN + I.DESCR  + ART_AUX.DEN DENART," & vbCrLf
sConsulta = sConsulta & "                 PRECIO_UC PRECIO," & vbCrLf
sConsulta = sConsulta & "                 UC," & vbCrLf
sConsulta = sConsulta & "                 FECHA_DESPUB," & vbCrLf
sConsulta = sConsulta & "                 CL.PROVE," & vbCrLf
sConsulta = sConsulta & "                 CL.GMN1," & vbCrLf
sConsulta = sConsulta & "                 CL.GMN2," & vbCrLf
sConsulta = sConsulta & "                 CL.GMN3," & vbCrLf
sConsulta = sConsulta & "                 CL.GMN4," & vbCrLf
sConsulta = sConsulta & "                 PA.COD_EXT," & vbCrLf
sConsulta = sConsulta & "        CL.PUB,CL.DEST,CL.CANT_ADJ,CL.UP_DEF,CL.FC_DEF,CL.CANT_MIN_DEF,CL.ANYO,CL.GMN1,CL.PROCE,CL.ID" & vbCrLf
sConsulta = sConsulta & "  FROM CATALOG_LIN CL" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN CATN1 C1" & vbCrLf
sConsulta = sConsulta & "             ON CL.CAT1 = C1.ID" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN CATN2 C2" & vbCrLf
sConsulta = sConsulta & "             ON CL.CAT2 = C2.ID" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN CATN3 C3" & vbCrLf
sConsulta = sConsulta & "             ON CL.CAT3 = C3.ID" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN CATN4 C4" & vbCrLf
sConsulta = sConsulta & "             ON CL.CAT4 = C4.ID" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN CATN5 C5" & vbCrLf
sConsulta = sConsulta & "             ON CL.CAT5 = C5.ID" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN ITEM I" & vbCrLf
sConsulta = sConsulta & "             ON CL.ANYO = I.ANYO" & vbCrLf
sConsulta = sConsulta & "            AND CL.GMN1=I.GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND CL.PROCE=I.PROCE " & vbCrLf
sConsulta = sConsulta & "            AND CL.ITEM=I.ID" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN ART4 A4" & vbCrLf
sConsulta = sConsulta & "            ON CL.GMN1=A4.GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND CL.GMN2=A4.GMN2 " & vbCrLf
sConsulta = sConsulta & "            AND CL.GMN3=A4.GMN3 " & vbCrLf
sConsulta = sConsulta & "            AND CL.GMN4=A4.GMN4 " & vbCrLf
sConsulta = sConsulta & "            AND CL.ART_INT=A4.COD" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN ART4 ART_AUX " & vbCrLf
sConsulta = sConsulta & "             ON I.ART=ART_AUX.COD " & vbCrLf
sConsulta = sConsulta & "            AND ART_AUX.GMN1=I.GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND ART_AUX.GMN2=I.GMN2 " & vbCrLf
sConsulta = sConsulta & "            AND ART_AUX.GMN3=I.GMN3 " & vbCrLf
sConsulta = sConsulta & "            AND ART_AUX.GMN4=I.GMN4 " & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN PROVE_ART4 PA" & vbCrLf
sConsulta = sConsulta & "             ON CL.PROVE = PA.PROVE" & vbCrLf
sConsulta = sConsulta & "            AND CL.GMN1 = PA.GMN1" & vbCrLf
sConsulta = sConsulta & "            AND CL.GMN2 = PA.GMN2" & vbCrLf
sConsulta = sConsulta & "            AND CL.GMN3 = PA.GMN3" & vbCrLf
sConsulta = sConsulta & "            AND CL.GMN4 = PA.GMN4" & vbCrLf
sConsulta = sConsulta & "            AND CASE WHEN CL.ART_INT IS NULL THEN I.ART ELSE CL.ART_INT END = PA.ART" & vbCrLf
sConsulta = sConsulta & " WHERE CL.SOLICIT = @ID" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_DEVOLVER_COMENTARIOS_SOLICITUD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_DEVOLVER_COMENTARIOS_SOLICITUD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSEP_DEVOLVER_COMENTARIOS_SOLICITUD @ID int AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT SE.ID, SE.PER,P.EMAIL, P.NOM,P.APE, SE.FECHA, SE.EST, SE.COMENT" & vbCrLf
sConsulta = sConsulta & "  FROM SOLICIT_EST SE" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN PER P" & vbCrLf
sConsulta = sConsulta & "               ON SE.PER = P.COD" & vbCrLf
sConsulta = sConsulta & "WHERE SE.SOLICIT = @ID" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_DEVOLVER_ID_SOLICITUDES]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_DEVOLVER_ID_SOLICITUDES]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSEP_DEVOLVER_ID_SOLICITUDES @SOLICITUD int, @EST smallint, @PER varchar(50), @FECDESDE DATETIME, @FECHASTA DATETIME, @APROB varchar(50), @DEN varchar(100), @MAXLINEAS smallint = 100 AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL nvarchar(2000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL2 nvarchar(2000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PARAM nvarchar(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = ''" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @FECHASTA = DATEADD(d,1,@FECHASTA)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT S.ID" & vbCrLf
sConsulta = sConsulta & "  FROM SOLICIT S" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN (PER P" & vbCrLf
sConsulta = sConsulta & "                    INNER JOIN DEP D" & vbCrLf
sConsulta = sConsulta & "                            ON P.DEP = D.COD)" & vbCrLf
sConsulta = sConsulta & "                ON S.PER = P.COD" & vbCrLf
sConsulta = sConsulta & "    LEFT JOIN SOLICIT_EST SE" & vbCrLf
sConsulta = sConsulta & "            ON S.ID = SE.SOLICIT" & vbCrLf
sConsulta = sConsulta & "           AND S.SOLICIT_EST = SE.ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & " WHERE 1 = 1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "'           " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @SOLICITUD!=0" & vbCrLf
sConsulta = sConsulta & "  SET @SQL = @SQL + ' AND S.ID = @SOLICITUD'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    declare @IN varchar(500)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    SET @IN = '('" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    IF @EST & 1 = 1" & vbCrLf
sConsulta = sConsulta & "         SET @IN = @IN + '1,'" & vbCrLf
sConsulta = sConsulta & "    IF @EST & 2 = 2" & vbCrLf
sConsulta = sConsulta & "         SET @IN = @IN + '2,'" & vbCrLf
sConsulta = sConsulta & "    IF @EST & 4 = 4" & vbCrLf
sConsulta = sConsulta & "         SET @IN = @IN + '3,'" & vbCrLf
sConsulta = sConsulta & "    IF @EST & 8 = 8" & vbCrLf
sConsulta = sConsulta & "         SET @IN = @IN + '4,'" & vbCrLf
sConsulta = sConsulta & "    IF @EST & 16 = 16" & vbCrLf
sConsulta = sConsulta & "         SET @IN = @IN + '5,'" & vbCrLf
sConsulta = sConsulta & "    IF @EST & 32 = 32" & vbCrLf
sConsulta = sConsulta & "         SET @IN = @IN + '6,'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    IF @IN!='('" & vbCrLf
sConsulta = sConsulta & "      set @SQL= @SQL + ' AND S.ESTADO IN ' + LEFT(@IN,LEN(@IN)-1) + ') '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    IF @PER != '' " & vbCrLf
sConsulta = sConsulta & "      SET @SQL = @SQL + ' AND S.PER = @PER'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    IF @APROB != ''" & vbCrLf
sConsulta = sConsulta & "      SET @SQL = @SQL + ' AND S.COMPRADOR = @APROB'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    SET @SQL = @SQL + ' AND S.DEN LIKE ''%'' + @DEN + ''%'''" & vbCrLf
sConsulta = sConsulta & "    " & vbCrLf
sConsulta = sConsulta & "    IF @FECDESDE IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "      SET @SQL = @SQL + ' AND S.FECALTA >= @FECDESDE'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    IF @FECHASTA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "      SET @SQL = @SQL + ' AND S.FECALTA < @FECHASTA'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + '" & vbCrLf
sConsulta = sConsulta & "ORDER BY S.ID DESC'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL2 = '" & vbCrLf
sConsulta = sConsulta & "create table #IDS (id INT)" & vbCrLf
sConsulta = sConsulta & "set nocount on" & vbCrLf
sConsulta = sConsulta & "DECLARE C1 CURSOR FOR " & vbCrLf
sConsulta = sConsulta & "' + @SQL + '" & vbCrLf
sConsulta = sConsulta & "DECLARE @miID INT" & vbCrLf
sConsulta = sConsulta & "declare @cont int" & vbCrLf
sConsulta = sConsulta & "set @cont=1" & vbCrLf
sConsulta = sConsulta & "OPEN C1" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C1 INTO @miID" & vbCrLf
sConsulta = sConsulta & "while @@fetch_status=0" & vbCrLf
sConsulta = sConsulta & "begin" & vbCrLf
sConsulta = sConsulta & "if @cont = 1" & vbCrLf
sConsulta = sConsulta & " insert into #IDS values (@miID)" & vbCrLf
sConsulta = sConsulta & "set @cont = @cont + 1        " & vbCrLf
sConsulta = sConsulta & "     " & vbCrLf
sConsulta = sConsulta & "if @cont = ' + CONVERT(VARCHAR,@MAXLINEAS) + ' + 1" & vbCrLf
sConsulta = sConsulta & " set @cont = 1" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C1 INTO @miID" & vbCrLf
sConsulta = sConsulta & "end" & vbCrLf
sConsulta = sConsulta & "CLOSE C1" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C1" & vbCrLf
sConsulta = sConsulta & "set nocount off" & vbCrLf
sConsulta = sConsulta & "select * from #ids" & vbCrLf
sConsulta = sConsulta & "'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @PARAM = N'@SOLICITUD int, @PER varchar(50), @APROB varchar(50), @DEN varchar(100), @FECDESDE datetime, @FECHASTA  datetime'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL2, @PARAM, @SOLICITUD = @SOLICITUD, @PER = @PER, @APROB = @APROB, @DEN=@DEN, @FECDESDE=@FECDESDE, @FECHASTA = @FECHASTA" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_DEVOLVER_ITEMS_SOLICITUD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_DEVOLVER_ITEMS_SOLICITUD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE  FSEP_DEVOLVER_ITEMS_SOLICITUD  @ANYO INT,@GMN1 VARCHAR(50),@PROCE INT, @SOLICIT int AS " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "set NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT I.ANYO, I.GMN1, I.PROCE, I.GRUPO, I.ID, I.GMN2, I.GMN3, I.GMN4, I.ART, I.DESCR + A.DEN AS DESCR, " & vbCrLf
sConsulta = sConsulta & "       I.DEST, I.UNI,UNI.DEN UNIDEN, I.CANT, I.PREC, I.PRES," & vbCrLf
sConsulta = sConsulta & "       I.PAG, PAG.DEN DENPAG, I.FECINI, I.FECFIN, I.ESP, I.CONF, I.EST," & vbCrLf
sConsulta = sConsulta & "       I.OBJ, I.FECHAOBJ" & vbCrLf
sConsulta = sConsulta & "  FROM ITEM I  " & vbCrLf
sConsulta = sConsulta & "        LEFT JOIN ART4 A" & vbCrLf
sConsulta = sConsulta & "               ON I.ART = A.COD " & vbCrLf
sConsulta = sConsulta & "              AND I.GMN4 = A.GMN4 " & vbCrLf
sConsulta = sConsulta & "              AND I.GMN3 = A.GMN3 " & vbCrLf
sConsulta = sConsulta & "              AND I.GMN2 = A.GMN2" & vbCrLf
sConsulta = sConsulta & "              AND I.GMN1 = A.GMN1" & vbCrLf
sConsulta = sConsulta & "        LEFT JOIN PAG " & vbCrLf
sConsulta = sConsulta & "               ON I.PAG = PAG.COD" & vbCrLf
sConsulta = sConsulta & "        LEFT JOIN UNI " & vbCrLf
sConsulta = sConsulta & "               ON I.UNI = UNI.COD" & vbCrLf
sConsulta = sConsulta & "WHERE I.ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "  AND I.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "  AND I.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "  AND I.SOLICIT = @SOLICIT" & vbCrLf
sConsulta = sConsulta & "ORDER BY I.ART,DESCR" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_DEVOLVER_PEDIDOS_SOLICIT]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_DEVOLVER_PEDIDOS_SOLICIT]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSEP_DEVOLVER_PEDIDOS_SOLICIT @ID INT   AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT O.ID, P.ANYO, P.NUM PNUM, P.TIPO, O.NUM ONUM, O.PROVE, PR.DEN, P.FECHA, O.EST , " & vbCrLf
sConsulta = sConsulta & "  LP.FECEMISION , O.NUMEXT , O.FECHA" & vbCrLf
sConsulta = sConsulta & "  FROM PEDIDO P" & vbCrLf
sConsulta = sConsulta & "         INNER JOIN ORDEN_ENTREGA O" & vbCrLf
sConsulta = sConsulta & "                 ON P.ID = O.PEDIDO" & vbCrLf
sConsulta = sConsulta & "         INNER JOIN PROVE PR" & vbCrLf
sConsulta = sConsulta & "                 ON O.PROVE = PR.COD" & vbCrLf
sConsulta = sConsulta & "         INNER JOIN LINEAS_PEDIDO LP" & vbCrLf
sConsulta = sConsulta & "                 ON O.ID = LP.ORDEN" & vbCrLf
sConsulta = sConsulta & " WHERE LP.SOLICIT = @ID" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "ORDER BY P.TIPO, P.FECHA DESC" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_DEVOLVER_PERSONAS_ACCION]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_DEVOLVER_PERSONAS_ACCION]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSEP_DEVOLVER_PERSONAS_ACCION @ACC int AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT P.COD, P.NOM, P.APE " & vbCrLf
sConsulta = sConsulta & "  FROM USU U" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PER P " & vbCrLf
sConsulta = sConsulta & "               ON U.PER  = P.COD " & vbCrLf
sConsulta = sConsulta & "        LEFT JOIN USU_ACC UA" & vbCrLf
sConsulta = sConsulta & "               ON U.COD =  UA.USU" & vbCrLf
sConsulta = sConsulta & "              AND @ACC = UA.ACC" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN PERF_ACC PA" & vbCrLf
sConsulta = sConsulta & "               ON U.PERF = PA.PERF" & vbCrLf
sConsulta = sConsulta & "              AND @ACC = PA.ACC" & vbCrLf
sConsulta = sConsulta & "WHERE ISNULL(UA.ACC,PA.ACC) IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 
sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_DEVOLVER_PROCESOS_SOLICITUD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_DEVOLVER_PROCESOS_SOLICITUD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSEP_DEVOLVER_PROCESOS_SOLICITUD @ID INT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT ANYO, GMN1, COD, DEN, FECNEC, EST , ADJDIR, FECPRES ,COM" & vbCrLf
sConsulta = sConsulta & "  FROM " & vbCrLf
sConsulta = sConsulta & "    (SELECT P.ANYO, P.GMN1, P.COD, P.DEN, P.FECNEC, P.EST ,P.ADJDIR, P.FECPRES ,P.COM" & vbCrLf
sConsulta = sConsulta & "       FROM PROCE P" & vbCrLf
sConsulta = sConsulta & "      WHERE P.SOLICIT = @ID" & vbCrLf
sConsulta = sConsulta & "     UNION" & vbCrLf
sConsulta = sConsulta & "     SELECT P.ANYO, P.GMN1, P.COD, P.DEN, P.FECNEC, P.EST ,P.ADJDIR, P.FECPRES ,P.COM" & vbCrLf
sConsulta = sConsulta & "       FROM PROCE P " & vbCrLf
sConsulta = sConsulta & "        INNER JOIN PROCE_GRUPO PG" & vbCrLf
sConsulta = sConsulta & "                ON P.ANYO = PG.ANYO" & vbCrLf
sConsulta = sConsulta & "               AND P.GMN1 = PG.GMN1" & vbCrLf
sConsulta = sConsulta & "               AND P.COD = PG.PROCE" & vbCrLf
sConsulta = sConsulta & "      WHERE PG.SOLICIT = @ID" & vbCrLf
sConsulta = sConsulta & "     UNION" & vbCrLf
sConsulta = sConsulta & "     SELECT P.ANYO, P.GMN1, P.COD, P.DEN, P.FECNEC, P.EST ,P.ADJDIR, P.FECPRES ,P.COM" & vbCrLf
sConsulta = sConsulta & "       FROM PROCE P " & vbCrLf
sConsulta = sConsulta & "        INNER JOIN ITEM I" & vbCrLf
sConsulta = sConsulta & "                ON P.ANYO = I.ANYO" & vbCrLf
sConsulta = sConsulta & "               AND P.GMN1 = I.GMN1" & vbCrLf
sConsulta = sConsulta & "               AND P.COD = I.PROCE" & vbCrLf
sConsulta = sConsulta & "      WHERE I.SOLICIT = @ID) PROCESOS" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 
sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_DEVOLVER_SOLICITUDES]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_DEVOLVER_SOLICITUDES]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSEP_DEVOLVER_SOLICITUDES @SOLICITUD int, @EST smallint, @PER varchar(50), @FECDESDE DATETIME, @FECHASTA DATETIME, @APROB varchar(50), @DEN varchar(100), @MAXLINEAS INT=100, @DESDEID INT = NULL AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL nvarchar(2000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PARAM nvarchar(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = ''" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @FECHASTA = DATEADD(d,1,@FECHASTA)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT TOP ' +  cast(@MAXLINEAS as varchar(20)) + 'S.ID, S.DEN, S.DESCR, S.IMPORTE, S.MON, S.CAMBIO, S.PROVEEDORES, S.CARGO, S.FECALTA, S.FECNEC, S.DEST, S.PER, " & vbCrLf
sConsulta = sConsulta & "       P.NOM, P.APE, P.EMAIL, P.TFNO, P.FAX, D.DEN DEP, S.ESTADO, S.COMPRADOR, SE.COMENT, SE.PER SEPER, SE.FECHA SEFECHA, " & vbCrLf
sConsulta = sConsulta & "       CASE WHEN EXISTS (SELECT TOP 1 ID FROM SOLICIT_EST WHERE SOLICIT = S.ID AND EST > 1) THEN 1 ELSE 0 END ENVIADA" & vbCrLf
sConsulta = sConsulta & "  FROM SOLICIT S" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN (PER P" & vbCrLf
sConsulta = sConsulta & "                    INNER JOIN DEP D" & vbCrLf
sConsulta = sConsulta & "                            ON P.DEP = D.COD)" & vbCrLf
sConsulta = sConsulta & "                ON S.PER = P.COD" & vbCrLf
sConsulta = sConsulta & "    LEFT JOIN SOLICIT_EST SE" & vbCrLf
sConsulta = sConsulta & "            ON S.ID = SE.SOLICIT" & vbCrLf
sConsulta = sConsulta & "           AND S.SOLICIT_EST = SE.ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & " WHERE 1 = 1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "'           " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @SOLICITUD!=0" & vbCrLf
sConsulta = sConsulta & "  SET @SQL = @SQL + ' AND S.ID = @SOLICITUD'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    declare @IN varchar(500)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    SET @IN = '('" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    IF @EST & 1 = 1" & vbCrLf
sConsulta = sConsulta & "         SET @IN = @IN + '1,'" & vbCrLf
sConsulta = sConsulta & "    IF @EST & 2 = 2" & vbCrLf
sConsulta = sConsulta & "         SET @IN = @IN + '2,'" & vbCrLf
sConsulta = sConsulta & "    IF @EST & 4 = 4" & vbCrLf
sConsulta = sConsulta & "         SET @IN = @IN + '3,'" & vbCrLf
sConsulta = sConsulta & "    IF @EST & 8 = 8" & vbCrLf
sConsulta = sConsulta & "         SET @IN = @IN + '4,'" & vbCrLf
sConsulta = sConsulta & "    IF @EST & 16 = 16" & vbCrLf
sConsulta = sConsulta & "         SET @IN = @IN + '5,'" & vbCrLf
sConsulta = sConsulta & "    IF @EST & 32 = 32" & vbCrLf
sConsulta = sConsulta & "         SET @IN = @IN + '6,'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    IF @IN!='('" & vbCrLf
sConsulta = sConsulta & "      set @SQL= @SQL + ' AND S.ESTADO IN ' + LEFT(@IN,LEN(@IN)-1) + ') '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    IF @PER != '' " & vbCrLf
sConsulta = sConsulta & "      SET @SQL = @SQL + ' AND S.PER = @PER'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    IF @APROB != ''" & vbCrLf
sConsulta = sConsulta & "      SET @SQL = @SQL + ' AND S.COMPRADOR = @APROB'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    SET @SQL = @SQL + ' AND S.DEN LIKE ''%'' + @DEN + ''%'''" & vbCrLf
sConsulta = sConsulta & "    " & vbCrLf
sConsulta = sConsulta & "    IF @FECDESDE IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "      SET @SQL = @SQL + ' AND S.FECALTA >= @FECDESDE'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    IF @FECHASTA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "      SET @SQL = @SQL + ' AND S.FECALTA < @FECHASTA'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if @DESDEID IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + '" & vbCrLf
sConsulta = sConsulta & "   AND S.ID<=@DESDEID '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + '" & vbCrLf
sConsulta = sConsulta & "ORDER BY S.ID DESC'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @PARAM = N'@SOLICITUD int, @PER varchar(50), @APROB varchar(50), @DEN varchar(100), @FECDESDE datetime, @FECHASTA  datetime, @DESDEID int'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, @PARAM, @SOLICITUD = @SOLICITUD, @PER = @PER, @APROB = @APROB, @DEN=@DEN, @FECDESDE=@FECDESDE, @FECHASTA = @FECHASTA, @DESDEID=@DESDEID" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_ELIMINAR_ADJUNTO_SOLICIT]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_ELIMINAR_ADJUNTO_SOLICIT]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSEP_ELIMINAR_ADJUNTO_SOLICIT @ADJUN integer AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM SOLICIT_ADJUN WHERE ID = @ADJUN" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_ELIMINAR_SOLICITUD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_ELIMINAR_SOLICITUD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSEP_ELIMINAR_SOLICITUD @ID int AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM SOLICIT_ADJUN WHERE SOLICIT = @ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM SOLICIT_EST WHERE SOLICIT = @ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM SOLICIT_ASIG_COMP WHERE SOLICIT = @ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM SOLICIT WHERE ID = @ID" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_GUARDAR_ADJUNTOS_SOLICIT]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_GUARDAR_ADJUNTOS_SOLICIT]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSEP_GUARDAR_ADJUNTOS_SOLICIT @SOLICIT integer, @FECHA datetime, @ADJUN integer, @NOM VARCHAR(300), @COM VARCHAR(500), @PER VARCHAR(50) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE SOLICIT_ADJUN" & vbCrLf
sConsulta = sConsulta & "   SET SOLICIT = @SOLICIT, FECHA=@FECHA, NOM = @NOM, COM = @COM, PER=@PER" & vbCrLf
sConsulta = sConsulta & " WHERE ID = @ADJUN" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 
sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_SOLICITUD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_SOLICITUD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSEP_SOLICITUD @ID int OUTPUT, @DEN varchar (50), @DESCR varchar (500), @IMPORTE float, @MON varchar (3), @PROVEEDORES varchar (300)  ," & vbCrLf
sConsulta = sConsulta & "@CARGO varchar (100), @FECNEC datetime, @DEST varchar (4), @PER varchar (20), @NOM varchar (50)  ," & vbCrLf
sConsulta = sConsulta & "@APE varchar (100), @EMAIL varchar (100), @TFNO varchar (20), @FAX varchar (20), @DEP varchar (100)  ," & vbCrLf
sConsulta = sConsulta & "@ESTADO tinyint, @COMPRADOR varchar (20) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @CAMBIO float" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @CAMBIO = EQUIV " & vbCrLf
sConsulta = sConsulta & "  from MON " & vbCrLf
sConsulta = sConsulta & " WHERE COD = @MON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @ESTADOANTERIOR tinyint" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ID =0" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    INSERT INTO SOLICIT (DEN, DESCR, IMPORTE, MON, CAMBIO, PROVEEDORES,CARGO, FECNEC, DEST, PER, ESTADO, COMPRADOR,FECALTA) " & vbCrLf
sConsulta = sConsulta & "    VALUES (@DEN, @DESCR, @IMPORTE, @MON, @CAMBIO, @PROVEEDORES, @CARGO, @FECNEC, @DEST, @PER, @ESTADO , @COMPRADOR,GETDATE())" & vbCrLf
sConsulta = sConsulta & "    SET @ID = (SELECT MAX(ID) FROM SOLICIT)" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    SELECT @ESTADOANTERIOR = ESTADO FROM SOLICIT WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    UPDATE SOLICIT" & vbCrLf
sConsulta = sConsulta & "       SET DEN=@DEN, DESCR= @DESCR, IMPORTE = @IMPORTE, MON=@MON, CAMBIO=@CAMBIO, PROVEEDORES=@PROVEEDORES, CARGO=@CARGO," & vbCrLf
sConsulta = sConsulta & "           FECNEC=@FECNEC, DEST=@DEST, PER=@PER,ESTADO=@ESTADO, COMPRADOR=@COMPRADOR" & vbCrLf
sConsulta = sConsulta & "     WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    IF @ESTADOANTERIOR!=@ESTADO " & vbCrLf
sConsulta = sConsulta & "      INSERT INTO SOLICIT_EST (SOLICIT, PER, FECHA, EST) VALUES (@ID, @PER, GETDATE(), @ESTADO)" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_APROBAR_SOLICITUD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_APROBAR_SOLICITUD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_APROBAR_SOLICITUD @ID int, @PER varchar(50)=NULL, @MOTIVO varchar(500) ,@COMP varchar(50)=NULL ,@MOTIVO_COMP varchar(500)=NULL AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO SOLICIT_EST(SOLICIT,PER, COMENT, FECHA,EST)" & vbCrLf
sConsulta = sConsulta & "VALUES (@ID, @PER, @MOTIVO, GETDATE(), 3)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @COMP IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "     UPDATE SOLICIT SET COMPRADOR=@COMP WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "     INSERT INTO SOLICIT_ASIG_COMP (SOLICIT,PER,COM,COMENT,FECHA) " & vbCrLf
sConsulta = sConsulta & "     VALUES (@ID, @PER, @COMP, @MOTIVO_COMP, GETDATE())" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE SOLICIT SET ESTADO = 3 WHERE ID = @ID" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ASIGNAR_COMP_SOLICITUD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_ASIGNAR_COMP_SOLICITUD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_ASIGNAR_COMP_SOLICITUD @ID int, @PER varchar(50)=NULL ,@COMP varchar(50), @COMENTARIO varchar(500)=NULL  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "     UPDATE SOLICIT SET COMPRADOR=@COMP WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "     INSERT INTO SOLICIT_ASIG_COMP (SOLICIT,PER,COM,COMENT,FECHA) " & vbCrLf
sConsulta = sConsulta & "     VALUES (@ID, @PER, @COMP, @COMENTARIO, GETDATE())" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_DEVOLVER_GRUPOS_ADJS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_DEVOLVER_GRUPOS_ADJS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_DEVOLVER_GRUPOS_ADJS @ANYO int, @GMN1 varchar(50), @PROCE int, @PROVE VARCHAR(100)=NULL AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ESTPROCE AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(3000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PARAM NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @CAMBIO FLOAT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @ESTPROCE=EST, @CAMBIO=CAMBIO FROM PROCE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "-- Si el proceso est� PC cojo el adjudicado de ar_item_prove si el grupo es abierto o de grupo_ofe si est� cerrado" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING = ' SELECT DISTINCT ITEM.GRUPO,PROCE_GRUPO.DEN,'" & vbCrLf
sConsulta = sConsulta & "IF @ESTPROCE=11" & vbCrLf
sConsulta = sConsulta & "  SET @SQLSTRING = @SQLSTRING + ' CASE WHEN PROCE_GRUPO.CERRADO=1 THEN GRUPO_OFE.ADJUDICADO/PROCE_OFE.CAMBIO " & vbCrLf
sConsulta = sConsulta & "          ELSE SUM(AR_ITEM_PROVE.PREC  * @CAMBIO) END AS ADJ'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   SET @SQLSTRING = @SQLSTRING + ' GRUPO_OFE.ADJUDICADO/PROCE_OFE.CAMBIO AS ADJ'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING= @SQLSTRING + '  FROM ITEM" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN ITEM_ADJ ON ITEM_ADJ.ANYO=ITEM.ANYO AND ITEM_ADJ.GMN1=ITEM.GMN1 AND ITEM_ADJ.PROCE=ITEM.PROCE  AND ITEM_ADJ.ITEM=ITEM.ID" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN GRUPO_OFE ON ITEM_ADJ.ANYO=GRUPO_OFE.ANYO AND ITEM_ADJ.GMN1=GRUPO_OFE.GMN1 AND ITEM_ADJ.PROCE=GRUPO_OFE.PROCE AND ITEM_ADJ.PROVE=GRUPO_OFE.PROVE AND ITEM_ADJ.OFE=GRUPO_OFE.OFE AND ITEM.GRUPO=GRUPO_OFE.GRUPO" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN PROCE_OFE ON PROCE_OFE.ANYO=GRUPO_OFE.ANYO AND PROCE_OFE.GMN1=GRUPO_OFE.GMN1 AND PROCE_OFE.PROCE=GRUPO_OFE.PROCE AND PROCE_OFE.PROVE=GRUPO_OFE.PROVE AND PROCE_OFE.OFE=GRUPO_OFE.OFE " & vbCrLf
sConsulta = sConsulta & "    INNER JOIN PROCE_GRUPO ON ITEM.ANYO=PROCE_GRUPO.ANYO AND ITEM.GMN1=PROCE_GRUPO.GMN1 AND ITEM.PROCE=PROCE_GRUPO.PROCE AND ITEM.GRUPO=PROCE_GRUPO.COD'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ESTPROCE=11" & vbCrLf
sConsulta = sConsulta & "   SET @SQLSTRING= @SQLSTRING + '  INNER JOIN AR_ITEM_PROVE ON ITEM.ANYO=AR_ITEM_PROVE.ANYO AND ITEM.GMN1=AR_ITEM_PROVE.GMN1 AND ITEM.PROCE=AR_ITEM_PROVE.PROCE  AND ITEM.ID=AR_ITEM_PROVE.ITEM  AND ITEM_ADJ.PROVE=AR_ITEM_PROVE.PROVE'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING= @SQLSTRING + '  WHERE ITEM_ADJ.ANYO=@ANYO AND ITEM_ADJ.GMN1=@GMN1 AND ITEM_ADJ.PROCE=@PROCE  AND ITEM_ADJ.PORCEN>0 AND ITEM.CONF=1'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @PROVE IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "    SET @SQLSTRING= @SQLSTRING +  '  AND ITEM_ADJ.PROVE=@PROVE'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ESTPROCE=11 " & vbCrLf
sConsulta = sConsulta & "    SET @SQLSTRING= @SQLSTRING +  ' AND ITEM.EST=1  GROUP BY ITEM.GRUPO,PROCE_GRUPO.DEN,PROCE_GRUPO.CERRADO,GRUPO_OFE.ADJUDICADO,PROCE_OFE.CAMBIO'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING= @SQLSTRING + '  ORDER BY ITEM.GRUPO,PROCE_GRUPO.DEN'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @PARAM = '@ANYO INT, @GMN1 varchar(50), @PROCE int, @PROVE VARCHAR(100),@CAMBIO FLOAT'" & vbCrLf
sConsulta = sConsulta & "EXECUTE sp_executesql @SQLSTRING,@PARAM,@ANYO=@ANYO, @GMN1=@GMN1, @PROCE=@PROCE,@PROVE=@PROVE,@CAMBIO=@CAMBIO" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_DEVOLVER_ITEMS_ADJS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_DEVOLVER_ITEMS_ADJS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_DEVOLVER_ITEMS_ADJS  @ANYO int, @GMN1 varchar(50), @PROCE int,@PROVE VARCHAR(50), @GRUPO VARCHAR(50)=NULL AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @GRUPO IS NOT NULL     " & vbCrLf
sConsulta = sConsulta & "    SELECT  ITEM_ADJ.ITEM,ITEM.GMN2,ITEM.GMN3,ITEM.GMN4,ITEM.ART,ITEM.DESCR + ART4.DEN AS DESCR,ITEM.DEST,ITEM.UNI,ITEM.CANT," & vbCrLf
sConsulta = sConsulta & "         ITEM.PREC,ITEM.PRES,ITEM.PAG,ITEM.FECINI,ITEM.FECFIN,ITEM.GRUPO," & vbCrLf
sConsulta = sConsulta & "         ITEM_ADJ.PORCEN, ITEM_OFE.PREC_VALIDO/PROCE_OFE.CAMBIO AS PRECIO, AR_ITEM_PROVE.PREC AS ADJUDICADO" & vbCrLf
sConsulta = sConsulta & "     FROM ITEM_ADJ INNER JOIN ITEM ON ITEM_ADJ.ANYO=ITEM.ANYO AND ITEM_ADJ.GMN1=ITEM.GMN1 AND ITEM_ADJ.PROCE=ITEM.PROCE AND ITEM_ADJ.ITEM=ITEM.ID  " & vbCrLf
sConsulta = sConsulta & "      INNER JOIN ITEM_OFE ON ITEM_ADJ.ANYO=ITEM_OFE.ANYO AND ITEM_ADJ.GMN1=ITEM_OFE.GMN1 AND ITEM_ADJ.PROCE=ITEM_OFE.PROCE AND ITEM_ADJ.PROVE=ITEM_OFE.PROVE AND ITEM_ADJ.OFE=ITEM_OFE.NUM AND ITEM_ADJ.ITEM=ITEM_OFE.ITEM" & vbCrLf
sConsulta = sConsulta & "      INNER JOIN PROCE_OFE ON ITEM_ADJ.ANYO=PROCE_OFE.ANYO AND ITEM_ADJ.GMN1=PROCE_OFE.GMN1 AND ITEM_ADJ.PROCE=PROCE_OFE.PROCE AND ITEM_ADJ.PROVE=PROCE_OFE.PROVE AND ITEM_ADJ.OFE=PROCE_OFE.OFE " & vbCrLf
sConsulta = sConsulta & "      INNER JOIN AR_ITEM_PROVE ON ITEM_ADJ.ANYO=AR_ITEM_PROVE.ANYO AND ITEM_ADJ.GMN1=AR_ITEM_PROVE.GMN1 AND ITEM_ADJ.PROCE=AR_ITEM_PROVE.PROCE AND ITEM_ADJ.PROVE=AR_ITEM_PROVE.PROVE AND ITEM_ADJ.ITEM=AR_ITEM_PROVE.ITEM" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN ART4 ON ITEM.GMN1=ART4.GMN1 AND ITEM.GMN2=ART4.GMN2 AND ITEM.GMN3=ART4.GMN3 AND ITEM.GMN4=ART4.GMN4 AND ITEM.ART=ART4.COD " & vbCrLf
sConsulta = sConsulta & "       WHERE ITEM_ADJ.ANYO=@ANYO AND ITEM_ADJ.GMN1=@GMN1 AND ITEM_ADJ.PROCE=@PROCE AND ITEM.GRUPO=@GRUPO AND ITEM_ADJ.PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "                      AND ITEM_ADJ.PORCEN >0  AND ITEM.CONF=1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "        ORDER BY ITEM.ART,ITEM.DESCR,ITEM_ADJ.ITEM" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "      SELECT  ITEM_ADJ.ITEM,ITEM.GMN2,ITEM.GMN3,ITEM.GMN4,ITEM.ART,ITEM.DESCR + ART4.DEN AS DESCR,ITEM.DEST,ITEM.UNI,ITEM.CANT," & vbCrLf
sConsulta = sConsulta & "         ITEM.PREC,ITEM.PRES,ITEM.PAG,ITEM.FECINI,ITEM.FECFIN,ITEM.GRUPO," & vbCrLf
sConsulta = sConsulta & "         ITEM_ADJ.PORCEN, ITEM_OFE.PREC_VALIDO/PROCE_OFE.CAMBIO AS PRECIO, AR_ITEM_PROVE.PREC AS ADJUDICADO" & vbCrLf
sConsulta = sConsulta & "                FROM ITEM_ADJ INNER JOIN ITEM ON ITEM_ADJ.ANYO=ITEM.ANYO AND ITEM_ADJ.GMN1=ITEM.GMN1 AND ITEM_ADJ.PROCE=ITEM.PROCE AND ITEM_ADJ.ITEM=ITEM.ID  " & vbCrLf
sConsulta = sConsulta & "      INNER JOIN ITEM_OFE ON ITEM_ADJ.ANYO=ITEM_OFE.ANYO AND ITEM_ADJ.GMN1=ITEM_OFE.GMN1 AND ITEM_ADJ.PROCE=ITEM_OFE.PROCE AND ITEM_ADJ.PROVE=ITEM_OFE.PROVE AND ITEM_ADJ.OFE=ITEM_OFE.NUM AND ITEM_ADJ.ITEM=ITEM_OFE.ITEM" & vbCrLf
sConsulta = sConsulta & "      INNER JOIN PROCE_OFE ON ITEM_ADJ.ANYO=PROCE_OFE.ANYO AND ITEM_ADJ.GMN1=PROCE_OFE.GMN1 AND ITEM_ADJ.PROCE=PROCE_OFE.PROCE AND ITEM_ADJ.PROVE=PROCE_OFE.PROVE AND ITEM_ADJ.OFE=PROCE_OFE.OFE " & vbCrLf
sConsulta = sConsulta & "      INNER JOIN AR_ITEM_PROVE ON ITEM_ADJ.ANYO=AR_ITEM_PROVE.ANYO AND ITEM_ADJ.GMN1=AR_ITEM_PROVE.GMN1 AND ITEM_ADJ.PROCE=AR_ITEM_PROVE.PROCE AND ITEM_ADJ.PROVE=AR_ITEM_PROVE.PROVE AND ITEM_ADJ.ITEM=AR_ITEM_PROVE.ITEM" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN ART4 ON ITEM.GMN1=ART4.GMN1 AND ITEM.GMN2=ART4.GMN2 AND ITEM.GMN3=ART4.GMN3 AND ITEM.GMN4=ART4.GMN4 AND ITEM.ART=ART4.COD " & vbCrLf
sConsulta = sConsulta & "       WHERE ITEM_ADJ.ANYO=@ANYO AND ITEM_ADJ.GMN1=@GMN1 AND ITEM_ADJ.PROCE=@PROCE AND ITEM_ADJ.PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "                      AND ITEM_ADJ.PORCEN >0  AND ITEM.CONF=1" & vbCrLf
sConsulta = sConsulta & "        ORDER BY ITEM.ART,ITEM.DESCR,ITEM_ADJ.ITEM" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_DEVOLVER_PEDIDOS_SOLICIT]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_DEVOLVER_PEDIDOS_SOLICIT]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_DEVOLVER_PEDIDOS_SOLICIT @ID INT ,@TIPO INT  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT O.ID, P.ANYO, P.NUM PNUM, P.TIPO, O.NUM ONUM, O.PROVE, PR.DEN, P.FECHA, O.EST , " & vbCrLf
sConsulta = sConsulta & "  LP.FECEMISION , O.NUMEXT , O.FECHA" & vbCrLf
sConsulta = sConsulta & "  FROM PEDIDO P" & vbCrLf
sConsulta = sConsulta & "         INNER JOIN ORDEN_ENTREGA O" & vbCrLf
sConsulta = sConsulta & "                 ON P.ID = O.PEDIDO" & vbCrLf
sConsulta = sConsulta & "         INNER JOIN PROVE PR" & vbCrLf
sConsulta = sConsulta & "                 ON O.PROVE = PR.COD" & vbCrLf
sConsulta = sConsulta & "         INNER JOIN LINEAS_PEDIDO LP" & vbCrLf
sConsulta = sConsulta & "                 ON O.ID = LP.ORDEN" & vbCrLf
sConsulta = sConsulta & " WHERE LP.SOLICIT = @ID AND P.TIPO=@TIPO" & vbCrLf
sConsulta = sConsulta & "  " & vbCrLf
sConsulta = sConsulta & "ORDER BY P.FECHA DESC" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_DOWNLOAD_ADJUNTO_SOLICIT]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_DOWNLOAD_ADJUNTO_SOLICIT]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_DOWNLOAD_ADJUNTO_SOLICIT @INIT INT, @OFFSET INT, @ID INT  AS " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "DECLARE @textpointer varbinary(16)  " & vbCrLf
sConsulta = sConsulta & "DECLARE @size int " & vbCrLf
sConsulta = sConsulta & "DECLARE @LOFFSET int " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "SET nocount on " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "SELECT @size=datalength(data), @textpointer = TEXTPTR(data)  FROM SOLICIT_ADJUN WHERE ID=@ID " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "if @INIT + @OFFSET > @SIZE " & vbCrLf
sConsulta = sConsulta & "    SET @LOFFSET = @SIZE-@INIT " & vbCrLf
sConsulta = sConsulta & "ELSE " & vbCrLf
sConsulta = sConsulta & "    BEGIN " & vbCrLf
sConsulta = sConsulta & "    SET @LOFFSET = @OFFSET +1 " & vbCrLf
sConsulta = sConsulta & "    END " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "READTEXT SOLICIT_ADJUN.data @textpointer @INIT  @LOFFSET" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_DOWNLOAD_CONTRATO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_DOWNLOAD_CONTRATO]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_DOWNLOAD_CONTRATO @INIT INT, @OFFSET INT, @ID INT  AS " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "DECLARE @textpointer varbinary(16)  " & vbCrLf
sConsulta = sConsulta & "DECLARE @size int " & vbCrLf
sConsulta = sConsulta & "DECLARE @LOFFSET int " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "SET nocount on " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "SELECT @size=datalength(data), @textpointer = TEXTPTR(data)  FROM PROCE_CONTR WHERE ID=@ID " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "if @INIT + @OFFSET > @SIZE " & vbCrLf
sConsulta = sConsulta & "    SET @LOFFSET = @SIZE-@INIT " & vbCrLf
sConsulta = sConsulta & "ELSE " & vbCrLf
sConsulta = sConsulta & "    BEGIN " & vbCrLf
sConsulta = sConsulta & "    SET @LOFFSET = @OFFSET +1 " & vbCrLf
sConsulta = sConsulta & "    END " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "READTEXT PROCE_CONTR.data @textpointer @INIT  @LOFFSET" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_NUEVO_ADJUNTO_SOLICIT]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_NUEVO_ADJUNTO_SOLICIT]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_NUEVO_ADJUNTO_SOLICIT  @NOM varchar(300), @DATA IMAGE, @MAXID int OUTPUT, @DATASIZE int OUTPUT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "set nocount on" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO SOLICIT_ADJUN (FECHA, NOM, DATA) VALUES (GETDATE(), @NOM ,@DATA)" & vbCrLf
sConsulta = sConsulta & "SET @MAXID= (SELECT MAX(ID) FROM SOLICIT_ADJUN)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @DATASIZE=DATALENGTH(DATA) FROM SOLICIT_ADJUN WHERE ID = @MAXID" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_REABRIR_SOLICITUD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_REABRIR_SOLICITUD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_REABRIR_SOLICITUD @ID int, @PER varchar(50), @MOTIVO varchar(500) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO SOLICIT_EST(SOLICIT,PER, COMENT, FECHA,EST)" & vbCrLf
sConsulta = sConsulta & "VALUES (@ID, @PER, @MOTIVO, GETDATE(), 3)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE SOLICIT" & vbCrLf
sConsulta = sConsulta & "   SET ESTADO = 3" & vbCrLf
sConsulta = sConsulta & " WHERE ID = @ID" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 
sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_RECHAZAR_SOLICITUD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_RECHAZAR_SOLICITUD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_RECHAZAR_SOLICITUD @ID int, @PER varchar(50), @MOTIVO varchar(500) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO SOLICIT_EST(SOLICIT,PER, COMENT, FECHA,EST)" & vbCrLf
sConsulta = sConsulta & "VALUES (@ID, @PER, @MOTIVO, GETDATE(), 4)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE SOLICIT" & vbCrLf
sConsulta = sConsulta & "   SET ESTADO = 4" & vbCrLf
sConsulta = sConsulta & " WHERE ID = @ID" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 
sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_SOLICITUD_DATOS_EMAIL]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_SOLICITUD_DATOS_EMAIL]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_SOLICITUD_DATOS_EMAIL @ID INT,@TYPE TINYINT, @EMAIL VARCHAR(100) OUTPUT,@IDIOMA VARCHAR(50) OUTPUT,@SUBJECT VARCHAR(2000) OUTPUT AS" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "/* " & vbCrLf
sConsulta = sConsulta & "   Par�metros de entrada:  ID  : Identificador de la solicitud" & vbCrLf
sConsulta = sConsulta & "               TYPE    :   0 -> Asignaci�n a comprador" & vbCrLf
sConsulta = sConsulta & "                       1 -> Aprobaci�n a peticionario" & vbCrLf
sConsulta = sConsulta & "                       2 -> Rechazo" & vbCrLf
sConsulta = sConsulta & "                       3 -> Anulaci�n desde GS         " & vbCrLf
sConsulta = sConsulta & "                       4 -> Cierre" & vbCrLf
sConsulta = sConsulta & "                       5 -> Reapertura" & vbCrLf
sConsulta = sConsulta & "                       6 -> Alta de solicitud " & vbCrLf
sConsulta = sConsulta & "                       7 -> Anulaci�n desde el EP" & vbCrLf
sConsulta = sConsulta & "   Par�metros de s�lida:   EMAIL   : Email del peticionario, menos en el tipo 0 que ser�a el del comprador" & vbCrLf
sConsulta = sConsulta & "               IDIOMA  : Idioma del usuario que ha realizado la solicitud, seg�n su preferencia indicada en el EP" & vbCrLf
sConsulta = sConsulta & "               SUBJECT: Asunto del mensaje" & vbCrLf
sConsulta = sConsulta & "*/" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @TYPE=0" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @EMAIL= (SELECT PER.EMAIL FROM PER INNER JOIN SOLICIT ON SOLICIT.ID=@ID AND PER.COD=SOLICIT.COMPRADOR)" & vbCrLf
sConsulta = sConsulta & "   SET @IDIOMA=(SELECT IDIOMA FROM PARGEN_DEF WHERE ID=1)" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = N' SET @SUBJECT= (SELECT TEXT_' + @IDIOMA + N' FROM WEBTEXT WHERE MODULO=101 AND ID=1)'" & vbCrLf
sConsulta = sConsulta & "   EXEC sp_executesql  @SQL ,N'@SUBJECT VARCHAR(2000) OUTPUT', @SUBJECT=@SUBJECT OUTPUT" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   IF @TYPE=1" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "       SET @EMAIL= (SELECT PER.EMAIL FROM PER INNER JOIN SOLICIT ON SOLICIT.ID=@ID AND PER.COD=SOLICIT.PER)" & vbCrLf
sConsulta = sConsulta & "       SET @IDIOMA=(SELECT IDIOMA FROM USU INNER JOIN SOLICIT ON SOLICIT.ID=@ID AND USU.PER=SOLICIT.PER)" & vbCrLf
sConsulta = sConsulta & "       IF @IDIOMA IS NULL   SET @IDIOMA=(SELECT IDIOMA FROM PARGEN_DEF WHERE ID=1)" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = N' SET @SUBJECT= (SELECT TEXT_' + @IDIOMA + N' FROM WEBTEXT WHERE MODULO=101 AND ID=2)'" & vbCrLf
sConsulta = sConsulta & "       EXEC sp_executesql  @SQL ,N'@SUBJECT VARCHAR(2000) OUTPUT', @SUBJECT=@SUBJECT OUTPUT" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "       IF @TYPE=2" & vbCrLf
sConsulta = sConsulta & "           BEGIN" & vbCrLf
sConsulta = sConsulta & "           SET @EMAIL= (SELECT PER.EMAIL FROM PER INNER JOIN SOLICIT ON SOLICIT.ID=@ID AND PER.COD=SOLICIT.PER)" & vbCrLf
sConsulta = sConsulta & "           SET @IDIOMA=(SELECT IDIOMA FROM USU INNER JOIN SOLICIT ON SOLICIT.ID=@ID AND USU.PER=SOLICIT.PER)" & vbCrLf
sConsulta = sConsulta & "           IF @IDIOMA IS NULL  SET @IDIOMA=(SELECT IDIOMA FROM PARGEN_DEF WHERE ID=1)" & vbCrLf
sConsulta = sConsulta & "           SET @SQL = N' SET @SUBJECT= (SELECT TEXT_' + @IDIOMA + N' FROM WEBTEXT WHERE MODULO=101 AND ID=3)'" & vbCrLf
sConsulta = sConsulta & "           EXEC sp_executesql  @SQL ,N'@SUBJECT VARCHAR(2000) OUTPUT', @SUBJECT=@SUBJECT OUTPUT" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "       ELSE" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "           IF @TYPE=3" & vbCrLf
sConsulta = sConsulta & "               BEGIN" & vbCrLf
sConsulta = sConsulta & "               SET @EMAIL= (SELECT PER.EMAIL FROM PER INNER JOIN SOLICIT ON SOLICIT.ID=@ID AND PER.COD=SOLICIT.PER)" & vbCrLf
sConsulta = sConsulta & "               SET @IDIOMA=(SELECT IDIOMA FROM USU INNER JOIN SOLICIT ON SOLICIT.ID=@ID AND USU.PER=SOLICIT.PER)" & vbCrLf
sConsulta = sConsulta & "               IF @IDIOMA IS NULL  SET @IDIOMA=(SELECT IDIOMA FROM PARGEN_DEF WHERE ID=1)" & vbCrLf
sConsulta = sConsulta & "               SET @SQL = N' SET @SUBJECT= (SELECT TEXT_' + @IDIOMA + N' FROM WEBTEXT WHERE MODULO=101 AND ID=4)'" & vbCrLf
sConsulta = sConsulta & "               EXEC sp_executesql  @SQL ,N'@SUBJECT VARCHAR(2000) OUTPUT', @SUBJECT=@SUBJECT OUTPUT" & vbCrLf
sConsulta = sConsulta & "               END" & vbCrLf
sConsulta = sConsulta & "           ELSE" & vbCrLf
sConsulta = sConsulta & "               " & vbCrLf
sConsulta = sConsulta & "               IF @TYPE=4" & vbCrLf
sConsulta = sConsulta & "                   BEGIN" & vbCrLf
sConsulta = sConsulta & "                   SET @EMAIL= (SELECT PER.EMAIL FROM PER INNER JOIN SOLICIT ON SOLICIT.ID=@ID AND PER.COD=SOLICIT.PER)" & vbCrLf
sConsulta = sConsulta & "                   SET @IDIOMA=(SELECT IDIOMA FROM USU INNER JOIN SOLICIT ON SOLICIT.ID=@ID AND USU.PER=SOLICIT.PER)" & vbCrLf
sConsulta = sConsulta & "                   IF @IDIOMA IS NULL  SET @IDIOMA=(SELECT IDIOMA FROM PARGEN_DEF WHERE ID=1)" & vbCrLf
sConsulta = sConsulta & "                   SET @SQL = N' SET @SUBJECT= (SELECT TEXT_' + @IDIOMA + N' FROM WEBTEXT WHERE MODULO=101 AND ID=5)'" & vbCrLf
sConsulta = sConsulta & "                   EXEC sp_executesql  @SQL ,N'@SUBJECT VARCHAR(2000) OUTPUT', @SUBJECT=@SUBJECT OUTPUT" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               ELSE" & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                   IF @TYPE=5" & vbCrLf
sConsulta = sConsulta & "                       BEGIN" & vbCrLf
sConsulta = sConsulta & "                       SET @EMAIL= (SELECT PER.EMAIL FROM PER INNER JOIN SOLICIT ON SOLICIT.ID=@ID AND PER.COD=SOLICIT.PER)" & vbCrLf
sConsulta = sConsulta & "                       SET @IDIOMA=(SELECT IDIOMA FROM USU INNER JOIN SOLICIT ON SOLICIT.ID=@ID AND USU.PER=SOLICIT.PER)" & vbCrLf
sConsulta = sConsulta & "                       IF @IDIOMA IS NULL  SET @IDIOMA=(SELECT IDIOMA FROM PARGEN_DEF WHERE ID=1)" & vbCrLf
sConsulta = sConsulta & "                       SET @SQL = N' SET @SUBJECT= (SELECT TEXT_' + @IDIOMA + N' FROM WEBTEXT WHERE MODULO=101 AND ID=6)'" & vbCrLf
sConsulta = sConsulta & "                       EXEC sp_executesql  @SQL ,N'@SUBJECT VARCHAR(2000) OUTPUT', @SUBJECT=@SUBJECT OUTPUT" & vbCrLf
sConsulta = sConsulta & "                       END" & vbCrLf
sConsulta = sConsulta & "                   ELSE" & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                       IF @TYPE=6" & vbCrLf
sConsulta = sConsulta & "                           BEGIN" & vbCrLf
sConsulta = sConsulta & "                           SET @EMAIL= (SELECT PER.EMAIL FROM PER INNER JOIN SOLICIT ON SOLICIT.ID=@ID AND PER.COD=SOLICIT.COMPRADOR)" & vbCrLf
sConsulta = sConsulta & "                           SET @IDIOMA=(SELECT IDIOMA FROM PARGEN_DEF WHERE ID=1)" & vbCrLf
sConsulta = sConsulta & "                           SET @SQL = N' SET @SUBJECT= (SELECT TEXT_' + @IDIOMA + N' FROM WEBTEXT WHERE MODULO=101 AND ID=7)'" & vbCrLf
sConsulta = sConsulta & "                           EXEC sp_executesql  @SQL ,N'@SUBJECT VARCHAR(2000) OUTPUT', @SUBJECT=@SUBJECT OUTPUT" & vbCrLf
sConsulta = sConsulta & "                           END" & vbCrLf
sConsulta = sConsulta & "                       ELSE" & vbCrLf
sConsulta = sConsulta & "                       " & vbCrLf
sConsulta = sConsulta & "                           IF @TYPE=7" & vbCrLf
sConsulta = sConsulta & "                               BEGIN" & vbCrLf
sConsulta = sConsulta & "                               SET @EMAIL= (SELECT PER.EMAIL FROM PER INNER JOIN SOLICIT ON SOLICIT.ID=@ID AND PER.COD=SOLICIT.COMPRADOR)" & vbCrLf
sConsulta = sConsulta & "                               SET @IDIOMA=(SELECT IDIOMA FROM PARGEN_DEF WHERE ID=1)" & vbCrLf
sConsulta = sConsulta & "                               SET @SQL = N' SET @SUBJECT= (SELECT TEXT_' + @IDIOMA + N' FROM WEBTEXT WHERE MODULO=101 AND ID=8)'" & vbCrLf
sConsulta = sConsulta & "                               EXEC sp_executesql  @SQL ,N'@SUBJECT VARCHAR(2000) OUTPUT', @SUBJECT=@SUBJECT OUTPUT" & vbCrLf
sConsulta = sConsulta & "                               END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_SOLICITUD_DATOS_MENSAJE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_SOLICITUD_DATOS_MENSAJE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_SOLICITUD_DATOS_MENSAJE " & vbCrLf
sConsulta = sConsulta & "@ID INT," & vbCrLf
sConsulta = sConsulta & "@TYPE TINYINT, " & vbCrLf
sConsulta = sConsulta & "@DESCR_BREVE VarChar(50) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@DESCR_DET VarChar( 500) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@FECHA_ALTA DateTIME OUTPUT," & vbCrLf
sConsulta = sConsulta & "@FECHA_NECESIDAD DateTIME OUTPUT," & vbCrLf
sConsulta = sConsulta & "@IMPORTE FLOAT OUTPUT," & vbCrLf
sConsulta = sConsulta & "@MON_COD VarChar (50) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@MON_DEN VarChar(100) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@APROBADOR VarChar(150) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@APROBADOR_COD VarChar( 50) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@APROBADOR_NOM VarChar(50) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@APROBADOR_APE VarChar(100) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@APROBADOR_TFNO VarChar(20) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@APROBADOR_EMAIL VarChar(100) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@APROBADOR_FAX VarChar(20) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@APROBADOR_UON varChar(250) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@APROBADOR_DEP_COD VarChar(50) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@APROBADOR_DEP_DEN varChar(100) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@COMPRADOR_COD VarChar( 50) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@COMPRADOR_NOM VarChar(50) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@COMPRADOR_APE VarChar(100) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@COMPRADOR_TFNO VarChar(20) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@COMPRADOR_EMAIL VarChar(100) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@COMPRADOR_FAX VarChar(20) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@COMPRADOR_UON varChar(250) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@COMPRADOR_DEP_COD VarChar(50) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@COMPRADOR_DEP_DEN varChar(100) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@PET_COD VarChar(50) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@PET_NOM VarChar(50) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@PET_APE VarChar(100) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@PET_TFNO varchar(20) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@PET_EMAIL VarChar(100) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@PET_FAX VarChar(20) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@PET_UON VarChar(250) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@PET_DEP_COD VarChar(50) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@PET_DEP_DEN VarChar(100) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@CARGO VarChar(100) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@PROVEEDORES varChar(300) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@DEST_COD VarChar(50) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@DEST_DEN VarChar(100) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@DEST_DIR VarChar(255) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@DEST_POB VarChar(100) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@DEST_CP VarChar(20) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@DEST_PROVI_COD VarChar(50) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@DEST_PROVI_DEN VarChar(100) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@DEST_PAI_COD VarChar(50) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@DEST_PAI_DEN VarChar(100) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@COMENTARIO varChar(500) OUTPUT" & vbCrLf
sConsulta = sConsulta & "AS   " & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "/* " & vbCrLf
sConsulta = sConsulta & "   Par�metros de entrada:  ID  : Identificador de la solicitud" & vbCrLf
sConsulta = sConsulta & "               TYPE    :   " & vbCrLf
sConsulta = sConsulta & "                       0 -> Asignaci�n a comprador" & vbCrLf
sConsulta = sConsulta & "                       1 -> Aprobaci�n a peticionario" & vbCrLf
sConsulta = sConsulta & "                       2 -> Rechazo" & vbCrLf
sConsulta = sConsulta & "                       3 -> Anulaci�n          " & vbCrLf
sConsulta = sConsulta & "                       4 -> Cierre" & vbCrLf
sConsulta = sConsulta & "                       5 -> Reapertura" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "*/" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @TYPE=0" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "   SELECT " & vbCrLf
sConsulta = sConsulta & "   @DESCR_BREVE=S.DEN," & vbCrLf
sConsulta = sConsulta & "   @DESCR_DET =S.DESCR," & vbCrLf
sConsulta = sConsulta & "   @FECHA_ALTA = S.FECALTA," & vbCrLf
sConsulta = sConsulta & "   @FECHA_NECESIDAD=S.FECNEC," & vbCrLf
sConsulta = sConsulta & "   @IMPORTE =S.IMPORTE," & vbCrLf
sConsulta = sConsulta & "   @MON_COD =S.MON," & vbCrLf
sConsulta = sConsulta & "   @MON_DEN =M.DEN," & vbCrLf
sConsulta = sConsulta & "   @APROBADOR = " & vbCrLf
sConsulta = sConsulta & "   case when SEST.PER IS NULL THEN" & vbCrLf
sConsulta = sConsulta & "       (select usu from adm)       " & vbCrLf
sConsulta = sConsulta & "       ELSE" & vbCrLf
sConsulta = sConsulta & "       P2.NOM + ' ' + P2.APE" & vbCrLf
sConsulta = sConsulta & "   end," & vbCrLf
sConsulta = sConsulta & "   @APROBADOR_COD = " & vbCrLf
sConsulta = sConsulta & "   case when SEST.PER IS NULL THEN" & vbCrLf
sConsulta = sConsulta & "       (select usu from adm)       " & vbCrLf
sConsulta = sConsulta & "       ELSE" & vbCrLf
sConsulta = sConsulta & "       SEST.PER            " & vbCrLf
sConsulta = sConsulta & "       END," & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   @APROBADOR_NOM = P2.NOM," & vbCrLf
sConsulta = sConsulta & "   @APROBADOR_APE = P2.APE," & vbCrLf
sConsulta = sConsulta & "   @APROBADOR_TFNO =P2.TFNO," & vbCrLf
sConsulta = sConsulta & "   @APROBADOR_EMAIL = P2.EMAIL," & vbCrLf
sConsulta = sConsulta & "   @APROBADOR_FAX =P2.FAX," & vbCrLf
sConsulta = sConsulta & "   @APROBADOR_UON = " & vbCrLf
sConsulta = sConsulta & "   case when U23.cod is null then" & vbCrLf
sConsulta = sConsulta & "       case when U22.cod is null then" & vbCrLf
sConsulta = sConsulta & "                       CASE WHEN U21.COD IS NULL THEN" & vbCrLf
sConsulta = sConsulta & "                ' '" & vbCrLf
sConsulta = sConsulta & "                               ELSE" & vbCrLf
sConsulta = sConsulta & "                                     U21.cod + ' - ' + U21.den" & vbCrLf
sConsulta = sConsulta & "                                END" & vbCrLf
sConsulta = sConsulta & "                  ELSE" & vbCrLf
sConsulta = sConsulta & "           U21.COD + ' - ' + U22.COD + ' - ' + U22.DEN                        " & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "       U21.COD + ' - ' + U22.COD + ' - ' + U23.COD + ' - '  + U23.DEN" & vbCrLf
sConsulta = sConsulta & "   END," & vbCrLf
sConsulta = sConsulta & "   @APROBADOR_DEP_COD=P2.DEP," & vbCrLf
sConsulta = sConsulta & "   @APROBADOR_DEP_DEN = D2.DEN," & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   @COMPRADOR_COD = S.COMPRADOR," & vbCrLf
sConsulta = sConsulta & "   @COMPRADOR_NOM = P.NOM," & vbCrLf
sConsulta = sConsulta & "   @COMPRADOR_APE= P.APE," & vbCrLf
sConsulta = sConsulta & "   @COMPRADOR_TFNO =P.TFNO," & vbCrLf
sConsulta = sConsulta & "   @COMPRADOR_EMAIL= P.EMAIL," & vbCrLf
sConsulta = sConsulta & "   @COMPRADOR_FAX =P.FAX," & vbCrLf
sConsulta = sConsulta & "   @COMPRADOR_UON= " & vbCrLf
sConsulta = sConsulta & "   case when U3.cod is null then" & vbCrLf
sConsulta = sConsulta & "       case when U2.cod is null then" & vbCrLf
sConsulta = sConsulta & "                       CASE WHEN U1.COD IS NULL THEN" & vbCrLf
sConsulta = sConsulta & "                ' '" & vbCrLf
sConsulta = sConsulta & "                               ELSE" & vbCrLf
sConsulta = sConsulta & "                                     U1.cod + ' - ' + U1.den" & vbCrLf
sConsulta = sConsulta & "                                END" & vbCrLf
sConsulta = sConsulta & "                  ELSE" & vbCrLf
sConsulta = sConsulta & "           U1.COD + ' - ' + U2.COD + ' - ' + U2.DEN                        " & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "       U1.COD + ' - ' + U2.COD + ' - ' + U3.COD + ' - '  + U3.DEN" & vbCrLf
sConsulta = sConsulta & "   END," & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   @PET_COD = S.PER," & vbCrLf
sConsulta = sConsulta & "   @PET_NOM = PET.NOM," & vbCrLf
sConsulta = sConsulta & "   @PET_APE = PET.APE," & vbCrLf
sConsulta = sConsulta & "   @PET_TFNO =PET.TFNO," & vbCrLf
sConsulta = sConsulta & "   @PET_EMAIL = PET.EMAIL," & vbCrLf
sConsulta = sConsulta & "   @PET_FAX = PET.FAX," & vbCrLf
sConsulta = sConsulta & "   @PET_UON = " & vbCrLf
sConsulta = sConsulta & "   case when U_PET3.cod is null then" & vbCrLf
sConsulta = sConsulta & "       case when U_PET2.cod is null then" & vbCrLf
sConsulta = sConsulta & "                       CASE WHEN U_PET1.COD IS NULL THEN" & vbCrLf
sConsulta = sConsulta & "                ' '" & vbCrLf
sConsulta = sConsulta & "                               ELSE" & vbCrLf
sConsulta = sConsulta & "                                     U_PET1.cod + ' - ' + U_PET1.den" & vbCrLf
sConsulta = sConsulta & "                                END" & vbCrLf
sConsulta = sConsulta & "                  ELSE" & vbCrLf
sConsulta = sConsulta & "           U_PET1.COD + ' - ' + U_PET2.COD + ' - ' + U_PET2.DEN                        " & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "       U_PET1.COD + ' - ' + U_PET2.COD + ' - ' + U_PET3.COD + ' - '  + U_PET3.DEN" & vbCrLf
sConsulta = sConsulta & "   END," & vbCrLf
sConsulta = sConsulta & "   @PET_DEP_COD= PET.DEP," & vbCrLf
sConsulta = sConsulta & "   @PET_DEP_DEN = PET_D.DEN," & vbCrLf
sConsulta = sConsulta & "   @CARGO = S.CARGO," & vbCrLf
sConsulta = sConsulta & "   @PROVEEDORES = S.PROVEEDORES," & vbCrLf
sConsulta = sConsulta & "   @DEST_COD= S.DEST," & vbCrLf
sConsulta = sConsulta & "   @DEST_DEN = DEST.DEN," & vbCrLf
sConsulta = sConsulta & "   @DEST_DIR =DEST.DIR," & vbCrLf
sConsulta = sConsulta & "   @DEST_POB =DEST.POB," & vbCrLf
sConsulta = sConsulta & "   @DEST_CP = DEST.CP," & vbCrLf
sConsulta = sConsulta & "   @DEST_PROVI_COD = DEST.PROVI," & vbCrLf
sConsulta = sConsulta & "   @DEST_PROVI_DEN = PROVI.DEN," & vbCrLf
sConsulta = sConsulta & "   @DEST_PAI_COD = DEST.PAI," & vbCrLf
sConsulta = sConsulta & "   @DEST_PAI_DEN = PAI.DEN," & vbCrLf
sConsulta = sConsulta & "   @COMENTARIO = SEST.COMENT" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   FROM SOLICIT S" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN SOLICIT_ASIG_COMP SEST ON S.ID=SEST.SOLICIT AND S.ID=@ID " & vbCrLf
sConsulta = sConsulta & "   AND SEST.ID IN (SELECT MAX (ID) FROM SOLICIT_ASIG_COMP WHERE SOLICIT=@ID) " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN MON M ON S.MON=M.COD" & vbCrLf
sConsulta = sConsulta & "   AND S.ID=@ID" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PER P ON S.COMPRADOR=P.COD" & vbCrLf
sConsulta = sConsulta & "   AND S.ID=@ID" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN DEP D ON P.DEP=D.COD" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN UON1 U1 ON U1.COD=P.UON1" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN UON2 U2 ON U2.UON1=P.UON1 AND P.UON2=U2.COD" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN UON3 U3 ON U3.UON1=P.UON1 AND P.UON2=U3.UON2 AND P.UON3=U3.COD" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN PER P2 ON SEST.PER=P2.COD" & vbCrLf
sConsulta = sConsulta & "   AND S.ID=@ID" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN DEP D2 ON P2.DEP=D2.COD" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN UON1 U21 ON U21.COD=P2.UON1" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN UON2 U22 ON U22.UON1=P2.UON1 AND P2.UON2=U22.COD" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN UON3 U23 ON U23.UON1=P2.UON1 AND P2.UON2=U23.UON2 AND P2.UON3=U23.COD" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PER PET ON S.PER=PET.COD AND S.ID=@ID" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN DEP PET_D ON P.DEP=PET_D.COD" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN UON1 U_PET1 ON U_PET1.COD=PET.UON1" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN UON2 U_PET2 ON U_PET2.UON1=PET.UON1 AND PET.UON2=U_PET2.COD" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN UON3 U_PET3 ON U_PET3.UON1=PET.UON1 AND PET.UON2=U_PET3.UON2 AND PET.UON3=U_PET3.COD" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN DEST ON S.DEST=DEST.COD" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PAI ON DEST.PAI=PAI.COD" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PROVI ON PROVI.PAI=PAI.COD AND PROVI.COD=DEST.PROVI" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT " & vbCrLf
sConsulta = sConsulta & "@DESCR_BREVE=S.DEN," & vbCrLf
sConsulta = sConsulta & "@DESCR_DET =S.DESCR," & vbCrLf
sConsulta = sConsulta & "@FECHA_ALTA = S.FECALTA," & vbCrLf
sConsulta = sConsulta & "@FECHA_NECESIDAD=S.FECNEC," & vbCrLf
sConsulta = sConsulta & "@IMPORTE =S.IMPORTE," & vbCrLf
sConsulta = sConsulta & "@MON_COD =S.MON," & vbCrLf
sConsulta = sConsulta & "@MON_DEN =M.DEN," & vbCrLf
sConsulta = sConsulta & "@APROBADOR = " & vbCrLf
sConsulta = sConsulta & "case when SEST.PER IS NULL THEN" & vbCrLf
sConsulta = sConsulta & "   (select usu from adm)       " & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "   P2.NOM + ' ' + P2.APE" & vbCrLf
sConsulta = sConsulta & "end," & vbCrLf
sConsulta = sConsulta & "@APROBADOR_COD = " & vbCrLf
sConsulta = sConsulta & "case when SEST.PER IS NULL THEN" & vbCrLf
sConsulta = sConsulta & "   (select usu from adm)       " & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "   SEST.PER            " & vbCrLf
sConsulta = sConsulta & "   END," & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "@APROBADOR_NOM = P2.NOM," & vbCrLf
sConsulta = sConsulta & "@APROBADOR_APE = P2.APE," & vbCrLf
sConsulta = sConsulta & "@APROBADOR_TFNO =P2.TFNO," & vbCrLf
sConsulta = sConsulta & "@APROBADOR_EMAIL = P2.EMAIL," & vbCrLf
sConsulta = sConsulta & "@APROBADOR_FAX =P2.FAX," & vbCrLf
sConsulta = sConsulta & "@APROBADOR_UON = " & vbCrLf
sConsulta = sConsulta & "case when U23.cod is null then" & vbCrLf
sConsulta = sConsulta & "   case when U22.cod is null then" & vbCrLf
sConsulta = sConsulta & "                   CASE WHEN U21.COD IS NULL THEN" & vbCrLf
sConsulta = sConsulta & "            ' '" & vbCrLf
sConsulta = sConsulta & "                            ELSE" & vbCrLf
sConsulta = sConsulta & "                                  U21.cod + ' - ' + U21.den" & vbCrLf
sConsulta = sConsulta & "                             END" & vbCrLf
sConsulta = sConsulta & "               ELSE" & vbCrLf
sConsulta = sConsulta & "       U21.COD + ' - ' + U22.COD + ' - ' + U22.DEN                        " & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   U21.COD + ' - ' + U22.COD + ' - ' + U23.COD + ' - '  + U23.DEN" & vbCrLf
sConsulta = sConsulta & "END," & vbCrLf
sConsulta = sConsulta & "@APROBADOR_DEP_COD=P2.DEP," & vbCrLf
sConsulta = sConsulta & "@APROBADOR_DEP_DEN = D2.DEN," & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "@COMPRADOR_COD = S.COMPRADOR," & vbCrLf
sConsulta = sConsulta & "@COMPRADOR_NOM = P.NOM," & vbCrLf
sConsulta = sConsulta & "@COMPRADOR_APE= P.APE," & vbCrLf
sConsulta = sConsulta & "@COMPRADOR_TFNO =P.TFNO," & vbCrLf
sConsulta = sConsulta & "@COMPRADOR_EMAIL= P.EMAIL," & vbCrLf
sConsulta = sConsulta & "@COMPRADOR_FAX =P.FAX," & vbCrLf
sConsulta = sConsulta & "@COMPRADOR_UON= " & vbCrLf
sConsulta = sConsulta & "case when U3.cod is null then" & vbCrLf
sConsulta = sConsulta & "   case when U2.cod is null then" & vbCrLf
sConsulta = sConsulta & "                   CASE WHEN U1.COD IS NULL THEN" & vbCrLf
sConsulta = sConsulta & "            ' '" & vbCrLf
sConsulta = sConsulta & "                            ELSE" & vbCrLf
sConsulta = sConsulta & "                                  U1.cod + ' - ' + U1.den" & vbCrLf
sConsulta = sConsulta & "                             END" & vbCrLf
sConsulta = sConsulta & "               ELSE" & vbCrLf
sConsulta = sConsulta & "       U1.COD + ' - ' + U2.COD + ' - ' + U2.DEN                        " & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   U1.COD + ' - ' + U2.COD + ' - ' + U3.COD + ' - '  + U3.DEN" & vbCrLf
sConsulta = sConsulta & "END," & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "@PET_COD = S.PER," & vbCrLf
sConsulta = sConsulta & "@PET_NOM = PET.NOM," & vbCrLf
sConsulta = sConsulta & "@PET_APE = PET.APE," & vbCrLf
sConsulta = sConsulta & "@PET_TFNO =PET.TFNO," & vbCrLf
sConsulta = sConsulta & "@PET_EMAIL = PET.EMAIL," & vbCrLf
sConsulta = sConsulta & "@PET_FAX = PET.FAX," & vbCrLf
sConsulta = sConsulta & "@PET_UON = " & vbCrLf
sConsulta = sConsulta & "case when U_PET3.cod is null then" & vbCrLf
sConsulta = sConsulta & "   case when U_PET2.cod is null then" & vbCrLf
sConsulta = sConsulta & "                   CASE WHEN U_PET1.COD IS NULL THEN" & vbCrLf
sConsulta = sConsulta & "            ' '" & vbCrLf
sConsulta = sConsulta & "                            ELSE" & vbCrLf
sConsulta = sConsulta & "                                  U_PET1.cod + ' - ' + U_PET1.den" & vbCrLf
sConsulta = sConsulta & "                             END" & vbCrLf
sConsulta = sConsulta & "               ELSE" & vbCrLf
sConsulta = sConsulta & "       U_PET1.COD + ' - ' + U_PET2.COD + ' - ' + U_PET2.DEN                        " & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   U_PET1.COD + ' - ' + U_PET2.COD + ' - ' + U_PET3.COD + ' - '  + U_PET3.DEN" & vbCrLf
sConsulta = sConsulta & "END," & vbCrLf
sConsulta = sConsulta & "@PET_DEP_COD= PET.DEP," & vbCrLf
sConsulta = sConsulta & "@PET_DEP_DEN = PET_D.DEN," & vbCrLf
sConsulta = sConsulta & "@CARGO = S.CARGO," & vbCrLf
sConsulta = sConsulta & "@PROVEEDORES = S.PROVEEDORES," & vbCrLf
sConsulta = sConsulta & "@DEST_COD= S.DEST," & vbCrLf
sConsulta = sConsulta & "@DEST_DEN = DEST.DEN," & vbCrLf
sConsulta = sConsulta & "@DEST_DIR =DEST.DIR," & vbCrLf
sConsulta = sConsulta & "@DEST_POB =DEST.POB," & vbCrLf
sConsulta = sConsulta & "@DEST_CP = DEST.CP," & vbCrLf
sConsulta = sConsulta & "@DEST_PROVI_COD = DEST.PROVI," & vbCrLf
sConsulta = sConsulta & "@DEST_PROVI_DEN = PROVI.DEN," & vbCrLf
sConsulta = sConsulta & "@DEST_PAI_COD = DEST.PAI," & vbCrLf
sConsulta = sConsulta & "@DEST_PAI_DEN = PAI.DEN," & vbCrLf
sConsulta = sConsulta & "@COMENTARIO = SEST.COMENT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "FROM SOLICIT S" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN SOLICIT_EST SEST ON S.ID=SEST.SOLICIT AND S.ID=@ID AND SEST.ID IN (SELECT MAX(ID) FROM SOLICIT_EST WHERE SOLICIT=@ID)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INNER JOIN MON M ON S.MON=M.COD" & vbCrLf
sConsulta = sConsulta & "AND S.ID=@ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PER P ON S.COMPRADOR=P.COD" & vbCrLf
sConsulta = sConsulta & "AND S.ID=@ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN DEP D ON P.DEP=D.COD" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN UON1 U1 ON U1.COD=P.UON1" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN UON2 U2 ON U2.UON1=P.UON1 AND P.UON2=U2.COD" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN UON3 U3 ON U3.UON1=P.UON1 AND P.UON2=U3.UON2 AND P.UON3=U3.COD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PER P2 ON SEST.PER=P2.COD" & vbCrLf
sConsulta = sConsulta & "AND S.ID=@ID" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN DEP D2 ON P2.DEP=D2.COD" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN UON1 U21 ON U21.COD=P2.UON1" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN UON2 U22 ON U22.UON1=P2.UON1 AND P2.UON2=U22.COD" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN UON3 U23 ON U23.UON1=P2.UON1 AND P2.UON2=U23.UON2 AND P2.UON3=U23.COD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PER PET ON S.PER=PET.COD AND S.ID=@ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN DEP PET_D ON P.DEP=PET_D.COD" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN UON1 U_PET1 ON U_PET1.COD=PET.UON1" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN UON2 U_PET2 ON U_PET2.UON1=PET.UON1 AND PET.UON2=U_PET2.COD" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN UON3 U_PET3 ON U_PET3.UON1=PET.UON1 AND PET.UON2=U_PET3.UON2 AND PET.UON3=U_PET3.COD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INNER JOIN DEST ON S.DEST=DEST.COD" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PAI ON DEST.PAI=PAI.COD" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PROVI ON PROVI.PAI=PAI.COD AND PROVI.COD=DEST.PROVI" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_UPLOAD_CONTRATO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_UPLOAD_CONTRATO]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_UPLOAD_CONTRATO @INIT INT,@ID INT,@DATA IMAGE  AS " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @textpointer varbinary(16)   " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "SET nocount on " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "SELECT @textpointer = TEXTPTR(data)  FROM PROCE_CONTR WHERE ID=@ID " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "IF @TEXTPOINTER  IS NULL " & vbCrLf
sConsulta = sConsulta & "    UPDATE PROCE_CONTR SET DATA = @DATA WHERE ID =@ID " & vbCrLf
sConsulta = sConsulta & "ELSE " & vbCrLf
sConsulta = sConsulta & "    UPDATETEXT PROCE_CONTR.DATA @textpointer @init 0 @data" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 
End Function

Private Function V_2_10_5_Storeds_2()
Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_CREAR_PEDIDO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_CREAR_PEDIDO]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSEP_CREAR_PEDIDO (@PER VARCHAR(30),  @PEDIDO INT OUTPUT) AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYOPROCE AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVE AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVE2 AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @CANT_PED AS FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRECIO_UP AS FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @SUMA AS FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @FC AS FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @NUMPEDIDO AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @ORDEN AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @ART_INT AS VARCHAR(20)" & vbCrLf
sConsulta = sConsulta & "DECLARE @UP AS VARCHAR(20)" & vbCrLf
sConsulta = sConsulta & "DECLARE @DEST AS VARCHAR(20)" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN1 AS VARCHAR (20)" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN2 AS VARCHAR (20)" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN3 AS VARCHAR (20)" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN4  AS VARCHAR (20)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCE AS SMALLINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ITEM AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @ENTREGAOBL AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @FECENTREGA AS DATETIME" & vbCrLf
sConsulta = sConsulta & "DECLARE @CANT_REC AS FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @CAT1 AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @CAT2 AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @CAT3 AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @CAT4 AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @CAT5 AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @SEGURIDAD AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @OBS AS VARCHAR (1000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @NUMORDEN AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @FECHAEMISION as DATETIME" & vbCrLf
sConsulta = sConsulta & "DECLARE @SOLICIT int" & vbCrLf
sConsulta = sConsulta & "SET @FECHAEMISION = CONVERT(DATETIME,CONVERT(VARCHAR,DAY(GETDATE())) + '/' + CONVERT(VARCHAR,MONTH(GETDATE())) + '/' + CONVERT(VARCHAR,YEAR(GETDATE())) ,103)" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "/*1- Crea el pedido :*/" & vbCrLf
sConsulta = sConsulta & "SET @ANYO=YEAR(@FECHAEMISION)" & vbCrLf
sConsulta = sConsulta & "SELECT @NUMPEDIDO=MAX(NUM) FROM PEDIDO WHERE ANYO=@ANYO" & vbCrLf
sConsulta = sConsulta & "SET @NUMPEDIDO=ISNULL(@NUMPEDIDO,0)+1" & vbCrLf
sConsulta = sConsulta & "INSERT INTO PEDIDO(ANYO,NUM,EST,TIPO,PER,FECHA) VALUES (@ANYO,@NUMPEDIDO,0,1,@PER,@FECHAEMISION)" & vbCrLf
sConsulta = sConsulta & "--SET @PEDIDO=IDENT_CURRENT('PEDIDO') " & vbCrLf
sConsulta = sConsulta & "SELECT @PEDIDO = MAX(ID) FROM PEDIDO" & vbCrLf
sConsulta = sConsulta & "/*2-Crea las �rdenes de entrega :*/" & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR FOR " & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT C.PROVE, SUM(C.PRECIO_UC*T.CANT_PED*T.FC)  " & vbCrLf
sConsulta = sConsulta & "  FROM CATALOG_LIN C " & vbCrLf
sConsulta = sConsulta & "        INNER JOIN #TMP_PEDIDO T " & vbCrLf
sConsulta = sConsulta & "                ON C.ID =T.CATLIN" & vbCrLf
sConsulta = sConsulta & "GROUP BY C.PROVE" & vbCrLf
sConsulta = sConsulta & "OPEN C " & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C INTO @PROVE,@SUMA" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SELECT @NUMORDEN=MAX(NUM) FROM ORDEN_ENTREGA WHERE ANYO=@ANYO AND PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "   SET @NUMORDEN = ISNULL(@NUMORDEN,0) + 1" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO ORDEN_ENTREGA (PEDIDO,ANYO,PROVE,NUM,EST,TIPO,FECHA,IMPORTE) " & vbCrLf
sConsulta = sConsulta & "        VALUES (@PEDIDO,@ANYO,@PROVE,@NUMORDEN,0,1,@FECHAEMISION,@SUMA)" & vbCrLf
sConsulta = sConsulta & "   SELECT @ORDEN = MAX(ID) FROM ORDEN_ENTREGA" & vbCrLf
sConsulta = sConsulta & "        " & vbCrLf
sConsulta = sConsulta & "   /*3- Crea las l�neas de pedido :*/" & vbCrLf
sConsulta = sConsulta & "   DECLARE D CURSOR FOR " & vbCrLf
sConsulta = sConsulta & "   SELECT DISTINCT C.ART_INT,C.PROVE,T.UP,C.PRECIO_UC*T.FC,T.CANT_PED," & vbCrLf
sConsulta = sConsulta & "   T.DEST,C.ANYO, C.GMN1,C.GMN2,C.GMN3,C.GMN4,C.PROCE,C.ITEM,T.ENTREGA_OBL,T.FECENTREGA,0,C.CAT1,C.CAT2,C.CAT3,C.CAT4,C.CAT5,C.SEGURIDAD," & vbCrLf
sConsulta = sConsulta & "   T.OBS,T.FC,C.SOLICIT" & vbCrLf
sConsulta = sConsulta & "     FROM #TMP_PEDIDO T " & vbCrLf
sConsulta = sConsulta & "             INNER JOIN CATALOG_LIN C " & vbCrLf
sConsulta = sConsulta & "                     ON T.CATLIN=C.ID" & vbCrLf
sConsulta = sConsulta & "             INNER JOIN ORDEN_ENTREGA D " & vbCrLf
sConsulta = sConsulta & "                    on D.PROVE=C.PROVE " & vbCrLf
sConsulta = sConsulta & "   WHERE D.PEDIDO=@PEDIDO " & vbCrLf
sConsulta = sConsulta & "     AND D.PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "       OPEN D" & vbCrLf
sConsulta = sConsulta & "       FETCH NEXT FROM D INTO @ART_INT,@PROVE2,@UP,@PRECIO_UP,@CANT_PED," & vbCrLf
sConsulta = sConsulta & "   @DEST,@ANYOPROCE, @GMN1,@GMN2,@GMN3,@GMN4,@PROCE,@ITEM,@ENTREGAOBL,@FECENTREGA,@CANT_REC,@CAT1,@CAT2,@CAT3,@CAT4,@CAT5,@SEGURIDAD,@OBS, @FC,@SOLICIT" & vbCrLf
sConsulta = sConsulta & "       WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "           BEGIN" & vbCrLf
sConsulta = sConsulta & "                PRINT @CANT_PED" & vbCrLf
sConsulta = sConsulta & "                INSERT INTO LINEAS_PEDIDO(PEDIDO,ORDEN,ART_INT,PROVE,UP,PREC_UP,CANT_PED,DEST,ANYO,GMN1,GMN2,GMN3," & vbCrLf
sConsulta = sConsulta & "                GMN4,PROCE,ITEM,FECEMISION,ENTREGA_OBL,FECENTREGA,EST,CANT_REC,CAT1,CAT2,CAT3,CAT4,CAT5,SEGURIDAD,NIVEL_APROB,OBS,FC,SOLICIT) " & vbCrLf
sConsulta = sConsulta & "                VALUES (@PEDIDO,@ORDEN,@ART_INT,@PROVE2,@UP,@PRECIO_UP,@CANT_PED,@DEST,@ANYOPROCE,@GMN1,@GMN2,@GMN3," & vbCrLf
sConsulta = sConsulta & "                @GMN4,@PROCE,@ITEM,@FECHAEMISION,@ENTREGAOBL,@FECENTREGA,0,@CANT_REC,@CAT1, @CAT2,@CAT3,@CAT4,@CAT5,@SEGURIDAD,-1,@OBS, @FC,@SOLICIT)" & vbCrLf
sConsulta = sConsulta & "       UPDATE PROCE " & vbCrLf
sConsulta = sConsulta & "                   SET PED_APROV=4 " & vbCrLf
sConsulta = sConsulta & "                 WHERE ANYO= @ANYOPROCE" & vbCrLf
sConsulta = sConsulta & "                   AND GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "                   AND COD = @PROCE" & vbCrLf
sConsulta = sConsulta & "                FETCH NEXT FROM D INTO @ART_INT,@PROVE2,@UP,@PRECIO_UP,@CANT_PED,@DEST,@ANYOPROCE, @GMN1,@GMN2,@GMN3,@GMN4,@PROCE,@ITEM,@ENTREGAOBL,@FECENTREGA,@CANT_REC,@CAT1,@CAT2,@CAT3,@CAT4,@CAT5,@SEGURIDAD,@OBS, @FC,@SOLICIT" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "       CLOSE  D" & vbCrLf
sConsulta = sConsulta & "       DEALLOCATE D" & vbCrLf
sConsulta = sConsulta & "   FETCH NEXT FROM C INTO @PROVE,@SUMA" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "CLOSE  C" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C" & vbCrLf
sConsulta = sConsulta & "DROP TABLE #TMP_PEDIDO" & vbCrLf
sConsulta = sConsulta & "RETURN" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_DEVOLVER_ARTICULOS_CATALOGO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_DEVOLVER_ARTICULOS_CATALOGO]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSEP_DEVOLVER_ARTICULOS_CATALOGO(@COD VARCHAR(40) = NULL, @DEN VARCHAR(300) = NULL, @PROVE VARCHAR(300) = NULL," & vbCrLf
sConsulta = sConsulta & "@CATN1 INTEGER = NULL, @CATN2 INTEGER = NULL, @CATN3 INTEGER = NULL, @CATN4 INTEGER = NULL, " & vbCrLf
sConsulta = sConsulta & "@CATN5 INTEGER = NULL, @ID varchar(200),@PRECIOMAX FLOAT = NULL,@PER VARCHAR(50),@MAXLINEAS INTEGER)  AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PARAM NVARCHAR(1000)" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING='" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT TOP ' + CONVERT(VARCHAR,@MAXLINEAS) +'  " & vbCrLf
sConsulta = sConsulta & "CATALOG_LIN.ID,CATALOG_LIN.ANYO,CATALOG_LIN.PROCE,CATALOG_LIN.ITEM," & vbCrLf
sConsulta = sConsulta & "CATALOG_LIN.PROVE,ART_INT + ITEM.ART COD_ITEM," & vbCrLf
sConsulta = sConsulta & "CASE WHEN ART_AUX.DEN IS NULL THEN ART4.DEN ELSE ART_AUX.DEN END + ITEM.DESCR ART_DEN," & vbCrLf
sConsulta = sConsulta & "CATALOG_LIN.DEST,UC,PROVE_ART4.COD_EXT," & vbCrLf
sConsulta = sConsulta & "CANT_ADJ,PRECIO_UC,UP_DEF,FC_DEF," & vbCrLf
sConsulta = sConsulta & "CANT_MIN_DEF,PUB,FECHA_DESPUB,CAT1,CAT2,CAT3,CAT4,CAT5,CATALOG_LIN.SEGURIDAD," & vbCrLf
sConsulta = sConsulta & "PROVE.DEN  AS PROVEDEN," & vbCrLf
sConsulta = sConsulta & "CATALOG_LIN.GMN1,CATALOG_LIN.GMN2,CATALOG_LIN.GMN3,CATALOG_LIN.GMN4,APROB_LIM.MOD_DEST," & vbCrLf
sConsulta = sConsulta & "CATALOG_LIN.ART_INT + ITEM.ART  + CONVERT(varchar,CATALOG_LIN.ID) '" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING= @SQLSTRING + ' " & vbCrLf
sConsulta = sConsulta & " FROM CATALOG_LIN " & vbCrLf
sConsulta = sConsulta & "         INNER JOIN PROVE " & vbCrLf
sConsulta = sConsulta & "            ON CATALOG_LIN.PROVE=PROVE.COD" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "LEFT JOIN ITEM " & vbCrLf
sConsulta = sConsulta & "       ON CATALOG_LIN.GMN1=ITEM.GMN1 " & vbCrLf
sConsulta = sConsulta & "      AND CATALOG_LIN.ANYO=ITEM.ANYO " & vbCrLf
sConsulta = sConsulta & "      AND CATALOG_LIN.PROCE=ITEM.PROCE " & vbCrLf
sConsulta = sConsulta & "      AND CATALOG_LIN.ITEM=ITEM.ID" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN ART4 " & vbCrLf
sConsulta = sConsulta & "       ON CATALOG_LIN.GMN1=ART4.GMN1 " & vbCrLf
sConsulta = sConsulta & "      AND CATALOG_LIN.GMN2=ART4.GMN2 " & vbCrLf
sConsulta = sConsulta & "      AND CATALOG_LIN.GMN3=ART4.GMN3 " & vbCrLf
sConsulta = sConsulta & "      AND CATALOG_LIN.GMN4=ART4.GMN4 " & vbCrLf
sConsulta = sConsulta & "      AND CATALOG_LIN.ART_INT + ITEM.ART=ART4.COD " & vbCrLf
sConsulta = sConsulta & "LEFT JOIN ART4 ART_AUX " & vbCrLf
sConsulta = sConsulta & "       ON ITEM.GMN1=ART_AUX.GMN1 " & vbCrLf
sConsulta = sConsulta & "      AND ITEM.GMN2=ART_AUX.GMN2 " & vbCrLf
sConsulta = sConsulta & "      AND ITEM.GMN3=ART_AUX.GMN3 " & vbCrLf
sConsulta = sConsulta & "      AND ITEM.GMN4=ART_AUX.GMN4 " & vbCrLf
sConsulta = sConsulta & "      AND ITEM.ART=ART_AUX.COD " & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PROVE_ART4 " & vbCrLf
sConsulta = sConsulta & "       ON CATALOG_LIN.GMN1=PROVE_ART4.GMN1 " & vbCrLf
sConsulta = sConsulta & "      AND CATALOG_LIN.GMN2=PROVE_ART4.GMN2 " & vbCrLf
sConsulta = sConsulta & "      AND CATALOG_LIN.GMN3=PROVE_ART4.GMN3 " & vbCrLf
sConsulta = sConsulta & "      AND CATALOG_LIN.GMN4=PROVE_ART4.GMN4 " & vbCrLf
sConsulta = sConsulta & "      AND CASE WHEN CATALOG_LIN.ART_INT IS NULL THEN ITEM.ART ELSE CATALOG_LIN.ART_INT  END =PROVE_ART4.ART " & vbCrLf
sConsulta = sConsulta & "      AND CATALOG_LIN.PROVE=PROVE_ART4.PROVE" & vbCrLf
sConsulta = sConsulta & "'" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING= @SQLSTRING + ' " & vbCrLf
sConsulta = sConsulta & "          INNER JOIN APROB_LIM " & vbCrLf
sConsulta = sConsulta & "                  ON CATALOG_LIN.SEGURIDAD=APROB_LIM.SEGURIDAD " & vbCrLf
sConsulta = sConsulta & "                 AND APROB_LIM.PER=@PER'" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING= @SQLSTRING +'  " & vbCrLf
sConsulta = sConsulta & " WHERE PUB=1" & vbCrLf
sConsulta = sConsulta & "   AND BAJALOG = 0" & vbCrLf
sConsulta = sConsulta & "'" & vbCrLf
sConsulta = sConsulta & "IF @PROVE is not null" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' " & vbCrLf
sConsulta = sConsulta & "           AND PROVE.COD = @PROVE'" & vbCrLf
sConsulta = sConsulta & "IF @COD is not null" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING +'" & vbCrLf
sConsulta = sConsulta & "   AND CATALOG_LIN.ART_INT+ITEM.ART LIKE @COD + ''%'''" & vbCrLf
sConsulta = sConsulta & "IF @DEN is not null" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING = @SQLSTRING + '" & vbCrLf
sConsulta = sConsulta & "   AND ITEM.DESCR + ART4.DEN + ART_AUX.DEN LIKE +  ''%'' + @DEN + ''%'''" & vbCrLf
sConsulta = sConsulta & "IF @CATN1 is not null" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING = @SQLSTRING + ' " & vbCrLf
sConsulta = sConsulta & "   AND CATALOG_LIN.CAT1=@CATN1'" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "IF @CATN2 is not null" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING = @SQLSTRING + ' " & vbCrLf
sConsulta = sConsulta & "   AND CATALOG_LIN.CAT2=@CATN2'" & vbCrLf
sConsulta = sConsulta & "IF @CATN3 is not null" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING = @SQLSTRING + ' " & vbCrLf
sConsulta = sConsulta & "   AND CATALOG_LIN.CAT3=@CATN3'" & vbCrLf
sConsulta = sConsulta & "IF @CATN4 is not null" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING = @SQLSTRING + ' " & vbCrLf
sConsulta = sConsulta & "   AND CATALOG_LIN.CAT4=@CATN4'" & vbCrLf
sConsulta = sConsulta & "IF @CATN5 is not null" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING = @SQLSTRING + '" & vbCrLf
sConsulta = sConsulta & "   AND CATALOG_LIN.CAT5=@CATN5'" & vbCrLf
sConsulta = sConsulta & "IF @PRECIOMAX is not null" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING = @SQLSTRING + '" & vbCrLf
sConsulta = sConsulta & "   AND CATALOG_LIN.PRECIO_UC<=@PRECIOMAX'" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING = @SQLSTRING + '" & vbCrLf
sConsulta = sConsulta & "   AND isnull(catalog_lin.art_int,'''') + isnull(ITEM.ART,'''') +  convert(varchar,isnull(catalog_lin.ID,'''')) >=@ID'" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING = @SQLSTRING + '" & vbCrLf
sConsulta = sConsulta & "ORDER BY CATALOG_LIN.ART_INT + ITEM.ART  + CONVERT(varchar,CATALOG_LIN.ID)'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @PARAM = '@COD VARCHAR(40), @DEN VARCHAR(300), @PROVE VARCHAR(300)," & vbCrLf
sConsulta = sConsulta & "@CATN1 INTEGER, @CATN2 INTEGER, @CATN3 INTEGER, @CATN4 INTEGER, " & vbCrLf
sConsulta = sConsulta & "@CATN5 INTEGER, @PRECIOMAX FLOAT,@PER VARCHAR(50),@ID VARCHAR(200)'" & vbCrLf
sConsulta = sConsulta & "EXECUTE sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "@COD=@COD, " & vbCrLf
sConsulta = sConsulta & "@DEN=@DEN, " & vbCrLf
sConsulta = sConsulta & "@PROVE=@PROVE," & vbCrLf
sConsulta = sConsulta & "@CATN1=@CATN1," & vbCrLf
sConsulta = sConsulta & "@CATN2=@CATN2, " & vbCrLf
sConsulta = sConsulta & "@CATN3=@CATN3," & vbCrLf
sConsulta = sConsulta & "@CATN4=@CATN4," & vbCrLf
sConsulta = sConsulta & "@CATN5=@CATN5, " & vbCrLf
sConsulta = sConsulta & "@PRECIOMAX=@PRECIOMAX, " & vbCrLf
sConsulta = sConsulta & "@PER=@PER," & vbCrLf
sConsulta = sConsulta & "@ID=@ID" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_DEVOLVER_LINEAS_DE_ORDEN]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_DEVOLVER_LINEAS_DE_ORDEN]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSEP_DEVOLVER_LINEAS_DE_ORDEN(@APROBADOR VARCHAR(50), @ORDENID INTEGER, @SOLICIT int = null )  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(1500)" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if @APROBADOR IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT LP.ID AS LINEAID," & vbCrLf
sConsulta = sConsulta & "                LP.ART_INT + ITEM.ART CODART," & vbCrLf
sConsulta = sConsulta & "                ART4.DEN + ITEM.DESCR  + ART_AUX.DEN DENART," & vbCrLf
sConsulta = sConsulta & "                LP.ART_INT + ITEM.ART + ' ' + ART4.DEN + ITEM.DESCR  + ART_AUX.DEN AS ARTICULO," & vbCrLf
sConsulta = sConsulta & "                PA.COD_EXT ART_EXT," & vbCrLf
sConsulta = sConsulta & "                LP.GMN1," & vbCrLf
sConsulta = sConsulta & "                LP.GMN2," & vbCrLf
sConsulta = sConsulta & "                LP.GMN3," & vbCrLf
sConsulta = sConsulta & "                LP.GMN4," & vbCrLf
sConsulta = sConsulta & "       LP.PROVE," & vbCrLf
sConsulta = sConsulta & "                LP.DEST AS DESTINO," & vbCrLf
sConsulta = sConsulta & "                LP.UP AS UNIDAD," & vbCrLf
sConsulta = sConsulta & "                LP.PREC_UP AS PRECIOUNITARIO," & vbCrLf
sConsulta = sConsulta & "                LP.CANT_PED AS CANTIDAD," & vbCrLf
sConsulta = sConsulta & "                LP.CANT_REC AS CANTIDAD_REC," & vbCrLf
sConsulta = sConsulta & "                ITEM_ADJ.CANT_ADJ / LP.FC AS CANTADJUDICADA," & vbCrLf
sConsulta = sConsulta & "                ITEM_ADJ.CANT_PED / LP.FC AS CANTCONSUMIDA," & vbCrLf
sConsulta = sConsulta & "                LP.PREC_UP * LP.CANT_PED IMPORTE," & vbCrLf
sConsulta = sConsulta & "                LP.EST AS APROBADA," & vbCrLf
sConsulta = sConsulta & "                LP.FECEMISION AS FECHA," & vbCrLf
sConsulta = sConsulta & "                GESTIONABLE=CASE WHEN APROB_LIM.PER= @APROBADOR   THEN 1 WHEN (APROB_LIM.PER IS NULL AND SEGURIDAD.APROB_TIPO1=@APROBADOR ) THEN 1  ELSE 0 END," & vbCrLf
sConsulta = sConsulta & "                LP.CAT1," & vbCrLf
sConsulta = sConsulta & "                LP.CAT2," & vbCrLf
sConsulta = sConsulta & "                LP.CAT3," & vbCrLf
sConsulta = sConsulta & "                LP.CAT4," & vbCrLf
sConsulta = sConsulta & "                LP.CAT5," & vbCrLf
sConsulta = sConsulta & "                LP.OBS," & vbCrLf
sConsulta = sConsulta & "                case when c5.cod is null then " & vbCrLf
sConsulta = sConsulta & "                        case when c4.cod is null then " & vbCrLf
sConsulta = sConsulta & "                             case when c3.cod is null then" & vbCrLf
sConsulta = sConsulta & "                                  case when c2.cod is null then" & vbCrLf
sConsulta = sConsulta & "                                       c1.cod" & vbCrLf
sConsulta = sConsulta & "                                  else" & vbCrLf
sConsulta = sConsulta & "                                  c2.cod" & vbCrLf
sConsulta = sConsulta & "                                  end" & vbCrLf
sConsulta = sConsulta & "                             else" & vbCrLf
sConsulta = sConsulta & "                             c3.cod" & vbCrLf
sConsulta = sConsulta & "                             end" & vbCrLf
sConsulta = sConsulta & "                        else" & vbCrLf
sConsulta = sConsulta & "                        c4.cod" & vbCrLf
sConsulta = sConsulta & "                        end" & vbCrLf
sConsulta = sConsulta & "                    else" & vbCrLf
sConsulta = sConsulta & "                    c5.cod" & vbCrLf
sConsulta = sConsulta & "                    end CATEGORIA," & vbCrLf
sConsulta = sConsulta & "                case when c5.cod is null then " & vbCrLf
sConsulta = sConsulta & "                        case when c4.cod is null then " & vbCrLf
sConsulta = sConsulta & "                             case when c3.cod is null then" & vbCrLf
sConsulta = sConsulta & "                                  case when c2.cod is null then" & vbCrLf
sConsulta = sConsulta & "                                       '(' + c1.cod + ') ' + c1.den" & vbCrLf
sConsulta = sConsulta & "                                  else" & vbCrLf
sConsulta = sConsulta & "                                  '(' + c1.cod + ') ' + c1.den + ' - (' + c2.cod + ') ' + c2.den" & vbCrLf
sConsulta = sConsulta & "                                  end" & vbCrLf
sConsulta = sConsulta & "                             else" & vbCrLf
sConsulta = sConsulta & "                             '(' + c1.cod + ') ' + c1.den + ' - (' + c2.cod + ') ' + c2.den + ' - (' + c3.cod + ') ' + c3.den" & vbCrLf
sConsulta = sConsulta & "                             end" & vbCrLf
sConsulta = sConsulta & "                        else" & vbCrLf
sConsulta = sConsulta & "                        '(' + c1.cod + ') ' + c1.den + ' - (' + c2.cod + ') ' + c2.den + ' - (' + c3.cod + ') ' + c3.den + ' - (' + c4.cod + ') ' + c4.den" & vbCrLf
sConsulta = sConsulta & "                        end" & vbCrLf
sConsulta = sConsulta & "                    else" & vbCrLf
sConsulta = sConsulta & "                    '(' + c1.cod + ') ' + c1.den + ' - (' + c2.cod + ') ' + c2.den + ' - (' + c3.cod + ') ' + c3.den + ' - (' + c4.cod + ') ' + c4.den + ' - (' + c5.cod + ') ' + c5.den" & vbCrLf
sConsulta = sConsulta & "                    end CATEGORIARAMA" & vbCrLf
sConsulta = sConsulta & "FROM LINEAS_PEDIDO LP" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN APROB_LIM" & vbCrLf
sConsulta = sConsulta & "            ON LP.APROB_LIM=APROB_LIM.ID" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN SEGURIDAD " & vbCrLf
sConsulta = sConsulta & "            ON LP.SEGURIDAD=SEGURIDAD.ID" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN ITEM " & vbCrLf
sConsulta = sConsulta & "            ON LP.ANYO = ITEM.ANYO" & vbCrLf
sConsulta = sConsulta & "           AND LP.GMN1=ITEM.GMN1 " & vbCrLf
sConsulta = sConsulta & "           AND LP.PROCE=ITEM.PROCE " & vbCrLf
sConsulta = sConsulta & "           AND LP.ITEM=ITEM.ID" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN ART4 " & vbCrLf
sConsulta = sConsulta & "            ON LP.GMN1=ART4.GMN1 " & vbCrLf
sConsulta = sConsulta & "           AND LP.GMN2=ART4.GMN2 " & vbCrLf
sConsulta = sConsulta & "           AND LP.GMN3=ART4.GMN3 " & vbCrLf
sConsulta = sConsulta & "           AND LP.GMN4=ART4.GMN4 " & vbCrLf
sConsulta = sConsulta & "           AND LP.ART_INT=ART4.COD" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN ITEM_ADJ " & vbCrLf
sConsulta = sConsulta & "            ON LP.ITEM=ITEM_ADJ.ITEM " & vbCrLf
sConsulta = sConsulta & "           AND LP.ANYO = ITEM_ADJ.ANYO" & vbCrLf
sConsulta = sConsulta & "           AND LP.GMN1=ITEM_ADJ.GMN1 " & vbCrLf
sConsulta = sConsulta & "           AND LP.PROCE=ITEM_ADJ.PROCE " & vbCrLf
sConsulta = sConsulta & "           AND LP.PROVE=ITEM_ADJ.PROVE" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN ART4 ART_AUX " & vbCrLf
sConsulta = sConsulta & "            ON ITEM.ART=ART_AUX.COD " & vbCrLf
sConsulta = sConsulta & "           AND ART_AUX.GMN1=ITEM.GMN1 " & vbCrLf
sConsulta = sConsulta & "           AND ART_AUX.GMN2=ITEM.GMN2 " & vbCrLf
sConsulta = sConsulta & "           AND ART_AUX.GMN3=ITEM.GMN3 " & vbCrLf
sConsulta = sConsulta & "           AND ART_AUX.GMN4=ITEM.GMN4 " & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN PROVE_ART4 PA" & vbCrLf
sConsulta = sConsulta & "            ON LP.PROVE = PA.PROVE" & vbCrLf
sConsulta = sConsulta & "           AND LP.GMN1 = PA.GMN1" & vbCrLf
sConsulta = sConsulta & "           AND LP.GMN2 = PA.GMN2" & vbCrLf
sConsulta = sConsulta & "           AND LP.GMN3 = PA.GMN3" & vbCrLf
sConsulta = sConsulta & "           AND LP.GMN4 = PA.GMN4" & vbCrLf
sConsulta = sConsulta & "           AND CASE WHEN LP.ART_INT IS NULL THEN ITEM.ART ELSE LP.ART_INT END = PA.ART" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN CATN1 C1" & vbCrLf
sConsulta = sConsulta & "            ON LP.CAT1 = C1.ID" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN CATN2 C2" & vbCrLf
sConsulta = sConsulta & "            ON LP.CAT2 = C2.ID" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN CATN3 C3" & vbCrLf
sConsulta = sConsulta & "            ON LP.CAT3 = C3.ID" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN CATN4 C4" & vbCrLf
sConsulta = sConsulta & "            ON LP.CAT4 = C4.ID" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN CATN5 C5" & vbCrLf
sConsulta = sConsulta & "            ON LP.CAT5 = C5.ID" & vbCrLf
sConsulta = sConsulta & "WHERE LP.ORDEN=@ORDENID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT LP.ID LINEAID," & vbCrLf
sConsulta = sConsulta & "                LP.ART_INT + I.ART CODART," & vbCrLf
sConsulta = sConsulta & "                LP.GMN1," & vbCrLf
sConsulta = sConsulta & "                LP.GMN2," & vbCrLf
sConsulta = sConsulta & "                LP.GMN3," & vbCrLf
sConsulta = sConsulta & "                LP.GMN4," & vbCrLf
sConsulta = sConsulta & "       LP.PROVE," & vbCrLf
sConsulta = sConsulta & "                ART4.DEN + I.DESCR  + ART_AUX.DEN DENART," & vbCrLf
sConsulta = sConsulta & "                PA.COD_EXT ART_EXT," & vbCrLf
sConsulta = sConsulta & "                LP.DEST DESTINO," & vbCrLf
sConsulta = sConsulta & "                D.DEN DESTDEN," & vbCrLf
sConsulta = sConsulta & "                LP.UP UNIDAD," & vbCrLf
sConsulta = sConsulta & "                U.DEN UNIDEN," & vbCrLf
sConsulta = sConsulta & "                LP.PREC_UP PRECIOUNITARIO," & vbCrLf
sConsulta = sConsulta & "                LP.CANT_PED CANTIDAD," & vbCrLf
sConsulta = sConsulta & "                LP.CANT_REC CANT_REC," & vbCrLf
sConsulta = sConsulta & "                IA.CANT_ADJ * LP.FC AS CANTADJUDICADA," & vbCrLf
sConsulta = sConsulta & "                LP.PREC_UP * LP.CANT_PED IMPORTE," & vbCrLf
sConsulta = sConsulta & "                LP.EST AS APROBADA," & vbCrLf
sConsulta = sConsulta & "                LP.FECEMISION FECHA," & vbCrLf
sConsulta = sConsulta & "                LP.FECENTREGA FECENTREGA," & vbCrLf
sConsulta = sConsulta & "                LP.EST,LP.ENTREGA_OBL,LP.FECENTREGAPROVE," & vbCrLf
sConsulta = sConsulta & "                LP.CAT1," & vbCrLf
sConsulta = sConsulta & "                LP.CAT2," & vbCrLf
sConsulta = sConsulta & "                LP.CAT3," & vbCrLf
sConsulta = sConsulta & "                LP.CAT4," & vbCrLf
sConsulta = sConsulta & "                LP.CAT5," & vbCrLf
sConsulta = sConsulta & "                LP.OBS," & vbCrLf
sConsulta = sConsulta & "                case when c5.cod is null then " & vbCrLf
sConsulta = sConsulta & "                        case when c4.cod is null then " & vbCrLf
sConsulta = sConsulta & "                             case when c3.cod is null then" & vbCrLf
sConsulta = sConsulta & "                                  case when c2.cod is null then" & vbCrLf
sConsulta = sConsulta & "                                       c1.cod" & vbCrLf
sConsulta = sConsulta & "                                  else" & vbCrLf
sConsulta = sConsulta & "                                  c2.cod" & vbCrLf
sConsulta = sConsulta & "                                  end" & vbCrLf
sConsulta = sConsulta & "                             else" & vbCrLf
sConsulta = sConsulta & "                             c3.cod" & vbCrLf
sConsulta = sConsulta & "                             end" & vbCrLf
sConsulta = sConsulta & "                        else" & vbCrLf
sConsulta = sConsulta & "                        c4.cod" & vbCrLf
sConsulta = sConsulta & "                        end" & vbCrLf
sConsulta = sConsulta & "                    else" & vbCrLf
sConsulta = sConsulta & "                    c5.cod" & vbCrLf
sConsulta = sConsulta & "                    end CATEGORIA," & vbCrLf
sConsulta = sConsulta & "                case when c5.cod is null then " & vbCrLf
sConsulta = sConsulta & "                        case when c4.cod is null then " & vbCrLf
sConsulta = sConsulta & "                             case when c3.cod is null then" & vbCrLf
sConsulta = sConsulta & "                                  case when c2.cod is null then" & vbCrLf
sConsulta = sConsulta & "                                       '(' + c1.cod + ') ' + c1.den" & vbCrLf
sConsulta = sConsulta & "                                  else" & vbCrLf
sConsulta = sConsulta & "                                  '(' + c1.cod + ') ' + c1.den + ' - (' + c2.cod + ') ' + c2.den" & vbCrLf
sConsulta = sConsulta & "                                  end" & vbCrLf
sConsulta = sConsulta & "                             else" & vbCrLf
sConsulta = sConsulta & "                             '(' + c1.cod + ') ' + c1.den + ' - (' + c2.cod + ') ' + c2.den + ' - (' + c3.cod + ') ' + c3.den" & vbCrLf
sConsulta = sConsulta & "                             end" & vbCrLf
sConsulta = sConsulta & "                        else" & vbCrLf
sConsulta = sConsulta & "                        '(' + c1.cod + ') ' + c1.den + ' - (' + c2.cod + ') ' + c2.den + ' - (' + c3.cod + ') ' + c3.den + ' - (' + c4.cod + ') ' + c4.den" & vbCrLf
sConsulta = sConsulta & "                        end" & vbCrLf
sConsulta = sConsulta & "                    else" & vbCrLf
sConsulta = sConsulta & "                    '(' + c1.cod + ') ' + c1.den + ' - (' + c2.cod + ') ' + c2.den + ' - (' + c3.cod + ') ' + c3.den + ' - (' + c4.cod + ') ' + c4.den + ' - (' + c5.cod + ') ' + c5.den" & vbCrLf
sConsulta = sConsulta & "                    end CATEGORIARAMA," & vbCrLf
sConsulta = sConsulta & "               " & vbCrLf
sConsulta = sConsulta & "                        " & vbCrLf
sConsulta = sConsulta & "                C1.DEN CAT1DEN," & vbCrLf
sConsulta = sConsulta & "                C2.DEN CAT2DEN," & vbCrLf
sConsulta = sConsulta & "                C3.DEN CAT3DEN," & vbCrLf
sConsulta = sConsulta & "                C4.DEN CAT4DEN," & vbCrLf
sConsulta = sConsulta & "                C5.DEN CAT5DEN," & vbCrLf
sConsulta = sConsulta & "                LE.COMENT" & vbCrLf
sConsulta = sConsulta & " FROM LINEAS_PEDIDO LP" & vbCrLf
sConsulta = sConsulta & "     INNER JOIN DEST D" & vbCrLf
sConsulta = sConsulta & "             ON LP.DEST = D.COD" & vbCrLf
sConsulta = sConsulta & "     INNER JOIN UNI U" & vbCrLf
sConsulta = sConsulta & "             ON LP.UP = U.COD" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN ITEM I " & vbCrLf
sConsulta = sConsulta & "             ON LP.ANYO = I.ANYO" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN1=I.GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND LP.PROCE=I.PROCE " & vbCrLf
sConsulta = sConsulta & "            AND LP.ITEM=I.ID" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN PROVE_ART4 PA" & vbCrLf
sConsulta = sConsulta & "             ON LP.PROVE = PA.PROVE" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN1 = PA.GMN1" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN2 = PA.GMN2" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN3 = PA.GMN3" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN4 = PA.GMN4" & vbCrLf
sConsulta = sConsulta & "            AND CASE WHEN LP.ART_INT IS NULL THEN I.ART ELSE LP.ART_INT END = PA.ART" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN ART4 " & vbCrLf
sConsulta = sConsulta & "             ON LP.GMN1=ART4.GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN2=ART4.GMN2 " & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN3=ART4.GMN3 " & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN4=ART4.GMN4 " & vbCrLf
sConsulta = sConsulta & "            AND LP.ART_INT=ART4.COD" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN ITEM_ADJ IA " & vbCrLf
sConsulta = sConsulta & "             ON LP.ITEM = IA.ITEM " & vbCrLf
sConsulta = sConsulta & "            AND LP.ANYO = IA.ANYO" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN1 = IA.GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND LP.PROCE= IA.PROCE " & vbCrLf
sConsulta = sConsulta & "            AND LP.PROVE= IA.PROVE" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN ART4 ART_AUX " & vbCrLf
sConsulta = sConsulta & "             ON ART_AUX.COD=I.ART " & vbCrLf
sConsulta = sConsulta & "            AND ART_AUX.GMN1=I.GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND ART_AUX.GMN2=I.GMN2 " & vbCrLf
sConsulta = sConsulta & "            AND ART_AUX.GMN3=I.GMN3 " & vbCrLf
sConsulta = sConsulta & "            AND ART_AUX.GMN4=I.GMN4" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN CATN1 C1" & vbCrLf
sConsulta = sConsulta & "             ON LP.CAT1 = C1.ID" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN CATN2 C2" & vbCrLf
sConsulta = sConsulta & "             ON LP.CAT2 = C2.ID" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN CATN3 C3" & vbCrLf
sConsulta = sConsulta & "             ON LP.CAT3 = C3.ID" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN CATN4 C4" & vbCrLf
sConsulta = sConsulta & "             ON LP.CAT4 = C4.ID" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN CATN5 C5" & vbCrLf
sConsulta = sConsulta & "             ON LP.CAT5 = C5.ID" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN LINEAS_EST LE" & vbCrLf
sConsulta = sConsulta & "             ON LP.LINEAS_EST = LE.ID" & vbCrLf
sConsulta = sConsulta & " WHERE LP.ORDEN=@ORDENID" & vbCrLf
sConsulta = sConsulta & "   AND ISNULL(LP.SOLICIT,-1) = ISNULL(@SOLICIT,-1)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_DEVOLVER_ORDEN]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_DEVOLVER_ORDEN]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSEP_DEVOLVER_ORDEN(@ID INTEGER)  AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SELECT ORDEN_ENTREGA.ID AS ORDENID,ORDEN_ENTREGA.ANYO AS ANYO" & vbCrLf
sConsulta = sConsulta & ",PEDIDO.NUM AS NUMPEDIDO" & vbCrLf
sConsulta = sConsulta & ",ORDEN_ENTREGA.NUM AS NUMORDEN" & vbCrLf
sConsulta = sConsulta & ",ORDEN_ENTREGA.PROVE + ' ' + PROVE.DEN AS PROVE" & vbCrLf
sConsulta = sConsulta & ",ORDEN_ENTREGA.PROVE PROVECOD" & vbCrLf
sConsulta = sConsulta & ",PROVE.DEN PROVEDEN" & vbCrLf
sConsulta = sConsulta & ",ORDEN_ENTREGA.FECHA" & vbCrLf
sConsulta = sConsulta & ",ORDEN_ENTREGA.EST" & vbCrLf
sConsulta = sConsulta & ",ORDEN_ENTREGA.IMPORTE " & vbCrLf
sConsulta = sConsulta & ",PER.APE + ',' + PER.NOM AS APROVISIONADOR" & vbCrLf
sConsulta = sConsulta & "FROM ORDEN_ENTREGA  " & vbCrLf
sConsulta = sConsulta & "INNER JOIN PEDIDO ON PEDIDO.ID=ORDEN_ENTREGA.PEDIDO AND ORDEN_ENTREGA.ID=@ID" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PER ON PER.COD=PEDIDO.PER" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PROVE ON PROVE.COD=ORDEN_ENTREGA.PROVE AND ORDEN_ENTREGA.ID=@ID" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GMN1_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[GMN1_COD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE GMN1_COD (@OLD VARCHAR(50),@NEW VARCHAR(50))  AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT ON" & vbCrLf
sConsulta = sConsulta & "BEGIN TRANSACTION" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR FOR SELECT COD FROM GMN1 WHERE COD=@OLD" & vbCrLf
sConsulta = sConsulta & "OPEN C" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C INTO @VAR_COD" & vbCrLf
sConsulta = sConsulta & "IF @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART1_ADJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART2_ADJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART3_ADJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ADJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE BLOQUEO_PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE DETALLE_PUJAS NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ADJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ADJREU NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ESP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OBJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OFE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OFE_WEB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRES NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESCON NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESPROY NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESCON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESCON4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFEMIN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_GRUPO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_ESP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_WEB_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_WEB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PER NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_PET NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_POT NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVENOTIF NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE REU_PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ESP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4_ACT NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATALOG_LIN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_PEDIDO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART_UP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_DEF NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO_DEF NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO_ESP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_GR_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_GR_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ITEM_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ITEM_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GRUPO_OFE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GRUPO_OFE_REU NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USAR_GR_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_CONTR NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN1 SET COD=@NEW WHERE COD=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART1_ADJ SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART2_ADJ SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART3_ADJ SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4_ADJ SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE BLOQUEO_PROCE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE COM_GMN1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE COM_GMN2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE COM_GMN3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE COM_GMN4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE DETALLE_PUJAS SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_ADJ SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_ADJREU SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_ESP SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_OBJ SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_OFE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_OFE_WEB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_PRES SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_PRESCON SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_PRESCON3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_PRESCON4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_PRESPROY SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_UON1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_UON2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_UON3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFEMIN SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES_MAT1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES_MAT2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES_MAT3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES_MAT4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_ESP SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE_ADJUN SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE_WEB_ADJUN SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE_WEB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PER SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE_PET SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE_POT SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_GMN1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_GMN2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_GMN3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_GMN4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVENOTIF SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE REU_PROCE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_PROCE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_PROVE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_GRUPO SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_UON1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_UON2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_UON3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_PRESCON3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_PRESCON4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_PROVE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4_ESP  SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN4_ACT SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART4_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN4_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4_ATRIB  SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CATALOG_LIN  SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE LINEAS_PEDIDO  SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART_UP  SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_DEF SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GRUPO SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GRUPO_DEF SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GRUPO_ESP SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PRES1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PRES2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PRES3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PRES4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_PRES1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_PRES2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_PRES3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_PRES4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_UON1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_UON2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_UON3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_UON1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_UON2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_UON3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_GR_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_GR_ADJUN SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_ITEM_ADJUN SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_ITEM_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ALL SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ALL_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ALL_PROVE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_GRUPO SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_GRUPO_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_GRUPO_PROVE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ITEM SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ITEM_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ITEM_PROVE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_PROCE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_PROCE_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_PROCE_PROVE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GRUPO_OFE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GRUPO_OFE_REU SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE USAR_GR_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_CONTR SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART1_ADJ CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART2_ADJ CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART3_ADJ CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ADJ CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE BLOQUEO_PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE DETALLE_PUJAS CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ADJ CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ADJREU CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ESP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OBJ CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OFE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OFE_WEB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRES CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESCON CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESCON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESCON4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESPROY CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFEMIN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_ESP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_WEB_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_WEB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PER CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_PET CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_POT CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVENOTIF CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE REU_PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_GRUPO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ESP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4_ACT CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATALOG_LIN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_PEDIDO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART_UP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_DEF CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO_DEF CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO_ESP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_GR_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_GR_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ITEM_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ITEM_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GRUPO_OFE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GRUPO_OFE_REU CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USAR_GR_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_CONTR CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close C" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "COMMIT TRANSACTION" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT OFF" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PROCE_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[PROCE_COD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE PROCE_COD (@OLD SMALLINT,@NEW SMALLINT,@ANYO INT,@GMN1 VARCHAR (50))  AS " & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_COD SMALLINT " & vbCrLf
sConsulta = sConsulta & "DECLARE @AN AS INT " & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN AS VARCHAR(50) " & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT ON " & vbCrLf
sConsulta = sConsulta & "BEGIN TRANSACTION " & vbCrLf
sConsulta = sConsulta & "BEGIN " & vbCrLf
sConsulta = sConsulta & "DECLARE D CURSOR FOR SELECT ANYO,GMN1,COD FROM PROCE WHERE ANYO=@ANYO AND COD=@OLD AND GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "OPEN D " & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM D INTO @AN,@GMN,@VAR_COD " & vbCrLf
sConsulta = sConsulta & "IF @@FETCH_STATUS=0 " & vbCrLf
sConsulta = sConsulta & "BEGIN " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART1_ADJ NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART2_ADJ NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART3_ADJ NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ADJ NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ADJ NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ADJREU NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ESP NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OBJ NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OFE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OFE_WEB NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRES NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESCON NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESCON3 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESCON4 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESPROY NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON1 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON2 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON3 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFEMIN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE DETALLE_PUJAS NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVENOTIF NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATALOG_LIN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_PEDIDO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_ESP NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_ADJUN NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_WEB_ADJUN NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_WEB NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PER NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_PET NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_POT NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE REU_PROCE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE BLOQUEO_PROCE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES1 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES2 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES3 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES4 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON1 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON2 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON3 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO_ESP NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO_DEF NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES1 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES2 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES3 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES4 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON1 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON2 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON3 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_ATRIB NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_DEF NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL_ATRIB NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL_PROVE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_ATRIB NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_PROVE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_ATRIB NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_PROVE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_ATRIB NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_PROVE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON1 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON2 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON3 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON4 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY1 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY2 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY3 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY4 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON1 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON2 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON3 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON3 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON4 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PROVE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_GRUPO NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROCE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROVE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GRUPO_OFE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GRUPO_OFE_REU NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_GR_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ITEM_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_GR_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ITEM_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USAR_GR_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_CONTR NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE ART1_ADJ SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ART2_ADJ SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ART3_ADJ SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ART4_ADJ SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_ADJ SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_ADJREU SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_ESP SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_OBJ SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_OFE SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_OFE_WEB SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_PRES SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_UON1 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_UON2 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_UON3 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_PRESCON SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_PRESCON3 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_PRESCON4 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_PRESPROY SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE OFEMIN SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE DETALLE_PUJAS SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVENOTIF SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CATALOG_LIN SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE LINEAS_PEDIDO SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_ESP SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE_ADJUN SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE_WEB_ADJUN SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE_WEB SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PER SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE_PET SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE_POT SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE REU_PROCE SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_PRES1 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_PRES2 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_PRES3 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_PRES4 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_UON1 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_UON2 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_UON3 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GRUPO_ESP SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GRUPO_DEF SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GRUPO SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PRES1 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PRES2 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PRES3 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PRES4 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_UON1 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_UON2 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_UON3 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_ATRIB SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_DEF SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ALL SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ALL_ATRIB SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ALL_PROVE SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_PROCE SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_PROCE_ATRIB SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_PROCE_PROVE SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_GRUPO SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_GRUPO_ATRIB SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_GRUPO_PROVE SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ITEM SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ITEM_ATRIB SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ITEM_PROVE SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON1 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON2 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON3 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON4 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY1 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY2 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY3 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY4 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_1 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_2 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_3 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_4 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_1 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_2 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_3 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_4 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_UON1 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_UON2 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD  " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_UON3 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_PRESCON3 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_PRESCON4 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_PROVE SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_GRUPO SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_PROVE SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_PROCE SET COD=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE GRUPO_OFE SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GRUPO_OFE_REU SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_ATRIB SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_GR_ATRIB SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_ITEM_ATRIB SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_GR_ADJUN SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_ITEM_ADJUN SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GRUPO SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET COD=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE BLOQUEO_PROCE SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE USAR_GR_ATRIB SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_CONTR SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART1_ADJ CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART2_ADJ CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART3_ADJ CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ADJ CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ADJ CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESCON CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESCON3 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESCON4 CHECK CONSTRAINT ALL  " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESPROY CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ADJREU CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ESP CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OBJ CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OFE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OFE_WEB CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRES CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON1 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON2 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON3 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFEMIN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE DETALLE_PUJAS CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVENOTIF CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATALOG_LIN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_PEDIDO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_ESP CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_ADJUN CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_WEB_ADJUN CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_WEB CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PER CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_PET CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_POT CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE REU_PROCE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE BLOQUEO_PROCE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES1 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES2 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES3 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES4 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON1 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON2 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON3 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO_ESP CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO_DEF CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES1 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES2 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES3 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES4 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON1 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON2 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON3 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_ATRIB CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_DEF CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL_ATRIB CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL_PROVE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_ATRIB CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_PROVE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_ATRIB CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_PROVE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_ATRIB CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_PROVE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON1 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON2 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON3 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON4 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY1 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY2 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY3 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY4 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON1 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON2 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON3 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON3 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON4 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PROVE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_GRUPO CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROCE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROVE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GRUPO_OFE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GRUPO_OFE_REU CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_GR_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ITEM_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_GR_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ITEM_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USAR_GR_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_CONTR CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "End " & vbCrLf
sConsulta = sConsulta & "Close D " & vbCrLf
sConsulta = sConsulta & "DEALLOCATE D " & vbCrLf
sConsulta = sConsulta & "End " & vbCrLf
sConsulta = sConsulta & "COMMIT TRANSACTION" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT OFF" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PROVE_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[PROVE_COD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE PROVE_COD (@OLD VARCHAR(50),@NEW VARCHAR(50))  AS " & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_COD VARCHAR(50) " & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT ON " & vbCrLf
sConsulta = sConsulta & "BEGIN TRANSACTION " & vbCrLf
sConsulta = sConsulta & "BEGIN " & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR FOR SELECT COD FROM PROVE WHERE COD=@OLD " & vbCrLf
sConsulta = sConsulta & "OPEN C " & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C INTO @VAR_COD " & vbCrLf
sConsulta = sConsulta & "IF @@FETCH_STATUS=0 " & vbCrLf
sConsulta = sConsulta & "BEGIN " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CON NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART1_ADJ NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART2_ADJ NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART3_ADJ NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ADJ NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ADJ NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ADJREU NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OFE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OFE_WEB NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE DETALLE_PUJAS NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFEMIN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_ADJUN NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_WEB_ADJUN NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_WEB NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_PET NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_POT NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_EQP NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN1 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN2 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN3 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN4 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVENOTIF NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ATRIB NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATALOG_LIN NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ORDEN_ENTREGA NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_PEDIDO NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART_UP NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ESP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ESP_PORTAL NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ITEM_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_GR_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ITEM_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_GR_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GRUPO_OFE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GRUPO_OFE_REU NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_CONTR NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE SET COD=@NEW WHERE COD=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE CON SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_PROVE SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART1_ADJ SET CODPROVE=@NEW WHERE CODPROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ART2_ADJ SET CODPROVE=@NEW WHERE CODPROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ART3_ADJ SET CODPROVE=@NEW WHERE CODPROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ART4_ADJ SET CODPROVE=@NEW WHERE CODPROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM SET PROVEACT=@NEW WHERE PROVEACT=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_ADJ SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_ADJREU SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_OFE SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_OFE_WEB SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE DETALLE_PUJAS SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFEMIN SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE_ADJUN SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE_WEB_ADJUN SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE_WEB SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE_PET SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE_POT SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_EQP SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_GMN1 SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_GMN2 SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_GMN3 SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_GMN4 SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART1 SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART2 SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART3 SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART4 SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVENOTIF SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE USU SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART4_ATRIB SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CATALOG_LIN SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ORDEN_ENTREGA SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE LINEAS_PEDIDO SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART_UP SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ESP SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ESP_PORTAL SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "-- 2.10" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET PROVEACT=@NEW WHERE PROVEACT=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GRUPO SET PROVEACT=@NEW WHERE PROVEACT=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ALL_PROVE SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_GRUPO_PROVE SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ITEM_PROVE SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_PROCE_PROVE SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_ATRIB SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_ITEM_ATRIB SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_GR_ATRIB SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_ITEM_ADJUN SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_GR_ADJUN SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GRUPO_OFE SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GRUPO_OFE_REU SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_PROVE SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_PROVE SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_CONTR SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CON CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PROVE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART1_ADJ CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART2_ADJ CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART3_ADJ CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ADJ CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ADJ CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ADJREU CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OFE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OFE_WEB CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_ADJUN CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_WEB_ADJUN CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_WEB CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_PET CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_POT CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_EQP CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN1 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN2 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN3 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN4 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE DETALLE_PUJAS CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFEMIN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ATRIB CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATALOG_LIN CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ORDEN_ENTREGA CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_PEDIDO CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART_UP CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ESP CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ESP_PORTAL CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "-- 2.10" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ITEM_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_GR_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ITEM_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_GR_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GRUPO_OFE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GRUPO_OFE_REU CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_CONTR CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "End " & vbCrLf
sConsulta = sConsulta & "Close C " & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C " & vbCrLf
sConsulta = sConsulta & "End " & vbCrLf
sConsulta = sConsulta & "COMMIT TRANSACTION" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT OFF" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USU_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[USU_COD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE USU_COD (@OLD VARCHAR(50),@NEW VARCHAR(50),@PWD VARCHAR(512))  AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT ON" & vbCrLf
sConsulta = sConsulta & "BEGIN TRANSACTION" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR FOR SELECT COD FROM USU WHERE COD=@OLD" & vbCrLf
sConsulta = sConsulta & "OPEN C" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C INTO @VAR_COD" & vbCrLf
sConsulta = sConsulta & "IF @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU_ACC NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU_LIS NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LOG NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_CONTR NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE USU SET COD=@NEW,PWD=@PWD WHERE COD=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE USU_ACC SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE USU_LIS SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET USUVAL=@NEW WHERE USUVAL=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET USUVALSELPROVE=@NEW WHERE USUVALSELPROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET USUVALCIERRE=@NEW WHERE USUVALCIERRE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET USUAPER=@NEW WHERE USUAPER=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_PROCE SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_PROCE_PROVE SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_PROCE_ATRIB SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_GRUPO SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_GRUPO_PROVE SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_GRUPO_ATRIB SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ITEM SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ITEM_PROVE SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ITEM_ATRIB SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_CONTR SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU_ACC CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU_LIS CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LOG CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_CONTR CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close C" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT OFF" & vbCrLf
sConsulta = sConsulta & "COMMIT TRANSACTION" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PER_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[PER_COD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE PER_COD (@OLD VARCHAR(50),@NEW VARCHAR(50))  AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @EQP VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT ON" & vbCrLf
sConsulta = sConsulta & "BEGIN TRANSACTION" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE D CURSOR FOR SELECT COD,EQP FROM PER WHERE COD=@OLD" & vbCrLf
sConsulta = sConsulta & "OPEN D" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM D INTO @VAR_COD,@EQP" & vbCrLf
sConsulta = sConsulta & "IF @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "/*Ejecuta el store procedure COM_COD*/" & vbCrLf
sConsulta = sConsulta & "IF NOT (@EQP) IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "EXECUTE COM_COD @OLD,@NEW,@EQP" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PER NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FIJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PER NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE APROB_LIM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CAT_PER NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_EST NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE NOTIF_TIPO1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE NOTIF_TIPO2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ORDEN_EST NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PEDIDO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE SEGURIDAD NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE SOLICIT NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE SOLICIT_EST NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE SOLICIT_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE SOLICIT_ASIG_COMP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE SOLICIT_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "UPDATE PER SET COD=@NEW WHERE COD=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE USU SET PER=@NEW WHERE PER=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE FIJ SET PER=@NEW WHERE PER=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PER SET PER=@NEW WHERE PER=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE APROB_LIM SET PER=@NEW WHERE PER=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CAT_PER SET PER=@NEW WHERE PER=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE LINEAS_EST SET PER=@NEW WHERE PER=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE NOTIF_TIPO1 SET NOTIF=@NEW WHERE NOTIF=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE NOTIF_TIPO2 SET PER=@NEW WHERE PER=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ORDEN_EST SET PER=@NEW WHERE PER=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PEDIDO SET PER=@NEW WHERE PER=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE SEGURIDAD SET APROB_TIPO1=@NEW WHERE APROB_TIPO1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE SOLICIT SET PER=@NEW WHERE PER=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE SOLICIT SET COMPRADOR=@NEW WHERE COMPRADOR=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE SOLICIT_EST SET PER=@NEW WHERE PER=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE SOLICIT_ADJUN SET PER=@NEW WHERE PER=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE SOLICIT_ASIG_COMP SET PER=@NEW WHERE PER=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE SOLICIT_ASIG_COMP SET COM=@NEW WHERE COM=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE SOLICIT_ADJUN SET PER=@NEW WHERE PER=@OLD" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PER CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FIJ CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PER CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE APROB_LIM CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CAT_PER CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_EST CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE NOTIF_TIPO1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE NOTIF_TIPO2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ORDEN_EST CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PEDIDO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE SEGURIDAD CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE SOLICIT CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE SOLICIT_EST CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE SOLICIT_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE SOLICIT_ASIG_COMP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE SOLICIT_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close D" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE D" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT OFF" & vbCrLf
sConsulta = sConsulta & "COMMIT TRANSACTION" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_DEVOLVER_OFERTAS_MINIMAS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_DEVOLVER_OFERTAS_MINIMAS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE  SP_DEVOLVER_OFERTAS_MINIMAS  @ANYO INT,@GMN1 VARCHAR(50),@PROCE INT, @PROVE varchar(50), @CODMON varchar(50), @FECLIMITE DATETIME OUTPUT , @VERPROVE tinyint OUTPUT, @VERDETALLE tinyint OUTPUT AS " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "set NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @EQUIV FLOAT " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @EQUIV= MON.EQUIV  FROM MON WHERE MON.COD=@CODMON " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FECLIMITE = FECLIMOFE , @VERPROVE = SUBASTAPROVE, @VERDETALLE = SUBASTAPUJAS" & vbCrLf
sConsulta = sConsulta & "  FROM PROCE P" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN PROCE_DEF PD" & vbCrLf
sConsulta = sConsulta & "            ON P.ANYO = PD.ANYO" & vbCrLf
sConsulta = sConsulta & "           AND P.GMN1 = PD.GMN1" & vbCrLf
sConsulta = sConsulta & "           AND P.COD = PD.PROCE" & vbCrLf
sConsulta = sConsulta & " WHERE P.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "   AND P.GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "   AND P.COD = @PROCE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT O.ITEM, I.GRUPO, O.PROVE MINPROVE, P2.DEN MINPROVEDEN, I.ART, I.DESCR + A.DEN AS DESCR, " & vbCrLf
sConsulta = sConsulta & "       O.PRECIO*@EQUIV MINPRECIO, CASE WHEN @PROVE = O.PROVE THEN 1 ELSE 0 END PROPIA" & vbCrLf
sConsulta = sConsulta & "  FROM OFEMIN O " & vbCrLf
sConsulta = sConsulta & "     INNER JOIN PROVE P2 " & vbCrLf
sConsulta = sConsulta & "             ON O.PROVE = P2.COD" & vbCrLf
sConsulta = sConsulta & "     INNER JOIN (ITEM I" & vbCrLf
sConsulta = sConsulta & "                 LEFT JOIN ART4 A" & vbCrLf
sConsulta = sConsulta & "                        ON I.ART = A.COD " & vbCrLf
sConsulta = sConsulta & "                       AND I.GMN4 = A.GMN4 " & vbCrLf
sConsulta = sConsulta & "                       AND I.GMN3 = A.GMN3 " & vbCrLf
sConsulta = sConsulta & "                       AND I.GMN2 = A.GMN2" & vbCrLf
sConsulta = sConsulta & "                       AND I.GMN1 = A.GMN1)" & vbCrLf
sConsulta = sConsulta & "             ON O.ANYO = I.ANYO" & vbCrLf
sConsulta = sConsulta & "            AND O.GMN1 = I.GMN1" & vbCrLf
sConsulta = sConsulta & "            AND O.PROCE = I.PROCE" & vbCrLf
sConsulta = sConsulta & "            AND O.ITEM = I.ID" & vbCrLf
sConsulta = sConsulta & " WHERE O.ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "   AND O.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "   AND O.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_DEVOLVER_PROCESO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_DEVOLVER_PROCESO]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_DEVOLVER_PROCESO (@ANYO INTEGER,@GMN1 VARCHAR(20),@PROCE INTEGER ,@ACTIVA SMALLINT OUTPUT) AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @SUBASTA AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @FECLIMOFE AS DATETIME" & vbCrLf
sConsulta = sConsulta & "DECLARE @HAYESP  AS BIT" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "  SELECT @SUBASTA=PD.SUBASTA , @FECLIMOFE =FECLIMOFE " & vbCrLf
sConsulta = sConsulta & "    FROM PROCE " & vbCrLf
sConsulta = sConsulta & "        INNER JOIN PROCE_DEF PD" & vbCrLf
sConsulta = sConsulta & "                ON PROCE.ANYO = PD.ANYO" & vbCrLf
sConsulta = sConsulta & "               AND PROCE.GMN1 = PD.GMN1" & vbCrLf
sConsulta = sConsulta & "               AND PROCE.COD = PD.PROCE" & vbCrLf
sConsulta = sConsulta & "   WHERE PROCE.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "     AND PROCE.COD=@PROCE " & vbCrLf
sConsulta = sConsulta & "     AND PROCE.ANYO=@ANYO" & vbCrLf
sConsulta = sConsulta & "  " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  SELECT @HAYESP = CASE WHEN ESP IS NULL OR ESP='' THEN 0 ELSE 1 END " & vbCrLf
sConsulta = sConsulta & "    FROM PROCE " & vbCrLf
sConsulta = sConsulta & "   WHERE ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "     AND COD=@PROCE " & vbCrLf
sConsulta = sConsulta & "     AND GMN1=@GMN1" & vbCrLf
sConsulta = sConsulta & "  IF @HAYESP = 0" & vbCrLf
sConsulta = sConsulta & "     BEGIN   " & vbCrLf
sConsulta = sConsulta & "       if (select count(id) " & vbCrLf
sConsulta = sConsulta & "             from proce_esp " & vbCrLf
sConsulta = sConsulta & "            where aNYo=@ANYO " & vbCrLf
sConsulta = sConsulta & "              and gmn1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "              and proce=@PROCE)>0" & vbCrLf
sConsulta = sConsulta & "         select @HAYESP = 1" & vbCrLf
sConsulta = sConsulta & "     END" & vbCrLf
sConsulta = sConsulta & "  IF @SUBASTA= 1 AND @FECLIMOFE <=GETDATE() " & vbCrLf
sConsulta = sConsulta & "      SET @ACTIVA=2" & vbCrLf
sConsulta = sConsulta & "  ELSE " & vbCrLf
sConsulta = sConsulta & "    IF @SUBASTA=1 " & vbCrLf
sConsulta = sConsulta & "      SET @ACTIVA=1" & vbCrLf
sConsulta = sConsulta & "    ELSE" & vbCrLf
sConsulta = sConsulta & "      SET @ACTIVA=0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  SELECT PROCE.ANYO,PROCE.GMN1,PROCE.COD,PROCE.DEN,PROCE.GMN2,PROCE.GMN3,PROCE.GMN4,PROCE.ADJDIR,PROCE.PRES,PROCE.EST," & vbCrLf
sConsulta = sConsulta & "         PROCE.FECAPE,PROCE.FECLIMOFE,PROCE.FECNEC,PROCE.FECPRES,PROCE.FECENVPET,PROCE.FECPROXREU,PROCE.FECULTREU," & vbCrLf
sConsulta = sConsulta & "         PROCE.MON,MON.DEN DENMON, PROCE.CAMBIO,PROCE.SOLICITUD,PROCE.ESP,PROCE.DEST,PROCE.PAG, PAG.DEN DENPAG, PROCE.FECINI,PROCE.FECFIN," & vbCrLf
sConsulta = sConsulta & "         PROCE.FECINISUB,PROCE.COM, PD.SUBASTA,PD.SUBASTAPROVE,PD.SUBASTAPUJAS,HAYESP = @HAYESP " & vbCrLf
sConsulta = sConsulta & "    FROM PROCE " & vbCrLf
sConsulta = sConsulta & "          INNER JOIN PROCE_DEF PD" & vbCrLf
sConsulta = sConsulta & "                  ON PROCE.ANYO = PD.ANYO" & vbCrLf
sConsulta = sConsulta & "                 AND PROCE.GMN1 = PD.GMN1" & vbCrLf
sConsulta = sConsulta & "                 AND PROCE.COD = PD.PROCE" & vbCrLf
sConsulta = sConsulta & "           LEFT JOIN PAG" & vbCrLf
sConsulta = sConsulta & "                  ON PROCE.PAG = PAG.COD" & vbCrLf
sConsulta = sConsulta & "           LEFT JOIN MON" & vbCrLf
sConsulta = sConsulta & "                  ON PROCE.MON = MON.COD" & vbCrLf
sConsulta = sConsulta & "   WHERE PROCE.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "     AND PROCE.COD=@PROCE " & vbCrLf
sConsulta = sConsulta & "     AND PROCE.ANYO=@ANYO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "RETURN" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta



End Function



Private Function V_2_10_5_Storeds_v4()
Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_VALIDAR_OFERTA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_VALIDAR_OFERTA]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "CREATE PROCEDURE SP_VALIDAR_OFERTA @ANYO SMALLINT,@GMN1 VARCHAR(50), @PROCE INT,@PROVE VARCHAR(50), @OFE INT , @ERROR TINYINT OUTPUT , @NEWOFE INT =NULL OUTPUT AS" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @OFEATRIB BIT" & vbCrLf
sConsulta = sConsulta & "DECLARE @OFEGRUPOATRIB BIT" & vbCrLf
sConsulta = sConsulta & "DECLARE @OFEITEMATRIB BIT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @ERROR = 0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @PAID INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @AMBITO SMALLINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @GRUPO VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @TIPO SMALLINT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE CATRIBS CURSOR LOCAL FORWARD_ONLY  FOR " & vbCrLf
sConsulta = sConsulta & "SELECT PA.ID, AMBITO, GRUPO, TIPO" & vbCrLf
sConsulta = sConsulta & "  FROM PROCE_ATRIB PA" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN DEF_ATRIB DA" & vbCrLf
sConsulta = sConsulta & "           ON PA.ATRIB = DA.ID" & vbCrLf
sConsulta = sConsulta & " WHERE ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "   AND GMN1= @GMN1" & vbCrLf
sConsulta = sConsulta & "   AND PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "   AND INTERNO = 0" & vbCrLf
sConsulta = sConsulta & "   AND OBLIG = 1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "OPEN CATRIBS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM CATRIBS INTO @PAID, @AMBITO, @GRUPO,  @TIPO" & vbCrLf
sConsulta = sConsulta & "SET @OFEATRIB=0" & vbCrLf
sConsulta = sConsulta & "SET @OFEGRUPOATRIB=0" & vbCrLf
sConsulta = sConsulta & "SET @OFEITEMATRIB=0" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0 AND @OFEATRIB=0 AND @OFEGRUPOATRIB=0 AND @OFEITEMATRIB=0" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    IF @AMBITO = 1" & vbCrLf
sConsulta = sConsulta & "      BEGIN" & vbCrLf
sConsulta = sConsulta & "        SET @OFEATRIB =1" & vbCrLf
sConsulta = sConsulta & "   SELECT @OFEATRIB = CASE " & vbCrLf
sConsulta = sConsulta & "                  WHEN CASE @TIPO " & vbCrLf
sConsulta = sConsulta & "                          WHEN 1 THEN CASE WHEN VALOR_TEXT ='' THEN NULL ELSE VALOR_TEXT END " & vbCrLf
sConsulta = sConsulta & "                          WHEN 2 THEN CAST(VALOR_NUM AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                          WHEN 3 THEN CAST(VALOR_FEC AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                          WHEN 4 THEN CAST(VALOR_BOOL AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                       END IS NULL " & vbCrLf
sConsulta = sConsulta & "                  THEN 1 " & vbCrLf
sConsulta = sConsulta & "                  ELSE 0 " & vbCrLf
sConsulta = sConsulta & "               END" & vbCrLf
sConsulta = sConsulta & "          FROM OFE_ATRIB" & vbCrLf
sConsulta = sConsulta & "         WHERE ANYO= @ANYO" & vbCrLf
sConsulta = sConsulta & "           AND GMN1= @GMN1" & vbCrLf
sConsulta = sConsulta & "           AND PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "           AND PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "           AND OFE = @OFE" & vbCrLf
sConsulta = sConsulta & "           AND ATRIB_ID = @PAID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "    IF @AMBITO = 2" & vbCrLf
sConsulta = sConsulta & "      BEGIN" & vbCrLf
sConsulta = sConsulta & "        SET @OFEGRUPOATRIB =0" & vbCrLf
sConsulta = sConsulta & "        IF @GRUPO IS NULL " & vbCrLf
sConsulta = sConsulta & "          BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "          IF (SELECT COUNT(*) " & vbCrLf
sConsulta = sConsulta & "                FROM OFE_GR_ATRIB " & vbCrLf
sConsulta = sConsulta & "               WHERE ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "                 AND GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "                 AND PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "                 AND PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "                 AND OFE = @OFE" & vbCrLf
sConsulta = sConsulta & "                 AND ATRIB_ID = @PAID" & vbCrLf
sConsulta = sConsulta & "                 AND CASE @TIPO " & vbCrLf
sConsulta = sConsulta & "                          WHEN 1 THEN CASE WHEN VALOR_TEXT ='' THEN NULL ELSE VALOR_TEXT END " & vbCrLf
sConsulta = sConsulta & "                          WHEN 2 THEN CAST(VALOR_NUM AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                          WHEN 3 THEN CAST(VALOR_FEC AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                          WHEN 4 THEN CAST(VALOR_BOOL AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                       END IS NOT NULL) <> (SELECT COUNT(*) " & vbCrLf
sConsulta = sConsulta & "                 FROM PROCE_GRUPO " & vbCrLf
sConsulta = sConsulta & "                WHERE ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "                  AND GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "                  AND PROCE = @PROCE)" & vbCrLf
sConsulta = sConsulta & "             SET @OFEGRUPOATRIB = 1" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "         ELSE" & vbCrLf
sConsulta = sConsulta & "          IF (SELECT COUNT(*) " & vbCrLf
sConsulta = sConsulta & "                FROM OFE_GR_ATRIB " & vbCrLf
sConsulta = sConsulta & "               WHERE ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "                 AND GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "                 AND PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "                 AND GRUPO = @GRUPO" & vbCrLf
sConsulta = sConsulta & "                 AND PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "                 AND OFE = @OFE" & vbCrLf
sConsulta = sConsulta & "                 AND ATRIB_ID = @PAID" & vbCrLf
sConsulta = sConsulta & "                 AND CASE @TIPO " & vbCrLf
sConsulta = sConsulta & "                          WHEN 1 THEN CASE WHEN VALOR_TEXT ='' THEN NULL ELSE VALOR_TEXT END " & vbCrLf
sConsulta = sConsulta & "                          WHEN 2 THEN CAST(VALOR_NUM AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                          WHEN 3 THEN CAST(VALOR_FEC AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                          WHEN 4 THEN CAST(VALOR_BOOL AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                       END IS NOT NULL)  =0 " & vbCrLf
sConsulta = sConsulta & "             SET @OFEGRUPOATRIB = 1" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "    IF @AMBITO = 3" & vbCrLf
sConsulta = sConsulta & "      BEGIN" & vbCrLf
sConsulta = sConsulta & "        SET @OFEITEMATRIB =0" & vbCrLf
sConsulta = sConsulta & "        IF @GRUPO IS NULL " & vbCrLf
sConsulta = sConsulta & "          BEGIN" & vbCrLf
sConsulta = sConsulta & "            IF (SELECT COUNT(*) " & vbCrLf
sConsulta = sConsulta & "                  FROM OFE_ITEM_ATRIB " & vbCrLf
sConsulta = sConsulta & "                 WHERE ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "                   AND GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "                   AND PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "                   AND PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "                   AND OFE = @OFE" & vbCrLf
sConsulta = sConsulta & "                   AND ATRIB_ID = @PAID" & vbCrLf
sConsulta = sConsulta & "                   AND CASE @TIPO " & vbCrLf
sConsulta = sConsulta & "                          WHEN 1 THEN CASE WHEN VALOR_TEXT ='' THEN NULL ELSE VALOR_TEXT END " & vbCrLf
sConsulta = sConsulta & "                          WHEN 2 THEN CAST(VALOR_NUM AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                          WHEN 3 THEN CAST(VALOR_FEC AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                          WHEN 4 THEN CAST(VALOR_BOOL AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                       END IS NOT NULL)  <>" & vbCrLf
sConsulta = sConsulta & "                (SELECT COUNT(*) " & vbCrLf
sConsulta = sConsulta & "                   FROM ITEM " & vbCrLf
sConsulta = sConsulta & "                  WHERE ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "                    AND GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "                    AND PROCE = @PROCE                    " & vbCrLf
sConsulta = sConsulta & "                    AND CONF = 1)" & vbCrLf
sConsulta = sConsulta & "                 SET @OFEITEMATRIB = 1" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "        ELSE" & vbCrLf
sConsulta = sConsulta & "           begin" & vbCrLf
sConsulta = sConsulta & "            IF (SELECT COUNT(*) " & vbCrLf
sConsulta = sConsulta & "                  FROM OFE_ITEM_ATRIB OIA" & vbCrLf
sConsulta = sConsulta & "                        INNER JOIN ITEM I" & vbCrLf
sConsulta = sConsulta & "                                ON OIA.ANYO = I.ANYO" & vbCrLf
sConsulta = sConsulta & "                               AND OIA.GMN1 = I.GMN1" & vbCrLf
sConsulta = sConsulta & "                               AND OIA.PROCE = I.PROCE" & vbCrLf
sConsulta = sConsulta & "                               AND OIA.ITEM = I.ID" & vbCrLf
sConsulta = sConsulta & "                 WHERE OIA.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "                   AND OIA.GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "                   AND OIA.PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "                   AND I.GRUPO = @GRUPO" & vbCrLf
sConsulta = sConsulta & "                   AND OIA.PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "                   AND OIA.OFE = @OFE" & vbCrLf
sConsulta = sConsulta & "                   AND OIA.ATRIB_ID = @PAID" & vbCrLf
sConsulta = sConsulta & "                   AND CASE @TIPO " & vbCrLf
sConsulta = sConsulta & "                          WHEN 1 THEN CASE WHEN VALOR_TEXT ='' THEN NULL ELSE VALOR_TEXT END " & vbCrLf
sConsulta = sConsulta & "                          WHEN 2 THEN CAST(VALOR_NUM AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                          WHEN 3 THEN CAST(VALOR_FEC AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                          WHEN 4 THEN CAST(VALOR_BOOL AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                       END IS NOT NULL)  <>" & vbCrLf
sConsulta = sConsulta & "                (SELECT COUNT(*) " & vbCrLf
sConsulta = sConsulta & "                   FROM ITEM " & vbCrLf
sConsulta = sConsulta & "                  WHERE ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "                    AND GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "                    AND PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "                    AND GRUPO = @GRUPO" & vbCrLf
sConsulta = sConsulta & "                    AND CONF = 1)" & vbCrLf
sConsulta = sConsulta & "                 SET @OFEITEMATRIB = 1" & vbCrLf
sConsulta = sConsulta & "             end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "      FETCH NEXT FROM CATRIBS INTO @PAID, @AMBITO, @GRUPO,  @TIPO" & vbCrLf
sConsulta = sConsulta & "    END" & vbCrLf
sConsulta = sConsulta & "    " & vbCrLf
sConsulta = sConsulta & "CLOSE CATRIBS" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE CATRIBS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @OFEATRIB = 1" & vbCrLf
sConsulta = sConsulta & "  SET @ERROR = 2" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @OFEGRUPOATRIB = 1" & vbCrLf
sConsulta = sConsulta & "  SET @ERROR = 3" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @OFEITEMATRIB = 1" & vbCrLf
sConsulta = sConsulta & "  SET @ERROR = 4" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ERROR = 0 " & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    UPDATE PROCE_OFE SET ULT= 0, LEIDO = 1  WHERE ANYO = @ANYO AND GMN1 = @GMN1 AND PROCE = @PROCE AND PROVE = @PROVE AND ULT = 1" & vbCrLf
sConsulta = sConsulta & "    UPDATE PROCE_OFE SET ULT= 1, ENVIADA = 1 WHERE ANYO = @ANYO AND GMN1 = @GMN1 AND PROCE = @PROCE AND PROVE = @PROVE AND OFE = @OFE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    ALTER TABLE PROCE_OFE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "    ALTER TABLE GRUPO_OFE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "    ALTER TABLE ITEM_OFE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "    ALTER TABLE OFE_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "    ALTER TABLE OFE_GR_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "    ALTER TABLE OFE_ITEM_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "    ALTER TABLE PROCE_OFE_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "    ALTER TABLE OFE_GR_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "    ALTER TABLE OFE_ITEM_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    DECLARE @NUMOFEWEB AS int" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    SELECT @NEWOFE = ISNULL(MAX(OFE),0) + 1 FROM PROCE_OFE WHERE ANYO = @ANYO AND GMN1 = @GMN1 AND PROCE = @PROCE AND PROVE = @PROVE AND OFE != -1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    UPDATE OFE_ITEM_ADJUN SET OFE = @NEWOFE WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE = @PROCE AND PROVE = @PROVE AND OFE = -1" & vbCrLf
sConsulta = sConsulta & "    UPDATE OFE_GR_ADJUN SET OFE = @NEWOFE WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE = @PROCE AND PROVE = @PROVE AND OFE = -1" & vbCrLf
sConsulta = sConsulta & "    UPDATE PROCE_OFE_ADJUN SET OFE = @NEWOFE WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE = @PROCE AND PROVE = @PROVE AND OFE = -1" & vbCrLf
sConsulta = sConsulta & "    UPDATE OFE_ITEM_ATRIB SET OFE = @NEWOFE WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE = @PROCE AND PROVE = @PROVE AND OFE = -1" & vbCrLf
sConsulta = sConsulta & "    UPDATE OFE_GR_ATRIB SET OFE = @NEWOFE WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE = @PROCE AND PROVE = @PROVE AND OFE = -1" & vbCrLf
sConsulta = sConsulta & "    UPDATE OFE_ATRIB SET OFE = @NEWOFE WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE = @PROCE AND PROVE = @PROVE AND OFE = -1" & vbCrLf
sConsulta = sConsulta & "    UPDATE ITEM_OFE SET NUM = @NEWOFE WHERE  ANYO = @ANYO AND GMN1=@GMN1 AND PROCE = @PROCE AND PROVE = @PROVE AND NUM = -1" & vbCrLf
sConsulta = sConsulta & "    UPDATE GRUPO_OFE SET OFE = @NEWOFE WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE = @PROCE AND PROVE = @PROVE AND OFE = -1" & vbCrLf
sConsulta = sConsulta & "    UPDATE PROCE_OFE SET OFE = @NEWOFE WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE = @PROCE AND PROVE = @PROVE AND OFE = -1" & vbCrLf
sConsulta = sConsulta & "    UPDATE PROCE_PROVE SET OFE = 1  WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE = @PROCE AND PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    SELECT @NUMOFEWEB = isnull(MAX(OFEWEB),0)+1 FROM PROCE_OFE_WEB WHERE ANYO = @ANYO AND GMN1 = @GMN1 AND PROCE = @PROCE AND PROVE = @PROVE " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    INSERT INTO PROCE_OFE_WEB (ANYO, GMN1, PROCE, PROVE, OFEWEB, OFE, FECREC, FECVAL, MON)" & vbCrLf
sConsulta = sConsulta & "    SELECT ANYO, GMN1, PROCE, PROVE, @NUMOFEWEB, OFE, FECREC,FECVAL,MON " & vbCrLf
sConsulta = sConsulta & "      FROM PROCE_OFE " & vbCrLf
sConsulta = sConsulta & "     WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE = @PROCE AND PROVE = @PROVE AND OFE = @NEWOFE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    INSERT INTO ITEM_OFE_WEB (ANYO, GMN1, PROCE, ITEM, PROVE, NUM, PRECIO, CANTMAX)" & vbCrLf
sConsulta = sConsulta & "    SELECT ANYO, GMN1, PROCE, ITEM, PROVE, @NUMOFEWEB, PRECIO, CANTMAX" & vbCrLf
sConsulta = sConsulta & "      FROM ITEM_OFE" & vbCrLf
sConsulta = sConsulta & "     WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE = @PROCE AND PROVE = @PROVE AND NUM = @NEWOFE" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    INSERT INTO PROCE_OFE_WEB_ADJUN (ANYO, GMN1, PROCE, PROVE, OFEWEB, ID, NOM, COM, IDPORTAL)" & vbCrLf
sConsulta = sConsulta & "    SELECT ANYO, GMN1, PROCE, PROVE, @NUMOFEWEB, ID, NOM, COM, IDPORTAL" & vbCrLf
sConsulta = sConsulta & "      FROM PROCE_OFE_ADJUN" & vbCrLf
sConsulta = sConsulta & "     WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE = @PROCE AND PROVE = @PROVE AND OFE = @NEWOFE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    ALTER TABLE PROCE_OFE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "    ALTER TABLE GRUPO_OFE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "    ALTER TABLE ITEM_OFE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "    ALTER TABLE OFE_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "    ALTER TABLE OFE_GR_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "    ALTER TABLE OFE_ITEM_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "    ALTER TABLE PROCE_OFE_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "    ALTER TABLE OFE_GR_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "    ALTER TABLE OFE_ITEM_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    UPDATE ITEM_OFE SET ULT = 0 WHERE ANYO = @ANYO AND GMN1 = @GMN1 AND PROCE =@PROCE AND PROVE=@PROVE AND NUM!=@NEWOFE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    DECLARE @ITEM INT" & vbCrLf
sConsulta = sConsulta & "    DECLARE C1 CURSOR LOCAL FOR SELECT ID FROM ITEM WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "    OPEN C1" & vbCrLf
sConsulta = sConsulta & "    " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    FETCH NEXT FROM C1 INTO @ITEM" & vbCrLf
sConsulta = sConsulta & "    WHILE @@FETCH_STATUS = 0" & vbCrLf
sConsulta = sConsulta & "      BEGIN" & vbCrLf
sConsulta = sConsulta & "        UPDATE ITEM_OFE SET ULT = 1 ,PRECIO = PRECIO" & vbCrLf
sConsulta = sConsulta & "          FROM ITEM_OFE " & vbCrLf
sConsulta = sConsulta & "         WHERE ANYO = @ANYO AND GMN1 = @GMN1 AND PROCE = @PROCE AND PROVE = @PROVE AND ITEM=@ITEM AND NUM = @NEWOFE" & vbCrLf
sConsulta = sConsulta & "        FETCH NEXT FROM C1 INTO @ITEM" & vbCrLf
sConsulta = sConsulta & "      END " & vbCrLf
sConsulta = sConsulta & "    CLOSE C1" & vbCrLf
sConsulta = sConsulta & "    DEALLOCATE C1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PROCE_OFE_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PROCE_OFE_TG_INS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TRIGGER PROCE_OFE_TG_INS ON dbo.PROCE_OFE" & vbCrLf
sConsulta = sConsulta & "FOR INSERT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN1 AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCE AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVE AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @OFE AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @MAXOFE AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ULT AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @MON VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @CAMBIO FLOAT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCEEST int" & vbCrLf
sConsulta = sConsulta & "DECLARE @FECLIMITE datetime" & vbCrLf
sConsulta = sConsulta & "DECLARE @SUBASTA tinyint" & vbCrLf
sConsulta = sConsulta & "DECLARE @MARGEN int" & vbCrLf
sConsulta = sConsulta & "DECLARE @AHORA datetime" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_PROCE_OFE_Ins CURSOR LOCAL FOR SELECT ANYO,GMN1,PROCE,PROVE,OFE,ULT,MON,CAMBIO FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_PROCE_OFE_Ins" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_OFE_Ins INTO @ANYO,@GMN1,@PROCE,@PROVE,@OFE,@ULT,@MON,@CAMBIO" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    EXEC SP_NUEVA_OFERTA @ANYO = @ANYO, @GMN1=@GMN1, @PROCE = @PROCE, @PROVE = @PROVE, @OFE=@OFE,@ULT=@ULT,@MONNEW=@MON,@CAMBIONEW=@CAMBIO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    /* Actualizamos el campo OFE de PROCE_PROVE que indica si el proveedor ha presentado ofertas para ese proceso*/" & vbCrLf
sConsulta = sConsulta & "    IF @OFE>0" & vbCrLf
sConsulta = sConsulta & "      UPDATE PROCE_PROVE SET OFE = 1  WHERE ANYO= @ANYO  AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "    /* Si se han insertado ofertas, actualizamos el estado del proceso  si es que este era inferior a 7 */" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    SELECT @PROCEEST = P.EST, @FECLIMITE= P.FECLIMOFE,@SUBASTA = PD.SUBASTA, @MARGEN = PD.SUBASTAESPERA  " & vbCrLf
sConsulta = sConsulta & "      FROM PROCE P " & vbCrLf
sConsulta = sConsulta & "              INNER JOIN PROCE_DEF PD" & vbCrLf
sConsulta = sConsulta & "                      ON P.ANYO = PD.ANYO" & vbCrLf
sConsulta = sConsulta & "                     AND P.GMN1 = PD.GMN1" & vbCrLf
sConsulta = sConsulta & "                     AND P.COD = PD.PROCE" & vbCrLf
sConsulta = sConsulta & "     WHERE P.ANYO=@ANYO  " & vbCrLf
sConsulta = sConsulta & "       AND P.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "       AND P.COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "    " & vbCrLf
sConsulta = sConsulta & "    IF @PROCEEST < 7  /* 7= Con ofertas */" & vbCrLf
sConsulta = sConsulta & "        UPDATE PROCE SET EST = 7  WHERE ANYO= @ANYO  AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    FETCH NEXT FROM curTG_PROCE_OFE_Ins INTO @ANYO,@GMN1,@PROCE,@PROVE,@OFE,@ULT,@MON,@CAMBIO" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CLOSE curTG_PROCE_OFE_Ins" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_PROCE_OFE_Ins" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Function


Public Function CodigoDeActualizacion2_10_50_00A2_10_50_01() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim bBool As Boolean

On Error GoTo error
    

sConsulta = "BEGIN TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = True
ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

frmProgreso.lblDetalle = "Modificamos tabla de log"
frmProgreso.lblDetalle.Refresh
frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

sConsulta = "ALTER TABLE LOG_ADJ ALTER COLUMN ESP VARCHAR(800) NULL"
ExecuteSQL gRDOCon, sConsulta

v_2_10_5_1_Storeds

sConsulta = "UPDATE VERSION SET NUM ='2.10.50.01'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "COMMIT TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = False

CodigoDeActualizacion2_10_50_00A2_10_50_01 = True
Exit Function
    
error:

    If bTransaccionEnCurso Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_10_50_00A2_10_50_01 = False

End Function


Public Function CodigoDeActualizacion2_10_50_01A2_10_50_02() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim bBool As Boolean

On Error GoTo error
    

sConsulta = "BEGIN TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = True
ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

frmProgreso.lblDetalle = "Modificamos tabla de log"
frmProgreso.lblDetalle.Refresh
frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ITEMS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_ITEMS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE  SP_ITEMS  @ANYO INT,@GMN1 VARCHAR(50),@PROCE INT,@GRUPO VARCHAR(50)=null, @PROVE varchar(50), @OFE smallint, @INTERNO TINYINT = NULL, @TODOS TINYINT = 0, @MON varchar(50) = null  AS " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "set NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @CAM FLOAT " & vbCrLf
sConsulta = sConsulta & "DECLARE @EQUIVPROC FLOAT  " & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCEEST smallint" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @TIPOATRIB tinyint" & vbCrLf
sConsulta = sConsulta & "DECLARE @CODATRIB varchar(500)" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDPROCEATRIB int" & vbCrLf
sConsulta = sConsulta & "DECLARE @PARAMDEF NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @ALTER NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @INSERT NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @EQUIVPROC = CAMBIO, @PROCEEST =EST " & vbCrLf
sConsulta = sConsulta & "  FROM PROCE " & vbCrLf
sConsulta = sConsulta & " WHERE ANYO=@ANYO" & vbCrLf
sConsulta = sConsulta & "   AND GMN1=@GMN1" & vbCrLf
sConsulta = sConsulta & "   AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @CAM =( SELECT CAMBIO" & vbCrLf
sConsulta = sConsulta & "             FROM PROCE_OFE" & vbCrLf
sConsulta = sConsulta & "            WHERE ANYO=@ANYO" & vbCrLf
sConsulta = sConsulta & "              AND GMN1=@GMN1" & vbCrLf
sConsulta = sConsulta & "              AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "              AND PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "              AND OFE=@OFE)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CAM IS NULL" & vbCrLf
sConsulta = sConsulta & "  SELECT @CAM =  EQUIV / @EQUIVPROC" & vbCrLf
sConsulta = sConsulta & "    FROM MON " & vbCrLf
sConsulta = sConsulta & "   WHERE COD = @MON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "declare C1 CURSOR LOCAL FOR" & vbCrLf
sConsulta = sConsulta & "SELECT PA.ID, DA.COD, DA.TIPO" & vbCrLf
sConsulta = sConsulta & " FROM PROCE_ATRIB PA" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN DEF_ATRIB DA" & vbCrLf
sConsulta = sConsulta & "               ON PA.ATRIB = DA.ID" & vbCrLf
sConsulta = sConsulta & "WHERE PA.ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "  AND PA.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "  AND PA.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "  AND ISNULL(PA.GRUPO,'#') = CASE WHEN PA.GRUPO IS NULL THEN ISNULL(PA.GRUPO,'#') ELSE @GRUPO END" & vbCrLf
sConsulta = sConsulta & "  AND PA.AMBITO = 3" & vbCrLf
sConsulta = sConsulta & "  AND PA.INTERNO = CASE WHEN @INTERNO IS NULL THEN PA.INTERNO ELSE @INTERNO END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE #ATRIBS (ATANYO int NULL ," & vbCrLf
If gCollation = "" Then
    sConsulta = sConsulta & "   ATGMN1 varchar (50) NULL ," & vbCrLf
Else
    sConsulta = sConsulta & "   ATGMN1 varchar (50) COLLATE " & gCollation & " NULL ," & vbCrLf
End If
sConsulta = sConsulta & "   ATPROCE int NULL ," & vbCrLf
sConsulta = sConsulta & "   ATITEM int NULL " & vbCrLf
sConsulta = sConsulta & ") " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @INSERT = 'INSERT INTO #ATRIBS (ATANYO, ATGMN1, ATPROCE, ATITEM, '" & vbCrLf
sConsulta = sConsulta & "SET @SQL='SELECT ANYO, GMN1,  PROCE, ITEM, '" & vbCrLf
sConsulta = sConsulta & "set @ALTER = 'ALTER TABLE #ATRIBS ADD '" & vbCrLf
sConsulta = sConsulta & "OPEN C1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C1 INTO @IDPROCEATRIB, @CODATRIB, @TIPOATRIB" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS = 0" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    " & vbCrLf
sConsulta = sConsulta & "    SET @INSERT = @INSERT + '[AT_' + @CODATRIB + '],'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    IF @TIPOATRIB = 1 " & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "        SET @ALTER = @ALTER  + '[AT_' + @CODATRIB + '] VARCHAR(800), '" & vbCrLf
sConsulta = sConsulta & "        SET @SQL = @SQL + 'MAX(case IOA.ATRIB_ID when ' + cast(@IDPROCEATRIB as varchar(100)) + ' THEN VALOR_TEXT else NULL end) as [' + @CODATRIB + '],'" & vbCrLf
sConsulta = sConsulta & "      END " & vbCrLf
sConsulta = sConsulta & "    ELSE IF @TIPOATRIB = 2" & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "        SET @ALTER = @ALTER  + '[AT_' + @CODATRIB + '] FLOAT, '" & vbCrLf
sConsulta = sConsulta & "        SET @SQL = @SQL + 'MAX(case IOA.ATRIB_ID when ' + cast(@IDPROCEATRIB as varchar(100)) + ' THEN VALOR_NUM else NULL end) as [' + @CODATRIB + '],'" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "    ELSE IF @TIPOATRIB = 3" & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "        SET @ALTER = @ALTER  + '[AT_' + @CODATRIB + '] DATETIME, '" & vbCrLf
sConsulta = sConsulta & "        SET @SQL = @SQL + 'MAX(case IOA.ATRIB_ID when ' + cast(@IDPROCEATRIB as varchar(100)) + ' THEN VALOR_FEC else NULL end) as [' + @CODATRIB + '],'" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "    ELSE " & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "        SET @ALTER = @ALTER + '[AT_' + @CODATRIB + '] TINYINT, '" & vbCrLf
sConsulta = sConsulta & "        SET @SQL = @SQL + 'MAX(case IOA.ATRIB_ID when ' + cast(@IDPROCEATRIB as varchar(100)) + ' THEN VALOR_BOOL else NULL end) as [' + @CODATRIB + '],'" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    FETCH NEXT FROM C1 INTO @IDPROCEATRIB, @CODATRIB, @TIPOATRIB" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if @INSERT != 'INSERT INTO #ATRIBS (ATANYO, ATGMN1, ATPROCE, ATITEM, '" & vbCrLf
sConsulta = sConsulta & "  begin" & vbCrLf
sConsulta = sConsulta & "    set @SQL = LEFT(@SQL,LEN(@SQL)-1)" & vbCrLf
sConsulta = sConsulta & "    set @INSERT = LEFT(@INSERT,LEN(@INSERT)-1)" & vbCrLf
sConsulta = sConsulta & "    set @ALTER = LEFT(@ALTER,LEN(@ALTER)-1)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    EXEC SP_EXECUTESQL  @ALTER " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    SET @SQL = @INSERT + ')" & vbCrLf
sConsulta = sConsulta & "    ' + @SQL + '" & vbCrLf
sConsulta = sConsulta & "      FROM OFE_ITEM_ATRIB IOA" & vbCrLf
sConsulta = sConsulta & "     WHERE IOA.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "       AND IOA.GMN1 = @GMN1  " & vbCrLf
sConsulta = sConsulta & "       AND IOA.PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "       AND IOA.PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "       AND IOA.OFE = @OFE" & vbCrLf
sConsulta = sConsulta & "    GROUP BY ANYO, GMN1,  PROCE, ITEM'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    SET @PARAMDEF = '@ANYO INT, @GMN1 VARCHAR(50), @PROCE INT,@GRUPO VARCHAR(50), @PROVE varchar(50), @OFE smallint'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    EXEC SP_EXECUTESQL  @SQL, @PARAMDEF, @ANYO = @ANYO, @GMN1 = @GMN1, @PROCE = @PROCE, @GRUPO = @GRUPO, @PROVE = @PROVE, @OFE = @OFE" & vbCrLf
sConsulta = sConsulta & "  end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--   Se devuelve siempre en la moneda de la oferta" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT I.ANYO, I.GMN1, I.PROCE, I.GRUPO, I.ID, I.GMN2, I.GMN3, I.GMN4, I.ART, I.DESCR + A.DEN AS DESCR, " & vbCrLf
sConsulta = sConsulta & "       I.DEST, I.UNI,UNI.DEN UNIDEN, I.CANT, I.PREC*@CAM as PREC, I.PRES*@CAM as PRES," & vbCrLf
sConsulta = sConsulta & "       I.PAG, PAG.DEN DENPAG, I.FECINI, I.FECFIN, I.ESP, I.CONF, I.EST," & vbCrLf
sConsulta = sConsulta & "       I.OBJ*@CAM AS OBJ , I.FECHAOBJ, " & vbCrLf
sConsulta = sConsulta & "       O.PROVE MINPROVE, P2.DEN MINPROVEDEN, O.PRECIO*@EQUIVPROC*@CAM MINPRECIO," & vbCrLf
sConsulta = sConsulta & "       CASE WHEN @PROVE = O.PROVE THEN 1 ELSE 0 END PROPIA," & vbCrLf
sConsulta = sConsulta & "       CASE  WHEN NUMESP > 0 THEN 1 WHEN NOT I.ESP  IS NULL THEN 1 ELSE 0 END ESPECADJ ," & vbCrLf
sConsulta = sConsulta & "       ITO.PRECIO, ITO.CANTMAX, ITO.ULT, ITO.PRECIO2, ITO.PRECIO3,ITO.USAR,ITO.FECACT," & vbCrLf
sConsulta = sConsulta & "       IA.NUMADJUN, ITO.COMENT1, ITO.COMENT2, ITO.COMENT3, @EQUIVPROC*@CAM CAMBIO, cast(ITO.OBSADJUN as varchar(2000)) OBSADJUN, ATR.*" & vbCrLf
sConsulta = sConsulta & "  FROM ITEM I" & vbCrLf
sConsulta = sConsulta & "        LEFT JOIN ART4 A" & vbCrLf
sConsulta = sConsulta & "               ON I.ART = A.COD " & vbCrLf
sConsulta = sConsulta & "              AND I.GMN4 = A.GMN4 " & vbCrLf
sConsulta = sConsulta & "              AND I.GMN3 = A.GMN3 " & vbCrLf
sConsulta = sConsulta & "              AND I.GMN2 = A.GMN2" & vbCrLf
sConsulta = sConsulta & "              AND I.GMN1 = A.GMN1" & vbCrLf
sConsulta = sConsulta & "        LEFT JOIN ITEM_OFE ITO" & vbCrLf
sConsulta = sConsulta & "               ON I.ANYO = ITO.ANYO" & vbCrLf
sConsulta = sConsulta & "              AND I.GMN1 = ITO.GMN1" & vbCrLf
sConsulta = sConsulta & "              AND I.PROCE = ITO.PROCE" & vbCrLf
sConsulta = sConsulta & "              AND I.ID = ITO.ITEM" & vbCrLf
sConsulta = sConsulta & "              AND @PROVE = ITO.PROVE" & vbCrLf
sConsulta = sConsulta & "              AND @OFE = ITO.NUM" & vbCrLf
sConsulta = sConsulta & "        LEFT JOIN (OFEMIN O " & vbCrLf
sConsulta = sConsulta & "                     INNER JOIN PROVE P2 " & vbCrLf
sConsulta = sConsulta & "                             ON O.PROVE = P2.COD" & vbCrLf
sConsulta = sConsulta & "                   )" & vbCrLf
sConsulta = sConsulta & "               ON I.ANYO = O.ANYO" & vbCrLf
sConsulta = sConsulta & "              AND I.GMN1 = O.GMN1" & vbCrLf
sConsulta = sConsulta & "              AND I.PROCE = O.PROCE" & vbCrLf
sConsulta = sConsulta & "              AND I.ID = O.ITEM" & vbCrLf
sConsulta = sConsulta & "               " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "        LEFT JOIN #ATRIBS ATR               ON I.ANYO = ATR.ATANYO" & vbCrLf
sConsulta = sConsulta & "              AND I.GMN1 = ATR.ATGMN1" & vbCrLf
sConsulta = sConsulta & "              AND I.PROCE = ATR.ATPROCE" & vbCrLf
sConsulta = sConsulta & "              AND I.ID = ATR.ATITEM        LEFT JOIN (SELECT IE.ANYO, IE.GMN1, IE.PROCE, IE.ITEM, COUNT(IE.ID) NUMESP " & vbCrLf
sConsulta = sConsulta & "                     FROM ITEM_ESP IE" & vbCrLf
sConsulta = sConsulta & "                    WHERE IE.ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "                      AND IE.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "                      AND IE.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "                 GROUP BY IE.ANYO, IE.GMN1, IE.PROCE, IE.ITEM) IE" & vbCrLf
sConsulta = sConsulta & "               ON I.ID = IE.ITEM " & vbCrLf
sConsulta = sConsulta & "              AND I.GMN1 = IE.GMN1" & vbCrLf
sConsulta = sConsulta & "              AND I.PROCE = IE.PROCE" & vbCrLf
sConsulta = sConsulta & "              AND I.ANYO = IE.ANYO " & vbCrLf
sConsulta = sConsulta & "        LEFT JOIN (SELECT IA.ANYO, IA.GMN1, IA.PROCE, IA.ITEM, COUNT(IA.ID) NUMADJUN" & vbCrLf
sConsulta = sConsulta & "                     FROM OFE_ITEM_ADJUN IA" & vbCrLf
sConsulta = sConsulta & "                    WHERE IA.ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "                      AND IA.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "                      AND IA.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "                      AND IA.PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "                      AND IA.OFE = @OFE" & vbCrLf
sConsulta = sConsulta & "                 GROUP BY IA.ANYO, IA.GMN1, IA.PROCE, IA.ITEM) IA" & vbCrLf
sConsulta = sConsulta & "               ON I.ID = IA.ITEM " & vbCrLf
sConsulta = sConsulta & "              AND I.GMN1 = IA.GMN1" & vbCrLf
sConsulta = sConsulta & "              AND I.PROCE = IA.PROCE" & vbCrLf
sConsulta = sConsulta & "              AND I.ANYO = IA.ANYO " & vbCrLf
sConsulta = sConsulta & "        LEFT JOIN PAG " & vbCrLf
sConsulta = sConsulta & "               ON I.PAG = PAG.COD" & vbCrLf
sConsulta = sConsulta & "        LEFT JOIN UNI " & vbCrLf
sConsulta = sConsulta & "               ON I.UNI = UNI.COD" & vbCrLf
sConsulta = sConsulta & "WHERE I.ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "  AND I.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "  AND I.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "  AND I.CONF=1" & vbCrLf
sConsulta = sConsulta & "  AND I.GRUPO=case when @GRUPO is null then i.grupo else @GRUPO END" & vbCrLf
sConsulta = sConsulta & "  AND CASE WHEN I.EST IS NULL THEN -1 ELSE I.EST END = CASE WHEN @PROCEEST=11 THEN CASE WHEN @TODOS=1 THEN I.EST ELSE 0 END ELSE -1 END" & vbCrLf
sConsulta = sConsulta & "ORDER BY I.ART,DESCR" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "UPDATE VERSION SET NUM ='2.10.50.02'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "COMMIT TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = False

CodigoDeActualizacion2_10_50_01A2_10_50_02 = True
Exit Function
    
error:

    If bTransaccionEnCurso Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_10_50_01A2_10_50_02 = False

End Function


Private Function v_2_10_5_1_Storeds()
Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_SOLICITUD_DATOS_MENSAJE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_SOLICITUD_DATOS_MENSAJE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_SOLICITUD_DATOS_MENSAJE " & vbCrLf
sConsulta = sConsulta & "@ID INT," & vbCrLf
sConsulta = sConsulta & "@TYPE TINYINT, " & vbCrLf
sConsulta = sConsulta & "@DESCR_BREVE VarChar(50) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@DESCR_DET VarChar( 500) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@FECHA_ALTA DateTIME OUTPUT," & vbCrLf
sConsulta = sConsulta & "@FECHA_NECESIDAD DateTIME OUTPUT," & vbCrLf
sConsulta = sConsulta & "@IMPORTE FLOAT OUTPUT," & vbCrLf
sConsulta = sConsulta & "@MON_COD VarChar (50) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@MON_DEN VarChar(100) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@APROBADOR VarChar(150) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@APROBADOR_COD VarChar( 50) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@APROBADOR_NOM VarChar(50) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@APROBADOR_APE VarChar(100) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@APROBADOR_TFNO VarChar(20) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@APROBADOR_EMAIL VarChar(100) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@APROBADOR_FAX VarChar(20) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@APROBADOR_UON varChar(250) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@APROBADOR_DEP_COD VarChar(50) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@APROBADOR_DEP_DEN varChar(100) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@COMPRADOR_COD VarChar( 50) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@COMPRADOR_NOM VarChar(50) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@COMPRADOR_APE VarChar(100) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@COMPRADOR_TFNO VarChar(20) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@COMPRADOR_EMAIL VarChar(100) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@COMPRADOR_FAX VarChar(20) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@COMPRADOR_UON varChar(250) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@COMPRADOR_DEP_COD VarChar(50) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@COMPRADOR_DEP_DEN varChar(100) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@PET_COD VarChar(50) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@PET_NOM VarChar(50) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@PET_APE VarChar(100) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@PET_TFNO varchar(20) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@PET_EMAIL VarChar(100) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@PET_FAX VarChar(20) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@PET_UON VarChar(250) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@PET_DEP_COD VarChar(50) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@PET_DEP_DEN VarChar(100) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@CARGO VarChar(100) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@PROVEEDORES varChar(300) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@DEST_COD VarChar(50) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@DEST_DEN VarChar(100) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@DEST_DIR VarChar(255) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@DEST_POB VarChar(100) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@DEST_CP VarChar(20) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@DEST_PROVI_COD VarChar(50) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@DEST_PROVI_DEN VarChar(100) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@DEST_PAI_COD VarChar(50) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@DEST_PAI_DEN VarChar(100) OUTPUT," & vbCrLf
sConsulta = sConsulta & "@COMENTARIO varChar(500) OUTPUT" & vbCrLf
sConsulta = sConsulta & "AS   " & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "/* " & vbCrLf
sConsulta = sConsulta & "   Par�metros de entrada:  ID  : Identificador de la solicitud" & vbCrLf
sConsulta = sConsulta & "               TYPE    :   " & vbCrLf
sConsulta = sConsulta & "                       0 -> Asignaci�n a comprador" & vbCrLf
sConsulta = sConsulta & "                       1 -> Aprobaci�n a peticionario" & vbCrLf
sConsulta = sConsulta & "                       2 -> Rechazo" & vbCrLf
sConsulta = sConsulta & "                       3 -> Anulaci�n          " & vbCrLf
sConsulta = sConsulta & "                       4 -> Cierre" & vbCrLf
sConsulta = sConsulta & "                       5 -> Reapertura" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "*/" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @TYPE=0" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "   SELECT " & vbCrLf
sConsulta = sConsulta & "   @DESCR_BREVE=S.DEN," & vbCrLf
sConsulta = sConsulta & "   @DESCR_DET =S.DESCR," & vbCrLf
sConsulta = sConsulta & "   @FECHA_ALTA = S.FECALTA," & vbCrLf
sConsulta = sConsulta & "   @FECHA_NECESIDAD=S.FECNEC," & vbCrLf
sConsulta = sConsulta & "   @IMPORTE =S.IMPORTE," & vbCrLf
sConsulta = sConsulta & "   @MON_COD =S.MON," & vbCrLf
sConsulta = sConsulta & "   @MON_DEN =M.DEN," & vbCrLf
sConsulta = sConsulta & "   @APROBADOR = " & vbCrLf
sConsulta = sConsulta & "   case when SEST.PER IS NULL THEN" & vbCrLf
sConsulta = sConsulta & "       (select usu from adm)       " & vbCrLf
sConsulta = sConsulta & "       ELSE" & vbCrLf
sConsulta = sConsulta & "       P2.NOM + ' ' + P2.APE" & vbCrLf
sConsulta = sConsulta & "   end," & vbCrLf
sConsulta = sConsulta & "   @APROBADOR_COD = " & vbCrLf
sConsulta = sConsulta & "   case when SEST.PER IS NULL THEN" & vbCrLf
sConsulta = sConsulta & "       (select usu from adm)       " & vbCrLf
sConsulta = sConsulta & "       ELSE" & vbCrLf
sConsulta = sConsulta & "       SEST.PER            " & vbCrLf
sConsulta = sConsulta & "       END," & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   @APROBADOR_NOM = P2.NOM," & vbCrLf
sConsulta = sConsulta & "   @APROBADOR_APE = P2.APE," & vbCrLf
sConsulta = sConsulta & "   @APROBADOR_TFNO =P2.TFNO," & vbCrLf
sConsulta = sConsulta & "   @APROBADOR_EMAIL = P2.EMAIL," & vbCrLf
sConsulta = sConsulta & "   @APROBADOR_FAX =P2.FAX," & vbCrLf
sConsulta = sConsulta & "   @APROBADOR_UON = " & vbCrLf
sConsulta = sConsulta & "   case when U23.cod is null then" & vbCrLf
sConsulta = sConsulta & "       case when U22.cod is null then" & vbCrLf
sConsulta = sConsulta & "                       CASE WHEN U21.COD IS NULL THEN" & vbCrLf
sConsulta = sConsulta & "                ' '" & vbCrLf
sConsulta = sConsulta & "                               ELSE" & vbCrLf
sConsulta = sConsulta & "                                     U21.cod + ' - ' + U21.den" & vbCrLf
sConsulta = sConsulta & "                                END" & vbCrLf
sConsulta = sConsulta & "                  ELSE" & vbCrLf
sConsulta = sConsulta & "           U21.COD + ' - ' + U22.COD + ' - ' + U22.DEN                        " & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "       U21.COD + ' - ' + U22.COD + ' - ' + U23.COD + ' - '  + U23.DEN" & vbCrLf
sConsulta = sConsulta & "   END," & vbCrLf
sConsulta = sConsulta & "   @APROBADOR_DEP_COD=P2.DEP," & vbCrLf
sConsulta = sConsulta & "   @APROBADOR_DEP_DEN = D2.DEN," & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   @COMPRADOR_COD = S.COMPRADOR," & vbCrLf
sConsulta = sConsulta & "   @COMPRADOR_NOM = P.NOM," & vbCrLf
sConsulta = sConsulta & "   @COMPRADOR_APE= P.APE," & vbCrLf
sConsulta = sConsulta & "   @COMPRADOR_TFNO =P.TFNO," & vbCrLf
sConsulta = sConsulta & "   @COMPRADOR_EMAIL= P.EMAIL," & vbCrLf
sConsulta = sConsulta & "   @COMPRADOR_FAX =P.FAX," & vbCrLf
sConsulta = sConsulta & "   @COMPRADOR_DEP_COD=P.DEP," & vbCrLf
sConsulta = sConsulta & "   @COMPRADOR_DEP_DEN=D.DEN," & vbCrLf
sConsulta = sConsulta & "   @COMPRADOR_UON= " & vbCrLf
sConsulta = sConsulta & "   case when U3.cod is null then" & vbCrLf
sConsulta = sConsulta & "       case when U2.cod is null then" & vbCrLf
sConsulta = sConsulta & "                       CASE WHEN U1.COD IS NULL THEN" & vbCrLf
sConsulta = sConsulta & "                ' '" & vbCrLf
sConsulta = sConsulta & "                               ELSE" & vbCrLf
sConsulta = sConsulta & "                                     U1.cod + ' - ' + U1.den" & vbCrLf
sConsulta = sConsulta & "                                END" & vbCrLf
sConsulta = sConsulta & "                  ELSE" & vbCrLf
sConsulta = sConsulta & "           U1.COD + ' - ' + U2.COD + ' - ' + U2.DEN                        " & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "       U1.COD + ' - ' + U2.COD + ' - ' + U3.COD + ' - '  + U3.DEN" & vbCrLf
sConsulta = sConsulta & "   END," & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   @PET_COD = S.PER," & vbCrLf
sConsulta = sConsulta & "   @PET_NOM = PET.NOM," & vbCrLf
sConsulta = sConsulta & "   @PET_APE = PET.APE," & vbCrLf
sConsulta = sConsulta & "   @PET_TFNO =PET.TFNO," & vbCrLf
sConsulta = sConsulta & "   @PET_EMAIL = PET.EMAIL," & vbCrLf
sConsulta = sConsulta & "   @PET_FAX = PET.FAX," & vbCrLf
sConsulta = sConsulta & "   @PET_UON = " & vbCrLf
sConsulta = sConsulta & "   case when U_PET3.cod is null then" & vbCrLf
sConsulta = sConsulta & "       case when U_PET2.cod is null then" & vbCrLf
sConsulta = sConsulta & "                       CASE WHEN U_PET1.COD IS NULL THEN" & vbCrLf
sConsulta = sConsulta & "                ' '" & vbCrLf
sConsulta = sConsulta & "                               ELSE" & vbCrLf
sConsulta = sConsulta & "                                     U_PET1.cod + ' - ' + U_PET1.den" & vbCrLf
sConsulta = sConsulta & "                                END" & vbCrLf
sConsulta = sConsulta & "                  ELSE" & vbCrLf
sConsulta = sConsulta & "           U_PET1.COD + ' - ' + U_PET2.COD + ' - ' + U_PET2.DEN                        " & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "       U_PET1.COD + ' - ' + U_PET2.COD + ' - ' + U_PET3.COD + ' - '  + U_PET3.DEN" & vbCrLf
sConsulta = sConsulta & "   END," & vbCrLf
sConsulta = sConsulta & "   @PET_DEP_COD= PET.DEP," & vbCrLf
sConsulta = sConsulta & "   @PET_DEP_DEN = PET_D.DEN," & vbCrLf
sConsulta = sConsulta & "   @CARGO = S.CARGO," & vbCrLf
sConsulta = sConsulta & "   @PROVEEDORES = S.PROVEEDORES," & vbCrLf
sConsulta = sConsulta & "   @DEST_COD= S.DEST," & vbCrLf
sConsulta = sConsulta & "   @DEST_DEN = DEST.DEN," & vbCrLf
sConsulta = sConsulta & "   @DEST_DIR =DEST.DIR," & vbCrLf
sConsulta = sConsulta & "   @DEST_POB =DEST.POB," & vbCrLf
sConsulta = sConsulta & "   @DEST_CP = DEST.CP," & vbCrLf
sConsulta = sConsulta & "   @DEST_PROVI_COD = DEST.PROVI," & vbCrLf
sConsulta = sConsulta & "   @DEST_PROVI_DEN = PROVI.DEN," & vbCrLf
sConsulta = sConsulta & "   @DEST_PAI_COD = DEST.PAI," & vbCrLf
sConsulta = sConsulta & "   @DEST_PAI_DEN = PAI.DEN," & vbCrLf
sConsulta = sConsulta & "   @COMENTARIO = SEST.COMENT" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   FROM SOLICIT S" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN SOLICIT_ASIG_COMP SEST ON S.ID=SEST.SOLICIT AND S.ID=@ID " & vbCrLf
sConsulta = sConsulta & "   AND SEST.ID IN (SELECT MAX (ID) FROM SOLICIT_ASIG_COMP WHERE SOLICIT=@ID) " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN MON M ON S.MON=M.COD" & vbCrLf
sConsulta = sConsulta & "   AND S.ID=@ID" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PER P ON S.COMPRADOR=P.COD" & vbCrLf
sConsulta = sConsulta & "   AND S.ID=@ID" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN DEP D ON P.DEP=D.COD" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN UON1 U1 ON U1.COD=P.UON1" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN UON2 U2 ON U2.UON1=P.UON1 AND P.UON2=U2.COD" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN UON3 U3 ON U3.UON1=P.UON1 AND P.UON2=U3.UON2 AND P.UON3=U3.COD" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN PER P2 ON SEST.PER=P2.COD" & vbCrLf
sConsulta = sConsulta & "   AND S.ID=@ID" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN DEP D2 ON P2.DEP=D2.COD" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN UON1 U21 ON U21.COD=P2.UON1" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN UON2 U22 ON U22.UON1=P2.UON1 AND P2.UON2=U22.COD" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN UON3 U23 ON U23.UON1=P2.UON1 AND P2.UON2=U23.UON2 AND P2.UON3=U23.COD" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PER PET ON S.PER=PET.COD AND S.ID=@ID" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN DEP PET_D ON P.DEP=PET_D.COD" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN UON1 U_PET1 ON U_PET1.COD=PET.UON1" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN UON2 U_PET2 ON U_PET2.UON1=PET.UON1 AND PET.UON2=U_PET2.COD" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN UON3 U_PET3 ON U_PET3.UON1=PET.UON1 AND PET.UON2=U_PET3.UON2 AND PET.UON3=U_PET3.COD" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN DEST ON S.DEST=DEST.COD" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PAI ON DEST.PAI=PAI.COD" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PROVI ON PROVI.PAI=PAI.COD AND PROVI.COD=DEST.PROVI" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT " & vbCrLf
sConsulta = sConsulta & "@DESCR_BREVE=S.DEN," & vbCrLf
sConsulta = sConsulta & "@DESCR_DET =S.DESCR," & vbCrLf
sConsulta = sConsulta & "@FECHA_ALTA = S.FECALTA," & vbCrLf
sConsulta = sConsulta & "@FECHA_NECESIDAD=S.FECNEC," & vbCrLf
sConsulta = sConsulta & "@IMPORTE =S.IMPORTE," & vbCrLf
sConsulta = sConsulta & "@MON_COD =S.MON," & vbCrLf
sConsulta = sConsulta & "@MON_DEN =M.DEN," & vbCrLf
sConsulta = sConsulta & "@APROBADOR = " & vbCrLf
sConsulta = sConsulta & "case when SEST.PER IS NULL THEN" & vbCrLf
sConsulta = sConsulta & "   (select usu from adm)       " & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "   P2.NOM + ' ' + P2.APE" & vbCrLf
sConsulta = sConsulta & "end," & vbCrLf
sConsulta = sConsulta & "@APROBADOR_COD = " & vbCrLf
sConsulta = sConsulta & "case when SEST.PER IS NULL THEN" & vbCrLf
sConsulta = sConsulta & "   (select usu from adm)       " & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "   SEST.PER            " & vbCrLf
sConsulta = sConsulta & "   END," & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "@APROBADOR_NOM = P2.NOM," & vbCrLf
sConsulta = sConsulta & "@APROBADOR_APE = P2.APE," & vbCrLf
sConsulta = sConsulta & "@APROBADOR_TFNO =P2.TFNO," & vbCrLf
sConsulta = sConsulta & "@APROBADOR_EMAIL = P2.EMAIL," & vbCrLf
sConsulta = sConsulta & "@APROBADOR_FAX =P2.FAX," & vbCrLf
sConsulta = sConsulta & "@APROBADOR_UON = " & vbCrLf
sConsulta = sConsulta & "case when U23.cod is null then" & vbCrLf
sConsulta = sConsulta & "   case when U22.cod is null then" & vbCrLf
sConsulta = sConsulta & "                   CASE WHEN U21.COD IS NULL THEN" & vbCrLf
sConsulta = sConsulta & "            ' '" & vbCrLf
sConsulta = sConsulta & "                            ELSE" & vbCrLf
sConsulta = sConsulta & "                                  U21.cod + ' - ' + U21.den" & vbCrLf
sConsulta = sConsulta & "                             END" & vbCrLf
sConsulta = sConsulta & "               ELSE" & vbCrLf
sConsulta = sConsulta & "       U21.COD + ' - ' + U22.COD + ' - ' + U22.DEN                        " & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   U21.COD + ' - ' + U22.COD + ' - ' + U23.COD + ' - '  + U23.DEN" & vbCrLf
sConsulta = sConsulta & "END," & vbCrLf
sConsulta = sConsulta & "@APROBADOR_DEP_COD=P2.DEP," & vbCrLf
sConsulta = sConsulta & "@APROBADOR_DEP_DEN = D2.DEN," & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "@COMPRADOR_COD = S.COMPRADOR," & vbCrLf
sConsulta = sConsulta & "@COMPRADOR_NOM = P.NOM," & vbCrLf
sConsulta = sConsulta & "@COMPRADOR_APE= P.APE," & vbCrLf
sConsulta = sConsulta & "@COMPRADOR_TFNO =P.TFNO," & vbCrLf
sConsulta = sConsulta & "@COMPRADOR_EMAIL= P.EMAIL," & vbCrLf
sConsulta = sConsulta & "@COMPRADOR_FAX =P.FAX," & vbCrLf
sConsulta = sConsulta & "@COMPRADOR_DEP_COD=P.DEP," & vbCrLf
sConsulta = sConsulta & "@COMPRADOR_DEP_DEN=D.DEN," & vbCrLf
sConsulta = sConsulta & "@COMPRADOR_UON= " & vbCrLf
sConsulta = sConsulta & "case when U3.cod is null then" & vbCrLf
sConsulta = sConsulta & "   case when U2.cod is null then" & vbCrLf
sConsulta = sConsulta & "                   CASE WHEN U1.COD IS NULL THEN" & vbCrLf
sConsulta = sConsulta & "            ' '" & vbCrLf
sConsulta = sConsulta & "                            ELSE" & vbCrLf
sConsulta = sConsulta & "                                  U1.cod + ' - ' + U1.den" & vbCrLf
sConsulta = sConsulta & "                             END" & vbCrLf
sConsulta = sConsulta & "               ELSE" & vbCrLf
sConsulta = sConsulta & "       U1.COD + ' - ' + U2.COD + ' - ' + U2.DEN                        " & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   U1.COD + ' - ' + U2.COD + ' - ' + U3.COD + ' - '  + U3.DEN" & vbCrLf
sConsulta = sConsulta & "END," & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "@PET_COD = S.PER," & vbCrLf
sConsulta = sConsulta & "@PET_NOM = PET.NOM," & vbCrLf
sConsulta = sConsulta & "@PET_APE = PET.APE," & vbCrLf
sConsulta = sConsulta & "@PET_TFNO =PET.TFNO," & vbCrLf
sConsulta = sConsulta & "@PET_EMAIL = PET.EMAIL," & vbCrLf
sConsulta = sConsulta & "@PET_FAX = PET.FAX," & vbCrLf
sConsulta = sConsulta & "@PET_UON = " & vbCrLf
sConsulta = sConsulta & "case when U_PET3.cod is null then" & vbCrLf
sConsulta = sConsulta & "   case when U_PET2.cod is null then" & vbCrLf
sConsulta = sConsulta & "                   CASE WHEN U_PET1.COD IS NULL THEN" & vbCrLf
sConsulta = sConsulta & "            ' '" & vbCrLf
sConsulta = sConsulta & "                            ELSE" & vbCrLf
sConsulta = sConsulta & "                                  U_PET1.cod + ' - ' + U_PET1.den" & vbCrLf
sConsulta = sConsulta & "                             END" & vbCrLf
sConsulta = sConsulta & "               ELSE" & vbCrLf
sConsulta = sConsulta & "       U_PET1.COD + ' - ' + U_PET2.COD + ' - ' + U_PET2.DEN                        " & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   U_PET1.COD + ' - ' + U_PET2.COD + ' - ' + U_PET3.COD + ' - '  + U_PET3.DEN" & vbCrLf
sConsulta = sConsulta & "END," & vbCrLf
sConsulta = sConsulta & "@PET_DEP_COD= PET.DEP," & vbCrLf
sConsulta = sConsulta & "@PET_DEP_DEN = PET_D.DEN," & vbCrLf
sConsulta = sConsulta & "@CARGO = S.CARGO," & vbCrLf
sConsulta = sConsulta & "@PROVEEDORES = S.PROVEEDORES," & vbCrLf
sConsulta = sConsulta & "@DEST_COD= S.DEST," & vbCrLf
sConsulta = sConsulta & "@DEST_DEN = DEST.DEN," & vbCrLf
sConsulta = sConsulta & "@DEST_DIR =DEST.DIR," & vbCrLf
sConsulta = sConsulta & "@DEST_POB =DEST.POB," & vbCrLf
sConsulta = sConsulta & "@DEST_CP = DEST.CP," & vbCrLf
sConsulta = sConsulta & "@DEST_PROVI_COD = DEST.PROVI," & vbCrLf
sConsulta = sConsulta & "@DEST_PROVI_DEN = PROVI.DEN," & vbCrLf
sConsulta = sConsulta & "@DEST_PAI_COD = DEST.PAI," & vbCrLf
sConsulta = sConsulta & "@DEST_PAI_DEN = PAI.DEN," & vbCrLf
sConsulta = sConsulta & "@COMENTARIO = SEST.COMENT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "FROM SOLICIT S" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN SOLICIT_EST SEST ON S.ID=SEST.SOLICIT AND S.ID=@ID AND SEST.ID IN (SELECT MAX(ID) FROM SOLICIT_EST WHERE SOLICIT=@ID)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INNER JOIN MON M ON S.MON=M.COD" & vbCrLf
sConsulta = sConsulta & "AND S.ID=@ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PER P ON S.COMPRADOR=P.COD" & vbCrLf
sConsulta = sConsulta & "AND S.ID=@ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN DEP D ON P.DEP=D.COD" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN UON1 U1 ON U1.COD=P.UON1" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN UON2 U2 ON U2.UON1=P.UON1 AND P.UON2=U2.COD" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN UON3 U3 ON U3.UON1=P.UON1 AND P.UON2=U3.UON2 AND P.UON3=U3.COD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PER P2 ON SEST.PER=P2.COD" & vbCrLf
sConsulta = sConsulta & "AND S.ID=@ID" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN DEP D2 ON P2.DEP=D2.COD" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN UON1 U21 ON U21.COD=P2.UON1" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN UON2 U22 ON U22.UON1=P2.UON1 AND P2.UON2=U22.COD" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN UON3 U23 ON U23.UON1=P2.UON1 AND P2.UON2=U23.UON2 AND P2.UON3=U23.COD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PER PET ON S.PER=PET.COD AND S.ID=@ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN DEP PET_D ON P.DEP=PET_D.COD" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN UON1 U_PET1 ON U_PET1.COD=PET.UON1" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN UON2 U_PET2 ON U_PET2.UON1=PET.UON1 AND PET.UON2=U_PET2.COD" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN UON3 U_PET3 ON U_PET3.UON1=PET.UON1 AND PET.UON2=U_PET3.UON2 AND PET.UON3=U_PET3.COD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INNER JOIN DEST ON S.DEST=DEST.COD" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PAI ON DEST.PAI=PAI.COD" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PROVI ON PROVI.PAI=PAI.COD AND PROVI.COD=DEST.PROVI" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_DEVOLVER_LINEAS_DE_ORDEN]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_DEVOLVER_LINEAS_DE_ORDEN]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSEP_DEVOLVER_LINEAS_DE_ORDEN(@APROBADOR VARCHAR(50), @ORDENID INTEGER, @SOLICIT int = null )  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(1500)" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if @APROBADOR IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT LP.ID AS LINEAID," & vbCrLf
sConsulta = sConsulta & "                LP.ART_INT + ITEM.ART CODART," & vbCrLf
sConsulta = sConsulta & "                ART4.DEN + ITEM.DESCR  + ART_AUX.DEN DENART," & vbCrLf
sConsulta = sConsulta & "                LP.ART_INT + ITEM.ART + ' ' + ART4.DEN + ITEM.DESCR  + ART_AUX.DEN AS ARTICULO," & vbCrLf
sConsulta = sConsulta & "                PA.COD_EXT ART_EXT," & vbCrLf
sConsulta = sConsulta & "                LP.GMN1," & vbCrLf
sConsulta = sConsulta & "                LP.GMN2," & vbCrLf
sConsulta = sConsulta & "                LP.GMN3," & vbCrLf
sConsulta = sConsulta & "                LP.GMN4," & vbCrLf
sConsulta = sConsulta & "       LP.PROVE," & vbCrLf
sConsulta = sConsulta & "                LP.DEST AS DESTINO," & vbCrLf
sConsulta = sConsulta & "                LP.UP AS UNIDAD," & vbCrLf
sConsulta = sConsulta & "                LP.PREC_UP AS PRECIOUNITARIO," & vbCrLf
sConsulta = sConsulta & "                LP.CANT_PED AS CANTIDAD," & vbCrLf
sConsulta = sConsulta & "                LP.CANT_REC AS CANTIDAD_REC," & vbCrLf
sConsulta = sConsulta & "                ITEM_ADJ.CANT_ADJ / LP.FC AS CANTADJUDICADA," & vbCrLf
sConsulta = sConsulta & "                ITEM_ADJ.CANT_PED / LP.FC AS CANTCONSUMIDA," & vbCrLf
sConsulta = sConsulta & "                LP.PREC_UP * LP.CANT_PED IMPORTE," & vbCrLf
sConsulta = sConsulta & "                LP.EST AS APROBADA," & vbCrLf
sConsulta = sConsulta & "                LP.FECEMISION AS FECHA," & vbCrLf
sConsulta = sConsulta & "                GESTIONABLE=CASE WHEN APROB_LIM.PER= @APROBADOR   THEN 1 WHEN (APROB_LIM.PER IS NULL AND SEGURIDAD.APROB_TIPO1=@APROBADOR ) THEN 1  ELSE 0 END," & vbCrLf
sConsulta = sConsulta & "                LP.CAT1," & vbCrLf
sConsulta = sConsulta & "                LP.CAT2," & vbCrLf
sConsulta = sConsulta & "                LP.CAT3," & vbCrLf
sConsulta = sConsulta & "                LP.CAT4," & vbCrLf
sConsulta = sConsulta & "                LP.CAT5," & vbCrLf
sConsulta = sConsulta & "                LP.OBS," & vbCrLf
sConsulta = sConsulta & "                case when c5.cod is null then " & vbCrLf
sConsulta = sConsulta & "                        case when c4.cod is null then " & vbCrLf
sConsulta = sConsulta & "                             case when c3.cod is null then" & vbCrLf
sConsulta = sConsulta & "                                  case when c2.cod is null then" & vbCrLf
sConsulta = sConsulta & "                                       c1.cod" & vbCrLf
sConsulta = sConsulta & "                                  else" & vbCrLf
sConsulta = sConsulta & "                                  c2.cod" & vbCrLf
sConsulta = sConsulta & "                                  end" & vbCrLf
sConsulta = sConsulta & "                             else" & vbCrLf
sConsulta = sConsulta & "                             c3.cod" & vbCrLf
sConsulta = sConsulta & "                             end" & vbCrLf
sConsulta = sConsulta & "                        else" & vbCrLf
sConsulta = sConsulta & "                        c4.cod" & vbCrLf
sConsulta = sConsulta & "                        end" & vbCrLf
sConsulta = sConsulta & "                    else" & vbCrLf
sConsulta = sConsulta & "                    c5.cod" & vbCrLf
sConsulta = sConsulta & "                    end CATEGORIA," & vbCrLf
sConsulta = sConsulta & "                case when c5.cod is null then " & vbCrLf
sConsulta = sConsulta & "                        case when c4.cod is null then " & vbCrLf
sConsulta = sConsulta & "                             case when c3.cod is null then" & vbCrLf
sConsulta = sConsulta & "                                  case when c2.cod is null then" & vbCrLf
sConsulta = sConsulta & "                                       '(' + c1.cod + ') ' + c1.den" & vbCrLf
sConsulta = sConsulta & "                                  else" & vbCrLf
sConsulta = sConsulta & "                                  '(' + c1.cod + ') ' + c1.den + ' - (' + c2.cod + ') ' + c2.den" & vbCrLf
sConsulta = sConsulta & "                                  end" & vbCrLf
sConsulta = sConsulta & "                             else" & vbCrLf
sConsulta = sConsulta & "                             '(' + c1.cod + ') ' + c1.den + ' - (' + c2.cod + ') ' + c2.den + ' - (' + c3.cod + ') ' + c3.den" & vbCrLf
sConsulta = sConsulta & "                             end" & vbCrLf
sConsulta = sConsulta & "                        else" & vbCrLf
sConsulta = sConsulta & "                        '(' + c1.cod + ') ' + c1.den + ' - (' + c2.cod + ') ' + c2.den + ' - (' + c3.cod + ') ' + c3.den + ' - (' + c4.cod + ') ' + c4.den" & vbCrLf
sConsulta = sConsulta & "                        end" & vbCrLf
sConsulta = sConsulta & "                    else" & vbCrLf
sConsulta = sConsulta & "                    '(' + c1.cod + ') ' + c1.den + ' - (' + c2.cod + ') ' + c2.den + ' - (' + c3.cod + ') ' + c3.den + ' - (' + c4.cod + ') ' + c4.den + ' - (' + c5.cod + ') ' + c5.den" & vbCrLf
sConsulta = sConsulta & "                    end CATEGORIARAMA" & vbCrLf
sConsulta = sConsulta & "FROM LINEAS_PEDIDO LP" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN APROB_LIM" & vbCrLf
sConsulta = sConsulta & "            ON LP.APROB_LIM=APROB_LIM.ID" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN SEGURIDAD " & vbCrLf
sConsulta = sConsulta & "            ON LP.SEGURIDAD=SEGURIDAD.ID" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN ITEM " & vbCrLf
sConsulta = sConsulta & "            ON LP.ANYO = ITEM.ANYO" & vbCrLf
sConsulta = sConsulta & "           AND LP.GMN1=ITEM.GMN1 " & vbCrLf
sConsulta = sConsulta & "           AND LP.PROCE=ITEM.PROCE " & vbCrLf
sConsulta = sConsulta & "           AND LP.ITEM=ITEM.ID" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN ART4 " & vbCrLf
sConsulta = sConsulta & "            ON LP.GMN1=ART4.GMN1 " & vbCrLf
sConsulta = sConsulta & "           AND LP.GMN2=ART4.GMN2 " & vbCrLf
sConsulta = sConsulta & "           AND LP.GMN3=ART4.GMN3 " & vbCrLf
sConsulta = sConsulta & "           AND LP.GMN4=ART4.GMN4 " & vbCrLf
sConsulta = sConsulta & "           AND LP.ART_INT=ART4.COD" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN ITEM_ADJ " & vbCrLf
sConsulta = sConsulta & "            ON LP.ITEM=ITEM_ADJ.ITEM " & vbCrLf
sConsulta = sConsulta & "           AND LP.ANYO = ITEM_ADJ.ANYO" & vbCrLf
sConsulta = sConsulta & "           AND LP.GMN1=ITEM_ADJ.GMN1 " & vbCrLf
sConsulta = sConsulta & "           AND LP.PROCE=ITEM_ADJ.PROCE " & vbCrLf
sConsulta = sConsulta & "           AND LP.PROVE=ITEM_ADJ.PROVE" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN ART4 ART_AUX " & vbCrLf
sConsulta = sConsulta & "            ON ITEM.ART=ART_AUX.COD " & vbCrLf
sConsulta = sConsulta & "           AND ART_AUX.GMN1=ITEM.GMN1 " & vbCrLf
sConsulta = sConsulta & "           AND ART_AUX.GMN2=ITEM.GMN2 " & vbCrLf
sConsulta = sConsulta & "           AND ART_AUX.GMN3=ITEM.GMN3 " & vbCrLf
sConsulta = sConsulta & "           AND ART_AUX.GMN4=ITEM.GMN4 " & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN PROVE_ART4 PA" & vbCrLf
sConsulta = sConsulta & "            ON LP.PROVE = PA.PROVE" & vbCrLf
sConsulta = sConsulta & "           AND LP.GMN1 = PA.GMN1" & vbCrLf
sConsulta = sConsulta & "           AND LP.GMN2 = PA.GMN2" & vbCrLf
sConsulta = sConsulta & "           AND LP.GMN3 = PA.GMN3" & vbCrLf
sConsulta = sConsulta & "           AND LP.GMN4 = PA.GMN4" & vbCrLf
sConsulta = sConsulta & "           AND CASE WHEN LP.ART_INT IS NULL THEN ITEM.ART ELSE LP.ART_INT END = PA.ART" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN CATN1 C1" & vbCrLf
sConsulta = sConsulta & "            ON LP.CAT1 = C1.ID" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN CATN2 C2" & vbCrLf
sConsulta = sConsulta & "            ON LP.CAT2 = C2.ID" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN CATN3 C3" & vbCrLf
sConsulta = sConsulta & "            ON LP.CAT3 = C3.ID" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN CATN4 C4" & vbCrLf
sConsulta = sConsulta & "            ON LP.CAT4 = C4.ID" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN CATN5 C5" & vbCrLf
sConsulta = sConsulta & "            ON LP.CAT5 = C5.ID" & vbCrLf
sConsulta = sConsulta & "WHERE LP.ORDEN=@ORDENID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT LP.ID LINEAID," & vbCrLf
sConsulta = sConsulta & "                LP.ART_INT + I.ART CODART," & vbCrLf
sConsulta = sConsulta & "                LP.GMN1," & vbCrLf
sConsulta = sConsulta & "                LP.GMN2," & vbCrLf
sConsulta = sConsulta & "                LP.GMN3," & vbCrLf
sConsulta = sConsulta & "                LP.GMN4," & vbCrLf
sConsulta = sConsulta & "       LP.PROVE," & vbCrLf
sConsulta = sConsulta & "                ART4.DEN + I.DESCR  + ART_AUX.DEN DENART," & vbCrLf
sConsulta = sConsulta & "                PA.COD_EXT ART_EXT," & vbCrLf
sConsulta = sConsulta & "                LP.DEST DESTINO," & vbCrLf
sConsulta = sConsulta & "                D.DEN DESTDEN," & vbCrLf
sConsulta = sConsulta & "                LP.UP UNIDAD," & vbCrLf
sConsulta = sConsulta & "                U.DEN UNIDEN," & vbCrLf
sConsulta = sConsulta & "                LP.PREC_UP PRECIOUNITARIO," & vbCrLf
sConsulta = sConsulta & "                LP.CANT_PED CANTIDAD," & vbCrLf
sConsulta = sConsulta & "                LP.CANT_REC CANT_REC," & vbCrLf
sConsulta = sConsulta & "                IA.CANT_ADJ * LP.FC AS CANTADJUDICADA," & vbCrLf
sConsulta = sConsulta & "                LP.PREC_UP * LP.CANT_PED IMPORTE," & vbCrLf
sConsulta = sConsulta & "                LP.EST AS APROBADA," & vbCrLf
sConsulta = sConsulta & "                LP.FECEMISION FECHA," & vbCrLf
sConsulta = sConsulta & "                LP.FECENTREGA FECENTREGA," & vbCrLf
sConsulta = sConsulta & "                LP.EST,LP.ENTREGA_OBL,LP.FECENTREGAPROVE," & vbCrLf
sConsulta = sConsulta & "                LP.CAT1," & vbCrLf
sConsulta = sConsulta & "                LP.CAT2," & vbCrLf
sConsulta = sConsulta & "                LP.CAT3," & vbCrLf
sConsulta = sConsulta & "                LP.CAT4," & vbCrLf
sConsulta = sConsulta & "                LP.CAT5," & vbCrLf
sConsulta = sConsulta & "                LP.OBS," & vbCrLf
sConsulta = sConsulta & "                case when c5.cod is null then " & vbCrLf
sConsulta = sConsulta & "                        case when c4.cod is null then " & vbCrLf
sConsulta = sConsulta & "                             case when c3.cod is null then" & vbCrLf
sConsulta = sConsulta & "                                  case when c2.cod is null then" & vbCrLf
sConsulta = sConsulta & "                                       c1.cod" & vbCrLf
sConsulta = sConsulta & "                                  else" & vbCrLf
sConsulta = sConsulta & "                                  c2.cod" & vbCrLf
sConsulta = sConsulta & "                                  end" & vbCrLf
sConsulta = sConsulta & "                             else" & vbCrLf
sConsulta = sConsulta & "                             c3.cod" & vbCrLf
sConsulta = sConsulta & "                             end" & vbCrLf
sConsulta = sConsulta & "                        else" & vbCrLf
sConsulta = sConsulta & "                        c4.cod" & vbCrLf
sConsulta = sConsulta & "                        end" & vbCrLf
sConsulta = sConsulta & "                    else" & vbCrLf
sConsulta = sConsulta & "                    c5.cod" & vbCrLf
sConsulta = sConsulta & "                    end CATEGORIA," & vbCrLf
sConsulta = sConsulta & "                case when c5.cod is null then " & vbCrLf
sConsulta = sConsulta & "                        case when c4.cod is null then " & vbCrLf
sConsulta = sConsulta & "                             case when c3.cod is null then" & vbCrLf
sConsulta = sConsulta & "                                  case when c2.cod is null then" & vbCrLf
sConsulta = sConsulta & "                                       '(' + c1.cod + ') ' + c1.den" & vbCrLf
sConsulta = sConsulta & "                                  else" & vbCrLf
sConsulta = sConsulta & "                                  '(' + c1.cod + ') ' + c1.den + ' - (' + c2.cod + ') ' + c2.den" & vbCrLf
sConsulta = sConsulta & "                                  end" & vbCrLf
sConsulta = sConsulta & "                             else" & vbCrLf
sConsulta = sConsulta & "                             '(' + c1.cod + ') ' + c1.den + ' - (' + c2.cod + ') ' + c2.den + ' - (' + c3.cod + ') ' + c3.den" & vbCrLf
sConsulta = sConsulta & "                             end" & vbCrLf
sConsulta = sConsulta & "                        else" & vbCrLf
sConsulta = sConsulta & "                        '(' + c1.cod + ') ' + c1.den + ' - (' + c2.cod + ') ' + c2.den + ' - (' + c3.cod + ') ' + c3.den + ' - (' + c4.cod + ') ' + c4.den" & vbCrLf
sConsulta = sConsulta & "                        end" & vbCrLf
sConsulta = sConsulta & "                    else" & vbCrLf
sConsulta = sConsulta & "                    '(' + c1.cod + ') ' + c1.den + ' - (' + c2.cod + ') ' + c2.den + ' - (' + c3.cod + ') ' + c3.den + ' - (' + c4.cod + ') ' + c4.den + ' - (' + c5.cod + ') ' + c5.den" & vbCrLf
sConsulta = sConsulta & "                    end CATEGORIARAMA," & vbCrLf
sConsulta = sConsulta & "               " & vbCrLf
sConsulta = sConsulta & "                        " & vbCrLf
sConsulta = sConsulta & "                C1.DEN CAT1DEN," & vbCrLf
sConsulta = sConsulta & "                C2.DEN CAT2DEN," & vbCrLf
sConsulta = sConsulta & "                C3.DEN CAT3DEN," & vbCrLf
sConsulta = sConsulta & "                C4.DEN CAT4DEN," & vbCrLf
sConsulta = sConsulta & "                C5.DEN CAT5DEN," & vbCrLf
sConsulta = sConsulta & "                LE.COMENT" & vbCrLf
sConsulta = sConsulta & " FROM LINEAS_PEDIDO LP" & vbCrLf
sConsulta = sConsulta & "     INNER JOIN DEST D" & vbCrLf
sConsulta = sConsulta & "             ON LP.DEST = D.COD" & vbCrLf
sConsulta = sConsulta & "     INNER JOIN UNI U" & vbCrLf
sConsulta = sConsulta & "             ON LP.UP = U.COD" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN ITEM I " & vbCrLf
sConsulta = sConsulta & "             ON LP.ANYO = I.ANYO" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN1=I.GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND LP.PROCE=I.PROCE " & vbCrLf
sConsulta = sConsulta & "            AND LP.ITEM=I.ID" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN PROVE_ART4 PA" & vbCrLf
sConsulta = sConsulta & "             ON LP.PROVE = PA.PROVE" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN1 = PA.GMN1" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN2 = PA.GMN2" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN3 = PA.GMN3" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN4 = PA.GMN4" & vbCrLf
sConsulta = sConsulta & "            AND CASE WHEN LP.ART_INT IS NULL THEN I.ART ELSE LP.ART_INT END = PA.ART" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN ART4 " & vbCrLf
sConsulta = sConsulta & "             ON LP.GMN1=ART4.GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN2=ART4.GMN2 " & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN3=ART4.GMN3 " & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN4=ART4.GMN4 " & vbCrLf
sConsulta = sConsulta & "            AND LP.ART_INT=ART4.COD" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN ITEM_ADJ IA " & vbCrLf
sConsulta = sConsulta & "             ON LP.ITEM = IA.ITEM " & vbCrLf
sConsulta = sConsulta & "            AND LP.ANYO = IA.ANYO" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN1 = IA.GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND LP.PROCE= IA.PROCE " & vbCrLf
sConsulta = sConsulta & "            AND LP.PROVE= IA.PROVE" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN ART4 ART_AUX " & vbCrLf
sConsulta = sConsulta & "             ON ART_AUX.COD=I.ART " & vbCrLf
sConsulta = sConsulta & "            AND ART_AUX.GMN1=I.GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND ART_AUX.GMN2=I.GMN2 " & vbCrLf
sConsulta = sConsulta & "            AND ART_AUX.GMN3=I.GMN3 " & vbCrLf
sConsulta = sConsulta & "            AND ART_AUX.GMN4=I.GMN4" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN CATN1 C1" & vbCrLf
sConsulta = sConsulta & "             ON LP.CAT1 = C1.ID" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN CATN2 C2" & vbCrLf
sConsulta = sConsulta & "             ON LP.CAT2 = C2.ID" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN CATN3 C3" & vbCrLf
sConsulta = sConsulta & "             ON LP.CAT3 = C3.ID" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN CATN4 C4" & vbCrLf
sConsulta = sConsulta & "             ON LP.CAT4 = C4.ID" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN CATN5 C5" & vbCrLf
sConsulta = sConsulta & "             ON LP.CAT5 = C5.ID" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN LINEAS_EST LE" & vbCrLf
sConsulta = sConsulta & "             ON LP.LINEAS_EST = LE.ID" & vbCrLf
sConsulta = sConsulta & " WHERE LP.ORDEN=@ORDENID" & vbCrLf
sConsulta = sConsulta & "   AND CASE WHEN LP.SOLICIT IS NULL AND @SOLICIT IS NULL THEN -1 ELSE LP.SOLICIT END " & vbCrLf
sConsulta = sConsulta & "       = CASE WHEN LP.SOLICIT IS NULL AND @SOLICIT IS NULL THEN -1 ELSE ISNULL(@SOLICIT,LP.SOLICIT) END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Function

Public Function CodigoDeActualizacion2_10_50_02A2_10_50_03() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim bBool As Boolean

On Error GoTo error
    

sConsulta = "BEGIN TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = True
ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

frmProgreso.lblDetalle = "Modificamos tabla de log"
frmProgreso.lblDetalle.Refresh
frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

'Eliminar FK de BLOQUEO_PROCE
sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[FK_BLOQUEO_PROCE_PROCE]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1) ALTER TABLE [dbo].[BLOQUEO_PROCE] DROP CONSTRAINT [FK_BLOQUEO_PROCE_PROCE]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[BLOQUEO_PROCE_FK_PROCE]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1) ALTER TABLE [dbo].[BLOQUEO_PROCE] DROP CONSTRAINT [BLOQUEO_PROCE_FK_PROCE]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GMN1_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[GMN1_COD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE GMN1_COD (@OLD VARCHAR(50),@NEW VARCHAR(50))  AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT ON" & vbCrLf
sConsulta = sConsulta & "BEGIN TRANSACTION" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR FOR SELECT COD FROM GMN1 WHERE COD=@OLD" & vbCrLf
sConsulta = sConsulta & "OPEN C" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C INTO @VAR_COD" & vbCrLf
sConsulta = sConsulta & "IF @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART1_ADJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART2_ADJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART3_ADJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ADJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE DETALLE_PUJAS NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ADJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ADJREU NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ESP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OBJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OFE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OFE_WEB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRES NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESCON NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESPROY NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESCON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESCON4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFEMIN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_GRUPO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_ESP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_WEB_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_WEB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PER NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_PET NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_POT NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVENOTIF NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE REU_PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ESP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4_ACT NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATALOG_LIN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_PEDIDO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART_UP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_DEF NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO_DEF NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO_ESP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_GR_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_GR_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ITEM_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ITEM_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GRUPO_OFE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GRUPO_OFE_REU NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USAR_GR_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_CONTR NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN1 SET COD=@NEW WHERE COD=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART1_ADJ SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART2_ADJ SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART3_ADJ SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4_ADJ SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE BLOQUEO_PROCE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE COM_GMN1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE COM_GMN2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE COM_GMN3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE COM_GMN4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE DETALLE_PUJAS SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_ADJ SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_ADJREU SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_ESP SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_OBJ SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_OFE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_OFE_WEB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_PRES SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_PRESCON SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_PRESCON3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_PRESCON4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_PRESPROY SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_UON1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_UON2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_UON3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFEMIN SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES_MAT1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES_MAT2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES_MAT3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES_MAT4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_ESP SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE_ADJUN SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE_WEB_ADJUN SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE_WEB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PER SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE_PET SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE_POT SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_GMN1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_GMN2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_GMN3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_GMN4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVENOTIF SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE REU_PROCE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_PROCE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_PROVE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_GRUPO SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_UON1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_UON2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_UON3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_PRESCON3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_PRESCON4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_PROVE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4_ESP  SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN4_ACT SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART4_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN4_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4_ATRIB  SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CATALOG_LIN  SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE LINEAS_PEDIDO  SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART_UP  SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_DEF SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GRUPO SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GRUPO_DEF SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GRUPO_ESP SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PRES1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PRES2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PRES3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PRES4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_PRES1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_PRES2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_PRES3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_PRES4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_UON1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_UON2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_UON3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_UON1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_UON2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_UON3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_GR_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_GR_ADJUN SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_ITEM_ADJUN SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_ITEM_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ALL SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ALL_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ALL_PROVE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_GRUPO SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_GRUPO_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_GRUPO_PROVE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ITEM SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ITEM_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ITEM_PROVE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_PROCE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_PROCE_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_PROCE_PROVE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GRUPO_OFE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GRUPO_OFE_REU SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE USAR_GR_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_CONTR SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART1_ADJ CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART2_ADJ CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART3_ADJ CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ADJ CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE DETALLE_PUJAS CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ADJ CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ADJREU CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ESP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OBJ CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OFE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OFE_WEB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRES CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESCON CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESCON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESCON4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESPROY CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFEMIN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_ESP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_WEB_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_WEB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PER CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_PET CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_POT CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVENOTIF CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE REU_PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_GRUPO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ESP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4_ACT CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATALOG_LIN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_PEDIDO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART_UP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_DEF CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO_DEF CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO_ESP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_GR_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_GR_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ITEM_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ITEM_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GRUPO_OFE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GRUPO_OFE_REU CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USAR_GR_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_CONTR CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close C" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "COMMIT TRANSACTION" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT OFF" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PROCE_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[PROCE_COD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE PROCE_COD (@OLD SMALLINT,@NEW SMALLINT,@ANYO INT,@GMN1 VARCHAR (50))  AS " & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_COD SMALLINT " & vbCrLf
sConsulta = sConsulta & "DECLARE @AN AS INT " & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN AS VARCHAR(50) " & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT ON " & vbCrLf
sConsulta = sConsulta & "BEGIN TRANSACTION " & vbCrLf
sConsulta = sConsulta & "BEGIN " & vbCrLf
sConsulta = sConsulta & "DECLARE D CURSOR FOR SELECT ANYO,GMN1,COD FROM PROCE WHERE ANYO=@ANYO AND COD=@OLD AND GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "OPEN D " & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM D INTO @AN,@GMN,@VAR_COD " & vbCrLf
sConsulta = sConsulta & "IF @@FETCH_STATUS=0 " & vbCrLf
sConsulta = sConsulta & "BEGIN " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART1_ADJ NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART2_ADJ NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART3_ADJ NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ADJ NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ADJ NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ADJREU NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ESP NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OBJ NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OFE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OFE_WEB NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRES NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESCON NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESCON3 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESCON4 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESPROY NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON1 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON2 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON3 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFEMIN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE DETALLE_PUJAS NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVENOTIF NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATALOG_LIN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_PEDIDO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_ESP NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_ADJUN NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_WEB_ADJUN NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_WEB NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PER NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_PET NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_POT NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE REU_PROCE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES1 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES2 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES3 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES4 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON1 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON2 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON3 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO_ESP NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO_DEF NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES1 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES2 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES3 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES4 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON1 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON2 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON3 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_ATRIB NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_DEF NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL_ATRIB NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL_PROVE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_ATRIB NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_PROVE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_ATRIB NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_PROVE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_ATRIB NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_PROVE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON1 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON2 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON3 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON4 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY1 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY2 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY3 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY4 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON1 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON2 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON3 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON3 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON4 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PROVE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_GRUPO NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROCE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROVE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GRUPO_OFE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GRUPO_OFE_REU NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_GR_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ITEM_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_GR_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ITEM_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USAR_GR_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_CONTR NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE ART1_ADJ SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ART2_ADJ SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ART3_ADJ SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ART4_ADJ SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_ADJ SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_ADJREU SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_ESP SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_OBJ SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_OFE SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_OFE_WEB SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_PRES SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_UON1 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_UON2 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_UON3 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_PRESCON SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_PRESCON3 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_PRESCON4 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_PRESPROY SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE OFEMIN SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE DETALLE_PUJAS SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVENOTIF SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CATALOG_LIN SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE LINEAS_PEDIDO SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_ESP SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE_ADJUN SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE_WEB_ADJUN SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE_WEB SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PER SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE_PET SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE_POT SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE REU_PROCE SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_PRES1 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_PRES2 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_PRES3 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_PRES4 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_UON1 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_UON2 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_UON3 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GRUPO_ESP SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GRUPO_DEF SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GRUPO SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PRES1 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PRES2 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PRES3 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PRES4 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_UON1 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_UON2 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_UON3 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_ATRIB SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_DEF SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ALL SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ALL_ATRIB SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ALL_PROVE SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_PROCE SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_PROCE_ATRIB SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_PROCE_PROVE SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_GRUPO SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_GRUPO_ATRIB SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_GRUPO_PROVE SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ITEM SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ITEM_ATRIB SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ITEM_PROVE SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON1 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON2 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON3 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON4 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY1 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY2 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY3 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY4 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_1 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_2 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_3 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_4 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_1 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_2 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_3 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_4 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_UON1 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_UON2 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD  " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_UON3 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_PRESCON3 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_PRESCON4 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_PROVE SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_GRUPO SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_PROVE SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_PROCE SET COD=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE GRUPO_OFE SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GRUPO_OFE_REU SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_ATRIB SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_GR_ATRIB SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_ITEM_ATRIB SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_GR_ADJUN SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_ITEM_ADJUN SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GRUPO SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET COD=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE BLOQUEO_PROCE SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE USAR_GR_ATRIB SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_CONTR SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART1_ADJ CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART2_ADJ CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART3_ADJ CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ADJ CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ADJ CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESCON CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESCON3 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESCON4 CHECK CONSTRAINT ALL  " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESPROY CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ADJREU CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ESP CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OBJ CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OFE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OFE_WEB CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRES CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON1 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON2 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON3 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFEMIN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE DETALLE_PUJAS CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVENOTIF CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATALOG_LIN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_PEDIDO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_ESP CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_ADJUN CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_WEB_ADJUN CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_WEB CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PER CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_PET CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_POT CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE REU_PROCE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES1 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES2 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES3 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES4 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON1 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON2 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON3 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO_ESP CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO_DEF CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES1 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES2 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES3 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES4 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON1 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON2 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON3 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_ATRIB CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_DEF CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL_ATRIB CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL_PROVE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_ATRIB CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_PROVE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_ATRIB CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_PROVE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_ATRIB CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_PROVE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON1 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON2 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON3 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON4 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY1 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY2 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY3 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY4 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON1 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON2 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON3 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON3 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON4 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PROVE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_GRUPO CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROCE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROVE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GRUPO_OFE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GRUPO_OFE_REU CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_GR_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ITEM_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_GR_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ITEM_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USAR_GR_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_CONTR CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "End " & vbCrLf
sConsulta = sConsulta & "Close D " & vbCrLf
sConsulta = sConsulta & "DEALLOCATE D " & vbCrLf
sConsulta = sConsulta & "End " & vbCrLf
sConsulta = sConsulta & "COMMIT TRANSACTION" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT OFF" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "UPDATE VERSION SET NUM ='2.10.50.03'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "COMMIT TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = False

CodigoDeActualizacion2_10_50_02A2_10_50_03 = True
Exit Function
    
error:

    If bTransaccionEnCurso Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_10_50_02A2_10_50_03 = False

End Function


Public Function CodigoDeActualizacion2_10_50_03A2_10_50_04() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim bBool As Boolean

On Error GoTo error
    

sConsulta = "BEGIN TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = True
ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

frmProgreso.lblDetalle = "Modificamos stored procedures"
frmProgreso.lblDetalle.Refresh
frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_10_5_Storeds_v4


sConsulta = "UPDATE VERSION SET NUM ='2.10.50.04'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "COMMIT TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = False

CodigoDeActualizacion2_10_50_03A2_10_50_04 = True
Exit Function
    
error:

    If bTransaccionEnCurso Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_10_50_03A2_10_50_04 = False

End Function



