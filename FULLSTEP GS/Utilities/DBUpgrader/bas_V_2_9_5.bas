Attribute VB_Name = "bas_V_2_9_5"
Option Explicit

Dim AdmUsu As String

Public Function CodigoDeActualizacion2_9_00_9A2_9_50_0() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String

On Error GoTo error
    
Dim rdores As rdoResultset

sConsulta = "SELECT USU FROM ADM"
Set rdores = gRDOCon.OpenResultset(sConsulta, rdOpenForwardOnly, rdConcurReadOnly)

AdmUsu = rdores("USU").Value

rdores.Close
Set rdores = Nothing

    
sConsulta = "BEGIN TRANSACTION"
gRDOCon.Execute sConsulta, rdExecDirect

gRDOCon.Execute "SET XACT_ABORT OFF", rdExecDirect
bTransaccionEnCurso = True

'METEMOS LAS NUEVAS TABLAS PARA LA VERSI�N 2.9.5

frmProgreso.lblDetalle = "Creamos las nuevas tablas de la versi�n 2.9.5"
frmProgreso.lblDetalle.Refresh
frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    
V_2_9_5_TABLAS


frmProgreso.lblDetalle = "Cambios en las tablas actuales"
frmProgreso.lblDetalle.Refresh
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    
V2_9_5_Cambios_Tablas

frmProgreso.lblDetalle = "Crear Stored Procedures"
frmProgreso.lblDetalle.Refresh
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
V2_9_5_Crear_Storeds

frmProgreso.lblDetalle = "Modificar Stored Procedures"
frmProgreso.lblDetalle.Refresh
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
V2_9_5_Modif_Storeds

sConsulta = "UPDATE VERSION SET NUM ='2.9.50.0'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
gRDOCon.Execute sConsulta, rdExecDirect

gRDOCon.Execute "COMMIT TRANSACTION", rdExecDirect
bTransaccionEnCurso = False

CodigoDeActualizacion2_9_00_9A2_9_50_0 = True
Exit Function
    
error:
'Resume Next

    If bTransaccionEnCurso Then
        gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_9_00_9A2_9_50_0 = False

End Function


Public Function CodigoDeActualizacion2_9_50_0A2_9_50_1() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim rdores As rdoResultset

On Error GoTo error
    

sConsulta = "BEGIN TRANSACTION"
gRDOCon.Execute sConsulta, rdExecDirect

gRDOCon.Execute "SET XACT_ABORT OFF", rdExecDirect
bTransaccionEnCurso = True

frmProgreso.lblDetalle = "Modificamos Tabla CON"
frmProgreso.lblDetalle.Refresh
frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
V_2_9_5_triggers_50_1

frmProgreso.lblDetalle = "Modificamos stored procedures"
frmProgreso.lblDetalle.Refresh
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
V_2_9_5_Storeds_50_1

'Ponemos un contacto de aprovisionamiento a los proves del portal
sConsulta = "UPDATE CON SET APROV = 1 WHERE ID IN (SELECT MIN(C2.ID) FROM CON C2 WHERE C2.PROVE=CON.PROVE AND C2.PORT=1)"
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = "UPDATE VERSION SET NUM ='2.9.50.1'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
gRDOCon.Execute sConsulta, rdExecDirect

gRDOCon.Execute "COMMIT TRANSACTION", rdExecDirect
bTransaccionEnCurso = False

CodigoDeActualizacion2_9_50_0A2_9_50_1 = True
Exit Function
    
error:
'Resume Next

    If bTransaccionEnCurso Then
        gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_9_50_0A2_9_50_1 = False

End Function






Public Function CodigoDeActualizacion2_9_50_1A2_9_50_2() As Boolean
    Dim oADOConnection As ADODB.Connection
    Dim sConsulta As String
    Dim adores As ADODB.Recordset
    Dim sFSP_SRV As String
    Dim sFSP_BD As String
    Dim sFSP_CIA As String
    Dim iCIA As Long
    Dim sPORT_CON As String
        
    On Error GoTo ErrorDeConexion
    
    Set oADOConnection = New ADODB.Connection
    oADOConnection.Open "Provider=SQLOLEDB.1;Auto Translate=FALSE;Server=" & gServer & ";Database=" & gBD & ";", gUID, gPWD
    oADOConnection.CursorLocation = adUseClient
    oADOConnection.CommandTimeout = 120
    
    oADOConnection.Execute "BEGIN DISTRIBUTED TRAN"
    oADOConnection.Execute "SET XACT_ABORT ON"
    
    Set adores = New ADODB.Recordset
    adores.Open "SELECT INSTWEB FROM PARGEN_INTERNO", oADOConnection, adOpenForwardOnly, adLockReadOnly
    If adores.EOF Then
        adores.Close
        Set adores = Nothing
        oADOConnection.Execute "ROLLBACK TRAN"
        oADOConnection.Execute "SET XACT_ABORT OFF"
        sConsulta = "UPDATE VERSION SET NUM ='2.9.50.2'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
        oADOConnection.Execute sConsulta
        oADOConnection.Close
        Set oADOConnection = Nothing
        CodigoDeActualizacion2_9_50_1A2_9_50_2 = True
        Exit Function
    End If
        
    If adores(0).Value <> 2 Then
        adores.Close
        Set adores = Nothing
        oADOConnection.Execute "ROLLBACK TRAN"
        oADOConnection.Execute "SET XACT_ABORT OFF"
        sConsulta = "UPDATE VERSION SET NUM ='2.9.50.2'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
        oADOConnection.Execute sConsulta
        oADOConnection.Close
        Set oADOConnection = Nothing
        CodigoDeActualizacion2_9_50_1A2_9_50_2 = True
        Exit Function
    End If
    
    adores.Close
    adores.Open "SELECT FSP_SRV,FSP_BD,FSP_CIA FROM PARGEN_PORT", oADOConnection, adOpenForwardOnly, adLockReadOnly
    If adores.EOF Then
        adores.Close
        Set adores = Nothing
        oADOConnection.Execute "ROLLBACK TRAN"
        oADOConnection.Execute "SET XACT_ABORT OFF"
        sConsulta = "UPDATE VERSION SET NUM ='2.9.50.2'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
        oADOConnection.Execute sConsulta
        oADOConnection.Close
        Set oADOConnection = Nothing
        CodigoDeActualizacion2_9_50_1A2_9_50_2 = True
        Exit Function
    End If
    
    If IsNull(adores(0).Value) Or IsNull(adores(1).Value) Or IsNull(adores(2).Value) Then
        adores.Close
        Set adores = Nothing
        oADOConnection.Execute "ROLLBACK TRAN"
        oADOConnection.Execute "SET XACT_ABORT OFF"
        sConsulta = "UPDATE VERSION SET NUM ='2.9.50.2'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
        oADOConnection.Execute sConsulta
        oADOConnection.Close
        Set oADOConnection = Nothing
        CodigoDeActualizacion2_9_50_1A2_9_50_2 = True
        Exit Function
    End If
                    
    sFSP_SRV = adores(0).Value
    sFSP_BD = adores(1).Value
    sFSP_CIA = adores(2).Value
    adores.Close
    sPORT_CON = sFSP_SRV & "." & sFSP_BD & "." & "dbo."
                    
    sConsulta = "SELECT CIAS.ID FROM " & sPORT_CON & "CIAS CIAS WHERE CIAS.COD ='" & DblQuote(sFSP_CIA) & "'"
    adores.Open sConsulta, oADOConnection, adOpenForwardOnly, adLockReadOnly
    If adores.EOF Then
        adores.Close
        Set adores = Nothing
        oADOConnection.Execute "ROLLBACK TRAN"
        oADOConnection.Execute "SET XACT_ABORT OFF"
        oADOConnection.Close
        Set oADOConnection = Nothing
        MsgBox "La compa��a no existe en el portal."
        CodigoDeActualizacion2_9_50_1A2_9_50_2 = False
        Exit Function
    End If
    
    iCIA = adores(0).Value
    adores.Close
    sConsulta = "UPDATE CON SET ID_PORT= USU.ID FROM " & sPORT_CON & "REL_CIAS PROVE_P INNER JOIN  " & sPORT_CON & "USU USU ON PROVE_P.CIA_PROVE = USU.CIA "
    sConsulta = sConsulta & " INNER JOIN " & sPORT_CON & "USU_PORT USU_PORT ON USU.CIA =USU_PORT.CIA AND USU.ID =USU_PORT.USU"
    sConsulta = sConsulta & " WHERE PROVE_P.CIA_COMP =" & iCIA & " AND  USU_PORT.FPEST=3 AND CON.EMAIL=USU.EMAIL AND CON.PROVE=PROVE_P.COD_PROVE_CIA AND CON.PORT=1"
    oADOConnection.Execute sConsulta
    
    Set adores = Nothing
    sConsulta = "UPDATE VERSION SET NUM ='2.9.50.2'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    oADOConnection.Execute sConsulta
    
    oADOConnection.Execute "COMMIT TRAN"
    oADOConnection.Execute "SET XACT_ABORT OFF"
    oADOConnection.Close
    Set oADOConnection = Nothing
    CodigoDeActualizacion2_9_50_1A2_9_50_2 = True
    Exit Function
        
    
ErrorDeConexion:
    MsgBox oADOConnection.Errors(0).NativeError & "  " & oADOConnection.Errors(0).Description, vbCritical, "FULLSTEP"
    oADOConnection.Execute "SET XACT_ABORT OFF"
    On Error Resume Next
    adores.Close
    Set adores = Nothing
    oADOConnection.Close
    Set oADOConnection = Nothing
    CodigoDeActualizacion2_9_50_1A2_9_50_2 = False
End Function

Public Function CodigoDeActualizacion2_9_50_2A2_9_50_3() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim rdores As rdoResultset

On Error GoTo error
    

sConsulta = "BEGIN TRANSACTION"
gRDOCon.Execute sConsulta, rdExecDirect

gRDOCon.Execute "SET XACT_ABORT OFF", rdExecDirect
bTransaccionEnCurso = True

frmProgreso.lblDetalle = "Modificamos Tabla PROCE"
frmProgreso.lblDetalle.Refresh
frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
sConsulta = "ALTER TABLE [dbo].[PROCE] WITH NOCHECK ADD CONSTRAINT [CK_PROCE] CHECK ([CAMBIO] <> 0)"
gRDOCon.Execute sConsulta, rdExecDirect

frmProgreso.lblDetalle = "Modificamos Tabla GMN4_ACT"
frmProgreso.lblDetalle.Refresh
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
sConsulta = "ALTER TABLE dbo.GMN4_ACT ALTER COLUMN ACT2 int NULL" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = "ALTER TABLE dbo.GMN4_ACT ALTER COLUMN ACT3 int NULL" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = "ALTER TABLE dbo.GMN4_ACT ALTER COLUMN ACT4 int NULL" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect


frmProgreso.lblDetalle = "Modificamos TRIGGERS"
frmProgreso.lblDetalle.Refresh
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
V_2_9_5_triggers_50_3

frmProgreso.lblDetalle = "Modificamos STORED PROCEDURES"
frmProgreso.lblDetalle.Refresh
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
V_2_9_5_Storeds_50_3

sConsulta = "UPDATE VERSION SET NUM ='2.9.50.3'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
gRDOCon.Execute sConsulta, rdExecDirect

gRDOCon.Execute "COMMIT TRANSACTION", rdExecDirect
bTransaccionEnCurso = False

CodigoDeActualizacion2_9_50_2A2_9_50_3 = True
Exit Function
    
error:
'Resume Next

    If bTransaccionEnCurso Then
        gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_9_50_2A2_9_50_3 = False

End Function

Public Function CodigoDeActualizacion2_9_50_3A2_9_50_4() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim rdores As rdoResultset

On Error GoTo error
    

sConsulta = "BEGIN TRANSACTION"
gRDOCon.Execute sConsulta, rdExecDirect

gRDOCon.Execute "SET XACT_ABORT OFF", rdExecDirect
bTransaccionEnCurso = True

frmProgreso.lblDetalle = "Modificamos STORED PROCEDURES"
frmProgreso.lblDetalle.Refresh
frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_9_5_Storeds_50_4

sConsulta = "UPDATE VERSION SET NUM ='2.9.50.4'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
gRDOCon.Execute sConsulta, rdExecDirect

gRDOCon.Execute "COMMIT TRANSACTION", rdExecDirect
bTransaccionEnCurso = False

CodigoDeActualizacion2_9_50_3A2_9_50_4 = True
Exit Function
    
error:
'Resume Next

    If bTransaccionEnCurso Then
        gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_9_50_3A2_9_50_4 = False

End Function



Public Function CodigoDeActualizacion2_9_50_4A2_9_50_5() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim rdores As rdoResultset

On Error GoTo error
    

sConsulta = "BEGIN TRANSACTION"
gRDOCon.Execute sConsulta, rdExecDirect

gRDOCon.Execute "SET XACT_ABORT OFF", rdExecDirect
bTransaccionEnCurso = True

frmProgreso.lblDetalle = "Modificamos STORED PROCEDURES"
frmProgreso.lblDetalle.Refresh
frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_9_5_Storeds_50_5

sConsulta = "UPDATE VERSION SET NUM ='2.9.50.5'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
gRDOCon.Execute sConsulta, rdExecDirect

gRDOCon.Execute "COMMIT TRANSACTION", rdExecDirect
bTransaccionEnCurso = False

CodigoDeActualizacion2_9_50_4A2_9_50_5 = True
Exit Function
    
error:
'Resume Next

    If bTransaccionEnCurso Then
        gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_9_50_4A2_9_50_5 = False

End Function

Public Function CodigoDeActualizacion2_9_50_5A2_9_50_6() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim rdores As rdoResultset

On Error GoTo error
    

sConsulta = "BEGIN TRANSACTION"
gRDOCon.Execute sConsulta, rdExecDirect

gRDOCon.Execute "SET XACT_ABORT OFF", rdExecDirect
bTransaccionEnCurso = True

frmProgreso.lblDetalle = "Modificamos STORED PROCEDURES"
frmProgreso.lblDetalle.Refresh
frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_9_5_Storeds_50_6

sConsulta = "UPDATE VERSION SET NUM ='2.9.50.6'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
gRDOCon.Execute sConsulta, rdExecDirect

gRDOCon.Execute "COMMIT TRANSACTION", rdExecDirect
bTransaccionEnCurso = False

CodigoDeActualizacion2_9_50_5A2_9_50_6 = True
Exit Function
    
error:
'Resume Next

    If bTransaccionEnCurso Then
        gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_9_50_5A2_9_50_6 = False

End Function


Public Function CodigoDeActualizacion2_9_50_6A2_9_50_7() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim rdores As rdoResultset

On Error GoTo error
    

sConsulta = "BEGIN TRANSACTION"
gRDOCon.Execute sConsulta, rdExecDirect

gRDOCon.Execute "SET XACT_ABORT OFF", rdExecDirect
bTransaccionEnCurso = True

frmProgreso.lblDetalle = "Modificamos STORED PROCEDURES"
frmProgreso.lblDetalle.Refresh
frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_9_5_Storeds_50_7

sConsulta = "UPDATE VERSION SET NUM ='2.9.50.7'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
gRDOCon.Execute sConsulta, rdExecDirect

gRDOCon.Execute "COMMIT TRANSACTION", rdExecDirect
bTransaccionEnCurso = False

CodigoDeActualizacion2_9_50_6A2_9_50_7 = True
Exit Function
    
error:
'Resume Next

    If bTransaccionEnCurso Then
        gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_9_50_6A2_9_50_7 = False

End Function

Public Function CodigoDeActualizacion2_9_50_7A2_9_50_8() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim rdores As rdoResultset

On Error GoTo error
    

sConsulta = "BEGIN TRANSACTION"
gRDOCon.Execute sConsulta, rdExecDirect

gRDOCon.Execute "SET XACT_ABORT OFF", rdExecDirect
bTransaccionEnCurso = True

frmProgreso.lblDetalle = "Modificamos STORED PROCEDURES"
frmProgreso.lblDetalle.Refresh
frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_9_5_Storeds_50_8

sConsulta = "UPDATE VERSION SET NUM ='2.9.50.8'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
gRDOCon.Execute sConsulta, rdExecDirect

gRDOCon.Execute "COMMIT TRANSACTION", rdExecDirect
bTransaccionEnCurso = False

CodigoDeActualizacion2_9_50_7A2_9_50_8 = True
Exit Function
    
error:
'Resume Next

    If bTransaccionEnCurso Then
        gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_9_50_7A2_9_50_8 = False

End Function


Public Function CodigoDeActualizacion2_9_50_8A2_9_50_9() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim rdores As rdoResultset

On Error GoTo error
    

sConsulta = "BEGIN TRANSACTION"
gRDOCon.Execute sConsulta, rdExecDirect

gRDOCon.Execute "SET XACT_ABORT OFF", rdExecDirect
bTransaccionEnCurso = True

frmProgreso.lblDetalle = "Modificamos TRIGGERS en PROCE_PROVE_PET"
frmProgreso.lblDetalle.Refresh
frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_9_5_Storeds_50_9

sConsulta = "UPDATE VERSION SET NUM ='2.9.50.9'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
gRDOCon.Execute sConsulta, rdExecDirect

gRDOCon.Execute "COMMIT TRANSACTION", rdExecDirect
bTransaccionEnCurso = False

CodigoDeActualizacion2_9_50_8A2_9_50_9 = True
Exit Function
    
error:

    If bTransaccionEnCurso Then
        gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_9_50_8A2_9_50_9 = False

End Function


Private Sub V_2_9_5_TABLAS()
Dim sConsulta As String

'PROVE_ESP_TG_INSUPD
sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PROVE_ESP_TG_INSUPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PROVE_ESP_TG_INSUPD]" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PROVE_ESP]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)" & vbCrLf
sConsulta = sConsulta & "drop table [dbo].[PROVE_ESP]" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE [dbo].[PROVE_ESP] (" & vbCrLf
sConsulta = sConsulta & " [PROVE] [varchar] (" & gLongitudesDeCodigos.giLongCodPROVE & ") NOT NULL ," & vbCrLf
sConsulta = sConsulta & " [ID] [smallint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & " [NOM] [varchar] (300)  NOT NULL ," & vbCrLf
sConsulta = sConsulta & " [COM] [varchar] (500)  NULL ," & vbCrLf
sConsulta = sConsulta & " [DATA] [image] NULL ," & vbCrLf
sConsulta = sConsulta & " [FECACT] [datetime] NULL " & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[PROVE_ESP] WITH NOCHECK ADD " & vbCrLf
sConsulta = sConsulta & " CONSTRAINT [PK_PROVE_ESP] PRIMARY KEY  CLUSTERED " & vbCrLf
sConsulta = sConsulta & " (" & vbCrLf
sConsulta = sConsulta & "     [PROVE]," & vbCrLf
sConsulta = sConsulta & "     [ID]" & vbCrLf
sConsulta = sConsulta & " )  ON [PRIMARY] " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[PROVE_ESP] ADD " & vbCrLf
sConsulta = sConsulta & " CONSTRAINT [FK_PROVE_ESP_PROVE] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & " (" & vbCrLf
sConsulta = sConsulta & "     [PROVE]" & vbCrLf
sConsulta = sConsulta & " ) REFERENCES [dbo].[PROVE] (" & vbCrLf
sConsulta = sConsulta & "     [COD]" & vbCrLf
sConsulta = sConsulta & " )" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER ON " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER PROVE_ESP_TG_INSUPD ON [dbo].[PROVE_ESP] " & vbCrLf
sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVE AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @ESP AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_PROVE_ESP_Ins CURSOR FOR SELECT PROVE,ID FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_PROVE_ESP_Ins" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROVE_ESP_Ins INTO @PROVE,@ESP" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ESP SET FECACT=GETDATE() WHERE PROVE=@PROVE AND ID=@ESP" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROVE_ESP_Ins INTO @PROVE,@ESP" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_PROVE_ESP_Ins" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_PROVE_ESP_Ins" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect


'PROVE_ESP_PORTAL
sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PROVE_ESP_PORTAL]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)" & vbCrLf
sConsulta = sConsulta & "drop table [dbo].[PROVE_ESP_PORTAL]" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE [dbo].[PROVE_ESP_PORTAL] (" & vbCrLf
sConsulta = sConsulta & " [PROVE] [varchar] (" & gLongitudesDeCodigos.giLongCodPROVE & ")  NOT NULL ," & vbCrLf
sConsulta = sConsulta & " [ID] [smallint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & " [NOM] [varchar] (300)  NOT NULL ," & vbCrLf
sConsulta = sConsulta & " [COM] [varchar] (500)  NULL ," & vbCrLf
sConsulta = sConsulta & " [FECHA] [datetime] NULL " & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[PROVE_ESP_PORTAL] WITH NOCHECK ADD " & vbCrLf
sConsulta = sConsulta & " CONSTRAINT [PK_PROVE_ESP_PORTAL] PRIMARY KEY  CLUSTERED " & vbCrLf
sConsulta = sConsulta & " (" & vbCrLf
sConsulta = sConsulta & "     [PROVE]," & vbCrLf
sConsulta = sConsulta & "     [ID]" & vbCrLf
sConsulta = sConsulta & " )  ON [PRIMARY] " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[PROVE_ESP_PORTAL] ADD " & vbCrLf
sConsulta = sConsulta & " CONSTRAINT [FK_PROVE_ESP_PORTAL_PROVE] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & " (" & vbCrLf
sConsulta = sConsulta & "     [PROVE]" & vbCrLf
sConsulta = sConsulta & " ) REFERENCES [dbo].[PROVE] (" & vbCrLf
sConsulta = sConsulta & "     [COD]" & vbCrLf
sConsulta = sConsulta & " )" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect


'UON0_DEST
sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[UON0_DEST]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)" & vbCrLf
sConsulta = sConsulta & "drop table [dbo].[UON0_DEST]" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE [dbo].[UON0_DEST] (" & vbCrLf
sConsulta = sConsulta & " [DEST] [varchar] (" & gLongitudesDeCodigos.giLongCodDEST & ")  NOT NULL " & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & " CREATE  CLUSTERED  INDEX [TEMP_PK_UON0_DEST] ON [dbo].[UON0_DEST]([DEST]) ON [PRIMARY]" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[UON0_DEST] WITH NOCHECK ADD " & vbCrLf
sConsulta = sConsulta & " CONSTRAINT [PK_UON0_DEST] PRIMARY KEY  NONCLUSTERED " & vbCrLf
sConsulta = sConsulta & " (" & vbCrLf
sConsulta = sConsulta & "     [DEST]" & vbCrLf
sConsulta = sConsulta & " )  ON [PRIMARY] " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & " CREATE  INDEX [IX_UON0_DEST] ON [dbo].[UON0_DEST]([DEST]) ON [PRIMARY]" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[UON0_DEST] ADD " & vbCrLf
sConsulta = sConsulta & " CONSTRAINT [UON0_DEST_FK_DEST] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & " (" & vbCrLf
sConsulta = sConsulta & "     [DEST]" & vbCrLf
sConsulta = sConsulta & " ) REFERENCES [dbo].[DEST] (" & vbCrLf
sConsulta = sConsulta & "     [COD]" & vbCrLf
sConsulta = sConsulta & " )" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect


'UON1_DEST
sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[UON1_DEST]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)" & vbCrLf
sConsulta = sConsulta & "drop table [dbo].[UON1_DEST]" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE [dbo].[UON1_DEST] (" & vbCrLf
sConsulta = sConsulta & " [UON1] [varchar] (" & gLongitudesDeCodigos.giLongCodUON1 & ")  NOT NULL ," & vbCrLf
sConsulta = sConsulta & " [DEST] [varchar] (" & gLongitudesDeCodigos.giLongCodDEST & ")  NOT NULL " & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & " CREATE  CLUSTERED  INDEX [TEMP_PK_UON1_DEST] ON [dbo].[UON1_DEST]([UON1], [DEST]) ON [PRIMARY]" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[UON1_DEST] WITH NOCHECK ADD " & vbCrLf
sConsulta = sConsulta & " CONSTRAINT [PK_UON1_DEST] PRIMARY KEY  NONCLUSTERED " & vbCrLf
sConsulta = sConsulta & " (" & vbCrLf
sConsulta = sConsulta & "     [UON1]," & vbCrLf
sConsulta = sConsulta & "     [DEST]" & vbCrLf
sConsulta = sConsulta & " )  ON [PRIMARY] " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & " CREATE  INDEX [IX_UON1_DEST_01] ON [dbo].[UON1_DEST]([DEST]) ON [PRIMARY]" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[UON1_DEST] ADD " & vbCrLf
sConsulta = sConsulta & " CONSTRAINT [UON1_DEST_FK_DEST] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & " (" & vbCrLf
sConsulta = sConsulta & "     [DEST]" & vbCrLf
sConsulta = sConsulta & " ) REFERENCES [dbo].[DEST] (" & vbCrLf
sConsulta = sConsulta & "     [COD]" & vbCrLf
sConsulta = sConsulta & " )," & vbCrLf
sConsulta = sConsulta & " CONSTRAINT [UON1_DEST_FK_UON1] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & " (" & vbCrLf
sConsulta = sConsulta & "     [UON1]" & vbCrLf
sConsulta = sConsulta & " ) REFERENCES [dbo].[UON1] (" & vbCrLf
sConsulta = sConsulta & "     [COD]" & vbCrLf
sConsulta = sConsulta & " )" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

'UON2_DEST
sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[UON2_DEST]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)" & vbCrLf
sConsulta = sConsulta & "drop table [dbo].[UON2_DEST]" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE [dbo].[UON2_DEST] (" & vbCrLf
sConsulta = sConsulta & " [UON1] [varchar] (" & gLongitudesDeCodigos.giLongCodUON1 & ")  NOT NULL ," & vbCrLf
sConsulta = sConsulta & " [UON2] [varchar] (" & gLongitudesDeCodigos.giLongCodUON2 & ")  NOT NULL ," & vbCrLf
sConsulta = sConsulta & " [DEST] [varchar] (" & gLongitudesDeCodigos.giLongCodDEST & ")  NOT NULL " & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & " CREATE  CLUSTERED  INDEX [TEMP_PK_UON2_DEST] ON [dbo].[UON2_DEST]([UON1], [UON2], [DEST]) ON [PRIMARY]" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[UON2_DEST] WITH NOCHECK ADD " & vbCrLf
sConsulta = sConsulta & " CONSTRAINT [PK_UON2_DEST] PRIMARY KEY  NONCLUSTERED " & vbCrLf
sConsulta = sConsulta & " (" & vbCrLf
sConsulta = sConsulta & "     [UON1]," & vbCrLf
sConsulta = sConsulta & "     [UON2]," & vbCrLf
sConsulta = sConsulta & "     [DEST]" & vbCrLf
sConsulta = sConsulta & " )  ON [PRIMARY] " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & " CREATE  INDEX [IX_UON2_DEST] ON [dbo].[UON2_DEST]([DEST]) ON [PRIMARY]" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[UON2_DEST] ADD " & vbCrLf
sConsulta = sConsulta & " CONSTRAINT [UON2_DEST_FK_DEST] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & " (" & vbCrLf
sConsulta = sConsulta & "     [DEST]" & vbCrLf
sConsulta = sConsulta & " ) REFERENCES [dbo].[DEST] (" & vbCrLf
sConsulta = sConsulta & "     [COD]" & vbCrLf
sConsulta = sConsulta & " )," & vbCrLf
sConsulta = sConsulta & " CONSTRAINT [UON2_DEST_FK_UON2] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & " (" & vbCrLf
sConsulta = sConsulta & "     [UON1]," & vbCrLf
sConsulta = sConsulta & "     [UON2]" & vbCrLf
sConsulta = sConsulta & " ) REFERENCES [dbo].[UON2] (" & vbCrLf
sConsulta = sConsulta & "     [UON1]," & vbCrLf
sConsulta = sConsulta & "     [COD]" & vbCrLf
sConsulta = sConsulta & " )" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

'UON3_DEST
sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[UON3_DEST]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)" & vbCrLf
sConsulta = sConsulta & "drop table [dbo].[UON3_DEST]" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE [dbo].[UON3_DEST] (" & vbCrLf
sConsulta = sConsulta & " [UON1] [varchar] (" & gLongitudesDeCodigos.giLongCodUON1 & ")  NOT NULL ," & vbCrLf
sConsulta = sConsulta & " [UON2] [varchar] (" & gLongitudesDeCodigos.giLongCodUON2 & ")  NOT NULL ," & vbCrLf
sConsulta = sConsulta & " [UON3] [varchar] (" & gLongitudesDeCodigos.giLongCodUON3 & ")  NOT NULL ," & vbCrLf
sConsulta = sConsulta & " [DEST] [varchar] (" & gLongitudesDeCodigos.giLongCodDEST & ")  NOT NULL " & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & " CREATE  CLUSTERED  INDEX [TEMP_PK_UON3_DEST] ON [dbo].[UON3_DEST]([UON1], [UON2], [UON3], [DEST]) ON [PRIMARY]" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[UON3_DEST] WITH NOCHECK ADD " & vbCrLf
sConsulta = sConsulta & " CONSTRAINT [PK_UON3_DEST] PRIMARY KEY  NONCLUSTERED " & vbCrLf
sConsulta = sConsulta & " (" & vbCrLf
sConsulta = sConsulta & "     [UON1]," & vbCrLf
sConsulta = sConsulta & "     [UON2]," & vbCrLf
sConsulta = sConsulta & "     [UON3]," & vbCrLf
sConsulta = sConsulta & "     [DEST]" & vbCrLf
sConsulta = sConsulta & " )  ON [PRIMARY] " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & " CREATE  INDEX [IX_UON3_DEST_01] ON [dbo].[UON3_DEST]([DEST]) ON [PRIMARY]" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[UON3_DEST] ADD " & vbCrLf
sConsulta = sConsulta & " CONSTRAINT [UON3_DEST_FK_DEST] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & " (" & vbCrLf
sConsulta = sConsulta & "     [DEST]" & vbCrLf
sConsulta = sConsulta & " ) REFERENCES [dbo].[DEST] (" & vbCrLf
sConsulta = sConsulta & "     [COD]" & vbCrLf
sConsulta = sConsulta & " )," & vbCrLf
sConsulta = sConsulta & " CONSTRAINT [UON3_DEST_FK_UON3] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & " (" & vbCrLf
sConsulta = sConsulta & "     [UON1]," & vbCrLf
sConsulta = sConsulta & "     [UON2]," & vbCrLf
sConsulta = sConsulta & "     [UON3]" & vbCrLf
sConsulta = sConsulta & " ) REFERENCES [dbo].[UON3] (" & vbCrLf
sConsulta = sConsulta & "     [UON1]," & vbCrLf
sConsulta = sConsulta & "     [UON2]," & vbCrLf
sConsulta = sConsulta & "     [COD]" & vbCrLf
sConsulta = sConsulta & " )" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

End Sub

Private Function V2_9_5_Cambios_Tablas()
Dim sConsulta As String

'ACC

sConsulta = ""
sConsulta = "ALTER TABLE dbo.ACC ADD" & vbCrLf
sConsulta = sConsulta & "   ORDEN int NULL" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = ""
sConsulta = "UPDATE ACC SET ORDEN = ID"
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = "ALTER TABLE dbo.ACC ALTER COLUMN" & vbCrLf
sConsulta = sConsulta & "   ORDEN int NOT NULL" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

'GMN4_ACT
sConsulta = ""
sConsulta = "ALTER TABLE dbo.GMN4_ACT ALTER COLUMN" & vbCrLf
sConsulta = sConsulta & "   ACT1 int NOT NULL" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = "ALTER TABLE dbo.GMN4_ACT ALTER COLUMN" & vbCrLf
sConsulta = sConsulta & "   ACT2 int NOT NULL" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = "ALTER TABLE dbo.GMN4_ACT ALTER COLUMN" & vbCrLf
sConsulta = sConsulta & "   ACT3 int NOT NULL" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect


'PARGEN_DEST
sConsulta = ""
sConsulta = "ALTER TABLE dbo.PARGEN_GEST ADD" & vbCrLf
sConsulta = sConsulta & "   PERIODOAVISODES smallint NOT NULL DEFAULT (30)" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = ""
sConsulta = "ALTER TABLE dbo.PARGEN_GEST ADD" & vbCrLf
sConsulta = sConsulta & "   PERIODOAVISOADJ smallint NOT NULL DEFAULT (30)" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = ""
sConsulta = "ALTER TABLE dbo.PARGEN_GEST ADD" & vbCrLf
sConsulta = sConsulta & "   AVISODES tinyint NOT NULL DEFAULT (1)" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = ""
sConsulta = "ALTER TABLE dbo.PARGEN_GEST ADD" & vbCrLf
sConsulta = sConsulta & "   AVISOADJ tinyint NOT NULL DEFAULT (1)" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

'PROCE
sConsulta = ""
sConsulta = "ALTER TABLE dbo.PROCE ADD" & vbCrLf
sConsulta = sConsulta & "   USUAPER Varchar(" & gLongitudesDeCodigos.giLongCodUSU & ") NULL" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = ""
sConsulta = "UPDATE PROCE SET USUAPER = USUVAL WHERE USUVAL IS NOT NULL"
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = ""
sConsulta = "UPDATE PROCE SET USUAPER = '" & AdmUsu & "' WHERE USUAPER IS NULL"
gRDOCon.Execute sConsulta, rdExecDirect


sConsulta = ""
sConsulta = "ALTER TABLE dbo.PROCE ALTER COLUMN" & vbCrLf
sConsulta = sConsulta & "   USUAPER Varchar(" & gLongitudesDeCodigos.giLongCodUSU & ") NOT NULL" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect


'CON
sConsulta = "ALTER TABLE dbo.CON ADD" & vbCrLf
sConsulta = sConsulta & "   ID_PORT int NULL" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect


End Function




Private Function V2_9_5_Crear_Storeds()
Dim sConsulta As String

'P_GMN4_ACT_MODIF

sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[P_GMN4_ACT_MODIF]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[P_GMN4_ACT_MODIF]" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE P_GMN4_ACT_MODIF (@ACT1 AS INT,@ACT2 AS INT,@ACT3 AS INT,@ACT4 AS INT,@ACT5 INT)  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ACT2 IS NULL" & vbCrLf
sConsulta = sConsulta & " DELETE FROM GMN4_ACT WHERE ACT1=@ACT1" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & " IF @ACT3 IS NULL" & vbCrLf
sConsulta = sConsulta & "     UPDATE GMN4_ACT SET ACT2=NULL WHERE ACT1=@ACT1 AND ACT2=@ACT2" & vbCrLf
sConsulta = sConsulta & " ELSE" & vbCrLf
sConsulta = sConsulta & "     IF @ACT4 IS NULL" & vbCrLf
sConsulta = sConsulta & "         UPDATE GMN4_ACT SET ACT3=NULL WHERE ACT1=@ACT1 AND ACT2=@ACT2 AND ACT3=@ACT3" & vbCrLf
sConsulta = sConsulta & "     ELSE" & vbCrLf
sConsulta = sConsulta & "         IF @ACT5 IS NULL" & vbCrLf
sConsulta = sConsulta & "             UPDATE GMN4_ACT SET ACT4=NULL WHERE ACT1=@ACT1 AND ACT2=@ACT2 AND ACT3=@ACT3 AND ACT4=@ACT4" & vbCrLf
sConsulta = sConsulta & "         ELSE" & vbCrLf
sConsulta = sConsulta & "             UPDATE GMN4_ACT SET ACT5=NULL WHERE ACT1=@ACT1 AND ACT2=@ACT2 AND ACT3=@ACT3 AND ACT4=@ACT4 AND ACT5=@ACT5" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

'SP_ACTUALIZAR_ADJUNTO


sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ACTUALIZAR_ADJUNTO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_ACTUALIZAR_ADJUNTO]" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE SP_ACTUALIZAR_ADJUNTO (@NOM VARCHAR(300),@COM VARCHAR(500),@FECACT DATETIME, " & vbCrLf
sConsulta = sConsulta & "                              @COD_PROVE_CIA VARCHAR(200), @ESPEC INT)AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ESP_PORTAL SET NOM=@NOM,COM=@COM,FECHA=@FECACT" & vbCrLf
sConsulta = sConsulta & " WHERE PROVE=@COD_PROVE_CIA AND ID=@ESPEC" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

'SP_ACTUALIZAR_PROV_GS


sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ACTUALIZAR_PROV_GS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_ACTUALIZAR_PROV_GS]" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE SP_ACTUALIZAR_PROV_GS (@COD_PROVE_CIA VARCHAR(40),@CIAS_DEN VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                              @CIAS_NIF VARCHAR(20),@CIAS_PAI VARCHAR(20),@CIAS_PROVI VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "                              @CIAS_CP VARCHAR(20),@CIAS_POB VARCHAR(100),@CIAS_DIR VARCHAR(255)," & vbCrLf
sConsulta = sConsulta & "                              @CIAS_MON VARCHAR(20),@CIAS_URLCIA VARCHAR(255)) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @PAIS VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @MON VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROV VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @PAIS = (SELECT COD FROM PAI WHERE FSP_COD=@CIAS_PAI)" & vbCrLf
sConsulta = sConsulta & "SET @PROV = (SELECT COD FROM PROVI WHERE FSP_COD=@CIAS_PROVI)" & vbCrLf
sConsulta = sConsulta & "SET @MON = (SELECT COD FROM MON WHERE FSP_COD=@CIAS_MON)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- Actualizamos los datos del proveedor de GS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE SET DEN=@CIAS_DEN ,DIR=@CIAS_DIR ,CP=@CIAS_CP ,POB= @CIAS_POB ,PAI= @PAIS, " & vbCrLf
sConsulta = sConsulta & "                     PROVI=@PROV, MON=@MON,NIF=@CIAS_NIF,URLPROVE=@CIAS_URLCIA" & vbCrLf
sConsulta = sConsulta & "                 WHERE COD=@COD_PROVE_CIA" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

'SP_DEVOLVER_PROVEEDORES_A_PUBLICAR

sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_DEVOLVER_PROVEEDORES_A_PUBLICAR]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_DEVOLVER_PROVEEDORES_A_PUBLICAR]" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE SP_DEVOLVER_PROVEEDORES_A_PUBLICAR (@ANYO INT,@PROCE INT,@GMN1 VARCHAR(50)) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDINSTWEB AS INT" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @IDINSTWEB= (SELECT INSTWEB FROM  PARGEN_INTERNO WHERE ID = 1)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if  @IDINSTWEB= 1" & vbCrLf
sConsulta = sConsulta & "begin" & vbCrLf
sConsulta = sConsulta & " SELECT DISTINCT PROCE.ANYO,PROCE.GMN1,PROCE.COD,PROCE.DEN ,PROCE_PROVE.PROVE,PROVE.FSP_COD,PROVE.COD,PROVE.DEN" & vbCrLf
sConsulta = sConsulta & " FROM PROCE INNER JOIN PROCE_PROVE ON" & vbCrLf
sConsulta = sConsulta & " PROCE.ANYO = PROCE_PROVE.ANYO AND PROCE.GMN1 = PROCE_PROVE.GMN1 AND PROCE.COD = PROCE_PROVE.PROCE " & vbCrLf
sConsulta = sConsulta & " AND PROCE_PROVE.PUB=0 " & vbCrLf
sConsulta = sConsulta & " INNER JOIN PROVE ON PROVE.COD=PROCE_PROVE.PROVE " & vbCrLf
sConsulta = sConsulta & " WHERE PROCE.COD= @PROCE AND PROCE.ANYO =@ANYO AND PROCE.GMN1=@GMN1" & vbCrLf
sConsulta = sConsulta & "end " & vbCrLf
sConsulta = sConsulta & "if  @IDINSTWEB= 2" & vbCrLf
sConsulta = sConsulta & "begin" & vbCrLf
sConsulta = sConsulta & " SELECT DISTINCT PROCE.ANYO,PROCE.GMN1,PROCE.COD,PROCE.DEN ,PROCE_PROVE.PROVE,PROVE.FSP_COD,PROVE.COD,PROVE.DEN" & vbCrLf
sConsulta = sConsulta & " FROM PROCE INNER JOIN PROCE_PROVE ON" & vbCrLf
sConsulta = sConsulta & " PROCE.ANYO = PROCE_PROVE.ANYO AND PROCE.GMN1 = PROCE_PROVE.GMN1 AND PROCE.COD = PROCE_PROVE.PROCE " & vbCrLf
sConsulta = sConsulta & " AND PROCE_PROVE.PUB=0 " & vbCrLf
sConsulta = sConsulta & " INNER JOIN PROVE ON PROVE.COD=PROCE_PROVE.PROVE AND PROVE.FSP_COD IS NOT NULL" & vbCrLf
sConsulta = sConsulta & " WHERE PROCE.COD= @PROCE AND PROCE.ANYO =@ANYO AND PROCE.GMN1=@GMN1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "end" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

'SP_ELIMINAR_ADJUNTO

sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ELIMINAR_ADJUNTO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_ELIMINAR_ADJUNTO]" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE SP_ELIMINAR_ADJUNTO(@COD_PROVE_CIA VARCHAR(200),@ESPEC INT) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM PROVE_ESP_PORTAL WHERE PROVE=@COD_PROVE_CIA AND ID=@ESPEC" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

'SP_ELIMINAR_CONTACTOS

sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ELIMINAR_CONTACTOS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_ELIMINAR_CONTACTOS]" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE SP_ELIMINAR_CONTACTOS (@COD_PROVE_CIA VARCHAR(200))  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM CON WHERE PROVE = @COD_PROVE_CIA AND PORT = 1" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

'SP_INSERTAR_ADJUNTO

sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_INSERTAR_ADJUNTO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_INSERTAR_ADJUNTO]" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE SP_INSERTAR_ADJUNTO (@NOM VARCHAR(300),@COM VARCHAR(500),@FECACT DATETIME, " & vbCrLf
sConsulta = sConsulta & "                              @COD_PROVE_CIA VARCHAR(200), @ESPEC INT) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @NOM IS NOT NULL   --Para que no falle al hacer el insert" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & " INSERT INTO PROVE_ESP_PORTAL (PROVE,ID,NOM,COM,FECHA) VALUES (@COD_PROVE_CIA,@ESPEC,@NOM, @COM, @FECACT)" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

'SP_INSERTAR_CONTACTO

sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_INSERTAR_CONTACTO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_INSERTAR_CONTACTO]" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE SP_INSERTAR_CONTACTO (@USU_APE VARCHAR(100),@USU_NOM VARCHAR(50), @USU_DEP VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                             @USU_CAR VARCHAR(100), @USU_TFNO VARCHAR(50), @USU_FAX VARCHAR(50)," & vbCrLf
sConsulta = sConsulta & "                             @USU_EMAIL VARCHAR(100), @USU_TFNO2 VARCHAR(50), " & vbCrLf
sConsulta = sConsulta & "                             @USU_TFNO_MOVIL VARCHAR(50),@COD_PROVE_CIA VARCHAR(200))AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @INDICE INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @INDICE = (SELECT MAX(ID) FROM CON WHERE PROVE = @COD_PROVE_CIA)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @INDICE IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & " SET @INDICE = 1" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & " SET @INDICE = @INDICE + 1" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO CON (PROVE,ID,APE,NOM,DEP,CAR,TFNO,FAX,EMAIL,RECPET,TFNO2,PORT,TFNO_MOVIL)" & vbCrLf
sConsulta = sConsulta & "             VALUES(@COD_PROVE_CIA,@INDICE,@USU_APE,@USU_NOM,@USU_DEP,@USU_CAR,@USU_TFNO," & vbCrLf
sConsulta = sConsulta & "                      @USU_FAX,@USU_EMAIL,1,@USU_TFNO2,1,@USU_TFNO_MOVIL)" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

'SP_TRANSFER_PROVE_ESP_PORTAL
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_TRANSFER_PROVE_ESP_PORTAL]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_TRANSFER_PROVE_ESP_PORTAL]" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE SP_TRANSFER_PROVE_ESP_PORTAL @PROVE varchar(50), @ESP SMALLINT, @INIT INT, @SIZE INT, @DATA IMAGE AS" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "DECLARE @textpointer varbinary(16)  " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "SET nocount on " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "SELECT @textpointer = TEXTPTR(data)  FROM PROVE_ESP WHERE PROVE = @PROVE AND ID=@ESP" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "IF @TEXTPOINTER  IS NULL " & vbCrLf
sConsulta = sConsulta & "    UPDATE PROVE_ESP SET DATA = @DATA WHERE PROVE = @PROVE AND ID=@ESP" & vbCrLf
sConsulta = sConsulta & "ELSE " & vbCrLf
sConsulta = sConsulta & "    UPDATETEXT PROVE_ESP.DATA @textpointer @init 0 @data" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

End Function

Private Function V2_9_5_Modif_Storeds()
Dim sConsulta As String


'DEP_COD
sConsulta = ""
sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[DEP_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[DEP_COD]" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER ON " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE DEP_COD (@OLD VARCHAR(50),@NEW VARCHAR(50))  AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT ON" & vbCrLf
sConsulta = sConsulta & "BEGIN TRANSACTION" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR FOR SELECT COD FROM DEP WHERE COD=@OLD" & vbCrLf
sConsulta = sConsulta & "OPEN C" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C INTO @VAR_COD" & vbCrLf
sConsulta = sConsulta & "IF @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE DEP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON0_DEP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON1_DEP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON2_DEP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3_DEP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PER NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "UPDATE DEP SET COD=@NEW WHERE COD=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON0_DEP SET DEP=@NEW WHERE DEP=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON1_DEP SET DEP=@NEW WHERE DEP=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON2_DEP SET DEP=@NEW WHERE DEP=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON3_DEP SET DEP=@NEW WHERE DEP=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PER SET DEP=@NEW WHERE DEP=@OLD" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE DEP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON0_DEP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON1_DEP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON2_DEP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3_DEP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PER CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close C" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "COMMIT TRANSACTION" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

'PROVE_COD
sConsulta = ""
sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PROVE_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[PROVE_COD]" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER ON " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE PROVE_COD (@OLD VARCHAR(50),@NEW VARCHAR(50))  AS " & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_COD VARCHAR(50) " & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT ON " & vbCrLf
sConsulta = sConsulta & "BEGIN TRANSACTION " & vbCrLf
sConsulta = sConsulta & "BEGIN " & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR FOR SELECT COD FROM PROVE WHERE COD=@OLD " & vbCrLf
sConsulta = sConsulta & "OPEN C " & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C INTO @VAR_COD " & vbCrLf
sConsulta = sConsulta & "IF @@FETCH_STATUS=0 " & vbCrLf
sConsulta = sConsulta & "BEGIN " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CON NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART1_ADJ NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART2_ADJ NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART3_ADJ NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ADJ NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ADJ NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ADJREU NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OFE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OFE_WEB NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE DETALLE_PUJAS NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFEMIN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_ADJUN NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_WEB_ADJUN NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_WEB NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_PET NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_POT NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_EQP NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN1 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN2 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN3 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN4 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PROVE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVENOTIF NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--2.9" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ATRIB NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATALOG_LIN NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ORDEN_ENTREGA NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_PEDIDO NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART_UP NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "----" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--2.9.5" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ESP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ESP_PORTAL NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "-----" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE SET COD=@NEW WHERE COD=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE CON SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_PROVE SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART1_ADJ SET CODPROVE=@NEW WHERE CODPROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ART2_ADJ SET CODPROVE=@NEW WHERE CODPROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ART3_ADJ SET CODPROVE=@NEW WHERE CODPROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ART4_ADJ SET CODPROVE=@NEW WHERE CODPROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM SET PROVEACT=@NEW WHERE PROVEACT=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_ADJ SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_ADJREU SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_OFE SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_OFE_WEB SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE DETALLE_PUJAS SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFEMIN SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE_ADJUN SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE_WEB_ADJUN SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE_WEB SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE_PET SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE_POT SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_EQP SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_GMN1 SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_GMN2 SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_GMN3 SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_GMN4 SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART1 SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART2 SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART3 SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART4 SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVENOTIF SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE USU SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART4_ATRIB SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CATALOG_LIN SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ORDEN_ENTREGA SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE LINEAS_PEDIDO SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART_UP SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ESP SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ESP_PORTAL SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CON CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PROVE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART1_ADJ CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART2_ADJ CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART3_ADJ CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ADJ CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ADJ CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ADJREU CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OFE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OFE_WEB CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_ADJUN CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_WEB_ADJUN CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_WEB CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_PET CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_POT CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_EQP CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN1 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN2 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN3 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN4 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE DETALLE_PUJAS CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFEMIN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ATRIB CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATALOG_LIN CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ORDEN_ENTREGA CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_PEDIDO CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART_UP CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ESP CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ESP_PORTAL CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "End " & vbCrLf
sConsulta = sConsulta & "Close C " & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C " & vbCrLf
sConsulta = sConsulta & "End " & vbCrLf
sConsulta = sConsulta & "COMMIT TRANSACTION" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

'SP_DEVOLVER_PROCESOS_A_DESPUB
sConsulta = ""
sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_DEVOLVER_PROCESOS_A_DESPUB]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_DEVOLVER_PROCESOS_A_DESPUB]" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE SP_DEVOLVER_PROCESOS_A_DESPUB(@EQP VARCHAR(50), @COM VARCHAR(50),@DESDES DATETIME,@HASDES DATETIME,@DESADJ DATETIME,@HASADJ DATETIME) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & " SELECT DISTINCT PROCE.ANYO,PROCE.GMN1,PROCE.COD,PROCE.DEN,PROCE.FECLIMOFE,PROCE.ADJDIR," & vbCrLf
sConsulta = sConsulta & " PROCE.FECPRES,PROCE_PROVE.EQP,PROCE_PROVE.COM,PROCE_PROVE.PUB,REU_PROCE.FECHA,REU_PROCE.HORA,REU_PROCE.RES," & vbCrLf
sConsulta = sConsulta & " PROCE.FECNEC,PROCE.GMN2,PROCE.GMN3,PROCE.GMN4,PROCE.EST" & vbCrLf
sConsulta = sConsulta & " FROM PROCE " & vbCrLf
sConsulta = sConsulta & " INNER JOIN PROCE_PROVE ON" & vbCrLf
sConsulta = sConsulta & " PROCE.ANYO = PROCE_PROVE.ANYO AND PROCE.GMN1 = PROCE_PROVE.GMN1 AND PROCE.COD = PROCE_PROVE.PROCE" & vbCrLf
sConsulta = sConsulta & " AND PROCE_PROVE.EQP=@EQP AND PROCE_PROVE.COM=@COM" & vbCrLf
sConsulta = sConsulta & " LEFT JOIN REU_PROCE ON REU_PROCE.ANYO=PROCE.ANYO AND REU_PROCE.GMN1=PROCE.GMN1 " & vbCrLf
sConsulta = sConsulta & " AND REU_PROCE.PROCE= PROCE.COD  " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & " WHERE (REU_PROCE.FECHA >= @DESADJ AND REU_PROCE.FECHA <=@HASADJ AND PROCE.ADJDIR=0)" & vbCrLf
sConsulta = sConsulta & " OR (PROCE_PROVE.PUB=1" & vbCrLf
sConsulta = sConsulta & " AND PROCE.FECLIMOFE>=@DESDES AND PROCE.FECLIMOFE <= @HASDES)" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & " OR (PROCE.FECPRES>=@DESADJ AND PROCE.FECPRES <= @HASADJ" & vbCrLf
sConsulta = sConsulta & " AND PROCE.ADJDIR=1 )" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & " AND PROCE.EST>2 AND PROCE.EST<12" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & " ORDER BY PROCE.FECLIMOFE" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

'SP_ELIM_ENLACE
sConsulta = ""
sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ELIM_ENLACE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_ELIM_ENLACE]" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE SP_ELIM_ENLACE (@FSP_COD VARCHAR(50))  AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVE AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @PROVE = (SELECT COD FROM PROVE WHERE FSP_COD=@FSP_COD)" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE SET FSP_COD=NULL,FSP_USADO=0,PREMIUM=0,ACTIVO=0  WHERE FSP_COD= @FSP_COD" & vbCrLf
sConsulta = sConsulta & "DELETE FROM CON WHERE PROVE=@PROVE AND PORT=1" & vbCrLf
sConsulta = sConsulta & "DELETE FROM PROVE_ESP_PORTAL WHERE PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

'SP_ELIM_ENLACE_2
sConsulta = ""
sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ELIM_ENLACE_2]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_ELIM_ENLACE_2]" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE SP_ELIM_ENLACE_2 (@COD VARCHAR(50))  AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE SET FSP_COD=NULL, FSP_USADO=0,PREMIUM=0,ACTIVO=0  WHERE COD= @COD" & vbCrLf
sConsulta = sConsulta & "DELETE FROM CON WHERE PROVE=@COD AND PORT=1" & vbCrLf
sConsulta = sConsulta & "DELETE FROM PROVE_ESP_PORTAL WHERE PROVE=@COD" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

'UON1_COD
sConsulta = ""
sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[UON1_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[UON1_COD]" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER ON " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE UON1_COD (@OLD VARCHAR(50),@NEW VARCHAR(50)) AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT ON" & vbCrLf
sConsulta = sConsulta & "BEGIN TRANSACTION" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR FOR SELECT COD FROM UON1 WHERE COD=@OLD" & vbCrLf
sConsulta = sConsulta & "OPEN C" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C INTO @VAR_COD" & vbCrLf
sConsulta = sConsulta & "IF @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON1_DEP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON2_DEP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3_DEP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PER NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON1_DEST NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON2_DEST NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3_DEST NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "UPDATE UON1 SET COD=@NEW WHERE COD=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON2 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON3 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON1_DEP SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON2_DEP SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON3_DEP SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PER SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_UON1 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_UON2 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_UON3 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_UON1 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_UON2 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_UON3 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON1_DEST SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON2_DEST SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON3_DEST SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON1_DEP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON2_DEP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3_DEP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PER CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON1_DEST CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON2_DEST CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3_DEST CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close C" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "COMMIT TRANSACTION" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect


'UON2_COD

sConsulta = ""
sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[UON2_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[UON2_COD]" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER ON " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE UON2_COD (@OLD VARCHAR(50),@NEW VARCHAR(50), @UON1 VARCHAR(50))  AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT ON" & vbCrLf
sConsulta = sConsulta & "BEGIN TRANSACTION" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR FOR SELECT COD FROM UON2 WHERE UON1=@UON1 AND COD=@OLD" & vbCrLf
sConsulta = sConsulta & "OPEN C" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C INTO @VAR_COD" & vbCrLf
sConsulta = sConsulta & "IF @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON2_DEP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3_DEP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PER NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON2_DEST NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3_DEST NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "UPDATE UON2 SET COD=@NEW WHERE UON1=@UON1 AND COD=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON3 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON2_DEP SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON3_DEP SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PER SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_UON2 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_UON3 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_UON2 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_UON3 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON2_DEST SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON3_DEST SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON2_DEP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3_DEP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PER CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON2_DEST CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3_DEST CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close C" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "COMMIT TRANSACTION" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect


'UON3_COD
sConsulta = ""
sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[UON3_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[UON3_COD]" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER ON " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE UON3_COD (@OLD VARCHAR(50),@NEW VARCHAR(50), @UON1 VARCHAR(50), @UON2 VARCHAR(50))  AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT ON" & vbCrLf
sConsulta = sConsulta & "BEGIN TRANSACTION" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR FOR SELECT COD FROM UON3 WHERE UON1=@UON1 AND UON2=@UON2 AND COD=@OLD" & vbCrLf
sConsulta = sConsulta & "OPEN C" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C INTO @VAR_COD" & vbCrLf
sConsulta = sConsulta & "IF @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3_DEP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PER NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3_DEST NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "UPDATE UON3 SET COD=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND COD=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON3_DEP SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PER SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_UON3 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_UON3 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON3_DEST SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3_DEP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PER CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3_DEST CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close C" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "COMMIT TRANSACTION" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

'USU_COD
sConsulta = ""
sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USU_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[USU_COD]" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER ON " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE USU_COD (@OLD VARCHAR(50),@NEW VARCHAR(50),@PWD VARCHAR(512))  AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT ON" & vbCrLf
sConsulta = sConsulta & "BEGIN TRANSACTION" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR FOR SELECT COD FROM USU WHERE COD=@OLD" & vbCrLf
sConsulta = sConsulta & "OPEN C" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C INTO @VAR_COD" & vbCrLf
sConsulta = sConsulta & "IF @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU_ACC NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU_LIS NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LOG NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "UPDATE USU SET COD=@NEW,PWD=@PWD WHERE COD=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE USU_ACC SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE USU_LIS SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET USUVAL=@NEW WHERE USUVAL=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET USUVALSELPROVE=@NEW WHERE USUVALSELPROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET USUVALCIERRE=@NEW WHERE USUVALCIERRE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET USUAPER=@NEW WHERE USUAPER=@OLD" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU_ACC CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU_LIS CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LOG CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close C" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "COMMIT TRANSACTION" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect



End Function
























Private Sub V_2_9_5_triggers_50_1()
Dim sConsulta As String

sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[ORDEN_ENTREGA_TG_UPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1) drop trigger [dbo].[ORDEN_ENTREGA_TG_UPD]"
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = "CREATE TRIGGER ORDEN_ENTREGA_TG_UPD ON dbo.ORDEN_ENTREGA"
sConsulta = sConsulta & vbCrLf & "FOR UPDATE"
sConsulta = sConsulta & vbCrLf & "AS"
sConsulta = sConsulta & vbCrLf & "DECLARE @ID AS INTEGER"
sConsulta = sConsulta & vbCrLf & "DECLARE @EST AS INTEGER"
sConsulta = sConsulta & vbCrLf & "DECLARE @ANYO AS INTEGER"
sConsulta = sConsulta & vbCrLf & "DECLARE @GMN1 AS VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "DECLARE @PROVE AS VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "DECLARE @ITEM AS INTEGER"
sConsulta = sConsulta & vbCrLf & "DECLARE @PROCE AS INTEGER"
sConsulta = sConsulta & vbCrLf & "DECLARE @CANT AS FLOAT"
sConsulta = sConsulta & vbCrLf & "DECLARE @LINEAID AS INTEGER"
sConsulta = sConsulta & vbCrLf & "DECLARE @DESPUB AS INTEGER"
sConsulta = sConsulta & vbCrLf & "DECLARE @CANT_PED AS FLOAT"
sConsulta = sConsulta & vbCrLf & "DECLARE @CANT_ADJ AS FLOAT"
sConsulta = sConsulta & vbCrLf & "DECLARE @TIPO AS INTEGER"
sConsulta = sConsulta & vbCrLf & "DECLARE @CAT1 AS INTEGER"
sConsulta = sConsulta & vbCrLf & "DECLARE @CAT2 AS INTEGER"
sConsulta = sConsulta & vbCrLf & "DECLARE @CAT3 AS INTEGER"
sConsulta = sConsulta & vbCrLf & "DECLARE @CAT4 AS INTEGER"
sConsulta = sConsulta & vbCrLf & "DECLARE @CAT5 AS INTEGER"
sConsulta = sConsulta & vbCrLf & "DECLARE @SEG AS INTEGER"
sConsulta = sConsulta & vbCrLf & ""
sConsulta = sConsulta & vbCrLf & "DECLARE curTG_ORDEN_ENTREGA_Upd CURSOR FOR SELECT ID,EST,TIPO FROM INSERTED"
sConsulta = sConsulta & vbCrLf & "OPEN curTG_ORDEN_ENTREGA_Upd"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM curTG_ORDEN_ENTREGA_Upd INTO @ID,@EST,@TIPO"
sConsulta = sConsulta & vbCrLf & "WHILE @@FETCH_STATUS=0"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "IF UPDATE(EST)"
sConsulta = sConsulta & vbCrLf & "   BEGIN"
sConsulta = sConsulta & vbCrLf & "   IF @EST=2"
sConsulta = sConsulta & vbCrLf & "       BEGIN"
sConsulta = sConsulta & vbCrLf & "       SET @DESPUB=(SELECT ELIMLINCATALOG FROM PARGEN_GEST)"
sConsulta = sConsulta & vbCrLf & ""
sConsulta = sConsulta & vbCrLf & "       DECLARE cur_LineasOrden CURSOR FOR SELECT ANYO,GMN1,PROCE,ITEM,PROVE,ID,CANT_PED*FC  FROM LINEAS_PEDIDO WHERE ORDEN=@ID AND EST=1 AND ITEM IS NOT NULL"
sConsulta = sConsulta & vbCrLf & "       OPEN cur_LineasOrden"
sConsulta = sConsulta & vbCrLf & "       FETCH NEXT FROM cur_LineasOrden INTO @ANYO,@GMN1,@PROCE,@ITEM,@PROVE,@LINEAID,@CANT"
sConsulta = sConsulta & vbCrLf & "       WHILE @@FETCH_STATUS=0"
sConsulta = sConsulta & vbCrLf & "           BEGIN"
sConsulta = sConsulta & vbCrLf & "           UPDATE ITEM_ADJ SET CANT_PED=CANT_PED + @CANT WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM AND PROVE=@PROVE"
sConsulta = sConsulta & vbCrLf & "           "
sConsulta = sConsulta & vbCrLf & "           SELECT  @CANT_PED= CANT_PED, @CANT_ADJ=CANT_ADJ FROM ITEM_ADJ WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM AND PROVE=@PROVE"
sConsulta = sConsulta & vbCrLf & "           IF @DESPUB=1 AND (@CANT_ADJ<=@CANT_PED)"
sConsulta = sConsulta & vbCrLf & "               UPDATE CATALOG_LIN SET PUB=0 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM AND PROVE=@PROVE"
sConsulta = sConsulta & vbCrLf & ""
sConsulta = sConsulta & vbCrLf & "           FETCH NEXT FROM cur_LineasOrden INTO @ANYO,@GMN1,@PROCE,@ITEM,@PROVE,@LINEAID,@CANT"
sConsulta = sConsulta & vbCrLf & "           End"
sConsulta = sConsulta & vbCrLf & "       Close cur_LineasOrden"
sConsulta = sConsulta & vbCrLf & "       DEALLOCATE cur_LineasOrden"
sConsulta = sConsulta & vbCrLf & "       End"
sConsulta = sConsulta & vbCrLf & "   End"
sConsulta = sConsulta & vbCrLf & "   IF @TIPO=1 AND (@EST >=  6)"
sConsulta = sConsulta & vbCrLf & "   BEGIN"
sConsulta = sConsulta & vbCrLf & "     DECLARE cur_LineasOrden1 CURSOR FOR SELECT DISTINCT CAT1,CAT2,CAT3,CAT4,CAT5 FROM LINEAS_PEDIDO WHERE LINEAS_PEDIDO.ORDEN=@ID"
sConsulta = sConsulta & vbCrLf & "     OPEN cur_LineasOrden1"
sConsulta = sConsulta & vbCrLf & "     FETCH NEXT FROM cur_LineasOrden1 INTO @CAT1,@CAT2,@CAT3,@CAT4,@CAT5"
sConsulta = sConsulta & vbCrLf & "     WHILE @@FETCH_STATUS=0"
sConsulta = sConsulta & vbCrLf & "     BEGIN"
sConsulta = sConsulta & vbCrLf & "       IF @CAT5 IS NOT NULL AND (SELECT BAJALOG FROM CATN5 WHERE ID=@CAT5) = 1"
sConsulta = sConsulta & vbCrLf & "          IF (SELECT COUNT(*) FROM LINEAS_PEDIDO INNER JOIN ORDEN_ENTREGA ON LINEAS_PEDIDO.ORDEN=ORDEN_ENTREGA.ID WHERE CAT5=@CAT5 AND ORDEN_ENTREGA.EST<6)=0"
sConsulta = sConsulta & vbCrLf & "          BEGIN"
sConsulta = sConsulta & vbCrLf & "             UPDATE CATN5 SET BAJALOG=2 WHERE ID=@CAT5"
sConsulta = sConsulta & vbCrLf & "             SET @SEG=NULL"
sConsulta = sConsulta & vbCrLf & "             SELECT @SEG=SEGURIDAD FROM CAT_SEGURIDAD WHERE CAT=@CAT5 AND NIVEL=5"
sConsulta = sConsulta & vbCrLf & "             IF @SEG IS NOT NULL EXEC FSEP_ELIMINAR_SEGURIDAD @SEG"
sConsulta = sConsulta & vbCrLf & "          End"
sConsulta = sConsulta & vbCrLf & "       IF @CAT4 IS NOT NULL AND (SELECT BAJALOG FROM CATN4 WHERE ID=@CAT4) = 1"
sConsulta = sConsulta & vbCrLf & "          IF (SELECT COUNT(*) FROM LINEAS_PEDIDO INNER JOIN ORDEN_ENTREGA ON LINEAS_PEDIDO.ORDEN=ORDEN_ENTREGA.ID WHERE CAT4=@CAT4 AND ORDEN_ENTREGA.EST<6)=0"
sConsulta = sConsulta & vbCrLf & "          BEGIN"
sConsulta = sConsulta & vbCrLf & "               UPDATE CATN4 SET BAJALOG=2 WHERE ID=@CAT4"
sConsulta = sConsulta & vbCrLf & "              SET @SEG=NULL"
sConsulta = sConsulta & vbCrLf & "              SELECT @SEG=SEGURIDAD FROM CAT_SEGURIDAD WHERE CAT=@CAT4 AND NIVEL=4"
sConsulta = sConsulta & vbCrLf & "              IF @SEG IS NOT NULL EXEC FSEP_ELIMINAR_SEGURIDAD @SEG"
sConsulta = sConsulta & vbCrLf & "          End"
sConsulta = sConsulta & vbCrLf & "       IF @CAT3 IS NOT NULL AND (SELECT BAJALOG FROM CATN3 WHERE ID=@CAT3) = 1"
sConsulta = sConsulta & vbCrLf & "           IF (SELECT COUNT(*) FROM LINEAS_PEDIDO INNER JOIN ORDEN_ENTREGA ON LINEAS_PEDIDO.ORDEN=ORDEN_ENTREGA.ID WHERE CAT3=@CAT3 AND ORDEN_ENTREGA.EST<6)=0"
sConsulta = sConsulta & vbCrLf & "           BEGIN"
sConsulta = sConsulta & vbCrLf & "               UPDATE CATN3 SET BAJALOG=2 WHERE ID=@CAT3"
sConsulta = sConsulta & vbCrLf & "               SET @SEG=NULL"
sConsulta = sConsulta & vbCrLf & "               SELECT @SEG=SEGURIDAD FROM CAT_SEGURIDAD WHERE CAT=@CAT3 AND NIVEL=3"
sConsulta = sConsulta & vbCrLf & "               IF @SEG IS NOT NULL EXEC FSEP_ELIMINAR_SEGURIDAD @SEG"
sConsulta = sConsulta & vbCrLf & "           End"
sConsulta = sConsulta & vbCrLf & "       IF @CAT2 IS NOT NULL AND (SELECT BAJALOG FROM CATN2 WHERE ID=@CAT2) = 1"
sConsulta = sConsulta & vbCrLf & "           IF (SELECT COUNT(*) FROM LINEAS_PEDIDO INNER JOIN ORDEN_ENTREGA ON LINEAS_PEDIDO.ORDEN=ORDEN_ENTREGA.ID WHERE CAT2=@CAT2 AND ORDEN_ENTREGA.EST<6)=0"
sConsulta = sConsulta & vbCrLf & "           BEGIN"
sConsulta = sConsulta & vbCrLf & "               UPDATE CATN2 SET BAJALOG=2 WHERE ID=@CAT2"
sConsulta = sConsulta & vbCrLf & "               SET @SEG=NULL"
sConsulta = sConsulta & vbCrLf & "               SELECT @SEG=SEGURIDAD FROM CAT_SEGURIDAD WHERE CAT=@CAT2 AND NIVEL=2"
sConsulta = sConsulta & vbCrLf & "               IF @SEG IS NOT NULL EXEC FSEP_ELIMINAR_SEGURIDAD @SEG"
sConsulta = sConsulta & vbCrLf & "           End"
sConsulta = sConsulta & vbCrLf & "       IF @CAT1 IS NOT NULL AND (SELECT BAJALOG FROM CATN1 WHERE ID=@CAT1) = 1"
sConsulta = sConsulta & vbCrLf & "           IF (SELECT COUNT(*) FROM LINEAS_PEDIDO INNER JOIN ORDEN_ENTREGA ON LINEAS_PEDIDO.ORDEN=ORDEN_ENTREGA.ID WHERE CAT1=@CAT1 AND ORDEN_ENTREGA.EST<6)=0"
sConsulta = sConsulta & vbCrLf & "           BEGIN"
sConsulta = sConsulta & vbCrLf & "              UPDATE CATN1 SET BAJALOG=2 WHERE ID=@CAT1"
sConsulta = sConsulta & vbCrLf & "              SET @SEG=NULL"
sConsulta = sConsulta & vbCrLf & "              SELECT @SEG=SEGURIDAD FROM CAT_SEGURIDAD WHERE CAT=@CAT1 AND NIVEL=1"
sConsulta = sConsulta & vbCrLf & "              IF @SEG IS NOT NULL EXEC FSEP_ELIMINAR_SEGURIDAD @SEG"
sConsulta = sConsulta & vbCrLf & "           End"
sConsulta = sConsulta & vbCrLf & "       FETCH NEXT FROM cur_LineasOrden1 INTO @CAT1,@CAT2,@CAT3,@CAT4,@CAT5"
sConsulta = sConsulta & vbCrLf & "     End"
sConsulta = sConsulta & vbCrLf & "     Close cur_LineasOrden1"
sConsulta = sConsulta & vbCrLf & "     DEALLOCATE cur_LineasOrden1"
sConsulta = sConsulta & vbCrLf & ""
sConsulta = sConsulta & vbCrLf & "   End"
sConsulta = sConsulta & vbCrLf & ""
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM curTG_ORDEN_ENTREGA_Upd INTO @ID,@EST,@TIPO"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "Close curTG_ORDEN_ENTREGA_Upd"
sConsulta = sConsulta & vbCrLf & "DEALLOCATE curTG_ORDEN_ENTREGA_Upd"
sConsulta = sConsulta & vbCrLf & ""
gRDOCon.Execute sConsulta, rdExecDirect


sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[APROB_LIM_TG_DEL]') and OBJECTPROPERTY(id, N'IsTrigger') = 1) drop trigger [dbo].[APROB_LIM_TG_DEL]"
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = "CREATE  TRIGGER APROB_LIM_TG_DEL ON dbo.APROB_LIM"
sConsulta = sConsulta & vbCrLf & "FOR DELETE"
sConsulta = sConsulta & vbCrLf & "AS"
sConsulta = sConsulta & vbCrLf & "DECLARE @ID INTEGER"
sConsulta = sConsulta & vbCrLf & "DECLARE @PER VARCHAR(100)"
sConsulta = sConsulta & vbCrLf & "DECLARE @PERPAD VARCHAR(100)"
sConsulta = sConsulta & vbCrLf & "DECLARE @CODUSU VARCHAR(100)"
sConsulta = sConsulta & vbCrLf & "DECLARE @TIPO SMALLINT"
sConsulta = sConsulta & vbCrLf & "DECLARE @PADRE INTEGER"
sConsulta = sConsulta & vbCrLf & "DECLARE @COUNT INTEGER"
sConsulta = sConsulta & vbCrLf & "DECLARE @SEG INTEGER"
sConsulta = sConsulta & vbCrLf & "DECLARE @CAT INTEGER"
sConsulta = sConsulta & vbCrLf & "DECLARE @NIVEL INTEGER"
sConsulta = sConsulta & vbCrLf & ""
sConsulta = sConsulta & vbCrLf & "DECLARE curTG_Aprob_Lim_Del CURSOR FOR SELECT ID,PER,PADRE,SEGURIDAD FROM DELETED"
sConsulta = sConsulta & vbCrLf & "OPEN curTG_Aprob_Lim_Del"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM curTG_Aprob_Lim_Del INTO @ID,@PER,@PADRE,@SEG"
sConsulta = sConsulta & vbCrLf & "WHILE @@FETCH_STATUS=0"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "   SELECT @CODUSU=COD,@TIPO=FSEPTIPO FROM USU WHERE PER =@PER"
sConsulta = sConsulta & vbCrLf & "   IF @TIPO =1 AND (SELECT COUNT(*) FROM APROB_LIM WHERE PER=@PER)=0"
sConsulta = sConsulta & vbCrLf & "       UPDATE USU SET FSEPTIPO=0 WHERE COD=@CODUSU"
sConsulta = sConsulta & vbCrLf & "   IF @PADRE IS NOT NULL"
sConsulta = sConsulta & vbCrLf & "   BEGIN"
sConsulta = sConsulta & vbCrLf & "       SELECT @COUNT=COUNT(*) FROM APROB_LIM WHERE PADRE=@PADRE"
sConsulta = sConsulta & vbCrLf & "       IF @COUNT=0  --No es aprobador de tipo2"
sConsulta = sConsulta & vbCrLf & "       BEGIN"
sConsulta = sConsulta & vbCrLf & "         SELECT @PERPAD=PER FROM APROB_LIM WHERE ID=@PADRE"
sConsulta = sConsulta & vbCrLf & "         SELECT @CODUSU=COD FROM USU WHERE PER =@PERPAD"
sConsulta = sConsulta & vbCrLf & "         IF (SELECT COUNT(*) AS APROB FROM APROB_LIM A1 INNER JOIN APROB_LIM A2 ON A1.PADRE=A2.ID WHERE A2.PER =@PERPAD )=0 -- No es aprobador tipo2"
sConsulta = sConsulta & vbCrLf & "             IF (SELECT COUNT(*) FROM SEGURIDAD WHERE APROB_TIPO1 =@PERPAD)=0 -- No es aprobador tipo1"
sConsulta = sConsulta & vbCrLf & "                 UPDATE USU SET FSEPTIPO=1 WHERE COD=@CODUSU"
sConsulta = sConsulta & vbCrLf & "            ELSE --Es aprobador tipo1"
sConsulta = sConsulta & vbCrLf & "                 IF @COUNT=0 UPDATE USU SET FSEPTIPO=2 WHERE COD=@CODUSU"
sConsulta = sConsulta & vbCrLf & "       End"
sConsulta = sConsulta & vbCrLf & "   End"
sConsulta = sConsulta & vbCrLf & ""
sConsulta = sConsulta & vbCrLf & "   SELECT @CAT=CAT,@NIVEL =NIVEL FROM CAT_SEGURIDAD WHERE SEGURIDAD=@SEG"
sConsulta = sConsulta & vbCrLf & "   IF @NIVEL = 1"
sConsulta = sConsulta & vbCrLf & "       DELETE FROM CAT_PER WHERE PER=@PER AND CAT1=@CAT"
sConsulta = sConsulta & vbCrLf & "   Else"
sConsulta = sConsulta & vbCrLf & "      IF @NIVEL=2"
sConsulta = sConsulta & vbCrLf & "       DELETE FROM CAT_PER WHERE PER=@PER AND CAT2=@CAT"
sConsulta = sConsulta & vbCrLf & "      Else"
sConsulta = sConsulta & vbCrLf & "           IF @NIVEL=3"
sConsulta = sConsulta & vbCrLf & "       DELETE FROM CAT_PER WHERE PER=@PER AND CAT3=@CAT"
sConsulta = sConsulta & vbCrLf & "           Else"
sConsulta = sConsulta & vbCrLf & "       IF @NIVEL=4"
sConsulta = sConsulta & vbCrLf & "          DELETE FROM CAT_PER WHERE PER=@PER AND CAT4=@CAT"
sConsulta = sConsulta & vbCrLf & "       Else"
sConsulta = sConsulta & vbCrLf & "              DELETE FROM CAT_PER WHERE PER=@PER AND CAT5=@CAT"
sConsulta = sConsulta & vbCrLf & ""
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM curTG_Aprob_Lim_Del INTO @ID,@PER,@PADRE,@SEG"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "Close curTG_Aprob_Lim_Del"
sConsulta = sConsulta & vbCrLf & "DEALLOCATE curTG_Aprob_Lim_Del"
sConsulta = sConsulta & vbCrLf & ""
gRDOCon.Execute sConsulta, rdExecDirect


End Sub

Private Sub V_2_9_5_Storeds_50_1()
Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_DEVOLVER_PROCESOS_A_DESPUB]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_DEVOLVER_PROCESOS_A_DESPUB]" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = "CREATE PROCEDURE SP_DEVOLVER_PROCESOS_A_DESPUB(@EQP VARCHAR(50), @COM VARCHAR(50),@DESDES INT ,@DESADJ INT,@RESPADJ TINYINT,@RESPPUB TINYINT) AS"
sConsulta = sConsulta & vbCrLf & ""
sConsulta = sConsulta & vbCrLf & "IF @RESPADJ=0  AND @RESPPUB=0"
sConsulta = sConsulta & vbCrLf & ""
sConsulta = sConsulta & vbCrLf & "     SELECT DISTINCT PROCE.ANYO,PROCE.GMN1,PROCE.COD,PROCE.DEN,PROCE.FECLIMOFE,PROCE.ADJDIR,"
sConsulta = sConsulta & vbCrLf & "     PROCE.FECPRES,PROCE_PROVE.EQP,PROCE_PROVE.COM,PROCE_PROVE.PUB,PROCE.USUAPER,"
sConsulta = sConsulta & vbCrLf & "     Proce.FECNEC , Proce.GMN2, Proce.GMN3, Proce.GMN4, Proce.EST"
sConsulta = sConsulta & vbCrLf & "     From Proce"
sConsulta = sConsulta & vbCrLf & "     INNER JOIN PROCE_PROVE ON"
sConsulta = sConsulta & vbCrLf & "     Proce.anyo = PROCE_PROVE.anyo And Proce.gmn1 = PROCE_PROVE.gmn1 And Proce.COD = PROCE_PROVE.Proce"
sConsulta = sConsulta & vbCrLf & "     AND PROCE_PROVE.EQP=@EQP AND PROCE_PROVE.COM=@COM"
sConsulta = sConsulta & vbCrLf & "     "
sConsulta = sConsulta & vbCrLf & "     WHERE ((PROCE_PROVE.PUB=1 AND convert(char,getdate(),111) <=PROCE.FECLIMOFE AND dateadd(day, @DESDES   ,PROCE.FECLIMOFE)<=convert(char,getdate(),111))"
sConsulta = sConsulta & vbCrLf & "     "
sConsulta = sConsulta & vbCrLf & "     OR (convert(char,getdate(),111)<=PROCE.FECPRES AND dateadd(day, @DESADJ   ,PROCE.FECPRES)<=convert(char,getdate(),111)))"
sConsulta = sConsulta & vbCrLf & "    "
sConsulta = sConsulta & vbCrLf & "     AND PROCE.EST>2 AND PROCE.EST<12"
sConsulta = sConsulta & vbCrLf & "     "
sConsulta = sConsulta & vbCrLf & "     ORDER BY PROCE.FECLIMOFE"
sConsulta = sConsulta & vbCrLf & "  "
sConsulta = sConsulta & vbCrLf & "Else"
sConsulta = sConsulta & vbCrLf & ""
sConsulta = sConsulta & vbCrLf & "  IF @RESPADJ=1  AND @RESPPUB=0"
sConsulta = sConsulta & vbCrLf & "  "
sConsulta = sConsulta & vbCrLf & "     SELECT DISTINCT PROCE.ANYO,PROCE.GMN1,PROCE.COD,PROCE.DEN,PROCE.FECLIMOFE,PROCE.ADJDIR,"
sConsulta = sConsulta & vbCrLf & "     PROCE.FECPRES,PROCE_PROVE.EQP,PROCE_PROVE.COM,PROCE_PROVE.PUB,PROCE.USUAPER,"
sConsulta = sConsulta & vbCrLf & "     Proce.FECNEC , Proce.GMN2, Proce.GMN3, Proce.GMN4, Proce.EST"
sConsulta = sConsulta & vbCrLf & "     From Proce"
sConsulta = sConsulta & vbCrLf & "     INNER JOIN PROCE_PROVE ON"
sConsulta = sConsulta & vbCrLf & "     Proce.anyo = PROCE_PROVE.anyo And Proce.gmn1 = PROCE_PROVE.gmn1 And Proce.COD = PROCE_PROVE.Proce"
sConsulta = sConsulta & vbCrLf & "     AND PROCE_PROVE.EQP=@EQP AND PROCE_PROVE.COM=@COM"
sConsulta = sConsulta & vbCrLf & "    "
sConsulta = sConsulta & vbCrLf & "     WHERE ((PROCE_PROVE.PUB=1 AND convert(char,getdate(),111) <=PROCE.FECLIMOFE AND dateadd(day, @DESDES   ,PROCE.FECLIMOFE)<=convert(char,getdate(),111)"
sConsulta = sConsulta & vbCrLf & "     AND PROCE.EQP=@EQP AND PROCE.COM=@COM)"
sConsulta = sConsulta & vbCrLf & "     "
sConsulta = sConsulta & vbCrLf & "     OR (convert(char,getdate(),111)<=PROCE.FECPRES AND dateadd(day,@DESADJ   ,PROCE.FECPRES)<=convert(char,getdate(),111)))"
sConsulta = sConsulta & vbCrLf & "    "
sConsulta = sConsulta & vbCrLf & "     AND PROCE.EST>2 AND PROCE.EST<12"
sConsulta = sConsulta & vbCrLf & "     "
sConsulta = sConsulta & vbCrLf & "     ORDER BY PROCE.FECLIMOFE"
sConsulta = sConsulta & vbCrLf & "  Else"
sConsulta = sConsulta & vbCrLf & "    IF @RESPADJ=0  AND @RESPPUB=1"
sConsulta = sConsulta & vbCrLf & " "
sConsulta = sConsulta & vbCrLf & "     SELECT DISTINCT PROCE.ANYO,PROCE.GMN1,PROCE.COD,PROCE.DEN,PROCE.FECLIMOFE,PROCE.ADJDIR,"
sConsulta = sConsulta & vbCrLf & "     PROCE.FECPRES,PROCE_PROVE.EQP,PROCE_PROVE.COM,PROCE_PROVE.PUB,PROCE.USUAPER,"
sConsulta = sConsulta & vbCrLf & "     Proce.FECNEC , Proce.GMN2, Proce.GMN3, Proce.GMN4, Proce.EST"
sConsulta = sConsulta & vbCrLf & "     From Proce"
sConsulta = sConsulta & vbCrLf & "     INNER JOIN PROCE_PROVE ON"
sConsulta = sConsulta & vbCrLf & "     Proce.anyo = PROCE_PROVE.anyo And Proce.gmn1 = PROCE_PROVE.gmn1 And Proce.COD = PROCE_PROVE.Proce"
sConsulta = sConsulta & vbCrLf & "     AND PROCE_PROVE.EQP=@EQP AND PROCE_PROVE.COM=@COM"
sConsulta = sConsulta & vbCrLf & "         "
sConsulta = sConsulta & vbCrLf & "     WHERE ( (PROCE_PROVE.PUB=1 AND convert(char,getdate(),111)<=PROCE.FECLIMOFE AND dateadd(day, @DESDES   ,PROCE.FECLIMOFE)<=convert(char,getdate(),111) )"
sConsulta = sConsulta & vbCrLf & "    "
sConsulta = sConsulta & vbCrLf & "     "
sConsulta = sConsulta & vbCrLf & "     OR (convert(char,getdate(),111)<=PROCE.FECPRES AND dateadd(day, @DESADJ   ,PROCE.FECPRES)<=convert(char,getdate(),111)"
sConsulta = sConsulta & vbCrLf & "           AND PROCE.EQP=@EQP AND PROCE.COM=@COM))"
sConsulta = sConsulta & vbCrLf & "    "
sConsulta = sConsulta & vbCrLf & "     AND PROCE.EST>2 AND PROCE.EST<12"
sConsulta = sConsulta & vbCrLf & "     "
sConsulta = sConsulta & vbCrLf & "     ORDER BY PROCE.FECLIMOFE"
sConsulta = sConsulta & vbCrLf & "Else"
sConsulta = sConsulta & vbCrLf & "    IF @RESPADJ=1  AND @RESPPUB=1"
sConsulta = sConsulta & vbCrLf & "  "
sConsulta = sConsulta & vbCrLf & "     SELECT DISTINCT PROCE.ANYO,PROCE.GMN1,PROCE.COD,PROCE.DEN,PROCE.FECLIMOFE,PROCE.ADJDIR,"
sConsulta = sConsulta & vbCrLf & "     PROCE.FECPRES,PROCE_PROVE.EQP,PROCE_PROVE.COM,PROCE_PROVE.PUB,PROCE.USUAPER,"
sConsulta = sConsulta & vbCrLf & "     Proce.FECNEC , Proce.GMN2, Proce.GMN3, Proce.GMN4, Proce.EST"
sConsulta = sConsulta & vbCrLf & "     From Proce"
sConsulta = sConsulta & vbCrLf & "     INNER JOIN PROCE_PROVE ON"
sConsulta = sConsulta & vbCrLf & "     Proce.anyo = PROCE_PROVE.anyo And Proce.gmn1 = PROCE_PROVE.gmn1 And Proce.COD = PROCE_PROVE.Proce"
sConsulta = sConsulta & vbCrLf & "     AND PROCE_PROVE.EQP=@EQP AND PROCE_PROVE.COM=@COM"
sConsulta = sConsulta & vbCrLf & "    "
sConsulta = sConsulta & vbCrLf & "     WHERE ((PROCE_PROVE.PUB=1 AND convert(char,getdate(),111)<=PROCE.FECLIMOFE AND dateadd(day, @DESDES   ,PROCE.FECLIMOFE)<=convert(char,getdate(),111)"
sConsulta = sConsulta & vbCrLf & "    AND PROCE.EQP=@EQP AND PROCE.COM=@COM)"
sConsulta = sConsulta & vbCrLf & "    "
sConsulta = sConsulta & vbCrLf & "     "
sConsulta = sConsulta & vbCrLf & "     OR (convert(char,getdate(),111)<=PROCE.FECPRES AND dateadd(day, @DESADJ   ,PROCE.FECPRES)<=convert(char,getdate(),111)"
sConsulta = sConsulta & vbCrLf & "            AND PROCE.EQP=@EQP AND PROCE.COM=@COM))"
sConsulta = sConsulta & vbCrLf & "    "
sConsulta = sConsulta & vbCrLf & "     AND PROCE.EST>2 AND PROCE.EST<12"
sConsulta = sConsulta & vbCrLf & "     "
sConsulta = sConsulta & vbCrLf & "     ORDER BY PROCE.FECLIMOFE"
sConsulta = sConsulta & vbCrLf & ""
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_INSERTAR_CONTACTO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_INSERTAR_CONTACTO]" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = "CREATE PROCEDURE SP_INSERTAR_CONTACTO (@USU_APE VARCHAR(100),@USU_NOM VARCHAR(20), @USU_DEP VARCHAR(100),"
sConsulta = sConsulta & vbCrLf & "                             @USU_CAR VARCHAR(100), @USU_TFNO VARCHAR(100), @USU_FAX VARCHAR(100),"
sConsulta = sConsulta & vbCrLf & "                             @USU_EMAIL VARCHAR(100), @USU_TFNO2 VARCHAR(100),"
sConsulta = sConsulta & vbCrLf & "                             @USU_TFNO_MOVIL VARCHAR(100),@COD_PROVE_CIA VARCHAR(200),@ID_P INT)AS"
sConsulta = sConsulta & vbCrLf & ""
sConsulta = sConsulta & vbCrLf & "DECLARE @INDICE INT"
sConsulta = sConsulta & vbCrLf & ""
sConsulta = sConsulta & vbCrLf & "SET @INDICE = (SELECT MAX(ID) FROM CON WHERE PROVE = @COD_PROVE_CIA)"
sConsulta = sConsulta & vbCrLf & ""
sConsulta = sConsulta & vbCrLf & "IF @INDICE IS NULL"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & " SET @INDICE = 1"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "Else"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & " SET @INDICE = @INDICE + 1"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & ""
sConsulta = sConsulta & vbCrLf & "INSERT INTO CON (PROVE,ID,APE,NOM,DEP,CAR,TFNO,FAX,EMAIL,RECPET,TFNO2,PORT,TFNO_MOVIL,ID_PORT)"
sConsulta = sConsulta & vbCrLf & "             VALUES(@COD_PROVE_CIA,@INDICE,@USU_APE,@USU_NOM,@USU_DEP,@USU_CAR,@USU_TFNO,"
sConsulta = sConsulta & vbCrLf & "                      @USU_FAX,@USU_EMAIL,1,@USU_TFNO2,1,@USU_TFNO_MOVIL,@ID_P)"
sConsulta = sConsulta & vbCrLf & ""
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ACTUALIZAR_CONTACTO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_ACTUALIZAR_CONTACTO]" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = "CREATE PROCEDURE SP_ACTUALIZAR_CONTACTO (@USU_APE VARCHAR(100),@USU_NOM VARCHAR(20), @USU_DEP VARCHAR(100),"
sConsulta = sConsulta & vbCrLf & "                             @USU_CAR VARCHAR(100), @USU_TFNO VARCHAR(20), @USU_FAX VARCHAR(20),"
sConsulta = sConsulta & vbCrLf & "                             @USU_EMAIL VARCHAR(100), @USU_TFNO2 VARCHAR(20),"
sConsulta = sConsulta & vbCrLf & "                             @USU_TFNO_MOVIL VARCHAR(20),@COD_PROVE_CIA VARCHAR(200),@ID_P INT) AS"
sConsulta = sConsulta & vbCrLf & ""
sConsulta = sConsulta & vbCrLf & "UPDATE CON SET APE=@USU_APE, NOM=@USU_NOM, DEP=@USU_DEP, CAR=@USU_CAR, TFNO=@USU_TFNO, FAX=@USU_FAX, EMAIL=@USU_EMAIL,TFNO2=@USU_TFNO2,TFNO_MOVIL=@USU_TFNO_MOVIL"
sConsulta = sConsulta & vbCrLf & "    WHERE PROVE=@COD_PROVE_CIA AND ID_PORT=@ID_P"
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ELIMINAR_CONTACTO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_ELIMINAR_CONTACTO]" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = "CREATE PROCEDURE SP_ELIMINAR_CONTACTO (@COD_PROVE_CIA VARCHAR(200),@ID_P INT)  AS"
sConsulta = sConsulta & vbCrLf & ""
sConsulta = sConsulta & vbCrLf & "DELETE FROM CON WHERE PROVE = @COD_PROVE_CIA AND ID_PORT = @ID_P"
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ELIMINAR_CONTACTOS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_ELIMINAR_CONTACTOS]" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ANYA_PROVE_ESP]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_ANYA_PROVE_ESP]" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = "CREATE PROCEDURE SP_ANYA_PROVE_ESP (@PROVE VARCHAR(100),@ID SMALLINT,@NOM VARCHAR(300),@COM VARCHAR(500),@FECHA DATETIME)  AS"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "DELETE FROM PROVE_ESP_PORTAL WHERE PROVE=@PROVE"
sConsulta = sConsulta & vbCrLf & "INSERT INTO PROVE_ESP_PORTAL (PROVE,ID,NOM,COM,FECHA) VALUES (@PROVE,@ID,@NOM,@COM,@FECHA)"
sConsulta = sConsulta & vbCrLf & "Return"
sConsulta = sConsulta & vbCrLf & "End"
gRDOCon.Execute sConsulta, rdExecDirect

End Sub

Private Sub V_2_9_5_triggers_50_3()

Dim sConsulta As String

sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[ORDEN_ENTREGA_TG_UPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1) drop trigger [dbo].[ORDEN_ENTREGA_TG_UPD]"
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = ""
sConsulta = sConsulta & "CREATE TRIGGER ORDEN_ENTREGA_TG_UPD ON dbo.ORDEN_ENTREGA" & vbCrLf
sConsulta = sConsulta & "FOR UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @EST AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN1 AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVE AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @ITEM AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCE AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @CANT AS FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @LINEAID AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @DESPUB AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @CANT_PED AS FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @CANT_ADJ AS FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @TIPO AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @CAT1 AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @CAT2 AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @CAT3 AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @CAT4 AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @CAT5 AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @SEG AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @CAT_LIN_ID AS INTEGER" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_ORDEN_ENTREGA_Upd CURSOR FOR SELECT ID,EST,TIPO FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_ORDEN_ENTREGA_Upd" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_ORDEN_ENTREGA_Upd INTO @ID,@EST,@TIPO" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF UPDATE(EST) " & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "   IF @EST=2 " & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "       SET @DESPUB=(SELECT ELIMLINCATALOG FROM PARGEN_GEST)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       DECLARE cur_LineasOrden CURSOR FOR SELECT ANYO,GMN1,PROCE,ITEM,PROVE,ID,CANT_PED*FC  FROM LINEAS_PEDIDO WHERE ORDEN=@ID AND EST=1 AND ITEM IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "       OPEN cur_LineasOrden" & vbCrLf
sConsulta = sConsulta & "       FETCH NEXT FROM cur_LineasOrden INTO @ANYO,@GMN1,@PROCE,@ITEM,@PROVE,@LINEAID,@CANT" & vbCrLf
sConsulta = sConsulta & "       WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "           BEGIN" & vbCrLf
sConsulta = sConsulta & "           UPDATE ITEM_ADJ SET CANT_PED=CANT_PED + @CANT WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM AND PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "           SELECT  @CANT_PED= CANT_PED, @CANT_ADJ=CANT_ADJ FROM ITEM_ADJ WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM AND PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "           IF @DESPUB=1 AND (@CANT_ADJ<=@CANT_PED)" & vbCrLf
sConsulta = sConsulta & "             BEGIN" & vbCrLf
sConsulta = sConsulta & "               SELECT @CAT_LIN_ID = ID " & vbCrLf
sConsulta = sConsulta & "                 FROM CATALOG_LIN  " & vbCrLf
sConsulta = sConsulta & "                WHERE ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "                  AND GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "                  AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "                  AND ITEM=@ITEM " & vbCrLf
sConsulta = sConsulta & "                  AND PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "               DELETE FROM CAT_LIN_MIN " & vbCrLf
sConsulta = sConsulta & "                WHERE LINEA=@CAT_LIN_ID " & vbCrLf
sConsulta = sConsulta & "               DELETE FROM CATALOG_LIN " & vbCrLf
sConsulta = sConsulta & "                WHERE ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "                  AND GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "                  AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "                  AND ITEM=@ITEM " & vbCrLf
sConsulta = sConsulta & "                  AND PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "             END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "           FETCH NEXT FROM cur_LineasOrden INTO @ANYO,@GMN1,@PROCE,@ITEM,@PROVE,@LINEAID,@CANT" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "       Close cur_LineasOrden" & vbCrLf
sConsulta = sConsulta & "       DEALLOCATE cur_LineasOrden" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "   IF @TIPO=1 AND (@EST >=  6) " & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "     DECLARE cur_LineasOrden1 CURSOR FOR SELECT DISTINCT CAT1,CAT2,CAT3,CAT4,CAT5 FROM LINEAS_PEDIDO WHERE LINEAS_PEDIDO.ORDEN=@ID " & vbCrLf
sConsulta = sConsulta & "     OPEN cur_LineasOrden1" & vbCrLf
sConsulta = sConsulta & "     FETCH NEXT FROM cur_LineasOrden1 INTO @CAT1,@CAT2,@CAT3,@CAT4,@CAT5" & vbCrLf
sConsulta = sConsulta & "     WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "     BEGIN" & vbCrLf
sConsulta = sConsulta & "       IF @CAT5 IS NOT NULL AND (SELECT BAJALOG FROM CATN5 WHERE ID=@CAT5) = 1" & vbCrLf
sConsulta = sConsulta & "          IF (SELECT COUNT(*) FROM LINEAS_PEDIDO INNER JOIN ORDEN_ENTREGA ON LINEAS_PEDIDO.ORDEN=ORDEN_ENTREGA.ID WHERE CAT5=@CAT5 AND ORDEN_ENTREGA.EST<6)=0 " & vbCrLf
sConsulta = sConsulta & "          BEGIN" & vbCrLf
sConsulta = sConsulta & "             UPDATE CATN5 SET BAJALOG=2 WHERE ID=@CAT5   " & vbCrLf
sConsulta = sConsulta & "             SET @SEG=NULL" & vbCrLf
sConsulta = sConsulta & "             SELECT @SEG=SEGURIDAD FROM CAT_SEGURIDAD WHERE CAT=@CAT5 AND NIVEL=5" & vbCrLf
sConsulta = sConsulta & "             IF @SEG IS NOT NULL EXEC FSEP_ELIMINAR_SEGURIDAD @SEG              " & vbCrLf
sConsulta = sConsulta & "          END" & vbCrLf
sConsulta = sConsulta & "       IF @CAT4 IS NOT NULL AND (SELECT BAJALOG FROM CATN4 WHERE ID=@CAT4) = 1" & vbCrLf
sConsulta = sConsulta & "          IF (SELECT COUNT(*) FROM LINEAS_PEDIDO INNER JOIN ORDEN_ENTREGA ON LINEAS_PEDIDO.ORDEN=ORDEN_ENTREGA.ID WHERE CAT4=@CAT4 AND ORDEN_ENTREGA.EST<6)=0 " & vbCrLf
sConsulta = sConsulta & "          BEGIN" & vbCrLf
sConsulta = sConsulta & "               UPDATE CATN4 SET BAJALOG=2 WHERE ID=@CAT4" & vbCrLf
sConsulta = sConsulta & "              SET @SEG=NULL" & vbCrLf
sConsulta = sConsulta & "              SELECT @SEG=SEGURIDAD FROM CAT_SEGURIDAD WHERE CAT=@CAT4 AND NIVEL=4" & vbCrLf
sConsulta = sConsulta & "              IF @SEG IS NOT NULL EXEC FSEP_ELIMINAR_SEGURIDAD @SEG" & vbCrLf
sConsulta = sConsulta & "          END" & vbCrLf
sConsulta = sConsulta & "       IF @CAT3 IS NOT NULL AND (SELECT BAJALOG FROM CATN3 WHERE ID=@CAT3) = 1" & vbCrLf
sConsulta = sConsulta & "           IF (SELECT COUNT(*) FROM LINEAS_PEDIDO INNER JOIN ORDEN_ENTREGA ON LINEAS_PEDIDO.ORDEN=ORDEN_ENTREGA.ID WHERE CAT3=@CAT3 AND ORDEN_ENTREGA.EST<6)=0 " & vbCrLf
sConsulta = sConsulta & "           BEGIN" & vbCrLf
sConsulta = sConsulta & "               UPDATE CATN3 SET BAJALOG=2 WHERE ID=@CAT3" & vbCrLf
sConsulta = sConsulta & "               SET @SEG=NULL" & vbCrLf
sConsulta = sConsulta & "               SELECT @SEG=SEGURIDAD FROM CAT_SEGURIDAD WHERE CAT=@CAT3 AND NIVEL=3" & vbCrLf
sConsulta = sConsulta & "               IF @SEG IS NOT NULL EXEC FSEP_ELIMINAR_SEGURIDAD @SEG" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "       IF @CAT2 IS NOT NULL AND (SELECT BAJALOG FROM CATN2 WHERE ID=@CAT2) = 1 " & vbCrLf
sConsulta = sConsulta & "           IF (SELECT COUNT(*) FROM LINEAS_PEDIDO INNER JOIN ORDEN_ENTREGA ON LINEAS_PEDIDO.ORDEN=ORDEN_ENTREGA.ID WHERE CAT2=@CAT2 AND ORDEN_ENTREGA.EST<6)=0 " & vbCrLf
sConsulta = sConsulta & "           BEGIN" & vbCrLf
sConsulta = sConsulta & "               UPDATE CATN2 SET BAJALOG=2 WHERE ID=@CAT2" & vbCrLf
sConsulta = sConsulta & "               SET @SEG=NULL" & vbCrLf
sConsulta = sConsulta & "               SELECT @SEG=SEGURIDAD FROM CAT_SEGURIDAD WHERE CAT=@CAT2 AND NIVEL=2" & vbCrLf
sConsulta = sConsulta & "               IF @SEG IS NOT NULL EXEC FSEP_ELIMINAR_SEGURIDAD @SEG" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "       IF @CAT1 IS NOT NULL AND (SELECT BAJALOG FROM CATN1 WHERE ID=@CAT1) = 1" & vbCrLf
sConsulta = sConsulta & "           IF (SELECT COUNT(*) FROM LINEAS_PEDIDO INNER JOIN ORDEN_ENTREGA ON LINEAS_PEDIDO.ORDEN=ORDEN_ENTREGA.ID WHERE CAT1=@CAT1 AND ORDEN_ENTREGA.EST<6)=0 " & vbCrLf
sConsulta = sConsulta & "           BEGIN" & vbCrLf
sConsulta = sConsulta & "              UPDATE CATN1 SET BAJALOG=2 WHERE ID=@CAT1" & vbCrLf
sConsulta = sConsulta & "              SET @SEG=NULL" & vbCrLf
sConsulta = sConsulta & "              SELECT @SEG=SEGURIDAD FROM CAT_SEGURIDAD WHERE CAT=@CAT1 AND NIVEL=1" & vbCrLf
sConsulta = sConsulta & "              IF @SEG IS NOT NULL EXEC FSEP_ELIMINAR_SEGURIDAD @SEG" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "       FETCH NEXT FROM cur_LineasOrden1 INTO @CAT1,@CAT2,@CAT3,@CAT4,@CAT5" & vbCrLf
sConsulta = sConsulta & "     END" & vbCrLf
sConsulta = sConsulta & "     Close cur_LineasOrden1" & vbCrLf
sConsulta = sConsulta & "     DEALLOCATE cur_LineasOrden1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   END " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_ORDEN_ENTREGA_Upd INTO @ID,@EST,@TIPO" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "Close curTG_ORDEN_ENTREGA_Upd" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_ORDEN_ENTREGA_Upd" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[GMN4_ATRIB_TG_DEL]') and OBJECTPROPERTY(id, N'IsTrigger') = 1) drop trigger [dbo].[GMN4_ATRIB_TG_DEL]"
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = ""
sConsulta = sConsulta & "CREATE TRIGGER GMN4_ATRIB_TG_DEL ON dbo.GMN4_ATRIB" & vbCrLf
sConsulta = sConsulta & "FOR DELETE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN1 VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN2 VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN3 VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN4 VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @ATRIB INT" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_GMN4_ATRIB_Del CURSOR LOCAL FOR SELECT GMN1,GMN2,GMN3,GMN4, ATRIB FROM DELETED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_GMN4_ATRIB_Del" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_GMN4_ATRIB_Del INTO @GMN1,@GMN2,@GMN3,@GMN4,@ATRIB" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DELETE FROM PROVE_ART4_ATRIB WHERE GMN1 =@GMN1 AND GMN2 =@GMN2 AND GMN3 =@GMN3 AND GMN4 =@GMN4 AND ATRIB= @ATRIB" & vbCrLf
sConsulta = sConsulta & "DELETE FROM ART4_ATRIB WHERE GMN1 =@GMN1 AND GMN2 =@GMN2 AND GMN3 =@GMN3 AND GMN4 =@GMN4 AND ATRIB= @ATRIB" & vbCrLf
sConsulta = sConsulta & "IF (SELECT COUNT(*)  FROM ART4_ATRIB WHERE  ATRIB= @ATRIB) =0" & vbCrLf
sConsulta = sConsulta & "  IF (SELECT COUNT(*)  FROM GMN4_ATRIB WHERE  ATRIB= @ATRIB) =0" & vbCrLf
sConsulta = sConsulta & "      DELETE FROM DEF_ATRIB WHERE ID =@ATRIB" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_GMN4_ATRIB_Del INTO @GMN1,@GMN2,@GMN3,@GMN4,@ATRIB" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_GMN4_ATRIB_Del" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_GMN4_ATRIB_Del" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[APROB_LIM_TG_DEL]') and OBJECTPROPERTY(id, N'IsTrigger') = 1) drop trigger [dbo].[APROB_LIM_TG_DEL]"
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = ""
sConsulta = sConsulta & "CREATE  TRIGGER APROB_LIM_TG_DEL ON dbo.APROB_LIM" & vbCrLf
sConsulta = sConsulta & "FOR DELETE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @PER VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PERPAD VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @CODUSU VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @TIPO SMALLINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PADRE INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @COUNT INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @SEG INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @CAT INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @NIVEL INTEGER" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_Aprob_Lim_Del CURSOR FOR SELECT ID,PER,PADRE,SEGURIDAD FROM DELETED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_Aprob_Lim_Del" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_Aprob_Lim_Del INTO @ID,@PER,@PADRE,@SEG" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SELECT @CODUSU=COD,@TIPO=FSEPTIPO FROM USU WHERE PER =@PER" & vbCrLf
sConsulta = sConsulta & "   IF @TIPO =1 AND (SELECT COUNT(*) FROM APROB_LIM WHERE PER=@PER)=0" & vbCrLf
sConsulta = sConsulta & "       UPDATE USU SET FSEPTIPO=0 WHERE COD=@CODUSU" & vbCrLf
sConsulta = sConsulta & "   IF @TIPO=3 AND (SELECT COUNT(*) AS APROB FROM APROB_LIM A1 INNER JOIN APROB_LIM A2 ON A1.PADRE=A2.ID WHERE A2.PER =@PER )=0 -- No es aprobador tipo2" & vbCrLf
sConsulta = sConsulta & "         IF (SELECT COUNT(*) FROM SEGURIDAD WHERE APROB_TIPO1 =@PER)=0 -- No es aprobador tipo1" & vbCrLf
sConsulta = sConsulta & "                 UPDATE USU SET FSEPTIPO=0 WHERE COD=@CODUSU" & vbCrLf
sConsulta = sConsulta & "         ELSE " & vbCrLf
sConsulta = sConsulta & "                 UPDATE USU SET FSEPTIPO=2 WHERE COD=@CODUSU" & vbCrLf
sConsulta = sConsulta & "   IF @PADRE IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       SELECT @COUNT=COUNT(*) FROM APROB_LIM WHERE PADRE=@PADRE" & vbCrLf
sConsulta = sConsulta & "       IF @COUNT=0  --No es aprobador de tipo2" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "         SELECT @PERPAD=PER FROM APROB_LIM WHERE ID=@PADRE" & vbCrLf
sConsulta = sConsulta & "         SELECT @CODUSU=COD FROM USU WHERE PER =@PERPAD" & vbCrLf
sConsulta = sConsulta & "         IF (SELECT COUNT(*) AS APROB FROM APROB_LIM A1 INNER JOIN APROB_LIM A2 ON A1.PADRE=A2.ID WHERE A2.PER =@PERPAD )=0 -- No es aprobador tipo2" & vbCrLf
sConsulta = sConsulta & "             IF (SELECT COUNT(*) FROM SEGURIDAD WHERE APROB_TIPO1 =@PERPAD)=0 -- No es aprobador tipo1" & vbCrLf
sConsulta = sConsulta & "                 UPDATE USU SET FSEPTIPO=1 WHERE COD=@CODUSU" & vbCrLf
sConsulta = sConsulta & "            ELSE --Es aprobador tipo1" & vbCrLf
sConsulta = sConsulta & "                 IF @COUNT=0 UPDATE USU SET FSEPTIPO=2 WHERE COD=@CODUSU" & vbCrLf
sConsulta = sConsulta & "       End" & vbCrLf
sConsulta = sConsulta & "   End" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SELECT @CAT=CAT,@NIVEL =NIVEL FROM CAT_SEGURIDAD WHERE SEGURIDAD=@SEG" & vbCrLf
sConsulta = sConsulta & "   IF @NIVEL = 1" & vbCrLf
sConsulta = sConsulta & "       DELETE FROM CAT_PER WHERE PER=@PER AND CAT1=@CAT" & vbCrLf
sConsulta = sConsulta & "   Else" & vbCrLf
sConsulta = sConsulta & "      IF @NIVEL=2" & vbCrLf
sConsulta = sConsulta & "       DELETE FROM CAT_PER WHERE PER=@PER AND CAT2=@CAT" & vbCrLf
sConsulta = sConsulta & "      Else" & vbCrLf
sConsulta = sConsulta & "           IF @NIVEL=3" & vbCrLf
sConsulta = sConsulta & "       DELETE FROM CAT_PER WHERE PER=@PER AND CAT3=@CAT" & vbCrLf
sConsulta = sConsulta & "           Else" & vbCrLf
sConsulta = sConsulta & "       IF @NIVEL=4" & vbCrLf
sConsulta = sConsulta & "          DELETE FROM CAT_PER WHERE PER=@PER AND CAT4=@CAT" & vbCrLf
sConsulta = sConsulta & "       Else" & vbCrLf
sConsulta = sConsulta & "              DELETE FROM CAT_PER WHERE PER=@PER AND CAT5=@CAT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_Aprob_Lim_Del INTO @ID,@PER,@PADRE,@SEG" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_Aprob_Lim_Del" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_Aprob_Lim_Del" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

End Sub

Private Sub V_2_9_5_Storeds_50_3()

Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_NUM_PED]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_NUM_PED]" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect


sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE  FSEP_NUM_PED (@PROVE VARCHAR(50),@NUMORDEN INT OUTPUT, @NUMORDENNUE INT OUTPUT)  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @NUMORDEN=(SELECT COUNT(*) " & vbCrLf
sConsulta = sConsulta & "                 FROM ORDEN_ENTREGA OE" & vbCrLf
sConsulta = sConsulta & "                      INNER JOIN PEDIDO P" & vbCrLf
sConsulta = sConsulta & "                              ON OE.PEDIDO = P.ID" & vbCrLf
sConsulta = sConsulta & "                WHERE OE.PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "                  AND P.TIPO = 1" & vbCrLf
sConsulta = sConsulta & "                  AND (OE.EST=2 OR OE.EST=3 OR OE.EST=4 OR OE.EST=5 or OE.EST=21) )" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @NUMORDENNUE=(SELECT COUNT(*) " & vbCrLf
sConsulta = sConsulta & "                    FROM ORDEN_ENTREGA OE" & vbCrLf
sConsulta = sConsulta & "                      INNER JOIN PEDIDO P" & vbCrLf
sConsulta = sConsulta & "                              ON OE.PEDIDO = P.ID" & vbCrLf
sConsulta = sConsulta & "                   WHERE OE.PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "                     AND P.TIPO = 1" & vbCrLf
sConsulta = sConsulta & "                     AND OE.EST=2)" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_DEVOLVER_PROCESOS_A_DESPUB]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_DEVOLVER_PROCESOS_A_DESPUB]" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE SP_DEVOLVER_PROCESOS_A_DESPUB(@EQP VARCHAR(50), @COM VARCHAR(50),@DESDES INT ,@DESADJ INT,@RESPADJ TINYINT,@RESPPUB TINYINT) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @RESPADJ=0  AND @RESPPUB=0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "     SELECT DISTINCT PROCE.ANYO,PROCE.GMN1,PROCE.COD,PROCE.DEN,PROCE.FECLIMOFE,PROCE.ADJDIR," & vbCrLf
sConsulta = sConsulta & "     PROCE.FECPRES,PROCE_PROVE.EQP,PROCE_PROVE.COM,PROCE_PROVE.PUB,PROCE.USUAPER," & vbCrLf
sConsulta = sConsulta & "     Proce.FECNEC , Proce.GMN2, Proce.GMN3, Proce.GMN4, Proce.EST,PROCE.FECAPE" & vbCrLf
sConsulta = sConsulta & "     From Proce" & vbCrLf
sConsulta = sConsulta & "     INNER JOIN PROCE_PROVE ON" & vbCrLf
sConsulta = sConsulta & "     Proce.anyo = PROCE_PROVE.anyo And Proce.gmn1 = PROCE_PROVE.gmn1 And Proce.COD = PROCE_PROVE.Proce" & vbCrLf
sConsulta = sConsulta & "     AND PROCE_PROVE.EQP=@EQP AND PROCE_PROVE.COM=@COM" & vbCrLf
sConsulta = sConsulta & "     " & vbCrLf
sConsulta = sConsulta & "     WHERE ((PROCE_PROVE.PUB=1 AND convert(char,getdate(),111) <=PROCE.FECLIMOFE AND dateadd(day, @DESDES   ,PROCE.FECLIMOFE)<=convert(char,getdate(),111))" & vbCrLf
sConsulta = sConsulta & "     " & vbCrLf
sConsulta = sConsulta & "     OR (convert(char,getdate(),111)<=PROCE.FECPRES AND dateadd(day, @DESADJ   ,PROCE.FECPRES)<=convert(char,getdate(),111)))" & vbCrLf
sConsulta = sConsulta & "    " & vbCrLf
sConsulta = sConsulta & "     AND PROCE.EST>2 AND PROCE.EST<12" & vbCrLf
sConsulta = sConsulta & "     " & vbCrLf
sConsulta = sConsulta & "     ORDER BY PROCE.FECLIMOFE" & vbCrLf
sConsulta = sConsulta & "  " & vbCrLf
sConsulta = sConsulta & "Else" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  IF @RESPADJ=0  AND @RESPPUB=1" & vbCrLf
sConsulta = sConsulta & "  " & vbCrLf
sConsulta = sConsulta & "     SELECT DISTINCT PROCE.ANYO,PROCE.GMN1,PROCE.COD,PROCE.DEN,PROCE.FECLIMOFE,PROCE.ADJDIR," & vbCrLf
sConsulta = sConsulta & "     PROCE.FECPRES,PROCE_PROVE.EQP,PROCE_PROVE.COM,PROCE_PROVE.PUB,PROCE.USUAPER," & vbCrLf
sConsulta = sConsulta & "     Proce.FECNEC , Proce.GMN2, Proce.GMN3, Proce.GMN4, Proce.EST,PROCE.FECAPE" & vbCrLf
sConsulta = sConsulta & "     From Proce" & vbCrLf
sConsulta = sConsulta & "     INNER JOIN PROCE_PROVE ON" & vbCrLf
sConsulta = sConsulta & "     Proce.anyo = PROCE_PROVE.anyo And Proce.gmn1 = PROCE_PROVE.gmn1 And Proce.COD = PROCE_PROVE.Proce" & vbCrLf
sConsulta = sConsulta & "     AND PROCE_PROVE.EQP=@EQP AND PROCE_PROVE.COM=@COM" & vbCrLf
sConsulta = sConsulta & "    " & vbCrLf
sConsulta = sConsulta & "     WHERE ((PROCE_PROVE.PUB=1 AND convert(char,getdate(),111) <=PROCE.FECLIMOFE AND dateadd(day, @DESDES   ,PROCE.FECLIMOFE)<=convert(char,getdate(),111)" & vbCrLf
sConsulta = sConsulta & "     AND PROCE.EQP=@EQP AND PROCE.COM=@COM)" & vbCrLf
sConsulta = sConsulta & "     " & vbCrLf
sConsulta = sConsulta & "     OR (convert(char,getdate(),111)<=PROCE.FECPRES AND dateadd(day,@DESADJ   ,PROCE.FECPRES)<=convert(char,getdate(),111)))" & vbCrLf
sConsulta = sConsulta & "    " & vbCrLf
sConsulta = sConsulta & "     AND PROCE.EST>2 AND PROCE.EST<12" & vbCrLf
sConsulta = sConsulta & "     " & vbCrLf
sConsulta = sConsulta & "     ORDER BY PROCE.FECLIMOFE" & vbCrLf
sConsulta = sConsulta & "  Else" & vbCrLf
sConsulta = sConsulta & "    IF @RESPADJ=1  AND @RESPPUB=0" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "     SELECT DISTINCT PROCE.ANYO,PROCE.GMN1,PROCE.COD,PROCE.DEN,PROCE.FECLIMOFE,PROCE.ADJDIR," & vbCrLf
sConsulta = sConsulta & "     PROCE.FECPRES,PROCE_PROVE.EQP,PROCE_PROVE.COM,PROCE_PROVE.PUB,PROCE.USUAPER," & vbCrLf
sConsulta = sConsulta & "     Proce.FECNEC , Proce.GMN2, Proce.GMN3, Proce.GMN4, Proce.EST,PROCE.FECAPE" & vbCrLf
sConsulta = sConsulta & "     From Proce" & vbCrLf
sConsulta = sConsulta & "     INNER JOIN PROCE_PROVE ON" & vbCrLf
sConsulta = sConsulta & "     Proce.anyo = PROCE_PROVE.anyo And Proce.gmn1 = PROCE_PROVE.gmn1 And Proce.COD = PROCE_PROVE.Proce" & vbCrLf
sConsulta = sConsulta & "     AND PROCE_PROVE.EQP=@EQP AND PROCE_PROVE.COM=@COM" & vbCrLf
sConsulta = sConsulta & "         " & vbCrLf
sConsulta = sConsulta & "     WHERE ( (PROCE_PROVE.PUB=1 AND convert(char,getdate(),111)<=PROCE.FECLIMOFE AND dateadd(day, @DESDES   ,PROCE.FECLIMOFE)<=convert(char,getdate(),111) )" & vbCrLf
sConsulta = sConsulta & "    " & vbCrLf
sConsulta = sConsulta & "     " & vbCrLf
sConsulta = sConsulta & "     OR (convert(char,getdate(),111)<=PROCE.FECPRES AND dateadd(day, @DESADJ   ,PROCE.FECPRES)<=convert(char,getdate(),111)" & vbCrLf
sConsulta = sConsulta & "           AND PROCE.EQP=@EQP AND PROCE.COM=@COM))" & vbCrLf
sConsulta = sConsulta & "    " & vbCrLf
sConsulta = sConsulta & "     AND PROCE.EST>2 AND PROCE.EST<12" & vbCrLf
sConsulta = sConsulta & "     " & vbCrLf
sConsulta = sConsulta & "     ORDER BY PROCE.FECLIMOFE" & vbCrLf
sConsulta = sConsulta & "Else" & vbCrLf
sConsulta = sConsulta & "    IF @RESPADJ=1  AND @RESPPUB=1" & vbCrLf
sConsulta = sConsulta & "  " & vbCrLf
sConsulta = sConsulta & "     SELECT DISTINCT PROCE.ANYO,PROCE.GMN1,PROCE.COD,PROCE.DEN,PROCE.FECLIMOFE,PROCE.ADJDIR," & vbCrLf
sConsulta = sConsulta & "     PROCE.FECPRES,PROCE_PROVE.EQP,PROCE_PROVE.COM,PROCE_PROVE.PUB,PROCE.USUAPER," & vbCrLf
sConsulta = sConsulta & "     Proce.FECNEC , Proce.GMN2, Proce.GMN3, Proce.GMN4, Proce.EST,PROCE.FECAPE" & vbCrLf
sConsulta = sConsulta & "     From Proce" & vbCrLf
sConsulta = sConsulta & "     INNER JOIN PROCE_PROVE ON" & vbCrLf
sConsulta = sConsulta & "     Proce.anyo = PROCE_PROVE.anyo And Proce.gmn1 = PROCE_PROVE.gmn1 And Proce.COD = PROCE_PROVE.Proce" & vbCrLf
sConsulta = sConsulta & "     AND PROCE_PROVE.EQP=@EQP AND PROCE_PROVE.COM=@COM" & vbCrLf
sConsulta = sConsulta & "    " & vbCrLf
sConsulta = sConsulta & "     WHERE ((PROCE_PROVE.PUB=1 AND convert(char,getdate(),111)<=PROCE.FECLIMOFE AND dateadd(day, @DESDES   ,PROCE.FECLIMOFE)<=convert(char,getdate(),111)" & vbCrLf
sConsulta = sConsulta & "    AND PROCE.EQP=@EQP AND PROCE.COM=@COM)" & vbCrLf
sConsulta = sConsulta & "    " & vbCrLf
sConsulta = sConsulta & "     " & vbCrLf
sConsulta = sConsulta & "     OR (convert(char,getdate(),111)<=PROCE.FECPRES AND dateadd(day, @DESADJ   ,PROCE.FECPRES)<=convert(char,getdate(),111)" & vbCrLf
sConsulta = sConsulta & "            AND PROCE.EQP=@EQP AND PROCE.COM=@COM))" & vbCrLf
sConsulta = sConsulta & "    " & vbCrLf
sConsulta = sConsulta & "     AND PROCE.EST>2 AND PROCE.EST<12" & vbCrLf
sConsulta = sConsulta & "     " & vbCrLf
sConsulta = sConsulta & "     ORDER BY PROCE.FECLIMOFE" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect


End Sub

Private Sub V_2_9_5_Storeds_50_4()

Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ACTUALIZAR_CONTACTO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_ACTUALIZAR_CONTACTO]" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE SP_ACTUALIZAR_CONTACTO (@USU_APE VARCHAR(100),@USU_NOM VARCHAR(20), @USU_DEP VARCHAR(100),"
sConsulta = sConsulta & vbCrLf & "                             @USU_CAR VARCHAR(100), @USU_TFNO VARCHAR(20), @USU_FAX VARCHAR(20),"
sConsulta = sConsulta & vbCrLf & "                             @USU_EMAIL VARCHAR(100), @USU_TFNO2 VARCHAR(20),"
sConsulta = sConsulta & vbCrLf & "                             @USU_TFNO_MOVIL VARCHAR(20),@COD_PROVE_CIA VARCHAR(200),@ID_P INT) AS"
sConsulta = sConsulta & vbCrLf & "DECLARE @INDICE INT"
sConsulta = sConsulta & vbCrLf & ""
sConsulta = sConsulta & vbCrLf & "IF (SELECT COUNT(*) FROM CON WHERE PROVE=@COD_PROVE_CIA AND ID_PORT=@ID_P)=0"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "    SET @INDICE = (SELECT MAX(ID) FROM CON WHERE PROVE = @COD_PROVE_CIA)"
sConsulta = sConsulta & vbCrLf & ""
sConsulta = sConsulta & vbCrLf & "    IF @INDICE IS NULL"
sConsulta = sConsulta & vbCrLf & "    BEGIN"
sConsulta = sConsulta & vbCrLf & "     SET @INDICE = 1"
sConsulta = sConsulta & vbCrLf & "    End"
sConsulta = sConsulta & vbCrLf & "    Else"
sConsulta = sConsulta & vbCrLf & "    BEGIN"
sConsulta = sConsulta & vbCrLf & "     SET @INDICE = @INDICE + 1"
sConsulta = sConsulta & vbCrLf & "    End"
sConsulta = sConsulta & vbCrLf & ""
sConsulta = sConsulta & vbCrLf & "    INSERT INTO CON (PROVE,ID,APE,NOM,DEP,CAR,TFNO,FAX,EMAIL,RECPET,TFNO2,PORT,TFNO_MOVIL,ID_PORT)"
sConsulta = sConsulta & vbCrLf & "             VALUES(@COD_PROVE_CIA,@INDICE,@USU_APE,@USU_NOM,@USU_DEP,@USU_CAR,@USU_TFNO,"
sConsulta = sConsulta & vbCrLf & "                      @USU_FAX,@USU_EMAIL,1,@USU_TFNO2,1,@USU_TFNO_MOVIL,@ID_P)"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "Else"
sConsulta = sConsulta & vbCrLf & "UPDATE CON SET APE=@USU_APE, NOM=@USU_NOM, DEP=@USU_DEP, CAR=@USU_CAR, TFNO=@USU_TFNO, FAX=@USU_FAX, EMAIL=@USU_EMAIL,TFNO2=@USU_TFNO2,TFNO_MOVIL=@USU_TFNO_MOVIL"
sConsulta = sConsulta & vbCrLf & "    WHERE PROVE=@COD_PROVE_CIA AND ID_PORT=@ID_P"
sConsulta = sConsulta & vbCrLf & ""
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ACTUALIZAR_PROV_GS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_ACTUALIZAR_PROV_GS]" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = "CREATE PROCEDURE SP_ACTUALIZAR_PROV_GS (@COD_PROVE_CIA VARCHAR(40),@CIAS_DEN VARCHAR(100),"
sConsulta = sConsulta & vbCrLf & "                              @CIAS_NIF VARCHAR(20),@CIAS_PAI VARCHAR(20),@CIAS_PROVI VARCHAR(20),"
sConsulta = sConsulta & vbCrLf & "                              @CIAS_CP VARCHAR(20),@CIAS_POB VARCHAR(100),@CIAS_DIR VARCHAR(255),"
sConsulta = sConsulta & vbCrLf & "                              @CIAS_MON VARCHAR(20),@CIAS_URLCIA VARCHAR(255)) AS"
sConsulta = sConsulta & vbCrLf & ""
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & ""
sConsulta = sConsulta & vbCrLf & "DECLARE @PAIS VARCHAR(100)"
sConsulta = sConsulta & vbCrLf & "DECLARE @MON VARCHAR(100)"
sConsulta = sConsulta & vbCrLf & "DECLARE @PROV VARCHAR(100)"
sConsulta = sConsulta & vbCrLf & ""
sConsulta = sConsulta & vbCrLf & "SET @PAIS = (SELECT COD FROM PAI WHERE FSP_COD=@CIAS_PAI)"
sConsulta = sConsulta & vbCrLf & "SET @PROV = (SELECT COD FROM PROVI WHERE  FSP_PAI=@CIAS_PAI and  FSP_COD=@CIAS_PROVI)"
sConsulta = sConsulta & vbCrLf & "SET @MON = (SELECT COD FROM MON WHERE FSP_COD=@CIAS_MON)"
sConsulta = sConsulta & vbCrLf & ""
sConsulta = sConsulta & vbCrLf & "-- Actualizamos los datos del proveedor de GS"
sConsulta = sConsulta & vbCrLf & ""
sConsulta = sConsulta & vbCrLf & "UPDATE PROVE SET DEN=@CIAS_DEN ,DIR=@CIAS_DIR ,CP=@CIAS_CP ,POB= @CIAS_POB ,PAI= @PAIS,"
sConsulta = sConsulta & vbCrLf & "                     PROVI=@PROV, MON=@MON,NIF=@CIAS_NIF,URLPROVE=@CIAS_URLCIA"
sConsulta = sConsulta & vbCrLf & "                 WHERE COD=@COD_PROVE_CIA"
sConsulta = sConsulta & vbCrLf & "End"
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[DEST_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[DEST_COD]" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE DEST_COD (@OLD VARCHAR(50),@NEW VARCHAR(50))  AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT ON" & vbCrLf
sConsulta = sConsulta & "BEGIN TRANSACTION" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR FOR SELECT COD FROM DEST WHERE COD=@OLD" & vbCrLf
sConsulta = sConsulta & "OPEN C" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C INTO @VAR_COD" & vbCrLf
sConsulta = sConsulta & "IF @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE DEST NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON0_DEST NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON1_DEST NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON2_DEST NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3_DEST NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART1_ADJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART2_ADJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART3_ADJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ADJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PARGEN_DEF NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "IF (SELECT DESTDEF FROM PARGEN_DEF) = @OLD " & vbCrLf
sConsulta = sConsulta & "  UPDATE PARGEN_DEF SET DESTDEF=@NEW" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--2.9" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATALOG_LIN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_PEDIDO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "----" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE DEST SET COD=@NEW WHERE COD=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON0_DEST SET DEST=@NEW WHERE DEST=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON1_DEST SET DEST=@NEW WHERE DEST=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON2_DEST SET DEST=@NEW WHERE DEST=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON3_DEST SET DEST=@NEW WHERE DEST=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM SET DEST=@NEW WHERE DEST=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART1_ADJ SET DEST=@NEW WHERE DEST=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART2_ADJ SET DEST=@NEW WHERE DEST=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART3_ADJ SET DEST=@NEW WHERE DEST=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4_ADJ SET DEST=@NEW WHERE DEST=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PARGEN_DEF SET DESTDEF=@NEW WHERE DESTDEF=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM SET DEST=@NEW WHERE DEST=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES SET DEST=@NEW WHERE DEST=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_UON1 SET DEST=@NEW WHERE DEST=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_UON2 SET DEST=@NEW WHERE DEST=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_UON3 SET DEST=@NEW WHERE DEST=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON1 SET DEST=@NEW WHERE DEST=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON2 SET DEST=@NEW WHERE DEST=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON3 SET DEST=@NEW WHERE DEST=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON4 SET DEST=@NEW WHERE DEST=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY1 SET DEST=@NEW WHERE DEST=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY2 SET DEST=@NEW WHERE DEST=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY3 SET DEST=@NEW WHERE DEST=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY4 SET DEST=@NEW WHERE DEST=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_1 SET DEST=@NEW WHERE DEST=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_2 SET DEST=@NEW WHERE DEST=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_3 SET DEST=@NEW WHERE DEST=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_4 SET DEST=@NEW WHERE DEST=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_1 SET DEST=@NEW WHERE DEST=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_2 SET DEST=@NEW WHERE DEST=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_3 SET DEST=@NEW WHERE DEST=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_4 SET DEST=@NEW WHERE DEST=@OLD" & vbCrLf
sConsulta = sConsulta & "--2.9" & vbCrLf
sConsulta = sConsulta & "UPDATE CATALOG_LIN SET DEST=@NEW WHERE DEST=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE LINEAS_PEDIDO SET DEST=@NEW WHERE DEST=@OLD" & vbCrLf
sConsulta = sConsulta & "----" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE DEST CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON0_DEST CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON1_DEST CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON2_DEST CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3_DEST CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART1_ADJ CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART2_ADJ CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART3_ADJ CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ADJ CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PARGEN_DEF CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--2.9" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATALOG_LIN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_PEDIDO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "----" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close C" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "COMMIT TRANSACTION"
gRDOCon.Execute sConsulta, rdExecDirect



End Sub


Private Sub V_2_9_5_Storeds_50_5()
Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[DEP_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[DEP_COD]" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = "CREATE PROCEDURE DEP_COD (@OLD VARCHAR(50),@NEW VARCHAR(50))  AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT ON" & vbCrLf
sConsulta = sConsulta & "BEGIN TRANSACTION" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR FOR SELECT COD FROM DEP WHERE COD=@OLD" & vbCrLf
sConsulta = sConsulta & "OPEN C" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C INTO @VAR_COD" & vbCrLf
sConsulta = sConsulta & "IF @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE DEP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON0_DEP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON1_DEP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON2_DEP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3_DEP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PER NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "UPDATE DEP SET COD=@NEW WHERE COD=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON0_DEP SET DEP=@NEW WHERE DEP=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON1_DEP SET DEP=@NEW WHERE DEP=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON2_DEP SET DEP=@NEW WHERE DEP=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON3_DEP SET DEP=@NEW WHERE DEP=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PER SET DEP=@NEW WHERE DEP=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE COM SET DEP=@NEW WHERE DEP=@OLD" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE DEP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON0_DEP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON1_DEP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON2_DEP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3_DEP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PER CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close C" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "COMMIT TRANSACTION" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[UON1_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[UON1_COD]" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = "CREATE PROCEDURE UON1_COD (@OLD VARCHAR(50),@NEW VARCHAR(50)) AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT ON" & vbCrLf
sConsulta = sConsulta & "BEGIN TRANSACTION" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR FOR SELECT COD FROM UON1 WHERE COD=@OLD" & vbCrLf
sConsulta = sConsulta & "OPEN C" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C INTO @VAR_COD" & vbCrLf
sConsulta = sConsulta & "IF @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON1_DEP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON2_DEP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3_DEP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PER NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON1_DEST NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON2_DEST NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3_DEST NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "UPDATE UON1 SET COD=@NEW WHERE COD=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON2 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON3 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON1_DEP SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON2_DEP SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON3_DEP SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PER SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE COM SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_UON1 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_UON2 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_UON3 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_UON1 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_UON2 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_UON3 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON1_DEST SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON2_DEST SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON3_DEST SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON1_DEP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON2_DEP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3_DEP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PER CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON1_DEST CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON2_DEST CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3_DEST CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close C" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "COMMIT TRANSACTION" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[UON2_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[UON2_COD]" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = "CREATE PROCEDURE UON2_COD (@OLD VARCHAR(50),@NEW VARCHAR(50), @UON1 VARCHAR(50))  AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT ON" & vbCrLf
sConsulta = sConsulta & "BEGIN TRANSACTION" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR FOR SELECT COD FROM UON2 WHERE UON1=@UON1 AND COD=@OLD" & vbCrLf
sConsulta = sConsulta & "OPEN C" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C INTO @VAR_COD" & vbCrLf
sConsulta = sConsulta & "IF @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON2_DEP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3_DEP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PER NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON2_DEST NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3_DEST NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "UPDATE UON2 SET COD=@NEW WHERE UON1=@UON1 AND COD=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON3 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON2_DEP SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON3_DEP SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PER SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE COM SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_UON2 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_UON3 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_UON2 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_UON3 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON2_DEST SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON3_DEST SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON2_DEP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3_DEP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PER CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON2_DEST CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3_DEST CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close C" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "COMMIT TRANSACTION" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[UON3_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[UON3_COD]" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = "CREATE PROCEDURE UON3_COD (@OLD VARCHAR(50),@NEW VARCHAR(50), @UON1 VARCHAR(50), @UON2 VARCHAR(50))  AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT ON" & vbCrLf
sConsulta = sConsulta & "BEGIN TRANSACTION" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR FOR SELECT COD FROM UON3 WHERE UON1=@UON1 AND UON2=@UON2 AND COD=@OLD" & vbCrLf
sConsulta = sConsulta & "OPEN C" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C INTO @VAR_COD" & vbCrLf
sConsulta = sConsulta & "IF @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3_DEP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PER NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3_DEST NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "UPDATE UON3 SET COD=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND COD=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON3_DEP SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PER SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE COM SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_UON3 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_UON3 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON3_DEST SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3_DEP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PER CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3_DEST CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close C" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "COMMIT TRANSACTION" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
'a�ADO FK A COM
sConsulta = "ALTER TABLE [dbo].[COM] ADD CONSTRAINT"
    sConsulta = sConsulta & vbLf & "[FK_COM_UON1] FOREIGN KEY"
    sConsulta = sConsulta & vbLf & "("
    sConsulta = sConsulta & vbLf & "[UON1]"
    sConsulta = sConsulta & vbLf & ") REFERENCES [dbo].[UON1]"
    sConsulta = sConsulta & vbLf & "("
    sConsulta = sConsulta & vbLf & "[COD]"
    sConsulta = sConsulta & vbLf & ")"
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = "ALTER TABLE [dbo].[COM] ADD CONSTRAINT"
    sConsulta = sConsulta & vbLf & "[FK_COM_UON2] FOREIGN KEY"
    sConsulta = sConsulta & vbLf & "("
    sConsulta = sConsulta & vbLf & "[UON1],"
    sConsulta = sConsulta & vbLf & "[UON2]"
    sConsulta = sConsulta & vbLf & ") REFERENCES [dbo].[UON2]"
    sConsulta = sConsulta & vbLf & "("
    sConsulta = sConsulta & vbLf & "[UON1],"
    sConsulta = sConsulta & vbLf & "[COD]"
    sConsulta = sConsulta & vbLf & ")"
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = "ALTER TABLE [dbo].[COM] ADD CONSTRAINT"
    sConsulta = sConsulta & vbLf & "[FK_COM_UON3] FOREIGN KEY"
    sConsulta = sConsulta & vbLf & "("
    sConsulta = sConsulta & vbLf & "[UON1],"
    sConsulta = sConsulta & vbLf & "[UON2],"
    sConsulta = sConsulta & vbLf & "[UON3]"
    sConsulta = sConsulta & vbLf & ") REFERENCES [dbo].[UON3]"
    sConsulta = sConsulta & vbLf & "("
    sConsulta = sConsulta & vbLf & "[UON1],"
    sConsulta = sConsulta & vbLf & "[UON2],"
    sConsulta = sConsulta & vbLf & "[COD]"
    sConsulta = sConsulta & vbLf & ")"
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = "ALTER TABLE [dbo].[COM] ADD CONSTRAINT"
    sConsulta = sConsulta & vbLf & "[FK_COM_DEP] FOREIGN KEY"
    sConsulta = sConsulta & vbLf & "("
    sConsulta = sConsulta & vbLf & "[DEP]"
    sConsulta = sConsulta & vbLf & ") REFERENCES [dbo].[DEP]"
    sConsulta = sConsulta & vbLf & "("
    sConsulta = sConsulta & vbLf & "[COD]"
    sConsulta = sConsulta & vbLf & ")"
gRDOCon.Execute sConsulta, rdExecDirect

End Sub


Private Sub V_2_9_5_Storeds_50_6()
Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PRES_4_CON4_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[PRES_4_CON4_COD]" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = "CREATE PROCEDURE PRES_4_CON4_COD (@OLD VARCHAR(50),@NEW VARCHAR(50),@PRES1 VARCHAR(50),@PRES2 VARCHAR(50),@PRES3 VARCHAR(50))  AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT ON" & vbCrLf
sConsulta = sConsulta & "BEGIN TRANSACTION" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR LOCAL FOR SELECT COD FROM PRES_4_CON4 WHERE PRES1=@PRES1 AND PRES2=@PRES2 AND PRES3=@PRES3 AND COD=@OLD" & vbCrLf
sConsulta = sConsulta & "OPEN C" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C INTO @VAR_COD" & vbCrLf
sConsulta = sConsulta & "IF @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_4_CON4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESCON4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES_4_CON4 SET COD=@NEW WHERE PRES1=@PRES1 AND PRES2=@PRES2 AND PRES3=@PRES3 AND COD=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_PRESCON4 SET PRES4=@NEW WHERE PRES1=@PRES1 AND PRES2=@PRES2 AND PRES3=@PRES3 AND PRES4=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_4 SET PRES4=@NEW WHERE PRES1=@PRES1 AND PRES2=@PRES2 AND PRES3=@PRES3 AND PRES4=@OLD" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_4_CON4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESCON4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close C" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "COMMIT TRANSACTION" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT OFF" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
 

End Sub

Private Sub V_2_9_5_Storeds_50_8()
Dim sConsulta As String


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_DEVOLVER_LINEAS_DE_ORDEN]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_DEVOLVER_LINEAS_DE_ORDEN]" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE FSEP_DEVOLVER_LINEAS_DE_ORDEN(@APROBADOR VARCHAR(50), @ORDENID INTEGER )  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(1500)" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if @APROBADOR IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT LP.ID AS LINEAID," & vbCrLf
sConsulta = sConsulta & "                LP.ART_INT + ITEM.ART CODART," & vbCrLf
sConsulta = sConsulta & "                ART4.DEN + ITEM.DESCR  + ART_AUX.DEN DENART," & vbCrLf
sConsulta = sConsulta & "                LP.ART_INT + ITEM.ART + ' ' + ART4.DEN + ITEM.DESCR  + ART_AUX.DEN AS ARTICULO," & vbCrLf
sConsulta = sConsulta & "                PA.COD_EXT ART_EXT," & vbCrLf
sConsulta = sConsulta & "                LP.GMN1," & vbCrLf
sConsulta = sConsulta & "                LP.GMN2," & vbCrLf
sConsulta = sConsulta & "                LP.GMN3," & vbCrLf
sConsulta = sConsulta & "                LP.GMN4," & vbCrLf
sConsulta = sConsulta & "       LP.PROVE," & vbCrLf
sConsulta = sConsulta & "                LP.DEST AS DESTINO," & vbCrLf
sConsulta = sConsulta & "                LP.UP AS UNIDAD," & vbCrLf
sConsulta = sConsulta & "                LP.PREC_UP AS PRECIOUNITARIO," & vbCrLf
sConsulta = sConsulta & "                LP.CANT_PED AS CANTIDAD," & vbCrLf
sConsulta = sConsulta & "                LP.CANT_REC AS CANTIDAD_REC," & vbCrLf
sConsulta = sConsulta & "                ITEM_ADJ.CANT_ADJ / LP.FC AS CANTADJUDICADA," & vbCrLf
sConsulta = sConsulta & "                ITEM_ADJ.CANT_PED / LP.FC AS CANTCONSUMIDA," & vbCrLf
sConsulta = sConsulta & "                LP.PREC_UP * LP.CANT_PED IMPORTE," & vbCrLf
sConsulta = sConsulta & "                LP.EST AS APROBADA," & vbCrLf
sConsulta = sConsulta & "                LP.FECEMISION AS FECHA," & vbCrLf
sConsulta = sConsulta & "                GESTIONABLE=CASE WHEN APROB_LIM.PER= @APROBADOR   THEN 1 WHEN (APROB_LIM.PER IS NULL AND SEGURIDAD.APROB_TIPO1=@APROBADOR ) THEN 1  ELSE 0 END," & vbCrLf
sConsulta = sConsulta & "                LP.CAT1," & vbCrLf
sConsulta = sConsulta & "                LP.CAT2," & vbCrLf
sConsulta = sConsulta & "                LP.CAT3," & vbCrLf
sConsulta = sConsulta & "                LP.CAT4," & vbCrLf
sConsulta = sConsulta & "                LP.CAT5," & vbCrLf
sConsulta = sConsulta & "                LP.OBS," & vbCrLf
sConsulta = sConsulta & "                case when c5.cod is null then " & vbCrLf
sConsulta = sConsulta & "                        case when c4.cod is null then " & vbCrLf
sConsulta = sConsulta & "                             case when c3.cod is null then" & vbCrLf
sConsulta = sConsulta & "                                  case when c2.cod is null then" & vbCrLf
sConsulta = sConsulta & "                                       c1.cod" & vbCrLf
sConsulta = sConsulta & "                                  else" & vbCrLf
sConsulta = sConsulta & "                                  c2.cod" & vbCrLf
sConsulta = sConsulta & "                                  end" & vbCrLf
sConsulta = sConsulta & "                             else" & vbCrLf
sConsulta = sConsulta & "                             c3.cod" & vbCrLf
sConsulta = sConsulta & "                             end" & vbCrLf
sConsulta = sConsulta & "                        else" & vbCrLf
sConsulta = sConsulta & "                        c4.cod" & vbCrLf
sConsulta = sConsulta & "                        end" & vbCrLf
sConsulta = sConsulta & "                    else" & vbCrLf
sConsulta = sConsulta & "                    c5.cod" & vbCrLf
sConsulta = sConsulta & "                    end CATEGORIA," & vbCrLf
sConsulta = sConsulta & "                case when c5.cod is null then " & vbCrLf
sConsulta = sConsulta & "                        case when c4.cod is null then " & vbCrLf
sConsulta = sConsulta & "                             case when c3.cod is null then" & vbCrLf
sConsulta = sConsulta & "                                  case when c2.cod is null then" & vbCrLf
sConsulta = sConsulta & "                                       '(' + c1.cod + ') ' + c1.den" & vbCrLf
sConsulta = sConsulta & "                                  else" & vbCrLf
sConsulta = sConsulta & "                                  '(' + c1.cod + ') ' + c1.den + ' - (' + c2.cod + ') ' + c2.den" & vbCrLf
sConsulta = sConsulta & "                                  end" & vbCrLf
sConsulta = sConsulta & "                             else" & vbCrLf
sConsulta = sConsulta & "                             '(' + c1.cod + ') ' + c1.den + ' - (' + c2.cod + ') ' + c2.den + ' - (' + c3.cod + ') ' + c3.den" & vbCrLf
sConsulta = sConsulta & "                             end" & vbCrLf
sConsulta = sConsulta & "                        else" & vbCrLf
sConsulta = sConsulta & "                        '(' + c1.cod + ') ' + c1.den + ' - (' + c2.cod + ') ' + c2.den + ' - (' + c3.cod + ') ' + c3.den + ' - (' + c4.cod + ') ' + c4.den" & vbCrLf
sConsulta = sConsulta & "                        end" & vbCrLf
sConsulta = sConsulta & "                    else" & vbCrLf
sConsulta = sConsulta & "                    '(' + c1.cod + ') ' + c1.den + ' - (' + c2.cod + ') ' + c2.den + ' - (' + c3.cod + ') ' + c3.den + ' - (' + c4.cod + ') ' + c4.den + ' - (' + c5.cod + ') ' + c5.den" & vbCrLf
sConsulta = sConsulta & "                    end CATEGORIARAMA" & vbCrLf
sConsulta = sConsulta & "FROM LINEAS_PEDIDO LP" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN APROB_LIM" & vbCrLf
sConsulta = sConsulta & "            ON LP.APROB_LIM=APROB_LIM.ID" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN SEGURIDAD " & vbCrLf
sConsulta = sConsulta & "            ON LP.SEGURIDAD=SEGURIDAD.ID" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN ITEM " & vbCrLf
sConsulta = sConsulta & "            ON LP.ANYO = ITEM.ANYO" & vbCrLf
sConsulta = sConsulta & "           AND LP.GMN1=ITEM.GMN1 " & vbCrLf
sConsulta = sConsulta & "           AND LP.PROCE=ITEM.PROCE " & vbCrLf
sConsulta = sConsulta & "           AND LP.ITEM=ITEM.ID" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN ART4 " & vbCrLf
sConsulta = sConsulta & "            ON LP.GMN1=ART4.GMN1 " & vbCrLf
sConsulta = sConsulta & "           AND LP.GMN2=ART4.GMN2 " & vbCrLf
sConsulta = sConsulta & "           AND LP.GMN3=ART4.GMN3 " & vbCrLf
sConsulta = sConsulta & "           AND LP.GMN4=ART4.GMN4 " & vbCrLf
sConsulta = sConsulta & "           AND LP.ART_INT=ART4.COD" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN ITEM_ADJ " & vbCrLf
sConsulta = sConsulta & "            ON LP.ITEM=ITEM_ADJ.ITEM " & vbCrLf
sConsulta = sConsulta & "           AND LP.ANYO = ITEM_ADJ.ANYO" & vbCrLf
sConsulta = sConsulta & "           AND LP.GMN1=ITEM_ADJ.GMN1 " & vbCrLf
sConsulta = sConsulta & "           AND LP.PROCE=ITEM_ADJ.PROCE " & vbCrLf
sConsulta = sConsulta & "           AND LP.PROVE=ITEM_ADJ.PROVE" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN ART4 ART_AUX " & vbCrLf
sConsulta = sConsulta & "            ON ITEM.ART=ART_AUX.COD " & vbCrLf
sConsulta = sConsulta & "           AND ART_AUX.GMN1=ITEM.GMN1 " & vbCrLf
sConsulta = sConsulta & "           AND ART_AUX.GMN2=ITEM.GMN2 " & vbCrLf
sConsulta = sConsulta & "           AND ART_AUX.GMN3=ITEM.GMN3 " & vbCrLf
sConsulta = sConsulta & "           AND ART_AUX.GMN4=ITEM.GMN4 " & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN PROVE_ART4 PA" & vbCrLf
sConsulta = sConsulta & "            ON LP.PROVE = PA.PROVE" & vbCrLf
sConsulta = sConsulta & "           AND LP.GMN1 = PA.GMN1" & vbCrLf
sConsulta = sConsulta & "           AND LP.GMN2 = PA.GMN2" & vbCrLf
sConsulta = sConsulta & "           AND LP.GMN3 = PA.GMN3" & vbCrLf
sConsulta = sConsulta & "           AND LP.GMN4 = PA.GMN4" & vbCrLf
sConsulta = sConsulta & "           AND CASE WHEN LP.ART_INT IS NULL THEN ITEM.ART ELSE LP.ART_INT END = PA.ART" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN CATN1 C1" & vbCrLf
sConsulta = sConsulta & "            ON LP.CAT1 = C1.ID" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN CATN2 C2" & vbCrLf
sConsulta = sConsulta & "            ON LP.CAT2 = C2.ID" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN CATN3 C3" & vbCrLf
sConsulta = sConsulta & "            ON LP.CAT3 = C3.ID" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN CATN4 C4" & vbCrLf
sConsulta = sConsulta & "            ON LP.CAT4 = C4.ID" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN CATN5 C5" & vbCrLf
sConsulta = sConsulta & "            ON LP.CAT5 = C5.ID" & vbCrLf
sConsulta = sConsulta & "WHERE LP.ORDEN=@ORDENID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT LP.ID LINEAID," & vbCrLf
sConsulta = sConsulta & "                LP.ART_INT + I.ART CODART," & vbCrLf
sConsulta = sConsulta & "                LP.GMN1," & vbCrLf
sConsulta = sConsulta & "                LP.GMN2," & vbCrLf
sConsulta = sConsulta & "                LP.GMN3," & vbCrLf
sConsulta = sConsulta & "                LP.GMN4," & vbCrLf
sConsulta = sConsulta & "       LP.PROVE," & vbCrLf
sConsulta = sConsulta & "                ART4.DEN + I.DESCR  + ART_AUX.DEN DENART," & vbCrLf
sConsulta = sConsulta & "                PA.COD_EXT ART_EXT," & vbCrLf
sConsulta = sConsulta & "                LP.DEST DESTINO," & vbCrLf
sConsulta = sConsulta & "                D.DEN DESTDEN," & vbCrLf
sConsulta = sConsulta & "                LP.UP UNIDAD," & vbCrLf
sConsulta = sConsulta & "                U.DEN UNIDEN," & vbCrLf
sConsulta = sConsulta & "                LP.PREC_UP PRECIOUNITARIO," & vbCrLf
sConsulta = sConsulta & "                LP.CANT_PED CANTIDAD," & vbCrLf
sConsulta = sConsulta & "                LP.CANT_REC CANT_REC," & vbCrLf
sConsulta = sConsulta & "                IA.CANT_ADJ * LP.FC AS CANTADJUDICADA," & vbCrLf
sConsulta = sConsulta & "                LP.PREC_UP * LP.CANT_PED IMPORTE," & vbCrLf
sConsulta = sConsulta & "                LP.EST AS APROBADA," & vbCrLf
sConsulta = sConsulta & "                LP.FECEMISION FECHA," & vbCrLf
sConsulta = sConsulta & "                LP.FECENTREGA FECENTREGA," & vbCrLf
sConsulta = sConsulta & "                LP.EST,LP.ENTREGA_OBL,LP.FECENTREGAPROVE," & vbCrLf
sConsulta = sConsulta & "                LP.CAT1," & vbCrLf
sConsulta = sConsulta & "                LP.CAT2," & vbCrLf
sConsulta = sConsulta & "                LP.CAT3," & vbCrLf
sConsulta = sConsulta & "                LP.CAT4," & vbCrLf
sConsulta = sConsulta & "                LP.CAT5," & vbCrLf
sConsulta = sConsulta & "                LP.OBS," & vbCrLf
sConsulta = sConsulta & "                case when c5.cod is null then " & vbCrLf
sConsulta = sConsulta & "                        case when c4.cod is null then " & vbCrLf
sConsulta = sConsulta & "                             case when c3.cod is null then" & vbCrLf
sConsulta = sConsulta & "                                  case when c2.cod is null then" & vbCrLf
sConsulta = sConsulta & "                                       c1.cod" & vbCrLf
sConsulta = sConsulta & "                                  else" & vbCrLf
sConsulta = sConsulta & "                                  c2.cod" & vbCrLf
sConsulta = sConsulta & "                                  end" & vbCrLf
sConsulta = sConsulta & "                             else" & vbCrLf
sConsulta = sConsulta & "                             c3.cod" & vbCrLf
sConsulta = sConsulta & "                             end" & vbCrLf
sConsulta = sConsulta & "                        else" & vbCrLf
sConsulta = sConsulta & "                        c4.cod" & vbCrLf
sConsulta = sConsulta & "                        end" & vbCrLf
sConsulta = sConsulta & "                    else" & vbCrLf
sConsulta = sConsulta & "                    c5.cod" & vbCrLf
sConsulta = sConsulta & "                    end CATEGORIA," & vbCrLf
sConsulta = sConsulta & "                case when c5.cod is null then " & vbCrLf
sConsulta = sConsulta & "                        case when c4.cod is null then " & vbCrLf
sConsulta = sConsulta & "                             case when c3.cod is null then" & vbCrLf
sConsulta = sConsulta & "                                  case when c2.cod is null then" & vbCrLf
sConsulta = sConsulta & "                                       '(' + c1.cod + ') ' + c1.den" & vbCrLf
sConsulta = sConsulta & "                                  else" & vbCrLf
sConsulta = sConsulta & "                                  '(' + c1.cod + ') ' + c1.den + ' - (' + c2.cod + ') ' + c2.den" & vbCrLf
sConsulta = sConsulta & "                                  end" & vbCrLf
sConsulta = sConsulta & "                             else" & vbCrLf
sConsulta = sConsulta & "                             '(' + c1.cod + ') ' + c1.den + ' - (' + c2.cod + ') ' + c2.den + ' - (' + c3.cod + ') ' + c3.den" & vbCrLf
sConsulta = sConsulta & "                             end" & vbCrLf
sConsulta = sConsulta & "                        else" & vbCrLf
sConsulta = sConsulta & "                        '(' + c1.cod + ') ' + c1.den + ' - (' + c2.cod + ') ' + c2.den + ' - (' + c3.cod + ') ' + c3.den + ' - (' + c4.cod + ') ' + c4.den" & vbCrLf
sConsulta = sConsulta & "                        end" & vbCrLf
sConsulta = sConsulta & "                    else" & vbCrLf
sConsulta = sConsulta & "                    '(' + c1.cod + ') ' + c1.den + ' - (' + c2.cod + ') ' + c2.den + ' - (' + c3.cod + ') ' + c3.den + ' - (' + c4.cod + ') ' + c4.den + ' - (' + c5.cod + ') ' + c5.den" & vbCrLf
sConsulta = sConsulta & "                    end CATEGORIARAMA," & vbCrLf
sConsulta = sConsulta & "               " & vbCrLf
sConsulta = sConsulta & "                        " & vbCrLf
sConsulta = sConsulta & "                C1.DEN CAT1DEN," & vbCrLf
sConsulta = sConsulta & "                C2.DEN CAT2DEN," & vbCrLf
sConsulta = sConsulta & "                C3.DEN CAT3DEN," & vbCrLf
sConsulta = sConsulta & "                C4.DEN CAT4DEN," & vbCrLf
sConsulta = sConsulta & "                C5.DEN CAT5DEN," & vbCrLf
sConsulta = sConsulta & "                LE.COMENT" & vbCrLf
sConsulta = sConsulta & " FROM LINEAS_PEDIDO LP" & vbCrLf
sConsulta = sConsulta & "     INNER JOIN DEST D" & vbCrLf
sConsulta = sConsulta & "             ON LP.DEST = D.COD" & vbCrLf
sConsulta = sConsulta & "     INNER JOIN UNI U" & vbCrLf
sConsulta = sConsulta & "             ON LP.UP = U.COD" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN ITEM I " & vbCrLf
sConsulta = sConsulta & "             ON LP.ANYO = I.ANYO" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN1=I.GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND LP.PROCE=I.PROCE " & vbCrLf
sConsulta = sConsulta & "            AND LP.ITEM=I.ID" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN PROVE_ART4 PA" & vbCrLf
sConsulta = sConsulta & "             ON LP.PROVE = PA.PROVE" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN1 = PA.GMN1" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN2 = PA.GMN2" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN3 = PA.GMN3" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN4 = PA.GMN4" & vbCrLf
sConsulta = sConsulta & "            AND CASE WHEN LP.ART_INT IS NULL THEN I.ART ELSE LP.ART_INT END = PA.ART" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN ART4 " & vbCrLf
sConsulta = sConsulta & "             ON LP.GMN1=ART4.GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN2=ART4.GMN2 " & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN3=ART4.GMN3 " & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN4=ART4.GMN4 " & vbCrLf
sConsulta = sConsulta & "            AND LP.ART_INT=ART4.COD" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN ITEM_ADJ IA " & vbCrLf
sConsulta = sConsulta & "             ON LP.ITEM = IA.ITEM " & vbCrLf
sConsulta = sConsulta & "            AND LP.ANYO = IA.ANYO" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN1 = IA.GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND LP.PROCE= IA.PROCE " & vbCrLf
sConsulta = sConsulta & "            AND LP.PROVE= IA.PROVE" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN ART4 ART_AUX " & vbCrLf
sConsulta = sConsulta & "             ON ART_AUX.COD=I.ART " & vbCrLf
sConsulta = sConsulta & "            AND ART_AUX.GMN1=I.GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND ART_AUX.GMN2=I.GMN2 " & vbCrLf
sConsulta = sConsulta & "            AND ART_AUX.GMN3=I.GMN3 " & vbCrLf
sConsulta = sConsulta & "            AND ART_AUX.GMN4=I.GMN4" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN CATN1 C1" & vbCrLf
sConsulta = sConsulta & "             ON LP.CAT1 = C1.ID" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN CATN2 C2" & vbCrLf
sConsulta = sConsulta & "             ON LP.CAT2 = C2.ID" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN CATN3 C3" & vbCrLf
sConsulta = sConsulta & "             ON LP.CAT3 = C3.ID" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN CATN4 C4" & vbCrLf
sConsulta = sConsulta & "             ON LP.CAT4 = C4.ID" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN CATN5 C5" & vbCrLf
sConsulta = sConsulta & "             ON LP.CAT5 = C5.ID" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN LINEAS_EST LE" & vbCrLf
sConsulta = sConsulta & "             ON LP.LINEAS_EST = LE.ID" & vbCrLf
sConsulta = sConsulta & " WHERE LP.ORDEN=@ORDENID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

End Sub

Private Sub V_2_9_5_Storeds_50_7()

Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_DEVOLVER_TODAS_ORDENES]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_DEVOLVER_TODAS_ORDENES]" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE FSEP_DEVOLVER_TODAS_ORDENES @MAXLINEAS integer, @DESDEID varchar(50)=null, @APROBADOR varchar(50) = null,@APROVISIONADOR varchar(50) = null, @ARTINT varchar(50) = null, @ARTDEN varchar(500) = null, @ARTEXT varchar(500) = null, @DESDEFECHA varchar(10) = null, @HASTAFECHA varchar(10) = null, @PROVECOD varchar(50) = null, @PROVEDEN varchar(500) = null,@NUMPEDPROVE varchar(100) = null,@ANYO int = null,@PEDIDO int = null,@ORDEN int = null,@EST int  = null, @ID int = null ,@RECINCORRECTO int = null AS " & vbCrLf
sConsulta = sConsulta & "declare @SQL nvarchar(4000)" & vbCrLf
sConsulta = sConsulta & "declare @PARAM nvarchar(500)" & vbCrLf
sConsulta = sConsulta & "declare @SQL1 nvarchar(4000)" & vbCrLf
sConsulta = sConsulta & "declare @SQL2 nvarchar(4000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "set  @SQL = N'" & vbCrLf
sConsulta = sConsulta & "SELECT TOP ' + CONVERT(varchar,@MAXLINEAS) + ' CONVERT(VARCHAR,OE.EST) + LEFT(''00000000'',8-LEN(CONVERT(VARCHAR,OE.ID))) + CONVERT(VARCHAR,OE.ID), " & vbCrLf
sConsulta = sConsulta & "       OE.ID, OE.ANYO, PE.NUM NUMPEDIDO, OE.NUM NUMORDEN, OE.PROVE PROVECOD, P.DEN PROVEDEN, " & vbCrLf
sConsulta = sConsulta & "       OE.NUMEXT, OE.FECHA FECHA, OE.EST EST, OE.IMPORTE IMPORTE, " & vbCrLf
sConsulta = sConsulta & "       P1.APE + '', '' + P1.NOM APROVISIONADOR,P1.APE,P1.NOM," & vbCrLf
sConsulta = sConsulta & "       OES.COMENT, OES.FECHA FECEST,OE.IMPORTE,OE.PEDIDO,OE.TIPO, OE.INCORRECTA" & vbCrLf
sConsulta = sConsulta & "  FROM ORDEN_ENTREGA OE" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN PROVE P" & vbCrLf
sConsulta = sConsulta & "            ON OE.PROVE = P.COD" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN PEDIDO PE" & vbCrLf
sConsulta = sConsulta & "            ON OE.PEDIDO = PE.ID '" & vbCrLf
sConsulta = sConsulta & "IF @APROBADOR IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN (SELECT DISTINCT ORDEN , S.APROB_TIPO1 PER" & vbCrLf
sConsulta = sConsulta & "                           FROM LINEAS_PEDIDO LP " & vbCrLf
sConsulta = sConsulta & "                                    INNER JOIN SEGURIDAD S" & vbCrLf
sConsulta = sConsulta & "                                            ON LP.SEGURIDAD = S.ID ) T1" & vbCrLf
sConsulta = sConsulta & "            ON OE.ID = T1.ORDEN" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN (SELECT DISTINCT ORDEN , AL.PER PER" & vbCrLf
sConsulta = sConsulta & "                            FROM LINEAS_PEDIDO LP " & vbCrLf
sConsulta = sConsulta & "                                     INNER JOIN APROB_LIM AL" & vbCrLf
sConsulta = sConsulta & "                                             ON LP.APROB_LIM = AL.ID) T2" & vbCrLf
sConsulta = sConsulta & "            ON OE.ID = T2.ORDEN '" & vbCrLf
sConsulta = sConsulta & "set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN PER P1" & vbCrLf
sConsulta = sConsulta & "            ON PE.PER = P1.COD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN ORDEN_EST OES" & vbCrLf
sConsulta = sConsulta & "            ON OE.ORDEN_EST = OES.ID'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if @ARTINT is not null or @ARTDEN is not null or @ARTEXT is not null" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "   set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN (SELECT DISTINCT ORDEN" & vbCrLf
sConsulta = sConsulta & "                  FROM LINEAS_PEDIDO LP" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN ITEM I " & vbCrLf
sConsulta = sConsulta & "             ON LP.ANYO = I.ANYO" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN1=I.GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND LP.PROCE=I.PROCE " & vbCrLf
sConsulta = sConsulta & "            AND LP.ITEM=I.ID'" & vbCrLf
sConsulta = sConsulta & "      IF @ARTDEN is NOT NULL " & vbCrLf
sConsulta = sConsulta & "         set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN ART4 " & vbCrLf
sConsulta = sConsulta & "             ON LP.GMN1=ART4.GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN2=ART4.GMN2 " & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN3=ART4.GMN3 " & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN4=ART4.GMN4 " & vbCrLf
sConsulta = sConsulta & "            AND LP.ART_INT=ART4.COD" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN ART4 ART_AUX " & vbCrLf
sConsulta = sConsulta & "             ON ART_AUX.COD=I.ART " & vbCrLf
sConsulta = sConsulta & "            AND ART_AUX.GMN1=I.GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND ART_AUX.GMN2=I.GMN2 " & vbCrLf
sConsulta = sConsulta & "            AND ART_AUX.GMN3=I.GMN3 " & vbCrLf
sConsulta = sConsulta & "            AND ART_AUX.GMN4=I.GMN4'" & vbCrLf
sConsulta = sConsulta & "      IF @ARTEXT is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN PROVE_ART4 PA" & vbCrLf
sConsulta = sConsulta & "             ON LP.PROVE = PA.PROVE" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN1 = PA.GMN1" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN2 = PA.GMN2" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN3 = PA.GMN3" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN4 = PA.GMN4" & vbCrLf
sConsulta = sConsulta & "            AND CASE WHEN LP.ART_INT IS NULL THEN I.ART ELSE LP.ART_INT END = PA.ART" & vbCrLf
sConsulta = sConsulta & "'" & vbCrLf
sConsulta = sConsulta & "set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & " WHERE 1=1'" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "   IF @ARTINT is NOT NULL " & vbCrLf
sConsulta = sConsulta & "      set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                   AND LP.ART_INT + I.ART like @ARTINT + ''%'' '" & vbCrLf
sConsulta = sConsulta & "   IF @ARTDEN is NOT NULL " & vbCrLf
sConsulta = sConsulta & "      set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                   AND ART4.DEN + I.DESCR  + ART_AUX.DEN like @ARTDEN + ''%'''" & vbCrLf
sConsulta = sConsulta & "     " & vbCrLf
sConsulta = sConsulta & "      IF @ARTEXT is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                   AND PA.COD_EXT like @ARTEXT + ''%''" & vbCrLf
sConsulta = sConsulta & "'" & vbCrLf
sConsulta = sConsulta & "   set  @SQL = @SQL + N') LP" & vbCrLf
sConsulta = sConsulta & "            ON OE.ID = LP.ORDEN" & vbCrLf
sConsulta = sConsulta & "'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                 WHERE PE.TIPO=1'" & vbCrLf
sConsulta = sConsulta & "        " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if @DESDEFECHA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.FECHA >= CONVERT(datetime,@DESDEFECHA,103)'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if @HASTAFECHA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.FECHA <= CONVERT(datetime,@HASTAFECHA,103)'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if @PROVECOD IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.PROVE = @PROVECOD'" & vbCrLf
sConsulta = sConsulta & "if @NUMPEDPROVE IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.NUMEXT = @NUMPEDPROVE '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if @ORDEN IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.NUM = @ORDEN '" & vbCrLf
sConsulta = sConsulta & "if @ANYO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.ANYO = @ANYO '" & vbCrLf
sConsulta = sConsulta & "if @PEDIDO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND PE.NUM = @PEDIDO '" & vbCrLf
sConsulta = sConsulta & "if @PROVEDEN IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND P.DEN  LIKE @PROVEDEN + ''%'' '" & vbCrLf
sConsulta = sConsulta & "IF @APROBADOR IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND (T1.PER = @APROBADOR " & vbCrLf
sConsulta = sConsulta & "                           OR T2.PER = @APROBADOR) '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if @APROVISIONADOR IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND PE.PER = @APROVISIONADOR '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "declare @IN varchar(500)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @IN = '('" & vbCrLf
sConsulta = sConsulta & "IF @EST & 1 = 1" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '0,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 2 = 2" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '1,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 4 = 4" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '2,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 8 = 8" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '3,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 16 = 16" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '4,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 32 = 32" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '5,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 64 = 64" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '6,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 128 = 128" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '20,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 256 = 256" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '21,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 512 = 512" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '22,'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL1 = ''" & vbCrLf
sConsulta = sConsulta & "SET @SQL2 = ''" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @IN!='('" & vbCrLf
sConsulta = sConsulta & "   --set @SQL = @SQL + '" & vbCrLf
sConsulta = sConsulta & "   --AND OE.EST IN ' + LEFT(@IN,LEN(@IN)-1) + ') '" & vbCrLf
sConsulta = sConsulta & "     set @SQL1=' OE.EST IN ' + LEFT(@IN,LEN(@IN)-1) + ') '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @RECINCORRECTO IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "  IF @RECINCORRECTO=1" & vbCrLf
sConsulta = sConsulta & "   --set @SQL = @SQL + '" & vbCrLf
sConsulta = sConsulta & "   --AND OE.INCORRECTA=1'" & vbCrLf
sConsulta = sConsulta & "   set @SQL2 = ' OE.INCORRECTA=1'  " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @SQL1<>'' and @SQL2<>'' " & vbCrLf
sConsulta = sConsulta & "   SET @SQL= @SQL + ' AND (' + @SQL1 + ' OR ' + @SQL2 + ')'" & vbCrLf
sConsulta = sConsulta & "ELSE " & vbCrLf
sConsulta = sConsulta & "if @SQL1!=''" & vbCrLf
sConsulta = sConsulta & "   SET @SQL= @SQL + ' AND ' + @SQL1 " & vbCrLf
sConsulta = sConsulta & "else" & vbCrLf
sConsulta = sConsulta & "if @SQL2!=''" & vbCrLf
sConsulta = sConsulta & "   SET @SQL= @SQL + ' AND ' + @SQL2" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "if @ID IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + '" & vbCrLf
sConsulta = sConsulta & "   AND OE.ID=@ID '" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "if @DESDEID IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + '" & vbCrLf
sConsulta = sConsulta & "   AND CONVERT(VARCHAR,OE.EST) + LEFT(''00000000'',8-LEN(CONVERT(VARCHAR,OE.ID))) + CONVERT(VARCHAR,OE.ID)>=@DESDEID '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "set @SQL = @SQL + '" & vbCrLf
sConsulta = sConsulta & "order by CONVERT(VARCHAR,OE.EST) + LEFT(''00000000'',8-LEN(CONVERT(VARCHAR,OE.ID))) + CONVERT(VARCHAR,OE.ID)'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @PARAM = N'@APROBADOR varchar(50), @APROVISIONADOR varchar(50), @DESDEFECHA varchar(10), @HASTAFECHA varchar(10), @ARTINT varchar(50), @ARTDEN varchar(500), @ARTEXT varchar(500), @PROVECOD varchar(50), @PROVEDEN varchar(500), @NUMPEDPROVE varchar(100),@ORDEN int, @ANYO int, @PEDIDO int, @DESDEID varchar(50), @EST int, @ID int'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, @PARAM, @APROBADOR= @APROBADOR, @APROVISIONADOR = @APROVISIONADOR, @DESDEFECHA=@DESDEFECHA, @HASTAFECHA=@HASTAFECHA, @ARTINT=@ARTINT, @ARTDEN=@ARTDEN, @ARTEXT =@ARTEXT , @PROVECOD=@PROVECOD, @PROVEDEN=@PROVEDEN, @NUMPEDPROVE=@NUMPEDPROVE, @ANYO=@ANYO, @ORDEN=@ORDEN, @PEDIDO = @PEDIDO,@DESDEID=@DESDEID, @EST=@EST, @ID = @ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ITEM]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_ITEM]" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE  SP_ITEM  (@ANYO INT,@GMN1 VARCHAR(50),@PROCE INT,@ITEM AS INT)  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    SELECT ITEM.ANYO, " & vbCrLf
sConsulta = sConsulta & "           ITEM.GMN1, " & vbCrLf
sConsulta = sConsulta & "           ITEM.PROCE, " & vbCrLf
sConsulta = sConsulta & "           ITEM.ID, " & vbCrLf
sConsulta = sConsulta & "           ITEM.GMN2, " & vbCrLf
sConsulta = sConsulta & "           ITEM.GMN3, " & vbCrLf
sConsulta = sConsulta & "           ITEM.GMN4, " & vbCrLf
sConsulta = sConsulta & "           ITEM.ART, " & vbCrLf
sConsulta = sConsulta & "           ITEM.DESCR + ART4.DEN AS DESCR, " & vbCrLf
sConsulta = sConsulta & "           ITEM.DEST, " & vbCrLf
sConsulta = sConsulta & "           ITEM.UNI, " & vbCrLf
sConsulta = sConsulta & "           ITEM.CANT, " & vbCrLf
sConsulta = sConsulta & "           ITEM.PREC," & vbCrLf
sConsulta = sConsulta & "           ITEM.PRES, " & vbCrLf
sConsulta = sConsulta & "           ITEM.PAG," & vbCrLf
sConsulta = sConsulta & "           ITEM.FECINI, " & vbCrLf
sConsulta = sConsulta & "           ITEM.FECFIN, " & vbCrLf
sConsulta = sConsulta & "           ITEM.ESP, " & vbCrLf
sConsulta = sConsulta & "           ITEM.CONF, " & vbCrLf
sConsulta = sConsulta & "           ITEM.OBJ, " & vbCrLf
sConsulta = sConsulta & "           ITEM.FECHAOBJ," & vbCrLf
sConsulta = sConsulta & "           ITEM.ANYO, " & vbCrLf
sConsulta = sConsulta & "           ITEM.GMN1, " & vbCrLf
sConsulta = sConsulta & "           ITEM.PROCE" & vbCrLf
sConsulta = sConsulta & "      FROM ITEM " & vbCrLf
sConsulta = sConsulta & "            LEFT JOIN ART4 " & vbCrLf
sConsulta = sConsulta & "                   ON ITEM.ART = ART4.COD " & vbCrLf
sConsulta = sConsulta & "                  AND ITEM.GMN4 = ART4.GMN4 " & vbCrLf
sConsulta = sConsulta & "                  AND ITEM.GMN3 = ART4.GMN3 " & vbCrLf
sConsulta = sConsulta & "                  AND ITEM.GMN2 = ART4.GMN2" & vbCrLf
sConsulta = sConsulta & "                  AND ITEM.GMN1 = ART4.GMN1" & vbCrLf
sConsulta = sConsulta & " WHERE item.ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "   AND item.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "   AND item.PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "   AND item.ID=@ITEM" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

End Sub



Private Sub V_2_9_5_Storeds_50_9()
Dim sConsulta As String


sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[PROCE_PROVE_PET_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1) drop trigger [dbo].[PROCE_PROVE_PET_TG_INS]"
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = ""
sConsulta = sConsulta & "CREATE TRIGGER PROCE_PROVE_PET_TG_INS ON dbo.PROCE_PROVE_PET" & vbCrLf
sConsulta = sConsulta & "FOR INSERT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN1 AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCE AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @EST AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @TIPOPET AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @FECHAREU AS DATETIME" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_PROCE_PROVE_PET_Ins CURSOR FOR SELECT ANYO,GMN1,PROCE,TIPOPET,FECHAREU FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_PROCE_PROVE_PET_Ins" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_PROVE_PET_Ins INTO @ANYO,@GMN1,@PROCE,@TIPOPET,@FECHAREU" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @EST =  (SELECT PROCE.EST FROM PROCE WHERE ANYO=@ANYO  AND GMN1=@GMN1 AND COD=@PROCE)" & vbCrLf
sConsulta = sConsulta & "IF @TIPOPET = 0 /*Peticiones de oferta*/" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @EST < 6  /* 3= Validado */" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET EST =6  WHERE ANYO= @ANYO  AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Else" & vbCrLf
sConsulta = sConsulta & "IF @TIPOPET = 1 /*Comunicando objetivos*/" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @EST = 8  /*Con objetivos sin notificar*/" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET EST=7 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "IF @EST = 9  /*Con objetivos sin notificar y preadjudicado*/" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET EST=10 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Else" & vbCrLf
sConsulta = sConsulta & "IF @TIPOPET = 2 OR @TIPOPET=3  /*Notificaci�n decierre*/" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @EST=12" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET EST=13 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_PROVE_PET_Ins INTO @ANYO,@GMN1,@PROCE,@TIPOPET,@FECHAREU" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_PROCE_PROVE_PET_Ins" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_PROCE_PROVE_PET_Ins" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf

gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[PROCE_PROVE_PET_TG_DEL]') and OBJECTPROPERTY(id, N'IsTrigger') = 1) drop trigger [dbo].[PROCE_PROVE_PET_TG_DEL]"
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = ""
sConsulta = sConsulta & "CREATE TRIGGER PROCE_PROVE_PET_TG_DEL ON dbo.PROCE_PROVE_PET" & vbCrLf
sConsulta = sConsulta & "FOR DELETE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN1 AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCE AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_PROCE_PROVE_PET_Del CURSOR FOR SELECT ANYO,GMN1,PROCE FROM DELETED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_PROCE_PROVE_PET_Del" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_PROVE_PET_Del INTO @ANYO,@GMN1,@PROCE" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "/* Si se han insertado proveedores, actualizamos el estado del proceso  si es que este era inferior a */" & vbCrLf
sConsulta = sConsulta & "IF (SELECT EST FROM PROCE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE) =6 /* 4=Con proveedores asignados */" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF (SELECT COUNT(*) FROM PROCE_PROVE_PET WHERE ANYO=@ANYO  AND GMN1=@GMN1 AND PROCE=@PROCE) = 0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET EST = 5  WHERE ANYO= @ANYO  AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_PROVE_PET_Del INTO @ANYO,@GMN1,@PROCE" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_PROCE_PROVE_PET_Del" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_PROCE_PROVE_PET_Del" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf

gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[PROCE_PROVE_PET_TG_DEL]') and OBJECTPROPERTY(id, N'IsTrigger') = 1) drop trigger [dbo].[PROCE_PROVE_PET_TG_DEL]"
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = ""
sConsulta = sConsulta & "CREATE TRIGGER PROCE_PROVE_PET_TG_DEL ON dbo.PROCE_PROVE_PET" & vbCrLf
sConsulta = sConsulta & "FOR DELETE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN1 AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCE AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_PROCE_PROVE_PET_Del CURSOR FOR SELECT ANYO,GMN1,PROCE FROM DELETED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_PROCE_PROVE_PET_Del" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_PROVE_PET_Del INTO @ANYO,@GMN1,@PROCE" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "/* Si se han insertado proveedores, actualizamos el estado del proceso  si es que este era inferior a */" & vbCrLf
sConsulta = sConsulta & "IF (SELECT EST FROM PROCE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE) =6 /* 4=Con proveedores asignados */" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF (SELECT COUNT(*) FROM PROCE_PROVE_PET WHERE ANYO=@ANYO  AND GMN1=@GMN1 AND PROCE=@PROCE) = 0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET EST = 5  WHERE ANYO= @ANYO  AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_PROVE_PET_Del INTO @ANYO,@GMN1,@PROCE" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_PROCE_PROVE_PET_Del" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_PROCE_PROVE_PET_Del" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf

gRDOCon.Execute sConsulta, rdExecDirect



End Sub


