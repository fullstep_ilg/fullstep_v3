VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CProvincias"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'********************* CProvincias **********************************
'*             Autor : Javier Arana
'*             Creada : 3/9/98
'****************************************************************

Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
   
End Enum
Private mCol As Collection
Private m_bEOF As Boolean
Private m_oConexion As CConexion
Private m_bPortal As Boolean
' Variables para el control de cambios del Log e Integración
'


Public Function Add(ByVal Cod As String, ByVal Den As String, Optional ByVal varIndice As Variant, Optional ByVal ID As Integer, Optional ByVal FecAct As Variant, Optional ByVal varEstado As Variant) As CProvincia
    
    Dim objnewmember As CProvincia
    Set objnewmember = New CProvincia
    
    objnewmember.Cod = Cod
    objnewmember.Den = Den
    objnewmember.FecAct = FecAct
    objnewmember.EstadoIntegracion = varEstado 'Estado integracion

    Set objnewmember.Conexion = m_oConexion
    
    
    If Not IsMissing(ID) Then
        objnewmember.ID = ID
    End If
    
    If Not IsMissing(varIndice) And Not IsNull(varIndice) Then
        objnewmember.Indice = varIndice
        mCol.Add objnewmember, CStr(varIndice)
    Else
       mCol.Add objnewmember, CStr(Cod)
    End If
    
    'return the object created
    Set Add = objnewmember
    Set objnewmember = Nothing


End Function
Public Property Let Portal(ByVal Data As Boolean)
    Let m_bPortal = Data
End Property

Public Property Get Item(vntIndexKey As Variant) As CProvincia

On Error GoTo NoSeEncuentra:

    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:

    Set Item = Nothing
    
End Property
Public Property Get eof() As Boolean
    eof = m_bEOF
End Property
Friend Property Let eof(ByVal b As Boolean)
    m_bEOF = b
End Property

Friend Property Set Conexion(ByVal con As CConexion)
Set m_oConexion = con
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = m_oConexion
End Property



Public Property Get Count() As Long

If mCol Is Nothing Then
    Count = 0
Else
     Count = mCol.Count
End If


End Property


Public Sub Remove(vntIndexKey As Variant)
   
    mCol.Remove vntIndexKey
End Sub


Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
     Set NewEnum = mCol.[_NewEnum]
End Property


Private Sub Class_Initialize()
 
    Set mCol = New Collection
End Sub


Private Sub Class_Terminate()
    'destroys collection when this class is terminated
    Set mCol = Nothing
    Set m_oConexion = Nothing

End Sub

