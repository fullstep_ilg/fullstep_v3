Attribute VB_Name = "bas_V_31900_8_2_HF8"
'@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
''' Desde el 31900.8.002 HF8 al HF12
'@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
Option Explicit



Public Function CodigoDeActualizacion31900_08_1897_A31900_08_1898() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim oADOConnection As ADODB.Connection
    Dim sConsulta As String

    On Error GoTo Error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    
    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Actualiza Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    frmProgreso.ProgressBar1.Refresh
    V_31900_8_Storeds_098
    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Actualiza Datos"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    frmProgreso.ProgressBar1.Refresh
    V_31900_8_Datos_098
    
    sConsulta = "UPDATE VERSION SET NUM ='3.00.18.98'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103), PRODVER='31900.8'"
    ExecuteSQL gRDOCon, sConsulta
        
    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False
        
    CodigoDeActualizacion31900_08_1897_A31900_08_1898 = True
    Exit Function

Error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    CodigoDeActualizacion31900_08_1897_A31900_08_1898 = False
End Function

Public Sub V_31900_8_Storeds_098()
Dim sConsulta As String
        
sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSQA_GETINSTINVISIBLEFIELDS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSQA_GETINSTINVISIBLEFIELDS]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSQA_GETINSTINVISIBLEFIELDS] @IDCAMPO INT,@USU VARCHAR(50)=NULL, @PROVE VARCHAR(50) = NULL AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @INSTANCIA INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @VERSION INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @TIPO TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @CCD INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @ESTADO AS INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @APROB AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @ESCRITURA_CERTIF AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @HAYCONFCUMP TINYINT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @INSTANCIA =CC.INSTANCIA, @VERSION =CC.NUM_VERSION" & vbCrLf
sConsulta = sConsulta & "FROM COPIA_CAMPO CC WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "WHERE CC.ID = @IDCAMPO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE  @SQL NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE  @SQLCOMMON NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Campos" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'SELECT DISTINCT C.ID, C.VALOR_TEXT, C.VALOR_FEC, C.VALOR_BOOL, C.VALOR_NUM, CD.TIPO_CAMPO_GS, ISNULL(CD.ID_ATRIB_GS,0) ID_ATRIB_GS, ISNULL(D.VALIDACION_ERP,0) VALIDACION_ERP, ISNULL(CD.SUBTIPO,0) SUBTIPO, C.COPIA_CAMPO_DEF, C.CAMBIO'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ' FROM COPIA_CAMPO C  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                         INNER JOIN COPIA_CAMPO_DEF CD  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                 ON C.INSTANCIA = @INSTANCIA AND C.NUM_VERSION=@VERSION AND C.COPIA_CAMPO_DEF=CD.ID " & vbCrLf
sConsulta = sConsulta & "                        LEFT JOIN DEF_ATRIB D WITH (NOLOCK)  ON D.ID=CD.ID_ATRIB_GS'" & vbCrLf
sConsulta = sConsulta & "set @SQLCOMMON=''" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @USU IS NULL  --  -- Se est� cargando como certificado por el proveedor" & vbCrLf
sConsulta = sConsulta & "       SET @SQLCOMMON = @SQLCOMMON + ' INNER JOIN COPIA_CONF_CUMPLIMENTACION CC  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                              ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                             AND CC.SOLIC_PROVE =2'    " & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "       SET @SQLCOMMON = @SQLCOMMON + ' INNER JOIN COPIA_CONF_CUMPLIMENTACION CC   WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                               AND CC.SOLIC_PROVE =1'       " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + @SQLCOMMON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ' WHERE CD.ES_SUBCAMPO=0 AND (CC.VISIBLE=0 OR CC.ESCRITURA=0)' " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA INT, @VERSION INT = NULL,@USU VARCHAR(50), @PROVE VARCHAR(50)', @INSTANCIA=@INSTANCIA, @VERSION=@VERSION,@USU=@USU,@PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Desglose-------------------------------" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'" & vbCrLf
sConsulta = sConsulta & "SELECT LD.LINEA, LD.CAMPO_PADRE, LD.CAMPO_HIJO, LD.VALOR_NUM, LD.VALOR_TEXT, LD.VALOR_FEC, LD.VALOR_BOOL, LD.FECACT, LD.VALOR_TEXT_DEN, LD.VALOR_TEXT_COD, LD.CAMBIO" & vbCrLf
sConsulta = sConsulta & ",CC.VISIBLE, CD.TIPO_CAMPO_GS,CVL.VALOR_TEXT_SPA LISTA_TEXT, CC.ESCRITURA, ISNULL(CD.ID_ATRIB_GS,0) ID_ATRIB_GS, ISNULL(D.VALIDACION_ERP,0) VALIDACION_ERP, C.COPIA_CAMPO_DEF, ISNULL(CD.SUBTIPO,0) SUBTIPO  " & vbCrLf
sConsulta = sConsulta & "  FROM COPIA_LINEA_DESGLOSE LD  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "         INNER JOIN COPIA_CAMPO C WITH (NOLOCK) ON LD.CAMPO_HIJO = C.ID" & vbCrLf
sConsulta = sConsulta & "         INNER JOIN COPIA_CAMPO_DEF CD  WITH (NOLOCK)  ON CD.ID = C.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "         LEFT JOIN COPIA_CAMPO_VALOR_LISTA CVL  WITH (NOLOCK) ON CVL.CAMPO_DEF=C.COPIA_CAMPO_DEF AND CVL.ORDEN=LD.VALOR_NUM" & vbCrLf
sConsulta = sConsulta & "        LEFT JOIN DEF_ATRIB D  WITH (NOLOCK) ON D.ID=CD.ID_ATRIB_GS'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + @SQLCOMMON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + '  WHERE C.INSTANCIA = @INSTANCIA AND C.NUM_VERSION=@VERSION AND CD.ES_SUBCAMPO=1' " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA INT, @VERSION INT = NULL,@USU VARCHAR(50), @PROVE VARCHAR(50)', @INSTANCIA=@INSTANCIA, @VERSION=@VERSION,@USU=@USU,@PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Parte Adjuntos-----------------------" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'SELECT LD.CAMPO_PADRE , LD.CAMPO_HIJO, LD.LINEA,LDA.ID ADJUN, C.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "  FROM COPIA_LINEA_DESGLOSE LD  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "  INNER JOIN COPIA_CAMPO C WITH (NOLOCK) ON LD.CAMPO_HIJO = C.ID" & vbCrLf
sConsulta = sConsulta & "  INNER JOIN COPIA_CAMPO_DEF CD WITH (NOLOCK) ON CD.ID = C.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "  INNER JOIN COPIA_LINEA_DESGLOSE_ADJUN LDA WITH (NOLOCK) ON LDA.LINEA=LD.LINEA AND LDA.CAMPO_PADRE=LD.CAMPO_PADRE AND LDA.CAMPO_HIJO=LD.CAMPO_HIJO'" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + @SQLCOMMON" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + '  WHERE C.INSTANCIA = @INSTANCIA AND C.NUM_VERSION=@VERSION AND CD.ES_SUBCAMPO=1'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA INT, @VERSION INT = NULL,@USU VARCHAR(50), @PROVE VARCHAR(50)', @INSTANCIA=@INSTANCIA, @VERSION=@VERSION,@USU=@USU,@PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "----------------------------------------" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'" & vbCrLf
sConsulta = sConsulta & "SELECT LD.LINEA, LD.CAMPO_PADRE, LD.CAMPO_HIJO, LD.VALOR_NUM, LD.VALOR_TEXT, LD.VALOR_FEC, LD.VALOR_BOOL, LD.FECACT, LD.VALOR_TEXT_DEN, LD.VALOR_TEXT_COD, LD.CAMBIO" & vbCrLf
sConsulta = sConsulta & "  FROM COPIA_LINEA_DESGLOSE LD  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "     INNER JOIN" & vbCrLf
sConsulta = sConsulta & "           (SELECT DISTINCT C.ID" & vbCrLf
sConsulta = sConsulta & "              FROM COPIA_CAMPO C  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                 INNER JOIN COPIA_CAMPO_DEF CD  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                         ON C.COPIA_CAMPO_DEF=CD.ID '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + @SQLCOMMON " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + '  WHERE C.INSTANCIA = @INSTANCIA AND C.NUM_VERSION=@VERSION AND CD.ES_SUBCAMPO=1' " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ') DESG ON LD.CAMPO_PADRE = DESG.ID' " & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA INT, @VERSION INT = NULL,@USU VARCHAR(50), @PROVE VARCHAR(50)', @INSTANCIA=@INSTANCIA, @VERSION=@VERSION,@USU=@USU,@PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "-------------------------------------------" & vbCrLf
sConsulta = sConsulta & "-- En el caso de que se creen lineas nuevas, hay que coger los campos del desglose de form_campo" & vbCrLf
sConsulta = sConsulta & "DECLARE @MONEDA_CENTRAL NVARCHAR(3)" & vbCrLf
sConsulta = sConsulta & "DECLARE @EQUIVALENCIA FLOAT" & vbCrLf
sConsulta = sConsulta & "SELECT @MONEDA_CENTRAL = MONCEN,@EQUIVALENCIA=EQUIV FROM PARGEN_DEF WITH(NOLOCK) INNER JOIN MON WITH(NOLOCK) ON MON.COD=PARGEN_DEF.MONCEN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'SELECT NULL AS LINEA, C2.ID AS CAMPO_PADRE, C.ID AS CAMPO_HIJO" & vbCrLf
sConsulta = sConsulta & ",LD.VALOR_NUM,LD.VALOR_FEC,LD.VALOR_BOOL" & vbCrLf
sConsulta = sConsulta & ",CASE WHEN CD.TIPO_CAMPO_GS=45 THEN @MONEDA_CENTRAL ELSE LD.VALOR_TEXT END VALOR_TEXT" & vbCrLf
sConsulta = sConsulta & ",ISNULL(CD.TIPO_CAMPO_GS,0) TIPO_CAMPO_GS,CD.GRUPO,CVL.VALOR_TEXT_SPA LISTA_TEXT" & vbCrLf
sConsulta = sConsulta & ",ISNULL(CD.ID_ATRIB_GS,0) ID_ATRIB_GS, ISNULL(DA.VALIDACION_ERP,0) VALIDACION_ERP, C.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & ",CASE WHEN CD.TIPO_CAMPO_GS=45 THEN @EQUIVALENCIA ELSE NULL END CAMBIO, ISNULL(CD.SUBTIPO,0) SUBTIPO" & vbCrLf
sConsulta = sConsulta & "FROM COPIA_CAMPO C WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_CAMPO_DEF CD WITH(NOLOCK) ON C.COPIA_CAMPO_DEF=CD.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN DESGLOSE D WITH(NOLOCK) ON CD.CAMPO_ORIGEN=D.CAMPO_HIJO" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN LINEA_DESGLOSE LD WITH(NOLOCK) ON D.CAMPO_PADRE=LD.CAMPO_PADRE AND D.CAMPO_HIJO=LD.CAMPO_HIJO" & vbCrLf
sConsulta = sConsulta & "INNER JOIN FORM_CAMPO F WITH (NOLOCK) ON D.CAMPO_PADRE=F.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_CAMPO_DEF CD2 WITH (NOLOCK) ON F.ID=CD2.CAMPO_ORIGEN" & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_CAMPO C2 WITH (NOLOCK) ON C2.INSTANCIA=@INSTANCIA AND C2.NUM_VERSION=@VERSION AND CD2.ID=C2.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN CAMPO_VALOR_LISTA CVL WITH (NOLOCK)  ON CVL.CAMPO=CD.ID AND CVL.ORDEN=LD.VALOR_NUM" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN DEF_ATRIB DA WITH (NOLOCK)  ON DA.ID=CD.ID_ATRIB_GS'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + @SQLCOMMON " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ' WHERE C.INSTANCIA = @INSTANCIA AND C.NUM_VERSION=@VERSION AND (CC.VISIBLE=0 OR CC.ESCRITURA=0)'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA INT, @VERSION INT,@MONEDA_CENTRAL NVARCHAR(3), @EQUIVALENCIA FLOAT', @INSTANCIA=@INSTANCIA, @VERSION=@VERSION, @MONEDA_CENTRAL=@MONEDA_CENTRAL,@EQUIVALENCIA=@EQUIVALENCIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "--Los adjuntos a nivel de campos" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'SELECT DISTINCT CA.ID, CA.CAMPO, C.COPIA_CAMPO_DEF'" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ' FROM COPIA_CAMPO C  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                    INNER JOIN COPIA_CAMPO_DEF CD WITH (NOLOCK) ON C.INSTANCIA = @INSTANCIA AND C.NUM_VERSION=@VERSION AND C.COPIA_CAMPO_DEF=CD.ID" & vbCrLf
sConsulta = sConsulta & "                    INNER JOIN COPIA_CAMPO_ADJUN CA WITH (NOLOCK) ON CA.CAMPO=C.ID '" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + @SQLCOMMON" & vbCrLf
sConsulta = sConsulta & "IF @USU IS NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + '  WHERE CD.SUBTIPO=8 AND CD.ES_SUBCAMPO=0 AND CC.VISIBLE=0 '" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + '  WHERE CD.SUBTIPO=8 AND CD.ES_SUBCAMPO=0 AND (CC.VISIBLE=0 OR CC.ESCRITURA=0) '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA INT, @VERSION INT = NULL', @INSTANCIA=@INSTANCIA, @VERSION=@VERSION" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-----------------------------------" & vbCrLf
sConsulta = sConsulta & "-- No Conformidades Ocultas no llegan a PORTAL. Ni campos, ni adjuntos, ni desgloses, ni acciones (noconf_acc y noconf_solicit_acc)" & vbCrLf
sConsulta = sConsulta & "IF @PROVE IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF (SELECT COUNT(1) FROM NOCONF_SOLICIT_ACC A WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN COPIA_CAMPO C WITH (NOLOCK) ON C.ID=A.COPIA_CAMPO AND C.INSTANCIA = @INSTANCIA AND C.NUM_VERSION=@VERSION " & vbCrLf
sConsulta = sConsulta & "       WHERE A.SOLICITAR=0)>0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   --NOCONF_SOLICIT_ACC" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = N'SELECT DISTINCT A.COPIA_CAMPO, A.FECHA_LIMITE, C.COPIA_CAMPO_DEF " & vbCrLf
sConsulta = sConsulta & "       FROM NOCONF_SOLICIT_ACC A WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN COPIA_CAMPO C WITH (NOLOCK) ON C.ID=A.COPIA_CAMPO AND C.INSTANCIA = @INSTANCIA AND C.NUM_VERSION=@VERSION " & vbCrLf
sConsulta = sConsulta & "       WHERE A.SOLICITAR=0'" & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA INT, @VERSION INT = NULL', @INSTANCIA=@INSTANCIA, @VERSION=@VERSION   " & vbCrLf
sConsulta = sConsulta & "   --NOCONF_ACC" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = N'SELECT DISTINCT A.CAMPO_PADRE,A.LINEA, A.ESTADO, A.ESTADO_INT, A.COMENT, C.COPIA_CAMPO_DEF " & vbCrLf
sConsulta = sConsulta & "       FROM NOCONF_SOLICIT_ACC SA WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN NOCONF_ACC A WITH (NOLOCK) ON A.CAMPO_PADRE=SA.COPIA_CAMPO" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN COPIA_CAMPO C WITH (NOLOCK) ON C.ID=SA.COPIA_CAMPO AND C.INSTANCIA = @INSTANCIA AND C.NUM_VERSION=@VERSION " & vbCrLf
sConsulta = sConsulta & "       WHERE SA.SOLICITAR=0'" & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA INT, @VERSION INT = NULL', @INSTANCIA=@INSTANCIA, @VERSION=@VERSION   " & vbCrLf
sConsulta = sConsulta & "   --CAMPOS" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = N'SELECT DISTINCT C.ID, C.VALOR_TEXT, C.VALOR_FEC, C.VALOR_BOOL, C.VALOR_NUM, CD.TIPO_CAMPO_GS, ISNULL(CD.SUBTIPO,0) SUBTIPO, ISNULL(CD.ID_ATRIB_GS,0) ID_ATRIB_GS, ISNULL(DA.VALIDACION_ERP,0) VALIDACION_ERP, ISNULL(CD.SUBTIPO,0) SUBTIPO, C.COPIA_CAMPO_DEF, C.CAMBIO" & vbCrLf
sConsulta = sConsulta & "       FROM NOCONF_SOLICIT_ACC A WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN COPIA_CAMPO CS WITH (NOLOCK) ON CS.ID=A.COPIA_CAMPO AND CS.INSTANCIA = @INSTANCIA AND CS.NUM_VERSION=@VERSION " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN COPIA_CAMPO_DEF CDS WITH (NOLOCK) ON CS.COPIA_CAMPO_DEF=CDS.ID       " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN COPIA_CAMPO_DEF CD WITH (NOLOCK) ON CD.GRUPO=CDS.GRUPO AND CD.ES_SUBCAMPO=0      " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN COPIA_CAMPO C WITH(NOLOCK) ON C.INSTANCIA = @INSTANCIA AND C.NUM_VERSION=@VERSION AND C.COPIA_CAMPO_DEF=CD.ID " & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN DEF_ATRIB DA WITH (NOLOCK)  ON DA.ID=CD.ID_ATRIB_GS" & vbCrLf
sConsulta = sConsulta & "       WHERE A.SOLICITAR=0'                    " & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA INT, @VERSION INT = NULL', @INSTANCIA=@INSTANCIA, @VERSION=@VERSION   " & vbCrLf
sConsulta = sConsulta & "   --CAMPOS        ADJUNTO" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = N'SELECT DISTINCT CA.ID, CA.CAMPO, C.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "       FROM NOCONF_SOLICIT_ACC A WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN COPIA_CAMPO CS WITH (NOLOCK) ON CS.ID=A.COPIA_CAMPO AND CS.INSTANCIA = @INSTANCIA AND CS.NUM_VERSION=@VERSION " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN COPIA_CAMPO_DEF CDS WITH (NOLOCK) ON CS.COPIA_CAMPO_DEF=CDS.ID       " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN COPIA_CAMPO_DEF CD WITH (NOLOCK) ON CD.GRUPO=CDS.GRUPO AND CD.SUBTIPO=8 AND CD.ES_SUBCAMPO=0     " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN COPIA_CAMPO C WITH(NOLOCK) ON C.INSTANCIA = @INSTANCIA AND C.NUM_VERSION=@VERSION AND C.COPIA_CAMPO_DEF=CD.ID " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN COPIA_CAMPO_ADJUN CA WITH (NOLOCK) ON CA.CAMPO=C.ID" & vbCrLf
sConsulta = sConsulta & "       WHERE A.SOLICITAR=0'                    " & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA INT, @VERSION INT = NULL', @INSTANCIA=@INSTANCIA, @VERSION=@VERSION           " & vbCrLf
sConsulta = sConsulta & "   --DESGLOSE" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = N'SELECT DISTINCT C.ID CAMPO_HIJO, C.COPIA_CAMPO_DEF CAMPO_HIJO_DEF,D.CAMPO_PADRE,CS.COPIA_CAMPO_DEF CAMPO_PADRE_DEF" & vbCrLf
sConsulta = sConsulta & "       FROM NOCONF_SOLICIT_ACC A WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN COPIA_CAMPO CS WITH (NOLOCK) ON CS.ID=A.COPIA_CAMPO AND CS.INSTANCIA = @INSTANCIA AND CS.NUM_VERSION=@VERSION " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN COPIA_DESGLOSE D WITH(NOLOCK) ON D.CAMPO_PADRE=A.COPIA_CAMPO" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN COPIA_CAMPO C WITH(NOLOCK) ON C.ID=D.CAMPO_HIJO " & vbCrLf
sConsulta = sConsulta & "       WHERE A.SOLICITAR=0'                " & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA INT, @VERSION INT = NULL', @INSTANCIA=@INSTANCIA, @VERSION=@VERSION" & vbCrLf
sConsulta = sConsulta & "   --DESGLOSE  LINEA   " & vbCrLf
sConsulta = sConsulta & "   SET @SQL = N'SELECT DISTINCT LD.LINEA, LD.CAMPO_PADRE, LD.CAMPO_HIJO, LD.VALOR_NUM, LD.VALOR_TEXT, LD.VALOR_FEC, LD.VALOR_BOOL, LD.CAMBIO" & vbCrLf
sConsulta = sConsulta & "       , CS.COPIA_CAMPO_DEF CAMPO_PADRE_DEF, C.COPIA_CAMPO_DEF CAMPO_HIJO_DEF  " & vbCrLf
sConsulta = sConsulta & "       FROM NOCONF_SOLICIT_ACC A WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN COPIA_CAMPO CS WITH (NOLOCK) ON CS.ID=A.COPIA_CAMPO AND CS.INSTANCIA = @INSTANCIA AND CS.NUM_VERSION=@VERSION " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN COPIA_DESGLOSE D WITH(NOLOCK) ON D.CAMPO_PADRE=A.COPIA_CAMPO" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN COPIA_LINEA_DESGLOSE LD WITH(NOLOCK) ON LD.CAMPO_PADRE=D.CAMPO_PADRE AND LD.CAMPO_HIJO=D.CAMPO_HIJO      " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN COPIA_CAMPO C WITH(NOLOCK) ON C.ID=D.CAMPO_HIJO " & vbCrLf
sConsulta = sConsulta & "       WHERE A.SOLICITAR=0'            " & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA INT, @VERSION INT = NULL', @INSTANCIA=@INSTANCIA, @VERSION=@VERSION   " & vbCrLf
sConsulta = sConsulta & "   --DESGLOSE  ADJUNTO             " & vbCrLf
sConsulta = sConsulta & "   SET @SQL = N'SELECT DISTINCT LD.CAMPO_PADRE , LD.CAMPO_HIJO, LD.LINEA,LDA.ID ADJUN, CS.COPIA_CAMPO_DEF CAMPO_PADRE_DEF, C.COPIA_CAMPO_DEF CAMPO_HIJO_DEF    " & vbCrLf
sConsulta = sConsulta & "       FROM NOCONF_SOLICIT_ACC A WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN COPIA_CAMPO CS WITH (NOLOCK) ON CS.ID=A.COPIA_CAMPO AND CS.INSTANCIA = @INSTANCIA AND CS.NUM_VERSION=@VERSION " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN COPIA_DESGLOSE D WITH(NOLOCK) ON D.CAMPO_PADRE=A.COPIA_CAMPO" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN COPIA_LINEA_DESGLOSE LD WITH(NOLOCK) ON LD.CAMPO_PADRE=D.CAMPO_PADRE AND LD.CAMPO_HIJO=D.CAMPO_HIJO      " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN COPIA_CAMPO C WITH(NOLOCK) ON C.ID=D.CAMPO_HIJO " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN COPIA_LINEA_DESGLOSE_ADJUN LDA WITH (NOLOCK) ON LDA.LINEA=LD.LINEA AND LDA.CAMPO_PADRE=LD.CAMPO_PADRE AND LDA.CAMPO_HIJO=LD.CAMPO_HIJO" & vbCrLf
sConsulta = sConsulta & "       WHERE A.SOLICITAR=0'" & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA INT, @VERSION INT = NULL', @INSTANCIA=@INSTANCIA, @VERSION=@VERSION       " & vbCrLf
sConsulta = sConsulta & "END    " & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "-- Caso q no deber�a darse, el desglose es visible y escritura (1ra select no lo saca) pero todas sus cols son invisibles LCX/2014/62" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'SELECT DISTINCT C.ID AS CAMPO_PADRE, ISNULL(CD.TIPO_CAMPO_GS,0) TIPO_CAMPO_GS_PADRE,CD.GRUPO GRUPO_PADRE" & vbCrLf
sConsulta = sConsulta & "           ,ISNULL(CD.SUBTIPO,0) SUBTIPO_PADRE, C.COPIA_CAMPO_DEF COPIA_CAMPO_DEF_PADRE'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ' FROM COPIA_CAMPO C  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                INNER JOIN COPIA_CAMPO_DEF CD WITH (NOLOCK)  ON C.INSTANCIA = @INSTANCIA AND C.NUM_VERSION=@VERSION AND C.COPIA_CAMPO_DEF=CD.ID" & vbCrLf
sConsulta = sConsulta & "                       AND CD.SUBTIPO=9'" & vbCrLf
sConsulta = sConsulta & "                       " & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + '  WHERE C.INSTANCIA = @INSTANCIA AND C.NUM_VERSION=@VERSION AND CD.ES_SUBCAMPO=0'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA INT, @VERSION INT = NULL', @INSTANCIA=@INSTANCIA, @VERSION=@VERSION" & vbCrLf
sConsulta = sConsulta & "--------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- se escapa el caso de desglose no popup invisible sin lineas en la versi�n" & vbCrLf
sConsulta = sConsulta & "IF (SELECT COUNT(1) " & vbCrLf
sConsulta = sConsulta & "   FROM COPIA_CAMPO C  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN COPIA_CAMPO_DEF CD WITH (NOLOCK)  ON C.INSTANCIA = @INSTANCIA AND C.NUM_VERSION=@VERSION AND C.COPIA_CAMPO_DEF=CD.ID" & vbCrLf
sConsulta = sConsulta & "       AND CD.ES_SUBCAMPO=0 AND CD.SUBTIPO=9" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN COPIA_CONF_CUMPLIMENTACION CC WITH (NOLOCK) ON  CD.ID=CC.COPIA_CAMPO AND CC.SOLIC_PROVE=CASE WHEN @USU IS NULL THEN 2 ELSE 1 END" & vbCrLf
sConsulta = sConsulta & "       AND CC.VISIBLE=0)>0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   --DESGLOSE" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = N'SELECT DISTINCT HIJO.ID CAMPO_HIJO, HIJO.COPIA_CAMPO_DEF CAMPO_HIJO_DEF" & vbCrLf
sConsulta = sConsulta & "   ,PADRE.ID CAMPO_PADRE, PADRE.COPIA_CAMPO_DEF CAMPO_PADRE_DEF" & vbCrLf
sConsulta = sConsulta & "       FROM COPIA_CAMPO PADRE WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN COPIA_CAMPO_DEF CD WITH (NOLOCK)  ON PADRE.INSTANCIA = @INSTANCIA AND PADRE.NUM_VERSION=@VERSION AND PADRE.COPIA_CAMPO_DEF=CD.ID" & vbCrLf
sConsulta = sConsulta & "           AND CD.ES_SUBCAMPO=0 AND CD.SUBTIPO=9 '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + @SQLCOMMON " & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + ' AND CC.VISIBLE=0" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN COPIA_DESGLOSE D WITH(NOLOCK) ON D.CAMPO_PADRE=PADRE.ID" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN COPIA_CAMPO HIJO WITH(NOLOCK) ON HIJO.ID=D.CAMPO_HIJO '" & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA INT, @VERSION INT = NULL', @INSTANCIA=@INSTANCIA, @VERSION=@VERSION" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_GETINSTINVISIBLEFIELDS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_GETINSTINVISIBLEFIELDS]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_GETINSTINVISIBLEFIELDS] @IDCAMPO INT,@USU VARCHAR(50)=NULL, @PROVE VARCHAR(50) = NULL, @IDI VARCHAR(50) = 'SPA'    AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @INSTANCIA INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @VERSION INT" & vbCrLf
sConsulta = sConsulta & "DECLARE  @SQL NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE  @SQLCOMMON NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @ROL INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @BLOQUE INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ENBLOQUE_ACT TINYINT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @INSTANCIA =INSTANCIA,  @VERSION =NUM_VERSION FROM COPIA_CAMPO WITH (NOLOCK) WHERE ID = @IDCAMPO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @ROL =NULL" & vbCrLf
sConsulta = sConsulta & "SET @BLOQUE=NULL" & vbCrLf
sConsulta = sConsulta & "SET @ENBLOQUE_ACT=1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC FSPM_IDENTIFICAR_ROL @USU=@USU, @PROVE=@PROVE, @INSTANCIA=@INSTANCIA, @ROL=@ROL OUTPUT , @BLOQUE= @BLOQUE OUTPUT,@ENBLOQUE_ACT=@ENBLOQUE_ACT OUTPUT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'SELECT DISTINCT C.ID, C.VALOR_TEXT, C.VALOR_FEC, C.VALOR_BOOL, C.VALOR_NUM, ISNULL(CD.ID_ATRIB_GS,0) ID_ATRIB_GS, ISNULL(CD.TIPO_CAMPO_GS,0) TIPO_CAMPO_GS,CD.GRUPO, ISNULL(D.VALIDACION_ERP,0) VALIDACION_ERP , ISNULL(CD.SUBTIPO,0) SUBTIPO, C.COPIA_CAMPO_DEF, C.CAMBIO'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ' FROM COPIA_CAMPO C  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                INNER JOIN COPIA_CAMPO_DEF CD WITH (NOLOCK)  ON C.INSTANCIA = @INSTANCIA AND C.NUM_VERSION=@VERSION AND C.COPIA_CAMPO_DEF=CD.ID" & vbCrLf
sConsulta = sConsulta & "               LEFT JOIN DEF_ATRIB D WITH (NOLOCK)  ON D.ID=CD.ID_ATRIB_GS '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "set @SQLCOMMON=' INNER JOIN PM_COPIA_CONF_CUMP_BLOQUE CC WITH (NOLOCK)  ON CC.BLOQUE=@BLOQUE AND CC.ROL=@ROL AND CD.ID=CC.CAMPO'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + @SQLCOMMON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + '  WHERE C.INSTANCIA = @INSTANCIA AND C.NUM_VERSION=@VERSION AND CD.ES_SUBCAMPO=0 AND (CC.VISIBLE=0 OR CC.ESCRITURA=0) '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA INT, @VERSION INT = NULL,@USU VARCHAR(50), @PROVE VARCHAR(50),@BLOQUE INT,@ROL INT ', @INSTANCIA=@INSTANCIA, @VERSION=@VERSION,@USU=@USU,@PROVE = @PROVE,@BLOQUE=@BLOQUE,@ROL=@ROL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "---------------------------------" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'" & vbCrLf
sConsulta = sConsulta & "SELECT LD.LINEA, LD.CAMPO_PADRE, LD.CAMPO_HIJO, LD.VALOR_NUM, LD.VALOR_TEXT, LD.VALOR_FEC, LD.VALOR_BOOL, LD.FECACT, LD.VALOR_TEXT_DEN, LD.VALOR_TEXT_COD, LD.CAMBIO, ISNULL(CD.SUBTIPO,0) SUBTIPO" & vbCrLf
sConsulta = sConsulta & ",CC.VISIBLE, ISNULL(CD.ID_ATRIB_GS,0) ID_ATRIB_GS, ISNULL(CD.TIPO_CAMPO_GS,0) TIPO_CAMPO_GS,CD.GRUPO, ISNULL(D.VALIDACION_ERP,0) VALIDACION_ERP,CVL.VALOR_TEXT_' + @IDI + ' LISTA_TEXT, CC.ESCRITURA, C.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "  FROM COPIA_LINEA_DESGLOSE LD  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "         INNER JOIN COPIA_CAMPO C WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                 ON LD.CAMPO_HIJO = C.ID AND C.INSTANCIA = @INSTANCIA AND C.NUM_VERSION=@VERSION " & vbCrLf
sConsulta = sConsulta & "         INNER JOIN COPIA_CAMPO_DEF CD  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                 ON CD.ID = C.COPIA_CAMPO_DEF AND CD.ES_SUBCAMPO=1" & vbCrLf
sConsulta = sConsulta & "         LEFT JOIN DEF_ATRIB D  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "      ON D.ID=CD.ID_ATRIB_GS" & vbCrLf
sConsulta = sConsulta & "         LEFT JOIN COPIA_CAMPO_VALOR_LISTA CVL  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   ON CVL.CAMPO_DEF=C.COPIA_CAMPO_DEF AND CVL.ORDEN=LD.VALOR_NUM '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + @SQLCOMMON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA INT, @VERSION INT = NULL,@USU VARCHAR(50), @PROVE VARCHAR(50),@BLOQUE INT,@ROL INT', @INSTANCIA=@INSTANCIA, @VERSION=@VERSION,@USU=@USU,@PROVE = @PROVE,@BLOQUE=@BLOQUE,@ROL=@ROL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N' SELECT LDA.LINEA, LDA.CAMPO_PADRE, LDA.CAMPO_HIJO , LDA.ID ADJUN, C.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "  FROM COPIA_LINEA_DESGLOSE LD  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "  INNER JOIN COPIA_CAMPO C WITH (NOLOCK) ON C.ID = LD.CAMPO_HIJO AND C.INSTANCIA = @INSTANCIA AND C.NUM_VERSION=@VERSION" & vbCrLf
sConsulta = sConsulta & "  INNER JOIN COPIA_CAMPO_DEF CD  WITH (NOLOCK) ON CD.ID = C.COPIA_CAMPO_DEF AND CD.ES_SUBCAMPO=1" & vbCrLf
sConsulta = sConsulta & "  INNER JOIN COPIA_LINEA_DESGLOSE_ADJUN LDA WITH (NOLOCK)  ON LDA.CAMPO_PADRE=LD.CAMPO_PADRE AND LDA.CAMPO_HIJO=LD.CAMPO_HIJO AND LDA.LINEA=LD.LINEA'" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + @SQLCOMMON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA INT, @VERSION INT = NULL,@USU VARCHAR(50), @PROVE VARCHAR(50),@BLOQUE INT,@ROL INT', @INSTANCIA=@INSTANCIA, @VERSION=@VERSION,@USU=@USU,@PROVE = @PROVE,@BLOQUE=@BLOQUE,@ROL=@ROL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-------------------------------------" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N' SELECT LD.LINEA, LD.CAMPO_PADRE, LD.CAMPO_HIJO, LD.VALOR_NUM, LD.VALOR_TEXT, LD.VALOR_FEC, LD.VALOR_BOOL, LD.FECACT, LD.VALOR_TEXT_DEN, LD.VALOR_TEXT_COD, LD.CAMBIO" & vbCrLf
sConsulta = sConsulta & "  FROM COPIA_LINEA_DESGLOSE LD  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "     INNER JOIN" & vbCrLf
sConsulta = sConsulta & "           (SELECT DISTINCT C.ID" & vbCrLf
sConsulta = sConsulta & "              FROM COPIA_CAMPO C  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                 INNER JOIN COPIA_CAMPO_DEF CD  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                         ON C.INSTANCIA = @INSTANCIA AND C.NUM_VERSION=@VERSION AND C.COPIA_CAMPO_DEF=CD.ID AND CD.ES_SUBCAMPO=1 '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + @SQLCOMMON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ') DESG ON LD.CAMPO_PADRE = DESG.ID'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA INT, @VERSION INT = NULL,@USU VARCHAR(50), @PROVE VARCHAR(50),@BLOQUE INT,@ROL INT', @INSTANCIA=@INSTANCIA, @VERSION=@VERSION,@USU=@USU,@PROVE = @PROVE,@BLOQUE=@BLOQUE,@ROL=@ROL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--------------------------------" & vbCrLf
sConsulta = sConsulta & "-- En el caso de que se creen lineas nuevas, hay que coger los campos del desglose de form_campo" & vbCrLf
sConsulta = sConsulta & "DECLARE @MONEDA_CENTRAL NVARCHAR(3)" & vbCrLf
sConsulta = sConsulta & "DECLARE @EQUIVALENCIA FLOAT" & vbCrLf
sConsulta = sConsulta & "SELECT @MONEDA_CENTRAL = MONCEN,@EQUIVALENCIA=EQUIV FROM PARGEN_DEF WITH(NOLOCK) INNER JOIN MON WITH(NOLOCK) ON MON.COD=PARGEN_DEF.MONCEN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "select null as linea, C2.ID AS CAMPO_PADRE, cc.id as campo_hijo,LD.VALOR_NUM,LD.VALOR_FEC" & vbCrLf
sConsulta = sConsulta & ",CASE WHEN ccd.TIPO_CAMPO_GS=45 THEN @MONEDA_CENTRAL ELSE LD.VALOR_TEXT END VALOR_TEXT" & vbCrLf
sConsulta = sConsulta & ",LD.VALOR_BOOL,ISNULL(CCD.TIPO_CAMPO_GS,0) TIPO_CAMPO_GS, ISNULL(CCD.SUBTIPO,0) SUBTIPO" & vbCrLf
sConsulta = sConsulta & ", ISNULL(CCD.ID_ATRIB_GS,0) ID_ATRIB_GS,CCD.GRUPO, ISNULL(DA.VALIDACION_ERP,0) VALIDACION_ERP,CVL.VALOR_TEXT_SPA LISTA_TEXT, CC.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & ",CASE WHEN ccd.TIPO_CAMPO_GS=45 THEN @EQUIVALENCIA ELSE NULL END CAMBIO" & vbCrLf
sConsulta = sConsulta & "from copia_campo cc with(nolock)" & vbCrLf
sConsulta = sConsulta & "inner join copia_campo_def ccd with(nolock) on cc.copia_campo_def=ccd.id" & vbCrLf
sConsulta = sConsulta & "inner join desglose d with(nolock) on ccd.campo_origen=d.campo_hijo" & vbCrLf
sConsulta = sConsulta & "left join linea_desglose ld with(nolock) on d.campo_padre=ld.campo_padre and d.campo_hijo=ld.campo_hijo" & vbCrLf
sConsulta = sConsulta & "inner JOIN FORM_CAMPO F WITH (NOLOCK) ON D.CAMPO_PADRE=F.ID" & vbCrLf
sConsulta = sConsulta & "inner JOIN COPIA_CAMPO_DEF CD2 WITH (NOLOCK) ON F.ID=CD2.CAMPO_ORIGEN" & vbCrLf
sConsulta = sConsulta & "inner JOIN COPIA_CAMPO C2 WITH (NOLOCK) ON  C2.INSTANCIA=@INSTANCIA AND C2.NUM_VERSION=@VERSION AND CD2.ID=C2.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN DEF_ATRIB DA WITH (NOLOCK)  ON DA.ID=CCD.ID_ATRIB_GS" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN CAMPO_VALOR_LISTA CVL WITH (NOLOCK)  ON CVL.CAMPO=CCD.ID AND CVL.ORDEN=LD.VALOR_NUM" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_COPIA_CONF_CUMP_BLOQUE CCCB WITH (NOLOCK)  ON CCCB.BLOQUE=@BLOQUE AND CCCB.ROL=@ROL AND ccd.ID=CCCB.CAMPO" & vbCrLf
sConsulta = sConsulta & "where CC.instancia = @INSTANCIA and CC.num_version=@VERSION and (CCCB.VISIBLE=0 OR CCCB.ESCRITURA=0)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "--Los adjuntos a nivel de campos" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'SELECT DISTINCT CA.ID, CA.CAMPO, C.COPIA_CAMPO_DEF'" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ' FROM COPIA_CAMPO C  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                    INNER JOIN COPIA_CAMPO_DEF CD WITH (NOLOCK) ON C.INSTANCIA = @INSTANCIA AND C.NUM_VERSION=@VERSION AND C.COPIA_CAMPO_DEF=CD.ID" & vbCrLf
sConsulta = sConsulta & "                    INNER JOIN COPIA_CAMPO_ADJUN CA WITH (NOLOCK) ON CA.CAMPO=C.ID '" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + @SQLCOMMON" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + '  WHERE CD.SUBTIPO=8 AND CD.ES_SUBCAMPO=0 AND (CC.VISIBLE=0 OR CC.ESCRITURA=0) '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA INT, @VERSION INT = NULL,@BLOQUE INT,@ROL INT ', @INSTANCIA=@INSTANCIA, @VERSION=@VERSION,@BLOQUE=@BLOQUE,@ROL=@ROL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "-- Caso q no deber�a darse, el desglose es visible y escritura (1ra select no lo saca) pero todas sus cols son invisibles LCX/2014/62" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'SELECT DISTINCT C.ID AS CAMPO_PADRE, ISNULL(CD.TIPO_CAMPO_GS,0) TIPO_CAMPO_GS_PADRE,CD.GRUPO GRUPO_PADRE" & vbCrLf
sConsulta = sConsulta & "           ,ISNULL(CD.SUBTIPO,0) SUBTIPO_PADRE, C.COPIA_CAMPO_DEF COPIA_CAMPO_DEF_PADRE'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ' FROM COPIA_CAMPO C  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                INNER JOIN COPIA_CAMPO_DEF CD WITH (NOLOCK)  ON C.INSTANCIA = @INSTANCIA AND C.NUM_VERSION=@VERSION AND C.COPIA_CAMPO_DEF=CD.ID" & vbCrLf
sConsulta = sConsulta & "                       AND CD.SUBTIPO=9'           " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + '  WHERE C.INSTANCIA = @INSTANCIA AND C.NUM_VERSION=@VERSION AND CD.ES_SUBCAMPO=0'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA INT, @VERSION INT = NULL', @INSTANCIA=@INSTANCIA, @VERSION=@VERSION" & vbCrLf
sConsulta = sConsulta & "--------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- se escapa el caso de desglose no popup invisible sin lineas en la versi�n" & vbCrLf
sConsulta = sConsulta & "IF (SELECT COUNT(1) " & vbCrLf
sConsulta = sConsulta & "   FROM COPIA_CAMPO C  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN COPIA_CAMPO_DEF CD WITH (NOLOCK)  ON C.INSTANCIA = @INSTANCIA AND C.NUM_VERSION=@VERSION AND C.COPIA_CAMPO_DEF=CD.ID" & vbCrLf
sConsulta = sConsulta & "       AND CD.ES_SUBCAMPO=0 AND CD.SUBTIPO=9" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PM_COPIA_CONF_CUMP_BLOQUE CC WITH (NOLOCK)  ON CC.BLOQUE=@BLOQUE AND CC.ROL=@ROL AND CD.ID=CC.CAMPO AND CC.VISIBLE=0)>0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   --DESGLOSE" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = N'SELECT DISTINCT HIJO.ID CAMPO_HIJO, HIJO.COPIA_CAMPO_DEF CAMPO_HIJO_DEF" & vbCrLf
sConsulta = sConsulta & "   ,PADRE.ID CAMPO_PADRE, PADRE.COPIA_CAMPO_DEF CAMPO_PADRE_DEF" & vbCrLf
sConsulta = sConsulta & "       FROM COPIA_CAMPO PADRE WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN COPIA_CAMPO_DEF CD WITH (NOLOCK)  ON PADRE.INSTANCIA = @INSTANCIA AND PADRE.NUM_VERSION=@VERSION AND PADRE.COPIA_CAMPO_DEF=CD.ID" & vbCrLf
sConsulta = sConsulta & "           AND CD.ES_SUBCAMPO=0 AND CD.SUBTIPO=9" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PM_COPIA_CONF_CUMP_BLOQUE CC WITH (NOLOCK)  ON CC.BLOQUE=@BLOQUE AND CC.ROL=@ROL AND CD.ID=CC.CAMPO AND CC.VISIBLE=0 " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN COPIA_DESGLOSE D WITH(NOLOCK) ON D.CAMPO_PADRE=PADRE.ID" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN COPIA_CAMPO HIJO WITH(NOLOCK) ON HIJO.ID=D.CAMPO_HIJO '" & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA INT, @VERSION INT = NULL,@BLOQUE INT,@ROL INT ', @INSTANCIA=@INSTANCIA, @VERSION=@VERSION,@BLOQUE=@BLOQUE,@ROL=@ROL   " & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_SALVAR_ROLES_PANTALLA]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_SALVAR_ROLES_PANTALLA]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_SALVAR_ROLES_PANTALLA] @INSTANCIA INT, @NUM_VERSION INT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   DECLARE @SQL NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   --Actualizar la tabla PM_COPIA_ROL, para establecer roles mediante campos" & vbCrLf
sConsulta = sConsulta & "   --Roles de tipo Comprador o usuario" & vbCrLf
sConsulta = sConsulta & "   UPDATE PM_COPIA_ROL" & vbCrLf
sConsulta = sConsulta & "   SET PER = CASE WHEN U.FSWS_SUSTITUCION IS NULL THEN CC.VALOR_TEXT ELSE U.FSWS_SUSTITUCION END, PER_ORIGINAL= CC.VALOR_TEXT" & vbCrLf
sConsulta = sConsulta & "   FROM COPIA_CAMPO CC WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN USU U WITH (NOLOCK) ON CC.VALOR_TEXT = U.PER AND U.BAJA = 0" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL PCR WITH (NOLOCK) ON CC.INSTANCIA = @INSTANCIA AND CC.NUM_VERSION = @NUM_VERSION AND PCR.CAMPO = CC.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN INSTANCIA WITH (NOLOCK)  ON PCR.INSTANCIA=INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "           AND (INSTANCIA.ESTADO=0 OR INSTANCIA.ESTADO=2 OR INSTANCIA.ESTADO=3 OR INSTANCIA.ESTADO=4 OR INSTANCIA.ESTADO=6)" & vbCrLf
sConsulta = sConsulta & "   WHERE (PCR.TIPO = 1 OR PCR.TIPO = 3 OR PCR.TIPO = 5)" & vbCrLf
sConsulta = sConsulta & "      AND PCR.INSTANCIA = @INSTANCIA" & vbCrLf
sConsulta = sConsulta & "      AND PCR.CUANDO_ASIGNAR = 1" & vbCrLf
sConsulta = sConsulta & "      AND PCR.BLOQUE IN (SELECT BLOQUE FROM INSTANCIA_BLOQUE WITH (NOLOCK) WHERE ESTADO = 1 AND INSTANCIA = @INSTANCIA)" & vbCrLf
sConsulta = sConsulta & "      AND PCR.COMO_ASIGNAR = 1   " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   --Roles de tipo Proveedor" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = N'UPDATE PM_COPIA_ROL SET PROVE = cast(CC.VALOR_TEXT as '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + (select 'varchar(' + convert(varchar,longitud) + ')' from DIC WITH(NOLOCK) where nombre='PROVE')" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + ')    FROM COPIA_CAMPO CC WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       WHERE CC.INSTANCIA = @INSTANCIA" & vbCrLf
sConsulta = sConsulta & "           AND CC.NUM_VERSION = @NUM_VERSION" & vbCrLf
sConsulta = sConsulta & "           AND PM_COPIA_ROL.CAMPO = CC.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "           AND PM_COPIA_ROL.TIPO = 2" & vbCrLf
sConsulta = sConsulta & "           AND PM_COPIA_ROL.INSTANCIA = @INSTANCIA" & vbCrLf
sConsulta = sConsulta & "           AND PM_COPIA_ROL.CUANDO_ASIGNAR = 1" & vbCrLf
sConsulta = sConsulta & "           AND PM_COPIA_ROL.BLOQUE IN (SELECT BLOQUE FROM INSTANCIA_BLOQUE WITH (NOLOCK) WHERE ESTADO = 1 AND INSTANCIA = @INSTANCIA)" & vbCrLf
sConsulta = sConsulta & "           AND PM_COPIA_ROL.COMO_ASIGNAR = 1'" & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL,N'@INSTANCIA INT, @NUM_VERSION INT', @INSTANCIA=@INSTANCIA,@NUM_VERSION=@NUM_VERSION" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   --Roles de tipo Proveedor de Contrato" & vbCrLf
sConsulta = sConsulta & "   DELETE PM_COPIA_ROL_CON " & vbCrLf
sConsulta = sConsulta & "   FROM CONTRATO WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL WITH(NOLOCK) ON CONTRATO.INSTANCIA= PM_COPIA_ROL.INSTANCIA " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL_CON WITH(NOLOCK) ON PM_COPIA_ROL.ID = PM_COPIA_ROL_CON.ID_ROL" & vbCrLf
sConsulta = sConsulta & "   AND CUANDO_ASIGNAR=3 AND CONTRATO.INSTANCIA = @INSTANCIA" & vbCrLf
sConsulta = sConsulta & "      " & vbCrLf
sConsulta = sConsulta & "   UPDATE PM_COPIA_ROL SET PROVE= CONTRATO.PROVE " & vbCrLf
sConsulta = sConsulta & "   FROM CONTRATO WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL WITH(NOLOCK) ON CONTRATO.INSTANCIA= PM_COPIA_ROL.INSTANCIA " & vbCrLf
sConsulta = sConsulta & "   AND CUANDO_ASIGNAR=3 AND CONTRATO.INSTANCIA = @INSTANCIA" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   --Roles de tipo Proveedor de Contrato" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO PM_COPIA_ROL_CON" & vbCrLf
sConsulta = sConsulta & "   SELECT PCR.ID,C.CONTACTO " & vbCrLf
sConsulta = sConsulta & "   FROM CONTRATO C WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL PCR WITH(NOLOCK) ON C.INSTANCIA= PCR.INSTANCIA " & vbCrLf
sConsulta = sConsulta & "   AND PCR.CUANDO_ASIGNAR=3 AND C.INSTANCIA = @INSTANCIA AND C.CONTACTO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_SALVAR_PARTICIPANTES]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_SALVAR_PARTICIPANTES]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_SALVAR_PARTICIPANTES] @ID INT, @PER VARCHAR(20), @PROVE VARCHAR(20), @CONTACTOS NVARCHAR(100)=NULL, @ROL INT AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   --  Guarda los participantes que haya que poner en esta etapa" & vbCrLf
sConsulta = sConsulta & "   UPDATE PM_COPIA_ROL " & vbCrLf
sConsulta = sConsulta & "   SET PER=CASE WHEN U.FSWS_SUSTITUCION IS NULL THEN CASE WHEN @PER='' THEN NULL ELSE @PER END ELSE U.FSWS_SUSTITUCION END" & vbCrLf
sConsulta = sConsulta & "   ,PER_ORIGINAL =@PER " & vbCrLf
sConsulta = sConsulta & "   , PROVE=CASE WHEN @PROVE='' THEN NULL ELSE @PROVE END" & vbCrLf
sConsulta = sConsulta & "   FROM PM_COPIA_ROL PCR WITH (NOLOCK)  " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN INSTANCIA WITH (NOLOCK) ON INSTANCIA.ID=@ID AND PCR.INSTANCIA=INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "       AND (INSTANCIA.ESTADO=0 OR INSTANCIA.ESTADO=2 OR INSTANCIA.ESTADO=3 OR INSTANCIA.ESTADO=4 OR INSTANCIA.ESTADO=6)" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN USU U WITH (NOLOCK) ON @PER = U.PER AND U.BAJA = 0" & vbCrLf
sConsulta = sConsulta & "   WHERE PCR.ID = @ROL" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   IF @CONTACTOS IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       DECLARE @CON INT" & vbCrLf
sConsulta = sConsulta & "       DECLARE @SEPARADOR_CON INT" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "       -- Inserta los contactos seleccionados del proveedor" & vbCrLf
sConsulta = sConsulta & "       SET @SEPARADOR_CON = CHARINDEX('|',@CONTACTOS)" & vbCrLf
sConsulta = sConsulta & "                       " & vbCrLf
sConsulta = sConsulta & "       WHILE @SEPARADOR_CON > 0" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           SET @CON = SUBSTRING(@CONTACTOS,1,@SEPARADOR_CON-1)" & vbCrLf
sConsulta = sConsulta & "           INSERT INTO PM_COPIA_ROL_CON (ID_ROL,ID_CON) VALUES (@ROL,@CON)" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "           SET @CONTACTOS = RIGHT(@CONTACTOS,LEN(@CONTACTOS)-@SEPARADOR_CON)   " & vbCrLf
sConsulta = sConsulta & "           SET @SEPARADOR_CON = CHARINDEX('|',@CONTACTOS)" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_CONTRATOS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_CONTRATOS]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_CONTRATOS] " & vbCrLf
sConsulta = sConsulta & "   @USU NVARCHAR(50)," & vbCrLf
sConsulta = sConsulta & "   @IDI NVARCHAR(3)," & vbCrLf
sConsulta = sConsulta & "   @CODIGO NVARCHAR(50)='', " & vbCrLf
sConsulta = sConsulta & "   @TIPO NVARCHAR(50)=''," & vbCrLf
sConsulta = sConsulta & "   @TITULO NVARCHAR(4000)=''," & vbCrLf
sConsulta = sConsulta & "   @PROVE NVARCHAR(200)=NULL," & vbCrLf
sConsulta = sConsulta & "   @PETICIONARIO NVARCHAR(200)=NULL," & vbCrLf
sConsulta = sConsulta & "   @FECHA_DESDE DATETIME=NULL, " & vbCrLf
sConsulta = sConsulta & "   @FECHA_HASTA DATETIME=NULL," & vbCrLf
sConsulta = sConsulta & "   @MATERIAL NVARCHAR (50)='', " & vbCrLf
sConsulta = sConsulta & "   @ART NVARCHAR(50)='', " & vbCrLf
sConsulta = sConsulta & "   @EMP INT=0, " & vbCrLf
sConsulta = sConsulta & "   @ANYO INT=0, " & vbCrLf
sConsulta = sConsulta & "   @GMN1 NVARCHAR (50)='', " & vbCrLf
sConsulta = sConsulta & "   @COD_PROCE INT=0, " & vbCrLf
sConsulta = sConsulta & "   @ESTADO  NVARCHAR (50)='', " & vbCrLf
sConsulta = sConsulta & "   @FILAESTADO NVARCHAR (50), " & vbCrLf
sConsulta = sConsulta & "   @NOM_TABLA NVARCHAR(50)=''," & vbCrLf
sConsulta = sConsulta & "   @ORDENACION NVARCHAR(50)=''," & vbCrLf
sConsulta = sConsulta & "   @PALABRA_CLAVE NVARCHAR(200)='', " & vbCrLf
sConsulta = sConsulta & "   @PAGENUMBER INT=1, " & vbCrLf
sConsulta = sConsulta & "   @PAGESIZE INT=1000, " & vbCrLf
sConsulta = sConsulta & "   @OBTENERTODOSCONTRATOS TINYINT=0," & vbCrLf
sConsulta = sConsulta & "   @CENTROCOSTE VARCHAR(50) = NULL," & vbCrLf
sConsulta = sConsulta & "   @PARTIDASPRES TBCADENATEXTO2 READONLY" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @SWHERE AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL_CONT AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL2 AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLFROM AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLFROM_EST AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PARAM NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SUSTITUTO AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @LONGGMN1 INT = 0" & vbCrLf
sConsulta = sConsulta & "DECLARE @LONGGMN2 INT = 0" & vbCrLf
sConsulta = sConsulta & "DECLARE @LONGGMN3 INT = 0" & vbCrLf
sConsulta = sConsulta & "DECLARE @LONGGMN4 INT = 0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @LONGGMN1 = LONGITUD FROM DIC WITH(NOLOCK) WHERE NOMBRE='GMN1'" & vbCrLf
sConsulta = sConsulta & "SELECT @LONGGMN2 = LONGITUD FROM DIC WITH(NOLOCK) WHERE NOMBRE='GMN2'" & vbCrLf
sConsulta = sConsulta & "SELECT @LONGGMN3 = LONGITUD FROM DIC WITH(NOLOCK) WHERE NOMBRE='GMN3'" & vbCrLf
sConsulta = sConsulta & "SELECT @LONGGMN4 = LONGITUD FROM DIC WITH(NOLOCK) WHERE NOMBRE='GMN4'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @FECHA_HASTA IS NOT NULL --para incluir los del mismo dia" & vbCrLf
sConsulta = sConsulta & "   SET @FECHA_HASTA =DATEADD(DD, 1, @FECHA_HASTA)" & vbCrLf
sConsulta = sConsulta & "SET @SWHERE = ''" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Tablas basicas para las consultas " & vbCrLf
sConsulta = sConsulta & "SET @SQLFROM = N' FROM CONTRATO C WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN INSTANCIA I WITH (NOLOCK) ON I.ID = C.INSTANCIA" & vbCrLf
sConsulta = sConsulta & "INNER JOIN SOLICITUD S WITH (NOLOCK) ON I.SOLICITUD=S.ID  " & vbCrLf
sConsulta = sConsulta & "INNER JOIN TIPO_SOLICITUDES TS WITH (NOLOCK) ON S.TIPO=TS.ID " & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PROVE PRO WITH (NOLOCK) ON PRO.COD = C.PROVE" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PER WITH (NOLOCK) ON PER.COD = I.PETICIONARIO" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN EMP WITH (NOLOCK) ON C.EMP=EMP.ID '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLFROM_EST = N' FROM CONTRATO C WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN INSTANCIA I WITH (NOLOCK)  ON I.ID = C.INSTANCIA" & vbCrLf
sConsulta = sConsulta & "INNER JOIN SOLICITUD S WITH (NOLOCK) ON I.SOLICITUD=S.ID  " & vbCrLf
sConsulta = sConsulta & "INNER JOIN TIPO_SOLICITUDES TS WITH (NOLOCK) ON S.TIPO=TS.ID '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SWHERE =' WHERE TS.TIPO=5 '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Condiciones where" & vbCrLf
sConsulta = sConsulta & "IF @CODIGO IS NOT NULL and @CODIGO <> ''" & vbCrLf
sConsulta = sConsulta & "    SET @SWHERE = @SWHERE +  ' AND C.CODIGO LIKE ''%' + @CODIGO + '%'''" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @TIPO IS NOT NULL and @TIPO<>''" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "    SET @TIPO = REPLACE(@TIPO,'$$$',',')" & vbCrLf
sConsulta = sConsulta & "    SET @SWHERE = @SWHERE + ' AND S.ID IN (' + @TIPO +')'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @TITULO IS NOT NULL AND @TITULO <> ''" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @SQLFROM = @SQLFROM + ' INNER JOIN INSTANCIA_VISORES_PM C1 WITH(NOLOCK) ON I.ID=C1.INSTANCIA'" & vbCrLf
sConsulta = sConsulta & "   SET @SWHERE = @SWHERE + ' AND (C1.TITULO_TEXT LIKE ''%' + @TITULO+ '%'' OR CONVERT(VARCHAR,TITULO_FEC) LIKE ''%' + @TITULO + '%'')'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @PROVE IS NOT NULL AND @PROVE <> ''" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "    SET @PROVE = REPLACE(@PROVE,'$$$',''',''')" & vbCrLf
sConsulta = sConsulta & "    SET @PROVE = '''' + @PROVE + ''''" & vbCrLf
sConsulta = sConsulta & "    SET @SWHERE = @SWHERE + ' AND PRO.COD IN (' + @PROVE + ')'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @PETICIONARIO IS NOT NULL AND @PETICIONARIO  <> ''" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "    SET @PETICIONARIO = REPLACE(@PETICIONARIO,'$$$',''',''')" & vbCrLf
sConsulta = sConsulta & "    SET @PETICIONARIO = '''' + @PETICIONARIO + ''''" & vbCrLf
sConsulta = sConsulta & "    SET @SWHERE = @SWHERE + ' AND PER.COD IN (' +@PETICIONARIO+')'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ESTADO IS NOT NULL AND @ESTADO<>''" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @ESTADO = REPLACE(@ESTADO,'$$$',',')" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @FILAESTADO IS NOT NULL AND @FILAESTADO  <>''" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @FILAESTADO = REPLACE(@FILAESTADO,'$$$',',')" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @FECHA_DESDE IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "      SET @SWHERE =@SWHERE + ' AND C.FEC_INI>= @FECHA_DESDE'" & vbCrLf
sConsulta = sConsulta & "      " & vbCrLf
sConsulta = sConsulta & "IF @FECHA_HASTA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "      SET @SWHERE = @SWHERE + ' AND C.FEC_FIN <= @FECHA_HASTA'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @EMP IS NOT NULL AND @EMP<>0 " & vbCrLf
sConsulta = sConsulta & "   SET @SWHERE = @SWHERE + ' AND EMP.ID = @EMP'" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "IF (@ANYO<>0 AND @GMN1<>'' AND @COD_PROCE<>0) OR (@MATERIAL IS NOT NULL AND @MATERIAL <>'') OR (@ART IS NOT NULL AND @ART <>'')" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @SQLFROM=@SQLFROM + ' INNER JOIN CONTRATO_ITEM CI WITH (NOLOCK) ON C.ID=CI.CONTRATO'" & vbCrLf
sConsulta = sConsulta & "   IF (@ANYO<>0 AND @GMN1<>'' AND @COD_PROCE<>0)" & vbCrLf
sConsulta = sConsulta & "       SET @SWHERE = @SWHERE + ' AND CI.GMN1=@GMN1 AND CI.PROCE=@COD_PROCE AND CI.ANYO=@ANYO'" & vbCrLf
sConsulta = sConsulta & "   DECLARE @HAYITEM BIT = 0" & vbCrLf
sConsulta = sConsulta & "   IF (@MATERIAL IS NOT NULL AND @MATERIAL <>'')" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "       --CUANDO SE BUSCA UN MATERIAL HAY QUE MIRAR EL MATERIAL (LOS GMN) DE LOS ITEMS ASOCIADOS AL CONTRATO (O SEA, LOS ITEMS QUE HAYA EN CONTRATO_ITEM PARA EL CONTRATO, QUE A SU VEZ ENLAZAN A LA TABLA ITEM: CONTRATO->CONTRATO_ITEM->ITEM)" & vbCrLf
sConsulta = sConsulta & "       --  EN ITEM, SI EL CAMPO ART VIENE INFORMADO, HAY QUE BUSCAR SU MATERIAL EN LA TABLA ART4 (RELACION:ART4.COD = ITEM.ART)" & vbCrLf
sConsulta = sConsulta & "       --  EN ITEM, SI EL CAMPO ART NO VIENE INFORMADO, LOS GMN DEL MATERIAL ESTAR�N DEFINIDOS DIRECTAMENTE EN LA TABLA ITEM." & vbCrLf
sConsulta = sConsulta & "       SET @SQLFROM = @SQLFROM + ' INNER JOIN ITEM IT WITH(NOLOCK) ON IT.ANYO = CI.ANYO AND IT.GMN1_PROCE = CI.GMN1 AND IT.PROCE = CI.PROCE AND IT.ID=CI.ITEM AND CASE WHEN ART IS NULL THEN (SPACE(@LONGGMN1-LEN(IT.GMN1)) + COALESCE(IT.GMN1,'''') + SPACE(@LONGGMN2-LEN(IT.GMN2)) + COALESCE(IT.GMN2,'''') + SPACE(@LONGGMN3-LEN(IT.GMN3)) + COALESCE(IT.GMN3,'''') + SPACE(@LONGGMN4-LEN(IT.GMN4)) + COALESCE(IT.GMN4,'''')) ELSE @MATERIAL END = @MATERIAL" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN ART4 A4 WITH(NOLOCK) ON A4.COD = IT.ART AND CASE WHEN ART IS NULL THEN @MATERIAL ELSE (SPACE(@LONGGMN1-LEN(A4.GMN1)) + COALESCE(A4.GMN1,'''') + SPACE(@LONGGMN2-LEN(A4.GMN2)) + COALESCE(A4.GMN2,'''') + SPACE(@LONGGMN3-LEN(A4.GMN3)) + COALESCE(A4.GMN3,'''') + SPACE(@LONGGMN4-LEN(A4.GMN4)) + COALESCE(A4.GMN4,'''')) END = @MATERIAL'" & vbCrLf
sConsulta = sConsulta & "       SET @HAYITEM = 1" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "   IF (@ART IS NOT NULL AND @ART <>'')" & vbCrLf
sConsulta = sConsulta & "       IF @HAYITEM = 0" & vbCrLf
sConsulta = sConsulta & "           SET @SQLFROM = @SQLFROM + ' INNER JOIN ITEM IT WITH(NOLOCK) ON IT.ANYO = CI.ANYO AND IT.GMN1_PROCE = CI.GMN1 AND IT.PROCE = CI.PROCE AND IT.ID=CI.ITEM " & vbCrLf
sConsulta = sConsulta & "           AND IT.ART = @ART '" & vbCrLf
sConsulta = sConsulta & "       ELSE" & vbCrLf
sConsulta = sConsulta & "           SET @SQLFROM = @SQLFROM + ' AND IT.ART = @ART '" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "IF @NOM_TABLA IS NOT NULL AND @NOM_TABLA <>''" & vbCrLf
sConsulta = sConsulta & "   SET @SQLFROM = @SQLFROM + ' INNER JOIN ' + @IDI + '_' + @NOM_TABLA + ' F WITH (NOLOCK) ON I.ID = F.INSTANCIA'" & vbCrLf
sConsulta = sConsulta & "        " & vbCrLf
sConsulta = sConsulta & "IF @PALABRA_CLAVE IS NOT NULL AND @PALABRA_CLAVE <> ''" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @SQLFROM = @SQLFROM + ' INNER JOIN PM_COPIA_ADJUN PCA WITH(NOLOCK) ON C.ID=PCA.ID_CONTRATO'" & vbCrLf
sConsulta = sConsulta & "   SET @SWHERE = @SWHERE + ' AND PCA.DATA LIKE CAST(''%' + @PALABRA_CLAVE + '%'' as varbinary(max))'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "IF (SELECT COUNT(1) FROM USU U WITH (NOLOCK) WHERE U.FSWS_SUSTITUCION=@USU)>0 " & vbCrLf
sConsulta = sConsulta & " SET @SUSTITUTO =1" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & " SET @SUSTITUTO =0" & vbCrLf
sConsulta = sConsulta & "--CALIDAD " & vbCrLf
sConsulta = sConsulta & "IF (EXISTS(SELECT 1 FROM @PARTIDASPRES))" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @SQLFROM = @SQLFROM + ' INNER JOIN INSTANCIA_CENTROS_PARTIDAS ICP1 WITH(NOLOCK) ON I.ID=ICP1.INSTANCIA AND ICP1.TIPO_CAMPO_GS = 130 '" & vbCrLf
sConsulta = sConsulta & "   --CALIDAD" & vbCrLf
sConsulta = sConsulta & "   SET @SQLFROM = @SQLFROM + ' INNER JOIN @PARTIDASPRES PP ON ICP1.PRES5 = PP.CADENA1 AND ICP1.VALOR_TEXT = PP.CADENA2 '" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "IF (NOT @CENTROCOSTE IS NULL AND @CENTROCOSTE <> '')" & vbCrLf
sConsulta = sConsulta & "   SET @SQLFROM = @SQLFROM + ' INNER JOIN INSTANCIA_CENTROS_PARTIDAS ICP2 WITH(NOLOCK) ON I.ID=ICP2.INSTANCIA AND ICP2.TIPO_CAMPO_GS = 129 AND ICP2.VALOR_TEXT = @CENTROCOSTE '" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "--------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--crea la tabla temporal" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE #G1 (ID INT,CODIGO NVARCHAR(50),ICONO TINYINT,OBV TINYINT,ACCION_APROBAR INT,ACCION_RECHAZAR INT)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = ''" & vbCrLf
sConsulta = sConsulta & "SET @PARAM =N'@USU NVARCHAR (50),@CODIGO NVARCHAR(50)='''',@TIPO NVARCHAR(50),@FECHA_DESDE DATETIME=NULL,@FECHA_HASTA DATETIME=NULL,@MATERIAL NVARCHAR (50)='''',@ART NVARCHAR (50)='''',@EMP INT=0,@GMN1 NVARCHAR (50)='''',@COD_PROCE INT=0,@ANYO INT=0,@ESTADO NVARCHAR (50)='''',@FILAESTADO NVARCHAR (50)='''',@LONGGMN1 INT,@LONGGMN2 INT,@LONGGMN3 INT,@LONGGMN4 INT,@CENTROCOSTE VARCHAR(50) = NULL, @PARTIDASPRES TBCADENATEXTO2 READONLY'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--1. Solicitudes abiertas por usted." & vbCrLf
sConsulta = sConsulta & "CREATE TABLE #T_ABIERTAS (ID INT, CODIGO NVARCHAR(50))" & vbCrLf
sConsulta = sConsulta & "SET @SQL = 'INSERT INTO #T_ABIERTAS (ID,CODIGO) SELECT I.ID,C.CODIGO ' + @SQLFROM + @SWHERE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' AND I.NUM IS NOT NULL AND I.PETICIONARIO = @USU AND I.EN_PROCESO<>2'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL,@PARAM,@CODIGO=@CODIGO,@TIPO=@TIPO,@FECHA_DESDE=@FECHA_DESDE,@FECHA_HASTA=@FECHA_HASTA,@MATERIAL=@MATERIAL,@ART=@ART,@EMP=@EMP,@GMN1=@GMN1,@COD_PROCE=@COD_PROCE,@ANYO=@ANYO,@ESTADO=@ESTADO,@FILAESTADO=@FILAESTADO,@USU=@USU,@LONGGMN1=@LONGGMN1,@LONGGMN2=@LONGGMN2,@LONGGMN3=@LONGGMN3,@LONGGMN4=@LONGGMN4,@CENTROCOSTE=@CENTROCOSTE,@PARTIDASPRES=@PARTIDASPRES" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--2. Procesos en los que particip�  " & vbCrLf
sConsulta = sConsulta & "CREATE TABLE #T_PARTICIPO (ID INT,CODIGO NVARCHAR(50))" & vbCrLf
sConsulta = sConsulta & "SET @SQL = 'INSERT INTO #T_PARTICIPO (ID,CODIGO) SELECT I.ID,C.CODIGO ' + @SQLFROM " & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ' INNER JOIN INSTANCIA_EST WITH (NOLOCK) ON I.ID=INSTANCIA_EST.INSTANCIA AND INSTANCIA_EST.PER=@USU'" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + @SWHERE + ' AND I.NUM IS NOT NULL AND  (INSTANCIA_EST.ROL IS NOT NULL OR (INSTANCIA_EST.ROL IS  NULL AND (INSTANCIA_EST.ACCION=3 OR INSTANCIA_EST.ACCION=4)))'  " & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL,@PARAM,@CODIGO=@CODIGO,@TIPO=@TIPO,@FECHA_DESDE=@FECHA_DESDE,@FECHA_HASTA=@FECHA_HASTA,@MATERIAL=@MATERIAL,@ART=@ART,@EMP=@EMP,@GMN1=@GMN1,@COD_PROCE=@COD_PROCE,@ANYO=@ANYO,@ESTADO=@ESTADO,@FILAESTADO=@FILAESTADO,@USU=@USU,@LONGGMN1=@LONGGMN1,@LONGGMN2=@LONGGMN2,@LONGGMN3=@LONGGMN3,@LONGGMN4=@LONGGMN4,@CENTROCOSTE=@CENTROCOSTE,@PARTIDASPRES=@PARTIDASPRES" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--3. Pendientes de aprobar por usted" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE #T_PENDIENTES (ID INT,CODIGO NVARCHAR(50), ACCION_APROBAR INT, ACCION_RECHAZAR INT)" & vbCrLf
sConsulta = sConsulta & "  SET @SQL = 'INSERT INTO #T_PENDIENTES (ID,CODIGO, ACCION_APROBAR,ACCION_RECHAZAR ) SELECT I.ID,C.CODIGO,isnull(RAA.ACCION,0) AS ACCION_APROBAR,isnull(RAR.ACCION,0) AS ACCION_RECHAZAR ' + @SQLFROM " & vbCrLf
sConsulta = sConsulta & "  SET @SQL=@SQL + ' INNER JOIN INSTANCIA_BLOQUE IB WITH (NOLOCK) ON I.ID=IB.INSTANCIA AND IB.EN_PROCESO=0 AND IB. ESTADO=1 AND IB.BLOQ<>1'" & vbCrLf
sConsulta = sConsulta & "  SET @SQL=@SQL + ' INNER JOIN PM_COPIA_ROL_BLOQUE RB WITH (NOLOCK) ON RB.BLOQUE=IB.BLOQUE AND IB.ESTADO=1 AND IB.BLOQ<>1 AND IB.TRASLADADA<>1'" & vbCrLf
sConsulta = sConsulta & "  SET @SQL=@SQL + ' INNER JOIN PM_COPIA_ROL R WITH (NOLOCK)  ON RB.ROL=R.ID'" & vbCrLf
sConsulta = sConsulta & "  SET @SQL=@SQL + ' INNER JOIN PM_COPIA_BLOQUE B WITH (NOLOCK) ON RB.BLOQUE=B.ID'" & vbCrLf
sConsulta = sConsulta & "  SET @SQL=@SQL + ' LEFT JOIN PM_COPIA_ROL_ACCION RAA WITH(NOLOCK)ON RAA.ROL=R.ID AND RAA.BLOQUE=RB.BLOQUE AND RAA.APROBAR=1'   --acciones del rol actual (Aprobar)" & vbCrLf
sConsulta = sConsulta & "  SET @SQL=@SQL + ' LEFT JOIN PM_COPIA_ROL_ACCION RAR WITH(NOLOCK)ON RAR.ROL=R.ID AND RAR.BLOQUE=RB.BLOQUE AND RAR.RECHAZAR=1'   --acciones del rol actual (Rechazar)" & vbCrLf
sConsulta = sConsulta & "  SET @SQL = @SQL + @SWHERE + ' AND I.NUM IS NOT NULL AND I.EN_PROCESO=0 AND I.TRASLADADA=0 AND (I.ESTADO = 2 OR I.ESTADO = 3 OR I.ESTADO = 4) AND '" & vbCrLf
sConsulta = sConsulta & "  IF @SUSTITUTO =1" & vbCrLf
sConsulta = sConsulta & "    SET @SQL=@SQL + ' ( (R.PER=@USU AND (SELECT ISNULL(U.FSWS_SUSTITUCION,'''') FROM USU U WITH (NOLOCK) WHERE U.PER=@USU AND U.BAJA=0) = '''')  OR R.PER IN ( SELECT U.PER FROM USU U WITH (NOLOCK) WHERE U.FSWS_SUSTITUCION=@USU))'" & vbCrLf
sConsulta = sConsulta & "  ELSE" & vbCrLf
sConsulta = sConsulta & "    SET @SQL=@SQL + '( (R.PER=@USU AND (SELECT ISNULL(U.FSWS_SUSTITUCION,'''') FROM USU U WITH (NOLOCK) WHERE U.PER=@USU AND U.BAJA=0) = ''''))'" & vbCrLf
sConsulta = sConsulta & "  SET @SQL=@SQL + ' AND NOT (B.TIPO=1 AND I.PETICIONARIO<>R.PER)'  " & vbCrLf
sConsulta = sConsulta & "  SET @SQL=@SQL + ' UNION '" & vbCrLf
sConsulta = sConsulta & "  SET @SQL=@SQL + ' SELECT I.ID,C.CODIGO,isnull(RAA.ACCION,0) AS ACCION_APROBAR, isnull(RAR.ACCION,0) AS ACCION_RECHAZAR' + @SQLFROM + ' INNER JOIN INSTANCIA_BLOQUE IB WITH (NOLOCK) ON I.ID=IB.INSTANCIA AND IB.EN_PROCESO=0 AND IB.ESTADO=1 AND IB.BLOQ<>1 AND IB.TRASLADADA<>1'" & vbCrLf
sConsulta = sConsulta & "  SET @SQL=@SQL + ' INNER JOIN PM_COPIA_ROL_BLOQUE RB WITH (NOLOCK)  ON RB.BLOQUE=IB.BLOQUE'" & vbCrLf
sConsulta = sConsulta & "  SET @SQL=@SQL + ' INNER JOIN PM_COPIA_ROL R WITH (NOLOCK)  ON RB.ROL=R.ID AND R.PER IS NULL AND R.COMO_ASIGNAR=3  AND R.TIPO<>5'" & vbCrLf
sConsulta = sConsulta & "  SET @SQL=@SQL + '  INNER JOIN PM_COPIA_PARTICIPANTES PT WITH (NOLOCK)  ON PT.ROL=R.ID'" & vbCrLf
sConsulta = sConsulta & "  SET @SQL=@SQL + ' LEFT JOIN PM_COPIA_ROL_ACCION RAA WITH(NOLOCK)ON RAA.ROL=R.ID AND RAA.BLOQUE=RB.BLOQUE AND RAA.APROBAR=1'   --acciones del rol actual (Aprobar)" & vbCrLf
sConsulta = sConsulta & "  SET @SQL=@SQL + ' LEFT JOIN PM_COPIA_ROL_ACCION RAR WITH(NOLOCK)ON RAR.ROL=R.ID AND RAR.BLOQUE=RB.BLOQUE AND RAR.RECHAZAR=1'   --acciones del rol actual (Rechazar)" & vbCrLf
sConsulta = sConsulta & "  SET @SQL=@SQL + @SWHERE + ' AND I.NUM IS NOT NULL AND I.EN_PROCESO=0 AND I.TRASLADADA=0 AND I.ESTADO=2 AND '" & vbCrLf
sConsulta = sConsulta & "  IF @SUSTITUTO =1" & vbCrLf
sConsulta = sConsulta & "    SET @SQL=@SQL + ' ( (PT.PER=@USU AND (SELECT ISNULL(U.FSWS_SUSTITUCION,'''') FROM USU U WITH (NOLOCK) WHERE U.PER=@USU AND U.BAJA=0) = '''') OR PT.PER IN ( SELECT U.PER FROM USU U WITH (NOLOCK) WHERE U.FSWS_SUSTITUCION=@USU))'" & vbCrLf
sConsulta = sConsulta & "  ELSE" & vbCrLf
sConsulta = sConsulta & "    SET @SQL=@SQL + ' ( (PT.PER=@USU AND (SELECT ISNULL(U.FSWS_SUSTITUCION,'''') FROM USU U WITH (NOLOCK) WHERE U.PER=@USU AND U.BAJA=0) = ''''))'                                         " & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL,@PARAM,@CODIGO=@CODIGO,@TIPO=@TIPO,@FECHA_DESDE=@FECHA_DESDE,@FECHA_HASTA=@FECHA_HASTA,@MATERIAL=@MATERIAL,@ART=@ART,@EMP=@EMP,@GMN1=@GMN1,@COD_PROCE=@COD_PROCE,@ANYO=@ANYO,@ESTADO=@ESTADO,@FILAESTADO=@FILAESTADO,@USU=@USU,@LONGGMN1=@LONGGMN1,@LONGGMN2=@LONGGMN2,@LONGGMN3=@LONGGMN3,@LONGGMN4=@LONGGMN4,@CENTROCOSTE=@CENTROCOSTE,@PARTIDASPRES=@PARTIDASPRES" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--4. Pendientes de devoluci�n" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE #T_DEVOLUCION (ID INT,CODIGO NVARCHAR(50))" & vbCrLf
sConsulta = sConsulta & " SET @SQL = 'INSERT INTO #T_DEVOLUCION (ID,CODIGO) SELECT I.ID,C.CODIGO ' + @SQLFROM " & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + ' INNER JOIN INSTANCIA_BLOQUE IB WITH (NOLOCK) ON I.ID=IB.INSTANCIA AND IB.EN_PROCESO=0 AND IB.ESTADO=1 AND IB.BLOQ<>1 AND IB.TRASLADADA=1'" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + @SWHERE + ' AND I.NUM IS NOT NULL AND I.TRASLADADA=1'" & vbCrLf
sConsulta = sConsulta & "   IF @SUSTITUTO =1" & vbCrLf
sConsulta = sConsulta & "      SET @SQL = @SQL + ' AND ((TRASLADO_A=@USU AND (SELECT ISNULL(U.FSWS_SUSTITUCION,'''') FROM USU U WITH (NOLOCK) WHERE U.PER=@USU AND U.BAJA=0) = '''') OR (TRASLADO_A IN (SELECT U.PER FROM USU U WITH (NOLOCK) WHERE U.FSWS_SUSTITUCION=@USU)))'" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "      SET @SQL = @SQL + ' AND (TRASLADO_A=@USU AND (SELECT ISNULL(U.FSWS_SUSTITUCION,'''') FROM USU U WITH (NOLOCK) WHERE U.PER=@USU AND U.BAJA=0) = '''')'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL,@PARAM,@CODIGO=@CODIGO,@TIPO=@TIPO,@FECHA_DESDE=@FECHA_DESDE,@FECHA_HASTA=@FECHA_HASTA,@MATERIAL=@MATERIAL,@ART=@ART,@EMP=@EMP,@GMN1=@GMN1,@COD_PROCE=@COD_PROCE,@ANYO=@ANYO,@ESTADO=@ESTADO,@FILAESTADO=@FILAESTADO,@USU=@USU,@LONGGMN1=@LONGGMN1,@LONGGMN2=@LONGGMN2,@LONGGMN3=@LONGGMN3,@LONGGMN4=@LONGGMN4,@CENTROCOSTE=@CENTROCOSTE,@PARTIDASPRES=@PARTIDASPRES" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--5. Trasladas" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE #T_TRASLADADAS (ID INT,CODIGO NVARCHAR(50))" & vbCrLf
sConsulta = sConsulta & "SET @SQL = 'INSERT INTO #T_TRASLADADAS (ID,CODIGO) SELECT I.ID,C.CODIGO ' + @SQLFROM " & vbCrLf
sConsulta = sConsulta & "    SET @SQL=@SQL + ' INNER JOIN INSTANCIA_BLOQUE IB WITH (NOLOCK) ON IB.INSTANCIA=I.ID AND IB.ESTADO=1 AND IB.TRASLADADA=1 AND IB.BLOQ=0'" & vbCrLf
sConsulta = sConsulta & "    SET @SQL = @SQL + @SWHERE + '  AND I.NUM IS NOT NULL AND I.TRASLADADA=1 AND ' " & vbCrLf
sConsulta = sConsulta & "    IF @SUSTITUTO =1" & vbCrLf
sConsulta = sConsulta & "      SET @SQL = @SQL + '((DEVOLUCION_A=@USU AND (SELECT ISNULL(U.FSWS_SUSTITUCION,'''') FROM USU U WITH (NOLOCK) WHERE U.PER=@USU AND U.BAJA=0) = '''') OR (DEVOLUCION_A IN ( SELECT U.PER FROM USU U WITH (NOLOCK) WHERE U.FSWS_SUSTITUCION=@USU)))'" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "     SET @SQL = @SQL + '(DEVOLUCION_A=@USU AND (SELECT ISNULL(U.FSWS_SUSTITUCION,'''') FROM USU U WITH (NOLOCK) WHERE U.PER=@USU AND U.BAJA=0) = '''')'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL,@PARAM,@CODIGO=@CODIGO,@TIPO=@TIPO,@FECHA_DESDE=@FECHA_DESDE,@FECHA_HASTA=@FECHA_HASTA,@MATERIAL=@MATERIAL,@ART=@ART,@EMP=@EMP,@GMN1=@GMN1,@COD_PROCE=@COD_PROCE,@ANYO=@ANYO,@ESTADO=@ESTADO,@FILAESTADO=@FILAESTADO,@USU=@USU,@LONGGMN1=@LONGGMN1,@LONGGMN2=@LONGGMN2,@LONGGMN3=@LONGGMN3,@LONGGMN4=@LONGGMN4,@CENTROCOSTE=@CENTROCOSTE,@PARTIDASPRES=@PARTIDASPRES" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--6. solicitudes de las que es observador" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE #T_OTRAS (ID INT,CODIGO NVARCHAR(50))" & vbCrLf
sConsulta = sConsulta & "SET @SQL = 'INSERT INTO #T_OTRAS (ID,CODIGO) SELECT I.ID,C.CODIGO ' + @SQLFROM " & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL +' INNER JOIN PM_ROL PR WITH (NOLOCK) ON PR.WORKFLOW=S.WORKFLOW'" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + @SWHERE + '  AND I.NUM IS NOT NULL AND PR.TIPO=5 AND (" & vbCrLf
sConsulta = sConsulta & "                                       PR.PER = @USU -- EL USUARIO ES OBSERVADOR" & vbCrLf
sConsulta = sConsulta & "                                       OR EXISTS (SELECT TOP 1 V.COD FROM VIEW_ROL_OBSERVADOR V WITH(NOLOCK) WHERE S.ID = V.SOLICITUD AND V.COD=@USU)" & vbCrLf
sConsulta = sConsulta & "                                       OR PR.UON0 = 1 --EL NIVEL UON0, O SEA, RA�Z (TODA LA ORGANIZACI�N) ES OBSERVADOR" & vbCrLf
sConsulta = sConsulta & "                                       )" & vbCrLf
sConsulta = sConsulta & "                                   AND I.ESTADO>0 AND I.EN_PROCESO<>2'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL,@PARAM,@CODIGO=@CODIGO,@TIPO=@TIPO,@FECHA_DESDE=@FECHA_DESDE,@FECHA_HASTA=@FECHA_HASTA,@MATERIAL=@MATERIAL,@ART=@ART,@EMP=@EMP,@GMN1=@GMN1,@COD_PROCE=@COD_PROCE,@ANYO=@ANYO,@ESTADO=@ESTADO,@FILAESTADO=@FILAESTADO,@USU=@USU,@LONGGMN1=@LONGGMN1,@LONGGMN2=@LONGGMN2,@LONGGMN3=@LONGGMN3,@LONGGMN4=@LONGGMN4,@CENTROCOSTE=@CENTROCOSTE,@PARTIDASPRES=@PARTIDASPRES" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL=''" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--1. Solicitudes abiertas por usted." & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' SELECT DISTINCT ID,CODIGO,0 as ICONO, 0 as OBV, 0 as ACCION_APROBAR, 0 as ACCION_RECHAZAR FROM #T_ABIERTAS WHERE 1=1'" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' AND ID NOT IN (SELECT ID FROM #T_PENDIENTES)' " & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' AND ID NOT IN (SELECT ID FROM #T_DEVOLUCION)'" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' AND ID NOT IN (SELECT ID FROM #T_TRASLADADAS)'" & vbCrLf
sConsulta = sConsulta & "SET @SQL= @SQL + ' UNION ALL '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--2. Procesos en los que particip�   " & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' SELECT DISTINCT ID,CODIGO,0 AS ICONO,0 AS OBV, 0 as ACCION_APROBAR, 0 as ACCION_RECHAZAR FROM #T_PARTICIPO WHERE 1=1'" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' AND ID NOT IN (SELECT ID FROM #T_ABIERTAS)'" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' AND ID NOT IN (SELECT ID FROM #T_PENDIENTES)'" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' AND ID NOT IN (SELECT ID FROM #T_DEVOLUCION)'" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' AND ID NOT IN (SELECT ID FROM #T_TRASLADADAS)' " & vbCrLf
sConsulta = sConsulta & "SET @SQL= @SQL + ' UNION ALL '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--3. Pendientes de tratar" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' SELECT DISTINCT ID,CODIGO,1 AS ICONO,0 AS OBV, ACCION_APROBAR, ACCION_RECHAZAR FROM #T_PENDIENTES'" & vbCrLf
sConsulta = sConsulta & "SET @SQL= @SQL + ' UNION ALL '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--4. Pendientes de devoluci�n" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' SELECT DISTINCT ID,CODIGO, 1 AS ICONO,0 AS OBV, 0 as ACCION_APROBAR, 0 as ACCION_RECHAZAR  FROM #T_DEVOLUCION'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL= @SQL + ' UNION ALL '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--5-Solicitudes trasladadas en espera de devoluci�n" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL +' SELECT DISTINCT ID, CODIGO,0 AS ICONO,0 AS OBV, 0 as ACCION_APROBAR, 0 as ACCION_RECHAZAR FROM #T_TRASLADADAS'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL= @SQL + ' UNION ALL '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--6-OTRAS SOLICITUDES (OBSERVADORES)" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL +' SELECT DISTINCT ID,CODIGO,0 AS ICONO,1 AS OBV, 0 as ACCION_APROBAR, 0 as ACCION_RECHAZAR FROM #T_OTRAS WHERE 1=1'" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' AND ID NOT IN (SELECT ID FROM #T_PENDIENTES)'" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' AND ID NOT IN (SELECT ID FROM #T_DEVOLUCION)'" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' AND ID NOT IN (SELECT ID FROM #T_TRASLADADAS)'" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' AND ID NOT IN (SELECT ID FROM #T_PARTICIPO)'   " & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' AND ID NOT IN (SELECT ID FROM #T_ABIERTAS)'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-----------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "--Solicitudes que el usuario puede iniciar flujo de modificaci�n" & vbCrLf
sConsulta = sConsulta & "-----------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "SET @SQL= @SQL + ' UNION ALL '" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' SELECT DISTINCT I.ID,C.CODIGO, 0 AS ICONO,0 AS OBV, 0 as ACCION_APROBAR, 0 as ACCION_RECHAZAR ' + @SQLFROM" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' INNER JOIN VIEW_ROL_PETICIONARIO_MODIF V WITH(NOLOCK) ON V.SOLICITUD=S.ID AND V.COD=@USU'" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + @SWHERE + ' AND I.NUM IS NOT NULL AND I.ESTADO>=100 AND I.EN_PROCESO<>2'" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' AND I.ID NOT IN (SELECT ID FROM #T_PENDIENTES WITH(NOLOCK) UNION SELECT ID FROM #T_DEVOLUCION WITH(NOLOCK))'" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' UNION ALL SELECT DISTINCT I.ID,C.CODIGO, 0 AS ICONO,0 AS OBV, 0 as ACCION_APROBAR, 0 as ACCION_RECHAZAR ' + @SQLFROM" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' INNER JOIN VIEW_ROL_PETICIONARIO_MODIF V WITH(NOLOCK) ON V.SOLICITUD=S.ID AND V.COD=@USU'" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + @SWHERE + ' AND I.NUM IS NOT NULL AND I.ESTADO=3 AND I.EN_PROCESO<>2'" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' AND I.ID NOT IN (SELECT ID FROM #T_PENDIENTES WITH(NOLOCK) UNION SELECT ID FROM #T_DEVOLUCION WITH(NOLOCK))'" & vbCrLf
sConsulta = sConsulta & "  " & vbCrLf
sConsulta = sConsulta & "-----------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "--Solicitudes que el usuario puede iniciar flujo de baja" & vbCrLf
sConsulta = sConsulta & "-----------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "SET @SQL= @SQL + ' UNION ALL '" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' SELECT DISTINCT I.ID,C.CODIGO, 0 AS ICONO,0 AS OBV, 0 as ACCION_APROBAR, 0 as ACCION_RECHAZAR ' + @SQLFROM" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' INNER JOIN VIEW_ROL_PETICIONARIO_BAJA V WITH(NOLOCK) ON V.SOLICITUD=S.ID AND V.COD=@USU'" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + @SWHERE + ' AND I.NUM IS NOT NULL AND I.EN_PROCESO<>2 AND I.ESTADO>=100'" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' AND I.ID NOT IN (SELECT ID FROM #T_PENDIENTES WITH(NOLOCK) UNION SELECT ID FROM #T_DEVOLUCION WITH(NOLOCK))'" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' UNION ALL SELECT DISTINCT I.ID,C.CODIGO, 0 AS ICONO,0 AS OBV, 0 as ACCION_APROBAR, 0 as ACCION_RECHAZAR ' + @SQLFROM" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' INNER JOIN VIEW_ROL_PETICIONARIO_BAJA V WITH(NOLOCK) ON V.SOLICITUD=S.ID AND V.COD=@USU'" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + @SWHERE + ' AND I.NUM IS NOT NULL AND I.EN_PROCESO<>2 AND I.ESTADO=4'" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' AND I.ID NOT IN (SELECT ID FROM #T_PENDIENTES WITH(NOLOCK) UNION SELECT ID FROM #T_DEVOLUCION WITH(NOLOCK))'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL='INSERT INTO #G1 (ID,CODIGO, ICONO,OBV,ACCION_APROBAR,ACCION_RECHAZAR)' +  @SQL" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL,@PARAM,@CODIGO=@CODIGO,@TIPO=@TIPO,@FECHA_DESDE=@FECHA_DESDE,@FECHA_HASTA=@FECHA_HASTA,@MATERIAL=@MATERIAL,@ART=@ART,@EMP=@EMP,@GMN1=@GMN1,@COD_PROCE=@COD_PROCE,@ANYO=@ANYO,@ESTADO=@ESTADO,@FILAESTADO=@FILAESTADO,@USU=@USU,@LONGGMN1=@LONGGMN1,@LONGGMN2=@LONGGMN2,@LONGGMN3=@LONGGMN3,@LONGGMN4=@LONGGMN4,@CENTROCOSTE=@CENTROCOSTE,@PARTIDASPRES=@PARTIDASPRES" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--*************************************************SELECT GEN�RICA PARA SACAR LOS DATOS ***********************************************************" & vbCrLf
sConsulta = sConsulta & "--Creamos una tabla temporal" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE #G2(ID INT, CODIGO NVARCHAR(50), FEC_ALTA DATETIME, CODESTADO INT, FECHA_INICIO DATETIME,FECHA_EXP DATETIME, CODPRO NVARCHAR(40), PROVEEDOR NVARCHAR(250),CODEMP NVARCHAR(40), EMPRESA NVARCHAR(150),ESTADO NVARCHAR(50),CODPET NVARCHAR(40),PETICIONARIO NVARCHAR(250),TIPO_CONTRATO NVARCHAR(250),ULT_VERSION INT, INSTANCIA_ESTADO INT,ID_CONTRATO INT,ICONO TINYINT, OBSERVADOR TINYINT, TRASLADADA TINYINT,MON NVARCHAR(5),CONTACTO NVARCHAR(100),IMPORTE FLOAT,PROCE NVARCHAR(100),EN_PROCESO TINYINT, ACCION_APROBAR INT, ACCION_RECHAZAR INT, BLOQUE INT)" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE #G3(ID INT, CODIGO NVARCHAR(50), FEC_ALTA DATETIME, CODESTADO INT, FECHA_INICIO DATETIME,FECHA_EXP DATETIME, CODPRO NVARCHAR(40), PROVEEDOR NVARCHAR(250),CODEMP NVARCHAR(40), EMPRESA NVARCHAR(150),ESTADO NVARCHAR(50),CODPET NVARCHAR(40),PETICIONARIO NVARCHAR(250),TIPO_CONTRATO NVARCHAR(250),ULT_VERSION INT, INSTANCIA_ESTADO INT,ID_CONTRATO INT,ICONO TINYINT, OBSERVADOR TINYINT, TRASLADADA TINYINT,MON NVARCHAR(5),CONTACTO NVARCHAR(100),IMPORTE FLOAT,PROCE NVARCHAR(100),EN_PROCESO TINYINT, ACCION_APROBAR INT, ACCION_RECHAZAR INT, BLOQUE INT)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL=''" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'SELECT I.ID AS ID,C.CODIGO,I.FEC_ALTA AS FECHA_ALTA,C.ESTADO CODESTADO,C.FEC_INI FECHA_INICIO,C.FEC_FIN FECHA_EXP,PRO.COD CODPRO,PRO.DEN PROVEEDOR,EMP.NIF CODEMP,  EMP.DEN EMPRESA,NULL AS ESTADO,PER.COD CODPET ,PER.NOM + '' '' + PER.APE AS PETICIONARIO, S.DEN_'+ @IDI + ' TIPO_CONTRATO, I.ULT_VERSION,I.ESTADO INSTANCIA_ESTADO,C.ID ID_CONTRATO,G1.ICONO,G1.OBV,I.TRASLADADA,C.MON,CON.NOM + '' '' + CON.APE AS CONTACTO,I.IMPORTE,CI.PROCESO,I.EN_PROCESO,G1.ACCION_APROBAR,G1.ACCION_RECHAZAR,M.BLOQUE '" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL +  ' FROM  INSTANCIA I WITH (NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL +  ' INNER JOIN #G1 G1 WITH(NOLOCK) ON I.ID=G1.ID'" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL +  ' INNER JOIN CONTRATO C WITH(NOLOCK) ON C.INSTANCIA=G1.ID'" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL +  ' LEFT JOIN (SELECT DISTINCT CONTRATO,ANYO,GMN1,PROCE,convert(NVARCHAR(50),ANYO) + ''/'' + GMN1 + ''/'' + convert(NVARCHAR(50),PROCE) AS PROCESO FROM CONTRATO_ITEM CI WITH(NOLOCK) ) CI ON CI.CONTRATO=C.ID'" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL +  ' INNER JOIN SOLICITUD S WITH (NOLOCK) ON I.SOLICITUD=S.ID'   --para sacar la solicitud" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL +  ' INNER JOIN TIPO_SOLICITUDES TS WITH (NOLOCK) ON S.TIPO=TS.ID'   --para sacar el tipo de solicitud" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL +  ' LEFT JOIN PROVE PRO WITH (NOLOCK) ON PRO.COD = C.PROVE'" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL +  ' LEFT JOIN CON CON WITH (NOLOCK) ON CON.PROVE = C.PROVE AND C.CONTACTO=CON.ID'" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL +  ' INNER JOIN PER WITH (NOLOCK) ON PER.COD = I.PETICIONARIO'" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL +  ' LEFT JOIN EMP WITH (NOLOCK) ON C.EMP=EMP.ID'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Mira si la persona es un rol de la instancia, y en ese caso si tiene permisos para ver el detalle del flujo y el detalle de los participantes:" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' LEFT JOIN(SELECT INSTANCIA I,PER P,MAX(VER_DETALLE_PER)VDP,MAX(VER_FLUJO)VER_FLUJO FROM PM_COPIA_ROL WITH(NOLOCK) GROUP BY INSTANCIA,PER) CR ON I.ID = CR.I AND @USU=CR.P'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Para obtener si hay etapas en paralelo" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ' INNER JOIN (SELECT INSTANCIA I, COUNT(*) P,MIN(BLOQUE) BLOQUE FROM INSTANCIA_BLOQUE  WITH (NOLOCK)  WHERE ESTADO=1 AND BLOQ<>1 GROUP BY INSTANCIA) M ON I.ID=M.I'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @NOM_TABLA ='' --AND (@ESTADO='8' OR @ESTADO='') " & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       --para las solicitudes que solo tienen registro en la tabla INSTANCIA y todav�a no las ha procesado el WebService" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + ' UNION ALL '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + ' SELECT I.ID AS ID,C.CODIGO,I.FEC_ALTA AS FECHA_ALTA,C.ESTADO CODESTADO,C.FEC_INI FECHA_INICIO,C.FEC_FIN FECHA_EXP,PRO.COD CODPRO,PRO.DEN PROVEEDOR,EMP.NIF CODEMP,  EMP.DEN EMPRESA,NULL AS ESTADO,PER.COD CODPET, PER.NOM + PER.APE AS PETICIONARIO, S.DEN_'+ @IDI + ' TIPO_CONTRATO, I.ULT_VERSION,I.ESTADO INSTANCIA_ESTADO,C.ID ID_CONTRATO,0 AS ICONO,0 AS OBV, 0 AS TRASLADADA,C.MON,CON.NOM + '' '' + CON.APE AS CONTACTO,I.IMPORTE,CI.PROCESO,I.EN_PROCESO,0 as ACCION_APROBAR,0 as ACCION_RECHAZAR,0 as BLOQUE '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + ' FROM CONTRATO C WITH(NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + ' LEFT JOIN (SELECT DISTINCT CONTRATO,ANYO,GMN1,PROCE,convert(NVARCHAR(50),ANYO) + ''/'' + GMN1 + ''/'' + convert(NVARCHAR(50),PROCE) AS PROCESO FROM CONTRATO_ITEM CI WITH(NOLOCK) ) CI ON CI.CONTRATO=C.ID'" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + ' INNER JOIN INSTANCIA I WITH(NOLOCK) ON I.ID=C.INSTANCIA '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + ' INNER JOIN PER P WITH(NOLOCK) ON P.COD = I.PETICIONARIO '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + ' INNER JOIN DEP D WITH (NOLOCK) ON P.DEP=D.COD '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + ' INNER JOIN SOLICITUD S WITH(NOLOCK) ON I.SOLICITUD=S.ID INNER JOIN TIPO_SOLICITUDES TS WITH (NOLOCK) ON S.TIPO=TS.ID'" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + ' LEFT JOIN PROVE PRO WITH (NOLOCK) ON PRO.COD = C.PROVE'" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=  @SQL + ' LEFT JOIN CON CON WITH (NOLOCK) ON CON.PROVE = C.PROVE AND C.CONTACTO=CON.ID'" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + ' INNER JOIN PER WITH (NOLOCK) ON PER.COD = I.PETICIONARIO'" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + ' LEFT JOIN EMP WITH (NOLOCK) ON C.EMP=EMP.ID'" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + ' LEFT JOIN INSTANCIA_VISORES_PM C1 WITH(NOLOCK) ON I.ID=C1.INSTANCIA'" & vbCrLf
sConsulta = sConsulta & "   IF @PALABRA_CLAVE IS NOT NULL AND @PALABRA_CLAVE <> '' " & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + ' INNER JOIN PM_COPIA_ADJUN PCA WITH(NOLOCK) ON C.ID = PCA.ID_CONTRATO'" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + @SWHERE + ' AND PETICIONARIO = @USU AND I.ULT_VERSION IS NULL AND I.NUM IS NULL AND EN_PROCESO=1 AND TS.TIPO =5'" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL2= N' INSERT INTO #G2 ' + @SQL" & vbCrLf
sConsulta = sConsulta & "SET @PARAM =N'@IDI NVARCHAR(20),@USU NVARCHAR (50),@CODIGO NVARCHAR(50)='''',@TIPO NVARCHAR(50),@FECHA_DESDE DATETIME=NULL,@FECHA_HASTA DATETIME=NULL,@MATERIAL NVARCHAR (50)='''',@ART NVARCHAR (50)='''',@EMP INT=0,@GMN1 NVARCHAR (50)='''',@COD_PROCE INT=0,@ANYO INT=0,@ESTADO NVARCHAR (50)='''',@FILAESTADO NVARCHAR (50)='''''" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL2" & vbCrLf
sConsulta = sConsulta & ", @PARAM" & vbCrLf
sConsulta = sConsulta & ",@CODIGO=@CODIGO,@TIPO=@TIPO,@FECHA_DESDE=@FECHA_DESDE,@FECHA_HASTA=@FECHA_HASTA,@MATERIAL=@MATERIAL,@ART=@ART,@EMP=@EMP,@GMN1=@GMN1,@COD_PROCE=@COD_PROCE,@ANYO=@ANYO,@ESTADO=@ESTADO,@FILAESTADO=@FILAESTADO,@USU=@USU,@IDI=@IDI  " & vbCrLf
sConsulta = sConsulta & "    " & vbCrLf
sConsulta = sConsulta & "-- *****LA SELECT FINAL ********" & vbCrLf
sConsulta = sConsulta & "set @SQL='SELECT DISTINCT #G2.ID,#G2.CODIGO, #G2.FEC_ALTA, #G2.CODESTADO, #G2.FECHA_INICIO, #G2.FECHA_EXP, #G2.CODPRO, #G2.PROVEEDOR, #G2.CODEMP, #G2.EMPRESA, #G2.ESTADO, #G2.CODPET, #G2.PETICIONARIO, #G2.TIPO_CONTRATO, #G2.ULT_VERSION, #G2.INSTANCIA_ESTADO, #G2.ID_CONTRATO, #G2.ICONO, #G2.OBSERVADOR, #G2.TRASLADADA, #G2.MON, #G2.CONTACTO, #G2.IMPORTE, #G2.PROCE, #G2.EN_PROCESO, #G2.ACCION_APROBAR, #G2.ACCION_RECHAZAR, #G2.BLOQUE '" & vbCrLf
sConsulta = sConsulta & "--CALIDAD: Cada tabla de filtro tiene unos campos diferentes. Hay que hacer la select con *." & vbCrLf
sConsulta = sConsulta & "IF @NOM_TABLA <>''" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + ', FILTRO.* '" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' FROM #G2'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ' LEFT JOIN INSTANCIA_ADICIONAL IA WITH(NOLOCK) ON #G2.ID=IA.INSTANCIA AND IA.PER=@USU '" & vbCrLf
sConsulta = sConsulta & "IF @NOM_TABLA<>''" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + ' INNER JOIN ' + @IDI + '_' + @NOM_TABLA + ' FILTRO WITH(NOLOCK) ON #G2.ID = FILTRO.INSTANCIA '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL_CONT=@SQL" & vbCrLf
sConsulta = sConsulta & "IF @NOM_TABLA <>''" & vbCrLf
sConsulta = sConsulta & "   SET @SQL_CONT = REPLACE(@SQL_CONT,', FILTRO.* ','')" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "IF @ESTADO<>'' AND @ESTADO<>'8' " & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       SET @ESTADO = REPLACE(@ESTADO,',',''',''')" & vbCrLf
sConsulta = sConsulta & "       SET @ESTADO = '''' + @ESTADO + ''''" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = @SQL + ' WHERE #G2.CODESTADO IN (' + @ESTADO + ')'" & vbCrLf
sConsulta = sConsulta & "   END " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ORDENACION IS NOT NULL AND @ORDENACION <> ''" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   IF SUBSTRING(@ORDENACION,1,2)='C_' --Es un campo del formulario" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = @SQL + ' ORDER BY FILTRO.' + @ORDENACION " & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "       --SET @SQL = @SQL + ' ORDER BY #G2.' + @ORDENACION " & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "       DECLARE @ROW INT = ((@PAGENUMBER-1) * @PAGESIZE) +1" & vbCrLf
sConsulta = sConsulta & "       IF @OBTENERTODOSCONTRATOS=0" & vbCrLf
sConsulta = sConsulta & "           BEGIN" & vbCrLf
sConsulta = sConsulta & "               --CALIDAD. Estos dos * se quedan pq no es la tabla de bbdd certificados sino una tabla hecha ad hoc mediante WITH CERTIFICADOS AS (...) y en estos puntos suspensivos puede venir filtro.* o no -> no se conocen las columnas" & vbCrLf
sConsulta = sConsulta & "               SET @SQL = ';WITH CONTRATOS AS (' + @SQL + '), TOTCONTRATOS AS (SELECT *,ROW_NUMBER() OVER (ORDER BY ' + @ORDENACION + ') AS ROWNUMBER FROM CONTRATOS WITH (NOLOCK)) SELECT *,ROWNUMBER FROM TOTCONTRATOS WITH (NOLOCK) WHERE ROWNUMBER BETWEEN ' + CONVERT(VARCHAR(10),@ROW) + ' AND ' + CONVERT(VARCHAR(10),(@ROW + @PAGESIZE - 1))" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "END " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL ,N'@USU NVARCHAR(50)',@USU=@USU" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @FILAESTADO <>''" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @SQL2= N'INSERT INTO #G3 ' + @SQL_CONT + ' WHERE #G2.CODESTADO IN (' + @FILAESTADO + ')'" & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL2,N'@USU NVARCHAR(50)',@USU=@USU" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @SQL2= N'INSERT INTO #G3 '  + @SQL_CONT" & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL2,N'@USU NVARCHAR(50)',@USU=@USU" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--***********************************************************************************************************************************" & vbCrLf
sConsulta = sConsulta & "--CONTADORES" & vbCrLf
sConsulta = sConsulta & "DECLARE @CONT0 INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @CONT1 INT --Borrador" & vbCrLf
sConsulta = sConsulta & "DECLARE @CONT2 INT --En Curso de Aprobaci�n" & vbCrLf
sConsulta = sConsulta & "DECLARE @CONT3 INT --Vigentes" & vbCrLf
sConsulta = sConsulta & "DECLARE @CONT4 INT --Expirados" & vbCrLf
sConsulta = sConsulta & "DECLARE @CONT5 INT --Rechazados" & vbCrLf
sConsulta = sConsulta & "DECLARE @CONT6 INT --Anulados" & vbCrLf
sConsulta = sConsulta & "DECLARE @CONT7 INT --Todas" & vbCrLf
sConsulta = sConsulta & "DECLARE @CONT8 INT --Proximas a expirar" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   --Borrador" & vbCrLf
sConsulta = sConsulta & "   SET @PARAM =N'@CONT0 INT = 0 OUTPUT '" & vbCrLf
sConsulta = sConsulta & "   SET @CONT1=0" & vbCrLf
sConsulta = sConsulta & "   IF CHARINDEX('0',@FILAESTADO,0)<>0 OR @FILAESTADO=''" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "      SET @SQL='SET @CONT0=(SELECT COUNT(DISTINCT C.ID) FROM #G3 C WITH (NOLOCK) WHERE CODESTADO=0)'" & vbCrLf
sConsulta = sConsulta & "      EXEC SP_EXECUTESQL @SQL" & vbCrLf
sConsulta = sConsulta & "      , @PARAM" & vbCrLf
sConsulta = sConsulta & "      ,@CONT0=@CONT1 OUTPUT  " & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "  " & vbCrLf
sConsulta = sConsulta & "   --En curso de aprobacion" & vbCrLf
sConsulta = sConsulta & "   SET @CONT2=0" & vbCrLf
sConsulta = sConsulta & "   IF CHARINDEX('2',@FILAESTADO,0)<>0 OR @FILAESTADO=''" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "      SET @SQL='SET @CONT0=(SELECT COUNT(DISTINCT C.ID) FROM #G3 C WITH (NOLOCK) WHERE CODESTADO=2)'" & vbCrLf
sConsulta = sConsulta & "      " & vbCrLf
sConsulta = sConsulta & "      EXEC SP_EXECUTESQL @SQL" & vbCrLf
sConsulta = sConsulta & "      , @PARAM" & vbCrLf
sConsulta = sConsulta & "      ,@CONT0=@CONT2 OUTPUT  " & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   --Proximos a expirar" & vbCrLf
sConsulta = sConsulta & "   SET @CONT8=0" & vbCrLf
sConsulta = sConsulta & "   IF CHARINDEX('4',@FILAESTADO,0)<>0 OR @FILAESTADO=''" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "     SET @SQL='SET @CONT0=(SELECT COUNT(DISTINCT C.ID) FROM #G3 C WITH (NOLOCK) WHERE CODESTADO=4)'" & vbCrLf
sConsulta = sConsulta & "     " & vbCrLf
sConsulta = sConsulta & "     EXEC SP_EXECUTESQL @SQL" & vbCrLf
sConsulta = sConsulta & "      , @PARAM" & vbCrLf
sConsulta = sConsulta & "      ,@CONT0=@CONT8 OUTPUT  " & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   --Vigentes" & vbCrLf
sConsulta = sConsulta & "   SET @CONT3=0" & vbCrLf
sConsulta = sConsulta & "   IF CHARINDEX('3',@FILAESTADO,0)<>0 OR @FILAESTADO=''" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "      SET @SQL='SET @CONT0=(SELECT COUNT(DISTINCT C.ID) FROM #G3 C WITH (NOLOCK) WHERE CODESTADO=3)'" & vbCrLf
sConsulta = sConsulta & "      " & vbCrLf
sConsulta = sConsulta & "      EXEC SP_EXECUTESQL @SQL" & vbCrLf
sConsulta = sConsulta & "      , @PARAM" & vbCrLf
sConsulta = sConsulta & "      ,@CONT0=@CONT3 OUTPUT  " & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   --Expirados" & vbCrLf
sConsulta = sConsulta & "   SET @CONT4=0" & vbCrLf
sConsulta = sConsulta & "   IF CHARINDEX('5',@FILAESTADO,0)<>0 OR @FILAESTADO=''" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "      SET @SQL='SET @CONT0=(SELECT COUNT(DISTINCT C.ID) FROM #G3 C WITH (NOLOCK) WHERE CODESTADO=5)'" & vbCrLf
sConsulta = sConsulta & "      " & vbCrLf
sConsulta = sConsulta & "      EXEC SP_EXECUTESQL @SQL" & vbCrLf
sConsulta = sConsulta & "      , @PARAM" & vbCrLf
sConsulta = sConsulta & "      ,@CONT0=@CONT4 OUTPUT  " & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   --Rechazados" & vbCrLf
sConsulta = sConsulta & "   SET @CONT5=0" & vbCrLf
sConsulta = sConsulta & "   IF CHARINDEX('6',@FILAESTADO,0)<>0 OR @FILAESTADO=''" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "      SET @SQL='SET @CONT0=(SELECT COUNT(DISTINCT C.ID) FROM #G3 C WITH (NOLOCK) WHERE CODESTADO=6)'" & vbCrLf
sConsulta = sConsulta & "      " & vbCrLf
sConsulta = sConsulta & "      EXEC SP_EXECUTESQL @SQL" & vbCrLf
sConsulta = sConsulta & "      , @PARAM" & vbCrLf
sConsulta = sConsulta & "      ,@CONT0=@CONT5 OUTPUT  " & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   --Anulados" & vbCrLf
sConsulta = sConsulta & "   SET @CONT6=0" & vbCrLf
sConsulta = sConsulta & "   IF CHARINDEX('7',@FILAESTADO,0)<>0 OR @FILAESTADO=''" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "      SET @SQL='SET @CONT0=(SELECT COUNT(DISTINCT C.ID) FROM #G3 C WITH (NOLOCK) WHERE CODESTADO=7)'" & vbCrLf
sConsulta = sConsulta & "      " & vbCrLf
sConsulta = sConsulta & "      EXEC SP_EXECUTESQL @SQL" & vbCrLf
sConsulta = sConsulta & "      , @PARAM" & vbCrLf
sConsulta = sConsulta & "      ,@CONT0=@CONT6 OUTPUT  " & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   --TODAS:   " & vbCrLf
sConsulta = sConsulta & "   SET @CONT7 =0" & vbCrLf
sConsulta = sConsulta & "   SET @CONT7 = @CONT1+@CONT2+@CONT3+@CONT4+@CONT5+@CONT6+@CONT8" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Muestra los contadores" & vbCrLf
sConsulta = sConsulta & "SELECT  @CONT1 GUARDADAS, @CONT2 EN_CURSO_APROBACION,@CONT3 VIGENTES,@CONT4 EXPIRADOS,@CONT5 RECHAZADOS,@CONT6 ANULADOS,@CONT7 TODAS, @CONT8 PROXIMO_A_EXPIRAR" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Contadores de Estados sin filtros (para mostrar las ALERTAS)" & vbCrLf
sConsulta = sConsulta & " SELECT COUNT(DISTINCT ID) PDTE_DE_TRATAR,@CONT8 PROXIMO_A_EXPIRAR FROM #G2 WITH (NOLOCK) WHERE ICONO = 1      " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DROP TABLE #T_ABIERTAS" & vbCrLf
sConsulta = sConsulta & "DROP TABLE #T_PARTICIPO" & vbCrLf
sConsulta = sConsulta & "DROP TABLE #T_PENDIENTES" & vbCrLf
sConsulta = sConsulta & "DROP TABLE #T_DEVOLUCION" & vbCrLf
sConsulta = sConsulta & "DROP TABLE #T_TRASLADADAS" & vbCrLf
sConsulta = sConsulta & "DROP TABLE #T_OTRAS" & vbCrLf
sConsulta = sConsulta & "DROP TABLE #G1" & vbCrLf
sConsulta = sConsulta & "DROP TABLE #G2" & vbCrLf
sConsulta = sConsulta & "DROP TABLE #G3" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Sub V_31900_8_Datos_098()
    Dim sConsulta As String
        
    sConsulta = "UPDATE CONF_VISTAS_ALL_PROVE SET VISIBLE=CASE V.OCULTAR_PROV_SIN_OFE WHEN 0 THEN VP.VISIBLE ELSE 2 END" & vbCrLf
    sConsulta = sConsulta & "FROM CONF_VISTAS_ALL_PROVE VP" & vbCrLf
    sConsulta = sConsulta & "      INNER JOIN CONF_VISTAS_ALL V WITH (NOLOCK) ON V.ANYO=VP.ANYO AND V.GMN1=VP.GMN1 AND V.PROCE=VP.PROCE " & vbCrLf
    sConsulta = sConsulta & "            AND V.USU=VP.USU AND V.VISTA=VP.VISTA " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
         
    sConsulta = "INSERT INTO CONF_VISTAS_ALL_PROVE (ANYO,GMN1,PROCE,PROVE,USU,VISTA,VISIBLE) " & vbCrLf
    sConsulta = sConsulta & "SELECT V.ANYO,V.GMN1,V.PROCE,PP.PROVE,V.USU,V.VISTA, CASE V.OCULTAR_PROV_SIN_OFE WHEN 0 THEN 1 ELSE 2 END" & vbCrLf
    sConsulta = sConsulta & "FROM CONF_VISTAS_ALL V WITH (NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "    INNER JOIN PROCE_PROVE PP WITH (NOLOCK) ON V.ANYO=PP.ANYO AND V.GMN1=PP.GMN1 AND V.PROCE=PP.PROCE " & vbCrLf
    sConsulta = sConsulta & "WHERE NOT EXISTS (SELECT ANYO " & vbCrLf
    sConsulta = sConsulta & "                          FROM CONF_VISTAS_ALL_PROVE VP WITH (NOLOCK) " & vbCrLf
    sConsulta = sConsulta & "                          WHERE V.ANYO=VP.ANYO AND V.GMN1=VP.GMN1 AND V.PROCE=VP.PROCE AND V.USU=VP.USU" & vbCrLf
    sConsulta = sConsulta & "                                   AND V.VISTA=VP.VISTA AND PP.PROVE=VP.PROVE)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
         
    sConsulta = "UPDATE CONF_VISTAS_PROCE_PROVE SET VISIBLE=CASE V.OCULTAR_PROV_SIN_OFE WHEN 0 THEN VP.VISIBLE ELSE 2 END" & vbCrLf
    sConsulta = sConsulta & "FROM CONF_VISTAS_PROCE_PROVE VP" & vbCrLf
    sConsulta = sConsulta & "      INNER JOIN CONF_VISTAS_PROCE V WITH (NOLOCK) ON V.ANYO=VP.ANYO AND V.GMN1=VP.GMN1 AND V.PROCE=VP.PROCE " & vbCrLf
    sConsulta = sConsulta & "            AND V.USU=VP.USU AND V.VISTA=VP.VISTA " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
     
    sConsulta = "INSERT INTO CONF_VISTAS_PROCE_PROVE (ANYO,GMN1,PROCE,PROVE,USU,VISTA,VISIBLE) " & vbCrLf
    sConsulta = sConsulta & "SELECT V.ANYO,V.GMN1,V.PROCE,PP.PROVE,V.USU,V.VISTA, CASE V.OCULTAR_PROV_SIN_OFE WHEN 0 THEN 1 ELSE 2 END" & vbCrLf
    sConsulta = sConsulta & "FROM CONF_VISTAS_PROCE V WITH (NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "    INNER JOIN PROCE_PROVE PP WITH (NOLOCK) ON V.ANYO=PP.ANYO AND V.GMN1=PP.GMN1 AND V.PROCE=PP.PROCE " & vbCrLf
    sConsulta = sConsulta & "WHERE NOT EXISTS (SELECT ANYO " & vbCrLf
    sConsulta = sConsulta & "                          FROM CONF_VISTAS_PROCE_PROVE VP WITH (NOLOCK) " & vbCrLf
    sConsulta = sConsulta & "                          WHERE V.ANYO=VP.ANYO AND V.GMN1=VP.GMN1 AND V.PROCE=VP.PROCE AND V.USU=VP.USU" & vbCrLf
    sConsulta = sConsulta & "                                   AND V.VISTA=VP.VISTA AND PP.PROVE=VP.PROVE)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
        
    sConsulta = "UPDATE CONF_VISTAS_GRUPO_PROVE SET VISIBLE=CASE V.OCULTAR_PROV_SIN_OFE WHEN 0 THEN VP.VISIBLE ELSE 2 END" & vbCrLf
    sConsulta = sConsulta & "FROM CONF_VISTAS_GRUPO_PROVE VP" & vbCrLf
    sConsulta = sConsulta & "      INNER JOIN CONF_VISTAS_GRUPO V WITH (NOLOCK) ON V.ANYO=VP.ANYO AND V.GMN1=VP.GMN1 AND V.PROCE=VP.PROCE " & vbCrLf
    sConsulta = sConsulta & "            AND V.GRUPO=VP.GRUPO AND V.USU=VP.USU AND V.VISTA=VP.VISTA " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
     
    sConsulta = "INSERT INTO CONF_VISTAS_GRUPO_PROVE (ANYO,GMN1,PROCE,GRUPO,PROVE,USU,VISTA,VISIBLE) " & vbCrLf
    sConsulta = sConsulta & "SELECT V.ANYO,V.GMN1,V.PROCE,V.GRUPO,PP.PROVE,V.USU,V.VISTA, CASE V.OCULTAR_PROV_SIN_OFE WHEN 0 THEN 1 ELSE 2 END" & vbCrLf
    sConsulta = sConsulta & "FROM CONF_VISTAS_GRUPO V WITH (NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "    INNER JOIN PROCE_PROVE PP WITH (NOLOCK) ON V.ANYO=PP.ANYO AND V.GMN1=PP.GMN1 AND V.PROCE=PP.PROCE " & vbCrLf
    sConsulta = sConsulta & "WHERE NOT EXISTS (SELECT ANYO " & vbCrLf
    sConsulta = sConsulta & "                          FROM CONF_VISTAS_GRUPO_PROVE VP WITH (NOLOCK) " & vbCrLf
    sConsulta = sConsulta & "                          WHERE V.ANYO=VP.ANYO AND V.GMN1=VP.GMN1 AND V.PROCE=VP.PROCE AND V.GRUPO=VP.GRUPO " & vbCrLf
    sConsulta = sConsulta & "                                   AND V.USU=VP.USU AND V.VISTA=VP.VISTA AND PP.PROVE=VP.PROVE)" & vbCrLf
    sConsulta = sConsulta & "                                   " & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
     
    sConsulta = "UPDATE CONF_VISTAS_ITEM_PROVE SET VISIBLE=CASE V.OCULTAR_PROV_SIN_OFE WHEN 0 THEN VP.VISIBLE ELSE 2 END" & vbCrLf
    sConsulta = sConsulta & "FROM CONF_VISTAS_ITEM_PROVE VP" & vbCrLf
    sConsulta = sConsulta & "      INNER JOIN CONF_VISTAS_ITEM V WITH (NOLOCK) ON V.ANYO=VP.ANYO AND V.GMN1=VP.GMN1 AND V.PROCE=VP.PROCE " & vbCrLf
    sConsulta = sConsulta & "            AND V.USU=VP.USU AND V.VISTA=VP.VISTA " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
     
    sConsulta = sConsulta & "INSERT INTO CONF_VISTAS_ITEM_PROVE (ANYO,GMN1,PROCE,GRUPO,PROVE,USU,VISTA,VISIBLE) " & vbCrLf
    sConsulta = sConsulta & "SELECT V.ANYO,V.GMN1,V.PROCE,V.GRUPO,PP.PROVE,V.USU,V.VISTA, CASE V.OCULTAR_PROV_SIN_OFE WHEN 0 THEN 1 ELSE 2 END" & vbCrLf
    sConsulta = sConsulta & "FROM CONF_VISTAS_ITEM V WITH (NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "    INNER JOIN PROCE_PROVE PP WITH (NOLOCK) ON V.ANYO=PP.ANYO AND V.GMN1=PP.GMN1 AND V.PROCE=PP.PROCE " & vbCrLf
    sConsulta = sConsulta & "WHERE NOT EXISTS (SELECT ANYO " & vbCrLf
    sConsulta = sConsulta & "                          FROM CONF_VISTAS_ITEM_PROVE VP WITH (NOLOCK) " & vbCrLf
    sConsulta = sConsulta & "                          WHERE V.ANYO=VP.ANYO AND V.GMN1=VP.GMN1 AND V.PROCE=VP.PROCE AND V.GRUPO=VP.GRUPO " & vbCrLf
    sConsulta = sConsulta & "                                   AND V.USU=VP.USU AND V.VISTA=VP.VISTA AND PP.PROVE=VP.PROVE)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

End Sub


Public Function CodigoDeActualizacion31900_08_1898_A31900_08_1899() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim oADOConnection As ADODB.Connection
    Dim sConsulta As String

    On Error GoTo Error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    
    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Actualiza Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    frmProgreso.ProgressBar1.Refresh
    V_31900_8_Storeds_099
    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Actualiza Datos"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    frmProgreso.ProgressBar1.Refresh
    V_31900_8_Datos_099
    
    sConsulta = "UPDATE VERSION SET NUM ='3.00.18.99'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103), PRODVER='31900.8'"
    ExecuteSQL gRDOCon, sConsulta
        
    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False
        
    CodigoDeActualizacion31900_08_1898_A31900_08_1899 = True
    Exit Function

Error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    CodigoDeActualizacion31900_08_1898_A31900_08_1899 = False
End Function

Public Sub V_31900_8_Storeds_099()
Dim sConsulta As String
        
sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_LEER_ADJUNTO]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_LEER_ADJUNTO]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_LEER_ADJUNTO]" & vbCrLf
sConsulta = sConsulta & "   @ID int = NULL,@TIPO INT=NULL" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "BEGIN  " & vbCrLf
sConsulta = sConsulta & "   SET NOCOUNT ON;" & vbCrLf
sConsulta = sConsulta & "    IF @TIPO IS NULL OR @TIPO=0" & vbCrLf
sConsulta = sConsulta & "       SELECT DATA.PathName() AS PATHNAME, GET_FILESTREAM_TRANSACTION_CONTEXT () AS TRANSACTIONCONTEXT" & vbCrLf
sConsulta = sConsulta & "       FROM   PM_COPIA_ADJUN WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       WHERE  ID=@ID " & vbCrLf
sConsulta = sConsulta & "   IF @TIPO=1" & vbCrLf
sConsulta = sConsulta & "       SELECT DATA.PathName() AS PATHNAME, GET_FILESTREAM_TRANSACTION_CONTEXT () AS TRANSACTIONCONTEXT" & vbCrLf
sConsulta = sConsulta & "       FROM   SOLICITUD_ADJUN WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       WHERE  ID=@ID  " & vbCrLf
sConsulta = sConsulta & "   IF @TIPO=2" & vbCrLf
sConsulta = sConsulta & "       SELECT DATA.PathName() AS PATHNAME, GET_FILESTREAM_TRANSACTION_CONTEXT () AS TRANSACTIONCONTEXT" & vbCrLf
sConsulta = sConsulta & "       FROM   CAMPO_ADJUN WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       WHERE  ID=@ID   " & vbCrLf
sConsulta = sConsulta & "   IF @TIPO=3" & vbCrLf
sConsulta = sConsulta & "       SELECT DATA.PathName() AS PATHNAME, GET_FILESTREAM_TRANSACTION_CONTEXT () AS TRANSACTIONCONTEXT" & vbCrLf
sConsulta = sConsulta & "       FROM   LINEA_DESGLOSE_ADJUN WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       WHERE  ID=@ID" & vbCrLf
sConsulta = sConsulta & "   IF @TIPO=4" & vbCrLf
sConsulta = sConsulta & "       SELECT DATA.PathName() AS PATHNAME, GET_FILESTREAM_TRANSACTION_CONTEXT () AS TRANSACTIONCONTEXT" & vbCrLf
sConsulta = sConsulta & "       FROM   PM_COPIA_ADJUN PCA WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN COPIA_CAMPO_ADJUN CCA WITH (NOLOCK)ON CCA.ADJUN=PCA.ID" & vbCrLf
sConsulta = sConsulta & "       WHERE  CCA.ID=@ID   " & vbCrLf
sConsulta = sConsulta & "   IF @TIPO=6" & vbCrLf
sConsulta = sConsulta & "       SELECT DATA.PathName() AS PATHNAME, GET_FILESTREAM_TRANSACTION_CONTEXT () AS TRANSACTIONCONTEXT" & vbCrLf
sConsulta = sConsulta & "       FROM   PM_COPIA_ADJUN PCA WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN COPIA_LINEA_DESGLOSE_ADJUN CLDA WITH (NOLOCK)ON CLDA.ADJUN=PCA.ID" & vbCrLf
sConsulta = sConsulta & "       WHERE  CLDA.ID=@ID   " & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_SAVE_ADJUN]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_SAVE_ADJUN]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_SAVE_ADJUN] @ID_ADJUN INT OUTPUT,@PER NVARCHAR(50)=NULL,@PROVE NVARCHAR(50),@NOM NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & ", @ID_CONTRATO INT=NULL, @COMENT NVARCHAR(4000), @SOLICITUD INT=NULL, @INSTANCIA INT=NULL, @CAMPO INT=NULL, @LINEA INT=NULL" & vbCrLf
sConsulta = sConsulta & ", @CAMPOPADRE INT=NULL, @CAMPOHIJO INT=NULL,@DATASIZE INT=NULL,@IDIOMA NVARCHAR(10)='',@FECHA DATETIME=NULL" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @SOLICITUD>0 " & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           INSERT INTO SOLICITUD_ADJUN (DATA,FECALTA,SOLICITUD,NOM,DATASIZE,PER, COMENT,IDIOMA)" & vbCrLf
sConsulta = sConsulta & "                       VALUES ((0x),@FECHA ,@SOLICITUD ,@NOM,@DATASIZE,@PER,@COMENT,@IDIOMA ) " & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           IF @INSTANCIA=1 " & vbCrLf
sConsulta = sConsulta & "           BEGIN" & vbCrLf
sConsulta = sConsulta & "               IF @CAMPO >0" & vbCrLf
sConsulta = sConsulta & "                   BEGIN" & vbCrLf
sConsulta = sConsulta & "                       INSERT INTO PM_COPIA_ADJUN (DATA,FECALTA,NOM,PER,PROVE, ID_CONTRATO, COMENT)" & vbCrLf
sConsulta = sConsulta & "                               VALUES ((0x),GETDATE(),@NOM,@PER,@PROVE,@ID_CONTRATO,@COMENT)     " & vbCrLf
sConsulta = sConsulta & "                       " & vbCrLf
sConsulta = sConsulta & "                       SET @ID_ADJUN=@@IDENTITY" & vbCrLf
sConsulta = sConsulta & "                       " & vbCrLf
sConsulta = sConsulta & "                       INSERT INTO COPIA_CAMPO_ADJUN(ADJUN,CAMPO,NOM,IDIOMA,DATASIZE,PER,COMENT,FECALTA)" & vbCrLf
sConsulta = sConsulta & "                           VALUES( @ID_ADJUN,@CAMPO,@NOM,@IDIOMA,@DATASIZE,@PER,@COMENT,GETDATE())" & vbCrLf
sConsulta = sConsulta & "                           " & vbCrLf
sConsulta = sConsulta & "                       " & vbCrLf
sConsulta = sConsulta & "                   END " & vbCrLf
sConsulta = sConsulta & "               ELSE" & vbCrLf
sConsulta = sConsulta & "                   BEGIN" & vbCrLf
sConsulta = sConsulta & "                       INSERT INTO PM_COPIA_ADJUN (DATA,FECALTA,NOM,PER,PROVE, ID_CONTRATO, COMENT)" & vbCrLf
sConsulta = sConsulta & "                               VALUES ((0x),GETDATE(),@NOM,@PER,@PROVE,@ID_CONTRATO,@COMENT)     " & vbCrLf
sConsulta = sConsulta & "                       " & vbCrLf
sConsulta = sConsulta & "                       SET @ID_ADJUN=@@IDENTITY" & vbCrLf
sConsulta = sConsulta & "                       " & vbCrLf
sConsulta = sConsulta & "                       INSERT INTO COPIA_LINEA_DESGLOSE_ADJUN(ADJUN,CAMPO_PADRE,CAMPO_HIJO,LINEA,NOM,IDIOMA,DATASIZE,PER,COMENT,FECALTA)" & vbCrLf
sConsulta = sConsulta & "                           VALUES( @ID_ADJUN,@CAMPOPADRE,@CAMPOHIJO,@LINEA,@NOM,@IDIOMA,@DATASIZE,@PER,@COMENT,GETDATE())" & vbCrLf
sConsulta = sConsulta & "                           " & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "           END " & vbCrLf
sConsulta = sConsulta & "           ELSE" & vbCrLf
sConsulta = sConsulta & "               BEGIN" & vbCrLf
sConsulta = sConsulta & "                   IF @CAMPO >0" & vbCrLf
sConsulta = sConsulta & "                   BEGIN" & vbCrLf
sConsulta = sConsulta & "                       INSERT INTO CAMPO_ADJUN (DATA,FECALTA,CAMPO,NOM,DATASIZE,PER, COMENT,IDIOMA)" & vbCrLf
sConsulta = sConsulta & "                           VALUES ((0x),@FECHA,@CAMPO,@NOM,@DATASIZE,@PER,@COMENT,@IDIOMA  ) " & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "                   ELSE" & vbCrLf
sConsulta = sConsulta & "                       IF @LINEA>0" & vbCrLf
sConsulta = sConsulta & "                       BEGIN" & vbCrLf
sConsulta = sConsulta & "                           INSERT INTO LINEA_DESGLOSE_ADJUN (DATA,FECALTA,CAMPO_PADRE,CAMPO_HIJO,LINEA,NOM,DATASIZE, IDIOMA, COMENT)" & vbCrLf
sConsulta = sConsulta & "                           VALUES ((0x),@FECHA,@CAMPOPADRE,@CAMPOHIJO,@LINEA,@NOM,@DATASIZE,@IDIOMA,@COMENT )     " & vbCrLf
sConsulta = sConsulta & "                       END" & vbCrLf
sConsulta = sConsulta & "                       ELSE" & vbCrLf
sConsulta = sConsulta & "                       BEGIN" & vbCrLf
sConsulta = sConsulta & "                           INSERT INTO PM_COPIA_ADJUN (DATA,FECALTA,NOM,PER,PROVE, ID_CONTRATO, COMENT)" & vbCrLf
sConsulta = sConsulta & "                           VALUES ((0x),GETDATE(),@NOM,@PER,@PROVE,@ID_CONTRATO,@COMENT)     " & vbCrLf
sConsulta = sConsulta & "                       END" & vbCrLf
sConsulta = sConsulta & "                       " & vbCrLf
sConsulta = sConsulta & "               END " & vbCrLf
sConsulta = sConsulta & "       END " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "       SET @ID_ADJUN=@@IDENTITY" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_UPDATE_ADJUN]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_UPDATE_ADJUN]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_UPDATE_ADJUN] @ID INT,@NOM NVARCHAR(200)='', @COMENT NVARCHAR(800)='', @DATASIZE INT = 0,@TIPO INT=NULL AS " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "IF @TIPO = 1" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    UPDATE SOLICITUD_ADJUN SET DATA=CAST('' AS VARBINARY(MAX)),COMENT=@COMENT,DATASIZE=@DATASIZE,NOM=@NOM  WHERE ID=@ID " & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "IF @TIPO = 2" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    UPDATE CAMPO_ADJUN SET DATA=CAST('' AS VARBINARY(MAX)),COMENT=@COMENT,DATASIZE=@DATASIZE,NOM=@NOM   WHERE ID=@ID " & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "IF @TIPO =4 OR @TIPO IS NULL" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    UPDATE PM_COPIA_ADJUN SET DATA=CAST('' AS VARBINARY(MAX)),COMENT=@COMENT,NOM=@NOM " & vbCrLf
sConsulta = sConsulta & "    FROM PM_COPIA_ADJUN PCA WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN COPIA_CAMPO_ADJUN CCA WITH (NOLOCK) ON CCA.ADJUN=PCA.ID  WHERE CCA.ID=@ID " & vbCrLf
sConsulta = sConsulta & "    " & vbCrLf
sConsulta = sConsulta & "    UPDATE COPIA_CAMPO_ADJUN SET NOM=@NOM WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "IF @TIPO = 3" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "   UPDATE LINEA_DESGLOSE_ADJUN SET DATA=CAST('' AS VARBINARY(MAX)),COMENT=@COMENT,DATASIZE=@DATASIZE,NOM=@NOM  WHERE ID=@ID " & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @TIPO =6 --adjunto de desglose de una instancia que se sustituye desde GS" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    UPDATE PM_COPIA_ADJUN SET DATA=CAST('' AS VARBINARY(MAX)),COMENT=@COMENT,NOM=@NOM " & vbCrLf
sConsulta = sConsulta & "    FROM PM_COPIA_ADJUN PCA WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN COPIA_LINEA_DESGLOSE_ADJUN CLDA WITH (NOLOCK) ON CLDA.ADJUN=PCA.ID  WHERE CLDA.ID=@ID " & vbCrLf
sConsulta = sConsulta & "    " & vbCrLf
sConsulta = sConsulta & "    UPDATE COPIA_LINEA_DESGLOSE_ADJUN SET NOM=@NOM WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "  END  " & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSWS_GETINSTADJUNDATA]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSWS_GETINSTADJUNDATA]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSWS_GETINSTADJUNDATA] @ID INT, @TIPO INT = 0  AS " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @TIPO = 1" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "   SELECT DATA.PathName()  FROM PM_COPIA_ADJUN CA WITH (NOLOCK) INNER JOIN COPIA_CAMPO_ADJUN  CCA WITH (NOLOCK)  ON CCA.ADJUN = CA.ID WHERE CCA.ID=@ID " & vbCrLf
sConsulta = sConsulta & "      " & vbCrLf
sConsulta = sConsulta & "    " & vbCrLf
sConsulta = sConsulta & "   IF @TIPO = 2 OR @TIPO=4" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "   SELECT DATA.PathName()  FROM PM_COPIA_ADJUN WITH (NOLOCK)  WHERE ID=@ID " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   IF @TIPO = 3 OR @TIPO=6 --(Tipo 6 para cuando lee el adjunto desde GS)" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   SELECT DATA.PathName()  FROM PM_COPIA_ADJUN CA WITH (NOLOCK) INNER JOIN COPIA_LINEA_DESGLOSE_ADJUN CLDA WITH (NOLOCK) ON CLDA.ADJUN = CA.ID  WHERE CLDA.ID=@ID " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @TIPO =5" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "   SELECT DATA.PathName()  FROM LINEA_DESGLOSE_ADJUN WITH (NOLOCK)  WHERE ID=@ID " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


        
sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_CONTRATOS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_CONTRATOS]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_CONTRATOS] " & vbCrLf
sConsulta = sConsulta & "   @USU NVARCHAR(50)," & vbCrLf
sConsulta = sConsulta & "   @IDI NVARCHAR(3)," & vbCrLf
sConsulta = sConsulta & "   @CODIGO NVARCHAR(50)='', " & vbCrLf
sConsulta = sConsulta & "   @TIPO NVARCHAR(50)=''," & vbCrLf
sConsulta = sConsulta & "   @TITULO NVARCHAR(4000)=''," & vbCrLf
sConsulta = sConsulta & "   @PROVE NVARCHAR(200)=NULL," & vbCrLf
sConsulta = sConsulta & "   @PETICIONARIO NVARCHAR(200)=NULL," & vbCrLf
sConsulta = sConsulta & "   @FECHA_DESDE DATETIME=NULL, " & vbCrLf
sConsulta = sConsulta & "   @FECHA_HASTA DATETIME=NULL," & vbCrLf
sConsulta = sConsulta & "   @MATERIAL NVARCHAR (50)='', " & vbCrLf
sConsulta = sConsulta & "   @ART NVARCHAR(50)='', " & vbCrLf
sConsulta = sConsulta & "   @EMP INT=0, " & vbCrLf
sConsulta = sConsulta & "   @ANYO INT=0, " & vbCrLf
sConsulta = sConsulta & "   @GMN1 NVARCHAR (50)='', " & vbCrLf
sConsulta = sConsulta & "   @COD_PROCE INT=0, " & vbCrLf
sConsulta = sConsulta & "   @ESTADO  NVARCHAR (50)='', " & vbCrLf
sConsulta = sConsulta & "   @FILAESTADO NVARCHAR (50), " & vbCrLf
sConsulta = sConsulta & "   @NOM_TABLA NVARCHAR(50)=''," & vbCrLf
sConsulta = sConsulta & "   @ORDENACION NVARCHAR(50)=''," & vbCrLf
sConsulta = sConsulta & "   @PALABRA_CLAVE NVARCHAR(200)='', " & vbCrLf
sConsulta = sConsulta & "   @PAGENUMBER INT=1, " & vbCrLf
sConsulta = sConsulta & "   @PAGESIZE INT=1000, " & vbCrLf
sConsulta = sConsulta & "   @OBTENERTODOSCONTRATOS TINYINT=0," & vbCrLf
sConsulta = sConsulta & "   @CENTROCOSTE VARCHAR(50) = NULL," & vbCrLf
sConsulta = sConsulta & "   @PARTIDASPRES TBCADENATEXTO2 READONLY" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @SWHERE AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL_CONT AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL2 AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLFROM AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLFROM_EST AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PARAM NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SUSTITUTO AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @LONGGMN1 INT = 0" & vbCrLf
sConsulta = sConsulta & "DECLARE @LONGGMN2 INT = 0" & vbCrLf
sConsulta = sConsulta & "DECLARE @LONGGMN3 INT = 0" & vbCrLf
sConsulta = sConsulta & "DECLARE @LONGGMN4 INT = 0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @LONGGMN1 = LONGITUD FROM DIC WITH(NOLOCK) WHERE NOMBRE='GMN1'" & vbCrLf
sConsulta = sConsulta & "SELECT @LONGGMN2 = LONGITUD FROM DIC WITH(NOLOCK) WHERE NOMBRE='GMN2'" & vbCrLf
sConsulta = sConsulta & "SELECT @LONGGMN3 = LONGITUD FROM DIC WITH(NOLOCK) WHERE NOMBRE='GMN3'" & vbCrLf
sConsulta = sConsulta & "SELECT @LONGGMN4 = LONGITUD FROM DIC WITH(NOLOCK) WHERE NOMBRE='GMN4'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @FECHA_HASTA IS NOT NULL --para incluir los del mismo dia" & vbCrLf
sConsulta = sConsulta & "   SET @FECHA_HASTA =DATEADD(DD, 1, @FECHA_HASTA)" & vbCrLf
sConsulta = sConsulta & "SET @SWHERE = ''" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Tablas basicas para las consultas " & vbCrLf
sConsulta = sConsulta & "SET @SQLFROM = N' FROM CONTRATO C WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN INSTANCIA I WITH (NOLOCK) ON I.ID = C.INSTANCIA" & vbCrLf
sConsulta = sConsulta & "INNER JOIN SOLICITUD S WITH (NOLOCK) ON I.SOLICITUD=S.ID  " & vbCrLf
sConsulta = sConsulta & "INNER JOIN TIPO_SOLICITUDES TS WITH (NOLOCK) ON S.TIPO=TS.ID " & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PROVE PRO WITH (NOLOCK) ON PRO.COD = C.PROVE" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PER WITH (NOLOCK) ON PER.COD = I.PETICIONARIO" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN EMP WITH (NOLOCK) ON C.EMP=EMP.ID '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLFROM_EST = N' FROM CONTRATO C WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN INSTANCIA I WITH (NOLOCK)  ON I.ID = C.INSTANCIA" & vbCrLf
sConsulta = sConsulta & "INNER JOIN SOLICITUD S WITH (NOLOCK) ON I.SOLICITUD=S.ID  " & vbCrLf
sConsulta = sConsulta & "INNER JOIN TIPO_SOLICITUDES TS WITH (NOLOCK) ON S.TIPO=TS.ID '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SWHERE =' WHERE TS.TIPO=5 '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Condiciones where" & vbCrLf
sConsulta = sConsulta & "IF @CODIGO IS NOT NULL and @CODIGO <> ''" & vbCrLf
sConsulta = sConsulta & "    SET @SWHERE = @SWHERE +  ' AND C.CODIGO LIKE ''%' + @CODIGO + '%'''" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @TIPO IS NOT NULL and @TIPO<>''" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "    SET @TIPO = REPLACE(@TIPO,'$$$',',')" & vbCrLf
sConsulta = sConsulta & "    SET @SWHERE = @SWHERE + ' AND S.ID IN (' + @TIPO +')'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @TITULO IS NOT NULL AND @TITULO <> ''" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @SQLFROM = @SQLFROM + ' INNER JOIN INSTANCIA_VISORES_PM C1 WITH(NOLOCK) ON I.ID=C1.INSTANCIA'" & vbCrLf
sConsulta = sConsulta & "   SET @SWHERE = @SWHERE + ' AND (C1.TITULO_TEXT LIKE ''%' + @TITULO+ '%'' OR CONVERT(VARCHAR,TITULO_FEC) LIKE ''%' + @TITULO + '%'')'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @PROVE IS NOT NULL AND @PROVE <> ''" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "    SET @PROVE = REPLACE(@PROVE,'$$$',''',''')" & vbCrLf
sConsulta = sConsulta & "    SET @PROVE = '''' + @PROVE + ''''" & vbCrLf
sConsulta = sConsulta & "    SET @SWHERE = @SWHERE + ' AND PRO.COD IN (' + @PROVE + ')'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @PETICIONARIO IS NOT NULL AND @PETICIONARIO  <> ''" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "    SET @PETICIONARIO = REPLACE(@PETICIONARIO,'$$$',''',''')" & vbCrLf
sConsulta = sConsulta & "    SET @PETICIONARIO = '''' + @PETICIONARIO + ''''" & vbCrLf
sConsulta = sConsulta & "    SET @SWHERE = @SWHERE + ' AND PER.COD IN (' +@PETICIONARIO+')'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ESTADO IS NOT NULL AND @ESTADO<>''" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @ESTADO = REPLACE(@ESTADO,'$$$',',')" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @FILAESTADO IS NOT NULL AND @FILAESTADO  <>''" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @FILAESTADO = REPLACE(@FILAESTADO,'$$$',',')" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @FECHA_DESDE IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "      SET @SWHERE =@SWHERE + ' AND C.FEC_INI>= @FECHA_DESDE'" & vbCrLf
sConsulta = sConsulta & "      " & vbCrLf
sConsulta = sConsulta & "IF @FECHA_HASTA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "      SET @SWHERE = @SWHERE + ' AND C.FEC_FIN <= @FECHA_HASTA'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @EMP IS NOT NULL AND @EMP<>0 " & vbCrLf
sConsulta = sConsulta & "   SET @SWHERE = @SWHERE + ' AND EMP.ID = @EMP'" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "IF (@ANYO<>0 AND @GMN1<>'' AND @COD_PROCE<>0) OR (@MATERIAL IS NOT NULL AND @MATERIAL <>'') OR (@ART IS NOT NULL AND @ART <>'')" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @SQLFROM=@SQLFROM + ' INNER JOIN CONTRATO_ITEM CI WITH (NOLOCK) ON C.ID=CI.CONTRATO'" & vbCrLf
sConsulta = sConsulta & "   IF (@ANYO<>0 AND @GMN1<>'' AND @COD_PROCE<>0)" & vbCrLf
sConsulta = sConsulta & "       SET @SWHERE = @SWHERE + ' AND CI.GMN1=@GMN1 AND CI.PROCE=@COD_PROCE AND CI.ANYO=@ANYO'" & vbCrLf
sConsulta = sConsulta & "   DECLARE @HAYITEM BIT = 0" & vbCrLf
sConsulta = sConsulta & "   IF (@MATERIAL IS NOT NULL AND @MATERIAL <>'')" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "       --CUANDO SE BUSCA UN MATERIAL HAY QUE MIRAR EL MATERIAL (LOS GMN) DE LOS ITEMS ASOCIADOS AL CONTRATO (O SEA, LOS ITEMS QUE HAYA EN CONTRATO_ITEM PARA EL CONTRATO, QUE A SU VEZ ENLAZAN A LA TABLA ITEM: CONTRATO->CONTRATO_ITEM->ITEM)" & vbCrLf
sConsulta = sConsulta & "       --  EN ITEM, SI EL CAMPO ART VIENE INFORMADO, HAY QUE BUSCAR SU MATERIAL EN LA TABLA ART4 (RELACION:ART4.COD = ITEM.ART)" & vbCrLf
sConsulta = sConsulta & "       --  EN ITEM, SI EL CAMPO ART NO VIENE INFORMADO, LOS GMN DEL MATERIAL ESTAR�N DEFINIDOS DIRECTAMENTE EN LA TABLA ITEM." & vbCrLf
sConsulta = sConsulta & "       SET @SQLFROM = @SQLFROM + ' INNER JOIN ITEM IT WITH(NOLOCK) ON IT.ANYO = CI.ANYO AND IT.GMN1_PROCE = CI.GMN1 AND IT.PROCE = CI.PROCE AND IT.ID=CI.ITEM AND CASE WHEN ART IS NULL THEN (SPACE(@LONGGMN1-LEN(IT.GMN1)) + COALESCE(IT.GMN1,'''') + SPACE(@LONGGMN2-LEN(IT.GMN2)) + COALESCE(IT.GMN2,'''') + SPACE(@LONGGMN3-LEN(IT.GMN3)) + COALESCE(IT.GMN3,'''') + SPACE(@LONGGMN4-LEN(IT.GMN4)) + COALESCE(IT.GMN4,'''')) ELSE @MATERIAL END = @MATERIAL" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN ART4 A4 WITH(NOLOCK) ON A4.COD = IT.ART AND CASE WHEN ART IS NULL THEN @MATERIAL ELSE (SPACE(@LONGGMN1-LEN(A4.GMN1)) + COALESCE(A4.GMN1,'''') + SPACE(@LONGGMN2-LEN(A4.GMN2)) + COALESCE(A4.GMN2,'''') + SPACE(@LONGGMN3-LEN(A4.GMN3)) + COALESCE(A4.GMN3,'''') + SPACE(@LONGGMN4-LEN(A4.GMN4)) + COALESCE(A4.GMN4,'''')) END = @MATERIAL'" & vbCrLf
sConsulta = sConsulta & "       SET @HAYITEM = 1" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "   IF (@ART IS NOT NULL AND @ART <>'')" & vbCrLf
sConsulta = sConsulta & "       IF @HAYITEM = 0" & vbCrLf
sConsulta = sConsulta & "           SET @SQLFROM = @SQLFROM + ' INNER JOIN ITEM IT WITH(NOLOCK) ON IT.ANYO = CI.ANYO AND IT.GMN1_PROCE = CI.GMN1 AND IT.PROCE = CI.PROCE AND IT.ID=CI.ITEM " & vbCrLf
sConsulta = sConsulta & "           AND IT.ART = @ART '" & vbCrLf
sConsulta = sConsulta & "       ELSE" & vbCrLf
sConsulta = sConsulta & "           SET @SQLFROM = @SQLFROM + ' AND IT.ART = @ART '" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "IF @NOM_TABLA IS NOT NULL AND @NOM_TABLA <>''" & vbCrLf
sConsulta = sConsulta & "   SET @SQLFROM = @SQLFROM + ' INNER JOIN ' + @IDI + '_' + @NOM_TABLA + ' F WITH (NOLOCK) ON I.ID = F.INSTANCIA'" & vbCrLf
sConsulta = sConsulta & "        " & vbCrLf
sConsulta = sConsulta & "IF @PALABRA_CLAVE IS NOT NULL AND @PALABRA_CLAVE <> ''" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @SQLFROM = @SQLFROM + ' INNER JOIN PM_COPIA_ADJUN PCA WITH(NOLOCK) ON C.ID=PCA.ID_CONTRATO'" & vbCrLf
sConsulta = sConsulta & "   SET @SWHERE = @SWHERE + ' AND PCA.DATA LIKE CAST(''%' + @PALABRA_CLAVE + '%'' as varbinary(max))'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "IF (SELECT COUNT(1) FROM USU U WITH (NOLOCK) WHERE U.FSWS_SUSTITUCION=@USU)>0 " & vbCrLf
sConsulta = sConsulta & " SET @SUSTITUTO =1" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & " SET @SUSTITUTO =0" & vbCrLf
sConsulta = sConsulta & "--CALIDAD " & vbCrLf
sConsulta = sConsulta & "IF (EXISTS(SELECT 1 FROM @PARTIDASPRES))" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @SQLFROM = @SQLFROM + ' INNER JOIN INSTANCIA_CENTROS_PARTIDAS ICP1 WITH(NOLOCK) ON I.ID=ICP1.INSTANCIA AND ICP1.TIPO_CAMPO_GS = 130 '" & vbCrLf
sConsulta = sConsulta & "   --CALIDAD" & vbCrLf
sConsulta = sConsulta & "   SET @SQLFROM = @SQLFROM + ' INNER JOIN @PARTIDASPRES PP ON ICP1.PRES5 = PP.CADENA1 AND ICP1.VALOR_TEXT = PP.CADENA2 '" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "IF (NOT @CENTROCOSTE IS NULL AND @CENTROCOSTE <> '')" & vbCrLf
sConsulta = sConsulta & "   SET @SQLFROM = @SQLFROM + ' INNER JOIN INSTANCIA_CENTROS_PARTIDAS ICP2 WITH(NOLOCK) ON I.ID=ICP2.INSTANCIA AND ICP2.TIPO_CAMPO_GS = 129 AND ICP2.VALOR_TEXT = @CENTROCOSTE '" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "--------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--crea la tabla temporal" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE #G1 (ID INT,CODIGO NVARCHAR(50),ICONO TINYINT,OBV TINYINT,ACCION_APROBAR INT,ACCION_RECHAZAR INT)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = ''" & vbCrLf
sConsulta = sConsulta & "SET @PARAM =N'@USU NVARCHAR (50),@CODIGO NVARCHAR(50)='''',@TIPO NVARCHAR(50),@FECHA_DESDE DATETIME=NULL,@FECHA_HASTA DATETIME=NULL,@MATERIAL NVARCHAR (50)='''',@ART NVARCHAR (50)='''',@EMP INT=0,@GMN1 NVARCHAR (50)='''',@COD_PROCE INT=0,@ANYO INT=0,@ESTADO NVARCHAR (50)='''',@FILAESTADO NVARCHAR (50)='''',@LONGGMN1 INT,@LONGGMN2 INT,@LONGGMN3 INT,@LONGGMN4 INT,@CENTROCOSTE VARCHAR(50) = NULL, @PARTIDASPRES TBCADENATEXTO2 READONLY'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--1. Solicitudes abiertas por usted." & vbCrLf
sConsulta = sConsulta & "CREATE TABLE #T_ABIERTAS (ID INT, CODIGO NVARCHAR(50))" & vbCrLf
sConsulta = sConsulta & "SET @SQL = 'INSERT INTO #T_ABIERTAS (ID,CODIGO) SELECT I.ID,C.CODIGO ' + @SQLFROM + @SWHERE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' AND I.NUM IS NOT NULL AND I.PETICIONARIO = @USU AND I.EN_PROCESO<>2'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL,@PARAM,@CODIGO=@CODIGO,@TIPO=@TIPO,@FECHA_DESDE=@FECHA_DESDE,@FECHA_HASTA=@FECHA_HASTA,@MATERIAL=@MATERIAL,@ART=@ART,@EMP=@EMP,@GMN1=@GMN1,@COD_PROCE=@COD_PROCE,@ANYO=@ANYO,@ESTADO=@ESTADO,@FILAESTADO=@FILAESTADO,@USU=@USU,@LONGGMN1=@LONGGMN1,@LONGGMN2=@LONGGMN2,@LONGGMN3=@LONGGMN3,@LONGGMN4=@LONGGMN4,@CENTROCOSTE=@CENTROCOSTE,@PARTIDASPRES=@PARTIDASPRES" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--2. Procesos en los que particip�  " & vbCrLf
sConsulta = sConsulta & "CREATE TABLE #T_PARTICIPO (ID INT,CODIGO NVARCHAR(50))" & vbCrLf
sConsulta = sConsulta & "SET @SQL = 'INSERT INTO #T_PARTICIPO (ID,CODIGO) SELECT I.ID,C.CODIGO ' + @SQLFROM " & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ' INNER JOIN INSTANCIA_EST WITH (NOLOCK) ON I.ID=INSTANCIA_EST.INSTANCIA AND INSTANCIA_EST.PER=@USU'" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + @SWHERE + ' AND I.NUM IS NOT NULL AND  (INSTANCIA_EST.ROL IS NOT NULL OR (INSTANCIA_EST.ROL IS  NULL AND (INSTANCIA_EST.ACCION=3 OR INSTANCIA_EST.ACCION=4)))'  " & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL,@PARAM,@CODIGO=@CODIGO,@TIPO=@TIPO,@FECHA_DESDE=@FECHA_DESDE,@FECHA_HASTA=@FECHA_HASTA,@MATERIAL=@MATERIAL,@ART=@ART,@EMP=@EMP,@GMN1=@GMN1,@COD_PROCE=@COD_PROCE,@ANYO=@ANYO,@ESTADO=@ESTADO,@FILAESTADO=@FILAESTADO,@USU=@USU,@LONGGMN1=@LONGGMN1,@LONGGMN2=@LONGGMN2,@LONGGMN3=@LONGGMN3,@LONGGMN4=@LONGGMN4,@CENTROCOSTE=@CENTROCOSTE,@PARTIDASPRES=@PARTIDASPRES" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--3. Pendientes de aprobar por usted" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE #T_PENDIENTES (ID INT,CODIGO NVARCHAR(50), ACCION_APROBAR INT, ACCION_RECHAZAR INT)" & vbCrLf
sConsulta = sConsulta & "  SET @SQL = 'INSERT INTO #T_PENDIENTES (ID,CODIGO, ACCION_APROBAR,ACCION_RECHAZAR ) SELECT I.ID,C.CODIGO,isnull(RAA.ACCION,0) AS ACCION_APROBAR,isnull(RAR.ACCION,0) AS ACCION_RECHAZAR ' + @SQLFROM " & vbCrLf
sConsulta = sConsulta & "  SET @SQL=@SQL + ' INNER JOIN INSTANCIA_BLOQUE IB WITH (NOLOCK) ON I.ID=IB.INSTANCIA AND IB.EN_PROCESO=0 AND IB. ESTADO=1 AND IB.BLOQ<>1'" & vbCrLf
sConsulta = sConsulta & "  SET @SQL=@SQL + ' INNER JOIN PM_COPIA_ROL_BLOQUE RB WITH (NOLOCK) ON RB.BLOQUE=IB.BLOQUE AND IB.ESTADO=1 AND IB.BLOQ<>1 AND IB.TRASLADADA<>1'" & vbCrLf
sConsulta = sConsulta & "  SET @SQL=@SQL + ' INNER JOIN PM_COPIA_ROL R WITH (NOLOCK)  ON RB.ROL=R.ID'" & vbCrLf
sConsulta = sConsulta & "  SET @SQL=@SQL + ' INNER JOIN PM_COPIA_BLOQUE B WITH (NOLOCK) ON RB.BLOQUE=B.ID'" & vbCrLf
sConsulta = sConsulta & "  SET @SQL=@SQL + ' LEFT JOIN PM_COPIA_ROL_ACCION RAA WITH(NOLOCK)ON RAA.ROL=R.ID AND RAA.BLOQUE=RB.BLOQUE AND RAA.APROBAR=1'   --acciones del rol actual (Aprobar)" & vbCrLf
sConsulta = sConsulta & "  SET @SQL=@SQL + ' LEFT JOIN PM_COPIA_ROL_ACCION RAR WITH(NOLOCK)ON RAR.ROL=R.ID AND RAR.BLOQUE=RB.BLOQUE AND RAR.RECHAZAR=1'   --acciones del rol actual (Rechazar)" & vbCrLf
sConsulta = sConsulta & "  SET @SQL = @SQL + @SWHERE + ' AND I.NUM IS NOT NULL AND I.EN_PROCESO=0 AND I.TRASLADADA=0 AND (I.ESTADO = 2 OR I.ESTADO = 3 OR I.ESTADO = 4) AND '" & vbCrLf
sConsulta = sConsulta & "  IF @SUSTITUTO =1" & vbCrLf
sConsulta = sConsulta & "    SET @SQL=@SQL + ' ( (R.PER=@USU AND (SELECT ISNULL(U.FSWS_SUSTITUCION,'''') FROM USU U WITH (NOLOCK) WHERE U.PER=@USU AND U.BAJA=0) = '''')  OR R.PER IN ( SELECT U.PER FROM USU U WITH (NOLOCK) WHERE U.FSWS_SUSTITUCION=@USU))'" & vbCrLf
sConsulta = sConsulta & "  ELSE" & vbCrLf
sConsulta = sConsulta & "    SET @SQL=@SQL + '( (R.PER=@USU AND (SELECT ISNULL(U.FSWS_SUSTITUCION,'''') FROM USU U WITH (NOLOCK) WHERE U.PER=@USU AND U.BAJA=0) = ''''))'" & vbCrLf
sConsulta = sConsulta & "  SET @SQL=@SQL + ' AND NOT (B.TIPO=1 AND I.PETICIONARIO<>R.PER)'  " & vbCrLf
sConsulta = sConsulta & "  SET @SQL=@SQL + ' UNION '" & vbCrLf
sConsulta = sConsulta & "  SET @SQL=@SQL + ' SELECT I.ID,C.CODIGO,isnull(RAA.ACCION,0) AS ACCION_APROBAR, isnull(RAR.ACCION,0) AS ACCION_RECHAZAR' + @SQLFROM + ' INNER JOIN INSTANCIA_BLOQUE IB WITH (NOLOCK) ON I.ID=IB.INSTANCIA AND IB.EN_PROCESO=0 AND IB.ESTADO=1 AND IB.BLOQ<>1 AND IB.TRASLADADA<>1'" & vbCrLf
sConsulta = sConsulta & "  SET @SQL=@SQL + ' INNER JOIN PM_COPIA_ROL_BLOQUE RB WITH (NOLOCK)  ON RB.BLOQUE=IB.BLOQUE'" & vbCrLf
sConsulta = sConsulta & "  SET @SQL=@SQL + ' INNER JOIN PM_COPIA_ROL R WITH (NOLOCK)  ON RB.ROL=R.ID AND R.PER IS NULL AND R.COMO_ASIGNAR=3  AND R.TIPO<>5'" & vbCrLf
sConsulta = sConsulta & "  SET @SQL=@SQL + '  INNER JOIN PM_COPIA_PARTICIPANTES PT WITH (NOLOCK)  ON PT.ROL=R.ID'" & vbCrLf
sConsulta = sConsulta & "  SET @SQL=@SQL + ' LEFT JOIN PM_COPIA_ROL_ACCION RAA WITH(NOLOCK)ON RAA.ROL=R.ID AND RAA.BLOQUE=RB.BLOQUE AND RAA.APROBAR=1'   --acciones del rol actual (Aprobar)" & vbCrLf
sConsulta = sConsulta & "  SET @SQL=@SQL + ' LEFT JOIN PM_COPIA_ROL_ACCION RAR WITH(NOLOCK)ON RAR.ROL=R.ID AND RAR.BLOQUE=RB.BLOQUE AND RAR.RECHAZAR=1'   --acciones del rol actual (Rechazar)" & vbCrLf
sConsulta = sConsulta & "  SET @SQL=@SQL + @SWHERE + ' AND I.NUM IS NOT NULL AND I.EN_PROCESO=0 AND I.TRASLADADA=0 AND I.ESTADO=2 AND '" & vbCrLf
sConsulta = sConsulta & "  IF @SUSTITUTO =1" & vbCrLf
sConsulta = sConsulta & "    SET @SQL=@SQL + ' ( (PT.PER=@USU AND (SELECT ISNULL(U.FSWS_SUSTITUCION,'''') FROM USU U WITH (NOLOCK) WHERE U.PER=@USU AND U.BAJA=0) = '''') OR PT.PER IN ( SELECT U.PER FROM USU U WITH (NOLOCK) WHERE U.FSWS_SUSTITUCION=@USU))'" & vbCrLf
sConsulta = sConsulta & "  ELSE" & vbCrLf
sConsulta = sConsulta & "    SET @SQL=@SQL + ' ( (PT.PER=@USU AND (SELECT ISNULL(U.FSWS_SUSTITUCION,'''') FROM USU U WITH (NOLOCK) WHERE U.PER=@USU AND U.BAJA=0) = ''''))'                                         " & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL,@PARAM,@CODIGO=@CODIGO,@TIPO=@TIPO,@FECHA_DESDE=@FECHA_DESDE,@FECHA_HASTA=@FECHA_HASTA,@MATERIAL=@MATERIAL,@ART=@ART,@EMP=@EMP,@GMN1=@GMN1,@COD_PROCE=@COD_PROCE,@ANYO=@ANYO,@ESTADO=@ESTADO,@FILAESTADO=@FILAESTADO,@USU=@USU,@LONGGMN1=@LONGGMN1,@LONGGMN2=@LONGGMN2,@LONGGMN3=@LONGGMN3,@LONGGMN4=@LONGGMN4,@CENTROCOSTE=@CENTROCOSTE,@PARTIDASPRES=@PARTIDASPRES" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--4. Pendientes de devoluci�n" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE #T_DEVOLUCION (ID INT,CODIGO NVARCHAR(50))" & vbCrLf
sConsulta = sConsulta & " SET @SQL = 'INSERT INTO #T_DEVOLUCION (ID,CODIGO) SELECT I.ID,C.CODIGO ' + @SQLFROM " & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + ' INNER JOIN INSTANCIA_BLOQUE IB WITH (NOLOCK) ON I.ID=IB.INSTANCIA AND IB.EN_PROCESO=0 AND IB.ESTADO=1 AND IB.BLOQ<>1 AND IB.TRASLADADA=1'" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + @SWHERE + ' AND I.NUM IS NOT NULL AND I.TRASLADADA=1'" & vbCrLf
sConsulta = sConsulta & "   IF @SUSTITUTO =1" & vbCrLf
sConsulta = sConsulta & "      SET @SQL = @SQL + ' AND ((TRASLADO_A=@USU AND (SELECT ISNULL(U.FSWS_SUSTITUCION,'''') FROM USU U WITH (NOLOCK) WHERE U.PER=@USU AND U.BAJA=0) = '''') OR (TRASLADO_A IN (SELECT U.PER FROM USU U WITH (NOLOCK) WHERE U.FSWS_SUSTITUCION=@USU)))'" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "      SET @SQL = @SQL + ' AND (TRASLADO_A=@USU AND (SELECT ISNULL(U.FSWS_SUSTITUCION,'''') FROM USU U WITH (NOLOCK) WHERE U.PER=@USU AND U.BAJA=0) = '''')'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL,@PARAM,@CODIGO=@CODIGO,@TIPO=@TIPO,@FECHA_DESDE=@FECHA_DESDE,@FECHA_HASTA=@FECHA_HASTA,@MATERIAL=@MATERIAL,@ART=@ART,@EMP=@EMP,@GMN1=@GMN1,@COD_PROCE=@COD_PROCE,@ANYO=@ANYO,@ESTADO=@ESTADO,@FILAESTADO=@FILAESTADO,@USU=@USU,@LONGGMN1=@LONGGMN1,@LONGGMN2=@LONGGMN2,@LONGGMN3=@LONGGMN3,@LONGGMN4=@LONGGMN4,@CENTROCOSTE=@CENTROCOSTE,@PARTIDASPRES=@PARTIDASPRES" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--5. Trasladas" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE #T_TRASLADADAS (ID INT,CODIGO NVARCHAR(50))" & vbCrLf
sConsulta = sConsulta & "SET @SQL = 'INSERT INTO #T_TRASLADADAS (ID,CODIGO) SELECT I.ID,C.CODIGO ' + @SQLFROM " & vbCrLf
sConsulta = sConsulta & "    SET @SQL=@SQL + ' INNER JOIN INSTANCIA_BLOQUE IB WITH (NOLOCK) ON IB.INSTANCIA=I.ID AND IB.ESTADO=1 AND IB.TRASLADADA=1 AND IB.BLOQ=0'" & vbCrLf
sConsulta = sConsulta & "    SET @SQL = @SQL + @SWHERE + '  AND I.NUM IS NOT NULL AND I.TRASLADADA=1 AND ' " & vbCrLf
sConsulta = sConsulta & "    IF @SUSTITUTO =1" & vbCrLf
sConsulta = sConsulta & "      SET @SQL = @SQL + '((DEVOLUCION_A=@USU AND (SELECT ISNULL(U.FSWS_SUSTITUCION,'''') FROM USU U WITH (NOLOCK) WHERE U.PER=@USU AND U.BAJA=0) = '''') OR (DEVOLUCION_A IN ( SELECT U.PER FROM USU U WITH (NOLOCK) WHERE U.FSWS_SUSTITUCION=@USU)))'" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "     SET @SQL = @SQL + '(DEVOLUCION_A=@USU AND (SELECT ISNULL(U.FSWS_SUSTITUCION,'''') FROM USU U WITH (NOLOCK) WHERE U.PER=@USU AND U.BAJA=0) = '''')'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL,@PARAM,@CODIGO=@CODIGO,@TIPO=@TIPO,@FECHA_DESDE=@FECHA_DESDE,@FECHA_HASTA=@FECHA_HASTA,@MATERIAL=@MATERIAL,@ART=@ART,@EMP=@EMP,@GMN1=@GMN1,@COD_PROCE=@COD_PROCE,@ANYO=@ANYO,@ESTADO=@ESTADO,@FILAESTADO=@FILAESTADO,@USU=@USU,@LONGGMN1=@LONGGMN1,@LONGGMN2=@LONGGMN2,@LONGGMN3=@LONGGMN3,@LONGGMN4=@LONGGMN4,@CENTROCOSTE=@CENTROCOSTE,@PARTIDASPRES=@PARTIDASPRES" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--6. solicitudes de las que es observador" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE #T_OTRAS (ID INT,CODIGO NVARCHAR(50))" & vbCrLf
sConsulta = sConsulta & "SET @SQL = 'INSERT INTO #T_OTRAS (ID,CODIGO) SELECT I.ID,C.CODIGO ' + @SQLFROM " & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL +' INNER JOIN PM_ROL PR WITH (NOLOCK) ON PR.WORKFLOW=S.WORKFLOW'" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + @SWHERE + '  AND I.NUM IS NOT NULL AND PR.TIPO=5 AND (" & vbCrLf
sConsulta = sConsulta & "                                       PR.PER = @USU -- EL USUARIO ES OBSERVADOR" & vbCrLf
sConsulta = sConsulta & "                                       OR EXISTS (SELECT TOP 1 V.COD FROM VIEW_ROL_OBSERVADOR V WITH(NOLOCK) WHERE S.ID = V.SOLICITUD AND V.COD=@USU)" & vbCrLf
sConsulta = sConsulta & "                                       OR PR.UON0 = 1 --EL NIVEL UON0, O SEA, RA�Z (TODA LA ORGANIZACI�N) ES OBSERVADOR" & vbCrLf
sConsulta = sConsulta & "                                       )" & vbCrLf
sConsulta = sConsulta & "                                   AND I.ESTADO>0 AND I.EN_PROCESO<>2'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL,@PARAM,@CODIGO=@CODIGO,@TIPO=@TIPO,@FECHA_DESDE=@FECHA_DESDE,@FECHA_HASTA=@FECHA_HASTA,@MATERIAL=@MATERIAL,@ART=@ART,@EMP=@EMP,@GMN1=@GMN1,@COD_PROCE=@COD_PROCE,@ANYO=@ANYO,@ESTADO=@ESTADO,@FILAESTADO=@FILAESTADO,@USU=@USU,@LONGGMN1=@LONGGMN1,@LONGGMN2=@LONGGMN2,@LONGGMN3=@LONGGMN3,@LONGGMN4=@LONGGMN4,@CENTROCOSTE=@CENTROCOSTE,@PARTIDASPRES=@PARTIDASPRES" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--7.Solicitudes que el usuario puede iniciar flujo de modificaci�n" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE #T_FLUJOMODIF (ID INT,CODIGO NVARCHAR(50))" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = 'INSERT INTO #T_FLUJOMODIF (ID,CODIGO) SELECT DISTINCT I.ID,C.CODIGO ' + @SQLFROM" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' INNER JOIN VIEW_ROL_PETICIONARIO_MODIF V WITH(NOLOCK) ON V.SOLICITUD=S.ID AND V.COD=@USU'" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + @SWHERE + ' AND I.NUM IS NOT NULL AND I.ESTADO>=100 AND I.EN_PROCESO<>2'" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' UNION ALL SELECT DISTINCT I.ID,C.CODIGO ' + @SQLFROM" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' INNER JOIN VIEW_ROL_PETICIONARIO_MODIF V WITH(NOLOCK) ON V.SOLICITUD=S.ID AND V.COD=@USU'" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + @SWHERE + ' AND I.NUM IS NOT NULL AND I.ESTADO=3 AND I.EN_PROCESO<>2'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL,@PARAM,@CODIGO=@CODIGO,@TIPO=@TIPO,@FECHA_DESDE=@FECHA_DESDE,@FECHA_HASTA=@FECHA_HASTA,@MATERIAL=@MATERIAL,@ART=@ART,@EMP=@EMP,@GMN1=@GMN1,@COD_PROCE=@COD_PROCE,@ANYO=@ANYO,@ESTADO=@ESTADO,@FILAESTADO=@FILAESTADO,@USU=@USU,@LONGGMN1=@LONGGMN1,@LONGGMN2=@LONGGMN2,@LONGGMN3=@LONGGMN3,@LONGGMN4=@LONGGMN4,@CENTROCOSTE=@CENTROCOSTE,@PARTIDASPRES=@PARTIDASPRES" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--8. Solicitudes que el usuario puede iniciar flujo de baja" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE #T_FLUJOBAJA (ID INT,CODIGO NVARCHAR(50))" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = 'INSERT INTO #T_FLUJOBAJA (ID,CODIGO) SELECT DISTINCT I.ID,C.CODIGO ' + @SQLFROM" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' INNER JOIN VIEW_ROL_PETICIONARIO_BAJA V WITH(NOLOCK) ON V.SOLICITUD=S.ID AND V.COD=@USU'" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + @SWHERE + ' AND I.NUM IS NOT NULL AND I.EN_PROCESO<>2 AND I.ESTADO>=100'" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' UNION ALL SELECT DISTINCT I.ID,C.CODIGO ' + @SQLFROM" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' INNER JOIN VIEW_ROL_PETICIONARIO_BAJA V WITH(NOLOCK) ON V.SOLICITUD=S.ID AND V.COD=@USU'" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + @SWHERE + ' AND I.NUM IS NOT NULL AND I.EN_PROCESO<>2 AND I.ESTADO=4'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL,@PARAM,@CODIGO=@CODIGO,@TIPO=@TIPO,@FECHA_DESDE=@FECHA_DESDE,@FECHA_HASTA=@FECHA_HASTA,@MATERIAL=@MATERIAL,@ART=@ART,@EMP=@EMP,@GMN1=@GMN1,@COD_PROCE=@COD_PROCE,@ANYO=@ANYO,@ESTADO=@ESTADO,@FILAESTADO=@FILAESTADO,@USU=@USU,@LONGGMN1=@LONGGMN1,@LONGGMN2=@LONGGMN2,@LONGGMN3=@LONGGMN3,@LONGGMN4=@LONGGMN4,@CENTROCOSTE=@CENTROCOSTE,@PARTIDASPRES=@PARTIDASPRES" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "------------------------------------------" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL=''" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--1. Solicitudes abiertas por usted." & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' SELECT DISTINCT ID,CODIGO,0 as ICONO, 0 as OBV, 0 as ACCION_APROBAR, 0 as ACCION_RECHAZAR FROM #T_ABIERTAS WHERE 1=1'" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' AND ID NOT IN (SELECT ID FROM #T_PENDIENTES)' " & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' AND ID NOT IN (SELECT ID FROM #T_DEVOLUCION)'" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' AND ID NOT IN (SELECT ID FROM #T_TRASLADADAS)'" & vbCrLf
sConsulta = sConsulta & "SET @SQL= @SQL + ' UNION ALL '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--2. Procesos en los que particip�   " & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' SELECT DISTINCT ID,CODIGO,0 AS ICONO,0 AS OBV, 0 as ACCION_APROBAR, 0 as ACCION_RECHAZAR FROM #T_PARTICIPO WHERE 1=1'" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' AND ID NOT IN (SELECT ID FROM #T_ABIERTAS)'" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' AND ID NOT IN (SELECT ID FROM #T_PENDIENTES)'" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' AND ID NOT IN (SELECT ID FROM #T_DEVOLUCION)'" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' AND ID NOT IN (SELECT ID FROM #T_TRASLADADAS)' " & vbCrLf
sConsulta = sConsulta & "SET @SQL= @SQL + ' UNION ALL '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--3. Pendientes de tratar" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' SELECT DISTINCT ID,CODIGO,1 AS ICONO,0 AS OBV, ACCION_APROBAR, ACCION_RECHAZAR FROM #T_PENDIENTES'" & vbCrLf
sConsulta = sConsulta & "SET @SQL= @SQL + ' UNION ALL '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--4. Pendientes de devoluci�n" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' SELECT DISTINCT ID,CODIGO, 1 AS ICONO,0 AS OBV, 0 as ACCION_APROBAR, 0 as ACCION_RECHAZAR  FROM #T_DEVOLUCION'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL= @SQL + ' UNION ALL '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--5-Solicitudes trasladadas en espera de devoluci�n" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL +' SELECT DISTINCT ID, CODIGO,0 AS ICONO,0 AS OBV, 0 as ACCION_APROBAR, 0 as ACCION_RECHAZAR FROM #T_TRASLADADAS'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL= @SQL + ' UNION ALL '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--6-OTRAS SOLICITUDES (OBSERVADORES)" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL +' SELECT DISTINCT ID,CODIGO,0 AS ICONO,1 AS OBV, 0 as ACCION_APROBAR, 0 as ACCION_RECHAZAR FROM #T_OTRAS WHERE 1=1'" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' AND ID NOT IN (SELECT ID FROM #T_PENDIENTES)'" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' AND ID NOT IN (SELECT ID FROM #T_DEVOLUCION)'" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' AND ID NOT IN (SELECT ID FROM #T_TRASLADADAS)'" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' AND ID NOT IN (SELECT ID FROM #T_PARTICIPO)'   " & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' AND ID NOT IN (SELECT ID FROM #T_ABIERTAS)'" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' AND ID NOT IN (SELECT ID FROM #T_FLUJOMODIF)'" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' AND ID NOT IN (SELECT ID FROM #T_FLUJOBAJA)'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-----------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "--Solicitudes que el usuario puede iniciar flujo de modificaci�n" & vbCrLf
sConsulta = sConsulta & "-----------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' UNION ALL '" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' SELECT DISTINCT ID, CODIGO,0 AS ICONO,0 AS OBV, 0 as ACCION_APROBAR, 0 as ACCION_RECHAZAR FROM #T_FLUJOMODIF'" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' WHERE ID NOT IN (SELECT ID FROM #T_PENDIENTES)'" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' AND ID NOT IN (SELECT ID FROM #T_DEVOLUCION)'  " & vbCrLf
sConsulta = sConsulta & "  " & vbCrLf
sConsulta = sConsulta & "-----------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "--Solicitudes que el usuario puede iniciar flujo de baja" & vbCrLf
sConsulta = sConsulta & "-----------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' UNION ALL '" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' SELECT DISTINCT ID, CODIGO,0 AS ICONO,0 AS OBV, 0 as ACCION_APROBAR, 0 as ACCION_RECHAZAR FROM #T_FLUJOBAJA'" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' WHERE ID NOT IN (SELECT ID FROM #T_PENDIENTES)'" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' AND ID NOT IN (SELECT ID FROM #T_DEVOLUCION)'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL='INSERT INTO #G1 (ID,CODIGO, ICONO,OBV,ACCION_APROBAR,ACCION_RECHAZAR)' +  @SQL" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL,@PARAM,@CODIGO=@CODIGO,@TIPO=@TIPO,@FECHA_DESDE=@FECHA_DESDE,@FECHA_HASTA=@FECHA_HASTA,@MATERIAL=@MATERIAL,@ART=@ART,@EMP=@EMP,@GMN1=@GMN1,@COD_PROCE=@COD_PROCE,@ANYO=@ANYO,@ESTADO=@ESTADO,@FILAESTADO=@FILAESTADO,@USU=@USU,@LONGGMN1=@LONGGMN1,@LONGGMN2=@LONGGMN2,@LONGGMN3=@LONGGMN3,@LONGGMN4=@LONGGMN4,@CENTROCOSTE=@CENTROCOSTE,@PARTIDASPRES=@PARTIDASPRES" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--*************************************************SELECT GEN�RICA PARA SACAR LOS DATOS ***********************************************************" & vbCrLf
sConsulta = sConsulta & "--Creamos una tabla temporal" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE #G2(ID INT, CODIGO NVARCHAR(50), FEC_ALTA DATETIME, CODESTADO INT, FECHA_INICIO DATETIME,FECHA_EXP DATETIME, CODPRO NVARCHAR(40), PROVEEDOR NVARCHAR(250),CODEMP NVARCHAR(40), EMPRESA NVARCHAR(150),ESTADO NVARCHAR(50),CODPET NVARCHAR(40),PETICIONARIO NVARCHAR(250),TIPO_CONTRATO NVARCHAR(250),ULT_VERSION INT, INSTANCIA_ESTADO INT,ID_CONTRATO INT,ICONO TINYINT, OBSERVADOR TINYINT, TRASLADADA TINYINT,MON NVARCHAR(5),CONTACTO NVARCHAR(100),IMPORTE FLOAT,PROCE NVARCHAR(100),EN_PROCESO TINYINT, ACCION_APROBAR INT, ACCION_RECHAZAR INT, BLOQUE INT)" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE #G3(ID INT, CODIGO NVARCHAR(50), FEC_ALTA DATETIME, CODESTADO INT, FECHA_INICIO DATETIME,FECHA_EXP DATETIME, CODPRO NVARCHAR(40), PROVEEDOR NVARCHAR(250),CODEMP NVARCHAR(40), EMPRESA NVARCHAR(150),ESTADO NVARCHAR(50),CODPET NVARCHAR(40),PETICIONARIO NVARCHAR(250),TIPO_CONTRATO NVARCHAR(250),ULT_VERSION INT, INSTANCIA_ESTADO INT,ID_CONTRATO INT,ICONO TINYINT, OBSERVADOR TINYINT, TRASLADADA TINYINT,MON NVARCHAR(5),CONTACTO NVARCHAR(100),IMPORTE FLOAT,PROCE NVARCHAR(100),EN_PROCESO TINYINT, ACCION_APROBAR INT, ACCION_RECHAZAR INT, BLOQUE INT)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL=''" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'SELECT I.ID AS ID,C.CODIGO,I.FEC_ALTA AS FECHA_ALTA,C.ESTADO CODESTADO,C.FEC_INI FECHA_INICIO,C.FEC_FIN FECHA_EXP,PRO.COD CODPRO,PRO.DEN PROVEEDOR,EMP.NIF CODEMP,  EMP.DEN EMPRESA,NULL AS ESTADO,PER.COD CODPET ,PER.NOM + '' '' + PER.APE AS PETICIONARIO, S.DEN_'+ @IDI + ' TIPO_CONTRATO, I.ULT_VERSION,I.ESTADO INSTANCIA_ESTADO,C.ID ID_CONTRATO,G1.ICONO,G1.OBV,I.TRASLADADA,C.MON,CON.NOM + '' '' + CON.APE AS CONTACTO,I.IMPORTE,CI.PROCESO,I.EN_PROCESO,G1.ACCION_APROBAR,G1.ACCION_RECHAZAR,M.BLOQUE '" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL +  ' FROM  INSTANCIA I WITH (NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL +  ' INNER JOIN #G1 G1 WITH(NOLOCK) ON I.ID=G1.ID'" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL +  ' INNER JOIN CONTRATO C WITH(NOLOCK) ON C.INSTANCIA=G1.ID'" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL +  ' LEFT JOIN (SELECT DISTINCT CONTRATO,ANYO,GMN1,PROCE,convert(NVARCHAR(50),ANYO) + ''/'' + GMN1 + ''/'' + convert(NVARCHAR(50),PROCE) AS PROCESO FROM CONTRATO_ITEM CI WITH(NOLOCK) ) CI ON CI.CONTRATO=C.ID'" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL +  ' INNER JOIN SOLICITUD S WITH (NOLOCK) ON I.SOLICITUD=S.ID'   --para sacar la solicitud" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL +  ' INNER JOIN TIPO_SOLICITUDES TS WITH (NOLOCK) ON S.TIPO=TS.ID'   --para sacar el tipo de solicitud" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL +  ' LEFT JOIN PROVE PRO WITH (NOLOCK) ON PRO.COD = C.PROVE'" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL +  ' LEFT JOIN CON CON WITH (NOLOCK) ON CON.PROVE = C.PROVE AND C.CONTACTO=CON.ID'" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL +  ' INNER JOIN PER WITH (NOLOCK) ON PER.COD = I.PETICIONARIO'" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL +  ' LEFT JOIN EMP WITH (NOLOCK) ON C.EMP=EMP.ID'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Mira si la persona es un rol de la instancia, y en ese caso si tiene permisos para ver el detalle del flujo y el detalle de los participantes:" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' LEFT JOIN(SELECT INSTANCIA I,PER P,MAX(VER_DETALLE_PER)VDP,MAX(VER_FLUJO)VER_FLUJO FROM PM_COPIA_ROL WITH(NOLOCK) GROUP BY INSTANCIA,PER) CR ON I.ID = CR.I AND @USU=CR.P'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Para obtener si hay etapas en paralelo" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ' INNER JOIN (SELECT INSTANCIA I, COUNT(*) P,MIN(BLOQUE) BLOQUE FROM INSTANCIA_BLOQUE  WITH (NOLOCK)  WHERE ESTADO=1 AND BLOQ<>1 GROUP BY INSTANCIA) M ON I.ID=M.I'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @NOM_TABLA ='' --AND (@ESTADO='8' OR @ESTADO='') " & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       --para las solicitudes que solo tienen registro en la tabla INSTANCIA y todav�a no las ha procesado el WebService" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + ' UNION ALL '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + ' SELECT I.ID AS ID,C.CODIGO,I.FEC_ALTA AS FECHA_ALTA,C.ESTADO CODESTADO,C.FEC_INI FECHA_INICIO,C.FEC_FIN FECHA_EXP,PRO.COD CODPRO,PRO.DEN PROVEEDOR,EMP.NIF CODEMP,  EMP.DEN EMPRESA,NULL AS ESTADO,PER.COD CODPET, PER.NOM + PER.APE AS PETICIONARIO, S.DEN_'+ @IDI + ' TIPO_CONTRATO, I.ULT_VERSION,I.ESTADO INSTANCIA_ESTADO,C.ID ID_CONTRATO,0 AS ICONO,0 AS OBV, 0 AS TRASLADADA,C.MON,CON.NOM + '' '' + CON.APE AS CONTACTO,I.IMPORTE,CI.PROCESO,I.EN_PROCESO,0 as ACCION_APROBAR,0 as ACCION_RECHAZAR,0 as BLOQUE '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + ' FROM CONTRATO C WITH(NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + ' LEFT JOIN (SELECT DISTINCT CONTRATO,ANYO,GMN1,PROCE,convert(NVARCHAR(50),ANYO) + ''/'' + GMN1 + ''/'' + convert(NVARCHAR(50),PROCE) AS PROCESO FROM CONTRATO_ITEM CI WITH(NOLOCK) ) CI ON CI.CONTRATO=C.ID'" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + ' INNER JOIN INSTANCIA I WITH(NOLOCK) ON I.ID=C.INSTANCIA '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + ' INNER JOIN PER P WITH(NOLOCK) ON P.COD = I.PETICIONARIO '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + ' INNER JOIN DEP D WITH (NOLOCK) ON P.DEP=D.COD '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + ' INNER JOIN SOLICITUD S WITH(NOLOCK) ON I.SOLICITUD=S.ID INNER JOIN TIPO_SOLICITUDES TS WITH (NOLOCK) ON S.TIPO=TS.ID'" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + ' LEFT JOIN PROVE PRO WITH (NOLOCK) ON PRO.COD = C.PROVE'" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=  @SQL + ' LEFT JOIN CON CON WITH (NOLOCK) ON CON.PROVE = C.PROVE AND C.CONTACTO=CON.ID'" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + ' INNER JOIN PER WITH (NOLOCK) ON PER.COD = I.PETICIONARIO'" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + ' LEFT JOIN EMP WITH (NOLOCK) ON C.EMP=EMP.ID'" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + ' LEFT JOIN INSTANCIA_VISORES_PM C1 WITH(NOLOCK) ON I.ID=C1.INSTANCIA'" & vbCrLf
sConsulta = sConsulta & "   IF @PALABRA_CLAVE IS NOT NULL AND @PALABRA_CLAVE <> '' " & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + ' INNER JOIN PM_COPIA_ADJUN PCA WITH(NOLOCK) ON C.ID = PCA.ID_CONTRATO'" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + @SWHERE + ' AND PETICIONARIO = @USU AND I.ULT_VERSION IS NULL AND I.NUM IS NULL AND EN_PROCESO=1 AND TS.TIPO =5'" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL2= N' INSERT INTO #G2 ' + @SQL" & vbCrLf
sConsulta = sConsulta & "SET @PARAM =N'@IDI NVARCHAR(20),@USU NVARCHAR (50),@CODIGO NVARCHAR(50)='''',@TIPO NVARCHAR(50),@FECHA_DESDE DATETIME=NULL,@FECHA_HASTA DATETIME=NULL,@MATERIAL NVARCHAR (50)='''',@ART NVARCHAR (50)='''',@EMP INT=0,@GMN1 NVARCHAR (50)='''',@COD_PROCE INT=0,@ANYO INT=0,@ESTADO NVARCHAR (50)='''',@FILAESTADO NVARCHAR (50)='''''" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL2" & vbCrLf
sConsulta = sConsulta & ", @PARAM" & vbCrLf
sConsulta = sConsulta & ",@CODIGO=@CODIGO,@TIPO=@TIPO,@FECHA_DESDE=@FECHA_DESDE,@FECHA_HASTA=@FECHA_HASTA,@MATERIAL=@MATERIAL,@ART=@ART,@EMP=@EMP,@GMN1=@GMN1,@COD_PROCE=@COD_PROCE,@ANYO=@ANYO,@ESTADO=@ESTADO,@FILAESTADO=@FILAESTADO,@USU=@USU,@IDI=@IDI  " & vbCrLf
sConsulta = sConsulta & "    " & vbCrLf
sConsulta = sConsulta & "-- *****LA SELECT FINAL ********" & vbCrLf
sConsulta = sConsulta & "set @SQL='SELECT DISTINCT #G2.ID,#G2.CODIGO, #G2.FEC_ALTA, #G2.CODESTADO, #G2.FECHA_INICIO, #G2.FECHA_EXP, #G2.CODPRO, #G2.PROVEEDOR, #G2.CODEMP, #G2.EMPRESA, #G2.ESTADO, #G2.CODPET, #G2.PETICIONARIO, #G2.TIPO_CONTRATO, #G2.ULT_VERSION, #G2.INSTANCIA_ESTADO, #G2.ID_CONTRATO, #G2.ICONO, #G2.OBSERVADOR, #G2.TRASLADADA, #G2.MON, #G2.CONTACTO, #G2.IMPORTE, #G2.PROCE, #G2.EN_PROCESO, #G2.ACCION_APROBAR, #G2.ACCION_RECHAZAR, #G2.BLOQUE '" & vbCrLf
sConsulta = sConsulta & "--CALIDAD: Cada tabla de filtro tiene unos campos diferentes. Hay que hacer la select con *." & vbCrLf
sConsulta = sConsulta & "IF @NOM_TABLA <>''" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + ', FILTRO.* '" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' FROM #G2'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ' LEFT JOIN INSTANCIA_ADICIONAL IA WITH(NOLOCK) ON #G2.ID=IA.INSTANCIA AND IA.PER=@USU '" & vbCrLf
sConsulta = sConsulta & "IF @NOM_TABLA<>''" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + ' INNER JOIN ' + @IDI + '_' + @NOM_TABLA + ' FILTRO WITH(NOLOCK) ON #G2.ID = FILTRO.INSTANCIA '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL_CONT=@SQL" & vbCrLf
sConsulta = sConsulta & "IF @NOM_TABLA <>''" & vbCrLf
sConsulta = sConsulta & "   SET @SQL_CONT = REPLACE(@SQL_CONT,', FILTRO.* ','')" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "IF @ESTADO<>'' AND @ESTADO<>'8' " & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       SET @ESTADO = REPLACE(@ESTADO,',',''',''')" & vbCrLf
sConsulta = sConsulta & "       SET @ESTADO = '''' + @ESTADO + ''''" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = @SQL + ' WHERE #G2.CODESTADO IN (' + @ESTADO + ')'" & vbCrLf
sConsulta = sConsulta & "   END " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ORDENACION IS NOT NULL AND @ORDENACION <> ''" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   IF SUBSTRING(@ORDENACION,1,2)='C_' --Es un campo del formulario" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = @SQL + ' ORDER BY FILTRO.' + @ORDENACION " & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "       --SET @SQL = @SQL + ' ORDER BY #G2.' + @ORDENACION " & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "       DECLARE @ROW INT = ((@PAGENUMBER-1) * @PAGESIZE) +1" & vbCrLf
sConsulta = sConsulta & "       IF @OBTENERTODOSCONTRATOS=0" & vbCrLf
sConsulta = sConsulta & "           BEGIN" & vbCrLf
sConsulta = sConsulta & "               --CALIDAD. Estos dos * se quedan pq no es la tabla de bbdd certificados sino una tabla hecha ad hoc mediante WITH CERTIFICADOS AS (...) y en estos puntos suspensivos puede venir filtro.* o no -> no se conocen las columnas" & vbCrLf
sConsulta = sConsulta & "               SET @SQL = ';WITH CONTRATOS AS (' + @SQL + '), TOTCONTRATOS AS (SELECT *,ROW_NUMBER() OVER (ORDER BY ' + @ORDENACION + ') AS ROWNUMBER FROM CONTRATOS WITH (NOLOCK)) SELECT *,ROWNUMBER FROM TOTCONTRATOS WITH (NOLOCK) WHERE ROWNUMBER BETWEEN ' + CONVERT(VARCHAR(10),@ROW) + ' AND ' + CONVERT(VARCHAR(10),(@ROW + @PAGESIZE - 1))" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "END " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL ,N'@USU NVARCHAR(50)',@USU=@USU" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @FILAESTADO <>''" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @SQL2= N'INSERT INTO #G3 ' + @SQL_CONT + ' WHERE #G2.CODESTADO IN (' + @FILAESTADO + ')'" & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL2,N'@USU NVARCHAR(50)',@USU=@USU" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @SQL2= N'INSERT INTO #G3 '  + @SQL_CONT" & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL2,N'@USU NVARCHAR(50)',@USU=@USU" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--***********************************************************************************************************************************" & vbCrLf
sConsulta = sConsulta & "--CONTADORES" & vbCrLf
sConsulta = sConsulta & "DECLARE @CONT0 INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @CONT1 INT --Borrador" & vbCrLf
sConsulta = sConsulta & "DECLARE @CONT2 INT --En Curso de Aprobaci�n" & vbCrLf
sConsulta = sConsulta & "DECLARE @CONT3 INT --Vigentes" & vbCrLf
sConsulta = sConsulta & "DECLARE @CONT4 INT --Expirados" & vbCrLf
sConsulta = sConsulta & "DECLARE @CONT5 INT --Rechazados" & vbCrLf
sConsulta = sConsulta & "DECLARE @CONT6 INT --Anulados" & vbCrLf
sConsulta = sConsulta & "DECLARE @CONT7 INT --Todas" & vbCrLf
sConsulta = sConsulta & "DECLARE @CONT8 INT --Proximas a expirar" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   --Borrador" & vbCrLf
sConsulta = sConsulta & "   SET @PARAM =N'@CONT0 INT = 0 OUTPUT '" & vbCrLf
sConsulta = sConsulta & "   SET @CONT1=0" & vbCrLf
sConsulta = sConsulta & "   IF CHARINDEX('0',@FILAESTADO,0)<>0 OR @FILAESTADO=''" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "      SET @SQL='SET @CONT0=(SELECT COUNT(DISTINCT C.ID) FROM #G3 C WITH (NOLOCK) WHERE CODESTADO=0)'" & vbCrLf
sConsulta = sConsulta & "      EXEC SP_EXECUTESQL @SQL" & vbCrLf
sConsulta = sConsulta & "      , @PARAM" & vbCrLf
sConsulta = sConsulta & "      ,@CONT0=@CONT1 OUTPUT  " & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "  " & vbCrLf
sConsulta = sConsulta & "   --En curso de aprobacion" & vbCrLf
sConsulta = sConsulta & "   SET @CONT2=0" & vbCrLf
sConsulta = sConsulta & "   IF CHARINDEX('2',@FILAESTADO,0)<>0 OR @FILAESTADO=''" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "      SET @SQL='SET @CONT0=(SELECT COUNT(DISTINCT C.ID) FROM #G3 C WITH (NOLOCK) WHERE CODESTADO=2)'" & vbCrLf
sConsulta = sConsulta & "      " & vbCrLf
sConsulta = sConsulta & "      EXEC SP_EXECUTESQL @SQL" & vbCrLf
sConsulta = sConsulta & "      , @PARAM" & vbCrLf
sConsulta = sConsulta & "      ,@CONT0=@CONT2 OUTPUT  " & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   --Proximos a expirar" & vbCrLf
sConsulta = sConsulta & "   SET @CONT8=0" & vbCrLf
sConsulta = sConsulta & "   IF CHARINDEX('4',@FILAESTADO,0)<>0 OR @FILAESTADO=''" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "     SET @SQL='SET @CONT0=(SELECT COUNT(DISTINCT C.ID) FROM #G3 C WITH (NOLOCK) WHERE CODESTADO=4)'" & vbCrLf
sConsulta = sConsulta & "     " & vbCrLf
sConsulta = sConsulta & "     EXEC SP_EXECUTESQL @SQL" & vbCrLf
sConsulta = sConsulta & "      , @PARAM" & vbCrLf
sConsulta = sConsulta & "      ,@CONT0=@CONT8 OUTPUT  " & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   --Vigentes" & vbCrLf
sConsulta = sConsulta & "   SET @CONT3=0" & vbCrLf
sConsulta = sConsulta & "   IF CHARINDEX('3',@FILAESTADO,0)<>0 OR @FILAESTADO=''" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "      SET @SQL='SET @CONT0=(SELECT COUNT(DISTINCT C.ID) FROM #G3 C WITH (NOLOCK) WHERE CODESTADO=3)'" & vbCrLf
sConsulta = sConsulta & "      " & vbCrLf
sConsulta = sConsulta & "      EXEC SP_EXECUTESQL @SQL" & vbCrLf
sConsulta = sConsulta & "      , @PARAM" & vbCrLf
sConsulta = sConsulta & "      ,@CONT0=@CONT3 OUTPUT  " & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   --Expirados" & vbCrLf
sConsulta = sConsulta & "   SET @CONT4=0" & vbCrLf
sConsulta = sConsulta & "   IF CHARINDEX('5',@FILAESTADO,0)<>0 OR @FILAESTADO=''" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "      SET @SQL='SET @CONT0=(SELECT COUNT(DISTINCT C.ID) FROM #G3 C WITH (NOLOCK) WHERE CODESTADO=5)'" & vbCrLf
sConsulta = sConsulta & "      " & vbCrLf
sConsulta = sConsulta & "      EXEC SP_EXECUTESQL @SQL" & vbCrLf
sConsulta = sConsulta & "      , @PARAM" & vbCrLf
sConsulta = sConsulta & "      ,@CONT0=@CONT4 OUTPUT  " & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   --Rechazados" & vbCrLf
sConsulta = sConsulta & "   SET @CONT5=0" & vbCrLf
sConsulta = sConsulta & "   IF CHARINDEX('6',@FILAESTADO,0)<>0 OR @FILAESTADO=''" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "      SET @SQL='SET @CONT0=(SELECT COUNT(DISTINCT C.ID) FROM #G3 C WITH (NOLOCK) WHERE CODESTADO=6)'" & vbCrLf
sConsulta = sConsulta & "      " & vbCrLf
sConsulta = sConsulta & "      EXEC SP_EXECUTESQL @SQL" & vbCrLf
sConsulta = sConsulta & "      , @PARAM" & vbCrLf
sConsulta = sConsulta & "      ,@CONT0=@CONT5 OUTPUT  " & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   --Anulados" & vbCrLf
sConsulta = sConsulta & "   SET @CONT6=0" & vbCrLf
sConsulta = sConsulta & "   IF CHARINDEX('7',@FILAESTADO,0)<>0 OR @FILAESTADO=''" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "      SET @SQL='SET @CONT0=(SELECT COUNT(DISTINCT C.ID) FROM #G3 C WITH (NOLOCK) WHERE CODESTADO=7)'" & vbCrLf
sConsulta = sConsulta & "      " & vbCrLf
sConsulta = sConsulta & "      EXEC SP_EXECUTESQL @SQL" & vbCrLf
sConsulta = sConsulta & "      , @PARAM" & vbCrLf
sConsulta = sConsulta & "      ,@CONT0=@CONT6 OUTPUT  " & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   --TODAS:   " & vbCrLf
sConsulta = sConsulta & "   SET @CONT7 =0" & vbCrLf
sConsulta = sConsulta & "   SET @CONT7 = @CONT1+@CONT2+@CONT3+@CONT4+@CONT5+@CONT6+@CONT8" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Muestra los contadores" & vbCrLf
sConsulta = sConsulta & "SELECT  @CONT1 GUARDADAS, @CONT2 EN_CURSO_APROBACION,@CONT3 VIGENTES,@CONT4 EXPIRADOS,@CONT5 RECHAZADOS,@CONT6 ANULADOS,@CONT7 TODAS, @CONT8 PROXIMO_A_EXPIRAR" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Contadores de Estados sin filtros (para mostrar las ALERTAS)" & vbCrLf
sConsulta = sConsulta & " SELECT COUNT(DISTINCT ID) PDTE_DE_TRATAR,@CONT8 PROXIMO_A_EXPIRAR FROM #G2 WITH (NOLOCK) WHERE ICONO = 1      " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DROP TABLE #T_ABIERTAS" & vbCrLf
sConsulta = sConsulta & "DROP TABLE #T_PARTICIPO" & vbCrLf
sConsulta = sConsulta & "DROP TABLE #T_PENDIENTES" & vbCrLf
sConsulta = sConsulta & "DROP TABLE #T_DEVOLUCION" & vbCrLf
sConsulta = sConsulta & "DROP TABLE #T_TRASLADADAS" & vbCrLf
sConsulta = sConsulta & "DROP TABLE #T_OTRAS" & vbCrLf
sConsulta = sConsulta & "DROP TABLE #G1" & vbCrLf
sConsulta = sConsulta & "DROP TABLE #G2" & vbCrLf
sConsulta = sConsulta & "DROP TABLE #G3" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSGSW_DEVOLVER_ITEMSGRUPO]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSGSW_DEVOLVER_ITEMSGRUPO]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSGSW_DEVOLVER_ITEMSGRUPO] @ANYO INT, @GMN1 VARCHAR(50), @PROCE INT, @CODGRUPO VARCHAR(20),@PROVE VARCHAR(50),@IDEMP INT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @EST INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PARAM AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @ERP INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ESCALADOS INT=0" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDGRUPO INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @USAR_ART4_UON INT=0" & vbCrLf
sConsulta = sConsulta & "DECLARE @TRASPASO_PLANIF INT=0" & vbCrLf
sConsulta = sConsulta & "DECLARE @USAR_ORGCOMPRAS INT =0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @ESCALADOS=COUNT(ID) FROM PROCE_GRUPO PG WITH (NOLOCK) WHERE PG.ANYO=@ANYO  AND PG.GMN1=@GMN1 AND PG.PROCE=@PROCE AND PG.COD=@CODGRUPO AND PG.ESCALADOS=1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @EST=EST FROM PROCE P WITH (NOLOCK) WHERE ANYO =@ANYO AND COD=@PROCE AND GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @ERP=ES.ERP FROM ERP_SOCIEDAD ES WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN EMP E WITH (NOLOCK) ON E.SOCIEDAD=ES.SOCIEDAD AND E.ID=@IDEMP" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "SELECT @IDGRUPO=ID FROM PROCE_GRUPO PG WITH (NOLOCK) WHERE PG.ANYO=@ANYO AND PG.GMN1=@GMN1 AND PG.PROCE=@PROCE AND PG.COD=@CODGRUPO " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @USAR_ART4_UON=USAR_ART4_UON, @TRASPASO_PLANIF=TRASPASO_ART_PLANIF, @USAR_ORGCOMPRAS=USAR_ORGCOMPRAS  FROM PARGEN_INTERNO WITH(NOLOCK) WHERE ID=1" & vbCrLf
sConsulta = sConsulta & "    " & vbCrLf
sConsulta = sConsulta & "   SET @SQL='SELECT DISTINCT COALESCE(LIA.ID,LIA1.ID) LIA_ID,I.ID ID_ITEM,I.ART CODART,I.DESCR DENART,I.UNI,'" & vbCrLf
sConsulta = sConsulta & "   if  @ESCALADOS>0 " & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + ' PGE.ID ESC_ID,PGE.INICIO,PGE.FIN,'" & vbCrLf
sConsulta = sConsulta & "   else" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + 'IA.CANT_ADJ,'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + ' IOF.PREC_VALIDO PREC_ADJ,P.MON,'" & vbCrLf
sConsulta = sConsulta & "   IF @USAR_ORGCOMPRAS =1  --Si el �ltimo traspaso es un borrado, lee lo que haya en el proceso. Si no (era una I), lee lo de log_item_adj" & vbCrLf
sConsulta = sConsulta & "      SET @SQL=@SQL + ' CASE WHEN LIA3.ACCION=''D'' THEN IT_CEN.CENTRO ELSE COALESCE(IT_CEN.CENTRO,LIA.CENTRO,'''') end CENTROS,'''' AS CENTRO,'" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "      SET @SQL=@SQL + ''''' CENTROS,'''' AS CENTRO,'" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + ' CASE WHEN LIA3.ACCION = ''D''  THEN NULL ELSE LG.ESTADO END AS ESTADOINT  " & vbCrLf
sConsulta = sConsulta & "   , I.FECINI FECINI_SUM,I.FECFIN FECFIN_SUM,LIA.PROVE_ERP,PE.DEN_ERP," & vbCrLf
sConsulta = sConsulta & "   COALESCE(IT_ATRIB1.INT_ATRIB_ID ,PA.ATRIB,IAE.ATRIB,PGA.ATRIB) ATRIBID,DA.COD ATRIBCOD,DA.DEN ATRIBDEN,COALESCE(IT_ATRIB1.VALOR_TEXT ,IAE.VALOR_TEXT,OA.VALOR_TEXT,PGA.VALOR_TEXT,IAT.VALOR_TEXT) ATRIBVALOR_TEXT ,COALESCE(IT_ATRIB1.VALOR_NUM ,IAE.VALOR_NUM,OA.VALOR_NUM,PGA.VALOR_NUM,IAT.VALOR_NUM) ATRIBVALOR_NUM,COALESCE(IT_ATRIB1.VALOR_FEC ,IAE.VALOR_FEC,OA.VALOR_FEC,PGA.VALOR_FEC,IAT.VALOR_FEC) ATRIBVALOR_FEC,COALESCE(IT_ATRIB1.VALOR_BOOL ,IAE.VALOR_BOOL,OA.VALOR_BOOL,PGA.VALOR_BOOL,IAT.VALOR_BOOL) ATRIBVALOR_BOOL,DA.TIPO ATRIBTIPO,COALESCE(PA.OBLIG,IAT.OBLIGATORIO) ATRIBOBLIGATORIO,'" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   IF @USAR_ART4_UON  =1 " & vbCrLf
sConsulta = sConsulta & "      SET @SQL=@SQL + 'CASE WHEN IT_EMP.UON1 IS not NULL THEN IT_EMP.UON1 ELSE ART4_UON.UON1 END UON1," & vbCrLf
sConsulta = sConsulta & "      CASE WHEN IT_EMP.UON2 IS not NULL THEN IT_EMP.UON2 ELSE ART4_UON.UON2 END UON2," & vbCrLf
sConsulta = sConsulta & "      CASE WHEN IT_EMP.UON3 IS not NULL THEN IT_EMP.UON3 ELSE ART4_UON.UON3 END UON3 '" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "      SET @SQL=@SQL + 'IT_EMP.UON1 UON1,IT_EMP.UON2 UON2,IT_EMP.UON3 UON3 '" & vbCrLf
sConsulta = sConsulta & "      " & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + ',CASE WHEN DISTR.DISTRIBUIDOR IS NULL THEN 0 ELSE 1 END DISTRIBUIDOR,'" & vbCrLf
sConsulta = sConsulta & "   IF @ESCALADOS>0 " & vbCrLf
sConsulta = sConsulta & "      SET @SQL=@SQL + '1 ESCALADOS,PG.ESCALAUNI '" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "      SET @SQL=@SQL + '0 ESCALADOS '" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   --------FROM--------------------------------------------" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + 'FROM PROCE P WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PROCE_GRUPO PG WITH (NOLOCK) ON PG.ANYO=P.ANYO AND PG.GMN1=P.GMN1 AND PG.PROCE=P.COD AND PG.COD=@CODGRUPO " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN ITEM I WITH (NOLOCK) ON I.ANYO=P.ANYO AND I.GMN1_PROCE=P.GMN1 AND I.PROCE=P.COD AND I.GRUPO=PG.ID '" & vbCrLf
sConsulta = sConsulta & "   IF @EST=11 --Si el proceso esta parcialmente cerrado" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL + ' AND I.EST=1 '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @ESCALADOS>0 --Recoge los datos de adjudicaciones de las tablas de escalados" & vbCrLf
sConsulta = sConsulta & "      SET @SQL =@SQL + 'INNER JOIN ITEM_ADJESC IA WITH (NOLOCK) ON IA.ANYO=I.ANYO AND IA.GMN1=I.GMN1_PROCE AND IA.PROCE=I.PROCE AND IA.ITEM=I.ID AND IA.PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PROCE_GR_ESC PGE WITH (NOLOCK) ON PGE.ANYO=I.ANYO AND PGE.GMN1=I.GMN1_PROCE AND PGE.PROCE=I.PROCE AND PGE.GRUPO =PG.ID --AND PGE.ID=IA.ESC '" & vbCrLf
sConsulta = sConsulta & "   ELSE -- Recoge la adjudicaci�n" & vbCrLf
sConsulta = sConsulta & "      SET @SQL =@SQL + 'INNER JOIN ITEM_ADJ IA WITH (NOLOCK) ON I.ANYO=IA.ANYO AND I.GMN1_PROCE=IA.GMN1 AND I.PROCE=IA.PROCE AND I.ID=IA.ITEM AND IA.PROVE=@PROVE '" & vbCrLf
sConsulta = sConsulta & "    " & vbCrLf
sConsulta = sConsulta & "    ---Obtiene las empresas a las que se ha distribuido la compra" & vbCrLf
sConsulta = sConsulta & "   SET @SQL =@SQL + 'INNER JOIN " & vbCrLf
sConsulta = sConsulta & "   (select distinct iu.item,iu.uon1,null uon2,null uon3,u.empresa " & vbCrLf
sConsulta = sConsulta & "   from item_uon1 iu WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   inner join uon1 u WITH(NOLOCK) on iu.uon1 = u.cod where IU.ANYO=@ANYO AND IU.GMN1=@GMN1 AND IU.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   Union " & vbCrLf
sConsulta = sConsulta & "   select distinct iu.item,iu.uon1,IU.uon2 ,null uon3,ISNULL(u1.empresa,u2.empresa) as empresa " & vbCrLf
sConsulta = sConsulta & "   from item_uon2 iu WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   inner join uon2 u2 WITH(NOLOCK) on iu.uon1=u2.uon1 and iu.uon2=u2.cod " & vbCrLf
sConsulta = sConsulta & "   inner join uon1 u1 WITH(NOLOCK) on u1.cod=u2.uon1 where IU.ANYO=@ANYO AND IU.GMN1=@GMN1 AND IU.PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "   Union select distinct iu.item,iu.uon1, IU.uon2 ,IU.uon3,ISNULL(u3.empresa,ISNULL(u2.empresa,u1.empresa)) as empresa " & vbCrLf
sConsulta = sConsulta & "   from item_uon3 iu WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   inner join uon3 u3 WITH(NOLOCK) on iu.uon1=u3.uon1 and iu.uon2=u3.uon2 and iu.uon3=u3.cod " & vbCrLf
sConsulta = sConsulta & "   inner join uon2 u2 WITH(NOLOCK) on u3.uon1=u2.uon1 and u3.uon2=u2.cod " & vbCrLf
sConsulta = sConsulta & "   inner join uon1 u1 WITH(NOLOCK) on u1.cod=u3.uon1 where IU.ANYO=@ANYO AND IU.GMN1=@GMN1 AND IU.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   ) IT_EMP ON IT_EMP.ITEM=IA.ITEM AND IT_EMP.EMPRESA=@IDEMP '" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   --Si los art�culos est�n vinculados a las unidades organizativas, solamente aparecen las empresas a las que pertenece el item (unidades organizativas comunes entre el art�culo y la distribuci�n del item)" & vbCrLf
sConsulta = sConsulta & "   IF @USAR_ART4_UON =1" & vbCrLf
sConsulta = sConsulta & "   begin" & vbCrLf
sConsulta = sConsulta & "      SET @SQL=@SQL + 'INNER JOIN ART4_UON WITH(NOLOCK) ON I.ART=ART4_UON.ART4 AND IT_EMP.UON1=ART4_UON.UON1 AND ISNULL(IT_EMP.UON2,ISNULL(ART4_UON.UON2,0))=ISNULL(ART4_UON.UON2,0) " & vbCrLf
sConsulta = sConsulta & "         AND ISNULL(IT_EMP.UON3,ISNULL(ART4_UON.UON3,0))=ISNULL(ART4_UON.UON3,0) AND ART4_UON.BAJALOG=0 '" & vbCrLf
sConsulta = sConsulta & "      IF  @TRASPASO_PLANIF=1" & vbCrLf
sConsulta = sConsulta & "           SET @SQL=@SQL + 'AND ART4_UON.CAMPO1 = ''1'' '" & vbCrLf
sConsulta = sConsulta & "      " & vbCrLf
sConsulta = sConsulta & "      IF @USAR_ORGCOMPRAS=1" & vbCrLf
sConsulta = sConsulta & "      begin " & vbCrLf
sConsulta = sConsulta & "          SET @SQL =@SQL + 'LEFT JOIN(" & vbCrLf
sConsulta = sConsulta & "          select distinct au.art4,au.uon1,aU.uon2,aU.uon3,ISNULL(u3.empresa,ISNULL(u2.empresa,u1.empresa)) as empresa,ISNULL(u3.CENTROS,ISNULL(u2.CENTROS,u1.CENTROS)) CENTRO" & vbCrLf
sConsulta = sConsulta & "          from art4_uon au WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "          inner join uon3 u3 WITH(NOLOCK) on au.uon1=u3.uon1 and au.uon2=u3.uon2 and au.uon3=u3.cod " & vbCrLf
sConsulta = sConsulta & "          inner join uon2 u2 WITH(NOLOCK) on u3.uon1=u2.uon1 and u3.uon2=u2.cod " & vbCrLf
sConsulta = sConsulta & "          inner join uon1 u1 WITH(NOLOCK) on u1.cod=u3.uon1" & vbCrLf
sConsulta = sConsulta & "          where au.bajalog=0 '" & vbCrLf
sConsulta = sConsulta & "          IF  @TRASPASO_PLANIF=1" & vbCrLf
sConsulta = sConsulta & "              SET @SQL =@SQL + ' and au.campo1=''1'''" & vbCrLf
sConsulta = sConsulta & "          SET @SQL =@SQL + ') IT_CEN ON I.ART=IT_CEN.ART4 AND IT_CEN.EMPRESA=@IDEMP AND IT_CEN.UON1=IT_EMP.UON1 AND IT_CEN.UON2=ISNULL(IT_EMP.uon2,IT_CEN.UON2) AND ISNULL(IT_EMP.uon3,IT_CEN.UON3) =IT_CEN.UON3 '" & vbCrLf
sConsulta = sConsulta & "      end" & vbCrLf
sConsulta = sConsulta & "   end" & vbCrLf
sConsulta = sConsulta & "  " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   IF @ESCALADOS>0 --Recoge las ofertas de la tabla de escalados" & vbCrLf
sConsulta = sConsulta & "      SET @SQL =@SQL + 'INNER JOIN ITEM_OFEESC IOF WITH (NOLOCK) ON IA.ANYO =IOF.ANYO AND IA.GMN1 =IOF.GMN1 AND IA.PROCE =IOF.PROCE AND IA.ITEM =IOF.ITEM AND IA.PROVE=IOF.PROVE AND IA.OFE =IOF.OFE AND IOF.ESC=PGE.ID '" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "      SET @SQL=@SQL + 'INNER JOIN ITEM_OFE IOF WITH (NOLOCK) ON IA.ANYO =IOF.ANYO AND IA.GMN1 =IOF.GMN1 AND IA.PROCE =IOF.PROCE AND IA.ITEM =IOF.ITEM AND IA.PROVE=IOF.PROVE AND IA.OFE =IOF.NUM '" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   ---Datos de integraci�n:" & vbCrLf
sConsulta = sConsulta & "   SET @SQL= @SQL + ' LEFT JOIN (SELECT MAX(ID) ID,LIA.PROVEPADRE,LIA.PROVE,LIA.ANYO,LIA.GMN1_PROCE,LIA.PROCE," & vbCrLf
sConsulta = sConsulta & "       LIA.ID_ITEM FROM LOG_ITEM_ADJ LIA WITH (NOLOCK) WHERE LIA.ORIGEN=0" & vbCrLf
sConsulta = sConsulta & "       GROUP BY LIA.PROVE,LIA.PROVEPADRE,LIA.ANYO,LIA.GMN1_PROCE,LIA.PROCE," & vbCrLf
sConsulta = sConsulta & "       LIA.ID_ITEM) LIA2" & vbCrLf
sConsulta = sConsulta & "       ON I.ANYO=LIA2.ANYO AND I.GMN1_PROCE=LIA2.GMN1_PROCE AND I.PROCE =LIA2.PROCE AND LIA2.PROVEPADRE=@PROVE AND LIA2.PROVE=@PROVE  AND I.ID=LIA2.ID_ITEM " & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN LOG_ITEM_ADJ LIA WITH (NOLOCK) ON LIA.ID=LIA2.ID" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN (SELECT MAX(ID) ID,LIA.PROVEPADRE,LIA.ANYO,LIA.GMN1_PROCE,LIA.PROCE," & vbCrLf
sConsulta = sConsulta & "       LIA.ID_ITEM   " & vbCrLf
sConsulta = sConsulta & "       FROM LOG_ITEM_ADJ LIA WITH (NOLOCK) WHERE LIA.ORIGEN=0 " & vbCrLf
sConsulta = sConsulta & "       GROUP BY LIA.PROVEPADRE,LIA.ANYO,LIA.GMN1_PROCE,LIA.PROCE," & vbCrLf
sConsulta = sConsulta & "       LIA.ID_ITEM) LIA1 " & vbCrLf
sConsulta = sConsulta & "   ON I.ANYO=LIA1.ANYO AND I.GMN1_PROCE=LIA1.GMN1_PROCE AND I.PROCE =LIA1.PROCE AND LIA1.PROVEPADRE=@PROVE  AND I.ID=LIA1.ID_ITEM " & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN LOG_ITEM_ADJ LIA3 WITH(NOLOCK) ON LIA3.ID=LIA1.ID " & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN LOG_GRAL LG WITH (NOLOCK) ON LG.ID_TABLA=LIA1.ID AND LG.TABLA=9" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN PROVE_ERP PE WITH (NOLOCK) ON LIA.PROVE_ERP=PE.COD_ERP AND PE.EMPRESA=@IDEMP " & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN (" & vbCrLf
sConsulta = sConsulta & "   SELECT DISTINCT PROVEP DISTRIBUIDOR FROM PROVE_REL PR WITH (NOLOCK) ' " & vbCrLf
sConsulta = sConsulta & "   SET @SQL =@SQL + ' INNER JOIN GS_PROVETREL GP WITH (NOLOCK) ON PR.PROVETREL=GP.ID AND PR.PROVEP=@PROVE AND GP.PEDIDO=1)" & vbCrLf
sConsulta = sConsulta & "    DISTR ON DISTR.DISTRIBUIDOR=@PROVE" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN INT_ATRIB IAT WITH(NOLOCK) ON IAT.ADJUDICACION=1 AND IAT.AMBITO=3 AND IAT.ERP=@ERP" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN DEF_ATRIB DA WITH (NOLOCK) ON DA.ID=IAT.ATRIB  AND IAT.ADJUDICACION=1 AND IAT.AMBITO=3 AND" & vbCrLf
sConsulta = sConsulta & "IAT.ERP=@ERP" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN ITEM_ATRIBESP IAE WITH (NOLOCK) ON IAE.ANYO=@ANYO AND IAE.GMN1=@GMN1 AND IAE.PROCE=@PROCE AND IAE.ATRIB=IAT.ATRIB AND IAE.ITEM=I.ID" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN PROCE_GRUPO_ATRIBESP PGA WITH (NOLOCK) ON PGA.ANYO=@ANYO AND PGA.GMN1=@GMN1 AND PGA.PROCE=@PROCE AND PGA.ATRIB=IAT.ATRIB AND PGA.GRUPO=@IDGRUPO" & vbCrLf
sConsulta = sConsulta & " LEFT JOIN (SELECT TOP 1 OFE,ANYO,GMN1,PROCE,PROVE FROM ITEM_ADJ WITH (NOLOCK) WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE) IAD ON IAD.ANYO=@ANYO  AND IAD.GMN1=@GMN1 AND IAD.PROCE=@PROCE AND IAD.PROVE=@PROVE   " & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN PROCE_ATRIB PA WITH (NOLOCK) ON PA.ANYO=@ANYO AND PA.GMN1=@GMN1 AND PA.PROCE=@PROCE AND PA.AMBITO=3 AND DA.ID=PA.ATRIB " & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN OFE_ITEM_ATRIB OA WITH (NOLOCK) ON OA.ANYO=@ANYO AND OA.GMN1=@GMN1 AND OA.PROCE=@PROCE AND OA.ATRIB_ID =PA.ID AND OA.PROVE=@PROVE AND OA.OFE=IAD.OFE AND OA.ITEM=I.ID" & vbCrLf
sConsulta = sConsulta & "    LEFT JOIN " & vbCrLf
sConsulta = sConsulta & "   (SELECT DA.ID INT_ATRIB_ID,LIAA.ID_LOG_ITEM_ADJ , DA.ID, DA.COD,DA.DEN," & vbCrLf
sConsulta = sConsulta & "   DA.TIPO,IA.INTRO,IA.OBLIGATORIO,LIAA.VALOR_TEXT ,LIAA.VALOR_NUM,LIAA.VALOR_FEC,LIAA.VALOR_BOOL" & vbCrLf
sConsulta = sConsulta & "   FROM INT_ATRIB IA WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN DEF_ATRIB DA WITH (NOLOCK) ON DA.ID=IA.ATRIB AND IA.ADJUDICACION=1 AND IA.AMBITO=3 AND IA.ERP=@ERP" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN LOG_ITEM_ADJ_ATRIB LIAA WITH (NOLOCK) ON LIAA.ATRIB=IA.ATRIB" & vbCrLf
sConsulta = sConsulta & "    ) IT_ATRIB1 ON IT_ATRIB1.INT_ATRIB_ID=IAT.ID AND LIA.ID=IT_ATRIB1.ID_LOG_ITEM_ADJ" & vbCrLf
sConsulta = sConsulta & "    WHERE P.ANYO=@ANYO AND P.GMN1=@GMN1 AND P.COD=@PROCE ' " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @PARAM =N'@ANYO INT, @GMN1 VARCHAR(50), @PROCE VARCHAR(50), @CODGRUPO VARCHAR(20),@PROVE VARCHAR(50),@IDEMP INT,@ERP INT,@IDGRUPO INT'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL ,@PARAM,@ANYO=@ANYO,@GMN1=@GMN1,@PROCE=@PROCE,@CODGRUPO=@CODGRUPO,@PROVE=@PROVE,@IDEMP=@IDEMP,@ERP=@ERP ,@IDGRUPO=@IDGRUPO" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


End Sub

Private Sub V_31900_8_Datos_099()
    Dim sConsulta As String
        
sConsulta = "BEGIN TRANSACTION" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE SET COD=RTRIM(COD) WHERE COD LIKE '% '" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE SET FSP_COD=RTRIM(FSP_COD) WHERE FSP_COD LIKE '% '" & vbCrLf
sConsulta = sConsulta & "UPDATE CON SET PROVE=RTRIM(PROVE) WHERE PROVE LIKE '% '" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_PROVE SET PROVE=RTRIM(PROVE) WHERE PROVE LIKE '% '" & vbCrLf
sConsulta = sConsulta & "UPDATE ART1_ADJ SET CODPROVE=RTRIM(CODPROVE) WHERE CODPROVE  LIKE '% '" & vbCrLf
sConsulta = sConsulta & "UPDATE ART2_ADJ SET CODPROVE=RTRIM(CODPROVE) WHERE CODPROVE  LIKE '% '" & vbCrLf
sConsulta = sConsulta & "UPDATE ART3_ADJ SET CODPROVE=RTRIM(CODPROVE) WHERE CODPROVE  LIKE '% '" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4_ADJ SET CODPROVE=RTRIM(CODPROVE) WHERE CODPROVE  LIKE '% '" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM SET PROVEACT=RTRIM(PROVEACT) WHERE PROVEACT LIKE '% '" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_ADJ SET PROVE=RTRIM(PROVE) WHERE PROVE LIKE '% '" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_ADJREU SET PROVE=RTRIM(PROVE) WHERE PROVE LIKE '% '" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_OFE SET PROVE=RTRIM(PROVE) WHERE PROVE LIKE '% '" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_OFE_WEB SET PROVE=RTRIM(PROVE) WHERE PROVE LIKE '% '" & vbCrLf
sConsulta = sConsulta & "UPDATE DETALLE_PUJAS SET PROVE=RTRIM(PROVE) WHERE PROVE LIKE '% '" & vbCrLf
sConsulta = sConsulta & "UPDATE OFEMIN SET PROVE=RTRIM(PROVE) WHERE PROVE LIKE '% '" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE_ADJUN SET PROVE=RTRIM(PROVE) WHERE PROVE LIKE '% '" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE SET PROVE=RTRIM(PROVE) WHERE PROVE LIKE '% '" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE_WEB_ADJUN SET PROVE=RTRIM(PROVE) WHERE PROVE LIKE '% '" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE_WEB SET PROVE=RTRIM(PROVE) WHERE PROVE LIKE '% ' " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE SET PROVE=RTRIM(PROVE) WHERE PROVE LIKE '% '" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE_PET SET PROVE=RTRIM(PROVE) WHERE PROVE LIKE '% '" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE_NO_POT SET PROVE=RTRIM(PROVE) WHERE PROVE LIKE '% '" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PEDIDO SET PROVE=RTRIM(PROVE) WHERE PROVE LIKE '% '" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_EQP SET PROVE=RTRIM(PROVE) WHERE PROVE LIKE '% '" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_GMN1 SET PROVE=RTRIM(PROVE) WHERE PROVE LIKE '% '" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_GMN2 SET PROVE=RTRIM(PROVE) WHERE PROVE LIKE '% '" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_GMN3 SET PROVE=RTRIM(PROVE) WHERE PROVE LIKE '% '" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_GMN4 SET PROVE=RTRIM(PROVE) WHERE PROVE LIKE '% '" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART1 SET PROVE=RTRIM(PROVE) WHERE PROVE LIKE '% '" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART2 SET PROVE=RTRIM(PROVE) WHERE PROVE LIKE '% '" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART3 SET PROVE=RTRIM(PROVE) WHERE PROVE LIKE '% '" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART4 SET PROVE=RTRIM(PROVE) WHERE PROVE LIKE '% '" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVENOTIF SET PROVE=RTRIM(PROVE) WHERE PROVE LIKE '% '" & vbCrLf
sConsulta = sConsulta & "UPDATE USU SET PROVE=RTRIM(PROVE) WHERE PROVE LIKE '% '" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART4_ATRIB SET PROVE=RTRIM(PROVE) WHERE PROVE LIKE '% '" & vbCrLf
sConsulta = sConsulta & "UPDATE CATALOG_LIN SET PROVE=RTRIM(PROVE) WHERE PROVE LIKE '% '" & vbCrLf
sConsulta = sConsulta & "UPDATE ORDEN_ENTREGA SET PROVE=RTRIM(PROVE) WHERE PROVE LIKE '% '" & vbCrLf
sConsulta = sConsulta & "UPDATE LINEAS_PEDIDO SET PROVE=RTRIM(PROVE) WHERE PROVE LIKE '% '" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART_UP SET PROVE=RTRIM(PROVE) WHERE PROVE LIKE '% '" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ESP SET PROVE=RTRIM(PROVE) WHERE PROVE LIKE '% '" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ESP_PORTAL SET PROVE=RTRIM(PROVE) WHERE PROVE LIKE '% '" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET PROVEACT=RTRIM(PROVEACT) WHERE PROVEACT LIKE '% '" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GRUPO SET PROVEACT=RTRIM(PROVEACT) WHERE PROVEACT LIKE '% '" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ALL_PROVE SET PROVE=RTRIM(PROVE) WHERE PROVE LIKE '% '" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_GRUPO_PROVE SET PROVE=RTRIM(PROVE) WHERE PROVE LIKE '% '" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ITEM_PROVE SET PROVE=RTRIM(PROVE) WHERE PROVE LIKE '% '" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_PROCE_PROVE SET PROVE=RTRIM(PROVE) WHERE PROVE LIKE '% '" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_ATRIB SET PROVE=RTRIM(PROVE) WHERE PROVE LIKE '% '" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_ITEM_ATRIB SET PROVE=RTRIM(PROVE) WHERE PROVE LIKE '% '" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_GR_ATRIB SET PROVE=RTRIM(PROVE) WHERE PROVE LIKE '% '" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_ITEM_ADJUN SET PROVE=RTRIM(PROVE) WHERE PROVE LIKE '% '" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_GR_ADJUN SET PROVE=RTRIM(PROVE) WHERE PROVE LIKE '% '" & vbCrLf
sConsulta = sConsulta & "UPDATE GRUPO_OFE SET PROVE=RTRIM(PROVE) WHERE PROVE LIKE '% '" & vbCrLf
sConsulta = sConsulta & "UPDATE GRUPO_OFE_REU SET PROVE=RTRIM(PROVE) WHERE PROVE LIKE '% '" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_PROVE SET PROVE=RTRIM(PROVE) WHERE PROVE LIKE '% '" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_PROVE SET PROVE=RTRIM(PROVE) WHERE PROVE LIKE '% '" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_CONTR SET PROVE=RTRIM(PROVE) WHERE PROVE LIKE '% '" & vbCrLf
sConsulta = sConsulta & "--2.12" & vbCrLf
sConsulta = sConsulta & "UPDATE CATN1_PROVE  SET PROVE=RTRIM(PROVE) WHERE PROVE LIKE '% '" & vbCrLf
sConsulta = sConsulta & "UPDATE CATN2_PROVE  SET PROVE=RTRIM(PROVE) WHERE PROVE LIKE '% '" & vbCrLf
sConsulta = sConsulta & "UPDATE CATN3_PROVE  SET PROVE=RTRIM(PROVE) WHERE PROVE LIKE '% '" & vbCrLf
sConsulta = sConsulta & "UPDATE CATN4_PROVE  SET PROVE=RTRIM(PROVE) WHERE PROVE LIKE '% '" & vbCrLf
sConsulta = sConsulta & "UPDATE CATN5_PROVE  SET PROVE=RTRIM(PROVE) WHERE PROVE LIKE '% '" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART4_ADJUN  SET PROVE=RTRIM(PROVE) WHERE PROVE LIKE '% '" & vbCrLf
sConsulta = sConsulta & "UPDATE FAVORITOS_ORDEN_ENTREGA  SET PROVE=RTRIM(PROVE) WHERE PROVE LIKE '% '" & vbCrLf
sConsulta = sConsulta & "UPDATE FAVORITOS_LINEAS_PEDIDO  SET PROVE=RTRIM(PROVE) WHERE PROVE LIKE '% '" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE_GRUPOS SET PROVE=RTRIM(PROVE) WHERE PROVE LIKE '% '" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_EMPRESA_COD_ERP SET PROVE=RTRIM(PROVE) WHERE PROVE LIKE '% '" & vbCrLf
sConsulta = sConsulta & "UPDATE FORM_CAMPO SET VALOR_TEXT=RTRIM(VALOR_TEXT) WHERE VALOR_TEXT LIKE '% ' AND TIPO_CAMPO_GS=100" & vbCrLf
sConsulta = sConsulta & "UPDATE LINEA_DESGLOSE SET VALOR_TEXT=RTRIM(VALOR_TEXT) WHERE VALOR_TEXT LIKE '% ' AND CAMPO_HIJO IN (SELECT ID FROM FORM_CAMPO WITH (NOLOCK) WHERE ES_SUBCAMPO=1 AND TIPO_CAMPO_GS=100)" & vbCrLf
sConsulta = sConsulta & "UPDATE COPIA_CAMPO SET VALOR_TEXT=RTRIM(VALOR_TEXT) WHERE VALOR_TEXT LIKE '% ' AND COPIA_CAMPO_DEF IN (SELECT ID FROM COPIA_CAMPO_DEF WITH (NOLOCK) WHERE TIPO_CAMPO_GS=100)" & vbCrLf
sConsulta = sConsulta & "UPDATE COPIA_LINEA_DESGLOSE SET VALOR_TEXT=RTRIM(VALOR_TEXT) WHERE VALOR_TEXT LIKE '% ' AND CAMPO_HIJO IN (SELECT COPIA_CAMPO.ID FROM COPIA_CAMPO WITH (NOLOCK) INNER JOIN COPIA_CAMPO_DEF WITH (NOLOCK) ON COPIA_CAMPO.COPIA_CAMPO_DEF=COPIA_CAMPO_DEF.ID WHERE ES_SUBCAMPO=1 AND TIPO_CAMPO_GS=100)" & vbCrLf
sConsulta = sConsulta & "UPDATE VERSION_INSTANCIA  SET PROVE=RTRIM(PROVE) WHERE PROVE LIKE '% '" & vbCrLf
sConsulta = sConsulta & "UPDATE CERTIFICADO  SET PROVE=RTRIM(PROVE) WHERE PROVE LIKE '% '" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_CERTIFICADO  SET PROVE=RTRIM(PROVE) WHERE PROVE LIKE '% '" & vbCrLf
sConsulta = sConsulta & "UPDATE INSTANCIA_EST  SET DESTINATARIO_PROV=RTRIM(DESTINATARIO_PROV) WHERE DESTINATARIO_PROV LIKE '% '" & vbCrLf
sConsulta = sConsulta & "UPDATE NOCONFORMIDAD  SET PROVE=RTRIM(PROVE) WHERE PROVE LIKE '% '" & vbCrLf
sConsulta = sConsulta & "UPDATE PM_ROL  SET PROVE=RTRIM(PROVE) WHERE PROVE LIKE '% '" & vbCrLf
sConsulta = sConsulta & "UPDATE PM_COPIA_ROL  SET PROVE=RTRIM(PROVE) WHERE PROVE LIKE '% '" & vbCrLf
sConsulta = sConsulta & "UPDATE INSTANCIA_EST  SET PROVE=RTRIM(PROVE) WHERE PROVE LIKE '% '" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_LEIDO  SET PROVE=RTRIM(PROVE) WHERE PROVE LIKE '% '" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_FAVORITOS  SET PROVE=RTRIM(PROVE) WHERE PROVE LIKE '% '" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_UNQA  SET PROVE=RTRIM(PROVE) WHERE PROVE LIKE '% '" & vbCrLf
sConsulta = sConsulta & "UPDATE CESTA_CABECERA SET PROVE=RTRIM(PROVE) WHERE PROVE LIKE '% '" & vbCrLf
sConsulta = sConsulta & "UPDATE CESTA SET PROVE=RTRIM(PROVE) WHERE PROVE LIKE '% '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE VAR_PROVE_UNQA_PUNT  SET PROVE=RTRIM(PROVE) WHERE PROVE LIKE '% '" & vbCrLf
sConsulta = sConsulta & "UPDATE VAR_PROVE_UNQA_X  SET PROVE=RTRIM(PROVE) WHERE PROVE LIKE '% '" & vbCrLf
sConsulta = sConsulta & "UPDATE VAR_PROVE_UNQA_PUNT_HIST  SET PROVE=RTRIM(PROVE) WHERE PROVE LIKE '% '" & vbCrLf
sConsulta = sConsulta & "UPDATE VAR_PROVE_UNQA_X_HIST  SET PROVE=RTRIM(PROVE) WHERE PROVE LIKE '% '" & vbCrLf
sConsulta = sConsulta & "UPDATE VAR_CAL_INT  SET PROVE=RTRIM(PROVE) WHERE PROVE LIKE '% '" & vbCrLf
sConsulta = sConsulta & "UPDATE VAR_CAL_MANUAL  SET PROVE=RTRIM(PROVE) WHERE PROVE LIKE '% '" & vbCrLf
sConsulta = sConsulta & "UPDATE PPM  SET PROVE=RTRIM(PROVE) WHERE PROVE LIKE '% '" & vbCrLf
sConsulta = sConsulta & "UPDATE CARGO_PROVEEDORES  SET PROVE=RTRIM(PROVE) WHERE PROVE LIKE '% '" & vbCrLf
sConsulta = sConsulta & "UPDATE TASA_SERVICIO  SET PROVE=RTRIM(PROVE) WHERE PROVE LIKE '% '" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_BANCA  SET PROVE=RTRIM(PROVE) WHERE PROVE LIKE '% '" & vbCrLf
sConsulta = sConsulta & "UPDATE FACTURA SET PROVE=RTRIM(PROVE) WHERE PROVE LIKE '% '" & vbCrLf
sConsulta = sConsulta & "UPDATE REGISTRO_EMAIL SET PROVE=RTRIM(PROVE) WHERE PROVE LIKE '% '" & vbCrLf
sConsulta = sConsulta & "UPDATE NOCONF_CONPROVE SET PROVE=RTRIM(PROVE) WHERE PROVE LIKE '% '" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_GR_COSTESDESC SET PROVE=RTRIM(PROVE) WHERE PROVE LIKE '% '" & vbCrLf
sConsulta = sConsulta & "UPDATE CONTRATO SET PROVE=RTRIM(PROVE) WHERE PROVE LIKE '% '" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_PUNT_ERRORES SET PROVE=RTRIM(PROVE) WHERE PROVE LIKE '% '" & vbCrLf
sConsulta = sConsulta & "UPDATE FSGA_H_CAT_PROVE SET PROVE=RTRIM(PROVE) WHERE PROVE LIKE '% '" & vbCrLf
sConsulta = sConsulta & "UPDATE FSGA_CAT_PROVE SET PROVE=RTRIM(PROVE) WHERE PROVE LIKE '% '" & vbCrLf
sConsulta = sConsulta & "UPDATE IMPUESTOS_PROVE SET PROVE=RTRIM(PROVE) WHERE PROVE LIKE '% '" & vbCrLf
sConsulta = sConsulta & "--CN" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_CON_NOTIF SET PROVE=RTRIM(PROVE) WHERE PROVE LIKE '% '" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_MEN_CON SET PROVE=RTRIM(PROVE) WHERE PROVE LIKE '% '" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_RESP SET PROVE=RTRIM(PROVE) WHERE PROVE LIKE '% '" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_RESP SET PROVECITADO=RTRIM(PROVECITADO) WHERE PROVECITADO LIKE '% '" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_USU SET PROVE=RTRIM(PROVE) WHERE PROVE LIKE '% '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "COMMIT TRANSACTION" & vbCrLf

ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion31900_08_1899_A31900_08_18991() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim oADOConnection As ADODB.Connection
    Dim sConsulta As String

    On Error GoTo Error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    
    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Actualiza Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    frmProgreso.ProgressBar1.Refresh
    V_31900_8_Storeds_0991

    
    sConsulta = "UPDATE VERSION SET NUM ='3.00.18.991'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103), PRODVER='31900.8'"
    ExecuteSQL gRDOCon, sConsulta
        
    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False
        
    CodigoDeActualizacion31900_08_1899_A31900_08_18991 = True
    Exit Function

Error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    CodigoDeActualizacion31900_08_1899_A31900_08_18991 = False
End Function

Public Sub V_31900_8_Storeds_0991()
Dim sConsulta As String
        
sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSWS_ART_GET_ULT_ADJ]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSWS_ART_GET_ULT_ADJ]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSWS_ART_GET_ULT_ADJ]" & vbCrLf
sConsulta = sConsulta & "   @ART NVARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "   @CODORGCOMPRAS NVARCHAR(50)=NULL," & vbCrLf
sConsulta = sConsulta & "   @CODCENTRO NVARCHAR(50)=NULL " & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PARAM AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--ID DEL REGISTRO QUE CONTIENE LA �LTIMA ADJUDICACI�N" & vbCrLf
sConsulta = sConsulta & "DECLARE @GENERICO TINYINT" & vbCrLf
sConsulta = sConsulta & "SELECT @GENERICO=GENERICO FROM ART4 WITH(NOLOCK) WHERE COD = @ART" & vbCrLf
sConsulta = sConsulta & "IF @GENERICO = 0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   DECLARE @CAMPO1 TINYINT=1 --inicializamos a 1 por si la ORGCOMPRAS o el CENTROS no coinciden con el del art�culo" & vbCrLf
sConsulta = sConsulta & "   SET @SQL= N' SELECT @CAMPO1=CAMPO1 FROM ART4_UON AU WITH(NOLOCK)'" & vbCrLf
sConsulta = sConsulta & "   IF @CODORGCOMPRAS <> NULL OR @CODCENTRO <> NULL" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       SET @SQL= @SQL +' INNER JOIN UON1 U1 WITH (NOLOCK) on U1.COD=AU.UON1'" & vbCrLf
sConsulta = sConsulta & "       SET @SQL= @SQL +' INNER JOIN UON2 U2 WITH (NOLOCK) on U2.COD=AU.UON2 and U2.UON1=AU.UON1'" & vbCrLf
sConsulta = sConsulta & "       SET @SQL= @SQL +' INNER JOIN UON3 U3 WITH (NOLOCK) on U3.COD=AU.UON3 and U3.UON2=AU.UON2 and U3.UON1=AU.UON1'" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "   SET @SQL= @SQL +' WHERE AU.ART4=@ART AND AU.BAJALOG=0'" & vbCrLf
sConsulta = sConsulta & "   IF @CODORGCOMPRAS <> NULL" & vbCrLf
sConsulta = sConsulta & "       SET @SQL= @SQL +' AND ISNULL(U3.ORGCOMPRAS,ISNULL(U2.ORGCOMPRAS,U1.ORGCOMPRAS))=@CODORGCOMPRAS'" & vbCrLf
sConsulta = sConsulta & "   IF @CODORGCOMPRAS <> NULL" & vbCrLf
sConsulta = sConsulta & "       SET @SQL= @SQL +' AND ISNULL(U3.CENTROS,ISNULL(U2.CENTROS,U1.CENTROS))=@CODCENTRO'" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "   SET @PARAM = N' @ART NVARCHAR(100), @CODORGCOMPRAS NVARCHAR(50), @CODCENTRO NVARCHAR(50), @CAMPO1 TINYINT OUTPUT'" & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL, @PARAM, @ART=@ART, @CODORGCOMPRAS=@CODORGCOMPRAS, @CODCENTRO=@CODCENTRO,@CAMPO1=@CAMPO1 OUTPUT" & vbCrLf
sConsulta = sConsulta & "   IF @CAMPO1=0" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       DECLARE @ID INTEGER = 0" & vbCrLf
sConsulta = sConsulta & "       SELECT @ID = MAX(ID) FROM ART4_ADJ AA WITH(NOLOCK) WHERE AA.ART = @ART AND AA.FECINI<=GETDATE() AND AA.FECFIN>=GETDATE()" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       SELECT DISTINCT PC.ANYO, PC.GMN1, PC.COD, PC.DEN, A4.CODPROVE, A4.DENPROVE" & vbCrLf
sConsulta = sConsulta & "       FROM ART4_ADJ A4 WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PROCE PC WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "          ON PC.ANYO=A4.ANYO AND PC.GMN1=A4.GMN1 AND PC.COD=A4.PROCE" & vbCrLf
sConsulta = sConsulta & "       WHERE " & vbCrLf
sConsulta = sConsulta & "          A4.ID = @ID AND A4.ART = @ART" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


End Sub

Public Function CodigoDeActualizacion31900_08_18991_A31900_08_18992() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim oADOConnection As ADODB.Connection
    Dim sConsulta As String

    On Error GoTo Error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    
    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Actualiza Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    frmProgreso.ProgressBar1.Refresh
    V_31900_8_Storeds_0992

    
    sConsulta = "UPDATE VERSION SET NUM ='3.00.18.992'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103), PRODVER='31900.8'"
    ExecuteSQL gRDOCon, sConsulta
        
    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False
        
    CodigoDeActualizacion31900_08_18991_A31900_08_18992 = True
    Exit Function

Error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    CodigoDeActualizacion31900_08_18991_A31900_08_18992 = False
End Function

Public Sub V_31900_8_Storeds_0992()
Dim sConsulta As String
        
sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSGSW_TRASPASAR_ERP]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSGSW_TRASPASAR_ERP]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSGSW_TRASPASAR_ERP] @ANYO INT, @GMN1 VARCHAR(50), @PROCE VARCHAR(50),@ID_ITEM INT,@IDEMP INT,@PROVE_PADRE NVARCHAR(30),@PROVE VARCHAR(50)," & vbCrLf
sConsulta = sConsulta & "                                            @CANT_ADJ FLOAT,@CENTRO NVARCHAR(30),@PROVE_ERP NVARCHAR(30),@CODUSU NVARCHAR(10) AS       " & vbCrLf
sConsulta = sConsulta & "                                            " & vbCrLf
sConsulta = sConsulta & "                                            " & vbCrLf
sConsulta = sConsulta & "INSERT INTO LOG_ITEM_ADJ (ID_ITEM,ART,DESCR,DEST,UNI,PAG,CANT,CANT_ADJ,PREC_VALIDO_ADJ,PREC_ADJ,PRES,FECINI,FECFIN,OBJ,FECOBJ,ESP,SOLICIT,LINEA_SOLICIT,ORIGEN,USU,GMN1,GMN2,GMN3,GMN4," & vbCrLf
sConsulta = sConsulta & "ACCION,ANYO,PROCE,GMN1_PROCE,DEN_PROCE,ADJDIR,FECNEC,FECAPE,FECVAL,FECPRES,FECPROXREU,FECULTREU,REUDEC,NUM_OFE,MON_PROCE,CAMBIO_PROCE,MON_OFE,CAMBIO_OFE,SOLICITUD,FECLIMOFE" & vbCrLf
sConsulta = sConsulta & ",USUAPER,COM,EQP,EMPRESA,PROVE_ERP,FECCIERRE,ERP, PROVE,PROVEPADRE,PORCEN,FECINI_PROCE,FECFIN_PROCE,CENTRO ) " & vbCrLf
sConsulta = sConsulta & "SELECT @ID_ITEM,I.ART,I.DESCR,I.DEST,I.UNI,I.PAG,I.CANT,@CANT_ADJ,IO.PREC_VALIDO " & vbCrLf
sConsulta = sConsulta & ",CASE USAR WHEN 1 THEN PRECIO WHEN 2 THEN PRECIO2 WHEN 3 THEN PRECIO3 END,I.PREC AS PRES,I.FECINI,I.FECFIN,I.OBJ,I.FECHAOBJ,I.ESP,I.SOLICIT,I.LINEA_SOLICIT," & vbCrLf
sConsulta = sConsulta & "'0',@CODUSU,ISNULL(I.GMN1,ART4.GMN1),ISNULL(I.GMN2,ART4.GMN2),ISNULL(I.GMN3,ART4.GMN3),ISNULL(I.GMN4,ART4.GMN4)," & vbCrLf
sConsulta = sConsulta & "'I',@ANYO ,@PROCE ,@GMN1,PROCE.DEN,ADJDIR,FECNEC,FECAPE,PROCE.FECVAL,FECPRES,FECPROXREU,I.FECULTREU,REUDEC,PROCE_OFE.OFE,PROCE.MON,PROCE.CAMBIO,PROCE_OFE.MON,PROCE_OFE.CAMBIO,SOLICITUD,FECLIMOFE" & vbCrLf
sConsulta = sConsulta & " ,USUAPER,PROCE_PROVE.COM,PROCE_PROVE.EQP,@IDEMP,@PROVE_ERP ,PROCE.FECCIERRE," & vbCrLf
sConsulta = sConsulta & " ERP_SOCIEDAD.ERP,@PROVE,@PROVE_PADRE ,(@CANT_ADJ/I.CANT),PROCE.FECINI,PROCE.FECFIN,@CENTRO  " & vbCrLf
sConsulta = sConsulta & " FROM PROCE WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & " INNER JOIN PROCE_PROVE WITH (NOLOCK) ON PROCE_PROVE.ANYO = PROCE.ANYO AND PROCE_PROVE.GMN1 = PROCE.GMN1 AND PROCE_PROVE.PROCE = PROCE.COD AND PROCE_PROVE.PROVE =@PROVE_PADRE" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PROCE_OFE WITH (NOLOCK) ON PROCE_OFE.ANYO = PROCE.ANYO AND PROCE_OFE.GMN1 = PROCE.GMN1 AND PROCE_OFE.PROCE = PROCE.COD AND PROCE_OFE.PROVE =@PROVE_PADRE AND PROCE_OFE.ULT=1" & vbCrLf
sConsulta = sConsulta & "INNER JOIN EMP WITH (NOLOCK) ON EMP.ID = @IDEMP" & vbCrLf
sConsulta = sConsulta & " LEFT JOIN ERP_SOCIEDAD WITH (NOLOCK) ON ERP_SOCIEDAD.SOCIEDAD = EMP.SOCIEDAD" & vbCrLf
sConsulta = sConsulta & "INNER JOIN  ITEM I WITH (NOLOCK) ON I.ANYO=PROCE.ANYO AND I.GMN1_PROCE =PROCE.GMN1 AND PROCE.COD=I.PROCE AND I.ID=@ID_ITEM " & vbCrLf
sConsulta = sConsulta & "INNER JOIN ITEM_OFE IO WITH(NOLOCK) ON I.ANYO=IO.ANYO AND I.GMN1_PROCE=IO.GMN1 AND I.PROCE=IO.PROCE AND I.ID=IO.ITEM AND IO.PROVE=@PROVE_PADRE AND IO.ULT=1" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN ART4 WITH(NOLOCK) ON ART4.COD=I.ART" & vbCrLf
sConsulta = sConsulta & "WHERE I.ANYO = @ANYO  AND I.GMN1_PROCE=@GMN1 AND I.PROCE=@PROCE AND I.ID=@ID_ITEM" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSGSW_DEVOLVER_ITEMSGRUPO]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSGSW_DEVOLVER_ITEMSGRUPO]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta



sConsulta = "CREATE PROCEDURE [dbo].[FSGSW_DEVOLVER_ITEMSGRUPO] @ANYO INT, @GMN1 VARCHAR(50), @PROCE INT, @CODGRUPO VARCHAR(20),@PROVE VARCHAR(50),@IDEMP INT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @EST INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PARAM AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @ERP INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ESCALADOS INT=0" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDGRUPO INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @USAR_ART4_UON INT=0" & vbCrLf
sConsulta = sConsulta & "DECLARE @TRASPASO_PLANIF INT=0" & vbCrLf
sConsulta = sConsulta & "DECLARE @USAR_ORGCOMPRAS INT =0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @ESCALADOS=COUNT(ID) FROM PROCE_GRUPO PG WITH (NOLOCK) WHERE PG.ANYO=@ANYO  AND PG.GMN1=@GMN1 AND PG.PROCE=@PROCE AND PG.COD=@CODGRUPO AND PG.ESCALADOS=1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @EST=EST FROM PROCE P WITH (NOLOCK) WHERE ANYO =@ANYO AND COD=@PROCE AND GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @ERP=ES.ERP FROM ERP_SOCIEDAD ES WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN EMP E WITH (NOLOCK) ON E.SOCIEDAD=ES.SOCIEDAD AND E.ID=@IDEMP" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "SELECT @IDGRUPO=ID FROM PROCE_GRUPO PG WITH (NOLOCK) WHERE PG.ANYO=@ANYO AND PG.GMN1=@GMN1 AND PG.PROCE=@PROCE AND PG.COD=@CODGRUPO " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @USAR_ART4_UON=USAR_ART4_UON, @TRASPASO_PLANIF=TRASPASO_ART_PLANIF, @USAR_ORGCOMPRAS=USAR_ORGCOMPRAS  FROM PARGEN_INTERNO WITH(NOLOCK) WHERE ID=1" & vbCrLf
sConsulta = sConsulta & "    " & vbCrLf
sConsulta = sConsulta & "   SET @SQL='SELECT DISTINCT COALESCE(LIA1.ID,LIA.ID) LIA_ID,I.ID ID_ITEM,I.ART CODART,I.DESCR DENART,I.UNI,'" & vbCrLf
sConsulta = sConsulta & "   if  @ESCALADOS>0 " & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + ' PGE.ID ESC_ID,PGE.INICIO,PGE.FIN,'" & vbCrLf
sConsulta = sConsulta & "   else" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + 'IA.CANT_ADJ*IT_EMP.PORCEN CANT_ADJ,'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + ' IOF.PREC_VALIDO PREC_ADJ,P.MON,'" & vbCrLf
sConsulta = sConsulta & "   IF @USAR_ORGCOMPRAS =1  --Si el �ltimo traspaso es un borrado, lee lo que haya en el proceso. Si no (era una I), lee lo de log_item_adj" & vbCrLf
sConsulta = sConsulta & "      SET @SQL=@SQL + ' CASE WHEN LIA3.ACCION=''D'' THEN IT_CEN.CENTRO ELSE COALESCE(it_cen.CENTRO,LIA.CENTRO,'''') end CENTROS,'''' AS CENTRO,'" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "      SET @SQL=@SQL + ''''' CENTROS,'''' AS CENTRO,'" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + ' CASE WHEN LIA3.ACCION = ''D'' AND LG.ESTADO=4  THEN NULL ELSE LG.ESTADO END AS ESTADOINT,CASE WHEN LIA3.ACCION = ''D'' AND LG.ESTADO=4 THEN NULL ELSE LG.FECENV END FECENV,CASE WHEN LIA3.ACCION = ''D'' AND LG.ESTADO=4 THEN NULL ELSE LIA3.USU  END USU " & vbCrLf
sConsulta = sConsulta & "   , I.FECINI FECINI_SUM,I.FECFIN FECFIN_SUM,LIA.PROVE_ERP,PE.DEN_ERP," & vbCrLf
sConsulta = sConsulta & "   COALESCE(IT_ATRIB1.INT_ATRIB_ID,IAE.ATRIB,PGA.ATRIB,PATR.ATRIB,PA.ATRIB,DA.ID) ATRIBID," & vbCrLf
sConsulta = sConsulta & "   DA.COD ATRIBCOD,DA.DEN ATRIBDEN," & vbCrLf
sConsulta = sConsulta & "   COALESCE(IT_ATRIB1.VALOR_TEXT,IAE.VALOR_TEXT,PGA.VALOR_TEXT,PATR.VALOR_TEXT,OA.VALOR_TEXT,OGATR.VALOR_TEXT,OATR.VALOR_TEXT,IAT.VALOR_TEXT) ATRIBVALOR_TEXT ," & vbCrLf
sConsulta = sConsulta & "   COALESCE(IT_ATRIB1.VALOR_NUM ,IAE.VALOR_NUM,PGA.VALOR_NUM,PATR.VALOR_NUM,OA.VALOR_NUM,OGATR.VALOR_NUM,OATR.VALOR_NUM,IAT.VALOR_NUM) ATRIBVALOR_NUM," & vbCrLf
sConsulta = sConsulta & "   COALESCE(IT_ATRIB1.VALOR_FEC ,IAE.VALOR_FEC,PGA.VALOR_FEC,PATR.VALOR_FEC,OA.VALOR_FEC,OGATR.VALOR_FEC,OATR.VALOR_FEC,IAT.VALOR_FEC) ATRIBVALOR_FEC," & vbCrLf
sConsulta = sConsulta & "   COALESCE(IT_ATRIB1.VALOR_BOOL,IAE.VALOR_BOOL,PGA.VALOR_BOOL,PATR.VALOR_BOOL,OA.VALOR_BOOL,OGATR.VALOR_BOOL,OATR.VALOR_BOOL,IAT.VALOR_BOOL) ATRIBVALOR_BOOL," & vbCrLf
sConsulta = sConsulta & "   DA.TIPO ATRIBTIPO,IAT.OBLIGATORIO ATRIBOBLIGATORIO,'" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   IF @USAR_ART4_UON  =1 " & vbCrLf
sConsulta = sConsulta & "      SET @SQL=@SQL + 'CASE WHEN IT_EMP.UON1 IS not NULL THEN IT_EMP.UON1 ELSE ART4_UON.UON1 END UON1," & vbCrLf
sConsulta = sConsulta & "      CASE WHEN IT_EMP.UON2 IS not NULL THEN IT_EMP.UON2 ELSE ART4_UON.UON2 END UON2," & vbCrLf
sConsulta = sConsulta & "      CASE WHEN IT_EMP.UON3 IS not NULL THEN IT_EMP.UON3 ELSE ART4_UON.UON3 END UON3 '" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "      SET @SQL=@SQL + 'IT_EMP.UON1 UON1,IT_EMP.UON2 UON2,IT_EMP.UON3 UON3 '" & vbCrLf
sConsulta = sConsulta & "      " & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + ',CASE WHEN DISTR.DISTRIBUIDOR IS NULL THEN 0 ELSE 1 END DISTRIBUIDOR,'" & vbCrLf
sConsulta = sConsulta & "   IF @ESCALADOS>0 " & vbCrLf
sConsulta = sConsulta & "      SET @SQL=@SQL + '1 ESCALADOS,PG.ESCALAUNI '" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "      SET @SQL=@SQL + '0 ESCALADOS '" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   --------FROM--------------------------------------------" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + 'FROM PROCE P WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PROCE_GRUPO PG WITH (NOLOCK) ON PG.ANYO=P.ANYO AND PG.GMN1=P.GMN1 AND PG.PROCE=P.COD AND PG.COD=@CODGRUPO " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN ITEM I WITH (NOLOCK) ON I.ANYO=P.ANYO AND I.GMN1_PROCE=P.GMN1 AND I.PROCE=P.COD AND I.GRUPO=PG.ID '" & vbCrLf
sConsulta = sConsulta & "   IF @EST=11 --Si el proceso esta parcialmente cerrado" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL + ' AND I.EST=1 '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @ESCALADOS>0 --Recoge los datos de adjudicaciones de las tablas de escalados" & vbCrLf
sConsulta = sConsulta & "      SET @SQL =@SQL + 'INNER JOIN ITEM_ADJESC IA WITH (NOLOCK) ON IA.ANYO=I.ANYO AND IA.GMN1=I.GMN1_PROCE AND IA.PROCE=I.PROCE AND IA.ITEM=I.ID AND IA.PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PROCE_GR_ESC PGE WITH (NOLOCK) ON PGE.ANYO=I.ANYO AND PGE.GMN1=I.GMN1_PROCE AND PGE.PROCE=I.PROCE AND PGE.GRUPO =PG.ID --AND PGE.ID=IA.ESC '" & vbCrLf
sConsulta = sConsulta & "   ELSE -- Recoge la adjudicaci�n" & vbCrLf
sConsulta = sConsulta & "      SET @SQL =@SQL + 'INNER JOIN ITEM_ADJ IA WITH (NOLOCK) ON I.ANYO=IA.ANYO AND I.GMN1_PROCE=IA.GMN1 AND I.PROCE=IA.PROCE AND I.ID=IA.ITEM AND IA.PROVE=@PROVE '" & vbCrLf
sConsulta = sConsulta & "    " & vbCrLf
sConsulta = sConsulta & "    ---Obtiene las empresas a las que se ha distribuido la compra" & vbCrLf
sConsulta = sConsulta & "   SET @SQL =@SQL + 'INNER JOIN " & vbCrLf
sConsulta = sConsulta & "   (select distinct iu.item,iu.uon1,null uon2,null uon3,u.empresa,iu.porcen " & vbCrLf
sConsulta = sConsulta & "   from item_uon1 iu WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   inner join uon1 u WITH(NOLOCK) on iu.uon1 = u.cod where IU.ANYO=@ANYO AND IU.GMN1=@GMN1 AND IU.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   Union " & vbCrLf
sConsulta = sConsulta & "   select distinct iu.item,iu.uon1,IU.uon2 ,null uon3,ISNULL(u1.empresa,u2.empresa) as empresa,iu.porcen " & vbCrLf
sConsulta = sConsulta & "   from item_uon2 iu WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   inner join uon2 u2 WITH(NOLOCK) on iu.uon1=u2.uon1 and iu.uon2=u2.cod " & vbCrLf
sConsulta = sConsulta & "   inner join uon1 u1 WITH(NOLOCK) on u1.cod=u2.uon1 where IU.ANYO=@ANYO AND IU.GMN1=@GMN1 AND IU.PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "   Union select distinct iu.item,iu.uon1, IU.uon2 ,IU.uon3,ISNULL(u3.empresa,ISNULL(u2.empresa,u1.empresa)) as empresa,iu.porcen " & vbCrLf
sConsulta = sConsulta & "   from item_uon3 iu WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   inner join uon3 u3 WITH(NOLOCK) on iu.uon1=u3.uon1 and iu.uon2=u3.uon2 and iu.uon3=u3.cod " & vbCrLf
sConsulta = sConsulta & "   inner join uon2 u2 WITH(NOLOCK) on u3.uon1=u2.uon1 and u3.uon2=u2.cod " & vbCrLf
sConsulta = sConsulta & "   inner join uon1 u1 WITH(NOLOCK) on u1.cod=u3.uon1 where IU.ANYO=@ANYO AND IU.GMN1=@GMN1 AND IU.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   ) IT_EMP ON IT_EMP.ITEM=IA.ITEM AND IT_EMP.EMPRESA=@IDEMP '" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   --Si los art�culos est�n vinculados a las unidades organizativas, solamente aparecen las empresas a las que pertenece el item (unidades organizativas comunes entre el art�culo y la distribuci�n del item)" & vbCrLf
sConsulta = sConsulta & "   IF @USAR_ART4_UON =1" & vbCrLf
sConsulta = sConsulta & "   begin" & vbCrLf
sConsulta = sConsulta & "      SET @SQL=@SQL + 'INNER JOIN ART4_UON WITH(NOLOCK) ON I.ART=ART4_UON.ART4 AND IT_EMP.UON1=ART4_UON.UON1 AND ISNULL(IT_EMP.UON2,ISNULL(ART4_UON.UON2,0))=ISNULL(ART4_UON.UON2,0) " & vbCrLf
sConsulta = sConsulta & "         AND ISNULL(IT_EMP.UON3,ISNULL(ART4_UON.UON3,0))=ISNULL(ART4_UON.UON3,0) AND ART4_UON.BAJALOG=0 '" & vbCrLf
sConsulta = sConsulta & "      IF  @TRASPASO_PLANIF=1" & vbCrLf
sConsulta = sConsulta & "           SET @SQL=@SQL + 'AND ART4_UON.CAMPO1 = ''1'' '" & vbCrLf
sConsulta = sConsulta & "      " & vbCrLf
sConsulta = sConsulta & "      IF @USAR_ORGCOMPRAS=1" & vbCrLf
sConsulta = sConsulta & "      begin " & vbCrLf
sConsulta = sConsulta & "          SET @SQL =@SQL + 'LEFT JOIN(" & vbCrLf
sConsulta = sConsulta & "          select distinct au.art4,au.uon1,aU.uon2,aU.uon3,ISNULL(u3.empresa,ISNULL(u2.empresa,u1.empresa)) as empresa,ISNULL(u3.CENTROS,ISNULL(u2.CENTROS,u1.CENTROS)) CENTRO" & vbCrLf
sConsulta = sConsulta & "          from art4_uon au WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "          inner join uon3 u3 WITH(NOLOCK) on au.uon1=u3.uon1 and au.uon2=u3.uon2 and au.uon3=u3.cod " & vbCrLf
sConsulta = sConsulta & "          inner join uon2 u2 WITH(NOLOCK) on u3.uon1=u2.uon1 and u3.uon2=u2.cod " & vbCrLf
sConsulta = sConsulta & "          inner join uon1 u1 WITH(NOLOCK) on u1.cod=u3.uon1" & vbCrLf
sConsulta = sConsulta & "          where au.bajalog=0 '" & vbCrLf
sConsulta = sConsulta & "          IF  @TRASPASO_PLANIF=1" & vbCrLf
sConsulta = sConsulta & "              SET @SQL =@SQL + ' and au.campo1=''1'''" & vbCrLf
sConsulta = sConsulta & "          SET @SQL =@SQL + ') IT_CEN ON I.ART=IT_CEN.ART4 AND IT_CEN.EMPRESA=@IDEMP AND IT_CEN.UON1=IT_EMP.UON1 AND IT_CEN.UON2=ISNULL(IT_EMP.uon2,IT_CEN.UON2) AND ISNULL(IT_EMP.uon3,IT_CEN.UON3) =IT_CEN.UON3 '" & vbCrLf
sConsulta = sConsulta & "      end" & vbCrLf
sConsulta = sConsulta & "   end" & vbCrLf
sConsulta = sConsulta & "  " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   IF @ESCALADOS>0 --Recoge las ofertas de la tabla de escalados" & vbCrLf
sConsulta = sConsulta & "      SET @SQL =@SQL + 'INNER JOIN ITEM_OFEESC IOF WITH (NOLOCK) ON IA.ANYO =IOF.ANYO AND IA.GMN1 =IOF.GMN1 AND IA.PROCE =IOF.PROCE AND IA.ITEM =IOF.ITEM AND IA.PROVE=IOF.PROVE AND IA.OFE =IOF.OFE AND IOF.ESC=PGE.ID '" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "      SET @SQL=@SQL + 'INNER JOIN ITEM_OFE IOF WITH (NOLOCK) ON IA.ANYO =IOF.ANYO AND IA.GMN1 =IOF.GMN1 AND IA.PROCE =IOF.PROCE AND IA.ITEM =IOF.ITEM AND IA.PROVE=IOF.PROVE AND IA.OFE =IOF.NUM '" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   ---Datos de integraci�n:" & vbCrLf
sConsulta = sConsulta & "   SET @SQL= @SQL + ' LEFT JOIN (SELECT ID,LIA.PROVEPADRE,LIA.PROVE,LIA.ANYO,LIA.GMN1_PROCE,LIA.PROCE," & vbCrLf
sConsulta = sConsulta & "       LIA.ID_ITEM FROM LOG_ITEM_ADJ LIA WITH (NOLOCK) WHERE LIA.ORIGEN=0" & vbCrLf
sConsulta = sConsulta & "       GROUP BY ID,LIA.PROVE,LIA.PROVEPADRE,LIA.ANYO,LIA.GMN1_PROCE,LIA.PROCE," & vbCrLf
sConsulta = sConsulta & "       LIA.ID_ITEM) LIA2" & vbCrLf
sConsulta = sConsulta & "       ON I.ANYO=LIA2.ANYO AND I.GMN1_PROCE=LIA2.GMN1_PROCE AND I.PROCE =LIA2.PROCE AND LIA2.PROVEPADRE=@PROVE AND LIA2.PROVE=@PROVE  AND I.ID=LIA2.ID_ITEM" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN LOG_ITEM_ADJ LIA WITH (NOLOCK) ON LIA.ID=LIA2.ID" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN (SELECT ID,LIA.PROVEPADRE,LIA.ANYO,LIA.GMN1_PROCE,LIA.PROCE," & vbCrLf
sConsulta = sConsulta & "       LIA.ID_ITEM,LIA.CENTRO  " & vbCrLf
sConsulta = sConsulta & "       FROM LOG_ITEM_ADJ LIA WITH (NOLOCK) WHERE LIA.ORIGEN=0" & vbCrLf
sConsulta = sConsulta & "       GROUP BY ID,LIA.PROVEPADRE,LIA.ANYO,LIA.GMN1_PROCE,LIA.PROCE," & vbCrLf
sConsulta = sConsulta & "       LIA.ID_ITEM,LIA.CENTRO) LIA1 " & vbCrLf
sConsulta = sConsulta & "   ON I.ANYO=LIA1.ANYO AND I.GMN1_PROCE=LIA1.GMN1_PROCE AND I.PROCE =LIA1.PROCE AND LIA1.PROVEPADRE=@PROVE  AND I.ID=LIA1.ID_ITEM AND LIA1.CENTRO=IT_CEN.CENTRO" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN LOG_ITEM_ADJ LIA3 WITH(NOLOCK) ON LIA3.ID=LIA1.ID " & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN LOG_GRAL LG WITH (NOLOCK) ON LG.ID_TABLA=LIA1.ID AND LG.TABLA=9" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN PROVE_ERP PE WITH (NOLOCK) ON LIA.PROVE_ERP=PE.COD_ERP AND PE.EMPRESA=@IDEMP " & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN (" & vbCrLf
sConsulta = sConsulta & "   SELECT DISTINCT PROVEP DISTRIBUIDOR FROM PROVE_REL PR WITH (NOLOCK) ' " & vbCrLf
sConsulta = sConsulta & "   SET @SQL =@SQL + ' INNER JOIN GS_PROVETREL GP WITH (NOLOCK) ON PR.PROVETREL=GP.ID AND PR.PROVEP=@PROVE AND GP.PEDIDO=1)" & vbCrLf
sConsulta = sConsulta & "    DISTR ON DISTR.DISTRIBUIDOR=@PROVE" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN INT_ATRIB IAT WITH(NOLOCK) ON IAT.ADJUDICACION=1 AND IAT.AMBITO=3 AND IAT.ERP=@ERP" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN DEF_ATRIB DA WITH (NOLOCK) ON DA.ID=IAT.ATRIB  AND IAT.ADJUDICACION=1 AND IAT.AMBITO=3 AND" & vbCrLf
sConsulta = sConsulta & "IAT.ERP=@ERP" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN ITEM_ATRIBESP IAE WITH (NOLOCK) ON IAE.ANYO=@ANYO AND IAE.GMN1=@GMN1 AND IAE.PROCE=@PROCE AND IAE.ATRIB=IAT.ATRIB AND IAE.ITEM=I.ID" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN PROCE_GRUPO_ATRIBESP PGA WITH (NOLOCK) ON PGA.ANYO=@ANYO AND PGA.GMN1=@GMN1 AND PGA.PROCE=@PROCE AND PGA.ATRIB=IAT.ATRIB AND PGA.GRUPO=@IDGRUPO" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN PROCE_ATRIBESP PATR WITH(NOLOCK) ON PATR.ANYO=@ANYO AND PATR.GMN1=@GMN1 AND PATR.PROCE=@PROCE AND PATR.ATRIB=IAT.ATRIB" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & " LEFT JOIN (SELECT TOP 1 OFE,ANYO,GMN1,PROCE,PROVE FROM ITEM_ADJ WITH (NOLOCK) WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE) IAD ON IAD.ANYO=@ANYO  AND IAD.GMN1=@GMN1 AND IAD.PROCE=@PROCE AND IAD.PROVE=@PROVE   " & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN PROCE_ATRIB PA WITH (NOLOCK) ON PA.ANYO=@ANYO AND PA.GMN1=@GMN1 AND PA.PROCE=@PROCE AND PA.AMBITO=3 AND DA.ID=PA.ATRIB " & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN OFE_ITEM_ATRIB OA WITH (NOLOCK) ON OA.ANYO=@ANYO AND OA.GMN1=@GMN1 AND OA.PROCE=@PROCE AND OA.ATRIB_ID =PA.ID AND OA.PROVE=@PROVE AND OA.OFE=IAD.OFE AND OA.ITEM=I.ID" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN OFE_GR_ATRIB OGATR WITH(NOLOCK) ON OGATR.ANYO=@ANYO AND OGATR.GMN1=@GMN1 AND OGATR.PROCE=@PROCE AND OGATR.ATRIB_ID =PA.ID AND OGATR.PROVE=@PROVE AND OGATR.OFE=IAD.OFE AND OGATR.GRUPO=I.GRUPO" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN OFE_ATRIB OATR WITH(NOLOCK) ON OATR.ANYO=@ANYO AND OATR.GMN1=@GMN1 AND OATR.PROCE=@PROCE AND OATR.ATRIB_ID =PA.ID AND OATR.PROVE=@PROVE AND OATR.OFE=IAD.OFE " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    LEFT JOIN " & vbCrLf
sConsulta = sConsulta & "   (SELECT DA.ID INT_ATRIB_ID,LIAA.ID_LOG_ITEM_ADJ , DA.ID, DA.COD,DA.DEN," & vbCrLf
sConsulta = sConsulta & "   DA.TIPO,IA.INTRO,IA.OBLIGATORIO,LIAA.VALOR_TEXT ,LIAA.VALOR_NUM,LIAA.VALOR_FEC,LIAA.VALOR_BOOL" & vbCrLf
sConsulta = sConsulta & "   FROM INT_ATRIB IA WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN DEF_ATRIB DA WITH (NOLOCK) ON DA.ID=IA.ATRIB AND IA.ADJUDICACION=1 AND IA.AMBITO=3 AND IA.ERP=@ERP" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN LOG_ITEM_ADJ_ATRIB LIAA WITH (NOLOCK) ON LIAA.ATRIB=IA.ATRIB" & vbCrLf
sConsulta = sConsulta & "    ) IT_ATRIB1 ON IT_ATRIB1.INT_ATRIB_ID=IAT.ID AND LIA.ID=IT_ATRIB1.ID_LOG_ITEM_ADJ" & vbCrLf
sConsulta = sConsulta & "    WHERE P.ANYO=@ANYO AND P.GMN1=@GMN1 AND P.COD=@PROCE  " & vbCrLf
sConsulta = sConsulta & "   ORDER BY I.ID ASC' " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @PARAM =N'@ANYO INT, @GMN1 VARCHAR(50), @PROCE VARCHAR(50), @CODGRUPO VARCHAR(20),@PROVE VARCHAR(50),@IDEMP INT,@ERP INT,@IDGRUPO INT'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL ,@PARAM,@ANYO=@ANYO,@GMN1=@GMN1,@PROCE=@PROCE,@CODGRUPO=@CODGRUPO,@PROVE=@PROVE,@IDEMP=@IDEMP,@ERP=@ERP ,@IDGRUPO=@IDGRUPO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta



End Sub



Public Function CodigoDeActualizacion31900_08_18992_A31900_08_18993() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim oADOConnection As ADODB.Connection
    Dim sConsulta As String

    On Error GoTo Error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    
    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Actualiza Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    frmProgreso.ProgressBar1.Refresh
    V_31900_8_Storeds_0993

    
    sConsulta = "UPDATE VERSION SET NUM ='3.00.18.993'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103), PRODVER='31900.8'"
    ExecuteSQL gRDOCon, sConsulta
        
    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False
        
    CodigoDeActualizacion31900_08_18992_A31900_08_18993 = True
    Exit Function

Error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    CodigoDeActualizacion31900_08_18992_A31900_08_18993 = False
End Function

Public Sub V_31900_8_Storeds_0993()
Dim sConsulta As String
        
sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_INICIAR_FLUJO_CONTRATO]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_INICIAR_FLUJO_CONTRATO]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_INICIAR_FLUJO_CONTRATO]" & vbCrLf
sConsulta = sConsulta & "   @INSTANCIA INT, @CONTRATO INT, @ESTADO INT, @WORKFLOW INT, @PER NVARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF (SELECT COUNT(ID) FROM INSTANCIA_EST IE WITH (NOLOCK) WHERE EST = 0 AND INSTANCIA = @INSTANCIA) = 0" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "      UPDATE INSTANCIA SET ESTADO = @ESTADO WHERE ID = @INSTANCIA" & vbCrLf
sConsulta = sConsulta & "      UPDATE INSTANCIA_BLOQUE SET ESTADO = 2 WHERE ESTADO=1 AND INSTANCIA = @INSTANCIA" & vbCrLf
sConsulta = sConsulta & "      INSERT INTO INSTANCIA_EST (INSTANCIA,FECHA,EST,PER) VALUES (@INSTANCIA,GETDATE(),0,@PER)" & vbCrLf
sConsulta = sConsulta & "          " & vbCrLf
sConsulta = sConsulta & "      IF @ESTADO = 3" & vbCrLf
sConsulta = sConsulta & "      BEGIN" & vbCrLf
sConsulta = sConsulta & "          -- Si es la primera vez que se llama al flujo de modificaci�n, se llama a FSPM_CREAR_CONFIGURACION" & vbCrLf
sConsulta = sConsulta & "          IF (SELECT COUNT(ID) FROM INSTANCIA_EST IE WITH (NOLOCK) WHERE EST = @ESTADO AND INSTANCIA = @INSTANCIA) = 0" & vbCrLf
sConsulta = sConsulta & "              EXEC FSPM_CREAR_CONFIGURACION @ID=@INSTANCIA, @WORKFLOW=@WORKFLOW,@PETICIONARIO=@PER" & vbCrLf
sConsulta = sConsulta & "          ELSE" & vbCrLf
sConsulta = sConsulta & "          BEGIN" & vbCrLf
sConsulta = sConsulta & "              INSERT INTO INSTANCIA_BLOQUE(INSTANCIA,BLOQUE,ESTADO) " & vbCrLf
sConsulta = sConsulta & "              SELECT @INSTANCIA,PM_COPIA_BLOQUE.ID, 1 FROM PM_COPIA_BLOQUE WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "              INNER JOIN PM_BLOQUE PB WITH(NOLOCK) ON PB.ID = PM_COPIA_BLOQUE.BLOQUE_ORIGEN AND PB.WORKFLOW=@WORKFLOW" & vbCrLf
sConsulta = sConsulta & "              WHERE INSTANCIA=@INSTANCIA AND PM_COPIA_BLOQUE.TIPO=1" & vbCrLf
sConsulta = sConsulta & "              " & vbCrLf
sConsulta = sConsulta & "              " & vbCrLf
sConsulta = sConsulta & "              -- Actualizo el Rol Peticionario, para que en el Campo PER tenga el codigo del la persona Peticionaria, " & vbCrLf
sConsulta = sConsulta & "              -- s�lo actualizo si no existe un rol peticionario que tenga la persona peticionaria" & vbCrLf
sConsulta = sConsulta & "              IF (SELECT COUNT(1) FROM PM_COPIA_ROL WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "              INNER JOIN PER P WITH (NOLOCK) ON P.COD = @PER AND ISNULL(P.UON1,'') = ISNULL(PM_COPIA_ROL.UON1,'') AND ISNULL(P.UON2,'') = ISNULL(PM_COPIA_ROL.UON2,'') " & vbCrLf
sConsulta = sConsulta & "                  AND ISNULL(P.UON3,'') = ISNULL(PM_COPIA_ROL.UON3,'') AND P.DEP=PM_COPIA_ROL.DEP AND P.COD=PM_COPIA_ROL.PER" & vbCrLf
sConsulta = sConsulta & "              INNER JOIN PM_ROL WITH(NOLOCK) ON PM_ROL.ID = PM_COPIA_ROL.ROL_ORIGEN AND PM_ROL.WORKFLOW=@WORKFLOW" & vbCrLf
sConsulta = sConsulta & "              WHERE  PM_COPIA_ROL.TIPO = 4 AND PM_COPIA_ROL.INSTANCIA = @INSTANCIA)=0" & vbCrLf
sConsulta = sConsulta & "              BEGIN" & vbCrLf
sConsulta = sConsulta & "                  --Actualizo los roles de la UON y DEP de la persona peticionaria" & vbCrLf
sConsulta = sConsulta & "                  UPDATE PM_COPIA_ROL SET PER = P.COD,PER_ORIGINAL = P.COD " & vbCrLf
sConsulta = sConsulta & "                  FROM PM_COPIA_ROL WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                  INNER JOIN PM_ROL WITH(NOLOCK) ON PM_ROL.ID = PM_COPIA_ROL.ROL_ORIGEN AND PM_ROL.WORKFLOW=@WORKFLOW" & vbCrLf
sConsulta = sConsulta & "                  INNER JOIN PER P WITH (NOLOCK) ON P.COD = @PER AND ISNULL(P.UON1,'') = ISNULL(PM_COPIA_ROL.UON1,'') AND ISNULL(P.UON2,'') = ISNULL(PM_COPIA_ROL.UON2,'') " & vbCrLf
sConsulta = sConsulta & "                       AND ISNULL(P.UON3,'') = ISNULL(PM_COPIA_ROL.UON3,'') AND P.DEP=PM_COPIA_ROL.DEP  " & vbCrLf
sConsulta = sConsulta & "                  WHERE PM_COPIA_ROL.TIPO = 4 AND PM_COPIA_ROL.INSTANCIA = @INSTANCIA --AND PM_COPIA_ROL.PER IS NULL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                  IF @@ROWCOUNT = 0 --Actualizo los roles de la UON  de la persona peticionaria, no hay DEP en el rol" & vbCrLf
sConsulta = sConsulta & "                  BEGIN" & vbCrLf
sConsulta = sConsulta & "                      UPDATE PM_COPIA_ROL SET PER = P.COD,PER_ORIGINAL = P.COD   " & vbCrLf
sConsulta = sConsulta & "                      FROM PM_COPIA_ROL WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                      INNER JOIN PM_ROL WITH(NOLOCK) ON PM_ROL.ID = PM_COPIA_ROL.ROL_ORIGEN AND PM_ROL.WORKFLOW=@WORKFLOW" & vbCrLf
sConsulta = sConsulta & "                      INNER JOIN PER P WITH (NOLOCK) ON P.COD = @PER AND ISNULL(P.UON1,'') = ISNULL(PM_COPIA_ROL.UON1,'') AND ISNULL(P.UON2,'') = ISNULL(PM_COPIA_ROL.UON2,'') " & vbCrLf
sConsulta = sConsulta & "                           AND ISNULL(P.UON3,'') = ISNULL(PM_COPIA_ROL.UON3,'')" & vbCrLf
sConsulta = sConsulta & "                      WHERE PM_COPIA_ROL.TIPO = 4 AND PM_COPIA_ROL.INSTANCIA = @INSTANCIA AND PM_COPIA_ROL.DEP IS NULL  --AND PM_COPIA_ROL.PER IS NULL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                      IF @@ROWCOUNT = 0 --Actualizo los roles de la UON  de la persona peticionaria, no hay DEP en el rol ni UON3" & vbCrLf
sConsulta = sConsulta & "                      BEGIN" & vbCrLf
sConsulta = sConsulta & "                          UPDATE PM_COPIA_ROL   SET PER = P.COD,PER_ORIGINAL = P.COD   " & vbCrLf
sConsulta = sConsulta & "                          FROM PM_COPIA_ROL WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                          INNER JOIN PM_ROL WITH(NOLOCK) ON PM_ROL.ID = PM_COPIA_ROL.ROL_ORIGEN AND PM_ROL.WORKFLOW=@WORKFLOW" & vbCrLf
sConsulta = sConsulta & "                          INNER JOIN PER P WITH (NOLOCK) ON P.COD = @PER AND ISNULL(P.UON1,'') = ISNULL(PM_COPIA_ROL.UON1,'') AND ISNULL(P.UON2,'') = ISNULL(PM_COPIA_ROL.UON2,'') " & vbCrLf
sConsulta = sConsulta & "                          WHERE PM_COPIA_ROL.UON3 IS NULL AND PM_COPIA_ROL.DEP IS NULL AND PM_COPIA_ROL.TIPO = 4 " & vbCrLf
sConsulta = sConsulta & "                          AND PM_COPIA_ROL.INSTANCIA = @INSTANCIA --AND PM_COPIA_ROL.PER IS NULL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                          IF @@ROWCOUNT = 0 --Actualizo los roles de la UON  de la persona peticionaria, no hay DEP en el rol ni UON3" & vbCrLf
sConsulta = sConsulta & "                          BEGIN" & vbCrLf
sConsulta = sConsulta & "                              UPDATE PM_COPIA_ROL   SET PER = P.COD,PER_ORIGINAL = P.COD   " & vbCrLf
sConsulta = sConsulta & "                              FROM PM_COPIA_ROL WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                              INNER JOIN PM_ROL WITH(NOLOCK) ON PM_ROL.ID = PM_COPIA_ROL.ROL_ORIGEN AND PM_ROL.WORKFLOW=@WORKFLOW" & vbCrLf
sConsulta = sConsulta & "                              INNER JOIN PER P WITH (NOLOCK) ON P.COD = @PER AND ISNULL(P.UON1,'') = ISNULL(PM_COPIA_ROL.UON1,'') " & vbCrLf
sConsulta = sConsulta & "                              AND PM_COPIA_ROL.UON3 IS NULL AND PM_COPIA_ROL.UON2 IS NULL AND PM_COPIA_ROL.DEP IS NULL AND PM_COPIA_ROL.TIPO = 4 " & vbCrLf
sConsulta = sConsulta & "                              AND PM_COPIA_ROL.INSTANCIA = @INSTANCIA --AND PM_COPIA_ROL.PER IS NULL" & vbCrLf
sConsulta = sConsulta & "                          END" & vbCrLf
sConsulta = sConsulta & "                      END" & vbCrLf
sConsulta = sConsulta & "                  END" & vbCrLf
sConsulta = sConsulta & "              END " & vbCrLf
sConsulta = sConsulta & "                  " & vbCrLf
sConsulta = sConsulta & "              " & vbCrLf
sConsulta = sConsulta & "              UPDATE INSTANCIA_EST SET INSTANCIA_EST.ROL = PM_COPIA_ROL.ID" & vbCrLf
sConsulta = sConsulta & "               FROM INSTANCIA_EST WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "               INNER JOIN PM_COPIA_ROL WITH(NOLOCK) ON PM_COPIA_ROL.INSTANCIA = INSTANCIA_EST.INSTANCIA AND PM_COPIA_ROL.PER = INSTANCIA_EST.PER" & vbCrLf
sConsulta = sConsulta & "               INNER JOIN PM_ROL WITH(NOLOCK) ON PM_ROL.ID=PM_COPIA_ROL.ROL_ORIGEN AND PM_ROL.WORKFLOW=@WORKFLOW" & vbCrLf
sConsulta = sConsulta & "               WHERE INSTANCIA_EST.INSTANCIA = @INSTANCIA AND INSTANCIA_EST.EST=0 AND INSTANCIA_EST.BLOQUE IS NULL" & vbCrLf
sConsulta = sConsulta & "               AND INSTANCIA_EST.ROL IS NULL AND PM_COPIA_ROL.TIPO = 4 AND PM_COPIA_ROL.PER = @PER" & vbCrLf
sConsulta = sConsulta & "          END" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "      ELSE" & vbCrLf
sConsulta = sConsulta & "      BEGIN" & vbCrLf
sConsulta = sConsulta & "          EXEC FSPM_CREAR_CONFIGURACION @ID=@INSTANCIA, @WORKFLOW=@WORKFLOW,@PETICIONARIO=@PER" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub




Public Function CodigoDeActualizacion31900_08_18993_A31900_08_18994() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim oADOConnection As ADODB.Connection
    Dim sConsulta As String

    On Error GoTo Error

    
    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Actualiza Tablas"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    frmProgreso.ProgressBar1.Refresh
    V_31900_8_Tablas_0994
    
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Actualiza Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    frmProgreso.ProgressBar1.Refresh
    V_31900_8_Storeds_0994

    
    sConsulta = "UPDATE VERSION SET NUM ='3.00.18.994'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103), PRODVER='31900.8'"
    ExecuteSQL gRDOCon, sConsulta
        
    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False
        
    CodigoDeActualizacion31900_08_18993_A31900_08_18994 = True
    Exit Function

Error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    CodigoDeActualizacion31900_08_18993_A31900_08_18994 = False
End Function

Public Sub V_31900_8_Storeds_0994()
Dim sConsulta As String
        
sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CN_BUSCADOR_PROVE]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[CN_BUSCADOR_PROVE]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[CN_BUSCADOR_PROVE]" & vbCrLf
sConsulta = sConsulta & "   @PROVE NVARCHAR(50)," & vbCrLf
sConsulta = sConsulta & "   @CON INT," & vbCrLf
sConsulta = sConsulta & "   @IDI NVARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "   @LCID INT," & vbCrLf
sConsulta = sConsulta & "   @SEARCH NVARCHAR(1000)," & vbCrLf
sConsulta = sConsulta & "   @ORDERTYPE TINYINT=0" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "/*" & vbCrLf
sConsulta = sConsulta & "Calidad" & vbCrLf
sConsulta = sConsulta & "Poner  WITH(NOLOCK) en INNER JOIN CONTAINSTABLE ....) MENF provoca un fallo" & vbCrLf
sConsulta = sConsulta & "Poner  WITH(NOLOCK) en FROM @FULLTEXTRESULTADOS R provoca un fallo" & vbCrLf
sConsulta = sConsulta & "*/" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FULLTEXTRESULTADOS AS TABLE(IDMENSAJE INT,IDRESPUESTA INT, IDADJUNTO INT, PESO FLOAT) " & vbCrLf
sConsulta = sConsulta & "INSERT INTO @FULLTEXTRESULTADOS " & vbCrLf
sConsulta = sConsulta & "   SELECT ID AS MEN,0 RESP,0 ADJUN,(RANK+1) PESO" & vbCrLf
sConsulta = sConsulta & "   FROM CN_MEN MEN WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN CONTAINSTABLE (CN_MEN,(TITULO,DONDE,CONTENIDO), @SEARCH, LANGUAGE @LCID) MENF ON MEN.ID=MENF.[KEY]" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN CN_MEN_CON WITH(NOLOCK) ON CN_MEN_CON.MEN=MEN.ID AND CN_MEN_CON.PROVE=@PROVE AND CN_MEN_CON.CON=@CON" & vbCrLf
sConsulta = sConsulta & "   UNION" & vbCrLf
sConsulta = sConsulta & "   SELECT RESP.MEN AS MEN,RESP.ID RESP,0 ADJUN,CONVERT(FLOAT,(RANK+1))/CONVERT(FLOAT,1000) PESO" & vbCrLf
sConsulta = sConsulta & "   FROM CN_RESP RESP WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN CONTAINSTABLE (CN_RESP,CONTENIDO, @SEARCH, LANGUAGE @LCID) RESPF ON RESP.ID=RESPF.[KEY]" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN CN_MEN MEN WITH(NOLOCK) ON MEN.ID=RESP.MEN AND (RESP.PROVE=@PROVE OR RESP.PROVECITADO=@PROVE)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN CN_MEN_CON WITH(NOLOCK) ON CN_MEN_CON.MEN=MEN.ID AND CN_MEN_CON.PROVE=@PROVE AND CN_MEN_CON.CON=@CON" & vbCrLf
sConsulta = sConsulta & "   UNION" & vbCrLf
sConsulta = sConsulta & "   SELECT ADJUN.MEN AS MEN,0 RESP,ADJUN.ID ADJUN,CONVERT(FLOAT,RANK+1)/CONVERT(FLOAT,1000000) PESO" & vbCrLf
sConsulta = sConsulta & "   FROM CN_ADJUN ADJUN WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN CONTAINSTABLE (CN_ADJUN,NOMBRE, @SEARCH, LANGUAGE @LCID) ADJUNF ON ADJUN.ID=ADJUNF.[KEY]" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN CN_MEN MEN WITH(NOLOCK) ON MEN.ID=ADJUN.MEN " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN CN_MEN_CON WITH(NOLOCK) ON CN_MEN_CON.MEN=MEN.ID AND CN_MEN_CON.PROVE=@PROVE AND CN_MEN_CON.CON=@CON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ORDERTYPE=1" & vbCrLf
sConsulta = sConsulta & "   SELECT MEN.ID,MEN.FECALTA,MEN.FECACT,MEN.USU,MEN.TIPO,MEN.TITULO,MEN.CONTENIDO,MEN.DONDE,MEN.CUANDO," & vbCrLf
sConsulta = sConsulta & "       PER.APE,PER.NOM,DEP.DEN SITUACIONUO,CN_USU.GUIDIMAGENUSU," & vbCrLf
sConsulta = sConsulta & "       CATNOMBRE.NOMBRECOMPLETO CATEGORIA,NULL LEIDO,0 OCULTO,SUM(PESO) PESO" & vbCrLf
sConsulta = sConsulta & "   FROM @FULLTEXTRESULTADOS R " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN CN_MEN MEN WITH(NOLOCK) ON MEN.ID=R.IDMENSAJE" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN CN_USU WITH(NOLOCK) ON CN_USU.COD=MEN.USU" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN USU WITH(NOLOCK) ON USU.COD=MEN.USU" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PER WITH(NOLOCK) ON PER.COD=USU.PER " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN DEP WITH(NOLOCK) ON DEP.COD=PER.DEP" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN CN_CATEGORIAS_NOMBRECOMPLETO CATNOMBRE WITH(NOLOCK) ON CATNOMBRE.ID=MEN.CAT AND CATNOMBRE.IDI=@IDI" & vbCrLf
sConsulta = sConsulta & "   GROUP BY MEN.ID,MEN.FECALTA,MEN.FECACT,MEN.USU,MEN.TIPO,MEN.TITULO,MEN.CONTENIDO,MEN.DONDE,MEN.CUANDO," & vbCrLf
sConsulta = sConsulta & "       PER.APE,PER.NOM,DEP.DEN,CN_USU.GUIDIMAGENUSU,CATNOMBRE.NOMBRECOMPLETO" & vbCrLf
sConsulta = sConsulta & "   ORDER BY PESO DESC" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   SELECT MEN.ID,MEN.FECALTA,MEN.FECACT,MEN.USU,MEN.TIPO,MEN.TITULO,MEN.CONTENIDO,MEN.DONDE,MEN.CUANDO," & vbCrLf
sConsulta = sConsulta & "       PER.APE,PER.NOM,DEP.DEN SITUACIONUO,CN_USU.GUIDIMAGENUSU," & vbCrLf
sConsulta = sConsulta & "       CATNOMBRE.NOMBRECOMPLETO CATEGORIA,NULL LEIDO,0 OCULTO,SUM(PESO) PESO" & vbCrLf
sConsulta = sConsulta & "   FROM @FULLTEXTRESULTADOS R " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN CN_MEN MEN WITH(NOLOCK) ON MEN.ID=R.IDMENSAJE" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN CN_USU WITH(NOLOCK) ON CN_USU.COD=MEN.USU" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN USU WITH(NOLOCK) ON USU.COD=MEN.USU" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PER WITH(NOLOCK) ON PER.COD=USU.PER " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN DEP WITH(NOLOCK) ON DEP.COD=PER.DEP" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN CN_CATEGORIAS_NOMBRECOMPLETO CATNOMBRE WITH(NOLOCK) ON CATNOMBRE.ID=MEN.CAT AND CATNOMBRE.IDI=@IDI" & vbCrLf
sConsulta = sConsulta & "   GROUP BY MEN.ID,MEN.FECALTA,MEN.FECACT,MEN.USU,MEN.TIPO,MEN.TITULO,MEN.CONTENIDO,MEN.DONDE,MEN.CUANDO," & vbCrLf
sConsulta = sConsulta & "       PER.APE,PER.NOM,DEP.DEN,CN_USU.GUIDIMAGENUSU,CATNOMBRE.NOMBRECOMPLETO" & vbCrLf
sConsulta = sConsulta & "   ORDER BY MEN.FECACT DESC" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT RESP.ID,R2.IDRESPUESTA VISIBLE,RESP.USU,PER.APE,PER.NOM,DEP.DEN SITUACIONUO,CN_USU.GUIDIMAGENUSU," & vbCrLf
sConsulta = sConsulta & "       RESP.CONTENIDO,RESP.MEGUSTA,RESP.FECALTA,RESP.FECACT,RESP.MEN," & vbCrLf
sConsulta = sConsulta & "       USU2.COD USUCITADO,PER2.APE APECITADO,PER2.NOM NOMCITADO,DEP2.DEN SITUACIONUOCITADO,RESP.RESP," & vbCrLf
sConsulta = sConsulta & "       RESP.PROVE,RESP.CON,CON.APE APEPROVE,CON.NOM NOMPROVE,CON.DEP SITUACIONUOPROVE,PROVE.DEN PROVEDEN," & vbCrLf
sConsulta = sConsulta & "       RESP.PROVECITADO,RESP.CONCITADO,CON2.APE APEPROVECITADO,CON2.NOM NOMPROVECITADO," & vbCrLf
sConsulta = sConsulta & "       CON2.DEP SITUACIONUOPROVECITADO,PROVE2.DEN PROVEDENCITADO,CN_USU2.GUIDIMAGENUSU GUIDIMAGENPROVE" & vbCrLf
sConsulta = sConsulta & "FROM @FULLTEXTRESULTADOS R" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN CN_RESP RESP WITH(NOLOCK) ON RESP.MEN=R.IDMENSAJE AND RESP.MEGUSTA=0 " & vbCrLf
sConsulta = sConsulta & "       AND (RESP.PROVE=@PROVE OR RESP.PROVECITADO=@PROVE)" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN @FULLTEXTRESULTADOS R2 ON RESP.ID=R2.IDRESPUESTA " & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN CON WITH(NOLOCK) ON CON.PROVE=RESP.PROVE AND CON.ID=RESP.CON" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN PROVE WITH(NOLOCK) ON PROVE.COD=CON.PROVE" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN CN_USU WITH(NOLOCK) ON CN_USU.COD=RESP.USU" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN CN_USU CN_USU2 WITH(NOLOCK) ON CN_USU2.PROVE=RESP.PROVE AND CN_USU2.CON=RESP.CON" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN USU WITH(NOLOCK) ON USU.COD=RESP.USU" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN PER WITH(NOLOCK) ON PER.COD=USU.PER " & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN DEP WITH(NOLOCK) ON DEP.COD=PER.DEP" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN CON CON2 WITH(NOLOCK) ON CON2.PROVE=RESP.PROVECITADO AND CON2.ID=RESP.CONCITADO   " & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN PROVE PROVE2 WITH(NOLOCK) ON PROVE2.COD=CON2.PROVE    " & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN USU USU2 WITH(NOLOCK) ON USU2.COD=RESP.USUCITADO" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN PER PER2 WITH(NOLOCK) ON PER2.COD=USU2.COD" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN DEP DEP2 WITH(NOLOCK) ON DEP2.COD=PER2.DEP" & vbCrLf
sConsulta = sConsulta & "ORDER BY RESP.FECACT ASC" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT RESP.ID,RESP.USU,PER.APE,PER.NOM,DEP.DEN SITUACIONUO,CN_USU.GUIDIMAGENUSU," & vbCrLf
sConsulta = sConsulta & "       RESP.MEGUSTA,RESP.FECALTA,RESP.FECACT,RESP.MEN," & vbCrLf
sConsulta = sConsulta & "       USU2.COD USUCITADO,PER2.APE APECITADO,PER2.NOM NOMCITADO,DEP2.DEN SITUACIONUOCITADO,RESP.RESP," & vbCrLf
sConsulta = sConsulta & "       RESP.PROVE,RESP.CON,CON.APE APEPROVE,CON.NOM NOMPROVE,CON.DEP SITUACIONUOPROVE,PROVE.DEN PROVEDEN," & vbCrLf
sConsulta = sConsulta & "       RESP.PROVECITADO,RESP.CONCITADO,CON2.APE APEPROVECITADO,CON2.NOM NOMPROVECITADO," & vbCrLf
sConsulta = sConsulta & "       CON2.DEP SITUACIONUOPROVECITADO,PROVE2.DEN PROVEDENCITADO,CN_USU2.GUIDIMAGENUSU GUIDIMAGENPROVE" & vbCrLf
sConsulta = sConsulta & "FROM @FULLTEXTRESULTADOS R" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN CN_RESP RESP WITH(NOLOCK) ON RESP.MEN=R.IDMENSAJE AND RESP.MEGUSTA=1 AND RESP.RESP IS NULL" & vbCrLf
sConsulta = sConsulta & "       AND (RESP.PROVE=@PROVE OR RESP.PROVECITADO=@PROVE)" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN CON WITH(NOLOCK) ON CON.PROVE=RESP.PROVE AND CON.ID=RESP.CON" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN PROVE WITH(NOLOCK) ON PROVE.COD=CON.PROVE" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN CN_USU WITH(NOLOCK) ON CN_USU.COD=RESP.USU" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN CN_USU CN_USU2 WITH(NOLOCK) ON CN_USU2.PROVE=RESP.PROVE AND CN_USU2.CON=RESP.CON" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN USU WITH(NOLOCK) ON USU.COD=RESP.USU" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN PER WITH(NOLOCK) ON PER.COD=USU.PER " & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN DEP WITH(NOLOCK) ON DEP.COD=PER.DEP" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN CON CON2 WITH(NOLOCK) ON CON2.PROVE=RESP.PROVECITADO AND CON2.ID=RESP.CONCITADO   " & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN PROVE PROVE2 WITH(NOLOCK) ON PROVE2.COD=CON2.PROVE    " & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN USU USU2 WITH(NOLOCK) ON USU2.COD=RESP.USUCITADO" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN PER PER2 WITH(NOLOCK) ON PER2.COD=USU2.COD" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN DEP DEP2 WITH(NOLOCK) ON DEP2.COD=PER2.DEP" & vbCrLf
sConsulta = sConsulta & "ORDER BY RESP.FECACT ASC" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT RESP.ID,RESP.USU,PER.APE,PER.NOM,DEP.DEN SITUACIONUO,CN_USU.GUIDIMAGENUSU," & vbCrLf
sConsulta = sConsulta & "       RESP.MEGUSTA,RESP.FECALTA,RESP.FECACT,RESP.MEN," & vbCrLf
sConsulta = sConsulta & "       USU2.COD USUCITADO,PER2.APE APECITADO,PER2.NOM NOMCITADO,DEP2.DEN SITUACIONUOCITADO,RESP.RESP," & vbCrLf
sConsulta = sConsulta & "       RESP.PROVE,RESP.CON,CON.APE APEPROVE,CON.NOM NOMPROVE,CON.DEP SITUACIONUOPROVE,PROVE.DEN PROVEDEN," & vbCrLf
sConsulta = sConsulta & "       RESP.PROVECITADO,RESP.CONCITADO,CON2.APE APEPROVECITADO,CON2.NOM NOMPROVECITADO," & vbCrLf
sConsulta = sConsulta & "       CON2.DEP SITUACIONUOPROVECITADO,PROVE2.DEN PROVEDENCITADO,CN_USU2.GUIDIMAGENUSU GUIDIMAGENPROVE" & vbCrLf
sConsulta = sConsulta & "FROM @FULLTEXTRESULTADOS R" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN CN_RESP RESP WITH(NOLOCK) ON RESP.MEN=R.IDMENSAJE AND RESP.MEGUSTA=1 AND RESP.RESP IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "       AND (RESP.PROVE=@PROVE OR RESP.PROVECITADO=@PROVE)" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN CON WITH(NOLOCK) ON CON.PROVE=RESP.PROVE AND CON.ID=RESP.CON" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN PROVE WITH(NOLOCK) ON PROVE.COD=CON.PROVE" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN CN_USU WITH(NOLOCK) ON CN_USU.COD=RESP.USU" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN CN_USU CN_USU2 WITH(NOLOCK) ON CN_USU2.PROVE=RESP.PROVE AND CN_USU2.CON=RESP.CON" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN USU WITH(NOLOCK) ON USU.COD=RESP.USU" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN PER WITH(NOLOCK) ON PER.COD=USU.PER " & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN DEP WITH(NOLOCK) ON DEP.COD=PER.DEP" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN CON CON2 WITH(NOLOCK) ON CON2.PROVE=RESP.PROVECITADO AND CON2.ID=RESP.CONCITADO   " & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN PROVE PROVE2 WITH(NOLOCK) ON PROVE2.COD=CON2.PROVE    " & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN USU USU2 WITH(NOLOCK) ON USU2.COD=RESP.USUCITADO" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN PER PER2 WITH(NOLOCK) ON PER2.COD=USU2.COD" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN DEP DEP2 WITH(NOLOCK) ON DEP2.COD=PER2.DEP" & vbCrLf
sConsulta = sConsulta & "ORDER BY RESP.FECACT ASC" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT ID,NOMBRE,GUID,SIZE,SIZEUNIT,TIPOADJUNTO,FECALTA,R.IDMENSAJE MEN,RESP" & vbCrLf
sConsulta = sConsulta & "FROM @FULLTEXTRESULTADOS R" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN CN_ADJUN ADJUN WITH(NOLOCK) ON ADJUN.MEN=R.IDMENSAJE" & vbCrLf
sConsulta = sConsulta & "ORDER BY FECALTA ASC" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CN_BUSCADOR]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[CN_BUSCADOR]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[CN_BUSCADOR]" & vbCrLf
sConsulta = sConsulta & "   @USU NVARCHAR(50)," & vbCrLf
sConsulta = sConsulta & "   @IDI NVARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "   @LCID INT," & vbCrLf
sConsulta = sConsulta & "   @SEARCH NVARCHAR(1000)," & vbCrLf
sConsulta = sConsulta & "   @ORDERTYPE TINYINT=0" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "/*" & vbCrLf
sConsulta = sConsulta & "Calidad" & vbCrLf
sConsulta = sConsulta & "Poner  WITH(NOLOCK) en INNER JOIN CONTAINSTABLE ....) MENF provoca un fallo" & vbCrLf
sConsulta = sConsulta & "Poner  WITH(NOLOCK) en FROM @FULLTEXTRESULTADOS R provoca un fallo" & vbCrLf
sConsulta = sConsulta & "Poner  WITH(NOLOCK) en FROM @FULLTEXTRESULTADOS R2 provoca un fallo" & vbCrLf
sConsulta = sConsulta & "*/" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FULLTEXTRESULTADOS AS TABLE(IDMENSAJE INT,IDRESPUESTA INT, IDADJUNTO INT, PESO FLOAT) " & vbCrLf
sConsulta = sConsulta & "INSERT INTO @FULLTEXTRESULTADOS " & vbCrLf
sConsulta = sConsulta & "   SELECT ID AS MEN,0 RESP,0 ADJUN,(RANK+1) PESO" & vbCrLf
sConsulta = sConsulta & "   FROM CN_MEN MEN WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN CONTAINSTABLE (CN_MEN,(TITULO,DONDE,CONTENIDO), @SEARCH, LANGUAGE @LCID) MENF ON MEN.ID=MENF.[KEY]" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN CN_OPT_MEN_USU WITH(NOLOCK) ON CN_OPT_MEN_USU.MEN=MEN.ID AND CN_OPT_MEN_USU.USU=@USU" & vbCrLf
sConsulta = sConsulta & "   UNION" & vbCrLf
sConsulta = sConsulta & "   SELECT RESP.MEN AS MEN,RESP.ID RESP,0 ADJUN,CONVERT(FLOAT,(RANK+1))/CONVERT(FLOAT,1000) PESO" & vbCrLf
sConsulta = sConsulta & "   FROM CN_RESP RESP WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN CONTAINSTABLE (CN_RESP,CONTENIDO, @SEARCH, LANGUAGE @LCID) RESPF ON RESP.ID=RESPF.[KEY]" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN CN_MEN MEN WITH(NOLOCK) ON MEN.ID=RESP.MEN" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN CN_OPT_MEN_USU WITH(NOLOCK) ON CN_OPT_MEN_USU.MEN=MEN.ID AND CN_OPT_MEN_USU.USU=@USU" & vbCrLf
sConsulta = sConsulta & "   UNION" & vbCrLf
sConsulta = sConsulta & "   SELECT ADJUN.MEN AS MEN,0 RESP,ADJUN.ID ADJUN,CONVERT(FLOAT,RANK+1)/CONVERT(FLOAT,1000000) PESO" & vbCrLf
sConsulta = sConsulta & "   FROM CN_ADJUN ADJUN WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN CONTAINSTABLE (CN_ADJUN,NOMBRE, @SEARCH, LANGUAGE @LCID) ADJUNF ON ADJUN.ID=ADJUNF.[KEY]" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN CN_MEN MEN WITH(NOLOCK) ON MEN.ID=ADJUN.MEN " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN CN_OPT_MEN_USU WITH(NOLOCK) ON CN_OPT_MEN_USU.MEN=MEN.ID AND CN_OPT_MEN_USU.USU=@USU" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ORDERTYPE=1" & vbCrLf
sConsulta = sConsulta & "   SELECT MEN.ID,MEN.FECALTA,MEN.FECACT,MEN.USU,MEN.TIPO,MEN.TITULO,MEN.CONTENIDO,MEN.DONDE,MEN.CUANDO," & vbCrLf
sConsulta = sConsulta & "       PER.APE,PER.NOM,DEP.DEN SITUACIONUO,CN_USU.GUIDIMAGENUSU,DISCRP.ID IDDISCREPANCIA," & vbCrLf
sConsulta = sConsulta & "       CATNOMBRE.DEN CATEGORIA,NULL LEIDO,0 OCULTO,0 MENSAJEHISTORICO,SUM(PESO) PESO," & vbCrLf
sConsulta = sConsulta & "       MEN.PROVE,MEN.IDCON CON,CON.APE APEPROVE,CON.NOM NOMPROVE,CON.DEP SITUACIONUOPROVE,PROVE.DEN PROVEDEN" & vbCrLf
sConsulta = sConsulta & "   FROM @FULLTEXTRESULTADOS R " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN CN_MEN MEN WITH(NOLOCK) ON MEN.ID=R.IDMENSAJE" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN LIN_FACTURA_DISCRP DISCRP WITH(NOLOCK) ON DISCRP.MEN=MEN.ID" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN CN_USU WITH(NOLOCK) ON CN_USU.COD=MEN.USU" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN USU WITH(NOLOCK) ON USU.COD=MEN.USU" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN PER WITH(NOLOCK) ON PER.COD=USU.PER " & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN DEP WITH(NOLOCK) ON DEP.COD=PER.DEP" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN CN_CAT_IDI CATNOMBRE WITH(NOLOCK) ON CATNOMBRE.CAT=MEN.CAT AND CATNOMBRE.IDI=@IDI" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN CON WITH(NOLOCK) ON CON.ID=MEN.IDCON AND CON.PROVE=MEN.PROVE" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN PROVE WITH(NOLOCK) ON PROVE.COD=CON.PROVE" & vbCrLf
sConsulta = sConsulta & "   GROUP BY MEN.ID,MEN.FECALTA,MEN.FECACT,MEN.USU,MEN.TIPO,MEN.TITULO,MEN.CONTENIDO,MEN.DONDE,MEN.CUANDO," & vbCrLf
sConsulta = sConsulta & "       PER.APE,PER.NOM,DEP.DEN,CN_USU.GUIDIMAGENUSU,CATNOMBRE.DEN,DISCRP.ID,MEN.PROVE,MEN.IDCON,CON.APE," & vbCrLf
sConsulta = sConsulta & "       CON.NOM,CON.DEP,PROVE.DEN " & vbCrLf
sConsulta = sConsulta & "   ORDER BY PESO DESC" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   SELECT MEN.ID,MEN.FECALTA,MEN.FECACT,MEN.USU,MEN.TIPO,MEN.TITULO,MEN.CONTENIDO,MEN.DONDE,MEN.CUANDO," & vbCrLf
sConsulta = sConsulta & "       PER.APE,PER.NOM,DEP.DEN SITUACIONUO,CN_USU.GUIDIMAGENUSU,DISCRP.ID IDDISCREPANCIA," & vbCrLf
sConsulta = sConsulta & "       CATNOMBRE.DEN CATEGORIA,NULL LEIDO,0 OCULTO,0 MENSAJEHISTORICO,SUM(PESO) PESO," & vbCrLf
sConsulta = sConsulta & "       MEN.PROVE,MEN.IDCON CON,CON.APE APEPROVE,CON.NOM NOMPROVE,CON.DEP SITUACIONUOPROVE,PROVE.DEN PROVEDEN" & vbCrLf
sConsulta = sConsulta & "   FROM @FULLTEXTRESULTADOS R " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN CN_MEN MEN WITH(NOLOCK) ON MEN.ID=R.IDMENSAJE" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN LIN_FACTURA_DISCRP DISCRP WITH(NOLOCK) ON DISCRP.MEN=MEN.ID" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN CN_USU WITH(NOLOCK) ON CN_USU.COD=MEN.USU" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN USU WITH(NOLOCK) ON USU.COD=MEN.USU" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN PER WITH(NOLOCK) ON PER.COD=USU.PER " & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN DEP WITH(NOLOCK) ON DEP.COD=PER.DEP       " & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN CN_CAT_IDI CATNOMBRE WITH(NOLOCK) ON CATNOMBRE.CAT=MEN.CAT AND CATNOMBRE.IDI=@IDI" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN CON WITH(NOLOCK) ON CON.ID=MEN.IDCON AND CON.PROVE=MEN.PROVE" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN PROVE WITH(NOLOCK) ON PROVE.COD=CON.PROVE" & vbCrLf
sConsulta = sConsulta & "   GROUP BY MEN.ID,MEN.FECALTA,MEN.FECACT,MEN.USU,MEN.TIPO,MEN.TITULO,MEN.CONTENIDO,MEN.DONDE,MEN.CUANDO," & vbCrLf
sConsulta = sConsulta & "       PER.APE,PER.NOM,DEP.DEN,CN_USU.GUIDIMAGENUSU,CATNOMBRE.DEN,DISCRP.ID,MEN.PROVE,MEN.IDCON,CON.APE," & vbCrLf
sConsulta = sConsulta & "       CON.NOM,CON.DEP,PROVE.DEN " & vbCrLf
sConsulta = sConsulta & "   ORDER BY MEN.FECACT DESC" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT RESP.ID,NTF.IDRESPUESTA VISIBLE,RESP.USU,PER.APE,PER.NOM,DEP.DEN SITUACIONUO,CN_USU.GUIDIMAGENUSU," & vbCrLf
sConsulta = sConsulta & "       RESP.CONTENIDO,RESP.MEGUSTA,RESP.FECALTA,RESP.FECACT,RESP.MEN," & vbCrLf
sConsulta = sConsulta & "       USU2.COD USUCITADO,PER2.APE APECITADO,PER2.NOM NOMCITADO,DEP2.DEN SITUACIONUOCITADO,RESP.RESP," & vbCrLf
sConsulta = sConsulta & "       RESP.PROVE,RESP.CON,CON.APE APEPROVE,CON.NOM NOMPROVE,CON.DEP SITUACIONUOPROVE,PROVE.DEN PROVEDEN," & vbCrLf
sConsulta = sConsulta & "       RESP.PROVECITADO,RESP.CONCITADO,CON2.APE APEPROVECITADO,CON2.NOM NOMPROVECITADO," & vbCrLf
sConsulta = sConsulta & "       CON2.DEP SITUACIONUOPROVECITADO,PROVE2.DEN PROVEDENCITADO,CN_USU2.GUIDIMAGENUSU GUIDIMAGENPROVE" & vbCrLf
sConsulta = sConsulta & "FROM @FULLTEXTRESULTADOS R" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN CN_RESP RESP WITH(NOLOCK) ON RESP.MEN=R.IDMENSAJE AND RESP.MEGUSTA=0" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN @FULLTEXTRESULTADOS R2 ON RESP.ID=R2.IDRESPUESTA " & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN CON WITH(NOLOCK) ON CON.PROVE=RESP.PROVE AND CON.ID=RESP.CON" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN PROVE WITH(NOLOCK) ON PROVE.COD=CON.PROVE" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN CN_USU WITH(NOLOCK) ON CN_USU.COD=RESP.USU" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN CN_USU CN_USU2 WITH(NOLOCK) ON CN_USU2.PROVE=RESP.PROVE AND CN_USU2.CON=RESP.CON" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN USU WITH(NOLOCK) ON USU.COD=RESP.USU" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN PER WITH(NOLOCK) ON PER.COD=USU.PER " & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN DEP WITH(NOLOCK) ON DEP.COD=PER.DEP" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN CON CON2 WITH(NOLOCK) ON CON2.PROVE=RESP.PROVECITADO AND CON2.ID=RESP.CONCITADO   " & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN PROVE PROVE2 WITH(NOLOCK) ON PROVE2.COD=CON2.PROVE    " & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN USU USU2 WITH(NOLOCK) ON USU2.COD=RESP.USUCITADO" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN PER PER2 WITH(NOLOCK) ON PER2.COD=USU2.COD" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN DEP DEP2 WITH(NOLOCK) ON DEP2.COD=PER2.DEP" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN CN_USU_NOTIF NTF WITH(NOLOCK) ON NTF.MEN=RESP.MEN AND NTF.IDRESPUESTA=RESP.ID AND NTF.USU=@USU" & vbCrLf
sConsulta = sConsulta & "ORDER BY RESP.FECACT ASC" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT RESP.ID,RESP.USU,PER.APE,PER.NOM,DEP.DEN SITUACIONUO,CN_USU.GUIDIMAGENUSU," & vbCrLf
sConsulta = sConsulta & "       RESP.MEGUSTA,RESP.FECALTA,RESP.FECACT,RESP.MEN," & vbCrLf
sConsulta = sConsulta & "       USU2.COD USUCITADO,PER2.APE APECITADO,PER2.NOM NOMCITADO,DEP2.DEN SITUACIONUOCITADO,RESP.RESP," & vbCrLf
sConsulta = sConsulta & "       RESP.PROVE,RESP.CON,CON.APE APEPROVE,CON.NOM NOMPROVE,CON.DEP SITUACIONUOPROVE,PROVE.DEN PROVEDEN," & vbCrLf
sConsulta = sConsulta & "       RESP.PROVECITADO,RESP.CONCITADO,CON2.APE APEPROVECITADO,CON2.NOM NOMPROVECITADO," & vbCrLf
sConsulta = sConsulta & "       CON2.DEP SITUACIONUOPROVECITADO,PROVE2.DEN PROVEDENCITADO,CN_USU2.GUIDIMAGENUSU GUIDIMAGENPROVE" & vbCrLf
sConsulta = sConsulta & "FROM @FULLTEXTRESULTADOS R" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN CN_RESP RESP WITH(NOLOCK) ON RESP.MEN=R.IDMENSAJE AND RESP.MEGUSTA=1 AND RESP.RESP IS NULL" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN CON WITH(NOLOCK) ON CON.PROVE=RESP.PROVE AND CON.ID=RESP.CON" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN PROVE WITH(NOLOCK) ON PROVE.COD=CON.PROVE" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN CN_USU WITH(NOLOCK) ON CN_USU.COD=RESP.USU" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN CN_USU CN_USU2 WITH(NOLOCK) ON CN_USU2.PROVE=RESP.PROVE AND CN_USU2.CON=RESP.CON" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN USU WITH(NOLOCK) ON USU.COD=RESP.USU" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN PER WITH(NOLOCK) ON PER.COD=USU.PER " & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN DEP WITH(NOLOCK) ON DEP.COD=PER.DEP" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN CON CON2 WITH(NOLOCK) ON CON2.PROVE=RESP.PROVECITADO AND CON2.ID=RESP.CONCITADO   " & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN PROVE PROVE2 WITH(NOLOCK) ON PROVE2.COD=CON2.PROVE    " & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN USU USU2 WITH(NOLOCK) ON USU2.COD=RESP.USUCITADO" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN PER PER2 WITH(NOLOCK) ON PER2.COD=USU2.COD" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN DEP DEP2 WITH(NOLOCK) ON DEP2.COD=PER2.DEP" & vbCrLf
sConsulta = sConsulta & "ORDER BY RESP.FECACT ASC" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT RESP.ID,RESP.USU,PER.APE,PER.NOM,DEP.DEN SITUACIONUO,CN_USU.GUIDIMAGENUSU," & vbCrLf
sConsulta = sConsulta & "       RESP.MEGUSTA,RESP.FECALTA,RESP.FECACT,RESP.MEN," & vbCrLf
sConsulta = sConsulta & "       USU2.COD USUCITADO,PER2.APE APECITADO,PER2.NOM NOMCITADO,DEP2.DEN SITUACIONUOCITADO,RESP.RESP," & vbCrLf
sConsulta = sConsulta & "       RESP.PROVE,RESP.CON,CON.APE APEPROVE,CON.NOM NOMPROVE,CON.DEP SITUACIONUOPROVE,PROVE.DEN PROVEDEN," & vbCrLf
sConsulta = sConsulta & "       RESP.PROVECITADO,RESP.CONCITADO,CON2.APE APEPROVECITADO,CON2.NOM NOMPROVECITADO," & vbCrLf
sConsulta = sConsulta & "       CON2.DEP SITUACIONUOPROVECITADO,PROVE2.DEN PROVEDENCITADO,CN_USU2.GUIDIMAGENUSU GUIDIMAGENPROVE" & vbCrLf
sConsulta = sConsulta & "FROM @FULLTEXTRESULTADOS R" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN CN_RESP RESP WITH(NOLOCK) ON RESP.MEN=R.IDMENSAJE AND RESP.MEGUSTA=1 AND RESP.RESP IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN CON WITH(NOLOCK) ON CON.PROVE=RESP.PROVE AND CON.ID=RESP.CON" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN PROVE WITH(NOLOCK) ON PROVE.COD=CON.PROVE" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN CN_USU WITH(NOLOCK) ON CN_USU.COD=RESP.USU" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN CN_USU CN_USU2 WITH(NOLOCK) ON CN_USU2.PROVE=RESP.PROVE AND CN_USU2.CON=RESP.CON" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN USU WITH(NOLOCK) ON USU.COD=RESP.USU" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN PER WITH(NOLOCK) ON PER.COD=USU.PER " & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN DEP WITH(NOLOCK) ON DEP.COD=PER.DEP" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN CON CON2 WITH(NOLOCK) ON CON2.PROVE=RESP.PROVECITADO AND CON2.ID=RESP.CONCITADO   " & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN PROVE PROVE2 WITH(NOLOCK) ON PROVE2.COD=CON2.PROVE    " & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN USU USU2 WITH(NOLOCK) ON USU2.COD=RESP.USUCITADO" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN PER PER2 WITH(NOLOCK) ON PER2.COD=USU2.COD" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN DEP DEP2 WITH(NOLOCK) ON DEP2.COD=PER2.DEP" & vbCrLf
sConsulta = sConsulta & "ORDER BY RESP.FECACT ASC" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT ID,NOMBRE,GUID,SIZE,SIZEUNIT,TIPOADJUNTO,FECALTA,R.IDMENSAJE MEN,RESP" & vbCrLf
sConsulta = sConsulta & "FROM @FULLTEXTRESULTADOS R" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN CN_ADJUN ADJUN WITH(NOLOCK) ON ADJUN.MEN=R.IDMENSAJE" & vbCrLf
sConsulta = sConsulta & "ORDER BY FECALTA ASC" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CN_OBTENER_MENSAJE_PROVE]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[CN_OBTENER_MENSAJE_PROVE]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[CN_OBTENER_MENSAJE_PROVE] " & vbCrLf
sConsulta = sConsulta & "   @IDMENSAJE INT," & vbCrLf
sConsulta = sConsulta & "   @PROVE NVARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "   @CON INT," & vbCrLf
sConsulta = sConsulta & "   @IDI NVARCHAR(50)   " & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "BEGIN  " & vbCrLf
sConsulta = sConsulta & "   SELECT MEN.ID,MEN.FECALTA," & vbCrLf
sConsulta = sConsulta & "       MEN.FECACT,MEN.USU,MEN.TIPO,MEN.TITULO,MEN.CONTENIDO,MEN.DONDE,MEN.CUANDO," & vbCrLf
sConsulta = sConsulta & "       PER.APE,PER.NOM,DEP.DEN SITUACIONUO,CN_USU.GUIDIMAGENUSU,LFD.ID IDDISCREPANCIA," & vbCrLf
sConsulta = sConsulta & "       CATNOMBRE.DEN CATEGORIA,CN_CON_NOTIF.CON LEIDO,ISNULL(CN_MEN_CON.OCULTO,0) OCULTO," & vbCrLf
sConsulta = sConsulta & "       MEN.PROVE,MEN.IDCON,CON.APE APEPROVE,CON.NOM NOMPROVE,CON.DEP SITUACIONUOPROVE,PROVE.DEN PROVEDEN " & vbCrLf
sConsulta = sConsulta & "   FROM CN_MEN MEN WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN CN_MEN_CON WITH(NOLOCK) ON CN_MEN_CON.MEN=MEN.ID AND CN_MEN_CON.PROVE=@PROVE AND CN_MEN_CON.CON=@CON  " & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN CN_CON_NOTIF WITH(NOLOCK) ON CN_CON_NOTIF.MEN=MEN.ID AND CN_CON_NOTIF.PROVE=@PROVE AND CN_CON_NOTIF.CON=@CON AND CN_CON_NOTIF.IDRESPUESTA=0       " & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN LIN_FACTURA_DISCRP LFD WITH(NOLOCK) ON LFD.MEN=MEN.ID " & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN USU WITH(NOLOCK) ON USU.COD=MEN.USU" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN PER WITH(NOLOCK) ON PER.COD=USU.PER " & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN DEP WITH(NOLOCK) ON DEP.COD=PER.DEP" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN CON WITH(NOLOCK) ON CON.PROVE=@PROVE AND CON.ID=@CON" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN PROVE WITH(NOLOCK) ON PROVE.COD=CON.PROVE" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN CN_USU WITH(NOLOCK) ON CN_USU.COD=MEN.USU" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN CN_CAT_IDI CATNOMBRE WITH(NOLOCK) ON CATNOMBRE.CAT=MEN.CAT AND CATNOMBRE.IDI=@IDI" & vbCrLf
sConsulta = sConsulta & "   WHERE MEN.ID=@IDMENSAJE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SELECT RESP.ID,NTF.IDRESPUESTA,RESP.USU,PER.APE,PER.NOM,DEP.DEN SITUACIONUO,CN_USU.GUIDIMAGENUSU," & vbCrLf
sConsulta = sConsulta & "       RESP.CONTENIDO,RESP.MEGUSTA,RESP.FECALTA,RESP.FECACT,RESP.MEN," & vbCrLf
sConsulta = sConsulta & "       USU2.COD USUCITADO,PER2.APE APECITADO,PER2.NOM NOMCITADO,DEP2.DEN SITUACIONUOCITADO,RESP.RESP," & vbCrLf
sConsulta = sConsulta & "       RESP.PROVE,RESP.CON,CON.APE APEPROVE,CON.NOM NOMPROVE,CON.DEP SITUACIONUOPROVE,PROVE.DEN PROVEDEN," & vbCrLf
sConsulta = sConsulta & "       RESP.PROVECITADO,RESP.CONCITADO,CON2.APE APEPROVECITADO,CON2.NOM NOMPROVECITADO," & vbCrLf
sConsulta = sConsulta & "       CON2.DEP SITUACIONUOPROVECITADO,PROVE2.DEN PROVEDENCITADO,CN_USU2.GUIDIMAGENUSU GUIDIMAGENPROVE" & vbCrLf
sConsulta = sConsulta & "   FROM CN_MEN MEN WITH(NOLOCK)        " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN CN_RESP RESP WITH(NOLOCK) ON RESP.MEN=MEN.ID AND RESP.MEGUSTA=0           " & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN LIN_FACTURA_DISCRP LFD WITH(NOLOCK) ON LFD.MEN=MEN.ID " & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN CON WITH(NOLOCK) ON CON.PROVE=RESP.PROVE AND CON.ID=RESP.CON" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN PROVE WITH(NOLOCK) ON PROVE.COD=CON.PROVE" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN CN_USU WITH(NOLOCK) ON CN_USU.COD=RESP.USU" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN CN_USU CN_USU2 WITH(NOLOCK) ON CN_USU2.PROVE=RESP.PROVE AND CN_USU2.CON=RESP.CON" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN USU WITH(NOLOCK) ON USU.COD=RESP.USU" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN PER WITH(NOLOCK) ON PER.COD=USU.PER " & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN DEP WITH(NOLOCK) ON DEP.COD=PER.DEP" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN CON CON2 WITH(NOLOCK) ON CON2.PROVE=RESP.PROVECITADO AND CON2.ID=RESP.CONCITADO   " & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN PROVE PROVE2 WITH(NOLOCK) ON PROVE2.COD=CON2.PROVE    " & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN USU USU2 WITH(NOLOCK) ON USU2.COD=RESP.USUCITADO" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN PER PER2 WITH(NOLOCK) ON PER2.COD=USU2.COD" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN DEP DEP2 WITH(NOLOCK) ON DEP2.COD=PER2.DEP" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN CN_CON_NOTIF NTF WITH(NOLOCK) ON NTF.MEN=RESP.MEN AND NTF.IDRESPUESTA=RESP.ID AND NTF.PROVE=@PROVE AND NTF.CON=@CON" & vbCrLf
sConsulta = sConsulta & "   WHERE MEN.ID=@IDMENSAJE AND (LFD.ID IS NOT NULL OR " & vbCrLf
sConsulta = sConsulta & "       (LFD.ID IS NULL AND (RESP.PROVE=@PROVE OR RESP.PROVECITADO=@PROVE)))" & vbCrLf
sConsulta = sConsulta & "   ORDER BY RESP.FECACT ASC" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SELECT RESP.ID,NTF.IDRESPUESTA,RESP.USU,PER.APE,PER.NOM,DEP.DEN SITUACIONUO," & vbCrLf
sConsulta = sConsulta & "       RESP.CONTENIDO,RESP.MEGUSTA,RESP.FECALTA,RESP.FECACT,RESP.MEN," & vbCrLf
sConsulta = sConsulta & "       USU2.COD USUCITADO,PER2.APE APECITADO,PER2.NOM NOMCITADO,DEP2.DEN SITUACIONUOCITADO,RESP.RESP," & vbCrLf
sConsulta = sConsulta & "       RESP.PROVE,RESP.CON,CON.APE APEPROVE,CON.NOM NOMPROVE,CON.DEP SITUACIONUOPROVE,PROVE.DEN PROVEDEN," & vbCrLf
sConsulta = sConsulta & "       RESP.PROVECITADO,RESP.CONCITADO,CON2.APE APEPROVECITADO,CON2.NOM NOMPROVECITADO," & vbCrLf
sConsulta = sConsulta & "       CON2.DEP SITUACIONUOPROVECITADO,PROVE2.DEN PROVEDENCITADO" & vbCrLf
sConsulta = sConsulta & "   FROM CN_MEN MEN WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN CN_RESP RESP WITH(NOLOCK) ON RESP.MEN=MEN.ID AND RESP.MEGUSTA=1 AND RESP.RESP IS NULL" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN LIN_FACTURA_DISCRP LFD WITH(NOLOCK) ON LFD.MEN=MEN.ID" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN CON WITH(NOLOCK) ON CON.PROVE=RESP.PROVE AND CON.ID=RESP.CON" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN PROVE WITH(NOLOCK) ON PROVE.COD=CON.PROVE" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN USU WITH(NOLOCK) ON USU.COD=RESP.USU" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN PER WITH(NOLOCK) ON PER.COD=USU.PER " & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN DEP WITH(NOLOCK) ON DEP.COD=PER.DEP" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN CON CON2 WITH(NOLOCK) ON CON2.PROVE=RESP.PROVECITADO AND CON2.ID=RESP.CONCITADO" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN PROVE PROVE2 WITH(NOLOCK) ON PROVE2.COD=CON2.PROVE        " & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN USU USU2 WITH(NOLOCK) ON USU2.COD=RESP.USUCITADO" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN PER PER2 WITH(NOLOCK) ON PER2.COD=USU2.COD" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN DEP DEP2 WITH(NOLOCK) ON DEP2.COD=PER2.DEP" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN CN_CON_NOTIF NTF WITH(NOLOCK) ON NTF.MEN=RESP.MEN AND NTF.IDRESPUESTA=RESP.ID AND NTF.PROVE=@PROVE AND NTF.CON=@CON" & vbCrLf
sConsulta = sConsulta & "   WHERE MEN.ID=@IDMENSAJE AND (LFD.ID IS NOT NULL OR " & vbCrLf
sConsulta = sConsulta & "       (LFD.ID IS NULL AND (RESP.PROVE=@PROVE OR RESP.PROVECITADO=@PROVE)))" & vbCrLf
sConsulta = sConsulta & "   ORDER BY RESP.FECACT DESC" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SELECT RESP.ID,NTF.IDRESPUESTA,RESP.USU,PER.APE,PER.NOM,DEP.DEN SITUACIONUO," & vbCrLf
sConsulta = sConsulta & "       RESP.CONTENIDO,RESP.MEGUSTA,RESP.FECALTA,RESP.FECACT,RESP.MEN," & vbCrLf
sConsulta = sConsulta & "       USU2.COD USUCITADO,PER2.APE APECITADO,PER2.NOM NOMCITADO,DEP2.DEN SITUACIONUOCITADO,RESP.RESP," & vbCrLf
sConsulta = sConsulta & "       RESP.PROVE,RESP.CON,CON.APE APEPROVE,CON.NOM NOMPROVE,CON.DEP SITUACIONUOPROVE,PROVE.DEN PROVEDEN," & vbCrLf
sConsulta = sConsulta & "       RESP.PROVECITADO,RESP.CONCITADO,CON2.APE APEPROVECITADO,CON2.NOM NOMPROVECITADO," & vbCrLf
sConsulta = sConsulta & "       CON2.DEP SITUACIONUOPROVECITADO,PROVE2.DEN PROVEDENCITADO" & vbCrLf
sConsulta = sConsulta & "   FROM CN_MEN MEN WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN CN_RESP RESP WITH(NOLOCK) ON RESP.MEN=MEN.ID AND RESP.MEGUSTA=1 AND RESP.RESP IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN LIN_FACTURA_DISCRP LFD WITH(NOLOCK) ON LFD.MEN=MEN.ID" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN CON WITH(NOLOCK) ON CON.PROVE=RESP.PROVE AND CON.ID=RESP.CON" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN PROVE WITH(NOLOCK) ON PROVE.COD=CON.PROVE" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN USU WITH(NOLOCK) ON USU.COD=RESP.USU" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN PER WITH(NOLOCK) ON PER.COD=USU.PER " & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN DEP WITH(NOLOCK) ON DEP.COD=PER.DEP" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN CON CON2 WITH(NOLOCK) ON CON2.PROVE=RESP.PROVECITADO AND CON2.ID=RESP.CONCITADO" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN PROVE PROVE2 WITH(NOLOCK) ON PROVE2.COD=CON2.PROVE        " & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN USU USU2 WITH(NOLOCK) ON USU2.COD=RESP.USUCITADO" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN PER PER2 WITH(NOLOCK) ON PER2.COD=USU2.COD" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN DEP DEP2 WITH(NOLOCK) ON DEP2.COD=PER2.DEP" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN CN_CON_NOTIF NTF WITH(NOLOCK) ON NTF.MEN=RESP.MEN AND NTF.IDRESPUESTA=RESP.ID AND NTF.PROVE=@PROVE AND NTF.CON=@CON" & vbCrLf
sConsulta = sConsulta & "   WHERE MEN.ID=@IDMENSAJE AND (LFD.ID IS NOT NULL OR " & vbCrLf
sConsulta = sConsulta & "       (LFD.ID IS NULL AND (RESP.PROVE=@PROVE OR RESP.PROVECITADO=@PROVE)))" & vbCrLf
sConsulta = sConsulta & "   ORDER BY RESP.FECACT DESC" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SELECT ADJUN.ID,ADJUN.NOMBRE,ADJUN.GUID,ADJUN.SIZE,ADJUN.SIZEUNIT,ADJUN.TIPOADJUNTO,ADJUN.FECALTA," & vbCrLf
sConsulta = sConsulta & "       ADJUN.MEN,ADJUN.RESP" & vbCrLf
sConsulta = sConsulta & "   FROM CN_MEN MEN WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN CN_ADJUN ADJUN WITH(NOLOCK) ON ADJUN.MEN=MEN.ID" & vbCrLf
sConsulta = sConsulta & "   WHERE MEN.ID=@IDMENSAJE" & vbCrLf
sConsulta = sConsulta & "   ORDER BY ADJUN.FECALTA ASC" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CN_BACKUPCN]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[CN_BACKUPCN]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[CN_BACKUPCN]" & vbCrLf
sConsulta = sConsulta & "   @PERIODOEXC INT," & vbCrLf
sConsulta = sConsulta & "   @LIMITEREG INT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   DECLARE @contador int" & vbCrLf
sConsulta = sConsulta & "   DECLARE @meses int" & vbCrLf
sConsulta = sConsulta & "   DECLARE @fecha datetime" & vbCrLf
sConsulta = sConsulta & "   DECLARE @id int" & vbCrLf
sConsulta = sConsulta & "   SET @contador = @LIMITEREG" & vbCrLf
sConsulta = sConsulta & "   SET @meses = @PERIODOEXC * -1" & vbCrLf
sConsulta = sConsulta & "   SET @fecha = getdate()" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   WHILE (@contador >= @LIMITEREG)" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       SET @fecha = DATEADD(month, @meses, getdate())" & vbCrLf
sConsulta = sConsulta & "       SELECT @contador = COUNT(ID) FROM cn_men WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       WHERE fecact > @fecha" & vbCrLf
sConsulta = sConsulta & "       SET @meses = @meses + 1" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   DECLARE cMensajes CURSOR FOR" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SELECT  ID" & vbCrLf
sConsulta = sConsulta & "   FROM CN_MEN WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   WHERE fecact <= @fecha" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   -- Apertura del cursor" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   OPEN cMensajes" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   -- Lectura de la primera fila del cursor" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   FETCH cMensajes" & vbCrLf
sConsulta = sConsulta & "   INTO @id" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   WHILE (@@FETCH_STATUS = 0 )" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   -- Insertar registros en tablas de hist�rico" & vbCrLf
sConsulta = sConsulta & "       INSERT INTO CN_H_MEN " & vbCrLf
sConsulta = sConsulta & "           --CALIDAD Se usa SELECT * para optimizar el mantenimiento de este STORED y para detectar errores si los cambios no se han efectuado en las tablas de hist�rico" & vbCrLf
sConsulta = sConsulta & "           SELECT * FROM CN_MEN  WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "           WHERE ID = @id" & vbCrLf
sConsulta = sConsulta & "       INSERT INTO CN_H_OPT_MEN_USU" & vbCrLf
sConsulta = sConsulta & "           --CALIDAD" & vbCrLf
sConsulta = sConsulta & "           SELECT * FROM CN_OPT_MEN_USU  WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "           WHERE MEN = @id" & vbCrLf
sConsulta = sConsulta & "       INSERT INTO CN_H_MEN_USU" & vbCrLf
sConsulta = sConsulta & "           --CALIDAD" & vbCrLf
sConsulta = sConsulta & "           SELECT * FROM CN_MEN_USU  WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "           WHERE MEN = @id" & vbCrLf
sConsulta = sConsulta & "       INSERT INTO CN_H_MEN_UON1" & vbCrLf
sConsulta = sConsulta & "           --CALIDAD" & vbCrLf
sConsulta = sConsulta & "           SELECT * FROM CN_MEN_UON1  WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "           WHERE MEN = @id" & vbCrLf
sConsulta = sConsulta & "       INSERT INTO CN_H_MEN_UON2" & vbCrLf
sConsulta = sConsulta & "           --CALIDAD" & vbCrLf
sConsulta = sConsulta & "           SELECT * FROM CN_MEN_UON2  WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "           WHERE MEN = @id " & vbCrLf
sConsulta = sConsulta & "       INSERT INTO CN_H_MEN_UON3" & vbCrLf
sConsulta = sConsulta & "           --CALIDAD" & vbCrLf
sConsulta = sConsulta & "           SELECT * FROM CN_MEN_UON3  WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "           WHERE MEN = @id" & vbCrLf
sConsulta = sConsulta & "       INSERT INTO CN_H_MEN_UON0_DEP" & vbCrLf
sConsulta = sConsulta & "           --CALIDAD" & vbCrLf
sConsulta = sConsulta & "           SELECT * FROM CN_MEN_UON0_DEP  WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "           WHERE MEN = @id" & vbCrLf
sConsulta = sConsulta & "       INSERT INTO CN_H_MEN_UON1_DEP" & vbCrLf
sConsulta = sConsulta & "           --CALIDAD" & vbCrLf
sConsulta = sConsulta & "           SELECT * FROM CN_MEN_UON1_DEP  WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "           WHERE MEN = @id" & vbCrLf
sConsulta = sConsulta & "       INSERT INTO CN_H_MEN_UON2_DEP" & vbCrLf
sConsulta = sConsulta & "           --CALIDAD" & vbCrLf
sConsulta = sConsulta & "           SELECT * FROM CN_MEN_UON2_DEP  WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "           WHERE MEN = @id" & vbCrLf
sConsulta = sConsulta & "       INSERT INTO CN_H_MEN_UON3_DEP" & vbCrLf
sConsulta = sConsulta & "           --CALIDAD" & vbCrLf
sConsulta = sConsulta & "           SELECT * FROM CN_MEN_UON3_DEP  WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "           WHERE MEN = @id" & vbCrLf
sConsulta = sConsulta & "       INSERT INTO CN_H_MEN_CON" & vbCrLf
sConsulta = sConsulta & "           --CALIDAD" & vbCrLf
sConsulta = sConsulta & "           SELECT * FROM CN_MEN_CON  WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "           WHERE MEN = @id" & vbCrLf
sConsulta = sConsulta & "       INSERT INTO CN_H_MEN_EQP" & vbCrLf
sConsulta = sConsulta & "           --CALIDAD" & vbCrLf
sConsulta = sConsulta & "           SELECT * FROM CN_MEN_EQP  WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "           WHERE MEN = @id" & vbCrLf
sConsulta = sConsulta & "       INSERT INTO CN_H_MEN_GRUPO" & vbCrLf
sConsulta = sConsulta & "           --CALIDAD" & vbCrLf
sConsulta = sConsulta & "           SELECT * FROM CN_MEN_GRUPO  WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "           WHERE MEN = @id" & vbCrLf
sConsulta = sConsulta & "       INSERT INTO CN_H_RESP" & vbCrLf
sConsulta = sConsulta & "           --CALIDAD" & vbCrLf
sConsulta = sConsulta & "           SELECT * FROM CN_RESP  WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "           WHERE MEN = @id" & vbCrLf
sConsulta = sConsulta & "        INSERT INTO CN_H_MEN_ADJUN(ID,NOMBRE,DATA,GUID,DGUID,SIZE,SIZEUNIT,TIPOADJUNTO,FECALTA,RESP,MEN)"
sConsulta = sConsulta & "           SELECT ID,NOMBRE,DATA,GUID,DGUID,SIZE,SIZEUNIT,TIPOADJUNTO,FECALTA,RESP,MEN FROM CN_ADJUN WITH(NOLOCK)"
sConsulta = sConsulta & "           WHERE MEN = @id"
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   -- Borrar registros en tablas" & vbCrLf
sConsulta = sConsulta & "       DELETE FROM CN_OPT_MEN_USU" & vbCrLf
sConsulta = sConsulta & "       WHERE MEN = @id" & vbCrLf
sConsulta = sConsulta & "       DELETE FROM CN_MEN_USU" & vbCrLf
sConsulta = sConsulta & "       WHERE MEN = @id" & vbCrLf
sConsulta = sConsulta & "       DELETE FROM CN_MEN_UON1" & vbCrLf
sConsulta = sConsulta & "       WHERE MEN = @id" & vbCrLf
sConsulta = sConsulta & "       DELETE FROM CN_MEN_UON2" & vbCrLf
sConsulta = sConsulta & "       WHERE MEN = @id" & vbCrLf
sConsulta = sConsulta & "       DELETE FROM CN_MEN_UON3" & vbCrLf
sConsulta = sConsulta & "       WHERE MEN = @id" & vbCrLf
sConsulta = sConsulta & "       DELETE FROM CN_MEN_UON0_DEP" & vbCrLf
sConsulta = sConsulta & "       WHERE MEN = @id" & vbCrLf
sConsulta = sConsulta & "       DELETE FROM CN_MEN_UON1_DEP" & vbCrLf
sConsulta = sConsulta & "       WHERE MEN = @id" & vbCrLf
sConsulta = sConsulta & "       DELETE FROM CN_MEN_UON2_DEP" & vbCrLf
sConsulta = sConsulta & "       WHERE MEN = @id" & vbCrLf
sConsulta = sConsulta & "       DELETE FROM CN_MEN_UON3_DEP" & vbCrLf
sConsulta = sConsulta & "       WHERE MEN = @id" & vbCrLf
sConsulta = sConsulta & "       DELETE FROM CN_MEN_CON" & vbCrLf
sConsulta = sConsulta & "       WHERE MEN = @id" & vbCrLf
sConsulta = sConsulta & "       DELETE FROM CN_MEN_EQP" & vbCrLf
sConsulta = sConsulta & "       WHERE MEN = @id" & vbCrLf
sConsulta = sConsulta & "       DELETE FROM CN_MEN_GRUPO" & vbCrLf
sConsulta = sConsulta & "       WHERE MEN = @id" & vbCrLf
sConsulta = sConsulta & "       DELETE FROM CN_ADJUN" & vbCrLf
sConsulta = sConsulta & "       WHERE MEN = @id" & vbCrLf
sConsulta = sConsulta & "       DELETE FROM CN_RESP" & vbCrLf
sConsulta = sConsulta & "       WHERE MEN = @id" & vbCrLf
sConsulta = sConsulta & "       DELETE FROM CN_MEN" & vbCrLf
sConsulta = sConsulta & "       WHERE ID = @id" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   -- Lectura de la siguiente fila del cursor" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       FETCH cMensajes " & vbCrLf
sConsulta = sConsulta & "       INTO @id" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   -- Cierre del cursor" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   CLOSE cMensajes" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   -- Liberar los recursos" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   DEALLOCATE cMensajes" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


End Sub

Private Sub V_31900_8_Tablas_0994()
    Dim sConsulta As String
    
sConsulta = "IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[CN_H_MEN]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & " CREATE TABLE [dbo].[CN_H_MEN](" & vbCrLf
sConsulta = sConsulta & "            [ID] [int] NOT NULL," & vbCrLf
sConsulta = sConsulta & "            [TIPO] [tinyint] NOT NULL," & vbCrLf
sConsulta = sConsulta & "            [TITULO] [nvarchar](500) NULL," & vbCrLf
sConsulta = sConsulta & "            [CONTENIDO] [nvarchar](max) NOT NULL," & vbCrLf
sConsulta = sConsulta & "            [DONDE] [nvarchar](500) NULL," & vbCrLf
sConsulta = sConsulta & "            [CUANDO] [datetime] NULL," & vbCrLf
sConsulta = sConsulta & "            [FECALTA] [datetime] NULL," & vbCrLf
sConsulta = sConsulta & "            [FECACT] [datetime] NULL," & vbCrLf
sConsulta = sConsulta & "            [USU] [varchar](" & gLongitudesDeCodigos.giLongCodUSU & ") NULL," & vbCrLf
sConsulta = sConsulta & "            [CAT] [int] NOT NULL," & vbCrLf
sConsulta = sConsulta & "            [PROCE_ANYO] [smallint] NULL," & vbCrLf
sConsulta = sConsulta & "            [PROCE_GMN1] [varchar](3) NULL," & vbCrLf
sConsulta = sConsulta & "            [PROCE_COD] [smallint] NULL," & vbCrLf
sConsulta = sConsulta & "            [PROCEPARARESP] [bit] NOT NULL," & vbCrLf
sConsulta = sConsulta & "            [PROCEPARAINVITADO] [bit] NOT NULL," & vbCrLf
sConsulta = sConsulta & "            [PROCEPARACOMP] [bit] NOT NULL," & vbCrLf
sConsulta = sConsulta & "            [PROCEPARAPROVE] [bit] NOT NULL," & vbCrLf
sConsulta = sConsulta & "            [UON0] [bit] NOT NULL," & vbCrLf
sConsulta = sConsulta & "            [GMN1] [varchar](" & gLongitudesDeCodigos.giLongCodGMN1 & ") NULL," & vbCrLf
sConsulta = sConsulta & "            [GMN2] [varchar](" & gLongitudesDeCodigos.giLongCodGMN2 & ") NULL," & vbCrLf
sConsulta = sConsulta & "            [GMN3] [varchar](" & gLongitudesDeCodigos.giLongCodGMN3 & ") NULL," & vbCrLf
sConsulta = sConsulta & "            [GMN4] [varchar](" & gLongitudesDeCodigos.giLongCodGMN4 & ") NULL," & vbCrLf
sConsulta = sConsulta & "            [COMPPARA] [bit] NULL," & vbCrLf
sConsulta = sConsulta & "            [MATERIALES_QA] [int] NULL," & vbCrLf
sConsulta = sConsulta & "            [PROVEPARA] [tinyint] NULL," & vbCrLf
sConsulta = sConsulta & "            [USUQAPARA] [bit] NULL," & vbCrLf
sConsulta = sConsulta & "            [USUPMPARA] [bit] NULL," & vbCrLf
sConsulta = sConsulta & "            [USUEPPARA] [bit] NULL," & vbCrLf
sConsulta = sConsulta & "            [USUGSPARA] [bit] NULL," & vbCrLf
sConsulta = sConsulta & "            [USUSMPARA] [bit] NULL," & vbCrLf
sConsulta = sConsulta & "            [DESPUBENRED] [bit] NOT NULL," & vbCrLf
sConsulta = sConsulta & "            [PROVE] [varchar](" & gLongitudesDeCodigos.giLongCodPROVE & ") NULL," & vbCrLf
sConsulta = sConsulta & "            [IDCON] [int] NULL," & vbCrLf
sConsulta = sConsulta & "CONSTRAINT [PK_CN_H_MEN] PRIMARY KEY CLUSTERED " & vbCrLf
sConsulta = sConsulta & "(" & vbCrLf
sConsulta = sConsulta & "            [ID] DESC" & vbCrLf
sConsulta = sConsulta & ")WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]" & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & " SET ANSI_PADDING OFF" & vbCrLf
sConsulta = sConsulta & " EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1.- Tema.2.- Evento.3.- Noticia.4.- Pregunta.5.-Mensaje urgente' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CN_H_MEN', @level2type=N'COLUMN',@level2name=N'TIPO'" & vbCrLf
sConsulta = sConsulta & " EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1-Todos los proveedores,2-Proveedores calidad,3-Proveedores potenciales,4-Proveedores reales calidad,5-Proveedores de un material concreto de QA,6-Proveedores de una rama de material' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'CN_H_MEN', @level2type=N'COLUMN',@level2name=N'PROVEPARA'" & vbCrLf
sConsulta = sConsulta & " ALTER TABLE [dbo].[CN_H_MEN]  WITH NOCHECK ADD  CONSTRAINT [FK_CN_H_MEN_MATERIALES_QA] FOREIGN KEY([MATERIALES_QA])" & vbCrLf
sConsulta = sConsulta & " REFERENCES [dbo].[MATERIALES_QA] ([ID])" & vbCrLf
sConsulta = sConsulta & " ALTER TABLE [dbo].[CN_H_MEN] CHECK CONSTRAINT [FK_CN_H_MEN_MATERIALES_QA]" & vbCrLf
sConsulta = sConsulta & " ALTER TABLE [dbo].[CN_H_MEN] ADD  CONSTRAINT [DF_CN_H_MEN_PROCEPARARESP]  DEFAULT ((0)) FOR [PROCEPARARESP]" & vbCrLf
sConsulta = sConsulta & " ALTER TABLE [dbo].[CN_H_MEN] ADD  CONSTRAINT [DF_CN_H_MEN_PROCEPARAINVITADO]  DEFAULT ((0)) FOR [PROCEPARAINVITADO]" & vbCrLf
sConsulta = sConsulta & " ALTER TABLE [dbo].[CN_H_MEN] ADD  CONSTRAINT [DF_CN_H_MEN_PROCEPARACOMP]  DEFAULT ((0)) FOR [PROCEPARACOMP]" & vbCrLf
sConsulta = sConsulta & " ALTER TABLE [dbo].[CN_H_MEN] ADD  CONSTRAINT [DF_CN_H_MEN_PROCEPARAPROVE]  DEFAULT ((0)) FOR [PROCEPARAPROVE]" & vbCrLf
sConsulta = sConsulta & " ALTER TABLE [dbo].[CN_H_MEN] ADD  CONSTRAINT [DF_CN_H_MEN_UON0]  DEFAULT ((0)) FOR [UON0]" & vbCrLf
sConsulta = sConsulta & " ALTER TABLE [dbo].[CN_H_MEN] ADD  CONSTRAINT [DF_CN_H_MEN_COMPPARA]  DEFAULT ((0)) FOR [COMPPARA]" & vbCrLf
sConsulta = sConsulta & " ALTER TABLE [dbo].[CN_H_MEN] ADD  CONSTRAINT [DF_CN_H_MEN_PROVEPARA]  DEFAULT ((0)) FOR [PROVEPARA]" & vbCrLf
sConsulta = sConsulta & " ALTER TABLE [dbo].[CN_H_MEN] ADD  CONSTRAINT [DF_CN_H_MEN_DESPUBENRED]  DEFAULT ((0)) FOR [DESPUBENRED]" & vbCrLf
sConsulta = sConsulta & "END"
ExecuteSQL gRDOCon, sConsulta
    
    
sConsulta = "IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[CN_H_OPT_MEN_USU]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE [dbo].[CN_H_OPT_MEN_USU](" & vbCrLf
sConsulta = sConsulta & "            [MEN] [int] NOT NULL," & vbCrLf
sConsulta = sConsulta & "            [USU] [varchar](" & gLongitudesDeCodigos.giLongCodUSU & ") NOT NULL," & vbCrLf
sConsulta = sConsulta & "            [OCULTO] [bit] NOT NULL," & vbCrLf
sConsulta = sConsulta & "CONSTRAINT [PK_CN_H_OPT_MEN_USU] PRIMARY KEY CLUSTERED " & vbCrLf
sConsulta = sConsulta & "(" & vbCrLf
sConsulta = sConsulta & "            [MEN] ASC," & vbCrLf
sConsulta = sConsulta & "            [USU] ASC" & vbCrLf
sConsulta = sConsulta & ")WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]" & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
sConsulta = sConsulta & " SET ANSI_PADDING OFF" & vbCrLf
sConsulta = sConsulta & " ALTER TABLE [dbo].[CN_H_OPT_MEN_USU]  WITH CHECK ADD  CONSTRAINT [FK_CN_H_OPT_MEN_USU_CN_H_MEN] FOREIGN KEY([MEN])" & vbCrLf
sConsulta = sConsulta & "REFERENCES [dbo].[CN_H_MEN] ([ID])" & vbCrLf
sConsulta = sConsulta & " ALTER TABLE [dbo].[CN_H_OPT_MEN_USU] CHECK CONSTRAINT [FK_CN_H_OPT_MEN_USU_CN_H_MEN]" & vbCrLf
sConsulta = sConsulta & " ALTER TABLE [dbo].[CN_H_OPT_MEN_USU]  WITH CHECK ADD  CONSTRAINT [FK_CN_H_OPT_MEN_USU_USU] FOREIGN KEY([USU])" & vbCrLf
sConsulta = sConsulta & "REFERENCES [dbo].[USU] ([COD])" & vbCrLf
sConsulta = sConsulta & " ALTER TABLE [dbo].[CN_H_OPT_MEN_USU] CHECK CONSTRAINT [FK_CN_H_OPT_MEN_USU_USU]" & vbCrLf
sConsulta = sConsulta & " ALTER TABLE [dbo].[CN_H_OPT_MEN_USU] ADD  CONSTRAINT [DF_CN_H_OPT_MEN_USU_Oculto]  DEFAULT ((0)) FOR [OCULTO]" & vbCrLf
sConsulta = sConsulta & "END"
ExecuteSQL gRDOCon, sConsulta


sConsulta = "IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[CN_H_MEN_USU]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE [dbo].[CN_H_MEN_USU](" & vbCrLf
sConsulta = sConsulta & "            [MEN] [int] NOT NULL," & vbCrLf
sConsulta = sConsulta & "            [USU] [varchar](" & gLongitudesDeCodigos.giLongCodUSU & ") NOT NULL," & vbCrLf
sConsulta = sConsulta & "CONSTRAINT [PK_CN_H_MEN_USU] PRIMARY KEY CLUSTERED " & vbCrLf
sConsulta = sConsulta & "(" & vbCrLf
sConsulta = sConsulta & "            [MEN] ASC," & vbCrLf
sConsulta = sConsulta & "            [USU] ASC" & vbCrLf
sConsulta = sConsulta & ")WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]" & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
sConsulta = sConsulta & " SET ANSI_PADDING OFF" & vbCrLf
sConsulta = sConsulta & " ALTER TABLE [dbo].[CN_H_MEN_USU]  WITH CHECK ADD  CONSTRAINT [FK_CN_H_MEN_USU_CN_H_MEN] FOREIGN KEY([MEN])" & vbCrLf
sConsulta = sConsulta & "REFERENCES [dbo].[CN_H_MEN] ([ID])" & vbCrLf
sConsulta = sConsulta & " ALTER TABLE [dbo].[CN_H_MEN_USU] CHECK CONSTRAINT [FK_CN_H_MEN_USU_CN_H_MEN]" & vbCrLf
sConsulta = sConsulta & " ALTER TABLE [dbo].[CN_H_MEN_USU]  WITH CHECK ADD  CONSTRAINT [FK_CN_H_MEN_USU_USU] FOREIGN KEY([USU])" & vbCrLf
sConsulta = sConsulta & "REFERENCES [dbo].[USU] ([COD])" & vbCrLf
sConsulta = sConsulta & " ALTER TABLE [dbo].[CN_H_MEN_USU] CHECK CONSTRAINT [FK_CN_H_MEN_USU_USU]" & vbCrLf
sConsulta = sConsulta & "END"
ExecuteSQL gRDOCon, sConsulta


sConsulta = "IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[CN_H_MEN_UON1]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE [dbo].[CN_H_MEN_UON1](" & vbCrLf
sConsulta = sConsulta & "            [MEN] [int] NOT NULL," & vbCrLf
sConsulta = sConsulta & "            [UON1] [varchar](" & gLongitudesDeCodigos.giLongCodUON1 & ") NOT NULL," & vbCrLf
sConsulta = sConsulta & "CONSTRAINT [PK_CN_H_MEN_UON1] PRIMARY KEY CLUSTERED " & vbCrLf
sConsulta = sConsulta & "(" & vbCrLf
sConsulta = sConsulta & "            [MEN] ASC," & vbCrLf
sConsulta = sConsulta & "            [UON1] ASC" & vbCrLf
sConsulta = sConsulta & ")WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]" & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
sConsulta = sConsulta & " SET ANSI_PADDING OFF" & vbCrLf
sConsulta = sConsulta & " ALTER TABLE [dbo].[CN_H_MEN_UON1]  WITH CHECK ADD  CONSTRAINT [FK_CN_H_MEN_UON1_CN_H_MEN_UON1] FOREIGN KEY([MEN])" & vbCrLf
sConsulta = sConsulta & "REFERENCES [dbo].[CN_H_MEN] ([ID])" & vbCrLf
sConsulta = sConsulta & " ALTER TABLE [dbo].[CN_H_MEN_UON1] CHECK CONSTRAINT [FK_CN_H_MEN_UON1_CN_H_MEN_UON1]" & vbCrLf
sConsulta = sConsulta & " ALTER TABLE [dbo].[CN_H_MEN_UON1]  WITH CHECK ADD  CONSTRAINT [FK_CN_H_MEN_UON1_UON1] FOREIGN KEY([UON1])" & vbCrLf
sConsulta = sConsulta & "REFERENCES [dbo].[UON1] ([COD])" & vbCrLf
sConsulta = sConsulta & " ALTER TABLE [dbo].[CN_H_MEN_UON1] CHECK CONSTRAINT [FK_CN_H_MEN_UON1_UON1]" & vbCrLf
sConsulta = sConsulta & "END"
ExecuteSQL gRDOCon, sConsulta


sConsulta = "IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[CN_H_MEN_UON2]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE [dbo].[CN_H_MEN_UON2](" & vbCrLf
sConsulta = sConsulta & "            [MEN] [int] NOT NULL," & vbCrLf
sConsulta = sConsulta & "            [UON1] [varchar](" & gLongitudesDeCodigos.giLongCodUON1 & ") NOT NULL," & vbCrLf
sConsulta = sConsulta & "            [UON2] [varchar](" & gLongitudesDeCodigos.giLongCodUON2 & ") NOT NULL," & vbCrLf
sConsulta = sConsulta & "CONSTRAINT [PK_CN_H_MEN_UON2] PRIMARY KEY CLUSTERED " & vbCrLf
sConsulta = sConsulta & "(" & vbCrLf
sConsulta = sConsulta & "            [MEN] ASC," & vbCrLf
sConsulta = sConsulta & "            [UON1] ASC," & vbCrLf
sConsulta = sConsulta & "            [UON2] ASC" & vbCrLf
sConsulta = sConsulta & ")WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]" & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
sConsulta = sConsulta & " SET ANSI_PADDING OFF" & vbCrLf
sConsulta = sConsulta & " ALTER TABLE [dbo].[CN_H_MEN_UON2]  WITH CHECK ADD  CONSTRAINT [FK_CN_H_MEN_UON2_CN_H_MEN] FOREIGN KEY([MEN])" & vbCrLf
sConsulta = sConsulta & "REFERENCES [dbo].[CN_H_MEN] ([ID])" & vbCrLf
sConsulta = sConsulta & " ALTER TABLE [dbo].[CN_H_MEN_UON2] CHECK CONSTRAINT [FK_CN_H_MEN_UON2_CN_H_MEN]" & vbCrLf
sConsulta = sConsulta & " ALTER TABLE [dbo].[CN_H_MEN_UON2]  WITH CHECK ADD  CONSTRAINT [FK_CN_H_MEN_UON2_UON2] FOREIGN KEY([UON1], [UON2])" & vbCrLf
sConsulta = sConsulta & "REFERENCES [dbo].[UON2] ([UON1], [COD])" & vbCrLf
sConsulta = sConsulta & " ALTER TABLE [dbo].[CN_H_MEN_UON2] CHECK CONSTRAINT [FK_CN_H_MEN_UON2_UON2]" & vbCrLf
sConsulta = sConsulta & "END"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[CN_H_MEN_UON3]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE [dbo].[CN_H_MEN_UON3](" & vbCrLf
sConsulta = sConsulta & "            [MEN] [int] NOT NULL," & vbCrLf
sConsulta = sConsulta & "            [UON1] [varchar](" & gLongitudesDeCodigos.giLongCodUON1 & ") NOT NULL," & vbCrLf
sConsulta = sConsulta & "            [UON2] [varchar](" & gLongitudesDeCodigos.giLongCodUON2 & ") NOT NULL," & vbCrLf
sConsulta = sConsulta & "            [UON3] [varchar](" & gLongitudesDeCodigos.giLongCodUON3 & ") NOT NULL," & vbCrLf
sConsulta = sConsulta & "CONSTRAINT [PK_CN_H_MEN_UON3] PRIMARY KEY CLUSTERED " & vbCrLf
sConsulta = sConsulta & "(" & vbCrLf
sConsulta = sConsulta & "            [MEN] ASC," & vbCrLf
sConsulta = sConsulta & "            [UON1] ASC," & vbCrLf
sConsulta = sConsulta & "            [UON2] ASC," & vbCrLf
sConsulta = sConsulta & "            [UON3] ASC" & vbCrLf
sConsulta = sConsulta & ")WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]" & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
sConsulta = sConsulta & " SET ANSI_PADDING OFF" & vbCrLf
sConsulta = sConsulta & " ALTER TABLE [dbo].[CN_H_MEN_UON3]  WITH CHECK ADD  CONSTRAINT [FK_CN_H_MEN_UON3_CN_H_MEN] FOREIGN KEY([MEN])" & vbCrLf
sConsulta = sConsulta & "REFERENCES [dbo].[CN_H_MEN] ([ID])" & vbCrLf
sConsulta = sConsulta & " ALTER TABLE [dbo].[CN_H_MEN_UON3] CHECK CONSTRAINT [FK_CN_H_MEN_UON3_CN_H_MEN]" & vbCrLf
sConsulta = sConsulta & " ALTER TABLE [dbo].[CN_H_MEN_UON3]  WITH CHECK ADD  CONSTRAINT [FK_CN_H_MEN_UON3_UON3] FOREIGN KEY([UON1], [UON2], [UON3])" & vbCrLf
sConsulta = sConsulta & "REFERENCES [dbo].[UON3] ([UON1], [UON2], [COD])" & vbCrLf
sConsulta = sConsulta & " ALTER TABLE [dbo].[CN_H_MEN_UON3] CHECK CONSTRAINT [FK_CN_H_MEN_UON3_UON3]" & vbCrLf
sConsulta = sConsulta & "END"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[CN_H_MEN_UON0_DEP]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE [dbo].[CN_H_MEN_UON0_DEP](" & vbCrLf
sConsulta = sConsulta & "            [MEN] [int] NOT NULL," & vbCrLf
sConsulta = sConsulta & "            [DEP] [varchar](" & gLongitudesDeCodigos.giLongCodDEP & ") NOT NULL," & vbCrLf
sConsulta = sConsulta & "CONSTRAINT [PK_CN_H_MEN_UON0_DEP] PRIMARY KEY CLUSTERED " & vbCrLf
sConsulta = sConsulta & "(" & vbCrLf
sConsulta = sConsulta & "            [MEN] ASC," & vbCrLf
sConsulta = sConsulta & "            [DEP] ASC" & vbCrLf
sConsulta = sConsulta & ")WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]" & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
sConsulta = sConsulta & " SET ANSI_PADDING OFF" & vbCrLf
sConsulta = sConsulta & " ALTER TABLE [dbo].[CN_H_MEN_UON0_DEP]  WITH CHECK ADD  CONSTRAINT [FK_CN_H_MEN_UON0_DEP_CN_H_MEN] FOREIGN KEY([MEN])" & vbCrLf
sConsulta = sConsulta & "REFERENCES [dbo].[CN_H_MEN] ([ID])" & vbCrLf
sConsulta = sConsulta & " ALTER TABLE [dbo].[CN_H_MEN_UON0_DEP] CHECK CONSTRAINT [FK_CN_H_MEN_UON0_DEP_CN_H_MEN]" & vbCrLf
sConsulta = sConsulta & " ALTER TABLE [dbo].[CN_H_MEN_UON0_DEP]  WITH CHECK ADD  CONSTRAINT [FK_CN_H_MEN_UON0_DEP_UON0_DEP] FOREIGN KEY([DEP])" & vbCrLf
sConsulta = sConsulta & "REFERENCES [dbo].[UON0_DEP] ([DEP])" & vbCrLf
sConsulta = sConsulta & " ALTER TABLE [dbo].[CN_H_MEN_UON0_DEP] CHECK CONSTRAINT [FK_CN_H_MEN_UON0_DEP_UON0_DEP]" & vbCrLf
sConsulta = sConsulta & "END"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[CN_H_MEN_UON1_DEP]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE [dbo].[CN_H_MEN_UON1_DEP](" & vbCrLf
sConsulta = sConsulta & "            [MEN] [int] NOT NULL," & vbCrLf
sConsulta = sConsulta & "            [UON1] [varchar](" & gLongitudesDeCodigos.giLongCodUON1 & ") NOT NULL," & vbCrLf
sConsulta = sConsulta & "            [DEP] [varchar](" & gLongitudesDeCodigos.giLongCodDEP & ") NOT NULL," & vbCrLf
sConsulta = sConsulta & "CONSTRAINT [PK_CN_H_MEN_UON1_DEP] PRIMARY KEY CLUSTERED " & vbCrLf
sConsulta = sConsulta & "(" & vbCrLf
sConsulta = sConsulta & "            [MEN] ASC," & vbCrLf
sConsulta = sConsulta & "            [UON1] ASC," & vbCrLf
sConsulta = sConsulta & "            [DEP] ASC" & vbCrLf
sConsulta = sConsulta & ")WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]" & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
sConsulta = sConsulta & " SET ANSI_PADDING OFF" & vbCrLf
sConsulta = sConsulta & " ALTER TABLE [dbo].[CN_H_MEN_UON1_DEP]  WITH CHECK ADD  CONSTRAINT [FK_CN_H_MEN_UON1_DEP_CN_H_MEN] FOREIGN KEY([MEN])" & vbCrLf
sConsulta = sConsulta & "REFERENCES [dbo].[CN_H_MEN] ([ID])" & vbCrLf
sConsulta = sConsulta & " ALTER TABLE [dbo].[CN_H_MEN_UON1_DEP] CHECK CONSTRAINT [FK_CN_H_MEN_UON1_DEP_CN_H_MEN]" & vbCrLf
sConsulta = sConsulta & " ALTER TABLE [dbo].[CN_H_MEN_UON1_DEP]  WITH CHECK ADD  CONSTRAINT [FK_CN_H_MEN_UON1_DEP_UON1_DEP] FOREIGN KEY([UON1], [DEP])" & vbCrLf
sConsulta = sConsulta & "REFERENCES [dbo].[UON1_DEP] ([UON1], [DEP])" & vbCrLf
sConsulta = sConsulta & " ALTER TABLE [dbo].[CN_H_MEN_UON1_DEP] CHECK CONSTRAINT [FK_CN_H_MEN_UON1_DEP_UON1_DEP]" & vbCrLf
sConsulta = sConsulta & "END"
ExecuteSQL gRDOCon, sConsulta


sConsulta = "IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[CN_H_MEN_UON2_DEP]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE [dbo].[CN_H_MEN_UON2_DEP](" & vbCrLf
sConsulta = sConsulta & "            [MEN] [int] NOT NULL," & vbCrLf
sConsulta = sConsulta & "            [UON1] [varchar](" & gLongitudesDeCodigos.giLongCodUON1 & ") NOT NULL," & vbCrLf
sConsulta = sConsulta & "            [UON2] [varchar](" & gLongitudesDeCodigos.giLongCodUON2 & ") NOT NULL," & vbCrLf
sConsulta = sConsulta & "            [DEP] [varchar](" & gLongitudesDeCodigos.giLongCodDEP & ") NOT NULL," & vbCrLf
sConsulta = sConsulta & "CONSTRAINT [PK_CN_H_MEN_UON2_DEP] PRIMARY KEY CLUSTERED " & vbCrLf
sConsulta = sConsulta & "(" & vbCrLf
sConsulta = sConsulta & "            [MEN] ASC," & vbCrLf
sConsulta = sConsulta & "            [UON1] ASC," & vbCrLf
sConsulta = sConsulta & "            [UON2] ASC," & vbCrLf
sConsulta = sConsulta & "            [DEP] ASC" & vbCrLf
sConsulta = sConsulta & ")WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]" & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
sConsulta = sConsulta & " SET ANSI_PADDING OFF" & vbCrLf
sConsulta = sConsulta & " ALTER TABLE [dbo].[CN_H_MEN_UON2_DEP]  WITH CHECK ADD  CONSTRAINT [FK_CN_H_MEN_UON2_DEP_CN_H_MEN] FOREIGN KEY([MEN])" & vbCrLf
sConsulta = sConsulta & "REFERENCES [dbo].[CN_H_MEN] ([ID])" & vbCrLf
sConsulta = sConsulta & " ALTER TABLE [dbo].[CN_H_MEN_UON2_DEP] CHECK CONSTRAINT [FK_CN_H_MEN_UON2_DEP_CN_H_MEN]" & vbCrLf
sConsulta = sConsulta & " ALTER TABLE [dbo].[CN_H_MEN_UON2_DEP]  WITH CHECK ADD  CONSTRAINT [FK_CN_H_MEN_UON2_DEP_UON2_DEP] FOREIGN KEY([UON1], [UON2], [DEP])" & vbCrLf
sConsulta = sConsulta & "REFERENCES [dbo].[UON2_DEP] ([UON1], [UON2], [DEP])" & vbCrLf
sConsulta = sConsulta & " ALTER TABLE [dbo].[CN_H_MEN_UON2_DEP] CHECK CONSTRAINT [FK_CN_H_MEN_UON2_DEP_UON2_DEP]" & vbCrLf
sConsulta = sConsulta & "END"
ExecuteSQL gRDOCon, sConsulta


sConsulta = "IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[CN_H_MEN_UON3_DEP]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE [dbo].[CN_H_MEN_UON3_DEP](" & vbCrLf
sConsulta = sConsulta & "            [MEN] [int] NOT NULL," & vbCrLf
sConsulta = sConsulta & "            [UON1] [varchar](" & gLongitudesDeCodigos.giLongCodUON1 & ") NOT NULL," & vbCrLf
sConsulta = sConsulta & "            [UON2] [varchar](" & gLongitudesDeCodigos.giLongCodUON2 & ") NOT NULL," & vbCrLf
sConsulta = sConsulta & "            [UON3] [varchar](" & gLongitudesDeCodigos.giLongCodUON3 & ") NOT NULL," & vbCrLf
sConsulta = sConsulta & "            [DEP] [varchar](" & gLongitudesDeCodigos.giLongCodDEP & ") NOT NULL," & vbCrLf
sConsulta = sConsulta & "CONSTRAINT [PK_CN_H_MEN_UON3_DEP] PRIMARY KEY CLUSTERED " & vbCrLf
sConsulta = sConsulta & "(" & vbCrLf
sConsulta = sConsulta & "            [MEN] ASC," & vbCrLf
sConsulta = sConsulta & "            [UON1] ASC," & vbCrLf
sConsulta = sConsulta & "            [UON2] ASC," & vbCrLf
sConsulta = sConsulta & "            [UON3] ASC," & vbCrLf
sConsulta = sConsulta & "            [DEP] ASC" & vbCrLf
sConsulta = sConsulta & ")WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]" & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
sConsulta = sConsulta & " SET ANSI_PADDING OFF" & vbCrLf
sConsulta = sConsulta & " ALTER TABLE [dbo].[CN_H_MEN_UON3_DEP]  WITH CHECK ADD  CONSTRAINT [FK_CN_H_MEN_UON3_DEP_CN_H_MEN] FOREIGN KEY([MEN])" & vbCrLf
sConsulta = sConsulta & "REFERENCES [dbo].[CN_H_MEN] ([ID])" & vbCrLf
sConsulta = sConsulta & " ALTER TABLE [dbo].[CN_H_MEN_UON3_DEP] CHECK CONSTRAINT [FK_CN_H_MEN_UON3_DEP_CN_H_MEN]" & vbCrLf
sConsulta = sConsulta & " ALTER TABLE [dbo].[CN_H_MEN_UON3_DEP]  WITH CHECK ADD  CONSTRAINT [FK_CN_H_MEN_UON3_DEP_UON3_DEP] FOREIGN KEY([UON1], [UON2], [UON3], [DEP])" & vbCrLf
sConsulta = sConsulta & "REFERENCES [dbo].[UON3_DEP] ([UON1], [UON2], [UON3], [DEP])" & vbCrLf
sConsulta = sConsulta & " ALTER TABLE [dbo].[CN_H_MEN_UON3_DEP] CHECK CONSTRAINT [FK_CN_H_MEN_UON3_DEP_UON3_DEP]" & vbCrLf
sConsulta = sConsulta & "END"
ExecuteSQL gRDOCon, sConsulta


sConsulta = "IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[CN_H_MEN_CON]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE [dbo].[CN_H_MEN_CON](" & vbCrLf
sConsulta = sConsulta & "            [MEN] [int] NOT NULL," & vbCrLf
sConsulta = sConsulta & "            [PROVE] [varchar](" & gLongitudesDeCodigos.giLongCodPROVE & ") NOT NULL," & vbCrLf
sConsulta = sConsulta & "            [CON] [int] NOT NULL," & vbCrLf
sConsulta = sConsulta & "            [OCULTO] [bit] NOT NULL," & vbCrLf
sConsulta = sConsulta & "CONSTRAINT [PK_CN_H_MEN_CON] PRIMARY KEY CLUSTERED " & vbCrLf
sConsulta = sConsulta & "(" & vbCrLf
sConsulta = sConsulta & "            [MEN] ASC," & vbCrLf
sConsulta = sConsulta & "            [PROVE] ASC," & vbCrLf
sConsulta = sConsulta & "            [CON] ASC" & vbCrLf
sConsulta = sConsulta & ")WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]" & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
sConsulta = sConsulta & " SET ANSI_PADDING OFF" & vbCrLf
sConsulta = sConsulta & " ALTER TABLE [dbo].[CN_H_MEN_CON]  WITH CHECK ADD  CONSTRAINT [FK_CN_H_MEN_CON_CN_H_MEN_CON] FOREIGN KEY([MEN])" & vbCrLf
sConsulta = sConsulta & "REFERENCES [dbo].[CN_H_MEN] ([ID])" & vbCrLf
sConsulta = sConsulta & " ALTER TABLE [dbo].[CN_H_MEN_CON] CHECK CONSTRAINT [FK_CN_H_MEN_CON_CN_H_MEN_CON]" & vbCrLf
sConsulta = sConsulta & " ALTER TABLE [dbo].[CN_H_MEN_CON]  WITH CHECK ADD  CONSTRAINT [FK_CN_H_MEN_CON_CON] FOREIGN KEY([PROVE], [CON])" & vbCrLf
sConsulta = sConsulta & "REFERENCES [dbo].[CON] ([PROVE], [ID])" & vbCrLf
sConsulta = sConsulta & " ALTER TABLE [dbo].[CN_H_MEN_CON] CHECK CONSTRAINT [FK_CN_H_MEN_CON_CON]" & vbCrLf
sConsulta = sConsulta & " ALTER TABLE [dbo].[CN_H_MEN_CON] ADD  CONSTRAINT [DF_CN_H_MEN_CON_OCULTO]  DEFAULT ((0)) FOR [OCULTO]" & vbCrLf
sConsulta = sConsulta & "END"
ExecuteSQL gRDOCon, sConsulta


sConsulta = "IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[CN_H_MEN_EQP]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE [dbo].[CN_H_MEN_EQP](" & vbCrLf
sConsulta = sConsulta & "            [MEN] [int] NOT NULL," & vbCrLf
sConsulta = sConsulta & "            [EQP] [varchar](" & gLongitudesDeCodigos.giLongCodEQP & ") NOT NULL," & vbCrLf
sConsulta = sConsulta & "CONSTRAINT [PK_CN_H_MEN_EQP] PRIMARY KEY CLUSTERED " & vbCrLf
sConsulta = sConsulta & "(" & vbCrLf
sConsulta = sConsulta & "            [MEN] ASC," & vbCrLf
sConsulta = sConsulta & "            [EQP] ASC" & vbCrLf
sConsulta = sConsulta & ")WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]" & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
sConsulta = sConsulta & " SET ANSI_PADDING OFF" & vbCrLf
sConsulta = sConsulta & " ALTER TABLE [dbo].[CN_H_MEN_EQP]  WITH CHECK ADD  CONSTRAINT [FK_CN_H_MEN_EQP_CN_H_MEN] FOREIGN KEY([MEN])" & vbCrLf
sConsulta = sConsulta & "REFERENCES [dbo].[CN_H_MEN] ([ID])" & vbCrLf
sConsulta = sConsulta & " ALTER TABLE [dbo].[CN_H_MEN_EQP] CHECK CONSTRAINT [FK_CN_H_MEN_EQP_CN_H_MEN]" & vbCrLf
sConsulta = sConsulta & " ALTER TABLE [dbo].[CN_H_MEN_EQP]  WITH CHECK ADD  CONSTRAINT [FK_CN_H_MEN_EQP_EQP] FOREIGN KEY([EQP])" & vbCrLf
sConsulta = sConsulta & "REFERENCES [dbo].[EQP] ([COD])" & vbCrLf
sConsulta = sConsulta & " ALTER TABLE [dbo].[CN_H_MEN_EQP] CHECK CONSTRAINT [FK_CN_H_MEN_EQP_EQP]" & vbCrLf
sConsulta = sConsulta & "END"
ExecuteSQL gRDOCon, sConsulta


sConsulta = "IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[CN_H_MEN_GRUPO]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE [dbo].[CN_H_MEN_GRUPO](" & vbCrLf
sConsulta = sConsulta & "            [MEN] [int] NOT NULL," & vbCrLf
sConsulta = sConsulta & "            [GRUPO] [int] NOT NULL," & vbCrLf
sConsulta = sConsulta & "CONSTRAINT [PK_CN_H_MEN_GRUPO] PRIMARY KEY CLUSTERED " & vbCrLf
sConsulta = sConsulta & "(" & vbCrLf
sConsulta = sConsulta & "            [MEN] ASC," & vbCrLf
sConsulta = sConsulta & "            [GRUPO] ASC" & vbCrLf
sConsulta = sConsulta & ")WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]" & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
sConsulta = sConsulta & " ALTER TABLE [dbo].[CN_H_MEN_GRUPO]  WITH CHECK ADD  CONSTRAINT [FK_CN_H_MEN_GRUPO_CN_GRUPO] FOREIGN KEY([GRUPO])" & vbCrLf
sConsulta = sConsulta & "REFERENCES [dbo].[CN_GRUPO] ([ID])" & vbCrLf
sConsulta = sConsulta & " ALTER TABLE [dbo].[CN_H_MEN_GRUPO] CHECK CONSTRAINT [FK_CN_H_MEN_GRUPO_CN_GRUPO]" & vbCrLf
sConsulta = sConsulta & " ALTER TABLE [dbo].[CN_H_MEN_GRUPO]  WITH CHECK ADD  CONSTRAINT [FK_CN_H_MEN_GRUPO_CN_H_MEN_GRUPO] FOREIGN KEY([MEN])" & vbCrLf
sConsulta = sConsulta & "REFERENCES [dbo].[CN_H_MEN] ([ID])" & vbCrLf
sConsulta = sConsulta & " ALTER TABLE [dbo].[CN_H_MEN_GRUPO] CHECK CONSTRAINT [FK_CN_H_MEN_GRUPO_CN_H_MEN_GRUPO]" & vbCrLf
sConsulta = sConsulta & "END"
ExecuteSQL gRDOCon, sConsulta


sConsulta = "IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[CN_H_RESP]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE [dbo].[CN_H_RESP](" & vbCrLf
sConsulta = sConsulta & "            [ID] [int] NOT NULL," & vbCrLf
sConsulta = sConsulta & "            [CONTENIDO] [nvarchar](max) NULL," & vbCrLf
sConsulta = sConsulta & "            [MEGUSTA] [bit] NOT NULL," & vbCrLf
sConsulta = sConsulta & "            [FECALTA] [datetime] NULL," & vbCrLf
sConsulta = sConsulta & "            [FECACT] [datetime] NULL," & vbCrLf
sConsulta = sConsulta & "            [MEN] [int] NOT NULL," & vbCrLf
sConsulta = sConsulta & "            [USU] [varchar](" & gLongitudesDeCodigos.giLongCodUSU & ") NULL," & vbCrLf
sConsulta = sConsulta & "            [PROVE] [varchar](" & gLongitudesDeCodigos.giLongCodPROVE & ") NULL," & vbCrLf
sConsulta = sConsulta & "            [CON] [int] NULL," & vbCrLf
sConsulta = sConsulta & "            [USUCITADO] [varchar](3) NULL," & vbCrLf
sConsulta = sConsulta & "            [RESP] [int] NULL," & vbCrLf
sConsulta = sConsulta & "            [PROVECITADO] [varchar](" & gLongitudesDeCodigos.giLongCodPROVE & ") NULL," & vbCrLf
sConsulta = sConsulta & "            [CONCITADO] [int] NULL," & vbCrLf
sConsulta = sConsulta & "CONSTRAINT [PK_CN_H_RESP] PRIMARY KEY CLUSTERED " & vbCrLf
sConsulta = sConsulta & "(" & vbCrLf
sConsulta = sConsulta & "            [ID] ASC" & vbCrLf
sConsulta = sConsulta & ")WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]" & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
sConsulta = sConsulta & " SET ANSI_PADDING OFF" & vbCrLf
sConsulta = sConsulta & " ALTER TABLE [dbo].[CN_H_RESP]  WITH CHECK ADD  CONSTRAINT [FK_CN_H_RESP_CN_H_RESP] FOREIGN KEY([RESP])" & vbCrLf
sConsulta = sConsulta & "REFERENCES [dbo].[CN_H_RESP] ([ID])" & vbCrLf
sConsulta = sConsulta & " ALTER TABLE [dbo].[CN_H_RESP] CHECK CONSTRAINT [FK_CN_H_RESP_CN_H_RESP]" & vbCrLf
sConsulta = sConsulta & " ALTER TABLE [dbo].[CN_H_RESP]  WITH CHECK ADD  CONSTRAINT [FK_CN_H_RESP_CON] FOREIGN KEY([PROVE], [CON])" & vbCrLf
sConsulta = sConsulta & "REFERENCES [dbo].[CON] ([PROVE], [ID])" & vbCrLf
sConsulta = sConsulta & " ALTER TABLE [dbo].[CN_H_RESP] CHECK CONSTRAINT [FK_CN_H_RESP_CON]" & vbCrLf
sConsulta = sConsulta & " ALTER TABLE [dbo].[CN_H_RESP]  WITH CHECK ADD  CONSTRAINT [FK_CN_H_RESP_USU] FOREIGN KEY([USU])" & vbCrLf
sConsulta = sConsulta & "REFERENCES [dbo].[USU] ([COD])" & vbCrLf
sConsulta = sConsulta & " ALTER TABLE [dbo].[CN_H_RESP] CHECK CONSTRAINT [FK_CN_H_RESP_USU]" & vbCrLf
sConsulta = sConsulta & " ALTER TABLE [dbo].[CN_H_RESP] ADD  CONSTRAINT [DF_CN_H_RESP_MEGUSTA]  DEFAULT ((0)) FOR [MEGUSTA]" & vbCrLf
sConsulta = sConsulta & "END"
ExecuteSQL gRDOCon, sConsulta


sConsulta = "IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[CN_H_MEN_ADJUN]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE [dbo].[CN_H_MEN_ADJUN](" & vbCrLf
sConsulta = sConsulta & "            [ID] [int] NOT NULL," & vbCrLf
sConsulta = sConsulta & "            [NOMBRE] [nvarchar](400) NULL," & vbCrLf
sConsulta = sConsulta & "            [DATA] [varbinary](max) FILESTREAM  NULL," & vbCrLf
sConsulta = sConsulta & "            [GUID] [nvarchar](50) NULL," & vbCrLf
sConsulta = sConsulta & "            [DGUID] [uniqueidentifier] ROWGUIDCOL  NOT NULL," & vbCrLf
sConsulta = sConsulta & "            [SIZE] [float] NULL," & vbCrLf
sConsulta = sConsulta & "            [SIZEUNIT] [nvarchar](10) NULL," & vbCrLf
sConsulta = sConsulta & "            [TIPOADJUNTO] [tinyint] NULL," & vbCrLf
sConsulta = sConsulta & "            [FECALTA] [datetime] NOT NULL," & vbCrLf
sConsulta = sConsulta & "            [RESP] [int] NULL," & vbCrLf
sConsulta = sConsulta & "            [MEN] [int] NULL," & vbCrLf
sConsulta = sConsulta & "CONSTRAINT [PK_CN_H_MEN_ADJUN] PRIMARY KEY CLUSTERED " & vbCrLf
sConsulta = sConsulta & "(" & vbCrLf
sConsulta = sConsulta & "            [ID] ASC" & vbCrLf
sConsulta = sConsulta & ")WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY] FILESTREAM_ON [FILESGROUP]," & vbCrLf
sConsulta = sConsulta & "CONSTRAINT [UC_CN_H_MEN_ADJUN] UNIQUE NONCLUSTERED " & vbCrLf
sConsulta = sConsulta & "(" & vbCrLf
sConsulta = sConsulta & "            [DGUID] ASC" & vbCrLf
sConsulta = sConsulta & ")WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]," & vbCrLf
sConsulta = sConsulta & "CONSTRAINT [UQ_CN_H_MEN_ADJUN] UNIQUE NONCLUSTERED " & vbCrLf
sConsulta = sConsulta & "(" & vbCrLf
sConsulta = sConsulta & "            [DGUID] ASC" & vbCrLf
sConsulta = sConsulta & ")WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]" & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY] FILESTREAM_ON [FILESGROUP]" & vbCrLf
sConsulta = sConsulta & " ALTER TABLE [dbo].[CN_H_MEN_ADJUN]  WITH CHECK ADD  CONSTRAINT [FK_CN_H_MEN_ADJUN_CN_H_MEN] FOREIGN KEY([MEN])" & vbCrLf
sConsulta = sConsulta & "REFERENCES [dbo].[CN_H_MEN] ([ID])" & vbCrLf
sConsulta = sConsulta & " ALTER TABLE [dbo].[CN_H_MEN_ADJUN] CHECK CONSTRAINT [FK_CN_H_MEN_ADJUN_CN_H_MEN]" & vbCrLf
sConsulta = sConsulta & " ALTER TABLE [dbo].[CN_H_MEN_ADJUN]  WITH CHECK ADD  CONSTRAINT [FK_CN_H_MEN_ADJUN_CN_H_RESP] FOREIGN KEY([RESP])" & vbCrLf
sConsulta = sConsulta & "REFERENCES [dbo].[CN_H_RESP] ([ID])" & vbCrLf
sConsulta = sConsulta & " ALTER TABLE [dbo].[CN_H_MEN_ADJUN] CHECK CONSTRAINT [FK_CN_H_MEN_ADJUN_CN_H_RESP]" & vbCrLf
sConsulta = sConsulta & "END"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sysfulltextcatalogs ftc WHERE ftc.name = N'CN_CTG_MEN')" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF  EXISTS (SELECT * FROM sys.fulltext_indexes fti WHERE fti.object_id = OBJECT_ID(N'[dbo].[CN_ADJUN]'))" & vbCrLf
sConsulta = sConsulta & "ALTER FULLTEXT INDEX ON [dbo].[CN_ADJUN] DISABLE" & vbCrLf
sConsulta = sConsulta & "IF  EXISTS (SELECT * FROM sys.fulltext_indexes fti WHERE fti.object_id = OBJECT_ID(N'[dbo].[CN_ADJUN]'))" & vbCrLf
sConsulta = sConsulta & "DROP FULLTEXT INDEX ON [dbo].[CN_ADJUN]" & vbCrLf
sConsulta = sConsulta & "IF EXISTS (SELECT ID FROM sysobjects WHERE name = N'CN_MEN' and type='u')" & vbCrLf
sConsulta = sConsulta & "   IF EXISTS (SELECT * FROM sys.fulltext_indexes WHERE OBJECT_ID = (SELECT ID FROM sysobjects WHERE name = N'CN_MEN' and type='u'))" & vbCrLf
sConsulta = sConsulta & "       DROP FULLTEXT INDEX ON CN_MEN" & vbCrLf
sConsulta = sConsulta & "IF EXISTS (SELECT ID FROM sysobjects WHERE name = N'CN_RESP' and type='u')" & vbCrLf
sConsulta = sConsulta & "   IF EXISTS (SELECT * FROM sys.fulltext_indexes WHERE OBJECT_ID = (SELECT ID FROM sysobjects WHERE name = N'CN_RESP' and type='u'))" & vbCrLf
sConsulta = sConsulta & "       DROP FULLTEXT INDEX ON CN_RESP " & vbCrLf
sConsulta = sConsulta & "IF EXISTS (SELECT ID FROM sysobjects WHERE name = N'CN_MEN_ADJUN' and type='u')" & vbCrLf
sConsulta = sConsulta & "   IF EXISTS (SELECT * FROM sys.fulltext_indexes WHERE OBJECT_ID = (SELECT ID FROM sysobjects WHERE name = N'CN_MEN_ADJUN' and type='u'))" & vbCrLf
sConsulta = sConsulta & "       DROP FULLTEXT INDEX ON CN_MEN_ADJUN " & vbCrLf
sConsulta = sConsulta & "DROP FULLTEXT CATALOG [CN_CTG_MEN]" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE FULLTEXT CATALOG [CN_CTG_MEN]WITH ACCENT_SENSITIVITY = ON" & vbCrLf
sConsulta = sConsulta & "AUTHORIZATION [dbo]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE FULLTEXT INDEX ON [dbo].[CN_MEN](" & vbCrLf
sConsulta = sConsulta & "[CONTENIDO] LANGUAGE [English], " & vbCrLf
sConsulta = sConsulta & "[DONDE] LANGUAGE [English], " & vbCrLf
sConsulta = sConsulta & "[TITULO] LANGUAGE [English])" & vbCrLf
sConsulta = sConsulta & "KEY INDEX [PK_CN_MEN]ON ([CN_CTG_MEN], FILEGROUP [SECONDARY])" & vbCrLf
sConsulta = sConsulta & "WITH (CHANGE_TRACKING = AUTO, STOPLIST = SYSTEM)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE FULLTEXT INDEX ON [dbo].[CN_RESP](" & vbCrLf
sConsulta = sConsulta & "[CONTENIDO] LANGUAGE [English])" & vbCrLf
sConsulta = sConsulta & "KEY INDEX [PK_CN_RESP]ON ([CN_CTG_MEN], FILEGROUP [SECONDARY])" & vbCrLf
sConsulta = sConsulta & "WITH (CHANGE_TRACKING = AUTO, STOPLIST = SYSTEM)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE FULLTEXT INDEX ON [dbo].[CN_ADJUN](" & vbCrLf
sConsulta = sConsulta & "[NOMBRE] LANGUAGE [English])" & vbCrLf
sConsulta = sConsulta & "KEY INDEX [PK_CN_ADJUN]ON ([CN_CTG_MEN], FILEGROUP [SECONDARY])" & vbCrLf
sConsulta = sConsulta & "WITH (CHANGE_TRACKING = AUTO, STOPLIST = SYSTEM)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion31900_08_18994_A31900_08_18995() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim oADOConnection As ADODB.Connection
    Dim sConsulta As String

    On Error GoTo Error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Actualiza Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    frmProgreso.ProgressBar1.Refresh
    V_31900_8_Storeds_0995

    
    sConsulta = "UPDATE VERSION SET NUM ='3.00.18.995'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103), PRODVER='31900.8'"
    ExecuteSQL gRDOCon, sConsulta
        
    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False
        
    CodigoDeActualizacion31900_08_18994_A31900_08_18995 = True
    Exit Function

Error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    CodigoDeActualizacion31900_08_18994_A31900_08_18995 = False
End Function

Public Sub V_31900_8_Storeds_0995()
    Dim sConsulta As String
            
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_SAVE_ADJUN]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_SAVE_ADJUN]" & vbCrLf
    gRDOCon.QueryTimeout = 7200
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "CREATE PROCEDURE [dbo].[FSPM_SAVE_ADJUN] @ID_ADJUN INT OUTPUT,@PER NVARCHAR(50)=NULL,@PROVE NVARCHAR(50),@NOM NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & ", @ID_CONTRATO INT=NULL, @COMENT NVARCHAR(4000)=NULL, @SOLICITUD INT=NULL, @INSTANCIA INT=NULL, @CAMPO INT=NULL, @LINEA INT=NULL" & vbCrLf
    sConsulta = sConsulta & ", @CAMPOPADRE INT=NULL, @CAMPOHIJO INT=NULL,@DATASIZE INT=NULL,@IDIOMA NVARCHAR(10)='',@FECHA DATETIME=NULL" & vbCrLf
    sConsulta = sConsulta & "AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   IF @SOLICITUD>0 " & vbCrLf
    sConsulta = sConsulta & "       BEGIN" & vbCrLf
    sConsulta = sConsulta & "           INSERT INTO SOLICITUD_ADJUN (DATA,FECALTA,SOLICITUD,NOM,DATASIZE,PER, COMENT,IDIOMA)" & vbCrLf
    sConsulta = sConsulta & "                       VALUES ((0x),@FECHA ,@SOLICITUD ,@NOM,@DATASIZE,@PER,@COMENT,@IDIOMA ) " & vbCrLf
    sConsulta = sConsulta & "       END" & vbCrLf
    sConsulta = sConsulta & "   ELSE" & vbCrLf
    sConsulta = sConsulta & "       BEGIN" & vbCrLf
    sConsulta = sConsulta & "           IF @INSTANCIA=1 " & vbCrLf
    sConsulta = sConsulta & "           BEGIN" & vbCrLf
    sConsulta = sConsulta & "               IF @CAMPO >0" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN" & vbCrLf
    sConsulta = sConsulta & "                       INSERT INTO PM_COPIA_ADJUN (DATA,FECALTA,NOM,PER,PROVE, ID_CONTRATO, COMENT)" & vbCrLf
    sConsulta = sConsulta & "                               VALUES ((0x),GETDATE(),@NOM,@PER,@PROVE,@ID_CONTRATO,@COMENT)     " & vbCrLf
    sConsulta = sConsulta & "                       " & vbCrLf
    sConsulta = sConsulta & "                       SET @ID_ADJUN=@@IDENTITY" & vbCrLf
    sConsulta = sConsulta & "                       " & vbCrLf
    sConsulta = sConsulta & "                       INSERT INTO COPIA_CAMPO_ADJUN(ADJUN,CAMPO,NOM,IDIOMA,DATASIZE,PER,COMENT,FECALTA)" & vbCrLf
    sConsulta = sConsulta & "                           VALUES( @ID_ADJUN,@CAMPO,@NOM,@IDIOMA,@DATASIZE,@PER,@COMENT,GETDATE())" & vbCrLf
    sConsulta = sConsulta & "                           " & vbCrLf
    sConsulta = sConsulta & "                       " & vbCrLf
    sConsulta = sConsulta & "                   END " & vbCrLf
    sConsulta = sConsulta & "               ELSE" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN" & vbCrLf
    sConsulta = sConsulta & "                       INSERT INTO PM_COPIA_ADJUN (DATA,FECALTA,NOM,PER,PROVE, ID_CONTRATO, COMENT)" & vbCrLf
    sConsulta = sConsulta & "                               VALUES ((0x),GETDATE(),@NOM,@PER,@PROVE,@ID_CONTRATO,@COMENT)     " & vbCrLf
    sConsulta = sConsulta & "                       " & vbCrLf
    sConsulta = sConsulta & "                       SET @ID_ADJUN=@@IDENTITY" & vbCrLf
    sConsulta = sConsulta & "                       " & vbCrLf
    sConsulta = sConsulta & "                       INSERT INTO COPIA_LINEA_DESGLOSE_ADJUN(ADJUN,CAMPO_PADRE,CAMPO_HIJO,LINEA,NOM,IDIOMA,DATASIZE,PER,COMENT,FECALTA)" & vbCrLf
    sConsulta = sConsulta & "                           VALUES( @ID_ADJUN,@CAMPOPADRE,@CAMPOHIJO,@LINEA,@NOM,@IDIOMA,@DATASIZE,@PER,@COMENT,GETDATE())" & vbCrLf
    sConsulta = sConsulta & "                           " & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "           END " & vbCrLf
    sConsulta = sConsulta & "           ELSE" & vbCrLf
    sConsulta = sConsulta & "               BEGIN" & vbCrLf
    sConsulta = sConsulta & "                   IF @CAMPO >0" & vbCrLf
    sConsulta = sConsulta & "                   BEGIN" & vbCrLf
    sConsulta = sConsulta & "                       INSERT INTO CAMPO_ADJUN (DATA,FECALTA,CAMPO,NOM,DATASIZE,PER, COMENT,IDIOMA)" & vbCrLf
    sConsulta = sConsulta & "                           VALUES ((0x),@FECHA,@CAMPO,@NOM,@DATASIZE,@PER,@COMENT,@IDIOMA  ) " & vbCrLf
    sConsulta = sConsulta & "                   END" & vbCrLf
    sConsulta = sConsulta & "                   ELSE" & vbCrLf
    sConsulta = sConsulta & "                       IF @LINEA>0" & vbCrLf
    sConsulta = sConsulta & "                       BEGIN" & vbCrLf
    sConsulta = sConsulta & "                           INSERT INTO LINEA_DESGLOSE_ADJUN (DATA,FECALTA,CAMPO_PADRE,CAMPO_HIJO,LINEA,NOM,DATASIZE, IDIOMA, COMENT)" & vbCrLf
    sConsulta = sConsulta & "                           VALUES ((0x),@FECHA,@CAMPOPADRE,@CAMPOHIJO,@LINEA,@NOM,@DATASIZE,@IDIOMA,@COMENT )     " & vbCrLf
    sConsulta = sConsulta & "                       END" & vbCrLf
    sConsulta = sConsulta & "                       ELSE" & vbCrLf
    sConsulta = sConsulta & "                       BEGIN" & vbCrLf
    sConsulta = sConsulta & "                           INSERT INTO PM_COPIA_ADJUN (DATA,FECALTA,NOM,PER,PROVE, ID_CONTRATO, COMENT)" & vbCrLf
    sConsulta = sConsulta & "                           VALUES ((0x),GETDATE(),@NOM,@PER,@PROVE,@ID_CONTRATO,@COMENT)     " & vbCrLf
    sConsulta = sConsulta & "                       END" & vbCrLf
    sConsulta = sConsulta & "                       " & vbCrLf
    sConsulta = sConsulta & "               END " & vbCrLf
    sConsulta = sConsulta & "       END " & vbCrLf
    sConsulta = sConsulta & "   " & vbCrLf
    sConsulta = sConsulta & "   " & vbCrLf
    sConsulta = sConsulta & "       SET @ID_ADJUN=@@IDENTITY" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

End Sub

