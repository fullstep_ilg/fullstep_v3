VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmSeleccionarIdiomas 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FS Actualizador"
   ClientHeight    =   2685
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   5310
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmSeleccionarIdiomas.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2685
   ScaleWidth      =   5310
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdContinuar 
      Caption         =   "Continuar"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   435
      Left            =   2040
      TabIndex        =   1
      Top             =   1800
      Width           =   1005
   End
   Begin SSDataWidgets_B.SSDBGrid sdbgIdiomas 
      Height          =   1335
      Left            =   480
      TabIndex        =   0
      Top             =   360
      Width           =   3975
      ScrollBars      =   0
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      FieldSeparator  =   ";"
      Col.Count       =   3
      stylesets.count =   3
      stylesets(0).Name=   "Rojo"
      stylesets(0).BackColor=   10198015
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmSeleccionarIdiomas.frx":0442
      stylesets(1).Name=   "conComent"
      stylesets(1).HasFont=   -1  'True
      BeginProperty stylesets(1).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(1).Picture=   "frmSeleccionarIdiomas.frx":045E
      stylesets(2).Name=   "Verde"
      stylesets(2).BackColor=   13172680
      stylesets(2).HasFont=   -1  'True
      BeginProperty stylesets(2).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(2).Picture=   "frmSeleccionarIdiomas.frx":04F1
      AllowRowSizing  =   0   'False
      AllowGroupSizing=   0   'False
      AllowGroupMoving=   0   'False
      AllowColumnMoving=   0
      AllowGroupSwapping=   0   'False
      AllowColumnSwapping=   0
      AllowGroupShrinking=   0   'False
      AllowColumnShrinking=   0   'False
      AllowDragDrop   =   0   'False
      SelectTypeCol   =   0
      SelectTypeRow   =   0
      CellNavigation  =   1
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      ExtraHeight     =   26
      Columns.Count   =   3
      Columns(0).Width=   3519
      Columns(0).Caption=   "Idioma"
      Columns(0).Name =   "Idioma"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(0).HasBackColor=   -1  'True
      Columns(0).BackColor=   16776960
      Columns(1).Width=   3200
      Columns(1).Caption=   "Seleccione el idioma"
      Columns(1).Name =   "SelIdioma"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      Columns(2).Width=   3200
      Columns(2).Visible=   0   'False
      Columns(2).Caption=   "CodIdioma"
      Columns(2).Name =   "CodIdioma"
      Columns(2).DataField=   "Column 2"
      Columns(2).DataType=   8
      Columns(2).FieldLen=   256
      _ExtentX        =   7011
      _ExtentY        =   2355
      _StockProps     =   79
      BackColor       =   16777215
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageFooterFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty PageHeaderFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin SSDataWidgets_B.SSDBDropDown sdbddValor 
      Height          =   915
      Left            =   0
      TabIndex        =   2
      Top             =   0
      Width           =   2025
      ListAutoValidate=   0   'False
      ListAutoPosition=   0   'False
      _Version        =   196617
      DataMode        =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ColumnHeaders   =   0   'False
      FieldSeparator  =   ";"
      stylesets.count =   1
      stylesets(0).Name=   "Normal"
      stylesets(0).HasFont=   -1  'True
      BeginProperty stylesets(0).Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      stylesets(0).Picture=   "frmSeleccionarIdiomas.frx":050D
      DividerStyle    =   3
      StyleSet        =   "Normal"
      ForeColorEven   =   0
      BackColorOdd    =   16777215
      RowHeight       =   423
      Columns.Count   =   2
      Columns(0).Width=   5318
      Columns(0).Name =   "NOMBRE"
      Columns(0).DataField=   "Column 0"
      Columns(0).DataType=   8
      Columns(0).FieldLen=   256
      Columns(1).Width=   3200
      Columns(1).Visible=   0   'False
      Columns(1).Caption=   "ID"
      Columns(1).Name =   "ID"
      Columns(1).DataField=   "Column 1"
      Columns(1).DataType=   8
      Columns(1).FieldLen=   256
      _ExtentX        =   3572
      _ExtentY        =   1614
      _StockProps     =   77
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmSeleccionarIdiomas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmdContinuar_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    sdbgIdiomas.AddItem ("Espa�ol") & ";Espa�ol;SPA"
    sdbgIdiomas.AddItem ("English") & ";English (ENG);ENG"
    sdbgIdiomas.AddItem ("Deutsch") & ";Deutsch;GER"
    sdbgIdiomas.AddItem ("Fran�ais") & ";Fran�ais;FRA"
    
    sdbddValor.AddItem "Espa�ol;SPA"
    sdbddValor.AddItem "English (ENG);ENG"
    sdbddValor.AddItem "English (ENU);ENU"
    sdbddValor.AddItem "Deutsch;GER"
    sdbddValor.AddItem "Fran�ais;FRA"
    sdbddValor.AddItem "Portugu�s;POR"
    sdbddValor.AddItem "Catal�n;CAT"
    sdbddValor.AddItem "Euskera;EU"
    sdbddValor.AddItem "Italiano;ITA"
    
    basPublic.gIdiomaSPA = "SPA"
    basPublic.gIdiomaENG = "ENG"
    basPublic.gIdiomaGER = "GER"
    basPublic.gIdiomaFRA = "FRA"
End Sub

Private Sub sdbddValor_DropDown()
        
    Screen.MousePointer = vbHourglass

    sdbgIdiomas.ActiveCell.SelStart = 0
    sdbgIdiomas.ActiveCell.SelLength = Len(sdbgIdiomas.ActiveCell.Text)
    
    Screen.MousePointer = vbNormal

End Sub

Private Sub sdbddValor_InitColumnProps()
    sdbddValor.DataFieldList = "Column 0"
    sdbddValor.DataFieldToDisplay = "Column 0"
End Sub

Private Sub sdbddValor_CloseUp()
    sdbgIdiomas.Columns("CODIDIOMA").Value = sdbddValor.Columns("ID").Value
    If sdbgIdiomas.Columns("IDIOMA").Value = "Espa�ol" Then
        basPublic.gIdiomaSPA = sdbgIdiomas.Columns("CODIDIOMA").Value
    ElseIf sdbgIdiomas.Columns("IDIOMA").Value = "English" Then
        basPublic.gIdiomaENG = sdbgIdiomas.Columns("CODIDIOMA").Value
    ElseIf sdbgIdiomas.Columns("IDIOMA").Value = "Deutsch" Then
        basPublic.gIdiomaGER = sdbgIdiomas.Columns("CODIDIOMA").Value
    ElseIf sdbgIdiomas.Columns("IDIOMA").Value = "Fran�ais" Then
        basPublic.gIdiomaFRA = sdbgIdiomas.Columns("CODIDIOMA").Value
    End If
End Sub


Private Sub sdbddValor_PositionList(ByVal Text As String)
    Dim i As Long
    Dim bm As Variant

    On Error Resume Next
    
    sdbddValor.MoveFirst
    
    If Text <> "" Then
        For i = 0 To sdbddValor.Rows - 1
            bm = sdbddValor.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbddValor.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbddValor.Bookmark = bm
                Exit For
            End If
        Next i
    End If
End Sub

Private Sub sdbgIdiomas_RowColChange(ByVal LastRow As Variant, ByVal LastCol As Integer)
    sdbgIdiomas.Columns("SELIDIOMA").Style = ssStyleComboBox
    sdbgIdiomas.Columns("SELIDIOMA").DropDownHwnd = sdbddValor.hWnd
    sdbgIdiomas.Columns("SELIDIOMA").Locked = True
End Sub
