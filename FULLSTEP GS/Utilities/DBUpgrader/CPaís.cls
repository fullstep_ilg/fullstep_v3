VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CPais"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
''' *** Clase: CPais
''' *** Creacion: 26/10/1998 (Javier Arana)
''' *** Ultima revision: 18/01/1999 (Alfredo Magallon)

Option Explicit



''' Variables privadas con la informacion de un pais

Private m_sCod As String
Private m_sDen As String
Private m_oProvincias As CProvincias
Private m_bPortal As Boolean
Private m_iId As Integer
Private m_vFecAct As Variant

''' Estado integraci�n
Private m_vEstIntegracion As Variant

' Variables para el control de cambios del Log e Integraci�n
Private m_sUsuario As String             'Usuario que realiza los cambios.
''' Conexion

Private m_oConexion As CConexion

''' Control de errores

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
    
End Enum

'ado Private mvarResultset As rdoResultset

''' Recordset para el Pa�s a editar
Private m_adores As ADODB.Recordset

''' Indice del pais en la coleccion

Private m_lIndice As Long
Public Property Let FecAct(ByVal vData As Variant)

    ''' * Objetivo: Dar valor a la variable privada FECACT
    ''' * Recibe: Fecha de ultima actualizacion de la moneda
    ''' * Devuelve: Nada

    m_vFecAct = vData
    
End Property
Public Property Get FecAct() As Variant

    ''' * Objetivo: Devolver la variable privada FECACT
    ''' * Recibe: Nada
    ''' * Devuelve: Fecha de ultima actualizacion de la moneda

    FecAct = m_vFecAct
    
End Property

Public Property Let Usuario(ByVal vData As String)

    ''' * Objetivo: Dar valor a la variable privada Usuario
    ''' * Recibe: Codigo del usuario conectado
    ''' * Devuelve: Nada

    m_sUsuario = vData
    
End Property
Public Property Let EstadoIntegracion(ByVal vData As Variant)

    ''' * Objetivo: Dar valor al estado de integraci�n de la moneda
    ''' * Recibe: el valor
    ''' * Devuelve: Nada

    m_vEstIntegracion = vData
    
End Property
Public Property Get EstadoIntegracion() As Variant

    ''' * Objetivo: Devolver el estado de integraci�n de la moneda
    ''' * Recibe: Nada
    ''' * Devuelve: El estado de integraci�n de la moneda

    EstadoIntegracion = m_vEstIntegracion
    
End Property
Friend Property Let Portal(ByVal Data As Boolean)
    m_bPortal = Data
End Property

Public Property Get Indice() As Long

    ''' * Objetivo: Devolver el indice del pais en la coleccion
    ''' * Recibe: Nada
    ''' * Devuelve: Indice del pais en la coleccion

    Indice = m_lIndice
    
End Property
Public Property Let Indice(ByVal lInd As Long)

    ''' * Objetivo: Dar valor al indice del pais en la coleccion
    ''' * Recibe: Indice del pais en la coleccion
    ''' * Devuelve: Nada

    m_lIndice = lInd
    
End Property
Friend Property Set Conexion(ByVal vData As CConexion)

    ''' * Objetivo: Dar valor a la conexion del pais
    ''' * Recibe: Conexion
    ''' * Devuelve: Nada

    Set m_oConexion = vData
    
End Property
Friend Property Get Conexion() As CConexion

    ''' * Objetivo: Devolver la conexion del pais
    ''' * Recibe: Nada
    ''' * Devuelve: Conexion

    Set Conexion = m_oConexion
    
End Property

Public Property Set Provincias(ByVal vData As CProvincias)

    ''' * Objetivo: Dar valor a la variable privada Provincias
    ''' * Recibe: Provincias del pais
    ''' * Devuelve: Nada

    Set m_oProvincias = vData
    
End Property
Public Property Get Provincias() As CProvincias

    ''' * Objetivo: Devolver la variable privada Provincias
    ''' * Recibe: Nada
    ''' * Devuelve: Provincias del pais
    
    Set Provincias = m_oProvincias
    
End Property
Public Property Let Den(ByVal vData As String)

    ''' * Objetivo: Dar valor a la variable privada DEN
    ''' * Recibe: Denominacion del pais
    ''' * Devuelve: Nada
    
    m_sDen = vData
    
End Property
Public Property Get Den() As String

    ''' * Objetivo: Devolver la variable privada DEN
    ''' * Recibe: Nada
    ''' * Devuelve: Denominacion del pais

    Den = m_sDen
    
End Property
Public Property Let ID(ByVal Data As Integer)
    
    Let m_iId = Data
    
End Property
Public Property Get ID() As Integer
    
    ID = m_iId
    
End Property
Public Property Let Cod(ByVal vData As String)

    ''' * Objetivo: Dar valor a la variable privada COD
    ''' * Recibe: Codigo del pais
    ''' * Devuelve: Nada

    m_sCod = vData
    
End Property
Public Property Get Cod() As String

    ''' * Objetivo: Devolver la variable privada COD
    ''' * Recibe: Nada
    ''' * Devuelve: Codigo del pais

    Cod = m_sCod
    
End Property

Private Sub Class_Terminate()

    ''' * Objetivo: Limpiar la memoria
    
    Set m_oProvincias = Nothing
    Set m_oConexion = Nothing
    
End Sub
Public Sub CargarTodasLasProvincias(Optional ByVal CarIniCod As String, Optional ByVal CarIniDen As String, Optional ByVal CoincidenciaTotal As Boolean, Optional ByVal OrdenadasPorDen As Boolean = False, Optional ByVal UsarIndice As Boolean = False, Optional ByVal MostrarEstadoIntegracion As Boolean = False, Optional ByVal OrdenadasPorEstadoInt As Boolean = False)

    ''' ! Pendiente para cuando veamos lo de provincias
    
'ado  Dim rdores As rdoResultset
Dim rs As ADODB.Recordset
Dim fldCod As ADODB.Field
Dim fldDen As ADODB.Field
Dim fldId As ADODB.Field
Dim fldFecAct As ADODB.Field
Dim fldEstadoInt As ADODB.Field   'Estado integraci�n
Dim sConsulta As String
Dim lIndice As Long
Dim sFSP As String
Dim v_Estado As Variant

'*************************************************************************************
''' * Objetivo: Cuando se trabaja con integraci�n, desde el mantenimiento se visualiza el estado de integraci�n
''' *            en el sistema receptor de las modificaciones originadas en FSGS.
''' *           Buscamos por coincidencia de c�digo (COD) cuando la acci�n es distinta a un cambio de c�digo (C)
''' *           y por coincidencia en c�digo nuevo (COD_NEW) cuando la acci�n es un cambio de c�digo
'*************************************************************************************

'********* Precondicion **************************************
If m_oConexion Is Nothing Then
    Err.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CPais.CargarTodasLasdivisiones", "No se ha establecido la conexion"
    Exit Sub
End If
'*************************************************************
   
sConsulta = "SELECT PROVI.COD,PROVI.DEN,PROVI.FECACT FROM PROVI"
sConsulta = sConsulta & " WHERE PROVI.PAI='" & DblQuote(m_sCod) & "'"

If CarIniCod = "" And CarIniDen = "" Then
    
Else
    If Not (CarIniCod = "") And Not (CarIniDen = "") Then
        
        If CoincidenciaTotal Then
            
            sConsulta = sConsulta & " AND COD ='" & DblQuote(CarIniCod) & "'"
            sConsulta = sConsulta & " AND DEN='" & DblQuote(CarIniDen) & "'"
        Else
            sConsulta = sConsulta & " AND PROVI.COD LIKE '" & DblQuote(CarIniCod) & "%'"
            sConsulta = sConsulta & " PROVI.DEN LIKE '" & DblQuote(CarIniDen) & "%'"
        End If
    Else
        If Not (CarIniCod = "") Then
            
            If CoincidenciaTotal Then
            
                sConsulta = sConsulta & " AND PROVI.COD ='" & DblQuote(CarIniCod) & "'"
                
            Else
                    
                sConsulta = sConsulta & " AND PROVI.COD LIKE '" & DblQuote(CarIniCod) & "%'"
                
            End If
            
        Else
            
            If CoincidenciaTotal Then
            
                sConsulta = sConsulta & " AND PROVI.DEN='" & DblQuote(CarIniDen) & "'"
                
            Else
           
                sConsulta = sConsulta & " AND PROVI.DEN LIKE '" & DblQuote(CarIniDen) & "%'"
                  
            End If
            
        End If
    
    End If
    
End If

If MostrarEstadoIntegracion Then
    sConsulta = sConsulta & " GROUP BY PROVI.COD,PROVI.DEN,PROVI.FECACT "
End If

If OrdenadasPorDen Then
    sConsulta = sConsulta & " ORDER BY PROVI.DEN,PROVI.COD"
Else
    If OrdenadasPorEstadoInt Then
        sConsulta = sConsulta & " ORDER BY ESTADO,PROVI.COD,PROVI.DEN"
    Else
        sConsulta = sConsulta & " ORDER BY PROVI.COD,PROVI.DEN"
    End If
End If
      
Set rs = New ADODB.Recordset
rs.Open sConsulta, m_oConexion.adoCon, adOpenForwardOnly, adLockReadOnly
If rs.eof Then
        
    rs.Close
    Set rs = Nothing
    Set m_oProvincias = Nothing
    Set m_oProvincias = New CProvincias
    Set m_oProvincias.Conexion = m_oConexion

    
    Exit Sub
      
Else
         
    Set m_oProvincias = Nothing
    Set m_oProvincias = New CProvincias
    Set m_oProvincias.Conexion = m_oConexion

    
    If m_bPortal Then
        
        m_oProvincias.Portal = True
        Set fldCod = rs.Fields("COD")
        Set fldDen = rs.Fields("DEN")
        Set fldId = rs.Fields("ID")
        
        If UsarIndice Then
        
            lIndice = 0
            
            While Not rs.eof
                m_oProvincias.Add fldCod.Value, fldDen.Value, lIndice, fldId.Value
                rs.MoveNext
                lIndice = lIndice + 1
            Wend
        Else
            While Not rs.eof
                m_oProvincias.Add fldCod.Value, fldDen.Value, , fldId.Value
                rs.MoveNext
            Wend
        End If
    
    Else
        Set fldCod = rs.Fields("COD")
        Set fldDen = rs.Fields("DEN")
        Set fldFecAct = rs.Fields("FECACT")
        If MostrarEstadoIntegracion Then
            Set fldEstadoInt = rs.Fields("ESTADO")
        End If

        If UsarIndice Then
        
            lIndice = 0
            
            While Not rs.eof
                If MostrarEstadoIntegracion Then
                    v_Estado = fldEstadoInt.Value
                Else
                    v_Estado = Null
                End If
                m_oProvincias.Add fldCod.Value, fldDen.Value, lIndice, , fldFecAct.Value, v_Estado
                rs.MoveNext
                lIndice = lIndice + 1
            Wend
        Else
            While Not rs.eof
                If MostrarEstadoIntegracion Then
                    v_Estado = fldEstadoInt.Value
                Else
                    v_Estado = Null
                End If

                m_oProvincias.Add fldCod.Value, fldDen.Value, , , fldFecAct.Value, v_Estado
                
                rs.MoveNext
            Wend
        End If
        
    End If
    
    rs.Close
    Set rs = Nothing
    Set fldCod = Nothing
    Set fldDen = Nothing
    Set fldId = Nothing
    Set fldFecAct = Nothing
    Set fldEstadoInt = Nothing
    Exit Sub
      
End If

End Sub
Public Sub CargarTodasLasProvinciasDesde(ByVal NumMaximo As Integer, Optional ByVal CaracteresInicialesCod As String, Optional ByVal CaracteresInicialesDen As String, Optional ByVal OrdenadosPorDen As Boolean, Optional ByVal UsarIndice As Boolean)

    ''' ! Pendiente para cuando veamos lo de provincias
    
'ado  Dim rdores As rdoResultset
Dim rs As ADODB.Recordset
Dim fldCod As ADODB.Field
Dim fldDen As ADODB.Field
Dim fldId As ADODB.Field
Dim sConsulta As String
Dim lIndice As Long
''''''''''''Dim iNumProve As Integer
Dim sFSP As String
'********* Precondicion **************************************
If m_oConexion Is Nothing Then
    Err.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CDestinos.CargarTodosLosDestinos", "No se ha establecido la conexion"
    Exit Sub
End If
'*************************************************************
sConsulta = "SELECT TOP " & NumMaximo & " PROVI.COD,PROVI.DEN FROM PROVI"
sConsulta = sConsulta & " WHERE PROVI.PAI='" & DblQuote(m_sCod) & "' "

If CaracteresInicialesCod = "" And CaracteresInicialesDen = "" Then
   
Else
    If Not (CaracteresInicialesCod = "") And Not (CaracteresInicialesDen = "") Then
        
            sConsulta = sConsulta & " AND COD >='" & DblQuote(CaracteresInicialesCod) & "'"
            sConsulta = sConsulta & " AND DEN >='" & DblQuote(CaracteresInicialesDen) & "'"
                    
    Else
        
        If Not (CaracteresInicialesCod = "") Then
                sConsulta = sConsulta & "AND COD >='" & DblQuote(CaracteresInicialesCod) & "'"
        Else
                sConsulta = sConsulta & "AND DEN >='" & DblQuote(CaracteresInicialesDen) & "'"
        End If
    
    End If
           
End If

If OrdenadosPorDen Then
    sConsulta = sConsulta & " ORDER BY DEN,COD"
Else
    sConsulta = sConsulta & " ORDER BY COD,DEN"
End If
      
'ado Set rdores = m_oConexion.rdoSummitCon.OpenResultset(sConsulta, rdOpenForwardOnly, rdConcurReadOnly, rdExecDirect)
Set rs = New ADODB.Recordset
rs.Open sConsulta, m_oConexion.adoCon, adOpenForwardOnly, adLockReadOnly
If rs.eof Then
        
    Set rs = Nothing
    Set m_oProvincias = Nothing
    Set m_oProvincias = New CProvincias
    Set m_oProvincias.Conexion = m_oConexion
    m_oProvincias.eof = True
    Exit Sub
      
Else
    
    Set m_oProvincias = Nothing
    Set m_oProvincias = New CProvincias
    Set m_oProvincias.Conexion = m_oConexion
 
    
    
    If m_bPortal Then
        m_oProvincias.Portal = True
        Set fldCod = rs.Fields("COD")
        Set fldDen = rs.Fields("DEN")
        Set fldId = rs.Fields("ID")
        If UsarIndice Then
            
            lIndice = 0
            
                    
            While Not rs.eof
                ' Dejamos en manos del programador cuando asignar la conexion al obejto usuario
                'ado  m_oProvincias.Add rdores("COD"), rdores("DEN"), lIndice, rdores("ID").Value
                m_oProvincias.Add fldCod.Value, fldDen.Value, lIndice, fldId.Value
                rs.MoveNext
                lIndice = lIndice + 1
                
            Wend
            
            If Not rs.eof Then
                m_oProvincias.eof = False
                rs.Close
            Else
                m_oProvincias.eof = True
            End If
            
            Set rs = Nothing
                
        Else
            
            While Not rs.eof
                ' Dejamos en manos del programador cuando asignar la conexion al obejto usuario
                'ado  m_oProvincias.Add rdores("COD"), rdores("DEN"), , rdores("ID").Value
                m_oProvincias.Add fldCod.Value, fldDen.Value, , fldId.Value
                rs.MoveNext
                
            Wend
            
            If Not rs.eof Then
                m_oProvincias.eof = False
                rs.Close
            Else
                m_oProvincias.eof = True
            End If
            
        End If
    Else
        Set fldCod = rs.Fields("COD")
        Set fldDen = rs.Fields("DEN")
        
        If UsarIndice Then
            
            lIndice = 0
            
            While Not rs.eof
                ' Dejamos en manos del programador cuando asignar la conexion al obejto usuario
                'ado  m_oProvincias.Add rdores("COD"), rdores("DEN"), lIndice
                m_oProvincias.Add fldCod.Value, fldDen.Value, lIndice
                rs.MoveNext
                lIndice = lIndice + 1
                
            Wend
            
            If Not rs.eof Then
                m_oProvincias.eof = False
                rs.Close
            Else
                m_oProvincias.eof = True
            End If
            
            Set rs = Nothing
                
        Else
            
            While Not rs.eof
                ' Dejamos en manos del programador cuando asignar la conexion al obejto usuario
                'ado  m_oProvincias.Add rdores("COD"), rdores("DEN")
                m_oProvincias.Add fldCod.Value, fldDen.Value
                rs.MoveNext
                
            Wend
            
            If Not rs.eof Then
                m_oProvincias.eof = False
                rs.Close
            Else
                m_oProvincias.eof = True
            End If
            
        End If
    End If
    
    Set rs = Nothing
    Set fldCod = Nothing
    Set fldDen = Nothing
    Set fldId = Nothing
    Exit Sub
      
End If
End Sub

