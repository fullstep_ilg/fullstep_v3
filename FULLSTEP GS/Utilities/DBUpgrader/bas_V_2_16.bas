Attribute VB_Name = "bas_V_2_16"
Public Function CodigoDeActualizacion2_15_00_05A2_16_00_00() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim bBool As Boolean

On Error GoTo error
    

sConsulta = "BEGIN TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = True
ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"


frmProgreso.lblDetalle = "Modificamos tablas y a�adimos nuevas"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
V_2_16_Tablas_0

frmProgreso.lblDetalle = "Modificamos triggers"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_16_Triggers_0

frmProgreso.lblDetalle = "Modificamos storeds"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_16_Storeds_0


frmProgreso.lblDetalle = "Actualizamos datos"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_16_Datos_0

sConsulta = "UPDATE VERSION SET NUM ='2.16.00.00'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "COMMIT TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = False

CodigoDeActualizacion2_15_00_05A2_16_00_00 = True
Exit Function
    
error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_15_00_05A2_16_00_00 = False

End Function

Public Function CodigoDeActualizacion2_16_00_00A2_16_00_01() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim bBool As Boolean
Dim oRS As ADODB.Recordset
Dim oADOConnection As ADODB.Connection
Dim iRes As Integer

On Error GoTo error
    
sMensaje = "�Desea comprobar qu� usuarios de los procesos han sido eliminados?"
iRes = MsgBox(sMensaje, vbCritical + vbYesNo, "FULLSTEP UPGRADER")
If iRes = vbYes Then
    sConsulta = ""
    sConsulta = sConsulta & "SELECT DISTINCT USUAPER FROM PROCE P WHERE NOT EXISTS (SELECT COD FROM (SELECT COD FROM USU UNION SELECT USU FROM ADM) U WHERE P.USUAPER=U.COD) AND USUAPER IS NOT NULL" & vbCrLf
    sConsulta = sConsulta & "UNION" & vbCrLf
    sConsulta = sConsulta & "SELECT DISTINCT USUVALSELPROVE FROM PROCE P WHERE NOT EXISTS (SELECT COD FROM (SELECT COD FROM USU UNION SELECT USU FROM ADM) U WHERE P.USUVALSELPROVE=U.COD) AND USUVALSELPROVE IS NOT NULL" & vbCrLf
    sConsulta = sConsulta & "UNION" & vbCrLf
    sConsulta = sConsulta & "SELECT DISTINCT USUVAL FROM PROCE P WHERE NOT EXISTS (SELECT COD FROM (SELECT COD FROM USU UNION SELECT USU FROM ADM) U WHERE P.USUVAL =U.COD) AND USUVAL  IS NOT NULL" & vbCrLf
    sConsulta = sConsulta & "UNION" & vbCrLf
    sConsulta = sConsulta & "SELECT DISTINCT USUVALCIERRE FROM PROCE P WHERE NOT EXISTS (SELECT COD FROM (SELECT COD FROM USU UNION SELECT USU FROM ADM) U WHERE P.USUVALCIERRE =U.COD) AND USUVALCIERRE IS NOT NULL" & vbCrLf
    
    Set oADOConnection = New ADODB.Connection
    oADOConnection.Open "Provider=SQLOLEDB.1;Auto Translate=FALSE;Server=" & gServer & ";Database=" & gBD & ";", gUID, gPWD
    oADOConnection.CursorLocation = adUseClient
    oADOConnection.CommandTimeout = 120
    
    Set oRS = New ADODB.Recordset
    oRS.Open sConsulta, oADOConnection, adOpenForwardOnly, adLockReadOnly
    
    If Not oRS.eof Then
        sMensaje = "Los siguientes usuarios no existen en el GS: "
        While Not oRS.eof
            sMensaje = sMensaje & oRS.Fields(0).Value & "; "
            oRS.MoveNext
        Wend
        oRS.Close
        Set oRS = Nothing
        oADOConnection.Close
        Set oADOConnection = Nothing
        sMensaje = Left(sMensaje, Len(sMensaje) - 1)
        
        sMensaje = sMensaje & ". " & vbCrLf & "Para poder actualizar la versi�n deber�n existir los usuarios correspondientes a dichos c�digos."
        
        MsgBox sMensaje, vbCritical + vbOKOnly, "FULLSTEP UPGRADER"
        CodigoDeActualizacion2_16_00_00A2_16_00_01 = False
        Exit Function
    End If
    
    oRS.Close
    Set oRS = Nothing
    oADOConnection.Close
    Set oADOConnection = Nothing
End If
sConsulta = "BEGIN TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = True
ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

frmProgreso.lblDetalle = "Actualizamos datos"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1


V_2_16_Datos_1


frmProgreso.lblDetalle = "Actualizamos trigger de proce"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1


V_2_16_Triggers_1


sConsulta = "UPDATE VERSION SET NUM ='2.16.00.01'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "COMMIT TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = False

CodigoDeActualizacion2_16_00_00A2_16_00_01 = True
Exit Function
    
error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_16_00_00A2_16_00_01 = False

End Function


Private Sub V_2_16_Storeds_0()
Dim sConsulta As String

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[COM_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[COM_COD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[EQP_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[EQP_COD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GMN1_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[GMN1_COD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GMN2_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[GMN2_COD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GMN3_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[GMN3_COD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GMN4_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[GMN4_COD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_CARGAR_ULTIMAS_OFERTAS_GRUPOS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_CARGAR_ULTIMAS_OFERTAS_GRUPOS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ELIMINAR_ADJUDICACIONES]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_ELIMINAR_ADJUDICACIONES]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_TRASPASAR_OFERTA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_TRASPASAR_OFERTA]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[UON1_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[UON1_COD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[UON2_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[UON2_COD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[UON3_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[UON3_COD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USU_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[USU_COD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE COM_COD (@OLD VARCHAR(50),@NEW VARCHAR(50),@EQP VARCHAR(50))  AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT ON" & vbCrLf
sConsulta = sConsulta & "BEGIN TRANSACTION" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR FOR SELECT COD FROM COM WHERE COD=@OLD" & vbCrLf
sConsulta = sConsulta & "OPEN C" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C INTO @VAR_COD" & vbCrLf
sConsulta = sConsulta & "IF @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_POT NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISOR NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE COM SET COD=@NEW WHERE EQP=@EQP AND COD=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE COM_GMN1 SET COM=@NEW WHERE EQP=@EQP AND COM=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE COM_GMN2 SET COM=@NEW WHERE EQP=@EQP AND COM=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE COM_GMN3 SET COM=@NEW WHERE EQP=@EQP AND COM=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE COM_GMN4 SET COM=@NEW WHERE EQP=@EQP AND COM=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET COM=@NEW WHERE EQP=@EQP AND COM=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE SET COM=@NEW WHERE EQP=@EQP AND COM=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE_POT SET COM=@NEW WHERE EQP=@EQP AND COM=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_PROCE SET COM=@NEW WHERE EQP=@EQP AND COM=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_PROVE SET COM=@NEW WHERE EQP=@EQP AND COM=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_PROVE SET COM=@NEW WHERE EQP=@EQP AND COM=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISOR SET COM=@NEW WHERE EQP=@EQP AND COM=@OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_POT CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISOR CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close C" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "COMMIT TRANSACTION" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT OFF" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE EQP_COD (@OLD VARCHAR(50),@NEW VARCHAR(50))  AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT ON" & vbCrLf
sConsulta = sConsulta & "BEGIN TRANSACTION" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR FOR SELECT COD FROM EQP WHERE COD=@OLD" & vbCrLf
sConsulta = sConsulta & "OPEN C" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C INTO @VAR_COD" & vbCrLf
sConsulta = sConsulta & "IF @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE EQP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_POT NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_EQP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PER NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISOR NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE EQP SET COD=@NEW WHERE COD=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE COM SET EQP=@NEW WHERE EQP=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE COM_GMN1 SET EQP=@NEW WHERE EQP=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE COM_GMN2 SET EQP=@NEW WHERE EQP=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE COM_GMN3 SET EQP=@NEW WHERE EQP=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE COM_GMN4 SET EQP=@NEW WHERE EQP=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET EQP=@NEW WHERE EQP=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE SET EQP=@NEW WHERE EQP=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE_POT SET EQP=@NEW WHERE EQP=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_EQP SET EQP=@NEW WHERE EQP=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE USU SET EQP=@NEW WHERE EQP=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_PROCE SET EQP=@NEW WHERE EQP=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_PROVE SET EQP=@NEW WHERE EQP=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_PROVE SET EQP=@NEW WHERE EQP=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PER SET EQP=@NEW WHERE EQP=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISOR SET EQP=@NEW WHERE EQP=@OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE EQP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_POT CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_EQP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PER CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISOR CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close C" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT OFF" & vbCrLf
sConsulta = sConsulta & "COMMIT TRANSACTION" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE GMN1_COD (@OLD VARCHAR(50),@NEW VARCHAR(50),@INT_ORIGEN VARCHAR(1),@USUARIO VARCHAR(50))  AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_DEN VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT ON" & vbCrLf
sConsulta = sConsulta & "BEGIN TRANSACTION" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR FOR SELECT COD,DEN FROM GMN1 WHERE COD=@OLD" & vbCrLf
sConsulta = sConsulta & "OPEN C" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C INTO @VAR_COD,@VAR_DEN" & vbCrLf
sConsulta = sConsulta & "IF @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART1_ADJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART2_ADJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART3_ADJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ADJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE DETALLE_PUJAS NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ADJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ADJREU NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ESP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OBJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OFE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OFE_WEB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRES NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESCON NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESPROY NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESCON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESCON4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFEMIN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_GRUPO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_ESP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_WEB_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_WEB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PER NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_PET NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_POT NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVENOTIF NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE REU_PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ESP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4_ACT NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATALOG_LIN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_PEDIDO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART_UP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_DEF NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO_DEF NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO_ESP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_GR_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_GR_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ITEM_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ITEM_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_SOBRE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GRUPO_OFE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GRUPO_OFE_REU NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USAR_GR_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_CONTR NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE SOBRE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FAVORITOS_LINEAS_PEDIDO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_NUMDEC NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_ART NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISOR NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN1 SET COD=@NEW WHERE COD=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART1_ADJ SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART2_ADJ SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART3_ADJ SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4_ADJ SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE BLOQUEO_PROCE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE COM_GMN1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE COM_GMN2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE COM_GMN3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE COM_GMN4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE DETALLE_PUJAS SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_ADJ SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_ADJREU SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_ESP SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_OBJ SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_OFE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_OFE_WEB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_PRES SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_PRESCON SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_PRESCON3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_PRESCON4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_PRESPROY SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_UON1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_UON2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_UON3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFEMIN SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES_MAT1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES_MAT2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES_MAT3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES_MAT4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_ESP SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE_ADJUN SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE_WEB_ADJUN SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE_WEB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PER SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE_PET SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE_POT SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_GMN1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_GMN2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_GMN3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_GMN4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVENOTIF SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE REU_PROCE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_PROCE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_PROVE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_GRUPO SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_UON1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_UON2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_UON3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_PRESCON3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_PRESCON4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_PROVE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4_ESP  SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN4_ACT SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART4_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN4_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4_ATRIB  SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CATALOG_LIN  SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE LINEAS_PEDIDO  SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART_UP  SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_DEF SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GRUPO SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GRUPO_DEF SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GRUPO_ESP SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PRES1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PRES2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PRES3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PRES4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_PRES1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_PRES2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_PRES3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_PRES4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_UON1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_UON2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_UON3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_UON1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_UON2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_UON3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_GR_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_GR_ADJUN SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_ITEM_ADJUN SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_ITEM_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ALL SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ALL_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ALL_PROVE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_GRUPO SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_GRUPO_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_GRUPO_PROVE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ITEM SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ITEM_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ITEM_PROVE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_PROCE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_PROCE_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_PROCE_PROVE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_PROCE_SOBRE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GRUPO_OFE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GRUPO_OFE_REU SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE USAR_GR_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_CONTR SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE SOBRE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART4_ADJUN SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE FAVORITOS_LINEAS_PEDIDO SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_NUMDEC SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES_ART SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISOR SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART1_ADJ CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART2_ADJ CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART3_ADJ CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ADJ CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE DETALLE_PUJAS CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ADJ CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ADJREU CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ESP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OBJ CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OFE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OFE_WEB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRES CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESCON CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESCON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESCON4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESPROY CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFEMIN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_ESP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_WEB_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_WEB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PER CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_PET CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_POT CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVENOTIF CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE REU_PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_GRUPO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ESP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4_ACT CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATALOG_LIN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_PEDIDO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART_UP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_DEF CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO_DEF CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO_ESP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_GR_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_GR_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ITEM_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ITEM_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_SOBRE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GRUPO_OFE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GRUPO_OFE_REU CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USAR_GR_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_CONTR CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE SOBRE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FAVORITOS_LINEAS_PEDIDO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_NUMDEC CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_ART CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISOR CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--INTEGRACION" & vbCrLf
sConsulta = sConsulta & "IF @INT_ORIGEN <> ''" & vbCrLf
sConsulta = sConsulta & "INSERT INTO LOG_GMN (ACCION,GMN1,DEN,COD_NEW,ORIGEN,USU) VALUES ('C',@VAR_COD,@VAR_DEN,@NEW,@INT_ORIGEN,@USUARIO)" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close C" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "COMMIT TRANSACTION" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT OFF" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE GMN2_COD (@OLD VARCHAR(50),@NEW VARCHAR(50),@GMN1 VARCHAR(50),@INT_ORIGEN VARCHAR(1),@USUARIO VARCHAR(50))  AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_DEN VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT ON" & vbCrLf
sConsulta = sConsulta & "BEGIN TRANSACTION" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR FOR SELECT COD,DEN FROM GMN2 WHERE GMN1=@GMN1 AND COD=@OLD" & vbCrLf
sConsulta = sConsulta & "OPEN C" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C INTO @VAR_COD,@VAR_DEN" & vbCrLf
sConsulta = sConsulta & "IF @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART2_ADJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART3_ADJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ADJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ESP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4_ACT NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATALOG_LIN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_PEDIDO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART_UP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FAVORITOS_LINEAS_PEDIDO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_ART NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISOR NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "----" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN2 SET COD=@NEW WHERE GMN1=@GMN1 AND COD=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART2 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART2_ADJ SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART3 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART3_ADJ SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4_ADJ SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE COM_GMN2 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE COM_GMN3 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE COM_GMN4 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN3 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN4 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES_MAT2 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES_MAT3 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES_MAT4 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_GMN2 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_GMN3 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_GMN4 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_PROCE SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART2 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART3 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART4 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4_ESP SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN4_ACT SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4_ATRIB SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN4_ATRIB SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART4_ATRIB SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CATALOG_LIN SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE LINEAS_PEDIDO SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART_UP SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART4_ADJUN SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE FAVORITOS_LINEAS_PEDIDO SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES_ART SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISOR SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "----" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART2_ADJ CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART3_ADJ CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ADJ CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ESP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4_ACT CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATALOG_LIN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_PEDIDO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART_UP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FAVORITOS_LINEAS_PEDIDO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_ART CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISOR CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--INTEGRACION" & vbCrLf
sConsulta = sConsulta & "IF @INT_ORIGEN <> ''" & vbCrLf
sConsulta = sConsulta & "INSERT INTO LOG_GMN (ACCION,GMN1,GMN2,DEN,COD_NEW,ORIGEN,USU) VALUES ('C',@GMN1,@VAR_COD,@VAR_DEN,@NEW,@INT_ORIGEN,@USUARIO)" & vbCrLf
sConsulta = sConsulta & "----" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close C" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT OFF" & vbCrLf
sConsulta = sConsulta & "COMMIT TRANSACTION" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE GMN3_COD (@OLD VARCHAR(50),@NEW VARCHAR(50),@GMN1 VARCHAR(50),@GMN2 VARCHAR(50),@INT_ORIGEN VARCHAR(1),@USUARIO VARCHAR(50))  AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_DEN VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT ON" & vbCrLf
sConsulta = sConsulta & "BEGIN TRANSACTION" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR FOR SELECT COD,DEN FROM GMN3 WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND COD=@OLD" & vbCrLf
sConsulta = sConsulta & "OPEN C" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C INTO @VAR_COD,@VAR_DEN" & vbCrLf
sConsulta = sConsulta & "IF @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART3_ADJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ADJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ESP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4_ACT NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATALOG_LIN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_PEDIDO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART_UP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FAVORITOS_LINEAS_PEDIDO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_ART NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISOR NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "----" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN3 SET COD=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND COD=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART3 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART3_ADJ SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4_ADJ SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE COM_GMN3 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE COM_GMN4 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN4 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES_MAT3 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES_MAT4 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_GMN3 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_GMN4 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_PROCE SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART3 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART4 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4_ESP SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN4_ACT SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4_ATRIB SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN4_ATRIB SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CATALOG_LIN SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE LINEAS_PEDIDO SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART_UP SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART4_ATRIB SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART4_ADJUN SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE FAVORITOS_LINEAS_PEDIDO SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES_ART SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISOR SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "----" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART3_ADJ CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ADJ CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ESP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4_ACT CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATALOG_LIN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_PEDIDO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART_UP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FAVORITOS_LINEAS_PEDIDO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_ART CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISOR CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "--INTEGRACION" & vbCrLf
sConsulta = sConsulta & "IF @INT_ORIGEN <> ''" & vbCrLf
sConsulta = sConsulta & "INSERT INTO LOG_GMN (ACCION,GMN1,GMN2,GMN3,DEN,COD_NEW,ORIGEN,USU) VALUES ('C',@GMN1,@GMN2,@VAR_COD,@VAR_DEN,@NEW,@INT_ORIGEN,@USUARIO)" & vbCrLf
sConsulta = sConsulta & "----" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close C" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "COMMIT TRANSACTION" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT OFF" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE GMN4_COD (@OLD VARCHAR(50),@NEW VARCHAR(50),@GMN1 VARCHAR(50),@GMN2 VARCHAR(50),@GMN3 VARCHAR(50),@INT_ORIGEN VARCHAR(1),@USUARIO VARCHAR(50))  AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_DEN VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT ON" & vbCrLf
sConsulta = sConsulta & "BEGIN TRANSACTION" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR FOR SELECT COD,DEN FROM GMN4 WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND COD=@OLD" & vbCrLf
sConsulta = sConsulta & "OPEN C" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C INTO @VAR_COD,@VAR_DEN" & vbCrLf
sConsulta = sConsulta & "IF @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ADJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ESP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4_ACT NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATALOG_LIN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_PEDIDO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART_UP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FAVORITOS_LINEAS_PEDIDO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_ART NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISOR NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "----" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN4 SET COD=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND COD=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4 SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4_ADJ SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE COM_GMN4 SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES_MAT4 SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_GMN4 SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_PROCE SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART4 SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4_ESP SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN4_ACT SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART4_ATRIB SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN4_ATRIB SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4_ATRIB SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CATALOG_LIN SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE LINEAS_PEDIDO SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART_UP SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART4_ADJUN SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE FAVORITOS_LINEAS_PEDIDO SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES_ART SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISOR SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD" & vbCrLf
sConsulta = sConsulta & "----" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ADJ CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ESP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4_ACT CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATALOG_LIN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_PEDIDO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART_UP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FAVORITOS_LINEAS_PEDIDO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_ART CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISOR CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "--INTEGRACION" & vbCrLf
sConsulta = sConsulta & "IF @INT_ORIGEN <> ''" & vbCrLf
sConsulta = sConsulta & "INSERT INTO LOG_GMN (ACCION,GMN1,GMN2,GMN3,GMN4,DEN,COD_NEW,ORIGEN,USU) VALUES ('C',@GMN1,@GMN2,@GMN3,@VAR_COD,@VAR_DEN,@NEW,@INT_ORIGEN,@USUARIO)" & vbCrLf
sConsulta = sConsulta & "----" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close C" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT OFF" & vbCrLf
sConsulta = sConsulta & "COMMIT TRANSACTION" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE SP_CARGAR_ULTIMAS_OFERTAS_GRUPOS @ANYO int, @GMN1 varchar(50), @PROCE int, @IDI VARCHAR(50)=NULL AS" & vbCrLf
sConsulta = sConsulta & "      SELECT PROVE.COD,PROVE.DEN,PROCE_OFE.OFE,PROCE_OFE.EST,PROCE_OFE.FECREC,PROCE_OFE.FECVAL," & vbCrLf
sConsulta = sConsulta & "      PROCE_OFE.MON,PROCE_OFE.CAMBIO,PROCE_OFE.OBS,MON.DEN AS MONDEN,OFEEST_IDIOMA.DEN AS ESTDEN,GRUPO_OFE.GRUPO,GRUPO_OFE.OBSADJUN, PROCE_OFE.PORTAL, PROCE_OFE.USU, PROCE_OFE.NOMUSU" & vbCrLf
sConsulta = sConsulta & "      FROM PROCE_OFE" & vbCrLf
sConsulta = sConsulta & "      INNER JOIN GRUPO_OFE ON PROCE_OFE.ANYO=GRUPO_OFE.ANYO AND PROCE_OFE.GMN1=GRUPO_OFE.GMN1 AND PROCE_OFE.PROCE=GRUPO_OFE.PROCE" & vbCrLf
sConsulta = sConsulta & "       AND PROCE_OFE.PROVE=GRUPO_OFE.PROVE AND PROCE_OFE.OFE=GRUPO_OFE.OFE" & vbCrLf
sConsulta = sConsulta & "      INNER JOIN MON ON PROCE_OFE.MON=MON.COD" & vbCrLf
sConsulta = sConsulta & "      INNER JOIN PROCE_PROVE ON PROCE_OFE.PROVE=PROCE_PROVE.PROVE AND PROCE_OFE.ANYO=PROCE_PROVE.ANYO AND PROCE_OFE.GMN1=PROCE_PROVE.GMN1 AND PROCE_OFE.PROCE=PROCE_PROVE.PROCE" & vbCrLf
sConsulta = sConsulta & "      INNER JOIN PROVE ON PROCE_PROVE.PROVE=PROVE.COD" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN PROVI ON PROVE.PAI=PROVI.PAI AND PROVE.PROVI=PROVI.COD" & vbCrLf
sConsulta = sConsulta & "      INNER JOIN OFEEST ON PROCE_OFE.EST=OFEEST.COD AND OFEEST.COMP= 1" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN OFEEST_IDIOMA ON OFEEST.COD= OFEEST_IDIOMA.COD AND OFEEST_IDIOMA.IDIOMA =@IDI" & vbCrLf
sConsulta = sConsulta & "      WHERE PROCE_OFE.ANYO=@ANYO AND PROCE_OFE.GMN1=@GMN1" & vbCrLf
sConsulta = sConsulta & "      AND PROCE_OFE.PROCE=@PROCE AND PROCE_OFE.ULT=1 AND PROCE_OFE.ENVIADA=1" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE SP_ELIMINAR_ADJUDICACIONES(@ANYO AS SMALLINT,@GMN1 AS VARCHAR(50), @PROCE AS SMALLINT) AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @EST INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @HAY_REU INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @FECREU DATETIME" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF  (SELECT COUNT(*) FROM ITEM_ADJ WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE) >0 " & vbCrLf
sConsulta = sConsulta & "BEGIN  " & vbCrLf
sConsulta = sConsulta & "      SET @EST = (SELECT EST FROM PROCE  WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE)" & vbCrLf
sConsulta = sConsulta & "      IF @EST>=9 AND @EST<=11" & vbCrLf
sConsulta = sConsulta & "          BEGIN" & vbCrLf
sConsulta = sConsulta & "       SET @HAY_REU=(SELECT COUNT(*) FROM REU_PROCE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       DELETE FROM ITEM_ADJ WHERE ANYO=@ANYO  AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "                 UPDATE GRUPO_OFE SET CONSUMIDO=NULL,ADJUDICADO=NULL,AHORRADO=NULL,AHORRADO_PORCEN=NULL WHERE ANYO=@ANYO  AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       IF @HAY_REU>0 " & vbCrLf
sConsulta = sConsulta & "                    BEGIN" & vbCrLf
sConsulta = sConsulta & "             DELETE FROM ITEM_ADJREU WHERE ANYO=@ANYO  AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "             UPDATE GRUPO_OFE_REU SET CONSUMIDO=NULL,ADJUDICADO=NULL,AHORRADO=NULL,AHORRADO_PORCEN=NULL WHERE ANYO=@ANYO  AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                   IF @EST = 9 " & vbCrLf
sConsulta = sConsulta & "       UPDATE PROCE SET ADJUDICADO=NULL,CONSUMIDO=NULL,FECULTREU=NULL,EST=8 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "       ELSE" & vbCrLf
sConsulta = sConsulta & "                UPDATE PROCE SET ADJUDICADO=NULL,CONSUMIDO=NULL,FECULTREU=NULL,EST=7  WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "       --Llama al stored que calcula el precio v�lido y el abierto de GRUPO_OFE" & vbCrLf
sConsulta = sConsulta & "                 EXEC SP_CALCULAR_PREC_VALIDO @ANYO, @GMN1, @PROCE   " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                 IF @HAY_REU>0" & vbCrLf
sConsulta = sConsulta & "         BEGIN              " & vbCrLf
sConsulta = sConsulta & "           DECLARE CUR_REU CURSOR LOCAL FORWARD_ONLY FOR" & vbCrLf
sConsulta = sConsulta & "               SELECT FECHA FROM REU_PROCE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "           OPEN CUR_REU" & vbCrLf
sConsulta = sConsulta & "                    FETCH NEXT FROM CUR_REU INTO @FECREU" & vbCrLf
sConsulta = sConsulta & "                              WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "                                  BEGIN" & vbCrLf
sConsulta = sConsulta & "           EXEC SP_CALCULAR_PREC_VALIDO_REU @ANYO, @GMN1, @PROCE, @FECREU" & vbCrLf
sConsulta = sConsulta & "                                        FETCH NEXT FROM CUR_REU INTO @FECREU" & vbCrLf
sConsulta = sConsulta & "                                  END" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "         CLOSE CUR_REU" & vbCrLf
sConsulta = sConsulta & "         DEALLOCATE CUR_REU" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "         END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE SP_TRASPASAR_OFERTA @ID int , @NUM INT OUTPUT, @NUMNUE INT OUTPUT, @NUMAREV INT OUTPUT, @ERROR SMALLINT = NULL OUTPUT, @COMPROBARATRIBS TINYINT = 0  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @NEWOFE INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @NUMOFEWEB AS int" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN1 VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCE INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVE VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @MON VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @ADJUN INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDADJUN INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDGS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @GRUPO VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @ITEM INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @NUMOBJ INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @NUMOBJGS INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @ERROR = 0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @ANYO = ANYO, @GMN1= GMN1, @PROCE = PROCE, @PROVE = PROVE, @MON = MON, @NUMOBJ = NUMOBJ" & vbCrLf
sConsulta = sConsulta & "  FROM P_PROCE_OFE " & vbCrLf
sConsulta = sConsulta & " WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @NUMOBJGS = (SELECT NUMOBJ FROM PROCE_PROVE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE)" & vbCrLf
sConsulta = sConsulta & "IF @NUMOBJGS  <=@NUMOBJ" & vbCrLf
sConsulta = sConsulta & "  UPDATE PROCE_PROVE SET OBJNUEVOS=0 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "/*" & vbCrLf
sConsulta = sConsulta & "DECLARE @EQUIV FLOAT " & vbCrLf
sConsulta = sConsulta & "SELECT @EQUIV = (MON.EQUIV / PROCE.CAMBIO) " & vbCrLf
sConsulta = sConsulta & "  FROM PROCE " & vbCrLf
sConsulta = sConsulta & "     INNER JOIN MON " & vbCrLf
sConsulta = sConsulta & "             ON PROCE.ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "            AND PROCE.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND PROCE.COD=@PROCE  " & vbCrLf
sConsulta = sConsulta & "            AND MON.COD=@MON " & vbCrLf
sConsulta = sConsulta & "*/" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @SUBASTA INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @SUBASTA = SUBASTA " & vbCrLf
sConsulta = sConsulta & "  FROM PROCE_DEF " & vbCrLf
sConsulta = sConsulta & " WHERE ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "   AND GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "   AND PROCE=@PROCE  " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @SUBASTA = 1 OR @COMPROBARATRIBS  = 1" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    DECLARE @OFE INT" & vbCrLf
sConsulta = sConsulta & "    SET @OFE = ( SELECT MAX(OFE) FROM PROCE_OFE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE AND OFE!=-1)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    IF @OFE IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "      BEGIN" & vbCrLf
sConsulta = sConsulta & "        INSERT INTO P_OFE_GR_ATRIB (ID, GRUPO, ATRIB_ID,  VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL)" & vbCrLf
sConsulta = sConsulta & "        SELECT @ID, O.GRUPO, O.ATRIB_ID, O.VALOR_NUM, O.VALOR_TEXT, O.VALOR_FEC, O.VALOR_BOOL" & vbCrLf
sConsulta = sConsulta & "          FROM OFE_GR_ATRIB O" & vbCrLf
sConsulta = sConsulta & "         WHERE O.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "           AND O.GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "           AND O.PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "           AND O.PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "           AND O.OFE = @OFE" & vbCrLf
sConsulta = sConsulta & "           AND NOT EXISTS (SELECT * FROM P_OFE_GR_ATRIB T WHERE T.ID = @ID AND T.GRUPO = O.GRUPO AND T.ATRIB_ID = O.ATRIB_ID)" & vbCrLf
sConsulta = sConsulta & "        IF @@ERROR!=0 GOTO ERRORSINTRAN_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "        INSERT INTO P_OFE_ITEM_ATRIB (ID, GRUPO, ITEM, ATRIB_ID,  VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL)" & vbCrLf
sConsulta = sConsulta & "        SELECT @ID, I.GRUPO, IA.ITEM, ATRIB_ID, VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL" & vbCrLf
sConsulta = sConsulta & "          FROM OFE_ITEM_ATRIB IA" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN ITEM I" & vbCrLf
sConsulta = sConsulta & "                   ON I.ANYO = IA.ANYO" & vbCrLf
sConsulta = sConsulta & "                  AND I.GMN1 = IA.GMN1" & vbCrLf
sConsulta = sConsulta & "                  AND I.PROCE = IA.PROCE" & vbCrLf
sConsulta = sConsulta & "                  AND I.ID = IA.ITEM" & vbCrLf
sConsulta = sConsulta & "         WHERE IA.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "           AND IA.GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "           AND IA.PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "           AND IA.PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "           AND IA.OFE = @OFE" & vbCrLf
sConsulta = sConsulta & "           AND NOT EXISTS (SELECT * FROM P_OFE_ITEM_ATRIB T WHERE T.ID = @ID AND T.ATRIB_ID = IA.ATRIB_ID AND T.ITEM = IA.ITEM)" & vbCrLf
sConsulta = sConsulta & "        IF @@ERROR!=0 GOTO ERRORSINTRAN_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "        INSERT INTO P_OFE_ATRIB (ID, ATRIB_ID,  VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL)" & vbCrLf
sConsulta = sConsulta & "        SELECT @ID, O.ATRIB_ID, O.VALOR_NUM, O.VALOR_TEXT, O.VALOR_FEC, O.VALOR_BOOL" & vbCrLf
sConsulta = sConsulta & "          FROM OFE_ATRIB O" & vbCrLf
sConsulta = sConsulta & "         WHERE O.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "           AND O.GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "           AND O.PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "           AND O.PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "           AND O.OFE = @OFE" & vbCrLf
sConsulta = sConsulta & "           AND NOT EXISTS (SELECT * FROM P_OFE_ATRIB T WHERE T.ID = @ID AND T.ATRIB_ID = O.ATRIB_ID)" & vbCrLf
sConsulta = sConsulta & "        IF @@ERROR!=0 GOTO ERRORSINTRAN_" & vbCrLf
sConsulta = sConsulta & "      END " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "      DECLARE @OFEATRIB BIT" & vbCrLf
sConsulta = sConsulta & "      DECLARE @OFEGRUPOATRIB BIT" & vbCrLf
sConsulta = sConsulta & "      DECLARE @OFEITEMATRIB BIT" & vbCrLf
sConsulta = sConsulta & "      DECLARE @PAID INT" & vbCrLf
sConsulta = sConsulta & "      DECLARE @AMBITO SMALLINT" & vbCrLf
sConsulta = sConsulta & "      DECLARE @TIPO SMALLINT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "      DECLARE CATRIBS CURSOR LOCAL FORWARD_ONLY  FOR " & vbCrLf
sConsulta = sConsulta & "      SELECT PA.ID, AMBITO, GRUPO, TIPO" & vbCrLf
sConsulta = sConsulta & "        FROM PROCE_ATRIB PA" & vbCrLf
sConsulta = sConsulta & "         INNER JOIN DEF_ATRIB DA" & vbCrLf
sConsulta = sConsulta & "                 ON PA.ATRIB = DA.ID" & vbCrLf
sConsulta = sConsulta & "       WHERE ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "         AND GMN1= @GMN1" & vbCrLf
sConsulta = sConsulta & "         AND PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "         AND INTERNO = 0" & vbCrLf
sConsulta = sConsulta & "         AND OBLIG = 1" & vbCrLf
sConsulta = sConsulta & "      " & vbCrLf
sConsulta = sConsulta & "      OPEN CATRIBS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "      FETCH NEXT FROM CATRIBS INTO @PAID, @AMBITO, @GRUPO,  @TIPO" & vbCrLf
sConsulta = sConsulta & "      SET @OFEATRIB=0" & vbCrLf
sConsulta = sConsulta & "      SET @OFEGRUPOATRIB=0" & vbCrLf
sConsulta = sConsulta & "      SET @OFEITEMATRIB=0" & vbCrLf
sConsulta = sConsulta & "      WHILE @@FETCH_STATUS=0 AND @OFEATRIB=0 AND @OFEGRUPOATRIB=0 AND @OFEITEMATRIB=0" & vbCrLf
sConsulta = sConsulta & "        BEGIN" & vbCrLf
sConsulta = sConsulta & "          IF @AMBITO = 1" & vbCrLf
sConsulta = sConsulta & "            BEGIN" & vbCrLf
sConsulta = sConsulta & "              SET @OFEATRIB =1" & vbCrLf
sConsulta = sConsulta & "              SELECT @OFEATRIB = CASE " & vbCrLf
sConsulta = sConsulta & "                        WHEN CASE @TIPO " & vbCrLf
sConsulta = sConsulta & "                                WHEN 1 THEN CASE WHEN VALOR_TEXT ='' THEN NULL ELSE VALOR_TEXT END " & vbCrLf
sConsulta = sConsulta & "                                WHEN 2 THEN CAST(VALOR_NUM AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                                WHEN 3 THEN CAST(VALOR_FEC AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                                WHEN 4 THEN CAST(VALOR_BOOL AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                             END IS NULL " & vbCrLf
sConsulta = sConsulta & "                        THEN 1 " & vbCrLf
sConsulta = sConsulta & "                        ELSE 0 " & vbCrLf
sConsulta = sConsulta & "                     END" & vbCrLf
sConsulta = sConsulta & "                FROM P_OFE_ATRIB" & vbCrLf
sConsulta = sConsulta & "               WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "                 AND ATRIB_ID = @PAID" & vbCrLf
sConsulta = sConsulta & "            END" & vbCrLf
sConsulta = sConsulta & "          IF @AMBITO = 2" & vbCrLf
sConsulta = sConsulta & "            BEGIN" & vbCrLf
sConsulta = sConsulta & "              SET @OFEGRUPOATRIB =0" & vbCrLf
sConsulta = sConsulta & "              IF @GRUPO IS NULL " & vbCrLf
sConsulta = sConsulta & "                BEGIN" & vbCrLf
sConsulta = sConsulta & "                IF (SELECT COUNT(*) " & vbCrLf
sConsulta = sConsulta & "                      FROM P_OFE_GR_ATRIB " & vbCrLf
sConsulta = sConsulta & "                     WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "                       AND ATRIB_ID = @PAID" & vbCrLf
sConsulta = sConsulta & "                       AND CASE @TIPO " & vbCrLf
sConsulta = sConsulta & "                                WHEN 1 THEN CASE WHEN VALOR_TEXT ='' THEN NULL ELSE VALOR_TEXT END " & vbCrLf
sConsulta = sConsulta & "                                WHEN 2 THEN CAST(VALOR_NUM AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                                WHEN 3 THEN CAST(VALOR_FEC AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                                WHEN 4 THEN CAST(VALOR_BOOL AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                             END IS NOT NULL) <> (SELECT COUNT(*) " & vbCrLf
sConsulta = sConsulta & "                       FROM PROCE_GRUPO " & vbCrLf
sConsulta = sConsulta & "                      WHERE ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "                        AND GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "                        AND PROCE = @PROCE)" & vbCrLf
sConsulta = sConsulta & "                   SET @OFEGRUPOATRIB = 1" & vbCrLf
sConsulta = sConsulta & "                 END" & vbCrLf
sConsulta = sConsulta & "               ELSE" & vbCrLf
sConsulta = sConsulta & "                IF (SELECT COUNT(*) " & vbCrLf
sConsulta = sConsulta & "                      FROM P_OFE_GR_ATRIB " & vbCrLf
sConsulta = sConsulta & "                     WHERE GRUPO = @GRUPO" & vbCrLf
sConsulta = sConsulta & "                       AND ATRIB_ID = @PAID" & vbCrLf
sConsulta = sConsulta & "                       AND CASE @TIPO " & vbCrLf
sConsulta = sConsulta & "                                WHEN 1 THEN CASE WHEN VALOR_TEXT ='' THEN NULL ELSE VALOR_TEXT END " & vbCrLf
sConsulta = sConsulta & "                                WHEN 2 THEN CAST(VALOR_NUM AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                                WHEN 3 THEN CAST(VALOR_FEC AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                                WHEN 4 THEN CAST(VALOR_BOOL AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                             END IS NOT NULL)  =0 " & vbCrLf
sConsulta = sConsulta & "                       SET @OFEGRUPOATRIB = 1" & vbCrLf
sConsulta = sConsulta & "            END" & vbCrLf
sConsulta = sConsulta & "          IF @AMBITO = 3" & vbCrLf
sConsulta = sConsulta & "            BEGIN" & vbCrLf
sConsulta = sConsulta & "              SET @OFEITEMATRIB =0" & vbCrLf
sConsulta = sConsulta & "              IF @GRUPO IS NULL " & vbCrLf
sConsulta = sConsulta & "                BEGIN" & vbCrLf
sConsulta = sConsulta & "                  IF (SELECT COUNT(*) " & vbCrLf
sConsulta = sConsulta & "                        FROM P_OFE_ITEM_ATRIB " & vbCrLf
sConsulta = sConsulta & "                       WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "                         AND ATRIB_ID = @PAID" & vbCrLf
sConsulta = sConsulta & "                         AND CASE @TIPO " & vbCrLf
sConsulta = sConsulta & "                                WHEN 1 THEN CASE WHEN VALOR_TEXT ='' THEN NULL ELSE VALOR_TEXT END " & vbCrLf
sConsulta = sConsulta & "                                WHEN 2 THEN CAST(VALOR_NUM AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                                WHEN 3 THEN CAST(VALOR_FEC AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                                WHEN 4 THEN CAST(VALOR_BOOL AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                     END IS NOT NULL)  <>" & vbCrLf
sConsulta = sConsulta & "                      (SELECT COUNT(*) " & vbCrLf
sConsulta = sConsulta & "                         FROM ITEM " & vbCrLf
sConsulta = sConsulta & "                        WHERE ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "                          AND GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "                          AND PROCE = @PROCE                    " & vbCrLf
sConsulta = sConsulta & "                          AND CONF = 1)" & vbCrLf
sConsulta = sConsulta & "                       SET @OFEITEMATRIB = 1" & vbCrLf
sConsulta = sConsulta & "                 END" & vbCrLf
sConsulta = sConsulta & "              ELSE" & vbCrLf
sConsulta = sConsulta & "                 begin" & vbCrLf
sConsulta = sConsulta & "                  IF (SELECT COUNT(*) " & vbCrLf
sConsulta = sConsulta & "                        FROM P_OFE_ITEM_ATRIB" & vbCrLf
sConsulta = sConsulta & "                       WHERE GRUPO = @GRUPO" & vbCrLf
sConsulta = sConsulta & "                         AND ID = @ID" & vbCrLf
sConsulta = sConsulta & "                         AND ATRIB_ID = @PAID" & vbCrLf
sConsulta = sConsulta & "                         AND CASE @TIPO " & vbCrLf
sConsulta = sConsulta & "                                WHEN 1 THEN CASE WHEN VALOR_TEXT ='' THEN NULL ELSE VALOR_TEXT END " & vbCrLf
sConsulta = sConsulta & "                                WHEN 2 THEN CAST(VALOR_NUM AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                                WHEN 3 THEN CAST(VALOR_FEC AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                                WHEN 4 THEN CAST(VALOR_BOOL AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                             END IS NOT NULL)  <>" & vbCrLf
sConsulta = sConsulta & "                      (SELECT COUNT(*) " & vbCrLf
sConsulta = sConsulta & "                         FROM ITEM " & vbCrLf
sConsulta = sConsulta & "                        WHERE ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "                          AND GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "                          AND PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "                          AND GRUPO = @GRUPO" & vbCrLf
sConsulta = sConsulta & "                          AND CONF = 1)" & vbCrLf
sConsulta = sConsulta & "                       SET @OFEITEMATRIB = 1" & vbCrLf
sConsulta = sConsulta & "                   end" & vbCrLf
sConsulta = sConsulta & "            END" & vbCrLf
sConsulta = sConsulta & "            FETCH NEXT FROM CATRIBS INTO @PAID, @AMBITO, @GRUPO,  @TIPO" & vbCrLf
sConsulta = sConsulta & "          END" & vbCrLf
sConsulta = sConsulta & "      CLOSE CATRIBS" & vbCrLf
sConsulta = sConsulta & "      DEALLOCATE CATRIBS" & vbCrLf
sConsulta = sConsulta & "      IF @OFEATRIB = 1" & vbCrLf
sConsulta = sConsulta & "        SET @ERROR = 2" & vbCrLf
sConsulta = sConsulta & "      IF @OFEGRUPOATRIB = 1" & vbCrLf
sConsulta = sConsulta & "        SET @ERROR = 3" & vbCrLf
sConsulta = sConsulta & "      IF @OFEITEMATRIB = 1" & vbCrLf
sConsulta = sConsulta & "        SET @ERROR = 4" & vbCrLf
sConsulta = sConsulta & "      IF @ERROR>0" & vbCrLf
sConsulta = sConsulta & "        GOTO ELIMINAR_" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "DECLARE  @ESTWEB VARCHAR(3) " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "SELECT @ESTWEB=ESTWEB FROM PARGEN_DEF INNER JOIN PARGEN_GEST ON PARGEN_DEF.ID=PARGEN_GEST.ID  " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN TRANSACTION" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE SET ULT= 0, LEIDO = 1  WHERE ANYO = @ANYO AND GMN1 = @GMN1 AND PROCE = @PROCE AND PROVE = @PROVE AND ULT = 1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @NEWOFE = ISNULL(MAX(OFE),0) + 1 FROM PROCE_OFE WHERE ANYO = @ANYO AND GMN1 = @GMN1 AND PROCE = @PROCE AND PROVE = @PROVE AND OFE != -1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO PROCE_OFE(ANYO, GMN1, PROCE, PROVE, OFE, EST, FECREC, FECVAL, MON, CAMBIO, OBS, LEIDO, ULT, ENVIADA, OBSADJUN, PORTAL,USU,NOMUSU)" & vbCrLf
sConsulta = sConsulta & "SELECT @ANYO, @GMN1, @PROCE, @PROVE, @NEWOFE, @ESTWEB, getdate(), FECVAL, MON, CAMBIO, OBS, 0, 1, 1, OBSADJUN, PORTAL,USU,NOMUSU" & vbCrLf
sConsulta = sConsulta & "  FROM P_PROCE_OFE" & vbCrLf
sConsulta = sConsulta & "WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE GRUPO_OFE " & vbCrLf
sConsulta = sConsulta & "   SET OBSADJUN = PGO.OBSADJUN" & vbCrLf
sConsulta = sConsulta & "  FROM GRUPO_OFE GO" & vbCrLf
sConsulta = sConsulta & "      INNER JOIN P_GRUPO_OFE PGO" & vbCrLf
sConsulta = sConsulta & "              ON GO.GRUPO = PGO.GRUPO" & vbCrLf
sConsulta = sConsulta & " WHERE GO.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "   AND GO.GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "   AND GO.PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "   AND GO.PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "   AND GO.OFE = @NEWOFE" & vbCrLf
sConsulta = sConsulta & "   AND PGO.ID = @ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_OFE" & vbCrLf
sConsulta = sConsulta & "   SET PRECIO = PIO.PRECIO," & vbCrLf
sConsulta = sConsulta & "       CANTMAX = PIO.CANTMAX," & vbCrLf
sConsulta = sConsulta & "       PRECIO2 = PIO.PRECIO2," & vbCrLf
sConsulta = sConsulta & "       PRECIO3 = PIO.PRECIO3," & vbCrLf
sConsulta = sConsulta & "       PREC_VALIDO = PIO.PRECIO," & vbCrLf
sConsulta = sConsulta & "       OBSADJUN = PIO.OBSADJUN," & vbCrLf
sConsulta = sConsulta & "       COMENT1 = PIO.COMENT1," & vbCrLf
sConsulta = sConsulta & "       COMENT2 = PIO.COMENT2," & vbCrLf
sConsulta = sConsulta & "       COMENT3 = PIO.COMENT3" & vbCrLf
sConsulta = sConsulta & "  FROM ITEM_OFE IO" & vbCrLf
sConsulta = sConsulta & "     INNER JOIN P_ITEM_OFE PIO" & vbCrLf
sConsulta = sConsulta & "             ON IO.ITEM = PIO.ITEM" & vbCrLf
sConsulta = sConsulta & "     INNER JOIN ITEM I ON IO.ANYO=I.ANYO AND IO.GMN1=I.GMN1 AND IO.PROCE=I.PROCE AND IO.ITEM= I.ID " & vbCrLf
sConsulta = sConsulta & " WHERE IO.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "   AND IO.GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "   AND IO.PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "   AND IO.PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "   AND IO.NUM = @NEWOFE" & vbCrLf
sConsulta = sConsulta & "   AND PIO.ID = @ID" & vbCrLf
sConsulta = sConsulta & "   AND (I.EST=0 OR I.EST IS NULL)" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_ITEM_ADJUN " & vbCrLf
sConsulta = sConsulta & "  FROM OFE_ITEM_ADJUN R" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN P_ITEM_OFE PIO" & vbCrLf
sConsulta = sConsulta & "               ON PIO.ID = @ID" & vbCrLf
sConsulta = sConsulta & "              AND R.ITEM = PIO.ITEM" & vbCrLf
sConsulta = sConsulta & " WHERE R.ANYO = @ANYO " & vbCrLf
sConsulta = sConsulta & "   AND R.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "   AND R.PROCE= @PROCE " & vbCrLf
sConsulta = sConsulta & "   AND R.PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "   AND R.OFE IN (@NEWOFE,-1)" & vbCrLf
sConsulta = sConsulta & "   AND PIO.ADJUN = 1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_ITEM_ATRIB " & vbCrLf
sConsulta = sConsulta & "  FROM OFE_ITEM_ATRIB R" & vbCrLf
sConsulta = sConsulta & " WHERE R.ANYO = @ANYO " & vbCrLf
sConsulta = sConsulta & "   AND R.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "   AND R.PROCE= @PROCE " & vbCrLf
sConsulta = sConsulta & "   AND R.PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "   AND R.OFE IN (@NEWOFE,-1)" & vbCrLf
sConsulta = sConsulta & "   AND EXISTS (SELECT * FROM P_OFE_ITEM_ATRIB P  " & vbCrLf
sConsulta = sConsulta & "                WHERE P.ID  = @ID " & vbCrLf
sConsulta = sConsulta & "                  AND P.ITEM = R.ITEM" & vbCrLf
sConsulta = sConsulta & "                  AND P.ATRIB_ID = R.ATRIB_ID)" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_GR_ATRIB " & vbCrLf
sConsulta = sConsulta & "  FROM OFE_GR_ATRIB R" & vbCrLf
sConsulta = sConsulta & " WHERE R.ANYO = @ANYO " & vbCrLf
sConsulta = sConsulta & "   AND R.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "   AND R.PROCE= @PROCE " & vbCrLf
sConsulta = sConsulta & "   AND R.PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "   AND R.OFE IN (@NEWOFE,-1)" & vbCrLf
sConsulta = sConsulta & "   AND EXISTS (SELECT * FROM P_OFE_GR_ATRIB P " & vbCrLf
sConsulta = sConsulta & "                WHERE P.ID  = @ID " & vbCrLf
sConsulta = sConsulta & "                  AND CAST(P.GRUPO AS VARCHAR(50)) = CAST(R.GRUPO AS VARCHAR(50))" & vbCrLf
sConsulta = sConsulta & "                  AND P.ATRIB_ID = R.ATRIB_ID)" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_GR_ADJUN " & vbCrLf
sConsulta = sConsulta & "  FROM OFE_GR_ADJUN R" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN P_GRUPO_OFE PGO" & vbCrLf
sConsulta = sConsulta & "            ON PGO.ID = @ID" & vbCrLf
sConsulta = sConsulta & "           AND PGO.GRUPO = R.GRUPO" & vbCrLf
sConsulta = sConsulta & " WHERE R.ANYO = @ANYO " & vbCrLf
sConsulta = sConsulta & "   AND R.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "   AND R.PROCE= @PROCE " & vbCrLf
sConsulta = sConsulta & "   AND R.PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "   AND R.OFE IN (@NEWOFE,-1)" & vbCrLf
sConsulta = sConsulta & "   AND PGO.ADJUN = 1" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_ATRIB " & vbCrLf
sConsulta = sConsulta & "  FROM OFE_ATRIB R" & vbCrLf
sConsulta = sConsulta & " WHERE R.ANYO = @ANYO " & vbCrLf
sConsulta = sConsulta & "   AND R.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "   AND R.PROCE= @PROCE " & vbCrLf
sConsulta = sConsulta & "   AND R.PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "   AND R.OFE IN (@NEWOFE,-1)" & vbCrLf
sConsulta = sConsulta & "   AND EXISTS (SELECT * FROM P_OFE_ATRIB P " & vbCrLf
sConsulta = sConsulta & "                WHERE P.ID  = @ID " & vbCrLf
sConsulta = sConsulta & "                  AND P.ATRIB_ID = R.ATRIB_ID)" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF (SELECT ADJUN FROM P_PROCE_OFE WHERE ID = @ID) = 1" & vbCrLf
sConsulta = sConsulta & "DELETE FROM PROCE_OFE_ADJUN " & vbCrLf
sConsulta = sConsulta & "  FROM PROCE_OFE_ADJUN R" & vbCrLf
sConsulta = sConsulta & " WHERE R.ANYO = @ANYO " & vbCrLf
sConsulta = sConsulta & "   AND R.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "   AND R.PROCE= @PROCE " & vbCrLf
sConsulta = sConsulta & "   AND R.PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "   AND R.OFE IN (@NEWOFE,-1)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM PROCE_OFE_ADJUN WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE= @PROCE and PROVE=@PROVE AND OFE = -1" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_GR_ADJUN WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE= @PROCE and PROVE=@PROVE AND OFE = -1" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_ITEM_ADJUN WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE= @PROCE and PROVE=@PROVE AND OFE = -1" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_ATRIB WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE= @PROCE and PROVE=@PROVE AND OFE = -1" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_GR_ATRIB WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE= @PROCE and PROVE=@PROVE AND OFE = -1" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_ITEM_ATRIB WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE= @PROCE and PROVE=@PROVE AND OFE = -1" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM ITEM_OFE WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE= @PROCE and PROVE=@PROVE AND NUM = -1" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM GRUPO_OFE WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE= @PROCE and PROVE=@PROVE AND OFE = -1" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM PROCE_OFE WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE= @PROCE and PROVE=@PROVE AND OFE = -1" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR LOCAL FOR" & vbCrLf
sConsulta = sConsulta & "SELECT ADJUN, IDGS FROM P_PROCE_OFE_ADJUN WHERE ID=@ID AND IDGS IS NULL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "OPEN C" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C INTO @ADJUN, @IDGS" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    SELECT @IDADJUN = isnull(MAX(ID),0) + 1 FROM ADJUN " & vbCrLf
sConsulta = sConsulta & "    INSERT INTO ADJUN (ID, WEB) VALUES (@IDADJUN, 1) " & vbCrLf
sConsulta = sConsulta & "    UPDATE P_PROCE_OFE_ADJUN SET IDGS = @IDADJUN WHERE ID = @ID AND ADJUN = @ADJUN" & vbCrLf
sConsulta = sConsulta & "    IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "    FETCH NEXT FROM C INTO @ADJUN, @IDGS" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "CLOSE C" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO PROCE_OFE_ADJUN(ANYO, GMN1, PROCE, PROVE, OFE, ID, NOM, COM, IDPORTAL, DATASIZE)" & vbCrLf
sConsulta = sConsulta & "SELECT @ANYO, @GMN1, @PROCE, @PROVE, @NEWOFE, POA.IDGS, POA.NOM, POA.COM, POA.IDPORTAL, POA.DATASIZE" & vbCrLf
sConsulta = sConsulta & "  FROM P_PROCE_OFE_ADJUN POA" & vbCrLf
sConsulta = sConsulta & "         INNER JOIN ADJUN A" & vbCrLf
sConsulta = sConsulta & "                 ON POA.IDGS = A.ID" & vbCrLf
sConsulta = sConsulta & "WHERE POA.ID = @ID" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR LOCAL FOR" & vbCrLf
sConsulta = sConsulta & "SELECT ADJUN, IDGS, GRUPO FROM P_OFE_GR_ADJUN WHERE ID=@ID AND IDGS IS NULL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "OPEN C" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C INTO @ADJUN, @IDGS, @GRUPO" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    SELECT @IDADJUN = isnull(MAX(ID),0) + 1 FROM ADJUN " & vbCrLf
sConsulta = sConsulta & "    INSERT INTO ADJUN (ID, WEB) VALUES (@IDADJUN, 1) " & vbCrLf
sConsulta = sConsulta & "    UPDATE P_OFE_GR_ADJUN SET IDGS = @IDADJUN WHERE ID = @ID AND ADJUN = @ADJUN AND GRUPO = @GRUPO" & vbCrLf
sConsulta = sConsulta & "    IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "    FETCH NEXT FROM C INTO @ADJUN, @IDGS, @GRUPO" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "CLOSE C" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO OFE_GR_ADJUN(ANYO, GMN1, PROCE, GRUPO, PROVE, OFE, ID, NOM, COM, IDPORTAL, DATASIZE)" & vbCrLf
sConsulta = sConsulta & "SELECT @ANYO, @GMN1, @PROCE, OGA.GRUPO, @PROVE, @NEWOFE, OGA.IDGS, OGA.NOM, OGA.COM, OGA.IDPORTAL, OGA.DATASIZE" & vbCrLf
sConsulta = sConsulta & "  FROM P_OFE_GR_ADJUN OGA" & vbCrLf
sConsulta = sConsulta & "         INNER JOIN ADJUN A" & vbCrLf
sConsulta = sConsulta & "                 ON OGA.IDGS = A.ID" & vbCrLf
sConsulta = sConsulta & " WHERE OGA.ID = @ID" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR LOCAL FOR" & vbCrLf
sConsulta = sConsulta & "SELECT ADJUN, IDGS, ITEM FROM P_OFE_ITEM_ADJUN WHERE ID=@ID AND IDGS IS NULL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "OPEN C" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C INTO @ADJUN, @IDGS, @ITEM" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    SELECT @IDADJUN = isnull(MAX(ID),0) + 1 FROM ADJUN " & vbCrLf
sConsulta = sConsulta & "    INSERT INTO ADJUN (ID, WEB) VALUES (@IDADJUN, 1) " & vbCrLf
sConsulta = sConsulta & "    UPDATE P_OFE_ITEM_ADJUN SET IDGS = @IDADJUN WHERE ID = @ID AND ADJUN = @ADJUN AND ITEM = @ITEM" & vbCrLf
sConsulta = sConsulta & "    IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "    FETCH NEXT FROM C INTO @ADJUN, @IDGS, @ITEM" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "CLOSE C" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO OFE_ITEM_ADJUN(ANYO, GMN1, PROCE, ITEM, PROVE, OFE, ID, NOM, COM, IDPORTAL, DATASIZE)" & vbCrLf
sConsulta = sConsulta & "SELECT @ANYO, @GMN1, @PROCE, OIA.ITEM, @PROVE, @NEWOFE, OIA.IDGS, OIA.NOM, OIA.COM, OIA.IDPORTAL, OIA.DATASIZE" & vbCrLf
sConsulta = sConsulta & "  FROM P_OFE_ITEM_ADJUN OIA" & vbCrLf
sConsulta = sConsulta & "         INNER JOIN ADJUN A" & vbCrLf
sConsulta = sConsulta & "                 ON OIA.IDGS = A.ID" & vbCrLf
sConsulta = sConsulta & "WHERE OIA.ID = @ID" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO OFE_GR_ATRIB(ANYO, GMN1, PROCE, GRUPO, PROVE, OFE, ATRIB_ID, VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL)" & vbCrLf
sConsulta = sConsulta & "SELECT @ANYO, @GMN1, @PROCE, GRUPO, @PROVE, @NEWOFE, ATRIB_ID, VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL" & vbCrLf
sConsulta = sConsulta & "  FROM P_OFE_GR_ATRIB" & vbCrLf
sConsulta = sConsulta & " WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "   AND NOT (VALOR_NUM IS NULL" & vbCrLf
sConsulta = sConsulta & "        AND VALOR_TEXT IS NULL" & vbCrLf
sConsulta = sConsulta & "        AND VALOR_FEC IS NULL" & vbCrLf
sConsulta = sConsulta & "        AND VALOR_BOOL IS NULL)" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO OFE_ITEM_ATRIB(ANYO, GMN1, PROCE, ITEM, PROVE, OFE, ATRIB_ID, VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL)" & vbCrLf
sConsulta = sConsulta & "SELECT @ANYO, @GMN1, @PROCE, ITEM, @PROVE, @NEWOFE, ATRIB_ID, VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL" & vbCrLf
sConsulta = sConsulta & "  FROM P_OFE_ITEM_ATRIB" & vbCrLf
sConsulta = sConsulta & " WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "   AND NOT (VALOR_NUM IS NULL" & vbCrLf
sConsulta = sConsulta & "        AND VALOR_TEXT IS NULL" & vbCrLf
sConsulta = sConsulta & "        AND VALOR_FEC IS NULL" & vbCrLf
sConsulta = sConsulta & "        AND VALOR_BOOL IS NULL)" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO OFE_ATRIB(ANYO, GMN1, PROCE, PROVE, OFE, ATRIB_ID, VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL)" & vbCrLf
sConsulta = sConsulta & "SELECT @ANYO, @GMN1, @PROCE, @PROVE, @NEWOFE, ATRIB_ID, VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL" & vbCrLf
sConsulta = sConsulta & "  FROM P_OFE_ATRIB " & vbCrLf
sConsulta = sConsulta & " WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "   AND NOT (VALOR_NUM IS NULL" & vbCrLf
sConsulta = sConsulta & "        AND VALOR_TEXT IS NULL" & vbCrLf
sConsulta = sConsulta & "        AND VALOR_FEC IS NULL" & vbCrLf
sConsulta = sConsulta & "        AND VALOR_BOOL IS NULL)" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE SET OFE = 1  WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE = @PROCE AND PROVE=@PROVE AND OFE = 0" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @NUMOFEWEB = isnull(MAX(OFEWEB),0)+1 FROM PROCE_OFE_WEB WHERE ANYO = @ANYO AND GMN1 = @GMN1 AND PROCE = @PROCE AND PROVE = @PROVE " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO PROCE_OFE_WEB (ANYO, GMN1, PROCE, PROVE, OFEWEB, OFE, FECREC, FECVAL, MON)" & vbCrLf
sConsulta = sConsulta & "SELECT ANYO, GMN1, PROCE, PROVE, @NUMOFEWEB, OFE, FECREC,FECVAL,MON " & vbCrLf
sConsulta = sConsulta & "FROM PROCE_OFE " & vbCrLf
sConsulta = sConsulta & "WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE = @PROCE AND PROVE = @PROVE AND OFE = @NEWOFE" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO ITEM_OFE_WEB (ANYO, GMN1, PROCE, ITEM, PROVE, NUM, PRECIO, CANTMAX)" & vbCrLf
sConsulta = sConsulta & "SELECT ANYO, GMN1, PROCE, ITEM, PROVE, @NUMOFEWEB, PRECIO, CANTMAX" & vbCrLf
sConsulta = sConsulta & "FROM ITEM_OFE" & vbCrLf
sConsulta = sConsulta & "WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE = @PROCE AND PROVE = @PROVE AND NUM = @NEWOFE" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO PROCE_OFE_WEB_ADJUN (ANYO, GMN1, PROCE, PROVE, OFEWEB, ID, NOM, COM, IDPORTAL)" & vbCrLf
sConsulta = sConsulta & "SELECT ANYO, GMN1, PROCE, PROVE, @NUMOFEWEB, ID, NOM, COM, IDPORTAL" & vbCrLf
sConsulta = sConsulta & "FROM PROCE_OFE_ADJUN" & vbCrLf
sConsulta = sConsulta & "WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE = @PROCE AND PROVE = @PROVE AND OFE = @NEWOFE" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_OFE SET ULT = 0 WHERE ANYO = @ANYO AND GMN1 = @GMN1 AND PROCE =@PROCE AND PROVE=@PROVE AND NUM!=@NEWOFE" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "COMMIT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "GOTO ELIMINAR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ERROR_:" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @ERROR = 100" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ROLLBACK" & vbCrLf
sConsulta = sConsulta & "GOTO ELIMINAR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ERRORSINTRAN_:" & vbCrLf
sConsulta = sConsulta & "SET @ERROR = 200" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ELIMINAR_:" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC NUM_PROCE_PUB @PROVE =@PROVE ,@NUM = @NUM OUTPUT,@NUMNUE = @NUMNUE OUTPUT ,@NUMAREV = @NUMAREV OUTPUT " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM P_PROCE_OFE WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM P_GRUPO_OFE WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM P_ITEM_OFE WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM P_OFE_ATRIB WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM P_OFE_GR_ATRIB WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM P_OFE_ITEM_ATRIB WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM P_PROCE_OFE_ADJUN WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM P_OFE_GR_ADJUN WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM P_OFE_ITEM_ADJUN WHERE ID=@ID" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE UON1_COD (@OLD VARCHAR(50),@NEW VARCHAR(50)) AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT ON" & vbCrLf
sConsulta = sConsulta & "BEGIN TRANSACTION" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR FOR SELECT COD FROM UON1 WHERE COD=@OLD" & vbCrLf
sConsulta = sConsulta & "OPEN C" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C INTO @VAR_COD" & vbCrLf
sConsulta = sConsulta & "IF @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON1_DEP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON2_DEP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3_DEP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PER NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON1_DEST NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON2_DEST NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3_DEST NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISOR NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE UON1 SET COD=@NEW WHERE COD=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON2 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON3 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON1_DEP SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON2_DEP SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON3_DEP SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PER SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE COM SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_UON1 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_UON2 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_UON3 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_UON1 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_UON2 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_UON3 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON1_DEST SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON2_DEST SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON3_DEST SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_UON1 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_UON2 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_UON3 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_UON1 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_UON2 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_UON3 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES1_NIV1 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES1_NIV2 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES1_NIV3 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES1_NIV4 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES2_NIV1 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES2_NIV2 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES2_NIV3 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES2_NIV4 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES3_NIV1 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES3_NIV2 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES3_NIV3 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES3_NIV4 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES4_NIV1 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES4_NIV2 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES4_NIV3 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES4_NIV4 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_1 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_2 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_3 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_4 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_1 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_2 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_3 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_4 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON1 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON2 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON3 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON4 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY1 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY2 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY3 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY4 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_PRESCON3 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_PRESCON4 SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISOR SET UON1=@NEW WHERE UON1=@OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON1_DEP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON2_DEP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3_DEP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PER CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON1_DEST CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON2_DEST CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3_DEST CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISOR CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close C" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "COMMIT TRANSACTION" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT OFF" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE UON2_COD (@OLD VARCHAR(50),@NEW VARCHAR(50), @UON1 VARCHAR(50))  AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT ON" & vbCrLf
sConsulta = sConsulta & "BEGIN TRANSACTION" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR FOR SELECT COD FROM UON2 WHERE UON1=@UON1 AND COD=@OLD" & vbCrLf
sConsulta = sConsulta & "OPEN C" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C INTO @VAR_COD" & vbCrLf
sConsulta = sConsulta & "IF @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON2_DEP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3_DEP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PER NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON2_DEST NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3_DEST NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISOR NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE UON2 SET COD=@NEW WHERE UON1=@UON1 AND COD=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON3 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON2_DEP SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON3_DEP SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PER SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE COM SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_UON2 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_UON3 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_UON2 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_UON3 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON2_DEST SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON3_DEST SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_UON2 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_UON3 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_UON2 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_UON3 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_1 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_2 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_3 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_4 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_1 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_2 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_3 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_4 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON1 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON2 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON3 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON4 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY1 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY2 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY3 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY4 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_PRESCON3 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_PRESCON4 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES1_NIV1 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES1_NIV2 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES1_NIV3 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES1_NIV4 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES2_NIV1 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES2_NIV2 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES2_NIV3 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES2_NIV4 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES3_NIV1 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES3_NIV2 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES3_NIV3 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES3_NIV4 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES4_NIV1 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES4_NIV2 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES4_NIV3 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES4_NIV4 SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISOR SET UON2=@NEW WHERE UON1=@UON1 AND UON2=@OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON2_DEP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3_DEP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PER CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON2_DEST CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3_DEST CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISOR CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close C" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "COMMIT TRANSACTION" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT OFF" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE UON3_COD (@OLD VARCHAR(50),@NEW VARCHAR(50), @UON1 VARCHAR(50), @UON2 VARCHAR(50))  AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT ON" & vbCrLf
sConsulta = sConsulta & "BEGIN TRANSACTION" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR FOR SELECT COD FROM UON3 WHERE UON1=@UON1 AND UON2=@UON2 AND COD=@OLD" & vbCrLf
sConsulta = sConsulta & "OPEN C" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C INTO @VAR_COD" & vbCrLf
sConsulta = sConsulta & "IF @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3_DEP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PER NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3_DEST NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISOR NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE UON3 SET COD=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND COD=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON3_DEP SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PER SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE COM SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_UON3 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_UON3 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE UON3_DEST SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_UON3 SET UON1=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_UON3 SET UON1=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES1_NIV1 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES1_NIV2 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES1_NIV3 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES1_NIV4 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES2_NIV1 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES2_NIV2 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES2_NIV3 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES2_NIV4 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES3_NIV1 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES3_NIV2 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES3_NIV3 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES3_NIV4 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES4_NIV1 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES4_NIV2 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES4_NIV3 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES4_NIV4 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_1 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_2 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_3 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_4 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_1 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_2 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_3 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_4 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON1 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON2 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON3 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON4 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY1 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY2 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY3 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY4 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_PRESCON3 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_PRESCON4 SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISOR SET UON3=@NEW WHERE UON1=@UON1 AND UON2=@UON2 AND UON3=@OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3_DEP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PER CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE UON3_DEST CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES1_NIV4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES2_NIV4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES3_NIV4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES4_NIV4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISOR CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close C" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "COMMIT TRANSACTION" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT OFF" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE USU_COD (@OLD VARCHAR(50),@NEW VARCHAR(50),@PWD VARCHAR(512))  AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT ON" & vbCrLf
sConsulta = sConsulta & "--BEGIN TRANSACTION" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR FOR SELECT COD FROM USU WHERE COD=@OLD" & vbCrLf
sConsulta = sConsulta & "OPEN C" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C INTO @VAR_COD" & vbCrLf
sConsulta = sConsulta & "IF @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU_ACC NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU_LIS NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_SOBRE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTA_INICIAL_PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTA_INICIAL_GRUPO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LOG NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_CONTR NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE SOBRE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PARINS_DEF NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PARINS_DOT NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PARINS_GEST NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PARINS_PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FAVORITOS_ORDEN_ENTREGA NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FAVORITOS_ORDEN_ENTREGA_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FAVORITOS_LINEAS_PEDIDO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FAVORITOS_LINEAS_PEDIDO_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_NUMDEC NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE USU SET COD=@NEW,PWD=@PWD WHERE COD=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE USU_ACC SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE USU_LIS SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET USUVAL=@NEW WHERE USUVAL=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET USUVALSELPROVE=@NEW WHERE USUVALSELPROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET USUVALCIERRE=@NEW WHERE USUVALCIERRE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET USUAPER=@NEW WHERE USUAPER=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_PROCE SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_PROCE_PROVE SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_PROCE_ATRIB SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_PROCE_SOBRE SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_GRUPO SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_GRUPO_PROVE SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_GRUPO_ATRIB SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ITEM SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ITEM_PROVE SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ITEM_ATRIB SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ALL SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ALL_PROVE SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ALL_ATRIB SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTA_INICIAL_PROCE SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTA_INICIAL_GRUPO SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_CONTR SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE SOBRE SET USUAPER=@NEW WHERE USUAPER=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PARINS_DEF SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PARINS_DOT SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PARINS_GEST SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PARINS_PROCE SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE FAVORITOS_ORDEN_ENTREGA SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE FAVORITOS_ORDEN_ENTREGA_ADJUN SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE FAVORITOS_LINEAS_PEDIDO SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE FAVORITOS_LINEAS_PEDIDO_ADJUN SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_NUMDEC SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU_ACC CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU_LIS CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_SOBRE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTA_INICIAL_PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTA_INICIAL_GRUPO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LOG CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_CONTR CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE SOBRE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PARINS_DEF CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PARINS_DOT CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PARINS_GEST CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PARINS_PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FAVORITOS_ORDEN_ENTREGA CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FAVORITOS_ORDEN_ENTREGA_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FAVORITOS_LINEAS_PEDIDO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FAVORITOS_LINEAS_PEDIDO_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_NUMDEC CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close C" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT OFF" & vbCrLf
sConsulta = sConsulta & "--COMMIT TRANSACTION" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_NUEVA_OFERTA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_NUEVA_OFERTA]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE SP_NUEVA_OFERTA @ANYO int, @GMN1 varchar(50), @PROCE int, @PROVE varchar(50), @OFE int, @ULT SMALLINT,@MONNEW VARCHAR(50),@CAMBIONEW FLOAT  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @MAXOFE INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @CAMBIOOLD FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @MONOLD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "--  ESTA SP SOLO SE PUEDE LLAMAR DESDE EL TRIGGER DE INSERCION DE PROCE_OFE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @OFE=-1" & vbCrLf
sConsulta = sConsulta & "    SET @MAXOFE = ( SELECT MAX(OFE) FROM PROCE_OFE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE)" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "    SET @MAXOFE = ( SELECT MAX(OFE) FROM PROCE_OFE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE AND OFE < @OFE)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF (@MAXOFE > 0 ) --NO ES LA PRIMERA OFERTA QUE METE EL PROVEEDOR AS� QUE COPIAMOS LA ANTERIOR" & vbCrLf
sConsulta = sConsulta & "      BEGIN" & vbCrLf
sConsulta = sConsulta & "         --    Leo la moneda de la oferta que voy a copiar" & vbCrLf
sConsulta = sConsulta & "         SELECT @MONOLD=MON,@CAMBIOOLD=CAMBIO FROM PROCE_OFE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE AND OFE =@MAXOFE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "        INSERT INTO PROCE_OFE_ADJUN (ANYO, GMN1, PROCE, PROVE, OFE, ID,NOM, COM, IDPORTAL,DATASIZE)" & vbCrLf
sConsulta = sConsulta & "        SELECT ANYO, GMN1, PROCE, PROVE, @OFE, ID,NOM ,COM, IDPORTAL,DATASIZE" & vbCrLf
sConsulta = sConsulta & "          FROM PROCE_OFE_ADJUN" & vbCrLf
sConsulta = sConsulta & "         WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE = @PROVE AND OFE =@MAXOFE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "        INSERT INTO OFE_ATRIB (ANYO,GMN1,PROCE,PROVE,OFE,ATRIB_ID,VALOR_NUM ,VALOR_TEXT ,VALOR_FEC ,VALOR_BOOL ,VALOR_POND)" & vbCrLf
sConsulta = sConsulta & "        SELECT ANYO,GMN1,PROCE,PROVE,@OFE,ATRIB_ID,VALOR_NUM ,VALOR_TEXT ,VALOR_FEC ,VALOR_BOOL ,VALOR_POND " & vbCrLf
sConsulta = sConsulta & "          FROM OFE_ATRIB " & vbCrLf
sConsulta = sConsulta & "         WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE = @PROVE AND OFE =@MAXOFE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "        INSERT INTO GRUPO_OFE (ANYO,GMN1,PROCE,GRUPO,PROVE,OFE,OBSADJUN)" & vbCrLf
sConsulta = sConsulta & "        SELECT ANYO,GMN1,PROCE,GRUPO,PROVE,@OFE,OBSADJUN" & vbCrLf
sConsulta = sConsulta & "          FROM GRUPO_OFE" & vbCrLf
sConsulta = sConsulta & "         WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE AND OFE=@MAXOFE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "        INSERT INTO OFE_GR_ADJUN (ANYO,GMN1,PROCE,GRUPO,PROVE,OFE,ID,NOM ,COM ,IDPORTAL,DATASIZE)" & vbCrLf
sConsulta = sConsulta & "        SELECT ANYO,GMN1,PROCE,GRUPO,PROVE,@OFE,ID,NOM ,COM ,IDPORTAL,DATASIZE" & vbCrLf
sConsulta = sConsulta & "          FROM OFE_GR_ADJUN" & vbCrLf
sConsulta = sConsulta & "         WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE = @PROVE AND OFE =@MAXOFE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "        INSERT INTO OFE_GR_ATRIB (ANYO,GMN1,PROCE,GRUPO,PROVE,OFE,ATRIB_ID,VALOR_NUM ,VALOR_TEXT ,VALOR_FEC ,VALOR_BOOL ,VALOR_POND )" & vbCrLf
sConsulta = sConsulta & "        SELECT ANYO,GMN1,PROCE,GRUPO,PROVE,@OFE,ATRIB_ID,VALOR_NUM ,VALOR_TEXT ,VALOR_FEC ,VALOR_BOOL ,VALOR_POND " & vbCrLf
sConsulta = sConsulta & "          FROM OFE_GR_ATRIB" & vbCrLf
sConsulta = sConsulta & "         WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE AND OFE=@MAXOFE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "        IF @ULT = 1 " & vbCrLf
sConsulta = sConsulta & "               UPDATE ITEM_OFE SET ULT=0 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE AND NUM=@MAXOFE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--        IF @MONOLD = @MONNEW " & vbCrLf
sConsulta = sConsulta & "            INSERT INTO ITEM_OFE (ANYO,GMN1,PROCE,ITEM,PROVE,NUM,PRECIO,PRECIO2,PRECIO3,USAR,CANTMAX,OBSADJUN,ULT,COMENT1,COMENT2,COMENT3)" & vbCrLf
sConsulta = sConsulta & "            SELECT ANYO,GMN1,PROCE,ITEM,PROVE,@OFE,PRECIO,PRECIO2,PRECIO3,USAR,CANTMAX,OBSADJUN,@ULT,COMENT1,COMENT2,COMENT3" & vbCrLf
sConsulta = sConsulta & "             FROM ITEM_OFE" & vbCrLf
sConsulta = sConsulta & "             WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE AND NUM=@MAXOFE" & vbCrLf
sConsulta = sConsulta & "/*        ELSE" & vbCrLf
sConsulta = sConsulta & "            INSERT INTO ITEM_OFE (ANYO,GMN1,PROCE,ITEM,PROVE,NUM,PRECIO,PRECIO2,PRECIO3,USAR,CANTMAX,OBSADJUN,ULT)" & vbCrLf
sConsulta = sConsulta & "            SELECT ANYO,GMN1,PROCE,ITEM,PROVE,@OFE,((PRECIO/@CAMBIOOLD)*@CAMBIONEW),((PRECIO2/@CAMBIOOLD)*@CAMBIONEW),((PRECIO3/@CAMBIOOLD)*@CAMBIONEW),USAR,CANTMAX,OBSADJUN,@ULT" & vbCrLf
sConsulta = sConsulta & "             FROM ITEM_OFE" & vbCrLf
sConsulta = sConsulta & "             WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE AND NUM=@MAXOFE" & vbCrLf
sConsulta = sConsulta & "*/" & vbCrLf
sConsulta = sConsulta & "        INSERT INTO OFE_ITEM_ADJUN (ANYO,GMN1,PROCE,ITEM,PROVE,OFE,ID,NOM ,COM ,IDPORTAL,DATASIZE)" & vbCrLf
sConsulta = sConsulta & "        SELECT ANYO,GMN1,PROCE,ITEM,PROVE,@OFE,ID,NOM ,COM ,IDPORTAL,DATASIZE" & vbCrLf
sConsulta = sConsulta & "          FROM OFE_ITEM_ADJUN" & vbCrLf
sConsulta = sConsulta & "         WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE AND OFE=@MAXOFE" & vbCrLf
sConsulta = sConsulta & "        " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "        INSERT INTO OFE_ITEM_ATRIB (ANYO,GMN1,PROCE,ITEM,PROVE,OFE,ATRIB_ID,VALOR_NUM ,VALOR_TEXT ,VALOR_FEC ,VALOR_BOOL ,VALOR_POND)" & vbCrLf
sConsulta = sConsulta & "        SELECT ANYO,GMN1,PROCE,ITEM,PROVE,@OFE,ATRIB_ID,VALOR_NUM ,VALOR_TEXT ,VALOR_FEC ,VALOR_BOOL ,VALOR_POND" & vbCrLf
sConsulta = sConsulta & "          FROM OFE_ITEM_ATRIB" & vbCrLf
sConsulta = sConsulta & "         WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE AND OFE=@MAXOFE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "        IF @MONOLD <> @MONNEW " & vbCrLf
sConsulta = sConsulta & "            EXEC SP_MODIF_MON_OFE @ANYO, @GMN1, @PROCE, @PROVE, @OFE, @CAMBIOOLD,@CAMBIONEW" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    ELSE" & vbCrLf
sConsulta = sConsulta & "      BEGIN" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "        INSERT INTO GRUPO_OFE (ANYO,GMN1,PROCE,GRUPO,PROVE,OFE)" & vbCrLf
sConsulta = sConsulta & "        SELECT @ANYO,@GMN1,@PROCE,COD, @PROVE,@OFE" & vbCrLf
sConsulta = sConsulta & "          FROM PROCE_GRUPO" & vbCrLf
sConsulta = sConsulta & "         WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "        INSERT INTO ITEM_OFE (ANYO,GMN1,PROCE,ITEM,PROVE,NUM,PRECIO,PRECIO2,PRECIO3,CANTMAX,ULT,FECACT)" & vbCrLf
sConsulta = sConsulta & "        SELECT @ANYO,@GMN1,@PROCE,ID,@PROVE,@OFE,NULL,NULL,NULL,NULL,@ULT,NULL" & vbCrLf
sConsulta = sConsulta & "          FROM ITEM" & vbCrLf
sConsulta = sConsulta & "         WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "         UPDATE PROCE SET O=(O+1) WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Private Sub V_2_16_Tablas_0()
Dim sConsulta As String

sConsulta = ""
sConsulta = sConsulta & "ALTER TABLE dbo.PROCE_OFE ADD" & vbCrLf
sConsulta = sConsulta & "   PORTAL tinyint NOT NULL CONSTRAINT DF_PROCE_OFE_PORTAL DEFAULT (0)," & vbCrLf
sConsulta = sConsulta & "   USU varchar(50) NULL," & vbCrLf
sConsulta = sConsulta & "   NOMUSU varchar(200) NULL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE dbo.P_PROCE_OFE ADD" & vbCrLf
sConsulta = sConsulta & "   PORTAL tinyint NOT NULL CONSTRAINT DF_P_PROCE_OFE_PORTAL DEFAULT 0," & vbCrLf
sConsulta = sConsulta & "   NOMUSU varchar(200) NULL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "ALTER TABLE dbo.PROCE_PROVE ADD" & vbCrLf
sConsulta = sConsulta & "   AVISADODESPUB tinyint NOT NULL CONSTRAINT DF_PROCE_PROVE_AVISADODESPUB DEFAULT 0" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE dbo.PARGEN_GEST ADD" & vbCrLf
sConsulta = sConsulta & "   AVISODESPUB tinyint NOT NULL CONSTRAINT DF_PARGEN_GEST_AVISODESPUB DEFAULT 0," & vbCrLf
sConsulta = sConsulta & "   AVISOSOLONOOFE tinyint NOT NULL CONSTRAINT DF_PARGEN_GEST_AVISOSOLONOOFE DEFAULT 0," & vbCrLf
sConsulta = sConsulta & "   AVISONOOFERTAN tinyint NOT NULL CONSTRAINT DF_PARGEN_GEST_AVISONOOFERTAN DEFAULT 0," & vbCrLf
sConsulta = sConsulta & "   ANTELACIONAVISO smallint NULL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "ALTER TABLE dbo.PARGEN_DOT ADD" & vbCrLf
sConsulta = sConsulta & "   OFEAVISODESPUB_HTML varchar(255) NULL," & vbCrLf
sConsulta = sConsulta & "   OFEAVISODESPUB_TXT varchar(255) NULL," & vbCrLf
sConsulta = sConsulta & "   OFEAVISODESPUBSUBJECT varchar(255) NULL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "ALTER TABLE dbo.PROCE_DEF ADD" & vbCrLf
sConsulta = sConsulta & "   AVISARDESPUB tinyint NULL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "ALTER TABLE dbo.PROCE ADD" & vbCrLf
sConsulta = sConsulta & "   COMENT_ADJ varchar(4000) NULL," & vbCrLf
sConsulta = sConsulta & "   P int NULL," & vbCrLf
sConsulta = sConsulta & "   W int NULL," & vbCrLf
sConsulta = sConsulta & "   O int NULL," & vbCrLf
sConsulta = sConsulta & "   PRESTOTAL float(53) NULL," & vbCrLf
sConsulta = sConsulta & "   CONSUMIDO float(53) NULL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CONF_VISOR]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)" & vbCrLf
sConsulta = sConsulta & "drop table [dbo].[CONF_VISOR]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE [dbo].[CONF_VISOR] (" & vbCrLf
sConsulta = sConsulta & "   [USU] [varchar] (50) NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [ANYO_VISIBLE] [tinyint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [ANYO_POS] [smallint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [ANYO_WIDTH] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [GMN1_VISIBLE] [tinyint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [GMN1_POS] [smallint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [GMN1_WIDTH] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PROCE_VISIBLE] [tinyint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [PROCE_POS] [smallint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PROCE_WIDTH] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [DESCR_VISIBLE] [tinyint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [DESCR_POS] [smallint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [DESCR_WIDTH] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [EST_VISIBLE] [tinyint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [EST_POS] [smallint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [EST_WIDTH] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [RESP_VISIBLE] [tinyint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [RESP_POS] [smallint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [RESP_WIDTH] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [DIR_VISIBLE] [tinyint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [DIR_POS] [smallint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [DIR_WIDTH] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [O_VISIBLE] [tinyint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [O_POS] [smallint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [O_WIDTH] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [P_VISIBLE] [tinyint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [P_POS] [smallint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [P_WIDTH] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [W_VISIBLE] [tinyint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [W_POS] [smallint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [W_WIDTH] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [FECAPE_VISIBLE] [tinyint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [FECAPE_POS] [smallint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [FECAPE_WIDTH] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [FECLIMOFE_VISIBLE] [tinyint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [FECLIMOFE_POS] [smallint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [FECLIMOFE_WIDTH] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [FECPRES_VISIBLE] [tinyint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [FECPRES_POS] [smallint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [FECPRES_WIDTH] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [FECCIERRE_VISIBLE] [tinyint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [FECCIERRE_POS] [smallint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [FECCIERRE_WIDTH] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [FECNEC_VISIBLE] [tinyint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [FECNEC_POS] [smallint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [FECNEC_WIDTH] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [FECINI_VISIBLE] [tinyint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [FECINI_POS] [smallint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [FECINI_WIDTH] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [FECFIN_VISIBLE] [tinyint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [FECFIN_POS] [smallint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [FECFIN_WIDTH] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PRES_VISIBLE] [tinyint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [PRES_POS] [smallint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PRES_WIDTH] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [CONS_VISIBLE] [tinyint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [CONS_POS] [smallint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [CONS_WIDTH] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [IMP_VISIBLE] [tinyint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [IMP_POS] [smallint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [IMP_WIDTH] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [AHORRO_VISIBLE] [tinyint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [AHORRO_POS] [smallint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [AHORRO_WIDTH] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [MON_VISIBLE] [tinyint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [MON_POS] [smallint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [MON_WIDTH] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [DEST_VISIBLE] [tinyint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [DEST_POS] [smallint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [DEST_WIDTH] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [DESTDEN_VISIBLE] [tinyint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [DESTDEN_POS] [smallint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [DESTDEN_WIDTH] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PAGO_VISIBLE] [tinyint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [PAGO_POS] [smallint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PAGO_WIDTH] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PROVE_VISIBLE] [tinyint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [PROVE_POS] [smallint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PROVE_WIDTH] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PROVEDEN_VISIBLE] [tinyint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [PROVEDEN_POS] [smallint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PROVEDEN_WIDTH] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [OCULTAR_MENU] [tinyint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [OCULTAR_FILTRO] [tinyint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [ANYO_DESDE] [smallint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [MES_DESDE] [int] NULL ," & vbCrLf
sConsulta = sConsulta & "   [GMN1] [varchar] (" & gLongitudesDeCodigos.giLongCodGMN1 & ") NULL ," & vbCrLf
sConsulta = sConsulta & "   [GMN2] [varchar] (" & gLongitudesDeCodigos.giLongCodGMN2 & ") NULL ," & vbCrLf
sConsulta = sConsulta & "   [GMN3] [varchar] (" & gLongitudesDeCodigos.giLongCodGMN3 & ") NULL ," & vbCrLf
sConsulta = sConsulta & "   [GMN4] [varchar] (" & gLongitudesDeCodigos.giLongCodGMN4 & ") NULL ," & vbCrLf
sConsulta = sConsulta & "   [EQP] [varchar] (" & gLongitudesDeCodigos.giLongCodEQP & ") NULL ," & vbCrLf
sConsulta = sConsulta & "   [COM] [varchar] (" & gLongitudesDeCodigos.giLongCodCOM & ") NULL ," & vbCrLf
sConsulta = sConsulta & "   [RESPONSABLE] [tinyint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [UON1] [varchar] (" & gLongitudesDeCodigos.giLongCodUON1 & ") NULL ," & vbCrLf
sConsulta = sConsulta & "   [UON2] [varchar] (" & gLongitudesDeCodigos.giLongCodUON2 & ") NULL ," & vbCrLf
sConsulta = sConsulta & "   [UON3] [varchar] (" & gLongitudesDeCodigos.giLongCodUON3 & ") NULL ," & vbCrLf
sConsulta = sConsulta & "   [PRES1NIV1] [int] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PRES1NIV2] [int] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PRES1NIV3] [int] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PRES1NIV4] [int] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PRES2NIV1] [int] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PRES2NIV2] [int] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PRES2NIV3] [int] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PRES2NIV4] [int] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PRES3NIV1] [int] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PRES3NIV2] [int] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PRES3NIV3] [int] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PRES3NIV4] [int] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PRES4NIV1] [int] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PRES4NIV2] [int] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PRES4NIV3] [int] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PRES4NIV4] [int] NULL " & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[CONF_VISOR] WITH NOCHECK ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [PK_CONF_VISOR] PRIMARY KEY  CLUSTERED " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [USU]" & vbCrLf
sConsulta = sConsulta & "   )  ON [PRIMARY] " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[CONF_VISOR] WITH NOCHECK ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_CONF_VISOR_ANYO_VISIBLE] DEFAULT (1) FOR [ANYO_VISIBLE]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_CONF_VISOR_GMN1_VISIBLE] DEFAULT (1) FOR [GMN1_VISIBLE]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_CONF_VISOR_PROCE_VISIBLE] DEFAULT (1) FOR [PROCE_VISIBLE]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_CONF_VISOR_DESCR_VISIBLE] DEFAULT (1) FOR [DESCR_VISIBLE]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_CONF_VISOR_EST_VISIBLE] DEFAULT (1) FOR [EST_VISIBLE]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_CONF_VISOR_RESP_VISIBLE] DEFAULT (1) FOR [RESP_VISIBLE]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_CONF_VISOR_DIR_VISIBLE] DEFAULT (1) FOR [DIR_VISIBLE]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_CONF_VISOR_O_VISIBLE] DEFAULT (0) FOR [O_VISIBLE]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_CONF_VISOR_P_VISIBLE] DEFAULT (0) FOR [P_VISIBLE]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_CONF_VISOR_W_VISIBLE] DEFAULT (0) FOR [W_VISIBLE]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_CONF_VISOR_FECAPE_VISIBLE] DEFAULT (0) FOR [FECAPE_VISIBLE]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_CONF_VISOR_FECLIMOFE_VISIBLE] DEFAULT (0) FOR [FECLIMOFE_VISIBLE]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_CONF_VISOR_FECPRES_VISIBLE] DEFAULT (1) FOR [FECPRES_VISIBLE]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_CONF_VISOR_FECCIERRE_VISIBLE] DEFAULT (0) FOR [FECCIERRE_VISIBLE]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_CONF_VISOR_FECNEC_VISIBLE] DEFAULT (0) FOR [FECNEC_VISIBLE]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_CONF_VISOR_FECINI_VISIBLE] DEFAULT (0) FOR [FECINI_VISIBLE]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_CONF_VISOR_FECFIN_VISIBLE] DEFAULT (0) FOR [FECFIN_VISIBLE]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_CONF_VISOR_PRES_VISIBLE] DEFAULT (0) FOR [PRES_VISIBLE]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_CONF_VISOR_CONS_VISIBLE] DEFAULT (0) FOR [CONS_VISIBLE]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_CONF_VISOR_IMP_VISIBLE] DEFAULT (0) FOR [IMP_VISIBLE]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_CONF_VISOR_AHORRO_VISIBLE] DEFAULT (0) FOR [AHORRO_VISIBLE]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_CONF_VISOR_MON_VISIBLE] DEFAULT (0) FOR [MON_VISIBLE]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_CONF_VISOR_DEST_VISIBLE] DEFAULT (0) FOR [DEST_VISIBLE]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_CONF_VISOR_DESTDEN_VISIBLE] DEFAULT (0) FOR [DESTDEN_VISIBLE]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_CONF_VISOR_PAGO_VISIBLE] DEFAULT (0) FOR [PAGO_VISIBLE]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_CONF_VISOR_PROVE_VISIBLE] DEFAULT (0) FOR [PROVE_VISIBLE]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_CONF_VISOR_PROVEDEN_VISIBLE] DEFAULT (0) FOR [PROVEDEN_VISIBLE]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_CONF_VISOR_OCULTAR_MENU] DEFAULT (0) FOR [OCULTAR_MENU]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_CONF_VISOR_OCULTAR_FILTRO] DEFAULT (0) FOR [OCULTAR_FILTRO]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_CONF_VISOR_RESPONSABLE] DEFAULT (0) FOR [RESPONSABLE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "ALTER TABLE dbo.PARINS_GEST ADD" & vbCrLf
sConsulta = sConsulta & "   ORDENVISOR_ASC tinyint NOT NULL CONSTRAINT DF_PARINS_GEST_ORDENVISOR_ASC DEFAULT 1" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "ALTER TABLE dbo.PARGEN_INTERNO ADD" & vbCrLf
sConsulta = sConsulta & "   READER varchar(255) NULL," & vbCrLf
sConsulta = sConsulta & "   HELP_PDF varchar(255) NULL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Private Sub V_2_16_Triggers_0()
Dim sConsulta As String

sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PROCE_PROVE_TG_DEL]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PROCE_PROVE_TG_DEL]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PROCE_PROVE_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PROCE_PROVE_TG_INS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER PROCE_PROVE_TG_DEL ON dbo.PROCE_PROVE" & vbCrLf
sConsulta = sConsulta & "FOR DELETE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN1 AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCE AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVE AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_PROCE_PROVE_Del CURSOR FOR SELECT ANYO,GMN1,PROCE,PROVE FROM DELETED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_PROCE_PROVE_Del" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_PROVE_Del INTO @ANYO,@GMN1,@PROCE,@PROVE" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "  " & vbCrLf
sConsulta = sConsulta & "/*Borramos los registros de ese proveedor para las tablas de configuraci�n de la comparativa */" & vbCrLf
sConsulta = sConsulta & "DELETE FROM CONF_VISTAS_PROCE_PROVE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "DELETE FROM CONF_VISTAS_GRUPO_PROVE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "DELETE FROM CONF_VISTAS_ITEM_PROVE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "DELETE FROM CONF_VISTAS_ALL_PROVE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET P=(P-1) WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "/* Si se han insertado proveedores, actualizamos el estado del proceso  si es que este era inferior a */" & vbCrLf
sConsulta = sConsulta & "IF (SELECT EST FROM PROCE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE) =4 /* 4=Con proveedores asignados */" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF (SELECT COUNT(*) FROM PROCE_PROVE WHERE ANYO=@ANYO  AND GMN1=@GMN1 AND PROCE=@PROCE) = 0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET EST = 3  WHERE ANYO= @ANYO  AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_PROVE_Del INTO @ANYO,@GMN1,@PROCE,@PROVE" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_PROCE_PROVE_Del" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_PROCE_PROVE_Del" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER PROCE_PROVE_TG_INS ON dbo.PROCE_PROVE" & vbCrLf
sConsulta = sConsulta & "FOR INSERT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN1 AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCE AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVE AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @COUNT AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_PROCE_PROVE_Ins CURSOR FOR SELECT ANYO,GMN1,PROCE,PROVE FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_PROCE_PROVE_Ins" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_PROVE_Ins INTO @ANYO,@GMN1,@PROCE,@PROVE" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "/*Inserta los registros correspondientes en las tablas de configuraci�n de la comparativa*/" & vbCrLf
sConsulta = sConsulta & "SELECT @COUNT =COUNT(*) FROM CONF_VISTAS_PROCE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "IF @COUNT>0" & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO CONF_VISTAS_PROCE_PROVE (ANYO,GMN1,PROCE,PROVE,USU,VISTA, VISIBLE)" & vbCrLf
sConsulta = sConsulta & "             (SELECT ANYO,GMN1,PROCE,@PROVE,USU,VISTA,1 FROM CONF_VISTAS_PROCE WHERE ANYO=@ANYO" & vbCrLf
sConsulta = sConsulta & "   AND GMN1=@GMN1 AND PROCE=@PROCE )" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "SELECT @COUNT =COUNT(*) FROM CONF_VISTAS_ALL WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "IF @COUNT>0" & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO CONF_VISTAS_ALL_PROVE (ANYO,GMN1,PROCE,PROVE,USU,VISTA, VISIBLE)" & vbCrLf
sConsulta = sConsulta & "             (SELECT ANYO,GMN1,PROCE,@PROVE,USU,VISTA,1 FROM CONF_VISTAS_ALL WHERE ANYO=@ANYO" & vbCrLf
sConsulta = sConsulta & "   AND GMN1=@GMN1 AND PROCE=@PROCE )" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "SELECT @COUNT =COUNT(*) FROM CONF_VISTAS_GRUPO WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "IF @COUNT>0" & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO CONF_VISTAS_GRUPO_PROVE (ANYO,GMN1,PROCE,GRUPO,PROVE,USU,VISTA, VISIBLE)" & vbCrLf
sConsulta = sConsulta & "             (SELECT ANYO,GMN1,PROCE,GRUPO,@PROVE,USU,VISTA,1 FROM CONF_VISTAS_GRUPO WHERE ANYO=@ANYO" & vbCrLf
sConsulta = sConsulta & "   AND GMN1=@GMN1 AND PROCE=@PROCE )" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "SELECT @COUNT =COUNT(*) FROM CONF_VISTAS_ITEM WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "IF @COUNT>0" & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO CONF_VISTAS_ITEM_PROVE (ANYO,GMN1,PROCE,GRUPO,PROVE,USU,VISTA, VISIBLE)" & vbCrLf
sConsulta = sConsulta & "             (SELECT ANYO,GMN1,PROCE,GRUPO,@PROVE,USU,VISTA,1 FROM CONF_VISTAS_ITEM WHERE ANYO=@ANYO" & vbCrLf
sConsulta = sConsulta & "   AND GMN1=@GMN1 AND PROCE=@PROCE )" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET P= (P+1)  WHERE ANYO= @ANYO  AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "/* Si se han insertado proveedores, actualizamos el estado del proceso  si es que este era inferior a */" & vbCrLf
sConsulta = sConsulta & "IF (SELECT PROCE.EST FROM PROCE WHERE ANYO=@ANYO  AND GMN1=@GMN1 AND COD=@PROCE) < 4  /* 3= Validado */" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET EST = 4  WHERE ANYO= @ANYO  AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_PROVE_Ins INTO @ANYO,@GMN1,@PROCE,@PROVE" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_PROCE_PROVE_Ins" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_PROCE_PROVE_Ins" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = ""
sConsulta = sConsulta & ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PROCE_OFE_TG_DEL]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PROCE_OFE_TG_DEL]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER PROCE_OFE_TG_DEL ON dbo.PROCE_OFE " & vbCrLf
sConsulta = sConsulta & "FOR DELETE " & vbCrLf
sConsulta = sConsulta & "AS " & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO AS INTEGER " & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN1 AS VARCHAR(50) " & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCE AS INTEGER " & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVE AS VARCHAR(50) " & vbCrLf
sConsulta = sConsulta & "DECLARE @MAXOFE AS INT " & vbCrLf
sConsulta = sConsulta & "DECLARE @ITEM AS INTEGER " & vbCrLf
sConsulta = sConsulta & "DECLARE @PRECIO AS FLOAT " & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVE2 AS VARCHAR(50) " & vbCrLf
sConsulta = sConsulta & "DECLARE @OFE2 AS SMALLINT " & vbCrLf
sConsulta = sConsulta & "DECLARE @IDENT AS INTEGER " & vbCrLf
sConsulta = sConsulta & "DECLARE @CAMBIOMONCEN FLOAT " & vbCrLf
sConsulta = sConsulta & "DECLARE @NUMOFE AS INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_PROCE_OFE_Del CURSOR FOR SELECT ANYO,GMN1,PROCE,PROVE FROM DELETED " & vbCrLf
sConsulta = sConsulta & "OPEN curTG_PROCE_OFE_Del " & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_OFE_Del INTO @ANYO,@GMN1,@PROCE,@PROVE " & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0 " & vbCrLf
sConsulta = sConsulta & "BEGIN " & vbCrLf
sConsulta = sConsulta & " IF (SELECT SUBASTA FROM PROCE_DEF WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE) = 1 " & vbCrLf
sConsulta = sConsulta & " BEGIN " & vbCrLf
sConsulta = sConsulta & "     --Si existen precios en la oferta eliminada que eran m�nimas en la puja se deber�n recalcular los nuevos precios m�nimos " & vbCrLf
sConsulta = sConsulta & "         /*Obtiene todos los items para los cuales ese proveedor tiene la oferta m�nima*/ " & vbCrLf
sConsulta = sConsulta & "         DECLARE C CURSOR FOR SELECT ITEM FROM OFEMIN WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "         OPEN C " & vbCrLf
sConsulta = sConsulta & "         FETCH NEXT FROM C INTO @ITEM " & vbCrLf
sConsulta = sConsulta & "         WHILE @@FETCH_STATUS=0 " & vbCrLf
sConsulta = sConsulta & "         BEGIN " & vbCrLf
sConsulta = sConsulta & "             SELECT TOP 1 @PRECIO= A.PRECIO / (B.CAMBIO * C.CAMBIO), @PROVE2=A.PROVE,@OFE2=A.NUM" & vbCrLf
sConsulta = sConsulta & "                   FROM ITEM_OFE A INNER JOIN PROCE_OFE B ON A.ANYO=B.ANYO AND A.GMN1=B.GMN1 AND A.PROCE=B.PROCE AND A.PROVE=B.PROVE AND A.NUM=B.OFE" & vbCrLf
sConsulta = sConsulta & "                  INNER JOIN PROCE C ON A.ANYO=C.ANYO AND A.GMN1=C.GMN1 AND A.PROCE = C.COD" & vbCrLf
sConsulta = sConsulta & "             WHERE A.ANYO=@ANYO AND A.GMN1=@GMN1 AND A.PROCE=@PROCE AND A.ITEM=@ITEM AND A.ULT=1 AND A.PRECIO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "             ORDER BY A.PRECIO / (B.CAMBIO * C.CAMBIO) ASC" & vbCrLf
sConsulta = sConsulta & "             IF @PRECIO IS NULL " & vbCrLf
sConsulta = sConsulta & "             BEGIN " & vbCrLf
sConsulta = sConsulta & "                 UPDATE OFEMIN SET PRECIO=NULL, PROVE=NULL, OFE=NULL WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE = @PROCE AND ITEM = @ITEM " & vbCrLf
sConsulta = sConsulta & "             End " & vbCrLf
sConsulta = sConsulta & "             Else " & vbCrLf
sConsulta = sConsulta & "             BEGIN " & vbCrLf
sConsulta = sConsulta & "                 /*Actualiza la tabla OFEMIN con la oferta m�nima para el proceso e item*/ " & vbCrLf
sConsulta = sConsulta & "                 UPDATE OFEMIN SET PRECIO=@PRECIO,PROVE=@PROVE2,OFE=@OFE2, ULTOFE=0 WHERE ANYO=@ANYO  AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM " & vbCrLf
sConsulta = sConsulta & "             End " & vbCrLf
sConsulta = sConsulta & "             FETCH NEXT FROM C INTO @ITEM " & vbCrLf
sConsulta = sConsulta & "         End " & vbCrLf
sConsulta = sConsulta & "         Close C " & vbCrLf
sConsulta = sConsulta & "         DEALLOCATE C " & vbCrLf
sConsulta = sConsulta & "     EXEC SP_CALCULAR_DETALLE_PUJAS @ANYO, @GMN1, @PROCE" & vbCrLf
sConsulta = sConsulta & " END" & vbCrLf
sConsulta = sConsulta & "     SET @NUMOFE=(SELECT MAX(OFE) FROM PROCE_OFE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE AND ENVIADA=1)" & vbCrLf
sConsulta = sConsulta & "     IF (@NUMOFE IS NULL ) " & vbCrLf
sConsulta = sConsulta & "         BEGIN " & vbCrLf
sConsulta = sConsulta & "   UPDATE PROCE SET O=(O-1) WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "         END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "     SET @MAXOFE = ( SELECT MAX(OFE) FROM PROCE_OFE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ENVIADA=1 AND OFE <> -1 ) " & vbCrLf
sConsulta = sConsulta & "     IF (@MAXOFE IS NULL ) " & vbCrLf
sConsulta = sConsulta & "     BEGIN " & vbCrLf
sConsulta = sConsulta & "     /* Si se han insertado ofertas, actualizamos el estado del proceso  si es que este era inferior a 7 */ " & vbCrLf
sConsulta = sConsulta & "         IF (SELECT PROCE.EST FROM PROCE WHERE ANYO=@ANYO  AND GMN1=@GMN1 AND COD=@PROCE) = 7  /* 7= Con ofertas */ " & vbCrLf
sConsulta = sConsulta & "         BEGIN " & vbCrLf
sConsulta = sConsulta & "             UPDATE PROCE SET EST = 6  WHERE ANYO= @ANYO  AND GMN1=@GMN1 AND COD=@PROCE " & vbCrLf
sConsulta = sConsulta & "         End " & vbCrLf
sConsulta = sConsulta & "     End " & vbCrLf
sConsulta = sConsulta & " FETCH NEXT FROM curTG_PROCE_OFE_Del INTO @ANYO,@GMN1,@PROCE,@PROVE " & vbCrLf
sConsulta = sConsulta & "End " & vbCrLf
sConsulta = sConsulta & "Close curTG_PROCE_OFE_Del " & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_PROCE_OFE_Del " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PROCE_OFE_WEB_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PROCE_OFE_WEB_TG_INS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER PROCE_OFE_WEB_TG_INS ON PROCE_OFE_WEB" & vbCrLf
sConsulta = sConsulta & "FOR INSERT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVE varchar(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO AS INTEGER " & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN1 AS VARCHAR(50) " & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCE AS INTEGER " & vbCrLf
sConsulta = sConsulta & "DECLARE @OFEWEB INTEGER" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE curPROCE_OFE_WEB_TG_INS CURSOR FOR SELECT ANYO,GMN1,PROCE,PROVE,OFEWEB FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curPROCE_OFE_WEB_TG_INS" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curPROCE_OFE_WEB_TG_INS INTO @ANYO,@GMN1,@PROCE,@PROVE,@OFEWEB" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "update prove set fsp_usado = 1 where COD=@PROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @OFEWEB=1" & vbCrLf
sConsulta = sConsulta & "    UPDATE PROCE SET W=(W+1) WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curPROCE_OFE_WEB_TG_INS INTO @ANYO,@GMN1,@PROCE,@PROVE,@OFEWEB" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curPROCE_OFE_WEB_TG_INS" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curPROCE_OFE_WEB_TG_INS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ITEM_TG_DEL]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[ITEM_TG_DEL]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ITEM_TG_UPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[ITEM_TG_UPD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ITEM_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[ITEM_TG_INS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER ITEM_TG_DEL ON dbo.ITEM" & vbCrLf
sConsulta = sConsulta & "FOR DELETE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN1 AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCE AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @FECINI AS DATETIME" & vbCrLf
sConsulta = sConsulta & "DECLARE @FECMIN AS DATETIME" & vbCrLf
sConsulta = sConsulta & "DECLARE @FECNECMANUAL  AS TINYINT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_ITEM_Del CURSOR FOR SELECT ANYO,GMN1,PROCE,ID,FECINI FROM DELETED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_ITEM_Del" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_ITEM_Del INTO @ANYO,@GMN1,@PROCE,@ID,@FECINI" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET  @FECNECMANUAL=(SELECT PROCE.FECNECMANUAL FROM PROCE  WHERE ANYO=@ANYO  AND GMN1=@GMN1 AND COD=@PROCE)" & vbCrLf
sConsulta = sConsulta & "IF @FECNECMANUAL=0" & vbCrLf
sConsulta = sConsulta & "  UPDATE PROCE SET FECNEC =(SELECT MIN(FECINI) FROM ITEM WHERE  ANYO=@ANYO  AND GMN1=@GMN1 AND PROCE=@PROCE) WHERE COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET PRESTOTAL=(SELECT SUM(PRES) FROM ITEM WHERE ANYO= @ANYO  AND GMN1=@GMN1 AND PROCE=@PROCE AND CONF=1 GROUP BY ANYO,GMN1,PROCE )   WHERE ANYO= @ANYO  AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "/* Si se han eliminado todos los  items, actualizamos el estado del proceso  si es que este era inferior a 3*/" & vbCrLf
sConsulta = sConsulta & "IF (SELECT PROCE.EST FROM PROCE WHERE ANYO=@ANYO  AND GMN1=@GMN1 AND COD=@PROCE) = 2  /* 3= Validado */" & vbCrLf
sConsulta = sConsulta & " BEGIN" & vbCrLf
sConsulta = sConsulta & "  IF (SELECT COUNT(*) FROM ITEM WHERE ANYO=@ANYO  AND GMN1=@GMN1 AND PROCE=@PROCE) = 0  /* 3= Validado */" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "    UPDATE PROCE SET EST =1  WHERE ANYO= @ANYO  AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "   End" & vbCrLf
sConsulta = sConsulta & " End" & vbCrLf
sConsulta = sConsulta & " IF (SELECT SUBASTA FROM PROCE_DEF WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE) = 1 " & vbCrLf
sConsulta = sConsulta & " BEGIN " & vbCrLf
sConsulta = sConsulta & "   DELETE DETALLE_PUJAS  " & vbCrLf
sConsulta = sConsulta & "     FROM DETALLE_PUJAS A " & vbCrLf
sConsulta = sConsulta & "     INNER JOIN DELETED B" & vbCrLf
sConsulta = sConsulta & "             ON A.ANYO=B.ANYO " & vbCrLf
sConsulta = sConsulta & "            AND A.GMN1= B.GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND A.PROCE=B.PROCE " & vbCrLf
sConsulta = sConsulta & "            AND A.ITEM=B.ID" & vbCrLf
sConsulta = sConsulta & "    WHERE A.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "      AND A.GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "      AND A.PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "   IF (SELECT COUNT(*) FROM (SELECT DISTINCT PROVE " & vbCrLf
sConsulta = sConsulta & "                               FROM OFEMIN " & vbCrLf
sConsulta = sConsulta & "                              WHERE ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "                                AND GMN1=@GMN1" & vbCrLf
sConsulta = sConsulta & "                                AND PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "                                AND PROVE IS NOT NULL) A)>1 " & vbCrLf
sConsulta = sConsulta & "     UPDATE PROCE_PROVE" & vbCrLf
sConsulta = sConsulta & "        SET SUPERADA = 1" & vbCrLf
sConsulta = sConsulta & "      WHERE ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "        AND GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "        AND PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "  ELSE" & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "      DECLARE @PROVE VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "      SELECT TOP 1 @PROVE = PROVE " & vbCrLf
sConsulta = sConsulta & "        FROM OFEMIN" & vbCrLf
sConsulta = sConsulta & "       WHERE ANYO=@ANYO" & vbCrLf
sConsulta = sConsulta & "         AND GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "         AND PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "      UPDATE PROCE_PROVE" & vbCrLf
sConsulta = sConsulta & "         SET SUPERADA = CASE WHEN A.PROVE=@PROVE THEN 0 ELSE 1 END" & vbCrLf
sConsulta = sConsulta & "        FROM PROCE_PROVE A" & vbCrLf
sConsulta = sConsulta & "       WHERE A.ANYO=@ANYO" & vbCrLf
sConsulta = sConsulta & "         AND A.GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "         AND A.PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "    END" & vbCrLf
sConsulta = sConsulta & "end" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_ITEM_Del INTO @ANYO,@GMN1,@PROCE,@ID,@FECINI" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_ITEM_Del" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_ITEM_Del" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER ITEM_TG_INS ON dbo.ITEM" & vbCrLf
sConsulta = sConsulta & "FOR INSERT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN1 AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCE AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN2 AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN3 AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN4 AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @ART AS VARCHAR(20)" & vbCrLf
sConsulta = sConsulta & "DECLARE @CANT AS FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @FECINI AS DATETIME" & vbCrLf
sConsulta = sConsulta & "DECLARE @ESTADOPROCESO AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @FECNECMANUAL  AS TINYINT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_ITEM_Ins CURSOR FOR SELECT ANYO,GMN1,PROCE,ID,GMN2,GMN3,GMN4,ART,CANT,FECINI FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_ITEM_Ins" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_ITEM_Ins INTO @ANYO,@GMN1,@PROCE,@ID,@GMN2,@GMN3,@GMN4,@ART,@CANT,@FECINI" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SELECT @ESTADOPROCESO=PROCE.EST, @FECNECMANUAL=PROCE.FECNECMANUAL FROM PROCE  WHERE ANYO=@ANYO  AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "/* Si se han insertado items, actualizamos el estado del proceso  si es que este era inferior a 2*/" & vbCrLf
sConsulta = sConsulta & "IF @ESTADOPROCESO < 2  /* 3= Validado */" & vbCrLf
sConsulta = sConsulta & "  UPDATE PROCE SET EST = 2  WHERE ANYO= @ANYO  AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "Else" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "  IF @ESTADOPROCESO >=7" & vbCrLf
sConsulta = sConsulta & "      INSERT INTO ITEM_OFE (ANYO,GMN1,PROCE,ITEM,PROVE,NUM,ULT)  " & vbCrLf
sConsulta = sConsulta & "      SELECT @ANYO,@GMN1,@PROCE,@ID,PROVE,OFE,ULT From PROCE_OFE" & vbCrLf
sConsulta = sConsulta & "      WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @FECNECMANUAL=0" & vbCrLf
sConsulta = sConsulta & "     UPDATE PROCE SET FECNEC =(SELECT MIN(FECINI) FROM ITEM WHERE ANYO= @ANYO  AND GMN1=@GMN1 AND PROCE=@PROCE) WHERE COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET PRESTOTAL=(SELECT SUM(PRES) FROM ITEM WHERE ANYO= @ANYO  AND GMN1=@GMN1 AND PROCE=@PROCE AND CONF=1 GROUP BY ANYO,GMN1,PROCE )   WHERE ANYO= @ANYO  AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "_FETCH:" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_ITEM_Ins INTO @ANYO,@GMN1,@PROCE,@ID,@GMN2,@GMN3,@GMN4,@ART,@CANT,@FECINI" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_ITEM_Ins" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_ITEM_Ins" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER ITEM_TG_UPD ON dbo.ITEM" & vbCrLf
sConsulta = sConsulta & "FOR UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN1 AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCE AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN2 AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN3 AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN4 AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @ART AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @CANT AS FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @FECINI AS DATETIME" & vbCrLf
sConsulta = sConsulta & "DECLARE @FECFIN AS DATETIME" & vbCrLf
sConsulta = sConsulta & "DECLARE @EST AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @OBJ AS FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @OBJOLD AS FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @FECNECMANUAL  AS TINYINT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_ITEM_Upd CURSOR FOR SELECT ANYO,GMN1,PROCE,ID,GMN2,GMN3,GMN4,ART,CANT,FECINI,FECFIN,OBJ FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_ITEM_Upd" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_ITEM_Upd INTO @ANYO,@GMN1,@PROCE,@ID,@GMN2,@GMN3,@GMN4,@ART,@CANT,@FECINI,@FECFIN,@OBJ" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET  @FECNECMANUAL=(SELECT PROCE.FECNECMANUAL FROM PROCE  WHERE ANYO=@ANYO  AND GMN1=@GMN1 AND COD=@PROCE)" & vbCrLf
sConsulta = sConsulta & "IF @FECNECMANUAL=0" & vbCrLf
sConsulta = sConsulta & "  UPDATE PROCE SET FECNEC =(SELECT MIN(FECINI) FROM ITEM WHERE ANYO=@ANYO  AND GMN1=@GMN1 AND PROCE=@PROCE)  WHERE COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF UPDATE(PRES) OR UPDATE(CONF)" & vbCrLf
sConsulta = sConsulta & "  UPDATE PROCE SET PRESTOTAL=(SELECT SUM(PRES) FROM ITEM WHERE ANYO= @ANYO  AND GMN1=@GMN1 AND PROCE=@PROCE AND CONF=1 GROUP BY ANYO,GMN1,PROCE )   WHERE ANYO= @ANYO  AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF UPDATE(OBJ)" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @EST = (SELECT EST FROM PROCE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE)" & vbCrLf
sConsulta = sConsulta & "SET @OBJOLD = (SELECT OBJ FROM DELETED WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ID=@ID)" & vbCrLf
sConsulta = sConsulta & "IF @OBJ IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "/*Si estamos en estado 8 y borramos el �ltimo objetivo entonces pasamos a estado 7*/" & vbCrLf
sConsulta = sConsulta & "IF @EST = 8 AND ((SELECT COUNT(*) FROM ITEM WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND OBJ IS NOT NULL )=0) UPDATE PROCE SET EST=7 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "IF @EST = 9  AND ((SELECT COUNT(*) FROM ITEM WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND OBJ IS NOT NULL )=0) UPDATE PROCE SET EST=10 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "IF @OBJOLD IS NOT NULL AND ((SELECT COUNT(*) FROM ITEM WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND OBJ IS NOT NULL )=0) UPDATE PROCE_PROVE SET NUMOBJ=0,OBJNUEVOS=0 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Else" & vbCrLf
sConsulta = sConsulta & "IF @OBJOLD IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @EST = 7 UPDATE PROCE SET EST=8 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "IF @EST = 10 UPDATE PROCE SET EST=9 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE SET OBJNUEVOS=1,NUMOBJ=NUMOBJ+1 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Else" & vbCrLf
sConsulta = sConsulta & "IF @OBJOLD <> @OBJ" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @EST = 7 UPDATE PROCE SET EST=8 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "IF @EST = 10 UPDATE PROCE SET EST=9 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE SET OBJNUEVOS=1,NUMOBJ=NUMOBJ+1 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "_FETCH:" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_ITEM_Upd INTO @ANYO,@GMN1,@PROCE,@ID,@GMN2,@GMN3,@GMN4,@ART,@CANT,@FECINI,@FECFIN,@OBJ" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_ITEM_Upd" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_ITEM_Upd" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub


Private Sub V_2_16_Datos_0()

Dim sConsulta As String


sConsulta = ""
sConsulta = sConsulta & "UPDATE PROCE SET PRESTOTAL =  I.PRES" & vbCrLf
sConsulta = sConsulta & "  FROM (SELECT ANYO, GMN1,PROCE, SUM(PRES) PRES FROM ITEM WHERE CONF=1 GROUP BY ANYO,GMN1,PROCE ) I" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PROCE P"
sConsulta = sConsulta & "               ON P.ANYO = I.ANYO" & vbCrLf
sConsulta = sConsulta & "              AND P.GMN1 = I.GMN1" & vbCrLf
sConsulta = sConsulta & "              AND P.COD = I.PROCE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET P = PROVE" & vbCrLf
sConsulta = sConsulta & "  FROM (SELECT ANYO,GMN1,PROCE, COUNT(PROVE) PROVE FROM PROCE_PROVE GROUP BY ANYO, GMN1, PROCE) PP" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PROCE P " & vbCrLf
sConsulta = sConsulta & "               ON P.ANYO = PP.ANYO" & vbCrLf
sConsulta = sConsulta & "              AND P.GMN1 = PP.GMN1" & vbCrLf
sConsulta = sConsulta & "              AND P.COD = PP.PROCE" & vbCrLf
sConsulta = sConsulta & " WHERE P.EST >= 3" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET O = PROVE" & vbCrLf
sConsulta = sConsulta & "  FROM (SELECT ANYO, GMN1, PROCE, COUNT(PROVE) PROVE FROM (SELECT ANYO, GMN1, PROCE, PROVE FROM PROCE_OFE GROUP BY ANYO, GMN1, PROCE, PROVE)  PO GROUP BY ANYO, GMN1, PROCE ) PO" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PROCE P" & vbCrLf
sConsulta = sConsulta & "               ON P.ANYO = PO.ANYO" & vbCrLf
sConsulta = sConsulta & "              AND P.GMN1 = PO.GMN1" & vbCrLf
sConsulta = sConsulta & "              AND P.COD = PO.PROCE" & vbCrLf
sConsulta = sConsulta & "WHERE P.EST >=5" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET W = PROVE" & vbCrLf
sConsulta = sConsulta & "  FROM (SELECT ANYO, GMN1, PROCE, COUNT(PROVE) PROVE FROM (SELECT PO.ANYO, PO.GMN1, PO.PROCE, PO.PROVE FROM PROCE_OFE PO INNER JOIN PROCE_OFE_WEB POW ON PO.ANYO = POW.ANYO AND PO.GMN1=POW.GMN1 AND PO.PROCE = POW.PROCE AND PO.PROVE = POW.PROVE AND PO.OFE = POW.OFE GROUP BY PO.ANYO, PO.GMN1, PO.PROCE, PO.PROVE) PO GROUP BY ANYO, GMN1, PROCE ) PO" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PROCE P"
sConsulta = sConsulta & "    ON P.ANYO = PO.ANYO" & vbCrLf
sConsulta = sConsulta & "   AND P.GMN1 = PO.GMN1" & vbCrLf
sConsulta = sConsulta & "   AND P.COD = PO.PROCE" & vbCrLf
sConsulta = sConsulta & "WHERE P.EST >=5" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET W = 0 WHERE EST>=5 AND W IS NULL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "UPDATE PROCE SET O = 0 WHERE EST>=5 AND O IS NULL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "INSERT INTO ACC (ID, ORDEN) VALUES (12042,12042)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "INSERT INTO ACC (ID, ORDEN) VALUES (12043,12043)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "INSERT INTO ACC (ID, ORDEN) VALUES (12044,12044)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "INSERT INTO ACC (ID, ORDEN) VALUES (12045,12045)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "INSERT INTO ACC (ID, ORDEN) VALUES (12046,12046)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "INSERT INTO ACC (ID, ORDEN) VALUES (12047,12047)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "INSERT INTO ACC (ID, ORDEN) VALUES (12048,12048)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "INSERT INTO ACC (ID, ORDEN) VALUES (12119,12119)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "INSERT INTO ACC (ID, ORDEN) VALUES (12120,12120)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "INSERT INTO ACC (ID, ORDEN) VALUES (12121,12121)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "INSERT INTO ACC (ID, ORDEN) VALUES (12122,12122)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "INSERT INTO ACC (ID, ORDEN) VALUES (12123,12123)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "INSERT INTO ACC (ID, ORDEN) VALUES (12124,12124)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "INSERT INTO ACC (ID, ORDEN) VALUES (12125,12125)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "INSERT INTO ACC (ID, ORDEN) VALUES (12126,12126)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "INSERT INTO ACC (ID, ORDEN) VALUES (12127,12127)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "INSERT INTO ACC (ID, ORDEN) VALUES (12514,12514)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "INSERT INTO ACC (ID, ORDEN) VALUES (12515,12515)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "INSERT INTO ACC (ID, ORDEN) VALUES (12516,12516)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "INSERT INTO ACC (ID, ORDEN) VALUES (12517,12517)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "INSERT INTO ACC (ID, ORDEN) VALUES (12616,12616)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "INSERT INTO ACC (ID, ORDEN) VALUES (12617,12617)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "INSERT INTO ACC (ID, ORDEN) VALUES (12618,12618)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "INSERT INTO ACC (ID, ORDEN) VALUES (12619,12619)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "INSERT INTO ACC (ID, ORDEN) VALUES (12620,12620)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "INSERT INTO ACC (ID, ORDEN) VALUES (12711,12722)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "INSERT INTO ACC (ID, ORDEN) VALUES (12712,12723)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "INSERT INTO ACC (ID, ORDEN) VALUES (12713,12724)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "INSERT INTO ACC (ID, ORDEN) VALUES (12714,12725)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "INSERT INTO ACC (ID, ORDEN) VALUES (12715,12726)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "INSERT INTO ACC (ID, ORDEN) VALUES (12716,12710)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "INSERT INTO ACC (ID, ORDEN) VALUES (12717,12706)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "INSERT INTO ACC (ID, ORDEN) VALUES (12718,12707)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "INSERT INTO ACC (ID, ORDEN) VALUES (12719,12708)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "INSERT INTO ACC (ID, ORDEN) VALUES (12720,12709)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "INSERT INTO ACC (ID, ORDEN) VALUES (12721,12711)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "INSERT INTO ACC (ID, ORDEN) VALUES (12722,12712)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "INSERT INTO ACC (ID, ORDEN) VALUES (12723,12713)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "INSERT INTO ACC (ID, ORDEN) VALUES (12724,12714)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "INSERT INTO ACC (ID, ORDEN) VALUES (12725,12701)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "INSERT INTO ACC (ID, ORDEN) VALUES (12726,12702)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "INSERT INTO ACC (ID, ORDEN) VALUES (13010,13010)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "INSERT INTO ACC (ID, ORDEN) VALUES (13011,13011)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "INSERT INTO ACC (ID, ORDEN) VALUES (13012,13012)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "INSERT INTO ACC (ID, ORDEN) VALUES (13013,13013)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "INSERT INTO ACC (ID, ORDEN) VALUES (13014,13014)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "INSERT INTO ACC (ID, ORDEN) VALUES (13015,13015)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "INSERT INTO ACC (ID, ORDEN) VALUES (13016,13016)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "INSERT INTO ACC (ID, ORDEN) VALUES (13017,13017)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "INSERT INTO ACC (ID, ORDEN) VALUES (13018,13018)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "INSERT INTO ACC (ID, ORDEN) VALUES (15114,15114)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "INSERT INTO ACC (ID, ORDEN) VALUES (15115,15115)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "INSERT INTO ACC (ID, ORDEN) VALUES (15116,15116)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "INSERT INTO ACC (ID, ORDEN) VALUES (15117,15117)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "INSERT INTO ACC (ID, ORDEN) VALUES (15118,15118)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 
sConsulta = ""
sConsulta = sConsulta & ""
sConsulta = sConsulta & "INSERT INTO USU_ACC (USU,ACC) " & vbCrLf
sConsulta = sConsulta & "SELECT USU,12726 FROM USU_ACC WHERE ACC =12707" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "INSERT INTO USU_ACC (USU,ACC) " & vbCrLf
sConsulta = sConsulta & "SELECT USU,12725 FROM USU_ACC WHERE ACC =12708" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "INSERT INTO USU_ACC (USU,ACC) " & vbCrLf
sConsulta = sConsulta & "SELECT USU,12716 FROM USU_ACC WHERE ACC =12701 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "INSERT INTO USU_ACC (USU,ACC) " & vbCrLf
sConsulta = sConsulta & "SELECT USU,12717 FROM USU_ACC WHERE ACC =12701 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "INSERT INTO USU_ACC (USU,ACC) " & vbCrLf
sConsulta = sConsulta & "SELECT USU,12719 FROM USU_ACC WHERE ACC =12701 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "INSERT INTO USU_ACC (USU,ACC) " & vbCrLf
sConsulta = sConsulta & "SELECT USU,12721 FROM USU_ACC WHERE ACC =12701 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "INSERT INTO USU_ACC (USU,ACC) " & vbCrLf
sConsulta = sConsulta & "SELECT USU,12723 FROM USU_ACC WHERE ACC =12701 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & ""
sConsulta = sConsulta & "INSERT INTO PERF_ACC (PERF,ACC) " & vbCrLf
sConsulta = sConsulta & "SELECT PERF,12726 FROM PERF_ACC WHERE ACC =12707" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "INSERT INTO PERF_ACC (PERF,ACC) " & vbCrLf
sConsulta = sConsulta & "SELECT PERF,12725 FROM PERF_ACC WHERE ACC =12708" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "INSERT INTO PERF_ACC (PERF,ACC) " & vbCrLf
sConsulta = sConsulta & "SELECT PERF,12716 FROM PERF_ACC WHERE ACC =12701 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "INSERT INTO PERF_ACC (PERF,ACC) " & vbCrLf
sConsulta = sConsulta & "SELECT PERF,12717 FROM PERF_ACC WHERE ACC =12701 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "INSERT INTO PERF_ACC (PERF,ACC) " & vbCrLf
sConsulta = sConsulta & "SELECT PERF,12719 FROM PERF_ACC WHERE ACC =12701 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "INSERT INTO PERF_ACC (PERF,ACC) " & vbCrLf
sConsulta = sConsulta & "SELECT PERF,12721 FROM PERF_ACC WHERE ACC =12701 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "INSERT INTO PERF_ACC (PERF,ACC) " & vbCrLf
sConsulta = sConsulta & "SELECT PERF,12723 FROM PERF_ACC WHERE ACC =12701 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & ""
sConsulta = sConsulta & "ALTER TABLE FAVORITOS_ORDEN_ENTREGA CHECK CONSTRAINT ALL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "ALTER TABLE FAVORITOS_ORDEN_ENTREGA_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "ALTER TABLE FAVORITOS_LINEAS_PEDIDO CHECK CONSTRAINT ALL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "ALTER TABLE FAVORITOS_LINEAS_PEDIDO_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Private Sub V_2_16_Datos_1()
Dim sConsulta As String

sConsulta = ""
sConsulta = sConsulta & "UPDATE PROCE SET CONSUMIDO = C.CONSUMIDO" & vbCrLf
sConsulta = sConsulta & "  FROM " & vbCrLf
sConsulta = sConsulta & "(SELECT ITEM_ADJ.ANYO,ITEM_ADJ.GMN1,ITEM_ADJ.PROCE,SUM(ITEM.PREC*(ITEM.CANT*(PORCEN/100))) AS CONSUMIDO" & vbCrLf
sConsulta = sConsulta & "FROM ITEM_ADJ  " & vbCrLf
sConsulta = sConsulta & "     INNER JOIN PROCE_OFE " & vbCrLf
sConsulta = sConsulta & "             ON ITEM_ADJ.ANYO=PROCE_OFE.ANYO " & vbCrLf
sConsulta = sConsulta & "            AND ITEM_ADJ.GMN1=PROCE_OFE.GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND ITEM_ADJ.PROCE=PROCE_OFE.PROCE" & vbCrLf
sConsulta = sConsulta & "            AND ITEM_ADJ.PROVE=PROCE_OFE.PROVE " & vbCrLf
sConsulta = sConsulta & "            AND ITEM_ADJ.OFE=PROCE_OFE.OFE " & vbCrLf
sConsulta = sConsulta & "            AND ULT=1" & vbCrLf
sConsulta = sConsulta & "     INNER JOIN PROCE P" & vbCrLf
sConsulta = sConsulta & "             ON P.ANYO = PROCE_OFE.ANYO" & vbCrLf
sConsulta = sConsulta & "            AND P.GMN1 = PROCE_OFE.GMN1" & vbCrLf
sConsulta = sConsulta & "            AND P.COD = PROCE_OFE.PROCE" & vbCrLf
sConsulta = sConsulta & "     INNER JOIN ITEM " & vbCrLf
sConsulta = sConsulta & "             ON ITEM_ADJ.ANYO=ITEM.ANYO " & vbCrLf
sConsulta = sConsulta & "            AND ITEM_ADJ.GMN1=ITEM.GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND ITEM_ADJ.PROCE=ITEM.PROCE " & vbCrLf
sConsulta = sConsulta & "            AND ITEM_ADJ.ITEM=ITEM.ID" & vbCrLf
sConsulta = sConsulta & " WHERE P.EST>8" & vbCrLf
sConsulta = sConsulta & "GROUP BY ITEM_ADJ.ANYO,ITEM_ADJ.GMN1,ITEM_ADJ.PROCE) C" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PROCE P ON P.ANYO = C.ANYO" & vbCrLf
sConsulta = sConsulta & "                  AND P.GMN1 = C.GMN1" & vbCrLf
sConsulta = sConsulta & "                  AND P.COD = C.PROCE" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN1 VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCE INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @GRUPO VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVE VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @OFE INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ITEM INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @VALOR FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @OP varchar(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PARAM NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @GR VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "CREATE TABLE #TEMP (ANYO INT, GMN1 VARCHAR(50), PROCE INT, ITEM INT, PROVE VARCHAR(50), OFE INT, GRUPO VARCHAR(50), ADJUDICADO FLOAT)" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "INSERT INTO #TEMP SELECT ITEM_ADJ.ANYO,ITEM_ADJ.GMN1,ITEM_ADJ.PROCE,ITEM_ADJ.ITEM,ITEM_ADJ.PROVE,ITEM_ADJ.OFE,ITEM.GRUPO,SUM(ITEM_OFE.PREC_VALIDO*(ITEM.CANT*(PORCEN/100))) AS ADJUDICADO" & vbCrLf
sConsulta = sConsulta & "  FROM ITEM_ADJ" & vbCrLf
sConsulta = sConsulta & "         INNER JOIN PROCE_OFE" & vbCrLf
sConsulta = sConsulta & "                 ON ITEM_ADJ.ANYO=PROCE_OFE.ANYO" & vbCrLf
sConsulta = sConsulta & "                AND ITEM_ADJ.GMN1=PROCE_OFE.GMN1" & vbCrLf
sConsulta = sConsulta & "                AND ITEM_ADJ.PROCE=PROCE_OFE.PROCE" & vbCrLf
sConsulta = sConsulta & "                AND ITEM_ADJ.PROVE=PROCE_OFE.PROVE" & vbCrLf
sConsulta = sConsulta & "                AND ITEM_ADJ.OFE=PROCE_OFE.OFE" & vbCrLf
sConsulta = sConsulta & "                AND ULT=1 and item_adj.porcen>0" & vbCrLf
sConsulta = sConsulta & "         INNER JOIN ITEM" & vbCrLf
sConsulta = sConsulta & "                 ON ITEM_ADJ.ANYO=ITEM.ANYO" & vbCrLf
sConsulta = sConsulta & "                AND ITEM_ADJ.GMN1=ITEM.GMN1" & vbCrLf
sConsulta = sConsulta & "                AND ITEM_ADJ.PROCE=ITEM.PROCE" & vbCrLf
sConsulta = sConsulta & "                AND ITEM_ADJ.ITEM=ITEM.ID" & vbCrLf
sConsulta = sConsulta & "         INNER JOIN ITEM_OFE" & vbCrLf
sConsulta = sConsulta & "                 ON ITEM_ADJ.ANYO=ITEM_OFE.ANYO" & vbCrLf
sConsulta = sConsulta & "                AND ITEM_ADJ.GMN1=ITEM_OFE.GMN1" & vbCrLf
sConsulta = sConsulta & "                AND ITEM_ADJ.PROCE=ITEM_OFE.PROCE" & vbCrLf
sConsulta = sConsulta & "                AND ITEM_ADJ.ITEM=ITEM_OFE.ITEM" & vbCrLf
sConsulta = sConsulta & "                AND ITEM_ADJ.PROVE=ITEM_OFE.PROVE" & vbCrLf
sConsulta = sConsulta & "                AND ITEM_ADJ.OFE=ITEM_OFE.NUM" & vbCrLf
sConsulta = sConsulta & "         INNER JOIN PROCE P" & vbCrLf
sConsulta = sConsulta & "                 ON ITEM_ADJ.ANYO = P.ANYO" & vbCrLf
sConsulta = sConsulta & "                AND ITEM_ADJ.GMN1 = P.GMN1" & vbCrLf
sConsulta = sConsulta & "                AND ITEM_ADJ.PROCE = P.COD" & vbCrLf
sConsulta = sConsulta & "                AND P.EST >8" & vbCrLf
sConsulta = sConsulta & "--                AND P.ADJUDICADO IS NULL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "GROUP BY ITEM_ADJ.ANYO,ITEM_ADJ.GMN1,ITEM_ADJ.PROCE,ITEM_ADJ.ITEM,ITEM_ADJ.PROVE,ITEM_ADJ.OFE,ITEM.GRUPO" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "DECLARE CUR_TEMP CURSOR LOCAL FORWARD_ONLY FOR" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT ANYO, GMN1, PROCE, PROVE, OFE FROM #TEMP" & vbCrLf
sConsulta = sConsulta & "OPEN CUR_TEMP" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM CUR_TEMP INTO @ANYO, @GMN1, @PROCE, @PROVE, @OFE" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE CUR_ITEMS_OFE CURSOR LOCAL FORWARD_ONLY  FOR" & vbCrLf
sConsulta = sConsulta & "SELECT ID ITEM, OP_PREC, VALOR_NUM FROM" & vbCrLf
sConsulta = sConsulta & "  (SELECT IO.ID, PA.OP_PREC, OA.VALOR_NUM, PA.ORDEN" & vbCrLf
sConsulta = sConsulta & "     FROM ITEM IO" & vbCrLf
sConsulta = sConsulta & "          CROSS JOIN OFE_ATRIB OA" & vbCrLf
sConsulta = sConsulta & "          INNER JOIN PROCE_ATRIB PA" & vbCrLf
sConsulta = sConsulta & "                  ON OA.ANYO = PA.ANYO" & vbCrLf
sConsulta = sConsulta & "                 AND OA.GMN1 = PA.GMN1" & vbCrLf
sConsulta = sConsulta & "                 AND OA.PROCE = PA.PROCE" & vbCrLf
sConsulta = sConsulta & "                 AND OA.ATRIB_ID = PA.ID" & vbCrLf
sConsulta = sConsulta & "                 AND OA.PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "                 AND OA.OFE = @OFE" & vbCrLf
sConsulta = sConsulta & " INNER JOIN ITEM_ADJ IA ON IO.ANYO=IA.ANYO AND IO.GMN1=IA.GMN1 AND IO.PROCE=IA.PROCE AND IO.ID=IA.ITEM AND OA.PROVE=IA.PROVE AND OA.OFE=IA.OFE AND IA.PORCEN>0" & vbCrLf
sConsulta = sConsulta & "   WHERE IO.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "     AND IO.GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "     AND IO.PROCE =@PROCE" & vbCrLf
sConsulta = sConsulta & "     AND OA.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "     AND OA.GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "     AND OA.PROCE =@PROCE" & vbCrLf
sConsulta = sConsulta & "     AND OA.PROVE =@PROVE" & vbCrLf
sConsulta = sConsulta & "     AND OA.OFE = @OFE" & vbCrLf
sConsulta = sConsulta & "     AND PA.APLIC_PREC = 2" & vbCrLf
sConsulta = sConsulta & "     AND PA.USAR_PREC = 1" & vbCrLf
sConsulta = sConsulta & "   UNION" & vbCrLf
sConsulta = sConsulta & "SELECT  IO.ID, PA.OP_PREC, OGA.VALOR_NUM, PA.ORDEN" & vbCrLf
sConsulta = sConsulta & "  FROM ITEM IO" & vbCrLf
sConsulta = sConsulta & "        CROSS JOIN OFE_GR_ATRIB OGA" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN PROCE_ATRIB PA" & vbCrLf
sConsulta = sConsulta & "                ON OGA.ANYO = PA.ANYO" & vbCrLf
sConsulta = sConsulta & "               AND OGA.GMN1 = PA.GMN1" & vbCrLf
sConsulta = sConsulta & "               AND OGA.PROCE = PA.PROCE" & vbCrLf
sConsulta = sConsulta & "               AND OGA.ATRIB_ID = PA.ID" & vbCrLf
sConsulta = sConsulta & "               AND OGA.PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "               AND OGA.OFE = @OFE" & vbCrLf
sConsulta = sConsulta & " INNER JOIN ITEM_ADJ IA ON IO.ANYO=IA.ANYO AND IO.GMN1=IA.GMN1 AND IO.PROCE=IA.PROCE AND IO.ID=IA.ITEM AND OGA.PROVE=IA.PROVE AND OGA.OFE=IA.OFE AND IA.PORCEN>0" & vbCrLf
sConsulta = sConsulta & " WHERE IO.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "   AND IO.GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "   AND IO.PROCE =@PROCE" & vbCrLf
sConsulta = sConsulta & "   AND IO.GRUPO = OGA.GRUPO" & vbCrLf
sConsulta = sConsulta & "   AND OGA.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "   AND OGA.GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "   AND OGA.PROCE =@PROCE" & vbCrLf
sConsulta = sConsulta & "   AND OGA.PROVE =@PROVE" & vbCrLf
sConsulta = sConsulta & "   AND OGA.OFE = @OFE" & vbCrLf
sConsulta = sConsulta & "   AND PA.APLIC_PREC = 2" & vbCrLf
sConsulta = sConsulta & "   AND PA.USAR_PREC = 1" & vbCrLf
sConsulta = sConsulta & "UNION" & vbCrLf
sConsulta = sConsulta & "    SELECT  IO.ITEM, PA.OP_PREC , OIA.VALOR_NUM , PA.ORDEN" & vbCrLf
sConsulta = sConsulta & "      FROM ITEM_OFE IO" & vbCrLf
sConsulta = sConsulta & " INNER JOIN ITEM_ADJ IA ON IO.ANYO=IA.ANYO AND IO.GMN1=IA.GMN1 AND IO.PROCE=IA.PROCE AND IA.ITEM=IO.ITEM AND IO.PROVE=IA.PROVE AND IO.NUM=IA.OFE AND IA.PORCEN>0" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN OFE_ITEM_ATRIB OIA" & vbCrLf
sConsulta = sConsulta & "                ON IO.ANYO = OIA.ANYO" & vbCrLf
sConsulta = sConsulta & "               AND IO.GMN1 = OIA.GMN1" & vbCrLf
sConsulta = sConsulta & "               AND IO.PROCE = OIA.PROCE" & vbCrLf
sConsulta = sConsulta & "               AND IO.ITEM = OIA.ITEM" & vbCrLf
sConsulta = sConsulta & "               AND IO.PROVE = OIA.PROVE" & vbCrLf
sConsulta = sConsulta & "               AND IO.NUM = OIA.OFE" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN PROCE_ATRIB PA" & vbCrLf
sConsulta = sConsulta & "                ON OIA.ANYO = PA.ANYO" & vbCrLf
sConsulta = sConsulta & "               AND OIA.GMN1 = PA.GMN1" & vbCrLf
sConsulta = sConsulta & "               AND OIA.PROCE = PA.PROCE" & vbCrLf
sConsulta = sConsulta & "               AND OIA.ATRIB_ID = PA.ID" & vbCrLf
sConsulta = sConsulta & "               AND OIA.PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "               AND OIA.OFE = @OFE" & vbCrLf
sConsulta = sConsulta & "     WHERE OIA.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "       AND OIA.GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "       AND OIA.PROCE =@PROCE" & vbCrLf
sConsulta = sConsulta & "       AND OIA.PROVE =@PROVE" & vbCrLf
sConsulta = sConsulta & "       AND OIA.OFE = @OFE" & vbCrLf
sConsulta = sConsulta & "       AND PA.AMBITO = 3" & vbCrLf
sConsulta = sConsulta & "       AND PA.APLIC_PREC = 2" & vbCrLf
sConsulta = sConsulta & "       AND PA.USAR_PREC = 1" & vbCrLf
sConsulta = sConsulta & ") PORITEM" & vbCrLf
sConsulta = sConsulta & "ORDER BY ORDEN" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "OPEN CUR_ITEMS_OFE" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM CUR_ITEMS_OFE INTO @ITEM, @OP, @VALOR" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & " SET  @SQL = N' UPDATE #TEMP" & vbCrLf
sConsulta = sConsulta & "          SET ADJUDICADO=ADJUDICADO ' +" & vbCrLf
sConsulta = sConsulta & "        CASE WHEN @OP IN ('*','/') THEN @OP + ' isnull(@VALOR,1) '" & vbCrLf
sConsulta = sConsulta & "        WHEN @OP IN ('+','-') THEN @OP + ' isnull(@VALOR,0) '" & vbCrLf
sConsulta = sConsulta & "        WHEN @OP='+%' THEN '+  ADJUDICADO * isnull(@VALOR,0)/100 '" & vbCrLf
sConsulta = sConsulta & "        WHEN @OP='-%' THEN '- ADJUDICADO * isnull(@VALOR,0)/100 '" & vbCrLf
sConsulta = sConsulta & "        END  + '" & vbCrLf
sConsulta = sConsulta & "         FROM #TEMP" & vbCrLf
sConsulta = sConsulta & "        WHERE ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "          AND GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "          AND PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "          AND ITEM = @ITEM" & vbCrLf
sConsulta = sConsulta & "          AND PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "          AND OFE = @OFE'" & vbCrLf
sConsulta = sConsulta & "    SET @PARAM ='@ANYO INT, @GMN1 VARCHAR(50), @PROCE INT, @ITEM INT, @PROVE VARCHAR(50), @OFE INT, @VALOR FLOAT'" & vbCrLf
sConsulta = sConsulta & "    EXEC SP_EXECUTESQL @SQL, @PARAM , @ANYO=@ANYO, @GMN1=@GMN1, @PROCE = @PROCE, @ITEM = @ITEM, @PROVE = @PROVE, @OFE =@OFE, @VALOR =@VALOR" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "    FETCH NEXT FROM CUR_ITEMS_OFE INTO @ITEM, @OP, @VALOR" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "CLOSE CUR_ITEMS_OFE" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE CUR_ITEMS_OFE" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM CUR_TEMP INTO @ANYO, @GMN1, @PROCE, @PROVE, @OFE " & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "CLOSE CUR_TEMP" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE CUR_TEMP" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "CREATE TABLE #TMP_GR (ANYO INT, GMN1 VARCHAR(50), PROCE INT, GRUPO VARCHAR(50), PROVE VARCHAR(50), OFE INT, ADJUDICADO FLOAT)" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "INSERT INTO #TMP_GR" & vbCrLf
sConsulta = sConsulta & "SELECT ANYO,GMN1,PROCE,GRUPO,PROVE, OFE, SUM(ADJUDICADO) FROM #TEMP GROUP BY ANYO,GMN1,PROCE,GRUPO,PROVE, OFE" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE CUR_TEMP CURSOR LOCAL FORWARD_ONLY FOR" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT ANYO, GMN1, PROCE, PROVE, OFE FROM #TMP_GR" & vbCrLf
sConsulta = sConsulta & "OPEN CUR_TEMP" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM CUR_TEMP INTO @ANYO, @GMN1, @PROCE, @PROVE, @OFE" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "DECLARE CUR_GR_OFE CURSOR LOCAL FORWARD_ONLY  FOR" & vbCrLf
sConsulta = sConsulta & "SELECT GRUPO, OP_PREC, VALOR_NUM FROM" & vbCrLf
sConsulta = sConsulta & "  (" & vbCrLf
sConsulta = sConsulta & "SELECT  IO.COD GRUPO, PA.OP_PREC, OGA.VALOR_NUM, PA.ORDEN" & vbCrLf
sConsulta = sConsulta & "  FROM PROCE_GRUPO IO" & vbCrLf
sConsulta = sConsulta & "        CROSS JOIN OFE_GR_ATRIB OGA" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN PROCE_ATRIB PA" & vbCrLf
sConsulta = sConsulta & "                ON OGA.ANYO = PA.ANYO" & vbCrLf
sConsulta = sConsulta & "               AND OGA.GMN1 = PA.GMN1" & vbCrLf
sConsulta = sConsulta & "               AND OGA.PROCE = PA.PROCE" & vbCrLf
sConsulta = sConsulta & "               AND OGA.ATRIB_ID = PA.ID" & vbCrLf
sConsulta = sConsulta & "               AND OGA.PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "               AND OGA.OFE = @OFE" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN USAR_GR_ATRIB UGA" & vbCrLf
sConsulta = sConsulta & "                ON UGA.ANYO = PA.ANYO" & vbCrLf
sConsulta = sConsulta & "               AND UGA.GMN1 = PA.GMN1" & vbCrLf
sConsulta = sConsulta & "               AND UGA.PROCE = PA.PROCE" & vbCrLf
sConsulta = sConsulta & "               AND UGA.GRUPO = IO.COD" & vbCrLf
sConsulta = sConsulta & "               AND UGA.ATRIB = PA.ID" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & " WHERE IO.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "   AND IO.GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "   AND IO.PROCE =@PROCE" & vbCrLf
sConsulta = sConsulta & "   AND IO.COD = OGA.GRUPO" & vbCrLf
sConsulta = sConsulta & "   AND OGA.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "   AND OGA.GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "   AND OGA.PROCE =@PROCE" & vbCrLf
sConsulta = sConsulta & "   AND OGA.PROVE =@PROVE" & vbCrLf
sConsulta = sConsulta & "   AND OGA.OFE = @OFE" & vbCrLf
sConsulta = sConsulta & "   AND PA.APLIC_PREC = 1" & vbCrLf
sConsulta = sConsulta & "   AND UGA.USAR_PREC = 1" & vbCrLf
sConsulta = sConsulta & ") PORITEM" & vbCrLf
sConsulta = sConsulta & "ORDER BY ORDEN" & vbCrLf
sConsulta = sConsulta & "OPEN CUR_GR_OFE" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM CUR_GR_OFE INTO @GR, @OP, @VALOR" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'UPDATE #TMP_GR" & vbCrLf
sConsulta = sConsulta & "          SET ADJUDICADO=ADJUDICADO ' +" & vbCrLf
sConsulta = sConsulta & "        CASE WHEN @OP IN ('*','/') THEN @OP + ' isnull(@VALOR,1) '" & vbCrLf
sConsulta = sConsulta & "        WHEN @OP IN ('+','-') THEN @OP + ' isnull(@VALOR,0) '" & vbCrLf
sConsulta = sConsulta & "        WHEN @OP='+%' THEN '+  ADJUDICADO * isnull(@VALOR,0)/100 '" & vbCrLf
sConsulta = sConsulta & "        WHEN @OP='-%' THEN '- ADJUDICADO * isnull(@VALOR,0)/100 '" & vbCrLf
sConsulta = sConsulta & "        END  + '" & vbCrLf
sConsulta = sConsulta & "         FROM #TMP_GR" & vbCrLf
sConsulta = sConsulta & "        WHERE ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "          AND GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "          AND PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "          AND GRUPO= @GR" & vbCrLf
sConsulta = sConsulta & "          AND PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "          AND OFE = @OFE'" & vbCrLf
sConsulta = sConsulta & "    SET @PARAM ='@ANYO INT, @GMN1 VARCHAR(50), @PROCE INT, @GR VARCHAR(100), @PROVE VARCHAR(50), @OFE INT, @VALOR FLOAT'" & vbCrLf
sConsulta = sConsulta & "    EXEC SP_EXECUTESQL @SQL, @PARAM , @ANYO=@ANYO, @GMN1=@GMN1, @PROCE = @PROCE, @GR = @GR, @PROVE = @PROVE, @OFE =@OFE, @VALOR =@VALOR" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & " FETCH NEXT FROM CUR_GR_OFE INTO @GR, @OP, @VALOR" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "CLOSE CUR_GR_OFE" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE CUR_GR_OFE" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM CUR_TEMP INTO @ANYO, @GMN1, @PROCE, @PROVE, @OFE END" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "CLOSE CUR_TEMP" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE CUR_TEMP" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
If gCollation = "" Then
    sConsulta = sConsulta & "CREATE TABLE #TMP_PROCE (ANYO INT, GMN1 VARCHAR(50) NULL , PROCE INT, ADJUDICADO" & vbCrLf
Else
    sConsulta = sConsulta & "CREATE TABLE #TMP_PROCE (ANYO INT, GMN1 VARCHAR(50) COLLATE  " & gCollation & "  NULL , PROCE INT, ADJUDICADO" & vbCrLf
End If

sConsulta = sConsulta & "FLOAT)" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "INSERT INTO #TMP_PROCE(ANYO , GMN1 , PROCE , ADJUDICADO ) SELECT ANYO, GMN1, PROCE , SUM(ADJUDICADO) FROM #TMP_GR GROUP BY ANYO, GMN1, PROCE" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE CUR_TEMP CURSOR LOCAL FORWARD_ONLY FOR" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT ANYO, GMN1, PROCE FROM #TMP_GR" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "OPEN CUR_TEMP" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM CUR_TEMP INTO @ANYO, @GMN1, @PROCE" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE CUR_OFE CURSOR LOCAL FORWARD_ONLY  FOR" & vbCrLf
sConsulta = sConsulta & "SELECT  OP_PREC, VALOR_NUM FROM OFE_ATRIB OA" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PROCE_OFE PO" & vbCrLf
sConsulta = sConsulta & "         ON PO.ANYO = OA.ANYO" & vbCrLf
sConsulta = sConsulta & "        AND PO.GMN1 = OA.GMN1" & vbCrLf
sConsulta = sConsulta & "        AND PO.PROCE = OA.PROCE" & vbCrLf
sConsulta = sConsulta & "        AND PO.PROVE = OA.PROVE" & vbCrLf
sConsulta = sConsulta & "        AND PO.OFE = OA.OFE" & vbCrLf
sConsulta = sConsulta & "        AND PO.ULT = 1" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PROCE_ATRIB PA  ON OA.ANYO = PA.ANYO  AND OA.GMN1 = PA.GMN1 AND OA.PROCE = PA.PROCE AND OA.ATRIB_ID = PA.ID  WHERE PO.ANYO = @ANYO AND PO.GMN1 = @GMN1  AND PO.PROCE =@PROCE  AND OA.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & " AND OA.GMN1 = @GMN1   AND OA.PROCE =@PROCE" & vbCrLf
sConsulta = sConsulta & "  AND PA.APLIC_PREC = 0  AND PA.USAR_PREC = 1" & vbCrLf
sConsulta = sConsulta & "ORDER BY ORDEN" & vbCrLf
sConsulta = sConsulta & "OPEN CUR_OFE" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM CUR_OFE INTO  @OP, @VALOR" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'UPDATE #TMP_PROCE  SET ADJUDICADO=ADJUDICADO ' +" & vbCrLf
sConsulta = sConsulta & "        CASE WHEN @OP IN ('*','/') THEN @OP + ' isnull(@VALOR,1) '" & vbCrLf
sConsulta = sConsulta & "        WHEN @OP IN ('+','-') THEN @OP + ' isnull(@VALOR,0) '" & vbCrLf
sConsulta = sConsulta & "        WHEN @OP='+%' THEN '+  ADJUDICADO * isnull(@VALOR,0)/100 '" & vbCrLf
sConsulta = sConsulta & "        WHEN @OP='-%' THEN '- ADJUDICADO * isnull(@VALOR,0)/100 '" & vbCrLf
sConsulta = sConsulta & "        END  + '" & vbCrLf
sConsulta = sConsulta & "         FROM #TMP_PROCE" & vbCrLf
sConsulta = sConsulta & "        WHERE ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "          AND GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "          AND PROCE = @PROCE'" & vbCrLf
sConsulta = sConsulta & "    SET @PARAM ='@ANYO INT, @GMN1 VARCHAR(50), @PROCE INT,  @VALOR FLOAT'" & vbCrLf
sConsulta = sConsulta & "    EXEC SP_EXECUTESQL @SQL, @PARAM , @ANYO=@ANYO, @GMN1=@GMN1, @PROCE =" & vbCrLf
sConsulta = sConsulta & "@PROCE,   @VALOR =@VALOR" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & " FETCH NEXT FROM CUR_OFE INTO  @OP, @VALOR" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "CLOSE CUR_OFE" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE CUR_OFE" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM CUR_TEMP INTO @ANYO, @GMN1, @PROCE" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "CLOSE CUR_TEMP" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE CUR_TEMP" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET ADJUDICADO = T.ADJUDICADO" & vbCrLf
sConsulta = sConsulta & "  FROM #TMP_PROCE T" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN PROCE P" & vbCrLf
sConsulta = sConsulta & "                ON T.ANYO = P.ANYO" & vbCrLf
sConsulta = sConsulta & "               AND T.GMN1 = P.GMN1" & vbCrLf
sConsulta = sConsulta & "               AND T.PROCE = P.COD" & vbCrLf
ExecuteSQL gRDOCon, sConsulta



End Sub


Private Sub V_2_16_Triggers_1()

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PROCE_TG_INSUPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PROCE_TG_INSUPD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER PROCE_TG_INSUPD ON dbo.PROCE" & vbCrLf
sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO INT,@GMN1 VARCHAR(50),@COD SMALLINT" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_PROCE_Ins CURSOR FOR SELECT ANYO,GMN1,COD FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_PROCE_Ins" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_Ins INTO @ANYO,@GMN1,@COD" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET FECACT=GETDATE() WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@COD" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_Ins INTO @ANYO,@GMN1,@COD" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_PROCE_Ins" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_PROCE_Ins" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if update(USUAPER) " & vbCrLf
sConsulta = sConsulta & "  IF (SELECT COUNT(*) FROM INSERTED I WHERE NOT EXISTS (SELECT * FROM (SELECT COD FROM USU UNION SELECT USU FROM ADM) U WHERE I.USUAPER = U.COD))>0" & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "      RAISERROR ('Usuario de apertura no valido', 16, 1)" & vbCrLf
sConsulta = sConsulta & "      ROLLBACK" & vbCrLf
sConsulta = sConsulta & "    END" & vbCrLf
sConsulta = sConsulta & "if update(USUVALSELPROVE) " & vbCrLf
sConsulta = sConsulta & "  IF (SELECT COUNT(*) FROM INSERTED I WHERE NOT EXISTS (SELECT * FROM (SELECT COD FROM USU UNION SELECT USU FROM ADM) U WHERE I.USUVALSELPROVE = U.COD))>0" & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "      RAISERROR ('Usuario de validaci�n de selecci�n de proveedores no valido', 16, 1)" & vbCrLf
sConsulta = sConsulta & "      ROLLBACK" & vbCrLf
sConsulta = sConsulta & "    END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if update(USUVALCIERRE) " & vbCrLf
sConsulta = sConsulta & "  IF (SELECT COUNT(*) FROM INSERTED I WHERE NOT EXISTS (SELECT * FROM (SELECT COD FROM USU UNION SELECT USU FROM ADM) U WHERE I.USUVALCIERRE = U.COD))>0" & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "      RAISERROR ('Usuario de cierre de proceso no valido', 16, 1)" & vbCrLf
sConsulta = sConsulta & "      ROLLBACK" & vbCrLf
sConsulta = sConsulta & "    END" & vbCrLf
sConsulta = sConsulta & "if update(USUVAL) " & vbCrLf
sConsulta = sConsulta & "  IF (SELECT COUNT(*) FROM INSERTED I WHERE NOT EXISTS (SELECT * FROM (SELECT COD FROM USU UNION SELECT USU FROM ADM) U WHERE I.USUVAL = U.COD))>0" & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "      RAISERROR ('Usuario de validaci�n de apertura de proceso no valido', 16, 1)" & vbCrLf
sConsulta = sConsulta & "      ROLLBACK" & vbCrLf
sConsulta = sConsulta & "    END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion2_16_00_01A2_16_00_02() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim bBool As Boolean

On Error GoTo error
sConsulta = "BEGIN TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = True
ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

frmProgreso.lblDetalle = "Actualizamos tablas"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1


V_2_16_Tablas_2


sConsulta = "UPDATE VERSION SET NUM ='2.16.00.02'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "COMMIT TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = False

CodigoDeActualizacion2_16_00_01A2_16_00_02 = True
Exit Function
    
error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_16_00_01A2_16_00_02 = False

End Function

Private Sub V_2_16_Tablas_2()
Dim sConsulta As String

sConsulta = ""
sConsulta = sConsulta & "ALTER TABLE dbo.PARGEN_MAIL ADD" & vbCrLf
sConsulta = sConsulta & "   TIPOMAIL tinyint NOT NULL CONSTRAINT DF_PARGEN_MAIL_TIPOMAIL DEFAULT 0" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "ALTER TABLE dbo.USU ADD" & vbCrLf
sConsulta = sConsulta & "   BAJA tinyint NOT NULL CONSTRAINT DF_USU_BAJA DEFAULT 0" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PROCE_TG_INSUPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PROCE_TG_INSUPD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "CREATE TRIGGER PROCE_TG_INSUPD ON dbo.PROCE" & vbCrLf
sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO INT,@GMN1 VARCHAR(50),@COD SMALLINT" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_PROCE_Ins CURSOR FOR SELECT ANYO,GMN1,COD FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_PROCE_Ins" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_Ins INTO @ANYO,@GMN1,@COD" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET FECACT=GETDATE() WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@COD" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_Ins INTO @ANYO,@GMN1,@COD" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_PROCE_Ins" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_PROCE_Ins" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if update(USUAPER) " & vbCrLf
sConsulta = sConsulta & "  IF (SELECT COUNT(*) FROM INSERTED I WHERE I.USUAPER IS NOT NULL AND NOT EXISTS (SELECT * FROM (SELECT COD FROM USU UNION SELECT USU FROM ADM) U WHERE I.USUAPER = U.COD))>0" & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "      RAISERROR ('Usuario de apertura no valido', 16, 1)" & vbCrLf
sConsulta = sConsulta & "      ROLLBACK" & vbCrLf
sConsulta = sConsulta & "    END" & vbCrLf
sConsulta = sConsulta & "if update(USUVALSELPROVE) " & vbCrLf
sConsulta = sConsulta & "  IF (SELECT COUNT(*) FROM INSERTED I WHERE I.USUVALSELPROVE IS NOT NULL AND NOT EXISTS (SELECT * FROM (SELECT COD FROM USU UNION SELECT USU FROM ADM) U WHERE I.USUVALSELPROVE = U.COD))>0" & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "      RAISERROR ('Usuario de validaci�n de selecci�n de proveedores no valido', 16, 1)" & vbCrLf
sConsulta = sConsulta & "      ROLLBACK" & vbCrLf
sConsulta = sConsulta & "    END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if update(USUVALCIERRE) " & vbCrLf
sConsulta = sConsulta & "  IF (SELECT COUNT(*) FROM INSERTED I WHERE I.USUVALCIERRE IS NOT NULL AND NOT EXISTS (SELECT * FROM (SELECT COD FROM USU UNION SELECT USU FROM ADM) U WHERE I.USUVALCIERRE = U.COD))>0" & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "      RAISERROR ('Usuario de cierre de proceso no valido', 16, 1)" & vbCrLf
sConsulta = sConsulta & "      ROLLBACK" & vbCrLf
sConsulta = sConsulta & "    END" & vbCrLf
sConsulta = sConsulta & "if update(USUVAL) " & vbCrLf
sConsulta = sConsulta & "  IF (SELECT COUNT(*) FROM INSERTED I WHERE  I.USUVAL IS NOT NULL AND NOT EXISTS (SELECT * FROM (SELECT COD FROM USU UNION SELECT USU FROM ADM) U WHERE I.USUVAL = U.COD))>0" & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "      RAISERROR ('Usuario de validaci�n de apertura de proceso no valido', 16, 1)" & vbCrLf
sConsulta = sConsulta & "      ROLLBACK" & vbCrLf
sConsulta = sConsulta & "    END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion2_16_00_02A2_16_00_03() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim bBool As Boolean

On Error GoTo error
sConsulta = "BEGIN TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = True
ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

frmProgreso.lblDetalle = "Actualizamos storeds"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1


V_2_16_Storeds_3

frmProgreso.lblDetalle = "Actualizamos triggers"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1


V_2_16_Triggers_3


sConsulta = "UPDATE VERSION SET NUM ='2.16.00.03'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "COMMIT TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = False

CodigoDeActualizacion2_16_00_02A2_16_00_03 = True
Exit Function
    
error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_16_00_02A2_16_00_03 = False

End Function


Private Sub V_2_16_Storeds_3()
Dim sConsulta As String
sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_NOTIF_DESPUBLICACION]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_NOTIF_DESPUBLICACION]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE SP_NOTIF_DESPUBLICACION (@ANTELACION TINYINT, @NOHANOFERTADO tinyint, @NOVANAOFERTAR tinyint) AS" & vbCrLf
sConsulta = sConsulta & "SELECT PP.PROVE, P.ANYO, P.GMN1, P.COD,  P.FECLIMOFE, P.GMN2 AS GMN2_COD, P.GMN3 AS GMN3_COD, P.GMN4 AS GMN4_COD, GMN1.DEN AS GMN1_DEN, GMN2.DEN AS GMN2_DEN, GMN3.DEN AS GMN3_DEN," & vbCrLf
sConsulta = sConsulta & "       GMN4.DEN AS GMN4_DEN, COM.NOM AS NOM_COM, COM.APE AS APE_COM,  COM.EMAIL AS EMAIL_COM, COM.TFNO AS TFNO_COM, COM.TFNO2 AS TFNO2_COM, COM.FAX AS FAX_COM, COM.CAR AS CAR_COM, PROVE.COD AS PROVE_COD," & vbCrLf
sConsulta = sConsulta & "       PROVE.DEN AS PROVE_DEN, P.DEN AS PROCE_DEN,  CONVERT(VARCHAR,P.FECINISUB,120) AS PROCE_FECINISUB,  CONVERT(VARCHAR,P.FECLIMOFE,120) AS PROCE_FECLIMOFE" & vbCrLf
sConsulta = sConsulta & "FROM PROCE_DEF PD" & vbCrLf
sConsulta = sConsulta & "     INNER JOIN PROCE P " & vbCrLf
sConsulta = sConsulta & "             ON P.GMN1=PD.GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND P.ANYO=PD.ANYO " & vbCrLf
sConsulta = sConsulta & "            AND P.COD=PD.PROCE" & vbCrLf
sConsulta = sConsulta & "     INNER JOIN PROCE_PROVE PP " & vbCrLf
sConsulta = sConsulta & "             ON PP.GMN1=P.GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND PP.ANYO=P.ANYO " & vbCrLf
sConsulta = sConsulta & "            AND PP.PROCE=P.COD" & vbCrLf
sConsulta = sConsulta & "     INNER JOIN GMN1 " & vbCrLf
sConsulta = sConsulta & "             ON P.GMN1=GMN1.COD" & vbCrLf
sConsulta = sConsulta & "     INNER JOIN GMN2 " & vbCrLf
sConsulta = sConsulta & "             ON P.GMN1=GMN2.GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND P.GMN2=GMN2.COD" & vbCrLf
sConsulta = sConsulta & "     INNER JOIN GMN3 " & vbCrLf
sConsulta = sConsulta & "             ON P.GMN1=GMN3.GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND P.GMN2=GMN3.GMN2 " & vbCrLf
sConsulta = sConsulta & "            AND P.GMN3=GMN3.COD" & vbCrLf
sConsulta = sConsulta & "     INNER JOIN GMN4 " & vbCrLf
sConsulta = sConsulta & "             ON P.GMN1=GMN4.GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND P.GMN2=GMN4.GMN2 " & vbCrLf
sConsulta = sConsulta & "            AND P.GMN3=GMN4.GMN3 " & vbCrLf
sConsulta = sConsulta & "            AND P.GMN4=GMN4.COD" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN COM " & vbCrLf
sConsulta = sConsulta & "             ON PP.EQP=COM.EQP " & vbCrLf
sConsulta = sConsulta & "            AND PP.COM=COM.COD" & vbCrLf
sConsulta = sConsulta & "     INNER JOIN PROVE " & vbCrLf
sConsulta = sConsulta & "             ON PP.PROVE=PROVE.COD" & vbCrLf
sConsulta = sConsulta & " WHERE PD.AVISARDESPUB = 1" & vbCrLf
sConsulta = sConsulta & "   AND PP.PUB=1 " & vbCrLf
sConsulta = sConsulta & "   AND PP.AVISADODESPUB = 0" & vbCrLf
sConsulta = sConsulta & "   AND NOT P.FECLIMOFE IS NULL " & vbCrLf
sConsulta = sConsulta & "   AND DATEADD(day,-@ANTELACION,P.FECLIMOFE) < GETDATE()" & vbCrLf
sConsulta = sConsulta & "   AND GETDATE()<=P.FECLIMOFE" & vbCrLf
sConsulta = sConsulta & "   AND PP.NO_OFE = CASE WHEN @NOVANAOFERTAR=1 THEN 0 ELSE PP.NO_OFE END" & vbCrLf
sConsulta = sConsulta & "   AND PP.OFE = CASE WHEN @NOHANOFERTADO = 1 THEN 0 ELSE PP.OFE END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

 sConsulta = ""
sConsulta = sConsulta & ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_DEVOLVER_PROVEEDORES_A_AVISAR]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_DEVOLVER_PROVEEDORES_A_AVISAR]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE SP_DEVOLVER_PROVEEDORES_A_AVISAR (@ANYO INT,@PROCE INT,@GMN1 VARCHAR(50),@EQP VARCHAR(50),@COM VARCHAR(50)) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF  @COM IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "  SELECT DISTINCT PROCE.ANYO,PROCE.GMN1,PROCE.COD,PROCE.DEN ,PROCE_PROVE.PROVE,PROVE.FSP_COD,PROVE.COD,PROVE.DEN, PROCE_PROVE.EQP, PROCE_PROVE.COM,  C1.ID C1ID, C1.NOM C1NOM, C1.APE C1APE, C1.EMAIL C1EMAIL, C2.ID C2ID, C2.NOM C2NOM, C2.APE C2APE, C2.EMAIL C2EMAIL" & vbCrLf
sConsulta = sConsulta & "    FROM PROCE " & vbCrLf
sConsulta = sConsulta & "         INNER JOIN PROCE_PROVE " & vbCrLf
sConsulta = sConsulta & "                 ON PROCE.ANYO = PROCE_PROVE.ANYO " & vbCrLf
sConsulta = sConsulta & "                AND PROCE.GMN1 = PROCE_PROVE.GMN1 " & vbCrLf
sConsulta = sConsulta & "                AND PROCE.COD = PROCE_PROVE.PROCE " & vbCrLf
sConsulta = sConsulta & "                AND PROCE_PROVE.PUB=1" & vbCrLf
sConsulta = sConsulta & "                AND PROCE_PROVE.AVISADODESPUB=0" & vbCrLf
sConsulta = sConsulta & "                AND PROCE_PROVE.EQP=@EQP " & vbCrLf
sConsulta = sConsulta & "                AND PROCE_PROVE.COM=@COM" & vbCrLf
sConsulta = sConsulta & "         INNER JOIN PROVE " & vbCrLf
sConsulta = sConsulta & "                 ON PROVE.COD=PROCE_PROVE.PROVE " & vbCrLf
sConsulta = sConsulta & "                AND PROVE.FSP_COD IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "         LEFT JOIN (" & vbCrLf
sConsulta = sConsulta & "             SELECT C.PROVE, MAX(C.ID) ID ,C.APE,C.NOM,C.EMAIL " & vbCrLf
sConsulta = sConsulta & "                      FROM CON C " & vbCrLf
sConsulta = sConsulta & "                             INNER JOIN ((SELECT ANYO, GMN1, PROCE, PROVE, MAX(ID) ID FROM PROCE_PROVE_PET WHERE ANYO  =@ANYO AND GMN1=@GMN1 AND PROCE = @PROCE AND TIPOPET IN (0,1) GROUP BY ANYO, GMN1,PROCE, PROVE) MPPP " & vbCrLf
sConsulta = sConsulta & "                                             INNER JOIN PROCE_PROVE_PET PPP " & vbCrLf
sConsulta = sConsulta & "                                                     ON MPPP.ANYO = PPP.ANYO " & vbCrLf
sConsulta = sConsulta & "                                                    AND MPPP.GMN1=PPP.GMN1 " & vbCrLf
sConsulta = sConsulta & "                                                    AND MPPP.PROCE = PPP.PROCE " & vbCrLf
sConsulta = sConsulta & "                                                    AND MPPP.PROVE = PPP.PROVE " & vbCrLf
sConsulta = sConsulta & "                                                    AND MPPP.ID = PPP.ID) " & vbCrLf
sConsulta = sConsulta & "                                     ON  C.APE = PPP.APECON " & vbCrLf
sConsulta = sConsulta & "                                    AND C.NOM = PPP.NOMCON " & vbCrLf
sConsulta = sConsulta & "                                    AND C.PROVE = PPP.PROVE" & vbCrLf
sConsulta = sConsulta & "             GROUP BY C.PROVE, C.APE,C.NOM,C.EMAIL) C1" & vbCrLf
sConsulta = sConsulta & "                ON PROVE.COD = C1.PROVE" & vbCrLf
sConsulta = sConsulta & "         LEFT JOIN (" & vbCrLf
sConsulta = sConsulta & "              SELECT C.PROVE, C.ID, C.APE,C.NOM, C.EMAIL " & vbCrLf
sConsulta = sConsulta & "                FROM (SELECT PROVE, MAX(ID) ID FROM CON GROUP BY PROVE) C2 INNER JOIN CON C ON C2.PROVE = C.PROVE AND C2.ID = C.ID" & vbCrLf
sConsulta = sConsulta & "               WHERE C.RECPET=1 AND C.PORT = 1) C2" & vbCrLf
sConsulta = sConsulta & "                ON PROVE.COD = C2.PROVE" & vbCrLf
sConsulta = sConsulta & "    " & vbCrLf
sConsulta = sConsulta & "          " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  WHERE PROCE.COD= @PROCE " & vbCrLf
sConsulta = sConsulta & "    AND PROCE.ANYO =@ANYO " & vbCrLf
sConsulta = sConsulta & "    AND PROCE.GMN1=@GMN1" & vbCrLf
sConsulta = sConsulta & "    AND EXISTS (SELECT ID FROM PROCE_PROVE_PET PPP WHERE PPP.ANYO = PROCE_PROVE.ANYO AND PPP.GMN1=PROCE_PROVE.GMN1 AND PPP.PROCE = PROCE_PROVE.PROCE AND PPP.PROVE = PROCE_PROVE.PROVE AND PPP.TIPOPET IN (0,1))" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  IF @EQP IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "  SELECT DISTINCT PROCE.ANYO,PROCE.GMN1,PROCE.COD,PROCE.DEN ,PROCE_PROVE.PROVE,PROVE.FSP_COD,PROVE.COD,PROVE.DEN, PROCE_PROVE.EQP, PROCE_PROVE.COM, C1.ID C1ID, C1.NOM C1NOM, C1.APE C1APE, C1.EMAIL C1EMAIL, C2.ID C2ID, C2.NOM C2NOM, C2.APE C2APE, C2.EMAIL C2EMAIL" & vbCrLf
sConsulta = sConsulta & "    FROM PROCE " & vbCrLf
sConsulta = sConsulta & "         INNER JOIN PROCE_PROVE " & vbCrLf
sConsulta = sConsulta & "                 ON PROCE.ANYO = PROCE_PROVE.ANYO " & vbCrLf
sConsulta = sConsulta & "                AND PROCE.GMN1 = PROCE_PROVE.GMN1 " & vbCrLf
sConsulta = sConsulta & "                AND PROCE.COD = PROCE_PROVE.PROCE " & vbCrLf
sConsulta = sConsulta & "                AND PROCE_PROVE.PUB=1" & vbCrLf
sConsulta = sConsulta & "                AND PROCE_PROVE.AVISADODESPUB=0" & vbCrLf
sConsulta = sConsulta & "                AND PROCE_PROVE.EQP=@EQP " & vbCrLf
sConsulta = sConsulta & "         INNER JOIN PROVE " & vbCrLf
sConsulta = sConsulta & "                 ON PROVE.COD=PROCE_PROVE.PROVE " & vbCrLf
sConsulta = sConsulta & "                AND PROVE.FSP_COD IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "         LEFT JOIN (" & vbCrLf
sConsulta = sConsulta & "             SELECT C.PROVE, MAX(C.ID) ID ,C.APE,C.NOM,C.EMAIL " & vbCrLf
sConsulta = sConsulta & "                      FROM CON C " & vbCrLf
sConsulta = sConsulta & "                             INNER JOIN ((SELECT ANYO, GMN1, PROCE, PROVE, MAX(ID) ID FROM PROCE_PROVE_PET WHERE ANYO  =@ANYO AND GMN1=@GMN1 AND PROCE = @PROCE AND TIPOPET IN (0,1) GROUP BY ANYO, GMN1,PROCE, PROVE) MPPP " & vbCrLf
sConsulta = sConsulta & "                                             INNER JOIN PROCE_PROVE_PET PPP " & vbCrLf
sConsulta = sConsulta & "                                                     ON MPPP.ANYO = PPP.ANYO " & vbCrLf
sConsulta = sConsulta & "                                                    AND MPPP.GMN1=PPP.GMN1 " & vbCrLf
sConsulta = sConsulta & "                                                    AND MPPP.PROCE = PPP.PROCE " & vbCrLf
sConsulta = sConsulta & "                                                    AND MPPP.PROVE = PPP.PROVE " & vbCrLf
sConsulta = sConsulta & "                                                    AND MPPP.ID = PPP.ID) " & vbCrLf
sConsulta = sConsulta & "                                     ON  C.APE = PPP.APECON " & vbCrLf
sConsulta = sConsulta & "                                    AND C.NOM = PPP.NOMCON " & vbCrLf
sConsulta = sConsulta & "                                    AND C.PROVE = PPP.PROVE" & vbCrLf
sConsulta = sConsulta & "             GROUP BY C.PROVE, C.APE,C.NOM,C.EMAIL) C1" & vbCrLf
sConsulta = sConsulta & "                ON PROVE.COD = C1.PROVE" & vbCrLf
sConsulta = sConsulta & "         LEFT JOIN (" & vbCrLf
sConsulta = sConsulta & "              SELECT C.PROVE, C.ID, C.APE,C.NOM, C.EMAIL " & vbCrLf
sConsulta = sConsulta & "                FROM (SELECT PROVE, MAX(ID) ID FROM CON GROUP BY PROVE) C2 INNER JOIN CON C ON C2.PROVE = C.PROVE AND C2.ID = C.ID" & vbCrLf
sConsulta = sConsulta & "               WHERE C.RECPET=1 AND C.PORT = 1) C2" & vbCrLf
sConsulta = sConsulta & "                ON PROVE.COD = C2.PROVE" & vbCrLf
sConsulta = sConsulta & "    " & vbCrLf
sConsulta = sConsulta & "          " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  WHERE PROCE.COD= @PROCE " & vbCrLf
sConsulta = sConsulta & "    AND PROCE.ANYO =@ANYO " & vbCrLf
sConsulta = sConsulta & "    AND PROCE.GMN1=@GMN1" & vbCrLf
sConsulta = sConsulta & "    AND EXISTS (SELECT ID FROM PROCE_PROVE_PET PPP WHERE PPP.ANYO = PROCE_PROVE.ANYO AND PPP.GMN1=PROCE_PROVE.GMN1 AND PPP.PROCE = PROCE_PROVE.PROCE AND PPP.PROVE = PROCE_PROVE.PROVE AND PPP.TIPOPET IN (0,1))" & vbCrLf
sConsulta = sConsulta & "  ELSE" & vbCrLf
sConsulta = sConsulta & "  SELECT DISTINCT PROCE.ANYO,PROCE.GMN1,PROCE.COD,PROCE.DEN ,PROCE_PROVE.PROVE,PROVE.FSP_COD,PROVE.COD,PROVE.DEN, PROCE_PROVE.EQP, PROCE_PROVE.COM, C1.ID C1ID, C1.NOM C1NOM, C1.APE C1APE, C1.EMAIL C1EMAIL, C2.ID C2ID, C2.NOM C2NOM, C2.APE C2APE, C2.EMAIL C2EMAIL" & vbCrLf
sConsulta = sConsulta & "    FROM PROCE " & vbCrLf
sConsulta = sConsulta & "         INNER JOIN PROCE_PROVE " & vbCrLf
sConsulta = sConsulta & "                 ON PROCE.ANYO = PROCE_PROVE.ANYO " & vbCrLf
sConsulta = sConsulta & "                AND PROCE.GMN1 = PROCE_PROVE.GMN1 " & vbCrLf
sConsulta = sConsulta & "                AND PROCE.COD = PROCE_PROVE.PROCE " & vbCrLf
sConsulta = sConsulta & "                AND PROCE_PROVE.PUB=1" & vbCrLf
sConsulta = sConsulta & "                AND PROCE_PROVE.AVISADODESPUB=0" & vbCrLf
sConsulta = sConsulta & "         INNER JOIN PROVE " & vbCrLf
sConsulta = sConsulta & "                 ON PROVE.COD=PROCE_PROVE.PROVE " & vbCrLf
sConsulta = sConsulta & "                AND PROVE.FSP_COD IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "         LEFT JOIN (" & vbCrLf
sConsulta = sConsulta & "             SELECT C.PROVE, MAX(C.ID) ID ,C.APE,C.NOM,C.EMAIL " & vbCrLf
sConsulta = sConsulta & "                      FROM CON C " & vbCrLf
sConsulta = sConsulta & "                             INNER JOIN ((SELECT ANYO, GMN1, PROCE, PROVE, MAX(ID) ID FROM PROCE_PROVE_PET WHERE ANYO  =@ANYO AND GMN1=@GMN1 AND PROCE = @PROCE AND TIPOPET IN (0,1) GROUP BY ANYO, GMN1,PROCE, PROVE) MPPP " & vbCrLf
sConsulta = sConsulta & "                                             INNER JOIN PROCE_PROVE_PET PPP " & vbCrLf
sConsulta = sConsulta & "                                                     ON MPPP.ANYO = PPP.ANYO " & vbCrLf
sConsulta = sConsulta & "                                                    AND MPPP.GMN1=PPP.GMN1 " & vbCrLf
sConsulta = sConsulta & "                                                    AND MPPP.PROCE = PPP.PROCE " & vbCrLf
sConsulta = sConsulta & "                                                    AND MPPP.PROVE = PPP.PROVE " & vbCrLf
sConsulta = sConsulta & "                                                    AND MPPP.ID = PPP.ID) " & vbCrLf
sConsulta = sConsulta & "                                     ON  C.APE = PPP.APECON " & vbCrLf
sConsulta = sConsulta & "                                    AND C.NOM = PPP.NOMCON " & vbCrLf
sConsulta = sConsulta & "                                    AND C.PROVE = PPP.PROVE" & vbCrLf
sConsulta = sConsulta & "             GROUP BY C.PROVE, C.APE,C.NOM,C.EMAIL) C1" & vbCrLf
sConsulta = sConsulta & "                ON PROVE.COD = C1.PROVE" & vbCrLf
sConsulta = sConsulta & "         LEFT JOIN (" & vbCrLf
sConsulta = sConsulta & "              SELECT C.PROVE, C.ID, C.APE,C.NOM, C.EMAIL " & vbCrLf
sConsulta = sConsulta & "                FROM (SELECT PROVE, MAX(ID) ID FROM CON GROUP BY PROVE) C2 INNER JOIN CON C ON C2.PROVE = C.PROVE AND C2.ID = C.ID" & vbCrLf
sConsulta = sConsulta & "               WHERE C.RECPET=1 AND C.PORT = 1) C2" & vbCrLf
sConsulta = sConsulta & "                ON PROVE.COD = C2.PROVE" & vbCrLf
sConsulta = sConsulta & "    " & vbCrLf
sConsulta = sConsulta & "          " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  WHERE PROCE.COD= @PROCE " & vbCrLf
sConsulta = sConsulta & "    AND PROCE.ANYO =@ANYO " & vbCrLf
sConsulta = sConsulta & "    AND PROCE.GMN1=@GMN1" & vbCrLf
sConsulta = sConsulta & "    AND EXISTS (SELECT ID FROM PROCE_PROVE_PET PPP WHERE PPP.ANYO = PROCE_PROVE.ANYO AND PPP.GMN1=PROCE_PROVE.GMN1 AND PPP.PROCE = PROCE_PROVE.PROCE AND PPP.PROVE = PROCE_PROVE.PROVE AND PPP.TIPOPET IN (0,1))" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_SOLICITUD_DATOS_EMAIL]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_SOLICITUD_DATOS_EMAIL]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_SOLICITUD_DATOS_EMAIL @ID INT,@TYPE TINYINT, @EMAIL VARCHAR(100) OUTPUT,@IDIOMA VARCHAR(50) OUTPUT,@SUBJECT VARCHAR(2000) OUTPUT, @TIPOEMAIL TINYINT = 0 OUTPUT  AS" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "/* " & vbCrLf
sConsulta = sConsulta & "   Par�metros de entrada:  ID  : Identificador de la solicitud" & vbCrLf
sConsulta = sConsulta & "               TYPE    :   0 -> Asignaci�n a comprador" & vbCrLf
sConsulta = sConsulta & "                       1 -> Aprobaci�n a peticionario" & vbCrLf
sConsulta = sConsulta & "                       2 -> Rechazo" & vbCrLf
sConsulta = sConsulta & "                       3 -> Anulaci�n desde GS         " & vbCrLf
sConsulta = sConsulta & "                       4 -> Cierre" & vbCrLf
sConsulta = sConsulta & "                       5 -> Reapertura" & vbCrLf
sConsulta = sConsulta & "                       6 -> Alta de solicitud " & vbCrLf
sConsulta = sConsulta & "                       7 -> Anulaci�n desde el EP" & vbCrLf
sConsulta = sConsulta & "   Par�metros de s�lida:   EMAIL   : Email del peticionario, menos en el tipo 0 que ser�a el del comprador" & vbCrLf
sConsulta = sConsulta & "               IDIOMA  : Idioma del usuario que ha realizado la solicitud, seg�n su preferencia indicada en el EP" & vbCrLf
sConsulta = sConsulta & "               SUBJECT: Asunto del mensaje" & vbCrLf
sConsulta = sConsulta & "*/" & vbCrLf
sConsulta = sConsulta & "IF @TYPE=0" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "   SELECT @EMAIL=PER.EMAIL, @TIPOEMAIL= U.TIPOEMAIL FROM PER INNER JOIN SOLICIT ON SOLICIT.ID=@ID AND PER.COD=SOLICIT.COMPRADOR LEFT JOIN USU U ON PER.COD = U.PER" & vbCrLf
sConsulta = sConsulta & "   SET @IDIOMA=(SELECT IDIOMA FROM PARGEN_DEF WHERE ID=1)" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = N' SET @SUBJECT= (SELECT TEXT_' + @IDIOMA + N' FROM WEBTEXT WHERE MODULO=101 AND ID=1)'" & vbCrLf
sConsulta = sConsulta & "   EXEC sp_executesql  @SQL ,N'@SUBJECT VARCHAR(2000) OUTPUT', @SUBJECT=@SUBJECT OUTPUT" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   IF @TYPE=1" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "       SELECT @EMAIL=PER.EMAIL, @TIPOEMAIL= U.TIPOEMAIL FROM PER INNER JOIN SOLICIT ON SOLICIT.ID=@ID AND PER.COD=SOLICIT.PER LEFT JOIN USU U ON PER.COD = U.PER" & vbCrLf
sConsulta = sConsulta & "       SET @IDIOMA=(SELECT IDIOMA FROM USU INNER JOIN SOLICIT ON SOLICIT.ID=@ID AND USU.PER=SOLICIT.PER)" & vbCrLf
sConsulta = sConsulta & "       IF @IDIOMA IS NULL   SET @IDIOMA=(SELECT IDIOMA FROM PARGEN_DEF WHERE ID=1)" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = N' SET @SUBJECT= (SELECT TEXT_' + @IDIOMA + N' FROM WEBTEXT WHERE MODULO=101 AND ID=2)'" & vbCrLf
sConsulta = sConsulta & "       EXEC sp_executesql  @SQL ,N'@SUBJECT VARCHAR(2000) OUTPUT', @SUBJECT=@SUBJECT OUTPUT" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "       IF @TYPE=2" & vbCrLf
sConsulta = sConsulta & "           BEGIN" & vbCrLf
sConsulta = sConsulta & "           SELECT @EMAIL=PER.EMAIL, @TIPOEMAIL= U.TIPOEMAIL FROM PER INNER JOIN SOLICIT ON SOLICIT.ID=@ID AND PER.COD=SOLICIT.PER LEFT JOIN USU U ON PER.COD = U.PER" & vbCrLf
sConsulta = sConsulta & "           SET @IDIOMA=(SELECT IDIOMA FROM USU INNER JOIN SOLICIT ON SOLICIT.ID=@ID AND USU.PER=SOLICIT.PER)" & vbCrLf
sConsulta = sConsulta & "           IF @IDIOMA IS NULL  SET @IDIOMA=(SELECT IDIOMA FROM PARGEN_DEF WHERE ID=1)" & vbCrLf
sConsulta = sConsulta & "           SET @SQL = N' SET @SUBJECT= (SELECT TEXT_' + @IDIOMA + N' FROM WEBTEXT WHERE MODULO=101 AND ID=3)'" & vbCrLf
sConsulta = sConsulta & "           EXEC sp_executesql  @SQL ,N'@SUBJECT VARCHAR(2000) OUTPUT', @SUBJECT=@SUBJECT OUTPUT" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "       ELSE" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "           IF @TYPE=3" & vbCrLf
sConsulta = sConsulta & "               BEGIN" & vbCrLf
sConsulta = sConsulta & "               SELECT @EMAIL=PER.EMAIL, @TIPOEMAIL= U.TIPOEMAIL FROM PER INNER JOIN SOLICIT ON SOLICIT.ID=@ID AND PER.COD=SOLICIT.PER LEFT JOIN USU U ON PER.COD = U.PER" & vbCrLf
sConsulta = sConsulta & "               SET @IDIOMA=(SELECT IDIOMA FROM USU INNER JOIN SOLICIT ON SOLICIT.ID=@ID AND USU.PER=SOLICIT.PER)" & vbCrLf
sConsulta = sConsulta & "               IF @IDIOMA IS NULL  SET @IDIOMA=(SELECT IDIOMA FROM PARGEN_DEF WHERE ID=1)" & vbCrLf
sConsulta = sConsulta & "               SET @SQL = N' SET @SUBJECT= (SELECT TEXT_' + @IDIOMA + N' FROM WEBTEXT WHERE MODULO=101 AND ID=4)'" & vbCrLf
sConsulta = sConsulta & "               EXEC sp_executesql  @SQL ,N'@SUBJECT VARCHAR(2000) OUTPUT', @SUBJECT=@SUBJECT OUTPUT" & vbCrLf
sConsulta = sConsulta & "               END" & vbCrLf
sConsulta = sConsulta & "           ELSE" & vbCrLf
sConsulta = sConsulta & "               " & vbCrLf
sConsulta = sConsulta & "               IF @TYPE=4" & vbCrLf
sConsulta = sConsulta & "                   BEGIN" & vbCrLf
sConsulta = sConsulta & "                   SELECT @EMAIL=PER.EMAIL, @TIPOEMAIL= U.TIPOEMAIL FROM PER INNER JOIN SOLICIT ON SOLICIT.ID=@ID AND PER.COD=SOLICIT.PER LEFT JOIN USU U ON PER.COD = U.PER" & vbCrLf
sConsulta = sConsulta & "                   SET @IDIOMA=(SELECT IDIOMA FROM USU INNER JOIN SOLICIT ON SOLICIT.ID=@ID AND USU.PER=SOLICIT.PER)" & vbCrLf
sConsulta = sConsulta & "                   IF @IDIOMA IS NULL  SET @IDIOMA=(SELECT IDIOMA FROM PARGEN_DEF WHERE ID=1)" & vbCrLf
sConsulta = sConsulta & "                   SET @SQL = N' SET @SUBJECT= (SELECT TEXT_' + @IDIOMA + N' FROM WEBTEXT WHERE MODULO=101 AND ID=5)'" & vbCrLf
sConsulta = sConsulta & "                   EXEC sp_executesql  @SQL ,N'@SUBJECT VARCHAR(2000) OUTPUT', @SUBJECT=@SUBJECT OUTPUT" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               ELSE" & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                   IF @TYPE=5" & vbCrLf
sConsulta = sConsulta & "                       BEGIN" & vbCrLf
sConsulta = sConsulta & "                       SELECT @EMAIL=PER.EMAIL, @TIPOEMAIL= U.TIPOEMAIL FROM PER INNER JOIN SOLICIT ON SOLICIT.ID=@ID AND PER.COD=SOLICIT.PER LEFT JOIN USU U ON PER.COD = U.PER" & vbCrLf
sConsulta = sConsulta & "                       SET @IDIOMA=(SELECT IDIOMA FROM USU INNER JOIN SOLICIT ON SOLICIT.ID=@ID AND USU.PER=SOLICIT.PER)" & vbCrLf
sConsulta = sConsulta & "                       IF @IDIOMA IS NULL  SET @IDIOMA=(SELECT IDIOMA FROM PARGEN_DEF WHERE ID=1)" & vbCrLf
sConsulta = sConsulta & "                       SET @SQL = N' SET @SUBJECT= (SELECT TEXT_' + @IDIOMA + N' FROM WEBTEXT WHERE MODULO=101 AND ID=6)'" & vbCrLf
sConsulta = sConsulta & "                       EXEC sp_executesql  @SQL ,N'@SUBJECT VARCHAR(2000) OUTPUT', @SUBJECT=@SUBJECT OUTPUT" & vbCrLf
sConsulta = sConsulta & "                       END" & vbCrLf
sConsulta = sConsulta & "                   ELSE" & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                       IF @TYPE=6" & vbCrLf
sConsulta = sConsulta & "                           BEGIN" & vbCrLf
sConsulta = sConsulta & "                           SELECT @EMAIL=PER.EMAIL, @TIPOEMAIL= U.TIPOEMAIL FROM PER INNER JOIN SOLICIT ON SOLICIT.ID=@ID AND PER.COD=SOLICIT.COMPRADOR LEFT JOIN USU U ON PER.COD = U.PER" & vbCrLf
sConsulta = sConsulta & "                           SET @IDIOMA=(SELECT IDIOMA FROM PARGEN_DEF WHERE ID=1)" & vbCrLf
sConsulta = sConsulta & "                           SET @SQL = N' SET @SUBJECT= (SELECT TEXT_' + @IDIOMA + N' FROM WEBTEXT WHERE MODULO=101 AND ID=7)'" & vbCrLf
sConsulta = sConsulta & "                           EXEC sp_executesql  @SQL ,N'@SUBJECT VARCHAR(2000) OUTPUT', @SUBJECT=@SUBJECT OUTPUT" & vbCrLf
sConsulta = sConsulta & "                           END" & vbCrLf
sConsulta = sConsulta & "                       ELSE" & vbCrLf
sConsulta = sConsulta & "                       " & vbCrLf
sConsulta = sConsulta & "                           IF @TYPE=7" & vbCrLf
sConsulta = sConsulta & "                               BEGIN" & vbCrLf
sConsulta = sConsulta & "                               SELECT @EMAIL=PER.EMAIL, @TIPOEMAIL= U.TIPOEMAIL FROM PER INNER JOIN SOLICIT ON SOLICIT.ID=@ID AND PER.COD=SOLICIT.COMPRADOR LEFT JOIN USU U ON PER.COD = U.PER" & vbCrLf
sConsulta = sConsulta & "                               SET @IDIOMA=(SELECT IDIOMA FROM PARGEN_DEF WHERE ID=1)" & vbCrLf
sConsulta = sConsulta & "                               SET @SQL = N' SET @SUBJECT= (SELECT TEXT_' + @IDIOMA + N' FROM WEBTEXT WHERE MODULO=101 AND ID=8)'" & vbCrLf
sConsulta = sConsulta & "                               EXEC sp_executesql  @SQL ,N'@SUBJECT VARCHAR(2000) OUTPUT', @SUBJECT=@SUBJECT OUTPUT" & vbCrLf
sConsulta = sConsulta & "                               END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_VALIDAR_USUARIO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_VALIDAR_USUARIO]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE  FSEP_VALIDAR_USUARIO (@USU VARCHAR(80),@PWD VARCHAR(255),@BLOQUEO INTEGER)  AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE @NUM_INTENTOS AS INTEGER" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @NUMARTICULOS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @IMPORTETOTAL FLOAT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @NUMARTICULOS = COUNT(*) , @IMPORTETOTAL = ISNULL(SUM(isnull(CANT,0)*isnull(PREC,0)*isnull(FC,1)),0)" & vbCrLf
sConsulta = sConsulta & "  FROM CESTA " & vbCrLf
sConsulta = sConsulta & "    INNER JOIN USU " & vbCrLf
sConsulta = sConsulta & "            ON USU.PER = CESTA.PER" & vbCrLf
sConsulta = sConsulta & " WHERE USU.COD = @USU" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "If @BLOQUEO > 0 " & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    SET @NUM_INTENTOS = (SELECT USU.BLOQ FROM USU WHERE USU.COD=@USU AND USU.PWD=@PWD AND USU.BLOQ<@BLOQUEO)" & vbCrLf
sConsulta = sConsulta & "    IF (@NUM_INTENTOS IS NULL)" & vbCrLf
sConsulta = sConsulta & "       UPDATE USU SET BLOQ=BLOQ+1 WHERE USU.COD=@USU" & vbCrLf
sConsulta = sConsulta & "    ELSE" & vbCrLf
sConsulta = sConsulta & "      BEGIN" & vbCrLf
sConsulta = sConsulta & "        IF @NUM_INTENTOS < @BLOQUEO" & vbCrLf
sConsulta = sConsulta & "          BEGIN" & vbCrLf
sConsulta = sConsulta & "            SELECT USU.COD, PER.COD AS PER, PER.NOM, USU.THOUSANFMT, USU.DECIMALFMT, USU.PRECISIONFMT, " & vbCrLf
sConsulta = sConsulta & "                   USU.MOSTRARFMT,USU.DATEFMT,FSEP,USU.MON,USU.FSEPTIPO,MON.EQUIV EQUIV,USU.TIPOEMAIL,USU.IDIOMA," & vbCrLf
sConsulta = sConsulta & "                   USU.DEST,USU.DEST_OTRAS_UO,USU.SOL_COMPRA, MOSTIMGART, MOSTATRIB, MOSTCANTMIN, MOSTCODPROVE, MOSTCODART ," & vbCrLf
sConsulta = sConsulta & "                   @NUMARTICULOS NUMARTICULOS, @IMPORTETOTAL IMPORTETOTAL, ORDEN, DIREC, PEDLIBRE,PED_OTRAS_EMP" & vbCrLf
sConsulta = sConsulta & "              FROM USU INNER JOIN PER ON USU.PER=PER.COD" & vbCrLf
sConsulta = sConsulta & "                       INNER JOIN MON ON USU.MON =MON.COD" & vbCrLf
sConsulta = sConsulta & "             WHERE USU.COD =@USU AND USU.PWD=@PWD AND FSEP>0 AND BAJA = 0" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "            UPDATE USU SET BLOQ=0 WHERE USU.COD=@USU" & vbCrLf
sConsulta = sConsulta & "          END" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "  END " & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "    SELECT USU.COD, PER.COD AS PER, PER.NOM, USU.THOUSANFMT, USU.DECIMALFMT, USU.PRECISIONFMT, " & vbCrLf
sConsulta = sConsulta & "    USU.MOSTRARFMT,USU.DATEFMT,FSEP,USU.MON,USU.FSEPTIPO,MON.EQUIV EQUIV, USU.TIPOEMAIL, USU.IDIOMA," & vbCrLf
sConsulta = sConsulta & "    USU.DEST,USU.DEST_OTRAS_UO,USU.SOL_COMPRA, MOSTIMGART, MOSTATRIB, MOSTCANTMIN, MOSTCODPROVE, MOSTCODART ," & vbCrLf
sConsulta = sConsulta & "    @NUMARTICULOS NUMARTICULOS, @IMPORTETOTAL IMPORTETOTAL, ORDEN, DIREC, PEDLIBRE,PED_OTRAS_EMP" & vbCrLf
sConsulta = sConsulta & "     From USU INNER JOIN PER ON USU.PER=PER.COD" & vbCrLf
sConsulta = sConsulta & "     inner join MON ON USU.MON =MON.COD" & vbCrLf
sConsulta = sConsulta & "    WHERE USU.COD =@USU AND USU.PWD=@PWD AND FSEP>0 AND BAJA = 0" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Private Sub V_2_16_Triggers_3()
Dim sConsulta As String

sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ITEM_TG_UPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[ITEM_TG_UPD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TRIGGER ITEM_TG_UPD ON dbo.ITEM" & vbCrLf
sConsulta = sConsulta & "FOR UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN1 AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCE AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN2 AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN3 AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN4 AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @ART AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @CANT AS FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @FECINI AS DATETIME" & vbCrLf
sConsulta = sConsulta & "DECLARE @FECFIN AS DATETIME" & vbCrLf
sConsulta = sConsulta & "DECLARE @EST AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @OBJ AS FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @OBJOLD AS FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @FECNECMANUAL  AS TINYINT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_ITEM_Upd CURSOR FOR SELECT ANYO,GMN1,PROCE,ID,GMN2,GMN3,GMN4,ART,CANT,FECINI,FECFIN,OBJ FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_ITEM_Upd" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_ITEM_Upd INTO @ANYO,@GMN1,@PROCE,@ID,@GMN2,@GMN3,@GMN4,@ART,@CANT,@FECINI,@FECFIN,@OBJ" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET  @FECNECMANUAL=(SELECT PROCE.FECNECMANUAL FROM PROCE  WHERE ANYO=@ANYO  AND GMN1=@GMN1 AND COD=@PROCE)" & vbCrLf
sConsulta = sConsulta & "IF @FECNECMANUAL=0" & vbCrLf
sConsulta = sConsulta & "  UPDATE PROCE SET FECNEC =(SELECT MIN(FECINI) FROM ITEM WHERE ANYO=@ANYO  AND GMN1=@GMN1 AND PROCE=@PROCE)  WHERE COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF UPDATE(PRES) OR UPDATE(CONF)" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "     UPDATE PROCE SET PRESTOTAL=(SELECT SUM(PRES) FROM ITEM WHERE ANYO= @ANYO  AND GMN1=@GMN1 AND PROCE=@PROCE AND CONF=1 GROUP BY ANYO,GMN1,PROCE )   ," & vbCrLf
sConsulta = sConsulta & "     CONSUMIDO=(select sum(a.consumido) from (select item_adj.anyo,item_adj.gmn1,item_adj.proce,item_adj.item,item_adj.prove,item_adj.ofe,sum(item.prec*(item.cant*(porcen/100))) as consumido from item_adj  " & vbCrLf
sConsulta = sConsulta & "     inner join proce_ofe on item_adj.anyo=proce_ofe.anyo and item_adj.gmn1=proce_ofe.gmn1 and item_adj.proce=proce_ofe.proce and item_adj.prove=proce_ofe.prove and item_adj.ofe=proce_ofe.ofe and ult=1" & vbCrLf
sConsulta = sConsulta & "     inner join item on item_adj.anyo=item.anyo and item_adj.gmn1=item.gmn1 and item_adj.proce=item.proce and item_adj.item=item.id where item_adj.anyo=@ANYO and item_adj.gmn1=@GMN1 and item_adj.proce=@PROCE group by item_adj.anyo,item_adj.gmn1,item_adj.proce,item_adj.item,item_adj.prove,item_adj.ofe) a )" & vbCrLf
sConsulta = sConsulta & "     WHERE ANYO= @ANYO  AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF UPDATE(OBJ)" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @EST = (SELECT EST FROM PROCE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE)" & vbCrLf
sConsulta = sConsulta & "SET @OBJOLD = (SELECT OBJ FROM DELETED WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ID=@ID)" & vbCrLf
sConsulta = sConsulta & "IF @OBJ IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "/*Si estamos en estado 8 y borramos el �ltimo objetivo entonces pasamos a estado 7*/" & vbCrLf
sConsulta = sConsulta & "IF @EST = 8 AND ((SELECT COUNT(*) FROM ITEM WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND OBJ IS NOT NULL )=0) UPDATE PROCE SET EST=7 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "IF @EST = 9  AND ((SELECT COUNT(*) FROM ITEM WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND OBJ IS NOT NULL )=0) UPDATE PROCE SET EST=10 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "IF @OBJOLD IS NOT NULL AND ((SELECT COUNT(*) FROM ITEM WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND OBJ IS NOT NULL )=0) UPDATE PROCE_PROVE SET NUMOBJ=0,OBJNUEVOS=0 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Else" & vbCrLf
sConsulta = sConsulta & "IF @OBJOLD IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @EST = 7 UPDATE PROCE SET EST=8 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "IF @EST = 10 UPDATE PROCE SET EST=9 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE SET OBJNUEVOS=1,NUMOBJ=NUMOBJ+1 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Else" & vbCrLf
sConsulta = sConsulta & "IF @OBJOLD <> @OBJ" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @EST = 7 UPDATE PROCE SET EST=8 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "IF @EST = 10 UPDATE PROCE SET EST=9 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE SET OBJNUEVOS=1,NUMOBJ=NUMOBJ+1 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "_FETCH:" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_ITEM_Upd INTO @ANYO,@GMN1,@PROCE,@ID,@GMN2,@GMN3,@GMN4,@ART,@CANT,@FECINI,@FECFIN,@OBJ" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_ITEM_Upd" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_ITEM_Upd" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub


Public Function CodigoDeActualizacion2_16_00_03A2_16_00_04() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim bBool As Boolean

On Error GoTo error
sConsulta = "BEGIN TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = True
ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

frmProgreso.lblDetalle = "Actualizamos storeds"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1


V_2_16_Storeds_4

frmProgreso.lblDetalle = "Actualizamos triggers"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1


V_2_16_Triggers_4


frmProgreso.lblDetalle = "Actualizamos idioma de los contactos"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_16_Datos_4

sConsulta = "UPDATE VERSION SET NUM ='2.16.00.04'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "COMMIT TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = False

CodigoDeActualizacion2_16_00_03A2_16_00_04 = True
Exit Function
    
error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_16_00_03A2_16_00_04 = False

End Function


Private Sub V_2_16_Storeds_4()
Dim sConsulta As String

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_SOLICITUD_DATOS_EMAIL]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_SOLICITUD_DATOS_EMAIL]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE SP_SOLICITUD_DATOS_EMAIL @ID INT,@TYPE TINYINT, @EMAIL VARCHAR(100) OUTPUT,@IDIOMA VARCHAR(50) OUTPUT,@SUBJECT VARCHAR(2000) OUTPUT, @TIPOEMAIL TINYINT = 0 OUTPUT  AS" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @USU VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @EP AS TINYINT" & vbCrLf
sConsulta = sConsulta & "/* " & vbCrLf
sConsulta = sConsulta & "   Par�metros de entrada:  ID  : Identificador de la solicitud" & vbCrLf
sConsulta = sConsulta & "               TYPE    :   0 -> Asignaci�n a comprador" & vbCrLf
sConsulta = sConsulta & "                       1 -> Aprobaci�n a peticionario" & vbCrLf
sConsulta = sConsulta & "                       2 -> Rechazo" & vbCrLf
sConsulta = sConsulta & "                       3 -> Anulaci�n desde GS         " & vbCrLf
sConsulta = sConsulta & "                       4 -> Cierre" & vbCrLf
sConsulta = sConsulta & "                       5 -> Reapertura" & vbCrLf
sConsulta = sConsulta & "                       6 -> Alta de solicitud " & vbCrLf
sConsulta = sConsulta & "                       7 -> Anulaci�n desde el EP" & vbCrLf
sConsulta = sConsulta & "   Par�metros de s�lida:   EMAIL   : Email del peticionario, menos en el tipo 0 que ser�a el del comprador" & vbCrLf
sConsulta = sConsulta & "               IDIOMA  : Idioma del usuario que ha realizado la solicitud, seg�n su preferencia indicada en el EP" & vbCrLf
sConsulta = sConsulta & "               SUBJECT: Asunto del mensaje" & vbCrLf
sConsulta = sConsulta & "*/" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @TYPE=0" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "   SELECT @EMAIL=PER.EMAIL, @TIPOEMAIL= U.TIPOEMAIL, @USU=U.COD,@IDIOMA=U.IDIOMA,@EP=U.FSEP  FROM PER INNER JOIN SOLICIT ON SOLICIT.ID=@ID AND PER.COD=SOLICIT.COMPRADOR LEFT JOIN USU U ON PER.COD = U.PER" & vbCrLf
sConsulta = sConsulta & "   IF @EP=0 OR @IDIOMA IS NULL" & vbCrLf
sConsulta = sConsulta & "   BEGIN " & vbCrLf
sConsulta = sConsulta & "       SET @IDIOMA =NULL" & vbCrLf
sConsulta = sConsulta & "       SET @IDIOMA=(SELECT IDIOMA FROM PARINS_GEST WHERE USU=@USU)" & vbCrLf
sConsulta = sConsulta & "       IF @IDIOMA IS  NULL   SET @IDIOMA=(SELECT IDIOMA FROM PARGEN_DEF WHERE ID=1)" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = N' SET @SUBJECT= (SELECT TEXT_' + @IDIOMA + N' FROM WEBTEXT WHERE MODULO=101 AND ID=1)'" & vbCrLf
sConsulta = sConsulta & "   EXEC sp_executesql  @SQL ,N'@SUBJECT VARCHAR(2000) OUTPUT', @SUBJECT=@SUBJECT OUTPUT" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   IF @TYPE=1" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "       SELECT @EMAIL=PER.EMAIL, @TIPOEMAIL= U.TIPOEMAIL, @USU=U.COD,@IDIOMA=U.IDIOMA,@EP=U.FSEP FROM PER INNER JOIN SOLICIT ON SOLICIT.ID=@ID AND PER.COD=SOLICIT.PER LEFT JOIN USU U ON PER.COD = U.PER" & vbCrLf
sConsulta = sConsulta & "       IF @EP=0 OR @IDIOMA IS NULL" & vbCrLf
sConsulta = sConsulta & "       BEGIN " & vbCrLf
sConsulta = sConsulta & "          SET @IDIOMA =NULL" & vbCrLf
sConsulta = sConsulta & "          SET @IDIOMA=(SELECT IDIOMA FROM PARINS_GEST WHERE USU=@USU)" & vbCrLf
sConsulta = sConsulta & "          IF @IDIOMA IS  NULL   SET @IDIOMA=(SELECT IDIOMA FROM PARGEN_DEF WHERE ID=1)" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = N' SET @SUBJECT= (SELECT TEXT_' + @IDIOMA + N' FROM WEBTEXT WHERE MODULO=101 AND ID=2)'" & vbCrLf
sConsulta = sConsulta & "       EXEC sp_executesql  @SQL ,N'@SUBJECT VARCHAR(2000) OUTPUT', @SUBJECT=@SUBJECT OUTPUT" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "       IF @TYPE=2" & vbCrLf
sConsulta = sConsulta & "           BEGIN" & vbCrLf
sConsulta = sConsulta & "           SELECT @EMAIL=PER.EMAIL, @TIPOEMAIL= U.TIPOEMAIL, @USU=U.COD,@IDIOMA=U.IDIOMA,@EP=U.FSEP  FROM PER INNER JOIN SOLICIT ON SOLICIT.ID=@ID AND PER.COD=SOLICIT.PER LEFT JOIN USU U ON PER.COD = U.PER" & vbCrLf
sConsulta = sConsulta & "           IF @EP=0 OR @IDIOMA IS NULL" & vbCrLf
sConsulta = sConsulta & "           BEGIN " & vbCrLf
sConsulta = sConsulta & "              SET @IDIOMA =NULL" & vbCrLf
sConsulta = sConsulta & "              SET @IDIOMA=(SELECT IDIOMA FROM PARINS_GEST WHERE USU=@USU)" & vbCrLf
sConsulta = sConsulta & "              IF @IDIOMA IS  NULL   SET @IDIOMA=(SELECT IDIOMA FROM PARGEN_DEF WHERE ID=1)" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "           SET @SQL = N' SET @SUBJECT= (SELECT TEXT_' + @IDIOMA + N' FROM WEBTEXT WHERE MODULO=101 AND ID=3)'" & vbCrLf
sConsulta = sConsulta & "           EXEC sp_executesql  @SQL ,N'@SUBJECT VARCHAR(2000) OUTPUT', @SUBJECT=@SUBJECT OUTPUT" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "       ELSE" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "           IF @TYPE=3" & vbCrLf
sConsulta = sConsulta & "               BEGIN" & vbCrLf
sConsulta = sConsulta & "               SELECT @EMAIL=PER.EMAIL, @TIPOEMAIL= U.TIPOEMAIL, @USU=U.COD,@IDIOMA=U.IDIOMA,@EP=U.FSEP  FROM PER INNER JOIN SOLICIT ON SOLICIT.ID=@ID AND PER.COD=SOLICIT.PER LEFT JOIN USU U ON PER.COD = U.PER" & vbCrLf
sConsulta = sConsulta & "               IF @EP=0 OR @IDIOMA IS NULL" & vbCrLf
sConsulta = sConsulta & "               BEGIN " & vbCrLf
sConsulta = sConsulta & "                  SET @IDIOMA =NULL" & vbCrLf
sConsulta = sConsulta & "                  SET @IDIOMA=(SELECT IDIOMA FROM PARINS_GEST WHERE USU=@USU)" & vbCrLf
sConsulta = sConsulta & "                  IF @IDIOMA IS  NULL   SET @IDIOMA=(SELECT IDIOMA FROM PARGEN_DEF WHERE ID=1)" & vbCrLf
sConsulta = sConsulta & "               END" & vbCrLf
sConsulta = sConsulta & "               SET @SQL = N' SET @SUBJECT= (SELECT TEXT_' + @IDIOMA + N' FROM WEBTEXT WHERE MODULO=101 AND ID=4)'" & vbCrLf
sConsulta = sConsulta & "               EXEC sp_executesql  @SQL ,N'@SUBJECT VARCHAR(2000) OUTPUT', @SUBJECT=@SUBJECT OUTPUT" & vbCrLf
sConsulta = sConsulta & "               END" & vbCrLf
sConsulta = sConsulta & "           ELSE" & vbCrLf
sConsulta = sConsulta & "               " & vbCrLf
sConsulta = sConsulta & "               IF @TYPE=4" & vbCrLf
sConsulta = sConsulta & "                   BEGIN" & vbCrLf
sConsulta = sConsulta & "                   SELECT @EMAIL=PER.EMAIL, @TIPOEMAIL= U.TIPOEMAIL, @USU=U.COD,@IDIOMA=U.IDIOMA,@EP=U.FSEP  FROM PER INNER JOIN SOLICIT ON SOLICIT.ID=@ID AND PER.COD=SOLICIT.PER LEFT JOIN USU U ON PER.COD = U.PER" & vbCrLf
sConsulta = sConsulta & "                   IF @EP=0 OR @IDIOMA IS NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN " & vbCrLf
sConsulta = sConsulta & "                     SET @IDIOMA =NULL" & vbCrLf
sConsulta = sConsulta & "                     SET @IDIOMA=(SELECT IDIOMA FROM PARINS_GEST WHERE USU=@USU)" & vbCrLf
sConsulta = sConsulta & "                     IF @IDIOMA IS  NULL   SET @IDIOMA=(SELECT IDIOMA FROM PARGEN_DEF WHERE ID=1)" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "                   SET @SQL = N' SET @SUBJECT= (SELECT TEXT_' + @IDIOMA + N' FROM WEBTEXT WHERE MODULO=101 AND ID=5)'" & vbCrLf
sConsulta = sConsulta & "                   EXEC sp_executesql  @SQL ,N'@SUBJECT VARCHAR(2000) OUTPUT', @SUBJECT=@SUBJECT OUTPUT" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               ELSE" & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                   IF @TYPE=5" & vbCrLf
sConsulta = sConsulta & "                       BEGIN" & vbCrLf
sConsulta = sConsulta & "                       SELECT @EMAIL=PER.EMAIL, @TIPOEMAIL= U.TIPOEMAIL, @USU=U.COD,@IDIOMA=U.IDIOMA,@EP=U.FSEP  FROM PER INNER JOIN SOLICIT ON SOLICIT.ID=@ID AND PER.COD=SOLICIT.PER LEFT JOIN USU U ON PER.COD = U.PER" & vbCrLf
sConsulta = sConsulta & "                       IF @EP=0 OR @IDIOMA IS NULL" & vbCrLf
sConsulta = sConsulta & "                       BEGIN " & vbCrLf
sConsulta = sConsulta & "                          SET @IDIOMA =NULL" & vbCrLf
sConsulta = sConsulta & "                          SET @IDIOMA=(SELECT IDIOMA FROM PARINS_GEST WHERE USU=@USU)" & vbCrLf
sConsulta = sConsulta & "                          IF @IDIOMA IS  NULL   SET @IDIOMA=(SELECT IDIOMA FROM PARGEN_DEF WHERE ID=1)" & vbCrLf
sConsulta = sConsulta & "                       END" & vbCrLf
sConsulta = sConsulta & "                       SET @SQL = N' SET @SUBJECT= (SELECT TEXT_' + @IDIOMA + N' FROM WEBTEXT WHERE MODULO=101 AND ID=6)'" & vbCrLf
sConsulta = sConsulta & "                       EXEC sp_executesql  @SQL ,N'@SUBJECT VARCHAR(2000) OUTPUT', @SUBJECT=@SUBJECT OUTPUT" & vbCrLf
sConsulta = sConsulta & "                       END" & vbCrLf
sConsulta = sConsulta & "                   ELSE" & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                       IF @TYPE=6" & vbCrLf
sConsulta = sConsulta & "                           BEGIN" & vbCrLf
sConsulta = sConsulta & "                           SELECT @EMAIL=PER.EMAIL, @TIPOEMAIL= U.TIPOEMAIL, @USU=U.COD,@IDIOMA=U.IDIOMA,@EP=U.FSEP  FROM PER INNER JOIN SOLICIT ON SOLICIT.ID=@ID AND PER.COD=SOLICIT.COMPRADOR LEFT JOIN USU U ON PER.COD = U.PER" & vbCrLf
sConsulta = sConsulta & "                           IF @EP=0 OR @IDIOMA IS NULL" & vbCrLf
sConsulta = sConsulta & "                           BEGIN " & vbCrLf
sConsulta = sConsulta & "                             SET @IDIOMA =NULL" & vbCrLf
sConsulta = sConsulta & "                             SET @IDIOMA=(SELECT IDIOMA FROM PARINS_GEST WHERE USU=@USU)" & vbCrLf
sConsulta = sConsulta & "                             IF @IDIOMA IS  NULL   SET @IDIOMA=(SELECT IDIOMA FROM PARGEN_DEF WHERE ID=1)" & vbCrLf
sConsulta = sConsulta & "                           END" & vbCrLf
sConsulta = sConsulta & "                           SET @SQL = N' SET @SUBJECT= (SELECT TEXT_' + @IDIOMA + N' FROM WEBTEXT WHERE MODULO=101 AND ID=7)'" & vbCrLf
sConsulta = sConsulta & "                           EXEC sp_executesql  @SQL ,N'@SUBJECT VARCHAR(2000) OUTPUT', @SUBJECT=@SUBJECT OUTPUT" & vbCrLf
sConsulta = sConsulta & "                           END" & vbCrLf
sConsulta = sConsulta & "                       ELSE" & vbCrLf
sConsulta = sConsulta & "                       " & vbCrLf
sConsulta = sConsulta & "                           IF @TYPE=7" & vbCrLf
sConsulta = sConsulta & "                               BEGIN" & vbCrLf
sConsulta = sConsulta & "                               SELECT @EMAIL=PER.EMAIL, @TIPOEMAIL= U.TIPOEMAIL, @USU=U.COD,@IDIOMA=U.IDIOMA,@EP=U.FSEP  FROM PER INNER JOIN SOLICIT ON SOLICIT.ID=@ID AND PER.COD=SOLICIT.COMPRADOR LEFT JOIN USU U ON PER.COD = U.PER" & vbCrLf
sConsulta = sConsulta & "                               IF @EP=0 OR @IDIOMA IS NULL" & vbCrLf
sConsulta = sConsulta & "                               BEGIN " & vbCrLf
sConsulta = sConsulta & "                                 SET @IDIOMA =NULL" & vbCrLf
sConsulta = sConsulta & "                                 SET @IDIOMA=(SELECT IDIOMA FROM PARINS_GEST WHERE USU=@USU)" & vbCrLf
sConsulta = sConsulta & "                                 IF @IDIOMA IS  NULL   SET @IDIOMA=(SELECT IDIOMA FROM PARGEN_DEF WHERE ID=1)" & vbCrLf
sConsulta = sConsulta & "                               END" & vbCrLf
sConsulta = sConsulta & "                               SET @SQL = N' SET @SUBJECT= (SELECT TEXT_' + @IDIOMA + N' FROM WEBTEXT WHERE MODULO=101 AND ID=8)'" & vbCrLf
sConsulta = sConsulta & "                               EXEC sp_executesql  @SQL ,N'@SUBJECT VARCHAR(2000) OUTPUT', @SUBJECT=@SUBJECT OUTPUT" & vbCrLf
sConsulta = sConsulta & "                               END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Private Sub V_2_16_Triggers_4()
Dim sConsulta As String

sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PROCE_OFE_TG_DEL]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PROCE_OFE_TG_DEL]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "CREATE TRIGGER PROCE_OFE_TG_DEL ON dbo.PROCE_OFE " & vbCrLf
sConsulta = sConsulta & "FOR DELETE " & vbCrLf
sConsulta = sConsulta & "AS " & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO AS INTEGER " & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN1 AS VARCHAR(50) " & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCE AS INTEGER " & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVE AS VARCHAR(50) " & vbCrLf
sConsulta = sConsulta & "DECLARE @MAXOFE AS INT " & vbCrLf
sConsulta = sConsulta & "DECLARE @ITEM AS INTEGER " & vbCrLf
sConsulta = sConsulta & "DECLARE @PRECIO AS FLOAT " & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVE2 AS VARCHAR(50) " & vbCrLf
sConsulta = sConsulta & "DECLARE @OFE2 AS SMALLINT " & vbCrLf
sConsulta = sConsulta & "DECLARE @IDENT AS INTEGER " & vbCrLf
sConsulta = sConsulta & "DECLARE @CAMBIOMONCEN FLOAT " & vbCrLf
sConsulta = sConsulta & "DECLARE @NUMOFE AS INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_PROCE_OFE_Del CURSOR FOR SELECT ANYO,GMN1,PROCE,PROVE FROM DELETED " & vbCrLf
sConsulta = sConsulta & "OPEN curTG_PROCE_OFE_Del " & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_OFE_Del INTO @ANYO,@GMN1,@PROCE,@PROVE " & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0 " & vbCrLf
sConsulta = sConsulta & "BEGIN " & vbCrLf
sConsulta = sConsulta & " IF (SELECT SUBASTA FROM PROCE_DEF WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE) = 1 " & vbCrLf
sConsulta = sConsulta & " BEGIN " & vbCrLf
sConsulta = sConsulta & "     --Si existen precios en la oferta eliminada que eran m�nimas en la puja se deber�n recalcular los nuevos precios m�nimos " & vbCrLf
sConsulta = sConsulta & "         /*Obtiene todos los items para los cuales ese proveedor tiene la oferta m�nima*/ " & vbCrLf
sConsulta = sConsulta & "         DECLARE C CURSOR FOR SELECT ITEM FROM OFEMIN WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "         OPEN C " & vbCrLf
sConsulta = sConsulta & "         FETCH NEXT FROM C INTO @ITEM " & vbCrLf
sConsulta = sConsulta & "         WHILE @@FETCH_STATUS=0 " & vbCrLf
sConsulta = sConsulta & "         BEGIN " & vbCrLf
sConsulta = sConsulta & "             SELECT TOP 1 @PRECIO= A.PRECIO / (B.CAMBIO * C.CAMBIO), @PROVE2=A.PROVE,@OFE2=A.NUM" & vbCrLf
sConsulta = sConsulta & "                   FROM ITEM_OFE A INNER JOIN PROCE_OFE B ON A.ANYO=B.ANYO AND A.GMN1=B.GMN1 AND A.PROCE=B.PROCE AND A.PROVE=B.PROVE AND A.NUM=B.OFE" & vbCrLf
sConsulta = sConsulta & "                  INNER JOIN PROCE C ON A.ANYO=C.ANYO AND A.GMN1=C.GMN1 AND A.PROCE = C.COD" & vbCrLf
sConsulta = sConsulta & "             WHERE A.ANYO=@ANYO AND A.GMN1=@GMN1 AND A.PROCE=@PROCE AND A.ITEM=@ITEM AND A.ULT=1 AND A.PRECIO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "             ORDER BY A.PRECIO / (B.CAMBIO * C.CAMBIO) ASC" & vbCrLf
sConsulta = sConsulta & "             IF @PRECIO IS NULL " & vbCrLf
sConsulta = sConsulta & "             BEGIN " & vbCrLf
sConsulta = sConsulta & "                 UPDATE OFEMIN SET PRECIO=NULL, PROVE=NULL, OFE=NULL WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE = @PROCE AND ITEM = @ITEM " & vbCrLf
sConsulta = sConsulta & "             End " & vbCrLf
sConsulta = sConsulta & "             Else " & vbCrLf
sConsulta = sConsulta & "             BEGIN " & vbCrLf
sConsulta = sConsulta & "                 /*Actualiza la tabla OFEMIN con la oferta m�nima para el proceso e item*/ " & vbCrLf
sConsulta = sConsulta & "                 UPDATE OFEMIN SET PRECIO=@PRECIO,PROVE=@PROVE2,OFE=@OFE2, ULTOFE=0 WHERE ANYO=@ANYO  AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM " & vbCrLf
sConsulta = sConsulta & "             End " & vbCrLf
sConsulta = sConsulta & "             FETCH NEXT FROM C INTO @ITEM " & vbCrLf
sConsulta = sConsulta & "         End " & vbCrLf
sConsulta = sConsulta & "         Close C " & vbCrLf
sConsulta = sConsulta & "         DEALLOCATE C " & vbCrLf
sConsulta = sConsulta & "     EXEC SP_CALCULAR_DETALLE_PUJAS @ANYO, @GMN1, @PROCE" & vbCrLf
sConsulta = sConsulta & " END" & vbCrLf
sConsulta = sConsulta & "     SET @NUMOFE=(SELECT MAX(OFE) FROM PROCE_OFE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE AND ENVIADA=1)" & vbCrLf
sConsulta = sConsulta & "     IF (@NUMOFE IS NULL ) " & vbCrLf
sConsulta = sConsulta & "         BEGIN " & vbCrLf
sConsulta = sConsulta & "   UPDATE PROCE SET O=(O-1) WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "         END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "     SET @MAXOFE = ( SELECT MAX(OFE) FROM PROCE_OFE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ENVIADA=1 AND OFE <> -1 ) " & vbCrLf
sConsulta = sConsulta & "     IF (@MAXOFE IS NULL ) " & vbCrLf
sConsulta = sConsulta & "     BEGIN " & vbCrLf
sConsulta = sConsulta & "     /* Si se han insertado ofertas, actualizamos el estado del proceso  si es que este era inferior a 7 */ " & vbCrLf
sConsulta = sConsulta & "         IF (SELECT PROCE.EST FROM PROCE WHERE ANYO=@ANYO  AND GMN1=@GMN1 AND COD=@PROCE) = 7  /* 7= Con ofertas */ " & vbCrLf
sConsulta = sConsulta & "         BEGIN " & vbCrLf
sConsulta = sConsulta & "           IF (SELECT COUNT(*) FROM PROCE_PROVE_PET WHERE ANYO=@ANYO  AND GMN1=@GMN1 AND PROCE=@PROCE) = 0" & vbCrLf
sConsulta = sConsulta & "               UPDATE PROCE SET EST = 5  WHERE ANYO= @ANYO  AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "             ELSE" & vbCrLf
sConsulta = sConsulta & "               UPDATE PROCE SET EST = 6  WHERE ANYO= @ANYO  AND GMN1=@GMN1 AND COD=@PROCE " & vbCrLf
sConsulta = sConsulta & "         End " & vbCrLf
sConsulta = sConsulta & "     End " & vbCrLf
sConsulta = sConsulta & " FETCH NEXT FROM curTG_PROCE_OFE_Del INTO @ANYO,@GMN1,@PROCE,@PROVE " & vbCrLf
sConsulta = sConsulta & "End " & vbCrLf
sConsulta = sConsulta & "Close curTG_PROCE_OFE_Del " & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_PROCE_OFE_Del " & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub


Private Sub V_2_16_Datos_4()
Dim sConsulta As String

Dim oRS As ADODB.Recordset
Dim oADOConnection As ADODB.Connection
Dim sCase As String

Set oADOConnection = New ADODB.Connection
oADOConnection.Open "Provider=SQLOLEDB.1;Auto Translate=FALSE;Server=" & gServer & ";Database=" & gBD & ";", gUID, gPWD
oADOConnection.CursorLocation = adUseClient
oADOConnection.CommandTimeout = 120

sConsulta = "SELECT INSTWEB FROM PARGEN_INTERNO"

Set oRS = New ADODB.Recordset
oRS.Open sConsulta, oADOConnection, adOpenForwardOnly, adLockReadOnly

If oRS(0).Value = 2 Then
    oRS.Close
    oRS.Open "SELECT FSP_SRV, FSP_BD FROM PARGEN_PORT", oADOConnection, adOpenForwardOnly, adLockReadOnly
    sFSP = oRS.Fields("FSP_SRV").Value & "." & oRS.Fields("FSP_BD").Value & ".DBO."
    oRS.Close
        
    
    sConsulta = "SELECT id, cod from IDI"
    
    Set oRS = New ADODB.Recordset
    oRS.Open "EXEC " & sFSP & "SP_EXECUTESQL N'" & sConsulta & "'", oADOConnection, adOpenForwardOnly, adLockReadOnly
    
    
    sCase = "UPDATE CON SET IDI = CASE IDI"
    While Not oRS.eof
        sCase = sCase & " WHEN '" & oRS.Fields("ID").Value & "' THEN '" & oRS.Fields("COD").Value & "' "
        oRS.MoveNext
    Wend
    oRS.Close
    
    sCase = sCase & " ELSE IDI END WHERE IDI IS NOT NULL "
    
    ExecuteSQL gRDOCon, sCase
Else
    oRS.Close
End If

Set oRS = Nothing

    
    
    
    

End Sub



Public Function CodigoDeActualizacion2_16_00_04A2_16_00_05() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim bBool As Boolean

On Error GoTo error
sConsulta = "BEGIN TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = True
ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

frmProgreso.lblDetalle = "Actualizamos storeds"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1


V_2_16_Storeds_5

sConsulta = "UPDATE VERSION SET NUM ='2.16.00.05'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "COMMIT TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = False

CodigoDeActualizacion2_16_00_04A2_16_00_05 = True
Exit Function
    
error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_16_00_04A2_16_00_05 = False

End Function


Private Sub V_2_16_Storeds_5()
Dim sConsulta As String

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_SOLICITUD_DATOS_EMAIL]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_DEVOLVER_PROCE_ATRIB]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_DEVOLVER_PROCE_ATRIB @ANYO smallint, @GMN1 varchar(50), @PROCE smallint, @AMBITO tinyint, @GRUPO varchar(50)=null, @CONLISTA tinyint = 1, @INTERNO tinyint= null, @ATRIB int = null AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CONLISTA = 1" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "   if @AMBITO = 1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "          SELECT PA.ATRIB, PA.ID, DA.COD, DA.DEN, PA.INTRO, DA.TIPO, PA.AMBITO," & vbCrLf
sConsulta = sConsulta & "             PA.MINNUM, PA.MINFEC," & vbCrLf
sConsulta = sConsulta & "             PA.MAXNUM, PA.MAXFEC," & vbCrLf
sConsulta = sConsulta & "             DAL.VALOR_TEXT DALVALOR_TEXT," & vbCrLf
sConsulta = sConsulta & "             DAL.VALOR_NUM DALVALOR_NUM," & vbCrLf
sConsulta = sConsulta & "             DAL.VALOR_FEC DALVALOR_FEC," & vbCrLf
sConsulta = sConsulta & "             PA.INTERNO,PA.POND,PA.DEF_PREC,PA.PREFBAJO," & vbCrLf
sConsulta = sConsulta & "             PA.OBLIG,PA.APLIC_PREC,PA.OP_PREC,PA.GRUPO,PA.USAR_PREC,PA.ORDEN," & vbCrLf
sConsulta = sConsulta & "             PA.OP_POND,PA.NUM_POND,PA.POND_SI,PA.POND_NO, DA.DESCR" & vbCrLf
sConsulta = sConsulta & "          FROM PROCE_ATRIB PA" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN DEF_ATRIB DA  ON PA.ATRIB = DA.ID" & vbCrLf
sConsulta = sConsulta & "           LEFT JOIN PROCE_ATRIB_LISTA DAL   ON PA.ID = DAL.ATRIB_ID" & vbCrLf
sConsulta = sConsulta & "             WHERE PA.ANYO=@ANYO   AND PA.GMN1=@GMN1 AND PA.PROCE=@PROCE  AND PA.AMBITO = 1  " & vbCrLf
sConsulta = sConsulta & "                 AND PA.INTERNO = CASE WHEN @INTERNO IS NULL THEN PA.INTERNO ELSE @INTERNO END" & vbCrLf
sConsulta = sConsulta & "                AND DA.ID = CASE WHEN @ATRIB IS NULL THEN DA.ID ELSE @ATRIB END" & vbCrLf
sConsulta = sConsulta & "           ORDER BY PA.GRUPO,PA.ORDEN_ATRIB" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "     ELSE IF @AMBITO = 2" & vbCrLf
sConsulta = sConsulta & "             IF @GRUPO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                SELECT PA.ATRIB, PA.ID, DA.COD, DA.DEN, PA.INTRO, DA.TIPO, PA.AMBITO," & vbCrLf
sConsulta = sConsulta & "                     PA.MINNUM, PA.MINFEC," & vbCrLf
sConsulta = sConsulta & "                     PA.MAXNUM, PA.MAXFEC," & vbCrLf
sConsulta = sConsulta & "                     DAL.VALOR_TEXT DALVALOR_TEXT," & vbCrLf
sConsulta = sConsulta & "                     DAL.VALOR_NUM DALVALOR_NUM," & vbCrLf
sConsulta = sConsulta & "                     DAL.VALOR_FEC DALVALOR_FEC,   " & vbCrLf
sConsulta = sConsulta & "                     PA.INTERNO,PA.POND,PA.DEF_PREC,PA.PREFBAJO," & vbCrLf
sConsulta = sConsulta & "                     PA.OBLIG,PA.APLIC_PREC,PA.OP_PREC,PA.GRUPO,PA.USAR_PREC,PA.ORDEN," & vbCrLf
sConsulta = sConsulta & "                    PA.OP_POND,PA.NUM_POND,PA.POND_SI,PA.POND_NO, DA.DESCR" & vbCrLf
sConsulta = sConsulta & "               FROM PROCE_ATRIB PA" & vbCrLf
sConsulta = sConsulta & "                   INNER JOIN DEF_ATRIB DA  ON PA.ATRIB = DA.ID" & vbCrLf
sConsulta = sConsulta & "                  LEFT JOIN PROCE_ATRIB_LISTA DAL  ON PA.ID = DAL.ATRIB_ID" & vbCrLf
sConsulta = sConsulta & "              WHERE PA.ANYO=@ANYO  AND PA.GMN1=@GMN1 AND PA.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "                   AND ISNULL(PA.GRUPO,'#') = CASE WHEN PA.GRUPO IS NULL THEN ISNULL(PA.GRUPO,'#') ELSE @GRUPO END" & vbCrLf
sConsulta = sConsulta & "                   AND PA.AMBITO = 2  AND PA.INTERNO = CASE WHEN @INTERNO IS NULL THEN PA.INTERNO ELSE @INTERNO END" & vbCrLf
sConsulta = sConsulta & "                  AND DA.ID = CASE WHEN @ATRIB IS NULL THEN DA.ID ELSE @ATRIB END" & vbCrLf
sConsulta = sConsulta & "               ORDER BY PA.GRUPO,PA.ORDEN_ATRIB" & vbCrLf
sConsulta = sConsulta & "          ELSE" & vbCrLf
sConsulta = sConsulta & "                SELECT PA.ATRIB, PA.ID, DA.COD, DA.DEN, PA.INTRO, DA.TIPO, PA.AMBITO," & vbCrLf
sConsulta = sConsulta & "                    PA.MINNUM, PA.MINFEC," & vbCrLf
sConsulta = sConsulta & "                    PA.MAXNUM, PA.MAXFEC," & vbCrLf
sConsulta = sConsulta & "                    DAL.VALOR_TEXT DALVALOR_TEXT," & vbCrLf
sConsulta = sConsulta & "                   DAL.VALOR_NUM DALVALOR_NUM," & vbCrLf
sConsulta = sConsulta & "                   DAL.VALOR_FEC DALVALOR_FEC,   " & vbCrLf
sConsulta = sConsulta & "                   PA.INTERNO,PA.POND,PA.DEF_PREC,PA.PREFBAJO," & vbCrLf
sConsulta = sConsulta & "                   PA.OBLIG,PA.APLIC_PREC,PA.OP_PREC,PA.GRUPO,PA.USAR_PREC,PA.ORDEN," & vbCrLf
sConsulta = sConsulta & "                   PA.OP_POND,PA.NUM_POND,PA.POND_SI,PA.POND_NO, DA.DESCR" & vbCrLf
sConsulta = sConsulta & "                FROM PROCE_ATRIB PA" & vbCrLf
sConsulta = sConsulta & "                INNER JOIN DEF_ATRIB DA  ON PA.ATRIB = DA.ID" & vbCrLf
sConsulta = sConsulta & "                LEFT JOIN PROCE_ATRIB_LISTA DAL  ON PA.ID = DAL.ATRIB_ID" & vbCrLf
sConsulta = sConsulta & "                 WHERE PA.ANYO=@ANYO  AND PA.GMN1=@GMN1 AND PA.PROCE=@PROCE  AND PA.AMBITO = 2" & vbCrLf
sConsulta = sConsulta & "                      AND PA.INTERNO = CASE WHEN @INTERNO IS NULL THEN PA.INTERNO ELSE @INTERNO END" & vbCrLf
sConsulta = sConsulta & "                     AND DA.ID = CASE WHEN @ATRIB IS NULL THEN DA.ID ELSE @ATRIB END" & vbCrLf
sConsulta = sConsulta & "                 ORDER BY PA.GRUPO,PA.ORDEN_ATRIB" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    ELSE IF @AMBITO = 3" & vbCrLf
sConsulta = sConsulta & "          IF @GRUPO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                SELECT PA.ATRIB, PA.ID, DA.COD, DA.DEN, PA.INTRO, DA.TIPO,  PA.AMBITO," & vbCrLf
sConsulta = sConsulta & "                  PA.MINNUM, PA.MINFEC," & vbCrLf
sConsulta = sConsulta & "                  PA.MAXNUM, PA.MAXFEC," & vbCrLf
sConsulta = sConsulta & "                  DAL.VALOR_TEXT DALVALOR_TEXT," & vbCrLf
sConsulta = sConsulta & "                  DAL.VALOR_NUM DALVALOR_NUM," & vbCrLf
sConsulta = sConsulta & "                  DAL.VALOR_FEC DALVALOR_FEC," & vbCrLf
sConsulta = sConsulta & "                  PA.INTERNO,PA.POND,PA.DEF_PREC,PA.PREFBAJO," & vbCrLf
sConsulta = sConsulta & "                  PA.OBLIG,PA.APLIC_PREC,PA.OP_PREC,PA.GRUPO,PA.USAR_PREC,PA.ORDEN," & vbCrLf
sConsulta = sConsulta & "                  PA.OP_POND,PA.NUM_POND,PA.POND_SI,PA.POND_NO, DA.DESCR" & vbCrLf
sConsulta = sConsulta & "                FROM PROCE_ATRIB PA" & vbCrLf
sConsulta = sConsulta & "                 INNER JOIN DEF_ATRIB DA  ON PA.ATRIB = DA.ID" & vbCrLf
sConsulta = sConsulta & "                 LEFT JOIN PROCE_ATRIB_LISTA DAL ON PA.ID = DAL.ATRIB_ID" & vbCrLf
sConsulta = sConsulta & "                 WHERE PA.ANYO=@ANYO  AND PA.GMN1=@GMN1 AND PA.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "                     AND ISNULL(PA.GRUPO,'#') = CASE WHEN PA.GRUPO IS NULL THEN ISNULL(PA.GRUPO,'#') ELSE @GRUPO END" & vbCrLf
sConsulta = sConsulta & "                     AND PA.AMBITO = 3 AND PA.INTERNO = CASE WHEN @INTERNO IS NULL THEN PA.INTERNO ELSE @INTERNO END" & vbCrLf
sConsulta = sConsulta & "                    AND DA.ID = CASE WHEN @ATRIB IS NULL THEN DA.ID ELSE @ATRIB END" & vbCrLf
sConsulta = sConsulta & "                ORDER BY PA.GRUPO,PA.ORDEN_ATRIB" & vbCrLf
sConsulta = sConsulta & "         ELSE" & vbCrLf
sConsulta = sConsulta & "               SELECT PA.ATRIB, PA.ID, DA.COD, DA.DEN, PA.INTRO, DA.TIPO,  PA.AMBITO," & vbCrLf
sConsulta = sConsulta & "                   PA.MINNUM, PA.MINFEC," & vbCrLf
sConsulta = sConsulta & "                   PA.MAXNUM, PA.MAXFEC," & vbCrLf
sConsulta = sConsulta & "                  DAL.VALOR_TEXT DALVALOR_TEXT," & vbCrLf
sConsulta = sConsulta & "                  DAL.VALOR_NUM DALVALOR_NUM," & vbCrLf
sConsulta = sConsulta & "                  DAL.VALOR_FEC DALVALOR_FEC," & vbCrLf
sConsulta = sConsulta & "                   PA.INTERNO,PA.POND,PA.DEF_PREC,PA.PREFBAJO," & vbCrLf
sConsulta = sConsulta & "                   PA.OBLIG,PA.APLIC_PREC,PA.OP_PREC,PA.GRUPO,PA.USAR_PREC,PA.ORDEN," & vbCrLf
sConsulta = sConsulta & "                   PA.OP_POND,PA.NUM_POND,PA.POND_SI,PA.POND_NO, DA.DESCR" & vbCrLf
sConsulta = sConsulta & "                FROM PROCE_ATRIB PA" & vbCrLf
sConsulta = sConsulta & "                INNER JOIN DEF_ATRIB DA  ON PA.ATRIB = DA.ID" & vbCrLf
sConsulta = sConsulta & "                 LEFT JOIN PROCE_ATRIB_LISTA DAL ON PA.ID = DAL.ATRIB_ID" & vbCrLf
sConsulta = sConsulta & "                WHERE PA.ANYO=@ANYO  AND PA.GMN1=@GMN1 AND PA.PROCE=@PROCE  AND PA.AMBITO = 3" & vbCrLf
sConsulta = sConsulta & "                     AND PA.INTERNO = CASE WHEN @INTERNO IS NULL THEN PA.INTERNO ELSE @INTERNO END" & vbCrLf
sConsulta = sConsulta & "                    AND DA.ID = CASE WHEN @ATRIB IS NULL THEN DA.ID ELSE @ATRIB END" & vbCrLf
sConsulta = sConsulta & "               ORDER BY PA.GRUPO,PA.ORDEN_ATRIB" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "if @AMBITO = 1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT PA.ATRIB, PA.ID, DA.COD, DA.DEN, PA.INTRO, DA.TIPO,  PA.AMBITO," & vbCrLf
sConsulta = sConsulta & "       PA.MINNUM, PA.MINFEC," & vbCrLf
sConsulta = sConsulta & "       PA.MAXNUM, PA.MAXFEC," & vbCrLf
sConsulta = sConsulta & "       NULL DALVALOR_TEXT," & vbCrLf
sConsulta = sConsulta & "       NULL DALVALOR_NUM," & vbCrLf
sConsulta = sConsulta & "       NULL DALVALOR_FEC," & vbCrLf
sConsulta = sConsulta & "       PA.INTERNO,PA.POND,PA.DEF_PREC,PA.PREFBAJO," & vbCrLf
sConsulta = sConsulta & "       PA.OBLIG,PA.APLIC_PREC,PA.OP_PREC,PA.GRUPO,PA.USAR_PREC,PA.ORDEN," & vbCrLf
sConsulta = sConsulta & "       PA.OP_POND,PA.NUM_POND,PA.POND_SI,PA.POND_NO, DA.DESCR" & vbCrLf
sConsulta = sConsulta & " FROM PROCE_ATRIB PA" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN DEF_ATRIB DA" & vbCrLf
sConsulta = sConsulta & "               ON PA.ATRIB = DA.ID" & vbCrLf
sConsulta = sConsulta & "WHERE PA.ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "  AND PA.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "  AND PA.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "  AND PA.AMBITO = 1" & vbCrLf
sConsulta = sConsulta & "  AND PA.INTERNO = CASE WHEN @INTERNO IS NULL THEN PA.INTERNO ELSE @INTERNO END" & vbCrLf
sConsulta = sConsulta & "  AND DA.ID = CASE WHEN @ATRIB IS NULL THEN DA.ID ELSE @ATRIB END" & vbCrLf
sConsulta = sConsulta & "ORDER BY PA.GRUPO,PA.ORDEN_ATRIB" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ELSE IF @AMBITO = 2" & vbCrLf
sConsulta = sConsulta & "  IF @GRUPO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "    SELECT PA.ATRIB, PA.ID, DA.COD, DA.DEN, PA.INTRO, DA.TIPO,  PA.AMBITO," & vbCrLf
sConsulta = sConsulta & "           PA.MINNUM, PA.MINFEC," & vbCrLf
sConsulta = sConsulta & "           PA.MAXNUM, PA.MAXFEC," & vbCrLf
sConsulta = sConsulta & "           NULL DALVALOR_TEXT," & vbCrLf
sConsulta = sConsulta & "           NULL DALVALOR_NUM," & vbCrLf
sConsulta = sConsulta & "           NULL DALVALOR_FEC,   " & vbCrLf
sConsulta = sConsulta & "           PA.INTERNO,PA.POND,PA.DEF_PREC,PA.PREFBAJO," & vbCrLf
sConsulta = sConsulta & "           PA.OBLIG,PA.APLIC_PREC,PA.OP_PREC,PA.GRUPO,PA.USAR_PREC,PA.ORDEN," & vbCrLf
sConsulta = sConsulta & "           PA.OP_POND,PA.NUM_POND,PA.POND_SI,PA.POND_NO, DA.DESCR" & vbCrLf
sConsulta = sConsulta & "     FROM PROCE_ATRIB PA" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN DEF_ATRIB DA" & vbCrLf
sConsulta = sConsulta & "                   ON PA.ATRIB = DA.ID" & vbCrLf
sConsulta = sConsulta & "    WHERE PA.ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "      AND PA.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "      AND PA.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "      AND ISNULL(PA.GRUPO,'#') = CASE WHEN PA.GRUPO IS NULL THEN ISNULL(PA.GRUPO,'#') ELSE @GRUPO END" & vbCrLf
sConsulta = sConsulta & "      AND PA.AMBITO = 2" & vbCrLf
sConsulta = sConsulta & "      AND PA.INTERNO = CASE WHEN @INTERNO IS NULL THEN PA.INTERNO ELSE @INTERNO END" & vbCrLf
sConsulta = sConsulta & "      AND DA.ID = CASE WHEN @ATRIB IS NULL THEN DA.ID ELSE @ATRIB END" & vbCrLf
sConsulta = sConsulta & "    ORDER BY PA.GRUPO,PA.ORDEN_ATRIB" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  ELSE" & vbCrLf
sConsulta = sConsulta & "    SELECT PA.ATRIB, PA.ID, DA.COD, DA.DEN, PA.INTRO, DA.TIPO,  PA.AMBITO," & vbCrLf
sConsulta = sConsulta & "           PA.MINNUM, PA.MINFEC," & vbCrLf
sConsulta = sConsulta & "           PA.MAXNUM, PA.MAXFEC," & vbCrLf
sConsulta = sConsulta & "           NULL DALVALOR_TEXT," & vbCrLf
sConsulta = sConsulta & "           NULL DALVALOR_NUM," & vbCrLf
sConsulta = sConsulta & "           NULL DALVALOR_FEC,   " & vbCrLf
sConsulta = sConsulta & "           PA.INTERNO,PA.POND,PA.DEF_PREC,PA.PREFBAJO," & vbCrLf
sConsulta = sConsulta & "           PA.OBLIG,PA.APLIC_PREC,PA.OP_PREC,PA.GRUPO,PA.USAR_PREC,PA.ORDEN," & vbCrLf
sConsulta = sConsulta & "           PA.OP_POND,PA.NUM_POND,PA.POND_SI,PA.POND_NO, DA.DESCR" & vbCrLf
sConsulta = sConsulta & "     FROM PROCE_ATRIB PA" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN DEF_ATRIB DA" & vbCrLf
sConsulta = sConsulta & "                   ON PA.ATRIB = DA.ID" & vbCrLf
sConsulta = sConsulta & "    WHERE PA.ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "      AND PA.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "      AND PA.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "      AND PA.AMBITO = 2" & vbCrLf
sConsulta = sConsulta & "      AND PA.INTERNO = CASE WHEN @INTERNO IS NULL THEN PA.INTERNO ELSE @INTERNO END" & vbCrLf
sConsulta = sConsulta & "      AND DA.ID = CASE WHEN @ATRIB IS NULL THEN DA.ID ELSE @ATRIB END" & vbCrLf
sConsulta = sConsulta & "    ORDER BY PA.GRUPO,PA.ORDEN_ATRIB" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "ELSE IF @AMBITO = 3" & vbCrLf
sConsulta = sConsulta & "  IF @GRUPO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "    SELECT PA.ATRIB, PA.ID, DA.COD, DA.DEN, PA.INTRO, DA.TIPO,  PA.AMBITO," & vbCrLf
sConsulta = sConsulta & "           PA.MINNUM, PA.MINFEC," & vbCrLf
sConsulta = sConsulta & "           PA.MAXNUM, PA.MAXFEC," & vbCrLf
sConsulta = sConsulta & "           NULL DALVALOR_TEXT," & vbCrLf
sConsulta = sConsulta & "           NULL DALVALOR_NUM," & vbCrLf
sConsulta = sConsulta & "           NULL DALVALOR_FEC," & vbCrLf
sConsulta = sConsulta & "           PA.INTERNO,PA.POND,PA.DEF_PREC,PA.PREFBAJO," & vbCrLf
sConsulta = sConsulta & "           PA.OBLIG,PA.APLIC_PREC,PA.OP_PREC,PA.GRUPO,PA.USAR_PREC,PA.ORDEN," & vbCrLf
sConsulta = sConsulta & "           PA.OP_POND,PA.NUM_POND,PA.POND_SI,PA.POND_NO, DA.DESCR" & vbCrLf
sConsulta = sConsulta & "     FROM PROCE_ATRIB PA" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN DEF_ATRIB DA" & vbCrLf
sConsulta = sConsulta & "                   ON PA.ATRIB = DA.ID" & vbCrLf
sConsulta = sConsulta & "    WHERE PA.ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "      AND PA.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "      AND PA.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "      AND ISNULL(PA.GRUPO,'#') = CASE WHEN PA.GRUPO IS NULL THEN ISNULL(PA.GRUPO,'#') ELSE @GRUPO END" & vbCrLf
sConsulta = sConsulta & "      AND PA.AMBITO = 3" & vbCrLf
sConsulta = sConsulta & "      AND PA.INTERNO = CASE WHEN @INTERNO IS NULL THEN PA.INTERNO ELSE @INTERNO END" & vbCrLf
sConsulta = sConsulta & "      AND DA.ID = CASE WHEN @ATRIB IS NULL THEN DA.ID ELSE @ATRIB END" & vbCrLf
sConsulta = sConsulta & "    ORDER BY PA.GRUPO,PA.ORDEN_ATRIB" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "   SELECT PA.ATRIB, PA.ID, DA.COD, DA.DEN, PA.INTRO, DA.TIPO,  PA.AMBITO," & vbCrLf
sConsulta = sConsulta & "                PA.MINNUM, PA.MINFEC," & vbCrLf
sConsulta = sConsulta & "                PA.MAXNUM, PA.MAXFEC," & vbCrLf
sConsulta = sConsulta & "                NULL DALVALOR_TEXT," & vbCrLf
sConsulta = sConsulta & "                NULL DALVALOR_NUM," & vbCrLf
sConsulta = sConsulta & "                NULL DALVALOR_FEC," & vbCrLf
sConsulta = sConsulta & "                PA.INTERNO,PA.POND,PA.DEF_PREC,PA.PREFBAJO," & vbCrLf
sConsulta = sConsulta & "                PA.OBLIG,PA.APLIC_PREC,PA.OP_PREC,PA.GRUPO,PA.USAR_PREC,PA.ORDEN," & vbCrLf
sConsulta = sConsulta & "                PA.OP_POND,PA.NUM_POND,PA.POND_SI,PA.POND_NO, DA.DESCR" & vbCrLf
sConsulta = sConsulta & "       FROM PROCE_ATRIB PA" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN DEF_ATRIB DA" & vbCrLf
sConsulta = sConsulta & "                   ON PA.ATRIB = DA.ID" & vbCrLf
sConsulta = sConsulta & "       WHERE PA.ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "         AND PA.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "         AND PA.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "         AND PA.AMBITO = 3" & vbCrLf
sConsulta = sConsulta & "               AND PA.INTERNO = CASE WHEN @INTERNO IS NULL THEN PA.INTERNO ELSE @INTERNO END" & vbCrLf
sConsulta = sConsulta & "               AND DA.ID = CASE WHEN @ATRIB IS NULL THEN DA.ID ELSE @ATRIB END" & vbCrLf
sConsulta = sConsulta & "           ORDER BY PA.GRUPO,PA.ORDEN_ATRIB" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PROVE_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[PROVE_COD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE PROVE_COD (@OLD VARCHAR(50),@NEW VARCHAR(50))  AS " & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_COD VARCHAR(50) " & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT ON " & vbCrLf
sConsulta = sConsulta & "BEGIN TRANSACTION " & vbCrLf
sConsulta = sConsulta & "BEGIN " & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR FOR SELECT COD FROM PROVE WHERE COD=@OLD " & vbCrLf
sConsulta = sConsulta & "OPEN C " & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C INTO @VAR_COD " & vbCrLf
sConsulta = sConsulta & "IF @@FETCH_STATUS=0 " & vbCrLf
sConsulta = sConsulta & "BEGIN " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CON NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART1_ADJ NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART2_ADJ NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART3_ADJ NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ADJ NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ADJ NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ADJREU NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OFE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OFE_WEB NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE DETALLE_PUJAS NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFEMIN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_ADJUN NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_WEB_ADJUN NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_WEB NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_PET NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_POT NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_EQP NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN1 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN2 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN3 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN4 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVENOTIF NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ATRIB NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATALOG_LIN NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ORDEN_ENTREGA NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_PEDIDO NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART_UP NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ESP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ESP_PORTAL NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ITEM_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_GR_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ITEM_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_GR_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GRUPO_OFE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GRUPO_OFE_REU NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_CONTR NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATN1_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATN2_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATN3_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATN4_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATN5_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FAVORITOS_ORDEN_ENTREGA NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FAVORITOS_LINEAS_PEDIDO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_EMPRESA_COD_ERP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE SET COD=@NEW WHERE COD=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE CON SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_PROVE SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART1_ADJ SET CODPROVE=@NEW WHERE CODPROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ART2_ADJ SET CODPROVE=@NEW WHERE CODPROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ART3_ADJ SET CODPROVE=@NEW WHERE CODPROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ART4_ADJ SET CODPROVE=@NEW WHERE CODPROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM SET PROVEACT=@NEW WHERE PROVEACT=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_ADJ SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_ADJREU SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_OFE SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_OFE_WEB SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE DETALLE_PUJAS SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFEMIN SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE_ADJUN SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE_WEB_ADJUN SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE_WEB SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE_PET SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE_POT SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_EQP SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_GMN1 SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_GMN2 SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_GMN3 SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_GMN4 SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART1 SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART2 SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART3 SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART4 SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVENOTIF SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE USU SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART4_ATRIB SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CATALOG_LIN SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ORDEN_ENTREGA SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE LINEAS_PEDIDO SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART_UP SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ESP SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ESP_PORTAL SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET PROVEACT=@NEW WHERE PROVEACT=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GRUPO SET PROVEACT=@NEW WHERE PROVEACT=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ALL_PROVE SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_GRUPO_PROVE SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ITEM_PROVE SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_PROCE_PROVE SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_ATRIB SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_ITEM_ATRIB SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_GR_ATRIB SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_ITEM_ADJUN SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_GR_ADJUN SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GRUPO_OFE SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GRUPO_OFE_REU SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_PROVE SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_PROVE SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_CONTR SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CATN1_PROVE  SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CATN2_PROVE  SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CATN3_PROVE  SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CATN4_PROVE  SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CATN5_PROVE  SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART4_ADJUN  SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE FAVORITOS_ORDEN_ENTREGA  SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE FAVORITOS_LINEAS_PEDIDO  SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_EMPRESA_COD_ERP  SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CON CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PROVE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART1_ADJ CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART2_ADJ CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART3_ADJ CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ADJ CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ADJ CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ADJREU CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OFE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OFE_WEB CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_ADJUN CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_WEB_ADJUN CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_WEB CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_PET CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_POT CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_EQP CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN1 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN2 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN3 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN4 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE DETALLE_PUJAS CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFEMIN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ATRIB CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATALOG_LIN CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ORDEN_ENTREGA CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_PEDIDO CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART_UP CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ESP CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ESP_PORTAL CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ITEM_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_GR_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ITEM_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_GR_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GRUPO_OFE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GRUPO_OFE_REU CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_CONTR CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATN1_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATN2_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATN3_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATN4_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATN5_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FAVORITOS_ORDEN_ENTREGA CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FAVORITOS_LINEAS_PEDIDO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_EMPRESA_COD_ERP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "End " & vbCrLf
sConsulta = sConsulta & "Close C " & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C " & vbCrLf
sConsulta = sConsulta & "End " & vbCrLf
sConsulta = sConsulta & "COMMIT TRANSACTION" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT OFF" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PER_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[PER_COD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE PER_COD (@OLD VARCHAR(50),@NEW VARCHAR(50))  AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @EQP VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT ON" & vbCrLf
sConsulta = sConsulta & "BEGIN TRANSACTION" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE D CURSOR FOR SELECT COD,EQP FROM PER WHERE COD=@OLD" & vbCrLf
sConsulta = sConsulta & "OPEN D" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM D INTO @VAR_COD,@EQP" & vbCrLf
sConsulta = sConsulta & "IF @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "/*Ejecuta el store procedure COM_COD*/" & vbCrLf
sConsulta = sConsulta & "IF NOT (@EQP) IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "EXECUTE COM_COD @OLD,@NEW,@EQP" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PER NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FIJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PER NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE APROB_LIM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CAT_PER NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_EST NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE NOTIF_TIPO1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE NOTIF_TIPO2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ORDEN_EST NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PEDIDO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE SEGURIDAD NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE SOLICIT NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE SOLICIT_EST NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE SOLICIT_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE SOLICIT_ASIG_COMP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE SOLICIT_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ORDEN_ENTREGA NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FAVORITOS_ORDEN_ENTREGA NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE PER SET COD=@NEW WHERE COD=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE USU SET PER=@NEW WHERE PER=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE FIJ SET PER=@NEW WHERE PER=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PER SET PER=@NEW WHERE PER=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE APROB_LIM SET PER=@NEW WHERE PER=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CAT_PER SET PER=@NEW WHERE PER=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE LINEAS_EST SET PER=@NEW WHERE PER=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE NOTIF_TIPO1 SET NOTIF=@NEW WHERE NOTIF=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE NOTIF_TIPO2 SET PER=@NEW WHERE PER=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ORDEN_EST SET PER=@NEW WHERE PER=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PEDIDO SET PER=@NEW WHERE PER=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE SEGURIDAD SET APROB_TIPO1=@NEW WHERE APROB_TIPO1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE SOLICIT SET PER=@NEW WHERE PER=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE SOLICIT SET COMPRADOR=@NEW WHERE COMPRADOR=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE SOLICIT_EST SET PER=@NEW WHERE PER=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE SOLICIT_ADJUN SET PER=@NEW WHERE PER=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE SOLICIT_ASIG_COMP SET PER=@NEW WHERE PER=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE SOLICIT_ASIG_COMP SET COM=@NEW WHERE COM=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE SOLICIT_ADJUN SET PER=@NEW WHERE PER=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ORDEN_ENTREGA SET RECEPTOR=@NEW WHERE RECEPTOR=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE FAVORITOS_ORDEN_ENTREGA SET RECEPTOR=@NEW WHERE RECEPTOR=@OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PER CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FIJ CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PER CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE APROB_LIM CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CAT_PER CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_EST CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE NOTIF_TIPO1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE NOTIF_TIPO2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ORDEN_EST CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PEDIDO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE SEGURIDAD CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE SOLICIT CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE SOLICIT_EST CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE SOLICIT_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE SOLICIT_ASIG_COMP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE SOLICIT_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ORDEN_ENTREGA CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FAVORITOS_ORDEN_ENTREGA CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close D" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE D" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT OFF" & vbCrLf
sConsulta = sConsulta & "COMMIT TRANSACTION" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

