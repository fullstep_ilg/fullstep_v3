VERSION 5.00
Object = "{8D650141-6025-11D1-BC40-0000C042AEC0}#3.0#0"; "ssdw3b32.ocx"
Begin VB.Form frmESTRORGDetalle 
   BackColor       =   &H00808000&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Datos de la empresa"
   ClientHeight    =   4320
   ClientLeft      =   2985
   ClientTop       =   4230
   ClientWidth     =   5850
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmESTRORGDetalle.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4320
   ScaleWidth      =   5850
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame fraEmpresa 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      Height          =   3705
      Left            =   120
      TabIndex        =   10
      Top             =   -30
      Width           =   5460
      Begin VB.TextBox txtNif 
         Height          =   285
         Left            =   1755
         MaxLength       =   20
         TabIndex        =   0
         Top             =   285
         Width           =   1665
      End
      Begin VB.TextBox txtDenCia 
         Height          =   285
         Left            =   1755
         MaxLength       =   255
         TabIndex        =   1
         Top             =   750
         Width           =   3615
      End
      Begin VB.TextBox txtDIR 
         Height          =   285
         Left            =   1755
         MaxLength       =   255
         TabIndex        =   2
         Top             =   1215
         Width           =   3615
      End
      Begin VB.TextBox txtCP 
         Height          =   285
         Left            =   1755
         MaxLength       =   20
         TabIndex        =   3
         Top             =   1680
         Width           =   1020
      End
      Begin VB.TextBox txtPOB 
         Height          =   285
         Left            =   1755
         MaxLength       =   100
         TabIndex        =   4
         Top             =   2145
         Width           =   3615
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcPaiDen 
         Height          =   285
         Left            =   1755
         TabIndex        =   5
         Top             =   2610
         Width           =   3615
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   6165
         Columns(0).Caption=   "Denominaci�n"
         Columns(0).Name =   "DEN"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   2117
         Columns(1).Caption=   "Cod"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   6376
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
      End
      Begin SSDataWidgets_B.SSDBCombo sdbcProviDen 
         Height          =   285
         Left            =   1755
         TabIndex        =   6
         Top             =   3075
         Width           =   3615
         DataFieldList   =   "Column 0"
         ListAutoValidate=   0   'False
         ListAutoPosition=   0   'False
         _Version        =   196617
         DataMode        =   2
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColorEven   =   0
         BackColorOdd    =   16777215
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   6165
         Columns(0).Caption=   "Denominaci�n"
         Columns(0).Name =   "DEN"
         Columns(0).DataField=   "Column 0"
         Columns(0).DataType=   8
         Columns(0).FieldLen=   256
         Columns(1).Width=   2117
         Columns(1).Caption=   "Cod"
         Columns(1).Name =   "COD"
         Columns(1).DataField=   "Column 1"
         Columns(1).DataType=   8
         Columns(1).FieldLen=   256
         _ExtentX        =   6376
         _ExtentY        =   503
         _StockProps     =   93
         BackColor       =   -2147483643
      End
      Begin VB.Label lblDenCia 
         BackColor       =   &H00808000&
         Caption         =   "(*) Raz�n social:"
         ForeColor       =   &H00FFFFFF&
         Height          =   225
         Left            =   240
         TabIndex        =   17
         Top             =   780
         Width           =   1500
      End
      Begin VB.Label lblDir 
         BackColor       =   &H00808000&
         Caption         =   "Direcci�n:"
         ForeColor       =   &H00FFFFFF&
         Height          =   225
         Left            =   240
         TabIndex        =   16
         Top             =   1245
         Width           =   1500
      End
      Begin VB.Label lblCP 
         BackColor       =   &H00808000&
         Caption         =   "C�digo postal:"
         ForeColor       =   &H00FFFFFF&
         Height          =   225
         Left            =   240
         TabIndex        =   15
         Top             =   1710
         Width           =   1500
      End
      Begin VB.Label lblPob 
         BackColor       =   &H00808000&
         Caption         =   "Poblaci�n:"
         ForeColor       =   &H00FFFFFF&
         Height          =   225
         Left            =   240
         TabIndex        =   14
         Top             =   2175
         Width           =   1500
      End
      Begin VB.Label lblProvi 
         BackColor       =   &H00808000&
         Caption         =   "Provincia:"
         ForeColor       =   &H00FFFFFF&
         Height          =   225
         Left            =   240
         TabIndex        =   13
         Top             =   3105
         Width           =   1500
      End
      Begin VB.Label lblPai 
         BackColor       =   &H00808000&
         Caption         =   "Pa�s:"
         ForeColor       =   &H00FFFFFF&
         Height          =   225
         Left            =   240
         TabIndex        =   12
         Top             =   2640
         Width           =   1500
      End
      Begin VB.Label lblCIF 
         BackColor       =   &H00808000&
         Caption         =   "(*) CIF:"
         ForeColor       =   &H00FFFFFF&
         Height          =   225
         Left            =   240
         TabIndex        =   11
         Top             =   315
         Width           =   1500
      End
   End
   Begin VB.PictureBox picEdit 
      BackColor       =   &H00808000&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Left            =   120
      ScaleHeight     =   420
      ScaleWidth      =   5460
      TabIndex        =   9
      Top             =   3765
      Width           =   5460
      Begin VB.CommandButton cmdCancelar 
         BackColor       =   &H00C9D2D6&
         Caption         =   "Cancelar"
         CausesValidation=   0   'False
         Height          =   315
         Left            =   120
         TabIndex        =   8
         Top             =   60
         Width           =   1005
      End
      Begin VB.CommandButton cmdAceptar 
         BackColor       =   &H00C9D2D6&
         Caption         =   "&Finalizar"
         Default         =   -1  'True
         Height          =   315
         Left            =   4410
         TabIndex        =   7
         Top             =   60
         Width           =   1005
      End
   End
End
Attribute VB_Name = "frmESTRORGDetalle"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit


Private m_bPaiRespetarCombo As Boolean
Private m_bPaiCargarComboDesde As Boolean
Private m_bNIFCargarComboDesde As Boolean
Private m_bProviRespetarCombo As Boolean
Private m_bProviCargarComboDesde As Boolean
Private m_bNIFRespetarCombo As Boolean

Private m_oEmpresas As CEmpresas
Private moEmpresaSeleccionada As CEmpresa

Private sIdiCod As String
Private sIdiDen As String
Private sIdiNif As String
Private sIdiRazonSocial As String

Private m_oPaisSeleccionado As CPais
Private m_oPaises As CPaises




Private Sub cmdAceptar_Click()
Dim oUON1 As CUnidadOrgNivel1
Dim oUON2 As CUnidadOrgNivel2
Dim oUON3 As CUnidadOrgNivel3
Dim tesError As Boolean
Screen.MousePointer = vbHourglass





If Me.txtNif.Text = "" Then
    MsgBox "Dato obligatorio: NIF"
    Screen.MousePointer = vbNormal
    Exit Sub
End If
If Me.txtDenCia.Text = "" Then
    MsgBox "Dato obligatorio: Raz�n social"
    Screen.MousePointer = vbNormal
    Exit Sub
End If


Set moEmpresaSeleccionada = oFSGSRaiz.Generar_CEmpresa


    With moEmpresaSeleccionada
        .Nif = Me.txtNif.Text
        .Den = Me.txtDenCia.Text
        .CP = Me.txtCP.Text
        .Poblacion = Me.txtPOB.Text
        .Direccion = Me.txtDIR.Text
        If Me.sdbcPaiDen.Text <> "" Then
            .CodPais = Me.sdbcPaiDen.Columns(1).Value
        End If
        If Me.sdbcProviDen.Text <> "" Then
            .CodProvi = Me.sdbcProviDen.Columns(1).Value
        End If
    End With

Select Case frmESTRORG.Accion
    
            
    Case ACCUON1Mod
            
            Set frmESTRORG.oUON1Seleccionada.Empresa = moEmpresaSeleccionada
            
            tesError = frmESTRORG.oUON1Seleccionada.FinalizarEdicionModificando
            
    Case ACCUON2Mod
            Set frmESTRORG.oUON2Seleccionada.Empresa = moEmpresaSeleccionada
            
            tesError = frmESTRORG.oUON2Seleccionada.FinalizarEdicionModificando
    
    Case ACCUON3Mod
            
            Set frmESTRORG.oUON3Seleccionada.Empresa = moEmpresaSeleccionada
            
            tesError = frmESTRORG.oUON3Seleccionada.FinalizarEdicionModificando
            
    
               
End Select


Screen.MousePointer = vbNormal
Set moEmpresaSeleccionada = Nothing

Unload Me
End Sub

Private Sub cmdCancelar_Click()
bCancelado = True
Unload Me
End Sub







Private Sub sdbcPaiDen_Change()

    If Not m_bPaiRespetarCombo Then

        m_bPaiRespetarCombo = True
'        sdbcPaiCod.Text = ""
        m_bPaiRespetarCombo = False
        m_bPaiCargarComboDesde = True
        Set m_oPaisSeleccionado = Nothing

    End If

'    sdbcProviCod = ""
    sdbcProviDen = ""

End Sub

Private Sub sdbcPaiDen_CloseUp()

    If sdbcPaiDen.Value = "..." Then
        sdbcPaiDen.Text = ""
        Exit Sub
    End If

    m_bPaiRespetarCombo = True
'    sdbcPaiCod.Text = sdbcPaiDen.Columns(1).Text
    sdbcPaiDen.Text = sdbcPaiDen.Columns(0).Text
    m_bPaiRespetarCombo = False

    Set m_oPaisSeleccionado = m_oPaises.Item(sdbcPaiDen.Columns(1).Text)

    m_bPaiCargarComboDesde = False

End Sub

Private Sub sdbcPaiDen_DropDown()

    Dim i As Integer
    Dim oPais As CPais
    

    Screen.MousePointer = vbHourglass

    Set m_oPaises = Nothing
    Set m_oPaises = oFSGSRaiz.Generar_CPaises

    sdbcPaiDen.RemoveAll

    If m_bPaiCargarComboDesde Then
        m_oPaises.CargarTodosLosPaisesDesde 5000, , Trim(sdbcPaiDen.Text), True, False
    Else
        m_oPaises.CargarTodosLosPaises , , , True, , False
    End If


    For Each oPais In m_oPaises
        sdbcPaiDen.AddItem oPais.Den & Chr(9) & oPais.Cod
    Next

    If m_bPaiCargarComboDesde And Not m_oPaises.eof Then
        sdbcPaiDen.AddItem "..."
    End If

    sdbcPaiDen.SelStart = 0
    sdbcPaiDen.SelLength = Len(sdbcPaiDen.Text)
'    sdbcPaiCod.Refresh

    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcPaiDen_InitColumnProps()

    sdbcPaiDen.DataFieldList = "Column 0"
    sdbcPaiDen.DataFieldToDisplay = "Column 0"

End Sub

Private Sub sdbcPaiDen_PositionList(ByVal Text As String)

    Dim i As Long
    Dim bm As Variant

    On Error Resume Next

    sdbcPaiDen.MoveFirst

    If Text <> "" Then
        For i = 0 To sdbcPaiDen.Rows - 1
            bm = sdbcPaiDen.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcPaiDen.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcPaiDen.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub


Private Sub sdbcProviDen_Change()

    If Not m_bProviRespetarCombo Then

        m_bProviRespetarCombo = True
'        sdbcProviCod.Text = ""
        m_bProviRespetarCombo = False
        m_bProviCargarComboDesde = True

    End If

End Sub

Private Sub sdbcProviDen_Click()

    If Not sdbcProviDen.DroppedDown Then
'        sdbcProviCod = ""
        sdbcProviDen = ""
    End If

End Sub

Private Sub sdbcProviDen_CloseUp()

    If sdbcProviDen.Value = "..." Then
        sdbcProviDen.Text = ""
        Exit Sub
    End If

    If sdbcProviDen.Value = "" Then Exit Sub

    m_bProviRespetarCombo = True
'    sdbcProviCod.Text = sdbcProviDen.Columns(1).Text
    sdbcProviDen.Text = sdbcProviDen.Columns(0).Text
    m_bProviRespetarCombo = False

    m_bProviCargarComboDesde = False

End Sub
Private Sub sdbcProviDen_DropDown()

    Dim i As Integer
    Dim oProvi As CProvincia

    Screen.MousePointer = vbHourglass

    sdbcProviDen.RemoveAll

    If Not m_oPaisSeleccionado Is Nothing Then

        If m_bProviCargarComboDesde Then
            m_oPaisSeleccionado.CargarTodasLasProvinciasDesde 5000, , Trim(sdbcProviDen.Text), True, False
        Else
            m_oPaisSeleccionado.CargarTodasLasProvincias , , , True, False
        End If


        For Each oProvi In m_oPaisSeleccionado.Provincias
            sdbcProviDen.AddItem oProvi.Den & Chr(9) & oProvi.Cod
        Next

        If m_bProviCargarComboDesde And Not m_oPaisSeleccionado.Provincias.eof Then
            sdbcProviDen.AddItem "..."
        End If

    End If

    sdbcProviDen.SelStart = 0
    sdbcProviDen.SelLength = Len(sdbcProviDen.Text)
'    sdbcProviCod.Refresh

    Screen.MousePointer = vbNormal
End Sub

Private Sub sdbcProviDen_InitColumnProps()

    sdbcProviDen.DataFieldList = "Column 0"
    sdbcProviDen.DataFieldToDisplay = "Column 0"

End Sub

Private Sub sdbcProviDen_PositionList(ByVal Text As String)

    ''' * Objetivo: Posicionarse en el combo segun la seleccion

    Dim i As Long
    Dim bm As Variant

    On Error Resume Next

    sdbcProviDen.MoveFirst

    If Text <> "" Then
        For i = 0 To sdbcProviDen.Rows - 1
            bm = sdbcProviDen.GetBookmark(i)
            If UCase(Text) = UCase(Mid(sdbcProviDen.Columns(0).CellText(bm), 1, Len(Text))) Then
                sdbcProviDen.Bookmark = bm
                Exit For
            End If
        Next i
    End If

End Sub


Private Sub sdbcPaiDen_Validate(Cancel As Boolean)

    Dim m_oPaises As CPaises
    Dim bExiste As Boolean

    Set m_oPaises = oFSGSRaiz.Generar_CPaises

    If sdbcPaiDen.Text = "" Then Exit Sub

    ''' Solo continuamos si existe el pais
    Screen.MousePointer = vbHourglass
    m_oPaises.CargarTodosLosPaises , sdbcPaiDen.Text, True, , False
    Screen.MousePointer = vbNormal

    bExiste = Not (m_oPaises.Count = 0)

    If Not bExiste Then
        sdbcPaiDen.Text = ""
    Else
        m_bPaiRespetarCombo = True
        sdbcPaiDen.Text = m_oPaises.Item(1).Den
        sdbcPaiDen.Columns(0).Value = m_oPaises.Item(1).Den
        sdbcPaiDen.Columns(1).Value = m_oPaises.Item(1).Cod
        m_bPaiRespetarCombo = False

        Set m_oPaisSeleccionado = m_oPaises.Item(1)
        m_bPaiCargarComboDesde = False

    End If

    Set m_oPaises = Nothing


End Sub


Private Sub sdbcProviDen_Validate(Cancel As Boolean)

    Dim bExiste As Boolean

    If sdbcProviDen.Text = "" Then Exit Sub

    ''' Solo continuamos si existe la provincia

    If Not m_oPaisSeleccionado Is Nothing Then

        Screen.MousePointer = vbHourglass
        m_oPaisSeleccionado.CargarTodasLasProvincias , sdbcProviDen.Text, True, , False

        bExiste = Not (m_oPaisSeleccionado.Provincias.Count = 0)
        Screen.MousePointer = vbNormal
    Else

        bExiste = False

    End If


    If Not bExiste Then
        sdbcProviDen.Text = ""
    Else
        m_bProviRespetarCombo = True
        sdbcProviDen.Text = m_oPaisSeleccionado.Provincias.Item(1).Den
        sdbcProviDen.Columns(1).Value = m_oPaisSeleccionado.Provincias.Item(1).Cod
        sdbcProviDen.Columns(0).Value = m_oPaisSeleccionado.Provincias.Item(1).Den

        m_bProviRespetarCombo = False
        m_bProviCargarComboDesde = False
    End If

End Sub


Private Sub LimpiarCampos()
    Me.txtCP.Text = ""
    Me.txtDenCia.Text = ""
    Me.txtDIR.Text = ""
    Me.txtPOB.Text = ""
    m_bPaiRespetarCombo = True
    Me.sdbcPaiDen.Text = ""
    m_bPaiRespetarCombo = False
    m_bProviRespetarCombo = True
    Me.sdbcProviDen.Text = ""
    m_bProviRespetarCombo = False
End Sub
