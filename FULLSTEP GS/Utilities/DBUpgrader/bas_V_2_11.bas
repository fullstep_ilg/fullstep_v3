Attribute VB_Name = "bas_V_2_11"
Option Explicit
Dim oDMOBD As SQLDMO.Database
Dim miBugMS As Integer

Public Function CodigoDeActualizacion2_10_50_04A2_11_00_00() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim bBool As Boolean

On Error GoTo error
    

sConsulta = "BEGIN TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = True
ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

frmProgreso.lblDetalle = "Creamos tablas"
frmProgreso.lblDetalle.Refresh
frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_11_Tablas

frmProgreso.lblDetalle = "Modificamos tablas"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_11_TablasModif

V_2_11_Triggers

frmProgreso.lblDetalle = "Modificamos stored procedures"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_11_Storeds

sConsulta = "UPDATE VERSION SET NUM ='2.11.00.00'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "COMMIT TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = False

CodigoDeActualizacion2_10_50_04A2_11_00_00 = True
Exit Function
    
error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_10_50_04A2_11_00_00 = False

End Function



Private Sub V_2_11_Tablas()
Dim sConsulta As String
'SOBRE
sConsulta = "CREATE TABLE [dbo].[SOBRE] (" & vbCrLf
sConsulta = sConsulta & "   [ANYO] [smallint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [GMN1] [varchar] (" & gLongitudesDeCodigos.giLongCodGMN1 & ") NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [PROCE] [smallint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [SOBRE] [smallint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [FECAPE] [varchar] (50) NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [USUAPER] [varchar] (" & gLongitudesDeCodigos.giLongCodUSU & ") NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [EST] [varchar] (50) NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [FECEST] [datetime] NULL ," & vbCrLf
sConsulta = sConsulta & "   [OBS] [varchar] (2000) NULL ," & vbCrLf
sConsulta = sConsulta & "   [FECACT] [datetime] NULL " & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

 sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[SOBRE]  ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [PK_SOBRE] PRIMARY KEY  CLUSTERED " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [ANYO]," & vbCrLf
sConsulta = sConsulta & "       [GMN1]," & vbCrLf
sConsulta = sConsulta & "       [PROCE]," & vbCrLf
sConsulta = sConsulta & "       [SOBRE]" & vbCrLf
sConsulta = sConsulta & "   )  ON [PRIMARY] " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[SOBRE] ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [FK_SOBRE_PROCE] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [ANYO]," & vbCrLf
sConsulta = sConsulta & "       [GMN1]," & vbCrLf
sConsulta = sConsulta & "       [PROCE]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[PROCE] (" & vbCrLf
sConsulta = sConsulta & "       [ANYO]," & vbCrLf
sConsulta = sConsulta & "       [GMN1]," & vbCrLf
sConsulta = sConsulta & "       [COD]" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER SOBRE_TG_INSUPD ON dbo.SOBRE" & vbCrLf
sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO INT,@GMN1 VARCHAR(50),@PROCE SMALLINT,@SOBRE SMALLINT" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_SOBRE_Ins CURSOR LOCAL FOR SELECT ANYO,GMN1,PROCE,SOBRE FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_SOBRE_Ins" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_SOBRE_Ins INTO @ANYO,@GMN1,@PROCE,@SOBRE" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "UPDATE SOBRE SET FECACT=GETDATE() WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND SOBRE=@SOBRE" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_SOBRE_Ins INTO @ANYO,@GMN1,@PROCE,@SOBRE" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_SOBRE_Ins" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_SOBRE_Ins" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER SOBRE_TG_INS ON dbo.SOBRE" & vbCrLf
sConsulta = sConsulta & "FOR INSERT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN1 AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCE AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @SOBRE AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @COUNT AS INTEGER" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_SOBRE_Ins CURSOR FOR SELECT ANYO,GMN1,PROCE,SOBRE FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_SOBRE_Ins" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_SOBRE_Ins INTO @ANYO,@GMN1,@PROCE,@SOBRE" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "/*Inserta los registros correspondientes en las tablas de configuraci�n de la comparativa*/" & vbCrLf
sConsulta = sConsulta & "SELECT @COUNT =COUNT(*) FROM CONF_VISTAS_PROCE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "IF @COUNT>0" & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "      INSERT INTO CONF_VISTAS_PROCE_SOBRE (ANYO,GMN1,PROCE,USU,VISTA, SOBRE,VISIBLE)" & vbCrLf
sConsulta = sConsulta & "             (SELECT ANYO,GMN1,PROCE,USU,VISTA,@SOBRE,1 FROM CONF_VISTAS_PROCE WHERE ANYO=@ANYO" & vbCrLf
sConsulta = sConsulta & "      AND GMN1=@GMN1 AND PROCE=@PROCE )" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_SOBRE_Ins INTO @ANYO,@GMN1,@PROCE,@SOBRE" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_SOBRE_Ins" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_SOBRE_Ins" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta


'CONF_VISTAS_PROCE_SOBRE
sConsulta = "CREATE TABLE [dbo].[CONF_VISTAS_PROCE_SOBRE] (" & vbCrLf
sConsulta = sConsulta & "   [ANYO] [smallint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [GMN1] [varchar] (" & gLongitudesDeCodigos.giLongCodGMN1 & ") NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [PROCE] [smallint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [USU] [varchar] (" & gLongitudesDeCodigos.giLongCodUSU & ") NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [VISTA] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [SOBRE] [smallint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [VISIBLE] [tinyint] NULL " & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[CONF_VISTAS_PROCE_SOBRE] ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [PK_CONF_VISTAS_PROCE_SOBRE] PRIMARY KEY  CLUSTERED " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [ANYO]," & vbCrLf
sConsulta = sConsulta & "       [GMN1]," & vbCrLf
sConsulta = sConsulta & "       [PROCE]," & vbCrLf
sConsulta = sConsulta & "       [USU]," & vbCrLf
sConsulta = sConsulta & "       [VISTA]," & vbCrLf
sConsulta = sConsulta & "       [SOBRE]" & vbCrLf
sConsulta = sConsulta & "   )  ON [PRIMARY] " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[CONF_VISTAS_PROCE_SOBRE] ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_CONF_VISTAS_PROCE_SOBRE_VISIBLE] DEFAULT (0) FOR [VISIBLE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[CONF_VISTAS_PROCE_SOBRE] ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [FK_CONF_VISTAS_PROCE_SOBRE_PROCE] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [ANYO]," & vbCrLf
sConsulta = sConsulta & "       [GMN1]," & vbCrLf
sConsulta = sConsulta & "       [PROCE]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[PROCE] (" & vbCrLf
sConsulta = sConsulta & "       [ANYO]," & vbCrLf
sConsulta = sConsulta & "       [GMN1]," & vbCrLf
sConsulta = sConsulta & "       [COD]" & vbCrLf
sConsulta = sConsulta & "   )," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [FK_CONF_VISTAS_PROCE_SOBRE_SOBRE] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [ANYO]," & vbCrLf
sConsulta = sConsulta & "       [GMN1]," & vbCrLf
sConsulta = sConsulta & "       [PROCE]," & vbCrLf
sConsulta = sConsulta & "       [SOBRE]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[SOBRE] (" & vbCrLf
sConsulta = sConsulta & "       [ANYO]," & vbCrLf
sConsulta = sConsulta & "       [GMN1]," & vbCrLf
sConsulta = sConsulta & "       [PROCE]," & vbCrLf
sConsulta = sConsulta & "       [SOBRE]" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


End Sub

Private Sub V_2_11_TablasModif()
Dim sConsulta As String

'Tabla PARGEN_INTERNO
sConsulta = "  ALTER TABLE [dbo].[PARGEN_INTERNO] ADD "
sConsulta = sConsulta & " [BD_IDIOMAS] [varchar] (50) NOT NULL CONSTRAINT [DF_PARGEN_INTERNO_BD_IDOMAS] DEFAULT 'FSINTCAP.MDB',"
sConsulta = sConsulta & " [ADMIN_PUBLICA] [tinyint] NOT NULL CONSTRAINT [DF_PARGEN_INTERNO_ADMIN_PUBLICA] DEFAULT 0"
ExecuteSQL gRDOCon, sConsulta
'Tabla PARGEN_GEST
sConsulta = "  ALTER TABLE [dbo].[PARGEN_GEST] ADD "
sConsulta = sConsulta & " [PROCE_ADMINPUB] [tinyint] NOT NULL CONSTRAINT [DF_PARGEN_GEST_PROCE_ADMINPUB] DEFAULT 0"
ExecuteSQL gRDOCon, sConsulta
'Tabla PROCE_DEF
sConsulta = "  ALTER TABLE [dbo].[PROCE_DEF] ADD "
sConsulta = sConsulta & " [ADMIN_PUB] [tinyint] NOT NULL CONSTRAINT [DF_PROCE_DEF_ADMIN_PUB] DEFAULT 0,"
sConsulta = sConsulta & " [MAX_OFE] [tinyint] NOT NULL CONSTRAINT [DF_PROCE_DEF_MAX_OFE] DEFAULT 0"
ExecuteSQL gRDOCon, sConsulta

'Tabla PROCE_GRUPO
sConsulta = "  ALTER TABLE [dbo].[PROCE_GRUPO] ADD [SOBRE] [smallint] NULL"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE [dbo].[PROCE_GRUPO] ADD" & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [FK_PROCE_GRUPO_SOBRE] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [ANYO]," & vbCrLf
sConsulta = sConsulta & "       [GMN1]," & vbCrLf
sConsulta = sConsulta & "       [PROCE]," & vbCrLf
sConsulta = sConsulta & "       [SOBRE]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[SOBRE] (" & vbCrLf
sConsulta = sConsulta & "       [ANYO]," & vbCrLf
sConsulta = sConsulta & "       [GMN1]," & vbCrLf
sConsulta = sConsulta & "       [PROCE]," & vbCrLf
sConsulta = sConsulta & "       [SOBRE]" & vbCrLf
sConsulta = sConsulta & "   )"
ExecuteSQL gRDOCon, sConsulta

'Tabla PLANTILLA
sConsulta = "  ALTER TABLE [dbo].[PLANTILLA] ADD [ADMIN_PUB] [tinyint] NOT NULL CONSTRAINT [DF_PLANTILLA_ADMIN_PUB] DEFAULT 0"
ExecuteSQL gRDOCon, sConsulta

'Tabla PLANTILLA_GR
sConsulta = "  ALTER TABLE [dbo].[PLANTILLA_GR] ADD [SOBRE] [smallint] NULL"
ExecuteSQL gRDOCon, sConsulta


End Sub

Private Sub V_2_11_Triggers()
Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PLANTILLA_TG_UPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PLANTILLA_TG_UPD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "CREATE TRIGGER PLANTILLA_TG_UPD ON dbo.PLANTILLA" & vbCrLf
sConsulta = sConsulta & "FOR UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @ADMIN AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_ATRIB AS INTEGER" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_PLANTILLA_Upd CURSOR FOR SELECT ID,ADMIN_PUB FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_PLANTILLA_Upd" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PLANTILLA_Upd INTO @ID,@ADMIN" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF UPDATE(ADMIN_PUB) AND @ADMIN=0" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "   UPDATE PLANTILLA_GR SET SOBRE=NULL WHERE PLANT=@ID" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF UPDATE(ADMIN_PUB) AND @ADMIN=1" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       DECLARE curTG_ATRIB CURSOR FOR SELECT ID FROM PLANTILLA_ATRIB WHERE PLANT=@ID  AND AMBITO=1" & vbCrLf
sConsulta = sConsulta & "       OPEN curTG_ATRIB" & vbCrLf
sConsulta = sConsulta & "       FETCH NEXT FROM curTG_ATRIB INTO @ID_ATRIB" & vbCrLf
sConsulta = sConsulta & "       WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           DELETE FROM PLANTILLA_ATRIB_LISTA WHERE ATRIB_ID=@ID_ATRIB" & vbCrLf
sConsulta = sConsulta & "           DELETE FROM PLANTILLA_ATRIB_POND WHERE ATRIB_ID=@ID_ATRIB  " & vbCrLf
sConsulta = sConsulta & "           DELETE FROM PLANTILLA_ATRIB WHERE ID=@ID_ATRIB" & vbCrLf
sConsulta = sConsulta & "           FETCH NEXT FROM curTG_ATRIB INTO @ID_ATRIB" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "       Close curTG_ATRIB" & vbCrLf
sConsulta = sConsulta & "       DEALLOCATE curTG_ATRIB" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "_FETCH:" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PLANTILLA_Upd INTO @ID,@ADMIN" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_PLANTILLA_Upd" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_PLANTILLA_Upd" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
End Sub
Private Sub V_2_11_Storeds()
Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GMN1_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[GMN1_COD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE GMN1_COD (@OLD VARCHAR(50),@NEW VARCHAR(50))  AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT ON" & vbCrLf
sConsulta = sConsulta & "BEGIN TRANSACTION" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR FOR SELECT COD FROM GMN1 WHERE COD=@OLD" & vbCrLf
sConsulta = sConsulta & "OPEN C" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C INTO @VAR_COD" & vbCrLf
sConsulta = sConsulta & "IF @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART1_ADJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART2_ADJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART3_ADJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ADJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE DETALLE_PUJAS NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ADJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ADJREU NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ESP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OBJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OFE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OFE_WEB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRES NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESCON NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESPROY NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESCON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESCON4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFEMIN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_GRUPO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_ESP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_WEB_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_WEB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PER NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_PET NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_POT NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVENOTIF NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE REU_PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ESP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4_ACT NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATALOG_LIN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_PEDIDO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART_UP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_DEF NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO_DEF NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO_ESP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_GR_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_GR_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ITEM_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ITEM_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GRUPO_OFE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GRUPO_OFE_REU NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USAR_GR_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_CONTR NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE SOBRE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN1 SET COD=@NEW WHERE COD=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART1_ADJ SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART2_ADJ SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART3_ADJ SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4_ADJ SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE BLOQUEO_PROCE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE COM_GMN1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE COM_GMN2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE COM_GMN3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE COM_GMN4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE DETALLE_PUJAS SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_ADJ SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_ADJREU SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_ESP SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_OBJ SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_OFE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_OFE_WEB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_PRES SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_PRESCON SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_PRESCON3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_PRESCON4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_PRESPROY SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_UON1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_UON2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_UON3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFEMIN SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES_MAT1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES_MAT2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES_MAT3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES_MAT4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_ESP SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE_ADJUN SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE_WEB_ADJUN SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE_WEB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PER SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE_PET SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE_POT SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_GMN1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_GMN2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_GMN3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_GMN4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVENOTIF SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE REU_PROCE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_PROCE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_PROVE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_GRUPO SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_UON1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_UON2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_UON3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_PRESCON3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_PRESCON4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_PROVE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4_ESP  SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN4_ACT SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART4_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN4_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4_ATRIB  SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CATALOG_LIN  SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE LINEAS_PEDIDO  SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART_UP  SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_DEF SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GRUPO SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GRUPO_DEF SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GRUPO_ESP SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PRES1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PRES2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PRES3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PRES4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_PRES1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_PRES2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_PRES3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_PRES4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_UON1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_UON2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_UON3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_UON1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_UON2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_UON3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_GR_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_GR_ADJUN SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_ITEM_ADJUN SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_ITEM_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ALL SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ALL_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ALL_PROVE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_GRUPO SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_GRUPO_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_GRUPO_PROVE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ITEM SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ITEM_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ITEM_PROVE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_PROCE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_PROCE_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_PROCE_PROVE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GRUPO_OFE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GRUPO_OFE_REU SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE USAR_GR_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_CONTR SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE SOBRE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART1_ADJ CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART2_ADJ CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART3_ADJ CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ADJ CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE DETALLE_PUJAS CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ADJ CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ADJREU CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ESP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OBJ CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OFE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OFE_WEB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRES CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESCON CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESCON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESCON4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESPROY CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFEMIN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_ESP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_WEB_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_WEB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PER CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_PET CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_POT CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVENOTIF CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE REU_PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_GRUPO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ESP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4_ACT CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATALOG_LIN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_PEDIDO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART_UP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_DEF CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO_DEF CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO_ESP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_GR_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_GR_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ITEM_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ITEM_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GRUPO_OFE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GRUPO_OFE_REU CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USAR_GR_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_CONTR CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE SOBRE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close C" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "COMMIT TRANSACTION" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT OFF" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PROCE_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[PROCE_COD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "CREATE PROCEDURE PROCE_COD (@OLD SMALLINT,@NEW SMALLINT,@ANYO INT,@GMN1 VARCHAR (50))  AS " & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_COD SMALLINT " & vbCrLf
sConsulta = sConsulta & "DECLARE @AN AS INT " & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN AS VARCHAR(50) " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN " & vbCrLf
sConsulta = sConsulta & "DECLARE D CURSOR FOR SELECT ANYO,GMN1,COD FROM PROCE WHERE ANYO=@ANYO AND COD=@OLD AND GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "OPEN D " & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM D INTO @AN,@GMN,@VAR_COD " & vbCrLf
sConsulta = sConsulta & "IF @@FETCH_STATUS=0 " & vbCrLf
sConsulta = sConsulta & "BEGIN " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART1_ADJ NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART2_ADJ NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART3_ADJ NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ADJ NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ADJ NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ADJREU NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ESP NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OBJ NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OFE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OFE_WEB NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRES NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESCON NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESCON3 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESCON4 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESPROY NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON1 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON2 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON3 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFEMIN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE DETALLE_PUJAS NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVENOTIF NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATALOG_LIN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_PEDIDO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_ESP NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_ADJUN NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_WEB_ADJUN NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_WEB NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PER NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_PET NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_POT NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE REU_PROCE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES1 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES2 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES3 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES4 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON1 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON2 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON3 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO_ESP NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO_DEF NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES1 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES2 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES3 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES4 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON1 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON2 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON3 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_ATRIB NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_DEF NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL_ATRIB NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL_PROVE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_ATRIB NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_PROVE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_ATRIB NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_PROVE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_ATRIB NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_PROVE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON1 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON2 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON3 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON4 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY1 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY2 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY3 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY4 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON1 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON2 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON3 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON3 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON4 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PROVE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_GRUPO NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROCE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROVE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GRUPO_OFE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GRUPO_OFE_REU NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_GR_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ITEM_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_GR_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ITEM_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USAR_GR_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_CONTR NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE SOBRE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE ART1_ADJ SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ART2_ADJ SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ART3_ADJ SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ART4_ADJ SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_ADJ SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_ADJREU SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_ESP SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_OBJ SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_OFE SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_OFE_WEB SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_PRES SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_UON1 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_UON2 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_UON3 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_PRESCON SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_PRESCON3 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_PRESCON4 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_PRESPROY SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE OFEMIN SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE DETALLE_PUJAS SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVENOTIF SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CATALOG_LIN SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE LINEAS_PEDIDO SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_ESP SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE_ADJUN SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE_WEB_ADJUN SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE_WEB SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PER SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE_PET SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE_POT SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE REU_PROCE SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_PRES1 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_PRES2 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_PRES3 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_PRES4 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_UON1 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_UON2 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_UON3 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GRUPO_ESP SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GRUPO_DEF SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GRUPO SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PRES1 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PRES2 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PRES3 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PRES4 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_UON1 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_UON2 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_UON3 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_ATRIB SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_DEF SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ALL SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ALL_ATRIB SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ALL_PROVE SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_PROCE SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_PROCE_ATRIB SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_PROCE_PROVE SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_GRUPO SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_GRUPO_ATRIB SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_GRUPO_PROVE SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ITEM SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ITEM_ATRIB SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ITEM_PROVE SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON1 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON2 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON3 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON4 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY1 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY2 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY3 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY4 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_1 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_2 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_3 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_4 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_1 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_2 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_3 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_4 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_UON1 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_UON2 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD  " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_UON3 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_PRESCON3 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_PRESCON4 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_PROVE SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_GRUPO SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_PROVE SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_PROCE SET COD=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE GRUPO_OFE SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GRUPO_OFE_REU SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_ATRIB SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_GR_ATRIB SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_ITEM_ATRIB SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_GR_ADJUN SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_ITEM_ADJUN SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GRUPO SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET COD=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE BLOQUEO_PROCE SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE USAR_GR_ATRIB SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_CONTR SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE SOBRE SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART1_ADJ CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART2_ADJ CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART3_ADJ CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ADJ CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ADJ CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESCON CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESCON3 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESCON4 CHECK CONSTRAINT ALL  " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESPROY CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ADJREU CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ESP CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OBJ CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OFE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OFE_WEB CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRES CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON1 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON2 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON3 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFEMIN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE DETALLE_PUJAS CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVENOTIF CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATALOG_LIN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_PEDIDO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_ESP CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_ADJUN CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_WEB_ADJUN CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_WEB CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PER CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_PET CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_POT CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE REU_PROCE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES1 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES2 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES3 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES4 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON1 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON2 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON3 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO_ESP CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO_DEF CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES1 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES2 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES3 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES4 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON1 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON2 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON3 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_ATRIB CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_DEF CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL_ATRIB CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL_PROVE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_ATRIB CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_PROVE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_ATRIB CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_PROVE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_ATRIB CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_PROVE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON1 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON2 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON3 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON4 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY1 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY2 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY3 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY4 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON1 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON2 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON3 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON3 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON4 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PROVE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_GRUPO CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROCE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROVE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GRUPO_OFE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GRUPO_OFE_REU CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_GR_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ITEM_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_GR_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ITEM_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USAR_GR_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_CONTR CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE SOBRE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "End " & vbCrLf
sConsulta = sConsulta & "Close D " & vbCrLf
sConsulta = sConsulta & "DEALLOCATE D " & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_CARGAR_CONFIG_PROCE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_CARGAR_CONFIG_PROCE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_CARGAR_CONFIG_PROCE @ANYO INT, @GMN1 VARCHAR(50), @PROCE INT,@DEFDEST tinyint  = null OUTPUT, @DEFPAG tinyint = null OUTPUT," & vbCrLf
sConsulta = sConsulta & "                                        @DEFFECSUM tinyint  = null OUTPUT, @DEFPROCEESP tinyint  = null OUTPUT, @DEFGRUPOESP  tinyint = null OUTPUT, @DEFITEMESP tinyint = null OUTPUT, " & vbCrLf
sConsulta = sConsulta & "                                        @DEFOFEADJUN tinyint = null OUTPUT, @DEFGRUPOADJUN tinyint = null OUTPUT, @DEFITEMADJUN tinyint = null OUTPUT," & vbCrLf
sConsulta = sConsulta & "                                        @DEFSUBASTA tinyint = null OUTPUT, @DEFSUVERPROVE tinyint = null OUTPUT, @DEFSUVERPUJAS tinyint = null OUTPUT, @DEFSUMINUTOS int = null OUTPUT," & vbCrLf
sConsulta = sConsulta & "                                        @DEFPRECALTER tinyint = null OUTPUT, @DEFCANTMAX tinyint = null OUTPUT, @HAYATRIBUTOS AS tinyint = null OUTPUT, " & vbCrLf
sConsulta = sConsulta & "                                        @CAMBIARMON tinyint = 1 OUTPUT, @ADMINPUB tinyint=null OUTPUT, @NUMMAXOFE int = 0 OUTPUT AS" & vbCrLf
sConsulta = sConsulta & "set nocount on" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCEEST INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @PROCEEST = EST " & vbCrLf
sConsulta = sConsulta & "  FROM PROCE " & vbCrLf
sConsulta = sConsulta & " WHERE ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "   AND GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "   AND COD = @PROCE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @DEFDEST=PD.DEST, @DEFPAG=PD.PAG, @DEFFECSUM=PD.FECSUM, @DEFPROCEESP=PD.PROCE_ESP, @DEFGRUPOESP = PD.GRUPO_ESP, @DEFITEMESP  = PD.ITEM_ESP, " & vbCrLf
sConsulta = sConsulta & "       @DEFOFEADJUN = PD.OFE_ADJUN, @DEFGRUPOADJUN=PD.GRUPO_ADJUN, @DEFITEMADJUN= PD.ITEM_ADJUN, @DEFCANTMAX=PD.SOLCANTMAX, @DEFPRECALTER = PD.PRECALTER, " & vbCrLf
sConsulta = sConsulta & "       @DEFSUBASTA = PD.SUBASTA, @DEFSUVERPROVE = PD.SUBASTAPROVE, @DEFSUVERPUJAS=PD.SUBASTAPUJAS, @DEFSUMINUTOS = PD.SUBASTAESPERA," & vbCrLf
sConsulta = sConsulta & "       @HAYATRIBUTOS = ISNULL(PA.ATRIB,0),@CAMBIARMON=PD.CAMBIARMON, @ADMINPUB = ADMIN_PUB, @NUMMAXOFE = PD.MAX_OFE " & vbCrLf
sConsulta = sConsulta & "  FROM PROCE_DEF PD" & vbCrLf
sConsulta = sConsulta & "          LEFT JOIN (SELECT ANYO, GMN1, PROCE, COUNT(ATRIB ) ATRIB" & vbCrLf
sConsulta = sConsulta & "                        FROM PROCE_ATRIB" & vbCrLf
sConsulta = sConsulta & "                       WHERE ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "                         AND GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "                         AND PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "                         AND AMBITO = 1" & vbCrLf
sConsulta = sConsulta & "                    GROUP BY ANYO, GMN1, PROCE) PA" & vbCrLf
sConsulta = sConsulta & "                  ON PD.ANYO = PA.ANYO" & vbCrLf
sConsulta = sConsulta & "                 AND PD.GMN1 = PA.GMN1" & vbCrLf
sConsulta = sConsulta & "                 AND PD.PROCE = PA.PROCE " & vbCrLf
sConsulta = sConsulta & " WHERE PD.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "   AND PD.GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "   AND PD.PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "  " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "select PG.anyo, PG.gmn1, PG.proce, PG.COD , PG.DEN, COUNT(PA.ATRIB) ATRIB, S.SOBRE, S.USUAPER, S.EST, S.FECAPE, S.FECEST, S.OBS," & vbCrLf
sConsulta = sConsulta & "       sum(case dato when 'ESP' THEN 1 else 0 end) as ESP," & vbCrLf
sConsulta = sConsulta & "       sum(case dato when 'PRES1' THEN 1 ELSE 0 END) AS PRES1," & vbCrLf
sConsulta = sConsulta & "       sum(case dato when 'PRES2' THEN 1 ELSE 0 END) AS PRES2," & vbCrLf
sConsulta = sConsulta & "       sum(case dato when 'PRESANU1' THEN 1 ELSE 0 END) AS PRESANU1," & vbCrLf
sConsulta = sConsulta & "       sum(case dato when 'PRESANU2' THEN 1 ELSE 0 END) AS PRESANU2," & vbCrLf
sConsulta = sConsulta & "       sum(case dato when 'DIST' THEN 1 ELSE 0 END) AS DIST," & vbCrLf
sConsulta = sConsulta & "       sum(case dato when 'PROVE' THEN 1 ELSE 0 END) AS PROVE," & vbCrLf
sConsulta = sConsulta & "       sum(case dato when 'FECSUM' THEN 1 ELSE 0 END) AS FECSUM," & vbCrLf
sConsulta = sConsulta & "       sum(case dato when 'PAG' THEN 1 ELSE 0 END) AS PAG," & vbCrLf
sConsulta = sConsulta & "       sum(case dato when 'DEST' THEN 1 ELSE 0 END) AS DEST," & vbCrLf
sConsulta = sConsulta & "       ISNULL(IT.NUMITEMS,0) NUMITEMS" & vbCrLf
sConsulta = sConsulta & "  from PROCE_GRUPO PG " & vbCrLf
sConsulta = sConsulta & "          LEFT JOIN PROCE_GRUPO_DEF PGD" & vbCrLf
sConsulta = sConsulta & "                  ON PG.ANYO = PGD.ANYO" & vbCrLf
sConsulta = sConsulta & "                 AND PG.GMN1 = PGD.GMN1" & vbCrLf
sConsulta = sConsulta & "                 AND PG.PROCE = PGD.PROCE" & vbCrLf
sConsulta = sConsulta & "                 AND PG.COD = PGD.GRUPO" & vbCrLf
sConsulta = sConsulta & "           LEFT JOIN (SELECT ANYO, GMN1, PROCE, ISNULL(GRUPO,'#T#') GRUPO, ATRIB " & vbCrLf
sConsulta = sConsulta & "                        FROM PROCE_ATRIB " & vbCrLf
sConsulta = sConsulta & "                       WHERE ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "                         AND GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "                         AND PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "                         AND AMBITO = 2) PA" & vbCrLf
sConsulta = sConsulta & "                  ON PG.ANYO = PA.ANYO" & vbCrLf
sConsulta = sConsulta & "                 AND PG.GMN1 = PA.GMN1" & vbCrLf
sConsulta = sConsulta & "                 AND PG.PROCE = PA.PROCE" & vbCrLf
sConsulta = sConsulta & "                 AND PG.COD = CASE WHEN PA.GRUPO ='#T#' THEN PG.COD ELSE PA.GRUPO END" & vbCrLf
sConsulta = sConsulta & "          LEFT JOIN (SELECT I.GRUPO, count(I.id) NUMITEMS" & vbCrLf
sConsulta = sConsulta & "                        FROM ITEM I " & vbCrLf
sConsulta = sConsulta & "                      WHERE I.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "                        AND I.GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "                        AND I.PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "                        AND I.CONF = 1" & vbCrLf
sConsulta = sConsulta & "                        AND CASE WHEN I.EST IS NULL THEN -1 ELSE I.EST END = CASE WHEN @PROCEEST=11 THEN 0 ELSE -1 END" & vbCrLf
sConsulta = sConsulta & "                     GROUP BY I.GRUPO) IT" & vbCrLf
sConsulta = sConsulta & "                  ON PG.COD = IT.GRUPO" & vbCrLf
sConsulta = sConsulta & "           LEFT JOIN SOBRE S" & vbCrLf
sConsulta = sConsulta & "                  ON PG.ANYO = S.ANYO" & vbCrLf
sConsulta = sConsulta & "                 AND PG.GMN1 = S.GMN1" & vbCrLf
sConsulta = sConsulta & "                 AND PG.PROCE = S.PROCE" & vbCrLf
sConsulta = sConsulta & "                 AND PG.SOBRE = S.SOBRE" & vbCrLf
sConsulta = sConsulta & "where PG.anyo = @ANYO" & vbCrLf
sConsulta = sConsulta & "  and PG.gmn1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "  and PG.proce = @PROCE" & vbCrLf
sConsulta = sConsulta & "  and isnull(pg.cerrado,0) = 0" & vbCrLf
sConsulta = sConsulta & "group by PG.anyo, PG.gmn1, PG.proce, PG.cod, PG.DEN, IT.NUMITEMS, S.SOBRE, S.USUAPER, S.EST, S.FECAPE, S.FECEST, S.OBS" & vbCrLf
sConsulta = sConsulta & "ORDER BY S.SOBRE,PG.COD" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ELIMINAR_SOBRE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_ELIMINAR_SOBRE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_ELIMINAR_SOBRE (@ANYO SMALLINT,@GMN1 VARCHAR(50),@PROCE SMALLINT,@SOBRE SMALLINT)  AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @GRUPO VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   DELETE FROM CONF_VISTAS_PROCE_SOBRE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND SOBRE=@SOBRE" & vbCrLf
sConsulta = sConsulta & "-- BORRAMOS LOS GRUPOS " & vbCrLf
sConsulta = sConsulta & "   DECLARE G CURSOR LOCAL FOR SELECT COD FROM PROCE_GRUPO WHERE ANYO=@ANYO AND PROCE=@PROCE AND GMN1=@GMN1 AND SOBRE=@SOBRE" & vbCrLf
sConsulta = sConsulta & "   OPEN G" & vbCrLf
sConsulta = sConsulta & "   FETCH NEXT FROM G INTO @GRUPO" & vbCrLf
sConsulta = sConsulta & "   WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "   EXEC SP_ELIMINAR_GRUPO @ANYO, @GMN1, @PROCE, @GRUPO" & vbCrLf
sConsulta = sConsulta & "   FETCH NEXT FROM G INTO @GRUPO" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "   CLOSE G" & vbCrLf
sConsulta = sConsulta & "   DEALLOCATE G" & vbCrLf
sConsulta = sConsulta & "   DELETE FROM SOBRE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND SOBRE=@SOBRE" & vbCrLf
sConsulta = sConsulta & "Return" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 
sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_MODIF_TIPO_PROCESO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_MODIF_TIPO_PROCESO]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_MODIF_TIPO_PROCESO (@ANYO SMALLINT,@GMN1 VARCHAR(50), @PROCE INT,@FECHA varchar(50)=NULL,@EST VARCHAR(50)=NULL,@USU VARCHAR(50)=NULL,@DEST VARCHAR(50) =NULL,@PAG VARCHAR(50)=NULL,@FECLIM DATETIME=NULL, @FECACT DATETIME OUTPUT ) AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @TIPO TINYINT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @TIPO=ADMIN_PUB FROM PROCE_DEF WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "If @TIPO = 1 " & vbCrLf
sConsulta = sConsulta & "--Proceso normal" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "   UPDATE PROCE_GRUPO SET SOBRE=NULL WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   DELETE FROM SOBRE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   UPDATE PROCE_DEF SET ADMIN_PUB=0,MAX_OFE=0 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "Else" & vbCrLf
sConsulta = sConsulta & "--Proceso admin pub" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO SOBRE (ANYO,GMN1,PROCE,SOBRE,FECAPE,USUAPER,EST) VALUES(@ANYO,@GMN1,@PROCE,1,@FECHA,@USU,@EST)" & vbCrLf
sConsulta = sConsulta & "   UPDATE PROCE_GRUPO SET SOBRE=1,DEST=NULL,PAG=NULL,PROVEACT=NULL,SOLICIT=NULL WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE    " & vbCrLf
sConsulta = sConsulta & "   DELETE FROM PROCE_GRUPO_DEF   WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE  AND DATO='DEST'  " & vbCrLf
sConsulta = sConsulta & "   DELETE FROM PROCE_GRUPO_DEF   WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE  AND DATO='PAG'" & vbCrLf
sConsulta = sConsulta & "   DELETE FROM PROCE_GRUPO_DEF   WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE  AND DATO='PROVE'" & vbCrLf
sConsulta = sConsulta & "   DELETE FROM PROCE_GRUPO_DEF   WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE  AND DATO='SOLICIT'" & vbCrLf
sConsulta = sConsulta & "   UPDATE PROCE_DEF SET ADMIN_PUB=1,DEST=1,PAG=1,PROVE=0,SOLICIT=0, OFE_ADJUN=0,SOLCANTMAX=0,PRECALTER=0, " & vbCrLf
sConsulta = sConsulta & "          SUBASTA=0, SUBASTAPROVE=0, SUBASTAPUJAS=0,SUBASTAESPERA=0,CAMBIARMON=0,SUBASTANOTIF=0,MAX_OFE=1  WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   IF (SELECT COUNT(*) FROM PROCE_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND AMBITO=1) >0 " & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       DELETE FROM PROCE_ATRIB_POND  WHERE ATRIB_ID IN (SELECT ID FROM PROCE_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND AMBITO=1)" & vbCrLf
sConsulta = sConsulta & "       DELETE FROM PROCE_ATRIB_LISTA  WHERE ATRIB_ID IN (SELECT ID FROM PROCE_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND AMBITO=1)" & vbCrLf
sConsulta = sConsulta & "       DELETE FROM PROCE_ATRIB  WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND AMBITO=1" & vbCrLf
sConsulta = sConsulta & "       -- USAR_GR_ATRIB  se borra en un trigger de PROCE_aTRIB y las dem�s no existen pq est� en estado < validado" & vbCrLf
sConsulta = sConsulta & "   END " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   UPDATE PROCE SET FECLIMOFE=@FECLIM,DEST=@DEST,PAG=@PAG,PROVEACT=NULL,SOLICIT=NULL,FECINISUB=NULL  WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "SELECT @FECACT=FECACT FROM PROCE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


End Sub


Public Function CodigoDeActualizacion2_11_00_00A2_11_00_01() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim bBool As Boolean

On Error GoTo error
    

sConsulta = "BEGIN TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = True
ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"


frmProgreso.lblDetalle = "Modificamos TABLAS"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_11_TablasModif_1

frmProgreso.lblDetalle = "Modificamos TRIGGERS"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_11_Storeds_01_1

frmProgreso.lblDetalle = "Modificamos stored procedures 1"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_11_Storeds_01_2

frmProgreso.lblDetalle = "Modificamos stored procedures 2"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_11_Storeds_01_3

frmProgreso.lblDetalle = "Modificamos stored procedures 3"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_11_Storeds_01_4

frmProgreso.lblDetalle = "Modificamos stored procedures 4"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_11_Triggers_01

sConsulta = "UPDATE VERSION SET NUM ='2.11.00.01'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "COMMIT TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = False

CodigoDeActualizacion2_11_00_00A2_11_00_01 = True
Exit Function
    
error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_11_00_00A2_11_00_01 = False

End Function



Private Function V_2_11_Storeds_01_1()
Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_CADUCIDAD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_CADUCIDAD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "CREATE PROCEDURE SP_CADUCIDAD AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCE AS INT,@ANYO AS INT,@GMN1 AS VARCHAR(3),@PROVE AS VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @INSTWEB AS INT,@SERVER AS VARCHAR(50),@BD AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @NUM AS INT,@NUMNUE AS INT,@NUMAREV AS INT,@CODPORTAL AS VARCHAR(20),@ID AS SMALLINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @CONEX  AS VARCHAR(100),@FSP_CIA AS VARCHAR(20),@CONEX2 AS VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @LINEA AS FLOAT,@APROV AS INT,@CERRAR AS INT" & vbCrLf
sConsulta = sConsulta & "declare @FECHA AS DATETIME" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @FECHA=convert(char,getdate(),111)" & vbCrLf
sConsulta = sConsulta & "SET @INSTWEB=(SELECT INSTWEB FROM PARGEN_INTERNO)" & vbCrLf
sConsulta = sConsulta & "IF @INSTWEB<>0 " & vbCrLf
sConsulta = sConsulta & "  IF @INSTWEB=2" & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "      SET @SERVER=(SELECT FSP_SRV FROM PARGEN_PORT)" & vbCrLf
sConsulta = sConsulta & "      SET @BD=(SELECT FSP_BD FROM PARGEN_PORT)" & vbCrLf
sConsulta = sConsulta & "      SET @FSP_CIA =(SELECT FSP_CIA FROM PARGEN_PORT WHERE ID=1)" & vbCrLf
sConsulta = sConsulta & "      SET @CONEX2= @SERVER + '.' + @BD + '.dbo.SP_SELECT_ID'" & vbCrLf
sConsulta = sConsulta & "      EXEC @ID = @CONEX2  @FSP_CIA" & vbCrLf
sConsulta = sConsulta & "    END" & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR FOR " & vbCrLf
sConsulta = sConsulta & " SELECT PROCE_PROVE.PROCE,PROCE_PROVE.ANYO,PROCE_PROVE.GMN1,PROCE_PROVE.PROVE " & vbCrLf
sConsulta = sConsulta & "   FROM PROCE_PROVE " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PROCE " & vbCrLf
sConsulta = sConsulta & "                   ON PROCE_PROVE.PROCE= PROCE.COD " & vbCrLf
sConsulta = sConsulta & "                  AND PROCE_PROVE.ANYO=PROCE.ANYO " & vbCrLf
sConsulta = sConsulta & "                  AND PROCE_PROVE.GMN1=PROCE.GMN1 " & vbCrLf
sConsulta = sConsulta & "  WHERE PROCE_PROVE.PUB = 1 " & vbCrLf
sConsulta = sConsulta & "    AND PROCE.FECLIMOFE <= @FECHA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "OPEN C " & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C INTO @PROCE,@ANYO,@GMN1,@PROVE" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    UPDATE PROCE_PROVE SET PUB=0,FECPUB=NULL WHERE PROCE=@PROCE  AND ANYO= @ANYO AND GMN1= @GMN1 AND PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "    IF  @INSTWEB=2" & vbCrLf
sConsulta = sConsulta & "      BEGIN" & vbCrLf
sConsulta = sConsulta & "        SET @CODPORTAL=(SELECT FSP_COD FROM PROVE WHERE COD = @PROVE)" & vbCrLf
sConsulta = sConsulta & "        EXECUTE NUM_PROCE_PUB @PROVE,@NUM OUTPUT,@NUMNUE OUTPUT,@NUMAREV OUTPUT" & vbCrLf
sConsulta = sConsulta & "        IF @NUM IS NULL" & vbCrLf
sConsulta = sConsulta & "          SET @NUM=0" & vbCrLf
sConsulta = sConsulta & "        IF @NUMNUE IS NULL" & vbCrLf
sConsulta = sConsulta & "          SET @NUMNUE=0" & vbCrLf
sConsulta = sConsulta & "        IF @NUMAREV IS NULL" & vbCrLf
sConsulta = sConsulta & "          SET @NUMAREV=0" & vbCrLf
sConsulta = sConsulta & "        SET @CONEX=@SERVER + '.' + @BD + '.dbo.SP_ACT_PROCE_PUB'" & vbCrLf
sConsulta = sConsulta & "        EXECUTE @CONEX @ID,@CODPORTAL,@NUM,@NUMNUE,@NUMAREV" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "    FETCH NEXT FROM C INTO @PROCE,@ANYO,@GMN1,@PROVE" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "CLOSE C" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @APROV=(SELECT APROVISION FROM PARGEN_INTERNO) " & vbCrLf
sConsulta = sConsulta & "IF @APROV<>0" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    SET @CERRAR=1" & vbCrLf
sConsulta = sConsulta & "    DECLARE D CURSOR FOR SELECT ID FROM CATALOG_LIN  WHERE PUB = 1 AND FECHA_DESPUB <= @FECHA" & vbCrLf
sConsulta = sConsulta & "    OPEN D" & vbCrLf
sConsulta = sConsulta & "    FETCH NEXT FROM D INTO @LINEA" & vbCrLf
sConsulta = sConsulta & "    WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "      BEGIN" & vbCrLf
sConsulta = sConsulta & "        UPDATE CATALOG_LIN SET PUB=0,FECHA_DESPUB=NULL WHERE ID=@LINEA" & vbCrLf
sConsulta = sConsulta & "        FETCH NEXT FROM D INTO @LINEA" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "    CLOSE D" & vbCrLf
sConsulta = sConsulta & "    DEALLOCATE D" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 
 
sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_DEVOLVER_DATOS_PERSONA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_DEVOLVER_DATOS_PERSONA]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 
sConsulta = "CREATE PROCEDURE FSEP_DEVOLVER_DATOS_PERSONA(@PER VARCHAR(50))  AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SELECT P.COD, P.NOM, P.APE, P.DEP, P.CAR, P.EMAIL, P.TFNO, P.TFNO2, P.FAX, P.UON1, P.UON2, P.UON3,U1.DEN DENU1, U2.DEN DENU2, U3.DEN DENU3, D.DEN DENDEP" & vbCrLf
sConsulta = sConsulta & "  FROM PER P" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN UON1 U1" & vbCrLf
sConsulta = sConsulta & "            ON P.UON1 =U1.COD" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN UON2 U2" & vbCrLf
sConsulta = sConsulta & "            ON P.UON1 = U2.UON1" & vbCrLf
sConsulta = sConsulta & "           AND P.UON2 = U2.COD" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN UON3 U3" & vbCrLf
sConsulta = sConsulta & "            ON P.UON1 = U3.UON1" & vbCrLf
sConsulta = sConsulta & "           AND P.UON2 = U3.UON2" & vbCrLf
sConsulta = sConsulta & "           AND P.UON3 = U3.COD" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN DEP D" & vbCrLf
sConsulta = sConsulta & "            ON P.DEP = D.COD" & vbCrLf
sConsulta = sConsulta & "        " & vbCrLf
sConsulta = sConsulta & " WHERE P.COD=@PER" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 
sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USU_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[USU_COD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 
sConsulta = "CREATE PROCEDURE USU_COD (@OLD VARCHAR(50),@NEW VARCHAR(50),@PWD VARCHAR(512))  AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT ON" & vbCrLf
sConsulta = sConsulta & "BEGIN TRANSACTION" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR FOR SELECT COD FROM USU WHERE COD=@OLD" & vbCrLf
sConsulta = sConsulta & "OPEN C" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C INTO @VAR_COD" & vbCrLf
sConsulta = sConsulta & "IF @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU_ACC NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU_LIS NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LOG NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_CONTR NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE SOBRE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE USU SET COD=@NEW,PWD=@PWD WHERE COD=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE USU_ACC SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE USU_LIS SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET USUVAL=@NEW WHERE USUVAL=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET USUVALSELPROVE=@NEW WHERE USUVALSELPROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET USUVALCIERRE=@NEW WHERE USUVALCIERRE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET USUAPER=@NEW WHERE USUAPER=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_PROCE SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_PROCE_PROVE SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_PROCE_ATRIB SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_GRUPO SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_GRUPO_PROVE SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_GRUPO_ATRIB SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ITEM SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ITEM_PROVE SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ITEM_ATRIB SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_CONTR SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE SOBRE SET USUAPER=@NEW WHERE USUAPER=@OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU_ACC CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU_LIS CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LOG CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_CONTR CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE SOBRE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close C" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT OFF" & vbCrLf
sConsulta = sConsulta & "COMMIT TRANSACTION" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 
 
sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_CARGAR_CATEGORIAS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_CARGAR_CATEGORIAS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 
sConsulta = "CREATE PROCEDURE FSEP_CARGAR_CATEGORIAS (@PER VARCHAR(40),@NIVEL TINYINT,@CATN1 INT,@CATN2 INT,@CATN3 INT,@CATN4 INT,@CATN5 INT) AS" & vbCrLf
sConsulta = sConsulta & "IF @NIVEL IS NOT NULL AND @NIVEL<>0" & vbCrLf
sConsulta = sConsulta & "  begin" & vbCrLf
sConsulta = sConsulta & "    IF @PER IS NULL OR @PER=''" & vbCrLf
sConsulta = sConsulta & "      BEGIN " & vbCrLf
sConsulta = sConsulta & "        IF @NIVEL=5   " & vbCrLf
sConsulta = sConsulta & "          SELECT COD,DEN,ID AS CAT5,CAT4,CAT3,CAT2,CAT1 FROM CATN5 WHERE BAJALOG=0" & vbCrLf
sConsulta = sConsulta & "        ELSE IF @NIVEL=4" & vbCrLf
sConsulta = sConsulta & "          SELECT COD,DEN, NULL AS CAT5,ID AS CAT4,CAT3,CAT2,CAT1 FROM CATN4 WHERE BAJALOG=0" & vbCrLf
sConsulta = sConsulta & "        ELSE IF @NIVEL=3" & vbCrLf
sConsulta = sConsulta & "          SELECT COD,DEN,NULL AS CAT5,NULL AS CAT4,ID CAT3,CAT2,CAT1 FROM CATN3 WHERE BAJALOG=0" & vbCrLf
sConsulta = sConsulta & "        ELSE IF @NIVEL=2" & vbCrLf
sConsulta = sConsulta & "          SELECT COD,DEN,NULL AS CAT5,NULL AS CAT4,NULL AS CAT3,ID CAT2,CAT1 FROM CATN2 WHERE BAJALOG=0" & vbCrLf
sConsulta = sConsulta & "        ELSE " & vbCrLf
sConsulta = sConsulta & "          SELECT COD,DEN,NULL AS CAT5,NULL AS CAT4,NULL AS CAT3,NULL CAT2,ID AS CAT1 FROM CATN1 WHERE BAJALOG=0" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "    ELSE" & vbCrLf
sConsulta = sConsulta & "      BEGIN" & vbCrLf
sConsulta = sConsulta & "        IF @NIVEL=5    " & vbCrLf
sConsulta = sConsulta & "          SELECT DISTINCT C.COD,C.DEN,C.ID AS CAT5,C.CAT4,C.CAT3,C.CAT2,C.CAT1 FROM CATN5 C " & vbCrLf
sConsulta = sConsulta & "            INNER JOIN CAT_PER P ON C.CAT1=P.CAT1 AND C.CAT2=P.CAT2 AND C.CAT3=P.CAT3 AND C.CAT4=P.CAT4 AND C.ID=P.CAT5 AND P.PER=@PER AND BAJALOG=0" & vbCrLf
sConsulta = sConsulta & "        ELSE IF @NIVEL=4" & vbCrLf
sConsulta = sConsulta & "          SELECT DISTINCT C.COD,C.DEN,NULL AS CAT5,C.ID AS CAT4,C.CAT3,C.CAT2,C.CAT1 FROM CATN4 C " & vbCrLf
sConsulta = sConsulta & "            INNER JOIN CAT_PER P ON C.CAT1=P.CAT1 AND C.CAT2=P.CAT2 AND C.CAT3=P.CAT3 AND C.ID=P.CAT4 AND P.PER=@PER AND BAJALOG=0" & vbCrLf
sConsulta = sConsulta & "        ELSE IF @NIVEL=3    " & vbCrLf
sConsulta = sConsulta & "          SELECT DISTINCT C.COD,C.DEN,NULL AS CAT5,NULL AS CAT4,C.ID AS CAT3,C.CAT2,C.CAT1 FROM CATN3 C " & vbCrLf
sConsulta = sConsulta & "            INNER JOIN CAT_PER P ON C.CAT1=P.CAT1 AND C.CAT2=P.CAT2 AND C.ID=P.CAT3 AND P.PER=@PER AND BAJALOG=0" & vbCrLf
sConsulta = sConsulta & "        ELSE IF @NIVEL=2    " & vbCrLf
sConsulta = sConsulta & "          SELECT DISTINCT C.COD,C.DEN,NULL AS CAT5,NULL AS CAT4,NULL AS CAT3,C.ID AS CAT2,C.CAT1 FROM CATN2 C " & vbCrLf
sConsulta = sConsulta & "            INNER JOIN CAT_PER P ON C.CAT1=P.CAT1 AND C.ID=P.CAT2 AND P.PER=@PER AND BAJALOG=0" & vbCrLf
sConsulta = sConsulta & "        ELSE   " & vbCrLf
sConsulta = sConsulta & "          SELECT DISTINCT C.COD,C.DEN,NULL AS CAT5,NULL AS CAT4,NULL AS CAT3,NULL AS CAT2,C.ID AS CAT1 FROM CATN1 C " & vbCrLf
sConsulta = sConsulta & "            INNER JOIN CAT_PER P ON C.ID=P.CAT1 AND P.PER=@PER AND BAJALOG=0" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  begin" & vbCrLf
sConsulta = sConsulta & "    IF @PER IS NULL OR @PER='' " & vbCrLf
sConsulta = sConsulta & "      BEGIN" & vbCrLf
sConsulta = sConsulta & "        IF @CATN4 IS NOT NULL AND @CATN4<>0" & vbCrLf
sConsulta = sConsulta & "          SELECT COD,DEN,ID AS CAT5,CAT4,CAT3,CAT2,CAT1 FROM CATN5 WHERE CAT1=@CATN1 AND CAT2=@CATN2 AND CAT3=@CATN3 AND CAT4=@CATN4 AND BAJALOG=0" & vbCrLf
sConsulta = sConsulta & "        ELSE IF @CATN3 IS NOT NULL  AND @CATN3<>0" & vbCrLf
sConsulta = sConsulta & "          SELECT COD,DEN,NULL AS CAT5,ID AS CAT4,CAT3,CAT2,CAT1 FROM CATN4 WHERE CAT1=@CATN1 AND CAT2=@CATN2 AND CAT3=@CATN3 AND BAJALOG=0" & vbCrLf
sConsulta = sConsulta & "        ELSE IF @CATN2 IS NOT NULL  AND @CATN2<>0" & vbCrLf
sConsulta = sConsulta & "          SELECT COD,DEN,NULL AS CAT5,NULL AS CAT4,ID AS CAT3,CAT2,CAT1 FROM CATN3 WHERE CAT1=@CATN1 AND CAT2=@CATN2 AND BAJALOG=0 " & vbCrLf
sConsulta = sConsulta & "        ELSE IF @CATN1 IS NOT NULL  AND @CATN1<>0" & vbCrLf
sConsulta = sConsulta & "          SELECT COD,DEN,NULL AS CAT5,NULL AS CAT4,NULL AS CAT3,ID AS CAT2,CAT1 FROM CATN2 WHERE CAT1=@CATN1 AND BAJALOG=0" & vbCrLf
sConsulta = sConsulta & "        ELSE" & vbCrLf
sConsulta = sConsulta & "          SELECT COD,DEN,NULL AS CAT5,NULL AS CAT4,NULL AS CAT3,NULL AS CAT2,ID AS CAT1 FROM CATN1 WHERE BAJALOG=0" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "    ELSE  /*si PER no es nulo*/" & vbCrLf
sConsulta = sConsulta & "      BEGIN" & vbCrLf
sConsulta = sConsulta & "        IF @CATN4 IS NOT NULL  AND @CATN4<>0" & vbCrLf
sConsulta = sConsulta & "          SELECT DISTINCT C.COD,C.DEN,C.ID AS CAT5,C.CAT4,C.CAT3,C.CAT2,C.CAT1 FROM CATN5 C INNER JOIN CAT_PER P  ON C.CAT1=P.CAT1 AND C.CAT2=P.CAT2 AND C.CAT3=P.CAT3 AND C.CAT4=P.CAT4 AND C.ID=P.CAT5" & vbCrLf
sConsulta = sConsulta & "           WHERE C.CAT1=@CATN1 AND C.CAT2=@CATN2 AND   C.CAT3=@CATN3 AND C.CAT4=@CATN4 AND P.PER=@PER AND BAJALOG=0" & vbCrLf
sConsulta = sConsulta & "        ELSE IF @CATN3 IS NOT NULL  AND @CATN3<>0" & vbCrLf
sConsulta = sConsulta & "          SELECT DISTINCT C.COD,C.DEN,NULL AS CAT5,C.ID AS CAT4,C.CAT3,C.CAT2,C.CAT1 FROM CATN4 C INNER JOIN CAT_PER P  ON C.CAT1=P.CAT1 AND C.CAT2=P.CAT2 AND C.CAT3=P.CAT3 AND C.ID=P.CAT4 " & vbCrLf
sConsulta = sConsulta & "           WHERE C.CAT1=@CATN1 AND C.CAT2=@CATN2 AND   C.CAT3=@CATN3 AND P.PER=@PER AND BAJALOG=0" & vbCrLf
sConsulta = sConsulta & "        ELSE IF @CATN2 IS NOT NULL  AND @CATN2<>0" & vbCrLf
sConsulta = sConsulta & "          SELECT DISTINCT C.COD,C.DEN,NULL AS CAT5,NULL AS CAT4,C.ID AS CAT3,C.CAT2,C.CAT1 FROM CATN3 C INNER JOIN CAT_PER P  ON C.CAT1=P.CAT1 AND C.CAT2=P.CAT2 AND C.ID=P.CAT3 "
sConsulta = sConsulta & "           WHERE C.CAT1=@CATN1 AND C.CAT2=@CATN2 AND   P.PER=@PER AND BAJALOG=0" & vbCrLf
sConsulta = sConsulta & "        ELSE IF @CATN1 IS NOT NULL  AND @CATN1<>0" & vbCrLf
sConsulta = sConsulta & "          SELECT DISTINCT C.COD,C.DEN,NULL AS CAT5,NULL AS CAT4,NULL AS CAT3,C.ID AS CAT2,C.CAT1 FROM CATN2 C INNER JOIN CAT_PER P  ON C.CAT1=P.CAT1 AND C.ID=P.CAT2 " & vbCrLf
sConsulta = sConsulta & "           WHERE C.CAT1=@CATN1 AND P.PER=@PER AND BAJALOG=0" & vbCrLf
sConsulta = sConsulta & "        ELSE" & vbCrLf
sConsulta = sConsulta & "          SELECT DISTINCT C.COD,C.DEN,NULL AS CAT5,NULL AS CAT4,NULL AS CAT3,NULL AS CAT2,C.ID AS CAT1 FROM CATN1 C INNER JOIN CAT_PER P  ON C.ID=P.CAT1 WHERE P.PER=@PER AND BAJALOG=0" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "  end" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

 
End Function

Private Sub V_2_11_TablasModif_1()
Dim sConsulta As String

'Tabla PARGEN_GEST
sConsulta = "  ALTER TABLE [dbo].[PARGEN_GEST] ADD "
sConsulta = sConsulta & " [SUBASTA_BAJMINPUJA] [tinyint] NOT NULL CONSTRAINT [DF_PARGEN_GEST_SUBASTA_BAJMINPUJA] DEFAULT 0"
ExecuteSQL gRDOCon, sConsulta
'Tabla PROCE_DEF
sConsulta = "  ALTER TABLE [dbo].[PARGEN_DEF] ADD "
sConsulta = sConsulta & " [SUGERIR_ART] [tinyint] NOT NULL CONSTRAINT [DF_PARGEN_DEF_SUGERIR_ART] DEFAULT 0"
ExecuteSQL gRDOCon, sConsulta
'Tabla PROCE_DEF
sConsulta = "  ALTER TABLE [dbo].[PROCE_DEF] ADD "
sConsulta = sConsulta & " [SUBASTABAJMINPUJA] [tinyint] NOT NULL CONSTRAINT [DF_PROCE_DEF_SUBASTABAJMINPUJA] DEFAULT 0"
ExecuteSQL gRDOCon, sConsulta

End Sub


Private Function V_2_11_Storeds_01_2()
Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_CONTAR_ORDENES]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_CONTAR_ORDENES]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSEP_CONTAR_ORDENES(@APROBADOR VARCHAR(50), @MAXLINEAS INTEGER, @DESDEID INTEGER," & vbCrLf
sConsulta = sConsulta & "@ANYO INTEGER, @NUMPEDIDO INTEGER, @ORDEN INTEGER, @APROBADOS INTEGER,@DENEGADOS INTEGER,@CODPROVE VARCHAR(50),@NUMPEDIDOPROVE VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & ",@DESDEFECHA VARCHAR(10), @APROVISIONADOR VARCHAR(50) )  AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @INDICE INTEGER" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING= 'SELECT DISTINCT ORDEN_ENTREGA.ID FROM LINEAS_PEDIDO INNER JOIN SEGURIDAD" & vbCrLf
sConsulta = sConsulta & "ON LINEAS_PEDIDO.SEGURIDAD=SEGURIDAD.ID AND APROB_TIPO1='''+ @APROBADOR +''' AND LINEAS_PEDIDO.NIVEL_APROB=0'" & vbCrLf
sConsulta = sConsulta & "IF @APROBADOS=0 AND @DENEGADOS <> 0" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND LINEAS_PEDIDO.EST IN (0,20)' " & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & " IF @APROBADOS<>0 AND @DENEGADOS=0" & vbCrLf
sConsulta = sConsulta & "     SET @SQLSTRING= @SQLSTRING + ' AND LINEAS_PEDIDO.EST IN (0,1,2,3)'" & vbCrLf
sConsulta = sConsulta & " ELSE            " & vbCrLf
sConsulta = sConsulta & "     IF @APROBADOS=0 AND @DENEGADOS=0" & vbCrLf
sConsulta = sConsulta & "         SET @SQLSTRING= @SQLSTRING + ' AND LINEAS_PEDIDO.EST =0 '" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING= @SQLSTRING + ' INNER JOIN ORDEN_ENTREGA ON LINEAS_PEDIDO.ORDEN=ORDEN_ENTREGA.ID'" & vbCrLf
sConsulta = sConsulta & "IF @ORDEN<>0" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND ORDEN_ENTREGA.NUM=' +CONVERT(VARCHAR,@ORDEN )" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING= @SQLSTRING + ' INNER JOIN PEDIDO ON ORDEN_ENTREGA.PEDIDO=PEDIDO.ID '" & vbCrLf
sConsulta = sConsulta & "IF @NUMPEDIDO <>0 " & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND PEDIDO.NUM=' +CONVERT(VARCHAR,@NUMPEDIDO )" & vbCrLf
sConsulta = sConsulta & "IF @APROVISIONADOR <>''" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND PEDIDO.PER=''' + @APROVISIONADOR +''' '" & vbCrLf
sConsulta = sConsulta & "if @ANYO<>0" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING=@SQLSTRING + ' AND PEDIDO.ANYO =' + CONVERT(VARCHAR,@ANYO) " & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING=@SQLSTRING + '  INNER JOIN PROVE ON ORDEN_ENTREGA.PROVE=PROVE.COD INNER JOIN PER ON PER.COD=PEDIDO.PER '" & vbCrLf
sConsulta = sConsulta & "IF @DESDEID <> 0 " & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND ORDEN_ENTREGA.ID >=' + CONVERT(VARCHAR,@DESDEID) " & vbCrLf
sConsulta = sConsulta & "IF @CODPROVE <>'' " & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND ORDEN_ENTREGA.PROVE =''' + @CODPROVE + ''''" & vbCrLf
sConsulta = sConsulta & "IF @NUMPEDIDOPROVE <> ''" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND ORDEN_ENTREGA.NUMEXT = ''' + @NUMPEDIDOPROVE + ''''" & vbCrLf
sConsulta = sConsulta & "IF @DESDEFECHA <> ''" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND ORDEN_ENTREGA.FECHA >= CONVERT(DATETIME,''' +  @DESDEFECHA + ''',103)'" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING= @SQLSTRING + ' UNION SELECT DISTINCT ORDEN_ENTREGA.ID FROM LINEAS_PEDIDO INNER JOIN APROB_LIM" & vbCrLf
sConsulta = sConsulta & "ON LINEAS_PEDIDO.APROB_LIM=APROB_LIM.ID AND APROB_LIM.PER='''+ @APROBADOR +''''" & vbCrLf
sConsulta = sConsulta & "IF @APROBADOS=0 AND @DENEGADOS <> 0" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND LINEAS_PEDIDO.EST IN (0,20)' " & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & " IF @APROBADOS<>0 AND @DENEGADOS=0" & vbCrLf
sConsulta = sConsulta & "     SET @SQLSTRING= @SQLSTRING + ' AND LINEAS_PEDIDO.EST IN (0,1,2,3)'" & vbCrLf
sConsulta = sConsulta & " ELSE" & vbCrLf
sConsulta = sConsulta & "     IF @APROBADOS=0 AND @DENEGADOS=0    " & vbCrLf
sConsulta = sConsulta & "         SET @SQLSTRING= @SQLSTRING + ' AND LINEAS_PEDIDO.EST =0 '" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING= @SQLSTRING + ' INNER JOIN ORDEN_ENTREGA ON LINEAS_PEDIDO.ORDEN=ORDEN_ENTREGA.ID '" & vbCrLf
sConsulta = sConsulta & "IF @ORDEN<>0" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND ORDEN_ENTREGA.NUM=' +CONVERT(VARCHAR,@ORDEN )" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING= @SQLSTRING + ' INNER JOIN PEDIDO ON ORDEN_ENTREGA.PEDIDO=PEDIDO.ID '" & vbCrLf
sConsulta = sConsulta & "IF @NUMPEDIDO <>0 " & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND PEDIDO.NUM=' +CONVERT(VARCHAR,@NUMPEDIDO )" & vbCrLf
sConsulta = sConsulta & "IF @APROVISIONADOR <>''" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND PEDIDO.PER=''' + @APROVISIONADOR +''' '" & vbCrLf
sConsulta = sConsulta & "if @ANYO<>0" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING=@SQLSTRING + ' AND PEDIDO.ANYO =' + CONVERT(VARCHAR,@ANYO) " & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING=@SQLSTRING + '  INNER JOIN PROVE ON ORDEN_ENTREGA.PROVE=PROVE.COD INNER JOIN PER ON PER.COD=PEDIDO.PER WHERE 1=1 '" & vbCrLf
sConsulta = sConsulta & "IF @DESDEID <> 0 " & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND ORDEN_ENTREGA.ID >=' + CONVERT(VARCHAR,@DESDEID) " & vbCrLf
sConsulta = sConsulta & "IF @CODPROVE <>''" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND ORDEN_ENTREGA.PROVE =''' + @CODPROVE + ''''" & vbCrLf
sConsulta = sConsulta & "IF @NUMPEDIDOPROVE <> ''" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND ORDEN_ENTREGA.NUMEXT = ''' + @NUMPEDIDOPROVE + ''''" & vbCrLf
sConsulta = sConsulta & "IF @DESDEFECHA <> ''" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND ORDEN_ENTREGA.FECHA >= CONVERT(DATETIME,''' + @DESDEFECHA + ''',103)'" & vbCrLf
sConsulta = sConsulta & "/*PRINT @SQLSTRING*/" & vbCrLf
sConsulta = sConsulta & "EXECUTE sp_executesql @SQLSTRING" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_DEVOLVER_ARTICULOS_CATALOGO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_DEVOLVER_ARTICULOS_CATALOGO]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSEP_DEVOLVER_ARTICULOS_CATALOGO(@COD VARCHAR(40) = NULL, @DEN VARCHAR(300) = NULL, @PROVE VARCHAR(300) = NULL," & vbCrLf
sConsulta = sConsulta & "@CATN1 INTEGER = NULL, @CATN2 INTEGER = NULL, @CATN3 INTEGER = NULL, @CATN4 INTEGER = NULL, " & vbCrLf
sConsulta = sConsulta & "@CATN5 INTEGER = NULL, @ID varchar(200),@PRECIOMAX FLOAT = NULL,@PER VARCHAR(50)," & vbCrLf
sConsulta = sConsulta & "@MAXLINEAS INTEGER,@COINCIDENCIA INTEGER=NULL)  AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PARAM NVARCHAR(1000)" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING='" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT TOP ' + CONVERT(VARCHAR,@MAXLINEAS) +'  " & vbCrLf
sConsulta = sConsulta & "CATALOG_LIN.ID,CATALOG_LIN.ANYO,CATALOG_LIN.PROCE,CATALOG_LIN.ITEM," & vbCrLf
sConsulta = sConsulta & "CATALOG_LIN.PROVE,ART_INT + ITEM.ART COD_ITEM," & vbCrLf
sConsulta = sConsulta & "CASE WHEN ART_AUX.DEN IS NULL THEN ART4.DEN ELSE ART_AUX.DEN END + ITEM.DESCR ART_DEN," & vbCrLf
sConsulta = sConsulta & "CATALOG_LIN.DEST,UC,PROVE_ART4.COD_EXT," & vbCrLf
sConsulta = sConsulta & "CANT_ADJ,PRECIO_UC,UP_DEF,FC_DEF," & vbCrLf
sConsulta = sConsulta & "CANT_MIN_DEF,PUB,FECHA_DESPUB,CAT1,CAT2,CAT3,CAT4,CAT5,CATALOG_LIN.SEGURIDAD," & vbCrLf
sConsulta = sConsulta & "PROVE.DEN  AS PROVEDEN," & vbCrLf
sConsulta = sConsulta & "CATALOG_LIN.GMN1,CATALOG_LIN.GMN2,CATALOG_LIN.GMN3,CATALOG_LIN.GMN4,APROB_LIM.MOD_DEST," & vbCrLf
sConsulta = sConsulta & "CATALOG_LIN.ART_INT + ITEM.ART  + CONVERT(varchar,CATALOG_LIN.ID) '" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING= @SQLSTRING + ' " & vbCrLf
sConsulta = sConsulta & " FROM CATALOG_LIN " & vbCrLf
sConsulta = sConsulta & "         INNER JOIN PROVE " & vbCrLf
sConsulta = sConsulta & "            ON CATALOG_LIN.PROVE=PROVE.COD" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "LEFT JOIN ITEM " & vbCrLf
sConsulta = sConsulta & "       ON CATALOG_LIN.GMN1=ITEM.GMN1 " & vbCrLf
sConsulta = sConsulta & "      AND CATALOG_LIN.ANYO=ITEM.ANYO " & vbCrLf
sConsulta = sConsulta & "      AND CATALOG_LIN.PROCE=ITEM.PROCE " & vbCrLf
sConsulta = sConsulta & "      AND CATALOG_LIN.ITEM=ITEM.ID" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN ART4 " & vbCrLf
sConsulta = sConsulta & "       ON CATALOG_LIN.GMN1=ART4.GMN1 " & vbCrLf
sConsulta = sConsulta & "      AND CATALOG_LIN.GMN2=ART4.GMN2 " & vbCrLf
sConsulta = sConsulta & "      AND CATALOG_LIN.GMN3=ART4.GMN3 " & vbCrLf
sConsulta = sConsulta & "      AND CATALOG_LIN.GMN4=ART4.GMN4 " & vbCrLf
sConsulta = sConsulta & "      AND CATALOG_LIN.ART_INT + ITEM.ART=ART4.COD " & vbCrLf
sConsulta = sConsulta & "LEFT JOIN ART4 ART_AUX " & vbCrLf
sConsulta = sConsulta & "       ON ITEM.GMN1=ART_AUX.GMN1 " & vbCrLf
sConsulta = sConsulta & "      AND ITEM.GMN2=ART_AUX.GMN2 " & vbCrLf
sConsulta = sConsulta & "      AND ITEM.GMN3=ART_AUX.GMN3 " & vbCrLf
sConsulta = sConsulta & "      AND ITEM.GMN4=ART_AUX.GMN4 " & vbCrLf
sConsulta = sConsulta & "      AND ITEM.ART=ART_AUX.COD " & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PROVE_ART4 " & vbCrLf
sConsulta = sConsulta & "       ON CATALOG_LIN.GMN1=PROVE_ART4.GMN1 " & vbCrLf
sConsulta = sConsulta & "      AND CATALOG_LIN.GMN2=PROVE_ART4.GMN2 " & vbCrLf
sConsulta = sConsulta & "      AND CATALOG_LIN.GMN3=PROVE_ART4.GMN3 " & vbCrLf
sConsulta = sConsulta & "      AND CATALOG_LIN.GMN4=PROVE_ART4.GMN4 " & vbCrLf
sConsulta = sConsulta & "      AND CASE WHEN CATALOG_LIN.ART_INT IS NULL THEN ITEM.ART ELSE CATALOG_LIN.ART_INT  END =PROVE_ART4.ART " & vbCrLf
sConsulta = sConsulta & "      AND CATALOG_LIN.PROVE=PROVE_ART4.PROVE" & vbCrLf
sConsulta = sConsulta & "'" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING= @SQLSTRING + ' " & vbCrLf
sConsulta = sConsulta & "          INNER JOIN APROB_LIM " & vbCrLf
sConsulta = sConsulta & "                  ON CATALOG_LIN.SEGURIDAD=APROB_LIM.SEGURIDAD " & vbCrLf
sConsulta = sConsulta & "                 AND APROB_LIM.PER=@PER'" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING= @SQLSTRING +'  " & vbCrLf
sConsulta = sConsulta & " WHERE PUB=1" & vbCrLf
sConsulta = sConsulta & "   AND BAJALOG = 0" & vbCrLf
sConsulta = sConsulta & "'" & vbCrLf
sConsulta = sConsulta & "IF @PROVE is not null" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' " & vbCrLf
sConsulta = sConsulta & "           AND PROVE.COD = @PROVE'" & vbCrLf
sConsulta = sConsulta & "IF @COD is not null" & vbCrLf
sConsulta = sConsulta & " BEGIN" & vbCrLf
sConsulta = sConsulta & "  IF @COINCIDENCIA=0 " & vbCrLf
sConsulta = sConsulta & "   SET @SQLSTRING= @SQLSTRING +' AND (CATALOG_LIN.ART_INT + ITEM.ART LIKE ''% ''  +  @COD  + '' %''' +" & vbCrLf
sConsulta = sConsulta & "   ' OR CATALOG_LIN.ART_INT + ITEM.ART LIKE @COD  + '' %''' +" & vbCrLf
sConsulta = sConsulta & "   ' OR CATALOG_LIN.ART_INT + ITEM.ART LIKE ''% '' + @COD  ' + " & vbCrLf
sConsulta = sConsulta & "   ' OR CATALOG_LIN.ART_INT + ITEM.ART =@COD)' " & vbCrLf
sConsulta = sConsulta & "  ELSE" & vbCrLf
sConsulta = sConsulta & "   SET @SQLSTRING= @SQLSTRING +' AND CATALOG_LIN.ART_INT + ITEM.ART LIKE @COD + ''%'''" & vbCrLf
sConsulta = sConsulta & " END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @DEN is not null" & vbCrLf
sConsulta = sConsulta & " BEGIN" & vbCrLf
sConsulta = sConsulta & "   IF @COINCIDENCIA=0 " & vbCrLf
sConsulta = sConsulta & "   SET @SQLSTRING = @SQLSTRING + ' AND (ITEM.DESCR + ART4.DEN + ART_AUX.DEN LIKE ''% ''  +  @DEN  + '' %''' +" & vbCrLf
sConsulta = sConsulta & "   ' OR ITEM.DESCR + ART4.DEN + ART_AUX.DEN LIKE @DEN  + '' %''' +" & vbCrLf
sConsulta = sConsulta & "   ' OR ITEM.DESCR + ART4.DEN + ART_AUX.DEN LIKE ''% '' + @DEN ' + " & vbCrLf
sConsulta = sConsulta & "   ' OR ITEM.DESCR + ART4.DEN + ART_AUX.DEN=@DEN)'" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "        SET @SQLSTRING = @SQLSTRING + ' AND ITEM.DESCR + ART4.DEN + ART_AUX.DEN LIKE + ''%'' + @DEN + ''%'''" & vbCrLf
sConsulta = sConsulta & " END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CATN1 is not null" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING = @SQLSTRING + ' " & vbCrLf
sConsulta = sConsulta & "   AND CATALOG_LIN.CAT1=@CATN1'" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "IF @CATN2 is not null" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING = @SQLSTRING + ' " & vbCrLf
sConsulta = sConsulta & "   AND CATALOG_LIN.CAT2=@CATN2'" & vbCrLf
sConsulta = sConsulta & "IF @CATN3 is not null" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING = @SQLSTRING + ' " & vbCrLf
sConsulta = sConsulta & "   AND CATALOG_LIN.CAT3=@CATN3'" & vbCrLf
sConsulta = sConsulta & "IF @CATN4 is not null" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING = @SQLSTRING + ' " & vbCrLf
sConsulta = sConsulta & "   AND CATALOG_LIN.CAT4=@CATN4'" & vbCrLf
sConsulta = sConsulta & "IF @CATN5 is not null" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING = @SQLSTRING + '" & vbCrLf
sConsulta = sConsulta & "   AND CATALOG_LIN.CAT5=@CATN5'" & vbCrLf
sConsulta = sConsulta & "IF @PRECIOMAX is not null" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING = @SQLSTRING + '" & vbCrLf
sConsulta = sConsulta & "   AND CATALOG_LIN.PRECIO_UC<=@PRECIOMAX'" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING = @SQLSTRING + '" & vbCrLf
sConsulta = sConsulta & "   AND isnull(catalog_lin.art_int,'''') + isnull(ITEM.ART,'''') +  convert(varchar,isnull(catalog_lin.ID,'''')) >=@ID'" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING = @SQLSTRING + '" & vbCrLf
sConsulta = sConsulta & "ORDER BY CATALOG_LIN.ART_INT + ITEM.ART  + CONVERT(varchar,CATALOG_LIN.ID)'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @PARAM = '@COD VARCHAR(40), @DEN VARCHAR(300), @PROVE VARCHAR(300)," & vbCrLf
sConsulta = sConsulta & "@CATN1 INTEGER, @CATN2 INTEGER, @CATN3 INTEGER, @CATN4 INTEGER, " & vbCrLf
sConsulta = sConsulta & "@CATN5 INTEGER, @PRECIOMAX FLOAT,@PER VARCHAR(50),@ID VARCHAR(200)'" & vbCrLf
sConsulta = sConsulta & "EXECUTE sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "@COD=@COD, " & vbCrLf
sConsulta = sConsulta & "@DEN=@DEN, " & vbCrLf
sConsulta = sConsulta & "@PROVE=@PROVE," & vbCrLf
sConsulta = sConsulta & "@CATN1=@CATN1," & vbCrLf
sConsulta = sConsulta & "@CATN2=@CATN2, " & vbCrLf
sConsulta = sConsulta & "@CATN3=@CATN3," & vbCrLf
sConsulta = sConsulta & "@CATN4=@CATN4," & vbCrLf
sConsulta = sConsulta & "@CATN5=@CATN5, " & vbCrLf
sConsulta = sConsulta & "@PRECIOMAX=@PRECIOMAX, " & vbCrLf
sConsulta = sConsulta & "@PER=@PER," & vbCrLf
sConsulta = sConsulta & "@ID=@ID" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_DEVOLVER_ARTICULOS_SOLICIT]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_DEVOLVER_ARTICULOS_SOLICIT]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSEP_DEVOLVER_ARTICULOS_SOLICIT @ID int AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT case when c5.cod is null then " & vbCrLf
sConsulta = sConsulta & "                     case when c4.cod is null then " & vbCrLf
sConsulta = sConsulta & "                          case when c3.cod is null then" & vbCrLf
sConsulta = sConsulta & "                               case when c2.cod is null then" & vbCrLf
sConsulta = sConsulta & "                                    c1.cod " & vbCrLf
sConsulta = sConsulta & "                               else" & vbCrLf
sConsulta = sConsulta & "                               c1.cod + ' - ' + c2.cod " & vbCrLf
sConsulta = sConsulta & "                               end" & vbCrLf
sConsulta = sConsulta & "                          else" & vbCrLf
sConsulta = sConsulta & "                          c1.cod + ' - ' + c2.cod + ' - ' + c3.cod " & vbCrLf
sConsulta = sConsulta & "                          end" & vbCrLf
sConsulta = sConsulta & "                     else" & vbCrLf
sConsulta = sConsulta & "                     c1.cod + ' - ' + c2.cod + ' - ' + c3.cod + ' - ' + c4.cod" & vbCrLf
sConsulta = sConsulta & "                     end" & vbCrLf
sConsulta = sConsulta & "                 else" & vbCrLf
sConsulta = sConsulta & "                 c1.cod + ' - ' + c2.cod + ' - ' + c3.cod + ' - ' + c4.cod + ' - ' + c5.cod" & vbCrLf
sConsulta = sConsulta & "                 end CAT," & vbCrLf
sConsulta = sConsulta & "                 case when c5.cod is null then " & vbCrLf
sConsulta = sConsulta & "                         case when c4.cod is null then " & vbCrLf
sConsulta = sConsulta & "                              case when c3.cod is null then" & vbCrLf
sConsulta = sConsulta & "                                   case when c2.cod is null then" & vbCrLf
sConsulta = sConsulta & "                                        c1.cod" & vbCrLf
sConsulta = sConsulta & "                                   else" & vbCrLf
sConsulta = sConsulta & "                                   c2.cod" & vbCrLf
sConsulta = sConsulta & "                                   end" & vbCrLf
sConsulta = sConsulta & "                              else" & vbCrLf
sConsulta = sConsulta & "                              c3.cod" & vbCrLf
sConsulta = sConsulta & "                              end" & vbCrLf
sConsulta = sConsulta & "                         else" & vbCrLf
sConsulta = sConsulta & "                         c4.cod" & vbCrLf
sConsulta = sConsulta & "                         end" & vbCrLf
sConsulta = sConsulta & "                     else" & vbCrLf
sConsulta = sConsulta & "                     c5.cod" & vbCrLf
sConsulta = sConsulta & "                 end ULTCAT," & vbCrLf
sConsulta = sConsulta & "                 case when c5.cod is null then " & vbCrLf
sConsulta = sConsulta & "                         case when c4.cod is null then " & vbCrLf
sConsulta = sConsulta & "                              case when c3.cod is null then" & vbCrLf
sConsulta = sConsulta & "                                   case when c2.cod is null then" & vbCrLf
sConsulta = sConsulta & "                                        '(' + c1.cod + ') ' + c1.den" & vbCrLf
sConsulta = sConsulta & "                                   else" & vbCrLf
sConsulta = sConsulta & "                                   '(' + c1.cod + ') ' + c1.den + ' - (' + c2.cod + ') ' + c2.den" & vbCrLf
sConsulta = sConsulta & "                                   end" & vbCrLf
sConsulta = sConsulta & "                              else" & vbCrLf
sConsulta = sConsulta & "                              '(' + c1.cod + ') ' + c1.den + ' - (' + c2.cod + ') ' + c2.den + ' - (' + c3.cod + ') ' + c3.den" & vbCrLf
sConsulta = sConsulta & "                              end" & vbCrLf
sConsulta = sConsulta & "                         else" & vbCrLf
sConsulta = sConsulta & "                         '(' + c1.cod + ') ' + c1.den + ' - (' + c2.cod + ') ' + c2.den + ' - (' + c3.cod + ') ' + c3.den + ' - (' + c4.cod + ') ' + c4.den" & vbCrLf
sConsulta = sConsulta & "                         end" & vbCrLf
sConsulta = sConsulta & "                     else" & vbCrLf
sConsulta = sConsulta & "                     '(' + c1.cod + ') ' + c1.den + ' - (' + c2.cod + ') ' + c2.den + ' - (' + c3.cod + ') ' + c3.den + ' - (' + c4.cod + ') ' + c4.den + ' - (' + c5.cod + ') ' + c5.den" & vbCrLf
sConsulta = sConsulta & "                 end CATEGORIARAMA," & vbCrLf
sConsulta = sConsulta & "                 CL.ART_INT + I.ART CODART," & vbCrLf
sConsulta = sConsulta & "                 A4.DEN + I.DESCR  + ART_AUX.DEN DENART," & vbCrLf
sConsulta = sConsulta & "                 PRECIO_UC PRECIO," & vbCrLf
sConsulta = sConsulta & "                 UC," & vbCrLf
sConsulta = sConsulta & "                 FECHA_DESPUB," & vbCrLf
sConsulta = sConsulta & "                 CL.PROVE," & vbCrLf
sConsulta = sConsulta & "                 CL.GMN1," & vbCrLf
sConsulta = sConsulta & "                 CL.GMN2," & vbCrLf
sConsulta = sConsulta & "                 CL.GMN3," & vbCrLf
sConsulta = sConsulta & "                 CL.GMN4," & vbCrLf
sConsulta = sConsulta & "                 PA.COD_EXT," & vbCrLf
sConsulta = sConsulta & "        CL.PUB,CL.DEST,CL.CANT_ADJ,CL.UP_DEF,CL.FC_DEF,CL.CANT_MIN_DEF,CL.ANYO,CL.GMN1,CL.PROCE,CL.ID" & vbCrLf
sConsulta = sConsulta & "  FROM CATALOG_LIN CL" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN CATN1 C1" & vbCrLf
sConsulta = sConsulta & "             ON CL.CAT1 = C1.ID" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN CATN2 C2" & vbCrLf
sConsulta = sConsulta & "             ON CL.CAT2 = C2.ID" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN CATN3 C3" & vbCrLf
sConsulta = sConsulta & "             ON CL.CAT3 = C3.ID" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN CATN4 C4" & vbCrLf
sConsulta = sConsulta & "             ON CL.CAT4 = C4.ID" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN CATN5 C5" & vbCrLf
sConsulta = sConsulta & "             ON CL.CAT5 = C5.ID" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN ITEM I" & vbCrLf
sConsulta = sConsulta & "             ON CL.ANYO = I.ANYO" & vbCrLf
sConsulta = sConsulta & "            AND CL.GMN1=I.GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND CL.PROCE=I.PROCE " & vbCrLf
sConsulta = sConsulta & "            AND CL.ITEM=I.ID" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN ART4 A4" & vbCrLf
sConsulta = sConsulta & "            ON CL.GMN1=A4.GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND CL.GMN2=A4.GMN2 " & vbCrLf
sConsulta = sConsulta & "            AND CL.GMN3=A4.GMN3 " & vbCrLf
sConsulta = sConsulta & "            AND CL.GMN4=A4.GMN4 " & vbCrLf
sConsulta = sConsulta & "            AND CL.ART_INT=A4.COD" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN ART4 ART_AUX " & vbCrLf
sConsulta = sConsulta & "             ON I.ART=ART_AUX.COD " & vbCrLf
sConsulta = sConsulta & "            AND ART_AUX.GMN1=I.GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND ART_AUX.GMN2=I.GMN2 " & vbCrLf
sConsulta = sConsulta & "            AND ART_AUX.GMN3=I.GMN3 " & vbCrLf
sConsulta = sConsulta & "            AND ART_AUX.GMN4=I.GMN4 " & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN PROVE_ART4 PA" & vbCrLf
sConsulta = sConsulta & "             ON CL.PROVE = PA.PROVE" & vbCrLf
sConsulta = sConsulta & "            AND CL.GMN1 = PA.GMN1" & vbCrLf
sConsulta = sConsulta & "            AND CL.GMN2 = PA.GMN2" & vbCrLf
sConsulta = sConsulta & "            AND CL.GMN3 = PA.GMN3" & vbCrLf
sConsulta = sConsulta & "            AND CL.GMN4 = PA.GMN4" & vbCrLf
sConsulta = sConsulta & "            AND CASE WHEN CL.ART_INT IS NULL THEN I.ART ELSE CL.ART_INT END = PA.ART" & vbCrLf
sConsulta = sConsulta & " WHERE CL.SOLICIT = @ID" & vbCrLf
sConsulta = sConsulta & "   AND CL.PUB = 1" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_DEVOLVER_LINEAS_DE_ORDEN]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_DEVOLVER_LINEAS_DE_ORDEN]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSEP_DEVOLVER_LINEAS_DE_ORDEN(@APROBADOR VARCHAR(50), @ORDENID INTEGER, @SOLICIT int = null )  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(1500)" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if @APROBADOR IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT LP.ID AS LINEAID," & vbCrLf
sConsulta = sConsulta & "                LP.ART_INT + ITEM.ART CODART," & vbCrLf
sConsulta = sConsulta & "                ART4.DEN + ITEM.DESCR  + ART_AUX.DEN DENART," & vbCrLf
sConsulta = sConsulta & "                LP.ART_INT + ITEM.ART + ' ' + ART4.DEN + ITEM.DESCR  + ART_AUX.DEN AS ARTICULO," & vbCrLf
sConsulta = sConsulta & "                PA.COD_EXT ART_EXT," & vbCrLf
sConsulta = sConsulta & "                LP.GMN1," & vbCrLf
sConsulta = sConsulta & "                LP.GMN2," & vbCrLf
sConsulta = sConsulta & "                LP.GMN3," & vbCrLf
sConsulta = sConsulta & "                LP.GMN4," & vbCrLf
sConsulta = sConsulta & "       LP.PROVE," & vbCrLf
sConsulta = sConsulta & "                LP.DEST AS DESTINO," & vbCrLf
sConsulta = sConsulta & "                LP.UP AS UNIDAD," & vbCrLf
sConsulta = sConsulta & "                LP.PREC_UP AS PRECIOUNITARIO," & vbCrLf
sConsulta = sConsulta & "                LP.CANT_PED AS CANTIDAD," & vbCrLf
sConsulta = sConsulta & "                LP.CANT_REC AS CANTIDAD_REC," & vbCrLf
sConsulta = sConsulta & "                ITEM_ADJ.CANT_ADJ / LP.FC AS CANTADJUDICADA," & vbCrLf
sConsulta = sConsulta & "                ITEM_ADJ.CANT_PED / LP.FC AS CANTCONSUMIDA," & vbCrLf
sConsulta = sConsulta & "                LP.PREC_UP * LP.CANT_PED IMPORTE," & vbCrLf
sConsulta = sConsulta & "                LP.EST AS APROBADA," & vbCrLf
sConsulta = sConsulta & "                LP.FECEMISION AS FECHA," & vbCrLf
sConsulta = sConsulta & "                GESTIONABLE=CASE WHEN APROB_LIM.PER= @APROBADOR   THEN 1 WHEN (APROB_LIM.PER IS NULL AND SEGURIDAD.APROB_TIPO1=@APROBADOR ) THEN 1  ELSE 0 END," & vbCrLf
sConsulta = sConsulta & "                LP.CAT1," & vbCrLf
sConsulta = sConsulta & "                LP.CAT2," & vbCrLf
sConsulta = sConsulta & "                LP.CAT3," & vbCrLf
sConsulta = sConsulta & "                LP.CAT4," & vbCrLf
sConsulta = sConsulta & "                LP.CAT5," & vbCrLf
sConsulta = sConsulta & "                LP.OBS," & vbCrLf
sConsulta = sConsulta & "                case when c5.cod is null then " & vbCrLf
sConsulta = sConsulta & "                        case when c4.cod is null then " & vbCrLf
sConsulta = sConsulta & "                             case when c3.cod is null then" & vbCrLf
sConsulta = sConsulta & "                                  case when c2.cod is null then" & vbCrLf
sConsulta = sConsulta & "                                       c1.cod" & vbCrLf
sConsulta = sConsulta & "                                  else" & vbCrLf
sConsulta = sConsulta & "                                  c2.cod" & vbCrLf
sConsulta = sConsulta & "                                  end" & vbCrLf
sConsulta = sConsulta & "                             else" & vbCrLf
sConsulta = sConsulta & "                             c3.cod" & vbCrLf
sConsulta = sConsulta & "                             end" & vbCrLf
sConsulta = sConsulta & "                        else" & vbCrLf
sConsulta = sConsulta & "                        c4.cod" & vbCrLf
sConsulta = sConsulta & "                        end" & vbCrLf
sConsulta = sConsulta & "                    else" & vbCrLf
sConsulta = sConsulta & "                    c5.cod" & vbCrLf
sConsulta = sConsulta & "                    end CATEGORIA," & vbCrLf
sConsulta = sConsulta & "                case when c5.cod is null then " & vbCrLf
sConsulta = sConsulta & "                        case when c4.cod is null then " & vbCrLf
sConsulta = sConsulta & "                             case when c3.cod is null then" & vbCrLf
sConsulta = sConsulta & "                                  case when c2.cod is null then" & vbCrLf
sConsulta = sConsulta & "                                       '(' + c1.cod + ') ' + c1.den" & vbCrLf
sConsulta = sConsulta & "                                  else" & vbCrLf
sConsulta = sConsulta & "                                  '(' + c1.cod + ') ' + c1.den + ' - (' + c2.cod + ') ' + c2.den" & vbCrLf
sConsulta = sConsulta & "                                  end" & vbCrLf
sConsulta = sConsulta & "                             else" & vbCrLf
sConsulta = sConsulta & "                             '(' + c1.cod + ') ' + c1.den + ' - (' + c2.cod + ') ' + c2.den + ' - (' + c3.cod + ') ' + c3.den" & vbCrLf
sConsulta = sConsulta & "                             end" & vbCrLf
sConsulta = sConsulta & "                        else" & vbCrLf
sConsulta = sConsulta & "                        '(' + c1.cod + ') ' + c1.den + ' - (' + c2.cod + ') ' + c2.den + ' - (' + c3.cod + ') ' + c3.den + ' - (' + c4.cod + ') ' + c4.den" & vbCrLf
sConsulta = sConsulta & "                        end" & vbCrLf
sConsulta = sConsulta & "                    else" & vbCrLf
sConsulta = sConsulta & "                    '(' + c1.cod + ') ' + c1.den + ' - (' + c2.cod + ') ' + c2.den + ' - (' + c3.cod + ') ' + c3.den + ' - (' + c4.cod + ') ' + c4.den + ' - (' + c5.cod + ') ' + c5.den" & vbCrLf
sConsulta = sConsulta & "                    end CATEGORIARAMA," & vbCrLf
sConsulta = sConsulta & "                    ITEM.ANYO ANYOPROCE," & vbCrLf
sConsulta = sConsulta & "                    ITEM.PROCE CODPROCE" & vbCrLf
sConsulta = sConsulta & "                     " & vbCrLf
sConsulta = sConsulta & "FROM LINEAS_PEDIDO LP" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN APROB_LIM" & vbCrLf
sConsulta = sConsulta & "            ON LP.APROB_LIM=APROB_LIM.ID" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN SEGURIDAD " & vbCrLf
sConsulta = sConsulta & "            ON LP.SEGURIDAD=SEGURIDAD.ID" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN ITEM " & vbCrLf
sConsulta = sConsulta & "            ON LP.ANYO = ITEM.ANYO" & vbCrLf
sConsulta = sConsulta & "           AND LP.GMN1=ITEM.GMN1 " & vbCrLf
sConsulta = sConsulta & "           AND LP.PROCE=ITEM.PROCE " & vbCrLf
sConsulta = sConsulta & "           AND LP.ITEM=ITEM.ID" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN ART4 " & vbCrLf
sConsulta = sConsulta & "            ON LP.GMN1=ART4.GMN1 " & vbCrLf
sConsulta = sConsulta & "           AND LP.GMN2=ART4.GMN2 " & vbCrLf
sConsulta = sConsulta & "           AND LP.GMN3=ART4.GMN3 " & vbCrLf
sConsulta = sConsulta & "           AND LP.GMN4=ART4.GMN4 " & vbCrLf
sConsulta = sConsulta & "           AND LP.ART_INT=ART4.COD" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN ITEM_ADJ " & vbCrLf
sConsulta = sConsulta & "            ON LP.ITEM=ITEM_ADJ.ITEM " & vbCrLf
sConsulta = sConsulta & "           AND LP.ANYO = ITEM_ADJ.ANYO" & vbCrLf
sConsulta = sConsulta & "           AND LP.GMN1=ITEM_ADJ.GMN1 " & vbCrLf
sConsulta = sConsulta & "           AND LP.PROCE=ITEM_ADJ.PROCE " & vbCrLf
sConsulta = sConsulta & "           AND LP.PROVE=ITEM_ADJ.PROVE" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN ART4 ART_AUX " & vbCrLf
sConsulta = sConsulta & "            ON ITEM.ART=ART_AUX.COD " & vbCrLf
sConsulta = sConsulta & "           AND ART_AUX.GMN1=ITEM.GMN1 " & vbCrLf
sConsulta = sConsulta & "           AND ART_AUX.GMN2=ITEM.GMN2 " & vbCrLf
sConsulta = sConsulta & "           AND ART_AUX.GMN3=ITEM.GMN3 " & vbCrLf
sConsulta = sConsulta & "           AND ART_AUX.GMN4=ITEM.GMN4 " & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN PROVE_ART4 PA" & vbCrLf
sConsulta = sConsulta & "            ON LP.PROVE = PA.PROVE" & vbCrLf
sConsulta = sConsulta & "           AND LP.GMN1 = PA.GMN1" & vbCrLf
sConsulta = sConsulta & "           AND LP.GMN2 = PA.GMN2" & vbCrLf
sConsulta = sConsulta & "           AND LP.GMN3 = PA.GMN3" & vbCrLf
sConsulta = sConsulta & "           AND LP.GMN4 = PA.GMN4" & vbCrLf
sConsulta = sConsulta & "           AND CASE WHEN LP.ART_INT IS NULL THEN ITEM.ART ELSE LP.ART_INT END = PA.ART" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN CATN1 C1" & vbCrLf
sConsulta = sConsulta & "            ON LP.CAT1 = C1.ID" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN CATN2 C2" & vbCrLf
sConsulta = sConsulta & "            ON LP.CAT2 = C2.ID" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN CATN3 C3" & vbCrLf
sConsulta = sConsulta & "            ON LP.CAT3 = C3.ID" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN CATN4 C4" & vbCrLf
sConsulta = sConsulta & "            ON LP.CAT4 = C4.ID" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN CATN5 C5" & vbCrLf
sConsulta = sConsulta & "            ON LP.CAT5 = C5.ID" & vbCrLf
sConsulta = sConsulta & "WHERE LP.ORDEN=@ORDENID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT LP.ID LINEAID," & vbCrLf
sConsulta = sConsulta & "                LP.ART_INT + I.ART CODART," & vbCrLf
sConsulta = sConsulta & "                LP.GMN1," & vbCrLf
sConsulta = sConsulta & "                LP.GMN2," & vbCrLf
sConsulta = sConsulta & "                LP.GMN3," & vbCrLf
sConsulta = sConsulta & "                LP.GMN4," & vbCrLf
sConsulta = sConsulta & "       LP.PROVE," & vbCrLf
sConsulta = sConsulta & "                ART4.DEN + I.DESCR  + ART_AUX.DEN DENART," & vbCrLf
sConsulta = sConsulta & "                PA.COD_EXT ART_EXT," & vbCrLf
sConsulta = sConsulta & "                LP.DEST DESTINO," & vbCrLf
sConsulta = sConsulta & "                D.DEN DESTDEN," & vbCrLf
sConsulta = sConsulta & "                LP.UP UNIDAD," & vbCrLf
sConsulta = sConsulta & "                U.DEN UNIDEN," & vbCrLf
sConsulta = sConsulta & "                LP.PREC_UP PRECIOUNITARIO," & vbCrLf
sConsulta = sConsulta & "                LP.CANT_PED CANTIDAD," & vbCrLf
sConsulta = sConsulta & "                LP.CANT_REC CANT_REC," & vbCrLf
sConsulta = sConsulta & "                IA.CANT_ADJ * LP.FC AS CANTADJUDICADA," & vbCrLf
sConsulta = sConsulta & "                LP.PREC_UP * LP.CANT_PED IMPORTE," & vbCrLf
sConsulta = sConsulta & "                LP.EST AS APROBADA," & vbCrLf
sConsulta = sConsulta & "                LP.FECEMISION FECHA," & vbCrLf
sConsulta = sConsulta & "                LP.FECENTREGA FECENTREGA," & vbCrLf
sConsulta = sConsulta & "                LP.EST,LP.ENTREGA_OBL,LP.FECENTREGAPROVE," & vbCrLf
sConsulta = sConsulta & "                LP.CAT1," & vbCrLf
sConsulta = sConsulta & "                LP.CAT2," & vbCrLf
sConsulta = sConsulta & "                LP.CAT3," & vbCrLf
sConsulta = sConsulta & "                LP.CAT4," & vbCrLf
sConsulta = sConsulta & "                LP.CAT5," & vbCrLf
sConsulta = sConsulta & "                LP.OBS," & vbCrLf
sConsulta = sConsulta & "                case when c5.cod is null then " & vbCrLf
sConsulta = sConsulta & "                        case when c4.cod is null then " & vbCrLf
sConsulta = sConsulta & "                             case when c3.cod is null then" & vbCrLf
sConsulta = sConsulta & "                                  case when c2.cod is null then" & vbCrLf
sConsulta = sConsulta & "                                       c1.cod" & vbCrLf
sConsulta = sConsulta & "                                  else" & vbCrLf
sConsulta = sConsulta & "                                  c2.cod" & vbCrLf
sConsulta = sConsulta & "                                  end" & vbCrLf
sConsulta = sConsulta & "                             else" & vbCrLf
sConsulta = sConsulta & "                             c3.cod" & vbCrLf
sConsulta = sConsulta & "                             end" & vbCrLf
sConsulta = sConsulta & "                        else" & vbCrLf
sConsulta = sConsulta & "                        c4.cod" & vbCrLf
sConsulta = sConsulta & "                        end" & vbCrLf
sConsulta = sConsulta & "                    else" & vbCrLf
sConsulta = sConsulta & "                    c5.cod" & vbCrLf
sConsulta = sConsulta & "                    end CATEGORIA," & vbCrLf
sConsulta = sConsulta & "                case when c5.cod is null then " & vbCrLf
sConsulta = sConsulta & "                        case when c4.cod is null then " & vbCrLf
sConsulta = sConsulta & "                             case when c3.cod is null then" & vbCrLf
sConsulta = sConsulta & "                                  case when c2.cod is null then" & vbCrLf
sConsulta = sConsulta & "                                       '(' + c1.cod + ') ' + c1.den" & vbCrLf
sConsulta = sConsulta & "                                  else" & vbCrLf
sConsulta = sConsulta & "                                  '(' + c1.cod + ') ' + c1.den + ' - (' + c2.cod + ') ' + c2.den" & vbCrLf
sConsulta = sConsulta & "                                  end" & vbCrLf
sConsulta = sConsulta & "                             else" & vbCrLf
sConsulta = sConsulta & "                             '(' + c1.cod + ') ' + c1.den + ' - (' + c2.cod + ') ' + c2.den + ' - (' + c3.cod + ') ' + c3.den" & vbCrLf
sConsulta = sConsulta & "                             end" & vbCrLf
sConsulta = sConsulta & "                        else" & vbCrLf
sConsulta = sConsulta & "                        '(' + c1.cod + ') ' + c1.den + ' - (' + c2.cod + ') ' + c2.den + ' - (' + c3.cod + ') ' + c3.den + ' - (' + c4.cod + ') ' + c4.den" & vbCrLf
sConsulta = sConsulta & "                        end" & vbCrLf
sConsulta = sConsulta & "                    else" & vbCrLf
sConsulta = sConsulta & "                    '(' + c1.cod + ') ' + c1.den + ' - (' + c2.cod + ') ' + c2.den + ' - (' + c3.cod + ') ' + c3.den + ' - (' + c4.cod + ') ' + c4.den + ' - (' + c5.cod + ') ' + c5.den" & vbCrLf
sConsulta = sConsulta & "                    end CATEGORIARAMA," & vbCrLf
sConsulta = sConsulta & "               " & vbCrLf
sConsulta = sConsulta & "                        " & vbCrLf
sConsulta = sConsulta & "                C1.DEN CAT1DEN," & vbCrLf
sConsulta = sConsulta & "                C2.DEN CAT2DEN," & vbCrLf
sConsulta = sConsulta & "                C3.DEN CAT3DEN," & vbCrLf
sConsulta = sConsulta & "                C4.DEN CAT4DEN," & vbCrLf
sConsulta = sConsulta & "                C5.DEN CAT5DEN," & vbCrLf
sConsulta = sConsulta & "                LE.COMENT," & vbCrLf
sConsulta = sConsulta & "                I.ANYO ANYOPROCE," & vbCrLf
sConsulta = sConsulta & "                I.PROCE CODPROCE," & vbCrLf
sConsulta = sConsulta & "                I.ID ITEM" & vbCrLf
sConsulta = sConsulta & " FROM LINEAS_PEDIDO LP" & vbCrLf
sConsulta = sConsulta & "     INNER JOIN DEST D" & vbCrLf
sConsulta = sConsulta & "             ON LP.DEST = D.COD" & vbCrLf
sConsulta = sConsulta & "     INNER JOIN UNI U" & vbCrLf
sConsulta = sConsulta & "             ON LP.UP = U.COD" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN ITEM I " & vbCrLf
sConsulta = sConsulta & "             ON LP.ANYO = I.ANYO" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN1=I.GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND LP.PROCE=I.PROCE " & vbCrLf
sConsulta = sConsulta & "            AND LP.ITEM=I.ID" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN PROVE_ART4 PA" & vbCrLf
sConsulta = sConsulta & "             ON LP.PROVE = PA.PROVE" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN1 = PA.GMN1" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN2 = PA.GMN2" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN3 = PA.GMN3" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN4 = PA.GMN4" & vbCrLf
sConsulta = sConsulta & "            AND CASE WHEN LP.ART_INT IS NULL THEN I.ART ELSE LP.ART_INT END = PA.ART" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN ART4 " & vbCrLf
sConsulta = sConsulta & "             ON LP.GMN1=ART4.GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN2=ART4.GMN2 " & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN3=ART4.GMN3 " & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN4=ART4.GMN4 " & vbCrLf
sConsulta = sConsulta & "            AND LP.ART_INT=ART4.COD" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN ITEM_ADJ IA " & vbCrLf
sConsulta = sConsulta & "             ON LP.ITEM = IA.ITEM " & vbCrLf
sConsulta = sConsulta & "            AND LP.ANYO = IA.ANYO" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN1 = IA.GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND LP.PROCE= IA.PROCE " & vbCrLf
sConsulta = sConsulta & "            AND LP.PROVE= IA.PROVE" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN ART4 ART_AUX " & vbCrLf
sConsulta = sConsulta & "             ON ART_AUX.COD=I.ART " & vbCrLf
sConsulta = sConsulta & "            AND ART_AUX.GMN1=I.GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND ART_AUX.GMN2=I.GMN2 " & vbCrLf
sConsulta = sConsulta & "            AND ART_AUX.GMN3=I.GMN3 " & vbCrLf
sConsulta = sConsulta & "            AND ART_AUX.GMN4=I.GMN4" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN CATN1 C1" & vbCrLf
sConsulta = sConsulta & "             ON LP.CAT1 = C1.ID" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN CATN2 C2" & vbCrLf
sConsulta = sConsulta & "             ON LP.CAT2 = C2.ID" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN CATN3 C3" & vbCrLf
sConsulta = sConsulta & "             ON LP.CAT3 = C3.ID" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN CATN4 C4" & vbCrLf
sConsulta = sConsulta & "             ON LP.CAT4 = C4.ID" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN CATN5 C5" & vbCrLf
sConsulta = sConsulta & "             ON LP.CAT5 = C5.ID" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN LINEAS_EST LE" & vbCrLf
sConsulta = sConsulta & "             ON LP.LINEAS_EST = LE.ID" & vbCrLf
sConsulta = sConsulta & " WHERE LP.ORDEN=@ORDENID" & vbCrLf
sConsulta = sConsulta & "   AND CASE WHEN LP.SOLICIT IS NULL AND @SOLICIT IS NULL THEN -1 ELSE LP.SOLICIT END " & vbCrLf
sConsulta = sConsulta & "       = CASE WHEN LP.SOLICIT IS NULL AND @SOLICIT IS NULL THEN -1 ELSE ISNULL(@SOLICIT,LP.SOLICIT) END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Function
Private Function V_2_11_Storeds_01_3()
Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_DEVOLVER_ORDENES]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_DEVOLVER_ORDENES]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSEP_DEVOLVER_ORDENES(@APROBADOR VARCHAR(50), @MAXLINEAS INTEGER, @DESDEID INTEGER," & vbCrLf
sConsulta = sConsulta & "@ANYO INTEGER, @NUMPEDIDO INTEGER, @ORDEN INTEGER, @APROBADOS INTEGER,@DENEGADOS INTEGER,@CODPROVE VARCHAR(50),@NUMPEDIDOPROVE VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & ",@DESDEFECHA VARCHAR(10), @APROVISIONADOR VARCHAR(50) )  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @INDICE INTEGER" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING= 'SELECT DISTINCT TOP '" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING= @SQLSTRING + CONVERT(VARCHAR,@MAXLINEAS) + ' ORDEN_ENTREGA.ID AS ORDENID,ORDEN_ENTREGA.ANYO AS ANYO" & vbCrLf
sConsulta = sConsulta & ",PEDIDO.NUM AS NUMPEDIDO" & vbCrLf
sConsulta = sConsulta & ",ORDEN_ENTREGA.NUM AS NUMORDEN" & vbCrLf
sConsulta = sConsulta & ",ORDEN_ENTREGA.PROVE + '' '' + PROVE.DEN AS PROVE" & vbCrLf
sConsulta = sConsulta & ",ORDEN_ENTREGA.PROVE AS PROVECOD" & vbCrLf
sConsulta = sConsulta & ",PROVE.DEN AS PROVEDEN" & vbCrLf
sConsulta = sConsulta & ",ORDEN_ENTREGA.FECHA" & vbCrLf
sConsulta = sConsulta & ",ORDEN_ENTREGA.EST" & vbCrLf
sConsulta = sConsulta & ",ORDEN_ENTREGA.IMPORTE " & vbCrLf
sConsulta = sConsulta & ",PER.APE + '','' + PER.NOM AS APROVISIONADOR" & vbCrLf
sConsulta = sConsulta & "FROM LINEAS_PEDIDO INNER JOIN SEGURIDAD" & vbCrLf
sConsulta = sConsulta & "ON LINEAS_PEDIDO.SEGURIDAD=SEGURIDAD.ID AND APROB_TIPO1='''+ @APROBADOR +''' AND LINEAS_PEDIDO.NIVEL_APROB=0'" & vbCrLf
sConsulta = sConsulta & "IF @APROBADOS=0 AND @DENEGADOS <> 0" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND LINEAS_PEDIDO.EST IN (0,20)' " & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & " IF @APROBADOS<>0 AND @DENEGADOS=0" & vbCrLf
sConsulta = sConsulta & "     SET @SQLSTRING= @SQLSTRING + ' AND LINEAS_PEDIDO.EST IN (0,1,2,3)'" & vbCrLf
sConsulta = sConsulta & " ELSE            " & vbCrLf
sConsulta = sConsulta & "     IF @APROBADOS=0 AND @DENEGADOS=0" & vbCrLf
sConsulta = sConsulta & "         SET @SQLSTRING= @SQLSTRING + ' AND LINEAS_PEDIDO.EST =0 '" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING= @SQLSTRING + ' INNER JOIN ORDEN_ENTREGA ON LINEAS_PEDIDO.ORDEN=ORDEN_ENTREGA.ID'" & vbCrLf
sConsulta = sConsulta & "IF @ORDEN<>0" & vbCrLf
sConsulta = sConsulta & "  SET @SQLSTRING= @SQLSTRING + ' AND ORDEN_ENTREGA.NUM=' + CONVERT(VARCHAR,@ORDEN ) " & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING= @SQLSTRING + ' INNER JOIN PEDIDO ON ORDEN_ENTREGA.PEDIDO=PEDIDO.ID '" & vbCrLf
sConsulta = sConsulta & "IF @NUMPEDIDO <>0 " & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND PEDIDO.NUM=' +CONVERT(VARCHAR,@NUMPEDIDO )" & vbCrLf
sConsulta = sConsulta & "IF @APROVISIONADOR <>''" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND PEDIDO.PER=''' + @APROVISIONADOR +''' '" & vbCrLf
sConsulta = sConsulta & "if @ANYO<>0" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING=@SQLSTRING + ' AND PEDIDO.ANYO =' + CONVERT(VARCHAR,@ANYO) " & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING=@SQLSTRING + '  INNER JOIN PROVE ON ORDEN_ENTREGA.PROVE=PROVE.COD INNER JOIN PER ON PER.COD=PEDIDO.PER '" & vbCrLf
sConsulta = sConsulta & "IF @DESDEID <> 0 " & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND ORDEN_ENTREGA.ID >=' + CONVERT(VARCHAR,@DESDEID) " & vbCrLf
sConsulta = sConsulta & "IF @CODPROVE <>'' " & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND ORDEN_ENTREGA.PROVE =''' + @CODPROVE + ''''" & vbCrLf
sConsulta = sConsulta & "IF @NUMPEDIDOPROVE <> ''" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND ORDEN_ENTREGA.NUMEXT = ''' + @NUMPEDIDOPROVE + ''''" & vbCrLf
sConsulta = sConsulta & "IF @DESDEFECHA <> ''" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND ORDEN_ENTREGA.FECHA >= CONVERT(DATETIME,''' + @DESDEFECHA + ''',103) '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING= @SQLSTRING + ' UNION SELECT DISTINCT TOP '" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING= @SQLSTRING + CONVERT(VARCHAR,@MAXLINEAS) + ' ORDEN_ENTREGA.ID AS ORDENID,ORDEN_ENTREGA.ANYO AS ANYO" & vbCrLf
sConsulta = sConsulta & ",PEDIDO.NUM AS NUMPEDIDO" & vbCrLf
sConsulta = sConsulta & ",ORDEN_ENTREGA.NUM AS NUMORDEN" & vbCrLf
sConsulta = sConsulta & ",ORDEN_ENTREGA.PROVE + '' '' + PROVE.DEN AS PROVE" & vbCrLf
sConsulta = sConsulta & ",ORDEN_ENTREGA.PROVE AS PROVECOD" & vbCrLf
sConsulta = sConsulta & ",PROVE.DEN AS PROVEDEN" & vbCrLf
sConsulta = sConsulta & ",ORDEN_ENTREGA.FECHA" & vbCrLf
sConsulta = sConsulta & ",ORDEN_ENTREGA.EST" & vbCrLf
sConsulta = sConsulta & ",ORDEN_ENTREGA.IMPORTE " & vbCrLf
sConsulta = sConsulta & ",PER.APE + '','' + PER.NOM AS APROVISIONADOR" & vbCrLf
sConsulta = sConsulta & "FROM LINEAS_PEDIDO INNER JOIN APROB_LIM" & vbCrLf
sConsulta = sConsulta & "ON LINEAS_PEDIDO.APROB_LIM=APROB_LIM.ID AND APROB_LIM.PER='''+ @APROBADOR +''''" & vbCrLf
sConsulta = sConsulta & "IF @APROBADOS=0 AND @DENEGADOS <> 0" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND LINEAS_PEDIDO.EST IN (0,20)' " & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & " IF @APROBADOS<>0 AND @DENEGADOS=0" & vbCrLf
sConsulta = sConsulta & "     SET @SQLSTRING= @SQLSTRING + ' AND LINEAS_PEDIDO.EST IN (0,1,2,3)'" & vbCrLf
sConsulta = sConsulta & " ELSE" & vbCrLf
sConsulta = sConsulta & "     IF @APROBADOS=0 AND @DENEGADOS=0    " & vbCrLf
sConsulta = sConsulta & "         SET @SQLSTRING= @SQLSTRING + ' AND LINEAS_PEDIDO.EST =0 '" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING= @SQLSTRING + ' INNER JOIN ORDEN_ENTREGA ON LINEAS_PEDIDO.ORDEN=ORDEN_ENTREGA.ID '" & vbCrLf
sConsulta = sConsulta & "IF @ORDEN<>0" & vbCrLf
sConsulta = sConsulta & "  SET @SQLSTRING= @SQLSTRING + ' AND ORDEN_ENTREGA.NUM=' + CONVERT(VARCHAR,@ORDEN ) " & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING= @SQLSTRING + ' INNER JOIN PEDIDO ON ORDEN_ENTREGA.PEDIDO=PEDIDO.ID '" & vbCrLf
sConsulta = sConsulta & "IF @NUMPEDIDO <>0 " & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND PEDIDO.NUM=' +CONVERT(VARCHAR,@NUMPEDIDO )" & vbCrLf
sConsulta = sConsulta & "IF @APROVISIONADOR <>''" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND PEDIDO.PER=''' + @APROVISIONADOR +''' '" & vbCrLf
sConsulta = sConsulta & "if @ANYO<>0 " & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING=@SQLSTRING + ' AND PEDIDO.ANYO =' + CONVERT(VARCHAR,@ANYO) " & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING=@SQLSTRING + ' INNER JOIN PROVE ON ORDEN_ENTREGA.PROVE=PROVE.COD INNER JOIN PER ON PER.COD=PEDIDO.PER WHERE 1=1 '" & vbCrLf
sConsulta = sConsulta & "IF @DESDEID <> 0 " & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND ORDEN_ENTREGA.ID >=' + CONVERT(VARCHAR,@DESDEID) " & vbCrLf
sConsulta = sConsulta & "IF @CODPROVE <>''" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND ORDEN_ENTREGA.PROVE =''' + @CODPROVE + ''''" & vbCrLf
sConsulta = sConsulta & "IF @NUMPEDIDOPROVE <> ''" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND ORDEN_ENTREGA.NUMEXT = ''' + @NUMPEDIDOPROVE + ''''" & vbCrLf
sConsulta = sConsulta & "IF @DESDEFECHA <> ''" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND ORDEN_ENTREGA.FECHA >=CONVERT(DATETIME,''' + @DESDEFECHA + ''',103) '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "/*PRINT @SQLSTRING*/" & vbCrLf
sConsulta = sConsulta & "EXECUTE sp_executesql @SQLSTRING" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_DEVOLVER_TODAS_ID_ORDENES]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_DEVOLVER_TODAS_ID_ORDENES]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSEP_DEVOLVER_TODAS_ID_ORDENES @MAXLINEAS integer, @APROBADOR varchar(50) =null,@APROVISIONADOR varchar(50)  =null,@ARTINT varchar(50)  =null, @ARTDEN varchar(500)  =null, @ARTEXT varchar(500)  =null, @DESDEFECHA varchar(10)  =null, @HASTAFECHA varchar(10)  =null, @PROVECOD varchar(50)  =null,@PROVEDEN varchar(500)  =null,@NUMPEDPROVE varchar(100)  =null,@ANYO int  =null,@PEDIDO int  =null,@ORDEN int  =null,@EST int  =null ,@COINCIDENCIA int =null  AS" & vbCrLf
sConsulta = sConsulta & "declare @SQL nvarchar(4000)" & vbCrLf
sConsulta = sConsulta & "declare @SQL2 nvarchar(4000)" & vbCrLf
sConsulta = sConsulta & "declare @PARAM nvarchar(500)" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
sConsulta = sConsulta & "set  @SQL = N'" & vbCrLf
sConsulta = sConsulta & "SELECT CONVERT(VARCHAR,OE.EST) + LEFT(''00000000'',8-LEN(CONVERT(VARCHAR,OE.ID))) + CONVERT(VARCHAR,OE.ID)" & vbCrLf
sConsulta = sConsulta & "  FROM ORDEN_ENTREGA OE" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN PROVE P" & vbCrLf
sConsulta = sConsulta & "            ON OE.PROVE = P.COD" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN PEDIDO PE" & vbCrLf
sConsulta = sConsulta & "            ON OE.PEDIDO = PE.ID '" & vbCrLf
sConsulta = sConsulta & "IF @APROBADOR IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN (SELECT DISTINCT ORDEN , S.APROB_TIPO1 PER" & vbCrLf
sConsulta = sConsulta & "                           FROM LINEAS_PEDIDO LP " & vbCrLf
sConsulta = sConsulta & "                                    INNER JOIN SEGURIDAD S" & vbCrLf
sConsulta = sConsulta & "                                            ON LP.SEGURIDAD = S.ID ) T1" & vbCrLf
sConsulta = sConsulta & "            ON OE.ID = T1.ORDEN" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN (SELECT DISTINCT ORDEN , AL.PER PER" & vbCrLf
sConsulta = sConsulta & "                            FROM LINEAS_PEDIDO LP " & vbCrLf
sConsulta = sConsulta & "                                     INNER JOIN APROB_LIM AL" & vbCrLf
sConsulta = sConsulta & "                                             ON LP.APROB_LIM = AL.ID) T2" & vbCrLf
sConsulta = sConsulta & "            ON OE.ID = T2.ORDEN '" & vbCrLf
sConsulta = sConsulta & "set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN PER P1" & vbCrLf
sConsulta = sConsulta & "            ON PE.PER = P1.COD" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN ORDEN_EST OES" & vbCrLf
sConsulta = sConsulta & "            ON OE.ORDEN_EST = OES.ID'" & vbCrLf
sConsulta = sConsulta & "if @ARTINT is not null or @ARTDEN is not null or @ARTEXT is not null" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "   set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN (SELECT DISTINCT ORDEN" & vbCrLf
sConsulta = sConsulta & "                  FROM LINEAS_PEDIDO LP" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN ITEM I " & vbCrLf
sConsulta = sConsulta & "             ON LP.ANYO = I.ANYO" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN1=I.GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND LP.PROCE=I.PROCE " & vbCrLf
sConsulta = sConsulta & "            AND LP.ITEM=I.ID'" & vbCrLf
sConsulta = sConsulta & "      IF @ARTDEN is NOT NULL " & vbCrLf
sConsulta = sConsulta & "         set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN ART4 " & vbCrLf
sConsulta = sConsulta & "             ON LP.GMN1=ART4.GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN2=ART4.GMN2 " & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN3=ART4.GMN3 " & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN4=ART4.GMN4 " & vbCrLf
sConsulta = sConsulta & "            AND LP.ART_INT=ART4.COD" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN ART4 ART_AUX " & vbCrLf
sConsulta = sConsulta & "             ON ART_AUX.COD=I.ART " & vbCrLf
sConsulta = sConsulta & "            AND ART_AUX.GMN1=I.GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND ART_AUX.GMN2=I.GMN2 " & vbCrLf
sConsulta = sConsulta & "            AND ART_AUX.GMN3=I.GMN3 " & vbCrLf
sConsulta = sConsulta & "            AND ART_AUX.GMN4=I.GMN4'" & vbCrLf
sConsulta = sConsulta & "      IF @ARTEXT is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN PROVE_ART4 PA" & vbCrLf
sConsulta = sConsulta & "             ON LP.PROVE = PA.PROVE" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN1 = PA.GMN1" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN2 = PA.GMN2" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN3 = PA.GMN3" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN4 = PA.GMN4" & vbCrLf
sConsulta = sConsulta & "            AND CASE WHEN LP.ART_INT IS NULL THEN I.ART ELSE LP.ART_INT END = PA.ART" & vbCrLf
sConsulta = sConsulta & "'" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "   set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                 WHERE 1=1'" & vbCrLf
sConsulta = sConsulta & "   IF @ARTINT is NOT NULL " & vbCrLf
sConsulta = sConsulta & "     begin " & vbCrLf
sConsulta = sConsulta & "   IF @COINCIDENCIA=1 " & vbCrLf
sConsulta = sConsulta & "          set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                   AND (LP.ART_INT + I.ART like ''% ''  + @ARTINT + '' %''' +" & vbCrLf
sConsulta = sConsulta & "          ' OR LP.ART_INT + I.ART like @ARTINT  + '' %''' +" & vbCrLf
sConsulta = sConsulta & "          ' OR LP.ART_INT + I.ART like ''% '' + @ARTINT  ' +" & vbCrLf
sConsulta = sConsulta & "          ' OR LP.ART_INT + I.ART =@ARTINT)'" & vbCrLf

sConsulta = sConsulta & "        ELSE" & vbCrLf
sConsulta = sConsulta & "      set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                   AND LP.ART_INT + I.ART like @ARTINT + ''%'' '" & vbCrLf
sConsulta = sConsulta & "     end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @ARTDEN is NOT NULL " & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "      IF @COINCIDENCIA=1 " & vbCrLf
sConsulta = sConsulta & "         set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                   AND (ART4.DEN + I.DESCR  + ART_AUX.DEN like ''% ''  + @ARTDEN + '' %''' + " & vbCrLf
sConsulta = sConsulta & "          ' OR ART4.DEN + I.DESCR  + ART_AUX.DEN like @ARTDEN + '' %''' +" & vbCrLf
sConsulta = sConsulta & "          ' OR ART4.DEN + I.DESCR  + ART_AUX.DEN like ''% '' + @ARTDEN ' + " & vbCrLf
sConsulta = sConsulta & "          ' OR ART4.DEN + I.DESCR  + ART_AUX.DEN =@ARTDEN)'" & vbCrLf
sConsulta = sConsulta & "      ELSE" & vbCrLf
sConsulta = sConsulta & "         set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                   AND ART4.DEN + I.DESCR  + ART_AUX.DEN like + ''%'' + @ARTDEN + ''%'''" & vbCrLf
sConsulta = sConsulta & "    END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    IF @ARTEXT is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                   AND PA.COD_EXT like @ARTEXT + ''%''" & vbCrLf
sConsulta = sConsulta & "'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   set  @SQL = @SQL + N') LP" & vbCrLf
sConsulta = sConsulta & "            ON OE.ID = LP.ORDEN" & vbCrLf
sConsulta = sConsulta & "'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "        " & vbCrLf
sConsulta = sConsulta & "set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "              WHERE PE.TIPO=1'" & vbCrLf
sConsulta = sConsulta & "if @DESDEFECHA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.FECHA >= CONVERT(datetime,@DESDEFECHA,103)'" & vbCrLf
sConsulta = sConsulta & "if @HASTAFECHA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.FECHA <= CONVERT(datetime,@HASTAFECHA,103)'" & vbCrLf
sConsulta = sConsulta & "if @PROVECOD IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.PROVE = @PROVECOD'" & vbCrLf
sConsulta = sConsulta & "if @NUMPEDPROVE IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.NUMEXT = @NUMPEDPROVE '" & vbCrLf
sConsulta = sConsulta & "if @ORDEN IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.NUM = @ORDEN '" & vbCrLf
sConsulta = sConsulta & "if @ANYO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.ANYO = @ANYO '" & vbCrLf
sConsulta = sConsulta & "if @PEDIDO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND PE.NUM = @PEDIDO '" & vbCrLf
sConsulta = sConsulta & "if @PROVEDEN IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND P.DEN  LIKE @PROVEDEN + ''%'' '" & vbCrLf
sConsulta = sConsulta & "IF @APROBADOR IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND (T1.PER = @APROBADOR " & vbCrLf
sConsulta = sConsulta & "                           OR T2.PER = @APROBADOR) '" & vbCrLf
sConsulta = sConsulta & "if @APROVISIONADOR IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND PE.PER = @APROVISIONADOR '" & vbCrLf
sConsulta = sConsulta & "declare @IN varchar(500)" & vbCrLf
sConsulta = sConsulta & "SET @IN = '('" & vbCrLf
sConsulta = sConsulta & "IF @EST & 1 = 1" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '0,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 2 = 2" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '1,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 4 = 4" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '2,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 8 = 8" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '3,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 16 = 16" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '4,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 32 = 32" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '5,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 64 = 64" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '6,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 128 = 128" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '20,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 256 = 256" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '21,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 512 = 512" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '22,'" & vbCrLf
sConsulta = sConsulta & "IF @IN!='('" & vbCrLf
sConsulta = sConsulta & "   set @SQL = @SQL + '" & vbCrLf
sConsulta = sConsulta & "   AND OE.EST IN ' + LEFT(@IN,LEN(@IN)-1) + ') '" & vbCrLf
sConsulta = sConsulta & "set @SQL = @SQL + '" & vbCrLf
sConsulta = sConsulta & "order by CONVERT(VARCHAR,OE.EST) + LEFT(''00000000'',8-LEN(CONVERT(VARCHAR,OE.ID))) + CONVERT(VARCHAR,OE.ID)'" & vbCrLf
sConsulta = sConsulta & "SET @SQL2 = '" & vbCrLf
sConsulta = sConsulta & "create table #IDS (id varchar(200))" & vbCrLf
sConsulta = sConsulta & "set nocount on" & vbCrLf
sConsulta = sConsulta & "DECLARE C1 CURSOR FOR " & vbCrLf
sConsulta = sConsulta & "' + @SQL + '" & vbCrLf
sConsulta = sConsulta & "DECLARE @miID varchar(200)" & vbCrLf
sConsulta = sConsulta & "declare @cont int" & vbCrLf
sConsulta = sConsulta & "set @cont=1" & vbCrLf
sConsulta = sConsulta & "OPEN C1" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C1 INTO @miID" & vbCrLf
sConsulta = sConsulta & "while @@fetch_status=0" & vbCrLf
sConsulta = sConsulta & "begin" & vbCrLf
sConsulta = sConsulta & "if @cont = 1" & vbCrLf
sConsulta = sConsulta & " insert into #IDS values (@miID)" & vbCrLf
sConsulta = sConsulta & "set @cont = @cont + 1        " & vbCrLf
sConsulta = sConsulta & "     " & vbCrLf
sConsulta = sConsulta & "if @cont = ' + CONVERT(VARCHAR,@MAXLINEAS) + ' + 1" & vbCrLf
sConsulta = sConsulta & " set @cont = 1" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C1 INTO @miID" & vbCrLf
sConsulta = sConsulta & "end" & vbCrLf
sConsulta = sConsulta & "CLOSE C1" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C1" & vbCrLf
sConsulta = sConsulta & "set nocount off" & vbCrLf
sConsulta = sConsulta & "select * from #ids" & vbCrLf
sConsulta = sConsulta & "'" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF" & vbCrLf
sConsulta = sConsulta & "SET @PARAM = N'@APROBADOR varchar(50), @APROVISIONADOR varchar(50), @DESDEFECHA varchar(10), @HASTAFECHA varchar(10), @ARTINT varchar(50), @ARTDEN varchar(500), @ARTEXT varchar(500), @PROVECOD varchar(50), @PROVEDEN varchar(500), @NUMPEDPROVE varchar(100),@ORDEN int, @ANYO int, @PEDIDO int, @EST int'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL2, @PARAM, @APROBADOR= @APROBADOR, @APROVISIONADOR = @APROVISIONADOR, @DESDEFECHA=@DESDEFECHA, @HASTAFECHA=@HASTAFECHA, @ARTINT=@ARTINT, @ARTDEN=@ARTDEN, @ARTEXT=@ARTEXT, @PROVECOD=@PROVECOD, @PROVEDEN=@PROVEDEN, @NUMPEDPROVE=@NUMPEDPROVE, @ANYO=@ANYO, @ORDEN=@ORDEN, @PEDIDO = @PEDIDO, @EST=@EST" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_DEVOLVER_TODAS_ORDENES]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_DEVOLVER_TODAS_ORDENES]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSEP_DEVOLVER_TODAS_ORDENES @MAXLINEAS integer, @DESDEID varchar(50)=null, @APROBADOR varchar(50) = null,@APROVISIONADOR varchar(50) = null, @ARTINT varchar(50) = null, @ARTDEN varchar(500) = null, @ARTEXT varchar(500) = null, @DESDEFECHA varchar(10) = null, @HASTAFECHA varchar(10) = null, @PROVECOD varchar(50) = null, @PROVEDEN varchar(500) = null,@NUMPEDPROVE varchar(100) = null,@ANYO int = null,@PEDIDO int = null,@ORDEN int = null,@EST int  = null, @ID int = null ,@RECINCORRECTO int = null ,@COINCIDENCIA int =null AS " & vbCrLf
sConsulta = sConsulta & "declare @SQL nvarchar(4000)" & vbCrLf
sConsulta = sConsulta & "declare @PARAM nvarchar(500)" & vbCrLf
sConsulta = sConsulta & "declare @SQL1 nvarchar(4000)" & vbCrLf
sConsulta = sConsulta & "declare @SQL2 nvarchar(4000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "set  @SQL = N'" & vbCrLf
sConsulta = sConsulta & "SELECT TOP ' + CONVERT(varchar,@MAXLINEAS) + ' CONVERT(VARCHAR,OE.EST) + LEFT(''00000000'',8-LEN(CONVERT(VARCHAR,OE.ID))) + CONVERT(VARCHAR,OE.ID), " & vbCrLf
sConsulta = sConsulta & "       OE.ID, OE.ANYO, PE.NUM NUMPEDIDO, OE.NUM NUMORDEN, OE.PROVE PROVECOD, P.DEN PROVEDEN, " & vbCrLf
sConsulta = sConsulta & "       OE.NUMEXT, OE.FECHA FECHA, OE.EST EST, OE.IMPORTE IMPORTE, " & vbCrLf
sConsulta = sConsulta & "       P1.APE + '', '' + P1.NOM APROVISIONADOR,P1.APE,P1.NOM," & vbCrLf
sConsulta = sConsulta & "       OES.COMENT, OES.FECHA FECEST,OE.IMPORTE,OE.PEDIDO,OE.TIPO, OE.INCORRECTA, PE.PER" & vbCrLf
sConsulta = sConsulta & "  FROM ORDEN_ENTREGA OE" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN PROVE P" & vbCrLf
sConsulta = sConsulta & "            ON OE.PROVE = P.COD" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN PEDIDO PE" & vbCrLf
sConsulta = sConsulta & "            ON OE.PEDIDO = PE.ID '" & vbCrLf
sConsulta = sConsulta & "IF @APROBADOR IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN (SELECT DISTINCT ORDEN , S.APROB_TIPO1 PER" & vbCrLf
sConsulta = sConsulta & "                           FROM LINEAS_PEDIDO LP " & vbCrLf
sConsulta = sConsulta & "                                    INNER JOIN SEGURIDAD S" & vbCrLf
sConsulta = sConsulta & "                                            ON LP.SEGURIDAD = S.ID ) T1" & vbCrLf
sConsulta = sConsulta & "            ON OE.ID = T1.ORDEN" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN (SELECT DISTINCT ORDEN , AL.PER PER" & vbCrLf
sConsulta = sConsulta & "                            FROM LINEAS_PEDIDO LP " & vbCrLf
sConsulta = sConsulta & "                                     INNER JOIN APROB_LIM AL" & vbCrLf
sConsulta = sConsulta & "                                             ON LP.APROB_LIM = AL.ID) T2" & vbCrLf
sConsulta = sConsulta & "            ON OE.ID = T2.ORDEN '" & vbCrLf
sConsulta = sConsulta & "set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN PER P1" & vbCrLf
sConsulta = sConsulta & "            ON PE.PER = P1.COD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN ORDEN_EST OES" & vbCrLf
sConsulta = sConsulta & "            ON OE.ORDEN_EST = OES.ID'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if @ARTINT is not null or @ARTDEN is not null or @ARTEXT is not null" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "   set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN (SELECT DISTINCT ORDEN" & vbCrLf
sConsulta = sConsulta & "                  FROM LINEAS_PEDIDO LP" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN ITEM I " & vbCrLf
sConsulta = sConsulta & "             ON LP.ANYO = I.ANYO" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN1=I.GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND LP.PROCE=I.PROCE " & vbCrLf
sConsulta = sConsulta & "            AND LP.ITEM=I.ID'" & vbCrLf
sConsulta = sConsulta & "      IF @ARTDEN is NOT NULL " & vbCrLf
sConsulta = sConsulta & "         set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN ART4 " & vbCrLf
sConsulta = sConsulta & "             ON LP.GMN1=ART4.GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN2=ART4.GMN2 " & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN3=ART4.GMN3 " & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN4=ART4.GMN4 " & vbCrLf
sConsulta = sConsulta & "            AND LP.ART_INT=ART4.COD" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN ART4 ART_AUX " & vbCrLf
sConsulta = sConsulta & "             ON ART_AUX.COD=I.ART " & vbCrLf
sConsulta = sConsulta & "            AND ART_AUX.GMN1=I.GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND ART_AUX.GMN2=I.GMN2 " & vbCrLf
sConsulta = sConsulta & "            AND ART_AUX.GMN3=I.GMN3 " & vbCrLf
sConsulta = sConsulta & "            AND ART_AUX.GMN4=I.GMN4'" & vbCrLf
sConsulta = sConsulta & "      IF @ARTEXT is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN PROVE_ART4 PA" & vbCrLf
sConsulta = sConsulta & "             ON LP.PROVE = PA.PROVE" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN1 = PA.GMN1" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN2 = PA.GMN2" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN3 = PA.GMN3" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN4 = PA.GMN4" & vbCrLf
sConsulta = sConsulta & "            AND CASE WHEN LP.ART_INT IS NULL THEN I.ART ELSE LP.ART_INT END = PA.ART" & vbCrLf
sConsulta = sConsulta & "'" & vbCrLf
sConsulta = sConsulta & "set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & " WHERE 1=1'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @ARTINT is NOT NULL " & vbCrLf
sConsulta = sConsulta & "     begin " & vbCrLf
sConsulta = sConsulta & "   IF @COINCIDENCIA=1 " & vbCrLf
sConsulta = sConsulta & "          set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                   AND (LP.ART_INT + I.ART like ''% ''  + @ARTINT + '' %''' +" & vbCrLf
sConsulta = sConsulta & "          ' OR LP.ART_INT + I.ART like @ARTINT  + '' %''' +" & vbCrLf
sConsulta = sConsulta & "          ' OR LP.ART_INT + I.ART like ''% '' + @ARTINT  ' +" & vbCrLf
sConsulta = sConsulta & "          ' OR LP.ART_INT + I.ART=@ARTINT)'" & vbCrLf

sConsulta = sConsulta & "        ELSE" & vbCrLf
sConsulta = sConsulta & "      set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                   AND LP.ART_INT + I.ART like @ARTINT + ''%'' '" & vbCrLf
sConsulta = sConsulta & "     end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @ARTDEN is NOT NULL " & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "      IF @COINCIDENCIA=1 " & vbCrLf
sConsulta = sConsulta & "         set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                   AND (ART4.DEN + I.DESCR  + ART_AUX.DEN like ''% ''  + @ARTDEN + '' %''' + " & vbCrLf
sConsulta = sConsulta & "          ' OR ART4.DEN + I.DESCR  + ART_AUX.DEN like @ARTDEN + '' %''' +" & vbCrLf
sConsulta = sConsulta & "          ' OR ART4.DEN + I.DESCR  + ART_AUX.DEN like ''% '' + @ARTDEN ' + " & vbCrLf
sConsulta = sConsulta & "          ' OR ART4.DEN + I.DESCR  + ART_AUX.DEN=@ARTDEN)'" & vbCrLf
sConsulta = sConsulta & "      ELSE" & vbCrLf
sConsulta = sConsulta & "         set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                   AND ART4.DEN + I.DESCR  + ART_AUX.DEN like + ''%'' + @ARTDEN + ''%'''" & vbCrLf
sConsulta = sConsulta & "    END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @ARTEXT is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                   AND PA.COD_EXT like @ARTEXT + ''%''" & vbCrLf
sConsulta = sConsulta & "'" & vbCrLf
sConsulta = sConsulta & "   set  @SQL = @SQL + N') LP" & vbCrLf
sConsulta = sConsulta & "            ON OE.ID = LP.ORDEN" & vbCrLf
sConsulta = sConsulta & "'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                 WHERE PE.TIPO=1'" & vbCrLf
sConsulta = sConsulta & "        " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if @DESDEFECHA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.FECHA >= CONVERT(datetime,@DESDEFECHA,103)'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if @HASTAFECHA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.FECHA <= CONVERT(datetime,@HASTAFECHA,103)'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if @PROVECOD IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.PROVE = @PROVECOD'" & vbCrLf
sConsulta = sConsulta & "if @NUMPEDPROVE IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.NUMEXT = @NUMPEDPROVE '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if @ORDEN IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.NUM = @ORDEN '" & vbCrLf
sConsulta = sConsulta & "if @ANYO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.ANYO = @ANYO '" & vbCrLf
sConsulta = sConsulta & "if @PEDIDO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND PE.NUM = @PEDIDO '" & vbCrLf
sConsulta = sConsulta & "if @PROVEDEN IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND P.DEN  LIKE @PROVEDEN + ''%'' '" & vbCrLf
sConsulta = sConsulta & "IF @APROBADOR IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND (T1.PER = @APROBADOR " & vbCrLf
sConsulta = sConsulta & "                           OR T2.PER = @APROBADOR) '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if @APROVISIONADOR IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND PE.PER = @APROVISIONADOR '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "declare @IN varchar(500)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @IN = '('" & vbCrLf
sConsulta = sConsulta & "IF @EST & 1 = 1" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '0,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 2 = 2" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '1,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 4 = 4" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '2,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 8 = 8" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '3,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 16 = 16" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '4,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 32 = 32" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '5,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 64 = 64" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '6,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 128 = 128" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '20,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 256 = 256" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '21,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 512 = 512" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '22,'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL1 = ''" & vbCrLf
sConsulta = sConsulta & "SET @SQL2 = ''" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @IN!='('" & vbCrLf
sConsulta = sConsulta & "   --set @SQL = @SQL + '" & vbCrLf
sConsulta = sConsulta & "   --AND OE.EST IN ' + LEFT(@IN,LEN(@IN)-1) + ') '" & vbCrLf
sConsulta = sConsulta & "     set @SQL1=' OE.EST IN ' + LEFT(@IN,LEN(@IN)-1) + ') '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @RECINCORRECTO IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "  IF @RECINCORRECTO=1" & vbCrLf
sConsulta = sConsulta & "   --set @SQL = @SQL + '" & vbCrLf
sConsulta = sConsulta & "   --AND OE.INCORRECTA=1'" & vbCrLf
sConsulta = sConsulta & "   set @SQL2 = ' OE.INCORRECTA=1'  " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @SQL1<>'' and @SQL2<>'' " & vbCrLf
sConsulta = sConsulta & "   SET @SQL= @SQL + ' AND (' + @SQL1 + ' OR ' + @SQL2 + ')'" & vbCrLf
sConsulta = sConsulta & "ELSE " & vbCrLf
sConsulta = sConsulta & "if @SQL1!=''" & vbCrLf
sConsulta = sConsulta & "   SET @SQL= @SQL + ' AND ' + @SQL1 " & vbCrLf
sConsulta = sConsulta & "else" & vbCrLf
sConsulta = sConsulta & "if @SQL2!=''" & vbCrLf
sConsulta = sConsulta & "   SET @SQL= @SQL + ' AND ' + @SQL2" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "if @ID IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + '" & vbCrLf
sConsulta = sConsulta & "   AND OE.ID=@ID '" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "if @DESDEID IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + '" & vbCrLf
sConsulta = sConsulta & "   AND CONVERT(VARCHAR,OE.EST) + LEFT(''00000000'',8-LEN(CONVERT(VARCHAR,OE.ID))) + CONVERT(VARCHAR,OE.ID)>=@DESDEID '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "set @SQL = @SQL + '" & vbCrLf
sConsulta = sConsulta & "order by CONVERT(VARCHAR,OE.EST) + LEFT(''00000000'',8-LEN(CONVERT(VARCHAR,OE.ID))) + CONVERT(VARCHAR,OE.ID)'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @PARAM = N'@APROBADOR varchar(50), @APROVISIONADOR varchar(50), @DESDEFECHA varchar(10), @HASTAFECHA varchar(10), @ARTINT varchar(50), @ARTDEN varchar(500), @ARTEXT varchar(500), @PROVECOD varchar(50), @PROVEDEN varchar(500), @NUMPEDPROVE varchar(100),@ORDEN int, @ANYO int, @PEDIDO int, @DESDEID varchar(50), @EST int, @ID int'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, @PARAM, @APROBADOR= @APROBADOR, @APROVISIONADOR = @APROVISIONADOR, @DESDEFECHA=@DESDEFECHA, @HASTAFECHA=@HASTAFECHA, @ARTINT=@ARTINT, @ARTDEN=@ARTDEN, @ARTEXT =@ARTEXT , @PROVECOD=@PROVECOD, @PROVEDEN=@PROVEDEN, @NUMPEDPROVE=@NUMPEDPROVE, @ANYO=@ANYO, @ORDEN=@ORDEN, @PEDIDO = @PEDIDO,@DESDEID=@DESDEID, @EST=@EST, @ID = @ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


End Function

Private Function V_2_11_Storeds_01_4()

Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_NUM_ARTICULOS_CATALOGO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_NUM_ARTICULOS_CATALOGO]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSEP_NUM_ARTICULOS_CATALOGO(@COD VARCHAR(40), @DEN VARCHAR(300), @PROVE VARCHAR(300)," & vbCrLf
sConsulta = sConsulta & "@CATN1 INTEGER, @CATN2 INTEGER, @CATN3 INTEGER, @CATN4 INTEGER, " & vbCrLf
sConsulta = sConsulta & "@CATN5 INTEGER, @PRECIOMAX FLOAT,@PER VARCHAR(50),@MAXLINEAS INTEGER,@COINCIDENCIA INTEGER=NULL)  AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PARAM NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING='" & vbCrLf
sConsulta = sConsulta & "SELECT catalog_lin.art_int + ITEM.ART +  convert(varchar,catalog_lin.ID)  " & vbCrLf
sConsulta = sConsulta & "  FROM CATALOG_LIN INNER JOIN PROVE ON CATALOG_LIN.PROVE=PROVE.COD'" & vbCrLf
sConsulta = sConsulta & "IF @PROVE is not null" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' " & vbCrLf
sConsulta = sConsulta & "   AND PROVE.COD = @PROVE'" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING= @SQLSTRING + ' " & vbCrLf
sConsulta = sConsulta & "          INNER JOIN APROB_LIM " & vbCrLf
sConsulta = sConsulta & "                  ON CATALOG_LIN.SEGURIDAD=APROB_LIM.SEGURIDAD " & vbCrLf
sConsulta = sConsulta & "                 AND APROB_LIM.PER=@PER'" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING= @SQLSTRING + ' " & vbCrLf
sConsulta = sConsulta & " LEFT JOIN ITEM ON CATALOG_LIN.ANYO=ITEM.ANYO AND CATALOG_LIN.PROCE=ITEM.PROCE AND CATALOG_LIN.GMN1=ITEM.GMN1 AND CATALOG_LIN.ITEM=ITEM.ID'" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING= @SQLSTRING + ' " & vbCrLf
sConsulta = sConsulta & " LEFT JOIN ART4 ON CATALOG_LIN.GMN1=ART4.GMN1 AND CATALOG_LIN.GMN2=ART4.GMN2 AND CATALOG_LIN.GMN3=ART4.GMN3 AND " & vbCrLf
sConsulta = sConsulta & " CATALOG_LIN.GMN4=ART4.GMN4 AND CATALOG_LIN.ART_INT=ART4.COD '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING= @SQLSTRING + '" & vbCrLf
sConsulta = sConsulta & " LEFT JOIN ART4 ART_AUX ON ITEM.GMN1=ART_AUX.GMN1 AND ITEM.GMN2=ART_AUX.GMN2 AND ITEM.GMN3=ART_AUX.GMN3 AND ITEM.GMN4=ART_AUX.GMN4 AND ITEM.ART=ART_AUX.COD '" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING= @SQLSTRING +'  " & vbCrLf
sConsulta = sConsulta & " WHERE PUB=1  AND BAJALOG = 0 '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @COD is not null" & vbCrLf
sConsulta = sConsulta & " BEGIN" & vbCrLf
sConsulta = sConsulta & "  IF @COINCIDENCIA=0 " & vbCrLf
sConsulta = sConsulta & "   SET @SQLSTRING= @SQLSTRING +' AND (CATALOG_LIN.ART_INT + ITEM.ART LIKE ''% ''  +  @COD  + '' %''' +" & vbCrLf
sConsulta = sConsulta & "   ' OR CATALOG_LIN.ART_INT + ITEM.ART LIKE @COD  + '' %''' +" & vbCrLf
sConsulta = sConsulta & "   ' OR CATALOG_LIN.ART_INT + ITEM.ART LIKE ''% '' + @COD  ' + " & vbCrLf
sConsulta = sConsulta & "   ' OR CATALOG_LIN.ART_INT + ITEM.ART=@COD)'" & vbCrLf
sConsulta = sConsulta & "  ELSE" & vbCrLf
sConsulta = sConsulta & "   SET @SQLSTRING= @SQLSTRING +' AND CATALOG_LIN.ART_INT + ITEM.ART LIKE  @COD + ''%'''" & vbCrLf
sConsulta = sConsulta & " END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @DEN is not null" & vbCrLf
sConsulta = sConsulta & " BEGIN" & vbCrLf
sConsulta = sConsulta & "   IF @COINCIDENCIA=0 " & vbCrLf
sConsulta = sConsulta & "   SET @SQLSTRING = @SQLSTRING + ' AND (ITEM.DESCR + ART4.DEN + ART_AUX.DEN LIKE ''% ''  +  @DEN  + '' %''' +" & vbCrLf
sConsulta = sConsulta & "   ' OR ITEM.DESCR + ART4.DEN + ART_AUX.DEN LIKE @DEN  + '' %''' +" & vbCrLf
sConsulta = sConsulta & "   ' OR ITEM.DESCR + ART4.DEN + ART_AUX.DEN LIKE ''% '' + @DEN ' + " & vbCrLf
sConsulta = sConsulta & "   ' OR ITEM.DESCR + ART4.DEN + ART_AUX.DEN=@DEN )'" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "        SET @SQLSTRING = @SQLSTRING + ' AND ITEM.DESCR + ART4.DEN + ART_AUX.DEN LIKE ''%'' + @DEN + ''%'''" & vbCrLf
sConsulta = sConsulta & " END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CATN1 is not null" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING = @SQLSTRING + ' " & vbCrLf
sConsulta = sConsulta & "   AND CATALOG_LIN.CAT1=@CATN1'" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "IF @CATN2 is not null" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING = @SQLSTRING + ' " & vbCrLf
sConsulta = sConsulta & "   AND CATALOG_LIN.CAT2=@CATN2'" & vbCrLf
sConsulta = sConsulta & "IF @CATN3 is not null" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING = @SQLSTRING + ' " & vbCrLf
sConsulta = sConsulta & "   AND CATALOG_LIN.CAT3=@CATN3'" & vbCrLf
sConsulta = sConsulta & "IF @CATN4 is not null" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING = @SQLSTRING + ' " & vbCrLf
sConsulta = sConsulta & "   AND CATALOG_LIN.CAT4=@CATN4'" & vbCrLf
sConsulta = sConsulta & "IF @CATN5 is not null" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING = @SQLSTRING + '" & vbCrLf
sConsulta = sConsulta & "   AND CATALOG_LIN.CAT5=@CATN5'" & vbCrLf
sConsulta = sConsulta & "IF @PRECIOMAX is not null" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING = @SQLSTRING + '" & vbCrLf
sConsulta = sConsulta & "   AND CATALOG_LIN.PRECIO_UC<=@PRECIOMAX'" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING = @SQLSTRING + '" & vbCrLf
sConsulta = sConsulta & "ORDER BY catalog_lin.art_int + ITEM.ART +  convert(varchar,catalog_lin.ID) '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @PARAM = '@COD VARCHAR(40), @DEN VARCHAR(300), @PROVE VARCHAR(300)," & vbCrLf
sConsulta = sConsulta & "@CATN1 INTEGER, @CATN2 INTEGER, @CATN3 INTEGER, @CATN4 INTEGER, " & vbCrLf
sConsulta = sConsulta & "@CATN5 INTEGER, @PRECIOMAX FLOAT,@PER VARCHAR(50)'" & vbCrLf
sConsulta = sConsulta & "SET @SQL = '" & vbCrLf
sConsulta = sConsulta & "create table #IDS (id varchar(200))" & vbCrLf
sConsulta = sConsulta & "set nocount on" & vbCrLf
sConsulta = sConsulta & "DECLARE C1 CURSOR FOR " & vbCrLf
sConsulta = sConsulta & "' + @SQLSTRING + '" & vbCrLf
sConsulta = sConsulta & "DECLARE @miID varchar(200)" & vbCrLf
sConsulta = sConsulta & "declare @cont int" & vbCrLf
sConsulta = sConsulta & "set @cont=1" & vbCrLf
sConsulta = sConsulta & "OPEN C1" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C1 INTO @miID" & vbCrLf
sConsulta = sConsulta & "while @@fetch_status=0" & vbCrLf
sConsulta = sConsulta & "begin" & vbCrLf
sConsulta = sConsulta & "if @cont = 1" & vbCrLf
sConsulta = sConsulta & " insert into #IDS values (@miID)" & vbCrLf
sConsulta = sConsulta & "set @cont = @cont + 1        " & vbCrLf
sConsulta = sConsulta & "     " & vbCrLf
sConsulta = sConsulta & "if @cont = ' + CONVERT(VARCHAR,@MAXLINEAS) + ' + 1" & vbCrLf
sConsulta = sConsulta & " set @cont = 1" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C1 INTO @miID" & vbCrLf
sConsulta = sConsulta & "end" & vbCrLf
sConsulta = sConsulta & "CLOSE C1" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C1" & vbCrLf
sConsulta = sConsulta & "set nocount off" & vbCrLf
sConsulta = sConsulta & "select * from #ids" & vbCrLf
sConsulta = sConsulta & "'" & vbCrLf
sConsulta = sConsulta & "EXECUTE sp_executesql @SQL,@PARAM," & vbCrLf
sConsulta = sConsulta & "@COD=@COD, " & vbCrLf
sConsulta = sConsulta & "@DEN=@DEN, " & vbCrLf
sConsulta = sConsulta & "@PROVE=@PROVE," & vbCrLf
sConsulta = sConsulta & "@CATN1=@CATN1," & vbCrLf
sConsulta = sConsulta & "@CATN2=@CATN2, " & vbCrLf
sConsulta = sConsulta & "@CATN3=@CATN3," & vbCrLf
sConsulta = sConsulta & "@CATN4=@CATN4," & vbCrLf
sConsulta = sConsulta & "@CATN5=@CATN5, " & vbCrLf
sConsulta = sConsulta & "@PRECIOMAX=@PRECIOMAX, " & vbCrLf
sConsulta = sConsulta & "@PER=@PER" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_DEVOLVER_GRUPO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_DEVOLVER_GRUPO]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_DEVOLVER_GRUPO @ANYO INT, @GMN1 VARCHAR(20),@PROCE INT ,@GRUPO VARCHAR(20), @PROVE VARCHAR(50) = null, @OFE int = NULL, @MON VARCHAR(50) = NULL AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @HAYESP TINYINT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @HAYATRIB TINYINT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @EQUIVPROC FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @CAM FLOAT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @EQUIVPROC = CAMBIO" & vbCrLf
sConsulta = sConsulta & "  FROM PROCE " & vbCrLf
sConsulta = sConsulta & " WHERE ANYO=@ANYO" & vbCrLf
sConsulta = sConsulta & "   AND GMN1=@GMN1" & vbCrLf
sConsulta = sConsulta & "   AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @CAM =( SELECT CAMBIO" & vbCrLf
sConsulta = sConsulta & "             FROM PROCE_OFE" & vbCrLf
sConsulta = sConsulta & "            WHERE ANYO=@ANYO" & vbCrLf
sConsulta = sConsulta & "              AND GMN1=@GMN1" & vbCrLf
sConsulta = sConsulta & "              AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "              AND PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "              AND OFE=@OFE)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CAM IS NULL" & vbCrLf
sConsulta = sConsulta & "  SELECT @CAM =  EQUIV / @EQUIVPROC" & vbCrLf
sConsulta = sConsulta & "    FROM MON " & vbCrLf
sConsulta = sConsulta & "   WHERE COD = @MON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @HAYESP = CASE WHEN ESP IS NULL OR ESP='' THEN 0 ELSE 1 END " & vbCrLf
sConsulta = sConsulta & "  FROM PROCE_GRUPO" & vbCrLf
sConsulta = sConsulta & " WHERE ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "   AND GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "   AND PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "   AND COD = @GRUPO" & vbCrLf
sConsulta = sConsulta & "IF @HAYESP = 0" & vbCrLf
sConsulta = sConsulta & "  IF (SELECT count(id) " & vbCrLf
sConsulta = sConsulta & "        FROM PROCE_GRUPO_ESP" & vbCrLf
sConsulta = sConsulta & "       WHERE ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "         and GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "         and PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "         AND GRUPO = @GRUPO)>0" & vbCrLf
sConsulta = sConsulta & "    SET @HAYESP = 1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @HAYATRIB = 0" & vbCrLf
sConsulta = sConsulta & "IF (SELECT COUNT(*)" & vbCrLf
sConsulta = sConsulta & "      FROM PROCE_ATRIB" & vbCrLf
sConsulta = sConsulta & "     WHERE ANYO  = @ANYO" & vbCrLf
sConsulta = sConsulta & "       and GMN1  = @GMN1 " & vbCrLf
sConsulta = sConsulta & "       and PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "       AND AMBITO = 2)>0" & vbCrLf
sConsulta = sConsulta & "  SET @HAYATRIB = 1  " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "DECLARE @IMPORTEOFERTA float" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if @PROVE IS NOT NULL AND @OFE IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SELECT @IMPORTEOFERTA = SUM(ISNULL(IO.PRECIO,0) * ISNULL(I.CANT,0)) " & vbCrLf
sConsulta = sConsulta & "  FROM ITEM_OFE IO" & vbCrLf
sConsulta = sConsulta & "         INNER JOIN ITEM I" & vbCrLf
sConsulta = sConsulta & "                 ON IO.PROCE = I.PROCE" & vbCrLf
sConsulta = sConsulta & "                AND IO.GMN1 = I.GMN1" & vbCrLf
sConsulta = sConsulta & "                AND IO.ANYO = I.ANYO" & vbCrLf
sConsulta = sConsulta & "                AND IO.ITEM =I.ID" & vbCrLf
sConsulta = sConsulta & " WHERE IO.ANYO  = @ANYO" & vbCrLf
sConsulta = sConsulta & "   AND IO.GMN1  = @GMN1 " & vbCrLf
sConsulta = sConsulta & "   AND IO.PROCE = @PROCE " & vbCrLf
sConsulta = sConsulta & "   AND IO.PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "   AND IO.NUM   = @OFE" & vbCrLf
sConsulta = sConsulta & "   AND I.GRUPO = @GRUPO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT PG.COD, PG.DEN, PG.DESCR, PG.DEST, PG.PAG, PAG.DEN DENPAG, PG.FECINI, PG.FECFIN, PG.ESP, @HAYESP HAYESP, @HAYATRIB HAYATRIB, @IMPORTEOFERTA/(@EQUIVPROC*@CAM)  IMPORTEOFERTA" & vbCrLf
sConsulta = sConsulta & "  FROM PROCE_GRUPO PG" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN PAG" & vbCrLf
sConsulta = sConsulta & "               ON PG.PAG = PAG.COD" & vbCrLf
sConsulta = sConsulta & " WHERE PG.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "   AND PG.GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "   AND PG.PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "   AND PG.COD = @GRUPO" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_DEVOLVER_OFERTAS_MINIMAS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_DEVOLVER_OFERTAS_MINIMAS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_DEVOLVER_OFERTAS_MINIMAS @ANYO INT,@GMN1 VARCHAR(50),@PROCE INT, @PROVE varchar(50), @CODMON varchar(50), @FECLIMITE DATETIME OUTPUT , @VERPROVE tinyint OUTPUT, @VERDETALLE tinyint OUTPUT AS " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "set NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @EQUIV FLOAT " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @EQUIV= MON.EQUIV  FROM MON WHERE MON.COD=@CODMON " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @FECLIMITE = FECLIMOFE , @VERPROVE = SUBASTAPROVE, @VERDETALLE = SUBASTAPUJAS" & vbCrLf
sConsulta = sConsulta & "  FROM PROCE P" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN PROCE_DEF PD" & vbCrLf
sConsulta = sConsulta & "            ON P.ANYO = PD.ANYO" & vbCrLf
sConsulta = sConsulta & "           AND P.GMN1 = PD.GMN1" & vbCrLf
sConsulta = sConsulta & "           AND P.COD = PD.PROCE" & vbCrLf
sConsulta = sConsulta & " WHERE P.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "   AND P.GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "   AND P.COD = @PROCE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT O.ITEM, I.GRUPO, O.PROVE MINPROVE, P2.DEN MINPROVEDEN, I.ART, I.DESCR + A.DEN AS DESCR, " & vbCrLf
sConsulta = sConsulta & "       O.PRECIO*@EQUIV MINPRECIO, CASE WHEN @PROVE = CASE WHEN O.PROVE IS NULL THEN @PROVE ELSE O.PROVE END THEN 1 ELSE 0 END PROPIA" & vbCrLf
sConsulta = sConsulta & "  FROM OFEMIN O " & vbCrLf
sConsulta = sConsulta & "     left JOIN PROVE P2 " & vbCrLf
sConsulta = sConsulta & "             ON O.PROVE = P2.COD" & vbCrLf
sConsulta = sConsulta & "     INNER JOIN (ITEM I" & vbCrLf
sConsulta = sConsulta & "                 LEFT JOIN ART4 A" & vbCrLf
sConsulta = sConsulta & "                        ON I.ART = A.COD " & vbCrLf
sConsulta = sConsulta & "                       AND I.GMN4 = A.GMN4 " & vbCrLf
sConsulta = sConsulta & "                       AND I.GMN3 = A.GMN3 " & vbCrLf
sConsulta = sConsulta & "                       AND I.GMN2 = A.GMN2" & vbCrLf
sConsulta = sConsulta & "                       AND I.GMN1 = A.GMN1)" & vbCrLf
sConsulta = sConsulta & "             ON O.ANYO = I.ANYO" & vbCrLf
sConsulta = sConsulta & "            AND O.GMN1 = I.GMN1" & vbCrLf
sConsulta = sConsulta & "            AND O.PROCE = I.PROCE" & vbCrLf
sConsulta = sConsulta & "            AND O.ITEM = I.ID" & vbCrLf
sConsulta = sConsulta & " WHERE O.ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "   AND O.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "   AND O.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_DEVOLVER_OFERTA_PROVE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_DEVOLVER_OFERTA_PROVE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_DEVOLVER_OFERTA_PROVE @ANYO INTEGER,@GMN1 VARCHAR(20),@PROCE INTEGER ,@PROVE VARCHAR(50), @OFE smallint = null AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @LASTOFEENV smallint" & vbCrLf
sConsulta = sConsulta & "DECLARE @LASTOFE smallint" & vbCrLf
sConsulta = sConsulta & "DECLARE @NUMOFEENVIADAS smallint" & vbCrLf
sConsulta = sConsulta & "DECLARE @FECHAULTOFE datetime" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "SELECT @NUMOFEENVIADAS  = COUNT(OFE)" & vbCrLf
sConsulta = sConsulta & "  FROM PROCE_OFE " & vbCrLf
sConsulta = sConsulta & " WHERE ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "   AND GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "   AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "   AND PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "   AND ENVIADA = 1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @LASTOFEENV = MAX(OFE)" & vbCrLf
sConsulta = sConsulta & "  FROM PROCE_OFE " & vbCrLf
sConsulta = sConsulta & " WHERE ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "   AND GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "   AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "   AND PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "   AND ENVIADA = 1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "select @FECHAULTOFE = FECREC" & vbCrLf
sConsulta = sConsulta & "  FROM PROCE_OFE " & vbCrLf
sConsulta = sConsulta & " WHERE ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "   AND GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "   AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "   AND PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "   AND OFE = @LASTOFEENV" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @OFE IS NULL" & vbCrLf
sConsulta = sConsulta & "  IF EXISTS (SELECT * " & vbCrLf
sConsulta = sConsulta & "               FROM PROCE_OFE " & vbCrLf
sConsulta = sConsulta & "              WHERE ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "                AND GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "                AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "                AND PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "                AND OFE = -1)" & vbCrLf
sConsulta = sConsulta & "    SET @LASTOFE = -1" & vbCrLf
sConsulta = sConsulta & "  ELSE" & vbCrLf
sConsulta = sConsulta & "    SELECT @LASTOFE = MAX(OFE)" & vbCrLf
sConsulta = sConsulta & "      FROM PROCE_OFE" & vbCrLf
sConsulta = sConsulta & "     WHERE ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "       AND GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "       AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "       AND PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @LASTOFE =@OFE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @TOTALADJUNTOS int" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @TOTALADJUNTOS =(SELECT ISNULL(SUM(DATASIZE) ,0)" & vbCrLf
sConsulta = sConsulta & "                       FROM PROCE_OFE_ADJUN" & vbCrLf
sConsulta = sConsulta & "                      WHERE ANYO  = @ANYO" & vbCrLf
sConsulta = sConsulta & "                        AND GMN1  = @GMN1 " & vbCrLf
sConsulta = sConsulta & "                        AND PROCE = @PROCE " & vbCrLf
sConsulta = sConsulta & "                        AND PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "                        AND OFE   = @LASTOFE)" & vbCrLf
sConsulta = sConsulta & "SET @TOTALADJUNTOS = @TOTALADJUNTOS +" & vbCrLf
sConsulta = sConsulta & "(SELECT ISNULL(SUM(DATASIZE) ,0)" & vbCrLf
sConsulta = sConsulta & "   FROM OFE_GR_ADJUN" & vbCrLf
sConsulta = sConsulta & "  WHERE ANYO  = @ANYO" & vbCrLf
sConsulta = sConsulta & "    AND GMN1  = @GMN1 " & vbCrLf
sConsulta = sConsulta & "    AND PROCE = @PROCE " & vbCrLf
sConsulta = sConsulta & "    AND PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "    AND OFE   = @LASTOFE)" & vbCrLf
sConsulta = sConsulta & "SET @TOTALADJUNTOS = @TOTALADJUNTOS +" & vbCrLf
sConsulta = sConsulta & "(SELECT ISNULL(SUM(DATASIZE) ,0)" & vbCrLf
sConsulta = sConsulta & "   FROM OFE_ITEM_ADJUN" & vbCrLf
sConsulta = sConsulta & "  WHERE ANYO  = @ANYO" & vbCrLf
sConsulta = sConsulta & "    AND GMN1  = @GMN1 " & vbCrLf
sConsulta = sConsulta & "    AND PROCE = @PROCE " & vbCrLf
sConsulta = sConsulta & "    AND PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "    AND OFE   = @LASTOFE)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @IMPORTEOFERTA float" & vbCrLf
sConsulta = sConsulta & "SELECT @IMPORTEOFERTA = SUM(ISNULL(IO.PRECIO,0) * ISNULL(I.CANT,0)) " & vbCrLf
sConsulta = sConsulta & "  FROM ITEM_OFE IO" & vbCrLf
sConsulta = sConsulta & "         INNER JOIN ITEM I" & vbCrLf
sConsulta = sConsulta & "                 ON IO.PROCE = I.PROCE" & vbCrLf
sConsulta = sConsulta & "                AND IO.GMN1 = I.GMN1" & vbCrLf
sConsulta = sConsulta & "                AND IO.ANYO = I.ANYO" & vbCrLf
sConsulta = sConsulta & "                AND IO.ITEM =I.ID" & vbCrLf
sConsulta = sConsulta & " WHERE IO.ANYO  = @ANYO" & vbCrLf
sConsulta = sConsulta & "   AND IO.GMN1  = @GMN1 " & vbCrLf
sConsulta = sConsulta & "   AND IO.PROCE = @PROCE " & vbCrLf
sConsulta = sConsulta & "   AND IO.PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "   AND IO.NUM   = @LASTOFE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT PO.ANYO, PO.GMN1, PO.PROCE, PO.PROVE, PO.OFE, PO.EST, PO.FECREC," & vbCrLf
sConsulta = sConsulta & "       PO.FECVAL, PO.MON, MON.DEN MONDEN, PO.CAMBIO, PO.OBS , @LASTOFEENV LASTOFEENV, " & vbCrLf
sConsulta = sConsulta & "       @FECHAULTOFE FECHAULTOFE, @NUMOFEENVIADAS NUMOFEENVIADAS, ENVIADA ESTULTOFE,PO.FECACT, @TOTALADJUNTOS TOTALADJUNTOS, @IMPORTEOFERTA IMPORTEOFERTA" & vbCrLf
sConsulta = sConsulta & "  FROM PROCE_OFE PO " & vbCrLf
sConsulta = sConsulta & "    INNER JOIN MON " & vbCrLf
sConsulta = sConsulta & "            ON PO.MON = MON.COD" & vbCrLf
sConsulta = sConsulta & " WHERE ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "   AND GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "   AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "   AND PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "   AND OFE = @LASTOFE" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_LIMITES_SUBASTA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_LIMITES_SUBASTA]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_LIMITES_SUBASTA @ANYO SMALLINT, @GMN1 varchar(50), @PROCE int, @PROVE varchar(50), @MON varchar(50) = null AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @CAM FLOAT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @CAM =  EQUIV " & vbCrLf
sConsulta = sConsulta & "  FROM MON " & vbCrLf
sConsulta = sConsulta & " WHERE COD = @MON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT I.GRUPO, I.ID, I.LIMITE, IO.PRECIO, CASE WHEN PO.CAMBIO IS NULL THEN @CAM / P.CAMBIO   ELSE PO.CAMBIO END POCAMBIO,  P.CAMBIO PCAMBIO, O.PRECIO OFEMIN, @CAM / P.CAMBIO  NUEVOCAMBIO" & vbCrLf
sConsulta = sConsulta & "  FROM ITEM I" & vbCrLf
sConsulta = sConsulta & "    LEFT JOIN (ITEM_OFE IO" & vbCrLf
sConsulta = sConsulta & "                INNER JOIN PROCE_OFE PO" & vbCrLf
sConsulta = sConsulta & "                        ON IO.ANYO = PO.ANYO" & vbCrLf
sConsulta = sConsulta & "                       AND IO.GMN1 = PO.GMN1" & vbCrLf
sConsulta = sConsulta & "                       AND IO.PROCE = PO.PROCE" & vbCrLf
sConsulta = sConsulta & "                       AND IO.PROVE = PO.PROVE" & vbCrLf
sConsulta = sConsulta & "                       AND IO.NUM = PO.OFE)" & vbCrLf
sConsulta = sConsulta & "           ON I.ANYO = IO.ANYO" & vbCrLf
sConsulta = sConsulta & "          AND I.GMN1 = IO.GMN1" & vbCrLf
sConsulta = sConsulta & "          AND I.PROCE = IO.PROCE" & vbCrLf
sConsulta = sConsulta & "          AND I.ID = IO.ITEM" & vbCrLf
sConsulta = sConsulta & "          AND @PROVE = IO.PROVE" & vbCrLf
sConsulta = sConsulta & "          AND 1 = IO.ULT" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN OFEMIN O" & vbCrLf
sConsulta = sConsulta & "           ON I.ANYO = O.ANYO" & vbCrLf
sConsulta = sConsulta & "          AND I.GMN1 = O.GMN1" & vbCrLf
sConsulta = sConsulta & "          AND I.PROCE = O.PROCE" & vbCrLf
sConsulta = sConsulta & "          AND I.ID = O.ITEM" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PROCE P" & vbCrLf
sConsulta = sConsulta & "           ON I.ANYO = P.ANYO" & vbCrLf
sConsulta = sConsulta & "          AND I.GMN1 = P.GMN1" & vbCrLf
sConsulta = sConsulta & "          AND I.PROCE = P.COD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                  " & vbCrLf
sConsulta = sConsulta & " WHERE I.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "   AND I.GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "   AND I.PROCE = @PROCE" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Function

Private Sub V_2_11_Triggers_01()
Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PROCE_DEF_TG_UPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PROCE_DEF_TG_UPD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TRIGGER PROCE_DEF_TG_UPD ON dbo.PROCE_DEF" & vbCrLf
sConsulta = sConsulta & "FOR UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO INT,@GMN1 VARCHAR(50),@PROCE SMALLINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVEOLD TINYINT,@PROVENEW TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @DISTOLD TINYINT,@DISTNEW TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRESANU1OLD TINYINT,@PRESANU1NEW TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRESANU2OLD TINYINT,@PRESANU2NEW TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRES1OLD TINYINT,@PRES1NEW TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRES2OLD TINYINT,@PRES2NEW TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCE_ESPOLD TINYINT,@PROCE_ESPNEW TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @GRUPO_ESPOLD TINYINT,@GRUPO_ESPNEW TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ITEM_ESPOLD TINYINT,@ITEM_ESPNEW TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @SUBASTAOLD TINYINT,@SUBASTANEW TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @BAJMINPUJAOLD TINYINT,@BAJMINPUJANEW  TINYINT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_PROCE_DEF_Upd CURSOR LOCAL FOR " & vbCrLf
sConsulta = sConsulta & "SELECT I.ANYO,I.GMN1,I.PROCE,D.DIST,I.DIST,D.PRESANU1,I.PRESANU1,D.PRESANU2,I.PRESANU2,D.PRES1,I.PRES1,D.PRES2,I.PRES2,D.SUBASTA,I.SUBASTA,D.PROVE,I.PROVE,D.PROCE_ESP,I.PROCE_ESP,D.GRUPO_ESP,I.GRUPO_ESP,D.ITEM_ESP,I.ITEM_ESP, D.SUBASTABAJMINPUJA, I.SUBASTABAJMINPUJA " & vbCrLf
sConsulta = sConsulta & "FROM DELETED D " & vbCrLf
sConsulta = sConsulta & "INNER JOIN INSERTED I" & vbCrLf
sConsulta = sConsulta & "ON D.ANYO=I.ANYO AND D.GMN1=I.GMN1 AND D.PROCE=I.PROCE " & vbCrLf
sConsulta = sConsulta & "OPEN curTG_PROCE_DEF_Upd" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_DEF_Upd INTO @ANYO,@GMN1,@PROCE,@DISTOLD,@DISTNEW,@PRESANU1OLD,@PRESANU1NEW,@PRESANU2OLD,@PRESANU2NEW,@PRES1OLD,@PRES1NEW,@PRES2OLD,@PRES2NEW,@SUBASTAOLD,@SUBASTANEW,@PROVEOLD,@PROVENEW,@PROCE_ESPOLD,@PROCE_ESPNEW,@GRUPO_ESPOLD,@GRUPO_ESPNEW,@ITEM_ESPOLD,@ITEM_ESPNEW,@BAJMINPUJAOLD,@BAJMINPUJANEW" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "--PROVE" & vbCrLf
sConsulta = sConsulta & "IF ISNULL(@PROVEOLD,'')<>ISNULL(@PROVENEW,'')" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "       IF @PROVEOLD=1 AND @PROVENEW=0" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "      UPDATE PROCE SET PROVEACT=NULL WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "      UPDATE ITEM SET PROVEACT=NULL WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "       End" & vbCrLf
sConsulta = sConsulta & "       IF @PROVEOLD=2 AND @PROVENEW=0" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "      DELETE FROM PROCE_GRUPO_DEF WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND DATO='PROVE'" & vbCrLf
sConsulta = sConsulta & "      UPDATE PROCE_GRUPO SET PROVEACT=NULL WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "      UPDATE ITEM SET PROVEACT=NULL WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "       End" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "--DISTRIBUICION DE UO" & vbCrLf
sConsulta = sConsulta & "IF ISNULL(@DISTOLD,'')<>ISNULL(@DISTNEW,'')" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "       IF @DISTOLD=1 AND @DISTNEW=0" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "      DELETE FROM PROCE_UON1 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "      DELETE FROM PROCE_UON2 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "      DELETE FROM PROCE_UON3 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "      DELETE FROM ITEM_UON1 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "      DELETE FROM ITEM_UON2 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "      DELETE FROM ITEM_UON3 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "       End" & vbCrLf
sConsulta = sConsulta & "       IF @DISTOLD=2 AND @DISTNEW=0" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "      DELETE FROM PROCE_GRUPO_DEF WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND DATO='DIST'" & vbCrLf
sConsulta = sConsulta & "      DELETE FROM PROCE_GR_UON1 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "      DELETE FROM PROCE_GR_UON2 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "      DELETE FROM PROCE_GR_UON3 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "      DELETE FROM ITEM_UON1 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "      DELETE FROM ITEM_UON2 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "      DELETE FROM ITEM_UON3 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "       End" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "--PRESUPUESTO ANUAL1" & vbCrLf
sConsulta = sConsulta & "IF ISNULL(@PRESANU1OLD,'')<>ISNULL(@PRESANU1NEW,'')" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "       IF @PRESANU1OLD=1 AND @PRESANU1NEW=0" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "      DELETE FROM PROCE_PRES1 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "      DELETE FROM ITEM_PRESPROY WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "       End" & vbCrLf
sConsulta = sConsulta & "       IF @PRESANU1OLD=2 AND @PRESANU1NEW=0" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "      DELETE FROM PROCE_GRUPO_DEF WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND DATO='PRESANU1'" & vbCrLf
sConsulta = sConsulta & "      DELETE FROM PROCE_GR_PRES1 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "      DELETE FROM ITEM_PRESPROY WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "       End" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "--PRESUPUESTO ANUAL2" & vbCrLf
sConsulta = sConsulta & "IF ISNULL(@PRESANU2OLD,'')<>ISNULL(@PRESANU2NEW,'')" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "       IF @PRESANU2OLD=1 AND @PRESANU2NEW=0" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "      DELETE FROM PROCE_PRES2 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "      DELETE FROM ITEM_PRESCON WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "       End" & vbCrLf
sConsulta = sConsulta & "       IF @PRESANU2OLD=2 AND @PRESANU2NEW=0" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "      DELETE FROM PROCE_GRUPO_DEF WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND DATO='PRESANU2'" & vbCrLf
sConsulta = sConsulta & "      DELETE FROM PROCE_GR_PRES2 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "      DELETE FROM ITEM_PRESCON WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "       End" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "--PRESUPUESTO 1" & vbCrLf
sConsulta = sConsulta & "IF ISNULL(@PRES1OLD,'')<>ISNULL(@PRES1NEW,'')" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "       IF @PRES1OLD=1 AND @PRES1NEW=0" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "      DELETE FROM PROCE_PRES3 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "      DELETE FROM ITEM_PRESCON3 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "       End" & vbCrLf
sConsulta = sConsulta & "       IF @PRES1OLD=2 AND @PRES1NEW=0" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "      DELETE FROM PROCE_GRUPO_DEF WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND DATO='PRES1'" & vbCrLf
sConsulta = sConsulta & "      DELETE FROM PROCE_GR_PRES3 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "      DELETE FROM ITEM_PRESCON3 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "       End" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "--PRESUPUESTO 2" & vbCrLf
sConsulta = sConsulta & "IF ISNULL(@PRES2OLD,'')<>ISNULL(@PRES2NEW,'')" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "       IF @PRES2OLD=1 AND @PRES2NEW=0" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "      DELETE FROM PROCE_PRES4 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "      DELETE FROM ITEM_PRESCON4 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "       End" & vbCrLf
sConsulta = sConsulta & "       IF @PRES2OLD=2 AND @PRES2NEW=0" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "      DELETE FROM PROCE_GRUPO_DEF WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND DATO='PRES2'" & vbCrLf
sConsulta = sConsulta & "      DELETE FROM PROCE_GR_PRES4 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "      DELETE FROM ITEM_PRESCON4 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "       End" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "--PROCE_ESP" & vbCrLf
sConsulta = sConsulta & "IF @PROCE_ESPOLD=1 AND @PROCE_ESPNEW=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "       UPDATE PROCE SET ESP=NULL WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "       DELETE FROM PROCE_ESP WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "--GRUPO_ESP" & vbCrLf
sConsulta = sConsulta & "IF @GRUPO_ESPOLD=1 AND @GRUPO_ESPNEW=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "       UPDATE PROCE_GRUPO SET ESP=NULL WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "       DELETE FROM PROCE_GRUPO_ESP WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "--ITEM_ESP" & vbCrLf
sConsulta = sConsulta & "IF @ITEM_ESPOLD=1 AND @ITEM_ESPNEW=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "       UPDATE ITEM SET ESP=NULL WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "       DELETE FROM ITEM_ESP WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "--SUBASTA" & vbCrLf
sConsulta = sConsulta & "IF @SUBASTANEW!=@SUBASTAOLD" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    DECLARE @ITEM INT" & vbCrLf
sConsulta = sConsulta & "    DELETE FROM OFEMIN WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "    IF @SUBASTANEW=1" & vbCrLf
sConsulta = sConsulta & "      BEGIN" & vbCrLf
sConsulta = sConsulta & "        INSERT INTO OFEMIN (ANYO,GMN1,PROCE,ITEM,PROVE,PRECIO) " & vbCrLf
sConsulta = sConsulta & "        SELECT @ANYO,@GMN1,@PROCE, ID,NULL,NULL" & vbCrLf
sConsulta = sConsulta & "          FROM ITEM" & vbCrLf
sConsulta = sConsulta & "         WHERE ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "           AND GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "           AND PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--BAJADA M�NIMA DE PUJAS" & vbCrLf
sConsulta = sConsulta & "IF @BAJMINPUJANEW=0 AND @BAJMINPUJAOLD=1" & vbCrLf
sConsulta = sConsulta & " BEGIN" & vbCrLf
sConsulta = sConsulta & "    UPDATE ITEM SET LIMITE=NULL WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & " END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_DEF_Upd INTO @ANYO,@GMN1,@PROCE,@DISTOLD,@DISTNEW,@PRESANU1OLD,@PRESANU1NEW,@PRESANU2OLD,@PRESANU2NEW,@PRES1OLD,@PRES1NEW,@PRES2OLD,@PRES2NEW,@SUBASTAOLD, @SUBASTANEW,@PROVEOLD,@PROVENEW,@PROCE_ESPOLD,@PROCE_ESPNEW,@GRUPO_ESPOLD,@GRUPO_ESPNEW,@ITEM_ESPOLD,@ITEM_ESPNEW,@BAJMINPUJAOLD,@BAJMINPUJANEW" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_PROCE_DEF_Upd" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_PROCE_DEF_Upd" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub


Private Function V_2_11_01_Updates() As Boolean
Dim sConsulta As String
Dim oADOConnection As ADODB.Connection
Dim adores As ADODB.Recordset
Dim adores2 As ADODB.Recordset
Dim adocom As ADODB.Command
Dim oParam As ADODB.Parameter
Dim iResultado As Integer

On Error GoTo error:

    Set oADOConnection = New ADODB.Connection
    oADOConnection.Open "Provider=SQLOLEDB.1;Auto Translate=FALSE;Server=" & gServer & ";Database=" & gBD & ";", gUID, gPWD
    oADOConnection.CursorLocation = adUseClient
    oADOConnection.CommandTimeout = 120
    
    oADOConnection.Execute "BEGIN TRANSACTION"
    oADOConnection.Execute "SET XACT_ABORT OFF"
    
    sConsulta = "SELECT CON.PROVE FROM CON WHERE"
    sConsulta = sConsulta & " CON.APROV =1 GROUP BY CON.PROVE"
    sConsulta = sConsulta & " HAVING COUNT(*) > 1"

    Set adores = New ADODB.Recordset
    Set adores2 = New ADODB.Recordset
    adores.Open sConsulta, oADOConnection, adOpenForwardOnly, adLockReadOnly

    While Not adores.EOF
        sConsulta = "SELECT ID FROM CON WHERE PROVE='" & DblQuote(adores("PROVE").Value) & "'"
        sConsulta = sConsulta & " AND APROV=1 ORDER BY ID"
        adores2.Open sConsulta, oADOConnection, adOpenForwardOnly, adLockReadOnly
        adores2.MoveNext
        While Not adores2.EOF
            sConsulta = "UPDATE CON SET APROV=0 WHERE  PROVE='" & DblQuote(adores("PROVE").Value) & "' AND ID=" & adores2(0).Value
            oADOConnection.Execute sConsulta
            adores2.MoveNext
        Wend
        adores2.Close
        adores.MoveNext
    Wend
    adores.Close
    Set adores = Nothing
    Set adores2 = Nothing
    
    sConsulta = "UPDATE VERSION SET NUM ='2.11.00.02'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    oADOConnection.Execute sConsulta
    
    oADOConnection.Execute "COMMIT TRANSACTION"
    oADOConnection.Close
    Set oADOConnection = Nothing
    V_2_11_01_Updates = True
    Exit Function
        
    
error:
    MsgBox oADOConnection.Errors(0).NativeError & "  " & oADOConnection.Errors(0).Description, vbCritical, "FULLSTEP"
    oADOConnection.Execute "ROLLBACK TRANSACTION"
    On Error Resume Next
    adores.Close
    Set adores = Nothing
    oADOConnection.Close
    Set oADOConnection = Nothing
    V_2_11_01_Updates = False
    
End Function

Public Function CodigoDeActualizacion2_11_00_01A2_11_00_02() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim bBool As Boolean


frmProgreso.lblDetalle = "Actualizamos los contactos de aprovisionamiento"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

If V_2_11_01_Updates Then
    CodigoDeActualizacion2_11_00_01A2_11_00_02 = True
Else
    CodigoDeActualizacion2_11_00_01A2_11_00_02 = False
End If
    

End Function

Public Function CodigoDeActualizacion2_11_00_02A2_11_00_03() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim bBool As Boolean

On Error GoTo error
miBugMS = -1
    
ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

sConsulta = "BEGIN TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = True
ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

frmProgreso.lblDetalle = "Modificamos TABLAS"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[FK_PROVE_ART4_ATRIB_PROVE_ART4]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1) ALTER TABLE [dbo].[PROVE_ART4_ATRIB] DROP CONSTRAINT [FK_PROVE_ART4_ATRIB_PROVE_ART4]"
ExecuteSQL gRDOCon, sConsulta


miBugMS = 1
sConsulta = "ALTER TABLE LINEAS_PEDIDO ALTER COLUMN OBS VARCHAR(2000) NULL"
ExecuteSQL gRDOCon, sConsulta
sConsulta = "ALTER TABLE LOG_LINEAS_PEDIDO ALTER COLUMN OBS VARCHAR(2000) NULL"
ExecuteSQL gRDOCon, sConsulta
miBugMS = -1

frmProgreso.lblDetalle = "Modificamos stored procedures"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_11_Storeds_03_1

sConsulta = "UPDATE VERSION SET NUM ='2.11.00.03'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "COMMIT TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = False

CodigoDeActualizacion2_11_00_02A2_11_00_03 = True
Exit Function
    
error:

    If miBugMS = 1 Then
        MsgBox "No se han podido actualizar las tablas 'LINEAS_PEDIDO' y 'LOG_LINEAS_PEDIDO'." & vbCrLf & "Deber� hacerlo manualmente. Aumente el tama�o del campo OBS de ambas tablas a 2000." & vbCrLf & "Una vez realizado el cambio ejecute de nuevo el actualizador.", vbCritical, "FULLSTEP"
    End If
    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_11_00_02A2_11_00_03 = False

End Function


Public Function CodigoDeActualizacion2_11_00_03A2_11_00_04() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim bBool As Boolean

On Error GoTo error
    
ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

sConsulta = "BEGIN TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = True
ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"


frmProgreso.lblDetalle = "Modificamos TABLAS"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1


sConsulta = "ALTER TABLE PARGEN_INTERNO ADD TIMEOUT_BL_PROCE INT NOT NULL CONSTRAINT [DF_PARGEN_INTERNO_TIMEOUT_BL_PROCE] DEFAULT 20"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE PARGEN_DOT ADD ADJNOTIFDOT VARCHAR(255) NULL"
ExecuteSQL gRDOCon, sConsulta


frmProgreso.lblDetalle = "Modificamos stored procedures"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_11_Storeds_04

sConsulta = "UPDATE VERSION SET NUM ='2.11.00.04'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "COMMIT TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = False

CodigoDeActualizacion2_11_00_03A2_11_00_04 = True
Exit Function
    
error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_11_00_03A2_11_00_04 = False

End Function



Public Function CodigoDeActualizacion2_11_00_04A2_11_00_05() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim bBool As Boolean

On Error GoTo error
    
ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

sConsulta = "BEGIN TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = True
ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"


frmProgreso.lblDetalle = "Modificamos stored procedures"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_11_Storeds_05

sConsulta = "UPDATE VERSION SET NUM ='2.11.00.05'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "COMMIT TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = False

CodigoDeActualizacion2_11_00_04A2_11_00_05 = True
Exit Function
    
error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_11_00_04A2_11_00_05 = False

End Function




Private Function V_2_11_Storeds_03_1()
Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_CREAR_PEDIDO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_CREAR_PEDIDO]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSEP_CREAR_PEDIDO (@PER VARCHAR(30),  @PEDIDO INT OUTPUT) AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYOPROCE AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVE AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVE2 AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @CANT_PED AS FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRECIO_UP AS FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @SUMA AS FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @FC AS FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @NUMPEDIDO AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @ORDEN AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @ART_INT AS VARCHAR(20)" & vbCrLf
sConsulta = sConsulta & "DECLARE @UP AS VARCHAR(20)" & vbCrLf
sConsulta = sConsulta & "DECLARE @DEST AS VARCHAR(20)" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN1 AS VARCHAR (20)" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN2 AS VARCHAR (20)" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN3 AS VARCHAR (20)" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN4  AS VARCHAR (20)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCE AS SMALLINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ITEM AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @ENTREGAOBL AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @FECENTREGA AS DATETIME" & vbCrLf
sConsulta = sConsulta & "DECLARE @CANT_REC AS FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @CAT1 AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @CAT2 AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @CAT3 AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @CAT4 AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @CAT5 AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @SEGURIDAD AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @OBS AS VARCHAR (3000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @NUMORDEN AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @FECHAEMISION as DATETIME" & vbCrLf
sConsulta = sConsulta & "DECLARE @SOLICIT int" & vbCrLf
sConsulta = sConsulta & "SET @FECHAEMISION = CONVERT(DATETIME,CONVERT(VARCHAR,DAY(GETDATE())) + '/' + CONVERT(VARCHAR,MONTH(GETDATE())) + '/' + CONVERT(VARCHAR,YEAR(GETDATE())) ,103)" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "/*1- Crea el pedido :*/" & vbCrLf
sConsulta = sConsulta & "SET @ANYO=YEAR(@FECHAEMISION)" & vbCrLf
sConsulta = sConsulta & "SELECT @NUMPEDIDO=MAX(NUM) FROM PEDIDO WHERE ANYO=@ANYO" & vbCrLf
sConsulta = sConsulta & "SET @NUMPEDIDO=ISNULL(@NUMPEDIDO,0)+1" & vbCrLf
sConsulta = sConsulta & "INSERT INTO PEDIDO(ANYO,NUM,EST,TIPO,PER,FECHA) VALUES (@ANYO,@NUMPEDIDO,0,1,@PER,@FECHAEMISION)" & vbCrLf
sConsulta = sConsulta & "--SET @PEDIDO=IDENT_CURRENT('PEDIDO') " & vbCrLf
sConsulta = sConsulta & "SELECT @PEDIDO = MAX(ID) FROM PEDIDO" & vbCrLf
sConsulta = sConsulta & "/*2-Crea las �rdenes de entrega :*/" & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR FOR " & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT C.PROVE, SUM(C.PRECIO_UC*T.CANT_PED*T.FC)  " & vbCrLf
sConsulta = sConsulta & "  FROM CATALOG_LIN C " & vbCrLf
sConsulta = sConsulta & "        INNER JOIN #TMP_PEDIDO T " & vbCrLf
sConsulta = sConsulta & "                ON C.ID =T.CATLIN" & vbCrLf
sConsulta = sConsulta & "GROUP BY C.PROVE" & vbCrLf
sConsulta = sConsulta & "OPEN C " & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C INTO @PROVE,@SUMA" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SELECT @NUMORDEN=MAX(NUM) FROM ORDEN_ENTREGA WHERE ANYO=@ANYO AND PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "   SET @NUMORDEN = ISNULL(@NUMORDEN,0) + 1" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO ORDEN_ENTREGA (PEDIDO,ANYO,PROVE,NUM,EST,TIPO,FECHA,IMPORTE) " & vbCrLf
sConsulta = sConsulta & "        VALUES (@PEDIDO,@ANYO,@PROVE,@NUMORDEN,0,1,@FECHAEMISION,@SUMA)" & vbCrLf
sConsulta = sConsulta & "   SELECT @ORDEN = MAX(ID) FROM ORDEN_ENTREGA" & vbCrLf
sConsulta = sConsulta & "        " & vbCrLf
sConsulta = sConsulta & "   /*3- Crea las l�neas de pedido :*/" & vbCrLf
sConsulta = sConsulta & "   DECLARE D CURSOR FOR " & vbCrLf
sConsulta = sConsulta & "   SELECT DISTINCT C.ART_INT,C.PROVE,T.UP,C.PRECIO_UC*T.FC,T.CANT_PED," & vbCrLf
sConsulta = sConsulta & "   T.DEST,C.ANYO, C.GMN1,C.GMN2,C.GMN3,C.GMN4,C.PROCE,C.ITEM,T.ENTREGA_OBL,T.FECENTREGA,0,C.CAT1,C.CAT2,C.CAT3,C.CAT4,C.CAT5,C.SEGURIDAD," & vbCrLf
sConsulta = sConsulta & "   T.OBS,T.FC,C.SOLICIT" & vbCrLf
sConsulta = sConsulta & "     FROM #TMP_PEDIDO T " & vbCrLf
sConsulta = sConsulta & "             INNER JOIN CATALOG_LIN C " & vbCrLf
sConsulta = sConsulta & "                     ON T.CATLIN=C.ID" & vbCrLf
sConsulta = sConsulta & "             INNER JOIN ORDEN_ENTREGA D " & vbCrLf
sConsulta = sConsulta & "                    on D.PROVE=C.PROVE " & vbCrLf
sConsulta = sConsulta & "   WHERE D.PEDIDO=@PEDIDO " & vbCrLf
sConsulta = sConsulta & "     AND D.PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "       OPEN D" & vbCrLf
sConsulta = sConsulta & "       FETCH NEXT FROM D INTO @ART_INT,@PROVE2,@UP,@PRECIO_UP,@CANT_PED," & vbCrLf
sConsulta = sConsulta & "   @DEST,@ANYOPROCE, @GMN1,@GMN2,@GMN3,@GMN4,@PROCE,@ITEM,@ENTREGAOBL,@FECENTREGA,@CANT_REC,@CAT1,@CAT2,@CAT3,@CAT4,@CAT5,@SEGURIDAD,@OBS, @FC,@SOLICIT" & vbCrLf
sConsulta = sConsulta & "       WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "           BEGIN" & vbCrLf
sConsulta = sConsulta & "                PRINT @CANT_PED" & vbCrLf
sConsulta = sConsulta & "                INSERT INTO LINEAS_PEDIDO(PEDIDO,ORDEN,ART_INT,PROVE,UP,PREC_UP,CANT_PED,DEST,ANYO,GMN1,GMN2,GMN3," & vbCrLf
sConsulta = sConsulta & "                GMN4,PROCE,ITEM,FECEMISION,ENTREGA_OBL,FECENTREGA,EST,CANT_REC,CAT1,CAT2,CAT3,CAT4,CAT5,SEGURIDAD,NIVEL_APROB,OBS,FC,SOLICIT) " & vbCrLf
sConsulta = sConsulta & "                VALUES (@PEDIDO,@ORDEN,@ART_INT,@PROVE2,@UP,@PRECIO_UP,@CANT_PED,@DEST,@ANYOPROCE,@GMN1,@GMN2,@GMN3," & vbCrLf
sConsulta = sConsulta & "                @GMN4,@PROCE,@ITEM,@FECHAEMISION,@ENTREGAOBL,@FECENTREGA,0,@CANT_REC,@CAT1, @CAT2,@CAT3,@CAT4,@CAT5,@SEGURIDAD,-1,@OBS, @FC,@SOLICIT)" & vbCrLf
sConsulta = sConsulta & "       UPDATE PROCE " & vbCrLf
sConsulta = sConsulta & "                   SET PED_APROV=4 " & vbCrLf
sConsulta = sConsulta & "                 WHERE ANYO= @ANYOPROCE" & vbCrLf
sConsulta = sConsulta & "                   AND GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "                   AND COD = @PROCE" & vbCrLf
sConsulta = sConsulta & "                FETCH NEXT FROM D INTO @ART_INT,@PROVE2,@UP,@PRECIO_UP,@CANT_PED,@DEST,@ANYOPROCE, @GMN1,@GMN2,@GMN3,@GMN4,@PROCE,@ITEM,@ENTREGAOBL,@FECENTREGA,@CANT_REC,@CAT1,@CAT2,@CAT3,@CAT4,@CAT5,@SEGURIDAD,@OBS, @FC,@SOLICIT" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "       CLOSE  D" & vbCrLf
sConsulta = sConsulta & "       DEALLOCATE D" & vbCrLf
sConsulta = sConsulta & "   FETCH NEXT FROM C INTO @PROVE,@SUMA" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "CLOSE  C" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C" & vbCrLf
sConsulta = sConsulta & "DROP TABLE #TMP_PEDIDO" & vbCrLf
sConsulta = sConsulta & "RETURN" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_DEVOLVER_ORDENES]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_DEVOLVER_ORDENES]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSEP_DEVOLVER_ORDENES(@APROBADOR VARCHAR(50), @MAXLINEAS INTEGER, @DESDEID INTEGER," & vbCrLf
sConsulta = sConsulta & "@ANYO INTEGER, @NUMPEDIDO INTEGER, @ORDEN INTEGER, @APROBADOS INTEGER,@DENEGADOS INTEGER,@CODPROVE VARCHAR(50),@NUMPEDIDOPROVE VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & ",@DESDEFECHA VARCHAR(10), @APROVISIONADOR VARCHAR(50) )  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @INDICE INTEGER" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING= 'SELECT DISTINCT '" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING= @SQLSTRING + ' ORDEN_ENTREGA.ID AS ORDENID,ORDEN_ENTREGA.ANYO AS ANYO" & vbCrLf
sConsulta = sConsulta & ",PEDIDO.NUM AS NUMPEDIDO" & vbCrLf
sConsulta = sConsulta & ",ORDEN_ENTREGA.NUM AS NUMORDEN" & vbCrLf
sConsulta = sConsulta & ",ORDEN_ENTREGA.PROVE + '' '' + PROVE.DEN AS PROVE" & vbCrLf
sConsulta = sConsulta & ",ORDEN_ENTREGA.PROVE AS PROVECOD" & vbCrLf
sConsulta = sConsulta & ",PROVE.DEN AS PROVEDEN" & vbCrLf
sConsulta = sConsulta & ",ORDEN_ENTREGA.FECHA" & vbCrLf
sConsulta = sConsulta & ",ORDEN_ENTREGA.EST" & vbCrLf
sConsulta = sConsulta & ",ORDEN_ENTREGA.IMPORTE " & vbCrLf
sConsulta = sConsulta & ",PER.APE + '','' + PER.NOM AS APROVISIONADOR" & vbCrLf
sConsulta = sConsulta & "FROM LINEAS_PEDIDO INNER JOIN SEGURIDAD" & vbCrLf
sConsulta = sConsulta & "ON LINEAS_PEDIDO.SEGURIDAD=SEGURIDAD.ID AND APROB_TIPO1='''+ @APROBADOR +''' AND LINEAS_PEDIDO.NIVEL_APROB=0'" & vbCrLf
sConsulta = sConsulta & "IF @APROBADOS=0 AND @DENEGADOS <> 0" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND LINEAS_PEDIDO.EST IN (0,20)' " & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & " IF @APROBADOS<>0 AND @DENEGADOS=0" & vbCrLf
sConsulta = sConsulta & "     SET @SQLSTRING= @SQLSTRING + ' AND LINEAS_PEDIDO.EST IN (0,1,2,3)'" & vbCrLf
sConsulta = sConsulta & " ELSE            " & vbCrLf
sConsulta = sConsulta & "     IF @APROBADOS=0 AND @DENEGADOS=0" & vbCrLf
sConsulta = sConsulta & "         SET @SQLSTRING= @SQLSTRING + ' AND LINEAS_PEDIDO.EST =0 '" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING= @SQLSTRING + ' INNER JOIN ORDEN_ENTREGA ON LINEAS_PEDIDO.ORDEN=ORDEN_ENTREGA.ID'" & vbCrLf
sConsulta = sConsulta & "IF @ORDEN<>0" & vbCrLf
sConsulta = sConsulta & "  SET @SQLSTRING= @SQLSTRING + ' AND ORDEN_ENTREGA.NUM=' + CONVERT(VARCHAR,@ORDEN ) " & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING= @SQLSTRING + ' INNER JOIN PEDIDO ON ORDEN_ENTREGA.PEDIDO=PEDIDO.ID '" & vbCrLf
sConsulta = sConsulta & "IF @NUMPEDIDO <>0 " & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND PEDIDO.NUM=' +CONVERT(VARCHAR,@NUMPEDIDO )" & vbCrLf
sConsulta = sConsulta & "IF @APROVISIONADOR <>''" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND PEDIDO.PER=''' + @APROVISIONADOR +''' '" & vbCrLf
sConsulta = sConsulta & "if @ANYO<>0" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING=@SQLSTRING + ' AND PEDIDO.ANYO =' + CONVERT(VARCHAR,@ANYO) " & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING=@SQLSTRING + '  INNER JOIN PROVE ON ORDEN_ENTREGA.PROVE=PROVE.COD INNER JOIN PER ON PER.COD=PEDIDO.PER '" & vbCrLf
sConsulta = sConsulta & "IF @DESDEID <> 0 " & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND ORDEN_ENTREGA.ID >=' + CONVERT(VARCHAR,@DESDEID) " & vbCrLf
sConsulta = sConsulta & "IF @CODPROVE <>'' " & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND ORDEN_ENTREGA.PROVE =''' + @CODPROVE + ''''" & vbCrLf
sConsulta = sConsulta & "IF @NUMPEDIDOPROVE <> ''" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND ORDEN_ENTREGA.NUMEXT = ''' + @NUMPEDIDOPROVE + ''''" & vbCrLf
sConsulta = sConsulta & "IF @DESDEFECHA <> ''" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND ORDEN_ENTREGA.FECHA >= CONVERT(DATETIME,''' + @DESDEFECHA + ''',103) '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING= @SQLSTRING + ' UNION SELECT DISTINCT '" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING= @SQLSTRING + ' ORDEN_ENTREGA.ID AS ORDENID,ORDEN_ENTREGA.ANYO AS ANYO" & vbCrLf
sConsulta = sConsulta & ",PEDIDO.NUM AS NUMPEDIDO" & vbCrLf
sConsulta = sConsulta & ",ORDEN_ENTREGA.NUM AS NUMORDEN" & vbCrLf
sConsulta = sConsulta & ",ORDEN_ENTREGA.PROVE + '' '' + PROVE.DEN AS PROVE" & vbCrLf
sConsulta = sConsulta & ",ORDEN_ENTREGA.PROVE AS PROVECOD" & vbCrLf
sConsulta = sConsulta & ",PROVE.DEN AS PROVEDEN" & vbCrLf
sConsulta = sConsulta & ",ORDEN_ENTREGA.FECHA" & vbCrLf
sConsulta = sConsulta & ",ORDEN_ENTREGA.EST" & vbCrLf
sConsulta = sConsulta & ",ORDEN_ENTREGA.IMPORTE " & vbCrLf
sConsulta = sConsulta & ",PER.APE + '','' + PER.NOM AS APROVISIONADOR" & vbCrLf
sConsulta = sConsulta & "FROM LINEAS_PEDIDO INNER JOIN APROB_LIM" & vbCrLf
sConsulta = sConsulta & "ON LINEAS_PEDIDO.APROB_LIM=APROB_LIM.ID AND APROB_LIM.PER='''+ @APROBADOR +''''" & vbCrLf
sConsulta = sConsulta & "IF @APROBADOS=0 AND @DENEGADOS <> 0" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND LINEAS_PEDIDO.EST IN (0,20)' " & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & " IF @APROBADOS<>0 AND @DENEGADOS=0" & vbCrLf
sConsulta = sConsulta & "     SET @SQLSTRING= @SQLSTRING + ' AND LINEAS_PEDIDO.EST IN (0,1,2,3)'" & vbCrLf
sConsulta = sConsulta & " ELSE" & vbCrLf
sConsulta = sConsulta & "     IF @APROBADOS=0 AND @DENEGADOS=0    " & vbCrLf
sConsulta = sConsulta & "         SET @SQLSTRING= @SQLSTRING + ' AND LINEAS_PEDIDO.EST =0 '" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING= @SQLSTRING + ' INNER JOIN ORDEN_ENTREGA ON LINEAS_PEDIDO.ORDEN=ORDEN_ENTREGA.ID '" & vbCrLf
sConsulta = sConsulta & "IF @ORDEN<>0" & vbCrLf
sConsulta = sConsulta & "  SET @SQLSTRING= @SQLSTRING + ' AND ORDEN_ENTREGA.NUM=' + CONVERT(VARCHAR,@ORDEN ) " & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING= @SQLSTRING + ' INNER JOIN PEDIDO ON ORDEN_ENTREGA.PEDIDO=PEDIDO.ID '" & vbCrLf
sConsulta = sConsulta & "IF @NUMPEDIDO <>0 " & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND PEDIDO.NUM=' +CONVERT(VARCHAR,@NUMPEDIDO )" & vbCrLf
sConsulta = sConsulta & "IF @APROVISIONADOR <>''" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND PEDIDO.PER=''' + @APROVISIONADOR +''' '" & vbCrLf
sConsulta = sConsulta & "if @ANYO<>0 " & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING=@SQLSTRING + ' AND PEDIDO.ANYO =' + CONVERT(VARCHAR,@ANYO) " & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING=@SQLSTRING + ' INNER JOIN PROVE ON ORDEN_ENTREGA.PROVE=PROVE.COD INNER JOIN PER ON PER.COD=PEDIDO.PER WHERE 1=1 '" & vbCrLf
sConsulta = sConsulta & "IF @DESDEID <> 0 " & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND ORDEN_ENTREGA.ID >=' + CONVERT(VARCHAR,@DESDEID) " & vbCrLf
sConsulta = sConsulta & "IF @CODPROVE <>''" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND ORDEN_ENTREGA.PROVE =''' + @CODPROVE + ''''" & vbCrLf
sConsulta = sConsulta & "IF @NUMPEDIDOPROVE <> ''" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND ORDEN_ENTREGA.NUMEXT = ''' + @NUMPEDIDOPROVE + ''''" & vbCrLf
sConsulta = sConsulta & "IF @DESDEFECHA <> ''" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND ORDEN_ENTREGA.FECHA >=CONVERT(DATETIME,''' + @DESDEFECHA + ''',103) '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "/*PRINT @SQLSTRING*/" & vbCrLf
sConsulta = sConsulta & "SET ROWCOUNT @MAXLINEAS" & vbCrLf
sConsulta = sConsulta & "EXECUTE sp_executesql @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "SET ROWCOUNT 0" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_DEVOLVER_GRUPO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_DEVOLVER_GRUPO]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_DEVOLVER_GRUPO @ANYO INT, @GMN1 VARCHAR(20),@PROCE INT ,@GRUPO VARCHAR(20), @PROVE VARCHAR(50) = null, @OFE int = NULL, @MON VARCHAR(50) = NULL AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @HAYESP TINYINT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @HAYATRIB TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCEEST smallint" & vbCrLf
sConsulta = sConsulta & "DECLARE @EQUIVPROC FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @CAM FLOAT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @EQUIVPROC = CAMBIO,@PROCEEST = EST" & vbCrLf
sConsulta = sConsulta & "  FROM PROCE " & vbCrLf
sConsulta = sConsulta & " WHERE ANYO=@ANYO" & vbCrLf
sConsulta = sConsulta & "   AND GMN1=@GMN1" & vbCrLf
sConsulta = sConsulta & "   AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @CAM =( SELECT CAMBIO" & vbCrLf
sConsulta = sConsulta & "             FROM PROCE_OFE" & vbCrLf
sConsulta = sConsulta & "            WHERE ANYO=@ANYO" & vbCrLf
sConsulta = sConsulta & "              AND GMN1=@GMN1" & vbCrLf
sConsulta = sConsulta & "              AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "              AND PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "              AND OFE=@OFE)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CAM IS NULL" & vbCrLf
sConsulta = sConsulta & "  SELECT @CAM =  EQUIV / @EQUIVPROC" & vbCrLf
sConsulta = sConsulta & "    FROM MON " & vbCrLf
sConsulta = sConsulta & "   WHERE COD = @MON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @HAYESP = CASE WHEN ESP IS NULL OR ESP='' THEN 0 ELSE 1 END " & vbCrLf
sConsulta = sConsulta & "  FROM PROCE_GRUPO" & vbCrLf
sConsulta = sConsulta & " WHERE ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "   AND GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "   AND PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "   AND COD = @GRUPO" & vbCrLf
sConsulta = sConsulta & "IF @HAYESP = 0" & vbCrLf
sConsulta = sConsulta & "  IF (SELECT count(id) " & vbCrLf
sConsulta = sConsulta & "        FROM PROCE_GRUPO_ESP" & vbCrLf
sConsulta = sConsulta & "       WHERE ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "         and GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "         and PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "         AND GRUPO = @GRUPO)>0" & vbCrLf
sConsulta = sConsulta & "    SET @HAYESP = 1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @HAYATRIB = 0" & vbCrLf
sConsulta = sConsulta & "IF (SELECT COUNT(*)" & vbCrLf
sConsulta = sConsulta & "      FROM PROCE_ATRIB" & vbCrLf
sConsulta = sConsulta & "     WHERE ANYO  = @ANYO" & vbCrLf
sConsulta = sConsulta & "       and GMN1  = @GMN1 " & vbCrLf
sConsulta = sConsulta & "       and PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "       AND AMBITO = 2)>0" & vbCrLf
sConsulta = sConsulta & "  SET @HAYATRIB = 1  " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "DECLARE @IMPORTEOFERTA float" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if @PROVE IS NOT NULL AND @OFE IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SELECT @IMPORTEOFERTA = SUM(ISNULL(IO.PRECIO,0) * ISNULL(I.CANT,0)) " & vbCrLf
sConsulta = sConsulta & "  FROM ITEM_OFE IO" & vbCrLf
sConsulta = sConsulta & "         INNER JOIN ITEM I" & vbCrLf
sConsulta = sConsulta & "                 ON IO.PROCE = I.PROCE" & vbCrLf
sConsulta = sConsulta & "                AND IO.GMN1 = I.GMN1" & vbCrLf
sConsulta = sConsulta & "                AND IO.ANYO = I.ANYO" & vbCrLf
sConsulta = sConsulta & "                AND IO.ITEM =I.ID" & vbCrLf
sConsulta = sConsulta & " WHERE IO.ANYO  = @ANYO" & vbCrLf
sConsulta = sConsulta & "   AND IO.GMN1  = @GMN1 " & vbCrLf
sConsulta = sConsulta & "   AND IO.PROCE = @PROCE " & vbCrLf
sConsulta = sConsulta & "   AND IO.PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "   AND IO.NUM   = @OFE" & vbCrLf
sConsulta = sConsulta & "   AND I.GRUPO = @GRUPO" & vbCrLf
sConsulta = sConsulta & "   AND I.CONF=1" & vbCrLf
sConsulta = sConsulta & "   AND CASE WHEN I.EST IS NULL THEN -1 ELSE I.EST END = CASE WHEN @PROCEEST=11 THEN 0 ELSE -1 END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT PG.COD, PG.DEN, PG.DESCR, PG.DEST, PG.PAG, PAG.DEN DENPAG, PG.FECINI, PG.FECFIN, PG.ESP, @HAYESP HAYESP, @HAYATRIB HAYATRIB, @IMPORTEOFERTA/(@EQUIVPROC*@CAM)  IMPORTEOFERTA" & vbCrLf
sConsulta = sConsulta & "  FROM PROCE_GRUPO PG" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN PAG" & vbCrLf
sConsulta = sConsulta & "               ON PG.PAG = PAG.COD" & vbCrLf
sConsulta = sConsulta & " WHERE PG.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "   AND PG.GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "   AND PG.PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "   AND PG.COD = @GRUPO" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_DEVOLVER_OFERTA_PROVE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_DEVOLVER_OFERTA_PROVE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_DEVOLVER_OFERTA_PROVE @ANYO INTEGER,@GMN1 VARCHAR(20),@PROCE INTEGER ,@PROVE VARCHAR(50), @OFE smallint = null AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @LASTOFEENV smallint" & vbCrLf
sConsulta = sConsulta & "DECLARE @LASTOFE smallint" & vbCrLf
sConsulta = sConsulta & "DECLARE @NUMOFEENVIADAS smallint" & vbCrLf
sConsulta = sConsulta & "DECLARE @FECHAULTOFE datetime" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCEEST smallint" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "SELECT @PROCEEST = EST " & vbCrLf
sConsulta = sConsulta & "  FROM PROCE " & vbCrLf
sConsulta = sConsulta & " WHERE ANYO = @ANYO " & vbCrLf
sConsulta = sConsulta & "   AND GMN1 = @GMN1 " & vbCrLf
sConsulta = sConsulta & "   AND COD = @PROCE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @NUMOFEENVIADAS  = COUNT(OFE)" & vbCrLf
sConsulta = sConsulta & "  FROM PROCE_OFE " & vbCrLf
sConsulta = sConsulta & " WHERE ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "   AND GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "   AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "   AND PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "   AND ENVIADA = 1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @LASTOFEENV = MAX(OFE)" & vbCrLf
sConsulta = sConsulta & "  FROM PROCE_OFE " & vbCrLf
sConsulta = sConsulta & " WHERE ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "   AND GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "   AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "   AND PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "   AND ENVIADA = 1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "select @FECHAULTOFE = FECREC" & vbCrLf
sConsulta = sConsulta & "  FROM PROCE_OFE " & vbCrLf
sConsulta = sConsulta & " WHERE ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "   AND GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "   AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "   AND PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "   AND OFE = @LASTOFEENV" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @OFE IS NULL" & vbCrLf
sConsulta = sConsulta & "  IF EXISTS (SELECT * " & vbCrLf
sConsulta = sConsulta & "               FROM PROCE_OFE " & vbCrLf
sConsulta = sConsulta & "              WHERE ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "                AND GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "                AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "                AND PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "                AND OFE = -1)" & vbCrLf
sConsulta = sConsulta & "    SET @LASTOFE = -1" & vbCrLf
sConsulta = sConsulta & "  ELSE" & vbCrLf
sConsulta = sConsulta & "    SELECT @LASTOFE = MAX(OFE)" & vbCrLf
sConsulta = sConsulta & "      FROM PROCE_OFE" & vbCrLf
sConsulta = sConsulta & "     WHERE ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "       AND GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "       AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "       AND PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @LASTOFE =@OFE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @TOTALADJUNTOS int" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @TOTALADJUNTOS =(SELECT ISNULL(SUM(DATASIZE) ,0)" & vbCrLf
sConsulta = sConsulta & "                       FROM PROCE_OFE_ADJUN" & vbCrLf
sConsulta = sConsulta & "                      WHERE ANYO  = @ANYO" & vbCrLf
sConsulta = sConsulta & "                        AND GMN1  = @GMN1 " & vbCrLf
sConsulta = sConsulta & "                        AND PROCE = @PROCE " & vbCrLf
sConsulta = sConsulta & "                        AND PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "                        AND OFE   = @LASTOFE)" & vbCrLf
sConsulta = sConsulta & "SET @TOTALADJUNTOS = @TOTALADJUNTOS +" & vbCrLf
sConsulta = sConsulta & "(SELECT ISNULL(SUM(DATASIZE) ,0)" & vbCrLf
sConsulta = sConsulta & "   FROM OFE_GR_ADJUN" & vbCrLf
sConsulta = sConsulta & "  WHERE ANYO  = @ANYO" & vbCrLf
sConsulta = sConsulta & "    AND GMN1  = @GMN1 " & vbCrLf
sConsulta = sConsulta & "    AND PROCE = @PROCE " & vbCrLf
sConsulta = sConsulta & "    AND PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "    AND OFE   = @LASTOFE)" & vbCrLf
sConsulta = sConsulta & "SET @TOTALADJUNTOS = @TOTALADJUNTOS +" & vbCrLf
sConsulta = sConsulta & "(SELECT ISNULL(SUM(DATASIZE) ,0)" & vbCrLf
sConsulta = sConsulta & "   FROM OFE_ITEM_ADJUN" & vbCrLf
sConsulta = sConsulta & "  WHERE ANYO  = @ANYO" & vbCrLf
sConsulta = sConsulta & "    AND GMN1  = @GMN1 " & vbCrLf
sConsulta = sConsulta & "    AND PROCE = @PROCE " & vbCrLf
sConsulta = sConsulta & "    AND PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "    AND OFE   = @LASTOFE)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @IMPORTEOFERTA float" & vbCrLf
sConsulta = sConsulta & "SELECT @IMPORTEOFERTA = SUM(ISNULL(IO.PRECIO,0) * ISNULL(I.CANT,0)) " & vbCrLf
sConsulta = sConsulta & "  FROM ITEM_OFE IO" & vbCrLf
sConsulta = sConsulta & "         INNER JOIN ITEM I" & vbCrLf
sConsulta = sConsulta & "                 ON IO.PROCE = I.PROCE" & vbCrLf
sConsulta = sConsulta & "                AND IO.GMN1 = I.GMN1" & vbCrLf
sConsulta = sConsulta & "                AND IO.ANYO = I.ANYO" & vbCrLf
sConsulta = sConsulta & "                AND IO.ITEM =I.ID" & vbCrLf
sConsulta = sConsulta & " WHERE IO.ANYO  = @ANYO" & vbCrLf
sConsulta = sConsulta & "   AND IO.GMN1  = @GMN1 " & vbCrLf
sConsulta = sConsulta & "   AND IO.PROCE = @PROCE " & vbCrLf
sConsulta = sConsulta & "   AND IO.PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "   AND IO.NUM   = @LASTOFE" & vbCrLf
sConsulta = sConsulta & "   AND I.CONF=1" & vbCrLf
sConsulta = sConsulta & "   AND CASE WHEN I.EST IS NULL THEN -1 ELSE I.EST END = CASE WHEN @PROCEEST=11 THEN 0 ELSE -1 END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT PO.ANYO, PO.GMN1, PO.PROCE, PO.PROVE, PO.OFE, PO.EST, PO.FECREC," & vbCrLf
sConsulta = sConsulta & "       PO.FECVAL, PO.MON, MON.DEN MONDEN, PO.CAMBIO, PO.OBS , @LASTOFEENV LASTOFEENV, " & vbCrLf
sConsulta = sConsulta & "       @FECHAULTOFE FECHAULTOFE, @NUMOFEENVIADAS NUMOFEENVIADAS, ENVIADA ESTULTOFE,PO.FECACT, @TOTALADJUNTOS TOTALADJUNTOS, @IMPORTEOFERTA IMPORTEOFERTA" & vbCrLf
sConsulta = sConsulta & "  FROM PROCE_OFE PO " & vbCrLf
sConsulta = sConsulta & "    INNER JOIN MON " & vbCrLf
sConsulta = sConsulta & "            ON PO.MON = MON.COD" & vbCrLf
sConsulta = sConsulta & " WHERE ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "   AND GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "   AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "   AND PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "   AND OFE = @LASTOFE" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_DEVOLVER_DATOS_PERSONA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_DEVOLVER_DATOS_PERSONA]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSEP_DEVOLVER_DATOS_PERSONA(@PER VARCHAR(50)=NULL)  AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SELECT P.COD, P.NOM, P.APE, P.DEP, P.CAR, P.EMAIL, P.TFNO, P.TFNO2, P.FAX, P.UON1, P.UON2, P.UON3,U1.DEN DENU1, U2.DEN DENU2, U3.DEN DENU3, D.DEN DENDEP, P.EQP" & vbCrLf
sConsulta = sConsulta & "  FROM PER P" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN UON1 U1" & vbCrLf
sConsulta = sConsulta & "            ON P.UON1 =U1.COD" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN UON2 U2" & vbCrLf
sConsulta = sConsulta & "            ON P.UON1 = U2.UON1" & vbCrLf
sConsulta = sConsulta & "           AND P.UON2 = U2.COD" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN UON3 U3" & vbCrLf
sConsulta = sConsulta & "            ON P.UON1 = U3.UON1" & vbCrLf
sConsulta = sConsulta & "           AND P.UON2 = U3.UON2" & vbCrLf
sConsulta = sConsulta & "           AND P.UON3 = U3.COD" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN DEP D" & vbCrLf
sConsulta = sConsulta & "            ON P.DEP = D.COD" & vbCrLf
sConsulta = sConsulta & "        " & vbCrLf
sConsulta = sConsulta & " WHERE P.COD=ISNULL(@PER,P.COD)" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


End Function


Private Function V_2_11_Storeds_04()
Dim sConsulta As String

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_DEVOLVER_DESTINOS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_DEVOLVER_DESTINOS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE SP_DEVOLVER_DESTINOS @COD VARCHAR(50) =NULL, @DEN VARCHAR(500) = NULL, @COINCIDE TINYINT = 1, @ORDERDEN TINYINT = 1, @NUMMAXIMO int = null, @CONUON tinyint=0 AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL ='SELECT '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @NUMMAXIMO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "  SET @SQL = @SQL + 'TOP ' + CAST(@NUMMAXIMO AS VARCHAR(10)) + ' '" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +'" & vbCrLf
sConsulta = sConsulta & "       D.COD, D.DEN, D.SINTRANS, D.DIR, D.POB, D.CP, PR.DEN PROVI, PA.DEN PAI" & vbCrLf
sConsulta = sConsulta & "'" & vbCrLf
sConsulta = sConsulta & "if @CONUON  = 1" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +'," & vbCrLf
sConsulta = sConsulta & "       U0.DEST U0DEST," & vbCrLf
sConsulta = sConsulta & "       UON11.COD COD1U1, UON11.DEN DEN1U1, " & vbCrLf
sConsulta = sConsulta & "       UON12.COD COD2U1, UON12.DEN DEN2U1, UON22.COD COD2U2, UON22.DEN DEN2U2," & vbCrLf
sConsulta = sConsulta & "       UON13.COD COD3U1, UON13.DEN DEN3U1, UON23.COD COD3U2, UON23.DEN DEN3U2, UON33.COD COD3U3, UON33.DEN DEN3U3" & vbCrLf
sConsulta = sConsulta & "'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +'" & vbCrLf
sConsulta = sConsulta & "  FROM DEST D" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN PAI PA" & vbCrLf
sConsulta = sConsulta & "             ON D.PAI = PA.COD      " & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN PROVI PR" & vbCrLf
sConsulta = sConsulta & "             ON D.PAI = PR.PAI" & vbCrLf
sConsulta = sConsulta & "            AND D.PROVI = PR.COD" & vbCrLf
sConsulta = sConsulta & "'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if @CONUON  = 1" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +'" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN (SELECT DEST FROM UON0_DEST) U0 ON U0.DEST=D.COD " & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN ((SELECT DEST, MAX(UON1) U1 FROM UON1_DEST UD1 GROUP BY DEST) U1 INNER JOIN UON1 UON11 ON U1.U1 = UON11.COD) " & vbCrLf
sConsulta = sConsulta & "             ON U1.DEST=D.COD " & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN ((select ud.dest, ud.uon1 u1, ud.uon2 u2 from (select dest, max(uon1 + ''_'' + uon2) uo from uon2_dest group by dest) d inner join uon2_dest ud on ud.dest = d.dest  and ud.uon1 + ''_'' + ud.uon2 = d.uo) U2  INNER JOIN UON1 UON12 ON U2.U1 = UON12.COD INNER JOIN UON2 UON22 ON U2.U1 = UON22.UON1 AND  U2.U2 = UON22.COD)  " & vbCrLf
sConsulta = sConsulta & "             ON U2.DEST=D.COD " & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN ((select ud.dest, ud.uon1 u1, ud.uon2 u2, ud.uon3 u3 from (select dest, max(uon1 + ''_'' + uon2 + ''_'' + uon3) uo from uon3_dest group by dest) d inner join uon3_dest ud on ud.dest = d.dest and ud.uon1 + ''_'' + ud.uon2  + ''_'' + ud.uon3 = d.uo) U3 INNER JOIN UON1 UON13 ON U3.U1 = UON13.COD INNER JOIN UON2 UON23 ON U3.U1 = UON23.UON1 AND U3.U2 = UON23.COD INNER JOIN UON3 UON33 ON U3.U1 = UON33.UON1 AND U3.U2 = UON33.UON2 AND U3.U3 = UON33.COD) " & vbCrLf
sConsulta = sConsulta & "             ON U3.DEST=D.COD" & vbCrLf
sConsulta = sConsulta & "'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +'" & vbCrLf
sConsulta = sConsulta & " WHERE 1 = 1" & vbCrLf
sConsulta = sConsulta & "'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @COD IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "  IF @COINCIDE=1" & vbCrLf
sConsulta = sConsulta & "    SET @SQL = @SQL + '" & vbCrLf
sConsulta = sConsulta & "       AND D.COD = @COD'" & vbCrLf
sConsulta = sConsulta & "  ELSE" & vbCrLf
sConsulta = sConsulta & "    SET @SQL = @SQL + '" & vbCrLf
sConsulta = sConsulta & "       AND D.COD >= @COD'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @DEN IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "  IF @COINCIDE=1" & vbCrLf
sConsulta = sConsulta & "    SET @SQL = @SQL + '" & vbCrLf
sConsulta = sConsulta & "       AND D.DEN = @DEN'" & vbCrLf
sConsulta = sConsulta & "  ELSE" & vbCrLf
sConsulta = sConsulta & "    SET @SQL = @SQL + '" & vbCrLf
sConsulta = sConsulta & "       AND D.DEN >= @DEN'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ORDERDEN = 1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + '" & vbCrLf
sConsulta = sConsulta & "   ORDER BY D.DEN,D.COD'" & vbCrLf
sConsulta = sConsulta & "ElSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + '" & vbCrLf
sConsulta = sConsulta & "   ORDER BY D.COD,D.DEN'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "exec SP_EXECUTESQL @SQL, N'@COD VARCHAR(50), @DEN VARCHAR(500)', @COD=@COD, @DEN=@DEN" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Function


Private Sub V_2_11_Storeds_05()
Dim sConsulta As String

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_DEVOLVER_LINEAS_DE_ORDEN]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_DEVOLVER_LINEAS_DE_ORDEN]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_DEVOLVER_ORDENES]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_DEVOLVER_ORDENES]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_DEVOLVER_ADJUNTOS_ITEMS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_DEVOLVER_ADJUNTOS_ITEMS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_DEVOLVER_GRUPO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_DEVOLVER_GRUPO]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_VALIDAR_OFERTA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_VALIDAR_OFERTA]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE FSEP_DEVOLVER_LINEAS_DE_ORDEN(@APROBADOR VARCHAR(50), @ORDENID INTEGER, @SOLICIT int = null )  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(1500)" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if @APROBADOR IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT LP.ID AS LINEAID," & vbCrLf
sConsulta = sConsulta & "                LP.ART_INT + ITEM.ART CODART," & vbCrLf
sConsulta = sConsulta & "                ART4.DEN + ITEM.DESCR  + ART_AUX.DEN DENART," & vbCrLf
sConsulta = sConsulta & "                LP.ART_INT + ITEM.ART + ' ' + ART4.DEN + ITEM.DESCR  + ART_AUX.DEN AS ARTICULO," & vbCrLf
sConsulta = sConsulta & "                PA.COD_EXT ART_EXT," & vbCrLf
sConsulta = sConsulta & "                LP.GMN1," & vbCrLf
sConsulta = sConsulta & "                LP.GMN2," & vbCrLf
sConsulta = sConsulta & "                LP.GMN3," & vbCrLf
sConsulta = sConsulta & "                LP.GMN4," & vbCrLf
sConsulta = sConsulta & "       LP.PROVE," & vbCrLf
sConsulta = sConsulta & "                LP.DEST AS DESTINO," & vbCrLf
sConsulta = sConsulta & "                LP.UP AS UNIDAD," & vbCrLf
sConsulta = sConsulta & "                LP.PREC_UP AS PRECIOUNITARIO," & vbCrLf
sConsulta = sConsulta & "                LP.CANT_PED AS CANTIDAD," & vbCrLf
sConsulta = sConsulta & "                LP.CANT_REC AS CANTIDAD_REC," & vbCrLf
sConsulta = sConsulta & "                ITEM_ADJ.CANT_ADJ / LP.FC AS CANTADJUDICADA," & vbCrLf
sConsulta = sConsulta & "                ITEM_ADJ.CANT_PED / LP.FC AS CANTCONSUMIDA," & vbCrLf
sConsulta = sConsulta & "                LP.PREC_UP * LP.CANT_PED IMPORTE," & vbCrLf
sConsulta = sConsulta & "                LP.EST AS APROBADA," & vbCrLf
sConsulta = sConsulta & "                LP.FECEMISION AS FECHA," & vbCrLf
sConsulta = sConsulta & "                GESTIONABLE=CASE WHEN APROB_LIM.PER= @APROBADOR   THEN 1 WHEN (APROB_LIM.PER IS NULL AND SEGURIDAD.APROB_TIPO1=@APROBADOR ) THEN 1  ELSE 0 END," & vbCrLf
sConsulta = sConsulta & "                LP.CAT1," & vbCrLf
sConsulta = sConsulta & "                LP.CAT2," & vbCrLf
sConsulta = sConsulta & "                LP.CAT3," & vbCrLf
sConsulta = sConsulta & "                LP.CAT4," & vbCrLf
sConsulta = sConsulta & "                LP.CAT5," & vbCrLf
sConsulta = sConsulta & "                LP.OBS," & vbCrLf
sConsulta = sConsulta & "                case when c5.cod is null then " & vbCrLf
sConsulta = sConsulta & "                        case when c4.cod is null then " & vbCrLf
sConsulta = sConsulta & "                             case when c3.cod is null then" & vbCrLf
sConsulta = sConsulta & "                                  case when c2.cod is null then" & vbCrLf
sConsulta = sConsulta & "                                       c1.cod" & vbCrLf
sConsulta = sConsulta & "                                  else" & vbCrLf
sConsulta = sConsulta & "                                  c2.cod" & vbCrLf
sConsulta = sConsulta & "                                  end" & vbCrLf
sConsulta = sConsulta & "                             else" & vbCrLf
sConsulta = sConsulta & "                             c3.cod" & vbCrLf
sConsulta = sConsulta & "                             end" & vbCrLf
sConsulta = sConsulta & "                        else" & vbCrLf
sConsulta = sConsulta & "                        c4.cod" & vbCrLf
sConsulta = sConsulta & "                        end" & vbCrLf
sConsulta = sConsulta & "                    else" & vbCrLf
sConsulta = sConsulta & "                    c5.cod" & vbCrLf
sConsulta = sConsulta & "                    end CATEGORIA," & vbCrLf
sConsulta = sConsulta & "                case when c5.cod is null then " & vbCrLf
sConsulta = sConsulta & "                        case when c4.cod is null then " & vbCrLf
sConsulta = sConsulta & "                             case when c3.cod is null then" & vbCrLf
sConsulta = sConsulta & "                                  case when c2.cod is null then" & vbCrLf
sConsulta = sConsulta & "                                       '(' + c1.cod + ') ' + c1.den" & vbCrLf
sConsulta = sConsulta & "                                  else" & vbCrLf
sConsulta = sConsulta & "                                  '(' + c1.cod + ') ' + c1.den + ' - (' + c2.cod + ') ' + c2.den" & vbCrLf
sConsulta = sConsulta & "                                  end" & vbCrLf
sConsulta = sConsulta & "                             else" & vbCrLf
sConsulta = sConsulta & "                             '(' + c1.cod + ') ' + c1.den + ' - (' + c2.cod + ') ' + c2.den + ' - (' + c3.cod + ') ' + c3.den" & vbCrLf
sConsulta = sConsulta & "                             end" & vbCrLf
sConsulta = sConsulta & "                        else" & vbCrLf
sConsulta = sConsulta & "                        '(' + c1.cod + ') ' + c1.den + ' - (' + c2.cod + ') ' + c2.den + ' - (' + c3.cod + ') ' + c3.den + ' - (' + c4.cod + ') ' + c4.den" & vbCrLf
sConsulta = sConsulta & "                        end" & vbCrLf
sConsulta = sConsulta & "                    else" & vbCrLf
sConsulta = sConsulta & "                    '(' + c1.cod + ') ' + c1.den + ' - (' + c2.cod + ') ' + c2.den + ' - (' + c3.cod + ') ' + c3.den + ' - (' + c4.cod + ') ' + c4.den + ' - (' + c5.cod + ') ' + c5.den" & vbCrLf
sConsulta = sConsulta & "                    end CATEGORIARAMA," & vbCrLf
sConsulta = sConsulta & "                ITEM.ANYO ANYOPROCE," & vbCrLf
sConsulta = sConsulta & "                ITEM.PROCE CODPROCE," & vbCrLf
sConsulta = sConsulta & "                LE.COMENT" & vbCrLf
sConsulta = sConsulta & "                     " & vbCrLf
sConsulta = sConsulta & "FROM LINEAS_PEDIDO LP" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN APROB_LIM" & vbCrLf
sConsulta = sConsulta & "            ON LP.APROB_LIM=APROB_LIM.ID" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN SEGURIDAD " & vbCrLf
sConsulta = sConsulta & "            ON LP.SEGURIDAD=SEGURIDAD.ID" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN ITEM " & vbCrLf
sConsulta = sConsulta & "            ON LP.ANYO = ITEM.ANYO" & vbCrLf
sConsulta = sConsulta & "           AND LP.GMN1=ITEM.GMN1 " & vbCrLf
sConsulta = sConsulta & "           AND LP.PROCE=ITEM.PROCE " & vbCrLf
sConsulta = sConsulta & "           AND LP.ITEM=ITEM.ID" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN ART4 " & vbCrLf
sConsulta = sConsulta & "            ON LP.GMN1=ART4.GMN1 " & vbCrLf
sConsulta = sConsulta & "           AND LP.GMN2=ART4.GMN2 " & vbCrLf
sConsulta = sConsulta & "           AND LP.GMN3=ART4.GMN3 " & vbCrLf
sConsulta = sConsulta & "           AND LP.GMN4=ART4.GMN4 " & vbCrLf
sConsulta = sConsulta & "           AND LP.ART_INT=ART4.COD" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN ITEM_ADJ " & vbCrLf
sConsulta = sConsulta & "            ON LP.ITEM=ITEM_ADJ.ITEM " & vbCrLf
sConsulta = sConsulta & "           AND LP.ANYO = ITEM_ADJ.ANYO" & vbCrLf
sConsulta = sConsulta & "           AND LP.GMN1=ITEM_ADJ.GMN1 " & vbCrLf
sConsulta = sConsulta & "           AND LP.PROCE=ITEM_ADJ.PROCE " & vbCrLf
sConsulta = sConsulta & "           AND LP.PROVE=ITEM_ADJ.PROVE" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN ART4 ART_AUX " & vbCrLf
sConsulta = sConsulta & "            ON ITEM.ART=ART_AUX.COD " & vbCrLf
sConsulta = sConsulta & "           AND ART_AUX.GMN1=ITEM.GMN1 " & vbCrLf
sConsulta = sConsulta & "           AND ART_AUX.GMN2=ITEM.GMN2 " & vbCrLf
sConsulta = sConsulta & "           AND ART_AUX.GMN3=ITEM.GMN3 " & vbCrLf
sConsulta = sConsulta & "           AND ART_AUX.GMN4=ITEM.GMN4 " & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN PROVE_ART4 PA" & vbCrLf
sConsulta = sConsulta & "            ON LP.PROVE = PA.PROVE" & vbCrLf
sConsulta = sConsulta & "           AND LP.GMN1 = PA.GMN1" & vbCrLf
sConsulta = sConsulta & "           AND LP.GMN2 = PA.GMN2" & vbCrLf
sConsulta = sConsulta & "           AND LP.GMN3 = PA.GMN3" & vbCrLf
sConsulta = sConsulta & "           AND LP.GMN4 = PA.GMN4" & vbCrLf
sConsulta = sConsulta & "           AND CASE WHEN LP.ART_INT IS NULL THEN ITEM.ART ELSE LP.ART_INT END = PA.ART" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN CATN1 C1" & vbCrLf
sConsulta = sConsulta & "            ON LP.CAT1 = C1.ID" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN CATN2 C2" & vbCrLf
sConsulta = sConsulta & "            ON LP.CAT2 = C2.ID" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN CATN3 C3" & vbCrLf
sConsulta = sConsulta & "            ON LP.CAT3 = C3.ID" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN CATN4 C4" & vbCrLf
sConsulta = sConsulta & "            ON LP.CAT4 = C4.ID" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN CATN5 C5" & vbCrLf
sConsulta = sConsulta & "            ON LP.CAT5 = C5.ID" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN LINEAS_EST LE" & vbCrLf
sConsulta = sConsulta & "            ON LP.LINEAS_EST = LE.ID" & vbCrLf
sConsulta = sConsulta & "WHERE LP.ORDEN=@ORDENID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT LP.ID LINEAID," & vbCrLf
sConsulta = sConsulta & "                LP.ART_INT + I.ART CODART," & vbCrLf
sConsulta = sConsulta & "                LP.GMN1," & vbCrLf
sConsulta = sConsulta & "                LP.GMN2," & vbCrLf
sConsulta = sConsulta & "                LP.GMN3," & vbCrLf
sConsulta = sConsulta & "                LP.GMN4," & vbCrLf
sConsulta = sConsulta & "       LP.PROVE," & vbCrLf
sConsulta = sConsulta & "                ART4.DEN + I.DESCR  + ART_AUX.DEN DENART," & vbCrLf
sConsulta = sConsulta & "                PA.COD_EXT ART_EXT," & vbCrLf
sConsulta = sConsulta & "                LP.DEST DESTINO," & vbCrLf
sConsulta = sConsulta & "                D.DEN DESTDEN," & vbCrLf
sConsulta = sConsulta & "                LP.UP UNIDAD," & vbCrLf
sConsulta = sConsulta & "                U.DEN UNIDEN," & vbCrLf
sConsulta = sConsulta & "                LP.PREC_UP PRECIOUNITARIO," & vbCrLf
sConsulta = sConsulta & "                LP.CANT_PED CANTIDAD," & vbCrLf
sConsulta = sConsulta & "                LP.CANT_REC CANT_REC," & vbCrLf
sConsulta = sConsulta & "                IA.CANT_ADJ * LP.FC AS CANTADJUDICADA," & vbCrLf
sConsulta = sConsulta & "                LP.PREC_UP * LP.CANT_PED IMPORTE," & vbCrLf
sConsulta = sConsulta & "                LP.EST AS APROBADA," & vbCrLf
sConsulta = sConsulta & "                LP.FECEMISION FECHA," & vbCrLf
sConsulta = sConsulta & "                LP.FECENTREGA FECENTREGA," & vbCrLf
sConsulta = sConsulta & "                LP.EST,LP.ENTREGA_OBL,LP.FECENTREGAPROVE," & vbCrLf
sConsulta = sConsulta & "                LP.CAT1," & vbCrLf
sConsulta = sConsulta & "                LP.CAT2," & vbCrLf
sConsulta = sConsulta & "                LP.CAT3," & vbCrLf
sConsulta = sConsulta & "                LP.CAT4," & vbCrLf
sConsulta = sConsulta & "                LP.CAT5," & vbCrLf
sConsulta = sConsulta & "                LP.OBS," & vbCrLf
sConsulta = sConsulta & "                case when c5.cod is null then " & vbCrLf
sConsulta = sConsulta & "                        case when c4.cod is null then " & vbCrLf
sConsulta = sConsulta & "                             case when c3.cod is null then" & vbCrLf
sConsulta = sConsulta & "                                  case when c2.cod is null then" & vbCrLf
sConsulta = sConsulta & "                                       c1.cod" & vbCrLf
sConsulta = sConsulta & "                                  else" & vbCrLf
sConsulta = sConsulta & "                                  c2.cod" & vbCrLf
sConsulta = sConsulta & "                                  end" & vbCrLf
sConsulta = sConsulta & "                             else" & vbCrLf
sConsulta = sConsulta & "                             c3.cod" & vbCrLf
sConsulta = sConsulta & "                             end" & vbCrLf
sConsulta = sConsulta & "                        else" & vbCrLf
sConsulta = sConsulta & "                        c4.cod" & vbCrLf
sConsulta = sConsulta & "                        end" & vbCrLf
sConsulta = sConsulta & "                    else" & vbCrLf
sConsulta = sConsulta & "                    c5.cod" & vbCrLf
sConsulta = sConsulta & "                    end CATEGORIA," & vbCrLf
sConsulta = sConsulta & "                case when c5.cod is null then " & vbCrLf
sConsulta = sConsulta & "                        case when c4.cod is null then " & vbCrLf
sConsulta = sConsulta & "                             case when c3.cod is null then" & vbCrLf
sConsulta = sConsulta & "                                  case when c2.cod is null then" & vbCrLf
sConsulta = sConsulta & "                                       '(' + c1.cod + ') ' + c1.den" & vbCrLf
sConsulta = sConsulta & "                                  else" & vbCrLf
sConsulta = sConsulta & "                                  '(' + c1.cod + ') ' + c1.den + ' - (' + c2.cod + ') ' + c2.den" & vbCrLf
sConsulta = sConsulta & "                                  end" & vbCrLf
sConsulta = sConsulta & "                             else" & vbCrLf
sConsulta = sConsulta & "                             '(' + c1.cod + ') ' + c1.den + ' - (' + c2.cod + ') ' + c2.den + ' - (' + c3.cod + ') ' + c3.den" & vbCrLf
sConsulta = sConsulta & "                             end" & vbCrLf
sConsulta = sConsulta & "                        else" & vbCrLf
sConsulta = sConsulta & "                        '(' + c1.cod + ') ' + c1.den + ' - (' + c2.cod + ') ' + c2.den + ' - (' + c3.cod + ') ' + c3.den + ' - (' + c4.cod + ') ' + c4.den" & vbCrLf
sConsulta = sConsulta & "                        end" & vbCrLf
sConsulta = sConsulta & "                    else" & vbCrLf
sConsulta = sConsulta & "                    '(' + c1.cod + ') ' + c1.den + ' - (' + c2.cod + ') ' + c2.den + ' - (' + c3.cod + ') ' + c3.den + ' - (' + c4.cod + ') ' + c4.den + ' - (' + c5.cod + ') ' + c5.den" & vbCrLf
sConsulta = sConsulta & "                    end CATEGORIARAMA," & vbCrLf
sConsulta = sConsulta & "               " & vbCrLf
sConsulta = sConsulta & "                        " & vbCrLf
sConsulta = sConsulta & "                C1.DEN CAT1DEN," & vbCrLf
sConsulta = sConsulta & "                C2.DEN CAT2DEN," & vbCrLf
sConsulta = sConsulta & "                C3.DEN CAT3DEN," & vbCrLf
sConsulta = sConsulta & "                C4.DEN CAT4DEN," & vbCrLf
sConsulta = sConsulta & "                C5.DEN CAT5DEN," & vbCrLf
sConsulta = sConsulta & "                LE.COMENT," & vbCrLf
sConsulta = sConsulta & "                I.ANYO ANYOPROCE," & vbCrLf
sConsulta = sConsulta & "                I.PROCE CODPROCE," & vbCrLf
sConsulta = sConsulta & "                I.ID ITEM" & vbCrLf
sConsulta = sConsulta & " FROM LINEAS_PEDIDO LP" & vbCrLf
sConsulta = sConsulta & "     INNER JOIN DEST D" & vbCrLf
sConsulta = sConsulta & "             ON LP.DEST = D.COD" & vbCrLf
sConsulta = sConsulta & "     INNER JOIN UNI U" & vbCrLf
sConsulta = sConsulta & "             ON LP.UP = U.COD" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN ITEM I " & vbCrLf
sConsulta = sConsulta & "             ON LP.ANYO = I.ANYO" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN1=I.GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND LP.PROCE=I.PROCE " & vbCrLf
sConsulta = sConsulta & "            AND LP.ITEM=I.ID" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN PROVE_ART4 PA" & vbCrLf
sConsulta = sConsulta & "             ON LP.PROVE = PA.PROVE" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN1 = PA.GMN1" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN2 = PA.GMN2" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN3 = PA.GMN3" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN4 = PA.GMN4" & vbCrLf
sConsulta = sConsulta & "            AND CASE WHEN LP.ART_INT IS NULL THEN I.ART ELSE LP.ART_INT END = PA.ART" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN ART4 " & vbCrLf
sConsulta = sConsulta & "             ON LP.GMN1=ART4.GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN2=ART4.GMN2 " & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN3=ART4.GMN3 " & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN4=ART4.GMN4 " & vbCrLf
sConsulta = sConsulta & "            AND LP.ART_INT=ART4.COD" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN ITEM_ADJ IA " & vbCrLf
sConsulta = sConsulta & "             ON LP.ITEM = IA.ITEM " & vbCrLf
sConsulta = sConsulta & "            AND LP.ANYO = IA.ANYO" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN1 = IA.GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND LP.PROCE= IA.PROCE " & vbCrLf
sConsulta = sConsulta & "            AND LP.PROVE= IA.PROVE" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN ART4 ART_AUX " & vbCrLf
sConsulta = sConsulta & "             ON ART_AUX.COD=I.ART " & vbCrLf
sConsulta = sConsulta & "            AND ART_AUX.GMN1=I.GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND ART_AUX.GMN2=I.GMN2 " & vbCrLf
sConsulta = sConsulta & "            AND ART_AUX.GMN3=I.GMN3 " & vbCrLf
sConsulta = sConsulta & "            AND ART_AUX.GMN4=I.GMN4" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN CATN1 C1" & vbCrLf
sConsulta = sConsulta & "             ON LP.CAT1 = C1.ID" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN CATN2 C2" & vbCrLf
sConsulta = sConsulta & "             ON LP.CAT2 = C2.ID" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN CATN3 C3" & vbCrLf
sConsulta = sConsulta & "             ON LP.CAT3 = C3.ID" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN CATN4 C4" & vbCrLf
sConsulta = sConsulta & "             ON LP.CAT4 = C4.ID" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN CATN5 C5" & vbCrLf
sConsulta = sConsulta & "             ON LP.CAT5 = C5.ID" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN LINEAS_EST LE" & vbCrLf
sConsulta = sConsulta & "             ON LP.LINEAS_EST = LE.ID" & vbCrLf
sConsulta = sConsulta & " WHERE LP.ORDEN=@ORDENID" & vbCrLf
sConsulta = sConsulta & "   AND CASE WHEN LP.SOLICIT IS NULL AND @SOLICIT IS NULL THEN -1 ELSE LP.SOLICIT END " & vbCrLf
sConsulta = sConsulta & "       = CASE WHEN LP.SOLICIT IS NULL AND @SOLICIT IS NULL THEN -1 ELSE ISNULL(@SOLICIT,LP.SOLICIT) END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE FSEP_DEVOLVER_ORDENES(@APROBADOR VARCHAR(50), @MAXLINEAS INTEGER, @DESDEID INTEGER," & vbCrLf
sConsulta = sConsulta & "@ANYO INTEGER, @NUMPEDIDO INTEGER, @ORDEN INTEGER, @APROBADOS INTEGER,@DENEGADOS INTEGER,@CODPROVE VARCHAR(50),@NUMPEDIDOPROVE VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & ",@DESDEFECHA VARCHAR(10), @APROVISIONADOR VARCHAR(50) )  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @INDICE INTEGER" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING= 'SELECT DISTINCT '" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING= @SQLSTRING + ' ORDEN_ENTREGA.ID AS ORDENID,ORDEN_ENTREGA.ANYO AS ANYO" & vbCrLf
sConsulta = sConsulta & ",PEDIDO.NUM AS NUMPEDIDO" & vbCrLf
sConsulta = sConsulta & ",ORDEN_ENTREGA.NUM AS NUMORDEN" & vbCrLf
sConsulta = sConsulta & ",ORDEN_ENTREGA.PROVE + '' '' + PROVE.DEN AS PROVE" & vbCrLf
sConsulta = sConsulta & ",ORDEN_ENTREGA.PROVE AS PROVECOD" & vbCrLf
sConsulta = sConsulta & ",PROVE.DEN AS PROVEDEN" & vbCrLf
sConsulta = sConsulta & ",ORDEN_ENTREGA.FECHA" & vbCrLf
sConsulta = sConsulta & ",ORDEN_ENTREGA.EST" & vbCrLf
sConsulta = sConsulta & ",ORDEN_ENTREGA.IMPORTE " & vbCrLf
sConsulta = sConsulta & ",OES.COMENT, OES.FECHA FECEST" & vbCrLf
sConsulta = sConsulta & ",PER.APE + '','' + PER.NOM AS APROVISIONADOR" & vbCrLf
sConsulta = sConsulta & "FROM LINEAS_PEDIDO " & vbCrLf
sConsulta = sConsulta & "INNER JOIN SEGURIDAD" & vbCrLf
sConsulta = sConsulta & "ON LINEAS_PEDIDO.SEGURIDAD=SEGURIDAD.ID AND APROB_TIPO1='''+ @APROBADOR +''' AND LINEAS_PEDIDO.NIVEL_APROB=0'" & vbCrLf
sConsulta = sConsulta & "IF @APROBADOS=0 AND @DENEGADOS <> 0" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND LINEAS_PEDIDO.EST IN (0,20)' " & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & " IF @APROBADOS<>0 AND @DENEGADOS=0" & vbCrLf
sConsulta = sConsulta & "     SET @SQLSTRING= @SQLSTRING + ' AND LINEAS_PEDIDO.EST IN (0,1,2,3)'" & vbCrLf
sConsulta = sConsulta & " ELSE            " & vbCrLf
sConsulta = sConsulta & "     IF @APROBADOS=0 AND @DENEGADOS=0" & vbCrLf
sConsulta = sConsulta & "         SET @SQLSTRING= @SQLSTRING + ' AND LINEAS_PEDIDO.EST =0 '" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING= @SQLSTRING + ' INNER JOIN ORDEN_ENTREGA ON LINEAS_PEDIDO.ORDEN=ORDEN_ENTREGA.ID'" & vbCrLf
sConsulta = sConsulta & "IF @ORDEN<>0" & vbCrLf
sConsulta = sConsulta & "  SET @SQLSTRING= @SQLSTRING + ' AND ORDEN_ENTREGA.NUM=' + CONVERT(VARCHAR,@ORDEN ) " & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING= @SQLSTRING + ' INNER JOIN PEDIDO ON ORDEN_ENTREGA.PEDIDO=PEDIDO.ID '" & vbCrLf
sConsulta = sConsulta & "IF @NUMPEDIDO <>0 " & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND PEDIDO.NUM=' +CONVERT(VARCHAR,@NUMPEDIDO )" & vbCrLf
sConsulta = sConsulta & "IF @APROVISIONADOR <>''" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND PEDIDO.PER=''' + @APROVISIONADOR +''' '" & vbCrLf
sConsulta = sConsulta & "if @ANYO<>0" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING=@SQLSTRING + ' AND PEDIDO.ANYO =' + CONVERT(VARCHAR,@ANYO) " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING=@SQLSTRING + '" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN ORDEN_EST OES" & vbCrLf
sConsulta = sConsulta & "            ON ORDEN_ENTREGA.ORDEN_EST = OES.ID" & vbCrLf
sConsulta = sConsulta & "'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING=@SQLSTRING + '  INNER JOIN PROVE ON ORDEN_ENTREGA.PROVE=PROVE.COD INNER JOIN PER ON PER.COD=PEDIDO.PER '" & vbCrLf
sConsulta = sConsulta & "IF @DESDEID <> 0 " & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND ORDEN_ENTREGA.ID >=' + CONVERT(VARCHAR,@DESDEID) " & vbCrLf
sConsulta = sConsulta & "IF @CODPROVE <>'' " & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND ORDEN_ENTREGA.PROVE =''' + @CODPROVE + ''''" & vbCrLf
sConsulta = sConsulta & "IF @NUMPEDIDOPROVE <> ''" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND ORDEN_ENTREGA.NUMEXT = ''' + @NUMPEDIDOPROVE + ''''" & vbCrLf
sConsulta = sConsulta & "IF @DESDEFECHA <> ''" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND ORDEN_ENTREGA.FECHA >= CONVERT(DATETIME,''' + @DESDEFECHA + ''',103) '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING= @SQLSTRING + ' UNION SELECT DISTINCT '" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING= @SQLSTRING + ' ORDEN_ENTREGA.ID AS ORDENID,ORDEN_ENTREGA.ANYO AS ANYO" & vbCrLf
sConsulta = sConsulta & ",PEDIDO.NUM AS NUMPEDIDO" & vbCrLf
sConsulta = sConsulta & ",ORDEN_ENTREGA.NUM AS NUMORDEN" & vbCrLf
sConsulta = sConsulta & ",ORDEN_ENTREGA.PROVE + '' '' + PROVE.DEN AS PROVE" & vbCrLf
sConsulta = sConsulta & ",ORDEN_ENTREGA.PROVE AS PROVECOD" & vbCrLf
sConsulta = sConsulta & ",PROVE.DEN AS PROVEDEN" & vbCrLf
sConsulta = sConsulta & ",ORDEN_ENTREGA.FECHA" & vbCrLf
sConsulta = sConsulta & ",ORDEN_ENTREGA.EST" & vbCrLf
sConsulta = sConsulta & ",ORDEN_ENTREGA.IMPORTE " & vbCrLf
sConsulta = sConsulta & ",OES.COMENT, OES.FECHA FECEST" & vbCrLf
sConsulta = sConsulta & ",PER.APE + '','' + PER.NOM AS APROVISIONADOR" & vbCrLf
sConsulta = sConsulta & "FROM LINEAS_PEDIDO INNER JOIN APROB_LIM" & vbCrLf
sConsulta = sConsulta & "ON LINEAS_PEDIDO.APROB_LIM=APROB_LIM.ID AND APROB_LIM.PER='''+ @APROBADOR +''''" & vbCrLf
sConsulta = sConsulta & "IF @APROBADOS=0 AND @DENEGADOS <> 0" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND LINEAS_PEDIDO.EST IN (0,20)' " & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & " IF @APROBADOS<>0 AND @DENEGADOS=0" & vbCrLf
sConsulta = sConsulta & "     SET @SQLSTRING= @SQLSTRING + ' AND LINEAS_PEDIDO.EST IN (0,1,2,3)'" & vbCrLf
sConsulta = sConsulta & " ELSE" & vbCrLf
sConsulta = sConsulta & "     IF @APROBADOS=0 AND @DENEGADOS=0    " & vbCrLf
sConsulta = sConsulta & "         SET @SQLSTRING= @SQLSTRING + ' AND LINEAS_PEDIDO.EST =0 '" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING= @SQLSTRING + ' INNER JOIN ORDEN_ENTREGA ON LINEAS_PEDIDO.ORDEN=ORDEN_ENTREGA.ID '" & vbCrLf
sConsulta = sConsulta & "IF @ORDEN<>0" & vbCrLf
sConsulta = sConsulta & "  SET @SQLSTRING= @SQLSTRING + ' AND ORDEN_ENTREGA.NUM=' + CONVERT(VARCHAR,@ORDEN ) " & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING=@SQLSTRING + '" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN ORDEN_EST OES" & vbCrLf
sConsulta = sConsulta & "            ON ORDEN_ENTREGA.ORDEN_EST = OES.ID" & vbCrLf
sConsulta = sConsulta & "'" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING= @SQLSTRING + ' INNER JOIN PEDIDO ON ORDEN_ENTREGA.PEDIDO=PEDIDO.ID '" & vbCrLf
sConsulta = sConsulta & "IF @NUMPEDIDO <>0 " & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND PEDIDO.NUM=' +CONVERT(VARCHAR,@NUMPEDIDO )" & vbCrLf
sConsulta = sConsulta & "IF @APROVISIONADOR <>''" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND PEDIDO.PER=''' + @APROVISIONADOR +''' '" & vbCrLf
sConsulta = sConsulta & "if @ANYO<>0 " & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING=@SQLSTRING + ' AND PEDIDO.ANYO =' + CONVERT(VARCHAR,@ANYO) " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING=@SQLSTRING + ' INNER JOIN PROVE ON ORDEN_ENTREGA.PROVE=PROVE.COD INNER JOIN PER ON PER.COD=PEDIDO.PER WHERE 1=1 '" & vbCrLf
sConsulta = sConsulta & "IF @DESDEID <> 0 " & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND ORDEN_ENTREGA.ID >=' + CONVERT(VARCHAR,@DESDEID) " & vbCrLf
sConsulta = sConsulta & "IF @CODPROVE <>''" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND ORDEN_ENTREGA.PROVE =''' + @CODPROVE + ''''" & vbCrLf
sConsulta = sConsulta & "IF @NUMPEDIDOPROVE <> ''" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND ORDEN_ENTREGA.NUMEXT = ''' + @NUMPEDIDOPROVE + ''''" & vbCrLf
sConsulta = sConsulta & "IF @DESDEFECHA <> ''" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND ORDEN_ENTREGA.FECHA >=CONVERT(DATETIME,''' + @DESDEFECHA + ''',103) '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "/*PRINT @SQLSTRING*/" & vbCrLf
sConsulta = sConsulta & "SET ROWCOUNT @MAXLINEAS" & vbCrLf
sConsulta = sConsulta & "EXECUTE sp_executesql @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "SET ROWCOUNT 0" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE SP_DEVOLVER_ADJUNTOS_ITEMS @ANYO smallint, @GMN1 VARCHAR(50), @PROCE int , @GRUPO varchar(50) = null, @PROVE VARCHAR(50), @OFE INT AS " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    SELECT OIA.NOM AS NOM, " & vbCrLf
sConsulta = sConsulta & "           OIA.COM AS COM,  " & vbCrLf
sConsulta = sConsulta & "           A.ID AS ID,  " & vbCrLf
sConsulta = sConsulta & "           OIA.DATASIZE ," & vbCrLf
sConsulta = sConsulta & "           OIA.IDPORTAL," & vbCrLf
sConsulta = sConsulta & "           I.ID ITEM" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "      FROM OFE_ITEM_ADJUN OIA" & vbCrLf
sConsulta = sConsulta & "            INNER JOIN ADJUN A " & vbCrLf
sConsulta = sConsulta & "                    ON OIA.ID = A.ID  " & vbCrLf
sConsulta = sConsulta & "            INNER JOIN ITEM I" & vbCrLf
sConsulta = sConsulta & "                    ON OIA.ANYO = I.ANYO" & vbCrLf
sConsulta = sConsulta & "                   AND OIA.GMN1 = I.GMN1" & vbCrLf
sConsulta = sConsulta & "                   AND OIA.ITEM = I.ID" & vbCrLf
sConsulta = sConsulta & "     WHERE OIA.ANYO = @anyo " & vbCrLf
sConsulta = sConsulta & "       AND OIA.GMN1 = @GMN1   " & vbCrLf
sConsulta = sConsulta & "       AND OIA.PROCE = @PROCE  " & vbCrLf
sConsulta = sConsulta & "       AND OIA.PROVE = @PROVE " & vbCrLf
sConsulta = sConsulta & "       AND OIA.OFE = @OFE" & vbCrLf
sConsulta = sConsulta & "       AND I.GRUPO = @GRUPO" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE SP_DEVOLVER_GRUPO @ANYO INT, @GMN1 VARCHAR(20),@PROCE INT ,@GRUPO VARCHAR(20), @PROVE VARCHAR(50) = null, @OFE int = NULL, @MON VARCHAR(50) = NULL AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @HAYESP TINYINT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @HAYATRIB TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCEEST smallint" & vbCrLf
sConsulta = sConsulta & "DECLARE @EQUIVPROC FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @CAM FLOAT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @EQUIVPROC = CAMBIO,@PROCEEST = EST" & vbCrLf
sConsulta = sConsulta & "  FROM PROCE " & vbCrLf
sConsulta = sConsulta & " WHERE ANYO=@ANYO" & vbCrLf
sConsulta = sConsulta & "   AND GMN1=@GMN1" & vbCrLf
sConsulta = sConsulta & "   AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @CAM =( SELECT CAMBIO" & vbCrLf
sConsulta = sConsulta & "             FROM PROCE_OFE" & vbCrLf
sConsulta = sConsulta & "            WHERE ANYO=@ANYO" & vbCrLf
sConsulta = sConsulta & "              AND GMN1=@GMN1" & vbCrLf
sConsulta = sConsulta & "              AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "              AND PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "              AND OFE=@OFE)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CAM IS NULL" & vbCrLf
sConsulta = sConsulta & "  SELECT @CAM =  EQUIV / @EQUIVPROC" & vbCrLf
sConsulta = sConsulta & "    FROM MON " & vbCrLf
sConsulta = sConsulta & "   WHERE COD = @MON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @HAYESP = CASE WHEN ESP IS NULL OR ESP='' THEN 0 ELSE 1 END " & vbCrLf
sConsulta = sConsulta & "  FROM PROCE_GRUPO" & vbCrLf
sConsulta = sConsulta & " WHERE ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "   AND GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "   AND PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "   AND COD = @GRUPO" & vbCrLf
sConsulta = sConsulta & "IF @HAYESP = 0" & vbCrLf
sConsulta = sConsulta & "  IF (SELECT count(id) " & vbCrLf
sConsulta = sConsulta & "        FROM PROCE_GRUPO_ESP" & vbCrLf
sConsulta = sConsulta & "       WHERE ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "         and GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "         and PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "         AND GRUPO = @GRUPO)>0" & vbCrLf
sConsulta = sConsulta & "    SET @HAYESP = 1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @HAYATRIB = 0" & vbCrLf
sConsulta = sConsulta & "IF (SELECT COUNT(*)" & vbCrLf
sConsulta = sConsulta & "      FROM PROCE_ATRIB" & vbCrLf
sConsulta = sConsulta & "     WHERE ANYO  = @ANYO" & vbCrLf
sConsulta = sConsulta & "       and GMN1  = @GMN1 " & vbCrLf
sConsulta = sConsulta & "       and PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "       AND AMBITO = 2" & vbCrLf
sConsulta = sConsulta & "       and ISNULL(GRUPO, @GRUPO) = @GRUPO)>0" & vbCrLf
sConsulta = sConsulta & "  SET @HAYATRIB = 1  " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "DECLARE @IMPORTEOFERTA float" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if @PROVE IS NOT NULL AND @OFE IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SELECT @IMPORTEOFERTA = SUM(ISNULL(IO.PRECIO,0) * ISNULL(I.CANT,0)) " & vbCrLf
sConsulta = sConsulta & "  FROM ITEM_OFE IO" & vbCrLf
sConsulta = sConsulta & "         INNER JOIN ITEM I" & vbCrLf
sConsulta = sConsulta & "                 ON IO.PROCE = I.PROCE" & vbCrLf
sConsulta = sConsulta & "                AND IO.GMN1 = I.GMN1" & vbCrLf
sConsulta = sConsulta & "                AND IO.ANYO = I.ANYO" & vbCrLf
sConsulta = sConsulta & "                AND IO.ITEM =I.ID" & vbCrLf
sConsulta = sConsulta & " WHERE IO.ANYO  = @ANYO" & vbCrLf
sConsulta = sConsulta & "   AND IO.GMN1  = @GMN1 " & vbCrLf
sConsulta = sConsulta & "   AND IO.PROCE = @PROCE " & vbCrLf
sConsulta = sConsulta & "   AND IO.PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "   AND IO.NUM   = @OFE" & vbCrLf
sConsulta = sConsulta & "   AND I.GRUPO = @GRUPO" & vbCrLf
sConsulta = sConsulta & "   AND I.CONF=1" & vbCrLf
sConsulta = sConsulta & "   AND CASE WHEN I.EST IS NULL THEN -1 ELSE I.EST END = CASE WHEN @PROCEEST=11 THEN 0 ELSE -1 END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT PG.COD, PG.DEN, PG.DESCR, PG.DEST, PG.PAG, PAG.DEN DENPAG, PG.FECINI, PG.FECFIN, PG.ESP, @HAYESP HAYESP, @HAYATRIB HAYATRIB, @IMPORTEOFERTA/(@EQUIVPROC*@CAM)  IMPORTEOFERTA" & vbCrLf
sConsulta = sConsulta & "  FROM PROCE_GRUPO PG" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN PAG" & vbCrLf
sConsulta = sConsulta & "               ON PG.PAG = PAG.COD" & vbCrLf
sConsulta = sConsulta & " WHERE PG.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "   AND PG.GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "   AND PG.PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "   AND PG.COD = @GRUPO" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE SP_VALIDAR_OFERTA @ANYO SMALLINT,@GMN1 VARCHAR(50), @PROCE INT,@PROVE VARCHAR(50), @OFE INT , @ERROR TINYINT OUTPUT , @NEWOFE INT =NULL OUTPUT AS" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @OFEATRIB BIT" & vbCrLf
sConsulta = sConsulta & "DECLARE @OFEGRUPOATRIB BIT" & vbCrLf
sConsulta = sConsulta & "DECLARE @OFEITEMATRIB BIT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @ERROR = 0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @PAID INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @AMBITO SMALLINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @GRUPO VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @TIPO SMALLINT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE CATRIBS CURSOR LOCAL FORWARD_ONLY  FOR " & vbCrLf
sConsulta = sConsulta & "SELECT PA.ID, AMBITO, GRUPO, TIPO" & vbCrLf
sConsulta = sConsulta & "  FROM PROCE_ATRIB PA" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN DEF_ATRIB DA" & vbCrLf
sConsulta = sConsulta & "           ON PA.ATRIB = DA.ID" & vbCrLf
sConsulta = sConsulta & " WHERE ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "   AND GMN1= @GMN1" & vbCrLf
sConsulta = sConsulta & "   AND PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "   AND INTERNO = 0" & vbCrLf
sConsulta = sConsulta & "   AND OBLIG = 1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "OPEN CATRIBS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM CATRIBS INTO @PAID, @AMBITO, @GRUPO,  @TIPO" & vbCrLf
sConsulta = sConsulta & "SET @OFEATRIB=0" & vbCrLf
sConsulta = sConsulta & "SET @OFEGRUPOATRIB=0" & vbCrLf
sConsulta = sConsulta & "SET @OFEITEMATRIB=0" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0 AND @OFEATRIB=0 AND @OFEGRUPOATRIB=0 AND @OFEITEMATRIB=0" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    IF @AMBITO = 1" & vbCrLf
sConsulta = sConsulta & "      BEGIN" & vbCrLf
sConsulta = sConsulta & "        SET @OFEATRIB =1" & vbCrLf
sConsulta = sConsulta & "   SELECT @OFEATRIB = CASE " & vbCrLf
sConsulta = sConsulta & "                  WHEN CASE @TIPO " & vbCrLf
sConsulta = sConsulta & "                          WHEN 1 THEN CASE WHEN VALOR_TEXT ='' THEN NULL ELSE VALOR_TEXT END " & vbCrLf
sConsulta = sConsulta & "                          WHEN 2 THEN CAST(VALOR_NUM AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                          WHEN 3 THEN CAST(VALOR_FEC AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                          WHEN 4 THEN CAST(VALOR_BOOL AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                       END IS NULL " & vbCrLf
sConsulta = sConsulta & "                  THEN 1 " & vbCrLf
sConsulta = sConsulta & "                  ELSE 0 " & vbCrLf
sConsulta = sConsulta & "               END" & vbCrLf
sConsulta = sConsulta & "          FROM OFE_ATRIB" & vbCrLf
sConsulta = sConsulta & "         WHERE ANYO= @ANYO" & vbCrLf
sConsulta = sConsulta & "           AND GMN1= @GMN1" & vbCrLf
sConsulta = sConsulta & "           AND PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "           AND PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "           AND OFE = @OFE" & vbCrLf
sConsulta = sConsulta & "           AND ATRIB_ID = @PAID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "    IF @AMBITO = 2" & vbCrLf
sConsulta = sConsulta & "      BEGIN" & vbCrLf
sConsulta = sConsulta & "        SET @OFEGRUPOATRIB =0" & vbCrLf
sConsulta = sConsulta & "        IF @GRUPO IS NULL " & vbCrLf
sConsulta = sConsulta & "          BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "          IF (SELECT COUNT(*) " & vbCrLf
sConsulta = sConsulta & "                FROM OFE_GR_ATRIB " & vbCrLf
sConsulta = sConsulta & "               WHERE ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "                 AND GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "                 AND PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "                 AND PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "                 AND OFE = @OFE" & vbCrLf
sConsulta = sConsulta & "                 AND ATRIB_ID = @PAID" & vbCrLf
sConsulta = sConsulta & "                 AND CASE @TIPO " & vbCrLf
sConsulta = sConsulta & "                          WHEN 1 THEN CASE WHEN VALOR_TEXT ='' THEN NULL ELSE VALOR_TEXT END " & vbCrLf
sConsulta = sConsulta & "                          WHEN 2 THEN CAST(VALOR_NUM AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                          WHEN 3 THEN CAST(VALOR_FEC AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                          WHEN 4 THEN CAST(VALOR_BOOL AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                       END IS NOT NULL) <> (SELECT COUNT(*) " & vbCrLf
sConsulta = sConsulta & "                 FROM PROCE_GRUPO " & vbCrLf
sConsulta = sConsulta & "                WHERE ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "                  AND GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "                  AND PROCE = @PROCE)" & vbCrLf
sConsulta = sConsulta & "             SET @OFEGRUPOATRIB = 1" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "         ELSE" & vbCrLf
sConsulta = sConsulta & "          IF (SELECT COUNT(*) " & vbCrLf
sConsulta = sConsulta & "                FROM OFE_GR_ATRIB " & vbCrLf
sConsulta = sConsulta & "               WHERE ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "                 AND GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "                 AND PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "                 AND GRUPO = @GRUPO" & vbCrLf
sConsulta = sConsulta & "                 AND PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "                 AND OFE = @OFE" & vbCrLf
sConsulta = sConsulta & "                 AND ATRIB_ID = @PAID" & vbCrLf
sConsulta = sConsulta & "                 AND CASE @TIPO " & vbCrLf
sConsulta = sConsulta & "                          WHEN 1 THEN CASE WHEN VALOR_TEXT ='' THEN NULL ELSE VALOR_TEXT END " & vbCrLf
sConsulta = sConsulta & "                          WHEN 2 THEN CAST(VALOR_NUM AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                          WHEN 3 THEN CAST(VALOR_FEC AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                          WHEN 4 THEN CAST(VALOR_BOOL AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                       END IS NOT NULL)  =0 " & vbCrLf
sConsulta = sConsulta & "             SET @OFEGRUPOATRIB = 1" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "    IF @AMBITO = 3" & vbCrLf
sConsulta = sConsulta & "      BEGIN" & vbCrLf
sConsulta = sConsulta & "        SET @OFEITEMATRIB =0" & vbCrLf
sConsulta = sConsulta & "        IF @GRUPO IS NULL " & vbCrLf
sConsulta = sConsulta & "          BEGIN" & vbCrLf
sConsulta = sConsulta & "            IF (SELECT COUNT(*) " & vbCrLf
sConsulta = sConsulta & "                  FROM OFE_ITEM_ATRIB " & vbCrLf
sConsulta = sConsulta & "                 WHERE ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "                   AND GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "                   AND PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "                   AND PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "                   AND OFE = @OFE" & vbCrLf
sConsulta = sConsulta & "                   AND ATRIB_ID = @PAID" & vbCrLf
sConsulta = sConsulta & "                   AND CASE @TIPO " & vbCrLf
sConsulta = sConsulta & "                          WHEN 1 THEN CASE WHEN VALOR_TEXT ='' THEN NULL ELSE VALOR_TEXT END " & vbCrLf
sConsulta = sConsulta & "                          WHEN 2 THEN CAST(VALOR_NUM AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                          WHEN 3 THEN CAST(VALOR_FEC AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                          WHEN 4 THEN CAST(VALOR_BOOL AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                       END IS NOT NULL)  <>" & vbCrLf
sConsulta = sConsulta & "                (SELECT COUNT(*) " & vbCrLf
sConsulta = sConsulta & "                   FROM ITEM " & vbCrLf
sConsulta = sConsulta & "                  WHERE ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "                    AND GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "                    AND PROCE = @PROCE                    " & vbCrLf
sConsulta = sConsulta & "                    AND CONF = 1)" & vbCrLf
sConsulta = sConsulta & "                 SET @OFEITEMATRIB = 1" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "        ELSE" & vbCrLf
sConsulta = sConsulta & "           begin" & vbCrLf
sConsulta = sConsulta & "            IF (SELECT COUNT(*) " & vbCrLf
sConsulta = sConsulta & "                  FROM OFE_ITEM_ATRIB OIA" & vbCrLf
sConsulta = sConsulta & "                        INNER JOIN ITEM I" & vbCrLf
sConsulta = sConsulta & "                                ON OIA.ANYO = I.ANYO" & vbCrLf
sConsulta = sConsulta & "                               AND OIA.GMN1 = I.GMN1" & vbCrLf
sConsulta = sConsulta & "                               AND OIA.PROCE = I.PROCE" & vbCrLf
sConsulta = sConsulta & "                               AND OIA.ITEM = I.ID" & vbCrLf
sConsulta = sConsulta & "                 WHERE OIA.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "                   AND OIA.GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "                   AND OIA.PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "                   AND I.GRUPO = @GRUPO" & vbCrLf
sConsulta = sConsulta & "                   AND OIA.PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "                   AND OIA.OFE = @OFE" & vbCrLf
sConsulta = sConsulta & "                   AND OIA.ATRIB_ID = @PAID" & vbCrLf
sConsulta = sConsulta & "                   AND CASE @TIPO " & vbCrLf
sConsulta = sConsulta & "                          WHEN 1 THEN CASE WHEN VALOR_TEXT ='' THEN NULL ELSE VALOR_TEXT END " & vbCrLf
sConsulta = sConsulta & "                          WHEN 2 THEN CAST(VALOR_NUM AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                          WHEN 3 THEN CAST(VALOR_FEC AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                          WHEN 4 THEN CAST(VALOR_BOOL AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                       END IS NOT NULL)  <>" & vbCrLf
sConsulta = sConsulta & "                (SELECT COUNT(*) " & vbCrLf
sConsulta = sConsulta & "                   FROM ITEM " & vbCrLf
sConsulta = sConsulta & "                  WHERE ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "                    AND GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "                    AND PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "                    AND GRUPO = @GRUPO" & vbCrLf
sConsulta = sConsulta & "                    AND CONF = 1)" & vbCrLf
sConsulta = sConsulta & "                 SET @OFEITEMATRIB = 1" & vbCrLf
sConsulta = sConsulta & "             end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "      FETCH NEXT FROM CATRIBS INTO @PAID, @AMBITO, @GRUPO,  @TIPO" & vbCrLf
sConsulta = sConsulta & "    END" & vbCrLf
sConsulta = sConsulta & "    " & vbCrLf
sConsulta = sConsulta & "CLOSE CATRIBS" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE CATRIBS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @OFEATRIB = 1" & vbCrLf
sConsulta = sConsulta & "  SET @ERROR = 2" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @OFEGRUPOATRIB = 1" & vbCrLf
sConsulta = sConsulta & "  SET @ERROR = 3" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @OFEITEMATRIB = 1" & vbCrLf
sConsulta = sConsulta & "  SET @ERROR = 4" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ERROR = 0 " & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    UPDATE PROCE_OFE SET ULT= 0, LEIDO = 1  WHERE ANYO = @ANYO AND GMN1 = @GMN1 AND PROCE = @PROCE AND PROVE = @PROVE AND ULT = 1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    DECLARE @NUMOFEWEB AS int" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    SELECT @NEWOFE = ISNULL(MAX(OFE),0) + 1 FROM PROCE_OFE WHERE ANYO = @ANYO AND GMN1 = @GMN1 AND PROCE = @PROCE AND PROVE = @PROVE AND OFE != -1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO PROCE_OFE(ANYO, GMN1, PROCE, PROVE, OFE, EST, FECREC, FECVAL, MON, CAMBIO, OBS, ULT, FECACT, LEIDO, CERRADO, ENVIADA, OBSADJUN)" & vbCrLf
sConsulta = sConsulta & "SELECT ANYO, GMN1, PROCE, PROVE, @NEWOFE, EST, FECREC, FECVAL, MON, CAMBIO, OBS, 1, FECACT, LEIDO, CERRADO, 1, OBSADJUN" & vbCrLf
sConsulta = sConsulta & "  FROM PROCE_OFE" & vbCrLf
sConsulta = sConsulta & "WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE = @PROCE AND PROVE = @PROVE AND OFE = -1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_ITEM_ADJUN WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE= @PROCE and PROVE=@PROVE AND OFE = @NEWOFE" & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_ITEM_ATRIB WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE= @PROCE and PROVE=@PROVE AND OFE = @NEWOFE" & vbCrLf
sConsulta = sConsulta & "DELETE FROM ITEM_OFE WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE= @PROCE and PROVE=@PROVE AND NUM = @NEWOFE" & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_GR_ATRIB  WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE= @PROCE and PROVE=@PROVE AND OFE = @NEWOFE" & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_GR_ADJUN WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE= @PROCE and PROVE=@PROVE AND OFE = @NEWOFE" & vbCrLf
sConsulta = sConsulta & "DELETE FROM GRUPO_OFE WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE= @PROCE and PROVE=@PROVE AND OFE = @NEWOFE" & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_ATRIB  WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE= @PROCE and PROVE=@PROVE AND OFE = @NEWOFE" & vbCrLf
sConsulta = sConsulta & "DELETE FROM PROCE_OFE_ADJUN WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE= @PROCE and PROVE=@PROVE AND OFE = @NEWOFE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO GRUPO_OFE(ANYO, GMN1, PROCE, GRUPO, PROVE, OFE, CONSUMIDO, ADJUDICADO, AHORRADO, AHORRADO_PORCEN, ABIERTO, IMPORTE, AHORRO_OFE, AHORRO_OFE_PORCEN, FECACT, OBSADJUN)" & vbCrLf
sConsulta = sConsulta & "SELECT ANYO, GMN1, PROCE, GRUPO, PROVE, @NEWOFE, CONSUMIDO, ADJUDICADO, AHORRADO, AHORRADO_PORCEN, ABIERTO, IMPORTE, AHORRO_OFE, AHORRO_OFE_PORCEN, FECACT, OBSADJUN" & vbCrLf
sConsulta = sConsulta & "  FROM GRUPO_OFE" & vbCrLf
sConsulta = sConsulta & "WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE = @PROCE AND PROVE = @PROVE AND OFE = -1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO ITEM_OFE(ANYO, GMN1, PROCE, ITEM, PROVE, NUM, PRECIO, CANTMAX, ULT, PRECIO2, PRECIO3, USAR, PREC_VALIDO, FECACT, OBSADJUN, COMENT1, COMENT2, COMENT3)" & vbCrLf
sConsulta = sConsulta & "SELECT ANYO, GMN1, PROCE, ITEM, PROVE, @NEWOFE, PRECIO, CANTMAX, ULT, PRECIO2, PRECIO3, USAR, PREC_VALIDO, FECACT, OBSADJUN, COMENT1, COMENT2, COMENT3" & vbCrLf
sConsulta = sConsulta & "  FROM ITEM_OFE" & vbCrLf
sConsulta = sConsulta & "WHERE  ANYO = @ANYO AND GMN1=@GMN1 AND PROCE = @PROCE AND PROVE = @PROVE AND NUM = -1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO PROCE_OFE_ADJUN(ANYO, GMN1, PROCE, PROVE, OFE, ID, NOM, COM, IDPORTAL, DATASIZE)" & vbCrLf
sConsulta = sConsulta & "SELECT ANYO, GMN1, PROCE, PROVE, @NEWOFE, ID, NOM, COM, IDPORTAL, DATASIZE" & vbCrLf
sConsulta = sConsulta & "  FROM PROCE_OFE_ADJUN" & vbCrLf
sConsulta = sConsulta & "WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE = @PROCE AND PROVE = @PROVE AND OFE = -1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO OFE_ITEM_ADJUN(ANYO, GMN1, PROCE, ITEM, PROVE, OFE, ID, NOM, COM, IDPORTAL, DATASIZE)" & vbCrLf
sConsulta = sConsulta & "SELECT ANYO, GMN1, PROCE, ITEM, PROVE, @NEWOFE, ID, NOM, COM, IDPORTAL, DATASIZE" & vbCrLf
sConsulta = sConsulta & "  FROM OFE_ITEM_ADJUN" & vbCrLf
sConsulta = sConsulta & "WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE = @PROCE AND PROVE = @PROVE AND OFE = -1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO OFE_GR_ADJUN(ANYO, GMN1, PROCE, GRUPO, PROVE, OFE, ID, NOM, COM, IDPORTAL, DATASIZE)" & vbCrLf
sConsulta = sConsulta & "SELECT ANYO, GMN1, PROCE, GRUPO, PROVE, @NEWOFE, ID, NOM, COM, IDPORTAL, DATASIZE" & vbCrLf
sConsulta = sConsulta & "  FROM OFE_GR_ADJUN" & vbCrLf
sConsulta = sConsulta & " WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE = @PROCE AND PROVE = @PROVE AND OFE = -1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO OFE_GR_ATRIB(ANYO, GMN1, PROCE, GRUPO, PROVE, OFE, ATRIB_ID, VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL, VALOR_POND, FECACT)" & vbCrLf
sConsulta = sConsulta & "SELECT ANYO, GMN1, PROCE, GRUPO, PROVE, @NEWOFE, ATRIB_ID, VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL, VALOR_POND, FECACT" & vbCrLf
sConsulta = sConsulta & "  FROM OFE_GR_ATRIB" & vbCrLf
sConsulta = sConsulta & "WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE = @PROCE AND PROVE = @PROVE AND OFE = -1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO OFE_ITEM_ATRIB(ANYO, GMN1, PROCE, ITEM, PROVE, OFE, ATRIB_ID, VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL, VALOR_POND, FECACT)" & vbCrLf
sConsulta = sConsulta & "SELECT ANYO, GMN1, PROCE, ITEM, PROVE, @NEWOFE, ATRIB_ID, VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL, VALOR_POND, FECACT" & vbCrLf
sConsulta = sConsulta & "  FROM OFE_ITEM_ATRIB" & vbCrLf
sConsulta = sConsulta & "WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE = @PROCE AND PROVE = @PROVE AND OFE = -1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO OFE_ATRIB(ANYO, GMN1, PROCE, PROVE, OFE, ATRIB_ID, VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL, VALOR_POND, FECACT)" & vbCrLf
sConsulta = sConsulta & "SELECT ANYO, GMN1, PROCE, PROVE, @NEWOFE, ATRIB_ID, VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL, VALOR_POND, FECACT" & vbCrLf
sConsulta = sConsulta & "  FROM OFE_ATRIB " & vbCrLf
sConsulta = sConsulta & "WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE = @PROCE AND PROVE = @PROVE AND OFE = -1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE SET OFE = 1  WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE = @PROCE AND PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    SELECT @NUMOFEWEB = isnull(MAX(OFEWEB),0)+1 FROM PROCE_OFE_WEB WHERE ANYO = @ANYO AND GMN1 = @GMN1 AND PROCE = @PROCE AND PROVE = @PROVE " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    INSERT INTO PROCE_OFE_WEB (ANYO, GMN1, PROCE, PROVE, OFEWEB, OFE, FECREC, FECVAL, MON)" & vbCrLf
sConsulta = sConsulta & "    SELECT ANYO, GMN1, PROCE, PROVE, @NUMOFEWEB, OFE, FECREC,FECVAL,MON " & vbCrLf
sConsulta = sConsulta & "      FROM PROCE_OFE " & vbCrLf
sConsulta = sConsulta & "     WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE = @PROCE AND PROVE = @PROVE AND OFE = @NEWOFE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    INSERT INTO ITEM_OFE_WEB (ANYO, GMN1, PROCE, ITEM, PROVE, NUM, PRECIO, CANTMAX)" & vbCrLf
sConsulta = sConsulta & "    SELECT ANYO, GMN1, PROCE, ITEM, PROVE, @NUMOFEWEB, PRECIO, CANTMAX" & vbCrLf
sConsulta = sConsulta & "      FROM ITEM_OFE" & vbCrLf
sConsulta = sConsulta & "     WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE = @PROCE AND PROVE = @PROVE AND NUM = @NEWOFE" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    INSERT INTO PROCE_OFE_WEB_ADJUN (ANYO, GMN1, PROCE, PROVE, OFEWEB, ID, NOM, COM, IDPORTAL)" & vbCrLf
sConsulta = sConsulta & "    SELECT ANYO, GMN1, PROCE, PROVE, @NUMOFEWEB, ID, NOM, COM, IDPORTAL" & vbCrLf
sConsulta = sConsulta & "      FROM PROCE_OFE_ADJUN" & vbCrLf
sConsulta = sConsulta & "     WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE = @PROCE AND PROVE = @PROVE AND OFE = @NEWOFE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    UPDATE ITEM_OFE SET ULT = 0 WHERE ANYO = @ANYO AND GMN1 = @GMN1 AND PROCE =@PROCE AND PROVE=@PROVE AND NUM!=@NEWOFE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    DECLARE @ITEM INT" & vbCrLf
sConsulta = sConsulta & "    DECLARE C1 CURSOR LOCAL FOR SELECT ID FROM ITEM WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "    OPEN C1" & vbCrLf
sConsulta = sConsulta & "    " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    FETCH NEXT FROM C1 INTO @ITEM" & vbCrLf
sConsulta = sConsulta & "    WHILE @@FETCH_STATUS = 0" & vbCrLf
sConsulta = sConsulta & "      BEGIN" & vbCrLf
sConsulta = sConsulta & "        UPDATE ITEM_OFE SET ULT = 1 ,PRECIO = PRECIO" & vbCrLf
sConsulta = sConsulta & "          FROM ITEM_OFE " & vbCrLf
sConsulta = sConsulta & "         WHERE ANYO = @ANYO AND GMN1 = @GMN1 AND PROCE = @PROCE AND PROVE = @PROVE AND ITEM=@ITEM AND NUM = @NEWOFE" & vbCrLf
sConsulta = sConsulta & "        FETCH NEXT FROM C1 INTO @ITEM" & vbCrLf
sConsulta = sConsulta & "      END " & vbCrLf
sConsulta = sConsulta & "    CLOSE C1" & vbCrLf
sConsulta = sConsulta & "    DEALLOCATE C1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion2_11_00_05A2_11_00_06() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim bBool As Boolean

On Error GoTo error
    
ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

sConsulta = "BEGIN TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = True
ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"


frmProgreso.lblDetalle = "Modificamos stored procedures"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_11_Storeds_06

sConsulta = "UPDATE VERSION SET NUM ='2.11.00.06'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "COMMIT TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = False

CodigoDeActualizacion2_11_00_05A2_11_00_06 = True
Exit Function
    
error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_11_00_05A2_11_00_06 = False

End Function

Private Sub V_2_11_Storeds_06()
Dim sConsulta As String

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ATR_CARGAR_PROCE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[ATR_CARGAR_PROCE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE ATR_CARGAR_PROCE  (@PROCE SMALLINT,@ANYO AS INTEGER,@GMN1 AS VARCHAR(50), @ALL AS TINYINT,@GRUPO VARCHAR(50)=NULL,@ORDENADO SMALLINT=NULL)  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @NUMORD  VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & " if  @ORDENADO is null " & vbCrLf
sConsulta = sConsulta & "     set @NUMORD=25" & vbCrLf
sConsulta = sConsulta & "else" & vbCrLf
sConsulta = sConsulta & "     if  @ORDENADO=0" & vbCrLf
sConsulta = sConsulta & "      set  @NUMORD=11" & vbCrLf
sConsulta = sConsulta & "     else" & vbCrLf
sConsulta = sConsulta & "        if  @ORDENADO=1" & vbCrLf
sConsulta = sConsulta & "               set  @NUMORD=25" & vbCrLf
sConsulta = sConsulta & "        else" & vbCrLf
sConsulta = sConsulta & "        if  @ORDENADO=2" & vbCrLf
sConsulta = sConsulta & "            set @NUMORD=26" & vbCrLf
sConsulta = sConsulta & "       else" & vbCrLf
sConsulta = sConsulta & "           if  @ORDENADO=3" & vbCrLf
sConsulta = sConsulta & "                set @NUMORD=24" & vbCrLf
sConsulta = sConsulta & "          else" & vbCrLf
sConsulta = sConsulta & "                if  @ORDENADO=5" & vbCrLf
sConsulta = sConsulta & "                   set   @NUMORD=5" & vbCrLf
sConsulta = sConsulta & "               else" & vbCrLf
sConsulta = sConsulta & "                   if  @ORDENADO=6" & vbCrLf
sConsulta = sConsulta & "                      set   @NUMORD='6,8'" & vbCrLf
sConsulta = sConsulta & "                   else" & vbCrLf
sConsulta = sConsulta & "                        if  @ORDENADO=7" & vbCrLf
sConsulta = sConsulta & "                             set   @NUMORD='7,9'" & vbCrLf
sConsulta = sConsulta & "                       else" & vbCrLf
sConsulta = sConsulta & "                             if  @ORDENADO=8" & vbCrLf
sConsulta = sConsulta & "                                set @NUMORD=5" & vbCrLf
sConsulta = sConsulta & "                             else" & vbCrLf
sConsulta = sConsulta & "                                  if  @ORDENADO=10" & vbCrLf
sConsulta = sConsulta & "                                      set @NUMORD=12" & vbCrLf
sConsulta = sConsulta & "                                  else" & vbCrLf
sConsulta = sConsulta & "                                       if  @ORDENADO=11" & vbCrLf
sConsulta = sConsulta & "                                          set @NUMORD=13" & vbCrLf
sConsulta = sConsulta & "                                       else" & vbCrLf
sConsulta = sConsulta & "                                           if  @ORDENADO=12" & vbCrLf
sConsulta = sConsulta & "                                               set @NUMORD=10" & vbCrLf
sConsulta = sConsulta & "                                           else" & vbCrLf
sConsulta = sConsulta & "                                                if  @ORDENADO=13" & vbCrLf
sConsulta = sConsulta & "                                                    set @NUMORD=14" & vbCrLf
sConsulta = sConsulta & "                                                else" & vbCrLf
sConsulta = sConsulta & "                                                     if  @ORDENADO=14" & vbCrLf
sConsulta = sConsulta & "                                                        set  @NUMORD=15" & vbCrLf
sConsulta = sConsulta & "                                                     else" & vbCrLf
sConsulta = sConsulta & "                                                         if  @ORDENADO=15" & vbCrLf
sConsulta = sConsulta & "                                                             set @NUMORD=16" & vbCrLf
sConsulta = sConsulta & "                                                        else" & vbCrLf
sConsulta = sConsulta & "                                                             set @NUMORD=1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @GRUPO IS NULL" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=' SELECT PROCE_ATRIB.ID,PROCE_ATRIB.ATRIB,PROCE_ATRIB.PROCE,PROCE_ATRIB.GRUPO,PROCE_ATRIB.INTRO,PROCE_ATRIB.MINNUM,PROCE_ATRIB.MAXNUM,PROCE_ATRIB.MINFEC,PROCE_ATRIB.MAXFEC,PROCE_ATRIB.PREFBAJO,' " & vbCrLf
sConsulta = sConsulta & "         + '  PROCE_ATRIB.INTERNO,PROCE_ATRIB.AMBITO,PROCE_ATRIB.OBLIG,PROCE_ATRIB.OP_PREC,PROCE_ATRIB.APLIC_PREC,PROCE_ATRIB.DEF_PREC, PROCE_ATRIB.POND,PROCE_ATRIB.OP_POND,PROCE_ATRIB.NUM_POND,PROCE_ATRIB.POND_SI,PROCE_ATRIB.POND_NO,' " & vbCrLf
sConsulta = sConsulta & "        + '  PROCE_ATRIB.GMN1,PROCE_ATRIB.ANYO,DEF_ATRIB.TIPO,DEF_ATRIB.COD,DEF_ATRIB.DEN,PROCE_ATRIB.FECACT,PROCE_ATRIB.ORDEN' " & vbCrLf
sConsulta = sConsulta & "         + '  FROM PROCE_ATRIB INNER JOIN DEF_ATRIB ON PROCE_ATRIB.ATRIB=DEF_ATRIB.ID' " & vbCrLf
sConsulta = sConsulta & "         + '  WHERE PROCE_ATRIB.PROCE=' +  CAST(@PROCE AS VARCHAR) + '  AND  PROCE_ATRIB.GRUPO IS NULL AND  PROCE_ATRIB.ANYO=' +  CAST(@ANYO AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "         + ' AND PROCE_ATRIB.GMN1= '''  + @GMN1 + ''' ORDER BY ' + CAST( @NUMORD AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "      IF  @ALL = 1" & vbCrLf
sConsulta = sConsulta & "                    SET @SQL=' SELECT PROCE_ATRIB.ID,PROCE_ATRIB.ATRIB,PROCE_ATRIB.PROCE,PROCE_ATRIB.GRUPO,PROCE_ATRIB.INTRO,PROCE_ATRIB.MINNUM,PROCE_ATRIB.MAXNUM,PROCE_ATRIB.MINFEC,PROCE_ATRIB.MAXFEC,PROCE_ATRIB.PREFBAJO,' " & vbCrLf
sConsulta = sConsulta & "                        + '  PROCE_ATRIB.INTERNO,PROCE_ATRIB.AMBITO,PROCE_ATRIB.OBLIG,PROCE_ATRIB.OP_PREC,PROCE_ATRIB.APLIC_PREC,PROCE_ATRIB.DEF_PREC, PROCE_ATRIB.POND,PROCE_ATRIB.OP_POND,PROCE_ATRIB.NUM_POND,PROCE_ATRIB.POND_SI,PROCE_ATRIB.POND_NO,' " & vbCrLf
sConsulta = sConsulta & "                        + '  PROCE_ATRIB.GMN1,PROCE_ATRIB.ANYO,DEF_ATRIB.TIPO,DEF_ATRIB.COD,DEF_ATRIB.DEN,PROCE_ATRIB.FECACT,PROCE_ATRIB.ORDEN' " & vbCrLf
sConsulta = sConsulta & "                        + '  FROM PROCE_ATRIB INNER JOIN DEF_ATRIB ON PROCE_ATRIB.ATRIB=DEF_ATRIB.ID' " & vbCrLf
sConsulta = sConsulta & "                        + '  WHERE PROCE_ATRIB.PROCE=' +  CAST(@PROCE AS VARCHAR) + '  AND  PROCE_ATRIB.ANYO=' +  CAST(@ANYO AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                        + ' AND PROCE_ATRIB.GMN1= '''  + @GMN1 + ''' ORDER BY ' + CAST( @NUMORD AS VARCHAR)    " & vbCrLf
sConsulta = sConsulta & "       ELSE" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "             SET @SQL=' SELECT PROCE_ATRIB.ID,PROCE_ATRIB.ATRIB,PROCE_ATRIB.PROCE,PROCE_ATRIB.GRUPO,PROCE_ATRIB.INTRO,PROCE_ATRIB.MINNUM,PROCE_ATRIB.MAXNUM,PROCE_ATRIB.MINFEC,PROCE_ATRIB.MAXFEC,PROCE_ATRIB.PREFBAJO,' " & vbCrLf
sConsulta = sConsulta & "                        + '  PROCE_ATRIB.INTERNO,PROCE_ATRIB.AMBITO,PROCE_ATRIB.OBLIG,PROCE_ATRIB.OP_PREC,PROCE_ATRIB.APLIC_PREC,PROCE_ATRIB.DEF_PREC, PROCE_ATRIB.POND,PROCE_ATRIB.OP_POND,PROCE_ATRIB.NUM_POND,PROCE_ATRIB.POND_SI,PROCE_ATRIB.POND_NO,' " & vbCrLf
sConsulta = sConsulta & "                        + '  PROCE_ATRIB.GMN1,PROCE_ATRIB.ANYO,DEF_ATRIB.TIPO,DEF_ATRIB.COD,DEF_ATRIB.DEN,PROCE_ATRIB.FECACT,PROCE_ATRIB.ORDEN' " & vbCrLf
sConsulta = sConsulta & "                        + '  FROM PROCE_ATRIB INNER JOIN DEF_ATRIB ON PROCE_ATRIB.ATRIB=DEF_ATRIB.ID' " & vbCrLf
sConsulta = sConsulta & "                        + '  WHERE PROCE_ATRIB.PROCE=' +  CAST(@PROCE AS VARCHAR) + '  AND  PROCE_ATRIB.ANYO=' +  CAST(@ANYO AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                        + ' AND PROCE_ATRIB.GMN1= '''  + @GMN1 + '''  AND  PROCE_ATRIB.GRUPO=''' +@GRUPO+ ''' ORDER BY ' + CAST( @NUMORD AS VARCHAR)   " & vbCrLf
sConsulta = sConsulta & " EXEC SP_EXECUTESQL @STMT=@SQL" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ATR_CARGAR_PLANTILLA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[ATR_CARGAR_PLANTILLA]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE ATR_CARGAR_PLANTILLA  (@PLANTILLA INT,@GRUPO VARCHAR(50)=NULL)  AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @GRUPO IS NULL" & vbCrLf
sConsulta = sConsulta & "                    SELECT PLANTILLA_ATRIB.ID,PLANTILLA_ATRIB.ATRIB,PLANTILLA_ATRIB.PLANT,PLANTILLA_ATRIB.GRUPO,PLANTILLA_ATRIB.INTRO,PLANTILLA_ATRIB.MINNUM,PLANTILLA_ATRIB.MAXNUM,PLANTILLA_ATRIB.MINFEC,PLANTILLA_ATRIB.MAXFEC,PLANTILLA_ATRIB.PREFBAJO," & vbCrLf
sConsulta = sConsulta & "                            PLANTILLA_ATRIB.INTERNO,PLANTILLA_ATRIB.AMBITO,PLANTILLA_ATRIB.OBLIG,PLANTILLA_ATRIB.OP_PREC,PLANTILLA_ATRIB.APLIC_PREC,PLANTILLA_ATRIB.DEF_PREC," & vbCrLf
sConsulta = sConsulta & "                            PLANTILLA_ATRIB.POND,PLANTILLA_ATRIB.OP_POND,PLANTILLA_ATRIB.NUM_POND,PLANTILLA_ATRIB.POND_SI,PLANTILLA_ATRIB.POND_NO," & vbCrLf
sConsulta = sConsulta & "                            DEF_ATRIB.COD,DEF_ATRIB.DEN,DEF_ATRIB.TIPO,PLANTILLA_ATRIB.FECACT,PLANTILLA_ATRIB.ORDEN" & vbCrLf
sConsulta = sConsulta & "                            FROM PLANTILLA_ATRIB INNER JOIN DEF_ATRIB ON PLANTILLA_ATRIB.ATRIB=DEF_ATRIB.ID" & vbCrLf
sConsulta = sConsulta & "                            WHERE PLANTILLA_ATRIB.PLANT=@PLANTILLA  AND  PLANTILLA_ATRIB.GRUPO IS NULL   order by  DEF_ATRIB.COD" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "                    SELECT PLANTILLA_ATRIB.ID,PLANTILLA_ATRIB.ATRIB,PLANTILLA_ATRIB.PLANT,PLANTILLA_ATRIB.GRUPO,PLANTILLA_ATRIB.INTRO,PLANTILLA_ATRIB.MINNUM,PLANTILLA_ATRIB.MAXNUM,PLANTILLA_ATRIB.MINFEC,PLANTILLA_ATRIB.MAXFEC,PLANTILLA_ATRIB.PREFBAJO," & vbCrLf
sConsulta = sConsulta & "                            PLANTILLA_ATRIB.INTERNO,PLANTILLA_ATRIB.AMBITO,PLANTILLA_ATRIB.OBLIG,PLANTILLA_ATRIB.OP_PREC,PLANTILLA_ATRIB.APLIC_PREC,PLANTILLA_ATRIB.DEF_PREC," & vbCrLf
sConsulta = sConsulta & "                            PLANTILLA_ATRIB.POND,PLANTILLA_ATRIB.OP_POND,PLANTILLA_ATRIB.NUM_POND,PLANTILLA_ATRIB.POND_SI,PLANTILLA_ATRIB.POND_NO," & vbCrLf
sConsulta = sConsulta & "                            DEF_ATRIB.COD,DEF_ATRIB.DEN,DEF_ATRIB.TIPO,PLANTILLA_ATRIB.FECACT,PLANTILLA_ATRIB.ORDEN" & vbCrLf
sConsulta = sConsulta & "                            FROM PLANTILLA_ATRIB INNER JOIN DEF_ATRIB ON PLANTILLA_ATRIB.ATRIB=DEF_ATRIB.ID" & vbCrLf
sConsulta = sConsulta & "                            WHERE PLANTILLA_ATRIB.PLANT=@PLANTILLA  AND   (PLANTILLA_ATRIB.GRUPO=@GRUPO OR PLANTILLA_ATRIB.GRUPO IS NULL)" & vbCrLf
sConsulta = sConsulta & "                              order by  DEF_ATRIB.COD" & vbCrLf
sConsulta = sConsulta & "    " & vbCrLf
sConsulta = sConsulta & "    " & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_DEVOLVER_PROCE_ATRIB]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_DEVOLVER_PROCE_ATRIB]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_DEVOLVER_PROCE_ATRIB @ANYO smallint, @GMN1 varchar(50), @PROCE smallint, @AMBITO tinyint, @GRUPO varchar(50)=null, @CONLISTA tinyint = 1, @INTERNO tinyint= null, @ATRIB int = null AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CONLISTA = 1" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "if @AMBITO = 1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT PA.ATRIB, PA.ID, DA.COD, DA.DEN, PA.INTRO, DA.TIPO, PA.AMBITO," & vbCrLf
sConsulta = sConsulta & "       PA.MINNUM, PA.MINFEC," & vbCrLf
sConsulta = sConsulta & "       PA.MAXNUM, PA.MAXFEC," & vbCrLf
sConsulta = sConsulta & "       DAL.VALOR_TEXT DALVALOR_TEXT," & vbCrLf
sConsulta = sConsulta & "       DAL.VALOR_NUM DALVALOR_NUM," & vbCrLf
sConsulta = sConsulta & "       DAL.VALOR_FEC DALVALOR_FEC," & vbCrLf
sConsulta = sConsulta & "       PA.INTERNO,PA.POND,PA.DEF_PREC,PA.PREFBAJO," & vbCrLf
sConsulta = sConsulta & "       PA.OBLIG,PA.APLIC_PREC,PA.OP_PREC,PA.GRUPO,PA.USAR_PREC,PA.ORDEN," & vbCrLf
sConsulta = sConsulta & "       PA.OP_POND,PA.NUM_POND,PA.POND_SI,PA.POND_NO" & vbCrLf
sConsulta = sConsulta & " FROM PROCE_ATRIB PA" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN DEF_ATRIB DA" & vbCrLf
sConsulta = sConsulta & "               ON PA.ATRIB = DA.ID" & vbCrLf
sConsulta = sConsulta & "        LEFT JOIN PROCE_ATRIB_LISTA DAL" & vbCrLf
sConsulta = sConsulta & "               ON PA.ID = DAL.ATRIB_ID" & vbCrLf
sConsulta = sConsulta & "WHERE PA.ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "  AND PA.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "  AND PA.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "  AND PA.AMBITO = 1" & vbCrLf
sConsulta = sConsulta & "  AND PA.INTERNO = CASE WHEN @INTERNO IS NULL THEN PA.INTERNO ELSE @INTERNO END" & vbCrLf
sConsulta = sConsulta & "  AND DA.ID = CASE WHEN @ATRIB IS NULL THEN DA.ID ELSE @ATRIB END" & vbCrLf
sConsulta = sConsulta & "ORDER BY DA.COD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ELSE IF @AMBITO = 2" & vbCrLf
sConsulta = sConsulta & "  IF @GRUPO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "    SELECT PA.ATRIB, PA.ID, DA.COD, DA.DEN, PA.INTRO, DA.TIPO, PA.AMBITO," & vbCrLf
sConsulta = sConsulta & "           PA.MINNUM, PA.MINFEC," & vbCrLf
sConsulta = sConsulta & "           PA.MAXNUM, PA.MAXFEC," & vbCrLf
sConsulta = sConsulta & "           DAL.VALOR_TEXT DALVALOR_TEXT," & vbCrLf
sConsulta = sConsulta & "           DAL.VALOR_NUM DALVALOR_NUM," & vbCrLf
sConsulta = sConsulta & "           DAL.VALOR_FEC DALVALOR_FEC,   " & vbCrLf
sConsulta = sConsulta & "           PA.INTERNO,PA.POND,PA.DEF_PREC,PA.PREFBAJO," & vbCrLf
sConsulta = sConsulta & "           PA.OBLIG,PA.APLIC_PREC,PA.OP_PREC,PA.GRUPO,PA.USAR_PREC,PA.ORDEN," & vbCrLf
sConsulta = sConsulta & "           PA.OP_POND,PA.NUM_POND,PA.POND_SI,PA.POND_NO" & vbCrLf
sConsulta = sConsulta & "     FROM PROCE_ATRIB PA" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN DEF_ATRIB DA" & vbCrLf
sConsulta = sConsulta & "                   ON PA.ATRIB = DA.ID" & vbCrLf
sConsulta = sConsulta & "            LEFT JOIN PROCE_ATRIB_LISTA DAL" & vbCrLf
sConsulta = sConsulta & "                   ON PA.ID = DAL.ATRIB_ID" & vbCrLf
sConsulta = sConsulta & "    WHERE PA.ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "      AND PA.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "      AND PA.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "      AND ISNULL(PA.GRUPO,'#') = CASE WHEN PA.GRUPO IS NULL THEN ISNULL(PA.GRUPO,'#') ELSE @GRUPO END" & vbCrLf
sConsulta = sConsulta & "     AND PA.AMBITO = 2" & vbCrLf
sConsulta = sConsulta & "     AND PA.INTERNO = CASE WHEN @INTERNO IS NULL THEN PA.INTERNO ELSE @INTERNO END" & vbCrLf
sConsulta = sConsulta & "     AND DA.ID = CASE WHEN @ATRIB IS NULL THEN DA.ID ELSE @ATRIB END" & vbCrLf
sConsulta = sConsulta & "    ORDER BY DA.COD" & vbCrLf
sConsulta = sConsulta & "  ELSE" & vbCrLf
sConsulta = sConsulta & "    SELECT PA.ATRIB, PA.ID, DA.COD, DA.DEN, PA.INTRO, DA.TIPO, PA.AMBITO," & vbCrLf
sConsulta = sConsulta & "           PA.MINNUM, PA.MINFEC," & vbCrLf
sConsulta = sConsulta & "           PA.MAXNUM, PA.MAXFEC," & vbCrLf
sConsulta = sConsulta & "           DAL.VALOR_TEXT DALVALOR_TEXT," & vbCrLf
sConsulta = sConsulta & "           DAL.VALOR_NUM DALVALOR_NUM," & vbCrLf
sConsulta = sConsulta & "           DAL.VALOR_FEC DALVALOR_FEC,   " & vbCrLf
sConsulta = sConsulta & "           PA.INTERNO,PA.POND,PA.DEF_PREC,PA.PREFBAJO," & vbCrLf
sConsulta = sConsulta & "           PA.OBLIG,PA.APLIC_PREC,PA.OP_PREC,PA.GRUPO,PA.USAR_PREC,PA.ORDEN," & vbCrLf
sConsulta = sConsulta & "           PA.OP_POND,PA.NUM_POND,PA.POND_SI,PA.POND_NO" & vbCrLf
sConsulta = sConsulta & "     FROM PROCE_ATRIB PA" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN DEF_ATRIB DA" & vbCrLf
sConsulta = sConsulta & "                   ON PA.ATRIB = DA.ID" & vbCrLf
sConsulta = sConsulta & "            LEFT JOIN PROCE_ATRIB_LISTA DAL" & vbCrLf
sConsulta = sConsulta & "                   ON PA.ID = DAL.ATRIB_ID" & vbCrLf
sConsulta = sConsulta & "    WHERE PA.ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "      AND PA.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "      AND PA.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "      AND PA.AMBITO = 2" & vbCrLf
sConsulta = sConsulta & "      AND PA.INTERNO = CASE WHEN @INTERNO IS NULL THEN PA.INTERNO ELSE @INTERNO END" & vbCrLf
sConsulta = sConsulta & "      AND DA.ID = CASE WHEN @ATRIB IS NULL THEN DA.ID ELSE @ATRIB END" & vbCrLf
sConsulta = sConsulta & "    ORDER BY DA.COD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ELSE IF @AMBITO = 3" & vbCrLf
sConsulta = sConsulta & "  IF @GRUPO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "    SELECT PA.ATRIB, PA.ID, DA.COD, DA.DEN, PA.INTRO, DA.TIPO,  PA.AMBITO," & vbCrLf
sConsulta = sConsulta & "           PA.MINNUM, PA.MINFEC," & vbCrLf
sConsulta = sConsulta & "           PA.MAXNUM, PA.MAXFEC," & vbCrLf
sConsulta = sConsulta & "           DAL.VALOR_TEXT DALVALOR_TEXT," & vbCrLf
sConsulta = sConsulta & "           DAL.VALOR_NUM DALVALOR_NUM," & vbCrLf
sConsulta = sConsulta & "           DAL.VALOR_FEC DALVALOR_FEC," & vbCrLf
sConsulta = sConsulta & "           PA.INTERNO,PA.POND,PA.DEF_PREC,PA.PREFBAJO," & vbCrLf
sConsulta = sConsulta & "           PA.OBLIG,PA.APLIC_PREC,PA.OP_PREC,PA.GRUPO,PA.USAR_PREC,PA.ORDEN," & vbCrLf
sConsulta = sConsulta & "           PA.OP_POND,PA.NUM_POND,PA.POND_SI,PA.POND_NO     " & vbCrLf
sConsulta = sConsulta & "     FROM PROCE_ATRIB PA" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN DEF_ATRIB DA" & vbCrLf
sConsulta = sConsulta & "                   ON PA.ATRIB = DA.ID" & vbCrLf
sConsulta = sConsulta & "            LEFT JOIN PROCE_ATRIB_LISTA DAL" & vbCrLf
sConsulta = sConsulta & "                   ON PA.ID = DAL.ATRIB_ID" & vbCrLf
sConsulta = sConsulta & "    WHERE PA.ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "      AND PA.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "      AND PA.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "      AND ISNULL(PA.GRUPO,'#') = CASE WHEN PA.GRUPO IS NULL THEN ISNULL(PA.GRUPO,'#') ELSE @GRUPO END" & vbCrLf
sConsulta = sConsulta & "      AND PA.AMBITO = 3" & vbCrLf
sConsulta = sConsulta & "      AND PA.INTERNO = CASE WHEN @INTERNO IS NULL THEN PA.INTERNO ELSE @INTERNO END" & vbCrLf
sConsulta = sConsulta & "      AND DA.ID = CASE WHEN @ATRIB IS NULL THEN DA.ID ELSE @ATRIB END" & vbCrLf
sConsulta = sConsulta & "     ORDER BY DA.COD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  ELSE" & vbCrLf
sConsulta = sConsulta & "   SELECT PA.ATRIB, PA.ID, DA.COD, DA.DEN, PA.INTRO, DA.TIPO,  PA.AMBITO," & vbCrLf
sConsulta = sConsulta & "                PA.MINNUM, PA.MINFEC," & vbCrLf
sConsulta = sConsulta & "                PA.MAXNUM, PA.MAXFEC," & vbCrLf
sConsulta = sConsulta & "                NULL DALVALOR_TEXT," & vbCrLf
sConsulta = sConsulta & "                NULL DALVALOR_NUM," & vbCrLf
sConsulta = sConsulta & "                NULL DALVALOR_FEC," & vbCrLf
sConsulta = sConsulta & "                PA.INTERNO,PA.POND,PA.DEF_PREC,PA.PREFBAJO," & vbCrLf
sConsulta = sConsulta & "                PA.OBLIG,PA.APLIC_PREC,PA.OP_PREC,PA.GRUPO,PA.USAR_PREC,PA.ORDEN," & vbCrLf
sConsulta = sConsulta & "                PA.OP_POND,PA.NUM_POND,PA.POND_SI,PA.POND_NO" & vbCrLf
sConsulta = sConsulta & "       FROM PROCE_ATRIB PA" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN DEF_ATRIB DA" & vbCrLf
sConsulta = sConsulta & "                   ON PA.ATRIB = DA.ID" & vbCrLf
sConsulta = sConsulta & "       WHERE PA.ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "         AND PA.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "         AND PA.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "         AND PA.AMBITO = 3" & vbCrLf
sConsulta = sConsulta & "               AND PA.INTERNO = CASE WHEN @INTERNO IS NULL THEN PA.INTERNO ELSE @INTERNO END" & vbCrLf
sConsulta = sConsulta & "               AND DA.ID = CASE WHEN @ATRIB IS NULL THEN DA.ID ELSE @ATRIB END" & vbCrLf
sConsulta = sConsulta & "          ORDER BY DA.COD" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "if @AMBITO = 1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT PA.ATRIB, PA.ID, DA.COD, DA.DEN, PA.INTRO, DA.TIPO,  PA.AMBITO," & vbCrLf
sConsulta = sConsulta & "       PA.MINNUM, PA.MINFEC," & vbCrLf
sConsulta = sConsulta & "       PA.MAXNUM, PA.MAXFEC," & vbCrLf
sConsulta = sConsulta & "       NULL DALVALOR_TEXT," & vbCrLf
sConsulta = sConsulta & "       NULL DALVALOR_NUM," & vbCrLf
sConsulta = sConsulta & "       NULL DALVALOR_FEC," & vbCrLf
sConsulta = sConsulta & "       PA.INTERNO,PA.POND,PA.DEF_PREC,PA.PREFBAJO," & vbCrLf
sConsulta = sConsulta & "       PA.OBLIG,PA.APLIC_PREC,PA.OP_PREC,PA.GRUPO,PA.USAR_PREC,PA.ORDEN," & vbCrLf
sConsulta = sConsulta & "       PA.OP_POND,PA.NUM_POND,PA.POND_SI,PA.POND_NO" & vbCrLf
sConsulta = sConsulta & " FROM PROCE_ATRIB PA" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN DEF_ATRIB DA" & vbCrLf
sConsulta = sConsulta & "               ON PA.ATRIB = DA.ID" & vbCrLf
sConsulta = sConsulta & "WHERE PA.ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "  AND PA.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "  AND PA.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "  AND PA.AMBITO = 1" & vbCrLf
sConsulta = sConsulta & "  AND PA.INTERNO = CASE WHEN @INTERNO IS NULL THEN PA.INTERNO ELSE @INTERNO END" & vbCrLf
sConsulta = sConsulta & "  AND DA.ID = CASE WHEN @ATRIB IS NULL THEN DA.ID ELSE @ATRIB END" & vbCrLf
sConsulta = sConsulta & "ORDER BY DA.COD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ELSE IF @AMBITO = 2" & vbCrLf
sConsulta = sConsulta & "  IF @GRUPO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "    SELECT PA.ATRIB, PA.ID, DA.COD, DA.DEN, PA.INTRO, DA.TIPO,  PA.AMBITO," & vbCrLf
sConsulta = sConsulta & "           PA.MINNUM, PA.MINFEC," & vbCrLf
sConsulta = sConsulta & "           PA.MAXNUM, PA.MAXFEC," & vbCrLf
sConsulta = sConsulta & "           NULL DALVALOR_TEXT," & vbCrLf
sConsulta = sConsulta & "           NULL DALVALOR_NUM," & vbCrLf
sConsulta = sConsulta & "           NULL DALVALOR_FEC,   " & vbCrLf
sConsulta = sConsulta & "           PA.INTERNO,PA.POND,PA.DEF_PREC,PA.PREFBAJO," & vbCrLf
sConsulta = sConsulta & "           PA.OBLIG,PA.APLIC_PREC,PA.OP_PREC,PA.GRUPO,PA.USAR_PREC,PA.ORDEN," & vbCrLf
sConsulta = sConsulta & "           PA.OP_POND,PA.NUM_POND,PA.POND_SI,PA.POND_NO" & vbCrLf
sConsulta = sConsulta & "     FROM PROCE_ATRIB PA" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN DEF_ATRIB DA" & vbCrLf
sConsulta = sConsulta & "                   ON PA.ATRIB = DA.ID" & vbCrLf
sConsulta = sConsulta & "    WHERE PA.ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "      AND PA.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "      AND PA.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "      AND ISNULL(PA.GRUPO,'#') = CASE WHEN PA.GRUPO IS NULL THEN ISNULL(PA.GRUPO,'#') ELSE @GRUPO END" & vbCrLf
sConsulta = sConsulta & "      AND PA.AMBITO = 2" & vbCrLf
sConsulta = sConsulta & "      AND PA.INTERNO = CASE WHEN @INTERNO IS NULL THEN PA.INTERNO ELSE @INTERNO END" & vbCrLf
sConsulta = sConsulta & "      AND DA.ID = CASE WHEN @ATRIB IS NULL THEN DA.ID ELSE @ATRIB END" & vbCrLf
sConsulta = sConsulta & "    ORDER BY DA.COD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  ELSE" & vbCrLf
sConsulta = sConsulta & "    SELECT PA.ATRIB, PA.ID, DA.COD, DA.DEN, PA.INTRO, DA.TIPO,  PA.AMBITO," & vbCrLf
sConsulta = sConsulta & "           PA.MINNUM, PA.MINFEC," & vbCrLf
sConsulta = sConsulta & "           PA.MAXNUM, PA.MAXFEC," & vbCrLf
sConsulta = sConsulta & "           NULL DALVALOR_TEXT," & vbCrLf
sConsulta = sConsulta & "           NULL DALVALOR_NUM," & vbCrLf
sConsulta = sConsulta & "           NULL DALVALOR_FEC,   " & vbCrLf
sConsulta = sConsulta & "           PA.INTERNO,PA.POND,PA.DEF_PREC,PA.PREFBAJO," & vbCrLf
sConsulta = sConsulta & "           PA.OBLIG,PA.APLIC_PREC,PA.OP_PREC,PA.GRUPO,PA.USAR_PREC,PA.ORDEN," & vbCrLf
sConsulta = sConsulta & "           PA.OP_POND,PA.NUM_POND,PA.POND_SI,PA.POND_NO" & vbCrLf
sConsulta = sConsulta & "     FROM PROCE_ATRIB PA" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN DEF_ATRIB DA" & vbCrLf
sConsulta = sConsulta & "                   ON PA.ATRIB = DA.ID" & vbCrLf
sConsulta = sConsulta & "    WHERE PA.ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "      AND PA.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "      AND PA.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "      AND PA.AMBITO = 2" & vbCrLf
sConsulta = sConsulta & "      AND PA.INTERNO = CASE WHEN @INTERNO IS NULL THEN PA.INTERNO ELSE @INTERNO END" & vbCrLf
sConsulta = sConsulta & "      AND DA.ID = CASE WHEN @ATRIB IS NULL THEN DA.ID ELSE @ATRIB END" & vbCrLf
sConsulta = sConsulta & "    ORDER BY DA.COD" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "ELSE IF @AMBITO = 3" & vbCrLf
sConsulta = sConsulta & "  IF @GRUPO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "    SELECT PA.ATRIB, PA.ID, DA.COD, DA.DEN, PA.INTRO, DA.TIPO,  PA.AMBITO," & vbCrLf
sConsulta = sConsulta & "           PA.MINNUM, PA.MINFEC," & vbCrLf
sConsulta = sConsulta & "           PA.MAXNUM, PA.MAXFEC," & vbCrLf
sConsulta = sConsulta & "           NULL DALVALOR_TEXT," & vbCrLf
sConsulta = sConsulta & "           NULL DALVALOR_NUM," & vbCrLf
sConsulta = sConsulta & "           NULL DALVALOR_FEC," & vbCrLf
sConsulta = sConsulta & "           PA.INTERNO,PA.POND,PA.DEF_PREC,PA.PREFBAJO," & vbCrLf
sConsulta = sConsulta & "           PA.OBLIG,PA.APLIC_PREC,PA.OP_PREC,PA.GRUPO,PA.USAR_PREC,PA.ORDEN," & vbCrLf
sConsulta = sConsulta & "           PA.OP_POND,PA.NUM_POND,PA.POND_SI,PA.POND_NO" & vbCrLf
sConsulta = sConsulta & "     FROM PROCE_ATRIB PA" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN DEF_ATRIB DA" & vbCrLf
sConsulta = sConsulta & "                   ON PA.ATRIB = DA.ID" & vbCrLf
sConsulta = sConsulta & "    WHERE PA.ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "      AND PA.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "      AND PA.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "      AND ISNULL(PA.GRUPO,'#') = CASE WHEN PA.GRUPO IS NULL THEN ISNULL(PA.GRUPO,'#') ELSE @GRUPO END" & vbCrLf
sConsulta = sConsulta & "      AND PA.AMBITO = 3" & vbCrLf
sConsulta = sConsulta & "      AND PA.INTERNO = CASE WHEN @INTERNO IS NULL THEN PA.INTERNO ELSE @INTERNO END" & vbCrLf
sConsulta = sConsulta & "      AND DA.ID = CASE WHEN @ATRIB IS NULL THEN DA.ID ELSE @ATRIB END" & vbCrLf
sConsulta = sConsulta & "    ORDER BY DA.COD" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "   SELECT PA.ATRIB, PA.ID, DA.COD, DA.DEN, PA.INTRO, DA.TIPO,  PA.AMBITO," & vbCrLf
sConsulta = sConsulta & "                PA.MINNUM, PA.MINFEC," & vbCrLf
sConsulta = sConsulta & "                PA.MAXNUM, PA.MAXFEC," & vbCrLf
sConsulta = sConsulta & "                NULL DALVALOR_TEXT," & vbCrLf
sConsulta = sConsulta & "                NULL DALVALOR_NUM," & vbCrLf
sConsulta = sConsulta & "                NULL DALVALOR_FEC," & vbCrLf
sConsulta = sConsulta & "                PA.INTERNO,PA.POND,PA.DEF_PREC,PA.PREFBAJO," & vbCrLf
sConsulta = sConsulta & "                PA.OBLIG,PA.APLIC_PREC,PA.OP_PREC,PA.GRUPO,PA.USAR_PREC,PA.ORDEN," & vbCrLf
sConsulta = sConsulta & "                PA.OP_POND,PA.NUM_POND,PA.POND_SI,PA.POND_NO" & vbCrLf
sConsulta = sConsulta & "       FROM PROCE_ATRIB PA" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN DEF_ATRIB DA" & vbCrLf
sConsulta = sConsulta & "                   ON PA.ATRIB = DA.ID" & vbCrLf
sConsulta = sConsulta & "       WHERE PA.ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "         AND PA.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "         AND PA.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "         AND PA.AMBITO = 3" & vbCrLf
sConsulta = sConsulta & "               AND PA.INTERNO = CASE WHEN @INTERNO IS NULL THEN PA.INTERNO ELSE @INTERNO END" & vbCrLf
sConsulta = sConsulta & "               AND DA.ID = CASE WHEN @ATRIB IS NULL THEN DA.ID ELSE @ATRIB END" & vbCrLf
sConsulta = sConsulta & "           ORDER BY DA.COD" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ITEMS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_ITEMS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "CREATE PROCEDURE  SP_ITEMS  @ANYO INT,@GMN1 VARCHAR(50),@PROCE INT,@GRUPO VARCHAR(50)=null, @PROVE varchar(50), @OFE smallint, @INTERNO TINYINT = NULL, @TODOS TINYINT = 0, @MON varchar(50) = null  AS " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "set NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @CAM FLOAT " & vbCrLf
sConsulta = sConsulta & "DECLARE @EQUIVPROC FLOAT  " & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCEEST smallint" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @TIPOATRIB tinyint" & vbCrLf
sConsulta = sConsulta & "DECLARE @CODATRIB varchar(500)" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDPROCEATRIB int" & vbCrLf
sConsulta = sConsulta & "DECLARE @PARAMDEF NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @ALTER NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @INSERT NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @EQUIVPROC = CAMBIO, @PROCEEST =EST " & vbCrLf
sConsulta = sConsulta & "  FROM PROCE " & vbCrLf
sConsulta = sConsulta & " WHERE ANYO=@ANYO" & vbCrLf
sConsulta = sConsulta & "   AND GMN1=@GMN1" & vbCrLf
sConsulta = sConsulta & "   AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @CAM =( SELECT CAMBIO" & vbCrLf
sConsulta = sConsulta & "             FROM PROCE_OFE" & vbCrLf
sConsulta = sConsulta & "            WHERE ANYO=@ANYO" & vbCrLf
sConsulta = sConsulta & "              AND GMN1=@GMN1" & vbCrLf
sConsulta = sConsulta & "              AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "              AND PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "              AND OFE=@OFE)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CAM IS NULL" & vbCrLf
sConsulta = sConsulta & "  SELECT @CAM =  EQUIV / @EQUIVPROC" & vbCrLf
sConsulta = sConsulta & "    FROM MON " & vbCrLf
sConsulta = sConsulta & "   WHERE COD = @MON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "declare C1 CURSOR LOCAL FOR" & vbCrLf
sConsulta = sConsulta & "SELECT PA.ID, DA.COD, DA.TIPO" & vbCrLf
sConsulta = sConsulta & " FROM PROCE_ATRIB PA" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN DEF_ATRIB DA" & vbCrLf
sConsulta = sConsulta & "               ON PA.ATRIB = DA.ID" & vbCrLf
sConsulta = sConsulta & "WHERE PA.ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "  AND PA.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "  AND PA.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "  AND ISNULL(PA.GRUPO,'#') = CASE WHEN PA.GRUPO IS NULL THEN ISNULL(PA.GRUPO,'#') ELSE @GRUPO END" & vbCrLf
sConsulta = sConsulta & "  AND PA.AMBITO = 3" & vbCrLf
sConsulta = sConsulta & "  AND PA.INTERNO = CASE WHEN @INTERNO IS NULL THEN PA.INTERNO ELSE @INTERNO END" & vbCrLf
sConsulta = sConsulta & "ORDER BY DA.COD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE #ATRIBS (ATANYO int NULL ," & vbCrLf
If gCollation = "" Then
    sConsulta = sConsulta & "   ATGMN1 varchar (50) NULL ," & vbCrLf
Else
    sConsulta = sConsulta & "   ATGMN1 varchar (50) COLLATE " & gCollation & " NULL ," & vbCrLf
End If
sConsulta = sConsulta & "   ATPROCE int NULL ," & vbCrLf
sConsulta = sConsulta & "   ATITEM int NULL " & vbCrLf
sConsulta = sConsulta & ") " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @INSERT = 'INSERT INTO #ATRIBS (ATANYO, ATGMN1, ATPROCE, ATITEM, '" & vbCrLf
sConsulta = sConsulta & "SET @SQL='SELECT ANYO, GMN1,  PROCE, ITEM, '" & vbCrLf
sConsulta = sConsulta & "set @ALTER = 'ALTER TABLE #ATRIBS ADD '" & vbCrLf
sConsulta = sConsulta & "OPEN C1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C1 INTO @IDPROCEATRIB, @CODATRIB, @TIPOATRIB" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS = 0" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    " & vbCrLf
sConsulta = sConsulta & "    SET @INSERT = @INSERT + '[AT_' + @CODATRIB + '],'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    IF @TIPOATRIB = 1 " & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "        SET @ALTER = @ALTER  + '[AT_' + @CODATRIB + '] VARCHAR(800), '" & vbCrLf
sConsulta = sConsulta & "        SET @SQL = @SQL + 'MAX(case IOA.ATRIB_ID when ' + cast(@IDPROCEATRIB as varchar(100)) + ' THEN VALOR_TEXT else NULL end) as [' + @CODATRIB + '],'" & vbCrLf
sConsulta = sConsulta & "      END " & vbCrLf
sConsulta = sConsulta & "    ELSE IF @TIPOATRIB = 2" & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "        SET @ALTER = @ALTER  + '[AT_' + @CODATRIB + '] FLOAT, '" & vbCrLf
sConsulta = sConsulta & "        SET @SQL = @SQL + 'MAX(case IOA.ATRIB_ID when ' + cast(@IDPROCEATRIB as varchar(100)) + ' THEN VALOR_NUM else NULL end) as [' + @CODATRIB + '],'" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "    ELSE IF @TIPOATRIB = 3" & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "        SET @ALTER = @ALTER  + '[AT_' + @CODATRIB + '] DATETIME, '" & vbCrLf
sConsulta = sConsulta & "        SET @SQL = @SQL + 'MAX(case IOA.ATRIB_ID when ' + cast(@IDPROCEATRIB as varchar(100)) + ' THEN VALOR_FEC else NULL end) as [' + @CODATRIB + '],'" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "    ELSE " & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "        SET @ALTER = @ALTER + '[AT_' + @CODATRIB + '] TINYINT, '" & vbCrLf
sConsulta = sConsulta & "        SET @SQL = @SQL + 'MAX(case IOA.ATRIB_ID when ' + cast(@IDPROCEATRIB as varchar(100)) + ' THEN VALOR_BOOL else NULL end) as [' + @CODATRIB + '],'" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    FETCH NEXT FROM C1 INTO @IDPROCEATRIB, @CODATRIB, @TIPOATRIB" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if @INSERT != 'INSERT INTO #ATRIBS (ATANYO, ATGMN1, ATPROCE, ATITEM, '" & vbCrLf
sConsulta = sConsulta & "  begin" & vbCrLf
sConsulta = sConsulta & "    set @SQL = LEFT(@SQL,LEN(@SQL)-1)" & vbCrLf
sConsulta = sConsulta & "    set @INSERT = LEFT(@INSERT,LEN(@INSERT)-1)" & vbCrLf
sConsulta = sConsulta & "    set @ALTER = LEFT(@ALTER,LEN(@ALTER)-1)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    EXEC SP_EXECUTESQL  @ALTER " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    SET @SQL = @INSERT + ')" & vbCrLf
sConsulta = sConsulta & "    ' + @SQL + '" & vbCrLf
sConsulta = sConsulta & "      FROM OFE_ITEM_ATRIB IOA" & vbCrLf
sConsulta = sConsulta & "     WHERE IOA.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "       AND IOA.GMN1 = @GMN1  " & vbCrLf
sConsulta = sConsulta & "       AND IOA.PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "       AND IOA.PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "       AND IOA.OFE = @OFE" & vbCrLf
sConsulta = sConsulta & "    GROUP BY ANYO, GMN1,  PROCE, ITEM'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    SET @PARAMDEF = '@ANYO INT, @GMN1 VARCHAR(50), @PROCE INT,@GRUPO VARCHAR(50), @PROVE varchar(50), @OFE smallint'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    EXEC SP_EXECUTESQL  @SQL, @PARAMDEF, @ANYO = @ANYO, @GMN1 = @GMN1, @PROCE = @PROCE, @GRUPO = @GRUPO, @PROVE = @PROVE, @OFE = @OFE" & vbCrLf
sConsulta = sConsulta & "  end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--   Se devuelve siempre en la moneda de la oferta" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT I.ANYO, I.GMN1, I.PROCE, I.GRUPO, I.ID, I.GMN2, I.GMN3, I.GMN4, I.ART, I.DESCR + A.DEN AS DESCR, " & vbCrLf
sConsulta = sConsulta & "       I.DEST, I.UNI,UNI.DEN UNIDEN, I.CANT, I.PREC*@CAM as PREC, I.PRES*@CAM as PRES," & vbCrLf
sConsulta = sConsulta & "       I.PAG, PAG.DEN DENPAG, I.FECINI, I.FECFIN, I.ESP, I.CONF, I.EST," & vbCrLf
sConsulta = sConsulta & "       I.OBJ*@CAM AS OBJ , I.FECHAOBJ, " & vbCrLf
sConsulta = sConsulta & "       O.PROVE MINPROVE, P2.DEN MINPROVEDEN, O.PRECIO*@EQUIVPROC*@CAM MINPRECIO," & vbCrLf
sConsulta = sConsulta & "       CASE WHEN @PROVE = O.PROVE THEN 1 ELSE 0 END PROPIA," & vbCrLf
sConsulta = sConsulta & "       CASE  WHEN NUMESP > 0 THEN 1 WHEN NOT I.ESP  IS NULL THEN 1 ELSE 0 END ESPECADJ ," & vbCrLf
sConsulta = sConsulta & "       ITO.PRECIO, ITO.CANTMAX, ITO.ULT, ITO.PRECIO2, ITO.PRECIO3,ITO.USAR,ITO.FECACT," & vbCrLf
sConsulta = sConsulta & "       IA.NUMADJUN, ITO.COMENT1, ITO.COMENT2, ITO.COMENT3, @EQUIVPROC*@CAM CAMBIO, cast(ITO.OBSADJUN as varchar(2000)) OBSADJUN, ATR.*" & vbCrLf
sConsulta = sConsulta & "  FROM ITEM I" & vbCrLf
sConsulta = sConsulta & "        LEFT JOIN ART4 A" & vbCrLf
sConsulta = sConsulta & "               ON I.ART = A.COD " & vbCrLf
sConsulta = sConsulta & "              AND I.GMN4 = A.GMN4 " & vbCrLf
sConsulta = sConsulta & "              AND I.GMN3 = A.GMN3 " & vbCrLf
sConsulta = sConsulta & "              AND I.GMN2 = A.GMN2" & vbCrLf
sConsulta = sConsulta & "              AND I.GMN1 = A.GMN1" & vbCrLf
sConsulta = sConsulta & "        LEFT JOIN ITEM_OFE ITO" & vbCrLf
sConsulta = sConsulta & "               ON I.ANYO = ITO.ANYO" & vbCrLf
sConsulta = sConsulta & "              AND I.GMN1 = ITO.GMN1" & vbCrLf
sConsulta = sConsulta & "              AND I.PROCE = ITO.PROCE" & vbCrLf
sConsulta = sConsulta & "              AND I.ID = ITO.ITEM" & vbCrLf
sConsulta = sConsulta & "              AND @PROVE = ITO.PROVE" & vbCrLf
sConsulta = sConsulta & "              AND @OFE = ITO.NUM" & vbCrLf
sConsulta = sConsulta & "        LEFT JOIN (OFEMIN O " & vbCrLf
sConsulta = sConsulta & "                     INNER JOIN PROVE P2 " & vbCrLf
sConsulta = sConsulta & "                             ON O.PROVE = P2.COD" & vbCrLf
sConsulta = sConsulta & "                   )" & vbCrLf
sConsulta = sConsulta & "               ON I.ANYO = O.ANYO" & vbCrLf
sConsulta = sConsulta & "              AND I.GMN1 = O.GMN1" & vbCrLf
sConsulta = sConsulta & "              AND I.PROCE = O.PROCE" & vbCrLf
sConsulta = sConsulta & "              AND I.ID = O.ITEM" & vbCrLf
sConsulta = sConsulta & "               " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "        LEFT JOIN #ATRIBS ATR               ON I.ANYO = ATR.ATANYO" & vbCrLf
sConsulta = sConsulta & "              AND I.GMN1 = ATR.ATGMN1" & vbCrLf
sConsulta = sConsulta & "              AND I.PROCE = ATR.ATPROCE" & vbCrLf
sConsulta = sConsulta & "              AND I.ID = ATR.ATITEM        LEFT JOIN (SELECT IE.ANYO, IE.GMN1, IE.PROCE, IE.ITEM, COUNT(IE.ID) NUMESP " & vbCrLf
sConsulta = sConsulta & "                     FROM ITEM_ESP IE" & vbCrLf
sConsulta = sConsulta & "                    WHERE IE.ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "                      AND IE.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "                      AND IE.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "                 GROUP BY IE.ANYO, IE.GMN1, IE.PROCE, IE.ITEM) IE" & vbCrLf
sConsulta = sConsulta & "               ON I.ID = IE.ITEM " & vbCrLf
sConsulta = sConsulta & "              AND I.GMN1 = IE.GMN1" & vbCrLf
sConsulta = sConsulta & "              AND I.PROCE = IE.PROCE" & vbCrLf
sConsulta = sConsulta & "              AND I.ANYO = IE.ANYO " & vbCrLf
sConsulta = sConsulta & "        LEFT JOIN (SELECT IA.ANYO, IA.GMN1, IA.PROCE, IA.ITEM, COUNT(IA.ID) NUMADJUN" & vbCrLf
sConsulta = sConsulta & "                     FROM OFE_ITEM_ADJUN IA" & vbCrLf
sConsulta = sConsulta & "                    WHERE IA.ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "                      AND IA.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "                      AND IA.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "                      AND IA.PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "                      AND IA.OFE = @OFE" & vbCrLf
sConsulta = sConsulta & "                 GROUP BY IA.ANYO, IA.GMN1, IA.PROCE, IA.ITEM) IA" & vbCrLf
sConsulta = sConsulta & "               ON I.ID = IA.ITEM " & vbCrLf
sConsulta = sConsulta & "              AND I.GMN1 = IA.GMN1" & vbCrLf
sConsulta = sConsulta & "              AND I.PROCE = IA.PROCE" & vbCrLf
sConsulta = sConsulta & "              AND I.ANYO = IA.ANYO " & vbCrLf
sConsulta = sConsulta & "        LEFT JOIN PAG " & vbCrLf
sConsulta = sConsulta & "               ON I.PAG = PAG.COD" & vbCrLf
sConsulta = sConsulta & "        LEFT JOIN UNI " & vbCrLf
sConsulta = sConsulta & "               ON I.UNI = UNI.COD" & vbCrLf
sConsulta = sConsulta & "WHERE I.ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "  AND I.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "  AND I.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "  AND I.CONF=1" & vbCrLf
sConsulta = sConsulta & "  AND I.GRUPO=case when @GRUPO is null then i.grupo else @GRUPO END" & vbCrLf
sConsulta = sConsulta & "  AND CASE WHEN I.EST IS NULL THEN -1 ELSE I.EST END = CASE WHEN @PROCEEST=11 THEN CASE WHEN @TODOS=1 THEN I.EST ELSE 0 END ELSE -1 END" & vbCrLf
sConsulta = sConsulta & "ORDER BY I.ART,DESCR" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub
