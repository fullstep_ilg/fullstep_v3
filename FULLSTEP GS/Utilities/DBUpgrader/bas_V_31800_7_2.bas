Attribute VB_Name = "bas_V_31800_7_2"
Option Explicit

Public Function CodigoDeActualizacion31800_07_0847A_31800_07_0848() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim iRespuesta As Integer


On Error GoTo Error
sConsulta = "BEGIN TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = True
ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

frmProgreso.lblDetalle = "Actualiza Storeds"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
frmProgreso.ProgressBar1.Refresh
V_31800_7_Storeds_048

sConsulta = "UPDATE VERSION SET NUM ='3.00.08.48'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103), PRODVER='31800.7'"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "COMMIT TRANSACTION"
ExecuteSQL gRDOCon, sConsulta

bTransaccionEnCurso = False

CodigoDeActualizacion31800_07_0847A_31800_07_0848 = True
Exit Function

Error:
    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If

    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    CodigoDeActualizacion31800_07_0847A_31800_07_0848 = False
End Function


Private Sub V_31800_7_Storeds_048()

Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSQA_CERTIFICADO_DATOS_EMAIL]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSQA_CERTIFICADO_DATOS_EMAIL]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSQA_CERTIFICADO_DATOS_EMAIL @CERTIFICADO INT,@TYPE TINYINT AS" & vbCrLf
sConsulta = sConsulta & "/*" & vbCrLf
sConsulta = sConsulta & "---Descripcion:= Devuelve los datos sobre la informacion del usuario, asi como el asunto del email." & vbCrLf
sConsulta = sConsulta & "---Parametros entrada:" & vbCrLf
sConsulta = sConsulta & "   @CERTIFICADO: Id certificado" & vbCrLf
sConsulta = sConsulta & "   @TYPE:= tipo de envio de mail" & vbCrLf
sConsulta = sConsulta & "--Llamada desde:= root.vb--> Notificador_QADevolverDatosEMail" & vbCrLf
sConsulta = sConsulta & "--Tiempo ejecucion:= 0,3seg." & vbCrLf
sConsulta = sConsulta & "*/" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL  AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @COMP VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @CON_PORTAL INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @CONAUSAR INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @CONACTUAL INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVE VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @MODULOID INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "/*" & vbCrLf
sConsulta = sConsulta & "@TYPE" & vbCrLf
sConsulta = sConsulta & "1: Expiraci�n de certificados" & vbCrLf
sConsulta = sConsulta & "2: Certificados pr�ximos a caducar" & vbCrLf
sConsulta = sConsulta & "3: Certificados sin respuesta" & vbCrLf
sConsulta = sConsulta & "4: Certificado solicitado" & vbCrLf
sConsulta = sConsulta & "5: Certificado renovado" & vbCrLf
sConsulta = sConsulta & "6: Certificado enviado" & vbCrLf
sConsulta = sConsulta & "*/" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- 15/05/2008 Incidencia Pruebas 31600.8 / 2008 / 299" & vbCrLf
sConsulta = sConsulta & "-- Manda siempre al contacto de calidad" & vbCrLf
sConsulta = sConsulta & "-- Si quien esta en bbdd no es al que le envias cambia la bbdd" & vbCrLf
sConsulta = sConsulta & "-- 28/05/2008 Fagor Ederlan / 2008 / 198   " & vbCrLf
sConsulta = sConsulta & "-- 1� Contacto Calidad 2�Contacto Certificado (si existe como contacto) 3� Si no existe Contacto Certificado, 1er contacto piyes" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @TYPE >=1 AND @TYPE <=5" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SELECT @CON_PORTAL=CASE WHEN CON2.ID IS NULL THEN CON.ID_PORT ELSE CON2.ID_PORT END,@PROVE=C.PROVE," & vbCrLf
sConsulta = sConsulta & "   @CONAUSAR=ISNULL(CON2.ID,CON.ID),@CONACTUAL=CON.ID" & vbCrLf
sConsulta = sConsulta & "       FROM CERTIFICADO C WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN CON WITH (NOLOCK) ON C.PROVE=CON.PROVE AND C.CONTACTO=CON.ID" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN CON CON2 WITH (NOLOCK) ON C.PROVE=CON2.PROVE AND CON2.CALIDAD=1" & vbCrLf
sConsulta = sConsulta & "   WHERE C.ID=@CERTIFICADO" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   IF NOT(@CONAUSAR=@CONACTUAL)" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       UPDATE CERTIFICADO SET CONTACTO=@CONAUSAR  WHERE ID=@CERTIFICADO" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       --Y si es que el contacto ya no existe?" & vbCrLf
sConsulta = sConsulta & "       --  El contacto s� existe -> ok contacto" & vbCrLf
sConsulta = sConsulta & "       --  El contacto no existe" & vbCrLf
sConsulta = sConsulta & "       --      Usa el de calidad si hay" & vbCrLf
sConsulta = sConsulta & "       --      Usa el 1ro si no hay de calidad" & vbCrLf
sConsulta = sConsulta & "       IF (SELECT COUNT(1) FROM CERTIFICADO C WITH (NOLOCK) INNER JOIN CON WITH (NOLOCK) ON C.PROVE=CON.PROVE AND C.CONTACTO=CON.ID WHERE C.ID=@CERTIFICADO)=0" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           SELECT @PROVE=C.PROVE FROM CERTIFICADO C WITH (NOLOCK) WHERE C.ID=@CERTIFICADO" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "           IF (SELECT COUNT(1) FROM CON WITH (NOLOCK) WHERE CON.PROVE=@PROVE AND CON.CALIDAD=1)>0  " & vbCrLf
sConsulta = sConsulta & "               UPDATE CERTIFICADO SET CONTACTO=(SELECT ID FROM CON WITH (NOLOCK) WHERE CON.PROVE=@PROVE AND CON.CALIDAD=1)  WHERE ID=@CERTIFICADO" & vbCrLf
sConsulta = sConsulta & "           ELSE" & vbCrLf
sConsulta = sConsulta & "               UPDATE CERTIFICADO SET CONTACTO=(SELECT MIN(ID) FROM CON WITH (NOLOCK) WHERE CON.PROVE=@PROVE)  WHERE ID=@CERTIFICADO           " & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=N'SELECT CASE WHEN CON2.ID IS NULL THEN CON.EMAIL ELSE CON2.EMAIL END EMAIL," & vbCrLf
sConsulta = sConsulta & "       CASE WHEN CON2.ID IS NULL THEN CON.TIPOEMAIL ELSE CON2.TIPOEMAIL END TIPOEMAIL," & vbCrLf
sConsulta = sConsulta & "           CASE WHEN CON2.IDI IS NULL AND CON.IDI IS NULL THEN (SELECT IDIOMA FROM PARGEN_DEF WHERE PARGEN_DEF.ID=1)  ELSE CASE WHEN CON2.IDI IS NULL THEN CON.IDI ELSE CON2.IDI END END IDIOMA," & vbCrLf
sConsulta = sConsulta & "       WEBFSWSTEXT.*" & vbCrLf
sConsulta = sConsulta & "       ,CASE WHEN CON2.ID IS NULL THEN CON.DATEFMT ELSE CON2.DATEFMT END DATEFMT" & vbCrLf
sConsulta = sConsulta & "           FROM CERTIFICADO C WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN CON WITH (NOLOCK) ON C.PROVE=CON.PROVE AND C.CONTACTO=CON.ID" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN CON CON2 WITH (NOLOCK) ON C.PROVE=CON2.PROVE AND CON2.CALIDAD=1'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   --Calculo del id del modulo del idioma para evitar muchos if..." & vbCrLf
sConsulta = sConsulta & "   IF @TYPE = 5" & vbCrLf
sConsulta = sConsulta & "        SET @MODULOID = 18" & vbCrLf
sConsulta = sConsulta & "             ELSE" & vbCrLf
sConsulta = sConsulta & "       SET @MODULOID = 8 + @TYPE" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + ' LEFT JOIN WEBFSWSTEXT WITH (NOLOCK) ON MODULO=101 AND WEBFSWSTEXT.ID=' + CONVERT(NVARCHAR(3), @MODULOID)" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE  --Env�o de certificado" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=N'SELECT CON.EMAIL EMAIL," & vbCrLf
sConsulta = sConsulta & "       CON.TIPOEMAIL TIPOEMAIL," & vbCrLf
sConsulta = sConsulta & "           CASE WHEN CON.IDI IS NULL THEN (SELECT IDIOMA FROM PARGEN_DEF WHERE PARGEN_DEF.ID=1)  ELSE CON.IDI  END IDIOMA," & vbCrLf
sConsulta = sConsulta & "       WEBFSWSTEXT.*" & vbCrLf
sConsulta = sConsulta & "       ,CON.DATEFMT DATEFMT" & vbCrLf
sConsulta = sConsulta & "           FROM CERTIFICADO C WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN CON WITH (NOLOCK) ON C.PROVE=CON.PROVE AND C.CONTACTO=CON.ID" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN CON CON2 WITH (NOLOCK) ON C.PROVE=CON2.PROVE AND CON2.CALIDAD=1'" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + ' LEFT JOIN WEBFSWSTEXT WITH (NOLOCK) ON MODULO=101 AND WEBFSWSTEXT.ID=19'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' WHERE C.ID=@CERTIFICADO'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N' @CERTIFICADO INT ',@CERTIFICADO=@CERTIFICADO" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSQA_CALC_PROVE_UNQAS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSQA_CALC_PROVE_UNQAS]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSQA_CALC_PROVE_UNQAS AS" & vbCrLf
sConsulta = sConsulta & "DECLARE PROVEEDORES_UNQA cursor for" & vbCrLf
sConsulta = sConsulta & "   select prove, unqa" & vbCrLf
sConsulta = sConsulta & "from (" & vbCrLf
sConsulta = sConsulta & "   --nivel 0" & vbCrLf
sConsulta = sConsulta & "   SELECT  PROVE, convert(nvarchar(5),UNQA) + '_'+ convert(nvarchar(5),u.nivel)unqa" & vbCrLf
sConsulta = sConsulta & "   FROM PROVE WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PROVE_UNQA PU WITH (NOLOCK) ON PU.PROVE = PROVE.COD" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN UNQA U WITH (NOLOCK) ON U.ID = PU.UNQA" & vbCrLf
sConsulta = sConsulta & "   WHERE (CALIDAD_POT > 0 AND BAJA_CALIDAD=0) " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   UNION" & vbCrLf
sConsulta = sConsulta & "   --las hijas" & vbCrLf
sConsulta = sConsulta & "   SELECT PROVE, convert (nvarchar(5),U.ID) + '_'+ convert(nvarchar(5),u.nivel) UNQA" & vbCrLf
sConsulta = sConsulta & "   FROM PROVE WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PROVE_UNQA PU WITH (NOLOCK) ON PU.PROVE = PROVE.COD" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN UNQA U WITH (NOLOCK) ON U.UNQA1 = PU.UNQA" & vbCrLf
sConsulta = sConsulta & "   WHERE (CALIDAD_POT > 0 AND BAJA_CALIDAD=0) " & vbCrLf
sConsulta = sConsulta & "   UNION" & vbCrLf
sConsulta = sConsulta & "   --las nietas" & vbCrLf
sConsulta = sConsulta & "   SELECT PROVE,  convert (nvarchar(5),U.ID)  + '_'+ convert(nvarchar(5),u.nivel) UNQA" & vbCrLf
sConsulta = sConsulta & "   FROM PROVE WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PROVE_UNQA PU WITH (NOLOCK) ON PU.PROVE = PROVE.COD" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN UNQA U WITH (NOLOCK) ON U.UNQA2 = PU.UNQA" & vbCrLf
sConsulta = sConsulta & "   WHERE (CALIDAD_POT > 0 AND BAJA_CALIDAD=0) " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   union" & vbCrLf
sConsulta = sConsulta & "   --madres" & vbCrLf
sConsulta = sConsulta & "   SELECT PROVE, convert (nvarchar(5),U2.ID) + '_2'  UNQA" & vbCrLf
sConsulta = sConsulta & "       FROM PROVE WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PROVE_UNQA PU WITH (NOLOCK) ON PU.PROVE = PROVE.COD" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN UNQA U WITH (NOLOCK) ON U.id = PU.UNQA" & vbCrLf
sConsulta = sConsulta & "       inner join unqa u2 with (nolock) on u2.id = u.unqa1" & vbCrLf
sConsulta = sConsulta & "       WHERE (CALIDAD_POT > 0 AND BAJA_CALIDAD=0) " & vbCrLf
sConsulta = sConsulta & "   union" & vbCrLf
sConsulta = sConsulta & "   --amamas" & vbCrLf
sConsulta = sConsulta & "   SELECT PROVE, convert (nvarchar(5),U2.id) + '_1'  UNQA" & vbCrLf
sConsulta = sConsulta & "       FROM PROVE WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PROVE_UNQA PU WITH (NOLOCK) ON PU.PROVE = PROVE.COD" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN UNQA U WITH (NOLOCK) ON U.id = PU.UNQA" & vbCrLf
sConsulta = sConsulta & "       inner join unqa u2 with (nolock) on u2.id = u.unqa2" & vbCrLf
sConsulta = sConsulta & "       WHERE (CALIDAD_POT > 0 AND BAJA_CALIDAD=0)  " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & ") PROVEEDORES_UNQA " & vbCrLf
sConsulta = sConsulta & "order by prove" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_NIVEL0 INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @ID_NIVEL0=ID FROM UNQA WITH (NOLOCK) WHERE NIVEL = 0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVE NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVEANT NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @UNQAS NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @UNQAS_OK NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE #T (PROVE NVARCHAR(100), UNQAS NVARCHAR(100))" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "OPEN PROVEEDORES_UNQA FETCH NEXT FROM PROVEEDORES_UNQA INTO @PROVE, @UNQAS" & vbCrLf
sConsulta = sConsulta & "SET @UNQAS_OK = CONVERT(NVARCHAR(10),@ID_NIVEL0) + '_0,'" & vbCrLf
sConsulta = sConsulta & "SET @PROVEANT=''" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS = 0 " & vbCrLf
sConsulta = sConsulta & "BEGIN " & vbCrLf
sConsulta = sConsulta & "   IF @PROVE <> @PROVEANT AND @PROVEANT <> ''" & vbCrLf
sConsulta = sConsulta & "   begin" & vbCrLf
sConsulta = sConsulta & "   IF @UNQAS_OK <> ''" & vbCrLf
sConsulta = sConsulta & "       SET @UNQAS_OK = LEFT(@UNQAS_OK, LEN(@UNQAS_OK)-1)" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO #T (PROVE, UNQAS) VALUES (@PROVEANT, @UNQAS_OK)" & vbCrLf
sConsulta = sConsulta & "   SET @UNQAS_OK = CONVERT(NVARCHAR(10),@ID_NIVEL0) + '_0,'" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   end" & vbCrLf
sConsulta = sConsulta & "set @UNQAS_OK = @UNQAS_OK  + @UNQAS+ ','" & vbCrLf
sConsulta = sConsulta & "set @proveant = @prove" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM PROVEEDORES_UNQA  INTO @PROVE, @UNQAS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @UNQAS_OK <> ''" & vbCrLf
sConsulta = sConsulta & "   SET @UNQAS_OK = LEFT(@UNQAS_OK, LEN(@UNQAS_OK)-1)" & vbCrLf
sConsulta = sConsulta & "INSERT INTO #T (PROVE, UNQAS) VALUES (@PROVEANT, @UNQAS_OK)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "/*" & vbCrLf
sConsulta = sConsulta & "--Insertar los proveedores que no estan relacionados con ninguna unidad de negocio" & vbCrLf
sConsulta = sConsulta & "--por lo tanto relacionarlos con el grupo" & vbCrLf
sConsulta = sConsulta & "INSERT INTO #T (PROVE, UNQAS)" & vbCrLf
sConsulta = sConsulta & "   SELECT COD PROVE, convert(nvarchar(10),@ID_NIVEL0)+'_0'" & vbCrLf
sConsulta = sConsulta & "   FROM PROVE WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   WHERE COD NOT IN (" & vbCrLf
sConsulta = sConsulta & "       SELECT DISTINCT COD FROM PROVE WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PROVE_UNQA PU WITH (NOLOCK) ON PU.PROVE = PROVE.COD" & vbCrLf
sConsulta = sConsulta & "   WHERE (CALIDAD_POT > 0 AND BAJA_CALIDAD=0))" & vbCrLf
sConsulta = sConsulta & "*/" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT PROVE, UNQAS FROM #T WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "ORDER BY PROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DROP TABLE #T" & vbCrLf
sConsulta = sConsulta & "CLOSE PROVEEDORES_UNQA " & vbCrLf
sConsulta = sConsulta & "DEALLOCATE PROVEEDORES_UNQA" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_DEVOLUCION_INSTANCIA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_DEVOLUCION_INSTANCIA]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_DEVOLUCION_INSTANCIA @INSTANCIA INT,@PER VARCHAR(50) = NULL, @COMENT VARCHAR(500)=NULL, @PROVE VARCHAR (50) = NULL,@ETAPA INT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @ESTADO INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @DESTINATARIO VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PER_ORIGEN AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @ROL  INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @ESTADO=ESTADO FROM INSTANCIA WITH(NOLOCK) WHERE INSTANCIA.ID = @INSTANCIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT TOP 1 @PER_ORIGEN=CASE WHEN PER IS NOT NULL THEN PER ELSE PROVE END  FROM INSTANCIA_EST WITH(NOLOCK) WHERE ACCION=3 AND BLOQUE=@ETAPA AND INSTANCIA=@INSTANCIA ORDER BY ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--En el caso de que se devuelva la solicitud al usuario que ha hecho el traslado" & vbCrLf
sConsulta = sConsulta & "IF @PROVE IS NULL" & vbCrLf
sConsulta = sConsulta & "     BEGIN" & vbCrLf
sConsulta = sConsulta & "       SELECT TOP 1 @DESTINATARIO=CASE WHEN PER IS NOT NULL THEN PER ELSE PROVE END" & vbCrLf
sConsulta = sConsulta & "   FROM INSTANCIA_EST WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   WHERE ACCION=3 AND BLOQUE=@ETAPA AND INSTANCIA=@INSTANCIA AND  ( DESTINATARIO=@PER OR DESTINATARIO IN (SELECT U.PER FROM USU U WITH(NOLOCK) WHERE U.FSWS_SUSTITUCION = @PER))" & vbCrLf
sConsulta = sConsulta & "   ORDER BY ID DESC" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SELECT TOP 1 @ROL=R.ID " & vbCrLf
sConsulta = sConsulta & "   FROM PM_COPIA_ROL R WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL_BLOQUE B WITH(NOLOCK) ON R.ID=B.ROL " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN INSTANCIA_BLOQUE WITH(NOLOCK) ON B.BLOQUE=INSTANCIA_BLOQUE.BLOQUE AND  INSTANCIA_BLOQUE.ESTADO=1 AND INSTANCIA_BLOQUE.BLOQ<>1" & vbCrLf
sConsulta = sConsulta & "   WHERE INSTANCIA_BLOQUE.ID=@ETAPA AND R.PER=@PER_ORIGEN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       INSERT INTO INSTANCIA_EST (INSTANCIA, PER,COMENT, FECHA, EST,ACCION,DESTINATARIO,BLOQUE,ROL)  " & vbCrLf
sConsulta = sConsulta & "   VALUES (@INSTANCIA, @PER,@COMENT, GETDATE(), @ESTADO,4,@DESTINATARIO,@ETAPA,@ROL)" & vbCrLf
sConsulta = sConsulta & "     END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "     BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SELECT TOP 1 @DESTINATARIO=CASE WHEN PER IS NOT NULL THEN PER ELSE PROVE END, @ROL=ROL" & vbCrLf
sConsulta = sConsulta & "   FROM INSTANCIA_EST WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   WHERE ACCION=3 AND BLOQUE=@ETAPA AND DESTINATARIO_PROV=@PROVE AND INSTANCIA=@INSTANCIA " & vbCrLf
sConsulta = sConsulta & "   ORDER BY ID DESC" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   INSERT INTO INSTANCIA_EST (INSTANCIA, COMENT, FECHA, EST,ACCION,DESTINATARIO,BLOQUE,PROVE,ROL)  " & vbCrLf
sConsulta = sConsulta & "   VALUES (@INSTANCIA, @COMENT, GETDATE(), @ESTADO,4,@DESTINATARIO,@ETAPA,@PROVE,@ROL)" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   DELETE FROM p_copia_linea_desglose_adjun " & vbCrLf
sConsulta = sConsulta & "   FROM p_copia_linea_desglose_adjun CLDA" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN P_COPIA_CAMPO C" & vbCrLf
sConsulta = sConsulta & "             ON C.ID = CLDA.CAMPO_HIJO" & vbCrLf
sConsulta = sConsulta & "            AND C.PROVE = CLDA.PROVE" & vbCrLf
sConsulta = sConsulta & "   WHERE C.INSTANCIA = @INSTANCIA AND C.PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   DELETE FROM p_copia_linea_desglose " & vbCrLf
sConsulta = sConsulta & "   FROM p_copia_linea_desglose CLD" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN P_COPIA_CAMPO C" & vbCrLf
sConsulta = sConsulta & "             ON C.ID = CLD.CAMPO_HIJO" & vbCrLf
sConsulta = sConsulta & "            AND C.PROVE = CLD.PROVE" & vbCrLf
sConsulta = sConsulta & "   WHERE C.INSTANCIA = @INSTANCIA AND C.PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   DELETE FROM p_copia_campo_adjun " & vbCrLf
sConsulta = sConsulta & "   FROM p_copia_campo_adjun CA" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN P_COPIA_CAMPO C" & vbCrLf
sConsulta = sConsulta & "             ON C.ID = CA.CAMPO" & vbCrLf
sConsulta = sConsulta & "            AND C.PROVE = CA.PROVE" & vbCrLf
sConsulta = sConsulta & "   WHERE C.INSTANCIA = @INSTANCIA AND C.PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   DELETE FROM P_COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "   WHERE INSTANCIA = @INSTANCIA AND PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @PER_ORIGEN=@DESTINATARIO  --Si ha vuelto al rol que realiz� el traslado" & vbCrLf
sConsulta = sConsulta & "      BEGIN" & vbCrLf
sConsulta = sConsulta & "   UPDATE INSTANCIA_BLOQUE SET TRASLADADA=0,TRASLADO_A=NULL, DEVOLUCION_A=NULL  WHERE ID=@ETAPA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF (SELECT COUNT(*) FROM INSTANCIA_BLOQUE WITH(NOLOCK) WHERE ESTADO=1 AND TRASLADADA=1 AND INSTANCIA=@INSTANCIA)=0" & vbCrLf
sConsulta = sConsulta & "       UPDATE INSTANCIA SET TRASLADADA=0 WHERE ID=@INSTANCIA" & vbCrLf
sConsulta = sConsulta & "     END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "     DECLARE @ORIGEN AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "     SELECT TOP 1 @ORIGEN=CASE WHEN PER IS NOT NULL THEN PER ELSE PROVE END  FROM INSTANCIA_EST WITH(NOLOCK) WHERE ACCION=3 AND BLOQUE=@ETAPA AND INSTANCIA=@INSTANCIA AND DESTINATARIO=@DESTINATARIO ORDER BY ID DESC" & vbCrLf
sConsulta = sConsulta & "     UPDATE INSTANCIA_BLOQUE SET TRASLADO_A=@DESTINATARIO, DEVOLUCION_A=@ORIGEN WHERE ID=@ETAPA" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GET_CAMPOS_FILTRO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GET_CAMPOS_FILTRO]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_GET_CAMPOS_FILTRO @USU VARCHAR(50), @IDI VARCHAR(20), @ID INT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE #T1 (ID nVARCHAR(10), NOMBRE nVARCHAR(200), PADRE nVARCHAR(10))" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "------------------------  GRUPOS --------------------------" & vbCrLf
sConsulta = sConsulta & "--saca solo los grupos que tienen campos" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'INSERT INTO #T1 (ID, NOMBRE, PADRE) " & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT ''G'' + convert(nvarchar(10),G.ID) as ID,G.DEN_' + @IDI + ' AS NOMBRE,NULL AS PADRE " & vbCrLf
sConsulta = sConsulta & "FROM FORM_GRUPO G WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "INNER JOIN FORM_CAMPO C WITH(NOLOCK) ON G.ID=C.GRUPO " & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_FILTROS F WITH(NOLOCK) ON G.FORMULARIO=F.FORMULARIO" & vbCrLf
sConsulta = sConsulta & "INNER JOIN SOLICITUD S WITH(NOLOCK) ON F.FORMULARIO=S.FORMULARIO" & vbCrLf
sConsulta = sConsulta & "INNER JOIN VIEW_ROL_PETICIONARIO R WITH (NOLOCK) ON S.ID=R.SOLICITUD AND R.COD=@USU" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_ROL_BLOQUE RB WITH(NOLOCK) ON R.ID=RB.ROL" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_CONF_CUMP_BLOQUE CCB WITH(NOLOCK) ON RB.BLOQUE=CCB.BLOQUE AND RB.ROL=CCB.ROL AND C.ID=CCB.CAMPO AND CCB.VISIBLE=1" & vbCrLf
sConsulta = sConsulta & "WHERE C.ES_SUBCAMPO=0 AND C.SUBTIPO<>9 AND (C.TIPO_CAMPO_GS<>106 OR C.TIPO_CAMPO_GS IS NULL) AND F.ID=@ID" & vbCrLf
sConsulta = sConsulta & "UNION" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT ''G'' + convert(nvarchar(10),G.ID) as ID,G.DEN_' + @IDI + ' AS NOMBRE,NULL AS PADRE " & vbCrLf
sConsulta = sConsulta & "FROM FORM_GRUPO G WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "INNER JOIN FORM_CAMPO C WITH(NOLOCK) ON G.ID=C.GRUPO " & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_FILTROS F WITH(NOLOCK) ON G.FORMULARIO=F.FORMULARIO" & vbCrLf
sConsulta = sConsulta & "INNER JOIN SOLICITUD S WITH(NOLOCK) ON F.FORMULARIO=S.FORMULARIO" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_ROL R WITH(NOLOCK) ON S.WORKFLOW=R.WORKFLOW AND R.PER=@USU AND R.TIPO=5" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_ROL_OBS_ETAPAS RB WITH(NOLOCK) ON R.ID=RB.ROL" & vbCrLf
sConsulta = sConsulta & "WHERE C.ES_SUBCAMPO=0 AND C.SUBTIPO<>9 AND (C.TIPO_CAMPO_GS<>106 OR C.TIPO_CAMPO_GS IS NULL)  AND F.ID=@ID'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@USU VARCHAR(50), @ID INT',@USU=@USU,@ID=@ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'INSERT INTO #T1 (ID, NOMBRE, PADRE) " & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT ''G'' + convert(nvarchar(10),G.ID) as ID,G.DEN_' + @IDI + ' AS NOMBRE,NULL AS PADRE" & vbCrLf
sConsulta = sConsulta & "FROM FORM_GRUPO G WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "INNER JOIN FORM_CAMPO C WITH(NOLOCK) ON G.ID=C.GRUPO " & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_FILTROS F WITH(NOLOCK) ON G.FORMULARIO=F.FORMULARIO" & vbCrLf
sConsulta = sConsulta & "INNER JOIN SOLICITUD S WITH(NOLOCK) ON F.FORMULARIO=S.FORMULARIO" & vbCrLf
sConsulta = sConsulta & "INNER JOIN INSTANCIA I WITH (NOLOCK) ON s.id=I.solicitud " & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_COPIA_ROL R WITH(NOLOCK) ON i.id=R.INSTANCIA AND R.PER=@USU" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_COPIA_ROL_BLOQUE RB WITH(NOLOCK) ON R.ID=RB.ROL" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_COPIA_BLOQUE B WITH(NOLOCK) ON RB.BLOQUE=B.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_CONF_CUMP_BLOQUE CCB WITH(NOLOCK) ON B.BLOQUE_ORIGEN=CCB.BLOQUE AND R.ROL_ORIGEN=CCB.ROL AND C.ID=CCB.CAMPO AND CCB.VISIBLE=1" & vbCrLf
sConsulta = sConsulta & "WHERE C.ES_SUBCAMPO=0 AND C.SUBTIPO<>9 AND (C.TIPO_CAMPO_GS<>106 OR C.TIPO_CAMPO_GS IS NULL)  AND F.ID=@ID'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@USU VARCHAR(50), @ID INT',@USU=@USU,@ID=@ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "------------------------  CAMPOS --------------------------" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'INSERT INTO #T1 (ID, NOMBRE, PADRE) " & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT C.ID,C.DEN_' + @IDI + ' AS NOMBRE,''G'' + convert(nvarchar(10),G.ID) AS PADRE" & vbCrLf
sConsulta = sConsulta & "FROM FORM_GRUPO G WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "INNER JOIN FORM_CAMPO C WITH(NOLOCK) ON G.ID=C.GRUPO " & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_FILTROS F WITH(NOLOCK) ON G.FORMULARIO=F.FORMULARIO" & vbCrLf
sConsulta = sConsulta & "INNER JOIN SOLICITUD S WITH(NOLOCK) ON F.FORMULARIO=S.FORMULARIO" & vbCrLf
sConsulta = sConsulta & "INNER JOIN VIEW_ROL_PETICIONARIO R WITH (NOLOCK) ON S.ID=R.SOLICITUD AND R.COD=@USU" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_ROL_BLOQUE RB WITH(NOLOCK) ON R.ID=RB.ROL" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_CONF_CUMP_BLOQUE CCB WITH(NOLOCK) ON RB.BLOQUE=CCB.BLOQUE AND RB.ROL=CCB.ROL AND C.ID=CCB.CAMPO AND CCB.VISIBLE=1" & vbCrLf
sConsulta = sConsulta & "WHERE C.ES_SUBCAMPO=0 AND C.SUBTIPO<>9 AND (C.TIPO_CAMPO_GS<>106 OR C.TIPO_CAMPO_GS IS NULL)  AND F.ID=@ID " & vbCrLf
sConsulta = sConsulta & "UNION" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT C.ID,C.DEN_' + @IDI + ' AS NOMBRE,''G'' + convert(nvarchar(10),G.ID) AS PADRE" & vbCrLf
sConsulta = sConsulta & "FROM FORM_GRUPO G WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "INNER JOIN FORM_CAMPO C WITH(NOLOCK) ON G.ID=C.GRUPO " & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_FILTROS F WITH(NOLOCK) ON G.FORMULARIO=F.FORMULARIO" & vbCrLf
sConsulta = sConsulta & "INNER JOIN SOLICITUD S WITH(NOLOCK) ON F.FORMULARIO=S.FORMULARIO" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_ROL R WITH(NOLOCK) ON S.WORKFLOW=R.WORKFLOW AND R.PER=@USU AND R.TIPO=5" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_ROL_OBS_ETAPAS RB WITH(NOLOCK) ON R.ID=RB.ROL " & vbCrLf
sConsulta = sConsulta & "WHERE C.ES_SUBCAMPO=0 AND C.SUBTIPO<>9 AND (C.TIPO_CAMPO_GS<>106 OR C.TIPO_CAMPO_GS IS NULL)  AND F.ID=@ID'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@USU VARCHAR(50), @ID INT',@USU=@USU,@ID=@ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'INSERT INTO #T1 (ID, NOMBRE, PADRE) " & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT C.ID,C.DEN_' + @IDI + ' AS NOMBRE,''G'' + convert(nvarchar(10),G.ID) AS PADRE" & vbCrLf
sConsulta = sConsulta & "FROM FORM_GRUPO G WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "INNER JOIN FORM_CAMPO C WITH(NOLOCK) ON G.ID=C.GRUPO " & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_FILTROS F WITH(NOLOCK) ON G.FORMULARIO=F.FORMULARIO" & vbCrLf
sConsulta = sConsulta & "INNER JOIN SOLICITUD S WITH(NOLOCK) ON F.FORMULARIO=S.FORMULARIO" & vbCrLf
sConsulta = sConsulta & "INNER JOIN INSTANCIA I WITH (NOLOCK) ON s.id=I.solicitud " & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_COPIA_ROL R WITH(NOLOCK) ON i.id=R.INSTANCIA AND R.PER=@USU" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_COPIA_ROL_BLOQUE RB WITH(NOLOCK) ON R.ID=RB.ROL" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_COPIA_BLOQUE B WITH(NOLOCK) ON RB.BLOQUE=B.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_CONF_CUMP_BLOQUE CCB WITH(NOLOCK) ON B.BLOQUE_ORIGEN=CCB.BLOQUE AND R.ROL_ORIGEN=CCB.ROL AND C.ID=CCB.CAMPO AND CCB.VISIBLE=1" & vbCrLf
sConsulta = sConsulta & "WHERE C.ES_SUBCAMPO=0 AND C.SUBTIPO<>9 AND (C.TIPO_CAMPO_GS<>106 OR C.TIPO_CAMPO_GS IS NULL)  AND F.ID=@ID'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@USU VARCHAR(50), @ID INT',@USU=@USU,@ID=@ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT ID, NOMBRE, PADRE FROM #T1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DROP TABLE #T1" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_GET_INSTANCIAS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_GET_INSTANCIAS]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [FSPM_GET_INSTANCIAS] @IDI AS VARCHAR(20), @USU AS VARCHAR(50), @ORDENACION AS VARCHAR(50)=NULL, @SENTIDOORDENACION AS VARCHAR(50)=NULL," & vbCrLf
sConsulta = sConsulta & "@ID AS INT=NULL, @TITULO VARCHAR(400)=NULL, @TIPO AS INT=NULL, @FECHADESDE AS DATETIME=NULL,@FORMATOFECHA AS VARCHAR(50)=NULL," & vbCrLf
sConsulta = sConsulta & "@ORIGEN AS VARCHAR(50)=NULL, @IMPORTEDESDE AS  FLOAT=NULL,@IMPORTEHASTA AS  FLOAT=NULL ,@PETICIONARIO AS VARCHAR(50)=NULL, @ARTICULO AS VARCHAR(200)=NULL," & vbCrLf
sConsulta = sConsulta & "@ESTADO AS VARCHAR(50)=NULL, @NOM_TABLA AS VARCHAR(50)=NULL, " & vbCrLf
sConsulta = sConsulta & "@BUSCARID AS INT=NULL, @BUSCARTITULO VARCHAR(400)=NULL, @BUSCARTIPO AS INT=NULL, @BUSCARIMPORTEDESDE AS  FLOAT=NULL, @BUSCARIMPORTEHASTA AS  FLOAT=NULL, " & vbCrLf
sConsulta = sConsulta & "@BUSCARFECHADESDE AS DATETIME=NULL,@BUSCARORIGEN AS VARCHAR(50)=NULL, @BUSCARPETICIONARIO AS VARCHAR(50)=NULL, @BUSCARARTICULO AS VARCHAR(200)=NULL," & vbCrLf
sConsulta = sConsulta & "@PROCESOS AS TINYINT=0, @ABIERTASPORUSTED AS TINYINT=0, @PARTICIPO AS TINYINT=0, @PENDIENTES AS TINYINT=0, @PENDDEVOLUCION AS TINYINT=0, @TRASLADADAS AS TINYINT=0, @OTRAS AS TINYINT=0" & vbCrLf
sConsulta = sConsulta & "AS " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL  AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSELECT AS NVARCHAR(4000) " & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLFROM AS NVARCHAR(4000) " & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLFROMPET AS NVARCHAR(4000) " & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLFROMBUSQUEDA AS NVARCHAR(4000) " & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLWHERE  AS NVARCHAR(4000) " & vbCrLf
sConsulta = sConsulta & "DECLARE @TIPOFECHA AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ADICIONAL AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @HAYSELECT TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @TROCEAR TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL2 AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @TIPOFECHA=103" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @FORMATOFECHA='dd/mm/yyyy'" & vbCrLf
sConsulta = sConsulta & "   SET @TIPOFECHA=103" & vbCrLf
sConsulta = sConsulta & "ELSE IF @FORMATOFECHA='mm/dd/yyyy'" & vbCrLf
sConsulta = sConsulta & "   SET @TIPOFECHA=101" & vbCrLf
sConsulta = sConsulta & "ELSE IF @FORMATOFECHA='dd-mm-yyyy'" & vbCrLf
sConsulta = sConsulta & "   SET @TIPOFECHA=105" & vbCrLf
sConsulta = sConsulta & "ELSE IF @FORMATOFECHA='mm-dd-yyyy'" & vbCrLf
sConsulta = sConsulta & "   SET @TIPOFECHA=110" & vbCrLf
sConsulta = sConsulta & "ELSE IF @FORMATOFECHA='mm.dd.yyyy'" & vbCrLf
sConsulta = sConsulta & "   SET @TIPOFECHA=104" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Comprobamos el estado por el que se est� filtrando. No es necesario hacer siempre todas las consultas, as� aligeramos:" & vbCrLf
sConsulta = sConsulta & "IF @ESTADO='0'   ---Guardadas" & vbCrLf
sConsulta = sConsulta & " begin" & vbCrLf
sConsulta = sConsulta & "  SET @PARTICIPO=0" & vbCrLf
sConsulta = sConsulta & "  SET @PENDIENTES=0" & vbCrLf
sConsulta = sConsulta & "  SET @PENDDEVOLUCION=0" & vbCrLf
sConsulta = sConsulta & "  SET @TRASLADADAS=0" & vbCrLf
sConsulta = sConsulta & "  SET @OTRAS=0" & vbCrLf
sConsulta = sConsulta & " end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ESTADO='6' --Rechazadas " & vbCrLf
sConsulta = sConsulta & " begin" & vbCrLf
sConsulta = sConsulta & "  SET @PENDDEVOLUCION=0" & vbCrLf
sConsulta = sConsulta & "  SET @TRASLADADAS=0" & vbCrLf
sConsulta = sConsulta & " end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ESTADO='8' --Anuladas" & vbCrLf
sConsulta = sConsulta & " begin" & vbCrLf
sConsulta = sConsulta & "  SET @PENDDEVOLUCION=0" & vbCrLf
sConsulta = sConsulta & "  SET @TRASLADADAS=0" & vbCrLf
sConsulta = sConsulta & " end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ESTADO='100,101,102,103' --Finalizadas" & vbCrLf
sConsulta = sConsulta & " begin" & vbCrLf
sConsulta = sConsulta & "  SET @PENDIENTES=0" & vbCrLf
sConsulta = sConsulta & "  SET @PENDDEVOLUCION=0" & vbCrLf
sConsulta = sConsulta & "  SET @TRASLADADAS=0" & vbCrLf
sConsulta = sConsulta & " end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ESTADO='104' --Cerradas desde GS" & vbCrLf
sConsulta = sConsulta & " begin" & vbCrLf
sConsulta = sConsulta & "  SET @PENDIENTES=0" & vbCrLf
sConsulta = sConsulta & "  SET @PENDDEVOLUCION=0" & vbCrLf
sConsulta = sConsulta & "  SET @TRASLADADAS=0" & vbCrLf
sConsulta = sConsulta & " end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ESTADO='1000'  ---Pendientes" & vbCrLf
sConsulta = sConsulta & " begin" & vbCrLf
sConsulta = sConsulta & "  SET @TRASLADADAS=0" & vbCrLf
sConsulta = sConsulta & "  SET @OTRAS=0" & vbCrLf
sConsulta = sConsulta & "  SET @PARTICIPO=0" & vbCrLf
sConsulta = sConsulta & "  SET @ABIERTASPORUSTED=0  " & vbCrLf
sConsulta = sConsulta & " end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Primero obtenemos los identificadores de todas las instancias que cumplan las condiciones" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @HAYSELECT=0" & vbCrLf
sConsulta = sConsulta & "----B�SQUEDAS FILTRO: POR ID, T�TULO, IMPORTES,  PETICIONARIO, FECHA DE ALTA, TIPO, DEPARTAMENTO DEL PETICIONARIO, ESTADO Y ART�CULO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLWHERE = N' WHERE TS.TIPO NOT IN (2,3)'" & vbCrLf
sConsulta = sConsulta & "SET @SQLFROM = N' FROM INSTANCIA I WITH (NOLOCK) INNER JOIN SOLICITUD S WITH (NOLOCK) ON I.SOLICITUD=S.ID INNER JOIN TIPO_SOLICITUDES TS WITH (NOLOCK) ON S.TIPO=TS.ID'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ID <> 0 AND @ID IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "       SET @SQLWHERE = @SQLWHERE + ' AND I.ID=@ID'" & vbCrLf
sConsulta = sConsulta & "IF @TITULO IS NOT NULL AND @TITULO <> ''" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @SQLFROM = @SQLFROM + ' INNER JOIN INSTANCIA_VISORES_PM C1 WITH(NOLOCK) ON I.ID=C1.INSTANCIA'" & vbCrLf
sConsulta = sConsulta & "   SET @SQLWHERE = @SQLWHERE + ' AND (C1.TITULO_TEXT LIKE ''%' + @TITULO + '%'' OR CONVERT(VARCHAR,TITULO_FEC) LIKE ''%' + @TITULO + '%'')'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "IF @FECHADESDE IS NOT NULL AND @FECHADESDE<>''" & vbCrLf
sConsulta = sConsulta & "       SET @SQLWHERE = @SQLWHERE + ' AND I.FEC_ALTA>=CONVERT(datetime,@FECHADESDE,@TIPOFECHA)'" & vbCrLf
sConsulta = sConsulta & "IF @IMPORTEDESDE IS NOT NULL AND @IMPORTEDESDE<>0" & vbCrLf
sConsulta = sConsulta & "   SET @SQLWHERE = @SQLWHERE + ' AND I.IMPORTE>=@IMPORTEDESDE'" & vbCrLf
sConsulta = sConsulta & "IF @IMPORTEHASTA IS NOT NULL AND @IMPORTEHASTA<>0" & vbCrLf
sConsulta = sConsulta & "       SET @SQLWHERE = @SQLWHERE + ' AND I.IMPORTE<=@IMPORTEHASTA'" & vbCrLf
sConsulta = sConsulta & "IF @TIPO <> 0 AND @TIPO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "       SET @SQLWHERE = @SQLWHERE + ' AND I.SOLICITUD=@TIPO'" & vbCrLf
sConsulta = sConsulta & "IF @ORIGEN IS NOT NULL AND @ORIGEN <> ''" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @SQLFROM = @SQLFROM + ' INNER JOIN PER P WITH(NOLOCK) ON P.COD = I.PETICIONARIO'" & vbCrLf
sConsulta = sConsulta & "   SET @SQLFROM = @SQLFROM + ' INNER JOIN DEP WITH (NOLOCK) ON P.DEP=DEP.COD'" & vbCrLf
sConsulta = sConsulta & "   SET @SQLWHERE = @SQLWHERE + ' AND DEP.COD=@ORIGEN'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "IF @PETICIONARIO IS NOT NULL AND @PETICIONARIO <> ''" & vbCrLf
sConsulta = sConsulta & "   SET @SQLWHERE = @SQLWHERE + ' AND I.PETICIONARIO=@PETICIONARIO'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF (@ID = 0 OR @ID IS NULL) AND @BUSCARID <> 0 AND @BUSCARID IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "       SET @SQLWHERE = @SQLWHERE + ' AND I.ID=@BUSCARID'" & vbCrLf
sConsulta = sConsulta & "IF @BUSCARTITULO IS NOT NULL AND @BUSCARTITULO <> ''" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   IF @TITULO IS NULL OR @TITULO = ''" & vbCrLf
sConsulta = sConsulta & "       SET @SQLFROM = @SQLFROM + ' INNER JOIN INSTANCIA_VISORES_PM C1 WITH(NOLOCK) ON I.ID=C1.INSTANCIA'" & vbCrLf
sConsulta = sConsulta & "   SET @SQLWHERE = @SQLWHERE + ' AND (C1.TITULO_TEXT LIKE ''%' + @BUSCARTITULO+ '%'' OR CONVERT(VARCHAR,TITULO_FEC) LIKE ''%' + @BUSCARTITULO + '%'')'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "IF @BUSCARFECHADESDE IS NOT NULL AND @BUSCARFECHADESDE<>''" & vbCrLf
sConsulta = sConsulta & "       SET @SQLWHERE = @SQLWHERE + ' AND I.FEC_ALTA>=CONVERT(datetime,@BUSCARFECHADESDE,@TIPOFECHA)'" & vbCrLf
sConsulta = sConsulta & "IF @BUSCARIMPORTEDESDE IS NOT NULL AND @BUSCARIMPORTEDESDE<>0" & vbCrLf
sConsulta = sConsulta & "   SET @SQLWHERE = @SQLWHERE + ' AND I.IMPORTE>=@BUSCARIMPORTEDESDE'" & vbCrLf
sConsulta = sConsulta & "IF @BUSCARIMPORTEHASTA IS NOT NULL AND @BUSCARIMPORTEHASTA<>0" & vbCrLf
sConsulta = sConsulta & "       SET @SQLWHERE = @SQLWHERE + ' AND I.IMPORTE<=@BUSCARIMPORTEHASTA'" & vbCrLf
sConsulta = sConsulta & "IF (@TIPO = 0 OR @TIPO IS NULL) AND @BUSCARTIPO <> 0 AND @BUSCARTIPO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "       SET @SQLWHERE = @SQLWHERE + ' AND I.SOLICITUD=@BUSCARTIPO'" & vbCrLf
sConsulta = sConsulta & "IF  (@ORIGEN IS NULL OR @ORIGEN = '') AND  @BUSCARORIGEN IS NOT NULL AND @BUSCARORIGEN <> ''" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @SQLFROM = @SQLFROM + ' INNER JOIN PER P WITH(NOLOCK) ON P.COD = I.PETICIONARIO'" & vbCrLf
sConsulta = sConsulta & "   SET @SQLFROM = @SQLFROM + ' INNER JOIN DEP WITH (NOLOCK) ON P.DEP=DEP.COD'  " & vbCrLf
sConsulta = sConsulta & "   SET @SQLWHERE = @SQLWHERE + ' AND DEP.COD=@BUSCARORIGEN'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF (@PETICIONARIO IS NULL OR @PETICIONARIO = '') AND @BUSCARPETICIONARIO IS NOT NULL AND @BUSCARPETICIONARIO <> ''" & vbCrLf
sConsulta = sConsulta & "   SET @SQLWHERE = @SQLWHERE + ' AND I.PETICIONARIO=@BUSCARPETICIONARIO'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ESTADO IS NOT NULL AND @ESTADO<>''" & vbCrLf
sConsulta = sConsulta & "  begin" & vbCrLf
sConsulta = sConsulta & "   IF @ESTADO='1000'" & vbCrLf
sConsulta = sConsulta & "       SET @SQLWHERE = @SQLWHERE +  ' AND I.ESTADO =2' --pendientes de aprobar." & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "       SET @SQLWHERE = @SQLWHERE +  ' AND I.ESTADO IN(' + @ESTADO + ')'   ---guardadas, en curso, rechazadas, etc" & vbCrLf
sConsulta = sConsulta & "  end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @NOM_TABLA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "    begin " & vbCrLf
sConsulta = sConsulta & "   SET @SQLFROM = @SQLFROM + ' INNER JOIN ' + @IDI + '_' + @NOM_TABLA + ' F WITH (NOLOCK) ON I.ID = F.INSTANCIA'" & vbCrLf
sConsulta = sConsulta & "    end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--crea la tabla temporal" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE #G1 (ID INT, ICONO INT, OBV INT, ACCION_APROBAR INT, ACCION_RECHAZAR INT) " & vbCrLf
sConsulta = sConsulta & "SET @SQL = ''" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--1. Solicitudes abiertas por usted. " & vbCrLf
sConsulta = sConsulta & "IF @ABIERTASPORUSTED=1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "  SET @SQL=@SQL + ' SELECT DISTINCT I.ID,0 as ICONO, 0 as OBV, 0 as ACCION_APROBAR, 0 as ACCION_RECHAZAR ' + @SQLFROM + @SQLWHERE" & vbCrLf
sConsulta = sConsulta & "  SET @SQL=@SQL + ' AND I.NUM IS NOT NULL AND I.PETICIONARIO = @USU AND I.EN_PROCESO<>2'" & vbCrLf
sConsulta = sConsulta & "  IF @PENDIENTES=1 --las abiertas por usted que no est�n pendientes de aprobar por usted:" & vbCrLf
sConsulta = sConsulta & "   begin" & vbCrLf
sConsulta = sConsulta & "      SET @SQL=@SQL + ' AND (I.ESTADO<>2 OR (I.ESTADO=2 AND NOT EXISTS (SELECT IB2.INSTANCIA FROM INSTANCIA_BLOQUE IB2 WITH (NOLOCK) INNER JOIN PM_COPIA_ROL_BLOQUE RB2 WITH (NOLOCK) ON IB2.BLOQUE=RB2.BLOQUE INNER JOIN PM_COPIA_ROL R2 WITH (NOLOCK) ON RB2.ROL=R2.ID' " & vbCrLf
sConsulta = sConsulta & "      SET @SQL=@SQL+ ' WHERE (R2.PER=@USU OR R2.PER IN (SELECT U.PER FROM USU U WITH (NOLOCK) WHERE U.FSWS_SUSTITUCION=@USU))  AND I.ID=IB2.INSTANCIA AND IB2.ESTADO=1 AND IB2.BLOQ<>1)))'" & vbCrLf
sConsulta = sConsulta & "   end" & vbCrLf
sConsulta = sConsulta & "  IF @PENDDEVOLUCION=1 --que no saque las abiertas por usted y que tiene pendientes de devolver" & vbCrLf
sConsulta = sConsulta & "   begin" & vbCrLf
sConsulta = sConsulta & "      SET @SQL=@SQL + ' AND (I.ESTADO<>2 OR (I.ESTADO=2 AND NOT EXISTS (SELECT IB2.INSTANCIA FROM INSTANCIA_BLOQUE IB2 WITH (NOLOCK) WHERE I.ID=IB2.INSTANCIA AND IB2.ESTADO=1 AND IB2.TRASLADADA=1 AND IB2.BLOQ=0' " & vbCrLf
sConsulta = sConsulta & "      SET @SQL=@SQL + ' AND (IB2.TRASLADO_A=@USU OR IB2.TRASLADO_A IN (SELECT U.PER FROM USU U WITH (NOLOCK) WHERE U.FSWS_SUSTITUCION=@USU)))))'" & vbCrLf
sConsulta = sConsulta & "   end" & vbCrLf
sConsulta = sConsulta & " IF @OTRAS=1" & vbCrLf
sConsulta = sConsulta & "     SET @SQL=@SQL + ' AND NOT EXISTS (SELECT * FROM PM_ROL PR WITH (NOLOCK) WHERE PR.WORKFLOW=S.WORKFLOW AND PR.TIPO=5 AND PR.PER=@USU)'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  SET @HAYSELECT=1" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--2. Procesos en los que particip�" & vbCrLf
sConsulta = sConsulta & "IF @PARTICIPO=1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "  IF @HAYSELECT=1 " & vbCrLf
sConsulta = sConsulta & "     BEGIN" & vbCrLf
sConsulta = sConsulta & "       IF LEN(@SQL)>2000" & vbCrLf
sConsulta = sConsulta & "          begin" & vbCrLf
sConsulta = sConsulta & "             SET @SQL='INSERT INTO #G1 (ID,ICONO,OBV,ACCION_APROBAR,ACCION_RECHAZAR)' + @SQL" & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL, N'@IDI NVARCHAR(20),@USU NVARCHAR(50),@ID AS INT,@TITULO NVARCHAR(400),@TIPO INT,@FECHADESDE DATETIME,@TIPOFECHA INT,@ORIGEN NVARCHAR(50),@IMPORTEDESDE FLOAT,@IMPORTEHASTA FLOAT,@PETICIONARIO NVARCHAR(50),@BUSCARID AS INT,@BUSCARTITULO NVARCHAR(400),@BUSCARTIPO INT,@BUSCARFECHADESDE DATETIME,@BUSCARORIGEN NVARCHAR(50),@BUSCARIMPORTEDESDE FLOAT,@BUSCARIMPORTEHASTA FLOAT,@BUSCARPETICIONARIO NVARCHAR(50)', @IDI=@IDI,@USU=@USU,@ID=@ID,@TITULO=@TITULO,@TIPO=@TIPO,@FECHADESDE=@FECHADESDE,@TIPOFECHA=@TIPOFECHA,@ORIGEN=@ORIGEN,@IMPORTEDESDE=@IMPORTEDESDE,@IMPORTEHASTA=@IMPORTEHASTA,@PETICIONARIO=@PETICIONARIO,@BUSCARID=@BUSCARID,@BUSCARTITULO=@BUSCARTITULO,@BUSCARTIPO=@BUSCARTIPO,@BUSCARFECHADESDE=@BUSCARFECHADESDE,@BUSCARORIGEN=@BUSCARORIGEN,@BUSCARIMPORTEDESDE=@BUSCARIMPORTEDESDE,@BUSCARIMPORTEHASTA=@BUSCARIMPORTEHASTA,@BUSCARPETICIONARIO=@BUSCARPETICIONARIO" & vbCrLf
sConsulta = sConsulta & "             SET @TROCEAR=1" & vbCrLf
sConsulta = sConsulta & "             SET @SQL=''" & vbCrLf
sConsulta = sConsulta & "           end" & vbCrLf
sConsulta = sConsulta & "       ELSE" & vbCrLf
sConsulta = sConsulta & "           SET @SQL=@SQL + ' UNION'" & vbCrLf
sConsulta = sConsulta & "     END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  SET @SQL=@SQL + ' SELECT DISTINCT I.ID,0 as ICONO, 0 as OBV, 0 as ACCION_APROBAR, 0 as ACCION_RECHAZAR ' + @SQLFROM + ' INNER JOIN INSTANCIA_EST WITH (NOLOCK) ON I.ID=INSTANCIA_EST.INSTANCIA AND INSTANCIA_EST.PER=@USU'" & vbCrLf
sConsulta = sConsulta & "  SET @SQL = @SQL + @SQLWHERE + ' AND I.NUM IS NOT NULL AND  (INSTANCIA_EST.ROL IS NOT NULL OR (INSTANCIA_EST.ROL IS  NULL AND (INSTANCIA_EST.ACCION=3 OR INSTANCIA_EST.ACCION=4)))'" & vbCrLf
sConsulta = sConsulta & "  IF @ABIERTASPORUSTED=1" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL + ' AND I.PETICIONARIO<>@USU'" & vbCrLf
sConsulta = sConsulta & "  IF @OTRAS=1" & vbCrLf
sConsulta = sConsulta & "     SET @SQL=@SQL + ' AND NOT EXISTS (SELECT * FROM PM_ROL PR WITH (NOLOCK) WHERE PR.WORKFLOW=S.WORKFLOW AND PR.TIPO=5 AND PR.PER=@USU)'" & vbCrLf
sConsulta = sConsulta & "  IF @PENDIENTES=1" & vbCrLf
sConsulta = sConsulta & "    begin" & vbCrLf
sConsulta = sConsulta & "          SET @SQL=@SQL + ' AND NOT EXISTS (SELECT IB.INSTANCIA FROM INSTANCIA_BLOQUE IB WITH (NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "          SET @SQL=@SQL + ' INNER JOIN PM_COPIA_ROL_BLOQUE RB WITH (NOLOCK)  ON RB.BLOQUE=IB.BLOQUE INNER JOIN PM_COPIA_ROL R WITH (NOLOCK)  ON RB.ROL=R.ID AND R.TIPO<>5 AND I.EN_PROCESO=0 AND I.TRASLADADA=0 AND I.ESTADO=2 AND (R.PER=@USU OR R.PER IN ( SELECT U.PER FROM USU U WITH (NOLOCK) WHERE U.FSWS_SUSTITUCION=@USU))'" & vbCrLf
sConsulta = sConsulta & "          SET @SQL=@SQL + ' WHERE I.ID=IB.INSTANCIA AND IB.EN_PROCESO=0 AND IB. ESTADO=1 AND IB.BLOQ<>1)'" & vbCrLf
sConsulta = sConsulta & "    end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  SET @HAYSELECT=1" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--3. Pendientes de tratar" & vbCrLf
sConsulta = sConsulta & "IF @PENDIENTES=1 AND (@ESTADO='1000' OR @ESTADO='' OR @ESTADO IS NULL)" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   IF @HAYSELECT=1 " & vbCrLf
sConsulta = sConsulta & "     BEGIN" & vbCrLf
sConsulta = sConsulta & "       IF LEN(@SQL)>2000" & vbCrLf
sConsulta = sConsulta & "          begin" & vbCrLf
sConsulta = sConsulta & "             SET @SQL='INSERT INTO #G1 (ID,ICONO,OBV,ACCION_APROBAR,ACCION_RECHAZAR)' + @SQL" & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL, N'@IDI NVARCHAR(20),@USU NVARCHAR(50),@ID AS INT,@TITULO NVARCHAR(400),@TIPO INT,@FECHADESDE DATETIME,@TIPOFECHA INT,@ORIGEN NVARCHAR(50),@IMPORTEDESDE FLOAT,@IMPORTEHASTA FLOAT,@PETICIONARIO NVARCHAR(50),@BUSCARID AS INT,@BUSCARTITULO NVARCHAR(400),@BUSCARTIPO INT,@BUSCARFECHADESDE DATETIME,@BUSCARORIGEN NVARCHAR(50),@BUSCARIMPORTEDESDE FLOAT,@BUSCARIMPORTEHASTA FLOAT,@BUSCARPETICIONARIO NVARCHAR(50)', @IDI=@IDI,@USU=@USU,@ID=@ID,@TITULO=@TITULO,@TIPO=@TIPO,@FECHADESDE=@FECHADESDE,@TIPOFECHA=@TIPOFECHA,@ORIGEN=@ORIGEN,@IMPORTEDESDE=@IMPORTEDESDE,@IMPORTEHASTA=@IMPORTEHASTA,@PETICIONARIO=@PETICIONARIO,@BUSCARID=@BUSCARID,@BUSCARTITULO=@BUSCARTITULO,@BUSCARTIPO=@BUSCARTIPO,@BUSCARFECHADESDE=@BUSCARFECHADESDE,@BUSCARORIGEN=@BUSCARORIGEN,@BUSCARIMPORTEDESDE=@BUSCARIMPORTEDESDE,@BUSCARIMPORTEHASTA=@BUSCARIMPORTEHASTA,@BUSCARPETICIONARIO=@BUSCARPETICIONARIO" & vbCrLf
sConsulta = sConsulta & "             SET @TROCEAR=1" & vbCrLf
sConsulta = sConsulta & "             SET @SQL=''" & vbCrLf
sConsulta = sConsulta & "           end" & vbCrLf
sConsulta = sConsulta & "       ELSE" & vbCrLf
sConsulta = sConsulta & "           SET @SQL=@SQL + ' UNION'" & vbCrLf
sConsulta = sConsulta & "     END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  SET @SQL=@SQL + ' SELECT DISTINCT I.ID,1 as ICONO, 0 as OBV,case when R.PER=@USU THEN isnull(RAA.ACCION,0) ELSE 0 END AS ACCION_APROBAR,case when R.PER=@USU THEN isnull(RAR.ACCION,0) ELSE 0 END AS ACCION_RECHAZAR ' + @SQLFROM + ' INNER JOIN INSTANCIA_BLOQUE IB WITH (NOLOCK) ON I.ID=IB.INSTANCIA AND IB.EN_PROCESO=0 AND IB. ESTADO=1 AND IB.BLOQ<>1' " & vbCrLf
sConsulta = sConsulta & "  SET @SQL=@SQL + ' INNER JOIN PM_COPIA_ROL_BLOQUE RB WITH (NOLOCK) ON RB.BLOQUE=IB.BLOQUE AND IB.ESTADO=1 AND IB.BLOQ<>1 AND IB.TRASLADADA<>1'" & vbCrLf
sConsulta = sConsulta & "  SET @SQL=@SQL + ' INNER JOIN PM_COPIA_ROL R WITH (NOLOCK)  ON RB.ROL=R.ID AND R.TIPO<>5'" & vbCrLf
sConsulta = sConsulta & "  SET @SQL=@SQL + ' LEFT JOIN PM_COPIA_ROL_ACCION RAA WITH(NOLOCK)ON RAA.ROL=R.ID AND RAA.BLOQUE=RB.BLOQUE AND RAA.APROBAR=1'   --acciones del rol actual (Aprobar)" & vbCrLf
sConsulta = sConsulta & "  SET @SQL=@SQL + ' LEFT JOIN PM_COPIA_ROL_ACCION RAR WITH(NOLOCK)ON RAR.ROL=R.ID AND RAR.BLOQUE=RB.BLOQUE AND RAR.RECHAZAR=1'   --acciones del rol actual (Rechazar)" & vbCrLf
sConsulta = sConsulta & "  SET @SQL = @SQL + @SQLWHERE + ' AND I.NUM IS NOT NULL AND I.EN_PROCESO=0 AND I.TRASLADADA=0 AND I.ESTADO=2 AND ( (R.PER=@USU AND (SELECT ISNULL(U.FSWS_SUSTITUCION,'''') FROM USU U WITH (NOLOCK) WHERE U.PER=@USU AND U.BAJA=0) = '''')  OR R.PER IN ( SELECT U.PER FROM USU U WITH (NOLOCK) WHERE U.FSWS_SUSTITUCION=@USU))'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  --Las de una lista de autoasignacion" & vbCrLf
sConsulta = sConsulta & "  IF LEN(@SQL)>2000" & vbCrLf
sConsulta = sConsulta & "    begin" & vbCrLf
sConsulta = sConsulta & "        SET @SQL='INSERT INTO #G1 (ID,ICONO,OBV,ACCION_APROBAR,ACCION_RECHAZAR)' + @SQL" & vbCrLf
sConsulta = sConsulta & "        EXEC SP_EXECUTESQL @SQL, N'@IDI NVARCHAR(20),@USU NVARCHAR(50),@ID AS INT,@TITULO NVARCHAR(400),@TIPO INT,@FECHADESDE DATETIME,@TIPOFECHA INT,@ORIGEN NVARCHAR(50),@IMPORTEDESDE FLOAT,@IMPORTEHASTA FLOAT,@PETICIONARIO NVARCHAR(50),@BUSCARID AS INT,@BUSCARTITULO NVARCHAR(400),@BUSCARTIPO INT,@BUSCARFECHADESDE DATETIME,@BUSCARORIGEN NVARCHAR(50),@BUSCARIMPORTEDESDE FLOAT,@BUSCARIMPORTEHASTA FLOAT,@BUSCARPETICIONARIO NVARCHAR(50)', @IDI=@IDI,@USU=@USU,@ID=@ID,@TITULO=@TITULO,@TIPO=@TIPO,@FECHADESDE=@FECHADESDE,@TIPOFECHA=@TIPOFECHA,@ORIGEN=@ORIGEN,@IMPORTEDESDE=@IMPORTEDESDE,@IMPORTEHASTA=@IMPORTEHASTA,@PETICIONARIO=@PETICIONARIO,@BUSCARID=@BUSCARID,@BUSCARTITULO=@BUSCARTITULO,@BUSCARTIPO=@BUSCARTIPO,@BUSCARFECHADESDE=@BUSCARFECHADESDE,@BUSCARORIGEN=@BUSCARORIGEN,@BUSCARIMPORTEDESDE=@BUSCARIMPORTEDESDE,@BUSCARIMPORTEHASTA=@BUSCARIMPORTEHASTA,@BUSCARPETICIONARIO=@BUSCARPETICIONARIO" & vbCrLf
sConsulta = sConsulta & "        SET @TROCEAR=1" & vbCrLf
sConsulta = sConsulta & "        SET @SQL=''" & vbCrLf
sConsulta = sConsulta & "    end" & vbCrLf
sConsulta = sConsulta & "  ELSE" & vbCrLf
sConsulta = sConsulta & "     SET @SQL=@SQL + ' UNION'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  SET @SQL=@SQL + ' SELECT DISTINCT I.ID,1 as ICONO, 0 as OBV,case when PT.PER=@USU THEN isnull(RAA.ACCION,0) ELSE 0 END AS ACCION_APROBAR,case when PT.PER=@USU THEN isnull(RAR.ACCION,0) ELSE 0 END AS ACCION_RECHAZAR' + @SQLFROM + ' INNER JOIN INSTANCIA_BLOQUE IB WITH (NOLOCK) ON I.ID=IB.INSTANCIA AND IB.EN_PROCESO=0 AND IB.ESTADO=1 AND IB.BLOQ<>1 AND IB.TRASLADADA<>1' " & vbCrLf
sConsulta = sConsulta & "  SET @SQL=@SQL + ' INNER JOIN PM_COPIA_ROL_BLOQUE RB WITH (NOLOCK)  ON RB.BLOQUE=IB.BLOQUE'" & vbCrLf
sConsulta = sConsulta & "  SET @SQL=@SQL + ' INNER JOIN PM_COPIA_ROL R WITH (NOLOCK)  ON RB.ROL=R.ID AND R.PER IS NULL AND R.COMO_ASIGNAR=3  AND R.TIPO<>5'" & vbCrLf
sConsulta = sConsulta & "  SET @SQL=@SQL+ '  INNER JOIN PM_COPIA_PARTICIPANTES PT WITH (NOLOCK)  ON PT.ROL=R.ID'" & vbCrLf
sConsulta = sConsulta & "  SET @SQL=@SQL + ' LEFT JOIN PM_COPIA_ROL_ACCION RAA WITH(NOLOCK)ON RAA.ROL=R.ID AND RAA.BLOQUE=RB.BLOQUE AND RAA.APROBAR=1'   --acciones del rol actual (Aprobar)" & vbCrLf
sConsulta = sConsulta & "  SET @SQL=@SQL + ' LEFT JOIN PM_COPIA_ROL_ACCION RAR WITH(NOLOCK)ON RAR.ROL=R.ID AND RAR.BLOQUE=RB.BLOQUE AND RAR.RECHAZAR=1'   --acciones del rol actual (Rechazar)" & vbCrLf
sConsulta = sConsulta & "  SET @SQL=@SQL + @SQLWHERE + ' AND I.NUM IS NOT NULL AND I.EN_PROCESO=0 AND I.TRASLADADA=0 AND I.ESTADO=2 AND ( (PT.PER=@USU AND (SELECT ISNULL(U.FSWS_SUSTITUCION,'''') FROM USU U WITH (NOLOCK) WHERE U.PER=@USU AND U.BAJA=0) = '''') OR PT.PER IN ( SELECT U.PER FROM USU U WITH (NOLOCK) WHERE U.FSWS_SUSTITUCION=@USU))'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  SET @HAYSELECT=1" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--4. Pendientes de devoluci�n" & vbCrLf
sConsulta = sConsulta & "IF @PENDDEVOLUCION=1 AND (@ESTADO='1000' OR @ESTADO='' OR @ESTADO IS NULL)" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "    IF @HAYSELECT=1 " & vbCrLf
sConsulta = sConsulta & "     BEGIN" & vbCrLf
sConsulta = sConsulta & "       IF LEN(@SQL)>2000" & vbCrLf
sConsulta = sConsulta & "           begin" & vbCrLf
sConsulta = sConsulta & "             SET @SQL='INSERT INTO #G1 (ID,ICONO,OBV,ACCION_APROBAR,ACCION_RECHAZAR)' + @SQL" & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL, N'@IDI NVARCHAR(20),@USU NVARCHAR(50),@ID AS INT,@TITULO NVARCHAR(400),@TIPO INT,@FECHADESDE DATETIME,@TIPOFECHA INT,@ORIGEN NVARCHAR(50),@IMPORTEDESDE FLOAT,@IMPORTEHASTA FLOAT,@PETICIONARIO NVARCHAR(50),@BUSCARID AS INT,@BUSCARTITULO NVARCHAR(400),@BUSCARTIPO INT,@BUSCARFECHADESDE DATETIME,@BUSCARORIGEN NVARCHAR(50),@BUSCARIMPORTEDESDE FLOAT,@BUSCARIMPORTEHASTA FLOAT,@BUSCARPETICIONARIO NVARCHAR(50)', @IDI=@IDI,@USU=@USU,@ID=@ID,@TITULO=@TITULO,@TIPO=@TIPO,@FECHADESDE=@FECHADESDE,@TIPOFECHA=@TIPOFECHA,@ORIGEN=@ORIGEN,@IMPORTEDESDE=@IMPORTEDESDE,@IMPORTEHASTA=@IMPORTEHASTA,@PETICIONARIO=@PETICIONARIO,@BUSCARID=@BUSCARID,@BUSCARTITULO=@BUSCARTITULO,@BUSCARTIPO=@BUSCARTIPO,@BUSCARFECHADESDE=@BUSCARFECHADESDE,@BUSCARORIGEN=@BUSCARORIGEN,@BUSCARIMPORTEDESDE=@BUSCARIMPORTEDESDE,@BUSCARIMPORTEHASTA=@BUSCARIMPORTEHASTA,@BUSCARPETICIONARIO=@BUSCARPETICIONARIO" & vbCrLf
sConsulta = sConsulta & "             SET @TROCEAR=1" & vbCrLf
sConsulta = sConsulta & "             SET @SQL=''" & vbCrLf
sConsulta = sConsulta & "           end" & vbCrLf
sConsulta = sConsulta & "       ELSE" & vbCrLf
sConsulta = sConsulta & "           SET @SQL=@SQL + ' UNION'" & vbCrLf
sConsulta = sConsulta & "     END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    SET @SQL=@SQL + ' SELECT DISTINCT I.ID,1 as ICONO, 0 as OBV, 0 as ACCION_APROBAR, 0 as ACCION_RECHAZAR ' + @SQLFROM + ' INNER JOIN INSTANCIA_BLOQUE IB WITH (NOLOCK) ON I.ID=IB.INSTANCIA AND IB.EN_PROCESO=0 AND IB. ESTADO=1 AND IB.BLOQ<>1 AND IB.TRASLADADA=1' " & vbCrLf
sConsulta = sConsulta & "    SET @SQL = @SQL + @SQLWHERE + ' AND I.NUM IS NOT NULL AND I.TRASLADADA=1 AND (TRASLADO_A=@USU AND (SELECT ISNULL(U.FSWS_SUSTITUCION,'''') FROM USU U WITH (NOLOCK) WHERE U.PER=@USU AND U.BAJA=0) = '''') OR (TRASLADO_A IN (SELECT U.PER FROM USU U WITH (NOLOCK) WHERE U.FSWS_SUSTITUCION=@USU))'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  SET @HAYSELECT=1" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--5-Solicitudes trasladadas en espera de devoluci�n" & vbCrLf
sConsulta = sConsulta & "IF @TRASLADADAS =1 AND (@ESTADO='2' OR @ESTADO='' OR @ESTADO IS NULL)" & vbCrLf
sConsulta = sConsulta & "BEGIN " & vbCrLf
sConsulta = sConsulta & "     IF @HAYSELECT=1 " & vbCrLf
sConsulta = sConsulta & "     BEGIN" & vbCrLf
sConsulta = sConsulta & "       IF LEN(@SQL)>2000" & vbCrLf
sConsulta = sConsulta & "           begin" & vbCrLf
sConsulta = sConsulta & "             SET @SQL='INSERT INTO #G1 (ID,ICONO,OBV,ACCION_APROBAR,ACCION_RECHAZAR)' + @SQL" & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL, N'@IDI NVARCHAR(20),@USU NVARCHAR(50),@ID AS INT,@TITULO NVARCHAR(400),@TIPO INT,@FECHADESDE DATETIME,@TIPOFECHA INT,@ORIGEN NVARCHAR(50),@IMPORTEDESDE FLOAT,@IMPORTEHASTA FLOAT,@PETICIONARIO NVARCHAR(50),@BUSCARID AS INT,@BUSCARTITULO NVARCHAR(400),@BUSCARTIPO INT,@BUSCARFECHADESDE DATETIME,@BUSCARORIGEN NVARCHAR(50),@BUSCARIMPORTEDESDE FLOAT,@BUSCARIMPORTEHASTA FLOAT,@BUSCARPETICIONARIO NVARCHAR(50)', @IDI=@IDI,@USU=@USU,@ID=@ID,@TITULO=@TITULO,@TIPO=@TIPO,@FECHADESDE=@FECHADESDE,@TIPOFECHA=@TIPOFECHA,@ORIGEN=@ORIGEN,@IMPORTEDESDE=@IMPORTEDESDE,@IMPORTEHASTA=@IMPORTEHASTA,@PETICIONARIO=@PETICIONARIO,@BUSCARID=@BUSCARID,@BUSCARTITULO=@BUSCARTITULO,@BUSCARTIPO=@BUSCARTIPO,@BUSCARFECHADESDE=@BUSCARFECHADESDE,@BUSCARORIGEN=@BUSCARORIGEN,@BUSCARIMPORTEDESDE=@BUSCARIMPORTEDESDE,@BUSCARIMPORTEHASTA=@BUSCARIMPORTEHASTA,@BUSCARPETICIONARIO=@BUSCARPETICIONARIO" & vbCrLf
sConsulta = sConsulta & "             SET @TROCEAR=1" & vbCrLf
sConsulta = sConsulta & "             SET @SQL=''" & vbCrLf
sConsulta = sConsulta & "           end" & vbCrLf
sConsulta = sConsulta & "       ELSE" & vbCrLf
sConsulta = sConsulta & "           SET @SQL=@SQL + ' UNION'" & vbCrLf
sConsulta = sConsulta & "     END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    SET @SQL=@SQL +' SELECT DISTINCT I.ID,0 as ICONO, 0 as OBV, 0 as ACCION_APROBAR, 0 as ACCION_RECHAZAR' + @SQLFROM + ' INNER JOIN INSTANCIA_BLOQUE IB WITH (NOLOCK) ON IB.INSTANCIA=I.ID AND IB.ESTADO=1 AND IB.TRASLADADA=1 AND IB.BLOQ=0' " & vbCrLf
sConsulta = sConsulta & "    SET @SQL = @SQL + @SQLWHERE + '  AND I.NUM IS NOT NULL AND I.TRASLADADA=1 AND  (DEVOLUCION_A=@USU AND (SELECT ISNULL(U.FSWS_SUSTITUCION,'''') FROM USU U WITH (NOLOCK) WHERE U.PER=@USU AND U.BAJA=0) = '''') OR (DEVOLUCION_A IN ( SELECT U.PER FROM USU U WITH (NOLOCK) WHERE U.FSWS_SUSTITUCION=@USU))'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    SET @HAYSELECT=1" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--6-OTRAS SOLICITUDES (OBSERVADORES)" & vbCrLf
sConsulta = sConsulta & "IF @OTRAS=1" & vbCrLf
sConsulta = sConsulta & "BEGIN " & vbCrLf
sConsulta = sConsulta & "  --para los roles observadores" & vbCrLf
sConsulta = sConsulta & "   IF @HAYSELECT=1 " & vbCrLf
sConsulta = sConsulta & "     BEGIN" & vbCrLf
sConsulta = sConsulta & "       IF LEN(@SQL)>2000" & vbCrLf
sConsulta = sConsulta & "           begin" & vbCrLf
sConsulta = sConsulta & "             SET @SQL='INSERT INTO #G1 (ID,ICONO,OBV,ACCION_APROBAR,ACCION_RECHAZAR)' + @SQL" & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL, N'@IDI NVARCHAR(20),@USU NVARCHAR(50),@ID AS INT,@TITULO NVARCHAR(400),@TIPO INT,@FECHADESDE DATETIME,@TIPOFECHA INT,@ORIGEN NVARCHAR(50),@IMPORTEDESDE FLOAT,@IMPORTEHASTA FLOAT,@PETICIONARIO NVARCHAR(50),@BUSCARID AS INT,@BUSCARTITULO NVARCHAR(400),@BUSCARTIPO INT,@BUSCARFECHADESDE DATETIME,@BUSCARORIGEN NVARCHAR(50),@BUSCARIMPORTEDESDE FLOAT,@BUSCARIMPORTEHASTA FLOAT,@BUSCARPETICIONARIO NVARCHAR(50)', @IDI=@IDI,@USU=@USU,@ID=@ID,@TITULO=@TITULO,@TIPO=@TIPO,@FECHADESDE=@FECHADESDE,@TIPOFECHA=@TIPOFECHA,@ORIGEN=@ORIGEN,@IMPORTEDESDE=@IMPORTEDESDE,@IMPORTEHASTA=@IMPORTEHASTA,@PETICIONARIO=@PETICIONARIO,@BUSCARID=@BUSCARID,@BUSCARTITULO=@BUSCARTITULO,@BUSCARTIPO=@BUSCARTIPO,@BUSCARFECHADESDE=@BUSCARFECHADESDE,@BUSCARORIGEN=@BUSCARORIGEN,@BUSCARIMPORTEDESDE=@BUSCARIMPORTEDESDE,@BUSCARIMPORTEHASTA=@BUSCARIMPORTEHASTA,@BUSCARPETICIONARIO=@BUSCARPETICIONARIO" & vbCrLf
sConsulta = sConsulta & "             SET @TROCEAR=1" & vbCrLf
sConsulta = sConsulta & "             SET @SQL=''" & vbCrLf
sConsulta = sConsulta & "           end" & vbCrLf
sConsulta = sConsulta & "       ELSE" & vbCrLf
sConsulta = sConsulta & "           SET @SQL=@SQL + ' UNION'" & vbCrLf
sConsulta = sConsulta & "     END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    SET @SQL=@SQL +' SELECT DISTINCT I.ID,0 as ICONO, 1 as OBV, 0 as ACCION_APROBAR, 0 as ACCION_RECHAZAR' + @SQLFROM + ' INNER JOIN PM_ROL PR WITH (NOLOCK) ON PR.WORKFLOW=S.WORKFLOW' " & vbCrLf
sConsulta = sConsulta & "    SET @SQL = @SQL + @SQLWHERE + '  AND I.NUM IS NOT NULL AND PR.TIPO=5 AND PR.PER=@USU AND I.ESTADO>0 AND I.EN_PROCESO<>2'" & vbCrLf
sConsulta = sConsulta & "    SET @SQL = @SQL + ' AND NOT EXISTS (SELECT INSTANCIA.ID FROM INSTANCIA WITH (NOLOCK)'" & vbCrLf
sConsulta = sConsulta & "    SET @SQL = @SQL + ' INNER JOIN INSTANCIA_BLOQUE IB WITH (NOLOCK) ON INSTANCIA.ID=IB.INSTANCIA AND IB.EN_PROCESO=0 AND IB. ESTADO=1 AND IB.BLOQ<>1'" & vbCrLf
sConsulta = sConsulta & "    SET @SQL = @SQL + ' INNER JOIN PM_COPIA_ROL_BLOQUE RB WITH (NOLOCK) ON RB.BLOQUE=IB.BLOQUE AND IB.ESTADO=1 AND IB.BLOQ<>1 AND IB.TRASLADADA<>1'" & vbCrLf
sConsulta = sConsulta & "    SET @SQL = @SQL + ' INNER JOIN PM_COPIA_ROL R WITH (NOLOCK)  ON RB.ROL=R.ID AND R.TIPO<>5'" & vbCrLf
sConsulta = sConsulta & "    SET @SQL = @SQL + ' WHERE  I.ID=INSTANCIA.ID AND ( (R.PER=@USU AND (SELECT ISNULL(U.FSWS_SUSTITUCION,'''') FROM USU U WITH (NOLOCK) WHERE U.PER=@USU AND U.BAJA=0) = '''')'  " & vbCrLf
sConsulta = sConsulta & "    SET @SQL = @SQL + ' OR R.PER IN ( SELECT U.PER FROM USU U WITH (NOLOCK) WHERE U.FSWS_SUSTITUCION=@USU)))'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @SQL<>''" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & " SET @SQL='INSERT INTO #G1 (ID,ICONO,OBV,ACCION_APROBAR,ACCION_RECHAZAR)' + @SQL" & vbCrLf
sConsulta = sConsulta & " EXEC SP_EXECUTESQL @SQL, N'@USU VARCHAR(50), @ID AS INT,@TITULO VARCHAR(400),@TIPO INT,@FECHADESDE DATETIME,@TIPOFECHA INT,@ORIGEN VARCHAR(50),@IMPORTEDESDE FLOAT,@IMPORTEHASTA FLOAT,@PETICIONARIO VARCHAR(50),@BUSCARID AS INT,@BUSCARTITULO VARCHAR(400),@BUSCARTIPO INT,@BUSCARFECHADESDE DATETIME,@BUSCARORIGEN VARCHAR(50),@BUSCARIMPORTEDESDE FLOAT,@BUSCARIMPORTEHASTA FLOAT,@BUSCARPETICIONARIO VARCHAR(50)', @USU=@USU,@ID=@ID,@TITULO=@TITULO,@TIPO=@TIPO,@FECHADESDE=@FECHADESDE,@TIPOFECHA=@TIPOFECHA,@ORIGEN=@ORIGEN,@IMPORTEDESDE=@IMPORTEDESDE,@IMPORTEHASTA=@IMPORTEHASTA,@PETICIONARIO=@PETICIONARIO,@BUSCARID=@BUSCARID,@BUSCARTITULO=@BUSCARTITULO,@BUSCARTIPO=@BUSCARTIPO,@BUSCARFECHADESDE=@BUSCARFECHADESDE,@BUSCARORIGEN=@BUSCARORIGEN,@BUSCARIMPORTEDESDE=@BUSCARIMPORTEDESDE,@BUSCARIMPORTEHASTA=@BUSCARIMPORTEHASTA,@BUSCARPETICIONARIO=@BUSCARPETICIONARIO" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "----********************************************************************************************************************************************************************************************************" & vbCrLf
sConsulta = sConsulta & "SET @SQLFROMBUSQUEDA = N''" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--*************************************************SELECT GEN�RICA PARA SACAR LOS DATOS ***********************************************************" & vbCrLf
sConsulta = sConsulta & "--Creamos una tabla temporal" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE #G2 (TIPO VARCHAR(50), DEN VARCHAR(500) , ID INT, FECHA DATETIME, ESTADO INT, ETAPA_ACTUAL VARCHAR(100), ETAPA INT, EN_PROCESO TINYINT, VER_DETALLE_PER TINYINT, IMPORTE FLOAT,MON VARCHAR(10), PETICIONARIO VARCHAR(50), PET VARCHAR(150),VER_FLUJO TINYINT, KO TINYINT, ERROR VARCHAR(500)," & vbCrLf
sConsulta = sConsulta & "EST_VALIDACION INT, ERROR_VALIDACION VARCHAR(500), FECHA_VALIDACION DATETIME, BLOQUE INT, USUARIO VARCHAR(50), USU VARCHAR(100), DEP VARCHAR(100), PROVEEDOR VARCHAR(100), NUM_REEMISIONES INT, TIPO_SOLIC INT,TRASLADADA TINYINT, ULT_VERSION INT, ICONO TINYINT, OBSERVADOR TINYINT, ACCION_APROBAR INT,ACCION_RECHAZAR INT)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'SELECT DISTINCT S.COD as TIPO, CASE WHEN C1.TITULO_FEC IS NULL THEN C1.TITULO_TEXT ELSE CONVERT(VARCHAR ,C1.TITULO_FEC) END AS DEN, '" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'I.ID AS ID,I.FEC_ALTA AS FECHA, I.ESTADO'" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ', CASE WHEN M.P>1 THEN ''##'' ELSE ISNULL(CB.DEN,'' '') END AS ETAPA_ACTUAL, I.ESTADO AS ETAPA,I.EN_PROCESO' " & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ',CR.VDP,'  --ver detalle de persona" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'I.IMPORTE,I.MON,I.PETICIONARIO,P.NOM+'' ''+P.APE AS PET'" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ',CR.VER_FLUJO,INT.KO,INT.ERROR, isnull(I.EST_VALIDACION,0) EST_VALIDACION,I.ERROR_VALIDACION,I.FECHA_VALIDACION '" & vbCrLf
sConsulta = sConsulta & "--SET @SQL = @SQL + ', CASE WHEN M.P>1 THEN NULL ELSE M.BLOQUE END as BLOQUE,'" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ', M.BLOQUE  as BLOQUE,'" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ' CASE WHEN I.ESTADO=2 THEN CASE WHEN R5.PER IS NOT NULL THEN R5.PER END ELSE CASE WHEN IE.PER IS NULL THEN I.PETICIONARIO ELSE IE.PER END END AS USUARIO'" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ', CASE WHEN I.ESTADO=2 THEN CASE WHEN R5.PER IS NOT NULL THEN P3.NOM  + '' '' + P3.APE ELSE PV.DEN END ELSE CASE WHEN IE.PER IS NULL THEN P.NOM  +'' '' + P.APE ELSE P2.NOM  + '' '' + P2.APE END END AS USU'" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ', CASE WHEN I.ESTADO=2 THEN CASE WHEN R5.PER IS NOT NULL THEN D3.DEN ELSE NULL END ELSE CASE WHEN IE.PER IS NULL THEN D.DEN ELSE D2.DEN END END AS DEP'" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ', CASE WHEN I.ESTADO=2 THEN PV.COD ELSE NULL END AS PROVEEDOR'" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ',  I.NUM_REEMISIONES,TS.TIPO AS TIPO_SOLIC,I.TRASLADADA,I.ULT_VERSION'" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ', G1.ICONO,OBV,ACCION_APROBAR,ACCION_RECHAZAR'" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL +  ' FROM INSTANCIA I WITH (NOLOCK) ' " & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL +  ' INNER JOIN #G1 G1 ON I.ID=G1.ID'" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL +  ' INNER JOIN SOLICITUD S WITH (NOLOCK) ON I.SOLICITUD=S.ID'   --para sacar la solicitud" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL +  ' INNER JOIN TIPO_SOLICITUDES TS WITH (NOLOCK) ON S.TIPO=TS.ID'   --para sacar el tipo de solicitud" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL +  ' LEFT JOIN INSTANCIA_VISORES_PM C1 WITH (NOLOCK) ON I.ID=C1.INSTANCIA '   --para mostrar el t�tulo" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL +  ' LEFT JOIN INSTANCIA_EST IE WITH(NOLOCK)ON I.INSTANCIA_EST=IE.ID' --saca la �ltima acci�n realizada (fecha y usuario)" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL +  ' INNER JOIN PER P WITH (NOLOCK) ON P.COD = I.PETICIONARIO '  --para el nombre del peticionario" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL +  ' INNER JOIN DEP D WITH (NOLOCK) ON P.DEP=D.COD '  --para el departamento del peticionario" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL +  ' LEFT JOIN PER P2 WITH(NOLOCK) ON IE.PER=P2.COD' --para el nombre del �ltimo aprobador" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL +  ' LEFT JOIN DEP D2 WITH(NOLOCK) ON P2.DEP=D2.COD' --para el departamento del �ltimo aprobador" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL +  ' LEFT JOIN INSTANCIA_INT INT ON I.ID=INT.ID'  --para el estado de integraci�n" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Mira si la persona es un rol de la instancia, y en ese caso si tiene permisos para ver el detalle del flujo y el detalle de los participantes:" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' LEFT JOIN(SELECT INSTANCIA I,PER P,MAX(VER_DETALLE_PER)VDP,MAX(VER_FLUJO)VER_FLUJO FROM PM_COPIA_ROL WITH(NOLOCK) GROUP BY INSTANCIA,PER) CR ON I.ID = CR.I AND @USU=CR.P'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Para obtener si hay etapas en paralelo" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ' INNER JOIN (SELECT INSTANCIA I, COUNT(*) P,MIN(BLOQUE) BLOQUE FROM INSTANCIA_BLOQUE  WITH (NOLOCK)  WHERE ESTADO=1 AND BLOQ<>1 GROUP BY INSTANCIA) M ON I.ID=M.I'  " & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ' LEFT JOIN PM_COPIA_BLOQUE_DEN CB ON M.BLOQUE=CB.BLOQUE AND CB.IDI= @IDI'  --denominaci�n del bloque actual." & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "/*" & vbCrLf
sConsulta = sConsulta & "--obtiene el rol actual " & vbCrLf
sConsulta = sConsulta & "SET @SQLFROM = @SQLFROM + ' LEFT JOIN(SELECT P.BLOQUE,MAX(CASE WHEN PB.ID IS NOT NULL THEN CASE WHEN INS.ID IS NOT NULL THEN  PR.ID ELSE -1 END ELSE CASE WHEN A.TIPO=5 THEN NULL ELSE P.ROL END END)ROL' " & vbCrLf
sConsulta = sConsulta & "SET @SQLFROM = @SQLFROM + ' FROM PM_COPIA_ROL_BLOQUE P WITH(NOLOCK)'" & vbCrLf
sConsulta = sConsulta & "SET @SQLFROM = @SQLFROM + ' LEFT JOIN PM_COPIA_BLOQUE PB WITH(NOLOCK) ON P.BLOQUE=PB.ID AND PB.TIPO=1' " & vbCrLf
sConsulta = sConsulta & "SET @SQLFROM = @SQLFROM + ' LEFT JOIN PM_COPIA_ROL PR WITH(NOLOCK)ON P.ROL=PR.ID AND PR.TIPO=4 AND PB.INSTANCIA=PR.INSTANCIA' " & vbCrLf
sConsulta = sConsulta & "SET @SQLFROM = @SQLFROM + ' LEFT JOIN INSTANCIA INS WITH(NOLOCK)ON INS.ID=PB.INSTANCIA AND INS.PETICIONARIO=PR.PER' " & vbCrLf
sConsulta = sConsulta & "SET @SQLFROM = @SQLFROM + ' LEFT JOIN PM_COPIA_ROL A WITH(NOLOCK)ON A.ID=P.ROL GROUP BY P.BLOQUE)RB ON RB.BLOQUE=M.BLOQUE'" & vbCrLf
sConsulta = sConsulta & "*/" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ' LEFT JOIN (SELECT P.BLOQUE, MIN(P.ROL) ROL FROM PM_COPIA_ROL_BLOQUE P WITH(NOLOCK)  GROUP BY P.BLOQUE) RB5 ON RB5.BLOQUE=M.BLOQUE '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ' LEFT JOIN PM_COPIA_ROL R5 WITH(NOLOCK)ON RB5.ROL=R5.ID' --Rol" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ' LEFT JOIN PER P3 WITH(NOLOCK)ON R5.PER=P3.COD'   --nombre del rol actual" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ' LEFT JOIN DEP D3 WITH(NOLOCK)ON P3.DEP=D3.COD'  --departamento del rol actual" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ' LEFT JOIN PROVE PV WITH(NOLOCK)ON R5.PROVE=PV.COD'  --c�digo del proveedor del rol actual" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @NOM_TABLA IS NULL AND @ABIERTASPORUSTED=1 AND (@ESTADO='0' OR @ESTADO IS NULL)" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       --para las solicitudes que solo tienen registro en la tabla INSTANCIA y todav�a no las ha procesado el WebService" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + ' UNION SELECT DISTINCT S.COD, S.DEN_' + @IDI + ', I.ID, I.FEC_ALTA AS FECHA, I.ESTADO, NULL, I.ESTADO, I.EN_PROCESO, NULL, I.IMPORTE,I.MON,I.PETICIONARIO,P.NOM+'' ''+P.APE AS PET,NULL,NULL,NULL, isnull(I.EST_VALIDACION,0) as EST_VALIDACION,I.ERROR_VALIDACION,I.FECHA_VALIDACION,'" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + '0,NULL,NULL, NULL, NULL,0,TS.TIPO AS TIPO_SOLIC,I.TRASLADADA,I.ULT_VERSION, 0 AS ICONO,0 as OBV, 0 as ACCION_APROBAR, 0 as ACCION_RECHAZAR FROM INSTANCIA I WITH(NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "   IF (@TITULO IS NOT NULL AND @TITULO <> '') OR (@BUSCARTITULO IS NOT NULL AND @BUSCARTITULO <> '')" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = @SQL + 'INNER JOIN INSTANCIA_VISORES_PM C1 WITH(NOLOCK) ON I.ID=C1.INSTANCIA '" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = @SQL + 'LEFT JOIN INSTANCIA_VISORES_PM C1 WITH(NOLOCK) ON I.ID=C1.INSTANCIA '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + 'INNER JOIN PER P WITH(NOLOCK) ON P.COD = I.PETICIONARIO '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + 'INNER JOIN DEP D WITH (NOLOCK) ON P.DEP=D.COD '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + 'INNER JOIN SOLICITUD S WITH(NOLOCK) ON I.SOLICITUD=S.ID INNER JOIN TIPO_SOLICITUDES TS WITH (NOLOCK) ON S.TIPO=TS.ID'" & vbCrLf
sConsulta = sConsulta & "    IF (@ORIGEN IS NOT NULL AND @ORIGEN <> '') OR  (@BUSCARORIGEN IS NOT NULL AND @BUSCARORIGEN <> '')" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "         SET @SQL = @SQL + ' INNER JOIN PER P WITH(NOLOCK) ON P.COD = I.PETICIONARIO'" & vbCrLf
sConsulta = sConsulta & "         SET @SQL = @SQL + ' INNER JOIN DEP WITH (NOLOCK) ON P.DEP=DEP.COD'" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + @SQLWHERE +  ' AND PETICIONARIO = @USU AND I.ULT_VERSION IS NULL AND I.NUM IS NULL AND EN_PROCESO=1 AND TS.TIPO NOT IN (2,3)'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "--***********************************************************************************************************************************" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL2= N' INSERT INTO #G2 ' + @SQL " & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL2, N'@IDI NVARCHAR(20),@USU NVARCHAR(50),@ID AS INT,@TITULO NVARCHAR(400),@TIPO INT,@FECHADESDE DATETIME,@TIPOFECHA INT,@ORIGEN NVARCHAR(50),@IMPORTEDESDE FLOAT,@IMPORTEHASTA FLOAT,@PETICIONARIO NVARCHAR(50),@BUSCARID AS INT,@BUSCARTITULO NVARCHAR(400),@BUSCARTIPO INT,@BUSCARFECHADESDE DATETIME,@BUSCARORIGEN NVARCHAR(50),@BUSCARIMPORTEDESDE FLOAT,@BUSCARIMPORTEHASTA FLOAT,@BUSCARPETICIONARIO NVARCHAR(50)', @IDI=@IDI,@USU=@USU,@ID=@ID,@TITULO=@TITULO,@TIPO=@TIPO,@FECHADESDE=@FECHADESDE,@TIPOFECHA=@TIPOFECHA,@ORIGEN=@ORIGEN,@IMPORTEDESDE=@IMPORTEDESDE,@IMPORTEHASTA=@IMPORTEHASTA,@PETICIONARIO=@PETICIONARIO,@BUSCARID=@BUSCARID,@BUSCARTITULO=@BUSCARTITULO,@BUSCARTIPO=@BUSCARTIPO,@BUSCARFECHADESDE=@BUSCARFECHADESDE,@BUSCARORIGEN=@BUSCARORIGEN,@BUSCARIMPORTEDESDE=@BUSCARIMPORTEDESDE,@BUSCARIMPORTEHASTA=@BUSCARIMPORTEHASTA,@BUSCARPETICIONARIO=@BUSCARPETICIONARIO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "----********************************************************************************************************************************************************************************************************" & vbCrLf
sConsulta = sConsulta & "IF @PROCESOS = 1   --Si se muestra la informaci�n de procesos" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @proceso_pedido varchar(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @instancia int" & vbCrLf
sConsulta = sConsulta & "declare @ProcesosPedidos_instancia NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "declare @INSTANCIA_ANT int" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE Procesos_cursor cursor for" & vbCrLf
sConsulta = sConsulta & "SELECT PG.SOLICIT INSTANCIA,CONVERT(VARCHAR,PG.ANYO) +'/' + CONVERT(VARCHAR,PG.GMN1) +'/'+ CONVERT(VARCHAR,P.COD)  Proceso" & vbCrLf
sConsulta = sConsulta & "FROM PROCE P WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "INNER JOIN PROCE_GRUPO PG WITH(NOLOCK) ON P.ANYO = PG.ANYO AND P.GMN1 = PG.GMN1 AND P.COD = PG.PROCE" & vbCrLf
sConsulta = sConsulta & "INNER JOIN #G1 ON PG.SOLICIT=#G1.ID" & vbCrLf
sConsulta = sConsulta & "WHERE PG.SOLICIT IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "UNION" & vbCrLf
sConsulta = sConsulta & "SELECT I.SOLICIT INSTANCIA,CONVERT(VARCHAR,P2.ANYO) +'/' + CONVERT(VARCHAR,P2.GMN1 ) +'/'+ CONVERT(VARCHAR,P2.COD)  Proceso" & vbCrLf
sConsulta = sConsulta & "FROM PROCE P2 WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN ITEM I WITH(NOLOCK) ON P2.ANYO = I.ANYO AND P2.GMN1 = I.GMN1_PROCE AND P2.COD = I.PROCE" & vbCrLf
sConsulta = sConsulta & "INNER JOIN #G1 ON I.SOLICIT=#G1.ID" & vbCrLf
sConsulta = sConsulta & "WHERE I.SOLICIT IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "ORDER BY INSTANCIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @INSTANCIA_ANT = 0" & vbCrLf
sConsulta = sConsulta & "SET @ProcesosPedidos_instancia=''" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "OPEN Procesos_cursor FETCH NEXT FROM Procesos_cursor INTO @instancia,@proceso_pedido " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & " --Crea tablas temporales para tener la relaci�n de los campos almacenados en la temporal con el id nuevo:" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE #T1 (I INT, PRO nVARCHAR(2000))" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "---------------------------------------PROCESOS" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS = 0 " & vbCrLf
sConsulta = sConsulta & "BEGIN " & vbCrLf
sConsulta = sConsulta & "   IF @INSTANCIA_ANT = 0" & vbCrLf
sConsulta = sConsulta & "        SET @ProcesosPedidos_instancia = @proceso_pedido" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "        IF (@INSTANCIA = @INSTANCIA_ANT)        " & vbCrLf
sConsulta = sConsulta & "       SET @ProcesosPedidos_instancia = @ProcesosPedidos_instancia  + '; ' + @proceso_pedido " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "        else" & vbCrLf
sConsulta = sConsulta & "       begin" & vbCrLf
sConsulta = sConsulta & "           INSERT INTO #T1  VALUES (@INSTANCIA_ANT,@ProcesosPedidos_instancia)" & vbCrLf
sConsulta = sConsulta & "           SET @ProcesosPedidos_instancia=@proceso_pedido" & vbCrLf
sConsulta = sConsulta & "       end" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   set @INSTANCIA_ANT = @instancia" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM Procesos_cursor  INTO @INSTANCIA,@PROCESO_PEDIDO" & vbCrLf
sConsulta = sConsulta & "END " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ProcesosPedidos_instancia != ''" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO #T1 VALUES (@INSTANCIA_ANT,@ProcesosPedidos_instancia)" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "CLOSE Procesos_cursor " & vbCrLf
sConsulta = sConsulta & "DEALLOCATE Procesos_cursor " & vbCrLf
sConsulta = sConsulta & "-------------------------------------------------------PEDIDOS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE Pedidos_cursor cursor for" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT LP.SOLICIT INSTANCIA ,CONVERT(VARCHAR,P.ANYO) + '/' + CONVERT(VARCHAR,P.NUM)+ '/'+ CONVERT(VARCHAR,O.NUM) Pedido" & vbCrLf
sConsulta = sConsulta & "FROM PEDIDO P WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN ORDEN_ENTREGA O WITH(NOLOCK) ON P.ID = O.PEDIDO" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PROVE PR WITH(NOLOCK) ON O.PROVE = PR.COD" & vbCrLf
sConsulta = sConsulta & "INNER JOIN LINEAS_PEDIDO LP WITH(NOLOCK) ON O.ID = LP.ORDEN  AND LP.DESCR_LIBRE IS NULL" & vbCrLf
sConsulta = sConsulta & "INNER JOIN #G1 ON LP.SOLICIT=#G1.ID" & vbCrLf
sConsulta = sConsulta & "WHERE LP.SOLICIT is not null" & vbCrLf
sConsulta = sConsulta & "ORDER BY INSTANCIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @INSTANCIA_ANT = 0" & vbCrLf
sConsulta = sConsulta & "SET @ProcesosPedidos_instancia=''" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "OPEN Pedidos_cursor FETCH NEXT FROM Pedidos_cursor INTO @instancia,@proceso_pedido" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & " --Crea tablas temporales para tener la relaci�n de los campos almacenados en la temporal con el id nuevo:" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE #T2 (I INT, PED NVARCHAR(2000))" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS = 0 " & vbCrLf
sConsulta = sConsulta & "BEGIN " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @INSTANCIA_ANT = 0" & vbCrLf
sConsulta = sConsulta & "        set @ProcesosPedidos_instancia = @proceso_pedido" & vbCrLf
sConsulta = sConsulta & "   ELSE    " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "        IF (@INSTANCIA = @INSTANCIA_ANT) " & vbCrLf
sConsulta = sConsulta & "       SET @ProcesosPedidos_instancia = @ProcesosPedidos_instancia  + '; ' + @proceso_pedido " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "        ELSE" & vbCrLf
sConsulta = sConsulta & "        begin" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO #T2  VALUES (@INSTANCIA_ANT,@ProcesosPedidos_instancia)" & vbCrLf
sConsulta = sConsulta & "       SET @ProcesosPedidos_instancia=@proceso_pedido" & vbCrLf
sConsulta = sConsulta & "        end" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   set @INSTANCIA_ANT = @instancia" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM Pedidos_cursor  INTO  @INSTANCIA,@PROCESO_PEDIDO " & vbCrLf
sConsulta = sConsulta & "END " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ProcesosPedidos_instancia != ''" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO #T2 VALUES (@INSTANCIA_ANT,@ProcesosPedidos_instancia)" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "CLOSE Pedidos_cursor " & vbCrLf
sConsulta = sConsulta & "DEALLOCATE Pedidos_cursor " & vbCrLf
sConsulta = sConsulta & "------------------------------------   " & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- *****LA SELECT FINAL ********" & vbCrLf
sConsulta = sConsulta & "set @SQL='SELECT DISTINCT #G2.*'" & vbCrLf
sConsulta = sConsulta & "IF @NOM_TABLA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "      SET @SQL = @SQL + ', FILTRO.* '" & vbCrLf
sConsulta = sConsulta & "IF @PROCESOS = 1" & vbCrLf
sConsulta = sConsulta & "   set @SQL=@SQL + ',TP.PRO AS PROCESOS,TE.PED AS PEDIDOS FROM #G2 LEFT JOIN #T1 TP WITH(NOLOCK) ON TP.I=#G2.ID LEFT JOIN #T2 TE WITH(NOLOCK) ON TE.I=#G2.ID'" & vbCrLf
sConsulta = sConsulta & "else" & vbCrLf
sConsulta = sConsulta & "   set @SQL=@SQL + ',NULL AS PROCESOS, NULL AS PEDIDOS FROM #G2'" & vbCrLf
sConsulta = sConsulta & "IF @NOM_TABLA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + ' INNER JOIN ' + @IDI + '_' + @NOM_TABLA + ' FILTRO WITH(NOLOCK) ON #G2.ID = FILTRO.INSTANCIA '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF (@BUSCARARTICULO IS NOT NULL AND @BUSCARARTICULO <> '') OR (@ARTICULO IS NOT NULL AND @ARTICULO <> '')" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + ' INNER JOIN COPIA_CAMPO C WITH(NOLOCK) ON #G2.ID = C.INSTANCIA AND C.NUM_VERSION=#G2.ULT_VERSION INNER JOIN COPIA_CAMPO_DEF CD WITH(NOLOCK) ON C.COPIA_CAMPO_DEF = CD.ID AND TIPO_CAMPO_GS IN (118,119)'" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=  @SQL + ' INNER JOIN COPIA_LINEA_DESGLOSE L WITH (NOLOCK) ON C.ID=L.CAMPO_HIJO'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @ARTICULO IS NOT NULL AND @ARTICULO <> ''" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=  @SQL + '  AND L.VALOR_TEXT LIKE ''%' + @ARTICULO + '%'''" & vbCrLf
sConsulta = sConsulta & "   ELSE --IF @BUSCARARTICULO IS NOT NULL AND @BUSCARARTICULO <> ''     " & vbCrLf
sConsulta = sConsulta & "       SET @SQL=  @SQL + ' AND L.VALOR_TEXT LIKE ''%' + @BUSCARARTICULO + '%'''" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ORDENACION='ID' OR @ORDENACION='TIPO'" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + ' ORDER BY #G2.' + @ORDENACION + ' ' + @SENTIDOORDENACION" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & " begin" & vbCrLf
sConsulta = sConsulta & "      IF @ORDENACION IS NOT NULL AND @ORDENACION <> '' " & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + ' ORDER BY ' + @ORDENACION + ' ' + @SENTIDOORDENACION" & vbCrLf
sConsulta = sConsulta & " end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Borra las tablas temporales" & vbCrLf
sConsulta = sConsulta & "IF @PROCESOS = 1" & vbCrLf
sConsulta = sConsulta & " begin" & vbCrLf
sConsulta = sConsulta & "  DROP TABLE #T2" & vbCrLf
sConsulta = sConsulta & "  DROP TABLE #T1" & vbCrLf
sConsulta = sConsulta & " end" & vbCrLf
sConsulta = sConsulta & "DROP TABLE #G1" & vbCrLf
sConsulta = sConsulta & "DROP TABLE #G2" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_ROL_PETICIONARIOS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_ROL_PETICIONARIOS]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_ROL_PETICIONARIOS @USU VARCHAR(50) = NULL,@FILTRO INT = NULL AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @FILTRO IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SELECT NULL AS COD, NULL AS DEN" & vbCrLf
sConsulta = sConsulta & "   UNION" & vbCrLf
sConsulta = sConsulta & "   SELECT DISTINCT P.COD, P.NOM + ' ' + P.APE AS DEN FROM PER P WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PM_ROL R WITH(NOLOCK) ON P.COD = R.PER" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN WORKFLOW W WITH(NOLOCK) ON R.WORKFLOW = W.ID" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN SOLICITUD S WITH(NOLOCK) ON W.ID = S.WORKFLOW" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN FORMULARIO F WITH(NOLOCK) ON S.FORMULARIO = F.ID" & vbCrLf
sConsulta = sConsulta & "   WHERE R.TIPO = 4 AND P.BAJALOG= 0 AND F.ID IN (SELECT DISTINCT F.ID" & vbCrLf
sConsulta = sConsulta & "       FROM SOLICITUD S WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN TIPO_SOLICITUDES TS WITH (NOLOCK) ON S.TIPO=TS.ID" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN FORMULARIO F WITH (NOLOCK) ON S.FORMULARIO = F.ID" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PM_ROL R WITH (NOLOCK) ON R.WORKFLOW=S.WORKFLOW AND (PER=@USU OR SUSTITUTO=@USU)" & vbCrLf
sConsulta = sConsulta & "       WHERE TS.TIPO NOT IN (2,3)" & vbCrLf
sConsulta = sConsulta & "       UNION" & vbCrLf
sConsulta = sConsulta & "       SELECT DISTINCT F.ID" & vbCrLf
sConsulta = sConsulta & "       FROM SOLICITUD S WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN TIPO_SOLICITUDES TS WITH (NOLOCK) ON S.TIPO=TS.ID" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN FORMULARIO F WITH (NOLOCK) ON S.FORMULARIO = F.ID" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN VIEW_ROL_PETICIONARIO R WITH (NOLOCK) ON S.ID = R.SOLICITUD AND R.COD=@USU" & vbCrLf
sConsulta = sConsulta & "       WHERE TS.TIPO NOT IN (2,3))" & vbCrLf
sConsulta = sConsulta & "   UNION" & vbCrLf
sConsulta = sConsulta & "   SELECT DISTINCT P.COD, P.NOM + ' ' + P.APE AS DEN FROM PER P WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN VIEW_ROL_PETICIONARIO R WITH(NOLOCK) ON P.COD = R.COD" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN SOLICITUD S WITH(NOLOCK) ON R.SOLICITUD = S.ID" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN FORMULARIO F WITH(NOLOCK) ON S.FORMULARIO = F.ID" & vbCrLf
sConsulta = sConsulta & "   WHERE P.BAJALOG= 0 AND F.ID IN (SELECT DISTINCT F.ID" & vbCrLf
sConsulta = sConsulta & "       FROM SOLICITUD S WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN TIPO_SOLICITUDES TS WITH (NOLOCK) ON S.TIPO=TS.ID" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN FORMULARIO F WITH (NOLOCK) ON S.FORMULARIO = F.ID" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PM_ROL R WITH (NOLOCK) ON R.WORKFLOW=S.WORKFLOW AND (PER=@USU OR SUSTITUTO=@USU)" & vbCrLf
sConsulta = sConsulta & "       WHERE TS.TIPO NOT IN (2,3)" & vbCrLf
sConsulta = sConsulta & "       UNION" & vbCrLf
sConsulta = sConsulta & "       SELECT DISTINCT F.ID" & vbCrLf
sConsulta = sConsulta & "       FROM SOLICITUD S WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN TIPO_SOLICITUDES TS WITH (NOLOCK) ON S.TIPO=TS.ID" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN FORMULARIO F WITH (NOLOCK) ON S.FORMULARIO = F.ID" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN VIEW_ROL_PETICIONARIO R WITH (NOLOCK) ON S.ID = R.SOLICITUD AND R.COD=@USU" & vbCrLf
sConsulta = sConsulta & "       WHERE TS.TIPO NOT IN (2,3))" & vbCrLf
sConsulta = sConsulta & "   ORDER BY DEN" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SELECT NULL AS COD, NULL AS DEN" & vbCrLf
sConsulta = sConsulta & "   UNION" & vbCrLf
sConsulta = sConsulta & "   SELECT DISTINCT P.COD, P.NOM + ' ' + P.APE AS DEN FROM PER P WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PM_ROL R WITH(NOLOCK) ON P.COD = R.PER" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN WORKFLOW W WITH(NOLOCK) ON R.WORKFLOW = W.ID" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN SOLICITUD S WITH(NOLOCK) ON W.ID = S.WORKFLOW" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN FORMULARIO F WITH(NOLOCK) ON S.FORMULARIO = F.ID" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PM_FILTROS PF WITH(NOLOCK) ON F.ID=PF.FORMULARIO" & vbCrLf
sConsulta = sConsulta & "   WHERE R.TIPO = 4 AND P.BAJALOG= 0 AND PF.ID = @FILTRO" & vbCrLf
sConsulta = sConsulta & "   UNION" & vbCrLf
sConsulta = sConsulta & "   SELECT DISTINCT P.COD, P.NOM + ' ' + P.APE AS DEN FROM PER P WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN VIEW_ROL_PETICIONARIO R WITH(NOLOCK) ON P.COD = R.COD" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN SOLICITUD S WITH(NOLOCK) ON R.SOLICITUD = S.ID" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN FORMULARIO F WITH(NOLOCK) ON S.FORMULARIO = F.ID" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PM_FILTROS PF WITH(NOLOCK) ON F.ID=PF.FORMULARIO" & vbCrLf
sConsulta = sConsulta & "   WHERE P.BAJALOG= 0 AND PF.ID = @FILTRO" & vbCrLf
sConsulta = sConsulta & "   ORDER BY DEN" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_TRASLADAR_INSTANCIA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_TRASLADAR_INSTANCIA]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_TRASLADAR_INSTANCIA @INSTANCIA INT,@PER VARCHAR(50), @DESTINATARIO VARCHAR(50), @COMENT VARCHAR(500)=NULL,@FECLIMITE DATETIME=NULL,@ES_PROVEEDOR TINYINT=0 ,@ETAPA INT,@CONTACTO INT=0, @PER_ORIGEN AS VARCHAR(50) = NULL AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @ESTADO INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ROL INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @ESTADO=ESTADO FROM INSTANCIA WHERE INSTANCIA.ID = @INSTANCIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE INSTANCIA SET TRASLADADA=1 WHERE ID=@INSTANCIA" & vbCrLf
sConsulta = sConsulta & "UPDATE INSTANCIA_BLOQUE SET TRASLADADA=1,TRASLADO_A=@DESTINATARIO, DEVOLUCION_A=@PER WHERE ID=@ETAPA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Comprueba si existe un rol para el usuario realiza el traspaso y la etapa actual:" & vbCrLf
sConsulta = sConsulta & "SELECT TOP 1 @ROL=R.ID " & vbCrLf
sConsulta = sConsulta & "FROM PM_COPIA_ROL R WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_COPIA_ROL_BLOQUE B WITH(NOLOCK) ON R.ID=B.ROL " & vbCrLf
sConsulta = sConsulta & "INNER JOIN INSTANCIA_BLOQUE WITH(NOLOCK) ON B.BLOQUE=INSTANCIA_BLOQUE.BLOQUE AND INSTANCIA_BLOQUE.ESTADO=1 AND INSTANCIA_BLOQUE.BLOQ<>1" & vbCrLf
sConsulta = sConsulta & "WHERE INSTANCIA_BLOQUE.ID=@ETAPA AND R.PER=@PER_ORIGEN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ES_PROVEEDOR=1 " & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    INSERT INTO INSTANCIA_EST (INSTANCIA, PER,COMENT, FECHA, EST,ACCION,DESTINATARIO_PROV,FEC_LIMITE,ROL,BLOQUE,CONTACTO)  VALUES (@INSTANCIA, @PER,@COMENT, GETDATE(), @ESTADO,3,@DESTINATARIO,@FECLIMITE,@ROL,@ETAPA,@CONTACTO)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    -- Se llama al siguiente procedimiento almacenado que se encarga de actualizar las solicitudes actuales y nuevas del proveedor:" & vbCrLf
sConsulta = sConsulta & "    EXEC FSPM_ACT_REL_CIAS @INSTANCIA = @INSTANCIA, @DESTINATARIO_PROV=@DESTINATARIO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "    INSERT INTO INSTANCIA_EST (INSTANCIA, PER,COMENT, FECHA, EST,ACCION,DESTINATARIO,FEC_LIMITE,ROL,BLOQUE)  VALUES (@INSTANCIA, @PER,@COMENT, GETDATE(), @ESTADO,3,@DESTINATARIO,@FECLIMITE,@ROL,@ETAPA)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta



sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSPM_SAVE_NEW_INSTANCE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSPM_SAVE_NEW_INSTANCE]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_SAVE_NEW_INSTANCE @ID INT, @SOLICITUD INT, @FORMULARIO INT, @WORKFLOW INT, @IMPORTE_PEDIDO_DIRECTO FLOAT,@PETICIONARIO VARCHAR(50), @COMPRADOR VARCHAR(50) = NULL, @MON VARCHAR(50), @GUARDAR TINYINT =0, @IMPORTE FLOAT =NULL, @PEDIDO_AUT TINYINT=0, @ESTADO INT=0 OUTPUT,@COMENT_ALTA NVARCHAR(500)=NULL, @ACCION INT=0 OUTPUT, @ERRORVAR INT=0 OUTPUT, @ENTRATAMIENTO TINYINT=0  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT OFF" & vbCrLf
sConsulta = sConsulta & "DECLARE @NUM AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @FECHA AS DATETIME" & vbCrLf
sConsulta = sConsulta & "DECLARE @SENTIDO AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @NUM_VERSION AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @TIPO_SOLICIT INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVE VARCHAR(50) " & vbCrLf
sConsulta = sConsulta & "DECLARE @BLOQUE INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDIS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDIS2 NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDIS3 NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDIS4 NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @APROB INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PUB TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_CERTIF INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_NEW INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @OLD_VERSION AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @GRABADO AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @FILASTEMPORAL AS INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @FILASTEMPORAL = (SELECT COUNT(*) FROM #TEMPORAL)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @FILASTEMPORAL>0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @OLD_VERSION = (SELECT TOP 1 NUM_VERSION " & vbCrLf
sConsulta = sConsulta & "   FROM COPIA_CAMPO CC WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN COPIA_CAMPO_DEF CCD WITH (NOLOCK)  ON CC.COPIA_CAMPO_DEF = CCD.ID" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN #TEMPORAL T WITH (NOLOCK)  ON CCD.CAMPO_ORIGEN = T.CAMPO" & vbCrLf
sConsulta = sConsulta & "   WHERE CC.INSTANCIA = @ID)" & vbCrLf
sConsulta = sConsulta & "   set @OLD_VERSION = isnull(@OLD_VERSION,0)" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   SET @OLD_VERSION = (SELECT ISNULL(MAX(NUM_VERSION),0) FROM VERSION_INSTANCIA WHERE INSTANCIA = @ID)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Versi�n nueva:" & vbCrLf
sConsulta = sConsulta & "SET @NUM_VERSION = (SELECT ISNULL(MAX(NUM_VERSION),0) + 1 FROM VERSION_INSTANCIA WHERE INSTANCIA = @ID)" & vbCrLf
sConsulta = sConsulta & "IF @OLD_VERSION = 0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   --ESTE ES EL CASO DE QUE DESDE ALTA HAYAN PULSADO EN GUARDAR POR 1� VEZ" & vbCrLf
sConsulta = sConsulta & "   --GUARDAMOS LA ESTRUCTURA DEL WORKFLOW" & vbCrLf
sConsulta = sConsulta & "   SET @IDIS=N''" & vbCrLf
sConsulta = sConsulta & "   SET @IDIS2=N''" & vbCrLf
sConsulta = sConsulta & "   SET @IDIS3=N''" & vbCrLf
sConsulta = sConsulta & "   SET @IDIS4=N''" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @IDIS = @IDIS + N' A.DEN_ENG, A.DEN_GER, A.DEN_SPA, '" & vbCrLf
sConsulta = sConsulta & "   SET @IDIS2 = @IDIS2 + N' A.AYUDA_ENG, A.AYUDA_GER, A.AYUDA_SPA, '" & vbCrLf
sConsulta = sConsulta & "   SET @IDIS3 = @IDIS3 + N' CVL.VALOR_TEXT_ENG, CVL.VALOR_TEXT_GER, CVL.VALOR_TEXT_SPA, '" & vbCrLf
sConsulta = sConsulta & "   SET @IDIS4 = @IDIS4 + N' VALOR_TEXT_ENG, VALOR_TEXT_GER, VALOR_TEXT_SPA, '" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   SET @SQL = N'INSERT INTO COPIA_GRUPO (INSTANCIA, ' + @IDIS + ' ORDEN, GRUPO_ORIGEN) SELECT @ID,' + @IDIS + ' ORDEN, A.ID FROM FORM_GRUPO A  WITH (NOLOCK) WHERE FORMULARIO = @FORMULARIO'" & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL, N'@ID INT, @FORMULARIO INT', @ID=@ID, @FORMULARIO = @FORMULARIO" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = N'INSERT INTO COPIA_CAMPO_DEF (GRUPO, ' + @IDIS + ' ' + @IDIS2 + ' TIPO, SUBTIPO, INTRO, MINNUM, MAXNUM, MINFEC, MAXFEC, ORDEN, ES_SUBCAMPO,TIPO_CAMPO_GS, ID_ATRIB_GS, GMN1, GMN2, GMN3, GMN4, LINEAS_PRECONF, ID_CALCULO, FORMULA, ORDEN_CALCULO, IMPORTE_WORKFL, ORIGEN_CALC_DESGLOSE,CAMPO_ORIGEN,POPUP,ANYADIR_ART,TITULO,FECHA_VALOR_DEFECTO,IDREFSOLICITUD,AVISOBLOQUEOIMPACUM,CARGAR_ULT_ADJ,MAX_LENGTH,A_PROCESO,A_PEDIDO,TABLA_EXTERNA) " & vbCrLf
sConsulta = sConsulta & "                SELECT CG.ID, ' + @IDIS + ' ' + @IDIS2 + ' A.TIPO, A.SUBTIPO, A.INTRO, A.MINNUM, A.MAXNUM, A.MINFEC, A.MAXFEC, A.ORDEN, A.ES_SUBCAMPO, A.TIPO_CAMPO_GS, A.ID_ATRIB_GS, A.GMN1, A.GMN2, A.GMN3, A.GMN4, A.LINEAS_PRECONF, A.ID_CALCULO, A.FORMULA, A.ORDEN_CALCULO, A.IMPORTE_WORKFL, A.ORIGEN_CALC_DESGLOSE, A.ID , A.POPUP,A.ANYADIR_ART, A.TITULO, A.FECHA_VALOR_DEFECTO,A.IDREFSOLICITUD,A.AVISOBLOQUEOIMPACUM,A.CARGAR_ULT_ADJ,A.MAX_LENGTH,A.A_PROCESO,A.A_PEDIDO,A.TABLA_EXTERNA" & vbCrLf
sConsulta = sConsulta & "                  FROM FORM_CAMPO A  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                         INNER JOIN FORM_GRUPO FG  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                                 ON A.GRUPO = FG.ID " & vbCrLf
sConsulta = sConsulta & "                        INNER JOIN COPIA_GRUPO CG  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                                 ON FG.ID = CG.GRUPO_ORIGEN " & vbCrLf
sConsulta = sConsulta & "                 WHERE FG.FORMULARIO = @FORMULARIO" & vbCrLf
sConsulta = sConsulta & "                   AND CG.INSTANCIA = @ID" & vbCrLf
sConsulta = sConsulta & "      ORDER BY CG.ID, A.ES_SUBCAMPO, A.ORDEN'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL, N'@ID INT, @FORMULARIO INT', @ID=@ID, @FORMULARIO = @FORMULARIO" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   SET @SQL = N'INSERT INTO COPIA_CAMPO_VALOR_LISTA (CAMPO_DEF, ORDEN, VALOR_NUM, ' + @IDIS4 + ' VALOR_FEC, ORD) " & vbCrLf
sConsulta = sConsulta & "    SELECT CCD.ID, CVL.ORDEN, CVL.VALOR_NUM, ' + @IDIS3 + ' CVL.VALOR_FEC, CVL.ORD " & vbCrLf
sConsulta = sConsulta & "                  FROM COPIA_CAMPO_DEF CCD WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                         INNER JOIN FORM_CAMPO A WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                                 ON CCD.CAMPO_ORIGEN = A.ID" & vbCrLf
sConsulta = sConsulta & "                         INNER JOIN CAMPO_VALOR_LISTA CVL WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                        ON A.ID = CVL.CAMPO" & vbCrLf
sConsulta = sConsulta & "                         INNER JOIN FORM_GRUPO FG WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                 ON A.GRUPO = FG.ID " & vbCrLf
sConsulta = sConsulta & "                         INNER JOIN COPIA_GRUPO CG WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                 ON FG.ID = CG.GRUPO_ORIGEN " & vbCrLf
sConsulta = sConsulta & "                                AND CCD.GRUPO = CG.ID" & vbCrLf
sConsulta = sConsulta & "                 WHERE FG.FORMULARIO = @FORMULARIO" & vbCrLf
sConsulta = sConsulta & "                   AND CG.INSTANCIA = @ID'" & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL, N'@ID INT, @FORMULARIO INT', @ID=@ID, @FORMULARIO = @FORMULARIO" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   UPDATE COPIA_CAMPO_DEF SET ORIGEN_CALC_DESGLOSE = CCD2.ID" & vbCrLf
sConsulta = sConsulta & "    FROM COPIA_CAMPO_DEF CCD  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "         INNER JOIN (SELECT CCD.* FROM COPIA_CAMPO_DEF CCD WITH (NOLOCK) INNER JOIN COPIA_GRUPO CG WITH (NOLOCK) ON CG.ID = CCD.GRUPO WHERE CG.INSTANCIA = @ID) CCD2" & vbCrLf
sConsulta = sConsulta & "                 ON CCD.ORIGEN_CALC_DESGLOSE = CCD2.CAMPO_ORIGEN" & vbCrLf
sConsulta = sConsulta & "         INNER JOIN COPIA_GRUPO CG WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                 ON CCD.GRUPO = CG.ID" & vbCrLf
sConsulta = sConsulta & "   WHERE CG.INSTANCIA = @ID" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO  ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   ------------------------------------------------ Aqu� todas las tablas para el nuevo workflow: ------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "   SET @GRABADO = 0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @ENTRATAMIENTO=1" & vbCrLf
sConsulta = sConsulta & "       SET @GRABADO=(SELECT COUNT(1) FROM PM_COPIA_BLOQUE WITH (NOLOCK) WHERE INSTANCIA=@ID)" & vbCrLf
sConsulta = sConsulta & "   IF @GRABADO=0" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       INSERT INTO PM_COPIA_BLOQUE (INSTANCIA,TIPO,[TOP],[LEFT],WIDTH,HEIGHT,BLOQUE_ORIGEN) " & vbCrLf
sConsulta = sConsulta & "         SELECT @ID,TIPO,[TOP],[LEFT],WIDTH,HEIGHT,ID FROM PM_BLOQUE WITH (NOLOCK) WHERE WORKFLOW=@WORKFLOW" & vbCrLf
sConsulta = sConsulta & "       IF @@ERROR!=0 GOTO  ERROR_" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @ENTRATAMIENTO=1" & vbCrLf
sConsulta = sConsulta & "       SET @GRABADO=(SELECT COUNT(1) FROM PM_COPIA_BLOQUE_DEN " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PM_COPIA_BLOQUE B  WITH (NOLOCK) ON PM_COPIA_BLOQUE_DEN.BLOQUE=B.ID " & vbCrLf
sConsulta = sConsulta & "           WHERE  B.INSTANCIA=@ID)" & vbCrLf
sConsulta = sConsulta & "   IF @GRABADO=0" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       INSERT INTO PM_COPIA_BLOQUE_DEN (BLOQUE,IDI,DEN)" & vbCrLf
sConsulta = sConsulta & "       SELECT B.ID,PM_BLOQUE_DEN.IDI, PM_BLOQUE_DEN.DEN FROM PM_BLOQUE_DEN  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PM_COPIA_BLOQUE B  WITH (NOLOCK) ON PM_BLOQUE_DEN.BLOQUE=B.BLOQUE_ORIGEN" & vbCrLf
sConsulta = sConsulta & "       WHERE  B.INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "       IF @@ERROR!=0 GOTO  ERROR_" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @ENTRATAMIENTO=1" & vbCrLf
sConsulta = sConsulta & "       SET @GRABADO=(SELECT COUNT(1)   FROM PM_COPIA_ACCIONES  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PM_COPIA_BLOQUE B  WITH (NOLOCK) ON PM_COPIA_ACCIONES.BLOQUE=B.ID" & vbCrLf
sConsulta = sConsulta & "           WHERE  B.INSTANCIA=@ID)" & vbCrLf
sConsulta = sConsulta & "   IF @GRABADO=0" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       INSERT INTO PM_COPIA_ACCIONES (BLOQUE,CUMP_OBL_ROL,GUARDA,TIPO_RECHAZO,TIPO,ACCION_ORIGEN,SOLO_RECHAZADAS,INCREMENTAR_ID)" & vbCrLf
sConsulta = sConsulta & "       SELECT B.ID, PM_ACCIONES.CUMP_OBL_ROL,PM_ACCIONES.GUARDA,PM_ACCIONES.TIPO_RECHAZO,PM_ACCIONES.TIPO,PM_ACCIONES.ID,SOLO_RECHAZADAS,PM_ACCIONES.INCREMENTAR_ID" & vbCrLf
sConsulta = sConsulta & "       FROM PM_ACCIONES   WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PM_COPIA_BLOQUE B  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       ON PM_ACCIONES.BLOQUE=B.BLOQUE_ORIGEN" & vbCrLf
sConsulta = sConsulta & "       WHERE  B.INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "       IF @@ERROR!=0 GOTO  ERROR_" & vbCrLf
sConsulta = sConsulta & "   END     " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @ENTRATAMIENTO=1" & vbCrLf
sConsulta = sConsulta & "       SET @GRABADO=(SELECT COUNT(1) FROM PM_COPIA_ACCIONES_DEN  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PM_COPIA_ACCIONES A WITH (NOLOCK)    ON PM_COPIA_ACCIONES_DEN.ACCION=A.ID" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PM_COPIA_BLOQUE B WITH (NOLOCK) ON A.BLOQUE=B.ID" & vbCrLf
sConsulta = sConsulta & "           WHERE B.INSTANCIA=@ID)" & vbCrLf
sConsulta = sConsulta & "   IF @GRABADO=0" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       INSERT INTO PM_COPIA_ACCIONES_DEN (ACCION,IDI,DEN)" & vbCrLf
sConsulta = sConsulta & "       SELECT A.ID,PM_ACCIONES_DEN.IDI, PM_ACCIONES_DEN.DEN " & vbCrLf
sConsulta = sConsulta & "       FROM PM_ACCIONES_DEN  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PM_COPIA_ACCIONES A WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       ON PM_ACCIONES_DEN.ACCION=A.ACCION_ORIGEN" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PM_COPIA_BLOQUE B WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       ON A.BLOQUE=B.ID" & vbCrLf
sConsulta = sConsulta & "       WHERE B.INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "       IF @@ERROR!=0 GOTO  ERROR_" & vbCrLf
sConsulta = sConsulta & "   END     " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @ENTRATAMIENTO=1" & vbCrLf
sConsulta = sConsulta & "       SET @GRABADO=(SELECT COUNT(1) FROM PM_COPIA_ACCION_EMAIL WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PM_COPIA_ACCIONES A WITH (NOLOCK)    ON A.ID=PM_COPIA_ACCION_EMAIL.ACCION" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PM_COPIA_BLOQUE B WITH (NOLOCK)  ON B.ID=A.BLOQUE" & vbCrLf
sConsulta = sConsulta & "           WHERE B.INSTANCIA=@ID)" & vbCrLf
sConsulta = sConsulta & "   IF @GRABADO=0" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       INSERT INTO PM_COPIA_ACCION_EMAIL (ACCION,IDI,SUBJECT)" & vbCrLf
sConsulta = sConsulta & "       SELECT A.ID, AE.IDI, AE.SUBJECT" & vbCrLf
sConsulta = sConsulta & "       FROM PM_ACCIONES_EMAIL AE WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PM_COPIA_ACCIONES A WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       ON A.ACCION_ORIGEN=AE.ACCION" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PM_COPIA_BLOQUE B WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       ON B.ID=A.BLOQUE" & vbCrLf
sConsulta = sConsulta & "       WHERE B.INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "       IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @ENTRATAMIENTO=1" & vbCrLf
sConsulta = sConsulta & "       SET @GRABADO=(SELECT COUNT(1)   FROM PM_COPIA_ROL WITH (NOLOCK) WHERE INSTANCIA=@ID)" & vbCrLf
sConsulta = sConsulta & "   IF @GRABADO=0" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       INSERT INTO PM_COPIA_ROL(INSTANCIA,DEN,TIPO,CUANDO_ASIGNAR,BLOQUE,COMO_ASIGNAR,RESTRINGIR,CAMPO,ROL,PER,PROVE,CONF_BLOQUE_DEF,UON1,UON2,UON3,DEP,EQP,CON,ROL_ORIGEN,PER_ORIGINAL,VER_FLUJO,VER_DETALLE_PER)" & vbCrLf
sConsulta = sConsulta & "       SELECT @ID, P.DEN,P.TIPO,P.CUANDO_ASIGNAR,CB1.ID,P.COMO_ASIGNAR,P.RESTRINGIR, CAMPO_DEF.ID,P.ROL_ASIGNA,CASE WHEN USU.FSWS_SUSTITUCION IS NOT NULL THEN USU.FSWS_SUSTITUCION ELSE P.PER END,P.PROVE,CB2.ID,P.UON1,P.UON2,P.UON3,P.DEP,P.EQP,P.CON,P.ID,P.PER,P.VER_FLUJO,P.VER_DETALLE_PER" & vbCrLf
sConsulta = sConsulta & "       FROM PM_ROL P  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN PM_COPIA_BLOQUE CB1  WITH (NOLOCK) ON P.BLOQUE_ASIGNA=CB1.BLOQUE_ORIGEN AND CB1.INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN PM_COPIA_BLOQUE CB2  WITH (NOLOCK) ON P.CONF_BLOQUE_DEF=CB2.BLOQUE_ORIGEN  AND CB2.INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN USU  WITH (NOLOCK)                                   ON P.PER=USU.PER" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN (SELECT COPIA_CAMPO_DEF.ID,COPIA_CAMPO_DEF.CAMPO_ORIGEN  " & vbCrLf
sConsulta = sConsulta & "       FROM COPIA_CAMPO_DEF  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN COPIA_GRUPO  WITH (NOLOCK)   ON COPIA_CAMPO_DEF.GRUPO =COPIA_GRUPO.ID AND COPIA_GRUPO.INSTANCIA=@ID) CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "       ON P.CAMPO=CAMPO_DEF.CAMPO_ORIGEN" & vbCrLf
sConsulta = sConsulta & "       WHERE WORKFLOW=@WORKFLOW" & vbCrLf
sConsulta = sConsulta & "       IF @@ERROR!=0 GOTO  ERROR_" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF (SELECT COUNT(*) FROM PM_COPIA_ROL  WITH (NOLOCK) WHERE ROL IS NOT NULL and instancia=@id) >0" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       DECLARE @ROL_ORIGEN AS INTEGER" & vbCrLf
sConsulta = sConsulta & "       DECLARE @ROL AS INTEGER" & vbCrLf
sConsulta = sConsulta & "       DECLARE C1 CURSOR FOR SELECT ID,ROL_ORIGEN FROM PM_COPIA_ROL  WITH (NOLOCK) WHERE INSTANCIA=@ID AND ROL_ORIGEN IS NOT NULL AND ROL_ORIGEN IN (SELECT ROL FROM PM_COPIA_ROL WITH (NOLOCK)  WHERE INSTANCIA=@ID AND ROL IS NOT NULL)" & vbCrLf
sConsulta = sConsulta & "       OPEN C1" & vbCrLf
sConsulta = sConsulta & "       FETCH NEXT FROM C1 INTO @ROL,@ROL_ORIGEN" & vbCrLf
sConsulta = sConsulta & "       WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "            UPDATE PM_COPIA_ROL SET ROL = @ROL WHERE INSTANCIA = @ID AND ROL=@ROL_ORIGEN" & vbCrLf
sConsulta = sConsulta & "            IF @@ERROR!=0 GOTO  ERROR_" & vbCrLf
sConsulta = sConsulta & "           FETCH NEXT FROM C1 INTO @ROL,@ROL_ORIGEN" & vbCrLf
sConsulta = sConsulta & "       END " & vbCrLf
sConsulta = sConsulta & "       CLOSE C1" & vbCrLf
sConsulta = sConsulta & "       DEALLOCATE C1" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   -- Actualizo los Roles Peticionario, para que en el Campo PER tenga el codigo del la persona Peticionaria, " & vbCrLf
sConsulta = sConsulta & "   -- s�lo actualizo si no existe un rol peticionbario que tenga la persona peticionaria" & vbCrLf
sConsulta = sConsulta & "   IF (SELECT COUNT(*) FROM PM_COPIA_ROL WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "        INNER JOIN PER P WITH (NOLOCK) ON ISNULL(P.UON1,'') = ISNULL(PM_COPIA_ROL.UON1,'') AND ISNULL(P.UON2,'') = ISNULL(PM_COPIA_ROL.UON2,'') AND " & vbCrLf
sConsulta = sConsulta & "              ISNULL(P.UON3,'') = ISNULL(PM_COPIA_ROL.UON3,'') AND P.DEP=PM_COPIA_ROL.DEP AND P.COD=PM_COPIA_ROL.PER" & vbCrLf
sConsulta = sConsulta & "        WHERE P.COD = @PETICIONARIO AND PM_COPIA_ROL.TIPO = 4 AND PM_COPIA_ROL.INSTANCIA = @ID)=0" & vbCrLf
sConsulta = sConsulta & "      BEGIN" & vbCrLf
sConsulta = sConsulta & "          --Actualizo los roles de la UON y DEP de la persona peticionaria" & vbCrLf
sConsulta = sConsulta & "       UPDATE PM_COPIA_ROL  SET PER = P.COD,PER_ORIGINAL = P.COD FROM PER P WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "         WHERE ISNULL(P.UON1,'') = ISNULL(PM_COPIA_ROL.UON1,'') AND ISNULL(P.UON2,'') = ISNULL(PM_COPIA_ROL.UON2,'') AND ISNULL(P.UON3,'') = " & vbCrLf
sConsulta = sConsulta & "               ISNULL(PM_COPIA_ROL.UON3,'')  AND P.DEP=PM_COPIA_ROL.DEP AND P.COD = @PETICIONARIO AND PM_COPIA_ROL.TIPO = 4 AND PM_COPIA_ROL.INSTANCIA = @ID AND PM_COPIA_ROL.PER IS NULL" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "       IF @@ROWCOUNT = 0 --Actualizo los roles de la UON  de la persona peticionaria, no hay DEP en el rol" & vbCrLf
sConsulta = sConsulta & "          BEGIN" & vbCrLf
sConsulta = sConsulta & "          UPDATE PM_COPIA_ROL   SET PER = P.COD,PER_ORIGINAL = P.COD   FROM PER P WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "          WHERE ISNULL(P.UON1,'') = ISNULL(PM_COPIA_ROL.UON1,'') AND ISNULL(P.UON2,'') = ISNULL(PM_COPIA_ROL.UON2,'') AND ISNULL(P.UON3,'') = " & vbCrLf
sConsulta = sConsulta & "                 ISNULL(PM_COPIA_ROL.UON3,'') AND PM_COPIA_ROL.DEP IS NULL AND  P.COD = @PETICIONARIO AND PM_COPIA_ROL.TIPO = 4 AND PM_COPIA_ROL.INSTANCIA = @ID AND  PM_COPIA_ROL.PER IS NULL" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "          IF @@ROWCOUNT = 0 --Actualizo los roles de la UON  de la persona peticionaria, no hay DEP en el rol ni UON3" & vbCrLf
sConsulta = sConsulta & "            BEGIN" & vbCrLf
sConsulta = sConsulta & "              UPDATE PM_COPIA_ROL   SET PER = P.COD,PER_ORIGINAL = P.COD   FROM PER P WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "              WHERE ISNULL(P.UON1,'') = ISNULL(PM_COPIA_ROL.UON1,'') AND ISNULL(P.UON2,'') = ISNULL(PM_COPIA_ROL.UON2,'') AND PM_COPIA_ROL.UON3 IS NULL AND " & vbCrLf
sConsulta = sConsulta & "                 PM_COPIA_ROL.DEP IS NULL AND P.COD = @PETICIONARIO AND PM_COPIA_ROL.TIPO = 4 AND PM_COPIA_ROL.INSTANCIA = @ID AND  PM_COPIA_ROL.PER IS NULL" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "              IF @@ROWCOUNT = 0 --Actualizo los roles de la UON  de la persona peticionaria, no hay DEP en el rol ni UON3" & vbCrLf
sConsulta = sConsulta & "              BEGIN" & vbCrLf
sConsulta = sConsulta & "                UPDATE PM_COPIA_ROL   SET PER = P.COD,PER_ORIGINAL = P.COD   FROM PER P WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                WHERE ISNULL(P.UON1,'') = ISNULL(PM_COPIA_ROL.UON1,'') AND PM_COPIA_ROL.UON2 IS NULL AND PM_COPIA_ROL.UON3 IS NULL AND PM_COPIA_ROL.DEP IS NULL" & vbCrLf
sConsulta = sConsulta & "                         AND P.COD = @PETICIONARIO AND PM_COPIA_ROL.TIPO = 4 AND PM_COPIA_ROL.INSTANCIA = @ID AND  PM_COPIA_ROL.PER IS NULL" & vbCrLf
sConsulta = sConsulta & "                IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "                END" & vbCrLf
sConsulta = sConsulta & "            END" & vbCrLf
sConsulta = sConsulta & "          END" & vbCrLf
sConsulta = sConsulta & "       END " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- Ojo a las sustituciones temporales" & vbCrLf
sConsulta = sConsulta & "   IF (SELECT COUNT(*) FROM PM_COPIA_ROL WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PER P WITH (NOLOCK) ON P.COD=PM_COPIA_ROL.PER" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PM_ROL R WITH (NOLOCK) ON R.ID=PM_COPIA_ROL.ROL_ORIGEN" & vbCrLf
sConsulta = sConsulta & "       AND ISNULL(R.UON1,'')=ISNULL(P.UON1,'')" & vbCrLf
sConsulta = sConsulta & "       AND ISNULL(R.UON2,'')=ISNULL(P.UON2,'')" & vbCrLf
sConsulta = sConsulta & "       AND ISNULL(R.UON3,'')=ISNULL(P.UON3,'')" & vbCrLf
sConsulta = sConsulta & "       AND ISNULL(R.DEP,'')=ISNULL(P.DEP,'')" & vbCrLf
sConsulta = sConsulta & "       AND ISNULL(R.EQP,'')=ISNULL(P.EQP,'')" & vbCrLf
sConsulta = sConsulta & "        WHERE PM_COPIA_ROL.PER = @PETICIONARIO" & vbCrLf
sConsulta = sConsulta & "   AND PM_COPIA_ROL.PER =PM_COPIA_ROL.PER_ORIGINAL" & vbCrLf
sConsulta = sConsulta & "   AND PM_COPIA_ROL.TIPO = 4 AND PM_COPIA_ROL.INSTANCIA = @ID)=0" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "        --Coincidencia entre uon1 ... eqp de pm_rol y uon1 ... eqp del sustituto q gracias a los isnull me ha actualizado ya el pm_copia_rol per y per_original al sustituto. lo de per_original no esta bien" & vbCrLf
sConsulta = sConsulta & "   UPDATE PM_COPIA_ROL  SET PER_ORIGINAL = P.COD FROM PER P WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "           WHERE P.COD IN (SELECT PER FROM USU WITH(NOLOCK) WHERE FSWS_SUSTITUCION=@PETICIONARIO)" & vbCrLf
sConsulta = sConsulta & "              AND PM_COPIA_ROL.TIPO = 4 AND PM_COPIA_ROL.INSTANCIA = @ID AND PM_COPIA_ROL.PER=@PETICIONARIO" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "   -- Actualizo los Roles Peticionario, para que en el Campo PER tenga el codigo del la persona Peticionaria," & vbCrLf
sConsulta = sConsulta & "   -- s�lo actualizo si no existe un rol peticionbario que tenga la persona peticionaria" & vbCrLf
sConsulta = sConsulta & "   IF (SELECT COUNT(*) FROM PM_COPIA_ROL WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN PER P WITH (NOLOCK) ON ISNULL(P.UON1,'') = ISNULL(PM_COPIA_ROL.UON1,'')" & vbCrLf
sConsulta = sConsulta & "              AND ISNULL(P.UON2,'') = ISNULL(PM_COPIA_ROL.UON2,'') AND ISNULL(P.UON3,'') = ISNULL(PM_COPIA_ROL.UON3,'')" & vbCrLf
sConsulta = sConsulta & "              AND P.DEP=PM_COPIA_ROL.DEP AND P.COD=PM_COPIA_ROL.PER" & vbCrLf
sConsulta = sConsulta & "        WHERE P.COD = @PETICIONARIO AND PM_COPIA_ROL.TIPO = 4 AND PM_COPIA_ROL.INSTANCIA = @ID)=0" & vbCrLf
sConsulta = sConsulta & "      BEGIN" & vbCrLf
sConsulta = sConsulta & "          --Actualizo los roles de la UON y DEP de la persona peticionaria" & vbCrLf
sConsulta = sConsulta & "       UPDATE PM_COPIA_ROL  SET PER = @PETICIONARIO,PER_ORIGINAL = P.COD FROM PER P WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "         WHERE ISNULL(P.UON1,'') = ISNULL(PM_COPIA_ROL.UON1,'') AND ISNULL(P.UON2,'') = ISNULL(PM_COPIA_ROL.UON2,'')" & vbCrLf
sConsulta = sConsulta & "           AND ISNULL(P.UON3,'') = ISNULL(PM_COPIA_ROL.UON3,'') AND P.DEP=PM_COPIA_ROL.DEP" & vbCrLf
sConsulta = sConsulta & "           AND P.COD IN (SELECT PER FROM USU WITH(NOLOCK) WHERE FSWS_SUSTITUCION=@PETICIONARIO)" & vbCrLf
sConsulta = sConsulta & "           AND PM_COPIA_ROL.TIPO = 4 AND PM_COPIA_ROL.INSTANCIA = @ID AND PM_COPIA_ROL.PER IS NULL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       IF @@ROWCOUNT = 0 --Actualizo los roles de la UON  de la persona peticionaria, no hay DEP en el rol" & vbCrLf
sConsulta = sConsulta & "          BEGIN" & vbCrLf
sConsulta = sConsulta & "          UPDATE PM_COPIA_ROL   SET PER = @PETICIONARIO,PER_ORIGINAL = P.COD   FROM PER P WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "          WHERE ISNULL(P.UON1,'') = ISNULL(PM_COPIA_ROL.UON1,'') AND ISNULL(P.UON2,'') = ISNULL(PM_COPIA_ROL.UON2,'')" & vbCrLf
sConsulta = sConsulta & "            AND ISNULL(P.UON3,'') = ISNULL(PM_COPIA_ROL.UON3,'') AND PM_COPIA_ROL.DEP IS NULL" & vbCrLf
sConsulta = sConsulta & "            AND P.COD IN (SELECT PER FROM USU WITH(NOLOCK) WHERE FSWS_SUSTITUCION=@PETICIONARIO)" & vbCrLf
sConsulta = sConsulta & "            AND PM_COPIA_ROL.TIPO = 4 AND PM_COPIA_ROL.INSTANCIA = @ID AND  PM_COPIA_ROL.PER IS NULL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "          IF @@ROWCOUNT = 0 --Actualizo los roles de la UON  de la persona peticionaria, no hay DEP en el rol ni UON3" & vbCrLf
sConsulta = sConsulta & "            BEGIN" & vbCrLf
sConsulta = sConsulta & "              UPDATE PM_COPIA_ROL   SET PER = @PETICIONARIO,PER_ORIGINAL = P.COD   FROM PER P WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "              WHERE ISNULL(P.UON1,'') = ISNULL(PM_COPIA_ROL.UON1,'') AND ISNULL(P.UON2,'') = ISNULL(PM_COPIA_ROL.UON2,'')" & vbCrLf
sConsulta = sConsulta & "              AND PM_COPIA_ROL.UON3 IS NULL AND PM_COPIA_ROL.DEP IS NULL" & vbCrLf
sConsulta = sConsulta & "              AND P.COD IN (SELECT PER FROM USU WITH(NOLOCK) WHERE FSWS_SUSTITUCION=@PETICIONARIO)" & vbCrLf
sConsulta = sConsulta & "              AND PM_COPIA_ROL.TIPO = 4 AND PM_COPIA_ROL.INSTANCIA = @ID AND  PM_COPIA_ROL.PER IS NULL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "              IF @@ROWCOUNT = 0 --Actualizo los roles de la UON  de la persona peticionaria, no hay DEP en el rol ni UON3" & vbCrLf
sConsulta = sConsulta & "                 BEGIN" & vbCrLf
sConsulta = sConsulta & "                 UPDATE PM_COPIA_ROL   SET PER = @PETICIONARIO,PER_ORIGINAL = P.COD   FROM PER P WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                 WHERE ISNULL(P.UON1,'') = ISNULL(PM_COPIA_ROL.UON1,'') AND PM_COPIA_ROL.UON2 IS NULL" & vbCrLf
sConsulta = sConsulta & "                 AND PM_COPIA_ROL.UON3 IS NULL AND PM_COPIA_ROL.DEP IS NULL" & vbCrLf
sConsulta = sConsulta & "                 AND P.COD IN (SELECT PER FROM USU WITH(NOLOCK) WHERE FSWS_SUSTITUCION=@PETICIONARIO)" & vbCrLf
sConsulta = sConsulta & "                 AND PM_COPIA_ROL.TIPO = 4 AND PM_COPIA_ROL.INSTANCIA = @ID AND  PM_COPIA_ROL.PER IS NULL" & vbCrLf
sConsulta = sConsulta & "                 IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "                 END" & vbCrLf
sConsulta = sConsulta & "              END" & vbCrLf
sConsulta = sConsulta & "          END" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @ENTRATAMIENTO=1" & vbCrLf
sConsulta = sConsulta & "       SET @GRABADO=(SELECT COUNT(1) FROM PM_COPIA_PARTICIPANTES  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PM_PARTICIPANTES   WITH (NOLOCK) ON PM_PARTICIPANTES.ID = PM_COPIA_PARTICIPANTES.ID" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PM_COPIA_ROL   WITH (NOLOCK) ON PM_COPIA_PARTICIPANTES.ROL = PM_COPIA_ROL.ID" & vbCrLf
sConsulta = sConsulta & "           WHERE PM_COPIA_ROL.INSTANCIA=@ID )" & vbCrLf
sConsulta = sConsulta & "   IF @GRABADO=0" & vbCrLf
sConsulta = sConsulta & "   BEGIN   " & vbCrLf
sConsulta = sConsulta & "       IF (SELECT COUNT(*) FROM PM_PARTICIPANTES P  WITH (NOLOCK) INNER JOIN PM_ROL R  WITH (NOLOCK) ON P.ROL=R.ID WHERE R.WORKFLOW=@WORKFLOW) >0" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           INSERT INTO PM_COPIA_PARTICIPANTES(ID,ROL, PER,GMN1,GMN2,GMN3,GMN4)" & vbCrLf
sConsulta = sConsulta & "           SELECT PM_PARTICIPANTES.ID,PM_COPIA_ROL.ID, PM_PARTICIPANTES.PER,PM_PARTICIPANTES.GMN1,PM_PARTICIPANTES.GMN2,PM_PARTICIPANTES.GMN3,PM_PARTICIPANTES.GMN4 " & vbCrLf
sConsulta = sConsulta & "           FROM PM_PARTICIPANTES  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PM_COPIA_ROL   WITH (NOLOCK) ON PM_PARTICIPANTES.ROL = PM_COPIA_ROL.ROL_ORIGEN" & vbCrLf
sConsulta = sConsulta & "           WHERE PM_COPIA_ROL.INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "           IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "       END " & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   IF @ENTRATAMIENTO=1" & vbCrLf
sConsulta = sConsulta & "       SET @GRABADO=(SELECT COUNT(1)  FROM PM_COPIA_ROL_BLOQUE  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PM_COPIA_BLOQUE  WITH (NOLOCK) ON PM_COPIA_ROL_BLOQUE.BLOQUE=PM_COPIA_BLOQUE.ID" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PM_COPIA_ROL  WITH (NOLOCK)  ON PM_COPIA_ROL_BLOQUE.ROL=PM_COPIA_ROL.ID" & vbCrLf
sConsulta = sConsulta & "           WHERE PM_COPIA_BLOQUE.INSTANCIA=@ID AND PM_COPIA_ROL.INSTANCIA=@ID )" & vbCrLf
sConsulta = sConsulta & "   IF @GRABADO=0" & vbCrLf
sConsulta = sConsulta & "   BEGIN   " & vbCrLf
sConsulta = sConsulta & "       INSERT INTO PM_COPIA_ROL_BLOQUE(ROL,BLOQUE)" & vbCrLf
sConsulta = sConsulta & "       SELECT PM_COPIA_ROL.ID,PM_COPIA_BLOQUE.ID FROM PM_ROL_BLOQUE  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PM_COPIA_BLOQUE  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "       ON PM_ROL_BLOQUE.BLOQUE=PM_COPIA_BLOQUE.BLOQUE_ORIGEN" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PM_COPIA_ROL  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "       ON PM_ROL_BLOQUE.ROL=PM_COPIA_ROL.ROL_ORIGEN" & vbCrLf
sConsulta = sConsulta & "       WHERE PM_COPIA_BLOQUE.INSTANCIA=@ID AND PM_COPIA_ROL.INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "       IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   --Aqui copiamos los listados para cada rol y bloque " & vbCrLf
sConsulta = sConsulta & "   IF @ENTRATAMIENTO=1" & vbCrLf
sConsulta = sConsulta & "       SET @GRABADO=(SELECT COUNT(1) FROM PM_COPIA_ROL_LISPER WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PM_COPIA_BLOQUE B WITH (NOLOCK) ON PM_COPIA_ROL_LISPER.BLOQUE=B.ID" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PM_COPIA_ROL R   WITH (NOLOCK) ON PM_COPIA_ROL_LISPER.ROL=R.ID " & vbCrLf
sConsulta = sConsulta & "           WHERE  B.INSTANCIA=@ID AND R.INSTANCIA=@ID )" & vbCrLf
sConsulta = sConsulta & "   IF @GRABADO=0" & vbCrLf
sConsulta = sConsulta & "   BEGIN   " & vbCrLf
sConsulta = sConsulta & "       INSERT INTO PM_COPIA_ROL_LISPER(ROL,BLOQUE,LISPER)" & vbCrLf
sConsulta = sConsulta & "       SELECT R.ID,B.ID,L.LISPER FROM PM_ROL_LISPER L WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PM_COPIA_BLOQUE B WITH (NOLOCK) ON L.BLOQUE=B.BLOQUE_ORIGEN" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PM_COPIA_ROL R   WITH (NOLOCK) ON L.ROL=R.ROL_ORIGEN " & vbCrLf
sConsulta = sConsulta & "       WHERE  B.INSTANCIA=@ID and R.INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "       IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @ENTRATAMIENTO=1" & vbCrLf
sConsulta = sConsulta & "       SET @GRABADO=(SELECT COUNT(1) FROM PM_COPIA_ROL_LISPER_DEN WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PM_COPIA_BLOQUE B WITH (NOLOCK) ON PM_COPIA_ROL_LISPER_DEN.BLOQUE=B.ID" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PM_COPIA_ROL R WITH (NOLOCK) ON PM_COPIA_ROL_LISPER_DEN.ROL=R.ID " & vbCrLf
sConsulta = sConsulta & "           WHERE  B.INSTANCIA=@ID and R.INSTANCIA=@ID )" & vbCrLf
sConsulta = sConsulta & "   IF @GRABADO=0" & vbCrLf
sConsulta = sConsulta & "   BEGIN   " & vbCrLf
sConsulta = sConsulta & "       INSERT INTO PM_COPIA_ROL_LISPER_DEN(ROL,BLOQUE,LISPER,IDI,ARCHIVO_RPT,DEN)" & vbCrLf
sConsulta = sConsulta & "       SELECT R.ID, B.ID, LD.LISPER, LD.IDI, LD.ARCHIVO_RPT, LD.DEN FROM PM_ROL_LISPER_DEN LD  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PM_COPIA_BLOQUE B WITH (NOLOCK) ON LD.BLOQUE=B.BLOQUE_ORIGEN" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PM_COPIA_ROL R WITH (NOLOCK) ON LD.ROL=R.ROL_ORIGEN " & vbCrLf
sConsulta = sConsulta & "       WHERE  B.INSTANCIA=@ID and R.INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "       IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @ENTRATAMIENTO=1" & vbCrLf
sConsulta = sConsulta & "       SET @GRABADO=(SELECT COUNT(1)  FROM PM_COPIA_ROL_ACCION  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PM_COPIA_BLOQUE B   WITH (NOLOCK) ON PM_COPIA_ROL_ACCION.BLOQUE=B.ID" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PM_COPIA_ROL R   WITH (NOLOCK) ON PM_COPIA_ROL_ACCION.ROL=R.ID " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PM_COPIA_ACCIONES A   WITH (NOLOCK) ON PM_COPIA_ROL_ACCION.ACCION=A.ID AND A.BLOQUE=B.ID" & vbCrLf
sConsulta = sConsulta & "           WHERE  B.INSTANCIA=@ID and R.INSTANCIA=@ID )" & vbCrLf
sConsulta = sConsulta & "   IF @GRABADO=0" & vbCrLf
sConsulta = sConsulta & "   BEGIN       " & vbCrLf
sConsulta = sConsulta & "       INSERT INTO PM_COPIA_ROL_ACCION(ROL,BLOQUE,ACCION,APROBAR,RECHAZAR)" & vbCrLf
sConsulta = sConsulta & "       SELECT R.ID,B.ID,A.ID, PM_ROL_ACCION.APROBAR, PM_ROL_ACCION.RECHAZAR FROM PM_ROL_ACCION  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PM_COPIA_BLOQUE B   WITH (NOLOCK) ON PM_ROL_ACCION.BLOQUE=B.BLOQUE_ORIGEN" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PM_COPIA_ROL R   WITH (NOLOCK) ON PM_ROL_ACCION.ROL=R.ROL_ORIGEN " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PM_COPIA_ACCIONES A   WITH (NOLOCK) ON PM_ROL_ACCION.ACCION=A.ACCION_ORIGEN AND A.BLOQUE=B.ID" & vbCrLf
sConsulta = sConsulta & "       WHERE  B.INSTANCIA=@ID and R.INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "       IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   --Aqui deben ir las precondiciones" & vbCrLf
sConsulta = sConsulta & "   IF @ENTRATAMIENTO=1" & vbCrLf
sConsulta = sConsulta & "       SET @GRABADO=(SELECT COUNT(1) FROM PM_COPIA_ACCION_PRECOND  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PM_COPIA_ACCIONES CA  WITH (NOLOCK)   ON PM_COPIA_ACCION_PRECOND.ACCION = CA.ID " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PM_COPIA_BLOQUE B  WITH (NOLOCK)   ON CA.BLOQUE=B.ID" & vbCrLf
sConsulta = sConsulta & "           WHERE  B.INSTANCIA=@ID )" & vbCrLf
sConsulta = sConsulta & "   IF @GRABADO=0" & vbCrLf
sConsulta = sConsulta & "   BEGIN   " & vbCrLf
sConsulta = sConsulta & "       INSERT INTO PM_COPIA_ACCION_PRECOND(ORDEN,COD,ACCION,TIPO,FORMULA,PRECOND_ORIGEN)" & vbCrLf
sConsulta = sConsulta & "       SELECT AP.ORDEN, AP.COD, CA.ID, AP.TIPO, AP.FORMULA, AP.ID FROM PM_ACCION_PRECOND AP  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PM_COPIA_ACCIONES CA  WITH (NOLOCK)    ON AP.ACCION = CA.ACCION_ORIGEN " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PM_COPIA_BLOQUE B  WITH (NOLOCK)   ON CA.BLOQUE=B.ID" & vbCrLf
sConsulta = sConsulta & "       WHERE  B.INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "       IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @ENTRATAMIENTO=1" & vbCrLf
sConsulta = sConsulta & "       SET @GRABADO=(SELECT COUNT(1) FROM PM_COPIA_ACCION_PRECOND_DEN  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PM_COPIA_ACCION_PRECOND CAP  WITH (NOLOCK)    ON PM_COPIA_ACCION_PRECOND_DEN.ACCION_PRECOND = CAP.ID" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PM_COPIA_ACCIONES CA    WITH (NOLOCK) ON CAP.ACCION = CA.ID" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PM_COPIA_BLOQUE B  WITH (NOLOCK)  ON CA.BLOQUE=B.ID" & vbCrLf
sConsulta = sConsulta & "           WHERE B.INSTANCIA=@ID )" & vbCrLf
sConsulta = sConsulta & "   IF @GRABADO=0" & vbCrLf
sConsulta = sConsulta & "   BEGIN       " & vbCrLf
sConsulta = sConsulta & "       INSERT INTO PM_COPIA_ACCION_PRECOND_DEN(ACCION_PRECOND, IDI, DEN)" & vbCrLf
sConsulta = sConsulta & "       SELECT CAP.ID, APD.IDI, APD.DEN  FROM PM_ACCION_PRECOND_DEN APD  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PM_COPIA_ACCION_PRECOND CAP  WITH (NOLOCK)    ON APD.ACCION_PRECOND = CAP.PRECOND_ORIGEN" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PM_COPIA_ACCIONES CA    WITH (NOLOCK) ON CAP.ACCION = CA.ID" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PM_COPIA_BLOQUE B  WITH (NOLOCK)  ON CA.BLOQUE=B.ID" & vbCrLf
sConsulta = sConsulta & "       WHERE B.INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "       IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @ENTRATAMIENTO=1" & vbCrLf
sConsulta = sConsulta & "       SET @GRABADO=(SELECT COUNT(1)   FROM PM_COPIA_ACCION_CONDICIONES   WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PM_COPIA_ACCION_PRECOND CAP    WITH (NOLOCK)      ON PM_COPIA_ACCION_CONDICIONES.ACCION_PRECOND=CAP.ID" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PM_COPIA_ACCIONES CA                   WITH (NOLOCK)         ON CAP.ACCION = CA.ID" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PM_COPIA_BLOQUE CB                     WITH (NOLOCK)          ON CA.BLOQUE=CB.ID " & vbCrLf
sConsulta = sConsulta & "           WHERE CB.INSTANCIA=@ID  )" & vbCrLf
sConsulta = sConsulta & "   IF @GRABADO=0" & vbCrLf
sConsulta = sConsulta & "   BEGIN       " & vbCrLf
sConsulta = sConsulta & "       INSERT INTO PM_COPIA_ACCION_CONDICIONES(ACCION_PRECOND,COD,TIPO_CAMPO,CAMPO,OPERADOR,TIPO_VALOR,CAMPO_VALOR,VALOR_TEXT,VALOR_NUM,VALOR_FEC,VALOR_BOOL)" & vbCrLf
sConsulta = sConsulta & "       SELECT CAP.ID,AC.COD,AC.TIPO_CAMPO,CAMPO_DEF.ID,AC.OPERADOR,AC.TIPO_VALOR,CAMPO_DEF2.ID,AC.VALOR_TEXT,AC.VALOR_NUM,AC.VALOR_FEC,AC.VALOR_BOOL" & vbCrLf
sConsulta = sConsulta & "       FROM PM_ACCION_CONDICIONES AC   WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PM_COPIA_ACCION_PRECOND CAP    WITH (NOLOCK)      ON AC.ACCION_PRECOND=CAP.PRECOND_ORIGEN" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PM_COPIA_ACCIONES CA                   WITH (NOLOCK)         ON CAP.ACCION = CA.ID" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PM_COPIA_BLOQUE CB                     WITH (NOLOCK)          ON CA.BLOQUE=CB.ID " & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN (SELECT COPIA_CAMPO_DEF.ID,COPIA_CAMPO_DEF.CAMPO_ORIGEN" & vbCrLf
sConsulta = sConsulta & "           FROM COPIA_CAMPO_DEF  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN COPIA_GRUPO  WITH (NOLOCK)   ON COPIA_CAMPO_DEF.GRUPO =COPIA_GRUPO.ID AND COPIA_GRUPO.INSTANCIA=@ID) CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "       ON AC.CAMPO=CAMPO_DEF.CAMPO_ORIGEN" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN (SELECT COPIA_CAMPO_DEF.ID,COPIA_CAMPO_DEF.CAMPO_ORIGEN" & vbCrLf
sConsulta = sConsulta & "           FROM COPIA_CAMPO_DEF  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN COPIA_GRUPO   WITH (NOLOCK) ON COPIA_CAMPO_DEF.GRUPO =COPIA_GRUPO.ID AND COPIA_GRUPO.INSTANCIA=@ID) CAMPO_DEF2" & vbCrLf
sConsulta = sConsulta & "       ON AC.CAMPO_VALOR=CAMPO_DEF2.CAMPO_ORIGEN" & vbCrLf
sConsulta = sConsulta & "       WHERE CB.INSTANCIA=@ID " & vbCrLf
sConsulta = sConsulta & "       IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   IF @ENTRATAMIENTO=1" & vbCrLf
sConsulta = sConsulta & "       SET @GRABADO=(SELECT COUNT(1)   FROM PM_COPIA_ENLACE   WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PM_COPIA_BLOQUE B1    WITH (NOLOCK)   ON PM_COPIA_ENLACE.BLOQUE_ORIGEN=B1.ID " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PM_COPIA_BLOQUE B2    WITH (NOLOCK)   ON PM_COPIA_ENLACE.BLOQUE_DESTINO=B2.ID" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PM_COPIA_ACCIONES      WITH (NOLOCK)   ON PM_COPIA_ENLACE.ACCION=PM_COPIA_ACCIONES.ID " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PM_COPIA_BLOQUE B3    WITH (NOLOCK)   ON PM_COPIA_ACCIONES.BLOQUE=B3.ID " & vbCrLf
sConsulta = sConsulta & "           WHERE B1.INSTANCIA=@ID AND B2.INSTANCIA=@ID AND B3.INSTANCIA=@ID )" & vbCrLf
sConsulta = sConsulta & "   IF @GRABADO=0" & vbCrLf
sConsulta = sConsulta & "   BEGIN   " & vbCrLf
sConsulta = sConsulta & "       INSERT INTO PM_COPIA_ENLACE(BLOQUE_ORIGEN,BLOQUE_DESTINO,ACCION,FORMULA,BLOQUEA,ENLACE_ORIGEN)" & vbCrLf
sConsulta = sConsulta & "       SELECT B1.ID,B2.ID,PM_COPIA_ACCIONES.ID,FORMULA,BLOQUEA,PM_ENLACE.ID" & vbCrLf
sConsulta = sConsulta & "       FROM PM_ENLACE   WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PM_COPIA_BLOQUE B1      WITH (NOLOCK)   ON PM_ENLACE.BLOQUE_ORIGEN=B1.BLOQUE_ORIGEN " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PM_COPIA_BLOQUE B2     WITH (NOLOCK)     ON PM_ENLACE.BLOQUE_DESTINO=B2.BLOQUE_ORIGEN" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PM_COPIA_ACCIONES      WITH (NOLOCK)      ON PM_ENLACE.ACCION=PM_COPIA_ACCIONES.ACCION_ORIGEN " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PM_COPIA_BLOQUE B3       WITH (NOLOCK)    ON PM_COPIA_ACCIONES.BLOQUE=B3.ID " & vbCrLf
sConsulta = sConsulta & "       WHERE B1.INSTANCIA=@ID AND B2.INSTANCIA=@ID AND B3.INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "       IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @ENTRATAMIENTO=1" & vbCrLf
sConsulta = sConsulta & "       SET @GRABADO=(SELECT COUNT(1)   FROM PM_COPIA_ENLACE_EMAIL WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PM_COPIA_ENLACE E WITH (NOLOCK) ON PM_COPIA_ENLACE_EMAIL.ENLACE=E.ID" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PM_COPIA_BLOQUE B WITH (NOLOCK) ON B.ID=E.BLOQUE_ORIGEN" & vbCrLf
sConsulta = sConsulta & "       WHERE B.INSTANCIA=@ID )" & vbCrLf
sConsulta = sConsulta & "   IF @GRABADO=0" & vbCrLf
sConsulta = sConsulta & "   BEGIN   " & vbCrLf
sConsulta = sConsulta & "       INSERT INTO PM_COPIA_ENLACE_EMAIL (ENLACE,IDI,SUBJECT)" & vbCrLf
sConsulta = sConsulta & "       SELECT E.ID, EE.IDI, EE.SUBJECT" & vbCrLf
sConsulta = sConsulta & "       FROM PM_ENLACE_EMAIL EE WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PM_COPIA_ENLACE E WITH (NOLOCK) ON E.ENLACE_ORIGEN=EE.ENLACE" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PM_COPIA_BLOQUE B WITH (NOLOCK) ON B.ID=E.BLOQUE_ORIGEN" & vbCrLf
sConsulta = sConsulta & "       WHERE B.INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "       IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @ENTRATAMIENTO=1" & vbCrLf
sConsulta = sConsulta & "       SET @GRABADO=(SELECT COUNT(1)   FROM PM_COPIA_ENLACE_EXTRAPOINTS  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PM_COPIA_ENLACE E      WITH (NOLOCK)   ON PM_COPIA_ENLACE_EXTRAPOINTS.ENLACE=E.ID" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PM_COPIA_BLOQUE CB   WITH (NOLOCK)   ON E.BLOQUE_ORIGEN=CB.ID " & vbCrLf
sConsulta = sConsulta & "           WHERE CB.INSTANCIA=@ID )" & vbCrLf
sConsulta = sConsulta & "   IF @GRABADO=0" & vbCrLf
sConsulta = sConsulta & "   BEGIN       " & vbCrLf
sConsulta = sConsulta & "       INSERT INTO PM_COPIA_ENLACE_EXTRAPOINTS(ENLACE, ORDEN, X,Y)" & vbCrLf
sConsulta = sConsulta & "       SELECT E.ID, ORDEN, X,Y" & vbCrLf
sConsulta = sConsulta & "       FROM PM_ENLACE_EXTRAPOINTS X  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PM_COPIA_ENLACE E      WITH (NOLOCK)   ON X.ENLACE=E.ENLACE_ORIGEN" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PM_COPIA_BLOQUE CB   WITH (NOLOCK)   ON E.BLOQUE_ORIGEN=CB.ID " & vbCrLf
sConsulta = sConsulta & "       WHERE CB.INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "       IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @ENTRATAMIENTO=1" & vbCrLf
sConsulta = sConsulta & "       SET @GRABADO=(SELECT COUNT(1)   FROM PM_COPIA_ENLACE_CONDICION   WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PM_COPIA_ENLACE CE  WITH (NOLOCK) ON PM_COPIA_ENLACE_CONDICION.ENLACE=CE.ID" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PM_COPIA_BLOQUE CB  WITH (NOLOCK) ON CE.BLOQUE_ORIGEN=CB.ID " & vbCrLf
sConsulta = sConsulta & "           WHERE CB.INSTANCIA=@ID )" & vbCrLf
sConsulta = sConsulta & "   IF @GRABADO=0" & vbCrLf
sConsulta = sConsulta & "   BEGIN       " & vbCrLf
sConsulta = sConsulta & "       INSERT INTO PM_COPIA_ENLACE_CONDICION(ENLACE,COD,TIPO_CAMPO,CAMPO,OPERADOR,TIPO_VALOR,CAMPO_VALOR,VALOR_TEXT,VALOR_NUM,VALOR_FEC,VALOR_BOOL)" & vbCrLf
sConsulta = sConsulta & "       SELECT CE.ID,COD,TIPO_CAMPO,CAMPO_DEF.ID,OPERADOR,TIPO_VALOR,CAMPO_DEF2.ID,VALOR_TEXT,VALOR_NUM,VALOR_FEC,VALOR_BOOL" & vbCrLf
sConsulta = sConsulta & "       FROM PM_ENLACE_CONDICIONES   WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PM_COPIA_ENLACE CE  WITH (NOLOCK) ON PM_ENLACE_CONDICIONES.ENLACE=CE.ENLACE_ORIGEN" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PM_COPIA_BLOQUE CB  WITH (NOLOCK) ON CE.BLOQUE_ORIGEN=CB.ID " & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN (SELECT COPIA_CAMPO_DEF.ID,COPIA_CAMPO_DEF.CAMPO_ORIGEN FROM COPIA_CAMPO_DEF   WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN COPIA_GRUPO   WITH (NOLOCK)  ON COPIA_CAMPO_DEF.GRUPO =COPIA_GRUPO.ID AND COPIA_GRUPO.INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "       ) CAMPO_DEF ON PM_ENLACE_CONDICIONES.CAMPO=CAMPO_DEF.CAMPO_ORIGEN" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN (SELECT COPIA_CAMPO_DEF.ID,COPIA_CAMPO_DEF.CAMPO_ORIGEN FROM COPIA_CAMPO_DEF   WITH (NOLOCK)  " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN COPIA_GRUPO   WITH (NOLOCK)  ON COPIA_CAMPO_DEF.GRUPO =COPIA_GRUPO.ID AND COPIA_GRUPO.INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "       ) CAMPO_DEF2 ON PM_ENLACE_CONDICIONES.CAMPO_VALOR=CAMPO_DEF2.CAMPO_ORIGEN" & vbCrLf
sConsulta = sConsulta & "       WHERE CB.INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "       IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @ENTRATAMIENTO=1" & vbCrLf
sConsulta = sConsulta & "       SET @GRABADO=(SELECT COUNT(1)   FROM PM_COPIA_CONF_CUMP_BLOQUE  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PM_COPIA_BLOQUE CB  WITH (NOLOCK)    ON PM_COPIA_CONF_CUMP_BLOQUE.BLOQUE=CB.ID" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PM_COPIA_ROL CR  WITH (NOLOCK)  ON PM_COPIA_CONF_CUMP_BLOQUE.ROL=CR.ID" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN COPIA_CAMPO_DEF CCD  WITH (NOLOCK)  ON PM_COPIA_CONF_CUMP_BLOQUE.CAMPO=CCD.ID" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN COPIA_GRUPO  WITH (NOLOCK) ON CCD.GRUPO=COPIA_GRUPO.ID AND COPIA_GRUPO.INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "           WHERE  CB.INSTANCIA=@ID  AND CR.INSTANCIA=@ID)" & vbCrLf
sConsulta = sConsulta & "   IF @GRABADO=0" & vbCrLf
sConsulta = sConsulta & "   BEGIN   " & vbCrLf
sConsulta = sConsulta & "       INSERT INTO PM_COPIA_CONF_CUMP_BLOQUE(BLOQUE,ROL,CAMPO,VISIBLE,ESCRITURA,OBLIGATORIO,ORDEN,ESCRITURA_FORMULA) " & vbCrLf
sConsulta = sConsulta & "       SELECT CB.ID,CR.ID,CCD.ID,PM_CONF_CUMP_BLOQUE.VISIBLE,PM_CONF_CUMP_BLOQUE.ESCRITURA,PM_CONF_CUMP_BLOQUE.OBLIGATORIO,PM_CONF_CUMP_BLOQUE.ORDEN,PM_CONF_CUMP_BLOQUE.ESCRITURA_FORMULA" & vbCrLf
sConsulta = sConsulta & "       FROM PM_CONF_CUMP_BLOQUE  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PM_COPIA_BLOQUE CB  WITH (NOLOCK)    ON PM_CONF_CUMP_BLOQUE.BLOQUE=CB.BLOQUE_ORIGEN" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PM_COPIA_ROL CR  WITH (NOLOCK)  ON PM_CONF_CUMP_BLOQUE.ROL=CR.ROL_ORIGEN" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN COPIA_CAMPO_DEF CCD  WITH (NOLOCK)  ON PM_CONF_CUMP_BLOQUE.CAMPO=CCD.CAMPO_ORIGEN" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN COPIA_GRUPO  WITH (NOLOCK) ON CCD.GRUPO=COPIA_GRUPO.ID AND COPIA_GRUPO.INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "       WHERE  CB.INSTANCIA=@ID  AND CR.INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "       IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @ENTRATAMIENTO=1" & vbCrLf
sConsulta = sConsulta & "       SET @GRABADO=(SELECT COUNT(1) FROM PM_COPIA_CONF_CUMP_BLOQUE_LISTA  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PM_COPIA_BLOQUE CB  WITH (NOLOCK)    ON PM_COPIA_CONF_CUMP_BLOQUE_LISTA.BLOQUE=CB.ID" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PM_COPIA_ROL CR  WITH (NOLOCK) ON PM_COPIA_CONF_CUMP_BLOQUE_LISTA.ROL=CR.ID" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN COPIA_CAMPO_DEF CCD  WITH (NOLOCK) ON PM_COPIA_CONF_CUMP_BLOQUE_LISTA.CAMPO=CCD.ID" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN COPIA_GRUPO  WITH (NOLOCK) ON CCD.GRUPO=COPIA_GRUPO.ID AND COPIA_GRUPO.INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "           WHERE  CB.INSTANCIA=@ID  AND CR.INSTANCIA=@ID)" & vbCrLf
sConsulta = sConsulta & "   IF @GRABADO=0" & vbCrLf
sConsulta = sConsulta & "   BEGIN   " & vbCrLf
sConsulta = sConsulta & "       INSERT INTO PM_COPIA_CONF_CUMP_BLOQUE_LISTA(BLOQUE, ROL, CAMPO, ID, VISIBLE, VALOR_TEXT, VALOR_NUM, VALOR_FEC, ORDEN) " & vbCrLf
sConsulta = sConsulta & "       SELECT CB.ID, CR.ID, CCD.ID, CBL.ID, CBL.VISIBLE, CBL.VALOR_TEXT, CBL.VALOR_NUM, CBL.VALOR_FEC, CBL.ORDEN " & vbCrLf
sConsulta = sConsulta & "       FROM PM_CONF_CUMP_BLOQUE_LISTA CBL  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PM_COPIA_BLOQUE CB  WITH (NOLOCK)    ON CBL.BLOQUE=CB.BLOQUE_ORIGEN" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PM_COPIA_ROL CR  WITH (NOLOCK) ON CBL.ROL=CR.ROL_ORIGEN" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN COPIA_CAMPO_DEF CCD  WITH (NOLOCK) ON CBL.CAMPO=CCD.CAMPO_ORIGEN" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN COPIA_GRUPO  WITH (NOLOCK) ON CCD.GRUPO=COPIA_GRUPO.ID AND COPIA_GRUPO.INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "       WHERE  CB.INSTANCIA=@ID  AND CR.INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "       IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @ENTRATAMIENTO=1" & vbCrLf
sConsulta = sConsulta & "       SET @GRABADO=(SELECT COUNT(1) FROM PM_COPIA_CONF_CUMP_BLOQUE_COND  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PM_COPIA_BLOQUE CB  WITH (NOLOCK) ON PM_COPIA_CONF_CUMP_BLOQUE_COND.BLOQUE=CB.ID" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PM_COPIA_ROL CR  WITH (NOLOCK) ON PM_COPIA_CONF_CUMP_BLOQUE_COND.ROL=CR.ID" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN COPIA_CAMPO_DEF CCD  WITH (NOLOCK) ON PM_COPIA_CONF_CUMP_BLOQUE_COND.CAMPO=CCD.ID" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN COPIA_GRUPO  WITH (NOLOCK) ON CCD.GRUPO=COPIA_GRUPO.ID AND COPIA_GRUPO.INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "           WHERE  CB.INSTANCIA=@ID AND CR.INSTANCIA=@ID )" & vbCrLf
sConsulta = sConsulta & "   IF @GRABADO=0" & vbCrLf
sConsulta = sConsulta & "   BEGIN   " & vbCrLf
sConsulta = sConsulta & "       INSERT INTO PM_COPIA_CONF_CUMP_BLOQUE_COND(BLOQUE,ROL,CAMPO,COD,TIPO_CAMPO,CAMPO_DATO,OPERADOR,TIPO_VALOR, CAMPO_VALOR,VALOR_TEXT, VALOR_FEC,VALOR_BOOL, VALOR_NUM) " & vbCrLf
sConsulta = sConsulta & "       SELECT CB.ID,CR.ID,CCD.ID,CCBC.COD,CCBC.TIPO_CAMPO,CCD2.ID CAMPO_DATO,CCBC.OPERADOR,CCBC.TIPO_VALOR, CCD3.ID CAMPO_VALOR,CCBC.VALOR_TEXT,CCBC.VALOR_FEC,CCBC.VALOR_BOOL,CCBC.VALOR_NUM" & vbCrLf
sConsulta = sConsulta & "       FROM PM_CONF_CUMP_BLOQUE_COND CCBC  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PM_COPIA_BLOQUE CB  WITH (NOLOCK) ON CCBC.BLOQUE=CB.BLOQUE_ORIGEN" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PM_COPIA_ROL CR  WITH (NOLOCK) ON CCBC.ROL=CR.ROL_ORIGEN" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN COPIA_CAMPO_DEF CCD  WITH (NOLOCK) ON CCBC.CAMPO=CCD.CAMPO_ORIGEN" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN COPIA_GRUPO  WITH (NOLOCK) ON CCD.GRUPO=COPIA_GRUPO.ID AND COPIA_GRUPO.INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN (SELECT COPIA_CAMPO_DEF.ID,COPIA_CAMPO_DEF.CAMPO_ORIGEN FROM COPIA_CAMPO_DEF  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN COPIA_GRUPO   WITH (NOLOCK) ON COPIA_CAMPO_DEF.GRUPO =COPIA_GRUPO.ID AND COPIA_GRUPO.INSTANCIA=@ID) CCD2 ON CCBC.CAMPO_DATO=CCD2.CAMPO_ORIGEN" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN (SELECT COPIA_CAMPO_DEF.ID,COPIA_CAMPO_DEF.CAMPO_ORIGEN FROM COPIA_CAMPO_DEF  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN COPIA_GRUPO   WITH (NOLOCK) ON COPIA_CAMPO_DEF.GRUPO =COPIA_GRUPO.ID AND COPIA_GRUPO.INSTANCIA=@ID) CCD3 ON CCBC.CAMPO_VALOR=CCD3.CAMPO_ORIGEN" & vbCrLf
sConsulta = sConsulta & "       WHERE  CB.INSTANCIA=@ID  AND CR.INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "       IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @ENTRATAMIENTO=1" & vbCrLf
sConsulta = sConsulta & "       SET @GRABADO=(SELECT COUNT(1) FROM PM_COPIA_NOTIFICADO_ACCION  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PM_COPIA_ACCIONES    CA   WITH (NOLOCK)  ON PM_COPIA_NOTIFICADO_ACCION.ACCION=CA.ID" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PM_COPIA_BLOQUE       CB    WITH (NOLOCK) ON CA.BLOQUE=CB.ID" & vbCrLf
sConsulta = sConsulta & "           WHERE CB.INSTANCIA=@ID )" & vbCrLf
sConsulta = sConsulta & "   IF @GRABADO=0" & vbCrLf
sConsulta = sConsulta & "   BEGIN   " & vbCrLf
sConsulta = sConsulta & "       INSERT INTO PM_COPIA_NOTIFICADO_ACCION(ACCION, TIPO_NOTIFICADO, PER , ROL, NOTIFICADO_ORIGEN) " & vbCrLf
sConsulta = sConsulta & "       SELECT CA.ID,PM_NOTIFICADO_ACCION.TIPO_NOTIFICADO,PM_NOTIFICADO_ACCION.PER, CR.ID, PM_NOTIFICADO_ACCION.ID" & vbCrLf
sConsulta = sConsulta & "       FROM PM_NOTIFICADO_ACCION   WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PM_COPIA_ACCIONES    CA   WITH (NOLOCK)  ON PM_NOTIFICADO_ACCION.ACCION=CA.ACCION_ORIGEN" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PM_COPIA_BLOQUE       CB    WITH (NOLOCK) ON CA.BLOQUE=CB.ID" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN PM_COPIA_ROL                 CR   WITH (NOLOCK)  ON PM_NOTIFICADO_ACCION.ROL=CR.ROL_ORIGEN  AND CR.INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "       WHERE CB.INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "       IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @ENTRATAMIENTO=1" & vbCrLf
sConsulta = sConsulta & "       SET @GRABADO=(SELECT COUNT(1)  FROM PM_COPIA_NOTIFICADO_ENLACE  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PM_COPIA_ENLACE CE   WITH (NOLOCK)  ON PM_COPIA_NOTIFICADO_ENLACE.ENLACE=CE.ID" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PM_COPIA_BLOQUE CB   WITH (NOLOCK)  ON CE.BLOQUE_ORIGEN=CB.ID " & vbCrLf
sConsulta = sConsulta & "           WHERE CB.INSTANCIA=@ID )" & vbCrLf
sConsulta = sConsulta & "   IF @GRABADO=0" & vbCrLf
sConsulta = sConsulta & "   BEGIN       " & vbCrLf
sConsulta = sConsulta & "       INSERT INTO PM_COPIA_NOTIFICADO_ENLACE(ENLACE, TIPO_NOTIFICADO, PER , ROL, NOTIFICADO_ORIGEN) " & vbCrLf
sConsulta = sConsulta & "       SELECT CE.ID,PM_NOTIFICADO_ENLACE.TIPO_NOTIFICADO,PM_NOTIFICADO_ENLACE.PER, CR.ID, PM_NOTIFICADO_ENLACE.ID" & vbCrLf
sConsulta = sConsulta & "       FROM PM_NOTIFICADO_ENLACE  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PM_COPIA_ENLACE CE   WITH (NOLOCK)  ON PM_NOTIFICADO_ENLACE.ENLACE=CE.ENLACE_ORIGEN" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PM_COPIA_BLOQUE CB   WITH (NOLOCK)  ON CE.BLOQUE_ORIGEN=CB.ID " & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN PM_COPIA_ROL CR        WITH (NOLOCK)      ON PM_NOTIFICADO_ENLACE.ROL=CR.ROL_ORIGEN AND CR.INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "       WHERE CB.INSTANCIA=@ID    " & vbCrLf
sConsulta = sConsulta & "       IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   -- Campos visibles en notificados" & vbCrLf
sConsulta = sConsulta & "   IF @ENTRATAMIENTO=1" & vbCrLf
sConsulta = sConsulta & "       SET @GRABADO=(SELECT COUNT(1) FROM PM_COPIA_NOTIFICADO_ACCION_CAMPOS WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN COPIA_CAMPO_DEF CCD WITH (NOLOCK) ON CCD.ID=PM_COPIA_NOTIFICADO_ACCION_CAMPOS.CAMPO" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN COPIA_GRUPO CG WITH (NOLOCK) ON CG.ID=CCD.GRUPO" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PM_COPIA_NOTIFICADO_ACCION CNA WITH (NOLOCK) ON CNA.ID=PM_COPIA_NOTIFICADO_ACCION_CAMPOS.NOTIFICADO" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PM_COPIA_ACCIONES CA WITH (NOLOCK) ON CA.ID=CNA.ACCION" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PM_COPIA_BLOQUE CB WITH (NOLOCK) ON CB.ID=CA.BLOQUE AND CB.INSTANCIA=CG.INSTANCIA" & vbCrLf
sConsulta = sConsulta & "           WHERE CG.INSTANCIA=@ID )" & vbCrLf
sConsulta = sConsulta & "   IF @GRABADO=0" & vbCrLf
sConsulta = sConsulta & "   BEGIN   " & vbCrLf
sConsulta = sConsulta & "       INSERT INTO PM_COPIA_NOTIFICADO_ACCION_CAMPOS (CAMPO, NOTIFICADO, VISIBLE)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       SELECT CCD.ID, CNA.ID, NAC.VISIBLE" & vbCrLf
sConsulta = sConsulta & "       FROM PM_NOTIFICADO_ACCION_CAMPOS NAC WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN COPIA_CAMPO_DEF CCD WITH (NOLOCK) ON CCD.CAMPO_ORIGEN=NAC.CAMPO" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN COPIA_GRUPO CG WITH (NOLOCK) ON CG.ID=CCD.GRUPO" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PM_COPIA_NOTIFICADO_ACCION CNA WITH (NOLOCK) ON CNA.NOTIFICADO_ORIGEN=NAC.NOTIFICADO" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PM_COPIA_ACCIONES CA WITH (NOLOCK) ON CA.ID=CNA.ACCION" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PM_COPIA_BLOQUE CB WITH (NOLOCK) ON CB.ID=CA.BLOQUE AND CB.INSTANCIA=CG.INSTANCIA" & vbCrLf
sConsulta = sConsulta & "       WHERE CG.INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "       IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @ENTRATAMIENTO =1 " & vbCrLf
sConsulta = sConsulta & "       SET @GRABADO =  (SELECT COUNT(1)  FROM PM_COPIA_NOTIFICADO_ENLACE_CAMPOS WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN COPIA_CAMPO_DEF CCD WITH (NOLOCK) ON CCD.ID=PM_COPIA_NOTIFICADO_ENLACE_CAMPOS.CAMPO" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN COPIA_GRUPO CG WITH (NOLOCK) ON CG.ID=CCD.GRUPO" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PM_COPIA_NOTIFICADO_ENLACE CNE WITH (NOLOCK) ON CNE.ID=PM_COPIA_NOTIFICADO_ENLACE_CAMPOS.NOTIFICADO" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PM_COPIA_ENLACE CE WITH (NOLOCK) ON CE.ID=CNE.ENLACE" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PM_COPIA_BLOQUE CB WITH (NOLOCK) ON CB.ID=CE.BLOQUE_ORIGEN AND CB.INSTANCIA=CG.INSTANCIA" & vbCrLf
sConsulta = sConsulta & "           WHERE CG.INSTANCIA=@ID)" & vbCrLf
sConsulta = sConsulta & "   IF @GRABADO=0" & vbCrLf
sConsulta = sConsulta & "   BEGIN       " & vbCrLf
sConsulta = sConsulta & "       INSERT INTO PM_COPIA_NOTIFICADO_ENLACE_CAMPOS (CAMPO, NOTIFICADO, VISIBLE)" & vbCrLf
sConsulta = sConsulta & "       SELECT CCD.ID, CNE.ID, NEC.VISIBLE" & vbCrLf
sConsulta = sConsulta & "       FROM PM_NOTIFICADO_ENLACE_CAMPOS NEC WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN COPIA_CAMPO_DEF CCD WITH (NOLOCK) ON CCD.CAMPO_ORIGEN=NEC.CAMPO" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN COPIA_GRUPO CG WITH (NOLOCK) ON CG.ID=CCD.GRUPO" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PM_COPIA_NOTIFICADO_ENLACE CNE WITH (NOLOCK) ON CNE.NOTIFICADO_ORIGEN=NEC.NOTIFICADO" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PM_COPIA_ENLACE CE WITH (NOLOCK) ON CE.ID=CNE.ENLACE" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PM_COPIA_BLOQUE CB WITH (NOLOCK) ON CB.ID=CE.BLOQUE_ORIGEN AND CB.INSTANCIA=CG.INSTANCIA" & vbCrLf
sConsulta = sConsulta & "       WHERE CG.INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "       IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   ------------------------------------- Aqu� van las condiciones de bloqueo --------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "   IF @ENTRATAMIENTO =1 " & vbCrLf
sConsulta = sConsulta & "       SET @GRABADO = (SELECT COUNT(1) FROM PM_COPIA_BLOQUEO_ETAPA   WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PM_COPIA_BLOQUE B  WITH (NOLOCK) ON PM_COPIA_BLOQUEO_ETAPA.IDBLOQUE=B.ID" & vbCrLf
sConsulta = sConsulta & "           WHERE  B.INSTANCIA=@ID)" & vbCrLf
sConsulta = sConsulta & "   IF @GRABADO=0" & vbCrLf
sConsulta = sConsulta & "   BEGIN   " & vbCrLf
sConsulta = sConsulta & "       INSERT INTO PM_COPIA_BLOQUEO_ETAPA (BLOQUEO_APERTURA, BLOQUEO_ADJUDICACION, BLOQUEO_PEDIDOSDIRECTOS, BLOQUEO_EP, IDBLOQUE, IDBLOQUEO_ORIGEN)" & vbCrLf
sConsulta = sConsulta & "       SELECT BLOQUEO_APERTURA, BLOQUEO_ADJUDICACION, BLOQUEO_PEDIDOSDIRECTOS, BLOQUEO_EP, B.ID, PM_BLOQUEO_ETAPA.ID" & vbCrLf
sConsulta = sConsulta & "       FROM PM_BLOQUEO_ETAPA   WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PM_COPIA_BLOQUE B  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       ON PM_BLOQUEO_ETAPA.IDBLOQUE=B.BLOQUE_ORIGEN" & vbCrLf
sConsulta = sConsulta & "       WHERE  B.INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "       IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   IF @ENTRATAMIENTO =1 " & vbCrLf
sConsulta = sConsulta & "       SET @GRABADO = (SELECT COUNT(1) FROM PM_COPIA_CONDICIONES_BLOQUEO WITH (NOLOCK) WHERE INSTANCIA=@ID)" & vbCrLf
sConsulta = sConsulta & "   IF @GRABADO=0" & vbCrLf
sConsulta = sConsulta & "   BEGIN   " & vbCrLf
sConsulta = sConsulta & "       INSERT INTO PM_COPIA_CONDICIONES_BLOQUEO (COD,TIPO,MENSAJE_SPA,MENSAJE_ENG,MENSAJE_GER,TIPO_BLOQUEO,INSTANCIA,FECACT,FORMULA, CONDICIONES_ORIGEN) " & vbCrLf
sConsulta = sConsulta & "        SELECT COD,TIPO,MENSAJE_SPA,MENSAJE_ENG,MENSAJE_GER,TIPO_BLOQUEO,@ID,FECACT,FORMULA,ID " & vbCrLf
sConsulta = sConsulta & "       FROM PM_CONDICIONES_BLOQUEO WITH (NOLOCK) WHERE WORKFLOW=@WORKFLOW" & vbCrLf
sConsulta = sConsulta & "       IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @ENTRATAMIENTO =1 " & vbCrLf
sConsulta = sConsulta & "       SET @GRABADO = (SELECT COUNT(1)  FROM PM_COPIA_CONDICIONES_BLOQUEO_CONDICIONES WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PM_COPIA_CONDICIONES_BLOQUEO PCCB WITH (NOLOCK) ON PM_COPIA_CONDICIONES_BLOQUEO_CONDICIONES.ID_COD = PCCB.ID " & vbCrLf
sConsulta = sConsulta & "           WHERE PCCB.INSTANCIA = @ID)" & vbCrLf
sConsulta = sConsulta & "   IF @GRABADO=0" & vbCrLf
sConsulta = sConsulta & "   BEGIN   " & vbCrLf
sConsulta = sConsulta & "       INSERT INTO PM_COPIA_CONDICIONES_BLOQUEO_CONDICIONES (COD, TIPO_CAMPO, CAMPO, OPERADOR, TIPO_VALOR, CAMPO_VALOR, VALOR_TEXT, VALOR_NUM, VALOR_FEC, VALOR_BOOL, FECACT, ID_COD)" & vbCrLf
sConsulta = sConsulta & "         SELECT PCBC.COD, PCBC.TIPO_CAMPO, CAMPO_DEF.ID, PCBC.OPERADOR, PCBC.TIPO_VALOR, CAMPO_DEF2.ID, PCBC.VALOR_TEXT, PCBC.VALOR_NUM, PCBC.VALOR_FEC, PCBC.VALOR_BOOL, PCBC.FECACT, PCCB.ID" & vbCrLf
sConsulta = sConsulta & "         FROM PM_CONDICIONES_BLOQUEO_CONDICIONES PCBC WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "         INNER JOIN PM_CONDICIONES_BLOQUEO PCB WITH (NOLOCK) ON PCBC.ID_COD = PCB.ID " & vbCrLf
sConsulta = sConsulta & "         INNER JOIN PM_COPIA_CONDICIONES_BLOQUEO PCCB WITH (NOLOCK) ON PCCB.CONDICIONES_ORIGEN = PCB.ID " & vbCrLf
sConsulta = sConsulta & "         LEFT JOIN (SELECT COPIA_CAMPO_DEF.ID,COPIA_CAMPO_DEF.CAMPO_ORIGEN" & vbCrLf
sConsulta = sConsulta & "           FROM COPIA_CAMPO_DEF  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN COPIA_GRUPO  WITH (NOLOCK) ON COPIA_CAMPO_DEF.GRUPO =COPIA_GRUPO.ID AND COPIA_GRUPO.INSTANCIA=@ID) CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "         ON PCBC.CAMPO=CAMPO_DEF.CAMPO_ORIGEN" & vbCrLf
sConsulta = sConsulta & "         LEFT JOIN (SELECT COPIA_CAMPO_DEF.ID,COPIA_CAMPO_DEF.CAMPO_ORIGEN" & vbCrLf
sConsulta = sConsulta & "                   FROM COPIA_CAMPO_DEF  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN COPIA_GRUPO   WITH (NOLOCK) ON COPIA_CAMPO_DEF.GRUPO =COPIA_GRUPO.ID AND COPIA_GRUPO.INSTANCIA=@ID) CAMPO_DEF2" & vbCrLf
sConsulta = sConsulta & "         ON PCBC.CAMPO_VALOR=CAMPO_DEF2.CAMPO_ORIGEN" & vbCrLf
sConsulta = sConsulta & "         WHERE PCB.WORKFLOW = @WORKFLOW AND PCCB.INSTANCIA = @ID" & vbCrLf
sConsulta = sConsulta & "       IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   --AHORA INSERTAMOS LOS DATOS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @SENTIDO = (SELECT DISTINCT SENTIDO FROM TABLAS_INTEGRACION_ERP  WITH (NOLOCK) WHERE TABLA=18 AND ACTIVA=1 AND SENTIDO=1)" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   SET @FECHA=GETDATE()" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   --Ahora inserta los campos, copiando los del formulario. Si @GUARDAR=1 despu�s actualizar� los campos modificables desde el PM/QA:" & vbCrLf
sConsulta = sConsulta & "   SET @NUM_VERSION = 1" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   INSERT INTO VERSION_INSTANCIA(INSTANCIA,NUM_VERSION, PER_MODIF, FECHA_MODIF)" & vbCrLf
sConsulta = sConsulta & "    VALUES (@ID, @NUM_VERSION, @PETICIONARIO,GETDATE())" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   IF @GUARDAR=1" & vbCrLf
sConsulta = sConsulta & "   begin" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   SELECT top 1  @BLOQUE = CB.ID,@ROL = CR.ID" & vbCrLf
sConsulta = sConsulta & "   FROM PM_CONF_CUMP_BLOQUE  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "      INNER JOIN PM_COPIA_BLOQUE CB  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "          ON PM_CONF_CUMP_BLOQUE.BLOQUE=CB.BLOQUE_ORIGEN" & vbCrLf
sConsulta = sConsulta & "      INNER JOIN PM_COPIA_ROL CR  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "          ON PM_CONF_CUMP_BLOQUE.ROL=CR.ROL_ORIGEN" & vbCrLf
sConsulta = sConsulta & "   WHERE  CB.INSTANCIA=@ID AND CB.TIPO = 1  AND CR.INSTANCIA=@ID AND CR.PER = @PETICIONARIO AND CR.TIPO = 4" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   INSERT INTO COPIA_CAMPO (INSTANCIA, NUM_VERSION, SUCIO, VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL, COPIA_CAMPO_DEF)" & vbCrLf
sConsulta = sConsulta & "   SELECT @ID, @NUM_VERSION, 0, T.VALOR_NUM,  T.VALOR_TEXT , T.VALOR_FEC, T.VALOR_BOOL, CCD.ID " & vbCrLf
sConsulta = sConsulta & "     FROM FORM_CAMPO FC  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN #TEMPORAL T WITH (NOLOCK)   ON FC.ID  = T.CAMPO" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN (COPIA_CAMPO_DEF CCD WITH (NOLOCK)  " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN COPIA_GRUPO CGP WITH (NOLOCK)  ON CCD.GRUPO = CGP.ID AND @ID = CGP.INSTANCIA)  ON CCD.CAMPO_ORIGEN= FC.ID" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PM_copia_CONF_CUMP_BLOQUE CCB WITH (NOLOCK)  ON CCB.CAMPO= CCD.ID  AND CCB.ROL=@ROL AND CCB.BLOQUE=@BLOQUE" & vbCrLf
sConsulta = sConsulta & "    ORDER BY CGP.ID, FC.ES_SUBCAMPO, FC.ORDEN" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   INSERT INTO COPIA_CAMPO_ADJUN (ADJUN, CAMPO, NOM, IDIOMA, DATASIZE, PER, COMENT, FECALTA)" & vbCrLf
sConsulta = sConsulta & "   SELECT CA.ID, CC.ID, CA.NOM, 'SPA', DATALENGTH(CA.DATA), CA.PER, CA.COMENT, CA.FECALTA" & vbCrLf
sConsulta = sConsulta & "     FROM COPIA_ADJUN CA  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN #TEMPORAL_ADJUN T WITH (NOLOCK)  ON CA.ID = T.ADJUN" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN (COPIA_CAMPO CC WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN COPIA_CAMPO_DEF CCD  WITH (NOLOCK) ON CC.COPIA_CAMPO_DEF = CCD.ID) ON CCD.CAMPO_ORIGEN = T.CAMPO" & vbCrLf
sConsulta = sConsulta & "   WHERE CC.INSTANCIA = @ID AND T.TIPO = 2" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   DECLARE @MAXID INT" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   SET @MAXID= (SELECT ISNULL(MAX(ID),0) FROM COPIA_ADJUN)" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   INSERT INTO COPIA_ADJUN (DATA, FECALTA, NOM, PER, COMENT,ADJUN_ORIGEN)                " & vbCrLf
sConsulta = sConsulta & "    SELECT A.DATA, GETDATE(), A.NOM, @PETICIONARIO, A.COMENT, A.ID" & vbCrLf
sConsulta = sConsulta & "      FROM CAMPO_ADJUN A WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "      INNER JOIN #TEMPORAL_ADJUN T  WITH (NOLOCK) ON A.ID = T.ADJUN AND T.TIPO = 1" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "    " & vbCrLf
sConsulta = sConsulta & "    " & vbCrLf
sConsulta = sConsulta & "   INSERT INTO COPIA_CAMPO_ADJUN (ADJUN, CAMPO, NOM, IDIOMA, DATASIZE, PER, COMENT, FECALTA, RUTA)" & vbCrLf
sConsulta = sConsulta & "   SELECT CA.ID, CC.ID, CA.NOM, A.IDIOMA, A.DATASIZE, CA.PER, CA.COMENT, CA.FECALTA, A.RUTA" & vbCrLf
sConsulta = sConsulta & "     FROM CAMPO_ADJUN A WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN #TEMPORAL_ADJUN T WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                   ON A.ID = T.ADJUN" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN COPIA_ADJUN CA WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                   ON A.ID = CA.ADJUN_ORIGEN" & vbCrLf
sConsulta = sConsulta & "                  AND T.ADJUN = CA.ADJUN_ORIGEN" & vbCrLf
sConsulta = sConsulta & "                  AND CA.ID > @MAXID" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN (COPIA_CAMPO CC WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                           INNER JOIN COPIA_CAMPO_DEF CCD WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                   ON CC.COPIA_CAMPO_DEF = CCD.ID)" & vbCrLf
sConsulta = sConsulta & "                   ON CCD.CAMPO_ORIGEN = T.CAMPO " & vbCrLf
sConsulta = sConsulta & "   WHERE CC.INSTANCIA = @ID" & vbCrLf
sConsulta = sConsulta & "     AND T.TIPO = 1" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   INSERT INTO COPIA_DESGLOSE (CAMPO_PADRE, CAMPO_HIJO) " & vbCrLf
sConsulta = sConsulta & "   SELECT DISTINCT CCP.ID, CCH.ID " & vbCrLf
sConsulta = sConsulta & "     FROM (SELECT D.CAMPO_PADRE, D.CAMPO_HIJO " & vbCrLf
sConsulta = sConsulta & "             FROM DESGLOSE D  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                    INNER JOIN FORM_CAMPO FC WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                            ON D.CAMPO_PADRE = FC.ID" & vbCrLf
sConsulta = sConsulta & "                    INNER JOIN FORM_GRUPO FG  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                            ON FC.GRUPO = FG.ID " & vbCrLf
sConsulta = sConsulta & "            WHERE FG.FORMULARIO = @FORMULARIO) D" & vbCrLf
sConsulta = sConsulta & "            INNER JOIN (COPIA_CAMPO_DEF CCDP  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                         INNER JOIN COPIA_CAMPO CCP  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                     ON CCDP.ID=CCP.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "                             INNER JOIN COPIA_GRUPO CGP WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                     ON CCDP.GRUPO = CGP.ID" & vbCrLf
sConsulta = sConsulta & "                                    AND @ID = CGP.INSTANCIA)" & vbCrLf
sConsulta = sConsulta & "                    ON CCDP.CAMPO_ORIGEN = D.CAMPO_PADRE " & vbCrLf
sConsulta = sConsulta & "            INNER JOIN (COPIA_CAMPO_DEF CCDH  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                             INNER JOIN COPIA_CAMPO CCH  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                     ON CCDH.ID=CCH.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "                             INNER JOIN COPIA_GRUPO CGH WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                     ON CCDH.GRUPO = CGH.ID" & vbCrLf
sConsulta = sConsulta & "                                    AND @ID = CGH.INSTANCIA)" & vbCrLf
sConsulta = sConsulta & "                    ON CCDH.CAMPO_ORIGEN = D.CAMPO_HIJO " & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   --*********************************************************************************************************************" & vbCrLf
sConsulta = sConsulta & "   --INSERTAR LAS NUEVAS LINEAS DE DESGLOSE QUE SE INTRODUZCAN DESDE EL PM" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO COPIA_LINEA_DESGLOSE (LINEA, CAMPO_PADRE, CAMPO_HIJO, VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL)" & vbCrLf
sConsulta = sConsulta & "   SELECT  T.LINEA, CCP.ID,CCH.ID, T.VALOR_NUM,  T.VALOR_TEXT , T.VALOR_FEC, T.VALOR_BOOL " & vbCrLf
sConsulta = sConsulta & "     FROM #TEMPORAL_DESGLOSE T  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN (COPIA_CAMPO_DEF CCDPADRE WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                             INNER JOIN COPIA_CAMPO CCP  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                     ON CCDPADRE.ID=CCP.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "                             INNER JOIN COPIA_GRUPO CGP WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                     ON CCDPADRE.GRUPO = CGP.ID" & vbCrLf
sConsulta = sConsulta & "                                    AND @ID = CGP.INSTANCIA)" & vbCrLf
sConsulta = sConsulta & "               ON T.CAMPO_PADRE = CCDPADRE.CAMPO_ORIGEN " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN (COPIA_CAMPO_DEF CCDHIJO WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                             INNER JOIN COPIA_CAMPO CCH  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                     ON CCDHIJO.ID=CCH.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "                             INNER JOIN COPIA_GRUPO CGH WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                     ON CCDHIJO.GRUPO = CGH.ID" & vbCrLf
sConsulta = sConsulta & "                                    AND @ID = CGH.INSTANCIA)" & vbCrLf
sConsulta = sConsulta & "               ON T.CAMPO_HIJO = CCDHIJO.CAMPO_ORIGEN" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PM_copia_CONF_CUMP_BLOQUE CCB WITH (NOLOCK)  ON CCB.CAMPO=CCDHIJO.ID AND ROL=@ROL AND BLOQUE=@BLOQUE" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   SET @MAXID= (SELECT ISNULL(MAX(ID),0) FROM COPIA_ADJUN)" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   --Cambios para trazabilidad l�neas solicitudes con los ERP" & vbCrLf
sConsulta = sConsulta & "   if @sentido = 1 " & vbCrLf
sConsulta = sConsulta & "   begin" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO COPIA_LINEA (INSTANCIA, NUM_VERSION,  LINEA, CAMPO_PADRE)" & vbCrLf
sConsulta = sConsulta & "   SELECT DISTINCT @ID, @NUM_VERSION, T.LINEA, CCP.ID" & vbCrLf
sConsulta = sConsulta & "     FROM #TEMPORAL_DESGLOSE T  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN (COPIA_CAMPO_DEF CCDPADRE WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                             INNER JOIN COPIA_CAMPO CCP  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                     ON CCDPADRE.ID=CCP.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "                             INNER JOIN COPIA_GRUPO CGP WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                     ON CCDPADRE.GRUPO = CGP.ID" & vbCrLf
sConsulta = sConsulta & "                                    AND @ID = CGP.INSTANCIA AND CCDPADRE.TIPO_CAMPO_GS=106)" & vbCrLf
sConsulta = sConsulta & "               ON T.CAMPO_PADRE = CCDPADRE.CAMPO_ORIGEN " & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   End" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "--//////////////////////////////////////////////Defecto////////////////////////////////////////////////////" & vbCrLf
sConsulta = sConsulta & "   DECLARE @LINEA INT" & vbCrLf
sConsulta = sConsulta & "   DECLARE @ADJUN INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   DECLARE C1 CURSOR LOCAL FOR SELECT DISTINCT LINEA, ADJUN  FROM #TEMPORAL_DESGLOSE_ADJUN  WITH (NOLOCK) WHERE TIPO=3" & vbCrLf
sConsulta = sConsulta & "       OPEN C1" & vbCrLf
sConsulta = sConsulta & "       FETCH NEXT FROM C1 INTO @LINEA,@ADJUN" & vbCrLf
sConsulta = sConsulta & "       WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "       INSERT INTO COPIA_ADJUN (DATA, FECALTA, NOM, PER, COMENT,ADJUN_ORIGEN)                " & vbCrLf
sConsulta = sConsulta & "       SELECT A.DATA, GETDATE(), A.NOM, @PETICIONARIO, A.COMENT, A.ID FROM LINEA_DESGLOSE_ADJUN A WITH (NOLOCK) WHERE A.ID = @ADJUN  " & vbCrLf
sConsulta = sConsulta & "       IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       INSERT INTO COPIA_LINEA_DESGLOSE_ADJUN (CAMPO_PADRE, CAMPO_HIJO, LINEA, ADJUN, NOM, IDIOMA, DATASIZE, PER, COMENT, FECALTA, RUTA)" & vbCrLf
sConsulta = sConsulta & "       SELECT CCP.ID, CCH.ID,  T.LINEA, CA.ID, CA.NOM, 'SPA', A.DATASIZE, CA.PER, CA.COMENT, CA.FECALTA, A.RUTA" & vbCrLf
sConsulta = sConsulta & "           FROM LINEA_DESGLOSE_ADJUN A  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN #TEMPORAL_DESGLOSE_ADJUN T WITH (NOLOCK) ON A.ID = T.ADJUN" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN COPIA_ADJUN CA WITH (NOLOCK) ON  CA.ID>@MAXID AND A.ID = CA.ADJUN_ORIGEN AND T.ADJUN = CA.ADJUN_ORIGEN" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN (COPIA_CAMPO_DEF CCDPADRE WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "               INNER JOIN COPIA_CAMPO CCP  WITH (NOLOCK) ON CCDPADRE.ID=CCP.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "               INNER JOIN COPIA_GRUPO CGP WITH (NOLOCK) ON CCDPADRE.GRUPO = CGP.ID" & vbCrLf
sConsulta = sConsulta & "               AND @ID = CGP.INSTANCIA) ON T.CAMPO_PADRE = CCDPADRE.CAMPO_ORIGEN " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN (COPIA_CAMPO_DEF CCDHIJO WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "               INNER JOIN COPIA_CAMPO CCH  WITH (NOLOCK) ON CCDHIJO.ID=CCH.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "               INNER JOIN COPIA_GRUPO CGH WITH (NOLOCK) ON CCDHIJO.GRUPO = CGH.ID" & vbCrLf
sConsulta = sConsulta & "               AND @ID = CGH.INSTANCIA) ON T.CAMPO_HIJO = CCDHIJO.CAMPO_ORIGEN " & vbCrLf
sConsulta = sConsulta & "           WHERE T.LINEA=@LINEA AND T.ADJUN=@ADJUN" & vbCrLf
sConsulta = sConsulta & "       IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       SET @MAXID= (SELECT ISNULL(MAX(ID),0) FROM COPIA_ADJUN)" & vbCrLf
sConsulta = sConsulta & "               " & vbCrLf
sConsulta = sConsulta & "       FETCH NEXT FROM C1 INTO @LINEA,@ADJUN" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       CLOSE C1" & vbCrLf
sConsulta = sConsulta & "       DEALLOCATE C1" & vbCrLf
sConsulta = sConsulta & "--Fin Defecto   " & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "   INSERT INTO COPIA_ADJUN (DATA, FECALTA, NOM, PER, COMENT,ADJUN_ORIGEN)                " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    SELECT A.DATA, GETDATE(), A.NOM, @PETICIONARIO, A.COMENT, A.ID" & vbCrLf
sConsulta = sConsulta & "      FROM LINEA_DESGLOSE_ADJUN A WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "            INNER JOIN (select DISTINCT ADJUN, TIPO FROM #TEMPORAL_DESGLOSE_ADJUN  WITH (NOLOCK) ) T" & vbCrLf
sConsulta = sConsulta & "                    ON A.ID = T.ADJUN AND T.TIPO = 1" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   INSERT INTO COPIA_LINEA_DESGLOSE_ADJUN (CAMPO_PADRE, CAMPO_HIJO, LINEA, ADJUN, NOM, IDIOMA, DATASIZE, PER, COMENT, FECALTA, RUTA)" & vbCrLf
sConsulta = sConsulta & "   SELECT CCP.ID, CCH.ID,  T.LINEA, CA.ID, CA.NOM, 'SPA', A.DATASIZE, CA.PER, CA.COMENT, CA.FECALTA, A.RUTA" & vbCrLf
sConsulta = sConsulta & "     FROM LINEA_DESGLOSE_ADJUN A  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN #TEMPORAL_DESGLOSE_ADJUN T WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                   ON A.ID = T.ADJUN" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN COPIA_ADJUN CA WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                   ON CA.ID>@MAXID" & vbCrLf
sConsulta = sConsulta & "         AND A.ID = CA.ADJUN_ORIGEN" & vbCrLf
sConsulta = sConsulta & "                  AND T.ADJUN = CA.ADJUN_ORIGEN" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN (COPIA_CAMPO_DEF CCDPADRE WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                             INNER JOIN COPIA_CAMPO CCP  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "               ON CCDPADRE.ID=CCP.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "                             INNER JOIN COPIA_GRUPO CGP WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                     ON CCDPADRE.GRUPO = CGP.ID" & vbCrLf
sConsulta = sConsulta & "                                    AND @ID = CGP.INSTANCIA)" & vbCrLf
sConsulta = sConsulta & "               ON T.CAMPO_PADRE = CCDPADRE.CAMPO_ORIGEN " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN (COPIA_CAMPO_DEF CCDHIJO WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                             INNER JOIN COPIA_CAMPO CCH  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                     ON CCDHIJO.ID=CCH.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "                             INNER JOIN COPIA_GRUPO CGH WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                     ON CCDHIJO.GRUPO = CGH.ID" & vbCrLf
sConsulta = sConsulta & "                                    AND @ID = CGH.INSTANCIA)" & vbCrLf
sConsulta = sConsulta & "               ON T.CAMPO_HIJO = CCDHIJO.CAMPO_ORIGEN " & vbCrLf
sConsulta = sConsulta & "   WHERE T.TIPO = 1" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   INSERT INTO COPIA_LINEA_DESGLOSE_ADJUN (CAMPO_PADRE, CAMPO_HIJO, LINEA, ADJUN, NOM, IDIOMA, DATASIZE, PER, COMENT, FECALTA)" & vbCrLf
sConsulta = sConsulta & "   SELECT CCP.ID, CCH.ID,  T.LINEA, CA.ID, CA.NOM, 'SPA', DATALENGTH(CA.DATA), CA.PER, CA.COMENT, CA.FECALTA" & vbCrLf
sConsulta = sConsulta & "     FROM COPIA_ADJUN CA  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN #TEMPORAL_DESGLOSE_ADJUN T WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                   ON CA.ID = T.ADJUN" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN (COPIA_CAMPO_DEF CCDPADRE WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                             INNER JOIN COPIA_CAMPO CCP  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                     ON CCDPADRE.ID=CCP.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "                        INNER JOIN COPIA_GRUPO CGP WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                     ON CCDPADRE.GRUPO = CGP.ID" & vbCrLf
sConsulta = sConsulta & "                                    AND @ID = CGP.INSTANCIA)" & vbCrLf
sConsulta = sConsulta & "               ON T.CAMPO_PADRE = CCDPADRE.CAMPO_ORIGEN " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN (COPIA_CAMPO_DEF CCDHIJO WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                             INNER JOIN COPIA_CAMPO CCH  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                     ON CCDHIJO.ID=CCH.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "                             INNER JOIN COPIA_GRUPO CGH WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                     ON CCDHIJO.GRUPO = CGH.ID" & vbCrLf
sConsulta = sConsulta & "                                    AND @ID = CGH.INSTANCIA)" & vbCrLf
sConsulta = sConsulta & "               ON T.CAMPO_HIJO = CCDHIJO.CAMPO_ORIGEN" & vbCrLf
sConsulta = sConsulta & "   WHERE T.TIPO = 2" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   UPDATE PM_COPIA_ROL " & vbCrLf
sConsulta = sConsulta & "   SET PER=CASE WHEN U.FSWS_SUSTITUCION IS NULL THEN CASE WHEN T.PER='' THEN NULL ELSE T.PER END ELSE U.FSWS_SUSTITUCION END, " & vbCrLf
sConsulta = sConsulta & "       PER_ORIGINAL= T.PER," & vbCrLf
sConsulta = sConsulta & "       PROVE=CASE WHEN T.PROVE=''  THEN NULL ELSE T.PROVE END," & vbCrLf
sConsulta = sConsulta & "       CON=CASE WHEN T.CON=0  THEN NULL ELSE T.CON END" & vbCrLf
sConsulta = sConsulta & "   FROM #TEMPORAL_PARTICIPANTES T WITH (NOLOCK)  " & vbCrLf
sConsulta = sConsulta & "      left join USU U WITH (NOLOCK) on T.PER = U.PER" & vbCrLf
sConsulta = sConsulta & "      inner join PM_COPIA_ROL PCR WITH (NOLOCK) on PCR.ROL_ORIGEN = T.ROL" & vbCrLf
sConsulta = sConsulta & "      inner join INSTANCIA WITH (NOLOCK)  ON PCR.INSTANCIA=INSTANCIA.ID AND (INSTANCIA.ESTADO=0 or INSTANCIA.ESTADO=2 or INSTANCIA.ESTADO=6)" & vbCrLf
sConsulta = sConsulta & "   WHERE PCR.INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   end" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   ELSE  --No guarda los campos modificados desde el QA , simplemente hace una copia del formulario" & vbCrLf
sConsulta = sConsulta & "   begin" & vbCrLf
sConsulta = sConsulta & "     INSERT INTO COPIA_CAMPO (INSTANCIA, NUM_VERSION, SUCIO, VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL, COPIA_CAMPO_DEF)" & vbCrLf
sConsulta = sConsulta & "     SELECT @ID, @NUM_VERSION, 0, FC.VALOR_NUM, FC.VALOR_TEXT, FC.VALOR_FEC, FC.VALOR_BOOL, CCD.ID " & vbCrLf
sConsulta = sConsulta & "     FROM FORM_CAMPO FC  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "     INNER JOIN (COPIA_CAMPO_DEF CCD  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "     INNER JOIN COPIA_GRUPO CGP WITH (NOLOCK)  ON CCD.GRUPO = CGP.ID AND @ID = CGP.INSTANCIA)  ON FC.ID = CCD.CAMPO_ORIGEN" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   SET @MAXID= (SELECT ISNULL(MAX(ID),0) FROM COPIA_ADJUN)" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   INSERT INTO COPIA_ADJUN (DATA, FECALTA, NOM, PER, COMENT,ADJUN_ORIGEN)                " & vbCrLf
sConsulta = sConsulta & "    SELECT A.DATA, GETDATE(), A.NOM, @PETICIONARIO, A.COMENT, A.ID" & vbCrLf
sConsulta = sConsulta & "      FROM CAMPO_ADJUN A WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "      INNER JOIN FORM_CAMPO FC WITH (NOLOCK)  ON A.CAMPO=FC.ID" & vbCrLf
sConsulta = sConsulta & "      INNER JOIN (COPIA_CAMPO_DEF  CCD WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "      INNER JOIN COPIA_GRUPO CGP WITH (NOLOCK)  ON CCD.GRUPO = CGP.ID AND @ID = CGP.INSTANCIA)  ON FC.ID = CCD.CAMPO_ORIGEN" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   INSERT INTO COPIA_CAMPO_ADJUN (ADJUN, CAMPO, NOM, IDIOMA, DATASIZE, PER, COMENT, FECALTA, RUTA)" & vbCrLf
sConsulta = sConsulta & "   SELECT CA.ID, CC.ID, CA.NOM, A.IDIOMA, A.DATASIZE, CA.PER, CA.COMENT, CA.FECALTA, A.RUTA" & vbCrLf
sConsulta = sConsulta & "     FROM CAMPO_ADJUN A WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN COPIA_ADJUN CA WITH (NOLOCK)  ON A.ID = CA.ADJUN_ORIGEN  AND CA.ID > @MAXID" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN (COPIA_CAMPO CC WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                           INNER JOIN COPIA_CAMPO_DEF CCD WITH (NOLOCK)  ON CC.COPIA_CAMPO_DEF = CCD.ID)" & vbCrLf
sConsulta = sConsulta & "                   ON CCD.CAMPO_ORIGEN = A.CAMPO" & vbCrLf
sConsulta = sConsulta & "   WHERE CC.INSTANCIA = @ID" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   INSERT INTO COPIA_DESGLOSE (CAMPO_PADRE, CAMPO_HIJO) " & vbCrLf
sConsulta = sConsulta & "   SELECT DISTINCT CCP.ID, CCH.ID " & vbCrLf
sConsulta = sConsulta & "     FROM (SELECT D.CAMPO_PADRE, D.CAMPO_HIJO " & vbCrLf
sConsulta = sConsulta & "             FROM DESGLOSE D  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                    INNER JOIN FORM_CAMPO FC WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                            ON D.CAMPO_PADRE = FC.ID" & vbCrLf
sConsulta = sConsulta & "                    INNER JOIN FORM_GRUPO FG  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                            ON FC.GRUPO = FG.ID " & vbCrLf
sConsulta = sConsulta & "            WHERE FG.FORMULARIO = @FORMULARIO) D" & vbCrLf
sConsulta = sConsulta & "            INNER JOIN (COPIA_CAMPO_DEF CCDP  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                             INNER JOIN COPIA_CAMPO CCP  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                     ON CCDP.ID=CCP.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "                             INNER JOIN COPIA_GRUPO CGP WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                     ON CCDP.GRUPO = CGP.ID" & vbCrLf
sConsulta = sConsulta & "                                    AND @ID = CGP.INSTANCIA) " & vbCrLf
sConsulta = sConsulta & "                    ON CCDP.CAMPO_ORIGEN = D.CAMPO_PADRE " & vbCrLf
sConsulta = sConsulta & "            INNER JOIN (COPIA_CAMPO_DEF CCDH  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                             INNER JOIN COPIA_CAMPO CCH  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                     ON CCDH.ID=CCH.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "                             INNER JOIN COPIA_GRUPO CGH WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                    ON CCDH.GRUPO = CGH.ID" & vbCrLf
sConsulta = sConsulta & "                                    AND @ID = CGH.INSTANCIA)" & vbCrLf
sConsulta = sConsulta & "                    ON CCDH.CAMPO_ORIGEN = D.CAMPO_HIJO " & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   INSERT INTO COPIA_LINEA_DESGLOSE (LINEA, CAMPO_PADRE, CAMPO_HIJO, VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL)" & vbCrLf
sConsulta = sConsulta & "   SELECT  L.LINEA, CCP.ID,CCH.ID, L.VALOR_NUM, L.VALOR_TEXT, L.VALOR_FEC, L.VALOR_BOOL " & vbCrLf
sConsulta = sConsulta & "     FROM LINEA_DESGLOSE L  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN (COPIA_CAMPO_DEF CCDPADRE WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                             INNER JOIN COPIA_CAMPO CCP  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                     ON CCDPADRE.ID=CCP.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "                            INNER JOIN COPIA_GRUPO CGP WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                     ON CCDPADRE.GRUPO = CGP.ID" & vbCrLf
sConsulta = sConsulta & "                                    AND @ID = CGP.INSTANCIA)" & vbCrLf
sConsulta = sConsulta & "               ON L.CAMPO_PADRE = CCDPADRE.CAMPO_ORIGEN " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN (COPIA_CAMPO_DEF CCDHIJO WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                             INNER JOIN COPIA_CAMPO CCH  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                     ON CCDHIJO.ID=CCH.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "                             INNER JOIN COPIA_GRUPO CGH WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                     ON CCDHIJO.GRUPO = CGH.ID" & vbCrLf
sConsulta = sConsulta & "                                    AND @ID = CGH.INSTANCIA)" & vbCrLf
sConsulta = sConsulta & "               ON L.CAMPO_HIJO = CCDHIJO.CAMPO_ORIGEN" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   SET @MAXID= (SELECT ISNULL(MAX(ID),0) FROM COPIA_ADJUN)" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   --Cambios para trazabilidad l�neas solicitudes con los ERP" & vbCrLf
sConsulta = sConsulta & "   if @sentido = 1" & vbCrLf
sConsulta = sConsulta & "   begin" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO COPIA_LINEA (INSTANCIA, NUM_VERSION, LINEA, CAMPO_PADRE)" & vbCrLf
sConsulta = sConsulta & "   SELECT DISTINCT @ID, @NUM_VERSION, L.LINEA, CCP.ID" & vbCrLf
sConsulta = sConsulta & "     FROM LINEA_DESGLOSE L  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN (COPIA_CAMPO_DEF CCDPADRE WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                             INNER JOIN COPIA_CAMPO CCP  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                     ON CCDPADRE.ID=CCP.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                            INNER JOIN COPIA_GRUPO CGP WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                     ON CCDPADRE.GRUPO = CGP.ID" & vbCrLf
sConsulta = sConsulta & "                                    AND @ID = CGP.INSTANCIA AND CCDPADRE.TIPO_CAMPO_GS=106)" & vbCrLf
sConsulta = sConsulta & "               ON L.CAMPO_PADRE = CCDPADRE.CAMPO_ORIGEN" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   end" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   INSERT INTO COPIA_ADJUN (DATA, FECALTA, NOM, PER, COMENT,ADJUN_ORIGEN)                " & vbCrLf
sConsulta = sConsulta & "    SELECT A.DATA, GETDATE(), A.NOM, @PETICIONARIO, A.COMENT, A.ID" & vbCrLf
sConsulta = sConsulta & "      FROM LINEA_DESGLOSE_ADJUN A WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "     INNER JOIN FORM_CAMPO FC WITH (NOLOCK)  ON A.CAMPO_HIJO=FC.ID" & vbCrLf
sConsulta = sConsulta & "      INNER JOIN (COPIA_CAMPO_DEF CCD  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "      INNER JOIN COPIA_GRUPO CGP WITH (NOLOCK)  ON CCD.GRUPO = CGP.ID AND @ID = CGP.INSTANCIA)  ON FC.ID = CCD.CAMPO_ORIGEN" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   INSERT INTO COPIA_LINEA_DESGLOSE_ADJUN (CAMPO_PADRE, CAMPO_HIJO, LINEA, ADJUN, NOM, IDIOMA, DATASIZE, PER, COMENT, FECALTA, RUTA)" & vbCrLf
sConsulta = sConsulta & "   SELECT CCP.ID, CCH.ID,  A.LINEA, CA.ID, CA.NOM, 'SPA', A.DATASIZE, CA.PER, CA.COMENT, CA.FECALTA, A.RUTA" & vbCrLf
sConsulta = sConsulta & "     FROM LINEA_DESGLOSE_ADJUN A WITH (NOLOCK)  " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN COPIA_ADJUN CA WITH (NOLOCK)  ON A.ID = CA.ADJUN_ORIGEN  AND CA.ID>@MAXID" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN (COPIA_CAMPO_DEF CCDPADRE WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                             INNER JOIN COPIA_CAMPO CCP  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                     ON CCDPADRE.ID=CCP.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "                             INNER JOIN COPIA_GRUPO CGP WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                     ON CCDPADRE.GRUPO = CGP.ID" & vbCrLf
sConsulta = sConsulta & "                                    AND @ID = CGP.INSTANCIA)" & vbCrLf
sConsulta = sConsulta & "               ON A.CAMPO_PADRE = CCDPADRE.CAMPO_ORIGEN " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN (COPIA_CAMPO_DEF CCDHIJO WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                             INNER JOIN COPIA_CAMPO CCH  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                     ON CCDHIJO.ID=CCH.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "                             INNER JOIN COPIA_GRUPO CGH WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                     ON CCDHIJO.GRUPO = CGH.ID" & vbCrLf
sConsulta = sConsulta & "                                    AND @ID = CGH.INSTANCIA)" & vbCrLf
sConsulta = sConsulta & "               ON A.CAMPO_HIJO = CCDHIJO.CAMPO_ORIGEN" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   end" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   --Devuelve el id de la acci�n de copia_campo: " & vbCrLf
sConsulta = sConsulta & "   IF @ACCION<>0  --Si no est� guardando:" & vbCrLf
sConsulta = sConsulta & "   SET @ACCION=(SELECT PM_COPIA_ACCIONES.ID FROM PM_COPIA_ACCIONES  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PM_COPIA_BLOQUE B  WITH (NOLOCK) ON PM_COPIA_ACCIONES.BLOQUE=B.ID WHERE B.INSTANCIA=@ID AND  ACCION_ORIGEN=@ACCION)" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "   --Comprueba el tipo de la solicitud para saber si tiene que emitir un certificado o una no conformidad" & vbCrLf
sConsulta = sConsulta & "   SELECT @TIPO_SOLICIT=TP.TIPO FROM SOLICITUD WITH (NOLOCK)  INNER JOIN TIPO_SOLICITUDES TP WITH (NOLOCK)  ON SOLICITUD.TIPO=TP.ID WHERE SOLICITUD.ID=@SOLICITUD" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "   --Inserta en INSTANCIA_BLOQUE el bloque del peticionario:" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO INSTANCIA_BLOQUE(INSTANCIA,BLOQUE,ESTADO) " & vbCrLf
sConsulta = sConsulta & "   SELECT @ID,PM_COPIA_BLOQUE.ID, 1 FROM PM_COPIA_BLOQUE WITH (NOLOCK)  WHERE INSTANCIA=@ID AND TIPO=1" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   --Actualizar la tabla PM_COPIA_ROL, para establecer roles mediante campos" & vbCrLf
sConsulta = sConsulta & "   --Roles de tipo Comprador o usuario" & vbCrLf
sConsulta = sConsulta & "   UPDATE PM_COPIA_ROL" & vbCrLf
sConsulta = sConsulta & "   SET PER = CASE WHEN U.FSWS_SUSTITUCION IS NULL THEN CC.VALOR_TEXT ELSE U.FSWS_SUSTITUCION END," & vbCrLf
sConsulta = sConsulta & "       PER_ORIGINAL= CC.VALOR_TEXT" & vbCrLf
sConsulta = sConsulta & "   FROM COPIA_CAMPO CC WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   left join USU U WITH (NOLOCK) on CC.VALOR_TEXT = U.PER" & vbCrLf
sConsulta = sConsulta & "   inner join PM_COPIA_ROL PCR WITH (NOLOCK) on PCR.CAMPO = CC.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "   inner join INSTANCIA WITH (NOLOCK)  ON PCR.INSTANCIA=INSTANCIA.ID AND (INSTANCIA.ESTADO=0 or INSTANCIA.ESTADO=2 or INSTANCIA.ESTADO=6)" & vbCrLf
sConsulta = sConsulta & "   WHERE PCR.CAMPO = CC.COPIA_CAMPO_DEF " & vbCrLf
sConsulta = sConsulta & "      AND (PCR.TIPO = 1 OR PCR.TIPO = 3)" & vbCrLf
sConsulta = sConsulta & "      AND PCR.INSTANCIA = @ID" & vbCrLf
sConsulta = sConsulta & "      AND PCR.CUANDO_ASIGNAR = 1" & vbCrLf
sConsulta = sConsulta & "      AND PCR.BLOQUE IN (SELECT BLOQUE" & vbCrLf
sConsulta = sConsulta & "                      FROM INSTANCIA_BLOQUE WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                      WHERE ESTADO = 1 " & vbCrLf
sConsulta = sConsulta & "                      AND INSTANCIA = @ID)" & vbCrLf
sConsulta = sConsulta & "      AND PCR.COMO_ASIGNAR = 1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "      AND CC.INSTANCIA = @ID" & vbCrLf
sConsulta = sConsulta & "      AND CC.NUM_VERSION = @NUM_VERSION" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   --Roles de tipo Proveedor" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = N'UPDATE PM_COPIA_ROL SET PROVE = cast(CC.VALOR_TEXT as '" & vbCrLf
sConsulta = sConsulta & "   SET @sQL = @sQL + (select 'varchar(' + convert(varchar,longitud) + ')' from DIC where nombre='PROVE') " & vbCrLf
sConsulta = sConsulta & "   SET @sQL = @sQL + ')    FROM COPIA_CAMPO CC WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "       WHERE PM_COPIA_ROL.TIPO = 2" & vbCrLf
sConsulta = sConsulta & "       AND PM_COPIA_ROL.INSTANCIA = @ID" & vbCrLf
sConsulta = sConsulta & "       AND PM_COPIA_ROL.CUANDO_ASIGNAR = 1" & vbCrLf
sConsulta = sConsulta & "       AND PM_COPIA_ROL.BLOQUE IN (SELECT BLOQUE" & vbCrLf
sConsulta = sConsulta & "                             FROM INSTANCIA_BLOQUE WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                          WHERE INSTANCIA = @ID" & vbCrLf
sConsulta = sConsulta & "                          AND ESTADO = 1)" & vbCrLf
sConsulta = sConsulta & "          AND PM_COPIA_ROL.COMO_ASIGNAR = 1" & vbCrLf
sConsulta = sConsulta & "          AND PM_COPIA_ROL.CAMPO = CC.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "          AND CC.INSTANCIA = @ID" & vbCrLf
sConsulta = sConsulta & "          AND CC.NUM_VERSION = @NUM_VERSION'" & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL,N'@ID INT, @NUM_VERSION INT', @ID=@ID,@NUM_VERSION=@NUM_VERSION" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   UPDATE INSTANCIA SET PEDIDO_AUT = @PEDIDO_AUT, IMPORTE = @IMPORTE,  PEDIDO_DIRECTO =  @IMPORTE_PEDIDO_DIRECTO, NUM = @ID WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "      IF @@ERROR!=0 GOTO  ERROR_" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   --Sentido integraci�n PM" & vbCrLf
sConsulta = sConsulta & "   SET @SENTIDO = (SELECT DISTINCT SENTIDO FROM TABLAS_INTEGRACION_ERP  WITH (NOLOCK) WHERE TABLA=18 AND ACTIVA=1 AND SENTIDO=1)" & vbCrLf
sConsulta = sConsulta & "   SET @FECHA = GETDATE()" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO VERSION_INSTANCIA(INSTANCIA, NUM_VERSION, PROVE, PER_MODIF, FECHA_MODIF, TIPO, CERTIFICADO)" & vbCrLf
sConsulta = sConsulta & "     VALUES (@ID, @NUM_VERSION, @PROVE, @PETICIONARIO,@FECHA , 0,0)" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @IMPORTE IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "     UPDATE INSTANCIA  SET IMPORTE = @IMPORTE  WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "      IF @PETICIONARIO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "        SELECT @BLOQUE=IB.BLOQUE, @ROL=R.ID FROM INSTANCIA_BLOQUE IB  WITH (NOLOCK) INNER JOIN PM_COPIA_ROL_BLOQUE CRB WITH (NOLOCK)  ON IB.BLOQUE=CRB.BLOQUE" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN PM_COPIA_ROL R WITH (NOLOCK)  ON CRB.ROL=R.ID WHERE IB.INSTANCIA=@ID AND IB.ESTADO=1 AND R.PER=@PETICIONARIO" & vbCrLf
sConsulta = sConsulta & "      ELSE" & vbCrLf
sConsulta = sConsulta & "        SELECT @BLOQUE=IB.BLOQUE, @ROL=R.ID FROM INSTANCIA_BLOQUE IB WITH (NOLOCK)  INNER JOIN PM_COPIA_ROL_BLOQUE CRB WITH (NOLOCK)  ON IB.BLOQUE=CRB.BLOQUE" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN PM_COPIA_ROL R WITH (NOLOCK)  ON CRB.ROL=R.ID WHERE IB.INSTANCIA=@ID AND IB.ESTADO=1 AND R.PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   --Crea tablas temporales para tener la relaci�n de los campos almacenados en la temporal con el id nuevo:" & vbCrLf
sConsulta = sConsulta & "   CREATE TABLE #TEMPORAL_CAMPOS (CAMPO_OLD INT, CAMPO_NEW INT)" & vbCrLf
sConsulta = sConsulta & "   CREATE TABLE #TEMPORAL_CAMPO_ADJUN (ADJUN_OLD INT, ADJUN_NEW INT)" & vbCrLf
sConsulta = sConsulta & "   CREATE TABLE #TEMP_CAMPO_ADJUN (ADJUN_OLD INT, ADJUN_NEW INT, LINEA INT)" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_  " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   --Inserta en copia_campo con los valores de la �ltima versi�n:" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO COPIA_CAMPO (INSTANCIA, NUM_VERSION, SUCIO, VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL, COPIA_CAMPO_DEF)" & vbCrLf
sConsulta = sConsulta & "   SELECT @ID, @NUM_VERSION, 0, C.VALOR_NUM" & vbCrLf
sConsulta = sConsulta & "   , C.VALOR_TEXT , C.VALOR_FEC, C.VALOR_BOOL, C.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "   FROM COPIA_CAMPO C  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN COPIA_CAMPO_DEF  CCD WITH (NOLOCK)  ON CCD.ID = C.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PM_COPIA_CONF_CUMP_BLOQUE CCB WITH (NOLOCK)  ON C.COPIA_CAMPO_DEF=CCB.CAMPO AND ROL=@ROL AND BLOQUE=@BLOQUE" & vbCrLf
sConsulta = sConsulta & "   WHERE C.INSTANCIA= @ID  AND C.NUM_VERSION = @OLD_VERSION" & vbCrLf
sConsulta = sConsulta & "   ORDER BY CCD.GRUPO, CCD.ES_SUBCAMPO, CCB.ORDEN" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "   --Crea la temporal con el id de la versi�n anterior y el nuevo:" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO #TEMPORAL_CAMPOS (CAMPO_OLD, CAMPO_NEW)" & vbCrLf
sConsulta = sConsulta & "   SELECT COLD.ID CAMPO_OLD, CNEW.ID CAMPO_NEW" & vbCrLf
sConsulta = sConsulta & "   FROM COPIA_CAMPO COLD  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN COPIA_CAMPO CNEW  WITH (NOLOCK) ON COLD.COPIA_CAMPO_DEF = CNEW.COPIA_CAMPO_DEF AND COLD.INSTANCIA = CNEW.INSTANCIA" & vbCrLf
sConsulta = sConsulta & "   WHERE COLD.INSTANCIA = @ID AND COLD.NUM_VERSION = @OLD_VERSION AND CNEW.NUM_VERSION = @NUM_VERSION" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "   ------------------------------------------------------ Ahora actualiza los id de todas las tablas temporales creadas desde el PM/QA: ------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   UPDATE #TEMPORAL SET CAMPO = TC.CAMPO_NEW " & vbCrLf
sConsulta = sConsulta & "   FROM #TEMPORAL T  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN COPIA_CAMPO_DEF CCD WITH (NOLOCK) ON T.CAMPO = CCD.CAMPO_ORIGEN" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN COPIA_CAMPO CC WITH (NOLOCK) ON CCD.ID = CC.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN #TEMPORAL_CAMPOS TC WITH (NOLOCK) ON CC.ID = TC.CAMPO_OLD" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   UPDATE #TEMPORAL_ADJUN SET CAMPO = TC.CAMPO_NEW " & vbCrLf
sConsulta = sConsulta & "   FROM #TEMPORAL_ADJUN T  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN COPIA_CAMPO_DEF CCD WITH (NOLOCK) ON T.CAMPO = CCD.CAMPO_ORIGEN" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN COPIA_CAMPO CC WITH (NOLOCK) ON CCD.ID = CC.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN #TEMPORAL_CAMPOS TC WITH (NOLOCK) ON CC.ID = TC.CAMPO_OLD" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   UPDATE #TEMPORAL_DESGLOSE SET CAMPO_PADRE = TCP.CAMPO_NEW, CAMPO_HIJO = TCH.CAMPO_NEW" & vbCrLf
sConsulta = sConsulta & "   FROM #TEMPORAL_DESGLOSE T  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN COPIA_CAMPO_DEF CCDP WITH (NOLOCK) ON T.CAMPO_PADRE = CCDP.CAMPO_ORIGEN" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN COPIA_CAMPO CCP WITH (NOLOCK) ON CCDP.ID = CCP.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN #TEMPORAL_CAMPOS TCP WITH (NOLOCK)  ON CCP.ID = TCP.CAMPO_OLD" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN COPIA_CAMPO_DEF CCDH WITH (NOLOCK) ON T.CAMPO_HIJO = CCDH.CAMPO_ORIGEN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN COPIA_CAMPO CCH WITH (NOLOCK) ON CCDH.ID = CCH.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN #TEMPORAL_CAMPOS TCH WITH (NOLOCK)  ON CCH.ID = TCH.CAMPO_OLD" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   UPDATE #TEMPORAL_DESGLOSE_ADJUN SET CAMPO_PADRE = TCP.CAMPO_NEW, CAMPO_HIJO = TCH.CAMPO_NEW" & vbCrLf
sConsulta = sConsulta & "   FROM #TEMPORAL_DESGLOSE_ADJUN T  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN COPIA_CAMPO_DEF CCDP WITH (NOLOCK) ON T.CAMPO_PADRE = CCDP.CAMPO_ORIGEN" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN COPIA_CAMPO CCP WITH (NOLOCK) ON CCDP.ID = CCP.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN #TEMPORAL_CAMPOS TCP WITH (NOLOCK)  ON CCP.ID = TCP.CAMPO_OLD" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN COPIA_CAMPO_DEF CCDH WITH (NOLOCK) ON T.CAMPO_HIJO = CCDH.CAMPO_ORIGEN" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN COPIA_CAMPO CCH WITH (NOLOCK) ON CCDH.ID = CCH.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN #TEMPORAL_CAMPOS TCH WITH (NOLOCK)  ON CCH.ID = TCH.CAMPO_OLD" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "    ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "   --Inserta los datos en todas las tablas (con los mismos valores que en la versi�n anterior) :" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO COPIA_CAMPO_ADJUN (ADJUN, CAMPO, NOM, IDIOMA, DATASIZE, PER, COMENT, FECALTA)" & vbCrLf
sConsulta = sConsulta & "   SELECT CA.ADJUN, TC.CAMPO_NEW, CA.NOM, CA.IDIOMA, CA.DATASIZE, CA.PER, CA.COMENT, CA.FECALTA" & vbCrLf
sConsulta = sConsulta & "   FROM COPIA_CAMPO_ADJUN CA WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN #TEMPORAL_CAMPOS TC WITH (NOLOCK)  ON CA.CAMPO = TC.CAMPO_OLD" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "   INSERT INTO  #TEMPORAL_CAMPO_ADJUN (ADJUN_OLD ,ADJUN_NEW )" & vbCrLf
sConsulta = sConsulta & "   SELECT CCA_OLD.ID ,CCA_NEW.ID   " & vbCrLf
sConsulta = sConsulta & "   FROM COPIA_CAMPO_ADJUN CCA_OLD  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN COPIA_CAMPO_ADJUN CCA_NEW WITH (NOLOCK)  ON CCA_OLD.ADJUN = CCA_NEW.ADJUN " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN #TEMPORAL_CAMPOS TC  WITH (NOLOCK) ON CCA_OLD.CAMPO = TC.CAMPO_old AND CCA_NEW.CAMPO = TC.CAMPO_NEW" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "   UPDATE #TEMPORAL_ADJUN SET ADJUN = TC.ADJUN_NEW FROM #TEMPORAL_ADJUN T  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN #TEMPORAL_CAMPO_ADJUN  TC WITH (NOLOCK)  ON T.ADJUN = TC.ADJUN_OLD" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "   INSERT INTO COPIA_DESGLOSE (CAMPO_PADRE, CAMPO_HIJO)  SELECT DISTINCT CCP.CAMPO_NEW, CCH.CAMPO_NEW" & vbCrLf
sConsulta = sConsulta & "   FROM COPIA_DESGLOSE CD WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN #TEMPORAL_CAMPOS CCP WITH (NOLOCK)  ON CD.CAMPO_PADRE = CCP.CAMPO_OLD" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN #TEMPORAL_CAMPOS CCH WITH (NOLOCK)  ON CD.CAMPO_HIJO = CCH.CAMPO_OLD" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "   --Inserta en COPIA_LINEA_DESGLOSE los campos que hay en la temporal con LINEA la antigua" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO COPIA_LINEA_DESGLOSE (LINEA, CAMPO_PADRE, CAMPO_HIJO, VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL)" & vbCrLf
sConsulta = sConsulta & "   SELECT TD.LINEA, CCP.CAMPO_NEW, CCH.CAMPO_NEW, CL.VALOR_NUM" & vbCrLf
sConsulta = sConsulta & "   ,  CL.VALOR_TEXT, CL.VALOR_FEC, CL.VALOR_BOOL" & vbCrLf
sConsulta = sConsulta & "   FROM COPIA_LINEA_DESGLOSE CL WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN #TEMPORAL_CAMPOS CCP  WITH (NOLOCK) ON CL.CAMPO_PADRE = CCP.CAMPO_OLD" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN #TEMPORAL_CAMPOS CCH WITH (NOLOCK)  ON CL.CAMPO_HIJO = CCH.CAMPO_OLD" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN COPIA_CAMPO CC WITH (NOLOCK)  ON CC.ID = CL.CAMPO_HIJO" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN COPIA_CAMPO_DEF CCD WITH (NOLOCK)  ON CCD.ID = CC.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PM_COPIA_CONF_CUMP_BLOQUE CCB WITH (NOLOCK)  ON CCB.CAMPO = cc.copia_campo_def AND ROL=@ROL AND BLOQUE=@BLOQUE" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN #TEMPORAL_DESGLOSE TD WITH (NOLOCK) ON CCP.CAMPO_NEW = TD.CAMPO_PADRE AND CCH.CAMPO_NEW = TD.CAMPO_HIJO AND CL.LINEA = TD.LINEA_OLD" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   --los que ya estaban en desgloses" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO COPIA_LINEA_DESGLOSE_ADJUN (CAMPO_PADRE, CAMPO_HIJO, LINEA, ADJUN, NOM, IDIOMA, DATASIZE, PER, COMENT, FECALTA)" & vbCrLf
sConsulta = sConsulta & "   SELECT CCP.CAMPO_NEW, CCH.CAMPO_NEW, TD.LINEA, CL.ADJUN,CL.NOM, CL.IDIOMA, CL.DATASIZE, CL.PER, CL.COMENT, CL.FECALTA" & vbCrLf
sConsulta = sConsulta & "   FROM COPIA_LINEA_DESGLOSE_ADJUN CL WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN #TEMPORAL_CAMPOS CCP  WITH (NOLOCK) ON CL.CAMPO_PADRE = CCP.CAMPO_OLD" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN #TEMPORAL_CAMPOS CCH WITH (NOLOCK)  ON CL.CAMPO_HIJO = CCH.CAMPO_OLD" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN #TEMPORAL_DESGLOSE TD WITH (NOLOCK) ON CCP.CAMPO_NEW = TD.CAMPO_PADRE AND CCH.CAMPO_NEW = TD.CAMPO_HIJO AND CL.LINEA = TD.LINEA_OLD" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "   --Trazabilidad lineas PM con los ERP" & vbCrLf
sConsulta = sConsulta & "   IF @SENTIDO = 1" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "   ----Inserta en COPIA_LINEA  los campos que hay en la temporal con LINEA la antigua" & vbCrLf
sConsulta = sConsulta & "      INSERT INTO COPIA_LINEA (INSTANCIA, NUM_VERSION, CAMPO_PADRE, LINEA, NUM_LINEA_SAP)" & vbCrLf
sConsulta = sConsulta & "      SELECT DISTINCT @ID, @NUM_VERSION,  CCP.CAMPO_NEW, TD.LINEA, CL.NUM_LINEA_SAP" & vbCrLf
sConsulta = sConsulta & "      FROM COPIA_LINEA CL WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "      INNER JOIN #TEMPORAL_CAMPOS CCP  WITH (NOLOCK) ON CL.CAMPO_PADRE = CCP.CAMPO_OLD" & vbCrLf
sConsulta = sConsulta & "          INNER JOIN #TEMPORAL_DESGLOSE TD WITH (NOLOCK) ON CCP.CAMPO_NEW = TD.CAMPO_PADRE AND CL.LINEA = TD.LINEA_OLD" & vbCrLf
sConsulta = sConsulta & "      IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "   DELETE FROM #TEMPORAL_CAMPO_ADJUN" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "   -- Guarda los ID de COPIA_LINEA_DESGLOSE_ADJUN para los adjuntos que ya existian" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO  #TEMP_CAMPO_ADJUN (ADJUN_OLD ,ADJUN_NEW,LINEA )" & vbCrLf
sConsulta = sConsulta & "   SELECT CA.ID ,CCA_NEW.ID,CCA_NEW.LINEA" & vbCrLf
sConsulta = sConsulta & "   FROM COPIA_LINEA_DESGLOSE_ADJUN CCA_OLD WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN COPIA_ADJUN CCA WITH (NOLOCK) ON CCA_OLD.ADJUN = CCA.ID" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN CAMPO_ADJUN CA WITH (NOLOCK) ON CA.ID = CCA.ADJUN_ORIGEN " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN COPIA_LINEA_DESGLOSE_ADJUN CCA_NEW  WITH (NOLOCK)  ON CCA.ID = CCA_NEW.ADJUN " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN #TEMPORAL_CAMPOS TC  WITH (NOLOCK) ON CCA_OLD.CAMPO_HIJO = TC.CAMPO_old AND CCA_NEW.CAMPO_HIJO = TC.CAMPO_NEW" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   UPDATE #TEMPORAL_DESGLOSE_ADJUN SET ADJUN = TC.ADJUN_NEW " & vbCrLf
sConsulta = sConsulta & "   FROM #TEMPORAL_DESGLOSE_ADJUN T WITH (NOLOCK)  " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN #TEMP_CAMPO_ADJUN  TC WITH (NOLOCK)  ON T.ADJUN = TC.ADJUN_OLD AND T.LINEA = TC.LINEA" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   ------------------------------------------------Ahora actualiza las tablas con los valores modificados desde el PM/QA----------------------------------------------------------------------------------------------------------------------------------------- " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   --Actualizamos los campos con los valores del formulario modificables por el usuario --La Actualizacion del campo ARTICULO, (TIPO_CAMPO_GS = 119) tiene que ver con el DOC. DES_PRT_PM_ ID20" & vbCrLf
sConsulta = sConsulta & "   UPDATE COPIA_CAMPO SET VALOR_NUM = T.VALOR_NUM" & vbCrLf
sConsulta = sConsulta & "   , VALOR_TEXT = T.VALOR_TEXT , VALOR_FEC = T.VALOR_FEC, VALOR_BOOL=T.VALOR_BOOL" & vbCrLf
sConsulta = sConsulta & "   FROM COPIA_CAMPO C  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN COPIA_CAMPO_DEF CD WITH (NOLOCK)  ON C.COPIA_CAMPO_DEF=CD.ID" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN #TEMPORAL T WITH (NOLOCK)  ON C.ID = T.CAMPO" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PM_COPIA_CONF_CUMP_BLOQUE CCB WITH (NOLOCK)  ON C.COPIA_CAMPO_DEF=CCB.CAMPO AND ROL=@ROL AND BLOQUE=@BLOQUE AND (ESCRITURA=1  OR (ESCRITURA=0 AND CD.FORMULA IS NOT NULL) OR (VISIBLE = 0 AND( CD.TIPO_CAMPO_GS=119 or  CD.TIPO_CAMPO_GS=103  or  CD.TIPO_CAMPO_GS=105))  OR (ESCRITURA=0 AND CD.TIPO_CAMPO_GS=118))" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "   --Actualizamos a sucio los campos de versiones anteriores que han cambiado con la nueva versi�n:" & vbCrLf
sConsulta = sConsulta & "   UPDATE COPIA_CAMPO SET SUCIO = 1 FROM COPIA_CAMPO C WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN (SELECT * FROM COPIA_CAMPO WITH (NOLOCK)  WHERE INSTANCIA = @ID AND NUM_VERSION= @NUM_VERSION) CNEW" & vbCrLf
sConsulta = sConsulta & "   ON C.COPIA_CAMPO_DEF = CNEW.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "   WHERE C.INSTANCIA = @ID AND C.NUM_VERSION = @OLD_VERSION" & vbCrLf
sConsulta = sConsulta & "   AND (C.VALOR_NUM<>CNEW.VALOR_NUM OR C.VALOR_FEC <> CNEW.VALOR_FEC OR C.VALOR_TEXT <>CNEW.VALOR_TEXT OR C.VALOR_BOOL <> CNEW.VALOR_BOOL)" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "   --Eliminamos los adjuntos de los campos tipo form que ya no est�n en esta nueva versi�n" & vbCrLf
sConsulta = sConsulta & "   --Solo eliminamos los que no estan en #temporal_adjun y ademas son visibles y modificables" & vbCrLf
sConsulta = sConsulta & "   DELETE FROM COPIA_CAMPO_ADJUN FROM COPIA_CAMPO_ADJUN CCA WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN #TEMPORAL T WITH (NOLOCK)  ON CCA.CAMPO = T.CAMPO" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN COPIA_CAMPO C  WITH (NOLOCK) on CCA.CAMPO = C.ID" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PM_COPIA_CONF_CUMP_BLOQUE CCB WITH (NOLOCK)  ON BLOQUE=@BLOQUE AND ROL=@ROL AND C.COPIA_CAMPO_DEF=CCB.CAMPO  AND ESCRITURA=1  AND  VISIBLE=1" & vbCrLf
sConsulta = sConsulta & "   WHERE NOT EXISTS (SELECT * FROM #TEMPORAL_ADJUN TA WITH (NOLOCK)  WHERE CCA.ID = TA.ADJUN AND TA.TIPO = 1 and ta.portal=0 ) and not exists (SELECT * FROM #TEMPORAL_ADJUN TA WITH (NOLOCK) WHERE CCA.ADJUN = TA.ADJUN AND TA.TIPO = 1 and ta.portal=1 ) " & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "   --Insertamos los adjuntos de los campos tipo form (Insertados desde el PM)" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO COPIA_CAMPO_ADJUN (ADJUN, CAMPO, NOM, IDIOMA, DATASIZE, PER, COMENT, FECALTA)" & vbCrLf
sConsulta = sConsulta & "   SELECT CA.ID, T.CAMPO, CA.NOM, 'SPA', DATALENGTH(CA.DATA), CA.PER, CA.COMENT, CA.FECALTA" & vbCrLf
sConsulta = sConsulta & "   FROM COPIA_ADJUN CA  WITH (NOLOCK) INNER JOIN #TEMPORAL_ADJUN T WITH (NOLOCK)  ON CA.ID = T.ADJUN " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN COPIA_CAMPO C WITH (NOLOCK)  ON T.CAMPO=C.ID" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PM_COPIA_CONF_CUMP_BLOQUE CCB WITH (NOLOCK)  ON C.COPIA_CAMPO_DEF=CCB.CAMPO AND ROL=@ROL AND BLOQUE=@BLOQUE AND ESCRITURA=1" & vbCrLf
sConsulta = sConsulta & "   WHERE T.TIPO = 2 AND T.PORTAL = 0" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "   --Insertamos los adjuntos metidos en el portal por el proveedor" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO COPIA_CAMPO_ADJUN (ADJUN, CAMPO, NOM, IDIOMA, DATASIZE, COMENT, FECALTA, PROVE)" & vbCrLf
sConsulta = sConsulta & "   SELECT CA.ID, T.CAMPO, CA.NOM, 'SPA', CCA.DATASIZE, CA.COMENT, CA.FECALTA, CA.PROVE" & vbCrLf
sConsulta = sConsulta & "   FROM COPIA_ADJUN CA  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN (P_COPIA_CAMPO_ADJUN CCA WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "    inner join p_copia_campo c  WITH (NOLOCK) on cca.campo = c.id and c.instancia = @ID" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN #TEMPORAL_ADJUN T WITH (NOLOCK)  ON CCA.ADJUN_PORTAL = T.ADJUN" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN COPIA_CAMPO   WITH (NOLOCK) ON T.CAMPO=COPIA_CAMPO.ID" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PM_COPIA_CONF_CUMP_BLOQUE CCB  WITH (NOLOCK) ON BLOQUE=@BLOQUE AND ROL=@ROL AND COPIA_CAMPO.COPIA_CAMPO_DEF=CCB.CAMPO  AND ESCRITURA=1" & vbCrLf
sConsulta = sConsulta & "   ) ON CA.ADJUN_ORIGEN = CCA.id  AND CA.ADJUN_PORTAL = CCA.ADJUN_PORTAL" & vbCrLf
sConsulta = sConsulta & "   WHERE T.TIPO = 2 AND T.PORTAL = 1 and ca.portal = 1 and ca.prove=cca.prove" & vbCrLf
sConsulta = sConsulta & "   --Ojo el id que da el portal para los adjuntos no tiene en cuenta el id que da el PM -> Portal dice 143 PM dice 143 pero uno es 'Archivo.txt' y otro es 'Otro Archivo.txt' -> controla el prove" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   -- ELIMINAMOS LOS ADJUNTOS QUE HAYAN SIDO ELIMINADOS" & vbCrLf
sConsulta = sConsulta & "   DELETE FROM COPIA_LINEA_DESGLOSE_ADJUN " & vbCrLf
sConsulta = sConsulta & "   FROM COPIA_LINEA_DESGLOSE_ADJUN CLDA WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN #TEMPORAL_DESGLOSE TD WITH (NOLOCK)  ON CLDA.CAMPO_PADRE = TD.CAMPO_PADRE AND CLDA.CAMPO_HIJO = TD.CAMPO_HIJO AND CLDA.LINEA = TD.LINEA--_OLD" & vbCrLf
sConsulta = sConsulta & "   inner join copia_campo cc WITH (NOLOCK)  on cc.id = clda.campo_hijo" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PM_COPIA_CONF_CUMP_BLOQUE CCB  WITH (NOLOCK) ON BLOQUE=@BLOQUE AND ROL=@ROL AND CCB.CAMPO=cc.copia_campo_def   AND  VISIBLE=1  AND ESCRITURA=1" & vbCrLf
sConsulta = sConsulta & "   WHERE NOT EXISTS (SELECT  * FROM #TEMPORAL_DESGLOSE_ADJUN TDA  WITH (NOLOCK) WHERE CLDA.ID = TDA.ADJUN AND TDA.TIPO = 1  AND TDA.PORTAL = 0  AND TDA.LINEA = CLDA.LINEA) " & vbCrLf
sConsulta = sConsulta & "   AND NOT EXISTS (SELECT  * FROM #TEMPORAL_DESGLOSE_ADJUN TDA  WITH (NOLOCK) WHERE CLDA.ADJUN = TDA.ADJUN AND TDA.TIPO = 1  AND TDA.PORTAL = 1)" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   DELETE FROM COPIA_LINEA_DESGLOSE_ADJUN " & vbCrLf
sConsulta = sConsulta & "   FROM COPIA_LINEA_DESGLOSE_ADJUN CLDA WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN #TEMPORAL_DESGLOSE TD WITH (NOLOCK)  ON CLDA.CAMPO_PADRE = TD.CAMPO_PADRE AND CLDA.CAMPO_HIJO = TD.CAMPO_HIJO " & vbCrLf
sConsulta = sConsulta & "   WHERE NOT EXISTS (SELECT * FROM #TEMPORAL_DESGLOSE TD2  WITH (NOLOCK) WHERE CLDA.CAMPO_PADRE=TD2.CAMPO_PADRE AND CLDA.CAMPO_HIJO = TD2.CAMPO_HIJO AND CLDA.LINEA = TD2.LINEA)" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   --ELIMINAMOS Defecto iINVISIBLE EL CAMPO VISIBLE EL DESGLOSE" & vbCrLf
sConsulta = sConsulta & "   --Pop y no Pop se comportan distinto as� que lo meto todo y con esto borro lo que sabra (sobra pop/no pop podr�a quitar desde codigo. Pero creo q del delete este no me libro a menos que vaya a mirar si es pop)" & vbCrLf
sConsulta = sConsulta & "     DELETE FROM COPIA_LINEA_DESGLOSE_ADJUN" & vbCrLf
sConsulta = sConsulta & "     FROM COPIA_LINEA_DESGLOSE_ADJUN CLDA WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "     INNER JOIN #TEMPORAL_DESGLOSE TD WITH (NOLOCK)  ON CLDA.CAMPO_PADRE = TD.CAMPO_PADRE AND CLDA.CAMPO_HIJO = TD.CAMPO_HIJO AND CLDA.LINEA = TD.LINEA" & vbCrLf
sConsulta = sConsulta & "     INNER JOIN COPIA_CAMPO CCHIJO WITH (NOLOCK)  on CCHIJO.ID = CLDA.CAMPO_HIJO" & vbCrLf
sConsulta = sConsulta & "     INNER JOIN COPIA_CAMPO CCPADRE WITH (NOLOCK) on CCPADRE.ID = CLDA.CAMPO_PADRE" & vbCrLf
sConsulta = sConsulta & "     INNER JOIN PM_COPIA_CONF_CUMP_BLOQUE CCBHIJO  WITH (NOLOCK) ON CCBHIJO.BLOQUE=@BLOQUE AND CCBHIJO.ROL=@ROL AND CCBHIJO.CAMPO=CCHIJO.copia_campo_def " & vbCrLf
sConsulta = sConsulta & "           AND  (CCBHIJO.VISIBLE=0 OR (CCBHIJO.VISIBLE=1 AND ESCRITURA=0))" & vbCrLf
sConsulta = sConsulta & "     INNER JOIN PM_COPIA_CONF_CUMP_BLOQUE CCBPADRE WITH (NOLOCK) ON CCBPADRE.CAMPO=CCPADRE.copia_campo_def AND CCBPADRE.ROL=@ROL AND CCBPADRE.BLOQUE=@BLOQUE   AND  CCBPADRE.VISIBLE=1 --AND ESCRITURA=1" & vbCrLf
sConsulta = sConsulta & "     WHERE NOT EXISTS (SELECT  * FROM #TEMPORAL_DESGLOSE_ADJUN TDA  WITH (NOLOCK) WHERE CLDA.ID = TDA.ADJUN AND TDA.TIPO = 1  AND TDA.PORTAL = 0  AND TDA.LINEA = CLDA.LINEA)" & vbCrLf
sConsulta = sConsulta & "           AND NOT EXISTS (SELECT  * FROM #TEMPORAL_DESGLOSE_ADJUN TDA  WITH (NOLOCK) WHERE CLDA.ADJUN = TDA.ADJUN AND TDA.TIPO = 1  AND TDA.PORTAL = 1)" & vbCrLf
sConsulta = sConsulta & "     IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   --La Actualizacion del campo ARTICULO, (TIPO_CAMPO_GS = 119) tiene que ver con el DOC. DES_PRT_PM_ ID20 (Cuando unicamente este visible el Material)" & vbCrLf
sConsulta = sConsulta & "   UPDATE COPIA_LINEA_DESGLOSE" & vbCrLf
sConsulta = sConsulta & "   SET VALOR_TEXT =   T.VALOR_TEXT , VALOR_NUM = T.VALOR_NUM,VALOR_FEC = T.VALOR_FEC,VALOR_BOOL = T.VALOR_BOOL--,LINEA = T.LINEA" & vbCrLf
sConsulta = sConsulta & "   FROM COPIA_LINEA_DESGLOSE CLD WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN #TEMPORAL_DESGLOSE T  WITH (NOLOCK) ON CLD.CAMPO_PADRE = T.CAMPO_PADRE AND CLD.CAMPO_HIJO = T.CAMPO_HIJO AND CLD.LINEA = T.LINEA" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN COPIA_CAMPO C WITH (NOLOCK)  ON T.CAMPO_HIJO=C.ID" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN COPIA_CAMPO_DEF CD WITH (NOLOCK)  ON C.COPIA_CAMPO_DEF=CD.ID" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PM_COPIA_CONF_CUMP_BLOQUE CCB WITH (NOLOCK)  ON BLOQUE=@BLOQUE AND ROL=@ROL AND C.COPIA_CAMPO_DEF=CCB.CAMPO  AND  (ESCRITURA=1 OR (ESCRITURA=0 AND FORMULA IS NOT NULL)  OR (VISIBLE = 0 AND( CD.TIPO_CAMPO_GS=119 or  CD.TIPO_CAMPO_GS=103  or  CD.TIPO_CAMPO_GS=105))  OR (ESCRITURA=0 AND CD.TIPO_CAMPO_GS=118) OR (ESCRITURA=0 AND CD.TIPO_CAMPO_GS=105) OR (ESCRITURA=0 AND CD.TIPO_CAMPO_GS=123) OR (ESCRITURA=0 AND CD.TIPO_CAMPO_GS=124))" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO COPIA_LINEA_DESGLOSE (LINEA, CAMPO_PADRE, CAMPO_HIJO, VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL)" & vbCrLf
sConsulta = sConsulta & "   SELECT T.LINEA, T.CAMPO_PADRE, T.CAMPO_HIJO, T.VALOR_NUM" & vbCrLf
sConsulta = sConsulta & "   , T.VALOR_TEXT , T.VALOR_FEC, T.VALOR_BOOL " & vbCrLf
sConsulta = sConsulta & "   FROM #TEMPORAL_DESGLOSE T WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN COPIA_CAMPO C WITH (NOLOCK)  ON T.CAMPO_HIJO=C.ID" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN COPIA_CAMPO_DEF CD WITH (NOLOCK)  ON C.COPIA_CAMPO_DEF=CD.ID" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PM_COPIA_CONF_CUMP_BLOQUE CCB WITH (NOLOCK)  ON C.COPIA_CAMPO_DEF=CCB.CAMPO AND ROL=@ROL AND BLOQUE=@BLOQUE" & vbCrLf
sConsulta = sConsulta & "   WHERE NOT EXISTS (SELECT * FROM COPIA_LINEA_DESGLOSE CLD WITH (NOLOCK)  WHERE T.LINEA=CLD.LINEA AND T.CAMPO_PADRE = CLD.CAMPO_PADRE AND T.CAMPO_HIJO = CLD.CAMPO_HIJO)" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO COPIA_LINEA_DESGLOSE_ADJUN(CAMPO_PADRE, CAMPO_HIJO, LINEA, ADJUN, NOM, IDIOMA, DATASIZE, PER, COMENT, FECALTA)" & vbCrLf
sConsulta = sConsulta & "   SELECT DISTINCT T.CAMPO_PADRE, T.CAMPO_HIJO,  T.LINEA, CA.ID, CA.NOM, 'SPA', DATALENGTH(CA.DATA), CA.PER, CA.COMENT, GETDATE()" & vbCrLf
sConsulta = sConsulta & "     FROM COPIA_ADJUN CA  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "          INNER JOIN COPIA_LINEA_DESGLOSE_ADJUN CLDA WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                  ON CA.ID = CLDA.ADJUN" & vbCrLf
sConsulta = sConsulta & "      INNER JOIN #TEMP_CAMPO_ADJUN TC WITH (NOLOCK)  ON TC.ADJUN_NEW = CLDA.ID" & vbCrLf
sConsulta = sConsulta & "          INNER JOIN #TEMPORAL_DESGLOSE_ADJUN T WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                 ON T.ADJUN = TC.ADJUN_OLD" & vbCrLf
sConsulta = sConsulta & "                 AND T.CAMPO_PADRE = CLDA.CAMPO_PADRE" & vbCrLf
sConsulta = sConsulta & "                 AND T.CAMPO_HIJO = CLDA.CAMPO_HIJO" & vbCrLf
sConsulta = sConsulta & "   WHERE NOT EXISTS (SELECT * FROM COPIA_LINEA_DESGLOSE_ADJUN CLDA2 WITH (NOLOCK)  WHERE T.CAMPO_PADRE = CLDA2.CAMPO_PADRE AND T.CAMPO_HIJO = CLDA2.CAMPO_HIJO AND T.LINEA = CLDA2.LINEA) AND T.TIPO = 1" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO COPIA_LINEA_DESGLOSE_ADJUN(CAMPO_PADRE, CAMPO_HIJO, LINEA, ADJUN, NOM, IDIOMA, DATASIZE, PER, COMENT, FECALTA)" & vbCrLf
sConsulta = sConsulta & "   SELECT DISTINCT TDA.CAMPO_PADRE, TDA.CAMPO_HIJO, TDA.LINEA, CA.ID, CA.NOM, 'SPA', DATALENGTH(CA.DATA), CA.PER, CA.COMENT, GETDATE()" & vbCrLf
sConsulta = sConsulta & "   FROM #TEMPORAL_DESGLOSE_ADJUN TDA WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN COPIA_ADJUN CA WITH (NOLOCK) ON TDA.ADJUN = CA.ADJUN_ORIGEN" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN COPIA_LINEA_DESGLOSE_ADJUN CLDA WITH (NOLOCK) ON CLDA.ADJUN = CA.ID" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN COPIA_CAMPO CC WITH (NOLOCK) ON CLDA.CAMPO_PADRE = CC.ID AND CC.INSTANCIA= @ID AND CC.NUM_VERSION = @OLD_VERSION" & vbCrLf
sConsulta = sConsulta & "   WHERE CA.PORTAL <> 1" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO COPIA_LINEA_DESGLOSE_ADJUN (CAMPO_PADRE, CAMPO_HIJO, LINEA,ADJUN, NOM, IDIOMA, DATASIZE, COMENT, FECALTA, PROVE)" & vbCrLf
sConsulta = sConsulta & "   SELECT T.CAMPO_PADRE, T.CAMPO_HIJO, T.LINEA, CA.ID, CA.NOM, 'SPA', CCA.DATASIZE, CA.COMENT, CA.FECALTA, CA.PROVE" & vbCrLf
sConsulta = sConsulta & "     FROM COPIA_ADJUN CA  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN P_COPIA_LINEA_DESGLOSE_ADJUN CCA WITH (NOLOCK) ON  CA.ADJUN_PORTAL = CCA.ADJUN_PORTAL AND CA.ADJUN_ORIGEN = CCA.id AND CCA.PROVE=CA.PROVE" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN #TEMPORAL_DESGLOSE_ADJUN T WITH (NOLOCK) ON CCA.ADJUN_PORTAL = T.ADJUN" & vbCrLf
sConsulta = sConsulta & "   WHERE T.TIPO = 2 AND T.PORTAL = 1 and ca.portal = 1" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO COPIA_LINEA_DESGLOSE_ADJUN(CAMPO_PADRE, CAMPO_HIJO, LINEA, ADJUN, NOM, IDIOMA, DATASIZE, PER, COMENT, FECALTA)" & vbCrLf
sConsulta = sConsulta & "   SELECT T.CAMPO_PADRE, T.CAMPO_HIJO,  T.LINEA, CA.ID, CA.NOM, 'SPA', DATALENGTH(CA.DATA), CA.PER, CA.COMENT, GETDATE()" & vbCrLf
sConsulta = sConsulta & "     FROM COPIA_ADJUN CA  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN #TEMPORAL_DESGLOSE_ADJUN T WITH (NOLOCK) ON CA.ID = T.ADJUN " & vbCrLf
sConsulta = sConsulta & "   WHERE T.TIPO = 2   AND T.PORTAL <> 1" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   --Trazabilidad l�neas PM con los ERP" & vbCrLf
sConsulta = sConsulta & "   IF @SENTIDO = 1" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "      INSERT INTO COPIA_LINEA (INSTANCIA, NUM_VERSION, LINEA, CAMPO_PADRE)" & vbCrLf
sConsulta = sConsulta & "      SELECT DISTINCT @ID, @NUM_VERSION, T.LINEA, T.CAMPO_PADRE " & vbCrLf
sConsulta = sConsulta & "      FROM #TEMPORAL_DESGLOSE T WITH (NOLOCK)  WHERE NOT EXISTS (SELECT * FROM COPIA_LINEA CL WITH (NOLOCK)  WHERE T.LINEA=CL.LINEA AND T.CAMPO_PADRE = CL.CAMPO_PADRE)" & vbCrLf
sConsulta = sConsulta & "      IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "   --  Guarda los participantes que haya que poner en esta etapa" & vbCrLf
sConsulta = sConsulta & "   UPDATE PM_COPIA_ROL " & vbCrLf
sConsulta = sConsulta & "   SET PER=CASE WHEN U.FSWS_SUSTITUCION IS NULL THEN CASE WHEN T.PER='' THEN NULL ELSE T.PER END ELSE U.FSWS_SUSTITUCION END, " & vbCrLf
sConsulta = sConsulta & "       PER_ORIGINAL=T.PER," & vbCrLf
sConsulta = sConsulta & "       PROVE=CASE WHEN T.PROVE='' THEN NULL ELSE T.PROVE END," & vbCrLf
sConsulta = sConsulta & "       CON=CASE WHEN T.CON=0  THEN NULL ELSE T.CON END" & vbCrLf
sConsulta = sConsulta & "      FROM #TEMPORAL_PARTICIPANTES T  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "      left join USU U WITH (NOLOCK) on T.PER = U.PER" & vbCrLf
sConsulta = sConsulta & "      inner join PM_COPIA_ROL PCR WITH (NOLOCK) on PCR.ID = T.ROL" & vbCrLf
sConsulta = sConsulta & "      inner join INSTANCIA WITH (NOLOCK)  ON PCR.INSTANCIA=INSTANCIA.ID AND (INSTANCIA.ESTADO=0 or INSTANCIA.ESTADO=2 or INSTANCIA.ESTADO=6)" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "   --Actualizar la tabla PM_COPIA_ROL, para establecer roles mediante campos" & vbCrLf
sConsulta = sConsulta & "   --Roles de tipo Comprador o usuario" & vbCrLf
sConsulta = sConsulta & "   UPDATE PM_COPIA_ROL" & vbCrLf
sConsulta = sConsulta & "   SET PER = CASE WHEN U.FSWS_SUSTITUCION IS NULL THEN CC.VALOR_TEXT ELSE U.FSWS_SUSTITUCION END,PER_ORIGINAL=CC.VALOR_TEXT" & vbCrLf
sConsulta = sConsulta & "   FROM COPIA_CAMPO CC WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   left join USU U WITH (NOLOCK) on CC.VALOR_TEXT = U.PER" & vbCrLf
sConsulta = sConsulta & "   inner join PM_COPIA_ROL PCR WITH (NOLOCK) on PCR.CAMPO = CC.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "   inner join INSTANCIA WITH (NOLOCK)  ON PCR.INSTANCIA=INSTANCIA.ID AND (INSTANCIA.ESTADO=0 or INSTANCIA.ESTADO=2 or INSTANCIA.ESTADO=6)" & vbCrLf
sConsulta = sConsulta & "   WHERE (PCR.TIPO = 1 OR PCR.TIPO = 3)" & vbCrLf
sConsulta = sConsulta & "      AND PCR.INSTANCIA = @ID" & vbCrLf
sConsulta = sConsulta & "      AND PCR.CUANDO_ASIGNAR = 1" & vbCrLf
sConsulta = sConsulta & "      AND PCR.BLOQUE IN (SELECT BLOQUE" & vbCrLf
sConsulta = sConsulta & "           FROM INSTANCIA_BLOQUE WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                      WHERE ESTADO = 1" & vbCrLf
sConsulta = sConsulta & "                      AND INSTANCIA = @ID)" & vbCrLf
sConsulta = sConsulta & "      AND PCR.COMO_ASIGNAR = 1" & vbCrLf
sConsulta = sConsulta & "      AND CC.INSTANCIA = @ID" & vbCrLf
sConsulta = sConsulta & "      AND CC.NUM_VERSION = @NUM_VERSION" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_ " & vbCrLf
sConsulta = sConsulta & "   --Roles de tipo Proveedor" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = N'UPDATE PM_COPIA_ROL SET PROVE = cast(CC.VALOR_TEXT as '" & vbCrLf
sConsulta = sConsulta & "   SET @sQL = @sQL + (select 'varchar(' + convert(varchar,longitud) + ')' from DIC where nombre='PROVE') " & vbCrLf
sConsulta = sConsulta & "   SET @sQL = @sQL + ')    FROM COPIA_CAMPO CC WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "       WHERE PM_COPIA_ROL.TIPO = 2" & vbCrLf
sConsulta = sConsulta & "       AND PM_COPIA_ROL.INSTANCIA = @ID" & vbCrLf
sConsulta = sConsulta & "       AND PM_COPIA_ROL.CUANDO_ASIGNAR = 1" & vbCrLf
sConsulta = sConsulta & "       AND PM_COPIA_ROL.BLOQUE IN (SELECT BLOQUE" & vbCrLf
sConsulta = sConsulta & "                             FROM INSTANCIA_BLOQUE WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                          WHERE ESTADO = 1" & vbCrLf
sConsulta = sConsulta & "                          AND INSTANCIA = @ID)" & vbCrLf
sConsulta = sConsulta & "          AND PM_COPIA_ROL.COMO_ASIGNAR = 1" & vbCrLf
sConsulta = sConsulta & "          AND PM_COPIA_ROL.CAMPO = CC.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "          AND CC.INSTANCIA = @ID" & vbCrLf
sConsulta = sConsulta & "          AND CC.NUM_VERSION = @NUM_VERSION'" & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL,N'@ID INT, @NUM_VERSION INT', @ID=@ID,@NUM_VERSION=@NUM_VERSION" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   --Devuelve el id de la acci�n de copia_campo: " & vbCrLf
sConsulta = sConsulta & "   IF @ACCION<>0  --Si no est� guardando:" & vbCrLf
sConsulta = sConsulta & "   SET @ACCION=(SELECT PM_COPIA_ACCIONES.ID FROM PM_COPIA_ACCIONES  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PM_COPIA_BLOQUE B  WITH (NOLOCK) ON PM_COPIA_ACCIONES.BLOQUE=B.ID WHERE B.INSTANCIA=@ID AND  ACCION_ORIGEN=@ACCION)" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--HHF 31/10/2007 FIN" & vbCrLf
sConsulta = sConsulta & "--JGA 10/12/2007 INI" & vbCrLf
sConsulta = sConsulta & "DECLARE @TITULO_ID INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @TIPO_CAMPO_GS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @TITULO_TEXT VARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "DECLARE @TITULO_FEC DATETIME" & vbCrLf
sConsulta = sConsulta & "DECLARE @VALOR_TEXT VARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SUBTIPO INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @FEC_NECESIDAD DATETIME" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDIOMA VARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Obtener el IDIOMA de PER_MODIF" & vbCrLf
sConsulta = sConsulta & "SET @IDIOMA = (SELECT TOP 1 IDIOMA FROM USU WITH (NOLOCK)  WHERE PER = @PETICIONARIO)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Buscar si alg�n campo de la instancia es de fecha de necesidad" & vbCrLf
sConsulta = sConsulta & "SET @FEC_NECESIDAD = (SELECT TOP 1 VALOR_FEC FROM COPIA_CAMPO CC WITH (NOLOCK) INNER JOIN COPIA_CAMPO_DEF CCD WITH (NOLOCK) ON CC.COPIA_CAMPO_DEF = CCD.ID WHERE TIPO_CAMPO_GS = 5 And INSTANCIA = @ID AND NUM_VERSION = @NUM_VERSION)" & vbCrLf
sConsulta = sConsulta & "--Diferenciar casos en funci�n de la existencia de t�tulo" & vbCrLf
sConsulta = sConsulta & "SET (SELECT DISTINCT @TITULO_ID = CCD.ID, @TIPO_CAMPO_GS = CCD.TIPO_CAMPO_GS, @VALOR_TEXT = CC.VALOR_TEXT, @SUBTIPO = CCD.SUBTIPO FROM COPIA_CAMPO CC  WITH (NOLOCK) INNER JOIN COPIA_CAMPO_DEF CCD WITH (NOLOCK) ON CC.COPIA_CAMPO_DEF = CCD.ID WHERE TITULO = 1 AND INSTANCIA = @ID AND NUM_VERSION = @NUM_VERSION)" & vbCrLf
sConsulta = sConsulta & "IF @TITULO_ID IS NOT NULL  -- Si existe T�tulo" & vbCrLf
sConsulta = sConsulta & "begin" & vbCrLf
sConsulta = sConsulta & "   IF @TIPO_CAMPO_GS IS NOT NULL -- Si el T�tulo es un campo de tipo GS" & vbCrLf
sConsulta = sConsulta & "   begin" & vbCrLf
sConsulta = sConsulta & "      IF @TIPO_CAMPO_GS = 100 -- Tipo Proveedor" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "   SET @TITULO_TEXT = (SELECT PR.COD + ' - ' + PR.DEN FROM COPIA_CAMPO CC  WITH (NOLOCK) INNER JOIN PROVE PR  WITH (NOLOCK) ON CC.VALOR_TEXT = PR.COD WHERE COPIA_CAMPO_DEF = @TITULO_ID AND INSTANCIA = @ID AND NUM_VERSION = @NUM_VERSION )" & vbCrLf
sConsulta = sConsulta & "      end" & vbCrLf
sConsulta = sConsulta & "      ELSE" & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "      IF @TIPO_CAMPO_GS = 102 -- Tipo Moneda" & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "   IF @IDIOMA = 'SPA'" & vbCrLf
sConsulta = sConsulta & "   begin" & vbCrLf
sConsulta = sConsulta & "   SET @TITULO_TEXT = (SELECT M.DEN_SPA FROM COPIA_CAMPO CC WITH (NOLOCK) INNER JOIN MON M WITH (NOLOCK) ON CC.VALOR_TEXT = M.COD WHERE COPIA_CAMPO_DEF = @TITULO_ID  AND INSTANCIA = @ID AND NUM_VERSION = @NUM_VERSION)" & vbCrLf
sConsulta = sConsulta & "       end" & vbCrLf
sConsulta = sConsulta & "   IF @IDIOMA = 'ENG'" & vbCrLf
sConsulta = sConsulta & "   begin" & vbCrLf
sConsulta = sConsulta & "   SET @TITULO_TEXT = (SELECT M.DEN_ENG FROM COPIA_CAMPO CC WITH (NOLOCK) INNER JOIN MON M WITH (NOLOCK) ON CC.VALOR_TEXT = M.COD WHERE COPIA_CAMPO_DEF = @TITULO_ID  AND INSTANCIA = @ID AND NUM_VERSION = @NUM_VERSION)" & vbCrLf
sConsulta = sConsulta & "       end" & vbCrLf
sConsulta = sConsulta & "   IF @IDIOMA = 'GER'" & vbCrLf
sConsulta = sConsulta & "   begin" & vbCrLf
sConsulta = sConsulta & "   SET @TITULO_TEXT = (SELECT M.DEN_GER FROM COPIA_CAMPO CC WITH (NOLOCK) INNER JOIN MON M WITH (NOLOCK) ON CC.VALOR_TEXT = M.COD WHERE COPIA_CAMPO_DEF = @TITULO_ID  AND INSTANCIA = @ID AND NUM_VERSION = @NUM_VERSION)" & vbCrLf
sConsulta = sConsulta & "       end" & vbCrLf
sConsulta = sConsulta & "      end" & vbCrLf
sConsulta = sConsulta & "      ELSE" & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "      IF @TIPO_CAMPO_GS = 107 -- Tipo Pais" & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "   SET @TITULO_TEXT = (SELECT PA.DEN FROM COPIA_CAMPO CC WITH (NOLOCK) INNER JOIN PAI PA WITH (NOLOCK) ON CC.VALOR_TEXT = PA.COD WHERE COPIA_CAMPO_DEF = @TITULO_ID  AND INSTANCIA = @ID AND NUM_VERSION = @NUM_VERSION)" & vbCrLf
sConsulta = sConsulta & "      end" & vbCrLf
sConsulta = sConsulta & "      ELSE" & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "      IF @TIPO_CAMPO_GS = 108 -- Tipo Provincia" & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "   SET @TITULO_TEXT = (SELECT PRO.DEN FROM COPIA_CAMPO CC WITH (NOLOCK) INNER JOIN PROVI PRO WITH (NOLOCK) ON CC.VALOR_TEXT = PRO.COD WHERE COPIA_CAMPO_DEF = @TITULO_ID  AND INSTANCIA = @ID AND NUM_VERSION = @NUM_VERSION)" & vbCrLf
sConsulta = sConsulta & "      end" & vbCrLf
sConsulta = sConsulta & "      ELSE" & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "      IF @TIPO_CAMPO_GS = 109 -- Tipo Dest" & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "   IF @IDIOMA = 'SPA'" & vbCrLf
sConsulta = sConsulta & "   begin" & vbCrLf
sConsulta = sConsulta & "   SET @TITULO_TEXT = (SELECT D.DEN_SPA FROM COPIA_CAMPO CC WITH (NOLOCK) INNER JOIN DEST D WITH (NOLOCK) ON CC.VALOR_TEXT = D.COD WHERE COPIA_CAMPO_DEF = @TITULO_ID  AND INSTANCIA = @ID AND NUM_VERSION = @NUM_VERSION)" & vbCrLf
sConsulta = sConsulta & "   end" & vbCrLf
sConsulta = sConsulta & "   IF @IDIOMA = 'ENG'" & vbCrLf
sConsulta = sConsulta & "   begin" & vbCrLf
sConsulta = sConsulta & "   SET @TITULO_TEXT = (SELECT D.DEN_ENG FROM COPIA_CAMPO CC WITH (NOLOCK) INNER JOIN DEST D WITH (NOLOCK) ON CC.VALOR_TEXT = D.COD WHERE COPIA_CAMPO_DEF = @TITULO_ID  AND INSTANCIA = @ID AND NUM_VERSION = @NUM_VERSION)" & vbCrLf
sConsulta = sConsulta & "   end" & vbCrLf
sConsulta = sConsulta & "   IF @IDIOMA = 'GER'" & vbCrLf
sConsulta = sConsulta & "   begin" & vbCrLf
sConsulta = sConsulta & "   SET @TITULO_TEXT = (SELECT D.DEN_GER FROM COPIA_CAMPO CC WITH (NOLOCK) INNER JOIN DEST D WITH (NOLOCK) ON CC.VALOR_TEXT = D.COD WHERE COPIA_CAMPO_DEF = @TITULO_ID  AND INSTANCIA = @ID AND NUM_VERSION = @NUM_VERSION)" & vbCrLf
sConsulta = sConsulta & "   end" & vbCrLf
sConsulta = sConsulta & "     end" & vbCrLf
sConsulta = sConsulta & "      ELSE" & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "      IF @TIPO_CAMPO_GS = 115 -- Tipo Persona" & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "   SET @TITULO_TEXT = (SELECT PE.NOM + ' ' + PE.APE FROM COPIA_CAMPO CC WITH (NOLOCK) INNER JOIN PER PE WITH (NOLOCK) ON CC.VALOR_TEXT = PE.COD WHERE COPIA_CAMPO_DEF = @TITULO_ID AND INSTANCIA = @ID AND NUM_VERSION = @NUM_VERSION)" & vbCrLf
sConsulta = sConsulta & "      end" & vbCrLf
sConsulta = sConsulta & "      ELSE" & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "      IF @TIPO_CAMPO_GS = 122 -- Tipo Departamento" & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "   SET @TITULO_TEXT = (SELECT DE.COD + ' - ' + DE.DEN FROM COPIA_CAMPO CC WITH (NOLOCK) INNER JOIN DEP DE WITH (NOLOCK) ON CC.VALOR_TEXT = DE.COD WHERE COPIA_CAMPO_DEF = @TITULO_ID AND INSTANCIA = @ID AND NUM_VERSION = @NUM_VERSION)" & vbCrLf
sConsulta = sConsulta & "      end      " & vbCrLf
sConsulta = sConsulta & "      ELSE" & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "      IF @TIPO_CAMPO_GS = 123 -- Tipo Org Compras" & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "   SET @TITULO_TEXT = (SELECT O.COD + ' - ' + O.DEN FROM COPIA_CAMPO CC WITH (NOLOCK) INNER JOIN ORGCOMPRAS O WITH (NOLOCK) ON CC.VALOR_TEXT = O.COD WHERE COPIA_CAMPO_DEF = @TITULO_ID AND INSTANCIA = @ID AND NUM_VERSION = @NUM_VERSION)" & vbCrLf
sConsulta = sConsulta & "      end " & vbCrLf
sConsulta = sConsulta & "      ELSE" & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "      IF @TIPO_CAMPO_GS = 124 -- Tipo Centro" & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "   SET @TITULO_TEXT = (SELECT CE.COD + ' - ' + CE.DEN FROM COPIA_CAMPO CC WITH (NOLOCK) INNER JOIN CENTROS CE WITH (NOLOCK) ON CC.VALOR_TEXT = CE.COD WHERE COPIA_CAMPO_DEF = @TITULO_ID AND INSTANCIA = @ID AND NUM_VERSION = @NUM_VERSION)" & vbCrLf
sConsulta = sConsulta & "      end " & vbCrLf
sConsulta = sConsulta & "      ELSE" & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "      IF @TIPO_CAMPO_GS = 125 -- Tipo Almacen" & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "   SET @TITULO_TEXT = (SELECT AL.COD + ' - ' + AL.DEN FROM COPIA_CAMPO CC WITH (NOLOCK) INNER JOIN ALMACEN AL WITH (NOLOCK) ON CC.VALOR_TEXT = AL.COD WHERE COPIA_CAMPO_DEF = @TITULO_ID AND INSTANCIA = @ID AND NUM_VERSION = @NUM_VERSION)" & vbCrLf
sConsulta = sConsulta & "      end" & vbCrLf
sConsulta = sConsulta & "      ELSE" & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "      IF @TIPO_CAMPO_GS IN (5,6,7) -- FecNecesidad, IniSuministro, FinSuministro" & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "   SET @TITULO_FEC = (SELECT CC.VALOR_FEC FROM COPIA_CAMPO CC WITH (NOLOCK) WHERE COPIA_CAMPO_DEF = @TITULO_ID AND INSTANCIA = @ID AND NUM_VERSION = @NUM_VERSION)" & vbCrLf
sConsulta = sConsulta & "      end" & vbCrLf
sConsulta = sConsulta & "      ELSE" & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "      IF @TIPO_CAMPO_GS = 103 -- Tipo Material" & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "   IF @IDIOMA = 'SPA'" & vbCrLf
sConsulta = sConsulta & "   begin" & vbCrLf
sConsulta = sConsulta & "   SET @TITULO_TEXT = (SELECT CASE WHEN CC.VALOR_TEXT IS NOT NULL THEN  REPLACE(SUBSTRING(CC.VALOR_TEXT,(SELECT SUM(LONGITUD) FROM DIC WITH (NOLOCK)  WHERE NOMBRE= 'GMN1' OR NOMBRE= 'GMN2' OR NOMBRE= 'GMN3')+1,(SELECT LONGITUD FROM DIC WITH (NOLOCK)  WHERE NOMBRE= 'GMN4')),' ' ,'') + ' - ' +  (SELECT DEN_SPA FROM GMN4 WITH (NOLOCK)  WHERE GMN1=  REPLACE   (SUBSTRING(CC.VALOR_TEXT,1,(SELECT LONGITUD FROM DIC WITH (NOLOCK)  WHERE NOMBRE= 'GMN1')) ,' ' ,'') AND GMN2=REPLACE(SUBSTRING(CC.VALOR_TEXT,(SELECT SUM(LONGITUD) FROM DIC WITH (NOLOCK)  WHERE NOMBRE= 'GMN1')+1,(SELECT LONGITUD FROM DIC WITH (NOLOCK)  WHERE NOMBRE= 'GMN2')),' ' ,'') AND GMN3=REPLACE(SUBSTRING(CC.VALOR_TEXT,(SELECT SUM(LONGITUD) FROM DIC WITH (NOLOCK)  WHERE NOMBRE= 'GMN1' OR NOMBRE= 'GMN2')+1,(SELECT LONGITUD FROM DIC WITH (NOLOCK)  WHERE NOMBRE= 'GMN3')),' ','') AND COD=REPLACE(SUBSTRING(CC.VALOR_TEXT,(SELECT SUM(LONGITUD) FROM DIC WITH (NOLOCK)  WHERE NOMBRE= 'GMN1' OR NOMBRE= 'GMN2' OR NOMBRE= 'GMN3')+1,"
sConsulta = sConsulta & "   (SELECT LONGITUD FROM DIC WITH (NOLOCK)  WHERE NOMBRE= 'GMN4')),' ' ,'') ) ELSE CC.VALOR_TEXT END FROM COPIA_CAMPO CC WITH (NOLOCK) WHERE COPIA_CAMPO_DEF = @TITULO_ID AND INSTANCIA = @ID AND NUM_VERSION = @NUM_VERSION)" & vbCrLf
sConsulta = sConsulta & "   end" & vbCrLf
sConsulta = sConsulta & "   IF @IDIOMA = 'ENG'" & vbCrLf
sConsulta = sConsulta & "   begin" & vbCrLf
sConsulta = sConsulta & "   SET @TITULO_TEXT = (SELECT CASE WHEN CC.VALOR_TEXT IS NOT NULL THEN  REPLACE(SUBSTRING(CC.VALOR_TEXT,(SELECT SUM(LONGITUD) FROM DIC WITH (NOLOCK)  WHERE NOMBRE= 'GMN1' OR NOMBRE= 'GMN2' OR NOMBRE= 'GMN3')+1,(SELECT LONGITUD FROM DIC WITH (NOLOCK)  WHERE NOMBRE= 'GMN4')),' ' ,'') + ' - ' +  (SELECT DEN_ENG FROM GMN4 WITH (NOLOCK)  WHERE GMN1=  REPLACE   (SUBSTRING(CC.VALOR_TEXT,1,(SELECT LONGITUD FROM DIC WITH (NOLOCK)  WHERE NOMBRE= 'GMN1')) ,' ' ,'') AND GMN2=REPLACE(SUBSTRING(CC.VALOR_TEXT,(SELECT SUM(LONGITUD) FROM DIC WITH (NOLOCK)  WHERE NOMBRE= 'GMN1')+1,(SELECT LONGITUD FROM DIC WITH (NOLOCK)  WHERE NOMBRE= 'GMN2')),' ' ,'') AND GMN3=REPLACE(SUBSTRING(CC.VALOR_TEXT,(SELECT SUM(LONGITUD) FROM DIC WITH (NOLOCK)  WHERE NOMBRE= 'GMN1' OR NOMBRE= 'GMN2')+1,(SELECT LONGITUD FROM DIC WITH (NOLOCK)  WHERE NOMBRE= 'GMN3')),' ','') AND COD=REPLACE(SUBSTRING(CC.VALOR_TEXT,(SELECT SUM(LONGITUD) FROM DIC WITH (NOLOCK)  WHERE NOMBRE= 'GMN1' OR NOMBRE= 'GMN2' OR NOMBRE= 'GMN3')+1,"
sConsulta = sConsulta & "   (SELECT LONGITUD FROM DIC WITH (NOLOCK)  WHERE NOMBRE= 'GMN4')),' ' ,'') ) ELSE CC.VALOR_TEXT END FROM COPIA_CAMPO CC WITH (NOLOCK) WHERE COPIA_CAMPO_DEF = @TITULO_ID AND INSTANCIA = @ID AND NUM_VERSION = @NUM_VERSION)" & vbCrLf
sConsulta = sConsulta & "   end" & vbCrLf
sConsulta = sConsulta & "   IF @IDIOMA = 'GER'" & vbCrLf
sConsulta = sConsulta & "   begin" & vbCrLf
sConsulta = sConsulta & "   SET @TITULO_TEXT = (SELECT CASE WHEN CC.VALOR_TEXT IS NOT NULL THEN  REPLACE(SUBSTRING(CC.VALOR_TEXT,(SELECT SUM(LONGITUD) FROM DIC WITH (NOLOCK)  WHERE NOMBRE= 'GMN1' OR NOMBRE= 'GMN2' OR NOMBRE= 'GMN3')+1,(SELECT LONGITUD FROM DIC WITH (NOLOCK)  WHERE NOMBRE= 'GMN4')),' ' ,'') + ' - ' +  (SELECT DEN_GER FROM GMN4 WITH (NOLOCK)  WHERE GMN1=  REPLACE   (SUBSTRING(CC.VALOR_TEXT,1,(SELECT LONGITUD FROM DIC WITH (NOLOCK)  WHERE NOMBRE= 'GMN1')) ,' ' ,'') AND GMN2=REPLACE(SUBSTRING(CC.VALOR_TEXT,(SELECT SUM(LONGITUD) FROM DIC WITH (NOLOCK)  WHERE NOMBRE= 'GMN1')+1,(SELECT LONGITUD FROM DIC WITH (NOLOCK)  WHERE NOMBRE= 'GMN2')),' ' ,'') AND GMN3=REPLACE(SUBSTRING(CC.VALOR_TEXT,(SELECT SUM(LONGITUD) FROM DIC WITH (NOLOCK)  WHERE NOMBRE= 'GMN1' OR NOMBRE= 'GMN2')+1,(SELECT LONGITUD FROM DIC WITH (NOLOCK)  WHERE NOMBRE= 'GMN3')),' ','') AND COD=REPLACE(SUBSTRING(CC.VALOR_TEXT,(SELECT SUM(LONGITUD) FROM DIC WITH (NOLOCK)  WHERE NOMBRE= 'GMN1' OR NOMBRE= 'GMN2' OR NOMBRE= 'GMN3')+1,"
sConsulta = sConsulta & "   (SELECT LONGITUD FROM DIC WITH (NOLOCK)  WHERE NOMBRE= 'GMN4')),' ' ,'') ) ELSE CC.VALOR_TEXT END FROM COPIA_CAMPO CC WITH (NOLOCK) WHERE COPIA_CAMPO_DEF = @TITULO_ID AND INSTANCIA = @ID AND NUM_VERSION = @NUM_VERSION)" & vbCrLf
sConsulta = sConsulta & "   end" & vbCrLf
sConsulta = sConsulta & "      end" & vbCrLf
sConsulta = sConsulta & "      ELSE  -- En Cualquier otro caso" & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "   SET @TITULO_TEXT = @VALOR_TEXT" & vbCrLf
sConsulta = sConsulta & "      end end end end end end end end end end end end" & vbCrLf
sConsulta = sConsulta & "   end" & vbCrLf
sConsulta = sConsulta & "   ELSE  -- TIPO_CAMPO_GS es NULL" & vbCrLf
sConsulta = sConsulta & "   begin" & vbCrLf
sConsulta = sConsulta & "      IF @SUBTIPO = 3 " & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "   SET @TITULO_FEC = (SELECT CC.VALOR_FEC FROM COPIA_CAMPO CC WITH (NOLOCK) WHERE COPIA_CAMPO_DEF = @TITULO_ID AND INSTANCIA = @ID AND NUM_VERSION = @NUM_VERSION)     " & vbCrLf
sConsulta = sConsulta & "      end" & vbCrLf
sConsulta = sConsulta & "      ELSE" & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "   SET @TITULO_TEXT = @VALOR_TEXT           " & vbCrLf
sConsulta = sConsulta & "      end" & vbCrLf
sConsulta = sConsulta & "   end" & vbCrLf
sConsulta = sConsulta & "end" & vbCrLf
sConsulta = sConsulta & "ELSE  -- Sin Campo T�tulo   " & vbCrLf
sConsulta = sConsulta & "begin" & vbCrLf
sConsulta = sConsulta & "   SET @TITULO_TEXT = (SELECT TOP 1 VALOR_TEXT FROM COPIA_CAMPO CC WITH (NOLOCK) INNER JOIN COPIA_CAMPO_DEF CCD WITH (NOLOCK) ON CC.COPIA_CAMPO_DEF = CCD.ID WHERE TIPO_CAMPO_GS = 1 And INSTANCIA = @ID AND NUM_VERSION = @NUM_VERSION)" & vbCrLf
sConsulta = sConsulta & "   IF @TITULO_TEXT IS NULL" & vbCrLf
sConsulta = sConsulta & "   begin" & vbCrLf
sConsulta = sConsulta & "     SET @TITULO_TEXT = (SELECT DEN_SPA FROM SOLICITUD S WITH (NOLOCK)  INNER JOIN INSTANCIA I WITH (NOLOCK)  ON S.ID = I.SOLICITUD WHERE I.ID = @ID)" & vbCrLf
sConsulta = sConsulta & "   end" & vbCrLf
sConsulta = sConsulta & "end" & vbCrLf
sConsulta = sConsulta & "IF @OLD_VERSION = 0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   --Inserci�n en la tabla" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO INSTANCIA_VISORES_PM (INSTANCIA, TITULO_TEXT, TITULO_FEC, FEC_NECESIDAD) VALUES (@ID, @TITULO_TEXT, @TITULO_FEC, @FEC_NECESIDAD)" & vbCrLf
sConsulta = sConsulta & "      IF @@ERROR!=0 GOTO  ERROR_" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   --Actualizaci�n del registro" & vbCrLf
sConsulta = sConsulta & "   UPDATE INSTANCIA_VISORES_PM SET TITULO_TEXT = @TITULO_TEXT, TITULO_FEC = @TITULO_FEC, FEC_NECESIDAD = @FEC_NECESIDAD WHERE INSTANCIA = @ID" & vbCrLf
sConsulta = sConsulta & "      IF @@ERROR!=0 GOTO  ERROR_" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC FSWS_ACTDENCAMPOS @ID, @NUM_VERSION" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- Actualiza/Crea el registro en las tablas de filtros (1: realiza una INSERT / 0: realiza una UPDATE)" & vbCrLf
sConsulta = sConsulta & "IF @OLD_VERSION = 0" & vbCrLf
sConsulta = sConsulta & "   EXEC FSPM_SAVE_INSTANCIA_EN_FILTRO @ID,1" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   EXEC FSPM_SAVE_INSTANCIA_EN_FILTRO @ID,0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ERROR_:" & vbCrLf
sConsulta = sConsulta & "   SET @ERRORVAR = @@ERROR" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSWS_getRequests]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSWS_getRequests]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSWS_getRequests @IDI AS VARCHAR(20), @USU AS VARCHAR(50), @COMBO TINYINT=0,@TIPO INT=NULL,@DESDESEGUIMIENTO INT=NULL,@NUEVO_WORKFL TINYINT=0,@VIEW TINYINT=0  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SPET AS VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @NUEVO_WORKFL=1" & vbCrLf
sConsulta = sConsulta & "  SET @SPET='VIEW_ROL_PETICIONARIO'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  SET @SPET='VIEW_PETICIONARIOS'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @COMBO=1 " & vbCrLf
sConsulta = sConsulta & "begin" & vbCrLf
sConsulta = sConsulta & "   IF @DESDESEGUIMIENTO=1" & vbCrLf
sConsulta = sConsulta & "           begin" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = N'SELECT NULL AS ID, NULL AS COD, NULL AS DEN UNION SELECT DISTINCT S.ID, S.COD, S.DEN_' + @IDI + ' DEN " & vbCrLf
sConsulta = sConsulta & "       FROM SOLICITUD S  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                           INNER JOIN (' + @SPET + ' P  WITH (NOLOCK) INNER JOIN USU U  WITH (NOLOCK) ON U.PER = P.COD AND U.FSWS_SUSTITUCION IS NULL) ON S.ID = P.SOLICITUD" & vbCrLf
sConsulta = sConsulta & "                           INNER JOIN TIPO_SOLICITUDES TS  WITH (NOLOCK) ON S.TIPO=TS.ID WHERE S.PUB = 1 AND S.BAJA = 0 AND U.COD = @USU '        " & vbCrLf
sConsulta = sConsulta & "       IF @TIPO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "               SET @SQL= @SQL + ' AND TS.TIPO=@TIPO'" & vbCrLf
sConsulta = sConsulta & "                   SET @SQL= @SQL + ' UNION SELECT DISTINCT S.ID, S.COD, S.DEN_' + @IDI + ' DEN FROM SOLICITUD S  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN INSTANCIA I  WITH (NOLOCK) ON S.ID =I.SOLICITUD INNER JOIN TIPO_SOLICITUDES TS  WITH (NOLOCK) ON S.TIPO=TS.ID" & vbCrLf
sConsulta = sConsulta & "       WHERE I.PETICIONARIO=@USU'" & vbCrLf
sConsulta = sConsulta & "                   IF @TIPO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "               SET @SQL= @SQL + ' AND TS.TIPO=@TIPO'" & vbCrLf
sConsulta = sConsulta & "                   SET @SQL= @SQL + '  GROUP BY  S.ID, S.COD, S.DEN_' + @IDI" & vbCrLf
sConsulta = sConsulta & "           end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "       begin" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = N'SELECT NULL AS ID, NULL AS COD, NULL AS DEN " & vbCrLf
sConsulta = sConsulta & "       UNION SELECT DISTINCT S.ID, S.COD, S.DEN_' + @IDI + ' DEN " & vbCrLf
sConsulta = sConsulta & "           FROM SOLICITUD S  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                               INNER JOIN TIPO_SOLICITUDES TS WITH (NOLOCK) ON S.TIPO=TS.ID '" & vbCrLf
sConsulta = sConsulta & "       IF @VIEW =1" & vbCrLf
sConsulta = sConsulta & "           SET @SQL= @SQL + ' INNER JOIN INSTANCIA I WITH (NOLOCK) ON S.ID=I.SOLICITUD AND PETICIONARIO=@USU'" & vbCrLf
sConsulta = sConsulta & "       ELSE    " & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           IF @VIEW =2" & vbCrLf
sConsulta = sConsulta & "           BEGIN" & vbCrLf
sConsulta = sConsulta & "               SET @SQL= @SQL + ' INNER JOIN INSTANCIA I WITH (NOLOCK) ON S.ID=I.SOLICITUD'" & vbCrLf
sConsulta = sConsulta & "               SET @SQL= @SQL + ' INNER JOIN PM_COPIA_ROL R WITH (NOLOCK) ON R.INSTANCIA=I.ID'" & vbCrLf
sConsulta = sConsulta & "               SET @SQL= @SQL + ' AND (R.PER=@USU OR R.PER_ORIGINAL=@USU)' " & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "       SET @SQL= @SQL + ' WHERE S.PUB = 1 AND S.BAJA = 0  '        " & vbCrLf
sConsulta = sConsulta & "               IF @TIPO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "               SET @SQL= @SQL + ' AND TS.TIPO=@TIPO'" & vbCrLf
sConsulta = sConsulta & "               SET @SQL= @SQL + '  GROUP BY  S.ID, S.COD, S.DEN_' + @IDI" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       IF @VIEW =2" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           --Observadores" & vbCrLf
sConsulta = sConsulta & "           --persona o sustituto" & vbCrLf
sConsulta = sConsulta & "           SET @SQL= @SQL + ' UNION SELECT DISTINCT S.ID, S.COD, S.DEN_' + @IDI + ' DEN " & vbCrLf
sConsulta = sConsulta & "           FROM SOLICITUD S  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                              INNER JOIN TIPO_SOLICITUDES TS  WITH (NOLOCK) ON S.TIPO=TS.ID " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PM_ROL R WITH (NOLOCK) ON R.WORKFLOW=S.WORKFLOW AND R.TIPO=5 AND (PER=@USU OR SUSTITUTO=@USU)'" & vbCrLf
sConsulta = sConsulta & "           --uon1" & vbCrLf
sConsulta = sConsulta & "           SET @SQL= @SQL + ' UNION SELECT DISTINCT S.ID, S.COD, S.DEN_' + @IDI + ' DEN " & vbCrLf
sConsulta = sConsulta & "           FROM SOLICITUD S  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                              INNER JOIN TIPO_SOLICITUDES TS  WITH (NOLOCK) ON S.TIPO=TS.ID " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PM_ROL R WITH (NOLOCK) ON R.WORKFLOW=S.WORKFLOW AND R.TIPO=5 AND R.PER IS NULL AND R.UON2 IS NULL AND R.UON3 IS NULL AND R.DEP IS NULL" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PER P WITH (NOLOCK) ON P.COD=@USU AND P.UON1=R.UON1'" & vbCrLf
sConsulta = sConsulta & "           --uon2" & vbCrLf
sConsulta = sConsulta & "           SET @SQL= @SQL + ' UNION SELECT DISTINCT S.ID, S.COD, S.DEN_' + @IDI + ' DEN " & vbCrLf
sConsulta = sConsulta & "           FROM SOLICITUD S  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                              INNER JOIN TIPO_SOLICITUDES TS  WITH (NOLOCK) ON S.TIPO=TS.ID " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PM_ROL R WITH (NOLOCK) ON R.WORKFLOW=S.WORKFLOW AND R.TIPO=5 AND R.PER IS NULL AND R.UON3 IS NULL AND R.DEP IS NULL" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PER P WITH (NOLOCK) ON P.COD=@USU AND P.UON1=R.UON1 AND P.UON2=R.UON2  '" & vbCrLf
sConsulta = sConsulta & "           --uon3" & vbCrLf
sConsulta = sConsulta & "           SET @SQL= @SQL + ' UNION SELECT DISTINCT S.ID, S.COD, S.DEN_' + @IDI + ' DEN " & vbCrLf
sConsulta = sConsulta & "           FROM SOLICITUD S  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                              INNER JOIN TIPO_SOLICITUDES TS WITH (NOLOCK)  ON S.TIPO=TS.ID " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PM_ROL R WITH (NOLOCK) ON R.WORKFLOW=S.WORKFLOW AND R.TIPO=5 AND R.PER IS NULL AND R.DEP IS NULL" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PER P WITH (NOLOCK) ON P.COD=@USU AND P.UON1=R.UON1 AND P.UON2=R.UON2 AND P.UON3=R.UON3 '" & vbCrLf
sConsulta = sConsulta & "           --uon1 dep" & vbCrLf
sConsulta = sConsulta & "           SET @SQL= @SQL + ' UNION SELECT DISTINCT S.ID, S.COD, S.DEN_' + @IDI + ' DEN " & vbCrLf
sConsulta = sConsulta & "           FROM SOLICITUD S  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                              INNER JOIN TIPO_SOLICITUDES TS WITH (NOLOCK)  ON S.TIPO=TS.ID " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PM_ROL R WITH (NOLOCK) ON R.WORKFLOW=S.WORKFLOW AND R.TIPO=5 AND R.PER IS NULL AND R.UON2 IS NULL AND R.UON3 IS NULL" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PER P WITH (NOLOCK) ON P.COD=@USU AND P.UON1=R.UON1 AND P.DEP=R.DEP '" & vbCrLf
sConsulta = sConsulta & "           --uon2 dep" & vbCrLf
sConsulta = sConsulta & "           SET @SQL= @SQL + ' UNION SELECT DISTINCT S.ID, S.COD, S.DEN_' + @IDI + ' DEN " & vbCrLf
sConsulta = sConsulta & "           FROM SOLICITUD S  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                              INNER JOIN TIPO_SOLICITUDES TS WITH (NOLOCK)  ON S.TIPO=TS.ID " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PM_ROL R WITH (NOLOCK) ON R.WORKFLOW=S.WORKFLOW AND R.TIPO=5 AND R.PER IS NULL AND R.UON3 IS NULL" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PER P WITH (NOLOCK) ON P.COD=@USU AND P.UON1=R.UON1 AND P.UON2=R.UON2 AND P.DEP=R.DEP  '" & vbCrLf
sConsulta = sConsulta & "           --uon3 dep" & vbCrLf
sConsulta = sConsulta & "           SET @SQL= @SQL + ' UNION SELECT DISTINCT S.ID, S.COD, S.DEN_' + @IDI + ' DEN " & vbCrLf
sConsulta = sConsulta & "           FROM SOLICITUD S  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                              INNER JOIN TIPO_SOLICITUDES TS  WITH (NOLOCK) ON S.TIPO=TS.ID " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PM_ROL R WITH (NOLOCK) ON R.WORKFLOW=S.WORKFLOW AND R.TIPO=5 AND R.PER IS NULL" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PER P WITH (NOLOCK) ON P.COD=@USU AND P.UON1=R.UON1 AND P.UON2=R.UON2 AND P.UON3=R.UON3 AND P.DEP=R.DEP  '" & vbCrLf
sConsulta = sConsulta & "           --sin uon dep" & vbCrLf
sConsulta = sConsulta & "           SET @SQL= @SQL + ' UNION SELECT DISTINCT S.ID, S.COD, S.DEN_' + @IDI + ' DEN " & vbCrLf
sConsulta = sConsulta & "           FROM SOLICITUD S  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                              INNER JOIN TIPO_SOLICITUDES TS WITH (NOLOCK)  ON S.TIPO=TS.ID " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PM_ROL R WITH (NOLOCK) ON R.WORKFLOW=S.WORKFLOW AND R.TIPO=5 AND R.PER IS NULL AND R.UON1 IS NULL AND R.UON2 IS NULL AND R.UON3 IS NULL" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PER P WITH (NOLOCK) ON P.COD=@USU AND P.DEP=R.DEP '" & vbCrLf
sConsulta = sConsulta & "           --lista" & vbCrLf
sConsulta = sConsulta & "           SET @SQL= @SQL + ' UNION SELECT DISTINCT S.ID, S.COD, S.DEN_' + @IDI + ' DEN " & vbCrLf
sConsulta = sConsulta & "           FROM SOLICITUD S  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                              INNER JOIN TIPO_SOLICITUDES TS  WITH (NOLOCK) ON S.TIPO=TS.ID " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PM_ROL R WITH (NOLOCK) ON R.WORKFLOW=S.WORKFLOW AND R.TIPO=5 AND R.PER IS NULL AND R.UON1 IS NULL AND R.UON2 IS NULL AND R.UON3 IS NULL" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PM_PARTICIPANTES PA WITH (NOLOCK) ON PA.ROL=R.ID" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PER P WITH (NOLOCK) ON P.COD=@USU AND PA.PER=P.COD  '" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "       end" & vbCrLf
sConsulta = sConsulta & "end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "begin" & vbCrLf
sConsulta = sConsulta & "  SET @SQL = N'SELECT DISTINCT S.ID, S.COD, S.DEN_' + @IDI + ' DEN" & vbCrLf
sConsulta = sConsulta & "  , CASE WHEN S.DESCR_' + @IDI + ' IS NOT  NULL THEN CASE WHEN LTRIM(S.DESCR_' + @IDI + ')='''' THEN ISNULL(MAX(SA.ID),0) ELSE 1 END ELSE ISNULL(MAX(SA.ID),0) END  ADJUN" & vbCrLf
sConsulta = sConsulta & "  ,ISNULL(MAX(FAV.favoritos),0) FAVORITOS" & vbCrLf
sConsulta = sConsulta & "  FROM SOLICITUD S WITH (NOLOCK)  INNER JOIN FORM_GRUPO FG WITH (NOLOCK)  ON S.FORMULARIO = FG.FORMULARIO INNER JOIN FORM_CAMPO FC WITH (NOLOCK) ON FG.ID = FC.GRUPO" & vbCrLf
sConsulta = sConsulta & "  LEFT JOIN SOLICITUD_ADJUN SA WITH (NOLOCK) ON S.ID = SA.SOLICITUD " & vbCrLf
sConsulta = sConsulta & "  INNER JOIN (' + @SPET + ' P WITH (NOLOCK) INNER JOIN USU U WITH (NOLOCK) ON U.PER = P.COD AND (U.FSWS_SUSTITUCION IS NULL OR U.FSWS_SUSTITUCION=(SELECT PER FROM USU WITH(NOLOCK) WHERE COD=@USU)) ) ON S.ID = P.SOLICITUD" & vbCrLf
sConsulta = sConsulta & "  INNER JOIN TIPO_SOLICITUDES TS WITH (NOLOCK) ON S.TIPO=TS.ID " & vbCrLf
sConsulta = sConsulta & "  LEFT JOIN (SELECT COUNT(*) FAVORITOS ,SOLICITUD FROM PM_SOLICITUDES_FAV FAV WITH (NOLOCK) WHERE FAV.USU = @USU GROUP BY SOLICITUD ) FAV ON FAV.SOLICITUD = S.ID" & vbCrLf
sConsulta = sConsulta & "  WHERE S.PUB = 1 AND S.BAJA = 0 AND (U.COD = @USU OR U.FSWS_SUSTITUCION=(SELECT PER FROM USU WITH(NOLOCK) WHERE COD=@USU)) ' " & vbCrLf
sConsulta = sConsulta & "  IF @TIPO IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "   SET @SQL= @SQL + '  AND TS.TIPO=@TIPO' " & vbCrLf
sConsulta = sConsulta & "  ELSE" & vbCrLf
sConsulta = sConsulta & "             SET @SQL= @SQL + '  AND TS.TIPO<>2 AND TS.TIPO<>3' " & vbCrLf
sConsulta = sConsulta & "  SET @SQL= @SQL + '  GROUP BY  S.ID, S.COD, S.DEN_' + @IDI + ', S.DESCR_' + @IDI " & vbCrLf
sConsulta = sConsulta & "end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL= @SQL + '  ORDER BY S.COD'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@USU VARCHAR(50),@TIPO INT', @USU = @USU,@TIPO=@TIPO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "PRINT @SQL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub




Public Function CodigoDeActualizacion31800_07_0848A_31800_07_0849() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim iRespuesta As Integer


On Error GoTo Error
sConsulta = "BEGIN TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = True
ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

frmProgreso.lblDetalle = "Actualiza Storeds"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
frmProgreso.ProgressBar1.Refresh
V_31800_7_Storeds_049

sConsulta = "UPDATE VERSION SET NUM ='3.00.08.49'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103), PRODVER='31800.7'"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "COMMIT TRANSACTION"
ExecuteSQL gRDOCon, sConsulta

bTransaccionEnCurso = False

CodigoDeActualizacion31800_07_0848A_31800_07_0849 = True
Exit Function

Error:
    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If

    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    CodigoDeActualizacion31800_07_0848A_31800_07_0849 = False
End Function


Private Sub V_31800_7_Storeds_049()

Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_CARGAR_CESTA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_CARGAR_CESTA]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSEP_CARGAR_CESTA @PER VARCHAR(50), @IDIOMA VARCHAR(3) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF" & vbCrLf
sConsulta = sConsulta & "declare @SQL nvarchar(4000)" & vbCrLf
sConsulta = sConsulta & "declare @PARAM nvarchar(500)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @DEST VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @DESTDEF VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @USU VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "SELECT @DEST = DEST, @USU=COD" & vbCrLf
sConsulta = sConsulta & "  FROM USU " & vbCrLf
sConsulta = sConsulta & " WHERE USU.PER = @PER" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @DESTDEF = DESTDEF" & vbCrLf
sConsulta = sConsulta & "  FROM PARGEN_DEF" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE #TEMP_FSEP_CESTA" & vbCrLf
sConsulta = sConsulta & "(" & vbCrLf
sConsulta = sConsulta & "   ID INT NOT NULL, LINEA INT NULL, PROVE VARCHAR(50) COLLATE DATABASE_DEFAULT NULL," & vbCrLf
sConsulta = sConsulta & "   PROVEDEN VARCHAR(100) COLLATE DATABASE_DEFAULT NULL, COD_ITEM VARCHAR(50) COLLATE DATABASE_DEFAULT NULL," & vbCrLf
sConsulta = sConsulta & "   ART_DEN VARCHAR(400) COLLATE DATABASE_DEFAULT NULL, CANT FLOAT NULL, UP VARCHAR(6) COLLATE DATABASE_DEFAULT NULL," & vbCrLf
sConsulta = sConsulta & "   PREC FLOAT NULL, FC FLOAT NULL, MON VARCHAR(50) COLLATE DATABASE_DEFAULT NULL, CANT_MIN_DEF FLOAT NULL," & vbCrLf
sConsulta = sConsulta & "   TIPO TINYINT NOT NULL, GMN1 VARCHAR(50) COLLATE DATABASE_DEFAULT NULL, DEST VARCHAR(50) COLLATE DATABASE_DEFAULT NULL," & vbCrLf
sConsulta = sConsulta & "   CAT1 INT NULL, CAT2 INT NULL, CAT3 INT NULL, CAT4 INT NULL, CAT5 INT NULL," & vbCrLf
sConsulta = sConsulta & "   CAMPO1 INT NULL, CAMPO2 INT NULL, MODIF_PREC TINYINT NULL, MODIF_UNI TINYINT NULL," & vbCrLf
sConsulta = sConsulta & "   PRES5 NVARCHAR(50) COLLATE DATABASE_DEFAULT NULL, PRES5_NIV1 NVARCHAR(50) COLLATE DATABASE_DEFAULT NULL," & vbCrLf
sConsulta = sConsulta & "   PRES5_NIV2 NVARCHAR(50) COLLATE DATABASE_DEFAULT NULL, PRES5_NIV3 NVARCHAR(50) COLLATE DATABASE_DEFAULT NULL," & vbCrLf
sConsulta = sConsulta & "   PRES5_NIV4 NVARCHAR(50) COLLATE DATABASE_DEFAULT NULL, EQUIV FLOAT NULL," & vbCrLf
sConsulta = sConsulta & "   ENTREGA_OBL TINYINT NOT NULL, FECENTREGA DATETIME NULL, OBS NVARCHAR(2000) NULL, " & vbCrLf
sConsulta = sConsulta & "   VALOR1 NVARCHAR(100) NULL, VALOR2 NVARCHAR(100) NULL, " & vbCrLf
sConsulta = sConsulta & "   UON1 VARCHAR(20) NULL, UON2 VARCHAR(20) NULL, UON3 VARCHAR(20) NULL, UON4 VARCHAR(20) NULL, DEN_UON NVARCHAR(100) NULL" & vbCrLf
sConsulta = sConsulta & ")" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO #TEMP_FSEP_CESTA" & vbCrLf
sConsulta = sConsulta & "   SELECT C.ID, C.LINEA, CL.PROVE, P.DEN PROVEDEN,  CL.ART_INT  COD_ITEM,I.DESCR + CL.DEN ART_DEN, " & vbCrLf
sConsulta = sConsulta & "   C.CANT, ISNULL(C.UP,CL.UP_DEF) UP, ISNULL(C.PREC,CL.PRECIO_UC) PREC,  ISNULL(C.FC,CL.FC_DEF) FC, CL.MON, ISNULL(CLM.CANT_MIN,CL.CANT_MIN_DEF) CANT_MIN_DEF, C.TIPO, " & vbCrLf
sConsulta = sConsulta & "   CL.GMN1, " & vbCrLf
sConsulta = sConsulta & "   ISNULL(C.DEST, CASE DEST_USU WHEN 1 THEN ISNULL(@DEST,CL.DEST) ELSE CL.DEST END) DEST," & vbCrLf
sConsulta = sConsulta & "   CL.CAT1, CL.CAT2, CL.CAT3, CL.CAT4, CL.CAT5," & vbCrLf
sConsulta = sConsulta & "   case when c5.cod is null then case when c4.cod is null then case when c3.cod is null then case when c2.cod is null then c1.campo1 else c2.campo1 end else c3.campo1 end else c4.campo1 end else c5.campo1 end CAMPO1," & vbCrLf
sConsulta = sConsulta & "   case when c5.cod is null then case when c4.cod is null then case when c3.cod is null then case when c2.cod is null then c1.campo2 else c2.campo2 end else c3.campo2 end else c4.campo2 end else c5.campo2 end CAMPO2," & vbCrLf
sConsulta = sConsulta & "   case when c5.cod is null then case when c4.cod is null then case when c3.cod is null then case when c2.cod is null then c1.modifprec else c2.modifprec end else c3.modifprec end else c4.modifprec end else c5.modifprec end MODIF_PREC," & vbCrLf
sConsulta = sConsulta & "   0 AS MODIF_UNI," & vbCrLf
sConsulta = sConsulta & "   case when c5.cod is null then case when c4.cod is null then case when c3.cod is null then case when c2.cod is null then c1.PRES0 else c2.PRES0 end else c3.PRES0 end else c4.PRES0 end else c5.PRES0 end PRES5," & vbCrLf
sConsulta = sConsulta & "   case when c5.cod is null then case when c4.cod is null then case when c3.cod is null then case when c2.cod is null then c1.PRES1 else c2.PRES1 end else c3.PRES1 end else c4.PRES1 end else c5.PRES1 end PRES5_NIV1," & vbCrLf
sConsulta = sConsulta & "   case when c5.cod is null then case when c4.cod is null then case when c3.cod is null then case when c2.cod is null then c1.PRES2 else c2.PRES2 end else c3.PRES2 end else c4.PRES2 end else c5.PRES2 end PRES5_NIV2," & vbCrLf
sConsulta = sConsulta & "   case when c5.cod is null then case when c4.cod is null then case when c3.cod is null then case when c2.cod is null then c1.PRES3 else c2.PRES3 end else c3.PRES3 end else c4.PRES3 end else c5.PRES3 end PRES5_NIV3," & vbCrLf
sConsulta = sConsulta & "   case when c5.cod is null then case when c4.cod is null then case when c3.cod is null then case when c2.cod is null then c1.PRES4 else c2.PRES4 end else c3.PRES4 end else c4.PRES4 end else c5.PRES4 end PRES5_NIV4," & vbCrLf
sConsulta = sConsulta & "   MON.EQUIV, ISNULL(C.ENTREGA_OBL,0) ENTREGA_OBL, C.FECENTREGA, C.OBS, C.VALOR1, C.VALOR2, C.UON1, C.UON2, C.UON3, C.UON4, " & vbCrLf
sConsulta = sConsulta & "   COALESCE(UON1.DEN, UON2.DEN, UON3.DEN, UON4.DEN)" & vbCrLf
sConsulta = sConsulta & "   FROM CESTA C WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN CATALOG_LIN CL WITH (NOLOCK) ON C.LINEA = CL.ID" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PROVE P WITH (NOLOCK) ON Cl.PROVE=P.COD" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN ITEM I WITH (NOLOCK) ON CL.ANYO=I.ANYO AND CL.GMN1=I.GMN1_PROCE  AND CL.PROCE=I.PROCE AND CL.ITEM=I.ID" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN CATN1 C1 WITH (NOLOCK) ON CL.CAT1 = C1.ID LEFT JOIN CATN2 C2 ON CL.CAT2 = C2.ID LEFT JOIN CATN3 C3 ON CL.CAT3 = C3.ID LEFT JOIN CATN4 C4 ON CL.CAT4 = C4.ID" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN CATN5 C5 WITH (NOLOCK) ON CL.CAT5 = C5.ID" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN CAT_LIN_MIN CLM WITH (NOLOCK) ON C.LINEA = CLM.LINEA AND C.UP = CLM.UP" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN MON WITH (NOLOCK) ON MON.COD=CL.MON" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN UON1 WITH (NOLOCK) ON UON1.COD=C.UON1 AND C.UON2 IS NULL AND C.UON3 IS NULL AND C.UON4 IS NULL" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN UON2 WITH (NOLOCK) ON UON2.UON1=C.UON1 AND UON2.COD=C.UON2 AND C.UON3 IS NULL AND C.UON4 IS NULL" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN UON3 WITH (NOLOCK) ON UON3.UON1=C.UON1 AND UON3.UON2=C.UON2 AND UON3.COD=C.UON3 AND C.UON4 IS NULL" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN UON4 WITH (NOLOCK) ON UON4.UON1=C.UON1 AND UON4.UON2=C.UON2 AND UON4.UON3=C.UON3 AND UON4.COD=C.UON4" & vbCrLf
sConsulta = sConsulta & "   WHERE PER=@PER AND TIPO = 0" & vbCrLf
sConsulta = sConsulta & "   union SELECT C.ID, NULL LINEA, C.PROVE, P.DEN PROVEDEN, NULL COD_ITEM, DESCR ART_DEN,C.CANT, C.UP, C.PREC, 1 FC, C.MON, NULL CANT_MIN_DEF, C.TIPO, NULL GMN1, ISNULL(C.DEST, ISNULL(@DEST,@DESTDEF)) DEST," & vbCrLf
sConsulta = sConsulta & "   C.CAT1, C.CAT2, C.CAT3, C.CAT4, C.CAT5," & vbCrLf
sConsulta = sConsulta & "   case when c5.cod is null then case when c4.cod is null then case when c3.cod is null then case when c2.cod is null then c1.campo1 else c2.campo1 end else c3.campo1 end else c4.campo1 end else c5.campo1 end CAMPO1," & vbCrLf
sConsulta = sConsulta & "   case when c5.cod is null then case when c4.cod is null then case when c3.cod is null then case when c2.cod is null then c1.campo2 else c2.campo2 end else c3.campo2 end else c4.campo2 end else c5.campo2 end CAMPO2," & vbCrLf
sConsulta = sConsulta & "   case when c5.cod is null then case when c4.cod is null then case when c3.cod is null then case when c2.cod is null then c1.modifprec else c2.modifprec end else c3.modifprec end else c4.modifprec end else c5.modifprec end MODIF_PREC," & vbCrLf
sConsulta = sConsulta & "   0 MODIF_UNI," & vbCrLf
sConsulta = sConsulta & "   case when c5.cod is null then case when c4.cod is null then case when c3.cod is null then case when c2.cod is null then c1.PRES0 else c2.PRES0 end else c3.PRES0 end else c4.PRES0 end else c5.PRES0 end PRES5," & vbCrLf
sConsulta = sConsulta & "   case when c5.cod is null then case when c4.cod is null then case when c3.cod is null then case when c2.cod is null then c1.PRES1 else c2.PRES1 end else c3.PRES1 end else c4.PRES1 end else c5.PRES1 end PRES5_NIV1," & vbCrLf
sConsulta = sConsulta & "   case when c5.cod is null then case when c4.cod is null then case when c3.cod is null then case when c2.cod is null then c1.PRES2 else c2.PRES2 end else c3.PRES2 end else c4.PRES2 end else c5.PRES2 end PRES5_NIV2," & vbCrLf
sConsulta = sConsulta & "   case when c5.cod is null then case when c4.cod is null then case when c3.cod is null then case when c2.cod is null then c1.PRES3 else c2.PRES3 end else c3.PRES3 end else c4.PRES3 end else c5.PRES3 end PRES5_NIV3," & vbCrLf
sConsulta = sConsulta & "   case when c5.cod is null then case when c4.cod is null then case when c3.cod is null then case when c2.cod is null then c1.PRES4 else c2.PRES4 end else c3.PRES4 end else c4.PRES4 end else c5.PRES4 end PRES5_NIV4," & vbCrLf
sConsulta = sConsulta & "   MON.EQUIV, ISNULL(C.ENTREGA_OBL,0) ENTREGA_OBL, C.FECENTREGA, C.OBS, C.VALOR1, C.VALOR2, C.UON1, C.UON2, C.UON3, C.UON4," & vbCrLf
sConsulta = sConsulta & "   COALESCE(UON1.DEN, UON2.DEN, UON3.DEN, UON4.DEN)" & vbCrLf
sConsulta = sConsulta & "   FROM CESTA C WITH (NOLOCK) LEFT JOIN CATN1 C1 WITH (NOLOCK) ON C.CAT1 = C1.ID LEFT JOIN CATN2 C2 WITH (NOLOCK) ON C.CAT2 = C2.ID LEFT JOIN CATN3 C3 WITH (NOLOCK) ON C.CAT3 = C3.ID LEFT JOIN CATN4 C4 WITH (NOLOCK) ON C.CAT4 = C4.ID LEFT JOIN CATN5 C5 WITH (NOLOCK) ON C.CAT5 = C5.ID" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PROVE P WITH (NOLOCK) ON C.PROVE=P.COD" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN MON WITH (NOLOCK) ON MON.COD=C.MON" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN UON1 WITH (NOLOCK) ON UON1.COD=C.UON1 AND C.UON2 IS NULL AND C.UON3 IS NULL AND C.UON4 IS NULL" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN UON2 WITH (NOLOCK) ON UON2.UON1=C.UON1 AND UON2.COD=C.UON2 AND C.UON3 IS NULL AND C.UON4 IS NULL" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN UON3 WITH (NOLOCK) ON UON3.UON1=C.UON1 AND UON3.UON2=C.UON2 AND UON3.COD=C.UON3 AND C.UON4 IS NULL" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN UON4 WITH (NOLOCK) ON UON4.UON1=C.UON1 AND UON4.UON2=C.UON2 AND UON4.UON3=C.UON3 AND UON4.COD=C.UON4" & vbCrLf
sConsulta = sConsulta & "   WHERE PER=@PER AND TIPO = 1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   UPDATE #TEMP_FSEP_CESTA" & vbCrLf
sConsulta = sConsulta & "       SET MODIF_UNI=1" & vbCrLf
sConsulta = sConsulta & "   FROM #TEMP_FSEP_CESTA" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN CAT_LIN_MIN WITH (NOLOCK) ON CAT_LIN_MIN.LINEA=#TEMP_FSEP_CESTA.LINEA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT OFF" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT T.ID, T.LINEA, T.PROVE, T.PROVEDEN, T.COD_ITEM, T.ART_DEN, T.CANT, T.UP, T.PREC, T.FC, T.MON, T.CANT_MIN_DEF, T.TIPO," & vbCrLf
sConsulta = sConsulta & "   T.GMN1, T.DEST, T.CAT1, T.CAT2, T.CAT3, T.CAT4, T.CAT5, T.CAMPO1, T.CAMPO2, T.MODIF_PREC, T.MODIF_UNI," & vbCrLf
sConsulta = sConsulta & "   T.PRES5, T.PRES5_NIV1, T.PRES5_NIV2, T.PRES5_NIV3, T.PRES5_NIV4," & vbCrLf
sConsulta = sConsulta & "   C1.DEN DEN1, C1.OBL OBL1, C2.DEN DEN2, C2.OBL OBL2," & vbCrLf
sConsulta = sConsulta & "   COUNT(UX.PRES5) AS NUMCC," & vbCrLf
sConsulta = sConsulta & "   ISNULL(T.UON1, CASE COUNT(UX.PRES5) WHEN 1 THEN MAX(UX.UON1) ELSE NULL END) AS UON1, " & vbCrLf
sConsulta = sConsulta & "   ISNULL(T.UON2, CASE COUNT(UX.PRES5) WHEN 1 THEN MAX(UX.UON2) ELSE NULL END) AS UON2, " & vbCrLf
sConsulta = sConsulta & "   ISNULL(T.UON3, CASE COUNT(UX.PRES5) WHEN 1 THEN MAX(UX.UON3) ELSE NULL END) AS UON3, " & vbCrLf
sConsulta = sConsulta & "   ISNULL(T.UON4, CASE COUNT(UX.PRES5) WHEN 1 THEN MAX(UX.UON4) ELSE NULL END) AS UON4, " & vbCrLf
sConsulta = sConsulta & "   ISNULL(T.DEN_UON,MAX(UX.DEN)) AS DEN_UON," & vbCrLf
sConsulta = sConsulta & "   PX.PRES5_DEN, T.EQUIV, T.ENTREGA_OBL, T.FECENTREGA, T.OBS, T.VALOR1, T.VALOR2" & vbCrLf
sConsulta = sConsulta & "FROM #TEMP_FSEP_CESTA t  " & vbCrLf
sConsulta = sConsulta & "LEFT JOIN CATALOG_CAMPOS C1 WITH (NOLOCK) on T.CAMPO1 = C1.ID AND C1.IDIOMA = @IDIOMA" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN CATALOG_CAMPOS C2 WITH (NOLOCK) ON T.CAMPO2 = C2.ID AND C2.IDIOMA = @IDIOMA" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN" & vbCrLf
sConsulta = sConsulta & "   (SELECT U1.COD AS UON1, NULL AS UON2, NULL AS UON3, NULL AS UON4, U1.COD AS DEN, U1.PRES5 FROM UON1 U1 WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN USU_CC_IMPUTACION UI WITH (NOLOCK) ON UI.UON1=U1.COD" & vbCrLf
sConsulta = sConsulta & "       WHERE NOT U1.PRES5 IS NULL AND UI.UON2 IS NULL AND UI.UON3 IS NULL AND UI.UON4 IS NULL AND UI.USU=@USU" & vbCrLf
sConsulta = sConsulta & "   UNION SELECT U2.UON1, U2.COD AS UON2, NULL AS UON3, NULL AS UON4, U2.COD AS DEN, U2.PRES5 FROM UON2 U2 WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN USU_CC_IMPUTACION UI WITH (NOLOCK) ON UI.UON1=U2.UON1 AND UI.UON2=U2.COD" & vbCrLf
sConsulta = sConsulta & "       WHERE NOT U2.PRES5 IS NULL AND UI.UON3 IS NULL AND UI.UON4 IS NULL AND UI.USU=@USU" & vbCrLf
sConsulta = sConsulta & "   UNION SELECT U3.UON1, U3.UON2, U3.COD AS UON3, NULL AS UON4, U3.COD AS DEN, U3.PRES5 FROM UON3 U3 WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN USU_CC_IMPUTACION UI WITH (NOLOCK) ON UI.UON1=U3.UON1 AND UI.UON2=U3.UON2 AND UI.UON3=U3.COD" & vbCrLf
sConsulta = sConsulta & "       WHERE NOT U3.PRES5 IS NULL AND UI.UON4 IS NULL AND UI.USU=@USU" & vbCrLf
sConsulta = sConsulta & "   UNION SELECT U4.UON1, U4.UON2, U4.UON3, U4.COD AS UON4, U4.COD AS DEN, U4.PRES5 FROM UON4 U4 WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN USU_CC_IMPUTACION UI WITH (NOLOCK) ON UI.UON1=U4.UON1 AND UI.UON2=U4.UON2 AND UI.UON3=U4.UON3 AND UI.UON4=U4.COD" & vbCrLf
sConsulta = sConsulta & "       WHERE NOT U4.PRES5 IS NULL AND UI.USU=@USU" & vbCrLf
sConsulta = sConsulta & "   ) UX ON UX.PRES5=T.PRES5" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN" & vbCrLf
sConsulta = sConsulta & "   (SELECT P0.COD AS PRES0, NULL AS PRES1, NULL AS PRES2, NULL AS PRES3, NULL AS PRES4, P0.DEN AS PRES5_DEN" & vbCrLf
sConsulta = sConsulta & "       FROM PRES5_NIV0 P0 WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   UNION SELECT P1.PRES0, P1.COD AS PRES1, NULL AS PRES2, NULL AS PRES3, NULL AS PRES4, P1.DEN AS PRES5_DEN" & vbCrLf
sConsulta = sConsulta & "       FROM PRES5_NIV1 P1 WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       WHERE P1.BAJALOG=0" & vbCrLf
sConsulta = sConsulta & "   UNION SELECT P2.PRES0, P2.PRES1, P2.COD AS PRES2, NULL AS PRES3, NULL AS PRES4, P2.DEN AS PRES5_DEN" & vbCrLf
sConsulta = sConsulta & "       FROM PRES5_NIV2 P2 WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       WHERE P2.BAJALOG=0" & vbCrLf
sConsulta = sConsulta & "   UNION SELECT P3.PRES0, P3.PRES1, P3.PRES2, P3.COD AS PRES3, NULL AS PRES4, P3.DEN AS PRES5_DEN" & vbCrLf
sConsulta = sConsulta & "       FROM PRES5_NIV3 P3 WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       WHERE P3.BAJALOG=0" & vbCrLf
sConsulta = sConsulta & "   UNION SELECT P4.PRES0, P4.PRES1, P4.PRES2, P4.PRES3, P4.COD AS PRES4, P4.DEN AS PRES5_DEN" & vbCrLf
sConsulta = sConsulta & "       FROM PRES5_NIV4 P4 WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       WHERE P4.BAJALOG=0" & vbCrLf
sConsulta = sConsulta & "   ) PX ON PX.PRES0=T.PRES5 AND (PX.PRES1=T.PRES5_NIV1 OR PX.PRES1 IS NULL AND T.PRES5_NIV1 IS NULL) AND (PX.PRES2=T.PRES5_NIV2 OR PX.PRES2 IS NULL AND T.PRES5_NIV2 IS NULL)" & vbCrLf
sConsulta = sConsulta & "           AND (PX.PRES3=T.PRES5_NIV3 OR PX.PRES3 IS NULL AND T.PRES5_NIV3 IS NULL) AND (PX.PRES4=T.PRES5_NIV4 OR PX.PRES4 IS NULL AND T.PRES5_NIV4 IS NULL)" & vbCrLf
sConsulta = sConsulta & "GROUP BY T.ID, T.LINEA, T.PROVE, T.PROVEDEN, T.COD_ITEM, T.ART_DEN, T.CANT, T.UP, T.PREC, T.FC, T.MON, T.CANT_MIN_DEF, T.TIPO," & vbCrLf
sConsulta = sConsulta & "   T.GMN1, T.DEST, T.CAT1, T.CAT2, T.CAT3, T.CAT4, T.CAT5, T.CAMPO1, T.CAMPO2, T.MODIF_PREC, T.MODIF_UNI," & vbCrLf
sConsulta = sConsulta & "   T.PRES5, T.PRES5_NIV1, T.PRES5_NIV2, T.PRES5_NIV3, T.PRES5_NIV4," & vbCrLf
sConsulta = sConsulta & "   C1.DEN, C1.OBL, C2.DEN, C2.OBL, PX.PRES5_DEN, T.EQUIV, T.UON1, T.UON2, T.UON3, T.UON4," & vbCrLf
sConsulta = sConsulta & "   T.ENTREGA_OBL, T.FECENTREGA, T.OBS, T.VALOR1, T.VALOR2, T.DEN_UON" & vbCrLf
sConsulta = sConsulta & "ORDER BY PROVE, MON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DROP TABLE #TEMP_FSEP_CESTA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_CONTROLPRES_CESTA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_CONTROLPRES_CESTA]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSEP_CONTROLPRES_CESTA] @PER VARCHAR(50) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT C.ID, C.UON1, C.UON2, C.UON3, C.UON4, C.PRES0, C.PRES1, C.PRES2, C.PRES3, C.PRES4, PRES5.DEN" & vbCrLf
sConsulta = sConsulta & "   , ISNULL(C.BLOQUEO,0) AS BLOQUEO, ISNULL(C.CONTROL ,0) AS CONTROL, CASE WHEN UON.CC_INICIO>MONTH(GETDATE()) THEN YEAR(GETDATE())-1 ELSE YEAR(GETDATE()) END AS ANYO" & vbCrLf
sConsulta = sConsulta & "   , SUM(ISNULL(I.PRESUPUESTO,0)) AS PRESUPUESTO, SUM(ISNULL(I.COMPROMETIDO,0)) AS COMPROMETIDO, SUM(ISNULL(I.RECEPCIONADO,0)) AS RECEPCIONADO" & vbCrLf
sConsulta = sConsulta & "FROM PRES5_CONTROL C" & vbCrLf
sConsulta = sConsulta & "INNER JOIN (SELECT COD AS UON1, '' AS UON2, '' AS UON3, '' AS UON4, CC_INICIO, PRES5 FROM UON1 WHERE BAJALOG=0 AND CC=1 AND NOT PRES5 IS NULL" & vbCrLf
sConsulta = sConsulta & "           UNION SELECT UON1, COD AS UON2, '' AS UON3, '' AS UON4, CC_INICIO, PRES5 FROM UON2 WHERE BAJALOG=0 AND CC=1 AND NOT PRES5 IS NULL" & vbCrLf
sConsulta = sConsulta & "           UNION SELECT UON1, UON2, COD AS UON3, '' AS UON4, CC_INICIO, PRES5 FROM UON3 WHERE BAJALOG=0 AND CC=1 AND NOT PRES5 IS NULL" & vbCrLf
sConsulta = sConsulta & "           UNION SELECT UON1, UON2, UON3, COD AS UON4, CC_INICIO, PRES5 FROM UON4 WHERE BAJALOG=0 AND CC=1 AND NOT PRES5 IS NULL) UON" & vbCrLf
sConsulta = sConsulta & "   ON UON.UON1=C.UON1 AND UON.UON2=ISNULL(C.UON2,'') AND UON.UON3=ISNULL(C.UON3,'') AND UON.UON4=ISNULL(C.UON4,'') AND UON.PRES5=C.PRES0" & vbCrLf
sConsulta = sConsulta & "INNER JOIN (SELECT PRES0, COD AS PRES1, '' AS PRES2, '' AS PRES3, '' AS PRES4, DEN FROM PRES5_NIV1 WHERE BAJALOG=0" & vbCrLf
sConsulta = sConsulta & "           UNION SELECT PRES0, PRES1, COD AS PRES2, '' AS PRES3, '' AS PRES4, DEN FROM PRES5_NIV2 WHERE BAJALOG=0" & vbCrLf
sConsulta = sConsulta & "           UNION SELECT PRES0, PRES1, PRES2, COD AS PRES3, '' AS PRES4, DEN FROM PRES5_NIV3 WHERE BAJALOG=0" & vbCrLf
sConsulta = sConsulta & "           UNION SELECT PRES0, PRES1, PRES2, PRES3, COD AS PRES4, DEN FROM PRES5_NIV4 WHERE BAJALOG=0) PRES5" & vbCrLf
sConsulta = sConsulta & "   ON PRES5.PRES0=C.PRES0 AND PRES5.PRES1=C.PRES1 AND PRES5.PRES2=ISNULL(C.PRES2,'') AND PRES5.PRES3=ISNULL(C.PRES3,'') AND PRES5.PRES4=ISNULL(C.PRES4,'')" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PRES5_CONTROL_IMPORTES I ON I.PRES5_CONTROL=C.ID" & vbCrLf
sConsulta = sConsulta & "   AND ((C.CONTROL=1 AND I.ANYO=YEAR(GETDATE()) AND I.MES=MONTH(GETDATE()))" & vbCrLf
sConsulta = sConsulta & "       OR (ISNULL(C.CONTROL,0)=0 AND ( (CASE WHEN MONTH(GETDATE())>UON.CC_INICIO THEN YEAR(GETDATE())-1 ELSE YEAR(GETDATE()) END)=(CASE WHEN I.MES>UON.CC_INICIO THEN I.ANYO-1 ELSE I.ANYO END) )))" & vbCrLf
sConsulta = sConsulta & "   AND C.PRES0 IN" & vbCrLf
sConsulta = sConsulta & "       (SELECT CAT.PRES0 FROM CESTA CE" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN CATALOG_LIN CL ON CL.ID=CE.LINEA" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN (SELECT ID AS CAT1, 0 AS CAT2, 0 AS CAT3, 0 AS CAT4, 0 AS CAT5, PRES0 FROM CATN1 WHERE BAJALOG=0 AND NOT PRES0 IS NULL" & vbCrLf
sConsulta = sConsulta & "                       UNION SELECT CAT1, ID AS CAT2, 0 AS CAT3, 0 AS CAT4, 0 AS CAT5, PRES0 FROM CATN2 WHERE BAJALOG=0 AND NOT PRES0 IS NULL" & vbCrLf
sConsulta = sConsulta & "                       UNION SELECT CAT1, CAT2, ID AS CAT3, 0 AS CAT4, 0 AS CAT5, PRES0 FROM CATN3 WHERE BAJALOG=0 AND NOT PRES0 IS NULL" & vbCrLf
sConsulta = sConsulta & "                       UNION SELECT CAT1, CAT2, CAT3, ID AS CAT4, 0 AS CAT5, PRES0 FROM CATN4 WHERE BAJALOG=0 AND NOT PRES0 IS NULL" & vbCrLf
sConsulta = sConsulta & "                       UNION SELECT CAT1, CAT2, CAT3, CAT4, ID AS CAT5, PRES0 FROM CATN5 WHERE BAJALOG=0 AND NOT PRES0 IS NULL) CAT" & vbCrLf
sConsulta = sConsulta & "               ON CAT.CAT1=ISNULL(CE.CAT1, CL.CAT1) AND CAT.CAT2=(CASE WHEN CE.CAT1 IS NULL THEN ISNULL(CL.CAT2,0) ELSE ISNULL(CE.CAT2,0) END)" & vbCrLf
sConsulta = sConsulta & "                   AND CAT.CAT3=(CASE WHEN CE.CAT1 IS NULL THEN ISNULL(CL.CAT3,0) ELSE ISNULL(CE.CAT3,0) END) AND CAT.CAT4=(CASE WHEN CE.CAT1 IS NULL THEN ISNULL(CL.CAT4,0) ELSE ISNULL(CE.CAT4,0) END)" & vbCrLf
sConsulta = sConsulta & "                   AND CAT.CAT5=(CASE WHEN CE.CAT1 IS NULL THEN ISNULL(CL.CAT5,0) ELSE ISNULL(CE.CAT5,0) END)" & vbCrLf
sConsulta = sConsulta & "           WHERE CE.PER=@PER)" & vbCrLf
sConsulta = sConsulta & "GROUP BY C.ID, C.UON1, C.UON2, C.UON3, C.UON4, PRES5.DEN, C.PRES0, C.PRES1, C.PRES2, C.PRES3, C.PRES4, C.BLOQUEO, C.CONTROL, UON.CC_INICIO" & vbCrLf
sConsulta = sConsulta & "ORDER BY C.UON1, C.UON2, C.UON3, C.UON4, C.PRES0, C.PRES1, C.PRES2, C.PRES3, C.PRES4" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub


Public Function CodigoDeActualizacion31800_07_0849A_31800_07_0850() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim iRespuesta As Integer


On Error GoTo Error
sConsulta = "BEGIN TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = True
ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

frmProgreso.lblDetalle = "Actualiza Storeds"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
frmProgreso.ProgressBar1.Refresh
V_31800_7_Storeds_050

sConsulta = "UPDATE VERSION SET NUM ='3.00.08.50'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103), PRODVER='31800.7'"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "COMMIT TRANSACTION"
ExecuteSQL gRDOCon, sConsulta

bTransaccionEnCurso = False

CodigoDeActualizacion31800_07_0849A_31800_07_0850 = True
Exit Function

Error:
    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If

    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    CodigoDeActualizacion31800_07_0849A_31800_07_0850 = False
End Function




Private Sub V_31800_7_Storeds_050()

Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSWS_GETFIELDS_INSTANCIA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSWS_GETFIELDS_INSTANCIA]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSWS_GETFIELDS_INSTANCIA @ID INT, @IDI VARCHAR(20) = NULL, @VERSION INT,@USU VARCHAR(50)=NULL, @PROVE VARCHAR(50) = NULL  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @TRASLADADA AS TINYINT " & vbCrLf
sConsulta = sConsulta & "DECLARE @ESTADO AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @DESTINATARIO AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PETICIONARIO AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @APROB AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @ESCRITURA_CERTIF AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @HAYCONFCUMP TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @CUMP_PROVE TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ESCRITURA_NOCONF AS TINYINT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Obtiene los datos de la instancia" & vbCrLf
sConsulta = sConsulta & "SELECT * FROM INSTANCIA WITH (NOLOCK)  WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @APROB = (select CS.PER FROM COPIA_SUBPASO CS  WITH (NOLOCK)  INNER JOIN INSTANCIA I  WITH (NOLOCK)  ON CS.ID = I.APROB WHERE I.ID= @ID)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Obtiene los campos de la instancia:" & vbCrLf
sConsulta = sConsulta & "SELECT @ESTADO= ESTADO," & vbCrLf
sConsulta = sConsulta & "       @TRASLADADA=TRASLADADA," & vbCrLf
sConsulta = sConsulta & "       @DESTINATARIO=CASE WHEN DESTINATARIO IS NULL THEN PER ELSE DESTINATARIO END , " & vbCrLf
sConsulta = sConsulta & "       @PETICIONARIO = PETICIONARIO," & vbCrLf
sConsulta = sConsulta & "       @CUMP_PROVE = CUMP_PROVE" & vbCrLf
sConsulta = sConsulta & "  FROM INSTANCIA   WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "         LEFT JOIN INSTANCIA_EST   WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                ON INSTANCIA.INSTANCIA_EST=INSTANCIA_EST.ID " & vbCrLf
sConsulta = sConsulta & "               AND DESTINATARIO IS NOT NULL  " & vbCrLf
sConsulta = sConsulta & " WHERE INSTANCIA.ID=@ID" & vbCrLf
sConsulta = sConsulta & "IF @ESTADO=200  --Si  es un certificado comprueba que la instancia y versi�n que se pasan por par�metro sean las actuales del certificado.Si el certificado est� publicado no se podr�n modificar los datos" & vbCrLf
sConsulta = sConsulta & "  begin" & vbCrLf
sConsulta = sConsulta & "    SELECT @ESCRITURA_CERTIF=PUBLICADA " & vbCrLf
sConsulta = sConsulta & "      FROM CERTIFICADO C WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "               INNER JOIN INSTANCIA I  WITH (NOLOCK)  " & vbCrLf
sConsulta = sConsulta & "                       ON C.INSTANCIA=I.ID " & vbCrLf
sConsulta = sConsulta & "               INNER JOIN PROVE_CERTIFICADO PV  WITH (NOLOCK)  " & vbCrLf
sConsulta = sConsulta & "                       ON C.PROVE=PV.PROVE " & vbCrLf
sConsulta = sConsulta & "                      AND I.SOLICITUD=PV.TIPO_CERTIF " & vbCrLf
sConsulta = sConsulta & "                      AND C.ID=PV.ID_CERTIF " & vbCrLf
sConsulta = sConsulta & "    WHERE C.INSTANCIA=@ID AND C.NUM_VERSION=@VERSION AND C.PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "    IF @ESCRITURA_CERTIF IS NULL" & vbCrLf
sConsulta = sConsulta & "      SET @ESCRITURA_CERTIF=1" & vbCrLf
sConsulta = sConsulta & "  end" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & " begin" & vbCrLf
sConsulta = sConsulta & "   IF @ESTADO=300 OR @ESTADO=301" & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "        IF (SELECT ESTADO FROM NOCONFORMIDAD NC  WITH (NOLOCK)  WHERE NC.INSTANCIA=@ID)<2  AND (SELECT VERSION FROM NOCONFORMIDAD WITH (NOLOCK)   WHERE INSTANCIA=@ID)=@VERSION  --Si es una no conformidad y est�za cerrada  no se podr�n modificar los campos." & vbCrLf
sConsulta = sConsulta & "   SET @ESCRITURA_NOCONF=1" & vbCrLf
sConsulta = sConsulta & "        ELSE" & vbCrLf
sConsulta = sConsulta & "   SET @ESCRITURA_NOCONF=0" & vbCrLf
sConsulta = sConsulta & "      end " & vbCrLf
sConsulta = sConsulta & " end" & vbCrLf
sConsulta = sConsulta & "IF (SELECT COUNT(*) FROM COPIA_CONF_CUMPLIMENTACION  WITH (NOLOCK)  WHERE INSTANCIA = @ID)>0" & vbCrLf
sConsulta = sConsulta & "  SET @HAYCONFCUMP = 1" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  SET @HAYCONFCUMP = 0" & vbCrLf
sConsulta = sConsulta & "--Obtiene los grupos:" & vbCrLf
sConsulta = sConsulta & "IF @IDI IS NULL" & vbCrLf
sConsulta = sConsulta & "  SET @SQL = N'SELECT F.ID,F.INSTANCIA,F.DEN_SPA,F.DEN_ENG,F.DEN_GER FROM COPIA_GRUPO F WITH (NOLOCK)   WHERE F.INSTANCIA =  @ID ORDER BY F.ORDEN'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  SET @SQL = N'SELECT F.ID, F.INSTANCIA, F.DEN_' + @IDI + ', COUNT(CD.ID) NUMCAMPOS FROM COPIA_GRUPO F WITH (NOLOCK)   '" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + '  INNER JOIN COPIA_CAMPO_DEF CD WITH (NOLOCK)   ON F.ID=CD.GRUPO' " & vbCrLf
sConsulta = sConsulta & "IF @HAYCONFCUMP = 1" & vbCrLf
sConsulta = sConsulta & "  IF @ESTADO = 0" & vbCrLf
sConsulta = sConsulta & "    SET @SQL = @SQL + '   INNER JOIN COPIA_CONF_CUMPLIMENTACION CC WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                  ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                 AND CC.COPIA_PASO IS NULL  " & vbCrLf
sConsulta = sConsulta & "                                 AND CC.SOLIC_PROVE = 1 " & vbCrLf
sConsulta = sConsulta & "                                 AND CC.INSTANCIA=F.INSTANCIA '" & vbCrLf
sConsulta = sConsulta & "  ELSE" & vbCrLf
sConsulta = sConsulta & "    IF @ESTADO = 2 --Pendiente de aprobar" & vbCrLf
sConsulta = sConsulta & "      IF @TRASLADADA = 1-- ha sido trasladada" & vbCrLf
sConsulta = sConsulta & "        IF @USU = @DESTINATARIO -- la est� viendo el usuario al que le ha sido trasladada" & vbCrLf
sConsulta = sConsulta & "          SET @SQL = @SQL + '  INNER JOIN INSTANCIA WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                       ON F.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                               INNER JOIN COPIA_SUBPASO WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                       ON INSTANCIA.APROB=COPIA_SUBPASO.ID" & vbCrLf
sConsulta = sConsulta & "                               INNER JOIN COPIA_CONF_CUMPLIMENTACION CC WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                       ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                      AND CC.INSTANCIA=INSTANCIA.ID   " & vbCrLf
sConsulta = sConsulta & "                                      AND CC.COPIA_PASO=COPIA_SUBPASO.PASO " & vbCrLf
sConsulta = sConsulta & "                                      AND CC.SOLIC_PROVE IS NULL'    " & vbCrLf
sConsulta = sConsulta & "        ELSE -- la est� viendo otro..." & vbCrLf
sConsulta = sConsulta & "          IF @PROVE IS NULL" & vbCrLf
sConsulta = sConsulta & "            SET @SQL = @SQL + '  INNER JOIN INSTANCIA WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                         ON F.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                 INNER JOIN COPIA_SUBPASO WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                         ON INSTANCIA.APROB=COPIA_SUBPASO.ID" & vbCrLf
sConsulta = sConsulta & "                                 INNER JOIN COPIA_CONF_CUMPLIMENTACION CC WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                         ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.INSTANCIA=INSTANCIA.ID  " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.COPIA_PASO=COPIA_SUBPASO.PASO " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.SOLIC_PROVE IS NULL'  " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "          ELSE-- ha sido trasladada al proveedor y la est� viendo el proveedor" & vbCrLf
sConsulta = sConsulta & "            IF @CUMP_PROVE = 1 --usar configuracion de cumplimentaci�n del proveedor" & vbCrLf
sConsulta = sConsulta & "              SET @SQL = @SQL + '  INNER JOIN INSTANCIA WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                           ON F.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                   INNER JOIN INSTANCIA_EST IE WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                           ON IE.ID = INSTANCIA.INSTANCIA_EST " & vbCrLf
sConsulta = sConsulta & "                                          AND IE.INSTANCIA = INSTANCIA.ID" & vbCrLf
sConsulta = sConsulta & "                                          AND IE.DESTINATARIO_PROV = @PROVE" & vbCrLf
sConsulta = sConsulta & "                                   INNER JOIN COPIA_CONF_CUMPLIMENTACION CC WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                           ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.COPIA_PASO IS NULL  " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.INSTANCIA=INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.SOLIC_PROVE =2'   " & vbCrLf
sConsulta = sConsulta & "            ELSE --usar la configuragcion de cumplimentacion del aprobador que ha trasladado la instancia" & vbCrLf
sConsulta = sConsulta & "              SET @SQL = @SQL + '  INNER JOIN INSTANCIA WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                           ON F.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                   INNER JOIN COPIA_SUBPASO WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                           ON INSTANCIA.APROB=COPIA_SUBPASO.ID" & vbCrLf
sConsulta = sConsulta & "                                   INNER JOIN COPIA_CONF_CUMPLIMENTACION CC WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                           ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.INSTANCIA=INSTANCIA.ID  " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.COPIA_PASO=COPIA_SUBPASO.PASO " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.SOLIC_PROVE IS NULL'  " & vbCrLf
sConsulta = sConsulta & "      ELSE --no ha sido trasladada" & vbCrLf
sConsulta = sConsulta & "        IF @USU = @PETICIONARIO -- y la est� viendo el peticionario (no la podr� modificar)" & vbCrLf
sConsulta = sConsulta & "          IF @USU = @APROB -- a no ser que el peticionario sea aprobador, sacar�a la configuracion de cumplimentacion del paso" & vbCrLf
sConsulta = sConsulta & "            SET @SQL = @SQL + '  INNER JOIN INSTANCIA WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                         ON F.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                 INNER JOIN COPIA_SUBPASO WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                         ON INSTANCIA.APROB=COPIA_SUBPASO.ID" & vbCrLf
sConsulta = sConsulta & "                                 INNER JOIN COPIA_CONF_CUMPLIMENTACION CC  WITH (NOLOCK)  " & vbCrLf
sConsulta = sConsulta & "                                         ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.INSTANCIA=INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.COPIA_PASO=COPIA_SUBPASO.PASO " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.SOLIC_PROVE IS NULL'  " & vbCrLf
sConsulta = sConsulta & "          ELSE -- configuracion del peticionario" & vbCrLf
sConsulta = sConsulta & "            SET @SQL = @SQL + '    INNER JOIN INSTANCIA WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                           ON F.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                   INNER JOIN COPIA_CONF_CUMPLIMENTACION CC WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                           ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.COPIA_PASO IS NULL  " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.SOLIC_PROVE = 1" & vbCrLf
sConsulta = sConsulta & "                                          AND F.INSTANCIA=INSTANCIA.ID  '" & vbCrLf
sConsulta = sConsulta & "        ELSE -- la est� viendo el aprobador por lo que sacampos la configuracion de cumplimentacion del paso" & vbCrLf
sConsulta = sConsulta & "          SET @SQL = @SQL + '  INNER JOIN INSTANCIA WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                       ON F.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                               INNER JOIN COPIA_SUBPASO WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                       ON INSTANCIA.APROB=COPIA_SUBPASO.ID" & vbCrLf
sConsulta = sConsulta & "                               INNER JOIN COPIA_CONF_CUMPLIMENTACION CC  WITH (NOLOCK)  " & vbCrLf
sConsulta = sConsulta & "                                       ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                      AND CC.INSTANCIA=INSTANCIA.ID  " & vbCrLf
sConsulta = sConsulta & "                                      AND CC.COPIA_PASO=COPIA_SUBPASO.PASO " & vbCrLf
sConsulta = sConsulta & "                                      AND CC.SOLIC_PROVE IS NULL'  " & vbCrLf
sConsulta = sConsulta & "    ELSE -- NO EST� PENDIENTE DE APROBAR" & vbCrLf
sConsulta = sConsulta & "      IF @ESTADO = 200 or @ESTADO = 300 or @ESTADO=301 --Es un certificado o una no conformidad" & vbCrLf
sConsulta = sConsulta & "        IF @USU IS NULL  --  -- Se est� cargando como certificado por el proveedor" & vbCrLf
sConsulta = sConsulta & "          SET @SQL = @SQL + '  INNER JOIN INSTANCIA WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                       ON F.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                               INNER JOIN COPIA_CONF_CUMPLIMENTACION CC WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                       ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                      AND CC.INSTANCIA=INSTANCIA.ID   " & vbCrLf
sConsulta = sConsulta & "                                      AND CC.COPIA_PASO IS NULL  " & vbCrLf
sConsulta = sConsulta & "                                      AND CC.SOLIC_PROVE =2 AND CC.ES_ESTADO=0 '    " & vbCrLf
sConsulta = sConsulta & "        ELSE" & vbCrLf
sConsulta = sConsulta & "          IF @USU = @PETICIONARIO -- la est� viendo el peticionario" & vbCrLf
sConsulta = sConsulta & "            SET @SQL = @SQL + '  INNER JOIN INSTANCIA WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                         ON F.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                 INNER JOIN COPIA_CONF_CUMPLIMENTACION CC  WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                         ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.INSTANCIA=INSTANCIA.ID   " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.COPIA_PASO IS NULL  " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.SOLIC_PROVE =1 AND CC.ES_ESTADO=0 '    " & vbCrLf
sConsulta = sConsulta & "          ELSE --la est� viendo otra persona " & vbCrLf
sConsulta = sConsulta & "   IF @PETICIONARIO IS NULL --Pero es un certificado sin peticionario. Es decir, creado por el proveedor." & vbCrLf
sConsulta = sConsulta & "                   SET @SQL = @SQL + '  INNER JOIN INSTANCIA WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                         ON F.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                 INNER JOIN COPIA_CONF_CUMPLIMENTACION CC WITH (NOLOCK)    " & vbCrLf
sConsulta = sConsulta & "                                         ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.INSTANCIA=INSTANCIA.ID   " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.COPIA_PASO IS NULL  " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.SOLIC_PROVE =1 AND CC.ES_ESTADO=0 '    " & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "                   SET @SQL = @SQL + '    INNER JOIN INSTANCIA WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                           ON F.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                   INNER JOIN COPIA_CONF_CUMPLIMENTACION CC WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                           ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.COPIA_PASO IS NULL  " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.SOLIC_PROVE = 1" & vbCrLf
sConsulta = sConsulta & "                                          AND F.INSTANCIA=INSTANCIA.ID  AND CC.ES_ESTADO=0 '" & vbCrLf
sConsulta = sConsulta & "        ELSE --Est� en otro estado" & vbCrLf
sConsulta = sConsulta & "          IF @USU = @PETICIONARIO --la est� viendo el peticionario" & vbCrLf
sConsulta = sConsulta & "            SET @SQL = @SQL + '    INNER JOIN INSTANCIA WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                           ON F.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                   INNER JOIN COPIA_CONF_CUMPLIMENTACION CC  WITH (NOLOCK)  " & vbCrLf
sConsulta = sConsulta & "                                           ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.COPIA_PASO IS NULL  " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.SOLIC_PROVE = 1 " & vbCrLf
sConsulta = sConsulta & "                                          AND F.INSTANCIA=INSTANCIA.ID  '" & vbCrLf
sConsulta = sConsulta & "          ELSE --la est� viendo un aprobador por lo que se coje la conf-cumplimentacion que ten�a cuando la aprob�" & vbCrLf
sConsulta = sConsulta & "                   SET @SQL = @SQL + '  INNER JOIN INSTANCIA  WITH (NOLOCK)  " & vbCrLf
sConsulta = sConsulta & "                                         ON F.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                  INNER JOIN COPIA_SUBPASO WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                         ON ISNULL(INSTANCIA.APROB, (SELECT TOP 1 APROB FROM INSTANCIA_EST WITH(NOLOCK) WHERE INSTANCIA = @ID AND PER = @USU))=COPIA_SUBPASO.ID" & vbCrLf
sConsulta = sConsulta & "                                 INNER JOIN COPIA_CONF_CUMPLIMENTACION CC WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                         ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.INSTANCIA=INSTANCIA.ID   " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.COPIA_PASO=COPIA_SUBPASO.PASO " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.SOLIC_PROVE IS NULL'    " & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ' AND CC.VISIBLE = 1 AND CD.ES_SUBCAMPO=0'" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N' WHERE F.INSTANCIA =  @ID GROUP BY F.ID, F.ORDEN,F.INSTANCIA, F.DEN_' + @IDI + ' ORDER BY F.ORDEN'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID INT, @VERSION INT = NULL,@USU VARCHAR(50), @PROVE VARCHAR(50)', @ID=@ID, @VERSION=@VERSION,@USU=@USU,@PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "--- OBTIENE LOS CAMPOS:" & vbCrLf
sConsulta = sConsulta & "IF @IDI IS NULL " & vbCrLf
sConsulta = sConsulta & "    SET @SQL = N'SELECT CD.ID,CD.GRUPO,CD.DEN_SPA,CD.DEN_ENG,CD.DEN_GER,CD.AYUDA_SPA,CD.AYUDA_ENG,CD.AYUDA_GER,CD.TIPO,CD,SUBTIPO,CD.INTRO,CD.MINNUM, CD.MAXNUM, CD.MINFEC, CD.MAXFEC, CD.ORDEN, CD.ID_ATRIB_GS, CD.TIPO_CAMPO_GS, CD.ID_CALCULO, CD.FORMULA, CD.ORDEN_CALCULO, CD.ORIGEN_CALC_DESGLOSE, CD.POPUP,CD.ANYADIR_ART,CD.IDREFSOLICITUD,CD.AVISOBLOQUEOIMPACUM,CD.CARGAR_ULT_ADJ,CD.MAX_LENGTH,C.ID AS ID_CAMPO, C.VALOR_NUM, C.VALOR_TEXT, C.VALOR_FEC, C.VALOR_BOOL'   " & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "    SET @SQL = N'SELECT DISTINCT(C.ID) AS ID_CAMPO,CD.ID, CD.GRUPO, CD.DEN_' + @IDI + ', AYUDA_' + @IDI + ', CD.TIPO, CD.SUBTIPO, CD.INTRO, C.VALOR_NUM, LEFT(C.VALOR_TEXT, 3980) VALOR_TEXT, C.VALOR_FEC, C.VALOR_BOOL, CD.MINNUM, CD.MAXNUM, CD.MINFEC, CD.MAXFEC, CD.ORDEN, CD.ID_ATRIB_GS, CD.TIPO_CAMPO_GS, CD.ID_CALCULO, CD.FORMULA, CD.ORDEN_CALCULO, CD.ORIGEN_CALC_DESGLOSE, CD.POPUP,CD.ANYADIR_ART,CD.IDREFSOLICITUD,CD.AVISOBLOQUEOIMPACUM,CD.CARGAR_ULT_ADJ,CD.MAX_LENGTH '" & vbCrLf
sConsulta = sConsulta & "IF @HAYCONFCUMP = 0" & vbCrLf
sConsulta = sConsulta & "  IF @ESTADO=0 " & vbCrLf
sConsulta = sConsulta & "    IF @USU = @PETICIONARIO --La instancia est� guardada y es el propio cumplimentador el que accede a ella" & vbCrLf
sConsulta = sConsulta & "      SET @SQL = @SQL + ', 1 ESCRITURA, 1 VISIBLE, 0 OBLIGATORIO '" & vbCrLf
sConsulta = sConsulta & "    ELSE --La instancia est� guardada, pendiente de enviar y la ve otro ?�" & vbCrLf
sConsulta = sConsulta & "      SET @SQL = @SQL + ', 0 ESCRITURA, 1 VISIBLE, 0 OBLIGATORIO '" & vbCrLf
sConsulta = sConsulta & "  ELSE" & vbCrLf
sConsulta = sConsulta & "    IF @ESTADO = 2 --Pendiente de aprobar" & vbCrLf
sConsulta = sConsulta & "      IF @TRASLADADA = 1 -- ha sido trasladada" & vbCrLf
sConsulta = sConsulta & "        IF @USU = @DESTINATARIO -- la est� viendo el usuario al que le ha sido trasladada" & vbCrLf
sConsulta = sConsulta & "           SET @SQL = @SQL + ', 1 ESCRITURA, 1 VISIBLE, 0 OBLIGATORIO '" & vbCrLf
sConsulta = sConsulta & "        ELSE -- la est� viendo otro..." & vbCrLf
sConsulta = sConsulta & "          IF @PROVE IS NULL" & vbCrLf
sConsulta = sConsulta & "            SET @SQL = @SQL + ', 0 ESCRITURA, 1 VISIBLE , 0 OBLIGATORIO'" & vbCrLf
sConsulta = sConsulta & "          ELSE" & vbCrLf
sConsulta = sConsulta & "            SET @SQL = @SQL + ', 1 ESCRITURA, 1 VISIBLE , 0 OBLIGATORIO'" & vbCrLf
sConsulta = sConsulta & "      ELSE -- no ha sido trasladada" & vbCrLf
sConsulta = sConsulta & "        IF @USU = @PETICIONARIO -- y la est� viendo el peticionario (no la podr� modificar)" & vbCrLf
sConsulta = sConsulta & "          IF @USU = @APROB" & vbCrLf
sConsulta = sConsulta & "            SET @SQL = @SQL + ', 1 ESCRITURA, 1 VISIBLE, 0 OBLIGATORIO'" & vbCrLf
sConsulta = sConsulta & "          ELSE" & vbCrLf
sConsulta = sConsulta & "            SET @SQL = @SQL + ', 0 ESCRITURA, 1 VISIBLE, 0 OBLIGATORIO '" & vbCrLf
sConsulta = sConsulta & "        ELSE -- la est� viendo el aprobador" & vbCrLf
sConsulta = sConsulta & "           SET @SQL = @SQL + ', 1 ESCRITURA, 1 VISIBLE, 0 OBLIGATORIO'" & vbCrLf
sConsulta = sConsulta & "    ELSE" & vbCrLf
sConsulta = sConsulta & "      IF @ESTADO = 200 --Certificado publicado" & vbCrLf
sConsulta = sConsulta & "        IF @USU IS NULL" & vbCrLf
sConsulta = sConsulta & "          SET @SQL = @SQL + ', 1 ESCRITURA, 1 VISIBLE, 0 OBLIGATORIO'" & vbCrLf
sConsulta = sConsulta & "        ELSE" & vbCrLf
sConsulta = sConsulta & "              IF @USU=@PETICIONARIO" & vbCrLf
sConsulta = sConsulta & "                    IF @ESCRITURA_CERTIF=1" & vbCrLf
sConsulta = sConsulta & "                        SET @SQL = @SQL + ', 0 ESCRITURA,1 VISIBLE, 0 OBLIGATORIO'" & vbCrLf
sConsulta = sConsulta & "                    ELSE" & vbCrLf
sConsulta = sConsulta & "                        SET @SQL = @SQL + ', 1 ESCRITURA, 1 VISIBLE, 0 OBLIGATORIO'" & vbCrLf
sConsulta = sConsulta & "             ELSE" & vbCrLf
sConsulta = sConsulta & "                   SET @SQL = @SQL + ', 0 ESCRITURA, 1 VISIBLE, 0 OBLIGATORIO'" & vbCrLf
sConsulta = sConsulta & "       ELSE " & vbCrLf
sConsulta = sConsulta & "        IF @ESTADO = 300 OR @ESTADO = 301-- es una no conformidad" & vbCrLf
sConsulta = sConsulta & "              IF @USU IS NULL  --UN PROVEEDOR" & vbCrLf
sConsulta = sConsulta & "                 SET @SQL = @SQL + ', CC.ESCRITURA, CC.VISIBLE, CC.OBLIGATORIO'" & vbCrLf
sConsulta = sConsulta & "              ELSE" & vbCrLf
sConsulta = sConsulta & "                IF @USU=@PETICIONARIO -- la est� viendo el peticionario " & vbCrLf
sConsulta = sConsulta & "                    IF @ESCRITURA_NOCONF=1" & vbCrLf
sConsulta = sConsulta & "                  SET @SQL = @SQL + ', CC.ESCRITURA, CC.VISIBLE, CC.OBLIGATORIO'" & vbCrLf
sConsulta = sConsulta & "                    ELSE" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = @SQL + ', 0 ESCRITURA, CC.VISIBLE, CC.OBLIGATORIO'" & vbCrLf
sConsulta = sConsulta & "                ELSE -- la est� viendo otra persona" & vbCrLf
sConsulta = sConsulta & "                  SET @SQL = @SQL + ', 0 ESCRITURA, CC.VISIBLE, CC.OBLIGATORIO'" & vbCrLf
sConsulta = sConsulta & "        ELSE --est� en otro estado" & vbCrLf
sConsulta = sConsulta & "            SET @SQL = @SQL + ', 0 ESCRITURA, 1 VISIBLE, 0 OBLIGATORIO'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  IF @ESTADO=0 " & vbCrLf
sConsulta = sConsulta & "    IF @USU = @PETICIONARIO --La instancia est� guardada y es el propio cumplimentador el que accede a ella" & vbCrLf
sConsulta = sConsulta & "      SET @SQL = @SQL + ', CC.ESCRITURA, CC.VISIBLE, CC.OBLIGATORIO '" & vbCrLf
sConsulta = sConsulta & "    ELSE --La instancia est� guardada, pendiente de enviar y la ve otro ?�" & vbCrLf
sConsulta = sConsulta & "      SET @SQL = @SQL + ', 0 ESCRITURA, CC.VISIBLE, CC.OBLIGATORIO '" & vbCrLf
sConsulta = sConsulta & "  ELSE" & vbCrLf
sConsulta = sConsulta & "    IF @ESTADO = 2 --Pendiente de aprobar" & vbCrLf
sConsulta = sConsulta & "      IF @TRASLADADA = 1 -- ha sido trasladada" & vbCrLf
sConsulta = sConsulta & "        IF @USU = @DESTINATARIO -- la est� viendo el usuario al que le ha sido trasladada" & vbCrLf
sConsulta = sConsulta & "           SET @SQL = @SQL + ', CC.ESCRITURA, CC.VISIBLE, CC.OBLIGATORIO '" & vbCrLf
sConsulta = sConsulta & "        ELSE -- la est� viendo otro..." & vbCrLf
sConsulta = sConsulta & "          IF @PROVE IS NULL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "            SET @SQL = @SQL + ', 0 ESCRITURA, CC.VISIBLE , CC.OBLIGATORIO'" & vbCrLf
sConsulta = sConsulta & "          ELSE -- ha sido trasladada al proveedor" & vbCrLf
sConsulta = sConsulta & "            SET @SQL = @SQL + ', CC.ESCRITURA, CC.VISIBLE , CC.OBLIGATORIO'" & vbCrLf
sConsulta = sConsulta & "      ELSE -- no ha sido trasladada" & vbCrLf
sConsulta = sConsulta & "        IF @USU = @PETICIONARIO -- y la est� viendo el peticionario (no la podr� modificar)" & vbCrLf
sConsulta = sConsulta & "          IF @USU = @APROB -- a no ser que el peticionario sea aprobador" & vbCrLf
sConsulta = sConsulta & "            SET @SQL = @SQL + ', CC.ESCRITURA, CC.VISIBLE, CC.OBLIGATORIO'" & vbCrLf
sConsulta = sConsulta & "          ELSE -- la est� consultando el peticionario" & vbCrLf
sConsulta = sConsulta & "            SET @SQL = @SQL + ', 0 ESCRITURA, CC.VISIBLE, CC.OBLIGATORIO '" & vbCrLf
sConsulta = sConsulta & "        ELSE" & vbCrLf
sConsulta = sConsulta & "          IF @USU = @APROB  -- la est� viendo el aprobador" & vbCrLf
sConsulta = sConsulta & "            SET @SQL = @SQL + ', CC.ESCRITURA, CC.VISIBLE, CC.OBLIGATORIO'" & vbCrLf
sConsulta = sConsulta & "          ELSE  --la esta viendo otra persona (imagino que por que la podr� ver... pero modificar no)" & vbCrLf
sConsulta = sConsulta & "            SET @SQL = @SQL + ', 0 ESCRITURA, CC.VISIBLE, CC.OBLIGATORIO '" & vbCrLf
sConsulta = sConsulta & "    ELSE -- no est� pendiente de aprobar" & vbCrLf
sConsulta = sConsulta & "      IF @ESTADO = 200 --Certificado publicado" & vbCrLf
sConsulta = sConsulta & "        IF @USU IS NULL  --UN PROVEEDOR" & vbCrLf
sConsulta = sConsulta & "          SET @SQL = @SQL + ', CC.ESCRITURA, CC.VISIBLE, CC.OBLIGATORIO'" & vbCrLf
sConsulta = sConsulta & "        ELSE" & vbCrLf
sConsulta = sConsulta & "          IF @USU=@PETICIONARIO -- la est� viendo el peticionario" & vbCrLf
sConsulta = sConsulta & "            IF @ESCRITURA_CERTIF=1  --est� publicada, por tanto el peticionario no puede modificar" & vbCrLf
sConsulta = sConsulta & "              SET @SQL = @SQL + ', 0 ESCRITURA, CC.VISIBLE, CC.OBLIGATORIO'" & vbCrLf
sConsulta = sConsulta & "            ELSE" & vbCrLf
sConsulta = sConsulta & "              SET @SQL = @SQL + ', CC.ESCRITURA, CC.VISIBLE, CC.OBLIGATORIO'" & vbCrLf
sConsulta = sConsulta & "          ELSE -- la est� viendo otra persona" & vbCrLf
sConsulta = sConsulta & "   IF @PETICIONARIO IS NULL --Si es otra persona.Pero pq es un certificado creado por proveedor, lo que implica peticionario nulo." & vbCrLf
sConsulta = sConsulta & "       IF @ESCRITURA_CERTIF=1  --est� publicada, por tanto el peticionario no puede modificar" & vbCrLf
sConsulta = sConsulta & "                     SET @SQL = @SQL + ', 0 ESCRITURA, CC.VISIBLE, CC.OBLIGATORIO'" & vbCrLf
sConsulta = sConsulta & "                 ELSE" & vbCrLf
sConsulta = sConsulta & "                     SET @SQL = @SQL + ', CC.ESCRITURA, CC.VISIBLE, CC.OBLIGATORIO'    " & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "                   SET @SQL = @SQL + ', 0 ESCRITURA, CC.VISIBLE, CC.OBLIGATORIO'" & vbCrLf
sConsulta = sConsulta & "      ELSE --est� en otro estado" & vbCrLf
sConsulta = sConsulta & "        IF @ESTADO = 300 OR @ESTADO = 301 -- es una no conformidad" & vbCrLf
sConsulta = sConsulta & "              IF @USU IS NULL  --UN PROVEEDOR" & vbCrLf
sConsulta = sConsulta & "                 SET @SQL = @SQL + ', CC.ESCRITURA, CC.VISIBLE, CC.OBLIGATORIO'" & vbCrLf
sConsulta = sConsulta & "              ELSE" & vbCrLf
sConsulta = sConsulta & "                IF @USU=@PETICIONARIO -- la est� viendo el peticionario" & vbCrLf
sConsulta = sConsulta & "                   IF @ESCRITURA_NOCONF=1" & vbCrLf
sConsulta = sConsulta & "                  SET @SQL = @SQL + ', CC.ESCRITURA, CC.VISIBLE, CC.OBLIGATORIO'" & vbCrLf
sConsulta = sConsulta & "                    ELSE" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = @SQL + ', 0 ESCRITURA, CC.VISIBLE, CC.OBLIGATORIO'" & vbCrLf
sConsulta = sConsulta & "                ELSE -- la est� viendo otra persona" & vbCrLf
sConsulta = sConsulta & "                  SET @SQL = @SQL + ', 0 ESCRITURA, CC.VISIBLE, CC.OBLIGATORIO'" & vbCrLf
sConsulta = sConsulta & "        ELSE" & vbCrLf
sConsulta = sConsulta & "          IF @ESTADO=6 AND @USU=@PETICIONARIO" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = @SQL + ', CC.ESCRITURA, CC.VISIBLE, CC.OBLIGATORIO'" & vbCrLf
sConsulta = sConsulta & "          ELSE" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = @SQL + ', 0 ESCRITURA, CC.VISIBLE, CC.OBLIGATORIO'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @HAYCONFCUMP=1" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + ',CC.ES_ESTADO'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF (@ESTADO=0 OR @ESTADO=2) AND @USU IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "  IF @HAYCONFCUMP=1" & vbCrLf
sConsulta = sConsulta & "    SET @SQL = @SQL +  ' ,G.ID, CC.ORDEN'" & vbCrLf
sConsulta = sConsulta & "  ELSE" & vbCrLf
sConsulta = sConsulta & "    SET @SQL = @SQL +  ' ,G.ID, CD.ORDEN'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  SET @SQL = @SQL +  ' ,G.ID, CD.ORDEN'" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ', CD.TABLA_EXTERNA, C.CAMBIO'" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ' FROM COPIA_CAMPO C WITH (NOLOCK)   INNER JOIN COPIA_CAMPO_DEF CD WITH (NOLOCK)  ON C.COPIA_CAMPO_DEF=CD.ID INNER JOIN COPIA_GRUPO G WITH (NOLOCK)   ON CD.GRUPO = G.ID' " & vbCrLf
sConsulta = sConsulta & "IF @HAYCONFCUMP = 1" & vbCrLf
sConsulta = sConsulta & "  IF @ESTADO = 0" & vbCrLf
sConsulta = sConsulta & "    SET @SQL = @SQL + '   INNER JOIN COPIA_CONF_CUMPLIMENTACION CC WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                  ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                 AND CC.COPIA_PASO IS NULL  " & vbCrLf
sConsulta = sConsulta & "                                 AND CC.SOLIC_PROVE = 1" & vbCrLf
sConsulta = sConsulta & "                                 AND CC.INSTANCIA=C.INSTANCIA   '" & vbCrLf
sConsulta = sConsulta & "  ELSE" & vbCrLf
sConsulta = sConsulta & "    IF @ESTADO = 2 --Pendiente de aprobar" & vbCrLf
sConsulta = sConsulta & "      IF @TRASLADADA = 1-- ha sido trasladada" & vbCrLf
sConsulta = sConsulta & "        IF @USU = @DESTINATARIO -- la est� viendo el usuario al que le ha sido trasladada" & vbCrLf
sConsulta = sConsulta & "          SET @SQL = @SQL + '  INNER JOIN INSTANCIA WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                       ON C.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                               INNER JOIN COPIA_SUBPASO WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                       ON INSTANCIA.APROB=COPIA_SUBPASO.ID" & vbCrLf
sConsulta = sConsulta & "                               INNER JOIN COPIA_CONF_CUMPLIMENTACION CC WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                       ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                      AND CC.INSTANCIA=INSTANCIA.ID   " & vbCrLf
sConsulta = sConsulta & "                                      AND CC.COPIA_PASO=COPIA_SUBPASO.PASO " & vbCrLf
sConsulta = sConsulta & "                                      AND CC.SOLIC_PROVE IS NULL'    " & vbCrLf
sConsulta = sConsulta & "        ELSE -- la est� viendo otro..." & vbCrLf
sConsulta = sConsulta & "          IF @PROVE IS NULL" & vbCrLf
sConsulta = sConsulta & "            SET @SQL = @SQL + '  INNER JOIN INSTANCIA WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                         ON C.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                 INNER JOIN COPIA_SUBPASO WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                         ON INSTANCIA.APROB=COPIA_SUBPASO.ID" & vbCrLf
sConsulta = sConsulta & "                                 INNER JOIN COPIA_CONF_CUMPLIMENTACION CC WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                         ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.INSTANCIA=INSTANCIA.ID   " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.COPIA_PASO=COPIA_SUBPASO.PASO " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.SOLIC_PROVE IS NULL'  " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "          ELSE-- ha sido trasladada al proveedor y la est� viendo el proveedor" & vbCrLf
sConsulta = sConsulta & "            IF @CUMP_PROVE = 1 --usar configuracion de cumplimentaci�n del proveedor" & vbCrLf
sConsulta = sConsulta & "              SET @SQL = @SQL + '  INNER JOIN INSTANCIA  WITH (NOLOCK)  " & vbCrLf
sConsulta = sConsulta & "                                           ON C.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                   INNER JOIN INSTANCIA_EST IE WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                           ON IE.ID = INSTANCIA.INSTANCIA_EST" & vbCrLf
sConsulta = sConsulta & "                                          AND IE.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                          AND IE.DESTINATARIO_PROV = @PROVE" & vbCrLf
sConsulta = sConsulta & "                                   INNER JOIN COPIA_CONF_CUMPLIMENTACION CC  WITH (NOLOCK)  " & vbCrLf
sConsulta = sConsulta & "                                           ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.COPIA_PASO IS NULL  " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.INSTANCIA=INSTANCIA.ID" & vbCrLf
sConsulta = sConsulta & "                                          AND CC.SOLIC_PROVE =2'   " & vbCrLf
sConsulta = sConsulta & "            ELSE --usar la configuragcion de cumplimentacion del aprobador que ha trasladado la instancia" & vbCrLf
sConsulta = sConsulta & "              SET @SQL = @SQL + '  INNER JOIN INSTANCIA WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                           ON C.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                   INNER JOIN COPIA_SUBPASO WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                           ON INSTANCIA.APROB=COPIA_SUBPASO.ID" & vbCrLf
sConsulta = sConsulta & "                                   INNER JOIN COPIA_CONF_CUMPLIMENTACION CC WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                           ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.INSTANCIA=INSTANCIA.ID   " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.COPIA_PASO=COPIA_SUBPASO.PASO " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.SOLIC_PROVE IS NULL'  " & vbCrLf
sConsulta = sConsulta & "      ELSE --no ha sido trasladada" & vbCrLf
sConsulta = sConsulta & "        IF @USU = @PETICIONARIO -- y la est� viendo el peticionario (no la podr� modificar)" & vbCrLf
sConsulta = sConsulta & "          IF @USU = @APROB -- a no ser que el peticionario sea aprobador, sacar�a la configuracion de cumplimentacion del paso" & vbCrLf
sConsulta = sConsulta & "            SET @SQL = @SQL + '  INNER JOIN INSTANCIA WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                         ON C.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                 INNER JOIN COPIA_SUBPASO WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                         ON INSTANCIA.APROB=COPIA_SUBPASO.ID" & vbCrLf
sConsulta = sConsulta & "                                 INNER JOIN COPIA_CONF_CUMPLIMENTACION CC WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                         ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.INSTANCIA=INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.COPIA_PASO=COPIA_SUBPASO.PASO " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.SOLIC_PROVE IS NULL'  " & vbCrLf
sConsulta = sConsulta & "          ELSE -- configuracion del peticionario" & vbCrLf
sConsulta = sConsulta & "            SET @SQL = @SQL + '    INNER JOIN INSTANCIA WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                           ON C.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                   INNER JOIN COPIA_CONF_CUMPLIMENTACION CC WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                           ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.COPIA_PASO IS NULL  " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.SOLIC_PROVE = 1" & vbCrLf
sConsulta = sConsulta & "                                          AND C.INSTANCIA=INSTANCIA.ID '" & vbCrLf
sConsulta = sConsulta & "        ELSE -- la est� viendo el aprobador por lo que sacampos la configuracion de cumplimentacion del paso" & vbCrLf
sConsulta = sConsulta & "          SET @SQL = @SQL + '  INNER JOIN INSTANCIA WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                       ON C.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                               INNER JOIN COPIA_SUBPASO WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                       ON INSTANCIA.APROB=COPIA_SUBPASO.ID" & vbCrLf
sConsulta = sConsulta & "                               INNER JOIN COPIA_CONF_CUMPLIMENTACION CC WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                       ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                      AND CC.INSTANCIA=INSTANCIA.ID  " & vbCrLf
sConsulta = sConsulta & "                                      AND CC.COPIA_PASO=COPIA_SUBPASO.PASO " & vbCrLf
sConsulta = sConsulta & "                                      AND CC.SOLIC_PROVE IS NULL'  " & vbCrLf
sConsulta = sConsulta & "    ELSE -- NO EST� PENDIENTE DE APROBAR" & vbCrLf
sConsulta = sConsulta & "      IF @ESTADO = 200 or @ESTADO=300 OR @ESTADO = 301" & vbCrLf
sConsulta = sConsulta & "        IF @USU IS NULL  --  -- Se est� cargando como certificado por el proveedor" & vbCrLf
sConsulta = sConsulta & "          SET @SQL = @SQL + '  INNER JOIN INSTANCIA WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                       ON C.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                               INNER JOIN COPIA_CONF_CUMPLIMENTACION CC  WITH (NOLOCK)  " & vbCrLf
sConsulta = sConsulta & "                                       ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                      AND CC.INSTANCIA=INSTANCIA.ID  " & vbCrLf
sConsulta = sConsulta & "                                      AND CC.COPIA_PASO IS NULL  " & vbCrLf
sConsulta = sConsulta & "                                      AND CC.SOLIC_PROVE =2 AND CC.ES_ESTADO=0'    " & vbCrLf
sConsulta = sConsulta & "        ELSE" & vbCrLf
sConsulta = sConsulta & "          IF @USU = @PETICIONARIO -- la est� viendo el peticionario" & vbCrLf
sConsulta = sConsulta & "            SET @SQL = @SQL + '  INNER JOIN INSTANCIA WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                         ON C.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                 INNER JOIN COPIA_CONF_CUMPLIMENTACION CC WITH (NOLOCK)    " & vbCrLf
sConsulta = sConsulta & "                                         ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.INSTANCIA=INSTANCIA.ID   " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.COPIA_PASO IS NULL  " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.SOLIC_PROVE =1 AND CC.ES_ESTADO=0'    " & vbCrLf
sConsulta = sConsulta & "          ELSE --la est� viendo otra persona " & vbCrLf
sConsulta = sConsulta & "   IF @PETICIONARIO IS NULL --Pero es un certificado hecho por el proveedor. Lo que implica peticionario nulo" & vbCrLf
sConsulta = sConsulta & "        SET @SQL = @SQL + '  INNER JOIN INSTANCIA  WITH (NOLOCK)  " & vbCrLf
sConsulta = sConsulta & "                                         ON C.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                 INNER JOIN COPIA_CONF_CUMPLIMENTACION CC WITH (NOLOCK)    " & vbCrLf
sConsulta = sConsulta & "                                         ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.INSTANCIA=INSTANCIA.ID    " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.COPIA_PASO IS NULL  " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.SOLIC_PROVE =1 AND CC.ES_ESTADO=0'                " & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "        SET @SQL = @SQL + '    INNER JOIN INSTANCIA WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                           ON C.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                   INNER JOIN COPIA_CONF_CUMPLIMENTACION CC WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                           ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.COPIA_PASO IS NULL  " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.SOLIC_PROVE = 1 " & vbCrLf
sConsulta = sConsulta & "                                          AND C.INSTANCIA=INSTANCIA.ID  AND CC.ES_ESTADO=0 '" & vbCrLf
sConsulta = sConsulta & "        ELSE --Est� en otro estado" & vbCrLf
sConsulta = sConsulta & "          IF @USU = @PETICIONARIO --la est� viendo el peticionario" & vbCrLf
sConsulta = sConsulta & "            SET @SQL = @SQL + '    INNER JOIN INSTANCIA WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                           ON C.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                   INNER JOIN COPIA_CONF_CUMPLIMENTACION CC WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                           ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.COPIA_PASO IS NULL  " & vbCrLf
sConsulta = sConsulta & "                                          AND CC.SOLIC_PROVE = 1" & vbCrLf
sConsulta = sConsulta & "                                          AND C.INSTANCIA=INSTANCIA.ID  '" & vbCrLf
sConsulta = sConsulta & "          ELSE --la est� viendo un aprobador por lo que se coje la conf-cumplimentacion que ten�a cuando la aprob�" & vbCrLf
sConsulta = sConsulta & "                   SET @SQL = @SQL + '  INNER JOIN INSTANCIA WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                         ON C.INSTANCIA = INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                  INNER JOIN COPIA_SUBPASO WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                         ON ISNULL(INSTANCIA.APROB, (SELECT TOP 1 APROB FROM INSTANCIA_EST WITH(NOLOCK) WHERE INSTANCIA = @ID AND PER = @USU))=COPIA_SUBPASO.ID" & vbCrLf
sConsulta = sConsulta & "                                 INNER JOIN COPIA_CONF_CUMPLIMENTACION CC WITH (NOLOCK)   " & vbCrLf
sConsulta = sConsulta & "                                         ON CD.ID = CC.COPIA_CAMPO " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.INSTANCIA=INSTANCIA.ID " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.COPIA_PASO=COPIA_SUBPASO.PASO " & vbCrLf
sConsulta = sConsulta & "                                        AND CC.SOLIC_PROVE IS NULL'    " & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + '  WHERE C.INSTANCIA = @ID AND C.NUM_VERSION=@VERSION AND CD.ES_SUBCAMPO=0' " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF (@ESTADO=0 OR @ESTADO=2) AND @USU IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "  IF @HAYCONFCUMP=1" & vbCrLf
sConsulta = sConsulta & "    SET @SQL = @SQL +  ' ORDER BY G.ID, CC.ORDEN'" & vbCrLf
sConsulta = sConsulta & "  ELSE" & vbCrLf
sConsulta = sConsulta & "    SET @SQL = @SQL +  ' ORDER BY G.ID, CD.ORDEN'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  SET @SQL = @SQL +  ' ORDER BY G.ID, CD.ORDEN'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID INT, @VERSION INT = NULL,@USU VARCHAR(50), @PROVE VARCHAR(50)', @ID=@ID, @VERSION=@VERSION,@USU=@USU,@PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Obtiene los valores de listas:" & vbCrLf
sConsulta = sConsulta & "IF @IDI IS NULL" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = N'SELECT L.ORDEN, C.GRUPO, L.CAMPO_DEF, L.VALOR_NUM, L.VALOR_TEXT_SPA,L.VALOR_TEXT_ENG,L.VALOR_TEXT_GER, L.VALOR_FEC' " & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = N'SELECT L.ORDEN,C.GRUPO ,  L.CAMPO_DEF,L.VALOR_NUM, L.VALOR_TEXT_' + @IDI + ', L.VALOR_FEC'" & vbCrLf
sConsulta = sConsulta & "SET @SQL= @SQL + ' FROM COPIA_CAMPO_VALOR_LISTA L WITH (NOLOCK)   INNER JOIN COPIA_CAMPO_DEF C WITH (NOLOCK)  ON L.CAMPO_DEF = C.ID INNER JOIN COPIA_GRUPO G WITH (NOLOCK)   ON C.GRUPO=G.ID WHERE G.INSTANCIA = @ID  AND C.ES_SUBCAMPO=0 ORDER BY C.GRUPO, L.CAMPO_DEF, L.ORDEN'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID INT', @ID=@ID" & vbCrLf
sConsulta = sConsulta & "--Obtiene los adjuntos" & vbCrLf
sConsulta = sConsulta & "SELECT CD.GRUPO, A.ID, A.CAMPO, A.NOM, A.COMENT, A.IDIOMA, A.DATASIZE, A.ADJUN FROM COPIA_CAMPO_ADJUN A WITH (NOLOCK)   INNER JOIN COPIA_CAMPO C WITH (NOLOCK)   ON A.CAMPO = C.ID INNER JOIN COPIA_CAMPO_DEF CD WITH (NOLOCK)   ON C.COPIA_CAMPO_DEF=CD.ID INNER JOIN COPIA_GRUPO G WITH (NOLOCK)   ON CD.GRUPO=G.ID " & vbCrLf
sConsulta = sConsulta & "WHERE C.INSTANCIA=@ID AND C.NUM_VERSION=@VERSION AND CD.ES_SUBCAMPO=0 ORDER BY CD.GRUPO,A.CAMPO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Obtiene la Hora " & vbCrLf
sConsulta = sConsulta & "SELECT  DATEDIFF(MINUTE,GETUTCDATE(),GETDATE()) AS DIFERENCIA" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub


Public Function CodigoDeActualizacion31800_07_0850A_31800_07_0851() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim iRespuesta As Integer


On Error GoTo Error
sConsulta = "BEGIN TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = True
ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

frmProgreso.lblDetalle = "Actualiza Storeds"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
frmProgreso.ProgressBar1.Refresh
V_31800_7_Storeds_051

sConsulta = "UPDATE VERSION SET NUM ='3.00.08.51'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103), PRODVER='31800.7'"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "COMMIT TRANSACTION"
ExecuteSQL gRDOCon, sConsulta

bTransaccionEnCurso = False

CodigoDeActualizacion31800_07_0850A_31800_07_0851 = True
Exit Function

Error:
    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If

    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    CodigoDeActualizacion31800_07_0850A_31800_07_0851 = False
End Function


Private Sub V_31800_7_Storeds_051()

Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_GET_ORDENES_FAVORITOS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_GET_ORDENES_FAVORITOS]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE  PROCEDURE FSEP_GET_ORDENES_FAVORITOS @USU AS VARCHAR(30) , @MON AS VARCHAR(30) , @IDIOMA AS VARCHAR(20), @CARGARLINEAS AS TINYINT, @PEDIDO AS INTEGER, @PROVE AS VARCHAR(30)" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PARAM AS NVARCHAR(1000)" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING='SELECT OE.ID,OE.USU,OE.DEN,OE.ANYO,OE.PROVE PROVECOD,P.DEN PROVEDEN, OE.MON, MON.EQUIV, OE.FECHA,OE.IMPORTE,OE.OBS,OE.REFERENCIA, OE.EMPRESA, E.DEN DENEMPRESA, OE.COD_PROVE_ERP, OE.RECEPTOR ,PER.NOM + '' '' + PER.APE NOMRECEPTOR, T3.ADJUNTOS" & vbCrLf
sConsulta = sConsulta & "FROM FAVORITOS_ORDEN_ENTREGA OE WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PROVE P WITH(NOLOCK) ON OE.PROVE = P.COD" & vbCrLf
sConsulta = sConsulta & "INNER JOIN MON WITH(NOLOCK) ON OE.MON = MON.COD" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN EMP E WITH(NOLOCK) ON OE.EMPRESA = E.ID" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PER WITH(NOLOCK)  ON OE.RECEPTOR = PER.COD" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN (SELECT COUNT(*) AS ADJUNTOS,ORDEN FROM FAVORITOS_ORDEN_ENTREGA_ADJUN WITH(NOLOCK) GROUP BY FAVORITOS_ORDEN_ENTREGA_ADJUN.ORDEN) T3 ON OE.ID= T3.ORDEN" & vbCrLf
sConsulta = sConsulta & "WHERE USU=@USU'" & vbCrLf
sConsulta = sConsulta & "IF @MON IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQLSTRING=@SQLSTRING +' AND OE.MON=@MON'" & vbCrLf
sConsulta = sConsulta & "IF @PEDIDO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQLSTRING=@SQLSTRING + ' AND OE.ID=@PEDIDO'" & vbCrLf
sConsulta = sConsulta & "IF @PROVE IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "   SET @SQLSTRING=@SQLSTRING + ' AND OE.PROVE=@PROVE'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @PARAM = '@MON VARCHAR(30) , @PROVE VARCHAR(30), @PEDIDO INTEGER , @USU VARCHAR(30)'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXECUTE sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "@MON=@MON, " & vbCrLf
sConsulta = sConsulta & "@PROVE=@PROVE, " & vbCrLf
sConsulta = sConsulta & "@PEDIDO=@PEDIDO," & vbCrLf
sConsulta = sConsulta & "@USU=@USU" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CARGARLINEAS=1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET CONCAT_NULL_YIELDS_NULL OFF" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @SQLSTRING=" & vbCrLf
sConsulta = sConsulta & "'SELECT DISTINCT OE.ID as ORDEN," & vbCrLf
sConsulta = sConsulta & "LP.ID LINEAID," & vbCrLf
sConsulta = sConsulta & "LP.USU," & vbCrLf
sConsulta = sConsulta & "LP.ART_INT CODART," & vbCrLf
sConsulta = sConsulta & "I.GMN1 + ART4.GMN1 GMN1," & vbCrLf
sConsulta = sConsulta & "I.GMN2 + ART4.GMN2 GMN2," & vbCrLf
sConsulta = sConsulta & "I.GMN3 + ART4.GMN3 GMN3," & vbCrLf
sConsulta = sConsulta & "I.GMN4 + ART4.GMN4 GMN4," & vbCrLf
sConsulta = sConsulta & "LP.PROVE," & vbCrLf
sConsulta = sConsulta & "I.DESCR + CL.DEN DENART," & vbCrLf
sConsulta = sConsulta & "PA.COD_EXT ART_EXT," & vbCrLf
sConsulta = sConsulta & "LP.DEST DESTINO," & vbCrLf
sConsulta = sConsulta & "LP.UP UNIDAD," & vbCrLf
sConsulta = sConsulta & "LP.PREC_UP PRECIOUNITARIO," & vbCrLf
sConsulta = sConsulta & "LP.CANT_PED CANTIDAD," & vbCrLf
sConsulta = sConsulta & "IA.CANT_ADJ * LP.FC AS CANTADJUDICADA," & vbCrLf
sConsulta = sConsulta & "LP.PREC_UP * LP.CANT_PED IMPORTE," & vbCrLf
sConsulta = sConsulta & "LP.CAT1," & vbCrLf
sConsulta = sConsulta & "LP.CAT2," & vbCrLf
sConsulta = sConsulta & "LP.CAT3," & vbCrLf
sConsulta = sConsulta & "LP.CAT4," & vbCrLf
sConsulta = sConsulta & "LP.CAT5," & vbCrLf
sConsulta = sConsulta & "LP.OBS, " & vbCrLf
sConsulta = sConsulta & "case when c5.cod is null then " & vbCrLf
sConsulta = sConsulta & "   case when c4.cod is null then " & vbCrLf
sConsulta = sConsulta & "       case when c3.cod is null then " & vbCrLf
sConsulta = sConsulta & "           case when c2.cod is null then " & vbCrLf
sConsulta = sConsulta & "               c1.cod " & vbCrLf
sConsulta = sConsulta & "           else " & vbCrLf
sConsulta = sConsulta & "               c2.cod " & vbCrLf
sConsulta = sConsulta & "           end " & vbCrLf
sConsulta = sConsulta & "       else " & vbCrLf
sConsulta = sConsulta & "           c3.cod " & vbCrLf
sConsulta = sConsulta & "       end " & vbCrLf
sConsulta = sConsulta & "   else " & vbCrLf
sConsulta = sConsulta & "       c4.cod " & vbCrLf
sConsulta = sConsulta & "   end " & vbCrLf
sConsulta = sConsulta & "else " & vbCrLf
sConsulta = sConsulta & "   c5.cod" & vbCrLf
sConsulta = sConsulta & "end CATEGORIA," & vbCrLf
sConsulta = sConsulta & "case when c5.cod is null then " & vbCrLf
sConsulta = sConsulta & "   case when c4.cod is null then " & vbCrLf
sConsulta = sConsulta & "       case when c3.cod is null then " & vbCrLf
sConsulta = sConsulta & "           case when c2.cod is null then " & vbCrLf
sConsulta = sConsulta & "               ''('' + c1.cod + '') '' + c1.den " & vbCrLf
sConsulta = sConsulta & "           else " & vbCrLf
sConsulta = sConsulta & "               ''('' + c1.cod + '') '' + c1.den + '' - ('' + c2.cod + '') '' + c2.den " & vbCrLf
sConsulta = sConsulta & "           end " & vbCrLf
sConsulta = sConsulta & "       else " & vbCrLf
sConsulta = sConsulta & "           ''('' + c1.cod + '') '' + c1.den + '' - ('' + c2.cod + '') '' + c2.den + '' - ('' + c3.cod + '') '' + c3.den " & vbCrLf
sConsulta = sConsulta & "       end " & vbCrLf
sConsulta = sConsulta & "   else " & vbCrLf
sConsulta = sConsulta & "       ''('' + c1.cod + '') '' + c1.den + '' - ('' + c2.cod + '') '' + c2.den + '' - ('' + c3.cod + '') '' + c3.den + '' - ('' + c4.cod + '') '' + c4.den " & vbCrLf
sConsulta = sConsulta & "   end " & vbCrLf
sConsulta = sConsulta & "else " & vbCrLf
sConsulta = sConsulta & "   ''('' + c1.cod + '') '' + c1.den + '' - ('' + c2.cod + '') '' + c2.den + '' - ('' + c3.cod + '') '' + c3.den + '' - ('' + c4.cod + '') '' + c4.den + '' - ('' + c5.cod + '') '' + c5.den " & vbCrLf
sConsulta = sConsulta & "end CATEGORIARAMA," & vbCrLf
sConsulta = sConsulta & "c1.cod C1," & vbCrLf
sConsulta = sConsulta & "c2.cod C2," & vbCrLf
sConsulta = sConsulta & "c3.cod C3," & vbCrLf
sConsulta = sConsulta & "c4.cod C4," & vbCrLf
sConsulta = sConsulta & "c5.cod C5," & vbCrLf
sConsulta = sConsulta & "C1.DEN CAT1DEN," & vbCrLf
sConsulta = sConsulta & "C2.DEN CAT2DEN," & vbCrLf
sConsulta = sConsulta & "C3.DEN CAT3DEN," & vbCrLf
sConsulta = sConsulta & "C4.DEN CAT4DEN," & vbCrLf
sConsulta = sConsulta & "C5.DEN CAT5DEN," & vbCrLf
sConsulta = sConsulta & "I.ANYO ANYOPROCE," & vbCrLf
sConsulta = sConsulta & "I.GMN1_PROCE," & vbCrLf
sConsulta = sConsulta & "I.PROCE CODPROCE," & vbCrLf
sConsulta = sConsulta & "I.ID ITEM," & vbCrLf
sConsulta = sConsulta & "LPA.ADJUNTOS," & vbCrLf
sConsulta = sConsulta & "LP.VALOR1," & vbCrLf
sConsulta = sConsulta & "LP.VALOR2," & vbCrLf
sConsulta = sConsulta & "CC1.DEN AS DENCAMPO1," & vbCrLf
sConsulta = sConsulta & "CC2.DEN AS DENCAMPO2," & vbCrLf
sConsulta = sConsulta & "CC1.OBL AS OBL1," & vbCrLf
sConsulta = sConsulta & "CC2.OBL AS OBL2," & vbCrLf
sConsulta = sConsulta & "CC1.ID AS CAMPO1," & vbCrLf
sConsulta = sConsulta & "CC2.ID AS CAMPO2," & vbCrLf
sConsulta = sConsulta & "PROCE.DEN AS DENPROCE," & vbCrLf
sConsulta = sConsulta & "LP.OBSADJUN," & vbCrLf
sConsulta = sConsulta & "LP.LINEA," & vbCrLf
sConsulta = sConsulta & "ISNULL(CLM.CANT_MIN,CL.CANT_MIN_DEF) CANT_MIN," & vbCrLf
sConsulta = sConsulta & "ISNULL(LP.PREC_UP,CL.PRECIO_UC*CL.FC_DEF) PRECIO_UP," & vbCrLf
sConsulta = sConsulta & "LP.FC, " & vbCrLf
sConsulta = sConsulta & "case when c5.cod is null then " & vbCrLf
sConsulta = sConsulta & "   case when c4.cod is null then " & vbCrLf
sConsulta = sConsulta & "       case when c3.cod is null then " & vbCrLf
sConsulta = sConsulta & "           case when c2.cod is null then " & vbCrLf
sConsulta = sConsulta & "               c1.MODIFPREC " & vbCrLf
sConsulta = sConsulta & "           else " & vbCrLf
sConsulta = sConsulta & "               c2.MODIFPREC " & vbCrLf
sConsulta = sConsulta & "           end " & vbCrLf
sConsulta = sConsulta & "       else " & vbCrLf
sConsulta = sConsulta & "           c3.MODIFPREC " & vbCrLf
sConsulta = sConsulta & "       end " & vbCrLf
sConsulta = sConsulta & "   else " & vbCrLf
sConsulta = sConsulta & "       c4.MODIFPREC " & vbCrLf
sConsulta = sConsulta & "   end " & vbCrLf
sConsulta = sConsulta & "else " & vbCrLf
sConsulta = sConsulta & "   c5.MODIFPREC " & vbCrLf
sConsulta = sConsulta & "end MODIF_PREC, " & vbCrLf
sConsulta = sConsulta & "COALESCE(C1.PRES0, C2.PRES0, C3.PRES0, C4.PRES0, C5.PRES0) PRES0, COALESCE(C1.PRES1, C2.PRES1, C3.PRES1, C4.PRES1, C5.PRES1) PRES1 " & vbCrLf
sConsulta = sConsulta & ", COALESCE(C1.PRES2, C2.PRES2, C3.PRES2, C4.PRES2, C5.PRES2) PRES2, COALESCE(C1.PRES3, C2.PRES3, C3.PRES3, C4.PRES3, C5.PRES3) PRES3 " & vbCrLf
sConsulta = sConsulta & ", COALESCE(C1.PRES4, C2.PRES4, C3.PRES4, C4.PRES4, C5.PRES4) PRES4 , UNI.DEN_' + @IDIOMA + ' UNIDEN" & vbCrLf
sConsulta = sConsulta & "FROM FAVORITOS_LINEAS_PEDIDO LP " & vbCrLf
sConsulta = sConsulta & "LEFT JOIN ITEM I ON LP.ANYO = I.ANYO AND LP.GMN1=I.GMN1_PROCE AND LP.PROCE=I.PROCE AND LP.ITEM=I.ID " & vbCrLf
sConsulta = sConsulta & "LEFT JOIN ART4 ON ART4.COD=LP.ART_INT " & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PROVE_ART4 PA ON LP.PROVE = PA.PROVE AND LP.ART_INT = PA.ART " & vbCrLf
sConsulta = sConsulta & "LEFT JOIN ITEM_ADJ IA ON LP.ITEM = IA.ITEM AND LP.ANYO = IA.ANYO AND LP.GMN1 = IA.GMN1 AND LP.PROCE= IA.PROCE AND LP.PROVE= IA.PROVE " & vbCrLf
sConsulta = sConsulta & "LEFT JOIN CATN1 C1 ON LP.CAT1 = C1.ID " & vbCrLf
sConsulta = sConsulta & "LEFT JOIN CATN2 C2 ON LP.CAT2 = C2.ID " & vbCrLf
sConsulta = sConsulta & "LEFT JOIN CATN3 C3 ON LP.CAT3 = C3.ID " & vbCrLf
sConsulta = sConsulta & "LEFT JOIN CATN4 C4 ON LP.CAT4 = C4.ID " & vbCrLf
sConsulta = sConsulta & "LEFT JOIN CATN5 C5 ON LP.CAT5 = C5.ID " & vbCrLf
sConsulta = sConsulta & "LEFT JOIN " & vbCrLf
sConsulta = sConsulta & "(SELECT COUNT(*) AS ADJUNTOS,LINEA  FROM FAVORITOS_LINEAS_PEDIDO_ADJUN GROUP BY FAVORITOS_LINEAS_PEDIDO_ADJUN.LINEA) LPA ON LP.ID= LPA.LINEA " & vbCrLf
sConsulta = sConsulta & "LEFT JOIN CATALOG_CAMPOS AS CC1 ON CC1.ID=LP.CAMPO1 AND CC1.IDIOMA=@IDIOMA " & vbCrLf
sConsulta = sConsulta & "LEFT JOIN CATALOG_CAMPOS AS CC2 ON CC2.ID=LP.CAMPO2 AND CC2.IDIOMA=@IDIOMA " & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PROCE ON LP.ANYO=PROCE.ANYO AND LP.GMN1=PROCE.GMN1 AND LP.PROCE=PROCE.COD " & vbCrLf
sConsulta = sConsulta & "LEFT JOIN CATALOG_LIN CL ON LP.LINEA = CL.ID " & vbCrLf
sConsulta = sConsulta & "LEFT JOIN CAT_LIN_MIN CLM ON LP.LINEA = CLM.LINEA AND LP.UP = CLM.UP " & vbCrLf
sConsulta = sConsulta & "INNER JOIN FAVORITOS_ORDEN_ENTREGA OE ON OE.ID=LP.ORDEN " & vbCrLf
sConsulta = sConsulta & "INNER JOIN UNI ON LP.UP = UNI.COD" & vbCrLf
sConsulta = sConsulta & "WHERE OE.USU=@USU'" & vbCrLf
sConsulta = sConsulta & "   IF @MON IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQLSTRING=@SQLSTRING +' AND OE.MON=@MON'" & vbCrLf
sConsulta = sConsulta & "   IF @PEDIDO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQLSTRING=@SQLSTRING + ' AND OE.ID=@PEDIDO'" & vbCrLf
sConsulta = sConsulta & "   IF @PROVE IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "   SET @SQLSTRING=@SQLSTRING + ' AND OE.PROVE=@PROVE'" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   SET @PARAM = '@MON VARCHAR(30) , @PROVE VARCHAR(30)," & vbCrLf
sConsulta = sConsulta & "   @PEDIDO INTEGER , @USU VARCHAR(30) , @IDIOMA VARCHAR(30)'" & vbCrLf
sConsulta = sConsulta & "   EXECUTE sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "   @MON=@MON, " & vbCrLf
sConsulta = sConsulta & "   @PROVE=@PROVE, " & vbCrLf
sConsulta = sConsulta & "   @PEDIDO=@PEDIDO," & vbCrLf
sConsulta = sConsulta & "   @USU=@USU," & vbCrLf
sConsulta = sConsulta & "   @IDIOMA=@IDIOMA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET XACT_ABORT OFF" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_GET_TODAS_ORDENES]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_GET_TODAS_ORDENES]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSEP_GET_TODAS_ORDENES  @IDIOMA VARCHAR(20)='SPA', @APROVISIONADOR varchar(50) = null AS " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT CONVERT(VARCHAR,OE.EST) + LEFT('00000000',8-LEN(CONVERT(VARCHAR,OE.ID))) + CONVERT(VARCHAR,OE.ID), " & vbCrLf
sConsulta = sConsulta & "   OE.ID, OE.ANYO, PE.NUM NUMPEDIDO, OE.NUM NUMORDEN, OE.PROVE PROVECOD, PROV.DEN PROVEDEN, PROV.NIF," & vbCrLf
sConsulta = sConsulta & "   OE.NUMEXT, OE.FECHA, " & vbCrLf
sConsulta = sConsulta & "   CASE WHEN OE.EST>19 THEN POWER(2,OE.EST-13) ELSE POWER(2,OE.EST) END AS EST, " & vbCrLf
sConsulta = sConsulta & "   OE.IMPORTE, OE.CAMBIO, OE.MON, " & vbCrLf
sConsulta = sConsulta & "   CASE @IDIOMA WHEN 'SPA' THEN MON.DEN_SPA WHEN 'ENG' THEN MON.DEN_ENG WHEN 'GER' THEN MON.DEN_GER END AS MONDEN," & vbCrLf
sConsulta = sConsulta & "   APROV.APE APROV_APE, APROV.NOM APROV_NOM, OE.OBS, OES.COMENT, OES.FECHA FECEST, OE.IMPORTE, OE.PEDIDO, OE.TIPO, OE.INCORRECTA, PE.PER APROV, " & vbCrLf
sConsulta = sConsulta & "   OE.NUM_PED_ERP AS REFERENCIA, ADJ.ADJUNTOS, CP.COMENT COMENTPROVE, CP.EST ESTCOMENT, OE.EMPRESA, E.DEN DENEMPRESA, E.NIF NIFEMPRESA, OE.COD_PROVE_ERP, " & vbCrLf
sConsulta = sConsulta & "   OE.RECEPTOR , RECEP.NOM RECEP_NOM, RECEP.APE RECEP_APE, RECEP.TFNO RECEP_TFNO, RECEP.EMAIL RECEP_EMAIL, ISNULL(RECEP.UON3,ISNULL(RECEP.UON2,RECEP.UON1)) RECEP_UON, ISNULL(UON3.DEN,ISNULL(UON2.DEN,UON1.DEN)) RECEP_DENUON," & vbCrLf
sConsulta = sConsulta & "   PE.TIPO, PR.FECHA FECHARECEP, OE.PAG, " & vbCrLf
sConsulta = sConsulta & "   CASE @IDIOMA WHEN 'SPA' THEN PAG.DEN_SPA WHEN 'ENG' THEN PAG.DEN_ENG WHEN 'GER' THEN PAG.DEN_GER END AS PAGDEN" & vbCrLf
sConsulta = sConsulta & "FROM ORDEN_ENTREGA OE WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PROVE PROV WITH(NOLOCK) ON OE.PROVE = PROV.COD" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PEDIDO PE WITH(NOLOCK) ON OE.PEDIDO = PE.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN MON WITH(NOLOCK) ON  OE.MON = MON.COD" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN EMP E WITH(NOLOCK) ON OE.EMPRESA = E.ID" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PER RECEP WITH(NOLOCK) ON OE.RECEPTOR = RECEP.COD" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN UON1 WITH(NOLOCK) ON RECEP.UON1 = UON1.COD " & vbCrLf
sConsulta = sConsulta & "LEFT JOIN UON2 WITH(NOLOCK) ON RECEP.UON1 = UON2.UON1 AND RECEP.UON2 = UON2.COD " & vbCrLf
sConsulta = sConsulta & "LEFT JOIN UON3 WITH(NOLOCK) ON RECEP.UON1 = UON3.UON1 AND RECEP.UON2 = UON3.UON2 AND RECEP.UON3 = UON3.COD" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN (SELECT OE.ID, OE.PEDIDO, OE.ORDEN, OE.EST, OE.COMENT " & vbCrLf
sConsulta = sConsulta & "           FROM ORDEN_EST OE WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN (SELECT PEDIDO, ORDEN, MAX(ID) ID" & vbCrLf
sConsulta = sConsulta & "                       FROM ORDEN_EST WITH(NOLOCK) WHERE EST IN (3, 21) " & vbCrLf
sConsulta = sConsulta & "                       GROUP BY PEDIDO, ORDEN ) MO ON MO.ID = OE.ID) CP" & vbCrLf
sConsulta = sConsulta & "               ON OE.ID = CP.ORDEN" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN (SELECT DISTINCT ORDEN , S.APROB_TIPO1 PER" & vbCrLf
sConsulta = sConsulta & "           FROM LINEAS_PEDIDO LP WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN SEGURIDAD S WITH(NOLOCK) ON LP.SEGURIDAD = S.ID  AND LP.NIVEL_APROB = 0) APROB" & vbCrLf
sConsulta = sConsulta & "            ON OE.ID = APROB.ORDEN" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN (SELECT DISTINCT ORDEN , AL.PER PER" & vbCrLf
sConsulta = sConsulta & "           FROM LINEAS_PEDIDO LP WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN APROB_LIM AL WITH(NOLOCK) ON LP.APROB_LIM = AL.ID) APROB2" & vbCrLf
sConsulta = sConsulta & "            ON OE.ID = APROB2.ORDEN" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PER APROV WITH(NOLOCK) ON PE.PER = APROV.COD" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN ORDEN_EST OES WITH(NOLOCK) ON OE.ORDEN_EST = OES.ID" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN (SELECT COUNT(*) AS ADJUNTOS,ORDEN  " & vbCrLf
sConsulta = sConsulta & "           FROM ORDEN_ENTREGA_ADJUN WITH(NOLOCK) GROUP BY ORDEN_ENTREGA_ADJUN.ORDEN) ADJ ON OE.ID= ADJ.ORDEN " & vbCrLf
sConsulta = sConsulta & "LEFT JOIN (PEDIDO_RECEP PR WITH(NOLOCK) INNER JOIN (SELECT PEDIDO, ORDEN, MAX(ID) ID FROM PEDIDO_RECEP WITH(NOLOCK) GROUP BY PEDIDO,ORDEN)  PR2 ON PR.ID=PR2.ID )" & vbCrLf
sConsulta = sConsulta & "             ON PR.ORDEN= OE.ID" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PAG WITH(NOLOCK) ON PAG.COD= OE.PAG" & vbCrLf
sConsulta = sConsulta & "WHERE (@APROVISIONADOR IS NULL OR PE.PER=@APROVISIONADOR)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT LP.ID, LP.PEDIDO, LP.ORDEN, LP.CAT1, LP.CAT2, LP.CAT3, LP.CAT4, LP.CAT5, LP.ART_INT" & vbCrLf
sConsulta = sConsulta & "   , I.ART, I.DESCR, LP.DESCR_LIBRE, PA.COD_EXT, LP.OBS, LP.OBSADJUN" & vbCrLf
sConsulta = sConsulta & "FROM ORDEN_ENTREGA OE WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PEDIDO PE WITH(NOLOCK) ON OE.PEDIDO = PE.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN LINEAS_PEDIDO LP WITH (NOLOCK) ON LP.ORDEN=OE.ID" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN (SELECT DISTINCT ORDEN , S.APROB_TIPO1 PER" & vbCrLf
sConsulta = sConsulta & "           FROM LINEAS_PEDIDO LP WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN SEGURIDAD S WITH(NOLOCK) ON LP.SEGURIDAD = S.ID  AND LP.NIVEL_APROB = 0) APROB" & vbCrLf
sConsulta = sConsulta & "            ON OE.ID = APROB.ORDEN" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN (SELECT DISTINCT ORDEN , AL.PER PER" & vbCrLf
sConsulta = sConsulta & "           FROM LINEAS_PEDIDO LP WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN APROB_LIM AL WITH(NOLOCK) ON LP.APROB_LIM = AL.ID) APROB2" & vbCrLf
sConsulta = sConsulta & "            ON OE.ID = APROB2.ORDEN" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN ITEM I WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   ON I.ANYO = LP.ANYO AND I.GMN1_PROCE=LP.GMN1 AND I.PROCE=LP.PROCE AND I.ID=LP.ITEM" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN ART4 WITH(NOLOCK) ON ART4.COD=LP.ART_INT" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PROVE_ART4 PA WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   ON PA.PROVE=LP.PROVE AND PA.ART = CASE WHEN LP.ART_INT IS NULL THEN I.ART ELSE LP.ART_INT END" & vbCrLf
sConsulta = sConsulta & "WHERE (@APROVISIONADOR IS NULL OR PE.PER=@APROVISIONADOR)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_DEVOLVER_LINEAS_DE_ORDEN]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_DEVOLVER_LINEAS_DE_ORDEN]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSEP_DEVOLVER_LINEAS_DE_ORDEN(@APROBADOR VARCHAR(50)=null, @ORDENID INTEGER = 0, @SOLICIT int = null ,@IDI varchar(20) , @MOSTRARCINTERNO tinyint = null)  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(1500)" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if @APROBADOR IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "set NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE #TEMP1 ( LINEAID INT, CODART VARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, " & vbCrLf
sConsulta = sConsulta & "               DENART VARCHAR(500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, " & vbCrLf
sConsulta = sConsulta & "     ARTICULO VARCHAR(500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, " & vbCrLf
sConsulta = sConsulta & "               ART_EXT VARCHAR(120) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,  " & vbCrLf
sConsulta = sConsulta & "               GMN1 VARCHAR(10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, GMN2 VARCHAR(10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, GMN3 VARCHAR(10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, " & vbCrLf
sConsulta = sConsulta & "               GMN4 VARCHAR(10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, PROVE VARCHAR(20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, " & vbCrLf
sConsulta = sConsulta & "    DESTINO VARCHAR(10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,  " & vbCrLf
sConsulta = sConsulta & "              UNIDAD VARCHAR(10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, PRECIOUNITARIO FLOAT, CANTIDAD FLOAT, CANT_REC FLOAT, " & vbCrLf
sConsulta = sConsulta & "              CANTADJUDICADA FLOAT,  CANTCONSUMIDA FLOAT, IMPORTE FLOAT, APROBADA INT, FECHA DATETIME,  " & vbCrLf
sConsulta = sConsulta & "    GESTIONABLE VARCHAR(5),    " & vbCrLf
sConsulta = sConsulta & "              CAT1 INT, CAT2 INT, CAT3 INT, CAT4 INT, CAT5 INT,  OBS VARCHAR(2020) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, CATEGORIA VARCHAR(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, " & vbCrLf
sConsulta = sConsulta & "              CATEGORIARAMA VARCHAR(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,  " & vbCrLf
sConsulta = sConsulta & "    ANYOPROCE INT, GMN1_PROCE VARCHAR(3), CODPROCE INT,  COMENT VARCHAR(280) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, ADJUNTOS INT, " & vbCrLf
sConsulta = sConsulta & "              VALOR1 VARCHAR(120) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, VALOR2 VARCHAR(120) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, " & vbCrLf
sConsulta = sConsulta & "              DENCAMPO1 VARCHAR(120) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, DENCAMPO2 VARCHAR(120) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, " & vbCrLf
sConsulta = sConsulta & "              DENPROCE VARCHAR(120), OBSADJUN VARCHAR(2020) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, " & vbCrLf
sConsulta = sConsulta & "    FECENTREGA DATETIME,  PEDIDOABIERTO INT, ORDEN INT)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO #TEMP1 SELECT DISTINCT LP.ID AS LINEAID," & vbCrLf
sConsulta = sConsulta & "                LP.ART_INT CODART," & vbCrLf
sConsulta = sConsulta & "                ITEM.DESCR  + LP.DESCR_LIBRE DENART," & vbCrLf
sConsulta = sConsulta & "                LP.ART_INT + ' ' +  ITEM.DESCR  + LP.DESCR_LIBRE  AS ARTICULO," & vbCrLf
sConsulta = sConsulta & "                PA.COD_EXT ART_EXT," & vbCrLf
sConsulta = sConsulta & "                ITEM.GMN1 + ART4.GMN1 GMN1," & vbCrLf
sConsulta = sConsulta & "                ITEM.GMN2 + ART4.GMN2 GMN2," & vbCrLf
sConsulta = sConsulta & "                ITEM.GMN3 + ART4.GMN3 GMN3," & vbCrLf
sConsulta = sConsulta & "                ITEM.GMN4 + ART4.GMN4 GMN4," & vbCrLf
sConsulta = sConsulta & "                LP.PROVE," & vbCrLf
sConsulta = sConsulta & "                LP.DEST AS DESTINO," & vbCrLf
sConsulta = sConsulta & "                LP.UP AS UNIDAD," & vbCrLf
sConsulta = sConsulta & "                LP.PREC_UP AS PRECIOUNITARIO," & vbCrLf
sConsulta = sConsulta & "                LP.CANT_PED AS CANTIDAD," & vbCrLf
sConsulta = sConsulta & "                LP.CANT_REC AS CANTIDAD_REC," & vbCrLf
sConsulta = sConsulta & "                ITEM_ADJ.CANT_ADJ / LP.FC AS CANTADJUDICADA," & vbCrLf
sConsulta = sConsulta & "                ITEM_ADJ.CANT_PED / LP.FC AS CANTCONSUMIDA," & vbCrLf
sConsulta = sConsulta & "                LP.PREC_UP * LP.CANT_PED IMPORTE," & vbCrLf
sConsulta = sConsulta & "                LP.EST AS APROBADA," & vbCrLf
sConsulta = sConsulta & "                LP.FECEMISION AS FECHA," & vbCrLf
sConsulta = sConsulta & "                GESTIONABLE=CASE WHEN APROB_LIM.PER= @APROBADOR   THEN 1 WHEN (APROB_LIM.PER IS NULL AND SEGURIDAD.APROB_TIPO1=@APROBADOR ) THEN 1  ELSE 0 END," & vbCrLf
sConsulta = sConsulta & "                LP.CAT1," & vbCrLf
sConsulta = sConsulta & "                LP.CAT2," & vbCrLf
sConsulta = sConsulta & "                LP.CAT3," & vbCrLf
sConsulta = sConsulta & "                LP.CAT4," & vbCrLf
sConsulta = sConsulta & "                LP.CAT5," & vbCrLf
sConsulta = sConsulta & "                LP.OBS," & vbCrLf
sConsulta = sConsulta & "                case when c5.cod is null then " & vbCrLf
sConsulta = sConsulta & "                        case when c4.cod is null then " & vbCrLf
sConsulta = sConsulta & "                             case when c3.cod is null then" & vbCrLf
sConsulta = sConsulta & "                                  case when c2.cod is null then" & vbCrLf
sConsulta = sConsulta & "                                       c1.cod" & vbCrLf
sConsulta = sConsulta & "                                  else" & vbCrLf
sConsulta = sConsulta & "                                  c2.cod" & vbCrLf
sConsulta = sConsulta & "                                  end" & vbCrLf
sConsulta = sConsulta & "                             else" & vbCrLf
sConsulta = sConsulta & "                             c3.cod" & vbCrLf
sConsulta = sConsulta & "                             end" & vbCrLf
sConsulta = sConsulta & "                        else" & vbCrLf
sConsulta = sConsulta & "                        c4.cod" & vbCrLf
sConsulta = sConsulta & "                        end" & vbCrLf
sConsulta = sConsulta & "                    else" & vbCrLf
sConsulta = sConsulta & "                    c5.cod" & vbCrLf
sConsulta = sConsulta & "                    end CATEGORIA," & vbCrLf
sConsulta = sConsulta & "                case when c5.cod is null then " & vbCrLf
sConsulta = sConsulta & "                        case when c4.cod is null then " & vbCrLf
sConsulta = sConsulta & "                             case when c3.cod is null then" & vbCrLf
sConsulta = sConsulta & "                                  case when c2.cod is null then" & vbCrLf
sConsulta = sConsulta & "                                       '(' + c1.cod + ') ' + c1.den" & vbCrLf
sConsulta = sConsulta & "                                  else" & vbCrLf
sConsulta = sConsulta & "                                  '(' + c1.cod + ') ' + c1.den + ' - (' + c2.cod + ') ' + c2.den" & vbCrLf
sConsulta = sConsulta & "                                  end" & vbCrLf
sConsulta = sConsulta & "                             else" & vbCrLf
sConsulta = sConsulta & "                             '(' + c1.cod + ') ' + c1.den + ' - (' + c2.cod + ') ' + c2.den + ' - (' + c3.cod + ') ' + c3.den" & vbCrLf
sConsulta = sConsulta & "                             end" & vbCrLf
sConsulta = sConsulta & "                        else" & vbCrLf
sConsulta = sConsulta & "                        '(' + c1.cod + ') ' + c1.den + ' - (' + c2.cod + ') ' + c2.den + ' - (' + c3.cod + ') ' + c3.den + ' - (' + c4.cod + ') ' + c4.den" & vbCrLf
sConsulta = sConsulta & "                        end" & vbCrLf
sConsulta = sConsulta & "                    else" & vbCrLf
sConsulta = sConsulta & "                    '(' + c1.cod + ') ' + c1.den + ' - (' + c2.cod + ') ' + c2.den + ' - (' + c3.cod + ') ' + c3.den + ' - (' + c4.cod + ') ' + c4.den + ' - (' + c5.cod + ') ' + c5.den" & vbCrLf
sConsulta = sConsulta & "                    end CATEGORIARAMA," & vbCrLf
sConsulta = sConsulta & "                ITEM.ANYO ANYOPROCE," & vbCrLf
sConsulta = sConsulta & "      ITEM.GMN1_PROCE GMN1_PROCE," & vbCrLf
sConsulta = sConsulta & "                ITEM.PROCE CODPROCE," & vbCrLf
sConsulta = sConsulta & "                LE.COMENT,LPA.ADJUNTOS," & vbCrLf
sConsulta = sConsulta & "                CASE WHEN @MOSTRARCINTERNO IS NULL OR @MOSTRARCINTERNO=1 OR CC1.INTERNO=0 THEN LP.VALOR1 ELSE NULL END VALOR1," & vbCrLf
sConsulta = sConsulta & "                CASE WHEN @MOSTRARCINTERNO IS NULL OR @MOSTRARCINTERNO=1 OR CC2.INTERNO=0 THEN LP.VALOR2 ELSE NULL END VALOR2," & vbCrLf
sConsulta = sConsulta & "                CC1.DEN AS DENCAMPO1,CC2.DEN AS DENCAMPO2, PROCE.DEN AS DENPROCE,LP.OBSADJUN,LP.FECENTREGA," & vbCrLf
sConsulta = sConsulta & "                LP.PEDIDOABIERTO, LP.ORDEN" & vbCrLf
sConsulta = sConsulta & "FROM LINEAS_PEDIDO LP" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN APROB_LIM" & vbCrLf
sConsulta = sConsulta & "            ON LP.APROB_LIM=APROB_LIM.ID" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN SEGURIDAD " & vbCrLf
sConsulta = sConsulta & "            ON LP.SEGURIDAD=SEGURIDAD.ID" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN ITEM " & vbCrLf
sConsulta = sConsulta & "            ON LP.ANYO = ITEM.ANYO" & vbCrLf
sConsulta = sConsulta & "           AND LP.GMN1=ITEM.GMN1_PROCE" & vbCrLf
sConsulta = sConsulta & "           AND LP.PROCE=ITEM.PROCE " & vbCrLf
sConsulta = sConsulta & "           AND LP.ITEM=ITEM.ID" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN ART4 " & vbCrLf
sConsulta = sConsulta & "   ON ART4.COD=LP.ART_INT" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN ITEM_ADJ " & vbCrLf
sConsulta = sConsulta & "            ON LP.ITEM=ITEM_ADJ.ITEM " & vbCrLf
sConsulta = sConsulta & "           AND LP.ANYO = ITEM_ADJ.ANYO" & vbCrLf
sConsulta = sConsulta & "           AND LP.GMN1=ITEM_ADJ.GMN1 " & vbCrLf
sConsulta = sConsulta & "           AND LP.PROCE=ITEM_ADJ.PROCE " & vbCrLf
sConsulta = sConsulta & "           AND LP.PROVE=ITEM_ADJ.PROVE" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN PROVE_ART4 PA" & vbCrLf
sConsulta = sConsulta & "            ON LP.PROVE = PA.PROVE" & vbCrLf
sConsulta = sConsulta & "           AND LP.ART_INT = PA.ART" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN CATN1 C1" & vbCrLf
sConsulta = sConsulta & "            ON LP.CAT1 = C1.ID" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN CATN2 C2" & vbCrLf
sConsulta = sConsulta & "            ON LP.CAT2 = C2.ID" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN CATN3 C3" & vbCrLf
sConsulta = sConsulta & "            ON LP.CAT3 = C3.ID" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN CATN4 C4" & vbCrLf
sConsulta = sConsulta & "            ON LP.CAT4 = C4.ID" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN CATN5 C5" & vbCrLf
sConsulta = sConsulta & "            ON LP.CAT5 = C5.ID" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN LINEAS_EST LE" & vbCrLf
sConsulta = sConsulta & "            ON LP.LINEAS_EST = LE.ID" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN " & vbCrLf
sConsulta = sConsulta & "   (SELECT COUNT(*) AS ADJUNTOS,LINEA  FROM LINEAS_PEDIDO_ADJUN GROUP BY LINEAS_PEDIDO_ADJUN.LINEA) LPA ON LP.ID= LPA.LINEA " & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN CATALOG_CAMPOS AS CC1 " & vbCrLf
sConsulta = sConsulta & "            ON CC1.ID=LP.CAMPO1 AND CC1.IDIOMA=@IDI" & vbCrLf
sConsulta = sConsulta & "           AND CC1.INTERNO =  CASE WHEN @MOSTRARCINTERNO IS NULL OR @MOSTRARCINTERNO=1 THEN CC1.INTERNO ELSE 0 END" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN CATALOG_CAMPOS AS CC2 " & vbCrLf
sConsulta = sConsulta & "            ON CC2.ID=LP.CAMPO2 AND CC2.IDIOMA=@IDI" & vbCrLf
sConsulta = sConsulta & "           AND CC2.INTERNO = CASE WHEN @MOSTRARCINTERNO IS NULL OR @MOSTRARCINTERNO=1 THEN CC2.INTERNO ELSE 0 END" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN PROCE " & vbCrLf
sConsulta = sConsulta & "       ON LP.ANYO=PROCE.ANYO AND LP.GMN1=PROCE.GMN1 AND LP.PROCE=PROCE.COD" & vbCrLf
sConsulta = sConsulta & "WHERE CASE WHEN @ORDENID = 0 THEN -1 ELSE LP.ORDEN END " & vbCrLf
sConsulta = sConsulta & "       = CASE WHEN @ORDENID = 0 THEN -1 ELSE @ORDENID END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING = 'SELECT *, D.DEN_' + @IDI + ' DESTINODEN FROM #TEMP1  " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN DEST D  ON DESTINO = D.COD'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "set NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "set NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE #TEMP2 ( LINEAID INT, CODART VARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, GMN1 VARCHAR(10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, " & vbCrLf
sConsulta = sConsulta & "               GMN2 VARCHAR(10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, GMN3 VARCHAR(10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, GMN4 VARCHAR(10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL," & vbCrLf
sConsulta = sConsulta & "               PROVE VARCHAR(20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,  DENART VARCHAR(500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, " & vbCrLf
sConsulta = sConsulta & "               ART_EXT VARCHAR(120) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,  DESTINO VARCHAR(10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,  " & vbCrLf
sConsulta = sConsulta & "              UNIDAD VARCHAR(10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, PRECIOUNITARIO FLOAT, CANTIDAD FLOAT, CANT_REC FLOAT, " & vbCrLf
sConsulta = sConsulta & "              CANTADJUDICADA FLOAT, IMPORTE FLOAT, APROBADA INT, FECHA DATETIME,  FECENTREGA DATETIME, EST INT, ENTREGA_OBL INT,  FECENTREGAPROVE DATETIME, " & vbCrLf
sConsulta = sConsulta & "              CAT1 INT, CAT2 INT, CAT3 INT, CAT4 INT, CAT5 INT,  OBS VARCHAR(2020) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, CATEGORIA VARCHAR(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, " & vbCrLf
sConsulta = sConsulta & "              CATEGORIARAMA VARCHAR(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, CAT1DEN VARCHAR(120) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, " & vbCrLf
sConsulta = sConsulta & "              CAT2DEN VARCHAR(120) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,  CAT3DEN VARCHAR(120) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, " & vbCrLf
sConsulta = sConsulta & "              CAT4DEN VARCHAR(120) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, CAT5DEN VARCHAR(120) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, " & vbCrLf
sConsulta = sConsulta & "              COMENT VARCHAR(280) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, ANYOPROCE INT, GMN1_PROCE VARCHAR(3), CODPROCE INT, ITEM INT, ADJUNTOS INT, " & vbCrLf
sConsulta = sConsulta & "              VALOR1 VARCHAR(120) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, VALOR2 VARCHAR(120) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, " & vbCrLf
sConsulta = sConsulta & "              DENCAMPO1 VARCHAR(120) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, DENCAMPO2 VARCHAR(120) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, " & vbCrLf
sConsulta = sConsulta & "              DENPROCE VARCHAR(120), OBSADJUN VARCHAR(2020) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, " & vbCrLf
sConsulta = sConsulta & "              PEDIDOABIERTO INT, ORDEN INT)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO #TEMP2 SELECT DISTINCT LP.ID  ," & vbCrLf
sConsulta = sConsulta & "                LP.ART_INT CODART," & vbCrLf
sConsulta = sConsulta & "      I.GMN1 + ART4.GMN1 GMN1," & vbCrLf
sConsulta = sConsulta & "                I.GMN2 + ART4.GMN2 GMN2," & vbCrLf
sConsulta = sConsulta & "                I.GMN3 + ART4.GMN3 GMN3," & vbCrLf
sConsulta = sConsulta & "                I.GMN4 + ART4.GMN4 GMN4," & vbCrLf
sConsulta = sConsulta & "                LP.PROVE," & vbCrLf
sConsulta = sConsulta & "                I.DESCR  +  LP.DESCR_LIBRE DENART," & vbCrLf
sConsulta = sConsulta & "                PA.COD_EXT ART_EXT," & vbCrLf
sConsulta = sConsulta & "                LP.DEST DESTINO," & vbCrLf
sConsulta = sConsulta & "                LP.UP UNIDAD," & vbCrLf
sConsulta = sConsulta & "                LP.PREC_UP PRECIOUNITARIO," & vbCrLf
sConsulta = sConsulta & "                LP.CANT_PED CANTIDAD," & vbCrLf
sConsulta = sConsulta & "                LP.CANT_REC CANT_REC," & vbCrLf
sConsulta = sConsulta & "                IA.CANT_ADJ * LP.FC AS CANTADJUDICADA," & vbCrLf
sConsulta = sConsulta & "                LP.PREC_UP * LP.CANT_PED IMPORTE," & vbCrLf
sConsulta = sConsulta & "                ISNULL(LP.EST,1) AS APROBADA," & vbCrLf
sConsulta = sConsulta & "                LP.FECEMISION FECHA," & vbCrLf
sConsulta = sConsulta & "                LP.FECENTREGA FECENTREGA," & vbCrLf
sConsulta = sConsulta & "                ISNULL(LP.EST,1) EST,LP.ENTREGA_OBL,LP.FECENTREGAPROVE," & vbCrLf
sConsulta = sConsulta & "                LP.CAT1," & vbCrLf
sConsulta = sConsulta & "                LP.CAT2," & vbCrLf
sConsulta = sConsulta & "                LP.CAT3," & vbCrLf
sConsulta = sConsulta & "                LP.CAT4," & vbCrLf
sConsulta = sConsulta & "                LP.CAT5," & vbCrLf
sConsulta = sConsulta & "                LP.OBS," & vbCrLf
sConsulta = sConsulta & "                case when c5.cod is null then " & vbCrLf
sConsulta = sConsulta & "                        case when c4.cod is null then " & vbCrLf
sConsulta = sConsulta & "                             case when c3.cod is null then" & vbCrLf
sConsulta = sConsulta & "                                  case when c2.cod is null then" & vbCrLf
sConsulta = sConsulta & "                                       c1.cod" & vbCrLf
sConsulta = sConsulta & "                                  else" & vbCrLf
sConsulta = sConsulta & "                                  c2.cod" & vbCrLf
sConsulta = sConsulta & "                                  end" & vbCrLf
sConsulta = sConsulta & "                             else" & vbCrLf
sConsulta = sConsulta & "                             c3.cod" & vbCrLf
sConsulta = sConsulta & "                             end" & vbCrLf
sConsulta = sConsulta & "                        else" & vbCrLf
sConsulta = sConsulta & "                        c4.cod" & vbCrLf
sConsulta = sConsulta & "                        end" & vbCrLf
sConsulta = sConsulta & "                    else" & vbCrLf
sConsulta = sConsulta & "                    c5.cod" & vbCrLf
sConsulta = sConsulta & "                    end CATEGORIA," & vbCrLf
sConsulta = sConsulta & "                case when c5.cod is null then " & vbCrLf
sConsulta = sConsulta & "                        case when c4.cod is null then " & vbCrLf
sConsulta = sConsulta & "                             case when c3.cod is null then" & vbCrLf
sConsulta = sConsulta & "                                  case when c2.cod is null then" & vbCrLf
sConsulta = sConsulta & "                                       '(' + c1.cod + ') ' + c1.den" & vbCrLf
sConsulta = sConsulta & "                                  else" & vbCrLf
sConsulta = sConsulta & "                                  '(' + c1.cod + ') ' + c1.den + ' - (' + c2.cod + ') ' + c2.den" & vbCrLf
sConsulta = sConsulta & "                                  end" & vbCrLf
sConsulta = sConsulta & "                             else" & vbCrLf
sConsulta = sConsulta & "                             '(' + c1.cod + ') ' + c1.den + ' - (' + c2.cod + ') ' + c2.den + ' - (' + c3.cod + ') ' + c3.den" & vbCrLf
sConsulta = sConsulta & "                             end" & vbCrLf
sConsulta = sConsulta & "                        else" & vbCrLf
sConsulta = sConsulta & "                        '(' + c1.cod + ') ' + c1.den + ' - (' + c2.cod + ') ' + c2.den + ' - (' + c3.cod + ') ' + c3.den + ' - (' + c4.cod + ') ' + c4.den" & vbCrLf
sConsulta = sConsulta & "                        end" & vbCrLf
sConsulta = sConsulta & "                    else" & vbCrLf
sConsulta = sConsulta & "                    '(' + c1.cod + ') ' + c1.den + ' - (' + c2.cod + ') ' + c2.den + ' - (' + c3.cod + ') ' + c3.den + ' - (' + c4.cod + ') ' + c4.den + ' - (' + c5.cod + ') ' + c5.den" & vbCrLf
sConsulta = sConsulta & "                    end CATEGORIARAMA,               " & vbCrLf
sConsulta = sConsulta & "                        " & vbCrLf
sConsulta = sConsulta & "                C1.DEN CAT1DEN," & vbCrLf
sConsulta = sConsulta & "                C2.DEN CAT2DEN," & vbCrLf
sConsulta = sConsulta & "                C3.DEN CAT3DEN," & vbCrLf
sConsulta = sConsulta & "                C4.DEN CAT4DEN," & vbCrLf
sConsulta = sConsulta & "                C5.DEN CAT5DEN," & vbCrLf
sConsulta = sConsulta & "                LE.COMENT," & vbCrLf
sConsulta = sConsulta & "                I.ANYO ANYOPROCE," & vbCrLf
sConsulta = sConsulta & "      I.GMN1_PROCE GMN1_PROCE," & vbCrLf
sConsulta = sConsulta & "                I.PROCE CODPROCE," & vbCrLf
sConsulta = sConsulta & "                I.ID ITEM,LPA.ADJUNTOS," & vbCrLf
sConsulta = sConsulta & "                CASE WHEN @MOSTRARCINTERNO IS NULL OR @MOSTRARCINTERNO=1 OR CC1.INTERNO=0 THEN LP.VALOR1 ELSE NULL END VALOR1," & vbCrLf
sConsulta = sConsulta & "                CASE WHEN @MOSTRARCINTERNO IS NULL OR @MOSTRARCINTERNO=1 OR CC2.INTERNO=0 THEN LP.VALOR2 ELSE NULL END VALOR2," & vbCrLf
sConsulta = sConsulta & "                CC1.DEN AS DENCAMPO1,CC2.DEN AS DENCAMPO2,PROCE.DEN AS DENPROCE,LP.OBSADJUN, " & vbCrLf
sConsulta = sConsulta & "      LP.PEDIDOABIERTO, LP.ORDEN" & vbCrLf
sConsulta = sConsulta & " FROM LINEAS_PEDIDO LP" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN ITEM I " & vbCrLf
sConsulta = sConsulta & "             ON LP.ANYO = I.ANYO" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN1=I.GMN1_PROCE" & vbCrLf
sConsulta = sConsulta & "            AND LP.PROCE=I.PROCE " & vbCrLf
sConsulta = sConsulta & "            AND LP.ITEM=I.ID" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN ART4 " & vbCrLf
sConsulta = sConsulta & "   ON ART4.COD=LP.ART_INT" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN PROVE_ART4 PA" & vbCrLf
sConsulta = sConsulta & "             ON LP.PROVE = PA.PROVE" & vbCrLf
sConsulta = sConsulta & "            AND LP.ART_INT = PA.ART" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN ITEM_ADJ IA " & vbCrLf
sConsulta = sConsulta & "             ON LP.ITEM = IA.ITEM " & vbCrLf
sConsulta = sConsulta & "            AND LP.ANYO = IA.ANYO" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN1 = IA.GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND LP.PROCE= IA.PROCE " & vbCrLf
sConsulta = sConsulta & "            AND LP.PROVE= IA.PROVE" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN CATN1 C1" & vbCrLf
sConsulta = sConsulta & "             ON LP.CAT1 = C1.ID" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN CATN2 C2" & vbCrLf
sConsulta = sConsulta & "             ON LP.CAT2 = C2.ID" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN CATN3 C3" & vbCrLf
sConsulta = sConsulta & "             ON LP.CAT3 = C3.ID" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN CATN4 C4" & vbCrLf
sConsulta = sConsulta & "             ON LP.CAT4 = C4.ID" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN CATN5 C5" & vbCrLf
sConsulta = sConsulta & "             ON LP.CAT5 = C5.ID" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN LINEAS_EST LE" & vbCrLf
sConsulta = sConsulta & "             ON LP.LINEAS_EST = LE.ID" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN " & vbCrLf
sConsulta = sConsulta & "   (SELECT COUNT(*) AS ADJUNTOS,LINEA  FROM LINEAS_PEDIDO_ADJUN GROUP BY LINEAS_PEDIDO_ADJUN.LINEA) LPA ON LP.ID= LPA.LINEA " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN CATALOG_CAMPOS AS CC1 " & vbCrLf
sConsulta = sConsulta & "             ON CC1.ID=LP.CAMPO1 AND CC1.IDIOMA=@IDI" & vbCrLf
sConsulta = sConsulta & "            AND CC1.INTERNO = CASE WHEN @MOSTRARCINTERNO IS NULL OR @MOSTRARCINTERNO=1 THEN CC1.INTERNO ELSE 0 END" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN CATALOG_CAMPOS AS CC2 " & vbCrLf
sConsulta = sConsulta & "            ON CC2.ID=LP.CAMPO2 AND CC2.IDIOMA=@IDI" & vbCrLf
sConsulta = sConsulta & "           AND CC2.INTERNO = CASE WHEN @MOSTRARCINTERNO IS NULL OR @MOSTRARCINTERNO=1 THEN CC2.INTERNO ELSE 0 END" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN PROCE " & vbCrLf
sConsulta = sConsulta & "       ON LP.ANYO=PROCE.ANYO AND LP.GMN1=PROCE.GMN1 AND LP.PROCE=PROCE.COD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "WHERE CASE WHEN @ORDENID = 0 THEN -1 ELSE LP.ORDEN END " & vbCrLf
sConsulta = sConsulta & "       = CASE WHEN @ORDENID = 0 THEN -1 ELSE @ORDENID END" & vbCrLf
sConsulta = sConsulta & "   AND CASE WHEN LP.SOLICIT IS NULL AND @SOLICIT IS NULL THEN -1 ELSE LP.SOLICIT END " & vbCrLf
sConsulta = sConsulta & "       = CASE WHEN LP.SOLICIT IS NULL AND @SOLICIT IS NULL THEN -1 ELSE ISNULL(@SOLICIT,LP.SOLICIT) END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING = 'SELECT *, U.DEN_' + @IDI + ' UNIDEN, D.DEN_' + @IDI + ' DESTDEN FROM #TEMP2   " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN UNI U  ON UNIDAD = U.COD  " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN DEST D  ON DESTINO = D.COD'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "PRINT @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "set NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion31800_07_0851A_31800_07_0852() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim iRespuesta As Integer


On Error GoTo Error
sConsulta = "BEGIN TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = True
ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

frmProgreso.lblDetalle = "Actualiza Storeds"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
frmProgreso.ProgressBar1.Refresh
V_31800_7_Storeds_052

sConsulta = "UPDATE VERSION SET NUM ='3.00.08.52'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103), PRODVER='31800.7'"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "COMMIT TRANSACTION"
ExecuteSQL gRDOCon, sConsulta

bTransaccionEnCurso = False

CodigoDeActualizacion31800_07_0851A_31800_07_0852 = True
Exit Function

Error:
    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If

    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    CodigoDeActualizacion31800_07_0851A_31800_07_0852 = False
End Function



Private Sub V_31800_7_Storeds_052()

Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_DEVOLVER_LINEAS_DE_ORDEN]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_DEVOLVER_LINEAS_DE_ORDEN]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSEP_DEVOLVER_LINEAS_DE_ORDEN(@APROBADOR VARCHAR(50)=null, @ORDENID INTEGER = 0, @SOLICIT int = null ,@IDI varchar(20) , @MOSTRARCINTERNO tinyint = null)  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(1500)" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if @APROBADOR IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "set NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE #TEMP1 ( LINEAID INT, CODART VARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, " & vbCrLf
sConsulta = sConsulta & "               DENART VARCHAR(500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, " & vbCrLf
sConsulta = sConsulta & "     ARTICULO VARCHAR(500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, " & vbCrLf
sConsulta = sConsulta & "               ART_EXT VARCHAR(120) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,  " & vbCrLf
sConsulta = sConsulta & "               GMN1 VARCHAR(10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, GMN2 VARCHAR(10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, GMN3 VARCHAR(10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, " & vbCrLf
sConsulta = sConsulta & "               GMN4 VARCHAR(10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, PROVE VARCHAR(20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, " & vbCrLf
sConsulta = sConsulta & "    DESTINO VARCHAR(10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,  " & vbCrLf
sConsulta = sConsulta & "              UNIDAD VARCHAR(10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, PRECIOUNITARIO FLOAT, CANTIDAD FLOAT, CANT_REC FLOAT, " & vbCrLf
sConsulta = sConsulta & "              CANTADJUDICADA FLOAT,  CANTCONSUMIDA FLOAT, IMPORTE FLOAT, APROBADA INT, FECHA DATETIME,  " & vbCrLf
sConsulta = sConsulta & "    GESTIONABLE VARCHAR(5),    " & vbCrLf
sConsulta = sConsulta & "              CAT1 INT, CAT2 INT, CAT3 INT, CAT4 INT, CAT5 INT,  OBS VARCHAR(2020) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, CATEGORIA VARCHAR(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, " & vbCrLf
sConsulta = sConsulta & "              CATEGORIARAMA VARCHAR(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,  " & vbCrLf
sConsulta = sConsulta & "    ANYOPROCE INT, GMN1_PROCE VARCHAR(3), CODPROCE INT,  COMENT VARCHAR(280) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, ADJUNTOS INT, " & vbCrLf
sConsulta = sConsulta & "              VALOR1 VARCHAR(120) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, VALOR2 VARCHAR(120) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, " & vbCrLf
sConsulta = sConsulta & "              DENCAMPO1 VARCHAR(120) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, DENCAMPO2 VARCHAR(120) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, " & vbCrLf
sConsulta = sConsulta & "              DENPROCE VARCHAR(120), OBSADJUN VARCHAR(2020) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, " & vbCrLf
sConsulta = sConsulta & "    FECENTREGA DATETIME, ENTREGA_OBL INT, PEDIDOABIERTO INT, ORDEN INT)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO #TEMP1 SELECT DISTINCT LP.ID AS LINEAID," & vbCrLf
sConsulta = sConsulta & "                LP.ART_INT CODART," & vbCrLf
sConsulta = sConsulta & "                ITEM.DESCR  + LP.DESCR_LIBRE DENART," & vbCrLf
sConsulta = sConsulta & "                LP.ART_INT + ' ' +  ITEM.DESCR  + LP.DESCR_LIBRE  AS ARTICULO," & vbCrLf
sConsulta = sConsulta & "                PA.COD_EXT ART_EXT," & vbCrLf
sConsulta = sConsulta & "                ITEM.GMN1 + ART4.GMN1 GMN1," & vbCrLf
sConsulta = sConsulta & "                ITEM.GMN2 + ART4.GMN2 GMN2," & vbCrLf
sConsulta = sConsulta & "                ITEM.GMN3 + ART4.GMN3 GMN3," & vbCrLf
sConsulta = sConsulta & "                ITEM.GMN4 + ART4.GMN4 GMN4," & vbCrLf
sConsulta = sConsulta & "                LP.PROVE," & vbCrLf
sConsulta = sConsulta & "                LP.DEST AS DESTINO," & vbCrLf
sConsulta = sConsulta & "                LP.UP AS UNIDAD," & vbCrLf
sConsulta = sConsulta & "                LP.PREC_UP AS PRECIOUNITARIO," & vbCrLf
sConsulta = sConsulta & "                LP.CANT_PED AS CANTIDAD," & vbCrLf
sConsulta = sConsulta & "                LP.CANT_REC AS CANTIDAD_REC," & vbCrLf
sConsulta = sConsulta & "                ITEM_ADJ.CANT_ADJ / LP.FC AS CANTADJUDICADA," & vbCrLf
sConsulta = sConsulta & "                ITEM_ADJ.CANT_PED / LP.FC AS CANTCONSUMIDA," & vbCrLf
sConsulta = sConsulta & "                LP.PREC_UP * LP.CANT_PED IMPORTE," & vbCrLf
sConsulta = sConsulta & "                LP.EST AS APROBADA," & vbCrLf
sConsulta = sConsulta & "                LP.FECEMISION AS FECHA," & vbCrLf
sConsulta = sConsulta & "                GESTIONABLE=CASE WHEN APROB_LIM.PER= @APROBADOR   THEN 1 WHEN (APROB_LIM.PER IS NULL AND SEGURIDAD.APROB_TIPO1=@APROBADOR ) THEN 1  ELSE 0 END," & vbCrLf
sConsulta = sConsulta & "                LP.CAT1," & vbCrLf
sConsulta = sConsulta & "                LP.CAT2," & vbCrLf
sConsulta = sConsulta & "                LP.CAT3," & vbCrLf
sConsulta = sConsulta & "                LP.CAT4," & vbCrLf
sConsulta = sConsulta & "                LP.CAT5," & vbCrLf
sConsulta = sConsulta & "                LP.OBS," & vbCrLf
sConsulta = sConsulta & "                case when c5.cod is null then " & vbCrLf
sConsulta = sConsulta & "                        case when c4.cod is null then " & vbCrLf
sConsulta = sConsulta & "                             case when c3.cod is null then" & vbCrLf
sConsulta = sConsulta & "                                  case when c2.cod is null then" & vbCrLf
sConsulta = sConsulta & "                                       c1.cod" & vbCrLf
sConsulta = sConsulta & "                                  else" & vbCrLf
sConsulta = sConsulta & "                                  c2.cod" & vbCrLf
sConsulta = sConsulta & "                                  end" & vbCrLf
sConsulta = sConsulta & "                             else" & vbCrLf
sConsulta = sConsulta & "                             c3.cod" & vbCrLf
sConsulta = sConsulta & "                             end" & vbCrLf
sConsulta = sConsulta & "                        else" & vbCrLf
sConsulta = sConsulta & "                        c4.cod" & vbCrLf
sConsulta = sConsulta & "                        end" & vbCrLf
sConsulta = sConsulta & "                    else" & vbCrLf
sConsulta = sConsulta & "                    c5.cod" & vbCrLf
sConsulta = sConsulta & "                    end CATEGORIA," & vbCrLf
sConsulta = sConsulta & "                case when c5.cod is null then " & vbCrLf
sConsulta = sConsulta & "                        case when c4.cod is null then " & vbCrLf
sConsulta = sConsulta & "                             case when c3.cod is null then" & vbCrLf
sConsulta = sConsulta & "                                  case when c2.cod is null then" & vbCrLf
sConsulta = sConsulta & "                                       '(' + c1.cod + ') ' + c1.den" & vbCrLf
sConsulta = sConsulta & "                                  else" & vbCrLf
sConsulta = sConsulta & "                                  '(' + c1.cod + ') ' + c1.den + ' - (' + c2.cod + ') ' + c2.den" & vbCrLf
sConsulta = sConsulta & "                                  end" & vbCrLf
sConsulta = sConsulta & "                             else" & vbCrLf
sConsulta = sConsulta & "                             '(' + c1.cod + ') ' + c1.den + ' - (' + c2.cod + ') ' + c2.den + ' - (' + c3.cod + ') ' + c3.den" & vbCrLf
sConsulta = sConsulta & "                             end" & vbCrLf
sConsulta = sConsulta & "                        else" & vbCrLf
sConsulta = sConsulta & "                        '(' + c1.cod + ') ' + c1.den + ' - (' + c2.cod + ') ' + c2.den + ' - (' + c3.cod + ') ' + c3.den + ' - (' + c4.cod + ') ' + c4.den" & vbCrLf
sConsulta = sConsulta & "                        end" & vbCrLf
sConsulta = sConsulta & "                    else" & vbCrLf
sConsulta = sConsulta & "                    '(' + c1.cod + ') ' + c1.den + ' - (' + c2.cod + ') ' + c2.den + ' - (' + c3.cod + ') ' + c3.den + ' - (' + c4.cod + ') ' + c4.den + ' - (' + c5.cod + ') ' + c5.den" & vbCrLf
sConsulta = sConsulta & "                    end CATEGORIARAMA," & vbCrLf
sConsulta = sConsulta & "                ITEM.ANYO ANYOPROCE," & vbCrLf
sConsulta = sConsulta & "      ITEM.GMN1_PROCE GMN1_PROCE," & vbCrLf
sConsulta = sConsulta & "                ITEM.PROCE CODPROCE," & vbCrLf
sConsulta = sConsulta & "                LE.COMENT,LPA.ADJUNTOS," & vbCrLf
sConsulta = sConsulta & "                CASE WHEN @MOSTRARCINTERNO IS NULL OR @MOSTRARCINTERNO=1 OR CC1.INTERNO=0 THEN LP.VALOR1 ELSE NULL END VALOR1," & vbCrLf
sConsulta = sConsulta & "                CASE WHEN @MOSTRARCINTERNO IS NULL OR @MOSTRARCINTERNO=1 OR CC2.INTERNO=0 THEN LP.VALOR2 ELSE NULL END VALOR2," & vbCrLf
sConsulta = sConsulta & "                CC1.DEN AS DENCAMPO1,CC2.DEN AS DENCAMPO2, PROCE.DEN AS DENPROCE,LP.OBSADJUN,LP.FECENTREGA, LP.ENTREGA_OBL," & vbCrLf
sConsulta = sConsulta & "                LP.PEDIDOABIERTO, LP.ORDEN" & vbCrLf
sConsulta = sConsulta & "FROM LINEAS_PEDIDO LP" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN APROB_LIM" & vbCrLf
sConsulta = sConsulta & "            ON LP.APROB_LIM=APROB_LIM.ID" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN SEGURIDAD " & vbCrLf
sConsulta = sConsulta & "            ON LP.SEGURIDAD=SEGURIDAD.ID" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN ITEM " & vbCrLf
sConsulta = sConsulta & "            ON LP.ANYO = ITEM.ANYO" & vbCrLf
sConsulta = sConsulta & "           AND LP.GMN1=ITEM.GMN1_PROCE" & vbCrLf
sConsulta = sConsulta & "           AND LP.PROCE=ITEM.PROCE " & vbCrLf
sConsulta = sConsulta & "           AND LP.ITEM=ITEM.ID" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN ART4 " & vbCrLf
sConsulta = sConsulta & "   ON ART4.COD=LP.ART_INT" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN ITEM_ADJ " & vbCrLf
sConsulta = sConsulta & "            ON LP.ITEM=ITEM_ADJ.ITEM " & vbCrLf
sConsulta = sConsulta & "           AND LP.ANYO = ITEM_ADJ.ANYO" & vbCrLf
sConsulta = sConsulta & "           AND LP.GMN1=ITEM_ADJ.GMN1 " & vbCrLf
sConsulta = sConsulta & "           AND LP.PROCE=ITEM_ADJ.PROCE " & vbCrLf
sConsulta = sConsulta & "           AND LP.PROVE=ITEM_ADJ.PROVE" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN PROVE_ART4 PA" & vbCrLf
sConsulta = sConsulta & "            ON LP.PROVE = PA.PROVE" & vbCrLf
sConsulta = sConsulta & "           AND LP.ART_INT = PA.ART" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN CATN1 C1" & vbCrLf
sConsulta = sConsulta & "            ON LP.CAT1 = C1.ID" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN CATN2 C2" & vbCrLf
sConsulta = sConsulta & "            ON LP.CAT2 = C2.ID" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN CATN3 C3" & vbCrLf
sConsulta = sConsulta & "            ON LP.CAT3 = C3.ID" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN CATN4 C4" & vbCrLf
sConsulta = sConsulta & "            ON LP.CAT4 = C4.ID" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN CATN5 C5" & vbCrLf
sConsulta = sConsulta & "            ON LP.CAT5 = C5.ID" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN LINEAS_EST LE" & vbCrLf
sConsulta = sConsulta & "            ON LP.LINEAS_EST = LE.ID" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN " & vbCrLf
sConsulta = sConsulta & "   (SELECT COUNT(*) AS ADJUNTOS,LINEA  FROM LINEAS_PEDIDO_ADJUN GROUP BY LINEAS_PEDIDO_ADJUN.LINEA) LPA ON LP.ID= LPA.LINEA " & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN CATALOG_CAMPOS AS CC1 " & vbCrLf
sConsulta = sConsulta & "            ON CC1.ID=LP.CAMPO1 AND CC1.IDIOMA=@IDI" & vbCrLf
sConsulta = sConsulta & "           AND CC1.INTERNO =  CASE WHEN @MOSTRARCINTERNO IS NULL OR @MOSTRARCINTERNO=1 THEN CC1.INTERNO ELSE 0 END" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN CATALOG_CAMPOS AS CC2 " & vbCrLf
sConsulta = sConsulta & "            ON CC2.ID=LP.CAMPO2 AND CC2.IDIOMA=@IDI" & vbCrLf
sConsulta = sConsulta & "           AND CC2.INTERNO = CASE WHEN @MOSTRARCINTERNO IS NULL OR @MOSTRARCINTERNO=1 THEN CC2.INTERNO ELSE 0 END" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN PROCE " & vbCrLf
sConsulta = sConsulta & "       ON LP.ANYO=PROCE.ANYO AND LP.GMN1=PROCE.GMN1 AND LP.PROCE=PROCE.COD" & vbCrLf
sConsulta = sConsulta & "WHERE CASE WHEN @ORDENID = 0 THEN -1 ELSE LP.ORDEN END " & vbCrLf
sConsulta = sConsulta & "       = CASE WHEN @ORDENID = 0 THEN -1 ELSE @ORDENID END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING = 'SELECT *, U.DEN_' + @IDI + ' UNIDEN, D.DEN_' + @IDI + ' DESTINODEN FROM #TEMP1" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN UNI U  ON UNIDAD = U.COD  " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN DEST D  ON DESTINO = D.COD'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "set NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "set NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE #TEMP2 ( LINEAID INT, CODART VARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, GMN1 VARCHAR(10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, " & vbCrLf
sConsulta = sConsulta & "               GMN2 VARCHAR(10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, GMN3 VARCHAR(10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, GMN4 VARCHAR(10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL," & vbCrLf
sConsulta = sConsulta & "               PROVE VARCHAR(20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,  DENART VARCHAR(500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, " & vbCrLf
sConsulta = sConsulta & "               ART_EXT VARCHAR(120) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,  DESTINO VARCHAR(10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,  " & vbCrLf
sConsulta = sConsulta & "              UNIDAD VARCHAR(10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, PRECIOUNITARIO FLOAT, CANTIDAD FLOAT, CANT_REC FLOAT, " & vbCrLf
sConsulta = sConsulta & "              CANTADJUDICADA FLOAT, IMPORTE FLOAT, APROBADA INT, FECHA DATETIME,  FECENTREGA DATETIME, EST INT, ENTREGA_OBL INT,  FECENTREGAPROVE DATETIME, " & vbCrLf
sConsulta = sConsulta & "              CAT1 INT, CAT2 INT, CAT3 INT, CAT4 INT, CAT5 INT,  OBS VARCHAR(2020) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, CATEGORIA VARCHAR(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, " & vbCrLf
sConsulta = sConsulta & "              CATEGORIARAMA VARCHAR(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, CAT1DEN VARCHAR(120) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, " & vbCrLf
sConsulta = sConsulta & "              CAT2DEN VARCHAR(120) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,  CAT3DEN VARCHAR(120) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, " & vbCrLf
sConsulta = sConsulta & "              CAT4DEN VARCHAR(120) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, CAT5DEN VARCHAR(120) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, " & vbCrLf
sConsulta = sConsulta & "              COMENT VARCHAR(280) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, ANYOPROCE INT, GMN1_PROCE VARCHAR(3), CODPROCE INT, ITEM INT, ADJUNTOS INT, " & vbCrLf
sConsulta = sConsulta & "              VALOR1 VARCHAR(120) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, VALOR2 VARCHAR(120) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, " & vbCrLf
sConsulta = sConsulta & "              DENCAMPO1 VARCHAR(120) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, DENCAMPO2 VARCHAR(120) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, " & vbCrLf
sConsulta = sConsulta & "              DENPROCE VARCHAR(120), OBSADJUN VARCHAR(2020) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, " & vbCrLf
sConsulta = sConsulta & "              PEDIDOABIERTO INT, ORDEN INT)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO #TEMP2 SELECT DISTINCT LP.ID  ," & vbCrLf
sConsulta = sConsulta & "                LP.ART_INT CODART," & vbCrLf
sConsulta = sConsulta & "      I.GMN1 + ART4.GMN1 GMN1," & vbCrLf
sConsulta = sConsulta & "                I.GMN2 + ART4.GMN2 GMN2," & vbCrLf
sConsulta = sConsulta & "                I.GMN3 + ART4.GMN3 GMN3," & vbCrLf
sConsulta = sConsulta & "                I.GMN4 + ART4.GMN4 GMN4," & vbCrLf
sConsulta = sConsulta & "                LP.PROVE," & vbCrLf
sConsulta = sConsulta & "                I.DESCR  +  LP.DESCR_LIBRE DENART," & vbCrLf
sConsulta = sConsulta & "                PA.COD_EXT ART_EXT," & vbCrLf
sConsulta = sConsulta & "                LP.DEST DESTINO," & vbCrLf
sConsulta = sConsulta & "                LP.UP UNIDAD," & vbCrLf
sConsulta = sConsulta & "                LP.PREC_UP PRECIOUNITARIO," & vbCrLf
sConsulta = sConsulta & "                LP.CANT_PED CANTIDAD," & vbCrLf
sConsulta = sConsulta & "                LP.CANT_REC CANT_REC," & vbCrLf
sConsulta = sConsulta & "                IA.CANT_ADJ * LP.FC AS CANTADJUDICADA," & vbCrLf
sConsulta = sConsulta & "                LP.PREC_UP * LP.CANT_PED IMPORTE," & vbCrLf
sConsulta = sConsulta & "                ISNULL(LP.EST,1) AS APROBADA," & vbCrLf
sConsulta = sConsulta & "                LP.FECEMISION FECHA," & vbCrLf
sConsulta = sConsulta & "                LP.FECENTREGA FECENTREGA," & vbCrLf
sConsulta = sConsulta & "                ISNULL(LP.EST,1) EST,LP.ENTREGA_OBL,LP.FECENTREGAPROVE," & vbCrLf
sConsulta = sConsulta & "                LP.CAT1," & vbCrLf
sConsulta = sConsulta & "                LP.CAT2," & vbCrLf
sConsulta = sConsulta & "                LP.CAT3," & vbCrLf
sConsulta = sConsulta & "                LP.CAT4," & vbCrLf
sConsulta = sConsulta & "                LP.CAT5," & vbCrLf
sConsulta = sConsulta & "                LP.OBS," & vbCrLf
sConsulta = sConsulta & "                case when c5.cod is null then " & vbCrLf
sConsulta = sConsulta & "                        case when c4.cod is null then " & vbCrLf
sConsulta = sConsulta & "                             case when c3.cod is null then" & vbCrLf
sConsulta = sConsulta & "                                  case when c2.cod is null then" & vbCrLf
sConsulta = sConsulta & "                                       c1.cod" & vbCrLf
sConsulta = sConsulta & "                                  else" & vbCrLf
sConsulta = sConsulta & "                                  c2.cod" & vbCrLf
sConsulta = sConsulta & "                                  end" & vbCrLf
sConsulta = sConsulta & "                             else" & vbCrLf
sConsulta = sConsulta & "                             c3.cod" & vbCrLf
sConsulta = sConsulta & "                             end" & vbCrLf
sConsulta = sConsulta & "                        else" & vbCrLf
sConsulta = sConsulta & "                        c4.cod" & vbCrLf
sConsulta = sConsulta & "                        end" & vbCrLf
sConsulta = sConsulta & "                    else" & vbCrLf
sConsulta = sConsulta & "                    c5.cod" & vbCrLf
sConsulta = sConsulta & "                    end CATEGORIA," & vbCrLf
sConsulta = sConsulta & "                case when c5.cod is null then " & vbCrLf
sConsulta = sConsulta & "                        case when c4.cod is null then " & vbCrLf
sConsulta = sConsulta & "                             case when c3.cod is null then" & vbCrLf
sConsulta = sConsulta & "                                  case when c2.cod is null then" & vbCrLf
sConsulta = sConsulta & "                                       '(' + c1.cod + ') ' + c1.den" & vbCrLf
sConsulta = sConsulta & "                                  else" & vbCrLf
sConsulta = sConsulta & "                                  '(' + c1.cod + ') ' + c1.den + ' - (' + c2.cod + ') ' + c2.den" & vbCrLf
sConsulta = sConsulta & "                                  end" & vbCrLf
sConsulta = sConsulta & "                             else" & vbCrLf
sConsulta = sConsulta & "                             '(' + c1.cod + ') ' + c1.den + ' - (' + c2.cod + ') ' + c2.den + ' - (' + c3.cod + ') ' + c3.den" & vbCrLf
sConsulta = sConsulta & "                             end" & vbCrLf
sConsulta = sConsulta & "                        else" & vbCrLf
sConsulta = sConsulta & "                        '(' + c1.cod + ') ' + c1.den + ' - (' + c2.cod + ') ' + c2.den + ' - (' + c3.cod + ') ' + c3.den + ' - (' + c4.cod + ') ' + c4.den" & vbCrLf
sConsulta = sConsulta & "                        end" & vbCrLf
sConsulta = sConsulta & "                    else" & vbCrLf
sConsulta = sConsulta & "                    '(' + c1.cod + ') ' + c1.den + ' - (' + c2.cod + ') ' + c2.den + ' - (' + c3.cod + ') ' + c3.den + ' - (' + c4.cod + ') ' + c4.den + ' - (' + c5.cod + ') ' + c5.den" & vbCrLf
sConsulta = sConsulta & "                    end CATEGORIARAMA,               " & vbCrLf
sConsulta = sConsulta & "                        " & vbCrLf
sConsulta = sConsulta & "                C1.DEN CAT1DEN," & vbCrLf
sConsulta = sConsulta & "                C2.DEN CAT2DEN," & vbCrLf
sConsulta = sConsulta & "                C3.DEN CAT3DEN," & vbCrLf
sConsulta = sConsulta & "                C4.DEN CAT4DEN," & vbCrLf
sConsulta = sConsulta & "                C5.DEN CAT5DEN," & vbCrLf
sConsulta = sConsulta & "                LE.COMENT," & vbCrLf
sConsulta = sConsulta & "                I.ANYO ANYOPROCE," & vbCrLf
sConsulta = sConsulta & "      I.GMN1_PROCE GMN1_PROCE," & vbCrLf
sConsulta = sConsulta & "                I.PROCE CODPROCE," & vbCrLf
sConsulta = sConsulta & "                I.ID ITEM,LPA.ADJUNTOS," & vbCrLf
sConsulta = sConsulta & "                CASE WHEN @MOSTRARCINTERNO IS NULL OR @MOSTRARCINTERNO=1 OR CC1.INTERNO=0 THEN LP.VALOR1 ELSE NULL END VALOR1," & vbCrLf
sConsulta = sConsulta & "                CASE WHEN @MOSTRARCINTERNO IS NULL OR @MOSTRARCINTERNO=1 OR CC2.INTERNO=0 THEN LP.VALOR2 ELSE NULL END VALOR2," & vbCrLf
sConsulta = sConsulta & "                CC1.DEN AS DENCAMPO1,CC2.DEN AS DENCAMPO2,PROCE.DEN AS DENPROCE,LP.OBSADJUN, " & vbCrLf
sConsulta = sConsulta & "      LP.PEDIDOABIERTO, LP.ORDEN" & vbCrLf
sConsulta = sConsulta & " FROM LINEAS_PEDIDO LP" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN ITEM I " & vbCrLf
sConsulta = sConsulta & "             ON LP.ANYO = I.ANYO" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN1=I.GMN1_PROCE" & vbCrLf
sConsulta = sConsulta & "            AND LP.PROCE=I.PROCE " & vbCrLf
sConsulta = sConsulta & "            AND LP.ITEM=I.ID" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN ART4 " & vbCrLf
sConsulta = sConsulta & "   ON ART4.COD=LP.ART_INT" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN PROVE_ART4 PA" & vbCrLf
sConsulta = sConsulta & "             ON LP.PROVE = PA.PROVE" & vbCrLf
sConsulta = sConsulta & "            AND LP.ART_INT = PA.ART" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN ITEM_ADJ IA " & vbCrLf
sConsulta = sConsulta & "             ON LP.ITEM = IA.ITEM " & vbCrLf
sConsulta = sConsulta & "            AND LP.ANYO = IA.ANYO" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN1 = IA.GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND LP.PROCE= IA.PROCE " & vbCrLf
sConsulta = sConsulta & "            AND LP.PROVE= IA.PROVE" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN CATN1 C1" & vbCrLf
sConsulta = sConsulta & "             ON LP.CAT1 = C1.ID" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN CATN2 C2" & vbCrLf
sConsulta = sConsulta & "             ON LP.CAT2 = C2.ID" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN CATN3 C3" & vbCrLf
sConsulta = sConsulta & "             ON LP.CAT3 = C3.ID" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN CATN4 C4" & vbCrLf
sConsulta = sConsulta & "             ON LP.CAT4 = C4.ID" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN CATN5 C5" & vbCrLf
sConsulta = sConsulta & "             ON LP.CAT5 = C5.ID" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN LINEAS_EST LE" & vbCrLf
sConsulta = sConsulta & "             ON LP.LINEAS_EST = LE.ID" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN " & vbCrLf
sConsulta = sConsulta & "   (SELECT COUNT(*) AS ADJUNTOS,LINEA  FROM LINEAS_PEDIDO_ADJUN GROUP BY LINEAS_PEDIDO_ADJUN.LINEA) LPA ON LP.ID= LPA.LINEA " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN CATALOG_CAMPOS AS CC1 " & vbCrLf
sConsulta = sConsulta & "             ON CC1.ID=LP.CAMPO1 AND CC1.IDIOMA=@IDI" & vbCrLf
sConsulta = sConsulta & "            AND CC1.INTERNO = CASE WHEN @MOSTRARCINTERNO IS NULL OR @MOSTRARCINTERNO=1 THEN CC1.INTERNO ELSE 0 END" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN CATALOG_CAMPOS AS CC2 " & vbCrLf
sConsulta = sConsulta & "            ON CC2.ID=LP.CAMPO2 AND CC2.IDIOMA=@IDI" & vbCrLf
sConsulta = sConsulta & "           AND CC2.INTERNO = CASE WHEN @MOSTRARCINTERNO IS NULL OR @MOSTRARCINTERNO=1 THEN CC2.INTERNO ELSE 0 END" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN PROCE " & vbCrLf
sConsulta = sConsulta & "       ON LP.ANYO=PROCE.ANYO AND LP.GMN1=PROCE.GMN1 AND LP.PROCE=PROCE.COD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "WHERE CASE WHEN @ORDENID = 0 THEN -1 ELSE LP.ORDEN END " & vbCrLf
sConsulta = sConsulta & "       = CASE WHEN @ORDENID = 0 THEN -1 ELSE @ORDENID END" & vbCrLf
sConsulta = sConsulta & "   AND CASE WHEN LP.SOLICIT IS NULL AND @SOLICIT IS NULL THEN -1 ELSE LP.SOLICIT END " & vbCrLf
sConsulta = sConsulta & "       = CASE WHEN LP.SOLICIT IS NULL AND @SOLICIT IS NULL THEN -1 ELSE ISNULL(@SOLICIT,LP.SOLICIT) END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING = 'SELECT *, U.DEN_' + @IDI + ' UNIDEN, D.DEN_' + @IDI + ' DESTDEN FROM #TEMP2   " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN UNI U  ON UNIDAD = U.COD  " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN DEST D  ON DESTINO = D.COD'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "PRINT @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLSTRING" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "set NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_APROB_DEVOLVER_ORDENES]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_APROB_DEVOLVER_ORDENES]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSEP_APROB_DEVOLVER_ORDENES] @APROBADOR VARCHAR(50) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT OE.PEDIDO, OE.ID AS ID, OE.ANYO AS ANYO, PEDIDO.NUM AS NUMPEDIDO, OE.NUM AS NUMORDEN, OE.NUM_PED_ERP AS REFERENCIA" & vbCrLf
sConsulta = sConsulta & "   , OE.PROVE AS PROVECOD, PROVE.DEN AS PROVEDEN, OE.FECHA" & vbCrLf
sConsulta = sConsulta & "   , CASE WHEN OE.EST>19 THEN POWER(2,OE.EST-13) ELSE POWER(2,OE.EST) END AS EST" & vbCrLf
sConsulta = sConsulta & "   , OE.IMPORTE, OE.MON, OE.CAMBIO, OE.NUMEXT, OE.TIPO" & vbCrLf
sConsulta = sConsulta & "   , PEDIDO.PER AS APROVISIONADOR, PER.APE AS APROV_APE, PER.NOM AS APROV_NOM, OE.OBS, 1 MOTIVO, COUNT(ORDEN_ENTREGA_ADJUN.ID)  ARCH" & vbCrLf
sConsulta = sConsulta & "   , OE.EMPRESA, E.DEN DENEMPRESA, E.NIF NIFEMPRESA, OE.COD_PROVE_ERP, OE.RECEPTOR, R.APE AS RECEP_APE, R.NOM AS RECEP_NOM" & vbCrLf
sConsulta = sConsulta & "   , R.TFNO TFNORECEPTOR, R.EMAIL EMAILRECEPTOR" & vbCrLf
sConsulta = sConsulta & "   , ISNULL(R.UON3,ISNULL(R.UON2,R.UON1)) UONRECEPTOR, ISNULL(UON3.DEN,ISNULL(UON2.DEN,UON1.DEN)) DENUONRECEPTOR" & vbCrLf
sConsulta = sConsulta & "   , OES.FECHA FECEST, ADJ.ADJUNTOS" & vbCrLf
sConsulta = sConsulta & "FROM LINEAS_PEDIDO WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN ORDEN_ENTREGA OE WITH (NOLOCK) ON LINEAS_PEDIDO.ORDEN=OE.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PEDIDO WITH (NOLOCK) ON OE.PEDIDO=PEDIDO.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PROVE WITH (NOLOCK) ON OE.PROVE=PROVE.COD " & vbCrLf
sConsulta = sConsulta & "INNER JOIN PER ON PER.COD=PEDIDO.PER" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN SEGURIDAD WITH (NOLOCK) ON LINEAS_PEDIDO.SEGURIDAD=SEGURIDAD.ID" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN APROB_LIM WITH (NOLOCK) ON LINEAS_PEDIDO.APROB_LIM=APROB_LIM.ID" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN ORDEN_ENTREGA_ADJUN WITH (NOLOCK) ON OE.ID=ORDEN_ENTREGA_ADJUN.ORDEN" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN EMP E WITH (NOLOCK) ON OE.EMPRESA = E.ID" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN (PER R WITH (NOLOCK) LEFT JOIN UON1 WITH (NOLOCK) ON R.UON1 = UON1.COD LEFT JOIN UON2 WITH (NOLOCK) ON R.UON1 = UON2.UON1 AND R.UON2 = UON2.COD LEFT JOIN UON3 WITH (NOLOCK) ON R.UON1 = UON3.UON1 AND R.UON2 = UON3.UON2 AND R.UON3 = UON3.COD) ON OE.RECEPTOR = R.COD" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN ORDEN_EST OES WITH(NOLOCK) ON OE.ORDEN_EST = OES.ID" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN (SELECT COUNT(*) AS ADJUNTOS,ORDEN  " & vbCrLf
sConsulta = sConsulta & "           FROM ORDEN_ENTREGA_ADJUN WITH(NOLOCK) GROUP BY ORDEN_ENTREGA_ADJUN.ORDEN) ADJ ON OE.ID= ADJ.ORDEN " & vbCrLf
sConsulta = sConsulta & "WHERE LINEAS_PEDIDO.NIVEL_APROB=0 AND SEGURIDAD.APROB_TIPO1=@APROBADOR OR APROB_LIM.PER=@APROBADOR" & vbCrLf
sConsulta = sConsulta & "GROUP BY OE.PEDIDO, OE.ID, OE.ANYO, PEDIDO.NUM, OE.NUM, OE.NUM_PED_ERP, OE.PROVE, PROVE.DEN, OE.FECHA, OE.EST" & vbCrLf
sConsulta = sConsulta & "   , OE.IMPORTE, PEDIDO.PER, PER.APE, PER.NOM, OE.OBS, OE.EMPRESA, E.DEN, E.NIF, OE.MON, OE.CAMBIO, OE.NUMEXT, OE.TIPO" & vbCrLf
sConsulta = sConsulta & "   , OE.COD_PROVE_ERP, OE.RECEPTOR, R.APE, R.NOM, R.TFNO , R.EMAIL" & vbCrLf
sConsulta = sConsulta & "   , ISNULL(R.UON3,ISNULL(R.UON2,R.UON1)) , ISNULL(UON3.DEN,ISNULL(UON2.DEN,UON1.DEN))" & vbCrLf
sConsulta = sConsulta & "   , OES.FECHA, ADJ.ADJUNTOS" & vbCrLf
sConsulta = sConsulta & "ORDER BY OE.PEDIDO, OE.ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT LP.PEDIDO, LP.ORDEN, LP.ID, LP.CAT1, LP.CAT2, LP.CAT3, LP.CAT4, LP.CAT5, LP.ART_INT, I.ART, I.DESCR" & vbCrLf
sConsulta = sConsulta & "   , LP.DESCR_LIBRE, PA.COD_EXT, LP.OBS, LP.OBSADJUN, LP.EST" & vbCrLf
sConsulta = sConsulta & "FROM (" & vbCrLf
sConsulta = sConsulta & "     SELECT ORDEN ID, MIN(NIVEL_APROB) NIVEL_APROB" & vbCrLf
sConsulta = sConsulta & "    FROM LINEAS_PEDIDO L " & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN SEGURIDAD S ON S.ID= L.SEGURIDAD" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN APROB_LIM AP ON AP.SEGURIDAD=L.SEGURIDAD AND AP.ID=L.APROB_LIM AND AP.NIVEL=L.NIVEL_APROB" & vbCrLf
sConsulta = sConsulta & "     WHERE L.NIVEL_APROB=0 AND S.APROB_TIPO1=@APROBADOR OR AP.PER=@APROBADOR GROUP BY ORDEN) ORDEN" & vbCrLf
sConsulta = sConsulta & "INNER JOIN LINEAS_PEDIDO LP WITH (NOLOCK) ON LP.ORDEN=ORDEN.ID" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN ITEM I WITH (NOLOCK) ON I.ANYO=LP.ANYO AND I.GMN1_PROCE=LP.GMN1 AND I.PROCE=LP.PROCE AND I.ID=LP.ITEM" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PROVE_ART4 PA WITH (NOLOCK) ON PA.PROVE=LP.PROVE AND PA.ART=CASE WHEN LP.ART_INT IS NULL THEN I.ART ELSE LP.ART_INT END" & vbCrLf
sConsulta = sConsulta & "ORDER BY LP.PEDIDO, LP.ORDEN, LP.ID" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_GET_ORDENES_FAVORITOS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_GET_ORDENES_FAVORITOS]"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE  PROCEDURE FSEP_GET_ORDENES_FAVORITOS @USU AS VARCHAR(30) , @MON AS VARCHAR(30) , @IDIOMA AS VARCHAR(20), @CARGARLINEAS AS TINYINT, @PEDIDO AS INTEGER, @PROVE AS VARCHAR(30)" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PARAM AS NVARCHAR(1000)" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING='SELECT OE.ID,OE.USU,OE.DEN,OE.ANYO,OE.PROVE PROVECOD,P.DEN PROVEDEN, OE.MON, MON.EQUIV, OE.FECHA,OE.IMPORTE,OE.OBS,OE.REFERENCIA, OE.EMPRESA, E.DEN DENEMPRESA, OE.COD_PROVE_ERP, OE.RECEPTOR ,PER.NOM + '' '' + PER.APE NOMRECEPTOR, T3.ADJUNTOS" & vbCrLf
sConsulta = sConsulta & "FROM FAVORITOS_ORDEN_ENTREGA OE WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PROVE P WITH(NOLOCK) ON OE.PROVE = P.COD" & vbCrLf
sConsulta = sConsulta & "INNER JOIN MON WITH(NOLOCK) ON OE.MON = MON.COD" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN EMP E WITH(NOLOCK) ON OE.EMPRESA = E.ID" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PER WITH(NOLOCK)  ON OE.RECEPTOR = PER.COD" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN (SELECT COUNT(*) AS ADJUNTOS,ORDEN FROM FAVORITOS_ORDEN_ENTREGA_ADJUN WITH(NOLOCK) GROUP BY FAVORITOS_ORDEN_ENTREGA_ADJUN.ORDEN) T3 ON OE.ID= T3.ORDEN" & vbCrLf
sConsulta = sConsulta & "WHERE USU=@USU'" & vbCrLf
sConsulta = sConsulta & "IF @MON IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQLSTRING=@SQLSTRING +' AND OE.MON=@MON'" & vbCrLf
sConsulta = sConsulta & "IF @PEDIDO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQLSTRING=@SQLSTRING + ' AND OE.ID=@PEDIDO'" & vbCrLf
sConsulta = sConsulta & "IF @PROVE IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "   SET @SQLSTRING=@SQLSTRING + ' AND OE.PROVE=@PROVE'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @PARAM = '@MON VARCHAR(30) , @PROVE VARCHAR(30), @PEDIDO INTEGER , @USU VARCHAR(30)'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXECUTE sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "@MON=@MON, " & vbCrLf
sConsulta = sConsulta & "@PROVE=@PROVE, " & vbCrLf
sConsulta = sConsulta & "@PEDIDO=@PEDIDO," & vbCrLf
sConsulta = sConsulta & "@USU=@USU" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CARGARLINEAS=1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET CONCAT_NULL_YIELDS_NULL OFF" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @SQLSTRING= 'SELECT DISTINCT OE.ID as ORDEN, LP.ID LINEAID, LP.USU, LP.ART_INT CODART, I.GMN1 + ART4.GMN1 GMN1, I.GMN2 + ART4.GMN2 GMN2, I.GMN3 + ART4.GMN3 GMN3, I.GMN4 + ART4.GMN4 GMN4, LP.PROVE, I.DESCR + CL.DEN DENART, PA.COD_EXT ART_EXT, LP.DEST DESTINO, LP.UP UNIDAD, LP.PREC_UP PRECIOUNITARIO, LP.CANT_PED CANTIDAD, IA.CANT_ADJ * LP.FC AS CANTADJUDICADA, LP.PREC_UP * LP.CANT_PED IMPORTE, LP.CAT1, LP.CAT2, LP.CAT3, LP.CAT4, LP.CAT5, LP.OBS, " & vbCrLf
sConsulta = sConsulta & "case when c5.cod is null then '" & vbCrLf
sConsulta = sConsulta & "   + 'case when c4.cod is null then '" & vbCrLf
sConsulta = sConsulta & "       + 'case when c3.cod is null then '" & vbCrLf
sConsulta = sConsulta & "           + 'case when c2.cod is null then '" & vbCrLf
sConsulta = sConsulta & "               + 'c1.cod '" & vbCrLf
sConsulta = sConsulta & "           + 'else '" & vbCrLf
sConsulta = sConsulta & "               + 'c2.cod '" & vbCrLf
sConsulta = sConsulta & "           + 'end '" & vbCrLf
sConsulta = sConsulta & "       + 'else '" & vbCrLf
sConsulta = sConsulta & "           + 'c3.cod '" & vbCrLf
sConsulta = sConsulta & "       + 'end '" & vbCrLf
sConsulta = sConsulta & "   + 'else '" & vbCrLf
sConsulta = sConsulta & "       + 'c4.cod '" & vbCrLf
sConsulta = sConsulta & "   + 'end '" & vbCrLf
sConsulta = sConsulta & "+ 'else '" & vbCrLf
sConsulta = sConsulta & "   + 'c5.cod " & vbCrLf
sConsulta = sConsulta & "end CATEGORIA," & vbCrLf
sConsulta = sConsulta & "case when c5.cod is null then '" & vbCrLf
sConsulta = sConsulta & "   + 'case when c4.cod is null then '" & vbCrLf
sConsulta = sConsulta & "       + 'case when c3.cod is null then '" & vbCrLf
sConsulta = sConsulta & "           + 'case when c2.cod is null then '" & vbCrLf
sConsulta = sConsulta & "               + '''('' + c1.cod + '') '' + c1.den '" & vbCrLf
sConsulta = sConsulta & "           + 'else '" & vbCrLf
sConsulta = sConsulta & "               + '''('' + c1.cod + '') '' + c1.den + '' - ('' + c2.cod + '') '' + c2.den '" & vbCrLf
sConsulta = sConsulta & "           + 'end '" & vbCrLf
sConsulta = sConsulta & "       + 'else '" & vbCrLf
sConsulta = sConsulta & "           + '''('' + c1.cod + '') '' + c1.den + '' - ('' + c2.cod + '') '' + c2.den + '' - ('' + c3.cod + '') '' + c3.den '" & vbCrLf
sConsulta = sConsulta & "       + 'end '" & vbCrLf
sConsulta = sConsulta & "   + 'else '" & vbCrLf
sConsulta = sConsulta & "       + '''('' + c1.cod + '') '' + c1.den + '' - ('' + c2.cod + '') '' + c2.den + '' - ('' + c3.cod + '') '' + c3.den + '' - ('' + c4.cod + '') '' + c4.den '" & vbCrLf
sConsulta = sConsulta & "   + 'end '" & vbCrLf
sConsulta = sConsulta & "+ 'else '" & vbCrLf
sConsulta = sConsulta & "   + '''('' + c1.cod + '') '' + c1.den + '' - ('' + c2.cod + '') '' + c2.den + '' - ('' + c3.cod + '') '' + c3.den + '' - ('' + c4.cod + '') '' + c4.den + '' - ('' + c5.cod + '') '' + c5.den " & vbCrLf
sConsulta = sConsulta & "end CATEGORIARAMA," & vbCrLf
sConsulta = sConsulta & "c1.cod C1, c2.cod C2, c3.cod C3, c4.cod C4, c5.cod C5, C1.DEN CAT1DEN, C2.DEN CAT2DEN, C3.DEN CAT3DEN, C4.DEN CAT4DEN, C5.DEN CAT5DEN, I.ANYO ANYOPROCE, I.GMN1_PROCE, I.PROCE CODPROCE, I.ID ITEM, LPA.ADJUNTOS, LP.VALOR1, LP.VALOR2, CC1.DEN AS DENCAMPO1, CC2.DEN AS DENCAMPO2, CC1.OBL AS OBL1, CC2.OBL AS OBL2, CC1.ID AS CAMPO1, CC2.ID AS CAMPO2, PROCE.DEN AS DENPROCE, LP.OBSADJUN, LP.LINEA, ISNULL(CLM.CANT_MIN,CL.CANT_MIN_DEF) CANT_MIN, ISNULL(LP.PREC_UP,CL.PRECIO_UC*CL.FC_DEF) PRECIO_UP, LP.FC, " & vbCrLf
sConsulta = sConsulta & "case when c5.cod is null then '" & vbCrLf
sConsulta = sConsulta & "   + 'case when c4.cod is null then '" & vbCrLf
sConsulta = sConsulta & "       + 'case when c3.cod is null then '" & vbCrLf
sConsulta = sConsulta & "           + 'case when c2.cod is null then '" & vbCrLf
sConsulta = sConsulta & "               + 'c1.MODIFPREC '" & vbCrLf
sConsulta = sConsulta & "           + 'else '" & vbCrLf
sConsulta = sConsulta & "               + 'c2.MODIFPREC '" & vbCrLf
sConsulta = sConsulta & "           + 'end '" & vbCrLf
sConsulta = sConsulta & "       + 'else '" & vbCrLf
sConsulta = sConsulta & "           + 'c3.MODIFPREC '" & vbCrLf
sConsulta = sConsulta & "       + 'end '" & vbCrLf
sConsulta = sConsulta & "   + 'else '" & vbCrLf
sConsulta = sConsulta & "       + 'c4.MODIFPREC '" & vbCrLf
sConsulta = sConsulta & "   + 'end '" & vbCrLf
sConsulta = sConsulta & "+ 'else '" & vbCrLf
sConsulta = sConsulta & "   + 'c5.MODIFPREC " & vbCrLf
sConsulta = sConsulta & "end MODIF_PREC, " & vbCrLf
sConsulta = sConsulta & "COALESCE(C1.PRES0, C2.PRES0, C3.PRES0, C4.PRES0, C5.PRES0) PRES0, COALESCE(C1.PRES1, C2.PRES1, C3.PRES1, C4.PRES1, C5.PRES1) PRES1 " & vbCrLf
sConsulta = sConsulta & ", COALESCE(C1.PRES2, C2.PRES2, C3.PRES2, C4.PRES2, C5.PRES2) PRES2, COALESCE(C1.PRES3, C2.PRES3, C3.PRES3, C4.PRES3, C5.PRES3) PRES3 " & vbCrLf
sConsulta = sConsulta & ", COALESCE(C1.PRES4, C2.PRES4, C3.PRES4, C4.PRES4, C5.PRES4) PRES4 , UNI.DEN_' + @IDIOMA + ' UNIDEN" & vbCrLf
sConsulta = sConsulta & "FROM FAVORITOS_LINEAS_PEDIDO LP " & vbCrLf
sConsulta = sConsulta & "LEFT JOIN ITEM I ON LP.ANYO = I.ANYO AND LP.GMN1=I.GMN1_PROCE AND LP.PROCE=I.PROCE AND LP.ITEM=I.ID " & vbCrLf
sConsulta = sConsulta & "LEFT JOIN ART4 ON ART4.COD=LP.ART_INT " & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PROVE_ART4 PA ON LP.PROVE = PA.PROVE AND LP.ART_INT = PA.ART " & vbCrLf
sConsulta = sConsulta & "LEFT JOIN ITEM_ADJ IA ON LP.ITEM = IA.ITEM AND LP.ANYO = IA.ANYO AND LP.GMN1 = IA.GMN1 AND LP.PROCE= IA.PROCE AND LP.PROVE= IA.PROVE " & vbCrLf
sConsulta = sConsulta & "LEFT JOIN CATN1 C1 ON LP.CAT1 = C1.ID " & vbCrLf
sConsulta = sConsulta & "LEFT JOIN CATN2 C2 ON LP.CAT2 = C2.ID " & vbCrLf
sConsulta = sConsulta & "LEFT JOIN CATN3 C3 ON LP.CAT3 = C3.ID " & vbCrLf
sConsulta = sConsulta & "LEFT JOIN CATN4 C4 ON LP.CAT4 = C4.ID " & vbCrLf
sConsulta = sConsulta & "LEFT JOIN CATN5 C5 ON LP.CAT5 = C5.ID " & vbCrLf
sConsulta = sConsulta & "LEFT JOIN " & vbCrLf
sConsulta = sConsulta & "(SELECT COUNT(*) AS ADJUNTOS,LINEA  FROM FAVORITOS_LINEAS_PEDIDO_ADJUN GROUP BY FAVORITOS_LINEAS_PEDIDO_ADJUN.LINEA) LPA ON LP.ID= LPA.LINEA " & vbCrLf
sConsulta = sConsulta & "LEFT JOIN CATALOG_CAMPOS AS CC1 ON CC1.ID=LP.CAMPO1 AND CC1.IDIOMA=@IDIOMA " & vbCrLf
sConsulta = sConsulta & "LEFT JOIN CATALOG_CAMPOS AS CC2 ON CC2.ID=LP.CAMPO2 AND CC2.IDIOMA=@IDIOMA " & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PROCE ON LP.ANYO=PROCE.ANYO AND LP.GMN1=PROCE.GMN1 AND LP.PROCE=PROCE.COD " & vbCrLf
sConsulta = sConsulta & "LEFT JOIN CATALOG_LIN CL ON LP.LINEA = CL.ID " & vbCrLf
sConsulta = sConsulta & "LEFT JOIN CAT_LIN_MIN CLM ON LP.LINEA = CLM.LINEA AND LP.UP = CLM.UP " & vbCrLf
sConsulta = sConsulta & "INNER JOIN FAVORITOS_ORDEN_ENTREGA OE ON OE.ID=LP.ORDEN " & vbCrLf
sConsulta = sConsulta & "INNER JOIN UNI ON LP.UP = UNI.COD" & vbCrLf
sConsulta = sConsulta & "WHERE OE.USU=@USU'" & vbCrLf
sConsulta = sConsulta & "   IF @MON IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQLSTRING=@SQLSTRING +' AND OE.MON=@MON'" & vbCrLf
sConsulta = sConsulta & "   IF @PEDIDO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQLSTRING=@SQLSTRING + ' AND OE.ID=@PEDIDO'" & vbCrLf
sConsulta = sConsulta & "   IF @PROVE IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "   SET @SQLSTRING=@SQLSTRING + ' AND OE.PROVE=@PROVE'" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   SET @PARAM = '@MON VARCHAR(30) , @PROVE VARCHAR(30)," & vbCrLf
sConsulta = sConsulta & "   @PEDIDO INTEGER , @USU VARCHAR(30) , @IDIOMA VARCHAR(30)'" & vbCrLf
sConsulta = sConsulta & "   EXECUTE sp_executesql @SQLSTRING,@PARAM," & vbCrLf
sConsulta = sConsulta & "   @MON=@MON, " & vbCrLf
sConsulta = sConsulta & "   @PROVE=@PROVE, " & vbCrLf
sConsulta = sConsulta & "   @PEDIDO=@PEDIDO," & vbCrLf
sConsulta = sConsulta & "   @USU=@USU," & vbCrLf
sConsulta = sConsulta & "   @IDIOMA=@IDIOMA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET XACT_ABORT OFF" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub
