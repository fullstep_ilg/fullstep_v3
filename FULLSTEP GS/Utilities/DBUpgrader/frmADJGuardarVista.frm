VERSION 5.00
Begin VB.Form frmADJGuardarVista 
   BackColor       =   &H00808000&
   BorderStyle     =   5  'Sizable ToolWindow
   Caption         =   "Nombre para vistas existentes"
   ClientHeight    =   2475
   ClientLeft      =   2130
   ClientTop       =   4005
   ClientWidth     =   5565
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmADJGuardarVista.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2475
   ScaleWidth      =   5565
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox txtNombreVista 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   120
      TabIndex        =   2
      Top             =   480
      Width           =   5205
   End
   Begin VB.CommandButton cmdCancelar 
      Cancel          =   -1  'True
      Caption         =   "&Cancelar"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   2805
      TabIndex        =   1
      Top             =   1950
      Width           =   1005
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   1680
      TabIndex        =   0
      Top             =   1950
      Width           =   1005
   End
   Begin VB.Label Label1 
      BackColor       =   &H00808000&
      Caption         =   $"frmADJGuardarVista.frx":0CB2
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   975
      Left            =   150
      TabIndex        =   4
      Top             =   870
      Width           =   5265
   End
   Begin VB.Label lblLabel1 
      BackColor       =   &H00808000&
      Caption         =   "Introduzca un nombre para las vistas"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   240
      Left            =   120
      TabIndex        =   3
      Top             =   135
      Width           =   5370
   End
End
Attribute VB_Name = "frmADJGuardarVista"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdAceptar_Click()

    If txtNombreVista.Text = "" Then
        MsgBox "Debe introducir un nombre para las vistas.", vbCritical, "FULLSTEP GS DB Updater"
        Exit Sub
    End If
    
    bas_V_3_0.g_sNombreVista = txtNombreVista.Text

    Unload Me
End Sub
Private Sub cmdCancelar_Click()
    
    bas_V_3_0.g_sNombreVista = ""
    
    Unload Me
End Sub



