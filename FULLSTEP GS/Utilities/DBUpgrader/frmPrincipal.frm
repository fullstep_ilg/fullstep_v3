VERSION 5.00
Begin VB.Form frmPrincipal 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FS Actualizador"
   ClientHeight    =   2700
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5565
   Icon            =   "frmPrincipal.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2700
   ScaleWidth      =   5565
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdProceder 
      Caption         =   "&Proceder"
      Default         =   -1  'True
      Height          =   435
      Left            =   1140
      TabIndex        =   8
      Top             =   2040
      Width           =   2655
   End
   Begin VB.TextBox txtPassword 
      Height          =   315
      IMEMode         =   3  'DISABLE
      Left            =   1920
      PasswordChar    =   "*"
      TabIndex        =   3
      Top             =   1440
      Width           =   2895
   End
   Begin VB.TextBox txtUsuario 
      Height          =   315
      Left            =   1920
      TabIndex        =   2
      Top             =   1000
      Width           =   2895
   End
   Begin VB.TextBox txtBaseDatos 
      Height          =   315
      Left            =   1920
      TabIndex        =   1
      Top             =   560
      Width           =   2895
   End
   Begin VB.TextBox txtServidor 
      Height          =   315
      Left            =   1920
      TabIndex        =   0
      Top             =   120
      Width           =   2895
   End
   Begin VB.Label Label4 
      Caption         =   "Password:"
      Height          =   255
      Left            =   420
      TabIndex        =   7
      Top             =   1500
      Width           =   915
   End
   Begin VB.Label Label3 
      Caption         =   "Usuario:"
      Height          =   255
      Left            =   420
      TabIndex        =   6
      Top             =   1080
      Width           =   915
   End
   Begin VB.Label Label2 
      Caption         =   "Base de Datos:"
      Height          =   255
      Left            =   420
      TabIndex        =   5
      Top             =   660
      Width           =   1215
   End
   Begin VB.Label Label1 
      Caption         =   "Servidor:"
      Height          =   255
      Left            =   420
      TabIndex        =   4
      Top             =   180
      Width           =   915
   End
End
Attribute VB_Name = "frmPrincipal"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdProceder_Click()
Dim summitconn As String
Dim adoCon As ADODB.Connection
On Error GoTo Error

    If Trim(txtServidor = "") Then
        MsgBox "El nombre del servidor no es correcto", vbInformation + vbOKOnly
        txtServidor.SetFocus
    End If
    gServer = txtServidor
    If Trim(txtBaseDatos) = "" Then
        MsgBox "El nombre de la base de datos no es correcto", vbInformation + vbOKOnly
        txtBaseDatos.SetFocus
    End If
    gBD = txtBaseDatos
    If Trim(txtUsuario) = "" Then
        MsgBox "El nombre del usuario no es correcto", vbInformation + vbOKOnly
    End If
    gUID = txtUsuario
    If Trim(txtPassword) = "" Then
        MsgBox "La clave no es correcta", vbInformation + vbOKOnly
    End If
    gPWD = txtPassword
    
    summitconn = "DRIVER={SQL Server};SERVER=" & txtServidor & ";UID=" & txtUsuario & ";PWD=" & txtPassword & ";DATABASE=" & txtBaseDatos & ";"
    
    rdoEnvironments(0).CursorDriver = rdUseOdbc
    
    Set gRDOCon = rdoEnvironments(0).OpenConnection("", rdDriverNoPrompt, , summitconn)
    Set gRDOErs = rdoEngine.rdoErrors
    
    
    Set adoCon = New ADODB.Connection
    
    adoCon.Open "Provider=SQLOLEDB.1;Server=" & txtServidor & ";Database=" & txtBaseDatos & ";", txtUsuario, txtPassword
    adoCon.CursorLocation = adUseClient
    adoCon.CommandTimeout = 120
    
    Set moConexion = New CConexion
    Set moConexion.adoCon = adoCon
    
    Set adoCon = Nothing
    
    Set oFSGSRaiz = New CRaiz
    
    Set oFSGSRaiz.Conexion = moConexion
    Me.Hide
    
    frmConfirmar.Show 1
    
    Unload Me
    Exit Sub

Error:
        
        MsgBox "Imposible acceder a la BaseDeDatos" & vbLf & "Revise los datos introducidos y vuelva a intentarlo", vbCritical + vbOKOnly
    
End Sub

Private Sub Form_Activate()
    
    txtServidor.SetFocus
    
End Sub

