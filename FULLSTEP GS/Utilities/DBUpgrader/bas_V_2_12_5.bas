Attribute VB_Name = "bas_V_2_12_5"
Option Explicit

Public Function CodigoDeActualizacion2_12_00_15A2_12_05_00() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim bBool As Boolean

On Error GoTo Error
    

sConsulta = "BEGIN TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = True
ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

frmProgreso.lblDetalle = "Modificamos tablas"
frmProgreso.lblDetalle.Refresh
frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_12_5_Tablas_0


frmProgreso.lblDetalle = "Modificamos triggers"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_12_5_Triggers_0

frmProgreso.lblDetalle = "Modificamos storeds"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_12_5_Storeds_0

frmProgreso.lblDetalle = "Actualizamos los campos de orden de atributos"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_12_5_Inicializar_Orden_Atributos

sConsulta = "UPDATE VERSION SET NUM ='2.12.05.00'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "COMMIT TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = False

CodigoDeActualizacion2_12_00_15A2_12_05_00 = True
Exit Function
    
Error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_12_00_15A2_12_05_00 = False

End Function
Public Function CodigoDeActualizacion2_12_05_00A2_12_05_01() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim bBool As Boolean

On Error GoTo Error
    

sConsulta = "BEGIN TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = True
ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

frmProgreso.lblDetalle = "Modificamos stored procedures"
frmProgreso.lblDetalle.Refresh
frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_12_5_Storeds_1

frmProgreso.lblDetalle = "Modificamos triggers en tablas"
frmProgreso.lblDetalle.Refresh
frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_12_5_Triggers_1

sConsulta = "UPDATE VERSION SET NUM ='2.12.05.01'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "COMMIT TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = False

CodigoDeActualizacion2_12_05_00A2_12_05_01 = True
Exit Function
    
Error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_12_05_00A2_12_05_01 = False

End Function


Private Sub V_2_12_5_Tablas_0()
Dim sConsulta As String

sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE dbo.PLANTILLA ADD" & vbCrLf
sConsulta = sConsulta & "   ESP varchar(800) NULL," & vbCrLf
sConsulta = sConsulta & "   CONFVISTAS tinyint NOT NULL CONSTRAINT DF_PLANTILLA_CONFVISTAS DEFAULT (0)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "ALTER TABLE dbo.PROCE ADD" & vbCrLf
sConsulta = sConsulta & "   CALCPEND tinyint NOT NULL CONSTRAINT DF_PROCE_CALCPEND DEFAULT (0)," & vbCrLf
sConsulta = sConsulta & "   CALCFEC datetime NULL," & vbCrLf
sConsulta = sConsulta & "   ADJUDICADO float(53) NULL," & vbCrLf
sConsulta = sConsulta & "   PLANTILLA int NULL," & vbCrLf
sConsulta = sConsulta & "   PLANTILLA_VISTAS tinyint NOT NULL CONSTRAINT DF_PROCE_PLANTILLA_VISTAS DEFAULT (0)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "ALTER TABLE dbo.PROCE_ATRIB ADD" & vbCrLf
sConsulta = sConsulta & "   ORDEN_ATRIB smallint NULL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "ALTER TABLE dbo.PLANTILLA_ATRIB ADD" & vbCrLf
sConsulta = sConsulta & "   ORDEN_ATRIB smallint NULL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "ALTER TABLE dbo.PROCE_GRUPO ADD" & vbCrLf
sConsulta = sConsulta & "   ADJUDICADO float(53) NULL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "ALTER TABLE dbo.ITEM_ADJ ADD" & vbCrLf
sConsulta = sConsulta & "   ADJUDICADO float(53) NULL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "ALTER TABLE dbo.PARGEN_DEF ADD" & vbCrLf
sConsulta = sConsulta & "   FEC_ACT_AHORROS datetime NULL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "ALTER TABLE dbo.PROCE_DEF ADD" & vbCrLf
sConsulta = sConsulta & "   PUBLICARFINSUM tinyint NOT NULL CONSTRAINT DF_PROCE_DEF_PUBLICARFINSUM DEFAULT (1)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PLANTILLA_CONF_VISTAS_ALL_TG_INSUPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PLANTILLA_CONF_VISTAS_ALL_TG_INSUPD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PLANTILLA_CONF_VISTAS_ALL_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PLANTILLA_CONF_VISTAS_ALL_TG_INS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PLANTILLA_CONF_VISTAS_GRUPO_TG_INSUPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PLANTILLA_CONF_VISTAS_GRUPO_TG_INSUPD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PLANTILLA_CONF_VISTAS_GRUPO_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PLANTILLA_CONF_VISTAS_GRUPO_TG_INS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PLANTILLA_CONF_VISTAS_PROCE_TG_INSUPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PLANTILLA_CONF_VISTAS_PROCE_TG_INSUPD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PLANTILLA_CONF_VISTAS_PROCE_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PLANTILLA_CONF_VISTAS_PROCE_TG_INS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PLANTILLA_PROCE_ESP_TG_INSUPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PLANTILLA_PROCE_ESP_TG_INSUPD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PLANTILLA_CONF_VISTAS_ALL]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)" & vbCrLf
sConsulta = sConsulta & "drop table [dbo].[PLANTILLA_CONF_VISTAS_ALL]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PLANTILLA_CONF_VISTAS_ALL_ATRIB]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)" & vbCrLf
sConsulta = sConsulta & "drop table [dbo].[PLANTILLA_CONF_VISTAS_ALL_ATRIB]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PLANTILLA_CONF_VISTAS_GRUPO]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)" & vbCrLf
sConsulta = sConsulta & "drop table [dbo].[PLANTILLA_CONF_VISTAS_GRUPO]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PLANTILLA_CONF_VISTAS_GRUPO_ATRIB]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)" & vbCrLf
sConsulta = sConsulta & "drop table [dbo].[PLANTILLA_CONF_VISTAS_GRUPO_ATRIB]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PLANTILLA_CONF_VISTAS_PROCE]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)" & vbCrLf
sConsulta = sConsulta & "drop table [dbo].[PLANTILLA_CONF_VISTAS_PROCE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PLANTILLA_CONF_VISTAS_PROCE_ATRIB]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)" & vbCrLf
sConsulta = sConsulta & "drop table [dbo].[PLANTILLA_CONF_VISTAS_PROCE_ATRIB]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PLANTILLA_CONF_VISTAS_PROCE_SOBRE]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)" & vbCrLf
sConsulta = sConsulta & "drop table [dbo].[PLANTILLA_CONF_VISTAS_PROCE_SOBRE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PLANTILLA_PROCE_ESP]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)" & vbCrLf
sConsulta = sConsulta & "drop table [dbo].[PLANTILLA_PROCE_ESP]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE [dbo].[PLANTILLA_CONF_VISTAS_ALL] (" & vbCrLf
sConsulta = sConsulta & "   [PLANT] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [VISTA] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [DESCR_WIDTH] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [ANYO_POS] [smallint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [ANYO_VISIBLE] [tinyint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [ANYO_WIDTH] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [ANYO_LEVEL] [tinyint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PROV_POS] [smallint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PROV_VISIBLE] [tinyint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PROV_WIDTH] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PROV_LEVEL] [tinyint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PRESUNI_POS] [smallint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PRESUNI_VISIBLE] [tinyint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PRESUNI_WIDTH] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PRESUNI_LEVEL] [tinyint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [OBJ_POS] [smallint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [OBJ_VISIBLE] [tinyint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [OBJ_WIDTH] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [OBJ_LEVEL] [tinyint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [ADJ_POS] [smallint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [ADJ_VISIBLE] [tinyint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [ADJ_WIDTH] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [ADJ_LEVEL] [tinyint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [CANT_POS] [smallint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [CANT_VISIBLE] [tinyint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [CANT_WIDTH] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [CANT_LEVEL] [tinyint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [IMP_POS] [smallint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [IMP_VISIBLE] [tinyint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [IMP_WIDTH] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [IMP_LEVEL] [tinyint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [AHORRO_IMP_POS] [smallint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [AHORRO_IMP_VISIBLE] [tinyint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [AHORRO_IMP_WIDTH] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [AHORRO_IMP_LEVEL] [tinyint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [AHORRO_PORCEN_POS] [smallint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [AHORRO_PORCEN_VISIBLE] [tinyint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [AHORRO_PORCEN_WIDTH] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [AHORRO_PORCEN_LEVEL] [tinyint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [OCULTAR_FILA2] [tinyint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [DEFECTO] [tinyint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [GRUPO0_WIDTH] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [GRUPOS_WIDTH] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [IMP_ADJ_VISIBLE] [tinyint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [IMP_ADJ_WIDTH] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [ANCHO_FILA] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PRECIO_PROV_POS] [smallint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PRECIO_PROV_VISIBLE] [tinyint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PRECIO_PROV_WIDTH] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PRECIO_PROV_LEVEL] [tinyint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [CANT_PROV_POS] [smallint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [CANT_PROV_VISIBLE] [tinyint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [CANT_PROV_WIDTH] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [CANT_PROV_LEVEL] [tinyint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [ADJ_PROV_POS] [smallint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [ADJ_PROV_VISIBLE] [tinyint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [ADJ_PROV_WIDTH] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [ADJ_PROV_LEVEL] [tinyint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [IMP_ADJ_POS] [smallint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [IMP_ADJ_LEVEL] [tinyint] NOT NULL " & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE [dbo].[PLANTILLA_CONF_VISTAS_ALL_ATRIB] (" & vbCrLf
sConsulta = sConsulta & "   [PLANT] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [VISTA] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [ATRIB] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [VISIBLE] [tinyint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [WIDTH] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [POSICION] [smallint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [FILA] [tinyint] NOT NULL " & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE [dbo].[PLANTILLA_CONF_VISTAS_GRUPO] (" & vbCrLf
sConsulta = sConsulta & "   [PLANT] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [GRUPO] [varchar] (3) NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [VISTA] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [DESCR_WIDTH] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [ANYO_POS] [smallint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [ANYO_VISIBLE] [tinyint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [ANYO_WIDTH] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [ANYO_LEVEL] [tinyint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PROV_POS] [smallint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PROV_VISIBLE] [tinyint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PROV_WIDTH] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PROV_LEVEL] [tinyint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PRESUNI_POS] [smallint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PRESUNI_VISIBLE] [tinyint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PRESUNI_WIDTH] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PRESUNI_LEVEL] [tinyint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [OBJ_POS] [smallint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [OBJ_VISIBLE] [tinyint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [OBJ_WIDTH] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [OBJ_LEVEL] [tinyint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [ADJ_POS] [smallint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [ADJ_VISIBLE] [tinyint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [ADJ_WIDTH] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [ADJ_LEVEL] [tinyint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [CANT_POS] [smallint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [CANT_VISIBLE] [tinyint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [CANT_WIDTH] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [CANT_LEVEL] [tinyint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [IMP_POS] [smallint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [IMP_VISIBLE] [tinyint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [IMP_WIDTH] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [IMP_LEVEL] [tinyint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [AHORRO_IMP_POS] [smallint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [AHORRO_IMP_VISIBLE] [tinyint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [AHORRO_IMP_WIDTH] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [AHORRO_IMP_LEVEL] [tinyint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [AHORRO_PORCEN_POS] [smallint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [AHORRO_PORCEN_VISIBLE] [tinyint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [AHORRO_PORCEN_WIDTH] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [AHORRO_PORCEN_LEVEL] [tinyint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [OCULTAR_FILA2] [tinyint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [DEFECTO] [tinyint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [GRUPO0_WIDTH] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [GRUPOS_WIDTH] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [IMP_ADJ_VISIBLE] [tinyint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [IMP_ADJ_WIDTH] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [ANCHO_FILA] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PRECIO_PROV_POS] [smallint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PRECIO_PROV_VISIBLE] [tinyint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PRECIO_PROV_WIDTH] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PRECIO_PROV_LEVEL] [tinyint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [CANT_PROV_POS] [smallint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [CANT_PROV_VISIBLE] [tinyint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [CANT_PROV_WIDTH] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [CANT_PROV_LEVEL] [tinyint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [ADJ_PROV_POS] [smallint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [ADJ_PROV_VISIBLE] [tinyint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [ADJ_PROV_WIDTH] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [ADJ_PROV_LEVEL] [tinyint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [IMP_ADJ_POS] [smallint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [IMP_ADJ_LEVEL] [tinyint] NOT NULL " & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE [dbo].[PLANTILLA_CONF_VISTAS_GRUPO_ATRIB] (" & vbCrLf
sConsulta = sConsulta & "   [PLANT] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [GRUPO] [varchar] (3) NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [VISTA] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [ATRIB] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [VISIBLE] [tinyint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [WIDTH] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [POSICION] [smallint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [FILA] [tinyint] NOT NULL " & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE [dbo].[PLANTILLA_CONF_VISTAS_PROCE] (" & vbCrLf
sConsulta = sConsulta & "   [PLANT] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [VISTA] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [CONSUMIDO_POS] [smallint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [CONSUMIDO_VISIBLE] [tinyint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [CONSUMIDO_WIDTH] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [ADJ_POS] [smallint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [ADJ_VISIBLE] [tinyint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [ADJ_WIDTH] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [AHORR_ADJ_POS] [smallint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [AHORR_ADJ_VISIBLE] [tinyint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [AHORR_ADJ_WIDTH] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [AHORR_ADJ_PORCEN_POS] [smallint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [AHORR_ADJ_PORCEN_VISIBLE] [tinyint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [AHORR_ADJ_PORCEN_WIDTH] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [IMP_POS] [smallint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [IMP_VISIBLE] [tinyint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [IMP_WIDTH] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [AHORR_OFE_POS] [smallint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [AHORR_OFE_VISIBLE] [tinyint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [AHORR_OFE_WIDTH] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [AHORR_OFE_PORCEN_POS] [smallint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [AHORR_OFE_PORCEN_VISIBLE] [tinyint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [AHORR_OFE_PORCEN_WIDTH] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [TIPOVISION] [tinyint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [DEFECTO] [tinyint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [GRUPO_WIDTH] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [ANCHO_FILA] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [GRUPO0_WIDTH] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [COD_WIDTH] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "   [DEN_WIDTH] [float] NULL " & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE [dbo].[PLANTILLA_CONF_VISTAS_PROCE_ATRIB] (" & vbCrLf
sConsulta = sConsulta & "   [PLANT] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [VISTA] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [ATRIB] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [VISIBLE] [tinyint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [POS] [smallint] NULL ," & vbCrLf
sConsulta = sConsulta & "   [WIDTH] [float] NULL " & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE [dbo].[PLANTILLA_CONF_VISTAS_PROCE_SOBRE] (" & vbCrLf
sConsulta = sConsulta & "   [PLANT] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [VISTA] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [SOBRE] [smallint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [VISIBLE] [tinyint] NULL " & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE [dbo].[PLANTILLA_PROCE_ESP] (" & vbCrLf
sConsulta = sConsulta & "   [ID] [smallint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [PLANT] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [NOM] [varchar] (300) NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [COM] [varchar] (500) NULL ," & vbCrLf
sConsulta = sConsulta & "   [DATA] [image] NULL ," & vbCrLf
sConsulta = sConsulta & "   [FECACT] [datetime] NULL " & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[PLANTILLA_CONF_VISTAS_ALL] WITH NOCHECK ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [PK_PLANTILLA_CONF_VISTAS_ALL] PRIMARY KEY  CLUSTERED " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [PLANT]," & vbCrLf
sConsulta = sConsulta & "       [VISTA]" & vbCrLf
sConsulta = sConsulta & "   )  ON [PRIMARY] " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[PLANTILLA_CONF_VISTAS_ALL_ATRIB] WITH NOCHECK ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [PK_PLANTILLA_CONF_VISTAS_ALL_ATRIB] PRIMARY KEY  CLUSTERED " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [PLANT]," & vbCrLf
sConsulta = sConsulta & "       [VISTA]," & vbCrLf
sConsulta = sConsulta & "       [ATRIB]" & vbCrLf
sConsulta = sConsulta & "   )  ON [PRIMARY] " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[PLANTILLA_CONF_VISTAS_GRUPO] WITH NOCHECK ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [PK_PLANTILLA_CONF_VISTAS_GRUPO] PRIMARY KEY  CLUSTERED " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [PLANT]," & vbCrLf
sConsulta = sConsulta & "       [GRUPO]," & vbCrLf
sConsulta = sConsulta & "       [VISTA]" & vbCrLf
sConsulta = sConsulta & "   )  ON [PRIMARY] " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[PLANTILLA_CONF_VISTAS_GRUPO_ATRIB] WITH NOCHECK ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [PK_PLANTILLA_CONF_VISTAS_GRUPO_ATRIB] PRIMARY KEY  CLUSTERED " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [PLANT]," & vbCrLf
sConsulta = sConsulta & "       [GRUPO]," & vbCrLf
sConsulta = sConsulta & "       [VISTA]," & vbCrLf
sConsulta = sConsulta & "       [ATRIB]" & vbCrLf
sConsulta = sConsulta & "   )  ON [PRIMARY] " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[PLANTILLA_CONF_VISTAS_PROCE] WITH NOCHECK ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [PK_PLANTILLA_CONF_VISTAS_PROCE] PRIMARY KEY  CLUSTERED " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [PLANT]," & vbCrLf
sConsulta = sConsulta & "       [VISTA]" & vbCrLf
sConsulta = sConsulta & "   )  ON [PRIMARY] " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[PLANTILLA_CONF_VISTAS_PROCE_ATRIB] WITH NOCHECK ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [PK_PLANTILLA_CONF_VISTAS_PROCE_ATRIB] PRIMARY KEY  CLUSTERED " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [PLANT]," & vbCrLf
sConsulta = sConsulta & "       [VISTA]," & vbCrLf
sConsulta = sConsulta & "       [ATRIB]" & vbCrLf
sConsulta = sConsulta & "   )  ON [PRIMARY] " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[PLANTILLA_CONF_VISTAS_PROCE_SOBRE] WITH NOCHECK ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [PK_PLANTILLA_CONF_VISTAS_PROCE_SOBRE] PRIMARY KEY  CLUSTERED " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [PLANT]," & vbCrLf
sConsulta = sConsulta & "       [VISTA]," & vbCrLf
sConsulta = sConsulta & "       [SOBRE]" & vbCrLf
sConsulta = sConsulta & "   )  ON [PRIMARY] " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[PLANTILLA_PROCE_ESP] WITH NOCHECK ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [PK_PLANTILLA_PROCE_ESP] PRIMARY KEY  CLUSTERED " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [ID]," & vbCrLf
sConsulta = sConsulta & "       [PLANT]" & vbCrLf
sConsulta = sConsulta & "   )  ON [PRIMARY] " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[PLANTILLA_CONF_VISTAS_ALL] ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PLANTILLA_CONF_VISTAS_ALL_ANYO_VISIBLE] DEFAULT (1) FOR [ANYO_VISIBLE]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PLANTILLA_CONF_VISTAS_ALL_PROV_VISIBLE] DEFAULT (1) FOR [PROV_VISIBLE]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PLANTILLA_CONF_VISTAS_ALL_PRESUNI_VISIBLE] DEFAULT (1) FOR [PRESUNI_VISIBLE]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PLANTILLA_CONF_VISTAS_ALL_OBJ_VISIBLE] DEFAULT (1) FOR [OBJ_VISIBLE]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PLANTILLA_CONF_VISTAS_ALL_ADJ_VISIBLE] DEFAULT (1) FOR [ADJ_VISIBLE]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PLANTILLA_CONF_VISTAS_ALL_CANT_VISIBLE] DEFAULT (1) FOR [CANT_VISIBLE]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PLANTILLA_CONF_VISTAS_ALL_IMP_VISIBLE] DEFAULT (1) FOR [IMP_VISIBLE]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PLANTILLA_CONF_VISTAS_ALL_AHORRO_IMP_VISIBLE] DEFAULT (1) FOR [AHORRO_IMP_VISIBLE]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PLANTILLA_CONF_VISTAS_ALL_AHORRO_PORCEN_VISIBLE] DEFAULT (1) FOR [AHORRO_PORCEN_VISIBLE]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PLANTILLA_CONF_VISTAS_ALL_OCULTAR_FILA2] DEFAULT (0) FOR [OCULTAR_FILA2]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PLANTILLA_CONF_VISTAS_ALL_DEFECTO] DEFAULT (0) FOR [DEFECTO]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PLANTILLA_CONF_VISTAS_ALL_IMP_ADJ_VISIBLE] DEFAULT (0) FOR [IMP_ADJ_VISIBLE]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PLANTILLA_CONF_VISTAS_ALL_PRECIO_PROV_VISIBLE] DEFAULT (1) FOR [PRECIO_PROV_VISIBLE]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PLANTILLA_CONF_VISTAS_ALL_PRECIO_PROV_LEVEL] DEFAULT (0) FOR [PRECIO_PROV_LEVEL]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PLANTILLA_CONF_VISTAS_ALL_CANT_PROV_VISIBLE] DEFAULT (1) FOR [CANT_PROV_VISIBLE]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PLANTILLA_CONF_VISTAS_ALL_CANT_PROV_LEVEL] DEFAULT (1) FOR [CANT_PROV_LEVEL]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PLANTILLA_CONF_VISTAS_ALL_ADJ_PROV_VISIBLE] DEFAULT (1) FOR [ADJ_PROV_VISIBLE]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PLANTILLA_CONF_VISTAS_ALL_ADJ_PROV_LEVEL] DEFAULT (1) FOR [ADJ_PROV_LEVEL]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PLANTILLA_CONF_VISTAS_ALL_IMP_ADJ_LEVEL] DEFAULT (0) FOR [IMP_ADJ_LEVEL]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[PLANTILLA_CONF_VISTAS_ALL_ATRIB] ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PLANTILLA_CONF_VISTAS_ALL_ATRIB_VISIBLE] DEFAULT (0) FOR [VISIBLE]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PLANTILLA_CONF_VISTAS_ALL_ATRIB_FILA] DEFAULT (0) FOR [FILA]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[PLANTILLA_CONF_VISTAS_GRUPO] ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PLANTILLA_CONF_VISTAS_GRUPO_ANYO_VISIBLE] DEFAULT (1) FOR [ANYO_VISIBLE]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PLANTILLA_CONF_VISTAS_GRUPO_PROV_VISIBLE] DEFAULT (1) FOR [PROV_VISIBLE]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PLANTILLA_CONF_VISTAS_GRUPO_PRESUNI_VISIBLE] DEFAULT (1) FOR [PRESUNI_VISIBLE]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PLANTILLA_CONF_VISTAS_GRUPO_OBJ_VISIBLE] DEFAULT (1) FOR [OBJ_VISIBLE]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PLANTILLA_CONF_VISTAS_GRUPO_ADJ_VISIBLE] DEFAULT (1) FOR [ADJ_VISIBLE]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PLANTILLA_CONF_VISTAS_GRUPO_CANT_VISIBLE] DEFAULT (1) FOR [CANT_VISIBLE]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PLANTILLA_CONF_VISTAS_GRUPO_IMP_VISIBLE] DEFAULT (1) FOR [IMP_VISIBLE]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PLANTILLA_CONF_VISTAS_GRUPO_AHORRO_IMP_VISIBLE] DEFAULT (1) FOR [AHORRO_IMP_VISIBLE]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PLANTILLA_CONF_VISTAS_GRUPO_AHORRO_PORCEN_VISIBLE] DEFAULT (1) FOR [AHORRO_PORCEN_VISIBLE]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PLANTILLA_CONF_VISTAS_GRUPO_OCULTAR_FILA2] DEFAULT (0) FOR [OCULTAR_FILA2]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PLANTILLA_CONF_VISTAS_GRUPO_DEFECTO] DEFAULT (0) FOR [DEFECTO]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PLANTILLA_CONF_VISTAS_GRUPO_IMP_ADJ_VISIBLE] DEFAULT (0) FOR [IMP_ADJ_VISIBLE]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PLANTILLA_CONF_VISTAS_GRUPO_PRECIO_PROV_VISIBLE] DEFAULT (1) FOR [PRECIO_PROV_VISIBLE]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PLANTILLA_CONF_VISTAS_GRUPO_PRECIO_PROV_LEVEL] DEFAULT (0) FOR [PRECIO_PROV_LEVEL]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PLANTILLA_CONF_VISTAS_GRUPO_CANT_PROV_VISIBLE] DEFAULT (1) FOR [CANT_PROV_VISIBLE]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PLANTILLA_CONF_VISTAS_GRUPO_CANT_PROV_LEVEL] DEFAULT (1) FOR [CANT_PROV_LEVEL]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PLANTILLA_CONF_VISTAS_GRUPO_ADJ_PROV_VISIBLE] DEFAULT (1) FOR [ADJ_PROV_VISIBLE]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PLANTILLA_CONF_VISTAS_GRUPO_ADJ_PROV_LEVEL] DEFAULT (1) FOR [ADJ_PROV_LEVEL]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PLANTILLA_CONF_VISTAS_GRUPO_IMP_ADJ_LEVEL] DEFAULT (0) FOR [IMP_ADJ_LEVEL]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[PLANTILLA_CONF_VISTAS_GRUPO_ATRIB] ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PLANTILLA_CONF_VISTAS_GRUPO_ATRIB_VISIBLE] DEFAULT (0) FOR [VISIBLE]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PLANTILLA_CONF_VISTAS_GRUPO_ATRIB_FILA] DEFAULT (0) FOR [FILA]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[PLANTILLA_CONF_VISTAS_PROCE] ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PLANTILLA_CONF_VISTAS_PROCE_CONSUMIDO_VISIBLE] DEFAULT (1) FOR [CONSUMIDO_VISIBLE]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PLANTILLA_CONF_VISTAS_PROCE_ADJ_VISIBLE] DEFAULT (1) FOR [ADJ_VISIBLE]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PLANTILLA_CONF_VISTAS_PROCE_AHORR_ADJ_VISIBLE] DEFAULT (1) FOR [AHORR_ADJ_VISIBLE]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PLANTILLA_CONF_VISTAS_PROCE_AHORR_ADJ_PORCEN_VISIBLE] DEFAULT (0) FOR [AHORR_ADJ_PORCEN_VISIBLE]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PLANTILLA_CONF_VISTAS_PROCE_IMP_VISIBLE] DEFAULT (0) FOR [IMP_VISIBLE]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PLANTILLA_CONF_VISTAS_PROCE_AHORR_OFE_VISIBLE] DEFAULT (0) FOR [AHORR_OFE_VISIBLE]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PLANTILLA_CONF_VISTAS_PROCE_AHORR_OFE_PORCEN_VISIBLE] DEFAULT (0) FOR [AHORR_OFE_PORCEN_VISIBLE]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PLANTILLA_CONF_VISTAS_PROCE_TIPOVISION] DEFAULT (0) FOR [TIPOVISION]," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PLANTILLA_CONF_VISTAS_PROCE_DEFECTO] DEFAULT (0) FOR [DEFECTO]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[PLANTILLA_CONF_VISTAS_PROCE_ATRIB] ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PLANTILLA_CONF_VISTAS_PROCE_ATRIB_VISIBLE] DEFAULT (0) FOR [VISIBLE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[PLANTILLA_CONF_VISTAS_PROCE_SOBRE] ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PLANTILLA_CONF_VISTAS_PROCE_SOBRE_VISIBLE] DEFAULT (0) FOR [VISIBLE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[PLANTILLA_CONF_VISTAS_ALL] ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [FK_PLANTILLA_CONF_VISTAS_ALL_PLANTILLA] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [PLANT]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[PLANTILLA] (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[PLANTILLA_CONF_VISTAS_ALL_ATRIB] ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [FK_PLANTILLA_CONF_VISTAS_ALL_ATRIB_PLANTILLA] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [PLANT]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[PLANTILLA] (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [FK_PLANTILLA_CONF_VISTAS_ALL_ATRIB_PLANTILLA_ATRIB] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [ATRIB]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[PLANTILLA_ATRIB] (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[PLANTILLA_CONF_VISTAS_GRUPO] ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [FK_PLANTILLA_CONF_VISTAS_GRUPO_PLANTILLA_GR] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [PLANT]," & vbCrLf
sConsulta = sConsulta & "       [GRUPO]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[PLANTILLA_GR] (" & vbCrLf
sConsulta = sConsulta & "       [PLANT]," & vbCrLf
sConsulta = sConsulta & "       [COD]" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[PLANTILLA_CONF_VISTAS_GRUPO_ATRIB] ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [FK_PLANTILLA_CONF_VISTAS_GRUPO_ATRIB_PLANTILLA_ATRIB] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [ATRIB]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[PLANTILLA_ATRIB] (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [FK_PLANTILLA_CONF_VISTAS_GRUPO_ATRIB_PLANTILLA_GR] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [PLANT]," & vbCrLf
sConsulta = sConsulta & "       [GRUPO]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[PLANTILLA_GR] (" & vbCrLf
sConsulta = sConsulta & "       [PLANT]," & vbCrLf
sConsulta = sConsulta & "       [COD]" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[PLANTILLA_CONF_VISTAS_PROCE] ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [FK_PLANTILLA_CONF_VISTAS_PROCE_PLANTILLA] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [PLANT]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[PLANTILLA] (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[PLANTILLA_CONF_VISTAS_PROCE_ATRIB] ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [FK_PLANTILLA_CONF_VISTAS_PROCE_ATRIB_PLANTILLA] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [PLANT]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[PLANTILLA] (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [FK_PLANTILLA_CONF_VISTAS_PROCE_ATRIB_PLANTILLA_ATRIB] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [ATRIB]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[PLANTILLA_ATRIB] (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[PLANTILLA_CONF_VISTAS_PROCE_SOBRE] ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [FK_PLANTILLA_CONF_VISTAS_PROCE_SOBRE_PLANTILLA] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [PLANT]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[PLANTILLA] (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[PLANTILLA_PROCE_ESP] ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [FK_PLANTILLA_PROCE_ESP_PLANTILLA] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [PLANT]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[PLANTILLA] (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Private Sub V_2_12_5_Triggers_0()
Dim sConsulta As String

sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER PLANTILLA_CONF_VISTAS_ALL_TG_INSUPD ON dbo.PLANTILLA_CONF_VISTAS_ALL " & vbCrLf
sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @PLANT INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @VISTA INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @DEFECTO INT" & vbCrLf
sConsulta = sConsulta & "IF UPDATE(DEFECTO)" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & " SET @DEFECTO=(SELECT DEFECTO FROM INSERTED)" & vbCrLf
sConsulta = sConsulta & " IF @DEFECTO=1" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "     DECLARE curTG_PLANTILLA_CONF_VISTAS_ALL_Ins CURSOR FOR SELECT PLANT,VISTA FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "     OPEN curTG_PLANTILLA_CONF_VISTAS_ALL_Ins" & vbCrLf
sConsulta = sConsulta & "     FETCH NEXT FROM curTG_PLANTILLA_CONF_VISTAS_ALL_Ins INTO @PLANT,@VISTA" & vbCrLf
sConsulta = sConsulta & "     WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "          BEGIN" & vbCrLf
sConsulta = sConsulta & "     UPDATE PLANTILLA_CONF_VISTAS_ALL SET DEFECTO=0 WHERE PLANT=@PLANT   AND VISTA<>@VISTA" & vbCrLf
sConsulta = sConsulta & "     FETCH NEXT FROM curTG_PLANTILLA_CONF_VISTAS_ALL_Ins INTO @PLANT,@VISTA" & vbCrLf
sConsulta = sConsulta & "          END" & vbCrLf
sConsulta = sConsulta & "     Close curTG_PLANTILLA_CONF_VISTAS_ALL_Ins" & vbCrLf
sConsulta = sConsulta & "     DEALLOCATE curTG_PLANTILLA_CONF_VISTAS_ALL_Ins" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER PLANTILLA_CONF_VISTAS_ALL_TG_INS ON dbo.PLANTILLA_CONF_VISTAS_ALL " & vbCrLf
sConsulta = sConsulta & "FOR INSERT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @PLANT INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @CONFVISTAS INT" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "     DECLARE curTG_PLANTILLA_CONF_VISTAS_ALL_Ins CURSOR FOR SELECT PLANT FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "     OPEN curTG_PLANTILLA_CONF_VISTAS_ALL_Ins" & vbCrLf
sConsulta = sConsulta & "     FETCH NEXT FROM curTG_PLANTILLA_CONF_VISTAS_ALL_Ins INTO @PLANT" & vbCrLf
sConsulta = sConsulta & "     WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "          BEGIN" & vbCrLf
sConsulta = sConsulta & "   SELECT @CONFVISTAS= CONFVISTAS FROM PLANTILLA WHERE ID=@PLANT" & vbCrLf
sConsulta = sConsulta & "   IF @CONFVISTAS=0" & vbCrLf
sConsulta = sConsulta & "           UPDATE PLANTILLA SET CONFVISTAS=1 WHERE ID=@PLANT   " & vbCrLf
sConsulta = sConsulta & "       FETCH NEXT FROM curTG_PLANTILLA_CONF_VISTAS_ALL_Ins INTO @PLANT" & vbCrLf
sConsulta = sConsulta & "          END" & vbCrLf
sConsulta = sConsulta & "     Close curTG_PLANTILLA_CONF_VISTAS_ALL_Ins" & vbCrLf
sConsulta = sConsulta & "     DEALLOCATE curTG_PLANTILLA_CONF_VISTAS_ALL_Ins" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER PLANTILLA_CONF_VISTAS_GRUPO_TG_INSUPD ON dbo.PLANTILLA_CONF_VISTAS_GRUPO " & vbCrLf
sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @PLANT INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @GRUPO VARCHAR(10)" & vbCrLf
sConsulta = sConsulta & "DECLARE @VISTA INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @DEFECTO INT" & vbCrLf
sConsulta = sConsulta & "IF UPDATE(DEFECTO)" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & " SET @DEFECTO=(SELECT DEFECTO FROM INSERTED)" & vbCrLf
sConsulta = sConsulta & " IF @DEFECTO=1" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "     DECLARE curTG_PLANTILLA_CONF_VISTAS_GRUPO_Ins CURSOR FOR SELECT PLANT,GRUPO,VISTA FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "     OPEN curTG_PLANTILLA_CONF_VISTAS_GRUPO_Ins" & vbCrLf
sConsulta = sConsulta & "     FETCH NEXT FROM curTG_PLANTILLA_CONF_VISTAS_GRUPO_Ins INTO @PLANT,@GRUPO,@VISTA" & vbCrLf
sConsulta = sConsulta & "     WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "          BEGIN" & vbCrLf
sConsulta = sConsulta & "     UPDATE PLANTILLA_CONF_VISTAS_GRUPO SET DEFECTO=0 WHERE PLANT=@PLANT AND GRUPO=@GRUPO  AND VISTA<>@VISTA" & vbCrLf
sConsulta = sConsulta & "     FETCH NEXT FROM curTG_PLANTILLA_CONF_VISTAS_GRUPO_Ins INTO @PLANT,@GRUPO,@VISTA" & vbCrLf
sConsulta = sConsulta & "          END" & vbCrLf
sConsulta = sConsulta & "     Close curTG_PLANTILLA_CONF_VISTAS_GRUPO_Ins" & vbCrLf
sConsulta = sConsulta & "     DEALLOCATE curTG_PLANTILLA_CONF_VISTAS_GRUPO_Ins" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER PLANTILLA_CONF_VISTAS_GRUPO_TG_INS ON [dbo].[PLANTILLA_CONF_VISTAS_GRUPO] " & vbCrLf
sConsulta = sConsulta & "FOR INSERT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @PLANT INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @CONFVISTAS INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "     DECLARE curTG_PLANTILLA_CONF_VISTAS_GRUPO_Ins CURSOR FOR SELECT PLANT FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "     OPEN curTG_PLANTILLA_CONF_VISTAS_GRUPO_Ins" & vbCrLf
sConsulta = sConsulta & "     FETCH NEXT FROM curTG_PLANTILLA_CONF_VISTAS_GRUPO_Ins INTO @PLANT" & vbCrLf
sConsulta = sConsulta & "     WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "          BEGIN" & vbCrLf
sConsulta = sConsulta & "   SELECT @CONFVISTAS= CONFVISTAS FROM PLANTILLA WHERE ID=@PLANT" & vbCrLf
sConsulta = sConsulta & "   IF @CONFVISTAS=0" & vbCrLf
sConsulta = sConsulta & "           UPDATE PLANTILLA SET CONFVISTAS=1 WHERE ID=@PLANT   " & vbCrLf
sConsulta = sConsulta & "       FETCH NEXT FROM curTG_PLANTILLA_CONF_VISTAS_GRUPO_Ins INTO @PLANT" & vbCrLf
sConsulta = sConsulta & "          END" & vbCrLf
sConsulta = sConsulta & "     Close curTG_PLANTILLA_CONF_VISTAS_GRUPO_Ins" & vbCrLf
sConsulta = sConsulta & "     DEALLOCATE curTG_PLANTILLA_CONF_VISTAS_GRUPO_Ins" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER PLANTILLA_CONF_VISTAS_PROCE_TG_INSUPD ON [dbo].[PLANTILLA_CONF_VISTAS_PROCE] " & vbCrLf
sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @PLANT INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @VISTA INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @DEFECTO INT" & vbCrLf
sConsulta = sConsulta & "IF UPDATE(DEFECTO)" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & " SET @DEFECTO=(SELECT DEFECTO FROM INSERTED)" & vbCrLf
sConsulta = sConsulta & " IF @DEFECTO=1" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "     DECLARE curTG_PLANTILLA_CONF_VISTAS_PROCE_Ins CURSOR FOR SELECT PLANT,VISTA FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "     OPEN curTG_PLANTILLA_CONF_VISTAS_PROCE_Ins" & vbCrLf
sConsulta = sConsulta & "     FETCH NEXT FROM curTG_PLANTILLA_CONF_VISTAS_PROCE_Ins INTO @PLANT,@VISTA" & vbCrLf
sConsulta = sConsulta & "     WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "          BEGIN" & vbCrLf
sConsulta = sConsulta & "     UPDATE PLANTILLA_CONF_VISTAS_PROCE SET DEFECTO=0 WHERE PLANT=@PLANT AND VISTA<>@VISTA" & vbCrLf
sConsulta = sConsulta & "     FETCH NEXT FROM curTG_PLANTILLA_CONF_VISTAS_PROCE_Ins INTO @PLANT,@VISTA" & vbCrLf
sConsulta = sConsulta & "          END" & vbCrLf
sConsulta = sConsulta & "     Close curTG_PLANTILLA_CONF_VISTAS_PROCE_Ins" & vbCrLf
sConsulta = sConsulta & "     DEALLOCATE curTG_PLANTILLA_CONF_VISTAS_PROCE_Ins" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER PLANTILLA_CONF_VISTAS_PROCE_TG_INS ON [dbo].[PLANTILLA_CONF_VISTAS_PROCE] " & vbCrLf
sConsulta = sConsulta & "FOR INSERT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @PLANT INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @CONFVISTAS INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "     DECLARE curTG_PLANTILLA_CONF_VISTAS_PROCE_Ins CURSOR FOR SELECT PLANT FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "     OPEN curTG_PLANTILLA_CONF_VISTAS_PROCE_Ins" & vbCrLf
sConsulta = sConsulta & "     FETCH NEXT FROM curTG_PLANTILLA_CONF_VISTAS_PROCE_Ins INTO @PLANT" & vbCrLf
sConsulta = sConsulta & "     WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "          BEGIN" & vbCrLf
sConsulta = sConsulta & "   SELECT @CONFVISTAS= CONFVISTAS FROM PLANTILLA WHERE ID=@PLANT" & vbCrLf
sConsulta = sConsulta & "   IF @CONFVISTAS=0" & vbCrLf
sConsulta = sConsulta & "           UPDATE PLANTILLA SET CONFVISTAS=1 WHERE ID=@PLANT   " & vbCrLf
sConsulta = sConsulta & "       FETCH NEXT FROM curTG_PLANTILLA_CONF_VISTAS_PROCE_Ins INTO @PLANT" & vbCrLf
sConsulta = sConsulta & "          END" & vbCrLf
sConsulta = sConsulta & "     Close curTG_PLANTILLA_CONF_VISTAS_PROCE_Ins" & vbCrLf
sConsulta = sConsulta & "     DEALLOCATE curTG_PLANTILLA_CONF_VISTAS_PROCE_Ins" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER PLANTILLA_PROCE_ESP_TG_INSUPD ON dbo.PLANTILLA_PROCE_ESP" & vbCrLf
sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @PLANT AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @ESP AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_PLANTILLA_PROCE_ESP_Ins CURSOR FOR SELECT PLANT,ID FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_PLANTILLA_PROCE_ESP_Ins" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PLANTILLA_PROCE_ESP_Ins INTO @PLANT,@ESP" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "UPDATE PLANTILLA_PROCE_ESP SET FECACT=GETDATE() WHERE PLANT=@PLANT AND ID=@ESP" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PLANTILLA_PROCE_ESP_Ins INTO @PLANT,@ESP" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_PLANTILLA_PROCE_ESP_Ins" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_PLANTILLA_PROCE_ESP_Ins" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PLANTILLA_ATRIB_TG_DEL]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PLANTILLA_ATRIB_TG_DEL]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PLANTILLA_ATRIB_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PLANTILLA_ATRIB_TG_INS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PLANTILLA_ATRIB_TG_INSUPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PLANTILLA_ATRIB_TG_INSUPD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PLANTILLA_ATRIB_TG_UPD_DATVAR]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PLANTILLA_ATRIB_TG_UPD_DATVAR]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PROCE_ATRIB_TG_DEL]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PROCE_ATRIB_TG_DEL]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PROCE_ATRIB_TG_INSUPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PROCE_ATRIB_TG_INSUPD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PROCE_ATRIB_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PROCE_ATRIB_TG_INS]" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER PLANTILLA_ATRIB_TG_DEL ON [dbo].[PLANTILLA_ATRIB] " & vbCrLf
sConsulta = sConsulta & "FOR DELETE " & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @GRUPO VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @ORDEN INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @PLANT INTEGER" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_PLANTILLA_ATRIBS_DEL CURSOR LOCAL FOR SELECT ORDEN_ATRIB,GRUPO,PLANT FROM DELETED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_PLANTILLA_ATRIBS_DEL" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PLANTILLA_ATRIBS_DEL INTO @ORDEN,@GRUPO,@PLANT" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET PLANTILLA_VISTAS=0 WHERE PLANTILLA=@PLANT AND PLANTILLA_VISTAS=1" & vbCrLf
sConsulta = sConsulta & "IF @GRUPO IS NULL" & vbCrLf
sConsulta = sConsulta & "   UPDATE PLANTILLA_ATRIB SET ORDEN_ATRIB=ORDEN_ATRIB - 1 WHERE PLANT=@PLANT AND GRUPO IS NULL  AND " & vbCrLf
sConsulta = sConsulta & "   ORDEN_ATRIB > @ORDEN" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   UPDATE PLANTILLA_ATRIB SET ORDEN_ATRIB=ORDEN_ATRIB - 1 WHERE PLANT=@PLANT AND GRUPO=@GRUPO AND " & vbCrLf
sConsulta = sConsulta & "   ORDEN_ATRIB > @ORDEN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PLANTILLA_ATRIBS_DEL INTO @ORDEN,@GRUPO,@PLANT" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_PLANTILLA_ATRIBS_DEL" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_PLANTILLA_ATRIBS_DEL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER PLANTILLA_ATRIB_TG_INS ON dbo.PLANTILLA_ATRIB" & vbCrLf
sConsulta = sConsulta & "FOR INSERT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @GRUPO VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE  @ATRIB INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @PLANT INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE  @COUNT INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @AMBITO INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID INTEGER" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_PLANTILLA_ATRIBS_ins CURSOR LOCAL FOR SELECT ATRIB,GRUPO,PLANT,ID,AMBITO FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_PLANTILLA_ATRIBS_ins" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PLANTILLA_ATRIBS_ins INTO @ATRIB,@GRUPO,@PLANT,@ID,@AMBITO" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN  " & vbCrLf
sConsulta = sConsulta & "    UPDATE PROCE SET PLANTILLA_VISTAS=0 WHERE PLANTILLA=@PLANT AND PLANTILLA_VISTAS=1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    IF @GRUPO IS NULL" & vbCrLf
sConsulta = sConsulta & "        SELECT  @COUNT=COUNT(*) FROM PLANTILLA_ATRIB WHERE PLANT=@PLANT AND  ATRIB=@ATRIB AND GRUPO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "        SELECT  @COUNT=COUNT(*) FROM PLANTILLA_ATRIB WHERE  PLANT=@PLANT AND ATRIB=@ATRIB AND GRUPO IS NULL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @COUNT>0" & vbCrLf
sConsulta = sConsulta & "         RAISERROR ('Atributo existente',16,2)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  IF @AMBITO= 1 OR @AMBITO=2" & vbCrLf
sConsulta = sConsulta & "             SELECT @COUNT =COUNT(*) FROM PLANTILLA_CONF_VISTAS_PROCE WHERE PLANT=@PLANT " & vbCrLf
sConsulta = sConsulta & "             IF @COUNT>0" & vbCrLf
sConsulta = sConsulta & "                   INSERT INTO PLANTILLA_CONF_VISTAS_PROCE_ATRIB (PLANT,VISTA, ATRIB)" & vbCrLf
sConsulta = sConsulta & "                         (SELECT PLANT,VISTA,@ID FROM PLANTILLA_CONF_VISTAS_PROCE WHERE PLANT=@PLANT )" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "         BEGIN" & vbCrLf
sConsulta = sConsulta & "         SELECT @COUNT =COUNT(*) FROM PLANTILLA_CONF_VISTAS_GRUPO WHERE PLANT=@PLANT  AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "         IF @COUNT>0" & vbCrLf
sConsulta = sConsulta & "                   INSERT INTO PLANTILLA_CONF_VISTAS_GRUPO_ATRIB (PLANT,GRUPO,VISTA, ATRIB)" & vbCrLf
sConsulta = sConsulta & "                         (SELECT PLANT,GRUPO,VISTA,@ID FROM PLANTILLA_CONF_VISTAS_GRUPO WHERE PLANT=@PLANT AND GRUPO=@GRUPO)" & vbCrLf
sConsulta = sConsulta & "         SELECT @COUNT =COUNT(*) FROM PLANTILLA_CONF_VISTAS_ALL WHERE PLANT=@PLANT " & vbCrLf
sConsulta = sConsulta & "         IF @COUNT>0" & vbCrLf
sConsulta = sConsulta & "                   INSERT INTO PLANTILLA_CONF_VISTAS_ALL_ATRIB (PLANT,VISTA, ATRIB)" & vbCrLf
sConsulta = sConsulta & "                         (SELECT PLANT,VISTA,@ID FROM PLANTILLA_CONF_VISTAS_ALL WHERE PLANT=@PLANT )" & vbCrLf
sConsulta = sConsulta & "        END    " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PLANTILLA_ATRIBS_ins INTO @ATRIB,@GRUPO,@PLANT,@ID,@AMBITO" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_PLANTILLA_ATRIBS_ins" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_PLANTILLA_ATRIBS_ins" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER PLANTILLA_ATRIB_TG_INSUPD ON dbo.PLANTILLA_ATRIB" & vbCrLf
sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID INT" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_PLANTILLA_ATRIB_Ins CURSOR FOR SELECT ID FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_PLANTILLA_ATRIB_Ins" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PLANTILLA_ATRIB_Ins INTO @ID" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "UPDATE PLANTILLA_ATRIB SET FECACT=GETDATE() WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PLANTILLA_ATRIB_Ins INTO @ID" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_PLANTILLA_ATRIB_Ins" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_PLANTILLA_ATRIB_Ins" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER PLANTILLA_ATRIB_TG_UPD_DATVAR ON dbo.PLANTILLA_ATRIB" & vbCrLf
sConsulta = sConsulta & "FOR UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @INTROOLD TINYINT,@INTRONEW TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @AMBITOOLD INTEGER,@AMBITONEW INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @PLANT INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_PLANTILLA_ATRIB_Upd_DATVAR CURSOR LOCAL FOR " & vbCrLf
sConsulta = sConsulta & "SELECT I.ID,D.INTRO,I.INTRO,D.AMBITO,I.AMBITO,D.PLANT" & vbCrLf
sConsulta = sConsulta & "FROM DELETED D " & vbCrLf
sConsulta = sConsulta & "INNER JOIN INSERTED I" & vbCrLf
sConsulta = sConsulta & "ON D.ID=I.ID" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_PLANTILLA_ATRIB_Upd_DATVAR" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PLANTILLA_ATRIB_Upd_DATVAR INTO @ID,@INTROOLD,@INTRONEW,@AMBITOOLD,@AMBITONEW,@PLANT" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @INTROOLD<>@INTRONEW" & vbCrLf
sConsulta = sConsulta & "    IF  @INTRONEW=0" & vbCrLf
sConsulta = sConsulta & "          DELETE PLANTILLA_ATRIB_LISTA WHERE ATRIB_ID=@ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF  @AMBITOOLD<>@AMBITONEW" & vbCrLf
sConsulta = sConsulta & "   UPDATE PROCE SET PLANTILLA_VISTAS=0 WHERE PLANTILLA=@PLANT AND PLANTILLA_VISTAS=1" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PLANTILLA_ATRIB_Upd_DATVAR INTO @ID,@INTROOLD,@INTRONEW,@AMBITOOLD,@AMBITONEW,@PLANT" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_PLANTILLA_ATRIB_Upd_DATVAR" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_PLANTILLA_ATRIB_Upd_DATVAR" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER PROCE_ATRIB_TG_DEL ON dbo.PROCE_ATRIB" & vbCrLf
sConsulta = sConsulta & "FOR DELETE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @GRUPO VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN1 VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCE INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @COUNT INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @APLIC_PREC INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @DEF_PREC INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @ORDEN INTEGER" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_PROCE_ATRIBS_DEL CURSOR LOCAL FOR SELECT ID,ANYO,GMN1,PROCE,GRUPO,APLIC_PREC,DEF_PREC,ORDEN_ATRIB FROM DELETED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_PROCE_ATRIBS_DEL" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_ATRIBS_DEL INTO @ID,@ANYO,@GMN1,@PROCE,@GRUPO,@APLIC_PREC,@DEF_PREC,@ORDEN" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "    " & vbCrLf
sConsulta = sConsulta & "IF @APLIC_PREC = 1" & vbCrLf
sConsulta = sConsulta & " begin" & vbCrLf
sConsulta = sConsulta & "   IF @GRUPO IS NULL" & vbCrLf
sConsulta = sConsulta & "        DELETE USAR_GR_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ATRIB =@ID " & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "        DELETE USAR_GR_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ATRIB =@ID AND GRUPO =@GRUPO         " & vbCrLf
sConsulta = sConsulta & " end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @GRUPO IS NULL" & vbCrLf
sConsulta = sConsulta & "   UPDATE PROCE_ATRIB SET ORDEN_ATRIB=ORDEN_ATRIB - 1 WHERE  ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "   AND GRUPO IS NULL  AND ORDEN_ATRIB > @ORDEN" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   UPDATE PROCE_ATRIB SET ORDEN_ATRIB=ORDEN_ATRIB - 1 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   AND GRUPO =@GRUPO AND ORDEN_ATRIB > @ORDEN" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_ATRIBS_DEL INTO @ID,@ANYO,@GMN1,@PROCE,@GRUPO,@APLIC_PREC,@DEF_PREC,@ORDEN" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_PROCE_ATRIBS_DEL" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_PROCE_ATRIBS_DEL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER PROCE_ATRIB_TG_INSUPD ON dbo.PROCE_ATRIB" & vbCrLf
sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @GRUPO VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN1 VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCE INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @COUNT INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @APLIC_PRECNEW INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @DEF_PREC INTEGER" & vbCrLf
sConsulta = sConsulta & "declare @EST AS INTEGER" & vbCrLf
sConsulta = sConsulta & "declare @SOLOORDEN AS VARBINARY(50) " & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "SET @SOLOORDEN = 0x00000002" & vbCrLf
sConsulta = sConsulta & "if COLUMNS_UPDATED()!= @SOLOORDEN" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_ATRIB SET FECACT = GETDATE()" & vbCrLf
sConsulta = sConsulta & "  FROM PROCE_ATRIB PA " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN INSERTED I" & vbCrLf
sConsulta = sConsulta & "               ON PA.ID = I.ID" & vbCrLf
sConsulta = sConsulta & "IF UPDATE(APLIC_PREC) OR UPDATE(DEF_PREC)" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_PROCE_ATRIBS_insUPS CURSOR LOCAL FOR " & vbCrLf
sConsulta = sConsulta & "SELECT I.ID,I.ANYO,I.GMN1,I.PROCE,I.GRUPO,I.APLIC_PREC,I.DEF_PREC FROM  INSERTED I" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_PROCE_ATRIBS_insUPS" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_ATRIBS_insUPS INTO @ID,@ANYO,@GMN1,@PROCE,@GRUPO,@APLIC_PRECNEW,@DEF_PREC" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "    " & vbCrLf
sConsulta = sConsulta & "SET @EST=(SELECT EST FROM PROCE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE)" & vbCrLf
sConsulta = sConsulta & "IF @APLIC_PRECNEW = 1" & vbCrLf
sConsulta = sConsulta & "   IF @GRUPO IS NULL" & vbCrLf
sConsulta = sConsulta & "         BEGIN" & vbCrLf
sConsulta = sConsulta & "            SELECT  @COUNT = COUNT(*) FROM USAR_GR_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE  AND ATRIB=@ID" & vbCrLf
sConsulta = sConsulta & "            IF  @COUNT > 0 " & vbCrLf
sConsulta = sConsulta & "                BEGIN" & vbCrLf
sConsulta = sConsulta & "       IF @EST <=  7" & vbCrLf
sConsulta = sConsulta & "                  UPDATE USAR_GR_ATRIB SET USAR_PREC=@DEF_PREC  WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE  AND ATRIB=@ID" & vbCrLf
sConsulta = sConsulta & "                END            " & vbCrLf
sConsulta = sConsulta & "             ELSE" & vbCrLf
sConsulta = sConsulta & "                   INSERT INTO USAR_GR_ATRIB (ANYO,GMN1,PROCE,GRUPO,ATRIB,USAR_PREC)" & vbCrLf
sConsulta = sConsulta & "                        SELECT DISTINCT @ANYO,@GMN1,@PROCE,COD,@ID,@DEF_PREC FROM PROCE_GRUPO WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "         END " & vbCrLf
sConsulta = sConsulta & "    ELSE" & vbCrLf
sConsulta = sConsulta & "        BEGIN" & vbCrLf
sConsulta = sConsulta & "             SELECT  @COUNT = COUNT(*) FROM USAR_GR_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE  AND ATRIB=@ID AND GRUPO = @GRUPO" & vbCrLf
sConsulta = sConsulta & "             IF  @COUNT > 0 " & vbCrLf
sConsulta = sConsulta & "                 BEGIN" & vbCrLf
sConsulta = sConsulta & "                 IF @EST <= 7" & vbCrLf
sConsulta = sConsulta & "                  UPDATE USAR_GR_ATRIB SET USAR_PREC=@DEF_PREC  WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE  AND ATRIB=@ID AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "                 END" & vbCrLf
sConsulta = sConsulta & "             ELSE" & vbCrLf
sConsulta = sConsulta & "                  INSERT INTO USAR_GR_ATRIB  VALUES (@ANYO,@GMN1,@PROCE, @GRUPO,@ID,@DEF_PREC)" & vbCrLf
sConsulta = sConsulta & "        END          " & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "    IF @GRUPO IS NULL" & vbCrLf
sConsulta = sConsulta & "        DELETE USAR_GR_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ATRIB =@ID " & vbCrLf
sConsulta = sConsulta & "    ELSE" & vbCrLf
sConsulta = sConsulta & "        DELETE USAR_GR_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ATRIB =@ID AND GRUPO =@GRUPO                 " & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_ATRIBS_insUPS INTO @ID,@ANYO,@GMN1,@PROCE,@GRUPO,@APLIC_PRECNEW,@DEF_PREC" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_PROCE_ATRIBS_insUPS" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_PROCE_ATRIBS_insUPS" & vbCrLf
sConsulta = sConsulta & "END " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER PROCE_ATRIB_TG_INS ON dbo.PROCE_ATRIB" & vbCrLf
sConsulta = sConsulta & "FOR INSERT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @GRUPO VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN1 VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCE INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE  @ATRIB INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE  @COUNT INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE  @ID INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE  @AMBITO INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_PROCE_ATRIBS_ins CURSOR LOCAL FOR SELECT ATRIB,GRUPO,ANYO,GMN1,PROCE,ID,AMBITO FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_PROCE_ATRIBS_ins" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_ATRIBS_ins INTO @ATRIB,@GRUPO,@ANYO,@GMN1,@PROCE,@ID,@AMBITO" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "    IF @GRUPO IS NULL" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "        SELECT  @COUNT=COUNT(*) FROM PROCE_ATRIB WHERE ATRIB=@ATRIB  AND ANYO=@ANYO" & vbCrLf
sConsulta = sConsulta & "       AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "        " & vbCrLf
sConsulta = sConsulta & "        SELECT  @COUNT=COUNT(*) FROM PROCE_ATRIB WHERE ATRIB=@ATRIB   AND ANYO=@ANYO" & vbCrLf
sConsulta = sConsulta & "          AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO IS NULL" & vbCrLf
sConsulta = sConsulta & "   IF @COUNT>0" & vbCrLf
sConsulta = sConsulta & "               RAISERROR ('Atributo existente',16,2)" & vbCrLf
sConsulta = sConsulta & "   IF @AMBITO= 1 OR @AMBITO=2" & vbCrLf
sConsulta = sConsulta & "             SELECT @COUNT =COUNT(*) FROM CONF_VISTAS_PROCE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "         IF @COUNT>0" & vbCrLf
sConsulta = sConsulta & "                   INSERT INTO CONF_VISTAS_PROCE_ATRIB (ANYO,GMN1,PROCE,USU,VISTA, ATRIB)" & vbCrLf
sConsulta = sConsulta & "                         (SELECT ANYO,GMN1,PROCE,USU,VISTA,@ID FROM CONF_VISTAS_PROCE WHERE ANYO=@ANYO" & vbCrLf
sConsulta = sConsulta & "                         AND GMN1=@GMN1 AND PROCE=@PROCE )" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "         BEGIN" & vbCrLf
sConsulta = sConsulta & "         SELECT @COUNT =COUNT(*) FROM CONF_VISTAS_GRUPO WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "         IF @COUNT>0" & vbCrLf
sConsulta = sConsulta & "                   INSERT INTO CONF_VISTAS_GRUPO_ATRIB (ANYO,GMN1,PROCE,GRUPO,USU,VISTA, ATRIB)" & vbCrLf
sConsulta = sConsulta & "                         (SELECT ANYO,GMN1,PROCE,GRUPO,USU,VISTA,@ID FROM CONF_VISTAS_GRUPO WHERE ANYO=@ANYO" & vbCrLf
sConsulta = sConsulta & "                          AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO)" & vbCrLf
sConsulta = sConsulta & "         SELECT @COUNT =COUNT(*) FROM CONF_VISTAS_ALL WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "         IF @COUNT>0" & vbCrLf
sConsulta = sConsulta & "                   INSERT INTO CONF_VISTAS_ALL_ATRIB (ANYO,GMN1,PROCE,USU,VISTA, ATRIB)" & vbCrLf
sConsulta = sConsulta & "                         (SELECT ANYO,GMN1,PROCE,USU,VISTA,@ID FROM CONF_VISTAS_ALL WHERE ANYO=@ANYO" & vbCrLf
sConsulta = sConsulta & "                            AND GMN1=@GMN1 AND PROCE=@PROCE )" & vbCrLf
sConsulta = sConsulta & "         SELECT @COUNT =COUNT(*) FROM CONF_VISTAS_ITEM WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "         IF @COUNT>0" & vbCrLf
sConsulta = sConsulta & "                   INSERT INTO CONF_VISTAS_ITEM_ATRIB (ANYO,GMN1,PROCE,GRUPO,USU, VISTA,ATRIB)" & vbCrLf
sConsulta = sConsulta & "                         (SELECT ANYO,GMN1,PROCE,GRUPO,USU,VISTA,@ID FROM CONF_VISTAS_ITEM WHERE ANYO=@ANYO" & vbCrLf
sConsulta = sConsulta & "                          AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO)" & vbCrLf
sConsulta = sConsulta & "        END    " & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_ATRIBS_ins INTO @ATRIB,@GRUPO,@ANYO,@GMN1,@PROCE,@ID,@AMBITO" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_PROCE_ATRIBS_ins" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_PROCE_ATRIBS_ins" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PROCE_TG_UPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PROCE_TG_UPD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER PROCE_TG_UPD ON dbo.PROCE" & vbCrLf
sConsulta = sConsulta & "FOR UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO INT,@GMN1 VARCHAR(50),@COD SMALLINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @CALCPEND TINYINT, @CALCPEND_ANT TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_PROCE_Upd CURSOR LOCAL FOR SELECT I.ANYO,I.GMN1,I.COD,I.CALCPEND,D.CALCPEND FROM INSERTED I" & vbCrLf
sConsulta = sConsulta & "     INNER JOIN DELETED D ON I.ANYO=D.ANYO AND I.GMN1=D.GMN1 AND I.COD=D.COD" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_PROCE_Upd" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_Upd INTO @ANYO,@GMN1,@COD,@CALCPEND,@CALCPEND_ANT" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   IF UPDATE(CALCPEND)" & vbCrLf
sConsulta = sConsulta & "   IF @CALCPEND = 0 AND @CALCPEND_ANT=1" & vbCrLf
sConsulta = sConsulta & "                  UPDATE PROCE SET CALCFEC=GETDATE() WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@COD" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_Upd INTO @ANYO,@GMN1,@COD,@CALCPEND,@CALCPEND_ANT" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_PROCE_Upd" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_PROCE_Upd" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PLANTILLA_TG_UPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PLANTILLA_TG_UPD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PLANTILLA_GR_TG_UPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PLANTILLA_GR_TG_UPD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER PLANTILLA_TG_UPD ON dbo.PLANTILLA" & vbCrLf
sConsulta = sConsulta & "FOR UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @ADMIN AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_ATRIB AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @CONFVISTAS AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_PLANTILLA_Upd CURSOR FOR SELECT ID,ADMIN_PUB,CONFVISTAS FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_PLANTILLA_Upd" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PLANTILLA_Upd INTO @ID,@ADMIN,@CONFVISTAS" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF UPDATE(ADMIN_PUB) AND @ADMIN=0" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "   UPDATE PLANTILLA_GR SET SOBRE=NULL WHERE PLANT=@ID" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "IF UPDATE(ADMIN_PUB) AND @ADMIN=1" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       DECLARE curTG_ATRIB CURSOR FOR SELECT ID FROM PLANTILLA_ATRIB WHERE PLANT=@ID  AND AMBITO=1" & vbCrLf
sConsulta = sConsulta & "       OPEN curTG_ATRIB" & vbCrLf
sConsulta = sConsulta & "       FETCH NEXT FROM curTG_ATRIB INTO @ID_ATRIB" & vbCrLf
sConsulta = sConsulta & "       WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "                              IF @CONFVISTAS=1" & vbCrLf
sConsulta = sConsulta & "                                           BEGIN" & vbCrLf
sConsulta = sConsulta & "               DELETE FROM PLANTILLA_CONF_VISTAS_ALL_ATRIB WHERE PLANT=@ID AND ATRIB=@ID_ATRIB" & vbCrLf
sConsulta = sConsulta & "               DELETE FROM PLANTILLA_CONF_VISTAS_GRUPO_ATRIB WHERE PLANT=@ID AND ATRIB=@ID_ATRIB" & vbCrLf
sConsulta = sConsulta & "               DELETE FROM PLANTILLA_CONF_VISTAS_PROCE_ATRIB WHERE PLANT=@ID AND ATRIB=@ID_ATRIB" & vbCrLf
sConsulta = sConsulta & "                                          END" & vbCrLf
sConsulta = sConsulta & "           DELETE FROM PLANTILLA_ATRIB_LISTA WHERE ATRIB_ID=@ID_ATRIB" & vbCrLf
sConsulta = sConsulta & "           DELETE FROM PLANTILLA_ATRIB_POND WHERE ATRIB_ID=@ID_ATRIB  " & vbCrLf
sConsulta = sConsulta & "           DELETE FROM PLANTILLA_ATRIB WHERE ID=@ID_ATRIB" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "           FETCH NEXT FROM curTG_ATRIB INTO @ID_ATRIB" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "       Close curTG_ATRIB" & vbCrLf
sConsulta = sConsulta & "       DEALLOCATE curTG_ATRIB" & vbCrLf
sConsulta = sConsulta & "       IF @CONFVISTAS=1" & vbCrLf
sConsulta = sConsulta & "           UPDATE PROCE SET PLANTILLA_VISTAS=0 WHERE PLANTILLA=@ID AND PLANTILLA_VISTAS=1" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "_FETCH:" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PLANTILLA_Upd INTO @ID,@ADMIN,@CONFVISTAS" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_PLANTILLA_Upd" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_PLANTILLA_Upd" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER PLANTILLA_GR_TG_UPD ON dbo.PLANTILLA_GR" & vbCrLf
sConsulta = sConsulta & "FOR UPDATE " & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @PLANT AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @SOBRE AS INTEGER" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_PLANTILLA_GR_Upd CURSOR FOR SELECT PLANT,SOBRE FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_PLANTILLA_GR_Upd" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PLANTILLA_GR_Upd INTO @PLANT,@SOBRE" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF UPDATE(SOBRE)" & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "   DELETE FROM PLANTILLA_CONF_VISTAS_PROCE_SOBRE WHERE PLANT=@PLANT" & vbCrLf
sConsulta = sConsulta & "   UPDATE PROCE SET PLANTILLA_VISTAS=0 WHERE PLANTILLA=@PLANT AND PLANTILLA_VISTAS=1" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "_FETCH:" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PLANTILLA_GR_Upd INTO @PLANT,@SOBRE" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_PLANTILLA_GR_Upd" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_PLANTILLA_GR_Upd" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta



sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO ACC VALUES (12040,12018)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO USU_ACC (USU, ACC) SELECT USU, 12040 FROM USU_ACC WHERE ACC = 12003" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "INSERT INTO PERF_ACC (PERF, ACC) SELECT PERF, 12040 FROM PERF_ACC WHERE ACC = 12003" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & ""

End Sub

Private Sub V_2_12_5_Storeds_0()
Dim sConsulta As String

sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ATR_CARGAR_PLANTILLA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[ATR_CARGAR_PLANTILLA]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ATR_CARGAR_PROCE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[ATR_CARGAR_PROCE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ATR_ELIMINAR]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[ATR_ELIMINAR]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ATR_INSERTAR]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[ATR_INSERTAR]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ATR_MODIFICAR]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[ATR_MODIFICAR]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PLANTILLA_GR_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[PLANTILLA_GR_COD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_AR_AHORROS_CALCULAR]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_AR_AHORROS_CALCULAR]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_AR_ART4_ADJ]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_AR_ART4_ADJ]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_AR_CALCULAR]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_AR_CALCULAR]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_AR_JOB_LEER_DATOS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_AR_JOB_LEER_DATOS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_AR_JOB_MODIF_DATOS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_AR_JOB_MODIF_DATOS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_AR_PROCE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_AR_PROCE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_CIERRE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_CIERRE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_CIERRE_PARC]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_CIERRE_PARC]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_DEVOLVER_PROCE_ATRIB]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_DEVOLVER_PROCE_ATRIB]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_DEVOLVER_PROCE_ATRIB_PORTAL]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_DEVOLVER_PROCE_ATRIB_PORTAL]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ELIM_OFERTAS_NO_ENVIADAS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_ELIM_OFERTAS_NO_ENVIADAS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_COMPARAR_ARTICULOS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_COMPARAR_ARTICULOS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_CARGAR_CONFIG_PROCE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_CARGAR_CONFIG_PROCE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE ATR_CARGAR_PLANTILLA  (@PLANTILLA INT,@GRUPO VARCHAR(50)=NULL)  AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @GRUPO IS NULL" & vbCrLf
sConsulta = sConsulta & "                    SELECT PLANTILLA_ATRIB.ID,PLANTILLA_ATRIB.ATRIB,PLANTILLA_ATRIB.PLANT,PLANTILLA_ATRIB.GRUPO,PLANTILLA_ATRIB.INTRO,PLANTILLA_ATRIB.MINNUM,PLANTILLA_ATRIB.MAXNUM,PLANTILLA_ATRIB.MINFEC,PLANTILLA_ATRIB.MAXFEC,PLANTILLA_ATRIB.PREFBAJO," & vbCrLf
sConsulta = sConsulta & "                            PLANTILLA_ATRIB.INTERNO,PLANTILLA_ATRIB.AMBITO,PLANTILLA_ATRIB.OBLIG,PLANTILLA_ATRIB.OP_PREC,PLANTILLA_ATRIB.APLIC_PREC,PLANTILLA_ATRIB.DEF_PREC," & vbCrLf
sConsulta = sConsulta & "                            PLANTILLA_ATRIB.POND,PLANTILLA_ATRIB.OP_POND,PLANTILLA_ATRIB.NUM_POND,PLANTILLA_ATRIB.POND_SI,PLANTILLA_ATRIB.POND_NO," & vbCrLf
sConsulta = sConsulta & "                            DEF_ATRIB.COD,DEF_ATRIB.DEN,DEF_ATRIB.TIPO,PLANTILLA_ATRIB.FECACT,PLANTILLA_ATRIB.ORDEN" & vbCrLf
sConsulta = sConsulta & "                            FROM PLANTILLA_ATRIB INNER JOIN DEF_ATRIB ON PLANTILLA_ATRIB.ATRIB=DEF_ATRIB.ID" & vbCrLf
sConsulta = sConsulta & "                            WHERE PLANTILLA_ATRIB.PLANT=@PLANTILLA  AND  PLANTILLA_ATRIB.GRUPO IS NULL   ORDER BY PLANTILLA_ATRIB.ORDEN_ATRIB" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "                    SELECT PLANTILLA_ATRIB.ID,PLANTILLA_ATRIB.ATRIB,PLANTILLA_ATRIB.PLANT,PLANTILLA_ATRIB.GRUPO,PLANTILLA_ATRIB.INTRO,PLANTILLA_ATRIB.MINNUM,PLANTILLA_ATRIB.MAXNUM,PLANTILLA_ATRIB.MINFEC,PLANTILLA_ATRIB.MAXFEC,PLANTILLA_ATRIB.PREFBAJO," & vbCrLf
sConsulta = sConsulta & "                            PLANTILLA_ATRIB.INTERNO,PLANTILLA_ATRIB.AMBITO,PLANTILLA_ATRIB.OBLIG,PLANTILLA_ATRIB.OP_PREC,PLANTILLA_ATRIB.APLIC_PREC,PLANTILLA_ATRIB.DEF_PREC," & vbCrLf
sConsulta = sConsulta & "                            PLANTILLA_ATRIB.POND,PLANTILLA_ATRIB.OP_POND,PLANTILLA_ATRIB.NUM_POND,PLANTILLA_ATRIB.POND_SI,PLANTILLA_ATRIB.POND_NO," & vbCrLf
sConsulta = sConsulta & "                            DEF_ATRIB.COD,DEF_ATRIB.DEN,DEF_ATRIB.TIPO,PLANTILLA_ATRIB.FECACT,PLANTILLA_ATRIB.ORDEN" & vbCrLf
sConsulta = sConsulta & "                            FROM PLANTILLA_ATRIB INNER JOIN DEF_ATRIB ON PLANTILLA_ATRIB.ATRIB=DEF_ATRIB.ID" & vbCrLf
sConsulta = sConsulta & "                            WHERE PLANTILLA_ATRIB.PLANT=@PLANTILLA  AND   (PLANTILLA_ATRIB.GRUPO=@GRUPO OR PLANTILLA_ATRIB.GRUPO IS NULL)" & vbCrLf
sConsulta = sConsulta & "                               ORDER BY PLANTILLA_ATRIB.GRUPO, PLANTILLA_ATRIB.ORDEN_ATRIB" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE ATR_CARGAR_PROCE  (@PROCE SMALLINT,@ANYO AS INTEGER,@GMN1 AS VARCHAR(50), @ALL AS TINYINT,@GRUPO VARCHAR(50)=NULL,@ORDENADO SMALLINT=NULL)  AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @NUMORD  VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & " if  @ORDENADO is null " & vbCrLf
sConsulta = sConsulta & "     set @NUMORD='4,29'" & vbCrLf
sConsulta = sConsulta & "else" & vbCrLf
sConsulta = sConsulta & "     if  @ORDENADO=0" & vbCrLf
sConsulta = sConsulta & "      set  @NUMORD=11" & vbCrLf
sConsulta = sConsulta & "     else" & vbCrLf
sConsulta = sConsulta & "        if  @ORDENADO=1" & vbCrLf
sConsulta = sConsulta & "               set  @NUMORD=25" & vbCrLf
sConsulta = sConsulta & "        else" & vbCrLf
sConsulta = sConsulta & "        if  @ORDENADO=2" & vbCrLf
sConsulta = sConsulta & "            set @NUMORD=26" & vbCrLf
sConsulta = sConsulta & "       else" & vbCrLf
sConsulta = sConsulta & "           if  @ORDENADO=3" & vbCrLf
sConsulta = sConsulta & "                set @NUMORD=24" & vbCrLf
sConsulta = sConsulta & "          else" & vbCrLf
sConsulta = sConsulta & "                if  @ORDENADO=5" & vbCrLf
sConsulta = sConsulta & "                   set   @NUMORD=5" & vbCrLf
sConsulta = sConsulta & "               else" & vbCrLf
sConsulta = sConsulta & "                   if  @ORDENADO=6" & vbCrLf
sConsulta = sConsulta & "                      set   @NUMORD='6,8'" & vbCrLf
sConsulta = sConsulta & "                   else" & vbCrLf
sConsulta = sConsulta & "                        if  @ORDENADO=7" & vbCrLf
sConsulta = sConsulta & "                             set   @NUMORD='7,9'" & vbCrLf
sConsulta = sConsulta & "                       else" & vbCrLf
sConsulta = sConsulta & "                             if  @ORDENADO=8" & vbCrLf
sConsulta = sConsulta & "                                set @NUMORD=5" & vbCrLf
sConsulta = sConsulta & "                             else" & vbCrLf
sConsulta = sConsulta & "                                  if  @ORDENADO=10" & vbCrLf
sConsulta = sConsulta & "                                      set @NUMORD=12" & vbCrLf
sConsulta = sConsulta & "                                  else" & vbCrLf
sConsulta = sConsulta & "                                       if  @ORDENADO=11" & vbCrLf
sConsulta = sConsulta & "                                          set @NUMORD=13" & vbCrLf
sConsulta = sConsulta & "                                       else" & vbCrLf
sConsulta = sConsulta & "                                           if  @ORDENADO=12" & vbCrLf
sConsulta = sConsulta & "                                               set @NUMORD=10" & vbCrLf
sConsulta = sConsulta & "                                           else" & vbCrLf
sConsulta = sConsulta & "                                                if  @ORDENADO=13" & vbCrLf
sConsulta = sConsulta & "                                                    set @NUMORD=14" & vbCrLf
sConsulta = sConsulta & "                                                else" & vbCrLf
sConsulta = sConsulta & "                                                     if  @ORDENADO=14" & vbCrLf
sConsulta = sConsulta & "                                                        set  @NUMORD=15" & vbCrLf
sConsulta = sConsulta & "                                                     else" & vbCrLf
sConsulta = sConsulta & "                                                         if  @ORDENADO=15" & vbCrLf
sConsulta = sConsulta & "                                                             set @NUMORD=16" & vbCrLf
sConsulta = sConsulta & "                                                        else" & vbCrLf
sConsulta = sConsulta & "                                                             set @NUMORD=1" & vbCrLf
sConsulta = sConsulta & "IF @GRUPO IS NULL" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=' SELECT PROCE_ATRIB.ID,PROCE_ATRIB.ATRIB,PROCE_ATRIB.PROCE,PROCE_ATRIB.GRUPO,PROCE_ATRIB.INTRO,PROCE_ATRIB.MINNUM,PROCE_ATRIB.MAXNUM,PROCE_ATRIB.MINFEC,PROCE_ATRIB.MAXFEC,PROCE_ATRIB.PREFBAJO,' " & vbCrLf
sConsulta = sConsulta & "         + '  PROCE_ATRIB.INTERNO,PROCE_ATRIB.AMBITO,PROCE_ATRIB.OBLIG,PROCE_ATRIB.OP_PREC,PROCE_ATRIB.APLIC_PREC,PROCE_ATRIB.DEF_PREC, PROCE_ATRIB.POND,PROCE_ATRIB.OP_POND,PROCE_ATRIB.NUM_POND,PROCE_ATRIB.POND_SI,PROCE_ATRIB.POND_NO,' " & vbCrLf
sConsulta = sConsulta & "        + '  PROCE_ATRIB.GMN1,PROCE_ATRIB.ANYO,DEF_ATRIB.TIPO,DEF_ATRIB.COD,DEF_ATRIB.DEN,PROCE_ATRIB.FECACT,PROCE_ATRIB.ORDEN,PROCE_ATRIB.ORDEN_ATRIB' " & vbCrLf
sConsulta = sConsulta & "         + '  FROM PROCE_ATRIB INNER JOIN DEF_ATRIB ON PROCE_ATRIB.ATRIB=DEF_ATRIB.ID' " & vbCrLf
sConsulta = sConsulta & "         + '  WHERE PROCE_ATRIB.PROCE=' +  CAST(@PROCE AS VARCHAR) + '  AND  PROCE_ATRIB.GRUPO IS NULL AND  PROCE_ATRIB.ANYO=' +  CAST(@ANYO AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "         + ' AND PROCE_ATRIB.GMN1= '''  + @GMN1 + ''' ORDER BY ' + CAST( @NUMORD AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "      IF  @ALL = 1" & vbCrLf
sConsulta = sConsulta & "                    SET @SQL=' SELECT PROCE_ATRIB.ID,PROCE_ATRIB.ATRIB,PROCE_ATRIB.PROCE,PROCE_ATRIB.GRUPO,PROCE_ATRIB.INTRO,PROCE_ATRIB.MINNUM,PROCE_ATRIB.MAXNUM,PROCE_ATRIB.MINFEC,PROCE_ATRIB.MAXFEC,PROCE_ATRIB.PREFBAJO,' " & vbCrLf
sConsulta = sConsulta & "                        + '  PROCE_ATRIB.INTERNO,PROCE_ATRIB.AMBITO,PROCE_ATRIB.OBLIG,PROCE_ATRIB.OP_PREC,PROCE_ATRIB.APLIC_PREC,PROCE_ATRIB.DEF_PREC, PROCE_ATRIB.POND,PROCE_ATRIB.OP_POND,PROCE_ATRIB.NUM_POND,PROCE_ATRIB.POND_SI,PROCE_ATRIB.POND_NO,' " & vbCrLf
sConsulta = sConsulta & "                        + '  PROCE_ATRIB.GMN1,PROCE_ATRIB.ANYO,DEF_ATRIB.TIPO,DEF_ATRIB.COD,DEF_ATRIB.DEN,PROCE_ATRIB.FECACT,PROCE_ATRIB.ORDEN,PROCE_ATRIB.ORDEN_ATRIB' " & vbCrLf
sConsulta = sConsulta & "                        + '  FROM PROCE_ATRIB INNER JOIN DEF_ATRIB ON PROCE_ATRIB.ATRIB=DEF_ATRIB.ID' " & vbCrLf
sConsulta = sConsulta & "                        + '  WHERE PROCE_ATRIB.PROCE=' +  CAST(@PROCE AS VARCHAR) + '  AND  PROCE_ATRIB.ANYO=' +  CAST(@ANYO AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                        + ' AND PROCE_ATRIB.GMN1= '''  + @GMN1 + ''' ORDER BY ' + CAST( @NUMORD AS VARCHAR)    " & vbCrLf
sConsulta = sConsulta & "       ELSE" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "             SET @SQL=' SELECT PROCE_ATRIB.ID,PROCE_ATRIB.ATRIB,PROCE_ATRIB.PROCE,PROCE_ATRIB.GRUPO,PROCE_ATRIB.INTRO,PROCE_ATRIB.MINNUM,PROCE_ATRIB.MAXNUM,PROCE_ATRIB.MINFEC,PROCE_ATRIB.MAXFEC,PROCE_ATRIB.PREFBAJO,' " & vbCrLf
sConsulta = sConsulta & "                        + '  PROCE_ATRIB.INTERNO,PROCE_ATRIB.AMBITO,PROCE_ATRIB.OBLIG,PROCE_ATRIB.OP_PREC,PROCE_ATRIB.APLIC_PREC,PROCE_ATRIB.DEF_PREC, PROCE_ATRIB.POND,PROCE_ATRIB.OP_POND,PROCE_ATRIB.NUM_POND,PROCE_ATRIB.POND_SI,PROCE_ATRIB.POND_NO,' " & vbCrLf
sConsulta = sConsulta & "                        + '  PROCE_ATRIB.GMN1,PROCE_ATRIB.ANYO,DEF_ATRIB.TIPO,DEF_ATRIB.COD,DEF_ATRIB.DEN,PROCE_ATRIB.FECACT,PROCE_ATRIB.ORDEN,PROCE_ATRIB.ORDEN_ATRIB' " & vbCrLf
sConsulta = sConsulta & "                        + '  FROM PROCE_ATRIB INNER JOIN DEF_ATRIB ON PROCE_ATRIB.ATRIB=DEF_ATRIB.ID' " & vbCrLf
sConsulta = sConsulta & "                        + '  WHERE PROCE_ATRIB.PROCE=' +  CAST(@PROCE AS VARCHAR) + '  AND  PROCE_ATRIB.ANYO=' +  CAST(@ANYO AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                        + ' AND PROCE_ATRIB.GMN1= '''  + @GMN1 + '''  AND  PROCE_ATRIB.GRUPO=''' +@GRUPO+ ''' ORDER BY ' + CAST( @NUMORD AS VARCHAR)   " & vbCrLf
sConsulta = sConsulta & " EXEC SP_EXECUTESQL @STMT=@SQL" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE ATR_ELIMINAR  @TABLA SMALLINT,@ATRIB INTEGER  AS" & vbCrLf
sConsulta = sConsulta & "IF @TABLA = 0" & vbCrLf
sConsulta = sConsulta & "     BEGIN" & vbCrLf
sConsulta = sConsulta & "   DELETE FROM PROVE_ART4_ATRIB  WHERE ATRIB=@ATRIB" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   DELETE  FROM ART4_ATRIB  WHERE ATRIB=@ATRIB" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   DELETE FROM GMN4_ATRIB  WHERE ATRIB=@ATRIB" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   DELETE FROM PLANTILLA_ATRIB_LISTA  WHERE ATRIB_ID=@ATRIB" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   DELETE  FROM PLANTILLA_ATRIB_POND  WHERE ATRIB_ID=@ATRIB" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   DELETE FROM PLANTILLA_ATRIB  WHERE ATRIB=@ATRIB" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "   DELETE FROM DEF_ATRIB_LISTA WHERE ATRIB=@ATRIB" & vbCrLf
sConsulta = sConsulta & "      " & vbCrLf
sConsulta = sConsulta & "   DELETE FROM DEF_ATRIB_POND WHERE ATRIB=@ATRIB" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "    DELETE   FROM DEF_ATRIB  WHERE ID=@ATRIB" & vbCrLf
sConsulta = sConsulta & "    END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "         IF @TABLA=1" & vbCrLf
sConsulta = sConsulta & "              BEGIN" & vbCrLf
sConsulta = sConsulta & "                    DELETE FROM CONF_VISTAS_ALL_ATRIB  WHERE ATRIB=@ATRIB" & vbCrLf
sConsulta = sConsulta & "                    DELETE FROM CONF_VISTAS_GRUPO_ATRIB WHERE ATRIB=@ATRIB" & vbCrLf
sConsulta = sConsulta & "                    DELETE FROM CONF_VISTAS_ITEM_ATRIB WHERE ATRIB=@ATRIB" & vbCrLf
sConsulta = sConsulta & "                    DELETE FROM CONF_VISTAS_PROCE_ATRIB WHERE ATRIB=@ATRIB" & vbCrLf
sConsulta = sConsulta & "                    DELETE FROM OFE_ATRIB WHERE ATRIB_ID=@ATRIB" & vbCrLf
sConsulta = sConsulta & "                    DELETE FROM OFE_GR_ATRIB WHERE ATRIB_ID=@ATRIB" & vbCrLf
sConsulta = sConsulta & "                    DELETE FROM OFE_ITEM_ATRIB WHERE ATRIB_ID=@ATRIB" & vbCrLf
sConsulta = sConsulta & "                    DELETE FROM PROCE_ATRIB_POND WHERE ATRIB_ID=@ATRIB" & vbCrLf
sConsulta = sConsulta & "                    DELETE FROM PROCE_ATRIB_LISTA WHERE ATRIB_ID=@ATRIB" & vbCrLf
sConsulta = sConsulta & "                    DELETE FROM PROCE_ATRIB WHERE ID=@ATRIB" & vbCrLf
sConsulta = sConsulta & "               END" & vbCrLf
sConsulta = sConsulta & "         ELSE" & vbCrLf
sConsulta = sConsulta & "             IF @TABLA=2" & vbCrLf
sConsulta = sConsulta & "                 BEGIN" & vbCrLf
sConsulta = sConsulta & "           DELETE FROM PLANTILLA_CONF_VISTAS_ALL_ATRIB  WHERE ATRIB=@ATRIB" & vbCrLf
sConsulta = sConsulta & "                       DELETE FROM PLANTILLA_CONF_VISTAS_GRUPO_ATRIB WHERE ATRIB=@ATRIB" & vbCrLf
sConsulta = sConsulta & "                   DELETE FROM PLANTILLA_CONF_VISTAS_PROCE_ATRIB WHERE ATRIB=@ATRIB" & vbCrLf
sConsulta = sConsulta & "                        DELETE FROM PLANTILLA_ATRIB_POND WHERE ATRIB_ID=@ATRIB" & vbCrLf
sConsulta = sConsulta & "                        DELETE FROM PLANTILLA_ATRIB_LISTA WHERE ATRIB_ID=@ATRIB" & vbCrLf
sConsulta = sConsulta & "                        DELETE FROM PLANTILLA_ATRIB WHERE ID=@ATRIB" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                 END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE ATR_INSERTAR  @TABLA SMALLINT,@COD VARCHAR(50),@DEN VARCHAR(100),@TIPO SMALLINT,@PREFBAJO TINYINT,@INTRO TINYINT , @MINNUM FLOAT,@MINFEC DATETIME,@MAXNUM FLOAT,@MAXFEC DATETIME,@ATRIB INTEGER,@ANYO SMALLINT,@GMN1 VARCHAR(50),@PROCE SMALLINT,@PLANTILLA  VARCHAR(50),@GRUPO VARCHAR(50),@INTERNO  TINYINT,@AMBITO TINYINT,@OBLIG TINYINT,@OP_PREC VARCHAR(2),@APLIC_PREC TINYINT,@DEF_PREC TINYINT=NULL,@MOD_ATRIB TINYINT,@INS INT OUTPUT ,@FECACT DATETIME OUTPUT,@PONDE INT OUTPUT  AS   " & vbCrLf
sConsulta = sConsulta & "DECLARE @ID AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @POND AS integer" & vbCrLf
sConsulta = sConsulta & "DECLARE @OP_POND AS VARCHAR(2)" & vbCrLf
sConsulta = sConsulta & "DECLARE @POND_SI AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @POND_NO AS FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @NUM AS FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ORDEN AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @VALOR_NUM AS FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @VALOR_TEXT AS VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @VALOR_FEC AS DATETIME" & vbCrLf
sConsulta = sConsulta & "DECLARE @VALOR_POND AS FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @DESDE_NUM AS FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @HASTA_NUM AS FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @DESDE_FEC AS DATETIME" & vbCrLf
sConsulta = sConsulta & "DECLARE @HASTA_FEC AS DATETIME" & vbCrLf
sConsulta = sConsulta & "DECLARE @ATRIB_ID AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @ORDENPLANT AS SMALLINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_PLANT AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @INTRO_D AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ORDENATRIB AS INTEGER" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @TABLA=0" & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO DEF_ATRIB(COD,DEN,TIPO,PREFBAJO,INTRO,MAXNUM,MINNUM,MAXFEC,MINFEC)" & vbCrLf
sConsulta = sConsulta & "   VALUES(@COD,@DEN,@TIPO,@PREFBAJO,@INTRO,@MAXNUM,@MINNUM,@MAXFEC,@MINFEC) " & vbCrLf
sConsulta = sConsulta & "   SELECT @INS=ID,@PONDE=POND,@FECACT=FECACT FROM DEF_ATRIB WHERE ID=(SELECT MAX(ID)  FROM DEF_ATRIB )" & vbCrLf
sConsulta = sConsulta & "    END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "     IF @TABLA = 1" & vbCrLf
sConsulta = sConsulta & "             BEGIN" & vbCrLf
sConsulta = sConsulta & "              IF @MOD_ATRIB=1" & vbCrLf
sConsulta = sConsulta & "                  UPDATE DEF_ATRIB SET PREFBAJO=@PREFBAJO,INTRO=@INTRO,MAXNUM=@MAXNUM" & vbCrLf
sConsulta = sConsulta & "                                      ,MINNUM=@MINNUM,MAXFEC=CONVERT(VARCHAR(10),@MAXFEC,111),MINFEC=CONVERT(VARCHAR(10),@MINFEC,111) " & vbCrLf
sConsulta = sConsulta & "                                       WHERE ID=@ATRIB" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "              SELECT @INTRO_D=INTRO,@POND=POND,@OP_POND=FORMULA,@NUM=NUM,@POND_SI=POND_SI,@POND_NO=POND_NO" & vbCrLf
sConsulta = sConsulta & "                                FROM DEF_ATRIB WHERE ID=@ATRIB" & vbCrLf
sConsulta = sConsulta & "                         IF @INTRO_D<>@INTRO" & vbCrLf
sConsulta = sConsulta & "                                 IF @INTRO=0 AND @POND=2" & vbCrLf
sConsulta = sConsulta & "                                      SET @POND=0" & vbCrLf
sConsulta = sConsulta & "            IF @GRUPO IS NULL " & vbCrLf
sConsulta = sConsulta & "               SELECT @ORDENATRIB=MAX(ORDEN_ATRIB) FROM PROCE_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO IS NULL" & vbCrLf
sConsulta = sConsulta & "            ELSE" & vbCrLf
sConsulta = sConsulta & "               SELECT @ORDENATRIB=MAX(ORDEN_ATRIB) FROM PROCE_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO " & vbCrLf
sConsulta = sConsulta & "                IF @ORDENATRIB IS NULL " & vbCrLf
sConsulta = sConsulta & "                   SET @ORDENATRIB=1" & vbCrLf
sConsulta = sConsulta & "            ELSE" & vbCrLf
sConsulta = sConsulta & "               SET @ORDENATRIB=@ORDENATRIB +1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                         INSERT INTO PROCE_ATRIB(ANYO,GMN1,PROCE,GRUPO,ATRIB,INTERNO,INTRO,PREFBAJO,MAXNUM,MINNUM" & vbCrLf
sConsulta = sConsulta & "                                          ,MAXFEC,MINFEC,AMBITO,OBLIG,OP_PREC,APLIC_PREC" & vbCrLf
sConsulta = sConsulta & "                                         ,DEF_PREC,POND,OP_POND,NUM_POND,POND_SI,POND_NO,ORDEN_ATRIB)" & vbCrLf
sConsulta = sConsulta & "                             VALUES(@ANYO,@GMN1,@PROCE,@GRUPO,@ATRIB,@INTERNO,@INTRO,@PREFBAJO,@MAXNUM  ,@MINNUM" & vbCrLf
sConsulta = sConsulta & "                                        ,CONVERT(VARCHAR(10),@MAXFEC,111),CONVERT(VARCHAR(10),@MINFEC,111),@AMBITO,@OBLIG,@OP_PREC ,@APLIC_PREC" & vbCrLf
sConsulta = sConsulta & "                                        ,@DEF_PREC,@POND,@OP_POND,@NUM,@POND_SI,@POND_NO,@ORDENATRIB)" & vbCrLf
sConsulta = sConsulta & "                        SELECT @INS=ID,@FECACT=FECACT FROM PROCE_ATRIB WHERE ID=(SELECT MAX(ID)  FROM PROCE_ATRIB )" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "                         IF @INTRO = 1 " & vbCrLf
sConsulta = sConsulta & "                                 INSERT INTO PROCE_ATRIB_LISTA (ATRIB_ID, ORDEN, VALOR_NUM,VALOR_TEXT,VALOR_FEC,VALOR_POND) " & vbCrLf
sConsulta = sConsulta & "                                     SELECT @INS, ORDEN, VALOR_NUM,VALOR_TEXT,VALOR_FEC,VALOR_POND FROM DEF_ATRIB_LISTA WHERE ATRIB=@ATRIB" & vbCrLf
sConsulta = sConsulta & "                         INSERT INTO PROCE_ATRIB_POND (ATRIB_ID, ID, DESDE_NUM,HASTA_NUM,DESDE_FEC,HASTA_FEC,VALOR_POND) " & vbCrLf
sConsulta = sConsulta & "                                     SELECT @INS, ID, DESDE_NUM,HASTA_NUM,DESDE_FEC,HASTA_FEC,VALOR_POND FROM DEF_ATRIB_POND WHERE ATRIB=@ATRIB" & vbCrLf
sConsulta = sConsulta & "                            " & vbCrLf
sConsulta = sConsulta & "                         SET @PONDE =@POND          " & vbCrLf
sConsulta = sConsulta & "             END  " & vbCrLf
sConsulta = sConsulta & "      ELSE" & vbCrLf
sConsulta = sConsulta & "                 IF @TABLA=2" & vbCrLf
sConsulta = sConsulta & "                       BEGIN" & vbCrLf
sConsulta = sConsulta & "                            IF @MOD_ATRIB=1" & vbCrLf
sConsulta = sConsulta & "                                       UPDATE DEF_ATRIB SET PREFBAJO=@PREFBAJO,INTRO=@INTRO,MAXNUM=@MAXNUM " & vbCrLf
sConsulta = sConsulta & "                                        ,MINNUM=@MINNUM,MAXFEC=CONVERT(VARCHAR(10),@MAXFEC,111),MINFEC=CONVERT(VARCHAR(10)" & vbCrLf
sConsulta = sConsulta & "                                        ,@MINFEC,111) WHERE ID=@ATRIB" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "                                        SELECT @INTRO_D=INTRO,@POND=POND,@OP_POND=FORMULA,@NUM=NUM,@POND_SI=POND_SI,@POND_NO=POND_NO" & vbCrLf
sConsulta = sConsulta & "                                            FROM DEF_ATRIB WHERE ID=@ATRIB" & vbCrLf
sConsulta = sConsulta & "                                        IF @INTRO_D<>@INTRO" & vbCrLf
sConsulta = sConsulta & "                                              IF @INTRO=0 AND @POND=2" & vbCrLf
sConsulta = sConsulta & "                                                    SET @POND=0" & vbCrLf
sConsulta = sConsulta & "                   IF @GRUPO IS NULL " & vbCrLf
sConsulta = sConsulta & "                        SELECT @ORDENATRIB=MAX(ORDEN_ATRIB) FROM PLANTILLA_ATRIB WHERE PLANT=@PLANTILLA AND GRUPO IS NULL" & vbCrLf
sConsulta = sConsulta & "                   ELSE" & vbCrLf
sConsulta = sConsulta & "                        SELECT @ORDENATRIB=MAX(ORDEN_ATRIB) FROM PLANTILLA_ATRIB WHERE PLANT=@PLANTILLA AND GRUPO=@GRUPO " & vbCrLf
sConsulta = sConsulta & "                   IF @ORDENATRIB IS NULL " & vbCrLf
sConsulta = sConsulta & "                        SET @ORDENATRIB=1" & vbCrLf
sConsulta = sConsulta & "                   ELSE" & vbCrLf
sConsulta = sConsulta & "                        SET @ORDENATRIB=@ORDENATRIB +1" & vbCrLf
sConsulta = sConsulta & "                                        INSERT INTO PLANTILLA_ATRIB(PLANT,GRUPO,ATRIB,INTERNO,INTRO,PREFBAJO,MAXNUM,MINNUM" & vbCrLf
sConsulta = sConsulta & "                                            ,MAXFEC,MINFEC,AMBITO,OBLIG,OP_PREC,APLIC_PREC" & vbCrLf
sConsulta = sConsulta & "                                            ,DEF_PREC,POND,OP_POND,NUM_POND,POND_SI,POND_NO,ORDEN_ATRIB)" & vbCrLf
sConsulta = sConsulta & "                                        VALUES(@PLANTILLA,@GRUPO,@ATRIB,@INTERNO,@INTRO,@PREFBAJO,@MAXNUM  ,@MINNUM" & vbCrLf
sConsulta = sConsulta & "                                           ,CONVERT(VARCHAR(10),@MAXFEC,111),CONVERT(VARCHAR(10),@MINFEC,111),@AMBITO,@OBLIG,@OP_PREC ,@APLIC_PREC" & vbCrLf
sConsulta = sConsulta & "                                           ,@DEF_PREC,@POND,@OP_POND,@NUM,@POND_SI,@POND_NO,@ORDENATRIB)" & vbCrLf
sConsulta = sConsulta & "                                        SELECT @INS=ID,@FECACT=FECACT FROM PLANTILLA_ATRIB WHERE ID=(SELECT MAX(ID)  FROM PLANTILLA_ATRIB )" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "                                         IF @INTRO = 1 " & vbCrLf
sConsulta = sConsulta & "                                             INSERT INTO PLANTILLA_ATRIB_LISTA (ATRIB_ID, ORDEN, VALOR_NUM,VALOR_TEXT,VALOR_FEC,VALOR_POND) " & vbCrLf
sConsulta = sConsulta & "                                                SELECT @INS, ORDEN, VALOR_NUM,VALOR_TEXT,VALOR_FEC,VALOR_POND FROM DEF_ATRIB_LISTA WHERE ATRIB=@ATRIB" & vbCrLf
sConsulta = sConsulta & "                                        INSERT INTO PLANTILLA_ATRIB_POND (ATRIB_ID, ID, DESDE_NUM,HASTA_NUM,DESDE_FEC,HASTA_FEC,VALOR_POND) " & vbCrLf
sConsulta = sConsulta & "                                                SELECT @INS, ID, DESDE_NUM,HASTA_NUM,DESDE_FEC,HASTA_FEC,VALOR_POND FROM DEF_ATRIB_POND WHERE ATRIB=@ATRIB" & vbCrLf
sConsulta = sConsulta & "                                        SET @PONDE =@POND" & vbCrLf
sConsulta = sConsulta & "                  END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE ATR_MODIFICAR  @TABLA SMALLINT,@ID INTEGER,@DEN VARCHAR(100)=NULL,@TIPO SMALLINT=NULL, @PREFBAJO TINYINT , @INTRO TINYINT, @MINNUM FLOAT=NULL,@MINFEC DATETIME=NULL,@MAXNUM FLOAT=NULL,@MAXFEC DATETIME=NULL,@ATRIB INTEGER=NULL,@INTERNO  TINYINT=NULL,@AMBITO TINYINT=NULL,@OBLIG  TINYINT=NULL,@OP_PREC VARCHAR(2)=NULL,@APLIC_PREC VARCHAR(50)=NULL,@DEF_PREC TINYINT=NULL,@ANYO VARCHAR(50)=NULL,@GMN1 VARCHAR(50)=NULL,@PROCE VARCHAR(50)=NULL,@GRUPO VARCHAR(50)=NULL , @FECACT DATETIME OUTPUT,@ORDEN SMALLINT=NULL OUTPUT,@PLANT VARCHAR(50)=NULL AS " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @POND AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @AMBITOOLD AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @COUNT AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @APLI_PREC_V AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @ORDEN_V AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @ATRIB_PREC AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @USAR_PREC AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRIMERO AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @TIPOOLD AS INTEGER" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @TABLA=0  " & vbCrLf
sConsulta = sConsulta & "     BEGIN --@tabla=0" & vbCrLf
sConsulta = sConsulta & "            SELECT   @POND=POND,@TIPOOLD=TIPO  FROM DEF_ATRIB  WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "            IF @POND = 1 AND @TIPO<> 2 and @TIPO<>3  " & vbCrLf
sConsulta = sConsulta & "                         SET @POND = 0" & vbCrLf
sConsulta = sConsulta & "            ELSE " & vbCrLf
sConsulta = sConsulta & "                          IF @POND = 2 AND (@TIPO=4 OR @INTRO=0)" & vbCrLf
sConsulta = sConsulta & "                                       SET @POND= 0" & vbCrLf
sConsulta = sConsulta & "                          ELSE" & vbCrLf
sConsulta = sConsulta & "                                        IF  @POND = 3 AND @TIPO<> 2 " & vbCrLf
sConsulta = sConsulta & "                                                   SET @POND= 0" & vbCrLf
sConsulta = sConsulta & "                                        ELSE" & vbCrLf
sConsulta = sConsulta & "                                                    IF @POND =5 AND @TIPO<>4" & vbCrLf
sConsulta = sConsulta & "                                                            SET @POND= 0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "            IF @TIPOOLD <> @TIPO " & vbCrLf
sConsulta = sConsulta & "                  IF @TIPOOLD=2" & vbCrLf
sConsulta = sConsulta & "                        BEGIN" & vbCrLf
sConsulta = sConsulta & "                        UPDATE PROCE_ATRIB SET OP_PREC=NULL, APLIC_PREC=NULL, DEF_PREC=NULL WHERE ATRIB= @ID" & vbCrLf
sConsulta = sConsulta & "                        UPDATE PLANTILLA_ATRIB SET OP_PREC=NULL, APLIC_PREC=NULL, DEF_PREC=NULL WHERE ATRIB= @ID" & vbCrLf
sConsulta = sConsulta & "                        END " & vbCrLf
sConsulta = sConsulta & "            UPDATE DEF_ATRIB SET DEN=@DEN,TIPO=@TIPO,PREFBAJO=@PREFBAJO,POND=@POND,INTRO=@INTRO,MAXNUM=CAST(@MAXNUM AS VARCHAR(15)) ,MINNUM=CAST(@MINNUM AS VARCHAR(15)),MAXFEC=CONVERT(VARCHAR(10),@MAXFEC,111),MINFEC=CONVERT(VARCHAR(10),@MINFEC,111) WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "            SELECT @FECACT=FECACT FROM DEF_ATRIB WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "      END --@tabla=0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @TABLA=1" & vbCrLf
sConsulta = sConsulta & "    BEGIN -- @tabla=1" & vbCrLf
sConsulta = sConsulta & "       IF @APLIC_PREC IS not  NULL  AND @ORDEN is  NULL " & vbCrLf
sConsulta = sConsulta & "                 BEGIN --@orden=null" & vbCrLf
sConsulta = sConsulta & "                 SELECT @ATRIB_PREC =ATRIB  FROM PROCE_ATRIB WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "                 SELECT @ORDEN=ORDEN FROM PROCE_ATRIB WHERE  ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND APLIC_PREC = @APLIC_PREC AND" & vbCrLf
sConsulta = sConsulta & "                    OP_PREC=@OP_PREC AND ATRIB =@ATRIB_PREC" & vbCrLf
sConsulta = sConsulta & "                      " & vbCrLf
sConsulta = sConsulta & "                 SET @APLI_PREC_V=@APLIC_PREC" & vbCrLf
sConsulta = sConsulta & "                 SET @ORDEN_V= 0" & vbCrLf
sConsulta = sConsulta & "                 SET @PRIMERO=0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                 WHILE @ORDEN_V=0 and @APLI_PREC_V<=3" & vbCrLf
sConsulta = sConsulta & "                         BEGIN -- while" & vbCrLf
sConsulta = sConsulta & "                             SELECT  @ORDEN_V = MAX(ORDEN) FROM PROCE_ATRIB WHERE  ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND APLIC_PREC = @APLI_PREC_V" & vbCrLf
sConsulta = sConsulta & "                             IF @ORDEN_V IS NULL" & vbCrLf
sConsulta = sConsulta & "                               begin --@ORDEN_V =null" & vbCrLf
sConsulta = sConsulta & "                                 IF @APLIC_PREC=3" & vbCrLf
sConsulta = sConsulta & "                                     begin" & vbCrLf
sConsulta = sConsulta & "                                       SET @ORDEN_V=1 " & vbCrLf
sConsulta = sConsulta & "                                       SET @PRIMERO=1" & vbCrLf
sConsulta = sConsulta & "                                     end" & vbCrLf
sConsulta = sConsulta & "                                 ELSE                   " & vbCrLf
sConsulta = sConsulta & "                                   begin" & vbCrLf
sConsulta = sConsulta & "                                      SET @ORDEN_V= 0" & vbCrLf
sConsulta = sConsulta & "                                      SET @APLI_PREC_V=@APLI_PREC_V + 1" & vbCrLf
sConsulta = sConsulta & "                                   end" & vbCrLf
sConsulta = sConsulta & "                               end  --@ORDEN_V =null" & vbCrLf
sConsulta = sConsulta & "                         END -- while" & vbCrLf
sConsulta = sConsulta & "                if @ORDEN_V IS NULL  SET @ORDEN_V=0" & vbCrLf
sConsulta = sConsulta & "                      if @PRIMERO=1" & vbCrLf
sConsulta = sConsulta & "                              UPDATE PROCE_ATRIB SET ORDEN=1 WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "                     else" & vbCrLf
sConsulta = sConsulta & "                              UPDATE PROCE_ATRIB SET ORDEN=@ORDEN_V+1 WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "                UPDATE PROCE_ATRIB SET ORDEN = ORDEN +1 WHERE  APLIC_PREC < @APLIC_PREC AND ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ID<>@ID" & vbCrLf
sConsulta = sConsulta & "               SET @ORDEN=@ORDEN_V +1" & vbCrLf
sConsulta = sConsulta & "              " & vbCrLf
sConsulta = sConsulta & "             END --@orden=null" & vbCrLf
sConsulta = sConsulta & "       SELECT @POND=POND,@AMBITOOLD=AMBITO,@USAR_PREC=USAR_PREC FROM PROCE_ATRIB WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "       IF @INTRO=0 AND @POND=2 SET @POND=0" & vbCrLf
sConsulta = sConsulta & "       IF @DEF_PREC=1 SET @USAR_PREC=1" & vbCrLf
sConsulta = sConsulta & "       UPDATE PROCE_ATRIB SET PREFBAJO=@PREFBAJO,INTRO=@INTRO,MAXNUM=@MAXNUM  ," & vbCrLf
sConsulta = sConsulta & "                  MINNUM=@MINNUM,MAXFEC=CONVERT(VARCHAR(10),@MAXFEC,111)," & vbCrLf
sConsulta = sConsulta & "                  MINFEC=CONVERT(VARCHAR(10),@MINFEC,111),INTERNO=@INTERNO,AMBITO=@AMBITO,OBLIG=@OBLIG," & vbCrLf
sConsulta = sConsulta & "                  OP_PREC=@OP_PREC,APLIC_PREC=@APLIC_PREC,USAR_PREC=@USAR_PREC," & vbCrLf
sConsulta = sConsulta & "                  DEF_PREC=@DEF_PREC,POND=@POND WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "       SELECT @FECACT=FECACT FROM PROCE_ATRIB WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "       IF   @ATRIB IS NOT  NULL" & vbCrLf
sConsulta = sConsulta & "             BEGIN --@atrib not null" & vbCrLf
sConsulta = sConsulta & "                    SELECT @POND=POND FROM DEF_ATRIB WHERE ID=@ATRIB" & vbCrLf
sConsulta = sConsulta & "                    IF @INTRO=0 AND @POND=2 SET @POND=0" & vbCrLf
sConsulta = sConsulta & "                       UPDATE DEF_ATRIB SET PREFBAJO=@PREFBAJO,INTRO=@INTRO,MAXNUM=@MAXNUM ," & vbCrLf
sConsulta = sConsulta & "                           MINNUM=@MINNUM,MAXFEC=CONVERT(VARCHAR(10),@MAXFEC,111)," & vbCrLf
sConsulta = sConsulta & "                           MINFEC=CONVERT(VARCHAR(10),@MINFEC,111),POND=@POND WHERE ID=@ATRIB" & vbCrLf
sConsulta = sConsulta & "             END--@atrib not null" & vbCrLf
sConsulta = sConsulta & "--           END estaba mal" & vbCrLf
sConsulta = sConsulta & "       IF @AMBITO<>@AMBITOOLD" & vbCrLf
sConsulta = sConsulta & "               BEGIN --@ambito<>ambitoold" & vbCrLf
sConsulta = sConsulta & "                     SELECT @COUNT=COUNT(*) FROM PROCE_OFE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "                     IF @COUNT > 0" & vbCrLf
sConsulta = sConsulta & "                          IF  @AMBITOOLD>@AMBITO" & vbCrLf
sConsulta = sConsulta & "                               IF @AMBITOOLD=3" & vbCrLf
sConsulta = sConsulta & "                                     begin --old=3" & vbCrLf
sConsulta = sConsulta & "                                           DELETE OFE_ITEM_ATRIB WHERE  ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ATRIB_ID=@ID" & vbCrLf
sConsulta = sConsulta & "                                           DELETE CONF_VISTAS_GRUPO_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ATRIB=@ID" & vbCrLf
sConsulta = sConsulta & "                                           DELETE CONF_VISTAS_ALL_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE  AND ATRIB=@ID" & vbCrLf
sConsulta = sConsulta & "                                           DELETE CONF_VISTAS_ITEM_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE  AND ATRIB=@ID" & vbCrLf
sConsulta = sConsulta & "                                           INSERT INTO CONF_VISTAS_PROCE_ATRIB (ANYO,GMN1,PROCE,USU,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                              SELECT @ANYO,@GMN1,@PROCE,USU,VISTA,@ID FROM  CONF_VISTAS_PROCE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "                                     end  --old=3" & vbCrLf
sConsulta = sConsulta & "                               ELSE --ser�a old=2" & vbCrLf
sConsulta = sConsulta & "                                           DELETE OFE_GR_ATRIB WHERE  ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ATRIB_ID=@ID AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                          ELSE  --old < ambito" & vbCrLf
sConsulta = sConsulta & "                                 IF @AMBITOOLD=1" & vbCrLf
sConsulta = sConsulta & "                                     BEGIN --old=1" & vbCrLf
sConsulta = sConsulta & "                                        IF @AMBITO=2" & vbCrLf
sConsulta = sConsulta & "                                                INSERT INTO OFE_GR_ATRIB (ANYO, GMN1, PROCE,GRUPO,PROVE,OFE,ATRIB_ID,VALOR_NUM,VALOR_TEXT," & vbCrLf
sConsulta = sConsulta & "                                                      VALOR_FEC,VALOR_BOOL,VALOR_POND) " & vbCrLf
sConsulta = sConsulta & "                                                      SELECT DISTINCT OFE_ATRIB.ANYO, OFE_ATRIB.GMN1, OFE_ATRIB.PROCE,PROCE_ATRIB.GRUPO" & vbCrLf
sConsulta = sConsulta & "                                                                   ,OFE_ATRIB.PROVE,OFE_ATRIB.OFE,OFE_ATRIB.ATRIB_ID,OFE_ATRIB.VALOR_NUM,OFE_ATRIB.VALOR_TEXT" & vbCrLf
sConsulta = sConsulta & "                                                                   ,OFE_ATRIB.VALOR_FEC,OFE_ATRIB.VALOR_BOOL,OFE_ATRIB.VALOR_POND " & vbCrLf
sConsulta = sConsulta & "                                                     FROM OFE_ATRIB " & vbCrLf
sConsulta = sConsulta & "                                                     INNER JOIN PROCE_ATRIB ON  OFE_ATRIB.ANYO=PROCE_ATRIB.ANYO" & vbCrLf
sConsulta = sConsulta & "                                                                 AND OFE_ATRIB.GMN1=PROCE_ATRIB.GMN1 AND OFE_ATRIB.PROCE =PROCE_ATRIB.PROCE " & vbCrLf
sConsulta = sConsulta & "                                                                 AND PROCE_ATRIB.GRUPO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                                                      WHERE OFE_ATRIB.ATRIB_ID=@ID AND OFE_ATRIB.ANYO=@ANYO AND OFE_ATRIB.GMN1=@GMN1 AND OFE_ATRIB.PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "                                        DELETE OFE_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ATRIB_ID=@ID" & vbCrLf
sConsulta = sConsulta & "                                        IF @AMBITO = 3" & vbCrLf
sConsulta = sConsulta & "                                                  begin --ambito=3" & vbCrLf
sConsulta = sConsulta & "                                                  DELETE CONF_VISTAS_PROCE_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ATRIB=@ID" & vbCrLf
sConsulta = sConsulta & "                                                  IF @GRUPO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                                                        INSERT INTO CONF_VISTAS_GRUPO_ATRIB (ANYO,GMN1,PROCE,GRUPO,USU,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                                          SELECT @ANYO,@GMN1,@PROCE,@GRUPO,USU,VISTA,@ID FROM  CONF_VISTAS_GRUPO WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO " & vbCrLf
sConsulta = sConsulta & "                                                  ELSE" & vbCrLf
sConsulta = sConsulta & "                                                        INSERT INTO CONF_VISTAS_GRUPO_ATRIB (ANYO,GMN1,PROCE,GRUPO,USU,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                                          SELECT @ANYO,@GMN1,@PROCE,GRUPO,USU,VISTA,@ID FROM  CONF_VISTAS_GRUPO WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE         " & vbCrLf
sConsulta = sConsulta & "                                                 INSERT INTO CONF_VISTAS_ALL_ATRIB (ANYO,GMN1,PROCE,USU,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                                     SELECT @ANYO,@GMN1,@PROCE,USU,VISTA,@ID FROM  CONF_VISTAS_ALL WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "                                                 IF @GRUPO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                                                     INSERT INTO CONF_VISTAS_ITEM_ATRIB (ANYO,GMN1,PROCE,GRUPO,USU,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                                         SELECT @ANYO,@GMN1,@PROCE,@GRUPO,USU,VISTA,@ID FROM  CONF_VISTAS_ITEM WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO " & vbCrLf
sConsulta = sConsulta & "                                                  ELSE" & vbCrLf
sConsulta = sConsulta & "                                                     INSERT INTO CONF_VISTAS_ITEM_ATRIB (ANYO,GMN1,PROCE,GRUPO,USU,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                                         SELECT @ANYO,@GMN1,@PROCE,GRUPO,USU,VISTA,@ID FROM  CONF_VISTAS_ITEM WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                                                 end--ambito=3" & vbCrLf
sConsulta = sConsulta & "                                     END --old=1" & vbCrLf
sConsulta = sConsulta & "                                ELSE " & vbCrLf
sConsulta = sConsulta & "                                    BEGIN  --old es 2" & vbCrLf
sConsulta = sConsulta & "                                      DELETE OFE_GR_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ATRIB_ID=@ID AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "                                      DELETE CONF_VISTAS_PROCE_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ATRIB=@ID" & vbCrLf
sConsulta = sConsulta & "                                      IF @GRUPO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                                                 INSERT INTO CONF_VISTAS_GRUPO_ATRIB (ANYO,GMN1,PROCE,GRUPO,USU,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                                       SELECT @ANYO,@GMN1,@PROCE,@GRUPO,USU,VISTA,@ID FROM  CONF_VISTAS_GRUPO WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO " & vbCrLf
sConsulta = sConsulta & "                                      ELSE" & vbCrLf
sConsulta = sConsulta & "                                                INSERT INTO CONF_VISTAS_GRUPO_ATRIB (ANYO,GMN1,PROCE,GRUPO,USU,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                                       SELECT @ANYO,@GMN1,@PROCE,GRUPO,USU,VISTA,@ID FROM  CONF_VISTAS_GRUPO WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "                                      INSERT INTO CONF_VISTAS_ALL_ATRIB (ANYO,GMN1,PROCE,USU,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                           SELECT @ANYO,@GMN1,@PROCE,USU,VISTA,@ID FROM  CONF_VISTAS_ALL WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "                                      IF @GRUPO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                                                 INSERT INTO CONF_VISTAS_ITEM_ATRIB (ANYO,GMN1,PROCE,GRUPO,USU,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                                      SELECT @ANYO,@GMN1,@PROCE,@GRUPO,USU,VISTA,@ID FROM  CONF_VISTAS_ITEM WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO " & vbCrLf
sConsulta = sConsulta & "                                      ELSE" & vbCrLf
sConsulta = sConsulta & "                                                 INSERT INTO CONF_VISTAS_ITEM_ATRIB (ANYO,GMN1,PROCE,GRUPO,USU,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                                      SELECT @ANYO,@GMN1,@PROCE,GRUPO,USU,VISTA,@ID FROM  CONF_VISTAS_ITEM WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                                    END--old es 2" & vbCrLf
sConsulta = sConsulta & "               END --@ambito<>ambitoold" & vbCrLf
sConsulta = sConsulta & "         END -- @tabla=1" & vbCrLf
sConsulta = sConsulta & "IF @TABLA=2" & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "          IF @APLIC_PREC IS not  NULL  AND @ORDEN is  NULL " & vbCrLf
sConsulta = sConsulta & "                 BEGIN" & vbCrLf
sConsulta = sConsulta & "                     SELECT @ATRIB_PREC =ATRIB  FROM PLANTILLA_ATRIB WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "                     SELECT @ORDEN=ORDEN FROM PLANTILLA_ATRIB WHERE PLANT=@PLANT AND APLIC_PREC = @APLIC_PREC AND OP_PREC=@OP_PREC AND ATRIB =@ATRIB_PREC" & vbCrLf
sConsulta = sConsulta & "                     IF @ORDEN IS NULL" & vbCrLf
sConsulta = sConsulta & "                         BEGIN" & vbCrLf
sConsulta = sConsulta & "                           SET @APLI_PREC_V=@APLIC_PREC" & vbCrLf
sConsulta = sConsulta & "                           SET @ORDEN_V= 0" & vbCrLf
sConsulta = sConsulta & "                           WHILE @APLI_PREC_V < 3  AND @ORDEN_V= 0" & vbCrLf
sConsulta = sConsulta & "                                 BEGIN" & vbCrLf
sConsulta = sConsulta & "                                     SELECT  @ORDEN_V = MAX(ORDEN) FROM PLANTILLA_ATRIB WHERE  PLANT=@PLANT AND APLIC_PREC = @APLI_PREC_V" & vbCrLf
sConsulta = sConsulta & "                                      IF @ORDEN_V IS NULL" & vbCrLf
sConsulta = sConsulta & "                                         begin" & vbCrLf
sConsulta = sConsulta & "                                              SET @ORDEN_V= 0" & vbCrLf
sConsulta = sConsulta & "                                              SET @APLI_PREC_V=@APLI_PREC_V + 1" & vbCrLf
sConsulta = sConsulta & "                                         end" & vbCrLf
sConsulta = sConsulta & "                                END" & vbCrLf
sConsulta = sConsulta & "                          UPDATE PLANTILLA_ATRIB SET ORDEN = ORDEN +1 WHERE  APLIC_PREC < @APLIC_PREC AND PLANT=@PLANT" & vbCrLf
sConsulta = sConsulta & "                          SET @ORDEN=@ORDEN_V +1" & vbCrLf
sConsulta = sConsulta & "                        END" & vbCrLf
sConsulta = sConsulta & "                   else" & vbCrLf
sConsulta = sConsulta & "                      IF @APLIC_PREC IS not  NULL   " & vbCrLf
sConsulta = sConsulta & "                            UPDATE PLANTILLA_ATRIB SET ORDEN = ORDEN +1 WHERE  APLIC_PREC < @APLIC_PREC AND PLANT=@PLANT" & vbCrLf
sConsulta = sConsulta & "                 END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "           SELECT @POND=POND, @AMBITOOLD=AMBITO FROM PLANTILLA_ATRIB WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "           IF @INTRO=0 AND @POND=2 SET @POND=0" & vbCrLf
sConsulta = sConsulta & "               UPDATE PLANTILLA_ATRIB SET PREFBAJO=@PREFBAJO,INTRO=@INTRO,MAXNUM=@MAXNUM  ," & vbCrLf
sConsulta = sConsulta & "                    MINNUM=@MINNUM,MAXFEC=CONVERT(VARCHAR(10),@MAXFEC,111)," & vbCrLf
sConsulta = sConsulta & "                    MINFEC=CONVERT(VARCHAR(10),@MINFEC,111),INTERNO=@INTERNO,AMBITO=@AMBITO,OBLIG=@OBLIG," & vbCrLf
sConsulta = sConsulta & "                    OP_PREC=@OP_PREC,APLIC_PREC=@APLIC_PREC," & vbCrLf
sConsulta = sConsulta & "                    DEF_PREC=@DEF_PREC,POND=@POND,ORDEN=@ORDEN WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "          SELECT @FECACT=FECACT FROM PLANTILLA_ATRIB WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "          IF   @ATRIB IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                BEGIN" & vbCrLf
sConsulta = sConsulta & "                    SELECT @POND=POND FROM DEF_ATRIB WHERE ID=@ATRIB" & vbCrLf
sConsulta = sConsulta & "                    IF @INTRO=0 AND @POND=2 SET @POND=0" & vbCrLf
sConsulta = sConsulta & "                        UPDATE DEF_ATRIB SET PREFBAJO=@PREFBAJO,INTRO=@INTRO,MAXNUM=CAST(@MAXNUM AS VARCHAR(15)) ," & vbCrLf
sConsulta = sConsulta & "                            MINNUM=CAST(@MINNUM AS VARCHAR(15)),MAXFEC=CONVERT(VARCHAR(10),@MAXFEC,111)," & vbCrLf
sConsulta = sConsulta & "                            MINFEC=CONVERT(VARCHAR(10),@MINFEC,111),POND=@POND WHERE ID=@ATRIB" & vbCrLf
sConsulta = sConsulta & "                END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "     IF @AMBITO<>@AMBITOOLD" & vbCrLf
sConsulta = sConsulta & "       BEGIN --@ambito<>ambitoold           " & vbCrLf
sConsulta = sConsulta & "            IF  @AMBITOOLD>@AMBITO" & vbCrLf
sConsulta = sConsulta & "       begin" & vbCrLf
sConsulta = sConsulta & "             IF @AMBITOOLD=3" & vbCrLf
sConsulta = sConsulta & "                   begin --old=3" & vbCrLf
sConsulta = sConsulta & "                                              " & vbCrLf
sConsulta = sConsulta & "                         DELETE PLANTILLA_CONF_VISTAS_GRUPO_ATRIB WHERE PLANT=@PLANT AND ATRIB=@ID" & vbCrLf
sConsulta = sConsulta & "                         DELETE PLANTILLA_CONF_VISTAS_ALL_ATRIB WHERE PLANT=@PLANT AND ATRIB=@ID" & vbCrLf
sConsulta = sConsulta & "                                              " & vbCrLf
sConsulta = sConsulta & "                         INSERT INTO PLANTILLA_CONF_VISTAS_PROCE_ATRIB (PLANT,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                SELECT @PLANT,VISTA,@ID FROM  PLANTILLA_CONF_VISTAS_PROCE WHERE PLANT=@PLANT " & vbCrLf
sConsulta = sConsulta & "                   end  --old=3" & vbCrLf
sConsulta = sConsulta & "       end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "        ELSE  --old < ambito" & vbCrLf
sConsulta = sConsulta & "           IF @AMBITOOLD=1" & vbCrLf
sConsulta = sConsulta & "               BEGIN --old=1" & vbCrLf
sConsulta = sConsulta & "                    IF @AMBITO = 3" & vbCrLf
sConsulta = sConsulta & "                       begin --ambito=3" & vbCrLf
sConsulta = sConsulta & "                            DELETE PLANTILLA_CONF_VISTAS_PROCE_ATRIB WHERE PLANT=@PLANT AND ATRIB=@ID" & vbCrLf
sConsulta = sConsulta & "                            IF @GRUPO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                                   INSERT INTO PLANTILLA_CONF_VISTAS_GRUPO_ATRIB (PLANT,GRUPO,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                          SELECT @PLANT,@GRUPO,VISTA,@ID FROM  PLANTILLA_CONF_VISTAS_GRUPO WHERE PLANT=@PLANT AND GRUPO=@GRUPO " & vbCrLf
sConsulta = sConsulta & "                            ELSE" & vbCrLf
sConsulta = sConsulta & "                                   INSERT INTO PLANTILLA_CONF_VISTAS_GRUPO_ATRIB (PLANT,GRUPO,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                          SELECT @PLANT,GRUPO,VISTA,@ID FROM  PLANTILLA_CONF_VISTAS_GRUPO WHERE PLANT=@PLANT         " & vbCrLf
sConsulta = sConsulta & "                            INSERT INTO PLANTILLA_CONF_VISTAS_ALL_ATRIB (PLANT,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                   SELECT @PLANT,VISTA,@ID FROM PLANTILLA_CONF_VISTAS_ALL WHERE PLANT=@PLANT " & vbCrLf
sConsulta = sConsulta & "               end--ambito=3" & vbCrLf
sConsulta = sConsulta & "                 END --old=1" & vbCrLf
sConsulta = sConsulta & "             ELSE " & vbCrLf
sConsulta = sConsulta & "                 BEGIN  --old es 2" & vbCrLf
sConsulta = sConsulta & "                      DELETE PLANTILLA_CONF_VISTAS_PROCE_ATRIB WHERE PLANT=@PLANT AND ATRIB=@ID" & vbCrLf
sConsulta = sConsulta & "                      IF @GRUPO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                             INSERT INTO PLANTILLA_CONF_VISTAS_GRUPO_ATRIB (PLANT,GRUPO,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                    SELECT @PLANT,@GRUPO,VISTA,@ID FROM  PLANTILLA_CONF_VISTAS_GRUPO WHERE PLANT=@PLANT AND GRUPO=@GRUPO " & vbCrLf
sConsulta = sConsulta & "                      ELSE" & vbCrLf
sConsulta = sConsulta & "                             INSERT INTO PLANTILLA_CONF_VISTAS_GRUPO_ATRIB (PLANT,GRUPO,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                    SELECT @PLANT,GRUPO,VISTA,@ID FROM  PLANTILLA_CONF_VISTAS_GRUPO WHERE PLANT=@PLANT " & vbCrLf
sConsulta = sConsulta & "                      INSERT INTO PLANTILLA_CONF_VISTAS_ALL_ATRIB (PLANT,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                             SELECT @PLANT,VISTA,@ID FROM PLANTILLA_CONF_VISTAS_ALL WHERE PLANT=@PLANT " & vbCrLf
sConsulta = sConsulta & "                                             " & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "                END--old es 2" & vbCrLf
sConsulta = sConsulta & "       END --@ambito<>ambitoold" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE PLANTILLA_GR_COD (@OLD VARCHAR(50),@NEW VARCHAR(50),@PLANTILLA INT)  AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PLANT  AS INT" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT ON" & vbCrLf
sConsulta = sConsulta & "BEGIN TRANSACTION" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR FOR SELECT COD,PLANT  FROM PLANTILLA_GR WHERE COD=@OLD  AND PLANT=@PLANTILLA" & vbCrLf
sConsulta = sConsulta & "OPEN C" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C INTO @VAR_COD,@PLANT" & vbCrLf
sConsulta = sConsulta & "IF @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PLANTILLA_GR NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PLANTILLA_GR_DEF NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PLANTILLA_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PLANTILLA_CONF_VISTAS_GRUPO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PLANTILLA_CONF_VISTAS_GRUPO_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE PLANTILLA_GR SET COD=@NEW WHERE COD=@OLD AND PLANT =@PLANTILLA" & vbCrLf
sConsulta = sConsulta & "UPDATE PLANTILLA_GR_DEF SET GRUPO=@NEW WHERE GRUPO=@OLD  AND PLANT =@PLANTILLA" & vbCrLf
sConsulta = sConsulta & "UPDATE PLANTILLA_ATRIB SET GRUPO=@NEW WHERE  PLANT =@PLANTILLA AND  GRUPO=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PLANTILLA_CONF_VISTAS_GRUPO SET GRUPO=@NEW WHERE  PLANT =@PLANTILLA AND  GRUPO=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PLANTILLA_CONF_VISTAS_GRUPO_ATRIB SET GRUPO=@NEW WHERE  PLANT =@PLANTILLA AND  GRUPO=@OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PLANTILLA_GR CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PLANTILLA_GR_DEF CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PLANTILLA_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PLANTILLA_CONF_VISTAS_GRUPO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PLANTILLA_CONF_VISTAS_GRUPO_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close C" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT OFF" & vbCrLf
sConsulta = sConsulta & "COMMIT TRANSACTION" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE   PROCEDURE  SP_AR_AHORROS_CALCULAR ( @ANYO SMALLINT,@GMN1 VARCHAR(50), @PROCE INT,@RES INT OUTPUT)  AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE @EST SMALLINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ITEM INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @CAMBIO AS FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ADJDIR SMALLINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @FECULT DATETIME" & vbCrLf
sConsulta = sConsulta & "DECLARE @DEN VARCHAR(200)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PORTAL SMALLINT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @PORTAL=INSTWEB FROM PARGEN_INTERNO WHERE ID=1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @EST=EST,@CAMBIO=CAMBIO,@ADJDIR=ADJDIR,@FECULT=FECULTREU,@DEN=DEN FROM PROCE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE AND CALCPEND=1 AND EST>=11 AND EST<20" & vbCrLf
sConsulta = sConsulta & "--IF @@FETCH_STATUS<>0 GOTO ERR_" & vbCrLf
sConsulta = sConsulta & "   IF @EST = 11 " & vbCrLf
sConsulta = sConsulta & "     BEGIN" & vbCrLf
sConsulta = sConsulta & "          DECLARE Cur_ITEM CURSOR LOCAL FOR SELECT ID FROM ITEM WHERE ITEM.ANYO=@ANYO AND ITEM. GMN1=@GMN1 AND ITEM.PROCE=@PROCE AND ITEM.EST=1 AND ITEM.CONF=1" & vbCrLf
sConsulta = sConsulta & "            AND NOT EXISTS (SELECT * FROM AR_ITEM WHERE AR_ITEM.ANYO=@ANYO AND AR_ITEM.GMN1=@GMN1 AND AR_ITEM.PROCE=@PROCE AND AR_ITEM.ITEM=ITEM.ID) " & vbCrLf
sConsulta = sConsulta & "          OPEN Cur_ITEM " & vbCrLf
sConsulta = sConsulta & "          FETCH NEXT FROM Cur_ITEM INTO @ITEM" & vbCrLf
sConsulta = sConsulta & "          WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "           BEGIN" & vbCrLf
sConsulta = sConsulta & "                 INSERT INTO AR_ITEM (ANYO,GMN1,PROCE,ITEM,GRUPO,ADJDIR,GMN2,GMN3,GMN4,PRES,PREC) " & vbCrLf
sConsulta = sConsulta & "                    SELECT @ANYO,@GMN1,@PROCE,I.ID,I.GRUPO,@ADJDIR,I.GMN2,I.GMN3,I.GMN4, (SUM(IA.PORCEN/100) * I.CANT * I.PREC) / @CAMBIO" & vbCrLf
sConsulta = sConsulta & "                        , (SUM(IA.ADJUDICADO) / PO.CAMBIO) / @CAMBIO FROM ITEM_ADJ IA INNER JOIN ITEM I ON IA.ANYO=I.ANYO AND IA.GMN1=I.GMN1 AND IA.PROCE=I.PROCE AND IA.ITEM=I.ID " & vbCrLf
sConsulta = sConsulta & "                          INNER JOIN PROCE_OFE PO ON  IA.ANYO=PO.ANYO AND IA.GMN1=PO.GMN1 AND IA.PROCE=PO.PROCE AND IA.PROVE=PO.PROVE AND IA.OFE=PO.OFE" & vbCrLf
sConsulta = sConsulta & "                           WHERE IA.ANYO= @ANYO AND IA.GMN1= @GMN1 AND IA.PROCE=@PROCE AND IA.ITEM=@ITEM AND IA.PORCEN <>0 " & vbCrLf
sConsulta = sConsulta & "                              GROUP BY I.ANYO,I.GMN1,I.PROCE,I.ID,I.GRUPO,I.GMN2,I.GMN3,I.GMN4,I.CANT , I.PREC,PO.CAMBIO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                EXECUTE SP_CIERRE_PARC @ANYO,@GMN1,@PROCE,@ITEM,@RES output" & vbCrLf
sConsulta = sConsulta & "               IF @RES<>0  AND @RES IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "                   BEGIN" & vbCrLf
sConsulta = sConsulta & "                   CLOSE Cur_ITEM" & vbCrLf
sConsulta = sConsulta & "                   DEALLOCATE Cur_ITEM" & vbCrLf
sConsulta = sConsulta & "                   GOTO ERR_" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "                 " & vbCrLf
sConsulta = sConsulta & "            FETCH NEXT FROM Cur_ITEM INTO @ITEM" & vbCrLf
sConsulta = sConsulta & "            END " & vbCrLf
sConsulta = sConsulta & "            CLOSE Cur_ITEM" & vbCrLf
sConsulta = sConsulta & "            DEALLOCATE Cur_ITEM" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "            DELETE FROM AR_GRUPO WHERE ANYO= @ANYO AND GMN1= @GMN1 AND PROCE= @PROCE     " & vbCrLf
sConsulta = sConsulta & "            INSERT INTO AR_GRUPO (ANYO,GMN1,PROCE,GRUPO,PRES,PREC)  SELECT PG.ANYO,PG.GMN1,PG.PROCE,PG.COD,SUM(AI.PRES),PG.ADJUDICADO / @CAMBIO" & vbCrLf
sConsulta = sConsulta & "               FROM PROCE_GRUPO PG INNER JOIN AR_ITEM AI ON PG.ANYO=AI.ANYO AND PG.GMN1=AI.GMN1 AND PG.PROCE=AI.PROCE AND PG.COD=AI.GRUPO" & vbCrLf
sConsulta = sConsulta & "                WHERE PG.ANYO= @ANYO AND PG.GMN1= @GMN1 AND PG.PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "                GROUP BY PG.ANYO,PG.GMN1,PG.PROCE,PG.COD,PG.ADJUDICADO " & vbCrLf
sConsulta = sConsulta & "         " & vbCrLf
sConsulta = sConsulta & "            EXECUTE SP_AR_PROCE_PARC @ANYO,@GMN1,@PROCE,@RES output" & vbCrLf
sConsulta = sConsulta & "            IF @RES<>0  AND @RES IS NOT NULL GOTO ERR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "            EXECUTE SP_AR_PROVE @ANYO,@GMN1,@PROCE,@RES output" & vbCrLf
sConsulta = sConsulta & "            IF @RES<>0  AND @RES IS NOT NULL GOTO ERR_" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "       END " & vbCrLf
sConsulta = sConsulta & "  ELSE" & vbCrLf
sConsulta = sConsulta & "     BEGIN" & vbCrLf
sConsulta = sConsulta & "       --Ahora almacenamos el contenido en las tablas de resultados para informes" & vbCrLf
sConsulta = sConsulta & "        DELETE FROM AR_ITEM WHERE ANYO= @ANYO AND GMN1= @GMN1 AND PROCE= @PROCE" & vbCrLf
sConsulta = sConsulta & "        DELETE FROM AR_GRUPO WHERE ANYO= @ANYO AND GMN1= @GMN1 AND PROCE= @PROCE" & vbCrLf
sConsulta = sConsulta & "        DELETE FROM AR_PROCE WHERE ANYO= @ANYO AND GMN1= @GMN1 AND COD= @PROCE" & vbCrLf
sConsulta = sConsulta & "       --AR_ITEM" & vbCrLf
sConsulta = sConsulta & "        INSERT INTO AR_ITEM (ANYO,GMN1,PROCE,ITEM,GRUPO,ADJDIR,GMN2,GMN3,GMN4,PRES,PREC) " & vbCrLf
sConsulta = sConsulta & "          SELECT I.ANYO,I.GMN1,I.PROCE,I.ID,I.GRUPO,@ADJDIR,I.GMN2,I.GMN3,I.GMN4, (SUM(IA.PORCEN/100) * I.CANT * I.PREC) / @CAMBIO, (SUM(IA.ADJUDICADO) / PO.CAMBIO) / @CAMBIO" & vbCrLf
sConsulta = sConsulta & "            FROM ITEM_ADJ IA INNER JOIN ITEM I ON IA.ANYO=I.ANYO AND IA.GMN1=I.GMN1 AND IA.PROCE=I.PROCE AND IA.ITEM=I.ID AND I.CONF=1" & vbCrLf
sConsulta = sConsulta & "             INNER JOIN PROCE_OFE PO ON  IA.ANYO=PO.ANYO AND IA.GMN1=PO.GMN1 AND IA.PROCE=PO.PROCE AND IA.PROVE=PO.PROVE AND IA.OFE=PO.OFE" & vbCrLf
sConsulta = sConsulta & "             WHERE IA.ANYO= @ANYO AND IA.GMN1= @GMN1 AND IA.PROCE=@PROCE AND IA.PORCEN <>0" & vbCrLf
sConsulta = sConsulta & "            GROUP BY I.ANYO,I.GMN1,I.PROCE,I.ID,I.GRUPO,I.GMN2,I.GMN3,I.GMN4,I.CANT , I.PREC,PO.CAMBIO" & vbCrLf
sConsulta = sConsulta & "       --AR_GRUPO" & vbCrLf
sConsulta = sConsulta & "        INSERT INTO AR_GRUPO (ANYO,GMN1,PROCE,GRUPO,PRES,PREC)  SELECT PG.ANYO,PG.GMN1,PG.PROCE,PG.COD,SUM(AI.PRES),PG.ADJUDICADO / @CAMBIO" & vbCrLf
sConsulta = sConsulta & "           FROM PROCE_GRUPO PG INNER JOIN AR_ITEM AI ON PG.ANYO=AI.ANYO AND PG.GMN1=AI.GMN1 AND PG.PROCE=AI.PROCE AND PG.COD=AI.GRUPO" & vbCrLf
sConsulta = sConsulta & "           WHERE PG.ANYO= @ANYO AND PG.GMN1= @GMN1 AND PG.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "           GROUP BY PG.ANYO,PG.GMN1,PG.PROCE,PG.COD,PG.ADJUDICADO " & vbCrLf
sConsulta = sConsulta & "        " & vbCrLf
sConsulta = sConsulta & "         EXECUTE SP_CIERRE @ANYO,@GMN1,@PROCE,@RES output" & vbCrLf
sConsulta = sConsulta & "         IF @RES<>0  AND @RES IS NOT NULL GOTO ERR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "      END " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & " IF @PORTAL =2 " & vbCrLf
sConsulta = sConsulta & " BEGIN" & vbCrLf
sConsulta = sConsulta & "    EXECUTE SP_AR_VOLUMENES @ANYO,@GMN1,@PROCE,@FECULT,@DEN,@RES" & vbCrLf
sConsulta = sConsulta & "    IF @RES<>0  AND @RES IS NOT NULL  GOTO ERR_" & vbCrLf
sConsulta = sConsulta & " END " & vbCrLf
sConsulta = sConsulta & " UPDATE PROCE SET CALCPEND=0 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ERR_:" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR <> 0 SET @RES=@@ERROR" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "Return" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE SP_AR_ART4_ADJ (@ANYO SMALLINT,@GMN1 VARCHAR(20),@PROCE INTEGER, @ITEM INTEGER=NULL,@RES INTEGER OUTPUT) AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVE VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @DENPROVE VARCHAR(500)" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN2 VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN3 VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN4 VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @ART VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @DEST VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @UNI VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRECIO FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PORCEN FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @FECINI DATETIME" & vbCrLf
sConsulta = sConsulta & "DECLARE @FECFIN DATETIME" & vbCrLf
sConsulta = sConsulta & "DECLARE @CANTIDAD FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @MAX AS INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @ITEM IS NULL" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "--Ahora almacenamos las adjudicaciones de art�culos en las tablas ADJ_ART" & vbCrLf
sConsulta = sConsulta & "--Primero recupero todas las adjudicaciones" & vbCrLf
sConsulta = sConsulta & "  DECLARE Cur_PROCE CURSOR LOCAL FOR" & vbCrLf
sConsulta = sConsulta & "   SELECT ITEM_ADJ.PROVE AS PROVE,PROVE.DEN AS DENPROVE ,ITEM.GMN2,ITEM.GMN3,ITEM.GMN4,ITEM.ART, ITEM.DEST,ITEM.UNI, " & vbCrLf
sConsulta = sConsulta & "         ITEM_OFE.PREC_VALIDO/(PROCE_OFE.CAMBIO*PROCE.CAMBIO) AS PRECIO, ITEM_ADJ.PORCEN,ITEM.FECINI,ITEM.FECFIN,((PORCEN/100)* ITEM.CANT) AS CANTIDAD" & vbCrLf
sConsulta = sConsulta & "     FROM ITEM_ADJ INNER JOIN ITEM_OFE ON ITEM_ADJ.ANYO=ITEM_OFE.ANYO AND ITEM_ADJ.GMN1=ITEM_OFE.GMN1 AND ITEM_ADJ.PROCE=ITEM_OFE.PROCE AND ITEM_ADJ.OFE=ITEM_OFE.NUM AND ITEM_ADJ.PROVE=ITEM_OFE.PROVE AND ITEM_ADJ.ITEM=ITEM_OFE.ITEM" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PROVE ON ITEM_OFE.PROVE=PROVE.COD" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PROCE_OFE ON ITEM_OFE.ANYO=PROCE_OFE.ANYO AND ITEM_OFE.GMN1=PROCE_OFE.GMN1 AND ITEM_OFE.PROCE=PROCE_OFE.PROCE AND ITEM_OFE.NUM=PROCE_OFE.OFE AND ITEM_OFE.PROVE=PROCE_OFE.PROVE" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN ITEM ON ITEM_OFE.ANYO=ITEM.ANYO AND ITEM_OFE.GMN1=ITEM.GMN1 AND ITEM_OFE.PROCE=ITEM.PROCE AND ITEM_OFE.ITEM=ITEM.ID AND ITEM.ART IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PROCE ON ITEM.ANYO=PROCE.ANYO AND ITEM.GMN1=PROCE.GMN1 AND ITEM.PROCE=PROCE.COD" & vbCrLf
sConsulta = sConsulta & "      WHERE ITEM_ADJ.PORCEN <> 0 AND ITEM_ADJ.ANYO=@ANYO AND ITEM_ADJ.GMN1=@GMN1 AND ITEM_ADJ.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "    " & vbCrLf
sConsulta = sConsulta & "  OPEN Cur_PROCE" & vbCrLf
sConsulta = sConsulta & "  FETCH NEXT FROM Cur_PROCE INTO @PROVE,@DENPROVE,@GMN2,@GMN3,@GMN4,@ART,@DEST,@UNI,@PRECIO,@PORCEN,@FECINI,@FECFIN,@CANTIDAD" & vbCrLf
sConsulta = sConsulta & "  WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    --Primero recupero el identificador correspondiente a la adjudicaci�n" & vbCrLf
sConsulta = sConsulta & "    SET @MAX=NULL" & vbCrLf
sConsulta = sConsulta & "    SELECT @MAX= MAX(ID) FROM ART4_ADJ WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@GMN4 AND ART=@ART" & vbCrLf
sConsulta = sConsulta & "    IF @MAX IS NULL" & vbCrLf
sConsulta = sConsulta & "        SET @MAX=1" & vbCrLf
sConsulta = sConsulta & "    Else" & vbCrLf
sConsulta = sConsulta & "        SET @MAX= @MAX + 1" & vbCrLf
sConsulta = sConsulta & "        " & vbCrLf
sConsulta = sConsulta & "    --Ahora almaceno estas adjudicaciones en la tabla de ART4_ADJ" & vbCrLf
sConsulta = sConsulta & "    INSERT INTO ART4_ADJ (ANYO,GMN1,PROCE,CODPROVE,DENPROVE,GMN2,GMN3,GMN4,ART,ID,DEST,UNI,PREC,PORCEN,FECINI,FECFIN,CANT)" & vbCrLf
sConsulta = sConsulta & "    VALUES (@ANYO,@GMN1,@PROCE,@PROVE,@DENPROVE,@GMN2,@GMN3,@GMN4,@ART,@MAX,@DEST,@UNI,@PRECIO,@PORCEN,@FECINI,@FECFIN,@CANTIDAD)" & vbCrLf
sConsulta = sConsulta & "   FETCH NEXT FROM Cur_PROCE INTO @PROVE,@DENPROVE,@GMN2,@GMN3,@GMN4,@ART,@DEST,@UNI,@PRECIO,@PORCEN,@FECINI,@FECFIN,@CANTIDAD" & vbCrLf
sConsulta = sConsulta & "   End" & vbCrLf
sConsulta = sConsulta & "  Close Cur_PROCE" & vbCrLf
sConsulta = sConsulta & "   DEALLOCATE Cur_PROCE" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "  DECLARE Cur_ITEM CURSOR LOCAL FOR" & vbCrLf
sConsulta = sConsulta & "   SELECT ITEM_ADJ.PROVE AS PROVE,PROVE.DEN AS DENPROVE ,ITEM.GMN2,ITEM.GMN3,ITEM.GMN4,ITEM.ART, ITEM.DEST,ITEM.UNI, " & vbCrLf
sConsulta = sConsulta & "         ITEM_OFE.PREC_VALIDO/(PROCE_OFE.CAMBIO*PROCE.CAMBIO) AS PRECIO, ITEM_ADJ.PORCEN,ITEM.FECINI,ITEM.FECFIN,((PORCEN/100)* ITEM.CANT) AS CANTIDAD" & vbCrLf
sConsulta = sConsulta & "     FROM ITEM_ADJ INNER JOIN ITEM_OFE ON ITEM_ADJ.ANYO=ITEM_OFE.ANYO AND ITEM_ADJ.GMN1=ITEM_OFE.GMN1 AND ITEM_ADJ.PROCE=ITEM_OFE.PROCE AND ITEM_ADJ.OFE=ITEM_OFE.NUM AND ITEM_ADJ.PROVE=ITEM_OFE.PROVE AND ITEM_ADJ.ITEM=ITEM_OFE.ITEM" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PROVE ON ITEM_OFE.PROVE=PROVE.COD" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PROCE_OFE ON ITEM_OFE.ANYO=PROCE_OFE.ANYO AND ITEM_OFE.GMN1=PROCE_OFE.GMN1 AND ITEM_OFE.PROCE=PROCE_OFE.PROCE AND ITEM_OFE.NUM=PROCE_OFE.OFE AND ITEM_OFE.PROVE=PROCE_OFE.PROVE" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN ITEM ON ITEM_OFE.ANYO=ITEM.ANYO AND ITEM_OFE.GMN1=ITEM.GMN1 AND ITEM_OFE.PROCE=ITEM.PROCE AND ITEM_OFE.ITEM=ITEM.ID AND ITEM.ART IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PROCE ON ITEM.ANYO=PROCE.ANYO AND ITEM.GMN1=PROCE.GMN1 AND ITEM.PROCE=PROCE.COD" & vbCrLf
sConsulta = sConsulta & "      WHERE ITEM_ADJ.PORCEN <> 0 AND ITEM_ADJ.ANYO=@ANYO AND ITEM_ADJ.GMN1=@GMN1 AND ITEM_ADJ.PROCE=@PROCE AND ITEM_ADJ.ITEM=@ITEM" & vbCrLf
sConsulta = sConsulta & "    " & vbCrLf
sConsulta = sConsulta & "  OPEN Cur_ITEM" & vbCrLf
sConsulta = sConsulta & "  FETCH NEXT FROM Cur_ITEM INTO @PROVE,@DENPROVE,@GMN2,@GMN3,@GMN4,@ART,@DEST,@UNI,@PRECIO,@PORCEN,@FECINI,@FECFIN,@CANTIDAD" & vbCrLf
sConsulta = sConsulta & "  WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    --Primero recupero el identificador correspondiente a la adjudicaci�n" & vbCrLf
sConsulta = sConsulta & "    SET @MAX=NULL" & vbCrLf
sConsulta = sConsulta & "    SELECT @MAX= MAX(ID) FROM ART4_ADJ WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@GMN4 AND ART=@ART" & vbCrLf
sConsulta = sConsulta & "    IF @MAX IS NULL" & vbCrLf
sConsulta = sConsulta & "        SET @MAX=1" & vbCrLf
sConsulta = sConsulta & "    Else" & vbCrLf
sConsulta = sConsulta & "        SET @MAX= @MAX + 1" & vbCrLf
sConsulta = sConsulta & "        " & vbCrLf
sConsulta = sConsulta & "    --Ahora almaceno estas adjudicaciones en la tabla de ART4_ADJ" & vbCrLf
sConsulta = sConsulta & "    INSERT INTO ART4_ADJ (ANYO,GMN1,PROCE,CODPROVE,DENPROVE,GMN2,GMN3,GMN4,ART,ID,DEST,UNI,PREC,PORCEN,FECINI,FECFIN,CANT)" & vbCrLf
sConsulta = sConsulta & "    VALUES (@ANYO,@GMN1,@PROCE,@PROVE,@DENPROVE,@GMN2,@GMN3,@GMN4,@ART,@MAX,@DEST,@UNI,@PRECIO,@PORCEN,@FECINI,@FECFIN,@CANTIDAD)" & vbCrLf
sConsulta = sConsulta & "   FETCH NEXT FROM Cur_ITEM INTO @PROVE,@DENPROVE,@GMN2,@GMN3,@GMN4,@ART,@DEST,@UNI,@PRECIO,@PORCEN,@FECINI,@FECFIN,@CANTIDAD" & vbCrLf
sConsulta = sConsulta & "   End" & vbCrLf
sConsulta = sConsulta & "   Close Cur_ITEM" & vbCrLf
sConsulta = sConsulta & "   DEALLOCATE Cur_ITEM" & vbCrLf
sConsulta = sConsulta & "  END " & vbCrLf
sConsulta = sConsulta & "SELECT @RES=@@ERROR" & vbCrLf
sConsulta = sConsulta & "Return" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE SP_AR_CALCULAR AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCE AS INT,@ANYO AS INT,@GMN1 AS VARCHAR(3),@PROVE AS VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "declare @RES AS INT    " & vbCrLf
sConsulta = sConsulta & "declare @NumCal as int" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "set @NumCal=0" & vbCrLf
sConsulta = sConsulta & "DECLARE C_Cal CURSOR LOCAL FOR  SELECT PROCE.ANYO,PROCE.GMN1,PROCE.COD   FROM PROCE" & vbCrLf
sConsulta = sConsulta & "  WHERE PROCE.CALCPEND = 1  AND EST >=11 AND EST <20" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "OPEN C_Cal" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C_Cal INTO @ANYO,@GMN1,@PROCE" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "     begin transaction" & vbCrLf
sConsulta = sConsulta & "     EXEC SP_AR_AHORROS_CALCULAR @ANYO,@GMN1,@PROCE,@RES OUTPUT" & vbCrLf
sConsulta = sConsulta & "     if @res =0 " & vbCrLf
sConsulta = sConsulta & "              begin" & vbCrLf
sConsulta = sConsulta & "   commit  transaction" & vbCrLf
sConsulta = sConsulta & "             set @NumCal= @NumCal + 1" & vbCrLf
sConsulta = sConsulta & "             end" & vbCrLf
sConsulta = sConsulta & "     else" & vbCrLf
sConsulta = sConsulta & "             if @@trancount >0" & vbCrLf
sConsulta = sConsulta & "                 rollback  transaction" & vbCrLf
sConsulta = sConsulta & "      FETCH NEXT FROM C_Cal INTO @ANYO,@GMN1,@PROCE" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "CLOSE C_Cal " & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C_Cal" & vbCrLf
sConsulta = sConsulta & "IF @NumCal >0 " & vbCrLf
sConsulta = sConsulta & "    UPDATE PARGEN_DEF SET FEC_ACT_AHORROS=GETDATE()" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE dbo.SP_AR_JOB_LEER_DATOS (@BD VARCHAR(50), @ENABLE AS SMALLINT OUTPUT )AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @BD='ACT_AHORROS_' + @BD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SELECT @ENABLE=enabled FROM msdb.dbo.sysjobs where name=@BD" & vbCrLf
sConsulta = sConsulta & "   IF @ENABLE IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "        EXEC msdb.dbo.sp_help_job NULL, @BD,'SCHEDULES'" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE dbo.SP_AR_JOB_MODIF_DATOS (@MODIF TINYINT, @ENAB AS TINYINT ,@NOMBRE VARCHAR(50),@TIPO INT,@INTERVALO INT ,@RECFACTOR INT,@SUBDIA INT, @FRECSUBDIA INT,@RELATIVO INT, @FECINI INT,@TIMEINI INT, @RET_STAT INT OUTPUT)AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @BD AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "IF @MODIF=1 " & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "     IF @ENAB != 2" & vbCrLf
sConsulta = sConsulta & "             EXEC @RET_STAT= msdb.dbo.sp_update_job @job_name=@NOMBRE,@enabled=@ENAB" & vbCrLf
sConsulta = sConsulta & "             IF @RET_STAT = 1" & vbCrLf
sConsulta = sConsulta & "                   RETURN" & vbCrLf
sConsulta = sConsulta & "     EXEC @RET_STAT= msdb.dbo.sp_update_jobschedule @job_name=@NOMBRE,@name='ACT_AHORROS_SCH1',@freq_type =@TIPO,@freq_interval =@INTERVALO,@freq_subday_type=@SUBDIA,@freq_subday_interval =@FRECSUBDIA,@freq_relative_interval=@RELATIVO,@freq_recurrence_factor =@RECFACTOR, @active_start_date=@FECINI,@active_start_time =@TIMEINI" & vbCrLf
sConsulta = sConsulta & "END " & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @BD=SUBSTRING(@NOMBRE,13,LEN(@NOMBRE)-12)" & vbCrLf
sConsulta = sConsulta & "   EXEC @RET_STAT= msdb.dbo.sp_add_job @job_name=@NOMBRE,@enabled=@ENAB" & vbCrLf
sConsulta = sConsulta & "   IF @RET_STAT= 0" & vbCrLf
sConsulta = sConsulta & "   BEGIN       " & vbCrLf
sConsulta = sConsulta & "         EXEC @RET_STAT= msdb.dbo.sp_add_jobstep @job_name=@NOMBRE,@step_id =1,@step_name='ACT_AHORROS_STEP1',@command ='SP_AR_CALCULAR',@database_name =@BD" & vbCrLf
sConsulta = sConsulta & "         IF @RET_STAT=0" & vbCrLf
sConsulta = sConsulta & "         BEGIN" & vbCrLf
sConsulta = sConsulta & "             EXEC @RET_STAT = msdb.dbo.sp_add_jobserver  @job_name=@NOMBRE,  @server_name ='(LOCAL)'" & vbCrLf
sConsulta = sConsulta & "             IF @RET_STAT=0" & vbCrLf
sConsulta = sConsulta & "                   EXEC @RET_STAT= msdb.dbo.sp_add_jobschedule @job_name=@NOMBRE,@name='ACT_AHORROS_SCH1',@freq_type =@TIPO,@freq_interval =@INTERVALO,@freq_subday_type=@SUBDIA,@freq_subday_interval =@FRECSUBDIA,@freq_relative_interval=@RELATIVO,@freq_recurrence_factor =@RECFACTOR, @active_start_date=@FECINI,@active_start_time =@TIMEINI" & vbCrLf
sConsulta = sConsulta & "         END" & vbCrLf
sConsulta = sConsulta & "    END " & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE  SP_AR_PROCE (@ANYO SMALLINT,@GMN1 VARCHAR(50), @PROCE INT, @RES INT OUTPUT)  AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRESCONSUMIDO  AS FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRESABIERTO AS FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRESGLOBAL AS FLOAT" & vbCrLf
sConsulta = sConsulta & "/* Recorremos la tabla de ITEM_ADJ calculando la suma de presupuesto consumido*/" & vbCrLf
sConsulta = sConsulta & "SET @PRESCONSUMIDO =  (SELECT  SUM (((ITEM_ADJ.PORCEN/100)*ITEM.CANT*ITEM.PREC)/(PROCE.CAMBIO) )" & vbCrLf
sConsulta = sConsulta & "                                                  FROM ITEM_ADJ INNER JOIN ITEM ON ITEM_ADJ.ANYO=ITEM.ANYO AND ITEM_ADJ.GMN1=ITEM.GMN1 AND " & vbCrLf
sConsulta = sConsulta & "                                                             ITEM_ADJ.PROCE=ITEM.PROCE AND ITEM_ADJ.ITEM=ITEM.ID " & vbCrLf
sConsulta = sConsulta & "                                                             INNER JOIN PROCE ON ITEM.ANYO=PROCE.ANYO AND ITEM.GMN1=PROCE.GMN1 AND ITEM.PROCE=PROCE.COD" & vbCrLf
sConsulta = sConsulta & "                                                             WHERE  ITEM_ADJ.ANYO=@ANYO AND ITEM_ADJ.GMN1=@GMN1 AND ITEM_ADJ.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "                                                             AND ITEM_ADJ.PORCEN <> 0  AND ITEM.CONF=1)" & vbCrLf
sConsulta = sConsulta & "/* Recorremos la tabla de ITEM calculando la suma de presupuesto abierto*/" & vbCrLf
sConsulta = sConsulta & "SET @PRESABIERTO =  (SELECT  SUM ((ITEM.CANT*ITEM.PREC)/(PROCE.CAMBIO))" & vbCrLf
sConsulta = sConsulta & "                                           FROM ITEM INNER JOIN PROCE ON ITEM.ANYO=PROCE.ANYO AND ITEM.GMN1=PROCE.GMN1 AND ITEM.PROCE=PROCE.COD" & vbCrLf
sConsulta = sConsulta & "                                              AND ITEM.ANYO=@ANYO AND ITEM.GMN1=@GMN1 AND ITEM.PROCE=@PROCE  AND ITEM.CONF = 1)" & vbCrLf
sConsulta = sConsulta & "/*antes de almacenar borramos el registro de AR_PROCE si ya exist�a*/" & vbCrLf
sConsulta = sConsulta & "DELETE FROM AR_PROCE WHERE ANYO = @ANYO AND GMN1=@GMN1 AND COD = @PROCE" & vbCrLf
sConsulta = sConsulta & "/* Almacenamos los valores en la tabla AR_PROCE */" & vbCrLf
sConsulta = sConsulta & "INSERT INTO AR_PROCE" & vbCrLf
sConsulta = sConsulta & "(ANYO,GMN1,COD, ADJDIR, GMN2, GMN3, GMN4, EQP,COM,FECULTREU,PRESGLOBAL,PRESABIERTO,PRESCONSUMIDO,IMPORTE)" & vbCrLf
sConsulta = sConsulta & "SELECT  @ANYO,@GMN1,@PROCE,ADJDIR,GMN2,GMN3,GMN4,PROCE.EQP,PROCE.COM,FECULTREU,PRES,@PRESABIERTO,@PRESCONSUMIDO,ADJUDICADO/CAMBIO" & vbCrLf
sConsulta = sConsulta & "FROM PROCE" & vbCrLf
sConsulta = sConsulta & "WHERE PROCE.ANYO = @ANYO AND PROCE.GMN1=@GMN1 AND PROCE.COD = @PROCE" & vbCrLf
sConsulta = sConsulta & "SET @RES=@@ERROR" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "Return" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE SP_CIERRE(@ANYO SMALLINT,@GMN1 VARCHAR(20),@PROCE INTEGER, @RES INTEGER OUTPUT) AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ART4_ADJ @ANYO,@GMN1,@PROCE,NULL,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_PROCE @ANYO,@GMN1,@PROCE,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_PROVE @ANYO,@GMN1,@PROCE,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_PROVE @ANYO,@GMN1,@PROCE,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_PRESCON3 @ANYO,@GMN1,@PROCE,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_PRESCON4 @ANYO,@GMN1,@PROCE,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_MES @ANYO,@GMN1,@PROCE,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_MES_PARCON1 @ANYO,@GMN1,@PROCE,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_MES_PARCON2 @ANYO,@GMN1,@PROCE,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_MES_PARCON3 @ANYO,@GMN1,@PROCE,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_MES_PARCON4 @ANYO,@GMN1,@PROCE,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_MES_PRESCON3_1 @ANYO,@GMN1,@PROCE,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_MES_PRESCON3_2 @ANYO,@GMN1,@PROCE,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_MES_PRESCON3_3 @ANYO,@GMN1,@PROCE,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_MES_PRESCON3_4 @ANYO,@GMN1,@PROCE,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_MES_PRESCON4_1 @ANYO,@GMN1,@PROCE,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_MES_PRESCON4_2 @ANYO,@GMN1,@PROCE,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_MES_PRESCON4_3 @ANYO,@GMN1,@PROCE,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_MES_PRESCON4_4 @ANYO,@GMN1,@PROCE,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_MES_PROY1 @ANYO,@GMN1,@PROCE,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_MES_PROY2 @ANYO,@GMN1,@PROCE,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_MES_PROY3 @ANYO,@GMN1,@PROCE,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_MES_PROY4 @ANYO,@GMN1,@PROCE,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_MES_UON1 @ANYO,@GMN1,@PROCE,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_MES_UON2 @ANYO,@GMN1,@PROCE,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_MES_UON3 @ANYO,@GMN1,@PROCE,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES IS  NULL SET @RES=0" & vbCrLf
sConsulta = sConsulta & "Return @RES" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE SP_CIERRE_PARC(@ANYO SMALLINT,@GMN1 VARCHAR(20),@PROCE INTEGER, @ITEM INTEGER,@RES INTEGER OUTPUT) AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ART4_ADJ @ANYO,@GMN1,@PROCE,@ITEM ,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_PROVE_PARC @ANYO,@GMN1,@PROCE,@ITEM,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_PRESCON3_PARC @ANYO,@GMN1,@PROCE,@ITEM,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_PRESCON4_PARC @ANYO,@GMN1,@PROCE,@ITEM,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_MES_PARC @ANYO,@GMN1,@PROCE,@ITEM,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_MES_UON1_PARC @ANYO,@GMN1,@PROCE,@ITEM,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_MES_UON2_PARC @ANYO,@GMN1,@PROCE,@ITEM,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_MES_UON3_PARC @ANYO,@GMN1,@PROCE,@ITEM,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_MES_PARCON1_PARC @ANYO,@GMN1,@PROCE,@ITEM,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_MES_PARCON2_PARC @ANYO,@GMN1,@PROCE,@ITEM,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_MES_PARCON3_PARC @ANYO,@GMN1,@PROCE,@ITEM,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_MES_PARCON4_PARC @ANYO,@GMN1,@PROCE,@ITEM,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_MES_PROY1_PARC @ANYO,@GMN1,@PROCE,@ITEM,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_MES_PROY2_PARC @ANYO,@GMN1,@PROCE,@ITEM,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_MES_PROY3_PARC @ANYO,@GMN1,@PROCE,@ITEM,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_MES_PROY4_PARC @ANYO,@GMN1,@PROCE,@ITEM,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_MES_PRESCON3_1_PARC @ANYO,@GMN1,@PROCE,@ITEM,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_MES_PRESCON3_2_PARC @ANYO,@GMN1,@PROCE,@ITEM,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_MES_PRESCON3_3_PARC @ANYO,@GMN1,@PROCE,@ITEM,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_MES_PRESCON3_4_PARC @ANYO,@GMN1,@PROCE,@ITEM,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_MES_PRESCON4_1_PARC @ANYO,@GMN1,@PROCE,@ITEM,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_MES_PRESCON4_2_PARC @ANYO,@GMN1,@PROCE,@ITEM,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_MES_PRESCON4_3_PARC @ANYO,@GMN1,@PROCE,@ITEM,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_MES_PRESCON4_4_PARC @ANYO,@GMN1,@PROCE,@ITEM,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES IS NULL SET @RES=0" & vbCrLf
sConsulta = sConsulta & "Return @RES" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE SP_DEVOLVER_PROCE_ATRIB @ANYO smallint, @GMN1 varchar(50), @PROCE smallint, @AMBITO tinyint, @GRUPO varchar(50)=null, @CONLISTA tinyint = 1, @INTERNO tinyint= null, @ATRIB int = null AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CONLISTA = 1" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "if @AMBITO = 1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT PA.ATRIB, PA.ID, DA.COD, DA.DEN, PA.INTRO, DA.TIPO, PA.AMBITO," & vbCrLf
sConsulta = sConsulta & "       PA.MINNUM, PA.MINFEC," & vbCrLf
sConsulta = sConsulta & "       PA.MAXNUM, PA.MAXFEC," & vbCrLf
sConsulta = sConsulta & "       DAL.VALOR_TEXT DALVALOR_TEXT," & vbCrLf
sConsulta = sConsulta & "       DAL.VALOR_NUM DALVALOR_NUM," & vbCrLf
sConsulta = sConsulta & "       DAL.VALOR_FEC DALVALOR_FEC," & vbCrLf
sConsulta = sConsulta & "       PA.INTERNO,PA.POND,PA.DEF_PREC,PA.PREFBAJO," & vbCrLf
sConsulta = sConsulta & "       PA.OBLIG,PA.APLIC_PREC,PA.OP_PREC,PA.GRUPO,PA.USAR_PREC,PA.ORDEN," & vbCrLf
sConsulta = sConsulta & "       PA.OP_POND,PA.NUM_POND,PA.POND_SI,PA.POND_NO" & vbCrLf
sConsulta = sConsulta & " FROM PROCE_ATRIB PA" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN DEF_ATRIB DA" & vbCrLf
sConsulta = sConsulta & "               ON PA.ATRIB = DA.ID" & vbCrLf
sConsulta = sConsulta & "        LEFT JOIN PROCE_ATRIB_LISTA DAL" & vbCrLf
sConsulta = sConsulta & "               ON PA.ID = DAL.ATRIB_ID" & vbCrLf
sConsulta = sConsulta & "WHERE PA.ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "  AND PA.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "  AND PA.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "  AND PA.AMBITO = 1" & vbCrLf
sConsulta = sConsulta & "  AND PA.INTERNO = CASE WHEN @INTERNO IS NULL THEN PA.INTERNO ELSE @INTERNO END" & vbCrLf
sConsulta = sConsulta & "  AND DA.ID = CASE WHEN @ATRIB IS NULL THEN DA.ID ELSE @ATRIB END" & vbCrLf
sConsulta = sConsulta & "ORDER BY PA.GRUPO,PA.ORDEN_ATRIB" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ELSE IF @AMBITO = 2" & vbCrLf
sConsulta = sConsulta & "  IF @GRUPO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "    SELECT PA.ATRIB, PA.ID, DA.COD, DA.DEN, PA.INTRO, DA.TIPO, PA.AMBITO," & vbCrLf
sConsulta = sConsulta & "           PA.MINNUM, PA.MINFEC," & vbCrLf
sConsulta = sConsulta & "           PA.MAXNUM, PA.MAXFEC," & vbCrLf
sConsulta = sConsulta & "           DAL.VALOR_TEXT DALVALOR_TEXT," & vbCrLf
sConsulta = sConsulta & "           DAL.VALOR_NUM DALVALOR_NUM," & vbCrLf
sConsulta = sConsulta & "           DAL.VALOR_FEC DALVALOR_FEC,   " & vbCrLf
sConsulta = sConsulta & "           PA.INTERNO,PA.POND,PA.DEF_PREC,PA.PREFBAJO," & vbCrLf
sConsulta = sConsulta & "           PA.OBLIG,PA.APLIC_PREC,PA.OP_PREC,PA.GRUPO,PA.USAR_PREC,PA.ORDEN," & vbCrLf
sConsulta = sConsulta & "           PA.OP_POND,PA.NUM_POND,PA.POND_SI,PA.POND_NO" & vbCrLf
sConsulta = sConsulta & "     FROM PROCE_ATRIB PA" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN DEF_ATRIB DA" & vbCrLf
sConsulta = sConsulta & "                   ON PA.ATRIB = DA.ID" & vbCrLf
sConsulta = sConsulta & "            LEFT JOIN PROCE_ATRIB_LISTA DAL" & vbCrLf
sConsulta = sConsulta & "                   ON PA.ID = DAL.ATRIB_ID" & vbCrLf
sConsulta = sConsulta & "    WHERE PA.ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "      AND PA.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "      AND PA.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "      AND ISNULL(PA.GRUPO,'#') = CASE WHEN PA.GRUPO IS NULL THEN ISNULL(PA.GRUPO,'#') ELSE @GRUPO END" & vbCrLf
sConsulta = sConsulta & "     AND PA.AMBITO = 2" & vbCrLf
sConsulta = sConsulta & "     AND PA.INTERNO = CASE WHEN @INTERNO IS NULL THEN PA.INTERNO ELSE @INTERNO END" & vbCrLf
sConsulta = sConsulta & "     AND DA.ID = CASE WHEN @ATRIB IS NULL THEN DA.ID ELSE @ATRIB END" & vbCrLf
sConsulta = sConsulta & "    ORDER BY PA.GRUPO,PA.ORDEN_ATRIB" & vbCrLf
sConsulta = sConsulta & "  ELSE" & vbCrLf
sConsulta = sConsulta & "    SELECT PA.ATRIB, PA.ID, DA.COD, DA.DEN, PA.INTRO, DA.TIPO, PA.AMBITO," & vbCrLf
sConsulta = sConsulta & "           PA.MINNUM, PA.MINFEC," & vbCrLf
sConsulta = sConsulta & "           PA.MAXNUM, PA.MAXFEC," & vbCrLf
sConsulta = sConsulta & "           DAL.VALOR_TEXT DALVALOR_TEXT," & vbCrLf
sConsulta = sConsulta & "           DAL.VALOR_NUM DALVALOR_NUM," & vbCrLf
sConsulta = sConsulta & "           DAL.VALOR_FEC DALVALOR_FEC,   " & vbCrLf
sConsulta = sConsulta & "           PA.INTERNO,PA.POND,PA.DEF_PREC,PA.PREFBAJO," & vbCrLf
sConsulta = sConsulta & "           PA.OBLIG,PA.APLIC_PREC,PA.OP_PREC,PA.GRUPO,PA.USAR_PREC,PA.ORDEN," & vbCrLf
sConsulta = sConsulta & "           PA.OP_POND,PA.NUM_POND,PA.POND_SI,PA.POND_NO" & vbCrLf
sConsulta = sConsulta & "     FROM PROCE_ATRIB PA" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN DEF_ATRIB DA" & vbCrLf
sConsulta = sConsulta & "                   ON PA.ATRIB = DA.ID" & vbCrLf
sConsulta = sConsulta & "            LEFT JOIN PROCE_ATRIB_LISTA DAL" & vbCrLf
sConsulta = sConsulta & "                   ON PA.ID = DAL.ATRIB_ID" & vbCrLf
sConsulta = sConsulta & "    WHERE PA.ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "      AND PA.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "      AND PA.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "      AND PA.AMBITO = 2" & vbCrLf
sConsulta = sConsulta & "      AND PA.INTERNO = CASE WHEN @INTERNO IS NULL THEN PA.INTERNO ELSE @INTERNO END" & vbCrLf
sConsulta = sConsulta & "      AND DA.ID = CASE WHEN @ATRIB IS NULL THEN DA.ID ELSE @ATRIB END" & vbCrLf
sConsulta = sConsulta & "    ORDER BY PA.GRUPO,PA.ORDEN_ATRIB" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ELSE IF @AMBITO = 3" & vbCrLf
sConsulta = sConsulta & "  IF @GRUPO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "    SELECT PA.ATRIB, PA.ID, DA.COD, DA.DEN, PA.INTRO, DA.TIPO,  PA.AMBITO," & vbCrLf
sConsulta = sConsulta & "           PA.MINNUM, PA.MINFEC," & vbCrLf
sConsulta = sConsulta & "           PA.MAXNUM, PA.MAXFEC," & vbCrLf
sConsulta = sConsulta & "           DAL.VALOR_TEXT DALVALOR_TEXT," & vbCrLf
sConsulta = sConsulta & "           DAL.VALOR_NUM DALVALOR_NUM," & vbCrLf
sConsulta = sConsulta & "           DAL.VALOR_FEC DALVALOR_FEC," & vbCrLf
sConsulta = sConsulta & "           PA.INTERNO,PA.POND,PA.DEF_PREC,PA.PREFBAJO," & vbCrLf
sConsulta = sConsulta & "           PA.OBLIG,PA.APLIC_PREC,PA.OP_PREC,PA.GRUPO,PA.USAR_PREC,PA.ORDEN," & vbCrLf
sConsulta = sConsulta & "           PA.OP_POND,PA.NUM_POND,PA.POND_SI,PA.POND_NO     " & vbCrLf
sConsulta = sConsulta & "     FROM PROCE_ATRIB PA" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN DEF_ATRIB DA" & vbCrLf
sConsulta = sConsulta & "                   ON PA.ATRIB = DA.ID" & vbCrLf
sConsulta = sConsulta & "            LEFT JOIN PROCE_ATRIB_LISTA DAL" & vbCrLf
sConsulta = sConsulta & "                   ON PA.ID = DAL.ATRIB_ID" & vbCrLf
sConsulta = sConsulta & "    WHERE PA.ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "      AND PA.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "      AND PA.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "      AND ISNULL(PA.GRUPO,'#') = CASE WHEN PA.GRUPO IS NULL THEN ISNULL(PA.GRUPO,'#') ELSE @GRUPO END" & vbCrLf
sConsulta = sConsulta & "      AND PA.AMBITO = 3" & vbCrLf
sConsulta = sConsulta & "      AND PA.INTERNO = CASE WHEN @INTERNO IS NULL THEN PA.INTERNO ELSE @INTERNO END" & vbCrLf
sConsulta = sConsulta & "      AND DA.ID = CASE WHEN @ATRIB IS NULL THEN DA.ID ELSE @ATRIB END" & vbCrLf
sConsulta = sConsulta & "     ORDER BY PA.GRUPO,PA.ORDEN_ATRIB" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  ELSE" & vbCrLf
sConsulta = sConsulta & "   SELECT PA.ATRIB, PA.ID, DA.COD, DA.DEN, PA.INTRO, DA.TIPO,  PA.AMBITO," & vbCrLf
sConsulta = sConsulta & "                PA.MINNUM, PA.MINFEC," & vbCrLf
sConsulta = sConsulta & "                PA.MAXNUM, PA.MAXFEC," & vbCrLf
sConsulta = sConsulta & "                NULL DALVALOR_TEXT," & vbCrLf
sConsulta = sConsulta & "                NULL DALVALOR_NUM," & vbCrLf
sConsulta = sConsulta & "                NULL DALVALOR_FEC," & vbCrLf
sConsulta = sConsulta & "                PA.INTERNO,PA.POND,PA.DEF_PREC,PA.PREFBAJO," & vbCrLf
sConsulta = sConsulta & "                PA.OBLIG,PA.APLIC_PREC,PA.OP_PREC,PA.GRUPO,PA.USAR_PREC,PA.ORDEN," & vbCrLf
sConsulta = sConsulta & "                PA.OP_POND,PA.NUM_POND,PA.POND_SI,PA.POND_NO" & vbCrLf
sConsulta = sConsulta & "       FROM PROCE_ATRIB PA" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN DEF_ATRIB DA" & vbCrLf
sConsulta = sConsulta & "                   ON PA.ATRIB = DA.ID" & vbCrLf
sConsulta = sConsulta & "       WHERE PA.ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "         AND PA.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "         AND PA.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "         AND PA.AMBITO = 3" & vbCrLf
sConsulta = sConsulta & "               AND PA.INTERNO = CASE WHEN @INTERNO IS NULL THEN PA.INTERNO ELSE @INTERNO END" & vbCrLf
sConsulta = sConsulta & "               AND DA.ID = CASE WHEN @ATRIB IS NULL THEN DA.ID ELSE @ATRIB END" & vbCrLf
sConsulta = sConsulta & "          ORDER BY PA.GRUPO,PA.ORDEN_ATRIB" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "if @AMBITO = 1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT PA.ATRIB, PA.ID, DA.COD, DA.DEN, PA.INTRO, DA.TIPO,  PA.AMBITO," & vbCrLf
sConsulta = sConsulta & "       PA.MINNUM, PA.MINFEC," & vbCrLf
sConsulta = sConsulta & "       PA.MAXNUM, PA.MAXFEC," & vbCrLf
sConsulta = sConsulta & "       NULL DALVALOR_TEXT," & vbCrLf
sConsulta = sConsulta & "       NULL DALVALOR_NUM," & vbCrLf
sConsulta = sConsulta & "       NULL DALVALOR_FEC," & vbCrLf
sConsulta = sConsulta & "       PA.INTERNO,PA.POND,PA.DEF_PREC,PA.PREFBAJO," & vbCrLf
sConsulta = sConsulta & "       PA.OBLIG,PA.APLIC_PREC,PA.OP_PREC,PA.GRUPO,PA.USAR_PREC,PA.ORDEN," & vbCrLf
sConsulta = sConsulta & "       PA.OP_POND,PA.NUM_POND,PA.POND_SI,PA.POND_NO" & vbCrLf
sConsulta = sConsulta & " FROM PROCE_ATRIB PA" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN DEF_ATRIB DA" & vbCrLf
sConsulta = sConsulta & "               ON PA.ATRIB = DA.ID" & vbCrLf
sConsulta = sConsulta & "WHERE PA.ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "  AND PA.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "  AND PA.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "  AND PA.AMBITO = 1" & vbCrLf
sConsulta = sConsulta & "  AND PA.INTERNO = CASE WHEN @INTERNO IS NULL THEN PA.INTERNO ELSE @INTERNO END" & vbCrLf
sConsulta = sConsulta & "  AND DA.ID = CASE WHEN @ATRIB IS NULL THEN DA.ID ELSE @ATRIB END" & vbCrLf
sConsulta = sConsulta & "ORDER BY PA.GRUPO,PA.ORDEN_ATRIB" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ELSE IF @AMBITO = 2" & vbCrLf
sConsulta = sConsulta & "  IF @GRUPO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "    SELECT PA.ATRIB, PA.ID, DA.COD, DA.DEN, PA.INTRO, DA.TIPO,  PA.AMBITO," & vbCrLf
sConsulta = sConsulta & "           PA.MINNUM, PA.MINFEC," & vbCrLf
sConsulta = sConsulta & "           PA.MAXNUM, PA.MAXFEC," & vbCrLf
sConsulta = sConsulta & "           NULL DALVALOR_TEXT," & vbCrLf
sConsulta = sConsulta & "           NULL DALVALOR_NUM," & vbCrLf
sConsulta = sConsulta & "           NULL DALVALOR_FEC,   " & vbCrLf
sConsulta = sConsulta & "           PA.INTERNO,PA.POND,PA.DEF_PREC,PA.PREFBAJO," & vbCrLf
sConsulta = sConsulta & "           PA.OBLIG,PA.APLIC_PREC,PA.OP_PREC,PA.GRUPO,PA.USAR_PREC,PA.ORDEN," & vbCrLf
sConsulta = sConsulta & "           PA.OP_POND,PA.NUM_POND,PA.POND_SI,PA.POND_NO" & vbCrLf
sConsulta = sConsulta & "     FROM PROCE_ATRIB PA" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN DEF_ATRIB DA" & vbCrLf
sConsulta = sConsulta & "                   ON PA.ATRIB = DA.ID" & vbCrLf
sConsulta = sConsulta & "    WHERE PA.ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "      AND PA.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "      AND PA.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "      AND ISNULL(PA.GRUPO,'#') = CASE WHEN PA.GRUPO IS NULL THEN ISNULL(PA.GRUPO,'#') ELSE @GRUPO END" & vbCrLf
sConsulta = sConsulta & "      AND PA.AMBITO = 2" & vbCrLf
sConsulta = sConsulta & "      AND PA.INTERNO = CASE WHEN @INTERNO IS NULL THEN PA.INTERNO ELSE @INTERNO END" & vbCrLf
sConsulta = sConsulta & "      AND DA.ID = CASE WHEN @ATRIB IS NULL THEN DA.ID ELSE @ATRIB END" & vbCrLf
sConsulta = sConsulta & "    ORDER BY PA.GRUPO,PA.ORDEN_ATRIB" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  ELSE" & vbCrLf
sConsulta = sConsulta & "    SELECT PA.ATRIB, PA.ID, DA.COD, DA.DEN, PA.INTRO, DA.TIPO,  PA.AMBITO," & vbCrLf
sConsulta = sConsulta & "           PA.MINNUM, PA.MINFEC," & vbCrLf
sConsulta = sConsulta & "           PA.MAXNUM, PA.MAXFEC," & vbCrLf
sConsulta = sConsulta & "           NULL DALVALOR_TEXT," & vbCrLf
sConsulta = sConsulta & "           NULL DALVALOR_NUM," & vbCrLf
sConsulta = sConsulta & "           NULL DALVALOR_FEC,   " & vbCrLf
sConsulta = sConsulta & "           PA.INTERNO,PA.POND,PA.DEF_PREC,PA.PREFBAJO," & vbCrLf
sConsulta = sConsulta & "           PA.OBLIG,PA.APLIC_PREC,PA.OP_PREC,PA.GRUPO,PA.USAR_PREC,PA.ORDEN," & vbCrLf
sConsulta = sConsulta & "           PA.OP_POND,PA.NUM_POND,PA.POND_SI,PA.POND_NO" & vbCrLf
sConsulta = sConsulta & "     FROM PROCE_ATRIB PA" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN DEF_ATRIB DA" & vbCrLf
sConsulta = sConsulta & "                   ON PA.ATRIB = DA.ID" & vbCrLf
sConsulta = sConsulta & "    WHERE PA.ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "      AND PA.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "      AND PA.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "      AND PA.AMBITO = 2" & vbCrLf
sConsulta = sConsulta & "      AND PA.INTERNO = CASE WHEN @INTERNO IS NULL THEN PA.INTERNO ELSE @INTERNO END" & vbCrLf
sConsulta = sConsulta & "      AND DA.ID = CASE WHEN @ATRIB IS NULL THEN DA.ID ELSE @ATRIB END" & vbCrLf
sConsulta = sConsulta & "    ORDER BY PA.GRUPO,PA.ORDEN_ATRIB" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "ELSE IF @AMBITO = 3" & vbCrLf
sConsulta = sConsulta & "  IF @GRUPO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "    SELECT PA.ATRIB, PA.ID, DA.COD, DA.DEN, PA.INTRO, DA.TIPO,  PA.AMBITO," & vbCrLf
sConsulta = sConsulta & "           PA.MINNUM, PA.MINFEC," & vbCrLf
sConsulta = sConsulta & "           PA.MAXNUM, PA.MAXFEC," & vbCrLf
sConsulta = sConsulta & "           NULL DALVALOR_TEXT," & vbCrLf
sConsulta = sConsulta & "           NULL DALVALOR_NUM," & vbCrLf
sConsulta = sConsulta & "           NULL DALVALOR_FEC," & vbCrLf
sConsulta = sConsulta & "           PA.INTERNO,PA.POND,PA.DEF_PREC,PA.PREFBAJO," & vbCrLf
sConsulta = sConsulta & "           PA.OBLIG,PA.APLIC_PREC,PA.OP_PREC,PA.GRUPO,PA.USAR_PREC,PA.ORDEN," & vbCrLf
sConsulta = sConsulta & "           PA.OP_POND,PA.NUM_POND,PA.POND_SI,PA.POND_NO" & vbCrLf
sConsulta = sConsulta & "     FROM PROCE_ATRIB PA" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN DEF_ATRIB DA" & vbCrLf
sConsulta = sConsulta & "                   ON PA.ATRIB = DA.ID" & vbCrLf
sConsulta = sConsulta & "    WHERE PA.ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "      AND PA.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "      AND PA.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "      AND ISNULL(PA.GRUPO,'#') = CASE WHEN PA.GRUPO IS NULL THEN ISNULL(PA.GRUPO,'#') ELSE @GRUPO END" & vbCrLf
sConsulta = sConsulta & "      AND PA.AMBITO = 3" & vbCrLf
sConsulta = sConsulta & "      AND PA.INTERNO = CASE WHEN @INTERNO IS NULL THEN PA.INTERNO ELSE @INTERNO END" & vbCrLf
sConsulta = sConsulta & "      AND DA.ID = CASE WHEN @ATRIB IS NULL THEN DA.ID ELSE @ATRIB END" & vbCrLf
sConsulta = sConsulta & "    ORDER BY PA.GRUPO,PA.ORDEN_ATRIB" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "   SELECT PA.ATRIB, PA.ID, DA.COD, DA.DEN, PA.INTRO, DA.TIPO,  PA.AMBITO," & vbCrLf
sConsulta = sConsulta & "                PA.MINNUM, PA.MINFEC," & vbCrLf
sConsulta = sConsulta & "                PA.MAXNUM, PA.MAXFEC," & vbCrLf
sConsulta = sConsulta & "                NULL DALVALOR_TEXT," & vbCrLf
sConsulta = sConsulta & "                NULL DALVALOR_NUM," & vbCrLf
sConsulta = sConsulta & "                NULL DALVALOR_FEC," & vbCrLf
sConsulta = sConsulta & "                PA.INTERNO,PA.POND,PA.DEF_PREC,PA.PREFBAJO," & vbCrLf
sConsulta = sConsulta & "                PA.OBLIG,PA.APLIC_PREC,PA.OP_PREC,PA.GRUPO,PA.USAR_PREC,PA.ORDEN," & vbCrLf
sConsulta = sConsulta & "                PA.OP_POND,PA.NUM_POND,PA.POND_SI,PA.POND_NO" & vbCrLf
sConsulta = sConsulta & "       FROM PROCE_ATRIB PA" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN DEF_ATRIB DA" & vbCrLf
sConsulta = sConsulta & "                   ON PA.ATRIB = DA.ID" & vbCrLf
sConsulta = sConsulta & "       WHERE PA.ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "         AND PA.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "         AND PA.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "         AND PA.AMBITO = 3" & vbCrLf
sConsulta = sConsulta & "               AND PA.INTERNO = CASE WHEN @INTERNO IS NULL THEN PA.INTERNO ELSE @INTERNO END" & vbCrLf
sConsulta = sConsulta & "               AND DA.ID = CASE WHEN @ATRIB IS NULL THEN DA.ID ELSE @ATRIB END" & vbCrLf
sConsulta = sConsulta & "           ORDER BY PA.GRUPO,PA.ORDEN_ATRIB" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE SP_DEVOLVER_PROCE_ATRIB_PORTAL @ANYO smallint, @GMN1 varchar(50), @PROCE smallint, @PROVE varchar(50), @AMBITO tinyint, @GRUPO varchar(50)=null, @OFE smallint = null AS" & vbCrLf
sConsulta = sConsulta & "if @AMBITO = 1" & vbCrLf
sConsulta = sConsulta & "SELECT PA.ATRIB, PA.ID, DA.COD, DA.DEN, PA.INTRO, DA.TIPO, " & vbCrLf
sConsulta = sConsulta & "       PA.MINNUM, PA.MINFEC," & vbCrLf
sConsulta = sConsulta & "       PA.MAXNUM, PA.MAXFEC," & vbCrLf
sConsulta = sConsulta & "       OA.VALOR_TEXT OAVALOR_TEXT," & vbCrLf
sConsulta = sConsulta & "       OA.VALOR_NUM OAVALOR_NUM," & vbCrLf
sConsulta = sConsulta & "       OA.VALOR_FEC OAVALOR_FEC," & vbCrLf
sConsulta = sConsulta & "       OA.VALOR_BOOL OAVALOR_BOOL," & vbCrLf
sConsulta = sConsulta & "       DAL.VALOR_TEXT DALVALOR_TEXT," & vbCrLf
sConsulta = sConsulta & "       DAL.VALOR_NUM DALVALOR_NUM," & vbCrLf
sConsulta = sConsulta & "       DAL.VALOR_FEC DALVALOR_FEC" & vbCrLf
sConsulta = sConsulta & " FROM PROCE_ATRIB PA" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN DEF_ATRIB DA" & vbCrLf
sConsulta = sConsulta & "               ON PA.ATRIB = DA.ID" & vbCrLf
sConsulta = sConsulta & "        LEFT JOIN PROCE_ATRIB_LISTA DAL" & vbCrLf
sConsulta = sConsulta & "               ON PA.ID = DAL.ATRIB_ID" & vbCrLf
sConsulta = sConsulta & "        LEFT JOIN OFE_ATRIB OA" & vbCrLf
sConsulta = sConsulta & "               ON PA.ANYO = OA.ANYO" & vbCrLf
sConsulta = sConsulta & "              AND PA.GMN1 = OA.GMN1" & vbCrLf
sConsulta = sConsulta & "              AND PA.PROCE = OA.PROCE" & vbCrLf
sConsulta = sConsulta & "              AND @OFE = OA.OFE" & vbCrLf
sConsulta = sConsulta & "              AND @PROVE = OA.PROVE" & vbCrLf
sConsulta = sConsulta & "              AND PA.ID = OA.ATRIB_ID" & vbCrLf
sConsulta = sConsulta & "WHERE PA.ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "  AND PA.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "  AND PA.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "  AND PA.AMBITO = 1" & vbCrLf
sConsulta = sConsulta & "ORDER BY PA.GRUPO,PA.ORDEN_ATRIB" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ELSE IF @AMBITO = 2" & vbCrLf
sConsulta = sConsulta & "  IF @OFE IS NULL" & vbCrLf
sConsulta = sConsulta & "    SELECT PA.ATRIB, PA.ID, DA.COD, DA.DEN, PA.INTRO, DA.TIPO, " & vbCrLf
sConsulta = sConsulta & "           PA.MINNUM, PA.MINFEC," & vbCrLf
sConsulta = sConsulta & "           PA.MAXNUM, PA.MAXFEC," & vbCrLf
sConsulta = sConsulta & "           NULL OAVALOR_TEXT," & vbCrLf
sConsulta = sConsulta & "           NULL OAVALOR_NUM," & vbCrLf
sConsulta = sConsulta & "           NULL OAVALOR_FEC," & vbCrLf
sConsulta = sConsulta & "           NULL OAVALOR_BOOL," & vbCrLf
sConsulta = sConsulta & "           DAL.VALOR_TEXT DALVALOR_TEXT," & vbCrLf
sConsulta = sConsulta & "           DAL.VALOR_NUM DALVALOR_NUM," & vbCrLf
sConsulta = sConsulta & "           DAL.VALOR_FEC DALVALOR_FEC" & vbCrLf
sConsulta = sConsulta & "     FROM PROCE_ATRIB PA" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN DEF_ATRIB DA" & vbCrLf
sConsulta = sConsulta & "                   ON PA.ATRIB = DA.ID" & vbCrLf
sConsulta = sConsulta & "            LEFT JOIN PROCE_ATRIB_LISTA DAL" & vbCrLf
sConsulta = sConsulta & "                   ON PA.ID = DAL.ATRIB_ID" & vbCrLf
sConsulta = sConsulta & "    WHERE PA.ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "      AND PA.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "      AND PA.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "      AND ISNULL(PA.GRUPO,'#') = CASE WHEN PA.GRUPO IS NULL THEN ISNULL(PA.GRUPO,'#') ELSE @GRUPO END" & vbCrLf
sConsulta = sConsulta & "      AND PA.AMBITO = 2" & vbCrLf
sConsulta = sConsulta & "    ORDER BY PA.GRUPO,PA.ORDEN_ATRIB" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  ELSE" & vbCrLf
sConsulta = sConsulta & "    SELECT PA.ATRIB, PA.ID, DA.COD, DA.DEN, PA.INTRO, DA.TIPO, " & vbCrLf
sConsulta = sConsulta & "           PA.MINNUM, PA.MINFEC," & vbCrLf
sConsulta = sConsulta & "           PA.MAXNUM, PA.MAXFEC," & vbCrLf
sConsulta = sConsulta & "           OA.VALOR_TEXT OAVALOR_TEXT," & vbCrLf
sConsulta = sConsulta & "           OA.VALOR_NUM OAVALOR_NUM," & vbCrLf
sConsulta = sConsulta & "           OA.VALOR_FEC OAVALOR_FEC," & vbCrLf
sConsulta = sConsulta & "           OA.VALOR_BOOL OAVALOR_BOOL," & vbCrLf
sConsulta = sConsulta & "           DAL.VALOR_TEXT DALVALOR_TEXT," & vbCrLf
sConsulta = sConsulta & "           DAL.VALOR_NUM DALVALOR_NUM," & vbCrLf
sConsulta = sConsulta & "           DAL.VALOR_FEC DALVALOR_FEC" & vbCrLf
sConsulta = sConsulta & "      FROM OFE_GR_ATRIB OA" & vbCrLf
sConsulta = sConsulta & "            INNER JOIN PROCE_ATRIB PA" & vbCrLf
sConsulta = sConsulta & "                    ON OA.ATRIB_ID = PA.ID" & vbCrLf
sConsulta = sConsulta & "                   AND OA.ANYO = PA.ANYO" & vbCrLf
sConsulta = sConsulta & "                   AND OA.GMN1 = PA.GMN1" & vbCrLf
sConsulta = sConsulta & "                   AND OA.PROCE = PA.PROCE" & vbCrLf
sConsulta = sConsulta & "            INNER JOIN DEF_ATRIB DA" & vbCrLf
sConsulta = sConsulta & "                    ON PA.ATRIB = DA.ID" & vbCrLf
sConsulta = sConsulta & "             LEFT JOIN PROCE_ATRIB_LISTA DAL" & vbCrLf
sConsulta = sConsulta & "                    ON PA.ID = DAL.ATRIB_ID" & vbCrLf
sConsulta = sConsulta & "     WHERE OA.ANYO= @ANYO" & vbCrLf
sConsulta = sConsulta & "       AND OA.GMN1= @GMN1" & vbCrLf
sConsulta = sConsulta & "       AND OA.PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "       AND OA.OFE = @OFE" & vbCrLf
sConsulta = sConsulta & "       AND OA.PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "       AND OA.GRUPO = @GRUPO" & vbCrLf
sConsulta = sConsulta & "     ORDER BY PA.GRUPO,PA.ORDEN_ATRIB" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ELSE IF @AMBITO = 3" & vbCrLf
sConsulta = sConsulta & "    SELECT PA.ATRIB, PA.ID, DA.COD, DA.DEN, PA.INTRO, DA.TIPO, " & vbCrLf
sConsulta = sConsulta & "           PA.MINNUM, PA.MINFEC," & vbCrLf
sConsulta = sConsulta & "           PA.MAXNUM, PA.MAXFEC," & vbCrLf
sConsulta = sConsulta & "           NULL OAVALOR_TEXT," & vbCrLf
sConsulta = sConsulta & "           NULL OAVALOR_NUM," & vbCrLf
sConsulta = sConsulta & "           NULL OAVALOR_FEC," & vbCrLf
sConsulta = sConsulta & "           NULL OAVALOR_BOOL," & vbCrLf
sConsulta = sConsulta & "           DAL.VALOR_TEXT DALVALOR_TEXT," & vbCrLf
sConsulta = sConsulta & "           DAL.VALOR_NUM DALVALOR_NUM," & vbCrLf
sConsulta = sConsulta & "           DAL.VALOR_FEC DALVALOR_FEC" & vbCrLf
sConsulta = sConsulta & "     FROM PROCE_ATRIB PA" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN DEF_ATRIB DA" & vbCrLf
sConsulta = sConsulta & "                   ON PA.ATRIB = DA.ID" & vbCrLf
sConsulta = sConsulta & "            LEFT JOIN PROCE_ATRIB_LISTA DAL" & vbCrLf
sConsulta = sConsulta & "                   ON PA.ID = DAL.ATRIB_ID" & vbCrLf
sConsulta = sConsulta & "    WHERE PA.ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "      AND PA.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "      AND PA.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "      AND ISNULL(PA.GRUPO,'#') = CASE WHEN PA.GRUPO IS NULL THEN ISNULL(PA.GRUPO,'#') ELSE @GRUPO END" & vbCrLf
sConsulta = sConsulta & "      AND PA.AMBITO = 3" & vbCrLf
sConsulta = sConsulta & "    ORDER BY PA.GRUPO,PA.ORDEN_ATRIB" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE SP_ELIM_OFERTAS_NO_ENVIADAS(@ANYO SMALLINT,@GMN1 VARCHAR(50), @PROCE INT, @RES INTEGER OUTPUT)  AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVE VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @OFE INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE Cur_PROCE CURSOR LOCAL FOR SELECT PROVE,OFE  FROM PROCE_OFE  WHERE PROCE_OFE.ANYO=@ANYO AND PROCE_OFE.GMN1=@GMN1 AND PROCE_OFE.PROCE=@PROCE AND PROCE_OFE.ENVIADA=0" & vbCrLf
sConsulta = sConsulta & "OPEN Cur_PROCE" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM Cur_PROCE INTO @PROVE,@OFE" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "     DELETE FROM PROCE_OFE_ADJUN WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE AND OFE=@OFE" & vbCrLf
sConsulta = sConsulta & "     DELETE FROM OFE_GR_ADJUN WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE AND OFE=@OFE" & vbCrLf
sConsulta = sConsulta & "     DELETE FROM OFE_ITEM_ADJUN WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE AND OFE=@OFE" & vbCrLf
sConsulta = sConsulta & "     DELETE FROM OFE_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE AND OFE=@OFE" & vbCrLf
sConsulta = sConsulta & "     DELETE FROM OFE_GR_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE AND OFE=@OFE" & vbCrLf
sConsulta = sConsulta & "     DELETE FROM OFE_ITEM_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE AND OFE=@OFE" & vbCrLf
sConsulta = sConsulta & "     DELETE FROM GRUPO_OFE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE AND OFE=@OFE" & vbCrLf
sConsulta = sConsulta & "     DELETE FROM GRUPO_OFE_REU WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE AND OFE=@OFE" & vbCrLf
sConsulta = sConsulta & "     DELETE FROM ITEM_OFE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE AND NUM=@OFE" & vbCrLf
sConsulta = sConsulta & "     DELETE FROM PROCE_OFE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE AND OFE=@OFE" & vbCrLf
sConsulta = sConsulta & "     DELETE FROM PROCE_OFE_WEB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE AND OFE=@OFE    " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM Cur_PROCE INTO @PROVE,@OFE" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "Close Cur_PROCE" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE Cur_PROCE" & vbCrLf
sConsulta = sConsulta & "SET @RES=@@ERROR" & vbCrLf
sConsulta = sConsulta & "RETURN" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE FSEP_COMPARAR_ARTICULOS AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @TIPOATRIB tinyint" & vbCrLf
sConsulta = sConsulta & "DECLARE @CODATRIB varchar(500)" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDATRIB int" & vbCrLf
sConsulta = sConsulta & "DECLARE @PARAMDEF NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @ALTER NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @INSERT NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "set nocount on" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE C1 CURSOR LOCAL FOR" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT PAA.ATRIB, DA.COD, DA.TIPO" & vbCrLf
sConsulta = sConsulta & "  FROM PROVE_ART4_ATRIB PAA" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN #ARTS_COMP T" & vbCrLf
sConsulta = sConsulta & "               ON PAA.PROVE = T.PROVE" & vbCrLf
sConsulta = sConsulta & "              AND PAA.GMN1 = T.GMN1" & vbCrLf
sConsulta = sConsulta & "              AND PAA.GMN2 = T.GMN2" & vbCrLf
sConsulta = sConsulta & "              AND PAA.GMN3 = T.GMN3" & vbCrLf
sConsulta = sConsulta & "              AND PAA.GMN4 = T.GMN4" & vbCrLf
sConsulta = sConsulta & "              AND PAA.ART = T.ART" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN DEF_ATRIB DA" & vbCrLf
sConsulta = sConsulta & "               ON PAA.ATRIB = DA.ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE #ATRIBS (ATID INT NULL)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF EXISTS(SELECT @@VERSION WHERE @@VERSION LIKE '%2000%')" & vbCrLf
sConsulta = sConsulta & "SET @SQL='" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE #ATRIBS ADD " & vbCrLf
sConsulta = sConsulta & "   ATPROVE VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ," & vbCrLf
sConsulta = sConsulta & "   ATGMN1 varchar (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ," & vbCrLf
sConsulta = sConsulta & "   ATGMN2 varchar (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ," & vbCrLf
sConsulta = sConsulta & "   ATGMN3 varchar (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ," & vbCrLf
sConsulta = sConsulta & "   ATGMN4 varchar (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ," & vbCrLf
sConsulta = sConsulta & "   ATART varchar (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL " & vbCrLf
sConsulta = sConsulta & "'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL='" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE #ATRIBS ADD " & vbCrLf
sConsulta = sConsulta & "   ATPROVE VARCHAR(50) NULL ," & vbCrLf
sConsulta = sConsulta & "   ATGMN1 varchar (50) NULL ," & vbCrLf
sConsulta = sConsulta & "   ATGMN2 varchar (50) NULL ," & vbCrLf
sConsulta = sConsulta & "   ATGMN3 varchar (50) NULL ," & vbCrLf
sConsulta = sConsulta & "   ATGMN4 varchar (50) NULL ," & vbCrLf
sConsulta = sConsulta & "   ATART varchar (50) NULL" & vbCrLf
sConsulta = sConsulta & "'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL  @SQL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @INSERT = 'INSERT INTO #ATRIBS (ATID, ATPROVE, ATGMN1, ATGMN2, ATGMN3, ATGMN4, ATART, '" & vbCrLf
sConsulta = sConsulta & "SET @SQL='SELECT T.ID, PAA.PROVE, PAA.GMN1, PAA.GMN2, PAA.GMN3, PAA.GMN4, PAA.ART, '" & vbCrLf
sConsulta = sConsulta & "set @ALTER = 'ALTER TABLE #ATRIBS ADD '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "OPEN C1" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C1 INTO @IDATRIB, @CODATRIB, @TIPOATRIB" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS = 0" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    " & vbCrLf
sConsulta = sConsulta & "    SET @INSERT = @INSERT + '[AT_' + @CODATRIB + '],'" & vbCrLf
sConsulta = sConsulta & "    IF @TIPOATRIB = 1 " & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "        SET @ALTER = @ALTER  + '[AT_' + @CODATRIB + '] VARCHAR(800), '" & vbCrLf
sConsulta = sConsulta & "        SET @SQL = @SQL + 'MAX(case PAA.ATRIB when ' + cast(@IDATRIB as varchar(100)) + ' THEN VALOR_TEXT else NULL end) as [' + @CODATRIB + '],'" & vbCrLf
sConsulta = sConsulta & "      END " & vbCrLf
sConsulta = sConsulta & "    ELSE IF @TIPOATRIB = 2" & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "        SET @ALTER = @ALTER  + '[AT_' + @CODATRIB + '] FLOAT, '" & vbCrLf
sConsulta = sConsulta & "        SET @SQL = @SQL + 'MAX(case PAA.ATRIB when ' + cast(@IDATRIB as varchar(100)) + ' THEN VALOR_NUM else NULL end) as [' + @CODATRIB + '],'" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "    ELSE IF @TIPOATRIB = 3" & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "        SET @ALTER = @ALTER  + '[AT_' + @CODATRIB + '] DATETIME, '" & vbCrLf
sConsulta = sConsulta & "        SET @SQL = @SQL + 'MAX(case PAA.ATRIB when ' + cast(@IDATRIB as varchar(100)) + ' THEN VALOR_FEC else NULL end) as [' + @CODATRIB + '],'" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "    ELSE " & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "        SET @ALTER = @ALTER + '[AT_' + @CODATRIB + '] TINYINT, '" & vbCrLf
sConsulta = sConsulta & "        SET @SQL = @SQL + 'MAX(case PAA.ATRIB when ' + cast(@IDATRIB as varchar(100)) + ' THEN VALOR_BOOL else NULL end) as [' + @CODATRIB + '],'" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "    FETCH NEXT FROM C1 INTO @IDATRIB, @CODATRIB, @TIPOATRIB" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "if @INSERT != 'INSERT INTO #ATRIBS (ATID, ATPROVE, ATGMN1, ATGMN2, ATGMN3, ATGMN4, ATART, '" & vbCrLf
sConsulta = sConsulta & "  begin" & vbCrLf
sConsulta = sConsulta & "    set @SQL = LEFT(@SQL,LEN(@SQL)-1)" & vbCrLf
sConsulta = sConsulta & "    set @INSERT = LEFT(@INSERT,LEN(@INSERT)-1)" & vbCrLf
sConsulta = sConsulta & "    set @ALTER = LEFT(@ALTER,LEN(@ALTER)-1)" & vbCrLf
sConsulta = sConsulta & "    EXEC SP_EXECUTESQL  @ALTER " & vbCrLf
sConsulta = sConsulta & "    SET @SQL = @INSERT + ')" & vbCrLf
sConsulta = sConsulta & "    ' + @SQL + '" & vbCrLf
sConsulta = sConsulta & "      FROM PROVE_ART4_ATRIB PAA" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN #ARTS_COMP T" & vbCrLf
sConsulta = sConsulta & "               ON PAA.PROVE = T.PROVE" & vbCrLf
sConsulta = sConsulta & "              AND PAA.GMN1 = T.GMN1" & vbCrLf
sConsulta = sConsulta & "              AND PAA.GMN2 = T.GMN2" & vbCrLf
sConsulta = sConsulta & "              AND PAA.GMN3 = T.GMN3" & vbCrLf
sConsulta = sConsulta & "              AND PAA.GMN4 = T.GMN4" & vbCrLf
sConsulta = sConsulta & "              AND PAA.ART = T.ART" & vbCrLf
sConsulta = sConsulta & "    GROUP BY T.ID, PAA.PROVE, PAA.GMN1, PAA.GMN2, PAA.GMN3, PAA.GMN4, PAA.ART'" & vbCrLf
sConsulta = sConsulta & "    EXEC SP_EXECUTESQL  @SQL" & vbCrLf
sConsulta = sConsulta & "  end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF" & vbCrLf
sConsulta = sConsulta & "SELECT AC.ID, P.DEN PROVEDEN,  CASE WHEN ART_AUX.DEN IS NULL THEN A4.DEN ELSE ART_AUX.DEN END + I.DESCR  ARTDEN, CL.PRECIO_UC, CL.UP_DEF, CL.FC_DEF,CL.CANT_MIN_DEF, DATALENGTH(PA.IMAGEN) DATASIZE,  A.*" & vbCrLf
sConsulta = sConsulta & "  FROM #ARTS_COMP AC" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN" & vbCrLf
sConsulta = sConsulta & "              (CATALOG_LIN CL " & vbCrLf
sConsulta = sConsulta & "                    LEFT JOIN ITEM I" & vbCrLf
sConsulta = sConsulta & "                           ON CL.GMN1=I.GMN1 " & vbCrLf
sConsulta = sConsulta & "                          AND CL.ANYO=I.ANYO " & vbCrLf
sConsulta = sConsulta & "                          AND CL.PROCE=I.PROCE " & vbCrLf
sConsulta = sConsulta & "                          AND CL.ITEM=I.ID" & vbCrLf
sConsulta = sConsulta & "                    LEFT JOIN ART4 A4" & vbCrLf
sConsulta = sConsulta & "                           ON CL.GMN1=A4.GMN1 " & vbCrLf
sConsulta = sConsulta & "                          AND CL.GMN2=A4.GMN2 " & vbCrLf
sConsulta = sConsulta & "                          AND CL.GMN3=A4.GMN3 " & vbCrLf
sConsulta = sConsulta & "                          AND CL.GMN4=A4.GMN4 " & vbCrLf
sConsulta = sConsulta & "                          AND CL.ART_INT + I.ART=A4.COD " & vbCrLf
sConsulta = sConsulta & "                    LEFT JOIN ART4 ART_AUX " & vbCrLf
sConsulta = sConsulta & "                           ON I.GMN1=ART_AUX.GMN1 " & vbCrLf
sConsulta = sConsulta & "                          AND I.GMN2=ART_AUX.GMN2 " & vbCrLf
sConsulta = sConsulta & "                          AND I.GMN3=ART_AUX.GMN3 " & vbCrLf
sConsulta = sConsulta & "                          AND I.GMN4=ART_AUX.GMN4 " & vbCrLf
sConsulta = sConsulta & "                          AND I.ART=ART_AUX.COD " & vbCrLf
sConsulta = sConsulta & "                    LEFT JOIN PROVE_ART4 PA" & vbCrLf
sConsulta = sConsulta & "                           ON CL.GMN1=PA.GMN1 " & vbCrLf
sConsulta = sConsulta & "                          AND CL.GMN2=PA.GMN2 " & vbCrLf
sConsulta = sConsulta & "                          AND CL.GMN3=PA.GMN3 " & vbCrLf
sConsulta = sConsulta & "                          AND CL.GMN4=PA.GMN4 " & vbCrLf
sConsulta = sConsulta & "                          AND CASE WHEN CL.ART_INT IS NULL THEN I.ART ELSE CL.ART_INT  END =PA.ART " & vbCrLf
sConsulta = sConsulta & "                          AND CL.PROVE=PA.PROVE)" & vbCrLf
sConsulta = sConsulta & "             ON AC.ID = CL.ID" & vbCrLf
sConsulta = sConsulta & "    LEFT JOIN #ATRIBS A" & vbCrLf
sConsulta = sConsulta & "           ON AC.ID = A.ATID" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PROVE P" & vbCrLf
sConsulta = sConsulta & "           ON AC.PROVE = P.COD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
sConsulta = sConsulta & "set nocount off" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf


sConsulta = ""
sConsulta = sConsulta & ""
sConsulta = sConsulta & "CREATE PROCEDURE SP_CARGAR_CONFIG_PROCE @ANYO INT, @GMN1 VARCHAR(50), @PROCE INT,@DEFDEST tinyint  = null OUTPUT, @DEFPAG tinyint = null OUTPUT," & vbCrLf
sConsulta = sConsulta & "                                        @DEFFECSUM tinyint  = null OUTPUT, @DEFPROCEESP tinyint  = null OUTPUT, @DEFGRUPOESP  tinyint = null OUTPUT, @DEFITEMESP tinyint = null OUTPUT, " & vbCrLf
sConsulta = sConsulta & "                                        @DEFOFEADJUN tinyint = null OUTPUT, @DEFGRUPOADJUN tinyint = null OUTPUT, @DEFITEMADJUN tinyint = null OUTPUT," & vbCrLf
sConsulta = sConsulta & "                                        @DEFSUBASTA tinyint = null OUTPUT, @DEFSUVERPROVE tinyint = null OUTPUT, @DEFSUVERPUJAS tinyint = null OUTPUT, @DEFSUMINUTOS int = null OUTPUT," & vbCrLf
sConsulta = sConsulta & "                                        @DEFPRECALTER tinyint = null OUTPUT, @DEFCANTMAX tinyint = null OUTPUT, @HAYATRIBUTOS AS tinyint = null OUTPUT, " & vbCrLf
sConsulta = sConsulta & "                                        @CAMBIARMON tinyint = 1 OUTPUT, @ADMINPUB tinyint=null OUTPUT, @NUMMAXOFE int = 0 OUTPUT, @PUBLICARFINSUM tinyint = null OUTPUT AS" & vbCrLf
sConsulta = sConsulta & "set nocount on" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCEEST INT" & vbCrLf
sConsulta = sConsulta & "SELECT @PROCEEST = EST " & vbCrLf
sConsulta = sConsulta & "  FROM PROCE " & vbCrLf
sConsulta = sConsulta & " WHERE ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "   AND GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "   AND COD = @PROCE" & vbCrLf
sConsulta = sConsulta & "SELECT @DEFDEST=PD.DEST, @DEFPAG=PD.PAG, @DEFFECSUM=PD.FECSUM, @DEFPROCEESP=PD.PROCE_ESP, @DEFGRUPOESP = PD.GRUPO_ESP, @DEFITEMESP  = PD.ITEM_ESP, " & vbCrLf
sConsulta = sConsulta & "       @DEFOFEADJUN = PD.OFE_ADJUN, @DEFGRUPOADJUN=PD.GRUPO_ADJUN, @DEFITEMADJUN= PD.ITEM_ADJUN, @DEFCANTMAX=PD.SOLCANTMAX, @DEFPRECALTER = PD.PRECALTER, " & vbCrLf
sConsulta = sConsulta & "       @DEFSUBASTA = PD.SUBASTA, @DEFSUVERPROVE = PD.SUBASTAPROVE, @DEFSUVERPUJAS=PD.SUBASTAPUJAS, @DEFSUMINUTOS = PD.SUBASTAESPERA," & vbCrLf
sConsulta = sConsulta & "       @HAYATRIBUTOS = ISNULL(PA.ATRIB,0),@CAMBIARMON=PD.CAMBIARMON, @ADMINPUB = ADMIN_PUB, @NUMMAXOFE = PD.MAX_OFE , @PUBLICARFINSUM = PD.PUBLICARFINSUM" & vbCrLf
sConsulta = sConsulta & "  FROM PROCE_DEF PD" & vbCrLf
sConsulta = sConsulta & "          LEFT JOIN (SELECT ANYO, GMN1, PROCE, COUNT(ATRIB ) ATRIB" & vbCrLf
sConsulta = sConsulta & "                        FROM PROCE_ATRIB" & vbCrLf
sConsulta = sConsulta & "                       WHERE ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "                         AND GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "                         AND PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "                         AND AMBITO = 1" & vbCrLf
sConsulta = sConsulta & "                    GROUP BY ANYO, GMN1, PROCE) PA" & vbCrLf
sConsulta = sConsulta & "                  ON PD.ANYO = PA.ANYO" & vbCrLf
sConsulta = sConsulta & "                 AND PD.GMN1 = PA.GMN1" & vbCrLf
sConsulta = sConsulta & "                 AND PD.PROCE = PA.PROCE " & vbCrLf
sConsulta = sConsulta & " WHERE PD.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "   AND PD.GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "   AND PD.PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "  " & vbCrLf
sConsulta = sConsulta & "select PG.anyo, PG.gmn1, PG.proce, PG.COD , PG.DEN, COUNT(PA.ATRIB) ATRIB, S.SOBRE, S.USUAPER, S.EST, S.FECAPE, S.FECEST, S.OBS," & vbCrLf
sConsulta = sConsulta & "       sum(case dato when 'ESP' THEN 1 else 0 end) as ESP," & vbCrLf
sConsulta = sConsulta & "       sum(case dato when 'PRES1' THEN 1 ELSE 0 END) AS PRES1," & vbCrLf
sConsulta = sConsulta & "       sum(case dato when 'PRES2' THEN 1 ELSE 0 END) AS PRES2," & vbCrLf
sConsulta = sConsulta & "       sum(case dato when 'PRESANU1' THEN 1 ELSE 0 END) AS PRESANU1," & vbCrLf
sConsulta = sConsulta & "       sum(case dato when 'PRESANU2' THEN 1 ELSE 0 END) AS PRESANU2," & vbCrLf
sConsulta = sConsulta & "       sum(case dato when 'DIST' THEN 1 ELSE 0 END) AS DIST," & vbCrLf
sConsulta = sConsulta & "       sum(case dato when 'PROVE' THEN 1 ELSE 0 END) AS PROVE," & vbCrLf
sConsulta = sConsulta & "       sum(case dato when 'FECSUM' THEN 1 ELSE 0 END) AS FECSUM," & vbCrLf
sConsulta = sConsulta & "       sum(case dato when 'PAG' THEN 1 ELSE 0 END) AS PAG," & vbCrLf
sConsulta = sConsulta & "       sum(case dato when 'DEST' THEN 1 ELSE 0 END) AS DEST," & vbCrLf
sConsulta = sConsulta & "       ISNULL(IT.NUMITEMS,0) NUMITEMS" & vbCrLf
sConsulta = sConsulta & "  from PROCE_GRUPO PG " & vbCrLf
sConsulta = sConsulta & "          LEFT JOIN PROCE_GRUPO_DEF PGD" & vbCrLf
sConsulta = sConsulta & "                  ON PG.ANYO = PGD.ANYO" & vbCrLf
sConsulta = sConsulta & "                 AND PG.GMN1 = PGD.GMN1" & vbCrLf
sConsulta = sConsulta & "                 AND PG.PROCE = PGD.PROCE" & vbCrLf
sConsulta = sConsulta & "                 AND PG.COD = PGD.GRUPO" & vbCrLf
sConsulta = sConsulta & "           LEFT JOIN (SELECT ANYO, GMN1, PROCE, ISNULL(GRUPO,'#T#') GRUPO, ATRIB " & vbCrLf
sConsulta = sConsulta & "                        FROM PROCE_ATRIB " & vbCrLf
sConsulta = sConsulta & "                       WHERE ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "                         AND GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "                         AND PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "                         AND AMBITO = 2) PA" & vbCrLf
sConsulta = sConsulta & "                  ON PG.ANYO = PA.ANYO" & vbCrLf
sConsulta = sConsulta & "                 AND PG.GMN1 = PA.GMN1" & vbCrLf
sConsulta = sConsulta & "                 AND PG.PROCE = PA.PROCE" & vbCrLf
sConsulta = sConsulta & "                 AND PG.COD = CASE WHEN PA.GRUPO ='#T#' THEN PG.COD ELSE PA.GRUPO END" & vbCrLf
sConsulta = sConsulta & "          LEFT JOIN (SELECT I.GRUPO, count(I.id) NUMITEMS" & vbCrLf
sConsulta = sConsulta & "                        FROM ITEM I " & vbCrLf
sConsulta = sConsulta & "                      WHERE I.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "                        AND I.GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "                        AND I.PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "                        AND I.CONF = 1" & vbCrLf
sConsulta = sConsulta & "                        AND CASE WHEN I.EST IS NULL THEN -1 ELSE I.EST END = CASE WHEN @PROCEEST=11 THEN 0 ELSE -1 END" & vbCrLf
sConsulta = sConsulta & "                     GROUP BY I.GRUPO) IT" & vbCrLf
sConsulta = sConsulta & "                  ON PG.COD = IT.GRUPO" & vbCrLf
sConsulta = sConsulta & "           LEFT JOIN SOBRE S" & vbCrLf
sConsulta = sConsulta & "                  ON PG.ANYO = S.ANYO" & vbCrLf
sConsulta = sConsulta & "                 AND PG.GMN1 = S.GMN1" & vbCrLf
sConsulta = sConsulta & "                 AND PG.PROCE = S.PROCE" & vbCrLf
sConsulta = sConsulta & "                 AND PG.SOBRE = S.SOBRE" & vbCrLf
sConsulta = sConsulta & "where PG.anyo = @ANYO" & vbCrLf
sConsulta = sConsulta & "  and PG.gmn1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "  and PG.proce = @PROCE" & vbCrLf
sConsulta = sConsulta & "  and isnull(pg.cerrado,0) = 0" & vbCrLf
sConsulta = sConsulta & "group by PG.anyo, PG.gmn1, PG.proce, PG.cod, PG.DEN, IT.NUMITEMS, S.SOBRE, S.USUAPER, S.EST, S.FECAPE, S.FECEST, S.OBS" & vbCrLf
sConsulta = sConsulta & "ORDER BY S.SOBRE,PG.COD" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
End Sub

Private Sub V_2_12_5_Inicializar_Orden_Atributos()
Dim sConsulta As String
Dim sql As String

Dim rdoRS As rdo.rdoResultset

Dim oldAnyo As Integer
Dim oldGMN1 As String
Dim oldProce As Long
Dim oldGrupo As Variant
Dim oldPlant As Long


Dim newAnyo As Integer
Dim newGMN1 As String
Dim newProce As Long
Dim newGrupo As Variant
Dim newPlant As Long

Dim iContador As Integer

sConsulta = sConsulta & "select pa.id, pa.anyo, pa.gmn1, pa.proce, pa.grupo, da.cod" & vbCrLf
sConsulta = sConsulta & "  from proce_atrib pa " & vbCrLf
sConsulta = sConsulta & "     inner join def_atrib da " & vbCrLf
sConsulta = sConsulta & "             on pa.atrib = da.id" & vbCrLf
sConsulta = sConsulta & "order by pa.anyo, pa.gmn1, pa.proce, pa.grupo, da.cod" & vbCrLf

Set rdoRS = gRDOCon.OpenResultset(sConsulta, rdOpenDynamic)

iContador = 0

oldGrupo = "###"

While Not rdoRS.EOF
    
    newAnyo = rdoRS.rdoColumns("ANYO").Value
    newGMN1 = rdoRS.rdoColumns("GMN1").Value
    newProce = rdoRS.rdoColumns("PROCE").Value
    newGrupo = rdoRS.rdoColumns("GRUPO").Value
    If IsNull(newGrupo) Then
        newGrupo = "###"
    End If
    
    If newAnyo <> oldAnyo Or newGMN1 <> oldGMN1 Or newProce <> oldProce Or newGrupo <> oldGrupo Then
        iContador = 0
    End If
    iContador = iContador + 1
    sql = "UPDATE  PROCE_ATRIB SET ORDEN_ATRIB = " & iContador & " WHERE ID = " & rdoRS.rdoColumns("ID").Value
    
    ExecuteSQL gRDOCon, sql
    
    oldAnyo = newAnyo
    oldGMN1 = newGMN1
    oldProce = newProce
    oldGrupo = newGrupo
    
    rdoRS.MoveNext
Wend
rdoRS.Close

sConsulta = ""
sConsulta = sConsulta & "select pa.id, pa.plant, pa.grupo, da.cod" & vbCrLf
sConsulta = sConsulta & "  from plantilla_atrib pa " & vbCrLf
sConsulta = sConsulta & "     inner join def_atrib da " & vbCrLf
sConsulta = sConsulta & "             on pa.atrib = da.id" & vbCrLf
sConsulta = sConsulta & "order by pa.plant, pa.grupo, da.cod" & vbCrLf

Set rdoRS = gRDOCon.OpenResultset(sConsulta, rdOpenDynamic)


oldGrupo = "###"

iContador = 0
While Not rdoRS.EOF
    
    newPlant = rdoRS.rdoColumns("PLANT").Value
    newGrupo = rdoRS.rdoColumns("GRUPO").Value
    If IsNull(newGrupo) Then
        newGrupo = "###"
    End If
    
    If newPlant <> oldPlant Or newGrupo <> oldGrupo Then
        iContador = 0
    End If
    iContador = iContador + 1
    sql = "UPDATE  PLANTILLA_ATRIB SET ORDEN_ATRIB = " & iContador & " WHERE ID = " & rdoRS.rdoColumns("ID").Value
    
    ExecuteSQL gRDOCon, sql
    
    oldPlant = newPlant
    oldGrupo = newGrupo
    
    rdoRS.MoveNext
Wend
rdoRS.Close


Set rdoRS = Nothing

End Sub

Private Sub V_2_12_5_Storeds_1()
Dim sConsulta As String
sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_AR_VOLUMENES]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_AR_VOLUMENES]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE   PROCEDURE  SP_AR_VOLUMENES (@ANYO SMALLINT,@GMN1 VARCHAR(50), @PROCE INT,@FECULT DATETIME,@DEN VARCHAR(200),@RES INT OUTPUT)  AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVE AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSP_COD AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @VOL AS FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @SRV AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @BD AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @CODCIA AS VARCHAR(80)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL AS VARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @MONCEN AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @MONPORT AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @SRV=FSP_SRV,@BD=FSP_BD,@CODCIA=FSP_CIA FROM PARGEN_PORT WHERE ID=1" & vbCrLf
sConsulta = sConsulta & "SELECT @MONCEN=MONCEN FROM PARGEN_DEF WHERE ID=1" & vbCrLf
sConsulta = sConsulta & "SELECT @MONPORT= FSP_COD FROM MON WHERE COD= @MONCEN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE Cur_VOL Cursor FORWARD_ONLY STATIC READ_ONLY FOR " & vbCrLf
sConsulta = sConsulta & "SELECT AR_ITEM_PROVE.PROVE,FSP_COD,SUM(AR_ITEM_PROVE.PREC) FROM AR_ITEM_PROVE" & vbCrLf
sConsulta = sConsulta & "  INNER JOIN PROVE ON AR_ITEM_PROVE.PROVE= PROVE.COD WHERE AR_ITEM_PROVE.ANYO=@ANYO AND  AR_ITEM_PROVE.GMN1=@GMN1 AND AR_ITEM_PROVE.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "  AND AR_ITEM_PROVE.PROVE IN (SELECT PROVE FROM PROCE_OFE_WEB WHERE ANYO=@ANYO AND PROCE_OFE_WEB.GMN1 = @GMN1 AND PROCE_OFE_WEB.PROCE=@PROCE)" & vbCrLf
sConsulta = sConsulta & "  GROUP BY AR_ITEM_PROVE.PROVE,FSP_COD" & vbCrLf
sConsulta = sConsulta & "OPEN Cur_VOL" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM Cur_VOL INTO @PROVE,@FSP_COD,@VOL" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "     SET @SQL=@SRV + '.' + @BD + '.dbo.SP_ADD_VOLADJ'" & vbCrLf
sConsulta = sConsulta & "      EXECUTE @SQL @CODCIA,@PROVE,@ANYO,@GMN1,@PROCE,@VOL,@MONCEN,@MONPORT,@FECULT,@DEN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM Cur_VOL INTO @PROVE,@FSP_COD,@VOL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "ERR_:" & vbCrLf
sConsulta = sConsulta & "Close Cur_VOL" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE Cur_VOL" & vbCrLf
sConsulta = sConsulta & "SET @RES=@@ERROR" & vbCrLf
sConsulta = sConsulta & "Return" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Private Sub V_2_12_5_Triggers_1()
Dim sConsulta As String
sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PLANTILLA_CONF_VISTAS_GRUPO_ATRIB_TG_DEL]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PLANTILLA_CONF_VISTAS_GRUPO_ATRIB_TG_DEL]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PLANTILLA_CONF_VISTAS_PROCE_ATRIB_TG_DEL]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PLANTILLA_CONF_VISTAS_PROCE_ATRIB_TG_DEL]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PLANTILLA_CONF_VISTAS_ALL_ATRIB_TG_DEL]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PLANTILLA_CONF_VISTAS_ALL_ATRIB_TG_DEL]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER [PLANTILLA_CONF_VISTAS_GRUPO_ATRIB_TG_DEL] ON [dbo].[PLANTILLA_CONF_VISTAS_GRUPO_ATRIB] " & vbCrLf
sConsulta = sConsulta & "FOR DELETE " & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @PLANT AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @POSICION AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @GRUPO AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @VISTA AS INTEGER" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_PLANTILLA_Upd CURSOR LOCAL FOR SELECT PLANT,GRUPO,VISTA,POSICION FROM DELETED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_PLANTILLA_Upd" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PLANTILLA_Upd INTO @PLANT,@GRUPO,@VISTA,@POSICION" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "UPDATE PLANTILLA_CONF_VISTAS_GRUPO_ATRIB SET POSICION=POSICION-1 WHERE PLANT=@PLANT AND GRUPO=@GRUPO AND VISTA=@VISTA AND POSICION>@POSICION" & vbCrLf
sConsulta = sConsulta & "UPDATE PLANTILLA_CONF_VISTAS_GRUPO SET PRECIO_PROV_POS=PRECIO_PROV_POS-1 WHERE PLANT=@PLANT AND GRUPO=@GRUPO AND VISTA=@VISTA AND PRECIO_PROV_POS>@POSICION" & vbCrLf
sConsulta = sConsulta & "UPDATE PLANTILLA_CONF_VISTAS_GRUPO SET CANT_PROV_POS=CANT_PROV_POS-1 WHERE PLANT=@PLANT AND GRUPO=@GRUPO AND VISTA=@VISTA AND CANT_PROV_POS>@POSICION" & vbCrLf
sConsulta = sConsulta & "UPDATE PLANTILLA_CONF_VISTAS_GRUPO SET ADJ_PROV_POS=ADJ_PROV_POS-1 WHERE PLANT=@PLANT AND GRUPO=@GRUPO AND VISTA=@VISTA AND ADJ_PROV_POS>@POSICION" & vbCrLf
sConsulta = sConsulta & "UPDATE PLANTILLA_CONF_VISTAS_GRUPO SET IMP_ADJ_POS=IMP_ADJ_POS-1 WHERE PLANT=@PLANT AND GRUPO=@GRUPO AND VISTA=@VISTA AND IMP_ADJ_POS>@POSICION" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PLANTILLA_Upd INTO @PLANT,@GRUPO,@VISTA,@POSICION" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_PLANTILLA_Upd" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_PLANTILLA_Upd" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER [PLANTILLA_CONF_VISTAS_PROCE_ATRIB_TG_DEL] ON [dbo].[PLANTILLA_CONF_VISTAS_PROCE_ATRIB] " & vbCrLf
sConsulta = sConsulta & "FOR DELETE " & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @PLANT AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @POSICION AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @GRUPO AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @VISTA AS INTEGER" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_PLANTILLA_Upd CURSOR LOCAL FOR SELECT PLANT,VISTA,POS FROM DELETED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_PLANTILLA_Upd" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PLANTILLA_Upd INTO @PLANT,@VISTA,@POSICION" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "UPDATE PLANTILLA_CONF_VISTAS_PROCE_ATRIB SET POS=POS-1 WHERE PLANT=@PLANT AND VISTA=@VISTA AND POS>@POSICION" & vbCrLf
sConsulta = sConsulta & "UPDATE PLANTILLA_CONF_VISTAS_PROCE SET CONSUMIDO_POS=CONSUMIDO_POS-1 WHERE PLANT=@PLANT AND VISTA=@VISTA AND CONSUMIDO_POS>@POSICION" & vbCrLf
sConsulta = sConsulta & "UPDATE PLANTILLA_CONF_VISTAS_PROCE SET ADJ_POS=ADJ_POS-1 WHERE PLANT=@PLANT AND VISTA=@VISTA AND ADJ_POS>@POSICION" & vbCrLf
sConsulta = sConsulta & "UPDATE PLANTILLA_CONF_VISTAS_PROCE SET AHORR_ADJ_POS=AHORR_ADJ_POS-1 WHERE PLANT=@PLANT AND VISTA=@VISTA AND AHORR_ADJ_POS>@POSICION" & vbCrLf
sConsulta = sConsulta & "UPDATE PLANTILLA_CONF_VISTAS_PROCE SET AHORR_ADJ_PORCEN_POS=AHORR_ADJ_PORCEN_POS-1 WHERE PLANT=@PLANT AND VISTA=@VISTA AND AHORR_ADJ_PORCEN_POS>@POSICION" & vbCrLf
sConsulta = sConsulta & "UPDATE PLANTILLA_CONF_VISTAS_PROCE SET IMP_POS=IMP_POS-1 WHERE PLANT=@PLANT AND VISTA=@VISTA AND IMP_POS>@POSICION" & vbCrLf
sConsulta = sConsulta & "UPDATE PLANTILLA_CONF_VISTAS_PROCE SET AHORR_OFE_POS=AHORR_OFE_POS-1 WHERE PLANT=@PLANT AND VISTA=@VISTA AND AHORR_OFE_POS>@POSICION" & vbCrLf
sConsulta = sConsulta & "UPDATE PLANTILLA_CONF_VISTAS_PROCE SET AHORR_OFE_PORCEN_POS=AHORR_OFE_PORCEN_POS-1 WHERE PLANT=@PLANT AND VISTA=@VISTA AND AHORR_OFE_PORCEN_POS>@POSICION" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PLANTILLA_Upd INTO @PLANT,@VISTA,@POSICION" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_PLANTILLA_Upd" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_PLANTILLA_Upd" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TRIGGER [PLANTILLA_CONF_VISTAS_ALL_ATRIB_TG_DEL] ON [dbo].[PLANTILLA_CONF_VISTAS_ALL_ATRIB] " & vbCrLf
sConsulta = sConsulta & "FOR DELETE " & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @PLANT AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @POSICION AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @VISTA AS INTEGER" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_PLANTILLA_Upd CURSOR LOCAL FOR SELECT PLANT,VISTA,POSICION FROM DELETED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_PLANTILLA_Upd" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PLANTILLA_Upd INTO @PLANT,@VISTA,@POSICION" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "UPDATE PLANTILLA_CONF_VISTAS_ALL_ATRIB SET POSICION=POSICION-1 WHERE PLANT=@PLANT AND VISTA=@VISTA AND POSICION>@POSICION" & vbCrLf
sConsulta = sConsulta & "UPDATE PLANTILLA_CONF_VISTAS_ALL SET PRECIO_PROV_POS=PRECIO_PROV_POS-1 WHERE PLANT=@PLANT AND VISTA=@VISTA AND PRECIO_PROV_POS>@POSICION" & vbCrLf
sConsulta = sConsulta & "UPDATE PLANTILLA_CONF_VISTAS_ALL SET CANT_PROV_POS=CANT_PROV_POS-1 WHERE PLANT=@PLANT AND VISTA=@VISTA AND CANT_PROV_POS>@POSICION" & vbCrLf
sConsulta = sConsulta & "UPDATE PLANTILLA_CONF_VISTAS_ALL SET ADJ_PROV_POS=ADJ_PROV_POS-1 WHERE PLANT=@PLANT AND VISTA=@VISTA AND ADJ_PROV_POS>@POSICION" & vbCrLf
sConsulta = sConsulta & "UPDATE PLANTILLA_CONF_VISTAS_ALL SET IMP_ADJ_POS=IMP_ADJ_POS-1 WHERE PLANT=@PLANT AND VISTA=@VISTA AND IMP_ADJ_POS>@POSICION" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PLANTILLA_Upd INTO @PLANT,@VISTA,@POSICION" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_PLANTILLA_Upd" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_PLANTILLA_Upd" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
End Sub


Public Function CodigoDeActualizacion2_12_05_01A2_12_05_02() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim bBool As Boolean

On Error GoTo Error
    

sConsulta = "BEGIN TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = True
ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

frmProgreso.lblDetalle = "Modificamos tablas"
frmProgreso.lblDetalle.Refresh
frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_12_5_Tablas_2

frmProgreso.lblDetalle = "Modificamos stored procedures"
frmProgreso.lblDetalle.Refresh
frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_12_5_Storeds_2

frmProgreso.lblDetalle = "Modificamos triggers en tablas"
frmProgreso.lblDetalle.Refresh
frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_12_5_Triggers_2

sConsulta = "UPDATE VERSION SET NUM ='2.12.05.02'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "COMMIT TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = False

CodigoDeActualizacion2_12_05_01A2_12_05_02 = True
Exit Function
    
Error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_12_05_01A2_12_05_02 = False

End Function

Public Function CodigoDeActualizacion2_12_05_02A2_12_05_03() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim bBool As Boolean

On Error GoTo Error
    

sConsulta = "BEGIN TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = True
ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

frmProgreso.lblDetalle = "Modificamos tablas"
frmProgreso.lblDetalle.Refresh
frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_12_5_Tablas_3

frmProgreso.lblDetalle = "Modificamos stored procedures"
frmProgreso.lblDetalle.Refresh
frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_12_5_Storeds_3

frmProgreso.lblDetalle = "Modificamos triggers en tablas"
frmProgreso.lblDetalle.Refresh
frmProgreso.ProgressBar1.Value = 1

frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

sConsulta = "UPDATE VERSION SET NUM ='2.12.05.03'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
ExecuteSQL gRDOCon, sConsulta


sConsulta = "COMMIT TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = False

CodigoDeActualizacion2_12_05_02A2_12_05_03 = True
Exit Function
    
Error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_12_05_02A2_12_05_03 = False

End Function
Public Function CodigoDeActualizacion2_12_05_03A2_12_05_04() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim bBool As Boolean

On Error GoTo Error
    

sConsulta = "BEGIN TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = True
ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

frmProgreso.lblDetalle = "Actualizamos el id de las l�neas de los pedidos favoritos"
frmProgreso.lblDetalle.Refresh
frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

sConsulta = ""
sConsulta = sConsulta & "ALTER TABLE dbo.FAVORITOS_LINEAS_PEDIDO ADD" & vbCrLf
sConsulta = sConsulta & "   LINEA int NULL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "UPDATE FAVORITOS_LINEAS_PEDIDO" & vbCrLf
sConsulta = sConsulta & "   SET LINEA = CL.ID" & vbCrLf
sConsulta = sConsulta & "  FROM CATALOG_LIN CL" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN FAVORITOS_LINEAS_PEDIDO FLP" & vbCrLf
sConsulta = sConsulta & "                   ON ISNULL(CL.ART_INT,'#') = ISNULL(FLP.ART_INT,'#')" & vbCrLf
sConsulta = sConsulta & "                  AND ISNULL(CL.PROVE,'#') = ISNULL(FLP.PROVE,'#')" & vbCrLf
sConsulta = sConsulta & "                  AND ISNULL(CL.ANYO,-1) = ISNULL(FLP.ANYO,-1)" & vbCrLf
sConsulta = sConsulta & "                  AND ISNULL(CL.GMN1,'#') = ISNULL(FLP.GMN1,'#')" & vbCrLf
sConsulta = sConsulta & "                  AND ISNULL(CL.GMN2,'#') = ISNULL(FLP.GMN2,'#')" & vbCrLf
sConsulta = sConsulta & "                  AND ISNULL(CL.GMN3,'#') = ISNULL(FLP.GMN3,'#')" & vbCrLf
sConsulta = sConsulta & "                  AND ISNULL(CL.GMN4,'#') = ISNULL(FLP.GMN4,'#')" & vbCrLf
sConsulta = sConsulta & "                  AND ISNULL(CL.PROCE,-1) = ISNULL(FLP.PROCE,-1)" & vbCrLf
sConsulta = sConsulta & "                  AND ISNULL(CL.ITEM,-1) = ISNULL(FLP.ITEM,-1)" & vbCrLf
sConsulta = sConsulta & "                  AND ISNULL(CL.CAT1,-1) = ISNULL(FLP.CAT1,-1)" & vbCrLf
sConsulta = sConsulta & "                  AND ISNULL(CL.CAT2,-1) = ISNULL(FLP.CAT2,-1)" & vbCrLf
sConsulta = sConsulta & "                  AND ISNULL(CL.CAT3,-1) = ISNULL(FLP.CAT3,-1)" & vbCrLf
sConsulta = sConsulta & "                  AND ISNULL(CL.CAT4,-1) = ISNULL(FLP.CAT4,-1)" & vbCrLf
sConsulta = sConsulta & "                  AND ISNULL(CL.CAT5,-1) = ISNULL(FLP.CAT5,-1)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

'A�adido pedido por Pepe para que se actualicen los favoritos de las lineas de cat�logo
'que se han modificado (10/03/2004)
'Elimino las lineas de favoritos que no se corresponden con una linea de cat�logo
sConsulta = "DELETE FROM FAVORITOS_LINEAS_PEDIDO_ADJUN WHERE LINEA IN (SELECT ID FROM FAVORITOS_LINEAS_PEDIDO "
sConsulta = sConsulta & " WHERE FAVORITOS_LINEAS_PEDIDO.LINEA IS NULL AND FAVORITOS_LINEAS_PEDIDO.DESCR_LIBRE IS NULL)"
ExecuteSQL gRDOCon, sConsulta
sConsulta = "DELETE FROM FAVORITOS_LINEAS_PEDIDO WHERE LINEA IS NULL AND DESCR_LIBRE IS NULL"
ExecuteSQL gRDOCon, sConsulta
'Elimino las lineas de favoritos de l.catalogo no publicadas
sConsulta = "DELETE FROM FAVORITOS_LINEAS_PEDIDO_ADJUN WHERE  FAVORITOS_LINEAS_PEDIDO_ADJUN.LINEA IN "
sConsulta = sConsulta & " (SELECT ID FROM FAVORITOS_LINEAS_PEDIDO FV WHERE FV.LINEA IS NOT NULL AND"
sConsulta = sConsulta & " FV.LINEA  IN (SELECT ID FROM CATALOG_LIN CL WHERE CL.PUB=0))"
ExecuteSQL gRDOCon, sConsulta
sConsulta = "DELETE FROM FAVORITOS_LINEAS_PEDIDO WHERE FAVORITOS_LINEAS_PEDIDO.LINEA IS NOT NULL AND"
sConsulta = sConsulta & " FAVORITOS_LINEAS_PEDIDO.LINEA  IN (SELECT ID FROM CATALOG_LIN CL WHERE CL.PUB=0)"
ExecuteSQL gRDOCon, sConsulta
'Actualizo las que tengan una UP que no est� en la linea de cat�logo a la UP por defecto, FC por defecto
'precio del cat�logo y cantidad a cero
sConsulta = " UPDATE FAVORITOS_LINEAS_PEDIDO SET UP=CL.UP_DEF,FC=CL.FC_DEF,PREC_UP=CL.PRECIO_UC*CL.FC_DEF,CANT_PED=0"
sConsulta = sConsulta & " FROM FAVORITOS_LINEAS_PEDIDO FV "
sConsulta = sConsulta & " INNER JOIN CATALOG_LIN CL ON CL.ID=FV.LINEA WHERE FV.LINEA IS NOT NULL AND CL.UP_DEF<> FV.UP"
sConsulta = sConsulta & " AND FV.UP NOT IN (SELECT UP FROM CAT_LIN_MIN WHERE LINEA=FV.LINEA)"
ExecuteSQL gRDOCon, sConsulta
'Actualizo las que tengan una UP que SI est� en la linea de cat�logo pero que no es la UP por defecto
'y que el FC es distinto o el precio es distinto
sConsulta = " UPDATE FAVORITOS_LINEAS_PEDIDO SET FC=CLM.FC,PREC_UP=CL.PRECIO_UC*CLM.FC,CANT_PED=0"
sConsulta = sConsulta & " FROM FAVORITOS_LINEAS_PEDIDO FV "
sConsulta = sConsulta & " INNER JOIN CATALOG_LIN CL ON CL.ID=FV.LINEA INNER JOIN CAT_LIN_MIN CLM ON CLM.LINEA=FV.LINEA AND CLM.UP=FV.UP"
sConsulta = sConsulta & " WHERE FV.LINEA IS NOT NULL AND CL.UP_DEF<> FV.UP  AND (FV.FC<>CLM.FC OR FV.PREC_UP<>CL.PRECIO_UC*CLM.FC)"
ExecuteSQL gRDOCon, sConsulta

'Actualizo las que tengan la UP_DEF
'y que el FC es distinto o el precio es distinto
sConsulta = " UPDATE FAVORITOS_LINEAS_PEDIDO SET FC=CL.FC_DEF,PREC_UP=CL.PRECIO_UC*CL.FC_DEF,CANT_PED=0"
sConsulta = sConsulta & " FROM FAVORITOS_LINEAS_PEDIDO FV "
sConsulta = sConsulta & " INNER JOIN CATALOG_LIN CL ON CL.ID=FV.LINEA"
sConsulta = sConsulta & " WHERE FV.LINEA IS NOT NULL AND CL.UP_DEF= FV.UP  AND (FV.FC<>CL.FC_DEF OR FV.PREC_UP<>CL.PRECIO_UC*CL.FC_DEF)"
ExecuteSQL gRDOCon, sConsulta


sConsulta = "UPDATE VERSION SET NUM ='2.12.05.04'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
ExecuteSQL gRDOCon, sConsulta


sConsulta = "COMMIT TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = False

CodigoDeActualizacion2_12_05_03A2_12_05_04 = True
Exit Function
    
Error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_12_05_03A2_12_05_04 = False

End Function


Private Sub V_2_12_5_Tablas_2()
Dim sConsulta As String

sConsulta = "ALTER TABLE dbo.PARGEN_GEST ADD" & vbCrLf
sConsulta = sConsulta & "   PUBLICARFINSUM TINYINT NOT NULL  CONSTRAINT DF_PARGEN_GEST_PUBLICARFINSUM DEFAULT (1)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE dbo.PARINS_PROCE ADD" & vbCrLf
sConsulta = sConsulta & "   PUBLICARFINSUM TINYINT NOT NULL  CONSTRAINT DF_PARINS_PROCE_PUBLICARFINSUM DEFAULT (1)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


End Sub

Private Sub V_2_12_5_Triggers_2()
Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CONF_VISTAS_PROCE_ATRIB_TG_DEL]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[CONF_VISTAS_PROCE_ATRIB_TG_DEL]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TRIGGER CONF_VISTAS_PROCE_ATRIB_TG_DEL ON [dbo].[CONF_VISTAS_PROCE_ATRIB] " & vbCrLf
sConsulta = sConsulta & "FOR DELETE " & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO AS SMALLINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCE AS SMALLINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN1 AS VARCHAR(20)" & vbCrLf
sConsulta = sConsulta & "DECLARE @POSICION AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @VISTA AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @USU AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_PLANTILLA_Upd CURSOR LOCAL FOR SELECT ANYO,GMN1,PROCE,VISTA,POS,USU FROM DELETED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_PLANTILLA_Upd" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PLANTILLA_Upd INTO @ANYO,@GMN1,@PROCE,@VISTA,@POSICION,@USU" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_PROCE_ATRIB SET POS=POS-1 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND VISTA=@VISTA AND USU=@USU AND POS>@POSICION" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_PROCE SET CONSUMIDO_POS=CONSUMIDO_POS-1 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND VISTA=@VISTA AND USU=@USU AND CONSUMIDO_POS>@POSICION" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_PROCE SET ADJ_POS=ADJ_POS-1 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND VISTA=@VISTA AND USU=@USU AND ADJ_POS>@POSICION" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_PROCE SET AHORR_ADJ_POS=AHORR_ADJ_POS-1 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND VISTA=@VISTA AND USU=@USU AND AHORR_ADJ_POS>@POSICION" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_PROCE SET AHORR_ADJ_PORCEN_POS=AHORR_ADJ_PORCEN_POS-1 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND VISTA=@VISTA AND USU=@USU AND AHORR_ADJ_PORCEN_POS>@POSICION" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_PROCE SET IMP_POS=IMP_POS-1 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND VISTA=@VISTA AND USU=@USU AND IMP_POS>@POSICION" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_PROCE SET AHORR_OFE_POS=AHORR_OFE_POS-1 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND VISTA=@VISTA AND USU=@USU AND AHORR_OFE_POS>@POSICION" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_PROCE SET AHORR_OFE_PORCEN_POS=AHORR_OFE_PORCEN_POS-1 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND VISTA=@VISTA AND USU=@USU AND AHORR_OFE_PORCEN_POS>@POSICION" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PLANTILLA_Upd INTO @ANYO,@GMN1,@PROCE,@VISTA,@POSICION,@USU" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_PLANTILLA_Upd" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_PLANTILLA_Upd" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CONF_VISTAS_GRUPO_ATRIB_TG_DEL]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[CONF_VISTAS_GRUPO_ATRIB_TG_DEL]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TRIGGER CONF_VISTAS_GRUPO_ATRIB_TG_DEL ON [dbo].[CONF_VISTAS_GRUPO_ATRIB] " & vbCrLf
sConsulta = sConsulta & "FOR DELETE " & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO AS SMALLINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN1 AS VARCHAR(20)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCE AS SMALLINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @POSICION AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @GRUPO AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @VISTA AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @USU AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_PLANTILLA_Upd CURSOR LOCAL FOR SELECT ANYO,GMN1,PROCE,GRUPO,VISTA,POSICION,USU FROM DELETED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_PLANTILLA_Upd" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PLANTILLA_Upd INTO @ANYO,@GMN1,@PROCE,@GRUPO,@VISTA,@POSICION,@USU" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_GRUPO_ATRIB SET POSICION=POSICION-1 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE  AND GRUPO=@GRUPO AND VISTA=@VISTA AND USU=@USU AND POSICION>@POSICION" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_GRUPO SET PRECIO_PROV_POS=PRECIO_PROV_POS-1 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO AND VISTA=@VISTA AND USU=@USU AND PRECIO_PROV_POS>@POSICION" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_GRUPO SET CANT_PROV_POS=CANT_PROV_POS-1 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO AND VISTA=@VISTA AND USU=@USU AND CANT_PROV_POS>@POSICION" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_GRUPO SET ADJ_PROV_POS=ADJ_PROV_POS-1 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO AND VISTA=@VISTA AND USU=@USU AND ADJ_PROV_POS>@POSICION" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_GRUPO SET IMP_ADJ_POS=IMP_ADJ_POS-1 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO AND VISTA=@VISTA AND USU=@USU AND IMP_ADJ_POS>@POSICION" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PLANTILLA_Upd INTO  @ANYO,@GMN1,@PROCE,@GRUPO,@VISTA,@POSICION,@USU" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_PLANTILLA_Upd" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_PLANTILLA_Upd" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CONF_VISTAS_ALL_ATRIB_TG_DEL]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[CONF_VISTAS_ALL_ATRIB_TG_DEL]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TRIGGER CONF_VISTAS_ALL_ATRIB_TG_DEL ON [dbo].[CONF_VISTAS_ALL_ATRIB] " & vbCrLf
sConsulta = sConsulta & "FOR DELETE " & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO AS SMALLINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN1 AS VARCHAR(20)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCE AS SMALLINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @POSICION AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @VISTA AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @USU AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_PLANTILLA_Upd CURSOR LOCAL FOR SELECT ANYO,GMN1,PROCE,VISTA,POSICION,USU FROM DELETED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_PLANTILLA_Upd" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PLANTILLA_Upd INTO @ANYO,@GMN1,@PROCE,@VISTA,@POSICION,@USU" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ALL_ATRIB SET POSICION=POSICION-1 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE  AND VISTA=@VISTA AND USU=@USU AND POSICION>@POSICION" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ALL SET PRECIO_PROV_POS=PRECIO_PROV_POS-1 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND VISTA=@VISTA AND USU=@USU AND PRECIO_PROV_POS>@POSICION" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ALL SET CANT_PROV_POS=CANT_PROV_POS-1 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND VISTA=@VISTA AND USU=@USU AND CANT_PROV_POS>@POSICION" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ALL SET ADJ_PROV_POS=ADJ_PROV_POS-1 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND VISTA=@VISTA AND USU=@USU AND ADJ_PROV_POS>@POSICION" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ALL SET IMP_ADJ_POS=IMP_ADJ_POS-1 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND VISTA=@VISTA AND USU=@USU AND IMP_ADJ_POS>@POSICION" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PLANTILLA_Upd INTO  @ANYO,@GMN1,@PROCE,@VISTA,@POSICION,@USU" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_PLANTILLA_Upd" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_PLANTILLA_Upd" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USU_TG_NUEVOUSU]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[USU_TG_NUEVOUSU]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE  TRIGGER USU_TG_NUEVOUSU ON dbo.USU" & vbCrLf
sConsulta = sConsulta & "FOR INSERT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @COD VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PER VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @TIPO SMALLINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @COUNT INTEGER" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_Aprob_Lim_Del CURSOR LOCAL FOR SELECT COD,PER FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_Aprob_Lim_Del" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_Aprob_Lim_Del INTO @COD,@PER" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "    IF (SELECT COUNT(*) AS APROB FROM APROB_LIM WHERE PER=@PER)>0 " & vbCrLf
sConsulta = sConsulta & "        IF (SELECT COUNT(*) AS APROB FROM APROB_LIM A1 INNER JOIN APROB_LIM A2 ON A1.PADRE=A2.ID WHERE A2.PER =@PER )=0 -- No es aprobador tipo2" & vbCrLf
sConsulta = sConsulta & "            IF (SELECT COUNT(*) FROM SEGURIDAD WHERE APROB_TIPO1 =@PER)=0 -- No es aprobador tipo1" & vbCrLf
sConsulta = sConsulta & "     SET @TIPO=1" & vbCrLf
sConsulta = sConsulta & "            ELSE --Es aprobador tipo1" & vbCrLf
sConsulta = sConsulta & "     SET @TIPO=3" & vbCrLf
sConsulta = sConsulta & "        ELSE" & vbCrLf
sConsulta = sConsulta & "   SET @TIPO=3" & vbCrLf
sConsulta = sConsulta & "    ELSE" & vbCrLf
sConsulta = sConsulta & "           IF (SELECT COUNT(*) FROM SEGURIDAD WHERE APROB_TIPO1 =@PER)>0 -- No es aprobador tipo1" & vbCrLf
sConsulta = sConsulta & "     SET @TIPO=2" & vbCrLf
sConsulta = sConsulta & "    UPDATE USU SET FSEPTIPO=@TIPO WHERE COD=@COD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_Aprob_Lim_Del INTO @COD,@PER" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_Aprob_Lim_Del" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_Aprob_Lim_Del" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PROCE_PROVE_PET_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PROCE_PROVE_PET_TG_INS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TRIGGER PROCE_PROVE_PET_TG_INS ON dbo.PROCE_PROVE_PET" & vbCrLf
sConsulta = sConsulta & "FOR INSERT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN1 AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCE AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @EST AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @TIPOPET AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @FECHAREU AS DATETIME" & vbCrLf
sConsulta = sConsulta & "DECLARE @FECENVPET AS DATETIME" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_PROCE_PROVE_PET_Ins CURSOR FOR SELECT ANYO,GMN1,PROCE,TIPOPET,FECHAREU FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_PROCE_PROVE_PET_Ins" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_PROVE_PET_Ins INTO @ANYO,@GMN1,@PROCE,@TIPOPET,@FECHAREU" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @FECENVPET=NULL" & vbCrLf
sConsulta = sConsulta & "SELECT @EST=PROCE.EST,@FECENVPET=PROCE.FECENVPET FROM PROCE WHERE ANYO=@ANYO  AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "IF @FECENVPET IS NULL UPDATE PROCE SET FECENVPET=GETDATE()  WHERE ANYO=@ANYO  AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "IF @TIPOPET = 0 /*Peticiones de oferta*/" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @EST < 6  /* 3= Validado */" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET EST =6  WHERE ANYO= @ANYO  AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Else" & vbCrLf
sConsulta = sConsulta & "IF @TIPOPET = 1 /*Comunicando objetivos*/" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @EST = 8  /*Con objetivos sin notificar*/" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET EST=7 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "IF @EST = 9  /*Con objetivos sin notificar y preadjudicado*/" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET EST=10 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Else" & vbCrLf
sConsulta = sConsulta & "IF @TIPOPET = 2 OR @TIPOPET=3  /*Notificaci�n decierre*/" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "/*IF @EST=11" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET EST=12 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE*/" & vbCrLf
sConsulta = sConsulta & "IF @EST=12" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET EST=13 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_PROVE_PET_Ins INTO @ANYO,@GMN1,@PROCE,@TIPOPET,@FECHAREU" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_PROCE_PROVE_PET_Ins" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_PROCE_PROVE_PET_Ins" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub


Private Sub V_2_12_5_Storeds_2()
Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_INSERTAR_ADJUNTO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_INSERTAR_ADJUNTO]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_INSERTAR_ADJUNTO (@NOM VARCHAR(300),@COM VARCHAR(500),@FECACT DATETIME, " & vbCrLf
sConsulta = sConsulta & "                                @COD_PROVE_CIA VARCHAR(200), @ESPEC INT) AS" & vbCrLf
sConsulta = sConsulta & "/**************************************************************************************************/" & vbCrLf
sConsulta = sConsulta & "/******       Descripci�n : Inserta un archivo adjunto insertado desde el PS.                ******/" & vbCrLf
sConsulta = sConsulta & "/******                                                                 ******/" & vbCrLf
sConsulta = sConsulta & "/******       Par�metros :   @NOM >> El nombre del fichero adjunto                           ******/" & vbCrLf
sConsulta = sConsulta & "/******                              @COM >> Comentarios sobre el fichero adjunto            ******/" & vbCrLf
sConsulta = sConsulta & "/******                              @FECACT >> Fecha de actualizaci�n                       ******/" & vbCrLf
sConsulta = sConsulta & "/******                              @COD_PROVE_CIA >> Codigo del proveedor en el GS         ******/" & vbCrLf
sConsulta = sConsulta & "/******                              @ESPEC >> El identificador del archivo                  ******/" & vbCrLf
sConsulta = sConsulta & "/******                                                                                         ******/" & vbCrLf
sConsulta = sConsulta & "/**************************************************************************************************/" & vbCrLf
sConsulta = sConsulta & "IF @NOM IS NOT NULL  --Para que no falle al hacer insert  " & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   IF (SELECT COUNT(*) FROM PROVE_ESP_PORTAL WHERE PROVE=@COD_PROVE_CIA AND ID=@ESPEC)=0" & vbCrLf
sConsulta = sConsulta & "      INSERT INTO PROVE_ESP_PORTAL (PROVE,ID,NOM,COM,FECHA) VALUES (@COD_PROVE_CIA,@ESPEC,@NOM, @COM, @FECACT)" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_INSERTAR_CONTACTO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_INSERTAR_CONTACTO]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_INSERTAR_CONTACTO (@USU_APE VARCHAR(100),@USU_NOM VARCHAR(20), @USU_DEP VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                             @USU_CAR VARCHAR(100), @USU_TFNO VARCHAR(100), @USU_FAX VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                             @USU_EMAIL VARCHAR(100), @USU_TFNO2 VARCHAR(100)," & vbCrLf
sConsulta = sConsulta & "                             @USU_TFNO_MOVIL VARCHAR(100),@COD_PROVE_CIA VARCHAR(200),@ID_P INT,@IDI VARCHAR(100),@TIPOEMAIL SMALLINT)AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @INDICE INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @INT TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ACTIVA BIT" & vbCrLf
sConsulta = sConsulta & "DECLARE @SENTIDO TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_LOG_CON INT" & vbCrLf
sConsulta = sConsulta & "IF (SELECT COUNT(*) FROM CON WHERE PROVE=@COD_PROVE_CIA AND ID_PORT=@ID_P)=0 --POR SI ANTES HAB�AN PULSADO DATOS PORT" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @INDICE = (SELECT MAX(ID) FROM CON WHERE PROVE = @COD_PROVE_CIA)" & vbCrLf
sConsulta = sConsulta & "IF @INDICE IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & " SET @INDICE = 1" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Else" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & " SET @INDICE = @INDICE + 1" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "INSERT INTO CON (PROVE,ID,APE,NOM,DEP,CAR,TFNO,FAX,EMAIL,RECPET,TFNO2,PORT,TFNO_MOVIL,ID_PORT,IDI,TIPOEMAIL)" & vbCrLf
sConsulta = sConsulta & "             VALUES(@COD_PROVE_CIA,@INDICE,@USU_APE,@USU_NOM,@USU_DEP,@USU_CAR,@USU_TFNO," & vbCrLf
sConsulta = sConsulta & "                      @USU_FAX,@USU_EMAIL,1,@USU_TFNO2,1,@USU_TFNO_MOVIL,@ID_P,@IDI,@TIPOEMAIL)" & vbCrLf
sConsulta = sConsulta & "/*************************************************************************************************************************************************************/" & vbCrLf
sConsulta = sConsulta & "/******       INTEGRACION :  Si est� activada a nivel gral  y en concreto la entidad PROVE en sentido salida o entrada/salida       ******/" & vbCrLf
sConsulta = sConsulta & "/******                              grabaremos en las tablas LOG_CON y LOG_PROVE (origen = Integraci�n y el usuario a blancos            ******/" & vbCrLf
sConsulta = sConsulta & "/************************************************************************************************************************************************************/" & vbCrLf
sConsulta = sConsulta & "SET @INT = (SELECT INTEGRACION FROM PARGEN_INTERNO WHERE ID=1)" & vbCrLf
sConsulta = sConsulta & "IF @INT = 1 " & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & " SELECT @ACTIVA=ACTIVA,@SENTIDO=SENTIDO FROM TABLAS_INTEGRACION WHERE ID=8" & vbCrLf
sConsulta = sConsulta & " IF @ACTIVA = 1 AND (@SENTIDO = 1 OR @SENTIDO=3)" & vbCrLf
sConsulta = sConsulta & "       INSERT INTO LOG_CON (ACCION,PROVE,ID_CON,APE,NOM,DEP,CAR,TFNO,FAX,EMAIL,RECPET,TFNO2,TFNO_MOVIL,ORIGEN,USU)" & vbCrLf
sConsulta = sConsulta & "             VALUES('I',@COD_PROVE_CIA,@INDICE,@USU_APE,@USU_NOM,@USU_DEP,@USU_CAR,@USU_TFNO," & vbCrLf
sConsulta = sConsulta & "                      @USU_FAX,@USU_EMAIL,1,@USU_TFNO2,@USU_TFNO_MOVIL,0,'')" & vbCrLf
sConsulta = sConsulta & "       SET @ID_LOG_CON = (SELECT MAX(ID) FROM LOG_CON)" & vbCrLf
sConsulta = sConsulta & "       INSERT INTO LOG_PROVE (ACCION,ID_LOG_CON,COD,DEN,DIR,CP,POB,PAI,PROVI,MON,NIF,URLPROVE,ORIGEN,USU) SELECT 'R',@ID_LOG_CON,COD,DEN,DIR,CP,POB,PAI,PROVI,MON,NIF,URLPROVE,0,''  FROM PROVE WHERE COD =@COD_PROVE_CIA" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_AR_JOB_LEER_DATOS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_AR_JOB_LEER_DATOS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE dbo.SP_AR_JOB_LEER_DATOS (@BD VARCHAR(50))AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @BD='ACT_AHORROS_' + @BD" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   EXEC msdb.dbo.sp_help_job NULL, @BD" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ATR_MODIFICAR]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[ATR_MODIFICAR]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE ATR_MODIFICAR  @TABLA SMALLINT,@ID INTEGER,@DEN VARCHAR(100)=NULL,@TIPO SMALLINT=NULL, @PREFBAJO TINYINT , @INTRO TINYINT, @MINNUM FLOAT=NULL,@MINFEC DATETIME=NULL,@MAXNUM FLOAT=NULL,@MAXFEC DATETIME=NULL,@ATRIB INTEGER=NULL,@INTERNO  TINYINT=NULL,@AMBITO TINYINT=NULL,@OBLIG  TINYINT=NULL,@OP_PREC VARCHAR(2)=NULL,@APLIC_PREC VARCHAR(50)=NULL,@DEF_PREC TINYINT=NULL,@ANYO VARCHAR(50)=NULL,@GMN1 VARCHAR(50)=NULL,@PROCE VARCHAR(50)=NULL,@GRUPO VARCHAR(50)=NULL , @FECACT DATETIME OUTPUT,@ORDEN SMALLINT=NULL OUTPUT,@PLANT VARCHAR(50)=NULL AS " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @POND AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @AMBITOOLD AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @COUNT AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @APLI_PREC_V AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @ORDEN_V AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @ATRIB_PREC AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @USAR_PREC AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRIMERO AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @TIPOOLD AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @VISTAS AS TINYINT" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @TABLA=0  " & vbCrLf
sConsulta = sConsulta & "     BEGIN --@tabla=0" & vbCrLf
sConsulta = sConsulta & "            SELECT   @POND=POND,@TIPOOLD=TIPO  FROM DEF_ATRIB  WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "            IF @POND = 1 AND @TIPO<> 2 and @TIPO<>3  " & vbCrLf
sConsulta = sConsulta & "                         SET @POND = 0" & vbCrLf
sConsulta = sConsulta & "            ELSE " & vbCrLf
sConsulta = sConsulta & "                          IF @POND = 2 AND (@TIPO=4 OR @INTRO=0)" & vbCrLf
sConsulta = sConsulta & "                                       SET @POND= 0" & vbCrLf
sConsulta = sConsulta & "                          ELSE" & vbCrLf
sConsulta = sConsulta & "                                        IF  @POND = 3 AND @TIPO<> 2 " & vbCrLf
sConsulta = sConsulta & "                                                   SET @POND= 0" & vbCrLf
sConsulta = sConsulta & "                                        ELSE" & vbCrLf
sConsulta = sConsulta & "                                                    IF @POND =5 AND @TIPO<>4" & vbCrLf
sConsulta = sConsulta & "                                                            SET @POND= 0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "            IF @TIPOOLD <> @TIPO " & vbCrLf
sConsulta = sConsulta & "                  IF @TIPOOLD=2" & vbCrLf
sConsulta = sConsulta & "                        BEGIN" & vbCrLf
sConsulta = sConsulta & "                        UPDATE PROCE_ATRIB SET OP_PREC=NULL, APLIC_PREC=NULL, DEF_PREC=NULL WHERE ATRIB= @ID" & vbCrLf
sConsulta = sConsulta & "                        UPDATE PLANTILLA_ATRIB SET OP_PREC=NULL, APLIC_PREC=NULL, DEF_PREC=NULL WHERE ATRIB= @ID" & vbCrLf
sConsulta = sConsulta & "                        END " & vbCrLf
sConsulta = sConsulta & "            UPDATE DEF_ATRIB SET DEN=@DEN,TIPO=@TIPO,PREFBAJO=@PREFBAJO,POND=@POND,INTRO=@INTRO,MAXNUM=CAST(@MAXNUM AS VARCHAR(15)) ,MINNUM=CAST(@MINNUM AS VARCHAR(15)),MAXFEC=CONVERT(VARCHAR(10),@MAXFEC,111),MINFEC=CONVERT(VARCHAR(10),@MINFEC,111) WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "            SELECT @FECACT=FECACT FROM DEF_ATRIB WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "      END --@tabla=0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @TABLA=1" & vbCrLf
sConsulta = sConsulta & "    BEGIN -- @tabla=1" & vbCrLf
sConsulta = sConsulta & "       IF @APLIC_PREC IS not  NULL  AND @ORDEN is  NULL " & vbCrLf
sConsulta = sConsulta & "                 BEGIN --@orden=null" & vbCrLf
sConsulta = sConsulta & "                 SELECT @ATRIB_PREC =ATRIB  FROM PROCE_ATRIB WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "                 SELECT @ORDEN=ORDEN FROM PROCE_ATRIB WHERE  ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND APLIC_PREC = @APLIC_PREC AND" & vbCrLf
sConsulta = sConsulta & "                    OP_PREC=@OP_PREC AND ATRIB =@ATRIB_PREC" & vbCrLf
sConsulta = sConsulta & "                      " & vbCrLf
sConsulta = sConsulta & "                 SET @APLI_PREC_V=@APLIC_PREC" & vbCrLf
sConsulta = sConsulta & "                 SET @ORDEN_V= 0" & vbCrLf
sConsulta = sConsulta & "                 SET @PRIMERO=0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                 WHILE @ORDEN_V=0 and @APLI_PREC_V<=3" & vbCrLf
sConsulta = sConsulta & "                         BEGIN -- while" & vbCrLf
sConsulta = sConsulta & "                             SELECT  @ORDEN_V = MAX(ORDEN) FROM PROCE_ATRIB WHERE  ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND APLIC_PREC = @APLI_PREC_V" & vbCrLf
sConsulta = sConsulta & "                             IF @ORDEN_V IS NULL" & vbCrLf
sConsulta = sConsulta & "                               begin --@ORDEN_V =null" & vbCrLf
sConsulta = sConsulta & "                                 IF @APLIC_PREC=3" & vbCrLf
sConsulta = sConsulta & "                                     begin" & vbCrLf
sConsulta = sConsulta & "                                       SET @ORDEN_V=1 " & vbCrLf
sConsulta = sConsulta & "                                       SET @PRIMERO=1" & vbCrLf
sConsulta = sConsulta & "                                     end" & vbCrLf
sConsulta = sConsulta & "                                 ELSE                   " & vbCrLf
sConsulta = sConsulta & "                                   begin" & vbCrLf
sConsulta = sConsulta & "                                      SET @ORDEN_V= 0" & vbCrLf
sConsulta = sConsulta & "                                      SET @APLI_PREC_V=@APLI_PREC_V + 1" & vbCrLf
sConsulta = sConsulta & "                                   end" & vbCrLf
sConsulta = sConsulta & "                               end  --@ORDEN_V =null" & vbCrLf
sConsulta = sConsulta & "                         END -- while" & vbCrLf
sConsulta = sConsulta & "                if @ORDEN_V IS NULL  SET @ORDEN_V=0" & vbCrLf
sConsulta = sConsulta & "                      if @PRIMERO=1" & vbCrLf
sConsulta = sConsulta & "                              UPDATE PROCE_ATRIB SET ORDEN=1 WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "                     else" & vbCrLf
sConsulta = sConsulta & "                              UPDATE PROCE_ATRIB SET ORDEN=@ORDEN_V+1 WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "                UPDATE PROCE_ATRIB SET ORDEN = ORDEN +1 WHERE  APLIC_PREC < @APLIC_PREC AND ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ID<>@ID" & vbCrLf
sConsulta = sConsulta & "               SET @ORDEN=@ORDEN_V +1" & vbCrLf
sConsulta = sConsulta & "              " & vbCrLf
sConsulta = sConsulta & "             END --@orden=null" & vbCrLf
sConsulta = sConsulta & "       SELECT @POND=POND,@AMBITOOLD=AMBITO,@USAR_PREC=USAR_PREC FROM PROCE_ATRIB WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "       IF @INTRO=0 AND @POND=2 SET @POND=0" & vbCrLf
sConsulta = sConsulta & "       IF @DEF_PREC=1 SET @USAR_PREC=1" & vbCrLf
sConsulta = sConsulta & "       UPDATE PROCE_ATRIB SET PREFBAJO=@PREFBAJO,INTRO=@INTRO,MAXNUM=@MAXNUM  ," & vbCrLf
sConsulta = sConsulta & "                  MINNUM=@MINNUM,MAXFEC=CONVERT(VARCHAR(10),@MAXFEC,111)," & vbCrLf
sConsulta = sConsulta & "                  MINFEC=CONVERT(VARCHAR(10),@MINFEC,111),INTERNO=@INTERNO,AMBITO=@AMBITO,OBLIG=@OBLIG," & vbCrLf
sConsulta = sConsulta & "                  OP_PREC=@OP_PREC,APLIC_PREC=@APLIC_PREC,USAR_PREC=@USAR_PREC," & vbCrLf
sConsulta = sConsulta & "                  DEF_PREC=@DEF_PREC,POND=@POND WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "       SELECT @FECACT=FECACT FROM PROCE_ATRIB WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "       IF   @ATRIB IS NOT  NULL" & vbCrLf
sConsulta = sConsulta & "             BEGIN --@atrib not null" & vbCrLf
sConsulta = sConsulta & "                    SELECT @POND=POND FROM DEF_ATRIB WHERE ID=@ATRIB" & vbCrLf
sConsulta = sConsulta & "                    IF @INTRO=0 AND @POND=2 SET @POND=0" & vbCrLf
sConsulta = sConsulta & "                       UPDATE DEF_ATRIB SET PREFBAJO=@PREFBAJO,INTRO=@INTRO,MAXNUM=@MAXNUM ," & vbCrLf
sConsulta = sConsulta & "                           MINNUM=@MINNUM,MAXFEC=CONVERT(VARCHAR(10),@MAXFEC,111)," & vbCrLf
sConsulta = sConsulta & "                           MINFEC=CONVERT(VARCHAR(10),@MINFEC,111),POND=@POND WHERE ID=@ATRIB" & vbCrLf
sConsulta = sConsulta & "             END--@atrib not null" & vbCrLf
sConsulta = sConsulta & "--           END estaba mal" & vbCrLf
sConsulta = sConsulta & "       IF @AMBITO<>@AMBITOOLD" & vbCrLf
sConsulta = sConsulta & "               BEGIN --@ambito<>ambitoold" & vbCrLf
sConsulta = sConsulta & "                     SELECT @COUNT=COUNT(*) FROM PROCE_OFE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "                     IF @COUNT > 0" & vbCrLf
sConsulta = sConsulta & "                          IF  @AMBITOOLD>@AMBITO" & vbCrLf
sConsulta = sConsulta & "                               IF @AMBITOOLD=3" & vbCrLf
sConsulta = sConsulta & "                                     begin --old=3" & vbCrLf
sConsulta = sConsulta & "                                           DELETE OFE_ITEM_ATRIB WHERE  ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ATRIB_ID=@ID" & vbCrLf
sConsulta = sConsulta & "                                           DELETE CONF_VISTAS_GRUPO_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ATRIB=@ID" & vbCrLf
sConsulta = sConsulta & "                                           DELETE CONF_VISTAS_ALL_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE  AND ATRIB=@ID" & vbCrLf
sConsulta = sConsulta & "                                           DELETE CONF_VISTAS_ITEM_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE  AND ATRIB=@ID" & vbCrLf
sConsulta = sConsulta & "                                           INSERT INTO CONF_VISTAS_PROCE_ATRIB (ANYO,GMN1,PROCE,USU,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                              SELECT @ANYO,@GMN1,@PROCE,USU,VISTA,@ID FROM  CONF_VISTAS_PROCE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "                                     end  --old=3" & vbCrLf
sConsulta = sConsulta & "                               ELSE --ser�a old=2" & vbCrLf
sConsulta = sConsulta & "                                           DELETE OFE_GR_ATRIB WHERE  ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ATRIB_ID=@ID AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                          ELSE  --old < ambito" & vbCrLf
sConsulta = sConsulta & "                                 IF @AMBITOOLD=1" & vbCrLf
sConsulta = sConsulta & "                                     BEGIN --old=1" & vbCrLf
sConsulta = sConsulta & "                                        IF @AMBITO=2" & vbCrLf
sConsulta = sConsulta & "                                                INSERT INTO OFE_GR_ATRIB (ANYO, GMN1, PROCE,GRUPO,PROVE,OFE,ATRIB_ID,VALOR_NUM,VALOR_TEXT," & vbCrLf
sConsulta = sConsulta & "                                                      VALOR_FEC,VALOR_BOOL,VALOR_POND) " & vbCrLf
sConsulta = sConsulta & "                                                      SELECT DISTINCT OFE_ATRIB.ANYO, OFE_ATRIB.GMN1, OFE_ATRIB.PROCE,PROCE_ATRIB.GRUPO" & vbCrLf
sConsulta = sConsulta & "                                                                   ,OFE_ATRIB.PROVE,OFE_ATRIB.OFE,OFE_ATRIB.ATRIB_ID,OFE_ATRIB.VALOR_NUM,OFE_ATRIB.VALOR_TEXT" & vbCrLf
sConsulta = sConsulta & "                                                                   ,OFE_ATRIB.VALOR_FEC,OFE_ATRIB.VALOR_BOOL,OFE_ATRIB.VALOR_POND " & vbCrLf
sConsulta = sConsulta & "                                                     FROM OFE_ATRIB " & vbCrLf
sConsulta = sConsulta & "                                                     INNER JOIN PROCE_ATRIB ON  OFE_ATRIB.ANYO=PROCE_ATRIB.ANYO" & vbCrLf
sConsulta = sConsulta & "                                                                 AND OFE_ATRIB.GMN1=PROCE_ATRIB.GMN1 AND OFE_ATRIB.PROCE =PROCE_ATRIB.PROCE " & vbCrLf
sConsulta = sConsulta & "                                                                 AND PROCE_ATRIB.GRUPO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                                                      WHERE OFE_ATRIB.ATRIB_ID=@ID AND OFE_ATRIB.ANYO=@ANYO AND OFE_ATRIB.GMN1=@GMN1 AND OFE_ATRIB.PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "                                        DELETE OFE_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ATRIB_ID=@ID" & vbCrLf
sConsulta = sConsulta & "                                        IF @AMBITO = 3" & vbCrLf
sConsulta = sConsulta & "                                                  begin --ambito=3" & vbCrLf
sConsulta = sConsulta & "                                                  DELETE CONF_VISTAS_PROCE_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ATRIB=@ID" & vbCrLf
sConsulta = sConsulta & "                                                  IF @GRUPO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                                                        INSERT INTO CONF_VISTAS_GRUPO_ATRIB (ANYO,GMN1,PROCE,GRUPO,USU,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                                          SELECT @ANYO,@GMN1,@PROCE,@GRUPO,USU,VISTA,@ID FROM  CONF_VISTAS_GRUPO WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO " & vbCrLf
sConsulta = sConsulta & "                                                  ELSE" & vbCrLf
sConsulta = sConsulta & "                                                        INSERT INTO CONF_VISTAS_GRUPO_ATRIB (ANYO,GMN1,PROCE,GRUPO,USU,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                                          SELECT @ANYO,@GMN1,@PROCE,GRUPO,USU,VISTA,@ID FROM  CONF_VISTAS_GRUPO WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE         " & vbCrLf
sConsulta = sConsulta & "                                                 INSERT INTO CONF_VISTAS_ALL_ATRIB (ANYO,GMN1,PROCE,USU,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                                     SELECT @ANYO,@GMN1,@PROCE,USU,VISTA,@ID FROM  CONF_VISTAS_ALL WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "                                                 IF @GRUPO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                                                     INSERT INTO CONF_VISTAS_ITEM_ATRIB (ANYO,GMN1,PROCE,GRUPO,USU,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                                         SELECT @ANYO,@GMN1,@PROCE,@GRUPO,USU,VISTA,@ID FROM  CONF_VISTAS_ITEM WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO " & vbCrLf
sConsulta = sConsulta & "                                                  ELSE" & vbCrLf
sConsulta = sConsulta & "                                                     INSERT INTO CONF_VISTAS_ITEM_ATRIB (ANYO,GMN1,PROCE,GRUPO,USU,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                                         SELECT @ANYO,@GMN1,@PROCE,GRUPO,USU,VISTA,@ID FROM  CONF_VISTAS_ITEM WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                                                 end--ambito=3" & vbCrLf
sConsulta = sConsulta & "                                     END --old=1" & vbCrLf
sConsulta = sConsulta & "                                ELSE " & vbCrLf
sConsulta = sConsulta & "                                    BEGIN  --old es 2" & vbCrLf
sConsulta = sConsulta & "                                      DELETE OFE_GR_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ATRIB_ID=@ID AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "                                      DELETE CONF_VISTAS_PROCE_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ATRIB=@ID" & vbCrLf
sConsulta = sConsulta & "                                      IF @GRUPO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                                                 INSERT INTO CONF_VISTAS_GRUPO_ATRIB (ANYO,GMN1,PROCE,GRUPO,USU,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                                       SELECT @ANYO,@GMN1,@PROCE,@GRUPO,USU,VISTA,@ID FROM  CONF_VISTAS_GRUPO WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO " & vbCrLf
sConsulta = sConsulta & "                                      ELSE" & vbCrLf
sConsulta = sConsulta & "                                                INSERT INTO CONF_VISTAS_GRUPO_ATRIB (ANYO,GMN1,PROCE,GRUPO,USU,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                                       SELECT @ANYO,@GMN1,@PROCE,GRUPO,USU,VISTA,@ID FROM  CONF_VISTAS_GRUPO WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "                                      INSERT INTO CONF_VISTAS_ALL_ATRIB (ANYO,GMN1,PROCE,USU,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                           SELECT @ANYO,@GMN1,@PROCE,USU,VISTA,@ID FROM  CONF_VISTAS_ALL WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "                                      IF @GRUPO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                                                 INSERT INTO CONF_VISTAS_ITEM_ATRIB (ANYO,GMN1,PROCE,GRUPO,USU,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                                      SELECT @ANYO,@GMN1,@PROCE,@GRUPO,USU,VISTA,@ID FROM  CONF_VISTAS_ITEM WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO " & vbCrLf
sConsulta = sConsulta & "                                      ELSE" & vbCrLf
sConsulta = sConsulta & "                                                 INSERT INTO CONF_VISTAS_ITEM_ATRIB (ANYO,GMN1,PROCE,GRUPO,USU,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                                      SELECT @ANYO,@GMN1,@PROCE,GRUPO,USU,VISTA,@ID FROM  CONF_VISTAS_ITEM WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                                    END--old es 2" & vbCrLf
sConsulta = sConsulta & "                  ELSE --@COUNT >0" & vbCrLf
sConsulta = sConsulta & "                     SELECT @VISTAS=PLANTILLA_VISTAS FROM PROCE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "                     IF @VISTAS > 0" & vbCrLf
sConsulta = sConsulta & "                          IF  @AMBITOOLD>@AMBITO" & vbCrLf
sConsulta = sConsulta & "                               IF @AMBITOOLD=3" & vbCrLf
sConsulta = sConsulta & "                                     begin --old=3" & vbCrLf
sConsulta = sConsulta & "                                           DELETE CONF_VISTAS_GRUPO_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ATRIB=@ID" & vbCrLf
sConsulta = sConsulta & "                                           DELETE CONF_VISTAS_ALL_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE  AND ATRIB=@ID" & vbCrLf
sConsulta = sConsulta & "                                           DELETE CONF_VISTAS_ITEM_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE  AND ATRIB=@ID" & vbCrLf
sConsulta = sConsulta & "                                           INSERT INTO CONF_VISTAS_PROCE_ATRIB (ANYO,GMN1,PROCE,USU,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                              SELECT @ANYO,@GMN1,@PROCE,USU,VISTA,@ID FROM  CONF_VISTAS_PROCE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "                                     end  --old=3" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                          ELSE  --old < ambito" & vbCrLf
sConsulta = sConsulta & "                                 IF @AMBITOOLD=1" & vbCrLf
sConsulta = sConsulta & "                                     BEGIN --old=1" & vbCrLf
sConsulta = sConsulta & "                                        IF @AMBITO = 3" & vbCrLf
sConsulta = sConsulta & "                                                  begin --ambito=3" & vbCrLf
sConsulta = sConsulta & "                                                  DELETE CONF_VISTAS_PROCE_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ATRIB=@ID" & vbCrLf
sConsulta = sConsulta & "                                                  IF @GRUPO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                                                        INSERT INTO CONF_VISTAS_GRUPO_ATRIB (ANYO,GMN1,PROCE,GRUPO,USU,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                                          SELECT @ANYO,@GMN1,@PROCE,@GRUPO,USU,VISTA,@ID FROM  CONF_VISTAS_GRUPO WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO " & vbCrLf
sConsulta = sConsulta & "                                                  ELSE" & vbCrLf
sConsulta = sConsulta & "                                                        INSERT INTO CONF_VISTAS_GRUPO_ATRIB (ANYO,GMN1,PROCE,GRUPO,USU,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                                          SELECT @ANYO,@GMN1,@PROCE,GRUPO,USU,VISTA,@ID FROM  CONF_VISTAS_GRUPO WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE         " & vbCrLf
sConsulta = sConsulta & "                                                 INSERT INTO CONF_VISTAS_ALL_ATRIB (ANYO,GMN1,PROCE,USU,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                                     SELECT @ANYO,@GMN1,@PROCE,USU,VISTA,@ID FROM  CONF_VISTAS_ALL WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "                                                 IF @GRUPO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                                                     INSERT INTO CONF_VISTAS_ITEM_ATRIB (ANYO,GMN1,PROCE,GRUPO,USU,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                                         SELECT @ANYO,@GMN1,@PROCE,@GRUPO,USU,VISTA,@ID FROM  CONF_VISTAS_ITEM WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO " & vbCrLf
sConsulta = sConsulta & "                                                  ELSE" & vbCrLf
sConsulta = sConsulta & "                                                     INSERT INTO CONF_VISTAS_ITEM_ATRIB (ANYO,GMN1,PROCE,GRUPO,USU,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                                         SELECT @ANYO,@GMN1,@PROCE,GRUPO,USU,VISTA,@ID FROM  CONF_VISTAS_ITEM WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                                                 end--ambito=3" & vbCrLf
sConsulta = sConsulta & "                                     END --old=1" & vbCrLf
sConsulta = sConsulta & "                                ELSE " & vbCrLf
sConsulta = sConsulta & "                                    BEGIN  --old es 2" & vbCrLf
sConsulta = sConsulta & "                                      DELETE CONF_VISTAS_PROCE_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ATRIB=@ID" & vbCrLf
sConsulta = sConsulta & "                                      IF @GRUPO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                                                 INSERT INTO CONF_VISTAS_GRUPO_ATRIB (ANYO,GMN1,PROCE,GRUPO,USU,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                                       SELECT @ANYO,@GMN1,@PROCE,@GRUPO,USU,VISTA,@ID FROM  CONF_VISTAS_GRUPO WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO " & vbCrLf
sConsulta = sConsulta & "                                      ELSE" & vbCrLf
sConsulta = sConsulta & "                                                INSERT INTO CONF_VISTAS_GRUPO_ATRIB (ANYO,GMN1,PROCE,GRUPO,USU,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                                       SELECT @ANYO,@GMN1,@PROCE,GRUPO,USU,VISTA,@ID FROM  CONF_VISTAS_GRUPO WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "                                      INSERT INTO CONF_VISTAS_ALL_ATRIB (ANYO,GMN1,PROCE,USU,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                           SELECT @ANYO,@GMN1,@PROCE,USU,VISTA,@ID FROM  CONF_VISTAS_ALL WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "                                      IF @GRUPO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                                                 INSERT INTO CONF_VISTAS_ITEM_ATRIB (ANYO,GMN1,PROCE,GRUPO,USU,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                                      SELECT @ANYO,@GMN1,@PROCE,@GRUPO,USU,VISTA,@ID FROM  CONF_VISTAS_ITEM WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO " & vbCrLf
sConsulta = sConsulta & "                                      ELSE" & vbCrLf
sConsulta = sConsulta & "                                                 INSERT INTO CONF_VISTAS_ITEM_ATRIB (ANYO,GMN1,PROCE,GRUPO,USU,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                                      SELECT @ANYO,@GMN1,@PROCE,GRUPO,USU,VISTA,@ID FROM  CONF_VISTAS_ITEM WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                                    END--old es 2      " & vbCrLf
sConsulta = sConsulta & "               END --@ambito<>ambitoold" & vbCrLf
sConsulta = sConsulta & "         END -- @tabla=1" & vbCrLf
sConsulta = sConsulta & "IF @TABLA=2" & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "          IF @APLIC_PREC IS not  NULL  AND @ORDEN is  NULL " & vbCrLf
sConsulta = sConsulta & "                 BEGIN" & vbCrLf
sConsulta = sConsulta & "                     SELECT @ATRIB_PREC =ATRIB  FROM PLANTILLA_ATRIB WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "                     SELECT @ORDEN=ORDEN FROM PLANTILLA_ATRIB WHERE PLANT=@PLANT AND APLIC_PREC = @APLIC_PREC AND OP_PREC=@OP_PREC AND ATRIB =@ATRIB_PREC" & vbCrLf
sConsulta = sConsulta & "                     IF @ORDEN IS NULL" & vbCrLf
sConsulta = sConsulta & "                         BEGIN" & vbCrLf
sConsulta = sConsulta & "                           SET @APLI_PREC_V=@APLIC_PREC" & vbCrLf
sConsulta = sConsulta & "                           SET @ORDEN_V= 0" & vbCrLf
sConsulta = sConsulta & "                           WHILE @APLI_PREC_V < 3  AND @ORDEN_V= 0" & vbCrLf
sConsulta = sConsulta & "                                 BEGIN" & vbCrLf
sConsulta = sConsulta & "                                     SELECT  @ORDEN_V = MAX(ORDEN) FROM PLANTILLA_ATRIB WHERE  PLANT=@PLANT AND APLIC_PREC = @APLI_PREC_V" & vbCrLf
sConsulta = sConsulta & "                                      IF @ORDEN_V IS NULL" & vbCrLf
sConsulta = sConsulta & "                                         begin" & vbCrLf
sConsulta = sConsulta & "                                              SET @ORDEN_V= 0" & vbCrLf
sConsulta = sConsulta & "                                              SET @APLI_PREC_V=@APLI_PREC_V + 1" & vbCrLf
sConsulta = sConsulta & "                                         end" & vbCrLf
sConsulta = sConsulta & "                                END" & vbCrLf
sConsulta = sConsulta & "                          UPDATE PLANTILLA_ATRIB SET ORDEN = ORDEN +1 WHERE  APLIC_PREC < @APLIC_PREC AND PLANT=@PLANT" & vbCrLf
sConsulta = sConsulta & "                          SET @ORDEN=@ORDEN_V +1" & vbCrLf
sConsulta = sConsulta & "                        END" & vbCrLf
sConsulta = sConsulta & "                   else" & vbCrLf
sConsulta = sConsulta & "                      IF @APLIC_PREC IS not  NULL   " & vbCrLf
sConsulta = sConsulta & "                            UPDATE PLANTILLA_ATRIB SET ORDEN = ORDEN +1 WHERE  APLIC_PREC < @APLIC_PREC AND PLANT=@PLANT" & vbCrLf
sConsulta = sConsulta & "                 END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "           SELECT @POND=POND, @AMBITOOLD=AMBITO FROM PLANTILLA_ATRIB WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "           IF @INTRO=0 AND @POND=2 SET @POND=0" & vbCrLf
sConsulta = sConsulta & "               UPDATE PLANTILLA_ATRIB SET PREFBAJO=@PREFBAJO,INTRO=@INTRO,MAXNUM=@MAXNUM  ," & vbCrLf
sConsulta = sConsulta & "                    MINNUM=@MINNUM,MAXFEC=CONVERT(VARCHAR(10),@MAXFEC,111)," & vbCrLf
sConsulta = sConsulta & "                    MINFEC=CONVERT(VARCHAR(10),@MINFEC,111),INTERNO=@INTERNO,AMBITO=@AMBITO,OBLIG=@OBLIG," & vbCrLf
sConsulta = sConsulta & "                    OP_PREC=@OP_PREC,APLIC_PREC=@APLIC_PREC," & vbCrLf
sConsulta = sConsulta & "                    DEF_PREC=@DEF_PREC,POND=@POND,ORDEN=@ORDEN WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "          SELECT @FECACT=FECACT FROM PLANTILLA_ATRIB WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "          IF   @ATRIB IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                BEGIN" & vbCrLf
sConsulta = sConsulta & "                    SELECT @POND=POND FROM DEF_ATRIB WHERE ID=@ATRIB" & vbCrLf
sConsulta = sConsulta & "                    IF @INTRO=0 AND @POND=2 SET @POND=0" & vbCrLf
sConsulta = sConsulta & "                        UPDATE DEF_ATRIB SET PREFBAJO=@PREFBAJO,INTRO=@INTRO,MAXNUM=CAST(@MAXNUM AS VARCHAR(15)) ," & vbCrLf
sConsulta = sConsulta & "                            MINNUM=CAST(@MINNUM AS VARCHAR(15)),MAXFEC=CONVERT(VARCHAR(10),@MAXFEC,111)," & vbCrLf
sConsulta = sConsulta & "                            MINFEC=CONVERT(VARCHAR(10),@MINFEC,111),POND=@POND WHERE ID=@ATRIB" & vbCrLf
sConsulta = sConsulta & "                END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "     IF @AMBITO<>@AMBITOOLD" & vbCrLf
sConsulta = sConsulta & "       BEGIN --@ambito<>ambitoold           " & vbCrLf
sConsulta = sConsulta & "            IF  @AMBITOOLD>@AMBITO" & vbCrLf
sConsulta = sConsulta & "       begin" & vbCrLf
sConsulta = sConsulta & "             IF @AMBITOOLD=3" & vbCrLf
sConsulta = sConsulta & "                   begin --old=3" & vbCrLf
sConsulta = sConsulta & "                                              " & vbCrLf
sConsulta = sConsulta & "                         DELETE PLANTILLA_CONF_VISTAS_GRUPO_ATRIB WHERE PLANT=@PLANT AND ATRIB=@ID" & vbCrLf
sConsulta = sConsulta & "                         DELETE PLANTILLA_CONF_VISTAS_ALL_ATRIB WHERE PLANT=@PLANT AND ATRIB=@ID" & vbCrLf
sConsulta = sConsulta & "                                              " & vbCrLf
sConsulta = sConsulta & "                         INSERT INTO PLANTILLA_CONF_VISTAS_PROCE_ATRIB (PLANT,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                SELECT @PLANT,VISTA,@ID FROM  PLANTILLA_CONF_VISTAS_PROCE WHERE PLANT=@PLANT " & vbCrLf
sConsulta = sConsulta & "                   end  --old=3" & vbCrLf
sConsulta = sConsulta & "       end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "        ELSE  --old < ambito" & vbCrLf
sConsulta = sConsulta & "           IF @AMBITOOLD=1" & vbCrLf
sConsulta = sConsulta & "               BEGIN --old=1" & vbCrLf
sConsulta = sConsulta & "                    IF @AMBITO = 3" & vbCrLf
sConsulta = sConsulta & "                       begin --ambito=3" & vbCrLf
sConsulta = sConsulta & "                            DELETE PLANTILLA_CONF_VISTAS_PROCE_ATRIB WHERE PLANT=@PLANT AND ATRIB=@ID" & vbCrLf
sConsulta = sConsulta & "                            IF @GRUPO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                                   INSERT INTO PLANTILLA_CONF_VISTAS_GRUPO_ATRIB (PLANT,GRUPO,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                          SELECT @PLANT,@GRUPO,VISTA,@ID FROM  PLANTILLA_CONF_VISTAS_GRUPO WHERE PLANT=@PLANT AND GRUPO=@GRUPO " & vbCrLf
sConsulta = sConsulta & "                            ELSE" & vbCrLf
sConsulta = sConsulta & "                                   INSERT INTO PLANTILLA_CONF_VISTAS_GRUPO_ATRIB (PLANT,GRUPO,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                          SELECT @PLANT,GRUPO,VISTA,@ID FROM  PLANTILLA_CONF_VISTAS_GRUPO WHERE PLANT=@PLANT         " & vbCrLf
sConsulta = sConsulta & "                            INSERT INTO PLANTILLA_CONF_VISTAS_ALL_ATRIB (PLANT,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                   SELECT @PLANT,VISTA,@ID FROM PLANTILLA_CONF_VISTAS_ALL WHERE PLANT=@PLANT " & vbCrLf
sConsulta = sConsulta & "               end--ambito=3" & vbCrLf
sConsulta = sConsulta & "                 END --old=1" & vbCrLf
sConsulta = sConsulta & "             ELSE " & vbCrLf
sConsulta = sConsulta & "                 BEGIN  --old es 2" & vbCrLf
sConsulta = sConsulta & "                      DELETE PLANTILLA_CONF_VISTAS_PROCE_ATRIB WHERE PLANT=@PLANT AND ATRIB=@ID" & vbCrLf
sConsulta = sConsulta & "                      IF @GRUPO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                             INSERT INTO PLANTILLA_CONF_VISTAS_GRUPO_ATRIB (PLANT,GRUPO,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                    SELECT @PLANT,@GRUPO,VISTA,@ID FROM  PLANTILLA_CONF_VISTAS_GRUPO WHERE PLANT=@PLANT AND GRUPO=@GRUPO " & vbCrLf
sConsulta = sConsulta & "                      ELSE" & vbCrLf
sConsulta = sConsulta & "                             INSERT INTO PLANTILLA_CONF_VISTAS_GRUPO_ATRIB (PLANT,GRUPO,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                    SELECT @PLANT,GRUPO,VISTA,@ID FROM  PLANTILLA_CONF_VISTAS_GRUPO WHERE PLANT=@PLANT " & vbCrLf
sConsulta = sConsulta & "                      INSERT INTO PLANTILLA_CONF_VISTAS_ALL_ATRIB (PLANT,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                             SELECT @PLANT,VISTA,@ID FROM PLANTILLA_CONF_VISTAS_ALL WHERE PLANT=@PLANT " & vbCrLf
sConsulta = sConsulta & "                                             " & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "                END--old es 2" & vbCrLf
sConsulta = sConsulta & "       END --@ambito<>ambitoold" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Private Sub V_2_12_5_Storeds_3()
Dim sConsulta As String
sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_CARGAR_OFERTAS_REUNION_GRUPOS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_CARGAR_OFERTAS_REUNION_GRUPOS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_CARGAR_PRECOFE_GRUPOS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_CARGAR_PRECOFE_GRUPOS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_CARGAR_ULTIMAS_OFERTAS_GRUPOS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_CARGAR_ULTIMAS_OFERTAS_GRUPOS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE SP_CARGAR_OFERTAS_REUNION_GRUPOS @ANYO int, @GMN1 varchar(50), @PROCE int, @IDI VARCHAR(50)=NULL ,@FECREU datetime=NULL AS" & vbCrLf
sConsulta = sConsulta & "   SELECT PROVE.COD,PROVE.DEN,PROCE_OFE.OFE,PROCE_OFE.EST,PROCE_OFE.FECREC,PROCE_OFE.FECVAL," & vbCrLf
sConsulta = sConsulta & "   PROCE_OFE.MON,PROCE_OFE.CAMBIO,PROCE_OFE.OBS,MON.DEN AS MONDEN,OFEEST_IDIOMA.DEN AS ESTDEN,GRUPO_OFE.GRUPO,GRUPO_OFE.OBSADJUN" & vbCrLf
sConsulta = sConsulta & "   FROM PROCE_OFE" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN GRUPO_OFE ON PROCE_OFE.ANYO=GRUPO_OFE.ANYO AND PROCE_OFE.GMN1=GRUPO_OFE.GMN1 AND PROCE_OFE.PROCE=GRUPO_OFE.PROCE" & vbCrLf
sConsulta = sConsulta & "       AND PROCE_OFE.PROVE=GRUPO_OFE.PROVE AND PROCE_OFE.OFE=GRUPO_OFE.OFE" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN MON ON PROCE_OFE.MON=MON.COD" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN VIEW_PROCE_OFE_REU VPOR ON PROCE_OFE.ANYO=VPOR.ANYO AND PROCE_OFE.GMN1=VPOR.GMN1 AND PROCE_OFE.PROCE=VPOR.PROCE AND PROCE_OFE.PROVE = VPOR.PROVE AND PROCE_OFE.OFE = VPOR.OFE" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PROCE_PROVE ON PROCE_OFE.PROVE=PROCE_PROVE.PROVE AND PROCE_OFE.ANYO=PROCE_PROVE.ANYO AND PROCE_OFE.GMN1=PROCE_PROVE.GMN1 AND PROCE_OFE.PROCE=PROCE_PROVE.PROCE" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PROVE ON PROCE_PROVE.PROVE=PROVE.COD" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN PROVI ON PROVE.PAI=PROVI.PAI AND PROVE.PROVI=PROVI.COD" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN OFEEST ON PROCE_OFE.EST=OFEEST.COD AND OFEEST.COMP= 1" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN OFEEST_IDIOMA ON OFEEST.COD= OFEEST_IDIOMA.COD AND OFEEST_IDIOMA.IDIOMA =@IDI" & vbCrLf
sConsulta = sConsulta & "   WHERE PROCE_OFE.ANYO=@ANYO AND PROCE_OFE.GMN1=@GMN1" & vbCrLf
sConsulta = sConsulta & "   AND PROCE_OFE.PROCE=@PROCE AND VPOR.FECREU=@FECREU" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE SP_CARGAR_PRECOFE_GRUPOS @ANYO int, @GMN1 varchar(50), @PROCE int, @FECREU DATETIME=NULL AS" & vbCrLf
sConsulta = sConsulta & "IF @FECREU IS NULL " & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "        SELECT DISTINCT ITEM_OFE.PROVE,ITEM_OFE.NUM,ITEM.ANYO,ITEM.GMN1,ITEM.PROCE,ITEM.ID,ITEM.ART," & vbCrLf
sConsulta = sConsulta & "        ITEM.DESCR + ART4.DEN AS DESCR,ITEM.DEST,ITEM.UNI,ITEM.CANT,ITEM.PREC,ITEM.PRES," & vbCrLf
sConsulta = sConsulta & "        ITEM.PAG,ITEM.FECINI,ITEM.FECFIN,ITEM_OFE.CANTMAX,ITEM.EST AS ESTADO,PROVE_ART4.HOM," & vbCrLf
sConsulta = sConsulta & "        ITEM_OFE.PRECIO,ITEM_OFE.PRECIO2,ITEM_OFE.PRECIO3,ITEM_OFE.USAR,ITEM_OFE.PREC_VALIDO AS PRECOFE,ITEM.GRUPO," & vbCrLf
sConsulta = sConsulta & "   ITEM_OFE.COMENT1,ITEM_OFE.COMENT2,ITEM_OFE.COMENT3,CAST(ITEM_OFE.OBSADJUN AS VARCHAR(4000)) AS OBSADJUNTOS,IA.NUMADJUN" & vbCrLf
sConsulta = sConsulta & "        FROM ITEM" & vbCrLf
sConsulta = sConsulta & "        LEFT JOIN ART4 ON ITEM.GMN1=ART4.GMN1 AND ITEM.GMN2=ART4.GMN2 AND ITEM.GMN3=ART4.GMN3 AND ITEM.GMN4=ART4.GMN4 AND ITEM.ART=ART4.COD" & vbCrLf
sConsulta = sConsulta & "        LEFT JOIN ITEM_OFE ON ITEM.ANYO=ITEM_OFE.ANYO AND ITEM.GMN1=ITEM_OFE.GMN1 AND ITEM.PROCE=ITEM_OFE.PROCE AND ITEM.ID=ITEM_OFE.ITEM " & vbCrLf
sConsulta = sConsulta & "        INNER JOIN PROCE_OFE ON  ITEM_OFE.ANYO=PROCE_OFE.ANYO AND ITEM_OFE.GMN1=PROCE_OFE.GMN1 AND ITEM_OFE.PROCE=PROCE_OFE.PROCE" & vbCrLf
sConsulta = sConsulta & "                                                           AND ITEM_OFE.PROVE =PROCE_OFE.PROVE AND ITEM_OFE.NUM=PROCE_OFE.OFE AND PROCE_OFE.ENVIADA=1" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN PROCE_PROVE ON PROCE_OFE.PROVE=PROCE_PROVE.PROVE AND PROCE_OFE.ANYO=PROCE_PROVE.ANYO AND PROCE_OFE.GMN1=PROCE_PROVE.GMN1 AND PROCE_OFE.PROCE=PROCE_PROVE.PROCE" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN OFEEST ON PROCE_OFE.EST=OFEEST.COD AND OFEEST.COMP= 1" & vbCrLf
sConsulta = sConsulta & "        LEFT JOIN PROVE_ART4 ON PROVE_ART4.GMN1=ITEM.GMN1 AND PROVE_ART4.GMN2=ITEM.GMN2 AND PROVE_ART4.GMN3=ITEM.GMN3 AND PROVE_ART4.GMN4=ITEM.GMN4 " & vbCrLf
sConsulta = sConsulta & "                                                          AND PROVE_ART4.ART=ITEM.ART AND PROVE_ART4.PROVE=PROCE_OFE.PROVE" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN (SELECT IA.ANYO, IA.GMN1, IA.PROCE, IA.ITEM,IA.PROVE,IA.OFE,COUNT(IA.ID) NUMADJUN FROM OFE_ITEM_ADJUN IA" & vbCrLf
sConsulta = sConsulta & "          WHERE IA.ANYO=@ANYO AND IA.GMN1=@GMN1 AND IA.PROCE=@PROCE GROUP BY IA.ANYO, IA.GMN1, IA.PROCE, IA.ITEM,IA.PROVE,IA.OFE) " & vbCrLf
sConsulta = sConsulta & "       IA ON ITEM.ID = IA.ITEM AND ITEM.GMN1 = IA.GMN1 AND ITEM.PROCE = IA.PROCE AND ITEM.ANYO = IA.ANYO AND PROCE_OFE.PROVE=IA.PROVE AND PROCE_OFE.OFE=IA.OFE" & vbCrLf
sConsulta = sConsulta & "        WHERE PROCE_OFE.ANYO=@ANYO AND PROCE_OFE.GMN1=@GMN1 AND PROCE_OFE.PROCE=@PROCE AND PROCE_OFE.ULT=1 " & vbCrLf
sConsulta = sConsulta & "    END" & vbCrLf
sConsulta = sConsulta & "  ELSE" & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "   SELECT DISTINCT ITEM_OFE.PROVE,ITEM_OFE.NUM,ITEM.ANYO,ITEM.GMN1,ITEM.PROCE,ITEM.ID,ITEM.ART," & vbCrLf
sConsulta = sConsulta & "        ITEM.DESCR + ART4.DEN AS DESCR,ITEM.DEST,ITEM.UNI,ITEM.CANT,ITEM.PREC,CASE WHEN IP.PRES IS NULL THEN ITEM.PRES ELSE IP.PRES END PRES," & vbCrLf
sConsulta = sConsulta & "        ITEM.PAG,ITEM.FECINI,ITEM.FECFIN,ITEM_OFE.CANTMAX,ITEM.EST AS ESTADO,PROVE_ART4.HOM," & vbCrLf
sConsulta = sConsulta & "        ITEM_OFE.PRECIO,ITEM_OFE.PRECIO2,ITEM_OFE.PRECIO3,ITEM_OFE.USAR,ITEM_OFE.PREC_VALIDO AS PRECOFE,ITEM.GRUPO," & vbCrLf
sConsulta = sConsulta & "   ITEM_OFE.COMENT1,ITEM_OFE.COMENT2,ITEM_OFE.COMENT3,CAST(ITEM_OFE.OBSADJUN AS VARCHAR(4000)) AS OBSADJUNTOS,IA.NUMADJUN" & vbCrLf
sConsulta = sConsulta & "        FROM ITEM" & vbCrLf
sConsulta = sConsulta & "        LEFT JOIN ITEM_PRES IP ON ITEM.ANYO = IP.ANYO AND ITEM.GMN1 = IP.GMN1 AND ITEM.PROCE = IP.PROCE AND ITEM.ID = IP.ITEM AND IP.FECREU=@FECREU" & vbCrLf
sConsulta = sConsulta & "        LEFT JOIN ART4 ON ITEM.GMN1=ART4.GMN1 AND ITEM.GMN2=ART4.GMN2 AND ITEM.GMN3=ART4.GMN3 " & vbCrLf
sConsulta = sConsulta & "        AND ITEM.GMN4=ART4.GMN4 AND ITEM.ART=ART4.COD" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN ITEM_OFE ON ITEM.ANYO=ITEM_OFE.ANYO AND ITEM.GMN1=ITEM_OFE.GMN1 AND ITEM.PROCE=ITEM_OFE.PROCE AND ITEM.ID=ITEM_OFE.ITEM " & vbCrLf
sConsulta = sConsulta & "        INNER JOIN PROCE_OFE ON  ITEM_OFE.ANYO=PROCE_OFE.ANYO AND ITEM_OFE.GMN1=PROCE_OFE.GMN1 AND ITEM_OFE.PROCE=PROCE_OFE.PROCE " & vbCrLf
sConsulta = sConsulta & "                                                         AND ITEM_OFE.PROVE =PROCE_OFE.PROVE AND ITEM_OFE.NUM=PROCE_OFE.OFE AND PROCE_OFE.ENVIADA=1" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN VIEW_PROCE_OFE_REU VPOR ON PROCE_OFE.ANYO=VPOR.ANYO AND PROCE_OFE.GMN1=VPOR.GMN1 AND PROCE_OFE.PROCE=VPOR.PROCE AND PROCE_OFE.PROVE = VPOR.PROVE AND PROCE_OFE.OFE = VPOR.OFE and VPOR.FECREU=@FECREU" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN PROCE_PROVE ON PROCE_OFE.PROVE=PROCE_PROVE.PROVE AND PROCE_OFE.ANYO=PROCE_PROVE.ANYO AND PROCE_OFE.GMN1=PROCE_PROVE.GMN1 AND PROCE_OFE.PROCE=PROCE_PROVE.PROCE" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN OFEEST ON PROCE_OFE.EST=OFEEST.COD AND OFEEST.COMP= 1" & vbCrLf
sConsulta = sConsulta & "        LEFT JOIN PROVE_ART4 ON PROVE_ART4.GMN1=ITEM.GMN1 AND PROVE_ART4.GMN2=ITEM.GMN2 AND PROVE_ART4.GMN3=ITEM.GMN3 AND PROVE_ART4.GMN4=ITEM.GMN4 " & vbCrLf
sConsulta = sConsulta & "                                                         AND PROVE_ART4.ART=ITEM.ART AND PROVE_ART4.PROVE=PROCE_OFE.PROVE" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN (SELECT IA.ANYO, IA.GMN1, IA.PROCE, IA.ITEM,IA.PROVE,IA.OFE,COUNT(IA.ID) NUMADJUN FROM OFE_ITEM_ADJUN IA" & vbCrLf
sConsulta = sConsulta & "          WHERE IA.ANYO=@ANYO AND IA.GMN1=@GMN1 AND IA.PROCE=@PROCE GROUP BY IA.ANYO, IA.GMN1, IA.PROCE, IA.ITEM,IA.PROVE,IA.OFE) " & vbCrLf
sConsulta = sConsulta & "       IA ON ITEM.ID = IA.ITEM AND ITEM.GMN1 = IA.GMN1 AND ITEM.PROCE = IA.PROCE AND ITEM.ANYO = IA.ANYO AND PROCE_OFE.PROVE=IA.PROVE AND PROCE_OFE.OFE=IA.OFE" & vbCrLf
sConsulta = sConsulta & "        WHERE PROCE_OFE.ANYO=@ANYO AND PROCE_OFE.GMN1=@GMN1 AND PROCE_OFE.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "        " & vbCrLf
sConsulta = sConsulta & "    END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE SP_CARGAR_ULTIMAS_OFERTAS_GRUPOS @ANYO int, @GMN1 varchar(50), @PROCE int, @IDI VARCHAR(50)=NULL AS" & vbCrLf
sConsulta = sConsulta & "      SELECT PROVE.COD,PROVE.DEN,PROCE_OFE.OFE,PROCE_OFE.EST,PROCE_OFE.FECREC,PROCE_OFE.FECVAL," & vbCrLf
sConsulta = sConsulta & "      PROCE_OFE.MON,PROCE_OFE.CAMBIO,PROCE_OFE.OBS,MON.DEN AS MONDEN,OFEEST_IDIOMA.DEN AS ESTDEN,GRUPO_OFE.GRUPO,GRUPO_OFE.OBSADJUN" & vbCrLf
sConsulta = sConsulta & "      FROM PROCE_OFE" & vbCrLf
sConsulta = sConsulta & "      INNER JOIN GRUPO_OFE ON PROCE_OFE.ANYO=GRUPO_OFE.ANYO AND PROCE_OFE.GMN1=GRUPO_OFE.GMN1 AND PROCE_OFE.PROCE=GRUPO_OFE.PROCE" & vbCrLf
sConsulta = sConsulta & "       AND PROCE_OFE.PROVE=GRUPO_OFE.PROVE AND PROCE_OFE.OFE=GRUPO_OFE.OFE" & vbCrLf
sConsulta = sConsulta & "      INNER JOIN MON ON PROCE_OFE.MON=MON.COD" & vbCrLf
sConsulta = sConsulta & "      INNER JOIN PROCE_PROVE ON PROCE_OFE.PROVE=PROCE_PROVE.PROVE AND PROCE_OFE.ANYO=PROCE_PROVE.ANYO AND PROCE_OFE.GMN1=PROCE_PROVE.GMN1 AND PROCE_OFE.PROCE=PROCE_PROVE.PROCE" & vbCrLf
sConsulta = sConsulta & "      INNER JOIN PROVE ON PROCE_PROVE.PROVE=PROVE.COD" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN PROVI ON PROVE.PAI=PROVI.PAI AND PROVE.PROVI=PROVI.COD" & vbCrLf
sConsulta = sConsulta & "      INNER JOIN OFEEST ON PROCE_OFE.EST=OFEEST.COD AND OFEEST.COMP= 1" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN OFEEST_IDIOMA ON OFEEST.COD= OFEEST_IDIOMA.COD AND OFEEST_IDIOMA.IDIOMA =@IDI" & vbCrLf
sConsulta = sConsulta & "      WHERE PROCE_OFE.ANYO=@ANYO AND PROCE_OFE.GMN1=@GMN1" & vbCrLf
sConsulta = sConsulta & "      AND PROCE_OFE.PROCE=@PROCE AND PROCE_OFE.ULT=1 AND PROCE_OFE.ENVIADA=1" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_AR_CALCULAR]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_AR_CALCULAR]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_AR_VOLUMENES]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_AR_VOLUMENES]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE SP_AR_CALCULAR AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCE AS INT,@ANYO AS INT,@GMN1 AS VARCHAR(3),@PROVE AS VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "declare @RES AS INT    " & vbCrLf
sConsulta = sConsulta & "declare @NumCal as int" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "set @NumCal=0" & vbCrLf
sConsulta = sConsulta & "DECLARE C_Cal CURSOR LOCAL FOR  SELECT PROCE.ANYO,PROCE.GMN1,PROCE.COD   FROM PROCE" & vbCrLf
sConsulta = sConsulta & "  WHERE PROCE.CALCPEND = 1  AND EST >=11 AND EST <20" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "OPEN C_Cal" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C_Cal INTO @ANYO,@GMN1,@PROCE" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "     begin distributed tran" & vbCrLf
sConsulta = sConsulta & "     set xact_abort on" & vbCrLf
sConsulta = sConsulta & "     EXEC SP_AR_AHORROS_CALCULAR @ANYO,@GMN1,@PROCE,@RES OUTPUT" & vbCrLf
sConsulta = sConsulta & "     if @res =0 " & vbCrLf
sConsulta = sConsulta & "              begin" & vbCrLf
sConsulta = sConsulta & "             commit  transaction" & vbCrLf
sConsulta = sConsulta & "             set @NumCal= @NumCal + 1" & vbCrLf
sConsulta = sConsulta & "             set xact_abort off" & vbCrLf
sConsulta = sConsulta & "             end" & vbCrLf
sConsulta = sConsulta & "     else" & vbCrLf
sConsulta = sConsulta & "             if @@trancount >0" & vbCrLf
sConsulta = sConsulta & "                 rollback  tran" & vbCrLf
sConsulta = sConsulta & "      FETCH NEXT FROM C_Cal INTO @ANYO,@GMN1,@PROCE" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "CLOSE C_Cal " & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C_Cal" & vbCrLf
sConsulta = sConsulta & "IF @NumCal >0 " & vbCrLf
sConsulta = sConsulta & "    UPDATE PARGEN_DEF SET FEC_ACT_AHORROS=GETDATE()" & vbCrLf
sConsulta = sConsulta & "set xact_abort off" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE   PROCEDURE  SP_AR_VOLUMENES (@ANYO SMALLINT,@GMN1 VARCHAR(50), @PROCE INT,@FECULT DATETIME,@DEN VARCHAR(200),@RES INT OUTPUT)  AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVE AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSP_COD AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @VOL AS FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @SRV AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @BD AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @CODCIA AS VARCHAR(80)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL AS VARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @MONCEN AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @MONPORT AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @SRV=FSP_SRV,@BD=FSP_BD,@CODCIA=FSP_CIA FROM PARGEN_PORT WHERE ID=1" & vbCrLf
sConsulta = sConsulta & "SELECT @MONCEN=MONCEN FROM PARGEN_DEF WHERE ID=1" & vbCrLf
sConsulta = sConsulta & "SELECT @MONPORT= FSP_COD FROM MON WHERE COD= @MONCEN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE Cur_VOL Cursor FORWARD_ONLY STATIC READ_ONLY FOR " & vbCrLf
sConsulta = sConsulta & "SELECT AR_ITEM_PROVE.PROVE,FSP_COD,SUM(AR_ITEM_PROVE.PREC) FROM AR_ITEM_PROVE" & vbCrLf
sConsulta = sConsulta & "  INNER JOIN PROVE ON AR_ITEM_PROVE.PROVE= PROVE.COD WHERE AR_ITEM_PROVE.ANYO=@ANYO AND  AR_ITEM_PROVE.GMN1=@GMN1 AND AR_ITEM_PROVE.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "  AND AR_ITEM_PROVE.PROVE IN (SELECT PROVE FROM PROCE_OFE_WEB WHERE ANYO=@ANYO AND PROCE_OFE_WEB.GMN1 = @GMN1 AND PROCE_OFE_WEB.PROCE=@PROCE)" & vbCrLf
sConsulta = sConsulta & "  GROUP BY AR_ITEM_PROVE.PROVE,FSP_COD" & vbCrLf
sConsulta = sConsulta & "OPEN Cur_VOL" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM Cur_VOL INTO @PROVE,@FSP_COD,@VOL" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "     SET @SQL=@SRV + '.' + @BD + '.dbo.SP_ADD_VOLADJ'" & vbCrLf
sConsulta = sConsulta & "      EXECUTE @SQL @CODCIA,@FSP_COD,@ANYO,@GMN1,@PROCE,@VOL,@MONCEN,@MONPORT,@FECULT,@DEN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM Cur_VOL INTO @PROVE,@FSP_COD,@VOL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "ERR_:" & vbCrLf
sConsulta = sConsulta & "Close Cur_VOL" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE Cur_VOL" & vbCrLf
sConsulta = sConsulta & "SET @RES=@@ERROR" & vbCrLf
sConsulta = sConsulta & "Return" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_AR_JOB_MODIF_DATOS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_AR_JOB_MODIF_DATOS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE dbo.SP_AR_JOB_MODIF_DATOS (@MODIF TINYINT, @ENAB AS TINYINT ,@NOMBRE VARCHAR(50),@TIPO INT,@INTERVALO INT ,@RECFACTOR INT,@SUBDIA INT, @FRECSUBDIA INT,@RELATIVO INT, @FECINI INT,@TIMEINI INT, @RET_STAT INT OUTPUT)AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @BD AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @COMANDO AS VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "IF @MODIF=1 " & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "     IF @ENAB != 2" & vbCrLf
sConsulta = sConsulta & "             EXEC @RET_STAT= msdb.dbo.sp_update_job @job_name=@NOMBRE,@enabled=@ENAB" & vbCrLf
sConsulta = sConsulta & "             IF @RET_STAT = 1" & vbCrLf
sConsulta = sConsulta & "                   RETURN" & vbCrLf
sConsulta = sConsulta & "     EXEC @RET_STAT= msdb.dbo.sp_update_jobschedule @job_name=@NOMBRE,@name='ACT_AHORROS_SCH1',@freq_type =@TIPO,@freq_interval =@INTERVALO,@freq_subday_type=@SUBDIA,@freq_subday_interval =@FRECSUBDIA,@freq_relative_interval=@RELATIVO,@freq_recurrence_factor =@RECFACTOR, @active_start_date=@FECINI,@active_start_time =@TIMEINI" & vbCrLf
sConsulta = sConsulta & "END " & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @BD=SUBSTRING(@NOMBRE,13,LEN(@NOMBRE)-12)" & vbCrLf
sConsulta = sConsulta & "   EXEC @RET_STAT= msdb.dbo.sp_add_job @job_name=@NOMBRE,@enabled=@ENAB" & vbCrLf
sConsulta = sConsulta & "   IF @RET_STAT= 0" & vbCrLf
sConsulta = sConsulta & "   BEGIN       " & vbCrLf
sConsulta = sConsulta & "         SET @COMANDO='osql -E -Q ""Exec ' + @BD + '.dbo.AR_CALCULAR""'" & vbCrLf
sConsulta = sConsulta & "         EXEC @RET_STAT= msdb.dbo.sp_add_jobstep @job_name=@NOMBRE,@step_id =1,@step_name='ACT_AHORROS_STEP1',@subsystem = 'CMDEXEC', @command =@COMANDO " & vbCrLf
sConsulta = sConsulta & "         IF @RET_STAT=0" & vbCrLf
sConsulta = sConsulta & "         BEGIN" & vbCrLf
sConsulta = sConsulta & "             EXEC @RET_STAT = msdb.dbo.sp_add_jobserver  @job_name=@NOMBRE,  @server_name ='(LOCAL)'" & vbCrLf
sConsulta = sConsulta & "             IF @RET_STAT=0" & vbCrLf
sConsulta = sConsulta & "                   EXEC @RET_STAT= msdb.dbo.sp_add_jobschedule @job_name=@NOMBRE,@name='ACT_AHORROS_SCH1',@freq_type =@TIPO,@freq_interval =@INTERVALO,@freq_subday_type=@SUBDIA,@freq_subday_interval =@FRECSUBDIA,@freq_relative_interval=@RELATIVO,@freq_recurrence_factor =@RECFACTOR, @active_start_date=@FECINI,@active_start_time =@TIMEINI" & vbCrLf
sConsulta = sConsulta & "         END" & vbCrLf
sConsulta = sConsulta & "    END " & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Private Sub V_2_12_5_Tablas_3()
Dim sConsulta As String
sConsulta = ""
sConsulta = sConsulta & "ALTER TABLE dbo.PARGEN_GEST ADD" & vbCrLf
sConsulta = sConsulta & "   SELPOSITIVA tinyint NOT NULL CONSTRAINT DF_PARGEN_GEST_SELPOSITIVA DEFAULT 1" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "ALTER TABLE dbo.PARINS_GEST ADD" & vbCrLf
sConsulta = sConsulta & "   SELPOSITIVA tinyint NOT NULL CONSTRAINT DF_PARINS_GEST_SELPOSITIVA DEFAULT 1" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub


Public Function CodigoDeActualizacion2_12_05_04A2_12_05_05() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim bBool As Boolean

On Error GoTo Error
    

sConsulta = "BEGIN TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = True
ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

frmProgreso.lblDetalle = "Actualizaci�n de tablas"
frmProgreso.lblDetalle.Refresh
frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_12_5_Tablas_5

frmProgreso.lblDetalle = "Actualizaci�n de triggers"
frmProgreso.lblDetalle.Refresh
frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_12_5_Triggers_5

sConsulta = "UPDATE VERSION SET NUM ='2.12.05.05'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
ExecuteSQL gRDOCon, sConsulta


sConsulta = "COMMIT TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = False

CodigoDeActualizacion2_12_05_04A2_12_05_05 = True
Exit Function
    
Error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_12_05_04A2_12_05_05 = False

End Function

Public Function CodigoDeActualizacion2_12_05_05A2_12_05_06() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim bBool As Boolean

On Error GoTo Error
    

sConsulta = "BEGIN TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = True
ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

frmProgreso.lblDetalle = "Actualizaci�n de storeds"
frmProgreso.lblDetalle.Refresh
frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_12_5_Storeds_6


sConsulta = "UPDATE VERSION SET NUM ='2.12.05.06'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
ExecuteSQL gRDOCon, sConsulta


sConsulta = "COMMIT TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = False

CodigoDeActualizacion2_12_05_05A2_12_05_06 = True
Exit Function
    
Error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_12_05_05A2_12_05_06 = False

End Function

Private Sub V_2_12_5_Tablas_5()
Dim sConsulta As String
sConsulta = ""
sConsulta = sConsulta & "ALTER TABLE dbo.PROCE_PROVE_POT ADD" & vbCrLf
sConsulta = sConsulta & "   POT tinyint NOT NULL CONSTRAINT DF_PROCE_PROVE_POT_POT DEFAULT 0" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "UPDATE PROCE_PROVE_POT SET POT=1 WHERE ASIG=1"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "DELETE FROM CONF_VISTAS_PROCE_ATRIB WHERE ATRIB NOT  IN"
sConsulta = sConsulta & " (SELECT ID FROM PROCE_ATRIB WHERE PROCE_ATRIB.ANYO=CONF_VISTAS_PROCE_ATRIB.ANYO AND"
sConsulta = sConsulta & " PROCE_ATRIB.GMN1 = CONF_VISTAS_PROCE_ATRIB.GMN1 AND PROCE_ATRIB.PROCE = CONF_VISTAS_PROCE_ATRIB.PROCE)"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "DELETE FROM CONF_VISTAS_GRUPO_ATRIB WHERE ATRIB NOT  IN"
sConsulta = sConsulta & " (SELECT ID FROM PROCE_ATRIB WHERE PROCE_ATRIB.ANYO=CONF_VISTAS_GRUPO_ATRIB.ANYO AND"
sConsulta = sConsulta & " PROCE_ATRIB.GMN1 = CONF_VISTAS_GRUPO_ATRIB.GMN1 AND PROCE_ATRIB.PROCE = CONF_VISTAS_GRUPO_ATRIB.PROCE"
sConsulta = sConsulta & " AND (PROCE_ATRIB.GRUPO=CONF_VISTAS_GRUPO_ATRIB.GRUPO OR PROCE_ATRIB.GRUPO IS NULL))"
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "DELETE FROM CONF_VISTAS_ALL_ATRIB WHERE ATRIB NOT  IN"
sConsulta = sConsulta & " (SELECT ID FROM PROCE_ATRIB WHERE PROCE_ATRIB.ANYO=CONF_VISTAS_ALL_ATRIB.ANYO AND"
sConsulta = sConsulta & " PROCE_ATRIB.GMN1 = CONF_VISTAS_ALL_ATRIB.GMN1 AND PROCE_ATRIB.PROCE = CONF_VISTAS_ALL_ATRIB.PROCE)"
ExecuteSQL gRDOCon, sConsulta

End Sub

Private Sub V_2_12_5_Triggers_5()
Dim sConsulta As String
sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USU_TG_NUEVOUSU]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[USU_TG_NUEVOUSU]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE  TRIGGER USU_TG_NUEVOUSU ON dbo.USU" & vbCrLf
sConsulta = sConsulta & "FOR INSERT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @COD VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PER VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @TIPO SMALLINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @COUNT INTEGER" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_Aprob_Lim_Del CURSOR LOCAL FOR SELECT COD,PER FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_Aprob_Lim_Del" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_Aprob_Lim_Del INTO @COD,@PER" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "    IF (SELECT COUNT(*) AS APROB FROM APROB_LIM WHERE PER=@PER)>0 " & vbCrLf
sConsulta = sConsulta & "        IF (SELECT COUNT(*) AS APROB FROM APROB_LIM A1 INNER JOIN APROB_LIM A2 ON A1.PADRE=A2.ID WHERE A2.PER =@PER )=0 -- No es aprobador tipo2" & vbCrLf
sConsulta = sConsulta & "            IF (SELECT COUNT(*) FROM SEGURIDAD WHERE APROB_TIPO1 =@PER)=0 -- No es aprobador tipo1" & vbCrLf
sConsulta = sConsulta & "     SET @TIPO=1" & vbCrLf
sConsulta = sConsulta & "            ELSE --Es aprobador tipo1" & vbCrLf
sConsulta = sConsulta & "     SET @TIPO=3" & vbCrLf
sConsulta = sConsulta & "        ELSE" & vbCrLf
sConsulta = sConsulta & "   SET @TIPO=3" & vbCrLf
sConsulta = sConsulta & "    ELSE" & vbCrLf
sConsulta = sConsulta & "           IF (SELECT COUNT(*) FROM SEGURIDAD WHERE APROB_TIPO1 =@PER)>0 -- No es aprobador tipo1" & vbCrLf
sConsulta = sConsulta & "     SET @TIPO=2" & vbCrLf
sConsulta = sConsulta & "          ELSE" & vbCrLf
sConsulta = sConsulta & "     SET @TIPO=0" & vbCrLf
sConsulta = sConsulta & "    UPDATE USU SET FSEPTIPO=@TIPO WHERE COD=@COD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_Aprob_Lim_Del INTO @COD,@PER" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_Aprob_Lim_Del" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_Aprob_Lim_Del" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub


Private Sub V_2_12_5_Storeds_6()
Dim sConsulta As String


sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_DEVOLVER_LINEAS_DE_ORDEN_FAVORITOS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_DEVOLVER_LINEAS_DE_ORDEN_FAVORITOS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE FSEP_DEVOLVER_LINEAS_DE_ORDEN_FAVORITOS(@ORDENID INTEGER,@IDI varchar(20) = 'SPA')  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(1500)" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT LP.ID LINEAID,LP.USU," & vbCrLf
sConsulta = sConsulta & "                LP.ART_INT + I.ART CODART," & vbCrLf
sConsulta = sConsulta & "                LP.GMN1," & vbCrLf
sConsulta = sConsulta & "                LP.GMN2," & vbCrLf
sConsulta = sConsulta & "                LP.GMN3," & vbCrLf
sConsulta = sConsulta & "                LP.GMN4," & vbCrLf
sConsulta = sConsulta & "                LP.PROVE," & vbCrLf
sConsulta = sConsulta & "                ART4.DEN + I.DESCR  + ART_AUX.DEN + + LP.DESCR_LIBRE DENART," & vbCrLf
sConsulta = sConsulta & "                PA.COD_EXT ART_EXT," & vbCrLf
sConsulta = sConsulta & "                LP.DEST DESTINO," & vbCrLf
sConsulta = sConsulta & "                D.DEN DESTDEN," & vbCrLf
sConsulta = sConsulta & "                LP.UP UNIDAD," & vbCrLf
sConsulta = sConsulta & "                U.DEN UNIDEN," & vbCrLf
sConsulta = sConsulta & "                LP.PREC_UP PRECIOUNITARIO," & vbCrLf
sConsulta = sConsulta & "                LP.CANT_PED CANTIDAD," & vbCrLf
sConsulta = sConsulta & "                IA.CANT_ADJ * LP.FC AS CANTADJUDICADA," & vbCrLf
sConsulta = sConsulta & "                LP.PREC_UP * LP.CANT_PED IMPORTE," & vbCrLf
sConsulta = sConsulta & "                LP.CAT1," & vbCrLf
sConsulta = sConsulta & "                LP.CAT2," & vbCrLf
sConsulta = sConsulta & "                LP.CAT3," & vbCrLf
sConsulta = sConsulta & "                LP.CAT4," & vbCrLf
sConsulta = sConsulta & "                LP.CAT5," & vbCrLf
sConsulta = sConsulta & "                LP.OBS," & vbCrLf
sConsulta = sConsulta & "                case when c5.cod is null then " & vbCrLf
sConsulta = sConsulta & "                        case when c4.cod is null then " & vbCrLf
sConsulta = sConsulta & "                             case when c3.cod is null then" & vbCrLf
sConsulta = sConsulta & "                                  case when c2.cod is null then" & vbCrLf
sConsulta = sConsulta & "                                       c1.cod" & vbCrLf
sConsulta = sConsulta & "                                  else" & vbCrLf
sConsulta = sConsulta & "                                  c2.cod" & vbCrLf
sConsulta = sConsulta & "                                  end" & vbCrLf
sConsulta = sConsulta & "                             else" & vbCrLf
sConsulta = sConsulta & "                             c3.cod" & vbCrLf
sConsulta = sConsulta & "                             end" & vbCrLf
sConsulta = sConsulta & "                        else" & vbCrLf
sConsulta = sConsulta & "                        c4.cod" & vbCrLf
sConsulta = sConsulta & "                        end" & vbCrLf
sConsulta = sConsulta & "                    else" & vbCrLf
sConsulta = sConsulta & "                    c5.cod" & vbCrLf
sConsulta = sConsulta & "                    end CATEGORIA," & vbCrLf
sConsulta = sConsulta & "                case when c5.cod is null then " & vbCrLf
sConsulta = sConsulta & "                        case when c4.cod is null then " & vbCrLf
sConsulta = sConsulta & "                             case when c3.cod is null then" & vbCrLf
sConsulta = sConsulta & "                                  case when c2.cod is null then" & vbCrLf
sConsulta = sConsulta & "                                       '(' + c1.cod + ') ' + c1.den" & vbCrLf
sConsulta = sConsulta & "                                  else" & vbCrLf
sConsulta = sConsulta & "                                  '(' + c1.cod + ') ' + c1.den + ' - (' + c2.cod + ') ' + c2.den" & vbCrLf
sConsulta = sConsulta & "                                  end" & vbCrLf
sConsulta = sConsulta & "                             else" & vbCrLf
sConsulta = sConsulta & "                             '(' + c1.cod + ') ' + c1.den + ' - (' + c2.cod + ') ' + c2.den + ' - (' + c3.cod + ') ' + c3.den" & vbCrLf
sConsulta = sConsulta & "                             end" & vbCrLf
sConsulta = sConsulta & "                        else" & vbCrLf
sConsulta = sConsulta & "                        '(' + c1.cod + ') ' + c1.den + ' - (' + c2.cod + ') ' + c2.den + ' - (' + c3.cod + ') ' + c3.den + ' - (' + c4.cod + ') ' + c4.den" & vbCrLf
sConsulta = sConsulta & "                        end" & vbCrLf
sConsulta = sConsulta & "                    else" & vbCrLf
sConsulta = sConsulta & "                    '(' + c1.cod + ') ' + c1.den + ' - (' + c2.cod + ') ' + c2.den + ' - (' + c3.cod + ') ' + c3.den + ' - (' + c4.cod + ') ' + c4.den + ' - (' + c5.cod + ') ' + c5.den" & vbCrLf
sConsulta = sConsulta & "                    end CATEGORIARAMA," & vbCrLf
sConsulta = sConsulta & "               " & vbCrLf
sConsulta = sConsulta & "                        " & vbCrLf
sConsulta = sConsulta & "                C1.DEN CAT1DEN," & vbCrLf
sConsulta = sConsulta & "                C2.DEN CAT2DEN," & vbCrLf
sConsulta = sConsulta & "                C3.DEN CAT3DEN," & vbCrLf
sConsulta = sConsulta & "                C4.DEN CAT4DEN," & vbCrLf
sConsulta = sConsulta & "                C5.DEN CAT5DEN," & vbCrLf
sConsulta = sConsulta & "                I.ANYO ANYOPROCE," & vbCrLf
sConsulta = sConsulta & "                I.PROCE CODPROCE," & vbCrLf
sConsulta = sConsulta & "                I.ID ITEM,LPA.ADJUNTOS," & vbCrLf
sConsulta = sConsulta & "                LP.VALOR1,LP.VALOR2,CC1.DEN AS DENCAMPO1,CC2.DEN AS DENCAMPO2,CC1.OBL AS OBL1,CC2.OBL AS OBL2,CC1.ID AS CAMPO1,CC2.ID AS CAMPO2," & vbCrLf
sConsulta = sConsulta & "       PROCE.DEN AS DENPROCE,LP.OBSADJUN, LP.LINEA, ISNULL(CLM.CANT_MIN,CL.CANT_MIN_DEF) CANT_MIN, ISNULL(CL.PRECIO_UC,LP.PREC_UP) PRECIO_UC, LP.FC" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & " FROM FAVORITOS_LINEAS_PEDIDO LP" & vbCrLf
sConsulta = sConsulta & "     INNER JOIN DEST D" & vbCrLf
sConsulta = sConsulta & "             ON LP.DEST = D.COD" & vbCrLf
sConsulta = sConsulta & "     INNER JOIN UNI U" & vbCrLf
sConsulta = sConsulta & "             ON LP.UP = U.COD" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN ITEM I " & vbCrLf
sConsulta = sConsulta & "             ON LP.ANYO = I.ANYO" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN1=I.GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND LP.PROCE=I.PROCE " & vbCrLf
sConsulta = sConsulta & "            AND LP.ITEM=I.ID" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN PROVE_ART4 PA" & vbCrLf
sConsulta = sConsulta & "             ON LP.PROVE = PA.PROVE" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN1 = PA.GMN1" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN2 = PA.GMN2" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN3 = PA.GMN3" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN4 = PA.GMN4" & vbCrLf
sConsulta = sConsulta & "            AND CASE WHEN LP.ART_INT IS NULL THEN I.ART ELSE LP.ART_INT END = PA.ART" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN ART4 " & vbCrLf
sConsulta = sConsulta & "             ON LP.GMN1=ART4.GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN2=ART4.GMN2 " & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN3=ART4.GMN3 " & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN4=ART4.GMN4 " & vbCrLf
sConsulta = sConsulta & "            AND LP.ART_INT=ART4.COD" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN ITEM_ADJ IA " & vbCrLf
sConsulta = sConsulta & "             ON LP.ITEM = IA.ITEM " & vbCrLf
sConsulta = sConsulta & "            AND LP.ANYO = IA.ANYO" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN1 = IA.GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND LP.PROCE= IA.PROCE " & vbCrLf
sConsulta = sConsulta & "            AND LP.PROVE= IA.PROVE" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN ART4 ART_AUX " & vbCrLf
sConsulta = sConsulta & "             ON ART_AUX.COD=I.ART " & vbCrLf
sConsulta = sConsulta & "            AND ART_AUX.GMN1=I.GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND ART_AUX.GMN2=I.GMN2 " & vbCrLf
sConsulta = sConsulta & "            AND ART_AUX.GMN3=I.GMN3 " & vbCrLf
sConsulta = sConsulta & "            AND ART_AUX.GMN4=I.GMN4" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN CATN1 C1" & vbCrLf
sConsulta = sConsulta & "             ON LP.CAT1 = C1.ID" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN CATN2 C2" & vbCrLf
sConsulta = sConsulta & "             ON LP.CAT2 = C2.ID" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN CATN3 C3" & vbCrLf
sConsulta = sConsulta & "             ON LP.CAT3 = C3.ID" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN CATN4 C4" & vbCrLf
sConsulta = sConsulta & "             ON LP.CAT4 = C4.ID" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN CATN5 C5" & vbCrLf
sConsulta = sConsulta & "             ON LP.CAT5 = C5.ID" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN " & vbCrLf
sConsulta = sConsulta & "   (SELECT COUNT(*) AS ADJUNTOS,LINEA  FROM FAVORITOS_LINEAS_PEDIDO_ADJUN GROUP BY FAVORITOS_LINEAS_PEDIDO_ADJUN.LINEA) LPA ON LP.ID= LPA.LINEA " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN CATALOG_CAMPOS AS CC1 " & vbCrLf
sConsulta = sConsulta & "       ON CC1.ID=LP.CAMPO1 AND CC1.IDIOMA=@IDI" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN CATALOG_CAMPOS AS CC2 " & vbCrLf
sConsulta = sConsulta & "            ON CC2.ID=LP.CAMPO2 AND CC2.IDIOMA=@IDI" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN PROCE " & vbCrLf
sConsulta = sConsulta & "       ON LP.ANYO=PROCE.ANYO AND LP.GMN1=PROCE.GMN1 AND LP.PROCE=PROCE.COD" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN CATALOG_LIN CL" & vbCrLf
sConsulta = sConsulta & "             ON LP.LINEA = CL.ID" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN CAT_LIN_MIN CLM" & vbCrLf
sConsulta = sConsulta & "             ON LP.LINEA = CLM.LINEA" & vbCrLf
sConsulta = sConsulta & "            AND LP.UP = CLM.UP" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & " WHERE LP.ORDEN=@ORDENID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ANYA_PROVE_ESP]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_ANYA_PROVE_ESP]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_ANYA_PROVE_ESP (@PROVE VARCHAR(100),@ID SMALLINT,@NOM VARCHAR(300),@COM VARCHAR(500),@FECHA DATETIME)  AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "INSERT INTO PROVE_ESP_PORTAL (PROVE,ID,NOM,COM,FECHA) VALUES (@PROVE,@ID,@NOM,@COM,@FECHA)" & vbCrLf
sConsulta = sConsulta & "Return" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
ExecuteSQL gRDOCon, sConsulta



sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_AR_JOB_MODIF_DATOS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_AR_JOB_MODIF_DATOS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE dbo.SP_AR_JOB_MODIF_DATOS (@MODIF TINYINT, @ENAB AS TINYINT ,@NOMBRE VARCHAR(50),@TIPO INT,@INTERVALO INT ,@RECFACTOR INT,@SUBDIA INT, @FRECSUBDIA INT,@RELATIVO INT, @FECINI INT,@TIMEINI INT, @RET_STAT INT OUTPUT)AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @BD AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @COMANDO AS VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "IF @MODIF=1 " & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "     IF @ENAB != 2" & vbCrLf
sConsulta = sConsulta & "             EXEC @RET_STAT= msdb.dbo.sp_update_job @job_name=@NOMBRE,@enabled=@ENAB" & vbCrLf
sConsulta = sConsulta & "             IF @RET_STAT = 1" & vbCrLf
sConsulta = sConsulta & "                   RETURN" & vbCrLf
sConsulta = sConsulta & "     EXEC @RET_STAT= msdb.dbo.sp_update_jobschedule @job_name=@NOMBRE,@name='ACT_AHORROS_SCH1',@freq_type =@TIPO,@freq_interval =@INTERVALO,@freq_subday_type=@SUBDIA,@freq_subday_interval =@FRECSUBDIA,@freq_relative_interval=@RELATIVO,@freq_recurrence_factor =@RECFACTOR, @active_start_date=@FECINI,@active_start_time =@TIMEINI" & vbCrLf
sConsulta = sConsulta & "END " & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @BD=SUBSTRING(@NOMBRE,13,LEN(@NOMBRE)-12)" & vbCrLf
sConsulta = sConsulta & "   EXEC @RET_STAT= msdb.dbo.sp_add_job @job_name=@NOMBRE,@enabled=@ENAB" & vbCrLf
sConsulta = sConsulta & "   IF @RET_STAT= 0" & vbCrLf
sConsulta = sConsulta & "   BEGIN       " & vbCrLf
sConsulta = sConsulta & "         SET @COMANDO='osql -E -Q ""Exec ' + @BD + '.dbo.SP_AR_CALCULAR""'" & vbCrLf
sConsulta = sConsulta & "         EXEC @RET_STAT= msdb.dbo.sp_add_jobstep @job_name=@NOMBRE,@step_id =1,@step_name='ACT_AHORROS_STEP1',@subsystem = 'CMDEXEC', @command =@COMANDO " & vbCrLf
sConsulta = sConsulta & "         IF @RET_STAT=0" & vbCrLf
sConsulta = sConsulta & "         BEGIN" & vbCrLf
sConsulta = sConsulta & "             EXEC @RET_STAT = msdb.dbo.sp_add_jobserver  @job_name=@NOMBRE,  @server_name ='(LOCAL)'" & vbCrLf
sConsulta = sConsulta & "             IF @RET_STAT=0" & vbCrLf
sConsulta = sConsulta & "                   EXEC @RET_STAT= msdb.dbo.sp_add_jobschedule @job_name=@NOMBRE,@name='ACT_AHORROS_SCH1',@freq_type =@TIPO,@freq_interval =@INTERVALO,@freq_subday_type=@SUBDIA,@freq_subday_interval =@FRECSUBDIA,@freq_relative_interval=@RELATIVO,@freq_recurrence_factor =@RECFACTOR, @active_start_date=@FECINI,@active_start_time =@TIMEINI" & vbCrLf
sConsulta = sConsulta & "         END" & vbCrLf
sConsulta = sConsulta & "    END " & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub


Public Function CodigoDeActualizacion2_12_05_06A2_12_05_07() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim bBool As Boolean

On Error GoTo Error
    

sConsulta = "BEGIN TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = True
ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

frmProgreso.lblDetalle = "Actualizaci�n de storeds"
frmProgreso.lblDetalle.Refresh
frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_12_5_Storeds_7


sConsulta = "UPDATE VERSION SET NUM ='2.12.05.07'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
ExecuteSQL gRDOCon, sConsulta


sConsulta = "COMMIT TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = False

CodigoDeActualizacion2_12_05_06A2_12_05_07 = True
Exit Function
    
Error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_12_05_06A2_12_05_07 = False

End Function

Private Sub V_2_12_5_Storeds_7()
Dim sConsulta As String

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_INFORME2]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_INFORME2]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE dbo.FSEP_INFORME2(@APROVISIONADOR VARCHAR(50),@APROBADOR VARCHAR(50), @DESDEFECHA VARCHAR(10),@HASTAFECHA VARCHAR(10))  AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @APROBADOR IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SELECT DISTINCT LINEAS_PEDIDO.ID,LINEAS_PEDIDO.ART_INT + ITEM.ART AS ARTICULO," & vbCrLf
sConsulta = sConsulta & "   ART4.DEN + ITEM.DESCR  + ART_AUX.DEN + LINEAS_PEDIDO.DESCR_LIBRE AS DESCR," & vbCrLf
sConsulta = sConsulta & "   LINEAS_PEDIDO.FECEMISION AS FECHA ,LINEAS_PEDIDO.CANT_PED AS CANTIDAD,LINEAS_PEDIDO.UP AS UNIDAD," & vbCrLf
sConsulta = sConsulta & "   LINEAS_PEDIDO.DEST AS DESTINO," & vbCrLf
sConsulta = sConsulta & "   LINEAS_PEDIDO.PREC_UP*LINEAS_PEDIDO.CANT_PED AS IMPORTE," & vbCrLf
sConsulta = sConsulta & "   CASE WHEN C2.COD IS NOT NULL THEN" & vbCrLf
sConsulta = sConsulta & "        CASE WHEN C3.COD IS NOT NULL THEN " & vbCrLf
sConsulta = sConsulta & "             CASE WHEN C4.COD IS NOT NULL THEN " & vbCrLf
sConsulta = sConsulta & "                  CASE WHEN C5.COD IS NOT NULL THEN " & vbCrLf
sConsulta = sConsulta & "                         C1.COD + ' - ' + C2.COD + ' - ' + C3.COD + ' - ' + C4.COD + ' - ' + C5.COD" & vbCrLf
sConsulta = sConsulta & "                  ELSE" & vbCrLf
sConsulta = sConsulta & "                         C1.COD + ' - ' + C2.COD + ' - ' + C3.COD + ' - ' + C4.COD" & vbCrLf
sConsulta = sConsulta & "                  END" & vbCrLf
sConsulta = sConsulta & "             ELSE" & vbCrLf
sConsulta = sConsulta & "                  C1.COD + ' - ' + C2.COD + ' - ' + C3.COD" & vbCrLf
sConsulta = sConsulta & "             END" & vbCrLf
sConsulta = sConsulta & "        ELSE" & vbCrLf
sConsulta = sConsulta & "             C1.COD + ' - ' + C2.COD" & vbCrLf
sConsulta = sConsulta & "        END" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "         C1.COD" & vbCrLf
sConsulta = sConsulta & "   END CATEGORIA," & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   CASE WHEN C2.COD IS NOT NULL THEN" & vbCrLf
sConsulta = sConsulta & "        CASE WHEN C3.COD IS NOT NULL THEN " & vbCrLf
sConsulta = sConsulta & "             CASE WHEN C4.COD IS NOT NULL THEN " & vbCrLf
sConsulta = sConsulta & "                  CASE WHEN C5.COD IS NOT NULL THEN " & vbCrLf
sConsulta = sConsulta & "                         '(' + C1.COD + ' - ' + C2.COD + ' - ' + C3.COD + ' - ' + C4.COD + ' - ' + C5.COD + ') ' + C5.DEN" & vbCrLf
sConsulta = sConsulta & "                  ELSE" & vbCrLf
sConsulta = sConsulta & "                         '(' + C1.COD + ' - ' + C2.COD + ' - ' + C3.COD + ' - ' + C4.COD + ') ' + C4.DEN" & vbCrLf
sConsulta = sConsulta & "                  END" & vbCrLf
sConsulta = sConsulta & "             ELSE" & vbCrLf
sConsulta = sConsulta & "                  '(' + C1.COD + ' - ' + C2.COD + ' - ' + C3.COD + ') ' + C3.DEN" & vbCrLf
sConsulta = sConsulta & "             END" & vbCrLf
sConsulta = sConsulta & "        ELSE" & vbCrLf
sConsulta = sConsulta & "             '(' + C1.COD + ' - ' + C2.COD + ') ' + C2.DEN" & vbCrLf
sConsulta = sConsulta & "        END" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "         '(' + C1.COD + ') ' + C1.DEN" & vbCrLf
sConsulta = sConsulta & "   END CATEGORIADEN" & vbCrLf
sConsulta = sConsulta & "   FROM LINEAS_PEDIDO" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PEDIDO ON PEDIDO.ID=LINEAS_PEDIDO.PEDIDO AND PEDIDO.PER=@APROVISIONADOR" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN ITEM ON LINEAS_PEDIDO.ANYO = ITEM.ANYO" & vbCrLf
sConsulta = sConsulta & "   AND LINEAS_PEDIDO.GMN1=ITEM.GMN1 AND LINEAS_PEDIDO.PROCE=ITEM.PROCE " & vbCrLf
sConsulta = sConsulta & "   AND LINEAS_PEDIDO.ITEM=ITEM.ID" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN ART4 ON LINEAS_PEDIDO.GMN1=ART4.GMN1 AND LINEAS_PEDIDO.GMN2=ART4.GMN2 " & vbCrLf
sConsulta = sConsulta & "   AND LINEAS_PEDIDO.GMN3=ART4.GMN3 AND LINEAS_PEDIDO.GMN4=ART4.GMN4" & vbCrLf
sConsulta = sConsulta & "   AND LINEAS_PEDIDO.ART_INT=ART4.COD" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN ART4 ART_AUX ON ITEM.ART=ART_AUX.COD AND ART_AUX.GMN1=ITEM.GMN1 " & vbCrLf
sConsulta = sConsulta & "   AND ART_AUX.GMN2=ITEM.GMN2 AND ART_AUX.GMN3=ITEM.GMN3 AND ART_AUX.GMN4=ITEM.GMN4" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN CATN1 C1 ON C1.ID=LINEAS_PEDIDO.CAT1" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN CATN2 C2 ON C2.ID=LINEAS_PEDIDO.CAT2" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN CATN3 C3 ON C3.ID=LINEAS_PEDIDO.CAT3" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN CATN4 C4 ON C4.ID=LINEAS_PEDIDO.CAT4" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN CATN5 C5 ON C5.ID=LINEAS_PEDIDO.CAT5" & vbCrLf
sConsulta = sConsulta & "   WHERE" & vbCrLf
sConsulta = sConsulta & "   LINEAS_PEDIDO.FECEMISION >= '' + CONVERT(DATETIME,@DESDEFECHA,103) + ''" & vbCrLf
sConsulta = sConsulta & "   AND LINEAS_PEDIDO.FECEMISION <= '' + CONVERT(DATETIME,@HASTAFECHA,103) + ''" & vbCrLf
sConsulta = sConsulta & "   AND LINEAS_PEDIDO.NIVEL_APROB=-1" & vbCrLf
sConsulta = sConsulta & "   ORDER BY CATEGORIA,ARTICULO,FECHA" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SELECT DISTINCT LINEAS_PEDIDO.ID,LINEAS_PEDIDO.ART_INT + ITEM.ART AS ARTICULO," & vbCrLf
sConsulta = sConsulta & "   ART4.DEN + ITEM.DESCR  + ART_AUX.DEN + LINEAS_PEDIDO.DESCR_LIBRE AS DESCR," & vbCrLf
sConsulta = sConsulta & "   LINEAS_PEDIDO.FECEMISION AS FECHA ,LINEAS_PEDIDO.CANT_PED AS CANTIDAD,LINEAS_PEDIDO.UP AS UNIDAD," & vbCrLf
sConsulta = sConsulta & "   LINEAS_PEDIDO.DEST AS DESTINO," & vbCrLf
sConsulta = sConsulta & "   LINEAS_PEDIDO.PREC_UP*LINEAS_PEDIDO.CANT_PED AS IMPORTE," & vbCrLf
sConsulta = sConsulta & "   CASE WHEN C2.COD IS NOT NULL THEN" & vbCrLf
sConsulta = sConsulta & "        CASE WHEN C3.COD IS NOT NULL THEN " & vbCrLf
sConsulta = sConsulta & "             CASE WHEN C4.COD IS NOT NULL THEN " & vbCrLf
sConsulta = sConsulta & "                  CASE WHEN C5.COD IS NOT NULL THEN " & vbCrLf
sConsulta = sConsulta & "                         C1.COD + ' - ' + C2.COD + ' - ' + C3.COD + ' - ' + C4.COD + ' - ' + C5.COD" & vbCrLf
sConsulta = sConsulta & "                  ELSE" & vbCrLf
sConsulta = sConsulta & "                         C1.COD + ' - ' + C2.COD + ' - ' + C3.COD + ' - ' + C4.COD" & vbCrLf
sConsulta = sConsulta & "                  END" & vbCrLf
sConsulta = sConsulta & "             ELSE" & vbCrLf
sConsulta = sConsulta & "                  C1.COD + ' - ' + C2.COD + ' - ' + C3.COD" & vbCrLf
sConsulta = sConsulta & "             END" & vbCrLf
sConsulta = sConsulta & "        ELSE" & vbCrLf
sConsulta = sConsulta & "             C1.COD + ' - ' + C2.COD" & vbCrLf
sConsulta = sConsulta & "        END" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "         C1.COD" & vbCrLf
sConsulta = sConsulta & "   END CATEGORIA," & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   CASE WHEN C2.COD IS NOT NULL THEN" & vbCrLf
sConsulta = sConsulta & "        CASE WHEN C3.COD IS NOT NULL THEN " & vbCrLf
sConsulta = sConsulta & "             CASE WHEN C4.COD IS NOT NULL THEN " & vbCrLf
sConsulta = sConsulta & "                  CASE WHEN C5.COD IS NOT NULL THEN " & vbCrLf
sConsulta = sConsulta & "                         '(' + C1.COD + ' - ' + C2.COD + ' - ' + C3.COD + ' - ' + C4.COD + ' - ' + C5.COD + ')' + C5.DEN" & vbCrLf
sConsulta = sConsulta & "                  ELSE" & vbCrLf
sConsulta = sConsulta & "                         '(' + C1.COD + ' - ' + C2.COD + ' - ' + C3.COD + ' - ' + C4.COD + ')' + C4.DEN" & vbCrLf
sConsulta = sConsulta & "                  END" & vbCrLf
sConsulta = sConsulta & "             ELSE" & vbCrLf
sConsulta = sConsulta & "                  '(' + C1.COD + ' - ' + C2.COD + ' - ' + C3.COD + ')' + C3.DEN" & vbCrLf
sConsulta = sConsulta & "             END" & vbCrLf
sConsulta = sConsulta & "        ELSE" & vbCrLf
sConsulta = sConsulta & "             '(' + C1.COD + ' - ' + C2.COD + ')' + C2.DEN" & vbCrLf
sConsulta = sConsulta & "        END" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "         '(' + C1.COD + ')' + C1.DEN" & vbCrLf
sConsulta = sConsulta & "   END CATEGORIADEN" & vbCrLf
sConsulta = sConsulta & "   FROM LINEAS_PEDIDO" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PEDIDO ON PEDIDO.ID=LINEAS_PEDIDO.PEDIDO AND PEDIDO.PER=@APROVISIONADOR" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN APROB_LIM ON APROB_LIM.SEGURIDAD=LINEAS_PEDIDO.SEGURIDAD AND APROB_LIM.PER=@APROBADOR" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN ITEM ON LINEAS_PEDIDO.ANYO = ITEM.ANYO" & vbCrLf
sConsulta = sConsulta & "   AND LINEAS_PEDIDO.GMN1=ITEM.GMN1 AND LINEAS_PEDIDO.PROCE=ITEM.PROCE " & vbCrLf
sConsulta = sConsulta & "   AND LINEAS_PEDIDO.ITEM=ITEM.ID" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN ART4 ON LINEAS_PEDIDO.GMN1=ART4.GMN1 AND LINEAS_PEDIDO.GMN2=ART4.GMN2 " & vbCrLf
sConsulta = sConsulta & "   AND LINEAS_PEDIDO.GMN3=ART4.GMN3 AND LINEAS_PEDIDO.GMN4=ART4.GMN4" & vbCrLf
sConsulta = sConsulta & "   AND LINEAS_PEDIDO.ART_INT=ART4.COD" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN ART4 ART_AUX ON ITEM.ART=ART_AUX.COD AND ART_AUX.GMN1=ITEM.GMN1 " & vbCrLf
sConsulta = sConsulta & "   AND ART_AUX.GMN2=ITEM.GMN2 AND ART_AUX.GMN3=ITEM.GMN3 AND ART_AUX.GMN4=ITEM.GMN4" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN CATN1 C1 ON C1.ID=LINEAS_PEDIDO.CAT1" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN CATN2 C2 ON C2.ID=LINEAS_PEDIDO.CAT2" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN CATN3 C3 ON C3.ID=LINEAS_PEDIDO.CAT3" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN CATN4 C4 ON C4.ID=LINEAS_PEDIDO.CAT4" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN CATN5 C5 ON C5.ID=LINEAS_PEDIDO.CAT5" & vbCrLf
sConsulta = sConsulta & "   WHERE" & vbCrLf
sConsulta = sConsulta & "   LINEAS_PEDIDO.FECEMISION >= '' + CONVERT(DATETIME,@DESDEFECHA,103) + ''" & vbCrLf
sConsulta = sConsulta & "   AND LINEAS_PEDIDO.FECEMISION <= '' + CONVERT(DATETIME,@HASTAFECHA,103) + ''" & vbCrLf
sConsulta = sConsulta & "   AND LINEAS_PEDIDO.NIVEL_APROB=-1" & vbCrLf
sConsulta = sConsulta & "   ORDER BY CATEGORIA,ARTICULO,FECHA" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub


Public Function CodigoDeActualizacion2_12_05_07A2_12_05_08() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim bBool As Boolean

On Error GoTo Error
    

sConsulta = "BEGIN TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = True
ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

frmProgreso.lblDetalle = "Actualizaci�n de tablas"
frmProgreso.lblDetalle.Refresh
frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_12_5_Tablas_8

frmProgreso.lblDetalle = "Actualizaci�n de storeds"
frmProgreso.lblDetalle.Refresh
frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_12_5_Storeds_8_1

frmProgreso.lblDetalle = "Continua la actualizaci�n de storeds"
frmProgreso.lblDetalle.Refresh
frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_12_5_Storeds_8_2

frmProgreso.lblDetalle = "Actualizaci�n de datos"
frmProgreso.lblDetalle.Refresh
frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ORDEN_ENTREGA_TG_INSUPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[ORDEN_ENTREGA_TG_INSUPD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "UPDATE ORDEN_ENTREGA SET MON=(SELECT MONCEN FROM PARGEN_DEF WHERE ID=1),CAMBIO=1 WHERE MON IS NULL"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TRIGGER ORDEN_ENTREGA_TG_INSUPD ON dbo.ORDEN_ENTREGA" & vbCrLf
sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_ORDEN_ENTREGA_Ins CURSOR FOR SELECT ID FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_ORDEN_ENTREGA_Ins" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_ORDEN_ENTREGA_Ins INTO @ID" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "UPDATE ORDEN_ENTREGA SET FECACT=GETDATE() WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_ORDEN_ENTREGA_Ins INTO @ID" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_ORDEN_ENTREGA_Ins" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_ORDEN_ENTREGA_Ins" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "DELETE FROM DIC WHERE NOMBRE='CON'"
ExecuteSQL gRDOCon, sConsulta
sConsulta = "DELETE FROM DIC WHERE NOMBRE='DIC'"
ExecuteSQL gRDOCon, sConsulta
sConsulta = "DELETE FROM DIC WHERE NOMBRE='GRUPOPLANT'"
ExecuteSQL gRDOCon, sConsulta
sConsulta = "DELETE FROM DIC WHERE NOMBRE='CONTR'"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "UPDATE VERSION SET NUM ='2.12.05.08'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
ExecuteSQL gRDOCon, sConsulta


sConsulta = "COMMIT TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = False

CodigoDeActualizacion2_12_05_07A2_12_05_08 = True
Exit Function
    
Error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_12_05_07A2_12_05_08 = False

End Function

Private Sub V_2_12_5_Tablas_8()
Dim sConsulta As String

sConsulta = "ALTER TABLE dbo.PARINS_GEST ADD" & vbCrLf
sConsulta = sConsulta & "   NUMDECOFE smallint NOT NULL CONSTRAINT DF_PARINS_GEST_NUMDECOFE DEFAULT (2)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TABLE [dbo].[PROCE_NUMDEC] (" & vbCrLf
sConsulta = sConsulta & "   [ANYO] [smallint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [GMN1] [varchar] (3) NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [PROCE] [smallint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [USU] [varchar] (20) NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [DEC_OFE] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [FECACT] [datetime] NULL " & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE [dbo].[PROCE_NUMDEC] WITH NOCHECK ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [PK_PROCE_NUMDEC] PRIMARY KEY  CLUSTERED " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [ANYO]," & vbCrLf
sConsulta = sConsulta & "       [GMN1]," & vbCrLf
sConsulta = sConsulta & "       [PROCE]," & vbCrLf
sConsulta = sConsulta & "       [USU]" & vbCrLf
sConsulta = sConsulta & "   )  ON [PRIMARY] " & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE [dbo].[PROCE_NUMDEC] ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_PROCE_NUMDEC_DEC_OFE] DEFAULT (2) FOR [DEC_OFE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "SET QUOTED_IDENTIFIER ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TRIGGER PROCE_NUMDEC_TG_INSUPD ON dbo.PROCE_NUMDEC" & vbCrLf
sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO SMALLINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCE SMALLINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN1 VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @USU VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_PROCE_NUMDEC_Ins CURSOR LOCAL FOR SELECT ANYO,GMN1,PROCE,USU FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_PROCE_NUMDEC_Ins" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_NUMDEC_Ins INTO @ANYO,@GMN1,@PROCE,@USU" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_NUMDEC SET FECACT=GETDATE() WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND USU=@USU" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_NUMDEC_Ins INTO @ANYO,@GMN1,@PROCE,@USU" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_PROCE_NUMDEC_Ins" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_PROCE_NUMDEC_Ins" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 
sConsulta = "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta


End Sub

Private Sub V_2_12_5_Storeds_8_1()
Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GMN1_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[GMN1_COD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE GMN1_COD (@OLD VARCHAR(50),@NEW VARCHAR(50))  AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT ON" & vbCrLf
sConsulta = sConsulta & "BEGIN TRANSACTION" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR FOR SELECT COD FROM GMN1 WHERE COD=@OLD" & vbCrLf
sConsulta = sConsulta & "OPEN C" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C INTO @VAR_COD" & vbCrLf
sConsulta = sConsulta & "IF @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART1_ADJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART2_ADJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART3_ADJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ADJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE DETALLE_PUJAS NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ADJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ADJREU NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ESP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OBJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OFE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OFE_WEB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRES NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESCON NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESPROY NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESCON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESCON4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFEMIN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_GRUPO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_ESP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_WEB_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_WEB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PER NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_PET NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_POT NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVENOTIF NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE REU_PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ESP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4_ACT NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATALOG_LIN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_PEDIDO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART_UP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_DEF NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO_DEF NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO_ESP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_GR_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_GR_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ITEM_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ITEM_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GRUPO_OFE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GRUPO_OFE_REU NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USAR_GR_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_CONTR NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE SOBRE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FAVORITOS_LINEAS_PEDIDO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_NUMDEC NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN1 SET COD=@NEW WHERE COD=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART1_ADJ SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART2_ADJ SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART3_ADJ SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4_ADJ SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE BLOQUEO_PROCE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE COM_GMN1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE COM_GMN2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE COM_GMN3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE COM_GMN4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE DETALLE_PUJAS SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_ADJ SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_ADJREU SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_ESP SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_OBJ SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_OFE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_OFE_WEB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_PRES SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_PRESCON SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_PRESCON3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_PRESCON4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_PRESPROY SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_UON1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_UON2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_UON3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFEMIN SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES_MAT1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES_MAT2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES_MAT3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES_MAT4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_ESP SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE_ADJUN SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE_WEB_ADJUN SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE_WEB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PER SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE_PET SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE_POT SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_GMN1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_GMN2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_GMN3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_GMN4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVENOTIF SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE REU_PROCE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_PROCE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_PROVE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_GRUPO SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_UON1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_UON2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_UON3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_PRESCON3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_PRESCON4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_PROVE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4_ESP  SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN4_ACT SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART4_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN4_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4_ATRIB  SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CATALOG_LIN  SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE LINEAS_PEDIDO  SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART_UP  SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_DEF SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GRUPO SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GRUPO_DEF SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GRUPO_ESP SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PRES1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PRES2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PRES3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PRES4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_PRES1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_PRES2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_PRES3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_PRES4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_UON1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_UON2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_UON3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_UON1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_UON2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_UON3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_GR_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_GR_ADJUN SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_ITEM_ADJUN SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_ITEM_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ALL SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ALL_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ALL_PROVE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_GRUPO SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_GRUPO_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_GRUPO_PROVE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ITEM SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ITEM_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ITEM_PROVE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_PROCE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_PROCE_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_PROCE_PROVE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GRUPO_OFE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GRUPO_OFE_REU SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE USAR_GR_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_CONTR SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE SOBRE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART4_ADJUN SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE FAVORITOS_LINEAS_PEDIDO SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_NUMDEC SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART1_ADJ CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART2_ADJ CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART3_ADJ CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ADJ CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE DETALLE_PUJAS CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ADJ CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ADJREU CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ESP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OBJ CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OFE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OFE_WEB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRES CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESCON CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESCON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESCON4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESPROY CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFEMIN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_ESP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_WEB_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_WEB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PER CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_PET CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_POT CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVENOTIF CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE REU_PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_GRUPO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ESP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4_ACT CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATALOG_LIN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_PEDIDO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART_UP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_DEF CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO_DEF CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO_ESP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_GR_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_GR_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ITEM_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ITEM_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GRUPO_OFE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GRUPO_OFE_REU CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USAR_GR_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_CONTR CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE SOBRE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FAVORITOS_LINEAS_PEDIDO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_NUMDEC CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close C" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "COMMIT TRANSACTION" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT OFF" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PROCE_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[PROCE_COD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE PROCE_COD (@OLD SMALLINT,@NEW SMALLINT,@ANYO INT,@GMN1 VARCHAR (50))  AS " & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_COD SMALLINT " & vbCrLf
sConsulta = sConsulta & "DECLARE @AN AS INT " & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN AS VARCHAR(50) " & vbCrLf
sConsulta = sConsulta & "BEGIN " & vbCrLf
sConsulta = sConsulta & "DECLARE D CURSOR FOR SELECT ANYO,GMN1,COD FROM PROCE WHERE ANYO=@ANYO AND COD=@OLD AND GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "OPEN D " & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM D INTO @AN,@GMN,@VAR_COD " & vbCrLf
sConsulta = sConsulta & "IF @@FETCH_STATUS=0 " & vbCrLf
sConsulta = sConsulta & "BEGIN " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART1_ADJ NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART2_ADJ NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART3_ADJ NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ADJ NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ADJ NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ADJREU NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ESP NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OBJ NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OFE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OFE_WEB NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRES NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESCON NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESCON3 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESCON4 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESPROY NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON1 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON2 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON3 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFEMIN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE DETALLE_PUJAS NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVENOTIF NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATALOG_LIN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_PEDIDO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_ESP NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_ADJUN NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_WEB_ADJUN NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_WEB NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PER NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_PET NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_POT NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE REU_PROCE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES1 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES2 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES3 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES4 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON1 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON2 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON3 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO_ESP NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO_DEF NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES1 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES2 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES3 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES4 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON1 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON2 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON3 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_ATRIB NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_DEF NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL_ATRIB NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL_PROVE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_ATRIB NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_PROVE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_ATRIB NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_PROVE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_ATRIB NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_PROVE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON1 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON2 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON3 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON4 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY1 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY2 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY3 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY4 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON1 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON2 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON3 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON3 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON4 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PROVE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_GRUPO NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROCE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROVE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GRUPO_OFE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GRUPO_OFE_REU NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_GR_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ITEM_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_GR_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ITEM_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USAR_GR_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_CONTR NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE SOBRE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FAVORITOS_LINEAS_PEDIDO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_NUMDEC NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE ART1_ADJ SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ART2_ADJ SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ART3_ADJ SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ART4_ADJ SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_ADJ SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_ADJREU SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_ESP SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_OBJ SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_OFE SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_OFE_WEB SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_PRES SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_UON1 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_UON2 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_UON3 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_PRESCON SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_PRESCON3 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_PRESCON4 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_PRESPROY SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE OFEMIN SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE DETALLE_PUJAS SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVENOTIF SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CATALOG_LIN SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE LINEAS_PEDIDO SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_ESP SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE_ADJUN SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE_WEB_ADJUN SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE_WEB SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PER SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE_PET SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE_POT SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE REU_PROCE SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_PRES1 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_PRES2 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_PRES3 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_PRES4 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_UON1 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_UON2 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_UON3 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GRUPO_ESP SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GRUPO_DEF SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GRUPO SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PRES1 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PRES2 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PRES3 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PRES4 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_UON1 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_UON2 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_UON3 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_ATRIB SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_DEF SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ALL SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ALL_ATRIB SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ALL_PROVE SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_PROCE SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_PROCE_ATRIB SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_PROCE_PROVE SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_GRUPO SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_GRUPO_ATRIB SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_GRUPO_PROVE SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ITEM SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ITEM_ATRIB SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ITEM_PROVE SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON1 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON2 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON3 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON4 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY1 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY2 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY3 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY4 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_1 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_2 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_3 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_4 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_1 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_2 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_3 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_4 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_UON1 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_UON2 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD  " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_UON3 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_PRESCON3 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_PRESCON4 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_PROVE SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_GRUPO SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_PROVE SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_PROCE SET COD=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE GRUPO_OFE SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GRUPO_OFE_REU SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_ATRIB SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_GR_ATRIB SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_ITEM_ATRIB SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_GR_ADJUN SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_ITEM_ADJUN SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GRUPO SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET COD=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE BLOQUEO_PROCE SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE USAR_GR_ATRIB SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_CONTR SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE SOBRE SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE FAVORITOS_LINEAS_PEDIDO SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_NUMDEC SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART1_ADJ CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART2_ADJ CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART3_ADJ CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ADJ CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ADJ CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESCON CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESCON3 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESCON4 CHECK CONSTRAINT ALL  " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESPROY CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ADJREU CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ESP CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OBJ CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OFE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OFE_WEB CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRES CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON1 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON2 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON3 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFEMIN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE DETALLE_PUJAS CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVENOTIF CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATALOG_LIN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_PEDIDO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_ESP CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_ADJUN CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_WEB_ADJUN CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_WEB CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PER CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_PET CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_POT CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE REU_PROCE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES1 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES2 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES3 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES4 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON1 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON2 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON3 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO_ESP CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO_DEF CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES1 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES2 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES3 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES4 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON1 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON2 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON3 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_ATRIB CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_DEF CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL_ATRIB CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL_PROVE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_ATRIB CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_PROVE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_ATRIB CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_PROVE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_ATRIB CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_PROVE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON1 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON2 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON3 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON4 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY1 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY2 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY3 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY4 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON1 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON2 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON3 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON3 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON4 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PROVE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_GRUPO CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROCE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROVE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GRUPO_OFE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GRUPO_OFE_REU CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_GR_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ITEM_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_GR_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ITEM_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USAR_GR_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_CONTR CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE SOBRE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FAVORITOS_LINEAS_PEDIDO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_NUMDEC CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "End " & vbCrLf
sConsulta = sConsulta & "Close D " & vbCrLf
sConsulta = sConsulta & "DEALLOCATE D " & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USU_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[USU_COD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE USU_COD (@OLD VARCHAR(50),@NEW VARCHAR(50),@PWD VARCHAR(512))  AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT ON" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR FOR SELECT COD FROM USU WHERE COD=@OLD" & vbCrLf
sConsulta = sConsulta & "OPEN C" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C INTO @VAR_COD" & vbCrLf
sConsulta = sConsulta & "IF @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU_ACC NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU_LIS NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTA_INICIAL_PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTA_INICIAL_GRUPO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LOG NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_CONTR NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE SOBRE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PARINS_DEF NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PARINS_DOT NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PARINS_GEST NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PARINS_PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FAVORITOS_ORDEN_ENTREGA NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FAVORITOS_ORDEN_ENTREGA_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FAVORITOS_LINEAS_PEDIDO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FAVORITOS_LINEAS_PEDIDO_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_NUMDEC NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE USU SET COD=@NEW,PWD=@PWD WHERE COD=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE USU_ACC SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE USU_LIS SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET USUVAL=@NEW WHERE USUVAL=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET USUVALSELPROVE=@NEW WHERE USUVALSELPROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET USUVALCIERRE=@NEW WHERE USUVALCIERRE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET USUAPER=@NEW WHERE USUAPER=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_PROCE SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_PROCE_PROVE SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_PROCE_ATRIB SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_GRUPO SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_GRUPO_PROVE SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_GRUPO_ATRIB SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ITEM SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ITEM_PROVE SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ITEM_ATRIB SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ALL SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ALL_PROVE SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ALL_ATRIB SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTA_INICIAL_PROCE SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTA_INICIAL_GRUPO SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_CONTR SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE SOBRE SET USUAPER=@NEW WHERE USUAPER=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PARINS_DEF SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PARINS_DOT SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PARINS_GEST SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PARINS_PROCE SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_NUMDEC SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU_ACC CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU_LIS CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTA_INICIAL_PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTA_INICIAL_GRUPO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LOG CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_CONTR CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE SOBRE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PARINS_DEF CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PARINS_DOT CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PARINS_GEST CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PARINS_PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_NUMDEC CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close C" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT OFF" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Private Sub V_2_12_5_Storeds_8_2()
Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USU_LOGIN]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[USU_LOGIN]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE USU_LOGIN(@ADM INT,@USUOLD VARCHAR(50),@PWDOLD VARCHAR(512),@USUNEW VARCHAR(50),@PWDNEW VARCHAR(512),@FECPWD DATETIME,@CHANGE tinyint,@MAIL VARCHAR(200)=NULL) AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_PWD VARCHAR(512)" & vbCrLf
sConsulta = sConsulta & "DECLARE @ERROR INT" & vbCrLf
sConsulta = sConsulta & "--SET XACT_ABORT ON" & vbCrLf
sConsulta = sConsulta & "--BEGIN TRANSACTION" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @ERROR=0" & vbCrLf
sConsulta = sConsulta & "IF @ADM=1" & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR FOR SELECT PWD FROM ADM WHERE USU=@USUOLD" & vbCrLf
sConsulta = sConsulta & "Else" & vbCrLf
sConsulta = sConsulta & "IF @ADM=2" & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR FOR SELECT PWD FROM OBJSADM WHERE USU=@USUOLD" & vbCrLf
sConsulta = sConsulta & "Else" & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR FOR SELECT PWD FROM USU WHERE COD=@USUOLD" & vbCrLf
sConsulta = sConsulta & "OPEN C" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C INTO @VAR_PWD" & vbCrLf
sConsulta = sConsulta & "IF @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @VAR_PWD<>@PWDOLD" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "--SET XACT_ABORT OFF" & vbCrLf
sConsulta = sConsulta & "--ROLLBACK TRANSACTION" & vbCrLf
sConsulta = sConsulta & "RAISERROR('Incorrect password.',16,1)" & vbCrLf
sConsulta = sConsulta & "SET @ERROR=1" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Else" & vbCrLf
sConsulta = sConsulta & "  " & vbCrLf
sConsulta = sConsulta & "IF @ADM=1" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "   ALTER TABLE ADM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "   UPDATE ADM SET USU=@USUNEW WHERE USU=@USUOLD" & vbCrLf
sConsulta = sConsulta & "   ALTER TABLE ADM CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "   UPDATE ADM SET PWD=@PWDNEW,FECPWD=@FECPWD,EMAIL=@MAIL WHERE USU=@USUNEW" & vbCrLf
sConsulta = sConsulta & "   ALTER TABLE CONF_VISTAS_PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "   UPDATE CONF_VISTAS_PROCE SET USU=@USUNEW WHERE USU=@USUOLD" & vbCrLf
sConsulta = sConsulta & "   ALTER TABLE CONF_VISTAS_PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "   ALTER TABLE CONF_VISTAS_PROCE_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "   UPDATE CONF_VISTAS_PROCE_PROVE SET USU=@USUNEW WHERE USU=@USUOLD" & vbCrLf
sConsulta = sConsulta & "   ALTER TABLE CONF_VISTAS_PROCE_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "   ALTER TABLE CONF_VISTAS_PROCE_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "   UPDATE CONF_VISTAS_PROCE_ATRIB SET USU=@USUNEW WHERE USU=@USUOLD" & vbCrLf
sConsulta = sConsulta & "   ALTER TABLE CONF_VISTAS_PROCE_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "   ALTER TABLE CONF_VISTAS_GRUPO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "   UPDATE CONF_VISTAS_GRUPO SET USU=@USUNEW WHERE USU=@USUOLD" & vbCrLf
sConsulta = sConsulta & "   ALTER TABLE CONF_VISTAS_GRUPO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "   ALTER TABLE CONF_VISTAS_GRUPO_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "   UPDATE CONF_VISTAS_GRUPO_PROVE SET USU=@USUNEW WHERE USU=@USUOLD" & vbCrLf
sConsulta = sConsulta & "   ALTER TABLE CONF_VISTAS_GRUPO_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "   ALTER TABLE CONF_VISTAS_GRUPO_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "   UPDATE CONF_VISTAS_GRUPO_ATRIB SET USU=@USUNEW WHERE USU=@USUOLD" & vbCrLf
sConsulta = sConsulta & "   ALTER TABLE CONF_VISTAS_GRUPO_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "   ALTER TABLE CONF_VISTAS_ITEM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "   UPDATE CONF_VISTAS_ITEM SET USU=@USUNEW WHERE USU=@USUOLD" & vbCrLf
sConsulta = sConsulta & "   ALTER TABLE CONF_VISTAS_ITEM CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "   ALTER TABLE CONF_VISTAS_ITEM_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "   UPDATE CONF_VISTAS_ITEM_PROVE SET USU=@USUNEW WHERE USU=@USUOLD" & vbCrLf
sConsulta = sConsulta & "   ALTER TABLE CONF_VISTAS_ITEM_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "   ALTER TABLE CONF_VISTAS_ITEM_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "   UPDATE CONF_VISTAS_ITEM_ATRIB SET USU=@USUNEW WHERE USU=@USUOLD" & vbCrLf
sConsulta = sConsulta & "   ALTER TABLE CONF_VISTAS_ITEM_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "   ALTER TABLE CONF_VISTAS_ALL NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "   UPDATE CONF_VISTAS_ALL SET USU=@USUNEW WHERE USU=@USUOLD" & vbCrLf
sConsulta = sConsulta & "   ALTER TABLE CONF_VISTAS_ALL CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "   ALTER TABLE CONF_VISTAS_ALL_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "   UPDATE CONF_VISTAS_ALL_ATRIB SET USU=@USUNEW WHERE USU=@USUOLD" & vbCrLf
sConsulta = sConsulta & "   ALTER TABLE CONF_VISTAS_ALL_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "   ALTER TABLE PARINS_DEF NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "   UPDATE PARINS_DEF SET USU=@USUNEW WHERE USU=@USUOLD" & vbCrLf
sConsulta = sConsulta & "   ALTER TABLE PARINS_DEF CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "   ALTER TABLE PARINS_GEST NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "   UPDATE PARINS_GEST SET USU=@USUNEW WHERE USU=@USUOLD" & vbCrLf
sConsulta = sConsulta & "   ALTER TABLE PARINS_GEST CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "   ALTER TABLE PARINS_DOT NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "   UPDATE PARINS_DOT SET USU=@USUNEW WHERE USU=@USUOLD" & vbCrLf
sConsulta = sConsulta & "   ALTER TABLE PARINS_DOT CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "   ALTER TABLE PARINS_PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "   UPDATE PARINS_PROCE SET USU=@USUNEW WHERE USU=@USUOLD" & vbCrLf
sConsulta = sConsulta & "   ALTER TABLE PARINS_PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "   UPDATE PROCE_NUMDEC SET USU=@USUNEW WHERE USU=@USUOLD" & vbCrLf
sConsulta = sConsulta & "   ALTER TABLE PROCE_NUMDEC CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   End" & vbCrLf
sConsulta = sConsulta & "Else" & vbCrLf
sConsulta = sConsulta & "   IF @ADM=2" & vbCrLf
sConsulta = sConsulta & "     BEGIN" & vbCrLf
sConsulta = sConsulta & "     ALTER TABLE OBJSADM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "     UPDATE OBJSADM SET USU=@USUNEW WHERE USU=@USUOLD" & vbCrLf
sConsulta = sConsulta & "     ALTER TABLE OBJSADM CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "     UPDATE OBJSADM SET PWD=@PWDNEW,FECPWD=@FECPWD WHERE USU=@USUNEW" & vbCrLf
sConsulta = sConsulta & "     End" & vbCrLf
sConsulta = sConsulta & "   Else" & vbCrLf
sConsulta = sConsulta & "        IF @CHANGE=1" & vbCrLf
sConsulta = sConsulta & "             UPDATE USU SET PWD=@PWDNEW,FECPWD=@FECPWD,CHANGEPWD=0 WHERE COD=@USUNEW " & vbCrLf
sConsulta = sConsulta & "       ELSE" & vbCrLf
sConsulta = sConsulta & "             UPDATE USU SET PWD=@PWDNEW,FECPWD=@FECPWD WHERE COD=@USUNEW" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "IF @ERROR=0 CLOSE C" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "--IF @ERROR=0" & vbCrLf
sConsulta = sConsulta & "--COMMIT TRANSACTION" & vbCrLf
sConsulta = sConsulta & "--SET XACT_ABORT OFF" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[MON_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[MON_COD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE MON_COD (@OLD VARCHAR(50),@NEW VARCHAR(50))  AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR FOR SELECT COD FROM MON WHERE COD=@OLD" & vbCrLf
sConsulta = sConsulta & "OPEN C" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C INTO @VAR_COD" & vbCrLf
sConsulta = sConsulta & "IF @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE MON NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PAI NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_WEB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PARGEN_DEF NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ORDEN_ENTREGA NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LOG_ORDEN_ENTREGA NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "UPDATE MON SET COD=@NEW WHERE COD=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PAI SET MON=@NEW WHERE MON=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE SET MON=@NEW WHERE MON=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET MON=@NEW WHERE MON=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE SET MON=@NEW WHERE MON=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE_WEB SET MON=@NEW WHERE MON=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PARGEN_DEF SET MONCEN=@NEW WHERE MONCEN=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE USU SET MON=@NEW WHERE MON=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ORDEN_ENTREGA SET MON=@NEW WHERE MON=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE LOG_ORDEN_ENTREGA SET MON=@NEW WHERE MON=@OLD" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE MON CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PAI CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_WEB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PARGEN_DEF CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ORDEN_ENTREGA CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LOG_ORDEN_ENTREGA CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close C" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PROVI_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[PROVI_COD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE PROVI_COD (@OLD VARCHAR(50),@NEW VARCHAR(50), @PAI VARCHAR(50))  AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR FOR SELECT COD FROM PROVI WHERE PAI=@PAI AND COD=@OLD" & vbCrLf
sConsulta = sConsulta & "OPEN C" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C INTO @VAR_COD" & vbCrLf
sConsulta = sConsulta & "IF @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVI NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE DEST NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PARGEN_DEF NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVI SET COD=@NEW WHERE PAI=@PAI AND COD=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE DEST SET PROVI=@NEW WHERE PAI=@PAI AND PROVI=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE SET PROVI=@NEW WHERE PAI=@PAI AND PROVI=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PARGEN_DEF SET PROVIDEF=@NEW WHERE PAIDEF=@PAI AND PROVIDEF=@OLD" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVI CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE DEST CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PARGEN_DEF CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close C" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PAI_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[PAI_COD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE PAI_COD (@OLD VARCHAR(50),@NEW VARCHAR(50))  AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR FOR SELECT COD FROM PAI WHERE COD=@OLD" & vbCrLf
sConsulta = sConsulta & "OPEN C" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C INTO @VAR_COD" & vbCrLf
sConsulta = sConsulta & "IF @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PAI NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVI NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE DEST NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PARGEN_DEF NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "UPDATE PAI SET COD=@NEW WHERE COD=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVI SET PAI=@NEW WHERE PAI=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE DEST SET PAI=@NEW WHERE PAI=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE SET PAI=@NEW WHERE PAI=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PARGEN_DEF SET PAIDEF=@NEW WHERE PAIDEF=@OLD" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PAI CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVI CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE DEST CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PARGEN_DEF CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close C" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_DEVOLVER_TODAS_ORDENES]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_DEVOLVER_TODAS_ORDENES]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "CREATE PROCEDURE FSEP_DEVOLVER_TODAS_ORDENES @MAXLINEAS integer, @DESDEID varchar(50)=null, @APROBADOR varchar(50) = null,@APROVISIONADOR varchar(50) = null, @ARTINT varchar(50) = null, @ARTDEN varchar(500) = null, @ARTEXT varchar(500) = null, @DESDEFECHA varchar(10) = null, @HASTAFECHA varchar(10) = null, @PROVECOD varchar(50) = null, @PROVEDEN varchar(500) = null,@NUMPEDPROVE varchar(100) = null,@ANYO int = null,@PEDIDO int = null,@ORDEN int = null,@EST int  = null, @ID int = null ,@RECINCORRECTO int = null ,@COINCIDENCIA int =null,@REFERENCIA varchar(120) = null ,@CAMPO1 int = null ,@CAMPO2 int = null, @VALORCAMPO1 varchar(150) = null, @VALORCAMPO2 varchar(150)=null , @CATN1 int = null, @CATN2 int = null, @CATN3 int = null, @CATN4 int = null , @CATN5 int = null AS " & vbCrLf
sConsulta = sConsulta & "declare @SQL nvarchar(4000)" & vbCrLf
sConsulta = sConsulta & "declare @PARAM nvarchar(500)" & vbCrLf
sConsulta = sConsulta & "declare @SQL1 nvarchar(4000)" & vbCrLf
sConsulta = sConsulta & "declare @SQL2 nvarchar(4000)" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF" & vbCrLf
sConsulta = sConsulta & "set  @SQL = N'" & vbCrLf
sConsulta = sConsulta & "SELECT TOP ' + CONVERT(varchar,@MAXLINEAS) + ' CONVERT(VARCHAR,OE.EST) + LEFT(''00000000'',8-LEN(CONVERT(VARCHAR,OE.ID))) + CONVERT(VARCHAR,OE.ID), " & vbCrLf
sConsulta = sConsulta & "       OE.ID, OE.ANYO, PE.NUM NUMPEDIDO, OE.NUM NUMORDEN, OE.PROVE PROVECOD, P.DEN PROVEDEN, " & vbCrLf
sConsulta = sConsulta & "       OE.NUMEXT, OE.FECHA FECHA, OE.EST EST, OE.IMPORTE IMPORTE, " & vbCrLf
sConsulta = sConsulta & "       P1.APE + '', '' + P1.NOM APROVISIONADOR,P1.APE,P1.NOM,OE.OBS," & vbCrLf
sConsulta = sConsulta & "       OES.COMENT, OES.FECHA FECEST,OE.IMPORTE,OE.PEDIDO,OE.TIPO, OE.INCORRECTA, PE.PER,OE.REFERENCIA,T3.ADJUNTOS, CP.COMENT COMENTPROVE, CP.EST ESTCOMENT,OE.MON" & vbCrLf
sConsulta = sConsulta & "  FROM ORDEN_ENTREGA OE" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN PROVE P" & vbCrLf
sConsulta = sConsulta & "            ON OE.PROVE = P.COD" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN PEDIDO PE" & vbCrLf
sConsulta = sConsulta & "            ON OE.PEDIDO = PE.ID " & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN (SELECT OE.ID, OE.PEDIDO, OE.ORDEN, OE.EST, OE.COMENT " & vbCrLf
sConsulta = sConsulta & "                  FROM ORDEN_EST OE " & vbCrLf
sConsulta = sConsulta & "                        INNER JOIN (SELECT PEDIDO, ORDEN, MAX(ID) ID" & vbCrLf
sConsulta = sConsulta & "                                      FROM ORDEN_EST  WHERE EST IN (3, 21) " & vbCrLf
sConsulta = sConsulta & "                                    GROUP BY PEDIDO, ORDEN ) MO " & vbCrLf
sConsulta = sConsulta & "                                ON MO.ID = OE.ID) CP" & vbCrLf
sConsulta = sConsulta & "            ON OE.ID = CP.ORDEN" & vbCrLf
sConsulta = sConsulta & "'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @APROBADOR IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN (SELECT DISTINCT ORDEN , S.APROB_TIPO1 PER" & vbCrLf
sConsulta = sConsulta & "                           FROM LINEAS_PEDIDO LP " & vbCrLf
sConsulta = sConsulta & "                                    INNER JOIN SEGURIDAD S" & vbCrLf
sConsulta = sConsulta & "                                            ON LP.SEGURIDAD = S.ID " & vbCrLf
sConsulta = sConsulta & "                                           AND LP.NIVEL_APROB = 0) T1" & vbCrLf
sConsulta = sConsulta & "            ON OE.ID = T1.ORDEN" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN (SELECT DISTINCT ORDEN , AL.PER PER" & vbCrLf
sConsulta = sConsulta & "                            FROM LINEAS_PEDIDO LP " & vbCrLf
sConsulta = sConsulta & "                                     INNER JOIN APROB_LIM AL" & vbCrLf
sConsulta = sConsulta & "                                             ON LP.APROB_LIM = AL.ID) T2" & vbCrLf
sConsulta = sConsulta & "            ON OE.ID = T2.ORDEN '" & vbCrLf
sConsulta = sConsulta & "set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN PER P1" & vbCrLf
sConsulta = sConsulta & "            ON PE.PER = P1.COD" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN ORDEN_EST OES" & vbCrLf
sConsulta = sConsulta & "            ON OE.ORDEN_EST = OES.ID'" & vbCrLf
sConsulta = sConsulta & "set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "    LEFT JOIN (SELECT COUNT(*) AS ADJUNTOS,ORDEN  FROM orden_entrega_adjun GROUP BY orden_entrega_adjun.ORDEN) T3 ON OE.ID= T3.ORDEN '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if @ARTINT is not null or @ARTDEN is not null or @ARTEXT is not null or @CATN1 is not null or @CATN2 is not null or @CATN3 is not null or @CATN4 is not null or @CATN5 is not null or @CAMPO1 is not null or @CAMPO2 is not null or @VALORCAMPO1 is not null or @VALORCAMPO2 is not null" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "   set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN (SELECT DISTINCT ORDEN" & vbCrLf
sConsulta = sConsulta & "                  FROM LINEAS_PEDIDO LP" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN ITEM I " & vbCrLf
sConsulta = sConsulta & "             ON LP.ANYO = I.ANYO" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN1=I.GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND LP.PROCE=I.PROCE " & vbCrLf
sConsulta = sConsulta & "            AND LP.ITEM=I.ID'" & vbCrLf
sConsulta = sConsulta & "      IF @ARTDEN is NOT NULL " & vbCrLf
sConsulta = sConsulta & "         set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN ART4 " & vbCrLf
sConsulta = sConsulta & "             ON LP.GMN1=ART4.GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN2=ART4.GMN2 " & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN3=ART4.GMN3 " & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN4=ART4.GMN4 " & vbCrLf
sConsulta = sConsulta & "            AND LP.ART_INT=ART4.COD" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN ART4 ART_AUX " & vbCrLf
sConsulta = sConsulta & "             ON ART_AUX.COD=I.ART " & vbCrLf
sConsulta = sConsulta & "            AND ART_AUX.GMN1=I.GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND ART_AUX.GMN2=I.GMN2 " & vbCrLf
sConsulta = sConsulta & "            AND ART_AUX.GMN3=I.GMN3 " & vbCrLf
sConsulta = sConsulta & "            AND ART_AUX.GMN4=I.GMN4'" & vbCrLf
sConsulta = sConsulta & "      IF @ARTEXT is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN PROVE_ART4 PA" & vbCrLf
sConsulta = sConsulta & "             ON LP.PROVE = PA.PROVE" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN1 = PA.GMN1" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN2 = PA.GMN2" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN3 = PA.GMN3" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN4 = PA.GMN4" & vbCrLf
sConsulta = sConsulta & "            AND CASE WHEN LP.ART_INT IS NULL THEN I.ART ELSE LP.ART_INT END = PA.ART'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & " WHERE 1=1'" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   IF @CAMPO1 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQL = @SQL + N'   AND LP.CAMPO1 = @CAMPO1 '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @CAMPO2 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQL = @SQL + N'   AND LP.CAMPO2 = @CAMPO2 '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @VALORCAMPO1 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQL = @SQL + N'   AND LP.VALOR1 = @VALORCAMPO1 '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @VALORCAMPO2 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQL = @SQL + N'   AND LP.VALOR2 = @VALORCAMPO2 '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @CATN1 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQL = @SQL + N'   AND LP.CAT1 = @CATN1 '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @CATN2 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQL = @SQL + N'   AND LP.CAT2 = @CATN2 '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @CATN3 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQL = @SQL + N'   AND LP.CAT3 = @CATN3 '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @CATN4 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQL = @SQL + N'   AND LP.CAT4 = @CATN4 '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @CATN5 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQL = @SQL + N'   AND LP.CAT5 = @CATN5 '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  IF @ARTINT is NOT NULL " & vbCrLf
sConsulta = sConsulta & "     begin " & vbCrLf
sConsulta = sConsulta & "   IF @COINCIDENCIA=1 " & vbCrLf
sConsulta = sConsulta & "          set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                   AND (LP.ART_INT + I.ART like ''% ''  + @ARTINT + '' %''' +" & vbCrLf
sConsulta = sConsulta & "          ' OR LP.ART_INT + I.ART like @ARTINT  + '' %''' +" & vbCrLf
sConsulta = sConsulta & "          ' OR LP.ART_INT + I.ART like ''% '' + @ARTINT  ' + " & vbCrLf
sConsulta = sConsulta & "     ' OR LP.ART_INT + I.ART=@ARTINT)'" & vbCrLf
sConsulta = sConsulta & "        ELSE" & vbCrLf
sConsulta = sConsulta & "      set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                   AND LP.ART_INT + I.ART like + ''%'' + @ARTINT + ''%'' '" & vbCrLf
sConsulta = sConsulta & "     end" & vbCrLf
sConsulta = sConsulta & "   IF @ARTDEN is NOT NULL " & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "      IF @COINCIDENCIA=1 " & vbCrLf
sConsulta = sConsulta & "         set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                   AND (ART4.DEN + I.DESCR  + ART_AUX.DEN + LP.DESCR_LIBRE like ''% ''  + @ARTDEN + '' %''' + " & vbCrLf
sConsulta = sConsulta & "          ' OR ART4.DEN + I.DESCR  + ART_AUX.DEN + LP.DESCR_LIBRE like @ARTDEN + '' %''' +" & vbCrLf
sConsulta = sConsulta & "          ' OR ART4.DEN + I.DESCR  + ART_AUX.DEN + LP.DESCR_LIBRE like ''% '' + @ARTDEN ' + " & vbCrLf
sConsulta = sConsulta & "     ' OR ART4.DEN + I.DESCR  + ART_AUX.DEN + LP.DESCR_LIBRE =@ARTDEN)'" & vbCrLf
sConsulta = sConsulta & "      ELSE" & vbCrLf
sConsulta = sConsulta & "         set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                   AND ART4.DEN + I.DESCR  + ART_AUX.DEN + LP.DESCR_LIBRE like + ''%'' + @ARTDEN + ''%'''" & vbCrLf
sConsulta = sConsulta & "    END" & vbCrLf
sConsulta = sConsulta & "   IF @ARTEXT is NOT NULL" & vbCrLf
sConsulta = sConsulta & "     BEGIN" & vbCrLf
sConsulta = sConsulta & "       IF @COINCIDENCIA=1 " & vbCrLf
sConsulta = sConsulta & "         set @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                   AND (PA.COD_EXT like ''% ''  + @ARTEXT + '' %''' + " & vbCrLf
sConsulta = sConsulta & "          ' OR PA.COD_EXT like @ARTEXT + '' %''' +" & vbCrLf
sConsulta = sConsulta & "          ' OR PA.COD_EXT like ''% '' + @ARTEXT ' + " & vbCrLf
sConsulta = sConsulta & "     ' OR PA.COD_EXT =@ARTEXT)'" & vbCrLf
sConsulta = sConsulta & "       ELSE" & vbCrLf
sConsulta = sConsulta & "         set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                   AND PA.COD_EXT like + ''%'' + @ARTEXT + ''%'''  " & vbCrLf
sConsulta = sConsulta & "     END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   set  @SQL = @SQL + N') LP" & vbCrLf
sConsulta = sConsulta & "            ON OE.ID = LP.ORDEN" & vbCrLf
sConsulta = sConsulta & "'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "   set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                 WHERE PE.TIPO=1'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if @REFERENCIA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.REFERENCIA = @REFERENCIA '" & vbCrLf
sConsulta = sConsulta & "if @DESDEFECHA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.FECHA >= CONVERT(datetime,@DESDEFECHA,103)'" & vbCrLf
sConsulta = sConsulta & "if @HASTAFECHA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.FECHA <= CONVERT(datetime,@HASTAFECHA,103)'" & vbCrLf
sConsulta = sConsulta & "if @PROVECOD IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.PROVE = @PROVECOD'" & vbCrLf
sConsulta = sConsulta & "if @NUMPEDPROVE IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.NUMEXT = @NUMPEDPROVE '" & vbCrLf
sConsulta = sConsulta & "if @ORDEN IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.NUM = @ORDEN '" & vbCrLf
sConsulta = sConsulta & "if @ANYO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.ANYO = @ANYO '" & vbCrLf
sConsulta = sConsulta & "if @PEDIDO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND PE.NUM = @PEDIDO '" & vbCrLf
sConsulta = sConsulta & "if @PROVEDEN IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND P.DEN  LIKE @PROVEDEN + ''%'' '" & vbCrLf
sConsulta = sConsulta & "IF @APROBADOR IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND (T1.PER = @APROBADOR " & vbCrLf
sConsulta = sConsulta & "                           OR T2.PER = @APROBADOR) '" & vbCrLf
sConsulta = sConsulta & "if @APROVISIONADOR IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND PE.PER = @APROVISIONADOR '" & vbCrLf
sConsulta = sConsulta & "declare @IN varchar(500)" & vbCrLf
sConsulta = sConsulta & "SET @IN = '('" & vbCrLf
sConsulta = sConsulta & "IF @EST & 1 = 1" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '0,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 2 = 2" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '1,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 4 = 4" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '2,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 8 = 8" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '3,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 16 = 16" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '4,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 32 = 32" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '5,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 64 = 64" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '6,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 128 = 128" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '20,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 256 = 256" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '21,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 512 = 512" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '22,'" & vbCrLf
sConsulta = sConsulta & "SET @SQL1 = ''" & vbCrLf
sConsulta = sConsulta & "SET @SQL2 = ''" & vbCrLf
sConsulta = sConsulta & "IF @IN!='('" & vbCrLf
sConsulta = sConsulta & "     set @SQL1=' OE.EST IN ' + LEFT(@IN,LEN(@IN)-1) + ') '" & vbCrLf
sConsulta = sConsulta & "IF @RECINCORRECTO IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "  IF @RECINCORRECTO=1" & vbCrLf
sConsulta = sConsulta & "   set @SQL2 = ' OE.INCORRECTA=1'  " & vbCrLf
sConsulta = sConsulta & "IF @SQL1<>'' and @SQL2<>'' " & vbCrLf
sConsulta = sConsulta & "   SET @SQL= @SQL + ' AND (' + @SQL1 + ' OR ' + @SQL2 + ')'" & vbCrLf
sConsulta = sConsulta & "ELSE " & vbCrLf
sConsulta = sConsulta & "if @SQL1!=''" & vbCrLf
sConsulta = sConsulta & "   SET @SQL= @SQL + ' AND ' + @SQL1 " & vbCrLf
sConsulta = sConsulta & "else" & vbCrLf
sConsulta = sConsulta & "if @SQL2!=''" & vbCrLf
sConsulta = sConsulta & "   SET @SQL= @SQL + ' AND ' + @SQL2" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "if @ID IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + '" & vbCrLf
sConsulta = sConsulta & "   AND OE.ID=@ID '" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "if @DESDEID IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + '" & vbCrLf
sConsulta = sConsulta & "   AND CONVERT(VARCHAR,OE.EST) + LEFT(''00000000'',8-LEN(CONVERT(VARCHAR,OE.ID))) + CONVERT(VARCHAR,OE.ID)>=@DESDEID '" & vbCrLf
sConsulta = sConsulta & "set @SQL = @SQL + '" & vbCrLf
sConsulta = sConsulta & "order by CONVERT(VARCHAR,OE.EST) + LEFT(''00000000'',8-LEN(CONVERT(VARCHAR,OE.ID))) + CONVERT(VARCHAR,OE.ID)'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @PARAM = N'@APROBADOR varchar(50), @APROVISIONADOR varchar(50), @DESDEFECHA varchar(10), @HASTAFECHA varchar(10), @ARTINT varchar(50), @ARTDEN varchar(500), @ARTEXT varchar(500), @PROVECOD varchar(50), @PROVEDEN varchar(500), @NUMPEDPROVE varchar(100),@ORDEN int, @ANYO int, @PEDIDO int, @DESDEID varchar(50), @EST int, @ID int,@REFERENCIA varchar(120) , @CAMPO1 int ,@CAMPO2 int, @VALORCAMPO1 varchar(150) ,@VALORCAMPO2 varchar(150) ,@CATN1 int ,@CATN2 int ,@CATN3 int, @CATN4 int , @CATN5 int '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, @PARAM, @APROBADOR= @APROBADOR, @APROVISIONADOR = @APROVISIONADOR, @DESDEFECHA=@DESDEFECHA, @HASTAFECHA=@HASTAFECHA, @ARTINT=@ARTINT, @ARTDEN=@ARTDEN, @ARTEXT =@ARTEXT , @PROVECOD=@PROVECOD, @PROVEDEN=@PROVEDEN, @NUMPEDPROVE=@NUMPEDPROVE, @ANYO=@ANYO, @ORDEN=@ORDEN, @PEDIDO = @PEDIDO,@DESDEID=@DESDEID, @EST=@EST, @ID = @ID, @REFERENCIA=@REFERENCIA , @CAMPO1=@CAMPO1 , @CAMPO2=@CAMPO2 ,@VALORCAMPO1=@VALORCAMPO1 , @VALORCAMPO2=@VALORCAMPO2 , @CATN1=@CATN1 , @CATN2=@CATN2 ,@CATN3=@CATN3 ,@CATN4=@CATN4, @CATN5=@CATN5" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_MARCAR_BAJA_CAT]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_MARCAR_BAJA_CAT]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_MARCAR_BAJA_CAT (@ORDEN INT)AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @CAT1 AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @CAT2 AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @CAT3 AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @CAT4 AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @CAT5 AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @SEG AS INTEGER" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE cur_LineasOrden1 CURSOR FOR SELECT DISTINCT CAT1,CAT2,CAT3,CAT4,CAT5 FROM LINEAS_PEDIDO WHERE LINEAS_PEDIDO.ORDEN=@ORDEN " & vbCrLf
sConsulta = sConsulta & "     OPEN cur_LineasOrden1" & vbCrLf
sConsulta = sConsulta & "     FETCH NEXT FROM cur_LineasOrden1 INTO @CAT1,@CAT2,@CAT3,@CAT4,@CAT5" & vbCrLf
sConsulta = sConsulta & "     WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "     BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       IF @CAT5 IS NOT NULL AND (SELECT BAJALOG FROM CATN5 WHERE ID=@CAT5) = 1" & vbCrLf
sConsulta = sConsulta & "          IF (SELECT COUNT(*) FROM LINEAS_PEDIDO INNER JOIN ORDEN_ENTREGA ON LINEAS_PEDIDO.ORDEN=ORDEN_ENTREGA.ID WHERE CAT5=@CAT5 AND ORDEN_ENTREGA.EST<6 AND ORDEN_ENTREGA.ID<>@ORDEN)=0 " & vbCrLf
sConsulta = sConsulta & "          BEGIN" & vbCrLf
sConsulta = sConsulta & "             UPDATE CATN5 SET BAJALOG=2 WHERE ID=@CAT5   " & vbCrLf
sConsulta = sConsulta & "             SET @SEG=NULL" & vbCrLf
sConsulta = sConsulta & "             SELECT @SEG=SEGURIDAD FROM CAT_SEGURIDAD WHERE CAT=@CAT5 AND NIVEL=5" & vbCrLf
sConsulta = sConsulta & "             IF @SEG IS NOT NULL EXEC FSEP_ELIMINAR_SEGURIDAD @SEG              " & vbCrLf
sConsulta = sConsulta & "          END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       IF @CAT4 IS NOT NULL AND (SELECT BAJALOG FROM CATN4 WHERE ID=@CAT4) = 1" & vbCrLf
sConsulta = sConsulta & "          IF (SELECT COUNT(*) FROM LINEAS_PEDIDO INNER JOIN ORDEN_ENTREGA ON LINEAS_PEDIDO.ORDEN=ORDEN_ENTREGA.ID WHERE CAT4=@CAT4 AND ORDEN_ENTREGA.EST<6 AND ORDEN_ENTREGA.ID<>@ORDEN)=0 " & vbCrLf
sConsulta = sConsulta & "          BEGIN" & vbCrLf
sConsulta = sConsulta & "               UPDATE CATN4 SET BAJALOG=2 WHERE ID=@CAT4" & vbCrLf
sConsulta = sConsulta & "              SET @SEG=NULL" & vbCrLf
sConsulta = sConsulta & "              SELECT @SEG=SEGURIDAD FROM CAT_SEGURIDAD WHERE CAT=@CAT4 AND NIVEL=4" & vbCrLf
sConsulta = sConsulta & "              IF @SEG IS NOT NULL EXEC FSEP_ELIMINAR_SEGURIDAD @SEG" & vbCrLf
sConsulta = sConsulta & "          END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       IF @CAT3 IS NOT NULL AND (SELECT BAJALOG FROM CATN3 WHERE ID=@CAT3) = 1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "           IF (SELECT COUNT(*) FROM LINEAS_PEDIDO INNER JOIN ORDEN_ENTREGA ON LINEAS_PEDIDO.ORDEN=ORDEN_ENTREGA.ID WHERE CAT3=@CAT3 AND ORDEN_ENTREGA.EST<6 AND ORDEN_ENTREGA.ID<>@ORDEN)=0 " & vbCrLf
sConsulta = sConsulta & "           BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "               UPDATE CATN3 SET BAJALOG=2 WHERE ID=@CAT3" & vbCrLf
sConsulta = sConsulta & "               SET @SEG=NULL" & vbCrLf
sConsulta = sConsulta & "               SELECT @SEG=SEGURIDAD FROM CAT_SEGURIDAD WHERE CAT=@CAT3 AND NIVEL=3" & vbCrLf
sConsulta = sConsulta & "               IF @SEG IS NOT NULL EXEC FSEP_ELIMINAR_SEGURIDAD @SEG" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       IF @CAT2 IS NOT NULL AND (SELECT BAJALOG FROM CATN2 WHERE ID=@CAT2) = 1 " & vbCrLf
sConsulta = sConsulta & "           IF (SELECT COUNT(*) FROM LINEAS_PEDIDO INNER JOIN ORDEN_ENTREGA ON LINEAS_PEDIDO.ORDEN=ORDEN_ENTREGA.ID WHERE CAT2=@CAT2 AND ORDEN_ENTREGA.EST<6 AND ORDEN_ENTREGA.ID<>@ORDEN)=0 " & vbCrLf
sConsulta = sConsulta & "           BEGIN" & vbCrLf
sConsulta = sConsulta & "               UPDATE CATN2 SET BAJALOG=2 WHERE ID=@CAT2" & vbCrLf
sConsulta = sConsulta & "               SET @SEG=NULL" & vbCrLf
sConsulta = sConsulta & "               SELECT @SEG=SEGURIDAD FROM CAT_SEGURIDAD WHERE CAT=@CAT2 AND NIVEL=2" & vbCrLf
sConsulta = sConsulta & "               IF @SEG IS NOT NULL EXEC FSEP_ELIMINAR_SEGURIDAD @SEG" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       IF @CAT1 IS NOT NULL AND (SELECT BAJALOG FROM CATN1 WHERE ID=@CAT1) = 1" & vbCrLf
sConsulta = sConsulta & "           IF (SELECT COUNT(*) FROM LINEAS_PEDIDO INNER JOIN ORDEN_ENTREGA ON LINEAS_PEDIDO.ORDEN=ORDEN_ENTREGA.ID WHERE CAT1=@CAT1 AND ORDEN_ENTREGA.EST<6 AND ORDEN_ENTREGA.ID<>@ORDEN)=0 " & vbCrLf
sConsulta = sConsulta & "           BEGIN" & vbCrLf
sConsulta = sConsulta & "              UPDATE CATN1 SET BAJALOG=2 WHERE ID=@CAT1" & vbCrLf
sConsulta = sConsulta & "              SET @SEG=NULL" & vbCrLf
sConsulta = sConsulta & "              SELECT @SEG=SEGURIDAD FROM CAT_SEGURIDAD WHERE CAT=@CAT1 AND NIVEL=1" & vbCrLf
sConsulta = sConsulta & "              IF @SEG IS NOT NULL EXEC FSEP_ELIMINAR_SEGURIDAD @SEG" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "       FETCH NEXT FROM cur_LineasOrden1 INTO @CAT1,@CAT2,@CAT3,@CAT4,@CAT5" & vbCrLf
sConsulta = sConsulta & "     END" & vbCrLf
sConsulta = sConsulta & "     Close cur_LineasOrden1" & vbCrLf
sConsulta = sConsulta & "     DEALLOCATE cur_LineasOrden1" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion2_12_05_08A2_12_05_09() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim bBool As Boolean

On Error GoTo Error
    

sConsulta = "BEGIN TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = True
ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

frmProgreso.lblDetalle = "Actualizaci�n de storeds"
frmProgreso.lblDetalle.Refresh
frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_12_5_Storeds_9

sConsulta = "DELETE FROM DIC WHERE NOMBRE='CON'"
ExecuteSQL gRDOCon, sConsulta
sConsulta = "DELETE FROM DIC WHERE NOMBRE='DIC'"
ExecuteSQL gRDOCon, sConsulta
sConsulta = "DELETE FROM DIC WHERE NOMBRE='GRUPOPLANT'"
ExecuteSQL gRDOCon, sConsulta
sConsulta = "DELETE FROM DIC WHERE NOMBRE='CONTR'"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "UPDATE VERSION SET NUM ='2.12.05.09'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
ExecuteSQL gRDOCon, sConsulta


sConsulta = "COMMIT TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = False

CodigoDeActualizacion2_12_05_08A2_12_05_09 = True
Exit Function
    
Error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_12_05_08A2_12_05_09 = False

End Function


Private Sub V_2_12_5_Storeds_9()
Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_AR_JOB_MODIF_DATOS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_AR_JOB_MODIF_DATOS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE dbo.SP_AR_JOB_MODIF_DATOS (@MODIF TINYINT, @ENAB AS TINYINT ,@NOMBRE VARCHAR(50),@TIPO INT,@INTERVALO INT ,@RECFACTOR INT,@SUBDIA INT, @FRECSUBDIA INT,@RELATIVO INT, @FECINI INT,@TIMEINI INT, @RET_STAT INT OUTPUT)AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @BD AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @COMANDO AS VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SERVNAME AS VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @MODIF=1 " & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "     IF @ENAB != 2" & vbCrLf
sConsulta = sConsulta & "             EXEC @RET_STAT= msdb.dbo.sp_update_job @job_name=@NOMBRE,@enabled=@ENAB" & vbCrLf
sConsulta = sConsulta & "             IF @RET_STAT = 1" & vbCrLf
sConsulta = sConsulta & "                   RETURN" & vbCrLf
sConsulta = sConsulta & "     EXEC @RET_STAT= msdb.dbo.sp_update_jobschedule @job_name=@NOMBRE,@name='ACT_AHORROS_SCH1',@freq_type =@TIPO,@freq_interval =@INTERVALO,@freq_subday_type=@SUBDIA,@freq_subday_interval =@FRECSUBDIA,@freq_relative_interval=@RELATIVO,@freq_recurrence_factor =@RECFACTOR, @active_start_date=@FECINI,@active_start_time =@TIMEINI" & vbCrLf
sConsulta = sConsulta & "END " & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SELECT @SERVNAME=@@SERVERNAME" & vbCrLf
sConsulta = sConsulta & "   SET @BD=SUBSTRING(@NOMBRE,13,LEN(@NOMBRE)-12)" & vbCrLf
sConsulta = sConsulta & "   EXEC @RET_STAT= msdb.dbo.sp_add_job @job_name=@NOMBRE,@enabled=@ENAB" & vbCrLf
sConsulta = sConsulta & "   IF @RET_STAT= 0" & vbCrLf
sConsulta = sConsulta & "   BEGIN       " & vbCrLf
sConsulta = sConsulta & "         " & vbCrLf
sConsulta = sConsulta & "         SET @COMANDO='osql -E -S ' + @SERVNAME + ' -Q ""Exec ' + @BD + '.dbo.SP_AR_CALCULAR""'" & vbCrLf
sConsulta = sConsulta & "         EXEC @RET_STAT= msdb.dbo.sp_add_jobstep @job_name=@NOMBRE,@step_id =1,@step_name='ACT_AHORROS_STEP1',@subsystem = 'CMDEXEC', @command =@COMANDO " & vbCrLf
sConsulta = sConsulta & "         IF @RET_STAT=0" & vbCrLf
sConsulta = sConsulta & "         BEGIN" & vbCrLf
sConsulta = sConsulta & "             EXEC @RET_STAT = msdb.dbo.sp_add_jobserver  @job_name=@NOMBRE,  @server_name ='(LOCAL)'" & vbCrLf
sConsulta = sConsulta & "             IF @RET_STAT=0" & vbCrLf
sConsulta = sConsulta & "                   EXEC @RET_STAT= msdb.dbo.sp_add_jobschedule @job_name=@NOMBRE,@name='ACT_AHORROS_SCH1',@freq_type =@TIPO,@freq_interval =@INTERVALO,@freq_subday_type=@SUBDIA,@freq_subday_interval =@FRECSUBDIA,@freq_relative_interval=@RELATIVO,@freq_recurrence_factor =@RECFACTOR, @active_start_date=@FECINI,@active_start_time =@TIMEINI" & vbCrLf
sConsulta = sConsulta & "         END" & vbCrLf
sConsulta = sConsulta & "    END " & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


End Sub

Public Function CodigoDeActualizacion2_12_05_09A2_12_05_10() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim bBool As Boolean

On Error GoTo Error
    

sConsulta = "BEGIN TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = True
ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

frmProgreso.lblDetalle = "Actualizaci�n de storeds"
frmProgreso.lblDetalle.Refresh
frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_12_5_Triggers_10

sConsulta = "UPDATE VERSION SET NUM ='2.12.05.10'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
ExecuteSQL gRDOCon, sConsulta


sConsulta = "COMMIT TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = False

CodigoDeActualizacion2_12_05_09A2_12_05_10 = True
Exit Function
    
Error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_12_05_09A2_12_05_10 = False

End Function


Private Sub V_2_12_5_Triggers_10()
Dim sConsulta As String

    sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PAI_TG_INSUPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
    sConsulta = sConsulta & "drop trigger [dbo].[PAI_TG_INSUPD]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    sConsulta = "CREATE TRIGGER PAI_TG_INSUPD ON dbo.PAI" & vbCrLf
    sConsulta = sConsulta & " FOR INSERT, UPDATE" & vbCrLf
    sConsulta = sConsulta & " AS" & vbCrLf
    sConsulta = sConsulta & " DECLARE @COD VARCHAR(50)" & vbCrLf
    sConsulta = sConsulta & " DECLARE curTG_Pai_Ins CURSOR FOR SELECT COD FROM INSERTED" & vbCrLf
    sConsulta = sConsulta & " OPEN curTG_Pai_Ins" & vbCrLf
    sConsulta = sConsulta & " FETCH NEXT FROM curTG_Pai_Ins INTO @COD" & vbCrLf
    sConsulta = sConsulta & " WHILE @@FETCH_STATUS=0" & vbCrLf
    sConsulta = sConsulta & " BEGIN" & vbCrLf
    sConsulta = sConsulta & " UPDATE PAI SET FECACT=GETDATE() WHERE COD=@COD" & vbCrLf
    sConsulta = sConsulta & " FETCH NEXT FROM curTG_Pai_Ins INTO @COD" & vbCrLf
    sConsulta = sConsulta & " End" & vbCrLf
    sConsulta = sConsulta & " Close curTG_Pai_Ins" & vbCrLf
    sConsulta = sConsulta & " DEALLOCATE curTG_Pai_Ins" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion2_12_05_10A2_12_05_11() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim bBool As Boolean

On Error GoTo Error
    

sConsulta = "BEGIN TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = True
ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

frmProgreso.lblDetalle = "Actualizaci�n de storeds"
frmProgreso.lblDetalle.Refresh
frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_12_5_Storeds_11

sConsulta = "UPDATE VERSION SET NUM ='2.12.05.11'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
ExecuteSQL gRDOCon, sConsulta


sConsulta = "COMMIT TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = False

CodigoDeActualizacion2_12_05_10A2_12_05_11 = True
Exit Function
    
Error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_12_05_10A2_12_05_11 = False

End Function


Private Sub V_2_12_5_Storeds_11()
Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GMN1_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[GMN1_COD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE GMN1_COD (@OLD VARCHAR(50),@NEW VARCHAR(50))  AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT ON" & vbCrLf
sConsulta = sConsulta & "BEGIN TRANSACTION" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR FOR SELECT COD FROM GMN1 WHERE COD=@OLD" & vbCrLf
sConsulta = sConsulta & "OPEN C" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C INTO @VAR_COD" & vbCrLf
sConsulta = sConsulta & "IF @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART1_ADJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART2_ADJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART3_ADJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ADJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE DETALLE_PUJAS NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ADJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ADJREU NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ESP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OBJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OFE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OFE_WEB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRES NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESCON NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESPROY NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESCON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESCON4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFEMIN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_GRUPO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_ESP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_WEB_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_WEB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PER NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_PET NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_POT NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVENOTIF NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE REU_PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ESP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4_ACT NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATALOG_LIN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_PEDIDO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART_UP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_DEF NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO_DEF NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO_ESP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_GR_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_GR_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ITEM_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ITEM_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_SOBRE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GRUPO_OFE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GRUPO_OFE_REU NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USAR_GR_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_CONTR NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE SOBRE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FAVORITOS_LINEAS_PEDIDO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_NUMDEC NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN1 SET COD=@NEW WHERE COD=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART1_ADJ SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART2_ADJ SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART3_ADJ SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4_ADJ SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE BLOQUEO_PROCE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE COM_GMN1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE COM_GMN2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE COM_GMN3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE COM_GMN4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE DETALLE_PUJAS SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_ADJ SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_ADJREU SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_ESP SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_OBJ SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_OFE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_OFE_WEB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_PRES SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_PRESCON SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_PRESCON3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_PRESCON4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_PRESPROY SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_UON1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_UON2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_UON3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFEMIN SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES_MAT1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES_MAT2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES_MAT3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES_MAT4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_ESP SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE_ADJUN SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE_WEB_ADJUN SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE_WEB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PER SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE_PET SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE_POT SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_GMN1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_GMN2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_GMN3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_GMN4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVENOTIF SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE REU_PROCE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_PROCE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_PROVE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_GRUPO SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_UON1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_UON2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_UON3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_PRESCON3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_PRESCON4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_PROVE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4_ESP  SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN4_ACT SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART4_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN4_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4_ATRIB  SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CATALOG_LIN  SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE LINEAS_PEDIDO  SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART_UP  SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_DEF SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GRUPO SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GRUPO_DEF SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GRUPO_ESP SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PRES1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PRES2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PRES3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PRES4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_PRES1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_PRES2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_PRES3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_PRES4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_UON1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_UON2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_UON3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_UON1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_UON2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_UON3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_GR_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_GR_ADJUN SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_ITEM_ADJUN SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_ITEM_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ALL SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ALL_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ALL_PROVE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_GRUPO SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_GRUPO_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_GRUPO_PROVE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ITEM SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ITEM_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ITEM_PROVE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_PROCE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_PROCE_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_PROCE_PROVE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_PROCE_SOBRE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GRUPO_OFE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GRUPO_OFE_REU SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE USAR_GR_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_CONTR SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE SOBRE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART4_ADJUN SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE FAVORITOS_LINEAS_PEDIDO SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_NUMDEC SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART1_ADJ CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART2_ADJ CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART3_ADJ CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ADJ CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE DETALLE_PUJAS CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ADJ CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ADJREU CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ESP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OBJ CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OFE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OFE_WEB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRES CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESCON CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESCON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESCON4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESPROY CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFEMIN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_ESP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_WEB_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_WEB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PER CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_PET CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_POT CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVENOTIF CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE REU_PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_GRUPO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ESP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4_ACT CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATALOG_LIN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_PEDIDO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART_UP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_DEF CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO_DEF CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO_ESP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_GR_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_GR_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ITEM_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ITEM_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_SOBRE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GRUPO_OFE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GRUPO_OFE_REU CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USAR_GR_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_CONTR CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE SOBRE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FAVORITOS_LINEAS_PEDIDO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_NUMDEC CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close C" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "COMMIT TRANSACTION" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT OFF" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PROCE_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[PROCE_COD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE PROCE_COD (@OLD SMALLINT,@NEW SMALLINT,@ANYO INT,@GMN1 VARCHAR (50))  AS " & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_COD SMALLINT " & vbCrLf
sConsulta = sConsulta & "DECLARE @AN AS INT " & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN AS VARCHAR(50) " & vbCrLf
sConsulta = sConsulta & "BEGIN " & vbCrLf
sConsulta = sConsulta & "DECLARE D CURSOR FOR SELECT ANYO,GMN1,COD FROM PROCE WHERE ANYO=@ANYO AND COD=@OLD AND GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "OPEN D " & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM D INTO @AN,@GMN,@VAR_COD " & vbCrLf
sConsulta = sConsulta & "IF @@FETCH_STATUS=0 " & vbCrLf
sConsulta = sConsulta & "BEGIN " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART1_ADJ NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART2_ADJ NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART3_ADJ NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ADJ NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ADJ NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ADJREU NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ESP NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OBJ NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OFE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OFE_WEB NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRES NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESCON NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESCON3 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESCON4 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESPROY NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON1 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON2 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON3 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFEMIN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE DETALLE_PUJAS NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVENOTIF NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATALOG_LIN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_PEDIDO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_ESP NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_ADJUN NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_WEB_ADJUN NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_WEB NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PER NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_PET NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_POT NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE REU_PROCE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES1 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES2 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES3 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES4 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON1 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON2 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON3 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO_ESP NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO_DEF NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES1 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES2 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES3 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES4 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON1 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON2 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON3 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_ATRIB NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_DEF NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL_ATRIB NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL_PROVE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_ATRIB NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_PROVE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_SOBRE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_ATRIB NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_PROVE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_ATRIB NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_PROVE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON1 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON2 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON3 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON4 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY1 NOCHECK CONSTRAINT ALL  " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY2 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY3 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY4 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON1 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON2 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON3 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON3 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON4 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PROVE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_GRUPO NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROCE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROVE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GRUPO_OFE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GRUPO_OFE_REU NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_GR_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ITEM_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_GR_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ITEM_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USAR_GR_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_CONTR NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE SOBRE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FAVORITOS_LINEAS_PEDIDO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_NUMDEC NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE ART1_ADJ SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ART2_ADJ SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ART3_ADJ SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ART4_ADJ SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_ADJ SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_ADJREU SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_ESP SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_OBJ SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_OFE SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_OFE_WEB SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_PRES SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_UON1 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_UON2 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_UON3 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_PRESCON SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_PRESCON3 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_PRESCON4 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_PRESPROY SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE OFEMIN SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE DETALLE_PUJAS SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVENOTIF SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CATALOG_LIN SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE LINEAS_PEDIDO SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_ESP SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE_ADJUN SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE_WEB_ADJUN SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE_WEB SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PER SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE_PET SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE_POT SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE REU_PROCE SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_PRES1 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_PRES2 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_PRES3 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_PRES4 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_UON1 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_UON2 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_UON3 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GRUPO_ESP SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GRUPO_DEF SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GRUPO SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PRES1 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PRES2 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PRES3 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PRES4 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_UON1 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_UON2 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_UON3 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_ATRIB SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_DEF SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ALL SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ALL_ATRIB SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ALL_PROVE SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_PROCE SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_PROCE_ATRIB SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_PROCE_PROVE SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_PROCE_SOBRE SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_GRUPO SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_GRUPO_ATRIB SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_GRUPO_PROVE SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ITEM SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ITEM_ATRIB SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ITEM_PROVE SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON1 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON2 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON3 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON4 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY1 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY2 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY3 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY4 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_1 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_2 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_3 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_4 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_1 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_2 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_3 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_4 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_UON1 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_UON2 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD  " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_UON3 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_PRESCON3 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_PRESCON4 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_PROVE SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_GRUPO SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_PROVE SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE AR_PROCE SET COD=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE GRUPO_OFE SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GRUPO_OFE_REU SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_ATRIB SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_GR_ATRIB SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_ITEM_ATRIB SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_GR_ADJUN SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_ITEM_ADJUN SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GRUPO SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET COD=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE BLOQUEO_PROCE SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE USAR_GR_ATRIB SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_CONTR SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE SOBRE SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE FAVORITOS_LINEAS_PEDIDO SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_NUMDEC SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART1_ADJ CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART2_ADJ CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART3_ADJ CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ADJ CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ADJ CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESCON CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESCON3 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESCON4 CHECK CONSTRAINT ALL  " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESPROY CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ADJREU CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ESP CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OBJ CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OFE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OFE_WEB CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRES CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON1 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON2 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON3 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFEMIN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE DETALLE_PUJAS CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVENOTIF CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATALOG_LIN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_PEDIDO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_ESP CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_ADJUN CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_WEB_ADJUN CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_WEB CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PER CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_PET CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_POT CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE REU_PROCE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES1 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES2 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES3 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES4 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON1 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON2 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON3 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO_ESP CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO_DEF CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES1 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES2 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES3 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES4 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON1 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON2 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON3 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_ATRIB CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_DEF CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL_ATRIB CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL_PROVE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_ATRIB CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_PROVE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_SOBRE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_ATRIB CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_PROVE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_ATRIB CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_PROVE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON1 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON2 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON3 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON4 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY1 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY2 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY3 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY4 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON1 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON2 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON3 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON3 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON4 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PROVE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_GRUPO CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROCE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROVE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GRUPO_OFE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GRUPO_OFE_REU CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_GR_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ITEM_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_GR_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ITEM_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USAR_GR_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_CONTR CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE SOBRE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FAVORITOS_LINEAS_PEDIDO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_NUMDEC CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "End " & vbCrLf
sConsulta = sConsulta & "Close D " & vbCrLf
sConsulta = sConsulta & "DEALLOCATE D " & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USU_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[USU_COD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE USU_COD (@OLD VARCHAR(50),@NEW VARCHAR(50),@PWD VARCHAR(512))  AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT ON" & vbCrLf
sConsulta = sConsulta & "--BEGIN TRANSACTION" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR FOR SELECT COD FROM USU WHERE COD=@OLD" & vbCrLf
sConsulta = sConsulta & "OPEN C" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C INTO @VAR_COD" & vbCrLf
sConsulta = sConsulta & "IF @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU_ACC NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU_LIS NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_SOBRE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTA_INICIAL_PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTA_INICIAL_GRUPO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LOG NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_CONTR NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE SOBRE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PARINS_DEF NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PARINS_DOT NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PARINS_GEST NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PARINS_PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FAVORITOS_ORDEN_ENTREGA NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FAVORITOS_ORDEN_ENTREGA_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FAVORITOS_LINEAS_PEDIDO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FAVORITOS_LINEAS_PEDIDO_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_NUMDEC NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE USU SET COD=@NEW,PWD=@PWD WHERE COD=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE USU_ACC SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE USU_LIS SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET USUVAL=@NEW WHERE USUVAL=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET USUVALSELPROVE=@NEW WHERE USUVALSELPROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET USUVALCIERRE=@NEW WHERE USUVALCIERRE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET USUAPER=@NEW WHERE USUAPER=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_PROCE SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_PROCE_PROVE SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_PROCE_ATRIB SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_PROCE_SOBRE SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_GRUPO SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_GRUPO_PROVE SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_GRUPO_ATRIB SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ITEM SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ITEM_PROVE SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ITEM_ATRIB SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ALL SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ALL_PROVE SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ALL_ATRIB SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTA_INICIAL_PROCE SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTA_INICIAL_GRUPO SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_CONTR SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE SOBRE SET USUAPER=@NEW WHERE USUAPER=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PARINS_DEF SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PARINS_DOT SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PARINS_GEST SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PARINS_PROCE SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_NUMDEC SET USU=@NEW WHERE USU=@OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU_ACC CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU_LIS CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_SOBRE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTA_INICIAL_PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTA_INICIAL_GRUPO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LOG CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_CONTR CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE SOBRE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PARINS_DEF CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PARINS_DOT CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PARINS_GEST CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PARINS_PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_NUMDEC CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close C" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT OFF" & vbCrLf
sConsulta = sConsulta & "--COMMIT TRANSACTION" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_CAMBIARREUNION]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_CAMBIARREUNION]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE    PROCEDURE SP_CAMBIARREUNION (@FECHAANTERIOR DATETIME,@FECHANUEVA DATETIME,@RES INT OUTPUT)  AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE @REF AS VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & " SET @REF = (SELECT REF FROM REU WHERE FECHA=@FECHAANTERIOR)" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & " INSERT INTO REU (FECHA,REF) VALUES (@FECHANUEVA,@REF)" & vbCrLf
sConsulta = sConsulta & "           IF @@ERROR <> 0  GOTO ERR_" & vbCrLf
sConsulta = sConsulta & "  " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE REU_PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "declare @DIF int" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "set @dif = datediff(minute,@fechaanterior,@fechanueva)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE REU_PROCE SET FECHA=@FECHANUEVA," & vbCrLf
sConsulta = sConsulta & "    HORA=dateadd(minute,@dif,Convert(datetime,(Convert(varchar(10),@FECHAANTERIOR,110) + ' ' + Convert(varchar(8),HORA,108)),110))" & vbCrLf
sConsulta = sConsulta & " WHERE FECHA=@FECHAANTERIOR" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET FECULTREU=@FECHANUEVA  " & vbCrLf
sConsulta = sConsulta & "  FROM PROCE P INNER JOIN REU_PROCE RP ON P.ANYO = RP.ANYO AND P.GMN1 = RP.GMN1 AND P.COD = RP.PROCE" & vbCrLf
sConsulta = sConsulta & " WHERE RP.FECHA = @FECHANUEVA" & vbCrLf
sConsulta = sConsulta & "   AND P.FECULTREU=@FECHAANTERIOR" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM REU WHERE FECHA=@FECHAANTERIOR" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE REU_PROCE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "  " & vbCrLf
sConsulta = sConsulta & "ERR_: " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & " SET @RES = @@ERROR" & vbCrLf
sConsulta = sConsulta & " RETURN " & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ATR_MODIFICAR]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[ATR_MODIFICAR]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE ATR_MODIFICAR  @TABLA SMALLINT,@ID INTEGER,@DEN VARCHAR(100)=NULL,@TIPO SMALLINT=NULL, @PREFBAJO TINYINT , @INTRO TINYINT, @MINNUM FLOAT=NULL,@MINFEC DATETIME=NULL,@MAXNUM FLOAT=NULL,@MAXFEC DATETIME=NULL,@ATRIB INTEGER=NULL,@INTERNO  TINYINT=NULL,@AMBITO TINYINT=NULL,@OBLIG  TINYINT=NULL,@OP_PREC VARCHAR(2)=NULL,@APLIC_PREC VARCHAR(50)=NULL,@DEF_PREC TINYINT=NULL,@ANYO VARCHAR(50)=NULL,@GMN1 VARCHAR(50)=NULL,@PROCE VARCHAR(50)=NULL,@GRUPO VARCHAR(50)=NULL , @FECACT DATETIME OUTPUT,@ORDEN SMALLINT=NULL OUTPUT,@PLANT VARCHAR(50)=NULL AS " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @POND AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @AMBITOOLD AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @COUNT AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @APLI_PREC_V AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @ORDEN_V AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @ATRIB_PREC AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @USAR_PREC AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRIMERO AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @TIPOOLD AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @VISTAS AS TINYINT" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @TABLA=0  " & vbCrLf
sConsulta = sConsulta & "     BEGIN --@tabla=0" & vbCrLf
sConsulta = sConsulta & "            SELECT   @POND=POND,@TIPOOLD=TIPO  FROM DEF_ATRIB  WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "            IF @POND = 1 AND @TIPO<> 2 and @TIPO<>3  " & vbCrLf
sConsulta = sConsulta & "                         SET @POND = 0" & vbCrLf
sConsulta = sConsulta & "            ELSE " & vbCrLf
sConsulta = sConsulta & "                          begin" & vbCrLf
sConsulta = sConsulta & "                          IF @POND = 2 AND (@TIPO=4 OR @INTRO=0)" & vbCrLf
sConsulta = sConsulta & "                                       SET @POND= 0" & vbCrLf
sConsulta = sConsulta & "                          ELSE" & vbCrLf
sConsulta = sConsulta & "                                        IF  @POND = 3 AND @TIPO<> 2 " & vbCrLf
sConsulta = sConsulta & "                                                   SET @POND= 0" & vbCrLf
sConsulta = sConsulta & "                                        ELSE" & vbCrLf
sConsulta = sConsulta & "                                                    IF @POND =5 AND @TIPO<>4" & vbCrLf
sConsulta = sConsulta & "                                                            begin" & vbCrLf
sConsulta = sConsulta & "                                                            SET @POND= 0" & vbCrLf
sConsulta = sConsulta & "                                                            end" & vbCrLf
sConsulta = sConsulta & "                           end" & vbCrLf
sConsulta = sConsulta & "            IF @TIPOOLD <> @TIPO " & vbCrLf
sConsulta = sConsulta & "                  begin" & vbCrLf
sConsulta = sConsulta & "                  IF @TIPOOLD=2" & vbCrLf
sConsulta = sConsulta & "                        BEGIN" & vbCrLf
sConsulta = sConsulta & "                        UPDATE PROCE_ATRIB SET OP_PREC=NULL, APLIC_PREC=NULL, DEF_PREC=NULL WHERE ATRIB= @ID" & vbCrLf
sConsulta = sConsulta & "                        UPDATE PLANTILLA_ATRIB SET OP_PREC=NULL, APLIC_PREC=NULL, DEF_PREC=NULL WHERE ATRIB= @ID" & vbCrLf
sConsulta = sConsulta & "                        END " & vbCrLf
sConsulta = sConsulta & "                  end" & vbCrLf
sConsulta = sConsulta & "            UPDATE DEF_ATRIB SET DEN=@DEN,TIPO=@TIPO,PREFBAJO=@PREFBAJO,POND=@POND,INTRO=@INTRO,MAXNUM=CAST(@MAXNUM AS VARCHAR(15)) ,MINNUM=CAST(@MINNUM AS VARCHAR(15)),MAXFEC=CONVERT(VARCHAR(10),@MAXFEC,111),MINFEC=CONVERT(VARCHAR(10),@MINFEC,111) WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "            SELECT @FECACT=FECACT FROM DEF_ATRIB WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "      END --@tabla=0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @TABLA=1" & vbCrLf
sConsulta = sConsulta & "    BEGIN -- @tabla=1" & vbCrLf
sConsulta = sConsulta & "       IF @APLIC_PREC IS not  NULL  AND @ORDEN is  NULL " & vbCrLf
sConsulta = sConsulta & "                 BEGIN --@orden=null" & vbCrLf
sConsulta = sConsulta & "                 SELECT @ATRIB_PREC =ATRIB  FROM PROCE_ATRIB WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "                 SELECT @ORDEN=ORDEN FROM PROCE_ATRIB WHERE  ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND APLIC_PREC = @APLIC_PREC AND" & vbCrLf
sConsulta = sConsulta & "                    OP_PREC=@OP_PREC AND ATRIB =@ATRIB_PREC" & vbCrLf
sConsulta = sConsulta & "                      " & vbCrLf
sConsulta = sConsulta & "                 SET @APLI_PREC_V=@APLIC_PREC" & vbCrLf
sConsulta = sConsulta & "                 SET @ORDEN_V= 0" & vbCrLf
sConsulta = sConsulta & "                 SET @PRIMERO=0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                 WHILE @ORDEN_V=0 and @APLI_PREC_V<=3" & vbCrLf
sConsulta = sConsulta & "                         BEGIN -- while" & vbCrLf
sConsulta = sConsulta & "                             SELECT  @ORDEN_V = MAX(ORDEN) FROM PROCE_ATRIB WHERE  ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND APLIC_PREC = @APLI_PREC_V" & vbCrLf
sConsulta = sConsulta & "                             IF @ORDEN_V IS NULL" & vbCrLf
sConsulta = sConsulta & "                               begin --@ORDEN_V =null" & vbCrLf
sConsulta = sConsulta & "                                 IF @APLIC_PREC=3" & vbCrLf
sConsulta = sConsulta & "                                     begin" & vbCrLf
sConsulta = sConsulta & "                                       SET @ORDEN_V=1 " & vbCrLf
sConsulta = sConsulta & "                                       SET @PRIMERO=1" & vbCrLf
sConsulta = sConsulta & "                                     end" & vbCrLf
sConsulta = sConsulta & "                                 ELSE                   " & vbCrLf
sConsulta = sConsulta & "                                   begin" & vbCrLf
sConsulta = sConsulta & "                                      SET @ORDEN_V= 0" & vbCrLf
sConsulta = sConsulta & "                                      SET @APLI_PREC_V=@APLI_PREC_V + 1" & vbCrLf
sConsulta = sConsulta & "                                   end" & vbCrLf
sConsulta = sConsulta & "                               end  --@ORDEN_V =null" & vbCrLf
sConsulta = sConsulta & "                         END -- while" & vbCrLf
sConsulta = sConsulta & "                if @ORDEN_V IS NULL  SET @ORDEN_V=0" & vbCrLf
sConsulta = sConsulta & "                      begin" & vbCrLf
sConsulta = sConsulta & "                      if @PRIMERO=1" & vbCrLf
sConsulta = sConsulta & "                              UPDATE PROCE_ATRIB SET ORDEN=1 WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "                     else" & vbCrLf
sConsulta = sConsulta & "                              UPDATE PROCE_ATRIB SET ORDEN=@ORDEN_V+1 WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "                     end" & vbCrLf
sConsulta = sConsulta & "                UPDATE PROCE_ATRIB SET ORDEN = ORDEN +1 WHERE  APLIC_PREC < @APLIC_PREC AND ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ID<>@ID" & vbCrLf
sConsulta = sConsulta & "               SET @ORDEN=@ORDEN_V +1" & vbCrLf
sConsulta = sConsulta & "              " & vbCrLf
sConsulta = sConsulta & "             END --@orden=null" & vbCrLf
sConsulta = sConsulta & "       SELECT @POND=POND,@AMBITOOLD=AMBITO,@USAR_PREC=USAR_PREC FROM PROCE_ATRIB WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "       IF @INTRO=0 AND @POND=2 SET @POND=0" & vbCrLf
sConsulta = sConsulta & "       IF @DEF_PREC=1 SET @USAR_PREC=1" & vbCrLf
sConsulta = sConsulta & "       UPDATE PROCE_ATRIB SET PREFBAJO=@PREFBAJO,INTRO=@INTRO,MAXNUM=@MAXNUM  ," & vbCrLf
sConsulta = sConsulta & "                  MINNUM=@MINNUM,MAXFEC=CONVERT(VARCHAR(10),@MAXFEC,111)," & vbCrLf
sConsulta = sConsulta & "                  MINFEC=CONVERT(VARCHAR(10),@MINFEC,111),INTERNO=@INTERNO,AMBITO=@AMBITO,OBLIG=@OBLIG," & vbCrLf
sConsulta = sConsulta & "                  OP_PREC=@OP_PREC,APLIC_PREC=@APLIC_PREC,USAR_PREC=@USAR_PREC," & vbCrLf
sConsulta = sConsulta & "                  DEF_PREC=@DEF_PREC,POND=@POND WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "       SELECT @FECACT=FECACT FROM PROCE_ATRIB WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "       IF   @ATRIB IS NOT  NULL" & vbCrLf
sConsulta = sConsulta & "             BEGIN --@atrib not null" & vbCrLf
sConsulta = sConsulta & "                    SELECT @POND=POND FROM DEF_ATRIB WHERE ID=@ATRIB" & vbCrLf
sConsulta = sConsulta & "                    IF @INTRO=0 AND @POND=2 SET @POND=0" & vbCrLf
sConsulta = sConsulta & "                       UPDATE DEF_ATRIB SET PREFBAJO=@PREFBAJO,INTRO=@INTRO,MAXNUM=@MAXNUM ," & vbCrLf
sConsulta = sConsulta & "                           MINNUM=@MINNUM,MAXFEC=CONVERT(VARCHAR(10),@MAXFEC,111)," & vbCrLf
sConsulta = sConsulta & "                           MINFEC=CONVERT(VARCHAR(10),@MINFEC,111),POND=@POND WHERE ID=@ATRIB" & vbCrLf
sConsulta = sConsulta & "             END--@atrib not null" & vbCrLf
sConsulta = sConsulta & "--           END estaba mal" & vbCrLf
sConsulta = sConsulta & "       IF @AMBITO<>@AMBITOOLD" & vbCrLf
sConsulta = sConsulta & "               BEGIN --@ambito<>ambitoold" & vbCrLf
sConsulta = sConsulta & "                     SELECT @COUNT=COUNT(*) FROM PROCE_OFE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "                     IF @COUNT > 0" & vbCrLf
sConsulta = sConsulta & "                          IF  @AMBITOOLD>@AMBITO" & vbCrLf
sConsulta = sConsulta & "                               IF @AMBITOOLD=3" & vbCrLf
sConsulta = sConsulta & "                                     begin --old=3" & vbCrLf
sConsulta = sConsulta & "                                           DELETE OFE_ITEM_ATRIB WHERE  ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ATRIB_ID=@ID" & vbCrLf
sConsulta = sConsulta & "                                           DELETE CONF_VISTAS_GRUPO_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ATRIB=@ID" & vbCrLf
sConsulta = sConsulta & "                                           DELETE CONF_VISTAS_ALL_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE  AND ATRIB=@ID" & vbCrLf
sConsulta = sConsulta & "                                           DELETE CONF_VISTAS_ITEM_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE  AND ATRIB=@ID" & vbCrLf
sConsulta = sConsulta & "                                           INSERT INTO CONF_VISTAS_PROCE_ATRIB (ANYO,GMN1,PROCE,USU,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                              SELECT @ANYO,@GMN1,@PROCE,USU,VISTA,@ID FROM  CONF_VISTAS_PROCE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "                                     end  --old=3" & vbCrLf
sConsulta = sConsulta & "                               ELSE --ser�a old=2" & vbCrLf
sConsulta = sConsulta & "                                           DELETE OFE_GR_ATRIB WHERE  ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ATRIB_ID=@ID AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                          ELSE  --old < ambito" & vbCrLf
sConsulta = sConsulta & "                                 IF @AMBITOOLD=1" & vbCrLf
sConsulta = sConsulta & "                                     BEGIN --old=1" & vbCrLf
sConsulta = sConsulta & "                                        IF @AMBITO=2" & vbCrLf
sConsulta = sConsulta & "                                                BEGIN" & vbCrLf
sConsulta = sConsulta & "                                                INSERT INTO OFE_GR_ATRIB (ANYO, GMN1, PROCE,GRUPO,PROVE,OFE,ATRIB_ID,VALOR_NUM,VALOR_TEXT," & vbCrLf
sConsulta = sConsulta & "                                                      VALOR_FEC,VALOR_BOOL,VALOR_POND) " & vbCrLf
sConsulta = sConsulta & "                                                      SELECT DISTINCT OFE_ATRIB.ANYO, OFE_ATRIB.GMN1, OFE_ATRIB.PROCE,PROCE_ATRIB.GRUPO" & vbCrLf
sConsulta = sConsulta & "                                                                   ,OFE_ATRIB.PROVE,OFE_ATRIB.OFE,OFE_ATRIB.ATRIB_ID,OFE_ATRIB.VALOR_NUM,OFE_ATRIB.VALOR_TEXT" & vbCrLf
sConsulta = sConsulta & "                                                                   ,OFE_ATRIB.VALOR_FEC,OFE_ATRIB.VALOR_BOOL,OFE_ATRIB.VALOR_POND " & vbCrLf
sConsulta = sConsulta & "                                                     FROM OFE_ATRIB " & vbCrLf
sConsulta = sConsulta & "                                                     INNER JOIN PROCE_ATRIB ON  OFE_ATRIB.ANYO=PROCE_ATRIB.ANYO" & vbCrLf
sConsulta = sConsulta & "                                                                 AND OFE_ATRIB.GMN1=PROCE_ATRIB.GMN1 AND OFE_ATRIB.PROCE =PROCE_ATRIB.PROCE " & vbCrLf
sConsulta = sConsulta & "                                                                 AND PROCE_ATRIB.GRUPO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                                                      WHERE OFE_ATRIB.ATRIB_ID=@ID AND OFE_ATRIB.ANYO=@ANYO AND OFE_ATRIB.GMN1=@GMN1 AND OFE_ATRIB.PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "                                                 END" & vbCrLf
sConsulta = sConsulta & "                                        DELETE OFE_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ATRIB_ID=@ID" & vbCrLf
sConsulta = sConsulta & "                                        IF @AMBITO = 3" & vbCrLf
sConsulta = sConsulta & "                                                  begin --ambito=3" & vbCrLf
sConsulta = sConsulta & "                                                  DELETE CONF_VISTAS_PROCE_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ATRIB=@ID" & vbCrLf
sConsulta = sConsulta & "                                                  IF @GRUPO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                                                        INSERT INTO CONF_VISTAS_GRUPO_ATRIB (ANYO,GMN1,PROCE,GRUPO,USU,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                                          SELECT @ANYO,@GMN1,@PROCE,@GRUPO,USU,VISTA,@ID FROM  CONF_VISTAS_GRUPO WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO " & vbCrLf
sConsulta = sConsulta & "                                                  ELSE" & vbCrLf
sConsulta = sConsulta & "                                                        INSERT INTO CONF_VISTAS_GRUPO_ATRIB (ANYO,GMN1,PROCE,GRUPO,USU,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                                          SELECT @ANYO,@GMN1,@PROCE,GRUPO,USU,VISTA,@ID FROM  CONF_VISTAS_GRUPO WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE         " & vbCrLf
sConsulta = sConsulta & "                                                 INSERT INTO CONF_VISTAS_ALL_ATRIB (ANYO,GMN1,PROCE,USU,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                                     SELECT @ANYO,@GMN1,@PROCE,USU,VISTA,@ID FROM  CONF_VISTAS_ALL WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "                                                 IF @GRUPO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                                                     INSERT INTO CONF_VISTAS_ITEM_ATRIB (ANYO,GMN1,PROCE,GRUPO,USU,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                                         SELECT @ANYO,@GMN1,@PROCE,@GRUPO,USU,VISTA,@ID FROM  CONF_VISTAS_ITEM WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO " & vbCrLf
sConsulta = sConsulta & "                                                  ELSE" & vbCrLf
sConsulta = sConsulta & "                                                     INSERT INTO CONF_VISTAS_ITEM_ATRIB (ANYO,GMN1,PROCE,GRUPO,USU,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                                         SELECT @ANYO,@GMN1,@PROCE,GRUPO,USU,VISTA,@ID FROM  CONF_VISTAS_ITEM WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                                                 end--ambito=3" & vbCrLf
sConsulta = sConsulta & "                                     END --old=1" & vbCrLf
sConsulta = sConsulta & "                                ELSE " & vbCrLf
sConsulta = sConsulta & "                                    BEGIN  --old es 2" & vbCrLf
sConsulta = sConsulta & "                                      DELETE OFE_GR_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ATRIB_ID=@ID AND GRUPO=@GRUPO" & vbCrLf
sConsulta = sConsulta & "                                      DELETE CONF_VISTAS_PROCE_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ATRIB=@ID" & vbCrLf
sConsulta = sConsulta & "                                      IF @GRUPO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                                                 INSERT INTO CONF_VISTAS_GRUPO_ATRIB (ANYO,GMN1,PROCE,GRUPO,USU,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                                       SELECT @ANYO,@GMN1,@PROCE,@GRUPO,USU,VISTA,@ID FROM  CONF_VISTAS_GRUPO WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO " & vbCrLf
sConsulta = sConsulta & "                                      ELSE" & vbCrLf
sConsulta = sConsulta & "                                                INSERT INTO CONF_VISTAS_GRUPO_ATRIB (ANYO,GMN1,PROCE,GRUPO,USU,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                                       SELECT @ANYO,@GMN1,@PROCE,GRUPO,USU,VISTA,@ID FROM  CONF_VISTAS_GRUPO WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "                                      INSERT INTO CONF_VISTAS_ALL_ATRIB (ANYO,GMN1,PROCE,USU,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                           SELECT @ANYO,@GMN1,@PROCE,USU,VISTA,@ID FROM  CONF_VISTAS_ALL WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "                                      IF @GRUPO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                                                 INSERT INTO CONF_VISTAS_ITEM_ATRIB (ANYO,GMN1,PROCE,GRUPO,USU,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                                      SELECT @ANYO,@GMN1,@PROCE,@GRUPO,USU,VISTA,@ID FROM  CONF_VISTAS_ITEM WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO " & vbCrLf
sConsulta = sConsulta & "                                      ELSE" & vbCrLf
sConsulta = sConsulta & "                                                 INSERT INTO CONF_VISTAS_ITEM_ATRIB (ANYO,GMN1,PROCE,GRUPO,USU,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                                      SELECT @ANYO,@GMN1,@PROCE,GRUPO,USU,VISTA,@ID FROM  CONF_VISTAS_ITEM WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                                    END--old es 2" & vbCrLf
sConsulta = sConsulta & "                  ELSE --@COUNT >0" & vbCrLf
sConsulta = sConsulta & "                     BEGIN" & vbCrLf
sConsulta = sConsulta & "                     SELECT @VISTAS=PLANTILLA_VISTAS FROM PROCE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "                     IF @VISTAS > 0" & vbCrLf
sConsulta = sConsulta & "                          IF  @AMBITOOLD>@AMBITO" & vbCrLf
sConsulta = sConsulta & "                               BEGIN" & vbCrLf
sConsulta = sConsulta & "                               IF @AMBITOOLD=3" & vbCrLf
sConsulta = sConsulta & "                                     begin --old=3" & vbCrLf
sConsulta = sConsulta & "                                           DELETE CONF_VISTAS_GRUPO_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ATRIB=@ID" & vbCrLf
sConsulta = sConsulta & "                                           DELETE CONF_VISTAS_ALL_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE  AND ATRIB=@ID" & vbCrLf
sConsulta = sConsulta & "                                           DELETE CONF_VISTAS_ITEM_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE  AND ATRIB=@ID" & vbCrLf
sConsulta = sConsulta & "                                           INSERT INTO CONF_VISTAS_PROCE_ATRIB (ANYO,GMN1,PROCE,USU,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                              SELECT @ANYO,@GMN1,@PROCE,USU,VISTA,@ID FROM  CONF_VISTAS_PROCE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "                                     end  --old=3" & vbCrLf
sConsulta = sConsulta & "                                END" & vbCrLf
sConsulta = sConsulta & "                          ELSE  --old < ambito" & vbCrLf
sConsulta = sConsulta & "                                 BEGIN " & vbCrLf
sConsulta = sConsulta & "                                 IF @AMBITOOLD=1" & vbCrLf
sConsulta = sConsulta & "                                     BEGIN --old=1" & vbCrLf
sConsulta = sConsulta & "                                        IF @AMBITO = 3" & vbCrLf
sConsulta = sConsulta & "                                                  begin --ambito=3" & vbCrLf
sConsulta = sConsulta & "                                                  DELETE CONF_VISTAS_PROCE_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ATRIB=@ID" & vbCrLf
sConsulta = sConsulta & "                                                  IF @GRUPO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                                                        INSERT INTO CONF_VISTAS_GRUPO_ATRIB (ANYO,GMN1,PROCE,GRUPO,USU,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                                          SELECT @ANYO,@GMN1,@PROCE,@GRUPO,USU,VISTA,@ID FROM  CONF_VISTAS_GRUPO WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO " & vbCrLf
sConsulta = sConsulta & "                                                  ELSE" & vbCrLf
sConsulta = sConsulta & "                                                        INSERT INTO CONF_VISTAS_GRUPO_ATRIB (ANYO,GMN1,PROCE,GRUPO,USU,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                                          SELECT @ANYO,@GMN1,@PROCE,GRUPO,USU,VISTA,@ID FROM  CONF_VISTAS_GRUPO WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE         " & vbCrLf
sConsulta = sConsulta & "                                                 INSERT INTO CONF_VISTAS_ALL_ATRIB (ANYO,GMN1,PROCE,USU,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                                     SELECT @ANYO,@GMN1,@PROCE,USU,VISTA,@ID FROM  CONF_VISTAS_ALL WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "                                                 IF @GRUPO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                                                     INSERT INTO CONF_VISTAS_ITEM_ATRIB (ANYO,GMN1,PROCE,GRUPO,USU,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                                         SELECT @ANYO,@GMN1,@PROCE,@GRUPO,USU,VISTA,@ID FROM  CONF_VISTAS_ITEM WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO " & vbCrLf
sConsulta = sConsulta & "                                                  ELSE" & vbCrLf
sConsulta = sConsulta & "                                                     INSERT INTO CONF_VISTAS_ITEM_ATRIB (ANYO,GMN1,PROCE,GRUPO,USU,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                                         SELECT @ANYO,@GMN1,@PROCE,GRUPO,USU,VISTA,@ID FROM  CONF_VISTAS_ITEM WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                                                 end--ambito=3" & vbCrLf
sConsulta = sConsulta & "                                     END --old=1" & vbCrLf
sConsulta = sConsulta & "                                ELSE " & vbCrLf
sConsulta = sConsulta & "                                    BEGIN  --old es 2" & vbCrLf
sConsulta = sConsulta & "                                      DELETE CONF_VISTAS_PROCE_ATRIB WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ATRIB=@ID" & vbCrLf
sConsulta = sConsulta & "                                      IF @GRUPO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                                                 INSERT INTO CONF_VISTAS_GRUPO_ATRIB (ANYO,GMN1,PROCE,GRUPO,USU,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                                       SELECT @ANYO,@GMN1,@PROCE,@GRUPO,USU,VISTA,@ID FROM  CONF_VISTAS_GRUPO WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO " & vbCrLf
sConsulta = sConsulta & "                                      ELSE" & vbCrLf
sConsulta = sConsulta & "                                                INSERT INTO CONF_VISTAS_GRUPO_ATRIB (ANYO,GMN1,PROCE,GRUPO,USU,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                                       SELECT @ANYO,@GMN1,@PROCE,GRUPO,USU,VISTA,@ID FROM  CONF_VISTAS_GRUPO WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "                                      INSERT INTO CONF_VISTAS_ALL_ATRIB (ANYO,GMN1,PROCE,USU,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                           SELECT @ANYO,@GMN1,@PROCE,USU,VISTA,@ID FROM  CONF_VISTAS_ALL WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "                                      IF @GRUPO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                                                 INSERT INTO CONF_VISTAS_ITEM_ATRIB (ANYO,GMN1,PROCE,GRUPO,USU,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                                      SELECT @ANYO,@GMN1,@PROCE,@GRUPO,USU,VISTA,@ID FROM  CONF_VISTAS_ITEM WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO " & vbCrLf
sConsulta = sConsulta & "                                      ELSE" & vbCrLf
sConsulta = sConsulta & "                                                 INSERT INTO CONF_VISTAS_ITEM_ATRIB (ANYO,GMN1,PROCE,GRUPO,USU,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                                      SELECT @ANYO,@GMN1,@PROCE,GRUPO,USU,VISTA,@ID FROM  CONF_VISTAS_ITEM WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                                    END--old es 2      " & vbCrLf
sConsulta = sConsulta & "                             END" & vbCrLf
sConsulta = sConsulta & "                        END " & vbCrLf
sConsulta = sConsulta & "               END --@ambito<>ambitoold" & vbCrLf
sConsulta = sConsulta & "         END -- @tabla=1" & vbCrLf
sConsulta = sConsulta & "IF @TABLA=2" & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "          IF @APLIC_PREC IS not  NULL  AND @ORDEN is  NULL " & vbCrLf
sConsulta = sConsulta & "                 BEGIN" & vbCrLf
sConsulta = sConsulta & "                     SELECT @ATRIB_PREC =ATRIB  FROM PLANTILLA_ATRIB WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "                     SELECT @ORDEN=ORDEN FROM PLANTILLA_ATRIB WHERE PLANT=@PLANT AND APLIC_PREC = @APLIC_PREC AND OP_PREC=@OP_PREC AND ATRIB =@ATRIB_PREC" & vbCrLf
sConsulta = sConsulta & "                     IF @ORDEN IS NULL" & vbCrLf
sConsulta = sConsulta & "                         BEGIN" & vbCrLf
sConsulta = sConsulta & "                           SET @APLI_PREC_V=@APLIC_PREC" & vbCrLf
sConsulta = sConsulta & "                           SET @ORDEN_V= 0" & vbCrLf
sConsulta = sConsulta & "                           WHILE @APLI_PREC_V < 3  AND @ORDEN_V= 0" & vbCrLf
sConsulta = sConsulta & "                                 BEGIN" & vbCrLf
sConsulta = sConsulta & "                                     SELECT  @ORDEN_V = MAX(ORDEN) FROM PLANTILLA_ATRIB WHERE  PLANT=@PLANT AND APLIC_PREC = @APLI_PREC_V" & vbCrLf
sConsulta = sConsulta & "                                      IF @ORDEN_V IS NULL" & vbCrLf
sConsulta = sConsulta & "                                         begin" & vbCrLf
sConsulta = sConsulta & "                                              SET @ORDEN_V= 0" & vbCrLf
sConsulta = sConsulta & "                                              SET @APLI_PREC_V=@APLI_PREC_V + 1" & vbCrLf
sConsulta = sConsulta & "                                         end" & vbCrLf
sConsulta = sConsulta & "                                END" & vbCrLf
sConsulta = sConsulta & "                          UPDATE PLANTILLA_ATRIB SET ORDEN = ORDEN +1 WHERE  APLIC_PREC < @APLIC_PREC AND PLANT=@PLANT" & vbCrLf
sConsulta = sConsulta & "                          SET @ORDEN=@ORDEN_V +1" & vbCrLf
sConsulta = sConsulta & "                        END" & vbCrLf
sConsulta = sConsulta & "                   else" & vbCrLf
sConsulta = sConsulta & "                      IF @APLIC_PREC IS not  NULL   " & vbCrLf
sConsulta = sConsulta & "                            UPDATE PLANTILLA_ATRIB SET ORDEN = ORDEN +1 WHERE  APLIC_PREC < @APLIC_PREC AND PLANT=@PLANT" & vbCrLf
sConsulta = sConsulta & "                 END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "           SELECT @POND=POND, @AMBITOOLD=AMBITO FROM PLANTILLA_ATRIB WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "           IF @INTRO=0 AND @POND=2 SET @POND=0" & vbCrLf
sConsulta = sConsulta & "               UPDATE PLANTILLA_ATRIB SET PREFBAJO=@PREFBAJO,INTRO=@INTRO,MAXNUM=@MAXNUM  ," & vbCrLf
sConsulta = sConsulta & "                    MINNUM=@MINNUM,MAXFEC=CONVERT(VARCHAR(10),@MAXFEC,111)," & vbCrLf
sConsulta = sConsulta & "                    MINFEC=CONVERT(VARCHAR(10),@MINFEC,111),INTERNO=@INTERNO,AMBITO=@AMBITO,OBLIG=@OBLIG," & vbCrLf
sConsulta = sConsulta & "                    OP_PREC=@OP_PREC,APLIC_PREC=@APLIC_PREC," & vbCrLf
sConsulta = sConsulta & "                    DEF_PREC=@DEF_PREC,POND=@POND,ORDEN=@ORDEN WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "          SELECT @FECACT=FECACT FROM PLANTILLA_ATRIB WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "          IF   @ATRIB IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                BEGIN" & vbCrLf
sConsulta = sConsulta & "                    SELECT @POND=POND FROM DEF_ATRIB WHERE ID=@ATRIB" & vbCrLf
sConsulta = sConsulta & "                    IF @INTRO=0 AND @POND=2 SET @POND=0" & vbCrLf
sConsulta = sConsulta & "                        UPDATE DEF_ATRIB SET PREFBAJO=@PREFBAJO,INTRO=@INTRO,MAXNUM=CAST(@MAXNUM AS VARCHAR(15)) ," & vbCrLf
sConsulta = sConsulta & "                            MINNUM=CAST(@MINNUM AS VARCHAR(15)),MAXFEC=CONVERT(VARCHAR(10),@MAXFEC,111)," & vbCrLf
sConsulta = sConsulta & "                            MINFEC=CONVERT(VARCHAR(10),@MINFEC,111),POND=@POND WHERE ID=@ATRIB" & vbCrLf
sConsulta = sConsulta & "                END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "     IF @AMBITO<>@AMBITOOLD" & vbCrLf
sConsulta = sConsulta & "       BEGIN --@ambito<>ambitoold           " & vbCrLf
sConsulta = sConsulta & "            IF  @AMBITOOLD>@AMBITO" & vbCrLf
sConsulta = sConsulta & "       begin" & vbCrLf
sConsulta = sConsulta & "             IF @AMBITOOLD=3" & vbCrLf
sConsulta = sConsulta & "                   begin --old=3" & vbCrLf
sConsulta = sConsulta & "                                              " & vbCrLf
sConsulta = sConsulta & "                         DELETE PLANTILLA_CONF_VISTAS_GRUPO_ATRIB WHERE PLANT=@PLANT AND ATRIB=@ID" & vbCrLf
sConsulta = sConsulta & "                         DELETE PLANTILLA_CONF_VISTAS_ALL_ATRIB WHERE PLANT=@PLANT AND ATRIB=@ID" & vbCrLf
sConsulta = sConsulta & "                                              " & vbCrLf
sConsulta = sConsulta & "                         INSERT INTO PLANTILLA_CONF_VISTAS_PROCE_ATRIB (PLANT,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                SELECT @PLANT,VISTA,@ID FROM  PLANTILLA_CONF_VISTAS_PROCE WHERE PLANT=@PLANT " & vbCrLf
sConsulta = sConsulta & "                   end  --old=3" & vbCrLf
sConsulta = sConsulta & "       end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "        ELSE  --old < ambito" & vbCrLf
sConsulta = sConsulta & "           IF @AMBITOOLD=1" & vbCrLf
sConsulta = sConsulta & "               BEGIN --old=1" & vbCrLf
sConsulta = sConsulta & "                    IF @AMBITO = 3" & vbCrLf
sConsulta = sConsulta & "                       begin --ambito=3" & vbCrLf
sConsulta = sConsulta & "                            DELETE PLANTILLA_CONF_VISTAS_PROCE_ATRIB WHERE PLANT=@PLANT AND ATRIB=@ID" & vbCrLf
sConsulta = sConsulta & "                            IF @GRUPO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                                   INSERT INTO PLANTILLA_CONF_VISTAS_GRUPO_ATRIB (PLANT,GRUPO,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                          SELECT @PLANT,@GRUPO,VISTA,@ID FROM  PLANTILLA_CONF_VISTAS_GRUPO WHERE PLANT=@PLANT AND GRUPO=@GRUPO " & vbCrLf
sConsulta = sConsulta & "                            ELSE" & vbCrLf
sConsulta = sConsulta & "                                   INSERT INTO PLANTILLA_CONF_VISTAS_GRUPO_ATRIB (PLANT,GRUPO,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                          SELECT @PLANT,GRUPO,VISTA,@ID FROM  PLANTILLA_CONF_VISTAS_GRUPO WHERE PLANT=@PLANT         " & vbCrLf
sConsulta = sConsulta & "                            INSERT INTO PLANTILLA_CONF_VISTAS_ALL_ATRIB (PLANT,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                   SELECT @PLANT,VISTA,@ID FROM PLANTILLA_CONF_VISTAS_ALL WHERE PLANT=@PLANT " & vbCrLf
sConsulta = sConsulta & "               end--ambito=3" & vbCrLf
sConsulta = sConsulta & "                 END --old=1" & vbCrLf
sConsulta = sConsulta & "             ELSE " & vbCrLf
sConsulta = sConsulta & "                 BEGIN  --old es 2" & vbCrLf
sConsulta = sConsulta & "                      DELETE PLANTILLA_CONF_VISTAS_PROCE_ATRIB WHERE PLANT=@PLANT AND ATRIB=@ID" & vbCrLf
sConsulta = sConsulta & "                      IF @GRUPO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                             INSERT INTO PLANTILLA_CONF_VISTAS_GRUPO_ATRIB (PLANT,GRUPO,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                    SELECT @PLANT,@GRUPO,VISTA,@ID FROM  PLANTILLA_CONF_VISTAS_GRUPO WHERE PLANT=@PLANT AND GRUPO=@GRUPO " & vbCrLf
sConsulta = sConsulta & "                      ELSE" & vbCrLf
sConsulta = sConsulta & "                             INSERT INTO PLANTILLA_CONF_VISTAS_GRUPO_ATRIB (PLANT,GRUPO,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                                    SELECT @PLANT,GRUPO,VISTA,@ID FROM  PLANTILLA_CONF_VISTAS_GRUPO WHERE PLANT=@PLANT " & vbCrLf
sConsulta = sConsulta & "                      INSERT INTO PLANTILLA_CONF_VISTAS_ALL_ATRIB (PLANT,VISTA,ATRIB) " & vbCrLf
sConsulta = sConsulta & "                             SELECT @PLANT,VISTA,@ID FROM PLANTILLA_CONF_VISTAS_ALL WHERE PLANT=@PLANT " & vbCrLf
sConsulta = sConsulta & "                                             " & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "                END--old es 2" & vbCrLf
sConsulta = sConsulta & "       END --@ambito<>ambitoold" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub


