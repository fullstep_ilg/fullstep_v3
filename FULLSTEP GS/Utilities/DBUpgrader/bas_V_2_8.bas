Attribute VB_Name = "bas_V_2_8"
Option Explicit
Private bTransaccionEnCurso As Boolean

Public Function CodigoDeActualizacion2_7_51_0A2_8_00_0() As Boolean
'EN REALIDAD AQUI SE CAMBI� DE LA 2.7.5 SR1 A 2.8
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String

    On Error GoTo error
        
    sConsulta = "BEGIN TRANSACTION"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    gRDOCon.Execute "SET XACT_ABORT OFF", rdExecDirect
    bTransaccionEnCurso = True

    'Modificar los campos COM y APE de TMP_PRES2 para igualarlos a los de TMP_PRES1. COM varchar 50 y APE varchar 100.
    frmProgreso.lblDetalle = "Modificamos los campos COM y APE de TMP_PRES2"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "ALTER TABLE [dbo].[TMP_PRES2] ALTER COLUMN COM VARCHAR(50) NULL"
    gRDOCon.Execute sConsulta, rdExecDirect
    sConsulta = "ALTER TABLE [dbo].[TMP_PRES2] ALTER COLUMN APE VARCHAR(100) NULL"
    gRDOCon.Execute sConsulta, rdExecDirect

    'A�adir una foreignkey FK_ART4_UNI entre ART4 y UNI
    frmProgreso.lblDetalle = "A�adimos la foreignkey FK_ART4_UNI"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[FK_ART4_UNI]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1) ALTER TABLE [dbo].[ART4] DROP CONSTRAINT [FK_ART4_UNI]"
    gRDOCon.Execute sConsulta, rdExecDirect
    sConsulta = "ALTER TABLE [dbo].[ART4] ADD"
    sConsulta = sConsulta & vbLf & "CONSTRAINT [FK_ART4_UNI] FOREIGN KEY"
    sConsulta = sConsulta & vbLf & "("
    sConsulta = sConsulta & vbLf & "[UNI]"
    sConsulta = sConsulta & vbLf & ") REFERENCES [dbo].[UNI] ("
    sConsulta = sConsulta & vbLf & "[COD]"
    sConsulta = sConsulta & vbLf & ")"
    gRDOCon.Execute sConsulta, rdExecDirect

     'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE VERSION SET NUM ='2.8.00.0'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    gRDOCon.Execute "COMMIT TRANSACTION", rdExecDirect
    bTransaccionEnCurso = False

    CodigoDeActualizacion2_7_51_0A2_8_00_0 = True
    Exit Function
    
error:
    
    If InStr(1, rdoErrors(rdoErrors.Count - 1).Description, "23000: [Microsoft][ODBC SQL Server Driver][SQL Server]ALTER TABLE statement conflicted with COLUMN FOREIGN KEY constraint 'FK_ART4_UNI'") Then
        Dim sUnidades As String
        Dim rdores As rdoResultset
        
        sConsulta = "SELECT DISTINCT UNI FROM ART4 WHERE UNI NOT IN (SELECT COD FROM UNI)"
        Set rdores = gRDOCon.OpenResultset(sConsulta, rdOpenForwardOnly, rdConcurReadOnly)
        sUnidades = NullToStr(rdores(0))
        rdores.MoveNext
        While Not rdores.EOF
            sUnidades = sUnidades & ", " & NullToStr(rdores(0))
            rdores.MoveNext
        Wend
        rdores.Close
        Set rdores = Nothing
        MsgBox "Existen en su sistema art�culos cuya unidad no existe en la tabla de unidades." & vbLf & "Modifique la unidad a los art�culos con unidad:" & vbLf & sUnidades, vbCritical, "FULLSTEP"
    Else
        MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    End If
    If bTransaccionEnCurso Then
        gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    CodigoDeActualizacion2_7_51_0A2_8_00_0 = False
End Function

Public Function CodigoDeActualizacion2_8_00_0_0A2_8_00_1() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String

    On Error GoTo error
        
    sConsulta = "BEGIN TRANSACTION"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    gRDOCon.Execute "SET XACT_ABORT OFF", rdExecDirect
    bTransaccionEnCurso = True

    'Modificamos el stored procedure SP_ITEMS
    frmProgreso.lblDetalle = "Modificamos el stored procedure SP_ITEMS"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[SP_ITEMS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[SP_ITEMS]"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    gRDOCon.Execute Modificar_Trigger_SP_ITEMS_2_8_00_1(), rdExecDirect


     'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE VERSION SET NUM ='2.8.00.1'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    gRDOCon.Execute "COMMIT TRANSACTION", rdExecDirect
    bTransaccionEnCurso = False

    CodigoDeActualizacion2_8_00_0_0A2_8_00_1 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
        gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_8_00_0_0A2_8_00_1 = False
End Function

Public Function CodigoDeActualizacion2_8_00_1_0A2_8_00_2() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String

    On Error GoTo error
        
    sConsulta = "BEGIN TRANSACTION"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    gRDOCon.Execute "SET XACT_ABORT OFF", rdExecDirect
    bTransaccionEnCurso = True

    'Eliminamos el campo CAMBIO de OFEMIN
    frmProgreso.lblDetalle = "Eliminamos el campo CAMBIO de OFEMIN"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "ALTER TABLE dbo.OFEMIN DROP COLUMN CAMBIO"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    
    'A�adimos el campo ULTOFE en OFEMIN
    frmProgreso.lblDetalle = "Eliminamos el campo CAMBIO de OFEMIN"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "ALTER TABLE dbo.OFEMIN ADD ULTOFE smallint NULL"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    
    'Modificamos el trigger para la update de ITEM_OFE
    frmProgreso.lblDetalle = "Modificamos el trigger ITEM_OFE_TG_UPD"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[ITEM_OFE_TG_UPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1) drop trigger [dbo].[ITEM_OFE_TG_UPD]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute ModificarITEM_OFE_TG_UPD_2_8_00_2, rdExecDirect
    
    
    'Creamos el trigger para la update de OFEMIN
    frmProgreso.lblDetalle = "Creamos el trigger para la UPDATE en OFEMIN"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[OFEMIN_TG_UPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1) drop trigger [dbo].[OFEMIN_TG_UPD]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearOFEMIN_TG_UPD_2_8_00_2, rdExecDirect


    'Modificamos el trigger para la DELETE en PROCE_OFE
    frmProgreso.lblDetalle = "Modificamos el trigger para la DELETE en PROCE_OFE"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[PROCE_OFE_TG_DEL]') and OBJECTPROPERTY(id, N'IsTrigger') = 1) drop trigger [dbo].[PROCE_OFE_TG_DEL]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute ModificarPROCE_OFE_TG_DEL_2_8_00_2, rdExecDirect


     'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE VERSION SET NUM ='2.8.00.2'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    gRDOCon.Execute "COMMIT TRANSACTION", rdExecDirect
    bTransaccionEnCurso = False

    CodigoDeActualizacion2_8_00_1_0A2_8_00_2 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
        gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_8_00_1_0A2_8_00_2 = False
End Function

Public Function CodigoDeActualizacion2_8_00_2_0A2_8_00_3() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim rdores As rdoResultset
Dim Q As rdoQuery
Dim sSQL As String
Dim i As Integer
Dim iResultado As Integer

    On Error GoTo error
        
    sConsulta = "BEGIN TRANSACTION"
    gRDOCon.Execute sConsulta, rdExecDirect

    gRDOCon.Execute "SET XACT_ABORT OFF", rdExecDirect
    bTransaccionEnCurso = True


    'Creamos el trigger para la update de OFEMIN
    frmProgreso.lblDetalle = "Modificamos el trigger para la UPDATE en OFEMIN"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[OFEMIN_TG_UPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1) drop trigger [dbo].[OFEMIN_TG_UPD]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearOFEMIN_TG_UPD_2_8_00_2, rdExecDirect

    'Eliminamos el trigger para la update de PROCE_PROVE
    frmProgreso.lblDetalle = "Eliminamos el trigger para la update de PROCE_PROVE"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[PROCE_PROVE_TG_UPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1) drop trigger [dbo].[PROCE_PROVE_TG_UPD]"
    gRDOCon.Execute sConsulta, rdExecDirect

    'Modificamos el SP SP_AR_ITEM_MES_PROY1
    frmProgreso.lblDetalle = "Modificamos el stored SP_AR_ITEM_MES_PROY1"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[SP_AR_ITEM_MES_PROY1]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[SP_AR_ITEM_MES_PROY1]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute Modificar_SP_AR_ITEM_MES_PROY1_2_8_00_3(), rdExecDirect

    'Modificamos el trigger para la update de ITEM
    frmProgreso.lblDetalle = "Modificamos el trigger para la UPDATE en ITEM"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[ITEM_TG_UPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1) drop trigger [dbo].[ITEM_TG_UPD]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearITEM_TG_UPD_2_8_00_3, rdExecDirect

    'Actualizamos los ahorros por presupuestos
    frmProgreso.lblDetalle = "Borramos las stored ITEM_PRES"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

    sConsulta = "DELETE FROM ITEM_PRESPROY FROM ITEM I INNER JOIN ITEM_PRESPROY P ON I.ANYO=P.ANYO AND I.GMN1=P.GMN1"
    sConsulta = sConsulta & "  AND I.PROCE=P.PROCE AND I.ID = P.ITEM"
    sConsulta = sConsulta & " WHERE P.ANYOPROY < Year(I.FECINI) OR P.ANYOPROY > Year(I.FECFIN)"
    gRDOCon.Execute sConsulta, rdExecDirect

    sConsulta = "DELETE FROM ITEM_PRESCON FROM ITEM i INNER JOIN ITEM_PRESCON P ON i.ANYO=P.ANYO AND i.GMN1=P.GMN1"
    sConsulta = sConsulta & "  AND i.PROCE=P.PROCE AND i.ID = P.ITEM"
    sConsulta = sConsulta & " WHERE P.ANYOPAR < Year(i.FECINI) OR P.ANYOPAR > Year(i.FECFIN)"
    gRDOCon.Execute sConsulta, rdExecDirect

    sConsulta = "COMMIT TRANSACTION"
    gRDOCon.Execute sConsulta, rdExecDirect
    frmProgreso.lblDetalle = "Borramos las stored PARCON"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

    sConsulta = "BEGIN TRANSACTION"
    gRDOCon.Execute sConsulta, rdExecDirect

    'Borramos los ahorros por presupuesto
    sConsulta = "DELETE FROM AR_ITEM_MES_PARCON1"
    gRDOCon.Execute sConsulta, rdExecDirect
    sConsulta = "DELETE FROM AR_ITEM_MES_PARCON2"
    gRDOCon.Execute sConsulta, rdExecDirect
    sConsulta = "DELETE FROM AR_ITEM_MES_PARCON3"
    gRDOCon.Execute sConsulta, rdExecDirect
    sConsulta = "DELETE FROM AR_ITEM_MES_PARCON4"
    gRDOCon.Execute sConsulta, rdExecDirect

    sConsulta = "COMMIT TRANSACTION"
    gRDOCon.Execute sConsulta, rdExecDirect

    frmProgreso.lblDetalle = "Modificamos las stored PARCON"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1


    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

    'Calculamos los ahorros
    sConsulta = "SELECT DISTINCT PROCE.ANYO,PROCE.GMN1,PROCE.COD FROM PROCE INNER JOIN ITEM_PRESCON ON PROCE.ANYO=ITEM_PRESCON.ANYO AND PROCE.GMN1=ITEM_PRESCON.GMN1 AND PROCE.COD=ITEM_PRESCON.PROCE WHERE PROCE.EST =11 OR PROCE.EST=12"
    Set rdores = gRDOCon.OpenResultset(sConsulta, rdOpenForwardOnly, rdConcurReadOnly)
    Dim AR1()

    'ReDim AR(0 To 2, 0 To 1)
    i = 0
    While Not rdores.EOF
        ReDim Preserve AR1(0 To 2, 0 To i)
        AR1(0, i) = rdores(0)
        AR1(1, i) = rdores(1)
        AR1(2, i) = rdores(2)
        i = i + 1
        rdores.MoveNext
    Wend
    rdores.Close
    Set rdores = Nothing

    If i > 0 Then
        sConsulta = "BEGIN TRANSACTION"
        gRDOCon.Execute sConsulta, rdExecDirect
        For i = 0 To UBound(AR1, 2)
            frmProgreso.lblDetalle = "PROCESO: " & AR1(0, i) & " - " & AR1(1, i) & " - " & AR1(2, i)
            frmProgreso.lblDetalle.Refresh
            frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
            If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max - 10 Then
                frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Min + 1
            End If
    
            sSQL = "{ CALL SP_AR_ITEM_MES_PARCON1 (?, ?, ?, ?) }"
            Set Q = gRDOCon.CreateQuery("CambiarParCon1", sSQL)
            Q.rdoParameters(0) = AR1(0, i)
            Q.rdoParameters(1) = AR1(1, i)
            Q.rdoParameters(2) = AR1(2, i)
            Q.rdoParameters(3).Direction = rdParamOutput
            ''' Ejecutar la SP
            Q.Execute
            iResultado = Q.rdoParameters(3).Value
            Q.Close
            Set Q = Nothing
            If iResultado <> 0 Then
                If bTransaccionEnCurso Then
                    gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
                End If
                MsgBox "Error en SP_AR_ITEM_MES_PARCON1", vbCritical + vbOKOnly
                CodigoDeActualizacion2_8_00_2_0A2_8_00_3 = False
                Exit Function
            End If
            sSQL = "{ CALL SP_AR_ITEM_MES_PARCON2 (?, ?, ?, ?) }"
            Set Q = gRDOCon.CreateQuery("CambiarParCon2", sSQL)
            Q.rdoParameters(0) = AR1(0, i)
            Q.rdoParameters(1) = AR1(1, i)
            Q.rdoParameters(2) = AR1(2, i)
            Q.rdoParameters(3).Direction = rdParamOutput
    
            ''' Ejecutar la SP
            Q.Execute
            iResultado = Q.rdoParameters(3).Value
            Q.Close
            Set Q = Nothing
            If iResultado <> 0 Then
                If bTransaccionEnCurso Then
                    gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
                End If
                MsgBox "Error en SP_AR_ITEM_MES_PARCON2", vbCritical + vbOKOnly
                CodigoDeActualizacion2_8_00_2_0A2_8_00_3 = False
                Exit Function
            End If
    
            sSQL = "{ CALL SP_AR_ITEM_MES_PARCON3 (?, ?, ?, ?) }"
            Set Q = gRDOCon.CreateQuery("CambiarParCon3", sSQL)
            Q.rdoParameters(0) = AR1(0, i)
            Q.rdoParameters(1) = AR1(1, i)
            Q.rdoParameters(2) = AR1(2, i)
            Q.rdoParameters(3).Direction = rdParamOutput
    
            ''' Ejecutar la SP
            Q.Execute
            iResultado = Q.rdoParameters(3).Value
            Q.Close
            Set Q = Nothing
            If iResultado <> 0 Then
                If bTransaccionEnCurso Then
                    gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
                End If
                MsgBox "Error en SP_AR_ITEM_MES_PARCON3", vbCritical + vbOKOnly
                CodigoDeActualizacion2_8_00_2_0A2_8_00_3 = False
                Exit Function
            End If
    
            sSQL = "{ CALL SP_AR_ITEM_MES_PARCON4 (?, ?, ?, ?) }"
            Set Q = gRDOCon.CreateQuery("CambiarParCon4", sSQL)
            Q.rdoParameters(0) = AR1(0, i)
            Q.rdoParameters(1) = AR1(1, i)
            Q.rdoParameters(2) = AR1(2, i)
            Q.rdoParameters(3).Direction = rdParamOutput
            ''' Ejecutar la SP
            Q.Execute
            iResultado = Q.rdoParameters(3).Value
            Q.Close
            Set Q = Nothing
            If iResultado <> 0 Then
                If bTransaccionEnCurso Then
                    gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
                End If
                MsgBox "Error en SP_AR_ITEM_MES_PARCON4", vbCritical + vbOKOnly
                CodigoDeActualizacion2_8_00_2_0A2_8_00_3 = False
                Exit Function
            End If
            sConsulta = "COMMIT TRANSACTION"
            gRDOCon.Execute sConsulta, rdExecDirect
            sConsulta = "BEGIN TRANSACTION"
            gRDOCon.Execute sConsulta, rdExecDirect
    
        Next
    
        sConsulta = "COMMIT TRANSACTION"
        gRDOCon.Execute sConsulta, rdExecDirect
    End If
    frmProgreso.lblDetalle = "Borramos las stored PARPROY"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

    sConsulta = "BEGIN TRANSACTION"
    gRDOCon.Execute sConsulta, rdExecDirect

    sConsulta = "DELETE FROM AR_ITEM_MES_PARPROY1"
    gRDOCon.Execute sConsulta, rdExecDirect
    sConsulta = "DELETE FROM AR_ITEM_MES_PARPROY2"
    gRDOCon.Execute sConsulta, rdExecDirect
    sConsulta = "DELETE FROM AR_ITEM_MES_PARPROY3"
    gRDOCon.Execute sConsulta, rdExecDirect
    sConsulta = "DELETE FROM AR_ITEM_MES_PARPROY4"
    gRDOCon.Execute sConsulta, rdExecDirect

    sConsulta = "COMMIT TRANSACTION"
    gRDOCon.Execute sConsulta, rdExecDirect

    frmProgreso.lblDetalle = "Modificamos las stored PARPROY"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

    
    sConsulta = "SELECT DISTINCT PROCE.ANYO,PROCE.GMN1,PROCE.COD FROM PROCE INNER JOIN ITEM_PRESPROY ON PROCE.ANYO=ITEM_PRESPROY.ANYO AND PROCE.GMN1=ITEM_PRESPROY.GMN1 AND PROCE.COD=ITEM_PRESPROY.PROCE WHERE PROCE.EST =11 OR PROCE.EST=12"
    Set rdores = gRDOCon.OpenResultset(sConsulta, rdOpenForwardOnly, rdConcurReadOnly)
    Dim AR()
    'Dim I As Integer
    'ReDim AR(0 To 2, 0 To 1)
    i = 0
    While Not rdores.EOF
        ReDim Preserve AR(0 To 2, 0 To i)
        AR(0, i) = rdores(0)
        AR(1, i) = rdores(1)
        AR(2, i) = rdores(2)
        i = i + 1
        rdores.MoveNext
    Wend
    rdores.Close
    Set rdores = Nothing
    
    sConsulta = "BEGIN TRANSACTION"
    gRDOCon.Execute sConsulta, rdExecDirect
        
    If i > 0 Then
        For i = 0 To UBound(AR, 2)
            frmProgreso.lblDetalle = "PROCESO: " & AR(0, i) & " - " & AR(1, i) & " - " & AR(2, i)
            frmProgreso.lblDetalle.Refresh
            frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
            If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max - 10 Then
                frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Min + 1
            End If
            
            sSQL = "{ CALL SP_AR_ITEM_MES_PROY1 (?, ?, ?, ?) }"
            Set Q = gRDOCon.CreateQuery("CambiarPROY1", sSQL)
            Q.rdoParameters(0) = AR(0, i)
            Q.rdoParameters(1) = AR(1, i)
            Q.rdoParameters(2) = AR(2, i)
            Q.rdoParameters(3).Direction = rdParamOutput
            ''' Ejecutar la SP
            Q.Execute
            iResultado = Q.rdoParameters(3).Value
            Q.Close
            Set Q = Nothing
            If iResultado <> 0 Then
                If bTransaccionEnCurso Then
                    gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
                End If
                MsgBox "Error en SP_AR_ITEM_MES_PROY1", vbCritical + vbOKOnly
                CodigoDeActualizacion2_8_00_2_0A2_8_00_3 = False
                Exit Function
            End If
            sSQL = "{ CALL SP_AR_ITEM_MES_PROY2 (?, ?, ?, ?) }"
            Set Q = gRDOCon.CreateQuery("CambiarPROY2", sSQL)
            Q.rdoParameters(0) = AR(0, i)
            Q.rdoParameters(1) = AR(1, i)
            Q.rdoParameters(2) = AR(2, i)
            Q.rdoParameters(3).Direction = rdParamOutput
            ''' Ejecutar la SP
            Q.Execute
            iResultado = Q.rdoParameters(3).Value
            Q.Close
            Set Q = Nothing
            If iResultado <> 0 Then
                If bTransaccionEnCurso Then
                    gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
                End If
                MsgBox "Error en SP_AR_ITEM_MES_PROY2", vbCritical + vbOKOnly
                CodigoDeActualizacion2_8_00_2_0A2_8_00_3 = False
                Exit Function
            End If
            
            sSQL = "{ CALL SP_AR_ITEM_MES_PROY3 (?, ?, ?, ?) }"
            Set Q = gRDOCon.CreateQuery("CambiarPROY3", sSQL)
            Q.rdoParameters(0) = AR(0, i)
            Q.rdoParameters(1) = AR(1, i)
            Q.rdoParameters(2) = AR(2, i)
            Q.rdoParameters(3).Direction = rdParamOutput
            ''' Ejecutar la SP
            Q.Execute
            iResultado = Q.rdoParameters(3).Value
            Q.Close
            Set Q = Nothing
            If iResultado <> 0 Then
                If bTransaccionEnCurso Then
                    gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
                End If
                MsgBox "Error en SP_AR_ITEM_MES_PROY3", vbCritical + vbOKOnly
                CodigoDeActualizacion2_8_00_2_0A2_8_00_3 = False
                Exit Function
            End If
            
            sSQL = "{ CALL SP_AR_ITEM_MES_PROY4 (?, ?, ?, ?) }"
            Set Q = gRDOCon.CreateQuery("CambiarPROY4", sSQL)
            Q.rdoParameters(0) = AR(0, i)
            Q.rdoParameters(1) = AR(1, i)
            Q.rdoParameters(2) = AR(2, i)
            Q.rdoParameters(3).Direction = rdParamOutput
            ''' Ejecutar la SP
            Q.Execute
            iResultado = Q.rdoParameters(3).Value
            Q.Close
            Set Q = Nothing
            If iResultado <> 0 Then
                If bTransaccionEnCurso Then
                    gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
                End If
                MsgBox "Error en SP_AR_ITEM_MES_PROY4", vbCritical + vbOKOnly
                CodigoDeActualizacion2_8_00_2_0A2_8_00_3 = False
                Exit Function
            End If
    
            sConsulta = "COMMIT TRANSACTION"
            gRDOCon.Execute sConsulta, rdExecDirect
            sConsulta = "BEGIN TRANSACTION"
            gRDOCon.Execute sConsulta, rdExecDirect
    
        Next
    End If
        
    'rdores.Close
    'Set rdores = Nothing
     'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE VERSION SET NUM ='2.8.00.3'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    gRDOCon.Execute "COMMIT TRANSACTION", rdExecDirect
    bTransaccionEnCurso = False

    CodigoDeActualizacion2_8_00_2_0A2_8_00_3 = True
    Exit Function
    
error:
'Resume Next

    If bTransaccionEnCurso Then
        gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_8_00_2_0A2_8_00_3 = False
    
End Function

Public Function CodigoDeActualizacion2_8_00_4_0A2_8_00_5() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
    
    On Error GoTo error
        
    sConsulta = "BEGIN TRANSACTION"
    gRDOCon.Execute sConsulta, rdExecDirect

    gRDOCon.Execute "SET XACT_ABORT OFF", rdExecDirect
    bTransaccionEnCurso = True

    frmProgreso.lblDetalle = "Modificamos el stored procedure ART1_COD"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[ART1_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[ART1_COD]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearProcedure_ART1_COD_2_8_05, rdExecDirect
    
    frmProgreso.lblDetalle = "Modificamos el stored procedure ART2_COD"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[ART2_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[ART2_COD]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearProcedure_ART2_COD_2_8_05, rdExecDirect
    
    frmProgreso.lblDetalle = "Modificamos el stored procedure ART3_COD"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[ART3_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[ART3_COD]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearProcedure_ART3_COD_2_8_05, rdExecDirect
    
    frmProgreso.lblDetalle = "Modificamos el stored procedure ART4_COD"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[ART4_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[ART4_COD]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearProcedure_ART4_COD_2_8_05, rdExecDirect
            
    frmProgreso.lblDetalle = "Modificamos el stored procedure GMN1_COD"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[GMN1_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[GMN1_COD]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearProcedure_GMN1_COD_2_8_05, rdExecDirect
    
    frmProgreso.lblDetalle = "Modificamos el stored procedure GMN2_COD"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[GMN2_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[GMN2_COD]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearProcedure_GMN2_COD_2_8_05, rdExecDirect
    
    frmProgreso.lblDetalle = "Modificamos el stored procedure GMN3_COD"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[GMN3_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[GMN3_COD]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearProcedure_GMN3_COD_2_8_05, rdExecDirect
    
    frmProgreso.lblDetalle = "Modificamos el stored procedure GMN4_COD"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[GMN4_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[GMN4_COD]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearProcedure_GMN4_COD_2_8_05, rdExecDirect

    frmProgreso.lblDetalle = "Modificamos el stored procedure PAG_COD"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[PAG_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[PAG_COD]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearProcedure_PAG_COD_2_8_05, rdExecDirect

    frmProgreso.lblDetalle = "Modificamos el stored procedure DEST_COD"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[DEST_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[DEST_COD]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearProcedure_DEST_COD_2_8_05, rdExecDirect

    frmProgreso.lblDetalle = "Modificamos el stored procedure UNI_COD"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[UNI_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[UNI_COD]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearProcedure_UNI_COD_2_8_05, rdExecDirect

    frmProgreso.lblDetalle = "Modificamos el stored procedure USU_COD"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[USU_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[USU_COD]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearProcedure_USU_COD_2_8_05, rdExecDirect

    frmProgreso.lblDetalle = "Modificamos el stored procedure PROCE_COD"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[PROCE_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[PROCE_COD]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearProcedure_PROCE_COD_2_8_05, rdExecDirect

    frmProgreso.lblDetalle = "Modificamos el stored procedure PROVE_COD"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[PROVE_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[PROVE_COD]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearProcedure_PROVE_COD_2_8_05, rdExecDirect

    
    frmProgreso.lblDetalle = "Modificamos el stored procedure PERF_COD"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[PERF_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[PERF_COD]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearProcedure_PERF_COD_2_8_05, rdExecDirect
    
    frmProgreso.lblDetalle = "Modificamos el stored procedure PRES_3_CON1_COD"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[PRES_3_CON1_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[PRES_3_CON1_COD]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearProcedure_PRES_3_CON1_COD_2_8_05, rdExecDirect

    frmProgreso.lblDetalle = "Modificamos el stored procedure PRES_3_CON2_COD"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[PRES_3_CON2_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[PRES_3_CON2_COD]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearProcedure_PRES_3_CON2_COD_2_8_05, rdExecDirect

    frmProgreso.lblDetalle = "Modificamos el stored procedure PRES_3_CON3_COD"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[PRES_3_CON3_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[PRES_3_CON3_COD]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearProcedure_PRES_3_CON3_COD_2_8_05, rdExecDirect

    frmProgreso.lblDetalle = "Modificamos el stored procedure PRES_3_CON4_COD"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[PRES_3_CON4_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[PRES_3_CON4_COD]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearProcedure_PRES_3_CON4_COD_2_8_05, rdExecDirect

    frmProgreso.lblDetalle = "Modificamos el stored procedure PRES_4_CON1_COD"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[PRES_4_CON1_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[PRES_4_CON1_COD]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearProcedure_PRES_4_CON1_COD_2_8_05, rdExecDirect

    frmProgreso.lblDetalle = "Modificamos el stored procedure PRES_4_CON2_COD"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[PRES_4_CON2_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[PRES_4_CON2_COD]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearProcedure_PRES_4_CON2_COD_2_8_05, rdExecDirect

    frmProgreso.lblDetalle = "Modificamos el stored procedure PRES_4_CON3_COD"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[PRES_4_CON3_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[PRES_4_CON3_COD]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearProcedure_PRES_4_CON3_COD_2_8_05, rdExecDirect

    frmProgreso.lblDetalle = "Modificamos el stored procedure PRES_4_CON4_COD"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[PRES_4_CON4_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[PRES_4_CON4_COD]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearProcedure_PRES_4_CON4_COD_2_8_05, rdExecDirect
    
    frmProgreso.lblDetalle = "Creamos el stored procedure SP_REBUILD_PUB"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[SP_REBUILD_PUB]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[SP_REBUILD_PUB]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute Crear_SP_REBUILD_PUB_2_8_005, rdExecDirect

    sConsulta = "UPDATE VERSION SET NUM ='2.8.00.5'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    gRDOCon.Execute "COMMIT TRANSACTION", rdExecDirect
    bTransaccionEnCurso = False

    CodigoDeActualizacion2_8_00_4_0A2_8_00_5 = True
    Exit Function
    
error:
'Resume Next

    If bTransaccionEnCurso Then
        gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_8_00_4_0A2_8_00_5 = False
    

End Function
Public Function CodigoDeActualizacion2_8_00_5_0A2_8_50_0() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String

    On Error GoTo error
        
    sConsulta = "BEGIN TRANSACTION"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    gRDOCon.Execute "SET XACT_ABORT OFF", rdExecDirect
    bTransaccionEnCurso = True
    
    'A�adimos el campo EST a la tabla ITEM
    frmProgreso.lblDetalle = "A�adimos el campo EST a la tabla ITEM"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "ALTER TABLE [dbo].[ITEM] ADD EST tinyint NULL"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    'Modificar el trigger PROCE_PROVE_PET_TG_INS
    frmProgreso.lblDetalle = "Modificamos el trigger PROCE_PROVE_PET_TG_INS"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[PROCE_PROVE_PET_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1) drop trigger [dbo].[PROCE_PROVE_PET_TG_INS]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute ModificarTriggerPROCE_PROVE_PET_TG_INS, rdExecDirect
        
    'Modificar la Stored procedure SP_DEVOLVER_PROCESOS_PROVE_WEB
    frmProgreso.lblDetalle = "Modificamos la Stored procedure SP_DEVOLVER_PROCESOS_PROVE_WEB"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[SP_DEVOLVER_PROCESOS_PROVE_WEB]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[SP_DEVOLVER_PROCESOS_PROVE_WEB]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute ModificarSP_DEVOLVER_PROCESOS_PROVE_WEB_2_8_5_0, rdExecDirect
    
    'Modificar la Stored procedure NUM_PROCE_PUB
    frmProgreso.lblDetalle = "Modificamos la Stored procedure NUM_PROCE_PUB"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[NUM_PROCE_PUB]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[NUM_PROCE_PUB]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute ModificarNUM_PROCE_PUB_2_8_5_0, rdExecDirect

    'A�adimos el campo CERRADO a la tabla PROCE_OFE
    frmProgreso.lblDetalle = "A�adimos el campo CERRADO a la tabla PROCE_OFE"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "ALTER TABLE [dbo].[PROCE_OFE] ADD CERRADO [tinyint] NOT NULL DEFAULT (0)"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    'A�adimos el campo CERRADO a la tabla PROCE_PROVE_PET
    frmProgreso.lblDetalle = "A�adimos el campo CERRADO a la tabla PROCE_PROVE_PET"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "ALTER TABLE [dbo].[PROCE_PROVE_PET] ADD CERRADO [tinyint] NOT NULL DEFAULT (0)"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    'Modificamos la stored procedure SP_ITEMS
    frmProgreso.lblDetalle = "Modificamos la Stored procedure SP_ITEMS"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[SP_ITEMS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[SP_ITEMS]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute ModificarSP_ITEMS_2_8_5_0, rdExecDirect
    
    'Modificamos la stored procedure SP_LINEAS
    frmProgreso.lblDetalle = "Modificamos la Stored procedure SP_LINEAS"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[SP_LINEAS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[SP_LINEAS]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute ModificarSP_LINEAS_2_8_5_0, rdExecDirect
        
    'Modificamos los campos COM y APE de TMP_PRES2 para igualarlos a los de TMP_PRES1. COM varchar 50 y APE varchar 100.
    frmProgreso.lblDetalle = "Modificamos los campos COM y APE de TMP_PRES2"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "ALTER TABLE [dbo].[TMP_PRES2] ALTER COLUMN COM VARCHAR(50) NULL"
    gRDOCon.Execute sConsulta, rdExecDirect
    sConsulta = "ALTER TABLE [dbo].[TMP_PRES2] ALTER COLUMN APE VARCHAR(100) NULL"
    gRDOCon.Execute sConsulta, rdExecDirect

    
    'Nuevo campo CERRADO en la tabla ITEM_ADJREU (bit, allow nulls, default 0)
    frmProgreso.lblDetalle = "A�adimos ell campo CERRADO a la tabla ITEM_ADJREU"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "ALTER TABLE [dbo].[ITEM_ADJREU] ADD CERRADO [bit] NULL DEFAULT (0)"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    'Modificar el store procedure SP_CIERRE
    frmProgreso.lblDetalle = "Modificamos la Stored procedure SP_CIERRE"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[SP_CIERRE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[SP_CIERRE]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute ModificarSP_CIERRE_2_8_5_0, rdExecDirect
    
    'Modificar el store procedure SP_AR_PROCE
    frmProgreso.lblDetalle = "Modificamos la Stored procedure SP_AR_PROCE"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[SP_AR_PROCE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[SP_AR_PROCE]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute ModificarSP_AR_PROCE_2_8_5_0, rdExecDirect
    
    'Modificar el store procedure SP_AR_ITEM
    frmProgreso.lblDetalle = "Modificamos la Stored procedure SP_AR_ITEM"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[SP_AR_ITEM]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[SP_AR_ITEM]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute ModificarSP_AR_ITEM_2_8_5_0, rdExecDirect
        
    'Modificar el store procedure SP_AR_ITEM_MES
    frmProgreso.lblDetalle = "Modificamos la Stored procedure SP_AR_ITEM_MES"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[SP_AR_ITEM_MES]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[SP_AR_ITEM_MES]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute ModificarSP_AR_ITEM_MES_2_8_5_0, rdExecDirect
    
    'Modificar el store procedure SP_AR_ITEM_PROVE
    frmProgreso.lblDetalle = "Modificamos la Stored procedure SP_AR_ITEM_PROVE"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[SP_AR_ITEM_PROVE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[SP_AR_ITEM_PROVE]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute ModificarSP_AR_ITEM_PROVE_2_8_5_0, rdExecDirect
    
    'Modificar el store procedure SP_AR_ITEM_PRESCON3
    frmProgreso.lblDetalle = "Modificamos la Stored procedure SP_AR_ITEM_PRESCON3"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[SP_AR_ITEM_PRESCON3]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[SP_AR_ITEM_PRESCON3]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute ModificarSP_AR_ITEM_PRESCON3_2_8_5_0, rdExecDirect
    
    'Modificar el store procedure SP_AR_ITEM_PRESCON4
    frmProgreso.lblDetalle = "Modificamos la Stored procedure SP_AR_ITEM_PRESCON4"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[SP_AR_ITEM_PRESCON4]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[SP_AR_ITEM_PRESCON4]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute ModificarSP_AR_ITEM_PRESCON4_2_8_5_0, rdExecDirect

    'Nuevos campos en la tabla COM
    frmProgreso.lblDetalle = "Nuevos campos en la tabla COM"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "ALTER TABLE [dbo].[COM] ADD CAR varchar (50) NULL "
    gRDOCon.Execute sConsulta, rdExecDirect
    sConsulta = "ALTER TABLE [dbo].[COM] ADD UON1 varchar (" & gLongitudesDeCodigos.giLongCodUON1 & ") NULL "
    gRDOCon.Execute sConsulta, rdExecDirect
    sConsulta = "ALTER TABLE [dbo].[COM] ADD UON2 varchar (" & gLongitudesDeCodigos.giLongCodUON2 & ") NULL "
    gRDOCon.Execute sConsulta, rdExecDirect
    sConsulta = "ALTER TABLE [dbo].[COM] ADD UON3 varchar (" & gLongitudesDeCodigos.giLongCodUON3 & ") NULL "
    gRDOCon.Execute sConsulta, rdExecDirect
    sConsulta = "ALTER TABLE [dbo].[COM] ADD DEP varchar (" & gLongitudesDeCodigos.giLongCodDEP & ") NULL "
    gRDOCon.Execute sConsulta, rdExecDirect
    sConsulta = "ALTER TABLE [dbo].[COM] ADD TFNO2 varchar (20) NULL "
    gRDOCon.Execute sConsulta, rdExecDirect
    'PROVISIONAL PARA LA FUSION
    sConsulta = "ALTER TABLE [dbo].[COM] ADD ANYADIDO [tinyint] NULL"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    'Nuevo campo en la tabla PER
    frmProgreso.lblDetalle = "Nuevos campo EQP en la tabla PER"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "ALTER TABLE [dbo].[PER] ADD EQP varchar (" & gLongitudesDeCodigos.giLongCodEQP & ") NULL "
    gRDOCon.Execute sConsulta, rdExecDirect
    
    
    'Modificar el store procedure PER_COD
    frmProgreso.lblDetalle = "Modificamos la Stored procedure PER_COD"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[PER_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[PER_COD]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute ModificarPER_COD2_8_5_0, rdExecDirect
    
    'Actualizar constraint en la tabla USU
    frmProgreso.lblDetalle = "Modificamos la check constraint de la tabla USU"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "ALTER TABLE [dbo].[USU] DROP CONSTRAINT [CK_USU_01]"
    gRDOCon.Execute sConsulta, rdExecDirect
    sConsulta = "ALTER TABLE [dbo].[USU] WITH NOCHECK ADD CONSTRAINT [CK_USU_01] CHECK (([TIPO] = 1 or [TIPO] = 2 and ((not([EQP] is null))) or [TIPO] = 3 and ((not([PER] is null))) or [TIPO] = 4 and ((not([PROVE] is null))) or [TIPO] = 5 and ((not([NOM] is null)))))"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    sConsulta = "UPDATE PROCE SET EST=13 WHERE EST=12"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    sConsulta = "UPDATE PROCE SET EST=12 WHERE EST=11"
    gRDOCon.Execute sConsulta, rdExecDirect
    
     'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE VERSION SET NUM ='2.8.50.0'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    gRDOCon.Execute "COMMIT TRANSACTION", rdExecDirect
    bTransaccionEnCurso = False

    CodigoDeActualizacion2_8_00_5_0A2_8_50_0 = True
    Exit Function
    
error:
'Resume Next

    If bTransaccionEnCurso Then
        gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_8_00_5_0A2_8_50_0 = False
    
End Function

Public Function CodigoDeActualizacion2_8_00_3_0A2_8_00_4() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
    
    On Error GoTo error
        
    sConsulta = "BEGIN TRANSACTION"
    gRDOCon.Execute sConsulta, rdExecDirect

    gRDOCon.Execute "SET XACT_ABORT OFF", rdExecDirect
    bTransaccionEnCurso = True


    'Creamos el trigger para la update de OFEMIN
    frmProgreso.lblDetalle = "Modificamos el stored procedure GMN1_COD"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[GMN1_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[GMN1_COD]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearNuevoGMN1_COD_2_8_03, rdExecDirect
    
    frmProgreso.lblDetalle = "Modificamos el stored procedure GMN2_COD"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[GMN2_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[GMN2_COD]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearNuevoGMN2_COD_2_8_03, rdExecDirect
    
    frmProgreso.lblDetalle = "Modificamos el stored procedure GMN3_COD"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[GMN3_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[GMN3_COD]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearNuevoGMN3_COD_2_8_03, rdExecDirect
    
    frmProgreso.lblDetalle = "Modificamos el stored procedure GMN4_COD"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[GMN4_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[GMN4_COD]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearNuevoGMN4_COD_2_8_03, rdExecDirect
    
    frmProgreso.lblDetalle = "Insertamos en ACC_DEPEN"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "DELETE FROM ACC_DEPEN WHERE ACC=17005 AND EST =1 AND ACC_DEPEN=17000"
    gRDOCon.Execute sConsulta, rdExecDirect
        
    sConsulta = "INSERT INTO ACC_DEPEN (ACC,EST,ACC_DEPEN,EST_DEPEN) VALUES (17005,1,17000,1)"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    sConsulta = "DELETE FROM ACC_DEPEN WHERE ACC=17006 AND EST =1 AND ACC_DEPEN=17000"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    sConsulta = "INSERT INTO ACC_DEPEN (ACC,EST,ACC_DEPEN,EST_DEPEN) VALUES (17006,1,17000,1)"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    sConsulta = "UPDATE PROCE_OFE SET LEIDO=1  WHERE LEIDO=0 AND ULT<>1"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    sConsulta = "UPDATE PROCE_OFE SET LEIDO=1 FROM PROCE_OFE AS O INNER JOIN PROCE AS P ON O.ANYO=P.ANYO AND O.GMN1 =P.GMN1 AND O.PROCE=P.COD WHERE P.EST >10 AND O.LEIDO=0"
    gRDOCon.Execute sConsulta, rdExecDirect
        
    frmProgreso.lblDetalle = "Modificamos el stored procedure SP_MODIF_PROCE_OFE"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[SP_MODIF_PROCE_OFE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[SP_MODIF_PROCE_OFE]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute ModifSP_MODIF_PROCE_OFE_2_8_03, rdExecDirect
    
    frmProgreso.lblDetalle = "Modificamos el stored procedure SP_ITEMS"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[SP_ITEMS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[SP_ITEMS]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute Modificar_Trigger_SP_ITEMS_2_8_00_1, rdExecDirect
    

    sConsulta = "UPDATE VERSION SET NUM ='2.8.00.4'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    gRDOCon.Execute "COMMIT TRANSACTION", rdExecDirect
    bTransaccionEnCurso = False

    CodigoDeActualizacion2_8_00_3_0A2_8_00_4 = True
    Exit Function
    
error:
'Resume Next

    If bTransaccionEnCurso Then
        gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_8_00_3_0A2_8_00_4 = False
    
End Function
Public Function CodigoDeActualizacion2_8_50_0A2_8_50_1() As Boolean
Dim rdores As rdoResultset
Dim sConsulta As String

    
    sConsulta = "SELECT count(*) FROM COM"
    Set rdores = gRDOCon.OpenResultset(sConsulta, rdOpenForwardOnly, rdConcurReadOnly, rdExecDirect)
    If rdores(0).Value = 0 Then
        rdores.Close
        Set rdores = Nothing
        sConsulta = "UPDATE VERSION SET NUM ='2.8.50.1'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
        gRDOCon.Execute sConsulta, rdExecDirect
        CodigoDeActualizacion2_8_50_0A2_8_50_1 = True
        Exit Function
    End If
        
    
    'Fusionar los compradores en el organigrama
    frmFusionarComp.Show 1
    
    sConsulta = "SELECT COUNT(*) FROM COM WHERE ANYADIDO IS NULL"
    Set rdores = gRDOCon.OpenResultset(sConsulta, rdOpenForwardOnly, rdConcurReadOnly)
    
    If rdores(0).Value = 0 Then
        rdores.Close
        Set rdores = Nothing
        
        gRDOCon.Execute "BEGIN TRANSACTION", rdExecDirect
        bTransaccionEnCurso = True
        
        sConsulta = "ALTER TABLE [dbo].[COM] DROP COLUMN ANYADIDO"
        gRDOCon.Execute sConsulta, rdExecDirect
    
        sConsulta = "UPDATE VERSION SET NUM ='2.8.50.1'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
        gRDOCon.Execute sConsulta, rdExecDirect
        
        gRDOCon.Execute "COMMIT TRANSACTION", rdExecDirect
        bTransaccionEnCurso = False
    
        CodigoDeActualizacion2_8_50_0A2_8_50_1 = True
        Exit Function
    Else
        rdores.Close
        Set rdores = Nothing

        MsgBox "Existen compradores fuera de la estructura de la organizaci�n." & vbLf & "La versi�n no se actualizar�.", vbCritical, "FULLSTEP"
        CodigoDeActualizacion2_8_50_0A2_8_50_1 = False
        Exit Function
        
    End If
    
error:
'Resume Next

    If bTransaccionEnCurso Then
        gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_8_50_0A2_8_50_1 = False
    
End Function
    
Public Function CodigoDeActualizacion2_8_50_1A2_8_50_2() As Boolean
Dim sConsulta As String

    gRDOCon.Execute "BEGIN TRANSACTION", rdExecDirect
    bTransaccionEnCurso = True
    
    'A�adir en COM la foreign key FK_COM_PER a la tabla PER
    
    frmProgreso.lblDetalle = "A�adir en COM la foreign key FK_COM_PER a la tabla PER"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "ALTER TABLE [dbo].[COM] ADD"
    sConsulta = sConsulta & vbLf & "CONSTRAINT [FK_COM_PER] FOREIGN KEY"
    sConsulta = sConsulta & vbLf & "("
    sConsulta = sConsulta & vbLf & "[COD]"
    sConsulta = sConsulta & vbLf & ") REFERENCES [dbo].[PER] ("
    sConsulta = sConsulta & vbLf & "[COD]"
    sConsulta = sConsulta & vbLf & ")"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    'Eliminar la foreign key USU_FK_COM
    frmProgreso.lblDetalle = "Eliminar la foreign key USU_FK_COM"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "ALTER TABLE [dbo].[USU] DROP CONSTRAINT [USU_FK_COM]"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    'Crear la nueva foreign key FK_USU_EQP
    frmProgreso.lblDetalle = "Crear la nueva foreign key FK_USU_EQP"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "ALTER TABLE [dbo].[USU] ADD"
    sConsulta = sConsulta & vbLf & "CONSTRAINT [FK_USU_EQP] FOREIGN KEY"
    sConsulta = sConsulta & vbLf & "("
    sConsulta = sConsulta & vbLf & "[EQP]"
    sConsulta = sConsulta & vbLf & ") REFERENCES [dbo].[EQP] ("
    sConsulta = sConsulta & vbLf & "[COD]"
    sConsulta = sConsulta & vbLf & ")"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    
    
    
    'Antes de eliminar el campo COM de USU hay que actualizar la tabla y pasar los valores de com a per
    'donde per era null (update usu set per=com where per is null and (tipo=2 or tipo=3))
    frmProgreso.lblDetalle = "Actualizamos la tabla USU"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "UPDATE USU SET PER=COM WHERE PER IS NULL AND (TIPO=2 OR TIPO=3)"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    
    sConsulta = "ALTER TABLE dbo.USU DROP CONSTRAINT CK_USU_01"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    sConsulta = "ALTER TABLE dbo.USU WITH NOCHECK ADD CONSTRAINT CK_USU_01 Check(([TIPO] = 1 Or [TIPO] = 2 And ((Not ([PER] Is Null))) Or [TIPO] = 3 And ((Not ([PER] Is Null))) Or [TIPO] = 4 And ((Not ([PROVE] Is Null))) Or [TIPO] = 5 And ((Not ([NOM] Is Null)))))"
    gRDOCon.Execute sConsulta, rdExecDirect


    'Eliminar el campo COM de la tabla de USU
    sConsulta = "ALTER TABLE [dbo].[USU] DROP COLUMN COM"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    'Modificar el store procedure COM_COD
    frmProgreso.lblDetalle = "Modificamos la Stored procedure COM_COD"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[COM_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[COM_COD]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute ModificarCOM_COD_2_8_5_0, rdExecDirect
    
    
    'Introducimos el nuevo valor de versi�n
    sConsulta = "UPDATE VERSION SET NUM ='2.8.50.2'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    gRDOCon.Execute "COMMIT TRANSACTION", rdExecDirect
    bTransaccionEnCurso = False

    CodigoDeActualizacion2_8_50_1A2_8_50_2 = True
    Exit Function
    
error:
    If bTransaccionEnCurso Then
        gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_8_50_1A2_8_50_2 = False
End Function


Public Function CodigoDeActualizacion2_8_50_2A2_8_50_3() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
    
    On Error GoTo error
        
    sConsulta = "BEGIN TRANSACTION"
    gRDOCon.Execute sConsulta, rdExecDirect

    gRDOCon.Execute "SET XACT_ABORT OFF", rdExecDirect
    bTransaccionEnCurso = True

    frmProgreso.lblDetalle = "Modificamos el stored procedure ART1_COD"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[ART1_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[ART1_COD]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearProcedure_ART1_COD_2_8_03, rdExecDirect
    
    frmProgreso.lblDetalle = "Modificamos el stored procedure ART2_COD"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[ART2_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[ART2_COD]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearProcedure_ART2_COD_2_8_03, rdExecDirect
    
    frmProgreso.lblDetalle = "Modificamos el stored procedure ART3_COD"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[ART3_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[ART3_COD]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearProcedure_ART3_COD_2_8_03, rdExecDirect
    
    frmProgreso.lblDetalle = "Modificamos el stored procedure ART4_COD"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[ART4_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[ART4_COD]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearProcedure_ART4_COD_2_8_03, rdExecDirect
            
    frmProgreso.lblDetalle = "Modificamos el stored procedure GMN1_COD"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[GMN1_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[GMN1_COD]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearProcedure_GMN1_COD_2_8_03, rdExecDirect
    
    frmProgreso.lblDetalle = "Modificamos el stored procedure GMN2_COD"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[GMN2_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[GMN2_COD]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearProcedure_GMN2_COD_2_8_03, rdExecDirect
    
    frmProgreso.lblDetalle = "Modificamos el stored procedure GMN3_COD"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[GMN3_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[GMN3_COD]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearProcedure_GMN3_COD_2_8_03, rdExecDirect
    
    frmProgreso.lblDetalle = "Modificamos el stored procedure GMN4_COD"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[GMN4_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[GMN4_COD]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearProcedure_GMN4_COD_2_8_03, rdExecDirect

    frmProgreso.lblDetalle = "Modificamos el stored procedure PAG_COD"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[PAG_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[PAG_COD]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearProcedure_PAG_COD_2_8_03, rdExecDirect

    frmProgreso.lblDetalle = "Modificamos el stored procedure DEST_COD"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[DEST_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[DEST_COD]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearProcedure_DEST_COD_2_8_03, rdExecDirect

    frmProgreso.lblDetalle = "Modificamos el stored procedure UNI_COD"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[UNI_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[UNI_COD]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearProcedure_UNI_COD_2_8_03, rdExecDirect

    frmProgreso.lblDetalle = "Modificamos el stored procedure USU_COD"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[USU_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[USU_COD]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearProcedure_USU_COD_2_8_03, rdExecDirect

    frmProgreso.lblDetalle = "Modificamos el stored procedure PROCE_COD"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[PROCE_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[PROCE_COD]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearProcedure_PROCE_COD_2_8_03, rdExecDirect

    frmProgreso.lblDetalle = "Modificamos el stored procedure PROVE_COD"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[PROVE_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[PROVE_COD]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearProcedure_PROVE_COD_2_8_03, rdExecDirect

    
    frmProgreso.lblDetalle = "Modificamos el stored procedure PERF_COD"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[PERF_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[PERF_COD]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearProcedure_PERF_COD_2_8_03, rdExecDirect
    
    frmProgreso.lblDetalle = "Modificamos el stored procedure PRES_3_CON1_COD"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[PRES_3_CON1_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[PRES_3_CON1_COD]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearProcedure_PRES_3_CON1_COD_2_8_03, rdExecDirect

    frmProgreso.lblDetalle = "Modificamos el stored procedure PRES_3_CON2_COD"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[PRES_3_CON2_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[PRES_3_CON2_COD]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearProcedure_PRES_3_CON2_COD_2_8_03, rdExecDirect

    frmProgreso.lblDetalle = "Modificamos el stored procedure PRES_3_CON3_COD"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[PRES_3_CON3_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[PRES_3_CON3_COD]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearProcedure_PRES_3_CON3_COD_2_8_03, rdExecDirect

    frmProgreso.lblDetalle = "Modificamos el stored procedure PRES_3_CON4_COD"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[PRES_3_CON4_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[PRES_3_CON4_COD]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearProcedure_PRES_3_CON4_COD_2_8_03, rdExecDirect

    frmProgreso.lblDetalle = "Modificamos el stored procedure PRES_4_CON1_COD"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[PRES_4_CON1_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[PRES_4_CON1_COD]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearProcedure_PRES_4_CON1_COD_2_8_03, rdExecDirect

    frmProgreso.lblDetalle = "Modificamos el stored procedure PRES_4_CON2_COD"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[PRES_4_CON2_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[PRES_4_CON2_COD]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearProcedure_PRES_4_CON2_COD_2_8_03, rdExecDirect

    frmProgreso.lblDetalle = "Modificamos el stored procedure PRES_4_CON3_COD"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[PRES_4_CON3_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[PRES_4_CON3_COD]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearProcedure_PRES_4_CON3_COD_2_8_03, rdExecDirect

    frmProgreso.lblDetalle = "Modificamos el stored procedure PRES_4_CON4_COD"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[PRES_4_CON4_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[PRES_4_CON4_COD]"
    gRDOCon.Execute sConsulta, rdExecDirect
    gRDOCon.Execute CrearProcedure_PRES_4_CON4_COD_2_8_03, rdExecDirect

    sConsulta = "UPDATE VERSION SET NUM ='2.8.50.3'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    gRDOCon.Execute sConsulta, rdExecDirect
    
    gRDOCon.Execute "COMMIT TRANSACTION", rdExecDirect
    bTransaccionEnCurso = False

    CodigoDeActualizacion2_8_50_2A2_8_50_3 = True
    Exit Function
    
error:
'Resume Next

    If bTransaccionEnCurso Then
        gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_8_50_2A2_8_50_3 = False
    

End Function


Private Function Modificar_Trigger_SP_ITEMS_2_8_00_1() As String
Dim sConsulta As String

sConsulta = "CREATE PROCEDURE  SP_ITEMS  (@ANYO INT,@GMN1 VARCHAR(50),@COD INT,@PROVEEDOR BIT,@CODMON varchar(50))  AS  "
sConsulta = sConsulta & vbCrLf & "BEGIN  "
sConsulta = sConsulta & vbCrLf & "SET CONCAT_NULL_YIELDS_NULL OFF  "
sConsulta = sConsulta & vbCrLf & "DECLARE @EQUIV FLOAT  "
sConsulta = sConsulta & vbCrLf & "DECLARE @EQUIVPROC FLOAT  "
sConsulta = sConsulta & vbCrLf & ""
sConsulta = sConsulta & vbCrLf & "SELECT @EQUIVPROC = CAMBIO FROM PROCE  WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@COD"
sConsulta = sConsulta & vbCrLf & ""
sConsulta = sConsulta & vbCrLf & "SELECT @EQUIV= MON.EQUIV  FROM MON WHERE MON.COD=@CODMON  "
sConsulta = sConsulta & vbCrLf & ""
sConsulta = sConsulta & vbCrLf & ""
sConsulta = sConsulta & vbCrLf & ""
sConsulta = sConsulta & vbCrLf & ""
sConsulta = sConsulta & vbCrLf & "    SELECT ITEM.ANYO,  "
sConsulta = sConsulta & vbCrLf & "           ITEM.GMN1,  "
sConsulta = sConsulta & vbCrLf & "           ITEM.PROCE,  "
sConsulta = sConsulta & vbCrLf & "           ITEM.ID,  "
sConsulta = sConsulta & vbCrLf & "           ITEM.GMN2,  "
sConsulta = sConsulta & vbCrLf & "           ITEM.GMN3,  "
sConsulta = sConsulta & vbCrLf & "           ITEM.GMN4,  "
sConsulta = sConsulta & vbCrLf & "           ITEM.ART,  "
sConsulta = sConsulta & vbCrLf & "           ITEM.DESCR + ART4.DEN AS DESCR,  "
sConsulta = sConsulta & vbCrLf & "           ITEM.DEST,  "
sConsulta = sConsulta & vbCrLf & "           ITEM.UNI,  "
sConsulta = sConsulta & vbCrLf & "           ITEM.CANT,  "
sConsulta = sConsulta & vbCrLf & "           ITEM.PREC, "
sConsulta = sConsulta & vbCrLf & "           ITEM.PRES,  "
sConsulta = sConsulta & vbCrLf & "           ITEM.PAG, "
sConsulta = sConsulta & vbCrLf & "           ITEM.FECINI,  "
sConsulta = sConsulta & vbCrLf & "           ITEM.FECFIN,  "
sConsulta = sConsulta & vbCrLf & "           ITEM.ESP,  "
sConsulta = sConsulta & vbCrLf & "           ITEM.CONF,  "
sConsulta = sConsulta & vbCrLf & "           round(ITEM.OBJ*(@EQUIV/@EQUIVPROC),5) AS OBJ , "
sConsulta = sConsulta & vbCrLf & "           ITEM.FECHAOBJ, "
sConsulta = sConsulta & vbCrLf & "           CASE  WHEN NUMESP > 0 THEN 1 WHEN NOT ITEM.ESP  IS NULL THEN 1 ELSE 0 END ESPECADJ ,  "
sConsulta = sConsulta & vbCrLf & "           ITEM.ANYO,  "
sConsulta = sConsulta & vbCrLf & "           ITEM.GMN1,  "
sConsulta = sConsulta & vbCrLf & "           ITEM.PROCE ,  "
sConsulta = sConsulta & vbCrLf & "           round(OFEMIN.PRECIO*@EQUIV,5) AS MINPRECIO , "
sConsulta = sConsulta & vbCrLf & "           CASE WHEN @PROVEEDOR=1 THEN OFEMIN.PROVE + ' - ' + PROVE.DEN ELSE NULL END MINPROVE "
sConsulta = sConsulta & vbCrLf & "      FROM ((ITEM LEFT JOIN ART4  "
sConsulta = sConsulta & vbCrLf & "                    ON ITEM.ART = ART4.COD  "
sConsulta = sConsulta & vbCrLf & "                   AND ITEM.GMN4 = ART4.GMN4  "
sConsulta = sConsulta & vbCrLf & "                   AND ITEM.GMN3 = ART4.GMN3  "
sConsulta = sConsulta & vbCrLf & "                   AND ITEM.GMN2 = ART4.GMN2 "
sConsulta = sConsulta & vbCrLf & "                   AND ITEM.GMN1 = ART4.GMN1) LEFT JOIN  "
sConsulta = sConsulta & vbCrLf & "                                                (SELECT ITEM_ESP.ANYO,  "
sConsulta = sConsulta & vbCrLf & "                                                        ITEM_ESP.GMN1,  "
sConsulta = sConsulta & vbCrLf & "                                                        ITEM_ESP.PROCE,  "
sConsulta = sConsulta & vbCrLf & "                                                        ITEM_ESP.ITEM,  "
sConsulta = sConsulta & vbCrLf & "                                                        Count(ITEM_ESP.ID) NUMESP  "
sConsulta = sConsulta & vbCrLf & "                                                   From ITEM_ESP  "
sConsulta = sConsulta & vbCrLf & "                                                  WHERE ITEM_ESP.ANYO=@ANYO  "
sConsulta = sConsulta & vbCrLf & "                                                    AND ITEM_ESP.GMN1=@GMN1  "
sConsulta = sConsulta & vbCrLf & "                                                    AND ITEM_ESP.PROCE=@COD  "
sConsulta = sConsulta & vbCrLf & "                                                GROUP BY ITEM_ESP.ANYO,  "
sConsulta = sConsulta & vbCrLf & "                                                         ITEM_ESP.GMN1,  "
sConsulta = sConsulta & vbCrLf & "                                                         ITEM_ESP.PROCE,  "
sConsulta = sConsulta & vbCrLf & "                                                         ITEM_ESP.ITEM  "
sConsulta = sConsulta & vbCrLf & "                                                 ) ITEMESP  "
sConsulta = sConsulta & vbCrLf & "                                                ON ITEM.ID = ITEMESP.ITEM  "
sConsulta = sConsulta & vbCrLf & "                                               AND ITEM.GMN1 = ITEMESP.GMN1 "
sConsulta = sConsulta & vbCrLf & "                                               AND ITEM.PROCE = ITEMESP.PROCE "
sConsulta = sConsulta & vbCrLf & "                                               AND ITEM.ANYO = ITEMESP.ANYO  "
sConsulta = sConsulta & vbCrLf & "            ) "
sConsulta = sConsulta & vbCrLf & "           LEFT JOIN OFEMIN  "
sConsulta = sConsulta & vbCrLf & "             ON OFEMIN.ANYO=ITEM.ANYO  "
sConsulta = sConsulta & vbCrLf & "            AND OFEMIN.GMN1=ITEM.GMN1  "
sConsulta = sConsulta & vbCrLf & "            AND OFEMIN.PROCE=ITEM.PROCE  "
sConsulta = sConsulta & vbCrLf & "            AND OFEMIN.ITEM=ITEM.ID "
sConsulta = sConsulta & vbCrLf & "           LEFT JOIN PROVE  "
sConsulta = sConsulta & vbCrLf & "             ON OFEMIN.PROVE=PROVE.COD "
sConsulta = sConsulta & vbCrLf & "      WHERE ITEM.ANYO=@ANYO AND ITEM.GMN1=@GMN1 AND ITEM.PROCE=@COD  "
sConsulta = sConsulta & vbCrLf & " "
sConsulta = sConsulta & vbCrLf & "SET CONCAT_NULL_YIELDS_NULL ON  "
sConsulta = sConsulta & vbCrLf & "Return  "
sConsulta = sConsulta & vbCrLf & "End"

Modificar_Trigger_SP_ITEMS_2_8_00_1 = sConsulta

End Function


Private Function ModificarITEM_OFE_TG_UPD_2_8_00_2() As String
Dim sConsulta As String
sConsulta = "CREATE TRIGGER ITEM_OFE_TG_UPD ON [ITEM_OFE] "
sConsulta = sConsulta & vbCrLf & "FOR UPDATE "
sConsulta = sConsulta & vbCrLf & "AS "
sConsulta = sConsulta & vbCrLf & "DECLARE @ANYO INT"
sConsulta = sConsulta & vbCrLf & "DECLARE @GMN1 VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "DECLARE @PROCE INT"
sConsulta = sConsulta & vbCrLf & "DECLARE @ITEM INT"
sConsulta = sConsulta & vbCrLf & "DECLARE @PRECIO AS FLOAT "
sConsulta = sConsulta & vbCrLf & ""
sConsulta = sConsulta & vbCrLf & "DECLARE @PROVEINS varchar(50)"
sConsulta = sConsulta & vbCrLf & "DECLARE @OFEINS smallint"
sConsulta = sConsulta & vbCrLf & "DECLARE @ULTINS smallint"
sConsulta = sConsulta & vbCrLf & ""
sConsulta = sConsulta & vbCrLf & ""
sConsulta = sConsulta & vbCrLf & "DECLARE @PROVENEW AS VARCHAR(50) "
sConsulta = sConsulta & vbCrLf & "DECLARE @OFENEW AS SMALLINT "
sConsulta = sConsulta & vbCrLf & ""
sConsulta = sConsulta & vbCrLf & "DECLARE curTG_ITEM_OFE_Upd CURSOR FOR SELECT ANYO,GMN1,PROCE,ITEM,PROVE,NUM, ULT FROM INSERTED "
sConsulta = sConsulta & vbCrLf & ""
sConsulta = sConsulta & vbCrLf & "OPEN curTG_ITEM_OFE_Upd "
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM curTG_ITEM_OFE_Upd INTO @ANYO,@GMN1,@PROCE,@ITEM,@PROVEINS,@OFEINS,@ULTINS"
sConsulta = sConsulta & vbCrLf & "WHILE @@FETCH_STATUS=0 "
sConsulta = sConsulta & vbCrLf & "BEGIN "
sConsulta = sConsulta & vbCrLf & " IF (SELECT SUBASTA FROM PROCE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE) = 1 "
sConsulta = sConsulta & vbCrLf & " BEGIN "
sConsulta = sConsulta & vbCrLf & "     IF UPDATE(PRECIO) "
sConsulta = sConsulta & vbCrLf & "     BEGIN "
sConsulta = sConsulta & vbCrLf & "        --Obtenemos el precio mas bajo ofertado para ese ITEM"
sConsulta = sConsulta & vbCrLf & "             "
sConsulta = sConsulta & vbCrLf & "        SELECT TOP 1 @PRECIO= A.PRECIO / (B.CAMBIO * C.CAMBIO), @PROVENEW=A.PROVE,@OFENEW=A.NUM "
sConsulta = sConsulta & vbCrLf & "          FROM ITEM_OFE A "
sConsulta = sConsulta & vbCrLf & "                   INNER JOIN PROCE_OFE B "
sConsulta = sConsulta & vbCrLf & "                           ON A.ANYO=B.ANYO "
sConsulta = sConsulta & vbCrLf & "                          AND A.GMN1=B.GMN1 "
sConsulta = sConsulta & vbCrLf & "                          AND A.PROCE=B.PROCE "
sConsulta = sConsulta & vbCrLf & "                          AND A.PROVE=B.PROVE "
sConsulta = sConsulta & vbCrLf & "                          AND A.NUM=B.OFE"
sConsulta = sConsulta & vbCrLf & "                   INNER JOIN PROCE C "
sConsulta = sConsulta & vbCrLf & "                           ON A.ANYO=C.ANYO "
sConsulta = sConsulta & vbCrLf & "                          AND A.GMN1=C.GMN1 "
sConsulta = sConsulta & vbCrLf & "                          AND A.PROCE = C.COD"
sConsulta = sConsulta & vbCrLf & "         WHERE A.ANYO=@ANYO "
sConsulta = sConsulta & vbCrLf & "           AND A.GMN1=@GMN1  "
sConsulta = sConsulta & vbCrLf & "           AND A.PROCE=@PROCE "
sConsulta = sConsulta & vbCrLf & "           AND A.ITEM=@ITEM "
sConsulta = sConsulta & vbCrLf & "           AND A.ULT=1 "
sConsulta = sConsulta & vbCrLf & "           AND A.PRECIO IS NOT NULL"
sConsulta = sConsulta & vbCrLf & "      ORDER BY A.PRECIO / (B.CAMBIO * C.CAMBIO) ASC "
sConsulta = sConsulta & vbCrLf & ""
sConsulta = sConsulta & vbCrLf & "      DECLARE @ULTOFE smallint"
sConsulta = sConsulta & vbCrLf & "      IF @ULTINS=1 AND @PROVEINS=@PROVENEW AND @OFEINS=@OFENEW"
sConsulta = sConsulta & vbCrLf & "          set @ULTOFE=1"
sConsulta = sConsulta & vbCrLf & "      ELSE"
sConsulta = sConsulta & vbCrLf & "          set @ULTOFE=0"
sConsulta = sConsulta & vbCrLf & "         "
sConsulta = sConsulta & vbCrLf & "      --Actualizamos OFEMIN con los datos de la oferta m�s baja                       "
sConsulta = sConsulta & vbCrLf & "      UPDATE OFEMIN "
sConsulta = sConsulta & vbCrLf & "         SET PRECIO=@PRECIO,"
sConsulta = sConsulta & vbCrLf & "             PROVE=@PROVENEW,"
sConsulta = sConsulta & vbCrLf & "             OFE=@OFENEW,"
sConsulta = sConsulta & vbCrLf & "             ULTOFE = @ULTOFE"
sConsulta = sConsulta & vbCrLf & "       WHERE ANYO=@ANYO "
sConsulta = sConsulta & vbCrLf & "         AND GMN1=@GMN1 "
sConsulta = sConsulta & vbCrLf & "         AND PROCE=@PROCE "
sConsulta = sConsulta & vbCrLf & "         AND ITEM=@ITEM "
sConsulta = sConsulta & vbCrLf & "    END"
sConsulta = sConsulta & vbCrLf & "  END  "
sConsulta = sConsulta & vbCrLf & " FETCH NEXT FROM curTG_ITEM_OFE_Upd INTO @ANYO,@GMN1,@PROCE,@ITEM,@PROVEINS,@OFEINS,@ULTINS"
sConsulta = sConsulta & vbCrLf & "End "
sConsulta = sConsulta & vbCrLf & "Close curTG_ITEM_OFE_Upd "
sConsulta = sConsulta & vbCrLf & "DEALLOCATE curTG_ITEM_OFE_Upd "

ModificarITEM_OFE_TG_UPD_2_8_00_2 = sConsulta



End Function

Private Function CrearOFEMIN_TG_UPD_2_8_00_2() As String
Dim sConsulta As String

sConsulta = "CREATE TRIGGER OFEMIN_TG_UPD ON [OFEMIN] "
sConsulta = sConsulta & vbCrLf & "FOR UPDATE "
sConsulta = sConsulta & vbCrLf & "AS"
sConsulta = sConsulta & vbCrLf & ""
sConsulta = sConsulta & vbCrLf & "DECLARE @PROVEOLD varchar(100)"
sConsulta = sConsulta & vbCrLf & "DECLARE @ANYOOLD int"
sConsulta = sConsulta & vbCrLf & "DECLARE @GMN1OLD varchar(50)"
sConsulta = sConsulta & vbCrLf & "DECLARE @PROCEOLD int"
sConsulta = sConsulta & vbCrLf & "DECLARE @ITEMOLD int"
sConsulta = sConsulta & vbCrLf & "DECLARE @PRECIOOLD float"
sConsulta = sConsulta & vbCrLf & "DECLARE @OFEOLD smallint"
sConsulta = sConsulta & vbCrLf & ""
sConsulta = sConsulta & vbCrLf & "DECLARE @PROVENEW varchar(100)"
sConsulta = sConsulta & vbCrLf & "DECLARE @ANYONEW int"
sConsulta = sConsulta & vbCrLf & "DECLARE @GMN1NEW varchar(50)"
sConsulta = sConsulta & vbCrLf & "DECLARE @PROCENEW int"
sConsulta = sConsulta & vbCrLf & "DECLARE @ITEMNEW int"
sConsulta = sConsulta & vbCrLf & "DECLARE @PRECIONEW float"
sConsulta = sConsulta & vbCrLf & "DECLARE @OFENEW smallint"
sConsulta = sConsulta & vbCrLf & ""
sConsulta = sConsulta & vbCrLf & "DECLARE @IDENT int"
sConsulta = sConsulta & vbCrLf & "DECLARE @ULTOFE smallint"
sConsulta = sConsulta & vbCrLf & ""
sConsulta = sConsulta & vbCrLf & "DECLARE curTG_OFEMIN_Upd CURSOR FOR SELECT ANYO,GMN1,PROCE,ITEM, PROVE, PRECIO,OFE, ULTOFE FROM INSERTED "
sConsulta = sConsulta & vbCrLf & ""
sConsulta = sConsulta & vbCrLf & "OPEN curTG_OFEMIN_Upd "
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM curTG_OFEMIN_Upd INTO @ANYONEW,@GMN1NEW,@PROCENEW,@ITEMNEW,@PROVENEW,@PRECIONEW,@OFENEW,@ULTOFE"
sConsulta = sConsulta & vbCrLf & "WHILE @@FETCH_STATUS=0 "
sConsulta = sConsulta & vbCrLf & "   BEGIN"
sConsulta = sConsulta & vbCrLf & "   SELECT @ANYOOLD=ANYO, @GMN1OLD=GMN1, @PROCEOLD=PROCE, @PROVEOLD=PROVE,@ITEMOLD=ITEM, @PRECIOOLD=PRECIO, @OFEOLD=OFE"
sConsulta = sConsulta & vbCrLf & "     FROM DELETED"
sConsulta = sConsulta & vbCrLf & "    WHERE ANYO=@ANYONEW"
sConsulta = sConsulta & vbCrLf & "      AND GMN1=@GMN1NEW"
sConsulta = sConsulta & vbCrLf & "      AND PROCE=@PROCENEW"
sConsulta = sConsulta & vbCrLf & "      AND ITEM=@ITEMNEW"
sConsulta = sConsulta & vbCrLf & "    IF @PRECIONEW IS NOT NULL"
sConsulta = sConsulta & vbCrLf & "     BEGIN"
sConsulta = sConsulta & vbCrLf & "     SELECT @IDENT=MAX(ID) FROM DETALLE_PUJAS WHERE ANYO=@ANYONEW AND GMN1=@GMN1NEW AND PROCE=@PROCENEW AND ITEM=@ITEMNEW "
sConsulta = sConsulta & vbCrLf & "     IF @PROVEOLD IS NULL"
sConsulta = sConsulta & vbCrLf & "        BEGIN"
sConsulta = sConsulta & vbCrLf & "           SET @IDENT=ISNULL(@IDENT,0)"
sConsulta = sConsulta & vbCrLf & "           SET @IDENT= @IDENT+1 "
sConsulta = sConsulta & vbCrLf & "           INSERT INTO DETALLE_PUJAS (ANYO,GMN1,PROCE,ITEM,ID,OFE,PROVE,FECHA,PRECIO) "
sConsulta = sConsulta & vbCrLf & "               VALUES (@ANYONEW,@GMN1NEW,@PROCENEW,@ITEMNEW,@IDENT,@OFENEW,@PROVENEW,GETDATE(),@PRECIONEW) "
sConsulta = sConsulta & vbCrLf & "        END"
sConsulta = sConsulta & vbCrLf & "     ELSE"
sConsulta = sConsulta & vbCrLf & "       BEGIN"
sConsulta = sConsulta & vbCrLf & "         IF @PROVEOLD!=@PROVENEW"
sConsulta = sConsulta & vbCrLf & "           BEGIN"
sConsulta = sConsulta & vbCrLf & "              SET @IDENT=ISNULL(@IDENT,0)"
sConsulta = sConsulta & vbCrLf & "              SET @IDENT= @IDENT+1 "
sConsulta = sConsulta & vbCrLf & "              INSERT INTO DETALLE_PUJAS (ANYO,GMN1,PROCE,ITEM,ID,OFE,PROVE,FECHA,PRECIO) "
sConsulta = sConsulta & vbCrLf & "                              VALUES (@ANYONEW,@GMN1NEW,@PROCENEW,@ITEMNEW,@IDENT,@OFENEW,@PROVENEW,GETDATE(),@PRECIONEW) "
sConsulta = sConsulta & vbCrLf & "              ----LAS NOTIFICACIONES"
sConsulta = sConsulta & vbCrLf & "              IF @ULTOFE = 1"
sConsulta = sConsulta & vbCrLf & "                BEGIN"
sConsulta = sConsulta & vbCrLf & "                  DECLARE @CONEX  AS VARCHAR(100),@FSP_CIA AS VARCHAR(50) "
sConsulta = sConsulta & vbCrLf & "                  DECLARE @BD AS VARCHAR(100),@SERVER AS VARCHAR(100) "
sConsulta = sConsulta & vbCrLf & "                  DECLARE @EMAIL AS VARCHAR(100) ,@PROVEDEN AS VARCHAR(500) , @CONTACTO AS VARCHAR(500) "
sConsulta = sConsulta & vbCrLf & "                  DECLARE @PROCEDEN VARCHAR(500),@ITEMDEN VARCHAR(500) "
sConsulta = sConsulta & vbCrLf & "                  DECLARE @INST smallint"
sConsulta = sConsulta & vbCrLf & "                  SET CONCAT_NULL_YIELDS_NULL OFF "
sConsulta = sConsulta & vbCrLf & "                  SELECT TOP 1 @EMAIL=CON.EMAIL,@PROVEDEN=PROVE.DEN,@CONTACTO = NOMCON + ' ' + APECON "
sConsulta = sConsulta & vbCrLf & "                    FROM PROVE,PROCE_PROVE_PET,CON "
sConsulta = sConsulta & vbCrLf & "                   WHERE PROVE.COD= PROCE_PROVE_PET.PROVE "
sConsulta = sConsulta & vbCrLf & "                     AND PROCE_PROVE_PET.PROVE= CON.PROVE "
sConsulta = sConsulta & vbCrLf & "                     AND PROCE_PROVE_PET.NOMCON=CON.NOM "
sConsulta = sConsulta & vbCrLf & "                     AND PROCE_PROVE_PET.APECON = CON.APE "
sConsulta = sConsulta & vbCrLf & "                     AND ANYO=@ANYONEW"
sConsulta = sConsulta & vbCrLf & "                     AND GMN1=@GMN1NEW"
sConsulta = sConsulta & vbCrLf & "                     AND PROCE=@PROCENEW "
sConsulta = sConsulta & vbCrLf & "                     AND PROVE.COD=@PROVEOLD"
sConsulta = sConsulta & vbCrLf & "                ORDER BY PROCE_PROVE_PET.ID DESC "
sConsulta = sConsulta & vbCrLf & "                  SELECT @PROCEDEN=PROCE.DEN,@ITEMDEN=ITEM.DESCR + ART4.DEN "
sConsulta = sConsulta & vbCrLf & "                    FROM PROCE "
sConsulta = sConsulta & vbCrLf & "                       INNER JOIN ITEM "
sConsulta = sConsulta & vbCrLf & "                               ON PROCE.ANYO=ITEM.ANYO "
sConsulta = sConsulta & vbCrLf & "                              AND Proce.gmn1 = Item.gmn1 "
sConsulta = sConsulta & vbCrLf & "                              And Proce.COD = Item.Proce "
sConsulta = sConsulta & vbCrLf & "                        LEFT JOIN ART4 "
sConsulta = sConsulta & vbCrLf & "                               ON ITEM.ART=ART4.COD "
sConsulta = sConsulta & vbCrLf & "                              AND ITEM.GMN1=ART4.GMN1 "
sConsulta = sConsulta & vbCrLf & "                              AND ITEM.GMN2=ART4.GMN2  "
sConsulta = sConsulta & vbCrLf & "                              AND ITEM.GMN3=ART4.GMN3 "
sConsulta = sConsulta & vbCrLf & "                              AND ITEM.GMN4=ART4.GMN4  "
sConsulta = sConsulta & vbCrLf & "                   WHERE PROCE.ANYO=@ANYONEW"
sConsulta = sConsulta & vbCrLf & "                     AND PROCE.GMN1=@GMN1NEW"
sConsulta = sConsulta & vbCrLf & "                     AND PROCE.COD=@PROCENEW"
sConsulta = sConsulta & vbCrLf & "                     AND ITEM.ID=@ITEMNEW "
sConsulta = sConsulta & vbCrLf & "                  SET CONCAT_NULL_YIELDS_NULL ON"
sConsulta = sConsulta & vbCrLf & "                  --Si la instalaci�n es de BuySite"
sConsulta = sConsulta & vbCrLf & "                  SELECT @INST=INSTWEB "
sConsulta = sConsulta & vbCrLf & "                    FROM PARGEN_INTERNO "
sConsulta = sConsulta & vbCrLf & "                  IF @INST = 1 "
sConsulta = sConsulta & vbCrLf & "                    BEGIN "
sConsulta = sConsulta & vbCrLf & "                      IF @EMAIL IS NULL OR @CONTACTO IS NULL  /*Si el e-mail o el contacto son nulos saca de contactos alguno que no sea nulo*/ "
sConsulta = sConsulta & vbCrLf & "                        BEGIN "
sConsulta = sConsulta & vbCrLf & "                          SELECT TOP 1 @EMAIL=CON.EMAIL,@CONTACTO = CON.NOM + ' ' + CON.APE ,@PROVEDEN = PROVE.DEN "
sConsulta = sConsulta & vbCrLf & "                            FROM PROVE,CON  "
sConsulta = sConsulta & vbCrLf & "                           WHERE PROVE.COD= CON.PROVE "
sConsulta = sConsulta & vbCrLf & "                             AND PROVE.COD=@PROVEOLD"
sConsulta = sConsulta & vbCrLf & "                             AND CON.EMAIL IS NOT NULL "
sConsulta = sConsulta & vbCrLf & "                             AND CON.NOM IS NOT NULL "
sConsulta = sConsulta & vbCrLf & "                        END "
sConsulta = sConsulta & vbCrLf & "                     INSERT INTO PROVENOTIF (ANYO,GMN1,PROCE,PROCEDEN,ITEM,ITEMDEN,PROVE,PROVEDEN,EMAIL,CONTACTO) "
sConsulta = sConsulta & vbCrLf & "                     VALUES (@ANYONEW,@GMN1NEW,@PROCENEW,@PROCEDEN,@ITEMNEW,@ITEMDEN,@PROVEOLD,@PROVEDEN,@EMAIL,@CONTACTO) "
sConsulta = sConsulta & vbCrLf & "                  END "
sConsulta = sConsulta & vbCrLf & "                ELSE /*Si la instalaci�n es de Portal*/ "
sConsulta = sConsulta & vbCrLf & "                  BEGIN "
sConsulta = sConsulta & vbCrLf & "                    IF @EMAIL IS NULL OR @CONTACTO IS NULL  /*Si el e-mail o el contacto son nulos saca de contactos alguno que no sea nulo*/ "
sConsulta = sConsulta & vbCrLf & "                       BEGIN "
sConsulta = sConsulta & vbCrLf & "                          SELECT TOP 1 @EMAIL=CON.EMAIL,@CONTACTO = CON.NOM + ' ' + CON.APE,@PROVEDEN=PROVE.DEN "
sConsulta = sConsulta & vbCrLf & "                            FROM PROVE,CON  "
sConsulta = sConsulta & vbCrLf & "                           WHERE PROVE.COD= CON.PROVE "
sConsulta = sConsulta & vbCrLf & "                             AND PROVE.COD=@PROVEOLD  "
sConsulta = sConsulta & vbCrLf & "                             AND CON.EMAIL IS NOT NULL "
sConsulta = sConsulta & vbCrLf & "                             AND CON.NOM IS NOT NULL "
sConsulta = sConsulta & vbCrLf & "                             AND CON.PORT=1"
sConsulta = sConsulta & vbCrLf & "                          IF  @EMAIL IS NULL OR @CONTACTO  IS NULL  /*Si el e-mail o el contacto son nulos saca de contactos alguno que no sea nulo*/ "
sConsulta = sConsulta & vbCrLf & "                             BEGIN "
sConsulta = sConsulta & vbCrLf & "                                SELECT TOP 1 @EMAIL=CON.EMAIL,@CONTACTO = CON.NOM + ' ' + CON.APE,@PROVEDEN=PROVE.DEN "
sConsulta = sConsulta & vbCrLf & "                                  FROM PROVE,CON  "
sConsulta = sConsulta & vbCrLf & "                                 WHERE PROVE.COD= CON.PROVE "
sConsulta = sConsulta & vbCrLf & "                                   AND PROVE.COD=@PROVEOLD"
sConsulta = sConsulta & vbCrLf & "                                   AND CON.EMAIL IS NOT NULL "
sConsulta = sConsulta & vbCrLf & "                                   AND CON.NOM IS NOT NULL"
sConsulta = sConsulta & vbCrLf & "                             END"
sConsulta = sConsulta & vbCrLf & "                       END"
sConsulta = sConsulta & vbCrLf & "                     --Obtenemos el servidor,BD y el identificador de la Cia del portal"
sConsulta = sConsulta & vbCrLf & "                     SELECT @SERVER = FSP_SRV, @BD = FSP_BD , @FSP_CIA = FSP_CIA FROM PARGEN_PORT "
sConsulta = sConsulta & vbCrLf & "                     --Ejecutamos el store procedure de notificaci�n que se encuentra en el portal."
sConsulta = sConsulta & vbCrLf & "                     SET @CONEX=@SERVER + '.' + @BD + '.dbo.SP_ACTUALIZAR_PEND_NOTIF' "
sConsulta = sConsulta & vbCrLf & "                     EXECUTE @CONEX @FSP_CIA,@ANYONEW,@GMN1NEW,@PROCENEW,@ITEMNEW,@PROVEOLD,@PROCEDEN,@ITEMDEN,@EMAIL,@PROVEDEN,@CONTACTO "
sConsulta = sConsulta & vbCrLf & "                  END "
sConsulta = sConsulta & vbCrLf & "                END"
sConsulta = sConsulta & vbCrLf & "              ----FIN DE NOTIFICACIONES"
sConsulta = sConsulta & vbCrLf & "           END"
sConsulta = sConsulta & vbCrLf & "           ELSE"
sConsulta = sConsulta & vbCrLf & "             BEGIN"
sConsulta = sConsulta & vbCrLf & "                UPDATE DETALLE_PUJAS "
sConsulta = sConsulta & vbCrLf & "                   SET OFE=@OFENEW,"
sConsulta = sConsulta & vbCrLf & "                       FECHA=GETDATE(),"
sConsulta = sConsulta & vbCrLf & "                       PRECIO=@PRECIONEW"
sConsulta = sConsulta & vbCrLf & "                 WHERE ANYO=@ANYONEW"
sConsulta = sConsulta & vbCrLf & "                   AND GMN1=@GMN1NEW"
sConsulta = sConsulta & vbCrLf & "                   AND PROCE=@PROCENEW"
sConsulta = sConsulta & vbCrLf & "                   AND PROVE=@PROVENEW"
sConsulta = sConsulta & vbCrLf & "                   AND ITEM=@ITEMNEW"
sConsulta = sConsulta & vbCrLf & "                   AND ID=@IDENT"
sConsulta = sConsulta & vbCrLf & "              END"
sConsulta = sConsulta & vbCrLf & "         END"
sConsulta = sConsulta & vbCrLf & "       END"
sConsulta = sConsulta & vbCrLf & "       ELSE"
sConsulta = sConsulta & vbCrLf & "           DELETE FROM DETALLE_PUJAS"
sConsulta = sConsulta & vbCrLf & "                 WHERE ANYO=@ANYONEW"
sConsulta = sConsulta & vbCrLf & "                   AND GMN1=@GMN1NEW"
sConsulta = sConsulta & vbCrLf & "                   AND PROCE=@PROCENEW"
sConsulta = sConsulta & vbCrLf & "                   AND PROVE=@PROVEOLD"
sConsulta = sConsulta & vbCrLf & "                   AND ITEM=@ITEMNEW"
sConsulta = sConsulta & vbCrLf & "                   AND OFE = @OFEOLD"
sConsulta = sConsulta & vbCrLf & "       FETCH NEXT FROM curTG_OFEMIN_Upd INTO @ANYONEW,@GMN1NEW,@PROCENEW,@ITEMNEW,@PROVENEW,@PRECIONEW,@OFENEW,@ULTOFE"
sConsulta = sConsulta & vbCrLf & "    END"
sConsulta = sConsulta & vbCrLf & "Close curTG_OFEMIN_Upd "
sConsulta = sConsulta & vbCrLf & "DEALLOCATE curTG_OFEMIN_Upd "
sConsulta = sConsulta & vbCrLf & ""
sConsulta = sConsulta & vbCrLf & "if (select count(*) from ofemin where ANYO=@ANYONEW and GMN1=@GMN1NEW and PROCE=@PROCENEW and PROVE!=@PROVENEW AND PROVE IS NOT NULL)=0"
sConsulta = sConsulta & vbCrLf & "    UPDATE PROCE_PROVE "

sConsulta = sConsulta & vbCrLf & "       SET SUPERADA = case when a.prove=@PROVENEW then 0 else 1 end"
sConsulta = sConsulta & vbCrLf & "      FROM PROCE_PROVE A"
sConsulta = sConsulta & vbCrLf & "     WHERE A.ANYO= @ANYONEW "
sConsulta = sConsulta & vbCrLf & "       AND A.GMN1=@GMN1NEW "
sConsulta = sConsulta & vbCrLf & "       AND A.PROCE=@PROCENEW "
sConsulta = sConsulta & vbCrLf & "else"
sConsulta = sConsulta & vbCrLf & "    UPDATE PROCE_PROVE "
sConsulta = sConsulta & vbCrLf & "       SET SUPERADA = 1"
sConsulta = sConsulta & vbCrLf & "      FROM PROCE_PROVE A"
sConsulta = sConsulta & vbCrLf & "     WHERE A.ANYO= @ANYONEW"
sConsulta = sConsulta & vbCrLf & "       AND A.GMN1=@GMN1NEW "
sConsulta = sConsulta & vbCrLf & "       AND A.PROCE=@PROCENEW"


CrearOFEMIN_TG_UPD_2_8_00_2 = sConsulta



End Function

Private Function ModificarPROCE_OFE_TG_DEL_2_8_00_2() As String
Dim sConsulta As String

sConsulta = "CREATE TRIGGER PROCE_OFE_TG_DEL ON dbo.PROCE_OFE "
sConsulta = sConsulta & vbCrLf & "FOR DELETE "
sConsulta = sConsulta & vbCrLf & "AS "
sConsulta = sConsulta & vbCrLf & "DECLARE @ANYO AS INTEGER "
sConsulta = sConsulta & vbCrLf & "DECLARE @GMN1 AS VARCHAR(50) "
sConsulta = sConsulta & vbCrLf & "DECLARE @PROCE AS INTEGER "
sConsulta = sConsulta & vbCrLf & "DECLARE @PROVE AS VARCHAR(50) "
sConsulta = sConsulta & vbCrLf & "DECLARE @MAXOFE AS INT "
sConsulta = sConsulta & vbCrLf & "DECLARE @ITEM AS INTEGER "
sConsulta = sConsulta & vbCrLf & "DECLARE @PRECIO AS FLOAT "
sConsulta = sConsulta & vbCrLf & "DECLARE @PROVE2 AS VARCHAR(50) "
sConsulta = sConsulta & vbCrLf & "DECLARE @OFE2 AS SMALLINT "
sConsulta = sConsulta & vbCrLf & "DECLARE @IDENT AS INTEGER "
sConsulta = sConsulta & vbCrLf & "DECLARE @CAMBIOMONCEN FLOAT "
sConsulta = sConsulta & vbCrLf & "DECLARE curTG_PROCE_OFE_Del CURSOR FOR SELECT ANYO,GMN1,PROCE,PROVE FROM DELETED "
sConsulta = sConsulta & vbCrLf & "OPEN curTG_PROCE_OFE_Del "
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM curTG_PROCE_OFE_Del INTO @ANYO,@GMN1,@PROCE,@PROVE "
sConsulta = sConsulta & vbCrLf & "WHILE @@FETCH_STATUS=0 "
sConsulta = sConsulta & vbCrLf & "BEGIN "
sConsulta = sConsulta & vbCrLf & " IF (SELECT SUBASTA FROM PROCE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE) = 1 "
sConsulta = sConsulta & vbCrLf & " BEGIN "
sConsulta = sConsulta & vbCrLf & "     --Si existen precios en la oferta eliminada que eran m�nimas en la puja se deber�n recalcular los nuevos precios m�nimos "
sConsulta = sConsulta & vbCrLf & "     IF (SELECT COUNT(*) FROM OFEMIN WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE = @PROCE AND PROVE=@PROVE)>0 "
sConsulta = sConsulta & vbCrLf & "     BEGIN "
sConsulta = sConsulta & vbCrLf & "         /*Obtiene todos los items para los cuales ese proveedor tiene la oferta m�nima*/ "
sConsulta = sConsulta & vbCrLf & "         DECLARE C CURSOR FOR SELECT ITEM FROM OFEMIN WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE "
sConsulta = sConsulta & vbCrLf & "         OPEN C "
sConsulta = sConsulta & vbCrLf & "         FETCH NEXT FROM C INTO @ITEM "
sConsulta = sConsulta & vbCrLf & "         WHILE @@FETCH_STATUS=0 "
sConsulta = sConsulta & vbCrLf & "         BEGIN "
sConsulta = sConsulta & vbCrLf & "             SELECT TOP 1 @PRECIO= A.PRECIO / (B.CAMBIO * C.CAMBIO), @PROVE2=A.PROVE,@OFE2=A.NUM"
sConsulta = sConsulta & vbCrLf & "                   FROM ITEM_OFE A INNER JOIN PROCE_OFE B ON A.ANYO=B.ANYO AND A.GMN1=B.GMN1 AND A.PROCE=B.PROCE AND A.PROVE=B.PROVE AND A.NUM=B.OFE"
sConsulta = sConsulta & vbCrLf & "                  INNER JOIN PROCE C ON A.ANYO=C.ANYO AND A.GMN1=C.GMN1 AND A.PROCE = C.COD"
sConsulta = sConsulta & vbCrLf & "             WHERE A.ANYO=@ANYO AND A.GMN1=@GMN1 AND A.PROCE=@PROCE AND A.ITEM=@ITEM AND A.ULT=1 AND A.PRECIO IS NOT NULL"
sConsulta = sConsulta & vbCrLf & "             ORDER BY A.PRECIO / (B.CAMBIO * C.CAMBIO) ASC"
sConsulta = sConsulta & vbCrLf & "             /*Borramos de DETALLE_PUJAS el �ltimo registro*/ "
sConsulta = sConsulta & vbCrLf & "             SELECT @IDENT =MAX(ID) FROM DETALLE_PUJAS WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM "
sConsulta = sConsulta & vbCrLf & "             DELETE FROM DETALLE_PUJAS WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM AND ID=@IDENT "
sConsulta = sConsulta & vbCrLf & "             /*Insertamos en DETALLE_PUJAS*/ "
sConsulta = sConsulta & vbCrLf & "             IF @PRECIO IS NULL "
sConsulta = sConsulta & vbCrLf & "             BEGIN "
sConsulta = sConsulta & vbCrLf & "                 UPDATE OFEMIN SET PRECIO=NULL, PROVE=NULL, OFE=NULL WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE = @PROCE AND ITEM = @ITEM "
sConsulta = sConsulta & vbCrLf & "                 DELETE FROM DETALLE_PUJAS WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM "
sConsulta = sConsulta & vbCrLf & "             End "
sConsulta = sConsulta & vbCrLf & "             Else "
sConsulta = sConsulta & vbCrLf & "             BEGIN "
sConsulta = sConsulta & vbCrLf & "                 /*Actualiza la tabla OFEMIN con la oferta m�nima para el proceso e item*/ "
sConsulta = sConsulta & vbCrLf & "                 UPDATE OFEMIN SET PRECIO=@PRECIO,PROVE=@PROVE2,OFE=@OFE2, ULTOFE=0 WHERE ANYO=@ANYO  AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM "
sConsulta = sConsulta & vbCrLf & "             End "
sConsulta = sConsulta & vbCrLf & "             FETCH NEXT FROM C INTO @ITEM "
sConsulta = sConsulta & vbCrLf & "         End "
sConsulta = sConsulta & vbCrLf & "         Close C "
sConsulta = sConsulta & vbCrLf & "         DEALLOCATE C "
sConsulta = sConsulta & vbCrLf & "     End "
sConsulta = sConsulta & vbCrLf & "     /* Si hab�a ofertas anteriores de ese proveedor, introducimos su precio como ofertas por defecto */ "
sConsulta = sConsulta & vbCrLf & "     SET @MAXOFE = ( SELECT MAX(OFE) FROM PROCE_OFE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE ) "
sConsulta = sConsulta & vbCrLf & "     IF (@MAXOFE IS NULL ) "
sConsulta = sConsulta & vbCrLf & "     BEGIN "
sConsulta = sConsulta & vbCrLf & "     /* Si se han insertado ofertas, actualizamos el estado del proceso  si es que este era inferior a 7 */ "
sConsulta = sConsulta & vbCrLf & "         IF (SELECT PROCE.EST FROM PROCE WHERE ANYO=@ANYO  AND GMN1=@GMN1 AND COD=@PROCE) = 7  /* 7= Con ofertas */ "
sConsulta = sConsulta & vbCrLf & "         BEGIN "
sConsulta = sConsulta & vbCrLf & "             UPDATE PROCE SET EST = 6  WHERE ANYO= @ANYO  AND GMN1=@GMN1 AND COD=@PROCE "
sConsulta = sConsulta & vbCrLf & "         End "
sConsulta = sConsulta & vbCrLf & "     End "
sConsulta = sConsulta & vbCrLf & " End "
sConsulta = sConsulta & vbCrLf & " FETCH NEXT FROM curTG_PROCE_OFE_Del INTO @ANYO,@GMN1,@PROCE,@PROVE "
sConsulta = sConsulta & vbCrLf & "End "
sConsulta = sConsulta & vbCrLf & "Close curTG_PROCE_OFE_Del "
sConsulta = sConsulta & vbCrLf & "DEALLOCATE curTG_PROCE_OFE_Del "

ModificarPROCE_OFE_TG_DEL_2_8_00_2 = sConsulta
End Function


Private Function Modificar_SP_AR_ITEM_MES_PROY1_2_8_00_3() As String
Dim sConsulta As String

sConsulta = "CREATE   PROCEDURE  SP_AR_ITEM_MES_PROY1 (@ANYO SMALLINT,@GMN1 VARCHAR(50), @PROCE INT,@RES INT OUTPUT)  AS"
sConsulta = sConsulta & vbLf & "BEGIN "
sConsulta = sConsulta & vbLf & "DECLARE @FECINI AS DATETIME"
sConsulta = sConsulta & vbLf & "DECLARE @FECFIN AS DATETIME"
sConsulta = sConsulta & vbLf & "DECLARE @PRECADJ AS FLOAT"
sConsulta = sConsulta & vbLf & "DECLARE @CANTADJ AS FLOAT"
sConsulta = sConsulta & vbLf & "DECLARE @PRECAPE AS FLOAT"
sConsulta = sConsulta & vbLf & "DECLARE @DENGMN1 AS VARCHAR(100)"
sConsulta = sConsulta & vbLf & "DECLARE @DENGMN2 AS VARCHAR(100)"
sConsulta = sConsulta & vbLf & "DECLARE @DENGMN3 AS VARCHAR(100)"
sConsulta = sConsulta & vbLf & "DECLARE @DENGMN4 AS VARCHAR(100)"
sConsulta = sConsulta & vbLf & "DECLARE @PROCECAMBIO AS FLOAT"
sConsulta = sConsulta & vbLf & "DECLARE @ADJDIR AS BIT"
sConsulta = sConsulta & vbLf & "DECLARE @GMN2 AS VARCHAR (50)"
sConsulta = sConsulta & vbLf & "DECLARE @GMN3 AS VARCHAR (50)"
sConsulta = sConsulta & vbLf & "DECLARE @GMN4 AS VARCHAR (50)"
sConsulta = sConsulta & vbLf & "DECLARE @PORCEN AS FLOAT"
sConsulta = sConsulta & vbLf & "DECLARE @MESINI AS INTEGER"
sConsulta = sConsulta & vbLf & "DECLARE @TOTALDIAS AS FLOAT"
sConsulta = sConsulta & vbLf & "DECLARE @DIAS AS FLOAT"
sConsulta = sConsulta & vbLf & "DECLARE @PAG AS VARCHAR(50)"
sConsulta = sConsulta & vbLf & "DECLARE @DENPAG AS VARCHAR(100)"
sConsulta = sConsulta & vbLf & "DECLARE @UNI AS VARCHAR(50)"
sConsulta = sConsulta & vbLf & "DECLARE @DENUNI AS VARCHAR(100)"
sConsulta = sConsulta & vbLf & "DECLARE @DEST AS VARCHAR(50)"
sConsulta = sConsulta & vbLf & "DECLARE @DENDEST AS VARCHAR(100)"
sConsulta = sConsulta & vbLf & "DECLARE @ART AS VARCHAR(50)"
sConsulta = sConsulta & vbLf & "DECLARE @DESCR AS VARCHAR(100)"
sConsulta = sConsulta & vbLf & "DECLARE @CANT AS FLOAT"
sConsulta = sConsulta & vbLf & "DECLARE @FECHA AS DATETIME"
sConsulta = sConsulta & vbLf & "DECLARE @ITEM AS INTEGER"
sConsulta = sConsulta & vbLf & "DECLARE @PROVE AS VARCHAR(50)"
sConsulta = sConsulta & vbLf & "DECLARE @NEPC AS TINYINT /* Numero de niveles en la estructura de materiales*/"
sConsulta = sConsulta & vbLf & "DECLARE @FEC AS DATETIME"
sConsulta = sConsulta & vbLf & "DECLARE @FECFINAL AS DATETIME"
sConsulta = sConsulta & vbLf & "DECLARE @ANYOINI AS INTEGER"
sConsulta = sConsulta & vbLf & "DECLARE @ANYOFIN AS INTEGER"
sConsulta = sConsulta & vbLf & "DECLARE @ANYOACTUAL AS INTEGER"
sConsulta = sConsulta & vbLf & "DECLARE @PROY1 AS VARCHAR (50)"
sConsulta = sConsulta & vbLf & "DECLARE @DENPROY1 AS VARCHAR(100)"
sConsulta = sConsulta & vbLf & "DECLARE @ANYOPROY AS INTEGER"
sConsulta = sConsulta & vbLf & "DECLARE @FECINICIAL AS DATETIME"
sConsulta = sConsulta & vbLf & ""
sConsulta = sConsulta & vbLf & "SET CONCAT_NULL_YIELDS_NULL OFF"
sConsulta = sConsulta & vbLf & ""
sConsulta = sConsulta & vbLf & "DECLARE Cur_PROCE CURSOR FORWARD_ONLY STATIC READ_ONLY FOR"
sConsulta = sConsulta & vbLf & ""
sConsulta = sConsulta & vbLf & "SELECT GMN1.DEN,PROCE.GMN2, PROCE.GMN3, PROCE.GMN4"
sConsulta = sConsulta & vbLf & ",GMN2.DEN, GMN3.DEN, GMN4.DEN, PROCE.CAMBIO,PROCE.ADJDIR"
sConsulta = sConsulta & vbLf & "FROM PROCE"
sConsulta = sConsulta & vbLf & "INNER JOIN GMN1 ON PROCE.GMN1=GMN1.COD"
sConsulta = sConsulta & vbLf & "INNER JOIN GMN2 ON  PROCE.GMN1=GMN2.GMN1 AND PROCE.GMN2=GMN2.COD"
sConsulta = sConsulta & vbLf & "INNER JOIN GMN3 ON  PROCE.GMN1=GMN3.GMN1 AND  PROCE.GMN2=GMN3.GMN2 AND PROCE.GMN3=GMN3.COD"
sConsulta = sConsulta & vbLf & "INNER JOIN GMN4 ON  PROCE.GMN1=GMN4.GMN1 AND  PROCE.GMN2=GMN4.GMN2 AND PROCE.GMN3=GMN4.GMN3 AND PROCE.GMN4=GMN4.COD"
sConsulta = sConsulta & vbLf & "WHERE PROCE.ANYO=@ANYO AND PROCE.GMN1=@GMN1 AND PROCE.COD=@PROCE"
sConsulta = sConsulta & vbLf & ""
sConsulta = sConsulta & vbLf & "OPEN Cur_PROCE"
sConsulta = sConsulta & vbLf & "FETCH NEXT FROM cur_PROCE INTO @DENGMN1,@GMN2,@GMN3,@GMN4,@DENGMN2,@DENGMN3,@DENGMN4,@PROCECAMBIO,@ADJDIR"
sConsulta = sConsulta & vbLf & "IF @@FETCH_STATUS<>0 GOTO ERR_"
sConsulta = sConsulta & vbLf & ""
sConsulta = sConsulta & vbLf & "DECLARE Cur_ITEM_ADJ Cursor FORWARD_ONLY STATIC READ_ONLY FOR"
sConsulta = sConsulta & vbLf & ""
sConsulta = sConsulta & vbLf & "SELECT ITEM_ADJ.ITEM,ITEM_ADJ.PROVE,ITEM.FECINI,ITEM.FECFIN,(ITEM_OFE.PRECIO/(PROCE_OFE.CAMBIO*@PROCECAMBIO)) AS PRECADJ,(ITEM.CANT*(ITEM_ADJ.PORCEN/100)) AS CANTADJ,(ITEM.PREC/@PROCECAMBIO) AS PRECAPE"
sConsulta = sConsulta & vbLf & ",ITEM.PAG,PAG.DEN,ITEM.UNI,UNI.DEN,ITEM.DEST,DEST.DEN"
sConsulta = sConsulta & vbLf & ",ITEM.ART,ITEM.DESCR + ART4.DEN,ITEM.CANT,ITEM_PRESPROY.PROY1,PRES_PROY1.DEN,ITEM_PRESPROY.ANYOPROY"
sConsulta = sConsulta & vbLf & "FROM ITEM_ADJ"
sConsulta = sConsulta & vbLf & "INNER JOIN ITEM ON ITEM_ADJ.ANYO=ITEM.ANYO AND ITEM_ADJ.GMN1=ITEM.GMN1 AND ITEM_ADJ.PROCE=ITEM.PROCE  AND ITEM_ADJ.ITEM=ITEM.ID"
sConsulta = sConsulta & vbLf & "INNER JOIN ITEM_PRESPROY ON ITEM.ANYO=ITEM_PRESPROY.ANYO AND ITEM.GMN1=ITEM_PRESPROY.GMN1 AND ITEM.PROCE=ITEM_PRESPROY.PROCE AND ITEM.ID=ITEM_PRESPROY.ITEM"
sConsulta = sConsulta & vbLf & "INNER JOIN PRES_PROY1 ON ITEM_PRESPROY.ANYOPROY=PRES_PROY1.ANYO AND ITEM_PRESPROY.PROY1=PRES_PROY1.COD"
sConsulta = sConsulta & vbLf & "INNER JOIN PROCE_OFE ON ITEM_ADJ.ANYO=PROCE_OFE.ANYO AND ITEM_ADJ.GMN1=PROCE_OFE.GMN1 AND ITEM_ADJ.PROCE=PROCE_OFE.PROCE AND ITEM_ADJ.OFE=PROCE_OFE.OFE AND ITEM_ADJ.PROVE=PROCE_OFE.PROVE"
sConsulta = sConsulta & vbLf & "INNER JOIN ITEM_OFE ON ITEM_ADJ.ANYO=ITEM_OFE.ANYO AND ITEM_OFE.GMN1=ITEM_OFE.GMN1 AND ITEM_ADJ.PROCE=ITEM_OFE.PROCE AND ITEM_ADJ.PROVE=ITEM_OFE.PROVE AND ITEM_ADJ.ITEM=ITEM_OFE.ITEM AND ITEM_ADJ.OFE=ITEM_OFE.NUM"
sConsulta = sConsulta & vbLf & "INNER JOIN UNI ON ITEM.UNI=UNI.COD"
sConsulta = sConsulta & vbLf & "INNER JOIN DEST ON ITEM.DEST=DEST.COD"
sConsulta = sConsulta & vbLf & "INNER JOIN PAG ON ITEM.PAG=PAG.COD"
sConsulta = sConsulta & vbLf & "LEFT JOIN ART4 ON ITEM.ART=ART4.COD AND ITEM.GMN1=ART4.GMN1 AND ITEM.GMN2=ART4.GMN2 AND ITEM.GMN3=ART4.GMN3 AND ITEM.GMN4=ART4.GMN4"
sConsulta = sConsulta & vbLf & "WHERE ITEM_ADJ.ANYO=@ANYO AND ITEM_ADJ.GMN1=@GMN1 AND ITEM_ADJ.PROCE=@PROCE AND ITEM_ADJ.PORCEN <> 0"
sConsulta = sConsulta & vbLf & "                    "
                    
sConsulta = sConsulta & vbLf & "OPEN Cur_ITEM_ADJ"
sConsulta = sConsulta & vbLf & "FETCH NEXT FROM Cur_ITEM_ADJ INTO @ITEM,@PROVE,@FECINI,@FECFIN,@PRECADJ,@CANTADJ,@PRECAPE"
sConsulta = sConsulta & vbLf & ",@PAG,@DENPAG,@UNI,@DENUNI,@DEST,@DENDEST"
sConsulta = sConsulta & vbLf & ",@ART,@DESCR,@CANT,@PROY1,@DENPROY1,@ANYOPROY"
sConsulta = sConsulta & vbLf & "WHILE @@FETCH_STATUS=0"
sConsulta = sConsulta & vbLf & "BEGIN"
sConsulta = sConsulta & vbLf & " "
sConsulta = sConsulta & vbLf & "/* Tenemos que calcular el porcentaje correspondiente a cada mes del aNYo.*/"
sConsulta = sConsulta & vbLf & "SET @TOTALDIAS = datediff(day,@FECINI, @FECFIN) + 1"
sConsulta = sConsulta & vbLf & "    "
sConsulta = sConsulta & vbLf & "IF YEAR(@FECINI) < @ANYOPROY"
sConsulta = sConsulta & vbLf & "SET @FECINICIAL = cast('1/1/' + str(@ANYOPROY)  as datetime)"
sConsulta = sConsulta & vbLf & "Else"
sConsulta = sConsulta & vbLf & "/*Solo puede ser igual */"
sConsulta = sConsulta & vbLf & "SET @FECINICIAL = @FECINI"
sConsulta = sConsulta & vbLf & "        "
sConsulta = sConsulta & vbLf & "IF YEAR(@FECFIN) > @ANYOPROY"
sConsulta = sConsulta & vbLf & "SET @FECFINAL = cast('12/31/' + str(@ANYOPROY)  as datetime)"
sConsulta = sConsulta & vbLf & "Else"
sConsulta = sConsulta & vbLf & "SET @FECFINAL = @FECFIN"
sConsulta = sConsulta & vbLf & ""
sConsulta = sConsulta & vbLf & "SET @MESINI = MONTH(@FECINICIAL)"
sConsulta = sConsulta & vbLf & "SET @FECHA = @FECINICIAL"
sConsulta = sConsulta & vbLf & ""
            
sConsulta = sConsulta & vbLf & "WHILE @MESINI < MONTH(@FECFINAL)"
sConsulta = sConsulta & vbLf & "BEGIN"
sConsulta = sConsulta & vbLf & "/* Calculamos la diferencia en d�as entre el mes inicial y el siguiente mes*/"
sConsulta = sConsulta & vbLf & "  "
sConsulta = sConsulta & vbLf & "SET @DIAS = datediff(day,@FECHA,cast( str (month(@FECHA) +1) + '/1/' + str(year(@FECHA) ) as datetime))"
sConsulta = sConsulta & vbLf & "SET @PORCEN = @DIAS / @TOTALDIAS"
sConsulta = sConsulta & vbLf & "SET @FECHA = cast(str((month(@FECHA) +1)) + '/1/' + str(year(@FECHA)) as datetime)"
  
sConsulta = sConsulta & vbLf & "/*Miramos si hab�a alg�n resultado para ese mes*/"
sConsulta = sConsulta & vbLf & "IF (SELECT COUNT(*) FROM AR_ITEM_MES_PARPROY1 WHERE"
sConsulta = sConsulta & vbLf & "ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM AND ANYOADJ=YEAR(@FECINICIAL) AND MES=@MESINI AND PROY1=@PROY1)=0"
sConsulta = sConsulta & vbLf & "BEGIN"
sConsulta = sConsulta & vbLf & "    "
sConsulta = sConsulta & vbLf & "INSERT INTO  AR_ITEM_MES_PARPROY1 ( ANYO,GMN1,PROCE,ITEM,ANYOADJ,MES,DENGMN1,ADJDIR,GMN2,GMN3,GMN4,DENGMN2,DENGMN3,DENGMN4"
sConsulta = sConsulta & vbLf & ",ART,DESCR,CANT"
sConsulta = sConsulta & vbLf & ",UNI,DENUNI,DEST,DENDEST,PAG,DENPAG,FECINI,FECFIN,PRES,PREC,CANTPERIODO,PROY1,DENPROY1)"
 
sConsulta = sConsulta & vbLf & "VALUES(@ANYO,@GMN1, @PROCE , @ITEM,YEAR(@FECINICIAL),@MESINI, @DENGMN1,@ADJDIR, @GMN2, @GMN3, @GMN4 ,@DENGMN2, @DENGMN3, @DENGMN4"
sConsulta = sConsulta & vbLf & ",@ART,@DESCR,@CANT"
sConsulta = sConsulta & vbLf & ",@UNI,@DENUNI,@DEST,@DENDEST,@PAG,@DENPAG,@FECINI,@FECFIN,@PRECAPE*@PORCEN*@CANTADJ,@PRECADJ*@PORCEN*@CANTADJ,@CANT*@PORCEN,@PROY1,@DENPROY1)"
    
sConsulta = sConsulta & vbLf & "End"
sConsulta = sConsulta & vbLf & "Else"
sConsulta = sConsulta & vbLf & "UPDATE AR_ITEM_MES_PARPROY1 SET PRES=PRES+@PRECAPE*@PORCEN*@CANTADJ, PREC=PREC+@PRECADJ*@PORCEN*@CANTADJ"
sConsulta = sConsulta & vbLf & "WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND MES=@MESINI AND ANYOADJ=YEAR(@FECINICIAL)  AND ITEM=@ITEM AND PROY1=@PROY1"
  
sConsulta = sConsulta & vbLf & "SET @MESINI=@MESINI + 1"
sConsulta = sConsulta & vbLf & "  "
  
sConsulta = sConsulta & vbLf & "End"

sConsulta = sConsulta & vbLf & "/*Si el m�s es el mismo*/"
sConsulta = sConsulta & vbLf & "IF @MESINI = MONTH(@FECFINAL)"
sConsulta = sConsulta & vbLf & "BEGIN"

sConsulta = sConsulta & vbLf & "SET @DIAS = datediff(day,@FECHA,@FECFINAL) + 1"
sConsulta = sConsulta & vbLf & "SET @PORCEN = @DIAS/@TOTALDIAS"

sConsulta = sConsulta & vbLf & "/*Miramos si hab�a alg�n resultado para ese mes*/"
sConsulta = sConsulta & vbLf & "IF (SELECT COUNT(*) FROM AR_ITEM_MES_PARPROY1 WHERE"

sConsulta = sConsulta & vbLf & "ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM AND ANYOADJ=YEAR(@FECINICIAL) AND MES=@MESINI AND PROY1=@PROY1)=0"
sConsulta = sConsulta & vbLf & "BEGIN"
    
sConsulta = sConsulta & vbLf & "INSERT INTO  AR_ITEM_MES_PARPROY1 ( ANYO, GMN1, PROCE, ITEM, ANYOADJ, MES, DENGMN1, ADJDIR, GMN2, GMN3, GMN4, DENGMN2, DENGMN3,DENGMN4"
sConsulta = sConsulta & vbLf & ", ART, DESCR, CANT, UNI, DENUNI, DEST, DENDEST, PAG, DENPAG, FECINI, FECFIN, PRES, PREC, CANTPERIODO, PROY1, DENPROY1)"
sConsulta = sConsulta & vbLf & "VALUES(@ANYO,@GMN1, @PROCE , @ITEM,YEAR(@FECINICIAL),@MESINI, @DENGMN1,@ADJDIR, @GMN2, @GMN3, @GMN4 ,@DENGMN2, @DENGMN3, @DENGMN4"
sConsulta = sConsulta & vbLf & ",@ART,@DESCR,@CANT,@UNI,@DENUNI,@DEST,@DENDEST,@PAG,@DENPAG,@FECINI,@FECFIN, @PRECAPE*@PORCEN*@CANTADJ,@PRECADJ*@PORCEN*@CANTADJ,@CANT*@PORCEN,@PROY1,@DENPROY1)"
    
sConsulta = sConsulta & vbLf & "End"
sConsulta = sConsulta & vbLf & "Else"
sConsulta = sConsulta & vbLf & "UPDATE AR_ITEM_MES_PARPROY1 SET PRES=PRES+@PRECAPE*@PORCEN*@CANTADJ, PREC=PREC+@PRECADJ*@PORCEN*@CANTADJ"
sConsulta = sConsulta & vbLf & "WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND MES=@MESINI AND ANYOADJ=YEAR(@FECINICIAL)  AND ITEM=@ITEM AND PROY1=@PROY1"
sConsulta = sConsulta & vbLf & ""
sConsulta = sConsulta & vbLf & "End"
sConsulta = sConsulta & vbLf & "FETCH NEXT FROM Cur_ITEM_ADJ INTO @ITEM,@PROVE,@FECINI,@FECFIN,@PRECADJ,@CANTADJ,@PRECAPE"
sConsulta = sConsulta & vbLf & ",@PAG,@DENPAG,@UNI,@DENUNI,@DEST,@DENDEST,@ART,@DESCR,@CANT,@PROY1,@DENPROY1,@ANYOPROY"
sConsulta = sConsulta & vbLf & "End"
sConsulta = sConsulta & vbLf & "ERR_:"
sConsulta = sConsulta & vbLf & "Close Cur_PROCE"
sConsulta = sConsulta & vbLf & "DEALLOCATE Cur_PROCE"
sConsulta = sConsulta & vbLf & "Close Cur_ITEM_ADJ"
sConsulta = sConsulta & vbLf & "DEALLOCATE Cur_ITEM_ADJ"
sConsulta = sConsulta & vbLf & "SET CONCAT_NULL_YIELDS_NULL ON"
sConsulta = sConsulta & vbLf & "SET @RES=@@ERROR"
sConsulta = sConsulta & vbLf & "Return"
sConsulta = sConsulta & vbLf & "End"

Modificar_SP_AR_ITEM_MES_PROY1_2_8_00_3 = sConsulta
End Function

Private Function CrearITEM_TG_UPD_2_8_00_3() As String
Dim sConsulta As String

sConsulta = "CREATE TRIGGER ITEM_TG_UPD ON dbo.ITEM"
sConsulta = sConsulta & vbCrLf & "FOR UPDATE"
sConsulta = sConsulta & vbCrLf & "AS"
sConsulta = sConsulta & vbCrLf & "DECLARE @ANYO AS INTEGER"
sConsulta = sConsulta & vbCrLf & "DECLARE @GMN1 AS VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "DECLARE @PROCE AS INTEGER"
sConsulta = sConsulta & vbCrLf & "DECLARE @ID AS INTEGER"
sConsulta = sConsulta & vbCrLf & "DECLARE @GMN2 AS VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "DECLARE @GMN3 AS VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "DECLARE @GMN4 AS VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "DECLARE @ART AS VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "DECLARE @CANT AS FLOAT"
sConsulta = sConsulta & vbCrLf & "DECLARE @FECINI AS DATETIME"
sConsulta = sConsulta & vbCrLf & "DECLARE @FECFIN AS DATETIME"
sConsulta = sConsulta & vbCrLf & "DECLARE @EST AS INTEGER"
sConsulta = sConsulta & vbCrLf & "DECLARE @OBJ AS FLOAT"
sConsulta = sConsulta & vbCrLf & "DECLARE @OBJOLD AS FLOAT"
sConsulta = sConsulta & vbCrLf & "DECLARE @FECNECMANUAL  AS TINYINT"
sConsulta = sConsulta & vbCrLf & ""
sConsulta = sConsulta & vbCrLf & "DECLARE curTG_ITEM_Upd CURSOR FOR SELECT ANYO,GMN1,PROCE,ID,GMN2,GMN3,GMN4,ART,CANT,FECINI,FECFIN,OBJ FROM INSERTED"
sConsulta = sConsulta & vbCrLf & "OPEN curTG_ITEM_Upd"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM curTG_ITEM_Upd INTO @ANYO,@GMN1,@PROCE,@ID,@GMN2,@GMN3,@GMN4,@ART,@CANT,@FECINI,@FECFIN,@OBJ"
sConsulta = sConsulta & vbCrLf & "WHILE @@FETCH_STATUS=0"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "IF UPDATE(FECINI)"
sConsulta = sConsulta & vbCrLf & "    BEGIN"
sConsulta = sConsulta & vbCrLf & "    DELETE FROM ITEM_PRESPROY WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ANYOPROY<YEAR(@FECINI)"
sConsulta = sConsulta & vbCrLf & "    DELETE FROM ITEM_PRESCON  WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ANYOPAR<YEAR(@FECINI)"
sConsulta = sConsulta & vbCrLf & "    End"
sConsulta = sConsulta & vbCrLf & "IF UPDATE(FECFIN)"
sConsulta = sConsulta & vbCrLf & "    BEGIN"
sConsulta = sConsulta & vbCrLf & "    DELETE FROM ITEM_PRESPROY WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ANYOPROY>YEAR(@FECFIN)"
sConsulta = sConsulta & vbCrLf & "    DELETE FROM ITEM_PRESCON  WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ANYOPAR>YEAR(@FECFIN)"
sConsulta = sConsulta & vbCrLf & "    End"
sConsulta = sConsulta & vbCrLf & "SET @EST = (SELECT EST FROM PROCE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE)"
sConsulta = sConsulta & vbCrLf & "SET @OBJOLD = (SELECT OBJ FROM DELETED WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ID=@ID)"
sConsulta = sConsulta & vbCrLf & "SET  @FECNECMANUAL=(SELECT PROCE.FECNECMANUAL FROM PROCE  WHERE ANYO=@ANYO  AND GMN1=@GMN1 AND COD=@PROCE)"

sConsulta = sConsulta & vbCrLf & "IF @FECNECMANUAL=0"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE SET FECNEC =(SELECT MIN(FECINI) FROM ITEM WHERE ANYO=@ANYO  AND GMN1=@GMN1 AND PROCE=@PROCE)"
sConsulta = sConsulta & vbCrLf & "WHERE COD=@PROCE"
sConsulta = sConsulta & vbCrLf & ""
sConsulta = sConsulta & vbCrLf & "IF @OBJ IS NULL"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "/*Si estamos en estado 8 y borramos el �ltimoobjetivo entonces pasamos a estado 7*/"
sConsulta = sConsulta & vbCrLf & "IF @EST = 8 AND ((SELECT COUNT(*) FROM ITEM WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND OBJ IS NOT NULL )=0) UPDATE PROCE SET EST=7 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE"
sConsulta = sConsulta & vbCrLf & "IF @EST = 9  AND ((SELECT COUNT(*) FROM ITEM WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND OBJ IS NOT NULL )=0) UPDATE PROCE SET EST=10 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE"
sConsulta = sConsulta & vbCrLf & "IF @OBJOLD IS NOT NULL AND ((SELECT COUNT(*) FROM ITEM WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND OBJ IS NOT NULL )=0) UPDATE PROCE_PROVE SET NUMOBJ=0,OBJNUEVOS=0 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "Else"
sConsulta = sConsulta & vbCrLf & "IF @OBJOLD IS NULL"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "IF @EST = 7 UPDATE PROCE SET EST=8 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE"
sConsulta = sConsulta & vbCrLf & "IF @EST = 10 UPDATE PROCE SET EST=9 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_PROVE SET OBJNUEVOS=1,NUMOBJ=NUMOBJ+1 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "Else"
sConsulta = sConsulta & vbCrLf & "IF @OBJOLD <> @OBJ"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "IF @EST = 7 UPDATE PROCE SET EST=8 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE"
sConsulta = sConsulta & vbCrLf & "IF @EST = 10 UPDATE PROCE SET EST=9 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_PROVE SET OBJNUEVOS=1,NUMOBJ=NUMOBJ+1 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & ""
sConsulta = sConsulta & vbCrLf & "_FETCH:"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM curTG_ITEM_Upd INTO @ANYO,@GMN1,@PROCE,@ID,@GMN2,@GMN3,@GMN4,@ART,@CANT,@FECINI,@FECFIN,@OBJ"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "Close curTG_ITEM_Upd"
sConsulta = sConsulta & vbCrLf & "DEALLOCATE curTG_ITEM_Upd"

CrearITEM_TG_UPD_2_8_00_3 = sConsulta

End Function





Private Function ModificarTriggerPROCE_PROVE_PET_TG_INS() As String
    Dim sConsulta As String
    
    sConsulta = "CREATE TRIGGER PROCE_PROVE_PET_TG_INS ON dbo.PROCE_PROVE_PET"
    sConsulta = sConsulta & vbCrLf & "FOR INSERT, UPDATE, DELETE"
    sConsulta = sConsulta & vbCrLf & "AS"
    sConsulta = sConsulta & vbCrLf & "DECLARE @ANYO AS INTEGER"
    sConsulta = sConsulta & vbCrLf & "DECLARE @GMN1 AS VARCHAR(50)"
    sConsulta = sConsulta & vbCrLf & "DECLARE @PROCE AS INTEGER"
    sConsulta = sConsulta & vbCrLf & "DECLARE @EST AS INTEGER"
    sConsulta = sConsulta & vbCrLf & "DECLARE @TIPOPET AS INTEGER"
    sConsulta = sConsulta & vbCrLf & "DECLARE @FECHAREU AS DATETIME"
    sConsulta = sConsulta & vbCrLf & "DECLARE curTG_PROCE_PROVE_PET_Ins CURSOR FOR SELECT ANYO,GMN1,PROCE,TIPOPET,FECHAREU FROM INSERTED"
    sConsulta = sConsulta & vbCrLf & "OPEN curTG_PROCE_PROVE_PET_Ins"
    sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM curTG_PROCE_PROVE_PET_Ins INTO @ANYO,@GMN1,@PROCE,@TIPOPET,@FECHAREU"
    sConsulta = sConsulta & vbCrLf & "WHILE @@FETCH_STATUS=0"
    sConsulta = sConsulta & vbCrLf & "BEGIN"
    sConsulta = sConsulta & vbCrLf & "SET @EST =  (SELECT PROCE.EST FROM PROCE WHERE ANYO=@ANYO  AND GMN1=@GMN1 AND COD=@PROCE)"
    sConsulta = sConsulta & vbCrLf & "IF @TIPOPET = 0 /*Peticiones de oferta*/"
    sConsulta = sConsulta & vbCrLf & "BEGIN"
    sConsulta = sConsulta & vbCrLf & "IF @EST < 6  /* 3= Validado */"
    sConsulta = sConsulta & vbCrLf & "UPDATE PROCE SET EST =6  WHERE ANYO= @ANYO  AND GMN1=@GMN1 AND COD=@PROCE"
    sConsulta = sConsulta & vbCrLf & "End"
    sConsulta = sConsulta & vbCrLf & "Else"
    sConsulta = sConsulta & vbCrLf & "IF @TIPOPET = 1 /*Comunicando objetivos*/"
    sConsulta = sConsulta & vbCrLf & "BEGIN"
    sConsulta = sConsulta & vbCrLf & "IF @EST = 8  /*Con objetivos sin notificar*/"
    sConsulta = sConsulta & vbCrLf & "UPDATE PROCE SET EST=7 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE"
    sConsulta = sConsulta & vbCrLf & "IF @EST = 9  /*Con objetivos sin notificar y preadjudicado*/"
    sConsulta = sConsulta & vbCrLf & "UPDATE PROCE SET EST=10 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE"
    sConsulta = sConsulta & vbCrLf & "End"
    sConsulta = sConsulta & vbCrLf & "Else"
    sConsulta = sConsulta & vbCrLf & "IF @TIPOPET = 2 OR @TIPOPET=3  /*Notificaci�n decierre*/"
    sConsulta = sConsulta & vbCrLf & "BEGIN"
    sConsulta = sConsulta & vbCrLf & "IF @EST=11"
    sConsulta = sConsulta & vbCrLf & "UPDATE PROCE SET EST=12 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE"
    sConsulta = sConsulta & vbCrLf & "IF @EST=12"
    sConsulta = sConsulta & vbCrLf & "UPDATE PROCE SET EST=13 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE"
    sConsulta = sConsulta & vbCrLf & "End"
    sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM curTG_PROCE_PROVE_PET_Ins INTO @ANYO,@GMN1,@PROCE,@TIPOPET,@FECHAREU"
    sConsulta = sConsulta & vbCrLf & "End"
    sConsulta = sConsulta & vbCrLf & "Close curTG_PROCE_PROVE_PET_Ins"
    sConsulta = sConsulta & vbCrLf & "DEALLOCATE curTG_PROCE_PROVE_PET_Ins"

    ModificarTriggerPROCE_PROVE_PET_TG_INS = sConsulta
End Function


Private Function ModificarSP_DEVOLVER_PROCESOS_PROVE_WEB_2_8_5_0() As String
Dim sConsulta As String

    sConsulta = "CREATE PROCEDURE SP_DEVOLVER_PROCESOS_PROVE_WEB (@PROVE VARCHAR(50),@NORMAL SMALLINT OUTPUT ,@SUBASTA SMALLINT OUTPUT) AS"
    sConsulta = sConsulta & vbCrLf & "BEGIN"
    sConsulta = sConsulta & vbCrLf & "SET @NORMAL=(SELECT COUNT(*) FROM PROCE INNER JOIN PROCE_PROVE ON PROCE.ANYO= PROCE_PROVE.ANYO AND"
    sConsulta = sConsulta & vbCrLf & "PROCE.GMN1=PROCE_PROVE.GMN1 AND PROCE.COD=PROCE_PROVE.PROCE AND PROCE_PROVE.PROVE=@PROVE"
    sConsulta = sConsulta & vbCrLf & "AND PROCE_PROVE.PUB=1 AND PROCE.EST<=11 AND SUBASTA=0)"
    sConsulta = sConsulta & vbCrLf & "SET @SUBASTA=(SELECT COUNT(*) FROM PROCE INNER JOIN PROCE_PROVE ON PROCE.ANYO= PROCE_PROVE.ANYO AND"
    sConsulta = sConsulta & vbCrLf & "PROCE.GMN1=PROCE_PROVE.GMN1 AND PROCE.COD=PROCE_PROVE.PROCE AND PROCE_PROVE.PROVE=@PROVE"
    sConsulta = sConsulta & vbCrLf & "AND PROCE_PROVE.PUB=1 AND PROCE.EST<=11 AND SUBASTA=1)"
    sConsulta = sConsulta & vbCrLf & "/*Devuelve en 1� lugar los procesos normales y luego los abiertos en modo subasta*/"
    sConsulta = sConsulta & vbCrLf & ""
    sConsulta = sConsulta & vbCrLf & "SELECT PROCE.ANYO,PROCE.GMN1,PROCE.COD,PROCE.DEN,PROCE.FECLIMOFE,PROCE_PROVE.OFE,PROCE.EST,PROCE_PROVE.SUPERADA,"
    sConsulta = sConsulta & vbCrLf & "PROCE.SUBASTA,PROCE.SUBASTAPROVE,PROCE.SUBASTAPUJAS,PROCE.MON,PROCE_PROVE.NUMOBJ,PROCE_PROVE.OBJNUEVOS "
    sConsulta = sConsulta & vbCrLf & "FROM PROCE INNER JOIN PROCE_PROVE ON PROCE.ANYO= PROCE_PROVE.ANYO AND"
    sConsulta = sConsulta & vbCrLf & "PROCE.GMN1=PROCE_PROVE.GMN1 AND PROCE.COD=PROCE_PROVE.PROCE AND PROCE_PROVE.PROVE=@PROVE"
    sConsulta = sConsulta & vbCrLf & "AND PROCE_PROVE.PUB=1 AND PROCE.EST<=11  ORDER BY SUBASTA"
    sConsulta = sConsulta & vbCrLf & "End"
    
    ModificarSP_DEVOLVER_PROCESOS_PROVE_WEB_2_8_5_0 = sConsulta
End Function


Private Function ModificarNUM_PROCE_PUB_2_8_5_0() As String
Dim sConsulta As String

    sConsulta = "CREATE PROCEDURE  NUM_PROCE_PUB (@PROVE VARCHAR(50),@NUM INT OUTPUT,@NUMNUE INT OUTPUT,@NUMAREV INT OUTPUT)  AS"
    sConsulta = sConsulta & vbCrLf & "BEGIN"
    sConsulta = sConsulta & vbCrLf & ""
    sConsulta = sConsulta & vbCrLf & "SET @NUM = (SELECT COUNT(*) FROM PROCE_PROVE INNER JOIN PROCE ON PROCE_PROVE.ANYO=PROCE.ANYO"
    sConsulta = sConsulta & vbCrLf & " AND PROCE_PROVE.GMN1=PROCE.GMN1 AND PROCE_PROVE.PROCE=PROCE.COD AND PROCE_PROVE.PROVE=@PROVE"
    sConsulta = sConsulta & vbCrLf & " AND PROCE_PROVE.PUB=1 AND PROCE.EST<=11 )"
    sConsulta = sConsulta & vbCrLf & ""
    sConsulta = sConsulta & vbCrLf & ""
    sConsulta = sConsulta & vbCrLf & "SET @NUMNUE = (SELECT COUNT(*) FROM PROCE_PROVE INNER JOIN PROCE ON PROCE_PROVE.ANYO=PROCE.ANYO"
    sConsulta = sConsulta & vbCrLf & " AND PROCE_PROVE.GMN1=PROCE.GMN1 AND PROCE_PROVE.PROCE=PROCE.COD AND PROCE_PROVE.PROVE=@PROVE"
    sConsulta = sConsulta & vbCrLf & " AND PROCE_PROVE.PUB=1 AND PROCE.EST>=5 AND PROCE.EST<=6)"
    sConsulta = sConsulta & vbCrLf & ""
    sConsulta = sConsulta & vbCrLf & "SET @NUMAREV = (SELECT COUNT(*)  FROM PROCE_PROVE INNER JOIN PROCE ON PROCE_PROVE.ANYO=PROCE.ANYO"
    sConsulta = sConsulta & vbCrLf & "AND PROCE_PROVE.GMN1=PROCE.GMN1 AND PROCE_PROVE.PROCE=PROCE.COD AND PROCE_PROVE.PROVE=@PROVE"
    sConsulta = sConsulta & vbCrLf & "AND PROCE_PROVE.PUB=1 AND PROCE_PROVE.OBJNUEVOS=1 AND PROCE.SUBASTA=0 AND PROCE.EST <= 10)"
    sConsulta = sConsulta & vbCrLf & ""
    sConsulta = sConsulta & vbCrLf & "Return"
    sConsulta = sConsulta & vbCrLf & "End"

    ModificarNUM_PROCE_PUB_2_8_5_0 = sConsulta
End Function

Private Function ModificarSP_ITEMS_2_8_5_0() As String
    Dim sConsulta As String
    
    sConsulta = "CREATE PROCEDURE  SP_ITEMS  (@ANYO INT,@GMN1 VARCHAR(50),@COD INT,@PROVEEDOR BIT,@CODMON varchar(50),@PROCEEST SMALLINT)  AS "
    sConsulta = sConsulta & vbCrLf & "BEGIN "
    sConsulta = sConsulta & vbCrLf & "SET CONCAT_NULL_YIELDS_NULL OFF "
    sConsulta = sConsulta & vbCrLf & "DECLARE @EQUIV FLOAT "
    sConsulta = sConsulta & vbCrLf & "DECLARE @EQUIVPROC FLOAT  "
    sConsulta = sConsulta & vbCrLf & ""
    sConsulta = sConsulta & vbCrLf & "SELECT @EQUIVPROC = CAMBIO FROM PROCE  WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@COD"
    sConsulta = sConsulta & vbCrLf & "SELECT @EQUIV= MON.EQUIV  FROM MON WHERE MON.COD=@CODMON "
    sConsulta = sConsulta & vbCrLf & "    SELECT ITEM.ANYO, "
    sConsulta = sConsulta & vbCrLf & "           ITEM.GMN1, "
    sConsulta = sConsulta & vbCrLf & "           ITEM.PROCE, "
    sConsulta = sConsulta & vbCrLf & "           ITEM.ID, "
    sConsulta = sConsulta & vbCrLf & "           ITEM.GMN2, "
    sConsulta = sConsulta & vbCrLf & "           ITEM.GMN3, "
    sConsulta = sConsulta & vbCrLf & "           ITEM.GMN4, "
    sConsulta = sConsulta & vbCrLf & "           ITEM.ART, "
    sConsulta = sConsulta & vbCrLf & "           ITEM.DESCR + ART4.DEN AS DESCR, "
    sConsulta = sConsulta & vbCrLf & "           ITEM.DEST, "
    sConsulta = sConsulta & vbCrLf & "           ITEM.UNI, "
    sConsulta = sConsulta & vbCrLf & "           ITEM.CANT, "
    sConsulta = sConsulta & vbCrLf & "           ITEM.PREC,"
    sConsulta = sConsulta & vbCrLf & "           ITEM.PRES, "
    sConsulta = sConsulta & vbCrLf & "           ITEM.PAG,"
    sConsulta = sConsulta & vbCrLf & "           ITEM.FECINI, "
    sConsulta = sConsulta & vbCrLf & "           ITEM.FECFIN, "
    sConsulta = sConsulta & vbCrLf & "           ITEM.ESP, "
    sConsulta = sConsulta & vbCrLf & "           ITEM.CONF, "
    sConsulta = sConsulta & vbCrLf & "           round(ITEM.OBJ*@EQUIV*@EQUIVPROC,5) AS OBJ , "
    sConsulta = sConsulta & vbCrLf & "           ITEM.FECHAOBJ,"
    sConsulta = sConsulta & vbCrLf & "           CASE  WHEN NUMESP > 0 THEN 1 WHEN NOT ITEM.ESP  IS NULL THEN 1 ELSE 0 END ESPECADJ , "
    sConsulta = sConsulta & vbCrLf & "           ITEM.ANYO, "
    sConsulta = sConsulta & vbCrLf & "           ITEM.GMN1, "
    sConsulta = sConsulta & vbCrLf & "           ITEM.PROCE , "
    sConsulta = sConsulta & vbCrLf & "           round(OFEMIN.PRECIO*@EQUIV,5) AS MINPRECIO ,"
    sConsulta = sConsulta & vbCrLf & "           CASE WHEN @PROVEEDOR=1 THEN OFEMIN.PROVE + ' - ' + PROVE.DEN ELSE NULL END MINPROVE"
    sConsulta = sConsulta & vbCrLf & "      FROM ((ITEM LEFT JOIN ART4 "
    sConsulta = sConsulta & vbCrLf & "                    ON ITEM.ART = ART4.COD "
    sConsulta = sConsulta & vbCrLf & "                   AND ITEM.GMN4 = ART4.GMN4 "
    sConsulta = sConsulta & vbCrLf & "                   AND ITEM.GMN3 = ART4.GMN3 "
    sConsulta = sConsulta & vbCrLf & "                   AND ITEM.GMN2 = ART4.GMN2"
    sConsulta = sConsulta & vbCrLf & "                   AND ITEM.GMN1 = ART4.GMN1) LEFT JOIN "
    sConsulta = sConsulta & vbCrLf & "                                                (SELECT ITEM_ESP.ANYO, "
    sConsulta = sConsulta & vbCrLf & "                                                        ITEM_ESP.GMN1, "
    sConsulta = sConsulta & vbCrLf & "                                                        ITEM_ESP.PROCE, "
    sConsulta = sConsulta & vbCrLf & "                                                        ITEM_ESP.ITEM, "
    sConsulta = sConsulta & vbCrLf & "                                                        Count(ITEM_ESP.ID) NUMESP "
    sConsulta = sConsulta & vbCrLf & "                                                   From ITEM_ESP "
    sConsulta = sConsulta & vbCrLf & "                                                  WHERE ITEM_ESP.ANYO=@ANYO "
    sConsulta = sConsulta & vbCrLf & "                                                    AND ITEM_ESP.GMN1=@GMN1 "
    sConsulta = sConsulta & vbCrLf & "                                                    AND ITEM_ESP.PROCE=@COD "
    sConsulta = sConsulta & vbCrLf & "                                                GROUP BY ITEM_ESP.ANYO, "
    sConsulta = sConsulta & vbCrLf & "                                                         ITEM_ESP.GMN1, "
    sConsulta = sConsulta & vbCrLf & "                                                         ITEM_ESP.PROCE, "
    sConsulta = sConsulta & vbCrLf & "                                                         ITEM_ESP.ITEM "
    sConsulta = sConsulta & vbCrLf & "                                                 ) ITEMESP "
    sConsulta = sConsulta & vbCrLf & "                                                ON ITEM.ID = ITEMESP.ITEM "
    sConsulta = sConsulta & vbCrLf & "                                               AND ITEM.GMN1 = ITEMESP.GMN1"
    sConsulta = sConsulta & vbCrLf & "                                               AND ITEM.PROCE = ITEMESP.PROCE"
    sConsulta = sConsulta & vbCrLf & "                                               AND ITEM.ANYO = ITEMESP.ANYO "
    sConsulta = sConsulta & vbCrLf & "            )"
    sConsulta = sConsulta & vbCrLf & "           LEFT JOIN OFEMIN "
    sConsulta = sConsulta & vbCrLf & "             ON OFEMIN.ANYO=ITEM.ANYO "
    sConsulta = sConsulta & vbCrLf & "            AND OFEMIN.GMN1=ITEM.GMN1 "
    sConsulta = sConsulta & vbCrLf & "            AND OFEMIN.PROCE=ITEM.PROCE "
    sConsulta = sConsulta & vbCrLf & "            AND OFEMIN.ITEM=ITEM.ID"
    sConsulta = sConsulta & vbCrLf & "           LEFT JOIN PROVE "
    sConsulta = sConsulta & vbCrLf & "             ON OFEMIN.PROVE=PROVE.COD"
    sConsulta = sConsulta & vbCrLf & "      WHERE ITEM.ANYO=@ANYO "
    sConsulta = sConsulta & vbCrLf & "        AND ITEM.GMN1=@GMN1 "
    sConsulta = sConsulta & vbCrLf & "        AND ITEM.PROCE=@COD "
    sConsulta = sConsulta & vbCrLf & "        AND CASE WHEN ITEM.EST IS NULL THEN -1 ELSE ITEM.EST END = CASE WHEN @PROCEEST=11 THEN 0 ELSE -1 END"
    sConsulta = sConsulta & vbCrLf & "--      WHERE ITEM.ANYO=@ANYO AND ITEM.GMN1=@GMN1 AND ITEM.PROCE=@COD AND ITEM.EST=CASE WHEN @PROCEEST=11 THEN 0 ELSE NULL END"
    sConsulta = sConsulta & vbCrLf & ""
    sConsulta = sConsulta & vbCrLf & "SET CONCAT_NULL_YIELDS_NULL ON "
    sConsulta = sConsulta & vbCrLf & "Return "
    sConsulta = sConsulta & vbCrLf & "End"

    ModificarSP_ITEMS_2_8_5_0 = sConsulta
End Function

Private Function ModificarSP_LINEAS_2_8_5_0() As String
Dim sConsulta As String

   sConsulta = "CREATE PROCEDURE  SP_LINEAS  (@PROVE VARCHAR(50),@NUM INT,@ANYO INT,@GMN1 VARCHAR(50),@PROCE INT,@PROCEEST SMALLINT)  AS"
   sConsulta = sConsulta & vbCrLf & "BEGIN"
   sConsulta = sConsulta & vbCrLf & ""
   sConsulta = sConsulta & vbCrLf & "    SET CONCAT_NULL_YIELDS_NULL OFF"
   sConsulta = sConsulta & vbCrLf & "SELECT ITEM.ANYO,ITEM.GMN1,ITEM.PROCE,ITEM.ID,ITEM.ART,ITEM.DESCR + ART4.DEN AS DESCR,ITEM.DEST,ITEM.UNI,ITEM.CANT,ITEM.PREC,ITEM.PRES,ITEM.PAG,ITEM.FECINI,ITEM.FECFIN,ITEM_OFE.PRECIO AS PRECOFE,ITEM_OFE.CANTMAX,PROVE_ART4.HOM,DEST.DEN AS DESTDEN, UNI.DEN AS UNIDEN, PAG.DEN AS PAGDEN "
   sConsulta = sConsulta & vbCrLf & "  FROM  ITEM "
   sConsulta = sConsulta & vbCrLf & "    LEFT JOIN ART4 "
   sConsulta = sConsulta & vbCrLf & "           ON ITEM.GMN1=ART4.GMN1 "
   sConsulta = sConsulta & vbCrLf & "          AND ITEM.GMN2=ART4.GMN2 "
   sConsulta = sConsulta & vbCrLf & "          AND ITEM.GMN3=ART4.GMN3  "
   sConsulta = sConsulta & vbCrLf & "          AND ITEM.GMN4=ART4.GMN4 "
   sConsulta = sConsulta & vbCrLf & "          AND ITEM.ART=ART4.COD "
   sConsulta = sConsulta & vbCrLf & "    LEFT JOIN ITEM_OFE "
   sConsulta = sConsulta & vbCrLf & "           ON ITEM.ANYO=ITEM_OFE.ANYO "
   sConsulta = sConsulta & vbCrLf & "          AND ITEM.GMN1=ITEM_OFE.GMN1 "
   sConsulta = sConsulta & vbCrLf & "          AND ITEM.PROCE=ITEM_OFE.PROCE "
   sConsulta = sConsulta & vbCrLf & "          AND ITEM.ID=ITEM_OFE.ITEM "
   sConsulta = sConsulta & vbCrLf & "          AND ITEM_OFE.PROVE=@PROVE "
   sConsulta = sConsulta & vbCrLf & "          AND ITEM_OFE.NUM= @NUM "
   sConsulta = sConsulta & vbCrLf & "    LEFT JOIN PROVE_ART4 "
   sConsulta = sConsulta & vbCrLf & "           ON PROVE_ART4.GMN1=ITEM.GMN1 "
   sConsulta = sConsulta & vbCrLf & "          AND PROVE_ART4.GMN2=ITEM.GMN2 "
   sConsulta = sConsulta & vbCrLf & "          AND PROVE_ART4.GMN3=ITEM.GMN3 "
   sConsulta = sConsulta & vbCrLf & "          AND PROVE_ART4.GMN4=ITEM.GMN4 "
   sConsulta = sConsulta & vbCrLf & "          AND PROVE_ART4.ART=ITEM.ART "
   sConsulta = sConsulta & vbCrLf & "          AND PROVE_ART4.PROVE=@PROVE "
   sConsulta = sConsulta & vbCrLf & "    LEFT JOIN DEST "
   sConsulta = sConsulta & vbCrLf & "           ON ITEM.DEST=DEST.COD "
   sConsulta = sConsulta & vbCrLf & "    LEFT JOIN UNI "
   sConsulta = sConsulta & vbCrLf & "           ON ITEM.UNI = UNI.COD "
   sConsulta = sConsulta & vbCrLf & "    LEFT JOIN PAG "
   sConsulta = sConsulta & vbCrLf & "           ON ITEM.PAG = PAG.COD "
   sConsulta = sConsulta & vbCrLf & " WHERE ITEM.ANYO=@ANYO "
   sConsulta = sConsulta & vbCrLf & "   AND ITEM.GMN1=@GMN1 "
   sConsulta = sConsulta & vbCrLf & "   AND ITEM.PROCE=@PROCE "
   sConsulta = sConsulta & vbCrLf & "   AND ITEM.CONF=1 "
   sConsulta = sConsulta & vbCrLf & "   AND CASE WHEN ITEM.EST IS NULL THEN -1 ELSE ITEM.EST END = CASE WHEN @PROCEEST=11 THEN 0 ELSE -1 END"
   sConsulta = sConsulta & vbCrLf & "ORDER BY ITEM.ART"
   sConsulta = sConsulta & vbCrLf & ""
   sConsulta = sConsulta & vbCrLf & "        SET CONCAT_NULL_YIELDS_NULL ON"
   sConsulta = sConsulta & vbCrLf & "Return"
   sConsulta = sConsulta & vbCrLf & "End"
    
    ModificarSP_LINEAS_2_8_5_0 = sConsulta
End Function


Private Function ModificarSP_CIERRE_2_8_5_0() As String
Dim sConsulta As String
    
    sConsulta = "CREATE PROCEDURE SP_CIERRE(@NEM TINYINT,@ANYO SMALLINT,@GMN1 VARCHAR(20),@PROCE INTEGER, @RES INTEGER OUTPUT) AS"
    sConsulta = sConsulta & vbCrLf & "BEGIN"
    sConsulta = sConsulta & vbCrLf & "EXECUTE SP_AR_PROCE @ANYO,@GMN1,@PROCE,@RES"
    sConsulta = sConsulta & vbCrLf & "IF @RES<>0 RETURN @RES"
    sConsulta = sConsulta & vbCrLf & "EXECUTE SP_AR_ITEM @NEM,@ANYO,@GMN1,@PROCE,@RES"
    sConsulta = sConsulta & vbCrLf & "IF @RES<>0 RETURN @RES"
    sConsulta = sConsulta & vbCrLf & "EXECUTE SP_AR_ITEM_MES @NEM,@ANYO,@GMN1,@PROCE,@RES"
    sConsulta = sConsulta & vbCrLf & "IF @RES<>0 RETURN @RES"
    sConsulta = sConsulta & vbCrLf & "EXECUTE SP_AR_ITEM_MES_UON1 @NEM,@ANYO,@GMN1,@PROCE,@RES"
    sConsulta = sConsulta & vbCrLf & "IF @RES<>0 RETURN @RES"
    sConsulta = sConsulta & vbCrLf & "EXECUTE SP_AR_ITEM_MES_UON2 @NEM,@ANYO,@GMN1,@PROCE,@RES"
    sConsulta = sConsulta & vbCrLf & "IF @RES<>0 RETURN @RES"
    sConsulta = sConsulta & vbCrLf & "EXECUTE SP_AR_ITEM_MES_UON3 @NEM,@ANYO,@GMN1,@PROCE,@RES"
    sConsulta = sConsulta & vbCrLf & "IF @RES<>0 RETURN @RES"
    sConsulta = sConsulta & vbCrLf & "EXECUTE SP_AR_ITEM_MES_PARCON1 @ANYO,@GMN1,@PROCE,@RES"
    sConsulta = sConsulta & vbCrLf & "IF @RES<>0 RETURN @RES"
    sConsulta = sConsulta & vbCrLf & "EXECUTE SP_AR_ITEM_MES_PARCON2 @ANYO,@GMN1,@PROCE,@RES"
    sConsulta = sConsulta & vbCrLf & "IF @RES<>0 RETURN @RES"
    sConsulta = sConsulta & vbCrLf & "EXECUTE SP_AR_ITEM_MES_PARCON3 @ANYO,@GMN1,@PROCE,@RES"
    sConsulta = sConsulta & vbCrLf & "IF @RES<>0 RETURN @RES"
    sConsulta = sConsulta & vbCrLf & "EXECUTE SP_AR_ITEM_MES_PARCON4 @ANYO,@GMN1,@PROCE,@RES"
    sConsulta = sConsulta & vbCrLf & "IF @RES<>0 RETURN @RES"
    sConsulta = sConsulta & vbCrLf & "EXECUTE SP_AR_ITEM_MES_PROY1 @ANYO,@GMN1,@PROCE,@RES"
    sConsulta = sConsulta & vbCrLf & "IF @RES<>0 RETURN @RES"
    sConsulta = sConsulta & vbCrLf & "EXECUTE SP_AR_ITEM_MES_PROY2 @ANYO,@GMN1,@PROCE,@RES"
    sConsulta = sConsulta & vbCrLf & "IF @RES<>0 RETURN @RES"
    sConsulta = sConsulta & vbCrLf & "EXECUTE SP_AR_ITEM_MES_PROY3 @ANYO,@GMN1,@PROCE,@RES"
    sConsulta = sConsulta & vbCrLf & "IF @RES<>0 RETURN @RES"
    sConsulta = sConsulta & vbCrLf & "EXECUTE SP_AR_ITEM_MES_PROY4 @ANYO,@GMN1,@PROCE,@RES"
    sConsulta = sConsulta & vbCrLf & "IF @RES<>0 RETURN @RES"
    sConsulta = sConsulta & vbCrLf & "EXECUTE SP_AR_ITEM_PROVE @ANYO,@GMN1,@PROCE,@RES"
    sConsulta = sConsulta & vbCrLf & "IF @RES<>0 RETURN @RES"
    sConsulta = sConsulta & vbCrLf & "EXECUTE SP_AR_ITEM_MES_PRESCON3_1 @ANYO,@GMN1,@PROCE,@RES"
    sConsulta = sConsulta & vbCrLf & "IF @RES<>0 RETURN @RES"
    sConsulta = sConsulta & vbCrLf & "EXECUTE SP_AR_ITEM_MES_PRESCON3_2 @ANYO,@GMN1,@PROCE,@RES"
    sConsulta = sConsulta & vbCrLf & "IF @RES<>0 RETURN @RES"
    sConsulta = sConsulta & vbCrLf & "EXECUTE SP_AR_ITEM_MES_PRESCON3_3 @ANYO,@GMN1,@PROCE,@RES"
    sConsulta = sConsulta & vbCrLf & "IF @RES<>0 RETURN @RES"
    sConsulta = sConsulta & vbCrLf & "EXECUTE SP_AR_ITEM_MES_PRESCON3_4 @ANYO,@GMN1,@PROCE,@RES"
    sConsulta = sConsulta & vbCrLf & "IF @RES<>0 RETURN @RES"
    sConsulta = sConsulta & vbCrLf & "EXECUTE SP_AR_ITEM_MES_PRESCON4_1 @ANYO,@GMN1,@PROCE,@RES"
    sConsulta = sConsulta & vbCrLf & "IF @RES<>0 RETURN @RES"
    sConsulta = sConsulta & vbCrLf & "EXECUTE SP_AR_ITEM_MES_PRESCON4_2 @ANYO,@GMN1,@PROCE,@RES"
    sConsulta = sConsulta & vbCrLf & "IF @RES<>0 RETURN @RES"
    sConsulta = sConsulta & vbCrLf & "EXECUTE SP_AR_ITEM_MES_PRESCON4_3 @ANYO,@GMN1,@PROCE,@RES"
    sConsulta = sConsulta & vbCrLf & "IF @RES<>0 RETURN @RES"
    sConsulta = sConsulta & vbCrLf & "EXECUTE SP_AR_ITEM_MES_PRESCON4_4 @ANYO,@GMN1,@PROCE,@RES"
    sConsulta = sConsulta & vbCrLf & "IF @RES<>0 RETURN @RES"
    sConsulta = sConsulta & vbCrLf & "EXECUTE SP_AR_ITEM_PRESCON3 @ANYO,@GMN1,@PROCE,@RES"
    sConsulta = sConsulta & vbCrLf & "IF @RES<>0 RETURN @RES"
    sConsulta = sConsulta & vbCrLf & "EXECUTE SP_AR_ITEM_PRESCON4 @ANYO,@GMN1,@PROCE,@RES"
    sConsulta = sConsulta & vbCrLf & ""
    sConsulta = sConsulta & vbCrLf & "Return @RES"
    sConsulta = sConsulta & vbCrLf & "End"

    ModificarSP_CIERRE_2_8_5_0 = sConsulta
End Function

Private Function ModificarSP_AR_PROCE_2_8_5_0() As String
    Dim sConsulta As String
    
    sConsulta = "CREATE PROCEDURE  SP_AR_PROCE (@ANYO SMALLINT,@GMN1 VARCHAR(50), @PROCE INT,@RES INT OUTPUT)  AS"
    sConsulta = sConsulta & vbCrLf & "BEGIN"
    sConsulta = sConsulta & vbCrLf & "DECLARE @PRESCONSUMIDO AS FLOAT"
    sConsulta = sConsulta & vbCrLf & "DECLARE @PRESABIERTO AS FLOAT"
    sConsulta = sConsulta & vbCrLf & "DECLARE @PRESGLOBAL AS FLOAT"
    sConsulta = sConsulta & vbCrLf & "DECLARE @IMPORTE AS FLOAT"
    sConsulta = sConsulta & vbCrLf & "/* Recorremos la tabla de ITEM_ADJ calculando la suma de presupuesto consumido*/"
    sConsulta = sConsulta & vbCrLf & "SET @PRESCONSUMIDO =  (SELECT  SUM ((ITEM_ADJ.PORCEN/100)*ITEM.CANT*ITEM.PREC/PROCE.CAMBIO)"
    sConsulta = sConsulta & vbCrLf & "From ITEM_ADJ"
    sConsulta = sConsulta & vbCrLf & "INNER JOIN ITEM"
    sConsulta = sConsulta & vbCrLf & "ON ITEM_ADJ.ANYO=ITEM.ANYO AND ITEM_ADJ.GMN1=ITEM.GMN1 AND ITEM_ADJ.PROCE=ITEM.PROCE"
    sConsulta = sConsulta & vbCrLf & "AND ITEM_ADJ.ITEM=ITEM.ID AND ITEM_ADJ.ANYO=@ANYO AND ITEM_ADJ.GMN1=@GMN1 AND ITEM_ADJ.PROCE=@PROCE AND ITEM_ADJ.PORCEN <> 0"
    sConsulta = sConsulta & vbCrLf & "INNER JOIN PROCE"
    sConsulta = sConsulta & vbCrLf & "ON ITEM.ANYO=PROCE.ANYO AND ITEM.GMN1=PROCE.GMN1 AND ITEM.PROCE=PROCE.COD)"
    sConsulta = sConsulta & vbCrLf & " "
    sConsulta = sConsulta & vbCrLf & "/* Recorremos la tabla de ITEM calculando la suma de presupuesto abierto*/"
    sConsulta = sConsulta & vbCrLf & "SET @PRESABIERTO =  (SELECT  SUM (ITEM.CANT*ITEM.PREC/PROCE.CAMBIO)"
    sConsulta = sConsulta & vbCrLf & "From Item"
    sConsulta = sConsulta & vbCrLf & "INNER JOIN PROCE"
    sConsulta = sConsulta & vbCrLf & "ON ITEM.ANYO=PROCE.ANYO AND ITEM.GMN1=PROCE.GMN1 AND ITEM.PROCE=PROCE.COD"
    sConsulta = sConsulta & vbCrLf & "AND ITEM.ANYO=@ANYO AND ITEM.GMN1=@GMN1 AND ITEM.PROCE=@PROCE"
    sConsulta = sConsulta & vbCrLf & "AND ITEM.CONF = 1)"
    sConsulta = sConsulta & vbCrLf & " "
    sConsulta = sConsulta & vbCrLf & "/* Recorremos la tabla de ITEM_ADJ calculando la suma de importe adjudicado*/"
    sConsulta = sConsulta & vbCrLf & "SET @IMPORTE =  (SELECT  SUM ((ITEM_ADJ.PORCEN/100)*ITEM.CANT*ITEM_OFE.PRECIO/(PROCE_OFE.CAMBIO*PROCE.CAMBIO))"
    sConsulta = sConsulta & vbCrLf & "From ITEM_ADJ"
    sConsulta = sConsulta & vbCrLf & "INNER JOIN ITEM_OFE"
    sConsulta = sConsulta & vbCrLf & "ON ITEM_ADJ.ANYO=ITEM_OFE.ANYO AND ITEM_ADJ.GMN1=ITEM_OFE.GMN1 AND ITEM_ADJ.PROCE=ITEM_OFE.PROCE"
    sConsulta = sConsulta & vbCrLf & "AND ITEM_ADJ.OFE=ITEM_OFE.NUM AND ITEM_ADJ.PROVE=ITEM_OFE.PROVE"
    sConsulta = sConsulta & vbCrLf & "AND ITEM_ADJ.ITEM=ITEM_OFE.ITEM"
    sConsulta = sConsulta & vbCrLf & "AND ITEM_ADJ.PORCEN <> 0 AND ITEM_ADJ.ANYO=@ANYO AND ITEM_ADJ.GMN1=@GMN1 AND ITEM_ADJ.PROCE=@PROCE"
    sConsulta = sConsulta & vbCrLf & "INNER JOIN PROCE_OFE"
    sConsulta = sConsulta & vbCrLf & "ON ITEM_OFE.ANYO=PROCE_OFE.ANYO AND ITEM_OFE.GMN1=PROCE_OFE.GMN1 AND ITEM_OFE.PROCE=PROCE_OFE.PROCE AND ITEM_OFE.NUM=PROCE_OFE.OFE"
    sConsulta = sConsulta & vbCrLf & "AND ITEM_OFE.PROVE=PROCE_OFE.PROVE"
    sConsulta = sConsulta & vbCrLf & "INNER JOIN PROCE"
    sConsulta = sConsulta & vbCrLf & "ON PROCE_OFE.ANYO=PROCE.ANYO AND PROCE_OFE.GMN1=PROCE.GMN1 AND PROCE_OFE.PROCE=PROCE.COD"
    sConsulta = sConsulta & vbCrLf & "INNER JOIN ITEM ON ITEM_OFE.ANYO=ITEM.ANYO AND ITEM_OFE.GMN1=ITEM.GMN1 AND ITEM_OFE.PROCE=ITEM.PROCE"
    sConsulta = sConsulta & vbCrLf & "AND ITEM_OFE.ITEM=ITEM.ID )"
    sConsulta = sConsulta & vbCrLf & ""
    sConsulta = sConsulta & vbCrLf & "/*antes de almacenar borramos el registro de AR_PROCE si ya exist�a*/"
    sConsulta = sConsulta & vbCrLf & "DELETE FROM AR_PROCE WHERE ANYO = @ANYO AND GMN1=@GMN1 AND COD = @PROCE"
    sConsulta = sConsulta & vbCrLf & ""
    sConsulta = sConsulta & vbCrLf & "/* Almacenamos los valores en la tabla AR_PROCE */"
    sConsulta = sConsulta & vbCrLf & "INSERT INTO AR_PROCE"
    sConsulta = sConsulta & vbCrLf & "(ANYO,GMN1,COD, ADJDIR, GMN2, GMN3, GMN4, EQP,COM,DENGMN1,DENGMN2,DENGMN3,DENGMN4,DENEQP,APECOM"
    sConsulta = sConsulta & vbCrLf & ",NOMCOM,FECULTREU,PRESGLOBAL,PRESABIERTO,PRESCONSUMIDO,IMPORTE)"
    sConsulta = sConsulta & vbCrLf & "SELECT  @ANYO,@GMN1,@PROCE,ADJDIR,GMN2.COD,GMN3.COD,GMN4.COD,PROCE.EQP,PROCE.COM"
    sConsulta = sConsulta & vbCrLf & ",GMN1.DEN,GMN2.DEN,GMN3.DEN,GMN4.DEN,EQP.DEN,COM.APE,COM.NOM,FECULTREU,PRES"
    sConsulta = sConsulta & vbCrLf & ",@PRESABIERTO,@PRESCONSUMIDO,@IMPORTE"
    sConsulta = sConsulta & vbCrLf & "From PROCE"
    sConsulta = sConsulta & vbCrLf & "LEFT OUTER  JOIN EQP"
    sConsulta = sConsulta & vbCrLf & "ON PROCE.ANYO=@ANYO AND PROCE.GMN1=@GMN1 AND PROCE.COD=@PROCE AND PROCE.EQP=EQP.COD"
    sConsulta = sConsulta & vbCrLf & "LEFT OUTER JOIN COM ON PROCE.EQP=COM.EQP AND PROCE.COM=COM.COD"
    sConsulta = sConsulta & vbCrLf & "INNER JOIN GMN1 ON PROCE.GMN1=GMN1.COD"
    sConsulta = sConsulta & vbCrLf & "INNER JOIN GMN2 ON  PROCE.GMN1=GMN2.GMN1 AND PROCE.GMN2=GMN2.COD"
    sConsulta = sConsulta & vbCrLf & "INNER JOIN GMN3 ON  PROCE.GMN1=GMN3.GMN1 AND  PROCE.GMN2=GMN3.GMN2 AND PROCE.GMN3=GMN3.COD"
    sConsulta = sConsulta & vbCrLf & "INNER JOIN GMN4 ON  PROCE.GMN1=GMN4.GMN1 AND  PROCE.GMN2=GMN4.GMN2 AND PROCE.GMN3=GMN4.GMN3 AND PROCE.GMN4=GMN4.COD"
    sConsulta = sConsulta & vbCrLf & "WHERE PROCE.ANYO = @ANYO AND PROCE.GMN1=@GMN1 AND PROCE.COD = @PROCE"
    sConsulta = sConsulta & vbCrLf & "SET @RES=@@ERROR"
    sConsulta = sConsulta & vbCrLf & " "
    sConsulta = sConsulta & vbCrLf & "Return"
    sConsulta = sConsulta & vbCrLf & "End"

    ModificarSP_AR_PROCE_2_8_5_0 = sConsulta
End Function

Private Function ModificarSP_AR_ITEM_2_8_5_0() As String
    Dim sConsulta As String
    
    sConsulta = "CREATE PROCEDURE SP_AR_ITEM (@NEM TINYINT,@ANYO SMALLINT,@GMN1 VARCHAR(50), @PROCE INT,@RES INT OUTPUT)  AS"
    sConsulta = sConsulta & vbCrLf & "BEGIN"
    sConsulta = sConsulta & vbCrLf & "DECLARE @ITEM AS INTEGER"
    sConsulta = sConsulta & vbCrLf & "DECLARE @DENGMN1 AS VARCHAR(100)"
    sConsulta = sConsulta & vbCrLf & "DECLARE @DENGMN2 AS VARCHAR(100)"
    sConsulta = sConsulta & vbCrLf & "DECLARE @DENGMN3 AS VARCHAR(100)"
    sConsulta = sConsulta & vbCrLf & "DECLARE @DENGMN4 AS VARCHAR(100)"
    sConsulta = sConsulta & vbCrLf & "DECLARE @PROCECAMBIO AS FLOAT"
    sConsulta = sConsulta & vbCrLf & "DECLARE @ADJDIR AS BIT"
    sConsulta = sConsulta & vbCrLf & "DECLARE @GMN2 AS VARCHAR (50)"
    sConsulta = sConsulta & vbCrLf & "DECLARE @GMN3 AS VARCHAR (50)"
    sConsulta = sConsulta & vbCrLf & "DECLARE @GMN4 AS VARCHAR (50)"
    sConsulta = sConsulta & vbCrLf & "SET CONCAT_NULL_YIELDS_NULL OFF"
    sConsulta = sConsulta & vbCrLf & "DECLARE Cur_PROCE CURSOR FOR SELECT GMN1.DEN,PROCE.GMN2, PROCE.GMN3, PROCE.GMN4"
    sConsulta = sConsulta & vbCrLf & ",GMN2.DEN, GMN3.DEN, GMN4.DEN, PROCE.CAMBIO,PROCE.ADJDIR"
    sConsulta = sConsulta & vbCrLf & "From Proce, gmn1, GMN2, GMN3, GMN4"
    sConsulta = sConsulta & vbCrLf & "WHERE PROCE.ANYO=@ANYO AND PROCE.GMN1=@GMN1 AND PROCE.COD=@PROCE"
    sConsulta = sConsulta & vbCrLf & "AND GMN1.COD=PROCE.GMN1"
    sConsulta = sConsulta & vbCrLf & "AND GMN2.GMN1=PROCE.GMN1 AND GMN2.COD=PROCE.GMN2"
    sConsulta = sConsulta & vbCrLf & "AND GMN3.GMN1=PROCE.GMN1 AND GMN3.GMN2=PROCE.GMN2 AND GMN3.COD=PROCE.GMN3"
    sConsulta = sConsulta & vbCrLf & "AND GMN4.GMN1=PROCE.GMN1 AND GMN4.GMN2=PROCE.GMN2 AND GMN4.GMN3=PROCE.GMN3 AND GMN4.COD=PROCE.GMN4"
    sConsulta = sConsulta & vbCrLf & "OPEN Cur_PROCE"
    sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM cur_PROCE INTO @DENGMN1,@GMN2,@GMN3,@GMN4,@DENGMN2,@DENGMN3,@DENGMN4,@PROCECAMBIO,@ADJDIR"
    sConsulta = sConsulta & vbCrLf & "IF @@FETCH_STATUS<>0 GOTO ERR_"
    sConsulta = sConsulta & vbCrLf & ""
    sConsulta = sConsulta & vbCrLf & ""
    sConsulta = sConsulta & vbCrLf & "DECLARE Cur_ITEM Cursor FOR SELECT DISTINCT ITEM_ADJ.ITEM FROM ITEM_ADJ WHERE ITEM_ADJ.ANYO=@ANYO"
    sConsulta = sConsulta & vbCrLf & "AND ITEM_ADJ.GMN1=@GMN1 AND ITEM_ADJ.PROCE=@PROCE AND ITEM_ADJ.PORCEN <> 0"
    sConsulta = sConsulta & vbCrLf & "OPEN Cur_ITEM"
    sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM Cur_ITEM INTO @ITEM"
    sConsulta = sConsulta & vbCrLf & "WHILE @@FETCH_STATUS=0"
    sConsulta = sConsulta & vbCrLf & "BEGIN"
    sConsulta = sConsulta & vbCrLf & "/*Almacenamos los valores en la tabla AR_ITEM*/"
    sConsulta = sConsulta & vbCrLf & "    "
    sConsulta = sConsulta & vbCrLf & ""
    sConsulta = sConsulta & vbCrLf & ""
    sConsulta = sConsulta & vbCrLf & "IF (@NEM = 1 )"
    sConsulta = sConsulta & vbCrLf & "SET @NEM=1"
    sConsulta = sConsulta & vbCrLf & "Else"
    sConsulta = sConsulta & vbCrLf & "IF (@NEM = 2)"
    sConsulta = sConsulta & vbCrLf & "SET @NEM=2"
    sConsulta = sConsulta & vbCrLf & "Else"
    sConsulta = sConsulta & vbCrLf & "IF (@NEM=3)"
    sConsulta = sConsulta & vbCrLf & "SET @NEM=3"
    sConsulta = sConsulta & vbCrLf & "Else"
    sConsulta = sConsulta & vbCrLf & "IF (@NEM = 4)"
    sConsulta = sConsulta & vbCrLf & ""
    sConsulta = sConsulta & vbCrLf & "/*Antes de insertar si existe el registro se borra*/"
    sConsulta = sConsulta & vbCrLf & "DELETE FROM AR_ITEM WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM"
    sConsulta = sConsulta & vbCrLf & ""
    sConsulta = sConsulta & vbCrLf & "/*Inserta en AR_ITEM*/"
    sConsulta = sConsulta & vbCrLf & "INSERT INTO  AR_ITEM ( ANYO,GMN1,PROCE,ITEM,DENGMN1,ADJDIR,GMN2,GMN3,GMN4,DENGMN2,DENGMN3,DENGMN4"
    sConsulta = sConsulta & vbCrLf & ",ART,DESCR,CANT,UNI,DENUNI,DEST,DENDEST,PAG,DENPAG,FECINI,FECFIN,PRES,PREC )"
    sConsulta = sConsulta & vbCrLf & "            "
    sConsulta = sConsulta & vbCrLf & "SELECT  @ANYO , @GMN1, @PROCE , @ITEM, @DENGMN1,@ADJDIR, @GMN2, @GMN3, @GMN4"
    sConsulta = sConsulta & vbCrLf & ",@DENGMN2, @DENGMN3, @DENGMN4, ITEM.ART,ITEM.DESCR + ART4.DEN,ITEM.CANT"
    sConsulta = sConsulta & vbCrLf & ",ITEM.UNI,UNI.DEN,ITEM.DEST,DEST.DEN,ITEM.PAG,PAG.DEN,ITEM.FECINI,ITEM.FECFIN"
    sConsulta = sConsulta & vbCrLf & ",(SELECT SUM((ITEM_ADJ.PORCEN/100)*ITEM.CANT*ITEM.PREC/@PROCECAMBIO)"
    sConsulta = sConsulta & vbCrLf & "AS PRES FROM ITEM_ADJ INNER JOIN ITEM"
    sConsulta = sConsulta & vbCrLf & "ON ITEM_ADJ.ANYO=ITEM.ANYO AND ITEM_ADJ.GMN1=ITEM.GMN1 AND ITEM_ADJ.PROCE=ITEM.PROCE AND ITEM_ADJ.ITEM=ITEM.ID AND ITEM_ADJ.ITEM=@ITEM"
    sConsulta = sConsulta & vbCrLf & "AND ITEM_ADJ.ANYO=@ANYO AND ITEM_ADJ.GMN1=@GMN1 AND ITEM_ADJ.PROCE=@PROCE AND ITEM_ADJ.PORCEN <> 0 )"
    sConsulta = sConsulta & vbCrLf & ""
    sConsulta = sConsulta & vbCrLf & ",(SELECT SUM((ITEM_ADJ.PORCEN/100)*ITEM.CANT*ITEM_OFE.PRECIO/(PROCE_OFE.CAMBIO*@PROCECAMBIO))"
    sConsulta = sConsulta & vbCrLf & "AS PREC FROM ITEM_ADJ INNER JOIN ITEM"
    sConsulta = sConsulta & vbCrLf & "ON ITEM_ADJ.ANYO=ITEM.ANYO AND ITEM_ADJ.GMN1=ITEM.GMN1 AND ITEM_ADJ.PROCE=ITEM.PROCE AND ITEM_ADJ.ITEM=ITEM.ID AND ITEM_ADJ.ITEM=@ITEM"
    sConsulta = sConsulta & vbCrLf & "AND ITEM_ADJ.ANYO=@ANYO AND ITEM_ADJ.GMN1=@GMN1 AND ITEM_ADJ.PROCE=@PROCE AND ITEM_ADJ.PORCEN <> 0"
    sConsulta = sConsulta & vbCrLf & "INNER JOIN ITEM_OFE"
    sConsulta = sConsulta & vbCrLf & "ON ITEM_ADJ.ANYO=ITEM_OFE.ANYO AND ITEM_ADJ.GMN1=ITEM_OFE.GMN1 AND ITEM_ADJ.PROCE=ITEM_OFE.PROCE"
    sConsulta = sConsulta & vbCrLf & "AND ITEM_ADJ.OFE=ITEM_OFE.NUM AND ITEM_ADJ.PROVE=ITEM_OFE.PROVE"
    sConsulta = sConsulta & vbCrLf & "AND ITEM_ADJ.ITEM=ITEM_OFE.ITEM"
    sConsulta = sConsulta & vbCrLf & "INNER JOIN PROCE_OFE"
    sConsulta = sConsulta & vbCrLf & "ON ITEM_OFE.ANYO=PROCE_OFE.ANYO AND ITEM_OFE.GMN1=PROCE_OFE.GMN1 AND ITEM_OFE.PROCE=PROCE_OFE.PROCE AND ITEM_OFE.NUM=PROCE_OFE.OFE"
    sConsulta = sConsulta & vbCrLf & "AND ITEM_OFE.PROVE=PROCE_OFE.PROVE)"
    sConsulta = sConsulta & vbCrLf & "From Item"
    sConsulta = sConsulta & vbCrLf & "LEFT JOIN ART4 ON ITEM.ART=ART4.COD AND ITEM.GMN1=ART4.GMN1 AND ITEM.GMN2=ART4.GMN2 AND ITEM.GMN3=ART4.GMN3 AND ITEM.GMN4=ART4.GMN4"
    sConsulta = sConsulta & vbCrLf & "INNER JOIN UNI ON ITEM.UNI=UNI.COD"
    sConsulta = sConsulta & vbCrLf & "INNER JOIN DEST ON ITEM.DEST=DEST.COD"
    sConsulta = sConsulta & vbCrLf & "INNER JOIN PAG ON ITEM.PAG=PAG.COD"
    sConsulta = sConsulta & vbCrLf & "WHERE ITEM.ANYO=@ANYO AND ITEM.GMN1=@GMN1 AND ITEM.PROCE=@PROCE AND ITEM.ID=@ITEM"
    sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM Cur_ITEM INTO @ITEM"
    sConsulta = sConsulta & vbCrLf & "End"
    sConsulta = sConsulta & vbCrLf & "ERR_:"
    sConsulta = sConsulta & vbCrLf & "Close Cur_PROCE"
    sConsulta = sConsulta & vbCrLf & "DEALLOCATE Cur_PROCE"
    sConsulta = sConsulta & vbCrLf & "Close Cur_ITEM"
    sConsulta = sConsulta & vbCrLf & "DEALLOCATE Cur_ITEM"
    sConsulta = sConsulta & vbCrLf & "SET CONCAT_NULL_YIELDS_NULL ON"
    sConsulta = sConsulta & vbCrLf & "SELECT @RES=@@ERROR"
    sConsulta = sConsulta & vbCrLf & "Return"
    sConsulta = sConsulta & vbCrLf & "End"

    ModificarSP_AR_ITEM_2_8_5_0 = sConsulta
End Function

Private Function ModificarSP_AR_ITEM_MES_2_8_5_0() As String
    Dim sConsulta As String
    
    sConsulta = "CREATE   PROCEDURE  SP_AR_ITEM_MES (@NEM TINYINT,@ANYO SMALLINT,@GMN1 VARCHAR(50), @PROCE INT,@RES INT OUTPUT)  AS"
    sConsulta = sConsulta & vbCrLf & "BEGIN"
    sConsulta = sConsulta & vbCrLf & "DECLARE @FECINI AS DATETIME"
    sConsulta = sConsulta & vbCrLf & "DECLARE @FECFIN AS DATETIME"
    sConsulta = sConsulta & vbCrLf & "DECLARE @PRECADJ AS FLOAT"
    sConsulta = sConsulta & vbCrLf & "DECLARE @CANTADJ AS FLOAT"
    sConsulta = sConsulta & vbCrLf & "DECLARE @PRECAPE AS FLOAT"
    sConsulta = sConsulta & vbCrLf & "DECLARE @DENGMN1 AS VARCHAR(100)"
    sConsulta = sConsulta & vbCrLf & "DECLARE @DENGMN2 AS VARCHAR(100)"
    sConsulta = sConsulta & vbCrLf & "DECLARE @DENGMN3 AS VARCHAR(100)"
    sConsulta = sConsulta & vbCrLf & "DECLARE @DENGMN4 AS VARCHAR(100)"
    sConsulta = sConsulta & vbCrLf & "DECLARE @PROCECAMBIO AS FLOAT"
    sConsulta = sConsulta & vbCrLf & "DECLARE @ADJDIR AS BIT"
    sConsulta = sConsulta & vbCrLf & "DECLARE @GMN2 AS VARCHAR (50)"
    sConsulta = sConsulta & vbCrLf & "DECLARE @GMN3 AS VARCHAR (50)"
    sConsulta = sConsulta & vbCrLf & "DECLARE @GMN4 AS VARCHAR (50)"
    sConsulta = sConsulta & vbCrLf & "DECLARE @PORCEN AS FLOAT"
    sConsulta = sConsulta & vbCrLf & "DECLARE @MESINI AS INTEGER"
    sConsulta = sConsulta & vbCrLf & "DECLARE @TOTALDIAS AS FLOAT"
    sConsulta = sConsulta & vbCrLf & "DECLARE @DIAS AS FLOAT"
    sConsulta = sConsulta & vbCrLf & "DECLARE @PAG AS VARCHAR(50)"
    sConsulta = sConsulta & vbCrLf & "DECLARE @DENPAG AS VARCHAR(100)"
    sConsulta = sConsulta & vbCrLf & "DECLARE @UNI AS VARCHAR(50)"
    sConsulta = sConsulta & vbCrLf & "DECLARE @DENUNI AS VARCHAR(100)"
    sConsulta = sConsulta & vbCrLf & "DECLARE @DEST AS VARCHAR(50)"
    sConsulta = sConsulta & vbCrLf & "DECLARE @DENDEST AS VARCHAR(100)"
    sConsulta = sConsulta & vbCrLf & "DECLARE @ART AS VARCHAR(50)"
    sConsulta = sConsulta & vbCrLf & "DECLARE @DESCR AS VARCHAR(100)"
    sConsulta = sConsulta & vbCrLf & "DECLARE @CANT AS FLOAT"
    sConsulta = sConsulta & vbCrLf & "DECLARE @FECHA AS DATETIME"
    sConsulta = sConsulta & vbCrLf & "DECLARE @ITEM AS INTEGER"
    sConsulta = sConsulta & vbCrLf & "DECLARE @PROVE AS VARCHAR(50)"
    sConsulta = sConsulta & vbCrLf & "DECLARE @FEC AS DATETIME"
    sConsulta = sConsulta & vbCrLf & "DECLARE @FECFINAL AS DATETIME"
    sConsulta = sConsulta & vbCrLf & "DECLARE @ANYOINI AS INTEGER"
    sConsulta = sConsulta & vbCrLf & "DECLARE @ANYOFIN AS INTEGER"
    sConsulta = sConsulta & vbCrLf & "DECLARE @ANYOACTUAL AS INTEGER"
    sConsulta = sConsulta & vbCrLf & ""
    sConsulta = sConsulta & vbCrLf & ""
    sConsulta = sConsulta & vbCrLf & ""
    sConsulta = sConsulta & vbCrLf & "DECLARE Cur_PROCE CURSOR FORWARD_ONLY STATIC READ_ONLY FOR"
    sConsulta = sConsulta & vbCrLf & ""
    sConsulta = sConsulta & vbCrLf & "  "
    sConsulta = sConsulta & vbCrLf & "SELECT PROCE.GMN2, PROCE.GMN3, PROCE.GMN4,"
    sConsulta = sConsulta & vbCrLf & "Proce.CAMBIO , Proce.ADJDIR"
    sConsulta = sConsulta & vbCrLf & "From Proce"
    sConsulta = sConsulta & vbCrLf & "WHERE PROCE.ANYO=@ANYO AND PROCE.GMN1=@GMN1 AND PROCE.COD=@PROCE"
    sConsulta = sConsulta & vbCrLf & ""
    sConsulta = sConsulta & vbCrLf & "OPEN Cur_PROCE"
    sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM cur_PROCE INTO @GMN2,@GMN3,@GMN4,@PROCECAMBIO,@ADJDIR"
    sConsulta = sConsulta & vbCrLf & "IF @@FETCH_STATUS<>0 GOTO ERR_"
    sConsulta = sConsulta & vbCrLf & ""
    sConsulta = sConsulta & vbCrLf & "SET @DENGMN1=(SELECT DEN FROM GMN1 WHERE COD=@GMN1)"
    sConsulta = sConsulta & vbCrLf & "SET @DENGMN2=(SELECT DEN FROM GMN2 WHERE GMN1=@GMN1 AND COD=@GMN2)"
    sConsulta = sConsulta & vbCrLf & "SET @DENGMN3=(SELECT DEN FROM GMN3 WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND COD=@GMN3)"
    sConsulta = sConsulta & vbCrLf & "SET @DENGMN4=(SELECT DEN FROM GMN4 WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND COD=@GMN4)"
    sConsulta = sConsulta & vbCrLf & ""
    sConsulta = sConsulta & vbCrLf & "SET CONCAT_NULL_YIELDS_NULL OFF"
    sConsulta = sConsulta & vbCrLf & ""
    sConsulta = sConsulta & vbCrLf & "IF (@NEM = 1 )"
    sConsulta = sConsulta & vbCrLf & "SET @NEM=1"
    sConsulta = sConsulta & vbCrLf & "Else"
    sConsulta = sConsulta & vbCrLf & "IF (@NEM = 2)"
    sConsulta = sConsulta & vbCrLf & "SET @NEM=2"
    sConsulta = sConsulta & vbCrLf & "Else"
    sConsulta = sConsulta & vbCrLf & "IF (@NEM=3)"
    sConsulta = sConsulta & vbCrLf & "SET @NEM=3"
    sConsulta = sConsulta & vbCrLf & "Else"
    sConsulta = sConsulta & vbCrLf & "IF (@NEM = 4)"
    sConsulta = sConsulta & vbCrLf & " "
    sConsulta = sConsulta & vbCrLf & "DECLARE Cur_ITEM_ADJ Cursor FORWARD_ONLY STATIC READ_ONLY FOR"
    sConsulta = sConsulta & vbCrLf & "    "
    sConsulta = sConsulta & vbCrLf & "    "
    sConsulta = sConsulta & vbCrLf & "SELECT ITEM_ADJ.ITEM,ITEM_ADJ.PROVE,ITEM.FECINI,ITEM.FECFIN,(ITEM_OFE.PRECIO/(PROCE_OFE.CAMBIO*@PROCECAMBIO)) AS PRECADJ,(ITEM.CANT*(ITEM_ADJ.PORCEN/100)) AS CANTADJ,(ITEM.PREC/@PROCECAMBIO) AS PRECAPE"
    sConsulta = sConsulta & vbCrLf & ",ITEM.PAG,PAG.DEN,ITEM.UNI,UNI.DEN,ITEM.DEST,DEST.DEN"
    sConsulta = sConsulta & vbCrLf & ",ITEM.ART,ITEM.DESCR + ART4.DEN,ITEM.CANT"
    sConsulta = sConsulta & vbCrLf & "From ITEM_ADJ"
    sConsulta = sConsulta & vbCrLf & "INNER JOIN ITEM ON ITEM_ADJ.ANYO=ITEM.ANYO AND ITEM_ADJ.GMN1=ITEM.GMN1 AND ITEM_ADJ.PROCE=ITEM.PROCE  AND ITEM_ADJ.ITEM=ITEM.ID"
    sConsulta = sConsulta & vbCrLf & "INNER JOIN PROCE_OFE ON ITEM_ADJ.ANYO=PROCE_OFE.ANYO AND ITEM_ADJ.GMN1=PROCE_OFE.GMN1 AND ITEM_ADJ.PROCE=PROCE_OFE.PROCE AND ITEM_ADJ.OFE=PROCE_OFE.OFE AND ITEM_ADJ.PROVE=PROCE_OFE.PROVE"
    sConsulta = sConsulta & vbCrLf & "INNER JOIN ITEM_OFE ON ITEM_ADJ.ANYO=ITEM_OFE.ANYO AND ITEM_ADJ.GMN1=ITEM_OFE.GMN1 AND ITEM_ADJ.PROCE=ITEM_OFE.PROCE AND ITEM_ADJ.PROVE=ITEM_OFE.PROVE AND ITEM_ADJ.ITEM=ITEM_OFE.ITEM AND ITEM_ADJ.OFE=ITEM_OFE.NUM"
    sConsulta = sConsulta & vbCrLf & "INNER JOIN UNI ON ITEM.UNI=UNI.COD"
    sConsulta = sConsulta & vbCrLf & "INNER JOIN DEST ON ITEM.DEST=DEST.COD"
    sConsulta = sConsulta & vbCrLf & "INNER JOIN PAG ON ITEM.PAG=PAG.COD"
    sConsulta = sConsulta & vbCrLf & "LEFT JOIN ART4 ON ITEM.ART=ART4.COD AND ITEM.GMN1=ART4.GMN1 AND ITEM.GMN2=ART4.GMN2 AND ITEM.GMN3=ART4.GMN3 AND ITEM.GMN4=ART4.GMN4"
    sConsulta = sConsulta & vbCrLf & "WHERE ITEM_ADJ.ANYO=@ANYO AND ITEM_ADJ.GMN1=@GMN1 AND ITEM_ADJ.PROCE=@PROCE AND  ITEM_ADJ.PORCEN <> 0"
    sConsulta = sConsulta & vbCrLf & ""
    sConsulta = sConsulta & vbCrLf & "                "
    sConsulta = sConsulta & vbCrLf & " "
    sConsulta = sConsulta & vbCrLf & "OPEN Cur_ITEM_ADJ"
    sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM Cur_ITEM_ADJ INTO @ITEM,@PROVE,@FECINI,@FECFIN,@PRECADJ,@CANTADJ,@PRECAPE"
    sConsulta = sConsulta & vbCrLf & ",@PAG,@DENPAG,@UNI,@DENUNI,@DEST,@DENDEST"
    sConsulta = sConsulta & vbCrLf & ",@ART,@DESCR,@CANT"
    sConsulta = sConsulta & vbCrLf & "WHILE @@FETCH_STATUS=0"
    sConsulta = sConsulta & vbCrLf & "BEGIN"
    sConsulta = sConsulta & vbCrLf & " "
    sConsulta = sConsulta & vbCrLf & "/* Tenemos que calcular el porcentaje correspondiente a cada mes del aNYo.*/"
    sConsulta = sConsulta & vbCrLf & " "
    sConsulta = sConsulta & vbCrLf & "SET @ANYOINI = YEAR(@FECINI)"
    sConsulta = sConsulta & vbCrLf & "SET @ANYOFIN=YEAR(@FECFIN)"
    sConsulta = sConsulta & vbCrLf & "SET @ANYOACTUAL = @ANYOINI"
    sConsulta = sConsulta & vbCrLf & "SET @FEC = @FECINI"
    sConsulta = sConsulta & vbCrLf & "SET @FECFINAL = @FECFIN"
    sConsulta = sConsulta & vbCrLf & "SET @TOTALDIAS = datediff(day,@FECINI, @FECFIN) + 1"
    sConsulta = sConsulta & vbCrLf & ""
    sConsulta = sConsulta & vbCrLf & "WHILE @ANYOACTUAL < @ANYOFIN"
    sConsulta = sConsulta & vbCrLf & "BEGIN"
    sConsulta = sConsulta & vbCrLf & "SET @MESINI = MONTH(@FEC)"
    sConsulta = sConsulta & vbCrLf & "SET @FECHA = @FEC"
    sConsulta = sConsulta & vbCrLf & "SET @FECFINAL = CAST( '12/31/' + STR(@ANYOACTUAL)   AS DATETIME)"
    sConsulta = sConsulta & vbCrLf & ""
    sConsulta = sConsulta & vbCrLf & "/* Inicio de parte para un aNYo*************************************************************************/"
    sConsulta = sConsulta & vbCrLf & ""
    sConsulta = sConsulta & vbCrLf & "WHILE @MESINI < MONTH(@FECFINAL)"
    sConsulta = sConsulta & vbCrLf & "BEGIN"
    sConsulta = sConsulta & vbCrLf & "/* Calculamos la diferencia en d�as entre el mes inicial y el siguiente mes*/"
    sConsulta = sConsulta & vbCrLf & "  "
    sConsulta = sConsulta & vbCrLf & "SET @DIAS = datediff(day,@FEC,cast( str (month(@FEC) +1) + '/1/' + str(year(@FEC)) as datetime))"
    sConsulta = sConsulta & vbCrLf & "SET @PORCEN = @DIAS / @TOTALDIAS"
    sConsulta = sConsulta & vbCrLf & "SET @FEC = cast(str( month(@FEC) +1)  + '/1/' + str(year(@FEC)) as datetime)"
    sConsulta = sConsulta & vbCrLf & "  "
    sConsulta = sConsulta & vbCrLf & "/*Miramos si hab�a alg�n resultado para ese mes*/"
    sConsulta = sConsulta & vbCrLf & "IF (SELECT COUNT(*) FROM AR_ITEM_MES WHERE"
    sConsulta = sConsulta & vbCrLf & "ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM AND ANYOADJ=YEAR(@FEC) AND MES=@MESINI)=0"
    sConsulta = sConsulta & vbCrLf & "BEGIN"
    sConsulta = sConsulta & vbCrLf & ""
    sConsulta = sConsulta & vbCrLf & "/*Antes de insertar el registro si existe lo borra*/    "
    sConsulta = sConsulta & vbCrLf & "DELETE FROM AR_ITEM_MES WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM AND ANYOADJ=YEAR(@FEC) AND MES=@MESINI"
    sConsulta = sConsulta & vbCrLf & ""
    sConsulta = sConsulta & vbCrLf & "INSERT INTO  AR_ITEM_MES ( ANYO,GMN1,PROCE,ITEM,ANYOADJ,MES,DENGMN1,ADJDIR,GMN2,GMN3,GMN4,DENGMN2,DENGMN3,DENGMN4"
    sConsulta = sConsulta & vbCrLf & ",ART,DESCR,CANT"
    sConsulta = sConsulta & vbCrLf & ",UNI,DENUNI,DEST,DENDEST,PAG,DENPAG,FECINI,FECFIN,PRES,PREC,CANTPERIODO)"
    sConsulta = sConsulta & vbCrLf & " "
    sConsulta = sConsulta & vbCrLf & "VALUES(@ANYO,@GMN1, @PROCE , @ITEM,YEAR(@FEC),@MESINI, @DENGMN1,@ADJDIR, @GMN2, @GMN3, @GMN4 ,@DENGMN2, @DENGMN3, @DENGMN4"
    sConsulta = sConsulta & vbCrLf & ",@ART,@DESCR,@CANT"
    sConsulta = sConsulta & vbCrLf & ",@UNI,@DENUNI,@DEST,@DENDEST,@PAG,@DENPAG,@FECINI,@FECFIN,@PRECAPE*@PORCEN*@CANTADJ,@PRECADJ*@PORCEN*@CANTADJ,@CANT*@PORCEN)"
    sConsulta = sConsulta & vbCrLf & "    "
    sConsulta = sConsulta & vbCrLf & "End"
    sConsulta = sConsulta & vbCrLf & "Else"
    sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES SET PRES=PRES+@PRECAPE*@PORCEN*@CANTADJ, PREC=PREC+@PRECADJ*@PORCEN*@CANTADJ"
    sConsulta = sConsulta & vbCrLf & "WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND MES=@MESINI AND ANYOADJ=YEAR(@FEC)  AND ITEM=@ITEM"
    sConsulta = sConsulta & vbCrLf & "  "
    sConsulta = sConsulta & vbCrLf & "SET @MESINI=@MESINI + 1"
    sConsulta = sConsulta & vbCrLf & "  "
    sConsulta = sConsulta & vbCrLf & "End"
    sConsulta = sConsulta & vbCrLf & "/*Fin del while  mesini < mesfin */"
    sConsulta = sConsulta & vbCrLf & "/*Si el m�s es el mismo*/"
    sConsulta = sConsulta & vbCrLf & "IF @MESINI = MONTH(@FECFINAL)"
    sConsulta = sConsulta & vbCrLf & "BEGIN"
    sConsulta = sConsulta & vbCrLf & "SET @DIAS = datediff(day,@FEC,@FECFINAL) + 1"
    sConsulta = sConsulta & vbCrLf & "SET @PORCEN = @DIAS/@TOTALDIAS"
    sConsulta = sConsulta & vbCrLf & "/*Miramos si hab�a alg�n resultado para ese mes*/"
    sConsulta = sConsulta & vbCrLf & "IF (SELECT COUNT(*) FROM AR_ITEM_MES WHERE"
    sConsulta = sConsulta & vbCrLf & "ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM AND ANYOADJ=YEAR(@FEC) AND MES=@MESINI)=0"
    sConsulta = sConsulta & vbCrLf & "BEGIN"
    sConsulta = sConsulta & vbCrLf & ""
    sConsulta = sConsulta & vbCrLf & "/*Antes de insertar el registro si existe lo borra*/    "
    sConsulta = sConsulta & vbCrLf & "DELETE FROM AR_ITEM_MES WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM AND ANYOADJ=YEAR(@FEC) AND MES=@MESINI"
    sConsulta = sConsulta & vbCrLf & "    "
    sConsulta = sConsulta & vbCrLf & "INSERT INTO  AR_ITEM_MES ( ANYO,GMN1,PROCE,ITEM,ANYOADJ,MES,DENGMN1,ADJDIR,GMN2,GMN3,GMN4,DENGMN2,DENGMN3,DENGMN4"
    sConsulta = sConsulta & vbCrLf & ",ART,DESCR,CANT"
    sConsulta = sConsulta & vbCrLf & ",UNI,DENUNI,DEST,DENDEST,PAG,DENPAG,FECINI,FECFIN,PRES,PREC,CANTPERIODO)"
    sConsulta = sConsulta & vbCrLf & "VALUES(@ANYO,@GMN1, @PROCE , @ITEM,YEAR(@FEC),@MESINI, @DENGMN1,@ADJDIR, @GMN2, @GMN3, @GMN4 ,@DENGMN2, @DENGMN3, @DENGMN4"
    sConsulta = sConsulta & vbCrLf & ",@ART,@DESCR,@CANT"
    sConsulta = sConsulta & vbCrLf & ",@UNI,@DENUNI,@DEST,@DENDEST,@PAG,@DENPAG,@FECINI,@FECFIN,@PRECAPE*@PORCEN*@CANTADJ,@PRECADJ*@PORCEN*@CANTADJ,@CANT*@PORCEN)"
    sConsulta = sConsulta & vbCrLf & "    "
    sConsulta = sConsulta & vbCrLf & "End"
    sConsulta = sConsulta & vbCrLf & "Else"
    sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES SET PRES=PRES+@PRECAPE*@PORCEN*@CANTADJ, PREC=PREC+@PRECADJ*@PORCEN*@CANTADJ"
    sConsulta = sConsulta & vbCrLf & "WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND MES=@MESINI AND ANYOADJ=YEAR(@FEC)  AND ITEM=@ITEM"
    sConsulta = sConsulta & vbCrLf & "End"
    sConsulta = sConsulta & vbCrLf & "        "
    sConsulta = sConsulta & vbCrLf & "        "
    sConsulta = sConsulta & vbCrLf & "/* Fin de parte para un aNYo **********************************************************************************************/"
    sConsulta = sConsulta & vbCrLf & "        "
    sConsulta = sConsulta & vbCrLf & "        "
    sConsulta = sConsulta & vbCrLf & "SET @ANYOACTUAL = @ANYOACTUAL + 1"
    sConsulta = sConsulta & vbCrLf & "SET @FEC = CAST( '1/1/' + STR(@ANYOACTUAL) AS DATETIME)"
    sConsulta = sConsulta & vbCrLf & "SET @FECFINAL = CAST( '12/31/' + STR(@ANYOACTUAL) AS DATETIME)"
    sConsulta = sConsulta & vbCrLf & "End"
    sConsulta = sConsulta & vbCrLf & ""
    sConsulta = sConsulta & vbCrLf & "    "
    sConsulta = sConsulta & vbCrLf & "IF @ANYOINI=@ANYOACTUAL"
    sConsulta = sConsulta & vbCrLf & "BEGIN"
    sConsulta = sConsulta & vbCrLf & "SET @MESINI = MONTH(@FECINI)"
    sConsulta = sConsulta & vbCrLf & "SET @FECHA = @FECINI"
    sConsulta = sConsulta & vbCrLf & "End"
    sConsulta = sConsulta & vbCrLf & "Else"
    sConsulta = sConsulta & vbCrLf & ""
    sConsulta = sConsulta & vbCrLf & "BEGIN"
    sConsulta = sConsulta & vbCrLf & "SET @MESINI = 1"
    sConsulta = sConsulta & vbCrLf & "SET @FECHA = cast('1/1/' + str(@ANYOACTUAL)  as datetime)"
    sConsulta = sConsulta & vbCrLf & "SET @FECINI = cast('1/1/' + str(@ANYOACTUAL)  as datetime)"
    sConsulta = sConsulta & vbCrLf & "        "
    sConsulta = sConsulta & vbCrLf & "End"
    sConsulta = sConsulta & vbCrLf & "    "
    sConsulta = sConsulta & vbCrLf & "WHILE @MESINI < MONTH(@FECFIN)"
    sConsulta = sConsulta & vbCrLf & "BEGIN"
    sConsulta = sConsulta & vbCrLf & "/* Calculamos la diferencia en d�as entre el mes inicial y el siguiente mes*/"
    sConsulta = sConsulta & vbCrLf & "  "
    sConsulta = sConsulta & vbCrLf & "SET @DIAS = datediff(day,@FECHA,cast(str((month(@FECHA) +1)) + '/1/' + str(year(@FECINI)) as datetime))"
    sConsulta = sConsulta & vbCrLf & "SET @PORCEN = @DIAS / @TOTALDIAS"
    sConsulta = sConsulta & vbCrLf & "SET @FECHA = cast(str((month(@FECHA) +1)) + '/1/' + str(year(@FECHA)) as datetime)"
    sConsulta = sConsulta & vbCrLf & "  "
    sConsulta = sConsulta & vbCrLf & "/*Miramos si hab�a alg�n resultado para ese mes*/"
    sConsulta = sConsulta & vbCrLf & "IF (SELECT COUNT(*) FROM AR_ITEM_MES WHERE"
    sConsulta = sConsulta & vbCrLf & "ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM AND ANYOADJ=YEAR(@FECINI) AND MES=@MESINI)=0"
    sConsulta = sConsulta & vbCrLf & "BEGIN"
    sConsulta = sConsulta & vbCrLf & ""
    sConsulta = sConsulta & vbCrLf & "/*Antes de insertar el registro si existe lo borra*/    "
    sConsulta = sConsulta & vbCrLf & "DELETE FROM AR_ITEM_MES WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM AND ANYOADJ=YEAR(@FECINI) AND MES=@MESINI"
    sConsulta = sConsulta & vbCrLf & "    "
    sConsulta = sConsulta & vbCrLf & "INSERT INTO  AR_ITEM_MES ( ANYO,GMN1,PROCE,ITEM,ANYOADJ,MES,DENGMN1,ADJDIR,GMN2,GMN3,GMN4,DENGMN2,DENGMN3,DENGMN4"
    sConsulta = sConsulta & vbCrLf & ",ART,DESCR,CANT"
    sConsulta = sConsulta & vbCrLf & ",UNI,DENUNI,DEST,DENDEST,PAG,DENPAG,FECINI,FECFIN,PRES,PREC,CANTPERIODO)"
    sConsulta = sConsulta & vbCrLf & " "
    sConsulta = sConsulta & vbCrLf & "VALUES(@ANYO,@GMN1, @PROCE , @ITEM,YEAR(@FECINI),@MESINI, @DENGMN1,@ADJDIR, @GMN2, @GMN3, @GMN4 ,@DENGMN2, @DENGMN3, @DENGMN4"
    sConsulta = sConsulta & vbCrLf & ",@ART,@DESCR,@CANT"
    sConsulta = sConsulta & vbCrLf & ",@UNI,@DENUNI,@DEST,@DENDEST,@PAG,@DENPAG,@FECINI,@FECFIN,@PRECAPE*@PORCEN*@CANTADJ,@PRECADJ*@PORCEN*@CANTADJ,@CANT*@PORCEN)"
    sConsulta = sConsulta & vbCrLf & "    "
    sConsulta = sConsulta & vbCrLf & "End"
    sConsulta = sConsulta & vbCrLf & "Else"
    sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES SET PRES=PRES+@PRECAPE*@PORCEN*@CANTADJ, PREC=PREC+@PRECADJ*@PORCEN*@CANTADJ"
    sConsulta = sConsulta & vbCrLf & "WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND MES=@MESINI AND ANYOADJ=YEAR(@FECINI)  AND ITEM=@ITEM"
    sConsulta = sConsulta & vbCrLf & "  "
    sConsulta = sConsulta & vbCrLf & "SET @MESINI=@MESINI + 1"
    sConsulta = sConsulta & vbCrLf & "  "
    sConsulta = sConsulta & vbCrLf & "  "
    sConsulta = sConsulta & vbCrLf & "End"
    sConsulta = sConsulta & vbCrLf & ""
    sConsulta = sConsulta & vbCrLf & "/*Si el m�s es el mismo*/"
    sConsulta = sConsulta & vbCrLf & "IF @MESINI = MONTH(@FECFIN)"
    sConsulta = sConsulta & vbCrLf & "BEGIN"
    sConsulta = sConsulta & vbCrLf & "SET @DIAS = datediff(day,@FECHA,@FECFIN) + 1"
    sConsulta = sConsulta & vbCrLf & "SET @PORCEN = @DIAS/@TOTALDIAS"
    sConsulta = sConsulta & vbCrLf & "/*Miramos si hab�a alg�n resultado para ese mes*/"
    sConsulta = sConsulta & vbCrLf & "IF (SELECT COUNT(*) FROM AR_ITEM_MES WHERE"
    sConsulta = sConsulta & vbCrLf & "ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM AND ANYOADJ=YEAR(@FECINI) AND MES=@MESINI)=0"
    sConsulta = sConsulta & vbCrLf & "BEGIN"
    sConsulta = sConsulta & vbCrLf & ""
    sConsulta = sConsulta & vbCrLf & "/*Antes de insertar el registro si existe lo borra*/    "
    sConsulta = sConsulta & vbCrLf & "DELETE FROM AR_ITEM_MES WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM AND ANYOADJ=YEAR(@FECINI) AND MES=@MESINI"
    sConsulta = sConsulta & vbCrLf & "    "
    sConsulta = sConsulta & vbCrLf & "INSERT INTO  AR_ITEM_MES ( ANYO,GMN1,PROCE,ITEM,ANYOADJ,MES,DENGMN1,ADJDIR,GMN2,GMN3,GMN4,DENGMN2,DENGMN3,DENGMN4"
    sConsulta = sConsulta & vbCrLf & ",ART,DESCR,CANT"
    sConsulta = sConsulta & vbCrLf & ",UNI,DENUNI,DEST,DENDEST,PAG,DENPAG,FECINI,FECFIN,PRES,PREC,CANTPERIODO)"
    sConsulta = sConsulta & vbCrLf & ""
    sConsulta = sConsulta & vbCrLf & "VALUES(@ANYO,@GMN1, @PROCE , @ITEM,YEAR(@FECINI),@MESINI, @DENGMN1,@ADJDIR, @GMN2, @GMN3, @GMN4 ,@DENGMN2, @DENGMN3, @DENGMN4"
    sConsulta = sConsulta & vbCrLf & ",@ART,@DESCR,@CANT"
    sConsulta = sConsulta & vbCrLf & ",@UNI,@DENUNI,@DEST,@DENDEST,@PAG,@DENPAG,@FECINI,@FECFIN,@PRECAPE*@PORCEN*@CANTADJ,@PRECADJ*@PORCEN*@CANTADJ,@CANT*@PORCEN)"
    sConsulta = sConsulta & vbCrLf & "    "
    sConsulta = sConsulta & vbCrLf & "End"
    sConsulta = sConsulta & vbCrLf & "Else"
    sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES SET PRES=PRES+@PRECAPE*@PORCEN*@CANTADJ, PREC=PREC+@PRECADJ*@PORCEN*@CANTADJ"
    sConsulta = sConsulta & vbCrLf & "WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND MES=@MESINI AND ANYOADJ=YEAR(@FECINI)  AND ITEM=@ITEM"
    sConsulta = sConsulta & vbCrLf & "End"
    sConsulta = sConsulta & vbCrLf & " "
    sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM Cur_ITEM_ADJ INTO @ITEM,@PROVE,@FECINI,@FECFIN,@PRECADJ,@CANTADJ,@PRECAPE"
    sConsulta = sConsulta & vbCrLf & ",@PAG,@DENPAG,@UNI,@DENUNI,@DEST,@DENDEST"
    sConsulta = sConsulta & vbCrLf & ",@ART,@DESCR,@CANT"
    sConsulta = sConsulta & vbCrLf & "End"
    sConsulta = sConsulta & vbCrLf & "ERR_:"
    sConsulta = sConsulta & vbCrLf & "Close Cur_PROCE"
    sConsulta = sConsulta & vbCrLf & "DEALLOCATE Cur_PROCE"
    sConsulta = sConsulta & vbCrLf & "Close Cur_ITEM_ADJ"
    sConsulta = sConsulta & vbCrLf & "DEALLOCATE Cur_ITEM_ADJ"
    sConsulta = sConsulta & vbCrLf & "    "
    sConsulta = sConsulta & vbCrLf & "SET CONCAT_NULL_YIELDS_NULL ON"
    sConsulta = sConsulta & vbCrLf & "SET @RES=@@ERROR"
    sConsulta = sConsulta & vbCrLf & "Return"
    sConsulta = sConsulta & vbCrLf & "End"

    ModificarSP_AR_ITEM_MES_2_8_5_0 = sConsulta
End Function

Private Function ModificarSP_AR_ITEM_PROVE_2_8_5_0() As String
Dim sConsulta As String
    
    sConsulta = "CREATE PROCEDURE SP_AR_ITEM_PROVE (@ANYO SMALLINT,@GMN1 VARCHAR(50), @PROCE INT,@RES INT OUTPUT)  AS"
    sConsulta = sConsulta & vbCrLf & "BEGIN"
    sConsulta = sConsulta & vbCrLf & ""
    sConsulta = sConsulta & vbCrLf & "/*antes de insertar el registro si existe lo borra*/"
    sConsulta = sConsulta & vbCrLf & "DELETE FROM AR_ITEM_PROVE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM IN"
    sConsulta = sConsulta & vbCrLf & "(SELECT "
    sConsulta = sConsulta & vbCrLf & "ITEM_ADJ.ITEM FROM ITEM_ADJ  INNER JOIN ITEM"
    sConsulta = sConsulta & vbCrLf & "ON ITEM_ADJ.ANYO=ITEM.ANYO AND ITEM_ADJ.GMN1=ITEM.GMN1 AND ITEM_ADJ.PROCE=ITEM.PROCE AND ITEM_ADJ.ITEM=ITEM.ID"
    sConsulta = sConsulta & vbCrLf & "AND ITEM_ADJ.ANYO=@ANYO AND ITEM_ADJ.GMN1=@GMN1 AND ITEM_ADJ.PROCE=@PROCE AND ITEM_ADJ.PORCEN > 0"
    sConsulta = sConsulta & vbCrLf & "INNER JOIN ITEM_OFE"
    sConsulta = sConsulta & vbCrLf & "ON ITEM_ADJ.ANYO=ITEM_OFE.ANYO AND ITEM_ADJ.GMN1=ITEM_OFE.GMN1 AND ITEM_ADJ.PROCE=ITEM_OFE.PROCE"
    sConsulta = sConsulta & vbCrLf & "AND ITEM_ADJ.OFE=ITEM_OFE.NUM AND ITEM_ADJ.PROVE=ITEM_OFE.PROVE"
    sConsulta = sConsulta & vbCrLf & "AND ITEM_ADJ.ITEM=ITEM_OFE.ITEM"
    sConsulta = sConsulta & vbCrLf & "INNER JOIN PROCE ON ITEM_OFE.ANYO=PROCE.ANYO AND ITEM_OFE.GMN1=PROCE.GMN1 AND ITEM_OFE.PROCE=PROCE.COD"
    sConsulta = sConsulta & vbCrLf & "INNER JOIN PROCE_OFE ON ITEM_OFE.ANYO=PROCE_OFE.ANYO AND ITEM_OFE.GMN1=PROCE_OFE.GMN1 AND ITEM_OFE.PROCE=PROCE_OFE.PROCE AND ITEM_OFE.NUM=PROCE_OFE.OFE"
    sConsulta = sConsulta & vbCrLf & "AND ITEM_OFE.PROVE=PROCE_OFE.PROVE"
    sConsulta = sConsulta & vbCrLf & "INNER JOIN PROVE ON ITEM_ADJ.PROVE = PROVE.COD"
    sConsulta = sConsulta & vbCrLf & "INNER JOIN PROCE_PROVE ON ITEM_ADJ.PROVE=PROCE_PROVE.PROVE AND ITEM_ADJ.ANYO=PROCE_PROVE.ANYO AND ITEM_ADJ.GMN1=PROCE_PROVE.GMN1 AND ITEM_ADJ.PROCE=PROCE_PROVE.PROCE"
    sConsulta = sConsulta & vbCrLf & "INNER JOIN EQP ON PROCE_PROVE.EQP=EQP.COD"
    sConsulta = sConsulta & vbCrLf & "INNER JOIN COM ON PROCE_PROVE.EQP = COM.EQP AND PROCE_PROVE.COM = COM.COD)"
    sConsulta = sConsulta & vbCrLf & ""
    sConsulta = sConsulta & vbCrLf & "AND PROVE IN (SELECT ITEM_ADJ.PROVE FROM "
    sConsulta = sConsulta & vbCrLf & "ITEM_ADJ  INNER JOIN ITEM"
    sConsulta = sConsulta & vbCrLf & "ON ITEM_ADJ.ANYO=ITEM.ANYO AND ITEM_ADJ.GMN1=ITEM.GMN1 AND ITEM_ADJ.PROCE=ITEM.PROCE AND ITEM_ADJ.ITEM=ITEM.ID"
    sConsulta = sConsulta & vbCrLf & "AND ITEM_ADJ.ANYO=@ANYO AND ITEM_ADJ.GMN1=@GMN1 AND ITEM_ADJ.PROCE=@PROCE AND ITEM_ADJ.PORCEN > 0"
    sConsulta = sConsulta & vbCrLf & "INNER JOIN ITEM_OFE"
    sConsulta = sConsulta & vbCrLf & "ON ITEM_ADJ.ANYO=ITEM_OFE.ANYO AND ITEM_ADJ.GMN1=ITEM_OFE.GMN1 AND ITEM_ADJ.PROCE=ITEM_OFE.PROCE"
    sConsulta = sConsulta & vbCrLf & "AND ITEM_ADJ.OFE=ITEM_OFE.NUM AND ITEM_ADJ.PROVE=ITEM_OFE.PROVE"
    sConsulta = sConsulta & vbCrLf & "AND ITEM_ADJ.ITEM=ITEM_OFE.ITEM"
    sConsulta = sConsulta & vbCrLf & "INNER JOIN PROCE ON ITEM_OFE.ANYO=PROCE.ANYO AND ITEM_OFE.GMN1=PROCE.GMN1 AND ITEM_OFE.PROCE=PROCE.COD"
    sConsulta = sConsulta & vbCrLf & "INNER JOIN PROCE_OFE ON ITEM_OFE.ANYO=PROCE_OFE.ANYO AND ITEM_OFE.GMN1=PROCE_OFE.GMN1 AND ITEM_OFE.PROCE=PROCE_OFE.PROCE AND ITEM_OFE.NUM=PROCE_OFE.OFE"
    sConsulta = sConsulta & vbCrLf & "AND ITEM_OFE.PROVE=PROCE_OFE.PROVE"
    sConsulta = sConsulta & vbCrLf & "INNER JOIN PROVE ON ITEM_ADJ.PROVE = PROVE.COD"
    sConsulta = sConsulta & vbCrLf & "INNER JOIN PROCE_PROVE ON ITEM_ADJ.PROVE=PROCE_PROVE.PROVE AND ITEM_ADJ.ANYO=PROCE_PROVE.ANYO AND ITEM_ADJ.GMN1=PROCE_PROVE.GMN1 AND ITEM_ADJ.PROCE=PROCE_PROVE.PROCE"
    sConsulta = sConsulta & vbCrLf & "INNER JOIN EQP ON PROCE_PROVE.EQP=EQP.COD"
    sConsulta = sConsulta & vbCrLf & "INNER JOIN COM ON PROCE_PROVE.EQP = COM.EQP AND PROCE_PROVE.COM = COM.COD)"
    sConsulta = sConsulta & vbCrLf & ""
    sConsulta = sConsulta & vbCrLf & "INSERT INTO AR_ITEM_PROVE ( ANYO, GMN1, PROCE, ITEM, PROVE, EQP, COM, DENPROVE, DENEQP, APECOM, NOMCOM, PRES, PREC)"
    sConsulta = sConsulta & vbCrLf & "SELECT  @ANYO, @GMN1, @PROCE, ITEM_ADJ.ITEM"
    sConsulta = sConsulta & vbCrLf & ", ITEM_ADJ.PROVE, PROCE_PROVE.EQP, PROCE_PROVE.COM, PROVE.DEN, EQP.DEN, COM.APE, COM.NOM"
    sConsulta = sConsulta & vbCrLf & ", (ITEM_ADJ.PORCEN/100)*ITEM.CANT*ITEM.PREC/PROCE.CAMBIO AS PRES"
    sConsulta = sConsulta & vbCrLf & ",(ITEM_ADJ.PORCEN/100)*ITEM.CANT*ITEM_OFE.PRECIO / (PROCE_OFE.CAMBIO*PROCE.CAMBIO) AS PREC"
    sConsulta = sConsulta & vbCrLf & "From"
    sConsulta = sConsulta & vbCrLf & "ITEM_ADJ  INNER JOIN ITEM"
    sConsulta = sConsulta & vbCrLf & "ON ITEM_ADJ.ANYO=ITEM.ANYO AND ITEM_ADJ.GMN1=ITEM.GMN1 AND ITEM_ADJ.PROCE=ITEM.PROCE AND ITEM_ADJ.ITEM=ITEM.ID"
    sConsulta = sConsulta & vbCrLf & "AND ITEM_ADJ.ANYO=@ANYO AND ITEM_ADJ.GMN1=@GMN1 AND ITEM_ADJ.PROCE=@PROCE AND ITEM_ADJ.PORCEN > 0"
    sConsulta = sConsulta & vbCrLf & "INNER JOIN ITEM_OFE"
    sConsulta = sConsulta & vbCrLf & "ON ITEM_ADJ.ANYO=ITEM_OFE.ANYO AND ITEM_ADJ.GMN1=ITEM_OFE.GMN1 AND ITEM_ADJ.PROCE=ITEM_OFE.PROCE"
    sConsulta = sConsulta & vbCrLf & "AND ITEM_ADJ.OFE=ITEM_OFE.NUM AND ITEM_ADJ.PROVE=ITEM_OFE.PROVE"
    sConsulta = sConsulta & vbCrLf & "AND ITEM_ADJ.ITEM=ITEM_OFE.ITEM"
    sConsulta = sConsulta & vbCrLf & "INNER JOIN PROCE ON ITEM_OFE.ANYO=PROCE.ANYO AND ITEM_OFE.GMN1=PROCE.GMN1 AND ITEM_OFE.PROCE=PROCE.COD"
    sConsulta = sConsulta & vbCrLf & "INNER JOIN PROCE_OFE ON ITEM_OFE.ANYO=PROCE_OFE.ANYO AND ITEM_OFE.GMN1=PROCE_OFE.GMN1 AND ITEM_OFE.PROCE=PROCE_OFE.PROCE AND ITEM_OFE.NUM=PROCE_OFE.OFE"
    sConsulta = sConsulta & vbCrLf & "AND ITEM_OFE.PROVE=PROCE_OFE.PROVE"
    sConsulta = sConsulta & vbCrLf & "INNER JOIN PROVE ON ITEM_ADJ.PROVE = PROVE.COD"
    sConsulta = sConsulta & vbCrLf & "INNER JOIN PROCE_PROVE ON ITEM_ADJ.PROVE=PROCE_PROVE.PROVE AND ITEM_ADJ.ANYO=PROCE_PROVE.ANYO AND ITEM_ADJ.GMN1=PROCE_PROVE.GMN1 AND ITEM_ADJ.PROCE=PROCE_PROVE.PROCE"
    sConsulta = sConsulta & vbCrLf & "INNER JOIN EQP ON PROCE_PROVE.EQP=EQP.COD"
    sConsulta = sConsulta & vbCrLf & "INNER JOIN COM ON PROCE_PROVE.EQP = COM.EQP AND PROCE_PROVE.COM = COM.COD"
    sConsulta = sConsulta & vbCrLf & "SET @RES=@@ERROR"
    sConsulta = sConsulta & vbCrLf & "Return"
    sConsulta = sConsulta & vbCrLf & "End"

    ModificarSP_AR_ITEM_PROVE_2_8_5_0 = sConsulta
End Function

Private Function ModificarSP_AR_ITEM_PRESCON3_2_8_5_0() As String
Dim sConsulta As String
    
     sConsulta = "CREATE PROCEDURE SP_AR_ITEM_PRESCON3 (@ANYO SMALLINT,@GMN1 VARCHAR(50), @PROCE INT,@RES INT OUTPUT)  AS"
     sConsulta = sConsulta & vbCrLf & "BEGIN"
     sConsulta = sConsulta & vbCrLf & "DECLARE @ITEM AS INTEGER"
     sConsulta = sConsulta & vbCrLf & "DECLARE @DENGMN1 AS VARCHAR(100)"
     sConsulta = sConsulta & vbCrLf & "DECLARE @DENPRES1 AS VARCHAR(100)"
     sConsulta = sConsulta & vbCrLf & "DECLARE @DENPRES2 AS VARCHAR(100)"
     sConsulta = sConsulta & vbCrLf & "DECLARE @DENPRES3 AS VARCHAR(100)"
     sConsulta = sConsulta & vbCrLf & "DECLARE @DENPRES4 AS VARCHAR(100)"
     sConsulta = sConsulta & vbCrLf & "DECLARE @PROCECAMBIO AS FLOAT"
     sConsulta = sConsulta & vbCrLf & "DECLARE @ADJDIR AS BIT"
     sConsulta = sConsulta & vbCrLf & "DECLARE @PRES1 AS VARCHAR (50)"
     sConsulta = sConsulta & vbCrLf & "DECLARE @PRES2 AS VARCHAR (50)"
     sConsulta = sConsulta & vbCrLf & "DECLARE @PRES3 AS VARCHAR (50)"
     sConsulta = sConsulta & vbCrLf & "DECLARE @PRES4 AS VARCHAR (50)"
     sConsulta = sConsulta & vbCrLf & ""
     sConsulta = sConsulta & vbCrLf & "SET CONCAT_NULL_YIELDS_NULL OFF"
     sConsulta = sConsulta & vbCrLf & ""
     sConsulta = sConsulta & vbCrLf & "DECLARE Cur_PROCE CURSOR FOR SELECT GMN1.DEN,PROCE.CAMBIO,PROCE.ADJDIR"
     sConsulta = sConsulta & vbCrLf & "From Proce, gmn1"
     sConsulta = sConsulta & vbCrLf & "WHERE PROCE.ANYO=@ANYO AND PROCE.GMN1=@GMN1 AND PROCE.COD=@PROCE"
     sConsulta = sConsulta & vbCrLf & "AND GMN1.COD=PROCE.GMN1"
     sConsulta = sConsulta & vbCrLf & "OPEN Cur_PROCE"
     sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM cur_PROCE INTO @DENGMN1,@PROCECAMBIO,@ADJDIR"
     sConsulta = sConsulta & vbCrLf & "IF @@FETCH_STATUS<>0 GOTO ERR_"
     sConsulta = sConsulta & vbCrLf & ""
     sConsulta = sConsulta & vbCrLf & ""
     sConsulta = sConsulta & vbCrLf & "DECLARE Cur_ITEM Cursor FOR SELECT DISTINCT ITEM_ADJ.ITEM,ITEM_PRESCON3.PRES1,PRES_3_CON1.DEN,ITEM_PRESCON3.PRES2,PRES_3_CON2.DEN,ITEM_PRESCON3.PRES3,PRES_3_CON3.DEN, ITEM_PRESCON3.PRES4,PRES_3_CON4.DEN"
     sConsulta = sConsulta & vbCrLf & "From ITEM_ADJ"
     sConsulta = sConsulta & vbCrLf & "INNER JOIN ITEM ON ITEM_ADJ.ANYO=ITEM.ANYO AND ITEM_ADJ.GMN1=ITEM.GMN1 AND ITEM_ADJ.PROCE=ITEM.PROCE  AND ITEM_ADJ.ITEM=ITEM.ID"
     sConsulta = sConsulta & vbCrLf & "INNER JOIN ITEM_PRESCON3 ON ITEM.ANYO=ITEM_PRESCON3.ANYO AND ITEM.GMN1=ITEM_PRESCON3.GMN1 AND ITEM.PROCE=ITEM_PRESCON3.PROCE AND ITEM.ID=ITEM_PRESCON3.ITEM AND ITEM_PRESCON3.PRES4 IS NOT NULL"
     sConsulta = sConsulta & vbCrLf & "INNER JOIN PRES_3_CON1 ON ITEM_PRESCON3.PRES1=PRES_3_CON1.COD"
     sConsulta = sConsulta & vbCrLf & "LEFT JOIN PRES_3_CON2 ON ITEM_PRESCON3.PRES1=PRES_3_CON2.PRES1 AND ITEM_PRESCON3.PRES2=PRES_3_CON2.COD"
     sConsulta = sConsulta & vbCrLf & "LEFT JOIN PRES_3_CON3 ON ITEM_PRESCON3.PRES1=PRES_3_CON3.PRES1 AND ITEM_PRESCON3.PRES2=PRES_3_CON3.PRES2 AND ITEM_PRESCON3.PRES3 =PRES_3_CON3.COD"
     sConsulta = sConsulta & vbCrLf & "LEFT JOIN PRES_3_CON4 ON ITEM_PRESCON3.PRES1=PRES_3_CON4.PRES1 AND ITEM_PRESCON3.PRES2=PRES_3_CON4.PRES2 AND ITEM_PRESCON3.PRES3 =PRES_3_CON4.PRES3 AND ITEM_PRESCON3.PRES4 = PRES_3_CON4.COD"
     sConsulta = sConsulta & vbCrLf & "INNER JOIN PROCE_OFE ON ITEM_ADJ.ANYO=PROCE_OFE.ANYO AND ITEM_ADJ.GMN1=PROCE_OFE.GMN1 AND ITEM_ADJ.PROCE=PROCE_OFE.PROCE AND ITEM_ADJ.OFE=PROCE_OFE.OFE AND ITEM_ADJ.PROVE=PROCE_OFE.PROVE"
     sConsulta = sConsulta & vbCrLf & "INNER JOIN ITEM_OFE ON ITEM_ADJ.ANYO=ITEM_OFE.ANYO AND ITEM_OFE.GMN1=ITEM_OFE.GMN1 AND ITEM_ADJ.PROCE=ITEM_OFE.PROCE AND ITEM_ADJ.PROVE=ITEM_OFE.PROVE AND ITEM_ADJ.ITEM=ITEM_OFE.ITEM AND ITEM_ADJ.OFE=ITEM_OFE.NUM"
     sConsulta = sConsulta & vbCrLf & "LEFT JOIN ART4 ON ITEM.ART=ART4.COD AND ITEM.GMN1=ART4.GMN1 AND ITEM.GMN2=ART4.GMN2 AND ITEM.GMN3=ART4.GMN3 AND ITEM.GMN4=ART4.GMN4"
     sConsulta = sConsulta & vbCrLf & "WHERE ITEM_ADJ.ANYO=@ANYO AND ITEM_ADJ.GMN1=@GMN1 AND ITEM_ADJ.PROCE=@PROCE AND ITEM_ADJ.PORCEN <> 0"
     sConsulta = sConsulta & vbCrLf & "OPEN Cur_ITEM"
     sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM Cur_ITEM INTO @ITEM,@PRES1,@DENPRES1,@PRES2,@DENPRES2,@PRES3,@DENPRES3,@PRES4,@DENPRES4"
     sConsulta = sConsulta & vbCrLf & "WHILE @@FETCH_STATUS=0"
     sConsulta = sConsulta & vbCrLf & ""
     sConsulta = sConsulta & vbCrLf & "BEGIN"
     sConsulta = sConsulta & vbCrLf & ""
     sConsulta = sConsulta & vbCrLf & "/*Antes de insertar el registro lo borra si existe*/"
     sConsulta = sConsulta & vbCrLf & "DELETE FROM AR_ITEM_PRESCON3 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM AND PRES1=@PRES1"
     sConsulta = sConsulta & vbCrLf & ""
     sConsulta = sConsulta & vbCrLf & "INSERT INTO  AR_ITEM_PRESCON3 ( ANYO,GMN1,PROCE,ITEM,PRES1,PRES2,PRES3,PRES4,DENPRES1,DENPRES2,DENPRES3,DENPRES4"
     sConsulta = sConsulta & vbCrLf & ",DENGMN1,ADJDIR,ART,DESCR,CANT,PRES,PREC )    "
     sConsulta = sConsulta & vbCrLf & "SELECT  @ANYO , @GMN1, @PROCE , @ITEM, @PRES1,@PRES2,@PRES3,@PRES4,@DENPRES1,@DENPRES2, @DENPRES3, @DENPRES4"
     sConsulta = sConsulta & vbCrLf & ",@DENGMN1,@ADJDIR, ITEM.ART,ITEM.DESCR + ART4.DEN,ITEM.CANT"
     sConsulta = sConsulta & vbCrLf & ",(SELECT SUM((ITEM_ADJ.PORCEN/100)*ITEM.CANT*ITEM.PREC/@PROCECAMBIO)"
     sConsulta = sConsulta & vbCrLf & "AS PRES FROM ITEM_ADJ INNER JOIN ITEM"
     sConsulta = sConsulta & vbCrLf & "ON ITEM_ADJ.ANYO=ITEM.ANYO AND ITEM_ADJ.GMN1=ITEM.GMN1 AND ITEM_ADJ.PROCE=ITEM.PROCE AND ITEM_ADJ.ITEM=ITEM.ID AND ITEM_ADJ.ITEM=@ITEM"
     sConsulta = sConsulta & vbCrLf & "AND ITEM_ADJ.ANYO=@ANYO AND ITEM_ADJ.GMN1=@GMN1 AND ITEM_ADJ.PROCE=@PROCE AND ITEM_ADJ.PORCEN <> 0 )"
     sConsulta = sConsulta & vbCrLf & ",(SELECT SUM((ITEM_ADJ.PORCEN/100)*ITEM.CANT*ITEM_OFE.PRECIO/(PROCE_OFE.CAMBIO*@PROCECAMBIO))"
     sConsulta = sConsulta & vbCrLf & "AS PREC FROM ITEM_ADJ INNER JOIN ITEM"
     sConsulta = sConsulta & vbCrLf & "ON ITEM_ADJ.ANYO=ITEM.ANYO AND ITEM_ADJ.GMN1=ITEM.GMN1 AND ITEM_ADJ.PROCE=ITEM.PROCE AND ITEM_ADJ.ITEM=ITEM.ID AND ITEM_ADJ.ITEM=@ITEM"
     sConsulta = sConsulta & vbCrLf & "AND ITEM_ADJ.ANYO=@ANYO AND ITEM_ADJ.GMN1=@GMN1 AND ITEM_ADJ.PROCE=@PROCE AND ITEM_ADJ.PORCEN <> 0"
     sConsulta = sConsulta & vbCrLf & "INNER JOIN ITEM_OFE"
     sConsulta = sConsulta & vbCrLf & "ON ITEM_ADJ.ANYO=ITEM_OFE.ANYO AND ITEM_ADJ.GMN1=ITEM_OFE.GMN1 AND ITEM_ADJ.PROCE=ITEM_OFE.PROCE"
    
     sConsulta = sConsulta & vbCrLf & "AND ITEM_ADJ.OFE=ITEM_OFE.NUM AND ITEM_ADJ.PROVE=ITEM_OFE.PROVE"
     sConsulta = sConsulta & vbCrLf & "AND ITEM_ADJ.ITEM=ITEM_OFE.ITEM"
     sConsulta = sConsulta & vbCrLf & "INNER JOIN PROCE_OFE"
     sConsulta = sConsulta & vbCrLf & "ON ITEM_OFE.ANYO=PROCE_OFE.ANYO AND ITEM_OFE.GMN1=PROCE_OFE.GMN1 AND ITEM_OFE.PROCE=PROCE_OFE.PROCE AND ITEM_OFE.NUM=PROCE_OFE.OFE"
     sConsulta = sConsulta & vbCrLf & "AND ITEM_OFE.PROVE=PROCE_OFE.PROVE)"
     sConsulta = sConsulta & vbCrLf & "From Item"
     sConsulta = sConsulta & vbCrLf & "LEFT JOIN ART4 ON ITEM.ART=ART4.COD AND ITEM.GMN1=ART4.GMN1 AND ITEM.GMN2=ART4.GMN2 AND ITEM.GMN3=ART4.GMN3 AND ITEM.GMN4=ART4.GMN4"
     sConsulta = sConsulta & vbCrLf & "WHERE ITEM.ANYO=@ANYO AND ITEM.GMN1=@GMN1 AND ITEM.PROCE=@PROCE AND ITEM.ID=@ITEM"
     sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM Cur_ITEM INTO @ITEM,@PRES1,@DENPRES1,@PRES2,@DENPRES2,@PRES3,@DENPRES3,@PRES4,@DENPRES4"
     sConsulta = sConsulta & vbCrLf & "End"
     sConsulta = sConsulta & vbCrLf & ""
     sConsulta = sConsulta & vbCrLf & "ERR_:"
     sConsulta = sConsulta & vbCrLf & "Close Cur_PROCE"
     sConsulta = sConsulta & vbCrLf & "DEALLOCATE Cur_PROCE"
     sConsulta = sConsulta & vbCrLf & "Close Cur_ITEM"
     sConsulta = sConsulta & vbCrLf & "DEALLOCATE Cur_ITEM"
     sConsulta = sConsulta & vbCrLf & "SET CONCAT_NULL_YIELDS_NULL ON"
     sConsulta = sConsulta & vbCrLf & "SELECT @RES=@@ERROR"
     sConsulta = sConsulta & vbCrLf & "Return"
     sConsulta = sConsulta & vbCrLf & "End"

     ModificarSP_AR_ITEM_PRESCON3_2_8_5_0 = sConsulta
End Function

Private Function ModificarSP_AR_ITEM_PRESCON4_2_8_5_0() As String
Dim sConsulta As String
    
    sConsulta = "CREATE PROCEDURE SP_AR_ITEM_PRESCON4 (@ANYO SMALLINT,@GMN1 VARCHAR(50), @PROCE INT,@RES INT OUTPUT)  AS"
    sConsulta = sConsulta & vbCrLf & "BEGIN"
    sConsulta = sConsulta & vbCrLf & "DECLARE @ITEM AS INTEGER"
    sConsulta = sConsulta & vbCrLf & "DECLARE @DENGMN1 AS VARCHAR(100)"
    sConsulta = sConsulta & vbCrLf & "DECLARE @DENPRES1 AS VARCHAR(100)"
    sConsulta = sConsulta & vbCrLf & "DECLARE @DENPRES2 AS VARCHAR(100)"
    sConsulta = sConsulta & vbCrLf & "DECLARE @DENPRES3 AS VARCHAR(100)"
    sConsulta = sConsulta & vbCrLf & "DECLARE @DENPRES4 AS VARCHAR(100)"
    sConsulta = sConsulta & vbCrLf & "DECLARE @PROCECAMBIO AS FLOAT"
    sConsulta = sConsulta & vbCrLf & "DECLARE @ADJDIR AS BIT"
    sConsulta = sConsulta & vbCrLf & "DECLARE @PRES1 AS VARCHAR (50)"
    sConsulta = sConsulta & vbCrLf & "DECLARE @PRES2 AS VARCHAR (50)"
    sConsulta = sConsulta & vbCrLf & "DECLARE @PRES3 AS VARCHAR (50)"
    sConsulta = sConsulta & vbCrLf & "DECLARE @PRES4 AS VARCHAR (50)"
    sConsulta = sConsulta & vbCrLf & ""
    sConsulta = sConsulta & vbCrLf & "SET CONCAT_NULL_YIELDS_NULL OFF"
    sConsulta = sConsulta & vbCrLf & ""
    sConsulta = sConsulta & vbCrLf & "DECLARE Cur_PROCE CURSOR FOR SELECT GMN1.DEN,PROCE.CAMBIO,PROCE.ADJDIR"
    sConsulta = sConsulta & vbCrLf & "From Proce, gmn1"
    sConsulta = sConsulta & vbCrLf & "WHERE PROCE.ANYO=@ANYO AND PROCE.GMN1=@GMN1 AND PROCE.COD=@PROCE"
    sConsulta = sConsulta & vbCrLf & "AND GMN1.COD=PROCE.GMN1"
    sConsulta = sConsulta & vbCrLf & "OPEN Cur_PROCE"
    sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM cur_PROCE INTO @DENGMN1,@PROCECAMBIO,@ADJDIR"
    sConsulta = sConsulta & vbCrLf & "IF @@FETCH_STATUS<>0 GOTO ERR_"
    sConsulta = sConsulta & vbCrLf & ""
    sConsulta = sConsulta & vbCrLf & ""
    sConsulta = sConsulta & vbCrLf & "DECLARE Cur_ITEM Cursor FOR SELECT DISTINCT ITEM_ADJ.ITEM,ITEM_PRESCON4.PRES1,PRES_4_CON1.DEN,ITEM_PRESCON4.PRES2,PRES_4_CON2.DEN,ITEM_PRESCON4.PRES3,PRES_4_CON3.DEN, ITEM_PRESCON4.PRES4,PRES_4_CON4.DEN"
    sConsulta = sConsulta & vbCrLf & "From ITEM_ADJ"
    sConsulta = sConsulta & vbCrLf & "INNER JOIN ITEM ON ITEM_ADJ.ANYO=ITEM.ANYO AND ITEM_ADJ.GMN1=ITEM.GMN1 AND ITEM_ADJ.PROCE=ITEM.PROCE  AND ITEM_ADJ.ITEM=ITEM.ID"
    sConsulta = sConsulta & vbCrLf & "INNER JOIN ITEM_PRESCON4 ON ITEM.ANYO=ITEM_PRESCON4.ANYO AND ITEM.GMN1=ITEM_PRESCON4.GMN1 AND ITEM.PROCE=ITEM_PRESCON4.PROCE AND ITEM.ID=ITEM_PRESCON4.ITEM AND ITEM_PRESCON4.PRES4 IS NOT NULL"
    sConsulta = sConsulta & vbCrLf & "INNER JOIN PRES_4_CON1 ON ITEM_PRESCON4.PRES1=PRES_4_CON1.COD"
    sConsulta = sConsulta & vbCrLf & "LEFT JOIN PRES_4_CON2 ON ITEM_PRESCON4.PRES1=PRES_4_CON2.PRES1 AND ITEM_PRESCON4.PRES2=PRES_4_CON2.COD"
    sConsulta = sConsulta & vbCrLf & "LEFT JOIN PRES_4_CON3 ON ITEM_PRESCON4.PRES1=PRES_4_CON3.PRES1 AND ITEM_PRESCON4.PRES2=PRES_4_CON3.PRES2 AND ITEM_PRESCON4.PRES3 =PRES_4_CON3.COD"
    sConsulta = sConsulta & vbCrLf & "LEFT JOIN PRES_4_CON4 ON ITEM_PRESCON4.PRES1=PRES_4_CON4.PRES1 AND ITEM_PRESCON4.PRES2=PRES_4_CON4.PRES2 AND ITEM_PRESCON4.PRES3 =PRES_4_CON4.PRES3 AND ITEM_PRESCON4.PRES4 = PRES_4_CON4.COD"
    sConsulta = sConsulta & vbCrLf & "INNER JOIN PROCE_OFE ON ITEM_ADJ.ANYO=PROCE_OFE.ANYO AND ITEM_ADJ.GMN1=PROCE_OFE.GMN1 AND ITEM_ADJ.PROCE=PROCE_OFE.PROCE AND ITEM_ADJ.OFE=PROCE_OFE.OFE AND ITEM_ADJ.PROVE=PROCE_OFE.PROVE"
    sConsulta = sConsulta & vbCrLf & "INNER JOIN ITEM_OFE ON ITEM_ADJ.ANYO=ITEM_OFE.ANYO AND ITEM_OFE.GMN1=ITEM_OFE.GMN1 AND ITEM_ADJ.PROCE=ITEM_OFE.PROCE AND ITEM_ADJ.PROVE=ITEM_OFE.PROVE AND ITEM_ADJ.ITEM=ITEM_OFE.ITEM AND ITEM_ADJ.OFE=ITEM_OFE.NUM"
    sConsulta = sConsulta & vbCrLf & "LEFT JOIN ART4 ON ITEM.ART=ART4.COD AND ITEM.GMN1=ART4.GMN1 AND ITEM.GMN2=ART4.GMN2 AND ITEM.GMN3=ART4.GMN3 AND ITEM.GMN4=ART4.GMN4"
    sConsulta = sConsulta & vbCrLf & "WHERE ITEM_ADJ.ANYO=@ANYO AND ITEM_ADJ.GMN1=@GMN1 AND ITEM_ADJ.PROCE=@PROCE AND ITEM_ADJ.PORCEN <> 0"
    sConsulta = sConsulta & vbCrLf & "OPEN Cur_ITEM"
    sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM Cur_ITEM INTO @ITEM,@PRES1,@DENPRES1,@PRES2,@DENPRES2,@PRES3,@DENPRES3,@PRES4,@DENPRES4"
    sConsulta = sConsulta & vbCrLf & "WHILE @@FETCH_STATUS=0"
    sConsulta = sConsulta & vbCrLf & ""
    sConsulta = sConsulta & vbCrLf & ""
    sConsulta = sConsulta & vbCrLf & "BEGIN"
    sConsulta = sConsulta & vbCrLf & ""
    sConsulta = sConsulta & vbCrLf & "/*Antes de insertar el registro lo borra si existe*/"
    sConsulta = sConsulta & vbCrLf & "DELETE FROM AR_ITEM_PRESCON4 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ITEM=@ITEM AND PRES1=@PRES1"
    sConsulta = sConsulta & vbCrLf & ""
    sConsulta = sConsulta & vbCrLf & "INSERT INTO  AR_ITEM_PRESCON4 ( ANYO,GMN1,PROCE,ITEM,PRES1,PRES2,PRES3,PRES4,DENPRES1,DENPRES2,DENPRES3,DENPRES4"
    sConsulta = sConsulta & vbCrLf & ",DENGMN1,ADJDIR,ART,DESCR,CANT,PRES,PREC )"
    sConsulta = sConsulta & vbCrLf & "    "
    sConsulta = sConsulta & vbCrLf & "SELECT  @ANYO , @GMN1, @PROCE , @ITEM, @PRES1,@PRES2,@PRES3,@PRES4,@DENPRES1,@DENPRES2, @DENPRES3, @DENPRES4"
    sConsulta = sConsulta & vbCrLf & ",@DENGMN1,@ADJDIR, ITEM.ART,ITEM.DESCR + ART4.DEN,ITEM.CANT"
    sConsulta = sConsulta & vbCrLf & ",(SELECT SUM((ITEM_ADJ.PORCEN/100)*ITEM.CANT*ITEM.PREC/@PROCECAMBIO)"
    sConsulta = sConsulta & vbCrLf & "AS PRES FROM ITEM_ADJ INNER JOIN ITEM"
    sConsulta = sConsulta & vbCrLf & "ON ITEM_ADJ.ANYO=ITEM.ANYO AND ITEM_ADJ.GMN1=ITEM.GMN1 AND ITEM_ADJ.PROCE=ITEM.PROCE AND ITEM_ADJ.ITEM=ITEM.ID AND ITEM_ADJ.ITEM=@ITEM"
    sConsulta = sConsulta & vbCrLf & "AND ITEM_ADJ.ANYO=@ANYO AND ITEM_ADJ.GMN1=@GMN1 AND ITEM_ADJ.PROCE=@PROCE AND ITEM_ADJ.PORCEN <> 0 )"
    sConsulta = sConsulta & vbCrLf & ",(SELECT SUM((ITEM_ADJ.PORCEN/100)*ITEM.CANT*ITEM_OFE.PRECIO/(PROCE_OFE.CAMBIO*@PROCECAMBIO))"
    sConsulta = sConsulta & vbCrLf & "AS PREC FROM ITEM_ADJ INNER JOIN ITEM"
    sConsulta = sConsulta & vbCrLf & "ON ITEM_ADJ.ANYO=ITEM.ANYO AND ITEM_ADJ.GMN1=ITEM.GMN1 AND ITEM_ADJ.PROCE=ITEM.PROCE AND ITEM_ADJ.ITEM=ITEM.ID AND ITEM_ADJ.ITEM=@ITEM"
    sConsulta = sConsulta & vbCrLf & "AND ITEM_ADJ.ANYO=@ANYO AND ITEM_ADJ.GMN1=@GMN1 AND ITEM_ADJ.PROCE=@PROCE AND ITEM_ADJ.PORCEN <> 0"
    sConsulta = sConsulta & vbCrLf & "INNER JOIN ITEM_OFE"
    sConsulta = sConsulta & vbCrLf & "ON ITEM_ADJ.ANYO=ITEM_OFE.ANYO AND ITEM_ADJ.GMN1=ITEM_OFE.GMN1 AND ITEM_ADJ.PROCE=ITEM_OFE.PROCE"
    sConsulta = sConsulta & vbCrLf & "AND ITEM_ADJ.OFE=ITEM_OFE.NUM AND ITEM_ADJ.PROVE=ITEM_OFE.PROVE"
    sConsulta = sConsulta & vbCrLf & "AND ITEM_ADJ.ITEM=ITEM_OFE.ITEM"
    sConsulta = sConsulta & vbCrLf & "INNER JOIN PROCE_OFE"
    sConsulta = sConsulta & vbCrLf & "ON ITEM_OFE.ANYO=PROCE_OFE.ANYO AND ITEM_OFE.GMN1=PROCE_OFE.GMN1 AND ITEM_OFE.PROCE=PROCE_OFE.PROCE AND ITEM_OFE.NUM=PROCE_OFE.OFE"
    sConsulta = sConsulta & vbCrLf & "AND ITEM_OFE.PROVE=PROCE_OFE.PROVE)"
    sConsulta = sConsulta & vbCrLf & "From Item"
    sConsulta = sConsulta & vbCrLf & "LEFT JOIN ART4 ON ITEM.ART=ART4.COD AND ITEM.GMN1=ART4.GMN1 AND ITEM.GMN2=ART4.GMN2 AND ITEM.GMN3=ART4.GMN3 AND ITEM.GMN4=ART4.GMN4"
    sConsulta = sConsulta & vbCrLf & "WHERE ITEM.ANYO=@ANYO AND ITEM.GMN1=@GMN1 AND ITEM.PROCE=@PROCE AND ITEM.ID=@ITEM"
    sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM Cur_ITEM INTO @ITEM,@PRES1,@DENPRES1,@PRES2,@DENPRES2,@PRES3,@DENPRES3,@PRES4,@DENPRES4"
    sConsulta = sConsulta & vbCrLf & "End"
    sConsulta = sConsulta & vbCrLf & ""
    sConsulta = sConsulta & vbCrLf & "ERR_:"
    sConsulta = sConsulta & vbCrLf & "Close Cur_PROCE"
    sConsulta = sConsulta & vbCrLf & "DEALLOCATE Cur_PROCE"
    sConsulta = sConsulta & vbCrLf & "Close Cur_ITEM"
    sConsulta = sConsulta & vbCrLf & "DEALLOCATE Cur_ITEM"
    sConsulta = sConsulta & vbCrLf & "SET CONCAT_NULL_YIELDS_NULL ON"
    sConsulta = sConsulta & vbCrLf & "SELECT @RES=@@ERROR"
    sConsulta = sConsulta & vbCrLf & "Return"
    sConsulta = sConsulta & vbCrLf & "End"

    ModificarSP_AR_ITEM_PRESCON4_2_8_5_0 = sConsulta
End Function

Private Function ModificarTriggerPER_TG_UPD() As String
Dim sConsulta As String

    sConsulta = "CREATE TRIGGER PER_TG_UPD ON dbo.PER"
    sConsulta = sConsulta & vbCrLf & "FOR UPDATE"
    sConsulta = sConsulta & vbCrLf & "AS"
    sConsulta = sConsulta & vbCrLf & "DECLARE @EQP AS VARCHAR(50)"
    sConsulta = sConsulta & vbCrLf & "DECLARE @COD AS VARCHAR(50)"
    sConsulta = sConsulta & vbCrLf & ""
    sConsulta = sConsulta & vbCrLf & "DECLARE curTG_PER_Upd CURSOR FOR SELECT EQP,COD FROM INSERTED"
    sConsulta = sConsulta & vbCrLf & "OPEN curTG_PER_Upd"
    sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM curTG_PER_Upd INTO @EQP,@COD"
    sConsulta = sConsulta & vbCrLf & "WHILE @@FETCH_STATUS=0"
    sConsulta = sConsulta & vbCrLf & "BEGIN"
    sConsulta = sConsulta & vbCrLf & "IF UPDATE(EQP) "
    sConsulta = sConsulta & vbCrLf & "  begin"
    sConsulta = sConsulta & vbCrLf & "  "
    sConsulta = sConsulta & vbCrLf & "  IF @EQP IS NULL"
    sConsulta = sConsulta & vbCrLf & "      UPDATE USU SET TIPO=3  WHERE PER=@COD"
    sConsulta = sConsulta & vbCrLf & "  ELSE"
    sConsulta = sConsulta & vbCrLf & "      UPDATE USU SET TIPO=2  WHERE PER=@COD"
    sConsulta = sConsulta & vbCrLf & ""
    sConsulta = sConsulta & vbCrLf & "END"
    sConsulta = sConsulta & vbCrLf & ""
    sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM curTG_PER_Upd INTO @EQP,@COD"
    sConsulta = sConsulta & vbCrLf & "End"
    sConsulta = sConsulta & vbCrLf & "Close curTG_PER_Upd"
    sConsulta = sConsulta & vbCrLf & "DEALLOCATE curTG_PER_Upd"

    ModificarTriggerPER_TG_UPD = sConsulta
End Function

Private Function ModificarPER_COD2_8_5_0() As String
Dim sConsulta As String

    sConsulta = "CREATE PROCEDURE PER_COD (@OLD VARCHAR(50),@NEW VARCHAR(50))  AS"
    sConsulta = sConsulta & vbCrLf & "DECLARE @VAR_COD VARCHAR(50)"
    sConsulta = sConsulta & vbCrLf & "DECLARE @EQP VARCHAR(50)"
    sConsulta = sConsulta & vbCrLf & ""
    sConsulta = sConsulta & vbCrLf & "SET XACT_ABORT ON"
    sConsulta = sConsulta & vbCrLf & "BEGIN TRANSACTION"
    sConsulta = sConsulta & vbCrLf & "BEGIN"
    sConsulta = sConsulta & vbCrLf & "DECLARE D CURSOR FOR SELECT COD,EQP FROM PER WHERE COD=@OLD"
    sConsulta = sConsulta & vbCrLf & "OPEN D"
    sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM D INTO @VAR_COD,@EQP"
    sConsulta = sConsulta & vbCrLf & "IF @@FETCH_STATUS=0"
    sConsulta = sConsulta & vbCrLf & "BEGIN"
    sConsulta = sConsulta & vbCrLf & ""
    sConsulta = sConsulta & vbCrLf & "/*Ejecuta el store procedure COM_COD*/"
    sConsulta = sConsulta & vbCrLf & "IF NOT (@EQP) IS NULL"
    sConsulta = sConsulta & vbCrLf & "BEGIN"
    sConsulta = sConsulta & vbCrLf & "EXECUTE COM_COD @OLD,@NEW,@EQP"
    sConsulta = sConsulta & vbCrLf & "END"
    sConsulta = sConsulta & vbCrLf & ""
    sConsulta = sConsulta & vbCrLf & "ALTER TABLE PER NOCHECK CONSTRAINT ALL"
    sConsulta = sConsulta & vbCrLf & "ALTER TABLE USU NOCHECK CONSTRAINT ALL"
    sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_PER NOCHECK CONSTRAINT ALL"
    sConsulta = sConsulta & vbCrLf & "UPDATE PER SET COD=@NEW WHERE COD=@OLD"
    sConsulta = sConsulta & vbCrLf & "UPDATE USU SET PER=@NEW WHERE PER=@OLD"
    sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_PER SET PER=@NEW WHERE PER=@OLD"
    sConsulta = sConsulta & vbCrLf & "ALTER TABLE PER CHECK CONSTRAINT ALL"
    sConsulta = sConsulta & vbCrLf & "ALTER TABLE USU CHECK CONSTRAINT ALL"
    sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_PER CHECK CONSTRAINT ALL"
    sConsulta = sConsulta & vbCrLf & ""
    sConsulta = sConsulta & vbCrLf & "End"
    sConsulta = sConsulta & vbCrLf & "Close D"
    sConsulta = sConsulta & vbCrLf & "DEALLOCATE D"
    sConsulta = sConsulta & vbCrLf & "End"
    sConsulta = sConsulta & vbCrLf & "COMMIT TRANSACTION"

    ModificarPER_COD2_8_5_0 = sConsulta
End Function


Private Function CrearNuevoGMN1_COD_2_8_03() As String
Dim sConsulta As String

sConsulta = "CREATE PROCEDURE GMN1_COD (@OLD VARCHAR(50),@NEW VARCHAR(50))  AS"
sConsulta = sConsulta & vbCrLf & "DECLARE @VAR_COD VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "SET XACT_ABORT ON"
sConsulta = sConsulta & vbCrLf & "BEGIN TRANSACTION"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "DECLARE C CURSOR FOR SELECT COD FROM GMN1 WHERE COD=@OLD"
sConsulta = sConsulta & vbCrLf & "OPEN C"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM C INTO @VAR_COD"
sConsulta = sConsulta & vbCrLf & "IF @@FETCH_STATUS=0"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART1_ADJ NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART2_ADJ NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART3_ADJ NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4_ADJ NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE BLOQUEO_PROCE NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE COM_GMN1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE COM_GMN2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE COM_GMN3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE COM_GMN4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_ADJ NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_ADJREU NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_ESP NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_OBJ NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_OFE NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_OFE_WEB NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRES NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_UON1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_UON2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_UON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESPROY NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_PROVE NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_MAT1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_MAT2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_MAT3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_MAT4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_ESP NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_OFE_ADJUN NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_OFE NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_OFE_WEB_ADJUN NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_OFE_WEB NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_PER NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_PROVE NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_PROVE_PET NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_PROVE_POT NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE REU_PROCE NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_PROCE NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4_ESP NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN4_ACT NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "UPDATE GMN1 SET COD=@NEW WHERE COD=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART1 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART1_ADJ SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART2 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART2_ADJ SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART3 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART3_ADJ SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART4 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART4_ADJ SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE BLOQUEO_PROCE SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE COM_GMN1 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE COM_GMN2 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE COM_GMN3 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE COM_GMN4 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE GMN2 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE GMN3 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE GMN4 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_ADJ SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_ADJREU SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_ESP SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_OBJ SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_OFE SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_OFE_WEB SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_PRES SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_UON1 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_UON2 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_UON3 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_MAT1 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_MAT2 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_MAT3 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_MAT4 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_ESP SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_OFE_ADJUN SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_OFE SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_OFE_WEB_ADJUN SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_OFE_WEB SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_PER SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_PROVE SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_PROVE_PET SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_PROVE_POT SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_GMN1 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_GMN2 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_GMN3 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_GMN4 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE REU_PROCE SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_PROCE SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_UON1 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_UON2 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_UON3 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON1 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON2 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON3 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON4 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY1 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY2 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY3 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY4 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_PROVE SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_PRESCON SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_PRESCON3 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_PRESCON4 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_PRESPROY SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART4_ESP  SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE GMN4_ACT  SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART1_ADJ CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART2_ADJ CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART3_ADJ CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4_ADJ CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE BLOQUEO_PROCE CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE COM_GMN1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE COM_GMN2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE COM_GMN3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE COM_GMN4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_ADJ CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_ADJREU CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_ESP CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_OBJ CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_OFE CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_OFE_WEB CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRES CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_UON1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_UON2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_UON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_MAT1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_MAT2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_MAT3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_MAT4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_ESP CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_OFE_ADJUN CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_OFE CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_OFE_WEB_ADJUN CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_OFE_WEB CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_PER CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_PROVE CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_PROVE_PET CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_PROVE_POT CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE REU_PROCE CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_PROCE CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESPROY CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_PROVE CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4_ESP CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN4_ACT CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "Close C"
sConsulta = sConsulta & vbCrLf & "DEALLOCATE C"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "COMMIT TRANSACTION"

CrearNuevoGMN1_COD_2_8_03 = sConsulta

End Function

Function CrearNuevoGMN2_COD_2_8_03() As String
Dim sConsulta As String
sConsulta = " CREATE PROCEDURE GMN2_COD (@OLD VARCHAR(50),@NEW VARCHAR(50),@GMN1 VARCHAR(50))  AS"
sConsulta = sConsulta & vbCrLf & "DECLARE @VAR_COD VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "SET XACT_ABORT ON"
sConsulta = sConsulta & vbCrLf & "BEGIN TRANSACTION"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "DECLARE C CURSOR FOR SELECT COD FROM GMN2 WHERE GMN1=@GMN1 AND COD=@OLD"
sConsulta = sConsulta & vbCrLf & "OPEN C"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM C INTO @VAR_COD"
sConsulta = sConsulta & vbCrLf & "IF @@FETCH_STATUS=0"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART2_ADJ NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART3_ADJ NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4_ADJ NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE COM_GMN2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE COM_GMN3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE COM_GMN4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_MAT2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_MAT3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_MAT4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_PROCE NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4_ESP NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN4_ACT NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "UPDATE GMN2 SET COD=@NEW WHERE GMN1=@GMN1 AND COD=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART2 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART2_ADJ SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART3 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART3_ADJ SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART4 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART4_ADJ SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE COM_GMN2 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE COM_GMN3 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE COM_GMN4 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE GMN3 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE GMN4 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_MAT2 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_MAT3 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_MAT4 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_GMN2 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_GMN3 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_GMN4 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_PROCE SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_UON1 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_UON2 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_UON3 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON1 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON2 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON3 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON4 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY1 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY2 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY3 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY4 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_ART2 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_ART3 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_ART4 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART4_ESP SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE GMN4_ACT SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART2_ADJ CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART3_ADJ CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4_ADJ CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE COM_GMN2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE COM_GMN3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE COM_GMN4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_MAT2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_MAT3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_MAT4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_PROCE CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4_ESP CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN4_ACT CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "Close C"
sConsulta = sConsulta & vbCrLf & "DEALLOCATE C"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "COMMIT TRANSACTION"



CrearNuevoGMN2_COD_2_8_03 = sConsulta
End Function

Function CrearNuevoGMN3_COD_2_8_03() As String
Dim sConsulta As String

sConsulta = "CREATE PROCEDURE GMN3_COD (@OLD VARCHAR(50),@NEW VARCHAR(50),@GMN1 VARCHAR(50),@GMN2 VARCHAR(50))  AS"
sConsulta = sConsulta & vbCrLf & "DECLARE @VAR_COD VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "SET XACT_ABORT ON"
sConsulta = sConsulta & vbCrLf & "BEGIN TRANSACTION"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "DECLARE C CURSOR FOR SELECT COD FROM GMN3 WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND COD=@OLD"
sConsulta = sConsulta & vbCrLf & "OPEN C"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM C INTO @VAR_COD"
sConsulta = sConsulta & vbCrLf & "IF @@FETCH_STATUS=0"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART3_ADJ NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4_ADJ NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE COM_GMN3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE COM_GMN4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_MAT3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_MAT4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_PROCE NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4_ESP NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN4_ACT NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "UPDATE GMN3 SET COD=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND COD=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART3 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART3_ADJ SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART4 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART4_ADJ SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE COM_GMN3 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE COM_GMN4 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE GMN4 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_MAT3 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_MAT4 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_GMN3 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_GMN4 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_PROCE SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_UON1 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_UON2 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_UON3 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON3 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON4 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY3 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY4 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_ART3 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_ART4 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART4_ESP SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE GMN4_ACT SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART3_ADJ CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4_ADJ CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE COM_GMN3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE COM_GMN4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_MAT3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_MAT4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_PROCE CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4_ESP CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN4_ACT CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "Close C"
sConsulta = sConsulta & vbCrLf & "DEALLOCATE C"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "COMMIT TRANSACTION"

CrearNuevoGMN3_COD_2_8_03 = sConsulta
End Function

Function CrearNuevoGMN4_COD_2_8_03() As String
Dim sConsulta As String

sConsulta = "CREATE PROCEDURE GMN4_COD (@OLD VARCHAR(50),@NEW VARCHAR(50),@GMN1 VARCHAR(50),@GMN2 VARCHAR(50),@GMN3 VARCHAR(50))  AS"
sConsulta = sConsulta & vbCrLf & "DECLARE @VAR_COD VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "SET XACT_ABORT ON"
sConsulta = sConsulta & vbCrLf & "BEGIN TRANSACTION"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "DECLARE C CURSOR FOR SELECT COD FROM GMN4 WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND COD=@OLD"
sConsulta = sConsulta & vbCrLf & "OPEN C"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM C INTO @VAR_COD"
sConsulta = sConsulta & vbCrLf & "IF @@FETCH_STATUS=0"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4_ADJ NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE COM_GMN4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_MAT4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_PROCE NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4_ESP NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN4_ACT NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "UPDATE GMN4 SET COD=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND COD=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART4 SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART4_ADJ SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE COM_GMN4 SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_MAT4 SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_GMN4 SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_PROCE SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_UON1 SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_UON2 SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_UON3 SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON1 SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON2 SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON3 SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON4 SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY1 SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY2 SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY3 SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY4 SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_ART4 SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART4_ESP  SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE GMN4_ACT  SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4_ADJ CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE COM_GMN4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_MAT4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_PROCE CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4_ESP CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN4_ACT CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "Close C"
sConsulta = sConsulta & vbCrLf & "DEALLOCATE C"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "COMMIT TRANSACTION"

CrearNuevoGMN4_COD_2_8_03 = sConsulta
End Function

Private Function ModifSP_MODIF_PROCE_OFE_2_8_03() As String
Dim sConsulta As String

sConsulta = "CREATE PROCEDURE  SP_MODIF_PROCE_OFE (@ANYO SMALLINT,@GMN1 VARCHAR(50), @PROCE INT,@PROVE VARCHAR(50) )  AS"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "   UPDATE PROCE_OFE SET ULT=0 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE AND ULT=1"
sConsulta = sConsulta & vbCrLf & "   UPDATE PROCE_OFE SET LEIDO=1 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE"
sConsulta = sConsulta & vbCrLf & "End"

ModifSP_MODIF_PROCE_OFE_2_8_03 = sConsulta
End Function

Private Function CrearProcedure_ART1_COD_2_8_05() As String
Dim sConsulta As String

sConsulta = "CREATE PROCEDURE ART1_COD (@OLD VARCHAR(50),@NEW VARCHAR(50),@GMN1 VARCHAR(50))  AS"
sConsulta = sConsulta & vbCrLf & "DECLARE @VAR_COD VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "SET XACT_ABORT ON"
sConsulta = sConsulta & vbCrLf & "BEGIN TRANSACTION"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "DECLARE C CURSOR FOR SELECT COD FROM ART1 WHERE GMN1=@GMN1 AND COD=@OLD"
sConsulta = sConsulta & vbCrLf & "OPEN C"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM C INTO @VAR_COD"
sConsulta = sConsulta & vbCrLf & "IF @@FETCH_STATUS=0"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART1_ADJ NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_NES_UON1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_PRESCON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_PRESCON4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "UPDATE ART1 SET COD=@NEW WHERE GMN1=@GMN1 AND COD=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART1_ADJ SET ART=@NEW WHERE GMN1=@GMN1 AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM SET ART=@NEW WHERE GMN1=@GMN1 AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM SET ART=@NEW WHERE GMN1=@GMN1 AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES SET ART=@NEW WHERE GMN1=@GMN1 AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON1 SET ART=@NEW WHERE GMN1=@GMN1 AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON2 SET ART=@NEW WHERE GMN1=@GMN1 AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON3 SET ART=@NEW WHERE GMN1=@GMN1 AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON4 SET ART=@NEW WHERE GMN1=@GMN1 AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY1 SET ART=@NEW WHERE GMN1=@GMN1 AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY2 SET ART=@NEW WHERE GMN1=@GMN1 AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY3 SET ART=@NEW WHERE GMN1=@GMN1 AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY4 SET ART=@NEW WHERE GMN1=@GMN1 AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_1 SET ART=@NEW WHERE GMN1=@GMN1 AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_2 SET ART=@NEW WHERE GMN1=@GMN1 AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_3 SET ART=@NEW WHERE GMN1=@GMN1 AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_4 SET ART=@NEW WHERE GMN1=@GMN1 AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_1 SET ART=@NEW WHERE GMN1=@GMN1 AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_2 SET ART=@NEW WHERE GMN1=@GMN1 AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_3 SET ART=@NEW WHERE GMN1=@GMN1 AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_4 SET ART=@NEW WHERE GMN1=@GMN1 AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_UON1 SET ART=@NEW WHERE GMN1=@GMN1 AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_UON2 SET ART=@NEW WHERE GMN1=@GMN1 AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_UON3 SET ART=@NEW WHERE GMN1=@GMN1 AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_PRESCON3 SET ART=@NEW WHERE GMN1=@GMN1 AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_PRESCON4 SET ART=@NEW WHERE GMN1=@GMN1 AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_ART1 SET ART=@NEW WHERE GMN1=@GMN1 AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART1_ADJ CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_PRESCON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_PRESCON4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "Close C"
sConsulta = sConsulta & vbCrLf & "DEALLOCATE C"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "COMMIT TRANSACTION"

CrearProcedure_ART1_COD_2_8_05 = sConsulta

End Function


Private Function CrearProcedure_ART2_COD_2_8_05() As String
Dim sConsulta As String

sConsulta = "CREATE PROCEDURE ART2_COD (@OLD VARCHAR(50),@NEW VARCHAR(50),@GMN1 VARCHAR(50),@GMN2 VARCHAR(50))  AS"
sConsulta = sConsulta & vbCrLf & "DECLARE @VAR_COD VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "SET XACT_ABORT ON"
sConsulta = sConsulta & vbCrLf & "BEGIN TRANSACTION"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "DECLARE C CURSOR FOR SELECT COD FROM ART2 WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND COD=@OLD"
sConsulta = sConsulta & vbCrLf & "OPEN C"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM C INTO @VAR_COD"
sConsulta = sConsulta & vbCrLf & "IF @@FETCH_STATUS=0"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART2_ADJ NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_NES_UON1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "UPDATE ART2 SET COD=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND COD=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART2_ADJ SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON1 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON2 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON3 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON4 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY1 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY2 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY3 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY4 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_1 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_2 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_3 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_4 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_1 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_2 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_3 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_4 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_UON1 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_UON2 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_UON3 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_ART2 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART2_ADJ CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "Close C"
sConsulta = sConsulta & vbCrLf & "DEALLOCATE C"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "COMMIT TRANSACTION"

CrearProcedure_ART2_COD_2_8_05 = sConsulta

End Function

Private Function CrearProcedure_ART3_COD_2_8_05() As String
Dim sConsulta As String

sConsulta = "CREATE PROCEDURE ART3_COD (@OLD VARCHAR(50),@NEW VARCHAR(50),@GMN1 VARCHAR(50),@GMN2 VARCHAR(50),@GMN3 VARCHAR(50))  AS"
sConsulta = sConsulta & vbCrLf & "DECLARE @VAR_COD VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "SET XACT_ABORT ON"
sConsulta = sConsulta & vbCrLf & "BEGIN TRANSACTION"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "DECLARE C CURSOR FOR SELECT COD FROM ART3 WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND COD=@OLD"
sConsulta = sConsulta & vbCrLf & "OPEN C"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM C INTO @VAR_COD"
sConsulta = sConsulta & vbCrLf & "IF @@FETCH_STATUS=0"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART3_ADJ NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "UPDATE ART3 SET COD=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND COD=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART3_ADJ SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON1 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND GMN3=@GMN3 AND  ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON2 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND GMN3=@GMN3 AND  ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON3 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND GMN3=@GMN3 AND  ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON4 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND GMN3=@GMN3 AND  ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY1 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND GMN3=@GMN3 AND  ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY2 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND GMN3=@GMN3 AND  ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY3 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND GMN3=@GMN3 AND  ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY4 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND GMN3=@GMN3 AND  ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_1 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND GMN3=@GMN3 AND  ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_2 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND GMN3=@GMN3 AND  ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_3 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND GMN3=@GMN3 AND  ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_4 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND GMN3=@GMN3 AND  ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_1 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND GMN3=@GMN3 AND  ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_2 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND GMN3=@GMN3 AND  ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_3 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND GMN3=@GMN3 AND  ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_4 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND GMN3=@GMN3 AND  ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_UON1 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_UON2 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_UON3 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_ART3 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART3_ADJ CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "Close C"
sConsulta = sConsulta & vbCrLf & "DEALLOCATE C"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "COMMIT TRANSACTION"

CrearProcedure_ART3_COD_2_8_05 = sConsulta

End Function


Private Function CrearProcedure_ART4_COD_2_8_05() As String
Dim sConsulta As String

sConsulta = "CREATE PROCEDURE ART4_COD (@OLD VARCHAR(50),@NEW VARCHAR(50),@GMN1 VARCHAR(50),@GMN2 VARCHAR(50),@GMN3 VARCHAR(50),@GMN4 VARCHAR(50))  AS"
sConsulta = sConsulta & vbCrLf & "DECLARE @VAR_COD VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "SET XACT_ABORT ON"
sConsulta = sConsulta & vbCrLf & "BEGIN TRANSACTION"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "DECLARE C CURSOR FOR SELECT COD FROM ART4 WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@GMN4 AND COD=@OLD"
sConsulta = sConsulta & vbCrLf & "OPEN C"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM C INTO @VAR_COD"
sConsulta = sConsulta & vbCrLf & "IF @@FETCH_STATUS=0"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4_ADJ NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4_ESP NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "UPDATE ART4 SET COD=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@GMN4 AND COD=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART4_ADJ SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@GMN4 AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@GMN4 AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@GMN4 AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@GMN4 AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON1 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND GMN3=@GMN3 AND GMN4=@GMN4 AND   ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON2 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND GMN3=@GMN3 AND GMN4=@GMN4 AND   ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON3 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND GMN3=@GMN3 AND GMN4=@GMN4 AND   ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON4 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND GMN3=@GMN3 AND GMN4=@GMN4 AND   ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY1 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND GMN3=@GMN3 AND GMN4=@GMN4 AND   ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY2 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND GMN3=@GMN3 AND GMN4=@GMN4 AND   ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY3 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND GMN3=@GMN3 AND GMN4=@GMN4 AND   ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY4 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND GMN3=@GMN3 AND GMN4=@GMN4 AND   ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_1 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND GMN3=@GMN3 AND GMN4=@GMN4 AND   ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_2 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND GMN3=@GMN3 AND GMN4=@GMN4 AND   ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_3 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND GMN3=@GMN3 AND GMN4=@GMN4 AND   ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_4 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND GMN3=@GMN3 AND GMN4=@GMN4 AND   ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_1 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND GMN3=@GMN3 AND GMN4=@GMN4 AND   ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_2 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND GMN3=@GMN3 AND GMN4=@GMN4 AND   ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_3 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND GMN3=@GMN3 AND GMN4=@GMN4 AND   ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_4 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND GMN3=@GMN3 AND GMN4=@GMN4 AND   ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_UON1 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@GMN4 AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_UON2 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@GMN4 AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_UON3 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@GMN4 AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_ART4 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@GMN4 AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART4_ESP SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@GMN4 AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4_ADJ CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4_ESP CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "Close C"
sConsulta = sConsulta & vbCrLf & "DEALLOCATE C"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "COMMIT TRANSACTION"

CrearProcedure_ART4_COD_2_8_05 = sConsulta

End Function


Private Function CrearProcedure_GMN1_COD_2_8_05() As String
Dim sConsulta As String

sConsulta = "CREATE PROCEDURE GMN1_COD (@OLD VARCHAR(50),@NEW VARCHAR(50))  AS"
sConsulta = sConsulta & vbCrLf & "DECLARE @VAR_COD VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "SET XACT_ABORT ON"
sConsulta = sConsulta & vbCrLf & "BEGIN TRANSACTION"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "DECLARE C CURSOR FOR SELECT COD FROM GMN1 WHERE COD=@OLD"
sConsulta = sConsulta & vbCrLf & "OPEN C"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM C INTO @VAR_COD"
sConsulta = sConsulta & vbCrLf & "IF @@FETCH_STATUS=0"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART1_ADJ NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART2_ADJ NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART3_ADJ NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4_ADJ NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE BLOQUEO_PROCE NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE COM_GMN1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE COM_GMN2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE COM_GMN3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE COM_GMN4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE DETALLE_PUJAS NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_ADJ NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_ADJREU NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_ESP NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_OBJ NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_OFE NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_OFE_WEB NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRES NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESPROY NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_UON1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_UON2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_UON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE OFE_MIN NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_PROCE NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_PROVE NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_PRESCON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_PRESCON4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_PROVE NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_MAT1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_MAT2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_MAT3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_MAT4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_ESP NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_OFE_ADJUN NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_OFE NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_OFE_WEB_ADJUN NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_OFE_WEB NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_PER NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_PROVE NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_PROVE_PET NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_PROVE_POT NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVENOTIF NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE REU_PROCE NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4_ESP NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN4_ACT NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "UPDATE GMN1 SET COD=@NEW WHERE COD=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART1 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART1_ADJ SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART2 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART2_ADJ SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART3 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART3_ADJ SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART4 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART4_ADJ SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE BLOQUEO_PROCE SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE COM_GMN1 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE COM_GMN2 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE COM_GMN3 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE COM_GMN4 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE DETALLE_PUJAS SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE GMN2 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE GMN3 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE GMN4 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_ADJ SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_ADJREU SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_ESP SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_OBJ SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_OFE SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_OFE_WEB SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_PRES SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_PRESCON SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_PRESCON3 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_PRESCON4 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_PRESPROY SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_UON1 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_UON2 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_UON3 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE OFEMIN SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_MAT1 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_MAT2 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_MAT3 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_MAT4 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_ESP SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_OFE_ADJUN SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_OFE SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_OFE_WEB_ADJUN SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_OFE_WEB SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_PER SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_PROVE SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_PROVE_PET SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_PROVE_POT SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_GMN1 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_GMN2 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_GMN3 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_GMN4 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROVENOTIF SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE REU_PROCE SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_PROCE SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_UON1 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_UON2 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_UON3 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON1 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON2 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON3 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON4 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY1 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY2 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY3 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY4 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_1 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_2 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_3 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_4 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_1 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_2 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_3 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_4 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_PRESCON3 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_PRESCON4 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_PROVE SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART4_ESP  SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE GMN4_ACT  SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART1_ADJ CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART2_ADJ CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART3_ADJ CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4_ADJ CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE BLOQUEO_PROCE CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE COM_GMN1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE COM_GMN2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE COM_GMN3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE COM_GMN4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE DETALLE_PUJAS CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_ADJ CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_ADJREU CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_ESP CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_OBJ CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_OFE CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_OFE_WEB CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRES CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESPROY CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_UON1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_UON2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_UON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE OFEMIN CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_MAT1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_MAT2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_MAT3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_MAT4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_ESP CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_OFE_ADJUN CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_OFE CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_OFE_WEB_ADJUN CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_OFE_WEB CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_PER CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_PROVE CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_PROVE_PET CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_PROVE_POT CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVENOTIF CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE REU_PROCE CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_PROCE CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_PRESCON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_PRESCON4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_PROVE CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_PROVE CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4_ESP CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN4_ACT CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "Close C"
sConsulta = sConsulta & vbCrLf & "DEALLOCATE C"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "COMMIT TRANSACTION"

CrearProcedure_GMN1_COD_2_8_05 = sConsulta

End Function

Private Function CrearProcedure_GMN2_COD_2_8_05() As String
Dim sConsulta As String

sConsulta = "CREATE PROCEDURE GMN2_COD (@OLD VARCHAR(50),@NEW VARCHAR(50),@GMN1 VARCHAR(50))  AS"
sConsulta = sConsulta & vbCrLf & "DECLARE @VAR_COD VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "SET XACT_ABORT ON"
sConsulta = sConsulta & vbCrLf & "BEGIN TRANSACTION"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "DECLARE C CURSOR FOR SELECT COD FROM GMN2 WHERE GMN1=@GMN1 AND COD=@OLD"
sConsulta = sConsulta & vbCrLf & "OPEN C"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM C INTO @VAR_COD"
sConsulta = sConsulta & vbCrLf & "IF @@FETCH_STATUS=0"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART2_ADJ NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART3_ADJ NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4_ADJ NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE COM_GMN2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE COM_GMN3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE COM_GMN4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_MAT2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_MAT3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_MAT4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_PROCE NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4_ESP NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN4_ACT NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "UPDATE GMN2 SET COD=@NEW WHERE GMN1=@GMN1 AND COD=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART2 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART2_ADJ SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART3 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART3_ADJ SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART4 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART4_ADJ SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE COM_GMN2 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE COM_GMN3 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE COM_GMN4 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE GMN3 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE GMN4 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_MAT2 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_MAT3 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_MAT4 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_GMN2 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_GMN3 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_GMN4 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_PROCE SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_UON1 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_UON2 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_UON3 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON1 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON2 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON3 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON4 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY1 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY2 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY3 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY4 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_1 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_2 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_3 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_4 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_1 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_2 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_3 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_4 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_ART2 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_ART3 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_ART4 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART4_ESP SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE GMN4_ACT SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART2_ADJ CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART3_ADJ CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4_ADJ CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE COM_GMN2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE COM_GMN3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE COM_GMN4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_MAT2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_MAT3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_MAT4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_PROCE CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4_ESP CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN4_ACT CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "Close C"
sConsulta = sConsulta & vbCrLf & "DEALLOCATE C"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "COMMIT TRANSACTION"

CrearProcedure_GMN2_COD_2_8_05 = sConsulta

End Function
Private Function CrearProcedure_GMN3_COD_2_8_05() As String
Dim sConsulta As String

sConsulta = "CREATE PROCEDURE GMN3_COD (@OLD VARCHAR(50),@NEW VARCHAR(50),@GMN1 VARCHAR(50),@GMN2 VARCHAR(50))  AS"
sConsulta = sConsulta & vbCrLf & "DECLARE @VAR_COD VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "SET XACT_ABORT ON"
sConsulta = sConsulta & vbCrLf & "BEGIN TRANSACTION"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "DECLARE C CURSOR FOR SELECT COD FROM GMN3 WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND COD=@OLD"
sConsulta = sConsulta & vbCrLf & "OPEN C"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM C INTO @VAR_COD"
sConsulta = sConsulta & vbCrLf & "IF @@FETCH_STATUS=0"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART3_ADJ NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4_ADJ NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE COM_GMN3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE COM_GMN4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_MAT3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_MAT4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_PROCE NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4_ESP NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN4_ACT NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "UPDATE GMN3 SET COD=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND COD=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART3 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART3_ADJ SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART4 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART4_ADJ SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE COM_GMN3 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE COM_GMN4 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE GMN4 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_MAT3 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_MAT4 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_GMN3 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_GMN4 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_PROCE SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_UON1 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_UON2 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_UON3 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON1 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON2 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON3 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON4 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY1 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY2 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY3 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY4 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_1 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_2 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_3 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_4 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_1 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_2 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_3 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_4 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_ART3 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_ART4 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART4_ESP SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE GMN4_ACT SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART3_ADJ CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4_ADJ CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE COM_GMN3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE COM_GMN4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_MAT3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_MAT4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_PROCE CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4_ESP CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN4_ACT CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "Close C"
sConsulta = sConsulta & vbCrLf & "DEALLOCATE C"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "COMMIT TRANSACTION"

CrearProcedure_GMN3_COD_2_8_05 = sConsulta

End Function

Private Function CrearProcedure_GMN4_COD_2_8_05() As String
Dim sConsulta As String

sConsulta = "CREATE PROCEDURE GMN4_COD (@OLD VARCHAR(50),@NEW VARCHAR(50),@GMN1 VARCHAR(50),@GMN2 VARCHAR(50),@GMN3 VARCHAR(50))  AS"
sConsulta = sConsulta & vbCrLf & "DECLARE @VAR_COD VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "SET XACT_ABORT ON"
sConsulta = sConsulta & vbCrLf & "BEGIN TRANSACTION"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "DECLARE C CURSOR FOR SELECT COD FROM GMN4 WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND COD=@OLD"
sConsulta = sConsulta & vbCrLf & "OPEN C"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM C INTO @VAR_COD"
sConsulta = sConsulta & vbCrLf & "IF @@FETCH_STATUS=0"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4_ADJ NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE COM_GMN4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_MAT4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_PROCE NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4_ESP NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN4_ACT NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "UPDATE GMN4 SET COD=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND COD=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART4 SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART4_ADJ SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE COM_GMN4 SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_MAT4 SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_GMN4 SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_PROCE SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_UON1 SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_UON2 SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_UON3 SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON1 SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON2 SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON3 SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON4 SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY1 SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY2 SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY3 SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY4 SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_1 SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_2 SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_3 SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_4 SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_1 SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_2 SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_3 SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_4 SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_ART4 SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART4_ESP  SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE GMN4_ACT  SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4_ADJ CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE COM_GMN4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_MAT4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_PROCE CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4_ESP CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN4_ACT CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "Close C"
sConsulta = sConsulta & vbCrLf & "DEALLOCATE C"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "COMMIT TRANSACTION"

CrearProcedure_GMN4_COD_2_8_05 = sConsulta

End Function

Private Function CrearProcedure_PAG_COD_2_8_05() As String
Dim sConsulta As String

sConsulta = "CREATE PROCEDURE PAG_COD (@OLD VARCHAR(50),@NEW VARCHAR(50))  AS"
sConsulta = sConsulta & vbCrLf & "DECLARE @VAR_COD VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "SET XACT_ABORT ON"
sConsulta = sConsulta & vbCrLf & "BEGIN TRANSACTION"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "DECLARE C CURSOR FOR SELECT COD FROM PAG  WHERE COD=@OLD"
sConsulta = sConsulta & vbCrLf & "OPEN C"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM C INTO @VAR_COD"
sConsulta = sConsulta & vbCrLf & "IF @@FETCH_STATUS=0"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PAG NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PARGEN_DEF NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "UPDATE PAG SET COD=@NEW WHERE COD=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM SET PAG=@NEW WHERE PAG=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES SET PAG=@NEW WHERE PAG=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_UON1 SET PAG=@NEW WHERE PAG=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_UON2 SET PAG=@NEW WHERE PAG=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_UON3 SET PAG=@NEW WHERE PAG=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM SET PAG=@NEW WHERE PAG=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PARGEN_DEF SET PAGDEF=@NEW WHERE PAGDEF=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON1 SET PAG=@NEW WHERE PAG=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON2 SET PAG=@NEW WHERE PAG=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON3 SET PAG=@NEW WHERE PAG=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON4 SET PAG=@NEW WHERE PAG=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY1 SET PAG=@NEW WHERE PAG=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY2 SET PAG=@NEW WHERE PAG=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY3 SET PAG=@NEW WHERE PAG=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY4 SET PAG=@NEW WHERE PAG=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_1 SET PAG=@NEW WHERE PAG=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_2 SET PAG=@NEW WHERE PAG=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_3 SET PAG=@NEW WHERE PAG=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_4 SET PAG=@NEW WHERE PAG=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_1 SET PAG=@NEW WHERE PAG=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_2 SET PAG=@NEW WHERE PAG=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_3 SET PAG=@NEW WHERE PAG=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_4 SET PAG=@NEW WHERE PAG=@OLD"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PAG CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PARGEN_DEF CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "Close C"
sConsulta = sConsulta & vbCrLf & "DEALLOCATE C"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "COMMIT TRANSACTION"

CrearProcedure_PAG_COD_2_8_05 = sConsulta

End Function
Private Function CrearProcedure_DEST_COD_2_8_05() As String
Dim sConsulta As String

sConsulta = "CREATE PROCEDURE DEST_COD (@OLD VARCHAR(50),@NEW VARCHAR(50))  AS"
sConsulta = sConsulta & vbCrLf & "DECLARE @VAR_COD VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "SET XACT_ABORT ON"
sConsulta = sConsulta & vbCrLf & "BEGIN TRANSACTION"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "DECLARE C CURSOR FOR SELECT COD FROM DEST WHERE COD=@OLD"
sConsulta = sConsulta & vbCrLf & "OPEN C"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM C INTO @VAR_COD"
sConsulta = sConsulta & vbCrLf & "IF @@FETCH_STATUS=0"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE DEST NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART1_ADJ NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART2_ADJ NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART3_ADJ NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4_ADJ NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PARGEN_DEF NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "IF (SELECT DESTDEF FROM PARGEN_DEF) = @OLD"
sConsulta = sConsulta & vbCrLf & "  UPDATE PARGEN_DEF SET DESTDEF=@NEW"
sConsulta = sConsulta & vbCrLf & "UPDATE DEST SET COD=@NEW WHERE COD=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM SET DEST=@NEW WHERE DEST=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART1_ADJ SET DEST=@NEW WHERE DEST=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART2_ADJ SET DEST=@NEW WHERE DEST=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART3_ADJ SET DEST=@NEW WHERE DEST=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART4_ADJ SET DEST=@NEW WHERE DEST=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PARGEN_DEF SET DESTDEF=@NEW WHERE DESTDEF=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM SET DEST=@NEW WHERE DEST=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES SET DEST=@NEW WHERE DEST=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_UON1 SET DEST=@NEW WHERE DEST=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_UON2 SET DEST=@NEW WHERE DEST=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_UON3 SET DEST=@NEW WHERE DEST=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON1 SET DEST=@NEW WHERE DEST=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON2 SET DEST=@NEW WHERE DEST=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON3 SET DEST=@NEW WHERE DEST=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON4 SET DEST=@NEW WHERE DEST=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY1 SET DEST=@NEW WHERE DEST=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY2 SET DEST=@NEW WHERE DEST=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY3 SET DEST=@NEW WHERE DEST=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY4 SET DEST=@NEW WHERE DEST=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_1 SET DEST=@NEW WHERE DEST=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_2 SET DEST=@NEW WHERE DEST=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_3 SET DEST=@NEW WHERE DEST=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_4 SET DEST=@NEW WHERE DEST=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_1 SET DEST=@NEW WHERE DEST=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_2 SET DEST=@NEW WHERE DEST=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_3 SET DEST=@NEW WHERE DEST=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_4 SET DEST=@NEW WHERE DEST=@OLD"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE DEST CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART1_ADJ CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART2_ADJ CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART3_ADJ CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4_ADJ CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PARGEN_DEF CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "Close C"
sConsulta = sConsulta & vbCrLf & "DEALLOCATE C"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "COMMIT TRANSACTION"

CrearProcedure_DEST_COD_2_8_05 = sConsulta

End Function
Private Function CrearProcedure_UNI_COD_2_8_05() As String
Dim sConsulta As String

sConsulta = "CREATE PROCEDURE  UNI_COD (@OLD VARCHAR(50),@NEW VARCHAR(50))  AS"
sConsulta = sConsulta & vbCrLf & "DECLARE @VAR_COD VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "SET XACT_ABORT ON"
sConsulta = sConsulta & vbCrLf & "BEGIN TRANSACTION"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "DECLARE C CURSOR FOR SELECT COD FROM UNI WHERE COD=@OLD"
sConsulta = sConsulta & vbCrLf & "OPEN C"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM C INTO @VAR_COD"
sConsulta = sConsulta & vbCrLf & "IF @@FETCH_STATUS=0"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE UNI NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART1_ADJ NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART2_ADJ NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART3_ADJ NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4_ADJ NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PARGEN_DEF NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "UPDATE UNI SET COD=@NEW WHERE COD=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART1 SET UNI=@NEW WHERE UNI=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART1_ADJ SET UNI=@NEW WHERE UNI=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART2 SET UNI=@NEW WHERE UNI=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART2_ADJ SET UNI=@NEW WHERE UNI=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART3 SET UNI=@NEW WHERE UNI=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART3_ADJ SET UNI=@NEW WHERE UNI=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART4 SET UNI=@NEW WHERE UNI=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART4_ADJ SET UNI=@NEW WHERE UNI=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM SET UNI=@NEW WHERE UNI=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PARGEN_DEF SET UNIDEF=@NEW WHERE UNIDEF=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM SET UNI=@NEW WHERE UNI=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES SET UNI=@NEW WHERE UNI=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_UON1 SET UNI=@NEW WHERE UNI=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_UON2 SET UNI=@NEW WHERE UNI=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_UON3 SET UNI=@NEW WHERE UNI=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON1 SET UNI=@NEW WHERE UNI=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON2 SET UNI=@NEW WHERE UNI=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON3 SET UNI=@NEW WHERE UNI=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON4 SET UNI=@NEW WHERE UNI=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY1 SET UNI=@NEW WHERE UNI=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY2 SET UNI=@NEW WHERE UNI=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY3 SET UNI=@NEW WHERE UNI=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY4 SET UNI=@NEW WHERE UNI=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_1 SET UNI=@NEW WHERE UNI=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_2 SET UNI=@NEW WHERE UNI=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_3 SET UNI=@NEW WHERE UNI=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_4 SET UNI=@NEW WHERE UNI=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_1 SET UNI=@NEW WHERE UNI=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_2 SET UNI=@NEW WHERE UNI=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_3 SET UNI=@NEW WHERE UNI=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_4 SET UNI=@NEW WHERE UNI=@OLD"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE UNI CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART1_ADJ CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART2_ADJ CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART3_ADJ CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4_ADJ CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PARGEN_DEF CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "Close C"
sConsulta = sConsulta & vbCrLf & "DEALLOCATE C"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "COMMIT TRANSACTION"

CrearProcedure_UNI_COD_2_8_05 = sConsulta

End Function

Private Function CrearProcedure_USU_COD_2_8_05() As String
Dim sConsulta As String

sConsulta = "CREATE PROCEDURE USU_COD (@OLD VARCHAR(50),@NEW VARCHAR(50),@PWD VARCHAR(512))  AS"
sConsulta = sConsulta & vbCrLf & "DECLARE @VAR_COD VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "SET XACT_ABORT ON"
sConsulta = sConsulta & vbCrLf & "BEGIN TRANSACTION"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "DECLARE C CURSOR FOR SELECT COD FROM USU WHERE COD=@OLD"
sConsulta = sConsulta & vbCrLf & "OPEN C"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM C INTO @VAR_COD"
sConsulta = sConsulta & vbCrLf & "IF @@FETCH_STATUS=0"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE USU NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE USU_ACC NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE USU_LIS NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE LOG NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "UPDATE USU SET COD=@NEW,PWD=@PWD WHERE COD=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE USU_ACC SET USU=@NEW WHERE USU=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE USU_LIS SET USU=@NEW WHERE USU=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE SET USUVAL=@NEW WHERE USUVAL=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE SET USUVALSELPROVE=@NEW WHERE USUVALSELPROVE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE SET USUVALCIERRE=@NEW WHERE USUVALCIERRE=@OLD"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE USU CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE USU_ACC CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE USU_LIS CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE LOG CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "Close C"
sConsulta = sConsulta & vbCrLf & "DEALLOCATE C"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "COMMIT TRANSACTION"

CrearProcedure_USU_COD_2_8_05 = sConsulta

End Function

Private Function CrearProcedure_PROCE_COD_2_8_05() As String
Dim sConsulta As String

sConsulta = "CREATE PROCEDURE PROCE_COD (@OLD SMALLINT,@NEW SMALLINT,@ANYO INT,@GMN1 VARCHAR (3))  AS"
sConsulta = sConsulta & vbCrLf & "DECLARE @VAR_COD SMALLINT"
sConsulta = sConsulta & vbCrLf & "DECLARE @AN AS INT"
sConsulta = sConsulta & vbCrLf & "DECLARE @GMN AS VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "SET XACT_ABORT ON"
sConsulta = sConsulta & vbCrLf & "BEGIN TRANSACTION"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "DECLARE D CURSOR FOR SELECT ANYO,GMN1,COD FROM PROCE WHERE ANYO=@ANYO AND COD=@OLD AND GMN1=@GMN1"
sConsulta = sConsulta & vbCrLf & "OPEN D"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM D INTO @AN,@GMN,@VAR_COD"
sConsulta = sConsulta & vbCrLf & "IF @@FETCH_STATUS=0"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART1_ADJ NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART2_ADJ NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART3_ADJ NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4_ADJ NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_ADJ NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_ADJREU NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_ESP NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_OBJ NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_OFE NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_OFE_WEB NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRES NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESPROY NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_UON1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_UON2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_UON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE OFEMIN NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE DETALLE_PUJAS NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_NOTIF NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_ESP NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_OFE_ADJUN NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_OFE NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_OFE_WEB_ADJUN NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_OFE_WEB NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_PER NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_PROVE NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_PROVE_PET NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_PROVE_POT NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE REU_PROCE NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE BLOQUEO_PROCE NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "UPDATE ART1_ADJ SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART2_ADJ SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART3_ADJ SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART4_ADJ SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_ADJ SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_ADJREU SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_ESP SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_OBJ SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_OFE SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_OFE_WEB SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_PRES SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_UON1 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_UON2 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_UON3 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_PRESCON SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_PRESCON3 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_PRESCON4 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_PRESPROY SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE OFEMIN SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE DETALLE_PUJAS SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROVENOTIF SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_ESP SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_OFE_ADJUN SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_OFE SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_OFE_WEB_ADJUN SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_OFE_WEB SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_PER SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_PROVE SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_PROVE_PET SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_PROVE_POT SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE REU_PROCE SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE SET COD=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE BLOQUEO_PROCE SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART1_ADJ CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART2_ADJ CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART3_ADJ CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4_ADJ CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_ADJ CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESPROY CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_ADJREU CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_ESP CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_OBJ CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_OFE CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_OFE_WEB CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRES CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_UON1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_UON2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_UON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE OFEMIN CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE DETALLE_PUJAS CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVENOTIF CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_ESP CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_OFE_ADJUN CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_OFE CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_OFE_WEB_ADJUN CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_OFE_WEB CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_PER CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_PROVE CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_PROVE_PET CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_PROVE_POT CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE REU_PROCE CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE BLOQUEO_PROCE CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "Close D"
sConsulta = sConsulta & vbCrLf & "DEALLOCATE D"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "COMMIT TRANSACTION"

CrearProcedure_PROCE_COD_2_8_05 = sConsulta

End Function

Private Function CrearProcedure_PROVE_COD_2_8_05() As String
Dim sConsulta As String

sConsulta = "CREATE PROCEDURE PROVE_COD (@OLD VARCHAR(50),@NEW VARCHAR(50))  AS"
sConsulta = sConsulta & vbCrLf & "DECLARE @VAR_COD VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "SET XACT_ABORT ON"
sConsulta = sConsulta & vbCrLf & "BEGIN TRANSACTION"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "DECLARE C CURSOR FOR SELECT COD FROM PROVE WHERE COD=@OLD"
sConsulta = sConsulta & vbCrLf & "OPEN C"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM C INTO @VAR_COD"
sConsulta = sConsulta & vbCrLf & "IF @@FETCH_STATUS=0"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE CON NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_PROVE NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART1_ADJ NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART2_ADJ NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART3_ADJ NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4_ADJ NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_ADJ NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_ADJREU NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_OFE NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_OFE_WEB NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE DETALLE_PUJAS NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE OFEMIN NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_OFE_ADJUN NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_OFE NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_OFE_WEB_ADJUN NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_OFE_WEB NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_PROVE NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_PROVE_PET NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_PROVE_POT NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_EQP NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVENOTIF NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE USU NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "UPDATE PROVE SET COD=@NEW WHERE COD=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE CON SET PROVE=@NEW WHERE PROVE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_PROVE SET PROVE=@NEW WHERE PROVE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART1_ADJ SET CODPROVE=@NEW WHERE CODPROVE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART2_ADJ SET CODPROVE=@NEW WHERE CODPROVE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART3_ADJ SET CODPROVE=@NEW WHERE CODPROVE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART4_ADJ SET CODPROVE=@NEW WHERE CODPROVE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM SET PROVEACT=@NEW WHERE PROVEACT=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_ADJ SET PROVE=@NEW WHERE PROVE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_ADJREU SET PROVE=@NEW WHERE PROVE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_OFE SET PROVE=@NEW WHERE PROVE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_OFE_WEB SET PROVE=@NEW WHERE PROVE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE DETALLE_PUJAS SET PROVE=@NEW WHERE PROVE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE OFEMIN SET PROVE=@NEW WHERE PROVE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_OFE_ADJUN SET PROVE=@NEW WHERE PROVE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_OFE SET PROVE=@NEW WHERE PROVE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_OFE_WEB_ADJUN SET PROVE=@NEW WHERE PROVE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_OFE_WEB SET PROVE=@NEW WHERE PROVE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_PROVE SET PROVE=@NEW WHERE PROVE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_PROVE_PET SET PROVE=@NEW WHERE PROVE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_PROVE_POT SET PROVE=@NEW WHERE PROVE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_EQP SET PROVE=@NEW WHERE PROVE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_GMN1 SET PROVE=@NEW WHERE PROVE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_GMN2 SET PROVE=@NEW WHERE PROVE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_GMN3 SET PROVE=@NEW WHERE PROVE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_GMN4 SET PROVE=@NEW WHERE PROVE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_ART1 SET PROVE=@NEW WHERE PROVE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_ART2 SET PROVE=@NEW WHERE PROVE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_ART3 SET PROVE=@NEW WHERE PROVE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_ART4 SET PROVE=@NEW WHERE PROVE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROVENOTIF SET PROVE=@NEW WHERE PROVE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE USU SET PROVE=@NEW WHERE PROVE=@OLD"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE CON CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_PROVE CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART1_ADJ CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART2_ADJ CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART3_ADJ CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4_ADJ CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_ADJ CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_ADJREU CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_OFE CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_OFE_WEB CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_OFE_ADJUN CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_OFE CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_OFE_WEB_ADJUN CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_OFE_WEB CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_PROVE CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_PROVE_PET CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_PROVE_POT CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_EQP CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE USU CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "Close C"
sConsulta = sConsulta & vbCrLf & "DEALLOCATE C"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "COMMIT TRANSACTION"

CrearProcedure_PROVE_COD_2_8_05 = sConsulta

End Function

Private Function CrearProcedure_PERF_COD_2_8_05() As String
Dim sConsulta As String

sConsulta = "CREATE PROCEDURE PERF_COD (@OLD VARCHAR(50),@NEW VARCHAR(50))  AS"
sConsulta = sConsulta & vbCrLf & "DECLARE @VAR_COD VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "SET XACT_ABORT ON"
sConsulta = sConsulta & vbCrLf & "BEGIN TRANSACTION"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "DECLARE C CURSOR FOR SELECT COD FROM PERF WHERE COD=@OLD"
sConsulta = sConsulta & vbCrLf & "OPEN C"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM C INTO @VAR_COD"
sConsulta = sConsulta & vbCrLf & "IF @@FETCH_STATUS=0"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PERF NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PERF_ACC NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PERF_LIS NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE USU NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "UPDATE PERF SET COD=@NEW WHERE COD=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PERF_ACC SET PERF=@NEW WHERE PERF=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PERF_LIS SET PERF=@NEW WHERE PERF=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE USU SET PERF=@NEW WHERE PERF=@OLD"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PERF CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PERF_ACC CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PERF_LIS CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE USU CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "Close C"
sConsulta = sConsulta & vbCrLf & "DEALLOCATE C"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "COMMIT TRANSACTION"

CrearProcedure_PERF_COD_2_8_05 = sConsulta

End Function

Private Function CrearProcedure_PRES_3_CON1_COD_2_8_05() As String
Dim sConsulta As String

sConsulta = "CREATE PROCEDURE PRES_3_CON1_COD (@OLD VARCHAR(50),@NEW VARCHAR(50))  AS"
sConsulta = sConsulta & vbCrLf & "DECLARE @VAR_COD VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "SET XACT_ABORT ON"
sConsulta = sConsulta & vbCrLf & "BEGIN TRANSACTION"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "DECLARE C CURSOR FOR SELECT COD FROM PRES_3_CON1 WHERE  COD=@OLD"
sConsulta = sConsulta & vbCrLf & "OPEN C"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM C INTO @VAR_COD"
sConsulta = sConsulta & vbCrLf & "IF @@FETCH_STATUS=0"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_3_CON1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_3_CON2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_3_CON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_3_CON4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_3_CON1 SET COD=@NEW WHERE  COD=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_3_CON2 SET PRES1=@NEW WHERE PRES1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_3_CON3 SET PRES1=@NEW WHERE  PRES1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_3_CON4 SET PRES1=@NEW WHERE  PRES1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_1 SET PRES1=@NEW WHERE  PRES1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_2 SET PRES1=@NEW WHERE  PRES1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_3 SET PRES1=@NEW WHERE  PRES1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_4 SET PRES1=@NEW WHERE  PRES1=@OLD"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_3_CON1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_3_CON2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_3_CON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_3_CON4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "Close C"
sConsulta = sConsulta & vbCrLf & "DEALLOCATE C"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "COMMIT TRANSACTION"

CrearProcedure_PRES_3_CON1_COD_2_8_05 = sConsulta

End Function

Private Function CrearProcedure_PRES_3_CON2_COD_2_8_05() As String
Dim sConsulta As String

sConsulta = "CREATE PROCEDURE PRES_3_CON2_COD (@OLD VARCHAR(50),@NEW VARCHAR(50),@PRES1 VARCHAR(50))  AS"
sConsulta = sConsulta & vbCrLf & "DECLARE @VAR_COD VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "SET XACT_ABORT ON"
sConsulta = sConsulta & vbCrLf & "BEGIN TRANSACTION"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "DECLARE C CURSOR FOR SELECT COD FROM PRES_3_CON2 WHERE  PRES1=@PRES1 AND COD=@OLD"
sConsulta = sConsulta & vbCrLf & "OPEN C"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM C INTO @VAR_COD"
sConsulta = sConsulta & vbCrLf & "IF @@FETCH_STATUS=0"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_3_CON2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_3_CON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_3_CON4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_3_CON2 SET COD=@NEW WHERE  PRES1=@PRES1 AND COD=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_3_CON3 SET PRES2=@NEW WHERE  PRES1=@PRES1 AND PRES2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_3_CON4 SET PRES2=@NEW WHERE  PRES1=@PRES1 AND PRES2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_PRESCON3 SET PRES2=@NEW WHERE PRES1=@PRES1 AND PRES2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_2 SET PRES2=@NEW WHERE  PRES1=@PRES1 AND PRES2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_3 SET PRES2=@NEW WHERE  PRES1=@PRES1 AND PRES2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_4 SET PRES2=@NEW WHERE  PRES1=@PRES1 AND PRES2=@OLD"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_3_CON2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_3_CON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_3_CON4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "Close C"
sConsulta = sConsulta & vbCrLf & "DEALLOCATE C"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "COMMIT TRANSACTION"

CrearProcedure_PRES_3_CON2_COD_2_8_05 = sConsulta

End Function

Private Function CrearProcedure_PRES_3_CON3_COD_2_8_05() As String
Dim sConsulta As String

sConsulta = "CREATE PROCEDURE PRES_3_CON3_COD (@OLD VARCHAR(50),@NEW VARCHAR(50),@PRES1 VARCHAR(50),@PRES2 VARCHAR(50))  AS"
sConsulta = sConsulta & vbCrLf & "DECLARE @VAR_COD VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "SET XACT_ABORT ON"
sConsulta = sConsulta & vbCrLf & "BEGIN TRANSACTION"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "DECLARE C CURSOR FOR SELECT COD FROM PRES_3_CON3 WHERE  PRES1=@PRES1 AND PRES2=@PRES2 AND COD=@OLD"
sConsulta = sConsulta & vbCrLf & "OPEN C"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM C INTO @VAR_COD"
sConsulta = sConsulta & vbCrLf & "IF @@FETCH_STATUS=0"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_3_CON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_3_CON4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_3_CON3 SET COD=@NEW WHERE  PRES1=@PRES1 AND PRES2=@PRES2 AND COD=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_3_CON4 SET PRES3=@NEW WHERE PRES1=@PRES1 AND PRES2=@PRES2 AND PRES3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_PRESCON3 SET PRES3=@NEW WHERE  PRES1=@PRES1 AND PRES2=@PRES2 AND PRES3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_3 SET PRES3=@NEW WHERE  PRES1=@PRES1 AND PRES2=@PRES2 AND PRES3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_4 SET PRES3=@NEW WHERE   PRES1=@PRES1 AND PRES2=@PRES2 AND PRES3=@OLD"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_3_CON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_3_CON4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "Close C"
sConsulta = sConsulta & vbCrLf & "DEALLOCATE C"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "COMMIT TRANSACTION"

CrearProcedure_PRES_3_CON3_COD_2_8_05 = sConsulta

End Function

Private Function CrearProcedure_PRES_3_CON4_COD_2_8_05() As String
Dim sConsulta As String

sConsulta = "CREATE PROCEDURE PRES_3_CON4_COD (@OLD VARCHAR(50),@NEW VARCHAR(50),@PRES1 VARCHAR(50),@PRES2 VARCHAR(50),@PRES3 VARCHAR(50))  AS"
sConsulta = sConsulta & vbCrLf & "DECLARE @VAR_COD VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "SET XACT_ABORT ON"
sConsulta = sConsulta & vbCrLf & "BEGIN TRANSACTION"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "DECLARE C CURSOR FOR SELECT COD FROM PRES_3_CON4 WHERE PRES1=@PRES1 AND PRES2=@PRES2 AND PRES3=@PRES3 AND COD=@OLD"
sConsulta = sConsulta & vbCrLf & "OPEN C"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM C INTO @VAR_COD"
sConsulta = sConsulta & vbCrLf & "IF @@FETCH_STATUS=0"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_3_CON4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_PRESCON3_4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_3_CON4 SET COD=@NEW WHERE PRES1=@PRES1 AND PRES2=@PRES2 AND PRES3=@PRES3 AND COD=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_PRESCON3 SET PRES4=@NEW WHERE PRES1=@PRES1 AND PRES2=@PRES2 AND PRES3=@PRES3 AND PRES4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_PRESCON3_4 SET PRES4=@NEW WHERE PRES1=@PRES1 AND PRES2=@PRES2 AND PRES3=@PRES3 AND PRES4=@OLD"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_3_CON4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_PRESCON3_4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "Close C"
sConsulta = sConsulta & vbCrLf & "DEALLOCATE C"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "COMMIT TRANSACTION"

CrearProcedure_PRES_3_CON4_COD_2_8_05 = sConsulta

End Function

Private Function CrearProcedure_PRES_4_CON1_COD_2_8_05() As String
Dim sConsulta As String

sConsulta = "CREATE PROCEDURE PRES_4_CON1_COD (@OLD VARCHAR(50),@NEW VARCHAR(50))  AS"
sConsulta = sConsulta & vbCrLf & "DECLARE @VAR_COD VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "SET XACT_ABORT ON"
sConsulta = sConsulta & vbCrLf & "BEGIN TRANSACTION"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "DECLARE C CURSOR FOR SELECT COD FROM PRES_4_CON1 WHERE  COD=@OLD"
sConsulta = sConsulta & vbCrLf & "OPEN C"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM C INTO @VAR_COD"
sConsulta = sConsulta & vbCrLf & "IF @@FETCH_STATUS=0"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_4_CON1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_4_CON2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_4_CON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_4_CON4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_4_CON1 SET COD=@NEW WHERE  COD=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_4_CON2 SET PRES1=@NEW WHERE PRES1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_4_CON3 SET PRES1=@NEW WHERE  PRES1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_4_CON4 SET PRES1=@NEW WHERE  PRES1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_PRESCON4 SET PRES1=@NEW WHERE  PRES1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_1 SET PRES1=@NEW WHERE  PRES1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_2 SET PRES1=@NEW WHERE  PRES1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_3 SET PRES1=@NEW WHERE  PRES1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_4 SET PRES1=@NEW WHERE  PRES1=@OLD"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_4_CON1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_4_CON2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_4_CON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_4_CON4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "Close C"
sConsulta = sConsulta & vbCrLf & "DEALLOCATE C"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "COMMIT TRANSACTION"

CrearProcedure_PRES_4_CON1_COD_2_8_05 = sConsulta

End Function

Private Function CrearProcedure_PRES_4_CON2_COD_2_8_05() As String
Dim sConsulta As String

sConsulta = "CREATE PROCEDURE PRES_4_CON2_COD (@OLD VARCHAR(50),@NEW VARCHAR(50),@PRES1 VARCHAR(50))  AS"
sConsulta = sConsulta & vbCrLf & "DECLARE @VAR_COD VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "SET XACT_ABORT ON"
sConsulta = sConsulta & vbCrLf & "BEGIN TRANSACTION"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "DECLARE C CURSOR FOR SELECT COD FROM PRES_4_CON2 WHERE  PRES1=@PRES1 AND COD=@OLD"
sConsulta = sConsulta & vbCrLf & "OPEN C"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM C INTO @VAR_COD"
sConsulta = sConsulta & vbCrLf & "IF @@FETCH_STATUS=0"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_4_CON2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_4_CON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_4_CON4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_4_CON2 SET COD=@NEW WHERE  PRES1=@PRES1 AND COD=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_4_CON3 SET PRES2=@NEW WHERE  PRES1=@PRES1 AND PRES2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_4_CON4 SET PRES2=@NEW WHERE  PRES1=@PRES1 AND PRES2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_PRESCON4 SET PRES2=@NEW WHERE PRES1=@PRES1 AND PRES2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_2 SET PRES2=@NEW WHERE  PRES1=@PRES1 AND PRES2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_3 SET PRES2=@NEW WHERE  PRES1=@PRES1 AND PRES2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_4 SET PRES2=@NEW WHERE  PRES1=@PRES1 AND PRES2=@OLD"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_4_CON2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_4_CON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_4_CON4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "Close C"
sConsulta = sConsulta & vbCrLf & "DEALLOCATE C"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "COMMIT TRANSACTION"

CrearProcedure_PRES_4_CON2_COD_2_8_05 = sConsulta

End Function

Private Function CrearProcedure_PRES_4_CON3_COD_2_8_05() As String
Dim sConsulta As String

sConsulta = "CREATE PROCEDURE PRES_4_CON3_COD (@OLD VARCHAR(50),@NEW VARCHAR(50),@PRES1 VARCHAR(50),@PRES2 VARCHAR(50))  AS"
sConsulta = sConsulta & vbCrLf & "DECLARE @VAR_COD VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "SET XACT_ABORT ON"
sConsulta = sConsulta & vbCrLf & "BEGIN TRANSACTION"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "DECLARE C CURSOR FOR SELECT COD FROM PRES_4_CON3 WHERE  PRES1=@PRES1 AND PRES2=@PRES2 AND COD=@OLD"
sConsulta = sConsulta & vbCrLf & "OPEN C"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM C INTO @VAR_COD"
sConsulta = sConsulta & vbCrLf & "IF @@FETCH_STATUS=0"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_4_CON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_4_CON4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_4_CON3 SET COD=@NEW WHERE  PRES1=@PRES1 AND PRES2=@PRES2 AND COD=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_4_CON4 SET PRES3=@NEW WHERE PRES1=@PRES1 AND PRES2=@PRES2 AND PRES3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_PRESCON4 SET PRES3=@NEW WHERE  PRES1=@PRES1 AND PRES2=@PRES2 AND PRES3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_3 SET PRES3=@NEW WHERE  PRES1=@PRES1 AND PRES2=@PRES2 AND PRES3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_4 SET PRES3=@NEW WHERE   PRES1=@PRES1 AND PRES2=@PRES2 AND PRES3=@OLD"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_4_CON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_4_CON4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "Close C"
sConsulta = sConsulta & vbCrLf & "DEALLOCATE C"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "COMMIT TRANSACTION"

CrearProcedure_PRES_4_CON3_COD_2_8_05 = sConsulta

End Function

Private Function CrearProcedure_PRES_4_CON4_COD_2_8_05() As String
Dim sConsulta As String

sConsulta = "CREATE PROCEDURE PRES_4_CON4_COD (@OLD VARCHAR(50),@NEW VARCHAR(50),@PRES1 VARCHAR(50),@PRES2 VARCHAR(50),@PRES3 VARCHAR(50))  AS"
sConsulta = sConsulta & vbCrLf & "DECLARE @VAR_COD VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "SET XACT_ABORT ON"
sConsulta = sConsulta & vbCrLf & "BEGIN TRANSACTION"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "DECLARE C CURSOR FOR SELECT COD FROM PRES_4_CON4 WHERE PRES1=@PRES1 AND PRES2=@PRES2 AND PRES3=@PRES3 AND COD=@OLD"
sConsulta = sConsulta & vbCrLf & "OPEN C"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM C INTO @VAR_COD"
sConsulta = sConsulta & vbCrLf & "IF @@FETCH_STATUS=0"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_4_CON4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_PRESCON4_4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_4_CON4 SET COD=@NEW WHERE PRES1=@PRES1 AND PRES2=@PRES2 AND PRES3=@PRES3 AND COD=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_PRESCON4 SET PRES4=@NEW WHERE PRES1=@PRES1 AND PRES2=@PRES2 AND PRES3=@PRES3 AND PRES4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_PRESCON4_4 SET PRES4=@NEW WHERE PRES1=@PRES1 AND PRES2=@PRES2 AND PRES3=@PRES3 AND PRES4=@OLD"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_4_CON4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_PRESCON4_4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "Close C"
sConsulta = sConsulta & vbCrLf & "DEALLOCATE C"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "COMMIT TRANSACTION"

CrearProcedure_PRES_4_CON4_COD_2_8_05 = sConsulta

End Function

Private Function Crear_SP_REBUILD_PUB_2_8_005() As String
Dim sConsulta As String

sConsulta = "CREATE PROCEDURE SP_REBUILD_PUB AS"
sConsulta = sConsulta & vbCrLf & ""
sConsulta = sConsulta & vbCrLf & "DECLARE @CONT AS INTEGER"
sConsulta = sConsulta & vbCrLf & "DECLARE @CONT_NUE AS INTEGER"
sConsulta = sConsulta & vbCrLf & "DECLARE @CONT_AREV AS INTEGER"
sConsulta = sConsulta & vbCrLf & ""
sConsulta = sConsulta & vbCrLf & "DECLARE @SERVER AS VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "DECLARE @BD AS VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & ""
sConsulta = sConsulta & vbCrLf & "DECLARE @PROVE_COD AS VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "DECLARE @PROVE_CODPORTAL AS VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & ""
sConsulta = sConsulta & vbCrLf & "DECLARE @COMP_CODPORTAL AS VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "DECLARE @COMP_IDPORTAL AS INTEGER"
sConsulta = sConsulta & vbCrLf & ""
sConsulta = sConsulta & vbCrLf & "DECLARE @QRY AS VARCHAR(100)"
sConsulta = sConsulta & vbCrLf & ""
sConsulta = sConsulta & vbCrLf & "DECLARE REBUILD_PROVE CURSOR FOR SELECT COD,FSP_COD FROM PROVE WHERE FSP_COD IS NOT NULL"
sConsulta = sConsulta & vbCrLf & ""
sConsulta = sConsulta & vbCrLf & "SET @SERVER=(SELECT FSP_SRV FROM PARGEN_PORT)"
sConsulta = sConsulta & vbCrLf & "SET @BD=(SELECT FSP_BD FROM PARGEN_PORT)"
sConsulta = sConsulta & vbCrLf & ""
sConsulta = sConsulta & vbCrLf & "SET @COMP_CODPORTAL = (SELECT FSP_CIA FROM PARGEN_PORT)"
sConsulta = sConsulta & vbCrLf & "SET @QRY=@SERVER + '.' + @BD + '.dbo.SP_SELECT_ID'"
sConsulta = sConsulta & vbCrLf & "EXEC @COMP_IDPORTAL=@QRY @COMP_CODPORTAL"
sConsulta = sConsulta & vbCrLf & ""
sConsulta = sConsulta & vbCrLf & "OPEN REBUILD_PROVE"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM REBUILD_PROVE INTO @PROVE_COD,@PROVE_CODPORTAL"
sConsulta = sConsulta & vbCrLf & "WHILE @@FETCH_STATUS=0"
sConsulta = sConsulta & vbCrLf & ""
sConsulta = sConsulta & vbCrLf & "    BEGIN"
sConsulta = sConsulta & vbCrLf & "        BEGIN DISTRIBUTED TRANSACTION"
sConsulta = sConsulta & vbCrLf & "            EXECUTE NUM_PROCE_PUB @PROVE_COD,@CONT OUTPUT,@CONT_NUE OUTPUT,@CONT_AREV OUTPUT"
sConsulta = sConsulta & vbCrLf & "            SET @QRY=@SERVER+'.'+@BD+'.dbo.SP_ACT_PROCE_PUB'"
sConsulta = sConsulta & vbCrLf & "            Print 'COMP_IDPORTAL='+CONVERT(VARCHAR(50),@COMP_IDPORTAL) + ', PROVE_CODPORTAL=' + @PROVE_CODPORTAL+ ': ' +CONVERT(VARCHAR(50),@CONT) + '-' + CONVERT(VARCHAR(50),@CONT_NUE) + '-' + CONVERT(VARCHAR(50),@CONT_AREV)"
sConsulta = sConsulta & vbCrLf & "            EXECUTE @QRY @COMP_IDPORTAL,@PROVE_CODPORTAL,@CONT,@CONT_NUE,@CONT_AREV"
sConsulta = sConsulta & vbCrLf & "        COMMIT TRANSACTION"
sConsulta = sConsulta & vbCrLf & "        FETCH NEXT FROM REBUILD_PROVE INTO @PROVE_COD,@PROVE_CODPORTAL"
sConsulta = sConsulta & vbCrLf & ""
sConsulta = sConsulta & vbCrLf & "    End"
sConsulta = sConsulta & vbCrLf & ""
sConsulta = sConsulta & vbCrLf & "Close REBUILD_PROVE"
sConsulta = sConsulta & vbCrLf & "DEALLOCATE REBUILD_PROVE"

Crear_SP_REBUILD_PUB_2_8_005 = sConsulta
End Function



Private Function ModificarCOM_COD_2_8_5_0() As String
Dim sConsulta As String
    
    sConsulta = "CREATE PROCEDURE COM_COD (@OLD VARCHAR(50),@NEW VARCHAR(50),@EQP VARCHAR(50))  AS"
    sConsulta = sConsulta & vbCrLf & "DECLARE @VAR_COD VARCHAR(50)"
    sConsulta = sConsulta & vbCrLf & "SET XACT_ABORT ON"
    sConsulta = sConsulta & vbCrLf & "BEGIN TRANSACTION"
    sConsulta = sConsulta & vbCrLf & "BEGIN"
    sConsulta = sConsulta & vbCrLf & "DECLARE C CURSOR FOR SELECT COD FROM COM WHERE COD=@OLD"
    sConsulta = sConsulta & vbCrLf & "OPEN C"
    sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM C INTO @VAR_COD"
    sConsulta = sConsulta & vbCrLf & "IF @@FETCH_STATUS=0"
    sConsulta = sConsulta & vbCrLf & "BEGIN"
    sConsulta = sConsulta & vbCrLf & "ALTER TABLE COM NOCHECK CONSTRAINT ALL"
    sConsulta = sConsulta & vbCrLf & "ALTER TABLE COM_GMN1 NOCHECK CONSTRAINT ALL"
    sConsulta = sConsulta & vbCrLf & "ALTER TABLE COM_GMN2 NOCHECK CONSTRAINT ALL"
    sConsulta = sConsulta & vbCrLf & "ALTER TABLE COM_GMN3 NOCHECK CONSTRAINT ALL"
    sConsulta = sConsulta & vbCrLf & "ALTER TABLE COM_GMN4 NOCHECK CONSTRAINT ALL"
    sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE NOCHECK CONSTRAINT ALL"
    sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_PROVE NOCHECK CONSTRAINT ALL"
    sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_PROVE_POT NOCHECK CONSTRAINT ALL"
    sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_PROCE NOCHECK CONSTRAINT ALL"
    sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_PROVE NOCHECK CONSTRAINT ALL"
    sConsulta = sConsulta & vbCrLf & "UPDATE COM SET COD=@NEW WHERE EQP=@EQP AND COD=@OLD"
    sConsulta = sConsulta & vbCrLf & "UPDATE COM_GMN1 SET COM=@NEW WHERE EQP=@EQP AND COM=@OLD"
    sConsulta = sConsulta & vbCrLf & "UPDATE COM_GMN2 SET COM=@NEW WHERE EQP=@EQP AND COM=@OLD"
    sConsulta = sConsulta & vbCrLf & "UPDATE COM_GMN3 SET COM=@NEW WHERE EQP=@EQP AND COM=@OLD"
    sConsulta = sConsulta & vbCrLf & "UPDATE COM_GMN4 SET COM=@NEW WHERE EQP=@EQP AND COM=@OLD"
    sConsulta = sConsulta & vbCrLf & "UPDATE PROCE SET COM=@NEW WHERE EQP=@EQP AND COM=@OLD"
    sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_PROVE SET COM=@NEW WHERE EQP=@EQP AND COM=@OLD"
    sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_PROVE_POT SET COM=@NEW WHERE EQP=@EQP AND COM=@OLD"
    sConsulta = sConsulta & vbCrLf & "UPDATE AR_PROCE SET COM=@NEW WHERE EQP=@EQP AND COM=@OLD"
    sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_PROVE SET COM=@NEW WHERE EQP=@EQP AND COM=@OLD"
    sConsulta = sConsulta & vbCrLf & "ALTER TABLE COM CHECK CONSTRAINT ALL"
    sConsulta = sConsulta & vbCrLf & "ALTER TABLE COM_GMN1 CHECK CONSTRAINT ALL"
    sConsulta = sConsulta & vbCrLf & "ALTER TABLE COM_GMN2 CHECK CONSTRAINT ALL"
    sConsulta = sConsulta & vbCrLf & "ALTER TABLE COM_GMN3 CHECK CONSTRAINT ALL"
    sConsulta = sConsulta & vbCrLf & "ALTER TABLE COM_GMN4 CHECK CONSTRAINT ALL"
    sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE CHECK CONSTRAINT ALL"
    sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_PROVE CHECK CONSTRAINT ALL"
    sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_PROVE_POT CHECK CONSTRAINT ALL"
    sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_PROCE CHECK CONSTRAINT ALL"
    sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_PROVE CHECK CONSTRAINT ALL"
    sConsulta = sConsulta & vbCrLf & "End"
    sConsulta = sConsulta & vbCrLf & "Close C"
    sConsulta = sConsulta & vbCrLf & "DEALLOCATE C"
    sConsulta = sConsulta & vbCrLf & "End"
    sConsulta = sConsulta & vbCrLf & "COMMIT TRANSACTION"
    
    ModificarCOM_COD_2_8_5_0 = sConsulta
End Function

Private Function CrearProcedure_ART1_COD_2_8_03() As String
Dim sConsulta As String

sConsulta = "CREATE PROCEDURE ART1_COD (@OLD VARCHAR(50),@NEW VARCHAR(50),@GMN1 VARCHAR(50))  AS"
sConsulta = sConsulta & vbCrLf & "DECLARE @VAR_COD VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "SET XACT_ABORT ON"
sConsulta = sConsulta & vbCrLf & "BEGIN TRANSACTION"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "DECLARE C CURSOR FOR SELECT COD FROM ART1 WHERE GMN1=@GMN1 AND COD=@OLD"
sConsulta = sConsulta & vbCrLf & "OPEN C"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM C INTO @VAR_COD"
sConsulta = sConsulta & vbCrLf & "IF @@FETCH_STATUS=0"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART1_ADJ NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_NES_UON1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_PRESCON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_PRESCON4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "UPDATE ART1 SET COD=@NEW WHERE GMN1=@GMN1 AND COD=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART1_ADJ SET ART=@NEW WHERE GMN1=@GMN1 AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM SET ART=@NEW WHERE GMN1=@GMN1 AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM SET ART=@NEW WHERE GMN1=@GMN1 AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES SET ART=@NEW WHERE GMN1=@GMN1 AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON1 SET ART=@NEW WHERE GMN1=@GMN1 AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON2 SET ART=@NEW WHERE GMN1=@GMN1 AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON3 SET ART=@NEW WHERE GMN1=@GMN1 AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON4 SET ART=@NEW WHERE GMN1=@GMN1 AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY1 SET ART=@NEW WHERE GMN1=@GMN1 AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY2 SET ART=@NEW WHERE GMN1=@GMN1 AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY3 SET ART=@NEW WHERE GMN1=@GMN1 AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY4 SET ART=@NEW WHERE GMN1=@GMN1 AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_1 SET ART=@NEW WHERE GMN1=@GMN1 AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_2 SET ART=@NEW WHERE GMN1=@GMN1 AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_3 SET ART=@NEW WHERE GMN1=@GMN1 AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_4 SET ART=@NEW WHERE GMN1=@GMN1 AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_1 SET ART=@NEW WHERE GMN1=@GMN1 AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_2 SET ART=@NEW WHERE GMN1=@GMN1 AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_3 SET ART=@NEW WHERE GMN1=@GMN1 AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_4 SET ART=@NEW WHERE GMN1=@GMN1 AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_UON1 SET ART=@NEW WHERE GMN1=@GMN1 AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_UON2 SET ART=@NEW WHERE GMN1=@GMN1 AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_UON3 SET ART=@NEW WHERE GMN1=@GMN1 AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_PRESCON3 SET ART=@NEW WHERE GMN1=@GMN1 AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_PRESCON4 SET ART=@NEW WHERE GMN1=@GMN1 AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_ART1 SET ART=@NEW WHERE GMN1=@GMN1 AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART1_ADJ CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_PRESCON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_PRESCON4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "Close C"
sConsulta = sConsulta & vbCrLf & "DEALLOCATE C"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "COMMIT TRANSACTION"

CrearProcedure_ART1_COD_2_8_03 = sConsulta

End Function

Private Function CrearProcedure_ART2_COD_2_8_03() As String
Dim sConsulta As String

sConsulta = "CREATE PROCEDURE ART2_COD (@OLD VARCHAR(50),@NEW VARCHAR(50),@GMN1 VARCHAR(50),@GMN2 VARCHAR(50))  AS"
sConsulta = sConsulta & vbCrLf & "DECLARE @VAR_COD VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "SET XACT_ABORT ON"
sConsulta = sConsulta & vbCrLf & "BEGIN TRANSACTION"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "DECLARE C CURSOR FOR SELECT COD FROM ART2 WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND COD=@OLD"
sConsulta = sConsulta & vbCrLf & "OPEN C"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM C INTO @VAR_COD"
sConsulta = sConsulta & vbCrLf & "IF @@FETCH_STATUS=0"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART2_ADJ NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_NES_UON1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "UPDATE ART2 SET COD=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND COD=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART2_ADJ SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON1 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON2 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON3 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON4 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY1 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY2 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY3 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY4 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_1 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_2 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_3 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_4 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_1 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_2 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_3 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_4 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_UON1 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_UON2 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_UON3 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_ART2 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART2_ADJ CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "Close C"
sConsulta = sConsulta & vbCrLf & "DEALLOCATE C"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "COMMIT TRANSACTION"

CrearProcedure_ART2_COD_2_8_03 = sConsulta

End Function
Private Function CrearProcedure_ART3_COD_2_8_03() As String
Dim sConsulta As String

sConsulta = "CREATE PROCEDURE ART3_COD (@OLD VARCHAR(50),@NEW VARCHAR(50),@GMN1 VARCHAR(50),@GMN2 VARCHAR(50),@GMN3 VARCHAR(50))  AS"
sConsulta = sConsulta & vbCrLf & "DECLARE @VAR_COD VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "SET XACT_ABORT ON"
sConsulta = sConsulta & vbCrLf & "BEGIN TRANSACTION"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "DECLARE C CURSOR FOR SELECT COD FROM ART3 WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND COD=@OLD"
sConsulta = sConsulta & vbCrLf & "OPEN C"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM C INTO @VAR_COD"
sConsulta = sConsulta & vbCrLf & "IF @@FETCH_STATUS=0"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART3_ADJ NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "UPDATE ART3 SET COD=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND COD=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART3_ADJ SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON1 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND GMN3=@GMN3 AND  ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON2 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND GMN3=@GMN3 AND  ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON3 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND GMN3=@GMN3 AND  ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON4 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND GMN3=@GMN3 AND  ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY1 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND GMN3=@GMN3 AND  ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY2 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND GMN3=@GMN3 AND  ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY3 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND GMN3=@GMN3 AND  ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY4 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND GMN3=@GMN3 AND  ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_1 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND GMN3=@GMN3 AND  ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_2 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND GMN3=@GMN3 AND  ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_3 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND GMN3=@GMN3 AND  ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_4 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND GMN3=@GMN3 AND  ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_1 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND GMN3=@GMN3 AND  ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_2 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND GMN3=@GMN3 AND  ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_3 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND GMN3=@GMN3 AND  ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_4 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND GMN3=@GMN3 AND  ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_UON1 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_UON2 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_UON3 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_ART3 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART3_ADJ CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "Close C"
sConsulta = sConsulta & vbCrLf & "DEALLOCATE C"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "COMMIT TRANSACTION"

CrearProcedure_ART3_COD_2_8_03 = sConsulta

End Function

Private Function CrearProcedure_ART4_COD_2_8_03() As String
Dim sConsulta As String

sConsulta = "CREATE PROCEDURE ART4_COD (@OLD VARCHAR(50),@NEW VARCHAR(50),@GMN1 VARCHAR(50),@GMN2 VARCHAR(50),@GMN3 VARCHAR(50),@GMN4 VARCHAR(50))  AS"
sConsulta = sConsulta & vbCrLf & "DECLARE @VAR_COD VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "SET XACT_ABORT ON"
sConsulta = sConsulta & vbCrLf & "BEGIN TRANSACTION"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "DECLARE C CURSOR FOR SELECT COD FROM ART4 WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@GMN4 AND COD=@OLD"
sConsulta = sConsulta & vbCrLf & "OPEN C"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM C INTO @VAR_COD"
sConsulta = sConsulta & vbCrLf & "IF @@FETCH_STATUS=0"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4_ADJ NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4_ESP NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "UPDATE ART4 SET COD=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@GMN4 AND COD=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART4_ADJ SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@GMN4 AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@GMN4 AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@GMN4 AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@GMN4 AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON1 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND GMN3=@GMN3 AND GMN4=@GMN4 AND   ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON2 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND GMN3=@GMN3 AND GMN4=@GMN4 AND   ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON3 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND GMN3=@GMN3 AND GMN4=@GMN4 AND   ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON4 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND GMN3=@GMN3 AND GMN4=@GMN4 AND   ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY1 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND GMN3=@GMN3 AND GMN4=@GMN4 AND   ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY2 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND GMN3=@GMN3 AND GMN4=@GMN4 AND   ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY3 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND GMN3=@GMN3 AND GMN4=@GMN4 AND   ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY4 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND GMN3=@GMN3 AND GMN4=@GMN4 AND   ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_1 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND GMN3=@GMN3 AND GMN4=@GMN4 AND   ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_2 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND GMN3=@GMN3 AND GMN4=@GMN4 AND   ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_3 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND GMN3=@GMN3 AND GMN4=@GMN4 AND   ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_4 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND GMN3=@GMN3 AND GMN4=@GMN4 AND   ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_1 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND GMN3=@GMN3 AND GMN4=@GMN4 AND   ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_2 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND GMN3=@GMN3 AND GMN4=@GMN4 AND   ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_3 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND GMN3=@GMN3 AND GMN4=@GMN4 AND   ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_4 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND GMN3=@GMN3 AND GMN4=@GMN4 AND   ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_UON1 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@GMN4 AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_UON2 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@GMN4 AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_UON3 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@GMN4 AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_ART4 SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@GMN4 AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART4_ESP SET ART=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@GMN4 AND ART=@OLD"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4_ADJ CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4_ESP CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "Close C"
sConsulta = sConsulta & vbCrLf & "DEALLOCATE C"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "COMMIT TRANSACTION"

CrearProcedure_ART4_COD_2_8_03 = sConsulta

End Function

Private Function CrearProcedure_GMN1_COD_2_8_03() As String
Dim sConsulta As String

sConsulta = "CREATE PROCEDURE GMN1_COD (@OLD VARCHAR(50),@NEW VARCHAR(50))  AS"
sConsulta = sConsulta & vbCrLf & "DECLARE @VAR_COD VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "SET XACT_ABORT ON"
sConsulta = sConsulta & vbCrLf & "BEGIN TRANSACTION"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "DECLARE C CURSOR FOR SELECT COD FROM GMN1 WHERE COD=@OLD"
sConsulta = sConsulta & vbCrLf & "OPEN C"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM C INTO @VAR_COD"
sConsulta = sConsulta & vbCrLf & "IF @@FETCH_STATUS=0"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART1_ADJ NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART2_ADJ NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART3_ADJ NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4_ADJ NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE BLOQUEO_PROCE NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE COM_GMN1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE COM_GMN2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE COM_GMN3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE COM_GMN4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE DETALLE_PUJAS NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_ADJ NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_ADJREU NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_ESP NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_OBJ NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_OFE NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_OFE_WEB NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRES NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESPROY NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_UON1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_UON2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_UON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE OFE_MIN NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_PROCE NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_PROVE NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_PRESCON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_PRESCON4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_PROVE NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_MAT1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_MAT2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_MAT3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_MAT4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_ESP NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_OFE_ADJUN NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_OFE NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_OFE_WEB_ADJUN NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_OFE_WEB NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_PER NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_PROVE NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_PROVE_PET NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_PROVE_POT NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVENOTIF NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE REU_PROCE NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4_ESP NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN4_ACT NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "UPDATE GMN1 SET COD=@NEW WHERE COD=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART1 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART1_ADJ SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART2 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART2_ADJ SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART3 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART3_ADJ SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART4 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART4_ADJ SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE BLOQUEO_PROCE SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE COM_GMN1 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE COM_GMN2 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE COM_GMN3 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE COM_GMN4 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE DETALLE_PUJAS SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE GMN2 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE GMN3 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE GMN4 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_ADJ SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_ADJREU SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_ESP SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_OBJ SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_OFE SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_OFE_WEB SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_PRES SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_PRESCON SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_PRESCON3 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_PRESCON4 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_PRESPROY SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_UON1 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_UON2 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_UON3 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE OFEMIN SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_MAT1 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_MAT2 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_MAT3 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_MAT4 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_ESP SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_OFE_ADJUN SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_OFE SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_OFE_WEB_ADJUN SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_OFE_WEB SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_PER SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_PROVE SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_PROVE_PET SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_PROVE_POT SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_GMN1 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_GMN2 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_GMN3 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_GMN4 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROVENOTIF SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE REU_PROCE SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_PROCE SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_UON1 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_UON2 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_UON3 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON1 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON2 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON3 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON4 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY1 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY2 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY3 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY4 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_1 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_2 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_3 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_4 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_1 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_2 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_3 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_4 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_PRESCON3 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_PRESCON4 SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_PROVE SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART4_ESP  SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE GMN4_ACT  SET GMN1=@NEW WHERE GMN1=@OLD"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART1_ADJ CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART2_ADJ CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART3_ADJ CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4_ADJ CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE BLOQUEO_PROCE CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE COM_GMN1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE COM_GMN2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE COM_GMN3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE COM_GMN4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE DETALLE_PUJAS CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_ADJ CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_ADJREU CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_ESP CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_OBJ CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_OFE CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_OFE_WEB CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRES CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESPROY CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_UON1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_UON2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_UON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE OFEMIN CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_MAT1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_MAT2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_MAT3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_MAT4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_ESP CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_OFE_ADJUN CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_OFE CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_OFE_WEB_ADJUN CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_OFE_WEB CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_PER CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_PROVE CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_PROVE_PET CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_PROVE_POT CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVENOTIF CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE REU_PROCE CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_PROCE CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_PRESCON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_PRESCON4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_PROVE CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_PROVE CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4_ESP CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN4_ACT CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "Close C"
sConsulta = sConsulta & vbCrLf & "DEALLOCATE C"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "COMMIT TRANSACTION"

CrearProcedure_GMN1_COD_2_8_03 = sConsulta

End Function

Private Function CrearProcedure_GMN2_COD_2_8_03() As String
Dim sConsulta As String

sConsulta = "CREATE PROCEDURE GMN2_COD (@OLD VARCHAR(50),@NEW VARCHAR(50),@GMN1 VARCHAR(50))  AS"
sConsulta = sConsulta & vbCrLf & "DECLARE @VAR_COD VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "SET XACT_ABORT ON"
sConsulta = sConsulta & vbCrLf & "BEGIN TRANSACTION"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "DECLARE C CURSOR FOR SELECT COD FROM GMN2 WHERE GMN1=@GMN1 AND COD=@OLD"
sConsulta = sConsulta & vbCrLf & "OPEN C"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM C INTO @VAR_COD"
sConsulta = sConsulta & vbCrLf & "IF @@FETCH_STATUS=0"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART2_ADJ NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART3_ADJ NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4_ADJ NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE COM_GMN2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE COM_GMN3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE COM_GMN4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_MAT2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_MAT3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_MAT4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_PROCE NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4_ESP NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN4_ACT NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "UPDATE GMN2 SET COD=@NEW WHERE GMN1=@GMN1 AND COD=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART2 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART2_ADJ SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART3 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART3_ADJ SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART4 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART4_ADJ SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE COM_GMN2 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE COM_GMN3 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE COM_GMN4 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE GMN3 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE GMN4 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_MAT2 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_MAT3 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_MAT4 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_GMN2 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_GMN3 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_GMN4 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_PROCE SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_UON1 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_UON2 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_UON3 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON1 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON2 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON3 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON4 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY1 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY2 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY3 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY4 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_1 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_2 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_3 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_4 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_1 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_2 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_3 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_4 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_ART2 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_ART3 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_ART4 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART4_ESP SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE GMN4_ACT SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART2_ADJ CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART3_ADJ CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4_ADJ CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE COM_GMN2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE COM_GMN3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE COM_GMN4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_MAT2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_MAT3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_MAT4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_PROCE CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4_ESP CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN4_ACT CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "Close C"
sConsulta = sConsulta & vbCrLf & "DEALLOCATE C"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "COMMIT TRANSACTION"

CrearProcedure_GMN2_COD_2_8_03 = sConsulta

End Function
Private Function CrearProcedure_GMN3_COD_2_8_03() As String
Dim sConsulta As String

sConsulta = "CREATE PROCEDURE GMN3_COD (@OLD VARCHAR(50),@NEW VARCHAR(50),@GMN1 VARCHAR(50),@GMN2 VARCHAR(50))  AS"
sConsulta = sConsulta & vbCrLf & "DECLARE @VAR_COD VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "SET XACT_ABORT ON"
sConsulta = sConsulta & vbCrLf & "BEGIN TRANSACTION"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "DECLARE C CURSOR FOR SELECT COD FROM GMN3 WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND COD=@OLD"
sConsulta = sConsulta & vbCrLf & "OPEN C"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM C INTO @VAR_COD"
sConsulta = sConsulta & vbCrLf & "IF @@FETCH_STATUS=0"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART3_ADJ NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4_ADJ NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE COM_GMN3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE COM_GMN4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_MAT3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_MAT4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_PROCE NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4_ESP NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN4_ACT NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "UPDATE GMN3 SET COD=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND COD=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART3 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART3_ADJ SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART4 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART4_ADJ SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE COM_GMN3 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE COM_GMN4 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE GMN4 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_MAT3 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_MAT4 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_GMN3 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_GMN4 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_PROCE SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_UON1 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_UON2 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_UON3 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON1 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON2 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON3 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON4 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY1 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY2 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY3 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY4 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_1 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_2 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_3 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_4 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_1 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_2 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_3 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_4 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_ART3 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_ART4 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART4_ESP SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE GMN4_ACT SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART3_ADJ CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4_ADJ CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE COM_GMN3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE COM_GMN4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_MAT3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_MAT4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_PROCE CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4_ESP CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN4_ACT CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "Close C"
sConsulta = sConsulta & vbCrLf & "DEALLOCATE C"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "COMMIT TRANSACTION"

CrearProcedure_GMN3_COD_2_8_03 = sConsulta

End Function

Private Function CrearProcedure_GMN4_COD_2_8_03() As String
Dim sConsulta As String

sConsulta = "CREATE PROCEDURE GMN4_COD (@OLD VARCHAR(50),@NEW VARCHAR(50),@GMN1 VARCHAR(50),@GMN2 VARCHAR(50),@GMN3 VARCHAR(50))  AS"
sConsulta = sConsulta & vbCrLf & "DECLARE @VAR_COD VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "SET XACT_ABORT ON"
sConsulta = sConsulta & vbCrLf & "BEGIN TRANSACTION"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "DECLARE C CURSOR FOR SELECT COD FROM GMN4 WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND COD=@OLD"
sConsulta = sConsulta & vbCrLf & "OPEN C"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM C INTO @VAR_COD"
sConsulta = sConsulta & vbCrLf & "IF @@FETCH_STATUS=0"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4_ADJ NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE COM_GMN4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_MAT4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_PROCE NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4_ESP NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN4_ACT NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "UPDATE GMN4 SET COD=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND COD=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART4 SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART4_ADJ SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE COM_GMN4 SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_MAT4 SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_GMN4 SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_PROCE SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_UON1 SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_UON2 SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_UON3 SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON1 SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON2 SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON3 SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON4 SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY1 SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY2 SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY3 SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY4 SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_1 SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_2 SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_3 SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_4 SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_1 SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_2 SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_3 SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_4 SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_ART4 SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART4_ESP  SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE GMN4_ACT  SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4_ADJ CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE COM_GMN4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_MAT4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_PROCE CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4_ESP CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE GMN4_ACT CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "Close C"
sConsulta = sConsulta & vbCrLf & "DEALLOCATE C"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "COMMIT TRANSACTION"

CrearProcedure_GMN4_COD_2_8_03 = sConsulta

End Function

Private Function CrearProcedure_PAG_COD_2_8_03() As String
Dim sConsulta As String

sConsulta = "CREATE PROCEDURE PAG_COD (@OLD VARCHAR(50),@NEW VARCHAR(50))  AS"
sConsulta = sConsulta & vbCrLf & "DECLARE @VAR_COD VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "SET XACT_ABORT ON"
sConsulta = sConsulta & vbCrLf & "BEGIN TRANSACTION"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "DECLARE C CURSOR FOR SELECT COD FROM PAG  WHERE COD=@OLD"
sConsulta = sConsulta & vbCrLf & "OPEN C"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM C INTO @VAR_COD"
sConsulta = sConsulta & vbCrLf & "IF @@FETCH_STATUS=0"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PAG NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PARGEN_DEF NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "UPDATE PAG SET COD=@NEW WHERE COD=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM SET PAG=@NEW WHERE PAG=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES SET PAG=@NEW WHERE PAG=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_UON1 SET PAG=@NEW WHERE PAG=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_UON2 SET PAG=@NEW WHERE PAG=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_UON3 SET PAG=@NEW WHERE PAG=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM SET PAG=@NEW WHERE PAG=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PARGEN_DEF SET PAGDEF=@NEW WHERE PAGDEF=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON1 SET PAG=@NEW WHERE PAG=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON2 SET PAG=@NEW WHERE PAG=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON3 SET PAG=@NEW WHERE PAG=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON4 SET PAG=@NEW WHERE PAG=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY1 SET PAG=@NEW WHERE PAG=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY2 SET PAG=@NEW WHERE PAG=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY3 SET PAG=@NEW WHERE PAG=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY4 SET PAG=@NEW WHERE PAG=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_1 SET PAG=@NEW WHERE PAG=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_2 SET PAG=@NEW WHERE PAG=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_3 SET PAG=@NEW WHERE PAG=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_4 SET PAG=@NEW WHERE PAG=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_1 SET PAG=@NEW WHERE PAG=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_2 SET PAG=@NEW WHERE PAG=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_3 SET PAG=@NEW WHERE PAG=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_4 SET PAG=@NEW WHERE PAG=@OLD"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PAG CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PARGEN_DEF CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "Close C"
sConsulta = sConsulta & vbCrLf & "DEALLOCATE C"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "COMMIT TRANSACTION"

CrearProcedure_PAG_COD_2_8_03 = sConsulta

End Function
Private Function CrearProcedure_DEST_COD_2_8_03() As String
Dim sConsulta As String

sConsulta = "CREATE PROCEDURE DEST_COD (@OLD VARCHAR(50),@NEW VARCHAR(50))  AS"
sConsulta = sConsulta & vbCrLf & "DECLARE @VAR_COD VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "SET XACT_ABORT ON"
sConsulta = sConsulta & vbCrLf & "BEGIN TRANSACTION"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "DECLARE C CURSOR FOR SELECT COD FROM DEST WHERE COD=@OLD"
sConsulta = sConsulta & vbCrLf & "OPEN C"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM C INTO @VAR_COD"
sConsulta = sConsulta & vbCrLf & "IF @@FETCH_STATUS=0"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE DEST NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART1_ADJ NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART2_ADJ NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART3_ADJ NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4_ADJ NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PARGEN_DEF NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "IF (SELECT DESTDEF FROM PARGEN_DEF) = @OLD"
sConsulta = sConsulta & vbCrLf & "  UPDATE PARGEN_DEF SET DESTDEF=@NEW"
sConsulta = sConsulta & vbCrLf & "UPDATE DEST SET COD=@NEW WHERE COD=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM SET DEST=@NEW WHERE DEST=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART1_ADJ SET DEST=@NEW WHERE DEST=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART2_ADJ SET DEST=@NEW WHERE DEST=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART3_ADJ SET DEST=@NEW WHERE DEST=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART4_ADJ SET DEST=@NEW WHERE DEST=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PARGEN_DEF SET DESTDEF=@NEW WHERE DESTDEF=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM SET DEST=@NEW WHERE DEST=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES SET DEST=@NEW WHERE DEST=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_UON1 SET DEST=@NEW WHERE DEST=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_UON2 SET DEST=@NEW WHERE DEST=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_UON3 SET DEST=@NEW WHERE DEST=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON1 SET DEST=@NEW WHERE DEST=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON2 SET DEST=@NEW WHERE DEST=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON3 SET DEST=@NEW WHERE DEST=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON4 SET DEST=@NEW WHERE DEST=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY1 SET DEST=@NEW WHERE DEST=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY2 SET DEST=@NEW WHERE DEST=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY3 SET DEST=@NEW WHERE DEST=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY4 SET DEST=@NEW WHERE DEST=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_1 SET DEST=@NEW WHERE DEST=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_2 SET DEST=@NEW WHERE DEST=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_3 SET DEST=@NEW WHERE DEST=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_4 SET DEST=@NEW WHERE DEST=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_1 SET DEST=@NEW WHERE DEST=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_2 SET DEST=@NEW WHERE DEST=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_3 SET DEST=@NEW WHERE DEST=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_4 SET DEST=@NEW WHERE DEST=@OLD"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE DEST CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART1_ADJ CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART2_ADJ CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART3_ADJ CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4_ADJ CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PARGEN_DEF CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "Close C"
sConsulta = sConsulta & vbCrLf & "DEALLOCATE C"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "COMMIT TRANSACTION"

CrearProcedure_DEST_COD_2_8_03 = sConsulta

End Function
Private Function CrearProcedure_UNI_COD_2_8_03() As String
Dim sConsulta As String

sConsulta = "CREATE PROCEDURE  UNI_COD (@OLD VARCHAR(50),@NEW VARCHAR(50))  AS"
sConsulta = sConsulta & vbCrLf & "DECLARE @VAR_COD VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "SET XACT_ABORT ON"
sConsulta = sConsulta & vbCrLf & "BEGIN TRANSACTION"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "DECLARE C CURSOR FOR SELECT COD FROM UNI WHERE COD=@OLD"
sConsulta = sConsulta & vbCrLf & "OPEN C"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM C INTO @VAR_COD"
sConsulta = sConsulta & vbCrLf & "IF @@FETCH_STATUS=0"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE UNI NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART1_ADJ NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART2_ADJ NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART3_ADJ NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4_ADJ NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PARGEN_DEF NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "UPDATE UNI SET COD=@NEW WHERE COD=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART1 SET UNI=@NEW WHERE UNI=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART1_ADJ SET UNI=@NEW WHERE UNI=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART2 SET UNI=@NEW WHERE UNI=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART2_ADJ SET UNI=@NEW WHERE UNI=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART3 SET UNI=@NEW WHERE UNI=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART3_ADJ SET UNI=@NEW WHERE UNI=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART4 SET UNI=@NEW WHERE UNI=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART4_ADJ SET UNI=@NEW WHERE UNI=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM SET UNI=@NEW WHERE UNI=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PARGEN_DEF SET UNIDEF=@NEW WHERE UNIDEF=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM SET UNI=@NEW WHERE UNI=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES SET UNI=@NEW WHERE UNI=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_UON1 SET UNI=@NEW WHERE UNI=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_UON2 SET UNI=@NEW WHERE UNI=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_UON3 SET UNI=@NEW WHERE UNI=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON1 SET UNI=@NEW WHERE UNI=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON2 SET UNI=@NEW WHERE UNI=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON3 SET UNI=@NEW WHERE UNI=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARCON4 SET UNI=@NEW WHERE UNI=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY1 SET UNI=@NEW WHERE UNI=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY2 SET UNI=@NEW WHERE UNI=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY3 SET UNI=@NEW WHERE UNI=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PARPROY4 SET UNI=@NEW WHERE UNI=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_1 SET UNI=@NEW WHERE UNI=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_2 SET UNI=@NEW WHERE UNI=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_3 SET UNI=@NEW WHERE UNI=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_4 SET UNI=@NEW WHERE UNI=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_1 SET UNI=@NEW WHERE UNI=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_2 SET UNI=@NEW WHERE UNI=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_3 SET UNI=@NEW WHERE UNI=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_4 SET UNI=@NEW WHERE UNI=@OLD"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE UNI CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART1_ADJ CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART2_ADJ CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART3_ADJ CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4_ADJ CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PARGEN_DEF CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_UON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARCON4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PARPROY4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "Close C"
sConsulta = sConsulta & vbCrLf & "DEALLOCATE C"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "COMMIT TRANSACTION"

CrearProcedure_UNI_COD_2_8_03 = sConsulta

End Function

Private Function CrearProcedure_USU_COD_2_8_03() As String
Dim sConsulta As String

sConsulta = "CREATE PROCEDURE USU_COD (@OLD VARCHAR(50),@NEW VARCHAR(50),@PWD VARCHAR(512))  AS"
sConsulta = sConsulta & vbCrLf & "DECLARE @VAR_COD VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "SET XACT_ABORT ON"
sConsulta = sConsulta & vbCrLf & "BEGIN TRANSACTION"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "DECLARE C CURSOR FOR SELECT COD FROM USU WHERE COD=@OLD"
sConsulta = sConsulta & vbCrLf & "OPEN C"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM C INTO @VAR_COD"
sConsulta = sConsulta & vbCrLf & "IF @@FETCH_STATUS=0"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE USU NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE USU_ACC NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE USU_LIS NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE LOG NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "UPDATE USU SET COD=@NEW,PWD=@PWD WHERE COD=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE USU_ACC SET USU=@NEW WHERE USU=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE USU_LIS SET USU=@NEW WHERE USU=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE SET USUVAL=@NEW WHERE USUVAL=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE SET USUVALSELPROVE=@NEW WHERE USUVALSELPROVE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE SET USUVALCIERRE=@NEW WHERE USUVALCIERRE=@OLD"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE USU CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE USU_ACC CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE USU_LIS CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE LOG CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "Close C"
sConsulta = sConsulta & vbCrLf & "DEALLOCATE C"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "COMMIT TRANSACTION"

CrearProcedure_USU_COD_2_8_03 = sConsulta

End Function

Private Function CrearProcedure_PROCE_COD_2_8_03() As String
Dim sConsulta As String

sConsulta = "CREATE PROCEDURE PROCE_COD (@OLD SMALLINT,@NEW SMALLINT,@ANYO INT,@GMN1 VARCHAR (3))  AS"
sConsulta = sConsulta & vbCrLf & "DECLARE @VAR_COD SMALLINT"
sConsulta = sConsulta & vbCrLf & "DECLARE @AN AS INT"
sConsulta = sConsulta & vbCrLf & "DECLARE @GMN AS VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "SET XACT_ABORT ON"
sConsulta = sConsulta & vbCrLf & "BEGIN TRANSACTION"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "DECLARE D CURSOR FOR SELECT ANYO,GMN1,COD FROM PROCE WHERE ANYO=@ANYO AND COD=@OLD AND GMN1=@GMN1"
sConsulta = sConsulta & vbCrLf & "OPEN D"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM D INTO @AN,@GMN,@VAR_COD"
sConsulta = sConsulta & vbCrLf & "IF @@FETCH_STATUS=0"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART1_ADJ NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART2_ADJ NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART3_ADJ NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4_ADJ NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_ADJ NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_ADJREU NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_ESP NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_OBJ NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_OFE NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_OFE_WEB NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRES NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESPROY NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_UON1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_UON2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_UON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE OFEMIN NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE DETALLE_PUJAS NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_NOTIF NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_ESP NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_OFE_ADJUN NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_OFE NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_OFE_WEB_ADJUN NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_OFE_WEB NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_PER NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_PROVE NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_PROVE_PET NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_PROVE_POT NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE REU_PROCE NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE BLOQUEO_PROCE NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "UPDATE ART1_ADJ SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART2_ADJ SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART3_ADJ SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART4_ADJ SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_ADJ SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_ADJREU SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_ESP SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_OBJ SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_OFE SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_OFE_WEB SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_PRES SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_UON1 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_UON2 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_UON3 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_PRESCON SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_PRESCON3 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_PRESCON4 SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_PRESPROY SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE OFEMIN SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE DETALLE_PUJAS SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROVENOTIF SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_ESP SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_OFE_ADJUN SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_OFE SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_OFE_WEB_ADJUN SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_OFE_WEB SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_PER SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_PROVE SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_PROVE_PET SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_PROVE_POT SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE REU_PROCE SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE SET COD=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE BLOQUEO_PROCE SET PROCE=@NEW WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@OLD"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART1_ADJ CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART2_ADJ CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART3_ADJ CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4_ADJ CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_ADJ CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESPROY CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_ADJREU CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_ESP CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_OBJ CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_OFE CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_OFE_WEB CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRES CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_UON1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_UON2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_UON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE OFEMIN CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE DETALLE_PUJAS CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVENOTIF CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_ESP CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_OFE_ADJUN CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_OFE CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_OFE_WEB_ADJUN CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_OFE_WEB CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_PER CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_PROVE CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_PROVE_PET CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_PROVE_POT CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE REU_PROCE CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE BLOQUEO_PROCE CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "Close D"
sConsulta = sConsulta & vbCrLf & "DEALLOCATE D"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "COMMIT TRANSACTION"

CrearProcedure_PROCE_COD_2_8_03 = sConsulta

End Function

Private Function CrearProcedure_PROVE_COD_2_8_03() As String
Dim sConsulta As String

sConsulta = "CREATE PROCEDURE PROVE_COD (@OLD VARCHAR(50),@NEW VARCHAR(50))  AS"
sConsulta = sConsulta & vbCrLf & "DECLARE @VAR_COD VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "SET XACT_ABORT ON"
sConsulta = sConsulta & vbCrLf & "BEGIN TRANSACTION"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "DECLARE C CURSOR FOR SELECT COD FROM PROVE WHERE COD=@OLD"
sConsulta = sConsulta & vbCrLf & "OPEN C"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM C INTO @VAR_COD"
sConsulta = sConsulta & vbCrLf & "IF @@FETCH_STATUS=0"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE CON NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_PROVE NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART1_ADJ NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART2_ADJ NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART3_ADJ NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4_ADJ NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_ADJ NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_ADJREU NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_OFE NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_OFE_WEB NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE DETALLE_PUJAS NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE OFEMIN NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_OFE_ADJUN NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_OFE NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_OFE_WEB_ADJUN NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_OFE_WEB NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_PROVE NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_PROVE_PET NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_PROVE_POT NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_EQP NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVENOTIF NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE USU NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "UPDATE PROVE SET COD=@NEW WHERE COD=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE CON SET PROVE=@NEW WHERE PROVE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_PROVE SET PROVE=@NEW WHERE PROVE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART1_ADJ SET CODPROVE=@NEW WHERE CODPROVE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART2_ADJ SET CODPROVE=@NEW WHERE CODPROVE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART3_ADJ SET CODPROVE=@NEW WHERE CODPROVE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ART4_ADJ SET CODPROVE=@NEW WHERE CODPROVE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM SET PROVEACT=@NEW WHERE PROVEACT=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_ADJ SET PROVE=@NEW WHERE PROVE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_ADJREU SET PROVE=@NEW WHERE PROVE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_OFE SET PROVE=@NEW WHERE PROVE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_OFE_WEB SET PROVE=@NEW WHERE PROVE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE DETALLE_PUJAS SET PROVE=@NEW WHERE PROVE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE OFEMIN SET PROVE=@NEW WHERE PROVE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_OFE_ADJUN SET PROVE=@NEW WHERE PROVE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_OFE SET PROVE=@NEW WHERE PROVE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_OFE_WEB_ADJUN SET PROVE=@NEW WHERE PROVE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_OFE_WEB SET PROVE=@NEW WHERE PROVE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_PROVE SET PROVE=@NEW WHERE PROVE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_PROVE_PET SET PROVE=@NEW WHERE PROVE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROCE_PROVE_POT SET PROVE=@NEW WHERE PROVE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_EQP SET PROVE=@NEW WHERE PROVE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_GMN1 SET PROVE=@NEW WHERE PROVE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_GMN2 SET PROVE=@NEW WHERE PROVE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_GMN3 SET PROVE=@NEW WHERE PROVE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_GMN4 SET PROVE=@NEW WHERE PROVE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_ART1 SET PROVE=@NEW WHERE PROVE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_ART2 SET PROVE=@NEW WHERE PROVE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_ART3 SET PROVE=@NEW WHERE PROVE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROVE_ART4 SET PROVE=@NEW WHERE PROVE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PROVENOTIF SET PROVE=@NEW WHERE PROVE=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE USU SET PROVE=@NEW WHERE PROVE=@OLD"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE CON CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_PROVE CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART1_ADJ CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART2_ADJ CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART3_ADJ CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ART4_ADJ CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_ADJ CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_ADJREU CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_OFE CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_OFE_WEB CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_OFE_ADJUN CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_OFE CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_OFE_WEB_ADJUN CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_OFE_WEB CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_PROVE CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_PROVE_PET CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROCE_PROVE_POT CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_EQP CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_GMN4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PROVE_ART4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE USU CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "Close C"
sConsulta = sConsulta & vbCrLf & "DEALLOCATE C"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "COMMIT TRANSACTION"

CrearProcedure_PROVE_COD_2_8_03 = sConsulta

End Function

Private Function CrearProcedure_PERF_COD_2_8_03() As String
Dim sConsulta As String

sConsulta = "CREATE PROCEDURE PERF_COD (@OLD VARCHAR(50),@NEW VARCHAR(50))  AS"
sConsulta = sConsulta & vbCrLf & "DECLARE @VAR_COD VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "SET XACT_ABORT ON"
sConsulta = sConsulta & vbCrLf & "BEGIN TRANSACTION"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "DECLARE C CURSOR FOR SELECT COD FROM PERF WHERE COD=@OLD"
sConsulta = sConsulta & vbCrLf & "OPEN C"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM C INTO @VAR_COD"
sConsulta = sConsulta & vbCrLf & "IF @@FETCH_STATUS=0"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PERF NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PERF_ACC NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PERF_LIS NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE USU NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "UPDATE PERF SET COD=@NEW WHERE COD=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PERF_ACC SET PERF=@NEW WHERE PERF=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PERF_LIS SET PERF=@NEW WHERE PERF=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE USU SET PERF=@NEW WHERE PERF=@OLD"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PERF CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PERF_ACC CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PERF_LIS CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE USU CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "Close C"
sConsulta = sConsulta & vbCrLf & "DEALLOCATE C"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "COMMIT TRANSACTION"

CrearProcedure_PERF_COD_2_8_03 = sConsulta

End Function

Private Function CrearProcedure_PRES_3_CON1_COD_2_8_03() As String
Dim sConsulta As String

sConsulta = "CREATE PROCEDURE PRES_3_CON1_COD (@OLD VARCHAR(50),@NEW VARCHAR(50))  AS"
sConsulta = sConsulta & vbCrLf & "DECLARE @VAR_COD VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "SET XACT_ABORT ON"
sConsulta = sConsulta & vbCrLf & "BEGIN TRANSACTION"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "DECLARE C CURSOR FOR SELECT COD FROM PRES_3_CON1 WHERE  COD=@OLD"
sConsulta = sConsulta & vbCrLf & "OPEN C"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM C INTO @VAR_COD"
sConsulta = sConsulta & vbCrLf & "IF @@FETCH_STATUS=0"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_3_CON1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_3_CON2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_3_CON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_3_CON4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_3_CON1 SET COD=@NEW WHERE  COD=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_3_CON2 SET PRES1=@NEW WHERE PRES1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_3_CON3 SET PRES1=@NEW WHERE  PRES1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_3_CON4 SET PRES1=@NEW WHERE  PRES1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_1 SET PRES1=@NEW WHERE  PRES1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_2 SET PRES1=@NEW WHERE  PRES1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_3 SET PRES1=@NEW WHERE  PRES1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_4 SET PRES1=@NEW WHERE  PRES1=@OLD"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_3_CON1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_3_CON2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_3_CON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_3_CON4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "Close C"
sConsulta = sConsulta & vbCrLf & "DEALLOCATE C"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "COMMIT TRANSACTION"

CrearProcedure_PRES_3_CON1_COD_2_8_03 = sConsulta

End Function

Private Function CrearProcedure_PRES_3_CON2_COD_2_8_03() As String
Dim sConsulta As String

sConsulta = "CREATE PROCEDURE PRES_3_CON2_COD (@OLD VARCHAR(50),@NEW VARCHAR(50),@PRES1 VARCHAR(50))  AS"
sConsulta = sConsulta & vbCrLf & "DECLARE @VAR_COD VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "SET XACT_ABORT ON"
sConsulta = sConsulta & vbCrLf & "BEGIN TRANSACTION"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "DECLARE C CURSOR FOR SELECT COD FROM PRES_3_CON2 WHERE  PRES1=@PRES1 AND COD=@OLD"
sConsulta = sConsulta & vbCrLf & "OPEN C"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM C INTO @VAR_COD"
sConsulta = sConsulta & vbCrLf & "IF @@FETCH_STATUS=0"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_3_CON2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_3_CON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_3_CON4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_3_CON2 SET COD=@NEW WHERE  PRES1=@PRES1 AND COD=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_3_CON3 SET PRES2=@NEW WHERE  PRES1=@PRES1 AND PRES2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_3_CON4 SET PRES2=@NEW WHERE  PRES1=@PRES1 AND PRES2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_PRESCON3 SET PRES2=@NEW WHERE PRES1=@PRES1 AND PRES2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_2 SET PRES2=@NEW WHERE  PRES1=@PRES1 AND PRES2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_3 SET PRES2=@NEW WHERE  PRES1=@PRES1 AND PRES2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_4 SET PRES2=@NEW WHERE  PRES1=@PRES1 AND PRES2=@OLD"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_3_CON2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_3_CON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_3_CON4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "Close C"
sConsulta = sConsulta & vbCrLf & "DEALLOCATE C"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "COMMIT TRANSACTION"

CrearProcedure_PRES_3_CON2_COD_2_8_03 = sConsulta

End Function

Private Function CrearProcedure_PRES_3_CON3_COD_2_8_03() As String
Dim sConsulta As String

sConsulta = "CREATE PROCEDURE PRES_3_CON3_COD (@OLD VARCHAR(50),@NEW VARCHAR(50),@PRES1 VARCHAR(50),@PRES2 VARCHAR(50))  AS"
sConsulta = sConsulta & vbCrLf & "DECLARE @VAR_COD VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "SET XACT_ABORT ON"
sConsulta = sConsulta & vbCrLf & "BEGIN TRANSACTION"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "DECLARE C CURSOR FOR SELECT COD FROM PRES_3_CON3 WHERE  PRES1=@PRES1 AND PRES2=@PRES2 AND COD=@OLD"
sConsulta = sConsulta & vbCrLf & "OPEN C"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM C INTO @VAR_COD"
sConsulta = sConsulta & vbCrLf & "IF @@FETCH_STATUS=0"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_3_CON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_3_CON4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_3_CON3 SET COD=@NEW WHERE  PRES1=@PRES1 AND PRES2=@PRES2 AND COD=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_3_CON4 SET PRES3=@NEW WHERE PRES1=@PRES1 AND PRES2=@PRES2 AND PRES3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_PRESCON3 SET PRES3=@NEW WHERE  PRES1=@PRES1 AND PRES2=@PRES2 AND PRES3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_3 SET PRES3=@NEW WHERE  PRES1=@PRES1 AND PRES2=@PRES2 AND PRES3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON3_4 SET PRES3=@NEW WHERE   PRES1=@PRES1 AND PRES2=@PRES2 AND PRES3=@OLD"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_3_CON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_3_CON4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "Close C"
sConsulta = sConsulta & vbCrLf & "DEALLOCATE C"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "COMMIT TRANSACTION"

CrearProcedure_PRES_3_CON3_COD_2_8_03 = sConsulta

End Function

Private Function CrearProcedure_PRES_3_CON4_COD_2_8_03() As String
Dim sConsulta As String

sConsulta = "CREATE PROCEDURE PRES_3_CON4_COD (@OLD VARCHAR(50),@NEW VARCHAR(50),@PRES1 VARCHAR(50),@PRES2 VARCHAR(50),@PRES3 VARCHAR(50))  AS"
sConsulta = sConsulta & vbCrLf & "DECLARE @VAR_COD VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "SET XACT_ABORT ON"
sConsulta = sConsulta & vbCrLf & "BEGIN TRANSACTION"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "DECLARE C CURSOR FOR SELECT COD FROM PRES_3_CON4 WHERE PRES1=@PRES1 AND PRES2=@PRES2 AND PRES3=@PRES3 AND COD=@OLD"
sConsulta = sConsulta & vbCrLf & "OPEN C"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM C INTO @VAR_COD"
sConsulta = sConsulta & vbCrLf & "IF @@FETCH_STATUS=0"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_3_CON4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_PRESCON3_4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_3_CON4 SET COD=@NEW WHERE PRES1=@PRES1 AND PRES2=@PRES2 AND PRES3=@PRES3 AND COD=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_PRESCON3 SET PRES4=@NEW WHERE PRES1=@PRES1 AND PRES2=@PRES2 AND PRES3=@PRES3 AND PRES4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_PRESCON3_4 SET PRES4=@NEW WHERE PRES1=@PRES1 AND PRES2=@PRES2 AND PRES3=@PRES3 AND PRES4=@OLD"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_3_CON4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_PRESCON3_4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "Close C"
sConsulta = sConsulta & vbCrLf & "DEALLOCATE C"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "COMMIT TRANSACTION"

CrearProcedure_PRES_3_CON4_COD_2_8_03 = sConsulta

End Function

Private Function CrearProcedure_PRES_4_CON1_COD_2_8_03() As String
Dim sConsulta As String

sConsulta = "CREATE PROCEDURE PRES_4_CON1_COD (@OLD VARCHAR(50),@NEW VARCHAR(50))  AS"
sConsulta = sConsulta & vbCrLf & "DECLARE @VAR_COD VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "SET XACT_ABORT ON"
sConsulta = sConsulta & vbCrLf & "BEGIN TRANSACTION"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "DECLARE C CURSOR FOR SELECT COD FROM PRES_4_CON1 WHERE  COD=@OLD"
sConsulta = sConsulta & vbCrLf & "OPEN C"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM C INTO @VAR_COD"
sConsulta = sConsulta & vbCrLf & "IF @@FETCH_STATUS=0"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_4_CON1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_4_CON2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_4_CON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_4_CON4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_4_CON1 SET COD=@NEW WHERE  COD=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_4_CON2 SET PRES1=@NEW WHERE PRES1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_4_CON3 SET PRES1=@NEW WHERE  PRES1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_4_CON4 SET PRES1=@NEW WHERE  PRES1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_PRESCON4 SET PRES1=@NEW WHERE  PRES1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_1 SET PRES1=@NEW WHERE  PRES1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_2 SET PRES1=@NEW WHERE  PRES1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_3 SET PRES1=@NEW WHERE  PRES1=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_4 SET PRES1=@NEW WHERE  PRES1=@OLD"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_4_CON1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_4_CON2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_4_CON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_4_CON4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "Close C"
sConsulta = sConsulta & vbCrLf & "DEALLOCATE C"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "COMMIT TRANSACTION"

CrearProcedure_PRES_4_CON1_COD_2_8_03 = sConsulta

End Function

Private Function CrearProcedure_PRES_4_CON2_COD_2_8_03() As String
Dim sConsulta As String

sConsulta = "CREATE PROCEDURE PRES_4_CON2_COD (@OLD VARCHAR(50),@NEW VARCHAR(50),@PRES1 VARCHAR(50))  AS"
sConsulta = sConsulta & vbCrLf & "DECLARE @VAR_COD VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "SET XACT_ABORT ON"
sConsulta = sConsulta & vbCrLf & "BEGIN TRANSACTION"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "DECLARE C CURSOR FOR SELECT COD FROM PRES_4_CON2 WHERE  PRES1=@PRES1 AND COD=@OLD"
sConsulta = sConsulta & vbCrLf & "OPEN C"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM C INTO @VAR_COD"
sConsulta = sConsulta & vbCrLf & "IF @@FETCH_STATUS=0"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_4_CON2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_4_CON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_4_CON4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_4_CON2 SET COD=@NEW WHERE  PRES1=@PRES1 AND COD=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_4_CON3 SET PRES2=@NEW WHERE  PRES1=@PRES1 AND PRES2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_4_CON4 SET PRES2=@NEW WHERE  PRES1=@PRES1 AND PRES2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_PRESCON4 SET PRES2=@NEW WHERE PRES1=@PRES1 AND PRES2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_2 SET PRES2=@NEW WHERE  PRES1=@PRES1 AND PRES2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_3 SET PRES2=@NEW WHERE  PRES1=@PRES1 AND PRES2=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_4 SET PRES2=@NEW WHERE  PRES1=@PRES1 AND PRES2=@OLD"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_4_CON2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_4_CON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_4_CON4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "Close C"
sConsulta = sConsulta & vbCrLf & "DEALLOCATE C"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "COMMIT TRANSACTION"

CrearProcedure_PRES_4_CON2_COD_2_8_03 = sConsulta

End Function

Private Function CrearProcedure_PRES_4_CON3_COD_2_8_03() As String
Dim sConsulta As String

sConsulta = "CREATE PROCEDURE PRES_4_CON3_COD (@OLD VARCHAR(50),@NEW VARCHAR(50),@PRES1 VARCHAR(50),@PRES2 VARCHAR(50))  AS"
sConsulta = sConsulta & vbCrLf & "DECLARE @VAR_COD VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "SET XACT_ABORT ON"
sConsulta = sConsulta & vbCrLf & "BEGIN TRANSACTION"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "DECLARE C CURSOR FOR SELECT COD FROM PRES_4_CON3 WHERE  PRES1=@PRES1 AND PRES2=@PRES2 AND COD=@OLD"
sConsulta = sConsulta & vbCrLf & "OPEN C"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM C INTO @VAR_COD"
sConsulta = sConsulta & vbCrLf & "IF @@FETCH_STATUS=0"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_4_CON3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_4_CON4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_4_CON3 SET COD=@NEW WHERE  PRES1=@PRES1 AND PRES2=@PRES2 AND COD=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_4_CON4 SET PRES3=@NEW WHERE PRES1=@PRES1 AND PRES2=@PRES2 AND PRES3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_PRESCON4 SET PRES3=@NEW WHERE  PRES1=@PRES1 AND PRES2=@PRES2 AND PRES3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_3 SET PRES3=@NEW WHERE  PRES1=@PRES1 AND PRES2=@PRES2 AND PRES3=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_MES_PRESCON4_4 SET PRES3=@NEW WHERE   PRES1=@PRES1 AND PRES2=@PRES2 AND PRES3=@OLD"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_4_CON3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_4_CON4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "Close C"
sConsulta = sConsulta & vbCrLf & "DEALLOCATE C"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "COMMIT TRANSACTION"

CrearProcedure_PRES_4_CON3_COD_2_8_03 = sConsulta

End Function

Private Function CrearProcedure_PRES_4_CON4_COD_2_8_03() As String
Dim sConsulta As String

sConsulta = "CREATE PROCEDURE PRES_4_CON4_COD (@OLD VARCHAR(50),@NEW VARCHAR(50),@PRES1 VARCHAR(50),@PRES2 VARCHAR(50),@PRES3 VARCHAR(50))  AS"
sConsulta = sConsulta & vbCrLf & "DECLARE @VAR_COD VARCHAR(50)"
sConsulta = sConsulta & vbCrLf & "SET XACT_ABORT ON"
sConsulta = sConsulta & vbCrLf & "BEGIN TRANSACTION"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "DECLARE C CURSOR FOR SELECT COD FROM PRES_4_CON4 WHERE PRES1=@PRES1 AND PRES2=@PRES2 AND PRES3=@PRES3 AND COD=@OLD"
sConsulta = sConsulta & vbCrLf & "OPEN C"
sConsulta = sConsulta & vbCrLf & "FETCH NEXT FROM C INTO @VAR_COD"
sConsulta = sConsulta & vbCrLf & "IF @@FETCH_STATUS=0"
sConsulta = sConsulta & vbCrLf & "BEGIN"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_4_CON4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_PRESCON4_4 NOCHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "UPDATE PRES_4_CON4 SET COD=@NEW WHERE PRES1=@PRES1 AND PRES2=@PRES2 AND PRES3=@PRES3 AND COD=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE ITEM_PRESCON4 SET PRES4=@NEW WHERE PRES1=@PRES1 AND PRES2=@PRES2 AND PRES3=@PRES3 AND PRES4=@OLD"
sConsulta = sConsulta & vbCrLf & "UPDATE AR_ITEM_PRESCON4_4 SET PRES4=@NEW WHERE PRES1=@PRES1 AND PRES2=@PRES2 AND PRES3=@PRES3 AND PRES4=@OLD"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE PRES_4_CON4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE ITEM_PRESCON4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "ALTER TABLE AR_ITEM_PRESCON4_4 CHECK CONSTRAINT ALL"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "Close C"
sConsulta = sConsulta & vbCrLf & "DEALLOCATE C"
sConsulta = sConsulta & vbCrLf & "End"
sConsulta = sConsulta & vbCrLf & "COMMIT TRANSACTION"

CrearProcedure_PRES_4_CON4_COD_2_8_03 = sConsulta

End Function

