VERSION 5.00
Begin VB.Form frmTIPOSinProveMat 
   BackColor       =   &H00808000&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Tipo de sincronizaci�n de materiales asignados a proveedores"
   ClientHeight    =   3225
   ClientLeft      =   3180
   ClientTop       =   3705
   ClientWidth     =   6930
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmTIPOSinProveMat.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3225
   ScaleWidth      =   6930
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.OptionButton optPortal 
      BackColor       =   &H00808000&
      Caption         =   "Mantener s�lo la asignaci�n de actividades a proveedores del Portal"
      ForeColor       =   &H8000000E&
      Height          =   330
      Left            =   360
      TabIndex        =   4
      Top             =   810
      Width           =   6405
   End
   Begin VB.OptionButton optLasDos 
      BackColor       =   &H00808000&
      Caption         =   "Mantener la asignaci�n de materiales a proveedores del GS + la asignaci�n de actividades del portal."
      ForeColor       =   &H8000000E&
      Height          =   375
      Left            =   360
      TabIndex        =   3
      Top             =   1215
      Width           =   6405
   End
   Begin VB.OptionButton optGS 
      BackColor       =   &H00808000&
      Caption         =   "Mantener s�lo la asinaci�n de materiales a proveedores del sistema FULLSTEP GS"
      ForeColor       =   &H8000000E&
      Height          =   330
      Left            =   360
      TabIndex        =   2
      Top             =   420
      Value           =   -1  'True
      Width           =   6360
   End
   Begin VB.CommandButton cmdCancelar 
      Caption         =   "Cancelar"
      Height          =   315
      Left            =   3405
      TabIndex        =   1
      Top             =   2835
      Width           =   1005
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Default         =   -1  'True
      Height          =   315
      Left            =   2295
      TabIndex        =   0
      Top             =   2835
      Width           =   1005
   End
   Begin VB.Label Label4 
      BackColor       =   &H00808000&
      Caption         =   "ATENCI�N!!! "
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H0000FFFF&
      Height          =   285
      Left            =   135
      TabIndex        =   7
      Top             =   1710
      Width           =   4065
   End
   Begin VB.Label Label3 
      BackColor       =   &H00808000&
      Caption         =   "Si elige la segunda o la tercera opci�n se perder�n las asignaciones de actividades que no existan en la estructura de materiales."
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H0000FFFF&
      Height          =   645
      Left            =   135
      TabIndex        =   6
      Top             =   2070
      Width           =   6720
   End
   Begin VB.Label Label1 
      BackColor       =   &H00808000&
      Caption         =   "Elija el tipo de sincronizaci�n que desea para los materiales asignados a proveedores:"
      ForeColor       =   &H8000000E&
      Height          =   330
      Left            =   90
      TabIndex        =   5
      Top             =   45
      Width           =   6450
   End
End
Attribute VB_Name = "frmTIPOSinProveMat"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private Sub cmdAceptar_Click()
Dim iResp As Integer
Dim sMes As String

    If optGS.Value = True Then
        bas_V_2_16_5.iTipoSinc = 1
    Else
        sMes = "�Esta seguro de que desea utilizar la asignaci�n de actividades a proveedores del portal?"
        iResp = MsgBox(sMes, vbYesNo, "FULLSTEP GS Actualizador de base de datos")
        If iResp = vbNo Then Exit Sub
        
        If optPortal.Value = True Then
            bas_V_2_16_5.iTipoSinc = 2
        Else
            If optLasDos.Value = True Then
                bas_V_2_16_5.iTipoSinc = 3
            End If
        End If
    End If
    Unload Me
End Sub
Private Sub cmdCancelar_Click()
    bas_V_2_16_5.iTipoSinc = 99
    Unload Me
End Sub

Private Sub Form_Load()

    optGS.Value = True
    
End Sub

