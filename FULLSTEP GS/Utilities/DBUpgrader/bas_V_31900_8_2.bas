Attribute VB_Name = "bas_V_31900_8_2"
Option Explicit

Public Function CodigoDeActualizacion31900_08_1887_A31900_08_1888() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim oADOConnection As ADODB.Connection
    Dim sConsulta As String

    On Error GoTo Error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Actualiza Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 3
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    frmProgreso.ProgressBar1.Refresh
    V_31900_8_Storeds_088
    V_31900_8_Storeds_088_1
    
    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Actualiza Tablas"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 3
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    frmProgreso.ProgressBar1.Refresh
    V_31900_8_Tablas_088
    
            
    sConsulta = "UPDATE VERSION SET NUM ='3.00.18.88'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103), PRODVER='31900.8'"
    ExecuteSQL gRDOCon, sConsulta
        
    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False
        
    CodigoDeActualizacion31900_08_1887_A31900_08_1888 = True
    Exit Function

Error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    CodigoDeActualizacion31900_08_1887_A31900_08_1888 = False
End Function

Public Sub V_31900_8_Storeds_088()
Dim sConsulta As String
        
sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_GET_INSTANCIAS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_GET_INSTANCIAS]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_GET_INSTANCIAS] @IDI AS VARCHAR(20), @USU AS VARCHAR(50), @ORDENACION AS VARCHAR(50)=NULL, @SENTIDOORDENACION AS VARCHAR(50)=NULL," & vbCrLf
sConsulta = sConsulta & "@ID AS INT=NULL, @TITULO VARCHAR(400)=NULL, @TIPO AS INT=NULL, @FECHADESDE AS DATETIME=NULL,@FORMATOFECHA AS VARCHAR(50)=NULL," & vbCrLf
sConsulta = sConsulta & "@ORIGEN AS VARCHAR(50)=NULL, @IMPORTEDESDE AS  FLOAT=NULL,@IMPORTEHASTA AS  FLOAT=NULL ,@PETICIONARIO AS VARCHAR(50)=NULL, @ARTICULO AS VARCHAR(200)=NULL," & vbCrLf
sConsulta = sConsulta & "@ESTADO AS VARCHAR(50)=NULL, @NOM_TABLA AS VARCHAR(50)=NULL, " & vbCrLf
sConsulta = sConsulta & " @BUSCARTITULO VARCHAR(400)=NULL, @BUSCARTIPO AS INT=NULL, @BUSCARIMPORTEDESDE AS  FLOAT=NULL, @BUSCARIMPORTEHASTA AS  FLOAT=NULL, " & vbCrLf
sConsulta = sConsulta & "@BUSCARFECHADESDE AS DATETIME=NULL,@BUSCARORIGEN AS VARCHAR(50)=NULL, @BUSCARPETICIONARIO AS VARCHAR(50)=NULL, @BUSCARARTICULO AS VARCHAR(200)=NULL," & vbCrLf
sConsulta = sConsulta & "@PROCESOS AS TINYINT=0, @ABIERTASPORUSTED AS TINYINT=0, @PARTICIPO AS TINYINT=0, @PENDIENTES AS TINYINT=0, @PENDDEVOLUCION AS TINYINT=0, @TRASLADADAS AS TINYINT=0, @OTRAS AS TINYINT=0," & vbCrLf
sConsulta = sConsulta & "@TIPOS AS NVARCHAR(500)=NULL, @FECHAHASTA AS DATETIME=NULL, @PROVEEDOR AS VARCHAR(50) =NULL, @BUSCARPROVEEDOR AS VARCHAR(50)=NULL, @OBLCODPEDDIR BIT = 0" & vbCrLf
sConsulta = sConsulta & "AS " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL  AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSELECT AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLFROM AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLFROMPET AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLFROMBUSQUEDA AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLWHERE  AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @TIPOFECHA AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ADICIONAL AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL2 AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLART1 AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLART2 AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @BUSQUEDA_AUX TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @SUSTITUTO AS INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @TIPOFECHA=103" & vbCrLf
sConsulta = sConsulta & "SET @BUSQUEDA_AUX=0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @FORMATOFECHA='dd/mm/yyyy'" & vbCrLf
sConsulta = sConsulta & "   SET @TIPOFECHA=103" & vbCrLf
sConsulta = sConsulta & "ELSE IF @FORMATOFECHA='mm/dd/yyyy'" & vbCrLf
sConsulta = sConsulta & "   SET @TIPOFECHA=101" & vbCrLf
sConsulta = sConsulta & "ELSE IF @FORMATOFECHA='dd-mm-yyyy'" & vbCrLf
sConsulta = sConsulta & "   SET @TIPOFECHA=105" & vbCrLf
sConsulta = sConsulta & "ELSE IF @FORMATOFECHA='mm-dd-yyyy'" & vbCrLf
sConsulta = sConsulta & "   SET @TIPOFECHA=110" & vbCrLf
sConsulta = sConsulta & "ELSE IF @FORMATOFECHA='mm.dd.yyyy'" & vbCrLf
sConsulta = sConsulta & "   SET @TIPOFECHA=104" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Comprobamos el estado por el que se est� filtrando. No es necesario hacer siempre todas las consultas, as� aligeramos:" & vbCrLf
sConsulta = sConsulta & "IF @ESTADO='0'   ---Guardadas" & vbCrLf
sConsulta = sConsulta & " begin" & vbCrLf
sConsulta = sConsulta & "  SET @PARTICIPO=0" & vbCrLf
sConsulta = sConsulta & "  SET @PENDIENTES=0" & vbCrLf
sConsulta = sConsulta & "  SET @PENDDEVOLUCION=0" & vbCrLf
sConsulta = sConsulta & "  SET @TRASLADADAS=0" & vbCrLf
sConsulta = sConsulta & "  SET @OTRAS=0" & vbCrLf
sConsulta = sConsulta & " end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ESTADO='6' --Rechazadas" & vbCrLf
sConsulta = sConsulta & " begin" & vbCrLf
sConsulta = sConsulta & "  SET @PENDDEVOLUCION=0" & vbCrLf
sConsulta = sConsulta & "  SET @TRASLADADAS=0" & vbCrLf
sConsulta = sConsulta & " end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ESTADO='8' --Anuladas" & vbCrLf
sConsulta = sConsulta & " begin" & vbCrLf
sConsulta = sConsulta & "  SET @PENDDEVOLUCION=0" & vbCrLf
sConsulta = sConsulta & "  SET @TRASLADADAS=0" & vbCrLf
sConsulta = sConsulta & " end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ESTADO='100,101,102,103' --Finalizadas" & vbCrLf
sConsulta = sConsulta & " begin" & vbCrLf
sConsulta = sConsulta & "  SET @PENDIENTES=0" & vbCrLf
sConsulta = sConsulta & "  SET @PENDDEVOLUCION=0" & vbCrLf
sConsulta = sConsulta & "  SET @TRASLADADAS=0" & vbCrLf
sConsulta = sConsulta & " end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ESTADO='104' --Cerradas desde GS" & vbCrLf
sConsulta = sConsulta & " begin" & vbCrLf
sConsulta = sConsulta & "  SET @PENDIENTES=0" & vbCrLf
sConsulta = sConsulta & "  SET @PENDDEVOLUCION=0" & vbCrLf
sConsulta = sConsulta & "  SET @TRASLADADAS=0" & vbCrLf
sConsulta = sConsulta & " end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ESTADO='1000'  ---Pendientes" & vbCrLf
sConsulta = sConsulta & " begin" & vbCrLf
sConsulta = sConsulta & "  SET @OTRAS=0" & vbCrLf
sConsulta = sConsulta & "  SET @PARTICIPO=0" & vbCrLf
sConsulta = sConsulta & "  SET @ABIERTASPORUSTED=0" & vbCrLf
sConsulta = sConsulta & " end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Primero obtenemos los identificadores de todas las instancias que cumplan las condiciones" & vbCrLf
sConsulta = sConsulta & "----B�SQUEDAS FILTRO: POR ID, T�TULO, IMPORTES,  PETICIONARIO, FECHA DE ALTA, TIPO, DEPARTAMENTO DEL PETICIONARIO, ESTADO Y ART�CULO" & vbCrLf
sConsulta = sConsulta & "SET @SQLWHERE = N' WHERE TS.TIPO IN (0,1,4,6,8,9)' --Muestra solicitudes de compras, abonos, proyectos, otros (tipo 0), de pedido expr�s (8) y de pedido negociado (9)" & vbCrLf
sConsulta = sConsulta & "SET @SQLFROM = N' FROM INSTANCIA I WITH (NOLOCK) INNER JOIN SOLICITUD S WITH (NOLOCK) ON I.SOLICITUD=S.ID INNER JOIN TIPO_SOLICITUDES TS WITH (NOLOCK) ON S.TIPO=TS.ID'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ID <> 0 AND @ID IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "       SET @SQLWHERE = @SQLWHERE + ' AND I.ID=@ID'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @TITULO IS NOT NULL AND @TITULO <> ''" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "      SET @SQLFROM = @SQLFROM + ' INNER JOIN INSTANCIA_VISORES_PM C1 WITH(NOLOCK) ON I.ID=C1.INSTANCIA'" & vbCrLf
sConsulta = sConsulta & "      SET @SQLWHERE = @SQLWHERE + ' AND (C1.TITULO_TEXT LIKE ''%' + @TITULO + '%'' OR CONVERT(VARCHAR,TITULO_FEC) LIKE ''%' + @TITULO + '%'')'" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @FECHADESDE IS NOT NULL AND @FECHADESDE<>'' AND (@BUSCARFECHADESDE IS NULL OR @BUSCARFECHADESDE='')" & vbCrLf
sConsulta = sConsulta & "       SET @SQLWHERE = @SQLWHERE + ' AND I.FEC_ALTA>=CONVERT(datetime,@FECHADESDE,@TIPOFECHA)'" & vbCrLf
sConsulta = sConsulta & "IF @FECHAHASTA IS NOT NULL AND @FECHAHASTA<>''" & vbCrLf
sConsulta = sConsulta & "       SET @SQLWHERE = @SQLWHERE + ' AND I.FEC_ALTA<=CONVERT(datetime,@FECHAHASTA,@TIPOFECHA)'" & vbCrLf
sConsulta = sConsulta & "IF @IMPORTEDESDE IS NOT NULL AND @IMPORTEDESDE<>0" & vbCrLf
sConsulta = sConsulta & "   SET @SQLWHERE = @SQLWHERE + ' AND I.IMPORTE>=@IMPORTEDESDE'" & vbCrLf
sConsulta = sConsulta & "IF @IMPORTEHASTA IS NOT NULL AND @IMPORTEHASTA<>0" & vbCrLf
sConsulta = sConsulta & "       SET @SQLWHERE = @SQLWHERE + ' AND I.IMPORTE<=@IMPORTEHASTA'" & vbCrLf
sConsulta = sConsulta & "IF @TIPO <> 0 AND @TIPO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "       SET @SQLWHERE = @SQLWHERE + ' AND I.SOLICITUD=@TIPO'" & vbCrLf
sConsulta = sConsulta & "IF @TIPOS IS NOT NULL AND @TIPOS <> ''" & vbCrLf
sConsulta = sConsulta & "       SET @SQLWHERE = @SQLWHERE + ' AND I.SOLICITUD IN (' + @TIPOS + ')'" & vbCrLf
sConsulta = sConsulta & "IF @ORIGEN IS NOT NULL AND @ORIGEN <> ''" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @SQLFROM = @SQLFROM + ' INNER JOIN PER P WITH(NOLOCK) ON P.COD = I.PETICIONARIO'" & vbCrLf
sConsulta = sConsulta & "   SET @SQLFROM = @SQLFROM + ' INNER JOIN DEP WITH (NOLOCK) ON P.DEP=DEP.COD'" & vbCrLf
sConsulta = sConsulta & "   SET @SQLWHERE = @SQLWHERE + ' AND DEP.COD=@ORIGEN'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "IF @PETICIONARIO IS NOT NULL AND @PETICIONARIO <> ''" & vbCrLf
sConsulta = sConsulta & "   SET @SQLWHERE = @SQLWHERE + ' AND I.PETICIONARIO=@PETICIONARIO'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @BUSCARTITULO IS NOT NULL AND @BUSCARTITULO <> ''" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SELECT @BUSQUEDA_AUX=PARGEN_PM.BUSQUEDA_AUX FROM PARGEN_PM  WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   IF @BUSQUEDA_AUX IS NULL OR @BUSQUEDA_AUX=0" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           IF @TITULO IS NULL OR @TITULO = ''" & vbCrLf
sConsulta = sConsulta & "           BEGIN" & vbCrLf
sConsulta = sConsulta & "               SET @SQLFROM = @SQLFROM + ' INNER JOIN INSTANCIA_VISORES_PM C1 WITH(NOLOCK) ON I.ID=C1.INSTANCIA'" & vbCrLf
sConsulta = sConsulta & "               SET @SQLWHERE = @SQLWHERE + ' AND (C1.TITULO_TEXT LIKE ''%' + @BUSCARTITULO + '%'' OR CONVERT(VARCHAR,TITULO_FEC) LIKE ''%' + @BUSCARTITULO + '%'')'" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "          SET @SQLFROM = @SQLFROM + ' INNER JOIN PM_FILTROS_AUX C2 WITH(NOLOCK) ON I.ID=C2.INSTANCIA'" & vbCrLf
sConsulta = sConsulta & "          SET @SQLWHERE = @SQLWHERE + ' AND (C2.VALOR LIKE ''%' + @BUSCARTITULO + '%'')'" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "IF @BUSCARFECHADESDE IS NOT NULL AND @BUSCARFECHADESDE<>''" & vbCrLf
sConsulta = sConsulta & "       SET @SQLWHERE = @SQLWHERE + ' AND I.FEC_ALTA>=CONVERT(datetime,@BUSCARFECHADESDE,@TIPOFECHA)'" & vbCrLf
sConsulta = sConsulta & "IF @BUSCARIMPORTEDESDE IS NOT NULL AND @BUSCARIMPORTEDESDE<>0" & vbCrLf
sConsulta = sConsulta & "   SET @SQLWHERE = @SQLWHERE + ' AND I.IMPORTE>=@BUSCARIMPORTEDESDE'" & vbCrLf
sConsulta = sConsulta & "IF @BUSCARIMPORTEHASTA IS NOT NULL AND @BUSCARIMPORTEHASTA<>0" & vbCrLf
sConsulta = sConsulta & "       SET @SQLWHERE = @SQLWHERE + ' AND I.IMPORTE<=@BUSCARIMPORTEHASTA'" & vbCrLf
sConsulta = sConsulta & "IF @BUSCARTIPO <> 0 AND @BUSCARTIPO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "       SET @SQLWHERE = @SQLWHERE + ' AND I.SOLICITUD=@BUSCARTIPO'" & vbCrLf
sConsulta = sConsulta & "IF  (@ORIGEN IS NULL OR @ORIGEN = '') AND  @BUSCARORIGEN IS NOT NULL AND @BUSCARORIGEN <> ''" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @SQLFROM = @SQLFROM + ' INNER JOIN PER P WITH(NOLOCK) ON P.COD = I.PETICIONARIO'" & vbCrLf
sConsulta = sConsulta & "   SET @SQLFROM = @SQLFROM + ' INNER JOIN DEP WITH (NOLOCK) ON P.DEP=DEP.COD'" & vbCrLf
sConsulta = sConsulta & "   SET @SQLWHERE = @SQLWHERE + ' AND DEP.COD=@BUSCARORIGEN'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF (@PETICIONARIO IS NULL OR @PETICIONARIO = '') AND @BUSCARPETICIONARIO IS NOT NULL AND @BUSCARPETICIONARIO <> ''" & vbCrLf
sConsulta = sConsulta & "   SET @SQLWHERE = @SQLWHERE + ' AND I.PETICIONARIO=@BUSCARPETICIONARIO'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ESTADO IS NOT NULL AND @ESTADO<>''" & vbCrLf
sConsulta = sConsulta & "  begin" & vbCrLf
sConsulta = sConsulta & "   IF @ESTADO='1000'" & vbCrLf
sConsulta = sConsulta & "       SET @SQLWHERE = @SQLWHERE +  ' AND I.ESTADO =2' --pendientes de aprobar." & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "       SET @SQLWHERE = @SQLWHERE +  ' AND I.ESTADO IN(' + @ESTADO + ')'   ---guardadas, en curso, rechazadas, etc" & vbCrLf
sConsulta = sConsulta & "  end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @NOM_TABLA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "    begin" & vbCrLf
sConsulta = sConsulta & "   SET @SQLFROM = @SQLFROM + ' INNER JOIN ' + @IDI + '_' + @NOM_TABLA + ' F WITH (NOLOCK) ON I.ID = F.INSTANCIA'" & vbCrLf
sConsulta = sConsulta & "    end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Busqueda por Proveedor" & vbCrLf
sConsulta = sConsulta & "IF ((@PROVEEDOR IS NOT NULL) AND (@PROVEEDOR<>'')) OR ((@BUSCARPROVEEDOR IS NOT NULL) AND (@BUSCARPROVEEDOR<>''))" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @SQLFROM = @SQLFROM + ' INNER JOIN INSTANCIA_DESGLOSE_MATERIAL IDM WITH(NOLOCK) ON I.ID=IDM.INSTANCIA'" & vbCrLf
sConsulta = sConsulta & "   IF (@PROVEEDOR IS NOT NULL) AND (@PROVEEDOR<>'')" & vbCrLf
sConsulta = sConsulta & "       SET @SQLWHERE = @SQLWHERE + ' AND IDM.PROVE=@PROVEEDOR'" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "       SET @SQLWHERE = @SQLWHERE + ' AND IDM.PROVE=@BUSCARPROVEEDOR'   " & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF (SELECT COUNT(*) FROM USU U WITH (NOLOCK) WHERE U.FSWS_SUSTITUCION=@USU)>0 " & vbCrLf
sConsulta = sConsulta & " SET @SUSTITUTO =1" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & " SET @SUSTITUTO =0" & vbCrLf
sConsulta = sConsulta & "--------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE #G1 (ID INT, ICONO INT, OBV INT, ACCION_APROBAR INT, ACCION_RECHAZAR INT, BLOQUE INT,OBSYPART INT)" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE #T_ABIERTAS (ID INT)" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE #T_PARTICIPO (ID INT)" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE #T_PENDIENTES (ID INT, ICONO INT, OBV INT, ACCION_APROBAR INT, ACCION_RECHAZAR INT, BLOQUE INT)" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE #T_DEVOLUCION (ID INT)" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE #T_TRASLADADAS (ID INT)" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE #T_OTRAS (ID INT)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ABIERTASPORUSTED=1" & vbCrLf
sConsulta = sConsulta & " begin  " & vbCrLf
sConsulta = sConsulta & "  SET @SQL = 'INSERT INTO #T_ABIERTAS (ID) SELECT I.ID ' + @SQLFROM + @SQLWHERE" & vbCrLf
sConsulta = sConsulta & "  SET @SQL=@SQL + ' AND I.NUM IS NOT NULL AND I.PETICIONARIO = @USU AND I.EN_PROCESO<>2'" & vbCrLf
sConsulta = sConsulta & "  EXEC SP_EXECUTESQL @SQL, N'@USU VARCHAR(50), @ID AS INT,@TITULO VARCHAR(400),@TIPO INT,@FECHADESDE DATETIME,@TIPOFECHA INT,@ORIGEN VARCHAR(50),@IMPORTEDESDE FLOAT,@IMPORTEHASTA FLOAT,@PETICIONARIO VARCHAR(50)," & vbCrLf
sConsulta = sConsulta & " @BUSCARTITULO VARCHAR(400),@BUSCARTIPO INT,@BUSCARFECHADESDE DATETIME,@BUSCARORIGEN VARCHAR(50),@BUSCARIMPORTEDESDE FLOAT,@BUSCARIMPORTEHASTA FLOAT,@BUSCARPETICIONARIO VARCHAR(50),@TIPOS NVARCHAR(500), @FECHAHASTA DATETIME, @PROVEEDOR AS VARCHAR(50), @BUSCARPROVEEDOR AS VARCHAR(50)', @USU=@USU,@ID=@ID,@TITULO=@TITULO,@TIPO=@TIPO,@FECHADESDE=@FECHADESDE,@TIPOFECHA=@TIPOFECHA,@ORIGEN=@ORIGEN,@IMPORTEDESDE=@IMPORTEDESDE,@IMPORTEHASTA=@IMPORTEHASTA,@PETICIONARIO=@PETICIONARIO,@BUSCARTITULO=@BUSCARTITULO,@BUSCARTIPO=@BUSCARTIPO,@BUSCARFECHADESDE=@BUSCARFECHADESDE,@BUSCARORIGEN=@BUSCARORIGEN,@BUSCARIMPORTEDESDE=@BUSCARIMPORTEDESDE,@BUSCARIMPORTEHASTA=@BUSCARIMPORTEHASTA,@BUSCARPETICIONARIO=@BUSCARPETICIONARIO,@TIPOS=@TIPOS,@FECHAHASTA=@FECHAHASTA, @PROVEEDOR=@PROVEEDOR, @BUSCARPROVEEDOR=@BUSCARPROVEEDOR" & vbCrLf
sConsulta = sConsulta & " end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @PARTICIPO=1" & vbCrLf
sConsulta = sConsulta & " begin" & vbCrLf
sConsulta = sConsulta & "  SET @SQL = 'INSERT INTO #T_PARTICIPO (ID) SELECT I.ID ' + @SQLFROM " & vbCrLf
sConsulta = sConsulta & "  SET @SQL = @SQL + ' INNER JOIN INSTANCIA_EST WITH (NOLOCK) ON I.ID=INSTANCIA_EST.INSTANCIA AND INSTANCIA_EST.PER=@USU'" & vbCrLf
sConsulta = sConsulta & "  SET @SQL = @SQL + @SQLWHERE + ' AND I.NUM IS NOT NULL AND  (INSTANCIA_EST.ROL IS NOT NULL OR (INSTANCIA_EST.ROL IS  NULL AND (INSTANCIA_EST.ACCION=3 OR INSTANCIA_EST.ACCION=4)))'  " & vbCrLf
sConsulta = sConsulta & "  EXEC SP_EXECUTESQL @SQL, N'@USU VARCHAR(50), @ID AS INT,@TITULO VARCHAR(400),@TIPO INT,@FECHADESDE DATETIME,@TIPOFECHA INT,@ORIGEN VARCHAR(50),@IMPORTEDESDE FLOAT,@IMPORTEHASTA FLOAT,@PETICIONARIO VARCHAR(50)," & vbCrLf
sConsulta = sConsulta & "@BUSCARTITULO VARCHAR(400),@BUSCARTIPO INT,@BUSCARFECHADESDE DATETIME,@BUSCARORIGEN VARCHAR(50),@BUSCARIMPORTEDESDE FLOAT,@BUSCARIMPORTEHASTA FLOAT,@BUSCARPETICIONARIO VARCHAR(50),@TIPOS NVARCHAR(500), @FECHAHASTA DATETIME, @PROVEEDOR AS VARCHAR(50), @BUSCARPROVEEDOR AS VARCHAR(50)', @USU=@USU,@ID=@ID,@TITULO=@TITULO,@TIPO=@TIPO,@FECHADESDE=@FECHADESDE,@TIPOFECHA=@TIPOFECHA,@ORIGEN=@ORIGEN,@IMPORTEDESDE=@IMPORTEDESDE,@IMPORTEHASTA=@IMPORTEHASTA,@PETICIONARIO=@PETICIONARIO,@BUSCARTITULO=@BUSCARTITULO,@BUSCARTIPO=@BUSCARTIPO,@BUSCARFECHADESDE=@BUSCARFECHADESDE,@BUSCARORIGEN=@BUSCARORIGEN,@BUSCARIMPORTEDESDE=@BUSCARIMPORTEDESDE,@BUSCARIMPORTEHASTA=@BUSCARIMPORTEHASTA,@BUSCARPETICIONARIO=@BUSCARPETICIONARIO,@TIPOS=@TIPOS,@FECHAHASTA=@FECHAHASTA, @PROVEEDOR=@PROVEEDOR, @BUSCARPROVEEDOR=@BUSCARPROVEEDOR" & vbCrLf
sConsulta = sConsulta & " end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @PENDIENTES =1" & vbCrLf
sConsulta = sConsulta & " begin" & vbCrLf
sConsulta = sConsulta & "  SET @SQL = 'INSERT INTO #T_PENDIENTES (ID, ICONO, OBV, ACCION_APROBAR, ACCION_RECHAZAR, BLOQUE) SELECT I.ID, 1 as ICONO, 0 as OBV,isnull(RAA.ACCION,0) AS ACCION_APROBAR,isnull(RAR.ACCION,0) AS ACCION_RECHAZAR, isnull(IB.BLOQUE,0) AS BLOQUE ' + @SQLFROM " & vbCrLf
sConsulta = sConsulta & "  SET @SQL=@SQL + ' INNER JOIN INSTANCIA_BLOQUE IB WITH (NOLOCK) ON I.ID=IB.INSTANCIA AND IB.EN_PROCESO=0 AND IB. ESTADO=1 AND IB.BLOQ<>1'" & vbCrLf
sConsulta = sConsulta & "  SET @SQL=@SQL + ' INNER JOIN PM_COPIA_ROL_BLOQUE RB WITH (NOLOCK) ON RB.BLOQUE=IB.BLOQUE AND IB.ESTADO=1 AND IB.BLOQ<>1 AND IB.TRASLADADA<>1'" & vbCrLf
sConsulta = sConsulta & "  SET @SQL=@SQL + ' INNER JOIN PM_COPIA_ROL R WITH (NOLOCK)  ON RB.ROL=R.ID'" & vbCrLf
sConsulta = sConsulta & "  SET @SQL=@SQL + ' INNER JOIN PM_COPIA_BLOQUE B WITH (NOLOCK) ON RB.BLOQUE=B.ID'" & vbCrLf
sConsulta = sConsulta & "  SET @SQL=@SQL + ' LEFT JOIN PM_COPIA_ROL_ACCION RAA WITH(NOLOCK)ON RAA.ROL=R.ID AND RAA.BLOQUE=RB.BLOQUE AND RAA.APROBAR=1'   --acciones del rol actual (Aprobar)" & vbCrLf
sConsulta = sConsulta & "  SET @SQL=@SQL + ' LEFT JOIN PM_COPIA_ROL_ACCION RAR WITH(NOLOCK)ON RAR.ROL=R.ID AND RAR.BLOQUE=RB.BLOQUE AND RAR.RECHAZAR=1'   --acciones del rol actual (Rechazar)" & vbCrLf
sConsulta = sConsulta & "  SET @SQL = @SQL + @SQLWHERE + ' AND I.NUM IS NOT NULL AND I.EN_PROCESO=0 AND I.TRASLADADA=0 AND I.ESTADO=2 AND '" & vbCrLf
sConsulta = sConsulta & "  IF @SUSTITUTO =1" & vbCrLf
sConsulta = sConsulta & "    SET @SQL=@SQL + ' ( (R.PER=@USU AND (SELECT ISNULL(U.FSWS_SUSTITUCION,'''') FROM USU U WITH (NOLOCK) WHERE U.PER=@USU AND U.BAJA=0) = '''')  OR R.PER IN ( SELECT U.PER FROM USU U WITH (NOLOCK) WHERE U.FSWS_SUSTITUCION=@USU))'" & vbCrLf
sConsulta = sConsulta & "  ELSE" & vbCrLf
sConsulta = sConsulta & "    SET @SQL=@SQL + '( (R.PER=@USU AND (SELECT ISNULL(U.FSWS_SUSTITUCION,'''') FROM USU U WITH (NOLOCK) WHERE U.PER=@USU AND U.BAJA=0) = ''''))'" & vbCrLf
sConsulta = sConsulta & "  SET @SQL=@SQL + ' AND NOT (B.TIPO=1 AND I.PETICIONARIO<>R.PER)'  " & vbCrLf
sConsulta = sConsulta & "  SET @SQL=@SQL + ' UNION '" & vbCrLf
sConsulta = sConsulta & "  SET @SQL=@SQL + ' SELECT I.ID,1 as ICONO, 0 as OBV,isnull(RAA.ACCION,0) AS ACCION_APROBAR, isnull(RAR.ACCION,0) AS ACCION_RECHAZAR, isnull(IB.BLOQUE,0) AS BLOQUE ' + @SQLFROM + ' INNER JOIN INSTANCIA_BLOQUE IB WITH (NOLOCK) ON I.ID=IB.INSTANCIA AND IB.EN_PROCESO=0 AND IB.ESTADO=1 AND IB.BLOQ<>1 AND IB.TRASLADADA<>1'" & vbCrLf
sConsulta = sConsulta & "  SET @SQL=@SQL + ' INNER JOIN PM_COPIA_ROL_BLOQUE RB WITH (NOLOCK)  ON RB.BLOQUE=IB.BLOQUE'" & vbCrLf
sConsulta = sConsulta & "  SET @SQL=@SQL + ' INNER JOIN PM_COPIA_ROL R WITH (NOLOCK)  ON RB.ROL=R.ID AND R.PER IS NULL AND R.COMO_ASIGNAR=3  AND R.TIPO<>5'" & vbCrLf
sConsulta = sConsulta & "  SET @SQL=@SQL + '  INNER JOIN PM_COPIA_PARTICIPANTES PT WITH (NOLOCK)  ON PT.ROL=R.ID'" & vbCrLf
sConsulta = sConsulta & "  SET @SQL=@SQL + ' LEFT JOIN PM_COPIA_ROL_ACCION RAA WITH(NOLOCK)ON RAA.ROL=R.ID AND RAA.BLOQUE=RB.BLOQUE AND RAA.APROBAR=1'   --acciones del rol actual (Aprobar)" & vbCrLf
sConsulta = sConsulta & "  SET @SQL=@SQL + ' LEFT JOIN PM_COPIA_ROL_ACCION RAR WITH(NOLOCK)ON RAR.ROL=R.ID AND RAR.BLOQUE=RB.BLOQUE AND RAR.RECHAZAR=1'   --acciones del rol actual (Rechazar)" & vbCrLf
sConsulta = sConsulta & "  SET @SQL=@SQL + @SQLWHERE + ' AND I.NUM IS NOT NULL AND I.EN_PROCESO=0 AND I.TRASLADADA=0 AND I.ESTADO=2 AND '" & vbCrLf
sConsulta = sConsulta & "  IF @SUSTITUTO =1" & vbCrLf
sConsulta = sConsulta & "    SET @SQL=@SQL + ' ( (PT.PER=@USU AND (SELECT ISNULL(U.FSWS_SUSTITUCION,'''') FROM USU U WITH (NOLOCK) WHERE U.PER=@USU AND U.BAJA=0) = '''') OR PT.PER IN ( SELECT U.PER FROM USU U WITH (NOLOCK) WHERE U.FSWS_SUSTITUCION=@USU))'" & vbCrLf
sConsulta = sConsulta & "  ELSE" & vbCrLf
sConsulta = sConsulta & "    SET @SQL=@SQL + ' ( (PT.PER=@USU AND (SELECT ISNULL(U.FSWS_SUSTITUCION,'''') FROM USU U WITH (NOLOCK) WHERE U.PER=@USU AND U.BAJA=0) = ''''))'" & vbCrLf
sConsulta = sConsulta & "  EXEC SP_EXECUTESQL @SQL, N'@USU VARCHAR(50), @ID AS INT,@TITULO VARCHAR(400),@TIPO INT,@FECHADESDE DATETIME,@TIPOFECHA INT,@ORIGEN VARCHAR(50),@IMPORTEDESDE FLOAT,@IMPORTEHASTA FLOAT,@PETICIONARIO VARCHAR(50)," & vbCrLf
sConsulta = sConsulta & "@BUSCARTITULO VARCHAR(400),@BUSCARTIPO INT,@BUSCARFECHADESDE DATETIME,@BUSCARORIGEN VARCHAR(50),@BUSCARIMPORTEDESDE FLOAT,@BUSCARIMPORTEHASTA FLOAT,@BUSCARPETICIONARIO VARCHAR(50),@TIPOS NVARCHAR(500), @FECHAHASTA DATETIME, @PROVEEDOR AS VARCHAR(50), @BUSCARPROVEEDOR AS VARCHAR(50)', @USU=@USU,@ID=@ID,@TITULO=@TITULO,@TIPO=@TIPO,@FECHADESDE=@FECHADESDE,@TIPOFECHA=@TIPOFECHA,@ORIGEN=@ORIGEN,@IMPORTEDESDE=@IMPORTEDESDE,@IMPORTEHASTA=@IMPORTEHASTA,@PETICIONARIO=@PETICIONARIO,@BUSCARTITULO=@BUSCARTITULO,@BUSCARTIPO=@BUSCARTIPO,@BUSCARFECHADESDE=@BUSCARFECHADESDE,@BUSCARORIGEN=@BUSCARORIGEN,@BUSCARIMPORTEDESDE=@BUSCARIMPORTEDESDE,@BUSCARIMPORTEHASTA=@BUSCARIMPORTEHASTA,@BUSCARPETICIONARIO=@BUSCARPETICIONARIO,@TIPOS=@TIPOS,@FECHAHASTA=@FECHAHASTA, @PROVEEDOR=@PROVEEDOR, @BUSCARPROVEEDOR=@BUSCARPROVEEDOR" & vbCrLf
sConsulta = sConsulta & " end" & vbCrLf
sConsulta = sConsulta & "  " & vbCrLf
sConsulta = sConsulta & " --4. Pendientes de devoluci�n" & vbCrLf
sConsulta = sConsulta & "IF @PENDDEVOLUCION=1 AND (@ESTADO='1000' OR @ESTADO='' OR @ESTADO IS NULL)" & vbCrLf
sConsulta = sConsulta & " Begin" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = 'INSERT INTO #T_DEVOLUCION (ID) SELECT I.ID ' + @SQLFROM " & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + ' INNER JOIN INSTANCIA_BLOQUE IB WITH (NOLOCK) ON I.ID=IB.INSTANCIA AND IB.EN_PROCESO=0 AND IB.ESTADO=1 AND IB.BLOQ<>1 AND IB.TRASLADADA=1'" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + @SQLWHERE + ' AND I.NUM IS NOT NULL AND I.TRASLADADA=1'" & vbCrLf
sConsulta = sConsulta & "   IF @SUSTITUTO =1" & vbCrLf
sConsulta = sConsulta & "      SET @SQL = @SQL + ' AND ((TRASLADO_A=@USU AND (SELECT ISNULL(U.FSWS_SUSTITUCION,'''') FROM USU U WITH (NOLOCK) WHERE U.PER=@USU AND U.BAJA=0) = '''') OR (TRASLADO_A IN (SELECT U.PER FROM USU U WITH (NOLOCK) WHERE U.FSWS_SUSTITUCION=@USU)))'" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "      SET @SQL = @SQL + ' AND (TRASLADO_A=@USU AND (SELECT ISNULL(U.FSWS_SUSTITUCION,'''') FROM USU U WITH (NOLOCK) WHERE U.PER=@USU AND U.BAJA=0) = '''')'" & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL, N'@USU VARCHAR(50), @ID AS INT,@TITULO VARCHAR(400),@TIPO INT,@FECHADESDE DATETIME,@TIPOFECHA INT,@ORIGEN VARCHAR(50),@IMPORTEDESDE FLOAT,@IMPORTEHASTA FLOAT,@PETICIONARIO VARCHAR(50)," & vbCrLf
sConsulta = sConsulta & "@BUSCARTITULO VARCHAR(400),@BUSCARTIPO INT,@BUSCARFECHADESDE DATETIME,@BUSCARORIGEN VARCHAR(50),@BUSCARIMPORTEDESDE FLOAT,@BUSCARIMPORTEHASTA FLOAT,@BUSCARPETICIONARIO VARCHAR(50),@TIPOS NVARCHAR(500), @FECHAHASTA DATETIME, @PROVEEDOR AS VARCHAR(50), @BUSCARPROVEEDOR AS VARCHAR(50)', @USU=@USU,@ID=@ID,@TITULO=@TITULO,@TIPO=@TIPO,@FECHADESDE=@FECHADESDE,@TIPOFECHA=@TIPOFECHA,@ORIGEN=@ORIGEN,@IMPORTEDESDE=@IMPORTEDESDE,@IMPORTEHASTA=@IMPORTEHASTA,@PETICIONARIO=@PETICIONARIO,@BUSCARTITULO=@BUSCARTITULO,@BUSCARTIPO=@BUSCARTIPO,@BUSCARFECHADESDE=@BUSCARFECHADESDE,@BUSCARORIGEN=@BUSCARORIGEN,@BUSCARIMPORTEDESDE=@BUSCARIMPORTEDESDE,@BUSCARIMPORTEHASTA=@BUSCARIMPORTEHASTA,@BUSCARPETICIONARIO=@BUSCARPETICIONARIO,@TIPOS=@TIPOS,@FECHAHASTA=@FECHAHASTA, @PROVEEDOR=@PROVEEDOR, @BUSCARPROVEEDOR=@BUSCARPROVEEDOR" & vbCrLf
sConsulta = sConsulta & " End" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--5-Solicitudes trasladadas en espera de devoluci�n" & vbCrLf
sConsulta = sConsulta & "IF @TRASLADADAS =1 AND (@ESTADO='1000' OR @ESTADO='' OR @ESTADO IS NULL)" & vbCrLf
sConsulta = sConsulta & " BEGIN" & vbCrLf
sConsulta = sConsulta & "    SET @SQL = 'INSERT INTO #T_TRASLADADAS (ID) SELECT I.ID ' + @SQLFROM " & vbCrLf
sConsulta = sConsulta & "    SET @SQL=@SQL + ' INNER JOIN INSTANCIA_BLOQUE IB WITH (NOLOCK) ON IB.INSTANCIA=I.ID AND IB.ESTADO=1 AND IB.TRASLADADA=1 AND IB.BLOQ=0'" & vbCrLf
sConsulta = sConsulta & "    SET @SQL = @SQL + @SQLWHERE + '  AND I.NUM IS NOT NULL AND I.TRASLADADA=1 AND ' " & vbCrLf
sConsulta = sConsulta & "    IF @SUSTITUTO =1" & vbCrLf
sConsulta = sConsulta & "      SET @SQL = @SQL + '((DEVOLUCION_A=@USU AND (SELECT ISNULL(U.FSWS_SUSTITUCION,'''') FROM USU U WITH (NOLOCK) WHERE U.PER=@USU AND U.BAJA=0) = '''') OR (DEVOLUCION_A IN ( SELECT U.PER FROM USU U WITH (NOLOCK) WHERE U.FSWS_SUSTITUCION=@USU)))'" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "     SET @SQL = @SQL + '(DEVOLUCION_A=@USU AND (SELECT ISNULL(U.FSWS_SUSTITUCION,'''') FROM USU U WITH (NOLOCK) WHERE U.PER=@USU AND U.BAJA=0) = '''')'" & vbCrLf
sConsulta = sConsulta & "    EXEC SP_EXECUTESQL @SQL, N'@USU VARCHAR(50), @ID AS INT,@TITULO VARCHAR(400),@TIPO INT,@FECHADESDE DATETIME,@TIPOFECHA INT,@ORIGEN VARCHAR(50),@IMPORTEDESDE FLOAT,@IMPORTEHASTA FLOAT,@PETICIONARIO VARCHAR(50)," & vbCrLf
sConsulta = sConsulta & "@BUSCARTITULO VARCHAR(400),@BUSCARTIPO INT,@BUSCARFECHADESDE DATETIME,@BUSCARORIGEN VARCHAR(50),@BUSCARIMPORTEDESDE FLOAT,@BUSCARIMPORTEHASTA FLOAT,@BUSCARPETICIONARIO VARCHAR(50),@TIPOS NVARCHAR(500), @FECHAHASTA DATETIME, @PROVEEDOR AS VARCHAR(50), @BUSCARPROVEEDOR AS VARCHAR(50)', @USU=@USU,@ID=@ID,@TITULO=@TITULO,@TIPO=@TIPO,@FECHADESDE=@FECHADESDE,@TIPOFECHA=@TIPOFECHA,@ORIGEN=@ORIGEN,@IMPORTEDESDE=@IMPORTEDESDE,@IMPORTEHASTA=@IMPORTEHASTA,@PETICIONARIO=@PETICIONARIO,@BUSCARTITULO=@BUSCARTITULO,@BUSCARTIPO=@BUSCARTIPO,@BUSCARFECHADESDE=@BUSCARFECHADESDE,@BUSCARORIGEN=@BUSCARORIGEN,@BUSCARIMPORTEDESDE=@BUSCARIMPORTEDESDE,@BUSCARIMPORTEHASTA=@BUSCARIMPORTEHASTA,@BUSCARPETICIONARIO=@BUSCARPETICIONARIO,@TIPOS=@TIPOS,@FECHAHASTA=@FECHAHASTA, @PROVEEDOR=@PROVEEDOR, @BUSCARPROVEEDOR=@BUSCARPROVEEDOR" & vbCrLf
sConsulta = sConsulta & " END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--6-OTRAS SOLICITUDES (OBSERVADORES)" & vbCrLf
sConsulta = sConsulta & "IF @OTRAS=1" & vbCrLf
sConsulta = sConsulta & " BEGIN" & vbCrLf
sConsulta = sConsulta & "    SET @SQL = 'INSERT INTO #T_OTRAS (ID) SELECT I.ID ' + @SQLFROM " & vbCrLf
sConsulta = sConsulta & "    SET @SQL=@SQL +' INNER JOIN PM_ROL PR WITH (NOLOCK) ON PR.WORKFLOW=S.WORKFLOW'" & vbCrLf
sConsulta = sConsulta & "    SET @SQL=@SQL +' INNER JOIN INSTANCIA_BLOQUE IB WITH (NOLOCK) ON I.ID=IB.INSTANCIA AND IB.EN_PROCESO=0 AND IB.ESTADO=1 AND IB.BLOQ<>1'" & vbCrLf
sConsulta = sConsulta & "    SET @SQL=@SQL +' INNER JOIN PM_COPIA_BLOQUE CB1 WITH(NOLOCK)ON CB1.ID=IB.BLOQUE'" & vbCrLf
sConsulta = sConsulta & "    SET @SQL=@SQL +' INNER JOIN PM_ROL_OBS_ETAPAS ROE WITH(NOLOCK)ON (ROE.ROL=PR.ID AND ROE.BLOQUE=cb1.BLOQUE_ORIGEN) OR PR.VISIBILIDAD_OBSERVADOR=1'" & vbCrLf
sConsulta = sConsulta & "    SET @SQL = @SQL + @SQLWHERE + '  AND I.NUM IS NOT NULL AND PR.TIPO=5 AND (" & vbCrLf
sConsulta = sConsulta & "                                       PR.PER = @USU -- EL USUARIO ES OBSERVADOR" & vbCrLf
sConsulta = sConsulta & "                                       OR EXISTS (SELECT TOP 1 V.COD FROM VIEW_ROL_OBSERVADOR V WITH(NOLOCK) WHERE S.ID = V.SOLICITUD AND V.COD=@USU)" & vbCrLf
sConsulta = sConsulta & "                                       OR PR.UON0 = 1 --EL NIVEL UON0, O SEA, RA�Z (TODA LA ORGANIZACI�N) ES OBSERVADOR" & vbCrLf
sConsulta = sConsulta & "                                       )" & vbCrLf
sConsulta = sConsulta & "                                   AND I.ESTADO>0 AND I.EN_PROCESO<>2'" & vbCrLf
sConsulta = sConsulta & "  EXEC SP_EXECUTESQL @SQL, N'@USU VARCHAR(50), @ID AS INT,@TITULO VARCHAR(400),@TIPO INT,@FECHADESDE DATETIME,@TIPOFECHA INT,@ORIGEN VARCHAR(50),@IMPORTEDESDE FLOAT,@IMPORTEHASTA FLOAT,@PETICIONARIO VARCHAR(50)," & vbCrLf
sConsulta = sConsulta & "@BUSCARTITULO VARCHAR(400),@BUSCARTIPO INT,@BUSCARFECHADESDE DATETIME,@BUSCARORIGEN VARCHAR(50),@BUSCARIMPORTEDESDE FLOAT,@BUSCARIMPORTEHASTA FLOAT,@BUSCARPETICIONARIO VARCHAR(50),@TIPOS NVARCHAR(500), @FECHAHASTA DATETIME, @PROVEEDOR AS VARCHAR(50), @BUSCARPROVEEDOR AS VARCHAR(50)', @USU=@USU,@ID=@ID,@TITULO=@TITULO,@TIPO=@TIPO,@FECHADESDE=@FECHADESDE,@TIPOFECHA=@TIPOFECHA,@ORIGEN=@ORIGEN,@IMPORTEDESDE=@IMPORTEDESDE,@IMPORTEHASTA=@IMPORTEHASTA,@PETICIONARIO=@PETICIONARIO,@BUSCARTITULO=@BUSCARTITULO,@BUSCARTIPO=@BUSCARTIPO,@BUSCARFECHADESDE=@BUSCARFECHADESDE,@BUSCARORIGEN=@BUSCARORIGEN,@BUSCARIMPORTEDESDE=@BUSCARIMPORTEDESDE,@BUSCARIMPORTEHASTA=@BUSCARIMPORTEHASTA,@BUSCARPETICIONARIO=@BUSCARPETICIONARIO,@TIPOS=@TIPOS,@FECHAHASTA=@FECHAHASTA, @PROVEEDOR=@PROVEEDOR, @BUSCARPROVEEDOR=@BUSCARPROVEEDOR" & vbCrLf
sConsulta = sConsulta & " END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "SET @SQL=''" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ABIERTASPORUSTED=1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "  SET @SQL=@SQL + ' SELECT DISTINCT ID,0 as ICONO, 0 as OBV, 0 as ACCION_APROBAR, 0 as ACCION_RECHAZAR, 0 as BLOQUE,0 as OBSYPART FROM #T_ABIERTAS WHERE 1=1'" & vbCrLf
sConsulta = sConsulta & "  IF @PENDIENTES=1 --las abiertas por usted que no est�n pendientes de aprobar por usted:" & vbCrLf
sConsulta = sConsulta & "      SET @SQL=@SQL + ' AND ID NOT IN (SELECT ID FROM #T_PENDIENTES)' " & vbCrLf
sConsulta = sConsulta & "  IF @PENDDEVOLUCION=1 --que no saque las abiertas por usted y que tiene pendientes de devolver" & vbCrLf
sConsulta = sConsulta & "      SET @SQL=@SQL + ' AND ID NOT IN (SELECT ID FROM #T_DEVOLUCION)'" & vbCrLf
sConsulta = sConsulta & "   IF @TRASLADADAS=1 --que no saque las abiertas por usted y que tiene pendientes que le devuelvan" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL + ' AND ID NOT IN (SELECT ID FROM #T_TRASLADADAS)'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @PARTICIPO=1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "  IF @SQL<>''" & vbCrLf
sConsulta = sConsulta & "   BEGIN " & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + ' UNION '" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  SET @SQL=@SQL + ' SELECT DISTINCT P.ID ID,0 as ICONO,0 OBV , 0 as ACCION_APROBAR, 0 as ACCION_RECHAZAR, 0 as BLOQUE,CASE WHEN P.ID=O.ID THEN 1 ELSE 0 END as OBSYPART" & vbCrLf
sConsulta = sConsulta & "  FROM #T_PARTICIPO P " & vbCrLf
sConsulta = sConsulta & "  LEFT JOIN #T_OTRAS O ON P.ID=O.ID WHERE 1=1'" & vbCrLf
sConsulta = sConsulta & "  " & vbCrLf
sConsulta = sConsulta & "  IF @ABIERTASPORUSTED=1" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + ' AND P.ID NOT IN (SELECT ID FROM #T_ABIERTAS)'" & vbCrLf
sConsulta = sConsulta & "  IF @PENDIENTES=1" & vbCrLf
sConsulta = sConsulta & "    SET @SQL=@SQL + ' AND P.ID NOT IN (SELECT ID FROM #T_PENDIENTES)'" & vbCrLf
sConsulta = sConsulta & "  IF @PENDDEVOLUCION=1 " & vbCrLf
sConsulta = sConsulta & "    SET @SQL=@SQL + ' AND P.ID NOT IN (SELECT ID FROM #T_DEVOLUCION)'" & vbCrLf
sConsulta = sConsulta & "  IF @TRASLADADAS=1 " & vbCrLf
sConsulta = sConsulta & "    SET @SQL=@SQL + ' AND P.ID NOT IN (SELECT ID FROM #T_TRASLADADAS)'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--3. Pendientes de tratar" & vbCrLf
sConsulta = sConsulta & "IF @PENDIENTES=1 AND (@ESTADO='1000' OR @ESTADO='' OR @ESTADO IS NULL)" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "  IF @SQL<>''" & vbCrLf
sConsulta = sConsulta & "   BEGIN " & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + ' UNION '" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  SET @SQL=@SQL + ' SELECT DISTINCT ID,ICONO, OBV,ACCION_APROBAR,ACCION_RECHAZAR, BLOQUE,0 as OBSYPART FROM #T_PENDIENTES'  " & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--4. Pendientes de devoluci�n" & vbCrLf
sConsulta = sConsulta & "IF @PENDDEVOLUCION=1 AND (@ESTADO='1000' OR @ESTADO='' OR @ESTADO='2' OR @ESTADO IS NULL)" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "  IF @SQL<>''" & vbCrLf
sConsulta = sConsulta & "   BEGIN " & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + ' UNION '" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "    SET @SQL=@SQL + ' SELECT DISTINCT ID,1 as ICONO, 0 as OBV, 0 as ACCION_APROBAR, 0 as ACCION_RECHAZAR, 0 as BLOQUE,0 as OBSYPART FROM #T_DEVOLUCION'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--5-Solicitudes trasladadas en espera de devoluci�n" & vbCrLf
sConsulta = sConsulta & "IF @TRASLADADAS =1 AND (@ESTADO='1000' OR @ESTADO='' OR @ESTADO='2' OR @ESTADO IS NULL)" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "  IF @SQL<>''" & vbCrLf
sConsulta = sConsulta & "   BEGIN " & vbCrLf
sConsulta = sConsulta & "     SET @SQL=@SQL + ' UNION'" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "    SET @SQL=@SQL +' SELECT DISTINCT ID,1 as ICONO, 0 as OBV, 0 as ACCION_APROBAR, 0 as ACCION_RECHAZAR, 0 as BLOQUE,0 as OBSYPART FROM #T_TRASLADADAS'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--6-OTRAS SOLICITUDES (OBSERVADORES)" & vbCrLf
sConsulta = sConsulta & "IF @OTRAS=1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "  IF @SQL<>''" & vbCrLf
sConsulta = sConsulta & "   BEGIN " & vbCrLf
sConsulta = sConsulta & "     SET @SQL=@SQL + ' UNION'" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "    SET @SQL=@SQL +' SELECT DISTINCT ID,0 as ICONO, 1 as OBV, 0 as ACCION_APROBAR, 0 as ACCION_RECHAZAR, 0 as BLOQUE,0 as OBSYPART FROM #T_OTRAS WHERE 1=1'" & vbCrLf
sConsulta = sConsulta & "   IF @PENDIENTES=1 --las que no est�n pendientes de aprobar por usted:" & vbCrLf
sConsulta = sConsulta & "      SET @SQL=@SQL + ' AND ID NOT IN (SELECT ID FROM #T_PENDIENTES)'" & vbCrLf
sConsulta = sConsulta & "   IF @PENDDEVOLUCION=1 --las que tiene pendientes de devolver" & vbCrLf
sConsulta = sConsulta & "      SET @SQL=@SQL + ' AND ID NOT IN (SELECT ID FROM #T_DEVOLUCION)'" & vbCrLf
sConsulta = sConsulta & "   IF @TRASLADADAS=1 -- las que tiene pendientes que le devuelvan" & vbCrLf
sConsulta = sConsulta & "     SET @SQL=@SQL + ' AND ID NOT IN (SELECT ID FROM #T_TRASLADADAS)'" & vbCrLf
sConsulta = sConsulta & "   IF @PARTICIPO=1 --las que no est�n pendientes de aprobar por usted:" & vbCrLf
sConsulta = sConsulta & "     SET @SQL=@SQL + ' AND ID NOT IN (SELECT ID FROM #T_PARTICIPO)'   " & vbCrLf
sConsulta = sConsulta & "   IF @ABIERTASPORUSTED=1" & vbCrLf
sConsulta = sConsulta & "     SET @SQL=@SQL + ' AND ID NOT IN (SELECT ID FROM #T_ABIERTAS)'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @SQL<>''" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & " --crea la tabla temporal" & vbCrLf
sConsulta = sConsulta & " SET @SQL='INSERT INTO #G1 (ID,ICONO,OBV,ACCION_APROBAR,ACCION_RECHAZAR,BLOQUE,OBSYPART)' + @SQL" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & " EXEC SP_EXECUTESQL @SQL" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & " DROP TABLE #T_ABIERTAS" & vbCrLf
sConsulta = sConsulta & " DROP TABLE #T_PARTICIPO" & vbCrLf
sConsulta = sConsulta & " DROP TABLE #T_PENDIENTES" & vbCrLf
sConsulta = sConsulta & " DROP TABLE #T_DEVOLUCION" & vbCrLf
sConsulta = sConsulta & " DROP TABLE #T_TRASLADADAS" & vbCrLf
sConsulta = sConsulta & " DROP TABLE #T_OTRAS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "----********************************************************************************************************************************************************************************************************" & vbCrLf
sConsulta = sConsulta & "SET @SQLFROMBUSQUEDA = N''" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--*************************************************SELECT GEN�RICA PARA SACAR LOS DATOS ***********************************************************" & vbCrLf
sConsulta = sConsulta & "--Creamos una tabla temporal" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE #G2 (TIPO VARCHAR(50), DEN VARCHAR(500) , ID INT, FECHA DATETIME, ESTADO INT, ETAPA_ACTUAL VARCHAR(100), ETAPA INT, EN_PROCESO TINYINT, VER_DETALLE_PER TINYINT, IMPORTE FLOAT,MON VARCHAR(10), PETICIONARIO VARCHAR(50), PET VARCHAR(150),VER_FLUJO TINYINT, KO TINYINT, ERROR VARCHAR(500)," & vbCrLf
sConsulta = sConsulta & "EST_VALIDACION INT, ERROR_VALIDACION VARCHAR(500), FECHA_VALIDACION DATETIME, BLOQUE INT, USUARIO VARCHAR(50), USU VARCHAR(100), DEP VARCHAR(100), PROVEEDOR VARCHAR(100), NUM_REEMISIONES INT, TIPO_SOLIC INT,TRASLADADA TINYINT, ULT_VERSION INT, ICONO TINYINT, OBSERVADOR TINYINT, ACCION_APROBAR INT,ACCION_RECHAZAR INT,VER_DETALLE_PER_ROL TINYINT," & vbCrLf
sConsulta = sConsulta & "TRASLADO_A_NOMBRE VARCHAR(100), TRASLADO_A_PROV VARCHAR(50), TRASLADO_A_USU VARCHAR(50), FECHA_ACT DATETIME, FECHA_LIMITE DATETIME, FEC_FIN_FLUJO DATETIME, OBSYPART TINYINT)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'SELECT DISTINCT S.COD as TIPO, CASE WHEN C1.TITULO_FEC IS NULL THEN C1.TITULO_TEXT ELSE CONVERT(VARCHAR ,C1.TITULO_FEC) END AS DEN, '" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'I.ID AS ID,I.FEC_ALTA AS FECHA, I.ESTADO'" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ', CASE WHEN M.P>1 THEN ''##'' ELSE CASE WHEN G1.BLOQUE <> 0 THEN ISNULL(CB2.DEN,'' '') ELSE ISNULL(CB.DEN,'' '') END END AS ETAPA_ACTUAL, I.ESTADO AS ETAPA,I.EN_PROCESO'" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ',ISNULL(CR.VDP,1),'  --ver detalle de persona" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'I.IMPORTE,I.MON,I.PETICIONARIO,P.NOM+'' ''+P.APE AS PET'" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ',CR.VER_FLUJO,INT.KO,INT.ERROR, isnull(I.EST_VALIDACION,0) EST_VALIDACION,I.ERROR_VALIDACION,I.FECHA_VALIDACION '" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ', CASE WHEN G1.BLOQUE <> 0 THEN G1.BLOQUE ELSE M.BLOQUE END AS BLOQUE,'" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ' CASE WHEN I.ESTADO=2 THEN CASE WHEN PCB.TIPO = 1 THEN I.PETICIONARIO ELSE CASE WHEN R5.PER IS NOT NULL THEN R5.PER END END ELSE CASE WHEN IE.PER IS NULL THEN I.PETICIONARIO ELSE IE.PER END END AS USUARIO'" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ', CASE WHEN I.ESTADO=2 THEN CASE WHEN PCB.TIPO = 1 THEN P.NOM  +'' '' + P.APE ELSE CASE WHEN R5.PER IS NOT NULL THEN P3.NOM  + '' '' + P3.APE ELSE PV.DEN END END ELSE CASE WHEN IE.PER IS NULL THEN P.NOM  +'' '' + P.APE ELSE P2.NOM  + '' '' + P2.APE END END AS USU'" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ', CASE WHEN I.ESTADO=2 THEN CASE WHEN PCB.TIPO = 1 THEN D.DEN ELSE CASE WHEN R5.PER IS NOT NULL THEN D3.DEN ELSE NULL END END ELSE CASE WHEN IE.PER IS NULL THEN D.DEN ELSE D2.DEN END END AS DEP'" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ', CASE WHEN I.ESTADO=2 THEN PV.COD ELSE NULL END AS PROVEEDOR'" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ',  I.NUM_REEMISIONES,TS.TIPO AS TIPO_SOLIC,I.TRASLADADA,I.ULT_VERSION'" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ', G1.ICONO,OBV,ACCION_APROBAR,ACCION_RECHAZAR,ISNULL(CR1.VDP,1)'" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ', CASE WHEN I.TRASLADADA=1 THEN CASE WHEN IE.DESTINATARIO_PROV IS NOT NULL THEN PV2.DEN ELSE P4.NOM  +'' '' + P4.APE    END ELSE NULL END AS TRASLADO_A_NOMBRE'" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ', IE.DESTINATARIO_PROV, IE.DESTINATARIO, IE.FECHA ,IE.FEC_LIMITE, I.FEC_FIN_FLUJO,G1.OBSYPART'" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL +  ' FROM INSTANCIA I WITH (NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL +  ' INNER JOIN #G1 G1 WITH(NOLOCK) ON I.ID=G1.ID'" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL +  ' INNER JOIN SOLICITUD S WITH (NOLOCK) ON I.SOLICITUD=S.ID'   --para sacar la solicitud" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL +  ' INNER JOIN TIPO_SOLICITUDES TS WITH (NOLOCK) ON S.TIPO=TS.ID'   --para sacar el tipo de solicitud" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL +  ' LEFT JOIN INSTANCIA_VISORES_PM C1 WITH (NOLOCK) ON I.ID=C1.INSTANCIA '   --para mostrar el t�tulo" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL +  ' LEFT JOIN PM_FILTROS_AUX C2 WITH(NOLOCK) ON I.ID=C2.INSTANCIA '" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL +  ' LEFT JOIN INSTANCIA_EST IE WITH(NOLOCK)ON I.INSTANCIA_EST=IE.ID' --saca la �ltima acci�n realizada (fecha y usuario)" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL +  ' INNER JOIN PER P WITH (NOLOCK) ON P.COD = I.PETICIONARIO '  --para el nombre del peticionario" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL +  ' INNER JOIN DEP D WITH (NOLOCK) ON P.DEP=D.COD '  --para el departamento del peticionario" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL +  ' LEFT JOIN PER P2 WITH(NOLOCK) ON IE.PER=P2.COD' --para el nombre del �ltimo aprobador" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL +  ' LEFT JOIN DEP D2 WITH(NOLOCK) ON P2.DEP=D2.COD' --para el departamento del �ltimo aprobador" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL +  ' LEFT JOIN INSTANCIA_INT INT WITH(NOLOCK) ON I.ID=INT.ID'  --para el estado de integraci�n" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Mira si la persona es un rol de la instancia, y en ese caso si tiene permisos para ver el detalle del flujo y el detalle de los participantes:" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' LEFT JOIN(SELECT INSTANCIA I,PER P,MAX(VER_DETALLE_PER)VDP,MAX(VER_FLUJO)VER_FLUJO FROM PM_COPIA_ROL WITH(NOLOCK) GROUP BY INSTANCIA,PER) CR ON I.ID = CR.I AND @USU=CR.P'" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' LEFT JOIN(SELECT PER P,MAX(VER_DETALLE_PER)VDP,MAX(VER_FLUJO) VER_FLUJO,WORKFLOW FROM PM_ROL WITH(NOLOCK) GROUP BY PER,WORKFLOW) CR1 ON  @USU=CR1.P AND CR1.WORKFLOW=S.WORKFLOW '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Para obtener si hay etapas en paralelo" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ' INNER JOIN (SELECT INSTANCIA I, COUNT(*) P,MIN(BLOQUE) BLOQUE FROM INSTANCIA_BLOQUE  WITH (NOLOCK)  WHERE ESTADO=1 AND BLOQ<>1 GROUP BY INSTANCIA) M ON I.ID=M.I'" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ' LEFT JOIN PM_COPIA_BLOQUE_DEN CB WITH(NOLOCK) ON M.BLOQUE=CB.BLOQUE AND CB.IDI= @IDI'  --denominaci�n del bloque actual." & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ' LEFT JOIN PM_COPIA_BLOQUE PCB WITH(NOLOCK) ON M.BLOQUE=PCB.ID' --para saber si estamos o no en la etapa peticionario" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ' LEFT JOIN (SELECT P.BLOQUE, MIN(P.ROL) ROL FROM PM_COPIA_ROL_BLOQUE P WITH(NOLOCK)  GROUP BY P.BLOQUE) RB5 ON RB5.BLOQUE=M.BLOQUE '" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ' LEFT JOIN PM_COPIA_BLOQUE_DEN CB2 WITH(NOLOCK) ON G1.BLOQUE=CB2.BLOQUE AND CB2.IDI= @IDI'  --denominaci�n del bloque actual." & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ' LEFT JOIN PM_COPIA_ROL R5 WITH(NOLOCK)ON RB5.ROL=R5.ID' --Rol" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ' LEFT JOIN PER P3 WITH(NOLOCK)ON R5.PER=P3.COD'   --nombre del rol actual" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ' LEFT JOIN DEP D3 WITH(NOLOCK)ON P3.DEP=D3.COD'  --departamento del rol actual" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ' LEFT JOIN PROVE PV WITH(NOLOCK)ON R5.PROVE=PV.COD'  --c�digo del proveedor del rol actual" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ' LEFT JOIN PER P4 WITH(NOLOCK)ON IE.DESTINATARIO=P4.COD'   --nombre del usuario destinatario del traslado" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ' LEFT JOIN PROVE PV2 WITH(NOLOCK)ON IE.DESTINATARIO_PROV=PV2.COD'  --c�digo del proveedor destinatario del traslado" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF (@NOM_TABLA IS NULL OR @NOM_TABLA='' ) AND @ABIERTASPORUSTED=1 AND (@ESTADO='0' OR @ESTADO IS NULL)" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       --para las solicitudes que solo tienen registro en la tabla INSTANCIA y todav�a no las ha procesado el WebService" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + ' union all SELECT DISTINCT S.COD, S.DEN_' + @IDI + ', I.ID, I.FEC_ALTA AS FECHA, I.ESTADO, NULL, I.ESTADO, I.EN_PROCESO, NULL, I.IMPORTE,I.MON,I.PETICIONARIO,P.NOM+'' ''+P.APE AS PET,NULL,NULL,NULL, isnull(I.EST_VALIDACION,0) as EST_VALIDACION,I.ERROR_VALIDACION,I.FECHA_VALIDACION,'" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + '0,NULL,NULL, NULL, NULL,0,TS.TIPO AS TIPO_SOLIC,I.TRASLADADA,I.ULT_VERSION, 0 AS ICONO,0 as OBV, 0 as ACCION_APROBAR, 0 as ACCION_RECHAZAR, NULL, NULL, NULL, NULL, NULL, NULL, NULL,0 FROM INSTANCIA I WITH(NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "   IF (@TITULO IS NOT NULL AND @TITULO <> '') OR (@BUSQUEDA_AUX =0 AND @BUSCARTITULO IS NOT NULL AND @BUSCARTITULO <> '')" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = @SQL + 'INNER JOIN INSTANCIA_VISORES_PM C1 WITH(NOLOCK) ON I.ID=C1.INSTANCIA '" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "     BEGIN" & vbCrLf
sConsulta = sConsulta & "     IF @BUSQUEDA_AUX=1" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = @SQL + ' INNER JOIN PM_FILTROS_AUX C2 WITH(NOLOCK) ON I.ID=C2.INSTANCIA '" & vbCrLf
sConsulta = sConsulta & "     END " & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + 'INNER JOIN PER P WITH(NOLOCK) ON P.COD = I.PETICIONARIO '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + 'INNER JOIN DEP D WITH (NOLOCK) ON P.DEP=D.COD '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + 'INNER JOIN SOLICITUD S WITH(NOLOCK) ON I.SOLICITUD=S.ID INNER JOIN TIPO_SOLICITUDES TS WITH (NOLOCK) ON S.TIPO=TS.ID'" & vbCrLf
sConsulta = sConsulta & "    IF (@ORIGEN IS NOT NULL AND @ORIGEN <> '') OR  (@BUSCARORIGEN IS NOT NULL AND @BUSCARORIGEN <> '')" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "         SET @SQL = @SQL + ' INNER JOIN PER P WITH(NOLOCK) ON P.COD = I.PETICIONARIO'" & vbCrLf
sConsulta = sConsulta & "         SET @SQL = @SQL + ' INNER JOIN DEP WITH (NOLOCK) ON P.DEP=DEP.COD'" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "   IF ((@PROVEEDOR IS NOT NULL) AND (@PROVEEDOR<>'')) OR ((@BUSCARPROVEEDOR IS NOT NULL) AND (@BUSCARPROVEEDOR<>''))" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = @SQL + ' INNER JOIN INSTANCIA_DESGLOSE_MATERIAL IDM WITH(NOLOCK) ON I.ID=IDM.INSTANCIA'" & vbCrLf
sConsulta = sConsulta & "   END       " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + @SQLWHERE +  ' AND PETICIONARIO = @USU AND I.ULT_VERSION IS NULL AND I.NUM IS NULL AND EN_PROCESO=1 AND TS.TIPO NOT IN (2,3)'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "--***********************************************************************************************************************************" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL2= N' INSERT INTO #G2 ' + @SQL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL2, N'@IDI NVARCHAR(20),@USU NVARCHAR(50),@ID AS INT,@TITULO NVARCHAR(400),@TIPO INT,@FECHADESDE DATETIME,@TIPOFECHA INT,@ORIGEN NVARCHAR(50),@IMPORTEDESDE FLOAT,@IMPORTEHASTA FLOAT,@PETICIONARIO NVARCHAR(50)," & vbCrLf
sConsulta = sConsulta & "@BUSCARTITULO NVARCHAR(400),@BUSCARTIPO INT,@BUSCARFECHADESDE DATETIME,@BUSCARORIGEN NVARCHAR(50),@BUSCARIMPORTEDESDE FLOAT,@BUSCARIMPORTEHASTA FLOAT,@BUSCARPETICIONARIO NVARCHAR(50),@TIPOS NVARCHAR(500), @FECHAHASTA DATETIME,@PROVEEDOR NVARCHAR(50),@BUSCARPROVEEDOR NVARCHAR(50)', @IDI=@IDI,@USU=@USU,@ID=@ID,@TITULO=@TITULO,@TIPO=@TIPO,@FECHADESDE=@FECHADESDE,@TIPOFECHA=@TIPOFECHA,@ORIGEN=@ORIGEN,@IMPORTEDESDE=@IMPORTEDESDE,@IMPORTEHASTA=@IMPORTEHASTA,@PETICIONARIO=@PETICIONARIO,@BUSCARTITULO=@BUSCARTITULO,@BUSCARTIPO=@BUSCARTIPO,@BUSCARFECHADESDE=@BUSCARFECHADESDE,@BUSCARORIGEN=@BUSCARORIGEN,@BUSCARIMPORTEDESDE=@BUSCARIMPORTEDESDE,@BUSCARIMPORTEHASTA=@BUSCARIMPORTEHASTA,@BUSCARPETICIONARIO=@BUSCARPETICIONARIO,@TIPOS=@TIPOS,@FECHAHASTA=@FECHAHASTA,@PROVEEDOR=@PROVEEDOR,@BUSCARPROVEEDOR=@BUSCARPROVEEDOR" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "----********************************************************************************************************************************************************************************************************" & vbCrLf
sConsulta = sConsulta & "IF @PROCESOS = 1   --Si se muestra la informaci�n de procesos" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   DECLARE @proceso_pedido varchar(50)" & vbCrLf
sConsulta = sConsulta & "   DECLARE @instancia int" & vbCrLf
sConsulta = sConsulta & "   declare @ProcesosPedidos_instancia NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "   declare @INSTANCIA_ANT int" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   DECLARE Procesos_cursor cursor for" & vbCrLf
sConsulta = sConsulta & "   SELECT DISTINCT PG.SOLICIT INSTANCIA,CONVERT(VARCHAR,PG.ANYO) +'/' + CONVERT(VARCHAR,PG.GMN1) +'/'+ CONVERT(VARCHAR,P.COD)  Proceso" & vbCrLf
sConsulta = sConsulta & "   FROM PROCE P WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PROCE_GRUPO PG WITH(NOLOCK) ON P.ANYO = PG.ANYO AND P.GMN1 = PG.GMN1 AND P.COD = PG.PROCE" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN #G1 ON PG.SOLICIT=#G1.ID" & vbCrLf
sConsulta = sConsulta & "   WHERE PG.SOLICIT IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   union all" & vbCrLf
sConsulta = sConsulta & "   SELECT DISTINCT I.SOLICIT INSTANCIA,CONVERT(VARCHAR,P2.ANYO) +'/' + CONVERT(VARCHAR,P2.GMN1 ) +'/'+ CONVERT(VARCHAR,P2.COD)  Proceso" & vbCrLf
sConsulta = sConsulta & "   FROM PROCE P2 WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN ITEM I WITH(NOLOCK) ON P2.ANYO = I.ANYO AND P2.GMN1 = I.GMN1_PROCE AND P2.COD = I.PROCE" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN #G1 ON I.SOLICIT=#G1.ID" & vbCrLf
sConsulta = sConsulta & "   WHERE I.SOLICIT IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   ORDER BY INSTANCIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @INSTANCIA_ANT = 0" & vbCrLf
sConsulta = sConsulta & "   SET @ProcesosPedidos_instancia=''" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   OPEN Procesos_cursor FETCH NEXT FROM Procesos_cursor INTO @instancia,@proceso_pedido" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    --Crea tablas temporales para tener la relaci�n de los campos almacenados en la temporal con el id nuevo:" & vbCrLf
sConsulta = sConsulta & "   CREATE TABLE #T1 (I INT, PRO nVARCHAR(2000))" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   ---------------------------------------PROCESOS" & vbCrLf
sConsulta = sConsulta & "   WHILE @@FETCH_STATUS = 0" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "      IF @INSTANCIA_ANT = 0" & vbCrLf
sConsulta = sConsulta & "           SET @ProcesosPedidos_instancia = @proceso_pedido" & vbCrLf
sConsulta = sConsulta & "      ELSE" & vbCrLf
sConsulta = sConsulta & "           IF (@INSTANCIA = @INSTANCIA_ANT)" & vbCrLf
sConsulta = sConsulta & "          SET @ProcesosPedidos_instancia = @ProcesosPedidos_instancia  + '; ' + @proceso_pedido" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "           else" & vbCrLf
sConsulta = sConsulta & "          begin" & vbCrLf
sConsulta = sConsulta & "              INSERT INTO #T1  VALUES (@INSTANCIA_ANT,@ProcesosPedidos_instancia)" & vbCrLf
sConsulta = sConsulta & "              SET @ProcesosPedidos_instancia=@proceso_pedido" & vbCrLf
sConsulta = sConsulta & "          end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "      set @INSTANCIA_ANT = @instancia" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   FETCH NEXT FROM Procesos_cursor  INTO @INSTANCIA,@PROCESO_PEDIDO" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @ProcesosPedidos_instancia != ''" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "      INSERT INTO #T1 VALUES (@INSTANCIA_ANT,@ProcesosPedidos_instancia)" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   CLOSE Procesos_cursor" & vbCrLf
sConsulta = sConsulta & "   DEALLOCATE Procesos_cursor" & vbCrLf
sConsulta = sConsulta & "   -------------------------------------------------------PEDIDOS" & vbCrLf
sConsulta = sConsulta & "   IF @OBLCODPEDDIR = 1" & vbCrLf
sConsulta = sConsulta & "       DECLARE Pedidos_cursor cursor for" & vbCrLf
sConsulta = sConsulta & "       SELECT DISTINCT LP.SOLICIT INSTANCIA ,CONVERT(VARCHAR,P.ANYO) + '/' + CONVERT(VARCHAR,P.NUM)+ '/'+ CONVERT(VARCHAR,O.NUM) + CASE WHEN COALESCE(O.NUM_PED_ERP,'') = '' THEN '' ELSE ' (' + O.NUM_PED_ERP + ')' END Pedido" & vbCrLf
sConsulta = sConsulta & "       FROM PEDIDO P WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN ORDEN_ENTREGA O WITH(NOLOCK) ON P.ID = O.PEDIDO" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PROVE PR WITH(NOLOCK) ON O.PROVE = PR.COD" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN LINEAS_PEDIDO LP WITH(NOLOCK) ON O.ID = LP.ORDEN" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN #G1 ON LP.SOLICIT=#G1.ID" & vbCrLf
sConsulta = sConsulta & "       WHERE LP.SOLICIT is not null" & vbCrLf
sConsulta = sConsulta & "       ORDER BY INSTANCIA" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "       DECLARE Pedidos_cursor cursor for" & vbCrLf
sConsulta = sConsulta & "       SELECT DISTINCT LP.SOLICIT INSTANCIA ,CONVERT(VARCHAR,P.ANYO) + '/' + CONVERT(VARCHAR,P.NUM)+ '/'+ CONVERT(VARCHAR,O.NUM) Pedido" & vbCrLf
sConsulta = sConsulta & "       FROM PEDIDO P WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN ORDEN_ENTREGA O WITH(NOLOCK) ON P.ID = O.PEDIDO" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PROVE PR WITH(NOLOCK) ON O.PROVE = PR.COD" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN LINEAS_PEDIDO LP WITH(NOLOCK) ON O.ID = LP.ORDEN" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN #G1 ON LP.SOLICIT=#G1.ID" & vbCrLf
sConsulta = sConsulta & "       WHERE LP.SOLICIT is not null" & vbCrLf
sConsulta = sConsulta & "       ORDER BY INSTANCIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @INSTANCIA_ANT = 0" & vbCrLf
sConsulta = sConsulta & "   SET @ProcesosPedidos_instancia=''" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   OPEN Pedidos_cursor FETCH NEXT FROM Pedidos_cursor INTO @instancia,@proceso_pedido" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    --Crea tablas temporales para tener la relaci�n de los campos almacenados en la temporal con el id nuevo:" & vbCrLf
sConsulta = sConsulta & "   CREATE TABLE #T2 (I INT, PED NVARCHAR(2000))" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   WHILE @@FETCH_STATUS = 0" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "      IF @INSTANCIA_ANT = 0" & vbCrLf
sConsulta = sConsulta & "           set @ProcesosPedidos_instancia = @proceso_pedido" & vbCrLf
sConsulta = sConsulta & "      ELSE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "           IF (@INSTANCIA = @INSTANCIA_ANT)" & vbCrLf
sConsulta = sConsulta & "          SET @ProcesosPedidos_instancia = @ProcesosPedidos_instancia  + '; ' + @proceso_pedido" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "           ELSE" & vbCrLf
sConsulta = sConsulta & "           begin" & vbCrLf
sConsulta = sConsulta & "      INSERT INTO #T2  VALUES (@INSTANCIA_ANT,@ProcesosPedidos_instancia)" & vbCrLf
sConsulta = sConsulta & "          SET @ProcesosPedidos_instancia=@proceso_pedido" & vbCrLf
sConsulta = sConsulta & "           end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "      set @INSTANCIA_ANT = @instancia" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   FETCH NEXT FROM Pedidos_cursor  INTO  @INSTANCIA,@PROCESO_PEDIDO" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @ProcesosPedidos_instancia != ''" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "      INSERT INTO #T2 VALUES (@INSTANCIA_ANT,@ProcesosPedidos_instancia)" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   CLOSE Pedidos_cursor" & vbCrLf
sConsulta = sConsulta & "   DEALLOCATE Pedidos_cursor" & vbCrLf
sConsulta = sConsulta & "   ------------------------------------" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- *****LA SELECT FINAL ********" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--CALIDAD: SE USA SELECT * PORQUE LOS CAMPOS PUEDEN VARIAR" & vbCrLf
sConsulta = sConsulta & "set @SQL='SELECT DISTINCT #G2.*, isnull(SEG,0) as SEG'" & vbCrLf
sConsulta = sConsulta & "IF @NOM_TABLA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "      SET @SQL = @SQL + ', FILTRO.* '" & vbCrLf
sConsulta = sConsulta & "IF @PROCESOS = 1" & vbCrLf
sConsulta = sConsulta & "   set @SQL=@SQL + ',TP.PRO AS PROCESOS,TE.PED AS PEDIDOS FROM #G2 LEFT JOIN #T1 TP WITH(NOLOCK) ON TP.I=#G2.ID LEFT JOIN #T2 TE WITH(NOLOCK) ON TE.I=#G2.ID'" & vbCrLf
sConsulta = sConsulta & "else" & vbCrLf
sConsulta = sConsulta & "   set @SQL=@SQL + ',NULL AS PROCESOS, NULL AS PEDIDOS FROM #G2'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ' LEFT JOIN INSTANCIA_ADICIONAL IA WITH(NOLOCK) ON #G2.ID=IA.INSTANCIA AND IA.PER=@USU '" & vbCrLf
sConsulta = sConsulta & "IF @NOM_TABLA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + ' INNER JOIN ' + @IDI + '_' + @NOM_TABLA + ' FILTRO WITH(NOLOCK) ON #G2.ID = FILTRO.INSTANCIA '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF (@BUSCARARTICULO IS NOT NULL AND @BUSCARARTICULO <> '') OR (@ARTICULO IS NOT NULL AND @ARTICULO <> '')" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @SQLART1 = ' INNER JOIN INSTANCIA_DESGLOSE_MATERIAL IDM WITH(NOLOCK) ON #G2.ID = IDM.INSTANCIA '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @ARTICULO IS NOT NULL AND @ARTICULO <> ''" & vbCrLf
sConsulta = sConsulta & "       SET @SQLART1=  @SQLART1 + '  AND IDM.ART4 LIKE ''%' + @ARTICULO + '%'''" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "       SET @SQLART1=  @SQLART1 + ' AND IDM.ART4 LIKE ''%' + @BUSCARARTICULO + '%'''        " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=  @SQL + @SQLART1" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ORDENACION='ID' OR @ORDENACION='TIPO'" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + ' ORDER BY #G2.' + @ORDENACION + ' ' + @SENTIDOORDENACION" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & " begin" & vbCrLf
sConsulta = sConsulta & "      IF @ORDENACION IS NOT NULL AND @ORDENACION <> ''" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + ' ORDER BY ' + @ORDENACION + ' ' + @SENTIDOORDENACION" & vbCrLf
sConsulta = sConsulta & " end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL,N'@USU NVARCHAR(50)',@USU=@USU" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Borra las tablas temporales" & vbCrLf
sConsulta = sConsulta & "IF @PROCESOS = 1" & vbCrLf
sConsulta = sConsulta & " begin" & vbCrLf
sConsulta = sConsulta & "  DROP TABLE #T2" & vbCrLf
sConsulta = sConsulta & "  DROP TABLE #T1" & vbCrLf
sConsulta = sConsulta & " end" & vbCrLf
sConsulta = sConsulta & "DROP TABLE #G1" & vbCrLf
sConsulta = sConsulta & "DROP TABLE #G2" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSGSW_DEVOLVER_ITEMSGRUPO]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSGSW_DEVOLVER_ITEMSGRUPO]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSGSW_DEVOLVER_ITEMSGRUPO] @ANYO INT, @GMN1 VARCHAR(50), @PROCE INT, @CODGRUPO VARCHAR(20),@PROVE VARCHAR(50),@IDEMP INT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @EST INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PARAM AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @ERP INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ESCALADOS INT=0" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDGRUPO INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @USAR_ART4_UON INT=0" & vbCrLf
sConsulta = sConsulta & "DECLARE @TRASPASO_PLANIF INT=0" & vbCrLf
sConsulta = sConsulta & "DECLARE @USAR_ORGCOMPRAS INT =0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @ESCALADOS=COUNT(ID) FROM PROCE_GRUPO PG WITH (NOLOCK) WHERE PG.ANYO=@ANYO  AND PG.GMN1=@GMN1 AND PG.PROCE=@PROCE AND PG.COD=@CODGRUPO AND PG.ESCALADOS=1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @EST=EST FROM PROCE P WITH (NOLOCK) WHERE ANYO =@ANYO AND COD=@PROCE AND GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @ERP=ES.ERP FROM ERP_SOCIEDAD ES WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN EMP E WITH (NOLOCK) ON E.SOCIEDAD=ES.SOCIEDAD AND E.ID=@IDEMP" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "SELECT @IDGRUPO=ID FROM PROCE_GRUPO PG WITH (NOLOCK) WHERE PG.ANYO=@ANYO AND PG.GMN1=@GMN1 AND PG.PROCE=@PROCE AND PG.COD=@CODGRUPO " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @USAR_ART4_UON=USAR_ART4_UON, @TRASPASO_PLANIF=TRASPASO_ART_PLANIF, @USAR_ORGCOMPRAS=USAR_ORGCOMPRAS  FROM PARGEN_INTERNO WITH(NOLOCK) WHERE ID=1" & vbCrLf
sConsulta = sConsulta & "    " & vbCrLf
sConsulta = sConsulta & "   SET @SQL='SELECT DISTINCT COALESCE(LIA.ID,LIA1.ID) LIA_ID,I.ID ID_ITEM,I.ART CODART,I.DESCR DENART,I.UNI,'" & vbCrLf
sConsulta = sConsulta & "   if  @ESCALADOS>0 " & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + ' PGE.ID ESC_ID,PGE.INICIO,PGE.FIN,'" & vbCrLf
sConsulta = sConsulta & "   else" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + 'IA.CANT_ADJ,'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + ' IOF.PREC_VALIDO PREC_ADJ,P.MON,'" & vbCrLf
sConsulta = sConsulta & "   IF @USAR_ORGCOMPRAS =1  --Si el �ltimo traspaso es un borrado, lee lo que haya en el proceso. Si no (era una I), lee lo de log_item_adj" & vbCrLf
sConsulta = sConsulta & "      SET @SQL=@SQL + ' CASE WHEN LIA3.ACCION=''D'' THEN IT_CEN.CENTRO ELSE COALESCE(LIA.CENTRO,it_cen.CENTRO,'''') end CENTROS,'''' AS CENTRO,'" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "      SET @SQL=@SQL + ''''' CENTROS,'''' AS CENTRO,'" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + ' CASE WHEN LIA3.ACCION = ''D''  THEN NULL ELSE LG.ESTADO END AS ESTADOINT  " & vbCrLf
sConsulta = sConsulta & "   , I.FECINI FECINI_SUM,I.FECFIN FECFIN_SUM,LIA.PROVE_ERP,PE.DEN_ERP," & vbCrLf
sConsulta = sConsulta & "   COALESCE(IT_ATRIB1.INT_ATRIB_ID ,PA.ATRIB,IAE.ATRIB,PGA.ATRIB) ATRIBID,DA.COD ATRIBCOD,DA.DEN ATRIBDEN,COALESCE(IT_ATRIB1.VALOR_TEXT ,IAE.VALOR_TEXT,OA.VALOR_TEXT,PGA.VALOR_TEXT,IAT.VALOR_TEXT) ATRIBVALOR_TEXT ,COALESCE(IT_ATRIB1.VALOR_NUM ,IAE.VALOR_NUM,OA.VALOR_NUM,PGA.VALOR_NUM,IAT.VALOR_NUM) ATRIBVALOR_NUM,COALESCE(IT_ATRIB1.VALOR_FEC ,IAE.VALOR_FEC,OA.VALOR_FEC,PGA.VALOR_FEC,IAT.VALOR_FEC) ATRIBVALOR_FEC,COALESCE(IT_ATRIB1.VALOR_BOOL ,IAE.VALOR_BOOL,OA.VALOR_BOOL,PGA.VALOR_BOOL,IAT.VALOR_BOOL) ATRIBVALOR_BOOL,DA.TIPO ATRIBTIPO,COALESCE(PA.OBLIG,IAT.OBLIGATORIO) ATRIBOBLIGATORIO,'" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   IF @USAR_ART4_UON  =1 " & vbCrLf
sConsulta = sConsulta & "      SET @SQL=@SQL + 'CASE WHEN IT_EMP.UON1 IS not NULL THEN IT_EMP.UON1 ELSE ART4_UON.UON1 END UON1," & vbCrLf
sConsulta = sConsulta & "      CASE WHEN IT_EMP.UON2 IS not NULL THEN IT_EMP.UON2 ELSE ART4_UON.UON2 END UON2," & vbCrLf
sConsulta = sConsulta & "      CASE WHEN IT_EMP.UON1 IS not NULL THEN IT_EMP.UON3 ELSE ART4_UON.UON3 END UON3 '" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "      SET @SQL=@SQL + 'IT_EMP.UON1 UON1,IT_EMP.UON2 UON2,IT_EMP.UON3 UON3 '" & vbCrLf
sConsulta = sConsulta & "      " & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + ',CASE WHEN DISTR.DISTRIBUIDOR IS NULL THEN 0 ELSE 1 END DISTRIBUIDOR,'" & vbCrLf
sConsulta = sConsulta & "   IF @ESCALADOS>0 " & vbCrLf
sConsulta = sConsulta & "      SET @SQL=@SQL + '1 ESCALADOS,PG.ESCALAUNI '" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "      SET @SQL=@SQL + '0 ESCALADOS '" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   --------FROM--------------------------------------------" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + 'FROM PROCE P WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PROCE_GRUPO PG WITH (NOLOCK) ON PG.ANYO=P.ANYO AND PG.GMN1=P.GMN1 AND PG.PROCE=P.COD AND PG.COD=@CODGRUPO " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN ITEM I WITH (NOLOCK) ON I.ANYO=P.ANYO AND I.GMN1_PROCE=P.GMN1 AND I.PROCE=P.COD AND I.GRUPO=PG.ID '" & vbCrLf
sConsulta = sConsulta & "   IF @EST=11 --Si el proceso esta parcialmente cerrado" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL + ' AND I.EST=1 '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @ESCALADOS>0 --Recoge los datos de adjudicaciones de las tablas de escalados" & vbCrLf
sConsulta = sConsulta & "      SET @SQL =@SQL + 'INNER JOIN ITEM_ADJESC IA WITH (NOLOCK) ON IA.ANYO=I.ANYO AND IA.GMN1=I.GMN1_PROCE AND IA.PROCE=I.PROCE AND IA.ITEM=I.ID AND IA.PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PROCE_GR_ESC PGE WITH (NOLOCK) ON PGE.ANYO=I.ANYO AND PGE.GMN1=I.GMN1_PROCE AND PGE.PROCE=I.PROCE AND PGE.GRUPO =PG.ID --AND PGE.ID=IA.ESC '" & vbCrLf
sConsulta = sConsulta & "   ELSE -- Recoge la adjudicaci�n" & vbCrLf
sConsulta = sConsulta & "      SET @SQL =@SQL + 'INNER JOIN ITEM_ADJ IA WITH (NOLOCK) ON I.ANYO=IA.ANYO AND I.GMN1_PROCE=IA.GMN1 AND I.PROCE=IA.PROCE AND I.ID=IA.ITEM AND IA.PROVE=@PROVE '" & vbCrLf
sConsulta = sConsulta & "    " & vbCrLf
sConsulta = sConsulta & "    ---Obtiene las empresas a las que se ha distribuido la compra" & vbCrLf
sConsulta = sConsulta & "   SET @SQL =@SQL + 'INNER JOIN " & vbCrLf
sConsulta = sConsulta & "   (select distinct iu.item,iu.uon1,null uon2,null uon3,u.empresa " & vbCrLf
sConsulta = sConsulta & "   from item_uon1 iu WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   inner join uon1 u WITH(NOLOCK) on iu.uon1 = u.cod where IU.ANYO=@ANYO AND IU.GMN1=@GMN1 AND IU.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   Union " & vbCrLf
sConsulta = sConsulta & "   select distinct iu.item,iu.uon1,IU.uon2 ,null uon3,ISNULL(u1.empresa,u2.empresa) as empresa " & vbCrLf
sConsulta = sConsulta & "   from item_uon2 iu WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   inner join uon2 u2 WITH(NOLOCK) on iu.uon1=u2.uon1 and iu.uon2=u2.cod " & vbCrLf
sConsulta = sConsulta & "   inner join uon1 u1 WITH(NOLOCK) on u1.cod=u2.uon1 where IU.ANYO=@ANYO AND IU.GMN1=@GMN1 AND IU.PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "   Union select distinct iu.item,iu.uon1, IU.uon2 ,IU.uon3,ISNULL(u3.empresa,ISNULL(u2.empresa,u1.empresa)) as empresa " & vbCrLf
sConsulta = sConsulta & "   from item_uon3 iu WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   inner join uon3 u3 WITH(NOLOCK) on iu.uon1=u3.uon1 and iu.uon2=u3.uon2 and iu.uon3=u3.cod " & vbCrLf
sConsulta = sConsulta & "   inner join uon2 u2 WITH(NOLOCK) on u3.uon1=u2.uon1 and u3.uon2=u2.cod " & vbCrLf
sConsulta = sConsulta & "   inner join uon1 u1 WITH(NOLOCK) on u1.cod=u3.uon1 where IU.ANYO=@ANYO AND IU.GMN1=@GMN1 AND IU.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "   ) IT_EMP ON IT_EMP.ITEM=IA.ITEM AND IT_EMP.EMPRESA=@IDEMP '" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   --Si los art�culos est�n vinculados a las unidades organizativas, solamente aparecen las empresas a las que pertenece el item (unidades organizativas comunes entre el art�culo y la distribuci�n del item)" & vbCrLf
sConsulta = sConsulta & "   IF @USAR_ART4_UON =1" & vbCrLf
sConsulta = sConsulta & "   begin" & vbCrLf
sConsulta = sConsulta & "      SET @SQL=@SQL + 'INNER JOIN ART4_UON WITH(NOLOCK) ON I.ART=ART4_UON.ART4 AND IT_EMP.UON1=ART4_UON.UON1 AND ISNULL(IT_EMP.UON2,ISNULL(ART4_UON.UON2,0))=ISNULL(ART4_UON.UON2,0) " & vbCrLf
sConsulta = sConsulta & "         AND ISNULL(IT_EMP.UON3,ISNULL(ART4_UON.UON3,0))=ISNULL(ART4_UON.UON3,0) AND ART4_UON.BAJALOG=0 '" & vbCrLf
sConsulta = sConsulta & "      IF  @TRASPASO_PLANIF=1" & vbCrLf
sConsulta = sConsulta & "           SET @SQL=@SQL + 'AND ART4_UON.CAMPO1 = ''1'' '" & vbCrLf
sConsulta = sConsulta & "      " & vbCrLf
sConsulta = sConsulta & "      IF @USAR_ORGCOMPRAS=1" & vbCrLf
sConsulta = sConsulta & "      begin " & vbCrLf
sConsulta = sConsulta & "          SET @SQL =@SQL + 'LEFT JOIN(" & vbCrLf
sConsulta = sConsulta & "          select distinct au.art4,au.uon1,aU.uon2,aU.uon3,ISNULL(u3.empresa,ISNULL(u2.empresa,u1.empresa)) as empresa,ISNULL(u3.CENTROS,ISNULL(u2.CENTROS,u1.CENTROS)) CENTRO" & vbCrLf
sConsulta = sConsulta & "          from art4_uon au WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "          inner join uon3 u3 WITH(NOLOCK) on au.uon1=u3.uon1 and au.uon2=u3.uon2 and au.uon3=u3.cod " & vbCrLf
sConsulta = sConsulta & "          inner join uon2 u2 WITH(NOLOCK) on u3.uon1=u2.uon1 and u3.uon2=u2.cod " & vbCrLf
sConsulta = sConsulta & "          inner join uon1 u1 WITH(NOLOCK) on u1.cod=u3.uon1" & vbCrLf
sConsulta = sConsulta & "          where au.bajalog=0 '" & vbCrLf
sConsulta = sConsulta & "          IF  @TRASPASO_PLANIF=1" & vbCrLf
sConsulta = sConsulta & "              SET @SQL =@SQL + ' and au.campo1=''1'''" & vbCrLf
sConsulta = sConsulta & "          SET @SQL =@SQL + ') IT_CEN ON I.ART=IT_CEN.ART4 AND IT_CEN.EMPRESA=@IDEMP AND IT_CEN.UON1=IT_EMP.UON1 AND IT_CEN.UON2=ISNULL(IT_EMP.uon2,IT_CEN.UON2) AND ISNULL(IT_EMP.uon3,IT_CEN.UON3) =IT_CEN.UON3 '" & vbCrLf
sConsulta = sConsulta & "      end" & vbCrLf
sConsulta = sConsulta & "   end" & vbCrLf
sConsulta = sConsulta & "  " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   IF @ESCALADOS>0 --Recoge las ofertas de la tabla de escalados" & vbCrLf
sConsulta = sConsulta & "      SET @SQL =@SQL + 'INNER JOIN ITEM_OFEESC IOF WITH (NOLOCK) ON IA.ANYO =IOF.ANYO AND IA.GMN1 =IOF.GMN1 AND IA.PROCE =IOF.PROCE AND IA.ITEM =IOF.ITEM AND IA.PROVE=IOF.PROVE AND IA.OFE =IOF.OFE AND IOF.ESC=PGE.ID '" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "      SET @SQL=@SQL + 'INNER JOIN ITEM_OFE IOF WITH (NOLOCK) ON IA.ANYO =IOF.ANYO AND IA.GMN1 =IOF.GMN1 AND IA.PROCE =IOF.PROCE AND IA.ITEM =IOF.ITEM AND IA.PROVE=IOF.PROVE AND IA.OFE =IOF.NUM '" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   ---Datos de integraci�n:" & vbCrLf
sConsulta = sConsulta & "   SET @SQL= @SQL + ' LEFT JOIN (SELECT MAX(ID) ID,LIA.PROVEPADRE,LIA.PROVE,LIA.ANYO,LIA.GMN1_PROCE,LIA.PROCE," & vbCrLf
sConsulta = sConsulta & "       LIA.ID_ITEM FROM LOG_ITEM_ADJ LIA WITH (NOLOCK) WHERE LIA.ORIGEN=0" & vbCrLf
sConsulta = sConsulta & "       GROUP BY LIA.PROVE,LIA.PROVEPADRE,LIA.ANYO,LIA.GMN1_PROCE,LIA.PROCE," & vbCrLf
sConsulta = sConsulta & "       LIA.ID_ITEM) LIA2" & vbCrLf
sConsulta = sConsulta & "       ON I.ANYO=LIA2.ANYO AND I.GMN1_PROCE=LIA2.GMN1_PROCE AND I.PROCE =LIA2.PROCE AND LIA2.PROVEPADRE=@PROVE AND LIA2.PROVE=@PROVE  AND I.ID=LIA2.ID_ITEM " & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN LOG_ITEM_ADJ LIA WITH (NOLOCK) ON LIA.ID=LIA2.ID" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN (SELECT MAX(ID) ID,LIA.PROVEPADRE,LIA.ANYO,LIA.GMN1_PROCE,LIA.PROCE," & vbCrLf
sConsulta = sConsulta & "       LIA.ID_ITEM   " & vbCrLf
sConsulta = sConsulta & "       FROM LOG_ITEM_ADJ LIA WITH (NOLOCK) WHERE LIA.ORIGEN=0 " & vbCrLf
sConsulta = sConsulta & "       GROUP BY LIA.PROVEPADRE,LIA.ANYO,LIA.GMN1_PROCE,LIA.PROCE," & vbCrLf
sConsulta = sConsulta & "       LIA.ID_ITEM) LIA1 " & vbCrLf
sConsulta = sConsulta & "   ON I.ANYO=LIA1.ANYO AND I.GMN1_PROCE=LIA1.GMN1_PROCE AND I.PROCE =LIA1.PROCE AND LIA1.PROVEPADRE=@PROVE  AND I.ID=LIA1.ID_ITEM " & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN LOG_ITEM_ADJ LIA3 WITH(NOLOCK) ON LIA3.ID=LIA1.ID " & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN LOG_GRAL LG WITH (NOLOCK) ON LG.ID_TABLA=LIA1.ID AND LG.TABLA=9" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN PROVE_ERP PE WITH (NOLOCK) ON LIA.PROVE_ERP=PE.COD_ERP AND PE.EMPRESA=@IDEMP " & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN (" & vbCrLf
sConsulta = sConsulta & "   SELECT DISTINCT PROVEP DISTRIBUIDOR FROM PROVE_REL PR WITH (NOLOCK) ' " & vbCrLf
sConsulta = sConsulta & "   SET @SQL =@SQL + ' INNER JOIN GS_PROVETREL GP WITH (NOLOCK) ON PR.PROVETREL=GP.ID AND PR.PROVEP=@PROVE AND GP.PEDIDO=1)" & vbCrLf
sConsulta = sConsulta & "    DISTR ON DISTR.DISTRIBUIDOR=@PROVE" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN INT_ATRIB IAT WITH(NOLOCK) ON IAT.ADJUDICACION=1 AND IAT.AMBITO=3 AND IAT.ERP=@ERP" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN DEF_ATRIB DA WITH (NOLOCK) ON DA.ID=IAT.ATRIB  AND IAT.ADJUDICACION=1 AND IAT.AMBITO=3 AND" & vbCrLf
sConsulta = sConsulta & "IAT.ERP=@ERP" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN ITEM_ATRIBESP IAE WITH (NOLOCK) ON IAE.ANYO=@ANYO AND IAE.GMN1=@GMN1 AND IAE.PROCE=@PROCE AND IAE.ATRIB=IAT.ATRIB AND IAE.ITEM=I.ID" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN PROCE_GRUPO_ATRIBESP PGA WITH (NOLOCK) ON PGA.ANYO=@ANYO AND PGA.GMN1=@GMN1 AND PGA.PROCE=@PROCE AND PGA.ATRIB=IAT.ATRIB AND PGA.GRUPO=@IDGRUPO" & vbCrLf
sConsulta = sConsulta & " LEFT JOIN (SELECT TOP 1 OFE,ANYO,GMN1,PROCE,PROVE FROM ITEM_ADJ WITH (NOLOCK) WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE) IAD ON IAD.ANYO=@ANYO  AND IAD.GMN1=@GMN1 AND IAD.PROCE=@PROCE AND IAD.PROVE=@PROVE   " & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN PROCE_ATRIB PA WITH (NOLOCK) ON PA.ANYO=@ANYO AND PA.GMN1=@GMN1 AND PA.PROCE=@PROCE AND PA.AMBITO=3 AND DA.ID=PA.ATRIB " & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN OFE_ITEM_ATRIB OA WITH (NOLOCK) ON OA.ANYO=@ANYO AND OA.GMN1=@GMN1 AND OA.PROCE=@PROCE AND OA.ATRIB_ID =PA.ID AND OA.PROVE=@PROVE AND OA.OFE=IAD.OFE AND OA.ITEM=I.ID" & vbCrLf
sConsulta = sConsulta & "    LEFT JOIN " & vbCrLf
sConsulta = sConsulta & "   (SELECT DA.ID INT_ATRIB_ID,LIAA.ID_LOG_ITEM_ADJ , DA.ID, DA.COD,DA.DEN," & vbCrLf
sConsulta = sConsulta & "   DA.TIPO,IA.INTRO,IA.OBLIGATORIO,LIAA.VALOR_TEXT ,LIAA.VALOR_NUM,LIAA.VALOR_FEC,LIAA.VALOR_BOOL" & vbCrLf
sConsulta = sConsulta & "   FROM INT_ATRIB IA WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN DEF_ATRIB DA WITH (NOLOCK) ON DA.ID=IA.ATRIB AND IA.ADJUDICACION=1 AND IA.AMBITO=3 AND IA.ERP=@ERP" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN LOG_ITEM_ADJ_ATRIB LIAA WITH (NOLOCK) ON LIAA.ATRIB=IA.ATRIB" & vbCrLf
sConsulta = sConsulta & "    ) IT_ATRIB1 ON IT_ATRIB1.INT_ATRIB_ID=IAT.ID AND LIA.ID=IT_ATRIB1.ID_LOG_ITEM_ADJ" & vbCrLf
sConsulta = sConsulta & "    WHERE P.ANYO=@ANYO AND P.GMN1=@GMN1 AND P.COD=@PROCE ' " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @PARAM =N'@ANYO INT, @GMN1 VARCHAR(50), @PROCE VARCHAR(50), @CODGRUPO VARCHAR(20),@PROVE VARCHAR(50),@IDEMP INT,@ERP INT,@IDGRUPO INT'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL ,@PARAM,@ANYO=@ANYO,@GMN1=@GMN1,@PROCE=@PROCE,@CODGRUPO=@CODGRUPO,@PROVE=@PROVE,@IDEMP=@IDEMP,@ERP=@ERP ,@IDGRUPO=@IDGRUPO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSWS_GETADJUNTOS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSWS_GETADJUNTOS]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSWS_GETADJUNTOS] @TIPO INT, @IDS VARCHAR(2000) =NULL, @IDSNEW VARCHAR(2000) = NULL, @IDI VARCHAR(50) = NULL, @FAVORITO TINYINT = 0 AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N''" & vbCrLf
sConsulta = sConsulta & "IF @TIPO=1" & vbCrLf
sConsulta = sConsulta & " IF @IDS IS NULL AND @IDSNEW IS NULL" & vbCrLf
sConsulta = sConsulta & "   SELECT  1 TIPO, A.ID, A.FECALTA, A.PER, P.NOM + ' ' + P.APE NOMBRE, A.NOM,ROUND(A.DATASIZE/1024,0) DATASIZE , A.COMENT, A.IDIOMA , NULL ADJUN, NULL CAMPO, A.RUTA, 0 PORTAL,  A.COMENT as  COMENTSalto" & vbCrLf
sConsulta = sConsulta & "     FROM CAMPO_ADJUN A WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "          LEFT JOIN PER P  WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                 ON A.PER = P.COD " & vbCrLf
sConsulta = sConsulta & "   WHERE id=-1000" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "     ORDER BY A.ID" & vbCrLf
sConsulta = sConsulta & " ELSE" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "     IF @IDS IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = N'" & vbCrLf
sConsulta = sConsulta & "                 SELECT  1 TIPO, A.ID, A.FECALTA, A.PER, P.NOM + '' '' + P.APE NOMBRE, A.NOM, ROUND(A.DATASIZE/1024,0) DATASIZE , A.COMENT, A.IDIOMA , NULL ADJUN, NULL CAMPO, A.RUTA, 0 PORTAL,  A.COMENT as  COMENTSalto" & vbCrLf
sConsulta = sConsulta & "                   FROM CAMPO_ADJUN A  WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                        LEFT JOIN PER P  WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                               ON A.PER = P.COD " & vbCrLf
sConsulta = sConsulta & "                  WHERE A.ID IN (' + @IDS + ') AND  IDIOMA = ISNULL(@IDI,IDIOMA) '" & vbCrLf
sConsulta = sConsulta & "    IF @IDSNEW IS NOT NULL and @IDS IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "      SET @SQL = @SQL + N' UNION ALL '" & vbCrLf
sConsulta = sConsulta & "    IF @IDSNEW IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "      SET @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                 SELECT  2 TIPO, A.ID, A.FECALTA, A.PER, P.NOM + '' '' + P.APE NOMBRE, A.NOM,  ROUND(DATALENGTH(DATA)/1024 ,0) DATASIZE , A.COMENT, @IDI IDIOMA , null ADJUN, null CAMPO, null RUTA, 0 PORTAL,  A.COMENT as  COMENTSalto" & vbCrLf
sConsulta = sConsulta & "                   FROM PM_COPIA_ADJUN A  WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                           LEFT JOIN PER P  WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                                  ON A.PER = P.COD " & vbCrLf
sConsulta = sConsulta & "                  WHERE  A.ID IN (' + @IDSNEW + ')" & vbCrLf
sConsulta = sConsulta & "                      ORDER BY A.ID '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "     EXEC SP_EXECUTESQL @SQL, N'@IDI VARCHAR(50)', @IDI=@IDI" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  IF @TIPO=3" & vbCrLf
sConsulta = sConsulta & " IF @IDS IS NULL AND @IDSNEW IS NULL" & vbCrLf
sConsulta = sConsulta & "   SELECT  3 TIPO, A.ID, A.FECALTA, A.PER, P.NOM + ' ' + P.APE NOMBRE, A.NOM, ROUND(A.DATASIZE/1024,0) DATASIZE , A.COMENT, A.IDIOMA , null ADJUN, null CAMPO, A.RUTA, 0 PORTAL,  A.COMENT as  COMENTSalto" & vbCrLf
sConsulta = sConsulta & "     FROM LINEA_DESGLOSE_ADJUN A  WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "          LEFT JOIN PER P  WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                 ON A.PER = P.COD " & vbCrLf
sConsulta = sConsulta & "   WHERE id=-1000" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "     ORDER BY A.ID" & vbCrLf
sConsulta = sConsulta & " ELSE" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "     IF @IDS IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "     BEGIN" & vbCrLf
sConsulta = sConsulta & "       IF @FAVORITO=1 " & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = N'" & vbCrLf
sConsulta = sConsulta & "        SELECT  2 TIPO, A.ID, A.FECALTA, A.PER, P.NOM + '' '' + P.APE NOMBRE, A.NOM,  " & vbCrLf
sConsulta = sConsulta & "       ROUND(DATALENGTH(DATA)/1024 ,0) DATASIZE , A.COMENT, NULL IDIOMA, null ADJUN, null CAMPO, null RUTA, 0 PORTAL, A.COMENT as  COMENTSalto" & vbCrLf
sConsulta = sConsulta & "       FROM PM_COPIA_ADJUN A  WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "          LEFT JOIN PER P  WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "          ON A.PER = P.COD " & vbCrLf
sConsulta = sConsulta & "          WHERE  A.ID IN (' + @IDS + ') AND A.ID NOT IN (SELECT ID FROM LINEA_DESGLOSE_ADJUN WITH(NOLOCK) WHERE ID IN (' + @IDS + ') AND FAVORITO=1)" & vbCrLf
sConsulta = sConsulta & "       UNION ALL" & vbCrLf
sConsulta = sConsulta & "        SELECT  3 TIPO, A.ID, A.FECALTA, A.PER, P.NOM + '' '' + P.APE NOMBRE, A.NOM, ROUND(A.DATASIZE/1024,0) DATASIZE , A.COMENT, A.IDIOMA , null ADJUN, null CAMPO, A.RUTA, 0 PORTAL,  A.COMENT as  COMENTSalto" & vbCrLf
sConsulta = sConsulta & "        FROM LINEA_DESGLOSE_ADJUN A  WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "        LEFT JOIN PER P  WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "        ON A.PER = P.COD " & vbCrLf
sConsulta = sConsulta & "        WHERE A.ID IN (' + @IDS + ') AND FAVORITO=1'" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "       ELSE" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "          SET @SQL = N'" & vbCrLf
sConsulta = sConsulta & "                 SELECT  3 TIPO, A.ID, A.FECALTA, A.PER, P.NOM + '' '' + P.APE NOMBRE, A.NOM, ROUND(A.DATASIZE/1024,0) DATASIZE , A.COMENT, A.IDIOMA , null ADJUN, null CAMPO, A.RUTA, 0 PORTAL,  A.COMENT as  COMENTSalto" & vbCrLf
sConsulta = sConsulta & "                   FROM LINEA_DESGLOSE_ADJUN A  WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                        LEFT JOIN PER P  WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                               ON A.PER = P.COD " & vbCrLf
sConsulta = sConsulta & "                  WHERE   A.ID IN (' + @IDS + ') AND IDIOMA = ISNULL(@IDI,IDIOMA)    '" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "    IF @IDSNEW IS NOT NULL and @IDS IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "      SET @SQL = @SQL + N' UNION ALL '" & vbCrLf
sConsulta = sConsulta & "    IF @IDSNEW IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "      SET @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                 SELECT  4 TIPO, A.ID, A.FECALTA, A.PER, P.NOM + '' '' + P.APE NOMBRE, A.NOM,  ROUND(DATALENGTH(DATA)/1024 ,0) DATASIZE , A.COMENT, @IDI IDIOMA, null ADJUN, null CAMPO, null RUTA, 0 PORTAL,  A.COMENT as  COMENTSalto" & vbCrLf
sConsulta = sConsulta & "                   FROM PM_COPIA_ADJUN A  WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                           LEFT JOIN PER P  WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                                  ON A.PER = P.COD " & vbCrLf
sConsulta = sConsulta & "                  WHERE  A.ID IN (' + @IDSNEW + ')" & vbCrLf
sConsulta = sConsulta & "                      ORDER BY A.ID '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "     EXEC SP_EXECUTESQL @SQL, N'@IDI VARCHAR(50)', @IDI=@IDI" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_LOADFIELDSCALC]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_LOADFIELDSCALC]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_LOADFIELDSCALC @FORMULARIO INT,  @IDI VARCHAR(50)='SPA', @SOLICITUD INT ,@PER VARCHAR(50)  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT FC.ID,FC.GRUPO,FC.DEN_ENG,FC.DEN_SPA,FC.AYUDA_ENG,FC.AYUDA_SPA,FC.TIPO,FC.SUBTIPO,FC.INTRO,FC.VALOR_NUM,FC.VALOR_TEXT,FC.VALOR_FEC,FC.VALOR_BOOL,FC.MINNUM,FC.MAXNUM,FC.MINFEC,FC.MAXFEC,FC.ORDEN,FC.ID_ATRIB_GS,FC.TIPO_CAMPO_GS,FC.ES_SUBCAMPO,FC.GMN1,FC.GMN2,FC.GMN3,FC.GMN4,FC.LINEAS_PRECONF,FC.FECACT,FC.ID_CALCULO,FC.FORMULA,FC.ORDEN_CALCULO,FC.IMPORTE_WORKFL,FC.ORIGEN_CALC_DESGLOSE,FC.DEN_GER,FC.AYUDA_GER,FC.POPUP,FC.ANYADIR_ART,FC.CAMPO_ORIGEN,FC.TITULO,FC.FECHA_VALOR_DEFECTO,FC.IDREFSOLICITUD,FC.AVISOBLOQUEOIMPACUM,FC.CARGAR_ULT_ADJ,FC.MAX_LENGTH,FC.A_PROCESO,FC.A_PEDIDO,FC.TABLA_EXTERNA,FC.PRES5,FC.CENTRO_COSTE,FC.VER_UON,FC.SAVE_VALUES,FC.CAMPO_PADRE,FC.CAMPO_HIJO,FC.VINCULADO, C.GRUPO GRUPO_ORIGEN_CALC_DESGLOSE, ISNULL(CC.ESCRITURA,1) ESCRITURA,  CASE WHEN CCV.VISIBLE=0 THEN 0 ELSE ISNULL(CC.VISIBLE,1)  END AS VISIBLE" & vbCrLf
sConsulta = sConsulta & "  FROM FORM_CAMPO FC WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "      INNER JOIN FORM_GRUPO FG WITH (NOLOCK)   ON FC.GRUPO=FG.ID  AND FG.FORMULARIO=@FORMULARIO" & vbCrLf
sConsulta = sConsulta & "      INNER JOIN SOLICITUD S WITH (NOLOCK)  ON FG.FORMULARIO=S.FORMULARIO AND S.ID=@SOLICITUD" & vbCrLf
sConsulta = sConsulta & "      INNER JOIN PM_BLOQUE WITH (NOLOCK)  ON S.WORKFLOW=PM_BLOQUE.WORKFLOW AND PM_BLOQUE.TIPO=1 " & vbCrLf
sConsulta = sConsulta & "      INNER JOIN PM_ROL WITH (NOLOCK)  ON S.WORKFLOW=PM_ROL.WORKFLOW AND PM_ROL.TIPO=4" & vbCrLf
sConsulta = sConsulta & "      INNER JOIN VIEW_ROL_PETICIONARIO VW WITH (NOLOCK)  ON VW.ID=PM_ROL.ID AND VW.SOLICITUD=S.ID AND VW.COD=@PER" & vbCrLf
sConsulta = sConsulta & "      INNER JOIN PM_ROL_BLOQUE WITH (NOLOCK)  ON PM_ROL.ID=PM_ROL_BLOQUE.ROL AND PM_BLOQUE.ID=PM_ROL_BLOQUE.BLOQUE" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN FORM_CAMPO C WITH (NOLOCK)  ON FC.ORIGEN_CALC_DESGLOSE=C.ID      " & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN PM_CONF_CUMP_BLOQUE CC WITH (NOLOCK)  ON FC.ID = CC.CAMPO AND CC.BLOQUE=PM_BLOQUE.ID AND CC.ROL =  PM_ROL.ID" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN PM_CONF_CUMP_BLOQUE CCV WITH (NOLOCK)  ON  CCV.CAMPO=C.ORIGEN_CALC_DESGLOSE AND CCV.BLOQUE=PM_BLOQUE.ID  AND CCV.ROL =PM_ROL.ID " & vbCrLf
sConsulta = sConsulta & "WHERE FC.ES_SUBCAMPO = 0   AND FC.ID_CALCULO IS NOT NULL  AND (FC.SUBTIPO =2 OR FC.TIPO =3) " & vbCrLf
sConsulta = sConsulta & "ORDER BY FC.IMPORTE_WORKFL,FC.ORDEN_CALCULO" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_GET_PRECONDICIONES]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_GET_PRECONDICIONES]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_GET_PRECONDICIONES] @BSOLICITUD BIT, @IDACC INTEGER, @IDI VARCHAR(3) = 'SPA', @INSTANCIA INT = 0, @VERSION INT = 0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL_VERSION AS NVARCHAR(15)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @BSOLICITUD = 1 " & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    SET @SQL = 'SELECT ID, ORDEN, COD, ACCION, TIPO, FORMULA, DEN " & vbCrLf
sConsulta = sConsulta & "    FROM PM_ACCION_PRECOND P WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "    LEFT JOIN PM_ACCION_PRECOND_DEN PD WITH (NOLOCK)  ON PD.ACCION_PRECOND = P.ID AND IDI = @IDI " & vbCrLf
sConsulta = sConsulta & "    WHERE ACCION = @IDACC'" & vbCrLf
sConsulta = sConsulta & "    EXEC SP_EXECUTESQL @SQL, N'@IDACC INT, @IDI VARCHAR(3)', @IDACC=@IDACC, @IDI=@IDI" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    SET @SQL = 'SELECT ID,ACCION_PRECOND,COD,TIPO_CAMPO,CAMPO,OPERADOR,TIPO_VALOR,CAMPO_VALOR,VALOR_TEXT,VALOR_NUM,VALOR_BOOL,VALOR_FEC,FECACT " & vbCrLf
sConsulta = sConsulta & "   FROM PM_ACCION_CONDICIONES WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   WHERE ACCION_PRECOND IN " & vbCrLf
sConsulta = sConsulta & "       (" & vbCrLf
sConsulta = sConsulta & "       SELECT ID " & vbCrLf
sConsulta = sConsulta & "       FROM PM_ACCION_PRECOND WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "       WHERE ACCION = @IDACC" & vbCrLf
sConsulta = sConsulta & "       )'" & vbCrLf
sConsulta = sConsulta & "    EXEC SP_EXECUTESQL @SQL, N'@IDACC INT', @IDACC=@IDACC" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    SET @SQL = 'SELECT ID, ORDEN, COD, ACCION, TIPO, FORMULA, DEN " & vbCrLf
sConsulta = sConsulta & "    FROM PM_COPIA_ACCION_PRECOND P WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "    LEFT JOIN PM_COPIA_ACCION_PRECOND_DEN PD WITH (NOLOCK)  ON PD.ACCION_PRECOND = P.ID AND IDI = @IDI " & vbCrLf
sConsulta = sConsulta & "    WHERE ACCION = @IDACC'" & vbCrLf
sConsulta = sConsulta & "    EXEC SP_EXECUTESQL @SQL, N'@IDACC INT, @IDI VARCHAR(3)', @IDACC=@IDACC, @IDI=@IDI" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @VERSION = 0" & vbCrLf
sConsulta = sConsulta & "       SET @SQL_VERSION = 'I.ULT_VERSION'" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "       SET @SQL_VERSION = '@VERSION'" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "    SET @SQL = 'SELECT CAC.ID, CAC.ACCION_PRECOND, CAC.COD, CAC.TIPO_CAMPO, CC1.ID AS CAMPO, CAC.OPERADOR, CAC.TIPO_VALOR, CC2.ID AS CAMPO_VALOR, CAC.VALOR_TEXT, CAC.VALOR_NUM, CAC.VALOR_FEC, CAC.VALOR_BOOL, CAC.FECACT " & vbCrLf
sConsulta = sConsulta & "       FROM PM_COPIA_ACCION_CONDICIONES CAC WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN INSTANCIA I WITH (NOLOCK) ON I.ID = @INSTANCIA" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN COPIA_CAMPO CC1 WITH (NOLOCK) ON CAC.CAMPO = CC1.COPIA_CAMPO_DEF AND CC1.NUM_VERSION = ' + @SQL_VERSION + ' AND CC1.INSTANCIA = @INSTANCIA" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN COPIA_CAMPO CC2 WITH (NOLOCK) ON CAC.CAMPO_VALOR = CC2.COPIA_CAMPO_DEF AND CC2.NUM_VERSION = ' + @SQL_VERSION + ' AND CC2.INSTANCIA = @INSTANCIA" & vbCrLf
sConsulta = sConsulta & "       WHERE ACCION_PRECOND IN (SELECT ID FROM PM_COPIA_ACCION_PRECOND WITH (NOLOCK) WHERE ACCION = @IDACC)'" & vbCrLf
sConsulta = sConsulta & "    EXEC SP_EXECUTESQL @SQL, N'@IDACC INT, @INSTANCIA INT, @VERSION INT', @IDACC=@IDACC, @INSTANCIA=@INSTANCIA, @VERSION=@VERSION" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_LOADFIELDSDESGLOSECALC]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_LOADFIELDSDESGLOSECALC]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSPM_LOADFIELDSDESGLOSECALC @CAMPO INT, @SOLICITUD INT,@PER VARCHAR(50) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @TIPO INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @TIPO=TS.TIPO FROM TIPO_SOLICITUDES TS WITH (NOLOCK) INNER JOIN SOLICITUD S WITH (NOLOCK) ON TS.ID=S.TIPO AND S.ID=@SOLICITUD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF (@TIPO=3 OR @TIPO=2)" & vbCrLf
sConsulta = sConsulta & "       IF @PER IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "       SELECT DISTINCT FC.ID,FC.GRUPO,FC.DEN_ENG,FC.DEN_SPA,FC.AYUDA_ENG,FC.AYUDA_SPA,FC.TIPO,FC.SUBTIPO,FC.INTRO,FC.VALOR_NUM,FC.VALOR_TEXT,FC.VALOR_FEC,FC.VALOR_BOOL,FC.MINNUM,FC.MAXNUM,FC.MINFEC,FC.MAXFEC,FC.ORDEN,FC.ID_ATRIB_GS,FC.TIPO_CAMPO_GS,FC.ES_SUBCAMPO,FC.GMN1,FC.GMN2,FC.GMN3,FC.GMN4,FC.LINEAS_PRECONF,FC.FECACT,FC.ID_CALCULO,FC.FORMULA,FC.ORDEN_CALCULO,FC.IMPORTE_WORKFL,FC.ORIGEN_CALC_DESGLOSE,FC.DEN_GER,FC.AYUDA_GER,FC.POPUP,FC.ANYADIR_ART,FC.CAMPO_ORIGEN,FC.TITULO,FC.FECHA_VALOR_DEFECTO,FC.IDREFSOLICITUD,FC.AVISOBLOQUEOIMPACUM,FC.CARGAR_ULT_ADJ,FC.MAX_LENGTH,FC.A_PROCESO,FC.A_PEDIDO,FC.TABLA_EXTERNA,FC.PRES5,FC.CENTRO_COSTE,FC.VER_UON,FC.SAVE_VALUES,FC.CAMPO_PADRE,FC.CAMPO_HIJO,FC.VINCULADO, ISNULL(CCV.VISIBLE,1) VISIBLE, ISNULL(CCV.ESCRITURA,1) ESCRITURA" & vbCrLf
sConsulta = sConsulta & "       FROM FORM_CAMPO FC WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN FORM_GRUPO G WITH (NOLOCK) ON FC.GRUPO = G.ID" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN SOLICITUD S WITH (NOLOCK) ON S.ID=@SOLICITUD AND G.FORMULARIO=S.FORMULARIO" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN DESGLOSE D WITH (NOLOCK)  ON D.CAMPO_PADRE=@CAMPO AND FC.ID=D.CAMPO_HIJO" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN CONF_CUMPLIMENTACION CCV WITH (NOLOCK) ON FC.ID = CCV.CAMPO AND SOLIC_PROVE=1" & vbCrLf
sConsulta = sConsulta & "       WHERE FC.ID_CALCULO IS NOT NULL    AND (FC.SUBTIPO =2 OR FC.TIPO =3)" & vbCrLf
sConsulta = sConsulta & "       ORDER BY FC.ORDEN_CALCULO" & vbCrLf
sConsulta = sConsulta & "       ELSE" & vbCrLf
sConsulta = sConsulta & "       SELECT DISTINCT FC.ID,FC.GRUPO,FC.DEN_ENG,FC.DEN_SPA,FC.AYUDA_ENG,FC.AYUDA_SPA,FC.TIPO,FC.SUBTIPO,FC.INTRO,FC.VALOR_NUM,FC.VALOR_TEXT,FC.VALOR_FEC,FC.VALOR_BOOL,FC.MINNUM,FC.MAXNUM,FC.MINFEC,FC.MAXFEC,FC.ORDEN,FC.ID_ATRIB_GS,FC.TIPO_CAMPO_GS,FC.ES_SUBCAMPO,FC.GMN1,FC.GMN2,FC.GMN3,FC.GMN4,FC.LINEAS_PRECONF,FC.FECACT,FC.ID_CALCULO,FC.FORMULA,FC.ORDEN_CALCULO,FC.IMPORTE_WORKFL,FC.ORIGEN_CALC_DESGLOSE,FC.DEN_GER,FC.AYUDA_GER,FC.POPUP,FC.ANYADIR_ART,FC.CAMPO_ORIGEN,FC.TITULO,FC.FECHA_VALOR_DEFECTO,FC.IDREFSOLICITUD,FC.AVISOBLOQUEOIMPACUM,FC.CARGAR_ULT_ADJ,FC.MAX_LENGTH,FC.A_PROCESO,FC.A_PEDIDO,FC.TABLA_EXTERNA,FC.PRES5,FC.CENTRO_COSTE,FC.VER_UON,FC.SAVE_VALUES,FC.CAMPO_PADRE,FC.CAMPO_HIJO,FC.VINCULADO, ISNULL(CCV.VISIBLE,1) VISIBLE, ISNULL(CCV.ESCRITURA,1) ESCRITURA" & vbCrLf
sConsulta = sConsulta & "       FROM FORM_CAMPO FC WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN FORM_GRUPO G WITH (NOLOCK) ON FC.GRUPO = G.ID" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN SOLICITUD S WITH (NOLOCK) ON S.ID=@SOLICITUD AND G.FORMULARIO=S.FORMULARIO" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN DESGLOSE D WITH (NOLOCK)  ON  D.CAMPO_PADRE=@CAMPO AND FC.ID=D.CAMPO_HIJO" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN CONF_CUMPLIMENTACION CCV WITH (NOLOCK) ON FC.ID = CCV.CAMPO AND SOLIC_PROVE=2" & vbCrLf
sConsulta = sConsulta & "       WHERE FC.ID_CALCULO IS NOT NULL    AND (FC.SUBTIPO =2 OR FC.TIPO =3)" & vbCrLf
sConsulta = sConsulta & "       ORDER BY FC.ORDEN_CALCULO" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   SELECT DISTINCT C.ID,C.GRUPO,C.DEN_ENG,C.DEN_SPA,C.AYUDA_ENG,C.AYUDA_SPA,C.TIPO,C.SUBTIPO,C.INTRO,C.VALOR_NUM,C.VALOR_TEXT,C.VALOR_FEC,C.VALOR_BOOL,C.MINNUM,C.MAXNUM,C.MINFEC,C.MAXFEC,C.ORDEN,C.ID_ATRIB_GS,C.TIPO_CAMPO_GS,C.ES_SUBCAMPO,C.GMN1,C.GMN2,C.GMN3,C.GMN4,C.LINEAS_PRECONF,C.FECACT,C.ID_CALCULO,C.FORMULA,C.ORDEN_CALCULO,C.IMPORTE_WORKFL,C.ORIGEN_CALC_DESGLOSE,C.DEN_GER,C.AYUDA_GER,C.POPUP,C.ANYADIR_ART,C.CAMPO_ORIGEN,C.TITULO,C.FECHA_VALOR_DEFECTO,C.IDREFSOLICITUD,C.AVISOBLOQUEOIMPACUM,C.CARGAR_ULT_ADJ,C.MAX_LENGTH,C.A_PROCESO,C.A_PEDIDO,C.TABLA_EXTERNA,C.PRES5,C.CENTRO_COSTE,C.VER_UON,C.SAVE_VALUES,C.CAMPO_PADRE,C.CAMPO_HIJO,C.VINCULADO, CC.VISIBLE, CC.ESCRITURA" & vbCrLf
sConsulta = sConsulta & "   FROM FORM_CAMPO C WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN FORM_GRUPO G WITH (NOLOCK)  ON C.GRUPO = G.ID" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN SOLICITUD S WITH (NOLOCK)  ON S.ID=@SOLICITUD AND G.FORMULARIO=S.FORMULARIO" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PM_BLOQUE WITH (NOLOCK)  ON S.WORKFLOW=PM_BLOQUE.WORKFLOW AND PM_BLOQUE.TIPO=1" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PM_ROL WITH (NOLOCK)  ON S.WORKFLOW=PM_ROL.WORKFLOW AND PM_ROL.TIPO=4" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN (SELECT MAX(ID) ID FROM VIEW_ROL_PETICIONARIO VRP WITH (NOLOCK) WHERE  VRP.COD=@PER AND VRP.SOLICITUD=@SOLICITUD) VW ON VW.ID=PM_ROL.ID" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PM_ROL_BLOQUE WITH (NOLOCK)  ON PM_ROL.ID=PM_ROL_BLOQUE.ROL AND PM_BLOQUE.ID=PM_ROL_BLOQUE.BLOQUE" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN DESGLOSE D WITH (NOLOCK)   ON D.CAMPO_PADRE=@CAMPO  AND  C.ID=D.CAMPO_HIJO" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN PM_CONF_CUMP_BLOQUE CC WITH (NOLOCK)  ON CC.BLOQUE=PM_BLOQUE.ID AND CC.ROL =  PM_ROL.ID AND  C.ID = CC.CAMPO " & vbCrLf
sConsulta = sConsulta & "   WHERE C.ID_CALCULO IS NOT NULL    AND (C.SUBTIPO =2 OR C.TIPO =3)" & vbCrLf
sConsulta = sConsulta & "   ORDER BY C.ORDEN_CALCULO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT ISNULL(LD.LINEA,ISNULL(LD2.LINEA,1)) LINEA, D.CAMPO_PADRE,D.CAMPO_HIJO,LD.VALOR_NUM,LD.VALOR_FEC,LD.VALOR_TEXT,LD.VALOR_BOOL" & vbCrLf
sConsulta = sConsulta & "FROM DESGLOSE D WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN FORM_CAMPO C WITH (NOLOCK) ON C.ID = D.CAMPO_PADRE" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN LINEA_DESGLOSE LD WITH (NOLOCK) ON D.CAMPO_PADRE = LD.CAMPO_PADRE AND D.CAMPO_HIJO = LD.CAMPO_HIJO" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN LINEA_DESGLOSE LD2 WITH (NOLOCK) ON D.CAMPO_PADRE = LD2.CAMPO_PADRE" & vbCrLf
sConsulta = sConsulta & "WHERE D.CAMPO_PADRE=@CAMPO" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSWS_GETPEDIDO]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSWS_GETPEDIDO]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSWS_GETPEDIDO] @ID INT,@INSTANCIA INT, @IDI AS VARCHAR(20) =NULL AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Select 0: Obtener Datos Generales del Pedido" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'SELECT DISTINCT I.ID AS ID_INST, " & vbCrLf
sConsulta = sConsulta & "   CASE WHEN C1.VALOR_TEXT IS NULL THEN S.DEN_'+ @IDI + ' ELSE  C1.VALOR_TEXT END DESCR_INST," & vbCrLf
sConsulta = sConsulta & "   O.ID, " & vbCrLf
sConsulta = sConsulta & "   P.ANYO, " & vbCrLf
sConsulta = sConsulta & "   P.NUM PNUM, " & vbCrLf
sConsulta = sConsulta & "   O.NUM ONUM, " & vbCrLf
sConsulta = sConsulta & "   O.PROVE, " & vbCrLf
sConsulta = sConsulta & "   PR.DEN PROVE_DEN," & vbCrLf
sConsulta = sConsulta & "   O.IMPORTE," & vbCrLf
sConsulta = sConsulta & "   O.CAMBIO," & vbCrLf
sConsulta = sConsulta & "   O.MON, " & vbCrLf
sConsulta = sConsulta & "   O.EST, " & vbCrLf
sConsulta = sConsulta & "   LP.FECEMISION," & vbCrLf
sConsulta = sConsulta & "   I.PETICIONARIO ID_PETICIONARIO, " & vbCrLf
sConsulta = sConsulta & "   ISNULL(P_PET.NOM,'''') + '' '' + ISNULL(P_PET.APE,'''') NOM_PETICIONARIO," & vbCrLf
sConsulta = sConsulta & "   O.RECEPTOR ID_RECEPTOR, " & vbCrLf
sConsulta = sConsulta & "   ISNULL(P_REC.NOM,'''') + '' '' + ISNULL(P_REC.APE,'''') NOM_RECEPTOR," & vbCrLf
sConsulta = sConsulta & "   O.OBS, " & vbCrLf
sConsulta = sConsulta & "   O.NUM_PED_ERP, " & vbCrLf
sConsulta = sConsulta & "   O.TIPO" & vbCrLf
sConsulta = sConsulta & "FROM PEDIDO P WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN ORDEN_ENTREGA O WITH (NOLOCK) ON O.ID = @ID AND P.ID = O.PEDIDO " & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN PROVE PR  WITH (NOLOCK) ON O.PROVE = PR.COD" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN LINEAS_PEDIDO LP WITH (NOLOCK) ON O.ID = LP.ORDEN " & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN INSTANCIA I WITH (NOLOCK) ON LP.SOLICIT = I.ID" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN SOLICITUD S WITH (NOLOCK) ON I.SOLICITUD = S.ID" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN (SELECT M.INSTANCIA, M.NUM_VERSION, C.VALOR_TEXT FROM " & vbCrLf
sConsulta = sConsulta & "       (SELECT INSTANCIA,NUM_VERSION,MAX(C.ID) CAMPO FROM COPIA_CAMPO C WITH (NOLOCK) INNER JOIN COPIA_CAMPO_DEF CD WITH (NOLOCK) ON C.COPIA_CAMPO_DEF=CD.ID  AND CD.TIPO_CAMPO_GS=1  WHERE VALOR_TEXT IS NOT NULL GROUP BY C.INSTANCIA, C.NUM_VERSION ) M " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN COPIA_CAMPO C WITH (NOLOCK) ON M.CAMPO = C.ID) C1 ON I.ID=C1.INSTANCIA AND I.ULT_VERSION=C1.NUM_VERSION" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN PER P_PET WITH (NOLOCK) ON I.PETICIONARIO = P_PET.COD" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN PER P_REC WITH (NOLOCK) ON o.RECEPTOR = P_REC.COD" & vbCrLf
sConsulta = sConsulta & "WHERE LP.SOLICIT IS NOT NULL'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@ID INT, @IDI  VARCHAR(20)', @ID=@ID,@IDI=@IDI" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Select 1: Obtener los Atributos del Pedido" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT DA.ID,DA.COD,DA.DEN,DA.TIPO,OEA.INTRO,OEA.VALOR_TEXT,OEA.VALOR_NUM,OEA.VALOR_FEC,OEA.VALOR_BOOL" & vbCrLf
sConsulta = sConsulta & "FROM ORDEN_ENTREGA_ATRIB OEA WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN DEF_ATRIB DA WITH (NOLOCK) ON OEA.ATRIB=DA.ID " & vbCrLf
sConsulta = sConsulta & "WHERE OEA.ORDEN = @ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Select 2: Obtener los Archivos Adjuntos del Pedido" & vbCrLf
sConsulta = sConsulta & "SELECT ID, ORDEN, NOM, COM, DATA, DATASIZE, FECACT FROM ORDEN_ENTREGA_ADJUN WITH (NOLOCK) WHERE ORDEN = @ID ORDER BY ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Select 3: Carga los Datos Generales de las l�neas de pedido:" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT LP.ID," & vbCrLf
sConsulta = sConsulta & "CASE WHEN LP.ART_INT IS NULL THEN CASE WHEN ITEM.ART IS NULL THEN ART4.COD ELSE ITEM.ART  END ELSE LP.ART_INT END CODART," & vbCrLf
sConsulta = sConsulta & "CASE WHEN ITEM.DESCR IS NULL THEN CASE WHEN ART4.DEN IS NULL THEN LP.DESCR_LIBRE ELSE ART4.DEN  END ELSE ITEM.DESCR END DENART," & vbCrLf
sConsulta = sConsulta & "   LP.CANT_PED AS CANTIDAD," & vbCrLf
sConsulta = sConsulta & "   LP.UP AS UNIDAD," & vbCrLf
sConsulta = sConsulta & "   LP.PREC_UP AS PREC_UNIDAD," & vbCrLf
sConsulta = sConsulta & "   LP.FECENTREGA," & vbCrLf
sConsulta = sConsulta & "   LP.FECENTREGAPROVE," & vbCrLf
sConsulta = sConsulta & "   LP.DEST AS DESTINO, " & vbCrLf
sConsulta = sConsulta & "   LP.CANT_REC AS CANTIDAD_REC," & vbCrLf
sConsulta = sConsulta & "   LP.ENTREGA_OBL," & vbCrLf
sConsulta = sConsulta & "   LP.OBS" & vbCrLf
sConsulta = sConsulta & "FROM LINEAS_PEDIDO LP WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN ITEM WITH (NOLOCK)   ON LP.ANYO = ITEM.ANYO  AND LP.GMN1=ITEM.GMN1_PROCE   AND LP.PROCE=ITEM.PROCE   AND LP.ITEM=ITEM.ID" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN ART4 WITH (NOLOCK) ON ART4.COD=LP.ART_INT" & vbCrLf
sConsulta = sConsulta & "WHERE LP.ORDEN=@ID AND LP.SOLICIT=@INSTANCIA" & vbCrLf
sConsulta = sConsulta & "ORDER BY LP.ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Select 4: Obtener los Atributos de las Lineas de Pedido" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT LPA.LINEA,DA.ID,DA.COD,DA.DEN,DA.TIPO,LPA.INTRO,LPA.VALOR_TEXT,LPA.VALOR_NUM,LPA.VALOR_FEC,LPA.VALOR_BOOL" & vbCrLf
sConsulta = sConsulta & "FROM LINEAS_PEDIDO_ATRIB LPA WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN DEF_ATRIB DA WITH (NOLOCK) ON LPA.ATRIB=DA.ID " & vbCrLf
sConsulta = sConsulta & "WHERE LINEA IN (SELECT ID FROM LINEAS_PEDIDO WITH (NOLOCK) WHERE ORDEN = @ID AND SOLICIT = @INSTANCIA)" & vbCrLf
sConsulta = sConsulta & "ORDER BY LINEA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Select 5: Obtener los Archivos Adjuntos de las Lineas de  Pedido" & vbCrLf
sConsulta = sConsulta & "SELECT ID, LINEA, NOM, COM, DATA, DATASIZE, FECACT FROM LINEAS_PEDIDO_ADJUN WITH (NOLOCK) WHERE LINEA IN (SELECT ID FROM LINEAS_PEDIDO WITH (NOLOCK) WHERE ORDEN = @ID AND SOLICIT = @INSTANCIA)" & vbCrLf
sConsulta = sConsulta & "ORDER BY LINEA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_SAVE_ADJUN]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_SAVE_ADJUN]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_SAVE_ADJUN] @ID_ADJUN INT OUTPUT,@PER NVARCHAR(50)=NULL,@PROVE NVARCHAR(50),@NOM NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & ", @ID_CONTRATO INT=NULL, @COMENT NVARCHAR(4000), @SOLICITUD INT=NULL, @INSTANCIA INT=NULL, @CAMPO INT=NULL, @LINEA INT=NULL" & vbCrLf
sConsulta = sConsulta & ", @CAMPOPADRE INT=NULL, @CAMPOHIJO INT=NULL,@DATASIZE INT=NULL,@IDIOMA NVARCHAR(10)='',@FECHA DATETIME=NULL" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @SOLICITUD>0 " & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       INSERT INTO SOLICITUD_ADJUN (DATA,FECALTA,SOLICITUD,NOM,DATASIZE,PER, COMENT,IDIOMA)" & vbCrLf
sConsulta = sConsulta & "                   VALUES ((0x),@FECHA ,@SOLICITUD ,@NOM,@DATASIZE,@PER,@COMENT,@IDIOMA ) " & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "   IF @INSTANCIA=1 " & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       IF @CAMPO >0" & vbCrLf
sConsulta = sConsulta & "           BEGIN" & vbCrLf
sConsulta = sConsulta & "               INSERT INTO PM_COPIA_ADJUN (DATA,FECALTA,NOM,PER,PROVE, ID_CONTRATO, COMENT)" & vbCrLf
sConsulta = sConsulta & "                       VALUES ((0x),GETDATE(),@NOM,@PER,@PROVE,@ID_CONTRATO,@COMENT)     " & vbCrLf
sConsulta = sConsulta & "               " & vbCrLf
sConsulta = sConsulta & "               SET @ID_ADJUN=@@IDENTITY" & vbCrLf
sConsulta = sConsulta & "               " & vbCrLf
sConsulta = sConsulta & "               INSERT INTO COPIA_CAMPO_ADJUN(ADJUN,CAMPO,NOM,IDIOMA,DATASIZE,PER,COMENT,FECALTA)" & vbCrLf
sConsulta = sConsulta & "                   VALUES( @ID_ADJUN,@CAMPO,@NOM,@IDIOMA,@DATASIZE,@PER,@COMENT,GETDATE())" & vbCrLf
sConsulta = sConsulta & "           END " & vbCrLf
sConsulta = sConsulta & "       ELSE" & vbCrLf
sConsulta = sConsulta & "           BEGIN" & vbCrLf
sConsulta = sConsulta & "               INSERT INTO PM_COPIA_ADJUN (DATA,FECALTA,NOM,PER,PROVE, ID_CONTRATO, COMENT)" & vbCrLf
sConsulta = sConsulta & "                       VALUES ((0x),GETDATE(),@NOM,@PER,@PROVE,@ID_CONTRATO,@COMENT)     " & vbCrLf
sConsulta = sConsulta & "               " & vbCrLf
sConsulta = sConsulta & "               SET @ID_ADJUN=@@IDENTITY" & vbCrLf
sConsulta = sConsulta & "               " & vbCrLf
sConsulta = sConsulta & "               INSERT INTO COPIA_LINEA_DESGLOSE_ADJUN(ADJUN,CAMPO_PADRE,CAMPO_HIJO,LINEA,NOM,IDIOMA,DATASIZE,PER,COMENT,FECALTA)" & vbCrLf
sConsulta = sConsulta & "                   VALUES( @ID_ADJUN,@CAMPOPADRE,@CAMPOHIJO,@LINEA,@NOM,@IDIOMA,@DATASIZE,@PER,@COMENT,GETDATE())" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "   END " & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           IF @CAMPO >0" & vbCrLf
sConsulta = sConsulta & "           BEGIN" & vbCrLf
sConsulta = sConsulta & "               INSERT INTO CAMPO_ADJUN (DATA,FECALTA,CAMPO,NOM,DATASIZE,PER, COMENT,IDIOMA)" & vbCrLf
sConsulta = sConsulta & "                   VALUES ((0x),@FECHA,@CAMPO,@NOM,@DATASIZE,@PER,@COMENT,@IDIOMA  ) " & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "           ELSE" & vbCrLf
sConsulta = sConsulta & "               IF @LINEA>0" & vbCrLf
sConsulta = sConsulta & "               BEGIN" & vbCrLf
sConsulta = sConsulta & "                   INSERT INTO LINEA_DESGLOSE_ADJUN (DATA,FECALTA,CAMPO_PADRE,CAMPO_HIJO,LINEA,NOM,DATASIZE, IDIOMA, COMENT)" & vbCrLf
sConsulta = sConsulta & "                   VALUES ((0x),@FECHA,@CAMPOPADRE,@CAMPOHIJO,@LINEA,@NOM,@DATASIZE,@IDIOMA,@COMENT )     " & vbCrLf
sConsulta = sConsulta & "               END" & vbCrLf
sConsulta = sConsulta & "               ELSE" & vbCrLf
sConsulta = sConsulta & "               BEGIN" & vbCrLf
sConsulta = sConsulta & "                   INSERT INTO PM_COPIA_ADJUN (DATA,FECALTA,NOM,PER,PROVE, ID_CONTRATO, COMENT)" & vbCrLf
sConsulta = sConsulta & "                   VALUES ((0x),GETDATE(),@NOM,@PER,@PROVE,@ID_CONTRATO,@COMENT)     " & vbCrLf
sConsulta = sConsulta & "               END" & vbCrLf
sConsulta = sConsulta & "               " & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "    " & vbCrLf
sConsulta = sConsulta & "   IF @ID_ADJUN =0 --Si es igual a 0 es que no lo hemos recogido antes cuando INSTANCIA=1" & vbCrLf
sConsulta = sConsulta & "       SET @ID_ADJUN=@@IDENTITY" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_ESTADISTICAS_ENTORNO_DIA]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_ESTADISTICAS_ENTORNO_DIA]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSAL_ESTADISTICAS_ENTORNO_DIA]  " & vbCrLf
sConsulta = sConsulta & "   @Dia DATETIME," & vbCrLf
sConsulta = sConsulta & "   @entorno nvarchar(50)," & vbCrLf
sConsulta = sConsulta & "   @pyme nvarchar(50)" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "Declare @DiaSig as datetime" & vbCrLf
sConsulta = sConsulta & "set @Diasig = dateadd(day,1,@Dia);" & vbCrLf
sConsulta = sConsulta & "Declare @Hora as datetime;" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- Ahora se cargan los usuarios activos por Dia basandose en el m�ximo de usuarios de cada bloque de hora" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   DELETE FROM FSAL_USUARIOS_DIA" & vbCrLf
sConsulta = sConsulta & "   WHERE   ANYO = DATEPART(""yy"",@Dia)" & vbCrLf
sConsulta = sConsulta & "           and mes = DATEPART(""mm"",@Dia)" & vbCrLf
sConsulta = sConsulta & "           and dia = DATEPART(""dd"",@Dia)" & vbCrLf
sConsulta = sConsulta & "           and ENTORNO = @entorno" & vbCrLf
sConsulta = sConsulta & "           and isnull(PYME,'') = isnull(@Pyme,'');" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO FSAL_USUARIOS_DIA (FECHA,ANYO,MES,DIA,DIA_SEMANA,ENTORNO,PYME,ACTIVOS_GS,ACTIVOS_WEB,ACTIVOS_PORTAL,TOTAL_GS,TOTAL_WEB,TOTAL_PORTAL,TRASPASO) " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   Select" & vbCrLf
sConsulta = sConsulta & "       ACTIVOS.FECHA," & vbCrLf
sConsulta = sConsulta & "       ACTIVOS.ANYO," & vbCrLf
sConsulta = sConsulta & "       ACTIVOS.MES," & vbCrLf
sConsulta = sConsulta & "       ACTIVOS.DIA," & vbCrLf
sConsulta = sConsulta & "       datepart(""dw"",ACTIVOS.FECHA)," & vbCrLf
sConsulta = sConsulta & "       ACTIVOS.ENTORNO," & vbCrLf
sConsulta = sConsulta & "       ISNULL(ACTIVOS.PYME,'')," & vbCrLf
sConsulta = sConsulta & "       ACTIVOS.ACTIVOS_GS," & vbCrLf
sConsulta = sConsulta & "       ACTIVOS.ACTIVOS_WEB," & vbCrLf
sConsulta = sConsulta & "       ACTIVOS.ACTIVOS_PORTAL," & vbCrLf
sConsulta = sConsulta & "       ISNULL(USUARIOS_GS.TOTAL_GS,0) AS TOTAL_GS," & vbCrLf
sConsulta = sConsulta & "       ISNULL(USUARIOS_WEB.TOTAL_WEB,0) AS TOTAL_WEB," & vbCrLf
sConsulta = sConsulta & "       ISNULL(USUARIOS_PORTAL.TOTAL_PORTAL,0) AS TOTAL_PORTAL, " & vbCrLf
sConsulta = sConsulta & "       0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   FROM (" & vbCrLf
sConsulta = sConsulta & "   SELECT" & vbCrLf
sConsulta = sConsulta & "       @Dia as FECHA" & vbCrLf
sConsulta = sConsulta & "       ,ANYO" & vbCrLf
sConsulta = sConsulta & "       ,MES" & vbCrLf
sConsulta = sConsulta & "       ,DIA" & vbCrLf
sConsulta = sConsulta & "       ,ENTORNO" & vbCrLf
sConsulta = sConsulta & "       ,PYME" & vbCrLf
sConsulta = sConsulta & "       ,max(activos_gs) as ACTIVOS_GS" & vbCrLf
sConsulta = sConsulta & "       , max(activos_web) as ACTIVOS_WEB" & vbCrLf
sConsulta = sConsulta & "       , max(activos_portal) as ACTIVOS_PORTAL" & vbCrLf
sConsulta = sConsulta & "   FROM  FSAL_USUARIOS_HORA WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   where  ANYO = DATEPART(""yy"",@Dia)" & vbCrLf
sConsulta = sConsulta & "           and mes = DATEPART(""mm"",@Dia)" & vbCrLf
sConsulta = sConsulta & "           and dia = DATEPART(""dd"",@Dia)" & vbCrLf
sConsulta = sConsulta & "           and ENTORNO = @entorno" & vbCrLf
sConsulta = sConsulta & "           and isnull(PYME,'') = isnull(@Pyme,'')" & vbCrLf
sConsulta = sConsulta & "   group by " & vbCrLf
sConsulta = sConsulta & "       ANYO" & vbCrLf
sConsulta = sConsulta & "       ,MES" & vbCrLf
sConsulta = sConsulta & "       ,DIA" & vbCrLf
sConsulta = sConsulta & "       ,ENTORNO" & vbCrLf
sConsulta = sConsulta & "       ,PYME           " & vbCrLf
sConsulta = sConsulta & "   ) ACTIVOS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- Total_GS" & vbCrLf
sConsulta = sConsulta & "(" & vbCrLf
sConsulta = sConsulta & "SELECT" & vbCrLf
sConsulta = sConsulta & "datepart(""yy"",FISERVER) as ANYO" & vbCrLf
sConsulta = sConsulta & ",datepart(""mm"",FISERVER) as MES" & vbCrLf
sConsulta = sConsulta & ",datepart(""dd"",FISERVER) as DIA" & vbCrLf
sConsulta = sConsulta & ",count( distinct usuario) AS TOTAL_GS" & vbCrLf
sConsulta = sConsulta & "from fsal_accesos WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "where " & vbCrLf
sConsulta = sConsulta & "   producto = 'FULLSTEP GS'" & vbCrLf
sConsulta = sConsulta & "   and fsal_entorno = @entorno" & vbCrLf
sConsulta = sConsulta & "   and isnull(fsal_pyme,'') = isnull(@pyme,'')" & vbCrLf
sConsulta = sConsulta & "group by " & vbCrLf
sConsulta = sConsulta & "datepart(""yy"",FISERVER)" & vbCrLf
sConsulta = sConsulta & ",datepart(""mm"",FISERVER)" & vbCrLf
sConsulta = sConsulta & ",datepart(""dd"",FISERVER)" & vbCrLf
sConsulta = sConsulta & ") USUARIOS_GS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ON ACTIVOS.ANYO = USUARIOS_GS.ANYO" & vbCrLf
sConsulta = sConsulta & "   AND ACTIVOS.MES = USUARIOS_GS.MES" & vbCrLf
sConsulta = sConsulta & "   AND ACTIVOS.DIA = USUARIOS_GS.DIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- Total_WEB" & vbCrLf
sConsulta = sConsulta & "(" & vbCrLf
sConsulta = sConsulta & "SELECT" & vbCrLf
sConsulta = sConsulta & "datepart(""yy"",FISERVER) as ANYO" & vbCrLf
sConsulta = sConsulta & ",datepart(""mm"",FISERVER) as MES" & vbCrLf
sConsulta = sConsulta & ",datepart(""dd"",FISERVER) as DIA" & vbCrLf
sConsulta = sConsulta & ",count( distinct usuario) AS TOTAL_WEB" & vbCrLf
sConsulta = sConsulta & "from fsal_accesos WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "where " & vbCrLf
sConsulta = sConsulta & "   producto = 'FULLSTEP WEB'" & vbCrLf
sConsulta = sConsulta & "   and fsal_entorno = @entorno" & vbCrLf
sConsulta = sConsulta & "   and isnull(fsal_pyme,'') = isnull(@pyme,'')" & vbCrLf
sConsulta = sConsulta & "group by " & vbCrLf
sConsulta = sConsulta & "datepart(""yy"",FISERVER)" & vbCrLf
sConsulta = sConsulta & ",datepart(""mm"",FISERVER)" & vbCrLf
sConsulta = sConsulta & ",datepart(""dd"",FISERVER)" & vbCrLf
sConsulta = sConsulta & ") USUARIOS_WEB" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ON     ACTIVOS.ANYO  = USUARIOS_WEB.ANYO" & vbCrLf
sConsulta = sConsulta & "   AND ACTIVOS.MES = USUARIOS_WEB.MES" & vbCrLf
sConsulta = sConsulta & "   AND ACTIVOS.DIA = USUARIOS_WEB.DIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- Total_PORTAL" & vbCrLf
sConsulta = sConsulta & "(" & vbCrLf
sConsulta = sConsulta & "SELECT" & vbCrLf
sConsulta = sConsulta & "datepart(""yy"",FISERVER) as ANYO" & vbCrLf
sConsulta = sConsulta & ",datepart(""mm"",FISERVER) as MES" & vbCrLf
sConsulta = sConsulta & ",datepart(""dd"",FISERVER) as DIA" & vbCrLf
sConsulta = sConsulta & ",count( distinct usuario) AS TOTAL_PORTAL" & vbCrLf
sConsulta = sConsulta & "from fsal_accesos WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "where " & vbCrLf
sConsulta = sConsulta & "   producto = 'FULLSTEP PORTAL'" & vbCrLf
sConsulta = sConsulta & "   and fsal_entorno = @entorno" & vbCrLf
sConsulta = sConsulta & "   and isnull(fsal_pyme,'') = isnull(@pyme,'')" & vbCrLf
sConsulta = sConsulta & "group by " & vbCrLf
sConsulta = sConsulta & "datepart(""yy"",FISERVER)" & vbCrLf
sConsulta = sConsulta & ",datepart(""mm"",FISERVER)" & vbCrLf
sConsulta = sConsulta & ",datepart(""dd"",FISERVER)" & vbCrLf
sConsulta = sConsulta & ") USUARIOS_PORTAL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ON     ACTIVOS.ANYO  = USUARIOS_PORTAL.ANYO" & vbCrLf
sConsulta = sConsulta & "   AND ACTIVOS.MES = USUARIOS_PORTAL.MES" & vbCrLf
sConsulta = sConsulta & "   AND ACTIVOS.DIA = USUARIOS_PORTAL.DIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- Fin de procesado de las horas" & vbCrLf
sConsulta = sConsulta & "-- Por �ltimo a�ado los distintos ususrios conectados hoy a las tablas de registros semanales, mensuales y anuales" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Nos aseguramos de utilizar un formato de semana europeo " & vbCrLf
sConsulta = sConsulta & "-- Solo afecta a la sesi�n por lo que no alteramos el formato por defecto del sevidor" & vbCrLf
sConsulta = sConsulta & "set datefirst 1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO FSAL_USUARIOS_DIS_SEMANA (FECHA,ANYO,MES,DIA,ENTORNO,PYME,PRODUCTO,USU,TRASPASO)" & vbCrLf
sConsulta = sConsulta & "          SELECT DISTINCT " & vbCrLf
sConsulta = sConsulta & "           convert(datetime,ltrim(datepart(""yy"",CONVERT(datetime,CONVERT (char(10), dateadd(d,-1*(datepart(dw,FISERVER)-1),FISERVER), 112)))) +'-'+ ltrim(datepart(""mm"",CONVERT(datetime,CONVERT (char(10), dateadd(d,-1*(datepart(dw,FISERVER)-1),FISERVER), 112))))+'-'+ltrim(datepart(""dd"",CONVERT(datetime,CONVERT (char(10), dateadd(d,-1*(datepart(dw,FISERVER)-1),FISERVER), 112))))) as FISERVER" & vbCrLf
sConsulta = sConsulta & "           ,datepart(""yy"",CONVERT(datetime,CONVERT (char(10), dateadd(d,-1*(datepart(dw,FISERVER)-1),FISERVER), 112))) AS ANYO" & vbCrLf
sConsulta = sConsulta & "           ,datepart(""mm"",CONVERT(datetime,CONVERT (char(10), dateadd(d,-1*(datepart(dw,FISERVER)-1),FISERVER), 112))) AS MES" & vbCrLf
sConsulta = sConsulta & "           ,datepart(""dd"",CONVERT(datetime,CONVERT (char(10), dateadd(d,-1*(datepart(dw,FISERVER)-1),FISERVER), 112))) AS DIA -- OJO!!!! Primer d�a de la semaan en formato europeo" & vbCrLf
sConsulta = sConsulta & "           , FSAL_entorno AS ENTORNO" & vbCrLf
sConsulta = sConsulta & "           , FSAL_pyme AS PYME" & vbCrLf
sConsulta = sConsulta & "           , producto AS PRODUCTO" & vbCrLf
sConsulta = sConsulta & "           ,usuario as USU" & vbCrLf
sConsulta = sConsulta & "           ,0" & vbCrLf
sConsulta = sConsulta & "            FROM FSAL_ACCESOS WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "          WHERE usuario <> ''" & vbCrLf
sConsulta = sConsulta & "          --and CONVERT(datetime,CONVERT (char(10), FISERVER,112)) >= @Dia " & vbCrLf
sConsulta = sConsulta & "          and NOT EXISTS ( SELECT * FROM FSAL_USUARIOS_DIS_SEMANA" & vbCrLf
sConsulta = sConsulta & "                           WHERE FSAL_USUARIOS_DIS_SEMANA.ANYO = datepart(""yy"",FSAL_ACCESOS.FISERVER)" & vbCrLf
sConsulta = sConsulta & "                               and FSAL_USUARIOS_DIS_SEMANA.MES = datepart(""mm"",FSAL_ACCESOS.FISERVER)" & vbCrLf
sConsulta = sConsulta & "                               and FSAL_USUARIOS_DIS_SEMANA.DIA = datepart(""dd"",CONVERT(datetime,CONVERT (char(10), dateadd(d,-1*(datepart(dw,FISERVER)-1),FISERVER), 112)))" & vbCrLf
sConsulta = sConsulta & "                               and FSAL_USUARIOS_DIS_SEMANA.ENTORNO = FSAL_ACCESOS.FSAL_ENTORNO" & vbCrLf
sConsulta = sConsulta & "                               and isnull(FSAL_USUARIOS_DIS_SEMANA.pyme,'') = isnull(FSAL_ACCESOS.FSAL_pyme,'')" & vbCrLf
sConsulta = sConsulta & "                               and FSAL_USUARIOS_DIS_SEMANA.producto = FSAL_ACCESOS.producto" & vbCrLf
sConsulta = sConsulta & "                               and FSAL_USUARIOS_DIS_SEMANA.usu = FSAL_ACCESOS.usuario)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO FSAL_USUARIOS_DIS_MES (ANYO,MES,ENTORNO,PYME,PRODUCTO,USU,TRASPASO)" & vbCrLf
sConsulta = sConsulta & "          SELECT DISTINCT " & vbCrLf
sConsulta = sConsulta & "           datepart(""yy"",FISERVER) AS ANYO" & vbCrLf
sConsulta = sConsulta & "           ,datepart(""mm"",FISERVER) AS MES" & vbCrLf
sConsulta = sConsulta & "           , FSAL_entorno AS ENTORNO" & vbCrLf
sConsulta = sConsulta & "           , FSAL_pyme AS PYME" & vbCrLf
sConsulta = sConsulta & "           , producto AS PRODUCTO" & vbCrLf
sConsulta = sConsulta & "           ,usuario as USU" & vbCrLf
sConsulta = sConsulta & "           ,0" & vbCrLf
sConsulta = sConsulta & "            FROM FSAL_ACCESOS WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "          WHERE usuario <> ''" & vbCrLf
sConsulta = sConsulta & "              --and CONVERT(datetime,CONVERT (char(10), FISERVER,112)) = @Dia " & vbCrLf
sConsulta = sConsulta & "              and NOT EXISTS ( SELECT * FROM FSAL_USUARIOS_DIS_MES" & vbCrLf
sConsulta = sConsulta & "                           WHERE FSAL_USUARIOS_DIS_MES.ANYO = datepart(""yy"",FSAL_ACCESOS.FISERVER)" & vbCrLf
sConsulta = sConsulta & "                               and FSAL_USUARIOS_DIS_MES.MES = datepart(""mm"",FSAL_ACCESOS.FISERVER)" & vbCrLf
sConsulta = sConsulta & "                               and FSAL_USUARIOS_DIS_MES.ENTORNO = FSAL_ACCESOS.FSAL_ENTORNO" & vbCrLf
sConsulta = sConsulta & "                               and isnull(FSAL_USUARIOS_DIS_MES.pyme,'') = isnull(FSAL_ACCESOS.FSAL_pyme,'')" & vbCrLf
sConsulta = sConsulta & "                               and FSAL_USUARIOS_DIS_MES.producto = FSAL_ACCESOS.producto" & vbCrLf
sConsulta = sConsulta & "                               and FSAL_USUARIOS_DIS_MES.usu = FSAL_ACCESOS.usuario)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO FSAL_USUARIOS_DIS_ANYO (ANYO,ENTORNO,PYME,PRODUCTO,USU,TRASPASO)" & vbCrLf
sConsulta = sConsulta & "          SELECT DISTINCT " & vbCrLf
sConsulta = sConsulta & "           datepart(""yy"",FISERVER) AS ANYO" & vbCrLf
sConsulta = sConsulta & "           , FSAL_entorno AS ENTORNO" & vbCrLf
sConsulta = sConsulta & "           , FSAL_pyme AS PYME" & vbCrLf
sConsulta = sConsulta & "           , producto AS PRODUCTO" & vbCrLf
sConsulta = sConsulta & "           ,usuario as USU" & vbCrLf
sConsulta = sConsulta & "           ,0" & vbCrLf
sConsulta = sConsulta & "            FROM FSAL_ACCESOS WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "          WHERE usuario <> ''" & vbCrLf
sConsulta = sConsulta & "              --and CONVERT(datetime,CONVERT (char(10), FISERVER,112)) = @Dia " & vbCrLf
sConsulta = sConsulta & "              and NOT EXISTS ( SELECT * FROM FSAL_USUARIOS_DIS_ANYO" & vbCrLf
sConsulta = sConsulta & "                           WHERE FSAL_USUARIOS_DIS_ANYO.ANYO = datepart(""yy"",FSAL_ACCESOS.FISERVER)" & vbCrLf
sConsulta = sConsulta & "                               and FSAL_USUARIOS_DIS_ANYO.ENTORNO = FSAL_ACCESOS.FSAL_ENTORNO" & vbCrLf
sConsulta = sConsulta & "                               and isnull(FSAL_USUARIOS_DIS_ANYO.pyme,'') = isnull(FSAL_ACCESOS.FSAL_pyme,'')" & vbCrLf
sConsulta = sConsulta & "                               and FSAL_USUARIOS_DIS_ANYO.producto = FSAL_ACCESOS.producto" & vbCrLf
sConsulta = sConsulta & "                               and FSAL_USUARIOS_DIS_ANYO.usu = FSAL_ACCESOS.usuario)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_ESTADISTICAS_ENTORNO_HORA]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_ESTADISTICAS_ENTORNO_HORA]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSAL_ESTADISTICAS_ENTORNO_HORA]  " & vbCrLf
sConsulta = sConsulta & "   @Desde DATETIME," & vbCrLf
sConsulta = sConsulta & "   @Hasta DATETIME," & vbCrLf
sConsulta = sConsulta & "   @entorno nvarchar(50)," & vbCrLf
sConsulta = sConsulta & "   @pyme nvarchar(50)" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "Declare @HoraSig as datetime" & vbCrLf
sConsulta = sConsulta & "set @Horasig = dateadd(hour,1,@Desde);" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if (@Horasig > @hasta) begin set @horasig = @hasta end" & vbCrLf
sConsulta = sConsulta & "-- Para permiir genera estad�sticas parciales" & vbCrLf
sConsulta = sConsulta & "-- la sentencia INSERT INTO SELECT FROM puede fallar por existir previamente los registros" & vbCrLf
sConsulta = sConsulta & "-- Tampoco puedo detectar el error y hacer un UPDATE INNER JOIN porque puede que alguno no exista" & vbCrLf
sConsulta = sConsulta & "-- Asi que Me aseguro de borrar los datos que existan previamente y hago exclusivamente la INSERT" & vbCrLf
sConsulta = sConsulta & "delete FROM FSAL_USUARIOS_MINUTO" & vbCrLf
sConsulta = sConsulta & "WHERE --ANYO =  DATEPART(""yy"",@Desde)" & vbCrLf
sConsulta = sConsulta & "   --AND MES =  DATEPART(""mm"",@Desde)" & vbCrLf
sConsulta = sConsulta & "   --AND Dia =  DATEPART(""dd"",@Desde)" & vbCrLf
sConsulta = sConsulta & "   --AND Hora = DATEPART(""hh"",@Desde)" & vbCrLf
sConsulta = sConsulta & "   --AND MIN = right(replicate('0', 2) + ltrim(CAST((((60*DATEPART(""hh"",@Desde)+DATEPART(""Mi"",@Desde)) / 5) * 5) % 60 AS varchar)), 2)" & vbCrLf
sConsulta = sConsulta & "   entorno = @entorno" & vbCrLf
sConsulta = sConsulta & "   AND isnull(pyme,'') = isnull(@pyme,'')" & vbCrLf
sConsulta = sConsulta & "   AND FECHA >= @Desde" & vbCrLf
sConsulta = sConsulta & "   and FECHA <= @Hasta;" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- COMMON TABLE EXPRESSION para obtener todas las entradas (bloques de 5 min) entre las fechas-horas indicadas" & vbCrLf
sConsulta = sConsulta & "-- OJO! Esta CTE s�lo admite 100 pasos de iteraci�n/Recursi�n, pero se supone que es v�lida ya que son se van" & vbCrLf
sConsulta = sConsulta & "-- a consultar fragmentos de 1 hora de duracci�n (12 bloques de 5 min)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "WITH cteMinutos as" & vbCrLf
sConsulta = sConsulta & "( " & vbCrLf
sConsulta = sConsulta & "    SELECT @Desde  AS MisMinutos" & vbCrLf
sConsulta = sConsulta & "    UNION ALL" & vbCrLf
sConsulta = sConsulta & "    SELECT dateadd(MINUTE,5,MisMinutos)" & vbCrLf
sConsulta = sConsulta & "    FROM cteMinutos " & vbCrLf
sConsulta = sConsulta & "    WHERE dateadd(minute,5,MisMinutos) < @HoraSig" & vbCrLf
sConsulta = sConsulta & ")" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO FSAL_USUARIOS_MINUTO (FECHA,ANYO,Mes,Dia,Hora,Min,Entorno,Pyme,ACTIVOS_GS,ACTIVOS_WEB,ACTIVOS_PORTAL,TRASPASO)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT " & vbCrLf
sConsulta = sConsulta & "   MisMinutos as FECHA" & vbCrLf
sConsulta = sConsulta & "   ,DATEPART(""yy"",MisMinutos) as ANYO" & vbCrLf
sConsulta = sConsulta & "   ,DATEPART(""mm"",MisMinutos) as MES" & vbCrLf
sConsulta = sConsulta & "   ,DATEPART(""dd"",MisMinutos) as DIA" & vbCrLf
sConsulta = sConsulta & "   ,DATEPART(""hh"",MisMinutos) as HORA  " & vbCrLf
sConsulta = sConsulta & "   ,right(replicate('0', 2) + ltrim(CAST((((60*DATEPART(""hh"",MisMinutos)+DATEPART(""Mi"",MisMinutos)) / 5) * 5) % 60 AS varchar)), 2) as MIN" & vbCrLf
sConsulta = sConsulta & "   ,@entorno as ENTORNO" & vbCrLf
sConsulta = sConsulta & "   ,ISNULL(@pyme,'') as PYME" & vbCrLf
sConsulta = sConsulta & "   ,ISNULL(NUM_USUARIOS_GS,0) as ACTIVOS_GS" & vbCrLf
sConsulta = sConsulta & "   ,ISNULL(NUM_USUARIOS_WEB,0) as ACTIVOS_WEB" & vbCrLf
sConsulta = sConsulta & "   ,ISNULL(NUM_USUARIOS_PORTAL,0) as ACTIVOS_PORTAL" & vbCrLf
sConsulta = sConsulta & "   ,0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "FROM cteMinutos " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "left JOIN (  -- Usuarios de PORTAL" & vbCrLf
sConsulta = sConsulta & "   Select right(replicate('0', 2) + ltrim(cast((((60*DATEPART(""hh"",FISERVER)+DATEPART(""Mi"",FISERVER)) / 5) * 5) / 60 as varchar)), 2)  + ':' + " & vbCrLf
sConsulta = sConsulta & "   right(replicate('0', 2) + ltrim(CAST((((60*DATEPART(""hh"",FISERVER)+DATEPART(""Mi"",FISERVER)) / 5) * 5) % 60 AS varchar)), 2) as HORA_ACCESO " & vbCrLf
sConsulta = sConsulta & "   ,COUNT(DISTINCT USUARIO) as NUM_USUARIOS_PORTAL" & vbCrLf
sConsulta = sConsulta & "   from fsal_accesos WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   WHERE FISERVER < @HoraSig AND FISERVER > @Desde AND PRODUCTO = 'FULLSTEP PORTAL' AND FSAL_ENTORNO = @entorno AND isnull(FSAL_PYME,'') = isnull(@pyme,'')" & vbCrLf
sConsulta = sConsulta & "   GROUP BY right(replicate('0', 2) + ltrim(cast((((60*DATEPART(""hh"",FISERVER)+DATEPART(""Mi"",FISERVER)) / 5) * 5) / 60 as varchar)), 2)  + ':' + " & vbCrLf
sConsulta = sConsulta & "   right(replicate('0', 2) + ltrim(CAST((((60*DATEPART(""hh"",FISERVER)+DATEPART(""Mi"",FISERVER)) / 5) * 5) % 60 AS varchar)), 2) ) as fsal_horas_portal " & vbCrLf
sConsulta = sConsulta & "   on right(replicate('0', 2) + ltrim(cast((((60*DATEPART(""hh"",MisMinutos)+ DATEPART(""Mi"",MisMinutos)) / 5) * 5) /60 as varchar)), 2)+ ':' + " & vbCrLf
sConsulta = sConsulta & "   right(replicate('0', 2) + ltrim(CAST((((60*DATEPART(""hh"",MisMinutos)+DATEPART(""Mi"",MisMinutos)) / 5) * 5) % 60 AS varchar)), 2)=fsal_horas_portal.HORA_ACCESO " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "left JOIN ( -- Usuarios de FSNWEB" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   Select right(replicate('0', 2) + ltrim(cast((((60*DATEPART(""hh"",FISERVER)+DATEPART(""Mi"",FISERVER)) / 5) * 5) / 60 as varchar)), 2)  + ':' + " & vbCrLf
sConsulta = sConsulta & "   right(replicate('0', 2) + ltrim(CAST((((60*DATEPART(""hh"",FISERVER)+DATEPART(""Mi"",FISERVER)) / 5) * 5) % 60 AS varchar)), 2) as HORA_ACCESO " & vbCrLf
sConsulta = sConsulta & "   ,COUNT(DISTINCT USUARIO) as NUM_USUARIOS_WEB" & vbCrLf
sConsulta = sConsulta & "   from fsal_accesos WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   WHERE FISERVER < @HoraSig AND FISERVER > @Desde AND PRODUCTO = 'FULLSTEP WEB' AND FSAL_ENTORNO=@entorno AND isnull(FSAL_PYME,'') = isnull(@pyme,'')" & vbCrLf
sConsulta = sConsulta & "   GROUP BY right(replicate('0', 2) + ltrim(cast((((60*DATEPART(""hh"",FISERVER)+DATEPART(""Mi"",FISERVER)) / 5) * 5) / 60 as varchar)), 2)  + ':' + " & vbCrLf
sConsulta = sConsulta & "   right(replicate('0', 2) + ltrim(CAST((((60*DATEPART(""hh"",FISERVER)+DATEPART(""Mi"",FISERVER)) / 5) * 5) % 60 AS varchar)), 2) ) as fsal_horas_web " & vbCrLf
sConsulta = sConsulta & "   on right(replicate('0', 2) + ltrim(cast((((60*DATEPART(""hh"",MisMinutos)+ DATEPART(""Mi"",MisMinutos)) / 5) * 5) /60 as varchar)), 2)+ ':' + " & vbCrLf
sConsulta = sConsulta & "   right(replicate('0', 2) + ltrim(CAST((((60*DATEPART(""hh"",MisMinutos)+DATEPART(""Mi"",MisMinutos)) / 5) * 5) % 60 AS varchar)), 2)=fsal_horas_web.HORA_ACCESO " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "left JOIN (  -- Usuarios de GS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   Select right(replicate('0', 2) + ltrim(cast((((60*DATEPART(""hh"",FISERVER)+DATEPART(""Mi"",FISERVER)) / 5) * 5) / 60 as varchar)), 2)  + ':' + " & vbCrLf
sConsulta = sConsulta & "   right(replicate('0', 2) + ltrim(CAST((((60*DATEPART(""hh"",FISERVER)+DATEPART(""Mi"",FISERVER)) / 5) * 5) % 60 AS varchar)), 2) as HORA_ACCESO " & vbCrLf
sConsulta = sConsulta & "   ,COUNT(DISTINCT USUARIO) as NUM_USUARIOS_GS" & vbCrLf
sConsulta = sConsulta & "   from fsal_accesos WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   WHERE FISERVER < @HoraSig AND FISERVER > @Desde AND PRODUCTO = 'FULLSTEP GS' AND FSAL_ENTORNO=@entorno AND isnull(FSAL_PYME,'') = isnull(@pyme,'')" & vbCrLf
sConsulta = sConsulta & "   GROUP BY right(replicate('0', 2) + ltrim(cast((((60*DATEPART(""hh"",FISERVER)+DATEPART(""Mi"",FISERVER)) / 5) * 5) / 60 as varchar)), 2)  + ':' + " & vbCrLf
sConsulta = sConsulta & "   right(replicate('0', 2) + ltrim(CAST((((60*DATEPART(""hh"",FISERVER)+DATEPART(""Mi"",FISERVER)) / 5) * 5) % 60 AS varchar)), 2) ) as fsal_horas_gs " & vbCrLf
sConsulta = sConsulta & "   on right(replicate('0', 2) + ltrim(cast((((60*DATEPART(""hh"",MisMinutos)+ DATEPART(""Mi"",MisMinutos)) / 5) * 5) /60 as varchar)), 2)+ ':' + " & vbCrLf
sConsulta = sConsulta & "   right(replicate('0', 2) + ltrim(CAST((((60*DATEPART(""hh"",MisMinutos)+DATEPART(""Mi"",MisMinutos)) / 5) * 5) % 60 AS varchar)), 2)=fsal_horas_gs.HORA_ACCESO " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- Se cargan los usuarios activos por horas basandose en el m�ximo de usuarios de cada bloque de 5 minutos" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   Declare @DIA datetime" & vbCrLf
sConsulta = sConsulta & "   set @DIA = convert(datetime,convert(varchar(10),@Desde,102))" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   DELETE FROM FSAL_USUARIOS_HORA " & vbCrLf
sConsulta = sConsulta & "   WHERE   ANYO = DATEPART(""yy"",@Dia)" & vbCrLf
sConsulta = sConsulta & "           and mes = DATEPART(""mm"",@Dia)" & vbCrLf
sConsulta = sConsulta & "           and dia = DATEPART(""dd"",@Dia)" & vbCrLf
sConsulta = sConsulta & "           and (HORA = DATEPART(""hh"",@Desde) or HORA= DATEPART(""hh"",@hasta))" & vbCrLf
sConsulta = sConsulta & "           and ENTORNO = @entorno" & vbCrLf
sConsulta = sConsulta & "           and isnull(PYME,'') = isnull(@Pyme,'')" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "   INSERT INTO FSAL_USUARIOS_HORA (FECHA,ANYO,MES,DIA,HORA,ENTORNO,PYME,ACTIVOS_GS,ACTIVOS_WEB,ACTIVOS_PORTAL,TOTAL_GS,TOTAL_WEB,TOTAL_PORTAL,traspaso) " & vbCrLf
sConsulta = sConsulta & "         " & vbCrLf
sConsulta = sConsulta & "   Select" & vbCrLf
sConsulta = sConsulta & "       ACTIVOS.FECHA," & vbCrLf
sConsulta = sConsulta & "       ACTIVOS.ANYO," & vbCrLf
sConsulta = sConsulta & "       ACTIVOS.MES," & vbCrLf
sConsulta = sConsulta & "       ACTIVOS.DIA," & vbCrLf
sConsulta = sConsulta & "       ACTIVOS.HORA," & vbCrLf
sConsulta = sConsulta & "       ACTIVOS.ENTORNO," & vbCrLf
sConsulta = sConsulta & "       ACTIVOS.PYME," & vbCrLf
sConsulta = sConsulta & "       ACTIVOS.ACTIVOS_GS," & vbCrLf
sConsulta = sConsulta & "       ACTIVOS.ACTIVOS_WEB," & vbCrLf
sConsulta = sConsulta & "       ACTIVOS.ACTIVOS_PORTAL," & vbCrLf
sConsulta = sConsulta & "       ISNULL(USUARIOS_GS.TOTAL_GS,0) AS TOTAL_GS," & vbCrLf
sConsulta = sConsulta & "       ISNULL(USUARIOS_WEB.TOTAL_WEB,0) AS TOTAL_WEB," & vbCrLf
sConsulta = sConsulta & "       ISNULL(USUARIOS_PORTAL.TOTAL_PORTAL,0) AS TOTAL_PORTAL," & vbCrLf
sConsulta = sConsulta & "       0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   FROM (" & vbCrLf
sConsulta = sConsulta & "   SELECT" & vbCrLf
sConsulta = sConsulta & "       DATEADD(hour,HORA, @Dia)as FECHA" & vbCrLf
sConsulta = sConsulta & "       ,ANYO" & vbCrLf
sConsulta = sConsulta & "       ,MES" & vbCrLf
sConsulta = sConsulta & "       ,DIA" & vbCrLf
sConsulta = sConsulta & "       ,HORA           " & vbCrLf
sConsulta = sConsulta & "       ,ENTORNO" & vbCrLf
sConsulta = sConsulta & "       ,PYME" & vbCrLf
sConsulta = sConsulta & "       ,max(activos_gs) as ACTIVOS_GS" & vbCrLf
sConsulta = sConsulta & "       , max(activos_web) as ACTIVOS_WEB" & vbCrLf
sConsulta = sConsulta & "       , max(activos_portal) as ACTIVOS_PORTAL" & vbCrLf
sConsulta = sConsulta & "   FROM  FSAL_USUARIOS_MINUTO WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   where  ANYO = DATEPART(""yy"",@Dia)" & vbCrLf
sConsulta = sConsulta & "           and mes = DATEPART(""mm"",@Dia)" & vbCrLf
sConsulta = sConsulta & "           and dia = DATEPART(""dd"",@Dia)" & vbCrLf
sConsulta = sConsulta & "           --and HORA= DATEPART(""hh"",@Dia)" & vbCrLf
sConsulta = sConsulta & "           and ENTORNO = @entorno" & vbCrLf
sConsulta = sConsulta & "           and isnull(PYME,'') = isnull(@Pyme,'')" & vbCrLf
sConsulta = sConsulta & "           and fecha >=@Desde" & vbCrLf
sConsulta = sConsulta & "           and FECHA <=@Hasta" & vbCrLf
sConsulta = sConsulta & "   group by " & vbCrLf
sConsulta = sConsulta & "       ANYO" & vbCrLf
sConsulta = sConsulta & "       ,MES" & vbCrLf
sConsulta = sConsulta & "       ,DIA" & vbCrLf
sConsulta = sConsulta & "       ,HORA" & vbCrLf
sConsulta = sConsulta & "       ,ENTORNO" & vbCrLf
sConsulta = sConsulta & "       ,PYME           " & vbCrLf
sConsulta = sConsulta & "   ) ACTIVOS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- Total_GS" & vbCrLf
sConsulta = sConsulta & "(" & vbCrLf
sConsulta = sConsulta & "SELECT" & vbCrLf
sConsulta = sConsulta & "datepart(""yy"",FISERVER) as ANYO" & vbCrLf
sConsulta = sConsulta & ",datepart(""mm"",FISERVER) as MES" & vbCrLf
sConsulta = sConsulta & ",datepart(""dd"",FISERVER) as DIA" & vbCrLf
sConsulta = sConsulta & ",datepart(""hh"",FISERVER) as HORA" & vbCrLf
sConsulta = sConsulta & ",count( distinct usuario) AS TOTAL_GS" & vbCrLf
sConsulta = sConsulta & "from fsal_accesos WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "where " & vbCrLf
sConsulta = sConsulta & "   producto = 'FULLSTEP GS'" & vbCrLf
sConsulta = sConsulta & "   and fsal_entorno = @entorno" & vbCrLf
sConsulta = sConsulta & "   and isnull(fsal_pyme,'') = isnull(@pyme,'')" & vbCrLf
sConsulta = sConsulta & "group by " & vbCrLf
sConsulta = sConsulta & "datepart(""yy"",FISERVER)" & vbCrLf
sConsulta = sConsulta & ",datepart(""mm"",FISERVER)" & vbCrLf
sConsulta = sConsulta & ",datepart(""dd"",FISERVER)" & vbCrLf
sConsulta = sConsulta & ",datepart(""hh"",FISERVER)" & vbCrLf
sConsulta = sConsulta & ") USUARIOS_GS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ON ACTIVOS.ANYO = USUARIOS_GS.ANYO" & vbCrLf
sConsulta = sConsulta & "   AND ACTIVOS.MES = USUARIOS_GS.MES" & vbCrLf
sConsulta = sConsulta & "   AND ACTIVOS.DIA = USUARIOS_GS.DIA" & vbCrLf
sConsulta = sConsulta & "   AND ACTIVOS.HORA = USUARIOS_GS.HORA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- Total_WEB" & vbCrLf
sConsulta = sConsulta & "(" & vbCrLf
sConsulta = sConsulta & "SELECT" & vbCrLf
sConsulta = sConsulta & "datepart(""yy"",FISERVER) as ANYO" & vbCrLf
sConsulta = sConsulta & ",datepart(""mm"",FISERVER) as MES" & vbCrLf
sConsulta = sConsulta & ",datepart(""dd"",FISERVER) as DIA" & vbCrLf
sConsulta = sConsulta & ",datepart(""hh"",FISERVER) as HORA" & vbCrLf
sConsulta = sConsulta & ",count( distinct usuario) AS TOTAL_WEB" & vbCrLf
sConsulta = sConsulta & "from fsal_accesos WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "where " & vbCrLf
sConsulta = sConsulta & "   producto = 'FULLSTEP WEB'" & vbCrLf
sConsulta = sConsulta & "   and fsal_entorno = @entorno" & vbCrLf
sConsulta = sConsulta & "   and isnull(fsal_pyme,'') = isnull(@pyme,'')" & vbCrLf
sConsulta = sConsulta & "group by " & vbCrLf
sConsulta = sConsulta & "datepart(""yy"",FISERVER)" & vbCrLf
sConsulta = sConsulta & ",datepart(""mm"",FISERVER)" & vbCrLf
sConsulta = sConsulta & ",datepart(""dd"",FISERVER)" & vbCrLf
sConsulta = sConsulta & ",datepart(""hh"",FISERVER)" & vbCrLf
sConsulta = sConsulta & ") USUARIOS_WEB" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ON     ACTIVOS.ANYO  = USUARIOS_WEB.ANYO" & vbCrLf
sConsulta = sConsulta & "   AND ACTIVOS.MES = USUARIOS_WEB.MES" & vbCrLf
sConsulta = sConsulta & "   AND ACTIVOS.DIA = USUARIOS_WEB.DIA" & vbCrLf
sConsulta = sConsulta & "   AND ACTIVOS.HORA = USUARIOS_WEB.HORA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- Total_PORTAL" & vbCrLf
sConsulta = sConsulta & "(" & vbCrLf
sConsulta = sConsulta & "SELECT" & vbCrLf
sConsulta = sConsulta & "datepart(""yy"",FISERVER) as ANYO" & vbCrLf
sConsulta = sConsulta & ",datepart(""mm"",FISERVER) as MES" & vbCrLf
sConsulta = sConsulta & ",datepart(""dd"",FISERVER) as DIA" & vbCrLf
sConsulta = sConsulta & ",datepart(""hh"",FISERVER) as HORA" & vbCrLf
sConsulta = sConsulta & ",count( distinct usuario) AS TOTAL_PORTAL" & vbCrLf
sConsulta = sConsulta & "from fsal_accesos WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "where " & vbCrLf
sConsulta = sConsulta & "   producto = 'FULLSTEP PORTAL'" & vbCrLf
sConsulta = sConsulta & "   and fsal_entorno = @entorno" & vbCrLf
sConsulta = sConsulta & "   and isnull(fsal_pyme,'') = isnull(@pyme,'')" & vbCrLf
sConsulta = sConsulta & "group by " & vbCrLf
sConsulta = sConsulta & "datepart(""yy"",FISERVER)" & vbCrLf
sConsulta = sConsulta & ",datepart(""mm"",FISERVER)" & vbCrLf
sConsulta = sConsulta & ",datepart(""dd"",FISERVER)" & vbCrLf
sConsulta = sConsulta & ",datepart(""hh"",FISERVER)" & vbCrLf
sConsulta = sConsulta & ") USUARIOS_PORTAL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ON     ACTIVOS.ANYO  = USUARIOS_PORTAL.ANYO" & vbCrLf
sConsulta = sConsulta & "   AND ACTIVOS.MES = USUARIOS_PORTAL.MES" & vbCrLf
sConsulta = sConsulta & "   AND ACTIVOS.DIA = USUARIOS_PORTAL.DIA" & vbCrLf
sConsulta = sConsulta & "   AND ACTIVOS.HORA = USUARIOS_PORTAL.HORA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_CARGAR_ACCESOS_A_CORPORATE]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_CARGAR_ACCESOS_A_CORPORATE]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta


sConsulta = "CREATE PROCEDURE [dbo].[FSAL_CARGAR_ACCESOS_A_CORPORATE] @TAB AS dbo.TABLA_FSAL_ACCESOS READONLY, @RES INT OUTPUT AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE FSAL_ACCESOS_HIST" & vbCrLf
sConsulta = sConsulta & "SET" & vbCrLf
sConsulta = sConsulta & "PAGINA=TAB.PAGINA," & vbCrLf
sConsulta = sConsulta & "FSAL_ENTORNO=TAB.FSAL_ENTORNO," & vbCrLf
sConsulta = sConsulta & "PRODUCTO=TAB.PRODUCTO," & vbCrLf
sConsulta = sConsulta & "FICLIPREPAR=TAB.FICLIPREPAR," & vbCrLf
sConsulta = sConsulta & "FISERVER=TAB.FISERVER," & vbCrLf
sConsulta = sConsulta & "FFSERVER=TAB.FFSERVER," & vbCrLf
sConsulta = sConsulta & "FICLIRESP=TAB.FICLIRESP," & vbCrLf
sConsulta = sConsulta & "FFCLIRESP=TAB.FFCLIRESP," & vbCrLf
sConsulta = sConsulta & "TPREPAR=TAB.TPREPAR," & vbCrLf
sConsulta = sConsulta & "TUPLOAD=TAB.TUPLOAD," & vbCrLf
sConsulta = sConsulta & "TSERVER=TAB.TSERVER," & vbCrLf
sConsulta = sConsulta & "TDOWNLOAD=TAB.TDOWNLOAD ," & vbCrLf
sConsulta = sConsulta & "TRESPUESTA=TAB.TRESPUESTA," & vbCrLf
sConsulta = sConsulta & "TTOTAL=TAB.TTOTAL," & vbCrLf
sConsulta = sConsulta & "PAGINAORIGEN=TAB.PAGINAORIGEN," & vbCrLf
sConsulta = sConsulta & "USUARIO=TAB.USUARIO," & vbCrLf
sConsulta = sConsulta & "PROVE=TAB.PROVE," & vbCrLf
sConsulta = sConsulta & "IP=TAB.IP," & vbCrLf
sConsulta = sConsulta & "HTTP_USER_AGENT=TAB.HTTP_USER_AGENT" & vbCrLf
sConsulta = sConsulta & "FROM FSAL_ACCESOS_HIST FA INNER JOIN @TAB TAB" & vbCrLf
sConsulta = sConsulta & "ON " & vbCrLf
sConsulta = sConsulta & "TAB.PAGINA=FA.PAGINA AND " & vbCrLf
sConsulta = sConsulta & "TAB.FSAL_ENTORNO=FA.FSAL_ENTORNO AND " & vbCrLf
sConsulta = sConsulta & "TAB.PRODUCTO=FA.PRODUCTO AND " & vbCrLf
sConsulta = sConsulta & "TAB.FICLIPREPAR=FA.FICLIPREPAR AND " & vbCrLf
sConsulta = sConsulta & "TAB.FISERVER=FA.FISERVER AND " & vbCrLf
sConsulta = sConsulta & "TAB.FFSERVER=FA.FFSERVER AND " & vbCrLf
sConsulta = sConsulta & "TAB.FICLIRESP=FA.FICLIRESP AND " & vbCrLf
sConsulta = sConsulta & "TAB.FFCLIRESP=FA.FFCLIRESP AND " & vbCrLf
sConsulta = sConsulta & "TAB.TPREPAR=FA.TPREPAR AND " & vbCrLf
sConsulta = sConsulta & "TAB.TUPLOAD=FA.TUPLOAD AND " & vbCrLf
sConsulta = sConsulta & "TAB.TSERVER=FA.TSERVER AND " & vbCrLf
sConsulta = sConsulta & "TAB.TDOWNLOAD=FA.TDOWNLOAD AND " & vbCrLf
sConsulta = sConsulta & "TAB.TRESPUESTA=FA.TRESPUESTA AND " & vbCrLf
sConsulta = sConsulta & "TAB.TTOTAL=FA.TTOTAL AND " & vbCrLf
sConsulta = sConsulta & "TAB.PAGINAORIGEN=FA.PAGINAORIGEN AND " & vbCrLf
sConsulta = sConsulta & "TAB.USUARIO=FA.USUARIO AND " & vbCrLf
sConsulta = sConsulta & "TAB.PROVE=FA.PROVE AND " & vbCrLf
sConsulta = sConsulta & "TAB.IP=FA.IP AND " & vbCrLf
sConsulta = sConsulta & "TAB.HTTP_USER_AGENT=FA.HTTP_USER_AGENT  " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "insert into FSAL_ACCESOS_HIST(PAGINA,FSAL_ENTORNO,PRODUCTO,FICLIPREPAR,FFCLIPREPAR,FISERVER,FFSERVER,FICLIRESP,FFCLIRESP,TPREPAR,TUPLOAD,TSERVER,TDOWNLOAD,TRESPUESTA,TTOTAL,PAGINAORIGEN,USUARIO,PROVE,IP,HTTP_USER_AGENT)" & vbCrLf
sConsulta = sConsulta & "SELECT PAGINA,FSAL_ENTORNO,PRODUCTO,FICLIPREPAR,FFCLIPREPAR,FISERVER,FFSERVER,FICLIRESP,FFCLIRESP,TPREPAR,TUPLOAD,TSERVER,TDOWNLOAD,TRESPUESTA,TTOTAL,PAGINAORIGEN,USUARIO,PROVE,IP,HTTP_USER_AGENT FROM @TAB TAB" & vbCrLf
sConsulta = sConsulta & "WHERE NOT EXISTS(SELECT PAGINA,FSAL_ENTORNO,PRODUCTO,FICLIPREPAR,FFCLIPREPAR,FISERVER,FFSERVER,FICLIRESP,FFCLIRESP,TPREPAR,TUPLOAD,TSERVER,TDOWNLOAD,TRESPUESTA,TTOTAL,PAGINAORIGEN,USUARIO,PROVE,IP,HTTP_USER_AGENT FROM FSAL_ACCESOS_HIST" & vbCrLf
sConsulta = sConsulta & "               WHERE PAGINA=TAB.PAGINA AND FSAL_ENTORNO=TAB.FSAL_ENTORNO AND PRODUCTO=TAB.PRODUCTO AND FICLIPREPAR=TAB.FICLIPREPAR AND FFCLIPREPAR=TAB.FFCLIPREPAR AND FISERVER=TAB.FISERVER AND FFSERVER=TAB.FFSERVER AND FICLIRESP=TAB.FICLIRESP AND FFCLIRESP=TAB.FFCLIRESP AND TPREPAR=TAB.TPREPAR AND TUPLOAD=TAB.TUPLOAD AND TSERVER=TAB.TSERVER AND TDOWNLOAD=TAB.TDOWNLOAD AND TRESPUESTA=TAB.TRESPUESTA AND TTOTAL=TAB.TTOTAL AND PAGINAORIGEN=TAB.PAGINAORIGEN AND USUARIO=TAB.USUARIO AND PROVE=TAB.PROVE AND IP=TAB.IP AND HTTP_USER_AGENT=TAB.HTTP_USER_AGENT )" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR <> 0" & vbCrLf
sConsulta = sConsulta & "   SET @RES=1" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   SET @RES=0" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_CARGAR_ACCESOS_USUARIOS_ANYO_A_CORPORATE ]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_CARGAR_ACCESOS_USUARIOS_ANYO_A_CORPORATE ]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSAL_CARGAR_ACCESOS_USUARIOS_ANYO_A_CORPORATE] @TAB AS dbo.TABLA_FSAL_ACCESOS_ANYO READONLY, @RES INT OUTPUT AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE FSAL_USUARIOS_ANYO" & vbCrLf
sConsulta = sConsulta & "SET " & vbCrLf
sConsulta = sConsulta & "ANYO = TAB.ANYO," & vbCrLf
sConsulta = sConsulta & "ENTORNO=TAB.ENTORNO," & vbCrLf
sConsulta = sConsulta & "PYME=TAB.PYME," & vbCrLf
sConsulta = sConsulta & "ACTIVOS_GS=ISNULL(TAB.ACTIVOS_GS,'')," & vbCrLf
sConsulta = sConsulta & "ACTIVOS_WEB=ISNULL(TAB.ACTIVOS_WEB,''), " & vbCrLf
sConsulta = sConsulta & "ACTIVOS_PORTAL=ISNULL(TAB.ACTIVOS_PORTAL,''), " & vbCrLf
sConsulta = sConsulta & "TOTAL_GS=TAB.TOTAL_GS, " & vbCrLf
sConsulta = sConsulta & "TOTAL_WEB=TAB.TOTAL_WEB, " & vbCrLf
sConsulta = sConsulta & "TOTAL_PORTAL=TAB.TOTAL_PORTAL" & vbCrLf
sConsulta = sConsulta & "FROM FSAL_USUARIOS_ANYO FU INNER JOIN @TAB TAB" & vbCrLf
sConsulta = sConsulta & "ON" & vbCrLf
sConsulta = sConsulta & "TAB.ANYO=FU.ANYO AND" & vbCrLf
sConsulta = sConsulta & "TAB.ENTORNO=FU.ENTORNO AND" & vbCrLf
sConsulta = sConsulta & "TAB.PYME=FU.PYME AND" & vbCrLf
sConsulta = sConsulta & "ISNULL(TAB.ACTIVOS_GS,'')=ISNULL(FU.ACTIVOS_GS,'') AND" & vbCrLf
sConsulta = sConsulta & "ISNULL(TAB.ACTIVOS_WEB,'')=ISNULL(FU.ACTIVOS_WEB,'') AND " & vbCrLf
sConsulta = sConsulta & "ISNULL(TAB.ACTIVOS_PORTAL,'')=ISNULL(FU.ACTIVOS_PORTAL,'') AND " & vbCrLf
sConsulta = sConsulta & "TAB.TOTAL_GS=FU.TOTAL_GS AND " & vbCrLf
sConsulta = sConsulta & "TAB.TOTAL_WEB=FU.TOTAL_WEB AND " & vbCrLf
sConsulta = sConsulta & "TAB.TOTAL_PORTAL=FU.TOTAL_PORTAL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "insert into FSAL_USUARIOS_ANYO(ANYO,ENTORNO,PYME,ACTIVOS_GS, ACTIVOS_WEB, ACTIVOS_PORTAL, TOTAL_GS, TOTAL_WEB,TOTAL_PORTAL,FECACT,TRASPASO)" & vbCrLf
sConsulta = sConsulta & "SELECT ANYO,ENTORNO,PYME,ISNULL(ACTIVOS_GS,''),ISNULL(ACTIVOS_WEB,''),ISNULL(ACTIVOS_PORTAL,''),TOTAL_GS,TOTAL_WEB,TOTAL_PORTAL,FECACT,TRASPASO FROM @TAB TAB" & vbCrLf
sConsulta = sConsulta & "WHERE NOT EXISTS (SELECT ANYO, ENTORNO, PYME FROM FSAL_USUARIOS_ANYO WHERE ANYO=TAB.ANYO AND ENTORNO=TAB.ENTORNO AND PYME=TAB.PYME)" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR <> 0" & vbCrLf
sConsulta = sConsulta & "   SET @RES=1" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   SET @RES=0" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_CARGAR_ACCESOS_USUARIOS_DIA_A_CORPORATE]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_CARGAR_ACCESOS_USUARIOS_DIA_A_CORPORATE]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSAL_CARGAR_ACCESOS_USUARIOS_DIA_A_CORPORATE] @TAB AS dbo.TABLA_FSAL_ACCESOS_DIA READONLY, @RES INT OUTPUT AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE FSAL_USUARIOS_DIA " & vbCrLf
sConsulta = sConsulta & "set " & vbCrLf
sConsulta = sConsulta & "FECHA=TAB.FECHA ," & vbCrLf
sConsulta = sConsulta & "ANYO=TAB.ANYO," & vbCrLf
sConsulta = sConsulta & "MES=TAB.MES," & vbCrLf
sConsulta = sConsulta & "DIA=TAB.DIA  ," & vbCrLf
sConsulta = sConsulta & "DIA_SEMANA=TAB.DIA_SEMANA," & vbCrLf
sConsulta = sConsulta & "ENTORNO=TAB.ENTORNO  ," & vbCrLf
sConsulta = sConsulta & "PYME=TAB.PYME," & vbCrLf
sConsulta = sConsulta & "ACTIVOS_GS=TAB.ACTIVOS_GS ," & vbCrLf
sConsulta = sConsulta & "ACTIVOS_WEB=TAB.ACTIVOS_WEB," & vbCrLf
sConsulta = sConsulta & "ACTIVOS_PORTAL=TAB.ACTIVOS_PORTAL ," & vbCrLf
sConsulta = sConsulta & "TOTAL_GS=TAB.TOTAL_GS," & vbCrLf
sConsulta = sConsulta & "TOTAL_WEB=TAB.TOTAL_WEB," & vbCrLf
sConsulta = sConsulta & "TOTAL_PORTAL=TAB.TOTAL_PORTAL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "FROM FSAL_USUARIOS_DIA FU INNER JOIN @TAB TAB" & vbCrLf
sConsulta = sConsulta & "ON " & vbCrLf
sConsulta = sConsulta & "TAB.ANYO=FU.ANYO AND" & vbCrLf
sConsulta = sConsulta & "TAB.MES=FU.MES AND " & vbCrLf
sConsulta = sConsulta & "TAB.DIA=FU.DIA AND " & vbCrLf
sConsulta = sConsulta & "TAB.DIA_SEMANA=FU.DIA_SEMANA AND" & vbCrLf
sConsulta = sConsulta & "TAB.ENTORNO=FU.ENTORNO AND " & vbCrLf
sConsulta = sConsulta & "TAB.PYME=FU.PYME AND" & vbCrLf
sConsulta = sConsulta & "TAB.ACTIVOS_GS=FU.ACTIVOS_GS AND " & vbCrLf
sConsulta = sConsulta & "TAB.ACTIVOS_WEB=FU.ACTIVOS_WEB AND" & vbCrLf
sConsulta = sConsulta & "TAB.ACTIVOS_PORTAL=FU.ACTIVOS_PORTAL AND " & vbCrLf
sConsulta = sConsulta & "TAB.TOTAL_GS=FU.TOTAL_GS AND " & vbCrLf
sConsulta = sConsulta & "TAB.TOTAL_WEB=FU.TOTAL_WEB AND " & vbCrLf
sConsulta = sConsulta & "TAB.TOTAL_PORTAL=FU.TOTAL_PORTAL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "insert into FSAL_USUARIOS_DIA(FECHA,ANYO,MES,DIA,DIA_SEMANA,ENTORNO,PYME,ACTIVOS_GS,ACTIVOS_WEB,ACTIVOS_PORTAL,TOTAL_GS, TOTAL_WEB,TOTAL_PORTAL,FECACT,TRASPASO)" & vbCrLf
sConsulta = sConsulta & "SELECT FECHA,ANYO,MES,DIA,DIA_SEMANA,ENTORNO,PYME,ACTIVOS_GS,ACTIVOS_WEB,ACTIVOS_PORTAL,TOTAL_GS,TOTAL_WEB,TOTAL_PORTAL,FECACT,TRASPASO FROM @TAB TAB" & vbCrLf
sConsulta = sConsulta & "WHERE NOT EXISTS (SELECT FECHA,ANYO,MES,DIA, ENTORNO, PYME FROM FSAL_USUARIOS_DIA WHERE FECHA=TAB.FECHA AND ANYO=TAB.ANYO AND MES=TAB.MES AND DIA=TAB.DIA)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR <> 0" & vbCrLf
sConsulta = sConsulta & "   SET @RES=1" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   SET @RES=0" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_CARGAR_ACCESOS_USUARIOS_DIS_ANYO_A_CORPORATE]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_CARGAR_ACCESOS_USUARIOS_DIS_ANYO_A_CORPORATE]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSAL_CARGAR_ACCESOS_USUARIOS_DIS_ANYO_A_CORPORATE] @TAB AS dbo.TABLA_FSAL_ACCESOS_DIS_ANYO READONLY, @RES INT OUTPUT AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "UPDATE FSAL_USUARIOS_DIS_ANYO" & vbCrLf
sConsulta = sConsulta & "SET " & vbCrLf
sConsulta = sConsulta & "ANYO=TAB.ANYO," & vbCrLf
sConsulta = sConsulta & "ENTORNO=TAB.ENTORNO," & vbCrLf
sConsulta = sConsulta & "PYME=TAB.PYME, " & vbCrLf
sConsulta = sConsulta & "PRODUCTO=TAB.PRODUCTO," & vbCrLf
sConsulta = sConsulta & "USU=TAB.USU" & vbCrLf
sConsulta = sConsulta & "FROM FSAL_USUARIOS_DIS_ANYO FU INNER JOIN @TAB TAB" & vbCrLf
sConsulta = sConsulta & "ON" & vbCrLf
sConsulta = sConsulta & "TAB.ANYO=FU.ANYO AND" & vbCrLf
sConsulta = sConsulta & "TAB.ENTORNO=FU.ENTORNO AND " & vbCrLf
sConsulta = sConsulta & "TAB.PYME=FU.PYME AND " & vbCrLf
sConsulta = sConsulta & "TAB.PRODUCTO=FU.PRODUCTO AND" & vbCrLf
sConsulta = sConsulta & "TAB.USU=FU.USU" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "insert into FSAL_USUARIOS_DIS_ANYO(ANYO,ENTORNO,PYME,PRODUCTO, USU, TRASPASO)" & vbCrLf
sConsulta = sConsulta & "SELECT ANYO,ENTORNO,PYME,PRODUCTO,USU,TRASPASO FROM @TAB TAB" & vbCrLf
sConsulta = sConsulta & "WHERE NOT EXISTS(SELECT ANYO, ENTORNO, PYME FROM FSAL_USUARIOS_DIS_ANYO WHERE ANYO=TAB.ANYO AND ENTORNO=TAB.ENTORNO AND PYME=TAB.PYME)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR <> 0" & vbCrLf
sConsulta = sConsulta & "   SET @RES=1" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   SET @RES=0" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_CARGAR_ACCESOS_USUARIOS_DIS_MES_A_CORPORATE]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_CARGAR_ACCESOS_USUARIOS_DIS_MES_A_CORPORATE]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSAL_CARGAR_ACCESOS_USUARIOS_DIS_MES_A_CORPORATE] @TAB AS dbo.TABLA_FSAL_ACCESOS_DIS_MES READONLY, @RES INT OUTPUT AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE FSAL_USUARIOS_DIS_MES" & vbCrLf
sConsulta = sConsulta & "SET " & vbCrLf
sConsulta = sConsulta & "FECHA=TAB.FECHA," & vbCrLf
sConsulta = sConsulta & "ANYO=TAB.ANYO," & vbCrLf
sConsulta = sConsulta & "MES=TAB.MES," & vbCrLf
sConsulta = sConsulta & "ENTORNO=TAB.ENTORNO," & vbCrLf
sConsulta = sConsulta & "PYME=TAB.PYME," & vbCrLf
sConsulta = sConsulta & "PRODUCTO=TAB.PRODUCTO," & vbCrLf
sConsulta = sConsulta & "USU=TAB.USU" & vbCrLf
sConsulta = sConsulta & "FROM FSAL_USUARIOS_DIS_MES FU INNER JOIN @TAB TAB" & vbCrLf
sConsulta = sConsulta & "ON " & vbCrLf
sConsulta = sConsulta & "TAB.ANYO=FU.ANYO AND" & vbCrLf
sConsulta = sConsulta & "TAB.MES=FU.MES AND" & vbCrLf
sConsulta = sConsulta & "TAB.ENTORNO=FU.ENTORNO AND " & vbCrLf
sConsulta = sConsulta & "TAB.PYME=FU.PYME AND " & vbCrLf
sConsulta = sConsulta & "TAB.PRODUCTO=FU.PRODUCTO AND " & vbCrLf
sConsulta = sConsulta & "TAB.USU=FU.USU" & vbCrLf
sConsulta = sConsulta & "  " & vbCrLf
sConsulta = sConsulta & "insert into FSAL_USUARIOS_DIS_MES(FECHA,ANYO,MES,ENTORNO,PYME,PRODUCTO, USU, TRASPASO)" & vbCrLf
sConsulta = sConsulta & "SELECT FECHA,ANYO,MES,ENTORNO,PYME,PRODUCTO,USU,TRASPASO FROM @TAB TAB" & vbCrLf
sConsulta = sConsulta & "WHERE NOT EXISTS(SELECT ANYO, MES ,ENTORNO, PYME FROM FSAL_USUARIOS_DIS_MES WHERE ANYO=TAB.ANYO AND MES=TAB.MES AND ENTORNO=TAB.ENTORNO AND PYME=TAB.PYME)" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR <> 0" & vbCrLf
sConsulta = sConsulta & "   SET @RES=1" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   SET @RES=0" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 
sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_CARGAR_ACCESOS_USUARIOS_DIS_SEMANA_A_CORPORATE]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_CARGAR_ACCESOS_USUARIOS_DIS_SEMANA_A_CORPORATE]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSAL_CARGAR_ACCESOS_USUARIOS_DIS_SEMANA_A_CORPORATE] @TAB AS dbo.TABLA_FSAL_ACCESOS_DIS_SEMANA READONLY, @RES INT OUTPUT AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE FSAL_USUARIOS_DIS_SEMANA" & vbCrLf
sConsulta = sConsulta & "SET " & vbCrLf
sConsulta = sConsulta & "FECHA=TAB.FECHA," & vbCrLf
sConsulta = sConsulta & "ANYO=TAB.ANYO," & vbCrLf
sConsulta = sConsulta & "MES=TAB.MES," & vbCrLf
sConsulta = sConsulta & "DIA=TAB.DIA," & vbCrLf
sConsulta = sConsulta & "ENTORNO=TAB.ENTORNO," & vbCrLf
sConsulta = sConsulta & "PYME=TAB.PYME," & vbCrLf
sConsulta = sConsulta & "PRODUCTO=TAB.PRODUCTO," & vbCrLf
sConsulta = sConsulta & "USU=TAB.USU" & vbCrLf
sConsulta = sConsulta & "FROM FSAL_USUARIOS_DIS_SEMANA FU INNER JOIN @TAB TAB" & vbCrLf
sConsulta = sConsulta & "ON " & vbCrLf
sConsulta = sConsulta & "TAB.ANYO=FU.ANYO AND" & vbCrLf
sConsulta = sConsulta & "TAB.MES=FU.MES AND " & vbCrLf
sConsulta = sConsulta & "TAB.DIA=FU.DIA AND " & vbCrLf
sConsulta = sConsulta & "TAB.ENTORNO=FU.ENTORNO AND" & vbCrLf
sConsulta = sConsulta & "TAB.PYME=FU.PYME AND" & vbCrLf
sConsulta = sConsulta & "TAB.PRODUCTO=FU.PRODUCTO AND" & vbCrLf
sConsulta = sConsulta & "TAB.USU=FU.USU" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  " & vbCrLf
sConsulta = sConsulta & "insert into FSAL_USUARIOS_DIS_SEMANA(FECHA,ANYO,MES,DIA,ENTORNO,PYME,PRODUCTO, USU, TRASPASO)" & vbCrLf
sConsulta = sConsulta & "SELECT FECHA,ANYO,MES,DIA,ENTORNO,PYME,PRODUCTO,USU,TRASPASO FROM @TAB TAB" & vbCrLf
sConsulta = sConsulta & "WHERE NOT EXISTS(SELECT ANYO, MES, DIA, ENTORNO, PYME FROM FSAL_USUARIOS_SEMANA WHERE ANYO=TAB.ANYO AND MES = TAB.MES AND DIA= TAB.DIA AND ENTORNO = TAB.ENTORNO AND PYME = TAB.PYME)" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR <> 0" & vbCrLf
sConsulta = sConsulta & "   SET @RES=1" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   SET @RES=0" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_CARGAR_ACCESOS_USUARIOS_HORA_A_CORPORATE]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_CARGAR_ACCESOS_USUARIOS_HORA_A_CORPORATE]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSAL_CARGAR_ACCESOS_USUARIOS_HORA_A_CORPORATE] @TAB AS dbo.TABLA_FSAL_ACCESOS_HORA READONLY, @RES INT OUTPUT AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE FSAL_USUARIOS_HORA" & vbCrLf
sConsulta = sConsulta & "SET " & vbCrLf
sConsulta = sConsulta & "FECHA=TAB.FECHA," & vbCrLf
sConsulta = sConsulta & "ANYO=TAB.ANYO," & vbCrLf
sConsulta = sConsulta & "MES=TAB.MES," & vbCrLf
sConsulta = sConsulta & "DIA=TAB.DIA," & vbCrLf
sConsulta = sConsulta & "HORA=TAB.HORA," & vbCrLf
sConsulta = sConsulta & "ENTORNO=TAB.ENTORNO," & vbCrLf
sConsulta = sConsulta & "PYME=TAB.PYME," & vbCrLf
sConsulta = sConsulta & "ACTIVOS_GS=TAB.ACTIVOS_GS," & vbCrLf
sConsulta = sConsulta & "ACTIVOS_WEB=TAB.ACTIVOS_WEB," & vbCrLf
sConsulta = sConsulta & "ACTIVOS_PORTAL=TAB.ACTIVOS_PORTAL," & vbCrLf
sConsulta = sConsulta & "TOTAL_GS=TAB.TOTAL_GS," & vbCrLf
sConsulta = sConsulta & "TOTAL_WEB=TAB.TOTAL_WEB," & vbCrLf
sConsulta = sConsulta & "TOTAL_PORTAL=TAB.TOTAL_PORTAL" & vbCrLf
sConsulta = sConsulta & "FROM FSAL_USUARIOS_HORA FU INNER JOIN @TAB TAB" & vbCrLf
sConsulta = sConsulta & "ON" & vbCrLf
sConsulta = sConsulta & "TAB.ANYO=FU.ANYO AND" & vbCrLf
sConsulta = sConsulta & "TAB.MES=FU.MES AND " & vbCrLf
sConsulta = sConsulta & "TAB.DIA=FU.DIA AND" & vbCrLf
sConsulta = sConsulta & "TAB.HORA=FU.HORA AND " & vbCrLf
sConsulta = sConsulta & "TAB.ENTORNO=FU.ENTORNO AND " & vbCrLf
sConsulta = sConsulta & "TAB.PYME=FU.PYME AND " & vbCrLf
sConsulta = sConsulta & "TAB.ACTIVOS_GS=FU.ACTIVOS_GS AND " & vbCrLf
sConsulta = sConsulta & "TAB.ACTIVOS_WEB=FU.ACTIVOS_WEB AND " & vbCrLf
sConsulta = sConsulta & "TAB.ACTIVOS_PORTAL=FU.ACTIVOS_PORTAL AND " & vbCrLf
sConsulta = sConsulta & "TAB.TOTAL_GS=FU.TOTAL_GS AND " & vbCrLf
sConsulta = sConsulta & "TAB.TOTAL_WEB=FU.TOTAL_WEB AND " & vbCrLf
sConsulta = sConsulta & "TAB.TOTAL_PORTAL=FU.TOTAL_PORTAL" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "insert into FSAL_USUARIOS_HORA(FECHA,ANYO,MES,DIA,HORA,ENTORNO,PYME,ACTIVOS_GS,ACTIVOS_WEB,ACTIVOS_PORTAL,TOTAL_GS, TOTAL_WEB,TOTAL_PORTAL,FECACT,TRASPASO)" & vbCrLf
sConsulta = sConsulta & "SELECT FECHA,ANYO,MES,DIA,HORA,ENTORNO,PYME,ACTIVOS_GS,ACTIVOS_WEB,ACTIVOS_PORTAL,TOTAL_GS,TOTAL_WEB,TOTAL_PORTAL,FECACT,TRASPASO FROM @TAB TAB" & vbCrLf
sConsulta = sConsulta & "WHERE NOT EXISTS(SELECT FECHA,ANYO,MES,DIA,HORA,ENTORNO,PYME FROM FSAL_USUARIOS_HORA WHERE FECHA=TAB.FECHA AND ANYO=TAB.ANYO AND MES=TAB.MES AND DIA=TAB.DIA AND HORA= TAB.HORA AND ENTORNO=TAB.ENTORNO AND PYME=TAB.PYME)" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR <> 0" & vbCrLf
sConsulta = sConsulta & "   SET @RES=1" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   SET @RES=0" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_CARGAR_ACCESOS_USUARIOS_MES_A_CORPORATE]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_CARGAR_ACCESOS_USUARIOS_MES_A_CORPORATE]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSAL_CARGAR_ACCESOS_USUARIOS_MES_A_CORPORATE] @TAB AS dbo.TABLA_FSAL_ACCESOS_MES READONLY, @RES INT OUTPUT AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE FSAL_USUARIOS_MES" & vbCrLf
sConsulta = sConsulta & "SET" & vbCrLf
sConsulta = sConsulta & "FECHA=TAB.FECHA," & vbCrLf
sConsulta = sConsulta & "ANYO=TAB.ANYO," & vbCrLf
sConsulta = sConsulta & "MES=TAB.MES," & vbCrLf
sConsulta = sConsulta & "ENTORNO=TAB.ENTORNO," & vbCrLf
sConsulta = sConsulta & "PYME=TAB.PYME," & vbCrLf
sConsulta = sConsulta & "ACTIVOS_GS=TAB.ACTIVOS_GS," & vbCrLf
sConsulta = sConsulta & "ACTIVOS_WEB=TAB.ACTIVOS_WEB," & vbCrLf
sConsulta = sConsulta & "ACTIVOS_PORTAL=TAB.ACTIVOS_PORTAL," & vbCrLf
sConsulta = sConsulta & "TOTAL_GS=TAB.TOTAL_GS," & vbCrLf
sConsulta = sConsulta & "TOTAL_WEB=TAB.TOTAL_WEB," & vbCrLf
sConsulta = sConsulta & "TOTAL_PORTAL=TAB.TOTAL_PORTAL" & vbCrLf
sConsulta = sConsulta & "FROM FSAL_USUARIOS_MES FU INNER JOIN @TAB TAB" & vbCrLf
sConsulta = sConsulta & "ON" & vbCrLf
sConsulta = sConsulta & "TAB.ANYO=FU.ANYO AND " & vbCrLf
sConsulta = sConsulta & "TAB.MES=FU.MES AND " & vbCrLf
sConsulta = sConsulta & "TAB.ENTORNO=FU.ENTORNO AND " & vbCrLf
sConsulta = sConsulta & "TAB.PYME=FU.PYME AND " & vbCrLf
sConsulta = sConsulta & "TAB.ACTIVOS_GS=FU.ACTIVOS_GS AND " & vbCrLf
sConsulta = sConsulta & "TAB.ACTIVOS_WEB=FU.ACTIVOS_WEB AND " & vbCrLf
sConsulta = sConsulta & "TAB.ACTIVOS_PORTAL=FU.ACTIVOS_PORTAL AND " & vbCrLf
sConsulta = sConsulta & "TAB.TOTAL_GS=FU.TOTAL_GS AND " & vbCrLf
sConsulta = sConsulta & "TAB.TOTAL_WEB=FU.TOTAL_WEB AND " & vbCrLf
sConsulta = sConsulta & "TAB.TOTAL_PORTAL=FU.TOTAL_PORTAL      " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "insert into FSAL_USUARIOS_MES(FECHA,ANYO,MES,ENTORNO,PYME,ACTIVOS_GS, ACTIVOS_WEB, ACTIVOS_PORTAL, TOTAL_GS, TOTAL_WEB,TOTAL_PORTAL,FECACT,TRASPASO)" & vbCrLf
sConsulta = sConsulta & "SELECT FECHA,ANYO,MES,ENTORNO,PYME,ACTIVOS_GS,ACTIVOS_WEB,ACTIVOS_PORTAL,TOTAL_GS,TOTAL_WEB,TOTAL_PORTAL,FECACT,TRASPASO FROM @TAB TAB" & vbCrLf
sConsulta = sConsulta & "WHERE NOT EXISTS(SELECT FECHA,ANYO,MES,ENTORNO,PYME FROM FSAL_USUARIOS_MES WHERE FECHA=TAB.FECHA AND ANYO=TAB.ANYO AND MES=TAB.MES AND ENTORNO=TAB.ENTORNO AND PYME=TAB.PYME)" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR <> 0" & vbCrLf
sConsulta = sConsulta & "   SET @RES=1" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   SET @RES=0" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_CARGAR_ACCESOS_USUARIOS_MINUTO_A_CORPORATE]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_CARGAR_ACCESOS_USUARIOS_MINUTO_A_CORPORATE]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSAL_CARGAR_ACCESOS_USUARIOS_MINUTO_A_CORPORATE] @TAB AS dbo.TABLA_FSAL_ACCESOS_MINUTO READONLY, @RES INT OUTPUT AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE FSAL_USUARIOS_MINUTO" & vbCrLf
sConsulta = sConsulta & "SET" & vbCrLf
sConsulta = sConsulta & "FECHA=TAB.FECHA," & vbCrLf
sConsulta = sConsulta & "ANYO=TAB.ANYO," & vbCrLf
sConsulta = sConsulta & "MES=TAB.MES," & vbCrLf
sConsulta = sConsulta & "DIA=TAB.DIA," & vbCrLf
sConsulta = sConsulta & "HORA=TAB.HORA," & vbCrLf
sConsulta = sConsulta & "MIN=TAB.MIN," & vbCrLf
sConsulta = sConsulta & "ENTORNO=TAB.ENTORNO," & vbCrLf
sConsulta = sConsulta & "PYME=TAB.PYME," & vbCrLf
sConsulta = sConsulta & "ACTIVOS_GS=TAB.ACTIVOS_GS," & vbCrLf
sConsulta = sConsulta & "ACTIVOS_WEB=TAB.ACTIVOS_WEB," & vbCrLf
sConsulta = sConsulta & "ACTIVOS_PORTAL=TAB.ACTIVOS_PORTAL" & vbCrLf
sConsulta = sConsulta & "FROM FSAL_USUARIOS_MINUTO FU INNER JOIN @TAB TAB" & vbCrLf
sConsulta = sConsulta & "ON" & vbCrLf
sConsulta = sConsulta & "TAB.ANYO=FU.ANYO AND " & vbCrLf
sConsulta = sConsulta & "TAB.MES=FU.MES AND " & vbCrLf
sConsulta = sConsulta & "TAB.DIA=FU.DIA AND " & vbCrLf
sConsulta = sConsulta & "TAB.HORA=FU.HORA AND " & vbCrLf
sConsulta = sConsulta & "TAB.MIN=FU.MIN AND " & vbCrLf
sConsulta = sConsulta & "TAB.ENTORNO=FU.ENTORNO AND " & vbCrLf
sConsulta = sConsulta & "TAB.PYME=FU.PYME AND " & vbCrLf
sConsulta = sConsulta & "TAB.ACTIVOS_GS=FU.ACTIVOS_GS AND " & vbCrLf
sConsulta = sConsulta & "TAB.ACTIVOS_WEB=FU.ACTIVOS_WEB AND " & vbCrLf
sConsulta = sConsulta & "TAB.ACTIVOS_PORTAL=FU.ACTIVOS_PORTAL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "insert into FSAL_USUARIOS_MINUTO(FECHA,ANYO,MES,DIA,HORA,MIN,ENTORNO,PYME,ACTIVOS_GS,ACTIVOS_WEB,ACTIVOS_PORTAL,FECACT,TRASPASO)" & vbCrLf
sConsulta = sConsulta & "SELECT FECHA,ANYO,MES,DIA,HORA,MIN,ENTORNO,PYME,ACTIVOS_GS,ACTIVOS_WEB,ACTIVOS_PORTAL,FECACT,TRASPASO FROM @TAB TAB" & vbCrLf
sConsulta = sConsulta & "WHERE NOT EXISTS(SELECT ANYO,MES,DIA,HORA,MIN,ENTORNO,PYME FROM FSAL_USUARIOS_MINUTO WHERE ANYO=TAB.ANYO AND MES=TAB.MES AND DIA=TAB.DIA AND HORA=TAB.HORA AND MIN=TAB.MIN AND ENTORNO=TAB.ENTORNO AND PYME=TAB.PYME)" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR <> 0" & vbCrLf
sConsulta = sConsulta & "   SET @RES=1" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   SET @RES=0" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_CARGAR_ACCESOS_USUARIOS_SEMANA_A_CORPORATE]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_CARGAR_ACCESOS_USUARIOS_SEMANA_A_CORPORATE]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSAL_CARGAR_ACCESOS_USUARIOS_SEMANA_A_CORPORATE] @TAB AS dbo.TABLA_FSAL_ACCESOS_SEMANA READONLY, @RES INT OUTPUT AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE FSAL_USUARIOS_SEMANA" & vbCrLf
sConsulta = sConsulta & "SET " & vbCrLf
sConsulta = sConsulta & "FECHA=TAB.FECHA," & vbCrLf
sConsulta = sConsulta & "ANYO=TAB.ANYO," & vbCrLf
sConsulta = sConsulta & "MES=TAB.MES," & vbCrLf
sConsulta = sConsulta & "DIA=TAB.DIA," & vbCrLf
sConsulta = sConsulta & "ENTORNO=TAB.ENTORNO," & vbCrLf
sConsulta = sConsulta & "PYME=TAB.PYME," & vbCrLf
sConsulta = sConsulta & "TOTAL_GS=TAB.TOTAL_GS," & vbCrLf
sConsulta = sConsulta & "TOTAL_WEB=TAB.TOTAL_WEB," & vbCrLf
sConsulta = sConsulta & "TOTAL_PORTAL=TAB.TOTAL_PORTAL" & vbCrLf
sConsulta = sConsulta & "FROM FSAL_USUARIOS_SEMANA FU INNER JOIN @TAB TAB" & vbCrLf
sConsulta = sConsulta & "ON " & vbCrLf
sConsulta = sConsulta & "TAB.ANYO=FU.ANYO AND " & vbCrLf
sConsulta = sConsulta & "TAB.MES=FU.MES AND" & vbCrLf
sConsulta = sConsulta & "TAB.DIA=FU.DIA AND" & vbCrLf
sConsulta = sConsulta & "TAB.ENTORNO=FU.ENTORNO AND" & vbCrLf
sConsulta = sConsulta & "TAB.PYME=FU.PYME AND " & vbCrLf
sConsulta = sConsulta & "TAB.TOTAL_GS=FU.TOTAL_GS AND " & vbCrLf
sConsulta = sConsulta & "TAB.TOTAL_WEB=FU.TOTAL_WEB AND " & vbCrLf
sConsulta = sConsulta & "TAB.TOTAL_PORTAL=FU.TOTAL_PORTAL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "insert into FSAL_USUARIOS_SEMANA(FECHA,ANYO,MES,DIA,ENTORNO,PYME,TOTAL_GS, TOTAL_WEB,TOTAL_PORTAL,FECACT,TRASPASO)" & vbCrLf
sConsulta = sConsulta & "SELECT FECHA,ANYO,MES,DIA,ENTORNO,PYME,TOTAL_GS,TOTAL_WEB,TOTAL_PORTAL,FECACT,TRASPASO FROM @TAB TAB" & vbCrLf
sConsulta = sConsulta & "WHERE NOT EXISTS(SELECT ANYO,MES,DIA,ENTORNO,PYME FROM FSAL_USUARIOS_SEMANA WHERE ANYO=TAB.ANYO AND MES=TAB.MES AND DIA=TAB.DIA AND ENTORNO=TAB.ENTORNO AND PYME=TAB.PYME)" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR <> 0" & vbCrLf
sConsulta = sConsulta & "   SET @RES=1" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   SET @RES=0" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_CARGAR_EVENTOS_A_CORPORATE]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_CARGAR_EVENTOS_A_CORPORATE]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSAL_CARGAR_EVENTOS_A_CORPORATE] @TAB AS dbo.TABLA_FSAL_EVENTOS READONLY, @RES INT OUTPUT AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE FSAL_EVENTOS" & vbCrLf
sConsulta = sConsulta & "SET " & vbCrLf
sConsulta = sConsulta & "FSAL_ENTORNO=TAB.FSAL_ENTORNO," & vbCrLf
sConsulta = sConsulta & "FECHA_EVENTO=TAB.FECHA_EVENTO," & vbCrLf
sConsulta = sConsulta & "TIPO_EVENTO=TAB.TIPO_EVENTO," & vbCrLf
sConsulta = sConsulta & "DATA=TAB.DATA," & vbCrLf
sConsulta = sConsulta & "FSAL_PYME=TAB.FSAL_PYME" & vbCrLf
sConsulta = sConsulta & "FROM FSAL_EVENTOS FE INNER JOIN @TAB TAB" & vbCrLf
sConsulta = sConsulta & "ON " & vbCrLf
sConsulta = sConsulta & "TAB.FSAL_ENTORNO= FE.FSAL_ENTORNO AND" & vbCrLf
sConsulta = sConsulta & "TAB.FECHA_EVENTO=FE.FECHA_EVENTO AND " & vbCrLf
sConsulta = sConsulta & "TAB.TIPO_EVENTO=FE.TIPO_EVENTO AND " & vbCrLf
sConsulta = sConsulta & "TAB.DATA=FE.DATA AND " & vbCrLf
sConsulta = sConsulta & "TAB.FSAL_PYME=FE.FSAL_PYME" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "insert into FSAL_EVENTOS(FSAL_ENTORNO,FECHA_EVENTO,TIPO_EVENTO,DATA,FSAL_PYME,TRASPASO)" & vbCrLf
sConsulta = sConsulta & "SELECT FSAL_ENTORNO,FECHA_EVENTO,TIPO_EVENTO,DATA,FSAL_PYME,TRASPASO FROM @TAB TAB" & vbCrLf
sConsulta = sConsulta & "WHERE NOT EXISTS(SELECT FSAL_ENTORNO,FECHA_EVENTO,TIPO_EVENTO,DATA,FSAL_PYME FROM FSAL_EVENTOS WHERE FSAL_ENTORNO=TAB.FSAL_ENTORNO AND FECHA_EVENTO=TAB.FECHA_EVENTO AND TIPO_EVENTO=TAB.TIPO_EVENTO AND DATA=TAB.DATA AND FSAL_PYME=TAB.FSAL_PYME)" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR <> 0" & vbCrLf
sConsulta = sConsulta & "   SET @RES=1" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   SET @RES=0" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_CARGAR_EVENTOS_HIST_A_CORPORATE]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_CARGAR_EVENTOS_HIST_A_CORPORATE]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta


sConsulta = "CREATE PROCEDURE [dbo].[FSAL_CARGAR_EVENTOS_HIST_A_CORPORATE] @TAB AS dbo.TABLA_FSAL_EVENTOS_HIST READONLY, @RES INT OUTPUT AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE FSAL_EVENTOS_HIST" & vbCrLf
sConsulta = sConsulta & "SET" & vbCrLf
sConsulta = sConsulta & "FECHA_EVENTO=TAB.FECHA_EVENTO," & vbCrLf
sConsulta = sConsulta & "ANYO=TAB.ANYO," & vbCrLf
sConsulta = sConsulta & "MES=TAB.MES," & vbCrLf
sConsulta = sConsulta & "DIA=TAB.DIA," & vbCrLf
sConsulta = sConsulta & "HORA=TAB.HORA," & vbCrLf
sConsulta = sConsulta & "MIN=TAB.MIN," & vbCrLf
sConsulta = sConsulta & "NUM_EVENTOS=TAB.NUM_EVENTOS," & vbCrLf
sConsulta = sConsulta & "TIPO_EVENTO=TAB.TIPO_EVENTO," & vbCrLf
sConsulta = sConsulta & "FSAL_ENTORNO=TAB.FSAL_ENTORNO," & vbCrLf
sConsulta = sConsulta & "FSAL_PYME=TAB.FSAL_PYME" & vbCrLf
sConsulta = sConsulta & "FROM FSAL_EVENTOS_HIST FE INNER JOIN @TAB TAB" & vbCrLf
sConsulta = sConsulta & "ON" & vbCrLf
sConsulta = sConsulta & "TAB.ANYO=FE.ANYO AND" & vbCrLf
sConsulta = sConsulta & "TAB.MES=FE.MES AND" & vbCrLf
sConsulta = sConsulta & "TAB.DIA=FE.DIA AND " & vbCrLf
sConsulta = sConsulta & "TAB.HORA=FE.HORA AND " & vbCrLf
sConsulta = sConsulta & "TAB.MIN=FE.MIN AND" & vbCrLf
sConsulta = sConsulta & "TAB.NUM_EVENTOS=FE.NUM_EVENTOS AND" & vbCrLf
sConsulta = sConsulta & "TAB.TIPO_EVENTO=FE.TIPO_EVENTO AND" & vbCrLf
sConsulta = sConsulta & "TAB.FSAL_ENTORNO=FE.FSAL_ENTORNO AND" & vbCrLf
sConsulta = sConsulta & "TAB.FSAL_PYME=FE.FSAL_PYME" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "insert into FSAL_EVENTOS_HIST(FECHA_EVENTO,ANYO,MES,DIA,HORA,MIN,NUM_EVENTOS,TIPO_EVENTO,FSAL_ENTORNO,FSAL_PYME)" & vbCrLf
sConsulta = sConsulta & "SELECT FECHA_EVENTO,ANYO,MES,DIA,HORA,MIN,NUM_EVENTOS,TIPO_EVENTO,FSAL_ENTORNO,FSAL_PYME FROM @TAB TAB" & vbCrLf
sConsulta = sConsulta & "WHERE NOT EXISTS(SELECT ANYO,MES,DIA,HORA,MIN,FSAL_ENTORNO,FSAL_PYME FROM FSAL_EVENTOS_HIST WHERE ANYO=TAB.ANYO AND MES=TAB.MES AND DIA=TAB.DIA AND HORA=TAB.HORA AND MIN=TAB.MIN AND FSAL_ENTORNO=TAB.FSAL_ENTORNO AND FSAL_PYME=TAB.FSAL_PYME) " & vbCrLf
sConsulta = sConsulta & "IF @@ERROR <> 0" & vbCrLf
sConsulta = sConsulta & "   SET @RES=1" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   SET @RES=0" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta






End Sub


Public Sub V_31900_8_Storeds_088_1()
    Dim sConsulta As String
    
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_ACCESOS_USUARIOS_POR_DIA_MES]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_ACCESOS_USUARIOS_POR_DIA_MES]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSAL_ACCESOS_USUARIOS_POR_DIA_MES]  " & vbCrLf
sConsulta = sConsulta & "   @fecdesde DATETIME," & vbCrLf
sConsulta = sConsulta & "   @fechasta DATETIME," & vbCrLf
sConsulta = sConsulta & "   @entorno nvarchar(50)," & vbCrLf
sConsulta = sConsulta & "   @pyme nvarchar(50)=null" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @fechacomienzo DATETIME" & vbCrLf
sConsulta = sConsulta & "DECLARE @MEDIA FLOAT" & vbCrLf
sConsulta = sConsulta & "declare @numDiaSemanaHoras float" & vbCrLf
sConsulta = sConsulta & "SET @fechacomienzo=CONVERT(datetime,'2010-01-01')" & vbCrLf
sConsulta = sConsulta & "set @numDiaSemanaHoras = DATEDIFF(""WW"",@fechacomienzo,GETDATE());" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "with mycte as" & vbCrLf
sConsulta = sConsulta & "(" & vbCrLf
sConsulta = sConsulta & "    SELECT @fecdesde  AS MyDay" & vbCrLf
sConsulta = sConsulta & "    UNION ALL" & vbCrLf
sConsulta = sConsulta & "    SELECT dateadd(DAY ,1,MyDay)" & vbCrLf
sConsulta = sConsulta & "    FROM mycte " & vbCrLf
sConsulta = sConsulta & "    WHERE dateadd(DAY ,1,MyDay) <= @fechasta" & vbCrLf
sConsulta = sConsulta & ")" & vbCrLf
sConsulta = sConsulta & "SELECT ACCESOS.FECHA, ISNULL(ACTIVOS_GS,0) AS ACTIVOS_GS, ISNULL(ACTIVOS_WEB,0) AS ACTIVOS_WEB, ISNULL(ACTIVOS_PORTAL,0) AS ACTIVOS_PORTAL, ISNULL(AVG_GS,0) AS AVG_GS, ISNULL(AVG_WEB,0) AS AVG_WEB, ISNULL(AVG_PORTAL,0) AS AVG_PORTAL FROM" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "(SELECT right(replicate('0', 2)+LTRIM(DATEPART(""dd"",MyDay)),2)+'/'+right(replicate('0', 2)+LTRIM(DATEPART(""mm"",MyDay)),2)+'/'+LTRIM(DATEPART(""yyyy"",MyDay)) AS FECHA " & vbCrLf
sConsulta = sConsulta & "from mycte) ACCESOS" & vbCrLf
sConsulta = sConsulta & "left join" & vbCrLf
sConsulta = sConsulta & "(" & vbCrLf
sConsulta = sConsulta & "(SELECT ID,DIA_SEMANA, right(replicate('0', 2)+ltrim(DIA),2)+'/'+right(replicate('0', 2)+ ltrim(MES),2)+'/'+ltrim(ANYO) AS FECHA, ENTORNO, ACTIVOS_GS, ACTIVOS_WEB, ACTIVOS_PORTAL, TOTAL_GS, TOTAL_WEB, TOTAL_PORTAL" & vbCrLf
sConsulta = sConsulta & "FROM FSAL_USUARIOS_DIA WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE DATEPART(""yyyy"",@fecdesde) = ANYO AND DATEPART(""mm"", @fecdesde)= MES AND @fecdesde <= FECHA AND @fechasta >= FECHA AND ENTORNO=@entorno  " & vbCrLf
sConsulta = sConsulta & ") DATOS" & vbCrLf
sConsulta = sConsulta & "LEFT join " & vbCrLf
sConsulta = sConsulta & "(SELECT DIA_SEMANA, round(sum(convert(float,ACTIVOS_GS))/@numDiaSemanaHoras,2,0) AS AVG_GS, round(sum(convert(float,ACTIVOS_WEB))/@numDiaSemanaHoras,2,0) AS AVG_WEB, round(sum(convert(float,ACTIVOS_PORTAL))/@numDiaSemanaHoras,2,0) AS AVG_PORTAL " & vbCrLf
sConsulta = sConsulta & "FROM FSAL_USUARIOS_DIA WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE ENTORNO=@entorno " & vbCrLf
sConsulta = sConsulta & "and isnull(PYME,'') = isnull(@pyme,'')" & vbCrLf
sConsulta = sConsulta & "GROUP BY DIA_SEMANA ) MEDIAS" & vbCrLf
sConsulta = sConsulta & "ON DATOS.DIA_SEMANA = MEDIAS.DIA_SEMANA)" & vbCrLf
sConsulta = sConsulta & "ON ACCESOS.FECHA=DATOS.FECHA" & vbCrLf
sConsulta = sConsulta & "group by accesos.FECHA, datos.ACTIVOS_GS, datos.ACTIVOS_WEB, datos.ACTIVOS_PORTAL, MEDIAS.AVG_GS, medias.AVG_WEB, MEDIAS.AVG_PORTAL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_CARGAR_EVENTOS_POR_ANYO]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_CARGAR_EVENTOS_POR_ANYO]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta


sConsulta = "CREATE PROCEDURE [dbo].[FSAL_CARGAR_EVENTOS_POR_ANYO]  " & vbCrLf
sConsulta = sConsulta & "   @fecdesde DATETIME," & vbCrLf
sConsulta = sConsulta & "   @entorno nvarchar(50)," & vbCrLf
sConsulta = sConsulta & "   @tipoevento INT=null," & vbCrLf
sConsulta = sConsulta & "   @fechasta DATETIME," & vbCrLf
sConsulta = sConsulta & "   @pyme nvarchar(50)=null," & vbCrLf
sConsulta = sConsulta & "   @idioma nvarchar(5)" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @fechacomienzo DATETIME" & vbCrLf
sConsulta = sConsulta & "DECLARE @MEDIA FLOAT" & vbCrLf
sConsulta = sConsulta & "declare @numAnyos float" & vbCrLf
sConsulta = sConsulta & "SET @fechacomienzo=CONVERT(datetime,'2010-01-01')" & vbCrLf
sConsulta = sConsulta & "set @numAnyos = DATEDIFF(""yyyy"",@fechacomienzo,GETDATE())" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "select DISTINCT(RANKING.FECHA_EVENTO), " & vbCrLf
sConsulta = sConsulta & " DEN," & vbCrLf
sConsulta = sConsulta & " RANKING.rank," & vbCrLf
sConsulta = sConsulta & " media.AVG_EVENTOS," & vbCrLf
sConsulta = sConsulta & " RANKING.data   " & vbCrLf
sConsulta = sConsulta & "from " & vbCrLf
sConsulta = sConsulta & " (" & vbCrLf
sConsulta = sConsulta & " select fecha_evento," & vbCrLf
sConsulta = sConsulta & "   tipo_evento," & vbCrLf
sConsulta = sConsulta & "   rank() OVER (Partition by tipo_evento ORDER BY  fecha_evento asc ,id asc ) as rank," & vbCrLf
sConsulta = sConsulta & "  data" & vbCrLf
sConsulta = sConsulta & " from fsal_eventos WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & " where fecha_evento between @fecdesde and @fechasta" & vbCrLf
sConsulta = sConsulta & " ) RANKING" & vbCrLf
sConsulta = sConsulta & "inner join " & vbCrLf
sConsulta = sConsulta & " ( " & vbCrLf
sConsulta = sConsulta & "  select " & vbCrLf
sConsulta = sConsulta & "   tipo_evento," & vbCrLf
sConsulta = sConsulta & "   DEN," & vbCrLf
sConsulta = sConsulta & "   anyo," & vbCrLf
sConsulta = sConsulta & "   mes, " & vbCrLf
sConsulta = sConsulta & "   sum(num_eventos) as num_eventos " & vbCrLf
sConsulta = sConsulta & "  from FSAL_EVENTOS_HIST WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "  left join FSAL_TIPO_EVENTO_DEN on FSAL_EVENTOS_HIST.TIPO_EVENTO=FSAL_TIPO_EVENTO_DEN.ID" & vbCrLf
sConsulta = sConsulta & "  where anyo = datepart(""yyyy"",@fecdesde) " & vbCrLf
sConsulta = sConsulta & "  and ISNULL(@tipoevento,'') = (case when @tipoevento is null then isnull(@tipoEvento,'')" & vbCrLf
sConsulta = sConsulta & "                   ELSE TIPO_EVENTO end)" & vbCrLf
sConsulta = sConsulta & "  AND FSAL_ENTORNO=@entorno " & vbCrLf
sConsulta = sConsulta & "  and ISNULL(FSAL_PYME,'')=ISNULL(@pyme,'')" & vbCrLf
sConsulta = sConsulta & "  and FSAL_TIPO_EVENTO_DEN.IDI=@idioma " & vbCrLf
sConsulta = sConsulta & "  group by tipo_evento,anyo,mes,DEN" & vbCrLf
sConsulta = sConsulta & "  ) EVENTOS_MES" & vbCrLf
sConsulta = sConsulta & "  LEFT JOIN" & vbCrLf
sConsulta = sConsulta & "  (" & vbCrLf
sConsulta = sConsulta & "   SELECT TIPO_EVENTO, round(sum(convert(float, NUM_EVENTOS))/@numAnyos,2,0) as AVG_EVENTOS" & vbCrLf
sConsulta = sConsulta & "   FROM FSAL_EVENTOS_HIST WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   WHERE FSAL_ENTORNO=@entorno " & vbCrLf
sConsulta = sConsulta & "   AND isnull(FSAL_PYME,'') = isnull(@pyme,'') " & vbCrLf
sConsulta = sConsulta & "   AND ISNULL(@tipoevento,'')=(case when @tipoevento is null then isnull(@tipoEvento,'')" & vbCrLf
sConsulta = sConsulta & "                   ELSE TIPO_EVENTO end)" & vbCrLf
sConsulta = sConsulta & "   group by TIPO_EVENTO" & vbCrLf
sConsulta = sConsulta & "  )AS MEDIA" & vbCrLf
sConsulta = sConsulta & "  ON MEDIA.TIPO_EVENTO=EVENTOS_MES.TIPO_EVENTO" & vbCrLf
sConsulta = sConsulta & "  on ranking.TIPO_EVENTO = media.TIPO_EVENTO " & vbCrLf
sConsulta = sConsulta & "order by ranking.fecha_evento desc" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSWS_USU]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSWS_USU]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSWS_USU]" & vbCrLf
sConsulta = sConsulta & "   @COD AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @PERF AS INT" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF" & vbCrLf
sConsulta = sConsulta & "SELECT USU.COD,USU.PWD,USU.FECPWD,PER.NOM + ' ' + PER.APE AS NOMBRE,USU.PER,USU.IDIOMA,USU.MON,PER.DEP,PER.EQP," & vbCrLf
sConsulta = sConsulta & "USU.TIPOEMAIL,PER.EMAIL,USU.DATEFMT,USU.THOUSANFMT,USU.DECIMALFMT,USU.PRECISIONFMT,USU.ADM,USU.FEC_USU," & vbCrLf
sConsulta = sConsulta & "/*ACCESOS*/ISNULL(P.FSGS,USU.FSGS) FSGS,ISNULL(P.FSPM,USU.SOL_COMPRA) FSPM," & vbCrLf
sConsulta = sConsulta & "ISNULL(P.FSQA,USU.FSQA) FSQA,ISNULL(P.FSEP,USU.FSEP) FSEP,ISNULL(P.FSSM,USU.FSSM) FSSM," & vbCrLf
sConsulta = sConsulta & "ISNULL(P.FSIS,USU.FSINT) FSIS,ISNULL(P.FSIM,USU.FSIM) FSIM,ISNULL(P.FSCM,USU.CONTRATOS) FSCM," & vbCrLf
sConsulta = sConsulta & "ISNULL(P.FSBI,USU.FSBI) FSBI, ISNULL(P.FSAL, USU.FSAL) FSAL," & vbCrLf
sConsulta = sConsulta & "/*OTROS*/" & vbCrLf
sConsulta = sConsulta & "USU.FSWS_PRES1,USU.FSWS_PRES2,USU.FSWS_PRES3,USU.FSWS_PRES4," & vbCrLf
sConsulta = sConsulta & "USU.ANYADIR_PROV_FAV,USU.MOSTRAR_DEF_PROV_MAT," & vbCrLf
sConsulta = sConsulta & "USU.PM_ANYADIR_PRES1_FAV,USU.PM_ANYADIR_PRES2_FAV,USU.PM_ANYADIR_PRES3_FAV,USU.PM_ANYADIR_PRES4_FAV," & vbCrLf
sConsulta = sConsulta & "USU.PM_NUMFILAS," & vbCrLf
sConsulta = sConsulta & "USU.FSQA_PP_MANUAL,USU.FSQA_PP_AUTO," & vbCrLf
sConsulta = sConsulta & "USU.FSQA_PP_NIVEL,UNQAD.DEN GRUPOIDIOMAUSU,USU.FSQA_NOTIFICACIONERRORCALCPUNT," & vbCrLf
sConsulta = sConsulta & "/*ACCESO FSEP Y DATOS*/USU.MOSTRARFMT,USU.FSEPTIPO,USU.DEST,USU.MOSTATRIB,USU.MOSTCANTMIN," & vbCrLf
sConsulta = sConsulta & "USU.MOSTCODART,USU.MOSTCODPROVE,USU.MOSTIMGART,USU.ORDEN,USU.DIREC,USU.FSEP_SM_BAR," & vbCrLf
sConsulta = sConsulta & "USU.FSEP_SEG_VER_PED_USU,USU.FSEP_SEG_VER_PED_CCIMP,USU.CARGAR_CANT_PEND_RECEP," & vbCrLf
sConsulta = sConsulta & "USU.FSEP_RECEP_VER_PED_USU,USU.FSEP_RECEP_VER_PED_CCIMP,IDIOMAS.LANGUAGETAG " & vbCrLf
sConsulta = sConsulta & "FROM USU WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PER WITH (NOLOCK) ON USU.PER=PER.COD" & vbCrLf
sConsulta = sConsulta & "INNER JOIN (SELECT IDIOMA,DEN FROM UNQA U WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN UNQA_IDIOMAS UI WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "           ON UI.UNQA=U.ID AND U.NIVEL=0) UNQAD" & vbCrLf
sConsulta = sConsulta & "ON UNQAD.IDIOMA=USU.IDIOMA " & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PERF P WITH(NOLOCK) ON P.ID=USU.PERF" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN IDIOMAS WITH (NOLOCK) ON USU.IDIOMA = IDIOMAS.COD" & vbCrLf
sConsulta = sConsulta & "WHERE USU.COD=@COD AND USU.BAJA=0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @PERF=PERF FROM USU WITH(NOLOCK) WHERE COD=@COD" & vbCrLf
sConsulta = sConsulta & "IF @PERF IS NULL" & vbCrLf
sConsulta = sConsulta & "   SELECT ACC FROM USU_ACC WITH(NOLOCK) WHERE USU=@COD" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   SELECT ACC FROM PERF_ACC WITH(NOLOCK) WHERE PERF=@PERF" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT COUNT(UC.USU) CONTROLPRESUPUESTARIO" & vbCrLf
sConsulta = sConsulta & "FROM USU WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN USU_CC_CONTROL UC WITH (NOLOCK) ON UC.USU=USU.COD AND (UC.CONSULTA=1 OR UC.MODIF=1 OR UC.ALTA=1 OR UC.PRESUP=1)" & vbCrLf
sConsulta = sConsulta & "WHERE USU.COD=@COD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_UONSPRES]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_UONSPRES]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_UONSPRES] " & vbCrLf
sConsulta = sConsulta & "   @TIPO INT, " & vbCrLf
sConsulta = sConsulta & "   @ANYO INT, " & vbCrLf
sConsulta = sConsulta & "   @IDI VARCHAR(50)='SPA', " & vbCrLf
sConsulta = sConsulta & "   @PER VARCHAR(50) = NULL," & vbCrLf
sConsulta = sConsulta & "   @FSWS_PRES_UO BIT=0" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @UON1 VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @UON2 VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @UON3 VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF NOT @PER IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   IF @FSWS_PRES_UO=1" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "      SELECT P.UON1, P.UON2,  P.UON3 , " & vbCrLf
sConsulta = sConsulta & "          CASE WHEN P.UON3 IS NULL THEN " & vbCrLf
sConsulta = sConsulta & "              CASE WHEN P.UON2 IS NULL THEN " & vbCrLf
sConsulta = sConsulta & "                  CASE WHEN P.UON1 IS  NULL THEN '' " & vbCrLf
sConsulta = sConsulta & "                  ELSE UON1.DEN END" & vbCrLf
sConsulta = sConsulta & "              ELSE UON2.DEN END" & vbCrLf
sConsulta = sConsulta & "          ELSE UON3.DEN END DENOMINACION, NULL AS DEN" & vbCrLf
sConsulta = sConsulta & "      FROM PER P WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN UON1 WITH (NOLOCK) ON P.UON1=UON1.COD" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN UON2 WITH (NOLOCK) ON P.UON1=UON2.UON1 AND P.UON2=UON2.COD" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN UON3 WITH (NOLOCK) ON P.UON1=UON3.UON1 AND P.UON2=UON3.UON2 AND P.UON3=UON3.COD" & vbCrLf
sConsulta = sConsulta & "      WHERE P.COD = @PER" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "      SELECT @UON1=P.UON1, @UON2=P.UON2,  @UON3=P.UON3 " & vbCrLf
sConsulta = sConsulta & "      FROM PER P WITH (NOLOCK) WHERE P.COD = @PER" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @TIPO=1 " & vbCrLf
sConsulta = sConsulta & "     SELECT NULL AS UNO1, NULL AS UON2, NULL AS UON3,NULL AS DENOMINACION,NULL AS DEN UNION" & vbCrLf
sConsulta = sConsulta & "     SELECT DISTINCT U.UON1, U.UON2, U.UON3,U.DEN DENOMINACION, CASE WHEN U.UON1  <> '#' THEN U.UON1 + ' - ' ELSE '' END + CASE WHEN U.UON2 <> '#' THEN U.UON2 + ' - ' ELSE '' END + CASE WHEN U.UON3  <> '#' THEN U.UON3 + ' - ' ELSE '' END +   U.DEN DEN" & vbCrLf
sConsulta = sConsulta & "       FROM " & vbCrLf
sConsulta = sConsulta & "         (SELECT '#' UON1, '#' UON2, '#' UON3, DEN FROM PARGEN_LIT WITH (NOLOCK) WHERE IDI=@IDI and id = 1" & vbCrLf
sConsulta = sConsulta & "         UNION" & vbCrLf
sConsulta = sConsulta & "         select COD UON1, '#' UON2, '#' UON3, DEN from UON1 WITH (NOLOCK) WHERE COD = ISNULL(@UON1,COD) AND BAJALOG=0" & vbCrLf
sConsulta = sConsulta & "         UNION" & vbCrLf
sConsulta = sConsulta & "         select UON1, COD UON2, '#' UON3, DEN from UON2 WITH (NOLOCK) WHERE COD = ISNULL(@UON2,COD) AND UON1 = ISNULL(@UON1,UON1) AND BAJALOG=0" & vbCrLf
sConsulta = sConsulta & "         UNION" & vbCrLf
sConsulta = sConsulta & "         select UON1, UON2, COD UON3, DEN from UON3 WITH (NOLOCK)  WHERE COD = ISNULL(@UON3,COD) AND UON1 = ISNULL(@UON1,UON1) AND UON2 = ISNULL(@UON2,UON2) AND BAJALOG=0 ) U" & vbCrLf
sConsulta = sConsulta & "         INNER JOIN (select ISNULL(uon1,'#') UON1, ISNULL(uon2,'#') UON2, ISNULL(uon3,'#') UON3 from pres1_niv1 WITH (NOLOCK)) P1" & vbCrLf
sConsulta = sConsulta & "            ON U.UON1 = P1.UON1 " & vbCrLf
sConsulta = sConsulta & "           AND U.UON2 = P1.UON2 " & vbCrLf
sConsulta = sConsulta & "           AND U.UON3 = P1.UON3" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "     IF @TIPO=2" & vbCrLf
sConsulta = sConsulta & "       SELECT NULL AS UNO1, NULL AS UON2, NULL AS UON3,NULL AS DENOMINACION,NULL AS DEN UNION" & vbCrLf
sConsulta = sConsulta & "       SELECT DISTINCT U.UON1, U.UON2, U.UON3,U.DEN DENOMINACION, CASE WHEN U.UON1  <> '#' THEN U.UON1 + ' - ' ELSE '' END + CASE WHEN U.UON2  <> '#' THEN U.UON2 + ' - ' ELSE '' END + CASE WHEN U.UON3  <> '#' THEN U.UON3 + ' - ' ELSE '' END +   U.DEN DEN" & vbCrLf
sConsulta = sConsulta & "         FROM " & vbCrLf
sConsulta = sConsulta & "           (SELECT '#' UON1, '#' UON2, '#' UON3, DEN FROM PARGEN_LIT WITH (NOLOCK) WHERE IDI=@IDI and id = 1" & vbCrLf
sConsulta = sConsulta & "           UNION" & vbCrLf
sConsulta = sConsulta & "           select COD UON1, '#' UON2, '#' UON3, DEN from UON1 WITH (NOLOCK) WHERE COD = ISNULL(@UON1,COD) AND BAJALOG=0" & vbCrLf
sConsulta = sConsulta & "           UNION" & vbCrLf
sConsulta = sConsulta & "           select UON1, COD UON2, '#' UON3, DEN from UON2 WITH (NOLOCK)  WHERE COD = ISNULL(@UON2,COD) AND UON1 = ISNULL(@UON1,UON1) AND BAJALOG=0 " & vbCrLf
sConsulta = sConsulta & "           UNION" & vbCrLf
sConsulta = sConsulta & "           select UON1, UON2, COD UON3, DEN from UON3 WITH (NOLOCK) WHERE COD = ISNULL(@UON3,COD) AND UON1 = ISNULL(@UON1,UON1) AND UON2 = ISNULL(@UON2,UON2) AND BAJALOG=0 ) U" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN (select ISNULL(uon1,'#') UON1, ISNULL(uon2,'#') UON2, ISNULL(uon3,'#') UON3 from pres2_niv1 WITH (NOLOCK) ) P2" & vbCrLf
sConsulta = sConsulta & "              ON U.UON1 = P2.UON1 " & vbCrLf
sConsulta = sConsulta & "             AND U.UON2 = P2.UON2 " & vbCrLf
sConsulta = sConsulta & "             AND U.UON3 = P2.UON3" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "       IF @TIPO=3" & vbCrLf
sConsulta = sConsulta & "         SELECT NULL AS UNO1, NULL AS UON2, NULL AS UON3,NULL AS DENOMINACION,NULL AS DEN UNION" & vbCrLf
sConsulta = sConsulta & "         SELECT DISTINCT U.UON1, U.UON2, U.UON3,U.DEN DENOMINACION, CASE WHEN U.UON1  <> '#' THEN U.UON1 + ' - ' ELSE '' END + CASE WHEN U.UON2  <> '#' THEN U.UON2 + ' - ' ELSE '' END + CASE WHEN U.UON3  <> '#' THEN U.UON3 + ' - ' ELSE '' END +   U.DEN DEN" & vbCrLf
sConsulta = sConsulta & "           FROM " & vbCrLf
sConsulta = sConsulta & "             (SELECT '#' UON1, '#' UON2, '#' UON3, DEN FROM PARGEN_LIT WITH (NOLOCK) WHERE IDI=@IDI and id = 1" & vbCrLf
sConsulta = sConsulta & "             UNION" & vbCrLf
sConsulta = sConsulta & "             select COD UON1, '#' UON2, '#' UON3, DEN from UON1 WITH (NOLOCK) WHERE COD = ISNULL(@UON1,COD) AND BAJALOG=0" & vbCrLf
sConsulta = sConsulta & "             UNION" & vbCrLf
sConsulta = sConsulta & "             select UON1, COD UON2, '#' UON3, DEN from UON2 WITH (NOLOCK) WHERE COD = ISNULL(@UON2,COD) AND UON1 = ISNULL(@UON1,UON1) AND BAJALOG=0" & vbCrLf
sConsulta = sConsulta & "             UNION" & vbCrLf
sConsulta = sConsulta & "             select UON1, UON2, COD UON3, DEN from UON3 WITH (NOLOCK) WHERE COD = ISNULL(@UON3,COD) AND UON1 = ISNULL(@UON1,UON1) AND UON2 = ISNULL(@UON2,UON2) AND BAJALOG=0 ) U" & vbCrLf
sConsulta = sConsulta & "             INNER JOIN (select ISNULL(uon1,'#') UON1, ISNULL(uon2,'#') UON2, ISNULL(uon3,'#') UON3 from pres3_niv1 WITH (NOLOCK)) P3" & vbCrLf
sConsulta = sConsulta & "                ON U.UON1 = P3.UON1 " & vbCrLf
sConsulta = sConsulta & "               AND U.UON2 = P3.UON2 " & vbCrLf
sConsulta = sConsulta & "               AND U.UON3 = P3.UON3" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "         SELECT NULL AS UNO1, NULL AS UON2, NULL AS UON3,NULL AS DENOMINACION,NULL AS DEN UNION" & vbCrLf
sConsulta = sConsulta & "         SELECT DISTINCT U.UON1, U.UON2, U.UON3,U.DEN DENOMINACION, CASE WHEN U.UON1  <> '#' THEN U.UON1 + ' - ' ELSE '' END + CASE WHEN U.UON2  <> '#' THEN U.UON2 + ' - ' ELSE '' END + CASE WHEN U.UON3  <> '#' THEN U.UON3 + ' - ' ELSE '' END +   U.DEN DEN        FROM " & vbCrLf
sConsulta = sConsulta & "             (SELECT '#' UON1, '#' UON2, '#' UON3, DEN FROM PARGEN_LIT WITH (NOLOCK) WHERE IDI=@IDI and id = 1" & vbCrLf
sConsulta = sConsulta & "             UNION" & vbCrLf
sConsulta = sConsulta & "             select COD UON1, '#' UON2, '#' UON3, DEN from UON1 WITH (NOLOCK) WHERE COD = ISNULL(@UON1,COD) AND BAJALOG=0" & vbCrLf
sConsulta = sConsulta & "             UNION" & vbCrLf
sConsulta = sConsulta & "             select UON1, COD UON2, '#' UON3, DEN from UON2 WITH (NOLOCK) WHERE COD = ISNULL(@UON2,COD) AND UON1 = ISNULL(@UON1,UON1) AND BAJALOG=0" & vbCrLf
sConsulta = sConsulta & "             UNION" & vbCrLf
sConsulta = sConsulta & "             select UON1, UON2, COD UON3, DEN from UON3 WITH (NOLOCK) WHERE COD = ISNULL(@UON3,COD) AND UON1 = ISNULL(@UON1,UON1) AND UON2 = ISNULL(@UON2,UON2) AND BAJALOG=0) U" & vbCrLf
sConsulta = sConsulta & "             INNER JOIN (select ISNULL(uon1,'#') UON1, ISNULL(uon2,'#') UON2, ISNULL(uon3,'#') UON3 from pres4_niv1 WITH (NOLOCK)) P4" & vbCrLf
sConsulta = sConsulta & "                ON U.UON1 = P4.UON1 " & vbCrLf
sConsulta = sConsulta & "               AND U.UON2 = P4.UON2 " & vbCrLf
sConsulta = sConsulta & "               AND U.UON3 = P4.UON3" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       IF @TIPO=1 " & vbCrLf
sConsulta = sConsulta & "         SELECT NULL AS UNO1, NULL AS UON2, NULL AS UON3,NULL AS DENOMINACION,NULL AS DEN UNION" & vbCrLf
sConsulta = sConsulta & "         SELECT DISTINCT U.UON1, U.UON2, U.UON3,U.DEN DENOMINACION, CASE WHEN U.UON1  <> '#' THEN U.UON1 + ' - ' ELSE '' END + CASE WHEN U.UON2 <> '#' THEN U.UON2 + ' - ' ELSE '' END + CASE WHEN U.UON3  <> '#' THEN U.UON3 + ' - ' ELSE '' END +   U.DEN DEN" & vbCrLf
sConsulta = sConsulta & "           FROM " & vbCrLf
sConsulta = sConsulta & "             (SELECT '#' UON1, '#' UON2, '#' UON3, DEN FROM PARGEN_LIT WITH (NOLOCK) WHERE IDI=@IDI and id = 1" & vbCrLf
sConsulta = sConsulta & "             UNION" & vbCrLf
sConsulta = sConsulta & "             select COD UON1, '#' UON2, '#' UON3, DEN from UON1 WITH (NOLOCK) WHERE  BAJALOG=0" & vbCrLf
sConsulta = sConsulta & "             UNION" & vbCrLf
sConsulta = sConsulta & "             select UON1, COD UON2, '#' UON3, DEN from UON2 WITH (NOLOCK) WHERE  BAJALOG=0" & vbCrLf
sConsulta = sConsulta & "             UNION" & vbCrLf
sConsulta = sConsulta & "             select UON1, UON2, COD UON3, DEN from UON3 WITH (NOLOCK)  WHERE BAJALOG=0 ) U" & vbCrLf
sConsulta = sConsulta & "             INNER JOIN (select ISNULL(uon1,'#') UON1, ISNULL(uon2,'#') UON2, ISNULL(uon3,'#') UON3 from pres1_niv1 WITH (NOLOCK)) P1" & vbCrLf
sConsulta = sConsulta & "                ON U.UON1 = P1.UON1 " & vbCrLf
sConsulta = sConsulta & "               AND U.UON2 = P1.UON2 " & vbCrLf
sConsulta = sConsulta & "               AND U.UON3 = P1.UON3" & vbCrLf
sConsulta = sConsulta & "       ELSE" & vbCrLf
sConsulta = sConsulta & "         IF @TIPO=2" & vbCrLf
sConsulta = sConsulta & "           SELECT NULL AS UNO1, NULL AS UON2, NULL AS UON3,NULL AS DENOMINACION,NULL AS DEN UNION" & vbCrLf
sConsulta = sConsulta & "           SELECT DISTINCT U.UON1, U.UON2, U.UON3,U.DEN DENOMINACION, CASE WHEN U.UON1  <> '#' THEN U.UON1 + ' - ' ELSE '' END + CASE WHEN U.UON2  <> '#' THEN U.UON2 + ' - ' ELSE '' END + CASE WHEN U.UON3  <> '#' THEN U.UON3 + ' - ' ELSE '' END +   U.DEN DEN" & vbCrLf
sConsulta = sConsulta & "             FROM " & vbCrLf
sConsulta = sConsulta & "               (SELECT '#' UON1, '#' UON2, '#' UON3, DEN FROM PARGEN_LIT WITH (NOLOCK) WHERE IDI=@IDI and id = 1" & vbCrLf
sConsulta = sConsulta & "               UNION" & vbCrLf
sConsulta = sConsulta & "               select COD UON1, '#' UON2, '#' UON3, DEN from UON1 WITH (NOLOCK) WHERE  BAJALOG=0" & vbCrLf
sConsulta = sConsulta & "               UNION" & vbCrLf
sConsulta = sConsulta & "               select UON1, COD UON2, '#' UON3, DEN from UON2 WITH (NOLOCK)  WHERE BAJALOG=0 " & vbCrLf
sConsulta = sConsulta & "               UNION" & vbCrLf
sConsulta = sConsulta & "               select UON1, UON2, COD UON3, DEN from UON3 WITH (NOLOCK) WHERE BAJALOG=0 ) U" & vbCrLf
sConsulta = sConsulta & "               INNER JOIN (select ISNULL(uon1,'#') UON1, ISNULL(uon2,'#') UON2, ISNULL(uon3,'#') UON3 from pres2_niv1 WITH (NOLOCK) ) P2" & vbCrLf
sConsulta = sConsulta & "                  ON U.UON1 = P2.UON1 " & vbCrLf
sConsulta = sConsulta & "                 AND U.UON2 = P2.UON2 " & vbCrLf
sConsulta = sConsulta & "                 AND U.UON3 = P2.UON3" & vbCrLf
sConsulta = sConsulta & "         ELSE" & vbCrLf
sConsulta = sConsulta & "           IF @TIPO=3" & vbCrLf
sConsulta = sConsulta & "             SELECT NULL AS UNO1, NULL AS UON2, NULL AS UON3,NULL AS DENOMINACION,NULL AS DEN UNION" & vbCrLf
sConsulta = sConsulta & "             SELECT DISTINCT U.UON1, U.UON2, U.UON3,U.DEN DENOMINACION, CASE WHEN U.UON1  <> '#' THEN U.UON1 + ' - ' ELSE '' END + CASE WHEN U.UON2  <> '#' THEN U.UON2 + ' - ' ELSE '' END + CASE WHEN U.UON3  <> '#' THEN U.UON3 + ' - ' ELSE '' END +   U.DEN DEN" & vbCrLf
sConsulta = sConsulta & "               FROM " & vbCrLf
sConsulta = sConsulta & "                 (SELECT '#' UON1, '#' UON2, '#' UON3, DEN FROM PARGEN_LIT WITH (NOLOCK) WHERE IDI=@IDI and id = 1" & vbCrLf
sConsulta = sConsulta & "                 UNION" & vbCrLf
sConsulta = sConsulta & "                 select COD UON1, '#' UON2, '#' UON3, DEN from UON1 WITH (NOLOCK) WHERE  BAJALOG=0" & vbCrLf
sConsulta = sConsulta & "                 UNION" & vbCrLf
sConsulta = sConsulta & "                 select UON1, COD UON2, '#' UON3, DEN from UON2 WITH (NOLOCK) WHERE BAJALOG=0" & vbCrLf
sConsulta = sConsulta & "                 UNION" & vbCrLf
sConsulta = sConsulta & "                 select UON1, UON2, COD UON3, DEN from UON3 WITH (NOLOCK) WHERE BAJALOG=0 ) U" & vbCrLf
sConsulta = sConsulta & "                 INNER JOIN (select ISNULL(uon1,'#') UON1, ISNULL(uon2,'#') UON2, ISNULL(uon3,'#') UON3 from pres3_niv1 WITH (NOLOCK)) P3" & vbCrLf
sConsulta = sConsulta & "                    ON U.UON1 = P3.UON1 " & vbCrLf
sConsulta = sConsulta & "                   AND U.UON2 = P3.UON2 " & vbCrLf
sConsulta = sConsulta & "                   AND U.UON3 = P3.UON3" & vbCrLf
sConsulta = sConsulta & "           ELSE" & vbCrLf
sConsulta = sConsulta & "             SELECT NULL AS UNO1, NULL AS UON2, NULL AS UON3,NULL AS DENOMINACION,NULL AS DEN UNION" & vbCrLf
sConsulta = sConsulta & "             SELECT DISTINCT U.UON1, U.UON2, U.UON3,U.DEN DENOMINACION, CASE WHEN U.UON1  <> '#' THEN U.UON1 + ' - ' ELSE '' END + CASE WHEN U.UON2  <> '#' THEN U.UON2 + ' - ' ELSE '' END + CASE WHEN U.UON3  <> '#' THEN U.UON3 + ' - ' ELSE '' END +   U.DEN DEN        FROM " & vbCrLf
sConsulta = sConsulta & "                 (SELECT '#' UON1, '#' UON2, '#' UON3, DEN FROM PARGEN_LIT WITH (NOLOCK) WHERE IDI=@IDI and id = 1" & vbCrLf
sConsulta = sConsulta & "                 UNION" & vbCrLf
sConsulta = sConsulta & "                 select COD UON1, '#' UON2, '#' UON3, DEN from UON1 WITH (NOLOCK) WHERE  BAJALOG=0" & vbCrLf
sConsulta = sConsulta & "                 UNION" & vbCrLf
sConsulta = sConsulta & "                 select UON1, COD UON2, '#' UON3, DEN from UON2 WITH (NOLOCK) WHERE  BAJALOG=0" & vbCrLf
sConsulta = sConsulta & "                 UNION" & vbCrLf
sConsulta = sConsulta & "                 select UON1, UON2, COD UON3, DEN from UON3 WITH (NOLOCK) WHERE BAJALOG=0) U" & vbCrLf
sConsulta = sConsulta & "                 INNER JOIN (select ISNULL(uon1,'#') UON1, ISNULL(uon2,'#') UON2, ISNULL(uon3,'#') UON3 from pres4_niv1 WITH (NOLOCK)) P4" & vbCrLf
sConsulta = sConsulta & "                    ON U.UON1 = P4.UON1 " & vbCrLf
sConsulta = sConsulta & "                   AND U.UON2 = P4.UON2 " & vbCrLf
sConsulta = sConsulta & "                   AND U.UON3 = P4.UON3" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GS_REUNION_AGENDA]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[GS_REUNION_AGENDA]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta


sConsulta = "CREATE PROCEDURE [dbo].[GS_REUNION_AGENDA] " & vbCrLf
sConsulta = sConsulta & "   @FECHA DATETIME," & vbCrLf
sConsulta = sConsulta & "   @IDIOMA NVARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   DECLARE @SQL NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "   SELECT FECHA FEC_REUNION,REF REF_REUNION,FECHA HORA_INI_REU ,FECHA HORA_FIN_REU" & vbCrLf
sConsulta = sConsulta & "   FROM REU WITH(NOLOCK) WHERE FECHA=@FECHA " & vbCrLf
sConsulta = sConsulta & "         " & vbCrLf
sConsulta = sConsulta & "    SET @SQL=N'SELECT ROW_NUMBER() OVER (ORDER BY RP.HORA ASC) NUM_PROCESO,'" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'PROCE.ANYO,PROCE.GMN1,PROCE.COD,RP.HORA HORA_PROCESO,'       " & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'CONVERT(NVARCHAR(MAX),PROCE.ANYO)+''/''+CONVERT(NVARCHAR(MAX),PROCE.GMN1)+''/''+CONVERT(NVARCHAR(MAX),PROCE.COD) COD_PROCESO,'" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'PROCE.DEN DEN_PROCESO,PROCE.PRESTOTAL PRES_TOTAL,PROCE.UON1_APER,PROCE.UON2_APER,PROCE.UON3_APER,'''' UON_COD_PROCESO,'" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'UON1.DEN UON1_APER_DEN,UON2.DEN UON2_APER_DEN,UON3.DEN UON3_APER_DEN,'''' UON_DEN_PROCESO,'" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'PROCE.PRESTOTAL VOL_PROCESO,PROCE.MON MON_PROCESO,PROCE.ESP COMENT_PROCESO,'" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'PM.GMN1 MAT1_PROCESO,PM.GMN2 MAT2_PROCESO,PM.GMN3 MAT3_PROCESO,PM.GMN4 MAT4_PROCESO,'" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'GMN4.DEN_'+@IDIOMA+N' DMAT4_PROCESO,GMN3.DEN_'+@IDIOMA+N' DMAT3_PROCESO,'" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'GMN2.DEN_'+@IDIOMA+N' DMAT2_PROCESO,GMN1.DEN_'+@IDIOMA+N' DMAT1_PROCESO,'" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'PROCE.FECAPE FECAPER,PROCE.FECLIMOFE,PROCE.FECPRES,PROCE.FECNEC,PG.DEN NOM_REFERENCIA,'" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'PROCE.SOLICITUD REFERENCIA,PROCE.MON MONEDA,PROCE.CAMBIO,PROCE.REUDEC TIPO_REUNION,'" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'PROCE_DEF.DEST DEFDEST,PROCE.DEST COD_DEST_PROCE,DEST.DEN_'+@IDIOMA+N' DEN_DEST_PROCE,'" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'PROCE_DEF.PAG DEFPAG,PROCE.PAG COD_PAGO_PROCE,PAG_DEN.DEN DEN_PAGO_PROCE,'" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'PROCE_DEF.FECSUM DEFFECSUM,PROCE.FECINI FINI_SUM_PROCE,PROCE.FECFIN FFIN_SUM_PROCE,'" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'PROCE.COM COD_RESPONSABLE,COM.NOM NOM_RESPONSABLE,COM.APE APE_RESPONSABLE,'" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'EQP.DEN DEQP_RESPONSABLE,PROCE.EQP EQP_RESPONSABLE,COM.TFNO TLFNO_RESPONSABLE,'" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'COM.EMAIL MAIL_RESPONSABLE,COM.FAX FAX_RESPONSABLE,'" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'PROCE.DEST DEST_COD,DEST.DEN_'+@IDIOMA+N' DEST_DEN,DEST.DIR DEST_DIR,DEST.POB DEST_POB,'" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'DEST.CP DEST_CP,PROVI_DEN.DEN DEST_PROV,PAI_DEN.DEN DEST_PAIS,'" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'PROCE.PAG PAGO_COD,PAG_DEN.DEN PAGO_DEN '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'FROM REU_PROCE RP WITH(NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'INNER JOIN PROCE WITH (NOLOCK) ON RP.ANYO=PROCE.ANYO AND RP.GMN1=PROCE.GMN1 AND RP.PROCE=PROCE.COD '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'AND RP.FECHA=@FECHA AND PROCE.EST<20 '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'LEFT JOIN PROCE_DEF WITH (NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'ON PROCE.ANYO=PROCE_DEF.ANYO AND PROCE.GMN1=PROCE_DEF.GMN1 AND PROCE.COD=PROCE_DEF.PROCE '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'LEFT JOIN UON1 WITH(NOLOCK) ON UON1.COD=PROCE.UON1_APER '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'LEFT JOIN UON2 WITH(NOLOCK) ON UON2.UON1=PROCE.UON1_APER AND UON2.COD=PROCE.UON2_APER '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'LEFT JOIN UON3 WITH(NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'ON UON3.UON1=PROCE.UON1_APER AND UON3.UON2=PROCE.UON2_APER AND UON3.COD=PROCE.UON3_APER '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'LEFT JOIN COM WITH (NOLOCK) ON PROCE.COM=COM.COD AND PROCE.EQP=COM.EQP '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'LEFT JOIN EQP WITH (NOLOCK) ON EQP.COD=COM.EQP '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'INNER JOIN PROCE_GMN4 PM WITH (NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'ON RP.ANYO=PM.ANYO AND RP.GMN1=PM.GMN1_PROCE AND RP.PROCE=PM.PROCE AND RP.FECHA=@FECHA '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'INNER JOIN GMN4 WITH (NOLOCK) ON PM.GMN1=GMN4.GMN1 AND PM.GMN2=GMN4.GMN2 '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'AND PM.GMN3=GMN4.GMN3 AND PM.GMN4=GMN4.COD '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'INNER JOIN GMN3 WITH (NOLOCK) ON PM.GMN1=GMN3.GMN1 AND PM.GMN2=GMN3.GMN2 AND PM.GMN3=GMN3.COD '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'INNER JOIN GMN2 WITH (NOLOCK) ON PM.GMN1=GMN2.GMN1 AND PM.GMN2=GMN2.COD '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'INNER JOIN GMN1 WITH (NOLOCK) ON PM.GMN1=GMN1.COD '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'LEFT JOIN PARGEN_LIT PG WITH(NOLOCK) ON PG.ID=19 AND IDI=@IDIOMA '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'LEFT JOIN DEST WITH(NOLOCK) ON DEST.COD=PROCE.DEST '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'LEFT JOIN PAG_DEN WITH(NOLOCK) ON PAG_DEN.PAG=PROCE.PAG AND PAG_DEN.IDIOMA=@IDIOMA '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'LEFT JOIN PROVI_DEN WITH(NOLOCK) ON PROVI_DEN.PROVI=DEST.PROVI AND PROVI_DEN.PAI=DEST.PAI AND PROVI_DEN.IDIOMA=@IDIOMA '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'LEFT JOIN PAI_DEN WITH(NOLOCK) ON PAI_DEN.PAI=DEST.PAI AND PAI_DEN.IDIOMA=@IDIOMA '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'GROUP BY PROCE.ANYO,PROCE.GMN1,PROCE.COD,RP.HORA,'" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'PROCE.DEN,PROCE.PRESTOTAL,PROCE.UON1_APER,PROCE.UON2_APER,PROCE.UON3_APER,'" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'UON1.DEN,UON2.DEN,UON3.DEN,PROCE.MON,PROCE.ESP,PM.GMN1,PM.GMN2,PM.GMN3,PM.GMN4,'" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'GMN4.DEN_'+@IDIOMA+N',GMN3.DEN_'+@IDIOMA+N',GMN2.DEN_'+@IDIOMA+N',GMN1.DEN_'+@IDIOMA + N','" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'PROCE.FECAPE,PROCE.FECLIMOFE,PROCE.FECPRES,PROCE.FECNEC,PG.DEN,'" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'PROCE.SOLICITUD,PROCE.CAMBIO,PROCE.REUDEC,PROCE_DEF.DEST,PROCE.DEST,DEST.DEN_'+@IDIOMA + N','" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'PROCE_DEF.PAG,PROCE.PAG,PAG_DEN.DEN,PROCE_DEF.FECSUM,PROCE.FECINI,PROCE.FECFIN,'" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'PROCE.COM,COM.NOM,COM.APE,EQP.DEN,PROCE.EQP,COM.TFNO,COM.EMAIL,COM.FAX,'" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'PROCE.DEST,DEST.DEN_'+@IDIOMA+N',DEST.DIR,DEST.POB,'" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'DEST.CP,PROVI_DEN.DEN,PAI_DEN.DEN'" & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL,N'@FECHA DATETIME,@IDIOMA NVARCHAR(50)',@FECHA=@FECHA,@IDIOMA=@IDIOMA" & vbCrLf
sConsulta = sConsulta & "          " & vbCrLf
sConsulta = sConsulta & "    --GRUPOS" & vbCrLf
sConsulta = sConsulta & "    SET @SQL=N'SELECT PROCE_GRUPO.ID,PROCE_GRUPO.ANYO,PROCE_GRUPO.GMN1,PROCE_GRUPO.PROCE,'" & vbCrLf
sConsulta = sConsulta & "    SET @SQL=@SQL + N'PROCE_GRUPO.COD COD_GRUPO,PROCE_GRUPO.DEN DEN_GRUPO,PROCE_GRUPO.DEST COD_DEST_GR,'" & vbCrLf
sConsulta = sConsulta & "    SET @SQL=@SQL + N'DEST.DEN_'+@IDIOMA+N' DEN_DEST_GR,PROCE_GRUPO.PAG COD_PAGO_GR,PAG_DEN.DEN DEN_PAGO_GR,'" & vbCrLf
sConsulta = sConsulta & "    SET @SQL=@SQL + N'PROCE_GRUPO.FECINI FINI_SUM_GR,PROCE_GRUPO.FECFIN FFIN_SUM_GR,PROCE_GRUPO_DEF.DATO ' " & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'FROM REU_PROCE RP WITH(NOLOCK) '  " & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'INNER JOIN PROCE_GRUPO WITH (NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'ON PROCE_GRUPO.ANYO=RP.ANYO AND PROCE_GRUPO.GMN1=RP.GMN1 AND PROCE_GRUPO.PROCE=RP.PROCE AND RP.FECHA=@FECHA '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'LEFT JOIN PROCE_GRUPO_DEF WITH (NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'ON PROCE_GRUPO.ANYO=PROCE_GRUPO_DEF.ANYO AND PROCE_GRUPO.GMN1=PROCE_GRUPO_DEF.GMN1 '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'AND PROCE_GRUPO.PROCE=PROCE_GRUPO_DEF.PROCE AND PROCE_GRUPO.ID=PROCE_GRUPO_DEF.GRUPO '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'LEFT JOIN PROCE_GR_UON1 WITH (NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'ON PROCE_GRUPO.ANYO=PROCE_GR_UON1.ANYO AND PROCE_GRUPO.GMN1=PROCE_GR_UON1.GMN1 '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'AND PROCE_GRUPO.PROCE=PROCE_GR_UON1.PROCE AND PROCE_GRUPO.ID=PROCE_GR_UON1.GRUPO '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'LEFT JOIN PROCE_GR_UON2 WITH (NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'ON PROCE_GRUPO.ANYO=PROCE_GR_UON2.ANYO AND PROCE_GRUPO.GMN1=PROCE_GR_UON2.GMN1 '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'AND PROCE_GRUPO.PROCE=PROCE_GR_UON2.PROCE AND PROCE_GRUPO.ID=PROCE_GR_UON2.GRUPO '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'LEFT JOIN DEST WITH(NOLOCK) ON DEST.COD=PROCE_GRUPO.DEST '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'LEFT JOIN PAG_DEN WITH(NOLOCK) ON PAG_DEN.PAG=PROCE_GRUPO.PAG AND PAG_DEN.IDIOMA=@IDIOMA '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'LEFT JOIN PROCE_GR_UON3 WITH (NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'ON PROCE_GRUPO.ANYO=PROCE_GR_UON3.ANYO AND PROCE_GRUPO.GMN1=PROCE_GR_UON3.GMN1 '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'AND PROCE_GRUPO.PROCE=PROCE_GR_UON3.PROCE AND PROCE_GRUPO.ID=PROCE_GR_UON3.GRUPO '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'LEFT JOIN (SELECT DISTINCT PGP.ANYO,PGP.GMN1,PGP.PROCE,GRUPO,COUNT(PGP.ID) HAY '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'FROM REU_PROCE RP WITH(NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'INNER JOIN PROCE_GR_PRES1 PGP WITH (NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'ON PGP.ANYO=RP.ANYO AND PGP.GMN1=RP.GMN1 AND PGP.PROCE=RP.PROCE AND RP.FECHA=@FECHA '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'INNER JOIN PRES1_NIV1 WITH (NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'ON PGP.PRES1=PRES1_NIV1.ID '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'GROUP BY PGP.ANYO,PGP.GMN1,PGP.PROCE,GRUPO,PGP.ID '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'UNION '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'SELECT DISTINCT PGP.ANYO,PGP.GMN1,PGP.PROCE,GRUPO,COUNT(PGP.ID) HAY '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'FROM REU_PROCE RP WITH(NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'INNER JOIN PROCE_GR_PRES1 PGP WITH (NOLOCK) ' " & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'ON PGP.ANYO=RP.ANYO AND PGP.GMN1=RP.GMN1 AND PGP.PROCE=RP.PROCE AND RP.FECHA=@FECHA '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'INNER JOIN PRES1_NIV2 WITH (NOLOCK) ON PGP.PRES2=PRES1_NIV2.ID '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'GROUP BY PGP.ANYO,PGP.GMN1,PGP.PROCE,GRUPO,PGP.ID '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'UNION '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'SELECT DISTINCT PGP.ANYO,PGP.GMN1,PGP.PROCE,GRUPO,COUNT(PGP.ID) HAY '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'FROM REU_PROCE RP WITH(NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'INNER JOIN PROCE_GR_PRES1 PGP WITH (NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'ON PGP.ANYO=RP.ANYO AND PGP.GMN1=RP.GMN1 AND PGP.PROCE=RP.PROCE AND RP.FECHA=@FECHA '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'INNER JOIN PRES1_NIV3 WITH (NOLOCK) ON PGP.PRES3=PRES1_NIV3.ID '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'GROUP BY PGP.ANYO,PGP.GMN1,PGP.PROCE,GRUPO,PGP.ID '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'UNION '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'SELECT DISTINCT PGP.ANYO,PGP.GMN1,PGP.PROCE,GRUPO,COUNT(PGP.ID) HAY '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'FROM REU_PROCE RP WITH(NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'INNER JOIN PROCE_GR_PRES1 PGP WITH (NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'ON PGP.ANYO=RP.ANYO AND PGP.GMN1=RP.GMN1 AND PGP.PROCE=RP.PROCE AND RP.FECHA=@FECHA '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'INNER JOIN PRES1_NIV4 WITH (NOLOCK) ON PGP.PRES4=PRES1_NIV4.ID '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'GROUP BY PGP.ANYO,PGP.GMN1,PGP.PROCE,GRUPO,PGP.ID) PANU1 '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'ON PANU1.ANYO=PROCE_GRUPO.ANYO AND PANU1.GMN1=PROCE_GRUPO.GMN1 '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'AND PANU1.PROCE=PROCE_GRUPO.PROCE AND PANU1.GRUPO=PROCE_GRUPO.ID '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'LEFT JOIN (SELECT DISTINCT PGP.ANYO,PGP.GMN1,PGP.PROCE,GRUPO,COUNT(PGP.ID) HAY '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'FROM REU_PROCE RP WITH(NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'INNER JOIN PROCE_GR_PRES2 PGP WITH (NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'ON PGP.ANYO=RP.ANYO AND PGP.GMN1=RP.GMN1 AND PGP.PROCE=RP.PROCE AND RP.FECHA=@FECHA ' " & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'INNER JOIN PRES2_NIV1 WITH (NOLOCK) ON PGP.PRES1=PRES2_NIV1.ID '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'GROUP BY PGP.ANYO,PGP.GMN1,PGP.PROCE,GRUPO,PGP.ID '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'UNION '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'SELECT DISTINCT PGP.ANYO,PGP.GMN1,PGP.PROCE,GRUPO,COUNT(PGP.ID) HAY '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'FROM REU_PROCE RP WITH(NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'INNER JOIN PROCE_GR_PRES2 PGP WITH (NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'ON PGP.ANYO=RP.ANYO AND PGP.GMN1=RP.GMN1 AND PGP.PROCE=RP.PROCE AND RP.FECHA=@FECHA '  " & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'INNER JOIN PRES2_NIV2 WITH (NOLOCK) ON PGP.PRES2=PRES2_NIV2.ID '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'GROUP BY PGP.ANYO,PGP.GMN1,PGP.PROCE,GRUPO,PGP.ID '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'UNION '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'SELECT DISTINCT PGP.ANYO,PGP.GMN1,PGP.PROCE,GRUPO,COUNT(PGP.ID) HAY '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'FROM REU_PROCE RP WITH(NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'INNER JOIN PROCE_GR_PRES2 PGP WITH (NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'ON PGP.ANYO=RP.ANYO AND PGP.GMN1=RP.GMN1 AND PGP.PROCE=RP.PROCE AND RP.FECHA=@FECHA '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'INNER JOIN PRES2_NIV3 WITH (NOLOCK) ON PGP.PRES3=PRES2_NIV3.ID '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'GROUP BY PGP.ANYO,PGP.GMN1,PGP.PROCE,GRUPO,PGP.ID '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'UNION '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'SELECT DISTINCT PGP.ANYO,PGP.GMN1,PGP.PROCE,GRUPO,COUNT(PGP.ID) HAY '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'FROM REU_PROCE RP WITH(NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'INNER JOIN PROCE_GR_PRES2 PGP WITH (NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'ON PGP.ANYO=RP.ANYO AND PGP.GMN1=RP.GMN1 AND PGP.PROCE=RP.PROCE AND RP.FECHA=@FECHA ' " & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'INNER JOIN PRES2_NIV4 WITH (NOLOCK) ON PGP.PRES4=PRES2_NIV4.ID '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'GROUP BY PGP.ANYO,PGP.GMN1,PGP.PROCE,GRUPO,PGP.ID) PANU2 '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'ON PANU2.ANYO=PROCE_GRUPO.ANYO AND PANU2.GMN1=PROCE_GRUPO.GMN1 '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'AND PANU2.PROCE=PROCE_GRUPO.PROCE AND PANU2.GRUPO=PROCE_GRUPO.ID ' " & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'LEFT JOIN (SELECT DISTINCT PGP.ANYO,PGP.GMN1,PGP.PROCE,GRUPO,COUNT(PGP.ID) HAY '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'FROM REU_PROCE RP WITH(NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'INNER JOIN PROCE_GR_PRES3 PGP WITH(NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'ON PGP.ANYO=RP.ANYO AND PGP.GMN1=RP.GMN1 AND PGP.PROCE=RP.PROCE AND RP.FECHA=@FECHA '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'INNER JOIN PRES3_NIV1 WITH (NOLOCK) ON PGP.PRES1=PRES3_NIV1.ID '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'GROUP BY PGP.ANYO,PGP.GMN1,PGP.PROCE,GRUPO,PGP.ID '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'UNION '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'SELECT DISTINCT PGP.ANYO,PGP.GMN1,PGP.PROCE,GRUPO,COUNT(PGP.ID) HAY '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'FROM REU_PROCE RP WITH(NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'INNER JOIN PROCE_GR_PRES3 PGP WITH(NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'ON PGP.ANYO=RP.ANYO AND PGP.GMN1=RP.GMN1 AND PGP.PROCE=RP.PROCE AND RP.FECHA=@FECHA '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'INNER JOIN PRES3_NIV2 WITH (NOLOCK) ON PGP.PRES2=PRES3_NIV2.ID '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'GROUP BY PGP.ANYO,PGP.GMN1,PGP.PROCE,GRUPO,PGP.ID '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'UNION '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'SELECT DISTINCT PGP.ANYO,PGP.GMN1,PGP.PROCE,GRUPO,COUNT(PGP.ID) HAY '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'FROM REU_PROCE RP WITH(NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'INNER JOIN PROCE_GR_PRES3 PGP WITH(NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'ON PGP.ANYO=RP.ANYO AND PGP.GMN1=RP.GMN1 AND PGP.PROCE=RP.PROCE AND RP.FECHA=@FECHA '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'INNER JOIN PRES3_NIV3 WITH (NOLOCK) ON PGP.PRES3=PRES3_NIV3.ID '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'GROUP BY PGP.ANYO,PGP.GMN1,PGP.PROCE,GRUPO,PGP.ID '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'UNION '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'SELECT DISTINCT PGP.ANYO,PGP.GMN1,PGP.PROCE,GRUPO,COUNT(PGP.ID) HAY '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'FROM REU_PROCE RP WITH(NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'INNER JOIN PROCE_GR_PRES3 PGP WITH(NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'ON PGP.ANYO=RP.ANYO AND PGP.GMN1=RP.GMN1 AND PGP.PROCE=RP.PROCE AND RP.FECHA=@FECHA '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'INNER JOIN PRES3_NIV4 WITH (NOLOCK) ON PGP.PRES4=PRES3_NIV4.ID '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'GROUP BY PGP.ANYO,PGP.GMN1,PGP.PROCE,GRUPO,PGP.ID) PRES1 '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'ON PRES1.ANYO=PROCE_GRUPO.ANYO AND PRES1.GMN1=PROCE_GRUPO.GMN1 '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'AND PRES1.PROCE=PROCE_GRUPO.PROCE AND PRES1.GRUPO=PROCE_GRUPO.ID ' " & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'LEFT JOIN (SELECT DISTINCT PGP.ANYO,PGP.GMN1,PGP.PROCE,GRUPO,COUNT(PGP.ID) HAY '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'FROM REU_PROCE RP WITH(NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'INNER JOIN PROCE_GR_PRES4 PGP WITH (NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'ON PGP.ANYO=RP.ANYO AND PGP.GMN1=RP.GMN1 AND PGP.PROCE=RP.PROCE AND RP.FECHA=@FECHA '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'INNER JOIN PRES4_NIV1 WITH (NOLOCK) ON PGP.PRES1=PRES4_NIV1.ID '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'GROUP BY PGP.ANYO,PGP.GMN1,PGP.PROCE,GRUPO,PGP.ID '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'UNION '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'SELECT DISTINCT PGP.ANYO,PGP.GMN1,PGP.PROCE,GRUPO,COUNT(PGP.ID) HAY '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'FROM REU_PROCE RP WITH(NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'INNER JOIN PROCE_GR_PRES4 PGP WITH (NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'ON PGP.ANYO=RP.ANYO AND PGP.GMN1=RP.GMN1 AND PGP.PROCE=RP.PROCE AND RP.FECHA=@FECHA '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'INNER JOIN PRES4_NIV2 WITH (NOLOCK) ON PGP.PRES2=PRES4_NIV2.ID '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'GROUP BY PGP.ANYO,PGP.GMN1,PGP.PROCE,GRUPO,PGP.ID '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'UNION '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'SELECT DISTINCT PGP.ANYO,PGP.GMN1,PGP.PROCE,GRUPO,COUNT(PGP.ID) HAY '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'FROM REU_PROCE RP WITH(NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'INNER JOIN PROCE_GR_PRES4 PGP WITH (NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'ON PGP.ANYO=RP.ANYO AND PGP.GMN1=RP.GMN1 AND PGP.PROCE=RP.PROCE AND RP.FECHA=@FECHA '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'INNER JOIN PRES4_NIV3 WITH (NOLOCK) ON PGP.PRES3=PRES4_NIV3.ID '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'GROUP BY PGP.ANYO,PGP.GMN1,PGP.PROCE,GRUPO,PGP.ID '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'UNION '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'SELECT DISTINCT PGP.ANYO,PGP.GMN1,PGP.PROCE,GRUPO,COUNT(PGP.ID) HAY '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'FROM REU_PROCE RP WITH(NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'INNER JOIN PROCE_GR_PRES4 PGP WITH (NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'ON PGP.ANYO=RP.ANYO AND PGP.GMN1=RP.GMN1 AND PGP.PROCE=RP.PROCE AND RP.FECHA=@FECHA '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'INNER JOIN PRES4_NIV4 WITH (NOLOCK) ON PGP.PRES4=PRES4_NIV4.ID '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'GROUP BY PGP.ANYO,PGP.GMN1,PGP.PROCE,GRUPO,PGP.ID) PRES2 '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'ON PRES2.ANYO=PROCE_GRUPO.ANYO AND PRES2.GMN1=PROCE_GRUPO.GMN1 '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'AND PRES2.PROCE=PROCE_GRUPO.PROCE AND PRES2.GRUPO=PROCE_GRUPO.ID ' " & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'LEFT JOIN PROCE_GRUPO_ESP WITH (NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'ON PROCE_GRUPO.ANYO=PROCE_GRUPO_ESP.ANYO AND PROCE_GRUPO.GMN1=PROCE_GRUPO_ESP.GMN1 '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'AND PROCE_GRUPO.PROCE=PROCE_GRUPO_ESP.PROCE AND PROCE_GRUPO.ID=PROCE_GRUPO_ESP.GRUPO ' " & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'LEFT JOIN PROCE_GRUPO_ATRIBESP WITH (NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'ON PROCE_GRUPO.ANYO=PROCE_GRUPO_ATRIBESP.ANYO AND PROCE_GRUPO.GMN1=PROCE_GRUPO_ATRIBESP.GMN1 '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'AND PROCE_GRUPO.PROCE=PROCE_GRUPO_ATRIBESP.PROCE AND PROCE_GRUPO.ID=PROCE_GRUPO_ATRIBESP.GRUPO ' " & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'LEFT JOIN PROCE_ATRIB WITH (NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'ON PROCE_GRUPO.ANYO=PROCE_ATRIB.ANYO AND PROCE_GRUPO.GMN1=PROCE_ATRIB.GMN1 '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'AND PROCE_GRUPO.PROCE=PROCE_ATRIB.PROCE AND PROCE_GRUPO.ID=PROCE_ATRIB.GRUPO ' " & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'LEFT JOIN ITEM WITH (NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'ON PROCE_GRUPO.ANYO=ITEM.ANYO AND PROCE_GRUPO.GMN1=ITEM.GMN1_PROCE AND PROCE_GRUPO.PROCE=ITEM.PROCE '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'AND PROCE_GRUPO.ID=ITEM.GRUPO ' " & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'GROUP BY PROCE_GRUPO.ID,PROCE_GRUPO.ANYO,PROCE_GRUPO.GMN1,PROCE_GRUPO.PROCE,'" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'PROCE_GRUPO.COD,PROCE_GRUPO.DEN,PROCE_GRUPO.DEST,DEST.DEN_'+@IDIOMA+N',PROCE_GRUPO.PAG,PAG_DEN.DEN,'" & vbCrLf
sConsulta = sConsulta & "    SET @SQL=@SQL + N'PROCE_GRUPO.FECINI,PROCE_GRUPO.FECFIN,PROCE_GRUPO_DEF.DATO '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'ORDER BY COD_GRUPO'   " & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL,N'@FECHA DATETIME,@IDIOMA NVARCHAR(50)',@FECHA=@FECHA,@IDIOMA=@IDIOMA" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   --ITEMS" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=N'SELECT ITEM.ANYO,ITEM.GMN1_PROCE,ITEM.PROCE,ITEM.ID,ITEM.GRUPO,'" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'ITEM.ART COD_ART,ITEM.DESCR DEN_ART,ITEM.DEST COD_DEST,DEST.DEN_'+@IDIOMA+N' DEN_DEST,'" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'ITEM.CANT CANTIDAD,ITEM.UNI COD_UNI,UNI_DEN.DEN DEN_UNI,ITEM.PAG COD_PAGO,PAG_DEN.DEN DEN_PAGO,'" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'ITEM.FECINI FINI_SUMINISTRO,ITEM.FECFIN FFIN_SUMINISTRO,0 ITEM_DEST,0 ITEM_PAG,0 ITEM_FECSUM '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'FROM REU_PROCE RP WITH(NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'INNER JOIN ITEM WITH (NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'ON ITEM.ANYO=RP.ANYO AND ITEM.GMN1_PROCE=RP.GMN1 AND ITEM.PROCE=RP.PROCE AND RP.FECHA=@FECHA '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'LEFT JOIN DEST WITH(NOLOCK) ON DEST.COD=ITEM.DEST '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'LEFT JOIN PAG_DEN WITH(NOLOCK) ON PAG_DEN.PAG=ITEM.PAG AND PAG_DEN.IDIOMA=@IDIOMA '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'LEFT JOIN UNI_DEN WITH(NOLOCK) ON UNI_DEN.UNI=ITEM.UNI AND UNI_DEN.IDIOMA=@IDIOMA '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + N'ORDER BY ITEM.ART,ITEM.DESCR,ITEM.ID'" & vbCrLf
sConsulta = sConsulta & "   --SET @SQL=N'SELECT ANYO,GMN1_PROCE,PROCE,GRUPO FROM ITEM WITH(NOLOCK) WHERE ANYO=0'" & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL,N'@FECHA DATETIME,@IDIOMA NVARCHAR(50)',@FECHA=@FECHA,@IDIOMA=@IDIOMA" & vbCrLf
sConsulta = sConsulta & "   --PERSONAS ASIGNADAS" & vbCrLf
sConsulta = sConsulta & "   SELECT PP.ANYO,PP.GMN1,PP.PROCE,PER.COD COD_IMPLICADO,PER.APE APE_IMPLICADO,PER.NOM NOM_IMPLICADO," & vbCrLf
sConsulta = sConsulta & "       PER.UON1,PER.UON2,PER.UON3,PER.DEP DPTO_IMPLICADO,PER.CAR CARGO_IMPLICADO,PER.TFNO TLFNO_IMPLICADO," & vbCrLf
sConsulta = sConsulta & "       PER.TFNO2,PER.FAX FAX_IMPLICADO,PER.EMAIL MAIL_IMPLICADO,PER.BAJALOG,PP.ROL ROL_IMPLICADO," & vbCrLf
sConsulta = sConsulta & "       PP.INVITADO AS INVI,UON1.DEN AS UO1_IMPLICADO,UON2.DEN AS UO2_IMPLICADO,UON3.DEN AS UO3_IMPLICADO," & vbCrLf
sConsulta = sConsulta & "       ISNULL(ROL.CONV,0) CONVOCADO " & vbCrLf
sConsulta = sConsulta & "    FROM REU_PROCE RP WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN PROCE_PER PP WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "       ON PP.ANYO=RP.ANYO AND PP.GMN1=RP.GMN1 AND PP.PROCE=RP.PROCE AND RP.FECHA=@FECHA" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN PER WITH (NOLOCK) ON PP.PER=PER.COD " & vbCrLf
sConsulta = sConsulta & "    LEFT JOIN UON1 WITH (NOLOCK) ON UON1.COD = PER.UON1 " & vbCrLf
sConsulta = sConsulta & "    LEFT JOIN UON2 WITH (NOLOCK) ON UON2.UON1 = PER.UON1 and UON2.COD = PER.UON2 " & vbCrLf
sConsulta = sConsulta & "    LEFT JOIN UON3 WITH (NOLOCK) ON UON3.UON1 = PER.UON1 and UON3.UON2 = PER.UON2 and UON3.COD = PER.UON3 " & vbCrLf
sConsulta = sConsulta & "    LEFT JOIN ROL WITH (NOLOCK) ON ROL.COD = PP.ROL" & vbCrLf
sConsulta = sConsulta & "    WHERE ROL.CONV=1" & vbCrLf
sConsulta = sConsulta & "    ORDER BY PER.APE" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSINT_VISOR]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSINT_VISOR]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta


sConsulta = "CREATE PROCEDURE [dbo].[FSINT_VISOR]" & vbCrLf
sConsulta = sConsulta & "  @ERP AS VARCHAR(10)=NULL, @TABLA AS smallint=NULL, " & vbCrLf
sConsulta = sConsulta & "   @COD_FS AS NVARCHAR(100)=NULL, @COD_ERP AS NVARCHAR(100)=NULL, @RECEP_ERP AS NVARCHAR(1000)=NULL," & vbCrLf
sConsulta = sConsulta & "   @ANYO_PROCE AS SMALLINT=NULL, @GMN1_PROCE AS VARCHAR(10)=NULL, @COD_PROCE AS INT=NULL," & vbCrLf
sConsulta = sConsulta & "   @ANYO_PEDIDO AS SMALLINT=NULL, @NUM_PEDIDO AS INT =NULL, @NUM_ORDEN AS INT =NULL," & vbCrLf
sConsulta = sConsulta & "   @ID_LOG AS INTEGER=NULL, @ACCION AS VARCHAR(1)=NULL, @ESTADO AS VARCHAR(1)=NULL, @SENTIDO AS TINYINT=NULL, " & vbCrLf
sConsulta = sConsulta & "   @FECHA_DESDE AS DATETIME =NULL, @FECHA_HASTA AS DATETIME=NULL," & vbCrLf
sConsulta = sConsulta & "   @FORMATOFECHA AS VARCHAR(50)=NULL" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  --Variables para las consultas" & vbCrLf
sConsulta = sConsulta & "   DECLARE @CONSULTA AS NVARCHAR(MAX)   DECLARE @WHERE AS NVARCHAR(MAX)   DECLARE @WHEREPEDIDO AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "   DECLARE @WHEREPROCE AS NVARCHAR(MAX) DECLARE @WHERECOD AS NVARCHAR(MAX) DECLARE @WHEREPEDIDO_ENRECEP AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "   DECLARE @TIPOFECHA AS INT            DECLARE @GMN1 AS NVARCHAR(MAX)     DECLARE @GMN2 AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "   DECLARE @GMN3 AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "   --Variables para indicar si la entidad tiene integraci�n activada o no" & vbCrLf
sConsulta = sConsulta & "   DECLARE @MON_ACTIVA AS TINYINT=0            DECLARE @PAI_ACTIVA AS TINYINT=0            DECLARE @PROVI_ACTIVA AS TINYINT=0 " & vbCrLf
sConsulta = sConsulta & "   DECLARE @PAG_ACTIVA AS TINYINT=0            DECLARE @DEST_ACTIVA AS TINYINT=0           DECLARE @UNI_ACTIVA AS TINYINT=0 " & vbCrLf
sConsulta = sConsulta & "   DECLARE @ART4_ACTIVA AS TINYINT=0       DECLARE @PROVE_ACTIVA AS TINYINT=0          DECLARE @ADJ_ACTIVA AS TINYINT=0 " & vbCrLf
sConsulta = sConsulta & "   DECLARE @PEDIDO_ACTIVA AS TINYINT=0     DECLARE @RECEP_ACTIVA AS TINYINT=0          DECLARE @ESTRMAT_ACTIVA AS TINYINT=0 " & vbCrLf
sConsulta = sConsulta & "   DECLARE @CONSUMOS_ACTIVA AS TINYINT=0   DECLARE @VARCAL_ACTIVA AS TINYINT=0         DECLARE @SOLICIT_ACTIVA AS TINYINT=0   " & vbCrLf
sConsulta = sConsulta & "   DECLARE @PRES_ACTIVA AS TINYINT=0       DECLARE @PROCE_ACTIVA AS TINYINT=0          DECLARE @VIA_PAG_ACTIVA AS TINYINT=0 " & vbCrLf
sConsulta = sConsulta & "   DECLARE @PPM_ACTIVA AS TINYINT=0            DECLARE @TASA_SERVICIO_ACTIVA AS TINYINT=0  DECLARE @CARGO_PROVE_ACTIVA AS TINYINT=0 " & vbCrLf
sConsulta = sConsulta & "   DECLARE @UON_ACTIVA AS TINYINT=0            DECLARE @USU_ACTIVA AS TINYINT=0            DECLARE @ACTIVO_ACTIVA AS TINYINT=0 " & vbCrLf
sConsulta = sConsulta & "   DECLARE @FACTURA_ACTIVA AS TINYINT=0        DECLARE @PAGO_ACTIVA AS TINYINT=0           DECLARE @PRES5_ACTIVA AS TINYINT=0 " & vbCrLf
sConsulta = sConsulta & "   DECLARE @CENTRO_SM_ACTIVA AS TINYINT=0  DECLARE @EMPRESA_ACTIVA AS TINYINT =0       DECLARE @CON_ACTIVA AS TINYINT=0 " & vbCrLf
sConsulta = sConsulta & "   DECLARE @PROVE_ERP_ACTIVA AS TINYINT=0 " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "--Obtiene el formato de fecha" & vbCrLf
sConsulta = sConsulta & "   SET @TIPOFECHA=103" & vbCrLf
sConsulta = sConsulta & "   IF @FORMATOFECHA='dd/mm/yyyy'" & vbCrLf
sConsulta = sConsulta & "       SET @TIPOFECHA=103" & vbCrLf
sConsulta = sConsulta & "   ELSE IF @FORMATOFECHA='mm/dd/yyyy'" & vbCrLf
sConsulta = sConsulta & "       SET @TIPOFECHA=101" & vbCrLf
sConsulta = sConsulta & "   ELSE IF @FORMATOFECHA='dd-mm-yyyy'" & vbCrLf
sConsulta = sConsulta & "       SET @TIPOFECHA=105" & vbCrLf
sConsulta = sConsulta & "   ELSE IF @FORMATOFECHA='mm-dd-yyyy'" & vbCrLf
sConsulta = sConsulta & "       SET @TIPOFECHA=110" & vbCrLf
sConsulta = sConsulta & "   ELSE IF @FORMATOFECHA='mm.dd.yyyy'" & vbCrLf
sConsulta = sConsulta & "       SET @TIPOFECHA=104" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Siempre filtra por ERP. Si no hay, se coge el primero" & vbCrLf
sConsulta = sConsulta & "   IF @ERP is null " & vbCrLf
sConsulta = sConsulta & "       SET @ERP = (SELECT TOP 1 COD FROM ERP WITH(NOLOCK) )" & vbCrLf
sConsulta = sConsulta & "   IF @TABLA IS NULL" & vbCrLf
sConsulta = sConsulta & "       SET @TABLA=0" & vbCrLf
sConsulta = sConsulta & "--------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "--     Busca las entidades para las que hay que consultar datos" & vbCrLf
sConsulta = sConsulta & "--------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @TABLA=1 OR @TABLA=0" & vbCrLf
sConsulta = sConsulta & "       begin" & vbCrLf
sConsulta = sConsulta & "           IF @SENTIDO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "               SET @MON_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=1 AND ERP=@ERP AND SENTIDO=@SENTIDO)" & vbCrLf
sConsulta = sConsulta & "           ELSE IF @SENTIDO IS NULL" & vbCrLf
sConsulta = sConsulta & "               SET @MON_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=1 AND ERP=@ERP)" & vbCrLf
sConsulta = sConsulta & "       end" & vbCrLf
sConsulta = sConsulta & "   IF @TABLA=2 OR @TABLA=0" & vbCrLf
sConsulta = sConsulta & "       begin" & vbCrLf
sConsulta = sConsulta & "           IF @SENTIDO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "               SET @PAI_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=2 AND ERP=@ERP AND SENTIDO=@SENTIDO)" & vbCrLf
sConsulta = sConsulta & "           ELSE IF @SENTIDO IS NULL" & vbCrLf
sConsulta = sConsulta & "               SET @PAI_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=2 AND ERP=@ERP)" & vbCrLf
sConsulta = sConsulta & "       end" & vbCrLf
sConsulta = sConsulta & "   IF @TABLA=3 OR @TABLA=0" & vbCrLf
sConsulta = sConsulta & "       begin" & vbCrLf
sConsulta = sConsulta & "           IF @SENTIDO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "               SET @PROVI_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=3 AND ERP=@ERP AND SENTIDO=@SENTIDO)" & vbCrLf
sConsulta = sConsulta & "           ELSE IF @SENTIDO IS NULL" & vbCrLf
sConsulta = sConsulta & "               SET @PROVI_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=3 AND ERP=@ERP)" & vbCrLf
sConsulta = sConsulta & "       end" & vbCrLf
sConsulta = sConsulta & "   IF @TABLA=4 OR @TABLA=0" & vbCrLf
sConsulta = sConsulta & "       begin" & vbCrLf
sConsulta = sConsulta & "           IF @SENTIDO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "               SET @PAG_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=4 AND ERP=@ERP AND SENTIDO=@SENTIDO)" & vbCrLf
sConsulta = sConsulta & "           ELSE IF @SENTIDO IS NULL" & vbCrLf
sConsulta = sConsulta & "               SET @PAG_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=4 AND ERP=@ERP)" & vbCrLf
sConsulta = sConsulta & "       end" & vbCrLf
sConsulta = sConsulta & "   IF @TABLA=5 OR @TABLA=0" & vbCrLf
sConsulta = sConsulta & "       begin" & vbCrLf
sConsulta = sConsulta & "           IF @SENTIDO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "               SET @DEST_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=5 AND ERP=@ERP AND SENTIDO=@SENTIDO)   " & vbCrLf
sConsulta = sConsulta & "           ELSE IF @SENTIDO IS NULL" & vbCrLf
sConsulta = sConsulta & "               SET @DEST_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=5 AND ERP=@ERP)" & vbCrLf
sConsulta = sConsulta & "       end" & vbCrLf
sConsulta = sConsulta & "   IF @TABLA=6 OR @TABLA=0" & vbCrLf
sConsulta = sConsulta & "       begin" & vbCrLf
sConsulta = sConsulta & "           IF @SENTIDO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "               SET @UNI_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=6 AND ERP=@ERP AND SENTIDO=@SENTIDO)    " & vbCrLf
sConsulta = sConsulta & "           ELSE IF @SENTIDO IS NULL" & vbCrLf
sConsulta = sConsulta & "               SET @UNI_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=6 AND ERP=@ERP)" & vbCrLf
sConsulta = sConsulta & "       end" & vbCrLf
sConsulta = sConsulta & "   IF @TABLA=7 OR @TABLA=0" & vbCrLf
sConsulta = sConsulta & "       begin" & vbCrLf
sConsulta = sConsulta & "           IF @SENTIDO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "               SET @ART4_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=7 AND ERP=@ERP AND SENTIDO=@SENTIDO)" & vbCrLf
sConsulta = sConsulta & "           ELSE IF @SENTIDO IS NULL" & vbCrLf
sConsulta = sConsulta & "               SET @ART4_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=7 AND ERP=@ERP)" & vbCrLf
sConsulta = sConsulta & "       end" & vbCrLf
sConsulta = sConsulta & "   IF @TABLA=8 OR @TABLA=0" & vbCrLf
sConsulta = sConsulta & "       begin" & vbCrLf
sConsulta = sConsulta & "           SET @PROVE_ERP_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA_INTERMEDIA=1 AND TABLA=8 AND ERP=@ERP ) --Sentido=NULL " & vbCrLf
sConsulta = sConsulta & "           IF @PROVE_ERP_ACTIVA=1 AND NOT(@TABLA=0)" & vbCrLf
sConsulta = sConsulta & "              SET @TABLA=102 --Proveedores con tabla intermedia: TABLAS_INTEGRACION_ERP.TABLA=8 pero LOG_GRAL.TABLA=102" & vbCrLf
sConsulta = sConsulta & "              " & vbCrLf
sConsulta = sConsulta & "           IF @SENTIDO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "               SET @PROVE_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA_INTERMEDIA=0 AND TABLA=8 AND ERP=@ERP AND SENTIDO=@SENTIDO)" & vbCrLf
sConsulta = sConsulta & "           ELSE IF @SENTIDO IS NULL" & vbCrLf
sConsulta = sConsulta & "               SET @PROVE_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA_INTERMEDIA=0 AND TABLA=8 AND ERP=@ERP)" & vbCrLf
sConsulta = sConsulta & "       end" & vbCrLf
sConsulta = sConsulta & "   IF @TABLA=9 OR @TABLA=0" & vbCrLf
sConsulta = sConsulta & "       begin" & vbCrLf
sConsulta = sConsulta & "           IF @SENTIDO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "               SET @ADJ_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=9 AND ERP=@ERP AND SENTIDO=@SENTIDO)    " & vbCrLf
sConsulta = sConsulta & "           ELSE IF @SENTIDO IS NULL" & vbCrLf
sConsulta = sConsulta & "               SET @ADJ_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=9 AND ERP=@ERP)" & vbCrLf
sConsulta = sConsulta & "       end" & vbCrLf
sConsulta = sConsulta & "   IF @TABLA=11 OR @TABLA=0" & vbCrLf
sConsulta = sConsulta & "       begin" & vbCrLf
sConsulta = sConsulta & "           IF @SENTIDO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "               SET @PEDIDO_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=11 AND ERP=@ERP AND SENTIDO=@SENTIDO)    " & vbCrLf
sConsulta = sConsulta & "           ELSE IF @SENTIDO IS NULL" & vbCrLf
sConsulta = sConsulta & "               SET @PEDIDO_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=11 AND ERP=@ERP) " & vbCrLf
sConsulta = sConsulta & "       end " & vbCrLf
sConsulta = sConsulta & "   IF @TABLA=12 OR @TABLA=0" & vbCrLf
sConsulta = sConsulta & "       begin" & vbCrLf
sConsulta = sConsulta & "           IF @SENTIDO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "               SET @RECEP_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=12 AND ERP=@ERP AND SENTIDO=@SENTIDO)" & vbCrLf
sConsulta = sConsulta & "           ELSE IF @SENTIDO IS NULL" & vbCrLf
sConsulta = sConsulta & "               SET @RECEP_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=12 AND ERP=@ERP)" & vbCrLf
sConsulta = sConsulta & "       end" & vbCrLf
sConsulta = sConsulta & "   IF @TABLA=13 OR @TABLA=0" & vbCrLf
sConsulta = sConsulta & "       begin" & vbCrLf
sConsulta = sConsulta & "           IF @SENTIDO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "               SET @PEDIDO_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=13 AND ERP=@ERP AND SENTIDO=@SENTIDO)" & vbCrLf
sConsulta = sConsulta & "           ELSE IF @SENTIDO IS NULL" & vbCrLf
sConsulta = sConsulta & "               SET @PEDIDO_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=13 AND ERP=@ERP)" & vbCrLf
sConsulta = sConsulta & "       end" & vbCrLf
sConsulta = sConsulta & "   IF @TABLA=14 OR @TABLA=0" & vbCrLf
sConsulta = sConsulta & "       begin" & vbCrLf
sConsulta = sConsulta & "           IF @SENTIDO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "               SET @RECEP_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=14 AND ERP=@ERP AND SENTIDO=@SENTIDO)" & vbCrLf
sConsulta = sConsulta & "           ELSE IF @SENTIDO IS NULL" & vbCrLf
sConsulta = sConsulta & "               SET @RECEP_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=14 AND ERP=@ERP)" & vbCrLf
sConsulta = sConsulta & "       end" & vbCrLf
sConsulta = sConsulta & "   IF @TABLA=15 OR @TABLA=0" & vbCrLf
sConsulta = sConsulta & "       begin" & vbCrLf
sConsulta = sConsulta & "           IF @SENTIDO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "               SET @ESTRMAT_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=15 AND ERP=@ERP AND SENTIDO=@SENTIDO)" & vbCrLf
sConsulta = sConsulta & "           ELSE IF @SENTIDO IS NULL" & vbCrLf
sConsulta = sConsulta & "               SET @ESTRMAT_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=15 AND ERP=@ERP)" & vbCrLf
sConsulta = sConsulta & "       end" & vbCrLf
sConsulta = sConsulta & "   IF @TABLA=16 OR @TABLA=0" & vbCrLf
sConsulta = sConsulta & "       begin" & vbCrLf
sConsulta = sConsulta & "           IF @SENTIDO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "               SET @CONSUMOS_ACTIVA  = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=16 AND ERP=@ERP AND SENTIDO=@SENTIDO)" & vbCrLf
sConsulta = sConsulta & "           ELSE IF @SENTIDO IS NULL" & vbCrLf
sConsulta = sConsulta & "               SET @CONSUMOS_ACTIVA  = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=16 AND ERP=@ERP)" & vbCrLf
sConsulta = sConsulta & "       end" & vbCrLf
sConsulta = sConsulta & "   IF @TABLA=17 OR @TABLA=0" & vbCrLf
sConsulta = sConsulta & "       begin" & vbCrLf
sConsulta = sConsulta & "           IF @SENTIDO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "               SET @VARCAL_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=17 AND ERP=@ERP AND SENTIDO=@SENTIDO)" & vbCrLf
sConsulta = sConsulta & "           ELSE IF @SENTIDO IS NULL" & vbCrLf
sConsulta = sConsulta & "               SET @VARCAL_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=17 AND ERP=@ERP)" & vbCrLf
sConsulta = sConsulta & "       end" & vbCrLf
sConsulta = sConsulta & "   IF @TABLA=18 OR @TABLA=0" & vbCrLf
sConsulta = sConsulta & "       begin" & vbCrLf
sConsulta = sConsulta & "           IF @SENTIDO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "               SET @SOLICIT_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=18 AND ERP=@ERP AND SENTIDO=@SENTIDO)" & vbCrLf
sConsulta = sConsulta & "           ELSE IF @SENTIDO IS NULL" & vbCrLf
sConsulta = sConsulta & "               SET @SOLICIT_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=18 AND ERP=@ERP)" & vbCrLf
sConsulta = sConsulta & "       end" & vbCrLf
sConsulta = sConsulta & "   IF @TABLA=19 OR @TABLA=0" & vbCrLf
sConsulta = sConsulta & "       begin" & vbCrLf
sConsulta = sConsulta & "           IF @SENTIDO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "               SET @PRES_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=19 AND ERP=@ERP AND SENTIDO=@SENTIDO)" & vbCrLf
sConsulta = sConsulta & "           ELSE IF @SENTIDO IS NULL" & vbCrLf
sConsulta = sConsulta & "               SET @PRES_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=19 AND ERP=@ERP)" & vbCrLf
sConsulta = sConsulta & "       end" & vbCrLf
sConsulta = sConsulta & "   IF @TABLA=20 OR @TABLA=0" & vbCrLf
sConsulta = sConsulta & "       begin" & vbCrLf
sConsulta = sConsulta & "           IF @SENTIDO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "               SET @PRES_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=20 AND ERP=@ERP AND SENTIDO=@SENTIDO)" & vbCrLf
sConsulta = sConsulta & "           ELSE IF @SENTIDO IS NULL" & vbCrLf
sConsulta = sConsulta & "               SET @PRES_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=20 AND ERP=@ERP)" & vbCrLf
sConsulta = sConsulta & "       end" & vbCrLf
sConsulta = sConsulta & "   IF @TABLA=21 OR @TABLA=0" & vbCrLf
sConsulta = sConsulta & "       begin" & vbCrLf
sConsulta = sConsulta & "           IF @SENTIDO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "               SET @PRES_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=21 AND ERP=@ERP AND SENTIDO=@SENTIDO)" & vbCrLf
sConsulta = sConsulta & "           ELSE IF @SENTIDO IS NULL" & vbCrLf
sConsulta = sConsulta & "               SET @PRES_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=21 AND ERP=@ERP)" & vbCrLf
sConsulta = sConsulta & "       end" & vbCrLf
sConsulta = sConsulta & "   IF @TABLA=22 OR @TABLA=0" & vbCrLf
sConsulta = sConsulta & "       begin" & vbCrLf
sConsulta = sConsulta & "           IF @SENTIDO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "               SET @PRES_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=22 AND ERP=@ERP AND SENTIDO=@SENTIDO)" & vbCrLf
sConsulta = sConsulta & "           ELSE IF @SENTIDO IS NULL" & vbCrLf
sConsulta = sConsulta & "               SET @PRES_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=22 AND ERP=@ERP)" & vbCrLf
sConsulta = sConsulta & "       end" & vbCrLf
sConsulta = sConsulta & "   IF @TABLA=23 OR @TABLA=0" & vbCrLf
sConsulta = sConsulta & "       begin" & vbCrLf
sConsulta = sConsulta & "           IF @SENTIDO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "               SET @PROCE_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=23 AND ERP=@ERP AND SENTIDO=@SENTIDO)" & vbCrLf
sConsulta = sConsulta & "           ELSE IF @SENTIDO IS NULL" & vbCrLf
sConsulta = sConsulta & "               SET @PROCE_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=23 AND ERP=@ERP)" & vbCrLf
sConsulta = sConsulta & "       end" & vbCrLf
sConsulta = sConsulta & "   IF @TABLA=25 OR @TABLA=0" & vbCrLf
sConsulta = sConsulta & "       begin" & vbCrLf
sConsulta = sConsulta & "           IF @SENTIDO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "               SET @VIA_PAG_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=25 AND ERP=@ERP AND SENTIDO=@SENTIDO)" & vbCrLf
sConsulta = sConsulta & "           ELSE IF @SENTIDO IS NULL" & vbCrLf
sConsulta = sConsulta & "               SET @VIA_PAG_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=25 AND ERP=@ERP)" & vbCrLf
sConsulta = sConsulta & "       end" & vbCrLf
sConsulta = sConsulta & "   IF @TABLA=26 OR @TABLA=0" & vbCrLf
sConsulta = sConsulta & "       begin" & vbCrLf
sConsulta = sConsulta & "           IF @SENTIDO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "               SET @PPM_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=26 AND ERP=@ERP AND SENTIDO=@SENTIDO)" & vbCrLf
sConsulta = sConsulta & "           ELSE IF @SENTIDO IS NULL" & vbCrLf
sConsulta = sConsulta & "               SET @PPM_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=26 AND ERP=@ERP)" & vbCrLf
sConsulta = sConsulta & "       end" & vbCrLf
sConsulta = sConsulta & "   IF @TABLA=27 OR @TABLA=0" & vbCrLf
sConsulta = sConsulta & "       begin" & vbCrLf
sConsulta = sConsulta & "           IF @SENTIDO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "               SET @TASA_SERVICIO_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=27 AND ERP=@ERP AND SENTIDO=@SENTIDO)" & vbCrLf
sConsulta = sConsulta & "           ELSE IF @SENTIDO IS NULL" & vbCrLf
sConsulta = sConsulta & "               SET @TASA_SERVICIO_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=27 AND ERP=@ERP)" & vbCrLf
sConsulta = sConsulta & "       end" & vbCrLf
sConsulta = sConsulta & "   IF @TABLA=28 OR @TABLA=0" & vbCrLf
sConsulta = sConsulta & "       begin" & vbCrLf
sConsulta = sConsulta & "           IF @SENTIDO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "               SET @CARGO_PROVE_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=28 AND ERP=@ERP AND SENTIDO=@SENTIDO)" & vbCrLf
sConsulta = sConsulta & "           ELSE IF @SENTIDO IS NULL" & vbCrLf
sConsulta = sConsulta & "               SET @CARGO_PROVE_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=28 AND ERP=@ERP)" & vbCrLf
sConsulta = sConsulta & "       end" & vbCrLf
sConsulta = sConsulta & "   IF @TABLA=29 OR @TABLA=0" & vbCrLf
sConsulta = sConsulta & "       begin" & vbCrLf
sConsulta = sConsulta & "           IF @SENTIDO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "               SET @UON_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=29 AND ERP=@ERP AND SENTIDO=@SENTIDO)" & vbCrLf
sConsulta = sConsulta & "           ELSE IF @SENTIDO IS NULL" & vbCrLf
sConsulta = sConsulta & "               SET @UON_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=29 AND ERP=@ERP)" & vbCrLf
sConsulta = sConsulta & "       end" & vbCrLf
sConsulta = sConsulta & "   IF @TABLA=30 OR @TABLA=0" & vbCrLf
sConsulta = sConsulta & "       begin" & vbCrLf
sConsulta = sConsulta & "           IF @SENTIDO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "               SET @USU_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=30 AND ERP=@ERP AND SENTIDO=@SENTIDO)" & vbCrLf
sConsulta = sConsulta & "           ELSE IF @SENTIDO IS NULL" & vbCrLf
sConsulta = sConsulta & "               SET @USU_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=30 AND ERP=@ERP)" & vbCrLf
sConsulta = sConsulta & "       end" & vbCrLf
sConsulta = sConsulta & "   IF @TABLA=31 OR @TABLA=0" & vbCrLf
sConsulta = sConsulta & "       begin" & vbCrLf
sConsulta = sConsulta & "           IF @SENTIDO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "               SET @ACTIVO_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=31 AND ERP=@ERP AND SENTIDO=@SENTIDO)" & vbCrLf
sConsulta = sConsulta & "           ELSE IF @SENTIDO IS NULL" & vbCrLf
sConsulta = sConsulta & "               SET @ACTIVO_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=31 AND ERP=@ERP)" & vbCrLf
sConsulta = sConsulta & "       end" & vbCrLf
sConsulta = sConsulta & "   IF @TABLA=32 OR @TABLA=0" & vbCrLf
sConsulta = sConsulta & "       begin" & vbCrLf
sConsulta = sConsulta & "           IF @SENTIDO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "               SET @FACTURA_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=32 AND ERP=@ERP AND SENTIDO=@SENTIDO)" & vbCrLf
sConsulta = sConsulta & "           ELSE IF @SENTIDO IS NULL" & vbCrLf
sConsulta = sConsulta & "               SET @FACTURA_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=32 AND ERP=@ERP)" & vbCrLf
sConsulta = sConsulta & "       end" & vbCrLf
sConsulta = sConsulta & "   IF @TABLA=33 OR @TABLA=0" & vbCrLf
sConsulta = sConsulta & "       begin" & vbCrLf
sConsulta = sConsulta & "           IF @SENTIDO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "               SET @PAGO_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=33 AND ERP=@ERP AND SENTIDO=@SENTIDO)" & vbCrLf
sConsulta = sConsulta & "           ELSE IF @SENTIDO IS NULL" & vbCrLf
sConsulta = sConsulta & "               SET @PAGO_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=33 AND ERP=@ERP)" & vbCrLf
sConsulta = sConsulta & "       end" & vbCrLf
sConsulta = sConsulta & "   IF @TABLA=34 OR @TABLA=0" & vbCrLf
sConsulta = sConsulta & "       begin" & vbCrLf
sConsulta = sConsulta & "           IF @SENTIDO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "               SET @PRES5_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=34 AND ERP=@ERP AND SENTIDO=@SENTIDO)" & vbCrLf
sConsulta = sConsulta & "           ELSE IF @SENTIDO IS NULL" & vbCrLf
sConsulta = sConsulta & "               SET @PRES5_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=34 AND ERP=@ERP)" & vbCrLf
sConsulta = sConsulta & "       end" & vbCrLf
sConsulta = sConsulta & "   IF @TABLA=35 OR @TABLA=0" & vbCrLf
sConsulta = sConsulta & "       begin" & vbCrLf
sConsulta = sConsulta & "           IF @SENTIDO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "               SET @CENTRO_SM_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=35 AND ERP=@ERP AND SENTIDO=@SENTIDO)" & vbCrLf
sConsulta = sConsulta & "           ELSE IF @SENTIDO IS NULL" & vbCrLf
sConsulta = sConsulta & "               SET @CENTRO_SM_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=35 AND ERP=@ERP)" & vbCrLf
sConsulta = sConsulta & "       end" & vbCrLf
sConsulta = sConsulta & "   IF @TABLA=36 OR @TABLA=0" & vbCrLf
sConsulta = sConsulta & "       begin" & vbCrLf
sConsulta = sConsulta & "           IF @SENTIDO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "               SET @EMPRESA_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=36 AND ERP=@ERP AND SENTIDO=@SENTIDO)" & vbCrLf
sConsulta = sConsulta & "           ELSE IF @SENTIDO IS NULL" & vbCrLf
sConsulta = sConsulta & "               SET @EMPRESA_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=36 AND ERP=@ERP)" & vbCrLf
sConsulta = sConsulta & "       end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "--     Crea la parte WHERE con las opciones de filtrado" & vbCrLf
sConsulta = sConsulta & "--------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "   SET @WHERE=''" & vbCrLf
sConsulta = sConsulta & "----Filtros:" & vbCrLf
sConsulta = sConsulta & "   SET @WHERE=' AND LG.ERP=' +@ERP" & vbCrLf
sConsulta = sConsulta & "  " & vbCrLf
sConsulta = sConsulta & "   IF @ID_LOG IS not NULL" & vbCrLf
sConsulta = sConsulta & "       SET @WHERE=' AND LG.ID=@ID_LOG'" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "   IF @SENTIDO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   begin" & vbCrLf
sConsulta = sConsulta & "       IF @SENTIDO=1" & vbCrLf
sConsulta = sConsulta & "           SET @WHERE=@WHERE + ' AND LG.ORIGEN IN (0,2)'" & vbCrLf
sConsulta = sConsulta & "       ELSE IF @SENTIDO=2 --ENTRADA" & vbCrLf
sConsulta = sConsulta & "           SET @WHERE=@WHERE + ' AND LG.ORIGEN=3'" & vbCrLf
sConsulta = sConsulta & "       ELSE IF @SENTIDO=3" & vbCrLf
sConsulta = sConsulta & "           SET @WHERE=@WHERE + ' AND LG.ORIGEN IN (0,2,3)'" & vbCrLf
sConsulta = sConsulta & "   end" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "      SET @WHERE=@WHERE + ' AND LG.ORIGEN <> 1'" & vbCrLf
sConsulta = sConsulta & "      " & vbCrLf
sConsulta = sConsulta & "   IF @FECHA_DESDE IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       --Si es en sentido entrada, se busca por FECREC" & vbCrLf
sConsulta = sConsulta & "       --Si es en sentido salida, se busca por FECENV" & vbCrLf
sConsulta = sConsulta & "       SET @WHERE = @WHERE + ' AND ((LG.ORIGEN IN (0,2) AND LG.FECENV >= CONVERT(DATETIME,@FECHA_DESDE,@TIPOFECHA)) OR (LG.ORIGEN=3 AND LG.FECREC >=CONVERT(DATETIME,@FECHA_DESDE,@TIPOFECHA)))'" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @FECHA_HASTA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "       --Si es en sentido entrada, se busca por FECREC" & vbCrLf
sConsulta = sConsulta & "       --Si es en sentido salida, se busca por FECENV" & vbCrLf
sConsulta = sConsulta & "       SET @WHERE = @WHERE + ' AND ((LG.ORIGEN IN (0,2) AND LG.FECENV <= CONVERT(DATETIME,@FECHA_HASTA,@TIPOFECHA)) OR (LG.ORIGEN=3 AND LG.FECREC <=CONVERT(DATETIME,@FECHA_HASTA,@TIPOFECHA)))'" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "   IF @ESTADO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "       SET @WHERE=@WHERE + ' AND LG.ESTADO=@ESTADO'" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "         " & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   --Para Pedidos y recepciones:" & vbCrLf
sConsulta = sConsulta & "   SET @WHEREPEDIDO=''" & vbCrLf
sConsulta = sConsulta & "   SET @WHEREPEDIDO_ENRECEP=''" & vbCrLf
sConsulta = sConsulta & "   IF @ANYO_PEDIDO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   begin" & vbCrLf
sConsulta = sConsulta & "       SET @WHEREPEDIDO=@WHEREPEDIDO + ' AND LOE.ANYO=@ANYO_PEDIDO'" & vbCrLf
sConsulta = sConsulta & "       SET @WHEREPEDIDO_ENRECEP = @WHEREPEDIDO_ENRECEP +  ' AND O.ANYO=@ANYO_PEDIDO'" & vbCrLf
sConsulta = sConsulta & "    end" & vbCrLf
sConsulta = sConsulta & "   IF @NUM_PEDIDO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   begin" & vbCrLf
sConsulta = sConsulta & "       SET @WHEREPEDIDO=@WHEREPEDIDO + ' AND LOE.NUM_PEDIDO=@NUM_PEDIDO'  " & vbCrLf
sConsulta = sConsulta & "       SET @WHEREPEDIDO_ENRECEP = @WHEREPEDIDO_ENRECEP +  ' AND P.NUM=@NUM_PEDIDO'" & vbCrLf
sConsulta = sConsulta & "    end" & vbCrLf
sConsulta = sConsulta & "   IF @NUM_ORDEN IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   begin" & vbCrLf
sConsulta = sConsulta & "       SET @WHEREPEDIDO=@WHEREPEDIDO + ' AND LOE.NUM_ORDEN=@NUM_ORDEN'   " & vbCrLf
sConsulta = sConsulta & "       SET @WHEREPEDIDO_ENRECEP = @WHEREPEDIDO_ENRECEP +  ' AND O.NUM=@NUM_ORDEN'" & vbCrLf
sConsulta = sConsulta & "    end" & vbCrLf
sConsulta = sConsulta & "   --Para Procesos, adjudicaciones, asignaci�n de proveedores y recepciones de ofertas:" & vbCrLf
sConsulta = sConsulta & "   SET @WHEREPROCE=''" & vbCrLf
sConsulta = sConsulta & "   IF @ANYO_PROCE IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "       SET @WHEREPROCE=@WHEREPROCE + ' AND L.ANYO=@ANYO_PROCE'" & vbCrLf
sConsulta = sConsulta & "   IF @GMN1_PROCE  IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "      IF @TABLA=23 --Es un proceso" & vbCrLf
sConsulta = sConsulta & "           SET @WHEREPROCE=@WHEREPROCE + ' AND L.GMN1=@GMN1_PROCE'" & vbCrLf
sConsulta = sConsulta & "      ELSE --Es una adjudicaci�n" & vbCrLf
sConsulta = sConsulta & "           SET @WHEREPROCE=@WHEREPROCE + ' AND L.GMN1_PROCE=@GMN1_PROCE'" & vbCrLf
sConsulta = sConsulta & "       end" & vbCrLf
sConsulta = sConsulta & "   IF @COD_PROCE   IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "      begin" & vbCrLf
sConsulta = sConsulta & "      IF @TABLA=23 --Es un proceso" & vbCrLf
sConsulta = sConsulta & "           SET @WHEREPROCE=@WHEREPROCE + ' AND L.COD=@COD_PROCE'" & vbCrLf
sConsulta = sConsulta & "      ELSE --Es una adjudicaci�n" & vbCrLf
sConsulta = sConsulta & "           SET @WHEREPROCE=@WHEREPROCE + ' AND L.PROCE=@COD_PROCE'" & vbCrLf
sConsulta = sConsulta & "      end" & vbCrLf
sConsulta = sConsulta & "   --Filtro por codigo ERP  " & vbCrLf
sConsulta = sConsulta & "   IF @RECEP_ERP IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "       SET @WHERE = @WHERE + ' AND COD_ERP=@RECEP_ERP'" & vbCrLf
sConsulta = sConsulta & "   IF @COD_ERP IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "       IF @TABLA=12 OR @TABLA=14 --Es el codigo de pedido, lo incluye en @WHEREPEDIDO_ENRECEP" & vbCrLf
sConsulta = sConsulta & "           SET @WHEREPEDIDO_ENRECEP = @WHEREPEDIDO_ENRECEP + ' AND O.NUM_PED_ERP=@COD_ERP'  " & vbCrLf
sConsulta = sConsulta & "       ELSE" & vbCrLf
sConsulta = sConsulta & "           SET @WHERE = @WHERE + ' AND LG.COD_ERP=@COD_ERP'" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "         " & vbCrLf
sConsulta = sConsulta & "   --Para filtrar por c�digo: Siempre es L.COD excepto en los casos indicados" & vbCrLf
sConsulta = sConsulta & "   SET @WHERECOD=''" & vbCrLf
sConsulta = sConsulta & "   if @COD_FS is not null" & vbCrLf
sConsulta = sConsulta & "   begin " & vbCrLf
sConsulta = sConsulta & "       IF @TABLA=15" & vbCrLf
sConsulta = sConsulta & "      BEGIN" & vbCrLf
sConsulta = sConsulta & "           SET @GMN1 = SUBSTRING(@COD_FS, 0, CHARINDEX('-' ,@COD_FS))" & vbCrLf
sConsulta = sConsulta & "           SET @COD_FS = SUBSTRING(@COD_FS, CHARINDEX('-' ,@COD_FS) + 1, LEN(@COD_FS))" & vbCrLf
sConsulta = sConsulta & "           SET @GMN2 = SUBSTRING(@COD_FS, 0, CHARINDEX('-' ,@COD_FS))" & vbCrLf
sConsulta = sConsulta & "           SET @COD_FS = SUBSTRING(@COD_FS, CHARINDEX('-' ,@COD_FS) + 1, LEN(@COD_FS))" & vbCrLf
sConsulta = sConsulta & "           SET @GMN3 = SUBSTRING(@COD_FS, 0, CHARINDEX('-' ,@COD_FS))" & vbCrLf
sConsulta = sConsulta & "           SET @COD_FS = SUBSTRING(@COD_FS, CHARINDEX('-' ,@COD_FS) + 1, LEN(@COD_FS))" & vbCrLf
sConsulta = sConsulta & "           IF LEN(@GMN1) > 0" & vbCrLf
sConsulta = sConsulta & "               SET @WHERECOD=@WHERECOD + ' AND LGMN.GMN1='''+@GMN1+'''' " & vbCrLf
sConsulta = sConsulta & "           IF LEN(@GMN2) > 0" & vbCrLf
sConsulta = sConsulta & "               SET @WHERECOD=@WHERECOD + ' AND LGMN.GMN2='''+@GMN2+''''    " & vbCrLf
sConsulta = sConsulta & "           IF LEN(@GMN3) > 0" & vbCrLf
sConsulta = sConsulta & "               SET @WHERECOD=@WHERECOD + ' AND LGMN.GMN3='''+@GMN3+''''" & vbCrLf
sConsulta = sConsulta & "           IF LEN(@COD_FS) > 0" & vbCrLf
sConsulta = sConsulta & "               SET @WHERECOD=@WHERECOD + ' AND LGMN.GMN4='''+@COD_FS+''''" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "       ELSE IF @TABLA=12  " & vbCrLf
sConsulta = sConsulta & "           SET @WHERECOD=@WHERECOD + ' AND LR.ALBARAN=@COD_FS' " & vbCrLf
sConsulta = sConsulta & "       ELSE IF @TABLA=14 " & vbCrLf
sConsulta = sConsulta & "           SET @WHERECOD=@WHERECOD + ' AND LR.ALBARAN=@COD_FS' " & vbCrLf
sConsulta = sConsulta & "       ELSE IF @TABLA=17  ---HABR�A QUE FILTRAR POR TODOS LOS NIVELES" & vbCrLf
sConsulta = sConsulta & "           SET @WHERECOD=@WHERECOD + ' AND L.COD_CAL1=@COD_FS' " & vbCrLf
sConsulta = sConsulta & "       ELSE IF @TABLA=18 " & vbCrLf
sConsulta = sConsulta & "           SET @WHERECOD=@WHERECOD + ' AND L.INSTANCIA=@COD_FS ' " & vbCrLf
sConsulta = sConsulta & "       ELSE IF @TABLA=19 OR @TABLA=20 OR @TABLA=21 OR @TABLA=22  OR  @TABLA=34  ---HABR͍A QUE FILTRAR POR TODOS LOS PRES5_NIVx!" & vbCrLf
sConsulta = sConsulta & "           SET @WHERECOD=@WHERECOD + ' AND (L.PRES1=''' + @COD_FS + ''' OR L.PRES2=''' + @COD_FS + ''' OR L.PRES3=''' + @COD_FS + ''' OR L.PRES4=''' + @COD_FS + ''')'" & vbCrLf
sConsulta = sConsulta & "       ELSE IF @TABLA=29---HABR͍A QUE FILTRAR POR TODOS LOS UONx!" & vbCrLf
sConsulta = sConsulta & "           SET @WHERECOD=@WHERECOD + ' AND L.UON1=@COD_FS' " & vbCrLf
sConsulta = sConsulta & "       ELSE IF @TABLA=32" & vbCrLf
sConsulta = sConsulta & "           SET @WHERECOD=@WHERECOD + ' AND L.NUM=@COD_FS' " & vbCrLf
sConsulta = sConsulta & "       ELSE IF @TABLA=33" & vbCrLf
sConsulta = sConsulta & "           SET @WHERECOD=@WHERECOD + ' AND L.NUM=''' + @COD_FS + ''''" & vbCrLf
sConsulta = sConsulta & "       ELSE IF @TABLA=35" & vbCrLf
sConsulta = sConsulta & "           SET @WHERECOD=@WHERECOD + ' AND L.CENTRO_SM=@COD_FS' " & vbCrLf
sConsulta = sConsulta & "       ELSE IF @TABLA=36 " & vbCrLf
sConsulta = sConsulta & "           SET @WHERECOD=@WHERECOD + ' AND L.SOCIEDAD=@COD_FS' " & vbCrLf
sConsulta = sConsulta & "       ELSE IF @TABLA=101  --�EL NOMBRE DEL CONTACTO????" & vbCrLf
sConsulta = sConsulta & "           SET @WHERECOD=@WHERECOD + ' AND L.NOM=@COD_FS' " & vbCrLf
sConsulta = sConsulta & "       ELSE" & vbCrLf
sConsulta = sConsulta & "           SET @WHERECOD=@WHERECOD + ' AND L.COD=''' + @COD_FS + ''''" & vbCrLf
sConsulta = sConsulta & "   end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-------" & vbCrLf
sConsulta = sConsulta & "DECLARE @PARAM NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "SET @PARAM = N'@ERP VARCHAR(10), @TABLA smallint, " & vbCrLf
sConsulta = sConsulta & "   @COD_FS NVARCHAR(100), @COD_ERP NVARCHAR(100), @RECEP_ERP NVARCHAR(1000)," & vbCrLf
sConsulta = sConsulta & "   @ANYO_PROCE SMALLINT, @GMN1_PROCE VARCHAR(10), @COD_PROCE INT," & vbCrLf
sConsulta = sConsulta & "   @ANYO_PEDIDO SMALLINT, @NUM_PEDIDO INT, @NUM_ORDEN INT," & vbCrLf
sConsulta = sConsulta & "   @ID_LOG INTEGER, @ACCION VARCHAR(1), @ESTADO VARCHAR(1), " & vbCrLf
sConsulta = sConsulta & "   @FECHA_DESDE DATETIME, @FECHA_HASTA DATETIME,@TIPOFECHA INT'" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "-------" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "--     Crea la tabla temporal #T_LOG_GRAL e inserta los registros de LOG_GRAL con las opciones de filtrado" & vbCrLf
sConsulta = sConsulta & "-------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE #T_LOG_GRAL (ID INT, ID_TABLA INT, TABLA TINYINT," & vbCrLf
sConsulta = sConsulta & "ESTADO NVARCHAR(1) COLLATE SQL_Latin1_General_CP1_CI_AS," & vbCrLf
sConsulta = sConsulta & "ORIGEN TINYINT, TEXTO NVARCHAR(MAX), ARG1 NVARCHAR(MAX), ARG2 NVARCHAR(MAX)," & vbCrLf
sConsulta = sConsulta & "REINTENTOS TINYINT, FECENV DATETIME, FECREC DATETIME," & vbCrLf
sConsulta = sConsulta & "ERP NVARCHAR(20), NOM_FICHERO NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS, " & vbCrLf
sConsulta = sConsulta & "NOM_RECIBO NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS, " & vbCrLf
sConsulta = sConsulta & "COD_ERP NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @CONSULTA= 'INSERT INTO #T_LOG_GRAL" & vbCrLf
sConsulta = sConsulta & "SELECT ID,ID_TABLA,TABLA,ESTADO,ORIGEN,TEXTO,ARG1,ARG2,REINTENTOS,FECENV,FECREC,ERP,NOM_FICHERO," & vbCrLf
sConsulta = sConsulta & "NOM_RECIBO,COD_ERP FROM LOG_GRAL LG WITH(NOLOCK) WHERE 1=1 ' + @WHERE" & vbCrLf
sConsulta = sConsulta & "IF NOT(@TABLA=0)" & vbCrLf
sConsulta = sConsulta & "   SET @CONSULTA=@CONSULTA + ' AND LG.TABLA=@TABLA'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @CONSULTA,@PARAM,@ERP=@ERP, @TABLA=@TABLA, " & vbCrLf
sConsulta = sConsulta & "   @COD_FS=@COD_FS, @COD_ERP=@COD_ERP, @RECEP_ERP=@RECEP_ERP," & vbCrLf
sConsulta = sConsulta & "   @ANYO_PROCE=@ANYO_PROCE, @GMN1_PROCE=@GMN1_PROCE, @COD_PROCE=@COD_PROCE," & vbCrLf
sConsulta = sConsulta & "   @ANYO_PEDIDO=@ANYO_PEDIDO, @NUM_PEDIDO=@NUM_PEDIDO, @NUM_ORDEN=@NUM_ORDEN," & vbCrLf
sConsulta = sConsulta & "   @ID_LOG=@ID_LOG, @ACCION=@ACCION, @ESTADO=@ESTADO,@FECHA_DESDE=@FECHA_DESDE,@FECHA_HASTA=@FECHA_HASTA,@TIPOFECHA=@TIPOFECHA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "-- Crea la tabla temporal #T_CONSULTA donde se van a guardar las subconsultas" & vbCrLf
sConsulta = sConsulta & "-------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE #T_CONSULTA (ERP NVARCHAR(20) COLLATE SQL_Latin1_General_CP1_CI_AS, ENTIDAD TINYINT, IDENTIF INT, ESTADO NVARCHAR(1) COLLATE SQL_Latin1_General_CP1_CI_AS," & vbCrLf
sConsulta = sConsulta & "TEXTO NVARCHAR(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS, ARG1 NVARCHAR(MAX), ARG2 NVARCHAR(MAX), FECENV DATETIME," & vbCrLf
sConsulta = sConsulta & "FECREC DATETIME, NOM_FICHERO NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS, NOM_RECIBO NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS, ACCION NVARCHAR(1) COLLATE SQL_Latin1_General_CP1_CI_AS," & vbCrLf
sConsulta = sConsulta & "COD NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS, COD_ERP NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS, USU NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS, INFO NVARCHAR(200) COLLATE SQL_Latin1_General_CP1_CI_AS)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  " & vbCrLf
sConsulta = sConsulta & "---------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "-- Actualizamos la @WHERE con los filtros que NO son de LOG_GRAL, esto si lo hacemos antes de la creaci�n de la tabla" & vbCrLf
sConsulta = sConsulta & "-- temporal nos falla el script (La INSERT de dicha tabla solo tiene en cuenta LOG_GRAL)." & vbCrLf
sConsulta = sConsulta & "---------------------------------------------------------------------------------------------------------------------  " & vbCrLf
sConsulta = sConsulta & "  IF @ACCION IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "       SET @WHERE = @WHERE + ' AND ACCION=@ACCION' " & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "---------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "--             CONSULTA " & vbCrLf
sConsulta = sConsulta & "--No se muestran las tablas externas (LOG_TABLAS_EXTERNAS), recepciones de ofertas ni asignaci�n de proveedores" & vbCrLf
sConsulta = sConsulta & "--------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "IF @MON_ACTIVA=1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @CONSULTA= 'INSERT INTO #T_CONSULTA SELECT " & vbCrLf
sConsulta = sConsulta & "   LG.ERP AS ERP, LG.TABLA AS ENTIDAD, LG.ID AS IDENTIF, LG.ESTADO, LG.TEXTO, LG.ARG1, LG.ARG2,LG.FECENV, LG.FECREC, NOM_FICHERO , NOM_RECIBO," & vbCrLf
sConsulta = sConsulta & "   L.ACCION , L.COD , LG.COD_ERP , L.USU , '''' INFO" & vbCrLf
sConsulta = sConsulta & "   FROM #T_LOG_GRAL LG WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN LOG_MON L WITH(NOLOCK) ON LG.ID_TABLA=L.ID AND LG.TABLA=1" & vbCrLf
sConsulta = sConsulta & "   WHERE 1=1' + @WHERE + @WHERECOD " & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @CONSULTA,@PARAM,@ERP=@ERP, @TABLA=@TABLA, " & vbCrLf
sConsulta = sConsulta & "   @COD_FS=@COD_FS, @COD_ERP=@COD_ERP, @RECEP_ERP=@RECEP_ERP," & vbCrLf
sConsulta = sConsulta & "   @ANYO_PROCE=@ANYO_PROCE, @GMN1_PROCE=@GMN1_PROCE, @COD_PROCE=@COD_PROCE," & vbCrLf
sConsulta = sConsulta & "   @ANYO_PEDIDO=@ANYO_PEDIDO, @NUM_PEDIDO=@NUM_PEDIDO, @NUM_ORDEN=@NUM_ORDEN," & vbCrLf
sConsulta = sConsulta & "   @ID_LOG=@ID_LOG, @ACCION=@ACCION, @ESTADO=@ESTADO,@FECHA_DESDE=@FECHA_DESDE,@FECHA_HASTA=@FECHA_HASTA,@TIPOFECHA=@TIPOFECHA" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @PAI_ACTIVA=1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @CONSULTA= 'INSERT INTO #T_CONSULTA SELECT " & vbCrLf
sConsulta = sConsulta & "   LG.ERP AS ERP, LG.TABLA AS ENTIDAD, LG.ID AS IDENTIF, LG.ESTADO, LG.TEXTO, LG.ARG1, LG.ARG2,LG.FECENV, LG.FECREC, NOM_FICHERO , NOM_RECIBO," & vbCrLf
sConsulta = sConsulta & "   L.ACCION , L.COD , LG.COD_ERP , L.USU , '''' INFO" & vbCrLf
sConsulta = sConsulta & "   FROM #T_LOG_GRAL LG WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN LOG_PAI L WITH(NOLOCK) ON LG.ID_TABLA=L.ID AND LG.TABLA=2" & vbCrLf
sConsulta = sConsulta & "   WHERE 1=1' + @WHERE + @WHERECOD" & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @CONSULTA,@PARAM,@ERP=@ERP, @TABLA=@TABLA, " & vbCrLf
sConsulta = sConsulta & "   @COD_FS=@COD_FS, @COD_ERP=@COD_ERP, @RECEP_ERP=@RECEP_ERP," & vbCrLf
sConsulta = sConsulta & "   @ANYO_PROCE=@ANYO_PROCE, @GMN1_PROCE=@GMN1_PROCE, @COD_PROCE=@COD_PROCE," & vbCrLf
sConsulta = sConsulta & "   @ANYO_PEDIDO=@ANYO_PEDIDO, @NUM_PEDIDO=@NUM_PEDIDO, @NUM_ORDEN=@NUM_ORDEN," & vbCrLf
sConsulta = sConsulta & "   @ID_LOG=@ID_LOG, @ACCION=@ACCION, @ESTADO=@ESTADO,@FECHA_DESDE=@FECHA_DESDE,@FECHA_HASTA=@FECHA_HASTA,@TIPOFECHA=@TIPOFECHA" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @PROVI_ACTIVA=1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @CONSULTA= 'INSERT INTO #T_CONSULTA SELECT " & vbCrLf
sConsulta = sConsulta & "   LG.ERP AS ERP, LG.TABLA AS ENTIDAD, LG.ID AS IDENTIF, LG.ESTADO, LG.TEXTO, LG.ARG1, LG.ARG2,LG.FECENV, LG.FECREC, NOM_FICHERO , NOM_RECIBO," & vbCrLf
sConsulta = sConsulta & "   L.ACCION , L.COD , LG.COD_ERP , L.USU  , '''' INFO" & vbCrLf
sConsulta = sConsulta & "   FROM #T_LOG_GRAL LG WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN LOG_PROVI L WITH(NOLOCK) ON LG.ID_TABLA=L.ID AND LG.TABLA=3" & vbCrLf
sConsulta = sConsulta & "   WHERE 1=1' + @WHERE + @WHERECOD    " & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @CONSULTA,@PARAM,@ERP=@ERP, @TABLA=@TABLA, " & vbCrLf
sConsulta = sConsulta & "   @COD_FS=@COD_FS, @COD_ERP=@COD_ERP, @RECEP_ERP=@RECEP_ERP," & vbCrLf
sConsulta = sConsulta & "   @ANYO_PROCE=@ANYO_PROCE, @GMN1_PROCE=@GMN1_PROCE, @COD_PROCE=@COD_PROCE," & vbCrLf
sConsulta = sConsulta & "   @ANYO_PEDIDO=@ANYO_PEDIDO, @NUM_PEDIDO=@NUM_PEDIDO, @NUM_ORDEN=@NUM_ORDEN," & vbCrLf
sConsulta = sConsulta & "   @ID_LOG=@ID_LOG, @ACCION=@ACCION, @ESTADO=@ESTADO,@FECHA_DESDE=@FECHA_DESDE,@FECHA_HASTA=@FECHA_HASTA,@TIPOFECHA=@TIPOFECHA" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "IF @PAG_ACTIVA=1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @CONSULTA= 'INSERT INTO #T_CONSULTA SELECT " & vbCrLf
sConsulta = sConsulta & "   LG.ERP AS ERP, LG.TABLA AS ENTIDAD, LG.ID AS IDENTIF, LG.ESTADO, LG.TEXTO, LG.ARG1, LG.ARG2,LG.FECENV, LG.FECREC, NOM_FICHERO , NOM_RECIBO," & vbCrLf
sConsulta = sConsulta & "   L.ACCION , L.COD , LG.COD_ERP , L.USU  , '''' INFO" & vbCrLf
sConsulta = sConsulta & "   FROM #T_LOG_GRAL LG WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN LOG_PAG L WITH(NOLOCK) ON LG.ID_TABLA=L.ID AND LG.TABLA=4" & vbCrLf
sConsulta = sConsulta & "   WHERE 1=1' + @WHERE + @WHERECOD " & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @CONSULTA,@PARAM,@ERP=@ERP, @TABLA=@TABLA, " & vbCrLf
sConsulta = sConsulta & "   @COD_FS=@COD_FS, @COD_ERP=@COD_ERP, @RECEP_ERP=@RECEP_ERP," & vbCrLf
sConsulta = sConsulta & "   @ANYO_PROCE=@ANYO_PROCE, @GMN1_PROCE=@GMN1_PROCE, @COD_PROCE=@COD_PROCE," & vbCrLf
sConsulta = sConsulta & "   @ANYO_PEDIDO=@ANYO_PEDIDO, @NUM_PEDIDO=@NUM_PEDIDO, @NUM_ORDEN=@NUM_ORDEN," & vbCrLf
sConsulta = sConsulta & "   @ID_LOG=@ID_LOG, @ACCION=@ACCION, @ESTADO=@ESTADO,@FECHA_DESDE=@FECHA_DESDE,@FECHA_HASTA=@FECHA_HASTA,@TIPOFECHA=@TIPOFECHA" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "IF @DEST_ACTIVA=1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @CONSULTA= 'INSERT INTO #T_CONSULTA SELECT " & vbCrLf
sConsulta = sConsulta & "   LG.ERP AS ERP, LG.TABLA AS ENTIDAD, LG.ID AS IDENTIF, LG.ESTADO, LG.TEXTO, LG.ARG1, LG.ARG2,LG.FECENV, LG.FECREC, NOM_FICHERO , NOM_RECIBO," & vbCrLf
sConsulta = sConsulta & "   L.ACCION , L.COD , LG.COD_ERP , L.USU  , '''' INFO" & vbCrLf
sConsulta = sConsulta & "   FROM #T_LOG_GRAL LG WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN LOG_DEST L WITH(NOLOCK) ON LG.ID_TABLA=L.ID AND LG.TABLA=5" & vbCrLf
sConsulta = sConsulta & "   WHERE 1=1' + @WHERE + @WHERECOD " & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @CONSULTA,@PARAM,@ERP=@ERP, @TABLA=@TABLA, " & vbCrLf
sConsulta = sConsulta & "   @COD_FS=@COD_FS, @COD_ERP=@COD_ERP, @RECEP_ERP=@RECEP_ERP," & vbCrLf
sConsulta = sConsulta & "   @ANYO_PROCE=@ANYO_PROCE, @GMN1_PROCE=@GMN1_PROCE, @COD_PROCE=@COD_PROCE," & vbCrLf
sConsulta = sConsulta & "   @ANYO_PEDIDO=@ANYO_PEDIDO, @NUM_PEDIDO=@NUM_PEDIDO, @NUM_ORDEN=@NUM_ORDEN," & vbCrLf
sConsulta = sConsulta & "   @ID_LOG=@ID_LOG, @ACCION=@ACCION, @ESTADO=@ESTADO,@FECHA_DESDE=@FECHA_DESDE,@FECHA_HASTA=@FECHA_HASTA,@TIPOFECHA=@TIPOFECHA" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "IF @UNI_ACTIVA=1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @CONSULTA= 'INSERT INTO #T_CONSULTA SELECT " & vbCrLf
sConsulta = sConsulta & "   LG.ERP AS ERP, LG.TABLA AS ENTIDAD, LG.ID AS IDENTIF, LG.ESTADO, LG.TEXTO, LG.ARG1, LG.ARG2,LG.FECENV, LG.FECREC, NOM_FICHERO , NOM_RECIBO," & vbCrLf
sConsulta = sConsulta & "   L.ACCION , L.COD ,LG.COD_ERP , L.USU , '''' INFO" & vbCrLf
sConsulta = sConsulta & "   FROM #T_LOG_GRAL LG WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN LOG_UNI L WITH(NOLOCK) ON LG.ID_TABLA=L.ID AND LG.TABLA=6" & vbCrLf
sConsulta = sConsulta & "   WHERE 1=1' + @WHERE + @WHERECOD " & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @CONSULTA,@PARAM,@ERP=@ERP, @TABLA=@TABLA, " & vbCrLf
sConsulta = sConsulta & "   @COD_FS=@COD_FS, @COD_ERP=@COD_ERP, @RECEP_ERP=@RECEP_ERP," & vbCrLf
sConsulta = sConsulta & "   @ANYO_PROCE=@ANYO_PROCE, @GMN1_PROCE=@GMN1_PROCE, @COD_PROCE=@COD_PROCE," & vbCrLf
sConsulta = sConsulta & "   @ANYO_PEDIDO=@ANYO_PEDIDO, @NUM_PEDIDO=@NUM_PEDIDO, @NUM_ORDEN=@NUM_ORDEN," & vbCrLf
sConsulta = sConsulta & "   @ID_LOG=@ID_LOG, @ACCION=@ACCION, @ESTADO=@ESTADO,@FECHA_DESDE=@FECHA_DESDE,@FECHA_HASTA=@FECHA_HASTA,@TIPOFECHA=@TIPOFECHA" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "IF @ART4_ACTIVA=1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @CONSULTA= 'INSERT INTO #T_CONSULTA SELECT " & vbCrLf
sConsulta = sConsulta & "   LG.ERP AS ERP, LG.TABLA AS ENTIDAD, LG.ID AS IDENTIF, LG.ESTADO, LG.TEXTO, LG.ARG1, LG.ARG2,LG.FECENV, LG.FECREC, NOM_FICHERO , NOM_RECIBO," & vbCrLf
sConsulta = sConsulta & "   L.ACCION , L.COD , LG.COD_ERP , L.USU , '''' INFO" & vbCrLf
sConsulta = sConsulta & "   FROM #T_LOG_GRAL LG WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN LOG_ART4 L WITH(NOLOCK) ON LG.ID_TABLA=L.ID " & vbCrLf
sConsulta = sConsulta & "   WHERE LG.TABLA=7' + @WHERE + @WHERECOD " & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @CONSULTA,@PARAM,@ERP=@ERP, @TABLA=@TABLA, " & vbCrLf
sConsulta = sConsulta & "   @COD_FS=@COD_FS, @COD_ERP=@COD_ERP, @RECEP_ERP=@RECEP_ERP," & vbCrLf
sConsulta = sConsulta & "   @ANYO_PROCE=@ANYO_PROCE, @GMN1_PROCE=@GMN1_PROCE, @COD_PROCE=@COD_PROCE," & vbCrLf
sConsulta = sConsulta & "   @ANYO_PEDIDO=@ANYO_PEDIDO, @NUM_PEDIDO=@NUM_PEDIDO, @NUM_ORDEN=@NUM_ORDEN," & vbCrLf
sConsulta = sConsulta & "   @ID_LOG=@ID_LOG, @ACCION=@ACCION, @ESTADO=@ESTADO,@FECHA_DESDE=@FECHA_DESDE,@FECHA_HASTA=@FECHA_HASTA,@TIPOFECHA=@TIPOFECHA" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @PROVE_ACTIVA=1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @CONSULTA= 'INSERT INTO #T_CONSULTA SELECT " & vbCrLf
sConsulta = sConsulta & "   LG.ERP AS ERP, LG.TABLA AS ENTIDAD, LG.ID AS IDENTIF, LG.ESTADO, LG.TEXTO, LG.ARG1, LG.ARG2,LG.FECENV, LG.FECREC, NOM_FICHERO , NOM_RECIBO," & vbCrLf
sConsulta = sConsulta & "   L.ACCION , L.COD , LG.COD_ERP , L.USU , '''' INFO" & vbCrLf
sConsulta = sConsulta & "   FROM #T_LOG_GRAL LG WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN LOG_PROVE L WITH(NOLOCK) ON LG.ID_TABLA=L.ID AND LG.TABLA=8" & vbCrLf
sConsulta = sConsulta & "   WHERE 1=1' + @WHERE + @WHERECOD    " & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @CONSULTA,@PARAM,@ERP=@ERP, @TABLA=@TABLA, " & vbCrLf
sConsulta = sConsulta & "   @COD_FS=@COD_FS, @COD_ERP=@COD_ERP, @RECEP_ERP=@RECEP_ERP," & vbCrLf
sConsulta = sConsulta & "   @ANYO_PROCE=@ANYO_PROCE, @GMN1_PROCE=@GMN1_PROCE, @COD_PROCE=@COD_PROCE," & vbCrLf
sConsulta = sConsulta & "   @ANYO_PEDIDO=@ANYO_PEDIDO, @NUM_PEDIDO=@NUM_PEDIDO, @NUM_ORDEN=@NUM_ORDEN," & vbCrLf
sConsulta = sConsulta & "   @ID_LOG=@ID_LOG, @ACCION=@ACCION, @ESTADO=@ESTADO,@FECHA_DESDE=@FECHA_DESDE,@FECHA_HASTA=@FECHA_HASTA,@TIPOFECHA=@TIPOFECHA" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CON_ACTIVA =1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @CONSULTA= 'INSERT INTO #T_CONSULTA SELECT " & vbCrLf
sConsulta = sConsulta & "   LG.ERP AS ERP, LG.TABLA AS ENTIDAD, LG.ID AS IDENTIF, LG.ESTADO, LG.TEXTO, LG.ARG1, LG.ARG2,LG.FECENV, LG.FECREC, NOM_FICHERO , NOM_RECIBO," & vbCrLf
sConsulta = sConsulta & "   L.ACCION , L.NOM AS COD , LG.COD_ERP , L.USU , '''' INFO" & vbCrLf
sConsulta = sConsulta & "   FROM #T_LOG_GRAL LG WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN LOG_CON L WITH(NOLOCK) ON LG.ID_TABLA=L.ID AND LG.TABLA=101" & vbCrLf
sConsulta = sConsulta & "   WHERE 1=1' + @WHERE + @WHERECOD " & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @CONSULTA,@PARAM,@ERP=@ERP, @TABLA=@TABLA, " & vbCrLf
sConsulta = sConsulta & "   @COD_FS=@COD_FS, @COD_ERP=@COD_ERP, @RECEP_ERP=@RECEP_ERP," & vbCrLf
sConsulta = sConsulta & "   @ANYO_PROCE=@ANYO_PROCE, @GMN1_PROCE=@GMN1_PROCE, @COD_PROCE=@COD_PROCE," & vbCrLf
sConsulta = sConsulta & "   @ANYO_PEDIDO=@ANYO_PEDIDO, @NUM_PEDIDO=@NUM_PEDIDO, @NUM_ORDEN=@NUM_ORDEN," & vbCrLf
sConsulta = sConsulta & "   @ID_LOG=@ID_LOG, @ACCION=@ACCION, @ESTADO=@ESTADO,@FECHA_DESDE=@FECHA_DESDE,@FECHA_HASTA=@FECHA_HASTA,@TIPOFECHA=@TIPOFECHA" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @PROVE_ERP_ACTIVA =1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @CONSULTA= 'INSERT INTO #T_CONSULTA SELECT " & vbCrLf
sConsulta = sConsulta & "   LG.ERP AS ERP, LG.TABLA AS ENTIDAD, LG.ID AS IDENTIF, LG.ESTADO, LG.TEXTO, LG.ARG1, LG.ARG2,LG.FECENV, LG.FECREC, NOM_FICHERO , NOM_RECIBO," & vbCrLf
sConsulta = sConsulta & "   LPE.ACCION , LPE.NIF as COD , LG.COD_ERP , LPE.USU , '''' INFO" & vbCrLf
sConsulta = sConsulta & "   FROM #T_LOG_GRAL LG WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN LOG_PROVE_ERP LPE WITH(NOLOCK) ON LG.ID_TABLA=LPE.ID AND LG.TABLA=102" & vbCrLf
sConsulta = sConsulta & "   WHERE 1=1' + @WHERE    " & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @CONSULTA,@PARAM,@ERP=@ERP, @TABLA=@TABLA, " & vbCrLf
sConsulta = sConsulta & "   @COD_FS=@COD_FS, @COD_ERP=@COD_ERP, @RECEP_ERP=@RECEP_ERP," & vbCrLf
sConsulta = sConsulta & "   @ANYO_PROCE=@ANYO_PROCE, @GMN1_PROCE=@GMN1_PROCE, @COD_PROCE=@COD_PROCE," & vbCrLf
sConsulta = sConsulta & "   @ANYO_PEDIDO=@ANYO_PEDIDO, @NUM_PEDIDO=@NUM_PEDIDO, @NUM_ORDEN=@NUM_ORDEN," & vbCrLf
sConsulta = sConsulta & "   @ID_LOG=@ID_LOG, @ACCION=@ACCION, @ESTADO=@ESTADO,@FECHA_DESDE=@FECHA_DESDE,@FECHA_HASTA=@FECHA_HASTA,@TIPOFECHA=@TIPOFECHA" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "IF @ADJ_ACTIVA=1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @CONSULTA= 'INSERT INTO #T_CONSULTA SELECT " & vbCrLf
sConsulta = sConsulta & "   LG.ERP AS ERP, LG.TABLA AS ENTIDAD, LG.ID AS IDENTIF, LG.ESTADO, LG.TEXTO, LG.ARG1, LG.ARG2,LG.FECENV, LG.FECREC, NOM_FICHERO , NOM_RECIBO," & vbCrLf
sConsulta = sConsulta & "   L.ACCION , " & vbCrLf
sConsulta = sConsulta & "   cast(L.ANYO as varchar(4)) + ''/''+ L.GMN1_PROCE + ''/'' + cast(L.PROCE as varchar(10)) as COD, LG.cod_erp , L.USU ," & vbCrLf
sConsulta = sConsulta & "    L.PROVE + '' - '' + EMP.DEN AS INFO" & vbCrLf
sConsulta = sConsulta & "   FROM #T_LOG_GRAL LG WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN LOG_ITEM_ADJ L WITH(NOLOCK) ON LG.ID_TABLA=L.ID AND LG.TABLA=9" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN EMP WITH(NOLOCK) ON EMP.ID=L.EMPRESA" & vbCrLf
sConsulta = sConsulta & "   WHERE 1=1' + @WHERE + @WHEREPROCE " & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @CONSULTA,@PARAM,@ERP=@ERP, @TABLA=@TABLA, " & vbCrLf
sConsulta = sConsulta & "   @COD_FS=@COD_FS, @COD_ERP=@COD_ERP, @RECEP_ERP=@RECEP_ERP," & vbCrLf
sConsulta = sConsulta & "   @ANYO_PROCE=@ANYO_PROCE, @GMN1_PROCE=@GMN1_PROCE, @COD_PROCE=@COD_PROCE," & vbCrLf
sConsulta = sConsulta & "   @ANYO_PEDIDO=@ANYO_PEDIDO, @NUM_PEDIDO=@NUM_PEDIDO, @NUM_ORDEN=@NUM_ORDEN," & vbCrLf
sConsulta = sConsulta & "   @ID_LOG=@ID_LOG, @ACCION=@ACCION, @ESTADO=@ESTADO,@FECHA_DESDE=@FECHA_DESDE,@FECHA_HASTA=@FECHA_HASTA,@TIPOFECHA=@TIPOFECHA" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @PEDIDO_ACTIVA = 1" & vbCrLf
sConsulta = sConsulta & "begin" & vbCrLf
sConsulta = sConsulta & "   SET @CONSULTA= 'INSERT INTO #T_CONSULTA SELECT " & vbCrLf
sConsulta = sConsulta & "   LG.ERP AS ERP, LG.TABLA AS ENTIDAD, LG.ID AS IDENTIF, LG.ESTADO, LG.TEXTO, LG.ARG1, LG.ARG2,LG.FECENV, LG.FECREC, NOM_FICHERO , NOM_RECIBO," & vbCrLf
sConsulta = sConsulta & "   LOE.ACCION , CAST(LOE.ANYO AS VARCHAR(4)) + ''/''+ CAST(LOE.NUM_PEDIDO AS VARCHAR(20)) + ''/''+ CAST(LOE.NUM_ORDEN AS VARCHAR(20)) AS COD , " & vbCrLf
sConsulta = sConsulta & "   LG.COD_ERP , LOE.USU , '''' INFO" & vbCrLf
sConsulta = sConsulta & "   FROM #T_LOG_GRAL LG WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN LOG_ORDEN_ENTREGA LOE WITH(NOLOCK) ON LG.ID_TABLA=LOE.ID '" & vbCrLf
sConsulta = sConsulta & "   IF @TABLA=11 " & vbCrLf
sConsulta = sConsulta & "       SET @CONSULTA= @CONSULTA + 'AND LG.TABLA=11 '" & vbCrLf
sConsulta = sConsulta & "   ELSE IF @TABLA=13  " & vbCrLf
sConsulta = sConsulta & "       SET @CONSULTA= @CONSULTA +  'AND  LG.TABLA=13 '" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "       SET @CONSULTA= @CONSULTA + 'AND (LG.TABLA=11 or LG.TABLA=13)'" & vbCrLf
sConsulta = sConsulta & "   SET @CONSULTA= @CONSULTA + ' WHERE 1=1' + @WHERE + @WHEREPEDIDO " & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @CONSULTA,@PARAM,@ERP=@ERP, @TABLA=@TABLA, " & vbCrLf
sConsulta = sConsulta & "   @COD_FS=@COD_FS, @COD_ERP=@COD_ERP, @RECEP_ERP=@RECEP_ERP," & vbCrLf
sConsulta = sConsulta & "   @ANYO_PROCE=@ANYO_PROCE, @GMN1_PROCE=@GMN1_PROCE, @COD_PROCE=@COD_PROCE," & vbCrLf
sConsulta = sConsulta & "   @ANYO_PEDIDO=@ANYO_PEDIDO, @NUM_PEDIDO=@NUM_PEDIDO, @NUM_ORDEN=@NUM_ORDEN," & vbCrLf
sConsulta = sConsulta & "   @ID_LOG=@ID_LOG, @ACCION=@ACCION, @ESTADO=@ESTADO,@FECHA_DESDE=@FECHA_DESDE,@FECHA_HASTA=@FECHA_HASTA,@TIPOFECHA=@TIPOFECHA" & vbCrLf
sConsulta = sConsulta & "end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @RECEP_ACTIVA = 1" & vbCrLf
sConsulta = sConsulta & "  begin" & vbCrLf
sConsulta = sConsulta & "   SET @CONSULTA= 'INSERT INTO #T_CONSULTA SELECT " & vbCrLf
sConsulta = sConsulta & "   LG.ERP AS ERP, LG.TABLA AS ENTIDAD, LG.ID AS IDENTIF, LG.ESTADO, LG.TEXTO, LG.ARG1, LG.ARG2,LG.FECENV, LG.FECREC, NOM_FICHERO , NOM_RECIBO," & vbCrLf
sConsulta = sConsulta & "   LR.ACCION , LR.ALBARAN AS COD, LG.COD_ERP , LR.USU , " & vbCrLf
sConsulta = sConsulta & "   CAST(O.ANYO AS VARCHAR(4)) + ''/'' + CAST(P.NUM AS VARCHAR(20))  + ''/'' + CAST(O.NUM AS VARCHAR(20)) + ''  -  '' + O.NUM_PED_ERP AS INFO" & vbCrLf
sConsulta = sConsulta & "   FROM #T_LOG_GRAL LG WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN LOG_PEDIDO_RECEP LR WITH(NOLOCK) ON LG.ID_TABLA=LR.ID '" & vbCrLf
sConsulta = sConsulta & "   IF @TABLA=12 " & vbCrLf
sConsulta = sConsulta & "       SET @CONSULTA= @CONSULTA + 'AND LG.TABLA=12 '" & vbCrLf
sConsulta = sConsulta & "   ELSE IF @TABLA=13  " & vbCrLf
sConsulta = sConsulta & "       SET @CONSULTA= @CONSULTA +  'AND LG.TABLA=14 '" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "       SET @CONSULTA= @CONSULTA + 'AND (LG.TABLA=12 or LG.TABLA=14)'" & vbCrLf
sConsulta = sConsulta & "   SET @CONSULTA= @CONSULTA + '   LEFT JOIN ORDEN_ENTREGA O WITH(NOLOCK) ON O.ID=LR.ID_ORDEN_ENTREGA" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN PEDIDO P WITH(NOLOCK) ON P.ID=O.PEDIDO AND P.ID=LR.ID_PEDIDO" & vbCrLf
sConsulta = sConsulta & "   WHERE 1=1' + @WHERE + @WHERECOD + @WHEREPEDIDO_ENRECEP  " & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @CONSULTA,@PARAM,@ERP=@ERP, @TABLA=@TABLA, " & vbCrLf
sConsulta = sConsulta & "   @COD_FS=@COD_FS, @COD_ERP=@COD_ERP, @RECEP_ERP=@RECEP_ERP," & vbCrLf
sConsulta = sConsulta & "   @ANYO_PROCE=@ANYO_PROCE, @GMN1_PROCE=@GMN1_PROCE, @COD_PROCE=@COD_PROCE," & vbCrLf
sConsulta = sConsulta & "   @ANYO_PEDIDO=@ANYO_PEDIDO, @NUM_PEDIDO=@NUM_PEDIDO, @NUM_ORDEN=@NUM_ORDEN," & vbCrLf
sConsulta = sConsulta & "   @ID_LOG=@ID_LOG, @ACCION=@ACCION, @ESTADO=@ESTADO,@FECHA_DESDE=@FECHA_DESDE,@FECHA_HASTA=@FECHA_HASTA,@TIPOFECHA=@TIPOFECHA" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "IF @ESTRMAT_ACTIVA = 1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @CONSULTA= 'INSERT INTO #T_CONSULTA SELECT " & vbCrLf
sConsulta = sConsulta & "   LG.ERP AS ERP, LG.TABLA AS ENTIDAD, LG.ID AS IDENTIF, LG.ESTADO, LG.TEXTO, LG.ARG1, LG.ARG2,LG.FECENV, LG.FECREC, NOM_FICHERO , NOM_RECIBO," & vbCrLf
sConsulta = sConsulta & "   LGMN.ACCION, LGMN.GMN1 + ''-'' + LGMN.GMN2 + ''-'' + LGMN.GMN3 + ''-'' + LGMN.GMN4 AS COD , LG.COD_ERP , " & vbCrLf
sConsulta = sConsulta & "   LGMN.USU , '''' INFO" & vbCrLf
sConsulta = sConsulta & "   FROM #T_LOG_GRAL LG WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN LOG_GMN LGMN WITH(NOLOCK) ON LG.ID_TABLA=LGMN.ID AND LG.TABLA=15" & vbCrLf
sConsulta = sConsulta & "   WHERE 1=1' + @WHERE + @WHERECOD" & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @CONSULTA,@PARAM,@ERP=@ERP, @TABLA=@TABLA, " & vbCrLf
sConsulta = sConsulta & "   @COD_FS=@COD_FS, @COD_ERP=@COD_ERP, @RECEP_ERP=@RECEP_ERP," & vbCrLf
sConsulta = sConsulta & "   @ANYO_PROCE=@ANYO_PROCE, @GMN1_PROCE=@GMN1_PROCE, @COD_PROCE=@COD_PROCE," & vbCrLf
sConsulta = sConsulta & "   @ANYO_PEDIDO=@ANYO_PEDIDO, @NUM_PEDIDO=@NUM_PEDIDO, @NUM_ORDEN=@NUM_ORDEN," & vbCrLf
sConsulta = sConsulta & "   @ID_LOG=@ID_LOG, @ACCION=@ACCION, @ESTADO=@ESTADO,@FECHA_DESDE=@FECHA_DESDE,@FECHA_HASTA=@FECHA_HASTA,@TIPOFECHA=@TIPOFECHA" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CONSUMOS_ACTIVA = 1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @CONSULTA= 'INSERT INTO #T_CONSULTA SELECT " & vbCrLf
sConsulta = sConsulta & "   LG.ERP AS ERP, LG.TABLA AS ENTIDAD, LG.ID AS IDENTIF, LG.ESTADO, LG.TEXTO, LG.ARG1, LG.ARG2,LG.FECENV, LG.FECREC, NOM_FICHERO , NOM_RECIBO," & vbCrLf
sConsulta = sConsulta & "   L.ACCION , L.COD , LG.COD_ERP , L.USU , '''' INFO" & vbCrLf
sConsulta = sConsulta & "   FROM #T_LOG_GRAL LG WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN LOG_PRES_ART L WITH(NOLOCK) ON LG.ID_TABLA=L.ID AND LG.TABLA=16" & vbCrLf
sConsulta = sConsulta & "   WHERE 1=1' + @WHERE + @WHERECOD      " & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @CONSULTA,@PARAM,@ERP=@ERP, @TABLA=@TABLA, " & vbCrLf
sConsulta = sConsulta & "   @COD_FS=@COD_FS, @COD_ERP=@COD_ERP, @RECEP_ERP=@RECEP_ERP," & vbCrLf
sConsulta = sConsulta & "   @ANYO_PROCE=@ANYO_PROCE, @GMN1_PROCE=@GMN1_PROCE, @COD_PROCE=@COD_PROCE," & vbCrLf
sConsulta = sConsulta & "   @ANYO_PEDIDO=@ANYO_PEDIDO, @NUM_PEDIDO=@NUM_PEDIDO, @NUM_ORDEN=@NUM_ORDEN," & vbCrLf
sConsulta = sConsulta & "   @ID_LOG=@ID_LOG, @ACCION=@ACCION, @ESTADO=@ESTADO,@FECHA_DESDE=@FECHA_DESDE,@FECHA_HASTA=@FECHA_HASTA,@TIPOFECHA=@TIPOFECHA" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @VARCAL_ACTIVA = 1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @CONSULTA= 'INSERT INTO #T_CONSULTA SELECT " & vbCrLf
sConsulta = sConsulta & "   LG.ERP AS ERP, LG.TABLA AS ENTIDAD, LG.ID AS IDENTIF, LG.ESTADO, LG.TEXTO, LG.ARG1, LG.ARG2,LG.FECENV, LG.FECREC, NOM_FICHERO , NOM_RECIBO," & vbCrLf
sConsulta = sConsulta & "   L.ACCION , L.COD_CAL1 cod , LG.COD_ERP , L.USU , '''' INFO" & vbCrLf
sConsulta = sConsulta & "   FROM #T_LOG_GRAL LG WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN LOG_VAR_CAL L WITH(NOLOCK) ON LG.ID_TABLA=L.ID AND LG.TABLA=17" & vbCrLf
sConsulta = sConsulta & "   WHERE 1=1' + @WHERE + @WHERECOD    " & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @CONSULTA,@PARAM,@ERP=@ERP, @TABLA=@TABLA, " & vbCrLf
sConsulta = sConsulta & "   @COD_FS=@COD_FS, @COD_ERP=@COD_ERP, @RECEP_ERP=@RECEP_ERP," & vbCrLf
sConsulta = sConsulta & "   @ANYO_PROCE=@ANYO_PROCE, @GMN1_PROCE=@GMN1_PROCE, @COD_PROCE=@COD_PROCE," & vbCrLf
sConsulta = sConsulta & "   @ANYO_PEDIDO=@ANYO_PEDIDO, @NUM_PEDIDO=@NUM_PEDIDO, @NUM_ORDEN=@NUM_ORDEN," & vbCrLf
sConsulta = sConsulta & "   @ID_LOG=@ID_LOG, @ACCION=@ACCION, @ESTADO=@ESTADO,@FECHA_DESDE=@FECHA_DESDE,@FECHA_HASTA=@FECHA_HASTA,@TIPOFECHA=@TIPOFECHA" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @SOLICIT_ACTIVA = 1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @CONSULTA= 'INSERT INTO #T_CONSULTA SELECT " & vbCrLf
sConsulta = sConsulta & "   LG.ERP AS ERP, LG.TABLA AS ENTIDAD, LG.ID AS IDENTIF, LG.ESTADO, LG.TEXTO, LG.ARG1, LG.ARG2,LG.FECENV, LG.FECREC, NOM_FICHERO , NOM_RECIBO," & vbCrLf
sConsulta = sConsulta & "   L.ACCION , CAST(L.INSTANCIA AS VARCHAR(20)) as COD , LG.COD_ERP , L.USU  , SOLICITUD.COD AS INFO" & vbCrLf
sConsulta = sConsulta & "   FROM #T_LOG_GRAL LG WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN LOG_INSTANCIA L WITH(NOLOCK) ON LG.ID_TABLA=L.ID AND LG.TABLA=18" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN SOLICITUD WITH(NOLOCK) ON SOLICITUD.ID=L.SOLICITUD" & vbCrLf
sConsulta = sConsulta & "   WHERE 1=1' + @WHERE + @WHERECOD " & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @CONSULTA,@PARAM,@ERP=@ERP, @TABLA=@TABLA, " & vbCrLf
sConsulta = sConsulta & "   @COD_FS=@COD_FS, @COD_ERP=@COD_ERP, @RECEP_ERP=@RECEP_ERP," & vbCrLf
sConsulta = sConsulta & "   @ANYO_PROCE=@ANYO_PROCE, @GMN1_PROCE=@GMN1_PROCE, @COD_PROCE=@COD_PROCE," & vbCrLf
sConsulta = sConsulta & "   @ANYO_PEDIDO=@ANYO_PEDIDO, @NUM_PEDIDO=@NUM_PEDIDO, @NUM_ORDEN=@NUM_ORDEN," & vbCrLf
sConsulta = sConsulta & "   @ID_LOG=@ID_LOG, @ACCION=@ACCION, @ESTADO=@ESTADO,@FECHA_DESDE=@FECHA_DESDE,@FECHA_HASTA=@FECHA_HASTA,@TIPOFECHA=@TIPOFECHA" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @PRES_ACTIVA = 1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @CONSULTA= 'INSERT INTO #T_CONSULTA SELECT " & vbCrLf
sConsulta = sConsulta & "   LG.ERP AS ERP, LG.TABLA AS ENTIDAD, LG.ID AS IDENTIF, LG.ESTADO, LG.TEXTO, LG.ARG1, LG.ARG2,LG.FECENV, LG.FECREC, NOM_FICHERO , NOM_RECIBO," & vbCrLf
sConsulta = sConsulta & "   L.ACCION , L.PRES1 + ''-'' + L.PRES2 + ''-''  + L.PRES3 + ''-'' + L.PRES4 AS COD, LG.COD_ERP , L.USU  , '''' INFO" & vbCrLf
sConsulta = sConsulta & "   FROM #T_LOG_GRAL LG WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN LOG_PRES L WITH(NOLOCK) ON LG.ID_TABLA=L.ID AND (LG.TABLA=19 or LG.TABLA=20 or LG.TABLA=21 or LG.TABLA=22)" & vbCrLf
sConsulta = sConsulta & "   WHERE 1=1' + @WHERE  + @WHERECOD   " & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @CONSULTA,@PARAM,@ERP=@ERP, @TABLA=@TABLA, " & vbCrLf
sConsulta = sConsulta & "   @COD_FS=@COD_FS, @COD_ERP=@COD_ERP, @RECEP_ERP=@RECEP_ERP," & vbCrLf
sConsulta = sConsulta & "   @ANYO_PROCE=@ANYO_PROCE, @GMN1_PROCE=@GMN1_PROCE, @COD_PROCE=@COD_PROCE," & vbCrLf
sConsulta = sConsulta & "   @ANYO_PEDIDO=@ANYO_PEDIDO, @NUM_PEDIDO=@NUM_PEDIDO, @NUM_ORDEN=@NUM_ORDEN," & vbCrLf
sConsulta = sConsulta & "   @ID_LOG=@ID_LOG, @ACCION=@ACCION, @ESTADO=@ESTADO,@FECHA_DESDE=@FECHA_DESDE,@FECHA_HASTA=@FECHA_HASTA,@TIPOFECHA=@TIPOFECHA" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @PROCE_ACTIVA = 1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @CONSULTA= 'INSERT INTO #T_CONSULTA SELECT " & vbCrLf
sConsulta = sConsulta & "   LG.ERP AS ERP, LG.TABLA AS ENTIDAD, LG.ID AS IDENTIF, LG.ESTADO, LG.TEXTO, LG.ARG1, LG.ARG2,LG.FECENV, LG.FECREC, NOM_FICHERO , NOM_RECIBO," & vbCrLf
sConsulta = sConsulta & "   L.ACCION , CAST(L.ANYO AS VARCHAR(4)) + ''-'' + L.GMN1 + ''-'' + CAST(L.COD AS VARCHAR(10)) COD ," & vbCrLf
sConsulta = sConsulta & "    LG.COD_ERP , L.USU , '''' INFO " & vbCrLf
sConsulta = sConsulta & "   FROM #T_LOG_GRAL LG WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN LOG_PROCE L WITH(NOLOCK) ON LG.ID_TABLA=L.ID AND LG.TABLA=23" & vbCrLf
sConsulta = sConsulta & "   WHERE 1=1' + @WHERE + @WHEREPROCE" & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @CONSULTA,@PARAM,@ERP=@ERP, @TABLA=@TABLA, " & vbCrLf
sConsulta = sConsulta & "   @COD_FS=@COD_FS, @COD_ERP=@COD_ERP, @RECEP_ERP=@RECEP_ERP," & vbCrLf
sConsulta = sConsulta & "   @ANYO_PROCE=@ANYO_PROCE, @GMN1_PROCE=@GMN1_PROCE, @COD_PROCE=@COD_PROCE," & vbCrLf
sConsulta = sConsulta & "   @ANYO_PEDIDO=@ANYO_PEDIDO, @NUM_PEDIDO=@NUM_PEDIDO, @NUM_ORDEN=@NUM_ORDEN," & vbCrLf
sConsulta = sConsulta & "   @ID_LOG=@ID_LOG, @ACCION=@ACCION, @ESTADO=@ESTADO,@FECHA_DESDE=@FECHA_DESDE,@FECHA_HASTA=@FECHA_HASTA,@TIPOFECHA=@TIPOFECHA" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "IF @VIA_PAG_ACTIVA = 1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @CONSULTA= 'INSERT INTO #T_CONSULTA SELECT " & vbCrLf
sConsulta = sConsulta & "   LG.ERP AS ERP, LG.TABLA AS ENTIDAD, LG.ID AS IDENTIF, LG.ESTADO, LG.TEXTO, LG.ARG1, LG.ARG2,LG.FECENV, LG.FECREC, NOM_FICHERO , NOM_RECIBO," & vbCrLf
sConsulta = sConsulta & "   L.ACCION , L.COD , LG.COD_ERP , L.USU , '''' INFO" & vbCrLf
sConsulta = sConsulta & "   FROM #T_LOG_GRAL LG WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN LOG_VIA_PAG L WITH(NOLOCK) ON LG.ID_TABLA=L.ID AND LG.TABLA=25" & vbCrLf
sConsulta = sConsulta & "   WHERE 1=1' + @WHERE + @WHERECOD " & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @CONSULTA,@PARAM,@ERP=@ERP, @TABLA=@TABLA, " & vbCrLf
sConsulta = sConsulta & "   @COD_FS=@COD_FS, @COD_ERP=@COD_ERP, @RECEP_ERP=@RECEP_ERP," & vbCrLf
sConsulta = sConsulta & "   @ANYO_PROCE=@ANYO_PROCE, @GMN1_PROCE=@GMN1_PROCE, @COD_PROCE=@COD_PROCE," & vbCrLf
sConsulta = sConsulta & "   @ANYO_PEDIDO=@ANYO_PEDIDO, @NUM_PEDIDO=@NUM_PEDIDO, @NUM_ORDEN=@NUM_ORDEN," & vbCrLf
sConsulta = sConsulta & "   @ID_LOG=@ID_LOG, @ACCION=@ACCION, @ESTADO=@ESTADO,@FECHA_DESDE=@FECHA_DESDE,@FECHA_HASTA=@FECHA_HASTA,@TIPOFECHA=@TIPOFECHA" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @PPM_ACTIVA = 1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @CONSULTA= 'INSERT INTO #T_CONSULTA SELECT " & vbCrLf
sConsulta = sConsulta & "   LG.ERP AS ERP, LG.TABLA AS ENTIDAD, LG.ID AS IDENTIF, LG.ESTADO, LG.TEXTO, LG.ARG1, LG.ARG2,LG.FECENV, LG.FECREC, NOM_FICHERO , NOM_RECIBO," & vbCrLf
sConsulta = sConsulta & "   LPPM.ACCION , " & vbCrLf
sConsulta = sConsulta & "   cast(ROUND(LPPM.piezas_servidas,5) as nvarchar(100))  AS COD , " & vbCrLf
sConsulta = sConsulta & "   LG.COD_ERP , LPPM.USU  ," & vbCrLf
sConsulta = sConsulta & "   unqa.cod + '' - '' + LPPM.prove +  + '' - '' + LPPM.art4  AS INFO" & vbCrLf
sConsulta = sConsulta & "   FROM #T_LOG_GRAL LG WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN LOG_PPM LPPM WITH(NOLOCK) ON LG.ID_TABLA=LPPM.ID AND LG.TABLA=26" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN UNQA WITH(NOLOCK) ON UNQA.ID=LPPM.UONQA" & vbCrLf
sConsulta = sConsulta & "   WHERE 1=1' + @WHERE " & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @CONSULTA,@PARAM,@ERP=@ERP, @TABLA=@TABLA, " & vbCrLf
sConsulta = sConsulta & "   @COD_FS=@COD_FS, @COD_ERP=@COD_ERP, @RECEP_ERP=@RECEP_ERP," & vbCrLf
sConsulta = sConsulta & "   @ANYO_PROCE=@ANYO_PROCE, @GMN1_PROCE=@GMN1_PROCE, @COD_PROCE=@COD_PROCE," & vbCrLf
sConsulta = sConsulta & "   @ANYO_PEDIDO=@ANYO_PEDIDO, @NUM_PEDIDO=@NUM_PEDIDO, @NUM_ORDEN=@NUM_ORDEN," & vbCrLf
sConsulta = sConsulta & "   @ID_LOG=@ID_LOG, @ACCION=@ACCION, @ESTADO=@ESTADO,@FECHA_DESDE=@FECHA_DESDE,@FECHA_HASTA=@FECHA_HASTA,@TIPOFECHA=@TIPOFECHA" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @TASA_SERVICIO_ACTIVA = 1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @CONSULTA= 'INSERT INTO #T_CONSULTA SELECT " & vbCrLf
sConsulta = sConsulta & "   LG.ERP AS ERP, LG.TABLA AS ENTIDAD, LG.ID AS IDENTIF, LG.ESTADO, LG.TEXTO, LG.ARG1, LG.ARG2,LG.FECENV, LG.FECREC, NOM_FICHERO , NOM_RECIBO," & vbCrLf
sConsulta = sConsulta & "   LTS.ACCION , " & vbCrLf
sConsulta = sConsulta & "   cast(ROUND(LTS.TOTAL_ENVIOS,5) as nvarchar(100))  as COD , LG.COD_ERP , " & vbCrLf
sConsulta = sConsulta & "   LTS.USU , " & vbCrLf
sConsulta = sConsulta & "   unqa.cod + '' - '' + LTS.prove +  + '' - '' + LTS.art4  AS INFO" & vbCrLf
sConsulta = sConsulta & "   FROM #T_LOG_GRAL LG WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN LOG_TASA_SERVICIO LTS WITH(NOLOCK) ON LG.ID_TABLA=LTS.ID AND LG.TABLA=27" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN UNQA WITH(NOLOCK) ON UNQA.ID=LTS.UONQA" & vbCrLf
sConsulta = sConsulta & "   WHERE 1=1' + @WHERE " & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @CONSULTA,@PARAM,@ERP=@ERP, @TABLA=@TABLA, " & vbCrLf
sConsulta = sConsulta & "   @COD_FS=@COD_FS, @COD_ERP=@COD_ERP, @RECEP_ERP=@RECEP_ERP," & vbCrLf
sConsulta = sConsulta & "   @ANYO_PROCE=@ANYO_PROCE, @GMN1_PROCE=@GMN1_PROCE, @COD_PROCE=@COD_PROCE," & vbCrLf
sConsulta = sConsulta & "   @ANYO_PEDIDO=@ANYO_PEDIDO, @NUM_PEDIDO=@NUM_PEDIDO, @NUM_ORDEN=@NUM_ORDEN," & vbCrLf
sConsulta = sConsulta & "   @ID_LOG=@ID_LOG, @ACCION=@ACCION, @ESTADO=@ESTADO,@FECHA_DESDE=@FECHA_DESDE,@FECHA_HASTA=@FECHA_HASTA,@TIPOFECHA=@TIPOFECHA" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CARGO_PROVE_ACTIVA=1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @CONSULTA= 'INSERT INTO #T_CONSULTA SELECT " & vbCrLf
sConsulta = sConsulta & "   LG.ERP AS ERP, LG.TABLA AS ENTIDAD, LG.ID AS IDENTIF, LG.ESTADO, LG.TEXTO, LG.ARG1, LG.ARG2,LG.FECENV, LG.FECREC, NOM_FICHERO , NOM_RECIBO," & vbCrLf
sConsulta = sConsulta & "   LCP.ACCION , cast(round(LCP.importe_facturado,5) as nvarchar(100))  COD , LG.COD_ERP , " & vbCrLf
sConsulta = sConsulta & "   LCP.USU , " & vbCrLf
sConsulta = sConsulta & "   unqa.cod + '' - '' + LCP.prove + '' - '' + LCP.art4 as INFO" & vbCrLf
sConsulta = sConsulta & "   FROM #T_LOG_GRAL LG WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN LOG_CARGO_PROVEEDORES LCP WITH(NOLOCK) ON LG.ID_TABLA=LCP.ID AND LG.TABLA=28" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN UNQA WITH(NOLOCK) ON UNQA.ID=LCP.UONQA" & vbCrLf
sConsulta = sConsulta & "   WHERE 1=1' + @WHERE " & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @CONSULTA,@PARAM,@ERP=@ERP, @TABLA=@TABLA, " & vbCrLf
sConsulta = sConsulta & "   @COD_FS=@COD_FS, @COD_ERP=@COD_ERP, @RECEP_ERP=@RECEP_ERP," & vbCrLf
sConsulta = sConsulta & "   @ANYO_PROCE=@ANYO_PROCE, @GMN1_PROCE=@GMN1_PROCE, @COD_PROCE=@COD_PROCE," & vbCrLf
sConsulta = sConsulta & "   @ANYO_PEDIDO=@ANYO_PEDIDO, @NUM_PEDIDO=@NUM_PEDIDO, @NUM_ORDEN=@NUM_ORDEN," & vbCrLf
sConsulta = sConsulta & "   @ID_LOG=@ID_LOG, @ACCION=@ACCION, @ESTADO=@ESTADO,@FECHA_DESDE=@FECHA_DESDE,@FECHA_HASTA=@FECHA_HASTA,@TIPOFECHA=@TIPOFECHA" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @UON_ACTIVA=1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @CONSULTA= 'INSERT INTO #T_CONSULTA SELECT " & vbCrLf
sConsulta = sConsulta & "   LG.ERP AS ERP, LG.TABLA AS ENTIDAD, LG.ID AS IDENTIF, LG.ESTADO, LG.TEXTO, LG.ARG1, LG.ARG2,LG.FECENV, LG.FECREC, NOM_FICHERO , NOM_RECIBO," & vbCrLf
sConsulta = sConsulta & "   L.ACCION , L.UON1 + ''-'' + L.UON2 + ''-'' + L.UON3 + ''-'' + L.UON4 COD, LG.COD_ERP , L.USU , '''' INFO" & vbCrLf
sConsulta = sConsulta & "   FROM #T_LOG_GRAL LG WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN LOG_UON L WITH(NOLOCK) ON LG.ID_TABLA=L.ID AND LG.TABLA=29" & vbCrLf
sConsulta = sConsulta & "   WHERE 1=1' + @WHERE " & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @CONSULTA,@PARAM,@ERP=@ERP, @TABLA=@TABLA, " & vbCrLf
sConsulta = sConsulta & "   @COD_FS=@COD_FS, @COD_ERP=@COD_ERP, @RECEP_ERP=@RECEP_ERP," & vbCrLf
sConsulta = sConsulta & "   @ANYO_PROCE=@ANYO_PROCE, @GMN1_PROCE=@GMN1_PROCE, @COD_PROCE=@COD_PROCE," & vbCrLf
sConsulta = sConsulta & "   @ANYO_PEDIDO=@ANYO_PEDIDO, @NUM_PEDIDO=@NUM_PEDIDO, @NUM_ORDEN=@NUM_ORDEN," & vbCrLf
sConsulta = sConsulta & "   @ID_LOG=@ID_LOG, @ACCION=@ACCION, @ESTADO=@ESTADO,@FECHA_DESDE=@FECHA_DESDE,@FECHA_HASTA=@FECHA_HASTA,@TIPOFECHA=@TIPOFECHA" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "IF @USU_ACTIVA=1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @CONSULTA= 'INSERT INTO #T_CONSULTA SELECT " & vbCrLf
sConsulta = sConsulta & "   LG.ERP AS ERP, LG.TABLA AS ENTIDAD, LG.ID AS IDENTIF, LG.ESTADO, LG.TEXTO, LG.ARG1, LG.ARG2,LG.FECENV, LG.FECREC, NOM_FICHERO , NOM_RECIBO," & vbCrLf
sConsulta = sConsulta & "   L.ACCION , L.COD , LG.COD_ERP , L.USU  , '''' INFO" & vbCrLf
sConsulta = sConsulta & "   FROM #T_LOG_GRAL LG WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN LOG_USU L WITH(NOLOCK) ON LG.ID_TABLA=L.ID AND LG.TABLA=30" & vbCrLf
sConsulta = sConsulta & "   WHERE 1=1' + @WHERE + @WHERECOD " & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @CONSULTA,@PARAM,@ERP=@ERP, @TABLA=@TABLA, " & vbCrLf
sConsulta = sConsulta & "   @COD_FS=@COD_FS, @COD_ERP=@COD_ERP, @RECEP_ERP=@RECEP_ERP," & vbCrLf
sConsulta = sConsulta & "   @ANYO_PROCE=@ANYO_PROCE, @GMN1_PROCE=@GMN1_PROCE, @COD_PROCE=@COD_PROCE," & vbCrLf
sConsulta = sConsulta & "   @ANYO_PEDIDO=@ANYO_PEDIDO, @NUM_PEDIDO=@NUM_PEDIDO, @NUM_ORDEN=@NUM_ORDEN," & vbCrLf
sConsulta = sConsulta & "   @ID_LOG=@ID_LOG, @ACCION=@ACCION, @ESTADO=@ESTADO,@FECHA_DESDE=@FECHA_DESDE,@FECHA_HASTA=@FECHA_HASTA,@TIPOFECHA=@TIPOFECHA" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "IF @ACTIVO_ACTIVA=1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @CONSULTA= 'INSERT INTO #T_CONSULTA SELECT " & vbCrLf
sConsulta = sConsulta & "   LG.ERP AS ERP, LG.TABLA AS ENTIDAD, LG.ID AS IDENTIF, LG.ESTADO, LG.TEXTO, LG.ARG1, LG.ARG2,LG.FECENV, LG.FECREC, NOM_FICHERO , NOM_RECIBO," & vbCrLf
sConsulta = sConsulta & "   L.ACCION , L.COD , LG.COD_ERP , L.USU  , '''' INFO" & vbCrLf
sConsulta = sConsulta & "   FROM #T_LOG_GRAL LG WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN LOG_ACTIVO L WITH(NOLOCK) ON LG.ID_TABLA=L.ID AND LG.TABLA=31" & vbCrLf
sConsulta = sConsulta & "   WHERE 1=1' + @WHERE + @WHERECOD " & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @CONSULTA,@PARAM,@ERP=@ERP, @TABLA=@TABLA, " & vbCrLf
sConsulta = sConsulta & "   @COD_FS=@COD_FS, @COD_ERP=@COD_ERP, @RECEP_ERP=@RECEP_ERP," & vbCrLf
sConsulta = sConsulta & "   @ANYO_PROCE=@ANYO_PROCE, @GMN1_PROCE=@GMN1_PROCE, @COD_PROCE=@COD_PROCE," & vbCrLf
sConsulta = sConsulta & "   @ANYO_PEDIDO=@ANYO_PEDIDO, @NUM_PEDIDO=@NUM_PEDIDO, @NUM_ORDEN=@NUM_ORDEN," & vbCrLf
sConsulta = sConsulta & "   @ID_LOG=@ID_LOG, @ACCION=@ACCION, @ESTADO=@ESTADO,@FECHA_DESDE=@FECHA_DESDE,@FECHA_HASTA=@FECHA_HASTA,@TIPOFECHA=@TIPOFECHA" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @FACTURA_ACTIVA=1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @CONSULTA= 'INSERT INTO #T_CONSULTA SELECT " & vbCrLf
sConsulta = sConsulta & "   LG.ERP AS ERP, LG.TABLA AS ENTIDAD, LG.ID AS IDENTIF, LG.ESTADO, LG.TEXTO, LG.ARG1, LG.ARG2,LG.FECENV, LG.FECREC, NOM_FICHERO , NOM_RECIBO," & vbCrLf
sConsulta = sConsulta & "   L.ACCION , L.NUM AS COD , LG.COD_ERP , L.USU  , '''' INFO" & vbCrLf
sConsulta = sConsulta & "   FROM #T_LOG_GRAL LG WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN LOG_FACTURA L WITH(NOLOCK) ON LG.ID_TABLA=L.ID AND LG.TABLA=32" & vbCrLf
sConsulta = sConsulta & "   WHERE 1=1' + @WHERE + @WHERECOD " & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @CONSULTA,@PARAM,@ERP=@ERP, @TABLA=@TABLA, " & vbCrLf
sConsulta = sConsulta & "   @COD_FS=@COD_FS, @COD_ERP=@COD_ERP, @RECEP_ERP=@RECEP_ERP," & vbCrLf
sConsulta = sConsulta & "   @ANYO_PROCE=@ANYO_PROCE, @GMN1_PROCE=@GMN1_PROCE, @COD_PROCE=@COD_PROCE," & vbCrLf
sConsulta = sConsulta & "   @ANYO_PEDIDO=@ANYO_PEDIDO, @NUM_PEDIDO=@NUM_PEDIDO, @NUM_ORDEN=@NUM_ORDEN," & vbCrLf
sConsulta = sConsulta & "   @ID_LOG=@ID_LOG, @ACCION=@ACCION, @ESTADO=@ESTADO,@FECHA_DESDE=@FECHA_DESDE,@FECHA_HASTA=@FECHA_HASTA,@TIPOFECHA=@TIPOFECHA" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @PAGO_ACTIVA = 1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @CONSULTA= 'INSERT INTO #T_CONSULTA SELECT " & vbCrLf
sConsulta = sConsulta & "   LG.ERP AS ERP, LG.TABLA AS ENTIDAD, LG.ID AS IDENTIF, LG.ESTADO, LG.TEXTO, LG.ARG1, LG.ARG2,LG.FECENV, LG.FECREC, NOM_FICHERO , NOM_RECIBO," & vbCrLf
sConsulta = sConsulta & "   L.ACCION , L.NUM AS COD , LG.COD_ERP , L.USU  , '''' INFO" & vbCrLf
sConsulta = sConsulta & "   FROM #T_LOG_GRAL LG WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN LOG_PAGO L WITH(NOLOCK) ON LG.ID_TABLA=L.ID AND LG.TABLA=33" & vbCrLf
sConsulta = sConsulta & "   WHERE 1=1' + @WHERE + @WHERECOD" & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @CONSULTA,@PARAM,@ERP=@ERP, @TABLA=@TABLA, " & vbCrLf
sConsulta = sConsulta & "   @COD_FS=@COD_FS, @COD_ERP=@COD_ERP, @RECEP_ERP=@RECEP_ERP," & vbCrLf
sConsulta = sConsulta & "   @ANYO_PROCE=@ANYO_PROCE, @GMN1_PROCE=@GMN1_PROCE, @COD_PROCE=@COD_PROCE," & vbCrLf
sConsulta = sConsulta & "   @ANYO_PEDIDO=@ANYO_PEDIDO, @NUM_PEDIDO=@NUM_PEDIDO, @NUM_ORDEN=@NUM_ORDEN," & vbCrLf
sConsulta = sConsulta & "   @ID_LOG=@ID_LOG, @ACCION=@ACCION, @ESTADO=@ESTADO,@FECHA_DESDE=@FECHA_DESDE,@FECHA_HASTA=@FECHA_HASTA,@TIPOFECHA=@TIPOFECHA" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @PRES5_ACTIVA = 1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @CONSULTA= 'INSERT INTO #T_CONSULTA SELECT " & vbCrLf
sConsulta = sConsulta & "   LG.ERP AS ERP, LG.TABLA AS ENTIDAD, LG.ID AS IDENTIF, LG.ESTADO, LG.TEXTO, LG.ARG1, LG.ARG2,LG.FECENV, LG.FECREC, NOM_FICHERO , NOM_RECIBO," & vbCrLf
sConsulta = sConsulta & "   L.ACCION , L.PRES0 + ''-'' + L.PRES1 + ''-'' + L.PRES2 + ''-'' + L.PRES3 + ''-'' + L.PRES4 AS COD , " & vbCrLf
sConsulta = sConsulta & "   LG.COD_ERP , L.USU , '''' INFO" & vbCrLf
sConsulta = sConsulta & "   FROM #T_LOG_GRAL LG WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN LOG_PRES5 L WITH(NOLOCK) ON LG.ID_TABLA=L.ID AND LG.TABLA=34" & vbCrLf
sConsulta = sConsulta & "   WHERE 1=1' + @WHERE + @WHERECOD    " & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @CONSULTA,@PARAM,@ERP=@ERP, @TABLA=@TABLA, " & vbCrLf
sConsulta = sConsulta & "   @COD_FS=@COD_FS, @COD_ERP=@COD_ERP, @RECEP_ERP=@RECEP_ERP," & vbCrLf
sConsulta = sConsulta & "   @ANYO_PROCE=@ANYO_PROCE, @GMN1_PROCE=@GMN1_PROCE, @COD_PROCE=@COD_PROCE," & vbCrLf
sConsulta = sConsulta & "   @ANYO_PEDIDO=@ANYO_PEDIDO, @NUM_PEDIDO=@NUM_PEDIDO, @NUM_ORDEN=@NUM_ORDEN," & vbCrLf
sConsulta = sConsulta & "   @ID_LOG=@ID_LOG, @ACCION=@ACCION, @ESTADO=@ESTADO,@FECHA_DESDE=@FECHA_DESDE,@FECHA_HASTA=@FECHA_HASTA,@TIPOFECHA=@TIPOFECHA" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CENTRO_SM_ACTIVA = 1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @CONSULTA= 'INSERT INTO #T_CONSULTA SELECT " & vbCrLf
sConsulta = sConsulta & "   LG.ERP AS ERP, LG.TABLA AS ENTIDAD, LG.ID AS IDENTIF, LG.ESTADO, LG.TEXTO, LG.ARG1, LG.ARG2,LG.FECENV, LG.FECREC, NOM_FICHERO , NOM_RECIBO," & vbCrLf
sConsulta = sConsulta & "   L.ACCION , L.CENTRO_SM AS COD , LG.COD_ERP , L.USU , '''' INFO" & vbCrLf
sConsulta = sConsulta & "   FROM #T_LOG_GRAL LG WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN LOG_CENTRO_SM L WITH(NOLOCK) ON LG.ID_TABLA=L.ID AND LG.TABLA=35" & vbCrLf
sConsulta = sConsulta & "   WHERE 1=1' + @WHERE + @WHERECOD " & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @CONSULTA,@PARAM,@ERP=@ERP, @TABLA=@TABLA, " & vbCrLf
sConsulta = sConsulta & "   @COD_FS=@COD_FS, @COD_ERP=@COD_ERP, @RECEP_ERP=@RECEP_ERP," & vbCrLf
sConsulta = sConsulta & "   @ANYO_PROCE=@ANYO_PROCE, @GMN1_PROCE=@GMN1_PROCE, @COD_PROCE=@COD_PROCE," & vbCrLf
sConsulta = sConsulta & "   @ANYO_PEDIDO=@ANYO_PEDIDO, @NUM_PEDIDO=@NUM_PEDIDO, @NUM_ORDEN=@NUM_ORDEN," & vbCrLf
sConsulta = sConsulta & "   @ID_LOG=@ID_LOG, @ACCION=@ACCION, @ESTADO=@ESTADO,@FECHA_DESDE=@FECHA_DESDE,@FECHA_HASTA=@FECHA_HASTA,@TIPOFECHA=@TIPOFECHA" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @EMPRESA_ACTIVA=1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @CONSULTA= 'INSERT INTO #T_CONSULTA SELECT " & vbCrLf
sConsulta = sConsulta & "   LG.ERP AS ERP, LG.TABLA AS ENTIDAD, LG.ID AS IDENTIF, LG.ESTADO, LG.TEXTO, LG.ARG1, LG.ARG2,LG.FECENV, LG.FECREC, NOM_FICHERO , NOM_RECIBO," & vbCrLf
sConsulta = sConsulta & "   L.ACCION , L.NIF + '' - '' + L.DEN AS COD , LG.COD_ERP , L.USU  , '''' INFO" & vbCrLf
sConsulta = sConsulta & "   FROM #T_LOG_GRAL LG WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN LOG_EMPRESA L WITH(NOLOCK) ON LG.ID_TABLA=L.ID AND LG.TABLA=36" & vbCrLf
sConsulta = sConsulta & "   WHERE 1=1' + @WHERE + @WHERECOD " & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @CONSULTA,@PARAM,@ERP=@ERP, @TABLA=@TABLA, " & vbCrLf
sConsulta = sConsulta & "   @COD_FS=@COD_FS, @COD_ERP=@COD_ERP, @RECEP_ERP=@RECEP_ERP," & vbCrLf
sConsulta = sConsulta & "   @ANYO_PROCE=@ANYO_PROCE, @GMN1_PROCE=@GMN1_PROCE, @COD_PROCE=@COD_PROCE," & vbCrLf
sConsulta = sConsulta & "   @ANYO_PEDIDO=@ANYO_PEDIDO, @NUM_PEDIDO=@NUM_PEDIDO, @NUM_ORDEN=@NUM_ORDEN," & vbCrLf
sConsulta = sConsulta & "   @ID_LOG=@ID_LOG, @ACCION=@ACCION, @ESTADO=@ESTADO,@FECHA_DESDE=@FECHA_DESDE,@FECHA_HASTA=@FECHA_HASTA,@TIPOFECHA=@TIPOFECHA" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "      " & vbCrLf
sConsulta = sConsulta & "DECLARE @COMPLETA AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "SET @COMPLETA=''" & vbCrLf
sConsulta = sConsulta & "IF @PROVE_ERP_ACTIVA=1 " & vbCrLf
sConsulta = sConsulta & "begin" & vbCrLf
sConsulta = sConsulta & "   SET @COMPLETA = 'SELECT ERP.DEN AS ERP, T.ENTIDAD, T.IDENTIF, T.ESTADO, T.TEXTO, T.ARG1, T.ARG2, T.FECENV, T.FECREC," & vbCrLf
sConsulta = sConsulta & "        T.NOM_FICHERO, T.NOM_RECIBO,T.ACCION, T.COD, T.COD_ERP, T.USU , T.INFO, TIE.SENTIDO" & vbCrLf
sConsulta = sConsulta & "        FROM #T_CONSULTA T INNER JOIN ERP WITH(NOLOCK) ON ERP.COD=T.ERP " & vbCrLf
sConsulta = sConsulta & "        INNER JOIN TABLAS_INTEGRACION_ERP TIE WITH(NOLOCK) ON TIE.ERP=T.ERP AND TIE.TABLA=8 AND T.ENTIDAD=102'" & vbCrLf
sConsulta = sConsulta & "    IF @TABLA=0 " & vbCrLf
sConsulta = sConsulta & "           SET @COMPLETA = @COMPLETA + ' UNION '" & vbCrLf
sConsulta = sConsulta & "end" & vbCrLf
sConsulta = sConsulta & "IF @TABLA=0 OR NOT(@TABLA=102) " & vbCrLf
sConsulta = sConsulta & "   SET @COMPLETA = @COMPLETA + ' SELECT ERP.DEN AS ERP, T.ENTIDAD, T.IDENTIF, T.ESTADO, T.TEXTO, T.ARG1, T.ARG2, T.FECENV, T.FECREC," & vbCrLf
sConsulta = sConsulta & "        T.NOM_FICHERO, T.NOM_RECIBO,T.ACCION, T.COD, T.COD_ERP, T.USU , T.INFO, TIE.SENTIDO" & vbCrLf
sConsulta = sConsulta & "        FROM #T_CONSULTA T INNER JOIN ERP WITH(NOLOCK) ON ERP.COD=T.ERP " & vbCrLf
sConsulta = sConsulta & "        INNER JOIN TABLAS_INTEGRACION_ERP TIE WITH(NOLOCK) ON TIE.ERP=T.ERP AND TIE.TABLA=T.ENTIDAD'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL  @COMPLETA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DROP TABLE #T_LOG_GRAL" & vbCrLf
sConsulta = sConsulta & "DROP TABLE #T_CONSULTA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSGSW_DEVOLVER_ATRIBUTOSPROCESO]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSGSW_DEVOLVER_ATRIBUTOSPROCESO]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSGSW_DEVOLVER_ATRIBUTOSPROCESO] @ANYO INT, @GMN1 VARCHAR(50), @PROCE VARCHAR(50),@IDEMP INT,@PROVE VARCHAR(50) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @ERP INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @NUMOFE INT " & vbCrLf
sConsulta = sConsulta & "DECLARE @IDLOG INT " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @ERP=ES.ERP FROM ERP_SOCIEDAD ES WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN EMP E WITH (NOLOCK) ON E.SOCIEDAD=ES.SOCIEDAD AND E.ID=@IDEMP " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--N�mero de oferta del proveedor adjudicado:" & vbCrLf
sConsulta = sConsulta & "SET @NUMOFE=(SELECT TOP 1(OFE) FROM ITEM_ADJ IA WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN ITEM I WITH(NOLOCK) ON I.ANYO=IA.ANYO AND I.GMN1_PROCE=IA.GMN1 AND I.PROCE=IA.PROCE AND I.ID=IA.ITEM WHERE IA.ANYO=@ANYO AND IA.GMN1=@GMN1 AND IA.PROCE=@PROCE AND IA.PROVE=@PROVE AND (I.EST=1 OR I.EST IS NULL))" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--L�nea de LOG_ITEM_ADJ para sacar los atributos traspasados a nivel de proceso" & vbCrLf
sConsulta = sConsulta & "SET @IDLOG=(SELECT ISNULL(MAX(ID),0) ID FROM LOG_ITEM_ADJ WITH (NOLOCK) WHERE ORIGEN=0 AND ANYO=@ANYO AND GMN1_PROCE=@GMN1 AND PROCE=@PROCE  " & vbCrLf
sConsulta = sConsulta & "   AND PROVEPADRE=@PROVE AND EMPRESA=@IDEMP AND ACCION='I' )     " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "SELECT COALESCE(PA.ATRIB,PAE.ATRIB) ID, DA.COD,DA.DEN," & vbCrLf
sConsulta = sConsulta & "COALESCE(LIAA.VALOR_TEXT,PAE.VALOR_TEXT,OA.VALOR_TEXT,IA.VALOR_TEXT) VALOR_TEXT," & vbCrLf
sConsulta = sConsulta & "COALESCE(LIAA.VALOR_NUM,PAE.VALOR_NUM,OA.VALOR_NUM,IA.VALOR_NUM) VALOR_NUM," & vbCrLf
sConsulta = sConsulta & "COALESCE(LIAA.VALOR_FEC,PAE.VALOR_FEC,OA.VALOR_FEC,IA.VALOR_FEC) VALOR_FEC," & vbCrLf
sConsulta = sConsulta & "COALESCE(LIAA.VALOR_BOOL,PAE.VALOR_BOOL,OA.VALOR_BOOL,IA.VALOR_BOOL) VALOR_BOOL," & vbCrLf
sConsulta = sConsulta & "DA.TIPO,IA.INTRO,IA.OBLIGATORIO,IA.VALIDACION_ERP," & vbCrLf
sConsulta = sConsulta & "COALESCE(PAL.VALOR_TEXT,IAL.VALOR_TEXT) VALOR_TEXT_LISTA," & vbCrLf
sConsulta = sConsulta & "COALESCE(PAL.VALOR_NUM,IAL.VALOR_NUM) VALOR_NUM_LISTA," & vbCrLf
sConsulta = sConsulta & "COALESCE(PAL.VALOR_FEC,IAL.VALOR_FEC) VALOR_FEC_LISTA ," & vbCrLf
sConsulta = sConsulta & "COALESCE(PAL.ORDEN,IAL.ORDEN) ORDEN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "FROM INT_ATRIB IA WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN DEF_ATRIB DA WITH (NOLOCK) ON DA.ID=IA.ATRIB AND IA.ADJUDICACION=1 AND IA.AMBITO=1 AND IA.ERP=@ERP" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN INT_ATRIB_LISTA IAL WITH (NOLOCK) ON IAL.ATRIB_ID =IA.ID AND IA.INTRO=1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN LOG_ITEM_ADJ_ATRIB LIAA WITH (NOLOCK) ON LIAA.ID_LOG_ITEM_ADJ=@IDLOG AND LIAA.AMBITO=1 AND LIAA.ATRIB=IA.ATRIB --Valores cuando ya han sido traspasados" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PROCE_ATRIBESP PAE WITH (NOLOCK) ON PAE.ANYO=@ANYO AND PAE.GMN1=@GMN1 AND PAE.PROCE=@PROCE AND PAE.ATRIB=IA.ATRIB " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PROCE_ATRIB PA WITH (NOLOCK) ON PA.ANYO=@ANYO AND PA.GMN1=@GMN1 AND PA.PROCE=@PROCE AND PA.ATRIB =IA.ATRIB AND PA.AMBITO=1" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PROCE_ATRIB_LISTA PAL WITH (NOLOCK) ON PAL.ATRIB_ID=PA.ID AND PA.INTRO=1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN OFE_ATRIB OA WITH (NOLOCK) ON OA.ANYO=@ANYO AND OA.GMN1=@GMN1 AND OA.PROCE=@PROCE AND OA.ATRIB_ID =PA.ID AND OA.PROVE=@PROVE AND OA.OFE=@NUMOFE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "WHERE COALESCE(PA.ATRIB,PAE.ATRIB) IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ORDER BY IA.ATRIB , IAL.ORDEN ASC" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSGSW_DEVOLVER_ATRIBUTOSGRUPO]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSGSW_DEVOLVER_ATRIBUTOSGRUPO]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSGSW_DEVOLVER_ATRIBUTOSGRUPO] @ANYO INT, @GMN1 VARCHAR(50), @PROCE VARCHAR(50),@PROVE VARCHAR(50), @IDGRUPO INT,@IDEMP INT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @ERP INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDLOG INT " & vbCrLf
sConsulta = sConsulta & "DECLARE @NUMOFE INT " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @ERP=ES.ERP FROM ERP_SOCIEDAD ES WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN EMP E WITH (NOLOCK) ON E.SOCIEDAD=ES.SOCIEDAD AND E.ID=@IDEMP " & vbCrLf
sConsulta = sConsulta & "--N�mero de oferta del proveedor adjudicado:" & vbCrLf
sConsulta = sConsulta & "SET @NUMOFE=(SELECT TOP 1(OFE) FROM ITEM_ADJ IA WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN ITEM I WITH(NOLOCK) ON I.ANYO=IA.ANYO AND I.GMN1_PROCE=IA.GMN1 AND I.PROCE=IA.PROCE AND I.ID=IA.ITEM WHERE IA.ANYO=@ANYO AND IA.GMN1=@GMN1 AND IA.PROCE=@PROCE AND IA.PROVE=@PROVE AND (I.EST=1 OR I.EST IS NULL))" & vbCrLf
sConsulta = sConsulta & "--�ltima l�nea de LOG_ITEM_ADJ traspasada para ese proceso, grupo y empresa para despu�s sacar los atributos traspasados a nivel de grupo" & vbCrLf
sConsulta = sConsulta & "SET @IDLOG=(SELECT ISNULL(MAX(L.ID),0) ID FROM LOG_ITEM_ADJ L WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "INNER JOIN ITEM I WITH(NOLOCK) ON I.ANYO=L.ANYO AND I.GMN1_PROCE=L.GMN1_PROCE AND I.PROCE=L.PROCE AND I.ID= L.ID_ITEM   " & vbCrLf
sConsulta = sConsulta & "WHERE L.ORIGEN=0 AND L.ANYO=@ANYO AND L.GMN1_PROCE=@GMN1 AND L.PROCE=@PROCE  " & vbCrLf
sConsulta = sConsulta & "   AND L.PROVEPADRE=@PROVE AND L.EMPRESA=@IDEMP AND L.ACCION='I' AND I.GRUPO=@IDGRUPO )      " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT COALESCE(PGA.ATRIB,PA.ATRIB) ID, DA.COD,DA.DEN," & vbCrLf
sConsulta = sConsulta & "COALESCE(LIAA.VALOR_TEXT,PGA.VALOR_TEXT,OA.VALOR_TEXT,OA2.VALOR_TEXT,IA.VALOR_TEXT) VALOR_TEXT," & vbCrLf
sConsulta = sConsulta & "COALESCE(LIAA.VALOR_NUM,PGA.VALOR_NUM,OA.VALOR_NUM,OA2.VALOR_NUM,IA.VALOR_NUM) VALOR_NUM," & vbCrLf
sConsulta = sConsulta & "COALESCE(LIAA.VALOR_FEC,PGA.VALOR_FEC,OA.VALOR_FEC,OA2.VALOR_FEC,IA.VALOR_FEC) VALOR_FEC," & vbCrLf
sConsulta = sConsulta & "COALESCE(LIAA.VALOR_BOOL,PGA.VALOR_BOOL,OA.VALOR_BOOL,OA2.VALOR_BOOL,IA.VALOR_BOOL) VALOR_BOOL," & vbCrLf
sConsulta = sConsulta & "DA.TIPO,IA.INTRO,IA.OBLIGATORIO,DA.VALIDACION_ERP," & vbCrLf
sConsulta = sConsulta & "COALESCE(PAL.VALOR_TEXT,IAL.VALOR_TEXT) VALOR_TEXT_LISTA,COALESCE(PAL.VALOR_NUM,IAL.VALOR_NUM) VALOR_NUM_LISTA,COALESCE(PAL.VALOR_FEC,IAL.VALOR_FEC) VALOR_FEC_LISTA ," & vbCrLf
sConsulta = sConsulta & "COALESCE(PAL.ORDEN,IAL.ORDEN) ORDEN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "FROM INT_ATRIB IA WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN DEF_ATRIB DA WITH (NOLOCK) ON DA.ID=IA.ATRIB AND IA.ADJUDICACION=1 AND IA.AMBITO=2 AND IA.ERP=@ERP" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN INT_ATRIB_LISTA IAL WITH (NOLOCK) ON IAL.ATRIB_ID =IA.ID AND IA.INTRO=1" & vbCrLf
sConsulta = sConsulta & "--Atributos ya traspasados" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN LOG_ITEM_ADJ_ATRIB LIAA WITH (NOLOCK) ON LIAA.ID_LOG_ITEM_ADJ=@IDLOG AND LIAA.AMBITO=2 AND LIAA.ATRIB=IA.ATRIB --Valores cuando ya han sido traspasados" & vbCrLf
sConsulta = sConsulta & "--Atributos de especificaciones a nivel de grupo" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PROCE_GRUPO_ATRIBESP PGA WITH (NOLOCK) ON PGA.ANYO=@ANYO AND PGA.GMN1=@GMN1 AND PGA.PROCE=@PROCE AND PGA.ATRIB=IA.ATRIB AND (PGA.GRUPO=@IDGRUPO OR PGA.GRUPO IS NULL)" & vbCrLf
sConsulta = sConsulta & "--Atributos de oferta a nivel de grupo" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PROCE_ATRIB PA WITH (NOLOCK) ON PA.ANYO=@ANYO AND PA.GMN1=@GMN1 AND PA.PROCE=@PROCE AND PA.ATRIB =IA.ATRIB  AND (PA.GRUPO=@IDGRUPO OR PA.GRUPO IS NULL)" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PROCE_ATRIB_LISTA PAL WITH (NOLOCK) ON PAL.ATRIB_ID=PA.ID" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN OFE_GR_ATRIB OA WITH (NOLOCK) ON OA.ANYO=@ANYO AND OA.GMN1=@GMN1 AND OA.PROCE=@PROCE AND OA.ATRIB_ID =PA.ID AND OA.PROVE=@PROVE AND OA.GRUPO=@IDGRUPO AND OA.OFE=@NUMOFE AND PA.AMBITO=2" & vbCrLf
sConsulta = sConsulta & "--Atributos de especificaciones a nivel de proceso (ser�n comunes a todos los grupos)" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PROCE_ATRIBESP PAE WITH (NOLOCK) ON PAE.ANYO=@ANYO AND PAE.GMN1=@GMN1 AND PAE.PROCE=@PROCE AND PAE.ATRIB=IA.ATRIB " & vbCrLf
sConsulta = sConsulta & "--Atributos de oferta a nivel de proceso (ser�n comunes a todos los grupos)" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN OFE_ATRIB OA2 WITH (NOLOCK) ON OA2.ANYO=@ANYO AND OA2.GMN1=@GMN1 AND OA2.PROCE=@PROCE AND OA2.ATRIB_ID =PA.ID AND OA2.PROVE=@PROVE AND OA2.OFE=@NUMOFE AND PA.AMBITO=1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "WHERE COALESCE(PA.ATRIB,PGA.ATRIB) IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "ORDER BY IA.ATRIB , IAL.ORDEN ASC" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSGSW_DEVOLVER_ATRIBUTOSITEM ]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSGSW_DEVOLVER_ATRIBUTOSITEM ]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSGSW_DEVOLVER_ATRIBUTOSITEM] @ANYO INT, @GMN1 VARCHAR(50), @PROCE VARCHAR(50),@PROVE VARCHAR(50), @ITEM INT,@IDEMP INT,@LIA_ID INT,@IDGRUPO INT AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @ERP INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @NUMOFE INTEGER" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @ERP=ES.ERP FROM ERP_SOCIEDAD ES WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN EMP E WITH (NOLOCK) ON E.SOCIEDAD=ES.SOCIEDAD AND E.ID=@IDEMP " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @LIA_ID=0" & vbCrLf
sConsulta = sConsulta & "   begin" & vbCrLf
sConsulta = sConsulta & "   SET @NUMOFE=(SELECT TOP 1(OFE) FROM ITEM_ADJ WITH(NOLOCK) WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE AND ITEM=@ITEM)" & vbCrLf
sConsulta = sConsulta & "   SET @LIA_ID=NULL" & vbCrLf
sConsulta = sConsulta & "   end" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   --N�mero de oferta del proveedor adjudicado:" & vbCrLf
sConsulta = sConsulta & "   SELECT @NUMOFE=NUM_OFE FROM LOG_ITEM_ADJ WITH(NOLOCK) WHERE ID=@LIA_ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT COALESCE(PA.ATRIB,IAE.ATRIB,PGA.ATRIB,PAE.ATRIB) ID, DA.COD,DA.DEN," & vbCrLf
sConsulta = sConsulta & "COALESCE(LIAA.VALOR_TEXT,IAE.VALOR_TEXT,OA.VALOR_TEXT,PGA.VALOR_TEXT,OGA.VALOR_TEXT,PAE.VALOR_TEXT,OPA.VALOR_TEXT,IA.VALOR_TEXT) VALOR_TEXT," & vbCrLf
sConsulta = sConsulta & "COALESCE(LIAA.VALOR_NUM,IAE.VALOR_NUM,OA.VALOR_NUM,PGA.VALOR_NUM,OGA.VALOR_NUM,PAE.VALOR_NUM,OPA.VALOR_NUM,IA.VALOR_NUM) VALOR_NUM," & vbCrLf
sConsulta = sConsulta & "COALESCE(LIAA.VALOR_FEC,IAE.VALOR_FEC,OA.VALOR_FEC,PGA.VALOR_FEC,OGA.VALOR_FEC,PAE.VALOR_FEC,OPA.VALOR_FEC,IA.VALOR_FEC) VALOR_FEC," & vbCrLf
sConsulta = sConsulta & "COALESCE(LIAA.VALOR_BOOL,IAE.VALOR_BOOL,OA.VALOR_BOOL,PGA.VALOR_BOOL,OGA.VALOR_BOOL,PAE.VALOR_BOOL,OPA.VALOR_BOOL,IA.VALOR_BOOL) VALOR_BOOL," & vbCrLf
sConsulta = sConsulta & "DA.TIPO,IA.INTRO,IA.OBLIGATORIO,DA.VALIDACION_ERP, " & vbCrLf
sConsulta = sConsulta & "COALESCE(PAL.VALOR_TEXT,IAL.VALOR_TEXT) VALOR_TEXT_LISTA," & vbCrLf
sConsulta = sConsulta & "COALESCE(PAL.VALOR_NUM,IAL.VALOR_NUM) VALOR_NUM_LISTA," & vbCrLf
sConsulta = sConsulta & "COALESCE(PAL.VALOR_FEC,IAL.VALOR_FEC) VALOR_FEC_LISTA ,COALESCE(PAL.ORDEN,IAL.ORDEN) ORDEN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "FROM INT_ATRIB IA WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN DEF_ATRIB DA WITH (NOLOCK) ON DA.ID=IA.ATRIB AND IA.ADJUDICACION=1 AND IA.AMBITO=3 AND IA.ERP=@ERP" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN INT_ATRIB_LISTA IAL WITH (NOLOCK) ON IAL.ATRIB_ID =IA.ID " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN LOG_ITEM_ADJ_ATRIB LIAA WITH (NOLOCK) ON LIAA.ID_LOG_ITEM_ADJ=@LIA_ID AND LIAA.AMBITO=3 AND LIAA.ATRIB=IA.ATRIB " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--El atributo aparece como atributo de especificaciones a nivel de item, o de grupo, o de proceso" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN ITEM_ATRIBESP IAE WITH (NOLOCK) ON IAE.ANYO=@ANYO AND IAE.GMN1=@GMN1 AND IAE.PROCE=@PROCE AND IAE.ATRIB=IA.ATRIB AND IAE.ITEM =@ITEM" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PROCE_GRUPO_ATRIBESP PGA WITH (NOLOCK) ON PGA.ANYO=@ANYO AND PGA.GMN1=@GMN1 AND PGA.PROCE=@PROCE AND PGA.ATRIB=IA.ATRIB AND PGA.GRUPO=@IDGRUPO " & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PROCE_ATRIBESP PAE WITH (NOLOCK) ON PAE.ANYO=@ANYO AND PAE.GMN1=@GMN1 AND PAE.PROCE=@PROCE AND PAE.ATRIB=IA.ATRIB " & vbCrLf
sConsulta = sConsulta & "--El atributo aparece como atributo de oferta a nivel de item, o de grupo, o de proceso" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PROCE_ATRIB PA WITH (NOLOCK) ON PA.ANYO=@ANYO AND PA.GMN1=@GMN1 AND PA.PROCE=@PROCE AND PA.ATRIB =IA.ATRIB " & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PROCE_ATRIB_LISTA PAL WITH (NOLOCK) ON PAL.ATRIB_ID=PA.ID" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN OFE_ITEM_ATRIB OA WITH (NOLOCK) ON OA.ANYO=@ANYO AND OA.GMN1=@GMN1 AND OA.PROCE=@PROCE AND OA.ATRIB_ID =PA.ID " & vbCrLf
sConsulta = sConsulta & "   AND OA.PROVE=@PROVE AND OA.ITEM=@ITEM AND OA.OFE=@NUMOFE AND PA.AMBITO=3" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN OFE_GR_ATRIB OGA WITH (NOLOCK) ON OGA.ANYO=@ANYO AND OGA.GMN1=@GMN1 AND OGA.PROCE=@PROCE AND OGA.ATRIB_ID =PA.ID " & vbCrLf
sConsulta = sConsulta & "   AND OGA.PROVE=@PROVE AND OGA.GRUPO=@ITEM AND OGA.OFE=@NUMOFE AND PA.AMBITO=2" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN OFE_ATRIB OPA WITH (NOLOCK) ON OPA.ANYO=@ANYO AND OPA.GMN1=@GMN1 AND OPA.PROCE=@PROCE AND OPA.ATRIB_ID =PA.ID " & vbCrLf
sConsulta = sConsulta & "   AND OPA.PROVE=@PROVE AND OPA.OFE=@NUMOFE AND PA.AMBITO=1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "WHERE COALESCE(PA.ATRIB,IAE.ATRIB,PGA.ATRIB,PAE.ATRIB) IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "ORDER BY IA.ATRIB , IAL.ORDEN ASC" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
End Sub


Public Sub V_31900_8_Tablas_088()
    Dim sConsulta As String
    sConsulta = "ALTER TABLE FSAL_ENTORNOS" & vbCrLf
    sConsulta = sConsulta & "DROP CONSTRAINT FK_FSAL_ENTORNOS_USU" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "ALTER TABLE FSAL_ENTORNOS" & vbCrLf
    sConsulta = sConsulta & "DROP CONSTRAINT FK_FSAL_ENTORNOS_USU1" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    
End Sub


Public Function CodigoDeActualizacion31900_08_1888_A31900_08_1889() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim oADOConnection As ADODB.Connection
    Dim sConsulta As String

    On Error GoTo Error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Actualiza Tablas"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 3
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    frmProgreso.ProgressBar1.Refresh
    V_31900_8_Tablas_089
    
    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Actualiza Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 3
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    frmProgreso.ProgressBar1.Refresh
    V_31900_8_Storeds_089
    
    sConsulta = "UPDATE VERSION SET NUM ='3.00.18.89'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103), PRODVER='31900.8'"
    ExecuteSQL gRDOCon, sConsulta
        
    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False
        
    CodigoDeActualizacion31900_08_1888_A31900_08_1889 = True
    Exit Function

Error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    CodigoDeActualizacion31900_08_1888_A31900_08_1889 = False
End Function

Public Sub V_31900_8_Storeds_089()
    Dim sConsulta As String
            
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSWS_USU]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSWS_USU]" & vbCrLf
    gRDOCon.QueryTimeout = 7200
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "CREATE PROCEDURE [dbo].[FSWS_USU]" & vbCrLf
    sConsulta = sConsulta & "   @COD AS VARCHAR(50)" & vbCrLf
    sConsulta = sConsulta & "AS" & vbCrLf
    sConsulta = sConsulta & "DECLARE @PERF AS INT" & vbCrLf
    sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF" & vbCrLf
    sConsulta = sConsulta & "SELECT USU.COD,USU.PWD,USU.FECPWD,PER.NOM + ' ' + PER.APE AS NOMBRE,USU.PER,USU.IDIOMA,USU.MON,PER.DEP,PER.EQP," & vbCrLf
    sConsulta = sConsulta & "USU.TIPOEMAIL,PER.EMAIL,USU.DATEFMT,USU.THOUSANFMT,USU.DECIMALFMT,USU.PRECISIONFMT,USU.ADM,USU.FEC_USU," & vbCrLf
    sConsulta = sConsulta & "/*ACCESOS*/ISNULL(P.FSGS,USU.FSGS) FSGS,ISNULL(P.FSPM,USU.SOL_COMPRA) FSPM," & vbCrLf
    sConsulta = sConsulta & "ISNULL(P.FSQA,USU.FSQA) FSQA,ISNULL(P.FSEP,USU.FSEP) FSEP,ISNULL(P.FSSM,USU.FSSM) FSSM," & vbCrLf
    sConsulta = sConsulta & "ISNULL(P.FSIS,USU.FSINT) FSIS,ISNULL(P.FSIM,USU.FSIM) FSIM,ISNULL(P.FSCM,USU.CONTRATOS) FSCM," & vbCrLf
    sConsulta = sConsulta & "ISNULL(P.FSBI,USU.FSBI) FSBI, ISNULL(P.FSAL, ISNULL(USU.FSAL,0)) FSAL," & vbCrLf
    sConsulta = sConsulta & "/*OTROS*/" & vbCrLf
    sConsulta = sConsulta & "USU.FSWS_PRES1,USU.FSWS_PRES2,USU.FSWS_PRES3,USU.FSWS_PRES4," & vbCrLf
    sConsulta = sConsulta & "USU.ANYADIR_PROV_FAV,USU.MOSTRAR_DEF_PROV_MAT," & vbCrLf
    sConsulta = sConsulta & "USU.PM_ANYADIR_PRES1_FAV,USU.PM_ANYADIR_PRES2_FAV,USU.PM_ANYADIR_PRES3_FAV,USU.PM_ANYADIR_PRES4_FAV," & vbCrLf
    sConsulta = sConsulta & "USU.PM_NUMFILAS," & vbCrLf
    sConsulta = sConsulta & "USU.FSQA_PP_MANUAL,USU.FSQA_PP_AUTO," & vbCrLf
    sConsulta = sConsulta & "USU.FSQA_PP_NIVEL,UNQAD.DEN GRUPOIDIOMAUSU,USU.FSQA_NOTIFICACIONERRORCALCPUNT," & vbCrLf
    sConsulta = sConsulta & "/*ACCESO FSEP Y DATOS*/USU.MOSTRARFMT,USU.FSEPTIPO,USU.DEST,USU.MOSTATRIB,USU.MOSTCANTMIN," & vbCrLf
    sConsulta = sConsulta & "USU.MOSTCODART,USU.MOSTCODPROVE,USU.MOSTIMGART,USU.ORDEN,USU.DIREC,USU.FSEP_SM_BAR," & vbCrLf
    sConsulta = sConsulta & "USU.FSEP_SEG_VER_PED_USU,USU.FSEP_SEG_VER_PED_CCIMP,USU.CARGAR_CANT_PEND_RECEP," & vbCrLf
    sConsulta = sConsulta & "USU.FSEP_RECEP_VER_PED_USU,USU.FSEP_RECEP_VER_PED_CCIMP,IDIOMAS.LANGUAGETAG " & vbCrLf
    sConsulta = sConsulta & "FROM USU WITH (NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "LEFT JOIN PER WITH (NOLOCK) ON USU.PER=PER.COD" & vbCrLf
    sConsulta = sConsulta & "INNER JOIN (SELECT IDIOMA,DEN FROM UNQA U WITH(NOLOCK) " & vbCrLf
    sConsulta = sConsulta & "           INNER JOIN UNQA_IDIOMAS UI WITH(NOLOCK) " & vbCrLf
    sConsulta = sConsulta & "           ON UI.UNQA=U.ID AND U.NIVEL=0) UNQAD" & vbCrLf
    sConsulta = sConsulta & "ON UNQAD.IDIOMA=USU.IDIOMA " & vbCrLf
    sConsulta = sConsulta & "LEFT JOIN PERF P WITH(NOLOCK) ON P.ID=USU.PERF" & vbCrLf
    sConsulta = sConsulta & "LEFT JOIN IDIOMAS WITH (NOLOCK) ON USU.IDIOMA = IDIOMAS.COD" & vbCrLf
    sConsulta = sConsulta & "WHERE USU.COD=@COD AND USU.BAJA=0" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SELECT @PERF=PERF FROM USU WITH(NOLOCK) WHERE COD=@COD" & vbCrLf
    sConsulta = sConsulta & "IF @PERF IS NULL" & vbCrLf
    sConsulta = sConsulta & "   SELECT ACC FROM USU_ACC WITH(NOLOCK) WHERE USU=@COD" & vbCrLf
    sConsulta = sConsulta & "ELSE" & vbCrLf
    sConsulta = sConsulta & "   SELECT ACC FROM PERF_ACC WITH(NOLOCK) WHERE PERF=@PERF" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SELECT COUNT(UC.USU) CONTROLPRESUPUESTARIO" & vbCrLf
    sConsulta = sConsulta & "FROM USU WITH (NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "LEFT JOIN USU_CC_CONTROL UC WITH (NOLOCK) ON UC.USU=USU.COD AND (UC.CONSULTA=1 OR UC.MODIF=1 OR UC.ALTA=1 OR UC.PRESUP=1)" & vbCrLf
    sConsulta = sConsulta & "WHERE USU.COD=@COD" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_SOLICITUD_DATOS_EMAIL]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_SOLICITUD_DATOS_EMAIL]" & vbCrLf
    gRDOCon.QueryTimeout = 7200
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "CREATE PROCEDURE [dbo].[FSPM_SOLICITUD_DATOS_EMAIL] @ID INT, @TYPE TINYINT, @CONTACTO INT = 0, @BLOQUE AS INT = 0, @ACCION INT=0, @ENLACE INT=0 AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "IF @TYPE = 0   --Es un traslado:" & vbCrLf
    sConsulta = sConsulta & "       IF @CONTACTO=0" & vbCrLf
    sConsulta = sConsulta & "         SELECT TOP 1 WEBFSWSTEXT.MODULO, WEBFSWSTEXT.ID, WEBFSWSTEXT.TEXT_ENG, WEBFSWSTEXT.TEXT_GER, WEBFSWSTEXT.TEXT_SPA, U.COD,ISNULL(U.IDIOMA,'ENG') IDIOMA," & vbCrLf
    sConsulta = sConsulta & "       PER.EMAIL, U.TIPOEMAIL, U.DATEFMT, U.THOUSANFMT, U.DECIMALFMT, U.PRECISIONFMT, 0 AS ESPROVEEDOR,PER.COD CODDESTINATARIO,ISNULL(IDIOMAS.LANGUAGETAG,'en-GB') LANGUAGETAG " & vbCrLf
    sConsulta = sConsulta & "         FROM INSTANCIA I WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "    INNER JOIN INSTANCIA_EST IE WITH(NOLOCK) ON I.ID=IE.INSTANCIA AND IE.ACCION = 3" & vbCrLf
    sConsulta = sConsulta & "    INNER JOIN INSTANCIA_BLOQUE IB WITH(NOLOCK) ON IE.BLOQUE = IB.ID AND IB.ID = @BLOQUE AND IB.ESTADO=1" & vbCrLf
    sConsulta = sConsulta & "    INNER JOIN PER WITH(NOLOCK) ON IE.DESTINATARIO=PER.COD" & vbCrLf
    sConsulta = sConsulta & "    LEFT JOIN USU U WITH(NOLOCK) ON PER.COD = U.PER AND U.BAJA = 0" & vbCrLf
    sConsulta = sConsulta & "    LEFT JOIN WEBFSWSTEXT WITH(NOLOCK) ON MODULO=101 AND WEBFSWSTEXT.ID=1" & vbCrLf
    sConsulta = sConsulta & "    LEFT JOIN IDIOMAS WITH(NOLOCK) ON IDIOMAS.COD=U.IDIOMA " & vbCrLf
    sConsulta = sConsulta & "         WHERE I.ID=@ID" & vbCrLf
    sConsulta = sConsulta & "    ORDER BY IE.ID DESC" & vbCrLf
    sConsulta = sConsulta & "       ELSE" & vbCrLf
    sConsulta = sConsulta & "         begin" & vbCrLf
    sConsulta = sConsulta & "           DECLARE @DATEFMT AS VARCHAR(50)" & vbCrLf
    sConsulta = sConsulta & "           DECLARE @THOUSANFMT AS CHAR(1)" & vbCrLf
    sConsulta = sConsulta & "           DECLARE @DECIMALFMT AS CHAR(1)" & vbCrLf
    sConsulta = sConsulta & "           DECLARE @PRECISIONFMT AS TINYINT" & vbCrLf
    sConsulta = sConsulta & "           DECLARE @COMP VARCHAR(50)" & vbCrLf
    sConsulta = sConsulta & "           DECLARE @FSP NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "           DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "           DECLARE @CON_PORTAL INT" & vbCrLf
    sConsulta = sConsulta & "           DECLARE @PROVE VARCHAR(50)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "           SET @DATEFMT=NULL" & vbCrLf
    sConsulta = sConsulta & "           SET @THOUSANFMT=NULL" & vbCrLf
    sConsulta = sConsulta & "           SET @DECIMALFMT=NULL" & vbCrLf
    sConsulta = sConsulta & "           SET @PRECISIONFMT=NULL" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "           SELECT TOP 1 @CON_PORTAL=ID_PORT, @PROVE=CON.PROVE" & vbCrLf
    sConsulta = sConsulta & "      FROM INSTANCIA I WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "      INNER JOIN INSTANCIA_EST IE WITH(NOLOCK) ON I.ID=IE.INSTANCIA AND IE.ACCION = 3" & vbCrLf
    sConsulta = sConsulta & "      INNER JOIN INSTANCIA_BLOQUE IB WITH(NOLOCK) ON IE.BLOQUE = IB.ID AND IB.ID = @BLOQUE AND IB.ESTADO = 1" & vbCrLf
    sConsulta = sConsulta & "      INNER JOIN CON WITH(NOLOCK) ON IE.DESTINATARIO_PROV=CON.PROVE" & vbCrLf
    sConsulta = sConsulta & "      WHERE I.ID=@ID AND CON.ID=@CONTACTO" & vbCrLf
    sConsulta = sConsulta & "      ORDER BY IE.ID DESC" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "           IF @CON_PORTAL IS NOT NULL" & vbCrLf
    sConsulta = sConsulta & "             begin" & vbCrLf
    sConsulta = sConsulta & "                 SELECT @FSP = FSP_SRV + '.' + FSP_BD + '.dbo.',  @COMP  = FSP_CIA FROM PARGEN_PORT WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "                 SET @SQL = N'EXEC ' +  @FSP + 'FSPM_FORMATO_CONTACTO @CON_PORTAL=@CON_PORTAL,@COMP =@COMP ,@PROVE =" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "@PROVE,@DATEFMT=@DATEFMT OUTPUT,@THOUSANFMT=@THOUSANFMT OUTPUT,@DECIMALFMT=@DECIMALFMT" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "OUTPUT,@PRECISIONFMT=@PRECISIONFMT OUTPUT'" & vbCrLf
    sConsulta = sConsulta & "            EXEC SP_EXECUTESQL @SQL, N'@CON_PORTAL INT,@COMP VARCHAR(50),@PROVE VARCHAR(50) ,@DATEFMT" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "VARCHAR(50) OUTPUT, @THOUSANFMT CHAR(1) OUTPUT, @DECIMALFMT CHAR(1) OUTPUT, @PRECISIONFMT TINYINT OUTPUT'," & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "@CON_PORTAL=@CON_PORTAL,@COMP = @COMP, @PROVE = @PROVE,@DATEFMT=@DATEFMT OUTPUT,@THOUSANFMT=@THOUSANFMT" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "OUTPUT,@DECIMALFMT=@DECIMALFMT OUTPUT,@PRECISIONFMT=@PRECISIONFMT OUTPUT" & vbCrLf
    sConsulta = sConsulta & "             end" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "           SELECT TOP 1 WEBFSWSTEXT.MODULO, WEBFSWSTEXT.ID, WEBFSWSTEXT.TEXT_ENG, WEBFSWSTEXT.TEXT_GER, WEBFSWSTEXT.TEXT_SPA," & vbCrLf
    sConsulta = sConsulta & "       CASE WHEN CON.IDI IS NULL THEN" & vbCrLf
    sConsulta = sConsulta & "           (SELECT IDIOMA FROM PARGEN_DEF WITH(NOLOCK) WHERE PARGEN_DEF.ID=1)" & vbCrLf
    sConsulta = sConsulta & "       ELSE CON.IDI" & vbCrLf
    sConsulta = sConsulta & "       END IDIOMA," & vbCrLf
    sConsulta = sConsulta & "       CON.TIPOEMAIL, CON.EMAIL, @DATEFMT AS DATEFMT, @THOUSANFMT AS THOUSANFMT, @DECIMALFMT AS DECIMALFMT," & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "@PRECISIONFMT AS PRECISIONFMT, 1 AS ESPROVEEDOR,NULL CODDESTINATARIO,IDIOMAS.LANGUAGETAG " & vbCrLf
    sConsulta = sConsulta & "           FROM INSTANCIA I WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "      INNER JOIN INSTANCIA_EST IE WITH(NOLOCK) ON I.INSTANCIA_EST=IE.ID AND IE.ACCION = 3" & vbCrLf
    sConsulta = sConsulta & "          INNER JOIN INSTANCIA_BLOQUE IB WITH(NOLOCK) ON IE.BLOQUE = IB.ID AND IB.ID = @BLOQUE AND IB.ESTADO = 1" & vbCrLf
    sConsulta = sConsulta & "           INNER JOIN CON WITH(NOLOCK) ON IE.DESTINATARIO_PROV=CON.PROVE" & vbCrLf
    sConsulta = sConsulta & "      LEFT JOIN WEBFSWSTEXT WITH(NOLOCK) ON MODULO=101 AND WEBFSWSTEXT.ID=1" & vbCrLf
    sConsulta = sConsulta & "      LEFT JOIN IDIOMAS WITH(NOLOCK) ON IDIOMAS.COD = CON.ID" & vbCrLf
    sConsulta = sConsulta & "           WHERE I.ID=@ID  AND CON.ID=@CONTACTO" & vbCrLf
    sConsulta = sConsulta & "      " & vbCrLf
    sConsulta = sConsulta & "      ORDER BY IE.ID DESC" & vbCrLf
    sConsulta = sConsulta & "        end" & vbCrLf
    sConsulta = sConsulta & "ELSE" & vbCrLf
    sConsulta = sConsulta & "  IF @TYPE = 1  -- Es una Devolucion" & vbCrLf
    sConsulta = sConsulta & "   BEGIN" & vbCrLf
    sConsulta = sConsulta & "     SELECT TOP 1 WEBFSWSTEXT.MODULO, WEBFSWSTEXT.ID, WEBFSWSTEXT.TEXT_ENG, WEBFSWSTEXT.TEXT_GER, WEBFSWSTEXT.TEXT_SPA," & vbCrLf
    sConsulta = sConsulta & "       U.IDIOMA,U.TIPOEMAIL, PER.EMAIL, U.DATEFMT, U.THOUSANFMT, U.DECIMALFMT, U.PRECISIONFMT, 0 AS ESPROVEEDOR,PER.COD CODDESTINATARIO,IDIOMAS.LANGUAGETAG " & vbCrLf
    sConsulta = sConsulta & "          FROM INSTANCIA I WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "     INNER JOIN INSTANCIA_EST IE WITH(NOLOCK) ON I.ID=IE.INSTANCIA AND IE.ACCION = 4" & vbCrLf
    sConsulta = sConsulta & "     INNER JOIN INSTANCIA_BLOQUE IB WITH(NOLOCK) ON IE.BLOQUE = IB.ID AND IB.ID = @BLOQUE AND IB.ESTADO = 1" & vbCrLf
    sConsulta = sConsulta & "     INNER JOIN PER WITH(NOLOCK) ON IE.DESTINATARIO = PER.COD" & vbCrLf
    sConsulta = sConsulta & "     LEFT JOIN USU U WITH(NOLOCK) ON PER.COD = U.PER AND U.BAJA = 0" & vbCrLf
    sConsulta = sConsulta & "     LEFT JOIN WEBFSWSTEXT WITH(NOLOCK) ON MODULO=101 AND WEBFSWSTEXT.ID=2" & vbCrLf
    sConsulta = sConsulta & "     LEFT JOIN IDIOMAS WITH(NOLOCK) ON IDIOMAS.COD=U.IDIOMA " & vbCrLf
    sConsulta = sConsulta & "          WHERE I.ID=@ID ORDER BY IE.ID DESC" & vbCrLf
    sConsulta = sConsulta & "   END" & vbCrLf
    sConsulta = sConsulta & "ELSE" & vbCrLf
    sConsulta = sConsulta & "  IF @TYPE = 2  --NOTIFICADOS DE ACCION" & vbCrLf
    sConsulta = sConsulta & "  BEGIN" & vbCrLf
    sConsulta = sConsulta & "   CREATE TABLE #FSPM_DATOS_MAIL_ACCION (" & vbCrLf
    sConsulta = sConsulta & "       TEXT varchar(200)," & vbCrLf
    sConsulta = sConsulta & "       IDIOMA varchar(3)," & vbCrLf
    sConsulta = sConsulta & "       TIPOEMAIL tinyint," & vbCrLf
    sConsulta = sConsulta & "       EMAIL varchar(100)," & vbCrLf
    sConsulta = sConsulta & "       DATEFMT varchar(50)," & vbCrLf
    sConsulta = sConsulta & "       THOUSANFMT char(1)," & vbCrLf
    sConsulta = sConsulta & "       DECIMALFMT char(1)," & vbCrLf
    sConsulta = sConsulta & "       PRECISIONFMT tinyint," & vbCrLf
    sConsulta = sConsulta & "       ESPROVEEDOR int," & vbCrLf
    sConsulta = sConsulta & "       CODDESTINATARIO varchar(20)," & vbCrLf
    sConsulta = sConsulta & "       NOTIFICADO int," & vbCrLf
    sConsulta = sConsulta & "       CODUSUARIO varchar(50)" & vbCrLf
    sConsulta = sConsulta & "   )" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   INSERT INTO #FSPM_DATOS_MAIL_ACCION (TEXT, IDIOMA, TIPOEMAIL, EMAIL, DATEFMT, THOUSANFMT, DECIMALFMT, PRECISIONFMT, ESPROVEEDOR, CODDESTINATARIO, NOTIFICADO, CODUSUARIO)" & vbCrLf
    sConsulta = sConsulta & "--Notificados: Especificos" & vbCrLf
    sConsulta = sConsulta & "   SELECT DISTINCT ISNULL(AE.SUBJECT,'') AS TEXT," & vbCrLf
    sConsulta = sConsulta & "       U.IDIOMA,U.TIPOEMAIL, P.EMAIL, U.DATEFMT, U.THOUSANFMT, U.DECIMALFMT, U.PRECISIONFMT, 0 AS ESPROVEEDOR,P.COD CODDESTINATARIO, NA.ID AS NOTIFICADO, U.COD" & vbCrLf
    sConsulta = sConsulta & "   FROM PER P WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   LEFT JOIN USU U WITH(NOLOCK) ON P.COD=U.PER AND U.BAJA = 0" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_NOTIFICADO_ACCION NA WITH(NOLOCK) ON P.COD=NA.PER" & vbCrLf
    sConsulta = sConsulta & "   LEFT JOIN PM_ACCIONES_EMAIL AE WITH(NOLOCK) ON NA.ACCION=AE.ACCION AND AE.IDI=U.IDIOMA" & vbCrLf
    sConsulta = sConsulta & "    INNER JOIN PM_COPIA_ACCIONES A WITH(NOLOCK) ON AE.ACCION=A.ACCION_ORIGEN" & vbCrLf
    sConsulta = sConsulta & "   WHERE NA.TIPO_NOTIFICADO=1 AND A.ID=@ACCION" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "--Notificados: Roles(Proveedor)" & vbCrLf
    sConsulta = sConsulta & "   UNION" & vbCrLf
    sConsulta = sConsulta & "   SELECT DISTINCT ISNULL(AE.SUBJECT,'') AS TEXT," & vbCrLf
    sConsulta = sConsulta & "       ISNULL(C.IDI,(SELECT IDIOMA FROM PARGEN_DEF WITH(NOLOCK) WHERE PARGEN_DEF.ID=1)) IDIOMA," & vbCrLf
    sConsulta = sConsulta & "       C.TIPOEMAIL, C.EMAIL, C.DATEFMT, C.THOUSANFMT, C.DECIMALFMT, C.PRECISIONFMT, 1 AS ESPROVEEDOR,NULL CODDESTINATARIO, NA.ID AS NOTIFICADO, NULL CODUSUARIO" & vbCrLf
    sConsulta = sConsulta & "   FROM CON C WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL R WITH(NOLOCK) ON C.PROVE=R.PROVE" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL_CON RC WITH(NOLOCK) ON R.ID=RC.ID_ROL AND C.ID=RC.ID_CON" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_ROL ROL WITH(NOLOCK) ON R.ROL_ORIGEN=ROL.ID" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_ACCIONES_EMAIL AE WITH(NOLOCK) ON AE.IDI=ISNULL(C.IDI,(SELECT IDIOMA FROM PARGEN_DEF WITH(NOLOCK) WHERE PARGEN_DEF.ID=1))" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_NOTIFICADO_ACCION NA WITH(NOLOCK) ON AE.ACCION=NA.ACCION AND ROL.ID=NA.ROL" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ACCIONES A WITH(NOLOCK) ON NA.ACCION=A.ACCION_ORIGEN" & vbCrLf
    sConsulta = sConsulta & "   WHERE R.TIPO = 2 AND NA.TIPO_NOTIFICADO=2 AND A.ID=@ACCION AND R.INSTANCIA=@ID" & vbCrLf
    sConsulta = sConsulta & "   UNION" & vbCrLf
    sConsulta = sConsulta & "   SELECT DISTINCT ISNULL(AE.SUBJECT,'') AS TEXT," & vbCrLf
    sConsulta = sConsulta & "   ISNULL(C.IDI,(SELECT IDIOMA FROM PARGEN_DEF WITH(NOLOCK) WHERE PARGEN_DEF.ID=1)) IDIOMA," & vbCrLf
    sConsulta = sConsulta & "   C.TIPOEMAIL, C.EMAIL, C.DATEFMT, C.THOUSANFMT, C.DECIMALFMT, C.PRECISIONFMT, 1 AS ESPROVEEDOR,NULL CODDESTINATARIO, NA.ID AS NOTIFICADO, NULL CODUSUARIO" & vbCrLf
    sConsulta = sConsulta & "   FROM CON C WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL R WITH(NOLOCK) ON C.PROVE=R.PROVE " & vbCrLf
    sConsulta = sConsulta & "   AND NOT EXISTS (SELECT ID_CON FROM PM_COPIA_ROL_CON RC WITH(NOLOCK) WHERE RC.ID_ROL = R.ID)" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_ROL ROL WITH(NOLOCK) ON R.ROL_ORIGEN=ROL.ID" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_ACCIONES_EMAIL AE WITH(NOLOCK) ON AE.IDI=ISNULL(C.IDI,(SELECT IDIOMA FROM PARGEN_DEF WITH(NOLOCK) WHERE PARGEN_DEF.ID=1))" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_NOTIFICADO_ACCION NA WITH(NOLOCK) ON AE.ACCION=NA.ACCION AND ROL.ID=NA.ROL" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ACCIONES A WITH(NOLOCK) ON NA.ACCION=A.ACCION_ORIGEN" & vbCrLf
    sConsulta = sConsulta & "   WHERE R.TIPO = 2 AND NA.TIPO_NOTIFICADO=2 AND A.ID=@ACCION AND R.INSTANCIA=@ID" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "--Notificados: Roles(Comprador, Usuario, Peticionario)" & vbCrLf
    sConsulta = sConsulta & "   UNION" & vbCrLf
    sConsulta = sConsulta & "   SELECT DISTINCT ISNULL(AE.SUBJECT,'') AS TEXT," & vbCrLf
    sConsulta = sConsulta & "       U.IDIOMA," & vbCrLf
    sConsulta = sConsulta & "       U.TIPOEMAIL, P.EMAIL, U.DATEFMT, U.THOUSANFMT, U.DECIMALFMT, U.PRECISIONFMT, 0 AS ESPROVEEDOR,P.COD CODDESTINATARIO, NA.ID AS NOTIFICADO, U.COD" & vbCrLf
    sConsulta = sConsulta & "   FROM PER P WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   LEFT JOIN USU U WITH(NOLOCK) ON P.COD=U.PER AND U.BAJA = 0" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL R WITH(NOLOCK) ON P.COD=R.PER" & vbCrLf
    sConsulta = sConsulta & "    INNER JOIN PM_ROL ROL WITH(NOLOCK) ON R.ROL_ORIGEN=ROL.ID" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_NOTIFICADO_ACCION NA WITH(NOLOCK) ON ROL.ID=NA.ROL" & vbCrLf
    sConsulta = sConsulta & "   LEFT OUTER JOIN PM_ACCIONES_EMAIL AE WITH(NOLOCK) ON NA.ACCION=AE.ACCION AND AE.IDI=U.IDIOMA" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ACCIONES A WITH(NOLOCK) ON NA.ACCION=A.ACCION_ORIGEN" & vbCrLf
    sConsulta = sConsulta & "   WHERE R.TIPO <> 2 AND NA.TIPO_NOTIFICADO=2 AND A.ID=@ACCION AND R.INSTANCIA=@ID" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "--Notificados: Roles( Lista de autoasignaci�n)" & vbCrLf
    sConsulta = sConsulta & "   UNION" & vbCrLf
    sConsulta = sConsulta & "   SELECT DISTINCT ISNULL(AE.SUBJECT,'') AS TEXT," & vbCrLf
    sConsulta = sConsulta & "                   U.IDIOMA,U.TIPOEMAIL, P.EMAIL, U.DATEFMT, U.THOUSANFMT, U.DECIMALFMT, U.PRECISIONFMT, 0 AS ESPROVEEDOR,P.COD CODDESTINATARIO, NA.ID AS NOTIFICADO, U.COD" & vbCrLf
    sConsulta = sConsulta & "   FROM PM_COPIA_ROL R WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "    INNER JOIN PM_ROL ROL WITH(NOLOCK) ON R.ROL_ORIGEN=ROL.ID" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_NOTIFICADO_ACCION NA WITH(NOLOCK) ON ROL.ID=NA.ROL" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_PARTICIPANTES PCP WITH(NOLOCK)ON NA.ROL=PCP.ROL" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PER P WITH(NOLOCK) ON PCP.PER = P.COD" & vbCrLf
    sConsulta = sConsulta & "   LEFT JOIN USU U WITH(NOLOCK) ON P.COD = U.PER AND U.BAJA = 0" & vbCrLf
    sConsulta = sConsulta & "   LEFT OUTER JOIN PM_ACCIONES_EMAIL AE WITH(NOLOCK) ON NA.ACCION=AE.ACCION AND AE.IDI=U.IDIOMA" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ACCIONES A WITH(NOLOCK) ON NA.ACCION=A.ACCION_ORIGEN" & vbCrLf
    sConsulta = sConsulta & "              WHERE R.TIPO =3 AND NA.TIPO_NOTIFICADO=2 AND A.ID=@ACCION  AND R.COMO_ASIGNAR=3 AND R.INSTANCIA=@ID" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "--Notificados: Roles(GESTOR DE LA FACTURA)" & vbCrLf
    sConsulta = sConsulta & "   UNION" & vbCrLf
    sConsulta = sConsulta & "   SELECT DISTINCT ISNULL(AE.SUBJECT,'') AS TEXT," & vbCrLf
    sConsulta = sConsulta & "   U.IDIOMA,U.TIPOEMAIL, P.EMAIL, U.DATEFMT, U.THOUSANFMT, U.DECIMALFMT, U.PRECISIONFMT, 0 AS ESPROVEEDOR,P.COD CODDESTINATARIO, NA.ID AS NOTIFICADO, U.COD" & vbCrLf
    sConsulta = sConsulta & "   FROM PER P WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   LEFT JOIN USU U WITH(NOLOCK) ON P.COD=U.PER AND U.BAJA = 0" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PRES5_IMPORTES PI WITH(NOLOCK) ON PI.GESTOR = P.COD" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN LINEAS_PED_IMPUTACION LPI WITH(NOLOCK) ON LPI.PRES5_IMP = PI.ID" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN LINEAS_PEDIDO_FACTURA LPF WITH(NOLOCK) ON LPF.LINEA_PEDIDO = LPI.LINEA" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN LIN_FACTURA lf with(nolock) on lf.FACTURA = lpf.FACTURA" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN FACTURA F WITH(NOLOCK) ON F.ID = lPF.FACTURA" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL r with(nolock) on r.INSTANCIA = f.INSTANCIA" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_ROL ROL WITH(NOLOCK) ON R.ROL_ORIGEN=ROL.ID" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_NOTIFICADO_ACCION NA WITH(NOLOCK) ON ROL.ID=NA.ROL" & vbCrLf
    sConsulta = sConsulta & "   LEFT OUTER JOIN PM_ACCIONES_EMAIL AE WITH(NOLOCK) ON NA.ACCION=AE.ACCION AND AE.IDI=U.IDIOMA" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ACCIONES A WITH(NOLOCK) ON NA.ACCION=A.ACCION_ORIGEN" & vbCrLf
    sConsulta = sConsulta & "   WHERE F.INSTANCIA = @ID and A.ID=@ACCION and R.TIPO = 6 and R.GESTOR_FACTURA = 1 AND NA.TIPO_NOTIFICADO=2" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "--Notificado: roles observadores definidos como UON o Departamento" & vbCrLf
    sConsulta = sConsulta & "   UNION" & vbCrLf
    sConsulta = sConsulta & "   SELECT DISTINCT ISNULL(AE.SUBJECT,'') AS TEXT," & vbCrLf
    sConsulta = sConsulta & "                   U.IDIOMA,U.TIPOEMAIL, P.EMAIL, U.DATEFMT, U.THOUSANFMT, U.DECIMALFMT, U.PRECISIONFMT, 0 AS ESPROVEEDOR,P.COD CODDESTINATARIO, NA.ID AS NOTIFICADO, U.COD" & vbCrLf
    sConsulta = sConsulta & "   FROM PER P WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   LEFT JOIN USU U WITH(NOLOCK) ON P.COD=U.PER AND U.BAJA=0" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL R WITH(NOLOCK) ON R.CUANDO_ASIGNAR =2 AND R.PER IS NULL" & vbCrLf
    sConsulta = sConsulta & "   AND ISNULL(R.UON1,'') =ISNULL(P.UON1,'') AND  ISNULL(R.UON2,'') =ISNULL(P.UON2,'') AND ISNULL(R.UON3,'') =ISNULL(P.UON3,'') AND (R.DEP =P.DEP OR R.DEP IS NULL)" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_ROL ROL WITH(NOLOCK) ON R.ROL_ORIGEN=ROL.ID" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_NOTIFICADO_ACCION NA WITH(NOLOCK) ON NA.ROL=ROL.ID" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ACCIONES A WITH(NOLOCK) ON NA.ACCION=A.ACCION_ORIGEN" & vbCrLf
    sConsulta = sConsulta & "   LEFT OUTER JOIN PM_ACCIONES_EMAIL AE WITH(NOLOCK) ON NA.ACCION=AE.ACCION AND AE.IDI=U.IDIOMA" & vbCrLf
    sConsulta = sConsulta & "   WHERE R.TIPO =5 AND R.INSTANCIA =@ID AND NA.TIPO_NOTIFICADO=2 AND A.ID=@ACCION " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "--Notificado: Peticionario" & vbCrLf
    sConsulta = sConsulta & "   UNION" & vbCrLf
    sConsulta = sConsulta & "   SELECT DISTINCT ISNULL(AE.SUBJECT,'') AS TEXT," & vbCrLf
    sConsulta = sConsulta & "       U.IDIOMA,U.TIPOEMAIL, P.EMAIL, U.DATEFMT, U.THOUSANFMT, U.DECIMALFMT, U.PRECISIONFMT, 0 AS ESPROVEEDOR,P.COD CODDESTINATARIO, NA.ID AS NOTIFICADO, U.COD" & vbCrLf
    sConsulta = sConsulta & "   FROM PER P WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   LEFT JOIN USU U WITH(NOLOCK) ON P.COD=U.PER AND U.BAJA=0" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN INSTANCIA I WITH(NOLOCK) ON P.COD=I.PETICIONARIO" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN INSTANCIA_EST IE WITH(NOLOCK) ON I.ID=IE.INSTANCIA" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ACCIONES A WITH(NOLOCK) ON IE.PM_ACCION=A.ID" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_NOTIFICADO_ACCION NA WITH(NOLOCK) ON A.ACCION_ORIGEN=NA.ACCION" & vbCrLf
    sConsulta = sConsulta & "   LEFT OUTER JOIN PM_ACCIONES_EMAIL AE WITH(NOLOCK) ON NA.ACCION=AE.ACCION AND AE.IDI=U.IDIOMA" & vbCrLf
    sConsulta = sConsulta & "   WHERE NA.TIPO_NOTIFICADO = 3 AND A.ID = @ACCION AND I.ID=@ID" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "--Notificado: Autor de la accion (Rol Proveedor)" & vbCrLf
    sConsulta = sConsulta & "   UNION" & vbCrLf
    sConsulta = sConsulta & "   SELECT DISTINCT ISNULL(AE.SUBJECT,'') AS TEXT," & vbCrLf
    sConsulta = sConsulta & "       ISNULL(C.IDI,(SELECT IDIOMA FROM PARGEN_DEF WITH(NOLOCK) WHERE PARGEN_DEF.ID=1)) IDIOMA," & vbCrLf
    sConsulta = sConsulta & "       C.TIPOEMAIL, C.EMAIL, C.DATEFMT, C.THOUSANFMT, C.DECIMALFMT, C.PRECISIONFMT, 1 AS ESPROVEEDOR,NULL CODDESTINATARIO, NA.ID AS NOTIFICADO, NULL CODUSUARIO" & vbCrLf
    sConsulta = sConsulta & "   FROM CON C WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL R WITH(NOLOCK) ON C.PROVE=R.PROVE" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL_CON RC WITH(NOLOCK) ON R.ID=RC.ID_ROL AND C.ID=RC.ID_CON" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN INSTANCIA_EST IE WITH(NOLOCK) ON R.ID=IE.ROL" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ACCIONES A WITH(NOLOCK) ON IE.PM_ACCION=A.ID" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_NOTIFICADO_ACCION NA WITH(NOLOCK) ON A.ACCION_ORIGEN=NA.ACCION" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_ACCIONES_EMAIL AE WITH(NOLOCK) ON NA.ACCION=AE.ACCION AND AE.IDI=ISNULL(C.IDI,(SELECT IDIOMA FROM PARGEN_DEF WITH(NOLOCK) WHERE PARGEN_DEF.ID=1))" & vbCrLf
    sConsulta = sConsulta & "   WHERE R.TIPO =2 AND NA.TIPO_NOTIFICADO=4 AND A.ID=@ACCION AND R.INSTANCIA=@ID" & vbCrLf
    sConsulta = sConsulta & "   UNION" & vbCrLf
    sConsulta = sConsulta & "   SELECT DISTINCT ISNULL(AE.SUBJECT,'') AS TEXT," & vbCrLf
    sConsulta = sConsulta & "        ISNULL(C.IDI,(SELECT IDIOMA FROM PARGEN_DEF WITH(NOLOCK) WHERE PARGEN_DEF.ID=1)) IDIOMA," & vbCrLf
    sConsulta = sConsulta & "        C.TIPOEMAIL, C.EMAIL, C.DATEFMT, C.THOUSANFMT, C.DECIMALFMT, C.PRECISIONFMT, 1 AS ESPROVEEDOR,NULL CODDESTINATARIO, NA.ID AS NOTIFICADO, NULL CODUSUARIO" & vbCrLf
    sConsulta = sConsulta & "   FROM CON C WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL R WITH(NOLOCK) ON C.PROVE=R.PROVE" & vbCrLf
    sConsulta = sConsulta & "   AND NOT EXISTS (SELECT ID_CON FROM PM_COPIA_ROL_CON RC WITH(NOLOCK) WHERE RC.ID_ROL = R.ID)" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN INSTANCIA_EST IE WITH(NOLOCK) ON R.ID=IE.ROL" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ACCIONES A WITH(NOLOCK) ON IE.PM_ACCION=A.ID" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_NOTIFICADO_ACCION NA WITH(NOLOCK) ON A.ACCION_ORIGEN=NA.ACCION" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_ACCIONES_EMAIL AE WITH(NOLOCK) ON NA.ACCION=AE.ACCION AND AE.IDI=ISNULL(C.IDI,(SELECT IDIOMA FROM PARGEN_DEF WITH(NOLOCK) WHERE PARGEN_DEF.ID=1))" & vbCrLf
    sConsulta = sConsulta & "   WHERE R.TIPO =2 AND NA.TIPO_NOTIFICADO=4 AND A.ID=@ACCION AND R.INSTANCIA=@ID" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "--Notificado: Autor de la accion (Rol Comprador, Usuario, Peticionario)" & vbCrLf
    sConsulta = sConsulta & "   UNION" & vbCrLf
    sConsulta = sConsulta & "   SELECT DISTINCT ISNULL(AE.SUBJECT,'') AS TEXT," & vbCrLf
    sConsulta = sConsulta & "       U.IDIOMA,U.TIPOEMAIL, P.EMAIL, U.DATEFMT, U.THOUSANFMT, U.DECIMALFMT, U.PRECISIONFMT, 0 AS ESPROVEEDOR,P.COD CODDESTINATARIO, NA.ID AS NOTIFICADO, U.COD" & vbCrLf
    sConsulta = sConsulta & "   FROM PER P WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   LEFT JOIN USU U WITH(NOLOCK) ON P.COD=U.PER AND U.BAJA=0" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL R WITH(NOLOCK) ON P.COD=R.PER" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN INSTANCIA_EST IE WITH(NOLOCK) ON R.ID=IE.ROL" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ACCIONES A WITH(NOLOCK) ON IE.PM_ACCION = A.ID" & vbCrLf
    sConsulta = sConsulta & "    INNER JOIN PM_NOTIFICADO_ACCION NA WITH(NOLOCK) ON A.ACCION_ORIGEN=NA.ACCION" & vbCrLf
    sConsulta = sConsulta & "   LEFT OUTER JOIN PM_ACCIONES_EMAIL AE WITH(NOLOCK) ON NA.ACCION=AE.ACCION AND AE.IDI=U.IDIOMA" & vbCrLf
    sConsulta = sConsulta & "   WHERE R.TIPO<>2 AND NA.TIPO_NOTIFICADO=4 AND A.ID=@ACCION AND R.INSTANCIA=@ID" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "--Notificados: Personas que participaron en la etapa Origen" & vbCrLf
    sConsulta = sConsulta & "   UNION" & vbCrLf
    sConsulta = sConsulta & "   --Peticionario  Usuario" & vbCrLf
    sConsulta = sConsulta & "   SELECT DISTINCT ISNULL(AE.SUBJECT,'') AS TEXT," & vbCrLf
    sConsulta = sConsulta & "       U.IDIOMA,U.TIPOEMAIL, P.EMAIL, U.DATEFMT, U.THOUSANFMT, U.DECIMALFMT, U.PRECISIONFMT, 0 AS ESPROVEEDOR,P.COD CODDESTINATARIO, NA.ID AS NOTIFICADO, U.COD" & vbCrLf
    sConsulta = sConsulta & "   FROM PER P WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL R WITH(NOLOCK) ON P.COD=R.PER" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL_ACCION RA WITH(NOLOCK) ON R.ID=RA.ROL" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_BLOQUE B WITH(NOLOCK) ON RA.BLOQUE=B.ID AND R.INSTANCIA=B.INSTANCIA" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ACCIONES A WITH(NOLOCK) ON RA.ACCION=A.ID AND B.ID=A.BLOQUE" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_NOTIFICADO_ACCION NA WITH(NOLOCK) ON A.ACCION_ORIGEN=NA.ACCION" & vbCrLf
    sConsulta = sConsulta & "   LEFT JOIN USU U WITH(NOLOCK) ON P.COD=U.PER AND U.BAJA = 0" & vbCrLf
    sConsulta = sConsulta & "   LEFT OUTER JOIN PM_ACCIONES_EMAIL AE WITH(NOLOCK) ON NA.ACCION=AE.ACCION AND AE.IDI=U.IDIOMA" & vbCrLf
    sConsulta = sConsulta & "   WHERE R.TIPO=4 AND R.PER IS NOT NULL AND NA.TIPO_NOTIFICADO=5 AND RA.ACCION=@ACCION AND R.INSTANCIA=@ID" & vbCrLf
    sConsulta = sConsulta & "   UNION" & vbCrLf
    sConsulta = sConsulta & "   --Peticionario  Dep" & vbCrLf
    sConsulta = sConsulta & "   SELECT DISTINCT ISNULL(AE.SUBJECT,'') AS TEXT," & vbCrLf
    sConsulta = sConsulta & "       U.IDIOMA,U.TIPOEMAIL, P.EMAIL, U.DATEFMT, U.THOUSANFMT, U.DECIMALFMT, U.PRECISIONFMT, 0 AS ESPROVEEDOR,P.COD CODDESTINATARIO, NA.ID AS NOTIFICADO, U.COD" & vbCrLf
    sConsulta = sConsulta & "   FROM PER P WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PER P2 WITH(NOLOCK) ON P.COD=P2.COD" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN DEP D WITH(NOLOCK) ON P2.DEP=D.COD" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN USU U2 WITH(NOLOCK) ON P2.COD=U2.PER AND U2.BAJA=0" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL R WITH(NOLOCK) ON D.COD=R.DEP" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_BLOQUE B WITH(NOLOCK) ON R.INSTANCIA=B.INSTANCIA" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL_ACCION RA WITH(NOLOCK) ON R.ID=RA.ROL AND B.ID=RA.BLOQUE" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ACCIONES CA WITH(NOLOCK) ON RA.ACCION=CA.ID AND B.ID=CA.BLOQUE" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_NOTIFICADO_ACCION NA WITH(NOLOCK) ON CA.ACCION_ORIGEN=NA.ACCION" & vbCrLf
    sConsulta = sConsulta & "   LEFT JOIN USU U WITH(NOLOCK) ON P.COD=U.PER AND U.BAJA=0" & vbCrLf
    sConsulta = sConsulta & "   LEFT OUTER JOIN PM_ACCIONES_EMAIL AE WITH(NOLOCK) ON NA.ACCION=AE.ACCION AND AE.IDI=U.IDIOMA" & vbCrLf
    sConsulta = sConsulta & "   WHERE P2.BAJALOG=0 AND P2.UON1 IS NULL AND P2.UON2 IS NULL AND P.UON3 IS NULL AND U2.BAJA=0 AND R.TIPO=4 AND R.PER IS NULL AND R.DEP IS NOT NULL" & vbCrLf
    sConsulta = sConsulta & "       AND NA.TIPO_NOTIFICADO=5 AND RA.ACCION=@ACCION AND R.INSTANCIA=@ID" & vbCrLf
    sConsulta = sConsulta & "   UNION" & vbCrLf
    sConsulta = sConsulta & "   --Peticionario  Uon" & vbCrLf
    sConsulta = sConsulta & "   SELECT DISTINCT ISNULL(AE.SUBJECT,'') AS TEXT," & vbCrLf
    sConsulta = sConsulta & "       U.IDIOMA,U.TIPOEMAIL, P.EMAIL, U.DATEFMT, U.THOUSANFMT, U.DECIMALFMT, U.PRECISIONFMT, 0 AS ESPROVEEDOR,P.COD CODDESTINATARIO, NA.ID AS NOTIFICADO, U.COD" & vbCrLf
    sConsulta = sConsulta & "   FROM PER P WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PER P2 WITH(NOLOCK) ON P.COD=P2.COD" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN DEP D WITH(NOLOCK) ON P.DEP=D.COD" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL R WITH(NOLOCK) ON D.COD=R.DEP AND ISNULL(P2.UON1,'#')=ISNULL(R.UON1,'#') AND ISNULL(P2.UON2,'#')=ISNULL(R.UON2,'#') AND ISNULL(P2.UON3,'#')=ISNULL(R.UON3,'#')" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_BLOQUE B WITH(NOLOCK) ON R.INSTANCIA=B.INSTANCIA" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL_ACCION RA WITH(NOLOCK) ON R.ID=RA.ROL AND B.ID=RA.BLOQUE" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ACCIONES A WITH(NOLOCK) ON RA.ACCION=A.ID" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_NOTIFICADO_ACCION NA WITH(NOLOCK) ON A.ACCION_ORIGEN=NA.ACCION" & vbCrLf
    sConsulta = sConsulta & "   LEFT JOIN USU U WITH(NOLOCK) ON P.COD=U.PER AND U.BAJA=0" & vbCrLf
    sConsulta = sConsulta & "   LEFT OUTER JOIN PM_ACCIONES_EMAIL AE WITH(NOLOCK) ON NA.ACCION=AE.ACCION AND AE.IDI=U.IDIOMA" & vbCrLf
    sConsulta = sConsulta & "   WHERE P2.BAJALOG=0 AND R.TIPO=4 AND R.PER IS NULL AND (R.UON1 IS NOT NULL OR R.UON2 IS NOT NULL OR R.UON3 IS NOT NULL)" & vbCrLf
    sConsulta = sConsulta & "       AND NA.TIPO_NOTIFICADO=5 AND RA.ACCION=@ACCION AND R.INSTANCIA=@ID" & vbCrLf
    sConsulta = sConsulta & "   UNION" & vbCrLf
    sConsulta = sConsulta & "   --No Peticionario       Usuario" & vbCrLf
    sConsulta = sConsulta & "   SELECT DISTINCT ISNULL(AE.SUBJECT,'') AS TEXT," & vbCrLf
    sConsulta = sConsulta & "       U.IDIOMA,U.TIPOEMAIL, P.EMAIL, U.DATEFMT, U.THOUSANFMT, U.DECIMALFMT, U.PRECISIONFMT, 0 AS ESPROVEEDOR,P.COD CODDESTINATARIO, NA.ID AS NOTIFICADO, U.COD" & vbCrLf
    sConsulta = sConsulta & "   FROM PER P WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL R WITH(NOLOCK) ON P.COD=R.PER" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_BLOQUE B WITH(NOLOCK) ON R.INSTANCIA=B.INSTANCIA" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL_ACCION RA WITH(NOLOCK) ON R.ID=RA.ROL AND B.ID=RA.BLOQUE" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ACCIONES A WITH(NOLOCK) ON RA.ACCION=A.ID AND B.ID=A.BLOQUE" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_NOTIFICADO_ACCION NA WITH(NOLOCK) ON A.ACCION_ORIGEN=NA.ACCION" & vbCrLf
    sConsulta = sConsulta & "   LEFT JOIN USU U WITH(NOLOCK) ON P.COD=U.PER AND U.BAJA=0" & vbCrLf
    sConsulta = sConsulta & "   LEFT OUTER JOIN PM_ACCIONES_EMAIL AE WITH(NOLOCK) ON NA.ACCION=AE.ACCION AND AE.IDI=U.IDIOMA" & vbCrLf
    sConsulta = sConsulta & "   WHERE R.TIPO<>4 AND R.TIPO<>2 AND R.PER IS NOT NULL AND ISNULL(R.COMO_ASIGNAR,0)=0" & vbCrLf
    sConsulta = sConsulta & "       AND NA.TIPO_NOTIFICADO=5 AND RA.ACCION=@ACCION AND R.INSTANCIA=@ID" & vbCrLf
    sConsulta = sConsulta & "   UNION" & vbCrLf
    sConsulta = sConsulta & "   --No Peticionario       Lista" & vbCrLf
    sConsulta = sConsulta & "   SELECT DISTINCT ISNULL(AE.SUBJECT,'') AS TEXT," & vbCrLf
    sConsulta = sConsulta & "       U.IDIOMA,U.TIPOEMAIL, P.EMAIL, U.DATEFMT, U.THOUSANFMT, U.DECIMALFMT, U.PRECISIONFMT, 0 AS ESPROVEEDOR,P.COD CODDESTINATARIO, NA.ID AS NOTIFICADO, U.COD" & vbCrLf
    sConsulta = sConsulta & "   FROM PER P WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_PARTICIPANTES PA WITH(NOLOCK) ON P.COD=PA.PER" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL R WITH(NOLOCK) ON PA.ROL=R.ROL_ORIGEN AND PA.PER=R.PER" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_BLOQUE B WITH(NOLOCK) ON R.INSTANCIA=B.INSTANCIA" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL_ACCION RA WITH(NOLOCK) ON R.ID=RA.ROL AND B.ID=RA.BLOQUE" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ACCIONES A WITH(NOLOCK) ON RA.ACCION=A.ID AND B.ID=A.BLOQUE" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_NOTIFICADO_ACCION NA WITH(NOLOCK) ON A.ACCION_ORIGEN=NA.ACCION" & vbCrLf
    sConsulta = sConsulta & "   LEFT JOIN USU U WITH(NOLOCK) ON P.COD=U.PER AND U.BAJA=0" & vbCrLf
    sConsulta = sConsulta & "   LEFT OUTER JOIN PM_ACCIONES_EMAIL AE WITH(NOLOCK) ON NA.ACCION=AE.ACCION AND AE.IDI=U.IDIOMA" & vbCrLf
    sConsulta = sConsulta & "   WHERE R.TIPO<>4 AND R.TIPO<>2 AND NA.TIPO_NOTIFICADO=5 AND RA.ACCION=@ACCION AND R.INSTANCIA=@ID" & vbCrLf
    sConsulta = sConsulta & "   UNION" & vbCrLf
    sConsulta = sConsulta & "   --No Peticionario       DeCampoDePantalla" & vbCrLf
    sConsulta = sConsulta & "   SELECT DISTINCT ISNULL(AE.SUBJECT,'') AS TEXT," & vbCrLf
    sConsulta = sConsulta & "       U.IDIOMA,U.TIPOEMAIL, P.EMAIL, U.DATEFMT, U.THOUSANFMT, U.DECIMALFMT, U.PRECISIONFMT, 0 AS ESPROVEEDOR,P.COD CODDESTINATARIO, NA.ID AS NOTIFICADO, U.COD" & vbCrLf
    sConsulta = sConsulta & "   FROM PER P WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL R WITH(NOLOCK) ON P.COD=R.PER" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_BLOQUE B WITH(NOLOCK) ON R.INSTANCIA=B.INSTANCIA" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL_ACCION RA WITH(NOLOCK) ON R.ID=RA.ROL AND B.ID=RA.BLOQUE" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ACCIONES A WITH(NOLOCK) ON RA.ACCION=A.ID AND B.ID=A.BLOQUE" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_NOTIFICADO_ACCION NA WITH(NOLOCK) ON A.ACCION_ORIGEN=NA.ACCION" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN COPIA_CAMPO CC WITH(NOLOCK) ON R.CAMPO=CC.COPIA_CAMPO_DEF" & vbCrLf
    sConsulta = sConsulta & "       AND CC.NUM_VERSION=(SELECT MAX(NUM_VERSION) FROM COPIA_CAMPO WITH(NOLOCK) WHERE B.INSTANCIA=INSTANCIA)" & vbCrLf
    sConsulta = sConsulta & "   LEFT JOIN USU U WITH(NOLOCK) ON P.COD=U.PER AND U.BAJA=0" & vbCrLf
    sConsulta = sConsulta & "   LEFT OUTER JOIN PM_ACCIONES_EMAIL AE WITH(NOLOCK) ON NA.ACCION=AE.ACCION AND AE.IDI=U.IDIOMA" & vbCrLf
    sConsulta = sConsulta & "   WHERE R.TIPO<>4 AND R.TIPO<>2 AND R.COMO_ASIGNAR=1 AND NA.TIPO_NOTIFICADO=5 AND RA.ACCION=@ACCION AND R.INSTANCIA=@ID" & vbCrLf
    sConsulta = sConsulta & "   UNION" & vbCrLf
    sConsulta = sConsulta & "   --Peticionario / No Peticionario        Destinatario" & vbCrLf
    sConsulta = sConsulta & "   SELECT DISTINCT ISNULL(AE.SUBJECT,'') AS TEXT," & vbCrLf
    sConsulta = sConsulta & "       U.IDIOMA,U.TIPOEMAIL, P.EMAIL, U.DATEFMT, U.THOUSANFMT, U.DECIMALFMT, U.PRECISIONFMT, 0 AS ESPROVEEDOR,P.COD CODDESTINATARIO, NA.ID AS NOTIFICADO, U.COD" & vbCrLf
    sConsulta = sConsulta & "   FROM PER P WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN INSTANCIA_EST IE2 WITH(NOLOCK) ON P.COD=IE2.DESTINATARIO" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN INSTANCIA_CAMINO IC WITH(NOLOCK) ON IE2.BLOQUE=IC.INSTANCIA_BLOQUE_ORIGEN" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN INSTANCIA_EST IE WITH(NOLOCK) ON IC.INSTANCIA_EST=IE.ID" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ACCIONES A WITH(NOLOCK) ON IE.PM_ACCION = A.ID" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_NOTIFICADO_ACCION NA WITH(NOLOCK) ON A.ACCION_ORIGEN=NA.ACCION" & vbCrLf
    sConsulta = sConsulta & "   LEFT JOIN USU U WITH(NOLOCK) ON P.COD=U.PER AND U.BAJA=0" & vbCrLf
    sConsulta = sConsulta & "   LEFT OUTER JOIN PM_ACCIONES_EMAIL AE WITH(NOLOCK) ON NA.ACCION=AE.ACCION AND AE.IDI=U.IDIOMA" & vbCrLf
    sConsulta = sConsulta & "   WHERE IE2.ACCION=3 AND NA.TIPO_NOTIFICADO=5 AND IE.PM_ACCION=@ACCION AND IE.INSTANCIA=@ID" & vbCrLf
    sConsulta = sConsulta & "--Notificados: Proveedores que participaron en la etapa Origen" & vbCrLf
    sConsulta = sConsulta & "   UNION" & vbCrLf
    sConsulta = sConsulta & "   --No Peticionario       Prove" & vbCrLf
    sConsulta = sConsulta & "   SELECT DISTINCT ISNULL(AE.SUBJECT,'') AS TEXT," & vbCrLf
    sConsulta = sConsulta & "       ISNULL(C.IDI,(SELECT IDIOMA FROM PARGEN_DEF WITH(NOLOCK) WHERE PARGEN_DEF.ID=1)) IDIOMA," & vbCrLf
    sConsulta = sConsulta & "       C.TIPOEMAIL, C.EMAIL, C.DATEFMT, C.THOUSANFMT, C.DECIMALFMT, C.PRECISIONFMT, 1 AS ESPROVEEDOR,NULL CODDESTINATARIO, NA.ID AS NOTIFICADO, NULL CODUSUARIO" & vbCrLf
    sConsulta = sConsulta & "   FROM CON C WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL R WITH(NOLOCK) ON C.PROVE=R.PROVE" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL_CON RC WITH(NOLOCK) ON R.ID=RC.ID_ROL AND C.ID=RC.ID_CON" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ACCIONES CA WITH(NOLOCK) ON CA.ID=@ACCION" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_NOTIFICADO_ACCION NA WITH(NOLOCK) ON CA.ACCION_ORIGEN=NA.ACCION" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_BLOQUE CB WITH(NOLOCK) ON CA.BLOQUE=CB.ID AND R.INSTANCIA=CB.INSTANCIA" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL_ACCION RA WITH(NOLOCK) ON R.ID=RA.ROL AND CB.ID=RA.BLOQUE AND CA.ID=RA.ACCION" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_ACCIONES_EMAIL AE WITH(NOLOCK) ON NA.ACCION=AE.ACCION AND AE.IDI=ISNULL(C.IDI,(SELECT IDIOMA FROM PARGEN_DEF WITH(NOLOCK) WHERE PARGEN_DEF.ID=1))" & vbCrLf
    sConsulta = sConsulta & "   WHERE NA.TIPO_NOTIFICADO=5 AND R.TIPO=2 AND R.INSTANCIA=@ID" & vbCrLf
    sConsulta = sConsulta & "    UNION" & vbCrLf
    sConsulta = sConsulta & "    SELECT DISTINCT ISNULL(AE.SUBJECT,'') AS TEXT," & vbCrLf
    sConsulta = sConsulta & "       ISNULL(C.IDI,(SELECT IDIOMA FROM PARGEN_DEF WITH(NOLOCK) WHERE PARGEN_DEF.ID=1)) IDIOMA," & vbCrLf
    sConsulta = sConsulta & "       C.TIPOEMAIL, C.EMAIL, C.DATEFMT, C.THOUSANFMT, C.DECIMALFMT, C.PRECISIONFMT, 1 AS ESPROVEEDOR,NULL CODDESTINATARIO, NA.ID AS NOTIFICADO, NULL CODUSUARIO" & vbCrLf
    sConsulta = sConsulta & "   FROM CON C WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL R WITH(NOLOCK) ON C.PROVE=R.PROVE" & vbCrLf
    sConsulta = sConsulta & "   AND NOT EXISTS (SELECT ID_CON FROM PM_COPIA_ROL_CON RC WITH(NOLOCK) WHERE RC.ID_ROL = R.ID)" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ACCIONES CA WITH(NOLOCK) ON CA.ID=@ACCION" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_NOTIFICADO_ACCION NA WITH(NOLOCK) ON CA.ACCION_ORIGEN=NA.ACCION" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_BLOQUE CB WITH(NOLOCK) ON CA.BLOQUE=CB.ID AND R.INSTANCIA=CB.INSTANCIA" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL_ACCION RA WITH(NOLOCK) ON R.ID=RA.ROL AND CB.ID=RA.BLOQUE AND CA.ID=RA.ACCION" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_ACCIONES_EMAIL AE WITH(NOLOCK) ON NA.ACCION=AE.ACCION AND AE.IDI=ISNULL(C.IDI,(SELECT IDIOMA FROM PARGEN_DEF WITH(NOLOCK) WHERE PARGEN_DEF.ID=1))" & vbCrLf
    sConsulta = sConsulta & "   WHERE NA.TIPO_NOTIFICADO=5 AND R.TIPO=2 AND R.INSTANCIA=@ID" & vbCrLf
    sConsulta = sConsulta & "    UNION" & vbCrLf
    sConsulta = sConsulta & "   --No Peticionario       Destinatario Prove" & vbCrLf
    sConsulta = sConsulta & "   SELECT DISTINCT ISNULL(AE.SUBJECT,'') AS TEXT," & vbCrLf
    sConsulta = sConsulta & "       ISNULL(C.IDI,(SELECT IDIOMA FROM PARGEN_DEF WITH(NOLOCK) WHERE PARGEN_DEF.ID=1)) IDIOMA," & vbCrLf
    sConsulta = sConsulta & "       C.TIPOEMAIL, C.EMAIL, C.DATEFMT, C.THOUSANFMT, C.DECIMALFMT, C.PRECISIONFMT, 1 AS ESPROVEEDOR,NULL CODDESTINATARIO, NA.ID AS NOTIFICADO, NULL CODUSUARIO" & vbCrLf
    sConsulta = sConsulta & "   FROM CON C WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN INSTANCIA_EST IE2 WITH(NOLOCK) ON IE2.CONTACTO=C.ID AND IE2.DESTINATARIO_PROV=C.PROVE" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN INSTANCIA_CAMINO IC WITH(NOLOCK) ON IE2.BLOQUE=IC.INSTANCIA_BLOQUE_ORIGEN" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN INSTANCIA_EST IE WITH(NOLOCK) ON IC.INSTANCIA_EST=IE.ID" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ACCIONES A WITH(NOLOCK) ON IE.PM_ACCION=A.ID" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_NOTIFICADO_ACCION NA WITH(NOLOCK) ON A.ACCION_ORIGEN=NA.ACCION" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_ACCIONES_EMAIL AE WITH(NOLOCK) ON NA.ACCION=AE.ACCION AND AE.IDI=ISNULL(C.IDI,(SELECT IDIOMA FROM PARGEN_DEF WITH(NOLOCK) WHERE PARGEN_DEF.ID=1))" & vbCrLf
    sConsulta = sConsulta & "   WHERE NA.TIPO_NOTIFICADO=5 AND IE2.ACCION=3 AND NA.ACCION = @ACCION AND IE2.INSTANCIA=@ID" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "--Notificados: Responsables de Procesos de Compra asociados a la solicitud" & vbCrLf
    sConsulta = sConsulta & "   UNION" & vbCrLf
    sConsulta = sConsulta & "   SELECT DISTINCT ISNULL(AE.SUBJECT,'') AS TEXT," & vbCrLf
    sConsulta = sConsulta & "       U.IDIOMA,U.TIPOEMAIL, P.EMAIL, U.DATEFMT, U.THOUSANFMT, U.DECIMALFMT, U.PRECISIONFMT, 0 AS ESPROVEEDOR,P.COD CODDESTINATARIO, NA.ID AS NOTIFICADO, U.COD" & vbCrLf
    sConsulta = sConsulta & "   FROM PER P WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   LEFT JOIN USU U WITH(NOLOCK) ON P.COD=U.PER AND U.BAJA=0" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PROCE PR WITH(NOLOCK) ON P.COD=PR.COM" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN ITEM S WITH(NOLOCK) ON PR.ANYO =S.ANYO AND PR.GMN1 =S.GMN1_PROCE  AND PR.COD =S.PROCE " & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN INSTANCIA I WITH(NOLOCK) ON I.ID=S.SOLICIT" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN INSTANCIA_EST IE WITH(NOLOCK) ON I.ID=IE.INSTANCIA" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ACCIONES A WITH(NOLOCK) ON IE.PM_ACCION=A.ID" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_NOTIFICADO_ACCION NA WITH(NOLOCK) ON A.ACCION_ORIGEN=NA.ACCION" & vbCrLf
    sConsulta = sConsulta & "   LEFT OUTER JOIN PM_ACCIONES_EMAIL AE WITH(NOLOCK) ON NA.ACCION=AE.ACCION AND AE.IDI=U.IDIOMA" & vbCrLf
    sConsulta = sConsulta & "   WHERE NA.TIPO_NOTIFICADO=6 AND IE.PM_ACCION=@ACCION AND I.ID=@ID" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "--Notificado: Gestor de la Solicitud" & vbCrLf
    sConsulta = sConsulta & "   UNION" & vbCrLf
    sConsulta = sConsulta & "   SELECT DISTINCT ISNULL(AE.SUBJECT,'') AS TEXT," & vbCrLf
    sConsulta = sConsulta & "       U.IDIOMA,U.TIPOEMAIL, P.EMAIL, U.DATEFMT, U.THOUSANFMT, U.DECIMALFMT, U.PRECISIONFMT, 0 AS ESPROVEEDOR,P.COD CODDESTINATARIO, NA.ID AS NOTIFICADO, U.COD" & vbCrLf
    sConsulta = sConsulta & "   FROM PER P WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN SOLICITUD S WITH(NOLOCK) ON P.COD=S.GESTOR" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN INSTANCIA I WITH(NOLOCK) ON S.ID=I.SOLICITUD" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN INSTANCIA_EST IE WITH(NOLOCK) ON I.ID=IE.INSTANCIA" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ACCIONES A WITH(NOLOCK) ON IE.PM_ACCION=A.ID" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_NOTIFICADO_ACCION NA WITH(NOLOCK) ON A.ACCION_ORIGEN=NA.ACCION" & vbCrLf
    sConsulta = sConsulta & "   LEFT JOIN USU U WITH(NOLOCK) ON P.COD=U.PER AND U.BAJA=0" & vbCrLf
    sConsulta = sConsulta & "   LEFT OUTER JOIN PM_ACCIONES_EMAIL AE WITH(NOLOCK) ON NA.ACCION=AE.ACCION AND AE.IDI=U.IDIOMA" & vbCrLf
    sConsulta = sConsulta & "   WHERE NA.TIPO_NOTIFICADO=7 AND IE.PM_ACCION=@ACCION AND I.ID=@ID" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   SELECT TEXT, IDIOMA, TIPOEMAIL, EMAIL, DATEFMT, THOUSANFMT, DECIMALFMT, PRECISIONFMT, ESPROVEEDOR, CODDESTINATARIO, MIN(NOTIFICADO) AS NOTIFICADO, CODUSUARIO,IDIOMAS.LANGUAGETAG " & vbCrLf
    sConsulta = sConsulta & "   FROM #FSPM_DATOS_MAIL_ACCION FS" & vbCrLf
    sConsulta = sConsulta & "   LEFT JOIN IDIOMAS WITH (NOLOCK) ON FS.IDIOMA =IDIOMAS.COD " & vbCrLf
    sConsulta = sConsulta & "   GROUP BY TEXT, IDIOMA, TIPOEMAIL, EMAIL, DATEFMT, THOUSANFMT, DECIMALFMT, PRECISIONFMT, ESPROVEEDOR, CODDESTINATARIO, CODUSUARIO,IDIOMAS.LANGUAGETAG " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   DROP TABLE #FSPM_DATOS_MAIL_ACCION" & vbCrLf
    sConsulta = sConsulta & "  END" & vbCrLf
    sConsulta = sConsulta & "ELSE" & vbCrLf
    sConsulta = sConsulta & "  IF @TYPE = 3 --NOTIFICADOS DE ENLACE" & vbCrLf
    sConsulta = sConsulta & "  BEGIN" & vbCrLf
    sConsulta = sConsulta & "   --Esta stored no saca nada si no hay enlace. El rechazo definitivo pone enlace de instancia_camino a NULL" & vbCrLf
    sConsulta = sConsulta & "   IF @ENLACE=0" & vbCrLf
    sConsulta = sConsulta & "       SELECT @ENLACE=CE.ID FROM PM_COPIA_ENLACE CE WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "       INNER JOIN PM_COPIA_ACCIONES CA WITH(NOLOCK) ON CE.ACCION=CA.ID AND CE.BLOQUE_ORIGEN=CA.BLOQUE AND CA.TIPO_RECHAZO=2" & vbCrLf
    sConsulta = sConsulta & "       WHERE CA.ID=@ACCION" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   CREATE TABLE #FSPM_DATOS_MAIL_ENLACE (" & vbCrLf
    sConsulta = sConsulta & "       TEXT varchar(200)," & vbCrLf
    sConsulta = sConsulta & "       IDIOMA varchar(3)," & vbCrLf
    sConsulta = sConsulta & "       TIPOEMAIL tinyint," & vbCrLf
    sConsulta = sConsulta & "       EMAIL varchar(100)," & vbCrLf
    sConsulta = sConsulta & "       DATEFMT varchar(50)," & vbCrLf
    sConsulta = sConsulta & "       THOUSANFMT char(1)," & vbCrLf
    sConsulta = sConsulta & "       DECIMALFMT char(1)," & vbCrLf
    sConsulta = sConsulta & "       PRECISIONFMT tinyint," & vbCrLf
    sConsulta = sConsulta & "       ESPROVEEDOR int," & vbCrLf
    sConsulta = sConsulta & "       CODDESTINATARIO varchar(20)," & vbCrLf
    sConsulta = sConsulta & "       NOTIFICADO int," & vbCrLf
    sConsulta = sConsulta & "       CODUSUARIO varchar(50)" & vbCrLf
    sConsulta = sConsulta & "   )" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   INSERT INTO #FSPM_DATOS_MAIL_ENLACE (TEXT, IDIOMA, TIPOEMAIL, EMAIL, DATEFMT, THOUSANFMT, DECIMALFMT, PRECISIONFMT, ESPROVEEDOR, CODDESTINATARIO, NOTIFICADO, CODUSUARIO)" & vbCrLf
    sConsulta = sConsulta & "   --Notificados: Especificos" & vbCrLf
    sConsulta = sConsulta & "   SELECT DISTINCT ISNULL(EE.SUBJECT,'') AS TEXT," & vbCrLf
    sConsulta = sConsulta & "       U.IDIOMA," & vbCrLf
    sConsulta = sConsulta & "       U.TIPOEMAIL, P.EMAIL, U.DATEFMT, U.THOUSANFMT, U.DECIMALFMT, U.PRECISIONFMT, 0 AS ESPROVEEDOR,P.COD CODDESTINATARIO, NE.ID AS NOTIFICADO, U.COD CODUSUARIO" & vbCrLf
    sConsulta = sConsulta & "   FROM PER P WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_NOTIFICADO_ENLACE NE WITH(NOLOCK) ON P.COD=NE.PER" & vbCrLf
    sConsulta = sConsulta & "   LEFT JOIN USU U WITH(NOLOCK) ON P.COD=U.PER AND U.BAJA=0" & vbCrLf
    sConsulta = sConsulta & "   LEFT OUTER JOIN PM_ENLACE_EMAIL EE WITH(NOLOCK) ON NE.ENLACE=EE.ENLACE AND EE.IDI=U.IDIOMA" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ENLACE CE WITH(NOLOCK) ON CE.ENLACE_ORIGEN=EE.ENLACE" & vbCrLf
    sConsulta = sConsulta & "   WHERE NE.TIPO_NOTIFICADO=1 AND CE.ID=@ENLACE" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   --Notificados: Roles(Proveedor)" & vbCrLf
    sConsulta = sConsulta & "   UNION" & vbCrLf
    sConsulta = sConsulta & "   SELECT DISTINCT ISNULL(EE.SUBJECT,'') AS TEXT," & vbCrLf
    sConsulta = sConsulta & "       ISNULL(C.IDI,(SELECT IDIOMA FROM PARGEN_DEF WITH(NOLOCK) WHERE PARGEN_DEF.ID=1)) IDIOMA," & vbCrLf
    sConsulta = sConsulta & "       C.TIPOEMAIL, C.EMAIL, C.DATEFMT, C.THOUSANFMT, C.DECIMALFMT, C.PRECISIONFMT, 1 AS ESPROVEEDOR,NULL CODDESTINATARIO, NE.ID AS NOTIFICADO, NULL CODUSUARIO" & vbCrLf
    sConsulta = sConsulta & "   FROM CON C WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL R WITH(NOLOCK) ON C.PROVE=R.PROVE" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL_CON RC WITH(NOLOCK) ON R.ID=RC.ID_ROL AND C.ID=RC.ID_CON" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_ROL ROL WITH(NOLOCK) ON R.ROL_ORIGEN=ROL.ID" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_NOTIFICADO_ENLACE NE WITH(NOLOCK) ON ROL.ID=NE.ROL" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_ENLACE_EMAIL EE WITH(NOLOCK) ON EE.IDI=ISNULL(C.IDI,(SELECT IDIOMA FROM PARGEN_DEF WITH(NOLOCK) WHERE PARGEN_DEF.ID=1)) AND EE.ENLACE=NE.ENLACE" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ENLACE CE WITH(NOLOCK) ON CE.ENLACE_ORIGEN=EE.ENLACE" & vbCrLf
    sConsulta = sConsulta & "   WHERE R.TIPO=2 AND NE.TIPO_NOTIFICADO=2 AND CE.ID=@ENLACE AND R.INSTANCIA=@ID" & vbCrLf
    sConsulta = sConsulta & "   UNION" & vbCrLf
    sConsulta = sConsulta & "       SELECT DISTINCT ISNULL(EE.SUBJECT,'') AS TEXT," & vbCrLf
    sConsulta = sConsulta & "       ISNULL(C.IDI,(SELECT IDIOMA FROM PARGEN_DEF WITH(NOLOCK) WHERE PARGEN_DEF.ID=1)) IDIOMA," & vbCrLf
    sConsulta = sConsulta & "       C.TIPOEMAIL, C.EMAIL, C.DATEFMT, C.THOUSANFMT, C.DECIMALFMT, C.PRECISIONFMT, 1 AS ESPROVEEDOR,NULL CODDESTINATARIO, NE.ID AS NOTIFICADO, NULL CODUSUARIO" & vbCrLf
    sConsulta = sConsulta & "   FROM CON C WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL R WITH(NOLOCK) ON C.PROVE=R.PROVE" & vbCrLf
    sConsulta = sConsulta & "   AND NOT EXISTS (SELECT ID_CON FROM PM_COPIA_ROL_CON RC WITH(NOLOCK) WHERE RC.ID_ROL = R.ID)" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_ROL ROL WITH(NOLOCK) ON R.ROL_ORIGEN=ROL.ID" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_NOTIFICADO_ENLACE NE WITH(NOLOCK) ON ROL.ID=NE.ROL" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_ENLACE_EMAIL EE WITH(NOLOCK) ON EE.IDI=ISNULL(C.IDI,(SELECT IDIOMA FROM PARGEN_DEF WITH(NOLOCK) WHERE PARGEN_DEF.ID=1)) AND EE.ENLACE=NE.ENLACE" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ENLACE CE WITH(NOLOCK) ON CE.ENLACE_ORIGEN=EE.ENLACE" & vbCrLf
    sConsulta = sConsulta & "   WHERE R.TIPO=2 AND NE.TIPO_NOTIFICADO=2 AND CE.ID=@ENLACE AND R.INSTANCIA=@ID" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   --Notificados: Roles(Comprador, Usuario, Peticionario)" & vbCrLf
    sConsulta = sConsulta & "   UNION" & vbCrLf
    sConsulta = sConsulta & "   SELECT DISTINCT ISNULL(EE.SUBJECT,'') AS TEXT," & vbCrLf
    sConsulta = sConsulta & "       U.IDIOMA,U.TIPOEMAIL, P.EMAIL, U.DATEFMT, U.THOUSANFMT, U.DECIMALFMT, U.PRECISIONFMT, 0 AS ESPROVEEDOR,P.COD CODDESTINATARIO, NE.ID AS NOTIFICADO, U.COD CODUSUARIO" & vbCrLf
    sConsulta = sConsulta & "   FROM PER P WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   LEFT JOIN USU U WITH(NOLOCK) ON P.COD=U.PER AND U.BAJA=0" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL R WITH(NOLOCK) ON P.COD=R.PER" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_ROL ROL WITH(NOLOCK) ON R.ROL_ORIGEN=ROL.ID" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_NOTIFICADO_ENLACE NE WITH(NOLOCK) ON ROL.ID=NE.ROL" & vbCrLf
    sConsulta = sConsulta & "   LEFT OUTER JOIN PM_ENLACE_EMAIL EE WITH(NOLOCK) ON NE.ENLACE=EE.ENLACE AND EE.IDI=U.IDIOMA" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ENLACE CE WITH(NOLOCK) ON EE.ENLACE=CE.ENLACE_ORIGEN" & vbCrLf
    sConsulta = sConsulta & "   WHERE R.TIPO <> 2 AND NE.TIPO_NOTIFICADO=2 AND CE.ID=@ENLACE AND R.INSTANCIA=@ID" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   --Notificados: Roles(Lista de AutoAsignaci�n)" & vbCrLf
    sConsulta = sConsulta & "   UNION" & vbCrLf
    sConsulta = sConsulta & "   SELECT DISTINCT ISNULL(EE.SUBJECT,'') AS TEXT," & vbCrLf
    sConsulta = sConsulta & "       U.IDIOMA,U.TIPOEMAIL, P.EMAIL, U.DATEFMT, U.THOUSANFMT, U.DECIMALFMT, U.PRECISIONFMT, 0 AS ESPROVEEDOR,P.COD CODDESTINATARIO, NE.ID AS NOTIFICADO, U.COD CODUSUARIO" & vbCrLf
    sConsulta = sConsulta & "   FROM PM_COPIA_ROL R WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_ROL ROL WITH(NOLOCK) ON R.ROL_ORIGEN=ROL.ID" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_NOTIFICADO_ENLACE NE WITH(NOLOCK) ON ROL.ID=NE.ROL" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_PARTICIPANTES PCP WITH(NOLOCK) ON R.ID=PCP.ROL" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PER P WITH(NOLOCK) ON PCP.PER=P.COD" & vbCrLf
    sConsulta = sConsulta & "   LEFT JOIN USU U WITH(NOLOCK) ON P.COD=U.PER AND U.BAJA=0" & vbCrLf
    sConsulta = sConsulta & "   LEFT OUTER JOIN PM_ENLACE_EMAIL EE WITH(NOLOCK) ON NE.ENLACE=EE.ENLACE AND EE.IDI=U.IDIOMA" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ENLACE CE WITH(NOLOCK) ON EE.ENLACE=CE.ENLACE_ORIGEN" & vbCrLf
    sConsulta = sConsulta & "   WHERE R.TIPO = 3 AND NE.TIPO_NOTIFICADO=2 AND R.COMO_ASIGNAR=3 AND CE.ID=@ENLACE AND R.INSTANCIA=@ID" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   --Notificados: Roles(GESTOR DE LA FACTURA)" & vbCrLf
    sConsulta = sConsulta & "   UNION" & vbCrLf
    sConsulta = sConsulta & "   SELECT DISTINCT ISNULL(EE.SUBJECT,'') AS TEXT," & vbCrLf
    sConsulta = sConsulta & "   U.IDIOMA,U.TIPOEMAIL, P.EMAIL, U.DATEFMT, U.THOUSANFMT, U.DECIMALFMT, U.PRECISIONFMT, 0 AS ESPROVEEDOR,P.COD CODDESTINATARIO, NE.ID AS NOTIFICADO, U.COD" & vbCrLf
    sConsulta = sConsulta & "   FROM PER P WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   LEFT JOIN USU U WITH(NOLOCK) ON P.COD=U.PER AND U.BAJA = 0" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PRES5_IMPORTES PI WITH(NOLOCK) ON PI.GESTOR = P.COD" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN LINEAS_PED_IMPUTACION LPI WITH(NOLOCK) ON LPI.PRES5_IMP = PI.ID" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN LINEAS_PEDIDO_FACTURA LPF WITH(NOLOCK) ON LPF.LINEA_PEDIDO = LPI.LINEA" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN LIN_FACTURA lf with(nolock) on lf.FACTURA = lpf.FACTURA" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN FACTURA F WITH(NOLOCK) ON F.ID = lPF.FACTURA" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL r with(nolock) on r.INSTANCIA = f.INSTANCIA" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_ROL ROL WITH(NOLOCK) ON R.ROL_ORIGEN=ROL.ID" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_NOTIFICADO_ENLACE NE WITH(NOLOCK) ON ROL.ID=NE.ROL" & vbCrLf
    sConsulta = sConsulta & "   LEFT OUTER JOIN PM_ENLACE_EMAIL EE WITH(NOLOCK) ON NE.ENLACE=EE.ENLACE AND EE.IDI=U.IDIOMA" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ENLACE CE WITH(NOLOCK) ON NE.ENLACE=CE.ENLACE_ORIGEN" & vbCrLf
    sConsulta = sConsulta & "   WHERE F.INSTANCIA = @ID and CE.ID=@ENLACE and R.TIPO = 6 and R.GESTOR_FACTURA = 1 AND NE.TIPO_NOTIFICADO=2" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   --Notificado: Peticionario" & vbCrLf
    sConsulta = sConsulta & "   UNION" & vbCrLf
    sConsulta = sConsulta & "   SELECT DISTINCT ISNULL(EE.SUBJECT,'') AS TEXT," & vbCrLf
    sConsulta = sConsulta & "       U.IDIOMA,U.TIPOEMAIL, P.EMAIL, U.DATEFMT, U.THOUSANFMT, U.DECIMALFMT, U.PRECISIONFMT, 0 AS ESPROVEEDOR,P.COD CODDESTINATARIO, NE.ID AS NOTIFICADO, U.COD CODUSUARIO" & vbCrLf
    sConsulta = sConsulta & "   FROM PER P WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   LEFT JOIN USU U WITH(NOLOCK) ON P.COD=U.PER AND U.BAJA=0" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN INSTANCIA I WITH(NOLOCK) ON P.COD=I.PETICIONARIO" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_BLOQUE B WITH(NOLOCK) ON I.ID=B.INSTANCIA" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ENLACE E WITH(NOLOCK) ON B.ID=E.BLOQUE_ORIGEN" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_NOTIFICADO_ENLACE NE WITH(NOLOCK) ON E.ENLACE_ORIGEN=NE.ENLACE" & vbCrLf
    sConsulta = sConsulta & "   LEFT OUTER JOIN PM_ENLACE_EMAIL EE WITH(NOLOCK) ON NE.ENLACE=EE.ENLACE AND EE.IDI=U.IDIOMA" & vbCrLf
    sConsulta = sConsulta & "   WHERE NE.TIPO_NOTIFICADO=3 AND E.ID=@ENLACE AND I.ID=@ID" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   --Notificado: Autor de la accion (Rol Proveedor)" & vbCrLf
    sConsulta = sConsulta & "   UNION" & vbCrLf
    sConsulta = sConsulta & "   SELECT DISTINCT ISNULL(EE.SUBJECT,'') AS TEXT," & vbCrLf
    sConsulta = sConsulta & "       ISNULL(C.IDI,(SELECT IDIOMA FROM PARGEN_DEF WITH(NOLOCK) WHERE PARGEN_DEF.ID=1)) IDIOMA," & vbCrLf
    sConsulta = sConsulta & "       C.TIPOEMAIL, C.EMAIL, C.DATEFMT, C.THOUSANFMT, C.DECIMALFMT, C.PRECISIONFMT, 1 AS ESPROVEEDOR,NULL CODDESTINATARIO, NE.ID AS NOTIFICADO, NULL CODUSUARIO" & vbCrLf
    sConsulta = sConsulta & "   FROM CON C WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL R WITH(NOLOCK) ON C.PROVE=R.PROVE" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL_CON RC WITH(NOLOCK) ON R.ID=RC.ID_ROL AND C.ID=RC.ID_CON" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_ROL ROL WITH(NOLOCK) ON R.ROL_ORIGEN=ROL.ID" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN INSTANCIA_EST IE WITH(NOLOCK) ON R.ID=IE.ROL" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ENLACE E WITH(NOLOCK) ON IE.PM_ACCION=E.ACCION" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_NOTIFICADO_ENLACE NE WITH(NOLOCK) ON E.ENLACE_ORIGEN=NE.ENLACE" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_ENLACE_EMAIL EE WITH(NOLOCK) ON NE.ENLACE=EE.ENLACE AND EE.IDI=ISNULL(C.IDI,(SELECT IDIOMA FROM PARGEN_DEF WITH(NOLOCK) WHERE PARGEN_DEF.ID=1))" & vbCrLf
    sConsulta = sConsulta & "   WHERE R.TIPO =2 AND NE.TIPO_NOTIFICADO=4 AND IE.PM_ACCION=@ACCION AND E.ID=@ENLACE AND R.INSTANCIA=@ID" & vbCrLf
    sConsulta = sConsulta & "   UNION" & vbCrLf
    sConsulta = sConsulta & "   SELECT DISTINCT ISNULL(EE.SUBJECT,'') AS TEXT," & vbCrLf
    sConsulta = sConsulta & "       ISNULL(C.IDI,(SELECT IDIOMA FROM PARGEN_DEF WITH(NOLOCK) WHERE PARGEN_DEF.ID=1)) IDIOMA," & vbCrLf
    sConsulta = sConsulta & "       C.TIPOEMAIL, C.EMAIL, C.DATEFMT, C.THOUSANFMT, C.DECIMALFMT, C.PRECISIONFMT, 1 AS ESPROVEEDOR,NULL CODDESTINATARIO, NE.ID AS NOTIFICADO, NULL CODUSUARIO" & vbCrLf
    sConsulta = sConsulta & "   FROM CON C WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL R WITH(NOLOCK) ON C.PROVE=R.PROVE" & vbCrLf
    sConsulta = sConsulta & "   AND NOT EXISTS (SELECT ID_CON FROM PM_COPIA_ROL_CON RC WITH(NOLOCK) WHERE RC.ID_ROL = R.ID)" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_ROL ROL WITH(NOLOCK) ON R.ROL_ORIGEN=ROL.ID" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN INSTANCIA_EST IE WITH(NOLOCK) ON R.ID=IE.ROL" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ENLACE E WITH(NOLOCK) ON IE.PM_ACCION=E.ACCION" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_NOTIFICADO_ENLACE NE WITH(NOLOCK) ON E.ENLACE_ORIGEN=NE.ENLACE" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_ENLACE_EMAIL EE WITH(NOLOCK) ON NE.ENLACE=EE.ENLACE AND EE.IDI=ISNULL(C.IDI,(SELECT IDIOMA FROM PARGEN_DEF WITH(NOLOCK) WHERE PARGEN_DEF.ID=1))" & vbCrLf
    sConsulta = sConsulta & "   WHERE R.TIPO =2 AND NE.TIPO_NOTIFICADO=4 AND IE.PM_ACCION=@ACCION AND E.ID=@ENLACE AND R.INSTANCIA=@ID" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   --Notificado: Autor de la accion (Rol Comprador, Usuario, Peticionario)" & vbCrLf
    sConsulta = sConsulta & "   UNION" & vbCrLf
    sConsulta = sConsulta & "   SELECT DISTINCT ISNULL(EE.SUBJECT,'') AS TEXT," & vbCrLf
    sConsulta = sConsulta & "       U.IDIOMA,U.TIPOEMAIL, P.EMAIL, U.DATEFMT, U.THOUSANFMT, U.DECIMALFMT, U.PRECISIONFMT, 0 AS ESPROVEEDOR,P.COD CODDESTINATARIO, NE.ID AS NOTIFICADO, U.COD CODUSUARIO" & vbCrLf
    sConsulta = sConsulta & "     FROM PER P WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "     INNER JOIN PM_COPIA_ROL R WITH(NOLOCK) ON P.COD=R.PER" & vbCrLf
    sConsulta = sConsulta & "     INNER JOIN INSTANCIA_EST IE WITH(NOLOCK) ON R.ID=IE.ROL" & vbCrLf
    sConsulta = sConsulta & "     INNER JOIN PM_COPIA_ENLACE E WITH(NOLOCK) ON IE.PM_ACCION=E.ACCION" & vbCrLf
    sConsulta = sConsulta & "     INNER JOIN PM_NOTIFICADO_ENLACE NE WITH(NOLOCK) ON E.ENLACE_ORIGEN=NE.ENLACE" & vbCrLf
    sConsulta = sConsulta & "     LEFT JOIN USU U WITH(NOLOCK) ON P.COD=U.PER AND U.BAJA=0" & vbCrLf
    sConsulta = sConsulta & "     LEFT OUTER JOIN PM_ENLACE_EMAIL EE WITH(NOLOCK) ON NE.ENLACE=EE.ENLACE AND EE.IDI=U.IDIOMA" & vbCrLf
    sConsulta = sConsulta & "   WHERE R.TIPO<>2 AND NE.TIPO_NOTIFICADO=4 AND IE.PM_ACCION=@ACCION AND E.ID=@ENLACE AND R.INSTANCIA=@ID" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   --Notificados: Personas Peticionarias que participaron en la etapa Origen" & vbCrLf
    sConsulta = sConsulta & "   UNION" & vbCrLf
    sConsulta = sConsulta & "   --Peticionario  Usuario" & vbCrLf
    sConsulta = sConsulta & "   SELECT DISTINCT ISNULL(EE.SUBJECT,'') AS TEXT," & vbCrLf
    sConsulta = sConsulta & "       U.IDIOMA," & vbCrLf
    sConsulta = sConsulta & "       U.TIPOEMAIL, P.EMAIL, U.DATEFMT, U.THOUSANFMT, U.DECIMALFMT, U.PRECISIONFMT, 0 AS ESPROVEEDOR,P.COD CODDESTINATARIO, NE.ID AS NOTIFICADO, U.COD CODUSUARIO" & vbCrLf
    sConsulta = sConsulta & "    FROM PER P WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "    INNER JOIN PM_COPIA_ROL R WITH(NOLOCK) ON P.COD=R.PER" & vbCrLf
    sConsulta = sConsulta & "    INNER JOIN PM_COPIA_ROL_ACCION RA WITH(NOLOCK) ON R.ID=RA.ROL" & vbCrLf
    sConsulta = sConsulta & "    INNER JOIN PM_COPIA_ENLACE E WITH(NOLOCK) ON RA.ACCION=E.ACCION" & vbCrLf
    sConsulta = sConsulta & "    INNER JOIN PM_NOTIFICADO_ENLACE NE WITH(NOLOCK) ON E.ENLACE_ORIGEN=NE.ENLACE" & vbCrLf
    sConsulta = sConsulta & "    INNER JOIN PM_COPIA_BLOQUE B WITH(NOLOCK) ON E.BLOQUE_ORIGEN=B.ID AND R.INSTANCIA=B.INSTANCIA" & vbCrLf
    sConsulta = sConsulta & "    LEFT JOIN USU U WITH(NOLOCK) ON P.COD=U.PER AND U.BAJA=0" & vbCrLf
    sConsulta = sConsulta & "    LEFT OUTER JOIN PM_ENLACE_EMAIL EE WITH(NOLOCK) ON NE.ENLACE=EE.ENLACE AND EE.IDI=U.IDIOMA" & vbCrLf
    sConsulta = sConsulta & "   WHERE R.TIPO=4 AND R.PER IS NOT NULL AND NE.TIPO_NOTIFICADO=5 AND RA.ACCION=@ACCION AND E.ID=@ENLACE AND R.INSTANCIA=@ID" & vbCrLf
    sConsulta = sConsulta & "   UNION" & vbCrLf
    sConsulta = sConsulta & "   --Peticionario  Dep" & vbCrLf
    sConsulta = sConsulta & "   SELECT DISTINCT ISNULL(EE.SUBJECT,'') AS TEXT,U.IDIOMA," & vbCrLf
    sConsulta = sConsulta & "       U.TIPOEMAIL, P.EMAIL, U.DATEFMT, U.THOUSANFMT, U.DECIMALFMT, U.PRECISIONFMT, 0 AS ESPROVEEDOR,P.COD CODDESTINATARIO, NE.ID AS NOTIFICADO, U.COD CODUSUARIO" & vbCrLf
    sConsulta = sConsulta & "   FROM PER P WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PER P2 WITH(NOLOCK) ON P.COD=P2.COD" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN USU U2 WITH(NOLOCK) ON P2.COD=U2.PER AND U2.BAJA=0" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN DEP D WITH(NOLOCK) ON P.DEP=D.COD" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL R WITH(NOLOCK) ON D.COD=R.DEP" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_BLOQUE B WITH(NOLOCK) ON R.INSTANCIA=B.INSTANCIA" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL_ACCION RA WITH(NOLOCK) ON R.ID=RA.ROL" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ENLACE E WITH(NOLOCK) ON RA.ACCION=E.ACCION AND B.ID=E.BLOQUE_ORIGEN" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_NOTIFICADO_ENLACE NE WITH(NOLOCK) ON E.ENLACE_ORIGEN=NE.ENLACE" & vbCrLf
    sConsulta = sConsulta & "   LEFT JOIN USU U WITH(NOLOCK) ON P.COD=U.PER AND U.BAJA=0" & vbCrLf
    sConsulta = sConsulta & "   LEFT OUTER JOIN PM_ENLACE_EMAIL EE WITH(NOLOCK) ON NE.ENLACE=EE.ENLACE AND EE.IDI=U.IDIOMA" & vbCrLf
    sConsulta = sConsulta & "   WHERE P2.BAJALOG=0 AND P2.UON1 IS NULL AND P2.UON2 IS NULL AND P2.UON3 IS NULL AND U2.BAJA=0 AND R.TIPO=4" & vbCrLf
    sConsulta = sConsulta & "       AND R.PER IS NULL AND R.DEP IS NOT NULL AND NE.TIPO_NOTIFICADO=5" & vbCrLf
    sConsulta = sConsulta & "       AND R.INSTANCIA=@ID AND RA.ACCION=@ACCION AND E.ID=@ENLACE" & vbCrLf
    sConsulta = sConsulta & "    UNION" & vbCrLf
    sConsulta = sConsulta & "   --Peticionario  Uon" & vbCrLf
    sConsulta = sConsulta & "   SELECT DISTINCT ISNULL(EE.SUBJECT,'') AS TEXT,U.IDIOMA," & vbCrLf
    sConsulta = sConsulta & "       U.TIPOEMAIL, P.EMAIL, U.DATEFMT, U.THOUSANFMT, U.DECIMALFMT, U.PRECISIONFMT, 0 AS ESPROVEEDOR,P.COD CODDESTINATARIO, NE.ID AS NOTIFICADO, U.COD CODUSUARIO" & vbCrLf
    sConsulta = sConsulta & "   FROM PER P WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PER P2 WITH(NOLOCK) ON P.COD=P2.COD" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN USU U2 WITH(NOLOCK) ON P2.COD=U2.PER AND U2.BAJA=0" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN DEP D WITH(NOLOCK) ON P.DEP=D.COD" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL R WITH(NOLOCK) ON D.COD=R.DEP AND ISNULL(P2.UON1,'#')=ISNULL(R.UON1,'#') AND ISNULL(P2.UON2,'#')=ISNULL(R.UON2,'#') AND ISNULL(P2.UON3,'#')=ISNULL(R.UON3,'#')" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_BLOQUE B WITH(NOLOCK) ON R.INSTANCIA=B.INSTANCIA" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL_ACCION RA WITH(NOLOCK) ON R.ID=RA.ROL" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ENLACE E WITH(NOLOCK) ON RA.ACCION=E.ACCION AND B.ID=E.BLOQUE_ORIGEN" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_NOTIFICADO_ENLACE NE WITH(NOLOCK) ON E.ENLACE_ORIGEN=NE.ENLACE" & vbCrLf
    sConsulta = sConsulta & "   LEFT JOIN USU U WITH(NOLOCK) ON P.COD=U.PER AND U.BAJA=0" & vbCrLf
    sConsulta = sConsulta & "   LEFT OUTER JOIN PM_ENLACE_EMAIL EE WITH(NOLOCK) ON NE.ENLACE=EE.ENLACE AND EE.IDI=U.IDIOMA" & vbCrLf
    sConsulta = sConsulta & "   WHERE P2.BAJALOG=0 AND U2.BAJA=0 AND R.TIPO=4 AND R.PER IS NULL AND (R.UON1 IS NOT NULL OR R.UON2 IS NOT NULL OR R.UON3 IS NOT NULL)" & vbCrLf
    sConsulta = sConsulta & "       AND NE.TIPO_NOTIFICADO=5 AND R.INSTANCIA=@ID AND RA.ACCION=@ACCION AND E.ID=@ENLACE" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   --Notificados: Personas Destinatarias que participaron en la etapa Origen" & vbCrLf
    sConsulta = sConsulta & "   UNION" & vbCrLf
    sConsulta = sConsulta & "   SELECT DISTINCT ISNULL(EE.SUBJECT,'') AS TEXT," & vbCrLf
    sConsulta = sConsulta & "       U.IDIOMA,U.TIPOEMAIL, P.EMAIL, U.DATEFMT, U.THOUSANFMT, U.DECIMALFMT, U.PRECISIONFMT, 0 AS ESPROVEEDOR,P.COD CODDESTINATARIO, NE.ID AS NOTIFICADO, U.COD CODUSUARIO" & vbCrLf
    sConsulta = sConsulta & "       FROM PER P WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "       INNER JOIN INSTANCIA_EST IE2 WITH(NOLOCK) ON P.COD=IE2.DESTINATARIO" & vbCrLf
    sConsulta = sConsulta & "       INNER JOIN INSTANCIA_CAMINO IC WITH(NOLOCK) ON IE2.BLOQUE=IC.INSTANCIA_BLOQUE_ORIGEN" & vbCrLf
    sConsulta = sConsulta & "       INNER JOIN INSTANCIA_EST IE WITH(NOLOCK) ON IC.INSTANCIA_EST=IE.ID" & vbCrLf
    sConsulta = sConsulta & "       INNER JOIN PM_COPIA_ENLACE E WITH(NOLOCK) ON IE.PM_ACCION=E.ACCION" & vbCrLf
    sConsulta = sConsulta & "       INNER JOIN PM_NOTIFICADO_ENLACE NE WITH(NOLOCK) ON E.ENLACE_ORIGEN=NE.ENLACE" & vbCrLf
    sConsulta = sConsulta & "       LEFT JOIN USU U WITH(NOLOCK) ON P.COD=U.PER AND U.BAJA=0" & vbCrLf
    sConsulta = sConsulta & "       LEFT OUTER JOIN PM_ENLACE_EMAIL EE WITH(NOLOCK) ON NE.ENLACE=EE.ENLACE AND EE.IDI=U.IDIOMA" & vbCrLf
    sConsulta = sConsulta & "       WHERE NE.TIPO_NOTIFICADO=5 AND E.ID=@ENLACE AND IE2.INSTANCIA=@ID" & vbCrLf
    sConsulta = sConsulta & "   --Notificados: Personas NO Peticionarias que participaron en la etapa Origen" & vbCrLf
    sConsulta = sConsulta & "   UNION" & vbCrLf
    sConsulta = sConsulta & "           --No Peticionario       Usuario" & vbCrLf
    sConsulta = sConsulta & "       SELECT DISTINCT ISNULL(EE.SUBJECT,'') AS TEXT,U.IDIOMA," & vbCrLf
    sConsulta = sConsulta & "       U.TIPOEMAIL, P.EMAIL, U.DATEFMT, U.THOUSANFMT, U.DECIMALFMT, U.PRECISIONFMT, 0 AS ESPROVEEDOR,P.COD CODDESTINATARIO, NE.ID AS NOTIFICADO, U.COD CODUSUARIO" & vbCrLf
    sConsulta = sConsulta & "       FROM PER P WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "       INNER JOIN PM_COPIA_ROL R WITH(NOLOCK) ON P.COD=R.PER" & vbCrLf
    sConsulta = sConsulta & "       INNER JOIN PM_COPIA_ROL_ACCION RA WITH(NOLOCK) ON R.ID=RA.ROL" & vbCrLf
    sConsulta = sConsulta & "       INNER JOIN PM_COPIA_ENLACE E WITH(NOLOCK) ON RA.ACCION=E.ACCION" & vbCrLf
    sConsulta = sConsulta & "       INNER JOIN PM_NOTIFICADO_ENLACE NE WITH(NOLOCK) ON E.ENLACE_ORIGEN=NE.ENLACE" & vbCrLf
    sConsulta = sConsulta & "       LEFT JOIN USU U WITH(NOLOCK) ON P.COD=U.PER AND U.BAJA=0" & vbCrLf
    sConsulta = sConsulta & "       LEFT OUTER JOIN PM_ENLACE_EMAIL EE WITH(NOLOCK) ON NE.ENLACE=EE.ENLACE AND EE.IDI=U.IDIOMA" & vbCrLf
    sConsulta = sConsulta & "       WHERE R.TIPO<>4 AND R.TIPO<>2 AND R.PER IS NOT NULL AND ISNULL(R.COMO_ASIGNAR,0)=0 AND NE.TIPO_NOTIFICADO=5" & vbCrLf
    sConsulta = sConsulta & "           AND R.INSTANCIA=@ID AND E.ID=@ENLACE" & vbCrLf
    sConsulta = sConsulta & "       UNION" & vbCrLf
    sConsulta = sConsulta & "           --No Peticionario       Lista" & vbCrLf
    sConsulta = sConsulta & "       SELECT DISTINCT ISNULL(EE.SUBJECT,'') AS TEXT,U.IDIOMA," & vbCrLf
    sConsulta = sConsulta & "       U.TIPOEMAIL, P.EMAIL, U.DATEFMT, U.THOUSANFMT, U.DECIMALFMT, U.PRECISIONFMT, 0 AS ESPROVEEDOR,P.COD CODDESTINATARIO, NE.ID AS NOTIFICADO, U.COD CODUSUARIO" & vbCrLf
    sConsulta = sConsulta & "       FROM PER P WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "       INNER JOIN PM_PARTICIPANTES PA WITH(NOLOCK) ON P.COD=PA.PER" & vbCrLf
    sConsulta = sConsulta & "       INNER JOIN PM_COPIA_ROL R WITH(NOLOCK) ON PA.ROL=R.ROL_ORIGEN AND PA.PER=R.PER" & vbCrLf
    sConsulta = sConsulta & "       INNER JOIN PM_COPIA_ROL_ACCION RA WITH(NOLOCK) ON R.ID=RA.ROL" & vbCrLf
    sConsulta = sConsulta & "       INNER JOIN PM_COPIA_ENLACE E WITH(NOLOCK) ON RA.ACCION=E.ACCION" & vbCrLf
    sConsulta = sConsulta & "       INNER JOIN PM_NOTIFICADO_ENLACE NE WITH(NOLOCK) ON E.ENLACE_ORIGEN=NE.ENLACE" & vbCrLf
    sConsulta = sConsulta & "       LEFT JOIN USU U WITH(NOLOCK) ON P.COD=U.PER AND U.BAJA=0" & vbCrLf
    sConsulta = sConsulta & "       LEFT OUTER JOIN PM_ENLACE_EMAIL EE WITH(NOLOCK) ON NE.ENLACE=EE.ENLACE AND EE.IDI=U.IDIOMA" & vbCrLf
    sConsulta = sConsulta & "       WHERE NE.TIPO_NOTIFICADO=5 AND R.TIPO<>4 AND R.TIPO<>2 AND R.INSTANCIA=@ID AND E.ID=@ENLACE" & vbCrLf
    sConsulta = sConsulta & "        UNION" & vbCrLf
    sConsulta = sConsulta & "           --No Peticionario       DeCampoDePantalla" & vbCrLf
    sConsulta = sConsulta & "       SELECT DISTINCT ISNULL(EE.SUBJECT,'') AS TEXT,U.IDIOMA," & vbCrLf
    sConsulta = sConsulta & "       U.TIPOEMAIL, P.EMAIL, U.DATEFMT, U.THOUSANFMT, U.DECIMALFMT, U.PRECISIONFMT, 0 AS ESPROVEEDOR,P.COD CODDESTINATARIO, NE.ID AS NOTIFICADO, U.COD CODUSUARIO" & vbCrLf
    sConsulta = sConsulta & "       FROM PER P WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "       INNER JOIN PM_COPIA_ROL R WITH(NOLOCK) ON P.COD=R.PER" & vbCrLf
    sConsulta = sConsulta & "       INNER JOIN PM_COPIA_ROL_ACCION RA WITH(NOLOCK) ON R.ID=RA.ROL" & vbCrLf
    sConsulta = sConsulta & "       INNER JOIN PM_COPIA_ENLACE E WITH(NOLOCK) ON RA.ACCION=E.ACCION" & vbCrLf
    sConsulta = sConsulta & "       INNER JOIN PM_NOTIFICADO_ENLACE NE WITH(NOLOCK) ON E.ENLACE_ORIGEN=NE.ENLACE" & vbCrLf
    sConsulta = sConsulta & "       LEFT JOIN USU U WITH(NOLOCK) ON P.COD=U.PER AND U.BAJA=0" & vbCrLf
    sConsulta = sConsulta & "       LEFT OUTER JOIN PM_ENLACE_EMAIL EE WITH(NOLOCK) ON NE.ENLACE=EE.ENLACE AND EE.IDI=U.IDIOMA" & vbCrLf
    sConsulta = sConsulta & "       WHERE NE.TIPO_NOTIFICADO=5 AND R.TIPO<>4 AND R.TIPO<>2 AND R.COMO_ASIGNAR=1 AND R.INSTANCIA=@ID AND E.ID=@ENLACE" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "--Notificados: Proveedores que participaron en la etapa Origen" & vbCrLf
    sConsulta = sConsulta & "   UNION" & vbCrLf
    sConsulta = sConsulta & "   SELECT DISTINCT ISNULL(EE.SUBJECT,'') AS TEXT," & vbCrLf
    sConsulta = sConsulta & "       ISNULL(C.IDI,(SELECT IDIOMA FROM PARGEN_DEF WITH(NOLOCK) WHERE PARGEN_DEF.ID=1)) IDIOMA," & vbCrLf
    sConsulta = sConsulta & "       C.TIPOEMAIL, C.EMAIL, C.DATEFMT, C.THOUSANFMT, C.DECIMALFMT, C.PRECISIONFMT, 1 AS ESPROVEEDOR,NULL CODDESTINATARIO, NE.ID AS NOTIFICADO, NULL CODUSUARIO" & vbCrLf
    sConsulta = sConsulta & "       FROM CON C WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "       INNER JOIN PM_COPIA_ROL R WITH(NOLOCK) ON C.PROVE=R.PROVE" & vbCrLf
    sConsulta = sConsulta & "       INNER JOIN PM_COPIA_ROL_CON RC WITH(NOLOCK) ON R.ID=RC.ID_ROL AND C.ID=RC.ID_CON" & vbCrLf
    sConsulta = sConsulta & "       INNER JOIN PM_COPIA_ENLACE E WITH(NOLOCK) ON E.ACCION=@ACCION" & vbCrLf
    sConsulta = sConsulta & "       INNER JOIN PM_NOTIFICADO_ENLACE NE WITH(NOLOCK) ON E.ENLACE_ORIGEN=NE.ENLACE" & vbCrLf
    sConsulta = sConsulta & "       INNER JOIN PM_COPIA_ROL_ACCION RA WITH(NOLOCK) ON R.ID=RA.ROL AND E.ACCION=RA.ACCION" & vbCrLf
    sConsulta = sConsulta & "       INNER JOIN PM_ENLACE_EMAIL EE WITH(NOLOCK) ON NE.ENLACE=EE.ENLACE AND EE.IDI=ISNULL(C.IDI,(SELECT IDIOMA FROM PARGEN_DEF WITH(NOLOCK) WHERE PARGEN_DEF.ID=1))" & vbCrLf
    sConsulta = sConsulta & "       WHERE NE.TIPO_NOTIFICADO=5 AND R.INSTANCIA=@ID AND R.TIPO=2 AND E.ID = @ENLACE" & vbCrLf
    sConsulta = sConsulta & "   UNION" & vbCrLf
    sConsulta = sConsulta & "   SELECT DISTINCT ISNULL(EE.SUBJECT,'') AS TEXT," & vbCrLf
    sConsulta = sConsulta & "       ISNULL(C.IDI,(SELECT IDIOMA FROM PARGEN_DEF WITH(NOLOCK) WHERE PARGEN_DEF.ID=1)) IDIOMA," & vbCrLf
    sConsulta = sConsulta & "       C.TIPOEMAIL, C.EMAIL, C.DATEFMT, C.THOUSANFMT, C.DECIMALFMT, C.PRECISIONFMT, 1 AS ESPROVEEDOR,NULL CODDESTINATARIO, NE.ID AS NOTIFICADO, NULL CODUSUARIO" & vbCrLf
    sConsulta = sConsulta & "       FROM CON C WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "       INNER JOIN PM_COPIA_ROL R WITH(NOLOCK) ON C.PROVE=R.PROVE" & vbCrLf
    sConsulta = sConsulta & "       AND NOT EXISTS (SELECT ID_CON FROM PM_COPIA_ROL_CON RC WITH(NOLOCK) WHERE RC.ID_ROL = R.ID)" & vbCrLf
    sConsulta = sConsulta & "       INNER JOIN PM_COPIA_ENLACE E WITH(NOLOCK) ON E.ACCION=@ACCION" & vbCrLf
    sConsulta = sConsulta & "       INNER JOIN PM_NOTIFICADO_ENLACE NE WITH(NOLOCK) ON E.ENLACE_ORIGEN=NE.ENLACE" & vbCrLf
    sConsulta = sConsulta & "       INNER JOIN PM_COPIA_ROL_ACCION RA WITH(NOLOCK) ON R.ID=RA.ROL AND E.ACCION=RA.ACCION" & vbCrLf
    sConsulta = sConsulta & "       INNER JOIN PM_ENLACE_EMAIL EE WITH(NOLOCK) ON NE.ENLACE=EE.ENLACE AND EE.IDI=ISNULL(C.IDI,(SELECT IDIOMA FROM PARGEN_DEF WITH(NOLOCK) WHERE PARGEN_DEF.ID=1))" & vbCrLf
    sConsulta = sConsulta & "       WHERE NE.TIPO_NOTIFICADO=5 AND R.INSTANCIA=@ID AND R.TIPO=2 AND E.ID = @ENLACE" & vbCrLf
    sConsulta = sConsulta & "   UNION" & vbCrLf
    sConsulta = sConsulta & "   SELECT DISTINCT ISNULL(EE.SUBJECT,'') AS TEXT," & vbCrLf
    sConsulta = sConsulta & "       ISNULL(C.IDI,(SELECT IDIOMA FROM PARGEN_DEF WITH(NOLOCK) WHERE PARGEN_DEF.ID=1)) IDIOMA," & vbCrLf
    sConsulta = sConsulta & "       C.TIPOEMAIL, C.EMAIL, C.DATEFMT, C.THOUSANFMT, C.DECIMALFMT, C.PRECISIONFMT, 1 AS ESPROVEEDOR,NULL CODDESTINATARIO, NE.ID AS NOTIFICADO, NULL CODUSUARIO" & vbCrLf
    sConsulta = sConsulta & "   FROM CON C WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN INSTANCIA_EST IE2 WITH(NOLOCK) ON IE2.CONTACTO=C.ID AND IE2.DESTINATARIO_PROV=C.PROVE" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN INSTANCIA_CAMINO IC WITH(NOLOCK) ON IE2.BLOQUE=IC.INSTANCIA_BLOQUE_ORIGEN" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN INSTANCIA_EST IE WITH(NOLOCK) ON IC.INSTANCIA_EST=IE.ID" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ENLACE E WITH(NOLOCK) ON IE.PM_ACCION=E.ACCION" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_NOTIFICADO_ENLACE NE WITH(NOLOCK) ON E.ENLACE_ORIGEN=NE.ENLACE" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_ENLACE_EMAIL EE WITH(NOLOCK) ON EE.ENLACE=NE.ENLACE AND EE.IDI=ISNULL(C.IDI,(SELECT IDIOMA FROM PARGEN_DEF WITH(NOLOCK) WHERE PARGEN_DEF.ID=1))" & vbCrLf
    sConsulta = sConsulta & "       WHERE NE.TIPO_NOTIFICADO=5 AND E.ID = @ENLACE" & vbCrLf
    sConsulta = sConsulta & "       AND E.ACCION = @ACCION AND IE2.INSTANCIA=@ID" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "--Notificados: Responsables de Procesos de Compra asociados a la solicitud" & vbCrLf
    sConsulta = sConsulta & "   UNION" & vbCrLf
    sConsulta = sConsulta & "   SELECT DISTINCT ISNULL(EE.SUBJECT,'') AS TEXT," & vbCrLf
    sConsulta = sConsulta & "       U.IDIOMA," & vbCrLf
    sConsulta = sConsulta & "       U.TIPOEMAIL, P.EMAIL, U.DATEFMT, U.THOUSANFMT, U.DECIMALFMT, U.PRECISIONFMT, 0 AS ESPROVEEDOR,P.COD CODDESTINATARIO, NE.ID AS NOTIFICADO, U.COD CODUSUARIO" & vbCrLf
    sConsulta = sConsulta & "   FROM PER P WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PROCE PR WITH(NOLOCK) ON P.COD=PR.COM" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN ITEM S WITH(NOLOCK) ON PR.ANYO =S.ANYO AND PR.GMN1 =S.GMN1_PROCE  AND PR.COD =S.PROCE " & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN INSTANCIA I WITH(NOLOCK) ON S.SOLICIT=I.ID " & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN INSTANCIA_EST IE WITH(NOLOCK) ON I.ID=IE.INSTANCIA" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ENLACE E WITH(NOLOCK) ON IE.PM_ACCION=E.ACCION" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_NOTIFICADO_ENLACE NE WITH(NOLOCK) ON E.ENLACE_ORIGEN=NE.ENLACE" & vbCrLf
    sConsulta = sConsulta & "   LEFT JOIN USU U WITH(NOLOCK) ON P.COD=U.PER AND U.BAJA=0" & vbCrLf
    sConsulta = sConsulta & "   LEFT OUTER JOIN PM_ENLACE_EMAIL EE WITH(NOLOCK) ON NE.ENLACE=EE.ENLACE AND EE.IDI=U.IDIOMA" & vbCrLf
    sConsulta = sConsulta & "   WHERE NE.TIPO_NOTIFICADO=6 AND IE.PM_ACCION=@ACCION AND E.ID=@ENLACE AND I.ID=@ID" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "--Notificado: Gestor de la Solicitud" & vbCrLf
    sConsulta = sConsulta & "   UNION" & vbCrLf
    sConsulta = sConsulta & "   SELECT DISTINCT ISNULL(EE.SUBJECT,'') AS TEXT,U.IDIOMA," & vbCrLf
    sConsulta = sConsulta & "       U.TIPOEMAIL, P.EMAIL, U.DATEFMT, U.THOUSANFMT, U.DECIMALFMT, U.PRECISIONFMT, 0 AS ESPROVEEDOR,P.COD CODDESTINATARIO, NE.ID AS NOTIFICADO, U.COD CODUSUARIO" & vbCrLf
    sConsulta = sConsulta & "   FROM PER P WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN SOLICITUD S WITH(NOLOCK) ON P.COD=S.GESTOR" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN INSTANCIA I WITH(NOLOCK) ON S.ID=I.SOLICITUD" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN INSTANCIA_EST IE WITH(NOLOCK) ON I.ID=IE.INSTANCIA" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ENLACE E WITH(NOLOCK) ON IE.PM_ACCION=E.ACCION" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_NOTIFICADO_ENLACE NE WITH(NOLOCK) ON E.ENLACE_ORIGEN=NE.ENLACE" & vbCrLf
    sConsulta = sConsulta & "   LEFT JOIN USU U WITH(NOLOCK) ON P.COD=U.PER AND U.BAJA=0" & vbCrLf
    sConsulta = sConsulta & "   LEFT OUTER JOIN PM_ENLACE_EMAIL EE WITH(NOLOCK) ON NE.ENLACE=EE.ENLACE AND EE.IDI=U.IDIOMA" & vbCrLf
    sConsulta = sConsulta & "   WHERE NE.TIPO_NOTIFICADO=7 AND IE.PM_ACCION=@ACCION AND E.ID=@ENLACE AND I.ID=@ID" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "--Notificado: roles observadores definidos como UON o Departamento" & vbCrLf
    sConsulta = sConsulta & "   UNION" & vbCrLf
    sConsulta = sConsulta & "   SELECT DISTINCT ISNULL(EE.SUBJECT,'') AS TEXT,U.IDIOMA," & vbCrLf
    sConsulta = sConsulta & "       U.TIPOEMAIL, P.EMAIL, U.DATEFMT, U.THOUSANFMT, U.DECIMALFMT, U.PRECISIONFMT, 0 AS ESPROVEEDOR,P.COD CODDESTINATARIO, NE.ID AS NOTIFICADO, U.COD CODUSUARIO" & vbCrLf
    sConsulta = sConsulta & "   FROM PER P WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   LEFT JOIN USU U WITH(NOLOCK) ON P.COD=U.PER AND U.BAJA=0" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL R WITH(NOLOCK) ON R.TIPO =5 AND R.CUANDO_ASIGNAR =2 AND R.PER IS NULL" & vbCrLf
    sConsulta = sConsulta & "   AND ISNULL(R.UON1,'') =ISNULL(P.UON1,'') AND  ISNULL(R.UON2,'') =ISNULL(P.UON2,'') AND ISNULL(R.UON3,'') =ISNULL(P.UON3,'') AND (R.DEP =P.DEP OR R.DEP IS NULL)" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_ROL ROL WITH(NOLOCK) ON R.ROL_ORIGEN=ROL.ID" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_NOTIFICADO_ENLACE NE WITH(NOLOCK)  ON NE.ROL=ROL.ID" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ENLACE E WITH(NOLOCK) ON NE.ENLACE=E.ENLACE_ORIGEN" & vbCrLf
    sConsulta = sConsulta & "   LEFT OUTER JOIN PM_ENLACE_EMAIL EE WITH(NOLOCK) ON NE.ENLACE=EE.ENLACE AND EE.IDI=U.IDIOMA" & vbCrLf
    sConsulta = sConsulta & "   WHERE R.INSTANCIA =@ID AND NE.TIPO_NOTIFICADO=2 AND E.ID=@ENLACE " & vbCrLf
    sConsulta = sConsulta & "   " & vbCrLf
    sConsulta = sConsulta & "   SELECT TEXT, IDIOMA, TIPOEMAIL, EMAIL, DATEFMT, THOUSANFMT, DECIMALFMT, PRECISIONFMT, ESPROVEEDOR,CODDESTINATARIO, MIN(NOTIFICADO) AS NOTIFICADO, CODUSUARIO,IDIOMAS.LANGUAGETAG " & vbCrLf
    sConsulta = sConsulta & "   FROM #FSPM_DATOS_MAIL_ENLACE FS" & vbCrLf
    sConsulta = sConsulta & "   LEFT JOIN IDIOMAS WITH (NOLOCK) ON FS.IDIOMA =IDIOMAS.COD " & vbCrLf
    sConsulta = sConsulta & "   GROUP BY TEXT, IDIOMA, TIPOEMAIL, EMAIL, DATEFMT, THOUSANFMT, DECIMALFMT, PRECISIONFMT, ESPROVEEDOR, CODDESTINATARIO, CODUSUARIO,IDIOMAS.LANGUAGETAG " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   DROP TABLE #FSPM_DATOS_MAIL_ENLACE" & vbCrLf
    sConsulta = sConsulta & "  END" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
End Sub

Public Sub V_31900_8_Tablas_089()
    Dim sConsulta As String
    
    sConsulta = "ALTER TABLE LOG_PRES5_IMPORTES ALTER COLUMN PRES FLOAT NULL" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "DECLARE @SQL AS NVARCHAR(MAX)" & vbCrLf
    sConsulta = sConsulta & "SET @SQL= N'ALTER TABLE USU DROP CONSTRAINT ' + (select NAME from sys.objects where name like 'DF_USU_FSAL%' and type='D' and parent_object_id=OBJECT_ID(N'[dbo].[USU]'))" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL" & vbCrLf
    sConsulta = sConsulta & "UPDATE USU SET FSAL = 0 WHERE FSAL IS NULL" & vbCrLf
    sConsulta = sConsulta & "ALTER TABLE USU ALTER COLUMN FSAL BIT NOT NULL" & vbCrLf
    sConsulta = sConsulta & "ALTER TABLE USU ADD CONSTRAINT DF_USU_FSAL  DEFAULT 0 FOR FSAL" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
End Sub

Public Function CodigoDeActualizacion31900_08_1889_A31900_08_1890() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim oADOConnection As ADODB.Connection
    Dim sConsulta As String

    On Error GoTo Error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Actualiza Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 3
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    frmProgreso.ProgressBar1.Refresh
    V_31900_8_Storeds_090
    
    sConsulta = "UPDATE VERSION SET NUM ='3.00.18.90'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103), PRODVER='31900.8'"
    ExecuteSQL gRDOCon, sConsulta
        
    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False
        
    CodigoDeActualizacion31900_08_1889_A31900_08_1890 = True
    Exit Function

Error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    CodigoDeActualizacion31900_08_1889_A31900_08_1890 = False
End Function

Public Sub V_31900_8_Storeds_090()
    Dim sConsulta As String
    
    sConsulta = "IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_SOLICITUD_DATOS_EMAIL]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_SOLICITUD_DATOS_EMAIL]" & vbCrLf
    gRDOCon.QueryTimeout = 7200
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "CREATE PROCEDURE [dbo].[FSPM_SOLICITUD_DATOS_EMAIL] @ID INT, @TYPE TINYINT, @CONTACTO INT = 0, @BLOQUE AS INT = 0, @ACCION INT=0, @ENLACE INT=0 AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "IF @TYPE = 0   --Es un traslado:" & vbCrLf
    sConsulta = sConsulta & "       IF @CONTACTO=0" & vbCrLf
    sConsulta = sConsulta & "         SELECT TOP 1 WEBFSWSTEXT.MODULO, WEBFSWSTEXT.ID, WEBFSWSTEXT.TEXT_ENG, WEBFSWSTEXT.TEXT_GER, WEBFSWSTEXT.TEXT_SPA, U.COD,ISNULL(U.IDIOMA,'ENG') IDIOMA," & vbCrLf
    sConsulta = sConsulta & "       PER.EMAIL, U.TIPOEMAIL, U.DATEFMT, U.THOUSANFMT, U.DECIMALFMT, U.PRECISIONFMT, 0 AS ESPROVEEDOR,PER.COD CODDESTINATARIO,ISNULL(IDIOMAS.LANGUAGETAG,'en-GB') LANGUAGETAG " & vbCrLf
    sConsulta = sConsulta & "         FROM INSTANCIA I WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "    INNER JOIN INSTANCIA_EST IE WITH(NOLOCK) ON I.ID=IE.INSTANCIA AND IE.ACCION = 3" & vbCrLf
    sConsulta = sConsulta & "    INNER JOIN INSTANCIA_BLOQUE IB WITH(NOLOCK) ON IE.BLOQUE = IB.ID AND IB.ID = @BLOQUE AND IB.ESTADO=1" & vbCrLf
    sConsulta = sConsulta & "    INNER JOIN PER WITH(NOLOCK) ON IE.DESTINATARIO=PER.COD" & vbCrLf
    sConsulta = sConsulta & "    LEFT JOIN USU U WITH(NOLOCK) ON PER.COD = U.PER AND U.BAJA = 0" & vbCrLf
    sConsulta = sConsulta & "    LEFT JOIN WEBFSWSTEXT WITH(NOLOCK) ON MODULO=101 AND WEBFSWSTEXT.ID=1" & vbCrLf
    sConsulta = sConsulta & "    LEFT JOIN IDIOMAS WITH(NOLOCK) ON IDIOMAS.COD=U.IDIOMA " & vbCrLf
    sConsulta = sConsulta & "         WHERE I.ID=@ID" & vbCrLf
    sConsulta = sConsulta & "    ORDER BY IE.ID DESC" & vbCrLf
    sConsulta = sConsulta & "       ELSE" & vbCrLf
    sConsulta = sConsulta & "         begin" & vbCrLf
    sConsulta = sConsulta & "           DECLARE @DATEFMT AS VARCHAR(50)" & vbCrLf
    sConsulta = sConsulta & "           DECLARE @THOUSANFMT AS CHAR(1)" & vbCrLf
    sConsulta = sConsulta & "           DECLARE @DECIMALFMT AS CHAR(1)" & vbCrLf
    sConsulta = sConsulta & "           DECLARE @PRECISIONFMT AS TINYINT" & vbCrLf
    sConsulta = sConsulta & "           DECLARE @COMP VARCHAR(50)" & vbCrLf
    sConsulta = sConsulta & "           DECLARE @FSP NVARCHAR(100)" & vbCrLf
    sConsulta = sConsulta & "           DECLARE @SQL NVARCHAR(2000)" & vbCrLf
    sConsulta = sConsulta & "           DECLARE @CON_PORTAL INT" & vbCrLf
    sConsulta = sConsulta & "           DECLARE @PROVE VARCHAR(50)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "           SET @DATEFMT=NULL" & vbCrLf
    sConsulta = sConsulta & "           SET @THOUSANFMT=NULL" & vbCrLf
    sConsulta = sConsulta & "           SET @DECIMALFMT=NULL" & vbCrLf
    sConsulta = sConsulta & "           SET @PRECISIONFMT=NULL" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "           SELECT TOP 1 @CON_PORTAL=ID_PORT, @PROVE=CON.PROVE" & vbCrLf
    sConsulta = sConsulta & "      FROM INSTANCIA I WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "      INNER JOIN INSTANCIA_EST IE WITH(NOLOCK) ON I.ID=IE.INSTANCIA AND IE.ACCION = 3" & vbCrLf
    sConsulta = sConsulta & "      INNER JOIN INSTANCIA_BLOQUE IB WITH(NOLOCK) ON IE.BLOQUE = IB.ID AND IB.ID = @BLOQUE AND IB.ESTADO = 1" & vbCrLf
    sConsulta = sConsulta & "      INNER JOIN CON WITH(NOLOCK) ON IE.DESTINATARIO_PROV=CON.PROVE" & vbCrLf
    sConsulta = sConsulta & "      WHERE I.ID=@ID AND CON.ID=@CONTACTO" & vbCrLf
    sConsulta = sConsulta & "      ORDER BY IE.ID DESC" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "           IF @CON_PORTAL IS NOT NULL" & vbCrLf
    sConsulta = sConsulta & "             begin" & vbCrLf
    sConsulta = sConsulta & "                 SELECT @FSP = FSP_SRV + '.' + FSP_BD + '.dbo.',  @COMP  = FSP_CIA FROM PARGEN_PORT WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "                 SET @SQL = N'EXEC ' +  @FSP + 'FSPM_FORMATO_CONTACTO @CON_PORTAL=@CON_PORTAL,@COMP =@COMP ,@PROVE =" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "@PROVE,@DATEFMT=@DATEFMT OUTPUT,@THOUSANFMT=@THOUSANFMT OUTPUT,@DECIMALFMT=@DECIMALFMT" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "OUTPUT,@PRECISIONFMT=@PRECISIONFMT OUTPUT'" & vbCrLf
    sConsulta = sConsulta & "            EXEC SP_EXECUTESQL @SQL, N'@CON_PORTAL INT,@COMP VARCHAR(50),@PROVE VARCHAR(50) ,@DATEFMT" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "VARCHAR(50) OUTPUT, @THOUSANFMT CHAR(1) OUTPUT, @DECIMALFMT CHAR(1) OUTPUT, @PRECISIONFMT TINYINT OUTPUT'," & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "@CON_PORTAL=@CON_PORTAL,@COMP = @COMP, @PROVE = @PROVE,@DATEFMT=@DATEFMT OUTPUT,@THOUSANFMT=@THOUSANFMT" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "OUTPUT,@DECIMALFMT=@DECIMALFMT OUTPUT,@PRECISIONFMT=@PRECISIONFMT OUTPUT" & vbCrLf
    sConsulta = sConsulta & "             end" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "           SELECT TOP 1 WEBFSWSTEXT.MODULO, WEBFSWSTEXT.ID, WEBFSWSTEXT.TEXT_ENG, WEBFSWSTEXT.TEXT_GER, WEBFSWSTEXT.TEXT_SPA," & vbCrLf
    sConsulta = sConsulta & "       CASE WHEN CON.IDI IS NULL THEN" & vbCrLf
    sConsulta = sConsulta & "           (SELECT IDIOMA FROM PARGEN_DEF WITH(NOLOCK) WHERE PARGEN_DEF.ID=1)" & vbCrLf
    sConsulta = sConsulta & "       ELSE CON.IDI" & vbCrLf
    sConsulta = sConsulta & "       END IDIOMA," & vbCrLf
    sConsulta = sConsulta & "       CON.TIPOEMAIL, CON.EMAIL, @DATEFMT AS DATEFMT, @THOUSANFMT AS THOUSANFMT, @DECIMALFMT AS DECIMALFMT," & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "@PRECISIONFMT AS PRECISIONFMT, 1 AS ESPROVEEDOR,NULL CODDESTINATARIO,IDIOMAS.LANGUAGETAG " & vbCrLf
    sConsulta = sConsulta & "           FROM INSTANCIA I WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "      INNER JOIN INSTANCIA_EST IE WITH(NOLOCK) ON I.INSTANCIA_EST=IE.ID AND IE.ACCION = 3" & vbCrLf
    sConsulta = sConsulta & "          INNER JOIN INSTANCIA_BLOQUE IB WITH(NOLOCK) ON IE.BLOQUE = IB.ID AND IB.ID = @BLOQUE AND IB.ESTADO = 1" & vbCrLf
    sConsulta = sConsulta & "           INNER JOIN CON WITH(NOLOCK) ON IE.DESTINATARIO_PROV=CON.PROVE" & vbCrLf
    sConsulta = sConsulta & "      LEFT JOIN WEBFSWSTEXT WITH(NOLOCK) ON MODULO=101 AND WEBFSWSTEXT.ID=1" & vbCrLf
    sConsulta = sConsulta & "      LEFT JOIN IDIOMAS WITH(NOLOCK) ON IDIOMAS.COD = CON.ID" & vbCrLf
    sConsulta = sConsulta & "           WHERE I.ID=@ID  AND CON.ID=@CONTACTO" & vbCrLf
    sConsulta = sConsulta & "      " & vbCrLf
    sConsulta = sConsulta & "      ORDER BY IE.ID DESC" & vbCrLf
    sConsulta = sConsulta & "        end" & vbCrLf
    sConsulta = sConsulta & "ELSE" & vbCrLf
    sConsulta = sConsulta & "  IF @TYPE = 1  -- Es una Devolucion" & vbCrLf
    sConsulta = sConsulta & "   BEGIN" & vbCrLf
    sConsulta = sConsulta & "     SELECT TOP 1 WEBFSWSTEXT.MODULO, WEBFSWSTEXT.ID, WEBFSWSTEXT.TEXT_ENG, WEBFSWSTEXT.TEXT_GER, WEBFSWSTEXT.TEXT_SPA," & vbCrLf
    sConsulta = sConsulta & "       U.IDIOMA,U.TIPOEMAIL, PER.EMAIL, U.DATEFMT, U.THOUSANFMT, U.DECIMALFMT, U.PRECISIONFMT, 0 AS ESPROVEEDOR,PER.COD CODDESTINATARIO,IDIOMAS.LANGUAGETAG " & vbCrLf
    sConsulta = sConsulta & "          FROM INSTANCIA I WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "     INNER JOIN INSTANCIA_EST IE WITH(NOLOCK) ON I.ID=IE.INSTANCIA AND IE.ACCION = 4" & vbCrLf
    sConsulta = sConsulta & "     INNER JOIN INSTANCIA_BLOQUE IB WITH(NOLOCK) ON IE.BLOQUE = IB.ID AND IB.ID = @BLOQUE AND IB.ESTADO = 1" & vbCrLf
    sConsulta = sConsulta & "     INNER JOIN PER WITH(NOLOCK) ON IE.DESTINATARIO = PER.COD" & vbCrLf
    sConsulta = sConsulta & "     LEFT JOIN USU U WITH(NOLOCK) ON PER.COD = U.PER AND U.BAJA = 0" & vbCrLf
    sConsulta = sConsulta & "     LEFT JOIN WEBFSWSTEXT WITH(NOLOCK) ON MODULO=101 AND WEBFSWSTEXT.ID=2" & vbCrLf
    sConsulta = sConsulta & "     LEFT JOIN IDIOMAS WITH(NOLOCK) ON IDIOMAS.COD=U.IDIOMA " & vbCrLf
    sConsulta = sConsulta & "          WHERE I.ID=@ID ORDER BY IE.ID DESC" & vbCrLf
    sConsulta = sConsulta & "   END" & vbCrLf
    sConsulta = sConsulta & "ELSE" & vbCrLf
    sConsulta = sConsulta & "  IF @TYPE = 2  --NOTIFICADOS DE ACCION" & vbCrLf
    sConsulta = sConsulta & "  BEGIN" & vbCrLf
    sConsulta = sConsulta & "   CREATE TABLE #FSPM_DATOS_MAIL_ACCION (" & vbCrLf
    sConsulta = sConsulta & "       TEXT varchar(200)," & vbCrLf
    sConsulta = sConsulta & "       IDIOMA varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AS," & vbCrLf
    sConsulta = sConsulta & "       TIPOEMAIL tinyint," & vbCrLf
    sConsulta = sConsulta & "       EMAIL varchar(100)," & vbCrLf
    sConsulta = sConsulta & "       DATEFMT varchar(50)," & vbCrLf
    sConsulta = sConsulta & "       THOUSANFMT char(1)," & vbCrLf
    sConsulta = sConsulta & "       DECIMALFMT char(1)," & vbCrLf
    sConsulta = sConsulta & "       PRECISIONFMT tinyint," & vbCrLf
    sConsulta = sConsulta & "       ESPROVEEDOR int," & vbCrLf
    sConsulta = sConsulta & "       CODDESTINATARIO varchar(20)," & vbCrLf
    sConsulta = sConsulta & "       NOTIFICADO int," & vbCrLf
    sConsulta = sConsulta & "       CODUSUARIO varchar(50)" & vbCrLf
    sConsulta = sConsulta & "   )" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   INSERT INTO #FSPM_DATOS_MAIL_ACCION (TEXT, IDIOMA, TIPOEMAIL, EMAIL, DATEFMT, THOUSANFMT, DECIMALFMT, PRECISIONFMT, ESPROVEEDOR, CODDESTINATARIO, NOTIFICADO, CODUSUARIO)" & vbCrLf
    sConsulta = sConsulta & "--Notificados: Especificos" & vbCrLf
    sConsulta = sConsulta & "   SELECT DISTINCT ISNULL(AE.SUBJECT,'') AS TEXT," & vbCrLf
    sConsulta = sConsulta & "       U.IDIOMA,U.TIPOEMAIL, P.EMAIL, U.DATEFMT, U.THOUSANFMT, U.DECIMALFMT, U.PRECISIONFMT, 0 AS ESPROVEEDOR,P.COD CODDESTINATARIO, NA.ID AS NOTIFICADO, U.COD" & vbCrLf
    sConsulta = sConsulta & "   FROM PER P WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   LEFT JOIN USU U WITH(NOLOCK) ON P.COD=U.PER AND U.BAJA = 0" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_NOTIFICADO_ACCION NA WITH(NOLOCK) ON P.COD=NA.PER" & vbCrLf
    sConsulta = sConsulta & "   LEFT JOIN PM_ACCIONES_EMAIL AE WITH(NOLOCK) ON NA.ACCION=AE.ACCION AND AE.IDI=U.IDIOMA" & vbCrLf
    sConsulta = sConsulta & "    INNER JOIN PM_COPIA_ACCIONES A WITH(NOLOCK) ON AE.ACCION=A.ACCION_ORIGEN" & vbCrLf
    sConsulta = sConsulta & "   WHERE NA.TIPO_NOTIFICADO=1 AND A.ID=@ACCION" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "--Notificados: Roles(Proveedor)" & vbCrLf
    sConsulta = sConsulta & "   UNION" & vbCrLf
    sConsulta = sConsulta & "   SELECT DISTINCT ISNULL(AE.SUBJECT,'') AS TEXT," & vbCrLf
    sConsulta = sConsulta & "       ISNULL(C.IDI,(SELECT IDIOMA FROM PARGEN_DEF WITH(NOLOCK) WHERE PARGEN_DEF.ID=1)) IDIOMA," & vbCrLf
    sConsulta = sConsulta & "       C.TIPOEMAIL, C.EMAIL, C.DATEFMT, C.THOUSANFMT, C.DECIMALFMT, C.PRECISIONFMT, 1 AS ESPROVEEDOR,NULL CODDESTINATARIO, NA.ID AS NOTIFICADO, NULL CODUSUARIO" & vbCrLf
    sConsulta = sConsulta & "   FROM CON C WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL R WITH(NOLOCK) ON C.PROVE=R.PROVE" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL_CON RC WITH(NOLOCK) ON R.ID=RC.ID_ROL AND C.ID=RC.ID_CON" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_ROL ROL WITH(NOLOCK) ON R.ROL_ORIGEN=ROL.ID" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_ACCIONES_EMAIL AE WITH(NOLOCK) ON AE.IDI=ISNULL(C.IDI,(SELECT IDIOMA FROM PARGEN_DEF WITH(NOLOCK) WHERE PARGEN_DEF.ID=1))" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_NOTIFICADO_ACCION NA WITH(NOLOCK) ON AE.ACCION=NA.ACCION AND ROL.ID=NA.ROL" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ACCIONES A WITH(NOLOCK) ON NA.ACCION=A.ACCION_ORIGEN" & vbCrLf
    sConsulta = sConsulta & "   WHERE R.TIPO = 2 AND NA.TIPO_NOTIFICADO=2 AND A.ID=@ACCION AND R.INSTANCIA=@ID" & vbCrLf
    sConsulta = sConsulta & "   UNION" & vbCrLf
    sConsulta = sConsulta & "   SELECT DISTINCT ISNULL(AE.SUBJECT,'') AS TEXT," & vbCrLf
    sConsulta = sConsulta & "   ISNULL(C.IDI,(SELECT IDIOMA FROM PARGEN_DEF WITH(NOLOCK) WHERE PARGEN_DEF.ID=1)) IDIOMA," & vbCrLf
    sConsulta = sConsulta & "   C.TIPOEMAIL, C.EMAIL, C.DATEFMT, C.THOUSANFMT, C.DECIMALFMT, C.PRECISIONFMT, 1 AS ESPROVEEDOR,NULL CODDESTINATARIO, NA.ID AS NOTIFICADO, NULL CODUSUARIO" & vbCrLf
    sConsulta = sConsulta & "   FROM CON C WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL R WITH(NOLOCK) ON C.PROVE=R.PROVE " & vbCrLf
    sConsulta = sConsulta & "   AND NOT EXISTS (SELECT ID_CON FROM PM_COPIA_ROL_CON RC WITH(NOLOCK) WHERE RC.ID_ROL = R.ID)" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_ROL ROL WITH(NOLOCK) ON R.ROL_ORIGEN=ROL.ID" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_ACCIONES_EMAIL AE WITH(NOLOCK) ON AE.IDI=ISNULL(C.IDI,(SELECT IDIOMA FROM PARGEN_DEF WITH(NOLOCK) WHERE PARGEN_DEF.ID=1))" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_NOTIFICADO_ACCION NA WITH(NOLOCK) ON AE.ACCION=NA.ACCION AND ROL.ID=NA.ROL" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ACCIONES A WITH(NOLOCK) ON NA.ACCION=A.ACCION_ORIGEN" & vbCrLf
    sConsulta = sConsulta & "   WHERE R.TIPO = 2 AND NA.TIPO_NOTIFICADO=2 AND A.ID=@ACCION AND R.INSTANCIA=@ID" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "--Notificados: Roles(Comprador, Usuario, Peticionario)" & vbCrLf
    sConsulta = sConsulta & "   UNION" & vbCrLf
    sConsulta = sConsulta & "   SELECT DISTINCT ISNULL(AE.SUBJECT,'') AS TEXT," & vbCrLf
    sConsulta = sConsulta & "       U.IDIOMA," & vbCrLf
    sConsulta = sConsulta & "       U.TIPOEMAIL, P.EMAIL, U.DATEFMT, U.THOUSANFMT, U.DECIMALFMT, U.PRECISIONFMT, 0 AS ESPROVEEDOR,P.COD CODDESTINATARIO, NA.ID AS NOTIFICADO, U.COD" & vbCrLf
    sConsulta = sConsulta & "   FROM PER P WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   LEFT JOIN USU U WITH(NOLOCK) ON P.COD=U.PER AND U.BAJA = 0" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL R WITH(NOLOCK) ON P.COD=R.PER" & vbCrLf
    sConsulta = sConsulta & "    INNER JOIN PM_ROL ROL WITH(NOLOCK) ON R.ROL_ORIGEN=ROL.ID" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_NOTIFICADO_ACCION NA WITH(NOLOCK) ON ROL.ID=NA.ROL" & vbCrLf
    sConsulta = sConsulta & "   LEFT OUTER JOIN PM_ACCIONES_EMAIL AE WITH(NOLOCK) ON NA.ACCION=AE.ACCION AND AE.IDI=U.IDIOMA" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ACCIONES A WITH(NOLOCK) ON NA.ACCION=A.ACCION_ORIGEN" & vbCrLf
    sConsulta = sConsulta & "   WHERE R.TIPO <> 2 AND NA.TIPO_NOTIFICADO=2 AND A.ID=@ACCION AND R.INSTANCIA=@ID" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "--Notificados: Roles( Lista de autoasignaci�n)" & vbCrLf
    sConsulta = sConsulta & "   UNION" & vbCrLf
    sConsulta = sConsulta & "   SELECT DISTINCT ISNULL(AE.SUBJECT,'') AS TEXT," & vbCrLf
    sConsulta = sConsulta & "                   U.IDIOMA,U.TIPOEMAIL, P.EMAIL, U.DATEFMT, U.THOUSANFMT, U.DECIMALFMT, U.PRECISIONFMT, 0 AS ESPROVEEDOR,P.COD CODDESTINATARIO, NA.ID AS NOTIFICADO, U.COD" & vbCrLf
    sConsulta = sConsulta & "   FROM PM_COPIA_ROL R WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "    INNER JOIN PM_ROL ROL WITH(NOLOCK) ON R.ROL_ORIGEN=ROL.ID" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_NOTIFICADO_ACCION NA WITH(NOLOCK) ON ROL.ID=NA.ROL" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_PARTICIPANTES PCP WITH(NOLOCK)ON NA.ROL=PCP.ROL" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PER P WITH(NOLOCK) ON PCP.PER = P.COD" & vbCrLf
    sConsulta = sConsulta & "   LEFT JOIN USU U WITH(NOLOCK) ON P.COD = U.PER AND U.BAJA = 0" & vbCrLf
    sConsulta = sConsulta & "   LEFT OUTER JOIN PM_ACCIONES_EMAIL AE WITH(NOLOCK) ON NA.ACCION=AE.ACCION AND AE.IDI=U.IDIOMA" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ACCIONES A WITH(NOLOCK) ON NA.ACCION=A.ACCION_ORIGEN" & vbCrLf
    sConsulta = sConsulta & "              WHERE R.TIPO =3 AND NA.TIPO_NOTIFICADO=2 AND A.ID=@ACCION  AND R.COMO_ASIGNAR=3 AND R.INSTANCIA=@ID" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "--Notificados: Roles(GESTOR DE LA FACTURA)" & vbCrLf
    sConsulta = sConsulta & "   UNION" & vbCrLf
    sConsulta = sConsulta & "   SELECT DISTINCT ISNULL(AE.SUBJECT,'') AS TEXT," & vbCrLf
    sConsulta = sConsulta & "   U.IDIOMA,U.TIPOEMAIL, P.EMAIL, U.DATEFMT, U.THOUSANFMT, U.DECIMALFMT, U.PRECISIONFMT, 0 AS ESPROVEEDOR,P.COD CODDESTINATARIO, NA.ID AS NOTIFICADO, U.COD" & vbCrLf
    sConsulta = sConsulta & "   FROM PER P WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   LEFT JOIN USU U WITH(NOLOCK) ON P.COD=U.PER AND U.BAJA = 0" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PRES5_IMPORTES PI WITH(NOLOCK) ON PI.GESTOR = P.COD" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN LINEAS_PED_IMPUTACION LPI WITH(NOLOCK) ON LPI.PRES5_IMP = PI.ID" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN LINEAS_PEDIDO_FACTURA LPF WITH(NOLOCK) ON LPF.LINEA_PEDIDO = LPI.LINEA" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN LIN_FACTURA lf with(nolock) on lf.FACTURA = lpf.FACTURA" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN FACTURA F WITH(NOLOCK) ON F.ID = lPF.FACTURA" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL r with(nolock) on r.INSTANCIA = f.INSTANCIA" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_ROL ROL WITH(NOLOCK) ON R.ROL_ORIGEN=ROL.ID" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_NOTIFICADO_ACCION NA WITH(NOLOCK) ON ROL.ID=NA.ROL" & vbCrLf
    sConsulta = sConsulta & "   LEFT OUTER JOIN PM_ACCIONES_EMAIL AE WITH(NOLOCK) ON NA.ACCION=AE.ACCION AND AE.IDI=U.IDIOMA" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ACCIONES A WITH(NOLOCK) ON NA.ACCION=A.ACCION_ORIGEN" & vbCrLf
    sConsulta = sConsulta & "   WHERE F.INSTANCIA = @ID and A.ID=@ACCION and R.TIPO = 6 and R.GESTOR_FACTURA = 1 AND NA.TIPO_NOTIFICADO=2" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "--Notificado: roles observadores definidos como UON o Departamento" & vbCrLf
    sConsulta = sConsulta & "   UNION" & vbCrLf
    sConsulta = sConsulta & "   SELECT DISTINCT ISNULL(AE.SUBJECT,'') AS TEXT," & vbCrLf
    sConsulta = sConsulta & "                   U.IDIOMA,U.TIPOEMAIL, P.EMAIL, U.DATEFMT, U.THOUSANFMT, U.DECIMALFMT, U.PRECISIONFMT, 0 AS ESPROVEEDOR,P.COD CODDESTINATARIO, NA.ID AS NOTIFICADO, U.COD" & vbCrLf
    sConsulta = sConsulta & "   FROM PER P WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   LEFT JOIN USU U WITH(NOLOCK) ON P.COD=U.PER AND U.BAJA=0" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL R WITH(NOLOCK) ON R.CUANDO_ASIGNAR =2 AND R.PER IS NULL" & vbCrLf
    sConsulta = sConsulta & "   AND ISNULL(R.UON1,'') =ISNULL(P.UON1,'') AND  ISNULL(R.UON2,'') =ISNULL(P.UON2,'') AND ISNULL(R.UON3,'') =ISNULL(P.UON3,'') AND (R.DEP =P.DEP OR R.DEP IS NULL)" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_ROL ROL WITH(NOLOCK) ON R.ROL_ORIGEN=ROL.ID" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_NOTIFICADO_ACCION NA WITH(NOLOCK) ON NA.ROL=ROL.ID" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ACCIONES A WITH(NOLOCK) ON NA.ACCION=A.ACCION_ORIGEN" & vbCrLf
    sConsulta = sConsulta & "   LEFT OUTER JOIN PM_ACCIONES_EMAIL AE WITH(NOLOCK) ON NA.ACCION=AE.ACCION AND AE.IDI=U.IDIOMA" & vbCrLf
    sConsulta = sConsulta & "   WHERE R.TIPO =5 AND R.INSTANCIA =@ID AND NA.TIPO_NOTIFICADO=2 AND A.ID=@ACCION " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "--Notificado: Peticionario" & vbCrLf
    sConsulta = sConsulta & "   UNION" & vbCrLf
    sConsulta = sConsulta & "   SELECT DISTINCT ISNULL(AE.SUBJECT,'') AS TEXT," & vbCrLf
    sConsulta = sConsulta & "       U.IDIOMA,U.TIPOEMAIL, P.EMAIL, U.DATEFMT, U.THOUSANFMT, U.DECIMALFMT, U.PRECISIONFMT, 0 AS ESPROVEEDOR,P.COD CODDESTINATARIO, NA.ID AS NOTIFICADO, U.COD" & vbCrLf
    sConsulta = sConsulta & "   FROM PER P WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   LEFT JOIN USU U WITH(NOLOCK) ON P.COD=U.PER AND U.BAJA=0" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN INSTANCIA I WITH(NOLOCK) ON P.COD=I.PETICIONARIO" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN INSTANCIA_EST IE WITH(NOLOCK) ON I.ID=IE.INSTANCIA" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ACCIONES A WITH(NOLOCK) ON IE.PM_ACCION=A.ID" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_NOTIFICADO_ACCION NA WITH(NOLOCK) ON A.ACCION_ORIGEN=NA.ACCION" & vbCrLf
    sConsulta = sConsulta & "   LEFT OUTER JOIN PM_ACCIONES_EMAIL AE WITH(NOLOCK) ON NA.ACCION=AE.ACCION AND AE.IDI=U.IDIOMA" & vbCrLf
    sConsulta = sConsulta & "   WHERE NA.TIPO_NOTIFICADO = 3 AND A.ID = @ACCION AND I.ID=@ID" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "--Notificado: Autor de la accion (Rol Proveedor)" & vbCrLf
    sConsulta = sConsulta & "   UNION" & vbCrLf
    sConsulta = sConsulta & "   SELECT DISTINCT ISNULL(AE.SUBJECT,'') AS TEXT," & vbCrLf
    sConsulta = sConsulta & "       ISNULL(C.IDI,(SELECT IDIOMA FROM PARGEN_DEF WITH(NOLOCK) WHERE PARGEN_DEF.ID=1)) IDIOMA," & vbCrLf
    sConsulta = sConsulta & "       C.TIPOEMAIL, C.EMAIL, C.DATEFMT, C.THOUSANFMT, C.DECIMALFMT, C.PRECISIONFMT, 1 AS ESPROVEEDOR,NULL CODDESTINATARIO, NA.ID AS NOTIFICADO, NULL CODUSUARIO" & vbCrLf
    sConsulta = sConsulta & "   FROM CON C WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL R WITH(NOLOCK) ON C.PROVE=R.PROVE" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL_CON RC WITH(NOLOCK) ON R.ID=RC.ID_ROL AND C.ID=RC.ID_CON" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN INSTANCIA_EST IE WITH(NOLOCK) ON R.ID=IE.ROL" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ACCIONES A WITH(NOLOCK) ON IE.PM_ACCION=A.ID" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_NOTIFICADO_ACCION NA WITH(NOLOCK) ON A.ACCION_ORIGEN=NA.ACCION" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_ACCIONES_EMAIL AE WITH(NOLOCK) ON NA.ACCION=AE.ACCION AND AE.IDI=ISNULL(C.IDI,(SELECT IDIOMA FROM PARGEN_DEF WITH(NOLOCK) WHERE PARGEN_DEF.ID=1))" & vbCrLf
    sConsulta = sConsulta & "   WHERE R.TIPO =2 AND NA.TIPO_NOTIFICADO=4 AND A.ID=@ACCION AND R.INSTANCIA=@ID" & vbCrLf
    sConsulta = sConsulta & "   UNION" & vbCrLf
    sConsulta = sConsulta & "   SELECT DISTINCT ISNULL(AE.SUBJECT,'') AS TEXT," & vbCrLf
    sConsulta = sConsulta & "        ISNULL(C.IDI,(SELECT IDIOMA FROM PARGEN_DEF WITH(NOLOCK) WHERE PARGEN_DEF.ID=1)) IDIOMA," & vbCrLf
    sConsulta = sConsulta & "        C.TIPOEMAIL, C.EMAIL, C.DATEFMT, C.THOUSANFMT, C.DECIMALFMT, C.PRECISIONFMT, 1 AS ESPROVEEDOR,NULL CODDESTINATARIO, NA.ID AS NOTIFICADO, NULL CODUSUARIO" & vbCrLf
    sConsulta = sConsulta & "   FROM CON C WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL R WITH(NOLOCK) ON C.PROVE=R.PROVE" & vbCrLf
    sConsulta = sConsulta & "   AND NOT EXISTS (SELECT ID_CON FROM PM_COPIA_ROL_CON RC WITH(NOLOCK) WHERE RC.ID_ROL = R.ID)" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN INSTANCIA_EST IE WITH(NOLOCK) ON R.ID=IE.ROL" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ACCIONES A WITH(NOLOCK) ON IE.PM_ACCION=A.ID" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_NOTIFICADO_ACCION NA WITH(NOLOCK) ON A.ACCION_ORIGEN=NA.ACCION" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_ACCIONES_EMAIL AE WITH(NOLOCK) ON NA.ACCION=AE.ACCION AND AE.IDI=ISNULL(C.IDI,(SELECT IDIOMA FROM PARGEN_DEF WITH(NOLOCK) WHERE PARGEN_DEF.ID=1))" & vbCrLf
    sConsulta = sConsulta & "   WHERE R.TIPO =2 AND NA.TIPO_NOTIFICADO=4 AND A.ID=@ACCION AND R.INSTANCIA=@ID" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "--Notificado: Autor de la accion (Rol Comprador, Usuario, Peticionario)" & vbCrLf
    sConsulta = sConsulta & "   UNION" & vbCrLf
    sConsulta = sConsulta & "   SELECT DISTINCT ISNULL(AE.SUBJECT,'') AS TEXT," & vbCrLf
    sConsulta = sConsulta & "       U.IDIOMA,U.TIPOEMAIL, P.EMAIL, U.DATEFMT, U.THOUSANFMT, U.DECIMALFMT, U.PRECISIONFMT, 0 AS ESPROVEEDOR,P.COD CODDESTINATARIO, NA.ID AS NOTIFICADO, U.COD" & vbCrLf
    sConsulta = sConsulta & "   FROM PER P WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   LEFT JOIN USU U WITH(NOLOCK) ON P.COD=U.PER AND U.BAJA=0" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL R WITH(NOLOCK) ON P.COD=R.PER" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN INSTANCIA_EST IE WITH(NOLOCK) ON R.ID=IE.ROL" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ACCIONES A WITH(NOLOCK) ON IE.PM_ACCION = A.ID" & vbCrLf
    sConsulta = sConsulta & "    INNER JOIN PM_NOTIFICADO_ACCION NA WITH(NOLOCK) ON A.ACCION_ORIGEN=NA.ACCION" & vbCrLf
    sConsulta = sConsulta & "   LEFT OUTER JOIN PM_ACCIONES_EMAIL AE WITH(NOLOCK) ON NA.ACCION=AE.ACCION AND AE.IDI=U.IDIOMA" & vbCrLf
    sConsulta = sConsulta & "   WHERE R.TIPO<>2 AND NA.TIPO_NOTIFICADO=4 AND A.ID=@ACCION AND R.INSTANCIA=@ID" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "--Notificados: Personas que participaron en la etapa Origen" & vbCrLf
    sConsulta = sConsulta & "   UNION" & vbCrLf
    sConsulta = sConsulta & "   --Peticionario  Usuario" & vbCrLf
    sConsulta = sConsulta & "   SELECT DISTINCT ISNULL(AE.SUBJECT,'') AS TEXT," & vbCrLf
    sConsulta = sConsulta & "       U.IDIOMA,U.TIPOEMAIL, P.EMAIL, U.DATEFMT, U.THOUSANFMT, U.DECIMALFMT, U.PRECISIONFMT, 0 AS ESPROVEEDOR,P.COD CODDESTINATARIO, NA.ID AS NOTIFICADO, U.COD" & vbCrLf
    sConsulta = sConsulta & "   FROM PER P WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL R WITH(NOLOCK) ON P.COD=R.PER" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL_ACCION RA WITH(NOLOCK) ON R.ID=RA.ROL" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_BLOQUE B WITH(NOLOCK) ON RA.BLOQUE=B.ID AND R.INSTANCIA=B.INSTANCIA" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ACCIONES A WITH(NOLOCK) ON RA.ACCION=A.ID AND B.ID=A.BLOQUE" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_NOTIFICADO_ACCION NA WITH(NOLOCK) ON A.ACCION_ORIGEN=NA.ACCION" & vbCrLf
    sConsulta = sConsulta & "   LEFT JOIN USU U WITH(NOLOCK) ON P.COD=U.PER AND U.BAJA = 0" & vbCrLf
    sConsulta = sConsulta & "   LEFT OUTER JOIN PM_ACCIONES_EMAIL AE WITH(NOLOCK) ON NA.ACCION=AE.ACCION AND AE.IDI=U.IDIOMA" & vbCrLf
    sConsulta = sConsulta & "   WHERE R.TIPO=4 AND R.PER IS NOT NULL AND NA.TIPO_NOTIFICADO=5 AND RA.ACCION=@ACCION AND R.INSTANCIA=@ID" & vbCrLf
    sConsulta = sConsulta & "   UNION" & vbCrLf
    sConsulta = sConsulta & "   --Peticionario  Dep" & vbCrLf
    sConsulta = sConsulta & "   SELECT DISTINCT ISNULL(AE.SUBJECT,'') AS TEXT," & vbCrLf
    sConsulta = sConsulta & "       U.IDIOMA,U.TIPOEMAIL, P.EMAIL, U.DATEFMT, U.THOUSANFMT, U.DECIMALFMT, U.PRECISIONFMT, 0 AS ESPROVEEDOR,P.COD CODDESTINATARIO, NA.ID AS NOTIFICADO, U.COD" & vbCrLf
    sConsulta = sConsulta & "   FROM PER P WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PER P2 WITH(NOLOCK) ON P.COD=P2.COD" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN DEP D WITH(NOLOCK) ON P2.DEP=D.COD" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN USU U2 WITH(NOLOCK) ON P2.COD=U2.PER AND U2.BAJA=0" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL R WITH(NOLOCK) ON D.COD=R.DEP" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_BLOQUE B WITH(NOLOCK) ON R.INSTANCIA=B.INSTANCIA" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL_ACCION RA WITH(NOLOCK) ON R.ID=RA.ROL AND B.ID=RA.BLOQUE" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ACCIONES CA WITH(NOLOCK) ON RA.ACCION=CA.ID AND B.ID=CA.BLOQUE" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_NOTIFICADO_ACCION NA WITH(NOLOCK) ON CA.ACCION_ORIGEN=NA.ACCION" & vbCrLf
    sConsulta = sConsulta & "   LEFT JOIN USU U WITH(NOLOCK) ON P.COD=U.PER AND U.BAJA=0" & vbCrLf
    sConsulta = sConsulta & "   LEFT OUTER JOIN PM_ACCIONES_EMAIL AE WITH(NOLOCK) ON NA.ACCION=AE.ACCION AND AE.IDI=U.IDIOMA" & vbCrLf
    sConsulta = sConsulta & "   WHERE P2.BAJALOG=0 AND P2.UON1 IS NULL AND P2.UON2 IS NULL AND P.UON3 IS NULL AND U2.BAJA=0 AND R.TIPO=4 AND R.PER IS NULL AND R.DEP IS NOT NULL" & vbCrLf
    sConsulta = sConsulta & "       AND NA.TIPO_NOTIFICADO=5 AND RA.ACCION=@ACCION AND R.INSTANCIA=@ID" & vbCrLf
    sConsulta = sConsulta & "   UNION" & vbCrLf
    sConsulta = sConsulta & "   --Peticionario  Uon" & vbCrLf
    sConsulta = sConsulta & "   SELECT DISTINCT ISNULL(AE.SUBJECT,'') AS TEXT," & vbCrLf
    sConsulta = sConsulta & "       U.IDIOMA,U.TIPOEMAIL, P.EMAIL, U.DATEFMT, U.THOUSANFMT, U.DECIMALFMT, U.PRECISIONFMT, 0 AS ESPROVEEDOR,P.COD CODDESTINATARIO, NA.ID AS NOTIFICADO, U.COD" & vbCrLf
    sConsulta = sConsulta & "   FROM PER P WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PER P2 WITH(NOLOCK) ON P.COD=P2.COD" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN DEP D WITH(NOLOCK) ON P.DEP=D.COD" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL R WITH(NOLOCK) ON D.COD=R.DEP AND ISNULL(P2.UON1,'#')=ISNULL(R.UON1,'#') AND ISNULL(P2.UON2,'#')=ISNULL(R.UON2,'#') AND ISNULL(P2.UON3,'#')=ISNULL(R.UON3,'#')" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_BLOQUE B WITH(NOLOCK) ON R.INSTANCIA=B.INSTANCIA" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL_ACCION RA WITH(NOLOCK) ON R.ID=RA.ROL AND B.ID=RA.BLOQUE" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ACCIONES A WITH(NOLOCK) ON RA.ACCION=A.ID" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_NOTIFICADO_ACCION NA WITH(NOLOCK) ON A.ACCION_ORIGEN=NA.ACCION" & vbCrLf
    sConsulta = sConsulta & "   LEFT JOIN USU U WITH(NOLOCK) ON P.COD=U.PER AND U.BAJA=0" & vbCrLf
    sConsulta = sConsulta & "   LEFT OUTER JOIN PM_ACCIONES_EMAIL AE WITH(NOLOCK) ON NA.ACCION=AE.ACCION AND AE.IDI=U.IDIOMA" & vbCrLf
    sConsulta = sConsulta & "   WHERE P2.BAJALOG=0 AND R.TIPO=4 AND R.PER IS NULL AND (R.UON1 IS NOT NULL OR R.UON2 IS NOT NULL OR R.UON3 IS NOT NULL)" & vbCrLf
    sConsulta = sConsulta & "       AND NA.TIPO_NOTIFICADO=5 AND RA.ACCION=@ACCION AND R.INSTANCIA=@ID" & vbCrLf
    sConsulta = sConsulta & "   UNION" & vbCrLf
    sConsulta = sConsulta & "   --No Peticionario       Usuario" & vbCrLf
    sConsulta = sConsulta & "   SELECT DISTINCT ISNULL(AE.SUBJECT,'') AS TEXT," & vbCrLf
    sConsulta = sConsulta & "       U.IDIOMA,U.TIPOEMAIL, P.EMAIL, U.DATEFMT, U.THOUSANFMT, U.DECIMALFMT, U.PRECISIONFMT, 0 AS ESPROVEEDOR,P.COD CODDESTINATARIO, NA.ID AS NOTIFICADO, U.COD" & vbCrLf
    sConsulta = sConsulta & "   FROM PER P WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL R WITH(NOLOCK) ON P.COD=R.PER" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_BLOQUE B WITH(NOLOCK) ON R.INSTANCIA=B.INSTANCIA" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL_ACCION RA WITH(NOLOCK) ON R.ID=RA.ROL AND B.ID=RA.BLOQUE" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ACCIONES A WITH(NOLOCK) ON RA.ACCION=A.ID AND B.ID=A.BLOQUE" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_NOTIFICADO_ACCION NA WITH(NOLOCK) ON A.ACCION_ORIGEN=NA.ACCION" & vbCrLf
    sConsulta = sConsulta & "   LEFT JOIN USU U WITH(NOLOCK) ON P.COD=U.PER AND U.BAJA=0" & vbCrLf
    sConsulta = sConsulta & "   LEFT OUTER JOIN PM_ACCIONES_EMAIL AE WITH(NOLOCK) ON NA.ACCION=AE.ACCION AND AE.IDI=U.IDIOMA" & vbCrLf
    sConsulta = sConsulta & "   WHERE R.TIPO<>4 AND R.TIPO<>2 AND R.PER IS NOT NULL AND ISNULL(R.COMO_ASIGNAR,0)=0" & vbCrLf
    sConsulta = sConsulta & "       AND NA.TIPO_NOTIFICADO=5 AND RA.ACCION=@ACCION AND R.INSTANCIA=@ID" & vbCrLf
    sConsulta = sConsulta & "   UNION" & vbCrLf
    sConsulta = sConsulta & "   --No Peticionario       Lista" & vbCrLf
    sConsulta = sConsulta & "   SELECT DISTINCT ISNULL(AE.SUBJECT,'') AS TEXT," & vbCrLf
    sConsulta = sConsulta & "       U.IDIOMA,U.TIPOEMAIL, P.EMAIL, U.DATEFMT, U.THOUSANFMT, U.DECIMALFMT, U.PRECISIONFMT, 0 AS ESPROVEEDOR,P.COD CODDESTINATARIO, NA.ID AS NOTIFICADO, U.COD" & vbCrLf
    sConsulta = sConsulta & "   FROM PER P WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_PARTICIPANTES PA WITH(NOLOCK) ON P.COD=PA.PER" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL R WITH(NOLOCK) ON PA.ROL=R.ROL_ORIGEN AND PA.PER=R.PER" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_BLOQUE B WITH(NOLOCK) ON R.INSTANCIA=B.INSTANCIA" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL_ACCION RA WITH(NOLOCK) ON R.ID=RA.ROL AND B.ID=RA.BLOQUE" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ACCIONES A WITH(NOLOCK) ON RA.ACCION=A.ID AND B.ID=A.BLOQUE" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_NOTIFICADO_ACCION NA WITH(NOLOCK) ON A.ACCION_ORIGEN=NA.ACCION" & vbCrLf
    sConsulta = sConsulta & "   LEFT JOIN USU U WITH(NOLOCK) ON P.COD=U.PER AND U.BAJA=0" & vbCrLf
    sConsulta = sConsulta & "   LEFT OUTER JOIN PM_ACCIONES_EMAIL AE WITH(NOLOCK) ON NA.ACCION=AE.ACCION AND AE.IDI=U.IDIOMA" & vbCrLf
    sConsulta = sConsulta & "   WHERE R.TIPO<>4 AND R.TIPO<>2 AND NA.TIPO_NOTIFICADO=5 AND RA.ACCION=@ACCION AND R.INSTANCIA=@ID" & vbCrLf
    sConsulta = sConsulta & "   UNION" & vbCrLf
    sConsulta = sConsulta & "   --No Peticionario       DeCampoDePantalla" & vbCrLf
    sConsulta = sConsulta & "   SELECT DISTINCT ISNULL(AE.SUBJECT,'') AS TEXT," & vbCrLf
    sConsulta = sConsulta & "       U.IDIOMA,U.TIPOEMAIL, P.EMAIL, U.DATEFMT, U.THOUSANFMT, U.DECIMALFMT, U.PRECISIONFMT, 0 AS ESPROVEEDOR,P.COD CODDESTINATARIO, NA.ID AS NOTIFICADO, U.COD" & vbCrLf
    sConsulta = sConsulta & "   FROM PER P WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL R WITH(NOLOCK) ON P.COD=R.PER" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_BLOQUE B WITH(NOLOCK) ON R.INSTANCIA=B.INSTANCIA" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL_ACCION RA WITH(NOLOCK) ON R.ID=RA.ROL AND B.ID=RA.BLOQUE" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ACCIONES A WITH(NOLOCK) ON RA.ACCION=A.ID AND B.ID=A.BLOQUE" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_NOTIFICADO_ACCION NA WITH(NOLOCK) ON A.ACCION_ORIGEN=NA.ACCION" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN COPIA_CAMPO CC WITH(NOLOCK) ON R.CAMPO=CC.COPIA_CAMPO_DEF" & vbCrLf
    sConsulta = sConsulta & "       AND CC.NUM_VERSION=(SELECT MAX(NUM_VERSION) FROM COPIA_CAMPO WITH(NOLOCK) WHERE B.INSTANCIA=INSTANCIA)" & vbCrLf
    sConsulta = sConsulta & "   LEFT JOIN USU U WITH(NOLOCK) ON P.COD=U.PER AND U.BAJA=0" & vbCrLf
    sConsulta = sConsulta & "   LEFT OUTER JOIN PM_ACCIONES_EMAIL AE WITH(NOLOCK) ON NA.ACCION=AE.ACCION AND AE.IDI=U.IDIOMA" & vbCrLf
    sConsulta = sConsulta & "   WHERE R.TIPO<>4 AND R.TIPO<>2 AND R.COMO_ASIGNAR=1 AND NA.TIPO_NOTIFICADO=5 AND RA.ACCION=@ACCION AND R.INSTANCIA=@ID" & vbCrLf
    sConsulta = sConsulta & "   UNION" & vbCrLf
    sConsulta = sConsulta & "   --Peticionario / No Peticionario        Destinatario" & vbCrLf
    sConsulta = sConsulta & "   SELECT DISTINCT ISNULL(AE.SUBJECT,'') AS TEXT," & vbCrLf
    sConsulta = sConsulta & "       U.IDIOMA,U.TIPOEMAIL, P.EMAIL, U.DATEFMT, U.THOUSANFMT, U.DECIMALFMT, U.PRECISIONFMT, 0 AS ESPROVEEDOR,P.COD CODDESTINATARIO, NA.ID AS NOTIFICADO, U.COD" & vbCrLf
    sConsulta = sConsulta & "   FROM PER P WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN INSTANCIA_EST IE2 WITH(NOLOCK) ON P.COD=IE2.DESTINATARIO" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN INSTANCIA_CAMINO IC WITH(NOLOCK) ON IE2.BLOQUE=IC.INSTANCIA_BLOQUE_ORIGEN" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN INSTANCIA_EST IE WITH(NOLOCK) ON IC.INSTANCIA_EST=IE.ID" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ACCIONES A WITH(NOLOCK) ON IE.PM_ACCION = A.ID" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_NOTIFICADO_ACCION NA WITH(NOLOCK) ON A.ACCION_ORIGEN=NA.ACCION" & vbCrLf
    sConsulta = sConsulta & "   LEFT JOIN USU U WITH(NOLOCK) ON P.COD=U.PER AND U.BAJA=0" & vbCrLf
    sConsulta = sConsulta & "   LEFT OUTER JOIN PM_ACCIONES_EMAIL AE WITH(NOLOCK) ON NA.ACCION=AE.ACCION AND AE.IDI=U.IDIOMA" & vbCrLf
    sConsulta = sConsulta & "   WHERE IE2.ACCION=3 AND NA.TIPO_NOTIFICADO=5 AND IE.PM_ACCION=@ACCION AND IE.INSTANCIA=@ID" & vbCrLf
    sConsulta = sConsulta & "--Notificados: Proveedores que participaron en la etapa Origen" & vbCrLf
    sConsulta = sConsulta & "   UNION" & vbCrLf
    sConsulta = sConsulta & "   --No Peticionario       Prove" & vbCrLf
    sConsulta = sConsulta & "   SELECT DISTINCT ISNULL(AE.SUBJECT,'') AS TEXT," & vbCrLf
    sConsulta = sConsulta & "       ISNULL(C.IDI,(SELECT IDIOMA FROM PARGEN_DEF WITH(NOLOCK) WHERE PARGEN_DEF.ID=1)) IDIOMA," & vbCrLf
    sConsulta = sConsulta & "       C.TIPOEMAIL, C.EMAIL, C.DATEFMT, C.THOUSANFMT, C.DECIMALFMT, C.PRECISIONFMT, 1 AS ESPROVEEDOR,NULL CODDESTINATARIO, NA.ID AS NOTIFICADO, NULL CODUSUARIO" & vbCrLf
    sConsulta = sConsulta & "   FROM CON C WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL R WITH(NOLOCK) ON C.PROVE=R.PROVE" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL_CON RC WITH(NOLOCK) ON R.ID=RC.ID_ROL AND C.ID=RC.ID_CON" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ACCIONES CA WITH(NOLOCK) ON CA.ID=@ACCION" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_NOTIFICADO_ACCION NA WITH(NOLOCK) ON CA.ACCION_ORIGEN=NA.ACCION" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_BLOQUE CB WITH(NOLOCK) ON CA.BLOQUE=CB.ID AND R.INSTANCIA=CB.INSTANCIA" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL_ACCION RA WITH(NOLOCK) ON R.ID=RA.ROL AND CB.ID=RA.BLOQUE AND CA.ID=RA.ACCION" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_ACCIONES_EMAIL AE WITH(NOLOCK) ON NA.ACCION=AE.ACCION AND AE.IDI=ISNULL(C.IDI,(SELECT IDIOMA FROM PARGEN_DEF WITH(NOLOCK) WHERE PARGEN_DEF.ID=1))" & vbCrLf
    sConsulta = sConsulta & "   WHERE NA.TIPO_NOTIFICADO=5 AND R.TIPO=2 AND R.INSTANCIA=@ID" & vbCrLf
    sConsulta = sConsulta & "    UNION" & vbCrLf
    sConsulta = sConsulta & "    SELECT DISTINCT ISNULL(AE.SUBJECT,'') AS TEXT," & vbCrLf
    sConsulta = sConsulta & "       ISNULL(C.IDI,(SELECT IDIOMA FROM PARGEN_DEF WITH(NOLOCK) WHERE PARGEN_DEF.ID=1)) IDIOMA," & vbCrLf
    sConsulta = sConsulta & "       C.TIPOEMAIL, C.EMAIL, C.DATEFMT, C.THOUSANFMT, C.DECIMALFMT, C.PRECISIONFMT, 1 AS ESPROVEEDOR,NULL CODDESTINATARIO, NA.ID AS NOTIFICADO, NULL CODUSUARIO" & vbCrLf
    sConsulta = sConsulta & "   FROM CON C WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL R WITH(NOLOCK) ON C.PROVE=R.PROVE" & vbCrLf
    sConsulta = sConsulta & "   AND NOT EXISTS (SELECT ID_CON FROM PM_COPIA_ROL_CON RC WITH(NOLOCK) WHERE RC.ID_ROL = R.ID)" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ACCIONES CA WITH(NOLOCK) ON CA.ID=@ACCION" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_NOTIFICADO_ACCION NA WITH(NOLOCK) ON CA.ACCION_ORIGEN=NA.ACCION" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_BLOQUE CB WITH(NOLOCK) ON CA.BLOQUE=CB.ID AND R.INSTANCIA=CB.INSTANCIA" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL_ACCION RA WITH(NOLOCK) ON R.ID=RA.ROL AND CB.ID=RA.BLOQUE AND CA.ID=RA.ACCION" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_ACCIONES_EMAIL AE WITH(NOLOCK) ON NA.ACCION=AE.ACCION AND AE.IDI=ISNULL(C.IDI,(SELECT IDIOMA FROM PARGEN_DEF WITH(NOLOCK) WHERE PARGEN_DEF.ID=1))" & vbCrLf
    sConsulta = sConsulta & "   WHERE NA.TIPO_NOTIFICADO=5 AND R.TIPO=2 AND R.INSTANCIA=@ID" & vbCrLf
    sConsulta = sConsulta & "    UNION" & vbCrLf
    sConsulta = sConsulta & "   --No Peticionario       Destinatario Prove" & vbCrLf
    sConsulta = sConsulta & "   SELECT DISTINCT ISNULL(AE.SUBJECT,'') AS TEXT," & vbCrLf
    sConsulta = sConsulta & "       ISNULL(C.IDI,(SELECT IDIOMA FROM PARGEN_DEF WITH(NOLOCK) WHERE PARGEN_DEF.ID=1)) IDIOMA," & vbCrLf
    sConsulta = sConsulta & "       C.TIPOEMAIL, C.EMAIL, C.DATEFMT, C.THOUSANFMT, C.DECIMALFMT, C.PRECISIONFMT, 1 AS ESPROVEEDOR,NULL CODDESTINATARIO, NA.ID AS NOTIFICADO, NULL CODUSUARIO" & vbCrLf
    sConsulta = sConsulta & "   FROM CON C WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN INSTANCIA_EST IE2 WITH(NOLOCK) ON IE2.CONTACTO=C.ID AND IE2.DESTINATARIO_PROV=C.PROVE" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN INSTANCIA_CAMINO IC WITH(NOLOCK) ON IE2.BLOQUE=IC.INSTANCIA_BLOQUE_ORIGEN" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN INSTANCIA_EST IE WITH(NOLOCK) ON IC.INSTANCIA_EST=IE.ID" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ACCIONES A WITH(NOLOCK) ON IE.PM_ACCION=A.ID" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_NOTIFICADO_ACCION NA WITH(NOLOCK) ON A.ACCION_ORIGEN=NA.ACCION" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_ACCIONES_EMAIL AE WITH(NOLOCK) ON NA.ACCION=AE.ACCION AND AE.IDI=ISNULL(C.IDI,(SELECT IDIOMA FROM PARGEN_DEF WITH(NOLOCK) WHERE PARGEN_DEF.ID=1))" & vbCrLf
    sConsulta = sConsulta & "   WHERE NA.TIPO_NOTIFICADO=5 AND IE2.ACCION=3 AND NA.ACCION = @ACCION AND IE2.INSTANCIA=@ID" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "--Notificados: Responsables de Procesos de Compra asociados a la solicitud" & vbCrLf
    sConsulta = sConsulta & "   UNION" & vbCrLf
    sConsulta = sConsulta & "   SELECT DISTINCT ISNULL(AE.SUBJECT,'') AS TEXT," & vbCrLf
    sConsulta = sConsulta & "       U.IDIOMA,U.TIPOEMAIL, P.EMAIL, U.DATEFMT, U.THOUSANFMT, U.DECIMALFMT, U.PRECISIONFMT, 0 AS ESPROVEEDOR,P.COD CODDESTINATARIO, NA.ID AS NOTIFICADO, U.COD" & vbCrLf
    sConsulta = sConsulta & "   FROM PER P WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   LEFT JOIN USU U WITH(NOLOCK) ON P.COD=U.PER AND U.BAJA=0" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PROCE PR WITH(NOLOCK) ON P.COD=PR.COM" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN ITEM S WITH(NOLOCK) ON PR.ANYO =S.ANYO AND PR.GMN1 =S.GMN1_PROCE  AND PR.COD =S.PROCE " & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN INSTANCIA I WITH(NOLOCK) ON I.ID=S.SOLICIT" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN INSTANCIA_EST IE WITH(NOLOCK) ON I.ID=IE.INSTANCIA" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ACCIONES A WITH(NOLOCK) ON IE.PM_ACCION=A.ID" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_NOTIFICADO_ACCION NA WITH(NOLOCK) ON A.ACCION_ORIGEN=NA.ACCION" & vbCrLf
    sConsulta = sConsulta & "   LEFT OUTER JOIN PM_ACCIONES_EMAIL AE WITH(NOLOCK) ON NA.ACCION=AE.ACCION AND AE.IDI=U.IDIOMA" & vbCrLf
    sConsulta = sConsulta & "   WHERE NA.TIPO_NOTIFICADO=6 AND IE.PM_ACCION=@ACCION AND I.ID=@ID" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "--Notificado: Gestor de la Solicitud" & vbCrLf
    sConsulta = sConsulta & "   UNION" & vbCrLf
    sConsulta = sConsulta & "   SELECT DISTINCT ISNULL(AE.SUBJECT,'') AS TEXT," & vbCrLf
    sConsulta = sConsulta & "       U.IDIOMA,U.TIPOEMAIL, P.EMAIL, U.DATEFMT, U.THOUSANFMT, U.DECIMALFMT, U.PRECISIONFMT, 0 AS ESPROVEEDOR,P.COD CODDESTINATARIO, NA.ID AS NOTIFICADO, U.COD" & vbCrLf
    sConsulta = sConsulta & "   FROM PER P WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN SOLICITUD S WITH(NOLOCK) ON P.COD=S.GESTOR" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN INSTANCIA I WITH(NOLOCK) ON S.ID=I.SOLICITUD" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN INSTANCIA_EST IE WITH(NOLOCK) ON I.ID=IE.INSTANCIA" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ACCIONES A WITH(NOLOCK) ON IE.PM_ACCION=A.ID" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_NOTIFICADO_ACCION NA WITH(NOLOCK) ON A.ACCION_ORIGEN=NA.ACCION" & vbCrLf
    sConsulta = sConsulta & "   LEFT JOIN USU U WITH(NOLOCK) ON P.COD=U.PER AND U.BAJA=0" & vbCrLf
    sConsulta = sConsulta & "   LEFT OUTER JOIN PM_ACCIONES_EMAIL AE WITH(NOLOCK) ON NA.ACCION=AE.ACCION AND AE.IDI=U.IDIOMA" & vbCrLf
    sConsulta = sConsulta & "   WHERE NA.TIPO_NOTIFICADO=7 AND IE.PM_ACCION=@ACCION AND I.ID=@ID" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   SELECT TEXT, IDIOMA, TIPOEMAIL, EMAIL, DATEFMT, THOUSANFMT, DECIMALFMT, PRECISIONFMT, ESPROVEEDOR, CODDESTINATARIO, MIN(NOTIFICADO) AS NOTIFICADO, CODUSUARIO,IDIOMAS.LANGUAGETAG " & vbCrLf
    sConsulta = sConsulta & "   FROM #FSPM_DATOS_MAIL_ACCION FS" & vbCrLf
    sConsulta = sConsulta & "   LEFT JOIN IDIOMAS WITH (NOLOCK) ON FS.IDIOMA =IDIOMAS.COD " & vbCrLf
    sConsulta = sConsulta & "   GROUP BY TEXT, IDIOMA, TIPOEMAIL, EMAIL, DATEFMT, THOUSANFMT, DECIMALFMT, PRECISIONFMT, ESPROVEEDOR, CODDESTINATARIO, CODUSUARIO,IDIOMAS.LANGUAGETAG " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   DROP TABLE #FSPM_DATOS_MAIL_ACCION" & vbCrLf
    sConsulta = sConsulta & "  END" & vbCrLf
    sConsulta = sConsulta & "ELSE" & vbCrLf
    sConsulta = sConsulta & "  IF @TYPE = 3 --NOTIFICADOS DE ENLACE" & vbCrLf
    sConsulta = sConsulta & "  BEGIN" & vbCrLf
    sConsulta = sConsulta & "   --Esta stored no saca nada si no hay enlace. El rechazo definitivo pone enlace de instancia_camino a NULL" & vbCrLf
    sConsulta = sConsulta & "   IF @ENLACE=0" & vbCrLf
    sConsulta = sConsulta & "       SELECT @ENLACE=CE.ID FROM PM_COPIA_ENLACE CE WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "       INNER JOIN PM_COPIA_ACCIONES CA WITH(NOLOCK) ON CE.ACCION=CA.ID AND CE.BLOQUE_ORIGEN=CA.BLOQUE AND CA.TIPO_RECHAZO=2" & vbCrLf
    sConsulta = sConsulta & "       WHERE CA.ID=@ACCION" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   CREATE TABLE #FSPM_DATOS_MAIL_ENLACE (" & vbCrLf
    sConsulta = sConsulta & "       TEXT varchar(200)," & vbCrLf
    sConsulta = sConsulta & "       IDIOMA varchar(3) COLLATE SQL_Latin1_General_CP1_CI_AS," & vbCrLf
    sConsulta = sConsulta & "       TIPOEMAIL tinyint," & vbCrLf
    sConsulta = sConsulta & "       EMAIL varchar(100)," & vbCrLf
    sConsulta = sConsulta & "       DATEFMT varchar(50)," & vbCrLf
    sConsulta = sConsulta & "       THOUSANFMT char(1)," & vbCrLf
    sConsulta = sConsulta & "       DECIMALFMT char(1)," & vbCrLf
    sConsulta = sConsulta & "       PRECISIONFMT tinyint," & vbCrLf
    sConsulta = sConsulta & "       ESPROVEEDOR int," & vbCrLf
    sConsulta = sConsulta & "       CODDESTINATARIO varchar(20)," & vbCrLf
    sConsulta = sConsulta & "       NOTIFICADO int," & vbCrLf
    sConsulta = sConsulta & "       CODUSUARIO varchar(50)" & vbCrLf
    sConsulta = sConsulta & "   )" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   INSERT INTO #FSPM_DATOS_MAIL_ENLACE (TEXT, IDIOMA, TIPOEMAIL, EMAIL, DATEFMT, THOUSANFMT, DECIMALFMT, PRECISIONFMT, ESPROVEEDOR, CODDESTINATARIO, NOTIFICADO, CODUSUARIO)" & vbCrLf
    sConsulta = sConsulta & "   --Notificados: Especificos" & vbCrLf
    sConsulta = sConsulta & "   SELECT DISTINCT ISNULL(EE.SUBJECT,'') AS TEXT," & vbCrLf
    sConsulta = sConsulta & "       U.IDIOMA," & vbCrLf
    sConsulta = sConsulta & "       U.TIPOEMAIL, P.EMAIL, U.DATEFMT, U.THOUSANFMT, U.DECIMALFMT, U.PRECISIONFMT, 0 AS ESPROVEEDOR,P.COD CODDESTINATARIO, NE.ID AS NOTIFICADO, U.COD CODUSUARIO" & vbCrLf
    sConsulta = sConsulta & "   FROM PER P WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_NOTIFICADO_ENLACE NE WITH(NOLOCK) ON P.COD=NE.PER" & vbCrLf
    sConsulta = sConsulta & "   LEFT JOIN USU U WITH(NOLOCK) ON P.COD=U.PER AND U.BAJA=0" & vbCrLf
    sConsulta = sConsulta & "   LEFT OUTER JOIN PM_ENLACE_EMAIL EE WITH(NOLOCK) ON NE.ENLACE=EE.ENLACE AND EE.IDI=U.IDIOMA" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ENLACE CE WITH(NOLOCK) ON CE.ENLACE_ORIGEN=EE.ENLACE" & vbCrLf
    sConsulta = sConsulta & "   WHERE NE.TIPO_NOTIFICADO=1 AND CE.ID=@ENLACE" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   --Notificados: Roles(Proveedor)" & vbCrLf
    sConsulta = sConsulta & "   UNION" & vbCrLf
    sConsulta = sConsulta & "   SELECT DISTINCT ISNULL(EE.SUBJECT,'') AS TEXT," & vbCrLf
    sConsulta = sConsulta & "       ISNULL(C.IDI,(SELECT IDIOMA FROM PARGEN_DEF WITH(NOLOCK) WHERE PARGEN_DEF.ID=1)) IDIOMA," & vbCrLf
    sConsulta = sConsulta & "       C.TIPOEMAIL, C.EMAIL, C.DATEFMT, C.THOUSANFMT, C.DECIMALFMT, C.PRECISIONFMT, 1 AS ESPROVEEDOR,NULL CODDESTINATARIO, NE.ID AS NOTIFICADO, NULL CODUSUARIO" & vbCrLf
    sConsulta = sConsulta & "   FROM CON C WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL R WITH(NOLOCK) ON C.PROVE=R.PROVE" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL_CON RC WITH(NOLOCK) ON R.ID=RC.ID_ROL AND C.ID=RC.ID_CON" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_ROL ROL WITH(NOLOCK) ON R.ROL_ORIGEN=ROL.ID" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_NOTIFICADO_ENLACE NE WITH(NOLOCK) ON ROL.ID=NE.ROL" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_ENLACE_EMAIL EE WITH(NOLOCK) ON EE.IDI=ISNULL(C.IDI,(SELECT IDIOMA FROM PARGEN_DEF WITH(NOLOCK) WHERE PARGEN_DEF.ID=1)) AND EE.ENLACE=NE.ENLACE" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ENLACE CE WITH(NOLOCK) ON CE.ENLACE_ORIGEN=EE.ENLACE" & vbCrLf
    sConsulta = sConsulta & "   WHERE R.TIPO=2 AND NE.TIPO_NOTIFICADO=2 AND CE.ID=@ENLACE AND R.INSTANCIA=@ID" & vbCrLf
    sConsulta = sConsulta & "   UNION" & vbCrLf
    sConsulta = sConsulta & "       SELECT DISTINCT ISNULL(EE.SUBJECT,'') AS TEXT," & vbCrLf
    sConsulta = sConsulta & "       ISNULL(C.IDI,(SELECT IDIOMA FROM PARGEN_DEF WITH(NOLOCK) WHERE PARGEN_DEF.ID=1)) IDIOMA," & vbCrLf
    sConsulta = sConsulta & "       C.TIPOEMAIL, C.EMAIL, C.DATEFMT, C.THOUSANFMT, C.DECIMALFMT, C.PRECISIONFMT, 1 AS ESPROVEEDOR,NULL CODDESTINATARIO, NE.ID AS NOTIFICADO, NULL CODUSUARIO" & vbCrLf
    sConsulta = sConsulta & "   FROM CON C WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL R WITH(NOLOCK) ON C.PROVE=R.PROVE" & vbCrLf
    sConsulta = sConsulta & "   AND NOT EXISTS (SELECT ID_CON FROM PM_COPIA_ROL_CON RC WITH(NOLOCK) WHERE RC.ID_ROL = R.ID)" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_ROL ROL WITH(NOLOCK) ON R.ROL_ORIGEN=ROL.ID" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_NOTIFICADO_ENLACE NE WITH(NOLOCK) ON ROL.ID=NE.ROL" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_ENLACE_EMAIL EE WITH(NOLOCK) ON EE.IDI=ISNULL(C.IDI,(SELECT IDIOMA FROM PARGEN_DEF WITH(NOLOCK) WHERE PARGEN_DEF.ID=1)) AND EE.ENLACE=NE.ENLACE" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ENLACE CE WITH(NOLOCK) ON CE.ENLACE_ORIGEN=EE.ENLACE" & vbCrLf
    sConsulta = sConsulta & "   WHERE R.TIPO=2 AND NE.TIPO_NOTIFICADO=2 AND CE.ID=@ENLACE AND R.INSTANCIA=@ID" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   --Notificados: Roles(Comprador, Usuario, Peticionario)" & vbCrLf
    sConsulta = sConsulta & "   UNION" & vbCrLf
    sConsulta = sConsulta & "   SELECT DISTINCT ISNULL(EE.SUBJECT,'') AS TEXT," & vbCrLf
    sConsulta = sConsulta & "       U.IDIOMA,U.TIPOEMAIL, P.EMAIL, U.DATEFMT, U.THOUSANFMT, U.DECIMALFMT, U.PRECISIONFMT, 0 AS ESPROVEEDOR,P.COD CODDESTINATARIO, NE.ID AS NOTIFICADO, U.COD CODUSUARIO" & vbCrLf
    sConsulta = sConsulta & "   FROM PER P WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   LEFT JOIN USU U WITH(NOLOCK) ON P.COD=U.PER AND U.BAJA=0" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL R WITH(NOLOCK) ON P.COD=R.PER" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_ROL ROL WITH(NOLOCK) ON R.ROL_ORIGEN=ROL.ID" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_NOTIFICADO_ENLACE NE WITH(NOLOCK) ON ROL.ID=NE.ROL" & vbCrLf
    sConsulta = sConsulta & "   LEFT OUTER JOIN PM_ENLACE_EMAIL EE WITH(NOLOCK) ON NE.ENLACE=EE.ENLACE AND EE.IDI=U.IDIOMA" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ENLACE CE WITH(NOLOCK) ON EE.ENLACE=CE.ENLACE_ORIGEN" & vbCrLf
    sConsulta = sConsulta & "   WHERE R.TIPO <> 2 AND NE.TIPO_NOTIFICADO=2 AND CE.ID=@ENLACE AND R.INSTANCIA=@ID" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   --Notificados: Roles(Lista de AutoAsignaci�n)" & vbCrLf
    sConsulta = sConsulta & "   UNION" & vbCrLf
    sConsulta = sConsulta & "   SELECT DISTINCT ISNULL(EE.SUBJECT,'') AS TEXT," & vbCrLf
    sConsulta = sConsulta & "       U.IDIOMA,U.TIPOEMAIL, P.EMAIL, U.DATEFMT, U.THOUSANFMT, U.DECIMALFMT, U.PRECISIONFMT, 0 AS ESPROVEEDOR,P.COD CODDESTINATARIO, NE.ID AS NOTIFICADO, U.COD CODUSUARIO" & vbCrLf
    sConsulta = sConsulta & "   FROM PM_COPIA_ROL R WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_ROL ROL WITH(NOLOCK) ON R.ROL_ORIGEN=ROL.ID" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_NOTIFICADO_ENLACE NE WITH(NOLOCK) ON ROL.ID=NE.ROL" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_PARTICIPANTES PCP WITH(NOLOCK) ON R.ID=PCP.ROL" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PER P WITH(NOLOCK) ON PCP.PER=P.COD" & vbCrLf
    sConsulta = sConsulta & "   LEFT JOIN USU U WITH(NOLOCK) ON P.COD=U.PER AND U.BAJA=0" & vbCrLf
    sConsulta = sConsulta & "   LEFT OUTER JOIN PM_ENLACE_EMAIL EE WITH(NOLOCK) ON NE.ENLACE=EE.ENLACE AND EE.IDI=U.IDIOMA" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ENLACE CE WITH(NOLOCK) ON EE.ENLACE=CE.ENLACE_ORIGEN" & vbCrLf
    sConsulta = sConsulta & "   WHERE R.TIPO = 3 AND NE.TIPO_NOTIFICADO=2 AND R.COMO_ASIGNAR=3 AND CE.ID=@ENLACE AND R.INSTANCIA=@ID" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   --Notificados: Roles(GESTOR DE LA FACTURA)" & vbCrLf
    sConsulta = sConsulta & "   UNION" & vbCrLf
    sConsulta = sConsulta & "   SELECT DISTINCT ISNULL(EE.SUBJECT,'') AS TEXT," & vbCrLf
    sConsulta = sConsulta & "   U.IDIOMA,U.TIPOEMAIL, P.EMAIL, U.DATEFMT, U.THOUSANFMT, U.DECIMALFMT, U.PRECISIONFMT, 0 AS ESPROVEEDOR,P.COD CODDESTINATARIO, NE.ID AS NOTIFICADO, U.COD" & vbCrLf
    sConsulta = sConsulta & "   FROM PER P WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   LEFT JOIN USU U WITH(NOLOCK) ON P.COD=U.PER AND U.BAJA = 0" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PRES5_IMPORTES PI WITH(NOLOCK) ON PI.GESTOR = P.COD" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN LINEAS_PED_IMPUTACION LPI WITH(NOLOCK) ON LPI.PRES5_IMP = PI.ID" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN LINEAS_PEDIDO_FACTURA LPF WITH(NOLOCK) ON LPF.LINEA_PEDIDO = LPI.LINEA" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN LIN_FACTURA lf with(nolock) on lf.FACTURA = lpf.FACTURA" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN FACTURA F WITH(NOLOCK) ON F.ID = lPF.FACTURA" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL r with(nolock) on r.INSTANCIA = f.INSTANCIA" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_ROL ROL WITH(NOLOCK) ON R.ROL_ORIGEN=ROL.ID" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_NOTIFICADO_ENLACE NE WITH(NOLOCK) ON ROL.ID=NE.ROL" & vbCrLf
    sConsulta = sConsulta & "   LEFT OUTER JOIN PM_ENLACE_EMAIL EE WITH(NOLOCK) ON NE.ENLACE=EE.ENLACE AND EE.IDI=U.IDIOMA" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ENLACE CE WITH(NOLOCK) ON NE.ENLACE=CE.ENLACE_ORIGEN" & vbCrLf
    sConsulta = sConsulta & "   WHERE F.INSTANCIA = @ID and CE.ID=@ENLACE and R.TIPO = 6 and R.GESTOR_FACTURA = 1 AND NE.TIPO_NOTIFICADO=2" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   --Notificado: Peticionario" & vbCrLf
    sConsulta = sConsulta & "   UNION" & vbCrLf
    sConsulta = sConsulta & "   SELECT DISTINCT ISNULL(EE.SUBJECT,'') AS TEXT," & vbCrLf
    sConsulta = sConsulta & "       U.IDIOMA,U.TIPOEMAIL, P.EMAIL, U.DATEFMT, U.THOUSANFMT, U.DECIMALFMT, U.PRECISIONFMT, 0 AS ESPROVEEDOR,P.COD CODDESTINATARIO, NE.ID AS NOTIFICADO, U.COD CODUSUARIO" & vbCrLf
    sConsulta = sConsulta & "   FROM PER P WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   LEFT JOIN USU U WITH(NOLOCK) ON P.COD=U.PER AND U.BAJA=0" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN INSTANCIA I WITH(NOLOCK) ON P.COD=I.PETICIONARIO" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_BLOQUE B WITH(NOLOCK) ON I.ID=B.INSTANCIA" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ENLACE E WITH(NOLOCK) ON B.ID=E.BLOQUE_ORIGEN" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_NOTIFICADO_ENLACE NE WITH(NOLOCK) ON E.ENLACE_ORIGEN=NE.ENLACE" & vbCrLf
    sConsulta = sConsulta & "   LEFT OUTER JOIN PM_ENLACE_EMAIL EE WITH(NOLOCK) ON NE.ENLACE=EE.ENLACE AND EE.IDI=U.IDIOMA" & vbCrLf
    sConsulta = sConsulta & "   WHERE NE.TIPO_NOTIFICADO=3 AND E.ID=@ENLACE AND I.ID=@ID" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   --Notificado: Autor de la accion (Rol Proveedor)" & vbCrLf
    sConsulta = sConsulta & "   UNION" & vbCrLf
    sConsulta = sConsulta & "   SELECT DISTINCT ISNULL(EE.SUBJECT,'') AS TEXT," & vbCrLf
    sConsulta = sConsulta & "       ISNULL(C.IDI,(SELECT IDIOMA FROM PARGEN_DEF WITH(NOLOCK) WHERE PARGEN_DEF.ID=1)) IDIOMA," & vbCrLf
    sConsulta = sConsulta & "       C.TIPOEMAIL, C.EMAIL, C.DATEFMT, C.THOUSANFMT, C.DECIMALFMT, C.PRECISIONFMT, 1 AS ESPROVEEDOR,NULL CODDESTINATARIO, NE.ID AS NOTIFICADO, NULL CODUSUARIO" & vbCrLf
    sConsulta = sConsulta & "   FROM CON C WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL R WITH(NOLOCK) ON C.PROVE=R.PROVE" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL_CON RC WITH(NOLOCK) ON R.ID=RC.ID_ROL AND C.ID=RC.ID_CON" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_ROL ROL WITH(NOLOCK) ON R.ROL_ORIGEN=ROL.ID" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN INSTANCIA_EST IE WITH(NOLOCK) ON R.ID=IE.ROL" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ENLACE E WITH(NOLOCK) ON IE.PM_ACCION=E.ACCION" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_NOTIFICADO_ENLACE NE WITH(NOLOCK) ON E.ENLACE_ORIGEN=NE.ENLACE" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_ENLACE_EMAIL EE WITH(NOLOCK) ON NE.ENLACE=EE.ENLACE AND EE.IDI=ISNULL(C.IDI,(SELECT IDIOMA FROM PARGEN_DEF WITH(NOLOCK) WHERE PARGEN_DEF.ID=1))" & vbCrLf
    sConsulta = sConsulta & "   WHERE R.TIPO =2 AND NE.TIPO_NOTIFICADO=4 AND IE.PM_ACCION=@ACCION AND E.ID=@ENLACE AND R.INSTANCIA=@ID" & vbCrLf
    sConsulta = sConsulta & "   UNION" & vbCrLf
    sConsulta = sConsulta & "   SELECT DISTINCT ISNULL(EE.SUBJECT,'') AS TEXT," & vbCrLf
    sConsulta = sConsulta & "       ISNULL(C.IDI,(SELECT IDIOMA FROM PARGEN_DEF WITH(NOLOCK) WHERE PARGEN_DEF.ID=1)) IDIOMA," & vbCrLf
    sConsulta = sConsulta & "       C.TIPOEMAIL, C.EMAIL, C.DATEFMT, C.THOUSANFMT, C.DECIMALFMT, C.PRECISIONFMT, 1 AS ESPROVEEDOR,NULL CODDESTINATARIO, NE.ID AS NOTIFICADO, NULL CODUSUARIO" & vbCrLf
    sConsulta = sConsulta & "   FROM CON C WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL R WITH(NOLOCK) ON C.PROVE=R.PROVE" & vbCrLf
    sConsulta = sConsulta & "   AND NOT EXISTS (SELECT ID_CON FROM PM_COPIA_ROL_CON RC WITH(NOLOCK) WHERE RC.ID_ROL = R.ID)" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_ROL ROL WITH(NOLOCK) ON R.ROL_ORIGEN=ROL.ID" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN INSTANCIA_EST IE WITH(NOLOCK) ON R.ID=IE.ROL" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ENLACE E WITH(NOLOCK) ON IE.PM_ACCION=E.ACCION" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_NOTIFICADO_ENLACE NE WITH(NOLOCK) ON E.ENLACE_ORIGEN=NE.ENLACE" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_ENLACE_EMAIL EE WITH(NOLOCK) ON NE.ENLACE=EE.ENLACE AND EE.IDI=ISNULL(C.IDI,(SELECT IDIOMA FROM PARGEN_DEF WITH(NOLOCK) WHERE PARGEN_DEF.ID=1))" & vbCrLf
    sConsulta = sConsulta & "   WHERE R.TIPO =2 AND NE.TIPO_NOTIFICADO=4 AND IE.PM_ACCION=@ACCION AND E.ID=@ENLACE AND R.INSTANCIA=@ID" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   --Notificado: Autor de la accion (Rol Comprador, Usuario, Peticionario)" & vbCrLf
    sConsulta = sConsulta & "   UNION" & vbCrLf
    sConsulta = sConsulta & "   SELECT DISTINCT ISNULL(EE.SUBJECT,'') AS TEXT," & vbCrLf
    sConsulta = sConsulta & "       U.IDIOMA,U.TIPOEMAIL, P.EMAIL, U.DATEFMT, U.THOUSANFMT, U.DECIMALFMT, U.PRECISIONFMT, 0 AS ESPROVEEDOR,P.COD CODDESTINATARIO, NE.ID AS NOTIFICADO, U.COD CODUSUARIO" & vbCrLf
    sConsulta = sConsulta & "     FROM PER P WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "     INNER JOIN PM_COPIA_ROL R WITH(NOLOCK) ON P.COD=R.PER" & vbCrLf
    sConsulta = sConsulta & "     INNER JOIN INSTANCIA_EST IE WITH(NOLOCK) ON R.ID=IE.ROL" & vbCrLf
    sConsulta = sConsulta & "     INNER JOIN PM_COPIA_ENLACE E WITH(NOLOCK) ON IE.PM_ACCION=E.ACCION" & vbCrLf
    sConsulta = sConsulta & "     INNER JOIN PM_NOTIFICADO_ENLACE NE WITH(NOLOCK) ON E.ENLACE_ORIGEN=NE.ENLACE" & vbCrLf
    sConsulta = sConsulta & "     LEFT JOIN USU U WITH(NOLOCK) ON P.COD=U.PER AND U.BAJA=0" & vbCrLf
    sConsulta = sConsulta & "     LEFT OUTER JOIN PM_ENLACE_EMAIL EE WITH(NOLOCK) ON NE.ENLACE=EE.ENLACE AND EE.IDI=U.IDIOMA" & vbCrLf
    sConsulta = sConsulta & "   WHERE R.TIPO<>2 AND NE.TIPO_NOTIFICADO=4 AND IE.PM_ACCION=@ACCION AND E.ID=@ENLACE AND R.INSTANCIA=@ID" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   --Notificados: Personas Peticionarias que participaron en la etapa Origen" & vbCrLf
    sConsulta = sConsulta & "   UNION" & vbCrLf
    sConsulta = sConsulta & "   --Peticionario  Usuario" & vbCrLf
    sConsulta = sConsulta & "   SELECT DISTINCT ISNULL(EE.SUBJECT,'') AS TEXT," & vbCrLf
    sConsulta = sConsulta & "       U.IDIOMA," & vbCrLf
    sConsulta = sConsulta & "       U.TIPOEMAIL, P.EMAIL, U.DATEFMT, U.THOUSANFMT, U.DECIMALFMT, U.PRECISIONFMT, 0 AS ESPROVEEDOR,P.COD CODDESTINATARIO, NE.ID AS NOTIFICADO, U.COD CODUSUARIO" & vbCrLf
    sConsulta = sConsulta & "    FROM PER P WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "    INNER JOIN PM_COPIA_ROL R WITH(NOLOCK) ON P.COD=R.PER" & vbCrLf
    sConsulta = sConsulta & "    INNER JOIN PM_COPIA_ROL_ACCION RA WITH(NOLOCK) ON R.ID=RA.ROL" & vbCrLf
    sConsulta = sConsulta & "    INNER JOIN PM_COPIA_ENLACE E WITH(NOLOCK) ON RA.ACCION=E.ACCION" & vbCrLf
    sConsulta = sConsulta & "    INNER JOIN PM_NOTIFICADO_ENLACE NE WITH(NOLOCK) ON E.ENLACE_ORIGEN=NE.ENLACE" & vbCrLf
    sConsulta = sConsulta & "    INNER JOIN PM_COPIA_BLOQUE B WITH(NOLOCK) ON E.BLOQUE_ORIGEN=B.ID AND R.INSTANCIA=B.INSTANCIA" & vbCrLf
    sConsulta = sConsulta & "    LEFT JOIN USU U WITH(NOLOCK) ON P.COD=U.PER AND U.BAJA=0" & vbCrLf
    sConsulta = sConsulta & "    LEFT OUTER JOIN PM_ENLACE_EMAIL EE WITH(NOLOCK) ON NE.ENLACE=EE.ENLACE AND EE.IDI=U.IDIOMA" & vbCrLf
    sConsulta = sConsulta & "   WHERE R.TIPO=4 AND R.PER IS NOT NULL AND NE.TIPO_NOTIFICADO=5 AND RA.ACCION=@ACCION AND E.ID=@ENLACE AND R.INSTANCIA=@ID" & vbCrLf
    sConsulta = sConsulta & "   UNION" & vbCrLf
    sConsulta = sConsulta & "   --Peticionario  Dep" & vbCrLf
    sConsulta = sConsulta & "   SELECT DISTINCT ISNULL(EE.SUBJECT,'') AS TEXT,U.IDIOMA," & vbCrLf
    sConsulta = sConsulta & "       U.TIPOEMAIL, P.EMAIL, U.DATEFMT, U.THOUSANFMT, U.DECIMALFMT, U.PRECISIONFMT, 0 AS ESPROVEEDOR,P.COD CODDESTINATARIO, NE.ID AS NOTIFICADO, U.COD CODUSUARIO" & vbCrLf
    sConsulta = sConsulta & "   FROM PER P WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PER P2 WITH(NOLOCK) ON P.COD=P2.COD" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN USU U2 WITH(NOLOCK) ON P2.COD=U2.PER AND U2.BAJA=0" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN DEP D WITH(NOLOCK) ON P.DEP=D.COD" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL R WITH(NOLOCK) ON D.COD=R.DEP" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_BLOQUE B WITH(NOLOCK) ON R.INSTANCIA=B.INSTANCIA" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL_ACCION RA WITH(NOLOCK) ON R.ID=RA.ROL" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ENLACE E WITH(NOLOCK) ON RA.ACCION=E.ACCION AND B.ID=E.BLOQUE_ORIGEN" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_NOTIFICADO_ENLACE NE WITH(NOLOCK) ON E.ENLACE_ORIGEN=NE.ENLACE" & vbCrLf
    sConsulta = sConsulta & "   LEFT JOIN USU U WITH(NOLOCK) ON P.COD=U.PER AND U.BAJA=0" & vbCrLf
    sConsulta = sConsulta & "   LEFT OUTER JOIN PM_ENLACE_EMAIL EE WITH(NOLOCK) ON NE.ENLACE=EE.ENLACE AND EE.IDI=U.IDIOMA" & vbCrLf
    sConsulta = sConsulta & "   WHERE P2.BAJALOG=0 AND P2.UON1 IS NULL AND P2.UON2 IS NULL AND P2.UON3 IS NULL AND U2.BAJA=0 AND R.TIPO=4" & vbCrLf
    sConsulta = sConsulta & "       AND R.PER IS NULL AND R.DEP IS NOT NULL AND NE.TIPO_NOTIFICADO=5" & vbCrLf
    sConsulta = sConsulta & "       AND R.INSTANCIA=@ID AND RA.ACCION=@ACCION AND E.ID=@ENLACE" & vbCrLf
    sConsulta = sConsulta & "    UNION" & vbCrLf
    sConsulta = sConsulta & "   --Peticionario  Uon" & vbCrLf
    sConsulta = sConsulta & "   SELECT DISTINCT ISNULL(EE.SUBJECT,'') AS TEXT,U.IDIOMA," & vbCrLf
    sConsulta = sConsulta & "       U.TIPOEMAIL, P.EMAIL, U.DATEFMT, U.THOUSANFMT, U.DECIMALFMT, U.PRECISIONFMT, 0 AS ESPROVEEDOR,P.COD CODDESTINATARIO, NE.ID AS NOTIFICADO, U.COD CODUSUARIO" & vbCrLf
    sConsulta = sConsulta & "   FROM PER P WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PER P2 WITH(NOLOCK) ON P.COD=P2.COD" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN USU U2 WITH(NOLOCK) ON P2.COD=U2.PER AND U2.BAJA=0" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN DEP D WITH(NOLOCK) ON P.DEP=D.COD" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL R WITH(NOLOCK) ON D.COD=R.DEP AND ISNULL(P2.UON1,'#')=ISNULL(R.UON1,'#') AND ISNULL(P2.UON2,'#')=ISNULL(R.UON2,'#') AND ISNULL(P2.UON3,'#')=ISNULL(R.UON3,'#')" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_BLOQUE B WITH(NOLOCK) ON R.INSTANCIA=B.INSTANCIA" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL_ACCION RA WITH(NOLOCK) ON R.ID=RA.ROL" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ENLACE E WITH(NOLOCK) ON RA.ACCION=E.ACCION AND B.ID=E.BLOQUE_ORIGEN" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_NOTIFICADO_ENLACE NE WITH(NOLOCK) ON E.ENLACE_ORIGEN=NE.ENLACE" & vbCrLf
    sConsulta = sConsulta & "   LEFT JOIN USU U WITH(NOLOCK) ON P.COD=U.PER AND U.BAJA=0" & vbCrLf
    sConsulta = sConsulta & "   LEFT OUTER JOIN PM_ENLACE_EMAIL EE WITH(NOLOCK) ON NE.ENLACE=EE.ENLACE AND EE.IDI=U.IDIOMA" & vbCrLf
    sConsulta = sConsulta & "   WHERE P2.BAJALOG=0 AND U2.BAJA=0 AND R.TIPO=4 AND R.PER IS NULL AND (R.UON1 IS NOT NULL OR R.UON2 IS NOT NULL OR R.UON3 IS NOT NULL)" & vbCrLf
    sConsulta = sConsulta & "       AND NE.TIPO_NOTIFICADO=5 AND R.INSTANCIA=@ID AND RA.ACCION=@ACCION AND E.ID=@ENLACE" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   --Notificados: Personas Destinatarias que participaron en la etapa Origen" & vbCrLf
    sConsulta = sConsulta & "   UNION" & vbCrLf
    sConsulta = sConsulta & "   SELECT DISTINCT ISNULL(EE.SUBJECT,'') AS TEXT," & vbCrLf
    sConsulta = sConsulta & "       U.IDIOMA,U.TIPOEMAIL, P.EMAIL, U.DATEFMT, U.THOUSANFMT, U.DECIMALFMT, U.PRECISIONFMT, 0 AS ESPROVEEDOR,P.COD CODDESTINATARIO, NE.ID AS NOTIFICADO, U.COD CODUSUARIO" & vbCrLf
    sConsulta = sConsulta & "       FROM PER P WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "       INNER JOIN INSTANCIA_EST IE2 WITH(NOLOCK) ON P.COD=IE2.DESTINATARIO" & vbCrLf
    sConsulta = sConsulta & "       INNER JOIN INSTANCIA_CAMINO IC WITH(NOLOCK) ON IE2.BLOQUE=IC.INSTANCIA_BLOQUE_ORIGEN" & vbCrLf
    sConsulta = sConsulta & "       INNER JOIN INSTANCIA_EST IE WITH(NOLOCK) ON IC.INSTANCIA_EST=IE.ID" & vbCrLf
    sConsulta = sConsulta & "       INNER JOIN PM_COPIA_ENLACE E WITH(NOLOCK) ON IE.PM_ACCION=E.ACCION" & vbCrLf
    sConsulta = sConsulta & "       INNER JOIN PM_NOTIFICADO_ENLACE NE WITH(NOLOCK) ON E.ENLACE_ORIGEN=NE.ENLACE" & vbCrLf
    sConsulta = sConsulta & "       LEFT JOIN USU U WITH(NOLOCK) ON P.COD=U.PER AND U.BAJA=0" & vbCrLf
    sConsulta = sConsulta & "       LEFT OUTER JOIN PM_ENLACE_EMAIL EE WITH(NOLOCK) ON NE.ENLACE=EE.ENLACE AND EE.IDI=U.IDIOMA" & vbCrLf
    sConsulta = sConsulta & "       WHERE NE.TIPO_NOTIFICADO=5 AND E.ID=@ENLACE AND IE2.INSTANCIA=@ID" & vbCrLf
    sConsulta = sConsulta & "   --Notificados: Personas NO Peticionarias que participaron en la etapa Origen" & vbCrLf
    sConsulta = sConsulta & "   UNION" & vbCrLf
    sConsulta = sConsulta & "           --No Peticionario       Usuario" & vbCrLf
    sConsulta = sConsulta & "       SELECT DISTINCT ISNULL(EE.SUBJECT,'') AS TEXT,U.IDIOMA," & vbCrLf
    sConsulta = sConsulta & "       U.TIPOEMAIL, P.EMAIL, U.DATEFMT, U.THOUSANFMT, U.DECIMALFMT, U.PRECISIONFMT, 0 AS ESPROVEEDOR,P.COD CODDESTINATARIO, NE.ID AS NOTIFICADO, U.COD CODUSUARIO" & vbCrLf
    sConsulta = sConsulta & "       FROM PER P WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "       INNER JOIN PM_COPIA_ROL R WITH(NOLOCK) ON P.COD=R.PER" & vbCrLf
    sConsulta = sConsulta & "       INNER JOIN PM_COPIA_ROL_ACCION RA WITH(NOLOCK) ON R.ID=RA.ROL" & vbCrLf
    sConsulta = sConsulta & "       INNER JOIN PM_COPIA_ENLACE E WITH(NOLOCK) ON RA.ACCION=E.ACCION" & vbCrLf
    sConsulta = sConsulta & "       INNER JOIN PM_NOTIFICADO_ENLACE NE WITH(NOLOCK) ON E.ENLACE_ORIGEN=NE.ENLACE" & vbCrLf
    sConsulta = sConsulta & "       LEFT JOIN USU U WITH(NOLOCK) ON P.COD=U.PER AND U.BAJA=0" & vbCrLf
    sConsulta = sConsulta & "       LEFT OUTER JOIN PM_ENLACE_EMAIL EE WITH(NOLOCK) ON NE.ENLACE=EE.ENLACE AND EE.IDI=U.IDIOMA" & vbCrLf
    sConsulta = sConsulta & "       WHERE R.TIPO<>4 AND R.TIPO<>2 AND R.PER IS NOT NULL AND ISNULL(R.COMO_ASIGNAR,0)=0 AND NE.TIPO_NOTIFICADO=5" & vbCrLf
    sConsulta = sConsulta & "           AND R.INSTANCIA=@ID AND E.ID=@ENLACE" & vbCrLf
    sConsulta = sConsulta & "       UNION" & vbCrLf
    sConsulta = sConsulta & "           --No Peticionario       Lista" & vbCrLf
    sConsulta = sConsulta & "       SELECT DISTINCT ISNULL(EE.SUBJECT,'') AS TEXT,U.IDIOMA," & vbCrLf
    sConsulta = sConsulta & "       U.TIPOEMAIL, P.EMAIL, U.DATEFMT, U.THOUSANFMT, U.DECIMALFMT, U.PRECISIONFMT, 0 AS ESPROVEEDOR,P.COD CODDESTINATARIO, NE.ID AS NOTIFICADO, U.COD CODUSUARIO" & vbCrLf
    sConsulta = sConsulta & "       FROM PER P WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "       INNER JOIN PM_PARTICIPANTES PA WITH(NOLOCK) ON P.COD=PA.PER" & vbCrLf
    sConsulta = sConsulta & "       INNER JOIN PM_COPIA_ROL R WITH(NOLOCK) ON PA.ROL=R.ROL_ORIGEN AND PA.PER=R.PER" & vbCrLf
    sConsulta = sConsulta & "       INNER JOIN PM_COPIA_ROL_ACCION RA WITH(NOLOCK) ON R.ID=RA.ROL" & vbCrLf
    sConsulta = sConsulta & "       INNER JOIN PM_COPIA_ENLACE E WITH(NOLOCK) ON RA.ACCION=E.ACCION" & vbCrLf
    sConsulta = sConsulta & "       INNER JOIN PM_NOTIFICADO_ENLACE NE WITH(NOLOCK) ON E.ENLACE_ORIGEN=NE.ENLACE" & vbCrLf
    sConsulta = sConsulta & "       LEFT JOIN USU U WITH(NOLOCK) ON P.COD=U.PER AND U.BAJA=0" & vbCrLf
    sConsulta = sConsulta & "       LEFT OUTER JOIN PM_ENLACE_EMAIL EE WITH(NOLOCK) ON NE.ENLACE=EE.ENLACE AND EE.IDI=U.IDIOMA" & vbCrLf
    sConsulta = sConsulta & "       WHERE NE.TIPO_NOTIFICADO=5 AND R.TIPO<>4 AND R.TIPO<>2 AND R.INSTANCIA=@ID AND E.ID=@ENLACE" & vbCrLf
    sConsulta = sConsulta & "        UNION" & vbCrLf
    sConsulta = sConsulta & "           --No Peticionario       DeCampoDePantalla" & vbCrLf
    sConsulta = sConsulta & "       SELECT DISTINCT ISNULL(EE.SUBJECT,'') AS TEXT,U.IDIOMA," & vbCrLf
    sConsulta = sConsulta & "       U.TIPOEMAIL, P.EMAIL, U.DATEFMT, U.THOUSANFMT, U.DECIMALFMT, U.PRECISIONFMT, 0 AS ESPROVEEDOR,P.COD CODDESTINATARIO, NE.ID AS NOTIFICADO, U.COD CODUSUARIO" & vbCrLf
    sConsulta = sConsulta & "       FROM PER P WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "       INNER JOIN PM_COPIA_ROL R WITH(NOLOCK) ON P.COD=R.PER" & vbCrLf
    sConsulta = sConsulta & "       INNER JOIN PM_COPIA_ROL_ACCION RA WITH(NOLOCK) ON R.ID=RA.ROL" & vbCrLf
    sConsulta = sConsulta & "       INNER JOIN PM_COPIA_ENLACE E WITH(NOLOCK) ON RA.ACCION=E.ACCION" & vbCrLf
    sConsulta = sConsulta & "       INNER JOIN PM_NOTIFICADO_ENLACE NE WITH(NOLOCK) ON E.ENLACE_ORIGEN=NE.ENLACE" & vbCrLf
    sConsulta = sConsulta & "       LEFT JOIN USU U WITH(NOLOCK) ON P.COD=U.PER AND U.BAJA=0" & vbCrLf
    sConsulta = sConsulta & "       LEFT OUTER JOIN PM_ENLACE_EMAIL EE WITH(NOLOCK) ON NE.ENLACE=EE.ENLACE AND EE.IDI=U.IDIOMA" & vbCrLf
    sConsulta = sConsulta & "       WHERE NE.TIPO_NOTIFICADO=5 AND R.TIPO<>4 AND R.TIPO<>2 AND R.COMO_ASIGNAR=1 AND R.INSTANCIA=@ID AND E.ID=@ENLACE" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "--Notificados: Proveedores que participaron en la etapa Origen" & vbCrLf
    sConsulta = sConsulta & "   UNION" & vbCrLf
    sConsulta = sConsulta & "   SELECT DISTINCT ISNULL(EE.SUBJECT,'') AS TEXT," & vbCrLf
    sConsulta = sConsulta & "       ISNULL(C.IDI,(SELECT IDIOMA FROM PARGEN_DEF WITH(NOLOCK) WHERE PARGEN_DEF.ID=1)) IDIOMA," & vbCrLf
    sConsulta = sConsulta & "       C.TIPOEMAIL, C.EMAIL, C.DATEFMT, C.THOUSANFMT, C.DECIMALFMT, C.PRECISIONFMT, 1 AS ESPROVEEDOR,NULL CODDESTINATARIO, NE.ID AS NOTIFICADO, NULL CODUSUARIO" & vbCrLf
    sConsulta = sConsulta & "       FROM CON C WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "       INNER JOIN PM_COPIA_ROL R WITH(NOLOCK) ON C.PROVE=R.PROVE" & vbCrLf
    sConsulta = sConsulta & "       INNER JOIN PM_COPIA_ROL_CON RC WITH(NOLOCK) ON R.ID=RC.ID_ROL AND C.ID=RC.ID_CON" & vbCrLf
    sConsulta = sConsulta & "       INNER JOIN PM_COPIA_ENLACE E WITH(NOLOCK) ON E.ACCION=@ACCION" & vbCrLf
    sConsulta = sConsulta & "       INNER JOIN PM_NOTIFICADO_ENLACE NE WITH(NOLOCK) ON E.ENLACE_ORIGEN=NE.ENLACE" & vbCrLf
    sConsulta = sConsulta & "       INNER JOIN PM_COPIA_ROL_ACCION RA WITH(NOLOCK) ON R.ID=RA.ROL AND E.ACCION=RA.ACCION" & vbCrLf
    sConsulta = sConsulta & "       INNER JOIN PM_ENLACE_EMAIL EE WITH(NOLOCK) ON NE.ENLACE=EE.ENLACE AND EE.IDI=ISNULL(C.IDI,(SELECT IDIOMA FROM PARGEN_DEF WITH(NOLOCK) WHERE PARGEN_DEF.ID=1))" & vbCrLf
    sConsulta = sConsulta & "       WHERE NE.TIPO_NOTIFICADO=5 AND R.INSTANCIA=@ID AND R.TIPO=2 AND E.ID = @ENLACE" & vbCrLf
    sConsulta = sConsulta & "   UNION" & vbCrLf
    sConsulta = sConsulta & "   SELECT DISTINCT ISNULL(EE.SUBJECT,'') AS TEXT," & vbCrLf
    sConsulta = sConsulta & "       ISNULL(C.IDI,(SELECT IDIOMA FROM PARGEN_DEF WITH(NOLOCK) WHERE PARGEN_DEF.ID=1)) IDIOMA," & vbCrLf
    sConsulta = sConsulta & "       C.TIPOEMAIL, C.EMAIL, C.DATEFMT, C.THOUSANFMT, C.DECIMALFMT, C.PRECISIONFMT, 1 AS ESPROVEEDOR,NULL CODDESTINATARIO, NE.ID AS NOTIFICADO, NULL CODUSUARIO" & vbCrLf
    sConsulta = sConsulta & "       FROM CON C WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "       INNER JOIN PM_COPIA_ROL R WITH(NOLOCK) ON C.PROVE=R.PROVE" & vbCrLf
    sConsulta = sConsulta & "       AND NOT EXISTS (SELECT ID_CON FROM PM_COPIA_ROL_CON RC WITH(NOLOCK) WHERE RC.ID_ROL = R.ID)" & vbCrLf
    sConsulta = sConsulta & "       INNER JOIN PM_COPIA_ENLACE E WITH(NOLOCK) ON E.ACCION=@ACCION" & vbCrLf
    sConsulta = sConsulta & "       INNER JOIN PM_NOTIFICADO_ENLACE NE WITH(NOLOCK) ON E.ENLACE_ORIGEN=NE.ENLACE" & vbCrLf
    sConsulta = sConsulta & "       INNER JOIN PM_COPIA_ROL_ACCION RA WITH(NOLOCK) ON R.ID=RA.ROL AND E.ACCION=RA.ACCION" & vbCrLf
    sConsulta = sConsulta & "       INNER JOIN PM_ENLACE_EMAIL EE WITH(NOLOCK) ON NE.ENLACE=EE.ENLACE AND EE.IDI=ISNULL(C.IDI,(SELECT IDIOMA FROM PARGEN_DEF WITH(NOLOCK) WHERE PARGEN_DEF.ID=1))" & vbCrLf
    sConsulta = sConsulta & "       WHERE NE.TIPO_NOTIFICADO=5 AND R.INSTANCIA=@ID AND R.TIPO=2 AND E.ID = @ENLACE" & vbCrLf
    sConsulta = sConsulta & "   UNION" & vbCrLf
    sConsulta = sConsulta & "   SELECT DISTINCT ISNULL(EE.SUBJECT,'') AS TEXT," & vbCrLf
    sConsulta = sConsulta & "       ISNULL(C.IDI,(SELECT IDIOMA FROM PARGEN_DEF WITH(NOLOCK) WHERE PARGEN_DEF.ID=1)) IDIOMA," & vbCrLf
    sConsulta = sConsulta & "       C.TIPOEMAIL, C.EMAIL, C.DATEFMT, C.THOUSANFMT, C.DECIMALFMT, C.PRECISIONFMT, 1 AS ESPROVEEDOR,NULL CODDESTINATARIO, NE.ID AS NOTIFICADO, NULL CODUSUARIO" & vbCrLf
    sConsulta = sConsulta & "   FROM CON C WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN INSTANCIA_EST IE2 WITH(NOLOCK) ON IE2.CONTACTO=C.ID AND IE2.DESTINATARIO_PROV=C.PROVE" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN INSTANCIA_CAMINO IC WITH(NOLOCK) ON IE2.BLOQUE=IC.INSTANCIA_BLOQUE_ORIGEN" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN INSTANCIA_EST IE WITH(NOLOCK) ON IC.INSTANCIA_EST=IE.ID" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ENLACE E WITH(NOLOCK) ON IE.PM_ACCION=E.ACCION" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_NOTIFICADO_ENLACE NE WITH(NOLOCK) ON E.ENLACE_ORIGEN=NE.ENLACE" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_ENLACE_EMAIL EE WITH(NOLOCK) ON EE.ENLACE=NE.ENLACE AND EE.IDI=ISNULL(C.IDI,(SELECT IDIOMA FROM PARGEN_DEF WITH(NOLOCK) WHERE PARGEN_DEF.ID=1))" & vbCrLf
    sConsulta = sConsulta & "       WHERE NE.TIPO_NOTIFICADO=5 AND E.ID = @ENLACE" & vbCrLf
    sConsulta = sConsulta & "       AND E.ACCION = @ACCION AND IE2.INSTANCIA=@ID" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "--Notificados: Responsables de Procesos de Compra asociados a la solicitud" & vbCrLf
    sConsulta = sConsulta & "   UNION" & vbCrLf
    sConsulta = sConsulta & "   SELECT DISTINCT ISNULL(EE.SUBJECT,'') AS TEXT," & vbCrLf
    sConsulta = sConsulta & "       U.IDIOMA," & vbCrLf
    sConsulta = sConsulta & "       U.TIPOEMAIL, P.EMAIL, U.DATEFMT, U.THOUSANFMT, U.DECIMALFMT, U.PRECISIONFMT, 0 AS ESPROVEEDOR,P.COD CODDESTINATARIO, NE.ID AS NOTIFICADO, U.COD CODUSUARIO" & vbCrLf
    sConsulta = sConsulta & "   FROM PER P WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PROCE PR WITH(NOLOCK) ON P.COD=PR.COM" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN ITEM S WITH(NOLOCK) ON PR.ANYO =S.ANYO AND PR.GMN1 =S.GMN1_PROCE  AND PR.COD =S.PROCE " & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN INSTANCIA I WITH(NOLOCK) ON S.SOLICIT=I.ID " & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN INSTANCIA_EST IE WITH(NOLOCK) ON I.ID=IE.INSTANCIA" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ENLACE E WITH(NOLOCK) ON IE.PM_ACCION=E.ACCION" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_NOTIFICADO_ENLACE NE WITH(NOLOCK) ON E.ENLACE_ORIGEN=NE.ENLACE" & vbCrLf
    sConsulta = sConsulta & "   LEFT JOIN USU U WITH(NOLOCK) ON P.COD=U.PER AND U.BAJA=0" & vbCrLf
    sConsulta = sConsulta & "   LEFT OUTER JOIN PM_ENLACE_EMAIL EE WITH(NOLOCK) ON NE.ENLACE=EE.ENLACE AND EE.IDI=U.IDIOMA" & vbCrLf
    sConsulta = sConsulta & "   WHERE NE.TIPO_NOTIFICADO=6 AND IE.PM_ACCION=@ACCION AND E.ID=@ENLACE AND I.ID=@ID" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "--Notificado: Gestor de la Solicitud" & vbCrLf
    sConsulta = sConsulta & "   UNION" & vbCrLf
    sConsulta = sConsulta & "   SELECT DISTINCT ISNULL(EE.SUBJECT,'') AS TEXT,U.IDIOMA," & vbCrLf
    sConsulta = sConsulta & "       U.TIPOEMAIL, P.EMAIL, U.DATEFMT, U.THOUSANFMT, U.DECIMALFMT, U.PRECISIONFMT, 0 AS ESPROVEEDOR,P.COD CODDESTINATARIO, NE.ID AS NOTIFICADO, U.COD CODUSUARIO" & vbCrLf
    sConsulta = sConsulta & "   FROM PER P WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN SOLICITUD S WITH(NOLOCK) ON P.COD=S.GESTOR" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN INSTANCIA I WITH(NOLOCK) ON S.ID=I.SOLICITUD" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN INSTANCIA_EST IE WITH(NOLOCK) ON I.ID=IE.INSTANCIA" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ENLACE E WITH(NOLOCK) ON IE.PM_ACCION=E.ACCION" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_NOTIFICADO_ENLACE NE WITH(NOLOCK) ON E.ENLACE_ORIGEN=NE.ENLACE" & vbCrLf
    sConsulta = sConsulta & "   LEFT JOIN USU U WITH(NOLOCK) ON P.COD=U.PER AND U.BAJA=0" & vbCrLf
    sConsulta = sConsulta & "   LEFT OUTER JOIN PM_ENLACE_EMAIL EE WITH(NOLOCK) ON NE.ENLACE=EE.ENLACE AND EE.IDI=U.IDIOMA" & vbCrLf
    sConsulta = sConsulta & "   WHERE NE.TIPO_NOTIFICADO=7 AND IE.PM_ACCION=@ACCION AND E.ID=@ENLACE AND I.ID=@ID" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "--Notificado: roles observadores definidos como UON o Departamento" & vbCrLf
    sConsulta = sConsulta & "   UNION" & vbCrLf
    sConsulta = sConsulta & "   SELECT DISTINCT ISNULL(EE.SUBJECT,'') AS TEXT,U.IDIOMA," & vbCrLf
    sConsulta = sConsulta & "       U.TIPOEMAIL, P.EMAIL, U.DATEFMT, U.THOUSANFMT, U.DECIMALFMT, U.PRECISIONFMT, 0 AS ESPROVEEDOR,P.COD CODDESTINATARIO, NE.ID AS NOTIFICADO, U.COD CODUSUARIO" & vbCrLf
    sConsulta = sConsulta & "   FROM PER P WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   LEFT JOIN USU U WITH(NOLOCK) ON P.COD=U.PER AND U.BAJA=0" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ROL R WITH(NOLOCK) ON R.TIPO =5 AND R.CUANDO_ASIGNAR =2 AND R.PER IS NULL" & vbCrLf
    sConsulta = sConsulta & "   AND ISNULL(R.UON1,'') =ISNULL(P.UON1,'') AND  ISNULL(R.UON2,'') =ISNULL(P.UON2,'') AND ISNULL(R.UON3,'') =ISNULL(P.UON3,'') AND (R.DEP =P.DEP OR R.DEP IS NULL)" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_ROL ROL WITH(NOLOCK) ON R.ROL_ORIGEN=ROL.ID" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_NOTIFICADO_ENLACE NE WITH(NOLOCK)  ON NE.ROL=ROL.ID" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN PM_COPIA_ENLACE E WITH(NOLOCK) ON NE.ENLACE=E.ENLACE_ORIGEN" & vbCrLf
    sConsulta = sConsulta & "   LEFT OUTER JOIN PM_ENLACE_EMAIL EE WITH(NOLOCK) ON NE.ENLACE=EE.ENLACE AND EE.IDI=U.IDIOMA" & vbCrLf
    sConsulta = sConsulta & "   WHERE R.INSTANCIA =@ID AND NE.TIPO_NOTIFICADO=2 AND E.ID=@ENLACE " & vbCrLf
    sConsulta = sConsulta & "   " & vbCrLf
    sConsulta = sConsulta & "   SELECT TEXT, IDIOMA, TIPOEMAIL, EMAIL, DATEFMT, THOUSANFMT, DECIMALFMT, PRECISIONFMT, ESPROVEEDOR,CODDESTINATARIO, MIN(NOTIFICADO) AS NOTIFICADO, CODUSUARIO,IDIOMAS.LANGUAGETAG " & vbCrLf
    sConsulta = sConsulta & "   FROM #FSPM_DATOS_MAIL_ENLACE FS" & vbCrLf
    sConsulta = sConsulta & "   LEFT JOIN IDIOMAS WITH (NOLOCK) ON FS.IDIOMA =IDIOMAS.COD " & vbCrLf
    sConsulta = sConsulta & "   GROUP BY TEXT, IDIOMA, TIPOEMAIL, EMAIL, DATEFMT, THOUSANFMT, DECIMALFMT, PRECISIONFMT, ESPROVEEDOR, CODDESTINATARIO, CODUSUARIO,IDIOMAS.LANGUAGETAG " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   DROP TABLE #FSPM_DATOS_MAIL_ENLACE" & vbCrLf
    sConsulta = sConsulta & "  END" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion31900_08_1890_A31900_08_1891() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim oADOConnection As ADODB.Connection
    Dim sConsulta As String

    On Error GoTo Error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Actualiza Tablas"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 3
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    frmProgreso.ProgressBar1.Refresh
    V_31900_8_Tablas_091
    
    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Actualiza Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 3
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    frmProgreso.ProgressBar1.Refresh
    V_31900_8_Storeds_091
    
    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Actualiza Datos"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    frmProgreso.ProgressBar1.Refresh
    V_31900_8_Datos_091
    
    sConsulta = "UPDATE VERSION SET NUM ='3.00.18.91'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103), PRODVER='31900.8'"
    ExecuteSQL gRDOCon, sConsulta
        
    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False
        
    CodigoDeActualizacion31900_08_1890_A31900_08_1891 = True
    Exit Function

Error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    CodigoDeActualizacion31900_08_1890_A31900_08_1891 = False
End Function

Public Sub V_31900_8_Tablas_091()
    Dim sConsulta As String
    
    sConsulta = "CREATE TABLE [dbo].[PROVE_USU_JDE](" & vbCrLf
    sConsulta = sConsulta & "   [ID] [int] IDENTITY(1,1) NOT NULL," & vbCrLf
    sConsulta = sConsulta & "   [NIF] [varchar](50) NULL," & vbCrLf
    sConsulta = sConsulta & "   [COD] [nvarchar](100) NULL," & vbCrLf
    sConsulta = sConsulta & "   [PWD] [nvarchar](100) NULL," & vbCrLf
    sConsulta = sConsulta & "   [FECACT] [datetime] NULL," & vbCrLf
    sConsulta = sConsulta & "   PRIMARY KEY CLUSTERED " & vbCrLf
    sConsulta = sConsulta & "(" & vbCrLf
    sConsulta = sConsulta & "   [ID] ASC" & vbCrLf
    sConsulta = sConsulta & ")WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]" & vbCrLf
    sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "CREATE TRIGGER [dbo].[PROVE_USU_JDE_FECACT] ON [dbo].[PROVE_USU_JDE]" & vbCrLf
    sConsulta = sConsulta & "    FOR INSERT,UPDATE " & vbCrLf
    sConsulta = sConsulta & "  " & vbCrLf
    sConsulta = sConsulta & "  AS" & vbCrLf
    sConsulta = sConsulta & "  UPDATE PROVE_USU_JDE SET FECACT=GETDATE() FROM PROVE_USU_JDE WITH (NOLOCK) INNER JOIN INSERTED I ON dbo.PROVE_USU_JDE.ID = I.ID " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
End Sub

Public Sub V_31900_8_Storeds_091()
    Dim sConsulta As String
    
    sConsulta = "IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_WEBPART_TIPOS_SOLICITUD]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_WEBPART_TIPOS_SOLICITUD]" & vbCrLf
    gRDOCon.QueryTimeout = 7200
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "CREATE PROCEDURE [dbo].[FSPM_WEBPART_TIPOS_SOLICITUD] @USU VARCHAR(50), @IDI VARCHAR(20), @SOLOPETICIONARIO AS TINYINT = 0, @TIPO TINYINT = 0" & vbCrLf
    sConsulta = sConsulta & "AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(4000)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "IF @SOLOPETICIONARIO = 1 " & vbCrLf
    sConsulta = sConsulta & "BEGIN" & vbCrLf
    sConsulta = sConsulta & "   SET @SQL = N'SELECT DISTINCT S.ID, DEN_' + @IDI + ' AS DEN FROM SOLICITUD S WITH(NOLOCK) INNER JOIN TIPO_SOLICITUDES TS WITH(NOLOCK) ON S.TIPO=TS.ID '" & vbCrLf
    sConsulta = sConsulta & "       SET @SQL = @SQL + ' INNER JOIN VIEW_ROL_PETICIONARIO R WITH (NOLOCK) ON S.ID = R.SOLICITUD AND R.COD= @USU '" & vbCrLf
    sConsulta = sConsulta & "       IF @TIPO = 0 " & vbCrLf
    sConsulta = sConsulta & "               SET @SQL = @SQL + ' WHERE S.PUB=1 AND S.BAJA=0 AND TS.TIPO NOT IN (2,3,7) '" & vbCrLf
    sConsulta = sConsulta & "       ELSE" & vbCrLf
    sConsulta = sConsulta & "               SET @SQL = @SQL + ' WHERE S.PUB=1 AND S.BAJA=0 AND TS.TIPO = @TIPO'" & vbCrLf
    sConsulta = sConsulta & "END" & vbCrLf
    sConsulta = sConsulta & "ELSE" & vbCrLf
    sConsulta = sConsulta & "BEGIN" & vbCrLf
    sConsulta = sConsulta & "   SET @SQL = 'SELECT DISTINCT S.ID, DEN_' + @IDI + ' AS DEN FROM SOLICITUD S WITH(NOLOCK) INNER JOIN TIPO_SOLICITUDES TS WITH(NOLOCK) ON S.TIPO=TS.ID '" & vbCrLf
    sConsulta = sConsulta & "   SET @SQL = @SQL + ' INNER JOIN PM_ROL R WITH (NOLOCK) ON R.WORKFLOW=S.WORKFLOW AND (PER=@USU OR SUSTITUTO=@USU) '" & vbCrLf
    sConsulta = sConsulta & "   IF @TIPO = 0 " & vbCrLf
    sConsulta = sConsulta & "       SET @SQL = @SQL + ' WHERE TS.TIPO NOT IN (2,3,7) '" & vbCrLf
    sConsulta = sConsulta & "   ELSE" & vbCrLf
    sConsulta = sConsulta & "       SET @SQL = @SQL + ' WHERE TS.TIPO = @TIPO'" & vbCrLf
    sConsulta = sConsulta & "   SET @SQL = @SQL + ' UNION SELECT DISTINCT S.ID,DEN_' + @IDI + ' AS DEN FROM SOLICITUD S WITH(NOLOCK) INNER JOIN TIPO_SOLICITUDES TS WITH(NOLOCK) ON S.TIPO=TS.ID '" & vbCrLf
    sConsulta = sConsulta & "   SET @SQL = @SQL + ' INNER JOIN INSTANCIA I WITH(NOLOCK) ON I.SOLICITUD = S.ID '" & vbCrLf
    sConsulta = sConsulta & "   SET @SQL = @SQL + ' INNER JOIN PM_COPIA_ROL R WITH (NOLOCK) ON R.INSTANCIA=I.ID AND '" & vbCrLf
    sConsulta = sConsulta & "   SET @SQL = @SQL + '                                                             ('" & vbCrLf
    sConsulta = sConsulta & "   SET @SQL = @SQL + '                                                             (PER=@USU OR SUSTITUTO=@USU) '" & vbCrLf
    sConsulta = sConsulta & "   SET @SQL = @SQL + '                                                             OR'" & vbCrLf
    sConsulta = sConsulta & "   SET @SQL = @SQL + '                                                             (R.TIPO=5 AND'" & vbCrLf
    sConsulta = sConsulta & "   SET @SQL = @SQL + '                                                                     ('" & vbCrLf
    sConsulta = sConsulta & "   SET @SQL = @SQL + '                                                                     EXISTS (SELECT COD FROM PER P WITH (NOLOCK) WHERE P.COD = @USU AND P.UON1 = R.UON1 AND ISNULL(P.UON2, '''') = ISNULL(R.UON2,'''') AND ISNULL(P.UON3, '''') = ISNULL(R.UON3,'''') AND ISNULL(P.DEP,'''') =  ISNULL(R.DEP,'''') AND R.PER IS NULL) '-- EL DEPARTAMENTO DEL USUARIO ES OBSERVADOR" & vbCrLf
    sConsulta = sConsulta & "   SET @SQL = @SQL + '                                                                     OR '" & vbCrLf
    sConsulta = sConsulta & "   SET @SQL = @SQL + '                                                                     EXISTS (SELECT COD FROM PER P WITH (NOLOCK) WHERE P.COD = @USU AND P.UON1 = R.UON1 AND ISNULL(P.UON2, '''') = ISNULL(R.UON2,'''') AND ISNULL(P.UON3, '''') = ISNULL(R.UON3,'''') AND R.DEP IS NULL AND R.PER IS NULL) '-- EL NIVEL UON3 DEL USUARIO ES OBSERVADOR" & vbCrLf
    sConsulta = sConsulta & "   SET @SQL = @SQL + '                                                                     OR '" & vbCrLf
    sConsulta = sConsulta & "   SET @SQL = @SQL + '                                                                     EXISTS (SELECT COD FROM PER P WITH (NOLOCK) WHERE P.COD = @USU AND P.UON1 = R.UON1 AND ISNULL(P.UON2, '''') = ISNULL(R.UON2,'''') AND R.UON3 IS NULL AND R.DEP IS NULL AND R.PER IS NULL) '-- EL NIVEL UON2 DEL USUARIO ES OBSERVADOR" & vbCrLf
    sConsulta = sConsulta & "   SET @SQL = @SQL + '                                                                     OR '" & vbCrLf
    sConsulta = sConsulta & "   SET @SQL = @SQL + '                                                                     EXISTS (SELECT COD FROM PER P WITH (NOLOCK) WHERE P.COD = @USU AND P.UON1 = R.UON1 AND R.UON2 IS NULL AND R.UON3 IS NULL AND R.DEP IS NULL AND R.PER IS NULL) '-- EL NIVEL UON1 DEL USUARIO ES OBSERVADOR" & vbCrLf
    sConsulta = sConsulta & "   SET @SQL = @SQL + '                                                                     OR'" & vbCrLf
    sConsulta = sConsulta & "   SET @SQL = @SQL + '                                                                     R.UON0 = 1 '--EL NIVEL UON0, O SEA, RA�Z (TODA LA ORGANIZACI�N) ES OBSERVADOR" & vbCrLf
    sConsulta = sConsulta & "   SET @SQL = @SQL + '                                                                     )'" & vbCrLf
    sConsulta = sConsulta & "   SET @SQL = @SQL + '                                                             )'" & vbCrLf
    sConsulta = sConsulta & "   SET @SQL = @SQL + '                                                             )'" & vbCrLf
    sConsulta = sConsulta & "   IF @TIPO = 0 " & vbCrLf
    sConsulta = sConsulta & "       SET @SQL = @SQL + ' WHERE TS.TIPO NOT IN (2,3,7) '" & vbCrLf
    sConsulta = sConsulta & "   ELSE" & vbCrLf
    sConsulta = sConsulta & "       SET @SQL = @SQL + ' WHERE TS.TIPO = @TIPO'" & vbCrLf
    sConsulta = sConsulta & "   SET @SQL = @SQL + ' UNION  SELECT DISTINCT S.ID, DEN_' + @IDI + ' AS DEN FROM SOLICITUD S WITH(NOLOCK) INNER JOIN TIPO_SOLICITUDES TS WITH(NOLOCK) ON S.TIPO=TS.ID '" & vbCrLf
    sConsulta = sConsulta & "   SET @SQL = @SQL + ' INNER JOIN VIEW_ROL_PETICIONARIO R WITH (NOLOCK) ON S.ID = R.SOLICITUD AND R.COD= @USU '" & vbCrLf
    sConsulta = sConsulta & "   IF @TIPO = 0 " & vbCrLf
    sConsulta = sConsulta & "       SET @SQL = @SQL + ' WHERE TS.TIPO NOT IN (2,3,7) '" & vbCrLf
    sConsulta = sConsulta & "   ELSE" & vbCrLf
    sConsulta = sConsulta & "       SET @SQL = @SQL + ' WHERE TS.TIPO = @TIPO'" & vbCrLf
    sConsulta = sConsulta & "END" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@USU VARCHAR(50), @TIPO TINYINT', @USU=@USU,@TIPO=@TIPO" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSINT_VISOR]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSINT_VISOR]" & vbCrLf
    gRDOCon.QueryTimeout = 7200
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "CREATE PROCEDURE [dbo].[FSINT_VISOR]" & vbCrLf
    sConsulta = sConsulta & "  @ERP AS VARCHAR(10)=NULL, @TABLA AS smallint=NULL, " & vbCrLf
    sConsulta = sConsulta & "   @COD_FS AS NVARCHAR(100)=NULL, @COD_ERP AS NVARCHAR(100)=NULL, @RECEP_ERP AS NVARCHAR(1000)=NULL," & vbCrLf
    sConsulta = sConsulta & "   @ANYO_PROCE AS SMALLINT=NULL, @GMN1_PROCE AS VARCHAR(10)=NULL, @COD_PROCE AS INT=NULL," & vbCrLf
    sConsulta = sConsulta & "   @ANYO_PEDIDO AS SMALLINT=NULL, @NUM_PEDIDO AS INT =NULL, @NUM_ORDEN AS INT =NULL," & vbCrLf
    sConsulta = sConsulta & "   @ID_LOG AS INTEGER=NULL, @ACCION AS VARCHAR(1)=NULL, @ESTADO AS VARCHAR(1)=NULL, @SENTIDO AS TINYINT=NULL, " & vbCrLf
    sConsulta = sConsulta & "   @FECHA_DESDE AS DATETIME =NULL, @FECHA_HASTA AS DATETIME=NULL," & vbCrLf
    sConsulta = sConsulta & "   @FORMATOFECHA AS VARCHAR(50)=NULL" & vbCrLf
    sConsulta = sConsulta & "   " & vbCrLf
    sConsulta = sConsulta & "   " & vbCrLf
    sConsulta = sConsulta & "AS" & vbCrLf
    sConsulta = sConsulta & "BEGIN" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "  --Variables para las consultas" & vbCrLf
    sConsulta = sConsulta & "   DECLARE @CONSULTA AS NVARCHAR(MAX)   DECLARE @WHERE AS NVARCHAR(MAX)   DECLARE @WHEREPEDIDO AS NVARCHAR(MAX)" & vbCrLf
    sConsulta = sConsulta & "   DECLARE @WHEREPROCE AS NVARCHAR(MAX) DECLARE @WHERECOD AS NVARCHAR(MAX) DECLARE @WHEREPEDIDO_ENRECEP AS NVARCHAR(MAX)" & vbCrLf
    sConsulta = sConsulta & "   DECLARE @TIPOFECHA AS INT            DECLARE @GMN1 AS NVARCHAR(MAX)     DECLARE @GMN2 AS NVARCHAR(MAX)" & vbCrLf
    sConsulta = sConsulta & "   DECLARE @GMN3 AS NVARCHAR(MAX)" & vbCrLf
    sConsulta = sConsulta & "   --Variables para indicar si la entidad tiene integraci�n activada o no" & vbCrLf
    sConsulta = sConsulta & "   DECLARE @MON_ACTIVA AS TINYINT=0            DECLARE @PAI_ACTIVA AS TINYINT=0            DECLARE @PROVI_ACTIVA AS TINYINT=0 " & vbCrLf
    sConsulta = sConsulta & "   DECLARE @PAG_ACTIVA AS TINYINT=0            DECLARE @DEST_ACTIVA AS TINYINT=0           DECLARE @UNI_ACTIVA AS TINYINT=0 " & vbCrLf
    sConsulta = sConsulta & "   DECLARE @ART4_ACTIVA AS TINYINT=0       DECLARE @PROVE_ACTIVA AS TINYINT=0          DECLARE @ADJ_ACTIVA AS TINYINT=0 " & vbCrLf
    sConsulta = sConsulta & "   DECLARE @PEDIDO_ACTIVA AS TINYINT=0     DECLARE @RECEP_ACTIVA AS TINYINT=0          DECLARE @ESTRMAT_ACTIVA AS TINYINT=0 " & vbCrLf
    sConsulta = sConsulta & "   DECLARE @CONSUMOS_ACTIVA AS TINYINT=0   DECLARE @VARCAL_ACTIVA AS TINYINT=0         DECLARE @SOLICIT_ACTIVA AS TINYINT=0   " & vbCrLf
    sConsulta = sConsulta & "   DECLARE @PRES_ACTIVA AS TINYINT=0       DECLARE @PROCE_ACTIVA AS TINYINT=0          DECLARE @VIA_PAG_ACTIVA AS TINYINT=0 " & vbCrLf
    sConsulta = sConsulta & "   DECLARE @PPM_ACTIVA AS TINYINT=0            DECLARE @TASA_SERVICIO_ACTIVA AS TINYINT=0  DECLARE @CARGO_PROVE_ACTIVA AS TINYINT=0 " & vbCrLf
    sConsulta = sConsulta & "   DECLARE @UON_ACTIVA AS TINYINT=0            DECLARE @USU_ACTIVA AS TINYINT=0            DECLARE @ACTIVO_ACTIVA AS TINYINT=0 " & vbCrLf
    sConsulta = sConsulta & "   DECLARE @FACTURA_ACTIVA AS TINYINT=0        DECLARE @PAGO_ACTIVA AS TINYINT=0           DECLARE @PRES5_ACTIVA AS TINYINT=0 " & vbCrLf
    sConsulta = sConsulta & "   DECLARE @CENTRO_SM_ACTIVA AS TINYINT=0  DECLARE @EMPRESA_ACTIVA AS TINYINT =0       DECLARE @CON_ACTIVA AS TINYINT=0 " & vbCrLf
    sConsulta = sConsulta & "   DECLARE @PROVE_ERP_ACTIVA AS TINYINT=0 " & vbCrLf
    sConsulta = sConsulta & "   " & vbCrLf
    sConsulta = sConsulta & "--Obtiene el formato de fecha" & vbCrLf
    sConsulta = sConsulta & "   SET @TIPOFECHA=103" & vbCrLf
    sConsulta = sConsulta & "   IF @FORMATOFECHA='dd/mm/yyyy'" & vbCrLf
    sConsulta = sConsulta & "       SET @TIPOFECHA=103" & vbCrLf
    sConsulta = sConsulta & "   ELSE IF @FORMATOFECHA='mm/dd/yyyy'" & vbCrLf
    sConsulta = sConsulta & "       SET @TIPOFECHA=101" & vbCrLf
    sConsulta = sConsulta & "   ELSE IF @FORMATOFECHA='dd-mm-yyyy'" & vbCrLf
    sConsulta = sConsulta & "       SET @TIPOFECHA=105" & vbCrLf
    sConsulta = sConsulta & "   ELSE IF @FORMATOFECHA='mm-dd-yyyy'" & vbCrLf
    sConsulta = sConsulta & "       SET @TIPOFECHA=110" & vbCrLf
    sConsulta = sConsulta & "   ELSE IF @FORMATOFECHA='mm.dd.yyyy'" & vbCrLf
    sConsulta = sConsulta & "       SET @TIPOFECHA=104" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "--Siempre filtra por ERP. Si no hay, se coge el primero" & vbCrLf
    sConsulta = sConsulta & "   IF @ERP is null " & vbCrLf
    sConsulta = sConsulta & "       SET @ERP = (SELECT TOP 1 COD FROM ERP WITH(NOLOCK) )" & vbCrLf
    sConsulta = sConsulta & "   IF @TABLA IS NULL" & vbCrLf
    sConsulta = sConsulta & "       SET @TABLA=0" & vbCrLf
    sConsulta = sConsulta & "--------------------------------------------------------------------------------------" & vbCrLf
    sConsulta = sConsulta & "--     Busca las entidades para las que hay que consultar datos" & vbCrLf
    sConsulta = sConsulta & "--------------------------------------------------------------------------------------" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   IF @TABLA=1 OR @TABLA=0" & vbCrLf
    sConsulta = sConsulta & "       begin" & vbCrLf
    sConsulta = sConsulta & "           IF @SENTIDO IS NOT NULL" & vbCrLf
    sConsulta = sConsulta & "               SET @MON_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=1 AND ERP=@ERP AND SENTIDO=@SENTIDO)" & vbCrLf
    sConsulta = sConsulta & "           ELSE IF @SENTIDO IS NULL" & vbCrLf
    sConsulta = sConsulta & "               SET @MON_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=1 AND ERP=@ERP)" & vbCrLf
    sConsulta = sConsulta & "       end" & vbCrLf
    sConsulta = sConsulta & "   IF @TABLA=2 OR @TABLA=0" & vbCrLf
    sConsulta = sConsulta & "       begin" & vbCrLf
    sConsulta = sConsulta & "           IF @SENTIDO IS NOT NULL" & vbCrLf
    sConsulta = sConsulta & "               SET @PAI_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=2 AND ERP=@ERP AND SENTIDO=@SENTIDO)" & vbCrLf
    sConsulta = sConsulta & "           ELSE IF @SENTIDO IS NULL" & vbCrLf
    sConsulta = sConsulta & "               SET @PAI_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=2 AND ERP=@ERP)" & vbCrLf
    sConsulta = sConsulta & "       end" & vbCrLf
    sConsulta = sConsulta & "   IF @TABLA=3 OR @TABLA=0" & vbCrLf
    sConsulta = sConsulta & "       begin" & vbCrLf
    sConsulta = sConsulta & "           IF @SENTIDO IS NOT NULL" & vbCrLf
    sConsulta = sConsulta & "               SET @PROVI_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=3 AND ERP=@ERP AND SENTIDO=@SENTIDO)" & vbCrLf
    sConsulta = sConsulta & "           ELSE IF @SENTIDO IS NULL" & vbCrLf
    sConsulta = sConsulta & "               SET @PROVI_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=3 AND ERP=@ERP)" & vbCrLf
    sConsulta = sConsulta & "       end" & vbCrLf
    sConsulta = sConsulta & "   IF @TABLA=4 OR @TABLA=0" & vbCrLf
    sConsulta = sConsulta & "       begin" & vbCrLf
    sConsulta = sConsulta & "           IF @SENTIDO IS NOT NULL" & vbCrLf
    sConsulta = sConsulta & "               SET @PAG_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=4 AND ERP=@ERP AND SENTIDO=@SENTIDO)" & vbCrLf
    sConsulta = sConsulta & "           ELSE IF @SENTIDO IS NULL" & vbCrLf
    sConsulta = sConsulta & "               SET @PAG_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=4 AND ERP=@ERP)" & vbCrLf
    sConsulta = sConsulta & "       end" & vbCrLf
    sConsulta = sConsulta & "   IF @TABLA=5 OR @TABLA=0" & vbCrLf
    sConsulta = sConsulta & "       begin" & vbCrLf
    sConsulta = sConsulta & "           IF @SENTIDO IS NOT NULL" & vbCrLf
    sConsulta = sConsulta & "               SET @DEST_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=5 AND ERP=@ERP AND SENTIDO=@SENTIDO)   " & vbCrLf
    sConsulta = sConsulta & "           ELSE IF @SENTIDO IS NULL" & vbCrLf
    sConsulta = sConsulta & "               SET @DEST_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=5 AND ERP=@ERP)" & vbCrLf
    sConsulta = sConsulta & "       end" & vbCrLf
    sConsulta = sConsulta & "   IF @TABLA=6 OR @TABLA=0" & vbCrLf
    sConsulta = sConsulta & "       begin" & vbCrLf
    sConsulta = sConsulta & "           IF @SENTIDO IS NOT NULL" & vbCrLf
    sConsulta = sConsulta & "               SET @UNI_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=6 AND ERP=@ERP AND SENTIDO=@SENTIDO)    " & vbCrLf
    sConsulta = sConsulta & "           ELSE IF @SENTIDO IS NULL" & vbCrLf
    sConsulta = sConsulta & "               SET @UNI_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=6 AND ERP=@ERP)" & vbCrLf
    sConsulta = sConsulta & "       end" & vbCrLf
    sConsulta = sConsulta & "   IF @TABLA=7 OR @TABLA=0" & vbCrLf
    sConsulta = sConsulta & "       begin" & vbCrLf
    sConsulta = sConsulta & "           IF @SENTIDO IS NOT NULL" & vbCrLf
    sConsulta = sConsulta & "               SET @ART4_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=7 AND ERP=@ERP AND SENTIDO=@SENTIDO)" & vbCrLf
    sConsulta = sConsulta & "           ELSE IF @SENTIDO IS NULL" & vbCrLf
    sConsulta = sConsulta & "               SET @ART4_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=7 AND ERP=@ERP)" & vbCrLf
    sConsulta = sConsulta & "       end" & vbCrLf
    sConsulta = sConsulta & "   IF @TABLA=8 OR @TABLA=0" & vbCrLf
    sConsulta = sConsulta & "       begin" & vbCrLf
    sConsulta = sConsulta & "           SET @PROVE_ERP_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA_INTERMEDIA=1 AND TABLA=8 AND ERP=@ERP ) --Sentido=NULL " & vbCrLf
    sConsulta = sConsulta & "           IF @PROVE_ERP_ACTIVA=1 AND NOT(@TABLA=0)" & vbCrLf
    sConsulta = sConsulta & "              SET @TABLA=102 --Proveedores con tabla intermedia: TABLAS_INTEGRACION_ERP.TABLA=8 pero LOG_GRAL.TABLA=102" & vbCrLf
    sConsulta = sConsulta & "              " & vbCrLf
    sConsulta = sConsulta & "           IF @SENTIDO IS NOT NULL" & vbCrLf
    sConsulta = sConsulta & "               SET @PROVE_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA_INTERMEDIA=0 AND TABLA=8 AND ERP=@ERP AND SENTIDO=@SENTIDO)" & vbCrLf
    sConsulta = sConsulta & "           ELSE IF @SENTIDO IS NULL" & vbCrLf
    sConsulta = sConsulta & "               SET @PROVE_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA_INTERMEDIA=0 AND TABLA=8 AND ERP=@ERP)" & vbCrLf
    sConsulta = sConsulta & "       end" & vbCrLf
    sConsulta = sConsulta & "   IF @TABLA=9 OR @TABLA=0" & vbCrLf
    sConsulta = sConsulta & "       begin" & vbCrLf
    sConsulta = sConsulta & "           IF @SENTIDO IS NOT NULL" & vbCrLf
    sConsulta = sConsulta & "               SET @ADJ_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=9 AND ERP=@ERP AND SENTIDO=@SENTIDO)    " & vbCrLf
    sConsulta = sConsulta & "           ELSE IF @SENTIDO IS NULL" & vbCrLf
    sConsulta = sConsulta & "               SET @ADJ_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=9 AND ERP=@ERP)" & vbCrLf
    sConsulta = sConsulta & "       end" & vbCrLf
    sConsulta = sConsulta & "   IF @TABLA=11 OR @TABLA=0" & vbCrLf
    sConsulta = sConsulta & "       begin" & vbCrLf
    sConsulta = sConsulta & "           IF @SENTIDO IS NOT NULL" & vbCrLf
    sConsulta = sConsulta & "               SET @PEDIDO_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=11 AND ERP=@ERP AND SENTIDO=@SENTIDO)    " & vbCrLf
    sConsulta = sConsulta & "           ELSE IF @SENTIDO IS NULL" & vbCrLf
    sConsulta = sConsulta & "               SET @PEDIDO_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=11 AND ERP=@ERP) " & vbCrLf
    sConsulta = sConsulta & "       end " & vbCrLf
    sConsulta = sConsulta & "   IF @TABLA=12 OR @TABLA=0" & vbCrLf
    sConsulta = sConsulta & "       begin" & vbCrLf
    sConsulta = sConsulta & "           IF @SENTIDO IS NOT NULL" & vbCrLf
    sConsulta = sConsulta & "               SET @RECEP_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=12 AND ERP=@ERP AND SENTIDO=@SENTIDO)" & vbCrLf
    sConsulta = sConsulta & "           ELSE IF @SENTIDO IS NULL" & vbCrLf
    sConsulta = sConsulta & "               SET @RECEP_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=12 AND ERP=@ERP)" & vbCrLf
    sConsulta = sConsulta & "       end" & vbCrLf
    sConsulta = sConsulta & "   IF @TABLA=13 OR @TABLA=0" & vbCrLf
    sConsulta = sConsulta & "       begin" & vbCrLf
    sConsulta = sConsulta & "           IF @SENTIDO IS NOT NULL" & vbCrLf
    sConsulta = sConsulta & "               SET @PEDIDO_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=13 AND ERP=@ERP AND SENTIDO=@SENTIDO)" & vbCrLf
    sConsulta = sConsulta & "           ELSE IF @SENTIDO IS NULL" & vbCrLf
    sConsulta = sConsulta & "               SET @PEDIDO_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=13 AND ERP=@ERP)" & vbCrLf
    sConsulta = sConsulta & "       end" & vbCrLf
    sConsulta = sConsulta & "   IF @TABLA=14 OR @TABLA=0" & vbCrLf
    sConsulta = sConsulta & "       begin" & vbCrLf
    sConsulta = sConsulta & "           IF @SENTIDO IS NOT NULL" & vbCrLf
    sConsulta = sConsulta & "               SET @RECEP_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=14 AND ERP=@ERP AND SENTIDO=@SENTIDO)" & vbCrLf
    sConsulta = sConsulta & "           ELSE IF @SENTIDO IS NULL" & vbCrLf
    sConsulta = sConsulta & "               SET @RECEP_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=14 AND ERP=@ERP)" & vbCrLf
    sConsulta = sConsulta & "       end" & vbCrLf
    sConsulta = sConsulta & "   IF @TABLA=15 OR @TABLA=0" & vbCrLf
    sConsulta = sConsulta & "       begin" & vbCrLf
    sConsulta = sConsulta & "           IF @SENTIDO IS NOT NULL" & vbCrLf
    sConsulta = sConsulta & "               SET @ESTRMAT_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=15 AND ERP=@ERP AND SENTIDO=@SENTIDO)" & vbCrLf
    sConsulta = sConsulta & "           ELSE IF @SENTIDO IS NULL" & vbCrLf
    sConsulta = sConsulta & "               SET @ESTRMAT_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=15 AND ERP=@ERP)" & vbCrLf
    sConsulta = sConsulta & "       end" & vbCrLf
    sConsulta = sConsulta & "   IF @TABLA=16 OR @TABLA=0" & vbCrLf
    sConsulta = sConsulta & "       begin" & vbCrLf
    sConsulta = sConsulta & "           IF @SENTIDO IS NOT NULL" & vbCrLf
    sConsulta = sConsulta & "               SET @CONSUMOS_ACTIVA  = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=16 AND ERP=@ERP AND SENTIDO=@SENTIDO)" & vbCrLf
    sConsulta = sConsulta & "           ELSE IF @SENTIDO IS NULL" & vbCrLf
    sConsulta = sConsulta & "               SET @CONSUMOS_ACTIVA  = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=16 AND ERP=@ERP)" & vbCrLf
    sConsulta = sConsulta & "       end" & vbCrLf
    sConsulta = sConsulta & "   IF @TABLA=17 OR @TABLA=0" & vbCrLf
    sConsulta = sConsulta & "       begin" & vbCrLf
    sConsulta = sConsulta & "           IF @SENTIDO IS NOT NULL" & vbCrLf
    sConsulta = sConsulta & "               SET @VARCAL_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=17 AND ERP=@ERP AND SENTIDO=@SENTIDO)" & vbCrLf
    sConsulta = sConsulta & "           ELSE IF @SENTIDO IS NULL" & vbCrLf
    sConsulta = sConsulta & "               SET @VARCAL_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=17 AND ERP=@ERP)" & vbCrLf
    sConsulta = sConsulta & "       end" & vbCrLf
    sConsulta = sConsulta & "   IF @TABLA=18 OR @TABLA=0" & vbCrLf
    sConsulta = sConsulta & "       begin" & vbCrLf
    sConsulta = sConsulta & "           IF @SENTIDO IS NOT NULL" & vbCrLf
    sConsulta = sConsulta & "               SET @SOLICIT_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=18 AND ERP=@ERP AND SENTIDO=@SENTIDO)" & vbCrLf
    sConsulta = sConsulta & "           ELSE IF @SENTIDO IS NULL" & vbCrLf
    sConsulta = sConsulta & "               SET @SOLICIT_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=18 AND ERP=@ERP)" & vbCrLf
    sConsulta = sConsulta & "       end" & vbCrLf
    sConsulta = sConsulta & "   IF @TABLA=19 OR @TABLA=0" & vbCrLf
    sConsulta = sConsulta & "       begin" & vbCrLf
    sConsulta = sConsulta & "           IF @SENTIDO IS NOT NULL" & vbCrLf
    sConsulta = sConsulta & "               SET @PRES_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=19 AND ERP=@ERP AND SENTIDO=@SENTIDO)" & vbCrLf
    sConsulta = sConsulta & "           ELSE IF @SENTIDO IS NULL" & vbCrLf
    sConsulta = sConsulta & "               SET @PRES_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=19 AND ERP=@ERP)" & vbCrLf
    sConsulta = sConsulta & "       end" & vbCrLf
    sConsulta = sConsulta & "   IF @TABLA=20 OR @TABLA=0" & vbCrLf
    sConsulta = sConsulta & "       begin" & vbCrLf
    sConsulta = sConsulta & "           IF @SENTIDO IS NOT NULL" & vbCrLf
    sConsulta = sConsulta & "               SET @PRES_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=20 AND ERP=@ERP AND SENTIDO=@SENTIDO)" & vbCrLf
    sConsulta = sConsulta & "           ELSE IF @SENTIDO IS NULL" & vbCrLf
    sConsulta = sConsulta & "               SET @PRES_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=20 AND ERP=@ERP)" & vbCrLf
    sConsulta = sConsulta & "       end" & vbCrLf
    sConsulta = sConsulta & "   IF @TABLA=21 OR @TABLA=0" & vbCrLf
    sConsulta = sConsulta & "       begin" & vbCrLf
    sConsulta = sConsulta & "           IF @SENTIDO IS NOT NULL" & vbCrLf
    sConsulta = sConsulta & "               SET @PRES_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=21 AND ERP=@ERP AND SENTIDO=@SENTIDO)" & vbCrLf
    sConsulta = sConsulta & "           ELSE IF @SENTIDO IS NULL" & vbCrLf
    sConsulta = sConsulta & "               SET @PRES_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=21 AND ERP=@ERP)" & vbCrLf
    sConsulta = sConsulta & "       end" & vbCrLf
    sConsulta = sConsulta & "   IF @TABLA=22 OR @TABLA=0" & vbCrLf
    sConsulta = sConsulta & "       begin" & vbCrLf
    sConsulta = sConsulta & "           IF @SENTIDO IS NOT NULL" & vbCrLf
    sConsulta = sConsulta & "               SET @PRES_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=22 AND ERP=@ERP AND SENTIDO=@SENTIDO)" & vbCrLf
    sConsulta = sConsulta & "           ELSE IF @SENTIDO IS NULL" & vbCrLf
    sConsulta = sConsulta & "               SET @PRES_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=22 AND ERP=@ERP)" & vbCrLf
    sConsulta = sConsulta & "       end" & vbCrLf
    sConsulta = sConsulta & "   IF @TABLA=23 OR @TABLA=0" & vbCrLf
    sConsulta = sConsulta & "       begin" & vbCrLf
    sConsulta = sConsulta & "           IF @SENTIDO IS NOT NULL" & vbCrLf
    sConsulta = sConsulta & "               SET @PROCE_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=23 AND ERP=@ERP AND SENTIDO=@SENTIDO)" & vbCrLf
    sConsulta = sConsulta & "           ELSE IF @SENTIDO IS NULL" & vbCrLf
    sConsulta = sConsulta & "               SET @PROCE_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=23 AND ERP=@ERP)" & vbCrLf
    sConsulta = sConsulta & "       end" & vbCrLf
    sConsulta = sConsulta & "   IF @TABLA=25 OR @TABLA=0" & vbCrLf
    sConsulta = sConsulta & "       begin" & vbCrLf
    sConsulta = sConsulta & "           IF @SENTIDO IS NOT NULL" & vbCrLf
    sConsulta = sConsulta & "               SET @VIA_PAG_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=25 AND ERP=@ERP AND SENTIDO=@SENTIDO)" & vbCrLf
    sConsulta = sConsulta & "           ELSE IF @SENTIDO IS NULL" & vbCrLf
    sConsulta = sConsulta & "               SET @VIA_PAG_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=25 AND ERP=@ERP)" & vbCrLf
    sConsulta = sConsulta & "       end" & vbCrLf
    sConsulta = sConsulta & "   IF @TABLA=26 OR @TABLA=0" & vbCrLf
    sConsulta = sConsulta & "       begin" & vbCrLf
    sConsulta = sConsulta & "           IF @SENTIDO IS NOT NULL" & vbCrLf
    sConsulta = sConsulta & "               SET @PPM_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=26 AND ERP=@ERP AND SENTIDO=@SENTIDO)" & vbCrLf
    sConsulta = sConsulta & "           ELSE IF @SENTIDO IS NULL" & vbCrLf
    sConsulta = sConsulta & "               SET @PPM_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=26 AND ERP=@ERP)" & vbCrLf
    sConsulta = sConsulta & "       end" & vbCrLf
    sConsulta = sConsulta & "   IF @TABLA=27 OR @TABLA=0" & vbCrLf
    sConsulta = sConsulta & "       begin" & vbCrLf
    sConsulta = sConsulta & "           IF @SENTIDO IS NOT NULL" & vbCrLf
    sConsulta = sConsulta & "               SET @TASA_SERVICIO_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=27 AND ERP=@ERP AND SENTIDO=@SENTIDO)" & vbCrLf
    sConsulta = sConsulta & "           ELSE IF @SENTIDO IS NULL" & vbCrLf
    sConsulta = sConsulta & "               SET @TASA_SERVICIO_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=27 AND ERP=@ERP)" & vbCrLf
    sConsulta = sConsulta & "       end" & vbCrLf
    sConsulta = sConsulta & "   IF @TABLA=28 OR @TABLA=0" & vbCrLf
    sConsulta = sConsulta & "       begin" & vbCrLf
    sConsulta = sConsulta & "           IF @SENTIDO IS NOT NULL" & vbCrLf
    sConsulta = sConsulta & "               SET @CARGO_PROVE_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=28 AND ERP=@ERP AND SENTIDO=@SENTIDO)" & vbCrLf
    sConsulta = sConsulta & "           ELSE IF @SENTIDO IS NULL" & vbCrLf
    sConsulta = sConsulta & "               SET @CARGO_PROVE_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=28 AND ERP=@ERP)" & vbCrLf
    sConsulta = sConsulta & "       end" & vbCrLf
    sConsulta = sConsulta & "   IF @TABLA=29 OR @TABLA=0" & vbCrLf
    sConsulta = sConsulta & "       begin" & vbCrLf
    sConsulta = sConsulta & "           IF @SENTIDO IS NOT NULL" & vbCrLf
    sConsulta = sConsulta & "               SET @UON_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=29 AND ERP=@ERP AND SENTIDO=@SENTIDO)" & vbCrLf
    sConsulta = sConsulta & "           ELSE IF @SENTIDO IS NULL" & vbCrLf
    sConsulta = sConsulta & "               SET @UON_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=29 AND ERP=@ERP)" & vbCrLf
    sConsulta = sConsulta & "       end" & vbCrLf
    sConsulta = sConsulta & "   IF @TABLA=30 OR @TABLA=0" & vbCrLf
    sConsulta = sConsulta & "       begin" & vbCrLf
    sConsulta = sConsulta & "           IF @SENTIDO IS NOT NULL" & vbCrLf
    sConsulta = sConsulta & "               SET @USU_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=30 AND ERP=@ERP AND SENTIDO=@SENTIDO)" & vbCrLf
    sConsulta = sConsulta & "           ELSE IF @SENTIDO IS NULL" & vbCrLf
    sConsulta = sConsulta & "               SET @USU_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=30 AND ERP=@ERP)" & vbCrLf
    sConsulta = sConsulta & "       end" & vbCrLf
    sConsulta = sConsulta & "   IF @TABLA=31 OR @TABLA=0" & vbCrLf
    sConsulta = sConsulta & "       begin" & vbCrLf
    sConsulta = sConsulta & "           IF @SENTIDO IS NOT NULL" & vbCrLf
    sConsulta = sConsulta & "               SET @ACTIVO_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=31 AND ERP=@ERP AND SENTIDO=@SENTIDO)" & vbCrLf
    sConsulta = sConsulta & "           ELSE IF @SENTIDO IS NULL" & vbCrLf
    sConsulta = sConsulta & "               SET @ACTIVO_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=31 AND ERP=@ERP)" & vbCrLf
    sConsulta = sConsulta & "       end" & vbCrLf
    sConsulta = sConsulta & "   IF @TABLA=32 OR @TABLA=0" & vbCrLf
    sConsulta = sConsulta & "       begin" & vbCrLf
    sConsulta = sConsulta & "           IF @SENTIDO IS NOT NULL" & vbCrLf
    sConsulta = sConsulta & "               SET @FACTURA_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=32 AND ERP=@ERP AND SENTIDO=@SENTIDO)" & vbCrLf
    sConsulta = sConsulta & "           ELSE IF @SENTIDO IS NULL" & vbCrLf
    sConsulta = sConsulta & "               SET @FACTURA_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=32 AND ERP=@ERP)" & vbCrLf
    sConsulta = sConsulta & "       end" & vbCrLf
    sConsulta = sConsulta & "   IF @TABLA=33 OR @TABLA=0" & vbCrLf
    sConsulta = sConsulta & "       begin" & vbCrLf
    sConsulta = sConsulta & "           IF @SENTIDO IS NOT NULL" & vbCrLf
    sConsulta = sConsulta & "               SET @PAGO_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=33 AND ERP=@ERP AND SENTIDO=@SENTIDO)" & vbCrLf
    sConsulta = sConsulta & "           ELSE IF @SENTIDO IS NULL" & vbCrLf
    sConsulta = sConsulta & "               SET @PAGO_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=33 AND ERP=@ERP)" & vbCrLf
    sConsulta = sConsulta & "       end" & vbCrLf
    sConsulta = sConsulta & "   IF @TABLA=34 OR @TABLA=0" & vbCrLf
    sConsulta = sConsulta & "       begin" & vbCrLf
    sConsulta = sConsulta & "           IF @SENTIDO IS NOT NULL" & vbCrLf
    sConsulta = sConsulta & "               SET @PRES5_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=34 AND ERP=@ERP AND SENTIDO=@SENTIDO)" & vbCrLf
    sConsulta = sConsulta & "           ELSE IF @SENTIDO IS NULL" & vbCrLf
    sConsulta = sConsulta & "               SET @PRES5_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=34 AND ERP=@ERP)" & vbCrLf
    sConsulta = sConsulta & "       end" & vbCrLf
    sConsulta = sConsulta & "   IF @TABLA=35 OR @TABLA=0" & vbCrLf
    sConsulta = sConsulta & "       begin" & vbCrLf
    sConsulta = sConsulta & "           IF @SENTIDO IS NOT NULL" & vbCrLf
    sConsulta = sConsulta & "               SET @CENTRO_SM_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=35 AND ERP=@ERP AND SENTIDO=@SENTIDO)" & vbCrLf
    sConsulta = sConsulta & "           ELSE IF @SENTIDO IS NULL" & vbCrLf
    sConsulta = sConsulta & "               SET @CENTRO_SM_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=35 AND ERP=@ERP)" & vbCrLf
    sConsulta = sConsulta & "       end" & vbCrLf
    sConsulta = sConsulta & "   IF @TABLA=36 OR @TABLA=0" & vbCrLf
    sConsulta = sConsulta & "       begin" & vbCrLf
    sConsulta = sConsulta & "           IF @SENTIDO IS NOT NULL" & vbCrLf
    sConsulta = sConsulta & "               SET @EMPRESA_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=36 AND ERP=@ERP AND SENTIDO=@SENTIDO)" & vbCrLf
    sConsulta = sConsulta & "           ELSE IF @SENTIDO IS NULL" & vbCrLf
    sConsulta = sConsulta & "               SET @EMPRESA_ACTIVA = (SELECT ISNULL(ACTIVA,0) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=36 AND ERP=@ERP)" & vbCrLf
    sConsulta = sConsulta & "       end" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "--------------------------------------------------------------------------------------" & vbCrLf
    sConsulta = sConsulta & "--     Crea la parte WHERE con las opciones de filtrado" & vbCrLf
    sConsulta = sConsulta & "--------------------------------------------------------------------------------------" & vbCrLf
    sConsulta = sConsulta & "   SET @WHERE=''" & vbCrLf
    sConsulta = sConsulta & "----Filtros:" & vbCrLf
    sConsulta = sConsulta & "   SET @WHERE=' AND LG.ERP=' +@ERP" & vbCrLf
    sConsulta = sConsulta & "  " & vbCrLf
    sConsulta = sConsulta & "   IF @ID_LOG IS not NULL" & vbCrLf
    sConsulta = sConsulta & "       SET @WHERE=' AND LG.ID=@ID_LOG'" & vbCrLf
    sConsulta = sConsulta & "       " & vbCrLf
    sConsulta = sConsulta & "   IF @SENTIDO IS NOT NULL" & vbCrLf
    sConsulta = sConsulta & "   begin" & vbCrLf
    sConsulta = sConsulta & "       IF @SENTIDO=1" & vbCrLf
    sConsulta = sConsulta & "           SET @WHERE=@WHERE + ' AND LG.ORIGEN IN (0,2)'" & vbCrLf
    sConsulta = sConsulta & "       ELSE IF @SENTIDO=2 --ENTRADA" & vbCrLf
    sConsulta = sConsulta & "           SET @WHERE=@WHERE + ' AND LG.ORIGEN=3'" & vbCrLf
    sConsulta = sConsulta & "       ELSE IF @SENTIDO=3" & vbCrLf
    sConsulta = sConsulta & "           SET @WHERE=@WHERE + ' AND LG.ORIGEN IN (0,2,3)'" & vbCrLf
    sConsulta = sConsulta & "   end" & vbCrLf
    sConsulta = sConsulta & "   ELSE" & vbCrLf
    sConsulta = sConsulta & "      SET @WHERE=@WHERE + ' AND LG.ORIGEN <> 1'" & vbCrLf
    sConsulta = sConsulta & "      " & vbCrLf
    sConsulta = sConsulta & "   IF @FECHA_DESDE IS NOT NULL" & vbCrLf
    sConsulta = sConsulta & "   BEGIN" & vbCrLf
    sConsulta = sConsulta & "       --Si es en sentido entrada, se busca por FECREC" & vbCrLf
    sConsulta = sConsulta & "       --Si es en sentido salida, se busca por FECENV" & vbCrLf
    sConsulta = sConsulta & "       SET @WHERE = @WHERE + ' AND ((LG.ORIGEN IN (0,2) AND LG.FECENV >= CONVERT(DATETIME,@FECHA_DESDE,@TIPOFECHA)) OR (LG.ORIGEN=3 AND LG.FECREC >=CONVERT(DATETIME,@FECHA_DESDE,@TIPOFECHA)))'" & vbCrLf
    sConsulta = sConsulta & "   END" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   IF @FECHA_HASTA IS NOT NULL" & vbCrLf
    sConsulta = sConsulta & "       --Si es en sentido entrada, se busca por FECREC" & vbCrLf
    sConsulta = sConsulta & "       --Si es en sentido salida, se busca por FECENV" & vbCrLf
    sConsulta = sConsulta & "       SET @WHERE = @WHERE + ' AND ((LG.ORIGEN IN (0,2) AND LG.FECENV <= CONVERT(DATETIME,@FECHA_HASTA,@TIPOFECHA)) OR (LG.ORIGEN=3 AND LG.FECREC <=CONVERT(DATETIME,@FECHA_HASTA,@TIPOFECHA)))'" & vbCrLf
    sConsulta = sConsulta & "       " & vbCrLf
    sConsulta = sConsulta & "   IF @ESTADO IS NOT NULL" & vbCrLf
    sConsulta = sConsulta & "       SET @WHERE=@WHERE + ' AND LG.ESTADO=@ESTADO'" & vbCrLf
    sConsulta = sConsulta & "       " & vbCrLf
    sConsulta = sConsulta & "         " & vbCrLf
    sConsulta = sConsulta & "       " & vbCrLf
    sConsulta = sConsulta & "   " & vbCrLf
    sConsulta = sConsulta & "   --Para Pedidos y recepciones:" & vbCrLf
    sConsulta = sConsulta & "   SET @WHEREPEDIDO=''" & vbCrLf
    sConsulta = sConsulta & "   SET @WHEREPEDIDO_ENRECEP=''" & vbCrLf
    sConsulta = sConsulta & "   IF @ANYO_PEDIDO IS NOT NULL" & vbCrLf
    sConsulta = sConsulta & "   begin" & vbCrLf
    sConsulta = sConsulta & "       SET @WHEREPEDIDO=@WHEREPEDIDO + ' AND LOE.ANYO=@ANYO_PEDIDO'" & vbCrLf
    sConsulta = sConsulta & "       SET @WHEREPEDIDO_ENRECEP = @WHEREPEDIDO_ENRECEP +  ' AND O.ANYO=@ANYO_PEDIDO'" & vbCrLf
    sConsulta = sConsulta & "    end" & vbCrLf
    sConsulta = sConsulta & "   IF @NUM_PEDIDO IS NOT NULL" & vbCrLf
    sConsulta = sConsulta & "   begin" & vbCrLf
    sConsulta = sConsulta & "       SET @WHEREPEDIDO=@WHEREPEDIDO + ' AND LOE.NUM_PEDIDO=@NUM_PEDIDO'  " & vbCrLf
    sConsulta = sConsulta & "       SET @WHEREPEDIDO_ENRECEP = @WHEREPEDIDO_ENRECEP +  ' AND P.NUM=@NUM_PEDIDO'" & vbCrLf
    sConsulta = sConsulta & "    end" & vbCrLf
    sConsulta = sConsulta & "   IF @NUM_ORDEN IS NOT NULL" & vbCrLf
    sConsulta = sConsulta & "   begin" & vbCrLf
    sConsulta = sConsulta & "       SET @WHEREPEDIDO=@WHEREPEDIDO + ' AND LOE.NUM_ORDEN=@NUM_ORDEN'   " & vbCrLf
    sConsulta = sConsulta & "       SET @WHEREPEDIDO_ENRECEP = @WHEREPEDIDO_ENRECEP +  ' AND O.NUM=@NUM_ORDEN'" & vbCrLf
    sConsulta = sConsulta & "    end" & vbCrLf
    sConsulta = sConsulta & "   --Para Procesos, adjudicaciones, asignaci�n de proveedores y recepciones de ofertas:" & vbCrLf
    sConsulta = sConsulta & "   SET @WHEREPROCE=''" & vbCrLf
    sConsulta = sConsulta & "   IF @ANYO_PROCE IS NOT NULL" & vbCrLf
    sConsulta = sConsulta & "       SET @WHEREPROCE=@WHEREPROCE + ' AND L.ANYO=@ANYO_PROCE'" & vbCrLf
    sConsulta = sConsulta & "   IF @GMN1_PROCE  IS NOT NULL" & vbCrLf
    sConsulta = sConsulta & "      begin" & vbCrLf
    sConsulta = sConsulta & "      IF @TABLA=23 --Es un proceso" & vbCrLf
    sConsulta = sConsulta & "           SET @WHEREPROCE=@WHEREPROCE + ' AND L.GMN1=@GMN1_PROCE'" & vbCrLf
    sConsulta = sConsulta & "      ELSE --Es una adjudicaci�n" & vbCrLf
    sConsulta = sConsulta & "           SET @WHEREPROCE=@WHEREPROCE + ' AND L.GMN1_PROCE=@GMN1_PROCE'" & vbCrLf
    sConsulta = sConsulta & "       end" & vbCrLf
    sConsulta = sConsulta & "   IF @COD_PROCE   IS NOT NULL" & vbCrLf
    sConsulta = sConsulta & "      begin" & vbCrLf
    sConsulta = sConsulta & "      IF @TABLA=23 --Es un proceso" & vbCrLf
    sConsulta = sConsulta & "           SET @WHEREPROCE=@WHEREPROCE + ' AND L.COD=@COD_PROCE'" & vbCrLf
    sConsulta = sConsulta & "      ELSE --Es una adjudicaci�n" & vbCrLf
    sConsulta = sConsulta & "           SET @WHEREPROCE=@WHEREPROCE + ' AND L.PROCE=@COD_PROCE'" & vbCrLf
    sConsulta = sConsulta & "      end" & vbCrLf
    sConsulta = sConsulta & "   --Filtro por codigo ERP  " & vbCrLf
    sConsulta = sConsulta & "   IF @RECEP_ERP IS NOT NULL " & vbCrLf
    sConsulta = sConsulta & "       SET @WHERE = @WHERE + ' AND COD_ERP=@RECEP_ERP'" & vbCrLf
    sConsulta = sConsulta & "   IF @COD_ERP IS NOT NULL" & vbCrLf
    sConsulta = sConsulta & "       IF @TABLA=12 OR @TABLA=14 --Es el codigo de pedido, lo incluye en @WHEREPEDIDO_ENRECEP" & vbCrLf
    sConsulta = sConsulta & "           SET @WHEREPEDIDO_ENRECEP = @WHEREPEDIDO_ENRECEP + ' AND O.NUM_PED_ERP=@COD_ERP'  " & vbCrLf
    sConsulta = sConsulta & "       ELSE" & vbCrLf
    sConsulta = sConsulta & "           SET @WHERE = @WHERE + ' AND LG.COD_ERP=@COD_ERP'" & vbCrLf
    sConsulta = sConsulta & "       " & vbCrLf
    sConsulta = sConsulta & "         " & vbCrLf
    sConsulta = sConsulta & "   --Para filtrar por c�digo: Siempre es L.COD excepto en los casos indicados" & vbCrLf
    sConsulta = sConsulta & "   SET @WHERECOD=''" & vbCrLf
    sConsulta = sConsulta & "   if @COD_FS is not null" & vbCrLf
    sConsulta = sConsulta & "   begin " & vbCrLf
    sConsulta = sConsulta & "       IF @TABLA=15" & vbCrLf
    sConsulta = sConsulta & "      BEGIN" & vbCrLf
    sConsulta = sConsulta & "           SET @GMN1 = SUBSTRING(@COD_FS, 0, CHARINDEX('-' ,@COD_FS))" & vbCrLf
    sConsulta = sConsulta & "           SET @COD_FS = SUBSTRING(@COD_FS, CHARINDEX('-' ,@COD_FS) + 1, LEN(@COD_FS))" & vbCrLf
    sConsulta = sConsulta & "           SET @GMN2 = SUBSTRING(@COD_FS, 0, CHARINDEX('-' ,@COD_FS))" & vbCrLf
    sConsulta = sConsulta & "           SET @COD_FS = SUBSTRING(@COD_FS, CHARINDEX('-' ,@COD_FS) + 1, LEN(@COD_FS))" & vbCrLf
    sConsulta = sConsulta & "           SET @GMN3 = SUBSTRING(@COD_FS, 0, CHARINDEX('-' ,@COD_FS))" & vbCrLf
    sConsulta = sConsulta & "           SET @COD_FS = SUBSTRING(@COD_FS, CHARINDEX('-' ,@COD_FS) + 1, LEN(@COD_FS))" & vbCrLf
    sConsulta = sConsulta & "           IF LEN(@GMN1) > 0" & vbCrLf
    sConsulta = sConsulta & "               SET @WHERECOD=@WHERECOD + ' AND LGMN.GMN1='''+@GMN1+'''' " & vbCrLf
    sConsulta = sConsulta & "           IF LEN(@GMN2) > 0" & vbCrLf
    sConsulta = sConsulta & "               SET @WHERECOD=@WHERECOD + ' AND LGMN.GMN2='''+@GMN2+''''    " & vbCrLf
    sConsulta = sConsulta & "           IF LEN(@GMN3) > 0" & vbCrLf
    sConsulta = sConsulta & "               SET @WHERECOD=@WHERECOD + ' AND LGMN.GMN3='''+@GMN3+''''" & vbCrLf
    sConsulta = sConsulta & "           IF LEN(@COD_FS) > 0" & vbCrLf
    sConsulta = sConsulta & "               SET @WHERECOD=@WHERECOD + ' AND LGMN.GMN4='''+@COD_FS+''''" & vbCrLf
    sConsulta = sConsulta & "       END" & vbCrLf
    sConsulta = sConsulta & "       ELSE IF @TABLA=12  " & vbCrLf
    sConsulta = sConsulta & "           SET @WHERECOD=@WHERECOD + ' AND LR.ALBARAN=@COD_FS' " & vbCrLf
    sConsulta = sConsulta & "       ELSE IF @TABLA=14 " & vbCrLf
    sConsulta = sConsulta & "           SET @WHERECOD=@WHERECOD + ' AND LR.ALBARAN=@COD_FS' " & vbCrLf
    sConsulta = sConsulta & "       ELSE IF @TABLA=17  ---HABR�A QUE FILTRAR POR TODOS LOS NIVELES" & vbCrLf
    sConsulta = sConsulta & "           SET @WHERECOD=@WHERECOD + ' AND L.COD_CAL1=@COD_FS' " & vbCrLf
    sConsulta = sConsulta & "       ELSE IF @TABLA=18 " & vbCrLf
    sConsulta = sConsulta & "           SET @WHERECOD=@WHERECOD + ' AND L.INSTANCIA=@COD_FS ' " & vbCrLf
    sConsulta = sConsulta & "       ELSE IF @TABLA=19 OR @TABLA=20 OR @TABLA=21 OR @TABLA=22  OR  @TABLA=34  ---HABR͍A QUE FILTRAR POR TODOS LOS PRES5_NIVx!" & vbCrLf
    sConsulta = sConsulta & "           SET @WHERECOD=@WHERECOD + ' AND (L.PRES1=''' + @COD_FS + ''' OR L.PRES2=''' + @COD_FS + ''' OR L.PRES3=''' + @COD_FS + ''' OR L.PRES4=''' + @COD_FS + ''')'" & vbCrLf
    sConsulta = sConsulta & "       ELSE IF @TABLA=29---HABR͍A QUE FILTRAR POR TODOS LOS UONx!" & vbCrLf
    sConsulta = sConsulta & "           SET @WHERECOD=@WHERECOD + ' AND L.UON1=@COD_FS' " & vbCrLf
    sConsulta = sConsulta & "       ELSE IF @TABLA=32" & vbCrLf
    sConsulta = sConsulta & "           SET @WHERECOD=@WHERECOD + ' AND L.NUM=@COD_FS' " & vbCrLf
    sConsulta = sConsulta & "       ELSE IF @TABLA=33" & vbCrLf
    sConsulta = sConsulta & "           SET @WHERECOD=@WHERECOD + ' AND L.NUM=''' + @COD_FS + ''''" & vbCrLf
    sConsulta = sConsulta & "       ELSE IF @TABLA=35" & vbCrLf
    sConsulta = sConsulta & "           SET @WHERECOD=@WHERECOD + ' AND L.CENTRO_SM=@COD_FS' " & vbCrLf
    sConsulta = sConsulta & "       ELSE IF @TABLA=36 " & vbCrLf
    sConsulta = sConsulta & "           SET @WHERECOD=@WHERECOD + ' AND L.SOCIEDAD=@COD_FS' " & vbCrLf
    sConsulta = sConsulta & "       ELSE IF @TABLA=101  --�EL NOMBRE DEL CONTACTO????" & vbCrLf
    sConsulta = sConsulta & "           SET @WHERECOD=@WHERECOD + ' AND L.NOM=@COD_FS' " & vbCrLf
    sConsulta = sConsulta & "       ELSE" & vbCrLf
    sConsulta = sConsulta & "           SET @WHERECOD=@WHERECOD + ' AND L.COD=''' + @COD_FS + ''''" & vbCrLf
    sConsulta = sConsulta & "   end" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "-------" & vbCrLf
    sConsulta = sConsulta & "DECLARE @PARAM NVARCHAR(MAX)" & vbCrLf
    sConsulta = sConsulta & "SET @PARAM = N'@ERP VARCHAR(10), @TABLA smallint, " & vbCrLf
    sConsulta = sConsulta & "   @COD_FS NVARCHAR(100), @COD_ERP NVARCHAR(100), @RECEP_ERP NVARCHAR(1000)," & vbCrLf
    sConsulta = sConsulta & "   @ANYO_PROCE SMALLINT, @GMN1_PROCE VARCHAR(10), @COD_PROCE INT," & vbCrLf
    sConsulta = sConsulta & "   @ANYO_PEDIDO SMALLINT, @NUM_PEDIDO INT, @NUM_ORDEN INT," & vbCrLf
    sConsulta = sConsulta & "   @ID_LOG INTEGER, @ACCION VARCHAR(1), @ESTADO VARCHAR(1), " & vbCrLf
    sConsulta = sConsulta & "   @FECHA_DESDE DATETIME, @FECHA_HASTA DATETIME,@TIPOFECHA INT'" & vbCrLf
    sConsulta = sConsulta & "   " & vbCrLf
    sConsulta = sConsulta & "-------" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "-------------------------------------------------------------------------------------------------------------" & vbCrLf
    sConsulta = sConsulta & "--     Crea la tabla temporal #T_LOG_GRAL e inserta los registros de LOG_GRAL con las opciones de filtrado" & vbCrLf
    sConsulta = sConsulta & "-------------------------------------------------------------------------------------------------------------" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "CREATE TABLE #T_LOG_GRAL (ID INT, ID_TABLA INT, TABLA TINYINT," & vbCrLf
    sConsulta = sConsulta & "ESTADO NVARCHAR(1) COLLATE SQL_Latin1_General_CP1_CI_AS," & vbCrLf
    sConsulta = sConsulta & "ORIGEN TINYINT, TEXTO NVARCHAR(MAX), ARG1 NVARCHAR(MAX), ARG2 NVARCHAR(MAX)," & vbCrLf
    sConsulta = sConsulta & "REINTENTOS TINYINT, FECENV DATETIME, FECREC DATETIME," & vbCrLf
    sConsulta = sConsulta & "ERP NVARCHAR(20), NOM_FICHERO NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS, " & vbCrLf
    sConsulta = sConsulta & "NOM_RECIBO NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS, " & vbCrLf
    sConsulta = sConsulta & "COD_ERP NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SET @CONSULTA= 'INSERT INTO #T_LOG_GRAL" & vbCrLf
    sConsulta = sConsulta & "SELECT ID,ID_TABLA,TABLA,ESTADO,ORIGEN,TEXTO,ARG1,ARG2,REINTENTOS,FECENV,FECREC,ERP,NOM_FICHERO," & vbCrLf
    sConsulta = sConsulta & "NOM_RECIBO,COD_ERP FROM LOG_GRAL LG WITH(NOLOCK) WHERE 1=1 ' + @WHERE" & vbCrLf
    sConsulta = sConsulta & "IF NOT(@TABLA=0)" & vbCrLf
    sConsulta = sConsulta & "   SET @CONSULTA=@CONSULTA + ' AND LG.TABLA=@TABLA'" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL @CONSULTA,@PARAM,@ERP=@ERP, @TABLA=@TABLA, " & vbCrLf
    sConsulta = sConsulta & "   @COD_FS=@COD_FS, @COD_ERP=@COD_ERP, @RECEP_ERP=@RECEP_ERP," & vbCrLf
    sConsulta = sConsulta & "   @ANYO_PROCE=@ANYO_PROCE, @GMN1_PROCE=@GMN1_PROCE, @COD_PROCE=@COD_PROCE," & vbCrLf
    sConsulta = sConsulta & "   @ANYO_PEDIDO=@ANYO_PEDIDO, @NUM_PEDIDO=@NUM_PEDIDO, @NUM_ORDEN=@NUM_ORDEN," & vbCrLf
    sConsulta = sConsulta & "   @ID_LOG=@ID_LOG, @ACCION=@ACCION, @ESTADO=@ESTADO,@FECHA_DESDE=@FECHA_DESDE,@FECHA_HASTA=@FECHA_HASTA,@TIPOFECHA=@TIPOFECHA" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "------------------------------------------------------------------------------------------------" & vbCrLf
    sConsulta = sConsulta & "-- Crea la tabla temporal #T_CONSULTA donde se van a guardar las subconsultas" & vbCrLf
    sConsulta = sConsulta & "-------------------------------------------------------------------------------------------------" & vbCrLf
    sConsulta = sConsulta & "CREATE TABLE #T_CONSULTA (ERP NVARCHAR(20) COLLATE SQL_Latin1_General_CP1_CI_AS, ENTIDAD TINYINT, IDENTIF INT, ESTADO NVARCHAR(1) COLLATE SQL_Latin1_General_CP1_CI_AS," & vbCrLf
    sConsulta = sConsulta & "TEXTO NVARCHAR(MAX) COLLATE SQL_Latin1_General_CP1_CI_AS, ARG1 NVARCHAR(MAX), ARG2 NVARCHAR(MAX), FECENV DATETIME," & vbCrLf
    sConsulta = sConsulta & "FECREC DATETIME, NOM_FICHERO NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS, NOM_RECIBO NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS, ACCION NVARCHAR(1) COLLATE SQL_Latin1_General_CP1_CI_AS," & vbCrLf
    sConsulta = sConsulta & "COD NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS, COD_ERP NVARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS, USU NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS, INFO NVARCHAR(200) COLLATE SQL_Latin1_General_CP1_CI_AS, ORIGEN TINYINT)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "  " & vbCrLf
    sConsulta = sConsulta & "---------------------------------------------------------------------------------------------------------------------" & vbCrLf
    sConsulta = sConsulta & "-- Actualizamos la @WHERE con los filtros que NO son de LOG_GRAL, esto si lo hacemos antes de la creaci�n de la tabla" & vbCrLf
    sConsulta = sConsulta & "-- temporal nos falla el script (La INSERT de dicha tabla solo tiene en cuenta LOG_GRAL)." & vbCrLf
    sConsulta = sConsulta & "---------------------------------------------------------------------------------------------------------------------  " & vbCrLf
    sConsulta = sConsulta & "  IF @ACCION IS NOT NULL" & vbCrLf
    sConsulta = sConsulta & "       SET @WHERE = @WHERE + ' AND ACCION=@ACCION' " & vbCrLf
    sConsulta = sConsulta & "       " & vbCrLf
    sConsulta = sConsulta & "---------------------------------------------------------------------------------------------------------------------" & vbCrLf
    sConsulta = sConsulta & "--             CONSULTA " & vbCrLf
    sConsulta = sConsulta & "--No se muestran las tablas externas (LOG_TABLAS_EXTERNAS), recepciones de ofertas ni asignaci�n de proveedores" & vbCrLf
    sConsulta = sConsulta & "--------------------------------------------------------------------------------------------------------------------" & vbCrLf
    sConsulta = sConsulta & "IF @MON_ACTIVA=1" & vbCrLf
    sConsulta = sConsulta & "BEGIN" & vbCrLf
    sConsulta = sConsulta & "   SET @CONSULTA= 'INSERT INTO #T_CONSULTA SELECT " & vbCrLf
    sConsulta = sConsulta & "   LG.ERP AS ERP, LG.TABLA AS ENTIDAD, LG.ID AS IDENTIF, LG.ESTADO, LG.TEXTO, LG.ARG1, LG.ARG2,LG.FECENV, LG.FECREC, NOM_FICHERO , NOM_RECIBO," & vbCrLf
    sConsulta = sConsulta & "   L.ACCION , L.COD , LG.COD_ERP , L.USU , '''' INFO,LG.ORIGEN" & vbCrLf
    sConsulta = sConsulta & "   FROM #T_LOG_GRAL LG WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN LOG_MON L WITH(NOLOCK) ON LG.ID_TABLA=L.ID AND LG.TABLA=1" & vbCrLf
    sConsulta = sConsulta & "   WHERE 1=1' + @WHERE + @WHERECOD " & vbCrLf
    sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @CONSULTA,@PARAM,@ERP=@ERP, @TABLA=@TABLA, " & vbCrLf
    sConsulta = sConsulta & "   @COD_FS=@COD_FS, @COD_ERP=@COD_ERP, @RECEP_ERP=@RECEP_ERP," & vbCrLf
    sConsulta = sConsulta & "   @ANYO_PROCE=@ANYO_PROCE, @GMN1_PROCE=@GMN1_PROCE, @COD_PROCE=@COD_PROCE," & vbCrLf
    sConsulta = sConsulta & "   @ANYO_PEDIDO=@ANYO_PEDIDO, @NUM_PEDIDO=@NUM_PEDIDO, @NUM_ORDEN=@NUM_ORDEN," & vbCrLf
    sConsulta = sConsulta & "   @ID_LOG=@ID_LOG, @ACCION=@ACCION, @ESTADO=@ESTADO,@FECHA_DESDE=@FECHA_DESDE,@FECHA_HASTA=@FECHA_HASTA,@TIPOFECHA=@TIPOFECHA" & vbCrLf
    sConsulta = sConsulta & "END" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "IF @PAI_ACTIVA=1" & vbCrLf
    sConsulta = sConsulta & "BEGIN" & vbCrLf
    sConsulta = sConsulta & "   SET @CONSULTA= 'INSERT INTO #T_CONSULTA SELECT " & vbCrLf
    sConsulta = sConsulta & "   LG.ERP AS ERP, LG.TABLA AS ENTIDAD, LG.ID AS IDENTIF, LG.ESTADO, LG.TEXTO, LG.ARG1, LG.ARG2,LG.FECENV, LG.FECREC, NOM_FICHERO , NOM_RECIBO," & vbCrLf
    sConsulta = sConsulta & "   L.ACCION , L.COD , LG.COD_ERP , L.USU , '''' INFO,LG.ORIGEN" & vbCrLf
    sConsulta = sConsulta & "   FROM #T_LOG_GRAL LG WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN LOG_PAI L WITH(NOLOCK) ON LG.ID_TABLA=L.ID AND LG.TABLA=2" & vbCrLf
    sConsulta = sConsulta & "   WHERE 1=1' + @WHERE + @WHERECOD" & vbCrLf
    sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @CONSULTA,@PARAM,@ERP=@ERP, @TABLA=@TABLA, " & vbCrLf
    sConsulta = sConsulta & "   @COD_FS=@COD_FS, @COD_ERP=@COD_ERP, @RECEP_ERP=@RECEP_ERP," & vbCrLf
    sConsulta = sConsulta & "   @ANYO_PROCE=@ANYO_PROCE, @GMN1_PROCE=@GMN1_PROCE, @COD_PROCE=@COD_PROCE," & vbCrLf
    sConsulta = sConsulta & "   @ANYO_PEDIDO=@ANYO_PEDIDO, @NUM_PEDIDO=@NUM_PEDIDO, @NUM_ORDEN=@NUM_ORDEN," & vbCrLf
    sConsulta = sConsulta & "   @ID_LOG=@ID_LOG, @ACCION=@ACCION, @ESTADO=@ESTADO,@FECHA_DESDE=@FECHA_DESDE,@FECHA_HASTA=@FECHA_HASTA,@TIPOFECHA=@TIPOFECHA" & vbCrLf
    sConsulta = sConsulta & "END" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "IF @PROVI_ACTIVA=1" & vbCrLf
    sConsulta = sConsulta & "BEGIN" & vbCrLf
    sConsulta = sConsulta & "   SET @CONSULTA= 'INSERT INTO #T_CONSULTA SELECT " & vbCrLf
    sConsulta = sConsulta & "   LG.ERP AS ERP, LG.TABLA AS ENTIDAD, LG.ID AS IDENTIF, LG.ESTADO, LG.TEXTO, LG.ARG1, LG.ARG2,LG.FECENV, LG.FECREC, NOM_FICHERO , NOM_RECIBO," & vbCrLf
    sConsulta = sConsulta & "   L.ACCION , L.COD , LG.COD_ERP , L.USU  , '''' INFO,LG.ORIGEN" & vbCrLf
    sConsulta = sConsulta & "   FROM #T_LOG_GRAL LG WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN LOG_PROVI L WITH(NOLOCK) ON LG.ID_TABLA=L.ID AND LG.TABLA=3" & vbCrLf
    sConsulta = sConsulta & "   WHERE 1=1' + @WHERE + @WHERECOD    " & vbCrLf
    sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @CONSULTA,@PARAM,@ERP=@ERP, @TABLA=@TABLA, " & vbCrLf
    sConsulta = sConsulta & "   @COD_FS=@COD_FS, @COD_ERP=@COD_ERP, @RECEP_ERP=@RECEP_ERP," & vbCrLf
    sConsulta = sConsulta & "   @ANYO_PROCE=@ANYO_PROCE, @GMN1_PROCE=@GMN1_PROCE, @COD_PROCE=@COD_PROCE," & vbCrLf
    sConsulta = sConsulta & "   @ANYO_PEDIDO=@ANYO_PEDIDO, @NUM_PEDIDO=@NUM_PEDIDO, @NUM_ORDEN=@NUM_ORDEN," & vbCrLf
    sConsulta = sConsulta & "   @ID_LOG=@ID_LOG, @ACCION=@ACCION, @ESTADO=@ESTADO,@FECHA_DESDE=@FECHA_DESDE,@FECHA_HASTA=@FECHA_HASTA,@TIPOFECHA=@TIPOFECHA" & vbCrLf
    sConsulta = sConsulta & "END" & vbCrLf
    sConsulta = sConsulta & "   " & vbCrLf
    sConsulta = sConsulta & "IF @PAG_ACTIVA=1" & vbCrLf
    sConsulta = sConsulta & "BEGIN" & vbCrLf
    sConsulta = sConsulta & "   SET @CONSULTA= 'INSERT INTO #T_CONSULTA SELECT " & vbCrLf
    sConsulta = sConsulta & "   LG.ERP AS ERP, LG.TABLA AS ENTIDAD, LG.ID AS IDENTIF, LG.ESTADO, LG.TEXTO, LG.ARG1, LG.ARG2,LG.FECENV, LG.FECREC, NOM_FICHERO , NOM_RECIBO," & vbCrLf
    sConsulta = sConsulta & "   L.ACCION , L.COD , LG.COD_ERP , L.USU  , '''' INFO,LG.ORIGEN" & vbCrLf
    sConsulta = sConsulta & "   FROM #T_LOG_GRAL LG WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN LOG_PAG L WITH(NOLOCK) ON LG.ID_TABLA=L.ID AND LG.TABLA=4" & vbCrLf
    sConsulta = sConsulta & "   WHERE 1=1' + @WHERE + @WHERECOD " & vbCrLf
    sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @CONSULTA,@PARAM,@ERP=@ERP, @TABLA=@TABLA, " & vbCrLf
    sConsulta = sConsulta & "   @COD_FS=@COD_FS, @COD_ERP=@COD_ERP, @RECEP_ERP=@RECEP_ERP," & vbCrLf
    sConsulta = sConsulta & "   @ANYO_PROCE=@ANYO_PROCE, @GMN1_PROCE=@GMN1_PROCE, @COD_PROCE=@COD_PROCE," & vbCrLf
    sConsulta = sConsulta & "   @ANYO_PEDIDO=@ANYO_PEDIDO, @NUM_PEDIDO=@NUM_PEDIDO, @NUM_ORDEN=@NUM_ORDEN," & vbCrLf
    sConsulta = sConsulta & "   @ID_LOG=@ID_LOG, @ACCION=@ACCION, @ESTADO=@ESTADO,@FECHA_DESDE=@FECHA_DESDE,@FECHA_HASTA=@FECHA_HASTA,@TIPOFECHA=@TIPOFECHA" & vbCrLf
    sConsulta = sConsulta & "END" & vbCrLf
    sConsulta = sConsulta & "   " & vbCrLf
    sConsulta = sConsulta & "IF @DEST_ACTIVA=1" & vbCrLf
    sConsulta = sConsulta & "BEGIN" & vbCrLf
    sConsulta = sConsulta & "   SET @CONSULTA= 'INSERT INTO #T_CONSULTA SELECT " & vbCrLf
    sConsulta = sConsulta & "   LG.ERP AS ERP, LG.TABLA AS ENTIDAD, LG.ID AS IDENTIF, LG.ESTADO, LG.TEXTO, LG.ARG1, LG.ARG2,LG.FECENV, LG.FECREC, NOM_FICHERO , NOM_RECIBO," & vbCrLf
    sConsulta = sConsulta & "   L.ACCION , L.COD , LG.COD_ERP , L.USU  , '''' INFO,LG.ORIGEN" & vbCrLf
    sConsulta = sConsulta & "   FROM #T_LOG_GRAL LG WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN LOG_DEST L WITH(NOLOCK) ON LG.ID_TABLA=L.ID AND LG.TABLA=5" & vbCrLf
    sConsulta = sConsulta & "   WHERE 1=1' + @WHERE + @WHERECOD " & vbCrLf
    sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @CONSULTA,@PARAM,@ERP=@ERP, @TABLA=@TABLA, " & vbCrLf
    sConsulta = sConsulta & "   @COD_FS=@COD_FS, @COD_ERP=@COD_ERP, @RECEP_ERP=@RECEP_ERP," & vbCrLf
    sConsulta = sConsulta & "   @ANYO_PROCE=@ANYO_PROCE, @GMN1_PROCE=@GMN1_PROCE, @COD_PROCE=@COD_PROCE," & vbCrLf
    sConsulta = sConsulta & "   @ANYO_PEDIDO=@ANYO_PEDIDO, @NUM_PEDIDO=@NUM_PEDIDO, @NUM_ORDEN=@NUM_ORDEN," & vbCrLf
    sConsulta = sConsulta & "   @ID_LOG=@ID_LOG, @ACCION=@ACCION, @ESTADO=@ESTADO,@FECHA_DESDE=@FECHA_DESDE,@FECHA_HASTA=@FECHA_HASTA,@TIPOFECHA=@TIPOFECHA" & vbCrLf
    sConsulta = sConsulta & "END" & vbCrLf
    sConsulta = sConsulta & "   " & vbCrLf
    sConsulta = sConsulta & "IF @UNI_ACTIVA=1" & vbCrLf
    sConsulta = sConsulta & "BEGIN" & vbCrLf
    sConsulta = sConsulta & "   SET @CONSULTA= 'INSERT INTO #T_CONSULTA SELECT " & vbCrLf
    sConsulta = sConsulta & "   LG.ERP AS ERP, LG.TABLA AS ENTIDAD, LG.ID AS IDENTIF, LG.ESTADO, LG.TEXTO, LG.ARG1, LG.ARG2,LG.FECENV, LG.FECREC, NOM_FICHERO , NOM_RECIBO," & vbCrLf
    sConsulta = sConsulta & "   L.ACCION , L.COD ,LG.COD_ERP , L.USU , '''' INFO,LG.ORIGEN" & vbCrLf
    sConsulta = sConsulta & "   FROM #T_LOG_GRAL LG WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN LOG_UNI L WITH(NOLOCK) ON LG.ID_TABLA=L.ID AND LG.TABLA=6" & vbCrLf
    sConsulta = sConsulta & "   WHERE 1=1' + @WHERE + @WHERECOD " & vbCrLf
    sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @CONSULTA,@PARAM,@ERP=@ERP, @TABLA=@TABLA, " & vbCrLf
    sConsulta = sConsulta & "   @COD_FS=@COD_FS, @COD_ERP=@COD_ERP, @RECEP_ERP=@RECEP_ERP," & vbCrLf
    sConsulta = sConsulta & "   @ANYO_PROCE=@ANYO_PROCE, @GMN1_PROCE=@GMN1_PROCE, @COD_PROCE=@COD_PROCE," & vbCrLf
    sConsulta = sConsulta & "   @ANYO_PEDIDO=@ANYO_PEDIDO, @NUM_PEDIDO=@NUM_PEDIDO, @NUM_ORDEN=@NUM_ORDEN," & vbCrLf
    sConsulta = sConsulta & "   @ID_LOG=@ID_LOG, @ACCION=@ACCION, @ESTADO=@ESTADO,@FECHA_DESDE=@FECHA_DESDE,@FECHA_HASTA=@FECHA_HASTA,@TIPOFECHA=@TIPOFECHA" & vbCrLf
    sConsulta = sConsulta & "END" & vbCrLf
    sConsulta = sConsulta & "   " & vbCrLf
    sConsulta = sConsulta & "IF @ART4_ACTIVA=1" & vbCrLf
    sConsulta = sConsulta & "BEGIN" & vbCrLf
    sConsulta = sConsulta & "   SET @CONSULTA= 'INSERT INTO #T_CONSULTA SELECT " & vbCrLf
    sConsulta = sConsulta & "   LG.ERP AS ERP, LG.TABLA AS ENTIDAD, LG.ID AS IDENTIF, LG.ESTADO, LG.TEXTO, LG.ARG1, LG.ARG2,LG.FECENV, LG.FECREC, NOM_FICHERO , NOM_RECIBO," & vbCrLf
    sConsulta = sConsulta & "   L.ACCION , L.COD , LG.COD_ERP , L.USU , '''' INFO,LG.ORIGEN" & vbCrLf
    sConsulta = sConsulta & "   FROM #T_LOG_GRAL LG WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN LOG_ART4 L WITH(NOLOCK) ON LG.ID_TABLA=L.ID " & vbCrLf
    sConsulta = sConsulta & "   WHERE LG.TABLA=7' + @WHERE + @WHERECOD " & vbCrLf
    sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @CONSULTA,@PARAM,@ERP=@ERP, @TABLA=@TABLA, " & vbCrLf
    sConsulta = sConsulta & "   @COD_FS=@COD_FS, @COD_ERP=@COD_ERP, @RECEP_ERP=@RECEP_ERP," & vbCrLf
    sConsulta = sConsulta & "   @ANYO_PROCE=@ANYO_PROCE, @GMN1_PROCE=@GMN1_PROCE, @COD_PROCE=@COD_PROCE," & vbCrLf
    sConsulta = sConsulta & "   @ANYO_PEDIDO=@ANYO_PEDIDO, @NUM_PEDIDO=@NUM_PEDIDO, @NUM_ORDEN=@NUM_ORDEN," & vbCrLf
    sConsulta = sConsulta & "   @ID_LOG=@ID_LOG, @ACCION=@ACCION, @ESTADO=@ESTADO,@FECHA_DESDE=@FECHA_DESDE,@FECHA_HASTA=@FECHA_HASTA,@TIPOFECHA=@TIPOFECHA" & vbCrLf
    sConsulta = sConsulta & "END" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "IF @PROVE_ACTIVA=1" & vbCrLf
    sConsulta = sConsulta & "BEGIN" & vbCrLf
    sConsulta = sConsulta & "   SET @CONSULTA= 'INSERT INTO #T_CONSULTA SELECT " & vbCrLf
    sConsulta = sConsulta & "   LG.ERP AS ERP, LG.TABLA AS ENTIDAD, LG.ID AS IDENTIF, LG.ESTADO, LG.TEXTO, LG.ARG1, LG.ARG2,LG.FECENV, LG.FECREC, NOM_FICHERO , NOM_RECIBO," & vbCrLf
    sConsulta = sConsulta & "   L.ACCION , L.COD , LG.COD_ERP , L.USU , '''' INFO,LG.ORIGEN" & vbCrLf
    sConsulta = sConsulta & "   FROM #T_LOG_GRAL LG WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN LOG_PROVE L WITH(NOLOCK) ON LG.ID_TABLA=L.ID AND LG.TABLA=8" & vbCrLf
    sConsulta = sConsulta & "   WHERE 1=1' + @WHERE + @WHERECOD    " & vbCrLf
    sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @CONSULTA,@PARAM,@ERP=@ERP, @TABLA=@TABLA, " & vbCrLf
    sConsulta = sConsulta & "   @COD_FS=@COD_FS, @COD_ERP=@COD_ERP, @RECEP_ERP=@RECEP_ERP," & vbCrLf
    sConsulta = sConsulta & "   @ANYO_PROCE=@ANYO_PROCE, @GMN1_PROCE=@GMN1_PROCE, @COD_PROCE=@COD_PROCE," & vbCrLf
    sConsulta = sConsulta & "   @ANYO_PEDIDO=@ANYO_PEDIDO, @NUM_PEDIDO=@NUM_PEDIDO, @NUM_ORDEN=@NUM_ORDEN," & vbCrLf
    sConsulta = sConsulta & "   @ID_LOG=@ID_LOG, @ACCION=@ACCION, @ESTADO=@ESTADO,@FECHA_DESDE=@FECHA_DESDE,@FECHA_HASTA=@FECHA_HASTA,@TIPOFECHA=@TIPOFECHA" & vbCrLf
    sConsulta = sConsulta & "END" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "IF @CON_ACTIVA =1" & vbCrLf
    sConsulta = sConsulta & "BEGIN" & vbCrLf
    sConsulta = sConsulta & "   SET @CONSULTA= 'INSERT INTO #T_CONSULTA SELECT " & vbCrLf
    sConsulta = sConsulta & "   LG.ERP AS ERP, LG.TABLA AS ENTIDAD, LG.ID AS IDENTIF, LG.ESTADO, LG.TEXTO, LG.ARG1, LG.ARG2,LG.FECENV, LG.FECREC, NOM_FICHERO , NOM_RECIBO," & vbCrLf
    sConsulta = sConsulta & "   L.ACCION , L.NOM AS COD , LG.COD_ERP , L.USU , '''' INFO,LG.ORIGEN" & vbCrLf
    sConsulta = sConsulta & "   FROM #T_LOG_GRAL LG WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN LOG_CON L WITH(NOLOCK) ON LG.ID_TABLA=L.ID AND LG.TABLA=101" & vbCrLf
    sConsulta = sConsulta & "   WHERE 1=1' + @WHERE + @WHERECOD " & vbCrLf
    sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @CONSULTA,@PARAM,@ERP=@ERP, @TABLA=@TABLA, " & vbCrLf
    sConsulta = sConsulta & "   @COD_FS=@COD_FS, @COD_ERP=@COD_ERP, @RECEP_ERP=@RECEP_ERP," & vbCrLf
    sConsulta = sConsulta & "   @ANYO_PROCE=@ANYO_PROCE, @GMN1_PROCE=@GMN1_PROCE, @COD_PROCE=@COD_PROCE," & vbCrLf
    sConsulta = sConsulta & "   @ANYO_PEDIDO=@ANYO_PEDIDO, @NUM_PEDIDO=@NUM_PEDIDO, @NUM_ORDEN=@NUM_ORDEN," & vbCrLf
    sConsulta = sConsulta & "   @ID_LOG=@ID_LOG, @ACCION=@ACCION, @ESTADO=@ESTADO,@FECHA_DESDE=@FECHA_DESDE,@FECHA_HASTA=@FECHA_HASTA,@TIPOFECHA=@TIPOFECHA" & vbCrLf
    sConsulta = sConsulta & "END" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "IF @PROVE_ERP_ACTIVA =1" & vbCrLf
    sConsulta = sConsulta & "BEGIN" & vbCrLf
    sConsulta = sConsulta & "   SET @CONSULTA= 'INSERT INTO #T_CONSULTA SELECT " & vbCrLf
    sConsulta = sConsulta & "   LG.ERP AS ERP, LG.TABLA AS ENTIDAD, LG.ID AS IDENTIF, LG.ESTADO, LG.TEXTO, LG.ARG1, LG.ARG2,LG.FECENV, LG.FECREC, NOM_FICHERO , NOM_RECIBO," & vbCrLf
    sConsulta = sConsulta & "   LPE.ACCION , LPE.NIF as COD , LG.COD_ERP , LPE.USU , '''' INFO,LG.ORIGEN" & vbCrLf
    sConsulta = sConsulta & "   FROM #T_LOG_GRAL LG WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN LOG_PROVE_ERP LPE WITH(NOLOCK) ON LG.ID_TABLA=LPE.ID AND LG.TABLA=102" & vbCrLf
    sConsulta = sConsulta & "   WHERE 1=1' + @WHERE    " & vbCrLf
    sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @CONSULTA,@PARAM,@ERP=@ERP, @TABLA=@TABLA, " & vbCrLf
    sConsulta = sConsulta & "   @COD_FS=@COD_FS, @COD_ERP=@COD_ERP, @RECEP_ERP=@RECEP_ERP," & vbCrLf
    sConsulta = sConsulta & "   @ANYO_PROCE=@ANYO_PROCE, @GMN1_PROCE=@GMN1_PROCE, @COD_PROCE=@COD_PROCE," & vbCrLf
    sConsulta = sConsulta & "   @ANYO_PEDIDO=@ANYO_PEDIDO, @NUM_PEDIDO=@NUM_PEDIDO, @NUM_ORDEN=@NUM_ORDEN," & vbCrLf
    sConsulta = sConsulta & "   @ID_LOG=@ID_LOG, @ACCION=@ACCION, @ESTADO=@ESTADO,@FECHA_DESDE=@FECHA_DESDE,@FECHA_HASTA=@FECHA_HASTA,@TIPOFECHA=@TIPOFECHA" & vbCrLf
    sConsulta = sConsulta & "END" & vbCrLf
    sConsulta = sConsulta & "   " & vbCrLf
    sConsulta = sConsulta & "IF @ADJ_ACTIVA=1" & vbCrLf
    sConsulta = sConsulta & "BEGIN" & vbCrLf
    sConsulta = sConsulta & "   SET @CONSULTA= 'INSERT INTO #T_CONSULTA SELECT " & vbCrLf
    sConsulta = sConsulta & "   LG.ERP AS ERP, LG.TABLA AS ENTIDAD, LG.ID AS IDENTIF, LG.ESTADO, LG.TEXTO, LG.ARG1, LG.ARG2,LG.FECENV, LG.FECREC, NOM_FICHERO , NOM_RECIBO," & vbCrLf
    sConsulta = sConsulta & "   L.ACCION , " & vbCrLf
    sConsulta = sConsulta & "   cast(L.ANYO as varchar(4)) + ''/''+ L.GMN1_PROCE + ''/'' + cast(L.PROCE as varchar(10)) as COD, LG.cod_erp , L.USU ," & vbCrLf
    sConsulta = sConsulta & "   ISNULL(L.ART,'''') + '' - '' + L.PROVE + '' - '' + EMP.DEN AS INFO,LG.ORIGEN" & vbCrLf
    sConsulta = sConsulta & "   FROM #T_LOG_GRAL LG WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN LOG_ITEM_ADJ L WITH(NOLOCK) ON LG.ID_TABLA=L.ID AND LG.TABLA=9" & vbCrLf
    sConsulta = sConsulta & "   LEFT JOIN EMP WITH(NOLOCK) ON EMP.ID=L.EMPRESA" & vbCrLf
    sConsulta = sConsulta & "   WHERE 1=1' + @WHERE + @WHEREPROCE " & vbCrLf
    sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @CONSULTA,@PARAM,@ERP=@ERP, @TABLA=@TABLA, " & vbCrLf
    sConsulta = sConsulta & "   @COD_FS=@COD_FS, @COD_ERP=@COD_ERP, @RECEP_ERP=@RECEP_ERP," & vbCrLf
    sConsulta = sConsulta & "   @ANYO_PROCE=@ANYO_PROCE, @GMN1_PROCE=@GMN1_PROCE, @COD_PROCE=@COD_PROCE," & vbCrLf
    sConsulta = sConsulta & "   @ANYO_PEDIDO=@ANYO_PEDIDO, @NUM_PEDIDO=@NUM_PEDIDO, @NUM_ORDEN=@NUM_ORDEN," & vbCrLf
    sConsulta = sConsulta & "   @ID_LOG=@ID_LOG, @ACCION=@ACCION, @ESTADO=@ESTADO,@FECHA_DESDE=@FECHA_DESDE,@FECHA_HASTA=@FECHA_HASTA,@TIPOFECHA=@TIPOFECHA" & vbCrLf
    sConsulta = sConsulta & "END" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "IF @PEDIDO_ACTIVA = 1" & vbCrLf
    sConsulta = sConsulta & "begin" & vbCrLf
    sConsulta = sConsulta & "   SET @CONSULTA= 'INSERT INTO #T_CONSULTA SELECT " & vbCrLf
    sConsulta = sConsulta & "   LG.ERP AS ERP, LG.TABLA AS ENTIDAD, LG.ID AS IDENTIF, LG.ESTADO, LG.TEXTO, LG.ARG1, LG.ARG2,LG.FECENV, LG.FECREC, NOM_FICHERO , NOM_RECIBO," & vbCrLf
    sConsulta = sConsulta & "   LOE.ACCION , CAST(LOE.ANYO AS VARCHAR(4)) + ''/''+ CAST(LOE.NUM_PEDIDO AS VARCHAR(20)) + ''/''+ CAST(LOE.NUM_ORDEN AS VARCHAR(20)) AS COD , " & vbCrLf
    sConsulta = sConsulta & "   LG.COD_ERP , LOE.USU , '''' INFO,LG.ORIGEN" & vbCrLf
    sConsulta = sConsulta & "   FROM #T_LOG_GRAL LG WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN LOG_ORDEN_ENTREGA LOE WITH(NOLOCK) ON LG.ID_TABLA=LOE.ID '" & vbCrLf
    sConsulta = sConsulta & "   IF @TABLA=11 " & vbCrLf
    sConsulta = sConsulta & "       SET @CONSULTA= @CONSULTA + 'AND LG.TABLA=11 '" & vbCrLf
    sConsulta = sConsulta & "   ELSE IF @TABLA=13  " & vbCrLf
    sConsulta = sConsulta & "       SET @CONSULTA= @CONSULTA +  'AND  LG.TABLA=13 '" & vbCrLf
    sConsulta = sConsulta & "   ELSE" & vbCrLf
    sConsulta = sConsulta & "       SET @CONSULTA= @CONSULTA + 'AND (LG.TABLA=11 or LG.TABLA=13)'" & vbCrLf
    sConsulta = sConsulta & "   SET @CONSULTA= @CONSULTA + ' WHERE 1=1' + @WHERE + @WHEREPEDIDO " & vbCrLf
    sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @CONSULTA,@PARAM,@ERP=@ERP, @TABLA=@TABLA, " & vbCrLf
    sConsulta = sConsulta & "   @COD_FS=@COD_FS, @COD_ERP=@COD_ERP, @RECEP_ERP=@RECEP_ERP," & vbCrLf
    sConsulta = sConsulta & "   @ANYO_PROCE=@ANYO_PROCE, @GMN1_PROCE=@GMN1_PROCE, @COD_PROCE=@COD_PROCE," & vbCrLf
    sConsulta = sConsulta & "   @ANYO_PEDIDO=@ANYO_PEDIDO, @NUM_PEDIDO=@NUM_PEDIDO, @NUM_ORDEN=@NUM_ORDEN," & vbCrLf
    sConsulta = sConsulta & "   @ID_LOG=@ID_LOG, @ACCION=@ACCION, @ESTADO=@ESTADO,@FECHA_DESDE=@FECHA_DESDE,@FECHA_HASTA=@FECHA_HASTA,@TIPOFECHA=@TIPOFECHA" & vbCrLf
    sConsulta = sConsulta & "end" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "IF @RECEP_ACTIVA = 1" & vbCrLf
    sConsulta = sConsulta & "  begin" & vbCrLf
    sConsulta = sConsulta & "   SET @CONSULTA= 'INSERT INTO #T_CONSULTA SELECT " & vbCrLf
    sConsulta = sConsulta & "   LG.ERP AS ERP, LG.TABLA AS ENTIDAD, LG.ID AS IDENTIF, LG.ESTADO, LG.TEXTO, LG.ARG1, LG.ARG2,LG.FECENV, LG.FECREC, NOM_FICHERO , NOM_RECIBO," & vbCrLf
    sConsulta = sConsulta & "   LR.ACCION , LR.ALBARAN AS COD, LG.COD_ERP , LR.USU , " & vbCrLf
    sConsulta = sConsulta & "   CAST(O.ANYO AS VARCHAR(4)) + ''/'' + CAST(P.NUM AS VARCHAR(20))  + ''/'' + CAST(O.NUM AS VARCHAR(20)) + ''  -  '' + O.NUM_PED_ERP AS INFO,LG.ORIGEN" & vbCrLf
    sConsulta = sConsulta & "   FROM #T_LOG_GRAL LG WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN LOG_PEDIDO_RECEP LR WITH(NOLOCK) ON LG.ID_TABLA=LR.ID '" & vbCrLf
    sConsulta = sConsulta & "   IF @TABLA=12 " & vbCrLf
    sConsulta = sConsulta & "       SET @CONSULTA= @CONSULTA + 'AND LG.TABLA=12 '" & vbCrLf
    sConsulta = sConsulta & "   ELSE IF @TABLA=13  " & vbCrLf
    sConsulta = sConsulta & "       SET @CONSULTA= @CONSULTA +  'AND LG.TABLA=14 '" & vbCrLf
    sConsulta = sConsulta & "   ELSE" & vbCrLf
    sConsulta = sConsulta & "       SET @CONSULTA= @CONSULTA + 'AND (LG.TABLA=12 or LG.TABLA=14)'" & vbCrLf
    sConsulta = sConsulta & "   SET @CONSULTA= @CONSULTA + '   LEFT JOIN ORDEN_ENTREGA O WITH(NOLOCK) ON O.ID=LR.ID_ORDEN_ENTREGA" & vbCrLf
    sConsulta = sConsulta & "   LEFT JOIN PEDIDO P WITH(NOLOCK) ON P.ID=O.PEDIDO AND P.ID=LR.ID_PEDIDO" & vbCrLf
    sConsulta = sConsulta & "   WHERE 1=1' + @WHERE + @WHERECOD + @WHEREPEDIDO_ENRECEP  " & vbCrLf
    sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @CONSULTA,@PARAM,@ERP=@ERP, @TABLA=@TABLA, " & vbCrLf
    sConsulta = sConsulta & "   @COD_FS=@COD_FS, @COD_ERP=@COD_ERP, @RECEP_ERP=@RECEP_ERP," & vbCrLf
    sConsulta = sConsulta & "   @ANYO_PROCE=@ANYO_PROCE, @GMN1_PROCE=@GMN1_PROCE, @COD_PROCE=@COD_PROCE," & vbCrLf
    sConsulta = sConsulta & "   @ANYO_PEDIDO=@ANYO_PEDIDO, @NUM_PEDIDO=@NUM_PEDIDO, @NUM_ORDEN=@NUM_ORDEN," & vbCrLf
    sConsulta = sConsulta & "   @ID_LOG=@ID_LOG, @ACCION=@ACCION, @ESTADO=@ESTADO,@FECHA_DESDE=@FECHA_DESDE,@FECHA_HASTA=@FECHA_HASTA,@TIPOFECHA=@TIPOFECHA" & vbCrLf
    sConsulta = sConsulta & "END" & vbCrLf
    sConsulta = sConsulta & "   " & vbCrLf
    sConsulta = sConsulta & "IF @ESTRMAT_ACTIVA = 1" & vbCrLf
    sConsulta = sConsulta & "BEGIN" & vbCrLf
    sConsulta = sConsulta & "   SET @CONSULTA= 'INSERT INTO #T_CONSULTA SELECT " & vbCrLf
    sConsulta = sConsulta & "   LG.ERP AS ERP, LG.TABLA AS ENTIDAD, LG.ID AS IDENTIF, LG.ESTADO, LG.TEXTO, LG.ARG1, LG.ARG2,LG.FECENV, LG.FECREC, NOM_FICHERO , NOM_RECIBO," & vbCrLf
    sConsulta = sConsulta & "   LGMN.ACCION, LGMN.GMN1 + ''-'' + LGMN.GMN2 + ''-'' + LGMN.GMN3 + ''-'' + LGMN.GMN4 AS COD , LG.COD_ERP , " & vbCrLf
    sConsulta = sConsulta & "   LGMN.USU , '''' INFO,LG.ORIGEN" & vbCrLf
    sConsulta = sConsulta & "   FROM #T_LOG_GRAL LG WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN LOG_GMN LGMN WITH(NOLOCK) ON LG.ID_TABLA=LGMN.ID AND LG.TABLA=15" & vbCrLf
    sConsulta = sConsulta & "   WHERE 1=1' + @WHERE + @WHERECOD" & vbCrLf
    sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @CONSULTA,@PARAM,@ERP=@ERP, @TABLA=@TABLA, " & vbCrLf
    sConsulta = sConsulta & "   @COD_FS=@COD_FS, @COD_ERP=@COD_ERP, @RECEP_ERP=@RECEP_ERP," & vbCrLf
    sConsulta = sConsulta & "   @ANYO_PROCE=@ANYO_PROCE, @GMN1_PROCE=@GMN1_PROCE, @COD_PROCE=@COD_PROCE," & vbCrLf
    sConsulta = sConsulta & "   @ANYO_PEDIDO=@ANYO_PEDIDO, @NUM_PEDIDO=@NUM_PEDIDO, @NUM_ORDEN=@NUM_ORDEN," & vbCrLf
    sConsulta = sConsulta & "   @ID_LOG=@ID_LOG, @ACCION=@ACCION, @ESTADO=@ESTADO,@FECHA_DESDE=@FECHA_DESDE,@FECHA_HASTA=@FECHA_HASTA,@TIPOFECHA=@TIPOFECHA" & vbCrLf
    sConsulta = sConsulta & "END" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "IF @CONSUMOS_ACTIVA = 1" & vbCrLf
    sConsulta = sConsulta & "BEGIN" & vbCrLf
    sConsulta = sConsulta & "   SET @CONSULTA= 'INSERT INTO #T_CONSULTA SELECT " & vbCrLf
    sConsulta = sConsulta & "   LG.ERP AS ERP, LG.TABLA AS ENTIDAD, LG.ID AS IDENTIF, LG.ESTADO, LG.TEXTO, LG.ARG1, LG.ARG2,LG.FECENV, LG.FECREC, NOM_FICHERO , NOM_RECIBO," & vbCrLf
    sConsulta = sConsulta & "   L.ACCION , L.COD , LG.COD_ERP , L.USU , '''' INFO,LG.ORIGEN" & vbCrLf
    sConsulta = sConsulta & "   FROM #T_LOG_GRAL LG WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN LOG_PRES_ART L WITH(NOLOCK) ON LG.ID_TABLA=L.ID AND LG.TABLA=16" & vbCrLf
    sConsulta = sConsulta & "   WHERE 1=1' + @WHERE + @WHERECOD      " & vbCrLf
    sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @CONSULTA,@PARAM,@ERP=@ERP, @TABLA=@TABLA, " & vbCrLf
    sConsulta = sConsulta & "   @COD_FS=@COD_FS, @COD_ERP=@COD_ERP, @RECEP_ERP=@RECEP_ERP," & vbCrLf
    sConsulta = sConsulta & "   @ANYO_PROCE=@ANYO_PROCE, @GMN1_PROCE=@GMN1_PROCE, @COD_PROCE=@COD_PROCE," & vbCrLf
    sConsulta = sConsulta & "   @ANYO_PEDIDO=@ANYO_PEDIDO, @NUM_PEDIDO=@NUM_PEDIDO, @NUM_ORDEN=@NUM_ORDEN," & vbCrLf
    sConsulta = sConsulta & "   @ID_LOG=@ID_LOG, @ACCION=@ACCION, @ESTADO=@ESTADO,@FECHA_DESDE=@FECHA_DESDE,@FECHA_HASTA=@FECHA_HASTA,@TIPOFECHA=@TIPOFECHA" & vbCrLf
    sConsulta = sConsulta & "END" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "IF @VARCAL_ACTIVA = 1" & vbCrLf
    sConsulta = sConsulta & "BEGIN" & vbCrLf
    sConsulta = sConsulta & "   SET @CONSULTA= 'INSERT INTO #T_CONSULTA SELECT " & vbCrLf
    sConsulta = sConsulta & "   LG.ERP AS ERP, LG.TABLA AS ENTIDAD, LG.ID AS IDENTIF, LG.ESTADO, LG.TEXTO, LG.ARG1, LG.ARG2,LG.FECENV, LG.FECREC, NOM_FICHERO , NOM_RECIBO," & vbCrLf
    sConsulta = sConsulta & "   L.ACCION , L.COD_CAL1 cod , LG.COD_ERP , L.USU , '''' INFO,LG.ORIGEN" & vbCrLf
    sConsulta = sConsulta & "   FROM #T_LOG_GRAL LG WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN LOG_VAR_CAL L WITH(NOLOCK) ON LG.ID_TABLA=L.ID AND LG.TABLA=17" & vbCrLf
    sConsulta = sConsulta & "   WHERE 1=1' + @WHERE + @WHERECOD    " & vbCrLf
    sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @CONSULTA,@PARAM,@ERP=@ERP, @TABLA=@TABLA, " & vbCrLf
    sConsulta = sConsulta & "   @COD_FS=@COD_FS, @COD_ERP=@COD_ERP, @RECEP_ERP=@RECEP_ERP," & vbCrLf
    sConsulta = sConsulta & "   @ANYO_PROCE=@ANYO_PROCE, @GMN1_PROCE=@GMN1_PROCE, @COD_PROCE=@COD_PROCE," & vbCrLf
    sConsulta = sConsulta & "   @ANYO_PEDIDO=@ANYO_PEDIDO, @NUM_PEDIDO=@NUM_PEDIDO, @NUM_ORDEN=@NUM_ORDEN," & vbCrLf
    sConsulta = sConsulta & "   @ID_LOG=@ID_LOG, @ACCION=@ACCION, @ESTADO=@ESTADO,@FECHA_DESDE=@FECHA_DESDE,@FECHA_HASTA=@FECHA_HASTA,@TIPOFECHA=@TIPOFECHA" & vbCrLf
    sConsulta = sConsulta & "END" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "IF @SOLICIT_ACTIVA = 1" & vbCrLf
    sConsulta = sConsulta & "BEGIN" & vbCrLf
    sConsulta = sConsulta & "   SET @CONSULTA= 'INSERT INTO #T_CONSULTA SELECT " & vbCrLf
    sConsulta = sConsulta & "   LG.ERP AS ERP, LG.TABLA AS ENTIDAD, LG.ID AS IDENTIF, LG.ESTADO, LG.TEXTO, LG.ARG1, LG.ARG2,LG.FECENV, LG.FECREC, NOM_FICHERO , NOM_RECIBO," & vbCrLf
    sConsulta = sConsulta & "   L.ACCION , CAST(L.INSTANCIA AS VARCHAR(20)) as COD , LG.COD_ERP , L.USU  , SOLICITUD.COD AS INFO,LG.ORIGEN" & vbCrLf
    sConsulta = sConsulta & "   FROM #T_LOG_GRAL LG WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN LOG_INSTANCIA L WITH(NOLOCK) ON LG.ID_TABLA=L.ID AND LG.TABLA=18" & vbCrLf
    sConsulta = sConsulta & "   LEFT JOIN SOLICITUD WITH(NOLOCK) ON SOLICITUD.ID=L.SOLICITUD" & vbCrLf
    sConsulta = sConsulta & "   WHERE 1=1' + @WHERE + @WHERECOD " & vbCrLf
    sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @CONSULTA,@PARAM,@ERP=@ERP, @TABLA=@TABLA, " & vbCrLf
    sConsulta = sConsulta & "   @COD_FS=@COD_FS, @COD_ERP=@COD_ERP, @RECEP_ERP=@RECEP_ERP," & vbCrLf
    sConsulta = sConsulta & "   @ANYO_PROCE=@ANYO_PROCE, @GMN1_PROCE=@GMN1_PROCE, @COD_PROCE=@COD_PROCE," & vbCrLf
    sConsulta = sConsulta & "   @ANYO_PEDIDO=@ANYO_PEDIDO, @NUM_PEDIDO=@NUM_PEDIDO, @NUM_ORDEN=@NUM_ORDEN," & vbCrLf
    sConsulta = sConsulta & "   @ID_LOG=@ID_LOG, @ACCION=@ACCION, @ESTADO=@ESTADO,@FECHA_DESDE=@FECHA_DESDE,@FECHA_HASTA=@FECHA_HASTA,@TIPOFECHA=@TIPOFECHA" & vbCrLf
    sConsulta = sConsulta & "END" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "IF @PRES_ACTIVA = 1" & vbCrLf
    sConsulta = sConsulta & "BEGIN" & vbCrLf
    sConsulta = sConsulta & "   SET @CONSULTA= 'INSERT INTO #T_CONSULTA SELECT " & vbCrLf
    sConsulta = sConsulta & "   LG.ERP AS ERP, LG.TABLA AS ENTIDAD, LG.ID AS IDENTIF, LG.ESTADO, LG.TEXTO, LG.ARG1, LG.ARG2,LG.FECENV, LG.FECREC, NOM_FICHERO , NOM_RECIBO," & vbCrLf
    sConsulta = sConsulta & "   L.ACCION , L.PRES1 + ''-'' + L.PRES2 + ''-''  + L.PRES3 + ''-'' + L.PRES4 AS COD, LG.COD_ERP , L.USU  , '''' INFO,LG.ORIGEN" & vbCrLf
    sConsulta = sConsulta & "   FROM #T_LOG_GRAL LG WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN LOG_PRES L WITH(NOLOCK) ON LG.ID_TABLA=L.ID AND (LG.TABLA=19 or LG.TABLA=20 or LG.TABLA=21 or LG.TABLA=22)" & vbCrLf
    sConsulta = sConsulta & "   WHERE 1=1' + @WHERE  + @WHERECOD   " & vbCrLf
    sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @CONSULTA,@PARAM,@ERP=@ERP, @TABLA=@TABLA, " & vbCrLf
    sConsulta = sConsulta & "   @COD_FS=@COD_FS, @COD_ERP=@COD_ERP, @RECEP_ERP=@RECEP_ERP," & vbCrLf
    sConsulta = sConsulta & "   @ANYO_PROCE=@ANYO_PROCE, @GMN1_PROCE=@GMN1_PROCE, @COD_PROCE=@COD_PROCE," & vbCrLf
    sConsulta = sConsulta & "   @ANYO_PEDIDO=@ANYO_PEDIDO, @NUM_PEDIDO=@NUM_PEDIDO, @NUM_ORDEN=@NUM_ORDEN," & vbCrLf
    sConsulta = sConsulta & "   @ID_LOG=@ID_LOG, @ACCION=@ACCION, @ESTADO=@ESTADO,@FECHA_DESDE=@FECHA_DESDE,@FECHA_HASTA=@FECHA_HASTA,@TIPOFECHA=@TIPOFECHA" & vbCrLf
    sConsulta = sConsulta & "END" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "IF @PROCE_ACTIVA = 1" & vbCrLf
    sConsulta = sConsulta & "BEGIN" & vbCrLf
    sConsulta = sConsulta & "   SET @CONSULTA= 'INSERT INTO #T_CONSULTA SELECT " & vbCrLf
    sConsulta = sConsulta & "   LG.ERP AS ERP, LG.TABLA AS ENTIDAD, LG.ID AS IDENTIF, LG.ESTADO, LG.TEXTO, LG.ARG1, LG.ARG2,LG.FECENV, LG.FECREC, NOM_FICHERO , NOM_RECIBO," & vbCrLf
    sConsulta = sConsulta & "   L.ACCION , CAST(L.ANYO AS VARCHAR(4)) + ''-'' + L.GMN1 + ''-'' + CAST(L.COD AS VARCHAR(10)) COD ," & vbCrLf
    sConsulta = sConsulta & "    LG.COD_ERP , L.USU , '''' INFO ,LG.ORIGEN" & vbCrLf
    sConsulta = sConsulta & "   FROM #T_LOG_GRAL LG WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN LOG_PROCE L WITH(NOLOCK) ON LG.ID_TABLA=L.ID AND LG.TABLA=23" & vbCrLf
    sConsulta = sConsulta & "   WHERE 1=1' + @WHERE + @WHEREPROCE" & vbCrLf
    sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @CONSULTA,@PARAM,@ERP=@ERP, @TABLA=@TABLA, " & vbCrLf
    sConsulta = sConsulta & "   @COD_FS=@COD_FS, @COD_ERP=@COD_ERP, @RECEP_ERP=@RECEP_ERP," & vbCrLf
    sConsulta = sConsulta & "   @ANYO_PROCE=@ANYO_PROCE, @GMN1_PROCE=@GMN1_PROCE, @COD_PROCE=@COD_PROCE," & vbCrLf
    sConsulta = sConsulta & "   @ANYO_PEDIDO=@ANYO_PEDIDO, @NUM_PEDIDO=@NUM_PEDIDO, @NUM_ORDEN=@NUM_ORDEN," & vbCrLf
    sConsulta = sConsulta & "   @ID_LOG=@ID_LOG, @ACCION=@ACCION, @ESTADO=@ESTADO,@FECHA_DESDE=@FECHA_DESDE,@FECHA_HASTA=@FECHA_HASTA,@TIPOFECHA=@TIPOFECHA" & vbCrLf
    sConsulta = sConsulta & "END" & vbCrLf
    sConsulta = sConsulta & "   " & vbCrLf
    sConsulta = sConsulta & "IF @VIA_PAG_ACTIVA = 1" & vbCrLf
    sConsulta = sConsulta & "BEGIN" & vbCrLf
    sConsulta = sConsulta & "   SET @CONSULTA= 'INSERT INTO #T_CONSULTA SELECT " & vbCrLf
    sConsulta = sConsulta & "   LG.ERP AS ERP, LG.TABLA AS ENTIDAD, LG.ID AS IDENTIF, LG.ESTADO, LG.TEXTO, LG.ARG1, LG.ARG2,LG.FECENV, LG.FECREC, NOM_FICHERO , NOM_RECIBO," & vbCrLf
    sConsulta = sConsulta & "   L.ACCION , L.COD , LG.COD_ERP , L.USU , '''' INFO,LG.ORIGEN" & vbCrLf
    sConsulta = sConsulta & "   FROM #T_LOG_GRAL LG WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN LOG_VIA_PAG L WITH(NOLOCK) ON LG.ID_TABLA=L.ID AND LG.TABLA=25" & vbCrLf
    sConsulta = sConsulta & "   WHERE 1=1' + @WHERE + @WHERECOD " & vbCrLf
    sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @CONSULTA,@PARAM,@ERP=@ERP, @TABLA=@TABLA, " & vbCrLf
    sConsulta = sConsulta & "   @COD_FS=@COD_FS, @COD_ERP=@COD_ERP, @RECEP_ERP=@RECEP_ERP," & vbCrLf
    sConsulta = sConsulta & "   @ANYO_PROCE=@ANYO_PROCE, @GMN1_PROCE=@GMN1_PROCE, @COD_PROCE=@COD_PROCE," & vbCrLf
    sConsulta = sConsulta & "   @ANYO_PEDIDO=@ANYO_PEDIDO, @NUM_PEDIDO=@NUM_PEDIDO, @NUM_ORDEN=@NUM_ORDEN," & vbCrLf
    sConsulta = sConsulta & "   @ID_LOG=@ID_LOG, @ACCION=@ACCION, @ESTADO=@ESTADO,@FECHA_DESDE=@FECHA_DESDE,@FECHA_HASTA=@FECHA_HASTA,@TIPOFECHA=@TIPOFECHA" & vbCrLf
    sConsulta = sConsulta & "END" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "IF @PPM_ACTIVA = 1" & vbCrLf
    sConsulta = sConsulta & "BEGIN" & vbCrLf
    sConsulta = sConsulta & "   SET @CONSULTA= 'INSERT INTO #T_CONSULTA SELECT " & vbCrLf
    sConsulta = sConsulta & "   LG.ERP AS ERP, LG.TABLA AS ENTIDAD, LG.ID AS IDENTIF, LG.ESTADO, LG.TEXTO, LG.ARG1, LG.ARG2,LG.FECENV, LG.FECREC, NOM_FICHERO , NOM_RECIBO," & vbCrLf
    sConsulta = sConsulta & "   LPPM.ACCION , " & vbCrLf
    sConsulta = sConsulta & "   cast(ROUND(LPPM.piezas_servidas,5) as nvarchar(100))  AS COD , " & vbCrLf
    sConsulta = sConsulta & "   LG.COD_ERP , LPPM.USU  ," & vbCrLf
    sConsulta = sConsulta & "   unqa.cod + '' - '' + LPPM.prove +  + '' - '' + LPPM.art4  AS INFO,LG.ORIGEN" & vbCrLf
    sConsulta = sConsulta & "   FROM #T_LOG_GRAL LG WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN LOG_PPM LPPM WITH(NOLOCK) ON LG.ID_TABLA=LPPM.ID AND LG.TABLA=26" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN UNQA WITH(NOLOCK) ON UNQA.ID=LPPM.UONQA" & vbCrLf
    sConsulta = sConsulta & "   WHERE 1=1' + @WHERE " & vbCrLf
    sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @CONSULTA,@PARAM,@ERP=@ERP, @TABLA=@TABLA, " & vbCrLf
    sConsulta = sConsulta & "   @COD_FS=@COD_FS, @COD_ERP=@COD_ERP, @RECEP_ERP=@RECEP_ERP," & vbCrLf
    sConsulta = sConsulta & "   @ANYO_PROCE=@ANYO_PROCE, @GMN1_PROCE=@GMN1_PROCE, @COD_PROCE=@COD_PROCE," & vbCrLf
    sConsulta = sConsulta & "   @ANYO_PEDIDO=@ANYO_PEDIDO, @NUM_PEDIDO=@NUM_PEDIDO, @NUM_ORDEN=@NUM_ORDEN," & vbCrLf
    sConsulta = sConsulta & "   @ID_LOG=@ID_LOG, @ACCION=@ACCION, @ESTADO=@ESTADO,@FECHA_DESDE=@FECHA_DESDE,@FECHA_HASTA=@FECHA_HASTA,@TIPOFECHA=@TIPOFECHA" & vbCrLf
    sConsulta = sConsulta & "END" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "IF @TASA_SERVICIO_ACTIVA = 1" & vbCrLf
    sConsulta = sConsulta & "BEGIN" & vbCrLf
    sConsulta = sConsulta & "   SET @CONSULTA= 'INSERT INTO #T_CONSULTA SELECT " & vbCrLf
    sConsulta = sConsulta & "   LG.ERP AS ERP, LG.TABLA AS ENTIDAD, LG.ID AS IDENTIF, LG.ESTADO, LG.TEXTO, LG.ARG1, LG.ARG2,LG.FECENV, LG.FECREC, NOM_FICHERO , NOM_RECIBO," & vbCrLf
    sConsulta = sConsulta & "   LTS.ACCION , " & vbCrLf
    sConsulta = sConsulta & "   cast(ROUND(LTS.TOTAL_ENVIOS,5) as nvarchar(100))  as COD , LG.COD_ERP , " & vbCrLf
    sConsulta = sConsulta & "   LTS.USU , " & vbCrLf
    sConsulta = sConsulta & "   unqa.cod + '' - '' + LTS.prove +  + '' - '' + LTS.art4  AS INFO,LG.ORIGEN" & vbCrLf
    sConsulta = sConsulta & "   FROM #T_LOG_GRAL LG WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN LOG_TASA_SERVICIO LTS WITH(NOLOCK) ON LG.ID_TABLA=LTS.ID AND LG.TABLA=27" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN UNQA WITH(NOLOCK) ON UNQA.ID=LTS.UONQA" & vbCrLf
    sConsulta = sConsulta & "   WHERE 1=1' + @WHERE " & vbCrLf
    sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @CONSULTA,@PARAM,@ERP=@ERP, @TABLA=@TABLA, " & vbCrLf
    sConsulta = sConsulta & "   @COD_FS=@COD_FS, @COD_ERP=@COD_ERP, @RECEP_ERP=@RECEP_ERP," & vbCrLf
    sConsulta = sConsulta & "   @ANYO_PROCE=@ANYO_PROCE, @GMN1_PROCE=@GMN1_PROCE, @COD_PROCE=@COD_PROCE," & vbCrLf
    sConsulta = sConsulta & "   @ANYO_PEDIDO=@ANYO_PEDIDO, @NUM_PEDIDO=@NUM_PEDIDO, @NUM_ORDEN=@NUM_ORDEN," & vbCrLf
    sConsulta = sConsulta & "   @ID_LOG=@ID_LOG, @ACCION=@ACCION, @ESTADO=@ESTADO,@FECHA_DESDE=@FECHA_DESDE,@FECHA_HASTA=@FECHA_HASTA,@TIPOFECHA=@TIPOFECHA" & vbCrLf
    sConsulta = sConsulta & "END" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "IF @CARGO_PROVE_ACTIVA=1" & vbCrLf
    sConsulta = sConsulta & "BEGIN" & vbCrLf
    sConsulta = sConsulta & "   SET @CONSULTA= 'INSERT INTO #T_CONSULTA SELECT " & vbCrLf
    sConsulta = sConsulta & "   LG.ERP AS ERP, LG.TABLA AS ENTIDAD, LG.ID AS IDENTIF, LG.ESTADO, LG.TEXTO, LG.ARG1, LG.ARG2,LG.FECENV, LG.FECREC, NOM_FICHERO , NOM_RECIBO," & vbCrLf
    sConsulta = sConsulta & "   LCP.ACCION , cast(round(LCP.importe_facturado,5) as nvarchar(100))  COD , LG.COD_ERP , " & vbCrLf
    sConsulta = sConsulta & "   LCP.USU , " & vbCrLf
    sConsulta = sConsulta & "   unqa.cod + '' - '' + LCP.prove + '' - '' + LCP.art4 as INFO,LG.ORIGEN" & vbCrLf
    sConsulta = sConsulta & "   FROM #T_LOG_GRAL LG WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN LOG_CARGO_PROVEEDORES LCP WITH(NOLOCK) ON LG.ID_TABLA=LCP.ID AND LG.TABLA=28" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN UNQA WITH(NOLOCK) ON UNQA.ID=LCP.UONQA" & vbCrLf
    sConsulta = sConsulta & "   WHERE 1=1' + @WHERE " & vbCrLf
    sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @CONSULTA,@PARAM,@ERP=@ERP, @TABLA=@TABLA, " & vbCrLf
    sConsulta = sConsulta & "   @COD_FS=@COD_FS, @COD_ERP=@COD_ERP, @RECEP_ERP=@RECEP_ERP," & vbCrLf
    sConsulta = sConsulta & "   @ANYO_PROCE=@ANYO_PROCE, @GMN1_PROCE=@GMN1_PROCE, @COD_PROCE=@COD_PROCE," & vbCrLf
    sConsulta = sConsulta & "   @ANYO_PEDIDO=@ANYO_PEDIDO, @NUM_PEDIDO=@NUM_PEDIDO, @NUM_ORDEN=@NUM_ORDEN," & vbCrLf
    sConsulta = sConsulta & "   @ID_LOG=@ID_LOG, @ACCION=@ACCION, @ESTADO=@ESTADO,@FECHA_DESDE=@FECHA_DESDE,@FECHA_HASTA=@FECHA_HASTA,@TIPOFECHA=@TIPOFECHA" & vbCrLf
    sConsulta = sConsulta & "END" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "IF @UON_ACTIVA=1" & vbCrLf
    sConsulta = sConsulta & "BEGIN" & vbCrLf
    sConsulta = sConsulta & "   SET @CONSULTA= 'INSERT INTO #T_CONSULTA SELECT " & vbCrLf
    sConsulta = sConsulta & "   LG.ERP AS ERP, LG.TABLA AS ENTIDAD, LG.ID AS IDENTIF, LG.ESTADO, LG.TEXTO, LG.ARG1, LG.ARG2,LG.FECENV, LG.FECREC, NOM_FICHERO , NOM_RECIBO," & vbCrLf
    sConsulta = sConsulta & "   L.ACCION , L.UON1 + ''-'' + L.UON2 + ''-'' + L.UON3 + ''-'' + L.UON4 COD, LG.COD_ERP , L.USU , '''' INFO,LG.ORIGEN" & vbCrLf
    sConsulta = sConsulta & "   FROM #T_LOG_GRAL LG WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN LOG_UON L WITH(NOLOCK) ON LG.ID_TABLA=L.ID AND LG.TABLA=29" & vbCrLf
    sConsulta = sConsulta & "   WHERE 1=1' + @WHERE " & vbCrLf
    sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @CONSULTA,@PARAM,@ERP=@ERP, @TABLA=@TABLA, " & vbCrLf
    sConsulta = sConsulta & "   @COD_FS=@COD_FS, @COD_ERP=@COD_ERP, @RECEP_ERP=@RECEP_ERP," & vbCrLf
    sConsulta = sConsulta & "   @ANYO_PROCE=@ANYO_PROCE, @GMN1_PROCE=@GMN1_PROCE, @COD_PROCE=@COD_PROCE," & vbCrLf
    sConsulta = sConsulta & "   @ANYO_PEDIDO=@ANYO_PEDIDO, @NUM_PEDIDO=@NUM_PEDIDO, @NUM_ORDEN=@NUM_ORDEN," & vbCrLf
    sConsulta = sConsulta & "   @ID_LOG=@ID_LOG, @ACCION=@ACCION, @ESTADO=@ESTADO,@FECHA_DESDE=@FECHA_DESDE,@FECHA_HASTA=@FECHA_HASTA,@TIPOFECHA=@TIPOFECHA" & vbCrLf
    sConsulta = sConsulta & "END" & vbCrLf
    sConsulta = sConsulta & "   " & vbCrLf
    sConsulta = sConsulta & "IF @USU_ACTIVA=1" & vbCrLf
    sConsulta = sConsulta & "BEGIN" & vbCrLf
    sConsulta = sConsulta & "   SET @CONSULTA= 'INSERT INTO #T_CONSULTA SELECT " & vbCrLf
    sConsulta = sConsulta & "   LG.ERP AS ERP, LG.TABLA AS ENTIDAD, LG.ID AS IDENTIF, LG.ESTADO, LG.TEXTO, LG.ARG1, LG.ARG2,LG.FECENV, LG.FECREC, NOM_FICHERO , NOM_RECIBO," & vbCrLf
    sConsulta = sConsulta & "   L.ACCION , L.COD , LG.COD_ERP , L.USU  , '''' INFO,LG.ORIGEN" & vbCrLf
    sConsulta = sConsulta & "   FROM #T_LOG_GRAL LG WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN LOG_USU L WITH(NOLOCK) ON LG.ID_TABLA=L.ID AND LG.TABLA=30" & vbCrLf
    sConsulta = sConsulta & "   WHERE 1=1' + @WHERE + @WHERECOD " & vbCrLf
    sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @CONSULTA,@PARAM,@ERP=@ERP, @TABLA=@TABLA, " & vbCrLf
    sConsulta = sConsulta & "   @COD_FS=@COD_FS, @COD_ERP=@COD_ERP, @RECEP_ERP=@RECEP_ERP," & vbCrLf
    sConsulta = sConsulta & "   @ANYO_PROCE=@ANYO_PROCE, @GMN1_PROCE=@GMN1_PROCE, @COD_PROCE=@COD_PROCE," & vbCrLf
    sConsulta = sConsulta & "   @ANYO_PEDIDO=@ANYO_PEDIDO, @NUM_PEDIDO=@NUM_PEDIDO, @NUM_ORDEN=@NUM_ORDEN," & vbCrLf
    sConsulta = sConsulta & "   @ID_LOG=@ID_LOG, @ACCION=@ACCION, @ESTADO=@ESTADO,@FECHA_DESDE=@FECHA_DESDE,@FECHA_HASTA=@FECHA_HASTA,@TIPOFECHA=@TIPOFECHA" & vbCrLf
    sConsulta = sConsulta & "END" & vbCrLf
    sConsulta = sConsulta & "   " & vbCrLf
    sConsulta = sConsulta & "IF @ACTIVO_ACTIVA=1" & vbCrLf
    sConsulta = sConsulta & "BEGIN" & vbCrLf
    sConsulta = sConsulta & "   SET @CONSULTA= 'INSERT INTO #T_CONSULTA SELECT " & vbCrLf
    sConsulta = sConsulta & "   LG.ERP AS ERP, LG.TABLA AS ENTIDAD, LG.ID AS IDENTIF, LG.ESTADO, LG.TEXTO, LG.ARG1, LG.ARG2,LG.FECENV, LG.FECREC, NOM_FICHERO , NOM_RECIBO," & vbCrLf
    sConsulta = sConsulta & "   L.ACCION , L.COD , LG.COD_ERP , L.USU  , '''' INFO,LG.ORIGEN" & vbCrLf
    sConsulta = sConsulta & "   FROM #T_LOG_GRAL LG WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN LOG_ACTIVO L WITH(NOLOCK) ON LG.ID_TABLA=L.ID AND LG.TABLA=31" & vbCrLf
    sConsulta = sConsulta & "   WHERE 1=1' + @WHERE + @WHERECOD " & vbCrLf
    sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @CONSULTA,@PARAM,@ERP=@ERP, @TABLA=@TABLA, " & vbCrLf
    sConsulta = sConsulta & "   @COD_FS=@COD_FS, @COD_ERP=@COD_ERP, @RECEP_ERP=@RECEP_ERP," & vbCrLf
    sConsulta = sConsulta & "   @ANYO_PROCE=@ANYO_PROCE, @GMN1_PROCE=@GMN1_PROCE, @COD_PROCE=@COD_PROCE," & vbCrLf
    sConsulta = sConsulta & "   @ANYO_PEDIDO=@ANYO_PEDIDO, @NUM_PEDIDO=@NUM_PEDIDO, @NUM_ORDEN=@NUM_ORDEN," & vbCrLf
    sConsulta = sConsulta & "   @ID_LOG=@ID_LOG, @ACCION=@ACCION, @ESTADO=@ESTADO,@FECHA_DESDE=@FECHA_DESDE,@FECHA_HASTA=@FECHA_HASTA,@TIPOFECHA=@TIPOFECHA" & vbCrLf
    sConsulta = sConsulta & "END" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "IF @FACTURA_ACTIVA=1" & vbCrLf
    sConsulta = sConsulta & "BEGIN" & vbCrLf
    sConsulta = sConsulta & "   SET @CONSULTA= 'INSERT INTO #T_CONSULTA SELECT " & vbCrLf
    sConsulta = sConsulta & "   LG.ERP AS ERP, LG.TABLA AS ENTIDAD, LG.ID AS IDENTIF, LG.ESTADO, LG.TEXTO, LG.ARG1, LG.ARG2,LG.FECENV, LG.FECREC, NOM_FICHERO , NOM_RECIBO," & vbCrLf
    sConsulta = sConsulta & "   L.ACCION , L.NUM AS COD , LG.COD_ERP , L.USU  , '''' INFO,LG.ORIGEN" & vbCrLf
    sConsulta = sConsulta & "   FROM #T_LOG_GRAL LG WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN LOG_FACTURA L WITH(NOLOCK) ON LG.ID_TABLA=L.ID AND LG.TABLA=32" & vbCrLf
    sConsulta = sConsulta & "   WHERE 1=1' + @WHERE + @WHERECOD " & vbCrLf
    sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @CONSULTA,@PARAM,@ERP=@ERP, @TABLA=@TABLA, " & vbCrLf
    sConsulta = sConsulta & "   @COD_FS=@COD_FS, @COD_ERP=@COD_ERP, @RECEP_ERP=@RECEP_ERP," & vbCrLf
    sConsulta = sConsulta & "   @ANYO_PROCE=@ANYO_PROCE, @GMN1_PROCE=@GMN1_PROCE, @COD_PROCE=@COD_PROCE," & vbCrLf
    sConsulta = sConsulta & "   @ANYO_PEDIDO=@ANYO_PEDIDO, @NUM_PEDIDO=@NUM_PEDIDO, @NUM_ORDEN=@NUM_ORDEN," & vbCrLf
    sConsulta = sConsulta & "   @ID_LOG=@ID_LOG, @ACCION=@ACCION, @ESTADO=@ESTADO,@FECHA_DESDE=@FECHA_DESDE,@FECHA_HASTA=@FECHA_HASTA,@TIPOFECHA=@TIPOFECHA" & vbCrLf
    sConsulta = sConsulta & "END" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "IF @PAGO_ACTIVA = 1" & vbCrLf
    sConsulta = sConsulta & "BEGIN" & vbCrLf
    sConsulta = sConsulta & "   SET @CONSULTA= 'INSERT INTO #T_CONSULTA SELECT " & vbCrLf
    sConsulta = sConsulta & "   LG.ERP AS ERP, LG.TABLA AS ENTIDAD, LG.ID AS IDENTIF, LG.ESTADO, LG.TEXTO, LG.ARG1, LG.ARG2,LG.FECENV, LG.FECREC, NOM_FICHERO , NOM_RECIBO," & vbCrLf
    sConsulta = sConsulta & "   L.ACCION , L.NUM AS COD , LG.COD_ERP , L.USU  , '''' INFO,LG.ORIGEN" & vbCrLf
    sConsulta = sConsulta & "   FROM #T_LOG_GRAL LG WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN LOG_PAGO L WITH(NOLOCK) ON LG.ID_TABLA=L.ID AND LG.TABLA=33" & vbCrLf
    sConsulta = sConsulta & "   WHERE 1=1' + @WHERE + @WHERECOD" & vbCrLf
    sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @CONSULTA,@PARAM,@ERP=@ERP, @TABLA=@TABLA, " & vbCrLf
    sConsulta = sConsulta & "   @COD_FS=@COD_FS, @COD_ERP=@COD_ERP, @RECEP_ERP=@RECEP_ERP," & vbCrLf
    sConsulta = sConsulta & "   @ANYO_PROCE=@ANYO_PROCE, @GMN1_PROCE=@GMN1_PROCE, @COD_PROCE=@COD_PROCE," & vbCrLf
    sConsulta = sConsulta & "   @ANYO_PEDIDO=@ANYO_PEDIDO, @NUM_PEDIDO=@NUM_PEDIDO, @NUM_ORDEN=@NUM_ORDEN," & vbCrLf
    sConsulta = sConsulta & "   @ID_LOG=@ID_LOG, @ACCION=@ACCION, @ESTADO=@ESTADO,@FECHA_DESDE=@FECHA_DESDE,@FECHA_HASTA=@FECHA_HASTA,@TIPOFECHA=@TIPOFECHA" & vbCrLf
    sConsulta = sConsulta & "END" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "IF @PRES5_ACTIVA = 1" & vbCrLf
    sConsulta = sConsulta & "BEGIN" & vbCrLf
    sConsulta = sConsulta & "   SET @CONSULTA= 'INSERT INTO #T_CONSULTA SELECT " & vbCrLf
    sConsulta = sConsulta & "   LG.ERP AS ERP, LG.TABLA AS ENTIDAD, LG.ID AS IDENTIF, LG.ESTADO, LG.TEXTO, LG.ARG1, LG.ARG2,LG.FECENV, LG.FECREC, NOM_FICHERO , NOM_RECIBO," & vbCrLf
    sConsulta = sConsulta & "   L.ACCION , L.PRES0 + ''-'' + L.PRES1 + ''-'' + L.PRES2 + ''-'' + L.PRES3 + ''-'' + L.PRES4 AS COD , " & vbCrLf
    sConsulta = sConsulta & "   LG.COD_ERP , L.USU , '''' INFO,LG.ORIGEN" & vbCrLf
    sConsulta = sConsulta & "   FROM #T_LOG_GRAL LG WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN LOG_PRES5 L WITH(NOLOCK) ON LG.ID_TABLA=L.ID AND LG.TABLA=34" & vbCrLf
    sConsulta = sConsulta & "   WHERE 1=1' + @WHERE + @WHERECOD    " & vbCrLf
    sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @CONSULTA,@PARAM,@ERP=@ERP, @TABLA=@TABLA, " & vbCrLf
    sConsulta = sConsulta & "   @COD_FS=@COD_FS, @COD_ERP=@COD_ERP, @RECEP_ERP=@RECEP_ERP," & vbCrLf
    sConsulta = sConsulta & "   @ANYO_PROCE=@ANYO_PROCE, @GMN1_PROCE=@GMN1_PROCE, @COD_PROCE=@COD_PROCE," & vbCrLf
    sConsulta = sConsulta & "   @ANYO_PEDIDO=@ANYO_PEDIDO, @NUM_PEDIDO=@NUM_PEDIDO, @NUM_ORDEN=@NUM_ORDEN," & vbCrLf
    sConsulta = sConsulta & "   @ID_LOG=@ID_LOG, @ACCION=@ACCION, @ESTADO=@ESTADO,@FECHA_DESDE=@FECHA_DESDE,@FECHA_HASTA=@FECHA_HASTA,@TIPOFECHA=@TIPOFECHA" & vbCrLf
    sConsulta = sConsulta & "END" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "IF @CENTRO_SM_ACTIVA = 1" & vbCrLf
    sConsulta = sConsulta & "BEGIN" & vbCrLf
    sConsulta = sConsulta & "   SET @CONSULTA= 'INSERT INTO #T_CONSULTA SELECT " & vbCrLf
    sConsulta = sConsulta & "   LG.ERP AS ERP, LG.TABLA AS ENTIDAD, LG.ID AS IDENTIF, LG.ESTADO, LG.TEXTO, LG.ARG1, LG.ARG2,LG.FECENV, LG.FECREC, NOM_FICHERO , NOM_RECIBO," & vbCrLf
    sConsulta = sConsulta & "   L.ACCION , L.CENTRO_SM AS COD , LG.COD_ERP , L.USU , '''' INFO,LG.ORIGEN" & vbCrLf
    sConsulta = sConsulta & "   FROM #T_LOG_GRAL LG WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN LOG_CENTRO_SM L WITH(NOLOCK) ON LG.ID_TABLA=L.ID AND LG.TABLA=35" & vbCrLf
    sConsulta = sConsulta & "   WHERE 1=1' + @WHERE + @WHERECOD " & vbCrLf
    sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @CONSULTA,@PARAM,@ERP=@ERP, @TABLA=@TABLA, " & vbCrLf
    sConsulta = sConsulta & "   @COD_FS=@COD_FS, @COD_ERP=@COD_ERP, @RECEP_ERP=@RECEP_ERP," & vbCrLf
    sConsulta = sConsulta & "   @ANYO_PROCE=@ANYO_PROCE, @GMN1_PROCE=@GMN1_PROCE, @COD_PROCE=@COD_PROCE," & vbCrLf
    sConsulta = sConsulta & "   @ANYO_PEDIDO=@ANYO_PEDIDO, @NUM_PEDIDO=@NUM_PEDIDO, @NUM_ORDEN=@NUM_ORDEN," & vbCrLf
    sConsulta = sConsulta & "   @ID_LOG=@ID_LOG, @ACCION=@ACCION, @ESTADO=@ESTADO,@FECHA_DESDE=@FECHA_DESDE,@FECHA_HASTA=@FECHA_HASTA,@TIPOFECHA=@TIPOFECHA" & vbCrLf
    sConsulta = sConsulta & "END" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "IF @EMPRESA_ACTIVA=1" & vbCrLf
    sConsulta = sConsulta & "BEGIN" & vbCrLf
    sConsulta = sConsulta & "   SET @CONSULTA= 'INSERT INTO #T_CONSULTA SELECT " & vbCrLf
    sConsulta = sConsulta & "   LG.ERP AS ERP, LG.TABLA AS ENTIDAD, LG.ID AS IDENTIF, LG.ESTADO, LG.TEXTO, LG.ARG1, LG.ARG2,LG.FECENV, LG.FECREC, NOM_FICHERO , NOM_RECIBO," & vbCrLf
    sConsulta = sConsulta & "   L.ACCION , L.NIF + '' - '' + L.DEN AS COD , LG.COD_ERP , L.USU  , '''' INFO,LG.ORIGEN" & vbCrLf
    sConsulta = sConsulta & "   FROM #T_LOG_GRAL LG WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "   INNER JOIN LOG_EMPRESA L WITH(NOLOCK) ON LG.ID_TABLA=L.ID AND LG.TABLA=36" & vbCrLf
    sConsulta = sConsulta & "   WHERE 1=1' + @WHERE + @WHERECOD " & vbCrLf
    sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @CONSULTA,@PARAM,@ERP=@ERP, @TABLA=@TABLA, " & vbCrLf
    sConsulta = sConsulta & "   @COD_FS=@COD_FS, @COD_ERP=@COD_ERP, @RECEP_ERP=@RECEP_ERP," & vbCrLf
    sConsulta = sConsulta & "   @ANYO_PROCE=@ANYO_PROCE, @GMN1_PROCE=@GMN1_PROCE, @COD_PROCE=@COD_PROCE," & vbCrLf
    sConsulta = sConsulta & "   @ANYO_PEDIDO=@ANYO_PEDIDO, @NUM_PEDIDO=@NUM_PEDIDO, @NUM_ORDEN=@NUM_ORDEN," & vbCrLf
    sConsulta = sConsulta & "   @ID_LOG=@ID_LOG, @ACCION=@ACCION, @ESTADO=@ESTADO,@FECHA_DESDE=@FECHA_DESDE,@FECHA_HASTA=@FECHA_HASTA,@TIPOFECHA=@TIPOFECHA" & vbCrLf
    sConsulta = sConsulta & "END" & vbCrLf
    sConsulta = sConsulta & "      " & vbCrLf
    sConsulta = sConsulta & "DECLARE @COMPLETA AS NVARCHAR(MAX)" & vbCrLf
    sConsulta = sConsulta & "SET @COMPLETA=''" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "---Mapea el origen para que coincida con los campos sentido:  ORIGEN=0 o 2: SALIDA; ORIGEN=3: ENTRADA" & vbCrLf
    sConsulta = sConsulta & "-- SENTIDO=1 salida; SENTIDO=2 entrada" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "IF @PROVE_ERP_ACTIVA=1 " & vbCrLf
    sConsulta = sConsulta & "begin" & vbCrLf
    sConsulta = sConsulta & "   SET @COMPLETA = 'SELECT ERP.DEN AS ERP, T.ENTIDAD, T.IDENTIF, T.ESTADO, T.TEXTO, T.ARG1, T.ARG2, T.FECENV, T.FECREC," & vbCrLf
    sConsulta = sConsulta & "        T.NOM_FICHERO, T.NOM_RECIBO,T.ACCION, T.COD, T.COD_ERP, T.USU , T.INFO,  case T.ORIGEN when 3 then 2 else 1 end AS SENTIDO" & vbCrLf
    sConsulta = sConsulta & "        FROM #T_CONSULTA T INNER JOIN ERP WITH(NOLOCK) ON ERP.COD=T.ERP " & vbCrLf
    sConsulta = sConsulta & "        INNER JOIN TABLAS_INTEGRACION_ERP TIE WITH(NOLOCK) ON TIE.ERP=T.ERP AND TIE.TABLA=8 AND T.ENTIDAD=102'" & vbCrLf
    sConsulta = sConsulta & "    IF @TABLA=0 " & vbCrLf
    sConsulta = sConsulta & "           SET @COMPLETA = @COMPLETA + ' UNION '" & vbCrLf
    sConsulta = sConsulta & "end" & vbCrLf
    sConsulta = sConsulta & "IF @TABLA=0 OR NOT(@TABLA=102) " & vbCrLf
    sConsulta = sConsulta & "   SET @COMPLETA = @COMPLETA + ' SELECT ERP.DEN AS ERP, T.ENTIDAD, T.IDENTIF, T.ESTADO, T.TEXTO, T.ARG1, T.ARG2, T.FECENV, T.FECREC," & vbCrLf
    sConsulta = sConsulta & "        T.NOM_FICHERO, T.NOM_RECIBO,T.ACCION, T.COD, T.COD_ERP, T.USU , T.INFO,  case T.ORIGEN when 3 then 2 else 1 end AS SENTIDO" & vbCrLf
    sConsulta = sConsulta & "        FROM #T_CONSULTA T INNER JOIN ERP WITH(NOLOCK) ON ERP.COD=T.ERP " & vbCrLf
    sConsulta = sConsulta & "        INNER JOIN TABLAS_INTEGRACION_ERP TIE WITH(NOLOCK) ON TIE.ERP=T.ERP AND TIE.TABLA=T.ENTIDAD'" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "EXEC SP_EXECUTESQL  @COMPLETA" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "DROP TABLE #T_LOG_GRAL" & vbCrLf
    sConsulta = sConsulta & "DROP TABLE #T_CONSULTA" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "END" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSGSW_TRASPASAR_ERP]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSGSW_TRASPASAR_ERP]" & vbCrLf
    gRDOCon.QueryTimeout = 7200
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "CREATE PROCEDURE [dbo].[FSGSW_TRASPASAR_ERP] @ANYO INT, @GMN1 VARCHAR(50), @PROCE VARCHAR(50),@ID_ITEM INT,@IDEMP INT,@PROVE_PADRE NVARCHAR(30),@PROVE VARCHAR(50)," & vbCrLf
    sConsulta = sConsulta & "                                            @CANT_ADJ FLOAT,@CENTRO NVARCHAR(30),@PROVE_ERP NVARCHAR(30),@CODUSU NVARCHAR(10) AS       " & vbCrLf
    sConsulta = sConsulta & "                                            " & vbCrLf
    sConsulta = sConsulta & "                                            " & vbCrLf
    sConsulta = sConsulta & "INSERT INTO LOG_ITEM_ADJ (ID_ITEM,ART,DESCR,DEST,UNI,PAG,CANT,CANT_ADJ,PREC_VALIDO_ADJ,PREC_ADJ,PRES,FECINI,FECFIN,OBJ,FECOBJ,ESP,SOLICIT,ORIGEN,USU,GMN1,GMN2,GMN3,GMN4," & vbCrLf
    sConsulta = sConsulta & "ACCION,ANYO,PROCE,GMN1_PROCE,DEN_PROCE,ADJDIR,FECNEC,FECAPE,FECVAL,FECPRES,FECPROXREU,FECULTREU,REUDEC,NUM_OFE,MON_PROCE,CAMBIO_PROCE,MON_OFE,CAMBIO_OFE,SOLICITUD,FECLIMOFE" & vbCrLf
    sConsulta = sConsulta & ",USUAPER,COM,EQP,EMPRESA,PROVE_ERP,FECCIERRE,ERP, PROVE,PROVEPADRE,PORCEN,FECINI_PROCE,FECFIN_PROCE,CENTRO ) " & vbCrLf
    sConsulta = sConsulta & "SELECT @ID_ITEM,I.ART,I.DESCR,I.DEST,I.UNI,I.PAG,I.CANT,@CANT_ADJ,IO.PREC_VALIDO " & vbCrLf
    sConsulta = sConsulta & ",CASE USAR WHEN 1 THEN PRECIO WHEN 2 THEN PRECIO2 WHEN 3 THEN PRECIO3 END,I.PREC AS PRES,I.FECINI,I.FECFIN,I.OBJ,I.FECHAOBJ,I.ESP,I.SOLICIT," & vbCrLf
    sConsulta = sConsulta & "'0',@CODUSU,ISNULL(I.GMN1,ART4.GMN1),ISNULL(I.GMN2,ART4.GMN2),ISNULL(I.GMN3,ART4.GMN3),ISNULL(I.GMN4,ART4.GMN4)," & vbCrLf
    sConsulta = sConsulta & "'I',@ANYO ,@PROCE ,@GMN1,PROCE.DEN,ADJDIR,FECNEC,FECAPE,PROCE.FECVAL,FECPRES,FECPROXREU,I.FECULTREU,REUDEC,PROCE_OFE.OFE,PROCE.MON,PROCE.CAMBIO,PROCE_OFE.MON,PROCE_OFE.CAMBIO,SOLICITUD,FECLIMOFE" & vbCrLf
    sConsulta = sConsulta & " ,USUAPER,PROCE_PROVE.COM,PROCE_PROVE.EQP,@IDEMP,@PROVE_ERP ,PROCE.FECCIERRE," & vbCrLf
    sConsulta = sConsulta & " ERP_SOCIEDAD.ERP,@PROVE,@PROVE_PADRE ,(@CANT_ADJ/I.CANT),PROCE.FECINI,PROCE.FECFIN,@CENTRO  " & vbCrLf
    sConsulta = sConsulta & " FROM PROCE WITH (NOLOCK)" & vbCrLf
    sConsulta = sConsulta & " INNER JOIN PROCE_PROVE WITH (NOLOCK) ON PROCE_PROVE.ANYO = PROCE.ANYO AND PROCE_PROVE.GMN1 = PROCE.GMN1 AND PROCE_PROVE.PROCE = PROCE.COD AND PROCE_PROVE.PROVE =@PROVE_PADRE" & vbCrLf
    sConsulta = sConsulta & "INNER JOIN PROCE_OFE WITH (NOLOCK) ON PROCE_OFE.ANYO = PROCE.ANYO AND PROCE_OFE.GMN1 = PROCE.GMN1 AND PROCE_OFE.PROCE = PROCE.COD AND PROCE_OFE.PROVE =@PROVE_PADRE AND PROCE_OFE.ULT=1" & vbCrLf
    sConsulta = sConsulta & "INNER JOIN EMP WITH (NOLOCK) ON EMP.ID = @IDEMP" & vbCrLf
    sConsulta = sConsulta & " LEFT JOIN ERP_SOCIEDAD WITH (NOLOCK) ON ERP_SOCIEDAD.SOCIEDAD = EMP.SOCIEDAD" & vbCrLf
    sConsulta = sConsulta & "INNER JOIN  ITEM I WITH (NOLOCK) ON I.ANYO=PROCE.ANYO AND I.GMN1_PROCE =PROCE.GMN1 AND PROCE.COD=I.PROCE AND I.ID=@ID_ITEM " & vbCrLf
    sConsulta = sConsulta & "INNER JOIN ITEM_OFE IO WITH(NOLOCK) ON I.ANYO=IO.ANYO AND I.GMN1_PROCE=IO.GMN1 AND I.PROCE=IO.PROCE AND I.ID=IO.ITEM AND IO.PROVE=@PROVE_PADRE AND IO.ULT=1" & vbCrLf
    sConsulta = sConsulta & "LEFT JOIN ART4 WITH(NOLOCK) ON ART4.COD=I.ART" & vbCrLf
    sConsulta = sConsulta & "WHERE I.ANYO = @ANYO  AND I.GMN1_PROCE=@GMN1 AND I.PROCE=@PROCE AND I.ID=@ID_ITEM" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSGSW_BORRAR_ADJ_ERP]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSGSW_BORRAR_ADJ_ERP]" & vbCrLf
    gRDOCon.QueryTimeout = 7200
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "CREATE PROCEDURE [dbo].[FSGSW_BORRAR_ADJ_ERP] @ANYO INT, @GMN1 VARCHAR(50), @PROCE VARCHAR(50),@ID_ITEM INT,@IDEMP INT,@PROVE_PADRE NVARCHAR(30),@PROVE VARCHAR(50)," & vbCrLf
    sConsulta = sConsulta & "                                            @CANT_ADJ FLOAT,@CENTRO NVARCHAR(30),@PROVE_ERP NVARCHAR(30),@CODUSU NVARCHAR(10) AS       " & vbCrLf
    sConsulta = sConsulta & "                                            " & vbCrLf
    sConsulta = sConsulta & "                                            " & vbCrLf
    sConsulta = sConsulta & "INSERT INTO LOG_ITEM_ADJ (ID_ITEM,ART,DESCR,DEST,UNI,PAG,CANT,CANT_ADJ,PREC_VALIDO_ADJ,PREC_ADJ,PRES,FECINI,FECFIN,OBJ,FECOBJ,ESP,SOLICIT,ORIGEN,USU,GMN1,GMN2,GMN3,GMN4," & vbCrLf
    sConsulta = sConsulta & "ACCION,ANYO,PROCE,GMN1_PROCE,DEN_PROCE,ADJDIR,FECNEC,FECAPE,FECVAL,FECPRES,FECPROXREU,FECULTREU,REUDEC,NUM_OFE,MON_PROCE,CAMBIO_PROCE,MON_OFE,CAMBIO_OFE,SOLICITUD,FECLIMOFE" & vbCrLf
    sConsulta = sConsulta & ",USUAPER,COM,EQP,EMPRESA,PROVE_ERP,FECCIERRE,ERP, PROVE, PROVEPADRE ,CENTRO,PORCEN,FECINI_PROCE,FECFIN_PROCE ) " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "SELECT @ID_ITEM,I.ART,I.DESCR,I.DEST,I.UNI,I.PAG,I.CANT,@CANT_ADJ,IO.PREC_VALIDO " & vbCrLf
    sConsulta = sConsulta & ",CASE USAR WHEN 1 THEN PRECIO WHEN 2 THEN PRECIO2 WHEN 3 THEN PRECIO3 END,I.PREC AS PRES,I.FECINI,I.FECFIN,I.OBJ,I.FECHAOBJ,I.ESP,I.SOLICIT," & vbCrLf
    sConsulta = sConsulta & "'0',@CODUSU,ISNULL(I.GMN1,ART4.GMN1),ISNULL(I.GMN2,ART4.GMN2),ISNULL(I.GMN3,ART4.GMN3),ISNULL(I.GMN4,ART4.GMN4)," & vbCrLf
    sConsulta = sConsulta & "'D',@ANYO ,@PROCE ,@GMN1,PROCE.DEN,ADJDIR,FECNEC,FECAPE,PROCE.FECVAL,FECPRES,FECPROXREU,I.FECULTREU,REUDEC,PROCE_OFE.OFE,PROCE.MON,PROCE.CAMBIO,PROCE_OFE.MON,PROCE_OFE.CAMBIO,SOLICITUD,FECLIMOFE" & vbCrLf
    sConsulta = sConsulta & " ,USUAPER,PROCE_PROVE.COM,PROCE_PROVE.EQP,@IDEMP,@PROVE_ERP ,PROCE.FECCIERRE," & vbCrLf
    sConsulta = sConsulta & " ERP_SOCIEDAD.ERP,@PROVE,@PROVE_PADRE, @CENTRO,(@CANT_ADJ/I.CANT),PROCE.FECINI,PROCE.FECFIN  " & vbCrLf
    sConsulta = sConsulta & " FROM PROCE WITH (NOLOCK)" & vbCrLf
    sConsulta = sConsulta & " INNER JOIN PROCE_PROVE WITH (NOLOCK) ON PROCE_PROVE.ANYO = PROCE.ANYO AND PROCE_PROVE.GMN1 = PROCE.GMN1 AND PROCE_PROVE.PROCE = PROCE.COD AND PROCE_PROVE.PROVE =@PROVE" & vbCrLf
    sConsulta = sConsulta & "INNER JOIN PROCE_OFE WITH (NOLOCK) ON PROCE_OFE.ANYO = PROCE.ANYO AND PROCE_OFE.GMN1 = PROCE.GMN1 AND PROCE_OFE.PROCE = PROCE.COD AND PROCE_OFE.PROVE =@PROVE AND PROCE_OFE.ULT=1" & vbCrLf
    sConsulta = sConsulta & "INNER JOIN EMP WITH (NOLOCK) ON EMP.ID = @IDEMP" & vbCrLf
    sConsulta = sConsulta & " LEFT JOIN ERP_SOCIEDAD WITH (NOLOCK) ON ERP_SOCIEDAD.SOCIEDAD = EMP.SOCIEDAD" & vbCrLf
    sConsulta = sConsulta & "INNER JOIN  ITEM I WITH (NOLOCK) ON I.ANYO=PROCE.ANYO AND I.GMN1_PROCE =PROCE.GMN1 AND PROCE.COD=I.PROCE AND I.ID=@ID_ITEM " & vbCrLf
    sConsulta = sConsulta & "INNER JOIN ITEM_OFE IO WITH(NOLOCK) ON I.ANYO=IO.ANYO AND I.GMN1_PROCE=IO.GMN1 AND I.PROCE=IO.PROCE AND I.ID=IO.ITEM AND IO.PROVE=@PROVE AND IO.ULT=1" & vbCrLf
    sConsulta = sConsulta & "LEFT JOIN ART4 WITH(NOLOCK) ON ART4.COD=I.ART" & vbCrLf
    sConsulta = sConsulta & "WHERE I.ANYO = @ANYO  AND I.GMN1_PROCE=@GMN1 AND I.PROCE=@PROCE AND I.ID=@ID_ITEM" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_ESTADISTICAS_EVENTOS]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_ESTADISTICAS_EVENTOS]" & vbCrLf
    gRDOCon.QueryTimeout = 7200
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "CREATE PROCEDURE [dbo].[FSAL_ESTADISTICAS_EVENTOS]  " & vbCrLf
    sConsulta = sConsulta & "   @DESDE datetime," & vbCrLf
    sConsulta = sConsulta & "   @HASTA datetime," & vbCrLf
    sConsulta = sConsulta & "   @ENTORNO nvarchar(50)," & vbCrLf
    sConsulta = sConsulta & "   @PYME nvarchar(50)" & vbCrLf
    sConsulta = sConsulta & "AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "select @Desde, @hasta" & vbCrLf
    sConsulta = sConsulta & "Set datefirst 1" & vbCrLf
    sConsulta = sConsulta & "DECLARE @RC int" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "declare @ANYO datetime" & vbCrLf
    sConsulta = sConsulta & "set @ANYO = @DESDE" & vbCrLf
    sConsulta = sConsulta & "while  datepart(""yy"",@ANYO) <= datepart(""yy"",@HASTA)" & vbCrLf
    sConsulta = sConsulta & "begin " & vbCrLf
    sConsulta = sConsulta & "   -- Comienzo Bucle A�o" & vbCrLf
    sConsulta = sConsulta & "   declare @MES datetime" & vbCrLf
    sConsulta = sConsulta & "   set @MES = @ANYO" & vbCrLf
    sConsulta = sConsulta & "   while  datepart(""yy"",@MES) <= datepart(""yy"",@ANYO) AND  @MES <= @HASTA" & vbCrLf
    sConsulta = sConsulta & "   begin " & vbCrLf
    sConsulta = sConsulta & "       -- Comienzo Bucle Mes" & vbCrLf
    sConsulta = sConsulta & "       declare @DIA datetime" & vbCrLf
    sConsulta = sConsulta & "       set @DIA = @Mes" & vbCrLf
    sConsulta = sConsulta & "       while  datepart(""mm"",@DIA) <= datepart(""mm"",@MES) AND  @DIA <= @HASTA" & vbCrLf
    sConsulta = sConsulta & "       begin " & vbCrLf
    sConsulta = sConsulta & "           -- Comienzo Bucle Dia" & vbCrLf
    sConsulta = sConsulta & "           declare @HORA datetime" & vbCrLf
    sConsulta = sConsulta & "           set @HORA = @DIA" & vbCrLf
    sConsulta = sConsulta & "           while  datepart(""dd"",@HORA) <= datepart(""dd"",@DIA) AND  @HORA < @HASTA" & vbCrLf
    sConsulta = sConsulta & "           begin " & vbCrLf
    sConsulta = sConsulta & "               -- Comienzo Bucle HORA" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "               EXECUTE @RC = [dbo].[FSAL_ESTADISTICAS_EVENTO_HORA] " & vbCrLf
    sConsulta = sConsulta & "                  @HORA" & vbCrLf
    sConsulta = sConsulta & "                 ,@HASTA" & vbCrLf
    sConsulta = sConsulta & "                 ,@ENTORNO" & vbCrLf
    sConsulta = sConsulta & "                 ,@PYME" & vbCrLf
    sConsulta = sConsulta & "               -- Fin Bucle HORA" & vbCrLf
    sConsulta = sConsulta & "           " & vbCrLf
    sConsulta = sConsulta & "           set @HORA = dateadd(hour,1,@HORA)       " & vbCrLf
    sConsulta = sConsulta & "           set @HORA = convert(datetime,convert(varchar(13),@HORA,121) + ':00')" & vbCrLf
    sConsulta = sConsulta & "           end " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "           -- Fin Bucle Dia" & vbCrLf
    sConsulta = sConsulta & "       set @DIA = dateadd(day,1,@DIA)      " & vbCrLf
    sConsulta = sConsulta & "       set @DIA = convert(datetime,convert(varchar(10),@DIA,102))" & vbCrLf
    sConsulta = sConsulta & "       end " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "       -- Fin Bucle Mes" & vbCrLf
    sConsulta = sConsulta & "   set @MES = dateadd(month,1,@MES)" & vbCrLf
    sConsulta = sConsulta & "   set @MES = dateadd(day,-1*(datepart(""dd"",@MES)-1),@MES)" & vbCrLf
    sConsulta = sConsulta & "   end " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   -- Fin Bucle A�o" & vbCrLf
    sConsulta = sConsulta & "set @ANYO = dateadd(year,1,@ANYO)" & vbCrLf
    sConsulta = sConsulta & "set @ANYO = dateadd(month,-1*(datepart(""mm"",@ANYO)-1),@ANYO)" & vbCrLf
    sConsulta = sConsulta & "end" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_ACTIVITY_LOAD_POR_CADA_CINCO_MINUTOS]') AND type in (N'P', N'PC'))" & vbCrLf
    sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_ACTIVITY_LOAD_POR_CADA_CINCO_MINUTOS]" & vbCrLf
    gRDOCon.QueryTimeout = 7200
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "CREATE PROCEDURE [dbo].[FSAL_ACTIVITY_LOAD_POR_CADA_CINCO_MINUTOS]  " & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "   @fecdesde DATETIME," & vbCrLf
    sConsulta = sConsulta & "   @fechasta DATETIME," & vbCrLf
    sConsulta = sConsulta & "   @entorno nvarchar(200)," & vbCrLf
    sConsulta = sConsulta & "   @tipoEvento int," & vbCrLf
    sConsulta = sConsulta & "   @pyme nvarchar(200)=null" & vbCrLf
    sConsulta = sConsulta & "AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "-- COMMON TABLE EXPRESSION para obtener todas las entradas (bloques de 5 min) entre las fechas-horas indicadas" & vbCrLf
    sConsulta = sConsulta & "-- OJO! Esta CTE s�lo admite 100 pasos de iteraci�n/Recursi�n, pero se supone que es v�lida ya que solo se van" & vbCrLf
    sConsulta = sConsulta & "-- a consultar fragmentos de 1 hora de duracci�n (12 bloques de 5 min)" & vbCrLf
    sConsulta = sConsulta & "DECLARE @fechacomienzo DATETIME" & vbCrLf
    sConsulta = sConsulta & "DECLARE @MEDIA FLOAT" & vbCrLf
    sConsulta = sConsulta & "declare @numDiaSemanaHoras float" & vbCrLf
    sConsulta = sConsulta & "SET @fechacomienzo=CONVERT(datetime,'2010-01-01')" & vbCrLf
    sConsulta = sConsulta & "set @numDiaSemanaHoras = DATEDIFF(""WW"",@fechacomienzo,GETDATE())" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "DECLARE @fechadesdemedia datetime" & vbCrLf
    sConsulta = sConsulta & "DECLARE @fechahastamedia datetime" & vbCrLf
    sConsulta = sConsulta & "if(DATEPART(""MI"",@FECDESDE)>30)" & vbCrLf
    sConsulta = sConsulta & "BEGIN" & vbCrLf
    sConsulta = sConsulta & "   set @fechadesdemedia= CONVERT(DATETIME,CONVERT(VARCHAR,DATEPART(""YYYY"",@fecdesde))+'-'+CONVERT(VARCHAR,DATEPART(""MM"",@FECDESDE))+'-'+CONVERT(VARCHAR,DATEPART(""DD"",@FECDESDE))+' '+CONVERT(VARCHAR,DATEPART(""HH"",@FECDESDE+1))+':00:00')" & vbCrLf
    sConsulta = sConsulta & "END" & vbCrLf
    sConsulta = sConsulta & "ELSE" & vbCrLf
    sConsulta = sConsulta & "   begin" & vbCrLf
    sConsulta = sConsulta & "   set @fechadesdemedia= CONVERT(DATETIME,CONVERT(VARCHAR,DATEPART(""YYYY"",@fecdesde))+'-'+CONVERT(VARCHAR,DATEPART(""MM"",@FECDESDE))+'-'+CONVERT(VARCHAR,DATEPART(""DD"",@FECDESDE))+' '+CONVERT(VARCHAR,DATEPART(""HH"",@FECDESDE))+':00:00')" & vbCrLf
    sConsulta = sConsulta & "   end" & vbCrLf
    sConsulta = sConsulta & "SET @fecdesde=DBO.fn5MIN(@FECDESDE)" & vbCrLf
    sConsulta = sConsulta & "SET @fechasta=DBO.fn5MIN(@FECHASTA)" & vbCrLf
    sConsulta = sConsulta & "SET @fechadesdemedia=DBO.fn5MIN(@FECHADESDEMEDIA)" & vbCrLf
    sConsulta = sConsulta & "SET @fechahastamedia=dbO.fn5MIN(@FECHAHASTAMEDIA)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "select @MEDIA=(" & vbCrLf
    sConsulta = sConsulta & "SELECT round((sum(convert(float, NUM_EVENTOS))/@numDiaSemanaHoras),2,0) as SUM_EVENTOS" & vbCrLf
    sConsulta = sConsulta & "FROM FSAL_EVENTOS_HIST WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "WHERE FSAL_ENTORNO=@entorno " & vbCrLf
    sConsulta = sConsulta & "AND isnull(FSAL_PYME,'') = isnull(@pyme,'') " & vbCrLf
    sConsulta = sConsulta & "AND TIPO_EVENTO=@tipoEvento" & vbCrLf
    sConsulta = sConsulta & "AND DATEPART(""DW"",FECHA_EVENTO)=DATEPART(""DW"",@FECDESDE)" & vbCrLf
    sConsulta = sConsulta & "AND DATEPART(""HH"",fecha_evento)=DATEPART(""HH"",@fecdesde)" & vbCrLf
    sConsulta = sConsulta & "group by DATEPART(""DW"",fecha_evento),DATEPART(""hh"",fecha_evento));" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "with mycte as" & vbCrLf
    sConsulta = sConsulta & "(" & vbCrLf
    sConsulta = sConsulta & "    SELECT @fecdesde  AS MyHour" & vbCrLf
    sConsulta = sConsulta & "    UNION ALL" & vbCrLf
    sConsulta = sConsulta & "    SELECT dateadd(MINUTE ,5,MyHour)" & vbCrLf
    sConsulta = sConsulta & "    FROM mycte " & vbCrLf
    sConsulta = sConsulta & "    WHERE dateadd(MINUTE ,5,MyHour) < @fechasta" & vbCrLf
    sConsulta = sConsulta & ")" & vbCrLf
    sConsulta = sConsulta & "SELECT ACCESOS.FECHA, ISNULL(EVENTOS.NUM_EVENTOS,0) as NUM_EVENTOS, ISNULL(@MEDIA,0) AS AVG_EVENTOS FROM" & vbCrLf
    sConsulta = sConsulta & "(SELECT RIGHT(REPLICATE('0',2)+LTRIM(DATEPART(""HH"",MyHour)),2)+':'+RIGHT(REPLICATE('0',2)+LTRIM(DATEPART(""MI"",MyHour)),2) AS FECHA" & vbCrLf
    sConsulta = sConsulta & "FROM mycte) ACCESOS" & vbCrLf
    sConsulta = sConsulta & "LEFT JOIN(" & vbCrLf
    sConsulta = sConsulta & "select RIGHT(REPLICATE('0',2)+convert(varchar,HORA),2)+':'+RIGHT(REPLICATE('0',2)+convert(varchar,MIN),2) AS FECHA, SUM(NUM_EVENTOS) AS NUM_EVENTOS, DATEPART(""DW"",FECHA_EVENTO) as DIA_SEMANA" & vbCrLf
    sConsulta = sConsulta & "FROM FSAL_EVENTOS_HIST WITH(NOLOCK)" & vbCrLf
    sConsulta = sConsulta & "WHERE  FSAL_ENTORNO=@entorno and isnull(FSAL_PYME,'') = isnull(@pyme,'') AND TIPO_EVENTO=@tipoEvento AND FECHA_EVENTO>=@fecdesde AND FECHA_EVENTO<=@fechasta" & vbCrLf
    sConsulta = sConsulta & "GROUP BY DATEPART(""DW"",FECHA_EVENTO), RIGHT(REPLICATE('0',2)+convert(varchar,HORA),2)+':'+RIGHT(REPLICATE('0',2)+convert(varchar,MIN),2)) EVENTOS" & vbCrLf
    sConsulta = sConsulta & "ON ACCESOS.FECHA=EVENTOS.FECHA" & vbCrLf
    sConsulta = sConsulta & "order by ACCESOS.FECHA" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

End Sub

Public Sub V_31900_8_Datos_091()
    Dim sConsulta As String
    
    sConsulta = "IF (OBJECT_ID('PK_FSAL_TEMP_USUARIOS_DIS_ANYO', 'PK') IS NOT NULL)" & vbCrLf
    sConsulta = sConsulta & "BEGIN" & vbCrLf
    sConsulta = sConsulta & "   alter table FSAL_USUARIOS_DIS_ANYO" & vbCrLf
    sConsulta = sConsulta & "   drop constraint [PK_FSAL_TEMP_USUARIOS_DIS_ANYO]" & vbCrLf
    sConsulta = sConsulta & "END" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "alter table FSAL_USUARIOS_DIS_ANYO" & vbCrLf
    sConsulta = sConsulta & "alter column USU nvarchar(50) NOT NULL" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "IF (OBJECT_ID('PK_FSAL_USUARIOS_DIS_ANYO', 'PK') IS NOT NULL)" & vbCrLf
    sConsulta = sConsulta & "BEGIN" & vbCrLf
    sConsulta = sConsulta & "   alter table FSAL_USUARIOS_DIS_ANYO" & vbCrLf
    sConsulta = sConsulta & "   drop constraint [PK_FSAL_USUARIOS_DIS_ANYO] " & vbCrLf
    sConsulta = sConsulta & "END" & vbCrLf
    sConsulta = sConsulta & "alter table FSAL_USUARIOS_DIS_ANYO" & vbCrLf
    sConsulta = sConsulta & "add constraint [PK_FSAL_USUARIOS_DIS_ANYO] primary key (ID)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "IF (OBJECT_ID('UK_FSAL_USUARIOS_DIS_ANYO','UQ') IS NOT NULL)" & vbCrLf
    sConsulta = sConsulta & "BEGIN" & vbCrLf
    sConsulta = sConsulta & "   alter table FSAL_USUARIOS_DIS_ANYO" & vbCrLf
    sConsulta = sConsulta & "   drop constraint [UK_FSAL_USUARIOS_DIS_ANYO]" & vbCrLf
    sConsulta = sConsulta & "END" & vbCrLf
    sConsulta = sConsulta & "alter table FSAL_USUARIOS_DIS_ANYO" & vbCrLf
    sConsulta = sConsulta & "add constraint [UK_FSAL_USUARIOS_DIS_ANYO] unique (ANYO,ENTORNO,PRODUCTO,USU)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "IF (OBJECT_ID('PK_FSAL_TEMP_USUARIOS_DIS_MES', 'PK') IS NOT NULL)" & vbCrLf
    sConsulta = sConsulta & "BEGIN" & vbCrLf
    sConsulta = sConsulta & "   alter table FSAL_USUARIOS_DIS_MES" & vbCrLf
    sConsulta = sConsulta & "   drop constraint [PK_FSAL_TEMP_USUARIOS_DIS_MES]" & vbCrLf
    sConsulta = sConsulta & "END" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "alter table FSAL_USUARIOS_DIS_MES" & vbCrLf
    sConsulta = sConsulta & "alter column USU nvarchar(50) NOT NULL" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "IF (OBJECT_ID('PK_FSAL_USUARIOS_DIS_MES', 'PK') IS NOT NULL)" & vbCrLf
    sConsulta = sConsulta & "BEGIN" & vbCrLf
    sConsulta = sConsulta & "   alter table FSAL_USUARIOS_DIS_MES" & vbCrLf
    sConsulta = sConsulta & "   drop constraint [PK_FSAL_USUARIOS_DIS_MES] " & vbCrLf
    sConsulta = sConsulta & "END" & vbCrLf
    sConsulta = sConsulta & "alter table FSAL_USUARIOS_DIS_MES" & vbCrLf
    sConsulta = sConsulta & "add constraint [PK_FSAL_USUARIOS_DIS_MES] primary key (ID)" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "IF (OBJECT_ID('PK_FSAL_TEMP_USUARIOS_DIS_SEMANA', 'PK') IS NOT NULL)" & vbCrLf
    sConsulta = sConsulta & "BEGIN" & vbCrLf
    sConsulta = sConsulta & "   alter table FSAL_USUARIOS_DIS_SEMANA" & vbCrLf
    sConsulta = sConsulta & "   drop constraint [PK_FSAL_TEMP_USUARIOS_DIS_SEMANA] " & vbCrLf
    sConsulta = sConsulta & "END" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "IF (OBJECT_ID('PK_FSAL_USUARIOS_DIS_SEMANA', 'PK') IS NOT NULL)" & vbCrLf
    sConsulta = sConsulta & "BEGIN" & vbCrLf
    sConsulta = sConsulta & "   alter table FSAL_USUARIOS_DIS_SEMANA" & vbCrLf
    sConsulta = sConsulta & "   drop constraint [PK_FSAL_USUARIOS_DIS_SEMANA] " & vbCrLf
    sConsulta = sConsulta & "END" & vbCrLf
    sConsulta = sConsulta & "alter table FSAL_USUARIOS_DIS_SEMANA" & vbCrLf
    sConsulta = sConsulta & "add constraint [PK_FSAL_USUARIOS_DIS_SEMANA] primary key (ID)" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
End Sub

Public Function CodigoDeActualizacion31900_08_1891_A31900_08_1892() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim oADOConnection As ADODB.Connection
    Dim sConsulta As String

    On Error GoTo Error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"


    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Actualiza Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 3
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    frmProgreso.ProgressBar1.Refresh
    V_31900_8_Storeds_092
    
    
    sConsulta = "UPDATE VERSION SET NUM ='3.00.18.92'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103), PRODVER='31900.8'"
    ExecuteSQL gRDOCon, sConsulta
        
    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False
        
    CodigoDeActualizacion31900_08_1891_A31900_08_1892 = True
    Exit Function

Error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    CodigoDeActualizacion31900_08_1891_A31900_08_1892 = False
End Function

Public Sub V_31900_8_Storeds_092()
    Dim sConsulta As String

sConsulta = "IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_PERMISOS_IMPUTACION_LCX]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_PERMISOS_IMPUTACION_LCX]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [DBO].[FSPM_PERMISOS_IMPUTACION_LCX]" & vbCrLf
sConsulta = sConsulta & "   @CODUSUARIO VARCHAR(20),@CODEMPRESA VARCHAR(20),@CENTROCOSTE VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "   @OUTPUT AS VARCHAR(20) OUTPUT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   SET NOCOUNT ON;" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   DECLARE @UON1 AS VARCHAR(20) = NULL" & vbCrLf
sConsulta = sConsulta & "   DECLARE @UON2 AS VARCHAR(20) = NULL" & vbCrLf
sConsulta = sConsulta & "   DECLARE @UON3 AS VARCHAR(20) = NULL" & vbCrLf
sConsulta = sConsulta & "   DECLARE @UON4 AS VARCHAR(20) = NULL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @UON1 = @CODEMPRESA --PARA NO LIARNOS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--BORRAMOS LOS PERMISOS ANTERIORES DEL USUARIO       " & vbCrLf
sConsulta = sConsulta & "DELETE FROM USU_CC_IMPUTACION WHERE USU=@CODUSUARIO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--GRABAMOS LOS NUEVOS PERMISOS PARA EL CONJUNTO DE PAR�METROS SUMINISTRADO" & vbCrLf
sConsulta = sConsulta & "IF @CENTROCOSTE IS NULL --ES UN ADMINISTRADOR BORRAMOS TODAS LAS IMPUTACIONES E INSERTAMOS TODAS LAS IMPUTACIONES PARA LOS CENTROS DE COSTE DE ESA EMPRESA       " & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO USU_CC_IMPUTACION (USU,UON1,UON2,UON3,UON4) " & vbCrLf
sConsulta = sConsulta & "     SELECT DISTINCT @CODUSUARIO,UON1,UON2,UON3,UON4 " & vbCrLf
sConsulta = sConsulta & "     FROM CENTRO_SM_UON WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "     WHERE UON1=@UON1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @OUTPUT = ''" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "ELSE -- ES UN GESTOR O CONSULTOR INSERTAMOS SOLO PARA ESE CENTRO DE COSTE Y PARA LOS QUE CUELGUEN DE �L" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "   --DECLARAMOS UN CURSOR PARA SACAR TODOS LOS CENTROS DE COSTE DE LA EMPRESA " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   ----DADO QUE LOS C�DIGOS DE UON2 TB SE USAN EN UON3, PARA PREPARAR EL STORED DEFINITIVO HAY QUE SABER A QU� NIVEL EMPEZAR�N" & vbCrLf
sConsulta = sConsulta & "   ----LOS CENTROS POR LOS QUE SE BUSCAR� Y TB QUE POR DEBAJO DE ESE NIVEL NO SE PUEDEN REPETIR C�DIGOS." & vbCrLf
sConsulta = sConsulta & "   ----DE MOMENTO LOS CENTROS SER�N SIEMPRE DE NIVEL UON3 Y LOS CENTROS DEPENDIENTES SER�N DE NIVEL UON4." & vbCrLf
sConsulta = sConsulta & "   ----SI LOS CENTROS FUESEN DE NIVEL 2 O BIEN SE QUISIESE FILTRAR POR �REAS (LO QUE ACTUALMENTE ES EL NIVEL UON2), " & vbCrLf
sConsulta = sConsulta & "   ----HAY QUE DESCOMENTAR LO COMENTADO PERO ASUMIENDO QUE SI EL C�DIGO @CENTROCOSTE EST� REPETIDO EN UON2 Y TB EN UON3 (ACTUALMENTE HAY REPETIDOS)" & vbCrLf
sConsulta = sConsulta & "   ----SE COGE SIEMPRE EL DE UON2 PORQUE NO SE ESPECIFICA EL NIVEL DESEADO" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   --IF EXISTS(SELECT UON1,UON2, UON3, UON4 FROM CENTRO_SM_UON WITH(NOLOCK) WHERE UON1 = @UON1 AND UON2 = @CENTROCOSTE)" & vbCrLf
sConsulta = sConsulta & "   --  BEGIN" & vbCrLf
sConsulta = sConsulta & "   --  DECLARE CCENTRODECOSTE CURSOR FOR" & vbCrLf
sConsulta = sConsulta & "   --  SELECT DISTINCT UON1,UON2, UON3, UON4 FROM CENTRO_SM_UON WITH(NOLOCK) WHERE UON1 = @UON1 AND UON2 = @CENTROCOSTE" & vbCrLf
sConsulta = sConsulta & "   --  SET @OUTPUT = @UON1+'#'+ISNULL(@CENTROCOSTE,'NULL')" & vbCrLf
sConsulta = sConsulta & "   --  END" & vbCrLf
sConsulta = sConsulta & "   --ELSE" & vbCrLf
sConsulta = sConsulta & "       IF EXISTS(SELECT UON1,UON2, UON3, UON4 FROM CENTRO_SM_UON WITH(NOLOCK) WHERE UON1 = @UON1 AND UON3 = @CENTROCOSTE)" & vbCrLf
sConsulta = sConsulta & "           BEGIN" & vbCrLf
sConsulta = sConsulta & "           DECLARE CCENTRODECOSTE CURSOR FOR" & vbCrLf
sConsulta = sConsulta & "           SELECT DISTINCT UON1, UON2, UON3, UON4 FROM CENTRO_SM_UON WITH(NOLOCK) WHERE UON1 = @UON1 AND UON3 = @CENTROCOSTE" & vbCrLf
sConsulta = sConsulta & "           SELECT TOP 1 @UON2 = UON2 FROM CENTRO_SM_UON WITH(NOLOCK) WHERE UON1 = @UON1 AND UON3 = @CENTROCOSTE" & vbCrLf
sConsulta = sConsulta & "           SET @OUTPUT = @UON1+'#'+ISNULL(@UON2,'NULL')+'#'+ ISNULL(@CENTROCOSTE,'NULL')" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "       ELSE" & vbCrLf
sConsulta = sConsulta & "           IF EXISTS(SELECT UON1,UON2, UON3, UON4 FROM CENTRO_SM_UON WITH(NOLOCK) WHERE UON1 = @UON1 AND UON4 = @CENTROCOSTE)" & vbCrLf
sConsulta = sConsulta & "               BEGIN" & vbCrLf
sConsulta = sConsulta & "               DECLARE CCENTRODECOSTE CURSOR FOR" & vbCrLf
sConsulta = sConsulta & "               SELECT DISTINCT UON1,UON2, UON3, UON4 FROM CENTRO_SM_UON WITH(NOLOCK) WHERE UON1 = @UON1 AND UON4 = @CENTROCOSTE" & vbCrLf
sConsulta = sConsulta & "               SELECT TOP 1 @UON2 = UON2, @UON3 = UON3 FROM CENTRO_SM_UON WITH(NOLOCK) WHERE UON1 = @UON1 AND UON4 = @CENTROCOSTE" & vbCrLf
sConsulta = sConsulta & "               SET @OUTPUT = @UON1+'#'+ISNULL(@UON2,'NULL')+'#'+ISNULL(@UON3,'NULL')+'#'+ ISNULL(@CENTROCOSTE,'NULL')" & vbCrLf
sConsulta = sConsulta & "               END" & vbCrLf
sConsulta = sConsulta & "           ELSE --NO HAY DATOS" & vbCrLf
sConsulta = sConsulta & "               BEGIN" & vbCrLf
sConsulta = sConsulta & "               DECLARE CCENTRODECOSTE CURSOR FOR" & vbCrLf
sConsulta = sConsulta & "               SELECT DISTINCT UON1,UON2, UON3, UON4 FROM CENTRO_SM_UON WITH(NOLOCK) WHERE 1=2" & vbCrLf
sConsulta = sConsulta & "               SET @OUTPUT = ''" & vbCrLf
sConsulta = sConsulta & "               END" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "   -- APERTURA DEL CURSOR" & vbCrLf
sConsulta = sConsulta & "   OPEN CCENTRODECOSTE" & vbCrLf
sConsulta = sConsulta & "   FETCH CCENTRODECOSTE INTO @UON1,@UON2,@UON3,@UON4   " & vbCrLf
sConsulta = sConsulta & "  " & vbCrLf
sConsulta = sConsulta & "   WHILE (@@FETCH_STATUS = 0 )" & vbCrLf
sConsulta = sConsulta & "       BEGIN           " & vbCrLf
sConsulta = sConsulta & "       INSERT INTO USU_CC_IMPUTACION (USU,UON1,UON2,UON3,UON4) VALUES(@CODUSUARIO,@UON1,@UON2,@UON3,@UON4)" & vbCrLf
sConsulta = sConsulta & "        -- LECTURA DE LA SIGUIENTE FILA DEL CURSOR" & vbCrLf
sConsulta = sConsulta & "        FETCH CCENTRODECOSTE INTO @UON1,@UON2,@UON3,@UON4" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "   CLOSE CCENTRODECOSTE" & vbCrLf
sConsulta = sConsulta & "   DEALLOCATE CCENTRODECOSTE" & vbCrLf
sConsulta = sConsulta & "    END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

    
sConsulta = "IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_ACCESOS_USUARIOS_POR_HORA]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_ACCESOS_USUARIOS_POR_HORA]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta
    
sConsulta = "CREATE PROCEDURE [dbo].[FSAL_ACCESOS_USUARIOS_POR_HORA]  " & vbCrLf
sConsulta = sConsulta & "   @fecdesde DATETIME," & vbCrLf
sConsulta = sConsulta & "   @fechasta DATETIME," & vbCrLf
sConsulta = sConsulta & "   @entorno nvarchar(50)," & vbCrLf
sConsulta = sConsulta & "   @pyme nvarchar(50)=null" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @fechacomienzo DATETIME" & vbCrLf
sConsulta = sConsulta & "declare @numDiaSemanaHoras float" & vbCrLf
sConsulta = sConsulta & "SET @fechacomienzo=CONVERT(datetime,'2010-01-01')" & vbCrLf
sConsulta = sConsulta & "set @numDiaSemanaHoras = DATEDIFF(""ww"",@fechacomienzo,GETDATE());" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "with mycte as" & vbCrLf
sConsulta = sConsulta & "(" & vbCrLf
sConsulta = sConsulta & "    SELECT @fecdesde  AS MyDay" & vbCrLf
sConsulta = sConsulta & "    UNION ALL" & vbCrLf
sConsulta = sConsulta & "    SELECT dateadd(HOUR ,1,MyDay)" & vbCrLf
sConsulta = sConsulta & "    FROM mycte " & vbCrLf
sConsulta = sConsulta & "    WHERE dateadd(HOUR ,1,MyDay) <= @fechasta" & vbCrLf
sConsulta = sConsulta & ")" & vbCrLf
sConsulta = sConsulta & "SELECT distinct(ACCESOS.HORA_ACCESO), ISNULL(ACTIVOS_GS,0) AS ACTIVOS_GS, ISNULL(ACTIVOS_WEB,0) AS ACTIVOS_WEB, ISNULL(ACTIVOS_PORTAL,0) AS ACTIVOS_PORTAL, ISNULL(TOTAL_GS,0) AS TOTAL_GS, ISNULL(TOTAL_WEB,0) AS TOTAL_WEB, ISNULL(TOTAL_PORTAL,0) AS TOTAL_PORTAL, ISNULL(AVG_GS,0) AS AVG_GS, ISNULL(AVG_WEB,0) AS AVG_WEB, ISNULL(AVG_PORTAL,0) AS AVG_PORTAL FROM" & vbCrLf
sConsulta = sConsulta & "(SELECT right(replicate(0,2) + ltrim(DATEPART(""HH"",MyDay)),2)+':00' AS HORA_ACCESO" & vbCrLf
sConsulta = sConsulta & "from mycte) ACCESOS" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN " & vbCrLf
sConsulta = sConsulta & "(" & vbCrLf
sConsulta = sConsulta & "(SELECT ID, RIGHT(replicate(0,2) + LTRIM(HORA),2)+':00' AS HORA_ACCESO, ENTORNO, ACTIVOS_GS, ACTIVOS_WEB, ACTIVOS_PORTAL, TOTAL_GS, TOTAL_WEB, TOTAL_PORTAL" & vbCrLf
sConsulta = sConsulta & "FROM FSAL_USUARIOS_HORA WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE @fecdesde <= FECHA and @fechasta>= FECHA AND ENTORNO=@entorno " & vbCrLf
sConsulta = sConsulta & ") DATOS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "LEFT join (" & vbCrLf
sConsulta = sConsulta & "SELECT right(replicate(0,2)+LTRIM(HORA),2)+':00' AS HORA_ACCESO,  Round(sum(convert(float,ACTIVOS_GS))/@numDiaSemanaHoras, 2, 0) AS AVG_GS, round(sum(convert(float,ACTIVOS_WEB))/@numDiaSemanaHoras, 2, 0) AS AVG_WEB, round(sum(convert(float,ACTIVOS_PORTAL))/@numDiaSemanaHoras, 2, 0) AS AVG_PORTAL " & vbCrLf
sConsulta = sConsulta & "FROM FSAL_USUARIOS_HORA WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE ENTORNO = @entorno " & vbCrLf
sConsulta = sConsulta & "AND isnull(PYME,'') =isnull(@pyme,'') " & vbCrLf
sConsulta = sConsulta & "AND datepart(""dw"",fecha)= DATEPART(""dw"",@fecdesde)" & vbCrLf
sConsulta = sConsulta & "GROUP BY HORA ) MEDIAS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ON DATOS.HORA_ACCESO = MEDIAS.HORA_ACCESO )" & vbCrLf
sConsulta = sConsulta & "ON ACCESOS.HORA_ACCESO= DATOS.HORA_ACCESO" & vbCrLf
sConsulta = sConsulta & "order by accesos.HORA_ACCESO" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
    
sConsulta = "IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_ESTADISTICAS_ENTORNO]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_ESTADISTICAS_ENTORNO]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta
    
sConsulta = "CREATE PROCEDURE [dbo].[FSAL_ESTADISTICAS_ENTORNO]  " & vbCrLf
sConsulta = sConsulta & "   @DESDE datetime," & vbCrLf
sConsulta = sConsulta & "   @HASTA datetime," & vbCrLf
sConsulta = sConsulta & "   @ENTORNO nvarchar(50)=NULL," & vbCrLf
sConsulta = sConsulta & "   @PYME nvarchar(50)=NULL" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "set @DESDE= dbo.fn5MIN(dateadd(""mi"",-5,@DESDE))" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "Set datefirst 1" & vbCrLf
sConsulta = sConsulta & "DECLARE @RC int" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "declare @ANYO datetime" & vbCrLf
sConsulta = sConsulta & "set @ANYO = @DESDE" & vbCrLf
sConsulta = sConsulta & "while  datepart(""yy"",@ANYO) <= datepart(""yy"",@HASTA)" & vbCrLf
sConsulta = sConsulta & "begin " & vbCrLf
sConsulta = sConsulta & "   -- Comienzo Bucle A�o" & vbCrLf
sConsulta = sConsulta & "   declare @MES datetime" & vbCrLf
sConsulta = sConsulta & "   set @MES = @ANYO" & vbCrLf
sConsulta = sConsulta & "   while  datepart(""yy"",@MES) <= datepart(""yy"",@ANYO) AND  @MES <= @HASTA" & vbCrLf
sConsulta = sConsulta & "   begin " & vbCrLf
sConsulta = sConsulta & "       -- Comienzo Bucle Mes" & vbCrLf
sConsulta = sConsulta & "       declare @DIA datetime" & vbCrLf
sConsulta = sConsulta & "       set @DIA = @Mes" & vbCrLf
sConsulta = sConsulta & "       while  datepart(""mm"",@DIA) <= datepart(""mm"",@MES) AND  @DIA <= @HASTA" & vbCrLf
sConsulta = sConsulta & "       begin " & vbCrLf
sConsulta = sConsulta & "           -- Comienzo Bucle Dia" & vbCrLf
sConsulta = sConsulta & "           declare @HORA datetime" & vbCrLf
sConsulta = sConsulta & "           set @HORA = @DIA" & vbCrLf
sConsulta = sConsulta & "           while  datepart(""dd"",@HORA) <= datepart(""dd"",@DIA) AND  @HORA < @HASTA" & vbCrLf
sConsulta = sConsulta & "           begin " & vbCrLf
sConsulta = sConsulta & "               -- Comienzo Bucle HORA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "               EXECUTE @RC = [dbo].[FSAL_ESTADISTICAS_ENTORNO_HORA] " & vbCrLf
sConsulta = sConsulta & "                  @HORA" & vbCrLf
sConsulta = sConsulta & "                 ,@HASTA" & vbCrLf
sConsulta = sConsulta & "                 ,@ENTORNO" & vbCrLf
sConsulta = sConsulta & "                 ,@PYME" & vbCrLf
sConsulta = sConsulta & "               -- Fin Bucle HORA" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "           set @HORA = dateadd(hour,1,@HORA)       " & vbCrLf
sConsulta = sConsulta & "           set @HORA = convert(datetime,convert(varchar(13),@HORA,121) + ':00')" & vbCrLf
sConsulta = sConsulta & "           end " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "           EXECUTE @RC = [dbo].[FSAL_ESTADISTICAS_ENTORNO_DIA] " & vbCrLf
sConsulta = sConsulta & "               @DIA" & vbCrLf
sConsulta = sConsulta & "               ,@ENTORNO" & vbCrLf
sConsulta = sConsulta & "               ,@PYME" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "           -- Calculamos la semana para procesamiento de la misma" & vbCrLf
sConsulta = sConsulta & "           -- s�lo cuando cambiamos de semana o se hayan procesado todos los d�as" & vbCrLf
sConsulta = sConsulta & "           if (datepart(""dw"",@DIA) = 7 ) or ( datepart(""yy"",@DIA) = datepart(""yy"",@HASTA) AND datepart(""mm"",@DIA) = datepart(""mm"",@HASTA) AND  datepart(""dd"",@DIA) = datepart(""dd"",@HASTA))" & vbCrLf
sConsulta = sConsulta & "           begin" & vbCrLf
sConsulta = sConsulta & "           Declare @PrimerDiaSemana datetime" & vbCrLf
sConsulta = sConsulta & "           set @PrimerDiaSemana = dateadd(day,-1*(datepart(""dw"",@DIA)-1),@Dia)" & vbCrLf
sConsulta = sConsulta & "           EXECUTE @RC = [dbo].[FSAL_ESTADISTICAS_ENTORNO_SEMANA] " & vbCrLf
sConsulta = sConsulta & "               @PrimerDiaSemana" & vbCrLf
sConsulta = sConsulta & "               ,@ENTORNO" & vbCrLf
sConsulta = sConsulta & "               ,@PYME" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "           end" & vbCrLf
sConsulta = sConsulta & "           -- Fin Bucle Dia" & vbCrLf
sConsulta = sConsulta & "       set @DIA = dateadd(day,1,@DIA)      " & vbCrLf
sConsulta = sConsulta & "       set @DIA = convert(datetime,convert(varchar(10),@DIA,102))" & vbCrLf
sConsulta = sConsulta & "       end " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       EXECUTE @RC = [dbo].[FSAL_ESTADISTICAS_ENTORNO_MES] " & vbCrLf
sConsulta = sConsulta & "           @MES" & vbCrLf
sConsulta = sConsulta & "           ,@ENTORNO" & vbCrLf
sConsulta = sConsulta & "           ,@PYME" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       -- Fin Bucle Mes" & vbCrLf
sConsulta = sConsulta & "   set @MES = dateadd(month,1,@MES)" & vbCrLf
sConsulta = sConsulta & "   set @MES = dateadd(day,-1*(datepart(""dd"",@MES)-1),@MES)" & vbCrLf
sConsulta = sConsulta & "   end " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   EXECUTE @RC = [dbo].[FSAL_ESTADISTICAS_ENTORNO_ANYO] " & vbCrLf
sConsulta = sConsulta & "       @ANYO" & vbCrLf
sConsulta = sConsulta & "       ,@ENTORNO" & vbCrLf
sConsulta = sConsulta & "       ,@PYME" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   -- Fin Bucle A�o" & vbCrLf
sConsulta = sConsulta & "set @ANYO = dateadd(year,1,@ANYO)" & vbCrLf
sConsulta = sConsulta & "set @ANYO = dateadd(month,-1*(datepart(""mm"",@ANYO)-1),@ANYO)" & vbCrLf
sConsulta = sConsulta & "end " & vbCrLf
ExecuteSQL gRDOCon, sConsulta

    
sConsulta = "IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_ESTADISTICAS_EVENTOS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_ESTADISTICAS_EVENTOS]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta
    
sConsulta = "CREATE PROCEDURE [dbo].[FSAL_ESTADISTICAS_EVENTOS]  " & vbCrLf
sConsulta = sConsulta & "   @DESDE datetime," & vbCrLf
sConsulta = sConsulta & "   @HASTA datetime," & vbCrLf
sConsulta = sConsulta & "   @ENTORNO nvarchar(50)," & vbCrLf
sConsulta = sConsulta & "   @PYME nvarchar(50)" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "set @DESDE= dbo.fn5MIN(dateadd(""mi"",-5,@DESDE))" & vbCrLf
sConsulta = sConsulta & "Set datefirst 1" & vbCrLf
sConsulta = sConsulta & "DECLARE @RC int" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "declare @ANYO datetime" & vbCrLf
sConsulta = sConsulta & "set @ANYO = @DESDE" & vbCrLf
sConsulta = sConsulta & "while  datepart(""yy"",@ANYO) <= datepart(""yy"",@HASTA)" & vbCrLf
sConsulta = sConsulta & "begin " & vbCrLf
sConsulta = sConsulta & "   -- Comienzo Bucle A�o" & vbCrLf
sConsulta = sConsulta & "   declare @MES datetime" & vbCrLf
sConsulta = sConsulta & "   set @MES = @ANYO" & vbCrLf
sConsulta = sConsulta & "   while  datepart(""yy"",@MES) <= datepart(""yy"",@ANYO) AND  @MES <= @HASTA" & vbCrLf
sConsulta = sConsulta & "   begin " & vbCrLf
sConsulta = sConsulta & "       -- Comienzo Bucle Mes" & vbCrLf
sConsulta = sConsulta & "       declare @DIA datetime" & vbCrLf
sConsulta = sConsulta & "       set @DIA = @Mes" & vbCrLf
sConsulta = sConsulta & "       while  datepart(""mm"",@DIA) <= datepart(""mm"",@MES) AND  @DIA <= @HASTA" & vbCrLf
sConsulta = sConsulta & "       begin " & vbCrLf
sConsulta = sConsulta & "           -- Comienzo Bucle Dia" & vbCrLf
sConsulta = sConsulta & "           declare @HORA datetime" & vbCrLf
sConsulta = sConsulta & "           set @HORA = @DIA" & vbCrLf
sConsulta = sConsulta & "           while  datepart(""dd"",@HORA) <= datepart(""dd"",@DIA) AND  @HORA < @HASTA" & vbCrLf
sConsulta = sConsulta & "           begin " & vbCrLf
sConsulta = sConsulta & "               -- Comienzo Bucle HORA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "               EXECUTE @RC = [dbo].[FSAL_ESTADISTICAS_EVENTO_HORA] " & vbCrLf
sConsulta = sConsulta & "                  @HORA" & vbCrLf
sConsulta = sConsulta & "                 ,@HASTA" & vbCrLf
sConsulta = sConsulta & "                 ,@ENTORNO" & vbCrLf
sConsulta = sConsulta & "                 ,@PYME" & vbCrLf
sConsulta = sConsulta & "               -- Fin Bucle HORA" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "           set @HORA = dateadd(hour,1,@HORA)       " & vbCrLf
sConsulta = sConsulta & "           set @HORA = convert(datetime,convert(varchar(13),@HORA,121) + ':00')" & vbCrLf
sConsulta = sConsulta & "           end " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "           -- Fin Bucle Dia" & vbCrLf
sConsulta = sConsulta & "       set @DIA = dateadd(day,1,@DIA)      " & vbCrLf
sConsulta = sConsulta & "       set @DIA = convert(datetime,convert(varchar(10),@DIA,102))" & vbCrLf
sConsulta = sConsulta & "       end " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       -- Fin Bucle Mes" & vbCrLf
sConsulta = sConsulta & "   set @MES = dateadd(month,1,@MES)" & vbCrLf
sConsulta = sConsulta & "   set @MES = dateadd(day,-1*(datepart(""dd"",@MES)-1),@MES)" & vbCrLf
sConsulta = sConsulta & "   end " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   -- Fin Bucle A�o" & vbCrLf
sConsulta = sConsulta & "set @ANYO = dateadd(year,1,@ANYO)" & vbCrLf
sConsulta = sConsulta & "set @ANYO = dateadd(month,-1*(datepart(""mm"",@ANYO)-1),@ANYO)" & vbCrLf
sConsulta = sConsulta & "end " & vbCrLf
ExecuteSQL gRDOCon, sConsulta


    
End Sub


Public Function CodigoDeActualizacion31900_08_1892_A31900_08_1893() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim oADOConnection As ADODB.Connection
    Dim sConsulta As String

    On Error GoTo Error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"


    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Actualiza Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 3
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    frmProgreso.ProgressBar1.Refresh
    V_31900_8_Storeds_093
    
    
    sConsulta = "UPDATE VERSION SET NUM ='3.00.18.93'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103), PRODVER='31900.8'"
    ExecuteSQL gRDOCon, sConsulta
        
    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False
        
    CodigoDeActualizacion31900_08_1892_A31900_08_1893 = True
    Exit Function

Error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    CodigoDeActualizacion31900_08_1892_A31900_08_1893 = False
End Function

Public Sub V_31900_8_Storeds_093()
    Dim sConsulta As String

sConsulta = "IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSWS_ARTICULOS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSWS_ARTICULOS]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSWS_ARTICULOS] " & vbCrLf
sConsulta = sConsulta & "   @COD VARCHAR(50)=NULL, " & vbCrLf
sConsulta = sConsulta & "   @DEN VARCHAR(2000)=NULL, " & vbCrLf
sConsulta = sConsulta & "   @GMN1 VARCHAR(50)=NULL, " & vbCrLf
sConsulta = sConsulta & "   @GMN2 VARCHAR(50)=NULL, " & vbCrLf
sConsulta = sConsulta & "   @GMN3 VARCHAR(50)=NULL, " & vbCrLf
sConsulta = sConsulta & "   @GMN4 VARCHAR(50)=NULL, " & vbCrLf
sConsulta = sConsulta & "   @COINCID TINYINT=1, " & vbCrLf
sConsulta = sConsulta & "   @PER VARCHAR(50)=NULL, " & vbCrLf
sConsulta = sConsulta & "   @FILAS INT=NULL, " & vbCrLf
sConsulta = sConsulta & "   @DESDEFILA INT=NULL, " & vbCrLf
sConsulta = sConsulta & "   @IDI VARCHAR(50)='SPA', " & vbCrLf
sConsulta = sConsulta & "   @INSTANCIAMONEDA VARCHAR(3)='EUR'," & vbCrLf
sConsulta = sConsulta & "   @CARGAR_ULT_ADJ TINYINT=0, " & vbCrLf
sConsulta = sConsulta & "   @GENERICOS TINYINT=2, " & vbCrLf
sConsulta = sConsulta & "   @CODORGCOMPRAS VARCHAR(4)=NULL," & vbCrLf
sConsulta = sConsulta & "   @CODCENTRO VARCHAR(4)=NULL, " & vbCrLf
sConsulta = sConsulta & "   @CODINI VARCHAR(50)=NULL, " & vbCrLf
sConsulta = sConsulta & "   @CODULT VARCHAR(50)=NULL, " & vbCrLf
sConsulta = sConsulta & "   @CARGAR_CC TINYINT=0, " & vbCrLf
sConsulta = sConsulta & "   @CARGAR_ORG TINYINT=0," & vbCrLf
sConsulta = sConsulta & "   @GASTO TINYINT=0, " & vbCrLf
sConsulta = sConsulta & "   @INVERSION TINYINT=0, " & vbCrLf
sConsulta = sConsulta & "   @GASTOINVERSION TINYINT=0, " & vbCrLf
sConsulta = sConsulta & "   @NOALMACENABLE TINYINT=0, " & vbCrLf
sConsulta = sConsulta & "   @ALMACENABLE TINYINT=0, " & vbCrLf
sConsulta = sConsulta & "   @ALMACENOPCIONAL TINYINT=0, " & vbCrLf
sConsulta = sConsulta & "   @RECEPCIONOBLIG TINYINT=0, " & vbCrLf
sConsulta = sConsulta & "   @NORECEPCIONABLE TINYINT=0, " & vbCrLf
sConsulta = sConsulta & "   @RECEPCIONOPCIONAL TINYINT=0," & vbCrLf
sConsulta = sConsulta & "   @MATERIALESCATALOGADOS TINYINT=0," & vbCrLf
sConsulta = sConsulta & "   @RESTRICMATUSU INT=0," & vbCrLf
sConsulta = sConsulta & "   @FILTRARNEGOCIADOS INT=0," & vbCrLf
sConsulta = sConsulta & "   @CODPROVEEDOR VARCHAR(50)=NULL," & vbCrLf
sConsulta = sConsulta & "   @CONFPAGO AS TINYINT=0," & vbCrLf
sConsulta = sConsulta & "   @USAR_ORG AS TINYINT=0," & vbCrLf
sConsulta = sConsulta & "   @ACCESO_SM AS TINYINT=0," & vbCrLf
sConsulta = sConsulta & "   @UON1 NVARCHAR(15)=NULL," & vbCrLf
sConsulta = sConsulta & "   @UON2 NVARCHAR(15)=NULL," & vbCrLf
sConsulta = sConsulta & "   @UON3 NVARCHAR(15)=NULL," & vbCrLf
sConsulta = sConsulta & "   @UON4 NVARCHAR(15)=NULL     " & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @RESTRIC TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @MONCEN AS NVARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @EQUIV AS FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @EQUIVINS AS FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @RESTRIC_PLANIF TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @EQP VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL2 AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL3 AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSELPAG AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLJOINPAG_P AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLJOINPAG_D AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PARAM AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @RESTRIC = 0" & vbCrLf
sConsulta = sConsulta & "IF @PER IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "    SELECT @RESTRIC=ISNULL(ISNULL(USU_ACC.ACC,PERF_ACC.ACC),0),@EQP=U.EQP  " & vbCrLf
sConsulta = sConsulta & "    FROM USU U WITH (NOLOCK)  " & vbCrLf
sConsulta = sConsulta & "    LEFT JOIN USU_ACC WITH(NOLOCK) ON USU_ACC.USU=U.COD AND USU_ACC.ACC=@RESTRICMATUSU" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN PERF_ACC WITH(NOLOCK) ON PERF_ACC.PERF=U.PERF AND PERF_ACC.ACC=@RESTRICMATUSU" & vbCrLf
sConsulta = sConsulta & "    WHERE U.PER=@PER AND U.BAJA=0" & vbCrLf
sConsulta = sConsulta & "IF @RESTRIC>0" & vbCrLf
sConsulta = sConsulta & "   SET @RESTRIC=1" & vbCrLf
sConsulta = sConsulta & "if @desdefila = 0" & vbCrLf
sConsulta = sConsulta & "   set @desdefila = null" & vbCrLf
sConsulta = sConsulta & "if @FILAS=0" & vbCrLf
sConsulta = sConsulta & "   set @FILAS = null" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @RESTRIC_PLANIF= (select TRASPASO_ART_PLANIF from PARGEN_INTERNO WITH (NOLOCK))" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL2 = N'SELECT DISTINCT TOP ' + CAST(ISNULL(@FILAS,100) AS NVARCHAR(10)) + N' A.COD,A.DEN,A.GENERICO,A.CONCEPTO, A.ALMACENAR, A.RECEPCIONAR,A.GMN1,A.GMN2,A.GMN3,A.GMN4,A.UNI,G4.DEN_' + @IDI + ' DENGMN ,UNI_DEN.DEN DENUNI, UNI.NUMDEC'" & vbCrLf
sConsulta = sConsulta & "SET @SQL2=@SQL2 + ', A.PRES5_NIV0, A.PRES5_NIV1,A.PRES5_NIV2,A.PRES5_NIV3, A.PRES5_NIV4,PERM_EP.PERMISO_EP,CATALOGADO.ESTA_CATALOGADO, '''' AS PREC_VIGENTE'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @MONCEN = (SELECT MONCEN FROM PARGEN_DEF WITH (NOLOCK))" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CARGAR_ULT_ADJ = 1 AND NOT(@FILTRARNEGOCIADOS=1)" & vbCrLf
sConsulta = sConsulta & "BEGIN     " & vbCrLf
sConsulta = sConsulta & "    SET @EQUIV=(SELECT EQUIV FROM MON  WITH (NOLOCK)  WHERE COD=@MONCEN)" & vbCrLf
sConsulta = sConsulta & "    SET @EQUIVINS=(SELECT EQUIV FROM MON WITH (NOLOCK) WHERE COD=@INSTANCIAMONEDA)" & vbCrLf
sConsulta = sConsulta & "    SET @SQL2 = @SQL2 + ' , (AA.PREC/@EQUIV*@EQUIVINS) PREC_ULT_ADJ,AA.CODPROVE CODPROV_ULT_ADJ,AA.DENPROVE DENPROV_ULT_ADJ'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF (@FILTRARNEGOCIADOS=1)" & vbCrLf
sConsulta = sConsulta & "BEGIN      " & vbCrLf
sConsulta = sConsulta & "   --Art4_adj. Guarda en moneda central. Pero lo q se ha de ver es precio en moneda proveedor" & vbCrLf
sConsulta = sConsulta & "    SET @SQL2 = @SQL2 + ', NEGO.PREC*(NEGO.CAMBIO_OFE*NEGO.CAMBIO_PROCE) PREC_ULT_ADJ,NEGO.CODPROVE CODPROV_ULT_ADJ,NEGO.DENPROVE DENPROV_ULT_ADJ'" & vbCrLf
sConsulta = sConsulta & "    SET @SQL2 = @SQL2 + ', NEGO.UNI AS UNI_ULT_ADJ, NEGO.DEN AS DENUNI_ULT_ADJ, NEGO.MON AS MON_ULT_ADJ, NEGO.DENMON AS DENMON_ULT_ADJ'" & vbCrLf
sConsulta = sConsulta & "    --Junto con el proveedor viene la forma de pago. As� q con cada cambio de proveedor en pedidos negociados has de cambiar la forma pago , DE HABER este campo" & vbCrLf
sConsulta = sConsulta & "    IF @CONFPAGO = 1    " & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       --se cargar� con la forma de pago asociada al proveedor. Bien prove_erp o bien prove" & vbCrLf
sConsulta = sConsulta & "       SET @SQL2 = @SQL2 + ', NEGO.PAG PAG_ULT_ADJ, NEGO.DENPAG DENPAG_ULT_ADJ'        " & vbCrLf
sConsulta = sConsulta & "    END " & vbCrLf
sConsulta = sConsulta & "    " & vbCrLf
sConsulta = sConsulta & "    SET @SQLSELPAG = N''" & vbCrLf
sConsulta = sConsulta & "    SET @SQLJOINPAG_P = N''" & vbCrLf
sConsulta = sConsulta & "    SET @SQLJOINPAG_D = N''" & vbCrLf
sConsulta = sConsulta & "    " & vbCrLf
sConsulta = sConsulta & "   DECLARE @DESDEERP TINYINT" & vbCrLf
sConsulta = sConsulta & "   DECLARE @EMPRESA INT" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   SET @DESDEERP = 0   " & vbCrLf
sConsulta = sConsulta & "   SET @EMPRESA= NULL" & vbCrLf
sConsulta = sConsulta & "    " & vbCrLf
sConsulta = sConsulta & "    --Junto con el proveedor viene la forma de pago. As� q con cada cambio de proveedor en pedidos negociados has de cambiar la forma pago    " & vbCrLf
sConsulta = sConsulta & "   IF @CONFPAGO = 1    " & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       DECLARE @ERP INT" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "       --La carga de la forma de pago ser� similar a la que se realiza en la emisi�n de pedidos desde GS:" & vbCrLf
sConsulta = sConsulta & "       --Cogido de FSWS_PROVES" & vbCrLf
sConsulta = sConsulta & "       IF (@USAR_ORG=1 OR @ACCESO_SM=1) " & vbCrLf
sConsulta = sConsulta & "       BEGIN --Si no usas org compras o partida presupuestaria, no hay de donde sacar empresa/erp          " & vbCrLf
sConsulta = sConsulta & "           IF (SELECT ISNULL(TRASPASO_PED_ERP,0) FROM PARGEN_INTERNO WITH (NOLOCK) WHERE ID=1)=1" & vbCrLf
sConsulta = sConsulta & "           BEGIN   --Visto en GS q si NO se cumple esto no hay nada q hacer con ERP            " & vbCrLf
sConsulta = sConsulta & "               IF (SELECT COUNT(1) FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA =13 AND ACTIVA=1 AND (SENTIDO=1 OR SENTIDO=3))>0" & vbCrLf
sConsulta = sConsulta & "               BEGIN--Si la integraci�n de pedidos (en sentido FULLSTEP->ERP o FULLSTEP<->ERP) est� activada se cargar� por defecto la forma de pago " & vbCrLf
sConsulta = sConsulta & "               --del proveedor en el ERP. Si este dato no est� indicado se tomar� la forma de pago del proveedor en FULLSTEP" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                   SET @ERP= NULL" & vbCrLf
sConsulta = sConsulta & "                                               " & vbCrLf
sConsulta = sConsulta & "                   IF @ACCESO_SM=1 --si est� activado el SM se obtendr� la empresa de la partida presupuestaria seleccionada" & vbCrLf
sConsulta = sConsulta & "                   BEGIN" & vbCrLf
sConsulta = sConsulta & "                       --La partida es, por ejemplo: " & vbCrLf
sConsulta = sConsulta & "                       --  Uon1#Uon2..#Pres1|Pres2..." & vbCrLf
sConsulta = sConsulta & "                       --  Uon1#Pres1" & vbCrLf
sConsulta = sConsulta & "                       --Es decir dispongo en codigo del UON. As� pues se lo puedo pasar a este stored. " & vbCrLf
sConsulta = sConsulta & "                       --Las empresas estan en UON" & vbCrLf
sConsulta = sConsulta & "                       SET @EMPRESA= 0" & vbCrLf
sConsulta = sConsulta & "                       " & vbCrLf
sConsulta = sConsulta & "                       SELECT @EMPRESA=ISNULL(U4.EMPRESA,0) FROM UON4 U4 WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                       WHERE U4.UON1=@UON1 AND U4.UON2=@UON2 AND U4.UON3=@UON3 AND U4.COD=@UON4" & vbCrLf
sConsulta = sConsulta & "                               " & vbCrLf
sConsulta = sConsulta & "                       IF @EMPRESA=0" & vbCrLf
sConsulta = sConsulta & "                           SELECT @EMPRESA=ISNULL(U3.EMPRESA,0) FROM UON3 U3 WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                           WHERE U3.UON1=@UON1 AND U3.UON2=@UON2 AND U3.COD=@UON3" & vbCrLf
sConsulta = sConsulta & "                                   " & vbCrLf
sConsulta = sConsulta & "                       IF @EMPRESA=0" & vbCrLf
sConsulta = sConsulta & "                           SELECT @EMPRESA=ISNULL(U2.EMPRESA,0) FROM UON2 U2 WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                           WHERE U2.UON1=@UON1 AND U2.COD=@UON2" & vbCrLf
sConsulta = sConsulta & "                                   " & vbCrLf
sConsulta = sConsulta & "                       IF @EMPRESA=0" & vbCrLf
sConsulta = sConsulta & "                           SELECT @EMPRESA=ISNULL(U1.EMPRESA,0) FROM UON1 U1 WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                           WHERE U1.COD=@UON1                              " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                       IF @EMPRESA=0" & vbCrLf
sConsulta = sConsulta & "                           SET @EMPRESA= NULL" & vbCrLf
sConsulta = sConsulta & "                       " & vbCrLf
sConsulta = sConsulta & "                       --Muy bien, en frmpedidos de GS veo q la carga ERP se hace con la funci�n CERPInt.ObtenerCodERP" & vbCrLf
sConsulta = sConsulta & "                       --si le pasa una empresa, esta es la select" & vbCrLf
sConsulta = sConsulta & "                       SELECT @ERP=COD " & vbCrLf
sConsulta = sConsulta & "                       FROM ERP E WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                       INNER JOIN ERP_SOCIEDAD EO WITH (NOLOCK) ON EO.ERP=E.COD" & vbCrLf
sConsulta = sConsulta & "                       INNER JOIN EMP WITH(NOLOCK) ON EMP.SOCIEDAD=EO.SOCIEDAD" & vbCrLf
sConsulta = sConsulta & "                       WHERE EMP.ERP=1 AND EMP.ID =@EMPRESA        " & vbCrLf
sConsulta = sConsulta & "                                               " & vbCrLf
sConsulta = sConsulta & "                   END                             " & vbCrLf
sConsulta = sConsulta & "                   ELSE" & vbCrLf
sConsulta = sConsulta & "                   BEGIN" & vbCrLf
sConsulta = sConsulta & "                       IF @CODORGCOMPRAS IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                       BEGIN" & vbCrLf
sConsulta = sConsulta & "                           --Vamos a ver en GESTAMP, es una organizaci�n UNA empresa" & vbCrLf
sConsulta = sConsulta & "                           --Vamos a ver en FERROVIAL, es una organizaci�n VARIAS empresas" & vbCrLf
sConsulta = sConsulta & "                           --���Problema??? Pues comentado con Mertxe/Teresa/Encarni, lo q tiene sentido es lo de gestamp y Ferr lo hacen de " & vbCrLf
sConsulta = sConsulta & "                           --otra manera... nunca usa org. OK a top 1." & vbCrLf
sConsulta = sConsulta & "                           SELECT TOP 1 @EMPRESA=ID FROM EMP WITH(NOLOCK) WHERE ORG_COMPRAS=@CODORGCOMPRAS ORDER BY ID ASC" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                           --Muy bien, en frmPedidos de GS veo q la carga ERP se hace con la funci�n CERPInt.ObtenerCodERP" & vbCrLf
sConsulta = sConsulta & "                           --si le pasa una org compra, esta es la select                      " & vbCrLf
sConsulta = sConsulta & "                           SELECT TOP 1 @ERP=COD FROM ERP E WITH (NOLOCK) INNER JOIN ERP_ORGCOMPRAS EO WITH (NOLOCK) ON EO.ERP=E.COD " & vbCrLf
sConsulta = sConsulta & "                           WHERE EO.ORGCOMPRAS =@CODORGCOMPRAS                 " & vbCrLf
sConsulta = sConsulta & "                       END" & vbCrLf
sConsulta = sConsulta & "                   END                                                 " & vbCrLf
sConsulta = sConsulta & "                                                               " & vbCrLf
sConsulta = sConsulta & "                   IF @ERP IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                   BEGIN --integraci�n de pedidos del erp en concreto. Ver si esta activo o no y en sentido exportar o no." & vbCrLf
sConsulta = sConsulta & "                       IF (SELECT COUNT(1) FROM TABLAS_INTEGRACION_ERP TIE WITH (NOLOCK) WHERE TIE.ERP = @ERP AND TIE.ENTIDAD=13 AND ISNULL(TIE.ACTIVA,0)=1 AND ((ISNULL(TIE.SENTIDO,0)=1) OR (ISNULL(TIE.SENTIDO,0)=3)))>0" & vbCrLf
sConsulta = sConsulta & "                       BEGIN --integraci�n de pedidos del erp en concreto. Ver si esta activo o no y en sentido exportar o no. Lo esta.            " & vbCrLf
sConsulta = sConsulta & "                           SET @DESDEERP = 1" & vbCrLf
sConsulta = sConsulta & "                           SET @SQLJOINPAG_P = N' INNER JOIN PROVE WITH(NOLOCK) ON PROVE.COD=AA.CODPROVE '" & vbCrLf
sConsulta = sConsulta & "                           SET @SQLJOINPAG_P = @SQLJOINPAG_P + ' LEFT JOIN PROVE_ERP ERP WITH(NOLOCK) ON ERP.NIF=PROVE.NIF AND ERP.EMPRESA=@EMPRESA'                   " & vbCrLf
sConsulta = sConsulta & "                           SET @SQLJOINPAG_D = N' LEFT JOIN PAG_DEN PD WITH(NOLOCK) ON ISNULL(PROVE_ERP.PAG,PROVE.PAG)=PD.PAG AND PD.IDIOMA=@IDI'              " & vbCrLf
sConsulta = sConsulta & "                       END" & vbCrLf
sConsulta = sConsulta & "                       ELSE" & vbCrLf
sConsulta = sConsulta & "                       BEGIN --integraci�n de pedidos del erp en concreto. Ver si esta activo o no y en sentido exportar o no. NO lo esta." & vbCrLf
sConsulta = sConsulta & "                           SET @SQLJOINPAG_P = N' INNER JOIN PROVE WITH(NOLOCK) ON PROVE.COD=AA.CODPROVE '" & vbCrLf
sConsulta = sConsulta & "                           SET @SQLJOINPAG_D = N' LEFT JOIN PAG_DEN PD WITH(NOLOCK) ON PROVE.PAG=PD.PAG AND PD.IDIOMA=@IDI'                " & vbCrLf
sConsulta = sConsulta & "                       END             " & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "                   ELSE" & vbCrLf
sConsulta = sConsulta & "                   BEGIN --integraci�n de pedidos del erp en concreto. Ver si esta activo o no y en sentido exportar o no. NO lo esta pq no se ha encontrado ERP." & vbCrLf
sConsulta = sConsulta & "                       SET @SQLJOINPAG_P = N' INNER JOIN PROVE WITH(NOLOCK) ON PROVE.COD=AA.CODPROVE '" & vbCrLf
sConsulta = sConsulta & "                       SET @SQLJOINPAG_D = N' LEFT JOIN PAG_DEN PD WITH(NOLOCK) ON PROVE.PAG=PD.PAG AND PD.IDIOMA=@IDI' " & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               END     " & vbCrLf
sConsulta = sConsulta & "               ELSE --Si la integraci�n de pedidos (en sentido FULLSTEP->ERP o FULLSTEP<->ERP) no est� activada se cargar� por defecto la forma de pago del proveedor en FULLSTEP." & vbCrLf
sConsulta = sConsulta & "               BEGIN" & vbCrLf
sConsulta = sConsulta & "                   SET @SQLJOINPAG_P = N' INNER JOIN PROVE WITH(NOLOCK) ON PROVE.COD=AA.CODPROVE '" & vbCrLf
sConsulta = sConsulta & "                   SET @SQLJOINPAG_D = N' LEFT JOIN PAG_DEN PD WITH(NOLOCK) ON PROVE.PAG=PD.PAG AND PD.IDIOMA=@IDI'            " & vbCrLf
sConsulta = sConsulta & "               END" & vbCrLf
sConsulta = sConsulta & "           END " & vbCrLf
sConsulta = sConsulta & "           ELSE--Visto en GS q si NO se cumple esto no hay nada q hacer con ERP            " & vbCrLf
sConsulta = sConsulta & "           BEGIN" & vbCrLf
sConsulta = sConsulta & "               SET @SQLJOINPAG_P = N' INNER JOIN PROVE WITH(NOLOCK) ON PROVE.COD=AA.CODPROVE '" & vbCrLf
sConsulta = sConsulta & "               SET @SQLJOINPAG_D = N' LEFT JOIN PAG_DEN PD WITH(NOLOCK) ON PROVE.PAG=PD.PAG AND PD.IDIOMA=@IDI'    " & vbCrLf
sConsulta = sConsulta & "           END " & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "       ELSE --Si no usas org compras o partida presupuestaria, no hay de donde sacar empresa/erp" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           SET @SQLJOINPAG_P = N' INNER JOIN PROVE WITH(NOLOCK) ON PROVE.COD=AA.CODPROVE '" & vbCrLf
sConsulta = sConsulta & "           SET @SQLJOINPAG_D = N' LEFT JOIN PAG_DEN PD WITH(NOLOCK) ON PROVE.PAG=PD.PAG AND PD.IDIOMA=@IDI' " & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "       --se cargar� con la forma de pago asociada al proveedor. Bien prove_erp o bien prove" & vbCrLf
sConsulta = sConsulta & "       IF @DESDEERP=1 --prove_erp" & vbCrLf
sConsulta = sConsulta & "           SET @SQLSELPAG = N', ISNULL(PROVE_ERP.PAG,PROVE.PAG) PAG, PD.DEN DENPAG'                                    " & vbCrLf
sConsulta = sConsulta & "       ELSE --prove" & vbCrLf
sConsulta = sConsulta & "           SET @SQLSELPAG = N', PROVE.PAG PAG, PD.DEN DENPAG'              " & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "  " & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CODORGCOMPRAS IS NOT NULL OR @CARGAR_ORG=1" & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @SQL2 =@SQL2 +  N' ,isNull(u3.orgcompras,isNull(u2.orgcompras,u1.orgcompras)) CODORGCOMPRAS,o.den DENORGCOMPRAS,isNull(u3.centros,isNull(u2.centros,u1.centros)) CODCENTRO,c.den DENCENTRO '" & vbCrLf
sConsulta = sConsulta & "    END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CARGAR_CC = 1" & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @SQL2 =@SQL2 +  N' ,isNull(A.PRES5_NIV4,isNull(A.PRES5_NIV3,isNull(A.PRES5_NIV2,isNull(A.PRES5_NIV1,A.PRES5_NIV0)))) CC '" & vbCrLf
sConsulta = sConsulta & "    END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N' FROM ART4 A WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN GMN4 G4 WITH (NOLOCK) ON A.GMN1= G4.GMN1 AND A.GMN2= G4.GMN2 AND A.GMN3= G4.GMN3 AND A.GMN4= G4.COD" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN UNI_DEN WITH (NOLOCK) ON UNI_DEN.UNI = A.UNI AND UNI_DEN.IDIOMA= @IDI" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN UNI WITH(NOLOCK) ON A.UNI = UNI.COD '" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "--Pedidos negociados. La ultima adjudicaci�n solo para el proveedor seleccionado, no de todos como en el resto de casos" & vbCrLf
sConsulta = sConsulta & "IF @CARGAR_ULT_ADJ = 1 AND NOT(@FILTRARNEGOCIADOS=1)" & vbCrLf
sConsulta = sConsulta & "    SET @SQL = @SQL + '  LEFT JOIN ART4_ADJ AA WITH (NOLOCK) ON AA.ART = A.COD AND AA.ID = (SELECT MAX(ID) FROM ART4_ADJ WHERE ART4_ADJ.ART = A.COD) '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CODORGCOMPRAS IS NOT NULL OR @CARGAR_ORG=1" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N' INNER JOIN  ART4_UON AU WITH (NOLOCK) ON A.COD = AU.ART4 and AU.BAJALOG =0'" & vbCrLf
sConsulta = sConsulta & "   IF @RESTRIC_PLANIF = 1" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL +' AND CAMPO1=0 '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL +' INNER JOIN uon1 u1 WITH (NOLOCK) on u1.cod =au.uon1'" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL +' INNER JOIN uon2 u2 WITH (NOLOCK) on u2.cod = au.uon2 and u2.uon1 = au.uon1'" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL +' INNER JOIN uon3 u3 WITH (NOLOCK) on u3.cod = au.uon3 and u3.uon2 = au.uon2 and u3.uon1 = au.uon1'" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL +' INNER JOIN orgcompras o WITH (NOLOCK) on o.cod = isNull(u3.orgcompras,isNull(u2.orgcompras,u1.orgcompras))'" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL +' INNER JOIN Centros c  WITH (NOLOCK) on c.cod = isNull(u3.centros,isNull(u2.centros,u1.centros))'" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @RESTRIC = 1" & vbCrLf
sConsulta = sConsulta & "   SET @SQL =@SQL +  N' INNER JOIN COM_GMN4 CG  WITH (NOLOCK)  on CG.EQP=@EQP AND CG.COM = @PER AND A.GMN1 = CG.GMN1 AND A.GMN2 = CG.GMN2 AND A.GMN3 = CG.GMN3 AND A.GMN4 = CG.GMN4'" & vbCrLf
sConsulta = sConsulta & "   --INICIO A�ADIDO PARA TAREA 1739---------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "   SET @SQL =@SQL + ' LEFT JOIN " & vbCrLf
sConsulta = sConsulta & "   (SELECT COUNT(C.ART_INT) AS PERMISO_EP,C.ART_INT AS CODARTICULO" & vbCrLf
sConsulta = sConsulta & "    FROM CATALOG_LIN C WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN CAT_PER P WITH (NOLOCK) ON ISNULL(C.CAT1,'''')=ISNULL(P.CAT1,'''') AND ISNULL(C.CAT2,'''')=ISNULL(P.CAT2,'''') AND ISNULL(C.CAT3,'''')=ISNULL(P.CAT3,'''') AND ISNULL(C.CAT4,'''')=ISNULL(P.CAT4,'''') AND ISNULL(C.CAT5,'''')=ISNULL(P.CAT5,'''')'" & vbCrLf
sConsulta = sConsulta & "   IF @PER IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "       SET @SQL =@SQL + 'WHERE P.PER=@PER '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL =@SQL + 'GROUP BY C.ART_INT" & vbCrLf
sConsulta = sConsulta & "    ) AS PERM_EP ON PERM_EP.CODARTICULO=A.COD '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL =@SQL + 'LEFT JOIN " & vbCrLf
sConsulta = sConsulta & "   (SELECT COUNT(C.ART_INT) AS ESTA_CATALOGADO,C.ART_INT AS CODARTICULOCATALOGADO" & vbCrLf
sConsulta = sConsulta & "    FROM CATALOG_LIN C WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   GROUP BY C.ART_INT" & vbCrLf
sConsulta = sConsulta & "    ) AS CATALOGADO ON CATALOGADO.CODARTICULOCATALOGADO=A.COD '" & vbCrLf
sConsulta = sConsulta & "--FIN A�ADIDO PARA TAREA 1739---------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- Tarea 2904 ------------------------------" & vbCrLf
sConsulta = sConsulta & "IF @CODPROVEEDOR IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "BEGIN " & vbCrLf
sConsulta = sConsulta & "   IF @FILTRARNEGOCIADOS=1 --1- art negociado" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       --Negociado de prove dado " & vbCrLf
sConsulta = sConsulta & "       --, no necesito mirar prove_gmn4 pq si esta negociado se supone q sera art manejado por prove" & vbCrLf
sConsulta = sConsulta & "       SET @SQL =@SQL +  N' INNER JOIN (SELECT AA.ART,AA.PREC,AA.CODPROVE,AA.DENPROVE,AA.UNI,ISNULL(UD.DEN,'''') AS DEN, PO.MON MON" & vbCrLf
sConsulta = sConsulta & "               , PO.CAMBIO CAMBIO_OFE, P.CAMBIO CAMBIO_PROCE, MD.DEN AS DENMON' + @SQLSELPAG" & vbCrLf
sConsulta = sConsulta & "       SET @SQL =@SQL +  N' FROM ART4_ADJ AA WITH(NOLOCK)'" & vbCrLf
sConsulta = sConsulta & "       --Art4_adj. Guarda en moneda central. Para saber el cambio en moneda de la adjudicaci�n me voy a proceso" & vbCrLf
sConsulta = sConsulta & "       SET @SQL =@SQL +  N' INNER JOIN PROCE P WITH(NOLOCK) ON P.ANYO=AA.ANYO AND P.GMN1=AA.GMN1 AND P.COD=AA.PROCE'       " & vbCrLf
sConsulta = sConsulta & "       --Art4_adj. Guarda en moneda central. Para saber la moneda de la adjudicaci�n me voy a la ultima oferta" & vbCrLf
sConsulta = sConsulta & "       SET @SQL =@SQL +  N' INNER JOIN PROCE_OFE PO WITH(NOLOCK) ON PO.ANYO=AA.ANYO AND PO.GMN1=AA.GMN1 AND PO.PROCE=AA.PROCE AND PO.PROVE=AA.CODPROVE'" & vbCrLf
sConsulta = sConsulta & "       SET @SQL =@SQL +  N'    AND PO.OFE = (SELECT MAX(OFE) FROM PROCE_OFE  WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                   INNER JOIN ITEM_OFE IO WITH(NOLOCK) ON PROCE_OFE.ANYO=IO.ANYO AND PROCE_OFE.GMN1=IO.GMN1 AND PROCE_OFE.PROCE=IO.PROCE AND PROCE_OFE.PROVE=IO.PROVE AND PROCE_OFE.OFE=IO.NUM" & vbCrLf
sConsulta = sConsulta & "                                   INNER JOIN ITEM I WITH(NOLOCK) ON I.ANYO=IO.ANYO AND I.GMN1_PROCE=IO.GMN1 AND I.PROCE=IO.PROCE AND I.ART=AA.ART AND I.ID=IO.ITEM" & vbCrLf
sConsulta = sConsulta & "                                   WHERE PROCE_OFE.ANYO=AA.ANYO AND PROCE_OFE.GMN1=AA.GMN1 AND PROCE_OFE.PROCE=AA.PROCE AND PROCE_OFE.PROVE=AA.CODPROVE)'  " & vbCrLf
sConsulta = sConsulta & "       SET @SQL =@SQL +  N' LEFT JOIN UNI_DEN UD WITH(NOLOCK) ON UD.UNI=AA.UNI AND UD.IDIOMA=@IDI'" & vbCrLf
sConsulta = sConsulta & "       SET @SQL =@SQL +  N' INNER JOIN MON_DEN MD WITH(NOLOCK) ON PO.MON = MD.MON AND MD.IDIOMA =@IDI'" & vbCrLf
sConsulta = sConsulta & "       --Proveedores de la org. compras" & vbCrLf
sConsulta = sConsulta & "       IF @CODORGCOMPRAS IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           SET @SQL =@SQL +  N'  INNER JOIN PROVE WITH(NOLOCK) ON PROVE.COD=AA.CODPROVE '" & vbCrLf
sConsulta = sConsulta & "           SET @SQL =@SQL +  N'  INNER JOIN PROVE_ERP WITH(NOLOCK) ON PROVE.NIF = PROVE_ERP.NIF AND PROVE_ERP.BAJALOG=0'" & vbCrLf
sConsulta = sConsulta & "           IF @DESDEERP=1" & vbCrLf
sConsulta = sConsulta & "               SET @SQL =@SQL + ' AND PROVE_ERP.EMPRESA=ISNULL(@EMPRESA,PROVE_ERP.EMPRESA)'    " & vbCrLf
sConsulta = sConsulta & "           SET @SQL =@SQL +  N'  INNER JOIN EMP WITH(NOLOCK) ON PROVE_ERP.EMPRESA = EMP.ID '" & vbCrLf
sConsulta = sConsulta & "           SET @SQL =@SQL +  N'  INNER JOIN SOCIEDAD_ORGCOMPRAS WITH(NOLOCK) ON EMP.SOCIEDAD=SOCIEDAD_ORGCOMPRAS.SOCIEDAD '" & vbCrLf
sConsulta = sConsulta & "           SET @SQL =@SQL +  N'    AND SOCIEDAD_ORGCOMPRAS.ORGCOMPRAS=@CODORGCOMPRAS'          " & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "       ELSE" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           SET @SQL = @SQL + @SQLJOINPAG_P" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = @SQL + @SQLJOINPAG_D" & vbCrLf
sConsulta = sConsulta & "       SET @SQL =@SQL +  N' WHERE AA.ID = (SELECT MAX(ID) FROM ART4_ADJ  WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                               WHERE ART4_ADJ.ART=AA.ART" & vbCrLf
sConsulta = sConsulta & "                               AND ART4_ADJ.FECINI<=GETDATE() " & vbCrLf
sConsulta = sConsulta & "                               AND ART4_ADJ.FECFIN>=GETDATE() " & vbCrLf
sConsulta = sConsulta & "                               AND ART4_ADJ.CODPROVE=@CODPROVEEDOR)'                                           " & vbCrLf
sConsulta = sConsulta & "       SET @SQL =@SQL +  N' ) NEGO ON NEGO.ART=A.COD'      " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "   IF @FILTRARNEGOCIADOS=2 --2- art no negociado" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       --No negociado por nadie" & vbCrLf
sConsulta = sConsulta & "       SET @SQL =@SQL +  N' LEFT JOIN (SELECT ID, ART' " & vbCrLf
sConsulta = sConsulta & "       SET @SQL =@SQL +  N' FROM ART4_ADJ AA WITH(NOLOCK)'" & vbCrLf
sConsulta = sConsulta & "       SET @SQL =@SQL +  N' WHERE AA.FECINI<=GETDATE() AND AA.FECFIN>=GETDATE()'   " & vbCrLf
sConsulta = sConsulta & "       SET @SQL =@SQL +  N' ) NEGO ON NEGO.ART=A.COD'                  " & vbCrLf
sConsulta = sConsulta & "       --es del proveedor dado" & vbCrLf
sConsulta = sConsulta & "       SET @SQL =@SQL +  N' INNER JOIN PROVE_GMN4 PG WITH(NOLOCK) ON PG.PROVE=@CODPROVEEDOR'" & vbCrLf
sConsulta = sConsulta & "       SET @SQL =@SQL +  N' AND A.GMN1=PG.GMN1 AND A.GMN2=PG.GMN2 AND A.GMN3=PG.GMN3 AND A.GMN4=PG.GMN4'       " & vbCrLf
sConsulta = sConsulta & "   END " & vbCrLf
sConsulta = sConsulta & "   IF @FILTRARNEGOCIADOS=0 --0- Todos" & vbCrLf
sConsulta = sConsulta & "   BEGIN " & vbCrLf
sConsulta = sConsulta & "       --es del proveedor dado" & vbCrLf
sConsulta = sConsulta & "       SET @SQL =@SQL +  N' INNER JOIN PROVE_GMN4 PG WITH(NOLOCK) ON PG.PROVE=@CODPROVEEDOR'" & vbCrLf
sConsulta = sConsulta & "       SET @SQL =@SQL +  N' AND A.GMN1=PG.GMN1 AND A.GMN2=PG.GMN2 AND A.GMN3=PG.GMN3 AND A.GMN4=PG.GMN4'" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "END    " & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN  " & vbCrLf
sConsulta = sConsulta & "   IF @FILTRARNEGOCIADOS=1 --1- art negociado" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       --Ultimo negociado del art" & vbCrLf
sConsulta = sConsulta & "       SET @SQL =@SQL +  N' INNER JOIN (SELECT AA.ART,AA.ID,AA.PREC,AA.CODPROVE,AA.DENPROVE,AA.UNI,ISNULL(UD.DEN,'''') AS DEN, PO.MON MON" & vbCrLf
sConsulta = sConsulta & "           , PO.CAMBIO CAMBIO_OFE, P.CAMBIO CAMBIO_PROCE, MD.DEN AS DENMON' + @SQLSELPAG" & vbCrLf
sConsulta = sConsulta & "       SET @SQL =@SQL +  N' FROM ART4_ADJ AA WITH(NOLOCK)'" & vbCrLf
sConsulta = sConsulta & "       --Art4_adj. Guarda en moneda central. Para saber el cambio en moneda de la adjudicaci�n me voy a proceso" & vbCrLf
sConsulta = sConsulta & "       SET @SQL =@SQL +  N' INNER JOIN PROCE P WITH(NOLOCK) ON P.ANYO=AA.ANYO AND P.GMN1=AA.GMN1 AND P.COD=AA.PROCE'           " & vbCrLf
sConsulta = sConsulta & "       --Art4_adj. Guarda en moneda central. Para saber la moneda de la adjudicaci�n me voy a la ultima oferta" & vbCrLf
sConsulta = sConsulta & "       SET @SQL =@SQL +  N' INNER JOIN PROCE_OFE PO WITH(NOLOCK) ON PO.ANYO=AA.ANYO AND PO.GMN1=AA.GMN1 AND PO.PROCE=AA.PROCE AND PO.PROVE=AA.CODPROVE'" & vbCrLf
sConsulta = sConsulta & "       SET @SQL =@SQL +  N'    AND PO.OFE = (SELECT MAX(OFE) FROM PROCE_OFE  WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                   INNER JOIN ITEM_OFE IO WITH(NOLOCK) ON PROCE_OFE.ANYO=IO.ANYO AND PROCE_OFE.GMN1=IO.GMN1 AND PROCE_OFE.PROCE=IO.PROCE AND PROCE_OFE.PROVE=IO.PROVE AND PROCE_OFE.OFE=IO.NUM" & vbCrLf
sConsulta = sConsulta & "                                   INNER JOIN ITEM I WITH(NOLOCK) ON I.ANYO=IO.ANYO AND I.GMN1_PROCE=IO.GMN1 AND I.PROCE=IO.PROCE AND I.ART=AA.ART AND I.ID=IO.ITEM        " & vbCrLf
sConsulta = sConsulta & "                                   WHERE PROCE_OFE.ANYO=AA.ANYO AND PROCE_OFE.GMN1=AA.GMN1 AND PROCE_OFE.PROCE=AA.PROCE AND PROCE_OFE.PROVE=AA.CODPROVE)'          " & vbCrLf
sConsulta = sConsulta & "       SET @SQL =@SQL +  N' LEFT JOIN UNI_DEN UD WITH(NOLOCK) ON UD.UNI=AA.UNI AND UD.IDIOMA=@IDI'" & vbCrLf
sConsulta = sConsulta & "       SET @SQL =@SQL +  N' INNER JOIN MON_DEN MD WITH(NOLOCK) ON PO.MON = MD.MON AND MD.IDIOMA =@IDI'" & vbCrLf
sConsulta = sConsulta & "       --Proveedores de la org. compras" & vbCrLf
sConsulta = sConsulta & "       IF @CODORGCOMPRAS IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           SET @SQL =@SQL +  N'  INNER JOIN PROVE WITH(NOLOCK) ON PROVE.COD=AA.CODPROVE '" & vbCrLf
sConsulta = sConsulta & "           SET @SQL =@SQL +  N'  INNER JOIN PROVE_ERP WITH(NOLOCK) ON PROVE.NIF = PROVE_ERP.NIF AND PROVE_ERP.BAJALOG=0'" & vbCrLf
sConsulta = sConsulta & "           IF @DESDEERP=1" & vbCrLf
sConsulta = sConsulta & "               SET @SQL =@SQL + ' AND PROVE_ERP.EMPRESA=ISNULL(@EMPRESA,PROVE_ERP.EMPRESA)'                " & vbCrLf
sConsulta = sConsulta & "           SET @SQL =@SQL +  N'  INNER JOIN EMP WITH(NOLOCK) ON PROVE_ERP.EMPRESA = EMP.ID '" & vbCrLf
sConsulta = sConsulta & "           SET @SQL =@SQL +  N'  INNER JOIN SOCIEDAD_ORGCOMPRAS WITH(NOLOCK) ON EMP.SOCIEDAD=SOCIEDAD_ORGCOMPRAS.SOCIEDAD '" & vbCrLf
sConsulta = sConsulta & "           SET @SQL =@SQL +  N'    AND SOCIEDAD_ORGCOMPRAS.ORGCOMPRAS=@CODORGCOMPRAS'" & vbCrLf
sConsulta = sConsulta & "       END     " & vbCrLf
sConsulta = sConsulta & "       ELSE" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           SET @SQL = @SQL + @SQLJOINPAG_P" & vbCrLf
sConsulta = sConsulta & "       END     " & vbCrLf
sConsulta = sConsulta & "       SET @SQL = @SQL + @SQLJOINPAG_D" & vbCrLf
sConsulta = sConsulta & "       SET @SQL =@SQL +  N' WHERE AA.FECINI<=GETDATE() AND AA.FECFIN>=GETDATE()'" & vbCrLf
sConsulta = sConsulta & "       SET @SQL =@SQL +  N' ) NEGO ON NEGO.ART=A.COD' " & vbCrLf
sConsulta = sConsulta & "       SET @SQL =@SQL +  N' AND NEGO.ID = (SELECT MAX(ID) FROM ART4_ADJ WITH(NOLOCK)'              " & vbCrLf
sConsulta = sConsulta & "       SET @SQL =@SQL +  N'  WHERE ART4_ADJ.ART = A.COD AND ART4_ADJ.FECINI<=GETDATE() AND ART4_ADJ.FECFIN>=GETDATE())'" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "   IF @FILTRARNEGOCIADOS=2 --2- art no negociado" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       --No negociado por nadie" & vbCrLf
sConsulta = sConsulta & "       SET @SQL =@SQL +  N' LEFT JOIN (SELECT ID, ART' " & vbCrLf
sConsulta = sConsulta & "       SET @SQL =@SQL +  N' FROM ART4_ADJ AA WITH(NOLOCK)'" & vbCrLf
sConsulta = sConsulta & "       SET @SQL =@SQL +  N' WHERE AA.FECINI<=GETDATE() AND AA.FECFIN>=GETDATE()'" & vbCrLf
sConsulta = sConsulta & "       SET @SQL =@SQL +  N' ) NEGO ON NEGO.ART=A.COD'          " & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "END    " & vbCrLf
sConsulta = sConsulta & "-- FIN Tarea 2904 --------------------------" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL +  N' WHERE 1 = 1'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @COD IS NOT NULL AND @COD<>''" & vbCrLf
sConsulta = sConsulta & "   IF @COINCID=1" & vbCrLf
sConsulta = sConsulta & "         SET @SQL =@SQL +  N' AND A.COD = @COD'" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "         SET @SQL =@SQL +  N' AND A.COD LIKE @COD + ''%'''" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @DEN IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   IF @COINCID=1" & vbCrLf
sConsulta = sConsulta & "         SET @SQL =@SQL +  N' AND A.DEN = @DEN'" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "         SET @SQL =@SQL +  N' AND A.DEN LIKE @DEN + ''%'''" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @GMN1 IS NOT NULL AND @GMN1<>''" & vbCrLf
sConsulta = sConsulta & "   SET @SQL =@SQL +  N' AND A.GMN1 = @GMN1'" & vbCrLf
sConsulta = sConsulta & "IF @GMN2 IS NOT NULL AND @GMN2<>''" & vbCrLf
sConsulta = sConsulta & "   SET @SQL =@SQL +  N' AND A.GMN2 = @GMN2'" & vbCrLf
sConsulta = sConsulta & "IF @GMN3 IS NOT NULL AND @GMN3<>''" & vbCrLf
sConsulta = sConsulta & "   SET @SQL =@SQL +  N' AND A.GMN3 = @GMN3'" & vbCrLf
sConsulta = sConsulta & "IF @GMN4 IS NOT NULL AND @GMN4<>''" & vbCrLf
sConsulta = sConsulta & "   SET @SQL =@SQL +  N' AND A.GMN4 = @GMN4'" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "IF (@CODORGCOMPRAS IS NOT NULL)" & vbCrLf
sConsulta = sConsulta & "         SET @SQL = @SQL + N' and isNull(u3.orgcompras,isNull(u2.orgcompras,u1.orgcompras)) =@CODORGCOMPRAS'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF (@CODCENTRO IS NOT NULL)" & vbCrLf
sConsulta = sConsulta & "         SET @SQL = @SQL + N' and isNull(u3.centros,isNull(u2.centros,u1.centros)) = @CODCENTRO'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CODINI IS NOT NULL AND (@COD IS NULL OR (@COD IS NOT NULL AND @COINCID<>1))" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @SQL3=@SQL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL =@SQL +  N'  AND A.COD" & vbCrLf
sConsulta = sConsulta & "   NOT IN" & vbCrLf
sConsulta = sConsulta & "       (SELECT TOP ' + CAST(ISNULL(@DESDEFILA,1)-1 AS NVARCHAR(10)) + N'   A.COD ' + @SQL3 + ' ORDER BY A.COD)'" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   IF @CODULT IS NOT NULL AND (@COD IS NULL OR (@COD IS NOT NULL AND @COINCID<>1))" & vbCrLf
sConsulta = sConsulta & "         SET @SQL =@SQL +  N'  AND A.COD > @CODULT'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "----------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "-- OTRAS CARACTERISTICAS" & vbCrLf
sConsulta = sConsulta & "----------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Gen�ricos" & vbCrLf
sConsulta = sConsulta & "IF @GENERICOS=0" & vbCrLf
sConsulta = sConsulta & "   SET @SQL =@SQL +  N' AND A.GENERICO=1'" & vbCrLf
sConsulta = sConsulta & "IF @GENERICOS=1" & vbCrLf
sConsulta = sConsulta & "   SET @SQL =@SQL +  N' AND A.GENERICO=0'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Concepto" & vbCrLf
sConsulta = sConsulta & "IF @GASTO=1 OR @INVERSION=1 OR @GASTOINVERSION=1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @SQL =@SQL +  N' AND A.CONCEPTO IN ('" & vbCrLf
sConsulta = sConsulta & "   IF @GASTO=1" & vbCrLf
sConsulta = sConsulta & "       SET @SQL =@SQL + '0,'" & vbCrLf
sConsulta = sConsulta & "   IF @INVERSION=1" & vbCrLf
sConsulta = sConsulta & "       SET @SQL =@SQL + '1,'" & vbCrLf
sConsulta = sConsulta & "   IF @GASTOINVERSION=1" & vbCrLf
sConsulta = sConsulta & "       SET @SQL =@SQL + '2,'" & vbCrLf
sConsulta = sConsulta & "   SET @SQL= LEFT(@SQL,LEN(@SQL)-1) + ')'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Almacenar" & vbCrLf
sConsulta = sConsulta & "IF @NOALMACENABLE=1 OR @ALMACENABLE=1 OR @ALMACENOPCIONAL=1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @SQL =@SQL +  N' AND A.ALMACENAR IN ('" & vbCrLf
sConsulta = sConsulta & "   IF @NOALMACENABLE=1" & vbCrLf
sConsulta = sConsulta & "       SET @SQL =@SQL + '0,'" & vbCrLf
sConsulta = sConsulta & "   IF @ALMACENABLE=1" & vbCrLf
sConsulta = sConsulta & "       SET @SQL =@SQL + '1,'" & vbCrLf
sConsulta = sConsulta & "   IF @ALMACENOPCIONAL=1" & vbCrLf
sConsulta = sConsulta & "       SET @SQL =@SQL + '2,'" & vbCrLf
sConsulta = sConsulta & "   SET @SQL= LEFT(@SQL,LEN(@SQL)-1) + ')'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Recepcionar" & vbCrLf
sConsulta = sConsulta & "IF @RECEPCIONOBLIG=1 OR @NORECEPCIONABLE=1 OR @RECEPCIONOPCIONAL=1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @SQL =@SQL +  N' AND A.RECEPCIONAR IN ('" & vbCrLf
sConsulta = sConsulta & "   IF @NORECEPCIONABLE=1" & vbCrLf
sConsulta = sConsulta & "       SET @SQL =@SQL + '0,'" & vbCrLf
sConsulta = sConsulta & "   IF @RECEPCIONOBLIG=1" & vbCrLf
sConsulta = sConsulta & "       SET @SQL =@SQL + '1,'" & vbCrLf
sConsulta = sConsulta & "   IF @RECEPCIONOPCIONAL=1" & vbCrLf
sConsulta = sConsulta & "       SET @SQL =@SQL + '2,'" & vbCrLf
sConsulta = sConsulta & "   SET @SQL= LEFT(@SQL,LEN(@SQL)-1) + ')'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Catalogado" & vbCrLf
sConsulta = sConsulta & "--INICIO A�ADIDO PARA TAREA 1739---------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "IF @MATERIALESCATALOGADOS=0" & vbCrLf
sConsulta = sConsulta & "SET @SQL =@SQL + ' AND CATALOGADO.ESTA_CATALOGADO IS NULL'" & vbCrLf
sConsulta = sConsulta & "--FIN A�ADIDO PARA TAREA 1739---------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- Tarea 2904 ------------------------------" & vbCrLf
sConsulta = sConsulta & "IF @FILTRARNEGOCIADOS=2 --No negociado por nadie" & vbCrLf
sConsulta = sConsulta & "   SET @SQL =@SQL + ' AND (NEGO.ID IS NULL OR A.GENERICO=1)'" & vbCrLf
sConsulta = sConsulta & "-- FIN Tarea 2904 ------------------------------" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL2 + @SQL + N' ORDER BY A.COD'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @PARAM = N' @COD VARCHAR(50) =null, @DEN VARCHAR(2000) =null, @GMN1 VARCHAR(50) =null, @GMN2 VARCHAR(50) =null, @GMN3 VARCHAR(50) =null, @GMN4 VARCHAR(50) =null, @COINCID TINYINT = 1, @PER varchar(50) =null, @GENERICOS TINYINT = 0, @CODORGCOMPRAS VARCHAR(4),@CODCENTRO VARCHAR(4)=NULL,@CODINI VARCHAR(50),@CODULT VARCHAR(50),@EQP VARCHAR(50),@EQUIV FLOAT,@EQUIVINS FLOAT,@GASTO TINYINT, @INVERSION TINYINT, @GASTOINVERSION TINYINT, @NOALMACENABLE TINYINT, @ALMACENABLE TINYINT, @ALMACENOPCIONAL TINYINT, @RECEPCIONOBLIG TINYINT, @NORECEPCIONABLE TINYINT, @RECEPCIONOPCIONAL TINYINT, @IDI VARCHAR(50), @CODPROVEEDOR VARCHAR(50)=NULL, @EMPRESA INT=NULL'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, @PARAM, @COD = @COD, @DEN=@DEN, @GMN1=@GMN1, @GMN2=@GMN2, @GMN3=@GMN3, @GMN4=@GMN4, @COINCID=@COINCID, @PER=@PER, @GENERICOS=@GENERICOS, @CODORGCOMPRAS=@CODORGCOMPRAS,@CODCENTRO=@CODCENTRO,@CODINI=@CODINI,@CODULT=@CODULT,@EQP=@EQP,@EQUIV=@EQUIV,@EQUIVINS=@EQUIVINS,@GASTO=@GASTO, @INVERSION=@INVERSION, @GASTOINVERSION=@GASTOINVERSION, @NOALMACENABLE=@NOALMACENABLE, @ALMACENABLE=@ALMACENABLE, @ALMACENOPCIONAL=@ALMACENOPCIONAL, @RECEPCIONOBLIG=@RECEPCIONOBLIG, @NORECEPCIONABLE=@NORECEPCIONABLE, @RECEPCIONOPCIONAL=@RECEPCIONOPCIONAL,@IDI=@IDI, @CODPROVEEDOR=@CODPROVEEDOR, @EMPRESA=@EMPRESA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta



End Sub


Public Function CodigoDeActualizacion31900_08_1893_A31900_08_1894() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim oADOConnection As ADODB.Connection
    Dim sConsulta As String

    On Error GoTo Error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Actualiza Tablas"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 3
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    frmProgreso.ProgressBar1.Refresh
    V_31900_8_Tablas_094
    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Actualiza Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 3
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    frmProgreso.ProgressBar1.Refresh
    V_31900_8_Storeds_094
    
    
    sConsulta = "UPDATE VERSION SET NUM ='3.00.18.94'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103), PRODVER='31900.8'"
    ExecuteSQL gRDOCon, sConsulta
        
    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False
        
    CodigoDeActualizacion31900_08_1893_A31900_08_1894 = True
    Exit Function

Error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    CodigoDeActualizacion31900_08_1893_A31900_08_1894 = False
End Function

Public Sub V_31900_8_Storeds_094()
    Dim sConsulta As String

sConsulta = "IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_ESTADISTICAS_ERRORES]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_ESTADISTICAS_ERRORES]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSAL_ESTADISTICAS_ERRORES]  " & vbCrLf
sConsulta = sConsulta & "   @Desde DATETIME," & vbCrLf
sConsulta = sConsulta & "   @Hasta DATETIME," & vbCrLf
sConsulta = sConsulta & "   @entorno nvarchar(50)," & vbCrLf
sConsulta = sConsulta & "   @pyme nvarchar(50)=NULL " & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM FSAL_ERRORES " & vbCrLf
sConsulta = sConsulta & "WHERE FECHA_ERROR>=dateadd(""hh"",datediff(""hh"",GETDATE(),GETUTCDATE()),@Desde) AND FECHA_ERROR<=dateadd(""hh"",datediff(""hh"",GETDATE(),GETUTCDATE()),@Hasta)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO FSAL_ERRORES(FSAL_ENTORNO,FSAL_PYME,FECHA_ERROR,ID_ERROR,EX_FULLNAME,DATA,FECACT)" & vbCrLf
sConsulta = sConsulta & "SELECT @entorno, @pyme, dateadd(""hh"",datediff(""hh"",GETDATE(),GETUTCDATE()),FEC_ALTA), 2, EX_FULLNAME, EX_MESSAGE, GETDATE() " & vbCrLf
sConsulta = sConsulta & "FROM ERRORES WITH(NOLOCK) WHERE FEC_ALTA>=@Desde AND FEC_ALTA<=@Hasta " & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_ACTIVITY_USUARIOS_POR_HORA]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_ACTIVITY_USUARIOS_POR_HORA]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSAL_ACTIVITY_USUARIOS_POR_HORA]  " & vbCrLf
sConsulta = sConsulta & "   @fecdesde DATETIME," & vbCrLf
sConsulta = sConsulta & "   @fechasta DATETIME," & vbCrLf
sConsulta = sConsulta & "   @entorno nvarchar(50)," & vbCrLf
sConsulta = sConsulta & "   @tipoEvento INT," & vbCrLf
sConsulta = sConsulta & "   @pyme nvarchar(100)=null" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @fechacomienzo DATETIME" & vbCrLf
sConsulta = sConsulta & "DECLARE @MEDIA FLOAT" & vbCrLf
sConsulta = sConsulta & "declare @numDiaSemana float" & vbCrLf
sConsulta = sConsulta & "SET @fechacomienzo=CONVERT(datetime,'2010-01-01')" & vbCrLf
sConsulta = sConsulta & "set @numDiaSemana = DATEDIFF(""WW"",@fechacomienzo,GETDATE())" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "select @MEDIA=(" & vbCrLf
sConsulta = sConsulta & "SELECT round(sum(convert(float, NUM_EVENTOS))/@numDiaSemana,2,0) as SUM_EVENTOS" & vbCrLf
sConsulta = sConsulta & "FROM FSAL_EVENTOS_HIST WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE FSAL_ENTORNO=@entorno " & vbCrLf
sConsulta = sConsulta & "AND isnull(FSAL_PYME,'') = isnull(@pyme,'') " & vbCrLf
sConsulta = sConsulta & "AND TIPO_EVENTO=@tipoEvento" & vbCrLf
sConsulta = sConsulta & "AND DATEPART(""DW"",FECHA_EVENTO)=DATEPART(""DW"",@FECHASTA));" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "with mycte as" & vbCrLf
sConsulta = sConsulta & "(" & vbCrLf
sConsulta = sConsulta & "    SELECT @fecdesde  AS MyDay" & vbCrLf
sConsulta = sConsulta & "    UNION ALL" & vbCrLf
sConsulta = sConsulta & "    SELECT dateadd(HOUR ,1,MyDay)" & vbCrLf
sConsulta = sConsulta & "    FROM mycte " & vbCrLf
sConsulta = sConsulta & "    WHERE dateadd(HOUR ,1,MyDay) < @fechasta" & vbCrLf
sConsulta = sConsulta & ")" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "select ACCESOS.HORA_ACCESO, isnull(EVENTOS.NUM_EVENTOS,0) as NUM_EVENTOS, ISNULL(@Media,0) AS AVG_EVENTOS, EVENTOS.TIPO_EVENTO FROM" & vbCrLf
sConsulta = sConsulta & "(" & vbCrLf
sConsulta = sConsulta & "SELECT RIGHT(RIGHT(0,1)+LTRIM(DATEPART(""HH"",MyDay)),2)+':00' as HORA_ACCESO" & vbCrLf
sConsulta = sConsulta & "FROM mycte" & vbCrLf
sConsulta = sConsulta & ") as ACCESOS" & vbCrLf
sConsulta = sConsulta & "left join" & vbCrLf
sConsulta = sConsulta & "(" & vbCrLf
sConsulta = sConsulta & "SELECT right(right(0,1)+ltrim(DATEPART(""hh"",FECHA_EVENTO)),2)+':00' as HORA_ACCESO, SUM(NUM_EVENTOS) AS NUM_EVENTOS, TIPO_EVENTO" & vbCrLf
sConsulta = sConsulta & "FROM FSAL_EVENTOS_HIST WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "@fecdesde<FECHA_EVENTO" & vbCrLf
sConsulta = sConsulta & "AND @fechasta > FECHA_EVENTO" & vbCrLf
sConsulta = sConsulta & "--DATEPART(""yyyy"",@fecdesde) = DATEPART(""yyyy"", FECHA_EVENTO) " & vbCrLf
sConsulta = sConsulta & "--AND DATEPART(""mm"", @fecdesde)= DATEPART(""mm"", FECHA_EVENTO) " & vbCrLf
sConsulta = sConsulta & "--AND DATEPART(""dd"",@fecdesde)=DATEPART(""dd"",FECHA_EVENTO) " & vbCrLf
sConsulta = sConsulta & "AND FSAL_ENTORNO=@entorno " & vbCrLf
sConsulta = sConsulta & "AND isnull(FSAL_PYME,'') = isnull(@pyme,'') " & vbCrLf
sConsulta = sConsulta & "AND TIPO_EVENTO=@tipoEvento" & vbCrLf
sConsulta = sConsulta & "GROUP BY right(right(0,1)+ltrim(DATEPART(""hh"",FECHA_EVENTO)),2)+':00', TIPO_EVENTO" & vbCrLf
sConsulta = sConsulta & ") AS EVENTOS" & vbCrLf
sConsulta = sConsulta & "ON ACCESOS.HORA_ACCESO=EVENTOS.HORA_ACCESO" & vbCrLf
sConsulta = sConsulta & "ORDER BY HORA_ACCESO" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_CARGAR_EVENTOS_POR_DIA]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_CARGAR_EVENTOS_POR_DIA]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSAL_CARGAR_EVENTOS_POR_DIA]  " & vbCrLf
sConsulta = sConsulta & "   @fecdesde DATETIME," & vbCrLf
sConsulta = sConsulta & "   @entorno nvarchar(50)," & vbCrLf
sConsulta = sConsulta & "   @tipoevento INT=null," & vbCrLf
sConsulta = sConsulta & "   @fechasta DATETIME," & vbCrLf
sConsulta = sConsulta & "   @pyme nvarchar(50)=null," & vbCrLf
sConsulta = sConsulta & "   @idioma nvarchar(5)" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @fechacomienzo DATETIME" & vbCrLf
sConsulta = sConsulta & "DECLARE @MEDIA FLOAT" & vbCrLf
sConsulta = sConsulta & "declare @numDiaSemana float" & vbCrLf
sConsulta = sConsulta & "SET @fechacomienzo=CONVERT(datetime,'2010-01-01')" & vbCrLf
sConsulta = sConsulta & "set @numDiaSemana = DATEDIFF(""WW"",@fechacomienzo,GETDATE())" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "select DISTINCT(RANKING.FECHA_EVENTO)," & vbCrLf
sConsulta = sConsulta & " DIA, " & vbCrLf
sConsulta = sConsulta & " DEN," & vbCrLf
sConsulta = sConsulta & " RANKING.rank," & vbCrLf
sConsulta = sConsulta & " media.AVG_EVENTOS," & vbCrLf
sConsulta = sConsulta & " RANKING.data   " & vbCrLf
sConsulta = sConsulta & "from " & vbCrLf
sConsulta = sConsulta & " (" & vbCrLf
sConsulta = sConsulta & " select fecha_evento," & vbCrLf
sConsulta = sConsulta & "   tipo_evento," & vbCrLf
sConsulta = sConsulta & "   rank() OVER (Partition by tipo_evento ORDER BY  fecha_evento asc ,id asc ) as rank," & vbCrLf
sConsulta = sConsulta & "  data" & vbCrLf
sConsulta = sConsulta & " from fsal_eventos WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & " where fecha_evento between @fecdesde and @fechasta" & vbCrLf
sConsulta = sConsulta & " ) RANKING" & vbCrLf
sConsulta = sConsulta & "inner join " & vbCrLf
sConsulta = sConsulta & " ( " & vbCrLf
sConsulta = sConsulta & "  select " & vbCrLf
sConsulta = sConsulta & "   tipo_evento," & vbCrLf
sConsulta = sConsulta & "   DEN," & vbCrLf
sConsulta = sConsulta & "   DIA," & vbCrLf
sConsulta = sConsulta & "   MES," & vbCrLf
sConsulta = sConsulta & "   ANYO," & vbCrLf
sConsulta = sConsulta & "   sum(num_eventos) as num_eventos " & vbCrLf
sConsulta = sConsulta & "  from FSAL_EVENTOS_HIST WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "  left join FSAL_TIPO_EVENTO_DEN on FSAL_EVENTOS_HIST.TIPO_EVENTO=FSAL_TIPO_EVENTO_DEN.ID" & vbCrLf
sConsulta = sConsulta & "  where " & vbCrLf
sConsulta = sConsulta & "  @fecdesde<FECHA_EVENTO" & vbCrLf
sConsulta = sConsulta & "  AND @fechasta> FECHA_EVENTO " & vbCrLf
sConsulta = sConsulta & "  --mes = datepart(""mm"",@fecdesde)" & vbCrLf
sConsulta = sConsulta & "  --AND DIA=DATEPART(""DD"",@FECDESDE) " & vbCrLf
sConsulta = sConsulta & "  --AND ANYO>=DATEPART(""YY"",@FECDESDE) " & vbCrLf
sConsulta = sConsulta & "  and ISNULL(@tipoevento,'') = (case when @tipoevento is null then isnull(@tipoEvento,'')" & vbCrLf
sConsulta = sConsulta & "                   ELSE TIPO_EVENTO end)" & vbCrLf
sConsulta = sConsulta & "  AND FSAL_ENTORNO=@entorno " & vbCrLf
sConsulta = sConsulta & "  and ISNULL(FSAL_PYME,'')=ISNULL(@pyme,'') " & vbCrLf
sConsulta = sConsulta & "  and FSAL_TIPO_EVENTO_DEN.IDI=@idioma " & vbCrLf
sConsulta = sConsulta & "  group by tipo_evento,DIA,MES,ANYO,DEN" & vbCrLf
sConsulta = sConsulta & "  ) EVENTOS_DIA" & vbCrLf
sConsulta = sConsulta & "  LEFT JOIN" & vbCrLf
sConsulta = sConsulta & "  (" & vbCrLf
sConsulta = sConsulta & "       SELECT tipo_evento, round(sum(convert(float, NUM_EVENTOS))/@numDiaSemana,2,0) as AVG_EVENTOS" & vbCrLf
sConsulta = sConsulta & "       FROM FSAL_EVENTOS_HIST WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       WHERE FSAL_ENTORNO=@entorno " & vbCrLf
sConsulta = sConsulta & "       AND isnull(FSAL_PYME,'') = isnull(@pyme,'') " & vbCrLf
sConsulta = sConsulta & "       AND ISNULL(@tipoevento,'') = (case when @tipoevento is null then isnull(@tipoEvento,'')" & vbCrLf
sConsulta = sConsulta & "                   ELSE TIPO_EVENTO end)" & vbCrLf
sConsulta = sConsulta & "       AND DATEPART(""DW"",FECHA_EVENTO)=DATEPART(""DW"",@FECHASTA)" & vbCrLf
sConsulta = sConsulta & "       group by tipo_evento" & vbCrLf
sConsulta = sConsulta & "  )AS MEDIA" & vbCrLf
sConsulta = sConsulta & "  ON MEDIA.TIPO_EVENTO=EVENTOS_DIA.TIPO_EVENTO" & vbCrLf
sConsulta = sConsulta & "  on ranking.TIPO_EVENTO = media.TIPO_EVENTO " & vbCrLf
sConsulta = sConsulta & "order by ranking.fecha_evento desc" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_TRANSFERIR_EVENTOS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_TRANSFERIR_EVENTOS]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSAL_TRANSFERIR_EVENTOS]  " & vbCrLf
sConsulta = sConsulta & "   @DESDE datetime," & vbCrLf
sConsulta = sConsulta & "   @HASTA datetime," & vbCrLf
sConsulta = sConsulta & "   @ENTORNO nvarchar(50)," & vbCrLf
sConsulta = sConsulta & "   @PYME nvarchar(50)=NULL" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- Transferencia de las operaciones de la tabla log" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "insert into fsal_eventos(FSAL_ENTORNO,FSAL_PYME,FECHA_EVENTO,TiPO_EVENTO,DATA,TRASPASO)" & vbCrLf
sConsulta = sConsulta & "   select @ENTORNO as ENTORNO, @PYME as Pyme,DATEADD(""hh"",datediff(""hh"",GETDATE(),GETUTCDATE()), fec),acc,dat,0 from log " & vbCrLf
sConsulta = sConsulta & "   where  acc in (281,401,452)" & vbCrLf
sConsulta = sConsulta & "       -- 281: Nuevo proceso" & vbCrLf
sConsulta = sConsulta & "       -- 401: Nueva oferta" & vbCrLf
sConsulta = sConsulta & "       -- 452: Nueva Adjudicaci�n      " & vbCrLf
sConsulta = sConsulta & "   and fec between @desde and @hasta" & vbCrLf
sConsulta = sConsulta & "   AND NOT EXISTS( SELECT FSAL_ENTORNO, FSAL_PYME, FECHA_EVENTO, TIPO_EVENTO, DATA FROM FSAL_EVENTOS " & vbCrLf
sConsulta = sConsulta & "                  WHERE @ENTORNO=FSAL_ENTORNO " & vbCrLf
sConsulta = sConsulta & "                  AND @PYME=FSAL_PYME" & vbCrLf
sConsulta = sConsulta & "                  AND FECHA_EVENTO=FEC" & vbCrLf
sConsulta = sConsulta & "                  AND TIPO_EVENTO = ACC" & vbCrLf
sConsulta = sConsulta & "                  AND DATA= DAT ) " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- Transferencia de Solictudes, certificados, contratos y facturas" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "insert into fsal_eventos(FSAL_ENTORNO,FSAL_PYME,FECHA_EVENTO,TiPO_EVENTO,DATA, TRASPASO)" & vbCrLf
sConsulta = sConsulta & "   select @ENTORNO as ENTORNO, @PYME as Pyme,DATEADD(""HH"",DATEDIFF(""HH"",GETDATE(), GETUTCDATE()),INSTANCIA.FEC_ALTA), TIPO_SOLICITUDES.tipo, instancia.id, 0 " & vbCrLf
sConsulta = sConsulta & "    from TIPO_SOLICITUDES" & vbCrLf
sConsulta = sConsulta & "    inner join  SOLICITUD " & vbCrLf
sConsulta = sConsulta & "    on SOLICITUD.tipo = TIPO_SOLICITUDES.id " & vbCrLf
sConsulta = sConsulta & "    inner join INSTANCIA" & vbCrLf
sConsulta = sConsulta & "    on INSTANCIA.SOLICITUD= SOLICITUD.ID" & vbCrLf
sConsulta = sConsulta & "    where TIPO_SOLICITUDES.tipo in (0,1,2,3,5,7)" & vbCrLf
sConsulta = sConsulta & "       -- 1: Solicitud de compra" & vbCrLf
sConsulta = sConsulta & "       -- 2: Certificado" & vbCrLf
sConsulta = sConsulta & "       -- 3: No conformidad" & vbCrLf
sConsulta = sConsulta & "       --  5: Contrato" & vbCrLf
sConsulta = sConsulta & "       -- 7: Factura" & vbCrLf
sConsulta = sConsulta & "   and INSTANCIA.FEC_ALTA between @desde and @hasta" & vbCrLf
sConsulta = sConsulta & "   AND NOT EXISTS( SELECT FSAL_ENTORNO, FSAL_PYME, FECHA_EVENTO, TIPO_EVENTO, TIPO_EVENTO, DATA FROM FSAL_EVENTOS" & vbCrLf
sConsulta = sConsulta & "                   WHERE @ENTORNO=FSAL_ENTORNO " & vbCrLf
sConsulta = sConsulta & "                   AND @PYME=FSAL_PYME" & vbCrLf
sConsulta = sConsulta & "                   AND FECHA_EVENTO=INSTANCIA.FEC_ALTA" & vbCrLf
sConsulta = sConsulta & "                   AND TIPO_EVENTO=TIPO_SOLICITUDES.TIPO" & vbCrLf
sConsulta = sConsulta & "                   AND DATA = INSTANCIA.ID)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_ESTADISTICAS_EVENTO_HORA]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_ESTADISTICAS_EVENTO_HORA]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSAL_ESTADISTICAS_EVENTO_HORA]  " & vbCrLf
sConsulta = sConsulta & "   @Desde DATETIME," & vbCrLf
sConsulta = sConsulta & "   @Hasta DATETIME," & vbCrLf
sConsulta = sConsulta & "   @entorno nvarchar(50)," & vbCrLf
sConsulta = sConsulta & "   @pyme nvarchar(50)=NULL" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "Declare @HoraSig as datetime" & vbCrLf
sConsulta = sConsulta & "set @Horasig = dateadd(hour,1,@Desde);" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if (@Horasig > @hasta) begin set @horasig = @hasta end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- Para permiir genera estad�sticas parciales" & vbCrLf
sConsulta = sConsulta & "-- la sentencia INSERT INTO SELECT FROM puede fallar por existir previamente los registros" & vbCrLf
sConsulta = sConsulta & "-- Tampoco puedo detectar el error y hacer un UPDATE INNER JOIN porque puede que alguno no exista" & vbCrLf
sConsulta = sConsulta & "-- Asi que Me aseguro de borrar los datos que existan previamente y hago exclusivamente la INSERT" & vbCrLf
sConsulta = sConsulta & "DELETE FROM FSAL_EVENTOS_HIST" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "   FECHA_EVENTO>=@Desde" & vbCrLf
sConsulta = sConsulta & "   AND FECHA_EVENTO<= @Hasta" & vbCrLf
sConsulta = sConsulta & "   --ANYO =  DATEPART(""yy"",@Desde)" & vbCrLf
sConsulta = sConsulta & "    --AND MES =  DATEPART(""mm"",@Desde)" & vbCrLf
sConsulta = sConsulta & "    --AND Dia =  DATEPART(""dd"",@Desde)" & vbCrLf
sConsulta = sConsulta & "    --AND Hora = DATEPART(""hh"",@Desde)" & vbCrLf
sConsulta = sConsulta & "    AND FSAL_ENTORNO = @entorno" & vbCrLf
sConsulta = sConsulta & "    AND isnull(FSAL_PYME,'') = isnull(@pyme,'');" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- COMMON TABLE EXPRESSION para obtener todas las entradas (bloques de 5 min) entre las fechas-horas indicadas" & vbCrLf
sConsulta = sConsulta & "-- OJO! Esta CTE s�lo admite 100 pasos de iteraci�n/Recursi�n, pero se supone que es v�lida ya que son se van" & vbCrLf
sConsulta = sConsulta & "-- a consultar fragmentos de 1 hora de duracci�n (12 bloques de 5 min)" & vbCrLf
sConsulta = sConsulta & "WITH cteMinutos as" & vbCrLf
sConsulta = sConsulta & "( " & vbCrLf
sConsulta = sConsulta & "    SELECT @Desde  AS MisMinutos" & vbCrLf
sConsulta = sConsulta & "    UNION ALL" & vbCrLf
sConsulta = sConsulta & "    SELECT dateadd(MINUTE,5,MisMinutos)" & vbCrLf
sConsulta = sConsulta & "    FROM cteMinutos" & vbCrLf
sConsulta = sConsulta & "    WHERE dateadd(minute,5,MisMinutos) < @HoraSig" & vbCrLf
sConsulta = sConsulta & ")" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO FSAL_EVENTOS_HIST (FECHA_EVENTO,ANYO,Mes,Dia,Hora,Min,FSAL_ENTORNO,FSAL_PYME,NUM_EVENTOS, TIPO_EVENTO,TRASPASO)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT " & vbCrLf
sConsulta = sConsulta & "   MisMinutos as FECHA" & vbCrLf
sConsulta = sConsulta & "   ,DATEPART(""yy"",MisMinutos) as ANYO" & vbCrLf
sConsulta = sConsulta & "   ,DATEPART(""mm"",MisMinutos) as MES" & vbCrLf
sConsulta = sConsulta & "   ,DATEPART(""dd"",MisMinutos) as DIA" & vbCrLf
sConsulta = sConsulta & "   ,DATEPART(""hh"",MisMinutos) as HORA  " & vbCrLf
sConsulta = sConsulta & "   ,right(replicate('0', 2) + ltrim(CAST((((60*DATEPART(""HH"",MisMinutos)+DATEPART(""Mi"",MisMinutos)) / 5) * 5) % 60 AS varchar)), 2) as MIN" & vbCrLf
sConsulta = sConsulta & "   ,@entorno as ENTORNO" & vbCrLf
sConsulta = sConsulta & "   ,@pyme as PYME" & vbCrLf
sConsulta = sConsulta & "   ,ISNULL(NUM_EVENTOS,0) as NUM_EVENTOS" & vbCrLf
sConsulta = sConsulta & "   ,TIPO_EVENTO AS TIPO_EVENTO" & vbCrLf
sConsulta = sConsulta & "   ,0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "FROM cteMinutos " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "left JOIN (  --eventos" & vbCrLf
sConsulta = sConsulta & "   Select right(replicate('0', 2) + ltrim(cast((((60*DATEPART(""HH"",FECHA_EVENTO)+DATEPART(""Mi"",FECHA_EVENTO)) / 5) * 5) / 60 as varchar)), 2)  + ':' + " & vbCrLf
sConsulta = sConsulta & "   right(replicate('0', 2) + ltrim(CAST((((60*DATEPART(""HH"",FECHA_EVENTO)+DATEPART(""Mi"",FECHA_EVENTO)) / 5) * 5) % 60 AS varchar)), 2) as HORA_ACCESO " & vbCrLf
sConsulta = sConsulta & "   ,COUNT(DISTINCT ID) as NUM_EVENTOS, TIPO_EVENTO, FSAL_ENTORNO, FSAL_PYME" & vbCrLf
sConsulta = sConsulta & "   from FSAL_EVENTOS WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   WHERE FECHA_EVENTO < @HoraSig AND FECHA_EVENTO > @Desde AND FSAL_ENTORNO = @entorno" & vbCrLf
sConsulta = sConsulta & "   GROUP BY right(replicate('0', 2) + ltrim(cast((((60*DATEPART(""HH"",FECHA_EVENTO)+DATEPART(""Mi"",FECHA_EVENTO)) / 5) * 5) / 60 as varchar)), 2)  + ':' + " & vbCrLf
sConsulta = sConsulta & "   right(replicate('0', 2) + ltrim(CAST((((60*DATEPART(""HH"",FECHA_EVENTO)+DATEPART(""Mi"",FECHA_EVENTO)) / 5) * 5) % 60 AS varchar)), 2) , TIPO_EVENTO, FSAL_ENTORNO, FSAL_PYME) as fsal_horas_eventos" & vbCrLf
sConsulta = sConsulta & "   on right(replicate('0', 2) + ltrim(cast((((60*DATEPART(""hh"",MisMinutos)+ DATEPART(""mi"",MisMinutos)) / 5) * 5) /60 as varchar)), 2)+ ':' + " & vbCrLf
sConsulta = sConsulta & "   right(replicate('0', 2) + ltrim(CAST((((60*DATEPART(""HH"",MisMinutos)+DATEPART(""Mi"",MisMinutos)) / 5) * 5) % 60 AS varchar)), 2)=fsal_horas_eventos.HORA_ACCESO " & vbCrLf
sConsulta = sConsulta & "---- Se cargan los eventos por horas basandose en el m�ximo de eventos de cada bloque de 5 minutos" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_CARGAR_EVENTOS_POR_HORA]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_CARGAR_EVENTOS_POR_HORA]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSAL_CARGAR_EVENTOS_POR_HORA]  " & vbCrLf
sConsulta = sConsulta & "   @fecdesde DATETIME," & vbCrLf
sConsulta = sConsulta & "   @entorno nvarchar(50)," & vbCrLf
sConsulta = sConsulta & "   @tipoevento INT=null," & vbCrLf
sConsulta = sConsulta & "   @fechasta DATETIME," & vbCrLf
sConsulta = sConsulta & "   @pyme nvarchar(50)=null," & vbCrLf
sConsulta = sConsulta & "   @idioma nvarchar(5)" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @fechacomienzo DATETIME" & vbCrLf
sConsulta = sConsulta & "DECLARE @MEDIA FLOAT" & vbCrLf
sConsulta = sConsulta & "declare @numDiaSemanaHoras float" & vbCrLf
sConsulta = sConsulta & "SET @fechacomienzo=CONVERT(datetime,'2010-01-01')" & vbCrLf
sConsulta = sConsulta & "set @numDiaSemanaHoras = DATEDIFF(""WW"",@fechacomienzo,GETDATE())" & vbCrLf
sConsulta = sConsulta & "SET @fecdesde=DBO.fn5MIN(@FECDESDE)" & vbCrLf
sConsulta = sConsulta & "SET @fechasta=DBO.fn5MIN(@FECHASTA)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "select DISTINCT(RANKING.FECHA_EVENTO)," & vbCrLf
sConsulta = sConsulta & " DIA, " & vbCrLf
sConsulta = sConsulta & " DEN," & vbCrLf
sConsulta = sConsulta & " RANKING.rank," & vbCrLf
sConsulta = sConsulta & " media.AVG_EVENTOS," & vbCrLf
sConsulta = sConsulta & " RANKING.data   " & vbCrLf
sConsulta = sConsulta & "from " & vbCrLf
sConsulta = sConsulta & " (" & vbCrLf
sConsulta = sConsulta & " select fecha_evento," & vbCrLf
sConsulta = sConsulta & "   tipo_evento," & vbCrLf
sConsulta = sConsulta & "   rank() OVER (Partition by tipo_evento ORDER BY  fecha_evento asc ,id asc ) as rank," & vbCrLf
sConsulta = sConsulta & "  data" & vbCrLf
sConsulta = sConsulta & " from fsal_eventos WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & " where fecha_evento >= @fecdesde and FECHA_EVENTO <= @fechasta" & vbCrLf
sConsulta = sConsulta & " ) RANKING" & vbCrLf
sConsulta = sConsulta & "inner join " & vbCrLf
sConsulta = sConsulta & " ( " & vbCrLf
sConsulta = sConsulta & "  select " & vbCrLf
sConsulta = sConsulta & "   tipo_evento," & vbCrLf
sConsulta = sConsulta & "   DEN," & vbCrLf
sConsulta = sConsulta & "   HORA," & vbCrLf
sConsulta = sConsulta & "   MIN," & vbCrLf
sConsulta = sConsulta & "   DIA," & vbCrLf
sConsulta = sConsulta & "   MES," & vbCrLf
sConsulta = sConsulta & "   ANYO," & vbCrLf
sConsulta = sConsulta & "   sum(num_eventos) as num_eventos " & vbCrLf
sConsulta = sConsulta & "  from FSAL_EVENTOS_HIST WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "  left join FSAL_TIPO_EVENTO_DEN on FSAL_EVENTOS_HIST.TIPO_EVENTO=FSAL_TIPO_EVENTO_DEN.ID" & vbCrLf
sConsulta = sConsulta & "  where mes = datepart(""mm"",@fecdesde)" & vbCrLf
sConsulta = sConsulta & "  AND DIA=DATEPART(""DD"",@FECDESDE) " & vbCrLf
sConsulta = sConsulta & "  AND ANYO=DATEPART(""YY"",@FECDESDE) " & vbCrLf
sConsulta = sConsulta & "  AND CONVERT(VARCHAR(8),FECHA_EVENTO,108)>=CONVERT(VARCHAR(8),@FECDESDE,108) " & vbCrLf
sConsulta = sConsulta & "  AND CONVERT(VARCHAR(8),FECHA_EVENTO,108)<=CONVERT(VARCHAR(8),@FECHASTA,108) " & vbCrLf
sConsulta = sConsulta & "  and ISNULL(@tipoevento,'') = (case when @tipoevento is null then isnull(@tipoEvento,'')" & vbCrLf
sConsulta = sConsulta & "                   ELSE TIPO_EVENTO end)" & vbCrLf
sConsulta = sConsulta & "  AND FSAL_ENTORNO=@entorno " & vbCrLf
sConsulta = sConsulta & "  and ISNULL(FSAL_PYME,'')=ISNULL(@pyme,'') " & vbCrLf
sConsulta = sConsulta & "  and FSAL_TIPO_EVENTO_DEN.IDI=@idioma " & vbCrLf
sConsulta = sConsulta & "  group by tipo_evento,HORA,MIN,DIA,MES,ANYO,DEN" & vbCrLf
sConsulta = sConsulta & "  ) EVENTOS_HORA" & vbCrLf
sConsulta = sConsulta & "  LEFT JOIN" & vbCrLf
sConsulta = sConsulta & "  (" & vbCrLf
sConsulta = sConsulta & "   SELECT tipo_evento,round((sum(convert(float, NUM_EVENTOS))/@numDiaSemanaHoras),2,0) as AVG_EVENTOS" & vbCrLf
sConsulta = sConsulta & "   FROM FSAL_EVENTOS_HIST WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   WHERE FSAL_ENTORNO=@entorno " & vbCrLf
sConsulta = sConsulta & "   AND isnull(FSAL_PYME,'') = isnull(@pyme,'') " & vbCrLf
sConsulta = sConsulta & "   AND ISNULL(@tipoevento,'') = (case when @tipoevento is null then isnull(@tipoEvento,'')" & vbCrLf
sConsulta = sConsulta & "                   ELSE TIPO_EVENTO end)" & vbCrLf
sConsulta = sConsulta & "   AND DATEPART(""DW"",FECHA_EVENTO)=DATEPART(""DW"",@FECDESDE)" & vbCrLf
sConsulta = sConsulta & "   AND DATEPART(""HH"",fecha_evento)=DATEPART(""HH"",@fecdesde)" & vbCrLf
sConsulta = sConsulta & "   group by TIPO_EVENTO, DATEPART(""DW"",fecha_evento),DATEPART(""hh"",fecha_evento)" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "  )AS MEDIA" & vbCrLf
sConsulta = sConsulta & "  ON MEDIA.TIPO_EVENTO=EVENTOS_HORA.TIPO_EVENTO" & vbCrLf
sConsulta = sConsulta & "  on ranking.TIPO_EVENTO = media.TIPO_EVENTO " & vbCrLf
sConsulta = sConsulta & "order by ranking.fecha_evento desc" & vbCrLf
ExecuteSQL gRDOCon, sConsulta



sConsulta = "IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_ACCESOSTRATADOS_LOG_GS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_ACCESOSTRATADOS_LOG_GS]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSAL_ACCESOSTRATADOS_LOG_GS] @TAB AS dbo.Tabla_FSAL_IDS READONLY,@LOGNOELIMINAR INT, @OK bit   AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "if @OK=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   IF @LOGNOELIMINAR=0 " & vbCrLf
sConsulta = sConsulta & "       UPDATE SES SET TRASPASO=2 FROM SES INNER JOIN @TAB TAB ON TAB.ID = SES.ID" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "     UPDATE SES SET TRASPASO=0 FROM SES INNER JOIN @TAB tab ON tab.ID = SES.ID" & vbCrLf
sConsulta = sConsulta & "  " & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSSM_DEVOLVER_CENTROS_COSTE]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSSM_DEVOLVER_CENTROS_COSTE]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta


sConsulta = "CREATE PROCEDURE [dbo].[FSSM_DEVOLVER_CENTROS_COSTE] @VERUON TINYINT,@IDI VARCHAR(20),@USU VARCHAR(50)=NULL,@DESDE NVARCHAR(1) =''" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @TABLA NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @DESDE = '1'" & vbCrLf
sConsulta = sConsulta & "   SET @TABLA = 'USU_CC_CONTROL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   SET @TABLA = 'USU_CC_IMPUTACION'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @USU IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "    SET CONCAT_NULL_YIELDS_NULL OFF" & vbCrLf
sConsulta = sConsulta & "      " & vbCrLf
sConsulta = sConsulta & "    SELECT '###' as COD0,0 as ID,NULL as DEN, NULL AS CENTRO_SM --UON0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SELECT '###' as COD0," & vbCrLf
sConsulta = sConsulta & "       MIN(CSU.UON1 + case when csu.uon2 is not null then '#' else '' end  + CSU.UON2 + case when csu.uon3 is not null then '#' else '' end + CSU.UON3 + case when csu.uon4 is not null then '#' else '' end + CSU.UON4) AS COD" & vbCrLf
sConsulta = sConsulta & "       ,COALESCE(CSU.UON4, CSU.UON3, CSU.UON2, CSU.UON1) + ' - ' + SD.DEN AS DEN,  CSU.ID, CSU.CENTRO_SM" & vbCrLf
sConsulta = sConsulta & "        FROM CENTRO_SM_UON CSU WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN CENTRO_SM_DEN SD WITH(NOLOCK)ON CSU.CENTRO_SM=SD.CENTRO_SM AND IDI=@idi" & vbCrLf
sConsulta = sConsulta & "        GROUP BY CSU.ID ,CSU.UON4, CSU.UON3, CSU.UON2, CSU.UON1, SD.DEN, CSU.CENTRO_SM" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   IF (@VERUON=1 or @DESDE='1')    --Se muestra la estructura organizativa en �rbol" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       SELECT '###' as COD0,0 as ID,DEN, NULL as centro_sm " & vbCrLf
sConsulta = sConsulta & "       FROM PARGEN_LIT WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "       WHERE IDI=@IDI and id = 1" & vbCrLf
sConsulta = sConsulta & "        " & vbCrLf
sConsulta = sConsulta & "       --UON1" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = ' select distinct ''###'' as COD0,u.cod, u.cod + '' - '' + u.den as den, su.centro_sm  '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = @SQL + ' FROM UON1 u with(nolock)'" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = @SQL + ' inner join ' + @TABLA +  ' c with(nolock) on c.usu = @usu AND u.cod=c.uon1'" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = @SQL + ' left join centro_sm_uon su with(nolock) on u.cod=su.uon1 and su.uon2 is null and su.uon3 is null and su.uon4 is null '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = @SQL + ' where u.bajalog=0 '" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "       EXEC SP_EXECUTESQL @SQL,N'@USU VARCHAR(50)', @USU = @USU" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       --UON2" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = ' select distinct ''###'' as COD0,u.uon1,u.cod,u.cod + '' - '' + u.den as den, su.centro_sm '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = @SQL + ' from uon2 u with(nolock) '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = @SQL + ' inner join ' + @TABLA + ' c with(nolock) on c.usu = @usu and u.uon1=c.uon1 AND u.cod=c.uon2' " & vbCrLf
sConsulta = sConsulta & "       SET @SQL = @SQL + ' left join centro_sm_uon su with(nolock) on u.uon1=su.uon1 and u.cod=su.uon2 and su.uon3 is null and su.uon4 is null'" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = @SQL + ' where bajalog=0'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       EXEC SP_EXECUTESQL @SQL,N'@USU VARCHAR(50)', @USU = @USU" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "       --UON3" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = ' select distinct ''###'' as COD0,u.uon2,u.uon1,u.cod,u.cod + '' - '' + u.den as den, su.centro_sm '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = @SQL + ' from uon3 u with(nolock)'" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = @SQL + ' inner join '+ @TABLA +' c with(nolock) on c.usu = @usu and u.uon1=c.uon1 and u.uon2=c.uon2 AND u.cod=c.uon3  '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = @SQL + ' left join centro_sm_uon su with(nolock) on u.uon1=su.uon1 and u.uon2=su.uon2 and u.cod=su.uon3 and su.uon4 is null '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = @SQL + ' where bajalog=0 '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       EXEC SP_EXECUTESQL @SQL,N'@USU VARCHAR(50)', @USU = @USU" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       --UON4" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = ' select distinct ''###'' as COD0,u.uon3,u.uon2,u.uon1,u.cod,u.cod + '' - '' + u.den as den, su.centro_sm '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = @SQL + ' from uon4 u with(nolock)'" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = @SQL + ' inner join '+ @TABLA +' c with(nolock) on c.usu = @usu and u.uon1=c.uon1 and u.uon2=c.uon2 and u.uon3=c.uon3 AND u.cod=c.uon4 '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = @SQL + ' left join centro_sm_uon su with(nolock) on u.uon1=su.uon1 and u.uon2=su.uon2 and u.uon3=su.uon3 and u.cod=su.uon4'" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = @SQL + ' where bajalog=0'" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "       EXEC SP_EXECUTESQL @SQL,N'@USU VARCHAR(50)', @USU = @USU" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "   ELSE            -- los datos de los centros se obtienen de la tabla CENTROS_SM" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       SET CONCAT_NULL_YIELDS_NULL OFF" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       SELECT '###' as COD0,0 as ID,NULL as DEN, NULL AS CENTRO_SM --UON0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       SELECT '###' as COD0,CI.USU, " & vbCrLf
sConsulta = sConsulta & "       MIN(CSU.UON1 + case when csu.uon2 is not null then '#' else '' end  + CSU.UON2 + case when csu.uon3 is not null then '#' else '' end + CSU.UON3 + case when csu.uon4 is not null then '#' else '' end + CSU.UON4) AS COD" & vbCrLf
sConsulta = sConsulta & "       ,COALESCE(CI.UON4, CI.UON3, CI.UON2, CI.UON1) + ' - ' + SD.DEN AS DEN,  CSU.ID, CSU.CENTRO_SM" & vbCrLf
sConsulta = sConsulta & "        FROM CENTRO_SM_UON CSU WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN CENTRO_SM_DEN SD WITH(NOLOCK)ON CSU.CENTRO_SM=SD.CENTRO_SM AND IDI=@idi" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN USU_CC_IMPUTACION CI WITH(NOLOCK) ON CSU.UON1=CI.UON1 AND isnull(CSU.UON2,'')=isnull(CI.UON2,'') " & vbCrLf
sConsulta = sConsulta & "        AND isnull(CSU.UON3,'')=isnull(CI.UON3,'') AND isnull(CSU.UON4,'')=isnull(CI.UON4,'')" & vbCrLf
sConsulta = sConsulta & "        WHERE CI.USU=@usu" & vbCrLf
sConsulta = sConsulta & "       GROUP BY CI.USU, CSU.ID ,CI.UON4, CI.UON3, CI.UON2, CI.UON1,SD.DEN, CSU.CENTRO_SM" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "       SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta



End Sub

Public Sub V_31900_8_Tablas_094()
    Dim sConsulta As String
    
    sConsulta = "ALTER TABLE SES ADD TRASPASO TINYINT NOT NULL DEFAULT 0" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
End Sub

Public Function CodigoDeActualizacion31900_08_1894_A31900_08_1895() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim oADOConnection As ADODB.Connection
    Dim sConsulta As String

    On Error GoTo Error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Actualiza Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 3
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    frmProgreso.ProgressBar1.Refresh
    V_31900_8_Storeds_095
    
    sConsulta = "UPDATE VERSION SET NUM ='3.00.18.95'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103), PRODVER='31900.8'"
    ExecuteSQL gRDOCon, sConsulta
        
    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False
        
    CodigoDeActualizacion31900_08_1894_A31900_08_1895 = True
    Exit Function

Error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    CodigoDeActualizacion31900_08_1894_A31900_08_1895 = False
End Function

Public Sub V_31900_8_Storeds_095()
    Dim sConsulta As String

sConsulta = "IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSAL_CARGAR_EVENTOS_POR_SEMANA]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSAL_CARGAR_EVENTOS_POR_SEMANA]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSAL_CARGAR_EVENTOS_POR_SEMANA]  " & vbCrLf
sConsulta = sConsulta & "   @fecdesde DATETIME," & vbCrLf
sConsulta = sConsulta & "   @entorno nvarchar(50)," & vbCrLf
sConsulta = sConsulta & "   @tipoevento INT=null," & vbCrLf
sConsulta = sConsulta & "   @fechasta DATETIME," & vbCrLf
sConsulta = sConsulta & "   @pyme nvarchar(50)=null," & vbCrLf
sConsulta = sConsulta & "   @idioma nvarchar(5)" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @fechacomienzo DATETIME" & vbCrLf
sConsulta = sConsulta & "declare @numSemanas float" & vbCrLf
sConsulta = sConsulta & "SET @fechacomienzo=CONVERT(datetime,'2010-01-01')" & vbCrLf
sConsulta = sConsulta & "set @numSemanas = DATEDIFF(""WW"",@fechacomienzo,GETDATE())" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "select RANKING.FECHA_EVENTO," & vbCrLf
sConsulta = sConsulta & " SEMANA, " & vbCrLf
sConsulta = sConsulta & " DEN," & vbCrLf
sConsulta = sConsulta & " RANKING.rank," & vbCrLf
sConsulta = sConsulta & " media.AVG_EVENTOS," & vbCrLf
sConsulta = sConsulta & " RANKING.data   " & vbCrLf
sConsulta = sConsulta & "from " & vbCrLf
sConsulta = sConsulta & " (" & vbCrLf
sConsulta = sConsulta & " select fecha_evento," & vbCrLf
sConsulta = sConsulta & "   tipo_evento," & vbCrLf
sConsulta = sConsulta & "   rank() OVER (Partition by tipo_evento ORDER BY  fecha_evento asc ,id asc ) as rank," & vbCrLf
sConsulta = sConsulta & "  data" & vbCrLf
sConsulta = sConsulta & " from fsal_eventos WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & " where fecha_evento between @fecdesde and @fechasta" & vbCrLf
sConsulta = sConsulta & " ) RANKING" & vbCrLf
sConsulta = sConsulta & "inner join " & vbCrLf
sConsulta = sConsulta & " ( " & vbCrLf
sConsulta = sConsulta & "  select " & vbCrLf
sConsulta = sConsulta & "   tipo_evento," & vbCrLf
sConsulta = sConsulta & "   DEN," & vbCrLf
sConsulta = sConsulta & "   DATEPART(""WW"",FECHA_EVENTO) AS SEMANA," & vbCrLf
sConsulta = sConsulta & "   sum(num_eventos) as num_eventos " & vbCrLf
sConsulta = sConsulta & "  from FSAL_EVENTOS_HIST WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "  left join FSAL_TIPO_EVENTO_DEN on FSAL_EVENTOS_HIST.TIPO_EVENTO=FSAL_TIPO_EVENTO_DEN.ID" & vbCrLf
sConsulta = sConsulta & "  where " & vbCrLf
sConsulta = sConsulta & "  @fecdesde<=FECHA_EVENTO" & vbCrLf
sConsulta = sConsulta & "  and @fechasta>=FECHA_EVENTO " & vbCrLf
sConsulta = sConsulta & "  --mes >= datepart(""mm"",@fecdesde)" & vbCrLf
sConsulta = sConsulta & "  --AND MES<= DATEPART(""MM"",@FECHASTA) " & vbCrLf
sConsulta = sConsulta & "  --AND DIA>=DATEPART(""DD"",@FECDESDE) " & vbCrLf
sConsulta = sConsulta & "  --AND DIA<=DATEPART(""DD"",@FECHASTA) " & vbCrLf
sConsulta = sConsulta & "  --AND ANYO>=DATEPART(""YY"",@FECDESDE) " & vbCrLf
sConsulta = sConsulta & "  --AND ANYO<=DATEPART(""YY"",@FECHASTA) " & vbCrLf
sConsulta = sConsulta & "  and ISNULL(@tipoevento,'') = (case when @tipoevento is null then isnull(@tipoEvento,'')" & vbCrLf
sConsulta = sConsulta & "                   ELSE TIPO_EVENTO end)" & vbCrLf
sConsulta = sConsulta & "  AND FSAL_ENTORNO=@entorno " & vbCrLf
sConsulta = sConsulta & "  and ISNULL(FSAL_PYME,'')=ISNULL(@pyme,'')" & vbCrLf
sConsulta = sConsulta & "  and FSAL_TIPO_EVENTO_DEN.IDI=@idioma  " & vbCrLf
sConsulta = sConsulta & "  group by tipo_evento,DATEPART(""WW"",FECHA_EVENTO),DEN" & vbCrLf
sConsulta = sConsulta & "  ) EVENTOS_SEMANA" & vbCrLf
sConsulta = sConsulta & "  LEFT JOIN" & vbCrLf
sConsulta = sConsulta & "  (" & vbCrLf
sConsulta = sConsulta & "   SELECT TIPO_EVENTO, round(sum(convert(float, NUM_EVENTOS))/@numSemanas,2,0) as AVG_EVENTOS" & vbCrLf
sConsulta = sConsulta & "   FROM FSAL_EVENTOS_HIST WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   WHERE FSAL_ENTORNO=@entorno " & vbCrLf
sConsulta = sConsulta & "   AND isnull(FSAL_PYME,'') = isnull(@pyme,'') " & vbCrLf
sConsulta = sConsulta & "   AND ISNULL(@tipoevento,'') = (case when @tipoevento is null then isnull(@tipoEvento,'')" & vbCrLf
sConsulta = sConsulta & "                   ELSE TIPO_EVENTO end)" & vbCrLf
sConsulta = sConsulta & "   --AND DATEPART(""DW"",FECHA_EVENTO)=DATEPART(""DW"",@FECDESDE)" & vbCrLf
sConsulta = sConsulta & "   GROUP BY TIPO_EVENTO" & vbCrLf
sConsulta = sConsulta & "  )AS MEDIA" & vbCrLf
sConsulta = sConsulta & "  ON MEDIA.TIPO_EVENTO=EVENTOS_SEMANA.TIPO_EVENTO" & vbCrLf
sConsulta = sConsulta & "  on ranking.TIPO_EVENTO = media.TIPO_EVENTO " & vbCrLf
sConsulta = sConsulta & "order by ranking.fecha_evento desc" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


End Sub


Public Function CodigoDeActualizacion31900_08_1895_A31900_08_1896() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim oADOConnection As ADODB.Connection
    Dim sConsulta As String

    On Error GoTo Error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    
    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Actualiza Tablas"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    frmProgreso.ProgressBar1.Refresh
    V_31900_8_Tablas_096
    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Actualiza Datos"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    frmProgreso.ProgressBar1.Refresh
    V_31900_8_Datos_096
    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Actualiza Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    frmProgreso.ProgressBar1.Refresh
    V_31900_8_Storeds_096
    
    sConsulta = "UPDATE VERSION SET NUM ='3.00.18.96'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103), PRODVER='31900.8'"
    ExecuteSQL gRDOCon, sConsulta
        
    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False
        
    CodigoDeActualizacion31900_08_1895_A31900_08_1896 = True
    Exit Function

Error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    CodigoDeActualizacion31900_08_1895_A31900_08_1896 = False
End Function

Public Sub V_31900_8_Tablas_096()
    Dim sConsulta As String
    
    sConsulta = "ALTER TABLE INSTANCIA_EST ALTER COLUMN [COMENT] [nvarchar](4000) NULL" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    
    sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[COPIA_ADJUN]') AND type in (N'U'))" & vbCrLf
    sConsulta = sConsulta & "DROP TABLE [dbo].[COPIA_ADJUN]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

End Sub

Public Sub V_31900_8_Datos_096()
    Dim sConsulta As String
    
    sConsulta = "UPDATE PARINS_DOT SET PLANTILLA=REPLACE(PLANTILLA,'.DOT','.HTM') WHERE REF='PETOFEMAIL'" & vbCrLf
    sConsulta = sConsulta & "UPDATE PARINS_DOT SET PLANTILLA=REPLACE(PLANTILLA,'.DOT','.HTM') WHERE REF='PETOFEWEB'" & vbCrLf
    sConsulta = sConsulta & "UPDATE PARINS_DOT SET PLANTILLA=REPLACE(PLANTILLA,'.DOT','.HTM') WHERE REF='PETOFESUBMAIL'" & vbCrLf
    sConsulta = sConsulta & "UPDATE PARINS_DOT SET PLANTILLA=REPLACE(PLANTILLA,'.DOT','.HTM') WHERE REF='PETOFESUBWEB'" & vbCrLf
    sConsulta = sConsulta & "UPDATE PARINS_DOT SET PLANTILLA=REPLACE(PLANTILLA,'.DOT','.HTM') WHERE REF='OBJPETMAIL'" & vbCrLf
    sConsulta = sConsulta & "UPDATE PARINS_DOT SET PLANTILLA=REPLACE(PLANTILLA,'.DOT','.HTM') WHERE REF='OBJPETWEB'" & vbCrLf
    sConsulta = sConsulta & "UPDATE PARINS_DOT SET PLANTILLA=REPLACE(PLANTILLA,'.DOT','.HTM') WHERE REF='ADJMAIL'" & vbCrLf
    sConsulta = sConsulta & "UPDATE PARINS_DOT SET PLANTILLA=REPLACE(PLANTILLA,'.DOT','.HTM') WHERE REF='ADJNOMAIL'" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta

End Sub


Public Sub V_31900_8_Storeds_096()
    Dim sConsulta As String

sConsulta = "IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_CONTRATOS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_CONTRATOS]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE procedure [dbo].[FSPM_CONTRATOS] " & vbCrLf
sConsulta = sConsulta & "   @USU NVARCHAR(50)," & vbCrLf
sConsulta = sConsulta & "   @IDI NVARCHAR(3)," & vbCrLf
sConsulta = sConsulta & "   @CODIGO NVARCHAR(50)='', " & vbCrLf
sConsulta = sConsulta & "   @TIPO NVARCHAR(50)=''," & vbCrLf
sConsulta = sConsulta & "   @TITULO NVARCHAR(4000)=''," & vbCrLf
sConsulta = sConsulta & "   @PROVE NVARCHAR(200)=NULL," & vbCrLf
sConsulta = sConsulta & "   @PETICIONARIO NVARCHAR(200)=NULL," & vbCrLf
sConsulta = sConsulta & "   @FECHA_DESDE DATETIME=NULL, " & vbCrLf
sConsulta = sConsulta & "   @FECHA_HASTA DATETIME=NULL," & vbCrLf
sConsulta = sConsulta & "   @MATERIAL NVARCHAR (50)='', " & vbCrLf
sConsulta = sConsulta & "   @ART NVARCHAR(50)='', " & vbCrLf
sConsulta = sConsulta & "   @EMP INT=0, " & vbCrLf
sConsulta = sConsulta & "   @ANYO INT=0, " & vbCrLf
sConsulta = sConsulta & "   @GMN1 NVARCHAR (50)='', " & vbCrLf
sConsulta = sConsulta & "   @COD_PROCE INT=0, " & vbCrLf
sConsulta = sConsulta & "   @ESTADO  NVARCHAR (50)='', " & vbCrLf
sConsulta = sConsulta & "   @FILAESTADO NVARCHAR (50), " & vbCrLf
sConsulta = sConsulta & "   @NOM_TABLA NVARCHAR(50)=''," & vbCrLf
sConsulta = sConsulta & "   @ORDENACION NVARCHAR(50)=''," & vbCrLf
sConsulta = sConsulta & "   @PALABRA_CLAVE NVARCHAR(200)='', " & vbCrLf
sConsulta = sConsulta & "   @PAGENUMBER INT=1, " & vbCrLf
sConsulta = sConsulta & "   @PAGESIZE INT=1000, " & vbCrLf
sConsulta = sConsulta & "   @OBTENERTODOSCONTRATOS TINYINT=0," & vbCrLf
sConsulta = sConsulta & "   @CENTROCOSTE VARCHAR(50) = NULL," & vbCrLf
sConsulta = sConsulta & "   @PARTIDASPRES TBCADENATEXTO2 READONLY" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @SWHERE AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL_CONT AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL2 AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLFROM AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLFROM_EST AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PARAM NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SUSTITUTO AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @LONGGMN1 INT = 0" & vbCrLf
sConsulta = sConsulta & "DECLARE @LONGGMN2 INT = 0" & vbCrLf
sConsulta = sConsulta & "DECLARE @LONGGMN3 INT = 0" & vbCrLf
sConsulta = sConsulta & "DECLARE @LONGGMN4 INT = 0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @LONGGMN1 = LONGITUD FROM DIC WITH(NOLOCK) WHERE NOMBRE='GMN1'" & vbCrLf
sConsulta = sConsulta & "SELECT @LONGGMN2 = LONGITUD FROM DIC WITH(NOLOCK) WHERE NOMBRE='GMN2'" & vbCrLf
sConsulta = sConsulta & "SELECT @LONGGMN3 = LONGITUD FROM DIC WITH(NOLOCK) WHERE NOMBRE='GMN3'" & vbCrLf
sConsulta = sConsulta & "SELECT @LONGGMN4 = LONGITUD FROM DIC WITH(NOLOCK) WHERE NOMBRE='GMN4'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @FECHA_HASTA IS NOT NULL --para incluir los del mismo dia" & vbCrLf
sConsulta = sConsulta & "   SET @FECHA_HASTA =DATEADD(DD, 1, @FECHA_HASTA)" & vbCrLf
sConsulta = sConsulta & "SET @SWHERE = ''" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Tablas basicas para las consultas " & vbCrLf
sConsulta = sConsulta & "SET @SQLFROM = N' FROM CONTRATO C WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN INSTANCIA I WITH (NOLOCK) ON I.ID = C.INSTANCIA" & vbCrLf
sConsulta = sConsulta & "INNER JOIN SOLICITUD S WITH (NOLOCK) ON I.SOLICITUD=S.ID  " & vbCrLf
sConsulta = sConsulta & "INNER JOIN TIPO_SOLICITUDES TS WITH (NOLOCK) ON S.TIPO=TS.ID " & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PROVE PRO WITH (NOLOCK) ON PRO.COD = C.PROVE" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PER WITH (NOLOCK) ON PER.COD = I.PETICIONARIO" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN EMP WITH (NOLOCK) ON C.EMP=EMP.ID '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLFROM_EST = N' FROM CONTRATO C WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN INSTANCIA I WITH (NOLOCK)  ON I.ID = C.INSTANCIA" & vbCrLf
sConsulta = sConsulta & "INNER JOIN SOLICITUD S WITH (NOLOCK) ON I.SOLICITUD=S.ID  " & vbCrLf
sConsulta = sConsulta & "INNER JOIN TIPO_SOLICITUDES TS WITH (NOLOCK) ON S.TIPO=TS.ID '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SWHERE =' WHERE TS.TIPO=5 '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Condiciones where" & vbCrLf
sConsulta = sConsulta & "IF @CODIGO IS NOT NULL and @CODIGO <> ''" & vbCrLf
sConsulta = sConsulta & "    SET @SWHERE = @SWHERE +  ' AND C.CODIGO LIKE ''%' + @CODIGO + '%'''" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @TIPO IS NOT NULL and @TIPO<>''" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "    SET @TIPO = REPLACE(@TIPO,'$$$',',')" & vbCrLf
sConsulta = sConsulta & "    SET @SWHERE = @SWHERE + ' AND S.ID IN (' + @TIPO +')'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @TITULO IS NOT NULL AND @TITULO <> ''" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @SQLFROM = @SQLFROM + ' INNER JOIN INSTANCIA_VISORES_PM C1 WITH(NOLOCK) ON I.ID=C1.INSTANCIA'" & vbCrLf
sConsulta = sConsulta & "   SET @SWHERE = @SWHERE + ' AND (C1.TITULO_TEXT LIKE ''%' + @TITULO+ '%'' OR CONVERT(VARCHAR,TITULO_FEC) LIKE ''%' + @TITULO + '%'')'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @PROVE IS NOT NULL AND @PROVE <> ''" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "    SET @PROVE = REPLACE(@PROVE,'$$$',''',''')" & vbCrLf
sConsulta = sConsulta & "    SET @PROVE = '''' + @PROVE + ''''" & vbCrLf
sConsulta = sConsulta & "    SET @SWHERE = @SWHERE + ' AND PRO.COD IN (' + @PROVE + ')'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @PETICIONARIO IS NOT NULL AND @PETICIONARIO  <> ''" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "    SET @PETICIONARIO = REPLACE(@PETICIONARIO,'$$$',''',''')" & vbCrLf
sConsulta = sConsulta & "    SET @PETICIONARIO = '''' + @PETICIONARIO + ''''" & vbCrLf
sConsulta = sConsulta & "    SET @SWHERE = @SWHERE + ' AND PER.COD IN (' +@PETICIONARIO+')'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ESTADO IS NOT NULL AND @ESTADO<>''" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @ESTADO = REPLACE(@ESTADO,'$$$',',')" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @FILAESTADO IS NOT NULL AND @FILAESTADO  <>''" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @FILAESTADO = REPLACE(@FILAESTADO,'$$$',',')" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @FECHA_DESDE IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "      SET @SWHERE =@SWHERE + ' AND C.FEC_INI>= @FECHA_DESDE'" & vbCrLf
sConsulta = sConsulta & "      " & vbCrLf
sConsulta = sConsulta & "IF @FECHA_HASTA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "      SET @SWHERE = @SWHERE + ' AND C.FEC_FIN <= @FECHA_HASTA'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @EMP IS NOT NULL AND @EMP<>0 " & vbCrLf
sConsulta = sConsulta & "   SET @SWHERE = @SWHERE + ' AND EMP.ID = @EMP'" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "IF (@ANYO<>0 AND @GMN1<>'' AND @COD_PROCE<>0) OR (@MATERIAL IS NOT NULL AND @MATERIAL <>'') OR (@ART IS NOT NULL AND @ART <>'')" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @SQLFROM=@SQLFROM + ' INNER JOIN CONTRATO_ITEM CI WITH (NOLOCK) ON C.ID=CI.CONTRATO'" & vbCrLf
sConsulta = sConsulta & "   IF (@ANYO<>0 AND @GMN1<>'' AND @COD_PROCE<>0)" & vbCrLf
sConsulta = sConsulta & "       SET @SWHERE = @SWHERE + ' AND CI.GMN1=@GMN1 AND CI.PROCE=@COD_PROCE AND CI.ANYO=@ANYO'" & vbCrLf
sConsulta = sConsulta & "   DECLARE @HAYITEM BIT = 0" & vbCrLf
sConsulta = sConsulta & "   IF (@MATERIAL IS NOT NULL AND @MATERIAL <>'')" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "       --CUANDO SE BUSCA UN MATERIAL HAY QUE MIRAR EL MATERIAL (LOS GMN) DE LOS ITEMS ASOCIADOS AL CONTRATO (O SEA, LOS ITEMS QUE HAYA EN CONTRATO_ITEM PARA EL CONTRATO, QUE A SU VEZ ENLAZAN A LA TABLA ITEM: CONTRATO->CONTRATO_ITEM->ITEM)" & vbCrLf
sConsulta = sConsulta & "       --  EN ITEM, SI EL CAMPO ART VIENE INFORMADO, HAY QUE BUSCAR SU MATERIAL EN LA TABLA ART4 (RELACION:ART4.COD = ITEM.ART)" & vbCrLf
sConsulta = sConsulta & "       --  EN ITEM, SI EL CAMPO ART NO VIENE INFORMADO, LOS GMN DEL MATERIAL ESTAR�N DEFINIDOS DIRECTAMENTE EN LA TABLA ITEM." & vbCrLf
sConsulta = sConsulta & "       SET @SQLFROM = @SQLFROM + ' INNER JOIN ITEM IT WITH(NOLOCK) ON IT.ANYO = CI.ANYO AND IT.GMN1_PROCE = CI.GMN1 AND IT.PROCE = CI.PROCE AND IT.ID=CI.ITEM AND CASE WHEN ART IS NULL THEN (SPACE(@LONGGMN1-LEN(IT.GMN1)) + COALESCE(IT.GMN1,'''') + SPACE(@LONGGMN2-LEN(IT.GMN2)) + COALESCE(IT.GMN2,'''') + SPACE(@LONGGMN3-LEN(IT.GMN3)) + COALESCE(IT.GMN3,'''') + SPACE(@LONGGMN4-LEN(IT.GMN4)) + COALESCE(IT.GMN4,'''')) ELSE @MATERIAL END = @MATERIAL" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN ART4 A4 WITH(NOLOCK) ON A4.COD = IT.ART AND CASE WHEN ART IS NULL THEN @MATERIAL ELSE (SPACE(@LONGGMN1-LEN(A4.GMN1)) + COALESCE(A4.GMN1,'''') + SPACE(@LONGGMN2-LEN(A4.GMN2)) + COALESCE(A4.GMN2,'''') + SPACE(@LONGGMN3-LEN(A4.GMN3)) + COALESCE(A4.GMN3,'''') + SPACE(@LONGGMN4-LEN(A4.GMN4)) + COALESCE(A4.GMN4,'''')) END = @MATERIAL'" & vbCrLf
sConsulta = sConsulta & "       SET @HAYITEM = 1" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "   IF (@ART IS NOT NULL AND @ART <>'')" & vbCrLf
sConsulta = sConsulta & "       IF @HAYITEM = 0" & vbCrLf
sConsulta = sConsulta & "           SET @SQLFROM = @SQLFROM + ' INNER JOIN ITEM IT WITH(NOLOCK) ON IT.ANYO = CI.ANYO AND IT.GMN1_PROCE = CI.GMN1 AND IT.PROCE = CI.PROCE AND IT.ID=CI.ITEM " & vbCrLf
sConsulta = sConsulta & "           AND IT.ART = @ART '" & vbCrLf
sConsulta = sConsulta & "       ELSE" & vbCrLf
sConsulta = sConsulta & "           SET @SQLFROM = @SQLFROM + ' AND IT.ART = @ART '" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "IF @NOM_TABLA IS NOT NULL AND @NOM_TABLA <>''" & vbCrLf
sConsulta = sConsulta & "   SET @SQLFROM = @SQLFROM + ' INNER JOIN ' + @IDI + '_' + @NOM_TABLA + ' F WITH (NOLOCK) ON I.ID = F.INSTANCIA'" & vbCrLf
sConsulta = sConsulta & "        " & vbCrLf
sConsulta = sConsulta & "IF @PALABRA_CLAVE IS NOT NULL AND @PALABRA_CLAVE <> ''" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @SQLFROM = @SQLFROM + ' INNER JOIN PM_COPIA_ADJUN PCA WITH(NOLOCK) ON C.ID=PCA.ID_CONTRATO'" & vbCrLf
sConsulta = sConsulta & "   SET @SWHERE = @SWHERE + ' AND PCA.DATA LIKE CAST(''%' + @PALABRA_CLAVE + '%'' as varbinary(max))'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "IF (SELECT COUNT(1) FROM USU U WITH (NOLOCK) WHERE U.FSWS_SUSTITUCION=@USU)>0 " & vbCrLf
sConsulta = sConsulta & " SET @SUSTITUTO =1" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & " SET @SUSTITUTO =0" & vbCrLf
sConsulta = sConsulta & "--CALIDAD " & vbCrLf
sConsulta = sConsulta & "IF (EXISTS(SELECT 1 FROM @PARTIDASPRES))" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @SQLFROM = @SQLFROM + ' INNER JOIN INSTANCIA_CENTROS_PARTIDAS ICP1 WITH(NOLOCK) ON I.ID=ICP1.INSTANCIA AND ICP1.TIPO_CAMPO_GS = 130 '" & vbCrLf
sConsulta = sConsulta & "   --CALIDAD" & vbCrLf
sConsulta = sConsulta & "   SET @SQLFROM = @SQLFROM + ' INNER JOIN @PARTIDASPRES PP ON ICP1.PRES5 = PP.CADENA1 AND ICP1.VALOR_TEXT = PP.CADENA2 '" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "IF (NOT @CENTROCOSTE IS NULL AND @CENTROCOSTE <> '')" & vbCrLf
sConsulta = sConsulta & "   SET @SQLFROM = @SQLFROM + ' INNER JOIN INSTANCIA_CENTROS_PARTIDAS ICP2 WITH(NOLOCK) ON I.ID=ICP2.INSTANCIA AND ICP2.TIPO_CAMPO_GS = 129 AND ICP2.VALOR_TEXT = @CENTROCOSTE '" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "--------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--crea la tabla temporal" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE #G1 (ID INT,CODIGO NVARCHAR(50),ICONO TINYINT,OBV TINYINT,ACCION_APROBAR INT,ACCION_RECHAZAR INT)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = ''" & vbCrLf
sConsulta = sConsulta & "SET @PARAM =N'@USU NVARCHAR (50),@CODIGO NVARCHAR(50)='''',@TIPO NVARCHAR(50),@FECHA_DESDE DATETIME=NULL,@FECHA_HASTA DATETIME=NULL,@MATERIAL NVARCHAR (50)='''',@ART NVARCHAR (50)='''',@EMP INT=0,@GMN1 NVARCHAR (50)='''',@COD_PROCE INT=0,@ANYO INT=0,@ESTADO NVARCHAR (50)='''',@FILAESTADO NVARCHAR (50)='''',@LONGGMN1 INT,@LONGGMN2 INT,@LONGGMN3 INT,@LONGGMN4 INT,@CENTROCOSTE VARCHAR(50) = NULL, @PARTIDASPRES TBCADENATEXTO2 READONLY'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--1. Solicitudes abiertas por usted." & vbCrLf
sConsulta = sConsulta & "CREATE TABLE #T_ABIERTAS (ID INT, CODIGO NVARCHAR(50))" & vbCrLf
sConsulta = sConsulta & "SET @SQL = 'INSERT INTO #T_ABIERTAS (ID,CODIGO) SELECT I.ID,C.CODIGO ' + @SQLFROM + @SWHERE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' AND I.NUM IS NOT NULL AND I.PETICIONARIO = @USU AND I.EN_PROCESO<>2'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL,@PARAM,@CODIGO=@CODIGO,@TIPO=@TIPO,@FECHA_DESDE=@FECHA_DESDE,@FECHA_HASTA=@FECHA_HASTA,@MATERIAL=@MATERIAL,@ART=@ART,@EMP=@EMP,@GMN1=@GMN1,@COD_PROCE=@COD_PROCE,@ANYO=@ANYO,@ESTADO=@ESTADO,@FILAESTADO=@FILAESTADO,@USU=@USU,@LONGGMN1=@LONGGMN1,@LONGGMN2=@LONGGMN2,@LONGGMN3=@LONGGMN3,@LONGGMN4=@LONGGMN4,@CENTROCOSTE=@CENTROCOSTE,@PARTIDASPRES=@PARTIDASPRES" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--2. Procesos en los que particip�  " & vbCrLf
sConsulta = sConsulta & "CREATE TABLE #T_PARTICIPO (ID INT,CODIGO NVARCHAR(50))" & vbCrLf
sConsulta = sConsulta & "SET @SQL = 'INSERT INTO #T_PARTICIPO (ID,CODIGO) SELECT I.ID,C.CODIGO ' + @SQLFROM " & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ' INNER JOIN INSTANCIA_EST WITH (NOLOCK) ON I.ID=INSTANCIA_EST.INSTANCIA AND INSTANCIA_EST.PER=@USU'" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + @SWHERE + ' AND I.NUM IS NOT NULL AND  (INSTANCIA_EST.ROL IS NOT NULL OR (INSTANCIA_EST.ROL IS  NULL AND (INSTANCIA_EST.ACCION=3 OR INSTANCIA_EST.ACCION=4)))'  " & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL,@PARAM,@CODIGO=@CODIGO,@TIPO=@TIPO,@FECHA_DESDE=@FECHA_DESDE,@FECHA_HASTA=@FECHA_HASTA,@MATERIAL=@MATERIAL,@ART=@ART,@EMP=@EMP,@GMN1=@GMN1,@COD_PROCE=@COD_PROCE,@ANYO=@ANYO,@ESTADO=@ESTADO,@FILAESTADO=@FILAESTADO,@USU=@USU,@LONGGMN1=@LONGGMN1,@LONGGMN2=@LONGGMN2,@LONGGMN3=@LONGGMN3,@LONGGMN4=@LONGGMN4,@CENTROCOSTE=@CENTROCOSTE,@PARTIDASPRES=@PARTIDASPRES" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--3. Pendientes de aprobar por usted" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE #T_PENDIENTES (ID INT,CODIGO NVARCHAR(50), ACCION_APROBAR INT, ACCION_RECHAZAR INT)" & vbCrLf
sConsulta = sConsulta & "  SET @SQL = 'INSERT INTO #T_PENDIENTES (ID,CODIGO, ACCION_APROBAR,ACCION_RECHAZAR ) SELECT I.ID,C.CODIGO,isnull(RAA.ACCION,0) AS ACCION_APROBAR,isnull(RAR.ACCION,0) AS ACCION_RECHAZAR ' + @SQLFROM " & vbCrLf
sConsulta = sConsulta & "  SET @SQL=@SQL + ' INNER JOIN INSTANCIA_BLOQUE IB WITH (NOLOCK) ON I.ID=IB.INSTANCIA AND IB.EN_PROCESO=0 AND IB. ESTADO=1 AND IB.BLOQ<>1'" & vbCrLf
sConsulta = sConsulta & "  SET @SQL=@SQL + ' INNER JOIN PM_COPIA_ROL_BLOQUE RB WITH (NOLOCK) ON RB.BLOQUE=IB.BLOQUE AND IB.ESTADO=1 AND IB.BLOQ<>1 AND IB.TRASLADADA<>1'" & vbCrLf
sConsulta = sConsulta & "  SET @SQL=@SQL + ' INNER JOIN PM_COPIA_ROL R WITH (NOLOCK)  ON RB.ROL=R.ID'" & vbCrLf
sConsulta = sConsulta & "  SET @SQL=@SQL + ' INNER JOIN PM_COPIA_BLOQUE B WITH (NOLOCK) ON RB.BLOQUE=B.ID'" & vbCrLf
sConsulta = sConsulta & "  SET @SQL=@SQL + ' LEFT JOIN PM_COPIA_ROL_ACCION RAA WITH(NOLOCK)ON RAA.ROL=R.ID AND RAA.BLOQUE=RB.BLOQUE AND RAA.APROBAR=1'   --acciones del rol actual (Aprobar)" & vbCrLf
sConsulta = sConsulta & "  SET @SQL=@SQL + ' LEFT JOIN PM_COPIA_ROL_ACCION RAR WITH(NOLOCK)ON RAR.ROL=R.ID AND RAR.BLOQUE=RB.BLOQUE AND RAR.RECHAZAR=1'   --acciones del rol actual (Rechazar)" & vbCrLf
sConsulta = sConsulta & "  SET @SQL = @SQL + @SWHERE + ' AND I.NUM IS NOT NULL AND I.EN_PROCESO=0 AND I.TRASLADADA=0 AND (I.ESTADO = 2 OR I.ESTADO = 3 OR I.ESTADO = 4) AND '" & vbCrLf
sConsulta = sConsulta & "  IF @SUSTITUTO =1" & vbCrLf
sConsulta = sConsulta & "    SET @SQL=@SQL + ' ( (R.PER=@USU AND (SELECT ISNULL(U.FSWS_SUSTITUCION,'''') FROM USU U WITH (NOLOCK) WHERE U.PER=@USU AND U.BAJA=0) = '''')  OR R.PER IN ( SELECT U.PER FROM USU U WITH (NOLOCK) WHERE U.FSWS_SUSTITUCION=@USU))'" & vbCrLf
sConsulta = sConsulta & "  ELSE" & vbCrLf
sConsulta = sConsulta & "    SET @SQL=@SQL + '( (R.PER=@USU AND (SELECT ISNULL(U.FSWS_SUSTITUCION,'''') FROM USU U WITH (NOLOCK) WHERE U.PER=@USU AND U.BAJA=0) = ''''))'" & vbCrLf
sConsulta = sConsulta & "  SET @SQL=@SQL + ' AND NOT (B.TIPO=1 AND I.PETICIONARIO<>R.PER)'  " & vbCrLf
sConsulta = sConsulta & "  SET @SQL=@SQL + ' UNION '" & vbCrLf
sConsulta = sConsulta & "  SET @SQL=@SQL + ' SELECT I.ID,C.CODIGO,isnull(RAA.ACCION,0) AS ACCION_APROBAR, isnull(RAR.ACCION,0) AS ACCION_RECHAZAR' + @SQLFROM + ' INNER JOIN INSTANCIA_BLOQUE IB WITH (NOLOCK) ON I.ID=IB.INSTANCIA AND IB.EN_PROCESO=0 AND IB.ESTADO=1 AND IB.BLOQ<>1 AND IB.TRASLADADA<>1'" & vbCrLf
sConsulta = sConsulta & "  SET @SQL=@SQL + ' INNER JOIN PM_COPIA_ROL_BLOQUE RB WITH (NOLOCK)  ON RB.BLOQUE=IB.BLOQUE'" & vbCrLf
sConsulta = sConsulta & "  SET @SQL=@SQL + ' INNER JOIN PM_COPIA_ROL R WITH (NOLOCK)  ON RB.ROL=R.ID AND R.PER IS NULL AND R.COMO_ASIGNAR=3  AND R.TIPO<>5'" & vbCrLf
sConsulta = sConsulta & "  SET @SQL=@SQL + '  INNER JOIN PM_COPIA_PARTICIPANTES PT WITH (NOLOCK)  ON PT.ROL=R.ID'" & vbCrLf
sConsulta = sConsulta & "  SET @SQL=@SQL + ' LEFT JOIN PM_COPIA_ROL_ACCION RAA WITH(NOLOCK)ON RAA.ROL=R.ID AND RAA.BLOQUE=RB.BLOQUE AND RAA.APROBAR=1'   --acciones del rol actual (Aprobar)" & vbCrLf
sConsulta = sConsulta & "  SET @SQL=@SQL + ' LEFT JOIN PM_COPIA_ROL_ACCION RAR WITH(NOLOCK)ON RAR.ROL=R.ID AND RAR.BLOQUE=RB.BLOQUE AND RAR.RECHAZAR=1'   --acciones del rol actual (Rechazar)" & vbCrLf
sConsulta = sConsulta & "  SET @SQL=@SQL + @SWHERE + ' AND I.NUM IS NOT NULL AND I.EN_PROCESO=0 AND I.TRASLADADA=0 AND I.ESTADO=2 AND '" & vbCrLf
sConsulta = sConsulta & "  IF @SUSTITUTO =1" & vbCrLf
sConsulta = sConsulta & "    SET @SQL=@SQL + ' ( (PT.PER=@USU AND (SELECT ISNULL(U.FSWS_SUSTITUCION,'''') FROM USU U WITH (NOLOCK) WHERE U.PER=@USU AND U.BAJA=0) = '''') OR PT.PER IN ( SELECT U.PER FROM USU U WITH (NOLOCK) WHERE U.FSWS_SUSTITUCION=@USU))'" & vbCrLf
sConsulta = sConsulta & "  ELSE" & vbCrLf
sConsulta = sConsulta & "    SET @SQL=@SQL + ' ( (PT.PER=@USU AND (SELECT ISNULL(U.FSWS_SUSTITUCION,'''') FROM USU U WITH (NOLOCK) WHERE U.PER=@USU AND U.BAJA=0) = ''''))'                                         " & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL,@PARAM,@CODIGO=@CODIGO,@TIPO=@TIPO,@FECHA_DESDE=@FECHA_DESDE,@FECHA_HASTA=@FECHA_HASTA,@MATERIAL=@MATERIAL,@ART=@ART,@EMP=@EMP,@GMN1=@GMN1,@COD_PROCE=@COD_PROCE,@ANYO=@ANYO,@ESTADO=@ESTADO,@FILAESTADO=@FILAESTADO,@USU=@USU,@LONGGMN1=@LONGGMN1,@LONGGMN2=@LONGGMN2,@LONGGMN3=@LONGGMN3,@LONGGMN4=@LONGGMN4,@CENTROCOSTE=@CENTROCOSTE,@PARTIDASPRES=@PARTIDASPRES" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--4. Pendientes de devoluci�n" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE #T_DEVOLUCION (ID INT,CODIGO NVARCHAR(50))" & vbCrLf
sConsulta = sConsulta & " SET @SQL = 'INSERT INTO #T_DEVOLUCION (ID,CODIGO) SELECT I.ID,C.CODIGO ' + @SQLFROM " & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL + ' INNER JOIN INSTANCIA_BLOQUE IB WITH (NOLOCK) ON I.ID=IB.INSTANCIA AND IB.EN_PROCESO=0 AND IB.ESTADO=1 AND IB.BLOQ<>1 AND IB.TRASLADADA=1'" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + @SWHERE + ' AND I.NUM IS NOT NULL AND I.TRASLADADA=1'" & vbCrLf
sConsulta = sConsulta & "   IF @SUSTITUTO =1" & vbCrLf
sConsulta = sConsulta & "      SET @SQL = @SQL + ' AND ((TRASLADO_A=@USU AND (SELECT ISNULL(U.FSWS_SUSTITUCION,'''') FROM USU U WITH (NOLOCK) WHERE U.PER=@USU AND U.BAJA=0) = '''') OR (TRASLADO_A IN (SELECT U.PER FROM USU U WITH (NOLOCK) WHERE U.FSWS_SUSTITUCION=@USU)))'" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "      SET @SQL = @SQL + ' AND (TRASLADO_A=@USU AND (SELECT ISNULL(U.FSWS_SUSTITUCION,'''') FROM USU U WITH (NOLOCK) WHERE U.PER=@USU AND U.BAJA=0) = '''')'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL,@PARAM,@CODIGO=@CODIGO,@TIPO=@TIPO,@FECHA_DESDE=@FECHA_DESDE,@FECHA_HASTA=@FECHA_HASTA,@MATERIAL=@MATERIAL,@ART=@ART,@EMP=@EMP,@GMN1=@GMN1,@COD_PROCE=@COD_PROCE,@ANYO=@ANYO,@ESTADO=@ESTADO,@FILAESTADO=@FILAESTADO,@USU=@USU,@LONGGMN1=@LONGGMN1,@LONGGMN2=@LONGGMN2,@LONGGMN3=@LONGGMN3,@LONGGMN4=@LONGGMN4,@CENTROCOSTE=@CENTROCOSTE,@PARTIDASPRES=@PARTIDASPRES" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--5. Trasladas" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE #T_TRASLADADAS (ID INT,CODIGO NVARCHAR(50))" & vbCrLf
sConsulta = sConsulta & "SET @SQL = 'INSERT INTO #T_TRASLADADAS (ID,CODIGO) SELECT I.ID,C.CODIGO ' + @SQLFROM " & vbCrLf
sConsulta = sConsulta & "    SET @SQL=@SQL + ' INNER JOIN INSTANCIA_BLOQUE IB WITH (NOLOCK) ON IB.INSTANCIA=I.ID AND IB.ESTADO=1 AND IB.TRASLADADA=1 AND IB.BLOQ=0'" & vbCrLf
sConsulta = sConsulta & "    SET @SQL = @SQL + @SWHERE + '  AND I.NUM IS NOT NULL AND I.TRASLADADA=1 AND ' " & vbCrLf
sConsulta = sConsulta & "    IF @SUSTITUTO =1" & vbCrLf
sConsulta = sConsulta & "      SET @SQL = @SQL + '((DEVOLUCION_A=@USU AND (SELECT ISNULL(U.FSWS_SUSTITUCION,'''') FROM USU U WITH (NOLOCK) WHERE U.PER=@USU AND U.BAJA=0) = '''') OR (DEVOLUCION_A IN ( SELECT U.PER FROM USU U WITH (NOLOCK) WHERE U.FSWS_SUSTITUCION=@USU)))'" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "     SET @SQL = @SQL + '(DEVOLUCION_A=@USU AND (SELECT ISNULL(U.FSWS_SUSTITUCION,'''') FROM USU U WITH (NOLOCK) WHERE U.PER=@USU AND U.BAJA=0) = '''')'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL,@PARAM,@CODIGO=@CODIGO,@TIPO=@TIPO,@FECHA_DESDE=@FECHA_DESDE,@FECHA_HASTA=@FECHA_HASTA,@MATERIAL=@MATERIAL,@ART=@ART,@EMP=@EMP,@GMN1=@GMN1,@COD_PROCE=@COD_PROCE,@ANYO=@ANYO,@ESTADO=@ESTADO,@FILAESTADO=@FILAESTADO,@USU=@USU,@LONGGMN1=@LONGGMN1,@LONGGMN2=@LONGGMN2,@LONGGMN3=@LONGGMN3,@LONGGMN4=@LONGGMN4,@CENTROCOSTE=@CENTROCOSTE,@PARTIDASPRES=@PARTIDASPRES" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--6. solicitudes de las que es observador" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE #T_OTRAS (ID INT,CODIGO NVARCHAR(50))" & vbCrLf
sConsulta = sConsulta & "SET @SQL = 'INSERT INTO #T_OTRAS (ID,CODIGO) SELECT I.ID,C.CODIGO ' + @SQLFROM " & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL +' INNER JOIN PM_ROL PR WITH (NOLOCK) ON PR.WORKFLOW=S.WORKFLOW'" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + @SWHERE + '  AND I.NUM IS NOT NULL AND PR.TIPO=5 AND (" & vbCrLf
sConsulta = sConsulta & "                                       PR.PER = @USU -- EL USUARIO ES OBSERVADOR" & vbCrLf
sConsulta = sConsulta & "                                       OR EXISTS (SELECT TOP 1 V.COD FROM VIEW_ROL_OBSERVADOR V WITH(NOLOCK) WHERE S.ID = V.SOLICITUD AND V.COD=@USU)" & vbCrLf
sConsulta = sConsulta & "                                       OR PR.UON0 = 1 --EL NIVEL UON0, O SEA, RA�Z (TODA LA ORGANIZACI�N) ES OBSERVADOR" & vbCrLf
sConsulta = sConsulta & "                                       )" & vbCrLf
sConsulta = sConsulta & "                                   AND I.ESTADO>0 AND I.EN_PROCESO<>2'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL,@PARAM,@CODIGO=@CODIGO,@TIPO=@TIPO,@FECHA_DESDE=@FECHA_DESDE,@FECHA_HASTA=@FECHA_HASTA,@MATERIAL=@MATERIAL,@ART=@ART,@EMP=@EMP,@GMN1=@GMN1,@COD_PROCE=@COD_PROCE,@ANYO=@ANYO,@ESTADO=@ESTADO,@FILAESTADO=@FILAESTADO,@USU=@USU,@LONGGMN1=@LONGGMN1,@LONGGMN2=@LONGGMN2,@LONGGMN3=@LONGGMN3,@LONGGMN4=@LONGGMN4,@CENTROCOSTE=@CENTROCOSTE,@PARTIDASPRES=@PARTIDASPRES" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL=''" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--1. Solicitudes abiertas por usted." & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' SELECT DISTINCT ID,CODIGO,0 as ICONO, 0 as OBV, 0 as ACCION_APROBAR, 0 as ACCION_RECHAZAR FROM #T_ABIERTAS WHERE 1=1'" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' AND ID NOT IN (SELECT ID FROM #T_PENDIENTES)' " & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' AND ID NOT IN (SELECT ID FROM #T_DEVOLUCION)'" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' AND ID NOT IN (SELECT ID FROM #T_TRASLADADAS)'" & vbCrLf
sConsulta = sConsulta & "SET @SQL= @SQL + ' UNION ALL '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--2. Procesos en los que particip�   " & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' SELECT DISTINCT ID,CODIGO,0 AS ICONO,0 AS OBV, 0 as ACCION_APROBAR, 0 as ACCION_RECHAZAR FROM #T_PARTICIPO WHERE 1=1'" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' AND ID NOT IN (SELECT ID FROM #T_ABIERTAS)'" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' AND ID NOT IN (SELECT ID FROM #T_PENDIENTES)'" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' AND ID NOT IN (SELECT ID FROM #T_DEVOLUCION)'" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' AND ID NOT IN (SELECT ID FROM #T_TRASLADADAS)' " & vbCrLf
sConsulta = sConsulta & "SET @SQL= @SQL + ' UNION ALL '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--3. Pendientes de tratar" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' SELECT DISTINCT ID,CODIGO,1 AS ICONO,0 AS OBV, ACCION_APROBAR, ACCION_RECHAZAR FROM #T_PENDIENTES'" & vbCrLf
sConsulta = sConsulta & "SET @SQL= @SQL + ' UNION ALL '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--4. Pendientes de devoluci�n" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' SELECT DISTINCT ID,CODIGO, 1 AS ICONO,0 AS OBV, 0 as ACCION_APROBAR, 0 as ACCION_RECHAZAR  FROM #T_DEVOLUCION'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL= @SQL + ' UNION ALL '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--5-Solicitudes trasladadas en espera de devoluci�n" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL +' SELECT DISTINCT ID, CODIGO,0 AS ICONO,0 AS OBV, 0 as ACCION_APROBAR, 0 as ACCION_RECHAZAR FROM #T_TRASLADADAS'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL= @SQL + ' UNION ALL '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--6-OTRAS SOLICITUDES (OBSERVADORES)" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL +' SELECT DISTINCT ID,CODIGO,0 AS ICONO,1 AS OBV, 0 as ACCION_APROBAR, 0 as ACCION_RECHAZAR FROM #T_OTRAS WHERE 1=1'" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' AND ID NOT IN (SELECT ID FROM #T_PENDIENTES)'" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' AND ID NOT IN (SELECT ID FROM #T_DEVOLUCION)'" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' AND ID NOT IN (SELECT ID FROM #T_TRASLADADAS)'" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' AND ID NOT IN (SELECT ID FROM #T_PARTICIPO)'   " & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' AND ID NOT IN (SELECT ID FROM #T_ABIERTAS)'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-----------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "--Solicitudes que el usuario puede iniciar flujo de modificaci�n" & vbCrLf
sConsulta = sConsulta & "-----------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "SET @SQL= @SQL + ' UNION ALL '" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' SELECT DISTINCT I.ID,C.CODIGO, 0 AS ICONO,0 AS OBV, 0 as ACCION_APROBAR, 0 as ACCION_RECHAZAR ' + @SQLFROM" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' INNER JOIN VIEW_ROL_PETICIONARIO_MODIF V WITH(NOLOCK) ON V.SOLICITUD=S.ID AND V.COD=@USU'" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + @SWHERE + ' AND I.NUM IS NOT NULL AND I.ESTADO>=100 AND I.EN_PROCESO<>2'" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' UNION ALL SELECT DISTINCT I.ID,C.CODIGO, 0 AS ICONO,0 AS OBV, 0 as ACCION_APROBAR, 0 as ACCION_RECHAZAR ' + @SQLFROM" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' INNER JOIN VIEW_ROL_PETICIONARIO_MODIF V WITH(NOLOCK) ON V.SOLICITUD=S.ID AND V.COD=@USU'" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + @SWHERE + ' AND I.NUM IS NOT NULL AND I.ESTADO=3 AND I.EN_PROCESO<>2'" & vbCrLf
sConsulta = sConsulta & "  " & vbCrLf
sConsulta = sConsulta & "-----------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "--Solicitudes que el usuario puede iniciar flujo de baja" & vbCrLf
sConsulta = sConsulta & "-----------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "SET @SQL= @SQL + ' UNION ALL '" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' SELECT DISTINCT I.ID,C.CODIGO, 0 AS ICONO,0 AS OBV, 0 as ACCION_APROBAR, 0 as ACCION_RECHAZAR ' + @SQLFROM" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' INNER JOIN VIEW_ROL_PETICIONARIO_BAJA V WITH(NOLOCK) ON V.SOLICITUD=S.ID AND V.COD=@USU'" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + @SWHERE + ' AND I.NUM IS NOT NULL AND I.EN_PROCESO<>2 AND I.ESTADO>=100'" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' UNION ALL SELECT DISTINCT I.ID,C.CODIGO, 0 AS ICONO,0 AS OBV, 0 as ACCION_APROBAR, 0 as ACCION_RECHAZAR ' + @SQLFROM" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' INNER JOIN VIEW_ROL_PETICIONARIO_BAJA V WITH(NOLOCK) ON V.SOLICITUD=S.ID AND V.COD=@USU'" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + @SWHERE + ' AND I.NUM IS NOT NULL AND I.EN_PROCESO<>2 AND I.ESTADO=4'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL='INSERT INTO #G1 (ID,CODIGO, ICONO,OBV,ACCION_APROBAR,ACCION_RECHAZAR)' +  @SQL" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL,@PARAM,@CODIGO=@CODIGO,@TIPO=@TIPO,@FECHA_DESDE=@FECHA_DESDE,@FECHA_HASTA=@FECHA_HASTA,@MATERIAL=@MATERIAL,@ART=@ART,@EMP=@EMP,@GMN1=@GMN1,@COD_PROCE=@COD_PROCE,@ANYO=@ANYO,@ESTADO=@ESTADO,@FILAESTADO=@FILAESTADO,@USU=@USU,@LONGGMN1=@LONGGMN1,@LONGGMN2=@LONGGMN2,@LONGGMN3=@LONGGMN3,@LONGGMN4=@LONGGMN4,@CENTROCOSTE=@CENTROCOSTE,@PARTIDASPRES=@PARTIDASPRES" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--*************************************************SELECT GEN�RICA PARA SACAR LOS DATOS ***********************************************************" & vbCrLf
sConsulta = sConsulta & "--Creamos una tabla temporal" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE #G2(ID INT, CODIGO NVARCHAR(50), FEC_ALTA DATETIME, CODESTADO INT, FECHA_INICIO DATETIME,FECHA_EXP DATETIME, CODPRO NVARCHAR(40), PROVEEDOR NVARCHAR(250),CODEMP NVARCHAR(40), EMPRESA NVARCHAR(150),ESTADO NVARCHAR(50),CODPET NVARCHAR(40),PETICIONARIO NVARCHAR(250),TIPO_CONTRATO NVARCHAR(250),ULT_VERSION INT, INSTANCIA_ESTADO INT,ID_CONTRATO INT,ICONO TINYINT, OBSERVADOR TINYINT, TRASLADADA TINYINT,MON NVARCHAR(5),CONTACTO NVARCHAR(100),IMPORTE FLOAT,PROCE NVARCHAR(100),EN_PROCESO TINYINT, ACCION_APROBAR INT, ACCION_RECHAZAR INT, BLOQUE INT)" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE #G3(ID INT, CODIGO NVARCHAR(50), FEC_ALTA DATETIME, CODESTADO INT, FECHA_INICIO DATETIME,FECHA_EXP DATETIME, CODPRO NVARCHAR(40), PROVEEDOR NVARCHAR(250),CODEMP NVARCHAR(40), EMPRESA NVARCHAR(150),ESTADO NVARCHAR(50),CODPET NVARCHAR(40),PETICIONARIO NVARCHAR(250),TIPO_CONTRATO NVARCHAR(250),ULT_VERSION INT, INSTANCIA_ESTADO INT,ID_CONTRATO INT,ICONO TINYINT, OBSERVADOR TINYINT, TRASLADADA TINYINT,MON NVARCHAR(5),CONTACTO NVARCHAR(100),IMPORTE FLOAT,PROCE NVARCHAR(100),EN_PROCESO TINYINT, ACCION_APROBAR INT, ACCION_RECHAZAR INT, BLOQUE INT)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL=''" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'SELECT I.ID AS ID,C.CODIGO,I.FEC_ALTA AS FECHA_ALTA,C.ESTADO CODESTADO,C.FEC_INI FECHA_INICIO,C.FEC_FIN FECHA_EXP,PRO.COD CODPRO,PRO.DEN PROVEEDOR,EMP.NIF CODEMP,  EMP.DEN EMPRESA,NULL AS ESTADO,PER.COD CODPET ,PER.NOM + '' '' + PER.APE AS PETICIONARIO, S.DEN_'+ @IDI + ' TIPO_CONTRATO, I.ULT_VERSION,I.ESTADO INSTANCIA_ESTADO,C.ID ID_CONTRATO,G1.ICONO,G1.OBV,I.TRASLADADA,C.MON,CON.NOM + '' '' + CON.APE AS CONTACTO,I.IMPORTE,CI.PROCESO,I.EN_PROCESO,G1.ACCION_APROBAR,G1.ACCION_RECHAZAR,M.BLOQUE '" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL +  ' FROM  INSTANCIA I WITH (NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL +  ' INNER JOIN #G1 G1 WITH(NOLOCK) ON I.ID=G1.ID'" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL +  ' INNER JOIN CONTRATO C WITH(NOLOCK) ON C.INSTANCIA=G1.ID'" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL +  ' LEFT JOIN (SELECT DISTINCT CONTRATO,ANYO,GMN1,PROCE,convert(NVARCHAR(50),ANYO) + ''/'' + GMN1 + ''/'' + convert(NVARCHAR(50),PROCE) AS PROCESO FROM CONTRATO_ITEM CI WITH(NOLOCK) ) CI ON CI.CONTRATO=C.ID'" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL +  ' INNER JOIN SOLICITUD S WITH (NOLOCK) ON I.SOLICITUD=S.ID'   --para sacar la solicitud" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL +  ' INNER JOIN TIPO_SOLICITUDES TS WITH (NOLOCK) ON S.TIPO=TS.ID'   --para sacar el tipo de solicitud" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL +  ' LEFT JOIN PROVE PRO WITH (NOLOCK) ON PRO.COD = C.PROVE'" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL +  ' LEFT JOIN CON CON WITH (NOLOCK) ON CON.PROVE = C.PROVE AND C.CONTACTO=CON.ID'" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL +  ' INNER JOIN PER WITH (NOLOCK) ON PER.COD = I.PETICIONARIO'" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL +  ' LEFT JOIN EMP WITH (NOLOCK) ON C.EMP=EMP.ID'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Mira si la persona es un rol de la instancia, y en ese caso si tiene permisos para ver el detalle del flujo y el detalle de los participantes:" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' LEFT JOIN(SELECT INSTANCIA I,PER P,MAX(VER_DETALLE_PER)VDP,MAX(VER_FLUJO)VER_FLUJO FROM PM_COPIA_ROL WITH(NOLOCK) GROUP BY INSTANCIA,PER) CR ON I.ID = CR.I AND @USU=CR.P'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Para obtener si hay etapas en paralelo" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ' INNER JOIN (SELECT INSTANCIA I, COUNT(*) P,MIN(BLOQUE) BLOQUE FROM INSTANCIA_BLOQUE  WITH (NOLOCK)  WHERE ESTADO=1 AND BLOQ<>1 GROUP BY INSTANCIA) M ON I.ID=M.I'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @NOM_TABLA ='' --AND (@ESTADO='8' OR @ESTADO='') " & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       --para las solicitudes que solo tienen registro en la tabla INSTANCIA y todav�a no las ha procesado el WebService" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + ' UNION ALL '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + ' SELECT I.ID AS ID,C.CODIGO,I.FEC_ALTA AS FECHA_ALTA,C.ESTADO CODESTADO,C.FEC_INI FECHA_INICIO,C.FEC_FIN FECHA_EXP,PRO.COD CODPRO,PRO.DEN PROVEEDOR,EMP.NIF CODEMP,  EMP.DEN EMPRESA,NULL AS ESTADO,PER.COD CODPET, PER.NOM + PER.APE AS PETICIONARIO, S.DEN_'+ @IDI + ' TIPO_CONTRATO, I.ULT_VERSION,I.ESTADO INSTANCIA_ESTADO,C.ID ID_CONTRATO,0 AS ICONO,0 AS OBV, 0 AS TRASLADADA,C.MON,CON.NOM + '' '' + CON.APE AS CONTACTO,I.IMPORTE,CI.PROCESO,I.EN_PROCESO,0 as ACCION_APROBAR,0 as ACCION_RECHAZAR,0 as BLOQUE '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + ' FROM CONTRATO C WITH(NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + ' LEFT JOIN (SELECT DISTINCT CONTRATO,ANYO,GMN1,PROCE,convert(NVARCHAR(50),ANYO) + ''/'' + GMN1 + ''/'' + convert(NVARCHAR(50),PROCE) AS PROCESO FROM CONTRATO_ITEM CI WITH(NOLOCK) ) CI ON CI.CONTRATO=C.ID'" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + ' INNER JOIN INSTANCIA I WITH(NOLOCK) ON I.ID=C.INSTANCIA '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + ' INNER JOIN PER P WITH(NOLOCK) ON P.COD = I.PETICIONARIO '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + ' INNER JOIN DEP D WITH (NOLOCK) ON P.DEP=D.COD '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + ' INNER JOIN SOLICITUD S WITH(NOLOCK) ON I.SOLICITUD=S.ID INNER JOIN TIPO_SOLICITUDES TS WITH (NOLOCK) ON S.TIPO=TS.ID'" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + ' LEFT JOIN PROVE PRO WITH (NOLOCK) ON PRO.COD = C.PROVE'" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=  @SQL + ' LEFT JOIN CON CON WITH (NOLOCK) ON CON.PROVE = C.PROVE AND C.CONTACTO=CON.ID'" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + ' INNER JOIN PER WITH (NOLOCK) ON PER.COD = I.PETICIONARIO'" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + ' LEFT JOIN EMP WITH (NOLOCK) ON C.EMP=EMP.ID'" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + ' LEFT JOIN INSTANCIA_VISORES_PM C1 WITH(NOLOCK) ON I.ID=C1.INSTANCIA'" & vbCrLf
sConsulta = sConsulta & "   IF @PALABRA_CLAVE IS NOT NULL AND @PALABRA_CLAVE <> '' " & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + ' INNER JOIN PM_COPIA_ADJUN PCA WITH(NOLOCK) ON C.ID = PCA.ID_CONTRATO'" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + @SWHERE + ' AND PETICIONARIO = @USU AND I.ULT_VERSION IS NULL AND I.NUM IS NULL AND EN_PROCESO=1 AND TS.TIPO =5'" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL2= N' INSERT INTO #G2 ' + @SQL" & vbCrLf
sConsulta = sConsulta & "SET @PARAM =N'@IDI NVARCHAR(20),@USU NVARCHAR (50),@CODIGO NVARCHAR(50)='''',@TIPO NVARCHAR(50),@FECHA_DESDE DATETIME=NULL,@FECHA_HASTA DATETIME=NULL,@MATERIAL NVARCHAR (50)='''',@ART NVARCHAR (50)='''',@EMP INT=0,@GMN1 NVARCHAR (50)='''',@COD_PROCE INT=0,@ANYO INT=0,@ESTADO NVARCHAR (50)='''',@FILAESTADO NVARCHAR (50)='''''" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL2" & vbCrLf
sConsulta = sConsulta & ", @PARAM" & vbCrLf
sConsulta = sConsulta & ",@CODIGO=@CODIGO,@TIPO=@TIPO,@FECHA_DESDE=@FECHA_DESDE,@FECHA_HASTA=@FECHA_HASTA,@MATERIAL=@MATERIAL,@ART=@ART,@EMP=@EMP,@GMN1=@GMN1,@COD_PROCE=@COD_PROCE,@ANYO=@ANYO,@ESTADO=@ESTADO,@FILAESTADO=@FILAESTADO,@USU=@USU,@IDI=@IDI  " & vbCrLf
sConsulta = sConsulta & "    " & vbCrLf
sConsulta = sConsulta & "-- *****LA SELECT FINAL ********" & vbCrLf
sConsulta = sConsulta & "set @SQL='SELECT DISTINCT #G2.ID,#G2.CODIGO, #G2.FEC_ALTA, #G2.CODESTADO, #G2.FECHA_INICIO, #G2.FECHA_EXP, #G2.CODPRO, #G2.PROVEEDOR, #G2.CODEMP, #G2.EMPRESA, #G2.ESTADO, #G2.CODPET, #G2.PETICIONARIO, #G2.TIPO_CONTRATO, #G2.ULT_VERSION, #G2.INSTANCIA_ESTADO, #G2.ID_CONTRATO, #G2.ICONO, #G2.OBSERVADOR, #G2.TRASLADADA, #G2.MON, #G2.CONTACTO, #G2.IMPORTE, #G2.PROCE, #G2.EN_PROCESO, #G2.ACCION_APROBAR, #G2.ACCION_RECHAZAR, #G2.BLOQUE '" & vbCrLf
sConsulta = sConsulta & "--CALIDAD: Cada tabla de filtro tiene unos campos diferentes. Hay que hacer la select con *." & vbCrLf
sConsulta = sConsulta & "IF @NOM_TABLA <>''" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + ', FILTRO.* '" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + ' FROM #G2'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ' LEFT JOIN INSTANCIA_ADICIONAL IA WITH(NOLOCK) ON #G2.ID=IA.INSTANCIA AND IA.PER=@USU '" & vbCrLf
sConsulta = sConsulta & "IF @NOM_TABLA<>''" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + ' INNER JOIN ' + @IDI + '_' + @NOM_TABLA + ' FILTRO WITH(NOLOCK) ON #G2.ID = FILTRO.INSTANCIA '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL_CONT=@SQL" & vbCrLf
sConsulta = sConsulta & "IF @NOM_TABLA <>''" & vbCrLf
sConsulta = sConsulta & "   SET @SQL_CONT = REPLACE(@SQL_CONT,', FILTRO.* ','')" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "IF @ESTADO<>'' AND @ESTADO<>'8' " & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       SET @ESTADO = REPLACE(@ESTADO,',',''',''')" & vbCrLf
sConsulta = sConsulta & "       SET @ESTADO = '''' + @ESTADO + ''''" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = @SQL + ' WHERE #G2.CODESTADO IN (' + @ESTADO + ')'" & vbCrLf
sConsulta = sConsulta & "   END " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ORDENACION IS NOT NULL AND @ORDENACION <> ''" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   IF SUBSTRING(@ORDENACION,1,2)='C_' --Es un campo del formulario" & vbCrLf
sConsulta = sConsulta & "       SET @SQL = @SQL + ' ORDER BY FILTRO.' + @ORDENACION " & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "       --SET @SQL = @SQL + ' ORDER BY #G2.' + @ORDENACION " & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "       DECLARE @ROW INT = ((@PAGENUMBER-1) * @PAGESIZE) +1" & vbCrLf
sConsulta = sConsulta & "       IF @OBTENERTODOSCONTRATOS=0" & vbCrLf
sConsulta = sConsulta & "           BEGIN" & vbCrLf
sConsulta = sConsulta & "               --CALIDAD. Estos dos * se quedan pq no es la tabla de bbdd certificados sino una tabla hecha ad hoc mediante WITH CERTIFICADOS AS (...) y en estos puntos suspensivos puede venir filtro.* o no -> no se conocen las columnas" & vbCrLf
sConsulta = sConsulta & "               SET @SQL = ';WITH CONTRATOS AS (' + @SQL + '), TOTCONTRATOS AS (SELECT *,ROW_NUMBER() OVER (ORDER BY ' + @ORDENACION + ') AS ROWNUMBER FROM CONTRATOS WITH (NOLOCK)) SELECT *,ROWNUMBER FROM TOTCONTRATOS WITH (NOLOCK) WHERE ROWNUMBER BETWEEN ' + CONVERT(VARCHAR(10),@ROW) + ' AND ' + CONVERT(VARCHAR(10),(@ROW + @PAGESIZE - 1))" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "END " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL ,N'@USU NVARCHAR(50)',@USU=@USU" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @FILAESTADO <>''" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @SQL2= N'INSERT INTO #G3 ' + @SQL_CONT + ' WHERE #G2.CODESTADO IN (' + @FILAESTADO + ')'" & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL2,N'@USU NVARCHAR(50)',@USU=@USU" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @SQL2= N'INSERT INTO #G3 '  + @SQL_CONT" & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL2,N'@USU NVARCHAR(50)',@USU=@USU" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--***********************************************************************************************************************************" & vbCrLf
sConsulta = sConsulta & "--CONTADORES" & vbCrLf
sConsulta = sConsulta & "DECLARE @CONT0 INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @CONT1 INT --Borrador" & vbCrLf
sConsulta = sConsulta & "DECLARE @CONT2 INT --En Curso de Aprobaci�n" & vbCrLf
sConsulta = sConsulta & "DECLARE @CONT3 INT --Vigentes" & vbCrLf
sConsulta = sConsulta & "DECLARE @CONT4 INT --Expirados" & vbCrLf
sConsulta = sConsulta & "DECLARE @CONT5 INT --Rechazados" & vbCrLf
sConsulta = sConsulta & "DECLARE @CONT6 INT --Anulados" & vbCrLf
sConsulta = sConsulta & "DECLARE @CONT7 INT --Todas" & vbCrLf
sConsulta = sConsulta & "DECLARE @CONT8 INT --Proximas a expirar" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   --Borrador" & vbCrLf
sConsulta = sConsulta & "   SET @PARAM =N'@CONT0 INT = 0 OUTPUT '" & vbCrLf
sConsulta = sConsulta & "   SET @CONT1=0" & vbCrLf
sConsulta = sConsulta & "   IF CHARINDEX('0',@FILAESTADO,0)<>0 OR @FILAESTADO=''" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "      SET @SQL='SET @CONT0=(SELECT COUNT(DISTINCT C.ID) FROM #G3 C WITH (NOLOCK) WHERE CODESTADO=0)'" & vbCrLf
sConsulta = sConsulta & "      EXEC SP_EXECUTESQL @SQL" & vbCrLf
sConsulta = sConsulta & "      , @PARAM" & vbCrLf
sConsulta = sConsulta & "      ,@CONT0=@CONT1 OUTPUT  " & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "  " & vbCrLf
sConsulta = sConsulta & "   --En curso de aprobacion" & vbCrLf
sConsulta = sConsulta & "   SET @CONT2=0" & vbCrLf
sConsulta = sConsulta & "   IF CHARINDEX('2',@FILAESTADO,0)<>0 OR @FILAESTADO=''" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "      SET @SQL='SET @CONT0=(SELECT COUNT(DISTINCT C.ID) FROM #G3 C WITH (NOLOCK) WHERE CODESTADO=2)'" & vbCrLf
sConsulta = sConsulta & "      " & vbCrLf
sConsulta = sConsulta & "      EXEC SP_EXECUTESQL @SQL" & vbCrLf
sConsulta = sConsulta & "      , @PARAM" & vbCrLf
sConsulta = sConsulta & "      ,@CONT0=@CONT2 OUTPUT  " & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   --Proximos a expirar" & vbCrLf
sConsulta = sConsulta & "   SET @CONT8=0" & vbCrLf
sConsulta = sConsulta & "   IF CHARINDEX('4',@FILAESTADO,0)<>0 OR @FILAESTADO=''" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "     SET @SQL='SET @CONT0=(SELECT COUNT(DISTINCT C.ID) FROM #G3 C WITH (NOLOCK) WHERE CODESTADO=4)'" & vbCrLf
sConsulta = sConsulta & "     " & vbCrLf
sConsulta = sConsulta & "     EXEC SP_EXECUTESQL @SQL" & vbCrLf
sConsulta = sConsulta & "      , @PARAM" & vbCrLf
sConsulta = sConsulta & "      ,@CONT0=@CONT8 OUTPUT  " & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   --Vigentes" & vbCrLf
sConsulta = sConsulta & "   SET @CONT3=0" & vbCrLf
sConsulta = sConsulta & "   IF CHARINDEX('3',@FILAESTADO,0)<>0 OR @FILAESTADO=''" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "      SET @SQL='SET @CONT0=(SELECT COUNT(DISTINCT C.ID) FROM #G3 C WITH (NOLOCK) WHERE CODESTADO=3)'" & vbCrLf
sConsulta = sConsulta & "      " & vbCrLf
sConsulta = sConsulta & "      EXEC SP_EXECUTESQL @SQL" & vbCrLf
sConsulta = sConsulta & "      , @PARAM" & vbCrLf
sConsulta = sConsulta & "      ,@CONT0=@CONT3 OUTPUT  " & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   --Expirados" & vbCrLf
sConsulta = sConsulta & "   SET @CONT4=0" & vbCrLf
sConsulta = sConsulta & "   IF CHARINDEX('5',@FILAESTADO,0)<>0 OR @FILAESTADO=''" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "      SET @SQL='SET @CONT0=(SELECT COUNT(DISTINCT C.ID) FROM #G3 C WITH (NOLOCK) WHERE CODESTADO=5)'" & vbCrLf
sConsulta = sConsulta & "      " & vbCrLf
sConsulta = sConsulta & "      EXEC SP_EXECUTESQL @SQL" & vbCrLf
sConsulta = sConsulta & "      , @PARAM" & vbCrLf
sConsulta = sConsulta & "      ,@CONT0=@CONT4 OUTPUT  " & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   --Rechazados" & vbCrLf
sConsulta = sConsulta & "   SET @CONT5=0" & vbCrLf
sConsulta = sConsulta & "   IF CHARINDEX('6',@FILAESTADO,0)<>0 OR @FILAESTADO=''" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "      SET @SQL='SET @CONT0=(SELECT COUNT(DISTINCT C.ID) FROM #G3 C WITH (NOLOCK) WHERE CODESTADO=6)'" & vbCrLf
sConsulta = sConsulta & "      " & vbCrLf
sConsulta = sConsulta & "      EXEC SP_EXECUTESQL @SQL" & vbCrLf
sConsulta = sConsulta & "      , @PARAM" & vbCrLf
sConsulta = sConsulta & "      ,@CONT0=@CONT5 OUTPUT  " & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   --Anulados" & vbCrLf
sConsulta = sConsulta & "   SET @CONT6=0" & vbCrLf
sConsulta = sConsulta & "   IF CHARINDEX('7',@FILAESTADO,0)<>0 OR @FILAESTADO=''" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "      SET @SQL='SET @CONT0=(SELECT COUNT(DISTINCT C.ID) FROM #G3 C WITH (NOLOCK) WHERE CODESTADO=7)'" & vbCrLf
sConsulta = sConsulta & "      " & vbCrLf
sConsulta = sConsulta & "      EXEC SP_EXECUTESQL @SQL" & vbCrLf
sConsulta = sConsulta & "      , @PARAM" & vbCrLf
sConsulta = sConsulta & "      ,@CONT0=@CONT6 OUTPUT  " & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   --TODAS:   " & vbCrLf
sConsulta = sConsulta & "   SET @CONT7 =0" & vbCrLf
sConsulta = sConsulta & "   SET @CONT7 = @CONT1+@CONT2+@CONT3+@CONT4+@CONT5+@CONT6+@CONT8" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Muestra los contadores" & vbCrLf
sConsulta = sConsulta & "SELECT  @CONT1 GUARDADAS, @CONT2 EN_CURSO_APROBACION,@CONT3 VIGENTES,@CONT4 EXPIRADOS,@CONT5 RECHAZADOS,@CONT6 ANULADOS,@CONT7 TODAS, @CONT8 PROXIMO_A_EXPIRAR" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Contadores de Estados sin filtros (para mostrar las ALERTAS)" & vbCrLf
sConsulta = sConsulta & " SELECT COUNT(DISTINCT ID) PDTE_DE_TRATAR,@CONT8 PROXIMO_A_EXPIRAR FROM #G2 WITH (NOLOCK) WHERE ICONO = 1      " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DROP TABLE #T_ABIERTAS" & vbCrLf
sConsulta = sConsulta & "DROP TABLE #T_PARTICIPO" & vbCrLf
sConsulta = sConsulta & "DROP TABLE #T_PENDIENTES" & vbCrLf
sConsulta = sConsulta & "DROP TABLE #T_DEVOLUCION" & vbCrLf
sConsulta = sConsulta & "DROP TABLE #T_TRASLADADAS" & vbCrLf
sConsulta = sConsulta & "DROP TABLE #T_OTRAS" & vbCrLf
sConsulta = sConsulta & "DROP TABLE #G1" & vbCrLf
sConsulta = sConsulta & "DROP TABLE #G2" & vbCrLf
sConsulta = sConsulta & "DROP TABLE #G3" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_PETICIONARIO_FLUJO_LCX]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_PETICIONARIO_FLUJO_LCX]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE procedure [dbo].[FSPM_PETICIONARIO_FLUJO_LCX] @CONTRATO INT, @EMPRESA INT, @PERFIL VARCHAR(20), @USU NVARCHAR(20)" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE @CENTRO_COPIA_CAMPO VARCHAR(20)" & vbCrLf
sConsulta = sConsulta & "DECLARE @INSTANCIA INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF UPPER(@PERFIL)='ADMINISTRADOR'" & vbCrLf
sConsulta = sConsulta & "   SELECT COUNT(EMP) FROM CONTRATO WITH (NOLOCK)WHERE ID=@CONTRATO AND EMP=@EMPRESA" & vbCrLf
sConsulta = sConsulta & "ELSE IF UPPER(@PERFIL)='GESTOR'" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "   SELECT COUNT(1) FROM CONTRATO C WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN INSTANCIA I WITH (NOLOCK) ON C.INSTANCIA = I.ID" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN COPIA_CAMPO CC WITH (NOLOCK) ON CC.INSTANCIA = I.ID AND CC.NUM_VERSION = I.ULT_VERSION" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN COPIA_CAMPO_DEF CD WITH(NOLOCK) ON CC.COPIA_CAMPO_DEF=CD.ID AND CD.TIPO_CAMPO_GS=129" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN USU_CC_IMPUTACION UCCI WITH (NOLOCK) ON COALESCE(UCCI.UON1,'')+COALESCE('#'+UCCI.UON2,'')+COALESCE('#'+UCCI.UON3,'')+COALESCE('#'+UCCI.UON4,'') = CC.VALOR_TEXT AND UCCI.USU = @USU" & vbCrLf
sConsulta = sConsulta & "   WHERE C.ID=@CONTRATO AND C.EMP=@EMPRESA" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   SELECT 0" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_PERMISOS_IMPUTACION_LCX]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_PERMISOS_IMPUTACION_LCX]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE procedure [dbo].[FSPM_PERMISOS_IMPUTACION_LCX]" & vbCrLf
sConsulta = sConsulta & "   @CODUSUARIO VARCHAR(20),@CODEMPRESA VARCHAR(20),@CENTROCOSTE VARCHAR(20)," & vbCrLf
sConsulta = sConsulta & "   @OUTPUT AS VARCHAR(200) OUTPUT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   SET NOCOUNT ON;" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   DECLARE @UON1 AS VARCHAR(20) = NULL" & vbCrLf
sConsulta = sConsulta & "   DECLARE @UON2 AS VARCHAR(20) = NULL" & vbCrLf
sConsulta = sConsulta & "   DECLARE @UON3 AS VARCHAR(20) = NULL" & vbCrLf
sConsulta = sConsulta & "   DECLARE @UON4 AS VARCHAR(20) = NULL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @UON1 = @CODEMPRESA --PARA NO LIARNOS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--BORRAMOS LOS PERMISOS ANTERIORES DEL USUARIO       " & vbCrLf
sConsulta = sConsulta & "DELETE FROM USU_CC_IMPUTACION WHERE USU=@CODUSUARIO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--GRABAMOS LOS NUEVOS PERMISOS PARA EL CONJUNTO DE PAR�METROS SUMINISTRADO" & vbCrLf
sConsulta = sConsulta & "IF @CENTROCOSTE IS NULL --ES UN ADMINISTRADOR BORRAMOS TODAS LAS IMPUTACIONES E INSERTAMOS TODAS LAS IMPUTACIONES PARA LOS CENTROS DE COSTE DE ESA EMPRESA       " & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO USU_CC_IMPUTACION (USU,UON1,UON2,UON3,UON4) " & vbCrLf
sConsulta = sConsulta & "     SELECT DISTINCT @CODUSUARIO,UON1,UON2,UON3,UON4 " & vbCrLf
sConsulta = sConsulta & "     FROM CENTRO_SM_UON WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "     WHERE UON1=@UON1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @OUTPUT = ''" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "ELSE -- ES UN GESTOR O CONSULTOR INSERTAMOS SOLO PARA ESE CENTRO DE COSTE Y PARA LOS QUE CUELGUEN DE �L" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "   --DECLARAMOS UN CURSOR PARA SACAR TODOS LOS CENTROS DE COSTE DE LA EMPRESA " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   ----DADO QUE LOS C�DIGOS DE UON2 TB SE USAN EN UON3, PARA PREPARAR EL STORED DEFINITIVO HAY QUE SABER A QU� NIVEL EMPEZAR�N" & vbCrLf
sConsulta = sConsulta & "   ----LOS CENTROS POR LOS QUE SE BUSCAR� Y TB QUE POR DEBAJO DE ESE NIVEL NO SE PUEDEN REPETIR C�DIGOS." & vbCrLf
sConsulta = sConsulta & "   ----DE MOMENTO LOS CENTROS SER�N SIEMPRE DE NIVEL UON3 Y LOS CENTROS DEPENDIENTES SER�N DE NIVEL UON4." & vbCrLf
sConsulta = sConsulta & "   ----SI LOS CENTROS FUESEN DE NIVEL 2 O BIEN SE QUISIESE FILTRAR POR �REAS (LO QUE ACTUALMENTE ES EL NIVEL UON2), " & vbCrLf
sConsulta = sConsulta & "   ----HAY QUE DESCOMENTAR LO COMENTADO PERO ASUMIENDO QUE SI EL C�DIGO @CENTROCOSTE EST� REPETIDO EN UON2 Y TB EN UON3 (ACTUALMENTE HAY REPETIDOS)" & vbCrLf
sConsulta = sConsulta & "   ----SE COGE SIEMPRE EL DE UON2 PORQUE NO SE ESPECIFICA EL NIVEL DESEADO" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   --IF EXISTS(SELECT UON1,UON2, UON3, UON4 FROM CENTRO_SM_UON WITH(NOLOCK) WHERE UON1 = @UON1 AND UON2 = @CENTROCOSTE)" & vbCrLf
sConsulta = sConsulta & "   --  BEGIN" & vbCrLf
sConsulta = sConsulta & "   --  DECLARE CCENTRODECOSTE CURSOR FOR" & vbCrLf
sConsulta = sConsulta & "   --  SELECT DISTINCT UON1,UON2, UON3, UON4 FROM CENTRO_SM_UON WITH(NOLOCK) WHERE UON1 = @UON1 AND UON2 = @CENTROCOSTE" & vbCrLf
sConsulta = sConsulta & "   --  SET @OUTPUT = @UON1+'#'+ISNULL(@CENTROCOSTE,'NULL')" & vbCrLf
sConsulta = sConsulta & "   --  END" & vbCrLf
sConsulta = sConsulta & "   --ELSE" & vbCrLf
sConsulta = sConsulta & "       IF EXISTS(SELECT UON1,UON2, UON3, UON4 FROM CENTRO_SM_UON WITH(NOLOCK) WHERE UON1 = @UON1 AND UON3 = @CENTROCOSTE)" & vbCrLf
sConsulta = sConsulta & "           BEGIN" & vbCrLf
sConsulta = sConsulta & "           DECLARE CCENTRODECOSTE CURSOR FOR" & vbCrLf
sConsulta = sConsulta & "           SELECT DISTINCT UON1, UON2, UON3, UON4 FROM CENTRO_SM_UON WITH(NOLOCK) WHERE UON1 = @UON1 AND UON3 = @CENTROCOSTE" & vbCrLf
sConsulta = sConsulta & "           SELECT TOP 1 @UON2 = UON2 FROM CENTRO_SM_UON WITH(NOLOCK) WHERE UON1 = @UON1 AND UON3 = @CENTROCOSTE" & vbCrLf
sConsulta = sConsulta & "           SET @OUTPUT = @UON1+'#'+ISNULL(@UON2,'NULL')+'#'+ ISNULL(@CENTROCOSTE,'NULL')" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "       ELSE" & vbCrLf
sConsulta = sConsulta & "           IF EXISTS(SELECT UON1,UON2, UON3, UON4 FROM CENTRO_SM_UON WITH(NOLOCK) WHERE UON1 = @UON1 AND UON4 = @CENTROCOSTE)" & vbCrLf
sConsulta = sConsulta & "               BEGIN" & vbCrLf
sConsulta = sConsulta & "               DECLARE CCENTRODECOSTE CURSOR FOR" & vbCrLf
sConsulta = sConsulta & "               SELECT DISTINCT UON1,UON2, UON3, UON4 FROM CENTRO_SM_UON WITH(NOLOCK) WHERE UON1 = @UON1 AND UON4 = @CENTROCOSTE" & vbCrLf
sConsulta = sConsulta & "               SELECT TOP 1 @UON2 = UON2, @UON3 = UON3 FROM CENTRO_SM_UON WITH(NOLOCK) WHERE UON1 = @UON1 AND UON4 = @CENTROCOSTE" & vbCrLf
sConsulta = sConsulta & "               SET @OUTPUT = @UON1+'#'+ISNULL(@UON2,'NULL')+'#'+ISNULL(@UON3,'NULL')+'#'+ ISNULL(@CENTROCOSTE,'NULL')" & vbCrLf
sConsulta = sConsulta & "               END" & vbCrLf
sConsulta = sConsulta & "           ELSE --NO HAY DATOS" & vbCrLf
sConsulta = sConsulta & "               BEGIN" & vbCrLf
sConsulta = sConsulta & "               DECLARE CCENTRODECOSTE CURSOR FOR" & vbCrLf
sConsulta = sConsulta & "               SELECT DISTINCT UON1,UON2, UON3, UON4 FROM CENTRO_SM_UON WITH(NOLOCK) WHERE 1=2" & vbCrLf
sConsulta = sConsulta & "               SET @OUTPUT = ''" & vbCrLf
sConsulta = sConsulta & "               END" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "   -- APERTURA DEL CURSOR" & vbCrLf
sConsulta = sConsulta & "   OPEN CCENTRODECOSTE" & vbCrLf
sConsulta = sConsulta & "   FETCH CCENTRODECOSTE INTO @UON1,@UON2,@UON3,@UON4   " & vbCrLf
sConsulta = sConsulta & "  " & vbCrLf
sConsulta = sConsulta & "   WHILE (@@FETCH_STATUS = 0 )" & vbCrLf
sConsulta = sConsulta & "       BEGIN           " & vbCrLf
sConsulta = sConsulta & "       INSERT INTO USU_CC_IMPUTACION (USU,UON1,UON2,UON3,UON4) VALUES(@CODUSUARIO,@UON1,@UON2,@UON3,@UON4)" & vbCrLf
sConsulta = sConsulta & "        -- LECTURA DE LA SIGUIENTE FILA DEL CURSOR" & vbCrLf
sConsulta = sConsulta & "        FETCH CCENTRODECOSTE INTO @UON1,@UON2,@UON3,@UON4" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "   CLOSE CCENTRODECOSTE" & vbCrLf
sConsulta = sConsulta & "   DEALLOCATE CCENTRODECOSTE" & vbCrLf
sConsulta = sConsulta & "    END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta



sConsulta = "IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_COMPROBAR_DESCARGA_LCX]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_COMPROBAR_DESCARGA_LCX]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta
sConsulta = "CREATE procedure [dbo].[FSPM_COMPROBAR_DESCARGA_LCX] @INSTANCIA INT ,@USUARIO VARCHAR(20),@EMPRESA INT ,@PERFIL VARCHAR(20)AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDEMPRESA INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @COUNT INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @NIVEL INTEGER" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "---ADMINISTRADOR" & vbCrLf
sConsulta = sConsulta & "IF LOWER(@PERFIL)='administrador'" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "   SELECT @IDEMPRESA=EMP FROM CONTRATO WITH(NOLOCK) WHERE INSTANCIA =@INSTANCIA --- veo si el contrato es de la empresa login" & vbCrLf
sConsulta = sConsulta & "   IF @EMPRESA=@IDEMPRESA" & vbCrLf
sConsulta = sConsulta & "   SELECT 1" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "   SELECT 0" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "---GESTOR  Y CONSULTAS" & vbCrLf
sConsulta = sConsulta & "IF LOWER(@PERFIL)='gestor' OR LOWER(@PERFIL)='consulta'" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "   SET @COUNT=0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SELECT @COUNT=COUNT(1) " & vbCrLf
sConsulta = sConsulta & "   FROM COPIA_CAMPO CC WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN INSTANCIA I WITH (NOLOCK) ON CC.INSTANCIA = I.ID AND CC.NUM_VERSION=I.ULT_VERSION" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN COPIA_CAMPO_DEF CD WITH(NOLOCK) ON CC.COPIA_CAMPO_DEF=CD.ID " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN USU_CC_IMPUTACION UCCI WITH (NOLOCK) ON COALESCE(UCCI.UON1,'')+COALESCE('#'+UCCI.UON2,'')+COALESCE('#'+UCCI.UON3,'')+COALESCE('#'+UCCI.UON4,'') = CC.VALOR_TEXT AND UCCI.USU = @USUARIO" & vbCrLf
sConsulta = sConsulta & "   WHERE CD.TIPO_CAMPO_GS=129 AND INSTANCIA=@INSTANCIA" & vbCrLf
sConsulta = sConsulta & "                           " & vbCrLf
sConsulta = sConsulta & "   IF @COUNT=0" & vbCrLf
sConsulta = sConsulta & "   SELECT 0---no puede descargar" & vbCrLf
sConsulta = sConsulta & "   ELSE    " & vbCrLf
sConsulta = sConsulta & "   SELECT 1---puede" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion31900_08_1896_A31900_08_1897() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim oADOConnection As ADODB.Connection
    Dim sConsulta As String

    On Error GoTo Error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"
    
    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Actualiza Tablas"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    frmProgreso.ProgressBar1.Refresh
    V_31900_8_Tablas_097
    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Actualiza Datos"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    frmProgreso.ProgressBar1.Refresh
    V_31900_8_Datos_097

    
    sConsulta = "UPDATE VERSION SET NUM ='3.00.18.97'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103), PRODVER='31900.8'"
    ExecuteSQL gRDOCon, sConsulta
        
    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False
        
    CodigoDeActualizacion31900_08_1896_A31900_08_1897 = True
    Exit Function

Error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    CodigoDeActualizacion31900_08_1896_A31900_08_1897 = False
End Function

Public Sub V_31900_8_Tablas_097()
    Dim sConsulta As String
    sConsulta = "ALTER TABLE INSTANCIA_ASIG_COMP ALTER COLUMN [COMENT] [nvarchar](4000) NULL" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
End Sub

Public Sub V_31900_8_Datos_097()
    Dim sConsulta As String
sConsulta = "UPDATE PARGEN_DOT SET PETOFEMAILDOT=REPLACE(PETOFEMAILDOT,'.DOT','.HTM') " & vbCrLf
sConsulta = sConsulta & "UPDATE PARGEN_DOT SET PETOFEWEBDOT=REPLACE(PETOFEWEBDOT,'.DOT','.HTM') " & vbCrLf
sConsulta = sConsulta & "UPDATE PARGEN_DOT SET PETSUBMAILDOT=REPLACE(PETSUBMAILDOT,'.DOT','.HTM') " & vbCrLf
sConsulta = sConsulta & "UPDATE PARGEN_DOT SET PETSUBWEBDOT=REPLACE(PETSUBWEBDOT,'.DOT','.HTM') " & vbCrLf
sConsulta = sConsulta & "UPDATE PARGEN_DOT SET OBJMAILDOT=REPLACE(OBJMAILDOT,'.DOT','.HTM') " & vbCrLf
sConsulta = sConsulta & "UPDATE PARGEN_DOT SET OBJWEBDOT=REPLACE(OBJWEBDOT,'.DOT','.HTM') " & vbCrLf
sConsulta = sConsulta & "UPDATE PARGEN_DOT SET ADJMAILDOT=REPLACE(ADJMAILDOT,'.DOT','.HTM') " & vbCrLf
sConsulta = sConsulta & "UPDATE PARGEN_DOT SET ADJNOMAILDOT=REPLACE(ADJNOMAILDOT,'.DOT','.HTM')" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

