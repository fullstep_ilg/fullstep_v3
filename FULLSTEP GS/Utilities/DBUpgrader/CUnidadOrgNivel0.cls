VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CUnidadOrgNivel0"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
'********************* CUnidadOrgNivel0 **********************************
'*             Autor : Javier Arana
'*             Creada : 27/7/98
'* **************************************************************


Option Explicit

' *********************** Variables privadas de la clase *********+
Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
    
End Enum

Private mvarConexion As CConexion
Private mvarUnidadesOrgNivel1 As CUnidadesOrgNivel1

' *********************** Propiedades de la clase ******************

Friend Property Set Conexion(ByVal vData As CConexion)
    
    Set mvarConexion = vData
    
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = mvarConexion
End Property



Public Property Set UnidadesOrgNivel1(ByVal vData As CUnidadesOrgNivel1)
        Set mvarUnidadesOrgNivel1 = vData
End Property


Public Property Get UnidadesOrgNivel1() As CUnidadesOrgNivel1
     Set UnidadesOrgNivel1 = mvarUnidadesOrgNivel1
End Property

' ********************Funciones publicas de la clase ************

 Public Sub CargarTodasLasUnidadesOrgNivel1(Optional ByVal UON1 As Variant, Optional ByVal UON2 As Variant, Optional ByVal UON3 As Variant, Optional ByVal RUO As Boolean = False, Optional ByVal RUODep As Boolean, Optional ByVal RDepAsoc As Boolean, Optional ByVal CarIniCod As String, Optional ByVal CarIniDen As String, Optional ByVal CoincidenciaTotal As Boolean, Optional ByVal OrdenadasPorDen As Boolean = False, Optional ByVal UsarIndice As Boolean = False)

Dim rs As New ADODB.Recordset
Dim sConsulta As String
Dim sCad As String
Dim lIndice As Long
Dim fldCod As ADODB.Field
Dim fldDen As ADODB.Field

'********* Precondicion **************************************
If mvarConexion Is Nothing Then
    Err.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CGrupoDevolverTodasLasUnidadesOrgNivel1", "No se ha establecido la conexion"
    Exit Sub
End If
'*************************************************************


If Not IsMissing(UON1) Then
    If IsEmpty(UON1) Then
        UON1 = Null
    End If
    If Trim(UON1) = "" Then
        UON1 = Null
    End If
Else
    UON1 = Null
End If

If Not IsMissing(UON2) Then
    If IsEmpty(UON2) Then
        UON2 = Null
    End If
    If Trim(UON2) = "" Then
        UON2 = Null
    End If
Else
    UON2 = Null
End If

If Not IsMissing(UON3) Then
    If IsEmpty(UON3) Then
        UON3 = Null
    End If
    If Trim(UON3) = "" Then
        UON3 = Null
    End If
Else
    UON3 = Null
End If


If Not RUO And Not RUODep And Not RDepAsoc Then
    
    ' No hay restricciones
    sConsulta = "SELECT UON1.COD AS UON1COD, UON1.DEN as UON1DEN FROM UON1 "
    
Else
    
    If IsNull(UON1) Then
        'Es del nivel 0 , o sea que como si no hubiese restricciones
        sConsulta = "SELECT UON1.COD AS UON1COD, UON1.DEN as UON1DEN FROM UON1 "
    Else ' Es del nivel 1 o 2 o 3, asi que cargamos solo su unidad
        CarIniCod = UON1
        CoincidenciaTotal = True
        sConsulta = "SELECT UON1.COD AS UON1COD, UON1.DEN as UON1DEN FROM UON1 "
    End If

End If

If CarIniCod <> "" Then
    sCad = "1"
Else
    sCad = "0"
End If

If CarIniDen <> "" Then
    sCad = sCad & "1"
Else
    sCad = sCad & "0"
End If
    
    Select Case sCad
            
    Case "01"
            
            If CoincidenciaTotal Then
                sConsulta = sConsulta & "WHERE UON1DEN = '" & DblQuote(CarIniDen) & "'"
            Else
                sConsulta = sConsulta & "AND UON1DEN LIKE '" & DblQuote(CarIniDen) & "%'"
            End If
                      
    Case "10"
            If CoincidenciaTotal Then
                sConsulta = sConsulta & "AND UON1COD = '" & DblQuote(CarIniCod) & "'"
            Else
                sConsulta = sConsulta & "AND UON1COD LIKE '" & DblQuote(CarIniCod) & "%'"
            End If
                   
    Case "11"
            If CoincidenciaTotal Then
                sConsulta = sConsulta & "AND UON1COD = '" & DblQuote(CarIniCod) & "'"
                sConsulta = sConsulta & "AND UON1DEN = '" & DblQuote(CarIniDen) & "'"
            Else
                sConsulta = sConsulta & "AND UON1COD LIKE '" & DblQuote(CarIniCod) & "%'"
                sConsulta = sConsulta & "AND UON1DEN LIKE '" & DblQuote(CarIniDen) & "%'"
            End If
    End Select
    
If OrdenadasPorDen Then
    sConsulta = sConsulta & " ORDER BY UON1DEN"
Else
    sConsulta = sConsulta & " ORDER BY UON1COD"
End If

rs.Open sConsulta, mvarConexion.adoCon, adOpenForwardOnly, adLockReadOnly
      
If rs.EOF Then
        
    rs.Close
    Set rs = Nothing
    Set mvarUnidadesOrgNivel1 = Nothing
    Set mvarUnidadesOrgNivel1 = New CUnidadesOrgNivel1
    Set mvarUnidadesOrgNivel1.Conexion = mvarConexion
    
    Exit Sub
      
Else
    
    Set mvarUnidadesOrgNivel1 = Nothing
    Set mvarUnidadesOrgNivel1 = New CUnidadesOrgNivel1
    Set mvarUnidadesOrgNivel1.Conexion = mvarConexion
    
    Set fldCod = rs.Fields("COD")
    Set fldDen = rs.Fields("DEN")
    
    If UsarIndice Then
        lIndice = 0
        
        While Not rs.EOF
                
            mvarUnidadesOrgNivel1.Add fldCod.Value, fldDen.Value, lIndice
            rs.MoveNext
            lIndice = lIndice + 1
            
        Wend
    Else
        
        While Not rs.EOF
            mvarUnidadesOrgNivel1.Add fldCod.Value, fldDen.Value
            rs.MoveNext
            
        Wend
    End If
    
    rs.Close
    Set rs = Nothing
    Set fldCod = Nothing
    Set fldDen = Nothing
    Exit Sub
      
End If
        
End Sub




Private Sub Class_Terminate()
    Set mvarConexion = Nothing
End Sub


