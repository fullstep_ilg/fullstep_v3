VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CUnidadOrgNivel3"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'********************* CUnidadOrgNivel2 **********************************
'*             Autor : Javier Arana
'*             Creada : 28/7/98
'****************************************************************

Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
   
End Enum

Private m_oConexion As CConexion
Private mvarIndice As Long
Private m_sCod As String 'local copy
Private mvarDen As String 'local copy
Private m_sCodUnidadOrgNivel1 As Variant
Private m_sCodUnidadOrgNivel2 As Variant
Private mvarRecordset As ADODB.Recordset
Private mvarbPresupuestos As Boolean
Private m_bPresAsig As Boolean
Private m_vidEmpresa As Variant
Private m_oEmpresa As CEmpresa
Dim m_vFecAct As Variant



'*************** Propiedades de la clase ******
Public Property Let CodUnidadOrgNivel2(ByVal vCod As Variant)
    Let m_sCodUnidadOrgNivel2 = vCod
End Property
Public Property Get CodUnidadOrgNivel2() As Variant
    CodUnidadOrgNivel2 = m_sCodUnidadOrgNivel2
End Property
Public Property Let CodUnidadOrgNivel1(ByVal vCod As Variant)
    Let m_sCodUnidadOrgNivel1 = vCod
End Property
Public Property Get CodUnidadOrgNivel1() As Variant
    CodUnidadOrgNivel1 = m_sCodUnidadOrgNivel1
End Property

Friend Property Set Conexion(ByVal con As CConexion)
Set m_oConexion = con
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = m_oConexion
End Property

Public Property Get IdEmpresa() As Variant
    IdEmpresa = m_vidEmpresa
End Property

Public Property Let IdEmpresa(ByVal vEmpr As Variant)
    m_vidEmpresa = vEmpr
End Property
Public Property Set Empresa(ByVal oEmp As CEmpresa)
Set m_oEmpresa = oEmp
End Property
Public Property Get Empresa() As CEmpresa
Set Empresa = m_oEmpresa
End Property
Public Property Get FecAct() As Variant
    FecAct = m_vFecAct
End Property

Public Property Let FecAct(ByVal vFecAct As Variant)
    m_vFecAct = vFecAct
End Property



Public Property Let ConPresup(ByVal vData As Boolean)
    mvarbPresupuestos = vData
End Property

Public Property Get ConPresup() As Boolean
    ConPresup = mvarbPresupuestos
End Property

Public Property Let PresAsig(ByVal bAsig As Boolean)
    m_bPresAsig = bAsig
End Property

Public Property Get PresAsig() As Boolean
    PresAsig = m_bPresAsig
End Property

Public Property Get Indice() As Long
    Indice = mvarIndice

End Property

Public Property Let Indice(ByVal lInd As Long)
    mvarIndice = lInd
End Property

Public Property Let Den(ByVal vData As String)
    mvarDen = vData
End Property


Public Property Get Den() As String
    Den = mvarDen
End Property


Public Property Let Cod(ByVal vData As String)
    m_sCod = vData
End Property


Public Property Get Cod() As String
    Cod = m_sCod
End Property

Private Sub Class_Terminate()

Set m_oConexion = Nothing

End Sub


Public Function CargarEmpresa() As CEmpresa
Dim sConsulta As String
Dim adoRS As ADODB.Recordset

sConsulta = "SELECT E.* FROM EMP E INNER JOIN UON3 U3 ON E.ID=U3.EMPRESA AND U3.UON1 = '" & DblQuote(m_sCodUnidadOrgNivel1) & "'  AND U3.UON2 = '" & DblQuote(m_sCodUnidadOrgNivel2) & "' AND U3.COD='" & DblQuote(m_sCod) & "'"
Set adoRS = New ADODB.Recordset
adoRS.Open sConsulta, m_oConexion.adoCon, adOpenForwardOnly
If adoRS.eof Then
    m_vidEmpresa = Null
    adoRS.Close
    Set m_oEmpresa = Nothing
Else
    Set m_oEmpresa = New CEmpresa
    With m_oEmpresa
        Set .Conexion = m_oConexion
        .ID = adoRS.Fields("ID").Value
        m_vidEmpresa = .ID
        .Den = adoRS.Fields("DEN").Value
        .Nif = adoRS.Fields("NIF").Value
        .CP = adoRS.Fields("CP").Value
        .CodPais = adoRS.Fields("PAI").Value
        .CodProvi = adoRS.Fields("PROVI").Value
        .Direccion = adoRS.Fields("DIR").Value
        .Poblacion = adoRS.Fields("POB").Value
        .ERP = (adoRS.Fields("ERP").Value = 1)
    End With
    adoRS.Close
    
End If
Set adoRS = Nothing

Set CargarEmpresa = m_oEmpresa
End Function

Public Function ImposibleEliminar() As Boolean

Dim sConsulta As String
Dim adoRS As ADODB.Recordset


sConsulta = "if (select erp from emp where id = " & m_vidEmpresa & ") = 1" & vbCrLf
sConsulta = sConsulta & "    SELECT COUNT(*) NUM FROM (" & vbCrLf
sConsulta = sConsulta & "SELECT EMPRESA FROM UON1 WHERE EMPRESA = " & m_vidEmpresa & "" & vbCrLf
sConsulta = sConsulta & "UNION SELECT EMPRESA FROM UON2 WHERE EMPRESA = " & m_vidEmpresa & "" & vbCrLf
sConsulta = sConsulta & "UNION SELECT EMPRESA FROM UON3 WHERE EMPRESA = " & m_vidEmpresa & " AND UON1 <>'" & DblQuote(m_sCodUnidadOrgNivel1) & "' AND UON2 <>'" & DblQuote(m_sCodUnidadOrgNivel2) & "' AND COD <>'" & DblQuote(m_sCod) & "' ) u" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SELECT 0 NUM" & vbCrLf

ImposibleEliminar = False
Set adoRS = New ADODB.Recordset
adoRS.Open sConsulta, m_oConexion.adoCon, adOpenForwardOnly
If adoRS.eof Then
    adoRS.Close
Else
    If adoRS.Fields("NUM").Value > 0 Then
        ImposibleEliminar = True
    End If
    adoRS.Close
End If
Set adoRS = Nothing

End Function


Public Function FinalizarEdicionModificando() As Boolean

Dim sConsulta As String
Dim adoRS As ADODB.Recordset
m_oConexion.adoCon.Execute "BEGIN TRANSACTION"
m_oConexion.adoCon.Execute "SET XACT_ABORT OFF"
With m_oEmpresa
    
    sConsulta = "INSERT INTO EMP (NIF, DEN, DIR, CP,POB, PAI, PROVI, ERP) VALUES ('" & DblQuote(.Nif) & "','" & DblQuote(.Den) & "','" & DblQuote(.Direccion) & "'," _
              & StrToSQLNULL(.CP) & "," & StrToSQLNULL(.Poblacion) & "," & StrToSQLNULL(.CodPais) & "," & StrToSQLNULL(.CodProvi) & "," & BooleanToSQLBinary(.ERP) & ")"
    m_oConexion.adoCon.Execute sConsulta
    Set adoRS = New ADODB.Recordset
    sConsulta = "SELECT MAX(ID) ID FROM EMP"
    adoRS.Open sConsulta, m_oConexion.adoCon, adOpenForwardOnly
    .ID = adoRS.Fields("ID").Value
    adoRS.Close
    Set adoRS = Nothing

    sConsulta = "UPDATE UON3 SET EMPRESA = " & m_oEmpresa.ID & " WHERE UON1= '" & DblQuote(m_sCodUnidadOrgNivel1) & "' AND UON2= '" & DblQuote(m_sCodUnidadOrgNivel2) & "' AND COD = '" & DblQuote(m_sCod) & "'"
    m_oConexion.adoCon.Execute sConsulta
    
    sConsulta = "UPDATE ORDEN_ENTREGA SET EMPRESA = " & m_oEmpresa.ID & " WHERE EMPRESA IS NULL"
    m_oConexion.adoCon.Execute sConsulta
End With

m_oConexion.adoCon.Execute "COMMIT TRANSACTION"


FinalizarEdicionModificando = True
End Function


