--INTEGRIDAD CON TABLAS DE AHORROS
DELETE FROM AR_ITEM_MES_PRESCON3_1 FROM AR_ITEM_MES_PRESCON3_1 AI WHERE NOT EXISTS (SELECT * FROM ITEM I WHERE AI.ANYO = I.ANYO AND AI.GMN1=I.GMN1 AND AI.PROCE = I.PROCE AND AI.ITEM = I.ID)
select * FROM AR_ITEM_MES_PRESCON3_1 AI WHERE NOT EXISTS (SELECT * FROM ITEM I WHERE AI.ANYO = I.ANYO AND AI.GMN1=I.GMN1 AND AI.PROCE = I.PROCE AND AI.ITEM = I.ID)
DELETE FROM AR_ITEM_MES_PRESCON3_2 FROM AR_ITEM_MES_PRESCON3_2 AI WHERE NOT EXISTS (SELECT * FROM ITEM I WHERE AI.ANYO = I.ANYO AND AI.GMN1=I.GMN1 AND AI.PROCE = I.PROCE AND AI.ITEM = I.ID)
Select * FROM AR_ITEM_MES_PRESCON3_2 AI WHERE NOT EXISTS (SELECT * FROM ITEM I WHERE AI.ANYO = I.ANYO AND AI.GMN1=I.GMN1 AND AI.PROCE = I.PROCE AND AI.ITEM = I.ID)
DELETE FROM AR_ITEM_MES_PRESCON3_3 FROM AR_ITEM_MES_PRESCON3_3 AI WHERE NOT EXISTS (SELECT * FROM ITEM I WHERE AI.ANYO = I.ANYO AND AI.GMN1=I.GMN1 AND AI.PROCE = I.PROCE AND AI.ITEM = I.ID)
select * FROM AR_ITEM_MES_PRESCON3_3 AI WHERE NOT EXISTS (SELECT * FROM ITEM I WHERE AI.ANYO = I.ANYO AND AI.GMN1=I.GMN1 AND AI.PROCE = I.PROCE AND AI.ITEM = I.ID)
DELETE FROM AR_ITEM_MES_PRESCON3_4 FROM AR_ITEM_MES_PRESCON3_4 AI WHERE NOT EXISTS (SELECT * FROM ITEM I WHERE AI.ANYO = I.ANYO AND AI.GMN1=I.GMN1 AND AI.PROCE = I.PROCE AND AI.ITEM = I.ID)
select * FROM AR_ITEM_MES_PRESCON3_4 AI WHERE NOT EXISTS (SELECT * FROM ITEM I WHERE AI.ANYO = I.ANYO AND AI.GMN1=I.GMN1 AND AI.PROCE = I.PROCE AND AI.ITEM = I.ID)


--ERRORES

--TODAS
delete from perf_acc from perf_acc pa where not exists ( select * from acc where pa.acc = acc.id)
select * from perf_acc pa where not exists ( select * from acc where pa.acc = acc.id)


--ATEGI Y CAMPOFRIO
select * from item_uon3 iu where not exists ( select * from item i where iu.anyo = i.anyo and iu.gmn1 = i.gmn1 and iu.proce = i.proce and iu.item = i.id)
select * from item_uon2 iu where not exists ( select * from item i where iu.anyo = i.anyo and iu.gmn1 = i.gmn1 and iu.proce = i.proce and iu.item = i.id)
select * from item_uon1 iu where not exists ( select * from item i where iu.anyo = i.anyo and iu.gmn1 = i.gmn1 and iu.proce = i.proce and iu.item = i.id)

delete from item_uon3 from item_uon3 iu where not exists ( select * from item i where iu.anyo = i.anyo and iu.gmn1 = i.gmn1 and iu.proce = i.proce and iu.item = i.id)
delete from item_uon2 from item_uon2 iu where not exists ( select * from item i where iu.anyo = i.anyo and iu.gmn1 = i.gmn1 and iu.proce = i.proce and iu.item = i.id)
delete from item_uon1 from item_uon1 iu where not exists ( select * from item i where iu.anyo = i.anyo and iu.gmn1 = i.gmn1 and iu.proce = i.proce and iu.item = i.id)

--FICOSA
update item set proveact=null 
  from item where not exists(select * from prove where item.proveact=prove.cod)
              and proveact is not null


select *  from item where not exists(select * from prove where item.proveact=prove.cod)
              and proveact is not null

--CC
delete from prove_art4 where not exists (select * from art4 a where a.gmn1=prove_art4.gmn1
and a.gmn2=prove_art4.gmn2 and a.gmn3=prove_art4.gmn3 and a.gmn4=prove_art4.gmn4 and a.cod=prove_art4.art) 


