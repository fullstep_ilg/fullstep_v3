VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CProvincia"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit



Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
    FaltaObjetoPais = 614
    
End Enum
Private m_oConexion As CConexion 'local copy
Private m_lIndice As Long 'local copy
Private m_adores As ADODB.Recordset
Private m_sCod As String 'local copy
Private m_sDen As String 'local copy
Private m_oPais As CPais 'local copy
Private m_iId As Integer
Private m_vFecAct As Variant
''' Estado integración
Private m_vEstIntegracion As Variant

' Variables para el control de cambios del Log e Integración
Private m_sUsuario As String             'Usuario que realiza los cambios.

Public Property Let FecAct(ByVal vData As Variant)

    ''' * Objetivo: Dar valor a la variable privada FECACT
    ''' * Recibe: Fecha de ultima actualizacion de la moneda
    ''' * Devuelve: Nada

    m_vFecAct = vData
    
End Property
Public Property Get FecAct() As Variant

    ''' * Objetivo: Devolver la variable privada FECACT
    ''' * Recibe: Nada
    ''' * Devuelve: Fecha de ultima actualizacion de la moneda

    FecAct = m_vFecAct
    
End Property

Public Property Let ID(ByVal Data As Integer)
    Let m_iId = Data
End Property
Public Property Get ID() As Integer
    ID = m_iId
End Property


Public Property Set Pais(ByVal vData As CPais)
    Set m_oPais = vData
End Property


Public Property Get Pais() As CPais
    Set Pais = m_oPais
End Property

Public Property Let Indice(ByVal vData As Long)
    m_lIndice = vData
End Property


Public Property Get Indice() As Long
    Indice = m_lIndice
End Property



Friend Property Set Conexion(ByVal vData As CConexion)
    Set m_oConexion = vData
End Property


Friend Property Get Conexion() As CConexion
    Set Conexion = m_oConexion
End Property



Public Property Let Den(ByVal vData As String)
    m_sDen = vData
End Property


Public Property Get Den() As String
    Den = m_sDen
End Property



Public Property Let Cod(ByVal vData As String)
    m_sCod = vData
End Property


Public Property Get Cod() As String
    Cod = m_sCod
End Property
Public Property Let Usuario(ByVal vData As String)

    ''' * Objetivo: Dar valor a la variable privada Usuario
    ''' * Recibe: Codigo del usuario conectado
    ''' * Devuelve: Nada

    m_sUsuario = vData
    
End Property
Public Property Let EstadoIntegracion(ByVal vData As Variant)

    ''' * Objetivo: Dar valor al estado de integración de la moneda
    ''' * Recibe: el valor
    ''' * Devuelve: Nada

    m_vEstIntegracion = vData
    
End Property
Public Property Get EstadoIntegracion() As Variant

    ''' * Objetivo: Devolver el estado de integración de la moneda
    ''' * Recibe: Nada
    ''' * Devuelve: El estado de integración de la moneda

    EstadoIntegracion = m_vEstIntegracion
    
End Property




Private Sub Class_Terminate()
    Set m_oConexion = Nothing

End Sub


