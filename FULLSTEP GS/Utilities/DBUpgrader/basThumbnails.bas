Attribute VB_Name = "basThumbnails"
Option Explicit

'Data format definitions
Public Const PJT_RGB    As Long = 1
Public Const PJT_YCBCR  As Long = 2
Public Const PJT_CMYK   As Long = 3

'File format definitions
Public Const PJT_BMP         As Long = &H1
Public Const PJT_TIFF        As Long = &H2
Public Const PJT_JPEG        As Long = &H3
Public Const PJT_GIF         As Long = &H4
Public Const PJT_PNG         As Long = &H5
Public Const PJT_XBM         As Long = &H6

'Additional flags for JPEG
'A: Prog/Seq
Public Const PJT_JPEGSEQUENTIAL  As Long = &H0
Public Const PJT_JPEG12BIT       As Long = &H100
Public Const PJT_JPEGPROGRESSIVE2 As Long = &H200
Public Const PJT_JPEGPROGRESSIVE3 As Long = &H300
Public Const PJT_JPEGPROGRESSIVE4 As Long = &H400
Public Const PJT_JPEGPROGRESSIVE5 As Long = &H500

'B. Compression
Public Const PJT_JPEGCOMP1   As Long = &H10000
Public Const PJT_JPEGCOMP2   As Long = &H14000
Public Const PJT_JPEGCOMP3   As Long = &H1E000
Public Const PJT_JPEGCOMP4   As Long = &H28000
Public Const PJT_JPEGCOMP5   As Long = &H32000
Public Const PJT_JPEGCOMP6   As Long = &H3C000
Public Const PJT_JPEGCOMP7   As Long = &H46000
Public Const PJT_JPEGCOMP8   As Long = &H50000
Public Const PJT_JPEGCOMP9   As Long = &H5A000
Public Const PJT_JPEGCOMP10  As Long = &H64000

'Additional flags for TIFF
Public Const PJT_TIFFBASELINE        As Long = &H0
Public Const PJT_TIFFEXTENDDEPTH     As Long = &H100
Public Const PJT_TIFFEXTENDALPHA     As Long = &H200
Public Const PJT_TIFFCMYK            As Long = &H400

'Display mode definitions
Public Const PJT_NONE            As Long = &H0
Public Const PJT_SCROLL          As Long = &H1
Public Const PJT_ZOOM            As Long = &H2
Public Const PJT_SETROI          As Long = &H3
Public Const PJT_SETROIELIPSE    As Long = &H4
Public Const PJT_SETROIFREE      As Long = &H5
Public Const PJT_SCALETOFIT      As Long = &H6
Public Const PJT_SCALETOFITAR    As Long = &H7
Public Const PJT_ANIMATE         As Long = &H8
Public Const PJT_INTERPOLATEZOOM As Long = &H9

'Band specifiers
Public Const PJT_BAND1           As Long = &H1000
Public Const PJT_BAND2           As Long = &H2000
Public Const PJT_BAND3           As Long = &H3000
Public Const PJT_BAND4           As Long = &H4000

'Image processing operations
Public Const PJT_INVERT          As Long = &H1
Public Const PJT_MONO            As Long = &H2
Public Const PJT_CONVOLVE        As Long = &H3
Public Const PJT_BMORPH          As Long = &H4
Public Const PJT_GMORPH          As Long = &H5
Public Const PJT_AUTOEQUALIZE    As Long = &H6
Public Const PJT_HISTSTRETCH     As Long = &H7
Public Const PJT_THRESHOLD       As Long = &H8
Public Const PJT_CLIPTOROI       As Long = &H9
Public Const PJT_ALU             As Long = &HA
Public Const PJT_FLIPX           As Long = &HB
Public Const PJT_FLIPY           As Long = &HC
Public Const PJT_MATCHFORMAT     As Long = &HD
Public Const PJT_GLOBALPALETTE   As Long = &HE
Public Const PJT_ROTATE          As Long = &HF
Public Const PJT_ROTATEINTERP    As Long = &H10

'Standard convolve operations
Public Const PJT_EDGELAP         As Long = &H100
Public Const PJT_LOWPASS         As Long = &H200
Public Const PJT_HIPASS          As Long = &H300
Public Const PJT_PREWITTH        As Long = &H400
Public Const PJT_PREWITTV        As Long = &H500
Public Const PJT_SMOOTH          As Long = &H600
Public Const PJT_CUSTOM          As Long = &H1000

'Standard morph operations
Public Const PJT_ERODE           As Long = &H100
Public Const PJT_DILATE          As Long = &H200
Public Const PJT_OPEN            As Long = &H300
Public Const PJT_CLOSE           As Long = &H400
Public Const PJT_SKELETON        As Long = &H500
Public Const PJT_OUTLINE         As Long = &H600

'ALU operations
Public Const PJT_ALUADD          As Long = &H1
Public Const PJT_ALUSUBTRACT     As Long = &H2
Public Const PJT_ALUAND          As Long = &H3
Public Const PJT_ALUOR           As Long = &H4
Public Const PJT_ALUXOR          As Long = &H5
Public Const PJT_ALUWRAPSUBTRACT As Long = &H6
Public Const PJT_ALUWRAPADD      As Long = &H7

'Image part specifiers
Public Const PJT_ROI             As Long = &H1000000
Public Const PJT_FRAME           As Long = &H2000000
Public Const PJT_IMAGE           As Long = &H3000000

'Update Specifier
Public Const PJT_NOUPDATE        As Long = &H10000000

'Import modes
Public Const PJT_TOCURRENTFRAME  As Long = &H10
Public Const PJT_TONEWFRAME      As Long = &H20
Public Const PJT_TONEWIMAGE      As Long = &H30

'Import/export settings
Public Const PJT_CLIPBOARD         As Long = &H1
Public Const PJT_DIB               As Long = &H2
Public Const PJT_DEVICECONTEXT     As Long = &H3
Public Const PJT_WINDOW            As Long = &H4
Public Const PJT_BITMAP            As Long = &H5
Public Const PJT_MEMJPEG           As Long = &H6
Public Const PJT_MEMBMP            As Long = &H7
Public Const PJT_MEMPNG            As Long = &H8
Public Const PJT_PICTURE           As Long = &H9

'Process region settings
Public Const PJT_REGION            As Long = &H1
Public Const PJT_RECTANGLE         As Long = &H2

'Resize flags
Public Const PJT_SIMPLE              As Long = &H0
Public Const PJT_CANVAS              As Long = &H1
Public Const PJT_INTERPOLATE         As Long = &H2
Public Const PJT_NEWIMAGE            As Long = &H3

'Display range flags
Public Const PJT_BITRANGE            As Long = &H1
Public Const PJT_INTENSITYRANGE      As Long = &H2

'GetPixel/SetPixel flags
Public Const PJT_COLORREF            As Long = &H100
Public Const PJT_COORDINATES         As Long = &H1
Public Const PJT_BACKGROUND          As Long = &H3
Public Const PJT_FILLREGION          As Long = &H4
Public Const PJT_BUILDMASK           As Long = &H5

'Animation flags
Public Const PJT_ANIMRESTOREBG       As Long = &H1
Public Const PJT_ANIMADDTOFRAME      As Long = &H2
Public Const PJT_ANIMTRANSPARENT     As Long = &H4

'TWAIN flags
Public Const PJT_TWAINNOGUI          As Long = &H1
Public Const PJT_TWAINMEMXFER        As Long = &H2

'Window background definitions
Public Const PJT_BGCOLOUR    As Long = &H0
Public Const PJT_BGBRUSH     As Long = &H1

'Error codes
Public Const PJTE_UNKNOWN                    As Long = 101
Public Const PJTE_FILER                      As Long = 102
Public Const PJTE_FILEW                      As Long = 103
Public Const PJTE_FORMAT                     As Long = 104
Public Const PJTE_MEMORY                     As Long = 105
Public Const PJTE_FILEREND                   As Long = 106
Public Const PJTE_SAFEARRAY                  As Long = 107
Public Const PJTE_VALIDFRAME                 As Long = 108
Public Const PJTE_VALIDWINDOW                As Long = 109
Public Const PJTE_PARAMETER                  As Long = 110
Public Const PJTE_INTERNAL                   As Long = 112
Public Const PJTE_MAXDEPTH                   As Long = 113
Public Const PJTE_SUPPORTSUB                 As Long = 114
Public Const PJTE_FILECORRUPT                As Long = 115
Public Const PJTE_GLOBAL                     As Long = 117
Public Const PJTE_BITBUFFERFULL              As Long = 118
Public Const PJTE_IMAGESIZE                  As Long = 121
Public Const PJTE_IMAGEFORMAT                As Long = 124
Public Const PJTE_IMAGESIZEFORMAT            As Long = 125
Public Const PJTE_DEVICECONTEXT              As Long = 127
Public Const PJTE_COMPATIBILITY              As Long = 128
Public Const PJTE_DATAPRESENT                As Long = 129
Public Const PJTE_NOTRANSPARENT              As Long = 130
Public Const PJTE_TIMER                      As Long = 131
Public Const PJTE_FILEMULTIPLE               As Long = 132
Public Const PJTE_SUPPORT                    As Long = 133
Public Const PJTE_VALIDMASK                  As Long = 134
Public Const PJTE_FILETOBIG                  As Long = 135
Public Const PJTE_UNDEFINED                  As Long = 136
Public Const PJTE_TWAIN                      As Long = 137
Public Const PJTE_TWAINCANCEL                As Long = 138


'Parameter IDs
Public Const PJTP_CUSTOMPAINT               As Long = 1
Public Const PJTP_TRANSPARENTWINDOW         As Long = 4
Public Const PJTP_DISPLAYMETHOD             As Long = 5
Public Const PJTP_TWAINMANUFACTURER         As Long = 6
Public Const PJTP_TWAINPRODUCT              As Long = 7
Public Const PJTP_TWAINPRODUCTFAMILY        As Long = 8
Public Const PJTP_TWAINVERSION              As Long = 9
Public Const PJTP_TWAINLANGUAGE             As Long = 10

Public Const PJTP_DMBMP                     As Long = 1
Public Const PJTP_DMDIB                     As Long = 2




