Attribute VB_Name = "bas_V_3_5"
Public Function CodigoDeActualizacion3_3_3500_A3_3_3501() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim oADOConnection As ADODB.Connection
    Dim sConsulta As String

    On Error GoTo Error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Actualiza Tablas"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    frmProgreso.ProgressBar1.Refresh
    V_3_5_Tablas_001
    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Actualiza Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    frmProgreso.ProgressBar1.Refresh
    V_3_5_Storeds_001

    sConsulta = "UPDATE VERSION SET NUM ='3.00.35.001'" & ", FECHA=CONVERT(datetime,'" & Format(Now, "mm-dd-yyyy hh:mm:ss") & "',110), PRODVER='3.5.1'"
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False

    CodigoDeActualizacion3_3_3500_A3_3_3501 = True
    Exit Function

Error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If

    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    CodigoDeActualizacion3_3_3500_A3_3_3501 = False
End Function

Private Sub V_3_5_Tablas_001()
    Dim sConsulta As String
sConsulta = "IF NOT EXISTS (SELECT * FROM SYS.COLUMNS WHERE NAME=N'NO_CALC_PRES_UNI' AND Object_ID = Object_ID(N'PROCE_DEF'))" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_DEF ADD NO_CALC_PRES_UNI TINYINT NOT NULL CONSTRAINT DF_PROCE_DEF_NO_CALC_PRES_UNI DEFAULT 1" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF NOT EXISTS (SELECT * FROM SYS.COLUMNS WHERE NAME=N'CAL_LIN_BASE' AND Object_ID = Object_ID(N'PARGEN_GEST'))" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PARGEN_GEST ADD CAL_LIN_BASE INT NOT NULL DEFAULT(0)" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF NOT EXISTS (SELECT * FROM SYS.COLUMNS WHERE NAME=N'NUM_MEJ_OFE' AND Object_ID = Object_ID(N'PARGEN_GEST'))" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PARGEN_GEST ADD NUM_MEJ_OFE INT NULL" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF NOT EXISTS (SELECT * FROM SYS.COLUMNS WHERE NAME=N'MARCA_CALCULO_ITEMPREC' AND Object_ID = Object_ID(N'ITEM_OFE'))" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OFE ADD MARCA_CALCULO_ITEMPREC INT NOT NULL CONSTRAINT DF_ITEM_OFE_MARCA_CALCULO_ITEMPREC DEFAULT 1" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

End Sub

Private Sub V_3_5_Storeds_001()
    Dim sConsulta As String

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_NUEVA_OFERTA]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_NUEVA_OFERTA]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSN_RECALCULAR_PRECIO_ITEM]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSN_RECALCULAR_PRECIO_ITEM]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSN_RECALCULAR_PRECIO_ITEM]" & vbCrLf
sConsulta = sConsulta & "@ANYO AS INT," & vbCrLf
sConsulta = sConsulta & "@GMN AS VARCHAR(10)," & vbCrLf
sConsulta = sConsulta & "@PROCE AS INT," & vbCrLf
sConsulta = sConsulta & "@ITEM AS INT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--DECLARE @ANYO AS INT = 2015" & vbCrLf
sConsulta = sConsulta & "--DECLARE @GMN AS VARCHAR(10) = 'CGE'" & vbCrLf
sConsulta = sConsulta & "--DECLARE @PROCE AS INT = 96" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--update  NO_CALC_PRES_UNI= 1 FROM PROCE_DEF WITH(NOLOCK) WHERE ANYO=@ANYO AND GMN1 = @GMN AND PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "DECLARE @NO_ACTUALIZAR AS BIT" & vbCrLf
sConsulta = sConsulta & "DECLARE @MODO AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @MODO_NUM AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRECIO_ITEM AS FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @REGISTROS AS INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @MODO = CAL_LIN_BASE, @MODO_NUM = NUM_MEJ_OFE from PARGEN_GEST WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "if @MODO =1 OR @MODO = 2" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   ---SE MIRA SI EL PROCESO EST? CONFIGURADO PARA ACTUALIZAR LOS PRECIOS" & vbCrLf
sConsulta = sConsulta & "   SELECT @NO_ACTUALIZAR = NO_CALC_PRES_UNI FROM PROCE_DEF WITH(NOLOCK) WHERE ANYO=@ANYO AND GMN1 = @GMN AND PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "   IF @NO_ACTUALIZAR = 0" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       SET XACT_ABORT ON" & vbCrLf
sConsulta = sConsulta & "       BEGIN TRANSACTION" & vbCrLf
sConsulta = sConsulta & "       --REINICIAMOS EL VALOR DEL CAMPO ALTER TABLE ITEM_OFE ADD MARCA_CALCULO_ITEMPREC INT NOT NULL CONSTRAINT DF_ITEM_OFE_MARCA_CALCULO_ITEMPREC DEFAULT 0 DE ITEM_OFE PARA MARCARLO DESPUES" & vbCrLf
sConsulta = sConsulta & "       UPDATE ITEM_OFE SET MARCA_CALCULO_ITEMPREC =0 WHERE ANYO=@ANYO AND GMN1 =@GMN AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "       --ABRIMOS UN BUCLE CON TODOS LOS ITEMS DEL PROCESO PARA PODER CALCULAR EL PRECIO EN FUNCI?N DE LA CONFIGURACI?N ESTABLECIDA" & vbCrLf
sConsulta = sConsulta & "       IF @ITEM = 0" & vbCrLf
sConsulta = sConsulta & "           DECLARE Cur_ITEM_PROCESO Cursor FOR SELECT I.ID FROM ITEM I WITH(NOLOCK) WHERE I.ANYO = @ANYO AND I.GMN1_PROCE = @GMN AND I.PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "       ELSE" & vbCrLf
sConsulta = sConsulta & "           DECLARE Cur_ITEM_PROCESO Cursor FOR SELECT I.ID FROM ITEM I WITH(NOLOCK) WHERE I.ANYO = @ANYO AND I.GMN1_PROCE = @GMN AND I.PROCE = @PROCE AND I.ID = @ITEM" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       OPEN Cur_ITEM_PROCESO" & vbCrLf
sConsulta = sConsulta & "       FETCH NEXT FROM Cur_ITEM_PROCESO into @ITEM" & vbCrLf
sConsulta = sConsulta & "       WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "       --SE CREA UNA TEMPORAL PARA TRABAJAR" & vbCrLf
sConsulta = sConsulta & "           if object_id('tempdb..#TempPreciosOfertas') IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "               DROP TABLE #TempPreciosOfertas" & vbCrLf
sConsulta = sConsulta & "           CREATE TABLE #TempPreciosOfertas" & vbCrLf
sConsulta = sConsulta & "           (" & vbCrLf
sConsulta = sConsulta & "               ID [int] IDENTITY (1,1) not null," & vbCrLf
sConsulta = sConsulta & "               PRECIO  [float]   NOT NULL ," & vbCrLf
sConsulta = sConsulta & "               ANYO [INT] NOT NULL," & vbCrLf
sConsulta = sConsulta & "               GMN1 [Nvarchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL," & vbCrLf
sConsulta = sConsulta & "               PROCE [INT] NOT NULL," & vbCrLf
sConsulta = sConsulta & "               ITEM [INT] NOT NULL," & vbCrLf
sConsulta = sConsulta & "               OFE [int] NOT NULL," & vbCrLf
sConsulta = sConsulta & "               PROVE [NVARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL," & vbCrLf
sConsulta = sConsulta & "           );" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "           --MEDIANA DE LAS PRIMERAS OFERTAS" & vbCrLf
sConsulta = sConsulta & "           IF @MODO = 1" & vbCrLf
sConsulta = sConsulta & "               BEGIN" & vbCrLf
sConsulta = sConsulta & "                   insert into #TempPreciosOfertas SELECT IO.PRECIO, IO.ANYO, IO.GMN1,IO.PROCE,IO.ITEM, IO.NUM,IO.PROVE FROM ITEM_OFE IO WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                               INNER JOIN PROCE_OFE PO WITH(NOLOCK) ON IO.ANYO = PO.ANYO AND IO.GMN1 =PO.GMN1 AND IO.PROCE= PO.PROCE AND PO.PROVE = IO.PROVE AND IO.NUM = PO.OFE" & vbCrLf
sConsulta = sConsulta & "                               INNER JOIN OFEEST OE WITH(NOLOCK) ON OE.COD = PO.EST AND OE.COMP = 1 AND OE.ADJ = 1" & vbCrLf
sConsulta = sConsulta & "                               WHERE IO.ANYO=@ANYO AND IO.GMN1 = @GMN AND IO.PROCE =@PROCE AND IO.ITEM=@ITEM" & vbCrLf
sConsulta = sConsulta & "                                   AND IO.NUM = (SELECT  MIN(PO.OFE) PRIMERA_OFERTA FROM  PROCE_OFE PO WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                                               INNER JOIN OFEEST OE WITH(NOLOCK) ON OE.COD = PO.EST AND OE.COMP = 1 AND OE.ADJ = 1" & vbCrLf
sConsulta = sConsulta & "                                               WHERE PO.ANYO=@ANYO AND PO.GMN1 = @GMN AND PO.PROCE =@PROCE AND PROVE = IO.PROVE AND IO.PRECIO IS NOT NULL  GROUP BY  PO.PROVE) ORDER BY IO.PRECIO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                   SELECT @REGISTROS= COUNT(*) FROM #TempPreciosOfertas " & vbCrLf
sConsulta = sConsulta & "                   SELECT @PRECIO_ITEM=AVG(precio) from #TempPreciosOfertas T1 with(nolock)" & vbCrLf
sConsulta = sConsulta & "                   INNER JOIN (SELECT AVG (id) num FROM #TempPreciosOfertas" & vbCrLf
sConsulta = sConsulta & "                               UNION" & vbCrLf
sConsulta = sConsulta & "                               SELECT id num FROM #TempPreciosOfertas WHERE ID = ((@REGISTROS/2)+1) AND @REGISTROS % 2 = 0) T2 on T2.num = t1.id" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                   --MARCAMOS LOS REGISTROS DE ITEM:_OFE CON LOS QUE HEMOS REALIZADO EL C?LCULO" & vbCrLf
sConsulta = sConsulta & "                   UPDATE ITEM_OFE SET MARCA_CALCULO_ITEMPREC = 1 FROM ITEM_OFE IO WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                   INNER JOIN #TempPreciosOfertas T1 with(nolock) ON T1.ANYO =IO.ANYO AND T1.GMN1 = IO.GMN1 AND T1.PROCE= IO.PROCE AND T1.ITEM = IO.ITEM AND T1.OFE =IO.NUM AND T1.PROVE =IO.PROVE" & vbCrLf
sConsulta = sConsulta & "                   INNER JOIN (SELECT AVG (id) num FROM #TempPreciosOfertas" & vbCrLf
sConsulta = sConsulta & "                               UNION" & vbCrLf
sConsulta = sConsulta & "                               SELECT id num FROM #TempPreciosOfertas WHERE ID = ((@REGISTROS/2)+1) AND @REGISTROS % 2 = 0) T2 on T2.num = t1.id" & vbCrLf
sConsulta = sConsulta & "               END" & vbCrLf
sConsulta = sConsulta & "           ELSE" & vbCrLf
sConsulta = sConsulta & "           --MEDIA DEL N?MERO DE OFERTAS CONFIGURADAS" & vbCrLf
sConsulta = sConsulta & "           IF @MODO = 2" & vbCrLf
sConsulta = sConsulta & "               BEGIN" & vbCrLf
sConsulta = sConsulta & "                   --En caso de que est? configurado a 0 se coger? la media de todos los items" & vbCrLf
sConsulta = sConsulta & "                   --Si no es 0 se har? la media del n?mero configurado recogido en la variable @MODO_NUM" & vbCrLf
sConsulta = sConsulta & "                   IF @MODO_NUM = 0" & vbCrLf
sConsulta = sConsulta & "                       BEGIN" & vbCrLf
sConsulta = sConsulta & "                           INSERT INTO #TempPreciosOfertas SELECT IO.PRECIO, IO.ANYO, IO.GMN1,IO.PROCE,IO.ITEM, IO.NUM,IO.PROVE FROM ITEM_OFE IO WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                               INNER JOIN ITEM I WITH(NOLOCK) ON I.ANYO = IO.ANYO AND I.GMN1_PROCE =IO.GMN1 AND I.PROCE = IO.PROCE AND I.ID = IO.ITEM" & vbCrLf
sConsulta = sConsulta & "                               INNER JOIN PROCE_OFE PO WITH(NOLOCK) ON PO.ANYO = I.ANYO AND PO.GMN1 =I.GMN1_PROCE AND PO.PROCE =I.PROCE AND PO.PROVE = IO.PROVE AND IO.NUM = PO.OFE" & vbCrLf
sConsulta = sConsulta & "                               INNER JOIN OFEEST OE WITH(NOLOCK) ON OE.COD = PO.EST AND OE.COMP = 1 AND OE.ADJ = 1" & vbCrLf
sConsulta = sConsulta & "                               WHERE I.ANYO = @ANYO AND I.GMN1_PROCE = @GMN AND I.PROCE = @PROCE AND I.ID = @ITEM AND IO.PRECIO IS NOT NULL ORDER BY IO.PRECIO" & vbCrLf
sConsulta = sConsulta & "                       END" & vbCrLf
sConsulta = sConsulta & "                   ELSE" & vbCrLf
sConsulta = sConsulta & "                       BEGIN" & vbCrLf
sConsulta = sConsulta & "                           INSERT INTO #TempPreciosOfertas SELECT TOP (@MODO_NUM) IO.PRECIO, IO.ANYO, IO.GMN1,IO.PROCE,IO.ITEM, IO.NUM,IO.PROVE FROM ITEM_OFE IO WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                               INNER JOIN ITEM I WITH(NOLOCK) ON I.ANYO = IO.ANYO AND I.GMN1_PROCE =IO.GMN1 AND I.PROCE = IO.PROCE AND I.ID = IO.ITEM" & vbCrLf
sConsulta = sConsulta & "                               INNER JOIN PROCE_OFE PO WITH(NOLOCK) ON PO.ANYO = I.ANYO AND PO.GMN1 =I.GMN1_PROCE AND PO.PROCE =I.PROCE AND PO.PROVE = IO.PROVE AND IO.NUM = PO.OFE" & vbCrLf
sConsulta = sConsulta & "                               INNER JOIN OFEEST OE WITH(NOLOCK) ON OE.COD = PO.EST AND OE.COMP = 1 AND OE.ADJ = 1" & vbCrLf
sConsulta = sConsulta & "                               WHERE I.ANYO = @ANYO AND I.GMN1_PROCE = @GMN AND I.PROCE = @PROCE AND I.ID = @ITEM AND IO.PRECIO IS NOT NULL ORDER BY IO.PRECIO" & vbCrLf
sConsulta = sConsulta & "                       END" & vbCrLf
sConsulta = sConsulta & "                   --CALCULAMOS EL PRECIO EN BASE A LA TABLA TEMPORAL" & vbCrLf
sConsulta = sConsulta & "                   SELECT @PRECIO_ITEM=(SUM(PRECIO)/COUNT(PRECIO)) from (SELECT IO.PRECIO , IO.ITEM FROM #TempPreciosOfertas IO WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                       INNER JOIN ITEM I WITH(NOLOCK) ON I.ANYO = IO.ANYO AND I.GMN1_PROCE =IO.GMN1 AND I.PROCE = IO.PROCE AND I.ID = IO.ITEM" & vbCrLf
sConsulta = sConsulta & "                       INNER JOIN PROCE_OFE PO WITH(NOLOCK) ON PO.ANYO = I.ANYO AND PO.GMN1 =I.GMN1_PROCE AND PO.PROCE =I.PROCE AND PO.PROVE = IO.PROVE AND IO.OFE = PO.OFE" & vbCrLf
sConsulta = sConsulta & "                       INNER JOIN OFEEST OE WITH(NOLOCK) ON OE.COD = PO.EST AND OE.COMP = 1 AND OE.ADJ = 1" & vbCrLf
sConsulta = sConsulta & "                       WHERE I.ANYO = @ANYO AND I.GMN1_PROCE = @GMN AND I.PROCE = @PROCE AND I.ID = @ITEM ) A GROUP BY ITEM" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                   --MARCAMOS LOS REGISTROS DE ITEM:_OFE CON LOS QUE HEMOS REALIZADO EL C?LCULO" & vbCrLf
sConsulta = sConsulta & "                   UPDATE ITEM_OFE SET MARCA_CALCULO_ITEMPREC = 1 FROM ITEM_OFE IO WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                       INNER JOIN #TempPreciosOfertas T1 WITH(NOLOCK) ON T1.ANYO =IO.ANYO AND T1.GMN1 = IO.GMN1 AND T1.PROCE= IO.PROCE AND T1.ITEM = IO.ITEM AND T1.OFE =IO.NUM AND T1.PROVE =IO.PROVE" & vbCrLf
sConsulta = sConsulta & "               END" & vbCrLf
sConsulta = sConsulta & "               --Actualizamos los precios del item" & vbCrLf
sConsulta = sConsulta & "               UPDATE ITEM SET PREC = @PRECIO_ITEM, PRES = @PRECIO_ITEM * PRES WHERE ANYO = @ANYO AND GMN1_PROCE = @GMN AND PROCE = @PROCE AND ID = @ITEM" & vbCrLf
sConsulta = sConsulta & "               DROP TABLE #TempPreciosOfertas" & vbCrLf
sConsulta = sConsulta & "           FETCH NEXT FROM Cur_ITEM_PROCESO INTO @ITEM" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "       Close Cur_ITEM_PROCESO" & vbCrLf
sConsulta = sConsulta & "       DEALLOCATE Cur_ITEM_PROCESO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       COMMIT TRANSACTION" & vbCrLf
sConsulta = sConsulta & "       SET XACT_ABORT OFF" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion3_3_3501_A3_3_3502() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim oADOConnection As ADODB.Connection
    Dim sConsulta As String

    On Error GoTo Error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Actualiza Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    frmProgreso.ProgressBar1.Refresh
    V_3_5_Storeds_002

    sConsulta = "UPDATE VERSION SET NUM ='3.00.35.002'" & ", FECHA=CONVERT(datetime,'" & Format(Now, "mm-dd-yyyy hh:mm:ss") & "',110), PRODVER='3.5.1'"
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False

    CodigoDeActualizacion3_3_3501_A3_3_3502 = True
    Exit Function

Error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If

    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    CodigoDeActualizacion3_3_3501_A3_3_3502 = False
End Function

Private Sub V_3_5_Storeds_002()
    Dim sConsulta As String

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSGS_DEVOLVER_PARAM_GEN]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSGS_DEVOLVER_PARAM_GEN]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSGS_DEVOLVER_PARAM_GEN]" & vbCrLf
sConsulta = sConsulta & "@IDI VARCHAR(3)" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON;" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT NEO,NEM,NEPP,NEPC,INSTWEB,LOCKTIMEOUT,FUP,SUBASTA,NEP3,NEP4,APROVISION,PEDIDO,INTEGRACION," & vbCrLf
sConsulta = sConsulta & "ADMIN_PUBLICA,TIMEOUT_BL_PROCE,PEDIDO_LIBRE,WIN_SEC,REMOTEGROUP,TRASPASO_ADJ_ERP," & vbCrLf
sConsulta = sConsulta & "TRASPASO_PED_ERP,TRASLADO_APROV_ERP,READER,HELP_PDF,SINC_MAT,CONSERVAR_NUMPEDIDO," & vbCrLf
sConsulta = sConsulta & "PROCE_PROVE_GRUPOS,ACCESO_FSGS,ACCESO_FSWS,ACCESO_FSQA,VARCAL_MAT," & vbCrLf
sConsulta = sConsulta & "INVITADO,FSQA_REVISOR,REUVIR,USAR_ART4_UON,USAR_ORGCOMPRAS,TRASPASO_ART_PLANIF," & vbCrLf
sConsulta = sConsulta & "PM_CARGAR_ALL_GRUPOS,MULTIMAT, ARTGEN, ACCESO_FSSM, PYMES, POOLS, REUBICAR_AHORROS," & vbCrLf
sConsulta = sConsulta & "URL_WEBSERVICE, HOJA_ADJS_ABIERTO, URL_CHANGE_PWD, LISTADOS_MICRO, ACCESO_FSFA," & vbCrLf
sConsulta = sConsulta & "RECDIR_FECHA,RECDIR_CANT,PEDDIR_PETICIONARIO_SOL,WIN_SEC_WEB,PEDDIRFAC, VER_NUMEXT," & vbCrLf
sConsulta = sConsulta & "URL_PM,URL_PM_ALTACONTRATO, ACCESO_FSGA, URL_SESSIONSERVICE, URL_PM_DETALLECONTRATO," & vbCrLf
sConsulta = sConsulta & "URL_CAMPO_EDITOR,VALIDAGMN,ACCESO_FSCN,URL_CN,IM_FACTRASRECEP,IM_RECEPAUTO," & vbCrLf
sConsulta = sConsulta & "RUTASFULLSTEPWEB,ACCESO_CONTRATO,ACCESO_FSBI,ART_CENTRALES,CHUNK_SIZE,INT_MANT_EST_TRASP_ADJ," & vbCrLf
sConsulta = sConsulta & "      MOSTRAR_RECEP_SALIDA_ALMACEN,EP_MARCAR_ACEP_PROVE,USAR_RMTE_EMP , MARCADOR_CARPETA_PLANTILLAS," & vbCrLf
sConsulta = sConsulta & "      MOSTRAR_FECHA_CONTABLE, ACTIVAR_BORRADO_LOGICO_PEDIDOS" & vbCrLf
sConsulta = sConsulta & "FROM PARGEN_INTERNO WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE ID=1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT NOMBREPROY,RUTAXBAP FROM PARGEN_RUTAS WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT TABLA_INTERMEDIA FROM TABLAS_INTEGRACION_ERP WITH (NOLOCK) WHERE TABLA=8 AND TABLA_INTERMEDIA=1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT MONCEN,PAIDEF,PROVIDEF,DESTDEF,UNIDEF,ROLDEF,PAGDEF,ESTINI,ESTWEB,IDIOMA,OFFSET,MAX_ADJUN,GR_COD,GR_DEN," & vbCrLf
sConsulta = sConsulta & "GR_DESCR,SUGERIR_ART,TSRUTALOCAL,TSRUTAUNIDAD,TSUNIDAD,TIMEZONE" & vbCrLf
sConsulta = sConsulta & "FROM PARGEN_DEF WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN IDIOMAS WITH (NOLOCK) ON PARGEN_DEF.IDIOMA=IDIOMAS.COD" & vbCrLf
sConsulta = sConsulta & "WHERE ID=1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT ID,FSP_SRV,FSP_BD,FSP_CIA,IDIOMA FROM PARGEN_PORT WITH (NOLOCK) WHERE ID=1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT VOL_MAX_ADJ_DIR,IMP_MAX_ADJ_DIR,OBLPP,OBLPC,DIAREU,PERIODICIREU,TIEMPOREU,COMIENZOREU,CRITEORDREU1," & vbCrLf
sConsulta = sConsulta & "CRITEORDREU2,CONVSOLORESP,OBLPROVEEQP,SENT_ORD_CAL1,SENT_ORD_CAL2,SENT_ORD_CAL3,PERMITEMSINCOD,PERMADJSINHOM," & vbCrLf
sConsulta = sConsulta & "LOGPREBLOQ,ACTIVLOG,URLWEBPROVE,ADJORDEN,RPTPATH,SOLICIPATH,BUZPER,BUZLEC,ARTIC_ADJESP,USAPRES1,USAPRES2," & vbCrLf
sConsulta = sConsulta & "USAPRES3,USAPRES4,OBLPROVEMAT,OBLPRES3,OBLPRES4, PERIODOAVISODES,PERIODOAVISOADJ,AVISODES,AVISOADJ," & vbCrLf
sConsulta = sConsulta & "DEST,PAG,FECSUM,PROVE,DIST,PRESANU1,PRESANU2,PRES1,PRES2,PROCE_ESP,GRUPO_ESP,ITEM_ESP,SOLCANTMAX,PRECALTER," & vbCrLf
sConsulta = sConsulta & "PROCE_POND,USAPOND,OFE_ADJUN,GRUPO_ADJUN,ITEM_ADJUN,SUBASTA_ENVREC,SUBASTA_ACTENV,SUBASTA_ENVCIE," & vbCrLf
sConsulta = sConsulta & "SUBASTA_ATIEMPO,PRESDISTAUT,NIVDIST,NIVPP,NIVPC,NIVPRES3,NIVPRES4,SOLICIT,PROCE_ADMINPUB,PUBLICARFINSUM ," & vbCrLf
sConsulta = sConsulta & "SELPOSITIVA,PRES_PLANIFICADO, ACTCAMPO1, ACTCAMPO2, ACTCAMPO3, ACTCAMPO4,AVISODESPUB, AVISOSOLONOOFE," & vbCrLf
sConsulta = sConsulta & "AVISONOOFERTAN, ANTELACIONAVISO, UNSOLOPEDIDO, ADJNOTIF, CAMBIARMON, SOLIC_PROC_ADJDIR, IMP_SOLIC_PROC_ADJDIR," & vbCrLf
sConsulta = sConsulta & "VALIDEZ_SOLIC_OFE,OBLUNIMED, OBLASIGUONART, CTRLCAMBIOFECSUMIN, EMAIL_PORTAL_COMP_RESP," & vbCrLf
sConsulta = sConsulta & "EMAIL_PORTAL_COMP_ASIGNADO, OBLASIG_PRES_UON_ART_APER, OBLASIG_PRES_UON_ART_PEDIR,HISTORICO_PWD," & vbCrLf
sConsulta = sConsulta & "COMPLEJIDAD_PWD, EDAD_MIN_PWD, EDAD_MAX_PWD, MIN_SIZE_PWD,SUM_RETROACTIVO, ACT_APE_MAIL, ACT_PREADJ_MAIL," & vbCrLf
sConsulta = sConsulta & "ACT_ADJ_MAIL,PED_EMAIL,PED_EMAIL_MODO,PED_PUB_PORTAL,PED_PUB_PORTAL_PED_ABIERTOS,ACT_ANULACION_MAIL," & vbCrLf
sConsulta = sConsulta & "      CAL_LIN_BASE,NUM_MEJ_OFE" & vbCrLf
sConsulta = sConsulta & "FROM PARGEN_GEST WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE ID=1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT SUBASTA,SUBVERPROVEGAN,SUBVERDETPUJAS,SUBMINESPERACIERRE,SUBBAJMINGAN,SUBVERPRECIOGAN," & vbCrLf
sConsulta = sConsulta & "SUBTIPO,SUBMODO,SUBDURACION,SUBPUBLICAR,SUBMINESPERACIERRE,SUBNOTIFEVENTOS,SUBMINJAPONESA," & vbCrLf
sConsulta = sConsulta & "SUBVERPROVEGAN,SUBVERPRECIOGAN,SUBVERDETPUJAS,SUBVERDESDEPRIMPUJA,SUBBAJMINGAN,SUBBAJMINGANTIPO," & vbCrLf
sConsulta = sConsulta & "SUBBAJMINGANPROCVAL,SUBBAJMINGANGRUPOVAL,SUBBAJMINGANITEMVAL,SUBBAJMINPROVE,SUBBAJMINPROVETIPO," & vbCrLf
sConsulta = sConsulta & "SUBBAJMINPROVEPROCVAL,SUBBAJMINPROVEGRUPOVAL,SUBBAJMINPROVEITEMVAL,SUBTEXTOFIN" & vbCrLf
sConsulta = sConsulta & "FROM PARGEN_PROCE WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE ID=1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT ID,TIPOCLTEMAIL,SERVIDOR,PUERTO,SSL,AUTENTICACION,BASICA_GS,USU,PWD,USAR_CUENTA,CUENTAMAIL,MOSTRARMAIL," & vbCrLf
sConsulta = sConsulta & "ACUSERECIBO,FECPWD,TIPOMAIL" & vbCrLf
sConsulta = sConsulta & "FROM PARGEN_MAIL WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE ID=1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT ELIMLINCATALOG,OBLHOMPEDIDOS,OBLCODPEDIDO,OBLCODPEDDIR,CAT_ADJESP_ART,CAT_ADJESP_PROVE," & vbCrLf
sConsulta = sConsulta & "OBL_DIST_PEDIR,PEDPRES1,PEDPRES2,PEDPRES3,PEDPRES4," & vbCrLf
sConsulta = sConsulta & "DEFPRESITEM,SINCPRESITEM,OBLPRES1,OBLPRES2,OBLPRES3,OBLPRES4,NIVPRES1,NIVPRES2," & vbCrLf
sConsulta = sConsulta & "NIVPRES3,NIVPRES4,DIRENTRCABPED,DESGLPEDCENAPROV,PEDIDOABIERTO,VIAPAG,OBLCODPEDIDO_OBL,CODRECEPERP," & vbCrLf
sConsulta = sConsulta & "      ACT_PED_ABIERTO,PEDMAIL_CC_EMAILS,PEDMAIL_CC_PETICIONARIO,PEDMAIL_CC_COMPRADOR,BLOQCODPEDIDO,OBLFECENTREGA" & vbCrLf
sConsulta = sConsulta & "FROM PARGEN_PED WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE ID=1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT PETOFEDOT,PETOFECARTADOT,PETOFEMAILDOT,PETOFEMAILSUBJECT,PETOFEWEBDOT,CONVDOT,CONVMAILHTMLSPA," & vbCrLf
sConsulta = sConsulta & "CONVMAILHTMLENG,CONVMAILHTMLGER,CONVMAILHTMLFRA,CONVMAILTEXTSPA,CONVMAILTEXTENG,CONVMAILTEXTGER," & vbCrLf
sConsulta = sConsulta & "CONVMAILTEXTFRA,CONVMAILSUBJECTSPA,CONVMAILSUBJECTENG,CONVMAILSUBJECTGER,CONVMAILSUBJECTFRA,AGENDADOT," & vbCrLf
sConsulta = sConsulta & "HOJAADJDETPROCEDOT,HOJAADJDIRDOT,ACTADOT,OBJMAILSUBJECT,OBJDOT,OBJCARTADOT,OBJMAILDOT,OBJWEBDOT,ADJDOT," & vbCrLf
sConsulta = sConsulta & "ADJCARTADOT,ADJMAILDOT,ADJMAILSUBJECT,ADJNOCARTADOT,ADJNOMAILDOT,ADJNOMAILSUBJECT,COMPARATIVADOT," & vbCrLf
sConsulta = sConsulta & "COMPARATIVAITEMDOT,COMPARATIVAQA,LONGCABCOMP,LONGCABCOMPITEM,LONGCABCOMPQA,PEDDANUCARTADOT,PEDDANUMAILDOT," & vbCrLf
sConsulta = sConsulta & "PEDDANUMAILSUBJECT,PEDDEMICARTADOT,PEDDEMIMAILDOT,PEDDEMIMAILSUBJECT,PEDDRECOKCARTADOT,PEDDRECOKMAILDOT," & vbCrLf
sConsulta = sConsulta & "PEDDRECKOCARTADOT,PEDDRECKOMAILDOT,PEDDRECMAILSUBJECT,DATEXTMAILDOT,DATEXTXML,DATEXTXLS,PEDIDODOT,PEDRECDOT," & vbCrLf
sConsulta = sConsulta & "PEDIDOXLT,DATEXTMAILSUBJECT,PETSUBDOT,PETSUBCARTADOT,PETSUBMAILDOT,PETSUBWEBDOT,PETSUBMAILSUBJECT,ADJNOTIFDOT," & vbCrLf
sConsulta = sConsulta & "ACTADOT2,PEDNOTIFACEP_HTML,PEDNOTIFACEP_TXT,PEDNOTIFSUBJECT, OFEAVISODESPUB_HTML, OFEAVISODESPUB_TXT," & vbCrLf
sConsulta = sConsulta & "OFEAVISODESPUBSUBJECT,ACTATIPO" & vbCrLf
sConsulta = sConsulta & "FROM PARGEN_DOT WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT ID,DEN FROM PARGEN_LIT WITH (NOLOCK) WHERE IDI=@IDI" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion3_3_3502_A3_3_3503() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim oADOConnection As ADODB.Connection
    Dim sConsulta As String

    On Error GoTo Error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Actualiza Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    frmProgreso.ProgressBar1.Refresh
    V_3_5_Storeds_003

    sConsulta = "UPDATE VERSION SET NUM ='3.00.35.003'" & ", FECHA=CONVERT(datetime,'" & Format(Now, "mm-dd-yyyy hh:mm:ss") & "',110), PRODVER='3.5.1'"
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False

    CodigoDeActualizacion3_3_3502_A3_3_3503 = True
    Exit Function

Error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If

    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    CodigoDeActualizacion3_3_3502_A3_3_3503 = False
End Function

Private Sub V_3_5_Storeds_003()
    Dim sConsulta As String

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_NUEVA_OFERTA]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_NUEVA_OFERTA]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[SP_NUEVA_OFERTA] @ANYO int, @GMN1 varchar(50), @PROCE int, @PROVE varchar(50), @OFE int, @ULT SMALLINT,@MONNEW NVARCHAR(50),@CAMBIONEW FLOAT  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @MAXOFE INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @CAMBIOOLD FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @MONOLD NVARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "--  ESTA SP SOLO SE PUEDE LLAMAR DESDE EL TRIGGER DE INSERCION DE PROCE_OFE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @OFE=-1" & vbCrLf
sConsulta = sConsulta & "SET @MAXOFE = ( SELECT MAX(OFE) FROM PROCE_OFE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE)" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @MAXOFE = ( SELECT MAX(OFE) FROM PROCE_OFE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE AND OFE < @OFE)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF (@MAXOFE > 0 ) --NO ES LA PRIMERA OFERTA QUE METE EL PROVEEDOR AS? QUE COPIAMOS LA ANTERIOR" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "--    Leo la moneda de la oferta que voy a copiar" & vbCrLf
sConsulta = sConsulta & "SELECT @MONOLD=MON,@CAMBIOOLD=CAMBIO FROM PROCE_OFE WITH (NOLOCK) WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE AND OFE =@MAXOFE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO PROCE_OFE_ADJUN (ANYO, GMN1, PROCE, PROVE, OFE, ID,NOM, COM, IDPORTAL,DATASIZE)" & vbCrLf
sConsulta = sConsulta & "SELECT ANYO, GMN1, PROCE, PROVE, @OFE, ID,NOM ,COM, IDPORTAL,DATASIZE" & vbCrLf
sConsulta = sConsulta & "FROM PROCE_OFE_ADJUN WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE = @PROVE AND OFE =@MAXOFE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO OFE_ATRIB (ANYO,GMN1,PROCE,PROVE,OFE,ATRIB_ID,VALOR_NUM ,VALOR_TEXT ,VALOR_FEC ,VALOR_BOOL ,VALOR_POND,IMPPARCIAL)" & vbCrLf
sConsulta = sConsulta & "SELECT ANYO,GMN1,PROCE,PROVE,@OFE,ATRIB_ID,VALOR_NUM ,VALOR_TEXT ,VALOR_FEC ,VALOR_BOOL ,VALOR_POND ,IMPPARCIAL" & vbCrLf
sConsulta = sConsulta & "FROM OFE_ATRIB WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE = @PROVE AND OFE =@MAXOFE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO GRUPO_OFE (ANYO,GMN1,PROCE,GRUPO,PROVE,OFE,OBSADJUN,IMPORTE,IMPORTE_BRUTO)" & vbCrLf
sConsulta = sConsulta & "SELECT ANYO,GMN1,PROCE,GRUPO,PROVE,@OFE,OBSADJUN,IMPORTE,IMPORTE_BRUTO" & vbCrLf
sConsulta = sConsulta & "FROM GRUPO_OFE WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE AND OFE=@MAXOFE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO OFE_GR_ADJUN (ANYO,GMN1,PROCE,GRUPO,PROVE,OFE,ID,NOM ,COM ,IDPORTAL,DATASIZE)" & vbCrLf
sConsulta = sConsulta & "SELECT ANYO,GMN1,PROCE,GRUPO,PROVE,@OFE,ID,NOM ,COM ,IDPORTAL,DATASIZE" & vbCrLf
sConsulta = sConsulta & "FROM OFE_GR_ADJUN WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE = @PROVE AND OFE =@MAXOFE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO OFE_GR_ATRIB (ANYO,GMN1,PROCE,GRUPO,PROVE,OFE,ATRIB_ID,VALOR_NUM ,VALOR_TEXT ,VALOR_FEC ,VALOR_BOOL ,VALOR_POND )" & vbCrLf
sConsulta = sConsulta & "SELECT ANYO,GMN1,PROCE,GRUPO,PROVE,@OFE,ATRIB_ID,VALOR_NUM ,VALOR_TEXT ,VALOR_FEC ,VALOR_BOOL ,VALOR_POND" & vbCrLf
sConsulta = sConsulta & "FROM OFE_GR_ATRIB WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE AND OFE=@MAXOFE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ULT = 1" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_OFE SET ULT=0 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE AND NUM=@MAXOFE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--        IF @MONOLD = @MONNEW" & vbCrLf
sConsulta = sConsulta & "/*            INSERT INTO ITEM_OFE (ANYO,GMN1,PROCE,ITEM,PROVE,NUM,PRECIO,PRECIO2,PRECIO3,USAR,CANTMAX,OBSADJUN,ULT,COMENT1,COMENT2,COMENT3)" & vbCrLf
sConsulta = sConsulta & "SELECT ANYO,GMN1,PROCE,ITEM,PROVE,@OFE,PRECIO,PRECIO2,PRECIO3,USAR,CANTMAX,OBSADJUN,@ULT,COMENT1,COMENT2,COMENT3" & vbCrLf
sConsulta = sConsulta & "FROM ITEM_OFE" & vbCrLf
sConsulta = sConsulta & "WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE AND NUM=@MAXOFE" & vbCrLf
sConsulta = sConsulta & "*/" & vbCrLf
sConsulta = sConsulta & "INSERT INTO ITEM_OFE (ANYO,GMN1,PROCE,ITEM,PROVE,NUM,PRECIO,PRECIO2,PRECIO3,USAR,CANTMAX,OBSADJUN,ULT,COMENT1,COMENT2,COMENT3,PREC_VALIDO,IMPORTE)" & vbCrLf
sConsulta = sConsulta & "SELECT ITEM_OFE.ANYO,ITEM_OFE.GMN1,ITEM_OFE.PROCE,ITEM_OFE.ITEM,ITEM_OFE.PROVE,@OFE,ITEM_OFE.PRECIO,ITEM_OFE.PRECIO2,ITEM_OFE.PRECIO3,ITEM_OFE.USAR,ITEM_OFE.CANTMAX,ITEM_OFE.OBSADJUN,@ULT,ITEM_OFE.COMENT1,ITEM_OFE.COMENT2,ITEM_OFE.COMENT3,ITEM_OFE.PREC_VALIDO,ITEM_OFE.IMPORTE" & vbCrLf
sConsulta = sConsulta & "FROM ITEM_OFE WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN ITEM WITH (NOLOCK) ON ITEM_OFE.ANYO=ITEM.ANYO AND ITEM_OFE.GMN1=ITEM.GMN1_PROCE AND ITEM_OFE.PROCE = ITEM.PROCE AND ITEM_OFE.ITEM = ITEM.ID" & vbCrLf
sConsulta = sConsulta & "WHERE ITEM_OFE.ANYO=@ANYO AND ITEM_OFE.GMN1=@GMN1 AND ITEM_OFE.PROCE=@PROCE AND ITEM_OFE.PROVE=@PROVE AND ITEM_OFE.NUM=@MAXOFE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "/*        ELSE" & vbCrLf
sConsulta = sConsulta & "INSERT INTO ITEM_OFE (ANYO,GMN1,PROCE,ITEM,PROVE,NUM,PRECIO,PRECIO2,PRECIO3,USAR,CANTMAX,OBSADJUN,ULT)" & vbCrLf
sConsulta = sConsulta & "SELECT ANYO,GMN1,PROCE,ITEM,PROVE,@OFE,((PRECIO/@CAMBIOOLD)*@CAMBIONEW),((PRECIO2/@CAMBIOOLD)*@CAMBIONEW),((PRECIO3/@CAMBIOOLD)*@CAMBIONEW),USAR,CANTMAX,OBSADJUN,@ULT" & vbCrLf
sConsulta = sConsulta & "FROM ITEM_OFE" & vbCrLf
sConsulta = sConsulta & "WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE AND NUM=@MAXOFE" & vbCrLf
sConsulta = sConsulta & "*/" & vbCrLf
sConsulta = sConsulta & "INSERT INTO OFE_ITEM_ADJUN (ANYO,GMN1,PROCE,ITEM,PROVE,OFE,ID,NOM ,COM ,IDPORTAL,DATASIZE)" & vbCrLf
sConsulta = sConsulta & "SELECT ANYO,GMN1,PROCE,ITEM,PROVE,@OFE,ID,NOM ,COM ,IDPORTAL,DATASIZE" & vbCrLf
sConsulta = sConsulta & "FROM OFE_ITEM_ADJUN WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE AND OFE=@MAXOFE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO OFE_ITEM_ATRIB (ANYO,GMN1,PROCE,ITEM,PROVE,OFE,ATRIB_ID,VALOR_NUM ,VALOR_TEXT ,VALOR_FEC ,VALOR_BOOL ,VALOR_POND )" & vbCrLf
sConsulta = sConsulta & "SELECT ANYO,GMN1,PROCE,ITEM,PROVE,@OFE,ATRIB_ID,VALOR_NUM ,VALOR_TEXT ,VALOR_FEC ,VALOR_BOOL ,VALOR_POND" & vbCrLf
sConsulta = sConsulta & "FROM OFE_ITEM_ATRIB WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE AND OFE=@MAXOFE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO OFE_GR_COSTESDESC (ANYO,GMN1,PROCE,GRUPO,PROVE,OFE,ATRIB_ID,IMPPARCIAL)" & vbCrLf
sConsulta = sConsulta & "SELECT ANYO,GMN1,PROCE,GRUPO,PROVE,@OFE,ATRIB_ID,IMPPARCIAL" & vbCrLf
sConsulta = sConsulta & "FROM OFE_GR_COSTESDESC WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE AND OFE=@MAXOFE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @MONOLD <> @MONNEW" & vbCrLf
sConsulta = sConsulta & "EXEC SP_MODIF_MON_OFE @ANYO, @GMN1, @PROCE, @PROVE, @OFE, @CAMBIOOLD,@CAMBIONEW" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ELSE --Es la primera oferta" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "INSERT INTO GRUPO_OFE (ANYO,GMN1,PROCE,GRUPO,PROVE,OFE)" & vbCrLf
sConsulta = sConsulta & "SELECT @ANYO,@GMN1,@PROCE,ID, @PROVE,@OFE" & vbCrLf
sConsulta = sConsulta & "FROM PROCE_GRUPO WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO ITEM_OFE (ANYO,GMN1,PROCE,ITEM,PROVE,NUM,PRECIO,PRECIO2,PRECIO3,CANTMAX,ULT,FECACT)" & vbCrLf
sConsulta = sConsulta & "SELECT @ANYO,@GMN1,@PROCE,ID,@PROVE,@OFE,NULL,NULL,NULL,NULL,@ULT,NULL" & vbCrLf
sConsulta = sConsulta & "FROM ITEM WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE ANYO=@ANYO AND GMN1_PROCE=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET O=(O+1) WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "     EXEC FSN_RECALCULAR_PRECIO_ITEM @ANYO, @GMN1, @PROCE, 0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion3_3_3503_A3_3_3504() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim oADOConnection As ADODB.Connection
    Dim sConsulta As String

    On Error GoTo Error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Actualiza Tablas"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    frmProgreso.ProgressBar1.Refresh
    V_3_5_Tablas_004
    
    frmProgreso.lblDetalle = "Actualiza Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    frmProgreso.ProgressBar1.Refresh
    V_3_5_Storeds_004
    V_3_5_Storeds_004_1

    sConsulta = "UPDATE VERSION SET NUM ='3.00.35.004'" & ", FECHA=CONVERT(datetime,'" & Format(Now, "mm-dd-yyyy hh:mm:ss") & "',110), PRODVER='3.5.1'"
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False

    CodigoDeActualizacion3_3_3503_A3_3_3504 = True
    Exit Function

Error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If

    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    CodigoDeActualizacion3_3_3503_A3_3_3504 = False
End Function


Private Sub V_3_5_Storeds_004()
    Dim sConsulta As String


sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSN_ESCENARIOS_GET_ESCENARIOS_PROVEEDOR]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSN_ESCENARIOS_GET_ESCENARIOS_PROVEEDOR]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSN_ESCENARIOS_GET_ESCENARIOS_PROVEEDOR]" & vbCrLf
sConsulta = sConsulta & "   @TIPOVISOR INT," & vbCrLf
sConsulta = sConsulta & "   @IDI NVARCHAR(40)," & vbCrLf
sConsulta = sConsulta & "   @IDESCENARIO INT OUTPUT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "BEGIN  " & vbCrLf
sConsulta = sConsulta & "   SET NOCOUNT ON;     " & vbCrLf
sConsulta = sConsulta & "   DECLARE @VISORESC INT" & vbCrLf
sConsulta = sConsulta & "   IF @TIPOVISOR=0" & vbCrLf
sConsulta = sConsulta & "SET @VISORESC=1" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @VISORESC=@TIPOVISOR" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--ESCENARIOS" & vbCrLf
sConsulta = sConsulta & "   SELECT ID,NOMBRE,E.APLICATODAS" & vbCrLf
sConsulta = sConsulta & "   FROM ESCENARIO E WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   WHERE E.PORTAL=1 AND E.TIPOVISOR_PORTAL=@VISORESC AND (@IDESCENARIO=0 OR E.ID=@IDESCENARIO)" & vbCrLf
sConsulta = sConsulta & "   ORDER BY NOMBRE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   DECLARE @SQL NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "   --SOLICITUDES ASIGNADOS A ESTE ESCENARIO" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = 'SELECT ESF.ESCENARIO,ESF.SOLICITUD,ESF.FORMULARIO, S.DEN_' + @IDI +' AS DEN" & vbCrLf
sConsulta = sConsulta & "FROM ESCENARIO_SOLICITUD_FORMULARIO ESF WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN SOLICITUD S WITH(NOLOCK) ON ESF.SOLICITUD=S.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN ESCENARIO E WITH(NOLOCK) ON E.ID=ESF.ESCENARIO AND (@IDESCENARIO=0 OR E.ID=@IDESCENARIO) '" & vbCrLf
sConsulta = sConsulta & "   IF @TIPOVISOR=0" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'AND E.TIPOVISOR_PORTAL=1 '" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'AND E.TIPOVISOR_PORTAL=@TIPOVISOR '" & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL, N'@IDESCENARIO INT,@TIPOVISOR INT', @IDESCENARIO=@IDESCENARIO, @TIPOVISOR=@TIPOVISOR" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   --FILTROS" & vbCrLf
sConsulta = sConsulta & "   SELECT F.ID,F.ESCENARIO,F.FORMULAAVANZADA,F.ERROR_FORMULA,F.NOMBRE" & vbCrLf
sConsulta = sConsulta & "   FROM FILTRO F WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN ESCENARIO E WITH(NOLOCK) ON E.ID=F.ESCENARIO AND E.TIPOVISOR_PORTAL=@VISORESC AND (@IDESCENARIO=0 OR E.ID=@IDESCENARIO)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   --CAMPOS IMPLICADOS EN FILTRO" & vbCrLf
sConsulta = sConsulta & "   SELECT FC.FILTRO,F.ESCENARIO,FC.CAMPO,FC.ESGENERAL,FC.VISIBLE,FC.ELIMINADO,FCP.INTRO,FCP.SUBTIPO,FCP.TIPO_CAMPO_GS," & vbCrLf
sConsulta = sConsulta & "       FCP.DEN_SPA,FCP.DEN_ENG,FCP.DEN_GER,FCP.DEN_FRA,D.CAMPO_PADRE" & vbCrLf
sConsulta = sConsulta & "   FROM FILTRO_CAMPOS FC WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN FILTRO F WITH(NOLOCK) ON F.ID=FC.FILTRO AND FC.ESGENERAL=0" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN ESCENARIO E WITH(NOLOCK) ON F.ESCENARIO = E.ID AND E.TIPOVISOR_PORTAL=@VISORESC AND (@IDESCENARIO=0 OR E.ID=@IDESCENARIO)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN FORM_CAMPO FCP WITH(NOLOCK) ON FCP.ID=FC.CAMPO" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN DESGLOSE D WITH(NOLOCK) ON D.CAMPO_HIJO=FCP.ID" & vbCrLf
sConsulta = sConsulta & "   UNION ALL" & vbCrLf
sConsulta = sConsulta & "   SELECT FC.FILTRO,F.ESCENARIO,FC.CAMPO,FC.ESGENERAL,FC.VISIBLE,FC.ELIMINADO,NULL,NULL,NULL," & vbCrLf
sConsulta = sConsulta & "       NULL,NULL,NULL,NULL,NULL" & vbCrLf
sConsulta = sConsulta & "   FROM FILTRO_CAMPOS FC WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN FILTRO F WITH(NOLOCK) ON F.ID=FC.FILTRO AND FC.ESGENERAL=1" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN ESCENARIO E WITH(NOLOCK) ON F.ESCENARIO = E.ID AND E.TIPOVISOR_PORTAL=@VISORESC AND (@IDESCENARIO=0 OR E.ID=@IDESCENARIO)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--CONDICIONES FORMULA FILTRO" & vbCrLf
sConsulta = sConsulta & "SELECT FC.FILTRO,F.ESCENARIO,FC.ORDEN,FCP.ID CAMPO,FC.ESGENERAL,FC.DENOMINACION,FC.DENOMINACION_BD,FC.OPERADOR,FC.VALOR," & vbCrLf
sConsulta = sConsulta & "FCP.INTRO,FCP.SUBTIPO,FCP.TIPO_CAMPO_GS" & vbCrLf
sConsulta = sConsulta & "FROM FILTRO_CONDICIONES FC WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN FILTRO F WITH(NOLOCK) ON F.ID=FC.FILTRO AND FC.ESGENERAL=0 AND NOT FC.CAMPO=0" & vbCrLf
sConsulta = sConsulta & "INNER JOIN ESCENARIO E WITH(NOLOCK) ON F.ESCENARIO = E.ID AND E.TIPOVISOR_PORTAL=@VISORESC AND (@IDESCENARIO=0 OR E.ID=@IDESCENARIO)" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN FORM_CAMPO FCP WITH(NOLOCK) ON FCP.ID=FC.CAMPO" & vbCrLf
sConsulta = sConsulta & "UNION ALL" & vbCrLf
sConsulta = sConsulta & "SELECT FC.FILTRO,F.ESCENARIO,FC.ORDEN,FC.CAMPO,FC.ESGENERAL,FC.DENOMINACION,FC.DENOMINACION_BD,FC.OPERADOR,FC.VALOR," & vbCrLf
sConsulta = sConsulta & "NULL,NULL,NULL" & vbCrLf
sConsulta = sConsulta & "FROM FILTRO_CONDICIONES FC WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN FILTRO F WITH(NOLOCK) ON F.ID=FC.FILTRO AND (FC.ESGENERAL=1 OR (FC.ESGENERAL=0 AND FC.CAMPO=0))" & vbCrLf
sConsulta = sConsulta & "INNER JOIN ESCENARIO E WITH(NOLOCK) ON F.ESCENARIO = E.ID AND E.TIPOVISOR_PORTAL=@VISORESC AND (@IDESCENARIO=0 OR E.ID=@IDESCENARIO)" & vbCrLf
sConsulta = sConsulta & "ORDER BY FC.ORDEN ASC" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT V.ID,V.ESCENARIO,V.NOMBRE,V.ORDEN" & vbCrLf
sConsulta = sConsulta & "FROM VISTA V WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN ESCENARIO E WITH(NOLOCK) ON E.ID=V.ESCENARIO AND E.TIPOVISOR_PORTAL=@VISORESC AND (@IDESCENARIO=0 OR E.ID=@IDESCENARIO)" & vbCrLf
sConsulta = sConsulta & "ORDER BY V.NOMBRE,V.ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--CAMPOS VISUALIZADOS EN LA VISTA" & vbCrLf
sConsulta = sConsulta & "SELECT VC.VISTA,V.ESCENARIO,VC.CAMPO,VC.ESGENERAL,VC.ESDESGLOSE,VC.NOMBREPERSONALIZADO," & vbCrLf
sConsulta = sConsulta & "ISNULL(VCOU.ANCHO,VC.ANCHO) ANCHO,ISNULL(VCOU.POSICION,VC.POSICION) POSICION," & vbCrLf
sConsulta = sConsulta & "FCP.SUBTIPO,FCP.TIPO_CAMPO_GS,FCP.DEN_SPA,FCP.DEN_ENG,FCP.DEN_GER,FCP.DEN_FRA,D.CAMPO_PADRE," & vbCrLf
sConsulta = sConsulta & "FCP2.DEN_SPA DESG_SPA,FCP2.DEN_ENG DESG_ENG,FCP2.DEN_GER DESG_GER,FCP2.DEN_FRA DESG_FRA" & vbCrLf
sConsulta = sConsulta & "FROM VISTA_CAMPOS VC WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN VISTA V WITH(NOLOCK) ON V.ID=VC.VISTA" & vbCrLf
sConsulta = sConsulta & "INNER JOIN ESCENARIO E WITH(NOLOCK) ON V.ESCENARIO = E.ID AND E.TIPOVISOR_PORTAL=@VISORESC AND (@IDESCENARIO=0 OR E.ID=@IDESCENARIO)" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN FORM_CAMPO FCP WITH(NOLOCK) ON FCP.ID=VC.CAMPO" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN DESGLOSE D WITH(NOLOCK) ON D.CAMPO_HIJO=FCP.ID" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN FORM_CAMPO FCP2 WITH(NOLOCK) ON FCP2.ID=D.CAMPO_PADRE" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN VISTA_CAMPOS_OPCIONES_USU VCOU WITH(NOLOCK) ON VCOU.VISTA=VC.VISTA" & vbCrLf
sConsulta = sConsulta & "AND VCOU.CAMPO=VC.CAMPO AND VCOU.ESGENERAL=VC.ESGENERAL" & vbCrLf
sConsulta = sConsulta & "ORDER BY ESDESGLOSE,POSICION" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--OBTENGO EL ID DEL ESCENARIO DEL QUE OBTENDREMOS LOS DATOS" & vbCrLf
sConsulta = sConsulta & "   IF @IDESCENARIO=0" & vbCrLf
sConsulta = sConsulta & "       SELECT TOP 1 @IDESCENARIO=ID FROM ESCENARIO E WITH(NOLOCK) WHERE E.PORTAL=1 AND E.TIPOVISOR_PORTAL=@VISORESC ORDER BY NOMBRE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_TRASPASAR_OFERTA]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[SP_TRASPASAR_OFERTA]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE  PROCEDURE [dbo].[SP_TRASPASAR_OFERTA] @ID int , @NUM INT OUTPUT, @NUMNUE INT OUTPUT, @NUMAREV INT OUTPUT, @ERROR SMALLINT = NULL OUTPUT, @COMPROBARATRIBS TINYINT = 0  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @NEWOFE INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @NUMOFEWEB AS int" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN1 VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCE INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVE VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @MON NVARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @ADJUN INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDADJUN INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDGS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @CODGRUPO VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDGRUPO SMALLINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ITEM INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @NUMOBJ INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @NUMOBJGS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PORTAL INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @traza VARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @P_ADJUN_FIRMA_DIG_ID INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @P_ADJUN_FIRMA_DIG_NOM VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @P_ADJUN_FIRMA_DIG_DATA VARBINARY(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @P_ADJUN_FIRMA_DIG_TIPO INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @ESCALADOS SMALLINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ESCALATIPO SMALLINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDESC INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @DIFESC FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDESC_SEL INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @DIFESC_SEL FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_IT_ESC INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @CANT_IT_ESC FLOAT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @ERROR = 0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @ANYO = ANYO, @GMN1= GMN1, @PROCE = PROCE, @PROVE = PROVE, @MON = MON, @NUMOBJ = NUMOBJ, @PORTAL = PORTAL," & vbCrLf
sConsulta = sConsulta & "@P_ADJUN_FIRMA_DIG_ID= P_ADJUN_FIRMA_DIGITAL.ID, @P_ADJUN_FIRMA_DIG_NOM = P_ADJUN_FIRMA_DIGITAL.NOM, @P_ADJUN_FIRMA_DIG_DATA = P_ADJUN_FIRMA_DIGITAL.DATA, @P_ADJUN_FIRMA_DIG_TIPO = P_ADJUN_FIRMA_DIGITAL.TIPO" & vbCrLf
sConsulta = sConsulta & "FROM P_PROCE_OFE WITH (NOLOCK) LEFT JOIN P_ADJUN_FIRMA_DIGITAL WITH (NOLOCK) ON P_PROCE_OFE.ADJUN_FIRMA_DIGITAL = P_ADJUN_FIRMA_DIGITAL.ID WHERE P_PROCE_OFE.ID = @ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @NUMOBJGS = (SELECT NUMOBJ FROM PROCE_PROVE WITH (NOLOCK) WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE)" & vbCrLf
sConsulta = sConsulta & "IF @NUMOBJGS  <=@NUMOBJ" & vbCrLf
sConsulta = sConsulta & "   UPDATE PROCE_PROVE SET OBJNUEVOS=0 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @SUBASTA INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @SUBASTA = SUBASTA FROM PROCE_DEF WITH (NOLOCK)  WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE  " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @SUBASTA = 1 OR @COMPROBARATRIBS  = 1 --En las subastas puede que no haya entrado en la secci�n de atributos, por eso se comprueba aqu� que est�n rellenos:" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    DECLARE @OFE INT" & vbCrLf
sConsulta = sConsulta & "    -- CALIDAD Sin with (nolock) pq se usa en insert. En la where en vez de la clave pero no veo bien una lectura sucia. Pq antes de este stored hay otros y tambien se ha ejecutado antes" & vbCrLf
sConsulta = sConsulta & "    ---codigo creando sql, metiendo en archivo y haciendo un exec del contenido del archivo. Y eso en una subasta con X proves a la vez pues ... mucho trafico" & vbCrLf
sConsulta = sConsulta & "    SET @OFE = ( SELECT MAX(OFE) FROM PROCE_OFE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND PROVE=@PROVE AND OFE!=-1)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    IF @OFE IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "   --Calidad Recuerdo q El exists es Sin with(nolock) pq ya ha pasado q intenta meter 2 veces el mismo por rollos ambito etc. Algo mas lento pero te aseguras q no casca." & vbCrLf
sConsulta = sConsulta & "   --El hecho de que haya el exists ya apoya esta idea???  Si esto ya no pasa se quita el exists" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO P_OFE_GR_ATRIB (ID, GRUPO, ATRIB_ID,  VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL)" & vbCrLf
sConsulta = sConsulta & "   SELECT @ID, PG.COD GRUPO, O.ATRIB_ID, O.VALOR_NUM, O.VALOR_TEXT, O.VALOR_FEC, O.VALOR_BOOL" & vbCrLf
sConsulta = sConsulta & "   FROM OFE_GR_ATRIB O WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PROCE_GRUPO PG WITH (NOLOCK)  ON PG.ANYO = O.ANYO AND PG.GMN1 = O.GMN1 AND PG.PROCE = O.PROCE AND PG.ID = O.GRUPO" & vbCrLf
sConsulta = sConsulta & "   WHERE O.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "       AND O.GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "       AND O.PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "       AND O.PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "       AND O.OFE = @OFE" & vbCrLf
sConsulta = sConsulta & "       AND NOT EXISTS (SELECT T.ID FROM P_OFE_GR_ATRIB T WITH (NOLOCK) WHERE T.ID = @ID AND T.GRUPO = PG.COD AND T.ATRIB_ID = O.ATRIB_ID)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "           IF @@ERROR!=0 GOTO ERRORSINTRAN_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   --Calidad Recuerdo q El exists es Sin with(nolock) pq ya ha pasado q intenta meter 2 veces el mismo por rollos ambito etc. Algo mas lento pero te aseguras q no casca." & vbCrLf
sConsulta = sConsulta & "   --El hecho de que haya el exists ya apoya esta idea???  Si esto ya no pasa se quita el exists" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO P_OFE_ITEM_ATRIB (ID, GRUPO, ITEM, ATRIB_ID,  VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL)" & vbCrLf
sConsulta = sConsulta & "   SELECT @ID,PG.COD GRUPO, IA.ITEM, ATRIB_ID, VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL" & vbCrLf
sConsulta = sConsulta & "   FROM OFE_ITEM_ATRIB IA WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   INNER JOIN ITEM I WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   ON I.ANYO = IA.ANYO" & vbCrLf
sConsulta = sConsulta & "       AND I.GMN1_PROCE = IA.GMN1" & vbCrLf
sConsulta = sConsulta & "       AND I.PROCE = IA.PROCE" & vbCrLf
sConsulta = sConsulta & "       AND I.ID = IA.ITEM" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PROCE_GRUPO PG WITH (NOLOCK)  ON PG.ANYO = I.ANYO AND PG.GMN1 = I.GMN1 AND PG.PROCE = I.PROCE    AND PG.ID =I.GRUPO" & vbCrLf
sConsulta = sConsulta & "   WHERE IA.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "       AND IA.GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "       AND IA.PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "       AND IA.PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "       AND IA.OFE = @OFE" & vbCrLf
sConsulta = sConsulta & "       AND NOT EXISTS (SELECT T.ID FROM P_OFE_ITEM_ATRIB T WITH (NOLOCK) WHERE T.ID = @ID AND T.ATRIB_ID = IA.ATRIB_ID AND T.ITEM = IA.ITEM)" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERRORSINTRAN_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   --Calidad Recuerdo q El exists es Sin with(nolock) pq ya ha pasado q intenta meter 2 veces el mismo por rollos ambito etc. Algo mas lento pero te aseguras q no casca." & vbCrLf
sConsulta = sConsulta & "   --El hecho de que haya el exists ya apoya esta idea???  Si esto ya no pasa se quita el exists" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO P_OFE_ATRIB (ID, ATRIB_ID,  VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL)" & vbCrLf
sConsulta = sConsulta & "   SELECT @ID, O.ATRIB_ID, O.VALOR_NUM, O.VALOR_TEXT, O.VALOR_FEC, O.VALOR_BOOL" & vbCrLf
sConsulta = sConsulta & "   FROM OFE_ATRIB O WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   WHERE O.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "       AND O.GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "       AND O.PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "       AND O.PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "       AND O.OFE = @OFE" & vbCrLf
sConsulta = sConsulta & "       AND NOT EXISTS (SELECT T.ID FROM P_OFE_ATRIB T WITH (NOLOCK) WHERE T.ID = @ID AND T.ATRIB_ID = O.ATRIB_ID)" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERRORSINTRAN_" & vbCrLf
sConsulta = sConsulta & "      END " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "      DECLARE @OFEATRIB BIT" & vbCrLf
sConsulta = sConsulta & "      DECLARE @OFEGRUPOATRIB BIT" & vbCrLf
sConsulta = sConsulta & "      DECLARE @OFEITEMATRIB BIT" & vbCrLf
sConsulta = sConsulta & "      DECLARE @PAID INT" & vbCrLf
sConsulta = sConsulta & "      DECLARE @AMBITO SMALLINT" & vbCrLf
sConsulta = sConsulta & "      DECLARE @TIPO SMALLINT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    DECLARE CATRIBS CURSOR LOCAL FORWARD_ONLY  FOR " & vbCrLf
sConsulta = sConsulta & "    SELECT PA.ID, AMBITO, PG.COD CODGRUPO,PG.ID IDGRUPO, TIPO, ESCALADOS,ESCALATIPO" & vbCrLf
sConsulta = sConsulta & "    FROM PROCE_ATRIB PA WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "    INNER JOIN DEF_ATRIB DA WITH (NOLOCK) ON PA.ATRIB = DA.ID" & vbCrLf
sConsulta = sConsulta & "    LEFT JOIN PROCE_GRUPO PG WITH (NOLOCK) ON PG.ANYO = PA.ANYO AND PG.GMN1 = PA.GMN1 AND PG.PROCE = PA.PROCE AND PG.ID = PA.GRUPO" & vbCrLf
sConsulta = sConsulta & "    WHERE PA.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "       AND PA.GMN1= @GMN1" & vbCrLf
sConsulta = sConsulta & "       AND PA.PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "       AND PA.INTERNO = 0" & vbCrLf
sConsulta = sConsulta & "       AND PA.OBLIG = 1" & vbCrLf
sConsulta = sConsulta & "      " & vbCrLf
sConsulta = sConsulta & "    OPEN CATRIBS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    FETCH NEXT FROM CATRIBS INTO @PAID, @AMBITO, @CODGRUPO,@IDGRUPO,  @TIPO, @ESCALADOS, @ESCALATIPO" & vbCrLf
sConsulta = sConsulta & "      SET @OFEATRIB=0" & vbCrLf
sConsulta = sConsulta & "      SET @OFEGRUPOATRIB=0      SET @OFEITEMATRIB=0" & vbCrLf
sConsulta = sConsulta & "      WHILE @@FETCH_STATUS=0 AND @OFEATRIB=0 AND @OFEGRUPOATRIB=0 AND @OFEITEMATRIB=0" & vbCrLf
sConsulta = sConsulta & "        BEGIN" & vbCrLf
sConsulta = sConsulta & "          IF @AMBITO = 1" & vbCrLf
sConsulta = sConsulta & "            BEGIN" & vbCrLf
sConsulta = sConsulta & "              SET @OFEATRIB =1" & vbCrLf
sConsulta = sConsulta & "              SELECT @OFEATRIB = CASE " & vbCrLf
sConsulta = sConsulta & "                        WHEN CASE @TIPO " & vbCrLf
sConsulta = sConsulta & "                                WHEN 1 THEN CASE WHEN VALOR_TEXT ='' THEN NULL ELSE VALOR_TEXT END " & vbCrLf
sConsulta = sConsulta & "                                WHEN 2 THEN CAST(VALOR_NUM AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                                WHEN 3 THEN CAST(VALOR_FEC AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                                WHEN 4 THEN CAST(VALOR_BOOL AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                             END IS NULL " & vbCrLf
sConsulta = sConsulta & "                        THEN 1 " & vbCrLf
sConsulta = sConsulta & "                        ELSE 0 " & vbCrLf
sConsulta = sConsulta & "                     END" & vbCrLf
sConsulta = sConsulta & "                FROM P_OFE_ATRIB WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "               WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "                 AND ATRIB_ID = @PAID" & vbCrLf
sConsulta = sConsulta & "            END" & vbCrLf
sConsulta = sConsulta & "          IF @AMBITO = 2" & vbCrLf
sConsulta = sConsulta & "            BEGIN" & vbCrLf
sConsulta = sConsulta & "              SET @OFEGRUPOATRIB =0" & vbCrLf
sConsulta = sConsulta & "              IF @CODGRUPO IS NULL " & vbCrLf
sConsulta = sConsulta & "                BEGIN" & vbCrLf
sConsulta = sConsulta & "                IF (SELECT COUNT(*) " & vbCrLf
sConsulta = sConsulta & "                      FROM P_OFE_GR_ATRIB WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                     WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "                       AND ATRIB_ID = @PAID" & vbCrLf
sConsulta = sConsulta & "                       AND CASE @TIPO " & vbCrLf
sConsulta = sConsulta & "                                WHEN 1 THEN CASE WHEN VALOR_TEXT ='' THEN NULL ELSE VALOR_TEXT END " & vbCrLf
sConsulta = sConsulta & "                                WHEN 2 THEN CAST(VALOR_NUM AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                                WHEN 3 THEN CAST(VALOR_FEC AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                                WHEN 4 THEN CAST(VALOR_BOOL AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                             END IS NULL) > 0" & vbCrLf
sConsulta = sConsulta & "                   SET @OFEGRUPOATRIB = 1" & vbCrLf
sConsulta = sConsulta & "                 END" & vbCrLf
sConsulta = sConsulta & "               ELSE" & vbCrLf
sConsulta = sConsulta & "                IF (SELECT COUNT(*) " & vbCrLf
sConsulta = sConsulta & "                      FROM P_OFE_GR_ATRIB WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                     WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "                       AND GRUPO = @CODGRUPO" & vbCrLf
sConsulta = sConsulta & "                       AND ATRIB_ID = @PAID" & vbCrLf
sConsulta = sConsulta & "                       AND CASE @TIPO " & vbCrLf
sConsulta = sConsulta & "                                WHEN 1 THEN CASE WHEN VALOR_TEXT ='' THEN NULL ELSE VALOR_TEXT END " & vbCrLf
sConsulta = sConsulta & "                                WHEN 2 THEN CAST(VALOR_NUM AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                                WHEN 3 THEN CAST(VALOR_FEC AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                                WHEN 4 THEN CAST(VALOR_BOOL AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                             END IS NULL) >0" & vbCrLf
sConsulta = sConsulta & "                       SET @OFEGRUPOATRIB = 1" & vbCrLf
sConsulta = sConsulta & "            END" & vbCrLf
sConsulta = sConsulta & "          IF @AMBITO = 3" & vbCrLf
sConsulta = sConsulta & "            BEGIN" & vbCrLf
sConsulta = sConsulta & "              SET @OFEITEMATRIB =0" & vbCrLf
sConsulta = sConsulta & "              IF @CODGRUPO IS NULL " & vbCrLf
sConsulta = sConsulta & "                BEGIN" & vbCrLf
sConsulta = sConsulta & "                  IF (SELECT COUNT(*) " & vbCrLf
sConsulta = sConsulta & "                        FROM P_OFE_ITEM_ATRIB  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                       WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "                         AND ATRIB_ID = @PAID" & vbCrLf
sConsulta = sConsulta & "                         AND CASE @TIPO " & vbCrLf
sConsulta = sConsulta & "                                WHEN 1 THEN CASE WHEN VALOR_TEXT ='' THEN NULL ELSE VALOR_TEXT END " & vbCrLf
sConsulta = sConsulta & "                                WHEN 2 THEN CAST(VALOR_NUM AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                                WHEN 3 THEN CAST(VALOR_FEC AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                                WHEN 4 THEN CAST(VALOR_BOOL AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                     END IS NOT NULL)  <>" & vbCrLf
sConsulta = sConsulta & "                      (SELECT COUNT(*) " & vbCrLf
sConsulta = sConsulta & "                         FROM ITEM I  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN P_PROCE_OFE P WITH (NOLOCK)  ON I.ANYO=P.ANYO AND I.GMN1_PROCE=P.GMN1 AND I.PROCE=P.PROCE AND P.ID=@ID  " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN P_ITEM_OFE O  WITH (NOLOCK)  ON P.ID=O.ID AND I.ID=O.ITEM AND O.ID=@ID  " & vbCrLf
sConsulta = sConsulta & "                        WHERE I.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "                          AND I.GMN1_PROCE = @GMN1" & vbCrLf
sConsulta = sConsulta & "                          AND I.PROCE = @PROCE                    " & vbCrLf
sConsulta = sConsulta & "                          AND I.CONF = 1" & vbCrLf
sConsulta = sConsulta & "                          AND (O.PRECIO IS NOT NULL OR O.PRECIO2 IS NOT NULL OR O.PRECIO3 IS NOT NULL)" & vbCrLf
sConsulta = sConsulta & "                          )" & vbCrLf
sConsulta = sConsulta & "                       SET @OFEITEMATRIB = 1" & vbCrLf
sConsulta = sConsulta & "                 END" & vbCrLf
sConsulta = sConsulta & "              ELSE" & vbCrLf
sConsulta = sConsulta & "                 begin" & vbCrLf
sConsulta = sConsulta & "                  IF (SELECT COUNT(*) " & vbCrLf
sConsulta = sConsulta & "                        FROM P_OFE_ITEM_ATRIB WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                       WHERE GRUPO = @CODGRUPO" & vbCrLf
sConsulta = sConsulta & "                         AND ID = @ID" & vbCrLf
sConsulta = sConsulta & "                         AND ATRIB_ID = @PAID" & vbCrLf
sConsulta = sConsulta & "                         AND CASE @TIPO " & vbCrLf
sConsulta = sConsulta & "                                WHEN 1 THEN CASE WHEN VALOR_TEXT ='' THEN NULL ELSE VALOR_TEXT END " & vbCrLf
sConsulta = sConsulta & "                                WHEN 2 THEN CAST(VALOR_NUM AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                                WHEN 3 THEN CAST(VALOR_FEC AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                                WHEN 4 THEN CAST(VALOR_BOOL AS VARCHAR)" & vbCrLf
sConsulta = sConsulta & "                             END IS NOT NULL)  <>" & vbCrLf
sConsulta = sConsulta & "                      (SELECT COUNT(*) " & vbCrLf
sConsulta = sConsulta & "                         FROM ITEM I  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "        INNER JOIN P_PROCE_OFE P WITH (NOLOCK)  ON I.ANYO=P.ANYO AND I.GMN1_PROCE=P.GMN1 AND I.PROCE=P.PROCE AND P.ID=@ID  " & vbCrLf
sConsulta = sConsulta & "        INNER JOIN P_ITEM_OFE O  WITH (NOLOCK)  ON P.ID=O.ID AND I.ID=O.ITEM AND @CODGRUPO=O.GRUPO AND O.ID=@ID   " & vbCrLf
sConsulta = sConsulta & "                        WHERE I.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "                          AND I.GMN1_PROCE = @GMN1" & vbCrLf
sConsulta = sConsulta & "                          AND I.PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "                          AND I.GRUPO = @IDGRUPO" & vbCrLf
sConsulta = sConsulta & "                          AND I.CONF = 1" & vbCrLf
sConsulta = sConsulta & "                          AND (O.PRECIO IS NOT NULL OR O.PRECIO2 IS NOT NULL OR O.PRECIO3 IS NOT NULL)" & vbCrLf
sConsulta = sConsulta & "                          )" & vbCrLf
sConsulta = sConsulta & "                       BEGIN" & vbCrLf
sConsulta = sConsulta & "                           IF (@TIPO=2)" & vbCrLf
sConsulta = sConsulta & "                           BEGIN                               " & vbCrLf
sConsulta = sConsulta & "                               IF (@ESCALADOS=1 AND @ESCALATIPO=0)" & vbCrLf
sConsulta = sConsulta & "                               BEGIN                                                                                               " & vbCrLf
sConsulta = sConsulta & "                                   IF (SELECT COUNT(*) " & vbCrLf
sConsulta = sConsulta & "                                       FROM ITEM I  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                       INNER JOIN P_PROCE_OFE P WITH (NOLOCK)  ON I.ANYO=P.ANYO AND I.GMN1_PROCE=P.GMN1 AND I.PROCE=P.PROCE  " & vbCrLf
sConsulta = sConsulta & "                                       INNER JOIN P_ITEM_OFE O  WITH (NOLOCK)  ON P.ID=O.ID AND I.ID=O.ITEM AND @CODGRUPO=O.GRUPO   " & vbCrLf
sConsulta = sConsulta & "                                       WHERE I.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "                                         AND I.GMN1_PROCE = @GMN1" & vbCrLf
sConsulta = sConsulta & "                                         AND I.PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "                                         AND I.GRUPO = @IDGRUPO" & vbCrLf
sConsulta = sConsulta & "                                         AND I.CONF = 1" & vbCrLf
sConsulta = sConsulta & "                                         AND (O.PRECIO IS NOT NULL OR O.PRECIO2 IS NOT NULL OR O.PRECIO3 IS NOT NULL))" & vbCrLf
sConsulta = sConsulta & "                                          <> " & vbCrLf
sConsulta = sConsulta & "                                       (SELECT COUNT(*)" & vbCrLf
sConsulta = sConsulta & "                                       FROM P_OFE_ITEM_ATRIBESC WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                       INNER JOIN ITEM I  WITH (NOLOCK) ON I.ID = P_OFE_ITEM_ATRIBESC.ITEM" & vbCrLf
sConsulta = sConsulta & "                                           AND I.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "                                           AND I.GMN1_PROCE = @GMN1" & vbCrLf
sConsulta = sConsulta & "                                           AND I.PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "                                           AND I.GRUPO = @IDGRUPO" & vbCrLf
sConsulta = sConsulta & "                                           AND I.CONF = 1                          " & vbCrLf
sConsulta = sConsulta & "                                       INNER JOIN P_PROCE_OFE P WITH (NOLOCK)  ON I.ANYO=P.ANYO AND I.GMN1_PROCE=P.GMN1 AND I.PROCE=P.PROCE  " & vbCrLf
sConsulta = sConsulta & "                                       INNER JOIN P_ITEM_OFE O  WITH (NOLOCK)  ON P.ID=O.ID AND I.ID=O.ITEM AND @CODGRUPO=O.GRUPO                              " & vbCrLf
sConsulta = sConsulta & "                                           AND (O.PRECIO IS NOT NULL OR O.PRECIO2 IS NOT NULL OR O.PRECIO3 IS NOT NULL) " & vbCrLf
sConsulta = sConsulta & "                                       INNER JOIN PROCE_GR_ESC E WITH (NOLOCK) ON I.ANYO=E.ANYO AND I.GMN1_PROCE=E.GMN1 AND I.PROCE=E.PROCE AND E.GRUPO=E.GRUPO" & vbCrLf
sConsulta = sConsulta & "                                           AND (ISNULL(I.CANT,0) BETWEEN E.INICIO AND E.FIN)" & vbCrLf
sConsulta = sConsulta & "                                       WHERE P_OFE_ITEM_ATRIBESC.ID = @ID                                                              " & vbCrLf
sConsulta = sConsulta & "                                       AND ATRIB_ID = @PAID" & vbCrLf
sConsulta = sConsulta & "                                       AND P_OFE_ITEM_ATRIBESC.ESC= E.ID" & vbCrLf
sConsulta = sConsulta & "                                       AND VALOR_NUM IS NOT NULL)  " & vbCrLf
sConsulta = sConsulta & "                                           SET @OFEITEMATRIB = 1                                       " & vbCrLf
sConsulta = sConsulta & "                               END" & vbCrLf
sConsulta = sConsulta & "                               ELSE" & vbCrLf
sConsulta = sConsulta & "                                   IF (@ESCALADOS=1 AND @ESCALATIPO=1)" & vbCrLf
sConsulta = sConsulta & "                                   BEGIN" & vbCrLf
sConsulta = sConsulta & "                                       DECLARE CESCALADO_IT CURSOR LOCAL FORWARD_ONLY FOR" & vbCrLf
sConsulta = sConsulta & "                                       SELECT I.ID, ISNULL(I.CANT,0) CANT" & vbCrLf
sConsulta = sConsulta & "                                       FROM ITEM I WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                                       WHERE I.ANYO=@ANYO AND I.GMN1_PROCE=@GMN1 AND I.PROCE=@PROCE AND I.GRUPO=@CODGRUPO" & vbCrLf
sConsulta = sConsulta & "                                       ORDER BY I.ID" & vbCrLf
sConsulta = sConsulta & "                                       " & vbCrLf
sConsulta = sConsulta & "                                       OPEN CESCALADO_IT" & vbCrLf
sConsulta = sConsulta & "                                       FETCH NEXT FROM CESCALADO_IT INTO @ID_IT_ESC, @CANT_IT_ESC" & vbCrLf
sConsulta = sConsulta & "                                       WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "                                       BEGIN" & vbCrLf
sConsulta = sConsulta & "                                           DECLARE CESCALADO CURSOR LOCAL FORWARD_ONLY FOR" & vbCrLf
sConsulta = sConsulta & "                                           SELECT ID, ABS(INICIO-@CANT_IT_ESC) FROM PROCE_GR_ESC E WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                           WHERE E.ANYO=@ANYO AND E.GMN1=@GMN1 AND E.PROCE=@PROCE AND E.GRUPO=@CODGRUPO" & vbCrLf
sConsulta = sConsulta & "                                           ORDER BY ID" & vbCrLf
sConsulta = sConsulta & "                                           OPEN CESCALADO" & vbCrLf
sConsulta = sConsulta & "                                           FETCH NEXT FROM CESCALADO INTO @IDESC, @DIFESC  " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                                           SET @IDESC_SEL = -1" & vbCrLf
sConsulta = sConsulta & "                                           SET @DIFESC_SEL = -1    " & vbCrLf
sConsulta = sConsulta & "                                           " & vbCrLf
sConsulta = sConsulta & "                                           WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "                                           BEGIN       " & vbCrLf
sConsulta = sConsulta & "                                               IF @IDESC_SEL = -1" & vbCrLf
sConsulta = sConsulta & "                                               BEGIN " & vbCrLf
sConsulta = sConsulta & "                                                   SET @IDESC_SEL = @IDESC" & vbCrLf
sConsulta = sConsulta & "                                                   SET @DIFESC_SEL = @DIFESC " & vbCrLf
sConsulta = sConsulta & "                                               END " & vbCrLf
sConsulta = sConsulta & "                                               ELSE            " & vbCrLf
sConsulta = sConsulta & "                                                   IF  @DIFESC_SEL > @DIFESC " & vbCrLf
sConsulta = sConsulta & "                                                   BEGIN " & vbCrLf
sConsulta = sConsulta & "                                                       SET @IDESC_SEL = @IDESC" & vbCrLf
sConsulta = sConsulta & "                                                       SET @DIFESC_SEL = @DIFESC " & vbCrLf
sConsulta = sConsulta & "                                                   END             " & vbCrLf
sConsulta = sConsulta & "                                           " & vbCrLf
sConsulta = sConsulta & "                                               FETCH NEXT FROM CESCALADO INTO @IDESC, @DIFESC                                      " & vbCrLf
sConsulta = sConsulta & "                                           END" & vbCrLf
sConsulta = sConsulta & "                                           CLOSE CESCALADO" & vbCrLf
sConsulta = sConsulta & "                                           DEALLOCATE CESCALADO        " & vbCrLf
sConsulta = sConsulta & "                                           " & vbCrLf
sConsulta = sConsulta & "                                           IF (SELECT COUNT(*) FROM P_OFE_ITEM_ATRIBESC WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                                           WHERE P_OFE_ITEM_ATRIBESC.ID = @ID                                                              " & vbCrLf
sConsulta = sConsulta & "                                           AND P_OFE_ITEM_ATRIBESC.ATRIB_ID = @PAID" & vbCrLf
sConsulta = sConsulta & "                                           AND P_OFE_ITEM_ATRIBESC.ESC= @IDESC_SEL" & vbCrLf
sConsulta = sConsulta & "                                           AND P_OFE_ITEM_ATRIBESC.ITEM=@ID_IT_ESC " & vbCrLf
sConsulta = sConsulta & "                                           AND P_OFE_ITEM_ATRIBESC.VALOR_NUM IS NOT NULL)  =0" & vbCrLf
sConsulta = sConsulta & "                                               SET @OFEITEMATRIB = 1                                                   " & vbCrLf
sConsulta = sConsulta & "                                                                                                                           " & vbCrLf
sConsulta = sConsulta & "                                           GOTO MAS_" & vbCrLf
sConsulta = sConsulta & "                                       " & vbCrLf
sConsulta = sConsulta & "                                           FETCH NEXT FROM CESCALADO_IT INTO @ID_IT_ESC, @CANT_IT_ESC" & vbCrLf
sConsulta = sConsulta & "                                       END" & vbCrLf
sConsulta = sConsulta & "MAS_:                                      " & vbCrLf
sConsulta = sConsulta & "                                       CLOSE CESCALADO_IT" & vbCrLf
sConsulta = sConsulta & "                                       DEALLOCATE CESCALADO_IT" & vbCrLf
sConsulta = sConsulta & "                                                                           " & vbCrLf
sConsulta = sConsulta & "                                                                           " & vbCrLf
sConsulta = sConsulta & "                                   END" & vbCrLf
sConsulta = sConsulta & "                                   ELSE                                        " & vbCrLf
sConsulta = sConsulta & "                                       SET @OFEITEMATRIB = 1                               " & vbCrLf
sConsulta = sConsulta & "                           END" & vbCrLf
sConsulta = sConsulta & "                           ELSE                                                    " & vbCrLf
sConsulta = sConsulta & "                               SET @OFEITEMATRIB = 1" & vbCrLf
sConsulta = sConsulta & "                       END" & vbCrLf
sConsulta = sConsulta & "                   end" & vbCrLf
sConsulta = sConsulta & "            END" & vbCrLf
sConsulta = sConsulta & "            FETCH NEXT FROM CATRIBS INTO @PAID, @AMBITO, @CODGRUPO,@IDGRUPO,  @TIPO, @ESCALADOS, @ESCALATIPO" & vbCrLf
sConsulta = sConsulta & "          END" & vbCrLf
sConsulta = sConsulta & "      CLOSE CATRIBS" & vbCrLf
sConsulta = sConsulta & "      DEALLOCATE CATRIBS" & vbCrLf
sConsulta = sConsulta & "      IF @OFEATRIB = 1" & vbCrLf
sConsulta = sConsulta & "        SET @ERROR = 2" & vbCrLf
sConsulta = sConsulta & "      IF @OFEGRUPOATRIB = 1" & vbCrLf
sConsulta = sConsulta & "        SET @ERROR = 3" & vbCrLf
sConsulta = sConsulta & "      IF @OFEITEMATRIB = 1" & vbCrLf
sConsulta = sConsulta & "        SET @ERROR = 4" & vbCrLf
sConsulta = sConsulta & "      IF @ERROR>0" & vbCrLf
sConsulta = sConsulta & "        GOTO ELIMINAR_" & vbCrLf
sConsulta = sConsulta & "END --Fin comprobar atributos en subasta" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "DECLARE  @ESTWEB VARCHAR(3) " & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "SELECT @ESTWEB=ESTWEB FROM PARGEN_DEF WITH (NOLOCK) INNER JOIN PARGEN_GEST WITH (NOLOCK) ON PARGEN_DEF.ID=PARGEN_GEST.ID  " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "BEGIN TRANSACTION" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @FULL_ADJUN_FIRMA_DIG_NOM varchar(300)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PART_ADJUN_FIRMA_DIG_NOM varchar(300)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SEP char(1)" & vbCrLf
sConsulta = sConsulta & "DECLARE @CONT_SEP int" & vbCrLf
sConsulta = sConsulta & "DECLARE @CONT_CAR int" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @ADJUN_FIRMA_DIG_ID AS INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @FULL_ADJUN_FIRMA_DIG_NOM = @P_ADJUN_FIRMA_DIG_NOM" & vbCrLf
sConsulta = sConsulta & "SET @SEP = '_'" & vbCrLf
sConsulta = sConsulta & "SET @CONT_SEP = 0" & vbCrLf
sConsulta = sConsulta & "SET @CONT_CAR = 1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @ADJUN_FIRMA_DIG_ID = NULL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @FULL_ADJUN_FIRMA_DIG_NOM IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   WHILE @CONT_SEP < 5" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       IF @SEP = SUBSTRING(@FULL_ADJUN_FIRMA_DIG_NOM,@CONT_CAR,1)" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           SET @CONT_SEP = @CONT_SEP + 1" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "           Set @CONT_CAR = @CONT_CAR + 1" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "   SET @PART_ADJUN_FIRMA_DIG_NOM = SUBSTRING(@FULL_ADJUN_FIRMA_DIG_NOM,@CONT_CAR,len(@FULL_ADJUN_FIRMA_DIG_NOM) - @CONT_CAR +1)" & vbCrLf
sConsulta = sConsulta & "   SET @FULL_ADJUN_FIRMA_DIG_NOM = CONVERT(VARCHAR(50),@ANYO) + '_' + @GMN1 + '_' + CONVERT(VARCHAR(50),@PROCE) + '_' + @PROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @FULL_ADJUN_FIRMA_DIG_NOM = @FULL_ADJUN_FIRMA_DIG_NOM + '_' + CONVERT(VARCHAR(50),@NEWOFE) + '_' + @PART_ADJUN_FIRMA_DIG_NOM" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO ADJUN_FIRMA_DIGITAL (NOM,DATA,TIPO) SELECT @FULL_ADJUN_FIRMA_DIG_NOM, DATA,TIPO FROM P_ADJUN_FIRMA_DIGITAL WITH(NOLOCK) WHERE ID = @P_ADJUN_FIRMA_DIG_ID " & vbCrLf
sConsulta = sConsulta & "    SELECT @ADJUN_FIRMA_DIG_ID=SCOPE_IDENTITY()" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE SET ULT= 0 WHERE ANYO = @ANYO AND GMN1 = @GMN1 AND PROCE = @PROCE AND PROVE = @PROVE AND ULT = 1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--CALIDAD Sin With (nolock) pq se usa en insert como parte de la clave" & vbCrLf
sConsulta = sConsulta & "SELECT @NEWOFE = ISNULL(MAX(OFE),0) + 1 FROM PROCE_OFE WHERE ANYO = @ANYO AND GMN1 = @GMN1 AND PROCE = @PROCE AND PROVE = @PROVE AND OFE != -1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO PROCE_OFE(ANYO, GMN1, PROCE, PROVE, OFE, EST, FECREC, FECVAL, MON, CAMBIO, OBS, ULT, ENVIADA, OBSADJUN, PORTAL,USU,NOMUSU,IMPORTE,IMPORTE_BRUTO,ADJUN_FIRMA_DIGITAL)" & vbCrLf
sConsulta = sConsulta & "SELECT @ANYO, @GMN1, @PROCE, @PROVE, @NEWOFE, @ESTWEB, getutcdate(), FECVAL, MON, CAMBIO, OBS, 1, 1, OBSADJUN, PORTAL,USU,NOMUSU,IMPORTE,IMPORTE_BRUTO,@ADJUN_FIRMA_DIG_ID" & vbCrLf
sConsulta = sConsulta & " FROM P_PROCE_OFE WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & " WHERE ID = @ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE GRUPO_OFE " & vbCrLf
sConsulta = sConsulta & "   SET OBSADJUN = PGO.OBSADJUN,IMPORTE=PGO.IMPORTE,IMPORTE_BRUTO=PGO.IMPORTE_BRUTO" & vbCrLf
sConsulta = sConsulta & "  FROM GRUPO_OFE O WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PROCE_GRUPO PG WITH (NOLOCK) ON PG.ANYO = O.ANYO AND PG.GMN1 = O.GMN1 AND PG.PROCE = O.PROCE AND PG.ID =O.GRUPO" & vbCrLf
sConsulta = sConsulta & "      INNER JOIN P_GRUPO_OFE PGO WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "              ON PG.COD = PGO.GRUPO" & vbCrLf
sConsulta = sConsulta & " WHERE O.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "   AND O.GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "   AND O.PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "   AND O.PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "   AND O.OFE = @NEWOFE" & vbCrLf
sConsulta = sConsulta & "   AND PGO.ID = @ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--********************************************" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_OFE" & vbCrLf
sConsulta = sConsulta & "   SET PRECIO = PIO.PRECIO," & vbCrLf
sConsulta = sConsulta & "       CANTMAX = PIO.CANTMAX," & vbCrLf
sConsulta = sConsulta & "       PRECIO2 = PIO.PRECIO2," & vbCrLf
sConsulta = sConsulta & "       PRECIO3 = PIO.PRECIO3," & vbCrLf
sConsulta = sConsulta & "       OBSADJUN = PIO.OBSADJUN," & vbCrLf
sConsulta = sConsulta & "       COMENT1 = PIO.COMENT1," & vbCrLf
sConsulta = sConsulta & "       COMENT2 = PIO.COMENT2," & vbCrLf
sConsulta = sConsulta & "       COMENT3 = PIO.COMENT3," & vbCrLf
sConsulta = sConsulta & "       IMPORTE = PIO.IMPORTE," & vbCrLf
sConsulta = sConsulta & "       PREC_VALIDO = PIO.PREC_VALIDO" & vbCrLf
sConsulta = sConsulta & "  FROM ITEM_OFE IO  WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "     INNER JOIN P_ITEM_OFE PIO  WITH (NOLOCK)  ON IO.ITEM = PIO.ITEM" & vbCrLf
sConsulta = sConsulta & "     INNER JOIN ITEM I  WITH (NOLOCK)  ON IO.ANYO=I.ANYO AND IO.GMN1=I.GMN1_PROCE AND IO.PROCE=I.PROCE AND IO.ITEM= I.ID " & vbCrLf
sConsulta = sConsulta & " WHERE IO.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "   AND IO.GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "   AND IO.PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "   AND IO.PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "   AND IO.NUM = @NEWOFE" & vbCrLf
sConsulta = sConsulta & "   AND PIO.ID = @ID" & vbCrLf
sConsulta = sConsulta & "   AND (I.EST=0 OR I.EST IS NULL)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC FSN_RECALCULAR_PRECIO_ITEM @ANYO, @GMN1, @PROCE, 0" & vbCrLf
sConsulta = sConsulta & "--DPD************************************************" & vbCrLf
sConsulta = sConsulta & "--La posici�n en la puja se calcula autom�ticamente en el triger Update" & vbCrLf
sConsulta = sConsulta & "--de la tabala ITEM_OFE, as� como la del resto de proveedores" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--****************************************" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- Precios escalados" & vbCrLf
sConsulta = sConsulta & "INSERT INTO ITEM_OFEESC (ANYO,GMN1,PROCE,ITEM,PROVE,OFE,ESC,PRECIO,PREC_VALIDO)(" & vbCrLf
sConsulta = sConsulta & "SELECT @ANYO,@GMN1,@PROCE,ITEM,@PROVE,@NEWOFE,ESC,PRECIO,PREC_VALIDO" & vbCrLf
sConsulta = sConsulta & "FROM P_ITEM_OFEESC WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE ID=@ID AND PRECIO IS NOT NULL)" & vbCrLf
sConsulta = sConsulta & "-- Fin Precios Escalados" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_ITEM_ADJUN " & vbCrLf
sConsulta = sConsulta & "  FROM OFE_ITEM_ADJUN R  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN P_ITEM_OFE PIO  WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "               ON PIO.ID = @ID" & vbCrLf
sConsulta = sConsulta & "              AND R.ITEM = PIO.ITEM" & vbCrLf
sConsulta = sConsulta & " WHERE R.ANYO = @ANYO " & vbCrLf
sConsulta = sConsulta & "   AND R.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "   AND R.PROCE= @PROCE " & vbCrLf
sConsulta = sConsulta & "   AND R.PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "   AND R.OFE IN (@NEWOFE,-1)" & vbCrLf
sConsulta = sConsulta & "   AND PIO.ADJUN = 1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "--CALIDAD A este exists le pongo with (nolock) no como a los del principio pq insert es diferente a delete. Y ya he tenido mas tiempo." & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_ITEM_ATRIB " & vbCrLf
sConsulta = sConsulta & "  FROM OFE_ITEM_ATRIB R   WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & " WHERE R.ANYO = @ANYO " & vbCrLf
sConsulta = sConsulta & "   AND R.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "   AND R.PROCE= @PROCE " & vbCrLf
sConsulta = sConsulta & "   AND R.PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "   AND R.OFE IN (@NEWOFE,-1)" & vbCrLf
sConsulta = sConsulta & "   AND EXISTS (SELECT P.ID FROM P_OFE_ITEM_ATRIB P WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                WHERE P.ID  = @ID " & vbCrLf
sConsulta = sConsulta & "                  AND P.ITEM = R.ITEM" & vbCrLf
sConsulta = sConsulta & "                  AND P.ATRIB_ID = R.ATRIB_ID)" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--CALIDAD A este exists le pongo with (nolock) no como a los del principio pq insert es diferente a delete. Y ya he tenido mas tiempo." & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_GR_ATRIB " & vbCrLf
sConsulta = sConsulta & "  FROM OFE_GR_ATRIB R   WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "  INNER JOIN PROCE_GRUPO PG WITH (NOLOCK) ON PG.ANYO = R.ANYO AND PG.GMN1 = R.GMN1 AND PG.PROCE = R.PROCE AND PG.ID = R.GRUPO" & vbCrLf
sConsulta = sConsulta & " WHERE R.ANYO = @ANYO " & vbCrLf
sConsulta = sConsulta & "   AND R.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "   AND R.PROCE= @PROCE " & vbCrLf
sConsulta = sConsulta & "   AND R.PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "   AND R.OFE IN (@NEWOFE,-1)" & vbCrLf
sConsulta = sConsulta & "   AND EXISTS (SELECT P.ID FROM P_OFE_GR_ATRIB P   WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                WHERE P.ID  = @ID " & vbCrLf
sConsulta = sConsulta & "                  AND CAST(P.GRUPO AS VARCHAR(50)) = CAST(PG.COD AS VARCHAR(50))" & vbCrLf
sConsulta = sConsulta & "                  AND P.ATRIB_ID = R.ATRIB_ID)" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_GR_ADJUN " & vbCrLf
sConsulta = sConsulta & "  FROM OFE_GR_ADJUN R WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "  INNER JOIN PROCE_GRUPO PG WITH (NOLOCK) ON PG.ANYO = R.ANYO AND PG.GMN1 = R.GMN1 AND PG.PROCE = R.PROCE AND PG.ID = R.GRUPO" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN P_GRUPO_OFE PGO WITH (NOLOCK) ON PGO.ID = @ID AND PGO.GRUPO = PG.COD" & vbCrLf
sConsulta = sConsulta & " WHERE R.ANYO = @ANYO " & vbCrLf
sConsulta = sConsulta & "   AND R.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "   AND R.PROCE= @PROCE " & vbCrLf
sConsulta = sConsulta & "   AND R.PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "   AND R.OFE IN (@NEWOFE,-1)" & vbCrLf
sConsulta = sConsulta & "   AND PGO.ADJUN = 1" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--CALIDAD Este exists se queda como estaba with (nolock) " & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_ATRIB " & vbCrLf
sConsulta = sConsulta & "  FROM OFE_ATRIB R WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & " WHERE R.ANYO = @ANYO " & vbCrLf
sConsulta = sConsulta & "   AND R.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "   AND R.PROCE= @PROCE " & vbCrLf
sConsulta = sConsulta & "   AND R.PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "   AND R.OFE IN (@NEWOFE,-1)" & vbCrLf
sConsulta = sConsulta & "   AND EXISTS (SELECT P.ID FROM P_OFE_ATRIB P WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                WHERE P.ID  = @ID " & vbCrLf
sConsulta = sConsulta & "                  AND P.ATRIB_ID = R.ATRIB_ID)" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF (SELECT ADJUN FROM P_PROCE_OFE WITH (NOLOCK) WHERE ID = @ID) = 1" & vbCrLf
sConsulta = sConsulta & "DELETE FROM PROCE_OFE_ADJUN " & vbCrLf
sConsulta = sConsulta & "  FROM PROCE_OFE_ADJUN R WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & " WHERE R.ANYO = @ANYO " & vbCrLf
sConsulta = sConsulta & "   AND R.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "   AND R.PROCE= @PROCE " & vbCrLf
sConsulta = sConsulta & "   AND R.PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "   AND R.OFE IN (@NEWOFE,-1)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_GR_COSTESDESC" & vbCrLf
sConsulta = sConsulta & "  FROM OFE_GR_COSTESDESC R WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & " WHERE R.ANYO = @ANYO " & vbCrLf
sConsulta = sConsulta & "   AND R.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "   AND R.PROCE= @PROCE " & vbCrLf
sConsulta = sConsulta & "   AND R.PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "   AND R.OFE IN (@NEWOFE,-1)" & vbCrLf
sConsulta = sConsulta & "   AND EXISTS (SELECT P.ID FROM P_OFE_GR_COSTESDESC P WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "               INNER JOIN PROCE_GRUPO PG WITH(NOLOCK) ON pg.ANYO = @ANYO AND PG.GMN1=@GMN1 AND PG.PROCE= @PROCE AND PG.COD=P.GRUPO " & vbCrLf
sConsulta = sConsulta & "                WHERE P.ID  = @ID " & vbCrLf
sConsulta = sConsulta & "                 AND PG.ID=R.GRUPO" & vbCrLf
sConsulta = sConsulta & "                  AND P.ATRIB_ID = R.ATRIB_ID)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM PROCE_OFE_ADJUN WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE= @PROCE and PROVE=@PROVE AND OFE = -1" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_GR_ADJUN WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE= @PROCE and PROVE=@PROVE AND OFE = -1" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_ITEM_ADJUN WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE= @PROCE and PROVE=@PROVE AND OFE = -1" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_ATRIB WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE= @PROCE and PROVE=@PROVE AND OFE = -1" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_GR_ATRIB WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE= @PROCE and PROVE=@PROVE AND OFE = -1" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_ITEM_ATRIB WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE= @PROCE and PROVE=@PROVE AND OFE = -1" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_GR_COSTESDESC WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE= @PROCE and PROVE=@PROVE AND OFE = -1" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM ITEM_OFE WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE= @PROCE and PROVE=@PROVE AND NUM = -1" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM GRUPO_OFE WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE= @PROCE and PROVE=@PROVE AND OFE = -1" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM PROCE_OFE WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE= @PROCE and PROVE=@PROVE AND OFE = -1" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR LOCAL FOR" & vbCrLf
sConsulta = sConsulta & "SELECT ADJUN, IDGS FROM P_PROCE_OFE_ADJUN WITH (NOLOCK) WHERE ID=@ID AND IDGS IS NULL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "OPEN C" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C INTO @ADJUN, @IDGS" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    --CALIDAD Sin with(nolock) pq se usa en insert como clave" & vbCrLf
sConsulta = sConsulta & "    SELECT @IDADJUN = isnull(MAX(ID),0) + 1 FROM ADJUN " & vbCrLf
sConsulta = sConsulta & "    INSERT INTO ADJUN (ID, WEB) VALUES (@IDADJUN, 1) " & vbCrLf
sConsulta = sConsulta & "    UPDATE P_PROCE_OFE_ADJUN SET IDGS = @IDADJUN WHERE ID = @ID AND ADJUN = @ADJUN" & vbCrLf
sConsulta = sConsulta & "    IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "    FETCH NEXT FROM C INTO @ADJUN, @IDGS" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "CLOSE C" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO PROCE_OFE_ADJUN(ANYO, GMN1, PROCE, PROVE, OFE, ID, NOM, COM, IDPORTAL, DATASIZE)" & vbCrLf
sConsulta = sConsulta & "SELECT @ANYO, @GMN1, @PROCE, @PROVE, @NEWOFE, POA.IDGS, POA.NOM, POA.COM, POA.IDPORTAL, POA.DATASIZE" & vbCrLf
sConsulta = sConsulta & "  FROM P_PROCE_OFE_ADJUN POA WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "         INNER JOIN ADJUN A WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                 ON POA.IDGS = A.ID" & vbCrLf
sConsulta = sConsulta & "WHERE POA.ID = @ID" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR LOCAL FOR" & vbCrLf
sConsulta = sConsulta & "SELECT ADJUN, IDGS, GRUPO FROM P_OFE_GR_ADJUN WITH (NOLOCK) WHERE ID=@ID AND IDGS IS NULL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "OPEN C" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C INTO @ADJUN, @IDGS, @CODGRUPO" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    --CALIDAD Sin with(nolock) pq se usa en insert como clave" & vbCrLf
sConsulta = sConsulta & "    SELECT @IDADJUN = isnull(MAX(ID),0) + 1 FROM ADJUN " & vbCrLf
sConsulta = sConsulta & "    INSERT INTO ADJUN (ID, WEB) VALUES (@IDADJUN, 1) " & vbCrLf
sConsulta = sConsulta & "    UPDATE P_OFE_GR_ADJUN SET IDGS = @IDADJUN WHERE ID = @ID AND ADJUN = @ADJUN AND GRUPO = @CODGRUPO" & vbCrLf
sConsulta = sConsulta & "    IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "    FETCH NEXT FROM C INTO @ADJUN, @IDGS, @CODGRUPO" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "CLOSE C" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO OFE_GR_ADJUN(ANYO, GMN1, PROCE, GRUPO, PROVE, OFE, ID, NOM, COM, IDPORTAL, DATASIZE)" & vbCrLf
sConsulta = sConsulta & "SELECT @ANYO, @GMN1, @PROCE, PG.ID GRUPO, @PROVE, @NEWOFE, OGA.IDGS, OGA.NOM, OGA.COM, OGA.IDPORTAL, OGA.DATASIZE" & vbCrLf
sConsulta = sConsulta & "  FROM P_OFE_GR_ADJUN OGA WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "         INNER JOIN ADJUN A WITH (NOLOCK) ON OGA.IDGS = A.ID" & vbCrLf
sConsulta = sConsulta & "          INNER JOIN PROCE_GRUPO PG WITH (NOLOCK) ON PG.ANYO = @ANYO AND PG.GMN1 = @GMN1 AND PG.PROCE = @PROCE AND PG.COD = OGA.GRUPO" & vbCrLf
sConsulta = sConsulta & " WHERE OGA.ID = @ID" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR LOCAL FOR" & vbCrLf
sConsulta = sConsulta & "SELECT ADJUN, IDGS, ITEM FROM P_OFE_ITEM_ADJUN WITH (NOLOCK) WHERE ID=@ID AND IDGS IS NULL" & vbCrLf
sConsulta = sConsulta & "OPEN C" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C INTO @ADJUN, @IDGS, @ITEM" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    --CALIDAD Sin with(nolock) pq se usa en insert como clave" & vbCrLf
sConsulta = sConsulta & "    SELECT @IDADJUN = isnull(MAX(ID),0) + 1 FROM ADJUN " & vbCrLf
sConsulta = sConsulta & "    INSERT INTO ADJUN (ID, WEB) VALUES (@IDADJUN, 1) " & vbCrLf
sConsulta = sConsulta & "    UPDATE P_OFE_ITEM_ADJUN SET IDGS = @IDADJUN WHERE ID = @ID AND ADJUN = @ADJUN AND ITEM = @ITEM" & vbCrLf
sConsulta = sConsulta & "    IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "    FETCH NEXT FROM C INTO @ADJUN, @IDGS, @ITEM" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "CLOSE C" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO OFE_ITEM_ADJUN(ANYO, GMN1, PROCE, ITEM, PROVE, OFE, ID, NOM, COM, IDPORTAL, DATASIZE)" & vbCrLf
sConsulta = sConsulta & "SELECT @ANYO, @GMN1, @PROCE, OIA.ITEM, @PROVE, @NEWOFE, OIA.IDGS, OIA.NOM, OIA.COM, OIA.IDPORTAL, OIA.DATASIZE" & vbCrLf
sConsulta = sConsulta & "  FROM P_OFE_ITEM_ADJUN OIA WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "         INNER JOIN ADJUN A WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                 ON OIA.IDGS = A.ID" & vbCrLf
sConsulta = sConsulta & "WHERE OIA.ID = @ID" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO OFE_GR_ATRIB(ANYO, GMN1, PROCE, GRUPO, PROVE, OFE, ATRIB_ID, VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL)" & vbCrLf
sConsulta = sConsulta & "SELECT @ANYO, @GMN1, @PROCE, PG.ID GRUPO, @PROVE, @NEWOFE, ATRIB_ID, VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL" & vbCrLf
sConsulta = sConsulta & "  FROM P_OFE_GR_ATRIB PO WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "        INNER JOIN PROCE_ATRIB PA WITH (NOLOCK) ON PO.ATRIB_ID  = PA.ID AND PA.ANYO = @ANYO AND PA.GMN1 = @GMN1 AND PA.PROCE = @PROCE " & vbCrLf
sConsulta = sConsulta & "          INNER JOIN PROCE_GRUPO PG WITH (NOLOCK) ON PG.ANYO = @ANYO AND PG.GMN1 = @GMN1 AND PG.PROCE = @PROCE AND PG.COD = PO.GRUPO" & vbCrLf
sConsulta = sConsulta & " WHERE PO.ID = @ID" & vbCrLf
sConsulta = sConsulta & "   AND PA.AMBITO = 2" & vbCrLf
sConsulta = sConsulta & "   AND NOT (VALOR_NUM IS NULL" & vbCrLf
sConsulta = sConsulta & "        AND VALOR_TEXT IS NULL" & vbCrLf
sConsulta = sConsulta & "        AND VALOR_FEC IS NULL" & vbCrLf
sConsulta = sConsulta & "        AND VALOR_BOOL IS NULL)" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO OFE_ITEM_ATRIB(ANYO, GMN1, PROCE, ITEM, PROVE, OFE, ATRIB_ID, VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL)" & vbCrLf
sConsulta = sConsulta & "SELECT @ANYO, @GMN1, @PROCE, PO.ITEM, @PROVE, @NEWOFE, ATRIB_ID, VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL" & vbCrLf
sConsulta = sConsulta & "  FROM P_OFE_ITEM_ATRIB PO WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "        INNER JOIN PROCE_ATRIB PA WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                ON PO.ATRIB_ID  = PA.ID " & vbCrLf
sConsulta = sConsulta & "     AND PA.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "               AND PA.GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "               AND PA.PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "                " & vbCrLf
sConsulta = sConsulta & " WHERE PO.ID = @ID" & vbCrLf
sConsulta = sConsulta & "   AND PA.AMBITO = 3" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "--DPD Si hay escalados se permiten registros con los valores a null   " & vbCrLf
sConsulta = sConsulta & "--   AND NOT (VALOR_NUM IS NULL" & vbCrLf
sConsulta = sConsulta & "--        AND VALOR_TEXT IS NULL" & vbCrLf
sConsulta = sConsulta & "--        AND VALOR_FEC IS NULL" & vbCrLf
sConsulta = sConsulta & "--        AND VALOR_BOOL IS NULL)" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Costes-Descuentos Escalados" & vbCrLf
sConsulta = sConsulta & "INSERT INTO OFE_ITEM_ATRIBESC (ANYO,GMN1,PROCE,ITEM,PROVE,OFE,ATRIB_ID,ESC,VALOR_NUM)(" & vbCrLf
sConsulta = sConsulta & "SELECT @ANYO,@GMN1,@PROCE,ITEM,@PROVE,@NEWOFE,ATRIB_ID,ESC,VALOR_NUM" & vbCrLf
sConsulta = sConsulta & "FROM P_OFE_ITEM_ATRIBESC WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE ID=@ID AND VALOR_NUM IS NOT NULL)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- Fin Costes-Descuentos Escalados" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO OFE_ATRIB(ANYO, GMN1, PROCE, PROVE, OFE, ATRIB_ID, VALOR_NUM, VALOR_TEXT, VALOR_FEC, VALOR_BOOL,IMPPARCIAL)" & vbCrLf
sConsulta = sConsulta & "SELECT @ANYO, @GMN1, @PROCE, @PROVE, @NEWOFE, PO.ATRIB_ID, PO.VALOR_NUM, PO.VALOR_TEXT, PO.VALOR_FEC, PO.VALOR_BOOL,PO.IMPPARCIAL" & vbCrLf
sConsulta = sConsulta & "  FROM P_OFE_ATRIB PO WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "        INNER JOIN PROCE_ATRIB PA WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                ON  PO.ATRIB_ID  = PA.ID " & vbCrLf
sConsulta = sConsulta & "    AND PA.ANYO = @ANYO" & vbCrLf
sConsulta = sConsulta & "               AND PA.GMN1 = @GMN1" & vbCrLf
sConsulta = sConsulta & "               AND PA.PROCE = @PROCE               " & vbCrLf
sConsulta = sConsulta & " WHERE PO.ID = @ID" & vbCrLf
sConsulta = sConsulta & "   AND PA.AMBITO = 1" & vbCrLf
sConsulta = sConsulta & "   AND NOT (VALOR_NUM IS NULL" & vbCrLf
sConsulta = sConsulta & "        AND VALOR_TEXT IS NULL" & vbCrLf
sConsulta = sConsulta & "        AND VALOR_FEC IS NULL" & vbCrLf
sConsulta = sConsulta & "        AND VALOR_BOOL IS NULL)" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO OFE_GR_COSTESDESC(ANYO, GMN1, PROCE, GRUPO, PROVE, OFE,ATRIB_ID ,IMPPARCIAL)" & vbCrLf
sConsulta = sConsulta & "SELECT @ANYO, @GMN1, @PROCE, PG.ID , @PROVE, @NEWOFE, POA.ATRIB_ID, POA.IMPPARCIAL" & vbCrLf
sConsulta = sConsulta & "  FROM P_OFE_GR_COSTESDESC POA WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "  INNER JOIN PROCE_GRUPO PG WITH (NOLOCK) ON PG.ANYO = @ANYO AND PG.GMN1 = @GMN1 AND PG.PROCE = @PROCE AND PG.COD = POA.GRUPO" & vbCrLf
sConsulta = sConsulta & "WHERE POA.ID = @ID" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE SET OFE = 1  WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE = @PROCE AND PROVE=@PROVE AND OFE = 0" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @PORTAL = 1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   --CALIDAD Sin with (nolock) se usa en insert como clave" & vbCrLf
sConsulta = sConsulta & "   SELECT @NUMOFEWEB = isnull(MAX(OFEWEB),0)+1 FROM PROCE_OFE_WEB WHERE ANYO = @ANYO AND GMN1 = @GMN1 AND PROCE = @PROCE AND PROVE = @PROVE " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO PROCE_OFE_WEB (ANYO, GMN1, PROCE, PROVE, OFEWEB, OFE, FECREC, FECVAL, MON)" & vbCrLf
sConsulta = sConsulta & "   SELECT ANYO, GMN1, PROCE, PROVE, @NUMOFEWEB, OFE, FECREC,FECVAL,MON " & vbCrLf
sConsulta = sConsulta & "   FROM PROCE_OFE WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE = @PROCE AND PROVE = @PROVE AND OFE = @NEWOFE" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO ITEM_OFE_WEB (ANYO, GMN1, PROCE, ITEM, PROVE, NUM, PRECIO, CANTMAX)" & vbCrLf
sConsulta = sConsulta & "   SELECT ANYO, GMN1, PROCE, ITEM, PROVE, @NUMOFEWEB, PRECIO, CANTMAX" & vbCrLf
sConsulta = sConsulta & "   FROM ITEM_OFE WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE = @PROCE AND PROVE = @PROVE AND NUM = @NEWOFE" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO PROCE_OFE_WEB_ADJUN (ANYO, GMN1, PROCE, PROVE, OFEWEB, ID, NOM, COM, IDPORTAL)" & vbCrLf
sConsulta = sConsulta & "   SELECT ANYO, GMN1, PROCE, PROVE, @NUMOFEWEB, ID, NOM, COM, IDPORTAL" & vbCrLf
sConsulta = sConsulta & "   FROM PROCE_OFE_ADJUN WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "   WHERE ANYO = @ANYO AND GMN1=@GMN1 AND PROCE = @PROCE AND PROVE = @PROVE AND OFE = @NEWOFE" & vbCrLf
sConsulta = sConsulta & "   IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_OFE SET ULT = 0 WHERE ANYO = @ANYO AND GMN1 = @GMN1 AND PROCE =@PROCE AND PROVE=@PROVE AND NUM!=@NEWOFE" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "/* edu tarea 525 Crear marca de lectura pendiente para la nueva oferta y eliminar marcas anteriores para esa oferta */" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--paso 1. eliminar posibles marcas de ese proveedor para ese proceso de compra" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM OFE_LEIDO " & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "    ANYO = @ANYO " & vbCrLf
sConsulta = sConsulta & "AND GMN1 = @GMN1 " & vbCrLf
sConsulta = sConsulta & "AND PROCE =@PROCE  " & vbCrLf
sConsulta = sConsulta & "AND PROVE=@PROVE  " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- paso 2. Dar de alta la nueva marca para el comprador" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO OFE_LEIDO (ANYO, GMN1, PROCE, PROVE, OFE, PER, RESPONSABLE, LEIDO, FECACT)" & vbCrLf
sConsulta = sConsulta & "SELECT " & vbCrLf
sConsulta = sConsulta & "   ANYO, " & vbCrLf
sConsulta = sConsulta & "   GMN1, " & vbCrLf
sConsulta = sConsulta & "   PROCECOD AS PROCE, " & vbCrLf
sConsulta = sConsulta & "   PROVECOD AS PROVE, " & vbCrLf
sConsulta = sConsulta & "   OFE, " & vbCrLf
sConsulta = sConsulta & "   COM AS PER, " & vbCrLf
sConsulta = sConsulta & "   0 AS RESPONSABLE, " & vbCrLf
sConsulta = sConsulta & "   0 AS LEIDO, " & vbCrLf
sConsulta = sConsulta & "   FECREC AS FECACT" & vbCrLf
sConsulta = sConsulta & "FROM VIEW_OFERTAS WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "    ANYO = @ANYO " & vbCrLf
sConsulta = sConsulta & "AND GMN1 = @GMN1 " & vbCrLf
sConsulta = sConsulta & "AND PROCECOD =@PROCE  " & vbCrLf
sConsulta = sConsulta & "AND PROVECOD =@PROVE  " & vbCrLf
sConsulta = sConsulta & "AND OFE=@NEWOFE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- paso 3. Dar de alta la nueva marca para el responsable cuando es diferente del comprador" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO OFE_LEIDO " & vbCrLf
sConsulta = sConsulta & "SELECT " & vbCrLf
sConsulta = sConsulta & "   ANYO, " & vbCrLf
sConsulta = sConsulta & "   GMN1, " & vbCrLf
sConsulta = sConsulta & "   PROCECOD AS PROCE, " & vbCrLf
sConsulta = sConsulta & "   PROVECOD AS PROVE, " & vbCrLf
sConsulta = sConsulta & "   OFE, " & vbCrLf
sConsulta = sConsulta & "   VIEW_OFERTAS.RESPONSABLE AS PER, " & vbCrLf
sConsulta = sConsulta & "   1 AS RESPONSABLE, " & vbCrLf
sConsulta = sConsulta & "   0 AS LEIDO, " & vbCrLf
sConsulta = sConsulta & "   FECREC AS FECACT" & vbCrLf
sConsulta = sConsulta & "FROM VIEW_OFERTAS WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE " & vbCrLf
sConsulta = sConsulta & "    ANYO = @ANYO " & vbCrLf
sConsulta = sConsulta & "AND GMN1 = @GMN1 " & vbCrLf
sConsulta = sConsulta & "AND PROCECOD =@PROCE  " & vbCrLf
sConsulta = sConsulta & "AND PROVECOD =@PROVE  " & vbCrLf
sConsulta = sConsulta & "AND OFE = @NEWOFE" & vbCrLf
sConsulta = sConsulta & "AND VIEW_OFERTAS.COM <> VIEW_OFERTAS.RESPONSABLE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR!=0 GOTO ERROR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "/* fin de tarea 525 */" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "COMMIT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "GOTO ELIMINAR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ERROR_:" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @ERROR = 100" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ROLLBACK" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "GOTO ELIMINAR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ERRORSINTRAN_:" & vbCrLf
sConsulta = sConsulta & "SET @ERROR = 200" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ELIMINAR_:" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC NUM_PROCE_PUB @PROVE =@PROVE ,@NUM = @NUM OUTPUT,@NUMNUE = @NUMNUE OUTPUT ,@NUMAREV = @NUMAREV OUTPUT " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM P_ADJUN_FIRMA_DIGITAL FROM P_ADJUN_FIRMA_DIGITAL WITH (NOLOCK) INNER JOIN P_PROCE_OFE WITH (NOLOCK) ON P_PROCE_OFE.ADJUN_FIRMA_DIGITAL = P_ADJUN_FIRMA_DIGITAL.ID WHERE P_PROCE_OFE.ID = @ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM P_PROCE_OFE WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM P_GRUPO_OFE WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM P_ITEM_OFEESC WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM P_ITEM_OFE WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM P_OFE_ATRIB WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM P_OFE_GR_ATRIB WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM P_OFE_ITEM_ATRIBESC WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM P_OFE_ITEM_ATRIB WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM P_PROCE_OFE_ADJUN WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM P_OFE_GR_ADJUN WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM P_OFE_ITEM_ADJUN WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "DELETE FROM P_OFE_GR_COSTESDESC WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSWS_CTRLADJUN]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSWS_CTRLADJUN]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSWS_CTRLADJUN] @TIPO INT, @ID INT, @INSTANCIA INT=0, @PROVEEDOR VARCHAR(50)=NULL,@CAMPO INT, @NOMBRE VARCHAR(300), @FECHA DATETIME, @ESTODOS TINYINT" & vbCrLf
sConsulta = sConsulta & ",@USAIDCONTRATO TINYINT=0,@ESARCHCONTRATO TINYINT=0,@ARCHCONTRATOBD TINYINT=0" & vbCrLf
sConsulta = sConsulta & ",@ESALTAFACTURA TINYINT=0 AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   DECLARE @DEDONDE TINYINT" & vbCrLf
sConsulta = sConsulta & "   DECLARE @SOLICITUD INT=0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   /*" & vbCrLf
sConsulta = sConsulta & "   Para hacer control de fecha hora sin milisegundos. Convert(..110) da mm-dd-yyyy Y Convert(...108) da hh:mm:ss. En conjunto mm-dd-yyyy hh:mm:ss" & vbCrLf
sConsulta = sConsulta & "   */" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @INSTANCIA<=0 " & vbCrLf
sConsulta = sConsulta & "   BEGIN  " & vbCrLf
sConsulta = sConsulta & "       --Aqui SOLO puede ser CERTIFICADO AUTOMATICO/ALTA FACTURA, resto casos hay Instancia. " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "       --Se recibe en negativo para controlar el caso especial CERTIFICADO AUTOMATICO/ALTA FACTURA" & vbCrLf
sConsulta = sConsulta & "       SET @INSTANCIA = @INSTANCIA * (-1)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       --A falta de Instancia, se usa la solicitud. " & vbCrLf
sConsulta = sConsulta & "       IF @ESALTAFACTURA=0" & vbCrLf
sConsulta = sConsulta & "           SELECT @SOLICITUD=SOLICITUD FROM INSTANCIA I WITH(NOLOCK) WHERE ID=@INSTANCIA" & vbCrLf
sConsulta = sConsulta & "       ELSE" & vbCrLf
sConsulta = sConsulta & "           SET @SOLICITUD=@INSTANCIA" & vbCrLf
sConsulta = sConsulta & "               " & vbCrLf
sConsulta = sConsulta & "       IF @TIPO=1 -- Datos estan en FORMULARIO. Es un Campo -> CAMPO_ADJUN" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           -- No se hace control de proveedor pq no hay de donde tirar" & vbCrLf
sConsulta = sConsulta & "           IF (SELECT COUNT(1) " & vbCrLf
sConsulta = sConsulta & "           FROM CAMPO_ADJUN A WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN FORM_CAMPO FC WITH(NOLOCK) ON FC.ID=A.CAMPO AND FC.ID=@CAMPO" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN FORM_GRUPO FG WITH(NOLOCK) ON FG.ID=FC.GRUPO" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN SOLICITUD S WITH(NOLOCK) ON S.FORMULARIO=FG.FORMULARIO AND S.ID=@SOLICITUD" & vbCrLf
sConsulta = sConsulta & "           WHERE A.ID = @ID AND (@ESTODOS=1 OR (A.NOM=@NOMBRE AND CONVERT(VARCHAR,A.FECALTA, 110)=CONVERT(VARCHAR,@FECHA, 110) AND CONVERT(VARCHAR,A.FECALTA, 108)=CONVERT(VARCHAR,@FECHA, 108))))>=1" & vbCrLf
sConsulta = sConsulta & "               SELECT 1 AS ACEPTABLE" & vbCrLf
sConsulta = sConsulta & "           ELSE" & vbCrLf
sConsulta = sConsulta & "               SELECT 0 AS ACEPTABLE" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "       IF @TIPO=2 OR @TIPO=4  -- LO HAS METIDO T�. Solo esta en PM_COPIA_ADJUN. 2->campo 4->desglose" & vbCrLf
sConsulta = sConsulta & "       BEGIN           " & vbCrLf
sConsulta = sConsulta & "           --Control de proveedor en pm_copia_adjun" & vbCrLf
sConsulta = sConsulta & "           IF (SELECT COUNT(1) " & vbCrLf
sConsulta = sConsulta & "           FROM PM_COPIA_ADJUN PM WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN FORM_CAMPO FC WITH(NOLOCK) ON FC.ID=@CAMPO           " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN FORM_GRUPO FG WITH(NOLOCK) ON FG.ID=FC.GRUPO" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN SOLICITUD S WITH(NOLOCK) ON S.FORMULARIO=FG.FORMULARIO AND S.ID=@SOLICITUD" & vbCrLf
sConsulta = sConsulta & "           WHERE PM.ID = @ID AND PROVE=@PROVEEDOR AND (@ESTODOS=1 OR (PM.NOM=@NOMBRE AND CONVERT(VARCHAR,PM.FECALTA, 110)=CONVERT(VARCHAR,@FECHA, 110) AND CONVERT(VARCHAR,PM.FECALTA, 108)=CONVERT(VARCHAR,@FECHA, 108))))>=1" & vbCrLf
sConsulta = sConsulta & "               SELECT 1 AS ACEPTABLE" & vbCrLf
sConsulta = sConsulta & "           ELSE" & vbCrLf
sConsulta = sConsulta & "               SELECT 0 AS ACEPTABLE" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "       IF @TIPO=3-- Datos estan en FORMULARIO. Es un Desglose" & vbCrLf
sConsulta = sConsulta & "       BEGIN           " & vbCrLf
sConsulta = sConsulta & "           -- No se hace control de proveedor pq no hay de donde tirar ->LINEA_DESGLOSE_ADJUN" & vbCrLf
sConsulta = sConsulta & "           IF (SELECT COUNT(1) " & vbCrLf
sConsulta = sConsulta & "           FROM LINEA_DESGLOSE_ADJUN LA WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN FORM_CAMPO FC WITH(NOLOCK) ON FC.ID=LA.CAMPO_HIJO AND FC.ID=@CAMPO" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN FORM_GRUPO FG WITH(NOLOCK) ON FG.ID=FC.GRUPO" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN SOLICITUD S WITH(NOLOCK) ON S.FORMULARIO=FG.FORMULARIO AND S.ID=@SOLICITUD" & vbCrLf
sConsulta = sConsulta & "           WHERE LA.ID = @ID AND (@ESTODOS=1 OR (LA.NOM=@NOMBRE AND CONVERT(VARCHAR,LA.FECALTA, 110)=CONVERT(VARCHAR,@FECHA, 110) AND CONVERT(VARCHAR,LA.FECALTA, 108)=CONVERT(VARCHAR,@FECHA, 108))))>=1" & vbCrLf
sConsulta = sConsulta & "               SELECT 1 AS ACEPTABLE" & vbCrLf
sConsulta = sConsulta & "           ELSE" & vbCrLf
sConsulta = sConsulta & "               SELECT 0 AS ACEPTABLE" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "   BEGIN " & vbCrLf
sConsulta = sConsulta & "       /*" & vbCrLf
sConsulta = sConsulta & "       @DEDONDE=1 Contrato" & vbCrLf
sConsulta = sConsulta & "       @DEDONDE=2 Certificado" & vbCrLf
sConsulta = sConsulta & "       @DEDONDE=3 No Conformidad       " & vbCrLf
sConsulta = sConsulta & "       @DEDONDE=0 Eoc" & vbCrLf
sConsulta = sConsulta & "       */" & vbCrLf
sConsulta = sConsulta & "       SET @DEDONDE=0" & vbCrLf
sConsulta = sConsulta & "       IF (SELECT 1 FROM CONTRATO C WITH(NOLOCK) WHERE C.INSTANCIA=@INSTANCIA) = 1" & vbCrLf
sConsulta = sConsulta & "           SET @DEDONDE=1" & vbCrLf
sConsulta = sConsulta & "       ELSE" & vbCrLf
sConsulta = sConsulta & "           IF (SELECT 1 FROM CERTIFICADO C WITH(NOLOCK) WHERE C.INSTANCIA=@INSTANCIA) = 1" & vbCrLf
sConsulta = sConsulta & "               SET @DEDONDE=2" & vbCrLf
sConsulta = sConsulta & "           ELSE" & vbCrLf
sConsulta = sConsulta & "               IF (SELECT 1 FROM NOCONFORMIDAD NC WITH(NOLOCK) WHERE NC.INSTANCIA=@INSTANCIA) = 1" & vbCrLf
sConsulta = sConsulta & "                   SET @DEDONDE=3              " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       IF @TIPO=1-- Datos estan en INSTANCIA. Es un Campo. -> COPIA_CAMPO_ADJUN" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           IF @DEDONDE =1 --Contrato" & vbCrLf
sConsulta = sConsulta & "               --Proveedor de contrato" & vbCrLf
sConsulta = sConsulta & "               IF (SELECT COUNT(1) " & vbCrLf
sConsulta = sConsulta & "               FROM COPIA_CAMPO_ADJUN A WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "               INNER JOIN COPIA_CAMPO CC WITH(NOLOCK) ON CC.ID=A.CAMPO AND CC.INSTANCIA=@INSTANCIA AND CC.ID=@CAMPO" & vbCrLf
sConsulta = sConsulta & "               INNER JOIN CONTRATO C WITH(NOLOCK) ON C.PROVE=@PROVEEDOR AND C.INSTANCIA=@INSTANCIA" & vbCrLf
sConsulta = sConsulta & "               WHERE A.ID = @ID AND (@ESTODOS=1 OR (A.NOM=@NOMBRE AND CONVERT(VARCHAR,A.FECALTA, 110)=CONVERT(VARCHAR,@FECHA, 110) AND CONVERT(VARCHAR,A.FECALTA, 108)=CONVERT(VARCHAR,@FECHA, 108))))>=1" & vbCrLf
sConsulta = sConsulta & "                   SELECT 1 AS ACEPTABLE" & vbCrLf
sConsulta = sConsulta & "               ELSE" & vbCrLf
sConsulta = sConsulta & "                   SELECT 0 AS ACEPTABLE" & vbCrLf
sConsulta = sConsulta & "           IF @DEDONDE =2 --Certificado" & vbCrLf
sConsulta = sConsulta & "               --Proveedor de Certificado" & vbCrLf
sConsulta = sConsulta & "               IF (SELECT COUNT(1) " & vbCrLf
sConsulta = sConsulta & "               FROM COPIA_CAMPO_ADJUN A WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "               INNER JOIN COPIA_CAMPO CC WITH(NOLOCK) ON CC.ID=A.CAMPO AND CC.INSTANCIA=@INSTANCIA AND CC.ID=@CAMPO" & vbCrLf
sConsulta = sConsulta & "               INNER JOIN CERTIFICADO C WITH(NOLOCK) ON C.PROVE=@PROVEEDOR AND C.INSTANCIA=@INSTANCIA" & vbCrLf
sConsulta = sConsulta & "               WHERE A.ID = @ID AND (@ESTODOS=1 OR (A.NOM=@NOMBRE AND CONVERT(VARCHAR,A.FECALTA, 110)=CONVERT(VARCHAR,@FECHA, 110) AND CONVERT(VARCHAR,A.FECALTA, 108)=CONVERT(VARCHAR,@FECHA, 108))))>=1" & vbCrLf
sConsulta = sConsulta & "                   SELECT 1 AS ACEPTABLE" & vbCrLf
sConsulta = sConsulta & "               ELSE" & vbCrLf
sConsulta = sConsulta & "                   SELECT 0 AS ACEPTABLE" & vbCrLf
sConsulta = sConsulta & "           IF @DEDONDE =3 --No Conformidad" & vbCrLf
sConsulta = sConsulta & "               --Proveedor de No Conformidad" & vbCrLf
sConsulta = sConsulta & "               IF (SELECT COUNT(1) " & vbCrLf
sConsulta = sConsulta & "               FROM COPIA_CAMPO_ADJUN A WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "               INNER JOIN COPIA_CAMPO CC WITH(NOLOCK) ON CC.ID=A.CAMPO AND CC.INSTANCIA=@INSTANCIA AND CC.ID=@CAMPO" & vbCrLf
sConsulta = sConsulta & "               INNER JOIN NOCONFORMIDAD NC WITH(NOLOCK) ON NC.PROVE=@PROVEEDOR AND NC.INSTANCIA=@INSTANCIA" & vbCrLf
sConsulta = sConsulta & "               WHERE A.ID = @ID AND (@ESTODOS=1 OR (A.NOM=@NOMBRE AND CONVERT(VARCHAR,A.FECALTA, 110)=CONVERT(VARCHAR,@FECHA, 110) AND CONVERT(VARCHAR,A.FECALTA, 108)=CONVERT(VARCHAR,@FECHA, 108))))>=1" & vbCrLf
sConsulta = sConsulta & "                   SELECT 1 AS ACEPTABLE" & vbCrLf
sConsulta = sConsulta & "               ELSE" & vbCrLf
sConsulta = sConsulta & "                   SELECT 0 AS ACEPTABLE" & vbCrLf
sConsulta = sConsulta & "           IF @DEDONDE =0 --Resto" & vbCrLf
sConsulta = sConsulta & "               --Proveedor de instancia_rol ,tipo rol pet prove o prove" & vbCrLf
sConsulta = sConsulta & "               IF (SELECT COUNT(1) " & vbCrLf
sConsulta = sConsulta & "               FROM COPIA_CAMPO_ADJUN A WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "               INNER JOIN COPIA_CAMPO CC WITH(NOLOCK) ON CC.ID=A.CAMPO AND CC.INSTANCIA=@INSTANCIA AND CC.ID=@CAMPO    " & vbCrLf
sConsulta = sConsulta & "               INNER JOIN INSTANCIA_ROL IR WITH(NOLOCK) ON IR.PROVE=@PROVEEDOR AND IR.INSTANCIA=@INSTANCIA" & vbCrLf
sConsulta = sConsulta & "               INNER JOIN PM_COPIA_ROL CR WITH(NOLOCK) ON CR.ID=IR.ROL AND CR.TIPO in (2,8) " & vbCrLf
sConsulta = sConsulta & "               WHERE A.ID = @ID AND (@ESTODOS=1 OR (A.NOM=@NOMBRE AND CONVERT(VARCHAR,A.FECALTA, 110)=CONVERT(VARCHAR,@FECHA, 110) AND CONVERT(VARCHAR,A.FECALTA, 108)=CONVERT(VARCHAR,@FECHA, 108))))>=1" & vbCrLf
sConsulta = sConsulta & "                   SELECT 1 AS ACEPTABLE" & vbCrLf
sConsulta = sConsulta & "               ELSE" & vbCrLf
sConsulta = sConsulta & "                   SELECT 0 AS ACEPTABLE           " & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "       IF @TIPO=2 OR @TIPO=4-- LO HAS METIDO T�. Solo esta en PM_COPIA_ADJUN. 2->campo 4->desglose" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           IF @DEDONDE =1 --Contrato" & vbCrLf
sConsulta = sConsulta & "           BEGIN" & vbCrLf
sConsulta = sConsulta & "               --Proveedor de contrato" & vbCrLf
sConsulta = sConsulta & "               /*" & vbCrLf
sConsulta = sConsulta & "               Todo este condicionado sale de Root.vb/Adjunto_LeerContratoAdjunto. " & vbCrLf
sConsulta = sConsulta & "               Y lo q vi yo, de q a veces entras y lo tienes, a veces lo metes t�." & vbCrLf
sConsulta = sConsulta & "               Y lo q vi yo, tambien puedes tener en el contrato, archivos no de sistema Archivo Contrato" & vbCrLf
sConsulta = sConsulta & "               */" & vbCrLf
sConsulta = sConsulta & "               IF @ESARCHCONTRATO=0 --no es sistema Archivo Contrato. Archivo normal." & vbCrLf
sConsulta = sConsulta & "               BEGIN" & vbCrLf
sConsulta = sConsulta & "                   IF (SELECT COUNT(1) " & vbCrLf
sConsulta = sConsulta & "                   FROM PM_COPIA_ADJUN PM WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                   INNER JOIN CONTRATO C WITH(NOLOCK) ON C.PROVE=@PROVEEDOR AND C.INSTANCIA=@INSTANCIA" & vbCrLf
sConsulta = sConsulta & "                   INNER JOIN COPIA_CAMPO CC WITH(NOLOCK) ON CC.INSTANCIA=@INSTANCIA AND CC.ID=@CAMPO" & vbCrLf
sConsulta = sConsulta & "                   WHERE PM.ID = @ID AND (@ESTODOS=1 OR (PM.NOM=@NOMBRE AND CONVERT(VARCHAR,PM.FECALTA, 110)=CONVERT(VARCHAR,@FECHA, 110) AND CONVERT(VARCHAR,PM.FECALTA, 108)=CONVERT(VARCHAR,@FECHA, 108))))>=1" & vbCrLf
sConsulta = sConsulta & "                       SELECT 1 AS ACEPTABLE" & vbCrLf
sConsulta = sConsulta & "                   ELSE" & vbCrLf
sConsulta = sConsulta & "                       SELECT 0 AS ACEPTABLE                   " & vbCrLf
sConsulta = sConsulta & "               END" & vbCrLf
sConsulta = sConsulta & "               ELSE" & vbCrLf
sConsulta = sConsulta & "               BEGIN --es sistema Archivo Contrato" & vbCrLf
sConsulta = sConsulta & "                   IF @ARCHCONTRATOBD=0 --solo esta en pm_copia_adjun" & vbCrLf
sConsulta = sConsulta & "                   BEGIN" & vbCrLf
sConsulta = sConsulta & "                       IF @USAIDCONTRATO=0 --@ID es PM.ID" & vbCrLf
sConsulta = sConsulta & "                           IF (SELECT COUNT(1) " & vbCrLf
sConsulta = sConsulta & "                           FROM PM_COPIA_ADJUN PM WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                           INNER JOIN CONTRATO C WITH(NOLOCK) ON C.PROVE=@PROVEEDOR AND C.INSTANCIA=@INSTANCIA" & vbCrLf
sConsulta = sConsulta & "                           INNER JOIN COPIA_CAMPO CC WITH(NOLOCK) ON CC.INSTANCIA=@INSTANCIA AND CC.ID=@CAMPO" & vbCrLf
sConsulta = sConsulta & "                           WHERE PM.ID = @ID AND (@ESTODOS=1 OR (PM.NOM=@NOMBRE AND CONVERT(VARCHAR,PM.FECALTA, 110)=CONVERT(VARCHAR,@FECHA, 110) AND CONVERT(VARCHAR,PM.FECALTA, 108)=CONVERT(VARCHAR,@FECHA, 108))))>=1" & vbCrLf
sConsulta = sConsulta & "                               SELECT 1 AS ACEPTABLE" & vbCrLf
sConsulta = sConsulta & "                           ELSE" & vbCrLf
sConsulta = sConsulta & "                               SELECT 0 AS ACEPTABLE" & vbCrLf
sConsulta = sConsulta & "                       IF @USAIDCONTRATO=1 --@ID es PM.ID_CONTRATO" & vbCrLf
sConsulta = sConsulta & "                           IF (SELECT COUNT(1) " & vbCrLf
sConsulta = sConsulta & "                           FROM PM_COPIA_ADJUN PM WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                           INNER JOIN CONTRATO C WITH(NOLOCK) ON C.PROVE=@PROVEEDOR AND C.INSTANCIA=@INSTANCIA" & vbCrLf
sConsulta = sConsulta & "                           INNER JOIN COPIA_CAMPO CC WITH(NOLOCK) ON CC.INSTANCIA=@INSTANCIA AND CC.ID=@CAMPO" & vbCrLf
sConsulta = sConsulta & "                           WHERE PM.ID_CONTRATO = @ID AND (@ESTODOS=1 OR (PM.NOM=@NOMBRE AND CONVERT(VARCHAR,PM.FECALTA, 110)=CONVERT(VARCHAR,@FECHA, 110) AND CONVERT(VARCHAR,PM.FECALTA, 108)=CONVERT(VARCHAR,@FECHA, 108))))>=1" & vbCrLf
sConsulta = sConsulta & "                               SELECT 1 AS ACEPTABLE" & vbCrLf
sConsulta = sConsulta & "                           ELSE" & vbCrLf
sConsulta = sConsulta & "                               SELECT 0 AS ACEPTABLE" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "                   ELSE --ya esta en instancia. Mayor control de @campo e @Instancia" & vbCrLf
sConsulta = sConsulta & "                   BEGIN " & vbCrLf
sConsulta = sConsulta & "                       IF @USAIDCONTRATO=0 --@ID es PM.ID" & vbCrLf
sConsulta = sConsulta & "                           IF (SELECT COUNT(1) " & vbCrLf
sConsulta = sConsulta & "                           FROM PM_COPIA_ADJUN PM WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                           INNER JOIN COPIA_CAMPO_ADJUN A WITH(NOLOCK) ON A.ADJUN=PM.ID" & vbCrLf
sConsulta = sConsulta & "                           INNER JOIN COPIA_CAMPO CC WITH(NOLOCK) ON CC.ID=A.CAMPO AND CC.INSTANCIA=@INSTANCIA AND CC.ID=@CAMPO" & vbCrLf
sConsulta = sConsulta & "                           INNER JOIN CONTRATO C WITH(NOLOCK) ON C.PROVE=@PROVEEDOR AND C.INSTANCIA=@INSTANCIA" & vbCrLf
sConsulta = sConsulta & "                           WHERE PM.ID = @ID AND (@ESTODOS=1 OR (PM.NOM=@NOMBRE AND CONVERT(VARCHAR,PM.FECALTA, 110)=CONVERT(VARCHAR,@FECHA, 110) AND CONVERT(VARCHAR,PM.FECALTA, 108)=CONVERT(VARCHAR,@FECHA, 108))))>=1" & vbCrLf
sConsulta = sConsulta & "                               SELECT 1 AS ACEPTABLE" & vbCrLf
sConsulta = sConsulta & "                           ELSE" & vbCrLf
sConsulta = sConsulta & "                               SELECT 0 AS ACEPTABLE" & vbCrLf
sConsulta = sConsulta & "                       IF @USAIDCONTRATO=1 --@ID es PM.ID_CONTRATO" & vbCrLf
sConsulta = sConsulta & "                           IF (SELECT COUNT(1) " & vbCrLf
sConsulta = sConsulta & "                           FROM PM_COPIA_ADJUN PM WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                           INNER JOIN COPIA_CAMPO_ADJUN A WITH(NOLOCK) ON A.ADJUN=PM.ID" & vbCrLf
sConsulta = sConsulta & "                           INNER JOIN COPIA_CAMPO CC WITH(NOLOCK) ON CC.ID=A.CAMPO AND CC.INSTANCIA=@INSTANCIA AND CC.ID=@CAMPO" & vbCrLf
sConsulta = sConsulta & "                           INNER JOIN CONTRATO C WITH(NOLOCK) ON C.PROVE=@PROVEEDOR AND C.INSTANCIA=@INSTANCIA" & vbCrLf
sConsulta = sConsulta & "                           WHERE PM.ID_CONTRATO = @ID AND (@ESTODOS=1 OR (PM.NOM=@NOMBRE AND CONVERT(VARCHAR,PM.FECALTA, 110)=CONVERT(VARCHAR,@FECHA, 110) AND CONVERT(VARCHAR,PM.FECALTA, 108)=CONVERT(VARCHAR,@FECHA, 108))))>=1" & vbCrLf
sConsulta = sConsulta & "                               SELECT 1 AS ACEPTABLE" & vbCrLf
sConsulta = sConsulta & "                           ELSE" & vbCrLf
sConsulta = sConsulta & "                               SELECT 0 AS ACEPTABLE" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               END" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "           IF @DEDONDE =2--Certificado" & vbCrLf
sConsulta = sConsulta & "               --Proveedor de Certificado" & vbCrLf
sConsulta = sConsulta & "               IF (SELECT COUNT(1) " & vbCrLf
sConsulta = sConsulta & "               FROM PM_COPIA_ADJUN PM WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "               INNER JOIN CERTIFICADO C WITH(NOLOCK) ON C.PROVE=@PROVEEDOR AND C.INSTANCIA=@INSTANCIA" & vbCrLf
sConsulta = sConsulta & "               INNER JOIN COPIA_CAMPO CC WITH(NOLOCK) ON CC.INSTANCIA=@INSTANCIA AND CC.ID=@CAMPO" & vbCrLf
sConsulta = sConsulta & "               WHERE PM.ID = @ID AND (@ESTODOS=1 OR (PM.NOM=@NOMBRE AND CONVERT(VARCHAR,PM.FECALTA, 110)=CONVERT(VARCHAR,@FECHA, 110) AND CONVERT(VARCHAR,PM.FECALTA, 108)=CONVERT(VARCHAR,@FECHA, 108))))>=1" & vbCrLf
sConsulta = sConsulta & "                   SELECT 1 AS ACEPTABLE" & vbCrLf
sConsulta = sConsulta & "               ELSE" & vbCrLf
sConsulta = sConsulta & "                   SELECT 0 AS ACEPTABLE" & vbCrLf
sConsulta = sConsulta & "           IF @DEDONDE =3--No Conformidad" & vbCrLf
sConsulta = sConsulta & "               --Proveedor de No Conformidad" & vbCrLf
sConsulta = sConsulta & "               IF (SELECT COUNT(1) " & vbCrLf
sConsulta = sConsulta & "               FROM PM_COPIA_ADJUN PM WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "               INNER JOIN NOCONFORMIDAD NC WITH(NOLOCK) ON NC.PROVE=@PROVEEDOR AND NC.INSTANCIA=@INSTANCIA" & vbCrLf
sConsulta = sConsulta & "               INNER JOIN COPIA_CAMPO CC WITH(NOLOCK) ON CC.INSTANCIA=@INSTANCIA AND CC.ID=@CAMPO" & vbCrLf
sConsulta = sConsulta & "               WHERE PM.ID = @ID AND (@ESTODOS=1 OR (PM.NOM=@NOMBRE AND CONVERT(VARCHAR,PM.FECALTA, 110)=CONVERT(VARCHAR,@FECHA, 110) AND CONVERT(VARCHAR,PM.FECALTA, 108)=CONVERT(VARCHAR,@FECHA, 108))))>=1" & vbCrLf
sConsulta = sConsulta & "                   SELECT 1 AS ACEPTABLE" & vbCrLf
sConsulta = sConsulta & "               ELSE" & vbCrLf
sConsulta = sConsulta & "                   SELECT 0 AS ACEPTABLE" & vbCrLf
sConsulta = sConsulta & "           IF @DEDONDE =0--Resto" & vbCrLf
sConsulta = sConsulta & "               --Proveedor de instancia_rol ,tipo rol pet prove o prov" & vbCrLf
sConsulta = sConsulta & "               IF (SELECT COUNT(1) " & vbCrLf
sConsulta = sConsulta & "               FROM PM_COPIA_ADJUN PM WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "               INNER JOIN INSTANCIA_ROL IR WITH(NOLOCK) ON IR.PROVE=@PROVEEDOR AND IR.INSTANCIA=@INSTANCIA" & vbCrLf
sConsulta = sConsulta & "               INNER JOIN COPIA_CAMPO CC WITH(NOLOCK) ON CC.INSTANCIA=@INSTANCIA AND CC.ID=@CAMPO" & vbCrLf
sConsulta = sConsulta & "               INNER JOIN PM_COPIA_ROL CR WITH(NOLOCK) ON CR.ID=IR.ROL AND CR.TIPO IN (2,8)" & vbCrLf
sConsulta = sConsulta & "               WHERE PM.ID = @ID AND (@ESTODOS=1 OR (PM.NOM=@NOMBRE AND CONVERT(VARCHAR,PM.FECALTA, 110)=CONVERT(VARCHAR,@FECHA, 110) AND CONVERT(VARCHAR,PM.FECALTA, 108)=CONVERT(VARCHAR,@FECHA, 108))))>=1" & vbCrLf
sConsulta = sConsulta & "                   SELECT 1 AS ACEPTABLE" & vbCrLf
sConsulta = sConsulta & "               ELSE" & vbCrLf
sConsulta = sConsulta & "                   SELECT 0 AS ACEPTABLE           " & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "       IF @TIPO=3-- Datos estan en INSTANCIA. Es un Desglose" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           IF @DEDONDE =1 --Contrato" & vbCrLf
sConsulta = sConsulta & "               --Proveedor de contrato" & vbCrLf
sConsulta = sConsulta & "               IF (SELECT COUNT(1) " & vbCrLf
sConsulta = sConsulta & "               FROM COPIA_LINEA_DESGLOSE_ADJUN LA WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "               INNER JOIN COPIA_CAMPO CC WITH(NOLOCK) ON CC.ID=LA.CAMPO_HIJO AND CC.INSTANCIA=@INSTANCIA AND CC.ID=@CAMPO" & vbCrLf
sConsulta = sConsulta & "               INNER JOIN CONTRATO C WITH(NOLOCK) ON C.PROVE=@PROVEEDOR AND C.INSTANCIA=@INSTANCIA" & vbCrLf
sConsulta = sConsulta & "               WHERE LA.ID = @ID AND (@ESTODOS=1 OR (LA.NOM=@NOMBRE AND CONVERT(VARCHAR,LA.FECALTA, 110)=CONVERT(VARCHAR,@FECHA, 110) AND CONVERT(VARCHAR,LA.FECALTA, 108)=CONVERT(VARCHAR,@FECHA, 108))))>=1" & vbCrLf
sConsulta = sConsulta & "                   SELECT 1 AS ACEPTABLE" & vbCrLf
sConsulta = sConsulta & "               ELSE" & vbCrLf
sConsulta = sConsulta & "                   SELECT 0 AS ACEPTABLE" & vbCrLf
sConsulta = sConsulta & "           IF @DEDONDE =2--Certificado" & vbCrLf
sConsulta = sConsulta & "               --Proveedor de Certificado" & vbCrLf
sConsulta = sConsulta & "               IF (SELECT COUNT(1) " & vbCrLf
sConsulta = sConsulta & "               FROM COPIA_LINEA_DESGLOSE_ADJUN LA WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "               INNER JOIN COPIA_CAMPO CC WITH(NOLOCK) ON CC.ID=LA.CAMPO_HIJO AND CC.INSTANCIA=@INSTANCIA AND CC.ID=@CAMPO" & vbCrLf
sConsulta = sConsulta & "               INNER JOIN CERTIFICADO C WITH(NOLOCK) ON C.PROVE=@PROVEEDOR AND C.INSTANCIA=@INSTANCIA" & vbCrLf
sConsulta = sConsulta & "               WHERE LA.ID = @ID AND (@ESTODOS=1 OR (LA.NOM=@NOMBRE AND CONVERT(VARCHAR,LA.FECALTA, 110)=CONVERT(VARCHAR,@FECHA, 110) AND CONVERT(VARCHAR,LA.FECALTA, 108)=CONVERT(VARCHAR,@FECHA, 108))))>=1" & vbCrLf
sConsulta = sConsulta & "                   SELECT 1 AS ACEPTABLE" & vbCrLf
sConsulta = sConsulta & "               ELSE" & vbCrLf
sConsulta = sConsulta & "                   SELECT 0 AS ACEPTABLE" & vbCrLf
sConsulta = sConsulta & "           IF @DEDONDE =3--No Conformidad" & vbCrLf
sConsulta = sConsulta & "               --Proveedor de No Conformidad" & vbCrLf
sConsulta = sConsulta & "               IF (SELECT COUNT(1) " & vbCrLf
sConsulta = sConsulta & "               FROM COPIA_LINEA_DESGLOSE_ADJUN LA WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "               INNER JOIN COPIA_CAMPO CC WITH(NOLOCK) ON CC.ID=LA.CAMPO_HIJO AND CC.INSTANCIA=@INSTANCIA AND CC.ID=@CAMPO" & vbCrLf
sConsulta = sConsulta & "               INNER JOIN NOCONFORMIDAD NC WITH(NOLOCK) ON NC.PROVE=@PROVEEDOR AND NC.INSTANCIA=@INSTANCIA" & vbCrLf
sConsulta = sConsulta & "               WHERE LA.ID = @ID AND (@ESTODOS=1 OR (LA.NOM=@NOMBRE AND CONVERT(VARCHAR,LA.FECALTA, 110)=CONVERT(VARCHAR,@FECHA, 110) AND CONVERT(VARCHAR,LA.FECALTA, 108)=CONVERT(VARCHAR,@FECHA, 108))))>=1" & vbCrLf
sConsulta = sConsulta & "                   SELECT 1 AS ACEPTABLE" & vbCrLf
sConsulta = sConsulta & "               ELSE" & vbCrLf
sConsulta = sConsulta & "                   SELECT 0 AS ACEPTABLE" & vbCrLf
sConsulta = sConsulta & "           IF @DEDONDE =0 --Resto" & vbCrLf
sConsulta = sConsulta & "               --Proveedor de instancia_rol ,tipo rol prove" & vbCrLf
sConsulta = sConsulta & "               IF (SELECT COUNT(1) " & vbCrLf
sConsulta = sConsulta & "               FROM COPIA_LINEA_DESGLOSE_ADJUN LA WITH (NOLOCK) " & vbCrLf
sConsulta = sConsulta & "               INNER JOIN COPIA_CAMPO CC WITH(NOLOCK) ON CC.ID=LA.CAMPO_HIJO AND CC.INSTANCIA=@INSTANCIA AND CC.ID=@CAMPO" & vbCrLf
sConsulta = sConsulta & "               INNER JOIN INSTANCIA_ROL IR WITH(NOLOCK) ON IR.PROVE=@PROVEEDOR AND IR.INSTANCIA=@INSTANCIA" & vbCrLf
sConsulta = sConsulta & "               INNER JOIN PM_COPIA_ROL CR WITH(NOLOCK) ON CR.ID=IR.ROL AND CR.TIPO IN(2,8)" & vbCrLf
sConsulta = sConsulta & "               WHERE LA.ID = @ID AND (@ESTODOS=1 OR (LA.NOM=@NOMBRE AND CONVERT(VARCHAR,LA.FECALTA, 110)=CONVERT(VARCHAR,@FECHA, 110) AND CONVERT(VARCHAR,LA.FECALTA, 108)=CONVERT(VARCHAR,@FECHA, 108))))>=1" & vbCrLf
sConsulta = sConsulta & "                   SELECT 1 AS ACEPTABLE" & vbCrLf
sConsulta = sConsulta & "               ELSE" & vbCrLf
sConsulta = sConsulta & "                   SELECT 0 AS ACEPTABLE           " & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSWS_CERRAR_SOLICITUD]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSWS_CERRAR_SOLICITUD]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSWS_CERRAR_SOLICITUD] " & vbCrLf
sConsulta = sConsulta & "   @ID INT, " & vbCrLf
sConsulta = sConsulta & "   @PER VARCHAR(50), " & vbCrLf
sConsulta = sConsulta & "   @MOTIVO VARCHAR(500) " & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   DECLARE @ACTIVLOG AS INTEGER" & vbCrLf
sConsulta = sConsulta & "   DECLARE @ORIGEN AS INTEGER" & vbCrLf
sConsulta = sConsulta & "   DECLARE @USAR_ORGCOMPRAS AS INTEGER" & vbCrLf
sConsulta = sConsulta & "   DECLARE @ERP AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "   DECLARE @USU AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO INSTANCIA_EST(INSTANCIA,PER, COMENT, FECHA,EST)" & vbCrLf
sConsulta = sConsulta & "   VALUES (@ID,@PER,@MOTIVO, GETDATE(),104)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   UPDATE INSTANCIA SET ESTADO=104 WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   --Guardar en la Tabla LOG_INSTANCIA si la integraci?n est? activa y el sentido es a ERP o en ambos." & vbCrLf
sConsulta = sConsulta & "   ----El ERP lo deduce:" & vbCrLf
sConsulta = sConsulta & "   -------- Si est? activo el par?metro de usar OrgCompras, busca el campo organizaci?n de compras en un campo fuera de desglose en la solicitud" & vbCrLf
sConsulta = sConsulta & "   -------- Si no est? activo el par?metro de usar OrgCompras, busca un campo de tipo CentroSM fuera de desglose en la solicitud, y obtiene la empresa" & vbCrLf
sConsulta = sConsulta & "   -------- Si no recupera ning?n ERP, asigna por defecto el 1" & vbCrLf
sConsulta = sConsulta & "   SET @ACTIVLOG=(SELECT ACTIVLOG FROM PARGEN_GEST WITH(NOLOCK))" & vbCrLf
sConsulta = sConsulta & "   IF  @ACTIVLOG=1" & vbCrLf
sConsulta = sConsulta & "       SET @ORIGEN=2" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "       SET @ORIGEN=0" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   SET @USU=(SELECT COD FROM USU WITH(NOLOCK) WHERE PER=@PER)" & vbCrLf
sConsulta = sConsulta & "   SET @USAR_ORGCOMPRAS=(SELECT USAR_ORGCOMPRAS FROM PARGEN_INTERNO WITH(NOLOCK))" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   IF @USAR_ORGCOMPRAS=1" & vbCrLf
sConsulta = sConsulta & "       SET @ERP=(SELECT DISTINCT ERP FROM ERP_ORGCOMPRAS WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN COPIA_LINEA_DESGLOSE CL WITH(NOLOCK) ON CL.VALOR_TEXT=ERP_ORGCOMPRAS.ORGCOMPRAS" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN COPIA_CAMPO C WITH(NOLOCK) ON CL.CAMPO_HIJO=C.ID AND C.INSTANCIA=@ID" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN INSTANCIA I WITH(NOLOCK) ON I.ID = C.INSTANCIA AND I.ULT_VERSION=C.NUM_VERSION" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN FORM_CAMPO FC WITH(NOLOCK) ON FC.ID=C.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "           WHERE FC.TIPO_CAMPO_GS=123)" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       DECLARE @VALOR_TEXT AS VARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "       DECLARE @UON1 AS VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "       DECLARE @UON2 AS VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "       DECLARE @UON3 AS VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "       DECLARE @UON4 AS VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       SET @VALOR_TEXT =(SELECT DISTINCT C.VALOR_TEXT  FROM COPIA_CAMPO C WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN FORM_CAMPO FC WITH(NOLOCK) ON FC.ID=C.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN INSTANCIA WITH(NOLOCK) ON INSTANCIA.ID=C.INSTANCIA AND INSTANCIA.ULT_VERSION=C.NUM_VERSION" & vbCrLf
sConsulta = sConsulta & "       WHERE FC.TIPO_CAMPO_GS=129 AND INSTANCIA.ID=@ID AND C.VALOR_TEXT IS NOT NULL)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       EXEC @UON1=fnSPLIT @VALOR_TEXT, '#', 1" & vbCrLf
sConsulta = sConsulta & "       EXEC @UON2=fnSPLIT @VALOR_TEXT, '#', 2" & vbCrLf
sConsulta = sConsulta & "       EXEC @UON3=fnSPLIT @VALOR_TEXT, '#', 3" & vbCrLf
sConsulta = sConsulta & "       EXEC @UON4=fnSPLIT @VALOR_TEXT, '#', 4" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "       DECLARE @CONSULTA1 AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "       DECLARE @CONSULTA_total AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "       DECLARE @PARAM NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "       SET @CONSULTA1='SELECT @ERP=ERP_SOCIEDAD.ERP FROM ERP_SOCIEDAD WITH(NOLOCK) INNER JOIN EMP WITH(NOLOCK) ON EMP.SOCIEDAD=ERP_SOCIEDAD.SOCIEDAD '" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "       IF @UON1 <> ''" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           SET @CONSULTA_total=@CONSULTA1 + 'INNER JOIN UON1 WITH(NOLOCK) ON UON1.EMPRESA=EMP.ID WHERE UON1.COD=@UON1'" & vbCrLf
sConsulta = sConsulta & "           SET @PARAM = N'@ERP VARCHAR(50) OUT, @UON1 VARCHAR(100)'" & vbCrLf
sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @CONSULTA_total, @PARAM, @ERP OUT, @UON1=@UON1   " & vbCrLf
sConsulta = sConsulta & "           IF @ERP IS NULL AND @UON2 <> ''" & vbCrLf
sConsulta = sConsulta & "           BEGIN" & vbCrLf
sConsulta = sConsulta & "               SET @CONSULTA_total=@CONSULTA1 + 'INNER JOIN UON2 WITH(NOLOCK) ON UON2.EMPRESA=EMP.ID WHERE UON2.UON1=@UON1 AND UON2.COD=@UON2'" & vbCrLf
sConsulta = sConsulta & "               SET @PARAM = N'@ERP VARCHAR(50) OUT, @UON1 VARCHAR(100), @UON2 VARCHAR(100)'" & vbCrLf
sConsulta = sConsulta & "               EXEC SP_EXECUTESQL @CONSULTA_total, @PARAM, @ERP OUT, @UON1=@UON1, @UON2=@UON2" & vbCrLf
sConsulta = sConsulta & "               IF @ERP IS NULL AND @UON3 <> ''" & vbCrLf
sConsulta = sConsulta & "               BEGIN" & vbCrLf
sConsulta = sConsulta & "                   SET @CONSULTA_total=@CONSULTA1 + 'INNER JOIN UON3 WITH(NOLOCK) ON UON3.EMPRESA=EMP.ID WHERE UON3.UON1=@UON1 AND UON3.UON2=@UON2 AND UON3.COD=@UON3'" & vbCrLf
sConsulta = sConsulta & "                   SET @PARAM = N'@ERP VARCHAR(50) OUT, @UON1 VARCHAR(100), @UON2 VARCHAR(100), @UON3 VARCHAR(100)'" & vbCrLf
sConsulta = sConsulta & "                   EXEC SP_EXECUTESQL @CONSULTA_total, @PARAM, @ERP OUT, @UON1=@UON1, @UON2=@UON2, @UON3=@UON3" & vbCrLf
sConsulta = sConsulta & "                   IF @ERP IS NULL AND @UON4 <> ''" & vbCrLf
sConsulta = sConsulta & "                   BEGIN" & vbCrLf
sConsulta = sConsulta & "                       SET @CONSULTA_total=@CONSULTA1 + 'INNER JOIN UON4 WITH(NOLOCK) ON UON4.EMPRESA=EMP.ID WHERE UON4.UON1=@UON1 AND UON4.UON2=@UON2 AND UON4.UON3=@UON3 AND UON4.COD=@UON4'" & vbCrLf
sConsulta = sConsulta & "                       SET @PARAM = N'@ERP VARCHAR(50) OUT, @UON1 VARCHAR(100), @UON2 VARCHAR(100), @UON3 VARCHAR(100), @UON4 VARCHAR(100)'" & vbCrLf
sConsulta = sConsulta & "                       EXEC SP_EXECUTESQL @CONSULTA_total, @PARAM, @ERP OUT, @UON1=@UON1, @UON2=@UON2, @UON3=@UON3, @UON4=@UON4" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               END" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "   IF @ERP IS NULL" & vbCrLf
sConsulta = sConsulta & "       SET @ERP = '1'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF (SELECT ACTIVA FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=18 AND ERP=@ERP) = 1 AND" & vbCrLf
sConsulta = sConsulta & "       ((SELECT SENTIDO FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=18 AND ERP=@ERP) = 1" & vbCrLf
sConsulta = sConsulta & "       OR  (SELECT SENTIDO FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=18 AND ERP=@ERP) = 3)" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       INSERT INTO LOG_INSTANCIA (ACCION,INSTANCIA,SOLICITUD,FEC_ALTA,ESTADO,NUM_VERSION, PETICIONARIO,COMPRADOR,IMPORTE,MON,  USU,CAMBIO,ULT_VERSION,ORIGEN,FECACT, ERP)" & vbCrLf
sConsulta = sConsulta & "       SELECT  'U', @ID, SOLICITUD, FEC_ALTA, ESTADO, ULT_VERSION, PETICIONARIO, COMPRADOR, IMPORTE, MON, @USU, CAMBIO, ULT_VERSION, @ORIGEN, GETDATE(), @ERP FROM INSTANCIA WITH(NOLOCK) WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSQA_LOADCERTIFICADO]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSQA_LOADCERTIFICADO]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSQA_LOADCERTIFICADO] " & vbCrLf
sConsulta = sConsulta & "   @CERTIFICADO INT," & vbCrLf
sConsulta = sConsulta & "   @PORTAL TINYINT = NULL OUTPUT," & vbCrLf
sConsulta = sConsulta & "   @IDIOMA NVARCHAR(3) = 'SPA'" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   DECLARE @INSTANCIA INT" & vbCrLf
sConsulta = sConsulta & "   DECLARE @NUM_VERSION INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SELECT @INSTANCIA = INSTANCIA FROM CERTIFICADO WITH (NOLOCK) WHERE ID = @CERTIFICADO" & vbCrLf
sConsulta = sConsulta & "   --CUIDADO AL OBTENER LA VERSION DE UN CERTIFICADO YA QUE ANTES DE LA 31800_6 VARIOS CERTIFICADOS PODIAN COMPARTIR LA MISMA INSTANCIA POR ESO ESTA EL CAMPO CERTIFICADO EN VERSION_INSTANCIA" & vbCrLf
sConsulta = sConsulta & "   IF  @PORTAL IS NULL AND (SELECT COUNT(INSTANCIA) FROM VERSION_INSTANCIA WITH (NOLOCK) WHERE INSTANCIA=@INSTANCIA AND TIPO=3 AND CERTIFICADO=@CERTIFICADO)>0" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       SELECT @PORTAL=1" & vbCrLf
sConsulta = sConsulta & "       SELECT @NUM_VERSION=NUM_VERSION FROM VERSION_INSTANCIA WITH (NOLOCK) WHERE INSTANCIA=@INSTANCIA AND TIPO=3 AND CERTIFICADO=@CERTIFICADO" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       SELECT @PORTAL=0" & vbCrLf
sConsulta = sConsulta & "       -- Se hace la JOIN con CERTIFICADO, porque antes pod?a haber una instancia para m?s de un certificado, ahora ya no, pero los datos no se han actualizado." & vbCrLf
sConsulta = sConsulta & "       SELECT @NUM_VERSION=MAX(V.NUM_VERSION) FROM VERSION_INSTANCIA V WITH (NOLOCK) INNER JOIN CERTIFICADO C WITH (NOLOCK) ON V.INSTANCIA=C.INSTANCIA WHERE V.TIPO<>3 AND V.CERTIFICADO=@CERTIFICADO" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   DECLARE @SQL NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "   SET @SQL =N'SELECT C.ID_CERTIF ID,C.INSTANCIA,C.FEC_PUBLICACION,C.ID_SOLIC IDSOL,C.DEN_' + @IDIOMA + ' DEN,C.PER,C.NOMBREPETICIONARIO NOMBRE,C.EMAIL,C.FEC_RESPUESTA,C.PUBLICADA,C.BAJA,C.NUM_VERSION," & vbCrLf
sConsulta = sConsulta & "   S.COD AS TIPOCOD,C.CONTACTO,C.USU,C.DEP,C.UON1,C.UON2,C.UON3,C.FEC_DESPUB,C.FEC_LIM_CUMPLIM,ISNULL(C3.VALOR_TEXT,'''') AS NUM_CERTIF," & vbCrLf
sConsulta = sConsulta & "   C.PROVE_COD PROVE,C.PROVE_DEN AS DEN_PROVE,C.BAJA AS BAJA_PROVE,ISNULL(C1.VALOR_TEXT,'''') AS ENTIDAD,C.FEC_EXPIRACION AS FECHA_EXPIR," & vbCrLf
sConsulta = sConsulta & "   I.PETICIONARIO AS IPETICIONARIO, PP.NOM  + '' '' + PP.APE AS NOMBREPETICIONARIO,C.FEC_CUMPLIM,ISNULL(C2.VALOR_TEXT,'''') AS ALCANCE," & vbCrLf
sConsulta = sConsulta & "   S.PDTE_ENVIAR AS ENVIO_CONJUNTO,P.NIF PROVE_NIF,P.DIR PROVE_DIR,P.CP PROVE_CP,P.POB PROVE_POB,PD.DEN PROVE_PROVI,PAI_DEN.DEN PROVE_PAIS," & vbCrLf
sConsulta = sConsulta & "   S.FUERA_PLAZO,C.VALIDADO,C.ID_TEXTO_ESTADO,CASE WHEN DATEDIFF(day,getdate(),C.FEC_LIM_CUMPLIM)<0 THEN 1 ELSE 0 END AS PLAZOCUMPLIMENTACIONSUPERADO," & vbCrLf
sConsulta = sConsulta & "    CASE WHEN DATEDIFF(day,getdate(),C.FEC_EXPIRACION)<0 THEN -1 WHEN DATEDIFF(day,getdate(),C.FEC_EXPIRACION) BETWEEN 0 AND PARQA.FSQA_PROX_EXPIRAR+1 THEN 1 ELSE 0 END AS EXPIRADO" & vbCrLf
sConsulta = sConsulta & "   FROM VIEW_GET_CERTIFICADOS C" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN INSTANCIA I WITH(NOLOCK) ON I.ID=C.INSTANCIA" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN SOLICITUD S WITH(NOLOCK) ON S.ID=C.ID_SOLIC" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PROVE P WITH(NOLOCK) ON P.COD=C.PROVE_COD" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN PER PP WITH (NOLOCK) ON I.PETICIONARIO = PP.COD" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN PROVI_DEN PD WITH (NOLOCK) ON P.PAI = PD.PAI AND P.PROVI = PD.PROVI AND PD.IDIOMA =''' + @IDIOMA + '''" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN PAI_DEN WITH (NOLOCK) ON P.PAI = PAI_DEN.PAI AND PAI_DEN.IDIOMA =''' + @IDIOMA + '''" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN PARGEN_QA PARQA WITH (NOLOCK) ON PARQA.ID=1" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN" & vbCrLf
sConsulta = sConsulta & "   (SELECT M.INSTANCIA, M.NUM_VERSION, C.VALOR_TEXT FROM (SELECT INSTANCIA,NUM_VERSION,MAX(C.ID) CAMPO FROM COPIA_CAMPO C WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN FORM_CAMPO CD WITH (NOLOCK) ON C.COPIA_CAMPO_DEF=CD.ID AND CD.TIPO_CAMPO_GS=24 WHERE C.VALOR_TEXT IS NOT NULL GROUP BY C.INSTANCIA, C.NUM_VERSION) M" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN COPIA_CAMPO C WITH (NOLOCK) ON M.CAMPO = C.ID) C1 ON C.INSTANCIA=C1.INSTANCIA AND C.NUM_VERSION=C1.NUM_VERSION" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN" & vbCrLf
sConsulta = sConsulta & "   (SELECT M.INSTANCIA, M.NUM_VERSION, C.VALOR_TEXT FROM (SELECT INSTANCIA,NUM_VERSION,MAX(C.ID) CAMPO FROM COPIA_CAMPO C WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN FORM_CAMPO CD WITH (NOLOCK) ON C.COPIA_CAMPO_DEF=CD.ID AND CD.TIPO_CAMPO_GS=25 WHERE C.VALOR_TEXT IS NOT NULL GROUP BY C.INSTANCIA, C.NUM_VERSION) M" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN COPIA_CAMPO C WITH (NOLOCK) ON M.CAMPO = C.ID) C2 ON C.INSTANCIA=C2.INSTANCIA AND C.NUM_VERSION=C2.NUM_VERSION" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN" & vbCrLf
sConsulta = sConsulta & "   (SELECT M.INSTANCIA, M.NUM_VERSION, C.VALOR_TEXT FROM (SELECT INSTANCIA,NUM_VERSION,MAX(C.ID) CAMPO FROM COPIA_CAMPO C WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN FORM_CAMPO CD WITH (NOLOCK) ON C.COPIA_CAMPO_DEF=CD.ID AND CD.TIPO_CAMPO_GS=26 WHERE C.VALOR_TEXT IS NOT NULL GROUP BY C.INSTANCIA, C.NUM_VERSION) M" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN COPIA_CAMPO C WITH (NOLOCK) ON M.CAMPO = C.ID) C3 ON C.INSTANCIA=C3.INSTANCIA AND C.NUM_VERSION=C3.NUM_VERSION" & vbCrLf
sConsulta = sConsulta & "   WHERE C.ID_CERTIF=@CERTIFICADO AND (@NUM_VERSION IS NULL OR C.NUM_VERSION=@NUM_VERSION)'" & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL, N'@CERTIFICADO INT, @NUM_VERSION INT', @CERTIFICADO=@CERTIFICADO, @NUM_VERSION = @NUM_VERSION" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSQA_GET_CERTIFICADOS_PROVE]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSQA_GET_CERTIFICADOS_PROVE]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSQA_GET_CERTIFICADOS_PROVE] " & vbCrLf
sConsulta = sConsulta & "   @PROVE VARCHAR(50)  " & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SELECT DISTINCT ID_SOLIC,ID_CERTIF ID,VC.INSTANCIA,VC.NUM_VERSION,VC.DEN_SPA,VC.DEN_GER,VC.DEN_ENG,VC.DEN_FRA," & vbCrLf
sConsulta = sConsulta & "       FEC_PUBLICACION,FEC_RESPUESTA,VC.BAJA_CERTIFICADO,CONTACTO,VC.FEC_ALTA FECALTA,VC.CERTIFICADO FICHERO_ADJUNTO," & vbCrLf
sConsulta = sConsulta & "       EMAIL,EN_PROCESO,FEC_CUMPLIM,FEC_SOLICITUD,FEC_LIM_CUMPLIM,ID_TEXTO_ESTADO,OBLIGATORIO," & vbCrLf
sConsulta = sConsulta & "       ISNULL(VI.INSTANCIA,0) ESALTA,VC.PDTE_ENVIAR,VC.NOMBREPETICIONARIO NOMBRE,VC.PER" & vbCrLf
sConsulta = sConsulta & "   FROM VIEW_GET_CERTIFICADOS VC" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN SOLICITUD S WITH(NOLOCK) ON S.ID=VC.ID_SOLIC" & vbCrLf
sConsulta = sConsulta & "   LEFT JOIN VERSION_INSTANCIA VI WITH(NOLOCK) ON VI.INSTANCIA=VC.INSTANCIA" & vbCrLf
sConsulta = sConsulta & "   WHERE VC.PROVE_COD=@PROVE AND S.PUB=1 AND S.BAJA=0 AND VC.PUBLICADA=1 " & vbCrLf
sConsulta = sConsulta & "       AND ISNULL(S.TIPO_PROVE,VC.CALIDAD_POT)=VC.CALIDAD_POT" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSQA_CERTIF_DATOS_MENSAJE]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSQA_CERTIF_DATOS_MENSAJE]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSQA_CERTIF_DATOS_MENSAJE] @CERTIFICADO INT,@IDI VARCHAR(50),@DAT_COPIACAMPO TINYINT=0 AS   " & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SFROM NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SFROM=N'SELECT DISTINCT C.FEC_PUBLICACION, C.FEC_DESPUB, I.ID AS INSTANCIA, SOLICITUD.COD  AS TIPOCOD,SOLICITUD.DEN_' + @IDI + ' TIPODEN" & vbCrLf
sConsulta = sConsulta & ",SOLICITUD.DESCR_' + @IDI + ' TIPODESCR" & vbCrLf
sConsulta = sConsulta & ",C.FEC_EXPIRACION AS FEC_EXPIRACION" & vbCrLf
sConsulta = sConsulta & ",PET.COD AS PET_COD,PET.NOM AS PET_NOM,PET.APE AS PET_APE,PET.TFNO AS PET_TFNO,PET.EMAIL AS PET_EMAIL,PET.FAX AS PET_FAX,PET.DEP AS PET_DEP_COD,PET_D.DEN AS PET_DEP_DEN" & vbCrLf
sConsulta = sConsulta & ",case when U_PET3.cod is null then" & vbCrLf
sConsulta = sConsulta & "   case when U_PET2.cod is null then" & vbCrLf
sConsulta = sConsulta & "                   CASE WHEN U_PET1.COD IS NULL THEN  ' + '    '' ''  ' + '     ELSE  U_PET1.cod + '' - '' + U_PET1.den  END" & vbCrLf
sConsulta = sConsulta & "               ELSE" & vbCrLf
sConsulta = sConsulta & "       U_PET1.COD + '' - '' + U_PET2.COD + '' - '' + U_PET2.DEN                        " & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   U_PET1.COD + '' - '' + U_PET2.COD + '' - '' + U_PET3.COD + '' - '' + U_PET3.DEN " & vbCrLf
sConsulta = sConsulta & "END PET_UON" & vbCrLf
sConsulta = sConsulta & ",C.FEC_SOLICITUD,C.FEC_LIM_CUMPLIM, P.NIF PROVE_NIF, P.DEN PROVE_DEN, P.DIR PROVE_DIR, P.CP PROVE_CP, P.POB PROVE_POB, PD.DEN PROVE_PROVI, PAI_DEN.DEN PROVE_PAIS '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @DAT_COPIACAMPO =1 " & vbCrLf
sConsulta = sConsulta & "   SET @SFROM=@SFROM + ',C1.VALOR_TEXT AS ENTIDAD, C2.VALOR_TEXT AS ALCANCE, C3.VALOR_TEXT AS NUM_CERTIF'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SFROM=@SFROM + ' FROM CERTIFICADO C WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN INSTANCIA I WITH (NOLOCK) ON C.INSTANCIA=I.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN SOLICITUD WITH (NOLOCK) ON I.SOLICITUD=SOLICITUD.ID " & vbCrLf
sConsulta = sConsulta & "INNER JOIN PER PET WITH (NOLOCK) ON C.PER=PET.COD" & vbCrLf
sConsulta = sConsulta & "INNER JOIN DEP PET_D WITH (NOLOCK) ON PET.DEP=PET_D.COD" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PROVE P WITH (NOLOCK) ON C.PROVE = P.COD" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PROVI_DEN PD WITH (NOLOCK) ON P.PAI = PD.PAI AND P.PROVI = PD.PROVI AND PD.IDIOMA =''' + @IDI + '''" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PAI_DEN WITH (NOLOCK) ON P.PAI = PAI_DEN.PAI AND PAI_DEN.IDIOMA =''' + @IDI + '''" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN UON1 U_PET1 WITH (NOLOCK) ON U_PET1.COD=PET.UON1" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN UON2 U_PET2 WITH (NOLOCK) ON U_PET2.UON1=PET.UON1 AND PET.UON2=U_PET2.COD" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN UON3 U_PET3 WITH (NOLOCK) ON U_PET3.UON1=PET.UON1 AND PET.UON2=U_PET3.UON2 AND PET.UON3=U_PET3.COD '" & vbCrLf
sConsulta = sConsulta & "IF @DAT_COPIACAMPO =1 " & vbCrLf
sConsulta = sConsulta & "   SET @SFROM=@SFROM + ' " & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN" & vbCrLf
sConsulta = sConsulta & "       (SELECT M.INSTANCIA, M.NUM_VERSION, C.VALOR_TEXT FROM (SELECT INSTANCIA,NUM_VERSION,MAX(C.ID) CAMPO FROM COPIA_CAMPO C WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN FORM_CAMPO CD WITH (NOLOCK) ON C.COPIA_CAMPO_DEF=CD.ID AND CD.TIPO_CAMPO_GS=24 WHERE C.VALOR_TEXT IS NOT NULL GROUP BY C.INSTANCIA, C.NUM_VERSION) M " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN COPIA_CAMPO C WITH (NOLOCK) ON M.CAMPO = C.ID) C1 ON I.ID=C1.INSTANCIA AND C.NUM_VERSION=C1.NUM_VERSION" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN" & vbCrLf
sConsulta = sConsulta & "       (SELECT M.INSTANCIA, M.NUM_VERSION, C.VALOR_TEXT FROM (SELECT INSTANCIA,NUM_VERSION,MAX(C.ID) CAMPO FROM COPIA_CAMPO C WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN FORM_CAMPO CD WITH (NOLOCK) ON C.COPIA_CAMPO_DEF=CD.ID AND CD.TIPO_CAMPO_GS=25 WHERE C.VALOR_TEXT IS NOT NULL GROUP BY C.INSTANCIA, C.NUM_VERSION) M " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN COPIA_CAMPO C WITH (NOLOCK) ON M.CAMPO = C.ID) C2 ON I.ID=C2.INSTANCIA AND C.NUM_VERSION=C2.NUM_VERSION" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       LEFT JOIN" & vbCrLf
sConsulta = sConsulta & "       (SELECT M.INSTANCIA, M.NUM_VERSION, C.VALOR_TEXT FROM (SELECT INSTANCIA,NUM_VERSION,MAX(C.ID) CAMPO FROM COPIA_CAMPO C WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN FORM_CAMPO CD WITH (NOLOCK) ON C.COPIA_CAMPO_DEF=CD.ID AND CD.TIPO_CAMPO_GS=26 WHERE C.VALOR_TEXT IS NOT NULL GROUP BY C.INSTANCIA, C.NUM_VERSION) M " & vbCrLf
sConsulta = sConsulta & "       INNER JOIN COPIA_CAMPO C WITH (NOLOCK) ON M.CAMPO = C.ID) C3 ON I.ID=C3.INSTANCIA AND C.NUM_VERSION=C3.NUM_VERSION'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SFROM + ' WHERE C.ID=@CERTIFICADO'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL  @SQL, N' @CERTIFICADO INT', @CERTIFICADO=@CERTIFICADO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


End Sub

Private Sub V_3_5_Tablas_004()
    Dim sConsulta As String
    
sConsulta = "IF NOT EXISTS (SELECT * FROM SYS.COLUMNS WHERE NAME=N'MARCA_CALCULO_ITEMPREC' AND Object_ID = Object_ID(N'ITEM_OFE')) " & vbCrLf
sConsulta = sConsulta & " ALTER TABLE ITEM_OFE ADD MARCA_CALCULO_ITEMPREC INT NOT NULL CONSTRAINT DF_ITEM_OFE_MARCA_CALCULO_ITEMPREC DEFAULT 0" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

'Viene de la version 19 de 3.4
sConsulta = "IF NOT EXISTS (SELECT * FROM SYS.COLUMNS WHERE NAME=N'TIPOVISOR_PORTAL' AND Object_ID = Object_ID(N'ESCENARIO')) " & vbCrLf
sConsulta = sConsulta & " ALTER TABLE dbo.ESCENARIO ADD TIPOVISOR_PORTAL tinyint NULL" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[VIEW_GET_CERTIFICADOS]') and OBJECTPROPERTY(id, N'IsView') = 1)" & vbCrLf
sConsulta = sConsulta & "drop view [dbo].[VIEW_GET_CERTIFICADOS]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE VIEW [dbo].[VIEW_GET_CERTIFICADOS]" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT" & vbCrLf
sConsulta = sConsulta & "S.ID AS ID_SOLIC, C.ID AS ID_CERTIF, C.INSTANCIA, C.NUM_VERSION, I.FEC_ALTA, C.FEC_DESPUB, C.FEC_SOLICITUD, C.FEC_RESPUESTA, C.FEC_EXPIRACION, C.FEC_LIM_CUMPLIM, C.FEC_PUBLICACION," & vbCrLf
sConsulta = sConsulta & "C.FECACT AS FEC_ACT, C.FEC_CUMPLIM, C.PDTE_ENVIAR, C.PER, C.VALIDADO, C.PUBLICADA, C.CERTIFICADO, PR.COD AS PROVE_COD, PR.DEN AS PROVE_DEN, ISNULL(PR.CALIDAD_POT, 0) AS REAL," & vbCrLf
sConsulta = sConsulta & "PR.CALIDAD_PROVISIONAL AS PROVISIONAL, ISNULL(PR.BAJA_CALIDAD, 0) AS BAJA, C.BAJA AS BAJA_CERTIFICADO, S.ID AS TIPO_CERTIFICADO, S.DEN_SPA, S.DEN_GER, S.DEN_ENG, S.DEN_FRA, ISNULL(S.PLAZO_CUMPL," & vbCrLf
sConsulta = sConsulta & "0) AS PLAZO_CUMPL, ~ S.OPCIONAL AS OBLIGATORIO, S.MODO_SOLIC, PT.NOM + ' ' + PT.APE AS NOMBREPETICIONARIO, ISNULL(I.EN_PROCESO, 0) AS EN_PROCESO, PR.COD + '#' + CAST(S.ID AS VARCHAR(10))" & vbCrLf
sConsulta = sConsulta & "AS DATAKEY, CASE WHEN (C.FEC_CUMPLIM IS NULL) THEN 64 WHEN (C.FEC_CUMPLIM IS NOT NULL AND C.VALIDADO IS NULL) THEN 80 WHEN (C.FEC_CUMPLIM IS NOT NULL AND C.VALIDADO = 0)" & vbCrLf
sConsulta = sConsulta & "THEN 82 WHEN (C.FEC_CUMPLIM IS NOT NULL AND C.FEC_EXPIRACION IS NOT NULL AND (DATEDIFF(day, getdate(), C.FEC_EXPIRACION) < 0) AND NOT (ISNULL(C.VALIDADO, 0) = 0))" & vbCrLf
sConsulta = sConsulta & "THEN 67 WHEN (C.FEC_CUMPLIM IS NOT NULL AND C.FEC_EXPIRACION IS NOT NULL AND (DATEDIFF(day, getdate(), C.FEC_EXPIRACION) BETWEEN 0 AND (FSQA_PROX_EXPIRAR + 1)) AND NOT (ISNULL(C.VALIDADO, 0) = 0))" & vbCrLf
sConsulta = sConsulta & "THEN 66 WHEN (C.FEC_CUMPLIM IS NOT NULL AND C.FEC_EXPIRACION IS NULL AND (C.VALIDADO = 1 OR" & vbCrLf
sConsulta = sConsulta & "C.VALIDADO = - 1)) THEN 65 WHEN (C.FEC_CUMPLIM IS NOT NULL AND C.FEC_EXPIRACION IS NOT NULL AND (DATEDIFF(day, getdate(), C.FEC_EXPIRACION) > (FSQA_PROX_EXPIRAR + 1)) AND (C.VALIDADO = 1 OR" & vbCrLf
sConsulta = sConsulta & "C.VALIDADO = - 1)) THEN 65 ELSE 67 END AS ID_TEXTO_ESTADO, 0 AS ESTADO_DEN, '' AS DEN_MATERIAL, '' AS ID_MATERIAL, CC.EMAIL AS CON_EMAIL, 0 AS NOPETIC, C.USU, S.BAJA AS BAJA_SOLICITUD," & vbCrLf
sConsulta = sConsulta & "PR.BAJA_CALIDAD, PR.CALIDAD_POT, PR.CALIDAD_PROVISIONAL, dbo.PARGEN_QA.FSQA_PROX_EXPIRAR, C.DEP, C.UON1, C.UON2, C.UON3, S.FORMULARIO, C.CONTACTO, PT.EMAIL" & vbCrLf
sConsulta = sConsulta & "FROM            dbo.CERTIFICADO AS C WITH (NOLOCK) INNER JOIN" & vbCrLf
sConsulta = sConsulta & "dbo.INSTANCIA AS I WITH (NOLOCK) ON C.INSTANCIA = I.ID INNER JOIN" & vbCrLf
sConsulta = sConsulta & "dbo.SOLICITUD AS S WITH (NOLOCK) ON I.SOLICITUD = S.ID AND S.PUB = 1 INNER JOIN" & vbCrLf
sConsulta = sConsulta & "dbo.PROVE_CERTIFICADO AS PC WITH (NOLOCK) ON S.ID = PC.TIPO_CERTIF AND C.ID = PC.ID_CERTIF INNER JOIN" & vbCrLf
sConsulta = sConsulta & "dbo.PROVE AS PR WITH (NOLOCK) ON C.PROVE = PR.COD AND COALESCE (S.TIPO_PROVE, PR.CALIDAD_POT) = PR.CALIDAD_POT INNER JOIN" & vbCrLf
sConsulta = sConsulta & "dbo.PER AS PT WITH (NOLOCK) ON C.PER = PT.COD LEFT OUTER JOIN" & vbCrLf
sConsulta = sConsulta & "dbo.CON AS CC WITH (NOLOCK) ON C.PROVE = CC.PROVE AND C.CONTACTO = CC.ID CROSS JOIN" & vbCrLf
sConsulta = sConsulta & "dbo.PARGEN_QA" & vbCrLf
sConsulta = sConsulta & "WHERE        (dbo.PARGEN_QA.ID = 1)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta


End Sub

Private Sub V_3_5_Storeds_004_1()
    Dim sConsulta As String


sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_SAVE_INSTANCIA_EN_FILTRO]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_SAVE_INSTANCIA_EN_FILTRO]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_SAVE_INSTANCIA_EN_FILTRO]" & vbCrLf
sConsulta = sConsulta & "   @INSTANCIA INT," & vbCrLf
sConsulta = sConsulta & "   @INSERT TINYINT," & vbCrLf
sConsulta = sConsulta & "   @QA TINYINT=0," & vbCrLf
sConsulta = sConsulta & "   @CONTRATO TINYINT=0" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   DECLARE @INSTANCIA_L INT" & vbCrLf
sConsulta = sConsulta & "   SET @INSTANCIA_L=@INSTANCIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET CONCAT_NULL_YIELDS_NULL OFF" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   --Obtenemos el nombre de las tablas din?micas del filtro (falta a?adir el prefijo del idioma)." & vbCrLf
sConsulta = sConsulta & "   DECLARE @TABLA NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "   IF @QA=0" & vbCrLf
sConsulta = sConsulta & "   BEGIN       " & vbCrLf
sConsulta = sConsulta & "       IF @CONTRATO=0" & vbCrLf
sConsulta = sConsulta & "           SELECT DISTINCT @TABLA=F.NOM_TABLA FROM INSTANCIA I WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN FORM_GRUPO G2 WITH (NOLOCK) ON I.FORMULARIO=G2.FORMULARIO" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PM_FILTROS F WITH (NOLOCK) ON G2.FORMULARIO=F.FORMULARIO" & vbCrLf
sConsulta = sConsulta & "           WHERE I.ID = @INSTANCIA_L" & vbCrLf
sConsulta = sConsulta & "       ELSE" & vbCrLf
sConsulta = sConsulta & "           SELECT DISTINCT @TABLA=F.NOM_TABLA FROM INSTANCIA I WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN FORM_GRUPO G2 WITH (NOLOCK) ON I.FORMULARIO=G2.FORMULARIO" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PM_CONTR_FILTROS F WITH (NOLOCK) ON G2.FORMULARIO=F.FORMULARIO" & vbCrLf
sConsulta = sConsulta & "           WHERE I.ID = @INSTANCIA_L" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "       SELECT DISTINCT @TABLA=F.NOM_TABLA FROM INSTANCIA I WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN FORM_GRUPO G2 WITH (NOLOCK) ON I.FORMULARIO=G2.FORMULARIO" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN QA_FILTROS F WITH (NOLOCK) ON G2.FORMULARIO=F.FORMULARIO" & vbCrLf
sConsulta = sConsulta & "       WHERE I.ID = @INSTANCIA_L" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   --Obtenemos las longitudes de los materiales de la tabla DIC" & vbCrLf
sConsulta = sConsulta & "   DECLARE @LEN_GMN1 INT" & vbCrLf
sConsulta = sConsulta & "   DECLARE @LEN_GMN2 INT" & vbCrLf
sConsulta = sConsulta & "   DECLARE @LEN_GMN3 INT" & vbCrLf
sConsulta = sConsulta & "   DECLARE @LEN_GMN4 INT" & vbCrLf
sConsulta = sConsulta & "   SELECT @LEN_GMN1=DIC.LONGITUD, @LEN_GMN2=DIC2.LONGITUD,@LEN_GMN3=DIC3.LONGITUD, @LEN_GMN4=DIC4.LONGITUD FROM DIC WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN DIC DIC2 WITH (NOLOCK) ON DIC2.NOMBRE='GMN2'" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN DIC DIC3 WITH (NOLOCK) ON DIC3.NOMBRE='GMN3'" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN DIC DIC4 WITH (NOLOCK) ON DIC4.NOMBRE='GMN4'" & vbCrLf
sConsulta = sConsulta & "   WHERE DIC.NOMBRE = 'GMN1'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   --Guardamos los IDs en una tabla temporal" & vbCrLf
sConsulta = sConsulta & "   --Es lo ?nico que cambia de los 3 casos (QA, Solicitudes y Contratos)" & vbCrLf
sConsulta = sConsulta & "   DECLARE @SQLINSERT NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "   CREATE TABLE #G1 (ID_CAMPO INT)" & vbCrLf
sConsulta = sConsulta & "   SET @SQLINSERT = N'INSERT INTO #G1 SELECT ID_CAMPO FROM '" & vbCrLf
sConsulta = sConsulta & "   IF @QA=0" & vbCrLf
sConsulta = sConsulta & "       IF @CONTRATO = 0" & vbCrLf
sConsulta = sConsulta & "           SET @SQLINSERT = @SQLINSERT + N'UPD_VISORES_PM WITH(NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "       ELSE" & vbCrLf
sConsulta = sConsulta & "           SET @SQLINSERT = @SQLINSERT + N'UPD_VISORES_CONTR WITH(NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "       SET @SQLINSERT = @SQLINSERT + N'UPD_VISORES_QA WITH(NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @SQLINSERT = @SQLINSERT + N'WHERE (ACCION=''I'' OR ACCION=''A'') AND (NOM_TABLA=@TABLA OR NOM_TABLA=@TABLA+''_DESGLOSE'') AND OK=0'" & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQLINSERT, N'@TABLA NVARCHAR(100)', @TABLA=@TABLA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   DECLARE @LINEA INT" & vbCrLf
sConsulta = sConsulta & "   DECLARE @LINEA_ANT INT" & vbCrLf
sConsulta = sConsulta & "   DECLARE @COPIACAMPO_ID INT" & vbCrLf
sConsulta = sConsulta & "   DECLARE @CAMPO_ORIGEN INT" & vbCrLf
sConsulta = sConsulta & "   DECLARE @SUBTIPO INT" & vbCrLf
sConsulta = sConsulta & "   DECLARE @TIPO_CAMPO_GS INT" & vbCrLf
sConsulta = sConsulta & "   DECLARE @VALOR_NUM FLOAT" & vbCrLf
sConsulta = sConsulta & "   DECLARE @VALOR_TEXT NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "   DECLARE @VALOR_FEC DATETIME" & vbCrLf
sConsulta = sConsulta & "   DECLARE @VALOR_BOOL TINYINT" & vbCrLf
sConsulta = sConsulta & "   DECLARE @VALOR_TEXT_DEN NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "   DECLARE @VALOR_TEXT_COD NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "   DECLARE @INTRO TINYINT" & vbCrLf
sConsulta = sConsulta & "   DECLARE @ID_ATRIB_GS INT" & vbCrLf
sConsulta = sConsulta & "   DECLARE @TABLA_EXTERNA INT" & vbCrLf
sConsulta = sConsulta & "   DECLARE @CAMPO_DEF INT" & vbCrLf
sConsulta = sConsulta & "   DECLARE @SQL NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "   DECLARE @SQL_COD NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "   DECLARE @SQLCAMPOSINSERT NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "   DECLARE @EJECUTARSQL TINYINT" & vbCrLf
sConsulta = sConsulta & "   DECLARE @CAMPO_PADRE_DESGLOSE INT" & vbCrLf
sConsulta = sConsulta & "   DECLARE @CAMPO_PADRE_DESGLOSE_ANT INT" & vbCrLf
sConsulta = sConsulta & "   DECLARE @DENOMINACION NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "   DECLARE @CAMPOTRATADO TINYINT" & vbCrLf
sConsulta = sConsulta & "   DECLARE @EMPIEZAIDIOMA INT" & vbCrLf
sConsulta = sConsulta & "   DECLARE @ACABADENOMINACION INT" & vbCrLf
sConsulta = sConsulta & "   DECLARE @EMPIEZADENOMINACION INT" & vbCrLf
sConsulta = sConsulta & "   DECLARE @SEPARADOR INT" & vbCrLf
sConsulta = sConsulta & "   DECLARE @PRESUPUESTOS NVARCHAR(800)" & vbCrLf
sConsulta = sConsulta & "   DECLARE @PORCENTAJE NVARCHAR(400)" & vbCrLf
sConsulta = sConsulta & "   DECLARE @SEPARADOR_DEN INT" & vbCrLf
sConsulta = sConsulta & "   DECLARE @SEPARADOR_PORCENTAJE INT" & vbCrLf
sConsulta = sConsulta & "   DECLARE @IDIOMA AS NVARCHAR(400)" & vbCrLf
sConsulta = sConsulta & "   DECLARE @CAMPO_NUM_COUNT SMALLINT" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   CREATE TABLE #T_VALORES_NUM (ID SMALLINT PRIMARY KEY,VALOR FLOAT)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   DECLARE curIDIOMAS CURSOR LOCAL FOR" & vbCrLf
sConsulta = sConsulta & "   SELECT COD FROM IDIOMAS WITH(NOLOCK) WHERE APLICACION=1" & vbCrLf
sConsulta = sConsulta & "   OPEN curIDIOMAS" & vbCrLf
sConsulta = sConsulta & "   FETCH NEXT FROM curIDIOMAS INTO @IDIOMA" & vbCrLf
sConsulta = sConsulta & "   WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=''" & vbCrLf
sConsulta = sConsulta & "       SET @SQLCAMPOSINSERT=''" & vbCrLf
sConsulta = sConsulta & "       SET @EJECUTARSQL=0" & vbCrLf
sConsulta = sConsulta & "       SET @LINEA_ANT=1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       IF @INSERT = 1 AND @CONTRATO = 1" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           SET @SQL = N'INSERT INTO ' + @IDIOMA + N'_CONTR_FILTRO1 VALUES (@INSTANCIA_L)'" & vbCrLf
sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA_L INT', @INSTANCIA_L=@INSTANCIA_L" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       IF @INSERT = 1" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           SET @SQLCAMPOSINSERT=N'INSERT INTO '+@IDIOMA+N'_'+@TABLA+N' (INSTANCIA,'" & vbCrLf
sConsulta = sConsulta & "           SET @SQL = N' VALUES (@INSTANCIA_L,'" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "       ELSE" & vbCrLf
sConsulta = sConsulta & "           SET @SQL=N' UPDATE ' + @IDIOMA + N'_' + @TABLA  + N' SET '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       DECLARE curSOLICITUDES_CAMPOS CURSOR LOCAL FOR" & vbCrLf
sConsulta = sConsulta & "       SELECT DISTINCT C.ID,FC.ID CAMPO_ORIGEN,FC.SUBTIPO,FC.TIPO_CAMPO_GS,C.VALOR_TEXT,C.VALOR_NUM," & vbCrLf
sConsulta = sConsulta & "           C.VALOR_FEC,C.VALOR_BOOL,C.VALOR_TEXT_COD,C.VALOR_TEXT_DEN,FC.INTRO,FC.ID_ATRIB_GS," & vbCrLf
sConsulta = sConsulta & "           FC.TABLA_EXTERNA,C.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "       FROM COPIA_CAMPO C WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN INSTANCIA I WITH(NOLOCK) ON I.ID=C.INSTANCIA AND C.NUM_VERSION=I.ULT_VERSION" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN FORM_CAMPO FC WITH (NOLOCK) ON FC.ID=C.COPIA_CAMPO_DEF AND FC.BAJALOG = 0" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN FORM_GRUPO FG WITH (NOLOCK) ON FG.ID = FC.GRUPO AND FG.BAJALOG = 0" & vbCrLf
sConsulta = sConsulta & "       WHERE FC.ES_SUBCAMPO=0 AND NOT FC.SUBTIPO=9 AND I.ID=@INSTANCIA_L" & vbCrLf
sConsulta = sConsulta & "           AND (NOT FC.TIPO_CAMPO_GS=106 OR FC.TIPO_CAMPO_GS IS NULL)" & vbCrLf
sConsulta = sConsulta & "           AND (FC.SUBTIPO=FC.SUBTIPO OR (FC.SUBTIPO=5 AND FC.SUBTIPO IN (6,7)) OR (FC.SUBTIPO=6 AND FC.SUBTIPO=7))  --si la definici?n del campo ha cambiado de texto corto a texto medio o largo, o de texto medio a texto largo" & vbCrLf
sConsulta = sConsulta & "           AND FC.ID NOT IN (SELECT ID_CAMPO FROM #G1)  -- campos que se han a?adido en el formulario pero no se ha hecho todavia ALTER en las tablas de filtros (BatchUpdates)" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "      SET @CAMPO_NUM_COUNT=0       " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       OPEN curSOLICITUDES_CAMPOS" & vbCrLf
sConsulta = sConsulta & "       FETCH NEXT FROM curSOLICITUDES_CAMPOS INTO @COPIACAMPO_ID,@CAMPO_ORIGEN, @SUBTIPO, @TIPO_CAMPO_GS, @VALOR_TEXT, @VALOR_NUM, @VALOR_FEC, @VALOR_BOOL, @VALOR_TEXT_COD, @VALOR_TEXT_DEN, @INTRO, @ID_ATRIB_GS,@TABLA_EXTERNA, @CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "       WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "       BEGIN          " & vbCrLf
sConsulta = sConsulta & "           SET @EJECUTARSQL=1" & vbCrLf
sConsulta = sConsulta & "           --Reemplazamos las comillas simples en valor_text y valor_text_den" & vbCrLf
sConsulta = sConsulta & "           IF @VALOR_TEXT IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "               SET @VALOR_TEXT = REPLACE(@VALOR_TEXT,'''','''''')" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "           IF @VALOR_TEXT_DEN IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "               SET @VALOR_TEXT_DEN = REPLACE(@VALOR_TEXT_DEN,'''','''''')" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "           IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "               IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "                   SET @SQLCAMPOSINSERT = @SQLCAMPOSINSERT + 'C_' + CONVERT(NVARCHAR,@CAMPO_ORIGEN)  + ','" & vbCrLf
sConsulta = sConsulta & "               ELSE" & vbCrLf
sConsulta = sConsulta & "                   SET @SQLCAMPOSINSERT = @SQLCAMPOSINSERT + N'C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N','" & vbCrLf
sConsulta = sConsulta & "           ELSE" & vbCrLf
sConsulta = sConsulta & "               IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "                   SET @SQL = @SQL + 'C_' + CONVERT(NVARCHAR,@CAMPO_ORIGEN) + '='" & vbCrLf
sConsulta = sConsulta & "               ELSE" & vbCrLf
sConsulta = sConsulta & "                   SET @SQL = @SQL +N'C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'='" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "           ----------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "           IF @SUBTIPO = 3 -- tipo fecha           " & vbCrLf
sConsulta = sConsulta & "               IF @VALOR_FEC IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @SQL = @SQL + N'CONVERT(DATETIME,''' + CONVERT(VARCHAR,@VALOR_FEC) + ''')'" & vbCrLf
sConsulta = sConsulta & "               ELSE" & vbCrLf
sConsulta = sConsulta & "                   SET @SQL = @SQL + N'NULL'" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "           ELSE" & vbCrLf
sConsulta = sConsulta & "           BEGIN" & vbCrLf
sConsulta = sConsulta & "               IF @SUBTIPO = 4 -- tipo booleano                " & vbCrLf
sConsulta = sConsulta & "                   IF @VALOR_BOOL IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                       SET @SQL = @SQL + CONVERT(NVARCHAR,@VALOR_BOOL)" & vbCrLf
sConsulta = sConsulta & "                   ELSE" & vbCrLf
sConsulta = sConsulta & "                       SET @SQL = @SQL + N'NULL'               " & vbCrLf
sConsulta = sConsulta & "               ELSE" & vbCrLf
sConsulta = sConsulta & "               BEGIN" & vbCrLf
sConsulta = sConsulta & "                   IF @SUBTIPO = 8 -- tipo archivo                 " & vbCrLf
sConsulta = sConsulta & "                       IF @VALOR_TEXT IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                           SET @SQL = @SQL + N'N''' + LEFT(@VALOR_TEXT, 800) + N''''" & vbCrLf
sConsulta = sConsulta & "                       ELSE" & vbCrLf
sConsulta = sConsulta & "                           SET @SQL = @SQL + N'NULL'                   " & vbCrLf
sConsulta = sConsulta & "                   ELSE" & vbCrLf
sConsulta = sConsulta & "                   BEGIN" & vbCrLf
sConsulta = sConsulta & "                       IF @TIPO_CAMPO_GS IS NULL" & vbCrLf
sConsulta = sConsulta & "                       BEGIN" & vbCrLf
sConsulta = sConsulta & "                           IF @TABLA_EXTERNA IS NULL" & vbCrLf
sConsulta = sConsulta & "                           BEGIN                               " & vbCrLf
sConsulta = sConsulta & "                               IF @SUBTIPO=2 -- tipo num?rico " & vbCrLf
sConsulta = sConsulta & "                              BEGIN                                                                                                                                        " & vbCrLf
sConsulta = sConsulta & "                                   SET @CAMPO_NUM_COUNT=@CAMPO_NUM_COUNT+1                                 " & vbCrLf
sConsulta = sConsulta & "                                   INSERT INTO #T_VALORES_NUM VALUES (@CAMPO_NUM_COUNT,@VALOR_NUM)                                                                         " & vbCrLf
sConsulta = sConsulta & "                                    SET @SQL = @SQL + '(SELECT VALOR FROM #T_VALORES_NUM WHERE ID=' + CAST(@CAMPO_NUM_COUNT AS NVARCHAR) + ')'                                                                                                                                 " & vbCrLf
sConsulta = sConsulta & "                               END" & vbCrLf
sConsulta = sConsulta & "                               ELSE    -- tipo texto o tipo lista texto" & vbCrLf
sConsulta = sConsulta & "                               BEGIN                              " & vbCrLf
sConsulta = sConsulta & "                                   IF @INSERT=1 AND @QA=0 AND @CONTRATO=0" & vbCrLf
sConsulta = sConsulta & "                                       SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                                   IF @INTRO=1 AND @VALOR_NUM IS NOT NULL --tipo lista texto" & vbCrLf
sConsulta = sConsulta & "                                   BEGIN" & vbCrLf
sConsulta = sConsulta & "                                       IF @ID_ATRIB_GS IS NULL" & vbCrLf
sConsulta = sConsulta & "                                       BEGIN" & vbCrLf
sConsulta = sConsulta & "                                           IF @IDIOMA='SPA'" & vbCrLf
sConsulta = sConsulta & "                                               SELECT @DENOMINACION=VALOR_TEXT_SPA" & vbCrLf
sConsulta = sConsulta & "                                               FROM CAMPO_VALOR_LISTA WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                                               WHERE CAMPO=CONVERT(VARCHAR,@CAMPO_DEF) AND ORDEN=CONVERT(VARCHAR,@VALOR_NUM)" & vbCrLf
sConsulta = sConsulta & "                                           IF @IDIOMA='ENG'" & vbCrLf
sConsulta = sConsulta & "                                               SELECT @DENOMINACION=VALOR_TEXT_ENG" & vbCrLf
sConsulta = sConsulta & "                                               FROM CAMPO_VALOR_LISTA WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                                               WHERE CAMPO=CONVERT(VARCHAR,@CAMPO_DEF) AND ORDEN=CONVERT(VARCHAR,@VALOR_NUM)" & vbCrLf
sConsulta = sConsulta & "                                           IF @IDIOMA='GER'" & vbCrLf
sConsulta = sConsulta & "                                               SELECT @DENOMINACION=VALOR_TEXT_GER" & vbCrLf
sConsulta = sConsulta & "                                               FROM CAMPO_VALOR_LISTA WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                                               WHERE CAMPO=CONVERT(VARCHAR,@CAMPO_DEF) AND ORDEN=CONVERT(VARCHAR,@VALOR_NUM)" & vbCrLf
sConsulta = sConsulta & "                                           IF @IDIOMA ='FRA'" & vbCrLf
sConsulta = sConsulta & "                                               SELECT @DENOMINACION=VALOR_TEXT_FRA" & vbCrLf
sConsulta = sConsulta & "                                               FROM CAMPO_VALOR_LISTA WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                                               WHERE CAMPO=CONVERT(VARCHAR,@CAMPO_DEF) AND ORDEN=CONVERT(VARCHAR,@VALOR_NUM)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                                           IF @DENOMINACION IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                                               IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "                                                   SET @SQL = @SQL + 'N''' + replace(@DENOMINACION,'''','''''') + ''''" & vbCrLf
sConsulta = sConsulta & "                                               ELSE                                                " & vbCrLf
sConsulta = sConsulta & "                                                   IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "                                                       SET @SQL=@SQL+N'N'''+REPLACE(@DENOMINACION,'''','''''')+''''+N',CONVERT(NVARCHAR,'+CONVERT(NVARCHAR,@VALOR_NUM)+N')'" & vbCrLf
sConsulta = sConsulta & "                                                   ELSE" & vbCrLf
sConsulta = sConsulta & "                                                       SET @SQL=@SQL+N'N'''+REPLACE(@DENOMINACION,'''','''''')+''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=CONVERT(NVARCHAR,'+CONVERT(NVARCHAR,@VALOR_NUM)+N')'" & vbCrLf
sConsulta = sConsulta & "                                           ELSE" & vbCrLf
sConsulta = sConsulta & "                                               IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "                                                   SET @SQL = @SQL + 'NULL'" & vbCrLf
sConsulta = sConsulta & "                                               ELSE" & vbCrLf
sConsulta = sConsulta & "                                                   IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "                                                       SET @SQL=@SQL + N'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "                                                   ELSE" & vbCrLf
sConsulta = sConsulta & "                                                       SET @SQL=@SQL + N'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "                                       END" & vbCrLf
sConsulta = sConsulta & "                                       ELSE" & vbCrLf
sConsulta = sConsulta & "                                           IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "                                               SET @SQL = @SQL + 'N''' + @VALOR_TEXT + ''''" & vbCrLf
sConsulta = sConsulta & "                                           ELSE" & vbCrLf
sConsulta = sConsulta & "                                               IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "                                                   SET @SQL=@SQL + N'N'''+@VALOR_TEXT+N''''+N','+N''''+CONVERT(NVARCHAR,@VALOR_NUM)+N''''" & vbCrLf
sConsulta = sConsulta & "                                               ELSE" & vbCrLf
sConsulta = sConsulta & "                                                   SET @SQL=@SQL + N'N'''+@VALOR_TEXT+N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+ CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N''''+CONVERT(NVARCHAR,@VALOR_NUM)+N''''                                      " & vbCrLf
sConsulta = sConsulta & "                                   END" & vbCrLf
sConsulta = sConsulta & "                                   ELSE -- tipo texto" & vbCrLf
sConsulta = sConsulta & "                                   BEGIN" & vbCrLf
sConsulta = sConsulta & "                                       IF @VALOR_TEXT IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                                       BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                                           IF @SUBTIPO=6 --TEXTO MEDIO: 800" & vbCrLf
sConsulta = sConsulta & "                                               SET @VALOR_TEXT = LEFT(@VALOR_TEXT, 800)" & vbCrLf
sConsulta = sConsulta & "                                           IF @SUBTIPO=5 --TEXTO CORTO: 100" & vbCrLf
sConsulta = sConsulta & "                                               SET @VALOR_TEXT = LEFT(@VALOR_TEXT, 100)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                                           IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "                                               SET @SQL = @SQL + 'N''' + @VALOR_TEXT + ''''" & vbCrLf
sConsulta = sConsulta & "                                           ELSE" & vbCrLf
sConsulta = sConsulta & "                                               IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "                                                   SET @SQL=@SQL + N'N'''+@VALOR_TEXT+N''''+N','+N'N'''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "                                               ELSE" & vbCrLf
sConsulta = sConsulta & "                                                   SET @SQL=@SQL + N'N'''+@VALOR_TEXT+N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+ CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N'N'''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "                                       END" & vbCrLf
sConsulta = sConsulta & "                                       ELSE" & vbCrLf
sConsulta = sConsulta & "                                           IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "                                               SET @SQL = @SQL + 'NULL'" & vbCrLf
sConsulta = sConsulta & "                                           ELSE" & vbCrLf
sConsulta = sConsulta & "                                               IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "                                                   SET @SQL=@SQL + N'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "                                               ELSE" & vbCrLf
sConsulta = sConsulta & "                                                   SET @SQL=@SQL+N'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "                                   END" & vbCrLf
sConsulta = sConsulta & "                               END" & vbCrLf
sConsulta = sConsulta & "                           END" & vbCrLf
sConsulta = sConsulta & "                           ELSE" & vbCrLf
sConsulta = sConsulta & "                           BEGIN           " & vbCrLf
sConsulta = sConsulta & "                               IF @SUBTIPO=2 -- tipo num?rico    " & vbCrLf
sConsulta = sConsulta & "                              BEGIN" & vbCrLf
sConsulta = sConsulta & "                                   SET @CAMPO_NUM_COUNT=@CAMPO_NUM_COUNT+1                                 " & vbCrLf
sConsulta = sConsulta & "                                   INSERT INTO #T_VALORES_NUM VALUES (@CAMPO_NUM_COUNT,@VALOR_NUM)                                                                         " & vbCrLf
sConsulta = sConsulta & "                                    SET @SQL = @SQL + '(SELECT VALOR FROM #T_VALORES_NUM WHERE ID=' + CAST(@CAMPO_NUM_COUNT AS NVARCHAR) + ')'                                                                                                          " & vbCrLf
sConsulta = sConsulta & "                               END" & vbCrLf
sConsulta = sConsulta & "                               ELSE    -- tipo texto o tipo lista texto" & vbCrLf
sConsulta = sConsulta & "                               BEGIN" & vbCrLf
sConsulta = sConsulta & "                                   IF @INSERT=1 AND @QA=0 AND @CONTRATO=0" & vbCrLf
sConsulta = sConsulta & "                                       SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                                   IF @VALOR_TEXT_DEN IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                                       IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "                                           SET @SQL = @SQL + 'N''' + @VALOR_TEXT_DEN + ''''" & vbCrLf
sConsulta = sConsulta & "                                       ELSE" & vbCrLf
sConsulta = sConsulta & "                                           IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "                                               SET @SQL=@SQL+N'N'''+@VALOR_TEXT_DEN+N''''+N','+N'N'''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "                                           ELSE" & vbCrLf
sConsulta = sConsulta & "                                               SET @SQL=@SQL+N'N'''+@VALOR_TEXT_DEN+N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N'N'''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "                                   ELSE" & vbCrLf
sConsulta = sConsulta & "                                       IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "                                           SET @SQL = @SQL + 'NULL'" & vbCrLf
sConsulta = sConsulta & "                                       ELSE" & vbCrLf
sConsulta = sConsulta & "                                           IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "                                               SET @SQL=@SQL+N'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "                               ELSE" & vbCrLf
sConsulta = sConsulta & "                                   SET @SQL=@SQL+N'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_' +CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "                               END" & vbCrLf
sConsulta = sConsulta & "                           END" & vbCrLf
sConsulta = sConsulta & "                       END" & vbCrLf
sConsulta = sConsulta & "                       ELSE -- campo del sistema" & vbCrLf
sConsulta = sConsulta & "                       BEGIN" & vbCrLf
sConsulta = sConsulta & "                           SET @CAMPOTRATADO = 0" & vbCrLf
sConsulta = sConsulta & "                           ---------------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "                           -- Formas de pago, Monedas, Familia de materiales, Unidades, Destinos, Pa?ses y Provincias" & vbCrLf
sConsulta = sConsulta & "                           -- FORMATO: <Idioma>|<Texto>#<Idioma>|<Texto>" & vbCrLf
sConsulta = sConsulta & "                           -- FORMATO MATERIALES: cod. ultimo nivel (VALOR_TEXT) +  <Idioma>|<Texto>#<Idioma>|<Texto>" & vbCrLf
sConsulta = sConsulta & "                           ------------------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "                           IF @TIPO_CAMPO_GS = 101 OR @TIPO_CAMPO_GS = 102 OR @TIPO_CAMPO_GS = 103 OR @TIPO_CAMPO_GS = 105 OR @TIPO_CAMPO_GS = 109 OR @TIPO_CAMPO_GS = 107 OR @TIPO_CAMPO_GS = 108" & vbCrLf
sConsulta = sConsulta & "                           BEGIN" & vbCrLf
sConsulta = sConsulta & "                               IF @INSERT=1 AND @QA=0 AND @CONTRATO=0" & vbCrLf
sConsulta = sConsulta & "                                   SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+ N'_COD,'" & vbCrLf
sConsulta = sConsulta & "                           " & vbCrLf
sConsulta = sConsulta & "                               SET @CAMPOTRATADO = 1" & vbCrLf
sConsulta = sConsulta & "                               -- Materiales" & vbCrLf
sConsulta = sConsulta & "                               IF @TIPO_CAMPO_GS = 103" & vbCrLf
sConsulta = sConsulta & "                                   IF @VALOR_TEXT IS NOT NULL                              " & vbCrLf
sConsulta = sConsulta & "                                       -- Al hacer Len de valor_text me est? quitando los espacios en blanco de la derecha" & vbCrLf
sConsulta = sConsulta & "                                       IF LEN(@VALOR_TEXT + '#')-1 = @LEN_GMN1 + @LEN_GMN2 + @LEN_GMN3 + @LEN_GMN4" & vbCrLf
sConsulta = sConsulta & "                                           SET @SQL = @SQL + N'N''' + RTRIM(RIGHT(@VALOR_TEXT,@LEN_GMN4)) + N' - '" & vbCrLf
sConsulta = sConsulta & "                                       ELSE                                    " & vbCrLf
sConsulta = sConsulta & "                                           IF LEN(@VALOR_TEXT + '#')-1 = @LEN_GMN1 + @LEN_GMN2 + @LEN_GMN3" & vbCrLf
sConsulta = sConsulta & "                                               SET @SQL = @SQL + N'N''' + RTRIM(RIGHT(@VALOR_TEXT,@LEN_GMN3)) + N' - '" & vbCrLf
sConsulta = sConsulta & "                                           ELSE                                        " & vbCrLf
sConsulta = sConsulta & "                                               IF LEN(@VALOR_TEXT + '#')-1 = @LEN_GMN1 + @LEN_GMN2" & vbCrLf
sConsulta = sConsulta & "                                                   SET @SQL = @SQL + N'N''' + RTRIM(RIGHT(@VALOR_TEXT,@LEN_GMN2)) + N' - '" & vbCrLf
sConsulta = sConsulta & "                                               ELSE" & vbCrLf
sConsulta = sConsulta & "                                                   SET @SQL = @SQL + N'N''' + RTRIM(RIGHT(@VALOR_TEXT,@LEN_GMN1)) + N' - ' " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                               -- Todos" & vbCrLf
sConsulta = sConsulta & "                               IF RIGHT(@VALOR_TEXT_DEN,1)='#' SET @VALOR_TEXT_DEN=LEFT(@VALOR_TEXT_DEN,LEN(@VALOR_TEXT_DEN)-1)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                               SET @EMPIEZAIDIOMA=CHARINDEX(@IDIOMA + '|',@VALOR_TEXT_DEN)" & vbCrLf
sConsulta = sConsulta & "                               IF (@EMPIEZAIDIOMA) > 0" & vbCrLf
sConsulta = sConsulta & "                               BEGIN" & vbCrLf
sConsulta = sConsulta & "                                   SET @ACABADENOMINACION = CHARINDEX('#',@VALOR_TEXT_DEN,@EMPIEZAIDIOMA)" & vbCrLf
sConsulta = sConsulta & "                                   SET @EMPIEZADENOMINACION = CHARINDEX('|',@VALOR_TEXT_DEN,@EMPIEZAIDIOMA)+1" & vbCrLf
sConsulta = sConsulta & "                               " & vbCrLf
sConsulta = sConsulta & "                                   IF @TIPO_CAMPO_GS <> 103" & vbCrLf
sConsulta = sConsulta & "                                       SET @SQL=@SQL+'N'''" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                                   IF @ACABADENOMINACION>0" & vbCrLf
sConsulta = sConsulta & "                                       SET @SQL=@SQL+SUBSTRING(@VALOR_TEXT_DEN,@EMPIEZADENOMINACION,@ACABADENOMINACION-@EMPIEZADENOMINACION) + ''''" & vbCrLf
sConsulta = sConsulta & "                                   ELSE" & vbCrLf
sConsulta = sConsulta & "                                       SET @SQL=@SQL+RIGHT(@VALOR_TEXT_DEN,LEN(@VALOR_TEXT_DEN)-@EMPIEZADENOMINACION+1) + ''''" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                                   IF @QA=0 AND @CONTRATO=0                                " & vbCrLf
sConsulta = sConsulta & "                                       IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "                                           SET @SQL=@SQL+N','+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "                                       ELSE" & vbCrLf
sConsulta = sConsulta & "                                           SET @SQL=@SQL+N',C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N''''+@VALOR_TEXT+N''''                               " & vbCrLf
sConsulta = sConsulta & "                               END" & vbCrLf
sConsulta = sConsulta & "                               ELSE" & vbCrLf
sConsulta = sConsulta & "                               BEGIN" & vbCrLf
sConsulta = sConsulta & "                                   IF @VALOR_TEXT IS NOT NULL AND @TIPO_CAMPO_GS = 103" & vbCrLf
sConsulta = sConsulta & "                                       IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "                                           SET @SQL = @SQL + ''''" & vbCrLf
sConsulta = sConsulta & "                                       ELSE" & vbCrLf
sConsulta = sConsulta & "                                           IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "                                               SET @SQL=@SQL+N''''+N','+N'N'''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "                                           ELSE" & vbCrLf
sConsulta = sConsulta & "                                               SET @SQL=@SQL+N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N'N'''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "                                   ELSE" & vbCrLf
sConsulta = sConsulta & "                                       IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "                                           SET @SQL = @SQL + 'NULL'" & vbCrLf
sConsulta = sConsulta & "                                       ELSE" & vbCrLf
sConsulta = sConsulta & "                                           IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "                                               SET @SQL=@SQL+N'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "                                           ELSE" & vbCrLf
sConsulta = sConsulta & "                                               SET @SQL=@SQL+N'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "                               END" & vbCrLf
sConsulta = sConsulta & "                           END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                           --------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "                           -- Proveedores, Departamentos, Organizaciones de Compras, Centros,Proveedor ERP" & vbCrLf
sConsulta = sConsulta & "                           -- FORMATO: C?digo (VALOR_TEXT) - Denominaci?n (VALOR_TEXT_DEN)" & vbCrLf
sConsulta = sConsulta & "                           --------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "                           IF @TIPO_CAMPO_GS = 11 OR @TIPO_CAMPO_GS = 100  OR @TIPO_CAMPO_GS = 122 OR @TIPO_CAMPO_GS = 123 OR @TIPO_CAMPO_GS = 124 OR @TIPO_CAMPO_GS = 143" & vbCrLf
sConsulta = sConsulta & "                           BEGIN" & vbCrLf
sConsulta = sConsulta & "                               SET @CAMPOTRATADO = 1" & vbCrLf
sConsulta = sConsulta & "                               IF @INSERT=1 AND @QA=0 AND @CONTRATO=0" & vbCrLf
sConsulta = sConsulta & "                                   SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                               IF @VALOR_TEXT IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                                   IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "                                       SET @SQL = @SQL + 'N''' + @VALOR_TEXT + ' - ' + @VALOR_TEXT_DEN + ''''" & vbCrLf
sConsulta = sConsulta & "                                   ELSE" & vbCrLf
sConsulta = sConsulta & "                                       IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "                                           SET @SQL=@SQL+N'N'''+@VALOR_TEXT+N' - '+@VALOR_TEXT_DEN+N''''+N','+N'N'''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "                                       ELSE" & vbCrLf
sConsulta = sConsulta & "                                           SET @SQL=@SQL+N'N'''+@VALOR_TEXT+N' - '+@VALOR_TEXT_DEN+N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N'N'''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "                               ELSE" & vbCrLf
sConsulta = sConsulta & "                                   IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "                                       SET @SQL = @SQL + 'NULL'" & vbCrLf
sConsulta = sConsulta & "                                   ELSE" & vbCrLf
sConsulta = sConsulta & "                                       IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "                                           SET @SQL=@SQL+N'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "                                       ELSE" & vbCrLf
sConsulta = sConsulta & "                                           SET @SQL=@SQL+N'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "                           END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                           --------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "                           -- Centros de coste, Partidas presupuestarias, Activos" & vbCrLf
sConsulta = sConsulta & "                           -- FORMATO: cod. ultimo nivel (VALOR_TEXT) +  <Idioma>|<Texto>#<Idioma>|<Texto>" & vbCrLf
sConsulta = sConsulta & "                           --------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "                           IF @TIPO_CAMPO_GS = 129 OR @TIPO_CAMPO_GS = 130 OR @TIPO_CAMPO_GS = 131 OR @TIPO_CAMPO_GS=132" & vbCrLf
sConsulta = sConsulta & "                           BEGIN                               " & vbCrLf
sConsulta = sConsulta & "                               SET @CAMPOTRATADO=1" & vbCrLf
sConsulta = sConsulta & "                               IF @INSERT=1 AND @QA=0 AND @CONTRATO=0" & vbCrLf
sConsulta = sConsulta & "                                   SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                               IF @VALOR_TEXT IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                               BEGIN" & vbCrLf
sConsulta = sConsulta & "                                   IF RIGHT(@VALOR_TEXT_DEN,1)='#' SET @VALOR_TEXT_DEN=LEFT(@VALOR_TEXT_DEN,LEN(@VALOR_TEXT_DEN)-1)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                                   SET @SEPARADOR = CHARINDEX('#',@VALOR_TEXT,1)" & vbCrLf
sConsulta = sConsulta & "                                   WHILE @SEPARADOR > 0" & vbCrLf
sConsulta = sConsulta & "                                   BEGIN" & vbCrLf
sConsulta = sConsulta & "                                       SET @VALOR_TEXT = RIGHT(@VALOR_TEXT,LEN(@VALOR_TEXT)-@SEPARADOR)" & vbCrLf
sConsulta = sConsulta & "                                       SET @SEPARADOR = CHARINDEX('#',@VALOR_TEXT,1)" & vbCrLf
sConsulta = sConsulta & "                                   END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                                   SET @SEPARADOR = CHARINDEX('|',@VALOR_TEXT,1)" & vbCrLf
sConsulta = sConsulta & "                                   WHILE @SEPARADOR > 0" & vbCrLf
sConsulta = sConsulta & "                                   BEGIN" & vbCrLf
sConsulta = sConsulta & "                                       SET @VALOR_TEXT = RIGHT(@VALOR_TEXT,LEN(@VALOR_TEXT)-@SEPARADOR)" & vbCrLf
sConsulta = sConsulta & "                                       SET @SEPARADOR = CHARINDEX('|',@VALOR_TEXT,1)" & vbCrLf
sConsulta = sConsulta & "                                   END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                                   SET @SQL = @SQL + 'N''' + @VALOR_TEXT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                                   --Denominaci?n en el idioma" & vbCrLf
sConsulta = sConsulta & "                                   SET @EMPIEZAIDIOMA=CHARINDEX(@IDIOMA + '|',@VALOR_TEXT_DEN)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                                   IF (@EMPIEZAIDIOMA) > 0" & vbCrLf
sConsulta = sConsulta & "                                   BEGIN" & vbCrLf
sConsulta = sConsulta & "                                       SET @ACABADENOMINACION = CHARINDEX('#',@VALOR_TEXT_DEN,@EMPIEZAIDIOMA)" & vbCrLf
sConsulta = sConsulta & "                                       SET @EMPIEZADENOMINACION = CHARINDEX('|',@VALOR_TEXT_DEN,@EMPIEZAIDIOMA)+1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                                       IF @ACABADENOMINACION>0" & vbCrLf
sConsulta = sConsulta & "                                           SET @SQL = @SQL + ' - ' + SUBSTRING(@VALOR_TEXT_DEN,@EMPIEZADENOMINACION,@ACABADENOMINACION-@EMPIEZADENOMINACION)" & vbCrLf
sConsulta = sConsulta & "                                       ELSE" & vbCrLf
sConsulta = sConsulta & "                                           SET @SQL = @SQL + ' - ' + RIGHT(@VALOR_TEXT_DEN,LEN(@VALOR_TEXT_DEN)-@EMPIEZADENOMINACION+1)" & vbCrLf
sConsulta = sConsulta & "                                   END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                                   IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "                                       SET @SQL = @SQL + ''''" & vbCrLf
sConsulta = sConsulta & "                                   ELSE" & vbCrLf
sConsulta = sConsulta & "                                       IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "                                           SET @SQL=@SQL+N''''+N','+N'N'''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "                                       ELSE" & vbCrLf
sConsulta = sConsulta & "                                           SET @SQL=@SQL+N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N'N'''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "                               END" & vbCrLf
sConsulta = sConsulta & "                               ELSE" & vbCrLf
sConsulta = sConsulta & "                                   IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "                                       SET @SQL = @SQL + 'NULL'" & vbCrLf
sConsulta = sConsulta & "                                   ELSE" & vbCrLf
sConsulta = sConsulta & "                                       IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "                                           SET @SQL=@SQL+N'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "                                       ELSE" & vbCrLf
sConsulta = sConsulta & "                                           SET @SQL=@SQL+N'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "                           END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                           --------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "                           -- Personas, Comprador" & vbCrLf
sConsulta = sConsulta & "                           ---------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "                           IF @TIPO_CAMPO_GS = 115 OR @TIPO_CAMPO_GS = 144" & vbCrLf
sConsulta = sConsulta & "                           BEGIN" & vbCrLf
sConsulta = sConsulta & "                               SET @CAMPOTRATADO = 1" & vbCrLf
sConsulta = sConsulta & "                               IF @INSERT=1 AND @QA=0 AND @CONTRATO=0" & vbCrLf
sConsulta = sConsulta & "                                   SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                               IF @VALOR_TEXT_DEN IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                                   IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "                                       SET @SQL = @SQL + 'N''' + @VALOR_TEXT_DEN + ''''" & vbCrLf
sConsulta = sConsulta & "                                   ELSE" & vbCrLf
sConsulta = sConsulta & "                                       IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "                                           SET @SQL=@SQL+N'N'''+@VALOR_TEXT_DEN+N''''+N','+N'N'''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "                                       ELSE" & vbCrLf
sConsulta = sConsulta & "                                           SET @SQL=@SQL+N'N'''+@VALOR_TEXT_DEN+N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N'N'''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "                               ELSE" & vbCrLf
sConsulta = sConsulta & "                                   IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "                                       SET @SQL = @SQL + 'NULL'" & vbCrLf
sConsulta = sConsulta & "                                   ELSE" & vbCrLf
sConsulta = sConsulta & "                                       IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "                                           SET @SQL = @SQL + 'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "                                       ELSE" & vbCrLf
sConsulta = sConsulta & "                                           SET @SQL = @SQL + 'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "                           END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                           --------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "                           -- Almacen / Empresa" & vbCrLf
sConsulta = sConsulta & "                           ---------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "                           IF @TIPO_CAMPO_GS = 125 OR @TIPO_CAMPO_GS = 149" & vbCrLf
sConsulta = sConsulta & "                           BEGIN" & vbCrLf
sConsulta = sConsulta & "                               SET @CAMPOTRATADO = 1" & vbCrLf
sConsulta = sConsulta & "                               IF @INSERT=1 AND @QA=0 AND @CONTRATO=0" & vbCrLf
sConsulta = sConsulta & "                                   SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "                               " & vbCrLf
sConsulta = sConsulta & "                               IF @VALOR_TEXT_DEN IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                                   IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "                                       SET @SQL = @SQL + 'N''' + @VALOR_TEXT_DEN + ''''" & vbCrLf
sConsulta = sConsulta & "                                   ELSE" & vbCrLf
sConsulta = sConsulta & "                                       IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "                                           SET @SQL=@SQL+N'N'''+@VALOR_TEXT_DEN+N''''+N','+N''''+CONVERT(NVARCHAR,@VALOR_NUM)+N''''" & vbCrLf
sConsulta = sConsulta & "                                       ELSE" & vbCrLf
sConsulta = sConsulta & "                                           SET @SQL=@SQL+N'N'''+@VALOR_TEXT_DEN+N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N''''+CONVERT(NVARCHAR,@VALOR_NUM)+N''''" & vbCrLf
sConsulta = sConsulta & "                               ELSE" & vbCrLf
sConsulta = sConsulta & "                                   IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "                                       SET @SQL = @SQL + 'NULL'" & vbCrLf
sConsulta = sConsulta & "                                   ELSE" & vbCrLf
sConsulta = sConsulta & "                                       IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "                                           SET @SQL = @SQL + 'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "                                       ELSE" & vbCrLf
sConsulta = sConsulta & "                                           SET @SQL = @SQL + 'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "                           END                                             " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                           ------------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "                           -- Cod. Art?culo (viejo), Cod. Art?culo (nuevo), Denominaci?n del art?culo y Unidades Organizativas" & vbCrLf
sConsulta = sConsulta & "                           ------------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "                           IF @TIPO_CAMPO_GS = 104  OR @TIPO_CAMPO_GS = 118 OR @TIPO_CAMPO_GS = 119 OR @TIPO_CAMPO_GS = 121" & vbCrLf
sConsulta = sConsulta & "                           BEGIN" & vbCrLf
sConsulta = sConsulta & "                               SET @CAMPOTRATADO = 1" & vbCrLf
sConsulta = sConsulta & "                               IF @INSERT=1 AND @QA=0 AND @CONTRATO=0" & vbCrLf
sConsulta = sConsulta & "                                   SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "                               " & vbCrLf
sConsulta = sConsulta & "                               IF @VALOR_TEXT IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                                   IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "                                       SET @SQL = @SQL + 'N''' + @VALOR_TEXT + ''''" & vbCrLf
sConsulta = sConsulta & "                                   ELSE" & vbCrLf
sConsulta = sConsulta & "                                       IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "                                           SET @SQL=@SQL+N'N'''+@VALOR_TEXT+N''''+N','+N'N'''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "                                       ELSE" & vbCrLf
sConsulta = sConsulta & "                                           SET @SQL=@SQL+N'N'''+@VALOR_TEXT +N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N'N'''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "                               ELSE" & vbCrLf
sConsulta = sConsulta & "                                   IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "                                       SET @SQL = @SQL + 'NULL'" & vbCrLf
sConsulta = sConsulta & "                                   ELSE" & vbCrLf
sConsulta = sConsulta & "                                       IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "                                           SET @SQL=@SQL+N'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "                                       ELSE" & vbCrLf
sConsulta = sConsulta & "                                           SET @SQL=@SQL+N'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "                           END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                           ---------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "                           -- Presupuestos" & vbCrLf
sConsulta = sConsulta & "                           -- FORMATO: Ej 2005 - PC-002  (50.00%); 2005 - PC-001 (50.00%);" & vbCrLf
sConsulta = sConsulta & "                           --  VALOR_TEXT_DEN: 2005 - PC-002#2005 - PC-001" & vbCrLf
sConsulta = sConsulta & "                           --  VALOR_TEXT: 1_11_0.5#1_10_0.5" & vbCrLf
sConsulta = sConsulta & "                           ---------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "                           IF @TIPO_CAMPO_GS = 110  OR @TIPO_CAMPO_GS = 111 OR @TIPO_CAMPO_GS = 112 OR @TIPO_CAMPO_GS = 113" & vbCrLf
sConsulta = sConsulta & "                           BEGIN" & vbCrLf
sConsulta = sConsulta & "                               SET @CAMPOTRATADO = 1" & vbCrLf
sConsulta = sConsulta & "                               IF @INSERT=1 AND @QA=0 AND @CONTRATO=0" & vbCrLf
sConsulta = sConsulta & "                                   SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "                               " & vbCrLf
sConsulta = sConsulta & "                               SET @PRESUPUESTOS = ''" & vbCrLf
sConsulta = sConsulta & "                               SET @SEPARADOR_DEN = CHARINDEX('#',@VALOR_TEXT_DEN)" & vbCrLf
sConsulta = sConsulta & "                               WHILE @SEPARADOR_DEN > 0" & vbCrLf
sConsulta = sConsulta & "                               BEGIN" & vbCrLf
sConsulta = sConsulta & "                                   SET @PRESUPUESTOS = @PRESUPUESTOS + SUBSTRING(@VALOR_TEXT_DEN,1,@SEPARADOR_DEN-1)" & vbCrLf
sConsulta = sConsulta & "                                   SET @VALOR_TEXT_DEN = RIGHT(@VALOR_TEXT_DEN,LEN(@VALOR_TEXT_DEN)-@SEPARADOR_DEN)" & vbCrLf
sConsulta = sConsulta & "                                   SET @PORCENTAJE = SUBSTRING(@VALOR_TEXT,1,CHARINDEX('#',@VALOR_TEXT)-1)" & vbCrLf
sConsulta = sConsulta & "                                   SET @SEPARADOR_PORCENTAJE = 1" & vbCrLf
sConsulta = sConsulta & "                                   WHILE @SEPARADOR_PORCENTAJE > 0" & vbCrLf
sConsulta = sConsulta & "                                   BEGIN" & vbCrLf
sConsulta = sConsulta & "                                       SET @PORCENTAJE = RIGHT(@PORCENTAJE,LEN(@PORCENTAJE)-@SEPARADOR_PORCENTAJE)" & vbCrLf
sConsulta = sConsulta & "                                       SET @SEPARADOR_PORCENTAJE = CHARINDEX('_',@PORCENTAJE,@SEPARADOR_PORCENTAJE+1)" & vbCrLf
sConsulta = sConsulta & "                                   END" & vbCrLf
sConsulta = sConsulta & "                                   SET @PRESUPUESTOS = @PRESUPUESTOS + '(' + CONVERT(VARCHAR,CONVERT(DECIMAL(3,2),@PORCENTAJE)*100) + '%); '" & vbCrLf
sConsulta = sConsulta & "                                   SET @VALOR_TEXT = RIGHT(@VALOR_TEXT,LEN(@VALOR_TEXT)-CHARINDEX('#',@VALOR_TEXT))" & vbCrLf
sConsulta = sConsulta & "                                   SET @SEPARADOR_DEN = CHARINDEX('#',@VALOR_TEXT_DEN)" & vbCrLf
sConsulta = sConsulta & "                               END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                               SET @PORCENTAJE = @VALOR_TEXT" & vbCrLf
sConsulta = sConsulta & "                               SET @SEPARADOR_PORCENTAJE = 1" & vbCrLf
sConsulta = sConsulta & "                               WHILE @SEPARADOR_PORCENTAJE > 0" & vbCrLf
sConsulta = sConsulta & "                               BEGIN" & vbCrLf
sConsulta = sConsulta & "                                   SET @PORCENTAJE = RIGHT(@PORCENTAJE,LEN(@PORCENTAJE)-@SEPARADOR_PORCENTAJE)" & vbCrLf
sConsulta = sConsulta & "                                   SET @SEPARADOR_PORCENTAJE = CHARINDEX('_',@PORCENTAJE,@SEPARADOR_PORCENTAJE+1)" & vbCrLf
sConsulta = sConsulta & "                               END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                               SET @PRESUPUESTOS = @PRESUPUESTOS + @VALOR_TEXT_DEN + '(' + CAST(CONVERT(FLOAT,@PORCENTAJE)*100 AS VARCHAR) + '%);'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                               IF @PRESUPUESTOS <> ''" & vbCrLf
sConsulta = sConsulta & "                                   IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "                                       SET @SQL = @SQL + 'N''' + @PRESUPUESTOS + ''''" & vbCrLf
sConsulta = sConsulta & "                                   ELSE" & vbCrLf
sConsulta = sConsulta & "                                       IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "                                           SET @SQL = @SQL + N'N''' + @PRESUPUESTOS + N'''' + N',' + N'N''' + @VALOR_TEXT + N''''" & vbCrLf
sConsulta = sConsulta & "                                       ELSE" & vbCrLf
sConsulta = sConsulta & "                                           SET @SQL = @SQL + N'N''' + @PRESUPUESTOS + N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N'N'''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "                               ELSE" & vbCrLf
sConsulta = sConsulta & "                                   IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "                                       SET @SQL = @SQL + 'NULL'" & vbCrLf
sConsulta = sConsulta & "                                   ELSE" & vbCrLf
sConsulta = sConsulta & "                                       IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "                                           SET @SQL = @SQL + 'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "                                       ELSE" & vbCrLf
sConsulta = sConsulta & "                                           SET @SQL = @SQL + 'NULL,'+N'C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "                           END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                          ---------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "                           -- Periodo renovaci�n y estado homologaci�n                           " & vbCrLf
sConsulta = sConsulta & "                           ---------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "                          IF @TIPO_CAMPO_GS=148  OR @TIPO_CAMPO_GS=150" & vbCrLf
sConsulta = sConsulta & "                          BEGIN" & vbCrLf
sConsulta = sConsulta & "                               IF @VALOR_NUM IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                               BEGIN" & vbCrLf
sConsulta = sConsulta & "                                   SET @CAMPOTRATADO = 1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                                   IF @INSERT=1 AND @QA=0 AND @CONTRATO=0" & vbCrLf
sConsulta = sConsulta & "                                          SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+ N'_COD,'                                " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                                   IF @IDIOMA='SPA'" & vbCrLf
sConsulta = sConsulta & "                                       SELECT @DENOMINACION=VALOR_TEXT_SPA" & vbCrLf
sConsulta = sConsulta & "                                       FROM CAMPO_VALOR_LISTA WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                                       WHERE CAMPO=CONVERT(VARCHAR,@CAMPO_DEF) AND ORDEN=CONVERT(VARCHAR,@VALOR_NUM)" & vbCrLf
sConsulta = sConsulta & "                                   IF @IDIOMA='ENG'" & vbCrLf
sConsulta = sConsulta & "                                       SELECT @DENOMINACION=VALOR_TEXT_ENG" & vbCrLf
sConsulta = sConsulta & "                                       FROM CAMPO_VALOR_LISTA WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                                       WHERE CAMPO=CONVERT(VARCHAR,@CAMPO_DEF) AND ORDEN=CONVERT(VARCHAR,@VALOR_NUM)" & vbCrLf
sConsulta = sConsulta & "                                   IF @IDIOMA='GER'" & vbCrLf
sConsulta = sConsulta & "                                       SELECT @DENOMINACION=VALOR_TEXT_GER" & vbCrLf
sConsulta = sConsulta & "                                       FROM CAMPO_VALOR_LISTA WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                                       WHERE CAMPO=CONVERT(VARCHAR,@CAMPO_DEF) AND ORDEN=CONVERT(VARCHAR,@VALOR_NUM)" & vbCrLf
sConsulta = sConsulta & "                                   IF @IDIOMA ='FRA'" & vbCrLf
sConsulta = sConsulta & "                                       SELECT @DENOMINACION=VALOR_TEXT_FRA" & vbCrLf
sConsulta = sConsulta & "                                       FROM CAMPO_VALOR_LISTA WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                                       WHERE CAMPO=CONVERT(VARCHAR,@CAMPO_DEF) AND ORDEN=CONVERT(VARCHAR,@VALOR_NUM)                               " & vbCrLf
sConsulta = sConsulta & "                                   IF @DENOMINACION IS NOT NULL    " & vbCrLf
sConsulta = sConsulta & "                                       IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "                                           SET @SQL=@SQL+N'N'''+REPLACE(@DENOMINACION,'''','''''')+''''" & vbCrLf
sConsulta = sConsulta & "                                       ELSE                                                                                                                                        " & vbCrLf
sConsulta = sConsulta & "                                           IF @INSERT=1                                                                                " & vbCrLf
sConsulta = sConsulta & "                                               SET @SQL=@SQL+N'N'''+REPLACE(@DENOMINACION,'''','''''')+''''+N',CONVERT(NVARCHAR,'+CONVERT(NVARCHAR,@VALOR_NUM)+N')'" & vbCrLf
sConsulta = sConsulta & "                                           ELSE                                    " & vbCrLf
sConsulta = sConsulta & "                                               SET @SQL=@SQL+N'N'''+REPLACE(@DENOMINACION,'''','''''')+''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=CONVERT(NVARCHAR,'+CONVERT(NVARCHAR,@VALOR_NUM)+N')'                                                             " & vbCrLf
sConsulta = sConsulta & "                                   ELSE" & vbCrLf
sConsulta = sConsulta & "                                       IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "                                           SET @SQL=@SQL + N'NULL'" & vbCrLf
sConsulta = sConsulta & "                                       ELSE" & vbCrLf
sConsulta = sConsulta & "                                           IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "                                               SET @SQL=@SQL + N'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "                                           ELSE" & vbCrLf
sConsulta = sConsulta & "                                               SET @SQL=@SQL + N'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "                               END" & vbCrLf
sConsulta = sConsulta & "                          END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                           IF @CAMPOTRATADO = 0 -- Resto" & vbCrLf
sConsulta = sConsulta & "                           BEGIN" & vbCrLf
sConsulta = sConsulta & "                               IF @SUBTIPO = 3 -- tipo fecha                               " & vbCrLf
sConsulta = sConsulta & "                                   IF @VALOR_FEC IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                                       SET @SQL = @SQL + '''' + @VALOR_FEC + ''''" & vbCrLf
sConsulta = sConsulta & "                                   ELSE" & vbCrLf
sConsulta = sConsulta & "                                       SET @SQL = @SQL + 'NULL'                                " & vbCrLf
sConsulta = sConsulta & "                               ELSE" & vbCrLf
sConsulta = sConsulta & "                               BEGIN" & vbCrLf
sConsulta = sConsulta & "                                   IF @SUBTIPO = 4 -- tipo booleano                                    " & vbCrLf
sConsulta = sConsulta & "                                       IF @VALOR_BOOL IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                                           SET @SQL = @SQL + CONVERT(VARCHAR,@VALOR_BOOL)" & vbCrLf
sConsulta = sConsulta & "                                       ELSE" & vbCrLf
sConsulta = sConsulta & "                                           SET @SQL = @SQL + 'NULL'                                    " & vbCrLf
sConsulta = sConsulta & "                                   ELSE" & vbCrLf
sConsulta = sConsulta & "                                   BEGIN" & vbCrLf
sConsulta = sConsulta & "                                       IF @SUBTIPO=2 -- tipo num?rico " & vbCrLf
sConsulta = sConsulta & "                                      BEGIN" & vbCrLf
sConsulta = sConsulta & "                                          SET @CAMPO_NUM_COUNT=@CAMPO_NUM_COUNT+1                                  " & vbCrLf
sConsulta = sConsulta & "                                           INSERT INTO #T_VALORES_NUM VALUES (@CAMPO_NUM_COUNT,@VALOR_NUM)                                                                         " & vbCrLf
sConsulta = sConsulta & "                                           SET @SQL = @SQL + '(SELECT VALOR FROM #T_VALORES_NUM WHERE ID=' + CAST(@CAMPO_NUM_COUNT AS NVARCHAR) + ')'                                                                                                                " & vbCrLf
sConsulta = sConsulta & "                                       END" & vbCrLf
sConsulta = sConsulta & "                                       ELSE    -- tipo texto" & vbCrLf
sConsulta = sConsulta & "                                       BEGIN" & vbCrLf
sConsulta = sConsulta & "                                           IF @INSERT=1 AND @QA=0 AND @CONTRATO=0" & vbCrLf
sConsulta = sConsulta & "                                               SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "                                           " & vbCrLf
sConsulta = sConsulta & "                                           IF @VALOR_TEXT IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                                               IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "                                                   SET @SQL = @SQL + 'N''' + @VALOR_TEXT + ''''" & vbCrLf
sConsulta = sConsulta & "                                               ELSE" & vbCrLf
sConsulta = sConsulta & "                                                   IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "                                                       SET @SQL=@SQL+N'N'''+@VALOR_TEXT+N''''+N','+N'N'''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "                                                   ELSE" & vbCrLf
sConsulta = sConsulta & "                                                       SET @SQL=@SQL+'N'''+@VALOR_TEXT+''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+ N'_COD='+N'N'''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "                                           ELSE" & vbCrLf
sConsulta = sConsulta & "                                               IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "                                                   SET @SQL = @SQL + 'NULL'" & vbCrLf
sConsulta = sConsulta & "                                               ELSE" & vbCrLf
sConsulta = sConsulta & "                                                   IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "                                                       SET @SQL=@SQL+N'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "                                                   ELSE" & vbCrLf
sConsulta = sConsulta & "                                                       SET @SQL=@SQL+N'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+ N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "                                       END" & vbCrLf
sConsulta = sConsulta & "                                   END" & vbCrLf
sConsulta = sConsulta & "                               END" & vbCrLf
sConsulta = sConsulta & "                           END" & vbCrLf
sConsulta = sConsulta & "                       END" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               END" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "           SET @SQL = @SQL + ','" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "           FETCH NEXT FROM curSOLICITUDES_CAMPOS INTO @COPIACAMPO_ID,@CAMPO_ORIGEN, @SUBTIPO, @TIPO_CAMPO_GS, @VALOR_TEXT, @VALOR_NUM, @VALOR_FEC, @VALOR_BOOL, @VALOR_TEXT_COD, @VALOR_TEXT_DEN, @INTRO, @ID_ATRIB_GS,@TABLA_EXTERNA, @CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       CLOSE curSOLICITUDES_CAMPOS" & vbCrLf
sConsulta = sConsulta & "       DEALLOCATE curSOLICITUDES_CAMPOS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       IF @EJECUTARSQL=1" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           IF @INSERT = 1" & vbCrLf
sConsulta = sConsulta & "               SET @SQL = LEFT(@SQLCAMPOSINSERT,LEN(@SQLCAMPOSINSERT)-1) + ') ' + LEFT(@SQL,LEN(@SQL)-1) + ')'" & vbCrLf
sConsulta = sConsulta & "           ELSE" & vbCrLf
sConsulta = sConsulta & "               SET @SQL = LEFT(@SQL,LEN(@SQL)-1) + ' WHERE INSTANCIA = @INSTANCIA_L'" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "           SET @SQL=N'IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = ''dbo'' AND  TABLE_NAME='''+@IDIOMA+N'_'+@TABLA+''')) BEGIN '+@SQL+N' END'       " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA_L INT', @INSTANCIA_L=@INSTANCIA_L " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "          DELETE FROM #T_VALORES_NUM" & vbCrLf
sConsulta = sConsulta & "       END    " & vbCrLf
sConsulta = sConsulta & "       --*********************************" & vbCrLf
sConsulta = sConsulta & "       --*********************************" & vbCrLf
sConsulta = sConsulta & "       -- AHORA LOS CAMPOS DE LOS DESGLOSES (SI NO ES QA NI CONTRATO)" & vbCrLf
sConsulta = sConsulta & "       --**********************************" & vbCrLf
sConsulta = sConsulta & "       IF @QA=0 AND @CONTRATO=0" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           SET @SQL=''" & vbCrLf
sConsulta = sConsulta & "           SET @SQLCAMPOSINSERT=''" & vbCrLf
sConsulta = sConsulta & "           SET @EJECUTARSQL=0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "           --Pongo insert a 1 para que siempre inserte, ya que si de una etapa a otra se crean nuevas lineas de desglose s?lo estar?a haciendo update de las lineas existentes" & vbCrLf
sConsulta = sConsulta & "           --Ahora elimino todas las l?neas y vuelvo a insertar las que haya en ese momento, as? tambi?n controlo si se elimina alguna l?nea" & vbCrLf
sConsulta = sConsulta & "           DECLARE @SQLDELETE NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "           DECLARE @INSERT_ORIGINAL TINYINT" & vbCrLf
sConsulta = sConsulta & "           SET @SQLDELETE=''" & vbCrLf
sConsulta = sConsulta & "           SET @INSERT_ORIGINAL=@INSERT" & vbCrLf
sConsulta = sConsulta & "           SET @INSERT=1" & vbCrLf
sConsulta = sConsulta & "           SET @SQLDELETE='DELETE '+@IDIOMA+N'_'+@TABLA+N'_DESGLOSE WHERE FORM_INSTANCIA=@INSTANCIA_L'" & vbCrLf
sConsulta = sConsulta & "           SET @SQLDELETE=N'IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = ''dbo'' AND  TABLE_NAME='''+@IDIOMA+N'_'+@TABLA+N'_DESGLOSE'')) BEGIN '+@SQLDELETE+N' END'" & vbCrLf
sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQLDELETE, N'@INSTANCIA_L INT',@INSTANCIA_L=@INSTANCIA_L" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "           IF @INSERT = 1" & vbCrLf
sConsulta = sConsulta & "           BEGIN" & vbCrLf
sConsulta = sConsulta & "               SET @SQLCAMPOSINSERT=N'INSERT INTO '+@IDIOMA+N'_'+@TABLA+N'_DESGLOSE (FORM_INSTANCIA,LINEA,DESGLOSE,'" & vbCrLf
sConsulta = sConsulta & "               SET @SQL=N' VALUES (@INSTANCIA_L,@LINEA,'" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "           ELSE" & vbCrLf
sConsulta = sConsulta & "               SET @SQL=N' UPDATE '+@IDIOMA+N'_'+@TABLA+N'_DESGLOSE SET '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "           DECLARE curSOLICITUDES_CAMPOS_DESGLOSE CURSOR LOCAL FOR" & vbCrLf
sConsulta = sConsulta & "           SELECT DISTINCT CLD.LINEA,FC.ID CAMPO_ORIGEN,FC.SUBTIPO,FC.TIPO_CAMPO_GS,CLD.VALOR_TEXT,CLD.VALOR_NUM,CLD.VALOR_FEC,CLD.VALOR_BOOL," & vbCrLf
sConsulta = sConsulta & "               CLD.VALOR_TEXT_COD,CLD.VALOR_TEXT_DEN,FC.INTRO,FC.ID_ATRIB_GS,FC.TABLA_EXTERNA,CC.COPIA_CAMPO_DEF," & vbCrLf
sConsulta = sConsulta & "               D.CAMPO_PADRE" & vbCrLf
sConsulta = sConsulta & "           FROM COPIA_CAMPO CC WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN INSTANCIA I WITH(NOLOCK) ON I.ID=CC.INSTANCIA AND CC.NUM_VERSION=I.ULT_VERSION" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN FORM_CAMPO FC WITH (NOLOCK) ON FC.ID=CC.COPIA_CAMPO_DEF AND FC.BAJALOG = 0" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN FORM_GRUPO FG WITH (NOLOCK) ON FG.ID = FC.GRUPO AND FG.BAJALOG = 0" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN COPIA_LINEA_DESGLOSE CLD WITH(NOLOCK) ON CLD.CAMPO_HIJO=CC.ID" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN DESGLOSE D WITH(NOLOCK) ON D.CAMPO_HIJO=FC.ID" & vbCrLf
sConsulta = sConsulta & "           WHERE FC.ES_SUBCAMPO=1 AND FC.SUBTIPO<>9 AND (FC.TIPO_CAMPO_GS<>106 OR FC.TIPO_CAMPO_GS IS NULL)" & vbCrLf
sConsulta = sConsulta & "               AND (FC.SUBTIPO=FC.SUBTIPO OR (FC.SUBTIPO=5 AND FC.SUBTIPO IN (6,7)) OR (FC.SUBTIPO=6 AND FC.SUBTIPO=7))--si la definici?n del campo ha cambiado de texto corto a texto medio o largo, o de texto medio a texto largo" & vbCrLf
sConsulta = sConsulta & "               AND FC.ID NOT IN (SELECT ID_CAMPO FROM #G1)  -- campos que se han a?adido en el formulario pero no se ha hecho todavia ALTER en las tablas de filtros (BatchUpdates)" & vbCrLf
sConsulta = sConsulta & "               AND I.ID=@INSTANCIA_L" & vbCrLf
sConsulta = sConsulta & "           ORDER BY CAMPO_PADRE,LINEA         " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "           OPEN curSOLICITUDES_CAMPOS_DESGLOSE" & vbCrLf
sConsulta = sConsulta & "           FETCH NEXT FROM curSOLICITUDES_CAMPOS_DESGLOSE INTO @LINEA,@CAMPO_ORIGEN,@SUBTIPO,@TIPO_CAMPO_GS,@VALOR_TEXT," & vbCrLf
sConsulta = sConsulta & "           @VALOR_NUM,@VALOR_FEC,@VALOR_BOOL,@VALOR_TEXT_COD,@VALOR_TEXT_DEN,@INTRO,@ID_ATRIB_GS,@TABLA_EXTERNA," & vbCrLf
sConsulta = sConsulta & "           @CAMPO_DEF,@CAMPO_PADRE_DESGLOSE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "          SET @CAMPO_NUM_COUNT=0" & vbCrLf
sConsulta = sConsulta & "           SET @CAMPO_PADRE_DESGLOSE_ANT=@CAMPO_PADRE_DESGLOSE" & vbCrLf
sConsulta = sConsulta & "           DECLARE @SQLAUX NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "           DECLARE @SQLCAMPOSINSERTAUX NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "           SET @SQLAUX=@SQL" & vbCrLf
sConsulta = sConsulta & "           SET @SQLCAMPOSINSERTAUX=@SQLCAMPOSINSERT" & vbCrLf
sConsulta = sConsulta & "           IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "           SET @SQL=@SQL+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N','" & vbCrLf
sConsulta = sConsulta & "           WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "           BEGIN" & vbCrLf
sConsulta = sConsulta & "               SET @EJECUTARSQL=1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "               IF @LINEA<>@LINEA_ANT OR @CAMPO_PADRE_DESGLOSE<>@CAMPO_PADRE_DESGLOSE_ANT" & vbCrLf
sConsulta = sConsulta & "               BEGIN" & vbCrLf
sConsulta = sConsulta & "                   IF @EJECUTARSQL=1" & vbCrLf
sConsulta = sConsulta & "                   BEGIN" & vbCrLf
sConsulta = sConsulta & "                       IF @INSERT = 1" & vbCrLf
sConsulta = sConsulta & "                           SET @SQL=LEFT(@SQLCAMPOSINSERT,LEN(@SQLCAMPOSINSERT)-1) + ') '+LEFT(@SQL,LEN(@SQL)-1)+')'" & vbCrLf
sConsulta = sConsulta & "                       ELSE" & vbCrLf
sConsulta = sConsulta & "                           SET @SQL=LEFT(@SQL,LEN(@SQL)-1)+' WHERE FORM_INSTANCIA=@INSTANCIA_L AND DESGLOSE=@CAMPO_PADRE_DESGLOSE AND LINEA=@LINEA '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                       SET @SQL=N'IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = ''dbo'' AND  TABLE_NAME='''+@IDIOMA+N'_'+@TABLA+N'_DESGLOSE'')) BEGIN '+@SQL+N' END'" & vbCrLf
sConsulta = sConsulta & "                       EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA_L INT,@CAMPO_PADRE_DESGLOSE INT,@LINEA INT',@INSTANCIA_L=@INSTANCIA_L,@CAMPO_PADRE_DESGLOSE=@CAMPO_PADRE_DESGLOSE_ANT,@LINEA=@LINEA_ANT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                       SET @SQL=@SQLAUX" & vbCrLf
sConsulta = sConsulta & "                       SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERTAUX" & vbCrLf
sConsulta = sConsulta & "                       SET @LINEA_ANT=@LINEA" & vbCrLf
sConsulta = sConsulta & "                       SET @CAMPO_PADRE_DESGLOSE_ANT=@CAMPO_PADRE_DESGLOSE" & vbCrLf
sConsulta = sConsulta & "                       IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "                           SET @SQL=@SQL+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N','" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               END" & vbCrLf
sConsulta = sConsulta & "               --Reemplazamos las comillas simples en valor_text y valor_text_den" & vbCrLf
sConsulta = sConsulta & "               IF @VALOR_TEXT IS NOT NULL              " & vbCrLf
sConsulta = sConsulta & "                   SET @VALOR_TEXT = REPLACE(@VALOR_TEXT,'''','''''')" & vbCrLf
sConsulta = sConsulta & "               " & vbCrLf
sConsulta = sConsulta & "               IF @VALOR_TEXT_DEN IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @VALOR_TEXT_DEN = REPLACE(@VALOR_TEXT_DEN,'''','''''')" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "               IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "                   SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT + N'C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N','" & vbCrLf
sConsulta = sConsulta & "               ELSE" & vbCrLf
sConsulta = sConsulta & "                   SET @SQL=@SQL+N'C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'='" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "               ----------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "               IF @SUBTIPO = 3 -- tipo fecha               " & vbCrLf
sConsulta = sConsulta & "                   IF @VALOR_FEC IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                       SET @SQL = @SQL + N'CONVERT(DATETIME,''' + CONVERT(VARCHAR,@VALOR_FEC) + ''')'" & vbCrLf
sConsulta = sConsulta & "                   ELSE" & vbCrLf
sConsulta = sConsulta & "                       SET @SQL = @SQL + N'NULL'               " & vbCrLf
sConsulta = sConsulta & "               ELSE" & vbCrLf
sConsulta = sConsulta & "               BEGIN" & vbCrLf
sConsulta = sConsulta & "                   IF @SUBTIPO = 4 -- tipo booleano                    " & vbCrLf
sConsulta = sConsulta & "                       IF @VALOR_BOOL IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                           SET @SQL = @SQL + CONVERT(NVARCHAR,@VALOR_BOOL)" & vbCrLf
sConsulta = sConsulta & "                       ELSE" & vbCrLf
sConsulta = sConsulta & "                           SET @SQL = @SQL + N'NULL'                   " & vbCrLf
sConsulta = sConsulta & "                   ELSE" & vbCrLf
sConsulta = sConsulta & "                   BEGIN" & vbCrLf
sConsulta = sConsulta & "                       IF @SUBTIPO = 8 -- tipo archivo                     " & vbCrLf
sConsulta = sConsulta & "                           IF @VALOR_TEXT IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                               SET @SQL = @SQL + N'N''' + LEFT(@VALOR_TEXT, 800) + N''''" & vbCrLf
sConsulta = sConsulta & "                           ELSE" & vbCrLf
sConsulta = sConsulta & "                               SET @SQL = @SQL + N'NULL'                       " & vbCrLf
sConsulta = sConsulta & "                       ELSE" & vbCrLf
sConsulta = sConsulta & "                       BEGIN" & vbCrLf
sConsulta = sConsulta & "                           IF @TIPO_CAMPO_GS IS NULL" & vbCrLf
sConsulta = sConsulta & "                           BEGIN" & vbCrLf
sConsulta = sConsulta & "                               IF @TABLA_EXTERNA IS NULL" & vbCrLf
sConsulta = sConsulta & "                               BEGIN" & vbCrLf
sConsulta = sConsulta & "                                   IF @SUBTIPO=2 -- tipo num?rico " & vbCrLf
sConsulta = sConsulta & "                                  BEGIN" & vbCrLf
sConsulta = sConsulta & "                                       SET @CAMPO_NUM_COUNT=@CAMPO_NUM_COUNT+1                                 " & vbCrLf
sConsulta = sConsulta & "                                       INSERT INTO #T_VALORES_NUM VALUES (@CAMPO_NUM_COUNT,@VALOR_NUM)                                                                         " & vbCrLf
sConsulta = sConsulta & "                                       SET @SQL = @SQL + '(SELECT VALOR FROM #T_VALORES_NUM WHERE ID=' + CAST(@CAMPO_NUM_COUNT AS NVARCHAR) + ')'                                                                                                             " & vbCrLf
sConsulta = sConsulta & "                                   END                                      " & vbCrLf
sConsulta = sConsulta & "                                   ELSE    -- tipo texto o tipo lista texto" & vbCrLf
sConsulta = sConsulta & "                                   BEGIN" & vbCrLf
sConsulta = sConsulta & "                                       IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "                                           SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "                                           " & vbCrLf
sConsulta = sConsulta & "                                       IF @INTRO=1 AND @VALOR_NUM IS NOT NULL --tipo lista texto" & vbCrLf
sConsulta = sConsulta & "                                       BEGIN" & vbCrLf
sConsulta = sConsulta & "                                           IF @ID_ATRIB_GS IS NULL" & vbCrLf
sConsulta = sConsulta & "                                           BEGIN" & vbCrLf
sConsulta = sConsulta & "                                               IF @IDIOMA='SPA'" & vbCrLf
sConsulta = sConsulta & "                                                   SELECT @DENOMINACION=VALOR_TEXT_SPA" & vbCrLf
sConsulta = sConsulta & "                                                   FROM CAMPO_VALOR_LISTA WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                                                   WHERE CAMPO=CONVERT(VARCHAR,@CAMPO_DEF) AND ORDEN=CONVERT(VARCHAR,@VALOR_NUM)" & vbCrLf
sConsulta = sConsulta & "                                               IF @IDIOMA='ENG'" & vbCrLf
sConsulta = sConsulta & "                                                   SELECT @DENOMINACION=VALOR_TEXT_ENG" & vbCrLf
sConsulta = sConsulta & "                                                   FROM CAMPO_VALOR_LISTA WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                                                   WHERE CAMPO=CONVERT(VARCHAR,@CAMPO_DEF) AND ORDEN=CONVERT(VARCHAR,@VALOR_NUM)" & vbCrLf
sConsulta = sConsulta & "                                               IF @IDIOMA='GER'" & vbCrLf
sConsulta = sConsulta & "                                                   SELECT @DENOMINACION=VALOR_TEXT_GER" & vbCrLf
sConsulta = sConsulta & "                                                   FROM CAMPO_VALOR_LISTA WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                                                   WHERE CAMPO=CONVERT(VARCHAR,@CAMPO_DEF) AND ORDEN=CONVERT(VARCHAR,@VALOR_NUM)" & vbCrLf
sConsulta = sConsulta & "                                               IF @IDIOMA='FRA'" & vbCrLf
sConsulta = sConsulta & "                                                   SELECT @DENOMINACION=VALOR_TEXT_FRA" & vbCrLf
sConsulta = sConsulta & "                                                   FROM CAMPO_VALOR_LISTA WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                                                   WHERE CAMPO=CONVERT(VARCHAR,@CAMPO_DEF) AND ORDEN=CONVERT(VARCHAR,@VALOR_NUM)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                                               IF @DENOMINACION IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                                                   IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "                                                       SET @SQL=@SQL+N'N'''+REPLACE(@DENOMINACION,'''','''''')+''''+N',CONVERT(NVARCHAR,'+CONVERT(NVARCHAR,@VALOR_NUM)+N')'" & vbCrLf
sConsulta = sConsulta & "                                                   ELSE" & vbCrLf
sConsulta = sConsulta & "                                                       SET @SQL=@SQL+N'N'''+REPLACE(@DENOMINACION,'''','''''')+''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=CONVERT(NVARCHAR,'+CONVERT(NVARCHAR,@VALOR_NUM)+N')'" & vbCrLf
sConsulta = sConsulta & "                                               ELSE" & vbCrLf
sConsulta = sConsulta & "                                                   IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "                                                       SET @SQL=@SQL + N'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "                                                   ELSE" & vbCrLf
sConsulta = sConsulta & "                                                       SET @SQL=@SQL + N'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "                                           END" & vbCrLf
sConsulta = sConsulta & "                                           ELSE                                                " & vbCrLf
sConsulta = sConsulta & "                                               IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "                                                   SET @SQL=@SQL + N'N'''+@VALOR_TEXT+N''''+N','+N''''+CONVERT(NVARCHAR,@VALOR_NUM)+N''''" & vbCrLf
sConsulta = sConsulta & "                                               ELSE" & vbCrLf
sConsulta = sConsulta & "                                                   SET @SQL=@SQL + N'N'''+@VALOR_TEXT+N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+ CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N''''+CONVERT(NVARCHAR,@VALOR_NUM)+N''''                                               " & vbCrLf
sConsulta = sConsulta & "                                       END" & vbCrLf
sConsulta = sConsulta & "                                       ELSE -- tipo texto" & vbCrLf
sConsulta = sConsulta & "                                       BEGIN" & vbCrLf
sConsulta = sConsulta & "                                           IF @VALOR_TEXT IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                                           BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                                               IF @SUBTIPO=6 --TEXTO MEDIO: 800" & vbCrLf
sConsulta = sConsulta & "                                                   SET @VALOR_TEXT = LEFT(@VALOR_TEXT, 800)" & vbCrLf
sConsulta = sConsulta & "                                               IF @SUBTIPO=5 --TEXTO CORTO: 100" & vbCrLf
sConsulta = sConsulta & "                                                   SET @VALOR_TEXT = LEFT(@VALOR_TEXT, 100)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                                               IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "                                                   SET @SQL=@SQL + N'N'''+@VALOR_TEXT+N''''+N','+N'N'''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "                                               ELSE" & vbCrLf
sConsulta = sConsulta & "                                                   SET @SQL=@SQL + N'N'''+@VALOR_TEXT+N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+ CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N'N'''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "                                           END" & vbCrLf
sConsulta = sConsulta & "                                           ELSE" & vbCrLf
sConsulta = sConsulta & "                                               IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "                                                   SET @SQL=@SQL + N'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "                                               ELSE" & vbCrLf
sConsulta = sConsulta & "                                                   SET @SQL=@SQL+N'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "                                       END" & vbCrLf
sConsulta = sConsulta & "                                   END" & vbCrLf
sConsulta = sConsulta & "                               END" & vbCrLf
sConsulta = sConsulta & "                               ELSE" & vbCrLf
sConsulta = sConsulta & "                               BEGIN" & vbCrLf
sConsulta = sConsulta & "                                   IF @SUBTIPO=2 -- tipo num?rico" & vbCrLf
sConsulta = sConsulta & "                                   BEGIN" & vbCrLf
sConsulta = sConsulta & "                                       SET @CAMPO_NUM_COUNT=@CAMPO_NUM_COUNT+1                                 " & vbCrLf
sConsulta = sConsulta & "                                       INSERT INTO #T_VALORES_NUM VALUES (@CAMPO_NUM_COUNT,@VALOR_NUM)                                                                         " & vbCrLf
sConsulta = sConsulta & "                                       SET @SQL = @SQL + '(SELECT VALOR FROM #T_VALORES_NUM WHERE ID=' + CAST(@CAMPO_NUM_COUNT AS NVARCHAR) + ')'                                         " & vbCrLf
sConsulta = sConsulta & "                                   END" & vbCrLf
sConsulta = sConsulta & "                                   ELSE    -- tipo texto o tipo lista texto" & vbCrLf
sConsulta = sConsulta & "                                   BEGIN" & vbCrLf
sConsulta = sConsulta & "                                       IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "                                           SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                                       IF @VALOR_TEXT_DEN IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                                           IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "                                               SET @SQL=@SQL+N'N'''+@VALOR_TEXT_DEN+N''''+N','+N'N'''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "                                           ELSE" & vbCrLf
sConsulta = sConsulta & "                                               SET @SQL=@SQL+N'N'''+@VALOR_TEXT_DEN+N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N'N'''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "                                       ELSE" & vbCrLf
sConsulta = sConsulta & "                                           IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "                                               SET @SQL=@SQL+N'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "                                           ELSE" & vbCrLf
sConsulta = sConsulta & "                                               SET @SQL=@SQL+N'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_' +CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "                                   END" & vbCrLf
sConsulta = sConsulta & "                               END" & vbCrLf
sConsulta = sConsulta & "                           END" & vbCrLf
sConsulta = sConsulta & "                           ELSE -- campo del sistema" & vbCrLf
sConsulta = sConsulta & "                           BEGIN" & vbCrLf
sConsulta = sConsulta & "                               SET @CAMPOTRATADO = 0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                               ---------------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "                               -- Formas de pago, Monedas, Familia de materiales, Unidades, Destinos, Pa?ses y Provincias" & vbCrLf
sConsulta = sConsulta & "                               -- FORMATO: <Idioma>|<Texto>#<Idioma>|<Texto>" & vbCrLf
sConsulta = sConsulta & "                               -- FORMATO MATERIALES: cod. ultimo nivel (VALOR_TEXT) +  <Idioma>|<Texto>#<Idioma>|<Texto>" & vbCrLf
sConsulta = sConsulta & "                               ------------------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "                               IF @TIPO_CAMPO_GS = 101 OR @TIPO_CAMPO_GS = 102 OR @TIPO_CAMPO_GS = 103 OR @TIPO_CAMPO_GS = 105 OR @TIPO_CAMPO_GS = 109 OR @TIPO_CAMPO_GS = 107 OR @TIPO_CAMPO_GS = 108" & vbCrLf
sConsulta = sConsulta & "                               BEGIN" & vbCrLf
sConsulta = sConsulta & "                                   IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "                                       SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+ N'_COD,'" & vbCrLf
sConsulta = sConsulta & "                                   " & vbCrLf
sConsulta = sConsulta & "                                   SET @CAMPOTRATADO = 1" & vbCrLf
sConsulta = sConsulta & "                                   -- Materiales" & vbCrLf
sConsulta = sConsulta & "                                   IF @TIPO_CAMPO_GS = 103                                 " & vbCrLf
sConsulta = sConsulta & "                                       IF @VALOR_TEXT IS NOT NULL-- Al hacer Len de valor_text me est? quitando los espacios en blanco de la derecha" & vbCrLf
sConsulta = sConsulta & "                                           IF LEN(@VALOR_TEXT + '#')-1 = @LEN_GMN1 + @LEN_GMN2 + @LEN_GMN3 + @LEN_GMN4" & vbCrLf
sConsulta = sConsulta & "                                               SET @SQL = @SQL + N'N''' + RTRIM(RIGHT(@VALOR_TEXT,@LEN_GMN4)) + N' - '" & vbCrLf
sConsulta = sConsulta & "                                           ELSE                                            " & vbCrLf
sConsulta = sConsulta & "                                               IF LEN(@VALOR_TEXT + '#')-1 = @LEN_GMN1 + @LEN_GMN2 + @LEN_GMN3" & vbCrLf
sConsulta = sConsulta & "                                                   SET @SQL = @SQL + N'N''' + RTRIM(RIGHT(@VALOR_TEXT,@LEN_GMN3)) + N' - '" & vbCrLf
sConsulta = sConsulta & "                                               ELSE                                                " & vbCrLf
sConsulta = sConsulta & "                                                   IF LEN(@VALOR_TEXT + '#')-1 = @LEN_GMN1 + @LEN_GMN2" & vbCrLf
sConsulta = sConsulta & "                                                       SET @SQL = @SQL + N'N''' + RTRIM(RIGHT(@VALOR_TEXT,@LEN_GMN2)) + N' - '" & vbCrLf
sConsulta = sConsulta & "                                                   ELSE" & vbCrLf
sConsulta = sConsulta & "                                                       SET @SQL = @SQL + N'N''' + RTRIM(RIGHT(@VALOR_TEXT,@LEN_GMN1)) + N' - '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                                   -- Todos" & vbCrLf
sConsulta = sConsulta & "                                   IF RIGHT(@VALOR_TEXT_DEN,1)='#' SET @VALOR_TEXT_DEN=LEFT(@VALOR_TEXT_DEN,LEN(@VALOR_TEXT_DEN)-1)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                                   SET @EMPIEZAIDIOMA=CHARINDEX(@IDIOMA + '|',@VALOR_TEXT_DEN)" & vbCrLf
sConsulta = sConsulta & "                                   " & vbCrLf
sConsulta = sConsulta & "                                   IF (@EMPIEZAIDIOMA) > 0" & vbCrLf
sConsulta = sConsulta & "                                   BEGIN" & vbCrLf
sConsulta = sConsulta & "                                       SET @ACABADENOMINACION = CHARINDEX('#',@VALOR_TEXT_DEN,@EMPIEZAIDIOMA)" & vbCrLf
sConsulta = sConsulta & "                                       SET @EMPIEZADENOMINACION = CHARINDEX('|',@VALOR_TEXT_DEN,@EMPIEZAIDIOMA)+1" & vbCrLf
sConsulta = sConsulta & "                                       IF @TIPO_CAMPO_GS <> 103" & vbCrLf
sConsulta = sConsulta & "                                           SET @SQL=@SQL+'N'''" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                                       IF @ACABADENOMINACION>0" & vbCrLf
sConsulta = sConsulta & "                                           SET @SQL=@SQL+SUBSTRING(@VALOR_TEXT_DEN,@EMPIEZADENOMINACION,@ACABADENOMINACION-@EMPIEZADENOMINACION) + ''''" & vbCrLf
sConsulta = sConsulta & "                                       ELSE" & vbCrLf
sConsulta = sConsulta & "                                           SET @SQL=@SQL+RIGHT(@VALOR_TEXT_DEN,LEN(@VALOR_TEXT_DEN)-@EMPIEZADENOMINACION+1) + ''''" & vbCrLf
sConsulta = sConsulta & "                                       IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "                                           SET @SQL=@SQL+N','+N'N'''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "                                       ELSE" & vbCrLf
sConsulta = sConsulta & "                                           SET @SQL=@SQL+N',C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N'N'''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "                                   END" & vbCrLf
sConsulta = sConsulta & "                                   ELSE                                    " & vbCrLf
sConsulta = sConsulta & "                                       IF @VALOR_TEXT IS NOT NULL AND @TIPO_CAMPO_GS = 103" & vbCrLf
sConsulta = sConsulta & "                                           IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "                                               SET @SQL=@SQL+N''''+N','+N'N'''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "                                           ELSE" & vbCrLf
sConsulta = sConsulta & "                                               SET @SQL=@SQL+N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N'N'''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "                                       ELSE" & vbCrLf
sConsulta = sConsulta & "                                           IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "                                               SET @SQL=@SQL+N'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "                                           ELSE" & vbCrLf
sConsulta = sConsulta & "                                               SET @SQL=@SQL+N'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'                                    " & vbCrLf
sConsulta = sConsulta & "                               END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                               --------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "                               -- Proveedores, Departamentos, Organizaciones de Compras, Centros" & vbCrLf
sConsulta = sConsulta & "                               -- FORMATO: C?digo (VALOR_TEXT) - Denominaci?n (VALOR_TEXT_DEN)" & vbCrLf
sConsulta = sConsulta & "                               --------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "                               IF @TIPO_CAMPO_GS = 11 OR @TIPO_CAMPO_GS = 100  OR @TIPO_CAMPO_GS = 122 OR @TIPO_CAMPO_GS = 123 OR @TIPO_CAMPO_GS = 124" & vbCrLf
sConsulta = sConsulta & "                               BEGIN" & vbCrLf
sConsulta = sConsulta & "                                   SET @CAMPOTRATADO = 1" & vbCrLf
sConsulta = sConsulta & "                                   IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "                                       SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "                                   " & vbCrLf
sConsulta = sConsulta & "                                   IF @VALOR_TEXT IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                                       IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "                                           SET @SQL=@SQL+N'N'''+@VALOR_TEXT+N' - '+@VALOR_TEXT_DEN+N''''+N','+N'N'''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "                                       ELSE" & vbCrLf
sConsulta = sConsulta & "                                           SET @SQL=@SQL+N'N'''+@VALOR_TEXT+N' - '+@VALOR_TEXT_DEN+N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N'N'''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "                                   ELSE" & vbCrLf
sConsulta = sConsulta & "                                       IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "                                           SET @SQL=@SQL+N'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "                                       ELSE" & vbCrLf
sConsulta = sConsulta & "                                           SET @SQL=@SQL+N'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "                               END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                               --------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "                               -- Centros de coste, Partidas presupuestarias, Activos" & vbCrLf
sConsulta = sConsulta & "                               -- FORMATO: cod. ultimo nivel (VALOR_TEXT) +  <Idioma>|<Texto>#<Idioma>|<Texto>" & vbCrLf
sConsulta = sConsulta & "                               --------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "                               IF @TIPO_CAMPO_GS = 129 OR @TIPO_CAMPO_GS = 130 OR @TIPO_CAMPO_GS = 131 OR @TIPO_CAMPO_GS=132" & vbCrLf
sConsulta = sConsulta & "                               BEGIN" & vbCrLf
sConsulta = sConsulta & "                                   SET @CAMPOTRATADO=1" & vbCrLf
sConsulta = sConsulta & "                                   IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "                                       SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "                                   " & vbCrLf
sConsulta = sConsulta & "                                   IF @VALOR_TEXT IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                                   BEGIN" & vbCrLf
sConsulta = sConsulta & "                                       SET @SEPARADOR = CHARINDEX('#',@VALOR_TEXT,1)" & vbCrLf
sConsulta = sConsulta & "                                       WHILE @SEPARADOR > 0" & vbCrLf
sConsulta = sConsulta & "                                       BEGIN" & vbCrLf
sConsulta = sConsulta & "                                           SET @VALOR_TEXT = RIGHT(@VALOR_TEXT,LEN(@VALOR_TEXT)-@SEPARADOR)" & vbCrLf
sConsulta = sConsulta & "                                           SET @SEPARADOR = CHARINDEX('#',@VALOR_TEXT,1)" & vbCrLf
sConsulta = sConsulta & "                                       END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                                       SET @SEPARADOR = CHARINDEX('|',@VALOR_TEXT,1)" & vbCrLf
sConsulta = sConsulta & "                                       WHILE @SEPARADOR > 0" & vbCrLf
sConsulta = sConsulta & "                                       BEGIN" & vbCrLf
sConsulta = sConsulta & "                                           SET @VALOR_TEXT = RIGHT(@VALOR_TEXT,LEN(@VALOR_TEXT)-@SEPARADOR)" & vbCrLf
sConsulta = sConsulta & "                                           SET @SEPARADOR = CHARINDEX('|',@VALOR_TEXT,1)" & vbCrLf
sConsulta = sConsulta & "                                       END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                                       SET @SQL = @SQL + 'N''' + @VALOR_TEXT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                                       --Denominaci?n en el idioma" & vbCrLf
sConsulta = sConsulta & "                                       IF RIGHT(@VALOR_TEXT_DEN,1)='#' SET @VALOR_TEXT_DEN=LEFT(@VALOR_TEXT_DEN,LEN(@VALOR_TEXT_DEN)-1)" & vbCrLf
sConsulta = sConsulta & "                                       SET @EMPIEZAIDIOMA=CHARINDEX(@IDIOMA + '|',@VALOR_TEXT_DEN)" & vbCrLf
sConsulta = sConsulta & "                                       IF (@EMPIEZAIDIOMA) > 0" & vbCrLf
sConsulta = sConsulta & "                                       BEGIN" & vbCrLf
sConsulta = sConsulta & "                                           SET @ACABADENOMINACION = CHARINDEX('#',@VALOR_TEXT_DEN,@EMPIEZAIDIOMA)" & vbCrLf
sConsulta = sConsulta & "                                           SET @EMPIEZADENOMINACION = CHARINDEX('|',@VALOR_TEXT_DEN,@EMPIEZAIDIOMA)+1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                                           IF @ACABADENOMINACION>0" & vbCrLf
sConsulta = sConsulta & "                                               SET @SQL = @SQL + ' - ' + SUBSTRING(@VALOR_TEXT_DEN,@EMPIEZADENOMINACION,@ACABADENOMINACION-@EMPIEZADENOMINACION)" & vbCrLf
sConsulta = sConsulta & "                                           ELSE" & vbCrLf
sConsulta = sConsulta & "                                               SET @SQL = @SQL + ' - ' + RIGHT(@VALOR_TEXT_DEN,LEN(@VALOR_TEXT_DEN)-@EMPIEZADENOMINACION+1)" & vbCrLf
sConsulta = sConsulta & "                                       END" & vbCrLf
sConsulta = sConsulta & "                                       IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "                                           SET @SQL=@SQL+N''''+N','+N'N'''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "                                       ELSE" & vbCrLf
sConsulta = sConsulta & "                                           SET @SQL=@SQL+N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N'N'''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "                                   END" & vbCrLf
sConsulta = sConsulta & "                                   ELSE" & vbCrLf
sConsulta = sConsulta & "                                       IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "                                           SET @SQL=@SQL+N'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "                                       ELSE" & vbCrLf
sConsulta = sConsulta & "                                           SET @SQL=@SQL+N'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "                               END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                               --------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "                               -- Personas, Comprador" & vbCrLf
sConsulta = sConsulta & "                               ---------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "                               IF @TIPO_CAMPO_GS = 115 OR @TIPO_CAMPO_GS=144" & vbCrLf
sConsulta = sConsulta & "                               BEGIN" & vbCrLf
sConsulta = sConsulta & "                                   SET @CAMPOTRATADO = 1" & vbCrLf
sConsulta = sConsulta & "                                   IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "                                       SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "                                   " & vbCrLf
sConsulta = sConsulta & "                                   IF @VALOR_TEXT_DEN IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                                       IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "                                           SET @SQL=@SQL+N'N'''+@VALOR_TEXT_DEN+N''''+N','+N'N'''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "                                       ELSE" & vbCrLf
sConsulta = sConsulta & "                                           SET @SQL=@SQL+N'N'''+@VALOR_TEXT_DEN+N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N'N'''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "                                   ELSE" & vbCrLf
sConsulta = sConsulta & "                                       IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "                                           SET @SQL = @SQL + 'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "                                       ELSE" & vbCrLf
sConsulta = sConsulta & "                                           SET @SQL = @SQL + 'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "                               END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                               --------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "                               -- Almacen / Empresa" & vbCrLf
sConsulta = sConsulta & "                               ---------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "                               IF @TIPO_CAMPO_GS = 125 OR @TIPO_CAMPO_GS = 149" & vbCrLf
sConsulta = sConsulta & "                               BEGIN" & vbCrLf
sConsulta = sConsulta & "                                   SET @CAMPOTRATADO = 1" & vbCrLf
sConsulta = sConsulta & "                                   IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "                                       SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "                                   " & vbCrLf
sConsulta = sConsulta & "                                   IF @VALOR_TEXT_DEN IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                                       IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "                                           SET @SQL=@SQL+N'N'''+@VALOR_TEXT_DEN+N''''+N','+N''''+CONVERT(NVARCHAR,@VALOR_NUM)+N''''" & vbCrLf
sConsulta = sConsulta & "                                       ELSE" & vbCrLf
sConsulta = sConsulta & "                                           SET @SQL=@SQL+N'N'''+@VALOR_TEXT_DEN+N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N''''+CONVERT(NVARCHAR,@VALOR_NUM)+N''''" & vbCrLf
sConsulta = sConsulta & "                                   ELSE" & vbCrLf
sConsulta = sConsulta & "                                       IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "                                           SET @SQL = @SQL + 'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "                                       ELSE" & vbCrLf
sConsulta = sConsulta & "                                           SET @SQL = @SQL + 'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "                               END                             " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                               ------------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "                               -- Cod. Art?culo (viejo), Cod. Art?culo (nuevo), Denominaci?n del art?culo y Unidades Organizativas" & vbCrLf
sConsulta = sConsulta & "                               ------------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "                               IF @TIPO_CAMPO_GS = 104  OR @TIPO_CAMPO_GS = 118 OR @TIPO_CAMPO_GS = 119 OR @TIPO_CAMPO_GS = 121" & vbCrLf
sConsulta = sConsulta & "                               BEGIN" & vbCrLf
sConsulta = sConsulta & "                                   SET @CAMPOTRATADO = 1" & vbCrLf
sConsulta = sConsulta & "                                   IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "                                       SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "                                   " & vbCrLf
sConsulta = sConsulta & "                                   IF @VALOR_TEXT IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                                       IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "                                           SET @SQL=@SQL+N'N'''+@VALOR_TEXT+N''''+N','+N'N'''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "                                       ELSE" & vbCrLf
sConsulta = sConsulta & "                                           SET @SQL=@SQL+N'N'''+@VALOR_TEXT +N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N'N'''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "                                   ELSE" & vbCrLf
sConsulta = sConsulta & "                                       IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "                                           SET @SQL=@SQL+N'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "                                       ELSE" & vbCrLf
sConsulta = sConsulta & "                                           SET @SQL=@SQL+N'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "                               END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                               ---------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "                               -- Presupuestos" & vbCrLf
sConsulta = sConsulta & "                               -- FORMATO: Ej 2005 - PC-002  (50.00%); 2005 - PC-001 (50.00%);" & vbCrLf
sConsulta = sConsulta & "                               --  VALOR_TEXT_DEN: 2005 - PC-002#2005 - PC-001" & vbCrLf
sConsulta = sConsulta & "                               --  VALOR_TEXT: 1_11_0.5#1_10_0.5" & vbCrLf
sConsulta = sConsulta & "                               ---------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "                               IF @TIPO_CAMPO_GS = 110  OR @TIPO_CAMPO_GS = 111 OR @TIPO_CAMPO_GS = 112 OR @TIPO_CAMPO_GS = 113" & vbCrLf
sConsulta = sConsulta & "                               BEGIN" & vbCrLf
sConsulta = sConsulta & "                                   SET @CAMPOTRATADO = 1" & vbCrLf
sConsulta = sConsulta & "                                   IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "                                       SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "                                   " & vbCrLf
sConsulta = sConsulta & "                                   SET @PRESUPUESTOS = ''" & vbCrLf
sConsulta = sConsulta & "                                   SET @SEPARADOR_DEN = CHARINDEX('#',@VALOR_TEXT_DEN)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                                   WHILE @SEPARADOR_DEN > 0" & vbCrLf
sConsulta = sConsulta & "                                   BEGIN" & vbCrLf
sConsulta = sConsulta & "                                       SET @PRESUPUESTOS = @PRESUPUESTOS + SUBSTRING(@VALOR_TEXT_DEN,1,@SEPARADOR_DEN-1)" & vbCrLf
sConsulta = sConsulta & "                                       SET @VALOR_TEXT_DEN = RIGHT(@VALOR_TEXT_DEN,LEN(@VALOR_TEXT_DEN)-@SEPARADOR_DEN)" & vbCrLf
sConsulta = sConsulta & "                                       SET @PORCENTAJE = SUBSTRING(@VALOR_TEXT,1,CHARINDEX('#',@VALOR_TEXT)-1)" & vbCrLf
sConsulta = sConsulta & "                                       SET @SEPARADOR_PORCENTAJE = 1" & vbCrLf
sConsulta = sConsulta & "                                       " & vbCrLf
sConsulta = sConsulta & "                                       WHILE @SEPARADOR_PORCENTAJE > 0" & vbCrLf
sConsulta = sConsulta & "                                       BEGIN" & vbCrLf
sConsulta = sConsulta & "                                           SET @PORCENTAJE = RIGHT(@PORCENTAJE,LEN(@PORCENTAJE)-@SEPARADOR_PORCENTAJE)" & vbCrLf
sConsulta = sConsulta & "                                           SET @SEPARADOR_PORCENTAJE = CHARINDEX('_',@PORCENTAJE,@SEPARADOR_PORCENTAJE+1)" & vbCrLf
sConsulta = sConsulta & "                                       END" & vbCrLf
sConsulta = sConsulta & "                                       SET @PRESUPUESTOS = @PRESUPUESTOS + '(' + CONVERT(VARCHAR,CONVERT(DECIMAL(3,2),@PORCENTAJE)*100) + '%); '" & vbCrLf
sConsulta = sConsulta & "                                       SET @VALOR_TEXT = RIGHT(@VALOR_TEXT,LEN(@VALOR_TEXT)-CHARINDEX('#',@VALOR_TEXT))" & vbCrLf
sConsulta = sConsulta & "                                       SET @SEPARADOR_DEN = CHARINDEX('#',@VALOR_TEXT_DEN)" & vbCrLf
sConsulta = sConsulta & "                                   END" & vbCrLf
sConsulta = sConsulta & "                                   SET @PORCENTAJE = @VALOR_TEXT" & vbCrLf
sConsulta = sConsulta & "                                   SET @SEPARADOR_PORCENTAJE = 1" & vbCrLf
sConsulta = sConsulta & "                                   WHILE @SEPARADOR_PORCENTAJE > 0" & vbCrLf
sConsulta = sConsulta & "                                   BEGIN" & vbCrLf
sConsulta = sConsulta & "                                       SET @PORCENTAJE = RIGHT(@PORCENTAJE,LEN(@PORCENTAJE)-@SEPARADOR_PORCENTAJE)" & vbCrLf
sConsulta = sConsulta & "                                       SET @SEPARADOR_PORCENTAJE = CHARINDEX('_',@PORCENTAJE,@SEPARADOR_PORCENTAJE+1)" & vbCrLf
sConsulta = sConsulta & "                                   END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                                   SET @PRESUPUESTOS = @PRESUPUESTOS + @VALOR_TEXT_DEN + '(' + CAST(CONVERT(FLOAT,@PORCENTAJE)*100 AS VARCHAR) + '%);'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                                   IF @PRESUPUESTOS <> ''" & vbCrLf
sConsulta = sConsulta & "                                       IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "                                           SET @SQL = @SQL + N'N''' + @PRESUPUESTOS + N'''' + N',' + N'N''' + @VALOR_TEXT + N''''" & vbCrLf
sConsulta = sConsulta & "                                       ELSE" & vbCrLf
sConsulta = sConsulta & "                                           SET @SQL = @SQL + N'N''' + @PRESUPUESTOS + N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N'N'''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "                                   ELSE" & vbCrLf
sConsulta = sConsulta & "                                       IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "                                           SET @SQL = @SQL + 'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "                                       ELSE" & vbCrLf
sConsulta = sConsulta & "                                           SET @SQL = @SQL + 'NULL,'+N'C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "                               END                            " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                               IF @CAMPOTRATADO = 0 -- Resto" & vbCrLf
sConsulta = sConsulta & "                               BEGIN" & vbCrLf
sConsulta = sConsulta & "                                   IF @SUBTIPO = 3 -- tipo fecha                                   " & vbCrLf
sConsulta = sConsulta & "                                       IF @VALOR_FEC IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                                           SET @SQL = @SQL + '''' + @VALOR_FEC + ''''" & vbCrLf
sConsulta = sConsulta & "                                       ELSE" & vbCrLf
sConsulta = sConsulta & "                                           SET @SQL = @SQL + 'NULL'                                    " & vbCrLf
sConsulta = sConsulta & "                                   ELSE" & vbCrLf
sConsulta = sConsulta & "                                   BEGIN" & vbCrLf
sConsulta = sConsulta & "                                       IF @SUBTIPO = 4 -- tipo booleano                                        " & vbCrLf
sConsulta = sConsulta & "                                           IF @VALOR_BOOL IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                                               SET @SQL = @SQL + CONVERT(VARCHAR,@VALOR_BOOL)" & vbCrLf
sConsulta = sConsulta & "                                           ELSE" & vbCrLf
sConsulta = sConsulta & "                                               SET @SQL = @SQL + 'NULL'                                        " & vbCrLf
sConsulta = sConsulta & "                                       ELSE" & vbCrLf
sConsulta = sConsulta & "                                       BEGIN" & vbCrLf
sConsulta = sConsulta & "                                           IF @SUBTIPO=2 -- tipo num?rico                                          " & vbCrLf
sConsulta = sConsulta & "                                               IF @VALOR_NUM IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                                                   SET @SQL = @SQL + CONVERT(VARCHAR,@VALOR_NUM)" & vbCrLf
sConsulta = sConsulta & "                                               ELSE" & vbCrLf
sConsulta = sConsulta & "                                                   SET @SQL = @SQL + 'NULL'                                            " & vbCrLf
sConsulta = sConsulta & "                                           ELSE    -- tipo texto" & vbCrLf
sConsulta = sConsulta & "                                           BEGIN" & vbCrLf
sConsulta = sConsulta & "                                               IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "                                                   SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "                                           " & vbCrLf
sConsulta = sConsulta & "                                               IF @VALOR_TEXT IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                                                   IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "                                                       SET @SQL=@SQL+N'N'''+@VALOR_TEXT+N''''+N','+N'N'''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "                                                   ELSE" & vbCrLf
sConsulta = sConsulta & "                                                       SET @SQL=@SQL+'N'''+@VALOR_TEXT+''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+ N'_COD='+N'N'''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "                                               ELSE" & vbCrLf
sConsulta = sConsulta & "                                                   IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "                                                       SET @SQL=@SQL+N'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "                                                   ELSE" & vbCrLf
sConsulta = sConsulta & "                                                       SET @SQL=@SQL+N'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+ N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "                                           END" & vbCrLf
sConsulta = sConsulta & "                                       END" & vbCrLf
sConsulta = sConsulta & "                                   END" & vbCrLf
sConsulta = sConsulta & "                               END" & vbCrLf
sConsulta = sConsulta & "                           END" & vbCrLf
sConsulta = sConsulta & "                       END" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "               END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "               SET @SQL = @SQL + ','" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "               FETCH NEXT FROM curSOLICITUDES_CAMPOS_DESGLOSE INTO @LINEA,@CAMPO_ORIGEN,@SUBTIPO,@TIPO_CAMPO_GS," & vbCrLf
sConsulta = sConsulta & "               @VALOR_TEXT,@VALOR_NUM,@VALOR_FEC,@VALOR_BOOL,@VALOR_TEXT_COD,@VALOR_TEXT_DEN,@INTRO,@ID_ATRIB_GS," & vbCrLf
sConsulta = sConsulta & "               @TABLA_EXTERNA,@CAMPO_DEF,@CAMPO_PADRE_DESGLOSE" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "           CLOSE curSOLICITUDES_CAMPOS_DESGLOSE" & vbCrLf
sConsulta = sConsulta & "           DEALLOCATE curSOLICITUDES_CAMPOS_DESGLOSE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "           IF @EJECUTARSQL=1" & vbCrLf
sConsulta = sConsulta & "           BEGIN" & vbCrLf
sConsulta = sConsulta & "               IF @INSERT = 1" & vbCrLf
sConsulta = sConsulta & "                   SET @SQL = LEFT(@SQLCAMPOSINSERT,LEN(@SQLCAMPOSINSERT)-1) + ') ' + LEFT(@SQL,LEN(@SQL)-1) + ')'" & vbCrLf
sConsulta = sConsulta & "               ELSE" & vbCrLf
sConsulta = sConsulta & "                   SET @SQL = LEFT(@SQL,LEN(@SQL)-1) + ' WHERE FORM_INSTANCIA=@INSTANCIA_L AND DESGLOSE=@CAMPO_PADRE_DESGLOSE AND LINEA=@LINEA '" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "               SET @SQL=N'IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = ''dbo'' AND  TABLE_NAME='''+@IDIOMA+N'_'+@TABLA+N'_DESGLOSE'')) BEGIN '+@SQL+N' END'" & vbCrLf
sConsulta = sConsulta & "               EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA_L INT,@CAMPO_PADRE_DESGLOSE INT,@LINEA INT', @INSTANCIA_L=@INSTANCIA_L,@CAMPO_PADRE_DESGLOSE=@CAMPO_PADRE_DESGLOSE,@LINEA=@LINEA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "              DELETE FROM #T_VALORES_NUM" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "           SET @INSERT=@INSERT_ORIGINAL" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       FETCH NEXT FROM curIDIOMAS INTO @IDIOMA" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "   CLOSE curIDIOMAS" & vbCrLf
sConsulta = sConsulta & "   DEALLOCATE curIDIOMAS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   DROP TABLE #G1" & vbCrLf
sConsulta = sConsulta & "   DROP TABLE #T_VALORES_NUM" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_IDENTIFICAR_ROL]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_IDENTIFICAR_ROL]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_IDENTIFICAR_ROL]" & vbCrLf
sConsulta = sConsulta & "   @USU VARCHAR(50)," & vbCrLf
sConsulta = sConsulta & "   @PROVE VARCHAR(50)," & vbCrLf
sConsulta = sConsulta & "   @INSTANCIA INT," & vbCrLf
sConsulta = sConsulta & "   @ROL INT OUTPUT," & vbCrLf
sConsulta = sConsulta & "   @BLOQUE INT OUTPUT," & vbCrLf
sConsulta = sConsulta & "   @ENBLOQUE_ACT TINYINT OUTPUT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   DECLARE @SQL AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @ROL=NULL" & vbCrLf
sConsulta = sConsulta & "   SET @BLOQUE=NULL" & vbCrLf
sConsulta = sConsulta & "   SET @ENBLOQUE_ACT=1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   --1? Comprueba si el usuario o prove es un rol de un bloque que est? activo:" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=N'SELECT TOP 1 @ROL=INSTANCIA_ROL.ROL,@BLOQUE=PM_COPIA_ROL_BLOQUE.BLOQUE '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL+N'FROM INSTANCIA_BLOQUE IB WITH(NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL+N'INNER JOIN PM_COPIA_ROL_BLOQUE WITH(NOLOCK) ON IB.BLOQUE=PM_COPIA_ROL_BLOQUE.BLOQUE '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL+N'INNER JOIN INSTANCIA_ROL WITH(NOLOCK) ON INSTANCIA_ROL.ROL=PM_COPIA_ROL_BLOQUE.ROL AND INSTANCIA_ROL.INSTANCIA=IB.INSTANCIA '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @USU IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'AND (INSTANCIA_ROL.PER=@USU OR INSTANCIA_ROL.PER IN (SELECT U.PER FROM USU U WITH(NOLOCK) WHERE U.FSWS_SUSTITUCION=@USU)) '" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'AND INSTANCIA_ROL.PROVE=@PROVE '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL+N'WHERE IB.INSTANCIA=@INSTANCIA AND IB.ESTADO=1 AND IB.BLOQ<>1 AND IB.TRASLADADA=0'" & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA INT,@USU VARCHAR(50),@PROVE VARCHAR(50),@ROL INT OUTPUT,@BLOQUE INT OUTPUT',@INSTANCIA=@INSTANCIA,@USU=@USU,@PROVE=@PROVE,@ROL=@ROL OUTPUT,@BLOQUE=@BLOQUE OUTPUT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @ROL IS NULL AND @USU IS NOT NULL --No est? asignado al rol, comprueba si esta en la posible lista de asignados (COMO_ASIGNAR=3)" & vbCrLf
sConsulta = sConsulta & "   BEGIN       " & vbCrLf
sConsulta = sConsulta & "       SET @SQL=N'SELECT TOP 1 @ROL=R.ID,@BLOQUE=RB.BLOQUE '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'FROM INSTANCIA_BLOQUE IB WITH(NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'INNER JOIN PM_COPIA_ROL_BLOQUE RB WITH(NOLOCK) ON IB.BLOQUE=RB.BLOQUE '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'INNER JOIN PM_COPIA_ROL R WITH(NOLOCK) ON R.ID=RB.ROL AND (R.COMO_ASIGNAR=3 OR R.COMO_ASIGNAR=4) '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'INNER JOIN INSTANCIA_ROL IR WITH(NOLOCK) ON IR.ROL=R.ID AND IR.INSTANCIA=IB.INSTANCIA AND IR.PER IS NULL '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'INNER JOIN INSTANCIA_PARTICIPANTES IP WITH(NOLOCK) ON IP.ROL=IR.ROL AND IP.INSTANCIA=IR.INSTANCIA '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'AND (IP.PER=@USU OR IP.PER IN (SELECT U.PER FROM USU U WITH(NOLOCK) WHERE U.FSWS_SUSTITUCION=@USU)) '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'WHERE IB.INSTANCIA=@INSTANCIA AND IB.ESTADO=1 AND IB.BLOQ<>1 AND IB.TRASLADADA=0 '" & vbCrLf
sConsulta = sConsulta & "       EXEC SP_EXECUTESQL @SQL,N'@INSTANCIA INT,@USU VARCHAR(50),@PROVE VARCHAR(50),@ROL INT OUTPUT,@BLOQUE INT OUTPUT',@INSTANCIA=@INSTANCIA,@USU=@USU,@PROVE=@PROVE,@ROL=@ROL OUTPUT,@BLOQUE=@BLOQUE OUTPUT" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @ROL IS NULL --No es un rol de un bloque activo." & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       --No es un rol activo, ser? un usuario al que se le ha trasladado o devuelto la solicitud??:" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=N'SELECT TOP 1 @ROL=PM_COPIA_ROL.ID,@BLOQUE=PM_COPIA_ROL_BLOQUE.BLOQUE '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'FROM INSTANCIA_BLOQUE IB WITH (NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'INNER JOIN PM_COPIA_ROL_BLOQUE WITH (NOLOCK) ON IB.BLOQUE=PM_COPIA_ROL_BLOQUE.BLOQUE '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'INNER JOIN PM_COPIA_ROL WITH (NOLOCK) ON PM_COPIA_ROL_BLOQUE.ROL=PM_COPIA_ROL.ID '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'INNER JOIN(SELECT MAX(ID) ID ,INSTANCIA,BLOQUE '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'FROM INSTANCIA_EST  WITH (NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'WHERE DESTINATARIO IS NOT NULL OR DESTINATARIO_PROV IS NOT NULL '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'GROUP BY INSTANCIA,BLOQUE) T ON IB.INSTANCIA=T.INSTANCIA AND IB.ID=T.BLOQUE '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'INNER JOIN INSTANCIA_EST IE WITH (NOLOCK) ON T.ID=IE.ID '" & vbCrLf
sConsulta = sConsulta & "       IF @USU IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "           SET @SQL=@SQL+N'AND (IE.DESTINATARIO=@USU OR IE.DESTINATARIO IN (SELECT U.PER FROM USU U WITH(NOLOCK) WHERE U.FSWS_SUSTITUCION=@USU))'" & vbCrLf
sConsulta = sConsulta & "       ELSE" & vbCrLf
sConsulta = sConsulta & "           SET @SQL=@SQL+N'AND IE.DESTINATARIO_PROV=@PROVE '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'WHERE IB.INSTANCIA=@INSTANCIA AND IB.ESTADO=1 AND IB.BLOQ<>1 AND IB.TRASLADADA=1 '" & vbCrLf
sConsulta = sConsulta & "       EXEC SP_EXECUTESQL @SQL,N'@INSTANCIA INT,@USU VARCHAR(50),@PROVE VARCHAR(50),@ROL INT OUTPUT,@BLOQUE INT OUTPUT',@INSTANCIA=@INSTANCIA,@USU=@USU,@PROVE=@PROVE,@ROL=@ROL OUTPUT,@BLOQUE=@BLOQUE OUTPUT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       IF @ROL IS NULL  --Comprueba si es un rol de cualquier bloque:" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           SET @ENBLOQUE_ACT=0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "           SET @SQL=N'SELECT TOP 1 @ROL=PM_COPIA_ROL.ID,@BLOQUE=PM_COPIA_ROL_BLOQUE.BLOQUE '" & vbCrLf
sConsulta = sConsulta & "           SET @SQL=@SQL+N'FROM INSTANCIA_BLOQUE IB WITH(NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "           SET @SQL=@SQL+N'INNER JOIN PM_COPIA_ROL_BLOQUE WITH(NOLOCK)  ON IB.BLOQUE=PM_COPIA_ROL_BLOQUE.BLOQUE '" & vbCrLf
sConsulta = sConsulta & "           SET @SQL=@SQL+N'INNER JOIN PM_COPIA_ROL WITH(NOLOCK) ON PM_COPIA_ROL_BLOQUE.ROL=PM_COPIA_ROL.ID '" & vbCrLf
sConsulta = sConsulta & "           SET @SQL=@SQL+N'INNER JOIN INSTANCIA_ROL IR WITH(NOLOCK) ON IR.ROL=PM_COPIA_ROL.ID AND IR.INSTANCIA=IB.INSTANCIA '" & vbCrLf
sConsulta = sConsulta & "           SET @SQL=@SQL+N'WHERE IB.INSTANCIA=@INSTANCIA '" & vbCrLf
sConsulta = sConsulta & "           IF @USU IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "               SET @SQL=@SQL+N'AND IR.PER=@USU '" & vbCrLf
sConsulta = sConsulta & "           ELSE" & vbCrLf
sConsulta = sConsulta & "               SET @SQL=@SQL+N'AND IR.PROVE=@PROVE '" & vbCrLf
sConsulta = sConsulta & "           SET @SQL=@SQL+N'ORDER BY IB.ID DESC '" & vbCrLf
sConsulta = sConsulta & "           EXEC SP_EXECUTESQL @SQL,N'@INSTANCIA INT,@USU VARCHAR(50),@PROVE VARCHAR(50),@ROL INT OUTPUT,@BLOQUE INT OUTPUT',@INSTANCIA=@INSTANCIA,@USU=@USU,@PROVE=@PROVE,@ROL=@ROL OUTPUT,@BLOQUE=@BLOQUE OUTPUT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "           IF @ROL IS NULL  --No es el usuario activo de un traslado o devoluci?n,ser? uno intermedio o uno que intervino en alg?n momento." & vbCrLf
sConsulta = sConsulta & "           BEGIN" & vbCrLf
sConsulta = sConsulta & "               SET @SQL=N'SELECT TOP 1 @ROL=PM_COPIA_ROL.ID,@BLOQUE=PM_COPIA_ROL_BLOQUE.BLOQUE '" & vbCrLf
sConsulta = sConsulta & "               SET @SQL=@SQL+N'FROM INSTANCIA_BLOQUE IB WITH(NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "               SET @SQL=@SQL+N'INNER JOIN PM_COPIA_ROL_BLOQUE WITH(NOLOCK) ON IB.BLOQUE=PM_COPIA_ROL_BLOQUE.BLOQUE '" & vbCrLf
sConsulta = sConsulta & "               SET @SQL=@SQL+N'INNER JOIN PM_COPIA_ROL WITH(NOLOCK) ON PM_COPIA_ROL_BLOQUE.ROL=PM_COPIA_ROL.ID '" & vbCrLf
sConsulta = sConsulta & "               SET @SQL=@SQL+N'INNER JOIN (SELECT MAX(ID) ID,INSTANCIA,BLOQUE,ROL '" & vbCrLf
sConsulta = sConsulta & "               SET @SQL=@SQL+N'FROM INSTANCIA_EST WITH (NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "               IF @USU IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                   SET @SQL=@SQL+N'WHERE (DESTINATARIO=@USU OR DESTINATARIO IN (SELECT U.PER FROM USU U WITH(NOLOCK) WHERE U.FSWS_SUSTITUCION=@USU)) '" & vbCrLf
sConsulta = sConsulta & "               ELSE" & vbCrLf
sConsulta = sConsulta & "                   SET @SQL=@SQL+N'WHERE DESTINATARIO_PROV=@PROVE '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "               SET @SQL=@SQL+N'GROUP BY INSTANCIA,BLOQUE,ROL) T ON IB.INSTANCIA=T.INSTANCIA AND IB.ID=T.BLOQUE AND T.ROL=PM_COPIA_ROL.ID '" & vbCrLf
sConsulta = sConsulta & "               SET @SQL=@SQL+N'WHERE IB.INSTANCIA=@INSTANCIA '" & vbCrLf
sConsulta = sConsulta & "               SET @SQL=@SQL+N'ORDER BY IB.ID DESC '" & vbCrLf
sConsulta = sConsulta & "               EXEC SP_EXECUTESQL @SQL,N'@INSTANCIA INT,@USU VARCHAR(50),@PROVE VARCHAR(50),@ROL INT OUTPUT,@BLOQUE INT OUTPUT',@INSTANCIA=@INSTANCIA,@USU=@USU,@PROVE=@PROVE,@ROL=@ROL OUTPUT,@BLOQUE=@BLOQUE OUTPUT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "               IF @ROL IS NULL AND @USU IS NOT NULL  --es un rol del flujo cuya persona ha sido sustitu?da por @USU" & vbCrLf
sConsulta = sConsulta & "               BEGIN" & vbCrLf
sConsulta = sConsulta & "                   SET @SQL=N'SELECT TOP 1 @ROL=PM_COPIA_ROL.ID,@BLOQUE=PM_COPIA_ROL_BLOQUE.BLOQUE '" & vbCrLf
sConsulta = sConsulta & "                   SET @SQL=@SQL+N'FROM INSTANCIA_BLOQUE IB WITH (NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "                   SET @SQL=@SQL+N'INNER JOIN PM_COPIA_ROL_BLOQUE WITH (NOLOCK) ON IB.BLOQUE=PM_COPIA_ROL_BLOQUE.BLOQUE '" & vbCrLf
sConsulta = sConsulta & "                   SET @SQL=@SQL+N'INNER JOIN PM_COPIA_ROL WITH (NOLOCK) ON PM_COPIA_ROL_BLOQUE.ROL=PM_COPIA_ROL.ID '" & vbCrLf
sConsulta = sConsulta & "                   SET @SQL=@SQL+N'INNER JOIN INSTANCIA_ROL IR WITH(NOLOCK) ON IR.ROL=PM_COPIA_ROL.ID AND IR.INSTANCIA=IB.INSTANCIA '" & vbCrLf
sConsulta = sConsulta & "                   SET @SQL=@SQL+N'AND (IR.PER_ORIGINAL=@USU AND NOT IR.PER=PM_COPIA_ROL.PER_ORIGINAL) '" & vbCrLf
sConsulta = sConsulta & "                   SET @SQL=@SQL+N'WHERE IB.INSTANCIA=@INSTANCIA '" & vbCrLf
sConsulta = sConsulta & "                   SET @SQL=@SQL+N'ORDER BY IB.ID DESC '" & vbCrLf
sConsulta = sConsulta & "                   EXEC SP_EXECUTESQL @SQL,N'@INSTANCIA INT,@USU VARCHAR(50),@PROVE VARCHAR(50),@ROL INT OUTPUT,@BLOQUE INT OUTPUT',@INSTANCIA=@INSTANCIA,@USU=@USU,@PROVE=@PROVE,@ROL=@ROL OUTPUT,@BLOQUE=@BLOQUE OUTPUT" & vbCrLf
sConsulta = sConsulta & "               END" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSN_RECALCULAR_PRECIO_ITEM]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSN_RECALCULAR_PRECIO_ITEM]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSN_RECALCULAR_PRECIO_ITEM]" & vbCrLf
sConsulta = sConsulta & "@ANYO AS INT," & vbCrLf
sConsulta = sConsulta & "@GMN AS VARCHAR(10)," & vbCrLf
sConsulta = sConsulta & "@PROCE AS INT," & vbCrLf
sConsulta = sConsulta & "@ITEM AS INT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--DECLARE @ANYO AS INT = 2015" & vbCrLf
sConsulta = sConsulta & "--DECLARE @GMN AS VARCHAR(10) = 'CGE'" & vbCrLf
sConsulta = sConsulta & "--DECLARE @PROCE AS INT = 96" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--update  NO_CALC_PRES_UNI= 1 FROM PROCE_DEF WITH(NOLOCK) WHERE ANYO=@ANYO AND GMN1 = @GMN AND PROCE = @PROCE " & vbCrLf
sConsulta = sConsulta & "DECLARE @NO_ACTUALIZAR AS BIT " & vbCrLf
sConsulta = sConsulta & "DECLARE @MODO AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @MODO_NUM AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRECIO_ITEM AS FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @REGISTROS AS INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @MODO = CAL_LIN_BASE, @MODO_NUM = NUM_MEJ_OFE from PARGEN_GEST WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "if @MODO =1 OR @MODO = 2" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   ---SE MIRA SI EL PROCESO EST� CONFIGURADO PARA ACTUALIZAR LOS PRECIOS" & vbCrLf
sConsulta = sConsulta & "   SELECT @NO_ACTUALIZAR = NO_CALC_PRES_UNI FROM PROCE_DEF WITH(NOLOCK) WHERE ANYO=@ANYO AND GMN1 = @GMN AND PROCE = @PROCE " & vbCrLf
sConsulta = sConsulta & "   IF @NO_ACTUALIZAR = 0" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       SET XACT_ABORT ON" & vbCrLf
sConsulta = sConsulta & "       BEGIN TRANSACTION " & vbCrLf
sConsulta = sConsulta & "       --REINICIAMOS EL VALOR DEL CAMPO ALTER TABLE ITEM_OFE ADD MARCA_CALCULO_ITEMPREC INT NOT NULL CONSTRAINT DF_ITEM_OFE_MARCA_CALCULO_ITEMPREC DEFAULT 0 DE ITEM_OFE PARA MARCARLO DESPUES" & vbCrLf
sConsulta = sConsulta & "       UPDATE ITEM_OFE SET MARCA_CALCULO_ITEMPREC =0 WHERE ANYO=@ANYO AND GMN1 =@GMN AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "       --ABRIMOS UN BUCLE CON TODOS LOS ITEMS DEL PROCESO PARA PODER CALCULAR EL PRECIO EN FUNCI�N DE LA CONFIGURACI�N ESTABLECIDA" & vbCrLf
sConsulta = sConsulta & "       IF @ITEM = 0 " & vbCrLf
sConsulta = sConsulta & "           DECLARE Cur_ITEM_PROCESO Cursor FOR SELECT I.ID FROM ITEM I WITH(NOLOCK) WHERE I.ANYO = @ANYO AND I.GMN1_PROCE = @GMN AND I.PROCE = @PROCE " & vbCrLf
sConsulta = sConsulta & "       ELSE" & vbCrLf
sConsulta = sConsulta & "           DECLARE Cur_ITEM_PROCESO Cursor FOR SELECT I.ID FROM ITEM I WITH(NOLOCK) WHERE I.ANYO = @ANYO AND I.GMN1_PROCE = @GMN AND I.PROCE = @PROCE AND I.ID = @ITEM " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       OPEN Cur_ITEM_PROCESO" & vbCrLf
sConsulta = sConsulta & "       FETCH NEXT FROM Cur_ITEM_PROCESO into @ITEM" & vbCrLf
sConsulta = sConsulta & "       WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "       --SE CREA UNA TEMPORAL PARA TRABAJAR" & vbCrLf
sConsulta = sConsulta & "           if object_id('tempdb..#TempPreciosOfertas') IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "               DROP TABLE #TempPreciosOfertas" & vbCrLf
sConsulta = sConsulta & "           CREATE TABLE #TempPreciosOfertas  " & vbCrLf
sConsulta = sConsulta & "           (  " & vbCrLf
sConsulta = sConsulta & "               ID [int] IDENTITY (1,1) not null," & vbCrLf
sConsulta = sConsulta & "               PRECIO  [float]   NOT NULL ," & vbCrLf
sConsulta = sConsulta & "               ANYO [INT] NOT NULL," & vbCrLf
sConsulta = sConsulta & "               GMN1 [Nvarchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL," & vbCrLf
sConsulta = sConsulta & "               PROCE [INT] NOT NULL," & vbCrLf
sConsulta = sConsulta & "               ITEM [INT] NOT NULL," & vbCrLf
sConsulta = sConsulta & "               OFE [int] NOT NULL," & vbCrLf
sConsulta = sConsulta & "               PROVE [NVARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL," & vbCrLf
sConsulta = sConsulta & "           );" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "           --MEDIANA DE LAS PRIMERAS OFERTAS" & vbCrLf
sConsulta = sConsulta & "           IF @MODO = 1" & vbCrLf
sConsulta = sConsulta & "               BEGIN" & vbCrLf
sConsulta = sConsulta & "                   insert into #TempPreciosOfertas SELECT IO.PRECIO / PO.CAMBIO, IO.ANYO, IO.GMN1,IO.PROCE,IO.ITEM, IO.NUM,IO.PROVE FROM ITEM_OFE IO WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                               INNER JOIN PROCE_OFE PO WITH(NOLOCK) ON IO.ANYO = PO.ANYO AND IO.GMN1 =PO.GMN1 AND IO.PROCE= PO.PROCE AND PO.PROVE = IO.PROVE AND IO.NUM = PO.OFE" & vbCrLf
sConsulta = sConsulta & "                               INNER JOIN OFEEST OE WITH(NOLOCK) ON OE.COD = PO.EST AND OE.COMP = 1 AND OE.ADJ = 1" & vbCrLf
sConsulta = sConsulta & "                               WHERE IO.ANYO=@ANYO AND IO.GMN1 = @GMN AND IO.PROCE =@PROCE AND IO.ITEM=@ITEM " & vbCrLf
sConsulta = sConsulta & "                                   AND IO.NUM = (SELECT  MIN(PO.OFE) PRIMERA_OFERTA FROM  PROCE_OFE PO WITH(NOLOCK)  " & vbCrLf
sConsulta = sConsulta & "                                               INNER JOIN OFEEST OE WITH(NOLOCK) ON OE.COD = PO.EST AND OE.COMP = 1 AND OE.ADJ = 1" & vbCrLf
sConsulta = sConsulta & "                                               WHERE PO.ANYO=@ANYO AND PO.GMN1 = @GMN AND PO.PROCE =@PROCE AND PROVE = IO.PROVE AND IO.PRECIO IS NOT NULL  GROUP BY  PO.PROVE) ORDER BY IO.PRECIO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                   SELECT @REGISTROS= COUNT(*) FROM #TempPreciosOfertas" & vbCrLf
sConsulta = sConsulta & "                   SELECT @PRECIO_ITEM=AVG(precio) from #TempPreciosOfertas T1 with(nolock) " & vbCrLf
sConsulta = sConsulta & "                   INNER JOIN (SELECT AVG (id) num FROM #TempPreciosOfertas" & vbCrLf
sConsulta = sConsulta & "                               UNION " & vbCrLf
sConsulta = sConsulta & "                               SELECT id num FROM #TempPreciosOfertas WHERE ID = ((@REGISTROS/2)+1) AND @REGISTROS % 2 = 0) T2 on T2.num = t1.id" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                   --MARCAMOS LOS REGISTROS DE ITEM:_OFE CON LOS QUE HEMOS REALIZADO EL C�LCULO" & vbCrLf
sConsulta = sConsulta & "                   UPDATE ITEM_OFE SET MARCA_CALCULO_ITEMPREC = 1 FROM ITEM_OFE IO WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                   INNER JOIN #TempPreciosOfertas T1 with(nolock) ON T1.ANYO =IO.ANYO AND T1.GMN1 = IO.GMN1 AND T1.PROCE= IO.PROCE AND T1.ITEM = IO.ITEM AND T1.OFE =IO.NUM AND T1.PROVE =IO.PROVE" & vbCrLf
sConsulta = sConsulta & "                   INNER JOIN (SELECT AVG (id) num FROM #TempPreciosOfertas" & vbCrLf
sConsulta = sConsulta & "                               UNION " & vbCrLf
sConsulta = sConsulta & "                               SELECT id num FROM #TempPreciosOfertas WHERE ID = ((@REGISTROS/2)+1) AND @REGISTROS % 2 = 0) T2 on T2.num = t1.id" & vbCrLf
sConsulta = sConsulta & "               END" & vbCrLf
sConsulta = sConsulta & "           ELSE" & vbCrLf
sConsulta = sConsulta & "           --MEDIA DEL N�MERO DE OFERTAS CONFIGURADAS" & vbCrLf
sConsulta = sConsulta & "           IF @MODO = 2" & vbCrLf
sConsulta = sConsulta & "               BEGIN" & vbCrLf
sConsulta = sConsulta & "                   --En caso de que est� configurado a 0 se coger� la media de todos los items" & vbCrLf
sConsulta = sConsulta & "                   --Si no es 0 se har� la media del n�mero configurado recogido en la variable @MODO_NUM" & vbCrLf
sConsulta = sConsulta & "                   IF @MODO_NUM = 0" & vbCrLf
sConsulta = sConsulta & "                       BEGIN" & vbCrLf
sConsulta = sConsulta & "                           INSERT INTO #TempPreciosOfertas SELECT IO.PRECIO / PO.CAMBIO, IO.ANYO, IO.GMN1,IO.PROCE,IO.ITEM, IO.NUM,IO.PROVE FROM ITEM_OFE IO WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                               INNER JOIN ITEM I WITH(NOLOCK) ON I.ANYO = IO.ANYO AND I.GMN1_PROCE =IO.GMN1 AND I.PROCE = IO.PROCE AND I.ID = IO.ITEM " & vbCrLf
sConsulta = sConsulta & "                               INNER JOIN PROCE_OFE PO WITH(NOLOCK) ON PO.ANYO = I.ANYO AND PO.GMN1 =I.GMN1_PROCE AND PO.PROCE =I.PROCE AND PO.PROVE = IO.PROVE AND IO.NUM = PO.OFE" & vbCrLf
sConsulta = sConsulta & "                               INNER JOIN OFEEST OE WITH(NOLOCK) ON OE.COD = PO.EST AND OE.COMP = 1 AND OE.ADJ = 1" & vbCrLf
sConsulta = sConsulta & "                               WHERE I.ANYO = @ANYO AND I.GMN1_PROCE = @GMN AND I.PROCE = @PROCE AND I.ID = @ITEM AND IO.PRECIO IS NOT NULL ORDER BY IO.PRECIO" & vbCrLf
sConsulta = sConsulta & "                       END" & vbCrLf
sConsulta = sConsulta & "                   ELSE" & vbCrLf
sConsulta = sConsulta & "                       BEGIN" & vbCrLf
sConsulta = sConsulta & "                           INSERT INTO #TempPreciosOfertas SELECT TOP (@MODO_NUM) IO.PRECIO / PO.CAMBIO, IO.ANYO, IO.GMN1,IO.PROCE,IO.ITEM, IO.NUM,IO.PROVE FROM ITEM_OFE IO WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                               INNER JOIN ITEM I WITH(NOLOCK) ON I.ANYO = IO.ANYO AND I.GMN1_PROCE =IO.GMN1 AND I.PROCE = IO.PROCE AND I.ID = IO.ITEM " & vbCrLf
sConsulta = sConsulta & "                               INNER JOIN PROCE_OFE PO WITH(NOLOCK) ON PO.ANYO = I.ANYO AND PO.GMN1 =I.GMN1_PROCE AND PO.PROCE =I.PROCE AND PO.PROVE = IO.PROVE AND IO.NUM = PO.OFE" & vbCrLf
sConsulta = sConsulta & "                               INNER JOIN OFEEST OE WITH(NOLOCK) ON OE.COD = PO.EST AND OE.COMP = 1 AND OE.ADJ = 1" & vbCrLf
sConsulta = sConsulta & "                               WHERE I.ANYO = @ANYO AND I.GMN1_PROCE = @GMN AND I.PROCE = @PROCE AND I.ID = @ITEM AND IO.PRECIO IS NOT NULL ORDER BY IO.PRECIO" & vbCrLf
sConsulta = sConsulta & "                       END" & vbCrLf
sConsulta = sConsulta & "                   --CALCULAMOS EL PRECIO EN BASE A LA TABLA TEMPORAL " & vbCrLf
sConsulta = sConsulta & "                   SELECT @PRECIO_ITEM=(SUM(PRECIO)/COUNT(PRECIO)) from (SELECT IO.PRECIO , IO.ITEM FROM #TempPreciosOfertas IO WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                       INNER JOIN ITEM I WITH(NOLOCK) ON I.ANYO = IO.ANYO AND I.GMN1_PROCE =IO.GMN1 AND I.PROCE = IO.PROCE AND I.ID = IO.ITEM " & vbCrLf
sConsulta = sConsulta & "                       INNER JOIN PROCE_OFE PO WITH(NOLOCK) ON PO.ANYO = I.ANYO AND PO.GMN1 =I.GMN1_PROCE AND PO.PROCE =I.PROCE AND PO.PROVE = IO.PROVE AND IO.OFE = PO.OFE" & vbCrLf
sConsulta = sConsulta & "                       INNER JOIN OFEEST OE WITH(NOLOCK) ON OE.COD = PO.EST AND OE.COMP = 1 AND OE.ADJ = 1" & vbCrLf
sConsulta = sConsulta & "                       WHERE I.ANYO = @ANYO AND I.GMN1_PROCE = @GMN AND I.PROCE = @PROCE AND I.ID = @ITEM ) A GROUP BY ITEM " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                   --MARCAMOS LOS REGISTROS DE ITEM:_OFE CON LOS QUE HEMOS REALIZADO EL C�LCULO" & vbCrLf
sConsulta = sConsulta & "                   UPDATE ITEM_OFE SET MARCA_CALCULO_ITEMPREC = 1 FROM ITEM_OFE IO WITH(NOLOCK) " & vbCrLf
sConsulta = sConsulta & "                       INNER JOIN #TempPreciosOfertas T1 WITH(NOLOCK) ON T1.ANYO =IO.ANYO AND T1.GMN1 = IO.GMN1 AND T1.PROCE= IO.PROCE AND T1.ITEM = IO.ITEM AND T1.OFE =IO.NUM AND T1.PROVE =IO.PROVE" & vbCrLf
sConsulta = sConsulta & "               END" & vbCrLf
sConsulta = sConsulta & "               --Actualizamos los precios del item" & vbCrLf
sConsulta = sConsulta & "               SELECT @REGISTROS= COUNT(*) FROM #TempPreciosOfertas" & vbCrLf
sConsulta = sConsulta & "                if @REGISTROS > 0" & vbCrLf
sConsulta = sConsulta & "                   UPDATE ITEM SET PREC = @PRECIO_ITEM, PRES = @PRECIO_ITEM * CANT WHERE ANYO = @ANYO AND GMN1_PROCE = @GMN AND PROCE = @PROCE AND ID = @ITEM" & vbCrLf
sConsulta = sConsulta & "               DROP TABLE #TempPreciosOfertas" & vbCrLf
sConsulta = sConsulta & "           FETCH NEXT FROM Cur_ITEM_PROCESO INTO @ITEM" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "       Close Cur_ITEM_PROCESO" & vbCrLf
sConsulta = sConsulta & "       DEALLOCATE Cur_ITEM_PROCESO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       COMMIT TRANSACTION" & vbCrLf
sConsulta = sConsulta & "       SET XACT_ABORT OFF" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion3_3_3504_A3_3_3505() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim oADOConnection As ADODB.Connection
    Dim sConsulta As String

    On Error GoTo Error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Actualiza Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    frmProgreso.ProgressBar1.Refresh
    V_3_5_Storeds_005

    sConsulta = "UPDATE VERSION SET NUM ='3.00.35.005'" & ", FECHA=CONVERT(datetime,'" & Format(Now, "mm-dd-yyyy hh:mm:ss") & "',110), PRODVER='3.5.1'"
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False

    CodigoDeActualizacion3_3_3504_A3_3_3505 = True
    Exit Function

Error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If

    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    CodigoDeActualizacion3_3_3504_A3_3_3505 = False
End Function

Private Sub V_3_5_Storeds_005()
    Dim sConsulta As String

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSN_RECALCULAR_PRECIO_ITEM]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSN_RECALCULAR_PRECIO_ITEM]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSN_RECALCULAR_PRECIO_ITEM]" & vbCrLf
sConsulta = sConsulta & "@ANYO AS INT," & vbCrLf
sConsulta = sConsulta & "@GMN AS VARCHAR(10)," & vbCrLf
sConsulta = sConsulta & "@PROCE AS INT," & vbCrLf
sConsulta = sConsulta & "@ITEM AS INT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--DECLARE @ANYO AS INT = 2015" & vbCrLf
sConsulta = sConsulta & "--DECLARE @GMN AS VARCHAR(10) = 'CGE'" & vbCrLf
sConsulta = sConsulta & "--DECLARE @PROCE AS INT = 96" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--update  NO_CALC_PRES_UNI= 1 FROM PROCE_DEF WITH(NOLOCK) WHERE ANYO=@ANYO AND GMN1 = @GMN AND PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "DECLARE @NO_ACTUALIZAR AS BIT" & vbCrLf
sConsulta = sConsulta & "DECLARE @MODO AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @MODO_NUM AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRECIO_ITEM AS FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @REGISTROS AS INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @MODO = CAL_LIN_BASE, @MODO_NUM = NUM_MEJ_OFE from PARGEN_GEST WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "if @MODO =1 OR @MODO = 2" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   ---SE MIRA SI EL PROCESO EST? CONFIGURADO PARA ACTUALIZAR LOS PRECIOS" & vbCrLf
sConsulta = sConsulta & "   SELECT @NO_ACTUALIZAR = NO_CALC_PRES_UNI FROM PROCE_DEF WITH(NOLOCK) WHERE ANYO=@ANYO AND GMN1 = @GMN AND PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "   IF @NO_ACTUALIZAR = 0" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       SET XACT_ABORT ON" & vbCrLf
sConsulta = sConsulta & "       BEGIN TRANSACTION" & vbCrLf
sConsulta = sConsulta & "       --REINICIAMOS EL VALOR DEL CAMPO ALTER TABLE ITEM_OFE ADD MARCA_CALCULO_ITEMPREC INT NOT NULL CONSTRAINT DF_ITEM_OFE_MARCA_CALCULO_ITEMPREC DEFAULT 0 DE ITEM_OFE PARA MARCARLO DESPUES" & vbCrLf
sConsulta = sConsulta & "       UPDATE ITEM_OFE SET MARCA_CALCULO_ITEMPREC =0 WHERE ANYO=@ANYO AND GMN1 =@GMN AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "       --ABRIMOS UN BUCLE CON TODOS LOS ITEMS DEL PROCESO PARA PODER CALCULAR EL PRECIO EN FUNCI?N DE LA CONFIGURACI?N ESTABLECIDA" & vbCrLf
sConsulta = sConsulta & "       IF @ITEM = 0" & vbCrLf
sConsulta = sConsulta & "           DECLARE Cur_ITEM_PROCESO Cursor FOR SELECT I.ID FROM ITEM I WITH(NOLOCK) WHERE I.ANYO = @ANYO AND I.GMN1_PROCE = @GMN AND I.PROCE = @PROCE" & vbCrLf
sConsulta = sConsulta & "       ELSE" & vbCrLf
sConsulta = sConsulta & "           DECLARE Cur_ITEM_PROCESO Cursor FOR SELECT I.ID FROM ITEM I WITH(NOLOCK) WHERE I.ANYO = @ANYO AND I.GMN1_PROCE = @GMN AND I.PROCE = @PROCE AND I.ID = @ITEM" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       OPEN Cur_ITEM_PROCESO" & vbCrLf
sConsulta = sConsulta & "       FETCH NEXT FROM Cur_ITEM_PROCESO into @ITEM" & vbCrLf
sConsulta = sConsulta & "       WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "       --SE CREA UNA TEMPORAL PARA TRABAJAR" & vbCrLf
sConsulta = sConsulta & "           if object_id('tempdb..#TempPreciosOfertas') IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "               DROP TABLE #TempPreciosOfertas" & vbCrLf
sConsulta = sConsulta & "           CREATE TABLE #TempPreciosOfertas" & vbCrLf
sConsulta = sConsulta & "           (" & vbCrLf
sConsulta = sConsulta & "               ID [int] IDENTITY (1,1) not null," & vbCrLf
sConsulta = sConsulta & "               PRECIO  [float]   NOT NULL ," & vbCrLf
sConsulta = sConsulta & "               ANYO [INT] NOT NULL," & vbCrLf
sConsulta = sConsulta & "               GMN1 [Nvarchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL," & vbCrLf
sConsulta = sConsulta & "               PROCE [INT] NOT NULL," & vbCrLf
sConsulta = sConsulta & "               ITEM [INT] NOT NULL," & vbCrLf
sConsulta = sConsulta & "               OFE [int] NOT NULL," & vbCrLf
sConsulta = sConsulta & "               PROVE [NVARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL," & vbCrLf
sConsulta = sConsulta & "           );" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "           --MEDIANA DE LAS PRIMERAS OFERTAS" & vbCrLf
sConsulta = sConsulta & "           IF @MODO = 1" & vbCrLf
sConsulta = sConsulta & "               BEGIN" & vbCrLf
sConsulta = sConsulta & "                   insert into #TempPreciosOfertas SELECT IO.PRECIO / PO.CAMBIO, IO.ANYO, IO.GMN1,IO.PROCE,IO.ITEM, IO.NUM,IO.PROVE FROM ITEM_OFE IO WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                               INNER JOIN PROCE_OFE PO WITH(NOLOCK) ON IO.ANYO = PO.ANYO AND IO.GMN1 =PO.GMN1 AND IO.PROCE= PO.PROCE AND PO.PROVE = IO.PROVE AND IO.NUM = PO.OFE" & vbCrLf
sConsulta = sConsulta & "                               INNER JOIN OFEEST OE WITH(NOLOCK) ON OE.COD = PO.EST AND OE.COMP = 1 AND OE.ADJ = 1" & vbCrLf
sConsulta = sConsulta & "                               WHERE IO.ANYO=@ANYO AND IO.GMN1 = @GMN AND IO.PROCE =@PROCE AND IO.ITEM=@ITEM" & vbCrLf
sConsulta = sConsulta & "                                   AND IO.NUM = (SELECT  MIN(PO.OFE) PRIMERA_OFERTA FROM  PROCE_OFE PO WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                                               INNER JOIN OFEEST OE WITH(NOLOCK) ON OE.COD = PO.EST AND OE.COMP = 1 AND OE.ADJ = 1" & vbCrLf
sConsulta = sConsulta & "                                               WHERE PO.ANYO=@ANYO AND PO.GMN1 = @GMN AND PO.PROCE =@PROCE AND PROVE = IO.PROVE AND IO.PRECIO IS NOT NULL  GROUP BY  PO.PROVE) ORDER BY IO.PRECIO/ PO.CAMBIO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                   SELECT @REGISTROS= COUNT(*) FROM #TempPreciosOfertas" & vbCrLf
sConsulta = sConsulta & "                   SELECT @PRECIO_ITEM=AVG(precio) from #TempPreciosOfertas T1 with(nolock)" & vbCrLf
sConsulta = sConsulta & "                   INNER JOIN (SELECT AVG (id) num FROM #TempPreciosOfertas" & vbCrLf
sConsulta = sConsulta & "                               UNION" & vbCrLf
sConsulta = sConsulta & "                               SELECT id num FROM #TempPreciosOfertas WHERE ID = ((@REGISTROS/2)+1) AND @REGISTROS % 2 = 0) T2 on T2.num = t1.id" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                   --MARCAMOS LOS REGISTROS DE ITEM:_OFE CON LOS QUE HEMOS REALIZADO EL C?LCULO" & vbCrLf
sConsulta = sConsulta & "                   UPDATE ITEM_OFE SET MARCA_CALCULO_ITEMPREC = 1 FROM ITEM_OFE IO WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                   INNER JOIN #TempPreciosOfertas T1 with(nolock) ON T1.ANYO =IO.ANYO AND T1.GMN1 = IO.GMN1 AND T1.PROCE= IO.PROCE AND T1.ITEM = IO.ITEM AND T1.OFE =IO.NUM AND T1.PROVE =IO.PROVE" & vbCrLf
sConsulta = sConsulta & "                   INNER JOIN (SELECT AVG (id) num FROM #TempPreciosOfertas" & vbCrLf
sConsulta = sConsulta & "                               UNION" & vbCrLf
sConsulta = sConsulta & "                               SELECT id num FROM #TempPreciosOfertas WHERE ID = ((@REGISTROS/2)+1) AND @REGISTROS % 2 = 0) T2 on T2.num = t1.id" & vbCrLf
sConsulta = sConsulta & "               END" & vbCrLf
sConsulta = sConsulta & "           ELSE" & vbCrLf
sConsulta = sConsulta & "           --MEDIA DEL N?MERO DE OFERTAS CONFIGURADAS" & vbCrLf
sConsulta = sConsulta & "           IF @MODO = 2" & vbCrLf
sConsulta = sConsulta & "               BEGIN" & vbCrLf
sConsulta = sConsulta & "                   --En caso de que est? configurado a 0 se coger? la media de todos los items" & vbCrLf
sConsulta = sConsulta & "                   --Si no es 0 se har? la media del n?mero configurado recogido en la variable @MODO_NUM" & vbCrLf
sConsulta = sConsulta & "                   IF @MODO_NUM = 0" & vbCrLf
sConsulta = sConsulta & "                       BEGIN" & vbCrLf
sConsulta = sConsulta & "                           INSERT INTO #TempPreciosOfertas SELECT IO.PRECIO / PO.CAMBIO, IO.ANYO, IO.GMN1,IO.PROCE,IO.ITEM, IO.NUM,IO.PROVE FROM ITEM_OFE IO WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                               INNER JOIN ITEM I WITH(NOLOCK) ON I.ANYO = IO.ANYO AND I.GMN1_PROCE =IO.GMN1 AND I.PROCE = IO.PROCE AND I.ID = IO.ITEM" & vbCrLf
sConsulta = sConsulta & "                               INNER JOIN PROCE_OFE PO WITH(NOLOCK) ON PO.ANYO = I.ANYO AND PO.GMN1 =I.GMN1_PROCE AND PO.PROCE =I.PROCE AND PO.PROVE = IO.PROVE AND IO.NUM = PO.OFE" & vbCrLf
sConsulta = sConsulta & "                               INNER JOIN OFEEST OE WITH(NOLOCK) ON OE.COD = PO.EST AND OE.COMP = 1 AND OE.ADJ = 1" & vbCrLf
sConsulta = sConsulta & "                               WHERE I.ANYO = @ANYO AND I.GMN1_PROCE = @GMN AND I.PROCE = @PROCE AND I.ID = @ITEM AND IO.PRECIO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                               AND IO.NUM = (SELECT  MAX(PO.OFE) PRIMERA_OFERTA FROM  PROCE_OFE PO WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                                               INNER JOIN OFEEST OE WITH(NOLOCK) ON OE.COD = PO.EST AND OE.COMP = 1 AND OE.ADJ = 1" & vbCrLf
sConsulta = sConsulta & "                                               WHERE PO.ANYO=@ANYO AND PO.GMN1 = @GMN AND PO.PROCE =@PROCE AND PROVE = IO.PROVE AND IO.PRECIO IS NOT NULL  GROUP BY  PO.PROVE) ORDER BY IO.PRECIO/ PO.CAMBIO" & vbCrLf
sConsulta = sConsulta & "                       END" & vbCrLf
sConsulta = sConsulta & "                   ELSE" & vbCrLf
sConsulta = sConsulta & "                       BEGIN" & vbCrLf
sConsulta = sConsulta & "                           INSERT INTO #TempPreciosOfertas SELECT TOP (@MODO_NUM) IO.PRECIO / PO.CAMBIO, IO.ANYO, IO.GMN1,IO.PROCE,IO.ITEM, IO.NUM,IO.PROVE FROM ITEM_OFE IO WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                               INNER JOIN ITEM I WITH(NOLOCK) ON I.ANYO = IO.ANYO AND I.GMN1_PROCE =IO.GMN1 AND I.PROCE = IO.PROCE AND I.ID = IO.ITEM" & vbCrLf
sConsulta = sConsulta & "                               INNER JOIN PROCE_OFE PO WITH(NOLOCK) ON PO.ANYO = I.ANYO AND PO.GMN1 =I.GMN1_PROCE AND PO.PROCE =I.PROCE AND PO.PROVE = IO.PROVE AND IO.NUM = PO.OFE" & vbCrLf
sConsulta = sConsulta & "                               INNER JOIN OFEEST OE WITH(NOLOCK) ON OE.COD = PO.EST AND OE.COMP = 1 AND OE.ADJ = 1" & vbCrLf
sConsulta = sConsulta & "                               WHERE I.ANYO = @ANYO AND I.GMN1_PROCE = @GMN AND I.PROCE = @PROCE AND I.ID = @ITEM AND IO.PRECIO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                               AND IO.NUM = (SELECT  MAX(PO.OFE) PRIMERA_OFERTA FROM  PROCE_OFE PO WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                                               INNER JOIN OFEEST OE WITH(NOLOCK) ON OE.COD = PO.EST AND OE.COMP = 1 AND OE.ADJ = 1" & vbCrLf
sConsulta = sConsulta & "                                               WHERE PO.ANYO=@ANYO AND PO.GMN1 = @GMN AND PO.PROCE =@PROCE AND PROVE = IO.PROVE AND IO.PRECIO IS NOT NULL  GROUP BY  PO.PROVE) ORDER BY IO.PRECIO/ PO.CAMBIO" & vbCrLf
sConsulta = sConsulta & "                       END" & vbCrLf
sConsulta = sConsulta & "                   --CALCULAMOS EL PRECIO EN BASE A LA TABLA TEMPORAL" & vbCrLf
sConsulta = sConsulta & "                   SELECT @PRECIO_ITEM=(SUM(PRECIO)/COUNT(PRECIO)) from (SELECT IO.PRECIO , IO.ITEM FROM #TempPreciosOfertas IO WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                       INNER JOIN ITEM I WITH(NOLOCK) ON I.ANYO = IO.ANYO AND I.GMN1_PROCE =IO.GMN1 AND I.PROCE = IO.PROCE AND I.ID = IO.ITEM" & vbCrLf
sConsulta = sConsulta & "                       INNER JOIN PROCE_OFE PO WITH(NOLOCK) ON PO.ANYO = I.ANYO AND PO.GMN1 =I.GMN1_PROCE AND PO.PROCE =I.PROCE AND PO.PROVE = IO.PROVE AND IO.OFE = PO.OFE" & vbCrLf
sConsulta = sConsulta & "                       INNER JOIN OFEEST OE WITH(NOLOCK) ON OE.COD = PO.EST AND OE.COMP = 1 AND OE.ADJ = 1" & vbCrLf
sConsulta = sConsulta & "                       WHERE I.ANYO = @ANYO AND I.GMN1_PROCE = @GMN AND I.PROCE = @PROCE AND I.ID = @ITEM ) A GROUP BY ITEM" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                   --MARCAMOS LOS REGISTROS DE ITEM:_OFE CON LOS QUE HEMOS REALIZADO EL C?LCULO" & vbCrLf
sConsulta = sConsulta & "                   UPDATE ITEM_OFE SET MARCA_CALCULO_ITEMPREC = 1 FROM ITEM_OFE IO WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                       INNER JOIN #TempPreciosOfertas T1 WITH(NOLOCK) ON T1.ANYO =IO.ANYO AND T1.GMN1 = IO.GMN1 AND T1.PROCE= IO.PROCE AND T1.ITEM = IO.ITEM AND T1.OFE =IO.NUM AND T1.PROVE =IO.PROVE" & vbCrLf
sConsulta = sConsulta & "               END" & vbCrLf
sConsulta = sConsulta & "               --Actualizamos los precios del item" & vbCrLf
sConsulta = sConsulta & "               SELECT @REGISTROS= COUNT(*) FROM #TempPreciosOfertas" & vbCrLf
sConsulta = sConsulta & "if @REGISTROS > 0" & vbCrLf
sConsulta = sConsulta & "                   UPDATE ITEM SET PREC = @PRECIO_ITEM, PRES = @PRECIO_ITEM * CANT WHERE ANYO = @ANYO AND GMN1_PROCE = @GMN AND PROCE = @PROCE AND ID = @ITEM" & vbCrLf
sConsulta = sConsulta & "               DROP TABLE #TempPreciosOfertas" & vbCrLf
sConsulta = sConsulta & "           FETCH NEXT FROM Cur_ITEM_PROCESO INTO @ITEM" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "       Close Cur_ITEM_PROCESO" & vbCrLf
sConsulta = sConsulta & "       DEALLOCATE Cur_ITEM_PROCESO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       COMMIT TRANSACTION" & vbCrLf
sConsulta = sConsulta & "       SET XACT_ABORT OFF" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion3_3_3505_A3_3_3506() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim oADOConnection As ADODB.Connection
    Dim sConsulta As String

    On Error GoTo Error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Actualiza Tablas"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    frmProgreso.ProgressBar1.Refresh
    V_3_5_Tablas_006
    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Actualiza Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    frmProgreso.ProgressBar1.Refresh
    V_3_5_Storeds_006

    sConsulta = "UPDATE VERSION SET NUM ='3.00.35.006'" & ", FECHA=CONVERT(datetime,'" & Format(Now, "mm-dd-yyyy hh:mm:ss") & "',110), PRODVER='3.5.1'"
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False

    CodigoDeActualizacion3_3_3505_A3_3_3506 = True
    Exit Function

Error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If

    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    CodigoDeActualizacion3_3_3505_A3_3_3506 = False
End Function

Private Sub V_3_5_Tablas_006()
    Dim sConsulta As String
sConsulta = "IF NOT EXISTS(SELECT object_id,name FROM sys.indexes WHERE object_id = OBJECT_ID('LOG_GRAL') AND name = 'IX_LOG_GRAL_EXP')" & vbCrLf
sConsulta = sConsulta & "CREATE NONCLUSTERED INDEX [IX_LOG_GRAL_EXP]" & vbCrLf
sConsulta = sConsulta & "ON [dbo].[LOG_GRAL] ([ORIGEN])" & vbCrLf
sConsulta = sConsulta & "INCLUDE ([ID],[ID_TABLA],[TABLA],[ESTADO],[FECACT],[ERP])" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF NOT EXISTS(SELECT object_id,name FROM sys.indexes WHERE object_id = OBJECT_ID('LOG_LINEAS_PEDIDO') AND name = 'IX_LOG_LINEAS_PEDIDO_ID_LINEAS_PEDIDO_ID_ID_LOG_ORDEN_ENTREGA')" & vbCrLf
sConsulta = sConsulta & "CREATE INDEX IX_LOG_LINEAS_PEDIDO_ID_LINEAS_PEDIDO_ID_ID_LOG_ORDEN_ENTREGA ON [dbo].[LOG_LINEAS_PEDIDO] ([ID_LINEAS_PEDIDO]) INCLUDE ([ID], [ID_LOG_ORDEN_ENTREGA])" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF NOT EXISTS(SELECT object_id,name FROM sys.indexes WHERE object_id = OBJECT_ID('LOG_LINEAS_PEDIDO') AND name = 'IX_LOG_LINEAS_PEDIDO_ID_LINEAS_PEDIDO')" & vbCrLf
sConsulta = sConsulta & "CREATE INDEX IX_LOG_LINEAS_PEDIDO_ID_LINEAS_PEDIDO ON [dbo].[LOG_LINEAS_PEDIDO] ([ID_LINEAS_PEDIDO])" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF NOT EXISTS(SELECT object_id,name FROM sys.indexes WHERE object_id = OBJECT_ID('LOG_LINEAS_PEDIDO') AND name = 'IX_LOG_LINEAS_PEDIDO_ID_LINEAS_PEDIDO_ACCION')" & vbCrLf
sConsulta = sConsulta & "CREATE INDEX IX_LOG_LINEAS_PEDIDO_ID_LINEAS_PEDIDO_ACCION ON [dbo].[LOG_LINEAS_PEDIDO] ([ID_LINEAS_PEDIDO], [ACCION])" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF NOT EXISTS(SELECT object_id,name FROM sys.indexes WHERE object_id = OBJECT_ID('LOG_LINEAS_PEDIDO') AND name = 'IX_LOG_LINEAS_PEDIDO_ID_LINEAS_PEDIDO_ANYO')" & vbCrLf
sConsulta = sConsulta & "CREATE INDEX IX_LOG_LINEAS_PEDIDO_ID_LINEAS_PEDIDO_ANYO ON [dbo].[LOG_LINEAS_PEDIDO] ([ID_LINEAS_PEDIDO],[ANYO])" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF NOT EXISTS(SELECT object_id,name FROM sys.indexes WHERE object_id = OBJECT_ID('LOG_LINEAS_PEDIDO') AND name = 'IX_LOG_LINEAS_PEDIDO_ID_LINEAS_PEDIDO_ID_LOG_ORDEN_ENTREGA')" & vbCrLf
sConsulta = sConsulta & "CREATE INDEX IX_LOG_LINEAS_PEDIDO_ID_LINEAS_PEDIDO_ID_LOG_ORDEN_ENTREGA ON [dbo].[LOG_LINEAS_PEDIDO] ([ID_LINEAS_PEDIDO],[ID_LOG_ORDEN_ENTREGA])" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

End Sub

Private Sub V_3_5_Storeds_006()
    Dim sConsulta As String

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSINT_RELANZAR_INTEGRACION]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSINT_RELANZAR_INTEGRACION]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSINT_RELANZAR_INTEGRACION]" & vbCrLf
sConsulta = sConsulta & "@IDLOGGRAL INT ," & vbCrLf
sConsulta = sConsulta & "@USUARIO VARCHAR(25) AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "BEGIN TRAN" & vbCrLf
sConsulta = sConsulta & "DECLARE @MYID INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @TABLA INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @LOG INTEGER" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @TABLA=TABLA FROM LOG_GRAL WITH(NOLOCK) WHERE ID=@IDLOGGRAL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @TABLA=1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "--1 MONEDA" & vbCrLf
sConsulta = sConsulta & "INSERT INTO LOG_MON (ACCION,COD,DEN,EQUIV,COD_NEW,ORIGEN,USU,ERP)" & vbCrLf
sConsulta = sConsulta & "SELECT L.ACCION,L.COD,L.DEN,L.EQUIV,L.COD_NEW,L.ORIGEN,@USUARIO USU,L.ERP" & vbCrLf
sConsulta = sConsulta & "FROM LOG_MON L WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN LOG_GRAL WITH (NOLOCK)  on L.ID=log_GRAL.ID_TABLA" & vbCrLf
sConsulta = sConsulta & "WHERE LOG_GRAL.ID =@IDLOGGRAL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @MYID=SCOPE_IDENTITY()" & vbCrLf
sConsulta = sConsulta & "SET @LOG=@@IDENTITY" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "IF @TABLA=2" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "--2 PAIS" & vbCrLf
sConsulta = sConsulta & "INSERT INTO LOG_PAI (ACCION,COD,MON,COD_NEW,ORIGEN,USU,ERP)" & vbCrLf
sConsulta = sConsulta & "SELECT ACCION,COD,L.MON,COD_NEW,L.ORIGEN,@USUARIO USU,L.ERP" & vbCrLf
sConsulta = sConsulta & "FROM LOG_PAI L WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN LOG_GRAL WITH (NOLOCK)  on L.ID=log_GRAL.ID_TABLA" & vbCrLf
sConsulta = sConsulta & "WHERE TABLA=2 AND LOG_GRAL.ID =@IDLOGGRAL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @MYID=SCOPE_IDENTITY()" & vbCrLf
sConsulta = sConsulta & "SET @LOG=@@IDENTITY" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO LOG_PAI_DEN (ID_LOG_PAI,IDI,DEN)" & vbCrLf
sConsulta = sConsulta & "SELECT @MYID,IDI,DEN" & vbCrLf
sConsulta = sConsulta & "FROM LOG_PAI_DEN L WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN LOG_PAI WITH (NOLOCK) ON L.ID_LOG_PAI  = LOG_PAI.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN LOG_GRAL WITH (NOLOCK)  on LOG_PAI.ID=log_GRAL.ID_TABLA" & vbCrLf
sConsulta = sConsulta & "WHERE LOG_GRAL.ID =@IDLOGGRAL" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "IF @TABLA=3" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "--3 PROVINCIAS" & vbCrLf
sConsulta = sConsulta & "INSERT INTO LOG_PROVI (ACCION,PAI,COD,COD_NEW,ORIGEN,USU,ERP)" & vbCrLf
sConsulta = sConsulta & "SELECT ACCION,PAI,COD,COD_NEW,L.ORIGEN,@USUARIO USU,L.ERP" & vbCrLf
sConsulta = sConsulta & "FROM LOG_PROVI L WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN LOG_GRAL WITH (NOLOCK)  on L.ID=log_GRAL.ID_TABLA" & vbCrLf
sConsulta = sConsulta & "WHERE TABLA=3 AND LOG_GRAL.ID =@IDLOGGRAL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @MYID=SCOPE_IDENTITY()" & vbCrLf
sConsulta = sConsulta & "SET @LOG=@@IDENTITY" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO LOG_PROVI_DEN (ID_LOG_PROVI,IDI,DEN)" & vbCrLf
sConsulta = sConsulta & "SELECT @MYID,IDI,DEN" & vbCrLf
sConsulta = sConsulta & "FROM LOG_PROVI_DEN L WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN LOG_GRAL WITH (NOLOCK)  ON L.ID_LOG_PROVI =log_GRAL.ID_TABLA" & vbCrLf
sConsulta = sConsulta & "WHERE LOG_GRAL.ID =@IDLOGGRAL" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "IF @TABLA=4" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "--4 FORMAS DE PAG" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO LOG_PAG (ACCION,COD,DEN,COD_NEW,ORIGEN,USU,ERP)" & vbCrLf
sConsulta = sConsulta & "SELECT ACCION,COD,DEN,COD_NEW,L.ORIGEN,@USUARIO USU,L.ERP" & vbCrLf
sConsulta = sConsulta & "FROM LOG_PAG L WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN LOG_GRAL WITH (NOLOCK)  on L.ID=log_GRAL.ID_TABLA" & vbCrLf
sConsulta = sConsulta & "WHERE TABLA=4 AND LOG_GRAL.ID =@IDLOGGRAL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @MYID=SCOPE_IDENTITY ()" & vbCrLf
sConsulta = sConsulta & "SET @LOG=@@IDENTITY" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO LOG_PAG_DEN (ID_LOG_PAG,IDI,DEN)" & vbCrLf
sConsulta = sConsulta & "SELECT @MYID,IDI,L.DEN" & vbCrLf
sConsulta = sConsulta & "FROM LOG_PAG_DEN L WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN LOG_GRAL WITH (NOLOCK)  on L.ID_LOG_PAG =log_GRAL.ID_TABLA" & vbCrLf
sConsulta = sConsulta & "WHERE LOG_GRAL.ID =@IDLOGGRAL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "IF @TABLA=5" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--5 DESTINOS" & vbCrLf
sConsulta = sConsulta & "INSERT INTO LOG_DEST(ACCION,COD,SINTRANS,DIR,POB,CP,PROVI,PAI,COD_NEW,ORIGEN,USU,DEN_SPA,DEN_ENG,DEN_GER,ERP)" & vbCrLf
sConsulta = sConsulta & "SELECT ACCION,COD,SINTRANS,DIR,POB,CP,PROVI,PAI,COD_NEW,L.ORIGEN,@USUARIO USU,DEN_SPA,DEN_ENG,DEN_GER,L.ERP" & vbCrLf
sConsulta = sConsulta & "FROM LOG_DEST L WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN LOG_GRAL WITH (NOLOCK)  on L.ID=log_GRAL.ID_TABLA" & vbCrLf
sConsulta = sConsulta & "WHERE LOG_GRAL.ID =@IDLOGGRAL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @MYID=SCOPE_IDENTITY()" & vbCrLf
sConsulta = sConsulta & "SET @LOG=@@IDENTITY" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--6 UNI" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "IF @TABLA=6" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "INSERT INTO LOG_UNI(ACCION,COD,DEN,COD_NEW,ORIGEN,USU,ERP,NUMDEC)" & vbCrLf
sConsulta = sConsulta & "SELECT ACCION,COD,DEN,COD_NEW,L.ORIGEN,@USUARIO USU,L.ERP,NUMDEC" & vbCrLf
sConsulta = sConsulta & "FROM LOG_UNI L WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN LOG_GRAL WITH (NOLOCK)  on L.ID=log_GRAL.ID_TABLA" & vbCrLf
sConsulta = sConsulta & "WHERE LOG_GRAL.ID =@IDLOGGRAL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @MYID=SCOPE_IDENTITY ()" & vbCrLf
sConsulta = sConsulta & "SET @LOG=@@IDENTITY" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO LOG_UNI_DEN (ID_LOG_UNI ,IDI,DEN)" & vbCrLf
sConsulta = sConsulta & "SELECT @MYID,IDI,L.DEN" & vbCrLf
sConsulta = sConsulta & "FROM LOG_UNI_DEN L WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN LOG_GRAL WITH (NOLOCK)  on L.ID_LOG_UNI=log_GRAL.ID_TABLA" & vbCrLf
sConsulta = sConsulta & "WHERE LOG_GRAL.ID =@IDLOGGRAL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--7 ARTICULOS" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "IF @TABLA=7" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "INSERT INTO LOG_ART4(ACCION,GMN1,GMN2,GMN3,GMN4,COD,DEN,UNI,CANT,ESP,COD_NEW,ORIGEN,USU,GENERICO,ERP,CONCEPTO,ALMACENAR,RECEPCIONAR,COD_ERP)" & vbCrLf
sConsulta = sConsulta & "select ACCION,GMN1,GMN2,GMN3,GMN4,COD,DEN,UNI,CANT,ESP,COD_NEW,L.ORIGEN,@USUARIO USU,GENERICO,L.ERP,CONCEPTO,ALMACENAR,RECEPCIONAR,L.COD_ERP" & vbCrLf
sConsulta = sConsulta & "from LOG_ART4 L WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN LOG_GRAL WITH (NOLOCK) ON L.ID=log_GRAL.ID_TABLA" & vbCrLf
sConsulta = sConsulta & "WHERE LOG_GRAL.ID =@IDLOGGRAL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @MYID=SCOPE_IDENTITY()" & vbCrLf
sConsulta = sConsulta & "SET @LOG=@@IDENTITY" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO LOG_ART4_UON(ID_LOG_ART4,ACCION,UON1,UON2,UON3)" & vbCrLf
sConsulta = sConsulta & "SELECT @MYID,ACCION,UON1,UON2,UON3 FROM LOG_ART4_UON LAU WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN LOG_GRAL LG WITH(NOLOCK) ON LAU.ID_LOG_ART4=LG.ID_TABLA" & vbCrLf
sConsulta = sConsulta & "WHERE LG.ID=@IDLOGGRAL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--8 PROVEEDORES" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "IF @TABLA=8" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDPROVE INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO LOG_PROVE (ID_LOG_CON,ACCION,COD,DEN,DIR,CP,POB,PAI,PROVI,MON,VAL1,VAL2,VAL3,CAL1,CAL2,CAL3,OBS,NIF,URLPROVE,PAG,PED_FORM,COD_NEW,ORIGEN,USU,EMP,ERP,TIPO,AUTOFACTURA)" & vbCrLf
sConsulta = sConsulta & "select ID_LOG_CON,ACCION,COD,DEN,DIR,CP,POB,PAI,PROVI,MON,VAL1,VAL2,VAL3,CAL1,CAL2,CAL3,OBS,NIF,URLPROVE,PAG,PED_FORM,COD_NEW,L.ORIGEN,@USUARIO USU,EMP,L.ERP,TIPO,AUTOFACTURA" & vbCrLf
sConsulta = sConsulta & "from LOG_PROVE L WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN LOG_GRAL WITH (NOLOCK) ON L.ID=log_GRAL.ID_TABLA" & vbCrLf
sConsulta = sConsulta & "WHERE LOG_GRAL.ID =@IDLOGGRAL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @MYID=SCOPE_IDENTITY()" & vbCrLf
sConsulta = sConsulta & "SET @LOG=@@IDENTITY" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO LOG_PROVE_BANCA(ID_LOG_PROVE,PROVE,PAIS,CLAVE_BANCO,CUENTA,TITULAR,CC)" & vbCrLf
sConsulta = sConsulta & "select @MYID,PROVE,PAIS,CLAVE_BANCO,CUENTA,TITULAR,CC" & vbCrLf
sConsulta = sConsulta & "from LOG_PROVE_BANCA L WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN LOG_GRAL WITH (NOLOCK)  ON L.ID_LOG_PROVE=log_GRAL.ID_TABLA" & vbCrLf
sConsulta = sConsulta & "WHERE LOG_GRAL.ID =@IDLOGGRAL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @IDPROVE=L.ID" & vbCrLf
sConsulta = sConsulta & "FROM LOG_PROVE L WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN LOG_GRAL WITH (NOLOCK)  on L.ID=log_GRAL.ID_TABLA" & vbCrLf
sConsulta = sConsulta & "WHERE LOG_GRAL.ID =@IDLOGGRAL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO LOG_CON (ID_LOG_PROVE,ACCION,PROVE,ID_CON,APE,NOM,DEP,CAR,TFNO,TFNO2,TFNO_MOVIL,FAX,EMAIL,RECPET,APROV,SUBASTA,ORIGEN,USU,CALIDAD,ERP)" & vbCrLf
sConsulta = sConsulta & "select @MYID,L.ACCION,PROVE,ID_CON,APE,NOM,DEP,CAR,TFNO,TFNO2,TFNO_MOVIL,FAX,EMAIL,RECPET,APROV,SUBASTA,L.ORIGEN,@USUARIO USU,CALIDAD,L.ERP" & vbCrLf
sConsulta = sConsulta & "from LOG_CON L WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE L.ID_LOG_PROVE=@IDPROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "IF @TABLA=9" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "--9 ADJ" & vbCrLf
sConsulta = sConsulta & "INSERT INTO LOG_ITEM_ADJ(ACCION,ANYO,GMN1_PROCE,PROCE,ID_ITEM,PROVE,PROVEPADRE,EMPRESA,PROVE_ERP,PROVE_ERP_PADRE,PORCEN,CENTRO,ART,DESCR,DEST,UNI,PAG,CANT,CANT_ADJ,PREC_VALIDO_ADJ,PREC_ADJ,PRES,FECINI,FECFIN,OBJ,FECOBJ,ESP,SOLICIT,LINEA_SOLICIT,GMN1,GMN2,GMN3,GMN4,DEN_PROCE,ADJDIR,PRES_PROCE,FECNEC,FECAPE,FECVAL,FECPRES,FECLIMOFE,FECPROXREU,FECULTREU,REUDEC,NUM_OFE,MON_PROCE,CAMBIO_PROCE,MON_OFE,CAMBIO_OFE,ESP_PROCE,SOLICITUD,USUAPER,PAG_PROCE,FECINI_PROCE,DEST_PROCE,COM,EQP,FECCIERRE,ORIGEN,USU,ERP)" & vbCrLf
sConsulta = sConsulta & "select ACCION,ANYO,GMN1_PROCE,PROCE,ID_ITEM,PROVE,PROVEPADRE,EMPRESA,PROVE_ERP,PROVE_ERP_PADRE,PORCEN,CENTRO,ART,DESCR,DEST,UNI,PAG,CANT,CANT_ADJ,PREC_VALIDO_ADJ,PREC_ADJ,PRES,FECINI,FECFIN,OBJ,FECOBJ,ESP,SOLICIT,LINEA_SOLICIT,GMN1,GMN2,GMN3,GMN4,DEN_PROCE,ADJDIR,PRES_PROCE,FECNEC,FECAPE,FECVAL,FECPRES,FECLIMOFE,FECPROXREU,FECULTREU,REUDEC,NUM_OFE,MON_PROCE,CAMBIO_PROCE,MON_OFE,CAMBIO_OFE,ESP_PROCE,SOLICITUD,USUAPER,PAG_PROCE,FECINI_PROCE,DEST_PROCE,COM,EQP,FECCIERRE,L.ORIGEN,@USUARIO USU,L.ERP" & vbCrLf
sConsulta = sConsulta & "from LOG_ITEM_ADJ L WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN LOG_GRAL WITH (NOLOCK)  on L.ID=log_GRAL.ID_TABLA" & vbCrLf
sConsulta = sConsulta & "WHERE LOG_GRAL.ID =@IDLOGGRAL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @MYID=SCOPE_IDENTITY()" & vbCrLf
sConsulta = sConsulta & "SET @LOG=@@IDENTITY" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO LOG_ITEM_ADJ_ATRIB (ID_LOG_ITEM_ADJ,ATRIB,VALOR_NUM,VALOR_BOOL,VALOR_FEC,VALOR_TEXT,AMBITO)" & vbCrLf
sConsulta = sConsulta & "select @MYID,ATRIB,VALOR_NUM,VALOR_BOOL,VALOR_FEC,VALOR_TEXT,AMBITO" & vbCrLf
sConsulta = sConsulta & "from LOG_ITEM_ADJ_ATRIB L WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN LOG_GRAL WITH (NOLOCK)  on L.ID_LOG_ITEM_ADJ=log_GRAL.ID_TABLA" & vbCrLf
sConsulta = sConsulta & "WHERE LOG_GRAL.ID =@IDLOGGRAL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO LOG_ITEM_ADJ_ESC(ID_LOG_ITEM_ADJ,ESC,ART,DESCR,INICIO,FIN,PREC_ADJ)" & vbCrLf
sConsulta = sConsulta & "select @MYID,ESC,L.ART,L.DESCR,INICIO,FIN,L.PREC_ADJ" & vbCrLf
sConsulta = sConsulta & "from LOG_ITEM_ADJ_ESC L WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN LOG_GRAL WITH (NOLOCK)  on L.ID_LOG_ITEM_ADJ =log_GRAL.ID_TABLA" & vbCrLf
sConsulta = sConsulta & "WHERE LOG_GRAL.ID =@IDLOGGRAL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE LOG_ITEM_ADJ SET ULTIMO=0 FROM LOG_ITEM_ADJ WITH(NOLOCK) INNER JOIN LOG_GRAL WITH(NOLOCK) ON LOG_GRAL.ID_TABLA=LOG_ITEM_ADJ.ID WHERE ULTIMO=1 AND LOG_GRAL.ID=@IDLOGGRAL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "IF @TABLA=11 OR @TABLA=13" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "--11 Y 13 PEDIDOS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO LOG_ORDEN_ENTREGA(ACCION,ID_ORDEN_ENTREGA,ID_PEDIDO,ID_ORDEN_EST,ANYO,NUM_PEDIDO,PROVE,NUM_ORDEN,NUMEXT,TIPO,FECHA,IMPORTE,MON,CAMBIO,PAG,EST,INCORRECTA,COMENT,TIPO_MODIFICACION,ORIGEN,PER,USU," & vbCrLf
sConsulta = sConsulta & "REFERENCIA,OBS,EMPRESA,COD_PROVE_ERP,RECEPTOR,ORGCOMPRAS,ERP,VIA_PAG,TIPOPEDIDO,FAC_DIR_ENVIO,IM_RETGARANTIA,ABONO_DESDE,ABONO_HASTA,FACTURA,IM_AUTOFACTURA,COD_PROVE_ERP_FACTURA,COMEN_INT,FEC_ALTA, NUM_PED_ERP," & vbCrLf
sConsulta = sConsulta & "PED_ABIERTO_TIPO, PED_ABIERTO_FECINI, PED_ABIERTO_FECFIN, ACEP_PROVE, VISITADO_P, ABONO, PLAN_ENTREGA, COSTES, DESCUENTOS, NOTIFICADO, TIPO_LOG_PEDIDO, CAMPOS_MODIFICADOS, EST_INT, CON_NOM)" & vbCrLf
sConsulta = sConsulta & "select ACCION,ID_ORDEN_ENTREGA,ID_PEDIDO,ID_ORDEN_EST,ANYO,NUM_PEDIDO,PROVE,NUM_ORDEN,NUMEXT,TIPO,FECHA,IMPORTE,MON,CAMBIO,PAG,EST,INCORRECTA,COMENT,TIPO_MODIFICACION,L.ORIGEN,PER,@USUARIO USU," & vbCrLf
sConsulta = sConsulta & "REFERENCIA,OBS,EMPRESA,COD_PROVE_ERP,RECEPTOR,ORGCOMPRAS,L.ERP,VIA_PAG,TIPOPEDIDO,FAC_DIR_ENVIO,IM_RETGARANTIA,ABONO_DESDE,ABONO_HASTA,FACTURA,IM_AUTOFACTURA,COD_PROVE_ERP_FACTURA,COMEN_INT,FEC_ALTA, NUM_PED_ERP," & vbCrLf
sConsulta = sConsulta & "PED_ABIERTO_TIPO, PED_ABIERTO_FECINI, PED_ABIERTO_FECFIN, ACEP_PROVE, VISITADO_P, ABONO, PLAN_ENTREGA, COSTES, DESCUENTOS, NOTIFICADO, TIPO_LOG_PEDIDO, CAMPOS_MODIFICADOS, EST_INT, CON_NOM" & vbCrLf
sConsulta = sConsulta & "from LOG_ORDEN_ENTREGA L WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN LOG_GRAL WITH (NOLOCK)  on L.ID=log_GRAL.ID_TABLA" & vbCrLf
sConsulta = sConsulta & "WHERE  l.ultimo=1 and LOG_GRAL.ID =@IDLOGGRAL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @MYID=SCOPE_IDENTITY()" & vbCrLf
sConsulta = sConsulta & "SET @LOG=@@IDENTITY" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF NOT @LOG IS NULL" & vbCrLf
sConsulta = sConsulta & "begin" & vbCrLf
sConsulta = sConsulta & "INSERT INTO LOG_ORDEN_ENTREGA_ATRIB (ID_LOG_ORDEN_ENTREGA,ATRIB,VALOR_NUM,VALOR_TEXT,VALOR_FEC,VALOR_BOOL)" & vbCrLf
sConsulta = sConsulta & "select @MYID,ATRIB,VALOR_NUM,VALOR_TEXT,VALOR_FEC,VALOR_BOOL" & vbCrLf
sConsulta = sConsulta & "from LOG_ORDEN_ENTREGA_ATRIB L WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN LOG_GRAL WITH (NOLOCK) ON L.ID_LOG_ORDEN_ENTREGA=log_GRAL.ID_TABLA" & vbCrLf
sConsulta = sConsulta & "WHERE LOG_GRAL.ID =@IDLOGGRAL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO LOG_LINEAS_PEDIDO (ID_LOG_ORDEN_ENTREGA,ID_ORDEN_EST,ID_LINEAS_PEDIDO,ACCION,ART_INT,UP,PREC_UP,CANT_PED,DEST,ANYO,GMN1,PROCE,ITEM,ENTREGA_OBL,FECENTREGA,FECENTREGAPROVE," & vbCrLf
sConsulta = sConsulta & "EST,CAT1,CAT2,CAT3,CAT4,CAT5,OBS,FC,SOLICIT,DESCR_LIBRE,CAMPO1,VALOR1,CAMPO2,VALOR2,OBSADJUN,CENTROS,ALMACEN,PEDIDOABIERTO,ERP,ACTIVO,NUM, TIPORECEPCION, IMPORTE_PED," & vbCrLf
sConsulta = sConsulta & "LINEA_SOLICIT, BAJA_LOG, DEST_GENERICO, DEST_DIR, DEST_POB, DEST_CP, DEST_PROVI, DEST_PAI, DEST_TFNO, DEST_FAX, CAMPOS_MODIFICADOS, LINCAT, PROVEADJ, PORCEN_DESVIO, COSTES, DESCUENTOS, SM_IMPORTECOMPR, SM_IMPORTECOMPRIMPUESTOS," & vbCrLf
sConsulta = sConsulta & "CAMPO_SOLICIT, CANTIDAD_PED_ABIERTO, IMPORTE_PED_ABIERTO, FEC_ALTA, ORDEN_PED_ABIERTO, ADD_MATR_ART, BLOQ_MOD, LINEA_PED_ABIERTO, LINEAS_EST, DEST_DEN)" & vbCrLf
sConsulta = sConsulta & "select @MYID,L.ID_ORDEN_EST,ID_LINEAS_PEDIDO,L.ACCION,ART_INT,UP,PREC_UP,CANT_PED,DEST,L.ANYO,GMN1,PROCE,ITEM,ENTREGA_OBL,FECENTREGA,FECENTREGAPROVE," & vbCrLf
sConsulta = sConsulta & "L.EST,CAT1,CAT2,CAT3,CAT4,CAT5,L.OBS,FC,SOLICIT,DESCR_LIBRE,CAMPO1,VALOR1,CAMPO2,VALOR2,OBSADJUN,CENTROS,ALMACEN,PEDIDOABIERTO,L.ERP,ACTIVO,NUM,TIPORECEPCION, IMPORTE_PED," & vbCrLf
sConsulta = sConsulta & "LINEA_SOLICIT, BAJA_LOG, DEST_GENERICO, DEST_DIR, DEST_POB, DEST_CP, DEST_PROVI, DEST_PAI, DEST_TFNO, DEST_FAX, CAMPOS_MODIFICADOS, LINCAT, PROVEADJ, PORCEN_DESVIO, COSTES, DESCUENTOS, SM_IMPORTECOMPR, SM_IMPORTECOMPRIMPUESTOS," & vbCrLf
sConsulta = sConsulta & "CAMPO_SOLICIT, CANTIDAD_PED_ABIERTO, IMPORTE_PED_ABIERTO, FEC_ALTA, ORDEN_PED_ABIERTO, ADD_MATR_ART, BLOQ_MOD, LINEA_PED_ABIERTO, LINEAS_EST, DEST_DEN" & vbCrLf
sConsulta = sConsulta & "from LOG_LINEAS_PEDIDO L WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN LOG_GRAL WITH (NOLOCK) ON L.ID_LOG_ORDEN_ENTREGA=log_GRAL.ID_TABLA" & vbCrLf
sConsulta = sConsulta & "WHERE LOG_GRAL.ID =@IDLOGGRAL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO LOG_LINEAS_PEDIDO_ATRIB (ID_LOG_LINEA,ATRIB,VALOR_NUM,VALOR_TEXT,VALOR_FEC,VALOR_BOOL)" & vbCrLf
sConsulta = sConsulta & "select LLPN.ID,ATRIB,VALOR_NUM,VALOR_TEXT,VALOR_FEC,VALOR_BOOL" & vbCrLf
sConsulta = sConsulta & "from LOG_LINEAS_PEDIDO_ATRIB L WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN LOG_LINEAS_PEDIDO LLP WITH (NOLOCK) ON LLP.ID = L.ID_LOG_LINEA" & vbCrLf
sConsulta = sConsulta & "INNER JOIN LOG_GRAL WITH (NOLOCK) ON LLP.ID_LOG_ORDEN_ENTREGA  =log_GRAL.ID_TABLA" & vbCrLf
sConsulta = sConsulta & "INNER JOIN LOG_LINEAS_PEDIDO LLPN WITH (NOLOCK) ON LLPN.ID_LINEAS_PEDIDO = LLP.ID_LINEAS_PEDIDO" & vbCrLf
sConsulta = sConsulta & "WHERE LOG_GRAL.ID =@IDLOGGRAL AND LLPN.ID_LOG_ORDEN_ENTREGA=@MYID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO LOG_LINEAS_PED_IMPUTACION (ID_LOG_LINEA,LINEA,UON1,UON2,UON3,UON4,PRES0,PRES1,PRES2,PRES3,PRES4,CAMBIO)" & vbCrLf
sConsulta = sConsulta & "select LLPN.ID,LINEA,UON1,UON2,UON3,UON4,PRES0,PRES1,PRES2,PRES3,PRES4,L.CAMBIO" & vbCrLf
sConsulta = sConsulta & "from LOG_LINEAS_PED_IMPUTACION L WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN LOG_LINEAS_PEDIDO LLP WITH (NOLOCK) ON LLP.ID = L.ID_LOG_LINEA" & vbCrLf
sConsulta = sConsulta & "INNER JOIN LOG_GRAL WITH (NOLOCK) ON LLP.ID_LOG_ORDEN_ENTREGA  =log_GRAL.ID_TABLA" & vbCrLf
sConsulta = sConsulta & "INNER JOIN LOG_LINEAS_PEDIDO LLPN WITH (NOLOCK) ON LLPN.ID_LINEAS_PEDIDO = LLP.ID_LINEAS_PEDIDO" & vbCrLf
sConsulta = sConsulta & "WHERE LOG_GRAL.ID =@IDLOGGRAL AND LLPN.ID_LOG_ORDEN_ENTREGA=@MYID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO LOG_PEDIDO_PRES1 (ID_LOG_ORDEN_ENTREGA,ID_LOG_LINEAS_PEDIDO,ID_LINEA,PRES1,PRES2,PRES3,PRES4,PORCEN)" & vbCrLf
sConsulta = sConsulta & "select @MYID,LLPN.ID_LINEAS_PEDIDO ,ID_LINEA,PRES1,PRES2,PRES3,PRES4,PORCEN" & vbCrLf
sConsulta = sConsulta & "from LOG_PEDIDO_PRES1 L WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN LOG_LINEAS_PEDIDO LLP WITH (NOLOCK) ON LLP.ID = L.ID_LOG_LINEAS_PEDIDO" & vbCrLf
sConsulta = sConsulta & "INNER JOIN LOG_GRAL WITH (NOLOCK) ON LLP.ID_LOG_ORDEN_ENTREGA  =log_GRAL.ID_TABLA" & vbCrLf
sConsulta = sConsulta & "INNER JOIN LOG_LINEAS_PEDIDO LLPN WITH (NOLOCK) ON LLPN.ID_LINEAS_PEDIDO = LLP.ID_LINEAS_PEDIDO" & vbCrLf
sConsulta = sConsulta & "WHERE LOG_GRAL.ID =@IDLOGGRAL AND LLPN.ID_LOG_ORDEN_ENTREGA=@MYID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO LOG_PEDIDO_PRES2 (ID_LOG_ORDEN_ENTREGA,ID_LOG_LINEAS_PEDIDO,ID_LINEA,PRES1,PRES2,PRES3,PRES4,PORCEN)" & vbCrLf
sConsulta = sConsulta & "select @MYID,LLPN.ID_LINEAS_PEDIDO,ID_LINEA,PRES1,PRES2,PRES3,PRES4,PORCEN" & vbCrLf
sConsulta = sConsulta & "from LOG_PEDIDO_PRES2 L WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN LOG_LINEAS_PEDIDO LLP WITH (NOLOCK) ON LLP.ID = L.ID_LOG_LINEAS_PEDIDO" & vbCrLf
sConsulta = sConsulta & "INNER JOIN LOG_GRAL WITH (NOLOCK) ON LLP.ID_LOG_ORDEN_ENTREGA  =log_GRAL.ID_TABLA" & vbCrLf
sConsulta = sConsulta & "INNER JOIN LOG_LINEAS_PEDIDO LLPN WITH (NOLOCK) ON LLPN.ID_LINEAS_PEDIDO = LLP.ID_LINEAS_PEDIDO" & vbCrLf
sConsulta = sConsulta & "WHERE LOG_GRAL.ID =@IDLOGGRAL AND LLPN.ID_LOG_ORDEN_ENTREGA=@MYID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO LOG_PEDIDO_PRES3 (ID_LOG_ORDEN_ENTREGA,ID_LOG_LINEAS_PEDIDO,ID_LINEA,PRES1,PRES2,PRES3,PRES4,PORCEN)" & vbCrLf
sConsulta = sConsulta & "select @MYID,LLPN.ID_LINEAS_PEDIDO ,ID_LINEA,PRES1,PRES2,PRES3,PRES4,PORCEN" & vbCrLf
sConsulta = sConsulta & "from LOG_PEDIDO_PRES3 L WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN LOG_LINEAS_PEDIDO LLP WITH (NOLOCK) ON LLP.ID = L.ID_LOG_LINEAS_PEDIDO" & vbCrLf
sConsulta = sConsulta & "INNER JOIN LOG_GRAL WITH (NOLOCK) ON LLP.ID_LOG_ORDEN_ENTREGA  =log_GRAL.ID_TABLA" & vbCrLf
sConsulta = sConsulta & "INNER JOIN LOG_LINEAS_PEDIDO LLPN WITH (NOLOCK) ON LLPN.ID_LINEAS_PEDIDO = LLP.ID_LINEAS_PEDIDO" & vbCrLf
sConsulta = sConsulta & "WHERE LOG_GRAL.ID =@IDLOGGRAL AND LLPN.ID_LOG_ORDEN_ENTREGA=@MYID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO LOG_PEDIDO_PRES4 (ID_LOG_ORDEN_ENTREGA,ID_LOG_LINEAS_PEDIDO,ID_LINEA,PRES1,PRES2,PRES3,PRES4,PORCEN)" & vbCrLf
sConsulta = sConsulta & "select @MYID,LLPN.ID_LINEAS_PEDIDO,ID_LINEA,PRES1,PRES2,PRES3,PRES4,PORCEN" & vbCrLf
sConsulta = sConsulta & "from LOG_PEDIDO_PRES4 L WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN LOG_LINEAS_PEDIDO LLP WITH (NOLOCK) ON LLP.ID = L.ID_LOG_LINEAS_PEDIDO" & vbCrLf
sConsulta = sConsulta & "INNER JOIN LOG_GRAL WITH (NOLOCK) ON LLP.ID_LOG_ORDEN_ENTREGA  =log_GRAL.ID_TABLA" & vbCrLf
sConsulta = sConsulta & "INNER JOIN LOG_LINEAS_PEDIDO LLPN WITH (NOLOCK) ON LLPN.ID_LINEAS_PEDIDO = LLP.ID_LINEAS_PEDIDO" & vbCrLf
sConsulta = sConsulta & "WHERE LOG_GRAL.ID =@IDLOGGRAL AND LLPN.ID_LOG_ORDEN_ENTREGA=@MYID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "-- TAREA 5815 Y 5743 RELANZAR INTEGRACION DESDE GS Y EP." & vbCrLf
sConsulta = sConsulta & "-- HAY QUE ACTUALIZAR EL CAMPO EST_INT DE ORDEN_ENTREGA PARA QUE ESTE SINCRONIZADO CON EL ESTADO DEL ULTIMO MOVIMIENTO" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_ORDEN_ENTREGA INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @ID_ORDEN_ENTREGA=L.ID_ORDEN_ENTREGA" & vbCrLf
sConsulta = sConsulta & "FROM LOG_ORDEN_ENTREGA L WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN LOG_GRAL LG WITH (NOLOCK)  on L.ID=LG.ID_TABLA" & vbCrLf
sConsulta = sConsulta & "WHERE LG.ID =@IDLOGGRAL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE ORDEN_ENTREGA SET EST_INT=0 WHERE ID=@ID_ORDEN_ENTREGA" & vbCrLf
sConsulta = sConsulta & "end" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "IF @TABLA=12 OR @TABLA=14" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--12,14 RECEPCIONES" & vbCrLf
sConsulta = sConsulta & "INSERT INTO LOG_PEDIDO_RECEP (ACCION,ID_PEDIDO_RECEP,ID_ORDEN_ENTREGA,ID_PEDIDO,NUM_ORDEN,PROVE,FECEMISION,ANYO,TIPO,FECHA,ALBARAN,CORRECTO,COMENT,ORIGEN,PER,USU,ERP,FEC_SISTEMA,FEC_CONTABLE)" & vbCrLf
sConsulta = sConsulta & "select ACCION,ID_PEDIDO_RECEP,ID_ORDEN_ENTREGA,ID_PEDIDO,NUM_ORDEN,PROVE,FECEMISION,ANYO,TIPO,FECHA,ALBARAN,CORRECTO,COMENT,L.ORIGEN,PER,@USUARIO USU,L.ERP,FEC_SISTEMA,FEC_CONTABLE" & vbCrLf
sConsulta = sConsulta & "from LOG_PEDIDO_RECEP L WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN LOG_GRAL WITH (NOLOCK) ON L.ID=log_GRAL.ID_TABLA" & vbCrLf
sConsulta = sConsulta & "WHERE LOG_GRAL.ID =@IDLOGGRAL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @MYID=SCOPE_IDENTITY()" & vbCrLf
sConsulta = sConsulta & "SET @LOG=@@IDENTITY" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO LOG_LINEAS_RECEP (ID_LOG_PEDIDO_RECEP,ID_PEDIDO_RECEP,ID_LINEA_RECEP,ID_ORDEN_ENTREGA,ID_LINEA_PEDIDO,CANT,ACCION,IMPORTE, SALIDA_ALMACEN)" & vbCrLf
sConsulta = sConsulta & "select @MYID,L.ID_PEDIDO_RECEP,ID_LINEA_RECEP,L.ID_ORDEN_ENTREGA,ID_LINEA_PEDIDO,CANT,L.ACCION,L.IMPORTE, SALIDA_ALMACEN" & vbCrLf
sConsulta = sConsulta & "from LOG_LINEAS_RECEP L WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN LOG_GRAL WITH (NOLOCK) ON L.ID_LOG_PEDIDO_RECEP =log_GRAL.ID_TABLA" & vbCrLf
sConsulta = sConsulta & "WHERE LOG_GRAL.ID =@IDLOGGRAL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "IF @TABLA=15" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "--15 ESTRUCTURA DE MATERIALES" & vbCrLf
sConsulta = sConsulta & "INSERT INTO LOG_GMN(ACCION,GMN1,GMN2,GMN3,GMN4,DEN_SPA,COD_NEW,ORIGEN,USU,DEN_ENG,DEN_GER,ERP)" & vbCrLf
sConsulta = sConsulta & "select ACCION,GMN1,GMN2,GMN3,GMN4,DEN_SPA,COD_NEW,L.ORIGEN,@USUARIO USU,DEN_ENG,DEN_GER,L.ERP" & vbCrLf
sConsulta = sConsulta & "from LOG_GMN L WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN LOG_GRAL WITH (NOLOCK) on L.ID=log_GRAL.ID_TABLA" & vbCrLf
sConsulta = sConsulta & "WHERE LOG_GRAL.ID =@IDLOGGRAL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @MYID=SCOPE_IDENTITY()" & vbCrLf
sConsulta = sConsulta & "SET @LOG=@@IDENTITY" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "IF @TABLA=16" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "--16 PRESUPUESTOS ANUALES DE ARTICULOS" & vbCrLf
sConsulta = sConsulta & "INSERT INTO LOG_PRES_ART(ACCION,ANYO,COD,CANT,PRES,OBJ,ORIGEN,USU,ERP)" & vbCrLf
sConsulta = sConsulta & "select ACCION,ANYO,COD,CANT,PRES,OBJ,L.ORIGEN,@USUARIO USU,L.ERP" & vbCrLf
sConsulta = sConsulta & "from LOG_PRES_ART L WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN LOG_GRAL WITH (NOLOCK)  on L.ID=log_GRAL.ID_TABLA" & vbCrLf
sConsulta = sConsulta & "WHERE LOG_GRAL.ID =@IDLOGGRAL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @MYID=SCOPE_IDENTITY()" & vbCrLf
sConsulta = sConsulta & "SET @LOG=@@IDENTITY" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "IF @TABLA=17" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--17 VARCAL_INT" & vbCrLf
sConsulta = sConsulta & "INSERT INTO LOG_VAR_CAL_INT(ACCION,PROVE,UNQA,VARCAL,NIVEL,VALOR,FECHA,ORIGEN,USU,ERP)" & vbCrLf
sConsulta = sConsulta & "select ACCION,PROVE,UNQA,VARCAL,NIVEL,VALOR,FECHA,L.ORIGEN,@USUARIO USU,L.ERP" & vbCrLf
sConsulta = sConsulta & "from LOG_VAR_CAL_INT L WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN LOG_GRAL WITH (NOLOCK)  on L.ID=log_GRAL.ID_TABLA" & vbCrLf
sConsulta = sConsulta & "WHERE LOG_GRAL.ID =@IDLOGGRAL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @MYID=SCOPE_IDENTITY()" & vbCrLf
sConsulta = sConsulta & "SET @LOG=@@IDENTITY" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "IF @TABLA=18" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "--18 SOLICITUDES PM" & vbCrLf
sConsulta = sConsulta & "INSERT INTO LOG_INSTANCIA(INSTANCIA,ACCION,SOLICITUD,FEC_ALTA,ESTADO,NUM_VERSION,PETICIONARIO,COMPRADOR,IMPORTE,MON,CAMBIO,ULT_VERSION,ORIGEN,USU,ERP,INSTANCIA_EST)" & vbCrLf
sConsulta = sConsulta & "select INSTANCIA,ACCION,SOLICITUD,FEC_ALTA,L.ESTADO,NUM_VERSION,PETICIONARIO,COMPRADOR,IMPORTE,MON,CAMBIO,ULT_VERSION,L.ORIGEN,@USUARIO USU,L.ERP,INSTANCIA_EST" & vbCrLf
sConsulta = sConsulta & "from LOG_INSTANCIA L WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN LOG_GRAL WITH (NOLOCK)  on L.ID=log_GRAL.ID_TABLA" & vbCrLf
sConsulta = sConsulta & "WHERE LOG_GRAL.ID =@IDLOGGRAL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @MYID=SCOPE_IDENTITY()" & vbCrLf
sConsulta = sConsulta & "SET @LOG=@@IDENTITY" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "IF @TABLA=19 OR @TABLA=20 OR @TABLA=21 OR @TABLA=22" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "--19,20,21 Y 22 CONCEPTOS PRESUPUESTARIOS" & vbCrLf
sConsulta = sConsulta & "INSERT INTO LOG_PRES(ACCION,TIPO_PRES,PRES1,PRES2,PRES3,PRES4,ANYO,DEN,UON1,UON2,UON3,IMP,OBJ,FEC_INI_VAL,FEC_FIN_VAL,BAJALOG,COD_NEW,ORIGEN,USU,ERP)" & vbCrLf
sConsulta = sConsulta & "select ACCION,TIPO_PRES,PRES1,PRES2,PRES3,PRES4,ANYO,DEN,UON1,UON2,UON3,IMP,OBJ,FEC_INI_VAL,FEC_FIN_VAL,BAJALOG,COD_NEW,L.ORIGEN,@USUARIO USU,L.ERP" & vbCrLf
sConsulta = sConsulta & "from LOG_PRES L WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN LOG_GRAL WITH (NOLOCK)  on L.ID=log_GRAL.ID_TABLA" & vbCrLf
sConsulta = sConsulta & "WHERE LOG_GRAL.ID =@IDLOGGRAL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @MYID=SCOPE_IDENTITY()" & vbCrLf
sConsulta = sConsulta & "SET @LOG=@@IDENTITY" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "IF @TABLA=23" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "--23 PROCESOS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO LOG_PROCE (ACCION, ANYO,COD,GMN1,DEN,ADJDIR,PRES,FECNEC,FECAPE,FECPRES,FECLIMOFE,MON,ESP,SOLICITUD,DEST,PAG,FECINI,PROVEACT,PRESTOTAL,USUAPER,EQP,COM,OFE_ADJUN,GRUPO_ADJUN,ITEM_ADJUN,SOLCANTMAX,PONDERAR,PRECALTER,CAMBIARMON,UNSOLOPEDIDO,PUBLICARFINSUM,USU,ORIGEN,ERP)" & vbCrLf
sConsulta = sConsulta & "select ACCION, ANYO,COD,GMN1,DEN,ADJDIR,PRES,FECNEC,FECAPE,FECPRES,FECLIMOFE,MON,ESP,SOLICITUD,DEST,PAG,FECINI,PROVEACT,PRESTOTAL,USUAPER,EQP,COM,OFE_ADJUN,GRUPO_ADJUN,ITEM_ADJUN,SOLCANTMAX,PONDERAR,PRECALTER,CAMBIARMON,UNSOLOPEDIDO,PUBLICARFINSUM,@USUARIO USU,L.ORIGEN,L.ERP" & vbCrLf
sConsulta = sConsulta & "from LOG_PROCE L WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN LOG_GRAL WITH (NOLOCK)  on L.ID=log_GRAL.ID_TABLA" & vbCrLf
sConsulta = sConsulta & "WHERE LOG_GRAL.ID =@IDLOGGRAL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @MYID = SCOPE_IDENTITY ()" & vbCrLf
sConsulta = sConsulta & "SET @LOG=@@IDENTITY" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO LOG_PROCE_ATRIB (ID_LOG_PROCE,ACCION, GRUPO,ID_ATRIB,COD_ATRIB,INTERNO,AMBITO,OBLIG,OP_PREC,APLIC_PREC,DEF_PREC,ORDEN,ORDEN_ATRIB)" & vbCrLf
sConsulta = sConsulta & "SELECT @MYID,L.ACCION, GRUPO,ID_ATRIB,COD_ATRIB,INTERNO,AMBITO,OBLIG,OP_PREC,APLIC_PREC,DEF_PREC,ORDEN,ORDEN_ATRIB" & vbCrLf
sConsulta = sConsulta & "from LOG_PROCE_ATRIB L WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN LOG_GRAL WITH (NOLOCK)  on L.ID_LOG_PROCE =log_GRAL.ID_TABLA" & vbCrLf
sConsulta = sConsulta & "WHERE LOG_GRAL.ID =@IDLOGGRAL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO LOG_PROCE_ESP (ID_LOG_PROCE,ID_LOG_PROCE_GRUPO,ID_LOG_ITEM,ACCION,ID_ESP,NOM,COM,RUTA)" & vbCrLf
sConsulta = sConsulta & "SELECT @MYID,ID_LOG_PROCE_GRUPO,ID_LOG_ITEM,L.ACCION,ID_ESP,NOM,L.COM,RUTA" & vbCrLf
sConsulta = sConsulta & "from LOG_PROCE_ESP L WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN LOG_GRAL WITH (NOLOCK)  on L.ID_LOG_PROCE=log_GRAL.ID_TABLA" & vbCrLf
sConsulta = sConsulta & "WHERE LOG_GRAL.ID =@IDLOGGRAL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO LOG_PROCE_GRUPO (ID_LOG_PROCE,ACCION,COD,DEN,DESCR,DEST,PAG,FECINI,FECFIN,PROVEACT,ESP)" & vbCrLf
sConsulta = sConsulta & "SELECT @MYID,L.ACCION,L.COD,L.DEN,DESCR,L.DEST,L.PAG,L.FECINI,L.FECFIN,L.PROVEACT,L.ESP" & vbCrLf
sConsulta = sConsulta & "from LOG_PROCE_GRUPO L WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN LOG_GRAL WITH (NOLOCK)  on L.ID_LOG_PROCE=log_GRAL.ID_TABLA" & vbCrLf
sConsulta = sConsulta & "WHERE LOG_GRAL.ID =@IDLOGGRAL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO LOG_PROCE_PROVE(ACCION,ANYO,GMN1,PROCE,PROVE,EQP,COM,PUB,FECPUB,OFE,SUPERADA,NUMOBJ,OBJNUEVOS,CERRADO,NO_OFE,FEC_NO_OFE,MOTIVO_NO_OFE,AVISADODESPUB,PORTAL_NO_OFE,USU_NO_OFE,NOMUSU_NO_OFE,ORIGEN,USU,ERP)" & vbCrLf
sConsulta = sConsulta & "SELECT L.ACCION,L.ANYO,L.GMN1,PROCE,PROVE,L.EQP,L.COM,PUB,FECPUB,OFE,SUPERADA,NUMOBJ,OBJNUEVOS,CERRADO,NO_OFE,FEC_NO_OFE,MOTIVO_NO_OFE,AVISADODESPUB,PORTAL_NO_OFE,USU_NO_OFE,NOMUSU_NO_OFE,L.ORIGEN,@USUARIO USU,L.ERP" & vbCrLf
sConsulta = sConsulta & "from LOG_PROCE_PROVE L WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN LOG_GRAL WITH (NOLOCK)  on L.PROCE=log_GRAL.ID_TABLA" & vbCrLf
sConsulta = sConsulta & "WHERE TABLA=23 AND LOG_GRAL.ID =@IDLOGGRAL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO LOG_PROCE_UON (ID_LOG_PROCE,ID_LOG_PROCE_GRUPO,ID_LOG_ITEM,ACCION,UON1,UON2,UON3,PORCEN)" & vbCrLf
sConsulta = sConsulta & "SELECT @MYID,ID_LOG_PROCE_GRUPO,ID_LOG_ITEM,L.ACCION,UON1,UON2,UON3,PORCEN" & vbCrLf
sConsulta = sConsulta & "from LOG_PROCE_UON L WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN LOG_GRAL WITH (NOLOCK)  on L.ID_LOG_PROCE=log_GRAL.ID_TABLA" & vbCrLf
sConsulta = sConsulta & "WHERE LOG_GRAL.ID =@IDLOGGRAL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO LOG_ITEM (ID_LOG_PROCE,ACCION,NUM_LINEA,ID_ITEM,GMN1,GMN2,GMN3,GMN4,ART,DESCR,DEST,UNI,PAG,CANT,PREC,FECINI,FECFIN,ESP,PROVEACT,SOLICIT,LINEA_SOLICIT,USU,PRES_PLANIFICADO,NECESIDAD)" & vbCrLf
sConsulta = sConsulta & "SELECT @MYID,L.ACCION,NUM_LINEA,ID_ITEM,L.GMN1,GMN2,GMN3,GMN4,ART,DESCR,L.DEST,UNI,L.PAG,CANT,PREC,L.FECINI,L.FECFIN,L.ESP,L.PROVEACT,SOLICIT,LINEA_SOLICIT,@USUARIO USU,PRES_PLANIFICADO,NECESIDAD" & vbCrLf
sConsulta = sConsulta & "from LOG_ITEM L WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN LOG_GRAL WITH (NOLOCK)  on L.ID_LOG_PROCE=log_GRAL.ID_TABLA" & vbCrLf
sConsulta = sConsulta & "WHERE LOG_GRAL.ID =@IDLOGGRAL" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "IF @TABLA=24" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "--24 TABLAS EXTERNAS" & vbCrLf
sConsulta = sConsulta & "INSERT INTO LOG_TABLAS_EXTERNAS (TABLA,ACCION,CAMPO1,CAMPO2,CAMPO3,CAMPO4,CAMPO5,ORIGEN,USU,ERP)" & vbCrLf
sConsulta = sConsulta & "SELECT L.TABLA,ACCION,CAMPO1,CAMPO2,CAMPO3,CAMPO4,CAMPO5,L.ORIGEN,@USUARIO USU,L.ERP" & vbCrLf
sConsulta = sConsulta & "from LOG_TABLAS_EXTERNAS L WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN LOG_GRAL WITH (NOLOCK)  on L.ID=log_GRAL.ID_TABLA" & vbCrLf
sConsulta = sConsulta & "WHERE LOG_GRAL.ID =@IDLOGGRAL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @MYID=SCOPE_IDENTITY()" & vbCrLf
sConsulta = sConsulta & "SET @LOG=@@IDENTITY" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "IF @TABLA=25" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "--25 VIAS DE PAGO" & vbCrLf
sConsulta = sConsulta & "INSERT INTO LOG_VIA_PAG (ACCION,COD,COD_NEW,ORIGEN,USU,ERP)" & vbCrLf
sConsulta = sConsulta & "SELECT ACCION,COD,COD_NEW,L.ORIGEN,@USUARIO USU,L.ERP" & vbCrLf
sConsulta = sConsulta & "from LOG_VIA_PAG L WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN LOG_GRAL WITH (NOLOCK)ON L.ID=log_GRAL.ID_TABLA" & vbCrLf
sConsulta = sConsulta & "WHERE LOG_GRAL.ID =@IDLOGGRAL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @MYID=SCOPE_IDENTITY()" & vbCrLf
sConsulta = sConsulta & "SET @LOG=@@IDENTITY" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO LOG_VIA_PAG_DEN(ID_LOG_VIA_PAG,IDI,DEN)" & vbCrLf
sConsulta = sConsulta & "SELECT @MYID,IDI,DEN" & vbCrLf
sConsulta = sConsulta & "FROM LOG_VIA_PAG_DEN L WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN LOG_GRAL WITH (NOLOCK)  ON L.ID_LOG_VIA_PAG=log_GRAL.ID_TABLA" & vbCrLf
sConsulta = sConsulta & "WHERE LOG_GRAL.ID =@IDLOGGRAL" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "IF @TABLA=26" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--26 ppm" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO LOG_PPM (ACCION,PROVE,ART4,UONQA,FECHA,PIEZAS_DEFECTUOSAS,PIEZAS_SERVIDAS,ORIGEN,USU,ERP)" & vbCrLf
sConsulta = sConsulta & "SELECT ACCION,PROVE,ART4,UONQA,FECHA,PIEZAS_DEFECTUOSAS,PIEZAS_SERVIDAS,L.ORIGEN,@USUARIO USU,L.ERP" & vbCrLf
sConsulta = sConsulta & "from LOG_PPM  L WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN LOG_GRAL WITH (NOLOCK)  on L.ID=log_GRAL.ID_TABLA" & vbCrLf
sConsulta = sConsulta & "WHERE LOG_GRAL.ID =@IDLOGGRAL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @MYID=SCOPE_IDENTITY()" & vbCrLf
sConsulta = sConsulta & "SET @LOG=@@IDENTITY" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "IF @TABLA=27" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "--27 TASAS DE SERVICIOS" & vbCrLf
sConsulta = sConsulta & "INSERT INTO LOG_TASA_SERVICIO (ACCION,PROVE,ART4,UONQA,FECHA,ENVIOS_CORRECTOS,TOTAL_ENVIOS,ORIGEN,USU,ERP)" & vbCrLf
sConsulta = sConsulta & "SELECT ACCION,PROVE,ART4,UONQA,FECHA,ENVIOS_CORRECTOS,TOTAL_ENVIOS,L.ORIGEN,@USUARIO USU,L.ERP" & vbCrLf
sConsulta = sConsulta & "from LOG_TASA_SERVICIO  L WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN LOG_GRAL WITH (NOLOCK)  on L.ID=log_GRAL.ID_TABLA" & vbCrLf
sConsulta = sConsulta & "WHERE LOG_GRAL.ID =@IDLOGGRAL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @MYID=SCOPE_IDENTITY()" & vbCrLf
sConsulta = sConsulta & "SET @LOG=@@IDENTITY" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "IF @TABLA=28" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "--28 cargos a proveedores" & vbCrLf
sConsulta = sConsulta & "INSERT INTO LOG_CARGO_PROVEEDORES (ACCION,PROVE,ART4,UONQA,FECHA,IMPORTE_REPERCUTIDO,IMPORTE_FACTURADO,MON,ORIGEN,USU,ERP)" & vbCrLf
sConsulta = sConsulta & "SELECT ACCION,PROVE,ART4,UONQA,FECHA,IMPORTE_REPERCUTIDO,IMPORTE_FACTURADO,MON,L.ORIGEN,@USUARIO USU,L.ERP" & vbCrLf
sConsulta = sConsulta & "from LOG_CARGO_PROVEEDORES  L WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN LOG_GRAL WITH (NOLOCK)  on L.ID=log_GRAL.ID_TABLA" & vbCrLf
sConsulta = sConsulta & "WHERE LOG_GRAL.ID =@IDLOGGRAL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @MYID=SCOPE_IDENTITY()" & vbCrLf
sConsulta = sConsulta & "SET @LOG=@@IDENTITY" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "IF @TABLA=29" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "--29 UNIDADES ORGANIZATIVAS" & vbCrLf
sConsulta = sConsulta & "INSERT INTO LOG_UON (ACCION,UON1,UON2,UON3,UON4,DEN,EMPRESA,ORGCOMPRAS,CENTROS,ALMACEN,BAJALOG,COD_NEW,ORIGEN,USU,ERP)" & vbCrLf
sConsulta = sConsulta & "SELECT ACCION,UON1,UON2,UON3,UON4,DEN,EMPRESA,ORGCOMPRAS,CENTROS,ALMACEN,BAJALOG,COD_NEW,L.ORIGEN,@USUARIO USU,L.ERP" & vbCrLf
sConsulta = sConsulta & "from LOG_UON  L WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN LOG_GRAL WITH (NOLOCK)  on L.ID=log_GRAL.ID_TABLA" & vbCrLf
sConsulta = sConsulta & "WHERE LOG_GRAL.ID =@IDLOGGRAL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @MYID=SCOPE_IDENTITY()" & vbCrLf
sConsulta = sConsulta & "SET @LOG=@@IDENTITY" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "IF @TABLA=30" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "--30 USUARIOS" & vbCrLf
sConsulta = sConsulta & "INSERT INTO LOG_USU(ACCION,COD,PER,PWD,FECPWD,UON1,UON2,UON3,TIPO,NOM,APE,DEP,EQP,CAR,TFNO,TFNO2,FAX,EMAIL,COD_NEW,BAJALOG,ORIGEN,USU,ERP,PERF,THOUSANFMT,DECIMALFMT,PRECISIONFMT,DATEFMT,MOSTRARFMT,MON,IDIOMA,FSEP,SOL_COMPRA,FSGS,FSQA,FECALTA,FSINT)" & vbCrLf
sConsulta = sConsulta & "SELECT ACCION,COD,PER,PWD,FECPWD,UON1,UON2,UON3,TIPO,NOM,APE,DEP,EQP,CAR,TFNO,TFNO2,FAX,EMAIL,COD_NEW,BAJALOG,L.ORIGEN,@USUARIO USU,L.ERP,PERF,THOUSANFMT,DECIMALFMT,PRECISIONFMT,DATEFMT,MOSTRARFMT,MON,IDIOMA,FSEP,SOL_COMPRA,FSGS,FSQA,FECALTA,FSINT" & vbCrLf
sConsulta = sConsulta & "from LOG_USU  L WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN LOG_GRAL WITH (NOLOCK)  on L.ID=log_GRAL.ID_TABLA" & vbCrLf
sConsulta = sConsulta & "WHERE LOG_GRAL.ID =@IDLOGGRAL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @MYID=SCOPE_IDENTITY()" & vbCrLf
sConsulta = sConsulta & "SET @LOG=@@IDENTITY" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO LOG_USU_CC_IMPUTACION (ID_LOG_USU,ACCION,UON1,UON2,UON3,UON4,CONSULTA,MODIF,IMPUTACION)" & vbCrLf
sConsulta = sConsulta & "SELECT @MYID,L.ACCION,L.UON1,L.UON2,L.UON3,UON4,CONSULTA,MODIF,IMPUTACION" & vbCrLf
sConsulta = sConsulta & "from LOG_USU_CC_IMPUTACION  L WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN LOG_GRAL WITH (NOLOCK)  on L.ID_LOG_USU =log_GRAL.ID_TABLA" & vbCrLf
sConsulta = sConsulta & "WHERE LOG_GRAL.ID =@IDLOGGRAL" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "IF @TABLA=31" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "--31 activos" & vbCrLf
sConsulta = sConsulta & "INSERT INTO LOG_ACTIVO (ACCION, ID_ACTIVO,COD,COD_ERP,BAJALOG,EMPRESA,CENTRO_SM,ORIGEN,USU,ERP)" & vbCrLf
sConsulta = sConsulta & "select L.ACCION,L.ID_ACTIVO,COD,L.COD_ERP,BAJALOG,EMPRESA,CENTRO_SM,L.ORIGEN,L.ERP,@USUARIO USU" & vbCrLf
sConsulta = sConsulta & "from LOG_ACTIVO L WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN LOG_GRAL WITH (NOLOCK)  on L.ID=log_GRAL.ID_TABLA" & vbCrLf
sConsulta = sConsulta & "WHERE LOG_GRAL.ID =@IDLOGGRAL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @MYID=SCOPE_IDENTITY()" & vbCrLf
sConsulta = sConsulta & "SET @LOG=@@IDENTITY" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO LOG_ACTIVO_DEN(ID_LOG_ACTIVO,IDI,DEN)" & vbCrLf
sConsulta = sConsulta & "SELECT @MYID,IDI,DEN" & vbCrLf
sConsulta = sConsulta & "from LOG_ACTIVO_DEN  L WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN LOG_GRAL WITH (NOLOCK)  on L.ID_LOG_ACTIVO=log_GRAL.ID_TABLA" & vbCrLf
sConsulta = sConsulta & "WHERE LOG_GRAL.ID =@IDLOGGRAL" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "IF @TABLA=32" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "--32 FACTURAS" & vbCrLf
sConsulta = sConsulta & "INSERT INTO LOG_FACTURA (ACCION,ANYO,PROVE,ESTADO,FECHA,NUM,ORIGEN,USU,ERP,ID_FACTURA,NUM_ERP,IMPORTE,FEC_CONTA,EMPRESA,INSTANCIA,RET_GARANTIA,PAG,VIA_PAG,OBS,TIPO,NUM_ORIGINAL,FEC_ORIGINAL_INI,FEC_ORIGINAL_FIN,TOT_COSTES,TOT_DESCUENTOS,INTEGRACION,MON,MOTIVO_ANULACION)" & vbCrLf
sConsulta = sConsulta & "select ACCION,ANYO,PROVE,L.ESTADO,FECHA,NUM,L.ORIGEN,@USUARIO USU,L.ERP,ID_FACTURA,NUM_ERP,IMPORTE,FEC_CONTA,EMPRESA,INSTANCIA,RET_GARANTIA,PAG,VIA_PAG,OBS,TIPO,NUM_ORIGINAL,FEC_ORIGINAL_INI,FEC_ORIGINAL_FIN,TOT_COSTES,TOT_DESCUENTOS,INTEGRACION,MON,MOTIVO_ANULACION" & vbCrLf
sConsulta = sConsulta & "from LOG_FACTURA L WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN LOG_GRAL WITH (NOLOCK)  on L.ID=log_GRAL.ID_TABLA" & vbCrLf
sConsulta = sConsulta & "WHERE LOG_GRAL.ID =@IDLOGGRAL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @MYID=SCOPE_IDENTITY()" & vbCrLf
sConsulta = sConsulta & "SET @LOG=@@IDENTITY" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO LOG_FACTURA_CD (ID_LOG_FACTURA,TIPO,DEN,VALOR,IMPORTE,OPERACION,DEF_ATRIB_ID,PLANIFICADO )" & vbCrLf
sConsulta = sConsulta & "SELECT @MYID,L.TIPO,DEN,VALOR,L.IMPORTE,OPERACION,DEF_ATRIB_ID,PLANIFICADO" & vbCrLf
sConsulta = sConsulta & "from LOG_FACTURA_CD   L WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN LOG_GRAL WITH (NOLOCK)  on L.ID_LOG_FACTURA=log_GRAL.ID_TABLA" & vbCrLf
sConsulta = sConsulta & "WHERE LOG_GRAL.ID =@IDLOGGRAL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO LOG_LINEA_FACTURA (ID_LOG_FACTURA,LINEA,ID_LINEA_PEDIDO,NUM,ALBARAN,TOTAL_COSTES,TOTAL_DCTOS,TOTAL_IMP,IMPORTE_SIN_IMP,IMPORTE,OBS)" & vbCrLf
sConsulta = sConsulta & "SELECT @MYID,LINEA,ID_LINEA_PEDIDO,L.NUM,ALBARAN,TOTAL_COSTES,TOTAL_DCTOS,TOTAL_IMP,IMPORTE_SIN_IMP,L.IMPORTE,L.OBS" & vbCrLf
sConsulta = sConsulta & "from LOG_LINEA_FACTURA   L WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN LOG_GRAL WITH (NOLOCK)  ON L.ID_LOG_FACTURA=log_GRAL.ID_TABLA" & vbCrLf
sConsulta = sConsulta & "WHERE LOG_GRAL.ID =@IDLOGGRAL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO LOG_LINEA_FACTURA_CD(ID_LOG_LINEA_FACTURA,TIPO,DEN,VALOR,IMPORTE,OPERACION,DEF_ATRIB_ID,PLANIFICADO)" & vbCrLf
sConsulta = sConsulta & "SELECT LLFN.ID ,L.TIPO,DEN,VALOR,L.IMPORTE,OPERACION,DEF_ATRIB_ID,PLANIFICADO" & vbCrLf
sConsulta = sConsulta & "from LOG_LINEA_FACTURA_CD   L WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN LOG_LINEA_FACTURA LLF  WITH(NOLOCK) ON L.ID_LOG_LINEA_FACTURA =LLF.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN LOG_GRAL WITH (NOLOCK)  ON LLF.ID_LOG_FACTURA=log_GRAL.ID_TABLA" & vbCrLf
sConsulta = sConsulta & "INNER JOIN LOG_LINEA_FACTURA LLFN WITH (NOLOCK) ON L.ID_LOG_LINEA_FACTURA =LLFN.ID" & vbCrLf
sConsulta = sConsulta & "WHERE LLFN.ID_LOG_FACTURA=@MYID AND LOG_GRAL.ID =@IDLOGGRAL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO LOG_FACTURA_IMPUESTOS (ID_LOG_LINEA_FACTURA,COD,DEN,VALOR,IMPORTE,RETENIDO,COMENT )" & vbCrLf
sConsulta = sConsulta & "SELECT ID_LOG_LINEA_FACTURA,COD,DEN,VALOR,L.IMPORTE,RETENIDO,COMENT" & vbCrLf
sConsulta = sConsulta & "from LOG_FACTURA_IMPUESTOS   L WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN LOG_LINEA_FACTURA LLF WITH(NOLOCK) ON L.ID_LOG_LINEA_FACTURA =LLF.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN LOG_GRAL WITH (NOLOCK)  ON LLF.ID_LOG_FACTURA=log_GRAL.ID_TABLA" & vbCrLf
sConsulta = sConsulta & "INNER JOIN LOG_LINEA_FACTURA LLFN WITH (NOLOCK) ON L.ID_LOG_LINEA_FACTURA =LLFN.ID" & vbCrLf
sConsulta = sConsulta & "WHERE LLFN.ID_LOG_FACTURA=@MYID AND LOG_GRAL.ID =@IDLOGGRAL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "IF @TABLA=33" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "--33 PAGOS" & vbCrLf
sConsulta = sConsulta & "INSERT INTO LOG_PAGO (ACCION,ESTADO,FECHA,NUM,FACTURA,ORIGEN,USU,ERP)" & vbCrLf
sConsulta = sConsulta & "SELECT ACCION,L.ESTADO,FECHA,NUM,FACTURA,L.ORIGEN,@USUARIO USU,L.ERP" & vbCrLf
sConsulta = sConsulta & "FROM LOG_PAGO L WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN LOG_GRAL WITH (NOLOCK)  on L.ID=log_GRAL.ID_TABLA" & vbCrLf
sConsulta = sConsulta & "WHERE LOG_GRAL.ID =@IDLOGGRAL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @MYID=SCOPE_IDENTITY()" & vbCrLf
sConsulta = sConsulta & "SET @LOG=@@IDENTITY" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "IF @TABLA=35" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "--35 CENTROS DE COSTE" & vbCrLf
sConsulta = sConsulta & "INSERT INTO LOG_CENTRO_SM (ACCION,CENTRO_SM,ORIGEN,USU,ERP)" & vbCrLf
sConsulta = sConsulta & "select ACCION,CENTRO_SM,L.ORIGEN,@USUARIO USU,L.ERP" & vbCrLf
sConsulta = sConsulta & "from LOG_CENTRO_SM L WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN LOG_GRAL WITH (NOLOCK)  on L.ID=log_GRAL.ID_TABLA" & vbCrLf
sConsulta = sConsulta & "WHERE LOG_GRAL.ID =@IDLOGGRAL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @MYID=SCOPE_IDENTITY()" & vbCrLf
sConsulta = sConsulta & "SET @LOG=@@IDENTITY" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO LOG_CENTRO_SM_DEN (ID_LOG_CENTRO_SM,IDI,DEN)" & vbCrLf
sConsulta = sConsulta & "SELECT @MYID,IDI,DEN" & vbCrLf
sConsulta = sConsulta & "from LOG_CENTRO_SM_DEN L WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN LOG_GRAL WITH (NOLOCK)  on L.ID_LOG_CENTRO_SM=log_GRAL.ID_TABLA" & vbCrLf
sConsulta = sConsulta & "WHERE LOG_GRAL.ID =@IDLOGGRAL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "IF @TABLA=36" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "--36 EMPRESAS" & vbCrLf
sConsulta = sConsulta & "INSERT INTO LOG_EMPRESA (ACCION,ID_EMPRESA,NIF,DEN,DIR,CP,POB,PAI,PROVI,SOCIEDAD,ORG_COMPRAS,GRP_COMPRAS,ORIGEN,USU,ERP,ID_LOG_UON,NUM_LIN_FACTURA,VENTANA1_DIA,VENTANA1_MES,VENTANA2_CARDINAL,VENTANA2_DIA_SEMANA,VENTANA2_MES,TOLERANCIA_IMP,TOLERANCIA_PORCEN)" & vbCrLf
sConsulta = sConsulta & "select ACCION,ID_EMPRESA,NIF,DEN,DIR,CP,POB,PAI,PROVI,SOCIEDAD,ORG_COMPRAS,GRP_COMPRAS,L.ORIGEN,@USUARIO USU,L.ERP,ID_LOG_UON,NUM_LIN_FACTURA,VENTANA1_DIA,VENTANA1_MES,VENTANA2_CARDINAL,VENTANA2_DIA_SEMANA,VENTANA2_MES,TOLERANCIA_IMP,TOLERANCIA_PORCEN" & vbCrLf
sConsulta = sConsulta & "from LOG_EMPRESA L WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN LOG_GRAL WITH (NOLOCK)  on L.ID=log_GRAL.ID_TABLA" & vbCrLf
sConsulta = sConsulta & "WHERE LOG_GRAL.ID =@IDLOGGRAL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @MYID=SCOPE_IDENTITY()" & vbCrLf
sConsulta = sConsulta & "SET @LOG=@@IDENTITY" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "IF @TABLA=101" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "--101 CONTACTOS" & vbCrLf
sConsulta = sConsulta & "INSERT INTO LOG_CON (ID_LOG_PROVE,ACCION,PROVE,ID_CON,APE,NOM,DEP,CAR,TFNO,TFNO2,TFNO_MOVIL,FAX,EMAIL,RECPET,APROV,SUBASTA,ORIGEN,USU,CALIDAD,ERP)" & vbCrLf
sConsulta = sConsulta & "select ID_LOG_PROVE,ACCION,PROVE,ID_CON,APE,NOM,DEP,CAR,TFNO,TFNO2,TFNO_MOVIL,FAX,EMAIL,RECPET,APROV,SUBASTA,L.ORIGEN,@USUARIO USU,CALIDAD,L.ERP" & vbCrLf
sConsulta = sConsulta & "from LOG_CON L WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN LOG_GRAL WITH (NOLOCK)  on L.ID=log_GRAL.ID_TABLA" & vbCrLf
sConsulta = sConsulta & "WHERE TABLA=101 AND LOG_GRAL.ID =@IDLOGGRAL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @MYID=SCOPE_IDENTITY()" & vbCrLf
sConsulta = sConsulta & "SET @LOG=@@IDENTITY" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDCONTACTO INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @IDCONTACTO=L.ID" & vbCrLf
sConsulta = sConsulta & "FROM LOG_CON L WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN LOG_GRAL WITH (NOLOCK)  on L.ID=log_GRAL.ID_TABLA" & vbCrLf
sConsulta = sConsulta & "WHERE LOG_GRAL.ID =@IDLOGGRAL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO LOG_PROVE (ID_LOG_CON,ACCION,COD,DEN,DIR,CP,POB,PAI,PROVI,MON,VAL1,VAL2,VAL3,CAL1,CAL2,CAL3,OBS,NIF,URLPROVE,PAG,PED_FORM,COD_NEW,ORIGEN,USU,EMP,ERP,TIPO,AUTOFACTURA)" & vbCrLf
sConsulta = sConsulta & "select ID_LOG_CON,ACCION,COD,DEN,DIR,CP,POB,PAI,PROVI,MON,VAL1,VAL2,VAL3,CAL1,CAL2,CAL3,OBS,NIF,URLPROVE,PAG,PED_FORM,COD_NEW,L.ORIGEN,@USUARIO USU,EMP,L.ERP,TIPO,AUTOFACTURA" & vbCrLf
sConsulta = sConsulta & "from LOG_PROVE L WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE ID_LOG_CON=@IDCONTACTO" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "IF @TABLA=102" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "--102 prove erp" & vbCrLf
sConsulta = sConsulta & "INSERT INTO LOG_PROVE_ERP (ACCION,COD_ERP,EMPRESA,DEN_ERP,NIF,CAMPO1,CAMPO2,CAMPO3,CAMPO4,BAJALOG,ERP,ORIGEN,USU,PAG,VIA_PAG)" & vbCrLf
sConsulta = sConsulta & "SELECT ACCION,L.COD_ERP,EMPRESA,DEN_ERP,NIF,CAMPO1,CAMPO2,CAMPO3,CAMPO4,BAJALOG,L.ERP,L.ORIGEN,@USUARIO USU,PAG,VIA_PAG" & vbCrLf
sConsulta = sConsulta & "from LOG_PROVE_ERP L WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN LOG_GRAL WITH (NOLOCK) ON L.ID=log_GRAL.ID_TABLA" & vbCrLf
sConsulta = sConsulta & "WHERE LOG_GRAL.ID =@IDLOGGRAL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @MYID=SCOPE_IDENTITY()" & vbCrLf
sConsulta = sConsulta & "SET @LOG=@@IDENTITY" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE LOG_GRAL SET REENVIO_INT=@IDLOGGRAL WHERE ID=@LOG" & vbCrLf
sConsulta = sConsulta & "SELECT @LOG AS 'IDLOGGRAL',@MYID as 'IDLOGTABLA'" & vbCrLf
sConsulta = sConsulta & "COMMIT TRAN" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_GETWORKFLOW_PROVE]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_GETWORKFLOW_PROVE]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_GETWORKFLOW_PROVE]" & vbCrLf
sConsulta = sConsulta & "   @PRV VARCHAR(50)," & vbCrLf
sConsulta = sConsulta & "   @IDI VARCHAR(50)," & vbCrLf
sConsulta = sConsulta & "   @TI INT=0," & vbCrLf
sConsulta = sConsulta & "   @FEC VARCHAR(10)=NULL," & vbCrLf
sConsulta = sConsulta & "   @INS INT=0," & vbCrLf
sConsulta = sConsulta & "   @FILAESTADO INT=0," & vbCrLf
sConsulta = sConsulta & "   @FORMATOFECHA VARCHAR(20)=''," & vbCrLf
sConsulta = sConsulta & "   @NOM_PORTAL NVARCHAR(50)=NULL" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   DECLARE @SQL AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "   DECLARE @SWHERE  AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "   DECLARE @SQLFROM AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "   DECLARE @SQLFROM1 AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "   DECLARE @SQLFROM2 AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "   DECLARE @SQLFROM3 AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "   DECLARE @SQLFROM4 AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "   DECLARE @SWHERE2  AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "   DECLARE @SQL2 AS NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "   DECLARE @TIPOFECHA INT" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   SET @TIPOFECHA=103" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @FORMATOFECHA='dd/mm/yyyy'" & vbCrLf
sConsulta = sConsulta & "       SET @TIPOFECHA=103" & vbCrLf
sConsulta = sConsulta & "   ELSE IF @FORMATOFECHA='dd/MM/yyyy'" & vbCrLf
sConsulta = sConsulta & "       SET @TIPOFECHA=103" & vbCrLf
sConsulta = sConsulta & "   ELSE IF @FORMATOFECHA='mm/dd/yyyy'" & vbCrLf
sConsulta = sConsulta & "       SET @TIPOFECHA=101" & vbCrLf
sConsulta = sConsulta & "   ELSE IF @FORMATOFECHA='dd-mm-yyyy'" & vbCrLf
sConsulta = sConsulta & "       SET @TIPOFECHA=105" & vbCrLf
sConsulta = sConsulta & "   ELSE IF @FORMATOFECHA='mm-dd-yyyy'" & vbCrLf
sConsulta = sConsulta & "       SET @TIPOFECHA=110" & vbCrLf
sConsulta = sConsulta & "   ELSE IF @FORMATOFECHA='mm.dd.yyyy'" & vbCrLf
sConsulta = sConsulta & "       SET @TIPOFECHA=104" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @SWHERE=N'WHERE TS.TIPO NOT IN (2,3,7,10,11,12,13,14) AND I.NUM IS NOT NULL '" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   IF NOT @TI=0" & vbCrLf
sConsulta = sConsulta & "       SET @SWHERE=@SWHERE+N'AND I.SOLICITUD=@TI '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @FEC IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "       SET @SWHERE=@SWHERE+N'AND I.FEC_ALTA>=CONVERT(DATETIME,@FEC,103) '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF NOT @INS=0" & vbCrLf
sConsulta = sConsulta & "       SET @SWHERE=@SWHERE+N'AND I.ID=@INS '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   --------------------------------------------PROCESOS QUE REQUIEREN LA INTERVENCI?N DEL USUARIO (trasladados m?s los propios del rol)----------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=N'SELECT COUNT(*) AS PENDIENTES '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL+N'FROM (SELECT DISTINCT I.ID '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL+N'FROM INSTANCIA I WITH(NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL+N'INNER JOIN SOLICITUD S WITH(NOLOCK) ON I.SOLICITUD=S.ID '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL+N'INNER JOIN TIPO_SOLICITUDES TS WITH(NOLOCK) ON S.TIPO=TS.ID '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL+N'INNER JOIN INSTANCIA_BLOQUE WITH(NOLOCK) ON I.ID=INSTANCIA_BLOQUE.INSTANCIA AND INSTANCIA_BLOQUE.ESTADO=1 AND NOT INSTANCIA_BLOQUE.BLOQ=1 AND INSTANCIA_BLOQUE.TRASLADADA=0 '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL+N'INNER JOIN PM_COPIA_ROL_BLOQUE WITH(NOLOCK) ON INSTANCIA_BLOQUE.BLOQUE=PM_COPIA_ROL_BLOQUE.BLOQUE '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL+N'INNER JOIN PM_COPIA_ROL WITH(NOLOCK) ON PM_COPIA_ROL_BLOQUE.ROL=PM_COPIA_ROL.ID '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL+N'INNER JOIN INSTANCIA_ROL IR WITH(NOLOCK) ON IR.ROL=PM_COPIA_ROL.ID AND IR.INSTANCIA=I.ID AND IR.PROVE=@PRV '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL+N'LEFT JOIN CONTRATO C WITH(NOLOCK) ON I.ID=C.INSTANCIA '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL+@SWHERE" & vbCrLf
sConsulta = sConsulta & "   IF @NOM_PORTAL IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'AND C.ID IS NULL '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'UNION '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'SELECT DISTINCT I.ID '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'FROM INSTANCIA I WITH(NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'INNER JOIN SOLICITUD S WITH(NOLOCK) ON I.SOLICITUD=S.ID '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'INNER JOIN TIPO_SOLICITUDES TS WITH(NOLOCK) ON S.TIPO=TS.ID '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'INNER JOIN INSTANCIA_BLOQUE WITH(NOLOCK) ON I.ID=INSTANCIA_BLOQUE.INSTANCIA AND INSTANCIA_BLOQUE.ESTADO=1 AND NOT INSTANCIA_BLOQUE.BLOQ=1 AND INSTANCIA_BLOQUE.TRASLADADA=0 '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'INNER JOIN PM_COPIA_ROL_BLOQUE WITH(NOLOCK) ON INSTANCIA_BLOQUE.BLOQUE=PM_COPIA_ROL_BLOQUE.BLOQUE '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'INNER JOIN PM_COPIA_ROL WITH(NOLOCK) ON PM_COPIA_ROL_BLOQUE.ROL=PM_COPIA_ROL.ID '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'INNER JOIN INSTANCIA_ROL IR WITH(NOLOCK) ON IR.ROL=PM_COPIA_ROL.ID AND IR.INSTANCIA=I.ID AND IR.PROVE=@PRV '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'INNER JOIN CONTRATO C WITH(NOLOCK) ON I.ID=C.INSTANCIA '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'INNER JOIN EMPRESA_PORTAL EP WITH(NOLOCK) ON EP.EMP=C.EMP AND EP.PORTAL=@NOM_PORTAL '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+@SWHERE" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL+N'UNION '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL+N'SELECT DISTINCT I.ID '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL+N'FROM INSTANCIA I WITH(NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL+N'INNER JOIN SOLICITUD S WITH(NOLOCK) ON I.SOLICITUD=S.ID '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL+N'INNER JOIN TIPO_SOLICITUDES TS WITH(NOLOCK) ON S.TIPO=TS.ID '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL+N'INNER JOIN INSTANCIA_BLOQUE WITH (NOLOCK) ON I.ID=INSTANCIA_BLOQUE.INSTANCIA AND INSTANCIA_BLOQUE.ESTADO=1 AND NOT INSTANCIA_BLOQUE.BLOQ=1 AND INSTANCIA_BLOQUE.TRASLADADA=1 '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL+N'INNER JOIN (SELECT MAX(ID) ID,BLOQUE '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL+N'FROM INSTANCIA_EST '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL+N'GROUP BY BLOQUE) M ON INSTANCIA_BLOQUE.ID=M.BLOQUE '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL+N'INNER JOIN INSTANCIA_EST WITH(NOLOCK) ON M.ID=INSTANCIA_EST.ID AND INSTANCIA_EST.DESTINATARIO_PROV=@PRV '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL+N'INNER JOIN PM_COPIA_ROL R WITH(NOLOCK) ON INSTANCIA_EST.ROL=R.ID '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL+N'LEFT JOIN CONTRATO C WITH(NOLOCK) ON I.ID=C.INSTANCIA '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL+@SWHERE+N'AND (I.ESTADO=2 OR I.ESTADO=3 OR I.ESTADO=4) '" & vbCrLf
sConsulta = sConsulta & "   IF @NOM_PORTAL IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'AND C.ID IS NULL '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'UNION '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'SELECT DISTINCT I.ID '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'FROM INSTANCIA I WITH(NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'INNER JOIN SOLICITUD S WITH(NOLOCK) ON I.SOLICITUD=S.ID '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'INNER JOIN TIPO_SOLICITUDES TS WITH(NOLOCK) ON S.TIPO=TS.ID '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'INNER JOIN INSTANCIA_BLOQUE WITH (NOLOCK) ON I.ID=INSTANCIA_BLOQUE.INSTANCIA AND INSTANCIA_BLOQUE.ESTADO=1 AND NOT INSTANCIA_BLOQUE.BLOQ=1 AND INSTANCIA_BLOQUE.TRASLADADA=1 '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'INNER JOIN (SELECT MAX(ID) ID,BLOQUE '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'FROM INSTANCIA_EST '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'GROUP BY BLOQUE) M ON INSTANCIA_BLOQUE.ID=M.BLOQUE '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'INNER JOIN INSTANCIA_EST WITH(NOLOCK) ON M.ID=INSTANCIA_EST.ID AND INSTANCIA_EST.DESTINATARIO_PROV=@PRV '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'INNER JOIN PM_COPIA_ROL R WITH(NOLOCK) ON INSTANCIA_EST.ROL=R.ID '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'INNER JOIN CONTRATO C WITH(NOLOCK) ON I.ID=C.INSTANCIA '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'INNER JOIN EMPRESA_PORTAL EP WITH(NOLOCK) ON EP.EMP=C.EMP AND EP.PORTAL=@NOM_PORTAL '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+@SWHERE+N'AND (I.ESTADO=2 OR I.ESTADO=3 OR I.ESTADO=4) '" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL+N') SUMA1 '" & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL,N'@PRV VARCHAR(50),@TI INT,@FEC VARCHAR(10),@INS INT,@NOM_PORTAL NVARCHAR(50)',@PRV=@PRV,@TI=@TI,@FEC=@FEC,@INS=@INS,@NOM_PORTAL=@NOM_PORTAL" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   ----------------------------------------------------PROCESOS EN LOS QUE PARTICIP? EL USUARIO--------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "   -- 2.- En curso" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = N'SELECT COUNT(*) AS EN_CURSO '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL+N'FROM (SELECT DISTINCT I.ID '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL+N'FROM INSTANCIA_EST WITH(NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL+N'INNER JOIN INSTANCIA I WITH(NOLOCK) ON INSTANCIA_EST.INSTANCIA=I.ID AND INSTANCIA_EST.PROVE=@PRV AND INSTANCIA_EST.BLOQUE IS NOT NULL AND (I.ESTADO=2 OR I.ESTADO=3 OR I.ESTADO=4) '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL+N'INNER JOIN SOLICITUD S WITH(NOLOCK) ON I.SOLICITUD=S.ID '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL+N'INNER JOIN TIPO_SOLICITUDES TS WITH(NOLOCK) ON S.TIPO=TS.ID '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL+N'INNER JOIN PM_COPIA_ROL R WITH(NOLOCK) ON INSTANCIA_EST.ROL=R.ID '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL+N'LEFT JOIN CONTRATO C WITH(NOLOCK) ON I.ID=C.INSTANCIA '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL+@SWHERE" & vbCrLf
sConsulta = sConsulta & "   IF @NOM_PORTAL IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'AND C.ID IS NULL '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'UNION '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'SELECT DISTINCT I.ID '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'FROM INSTANCIA_EST WITH(NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'INNER JOIN INSTANCIA I WITH(NOLOCK) ON INSTANCIA_EST.INSTANCIA=I.ID AND INSTANCIA_EST.PROVE=@PRV AND INSTANCIA_EST.BLOQUE IS NOT NULL AND (I.ESTADO=2 OR I.ESTADO=3 OR I.ESTADO=4) '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'INNER JOIN SOLICITUD S WITH(NOLOCK) ON I.SOLICITUD=S.ID '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'INNER JOIN TIPO_SOLICITUDES TS WITH(NOLOCK) ON S.TIPO=TS.ID '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'INNER JOIN PM_COPIA_ROL R WITH(NOLOCK) ON INSTANCIA_EST.ROL=R.ID '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'INNER JOIN CONTRATO C WITH(NOLOCK) ON I.ID=C.INSTANCIA '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'INNER JOIN EMPRESA_PORTAL EP WITH(NOLOCK) ON EP.EMP=C.EMP AND EP.PORTAL=@NOM_PORTAL '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+@SWHERE" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL+N') SUMA2 '" & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL,N'@PRV VARCHAR(50),@TI INT,@FEC VARCHAR(10),@INS INT,@NOM_PORTAL NVARCHAR(50)',@PRV=@PRV,@TI=@TI,@FEC=@FEC,@INS=@INS,@NOM_PORTAL=@NOM_PORTAL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   -- 3.- Rechazadas" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = N'SELECT COUNT(*) AS RECHAZADAS '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL+N'FROM (SELECT DISTINCT I.ID '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL+N'FROM INSTANCIA_EST WITH(NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL+N'INNER JOIN INSTANCIA I WITH(NOLOCK) ON INSTANCIA_EST.INSTANCIA=I.ID AND INSTANCIA_EST.PROVE=@PRV AND INSTANCIA_EST.BLOQUE IS NOT NULL AND I.ESTADO=6 '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL+N'INNER JOIN SOLICITUD S WITH(NOLOCK) ON I.SOLICITUD=S.ID '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL+N'INNER JOIN TIPO_SOLICITUDES TS WITH(NOLOCK) ON S.TIPO=TS.ID '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL+N'INNER JOIN PM_COPIA_ROL R WITH(NOLOCK) ON INSTANCIA_EST.ROL=R.ID '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL+N'LEFT JOIN CONTRATO C WITH(NOLOCK) ON I.ID=C.INSTANCIA '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL+@SWHERE" & vbCrLf
sConsulta = sConsulta & "   IF @NOM_PORTAL IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'AND C.ID IS NULL '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'UNION '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'SELECT DISTINCT I.ID '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'FROM INSTANCIA_EST WITH(NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'INNER JOIN INSTANCIA I WITH(NOLOCK) ON INSTANCIA_EST.INSTANCIA=I.ID AND INSTANCIA_EST.PROVE=@PRV AND INSTANCIA_EST.BLOQUE IS NOT NULL AND I.ESTADO=6 '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'INNER JOIN SOLICITUD S WITH(NOLOCK) ON I.SOLICITUD=S.ID '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'INNER JOIN TIPO_SOLICITUDES TS WITH(NOLOCK) ON S.TIPO=TS.ID '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'INNER JOIN PM_COPIA_ROL R WITH(NOLOCK) ON INSTANCIA_EST.ROL=R.ID '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'INNER JOIN CONTRATO C WITH(NOLOCK) ON I.ID=C.INSTANCIA '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'INNER JOIN EMPRESA_PORTAL EP WITH(NOLOCK) ON EP.EMP=C.EMP AND EP.PORTAL=@NOM_PORTAL '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+@SWHERE" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL+N') SUMA3 '" & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL,N'@PRV VARCHAR(50),@TI INT,@FEC VARCHAR(10),@INS INT,@NOM_PORTAL NVARCHAR(50)',@PRV=@PRV,@TI=@TI,@FEC=@FEC,@INS=@INS,@NOM_PORTAL=@NOM_PORTAL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   -- 4.- Anuladas" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = N'SELECT COUNT(*) AS ANULADAS '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL+N'FROM (SELECT DISTINCT I.ID '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL+N'FROM INSTANCIA_EST WITH(NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL+N'INNER JOIN INSTANCIA I  WITH(NOLOCK) ON INSTANCIA_EST.INSTANCIA=I.ID AND INSTANCIA_EST.PROVE=@PRV AND INSTANCIA_EST.BLOQUE IS NOT NULL AND I.ESTADO=8 '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL+N'INNER JOIN SOLICITUD S WITH(NOLOCK) ON I.SOLICITUD=S.ID '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL+N'INNER JOIN TIPO_SOLICITUDES TS WITH(NOLOCK) ON S.TIPO=TS.ID '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL+N'INNER JOIN PM_COPIA_ROL R WITH(NOLOCK) ON INSTANCIA_EST.ROL=R.ID '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL+N'LEFT JOIN CONTRATO C WITH(NOLOCK) ON I.ID=C.INSTANCIA '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL+@SWHERE" & vbCrLf
sConsulta = sConsulta & "   IF @NOM_PORTAL IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'AND C.ID IS NULL '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'UNION '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'SELECT DISTINCT I.ID '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'FROM INSTANCIA_EST WITH(NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'INNER JOIN INSTANCIA I  WITH(NOLOCK) ON INSTANCIA_EST.INSTANCIA=I.ID AND INSTANCIA_EST.PROVE=@PRV AND INSTANCIA_EST.BLOQUE IS NOT NULL AND I.ESTADO=8 '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'INNER JOIN SOLICITUD S WITH(NOLOCK) ON I.SOLICITUD=S.ID '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'INNER JOIN TIPO_SOLICITUDES TS WITH(NOLOCK) ON S.TIPO=TS.ID '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'INNER JOIN PM_COPIA_ROL R WITH(NOLOCK) ON INSTANCIA_EST.ROL=R.ID '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'INNER JOIN CONTRATO C WITH(NOLOCK) ON I.ID=C.INSTANCIA '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'INNER JOIN EMPRESA_PORTAL EP WITH(NOLOCK) ON EP.EMP=C.EMP AND EP.PORTAL=@NOM_PORTAL '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+@SWHERE" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL+N') SUMA4 '" & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL,N'@PRV VARCHAR(50),@TI INT,@FEC VARCHAR(10),@INS INT,@NOM_PORTAL NVARCHAR(50)',@PRV=@PRV,@TI=@TI,@FEC=@FEC,@INS=@INS,@NOM_PORTAL=@NOM_PORTAL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   -- 4.- Finalizadas" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = N'SELECT COUNT(*) AS FINALIZADAS '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL+N'FROM (SELECT DISTINCT I.ID '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL+N'FROM INSTANCIA_EST WITH(NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL+N'INNER JOIN INSTANCIA I WITH(NOLOCK) ON INSTANCIA_EST.INSTANCIA=I.ID AND INSTANCIA_EST.PROVE=@PRV AND INSTANCIA_EST.BLOQUE IS NOT NULL  AND I.ESTADO>=100 '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL+N'INNER JOIN SOLICITUD S WITH(NOLOCK) ON I.SOLICITUD=S.ID '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL+N'INNER JOIN TIPO_SOLICITUDES TS WITH(NOLOCK) ON S.TIPO=TS.ID '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL+N'INNER JOIN PM_COPIA_ROL R WITH(NOLOCK) ON INSTANCIA_EST.ROL=R.ID '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL+N'LEFT JOIN CONTRATO C WITH(NOLOCK) ON I.ID=C.INSTANCIA '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL+@SWHERE" & vbCrLf
sConsulta = sConsulta & "   IF @NOM_PORTAL IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'AND C.ID IS NULL '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'UNION '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'SELECT DISTINCT I.ID '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'FROM INSTANCIA_EST WITH(NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'INNER JOIN INSTANCIA I WITH(NOLOCK) ON INSTANCIA_EST.INSTANCIA=I.ID AND INSTANCIA_EST.PROVE=@PRV AND INSTANCIA_EST.BLOQUE IS NOT NULL  AND I.ESTADO>=100 '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'INNER JOIN SOLICITUD S WITH(NOLOCK) ON I.SOLICITUD=S.ID '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'INNER JOIN TIPO_SOLICITUDES TS WITH(NOLOCK) ON S.TIPO=TS.ID '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'INNER JOIN PM_COPIA_ROL R WITH(NOLOCK) ON INSTANCIA_EST.ROL=R.ID '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'INNER JOIN CONTRATO C WITH(NOLOCK) ON I.ID=C.INSTANCIA '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'INNER JOIN EMPRESA_PORTAL EP WITH(NOLOCK) ON EP.EMP=C.EMP AND EP.PORTAL=@NOM_PORTAL '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+@SWHERE" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL+N') SUMA5 '" & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL,N'@PRV VARCHAR(50),@TI INT,@FEC VARCHAR(10),@INS INT,@NOM_PORTAL NVARCHAR(50)',@PRV=@PRV,@TI=@TI,@FEC=@FEC,@INS=@INS,@NOM_PORTAL=@NOM_PORTAL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   ----------------------------------------Devuelve las instancias que cumplen las condiciones:--------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "   --T?tulo: campo normal o atributo" & vbCrLf
sConsulta = sConsulta & "   SET @SQLFROM2=N'SELECT DISTINCT S.ID ID_SOLIC,S.COD,CASE WHEN C1.TITULO_FEC IS NULL THEN C1.TITULO_TEXT ELSE CONVERT(VARCHAR,C1.TITULO_FEC,@TIPOFECHA) END DESCR,'" & vbCrLf
sConsulta = sConsulta & "   SET @SQLFROM2=@SQLFROM2+N'I.ID ID_INS,IE.FECHA,CASE ISNULL(A.NUM,0) WHEN 0 THEN '''' WHEN 1 THEN IDM.ART4 ELSE ''##'' END ITEM,CASE WHEN IE.PER IS NOT NULL THEN E.NOM+'' ''+E.APE ELSE PR.DEN END NOM_USU,D.DEN ORIGEN,I.ESTADO,'" & vbCrLf
sConsulta = sConsulta & "   SET @SQLFROM2=@SQLFROM2+N'CASE WHEN M.PARALELO>1 THEN NULL ELSE ISNULL(CB.DEN,'' '') END DENETAPA_ACTUAL,R.VER_DETALLE_PER,I.EN_PROCESO,TS.TIPO,C.ID CONTRATO,C.CODIGO CODIGO_CONTRATO '" & vbCrLf
sConsulta = sConsulta & "   SET @SQLFROM2=@SQLFROM2+N'FROM INSTANCIA I WITH(NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "   SET @SQLFROM2=@SQLFROM2+N'INNER JOIN SOLICITUD S WITH(NOLOCK) ON I.SOLICITUD=S.ID '" & vbCrLf
sConsulta = sConsulta & "   SET @SQLFROM2=@SQLFROM2+N'INNER JOIN TIPO_SOLICITUDES TS WITH(NOLOCK) ON S.TIPO=TS.ID ' " & vbCrLf
sConsulta = sConsulta & "   SET @SQLFROM2=@SQLFROM2+N'INNER JOIN (SELECT INSTANCIA,COUNT(*) PARALELO,MIN(BLOQUE) BLOQUE '" & vbCrLf
sConsulta = sConsulta & "       SET @SQLFROM2=@SQLFROM2+N'FROM INSTANCIA_BLOQUE WITH(NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "       SET @SQLFROM2=@SQLFROM2+N'WHERE ESTADO=1 AND NOT BLOQ=1 '" & vbCrLf
sConsulta = sConsulta & "       SET @SQLFROM2=@SQLFROM2+N'GROUP BY INSTANCIA) M ON I.ID=M.INSTANCIA '" & vbCrLf
sConsulta = sConsulta & "   SET @SQLFROM2=@SQLFROM2+N'LEFT JOIN INSTANCIA_VISORES_PM C1 WITH(NOLOCK) ON C1.INSTANCIA=I.ID '" & vbCrLf
sConsulta = sConsulta & "   SET @SQLFROM2=@SQLFROM2+N'LEFT JOIN (SELECT INSTANCIA,COUNT(ID) NUM FROM INSTANCIA_DESGLOSE_MATERIAL WITH(NOLOCK) WHERE NOT ART4 IS NULL GROUP BY INSTANCIA) A ON A.INSTANCIA=I.ID '" & vbCrLf
sConsulta = sConsulta & "   SET @SQLFROM2=@SQLFROM2+N'LEFT JOIN INSTANCIA_DESGLOSE_MATERIAL IDM WITH (NOLOCK) ON IDM.INSTANCIA=A.INSTANCIA AND IDM.ART4 IS NOT NULL '" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   IF @FILAESTADO=0 --Pendientes:" & vbCrLf
sConsulta = sConsulta & "   BEGIN       " & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQLFROM2+N'LEFT JOIN CONTRATO C WITH(NOLOCK) ON I.ID=C.INSTANCIA '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'INNER JOIN INSTANCIA_BLOQUE IB WITH(NOLOCK) ON M.INSTANCIA=IB.INSTANCIA AND IB.ESTADO=1 AND NOT IB.BLOQ=1 AND IB.TRASLADADA=0 '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'INNER JOIN PM_COPIA_ROL_BLOQUE CRB WITH(NOLOCK) ON IB.BLOQUE=CRB.BLOQUE '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'INNER JOIN PM_COPIA_ROL R WITH(NOLOCK) ON CRB.ROL=R.ID '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'INNER JOIN INSTANCIA_ROL IR WITH(NOLOCK) ON IR.INSTANCIA=IB.INSTANCIA AND IR.ROL=R.ID AND IR.PROVE=@PRV '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'INNER JOIN INSTANCIA_CAMINO WITH(NOLOCK) ON INSTANCIA_BLOQUE_DESTINO=IB.ID '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'INNER JOIN INSTANCIA_EST IE WITH(NOLOCK) ON I.INSTANCIA_EST=IE.ID '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'INNER JOIN PM_COPIA_BLOQUE_DEN CB WITH(NOLOCK) ON IB.BLOQUE=CB.BLOQUE AND CB.IDI= @IDI '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'LEFT JOIN PROVE PR WITH(NOLOCK) ON IE.PROVE=PR.COD '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'LEFT JOIN PER E WITH(NOLOCK) ON IE.PER=E.COD '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'LEFT JOIN DEP D WITH(NOLOCK) ON E.DEP=D.COD '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+@SWHERE+N'AND (I.ESTADO=2 OR I.ESTADO=3 OR I.ESTADO=4) '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       IF @NOM_PORTAL IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           SET @SQL=@SQL+N'AND C.ID IS NULL '" & vbCrLf
sConsulta = sConsulta & "           SET @SQL=@SQL+N'UNION '+@SQLFROM2" & vbCrLf
sConsulta = sConsulta & "           SET @SQL=@SQL+N'INNER JOIN CONTRATO C WITH(NOLOCK) ON I.ID=C.INSTANCIA '" & vbCrLf
sConsulta = sConsulta & "           SET @SQL=@SQL+N'INNER JOIN EMPRESA_PORTAL EP WITH(NOLOCK) ON EP.EMP=C.EMP AND EP.PORTAL=@NOM_PORTAL '" & vbCrLf
sConsulta = sConsulta & "           SET @SQL=@SQL+N'INNER JOIN INSTANCIA_BLOQUE IB WITH(NOLOCK) ON M.INSTANCIA=IB.INSTANCIA AND IB.ESTADO=1 AND NOT IB.BLOQ=1 AND IB.TRASLADADA=0 '" & vbCrLf
sConsulta = sConsulta & "           SET @SQL=@SQL+N'INNER JOIN PM_COPIA_ROL_BLOQUE CRB WITH(NOLOCK) ON IB.BLOQUE=CRB.BLOQUE '" & vbCrLf
sConsulta = sConsulta & "           SET @SQL=@SQL+N'INNER JOIN PM_COPIA_ROL R WITH(NOLOCK) ON CRB.ROL=R.ID '" & vbCrLf
sConsulta = sConsulta & "           SET @SQL=@SQL+N'INNER JOIN INSTANCIA_ROL IR WITH(NOLOCK) ON IR.INSTANCIA=IB.INSTANCIA AND IR.ROL=R.ID AND IR.PROVE=@PRV '" & vbCrLf
sConsulta = sConsulta & "           SET @SQL=@SQL+N'INNER JOIN INSTANCIA_CAMINO WITH(NOLOCK) ON INSTANCIA_BLOQUE_DESTINO=IB.ID '" & vbCrLf
sConsulta = sConsulta & "           SET @SQL=@SQL+N'INNER JOIN INSTANCIA_EST IE WITH(NOLOCK) ON I.INSTANCIA_EST=IE.ID '" & vbCrLf
sConsulta = sConsulta & "           SET @SQL=@SQL+N'INNER JOIN PM_COPIA_BLOQUE_DEN CB WITH(NOLOCK) ON IB.BLOQUE=CB.BLOQUE AND CB.IDI= @IDI '" & vbCrLf
sConsulta = sConsulta & "           SET @SQL=@SQL+N'LEFT JOIN PROVE PR WITH(NOLOCK) ON IE.PROVE=PR.COD '" & vbCrLf
sConsulta = sConsulta & "           SET @SQL=@SQL+N'LEFT JOIN PER E WITH(NOLOCK) ON IE.PER=E.COD '" & vbCrLf
sConsulta = sConsulta & "           SET @SQL=@SQL+N'LEFT JOIN DEP D WITH(NOLOCK) ON E.DEP=D.COD '" & vbCrLf
sConsulta = sConsulta & "           SET @SQL=@SQL+@SWHERE+N'AND (I.ESTADO=2 OR I.ESTADO=3 OR I.ESTADO=4) '" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'UNION '+@SQLFROM2" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'LEFT JOIN CONTRATO C WITH(NOLOCK) ON I.ID=C.INSTANCIA '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'INNER JOIN INSTANCIA_BLOQUE IB WITH(NOLOCK) ON M.INSTANCIA=IB.INSTANCIA AND IB.ESTADO=1 AND NOT IB.BLOQ=1 AND IB.TRASLADADA=1 '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'INNER JOIN(SELECT MAX(ID) ID,BLOQUE '" & vbCrLf
sConsulta = sConsulta & "           SET @SQL=@SQL+N'FROM INSTANCIA_EST '" & vbCrLf
sConsulta = sConsulta & "           SET @SQL=@SQL+N'GROUP BY BLOQUE) P ON IB.ID=P.BLOQUE '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'INNER JOIN INSTANCIA_EST IE WITH(NOLOCK) ON P.ID=IE.ID AND IE.DESTINATARIO_PROV=@PRV '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'INNER JOIN PM_COPIA_ROL R WITH(NOLOCK) ON IE.ROL=R.ID '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'INNER JOIN PM_COPIA_BLOQUE_DEN CB WITH(NOLOCK) ON IB.BLOQUE=CB.BLOQUE AND CB.IDI=@IDI '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'LEFT JOIN PROVE PR WITH(NOLOCK) ON IE.PROVE=PR.COD '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'LEFT JOIN PER E WITH(NOLOCK) ON IE.PER=E.COD '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'LEFT JOIN DEP D WITH(NOLOCK) ON E.DEP=D.COD '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+@SWHERE+'AND (I.ESTADO=2 OR I.ESTADO=3 OR I.ESTADO=4) '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       IF @NOM_PORTAL IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           SET @SQL=@SQL+N'AND C.ID IS NULL '" & vbCrLf
sConsulta = sConsulta & "           SET @SQL=@SQL+N'UNION '+@SQLFROM2" & vbCrLf
sConsulta = sConsulta & "           SET @SQL=@SQL+N'INNER JOIN CONTRATO C WITH(NOLOCK) ON I.ID=C.INSTANCIA '" & vbCrLf
sConsulta = sConsulta & "           SET @SQL=@SQL+N'INNER JOIN EMPRESA_PORTAL EP WITH(NOLOCK) ON EP.EMP=C.EMP AND EP.PORTAL=@NOM_PORTAL '" & vbCrLf
sConsulta = sConsulta & "           SET @SQL=@SQL+N'INNER JOIN INSTANCIA_BLOQUE IB WITH(NOLOCK) ON M.INSTANCIA=IB.INSTANCIA AND IB.ESTADO=1 AND NOT IB.BLOQ=1 AND IB.TRASLADADA=1 '" & vbCrLf
sConsulta = sConsulta & "           SET @SQL=@SQL+N'INNER JOIN(SELECT MAX(ID) ID,BLOQUE '" & vbCrLf
sConsulta = sConsulta & "               SET @SQL=@SQL+N'FROM INSTANCIA_EST '" & vbCrLf
sConsulta = sConsulta & "               SET @SQL=@SQL+N'GROUP BY BLOQUE) P ON IB.ID=P.BLOQUE '" & vbCrLf
sConsulta = sConsulta & "           SET @SQL=@SQL+N'INNER JOIN INSTANCIA_EST IE WITH(NOLOCK) ON P.ID=IE.ID AND IE.DESTINATARIO_PROV=@PRV '" & vbCrLf
sConsulta = sConsulta & "           SET @SQL=@SQL+N'INNER JOIN PM_COPIA_ROL R WITH(NOLOCK) ON IE.ROL=R.ID '" & vbCrLf
sConsulta = sConsulta & "           SET @SQL=@SQL+N'INNER JOIN PM_COPIA_BLOQUE_DEN CB WITH(NOLOCK) ON IB.BLOQUE=CB.BLOQUE AND CB.IDI=@IDI '" & vbCrLf
sConsulta = sConsulta & "           SET @SQL=@SQL+N'LEFT JOIN PROVE PR WITH(NOLOCK) ON IE.PROVE=PR.COD '" & vbCrLf
sConsulta = sConsulta & "           SET @SQL=@SQL+N'LEFT JOIN PER E WITH(NOLOCK) ON IE.PER=E.COD '" & vbCrLf
sConsulta = sConsulta & "           SET @SQL=@SQL+N'LEFT JOIN DEP D WITH(NOLOCK) ON E.DEP=D.COD '" & vbCrLf
sConsulta = sConsulta & "           SET @SQL=@SQL+@SWHERE+'AND (I.ESTADO=2 OR I.ESTADO=3 OR I.ESTADO=4) AND C.ID IS NOT NULL '" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'ORDER BY I.ID '" & vbCrLf
sConsulta = sConsulta & "       EXEC SP_EXECUTESQL @SQL,N'@PRV VARCHAR(50),@IDI VARCHAR(20),@TI INT,@FEC VARCHAR(10),@INS INT,@TIPOFECHA INT,@NOM_PORTAL NVARCHAR(50)'," & vbCrLf
sConsulta = sConsulta & "       @PRV=@PRV,@IDI=@IDI,@TI=@TI,@FEC=@FEC,@INS=@INS,@TIPOFECHA=@TIPOFECHA,@NOM_PORTAL=@NOM_PORTAL" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       IF  @FILAESTADO=2   --2 Rechazadas" & vbCrLf
sConsulta = sConsulta & "           SET @SWHERE=@SWHERE+N'AND I.ESTADO=6 '" & vbCrLf
sConsulta = sConsulta & "       ELSE        " & vbCrLf
sConsulta = sConsulta & "           IF  @FILAESTADO=3   --3 Anuladas" & vbCrLf
sConsulta = sConsulta & "               SET @SWHERE=@SWHERE+N'AND I.ESTADO=8 '" & vbCrLf
sConsulta = sConsulta & "           ELSE            " & vbCrLf
sConsulta = sConsulta & "               IF  @FILAESTADO=4   ---4 finalizadas" & vbCrLf
sConsulta = sConsulta & "                   SET @SWHERE=@SWHERE+N'AND I.ESTADO>=100 '" & vbCrLf
sConsulta = sConsulta & "               ELSE  --1 En curso" & vbCrLf
sConsulta = sConsulta & "                   SET @SWHERE=@SWHERE+N'AND (I.ESTADO=2 OR I.ESTADO=3 OR I.ESTADO=4) '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=N'INNER JOIN INSTANCIA_BLOQUE IB  WITH(NOLOCK) ON M.INSTANCIA=IB.INSTANCIA AND IB.ESTADO=1 AND NOT IB.BLOQ=1 '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'LEFT JOIN PM_COPIA_BLOQUE_DEN CB WITH(NOLOCK) ON IB.BLOQUE=CB.BLOQUE AND CB.IDI=@IDI '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'INNER JOIN INSTANCIA_EST IE WITH(NOLOCK) ON I.ID=IE.INSTANCIA AND IE.PROVE=@PRV AND IE.BLOQUE IS NOT NULL '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'AND IE.ID=(SELECT MAX(ID) FROM INSTANCIA_EST WITH(NOLOCK) WHERE INSTANCIA_EST.INSTANCIA=I.ID AND INSTANCIA_EST.PROVE=@PRV AND INSTANCIA_EST.BLOQUE IS NOT NULL) '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'INNER JOIN PM_COPIA_ROL R WITH(NOLOCK) ON IE.ROL=R.ID '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'LEFT JOIN PROVE PR WITH(NOLOCK) ON IE.PROVE=PR.COD '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'LEFT JOIN PER E WITH(NOLOCK) ON IE.PER=E.COD '" & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N'LEFT JOIN DEP D WITH(NOLOCK) ON E.DEP=D.COD '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       IF @NOM_PORTAL IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           DECLARE @SQLEMPRESA NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "           SET @SQLEMPRESA=@SQLFROM2+@SQL+N'LEFT JOIN CONTRATO C WITH(NOLOCK) ON I.ID=C.INSTANCIA '+@SWHERE" & vbCrLf
sConsulta = sConsulta & "           SET @SQLEMPRESA=@SQLEMPRESA+N'AND C.ID IS NULL '" & vbCrLf
sConsulta = sConsulta & "           SET @SQLEMPRESA=@SQLEMPRESA+N'UNION '" & vbCrLf
sConsulta = sConsulta & "           SET @SQLEMPRESA=@SQLEMPRESA+@SQLFROM2+@SQL+N'INNER JOIN CONTRATO C WITH(NOLOCK) ON I.ID=C.INSTANCIA '" & vbCrLf
sConsulta = sConsulta & "           SET @SQLEMPRESA=@SQLEMPRESA+N'INNER JOIN EMPRESA_PORTAL EP WITH(NOLOCK) ON EP.EMP=C.EMP AND EP.PORTAL=@NOM_PORTAL '" & vbCrLf
sConsulta = sConsulta & "           SET @SQLEMPRESA=@SQLEMPRESA+@SWHERE+N'AND C.ID IS NOT NULL '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "           SET @SQLFROM2=@SQLEMPRESA" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "       ELSE" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           SET @SQLFROM2=@SQLFROM2+@SQL+N'LEFT JOIN CONTRATO C WITH(NOLOCK) ON I.ID=C.INSTANCIA '+@SWHERE" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "       EXEC SP_EXECUTESQL @SQLFROM2,N'@PRV VARCHAR(50),@IDI VARCHAR(20),@TI INT,@FEC VARCHAR(10),@INS INT,@TIPOFECHA INT,@NOM_PORTAL NVARCHAR(50)'," & vbCrLf
sConsulta = sConsulta & "       @PRV=@PRV,@IDI=@IDI,@TI=@TI,@FEC=@FEC,@INS=@INS,@TIPOFECHA=@TIPOFECHA,@NOM_PORTAL=@NOM_PORTAL" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_GET_SOLICITUDES_PDTES_PROVE]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_GET_SOLICITUDES_PDTES_PROVE]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_GET_SOLICITUDES_PDTES_PROVE]" & vbCrLf
sConsulta = sConsulta & "   @PRV VARCHAR(50)," & vbCrLf
sConsulta = sConsulta & "   @NOM_PORTAL NVARCHAR(50)=NULL" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SELECT COUNT(*) AS PENDIENTES FROM (" & vbCrLf
sConsulta = sConsulta & "       SELECT DISTINCT I.ID" & vbCrLf
sConsulta = sConsulta & "       FROM INSTANCIA I WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN INSTANCIA_BLOQUE WITH(NOLOCK) ON I.ID=INSTANCIA_BLOQUE.INSTANCIA AND INSTANCIA_BLOQUE.ESTADO=1 AND INSTANCIA_BLOQUE.BLOQ<>1 AND INSTANCIA_BLOQUE.TRASLADADA=0" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PM_COPIA_ROL_BLOQUE WITH(NOLOCK) ON INSTANCIA_BLOQUE.BLOQUE=PM_COPIA_ROL_BLOQUE.BLOQUE" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN INSTANCIA_ROL IR WITH(NOLOCK) ON IR.INSTANCIA=I.ID AND PM_COPIA_ROL_BLOQUE.ROL=IR.ROL AND IR.PROVE=@PRV" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN SOLICITUD S WITH(NOLOCK) ON I.SOLICITUD=S.ID" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN TIPO_SOLICITUDES TS WITH(NOLOCK) ON S.TIPO=TS.ID" & vbCrLf
sConsulta = sConsulta & "       WHERE I.NUM IS NOT NULL AND TS.TIPO IN(0,1,4,6,8,9)" & vbCrLf
sConsulta = sConsulta & "       UNION" & vbCrLf
sConsulta = sConsulta & "       SELECT DISTINCT I.ID" & vbCrLf
sConsulta = sConsulta & "       FROM INSTANCIA I WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN INSTANCIA_BLOQUE WITH(NOLOCK) ON I.ID=INSTANCIA_BLOQUE.INSTANCIA AND INSTANCIA_BLOQUE.ESTADO=1 AND INSTANCIA_BLOQUE.BLOQ<>1 AND INSTANCIA_BLOQUE.TRASLADADA=0" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PM_COPIA_ROL_BLOQUE WITH(NOLOCK) ON INSTANCIA_BLOQUE.BLOQUE=PM_COPIA_ROL_BLOQUE.BLOQUE" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN INSTANCIA_ROL IR WITH(NOLOCK) ON IR.INSTANCIA=I.ID AND PM_COPIA_ROL_BLOQUE.ROL=IR.ROL AND IR.PROVE=@PRV" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN SOLICITUD S WITH(NOLOCK) ON I.SOLICITUD=S.ID" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN TIPO_SOLICITUDES TS WITH(NOLOCK) ON S.TIPO=TS.ID" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN CONTRATO C WITH(NOLOCK) ON C.INSTANCIA=I.ID" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN EMPRESA_PORTAL E WITH(NOLOCK) ON (E.EMP=C.EMP AND E.PORTAL=@NOM_PORTAL) OR @NOM_PORTAL IS NULL" & vbCrLf
sConsulta = sConsulta & "       WHERE I.NUM IS NOT NULL AND TS.TIPO=5" & vbCrLf
sConsulta = sConsulta & "       UNION" & vbCrLf
sConsulta = sConsulta & "       SELECT DISTINCT I.ID" & vbCrLf
sConsulta = sConsulta & "       FROM INSTANCIA I WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN INSTANCIA_BLOQUE WITH(NOLOCK) ON I.ID=INSTANCIA_BLOQUE.INSTANCIA AND INSTANCIA_BLOQUE.ESTADO=1 AND INSTANCIA_BLOQUE.BLOQ<>1 AND INSTANCIA_BLOQUE.TRASLADADA=1" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN (SELECT MAX(ID) ID,BLOQUE FROM INSTANCIA_EST GROUP BY BLOQUE) M ON INSTANCIA_BLOQUE.ID=M.BLOQUE" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN INSTANCIA_EST WITH(NOLOCK) ON M.ID=INSTANCIA_EST.ID AND INSTANCIA_EST.DESTINATARIO_PROV=@PRV" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PM_COPIA_ROL R WITH(NOLOCK) ON INSTANCIA_EST.ROL=R.ID" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN SOLICITUD S WITH(NOLOCK) ON I.SOLICITUD=S.ID" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN TIPO_SOLICITUDES TS WITH(NOLOCK) ON S.TIPO=TS.ID" & vbCrLf
sConsulta = sConsulta & "       WHERE I.NUM IS NOT NULL AND I.ESTADO=2 AND TS.TIPO IN(0,1,4,6,8,9)" & vbCrLf
sConsulta = sConsulta & "       UNION" & vbCrLf
sConsulta = sConsulta & "       SELECT DISTINCT I.ID" & vbCrLf
sConsulta = sConsulta & "       FROM INSTANCIA I WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN INSTANCIA_BLOQUE WITH(NOLOCK) ON I.ID=INSTANCIA_BLOQUE.INSTANCIA AND INSTANCIA_BLOQUE.ESTADO=1 AND INSTANCIA_BLOQUE.BLOQ<>1 AND INSTANCIA_BLOQUE.TRASLADADA=1" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN (SELECT MAX(ID) ID,BLOQUE FROM INSTANCIA_EST GROUP BY BLOQUE) M ON INSTANCIA_BLOQUE.ID=M.BLOQUE" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN INSTANCIA_EST WITH(NOLOCK) ON M.ID=INSTANCIA_EST.ID AND INSTANCIA_EST.DESTINATARIO_PROV=@PRV" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN PM_COPIA_ROL R WITH(NOLOCK) ON INSTANCIA_EST.ROL=R.ID" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN SOLICITUD S WITH(NOLOCK) ON I.SOLICITUD=S.ID" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN TIPO_SOLICITUDES TS WITH(NOLOCK) ON S.TIPO=TS.ID" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN CONTRATO C WITH(NOLOCK) ON C.INSTANCIA=I.ID" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN EMPRESA_PORTAL E WITH(NOLOCK) ON (E.EMP=C.EMP AND E.PORTAL=@NOM_PORTAL) OR @NOM_PORTAL IS NULL" & vbCrLf
sConsulta = sConsulta & "       WHERE I.NUM IS NOT NULL AND I.ESTADO=2 AND TS.TIPO=5" & vbCrLf
sConsulta = sConsulta & "   ) SUMA1" & vbCrLf
sConsulta = sConsulta & "END        " & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PROVE_COD]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[PROVE_COD]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[PROVE_COD] (@OLD VARCHAR(50),@NEW VARCHAR(50))  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "-- Quitamos la transacci?n porque se hace desde programa" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR LOCAL FOR SELECT COD FROM PROVE WITH (NOLOCK) WHERE COD=@OLD" & vbCrLf
sConsulta = sConsulta & "OPEN C" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C INTO @VAR_COD" & vbCrLf
sConsulta = sConsulta & "IF @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_PUNT_ERRORES NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE NOCONF_CONPROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONTRATO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CON NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART1_ADJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART2_ADJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART3_ADJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ADJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ADJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ADJREU NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OFE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OFE_WEB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OFEESC NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE DETALLE_PUJAS NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFEMIN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_WEB_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_WEB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_PET NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_NO_POT NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PEDIDO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_EQP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVENOTIF NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATALOG_LIN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ORDEN_ENTREGA NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_PEDIDO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART_UP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ESP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ESP_PORTAL NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ITEM_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_GR_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ITEM_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_GR_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GRUPO_OFE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GRUPO_OFE_REU NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FAVORITOS_ORDEN_ENTREGA NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FAVORITOS_LINEAS_PEDIDO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATN1_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATN2_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATN3_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATN4_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATN5_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_EMPRESA_COD_ERP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_GRUPOS NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE VERSION_INSTANCIA NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CERTIFICADO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_CERTIFICADO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE INSTANCIA_EST NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE NOCONFORMIDAD NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_ROL NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE INSTANCIA_ROL_CON NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE INSTANCIA_ROL NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_COPIA_ROL NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_LEIDO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_FAVORITOS NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_UNQA NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE VAR_PROVE_UNQA_PUNT NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE VAR_PROVE_UNQA_X NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE VAR_PROVE_UNQA_PUNT_HIST NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE VAR_PROVE_UNQA_X_HIST NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE VAR_CAL_INT NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE VAR_CAL_MANUAL NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PPM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CARGO_PROVEEDORES NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE TASA_SERVICIO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_BANCA NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FACTURA NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE REGISTRO_EMAIL NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CESTA_CABECERA NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CESTA NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_GR_COSTESDESC NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FSGA_H_CAT_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FSGA_CAT_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE IMPUESTOS_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_PARTICIPANTES_FAV NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_REL NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_CON_NOTIF NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_MEN_CON NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_RESP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_USU NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ADJESC NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_MATERIAL_QA NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ESCALACION NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ESCALACION_HIST NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE NOTIF_PROVE_QA_REAL NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE NOTIF_PROVE_QA_REAL NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_SOLICITUD_EXPREG NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ESCALACION_PROPUESTAS NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OBJETIVO_SUELO_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE SET COD=@NEW WHERE COD=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CON SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_PROVE SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART1_ADJ SET CODPROVE=@NEW WHERE CODPROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART2_ADJ SET CODPROVE=@NEW WHERE CODPROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART3_ADJ SET CODPROVE=@NEW WHERE CODPROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4_ADJ SET CODPROVE=@NEW WHERE CODPROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM SET PROVEACT=@NEW WHERE PROVEACT=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_ADJ SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_ADJREU SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_OFE SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_OFE_WEB SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_OFEESC SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE DETALLE_PUJAS SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFEMIN SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE_ADJUN SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE_WEB_ADJUN SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE_WEB SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE_PET SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE_NO_POT SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PEDIDO SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_EQP SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_GMN1 SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_GMN2 SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_GMN3 SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_GMN4 SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART1 SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART2 SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART3 SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART4 SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVENOTIF SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE USU SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART4_ATRIB SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CATALOG_LIN SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ORDEN_ENTREGA SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE LINEAS_PEDIDO SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART_UP SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ESP SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ESP_PORTAL SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET PROVEACT=@NEW WHERE PROVEACT=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GRUPO SET PROVEACT=@NEW WHERE PROVEACT=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ALL_PROVE SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_GRUPO_PROVE SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ITEM_PROVE SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_PROCE_PROVE SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_ATRIB SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_ITEM_ATRIB SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_GR_ATRIB SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_ITEM_ADJUN SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_GR_ADJUN SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GRUPO_OFE SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GRUPO_OFE_REU SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_PROVE SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_PROVE SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "--2.12" & vbCrLf
sConsulta = sConsulta & "UPDATE CATN1_PROVE  SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CATN2_PROVE  SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CATN3_PROVE  SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CATN4_PROVE  SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CATN5_PROVE  SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART4_ADJUN  SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE FAVORITOS_ORDEN_ENTREGA  SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE FAVORITOS_LINEAS_PEDIDO  SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE_GRUPOS SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_EMPRESA_COD_ERP SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE FORM_CAMPO SET VALOR_TEXT=@NEW WHERE VALOR_TEXT=@OLD AND TIPO_CAMPO_GS=100" & vbCrLf
sConsulta = sConsulta & "UPDATE LINEA_DESGLOSE SET VALOR_TEXT=@NEW WHERE VALOR_TEXT=@OLD AND CAMPO_HIJO IN (SELECT ID FROM FORM_CAMPO WITH (NOLOCK) WHERE ES_SUBCAMPO=1 AND TIPO_CAMPO_GS=100)" & vbCrLf
sConsulta = sConsulta & "UPDATE COPIA_CAMPO SET VALOR_TEXT=@NEW WHERE VALOR_TEXT=@OLD AND COPIA_CAMPO_DEF IN (SELECT ID FROM FORM_CAMPO WITH (NOLOCK) WHERE TIPO_CAMPO_GS=100)" & vbCrLf
sConsulta = sConsulta & "UPDATE COPIA_LINEA_DESGLOSE SET VALOR_TEXT=@NEW WHERE VALOR_TEXT=@OLD AND CAMPO_HIJO IN (SELECT COPIA_CAMPO.ID FROM COPIA_CAMPO WITH (NOLOCK) INNER JOIN FORM_CAMPO WITH (NOLOCK) ON COPIA_CAMPO.COPIA_CAMPO_DEF=FORM_CAMPO.ID WHERE ES_SUBCAMPO=1 AND TIPO_CAMPO_GS=100)" & vbCrLf
sConsulta = sConsulta & "UPDATE VERSION_INSTANCIA  SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CERTIFICADO  SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_CERTIFICADO  SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE INSTANCIA_EST  SET DESTINATARIO_PROV=@NEW WHERE DESTINATARIO_PROV=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE NOCONFORMIDAD  SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PM_ROL  SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE INSTANCIA_ROL_CON SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE INSTANCIA_ROL SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PM_COPIA_ROL  SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE INSTANCIA_EST  SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_LEIDO  SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_FAVORITOS  SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_UNQA  SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CESTA_CABECERA SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CESTA SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE VAR_PROVE_UNQA_PUNT  SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE VAR_PROVE_UNQA_X  SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE VAR_PROVE_UNQA_PUNT_HIST  SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE VAR_PROVE_UNQA_X_HIST  SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE VAR_CAL_INT  SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE VAR_CAL_MANUAL  SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PPM  SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CARGO_PROVEEDORES  SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE TASA_SERVICIO  SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_BANCA  SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE FACTURA SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE REGISTRO_EMAIL SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE NOCONF_CONPROVE SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_GR_COSTESDESC SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONTRATO SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_PUNT_ERRORES SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE FSGA_H_CAT_PROVE SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE FSGA_CAT_PROVE SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE IMPUESTOS_PROVE SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PM_PARTICIPANTES_FAV SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_REL SET PROVEP=@NEW WHERE PROVEP=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_REL SET PROVES=@NEW WHERE PROVES=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_CON_NOTIF SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_MEN_CON SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_RESP SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_RESP SET PROVECITADO=@NEW WHERE PROVECITADO=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CN_USU SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_ADJESC SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_MATERIAL_QA SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ESCALACION SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ESCALACION_HIST SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE NOTIF_PROVE_QA_REAL SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_SOLICITUD_EXPREG SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ESCALACION_PROPUESTAS SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OBJETIVO_SUELO_PROVE SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CON CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART1_ADJ CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART2_ADJ CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART3_ADJ CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ADJ CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ADJ CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ADJREU CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OFE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OFE_WEB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OFEESC CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE DETALLE_PUJAS CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFEMIN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_WEB_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_WEB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_PET CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_NO_POT CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PEDIDO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_EQP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVENOTIF CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATALOG_LIN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ORDEN_ENTREGA CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_PEDIDO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART_UP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ESP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ESP_PORTAL CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ITEM_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_GR_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ITEM_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_GR_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GRUPO_OFE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GRUPO_OFE_REU CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FAVORITOS_ORDEN_ENTREGA CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FAVORITOS_LINEAS_PEDIDO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATN1_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATN2_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATN3_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATN4_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATN5_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_EMPRESA_COD_ERP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_GRUPOS CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE VERSION_INSTANCIA CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CERTIFICADO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_CERTIFICADO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE INSTANCIA_EST CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE NOCONFORMIDAD CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_ROL CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE INSTANCIA_ROL_CON CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE INSTANCIA_ROL CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_COPIA_ROL CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_LEIDO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_FAVORITOS CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_UNQA CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE VAR_PROVE_UNQA_PUNT CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE VAR_PROVE_UNQA_X CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE VAR_PROVE_UNQA_PUNT_HIST CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE VAR_PROVE_UNQA_X_HIST CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE VAR_CAL_INT CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE VAR_CAL_MANUAL CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PPM CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CARGO_PROVEEDORES CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE TASA_SERVICIO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_BANCA CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FACTURA CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE REGISTRO_EMAIL CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE NOCONF_CONPROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CESTA CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CESTA_CABECERA CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_GR_COSTESDESC CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONTRATO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_PUNT_ERRORES CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FSGA_H_CAT_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FSGA_CAT_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE IMPUESTOS_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PM_PARTICIPANTES_FAV CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_REL CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_CON_NOTIF CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_MEN_CON CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_RESP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CN_USU CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ADJESC CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_MATERIAL_QA CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ESCALACION CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ESCALACION_HIST CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE NOTIF_PROVE_QA_REAL CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_SOLICITUD_EXPREG CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ESCALACION_PROPUESTAS CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OBJETIVO_SUELO_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close C" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSQA_FILTRO_GET_CAMPOS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSQA_FILTRO_GET_CAMPOS]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSQA_FILTRO_GET_CAMPOS]" & vbCrLf
sConsulta = sConsulta & "   @ID INT," & vbCrLf
sConsulta = sConsulta & "   @ID_FILTRO_USU INT=0," & vbCrLf
sConsulta = sConsulta & "   @IDI NVARCHAR(20)" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   DECLARE @SQL NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "   CREATE TABLE #T1 (ID nVARCHAR(10),NOMBRE nVARCHAR(300),PADRE nVARCHAR(10),NOMBRE_GRID NVARCHAR(100),VISIBLE_GRID TINYINT,POSICION_GRID INT,TAMANYO_GRID FLOAT,POS INT)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   ------------------------  GRUPOS --------------------------" & vbCrLf
sConsulta = sConsulta & "   --saca solo los grupos que tienen campos" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=N'INSERT INTO #T1 (ID,NOMBRE,PADRE,NOMBRE_GRID,VISIBLE_GRID,POSICION_GRID,TAMANYO_GRID,POS) '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL+N'SELECT DISTINCT ''G''+CONVERT(NVARCHAR(10),G.ID) ID,G.DEN_'+@IDI+' NOMBRE,NULL PADRE,'''',0,0,0,G.ORDEN '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL+N'FROM FORM_GRUPO G WITH(NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL+N'INNER JOIN FORM_CAMPO C WITH(NOLOCK) ON G.ID=C.GRUPO '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL+N'INNER JOIN QA_FILTROS F WITH(NOLOCK) ON G.FORMULARIO=F.FORMULARIO '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL+N'INNER JOIN SOLICITUD S WITH(NOLOCK) ON F.FORMULARIO=S.FORMULARIO '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL+N'INNER JOIN CONF_CUMPLIMENTACION CC WITH(NOLOCK) ON CC.SOLICITUD=S.ID AND CC.VISIBLE=1 AND CC.CAMPO=C.ID AND SOLIC_PROVE=1 '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL+N'WHERE G.BAJALOG=0 AND C.ES_SUBCAMPO=0 AND C.SUBTIPO<>9 AND (C.TIPO_CAMPO_GS<>106 OR C.TIPO_CAMPO_GS IS NULL) AND F.ID=@ID'" & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL,N'@ID INT',@ID=@ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=N'INSERT INTO #T1 (ID,NOMBRE,PADRE,NOMBRE_GRID,VISIBLE_GRID,POSICION_GRID,TAMANYO_GRID,POS) '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL+N'SELECT DISTINCT ''G''+CONVERT(NVARCHAR(10),G.ID) ID,G.DEN_'+@IDI+' NOMBRE,NULL PADRE,'''',0,0,0,G.ORDEN '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL+N'FROM FORM_GRUPO G WITH(NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL+N'INNER JOIN FORM_CAMPO C WITH(NOLOCK) ON G.ID=C.GRUPO '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL+N'INNER JOIN QA_FILTROS F WITH(NOLOCK) ON G.FORMULARIO=F.FORMULARIO '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL+N'INNER JOIN SOLICITUD S WITH(NOLOCK) ON F.FORMULARIO=S.FORMULARIO '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL+N'INNER JOIN INSTANCIA I WITH(NOLOCK) ON S.ID=I.SOLICITUD '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL+N'INNER JOIN CONF_CUMPLIMENTACION CC WITH(NOLOCK) ON CC.SOLICITUD=S.ID AND CC.VISIBLE=1 AND CC.CAMPO=C.ID AND SOLIC_PROVE=1 '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL+N'WHERE G.BAJALOG=0 AND C.ES_SUBCAMPO=0 AND C.SUBTIPO<>9 AND (C.TIPO_CAMPO_GS<>106 OR C.TIPO_CAMPO_GS IS NULL) AND F.ID=@ID'" & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL,N'@ID INT',@ID=@ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   ------------------------  CAMPOS --------------------------" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=N'INSERT INTO #T1 (ID,NOMBRE,PADRE,NOMBRE_GRID,VISIBLE_GRID,POSICION_GRID,TAMANYO_GRID,POS) '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL+N'SELECT DISTINCT C.ID,C.DEN_'+@IDI+' NOMBRE,''G''+CONVERT(NVARCHAR(10),G.ID) PADRE'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @ID_FILTRO_USU>0" & vbCrLf
sConsulta = sConsulta & "      SET @SQL=@SQL+N',ISNULL(FCU.NOMBRE,'''') NOMBRE_GRID,ISNULL(FCU.VISIBLE,0) VISIBLE_GRID,ISNULL(FCU.POSICION,0) POSICION_GRID,ISNULL(FCU.TAMANYO,0) TAMANYO_GRID,C.ORDEN '" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "      SET @SQL=@SQL+N','''' NOMBRE_GRID,0 VISIBLE,0 POSICION_GRID,0 TAMANYO_GRID,C.ORDEN '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL+N'FROM FORM_CAMPO C WITH(NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL+N'INNER JOIN FORM_GRUPO G WITH(NOLOCK) ON C.GRUPO=G.ID '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL+N'INNER JOIN QA_FILTROS F WITH(NOLOCK) ON G.FORMULARIO=F.FORMULARIO '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL+N'INNER JOIN SOLICITUD S WITH(NOLOCK) ON F.FORMULARIO=S.FORMULARIO '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL+N'INNER JOIN CONF_CUMPLIMENTACION CC WITH(NOLOCK) ON CC.SOLICITUD=S.ID AND CC.VISIBLE=1 AND CC.CAMPO=C.ID AND SOLIC_PROVE=1 '" & vbCrLf
sConsulta = sConsulta & "   IF @ID_FILTRO_USU>0" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "      SET @SQL=@SQL+N'INNER JOIN QA_FILTRO_USU FU WITH(NOLOCK) ON FU.ID=@ID_FILTRO_USU AND FU.ID_FILTRO=F.ID '" & vbCrLf
sConsulta = sConsulta & "      SET @SQL=@SQL+N'LEFT JOIN QA_FILTRO_USU_CAMPOS FCU WITH(NOLOCK) ON FCU.ID_FILTRO_USU=@ID_FILTRO_USU AND FCU.CAMPO=C.ID '" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL+N'WHERE G.BAJALOG=0 AND C.BAJALOG=0 AND C.ES_SUBCAMPO=0 AND C.SUBTIPO<>9 AND (C.TIPO_CAMPO_GS<>106 OR C.TIPO_CAMPO_GS IS NULL) AND F.ID=@ID'" & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL,N'@ID INT,@ID_FILTRO_USU INT=0',@ID=@ID,@ID_FILTRO_USU=@ID_FILTRO_USU" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   SET @SQL=N'INSERT INTO #T1 (ID,NOMBRE,PADRE,NOMBRE_GRID,VISIBLE_GRID,POSICION_GRID,TAMANYO_GRID,POS) '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL+N'SELECT DISTINCT C.ID,C.DEN_'+@IDI+' NOMBRE,''G''+CONVERT(NVARCHAR(10),G.ID) PADRE'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @ID_FILTRO_USU>0" & vbCrLf
sConsulta = sConsulta & "      SET @SQL=@SQL+N',ISNULL(FCU.NOMBRE,'''') NOMBRE_GRID,ISNULL(FCU.VISIBLE,0) VISIBLE_GRID,ISNULL(FCU.POSICION,0) POSICION_GRID,ISNULL(FCU.TAMANYO,0) TAMANYO_GRID,C.ORDEN '" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "      SET @SQL=@SQL+N','''' NOMBRE_GRID,0 VISIBLE,0 POSICION_GRID,0 TAMANYO_GRID,C.ORDEN '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL+N'FROM FORM_GRUPO G WITH(NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL+N'INNER JOIN FORM_CAMPO C WITH(NOLOCK) ON G.ID=C.GRUPO '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL+N'INNER JOIN QA_FILTROS F WITH(NOLOCK) ON G.FORMULARIO=F.FORMULARIO '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL+N'INNER JOIN SOLICITUD S WITH(NOLOCK) ON F.FORMULARIO=S.FORMULARIO '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL+N'INNER JOIN INSTANCIA I WITH(NOLOCK) ON S.ID=I.SOLICITUD '" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL+N'INNER JOIN CONF_CUMPLIMENTACION CC WITH(NOLOCK) ON CC.SOLICITUD=S.ID AND CC.VISIBLE=1 AND CC.CAMPO=C.ID AND SOLIC_PROVE=1'" & vbCrLf
sConsulta = sConsulta & "   IF @ID_FILTRO_USU>0" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "      SET @SQL=@SQL+N'INNER JOIN QA_FILTRO_USU FU WITH(NOLOCK) ON FU.ID=@ID_FILTRO_USU AND FU.ID_FILTRO=F.ID '" & vbCrLf
sConsulta = sConsulta & "      SET @SQL=@SQL+N'LEFT JOIN QA_FILTRO_USU_CAMPOS FCU WITH(NOLOCK) ON FCU.ID_FILTRO_USU=@ID_FILTRO_USU AND FCU.CAMPO=C.ID '" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SET @SQL=@SQL+N'WHERE G.BAJALOG=0 AND C.BAJALOG=0 AND C.ES_SUBCAMPO=0 AND C.SUBTIPO<>9 AND (C.TIPO_CAMPO_GS<>106 OR C.TIPO_CAMPO_GS IS NULL) AND F.ID=@ID'" & vbCrLf
sConsulta = sConsulta & "   EXEC SP_EXECUTESQL @SQL,N'@ID INT,@ID_FILTRO_USU INT=0',@ID=@ID,@ID_FILTRO_USU=@ID_FILTRO_USU" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   --primero saco los grupos" & vbCrLf
sConsulta = sConsulta & "   SELECT DISTINCT ID,NOMBRE,PADRE,POS" & vbCrLf
sConsulta = sConsulta & "   FROM #T1 WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   WHERE ID NOT IN (SELECT CONVERT(NVARCHAR(10),ID_CAMPO) FROM UPD_VISORES_QA WITH(NOLOCK) WHERE ACCION='I' AND OK=0) -- campos que se han a?adido en el formulario pero no se ha hecho todavia ALTER en las tablas de filtros (BatchUpdates)" & vbCrLf
sConsulta = sConsulta & "   AND PADRE IS NULL" & vbCrLf
sConsulta = sConsulta & "   ORDER BY POS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   --luego saco los campos de todos los grupos" & vbCrLf
sConsulta = sConsulta & "   SELECT DISTINCT ID,NOMBRE,PADRE,NOMBRE_GRID,VISIBLE_GRID,POSICION_GRID,TAMANYO_GRID,POS" & vbCrLf
sConsulta = sConsulta & "   FROM #T1  WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   WHERE ID NOT IN (SELECT CONVERT(NVARCHAR(10),ID_CAMPO) FROM UPD_VISORES_QA WITH(NOLOCK) WHERE ACCION='I' AND OK=0) -- campos que se han a?adido en el formulario pero no se ha hecho todavia ALTER en las tablas de filtros (BatchUpdates)" & vbCrLf
sConsulta = sConsulta & "   AND PADRE IS NOT  NULL" & vbCrLf
sConsulta = sConsulta & "   ORDER BY PADRE,POS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   DROP TABLE #T1" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSQA_GET_PROVEEDORES_PANELCALIDAD]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSQA_GET_PROVEEDORES_PANELCALIDAD]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSQA_GET_PROVEEDORES_PANELCALIDAD]" & vbCrLf
sConsulta = sConsulta & "@USU  VARCHAR(50)," & vbCrLf
sConsulta = sConsulta & "@MATERIALESQA TBGMNS READONLY," & vbCrLf
sConsulta = sConsulta & "@UNQAS  VARCHAR(1000)," & vbCrLf
sConsulta = sConsulta & "@UNQAS_PUNT VARCHAR(1000)," & vbCrLf
sConsulta = sConsulta & "@VARCAL VARCHAR(1000)," & vbCrLf
sConsulta = sConsulta & "@PROVEEDORES TINYINT," & vbCrLf
sConsulta = sConsulta & "@PROVE VARCHAR(50)=NULL," & vbCrLf
sConsulta = sConsulta & "@PROVEEDORES_BAJA TINYINT=0," & vbCrLf
sConsulta = sConsulta & "@IDI VARCHAR(50)='SPA'," & vbCrLf
sConsulta = sConsulta & "@RCON AS TINYINT,--los que tengan al menos 1 contacto calidad" & vbCrLf
sConsulta = sConsulta & "@RMAT AS TINYINT,--al material del usuario" & vbCrLf
sConsulta = sConsulta & "@REQP AS TINYINT--al equipo del usuario" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(max)" & vbCrLf
sConsulta = sConsulta & "DECLARE @FROM NVARCHAR(max)" & vbCrLf
sConsulta = sConsulta & "DECLARE @FROM_PROVE NVARCHAR(max)" & vbCrLf
sConsulta = sConsulta & "DECLARE @FROM_PROVE_PUNT NVARCHAR(max)" & vbCrLf
sConsulta = sConsulta & "DECLARE @FROM_PROVE_MAT NVARCHAR(max)" & vbCrLf
sConsulta = sConsulta & "DECLARE @WHERE_PROVE NVARCHAR(max)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PER AS VARCHAR(50)--PERSONA DEL USU" & vbCrLf
sConsulta = sConsulta & "DECLARE @EQP AS VARCHAR(50)--EQUIPO DEL USU" & vbCrLf
sConsulta = sConsulta & "--Mostrar las variables de calidad en el panel de forma" & vbCrLf
sConsulta = sConsulta & "DECLARE @PP_MANUAL AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PP_AUTO AS TINYINT" & vbCrLf
sConsulta = sConsulta & "--Transformamos la cadena de pares" & vbCrLf
sConsulta = sConsulta & "DECLARE @WHERE_VARCAL NVARCHAR(max)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @WHERE_VARCAL=N'#I#' + @VARCAL  +N'#F#'" & vbCrLf
sConsulta = sConsulta & "SET @WHERE_VARCAL=REPLACE(@WHERE_VARCAL,N'#I#',N'(PUNT.VARCAL=')" & vbCrLf
sConsulta = sConsulta & "SET @WHERE_VARCAL=REPLACE(@WHERE_VARCAL,N',',N') OR (PUNT.VARCAL=')" & vbCrLf
sConsulta = sConsulta & "SET @WHERE_VARCAL=REPLACE(@WHERE_VARCAL,N'-',N' AND PUNT.NIVEL=')" & vbCrLf
sConsulta = sConsulta & "SET @WHERE_VARCAL=REPLACE(@WHERE_VARCAL,N'#F#',N')')" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Panel tiene 2 formas de pedir Materiales. Por MatQA  o MtGS" & vbCrLf
sConsulta = sConsulta & "DECLARE @MAT_QA_O_GS_O_NADA TINYINT=0" & vbCrLf
sConsulta = sConsulta & "--0->No hay mat por el q filtrar" & vbCrLf
sConsulta = sConsulta & "--1->Mat Gs. Lo q implica usar @MATERIALESQA.GMN1..4 y tabla MATERIAL_QA_GMN4 y tabla PROVE_GMN4" & vbCrLf
sConsulta = sConsulta & "--2->Mat Qa. Lo q implica usar @MATERIALESQA.GMNQA y tabla PROVE_MATERIAL_QA" & vbCrLf
sConsulta = sConsulta & "IF (EXISTS(SELECT 1 FROM @MATERIALESQA WHERE GMN1 IS NOT NULL))" & vbCrLf
sConsulta = sConsulta & "SET @MAT_QA_O_GS_O_NADA = 1" & vbCrLf
sConsulta = sConsulta & "IF (EXISTS(SELECT 1 FROM @MATERIALESQA WHERE GMNQA IS NOT NULL))" & vbCrLf
sConsulta = sConsulta & "SET @MAT_QA_O_GS_O_NADA = 2" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @PER=PER,@EQP=EQP,@PP_MANUAL=FSQA_PP_MANUAL,@PP_AUTO=FSQA_PP_AUTO FROM USU WITH(NOLOCK) WHERE COD=@USU" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--SELECT DE PROVEEDORES" & vbCrLf
sConsulta = sConsulta & "SET @SQL=N';WITH CO2 AS (SELECT CO.PROVE,MAX(CO.EMAIL) EMAIL,COUNT(CO.EMAIL) NUM_CON FROM CON CO WITH(NOLOCK) WHERE CO.CALIDAD=1 GROUP BY CO.PROVE) '" & vbCrLf
sConsulta = sConsulta & "SET @SQL= @SQL + N'SELECT P.COD AS PROVE_COD,P.NIF AS PROVE_CIF'" & vbCrLf
sConsulta = sConsulta & "SET @SQL= @SQL + N',CASE ISNULL(CO2.NUM_CON,0) WHEN 0 THEN '''' WHEN 1 THEN CO2.EMAIL ELSE ''##'' END CON_EMAIL'" & vbCrLf
sConsulta = sConsulta & "SET @SQL= @SQL + N',CASE WHEN ISNULL(CO2.NUM_CON,0)>1  THEN DBO.LISTAMAILS(P.COD) ELSE NULL END CON_EMAIL_VARIOS'" & vbCrLf
sConsulta = sConsulta & "SET @SQL= @SQL + N',P.DEN AS PROVE_NAME,'''' as MATERIAL_QA,'''' as MATERIAL_QA_ID'" & vbCrLf
sConsulta = sConsulta & "SET @SQL= @SQL + N',P.CALIDAD_POT PROVE_CALIDAD_POT,P.CALIDAD_PROVISIONAL PROVE_CALIDAD_PROVISIONAL,P.BAJA_CALIDAD PROVE_CALIDAD_BAJA'" & vbCrLf
sConsulta = sConsulta & "SET @SQL= @SQL + N',(SELECT COUNT(1) FROM PROVE_PUNT_ERRORES PPE WITH(NOLOCK) WHERE PPE.PROVE=P.COD) HAYERROR'" & vbCrLf
sConsulta = sConsulta & "SET @FROM= N' FROM PROVE P WITH(NOLOCK)'" & vbCrLf
sConsulta = sConsulta & "--Aplicamos restricciones de consulta de proveedores del usuario" & vbCrLf
sConsulta = sConsulta & "IF @RMAT=1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @FROM= @FROM + N' INNER JOIN PROVE_GMN4 PG WITH(NOLOCK) ON P.COD=PG.PROVE'" & vbCrLf
sConsulta = sConsulta & "SET @FROM= @FROM + N' INNER JOIN COM_GMN4 CG WITH(NOLOCK) ON CG.COM=@PER AND CG.GMN1=PG.GMN1  AND CG.GMN2=PG.GMN2  AND CG.GMN3=PG.GMN3  AND CG.GMN4=PG.GMN4'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "IF @REQP=1" & vbCrLf
sConsulta = sConsulta & "SET @FROM= @FROM + N' INNER JOIN PROVE_EQP E WITH(NOLOCK) ON E.PROVE=P.COD AND E.EQP=@EQP'" & vbCrLf
sConsulta = sConsulta & "IF @RCON=0" & vbCrLf
sConsulta = sConsulta & "SET @FROM= @FROM + N' LEFT JOIN CO2 ON CO2.PROVE=P.COD'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @FROM= @FROM + N' INNER JOIN CO2 ON CO2.PROVE=P.COD'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @MAT_QA_O_GS_O_NADA = 1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @FROM= @FROM + N' INNER JOIN PROVE_GMN4 PG WITH(NOLOCK) ON P.COD=PG.PROVE'" & vbCrLf
sConsulta = sConsulta & "SET @FROM= @FROM + N' INNER JOIN MATERIAL_QA_GMN4 PM WITH(NOLOCK) ON PM.GMN1=PG.GMN1 AND PM.GMN2=PG.GMN2 AND PM.GMN3=PG.GMN3 AND PM.GMN4=PG.GMN4'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "IF @MAT_QA_O_GS_O_NADA = 2" & vbCrLf
sConsulta = sConsulta & "SET @FROM= @FROM + N' INNER JOIN PROVE_MATERIAL_QA PM WITH(NOLOCK) ON PM.PROVE=P.COD'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @FROM_PROVE=@FROM" & vbCrLf
sConsulta = sConsulta & "--Aplicamos filtros de Materiales de QA a la JOIN con PROVE_MATERIAL_QA" & vbCrLf
sConsulta = sConsulta & "IF @MAT_QA_O_GS_O_NADA = 1" & vbCrLf
sConsulta = sConsulta & "SET @FROM_PROVE= @FROM_PROVE + N' INNER JOIN @MATERIALESQA MQA ON PM.GMN1= MQA.GMN1 AND PM.GMN2= ISNULL(MQA.GMN2,PM.GMN2) AND PM.GMN3= ISNULL(MQA.GMN3,PM.GMN3) AND PM.GMN4= ISNULL(MQA.GMN4,PM.GMN4)'" & vbCrLf
sConsulta = sConsulta & "IF @MAT_QA_O_GS_O_NADA = 2" & vbCrLf
sConsulta = sConsulta & "SET @FROM_PROVE= @FROM_PROVE + N' INNER JOIN @MATERIALESQA MQA ON PM.MATERIAL_QA=ISNULL(MQA.GMNQA,PM.MATERIAL_QA)'" & vbCrLf
sConsulta = sConsulta & "--WHERE" & vbCrLf
sConsulta = sConsulta & "SET @WHERE_PROVE= N' WHERE 1=1'" & vbCrLf
sConsulta = sConsulta & "--Aplicamos filtros de proveedores" & vbCrLf
sConsulta = sConsulta & "-- @PROVEEDORES: 1-Solo un proveedor, 2-Todos, 3-Potenciales, 4-Panel" & vbCrLf
sConsulta = sConsulta & "IF @PROVEEDORES=1 and @PROVE IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SET @WHERE_PROVE=@WHERE_PROVE + N' AND P.COD='''+ @PROVE + N''' AND P.CALIDAD_POT>0'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @PROVEEDORES=2" & vbCrLf
sConsulta = sConsulta & "SET @WHERE_PROVE=@WHERE_PROVE + N' AND P.CALIDAD_POT>0'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @PROVEEDORES=3" & vbCrLf
sConsulta = sConsulta & "SET @WHERE_PROVE=@WHERE_PROVE + N' AND P.CALIDAD_POT=1'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @PROVEEDORES=4" & vbCrLf
sConsulta = sConsulta & "SET @WHERE_PROVE=@WHERE_PROVE + N' AND P.CALIDAD_POT=2'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @PROVEEDORES=5" & vbCrLf
sConsulta = sConsulta & "SET @WHERE_PROVE=@WHERE_PROVE + N' AND P.CALIDAD_POT=2 AND P.CALIDAD_PROVISIONAL=1'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @WHERE_PROVE=@WHERE_PROVE + N' AND P.CALIDAD_POT=2 AND P.CALIDAD_PROVISIONAL=0'" & vbCrLf
sConsulta = sConsulta & "IF @PROVEEDORES_BAJA=0" & vbCrLf
sConsulta = sConsulta & "SET @WHERE_PROVE=@WHERE_PROVE + N' AND P.BAJA_CALIDAD=0'" & vbCrLf
sConsulta = sConsulta & "SET @WHERE_PROVE=@WHERE_PROVE + N' AND EXISTS(SELECT UNQA FROM PROVE_UNQA PU WITH (NOLOCK) WHERE PU.PROVE=P.COD'" & vbCrLf
sConsulta = sConsulta & "--Aplicamos filtros de Unidades de negocio de QA a la JOIN con PROVE_UNQA" & vbCrLf
sConsulta = sConsulta & "IF @UNQAS<>'' SET @WHERE_PROVE= @WHERE_PROVE + N' AND PU.UNQA IN ('+ @UNQAS + N')'" & vbCrLf
sConsulta = sConsulta & "SET @WHERE_PROVE= @WHERE_PROVE + N') '" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL +@FROM_PROVE+@WHERE_PROVE+ N' GROUP BY P.COD,P.NIF,P.DEN,P.CALIDAD_POT,P.CALIDAD_PROVISIONAL,P.BAJA_CALIDAD,CO2.EMAIL,CO2.NUM_CON ORDER BY P.DEN,P.COD'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL,N'@PER  AS VARCHAR(50),@EQP  AS VARCHAR(50),@MATERIALESQA TBGMNS READONLY ',@PER=@PER,@EQP=@EQP,@MATERIALESQA=@MATERIALESQA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--SELECT DE PUNTUACIONES" & vbCrLf
sConsulta = sConsulta & "SET @SQL=N';WITH CO2 AS (SELECT CO.PROVE,MAX(CO.EMAIL) EMAIL,COUNT(CO.EMAIL) NUM_CON FROM CON CO WITH(NOLOCK) WHERE CO.CALIDAD=1 GROUP BY CO.PROVE) '" & vbCrLf
sConsulta = sConsulta & "SET @SQL= @SQL + N'SELECT DISTINCT P.COD AS PROVE_COD'" & vbCrLf
sConsulta = sConsulta & "SET @SQL= @SQL + N', PUNT.NIVEL, PUNT.VARCAL,PUNT.UNQA,PUNT.PUNT,CALI.DEN AS CALIFICACION,PUNT.APLICA'" & vbCrLf
sConsulta = sConsulta & "SET @FROM_PROVE_PUNT= @FROM_PROVE + N' LEFT JOIN VAR_PROVE_UNQA_PUNT PUNT WITH(NOLOCK) ON P.COD=PUNT.PROVE'" & vbCrLf
sConsulta = sConsulta & "--Aplicamos filtros Unidades e Negocio de QA a JOIN con VAR_PROVE_UNQA_PUNT (si tiene acceso al grupo esta incluido)" & vbCrLf
sConsulta = sConsulta & "IF @UNQAS_PUNT<>''" & vbCrLf
sConsulta = sConsulta & "SET @FROM_PROVE_PUNT= @FROM_PROVE_PUNT + N' AND (PUNT.UNQA IN (' + @UNQAS_PUNT + N'))'" & vbCrLf
sConsulta = sConsulta & "--Aplicamos filtros Variables de calidad de QA a JOIN con VAR_PROVE_UNQA_PUNT" & vbCrLf
sConsulta = sConsulta & "SET @FROM_PROVE_PUNT= @FROM_PROVE_PUNT + N' AND (' + @WHERE_VARCAL + N')'" & vbCrLf
sConsulta = sConsulta & "SET @FROM_PROVE_PUNT= @FROM_PROVE_PUNT + N' LEFT JOIN CAL_PUNTUACION_IDIOMA CALI WITH(NOLOCK) ON CALI.VARCAL=PUNT.VARCAL AND CALI.NIVEL =PUNT.NIVEL AND CALI.CAL_PUNTUACION=PUNT.CAL AND CALI.IDIOMA='''+ @IDI + N''''" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL +@FROM_PROVE_PUNT+@WHERE_PROVE + N' ORDER BY P.COD'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL,N'@PER  AS VARCHAR(50),@EQP  AS VARCHAR(50),@MATERIALESQA TBGMNS READONLY',@PER=@PER,@EQP=@EQP,@MATERIALESQA=@MATERIALESQA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--SELECT MATERIALES DE QA" & vbCrLf
sConsulta = sConsulta & "SET @SQL=N';WITH CO2 AS (SELECT CO.PROVE,MAX(CO.EMAIL) EMAIL,COUNT(CO.EMAIL) NUM_CON FROM CON CO WITH(NOLOCK) WHERE CO.CALIDAD=1 GROUP BY CO.PROVE) '" & vbCrLf
sConsulta = sConsulta & "SET @SQL= @SQL + N'SELECT DISTINCT P.COD AS PROVE_COD,M.DEN_' + @IDI + N' AS MATERIAL,M.ID'" & vbCrLf
sConsulta = sConsulta & "IF @MAT_QA_O_GS_O_NADA = 0" & vbCrLf
sConsulta = sConsulta & "SET @FROM= @FROM + N' INNER JOIN PROVE_MATERIAL_QA PM WITH(NOLOCK) ON PM.PROVE=P.COD'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @FROM_PROVE_MAT= @FROM + N' INNER JOIN MATERIALES_QA M WITH(NOLOCK) ON M.ID=PM.MATERIAL_QA AND PM.BAJA=0'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL= @SQL + @FROM_PROVE_MAT+ @WHERE_PROVE + N' AND P.COD IN (SELECT DISTINCT P.COD ' + @FROM_PROVE + @WHERE_PROVE + N') ORDER BY COD,MATERIAL'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL,N'@PER  AS VARCHAR(50),@EQP  AS VARCHAR(50),@MATERIALESQA TBGMNS READONLY',@PER=@PER,@EQP=@EQP,@MATERIALESQA=@MATERIALESQA" & vbCrLf
sConsulta = sConsulta & "--GO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSN_ESCENARIOS_GET_ESCENARIOS_PROVEEDOR]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSN_ESCENARIOS_GET_ESCENARIOS_PROVEEDOR]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSN_ESCENARIOS_GET_ESCENARIOS_PROVEEDOR]" & vbCrLf
sConsulta = sConsulta & "   @TIPOVISOR INT," & vbCrLf
sConsulta = sConsulta & "@IDI NVARCHAR(40)," & vbCrLf
sConsulta = sConsulta & "@IDESCENARIO INT OUTPUT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON;" & vbCrLf
sConsulta = sConsulta & "DECLARE @VISORESC INT" & vbCrLf
sConsulta = sConsulta & "IF @TIPOVISOR=0" & vbCrLf
sConsulta = sConsulta & "SET @VISORESC=1" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @VISORESC=@TIPOVISOR" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--ESCENARIOS" & vbCrLf
sConsulta = sConsulta & "SELECT ID,NOMBRE,E.APLICATODAS" & vbCrLf
sConsulta = sConsulta & "FROM ESCENARIO E WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE E.PORTAL=1 AND COALESCE(E.TIPOVISOR_PORTAL,E.TIPOVISOR)=@VISORESC AND (@IDESCENARIO=0 OR E.ID=@IDESCENARIO)" & vbCrLf
sConsulta = sConsulta & "ORDER BY NOMBRE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "--SOLICITUDES ASIGNADOS A ESTE ESCENARIO" & vbCrLf
sConsulta = sConsulta & "SET @SQL = 'SELECT ESF.ESCENARIO,ESF.SOLICITUD,ESF.FORMULARIO, S.DEN_' + @IDI +' AS DEN" & vbCrLf
sConsulta = sConsulta & "FROM ESCENARIO_SOLICITUD_FORMULARIO ESF WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN SOLICITUD S WITH(NOLOCK) ON ESF.SOLICITUD=S.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN ESCENARIO E WITH(NOLOCK) ON E.ID=ESF.ESCENARIO AND (@IDESCENARIO=0 OR E.ID=@IDESCENARIO) '" & vbCrLf
sConsulta = sConsulta & "IF @TIPOVISOR=0" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'AND COALESCE(E.TIPOVISOR_PORTAL,E.TIPOVISOR)=1 '" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'AND COALESCE(E.TIPOVISOR_PORTAL,E.TIPOVISOR)=@TIPOVISOR '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@IDESCENARIO INT,@TIPOVISOR INT', @IDESCENARIO=@IDESCENARIO, @TIPOVISOR=@TIPOVISOR" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--FILTROS" & vbCrLf
sConsulta = sConsulta & "SELECT F.ID,F.ESCENARIO,F.FORMULAAVANZADA,F.ERROR_FORMULA,F.NOMBRE" & vbCrLf
sConsulta = sConsulta & "FROM FILTRO F WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN ESCENARIO E WITH(NOLOCK) ON E.ID=F.ESCENARIO AND COALESCE(E.TIPOVISOR_PORTAL,E.TIPOVISOR)=@VISORESC AND (@IDESCENARIO=0 OR E.ID=@IDESCENARIO)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--CAMPOS IMPLICADOS EN FILTRO" & vbCrLf
sConsulta = sConsulta & "SELECT FC.FILTRO,F.ESCENARIO,FC.CAMPO,FC.ESGENERAL,FC.VISIBLE,FC.ELIMINADO,FCP.INTRO,FCP.SUBTIPO,FCP.TIPO_CAMPO_GS," & vbCrLf
sConsulta = sConsulta & "FCP.DEN_SPA,FCP.DEN_ENG,FCP.DEN_GER,FCP.DEN_FRA,D.CAMPO_PADRE" & vbCrLf
sConsulta = sConsulta & "FROM FILTRO_CAMPOS FC WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN FILTRO F WITH(NOLOCK) ON F.ID=FC.FILTRO AND FC.ESGENERAL=0" & vbCrLf
sConsulta = sConsulta & "INNER JOIN ESCENARIO E WITH(NOLOCK) ON F.ESCENARIO = E.ID AND COALESCE(E.TIPOVISOR_PORTAL,E.TIPOVISOR)=@VISORESC AND (@IDESCENARIO=0 OR E.ID=@IDESCENARIO)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN FORM_CAMPO FCP WITH(NOLOCK) ON FCP.ID=FC.CAMPO" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN DESGLOSE D WITH(NOLOCK) ON D.CAMPO_HIJO=FCP.ID" & vbCrLf
sConsulta = sConsulta & "UNION ALL" & vbCrLf
sConsulta = sConsulta & "SELECT FC.FILTRO,F.ESCENARIO,FC.CAMPO,FC.ESGENERAL,FC.VISIBLE,FC.ELIMINADO,NULL,NULL,NULL," & vbCrLf
sConsulta = sConsulta & "NULL,NULL,NULL,NULL,NULL" & vbCrLf
sConsulta = sConsulta & "FROM FILTRO_CAMPOS FC WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN FILTRO F WITH(NOLOCK) ON F.ID=FC.FILTRO AND FC.ESGENERAL=1" & vbCrLf
sConsulta = sConsulta & "INNER JOIN ESCENARIO E WITH(NOLOCK) ON F.ESCENARIO = E.ID AND COALESCE(E.TIPOVISOR_PORTAL,E.TIPOVISOR)=@VISORESC AND (@IDESCENARIO=0 OR E.ID=@IDESCENARIO)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--CONDICIONES FORMULA FILTRO" & vbCrLf
sConsulta = sConsulta & "SELECT FC.FILTRO,F.ESCENARIO,FC.ORDEN,FCP.ID CAMPO,FC.ESGENERAL,FC.DENOMINACION,FC.DENOMINACION_BD,FC.OPERADOR,FC.VALOR," & vbCrLf
sConsulta = sConsulta & "FCP.INTRO,FCP.SUBTIPO,FCP.TIPO_CAMPO_GS" & vbCrLf
sConsulta = sConsulta & "FROM FILTRO_CONDICIONES FC WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN FILTRO F WITH(NOLOCK) ON F.ID=FC.FILTRO AND FC.ESGENERAL=0 AND NOT FC.CAMPO=0" & vbCrLf
sConsulta = sConsulta & "INNER JOIN ESCENARIO E WITH(NOLOCK) ON F.ESCENARIO = E.ID AND COALESCE(E.TIPOVISOR_PORTAL,E.TIPOVISOR)=@VISORESC AND (@IDESCENARIO=0 OR E.ID=@IDESCENARIO)" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN FORM_CAMPO FCP WITH(NOLOCK) ON FCP.ID=FC.CAMPO" & vbCrLf
sConsulta = sConsulta & "UNION ALL" & vbCrLf
sConsulta = sConsulta & "SELECT FC.FILTRO,F.ESCENARIO,FC.ORDEN,FC.CAMPO,FC.ESGENERAL,FC.DENOMINACION,FC.DENOMINACION_BD,FC.OPERADOR,FC.VALOR," & vbCrLf
sConsulta = sConsulta & "NULL,NULL,NULL" & vbCrLf
sConsulta = sConsulta & "FROM FILTRO_CONDICIONES FC WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN FILTRO F WITH(NOLOCK) ON F.ID=FC.FILTRO AND (FC.ESGENERAL=1 OR (FC.ESGENERAL=0 AND FC.CAMPO=0))" & vbCrLf
sConsulta = sConsulta & "INNER JOIN ESCENARIO E WITH(NOLOCK) ON F.ESCENARIO = E.ID AND COALESCE(E.TIPOVISOR_PORTAL,E.TIPOVISOR)=@VISORESC AND (@IDESCENARIO=0 OR E.ID=@IDESCENARIO)" & vbCrLf
sConsulta = sConsulta & "ORDER BY FC.ORDEN ASC" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT V.ID,V.ESCENARIO,V.NOMBRE,V.ORDEN" & vbCrLf
sConsulta = sConsulta & "FROM VISTA V WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN ESCENARIO E WITH(NOLOCK) ON E.ID=V.ESCENARIO AND COALESCE(E.TIPOVISOR_PORTAL,E.TIPOVISOR)=@VISORESC AND (@IDESCENARIO=0 OR E.ID=@IDESCENARIO)" & vbCrLf
sConsulta = sConsulta & "ORDER BY V.NOMBRE,V.ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--CAMPOS VISUALIZADOS EN LA VISTA" & vbCrLf
sConsulta = sConsulta & "SELECT VC.VISTA,V.ESCENARIO,VC.CAMPO,VC.ESGENERAL,VC.ESDESGLOSE,VC.NOMBREPERSONALIZADO," & vbCrLf
sConsulta = sConsulta & "ISNULL(VCOU.ANCHO,VC.ANCHO) ANCHO,ISNULL(VCOU.POSICION,VC.POSICION) POSICION," & vbCrLf
sConsulta = sConsulta & "FCP.SUBTIPO,FCP.TIPO_CAMPO_GS,FCP.DEN_SPA,FCP.DEN_ENG,FCP.DEN_GER,FCP.DEN_FRA,D.CAMPO_PADRE," & vbCrLf
sConsulta = sConsulta & "FCP2.DEN_SPA DESG_SPA,FCP2.DEN_ENG DESG_ENG,FCP2.DEN_GER DESG_GER,FCP2.DEN_FRA DESG_FRA" & vbCrLf
sConsulta = sConsulta & "FROM VISTA_CAMPOS VC WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN VISTA V WITH(NOLOCK) ON V.ID=VC.VISTA" & vbCrLf
sConsulta = sConsulta & "INNER JOIN ESCENARIO E WITH(NOLOCK) ON V.ESCENARIO = E.ID AND COALESCE(E.TIPOVISOR_PORTAL,E.TIPOVISOR)=@VISORESC AND (@IDESCENARIO=0 OR E.ID=@IDESCENARIO)" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN FORM_CAMPO FCP WITH(NOLOCK) ON FCP.ID=VC.CAMPO" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN DESGLOSE D WITH(NOLOCK) ON D.CAMPO_HIJO=FCP.ID" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN FORM_CAMPO FCP2 WITH(NOLOCK) ON FCP2.ID=D.CAMPO_PADRE" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN VISTA_CAMPOS_OPCIONES_USU VCOU WITH(NOLOCK) ON VCOU.VISTA=VC.VISTA" & vbCrLf
sConsulta = sConsulta & "AND VCOU.CAMPO=VC.CAMPO AND VCOU.ESGENERAL=VC.ESGENERAL" & vbCrLf
sConsulta = sConsulta & "ORDER BY ESDESGLOSE,POSICION" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--OBTENGO EL ID DEL ESCENARIO DEL QUE OBTENDREMOS LOS DATOS" & vbCrLf
sConsulta = sConsulta & "IF @IDESCENARIO=0" & vbCrLf
sConsulta = sConsulta & "SELECT TOP 1 @IDESCENARIO=ID FROM ESCENARIO E WITH(NOLOCK) WHERE E.PORTAL=1 AND COALESCE(E.TIPOVISOR_PORTAL,E.TIPOVISOR)=@VISORESC ORDER BY NOMBRE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion3_3_3506_A3_3_3507() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim oADOConnection As ADODB.Connection
    Dim sConsulta As String

    On Error GoTo Error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Actualiza Tablas"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    frmProgreso.ProgressBar1.Refresh
    V_3_5_Tablas_007
    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Actualiza Storeds"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    frmProgreso.ProgressBar1.Refresh
    V_3_5_Storeds_007

    sConsulta = "UPDATE VERSION SET NUM ='3.00.35.007'" & ", FECHA=CONVERT(datetime,'" & Format(Now, "mm-dd-yyyy hh:mm:ss") & "',110), PRODVER='3.5.2'"
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False

    CodigoDeActualizacion3_3_3506_A3_3_3507 = True
    Exit Function

Error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If

    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    CodigoDeActualizacion3_3_3506_A3_3_3507 = False
End Function

Private Sub V_3_5_Tablas_007()
    Dim sConsulta As String
sConsulta = "IF NOT EXISTS (SELECT * FROM SYS.COLUMNS WHERE NAME=N'TIPO_NOTIF_PROVE_CAMBIOS_CAL' AND Object_ID = Object_ID(N'PARGEN_QA'))" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PARGEN_QA ADD TIPO_NOTIF_PROVE_CAMBIOS_CAL TINYINT NOT NULL CONSTRAINT [DF_PARGEN_QA_TIPO_NOTIF_PROVE_CAMBIOS_CAL]  DEFAULT (0)" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF NOT EXISTS (SELECT * FROM SYS.COLUMNS WHERE NAME=N'VER_ATRIB_ESP_EXCEL_OFE' AND Object_ID = Object_ID(N'PROCE_DEF'))" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_DEF ADD VER_ATRIB_ESP_EXCEL_OFE TINYINT NOT NULL CONSTRAINT [DF_PROCE_DEF_VER_ATRIB_ESP_EXCEL_OFE]  DEFAULT (0)" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF NOT EXISTS (SELECT * FROM SYS.COLUMNS WHERE NAME=N'VER_DIST_ESP_EXCEL_OFE' AND Object_ID = Object_ID(N'PROCE_DEF'))" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_DEF ADD VER_DIST_ESP_EXCEL_OFE TINYINT NOT NULL CONSTRAINT [DF_PROCE_DEF_VER_DIST_ESP_EXCEL_OFE]  DEFAULT (0)" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF NOT EXISTS (SELECT * FROM SYS.COLUMNS WHERE NAME=N'MOSTRAR_TODOS_NIVELES_MATERIAL' AND Object_ID = Object_ID(N'PARGEN_INTERNO'))" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE dbo.PARGEN_INTERNO ADD MOSTRAR_TODOS_NIVELES_MATERIAL tinyint NOT NULL CONSTRAINT DF_PARGEN_INTERNO_MOSTRAR_TODOS_NIVELES_MATERIAL DEFAULT 0" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

End Sub

Private Sub V_3_5_Storeds_007()
    Dim sConsulta As String

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSN_GET_TIPOS_SOLICITUD]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSN_GET_TIPOS_SOLICITUD]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSN_GET_TIPOS_SOLICITUD]" & vbCrLf
sConsulta = sConsulta & "@USU VARCHAR(50)," & vbCrLf
sConsulta = sConsulta & "@IDI VARCHAR(50)," & vbCrLf
sConsulta = sConsulta & "@TIPOSOLICITUD TINYINT=NULL," & vbCrLf
sConsulta = sConsulta & "@IDSSOLICITUD NVARCHAR(MAX)=NULL," & vbCrLf
sConsulta = sConsulta & "@PROVE VARCHAR(50)=NULL" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @INFIJO NVARCHAR(10)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SUSTITUTO AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @EMP INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PER VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "SELECT @PER=PER FROM USU WITH(NOLOCK) WHERE COD=@USU" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF (SELECT COUNT(*) FROM USU U WITH (NOLOCK) WHERE U.FSWS_SUSTITUCION=@PER)>0" & vbCrLf
sConsulta = sConsulta & "SET @SUSTITUTO=1" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SUSTITUTO=0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE #T_PER(COD VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, SOLICITUD INT)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE #T_ROL (SOLICITUD INT, PER NVARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL," & vbCrLf
sConsulta = sConsulta & "UON0 VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL," & vbCrLf
sConsulta = sConsulta & "UON1 VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL," & vbCrLf
sConsulta = sConsulta & "UON2 VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL," & vbCrLf
sConsulta = sConsulta & "UON3 VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL," & vbCrLf
sConsulta = sConsulta & "DEP VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, " & vbCrLf
sConsulta = sConsulta & "TIPO INT," & vbCrLf
sConsulta = sConsulta & "      PROVE VARCHAR(50)  COLLATE SQL_Latin1_General_CP1_CI_AS NULL)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO #T_ROL" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT I.SOLICITUD,IR.PER,R.UON0,IR.UON1,IR.UON2,IR.UON3,IR.DEP,R.TIPO,NULL" & vbCrLf
sConsulta = sConsulta & "FROM PM_COPIA_ROL R WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN INSTANCIA_ROL IR WITH(NOLOCK) ON IR.ROL=R.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN INSTANCIA I WITH(NOLOCK) ON I.ID=IR.INSTANCIA" & vbCrLf
sConsulta = sConsulta & "WHERE R.TIPO=5 AND IR.PER IS NULL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO #T_ROL" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT I.SOLICITUD,IR.PER,R.UON0,IR.UON1,IR.UON2,IR.UON3,IR.DEP,R.TIPO,NULL" & vbCrLf
sConsulta = sConsulta & "FROM PM_COPIA_ROL R WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN INSTANCIA_ROL IR WITH(NOLOCK) ON IR.ROL=R.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN INSTANCIA I WITH(NOLOCK) ON I.ID=IR.INSTANCIA" & vbCrLf
sConsulta = sConsulta & "WHERE IR.PER=@PER" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO #T_ROL" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT I.SOLICITUD,R.PER,R.UON0,R.UON1,R.UON2,R.UON3,R.DEP,R.TIPO,NULL" & vbCrLf
sConsulta = sConsulta & "FROM PM_COPIA_ROL R WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN INSTANCIA_ROL IR WITH(NOLOCK) ON IR.ROL=R.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN INSTANCIA I WITH(NOLOCK) ON I.ID=IR.INSTANCIA" & vbCrLf
sConsulta = sConsulta & "WHERE R.UON0=1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF NOT @PROVE IS NULL" & vbCrLf
sConsulta = sConsulta & "       INSERT INTO #T_ROL" & vbCrLf
sConsulta = sConsulta & "          SELECT DISTINCT I.SOLICITUD,NULL,R.UON0,IR.UON1,IR.UON2,IR.UON3,IR.DEP,R.TIPO,IR.PROVE" & vbCrLf
sConsulta = sConsulta & "          FROM PM_COPIA_ROL R WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "          INNER JOIN INSTANCIA_ROL IR WITH(NOLOCK) ON IR.ROL=R.ID" & vbCrLf
sConsulta = sConsulta & "          INNER JOIN INSTANCIA I WITH(NOLOCK) ON I.ID=IR.INSTANCIA" & vbCrLf
sConsulta = sConsulta & "          WHERE IR.PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO #T_PER" & vbCrLf
sConsulta = sConsulta & "SELECT COD,R.SOLICITUD" & vbCrLf
sConsulta = sConsulta & "FROM PER P WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN #T_ROL R WITH(NOLOCK) ON P.COD=@PER AND P.UON1=R.UON1" & vbCrLf
sConsulta = sConsulta & "AND ISNULL(P.UON2,'')=ISNULL(R.UON2,'') AND ISNULL(P.UON3,'')=ISNULL(R.UON3,'')" & vbCrLf
sConsulta = sConsulta & "AND ISNULL(P.DEP,'')=ISNULL(R.DEP,'') AND R.PER IS NULL" & vbCrLf
sConsulta = sConsulta & "UNION" & vbCrLf
sConsulta = sConsulta & "SELECT COD,R.SOLICITUD" & vbCrLf
sConsulta = sConsulta & "FROM PER P WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN #T_ROL R WITH(NOLOCK) ON P.COD=@PER AND P.UON1=R.UON1" & vbCrLf
sConsulta = sConsulta & "AND ISNULL(P.UON2,'')=ISNULL(R.UON2,'') AND ISNULL(P.UON3,'')=ISNULL(R.UON3,'')" & vbCrLf
sConsulta = sConsulta & "AND R.DEP IS NULL AND R.PER IS NULL" & vbCrLf
sConsulta = sConsulta & "UNION" & vbCrLf
sConsulta = sConsulta & "SELECT COD,R.SOLICITUD" & vbCrLf
sConsulta = sConsulta & "FROM PER P WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN #T_ROL R WITH(NOLOCK) ON P.COD=@PER AND P.UON1=R.UON1" & vbCrLf
sConsulta = sConsulta & "AND ISNULL(P.UON2,'')=ISNULL(R.UON2,'') AND R.UON3 IS NULL AND R.DEP IS NULL AND R.PER IS NULL" & vbCrLf
sConsulta = sConsulta & "UNION" & vbCrLf
sConsulta = sConsulta & "SELECT COD,R.SOLICITUD" & vbCrLf
sConsulta = sConsulta & "FROM PER P WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN #T_ROL R WITH(NOLOCK) ON P.COD=@PER AND P.UON1=R.UON1 AND R.UON2 IS NULL" & vbCrLf
sConsulta = sConsulta & "AND R.UON3 IS NULL AND R.DEP IS NULL AND R.PER IS NULL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @IDSSOLICITUD IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SQL=N'SELECT S.ID,PF.FORMULARIO,S.DEN_'+@IDI+' DEN '" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'FROM SOLICITUD S WITH(NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'INNER JOIN TIPO_SOLICITUDES TS WITH(NOLOCK) ON TS.ID=S.TIPO '" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'INNER JOIN FORMULARIO F WITH(NOLOCK) ON S.FORMULARIO=F.ID '" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'INNER JOIN PM_FILTROS PF WITH(NOLOCK) ON F.ID=PF.FORMULARIO '" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'INNER JOIN PM_COPIA_WORKFLOW PW WITH(NOLOCK) ON PW.WORKFLOW_ORIGEN=S.WORKFLOW AND PW.ULTIMO=1 '" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'INNER JOIN PM_COPIA_ROL R WITH(NOLOCK) ON R.WORKFLOW=PW.ID '" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'AND ((R.PER=@PER OR R.SUSTITUTO=@PER) OR (R.TIPO=5 AND (EXISTS (SELECT TOP 1 V.COD '" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'FROM VIEW_ROL_OBSERVADOR V WITH(NOLOCK) WHERE S.ID=V.SOLICITUD AND V.COD=@PER) OR R.UON0=1))) '" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'WHERE S.PUB=1 AND S.BAJA=0 '" & vbCrLf
sConsulta = sConsulta & "IF @TIPOSOLICITUD IS NULL" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'AND TS.TIPO IN (0,1,4,6,8,9) '" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'AND TS.TIPO=@TIPOSOLICITUD '" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'UNION '" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'SELECT S.ID,PF.FORMULARIO,S.DEN_'+@IDI+' DEN '" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'FROM SOLICITUD S WITH(NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'INNER JOIN TIPO_SOLICITUDES TS WITH(NOLOCK) ON TS.ID=S.TIPO '" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'INNER JOIN FORMULARIO F WITH(NOLOCK) ON S.FORMULARIO=F.ID '" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'INNER JOIN PM_FILTROS PF WITH(NOLOCK) ON F.ID=PF.FORMULARIO '" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'INNER JOIN VIEW_ROL_PETICIONARIO R WITH(NOLOCK) ON S.ID=R.SOLICITUD AND R.COD=@PER '" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'WHERE S.PUB=1 AND S.BAJA=0 '" & vbCrLf
sConsulta = sConsulta & "IF @TIPOSOLICITUD IS NULL" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'AND TS.TIPO IN (0,1,4,6,8,9) '" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'AND TS.TIPO=@TIPOSOLICITUD '" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'UNION '" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'SELECT S.ID,PF.FORMULARIO,S.DEN_'+@IDI+' DEN '" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'FROM SOLICITUD S WITH(NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'INNER JOIN TIPO_SOLICITUDES TS WITH(NOLOCK) ON TS.ID=S.TIPO '" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'INNER JOIN FORMULARIO F WITH(NOLOCK) ON S.FORMULARIO=F.ID '" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'INNER JOIN PM_FILTROS PF WITH(NOLOCK) ON F.ID=PF.FORMULARIO '" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'INNER JOIN #T_ROL R WITH(NOLOCK) ON S.ID=R.SOLICITUD '" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'AND ('" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "       IF @PROVE IS NULL" & vbCrLf
sConsulta = sConsulta & "           BEGIN" & vbCrLf
sConsulta = sConsulta & "              IF @SUSTITUTO=1" & vbCrLf
sConsulta = sConsulta & "                  SET @SQL=@SQL+N'R.PER IN (SELECT U.PER FROM USU U WITH(NOLOCK) WHERE U.FSWS_SUSTITUCION=@PER) '" & vbCrLf
sConsulta = sConsulta & "              ELSE" & vbCrLf
sConsulta = sConsulta & "                  SET @SQL=@SQL+N'R.PER=@PER '         " & vbCrLf
sConsulta = sConsulta & "               SET @SQL=@SQL+N'OR (R.TIPO=5 AND EXISTS (SELECT COD FROM #T_PER TP WITH(NOLOCK) WHERE TP.SOLICITUD=R.SOLICITUD) '" & vbCrLf
sConsulta = sConsulta & "               SET @SQL=@SQL+N'OR R.UON0=1)'" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "       ELSE" & vbCrLf
sConsulta = sConsulta & "           SET @SQL=@SQL+N'R.PROVE=@PROVE'" & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "       SET @SQL=@SQL+N') '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'WHERE S.PUB=1 AND S.BAJA=0 '" & vbCrLf
sConsulta = sConsulta & "IF @TIPOSOLICITUD IS NULL" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'AND TS.TIPO IN (0,1,4,6,8,9) '" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'AND TS.TIPO=@TIPOSOLICITUD '" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'ORDER BY DEN'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SQL=N'SELECT DISTINCT S.ID,S.DEN_'+@IDI+' DEN '" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'FROM SOLICITUD S WITH(NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'INNER JOIN TIPO_SOLICITUDES TS WITH(NOLOCK) ON TS.ID=S.TIPO '" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'WHERE S.PUB=1 AND S.BAJA=0 AND S.ID IN('+@IDSSOLICITUD+N') '" & vbCrLf
sConsulta = sConsulta & "IF @TIPOSOLICITUD IS NULL" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'AND TS.TIPO IN (0,1,4,6,8,9) '" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'AND TS.TIPO=@TIPOSOLICITUD '" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'ORDER BY ID'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@PER VARCHAR(50),@IDSSOLICITUD NVARCHAR(MAX),@TIPOSOLICITUD TINYINT,@PROVE VARCHAR(50)',@PER=@PER,@IDSSOLICITUD=@IDSSOLICITUD,@TIPOSOLICITUD=@TIPOSOLICITUD,@PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DROP TABLE #T_ROL" & vbCrLf
sConsulta = sConsulta & "DROP TABLE #T_PER" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSGS_ANULAR_ORDEN_ENTREGA]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSGS_ANULAR_ORDEN_ENTREGA]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSGS_ANULAR_ORDEN_ENTREGA]" & vbCrLf
sConsulta = sConsulta & "@ID int," & vbCrLf
sConsulta = sConsulta & "@CODPERSONA varchar(50)," & vbCrLf
sConsulta = sConsulta & "@SM_ACTIVO BIT," & vbCrLf
sConsulta = sConsulta & "@IDIOMA varchar(10)" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE @TIPORECEP TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO SMALLINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN1 VARCHAR(25)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCE INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @TIPOPEDIDO TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @MONEDA VARCHAR(25)" & vbCrLf
sConsulta = sConsulta & "declare @COMPROMETIDO FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PED_APROV TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ESTADO SMALLINT" & vbCrLf
sConsulta = sConsulta & "declare @COUNT INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @TIPOABIERTO TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @INSTANCIA INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @LINEAPEDIDO INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @CANT_PEDIDA FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @IMPORTE_PEDIDO FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @SM_IMPORTECOMPR FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @SM_IMPORTECOMPRIMPUESTOS FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDCAMPOSOLICIT INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRES0 NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRES1 NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRES2 NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRES3 NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRES4 NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @ACUMULARIMP TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @COMPR_ACUM TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDESTADO INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @INSTANCIA_APROB INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @INSTANCIA_EST INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_LINEA INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @CONTROLDISPCONPRES TINYINT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET NOCOUNT ON;" & vbCrLf
sConsulta = sConsulta & "SELECT @MONEDA=MONCEN FROM PARGEN_DEF with (nolock)" & vbCrLf
sConsulta = sConsulta & "SELECT @ESTADO=EST,@TIPOPEDIDO=TIPO FROM ORDEN_ENTREGA with (nolock) WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "--La transaccion debe ir en el programa" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO ORDEN_EST ([PEDIDO],[ORDEN],[EST],[FECHA],[PER])  SELECT OE.PEDIDO, OE.ID, 20, GETDATE(), @CODPERSONA" & vbCrLf
sConsulta = sConsulta & "FROM ORDEN_ENTREGA OE WITH (NOLOCK)  WHERE (OE.id = @ID)" & vbCrLf
sConsulta = sConsulta & "SET @IDESTADO=SCOPE_IDENTITY()" & vbCrLf
sConsulta = sConsulta & "UPDATE ORDEN_ENTREGA SET EST=20,ORDEN_EST=@IDESTADO WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Si es un pedido contra abierto, resto a la linea del pedido abierto. Pueden ser varias l?neas a restar." & vbCrLf
sConsulta = sConsulta & "if (select count(*) FROM LINEAS_PEDIDO LP WITH (NOLOCK) INNER JOIN ORDEN_ENTREGA OE WITH (NOLOCK) ON OE.ID=LP.ORDEN WHERE OE.ID=@ID and LP.LINEA_PED_ABIERTO IS NOT NULL)>0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE PED CURSOR LOCAL FOR" & vbCrLf
sConsulta = sConsulta & "SELECT LP.ID" & vbCrLf
sConsulta = sConsulta & "FROM LINEAS_PEDIDO IA WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN LINEAS_PEDIDO LP WITH (NOLOCK) ON IA.id=LP.LINEA_PED_ABIERTO" & vbCrLf
sConsulta = sConsulta & "INNER JOIN ORDEN_ENTREGA OEA WITH (NOLOCK) ON OEA.ID=IA.ORDEN" & vbCrLf
sConsulta = sConsulta & "WHERE LP.ORDEN=@ID AND LP.BAJA_LOG=0" & vbCrLf
sConsulta = sConsulta & "OPEN PED" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM PED INTO @ID_LINEA" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "UPDATE IA SET IMPORTE_PED_ABIERTO=CASE WHEN OEA.PED_ABIERTO_TIPO=2 THEN IA.IMPORTE_PED_ABIERTO-LP.IMPORTE_PED ELSE IA.IMPORTE_PED_ABIERTO END" & vbCrLf
sConsulta = sConsulta & ",   CANTIDAD_PED_ABIERTO=CASE WHEN OEA.PED_ABIERTO_TIPO=3 THEN IA.CANTIDAD_PED_ABIERTO-(LP.CANT_PED*LP.FC) ELSE IA.CANTIDAD_PED_ABIERTO END" & vbCrLf
sConsulta = sConsulta & "FROM LINEAS_PEDIDO IA WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN LINEAS_PEDIDO LP WITH (NOLOCK) ON IA.id=LP.LINEA_PED_ABIERTO" & vbCrLf
sConsulta = sConsulta & "INNER JOIN ORDEN_ENTREGA OEA WITH (NOLOCK) ON OEA.ID=IA.ORDEN" & vbCrLf
sConsulta = sConsulta & "WHERE LP.ORDEN=@ID AND LP.BAJA_LOG=0 AND LP.ID=@ID_LINEA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM PED INTO @ID_LINEA" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "CLOSE PED" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE PED" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Actualizar la cantidad/importe pedido de la adjudicaci?n si se hab?a emitido" & vbCrLf
sConsulta = sConsulta & "IF @ESTADO>=2 AND @ESTADO<20" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_ADJ SET CANT_PED=case when LP.TIPORECEPCION=0 THEN IA.CANT_PED-(LP.CANT_PED*LP.FC) ELSE IA.CANT_PED END," & vbCrLf
sConsulta = sConsulta & "IMPORTE_PED=case when LP.TIPORECEPCION=1 THEN IA.IMPORTE_PED-(LP.IMPORTE_PED) ELSE IA.IMPORTE_PED END ,PEDIDO = NULL" & vbCrLf
sConsulta = sConsulta & "FROM ITEM_ADJ IA WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN LINEAS_PEDIDO LP WITH (NOLOCK) ON IA.ANYO=LP.ANYO AND IA.GMN1=LP.GMN1 AND IA.PROCE=LP.PROCE" & vbCrLf
sConsulta = sConsulta & "AND IA.ITEM=LP.ITEM AND IA.PROVE=ISNULL(LP.PROVEADJ,LP.PROVE)" & vbCrLf
sConsulta = sConsulta & "WHERE LP.ORDEN=@ID AND LP.BAJA_LOG=0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Si la l?nea tiene proceso de compra desvincular el proceso del pedido si el proceso no tiene" & vbCrLf
sConsulta = sConsulta & "--m?s l?neas en el pedido o m?s pedidos" & vbCrLf
sConsulta = sConsulta & "IF @TIPOPEDIDO<>1  --Pedido negociado, directos" & vbCrLf
sConsulta = sConsulta & "--Mirar otras l?neas de pedido para el proceso" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE C1 CURSOR LOCAL FOR SELECT LA.ANYO,LA.GMN1,LA.PROCE,P.PED_APROV,LA.TIPORECEPCION,COUNT(LP.ID) FROM LINEAS_PEDIDO LA WITH (NOLOCK) INNER JOIN LINEAS_PEDIDO LP WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "ON LA.ANYO=LP.ANYO AND LA.GMN1=LP.GMN1 AND LA.PROCE=LP.PROCE AND LA.ORDEN<>LP.ORDEN" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN PROCE P WITH (NOLOCK) ON P.ANYO=LA.ANYO AND P.GMN1=LA.GMN1 AND P.COD=LA.PROCE" & vbCrLf
sConsulta = sConsulta & "WHERE LA.ORDEN=@ID AND LA.ANYO IS NOT NULL AND LA.BAJA_LOG=0 GROUP BY LA.ANYO,LA.GMN1,LA.PROCE,P.PED_APROV,LA.TIPORECEPCION" & vbCrLf
sConsulta = sConsulta & "OPEN C1" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C1 INTO @ANYO,@GMN1,@PROCE,@PED_APROV,@TIPORECEP,@COUNT" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @COUNT=0" & vbCrLf
sConsulta = sConsulta & "--Si el proceso NO est? en otros pedidos se pone PED_APROV=0 Sin pedidos" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET PED_APROV=0 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @PED_APROV=3     --Pedido excedido, miro si sigue habiendo lineas excedidas" & vbCrLf
sConsulta = sConsulta & "IF (@TIPORECEP=0 AND  NOT EXISTS (SELECT COUNT(ITEM_ADJ.ANYO)  FROM ITEM_ADJ  WITH (NOLOCK) WHERE CANT_PED>ISNULL(CANT_ADJ,0) AND ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE)) OR" & vbCrLf
sConsulta = sConsulta & "(@TIPORECEP=1 AND  NOT EXISTS (SELECT COUNT(ITEM_ADJ.ANYO)  FROM ITEM_ADJ  WITH (NOLOCK) WHERE IMPORTE_PED>ISNULL(ADJUDICADO,0) AND ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE))" & vbCrLf
sConsulta = sConsulta & "--Con pedidos" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET PED_APROV=2 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C1 INTO @ANYO,@GMN1,@PROCE,@PED_APROV,@TIPORECEP,@COUNT" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "CLOSE C1" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C1" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE --Pedido de cat?logo" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET PED_APROV=0 FROM PROCE P WITH (NOLOCK) INNER JOIN LINEAS_PEDIDO LA WITH (NOLOCK) ON P.ANYO=LA.ANYO AND P.GMN1=LA.GMN1 AND P.COD=LA.PROCE" & vbCrLf
sConsulta = sConsulta & "WHERE LA.ORDEN=@ID AND LA.ANYO IS NOT NULL AND LA.BAJA_LOG=0 AND NOT EXISTS (SELECT *  FROM CATALOG_LIN CL WITH (NOLOCK) WHERE  P.ANYO=CL.ANYO AND P.GMN1=CL.GMN1 AND P.COD=CL.PROCE)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if @SM_ACTIVO=1" & vbCrLf
sConsulta = sConsulta & "begin" & vbCrLf
sConsulta = sConsulta & "--sI HAY SM RESTAR COMPROMETIDO, SUMAR SOLICITADO" & vbCrLf
sConsulta = sConsulta & "DECLARE C2 CURSOR LOCAL FOR" & vbCrLf
sConsulta = sConsulta & "      SELECT LP.ID,LP.CANT_PED,LP.IMPORTE_PED,LP.TIPORECEPCION, LP.SM_IMPORTECOMPR,LP.SM_IMPORTECOMPRIMPUESTOS,LP.SOLICIT,LP.CAMPO_SOLICIT,LPI.PRES0,LPI.PRES1,LPI.PRES2,LPI.PRES3,LPI.PRES4," & vbCrLf
sConsulta = sConsulta & "           SM.ACUMULARIMP,SM.COMPR_ACUM,SM.CONTROL_DISP_SC_CON_PRES" & vbCrLf
sConsulta = sConsulta & "      FROM LINEAS_PEDIDO LP WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN LINEAS_PED_IMPUTACION LPI WITH (NOLOCK) ON LPI.LINEA=LP.ID" & vbCrLf
sConsulta = sConsulta & "          INNER JOIN PARGEN_SM SM WITH (NOLOCK) ON SM.PRES5=LPI.PRES0" & vbCrLf
sConsulta = sConsulta & "WHERE LP.ORDEN=@ID AND LP.BAJA_LOG=0" & vbCrLf
sConsulta = sConsulta & "OPEN C2" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C2 INTO @LINEAPEDIDO,@CANT_PEDIDA,@IMPORTE_PEDIDO,@TIPORECEP,@SM_IMPORTECOMPR,@SM_IMPORTECOMPRIMPUESTOS,@INSTANCIA,@IDCAMPOSOLICIT,@PRES0,@PRES1,@PRES2,@PRES3,@PRES4 ," & vbCrLf
sConsulta = sConsulta & "           @ACUMULARIMP,@COMPR_ACUM,@CONTROLDISPCONPRES" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @COMPR_ACUM=1 --Si se acumula el comprometido en emision" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @COMPROMETIDO = (-1) * case when (@ACUMULARIMP=1) then @SM_IMPORTECOMPRIMPUESTOS else @SM_IMPORTECOMPR end" & vbCrLf
sConsulta = sConsulta & "IF NOT (@TIPORECEP= 0 And @CANT_PEDIDA<> 0 And @INSTANCIA<> 0)" & vbCrLf
sConsulta = sConsulta & "SET @CANT_PEDIDA= Null" & vbCrLf
sConsulta = sConsulta & "IF NOT (@TIPORECEP= 1 And @IMPORTE_PEDIDO<> 0 And @INSTANCIA<> 0)" & vbCrLf
sConsulta = sConsulta & "SET @IMPORTE_PEDIDO=NULL                                               " & vbCrLf
sConsulta = sConsulta & "                                   " & vbCrLf
sConsulta = sConsulta & "EXEC FSSM_ACTUALIZAR_IMPORTES_PARTIDA" & vbCrLf
sConsulta = sConsulta & "                  @IDIOMA, @PRES0,@PRES1,@PRES2,@PRES3,@PRES4,0,@CONTROLDISPCONPRES,@INSTANCIA,@MONEDA,@CANT_PEDIDA,@IMPORTE_PEDIDO,@IDCAMPOSOLICIT,NULL,@LINEAPEDIDO,@COMPROMETIDO                " & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C2 INTO @LINEAPEDIDO,@CANT_PEDIDA,@IMPORTE_PEDIDO,@TIPORECEP,@SM_IMPORTECOMPR,@SM_IMPORTECOMPRIMPUESTOS,@INSTANCIA,@IDCAMPOSOLICIT,@PRES0,@PRES1,@PRES2,@PRES3,@PRES4 ," & vbCrLf
sConsulta = sConsulta & "               @ACUMULARIMP,@COMPR_ACUM,@CONTROLDISPCONPRES" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "CLOSE C2" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C2" & vbCrLf
sConsulta = sConsulta & "--'Se elimina la imputacion" & vbCrLf
sConsulta = sConsulta & "--Berge / 2017 / 116: Obligatorio en la emisi?n de pedidos y se pierden cuando el pedido se anula. Esto supone que los usuarios que tengan permiso para ver pedidos de otros usuarios (basado en el centro de coste) no los vean desde el seguimiento de pedidos." & vbCrLf
sConsulta = sConsulta & "--DELETE FROM LINEAS_PED_IMPUTACION FROM LINEAS_PED_IMPUTACION LPI WITH (NOLOCK) INNER JOIN LINEAS_PEDIDO LP WITH (NOLOCK) ON LP.ID = LPI.LINEA WHERE LP.Orden = @ID" & vbCrLf
sConsulta = sConsulta & "end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ESTADO<2" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE C3 CURSOR LOCAL FOR SELECT LP.INSTANCIA_APROB,I.ESTADO" & vbCrLf
sConsulta = sConsulta & "FROM LINEAS_PEDIDO LP WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN INSTANCIA I WITH (NOLOCK) ON LP.INSTANCIA_APROB=I.ID" & vbCrLf
sConsulta = sConsulta & "WHERE LP.ORDEN=@ID" & vbCrLf
sConsulta = sConsulta & "OPEN C3" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C3 INTO @INSTANCIA_APROB,@INSTANCIA_EST" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @INSTANCIA_EST<>8" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "EXEC FSWS_ANULAR_SOLICITUD @INSTANCIA_APROB,@CODPERSONA,'PEDIDO ASOCIADO ANULADO',1" & vbCrLf
sConsulta = sConsulta & "IF (SELECT COUNT(INSTANCIA) FROM PM_INSTANCIAS_PENDIENTES WHERE INSTANCIA=@INSTANCIA_APROB)>0" & vbCrLf
sConsulta = sConsulta & "DELETE FROM PM_INSTANCIAS_PENDIENTES WHERE INSTANCIA=@INSTANCIA_APROB" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C3 INTO @INSTANCIA_APROB,@INSTANCIA_EST" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "CLOSE C3" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C3" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSQA_GETPARAMETROS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSQA_GETPARAMETROS]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSQA_GETPARAMETROS] AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT ID,FSQA_PROX_EXPIRAR,PERIODICIDAD_EMAIL,POT_A_VAL_ADJUDICACION,POT_A_VAL_PEDDIR_GS,POT_A_VAL_PED_EP,POT_A_VAL_PROVISIONAL_SEL_PROVE," & vbCrLf
sConsulta = sConsulta & "NOTIF_USU_ASIG_MAT_VAL,NOTIF_USU_ASIG_MAT_POT,NOTIF_USU_DESASIG_MAT_VAL,NOTIF_USU_DESASIG_MAT_POT," & vbCrLf
sConsulta = sConsulta & "NOTIF_USU_ALTA_MAT_GS,CONTACCERTAUTOM,PERIODO_VALIDEZ_CERT_EXP,NOTIF_PROVE_CAMBIOS_CAL,NOTIF_NC_FECFINRESOLUCION," & vbCrLf
sConsulta = sConsulta & "NOTIF_NC_FECFINACCIONES,PERIODO_NOTIF_NC,NIVELES_DEFECTO_UNQA," & vbCrLf
sConsulta = sConsulta & "(SELECT ISNULL(MAX(NIVEL),0) FROM UNQA WITH(NOLOCK)) MAXIMONIVELUNQA ,FILTRO_MAT,TIPO_NOTIF_PROVE_CAMBIOS_CAL" & vbCrLf
sConsulta = sConsulta & "FROM PARGEN_QA WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSQA_GUARDAR_PARAMETROS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSQA_GUARDAR_PARAMETROS]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSQA_GUARDAR_PARAMETROS] @PROX_EXPIRAR INT, @POT_A_VAL_ADJUDICACION TINYINT = 0," & vbCrLf
sConsulta = sConsulta & "@POT_A_VAL_PEDDIR_GS TINYINT = 0, @POT_A_VAL_PED_EP TINYINT = 0, @NOTIF_USU_ASIG_MAT_VAL TINYINT = 0," & vbCrLf
sConsulta = sConsulta & "@NOTIF_USU_ASIG_MAT_POT TINYINT = 0, @NOTIF_USU_DESASIG_MAT_VAL TINYINT = 0, @NOTIF_USU_DESASIG_MAT_POT TINYINT = 0," & vbCrLf
sConsulta = sConsulta & "@NOTIF_USU_ALTA_MAT_GS TINYINT = 0, @CONTACCERTAUTOM VARCHAR(20)='', @PERIODO_VALIDEZ_CERT_EXP SMALLINT=0," & vbCrLf
sConsulta = sConsulta & "@NOTIF_PROVE_CAMBIOS_CAL TINYINT=0, @NOTIF_NC_FECFINRESOLUCION TINYINT=0,@NOTIF_NC_FECFINACCIONES TINYINT=0," & vbCrLf
sConsulta = sConsulta & "@PERIODO_NOTIF_NC SMALLINT=0,@NIVELES_DEFECTO_UNQA NVARCHAR(50)='',@FILTRO_MAT TINYINT=NULL, @POT_A_VAL_PROVISIONAL_SEL_PROVE TINYINT=0," & vbCrLf
sConsulta = sConsulta & "@TIPO_NOTIF_PROVE_CAMBIOS_CAL TINYINT=0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE PARGEN_QA SET" & vbCrLf
sConsulta = sConsulta & "FSQA_PROX_EXPIRAR=@PROX_EXPIRAR," & vbCrLf
sConsulta = sConsulta & "POT_A_VAL_ADJUDICACION=@POT_A_VAL_ADJUDICACION," & vbCrLf
sConsulta = sConsulta & "POT_A_VAL_PEDDIR_GS=@POT_A_VAL_PEDDIR_GS," & vbCrLf
sConsulta = sConsulta & "POT_A_VAL_PED_EP=@POT_A_VAL_PED_EP," & vbCrLf
sConsulta = sConsulta & "NOTIF_USU_ASIG_MAT_VAL=@NOTIF_USU_ASIG_MAT_VAL," & vbCrLf
sConsulta = sConsulta & "NOTIF_USU_ASIG_MAT_POT=@NOTIF_USU_ASIG_MAT_POT," & vbCrLf
sConsulta = sConsulta & "NOTIF_USU_DESASIG_MAT_VAL=@NOTIF_USU_DESASIG_MAT_VAL," & vbCrLf
sConsulta = sConsulta & "NOTIF_USU_DESASIG_MAT_POT=@NOTIF_USU_DESASIG_MAT_POT," & vbCrLf
sConsulta = sConsulta & "NOTIF_USU_ALTA_MAT_GS=@NOTIF_USU_ALTA_MAT_GS," & vbCrLf
sConsulta = sConsulta & "CONTACCERTAUTOM=@CONTACCERTAUTOM," & vbCrLf
sConsulta = sConsulta & "PERIODO_VALIDEZ_CERT_EXP=@PERIODO_VALIDEZ_CERT_EXP," & vbCrLf
sConsulta = sConsulta & "NOTIF_PROVE_CAMBIOS_CAL=@NOTIF_PROVE_CAMBIOS_CAL," & vbCrLf
sConsulta = sConsulta & "NOTIF_NC_FECFINRESOLUCION=@NOTIF_NC_FECFINRESOLUCION," & vbCrLf
sConsulta = sConsulta & "NOTIF_NC_FECFINACCIONES=@NOTIF_NC_FECFINACCIONES," & vbCrLf
sConsulta = sConsulta & "PERIODO_NOTIF_NC=@PERIODO_NOTIF_NC," & vbCrLf
sConsulta = sConsulta & "NIVELES_DEFECTO_UNQA=@NIVELES_DEFECTO_UNQA ," & vbCrLf
sConsulta = sConsulta & "FILTRO_MAT=@FILTRO_MAT," & vbCrLf
sConsulta = sConsulta & "POT_A_VAL_PROVISIONAL_SEL_PROVE=@POT_A_VAL_PROVISIONAL_SEL_PROVE," & vbCrLf
sConsulta = sConsulta & "TIPO_NOTIF_PROVE_CAMBIOS_CAL=@TIPO_NOTIF_PROVE_CAMBIOS_CAL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSQA_UPDATE_PROVE_PUNT]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSQA_UPDATE_PROVE_PUNT]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSQA_UPDATE_PROVE_PUNT] (@PROVE AS VARCHAR(50),@GRABAHISTORICO TINYINT OUTPUT) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @EXISTE AS TINYINT" & vbCrLf
sConsulta = sConsulta & "declare @fecha DATETIME" & vbCrLf
sConsulta = sConsulta & "declare @FECHA_ANT DATETIME" & vbCrLf
sConsulta = sConsulta & "DECLARE @MES INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @MES_ANT INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO_ANT INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @GRABAHISTORICO=0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE VARIABLES cursor for" & vbCrLf
sConsulta = sConsulta & "   SELECT VARCAL,NIVEL,ISNULL(PUNT,0),CAL,ID_FORMULA,UNQA,TIPO,SUBTIPO,ORIGEN,PERIODO,TIPOPOND,FORMULA,VARIABLESXI,APLICA" & vbCrLf
sConsulta = sConsulta & "   FROM #TEMP_VARIABLES T WITH (NOLOCK)        " & vbCrLf
sConsulta = sConsulta & "   WHERE NOT EXISTS (SELECT 1 FROM PROVE_PUNT_ERRORES E WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "           WHERE E.PROVE = @PROVE AND E.UNQA = T.UNQA )" & vbCrLf
sConsulta = sConsulta & "   ORDER BY NIVEL,VARCAL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @VARCAL INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @NIVEL INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PUNT FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @CAL INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_FORMULA INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @UNQA INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @TIPO INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @SUBTIPO INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ORIGEN INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PERIODO INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @TIPOPOND INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @FORMULA NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @VARIABLESXI NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @APLICA BIT" & vbCrLf
sConsulta = sConsulta & "DECLARE @INICIAR TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_ENC INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @NOTIF_PROVE_CAMBIOS_CAL TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @TIPO_NOTIF_PROVE_CAMBIOS_CAL TINYINT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @INICIAR = 1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "------------------" & vbCrLf
sConsulta = sConsulta & "--Si la variable de calidad es simple almacenar la incognita en la tabla" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @inicioX int" & vbCrLf
sConsulta = sConsulta & "DECLARE @finEnc int" & vbCrLf
sConsulta = sConsulta & "DECLARE @inicioXi int" & vbCrLf
sConsulta = sConsulta & "DECLARE @inicioXi1 int" & vbCrLf
sConsulta = sConsulta & "DECLARE @i int" & vbCrLf
sConsulta = sConsulta & "DECLARE @valor nvarchar(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @valorX nvarchar(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @varX nvarchar(10)" & vbCrLf
sConsulta = sConsulta & "DECLARE @valorVarX float" & vbCrLf
sConsulta = sConsulta & "DECLARE @valor1 nvarchar(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @valor2 nvarchar(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @POSXENFORMULA INT" & vbCrLf
sConsulta = sConsulta & "------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @NOTIF_PROVE_CAMBIOS_CAL=NOTIF_PROVE_CAMBIOS_CAL,@TIPO_NOTIF_PROVE_CAMBIOS_CAL=TIPO_NOTIF_PROVE_CAMBIOS_CAL FROM PARGEN_QA WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "OPEN VARIABLES FETCH NEXT FROM VARIABLES INTO @VARCAL,@NIVEL,@PUNT,@CAL,@ID_FORMULA,@UNQA,@TIPO,@SUBTIPO,@ORIGEN,@PERIODO,@TIPOPOND,@FORMULA,@VARIABLESXI,@APLICA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS = 0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @INICIAR = 1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @MES = MONTH(GETDATE())" & vbCrLf
sConsulta = sConsulta & "SET @ANYO = YEAR(GETDATE())" & vbCrLf
sConsulta = sConsulta & "SET @FECHA_ANT=DATEADD(M,-1,GETDATE())" & vbCrLf
sConsulta = sConsulta & "SET @MES_ANT = MONTH(@FECHA_ANT)" & vbCrLf
sConsulta = sConsulta & "SET @ANYO_ANT = YEAR(@FECHA_ANT)" & vbCrLf
sConsulta = sConsulta & "SET @EXISTE=0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT TOP 1  @EXISTE = 1" & vbCrLf
sConsulta = sConsulta & "FROM VAR_PROVE_UNQA_PUNT_HIST WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE MES = @MES_ANT AND ANO = @ANYO_ANT AND PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @EXISTE = 0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "INSERT INTO VAR_PROVE_UNQA_PUNT_HIST(MES, ANO, PROVE, UNQA, VARCAL, NIVEL, PUNT, CAL, ID_FORMULA, TIPO, SUBTIPO, ORIGEN, PERIODO, TIPOPOND,APLICA)" & vbCrLf
sConsulta = sConsulta & "SELECT @MES_ANT, @ANYO_ANT, PROVE, UNQA, VARCAL, NIVEL, PUNT, CAL, ID_FORMULA, TIPO, SUBTIPO, ORIGEN, PERIODO, TIPOPOND,APLICA" & vbCrLf
sConsulta = sConsulta & "FROM VAR_PROVE_UNQA_PUNT WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO VAR_PROVE_UNQA_X_HIST(MES,ANO,PROVE,UNQA,VARCAL,NIVEL,VARIABLEX,VALORX,NC_IDS,ID_ENC)" & vbCrLf
sConsulta = sConsulta & "SELECT @MES_ANT,@ANYO_ANT,PROVE,UNQA,VARCAL,NIVEL,VARIABLEX,VALORX,NC_IDS,ID_ENC" & vbCrLf
sConsulta = sConsulta & "FROM VAR_PROVE_UNQA_X WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "          --Pruebas 32100.3 / 2016 / 217: Si no existe calificaci?n para la variable y los puntos, esta insert da Error. El Idioma es clave, luego NOT NULL, y el CAL_PUNTUACION_IDIOMA.IDIOMA." & vbCrLf
sConsulta = sConsulta & "INSERT INTO CAL_PUNTUACION_HIST (VARCAL, NIVEL, CAL, IDIOMA, MES, ANO, DEN)" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT VP.VARCAL, VP.NIVEL, VP.CAL, CPI.IDIOMA, @MES_ANT, @ANYO_ANT,CPI.DEN" & vbCrLf
sConsulta = sConsulta & "FROM VAR_PROVE_UNQA_PUNT VP WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN CAL_PUNTUACION_IDIOMA CPI WITH (NOLOCK) ON CPI.VARCAL = vp.varcal AND CPI.NIVEL = vp.NIVEL AND CPI.CAL_PUNTUACION = vp.CAL" & vbCrLf
sConsulta = sConsulta & "WHERE VP.PROVE=@PROVE AND VP.CAL > 0 AND NOT EXISTS (SELECT TOP 1 1 FROM CAL_PUNTUACION_HIST WITH (NOLOCK) WHERE VARCAL = VP.VARCAL AND NIVEL = VP.NIVEL AND MES = @MES_ANT AND ANO = @ANYO_ANT)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF (SELECT COUNT(1) FROM VAR_PROVE_UNQA_PUNT_HIST WITH (NOLOCK) WHERE MES=@MES_ANT AND ANO=@ANYO_ANT AND PROVE=@PROVE)>0" & vbCrLf
sConsulta = sConsulta & "SET @GRABAHISTORICO=1" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "DELETE FROM VAR_PROVE_UNQA_X WHERE PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "      --Eliminar registros de tabla de puntuacuiones anteriores" & vbCrLf
sConsulta = sConsulta & "      DELETE FROM VAR_PROVE_UNQA_PUNT_ANT WHERE PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "      --Si hay comunicaci?n y est? configurada con cada cambio se rellena la tabla de puntuaciones anteriores" & vbCrLf
sConsulta = sConsulta & "      IF @NOTIF_PROVE_CAMBIOS_CAL=1 AND @TIPO_NOTIF_PROVE_CAMBIOS_CAL=1" & vbCrLf
sConsulta = sConsulta & "           INSERT INTO VAR_PROVE_UNQA_PUNT_ANT (PROVE,UNQA,VARCAL,NIVEL,PUNT,CAL,FECACT) SELECT PROVE,UNQA,VARCAL,NIVEL,PUNT,CAL,FECACT FROM VAR_PROVE_UNQA_PUNT WITH (NOLOCK) WHERE PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "DELETE FROM VAR_PROVE_UNQA_PUNT WHERE PROVE = @PROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @INICIAR = 0" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @ID_FORMULA = 0" & vbCrLf
sConsulta = sConsulta & "      SET @ID_FORMULA = NULL" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO VAR_PROVE_UNQA_PUNT (PROVE,UNQA,VARCAL,NIVEL,PUNT,CAL,ID_FORMULA,TIPO,SUBTIPO,ORIGEN,PERIODO,TIPOPOND,APLICA)" & vbCrLf
sConsulta = sConsulta & "   VALUES  (@PROVE,@UNQA,@VARCAL,@NIVEL,@PUNT,@CAL,@ID_FORMULA,@TIPO,@SUBTIPO,@ORIGEN,@PERIODO,@TIPOPOND,@APLICA)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @TIPO = 0" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       IF @SUBTIPO = 8 --Encuestas" & vbCrLf
sConsulta = sConsulta & "       BEGIN" & vbCrLf
sConsulta = sConsulta & "           --En el string con los valores de las inc?gnitas de la f?rmula se guarda tambi?n el id de la encuesta" & vbCrLf
sConsulta = sConsulta & "           --IdEncuesta1;incognita1=valor1,incognita2=valor2/IdEncuesta2;incognita1=valor1,incognita2=valor2..." & vbCrLf
sConsulta = sConsulta & "           --Ej: 1000;x1=6,x2=3/1001;x1=4,x2=7..." & vbCrLf
sConsulta = sConsulta & "           SET @ID_ENC=0   " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "           WHILE @VARIABLESXI<>''" & vbCrLf
sConsulta = sConsulta & "           BEGIN" & vbCrLf
sConsulta = sConsulta & "               SET @finEnc = CHARINDEX('/',@VARIABLESXI)" & vbCrLf
sConsulta = sConsulta & "               " & vbCrLf
sConsulta = sConsulta & "               SET @valor=left(@variablesXi,@finEnc-1)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "               SET @inicioXi1 = CHARINDEX(';',@valor)" & vbCrLf
sConsulta = sConsulta & "               SET @valor2=left(@valor,@inicioXi1-1)" & vbCrLf
sConsulta = sConsulta & "               SET @ID_ENC=CAST(@valor2 AS INT)                " & vbCrLf
sConsulta = sConsulta & "                       " & vbCrLf
sConsulta = sConsulta & "               SET @valor1=SUBSTRING(@valor,@inicioXi1+1,len(@valor))              " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "               WHILE @valor1<>''" & vbCrLf
sConsulta = sConsulta & "               BEGIN                   " & vbCrLf
sConsulta = sConsulta & "                   --Variable Xi" & vbCrLf
sConsulta = sConsulta & "                   SET @inicioX = CHARINDEX('=',@valor1)" & vbCrLf
sConsulta = sConsulta & "                   SET @varX=left(@valor1,@inicioX-1)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                   --Valor" & vbCrLf
sConsulta = sConsulta & "                   SET @inicioXi1 = CHARINDEX('|',@valor1)" & vbCrLf
sConsulta = sConsulta & "                   SET @valor2=substring(@valor1,@inicioX+1,@inicioXi1-@inicioX-1)" & vbCrLf
sConsulta = sConsulta & "                   IF @valor2=''" & vbCrLf
sConsulta = sConsulta & "                       set @valorVarX=0" & vbCrLf
sConsulta = sConsulta & "                   else                        " & vbCrLf
sConsulta = sConsulta & "                       SET @valorVarX = CAST(REPLACE(@valor2,',','.') AS FLOAT)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                   INSERT INTO VAR_PROVE_UNQA_X (PROVE,UNQA,VARCAL,NIVEL,VARIABLEX,VALORX,ID_ENC)" & vbCrLf
sConsulta = sConsulta & "                   VALUES (@PROVE, @UNQA,@VARCAL,@NIVEL,@varX,@valorVarX,@ID_ENC)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                   SET @valor1=substring(@valor1,@inicioXi1+1,len(@valor1))" & vbCrLf
sConsulta = sConsulta & "               END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "               SET @VARIABLESXI=SUBSTRING(@VARIABLESXI,@finEnc+1,LEN(@VARIABLESXI))" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "       ELSE" & vbCrLf
sConsulta = sConsulta & "       BEGIN       " & vbCrLf
sConsulta = sConsulta & "           SET @i = 1" & vbCrLf
sConsulta = sConsulta & "           --Proceso que calcula las incognitas Xi de una variable simple" & vbCrLf
sConsulta = sConsulta & "           WHILE (@variablesXi <> '') AND (@I < 150)" & vbCrLf
sConsulta = sConsulta & "           BEGIN           " & vbCrLf
sConsulta = sConsulta & "               SET @VALOR1=0" & vbCrLf
sConsulta = sConsulta & "               SET @VALOR2=NULL" & vbCrLf
sConsulta = sConsulta & "               SELECT @inicioXi = CHARINDEX('/',@variablesXi)" & vbCrLf
sConsulta = sConsulta & "               SELECT @valor=left(@variablesXi,@inicioXi-1)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "               --Variable Xi" & vbCrLf
sConsulta = sConsulta & "               SELECT @inicioX = CHARINDEX('=',@valor)" & vbCrLf
sConsulta = sConsulta & "               SELECT @valorX=left(@valor,@inicioX-1)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "               SELECT @POSXENFORMULA =CHARINDEX(@VALORX, @FORMULA)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "               IF @POSXENFORMULA > 0 ---SI LA INCOGNITA ESTA EN LA FORMULA A?ADIR EN LA TABLA VAR_PROVE_UNQA_X" & vbCrLf
sConsulta = sConsulta & "               BEGIN" & vbCrLf
sConsulta = sConsulta & "                   --Dependiendo si es una noCOnformidad o no realizar distintas operaciones con @valorX" & vbCrLf
sConsulta = sConsulta & "                   IF @SUBTIPO = 4" & vbCrLf
sConsulta = sConsulta & "                   BEGIN" & vbCrLf
sConsulta = sConsulta & "                       SELECT @inicioXi1 = CHARINDEX(';',@valor)" & vbCrLf
sConsulta = sConsulta & "                       SELECT @valor1=left(@valor,@inicioXi1-1)" & vbCrLf
sConsulta = sConsulta & "                       set @valor1 = ltrim(replace(@valor1,@valorX +'=',''))                       " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                       --Si es noConformidad" & vbCrLf
sConsulta = sConsulta & "                       SELECT @valor2=substring(@valor,@inicioXi1+1,len(@valor))" & vbCrLf
sConsulta = sConsulta & "                       IF @valor2='' set @valor2 =0" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "                   ELSE                                                            " & vbCrLf
sConsulta = sConsulta & "                   BEGIN" & vbCrLf
sConsulta = sConsulta & "                       SET @valor1 = lTrim(replace(@valor,@valorX+'=',''))                     " & vbCrLf
sConsulta = sConsulta & "                       SET @VALOR2 = NULL" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                   IF @valor1=''" & vbCrLf
sConsulta = sConsulta & "                       set @valorVarX =0                   " & vbCrLf
sConsulta = sConsulta & "                   else" & vbCrLf
sConsulta = sConsulta & "                       SET @valorVarX = CAST(REPLACE(@VALOR1,',','.') AS FLOAT)" & vbCrLf
sConsulta = sConsulta & "                   INSERT INTO VAR_PROVE_UNQA_X (PROVE,UNQA,VARCAL,NIVEL,VARIABLEX,VALORX,NC_IDS)" & vbCrLf
sConsulta = sConsulta & "                   VALUES (@PROVE, @UNQA,@VARCAL,@NIVEL,@VALORX,@valorVarX,@VALOR2)" & vbCrLf
sConsulta = sConsulta & "               END         " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "               SELECT @variablesXi=substring(@variablesXi,@inicioXi+1,len(@variablesXi))" & vbCrLf
sConsulta = sConsulta & "               SET @i = @i +1" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "       END" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   FETCH NEXT FROM VARIABLES INTO  @VARCAL,@NIVEL,@PUNT,@CAL,@ID_FORMULA,@UNQA,@TIPO,@SUBTIPO,@ORIGEN,@PERIODO,@TIPOPOND,@FORMULA,@VARIABLESXI,@APLICA" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "CLOSE VARIABLES" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE VARIABLES" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSQA_CAMBIO_CALIFICACIONTOTAL]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSQA_CAMBIO_CALIFICACIONTOTAL]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSQA_CAMBIO_CALIFICACIONTOTAL] @PROVE VARCHAR(50) AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @MES_DATOS_ACT INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @MES_DATOS_ANT INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO_DATOS_ACT INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO_DATOS_ANT INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @MES_MOSTRAR_ANT INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO_MOSTRAR_ANT INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @MES_MOSTRAR_ACT INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO_MOSTRAR_ACT INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @TIPO_NOTIF_PROVE_CAMBIOS_CAL TINYINT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @TIPO_NOTIF_PROVE_CAMBIOS_CAL=TIPO_NOTIF_PROVE_CAMBIOS_CAL FROM PARGEN_QA WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @TIPO_NOTIF_PROVE_CAMBIOS_CAL=0" & vbCrLf
sConsulta = sConsulta & "   SELECT TOP 1 @MES_DATOS_ANT=MES,@ANYO_DATOS_ANT=ANO FROM VAR_PROVE_UNQA_PUNT_HIST WITH(NOLOCK) WHERE PROVE=@PROVE ORDER BY ANO DESC,MES DESC" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "   SELECT TOP 1 @MES_DATOS_ANT=MONTH(FECACT),@ANYO_DATOS_ANT=YEAR(FECACT) FROM VAR_PROVE_UNQA_PUNT_ANT WITH(NOLOCK) WHERE PROVE=@PROVE ORDER BY FECACT DESC" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @ANYO_MOSTRAR_ANT = CASE WHEN @MES_DATOS_ANT=1 THEN @ANYO_DATOS_ANT - 1 ELSE @ANYO_DATOS_ANT END" & vbCrLf
sConsulta = sConsulta & "SET @MES_MOSTRAR_ANT = CASE WHEN @MES_DATOS_ANT=1 THEN 12 ELSE @MES_DATOS_ANT - 1 END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @MES_DATOS_ACT=MONTH(GETDATE()),@ANYO_DATOS_ACT=YEAR(GETDATE())" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @ANYO_MOSTRAR_ACT = CASE WHEN @MES_DATOS_ACT=1 THEN @ANYO_DATOS_ACT - 1 ELSE @ANYO_DATOS_ACT END" & vbCrLf
sConsulta = sConsulta & "SET @MES_MOSTRAR_ACT = CASE WHEN @MES_DATOS_ACT=1 THEN 12 ELSE @MES_DATOS_ACT - 1 END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & ";WITH PUNT_ANT AS (" & vbCrLf
sConsulta = sConsulta & "   --Cada mes (se compara con la tabla de hist?rico)" & vbCrLf
sConsulta = sConsulta & "   SELECT PROVE,UNQA,VARCAL,NIVEL,PUNT,CAL" & vbCrLf
sConsulta = sConsulta & "   FROM VAR_PROVE_UNQA_PUNT_HIST WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   WHERE PROVE=@PROVE AND @TIPO_NOTIF_PROVE_CAMBIOS_CAL=0 AND MES=@MES_DATOS_ANT AND ANO=@ANYO_DATOS_ANT" & vbCrLf
sConsulta = sConsulta & "   UNION ALL" & vbCrLf
sConsulta = sConsulta & "   --En cada cambio (se compara con la tabla de puntuaciones anteriores)" & vbCrLf
sConsulta = sConsulta & "   SELECT PROVE,UNQA,VARCAL,NIVEL,PUNT,CAL" & vbCrLf
sConsulta = sConsulta & "   FROM VAR_PROVE_UNQA_PUNT_ANT WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   WHERE PROVE=@PROVE AND @TIPO_NOTIF_PROVE_CAMBIOS_CAL=1" & vbCrLf
sConsulta = sConsulta & ")" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT H.UNQA UNQA_COD,PUNT.CAL CAL_MES_ACTUAL,PUNT.FECACT FECHA_ACTUAL,@MES_MOSTRAR_ACT MES_ACTUAL,@ANYO_MOSTRAR_ACT ANYO_ACTUAL" & vbCrLf
sConsulta = sConsulta & ",H.CAL CAL_MES_ANTERIOR,@MES_MOSTRAR_ANT MES_ANTERIOR,@ANYO_MOSTRAR_ANT ANYO_ANTERIOR" & vbCrLf
sConsulta = sConsulta & ",ACT.TEXT_SPA ASPA,ANT.TEXT_SPA NSPA,ACT.TEXT_ENG AENG,ANT.TEXT_ENG NENG,ACT.TEXT_GER AGER,ANT.TEXT_GER NGER" & vbCrLf
sConsulta = sConsulta & ",CAA.TEXT_SPA CASPA,CAN.TEXT_SPA CNSPA,CAA.TEXT_ENG CAENG,CAN.TEXT_ENG CNENG,CAA.TEXT_GER CAGER,CAN.TEXT_GER CNGER,ACT.TEXT_FRA AFRA,ANT.TEXT_FRA NFRA,CAA.TEXT_FRA CAFRA,CAN.TEXT_FRA CNFRA," & vbCrLf
sConsulta = sConsulta & "@TIPO_NOTIF_PROVE_CAMBIOS_CAL TIPO_NOTIF_PROVE_CAMBIOS_CAL,PUNT.FECACT" & vbCrLf
sConsulta = sConsulta & "FROM VAR_PROVE_UNQA_PUNT PUNT WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PUNT_ANT H WITH(NOLOCK) ON  H.PROVE=PUNT.PROVE AND H.UNQA=PUNT.UNQA AND H.VARCAL=PUNT.VARCAL AND H.NIVEL=PUNT.NIVEL" & vbCrLf
sConsulta = sConsulta & "INNER JOIN WEBFSWSTEXT ACT WITH(NOLOCK) ON ACT.MODULO=58 AND ACT.ID=(12+@MES_MOSTRAR_ACT)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN WEBFSWSTEXT ANT WITH(NOLOCK) ON ANT.MODULO=58 AND ANT.ID=(12+@MES_MOSTRAR_ANT)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN WEBFSWSTEXT CAA WITH(NOLOCK) ON CAA.MODULO=58 AND CAA.ID=32" & vbCrLf
sConsulta = sConsulta & "INNER JOIN WEBFSWSTEXT CAN WITH(NOLOCK) ON CAN.MODULO=58 AND CAN.ID=33" & vbCrLf
sConsulta = sConsulta & "WHERE PUNT.NIVEL=0 AND NOT (ISNULL(PUNT.CAL,-1)=ISNULL(H.CAL,-1))" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT UNQA,IDIOMA,DEN FROM UNQA_IDIOMAS WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & ";WITH PUNT_ANT AS (" & vbCrLf
sConsulta = sConsulta & "   --Cada mes (se compara con la tabla de hist?rico)" & vbCrLf
sConsulta = sConsulta & "   SELECT PROVE,UNQA,VARCAL,NIVEL,PUNT,CAL" & vbCrLf
sConsulta = sConsulta & "   FROM VAR_PROVE_UNQA_PUNT_HIST WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   WHERE PROVE=@PROVE AND @TIPO_NOTIF_PROVE_CAMBIOS_CAL=0" & vbCrLf
sConsulta = sConsulta & "   UNION ALL" & vbCrLf
sConsulta = sConsulta & "   --En cada cambio (se compara con la tabla de puntuaciones anteriores)" & vbCrLf
sConsulta = sConsulta & "   SELECT PROVE,UNQA,VARCAL,NIVEL,PUNT,CAL" & vbCrLf
sConsulta = sConsulta & "   FROM VAR_PROVE_UNQA_PUNT_ANT WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   WHERE PROVE=@PROVE AND @TIPO_NOTIF_PROVE_CAMBIOS_CAL=1" & vbCrLf
sConsulta = sConsulta & ")" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT CAL_PUNTUACION,IDIOMA,DEN" & vbCrLf
sConsulta = sConsulta & "FROM CAL_PUNTUACION_IDIOMA CPI WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN CAL_PUNTUACION CP WITH(NOLOCK) ON CP.VARCAL=CPI.VARCAL AND CP.NIVEL=CPI.NIVEL AND CP.ID=CPI.CAL_PUNTUACION" & vbCrLf
sConsulta = sConsulta & "   INNER JOIN PUNT_ANT P WITH(NOLOCK) ON P.VARCAL=CP.VARCAL AND P.NIVEL=CP.NIVEL" & vbCrLf
sConsulta = sConsulta & "WHERE CPI.NIVEL=0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSQA_DEVOLVER_CONTACTOS_MAS_SUBJECT]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSQA_DEVOLVER_CONTACTOS_MAS_SUBJECT]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSQA_DEVOLVER_CONTACTOS_MAS_SUBJECT] @PROVE VARCHAR(50) AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT CON.EMAIL EMAIL,CON.TIPOEMAIL TIPOEMAIL,CON.DATEFMT," & vbCrLf
sConsulta = sConsulta & "CASE WHEN CON.IDI IS NULL THEN (SELECT IDIOMA FROM PARGEN_DEF WITH(NOLOCK) WHERE PARGEN_DEF.ID=1)  ELSE CON.IDI  END IDIOMA," & vbCrLf
sConsulta = sConsulta & "WEBFSWSTEXT.TEXT_SPA,WEBFSWSTEXT.TEXT_GER,WEBFSWSTEXT.TEXT_ENG," & vbCrLf
sConsulta = sConsulta & "CON.NOM + ' ' + CON.APE AS PARANOMBRE,WEBFSWSTEXT.TEXT_FRA" & vbCrLf
sConsulta = sConsulta & "FROM CON WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN WEBFSWSTEXT WITH (NOLOCK) ON MODULO=101 AND WEBFSWSTEXT.ID=31" & vbCrLf
sConsulta = sConsulta & "WHERE PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "ORDER BY CALIDAD DESC,NOM,APE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSSM_ACTUALIZAR_IMPORTES_PARTIDA]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSSM_ACTUALIZAR_IMPORTES_PARTIDA]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSSM_ACTUALIZAR_IMPORTES_PARTIDA]" & vbCrLf
sConsulta = sConsulta & "@IDIOMA NVARCHAR(10)," & vbCrLf
sConsulta = sConsulta & "@PRES0 NVARCHAR(100)=NULL," & vbCrLf
sConsulta = sConsulta & "@PRES1 NVARCHAR(100)=NULL," & vbCrLf
sConsulta = sConsulta & "@PRES2 NVARCHAR(100)=NULL," & vbCrLf
sConsulta = sConsulta & "@PRES3 NVARCHAR(100)=NULL," & vbCrLf
sConsulta = sConsulta & "@PRES4 NVARCHAR(100)=NULL," & vbCrLf
sConsulta = sConsulta & "@TIENECONTROLDISPONIBLE INT=0," & vbCrLf
sConsulta = sConsulta & "@CONTROLDISPCONPRES INT=0," & vbCrLf
sConsulta = sConsulta & "@INSTANCIA INT=0," & vbCrLf
sConsulta = sConsulta & "@MONEDA NVARCHAR(10)=NULL," & vbCrLf
sConsulta = sConsulta & "@CANT_PEDIDA FLOAT=NULL," & vbCrLf
sConsulta = sConsulta & "@IMPORTE_PEDIDO FLOAT=NULL," & vbCrLf
sConsulta = sConsulta & "@IDCAMPOSOLICIT INT=NULL," & vbCrLf
sConsulta = sConsulta & "@SOLICITADO FLOAT=NULL," & vbCrLf
sConsulta = sConsulta & "@LINEAPEDIDO INT=NULL," & vbCrLf
sConsulta = sConsulta & "@COMPROMETIDO FLOAT=NULL," & vbCrLf
sConsulta = sConsulta & "@OK INT=0 OUTPUT," & vbCrLf
sConsulta = sConsulta & "@ID_PRES5_IMPORTES INT=0 OUTPUT," & vbCrLf
sConsulta = sConsulta & "@CODPARTIDA NVARCHAR(100)=NULL OUTPUT," & vbCrLf
sConsulta = sConsulta & "@DENPARTIDA NVARCHAR(300)=NULL OUTPUT," & vbCrLf
sConsulta = sConsulta & "@PRESUPUESTADO FLOAT=NULL OUTPUT," & vbCrLf
sConsulta = sConsulta & "@DISPONIBLE FLOAT=NULL OUTPUT," & vbCrLf
sConsulta = sConsulta & "@EQUIV FLOAT=NULL OUTPUT," & vbCrLf
sConsulta = sConsulta & "@MON NVARCHAR(20)=NULL OUTPUT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE @COMPROMETIDO_ANT FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @SOLICITADO_ANT FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @EQUIVPRES FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLTOT NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @CAMPO_ORIGEN_PADRE INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRES1_SOLICITUD NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRES2_SOLICITUD NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRES3_SOLICITUD NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRES4_SOLICITUD NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--SI @PRES0 ES NULL ES PORQUE ES UN RECHAZO O ANULACION" & vbCrLf
sConsulta = sConsulta & "IF @PRES0 IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "--OBTENEMOS LOS DATOS DE LA PARTIDA PRESUPUESTARIA A LA QUE SE IMPUTA" & vbCrLf
sConsulta = sConsulta & "IF @PRES4 IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SQL=N'INNER JOIN PRES5_NIV4 P WITH (NOLOCK) ON P.PRES0=P5I.PRES0 AND P.PRES1=P5I.PRES1 AND P.PRES2=P5I.PRES2 AND P.PRES3=P5I.PRES3 AND P.COD=P5I.PRES4 '" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'INNER JOIN PRES5_IDIOMAS PIDI WITH (NOLOCK) ON PIDI.PRES0=P.PRES0 AND P5I.PRES1=P.PRES1 AND PIDI.PRES2=P.PRES2 AND PIDI.PRES3=P.PRES3 AND PIDI.PRES4=P.COD AND PIDI.IDIOMA=@IDIOMA '" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @PRES3 IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SQL=N'INNER JOIN PRES5_NIV3 P WITH (NOLOCK) ON P.PRES0=P5I.PRES0 AND P.PRES1=P5I.PRES1 AND P.PRES2=P5I.PRES2 AND P.COD=P5I.PRES3 '" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'INNER JOIN PRES5_IDIOMAS PIDI WITH (NOLOCK) ON PIDI.PRES0=P.PRES0 AND PIDI.PRES1=P.PRES1 AND PIDI.PRES2=P.PRES2 AND PIDI.PRES3=P.COD AND PIDI.PRES4 IS NULL AND PIDI.IDIOMA=@IDIOMA '" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @PRES2 IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SQL=N'INNER JOIN PRES5_NIV2 P WITH (NOLOCK) ON P.PRES0 = P5I.PRES0 AND P.PRES1=P5I.PRES1 AND P.COD=P5I.PRES2 '" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'INNER JOIN PRES5_IDIOMAS PIDI WITH (NOLOCK) ON P5I.PRES0=P.PRES0 AND PIDI.PRES1=P.PRES1 AND PIDI.PRES2=P.COD AND PIDI.PRES3 IS NULL AND PIDI.PRES4 IS NULL AND PIDI.IDIOMA=@IDIOMA '" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SQL=N'INNER JOIN PRES5_NIV1 P WITH (NOLOCK) ON P.PRES0=P5I.PRES0 AND P.COD=P5I.PRES1 '" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'INNER JOIN PRES5_IDIOMAS PIDI WITH (NOLOCK) ON PIDI.PRES0=P.PRES0 AND PIDI.PRES1=P.COD AND PIDI.PRES2 IS NULL AND PIDI.PRES3 IS NULL AND PIDI.PRES4 IS NULL AND PIDI.IDIOMA=@IDIOMA '" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "-- CALIDAD: NO PONEMOS WITH(NOLOCK) PORQUE DEBEMOS BLOQUEAR LOS IMPORTES DE LAS PARTIDAS PARA QUE CADA VEZ SEAN LAS REALES LAS CANTIDADES QUE SE MODIFIQUEN" & vbCrLf
sConsulta = sConsulta & "SET @SQLTOT=N'SELECT @ID_PRES5_IMPORTES=P5I.ID,@CODPARTIDA=P.COD,@DENPARTIDA=PIDI.DEN,@PRESUPUESTADO=P5I.PRES,@COMPROMETIDO_ANT=ISNULL(P5I.COMP,0),@SOLICITADO_ANT=(ISNULL(P5I.SOLICITADO,0)-ISNULL(IP.SOLICITADO,0)),@MON=P5I.MON,@EQUIVPRES=M1.EQUIV '" & vbCrLf
sConsulta = sConsulta & "SET @SQLTOT=@SQLTOT+N'FROM PRES5_IMPORTES P5I '" & vbCrLf
sConsulta = sConsulta & "SET @SQLTOT=@SQLTOT+@SQL" & vbCrLf
sConsulta = sConsulta & "SET @SQLTOT=@SQLTOT+N'LEFT JOIN MON M1 WITH (NOLOCK) ON M1.COD=P5I.MON '" & vbCrLf
sConsulta = sConsulta & "SET @SQLTOT=@SQLTOT+N'LEFT JOIN INSTANCIA_PRES5 IP WITH (NOLOCK) ON IP.INSTANCIA=@INSTANCIA '" & vbCrLf
sConsulta = sConsulta & "SET @SQLTOT=@SQLTOT+N'AND IP.PRES0=P.PRES0 AND IP.PRES1=@PRES1 AND (IP.PRES2=@PRES2 OR @PRES2 IS NULL) AND (IP.PRES3=@PRES3 OR @PRES3 IS NULL) AND (IP.PRES4=@PRES4 OR @PRES4 IS NULL) '" & vbCrLf
sConsulta = sConsulta & "SET @SQLTOT=@SQLTOT+N'WHERE P5I.PRES0=@PRES0 AND P5I.PRES1=@PRES1 AND ISNULL(P5I.PRES2,'''')=ISNULL(@PRES2,'''') AND ISNULL(P5I.PRES3,'''')=ISNULL(@PRES3,'''') AND ISNULL(P5I.PRES4,'''')=ISNULL(@PRES4,'''') '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLTOT,N'@INSTANCIA INT,@IDIOMA NVARCHAR(20),@PRES0 NVARCHAR(100),@PRES1 NVARCHAR(100),@PRES2 NVARCHAR(100),@PRES3 NVARCHAR(100),@PRES4 NVARCHAR(100),@ID_PRES5_IMPORTES INT OUTPUT,@CODPARTIDA NVARCHAR(100) OUTPUT,@DENPARTIDA NVARCHAR(300) OUTPUT,@PRESUPUESTADO FLOAT OUTPUT,@COMPROMETIDO_ANT FLOAT OUTPUT,@SOLICITADO_ANT FLOAT OUTPUT,@MON NVARCHAR(20) OUTPUT,@EQUIVPRES FLOAT OUTPUT'," & vbCrLf
sConsulta = sConsulta & "@INSTANCIA=@INSTANCIA,@IDIOMA=@IDIOMA,@PRES0=@PRES0,@PRES1=@PRES1,@PRES2=@PRES2,@PRES3=@PRES3,@PRES4=@PRES4,@ID_PRES5_IMPORTES=@ID_PRES5_IMPORTES OUTPUT,@CODPARTIDA=@CODPARTIDA OUTPUT,@DENPARTIDA=@DENPARTIDA OUTPUT,@PRESUPUESTADO=@PRESUPUESTADO OUTPUT,@COMPROMETIDO_ANT=@COMPROMETIDO_ANT OUTPUT,@SOLICITADO_ANT=@SOLICITADO_ANT OUTPUT,@MON=@MON OUTPUT,@EQUIVPRES=@EQUIVPRES OUTPUT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Necesitamos la equivalencia respecto a central de la solicitud" & vbCrLf
sConsulta = sConsulta & "SELECT @EQUIV=(@EQUIVPRES/M2.EQUIV) FROM MON M2 WITH (NOLOCK) WHERE M2.COD=@MONEDA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--PONEMOS EL SOLICITADO CON EQUIVALENCIA" & vbCrLf
sConsulta = sConsulta & "SET @SOLICITADO=@SOLICITADO*@EQUIV" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--TENEMOS QUE ACTUALIZAR LOS SOLICITADOS" & vbCrLf
sConsulta = sConsulta & "IF @SOLICITADO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "--ES UN RECHAZO O UNA ANULACION DE LA SOLICITUD SI VIENE LA INSTANCIA PERO NO PRES0 Y SOLICITADO=0" & vbCrLf
sConsulta = sConsulta & "--RESTO EL SOLICITADO QUE HAY EN INSTANCIA_PRES5 A LAS PARTIDAS CORRESPONDIENTES" & vbCrLf
sConsulta = sConsulta & "IF @SOLICITADO=0 AND @PRES0 IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "-- CALIDAD: NO PONEMOS WITH(NOLOCK) PORQUE DEBEMOS BLOQUEAR LOS IMPORTES DE LAS PARTIDAS PARA QUE CADA VEZ SEAN LAS REALES LAS CANTIDADES QUE SE MODIFIQUEN" & vbCrLf
sConsulta = sConsulta & "UPDATE P5I SET P5I.SOLICITADO=ISNULL(P5I.SOLICITADO,0)-ISNULL(IP.SOLICITADO,0)" & vbCrLf
sConsulta = sConsulta & "FROM PRES5_IMPORTES P5I" & vbCrLf
sConsulta = sConsulta & "INNER JOIN INSTANCIA_PRES5 IP WITH (NOLOCK) ON IP.PRES0=P5I.PRES0 AND IP.PRES1=P5I.PRES1" & vbCrLf
sConsulta = sConsulta & "AND ISNULL(IP.PRES2,'')=ISNULL(P5I.PRES2,'')" & vbCrLf
sConsulta = sConsulta & "AND ISNULL(IP.PRES3,'')=ISNULL(P5I.PRES3,'')" & vbCrLf
sConsulta = sConsulta & "AND ISNULL(IP.PRES4,'')=ISNULL(P5I.PRES4,'')" & vbCrLf
sConsulta = sConsulta & "AND IP.INSTANCIA=@INSTANCIA AND P5I.CERRADO=0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--BORRAMOS LO ALMACENADO PARA LA INSTANCIA Y SUS LINEAS" & vbCrLf
sConsulta = sConsulta & "DELETE FROM INSTANCIA_PRES5" & vbCrLf
sConsulta = sConsulta & "WHERE INSTANCIA=@INSTANCIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DELETE FROM INSTANCIA_PRES5_DESGLOSE" & vbCrLf
sConsulta = sConsulta & "WHERE INSTANCIA=@INSTANCIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @OK=1" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "--SI HAY CONTROL DEL DISPONIBLE CALCULAMOS SI HAY DISPONIBLE" & vbCrLf
sConsulta = sConsulta & "IF (@TIENECONTROLDISPONIBLE=1 AND (@CONTROLDISPCONPRES=0 OR (@CONTROLDISPCONPRES=1 AND @PRESUPUESTADO>0))) AND @OK=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "--CALCULAMOS EL TOTAL DE LA SOLICITUD SOBRE ESTA PARTIDA" & vbCrLf
sConsulta = sConsulta & "SET @DISPONIBLE=@PRESUPUESTADO-@COMPROMETIDO_ANT-@SOLICITADO_ANT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--COMPROBAMOS SI HAY DISPONIBLE / SI SE ACCEDE AL STORED TRAS UNA CONFIRMACION DEL AVISO NOS LO SALTAMOS" & vbCrLf
sConsulta = sConsulta & "IF NOT @SOLICITADO>@DISPONIBLE" & vbCrLf
sConsulta = sConsulta & "SET @OK=1" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @OK=1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--PONEMOS TODOS LOS REGISTROS DE LA INSTANCIA A PENDIENTE A 1 HASTA QUE SE CONFIRMEN TODAS LAS IMPUTACIONES" & vbCrLf
sConsulta = sConsulta & "UPDATE INSTANCIA_PRES5 SET PENDIENTE=1 WHERE" & vbCrLf
sConsulta = sConsulta & "INSTANCIA=@INSTANCIA AND ISNULL(PENDIENTE,0)=0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--SI PARA LA PARTIDA ACTUAL E INSTANCIA NO HAY REGISTRO EN INSTANCIA_PRES5 ES QUE HA HABIDO CAMBIO/ALTA" & vbCrLf
sConsulta = sConsulta & "--A?ADIMOS REGISTRO O ACTUALIZAMOS EN INSTANCIA_PRES5" & vbCrLf
sConsulta = sConsulta & "IF EXISTS(SELECT 1 FROM INSTANCIA_PRES5 WITH (NOLOCK) WHERE INSTANCIA=@INSTANCIA AND PRES0=@PRES0 AND PRES1=@PRES1" & vbCrLf
sConsulta = sConsulta & "AND ISNULL(PRES2,'')=ISNULL(@PRES2,'') AND ISNULL(PRES3,'')=ISNULL(@PRES3,'')" & vbCrLf
sConsulta = sConsulta & "AND ISNULL(PRES4,'')=ISNULL(@PRES4,''))" & vbCrLf
sConsulta = sConsulta & "UPDATE INSTANCIA_PRES5 SET PENDIENTE_SOLICITADO=@SOLICITADO" & vbCrLf
sConsulta = sConsulta & "WHERE INSTANCIA=@INSTANCIA AND PRES0=@PRES0 AND PRES1=@PRES1" & vbCrLf
sConsulta = sConsulta & "AND ISNULL(PRES2,'')=ISNULL(@PRES2,'')" & vbCrLf
sConsulta = sConsulta & "AND ISNULL(PRES3,'')=ISNULL(@PRES3,'')" & vbCrLf
sConsulta = sConsulta & "AND ISNULL(PRES4,'')=ISNULL(@PRES4,'')" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "INSERT INTO INSTANCIA_PRES5(INSTANCIA,PRES0,PRES1,PRES2,PRES3,PRES4,SOLICITADO,PENDIENTE,PENDIENTE_SOLICITADO)" & vbCrLf
sConsulta = sConsulta & "VALUES(@INSTANCIA,@PRES0,@PRES1,@PRES2,@PRES3,@PRES4,0,1,@SOLICITADO)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--SI HAY DISPONIBLE O NO HAY CONTROL DE DISPONIBLE ACTUALIZAMOS LOS IMPORTES DE LA PARTIDA Y DE LA TABLA INSTANCIA_PRES5_DESGLOSE" & vbCrLf
sConsulta = sConsulta & "IF @OK=1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "--PARA LA PARTIDA INDICADA ACTUALIZO EL SOLICITADO" & vbCrLf
sConsulta = sConsulta & "-- CALIDAD: NO PONEMOS WITH(NOLOCK) PORQUE DEBEMOS BLOQUEAR LOS IMPORTES DE LAS PARTIDAS PARA QUE CADA VEZ SEAN LAS REALES LAS CANTIDADES QUE SE MODIFIQUEN" & vbCrLf
sConsulta = sConsulta & "UPDATE P5I SET SOLICITADO=ISNULL(P5I.SOLICITADO,0)-ISNULL(IP.SOLICITADO,0)+@SOLICITADO" & vbCrLf
sConsulta = sConsulta & "FROM PRES5_IMPORTES P5I" & vbCrLf
sConsulta = sConsulta & "INNER JOIN INSTANCIA_PRES5 IP WITH (NOLOCK) ON IP.PRES0=P5I.PRES0 AND IP.PRES1=P5I.PRES1" & vbCrLf
sConsulta = sConsulta & "AND ISNULL(IP.PRES2,'')=ISNULL(P5I.PRES2,'') AND ISNULL(IP.PRES3,'')=ISNULL(P5I.PRES3,'')" & vbCrLf
sConsulta = sConsulta & "AND ISNULL(IP.PRES4,'')=ISNULL(P5I.PRES4,'') AND IP.INSTANCIA=@INSTANCIA" & vbCrLf
sConsulta = sConsulta & "WHERE P5I.CERRADO=0 AND P5I.PRES0=@PRES0 AND P5I.PRES1=@PRES1" & vbCrLf
sConsulta = sConsulta & "AND ISNULL(P5I.PRES2,'')=ISNULL(@PRES2,'')" & vbCrLf
sConsulta = sConsulta & "AND ISNULL(P5I.PRES3,'')=ISNULL(@PRES3,'')" & vbCrLf
sConsulta = sConsulta & "AND ISNULL(P5I.PRES4,'')=ISNULL(@PRES4,'')" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--LIBERAMOS SOLICITADO SI EL PEDIDO ES POR CANTIDAD" & vbCrLf
sConsulta = sConsulta & "IF @CANT_PEDIDA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE @CANT_SOLICITADA FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @CANT_PEDIDA_ANTERIOR FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRECIO_UNITARIO_EN_MON_PRES FLOAT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--OBTENEMOS LOS DATOS DE LA LINEA DE SOLICITUD VINCULADA A LA LINEA DE PEDIDO" & vbCrLf
sConsulta = sConsulta & "SELECT @CANT_SOLICITADA=IPD.CANT_SOLICITADA," & vbCrLf
sConsulta = sConsulta & "@CANT_PEDIDA_ANTERIOR=ISNULL(IPD.CANT_PEDIDA,0)," & vbCrLf
sConsulta = sConsulta & "@CAMPO_ORIGEN_PADRE=CCD.ID," & vbCrLf
sConsulta = sConsulta & "@PRECIO_UNITARIO_EN_MON_PRES=PRECIO_UNITARIO_EN_MON_PRES," & vbCrLf
sConsulta = sConsulta & "@PRES1_SOLICITUD=PRES1,@PRES2_SOLICITUD=PRES2,@PRES3_SOLICITUD=PRES3,@PRES4_SOLICITUD=PRES4" & vbCrLf
sConsulta = sConsulta & "FROM INSTANCIA_PRES5_DESGLOSE IPD WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN FORM_CAMPO CCD WITH (NOLOCK) ON CCD.ID=IPD.CAMPO_ORIGEN_PADRE" & vbCrLf
sConsulta = sConsulta & "INNER JOIN INSTANCIA I WITH (NOLOCK) ON I.ID=IPD.INSTANCIA" & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_CAMPO CC WITH (NOLOCK) ON CC.COPIA_CAMPO_DEF=CCD.ID AND CC.INSTANCIA=I.ID AND CC.NUM_VERSION=I.ULT_VERSION" & vbCrLf
sConsulta = sConsulta & "WHERE CC.COPIA_CAMPO_DEF=@IDCAMPOSOLICIT AND IPD.LINEA=@LINEAPEDIDO AND IPD.INSTANCIA=@INSTANCIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--SI ANTERIORMENTE TODAVIA NO SE HABIA LIBERADO TODO EL SOLICITADO CALCULAMOS," & vbCrLf
sConsulta = sConsulta & "--SINO SIMPLEMENTE ACTUALIZAMOS LA CANTIDAD_PEDIDA DE LA LINEA DE SOLICITUD" & vbCrLf
sConsulta = sConsulta & "IF @CANT_PEDIDA_ANTERIOR<@CANT_SOLICITADA" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF (@CANT_PEDIDA+@CANT_PEDIDA_ANTERIOR)>@CANT_SOLICITADA --LIBERAMOS EL SOLICITADO QUE RESTA POR LIBERAR" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES5_IMPORTES" & vbCrLf
sConsulta = sConsulta & "SET SOLICITADO=SOLICITADO-((@CANT_SOLICITADA-@CANT_PEDIDA_ANTERIOR)*@PRECIO_UNITARIO_EN_MON_PRES)" & vbCrLf
sConsulta = sConsulta & "WHERE CERRADO=0 AND PRES0=@PRES0 AND PRES1=@PRES1_SOLICITUD" & vbCrLf
sConsulta = sConsulta & "AND ISNULL(PRES2,'')=ISNULL(@PRES2_SOLICITUD,'')" & vbCrLf
sConsulta = sConsulta & "AND ISNULL(PRES3,'')=ISNULL(@PRES3_SOLICITUD,'')" & vbCrLf
sConsulta = sConsulta & "AND ISNULL(PRES4,'')=ISNULL(@PRES4_SOLICITUD,'')" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE INSTANCIA_PRES5 SET SOLICITADO=SOLICITADO-((@CANT_SOLICITADA-@CANT_PEDIDA_ANTERIOR)*@PRECIO_UNITARIO_EN_MON_PRES)" & vbCrLf
sConsulta = sConsulta & "WHERE INSTANCIA=@INSTANCIA AND PRES0=@PRES0 AND PRES1=@PRES1_SOLICITUD" & vbCrLf
sConsulta = sConsulta & "AND ISNULL(PRES2,'')=ISNULL(@PRES2_SOLICITUD,'')" & vbCrLf
sConsulta = sConsulta & "AND ISNULL(PRES3,'')=ISNULL(@PRES3_SOLICITUD,'')" & vbCrLf
sConsulta = sConsulta & "AND ISNULL(PRES4,'')=ISNULL(@PRES4_SOLICITUD,'')" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES5_IMPORTES" & vbCrLf
sConsulta = sConsulta & "SET SOLICITADO=SOLICITADO-(@CANT_PEDIDA*@PRECIO_UNITARIO_EN_MON_PRES)" & vbCrLf
sConsulta = sConsulta & "WHERE CERRADO=0 AND PRES0=@PRES0 AND PRES1=@PRES1_SOLICITUD" & vbCrLf
sConsulta = sConsulta & "AND ISNULL(PRES2,'')=ISNULL(@PRES2_SOLICITUD,'')" & vbCrLf
sConsulta = sConsulta & "AND ISNULL(PRES3,'')=ISNULL(@PRES3_SOLICITUD,'')" & vbCrLf
sConsulta = sConsulta & "AND ISNULL(PRES4,'')=ISNULL(@PRES4_SOLICITUD,'')" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE INSTANCIA_PRES5 SET SOLICITADO=SOLICITADO-(@CANT_PEDIDA*@PRECIO_UNITARIO_EN_MON_PRES)" & vbCrLf
sConsulta = sConsulta & "WHERE INSTANCIA=@INSTANCIA AND PRES0=@PRES0 AND PRES1=@PRES1_SOLICITUD" & vbCrLf
sConsulta = sConsulta & "AND ISNULL(PRES2,'')=ISNULL(@PRES2_SOLICITUD,'')" & vbCrLf
sConsulta = sConsulta & "AND ISNULL(PRES3,'')=ISNULL(@PRES3_SOLICITUD,'')" & vbCrLf
sConsulta = sConsulta & "AND ISNULL(PRES4,'')=ISNULL(@PRES4_SOLICITUD,'')" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN --AQUI LLEGA CUANDO ES UNA MODIFICACION" & vbCrLf
sConsulta = sConsulta & "IF (@CANT_PEDIDA_ANTERIOR+@CANT_PEDIDA)<@CANT_SOLICITADA --LA CANTIDAD PEDIDA PUEDE SER NEGATIVA SI ES MODIFICACION Y PEDIMOS MENOS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES5_IMPORTES" & vbCrLf
sConsulta = sConsulta & "SET SOLICITADO=SOLICITADO+((@CANT_SOLICITADA-@CANT_PEDIDA_ANTERIOR-@CANT_PEDIDA)*@PRECIO_UNITARIO_EN_MON_PRES)" & vbCrLf
sConsulta = sConsulta & "WHERE CERRADO=0 AND PRES0=@PRES0 AND PRES1=@PRES1_SOLICITUD" & vbCrLf
sConsulta = sConsulta & "AND ISNULL(PRES2,'')=ISNULL(@PRES2_SOLICITUD,'')" & vbCrLf
sConsulta = sConsulta & "AND ISNULL(PRES3,'')=ISNULL(@PRES3_SOLICITUD,'')" & vbCrLf
sConsulta = sConsulta & "AND ISNULL(PRES4,'')=ISNULL(@PRES4_SOLICITUD,'')" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE INSTANCIA_PRES5 SET SOLICITADO=SOLICITADO+((@CANT_SOLICITADA-@CANT_PEDIDA_ANTERIOR-@CANT_PEDIDA)*@PRECIO_UNITARIO_EN_MON_PRES)" & vbCrLf
sConsulta = sConsulta & "WHERE INSTANCIA=@INSTANCIA AND PRES0=@PRES0 AND PRES1=@PRES1_SOLICITUD" & vbCrLf
sConsulta = sConsulta & "AND ISNULL(PRES2,'')=ISNULL(@PRES2_SOLICITUD,'')" & vbCrLf
sConsulta = sConsulta & "AND ISNULL(PRES3,'')=ISNULL(@PRES3_SOLICITUD,'')" & vbCrLf
sConsulta = sConsulta & "AND ISNULL(PRES4,'')=ISNULL(@PRES4_SOLICITUD,'')" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--ACTUALIZAMOS AHORA LAS LINEAS DE DESGLOSE CON LA CANTIDAD PEDIDA" & vbCrLf
sConsulta = sConsulta & "UPDATE INSTANCIA_PRES5_DESGLOSE SET CANT_PEDIDA=ISNULL(CANT_PEDIDA,0)+@CANT_PEDIDA" & vbCrLf
sConsulta = sConsulta & "WHERE INSTANCIA=@INSTANCIA AND LINEA=@LINEAPEDIDO AND CAMPO_ORIGEN_PADRE=@CAMPO_ORIGEN_PADRE" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "--LIBERAMOS SOLICITADO SI EL PEDIDO ES POR IMPORTE" & vbCrLf
sConsulta = sConsulta & "IF @IMPORTE_PEDIDO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE @IMPORTE_SOLICITADO FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @IMPORTE_PEDIDO_ANTERIOR FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @EQUIVPRES_SOLICITUD FLOAT" & vbCrLf
sConsulta = sConsulta & "-- CALIDAD: NO PONEMOS WITH(NOLOCK) PORQUE DEBEMOS BLOQUEAR LOS IMPORTES DE LAS PARTIDAS PARA QUE CADA VEZ SEAN LAS REALES LAS CANTIDADES QUE SE MODIFIQUEN" & vbCrLf
sConsulta = sConsulta & "SELECT @IMPORTE_SOLICITADO=(IPD.CANT_SOLICITADA*IPD.PRECIO_UNITARIO_EN_MON_PRES)," & vbCrLf
sConsulta = sConsulta & "@IMPORTE_PEDIDO_ANTERIOR=ISNULL(IPD.IMPORTE_PEDIDO,0)," & vbCrLf
sConsulta = sConsulta & "@CAMPO_ORIGEN_PADRE=CCD.ID," & vbCrLf
sConsulta = sConsulta & "@PRES1_SOLICITUD=IPD.PRES1,@PRES2_SOLICITUD=IPD.PRES2,@PRES3_SOLICITUD=IPD.PRES3,@PRES4_SOLICITUD=IPD.PRES4," & vbCrLf
sConsulta = sConsulta & "@EQUIVPRES_SOLICITUD=EQUIV" & vbCrLf
sConsulta = sConsulta & "FROM INSTANCIA_PRES5_DESGLOSE IPD" & vbCrLf
sConsulta = sConsulta & "INNER JOIN FORM_CAMPO CCD WITH(NOLOCK) ON CCD.ID=IPD.CAMPO_ORIGEN_PADRE" & vbCrLf
sConsulta = sConsulta & "INNER JOIN INSTANCIA I WITH(NOLOCK) ON I.ID=IPD.INSTANCIA" & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_CAMPO CC WITH(NOLOCK) ON CC.COPIA_CAMPO_DEF=CCD.ID AND CC.INSTANCIA=I.ID AND CC.NUM_VERSION=I.ULT_VERSION" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PRES5_IMPORTES P5I ON P5I.PRES0=IPD.PRES0 AND P5I.PRES1=IPD.PRES1" & vbCrLf
sConsulta = sConsulta & "AND ISNULL(P5I.PRES2,'')=ISNULL(IPD.PRES2,'')" & vbCrLf
sConsulta = sConsulta & "AND ISNULL(P5I.PRES3,'')=ISNULL(IPD.PRES3,'')" & vbCrLf
sConsulta = sConsulta & "AND ISNULL(P5I.PRES4,'')=ISNULL(IPD.PRES4,'')" & vbCrLf
sConsulta = sConsulta & "INNER JOIN MON ON MON.COD=P5I.MON" & vbCrLf
sConsulta = sConsulta & "WHERE CC.COPIA_CAMPO_DEF=@IDCAMPOSOLICIT AND IPD.LINEA=@LINEAPEDIDO AND IPD.INSTANCIA=@INSTANCIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--EL IMPORTE PEDIDO VIENE EN MONEDA CENTRAL. APLICAMOS EL CAMBIO DE LA PARTIDA" & vbCrLf
sConsulta = sConsulta & "SET @IMPORTE_PEDIDO=@IMPORTE_PEDIDO*@EQUIVPRES_SOLICITUD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--SI ANTERIORMENTE TODAVIA NO SE HABIA LIBERADO TODO EL SOLICITADO CALCULAMOS," & vbCrLf
sConsulta = sConsulta & "--SINO SIMPLEMENTE ACTUALIZAMOS LA CANTIDAD_PEDIDA DE LA LINEA DE SOLICITUD" & vbCrLf
sConsulta = sConsulta & "IF @IMPORTE_PEDIDO_ANTERIOR<@IMPORTE_SOLICITADO" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF (@IMPORTE_PEDIDO+@IMPORTE_PEDIDO_ANTERIOR)>@IMPORTE_SOLICITADO --LIBERAMOS EL SOLICITADO QUE RESTA POR LIBERAR" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES5_IMPORTES" & vbCrLf
sConsulta = sConsulta & "SET SOLICITADO=SOLICITADO-(@IMPORTE_SOLICITADO-@IMPORTE_PEDIDO_ANTERIOR)" & vbCrLf
sConsulta = sConsulta & "WHERE CERRADO=0 AND PRES0=@PRES0 AND PRES1=@PRES1_SOLICITUD" & vbCrLf
sConsulta = sConsulta & "AND ISNULL(PRES2,'')=ISNULL(@PRES2_SOLICITUD,'')" & vbCrLf
sConsulta = sConsulta & "AND ISNULL(PRES3,'')=ISNULL(@PRES3_SOLICITUD,'')" & vbCrLf
sConsulta = sConsulta & "AND ISNULL(PRES4,'')=ISNULL(@PRES4_SOLICITUD,'')" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE INSTANCIA_PRES5 SET SOLICITADO=SOLICITADO-(@IMPORTE_SOLICITADO-@IMPORTE_PEDIDO_ANTERIOR)" & vbCrLf
sConsulta = sConsulta & "WHERE INSTANCIA=@INSTANCIA AND PRES0=@PRES0 AND PRES1=@PRES1_SOLICITUD" & vbCrLf
sConsulta = sConsulta & "AND ISNULL(PRES2,'')=ISNULL(@PRES2_SOLICITUD,'')" & vbCrLf
sConsulta = sConsulta & "AND ISNULL(PRES3,'')=ISNULL(@PRES3_SOLICITUD,'')" & vbCrLf
sConsulta = sConsulta & "AND ISNULL(PRES4,'')=ISNULL(@PRES4_SOLICITUD,'')" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES5_IMPORTES" & vbCrLf
sConsulta = sConsulta & "SET SOLICITADO=SOLICITADO-@IMPORTE_PEDIDO" & vbCrLf
sConsulta = sConsulta & "WHERE CERRADO=0 AND PRES0=@PRES0 AND PRES1=@PRES1_SOLICITUD" & vbCrLf
sConsulta = sConsulta & "AND ISNULL(PRES2,'')=ISNULL(@PRES2_SOLICITUD,'')" & vbCrLf
sConsulta = sConsulta & "AND ISNULL(PRES3,'')=ISNULL(@PRES3_SOLICITUD,'')" & vbCrLf
sConsulta = sConsulta & "AND ISNULL(PRES4,'')=ISNULL(@PRES4_SOLICITUD,'')" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE INSTANCIA_PRES5 SET SOLICITADO=SOLICITADO-@IMPORTE_PEDIDO" & vbCrLf
sConsulta = sConsulta & "WHERE INSTANCIA=@INSTANCIA AND PRES0=@PRES0 AND PRES1=@PRES1_SOLICITUD" & vbCrLf
sConsulta = sConsulta & "AND ISNULL(PRES2,'')=ISNULL(@PRES2_SOLICITUD,'')" & vbCrLf
sConsulta = sConsulta & "AND ISNULL(PRES3,'')=ISNULL(@PRES3_SOLICITUD,'')" & vbCrLf
sConsulta = sConsulta & "AND ISNULL(PRES4,'')=ISNULL(@PRES4_SOLICITUD,'')" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN --AQUI LLEGA CUANDO ES UNA MODIFICACION" & vbCrLf
sConsulta = sConsulta & "IF @IMPORTE_PEDIDO_ANTERIOR+@IMPORTE_PEDIDO<@IMPORTE_SOLICITADO" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES5_IMPORTES" & vbCrLf
sConsulta = sConsulta & "SET SOLICITADO=SOLICITADO+(@IMPORTE_SOLICITADO-@IMPORTE_PEDIDO_ANTERIOR-@IMPORTE_PEDIDO)" & vbCrLf
sConsulta = sConsulta & "WHERE CERRADO=0 AND PRES0=@PRES0 AND PRES1=@PRES1_SOLICITUD" & vbCrLf
sConsulta = sConsulta & "AND ISNULL(PRES2,'')=ISNULL(@PRES2_SOLICITUD,'')" & vbCrLf
sConsulta = sConsulta & "AND ISNULL(PRES3,'')=ISNULL(@PRES3_SOLICITUD,'')" & vbCrLf
sConsulta = sConsulta & "AND ISNULL(PRES4,'')=ISNULL(@PRES4_SOLICITUD,'')" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE INSTANCIA_PRES5 SET SOLICITADO=SOLICITADO+(@IMPORTE_SOLICITADO-@IMPORTE_PEDIDO_ANTERIOR-@IMPORTE_PEDIDO)" & vbCrLf
sConsulta = sConsulta & "WHERE INSTANCIA=@INSTANCIA AND PRES0=@PRES0 AND PRES1=@PRES1_SOLICITUD" & vbCrLf
sConsulta = sConsulta & "AND ISNULL(PRES2,'')=ISNULL(@PRES2_SOLICITUD,'')" & vbCrLf
sConsulta = sConsulta & "AND ISNULL(PRES3,'')=ISNULL(@PRES3_SOLICITUD,'')" & vbCrLf
sConsulta = sConsulta & "AND ISNULL(PRES4,'')=ISNULL(@PRES4_SOLICITUD,'')" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--ACTUALIZAMOS AHORA LAS LINEAS DE DESGLOSE CON LA CANTIDAD PEDIDA" & vbCrLf
sConsulta = sConsulta & "UPDATE INSTANCIA_PRES5_DESGLOSE SET IMPORTE_PEDIDO=ISNULL(IMPORTE_PEDIDO,0)+@IMPORTE_PEDIDO" & vbCrLf
sConsulta = sConsulta & "WHERE INSTANCIA=@INSTANCIA AND LINEA=@LINEAPEDIDO AND CAMPO_ORIGEN_PADRE=@CAMPO_ORIGEN_PADRE" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--ACTUALIZAMOS EL COMPROMETIDO DE LA PARTIDA DE LA LINEA DE PEDIDO" & vbCrLf
sConsulta = sConsulta & "IF @COMPROMETIDO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES5_IMPORTES" & vbCrLf
sConsulta = sConsulta & "SET COMP=ISNULL(COMP,0)+(@COMPROMETIDO*@EQUIVPRES)" & vbCrLf
sConsulta = sConsulta & "WHERE CERRADO=0 AND PRES0=@PRES0 AND PRES1=@PRES1" & vbCrLf
sConsulta = sConsulta & "AND ISNULL(PRES2,'')=ISNULL(@PRES2,'')" & vbCrLf
sConsulta = sConsulta & "AND ISNULL(PRES3,'')=ISNULL(@PRES3,'')" & vbCrLf
sConsulta = sConsulta & "AND ISNULL(PRES4,'')=ISNULL(@PRES4,'')" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   --En la llamada desde GS CInstancia.ComprobarSolicitado no se contempla un posible valor NULL" & vbCrLf
sConsulta = sConsulta & "   SET @PRESUPUESTADO=ISNULL(@PRESUPUESTADO,0)" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSN_COMPROBACIONES_DESHACER_BORRADO_LOGICO_PEDIDO]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSN_COMPROBACIONES_DESHACER_BORRADO_LOGICO_PEDIDO]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSN_COMPROBACIONES_DESHACER_BORRADO_LOGICO_PEDIDO]" & vbCrLf
sConsulta = sConsulta & "   @IDORDEN INT,   " & vbCrLf
sConsulta = sConsulta & "   @ESTADO_PEDIDO INT,     " & vbCrLf
sConsulta = sConsulta & "   @TIPO_PEDIDO INT," & vbCrLf
sConsulta = sConsulta & "   @LINEAS VARCHAR(2000)," & vbCrLf
sConsulta = sConsulta & "   @MOTIVO_NO_DESHACER_BORRADO INT OUTPUT," & vbCrLf
sConsulta = sConsulta & "   @LINEAS_NO_DESHACER_BORRADO VARCHAR(2000) OUTPUT," & vbCrLf
sConsulta = sConsulta & "   @LINEAS_A_DESHACER_BORRADO VARCHAR(2000) OUTPUT" & vbCrLf
sConsulta = sConsulta & "AS " & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   DECLARE @IDORDEN_PEDIDO_ABIERTO AS INT = 0" & vbCrLf
sConsulta = sConsulta & "   DECLARE @EST_PEDIDO_ABIERTO AS INT = 0" & vbCrLf
sConsulta = sConsulta & "   DECLARE @ESTA_BORRADO AS TINYINT = 0" & vbCrLf
sConsulta = sConsulta & "   DECLARE @PED_ABIERTO_TIPO AS TINYINT = 0    " & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "   SET @MOTIVO_NO_DESHACER_BORRADO=0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   --@MOTIVO_NO_BORRAR=0 NO HAY MOTIVO, SE PERMITE DESHACER BORRADO LOGICO" & vbCrLf
sConsulta = sConsulta & "   --@MOTIVO_NO_BORRAR=1 NO SE PUEDE DESHACER BORRADO DE UN PEDIDO si el pedido tiene estado<2 (estados 0 o 1) o estado anulado" & vbCrLf
sConsulta = sConsulta & "   --@MOTIVO_NO_BORRAR=2 NO SE PUEDE DESHACER BORRADO DE UN PEDIDO tipo ContraPedidoAbierto Si el pedido abierto correspondiente est� cerrado (o anulado o no vigente)" & vbCrLf
sConsulta = sConsulta & "   --@MOTIVO_NO_BORRAR=3 NO SE PUEDE DESHACER BORRADO DE UN PEDIDO tipo ContraPedidoAbierto Si el pedido abierto correspondiente est� borrado      " & vbCrLf
sConsulta = sConsulta & "   --@MOTIVO_NO_BORRAR=4 NO SE PUEDE DESHACER BORRADO DE UN PEDIDO tipo ContraPedidoAbierto Si se superar� la cantidad de las l�neas del abierto correspondiente" & vbCrLf
sConsulta = sConsulta & "   --@MOTIVO_NO_BORRAR=41 NO SE PUEDE DESHACER BORRADO DE UN PEDIDO tipo ContraPedidoAbierto Si se superar� la cantidad de las l�neas del abierto correspondiente. Ninguna fila es posible deshacer" & vbCrLf
sConsulta = sConsulta & "   --@MOTIVO_NO_BORRAR=5 NO SE PUEDE DESHACER BORRADO DE UN PEDIDO tipo ContraPedidoAbierto Si se superar� el importe de las l�neas del abierto correspondiente" & vbCrLf
sConsulta = sConsulta & "   --@MOTIVO_NO_BORRAR=51 NO SE PUEDE DESHACER BORRADO DE UN PEDIDO tipo ContraPedidoAbierto Si se superar� el importe de las l�neas del abierto correspondiente. Ninguna fila es posible deshacer" & vbCrLf
sConsulta = sConsulta & "   --@MOTIVO_NO_BORRAR=6 NO SE  PUEDE DESHACER BORRADO DE UN PEDIDO si alguna l�nea del pedido procede de un proceso que est� anulado" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   -- NO SE PUEDE DESHACER BORRADO DE UN PEDIDO si el pedido tiene estado<2 o anulado (o para pedidos abiertos con estado NO Abierto)" & vbCrLf
sConsulta = sConsulta & "   IF @ESTADO_PEDIDO = 0 OR  @ESTADO_PEDIDO = 1 OR @ESTADO_PEDIDO=20 OR @ESTADO_PEDIDO=100 OR @ESTADO_PEDIDO=102 OR @ESTADO_PEDIDO=103" & vbCrLf
sConsulta = sConsulta & "       SET @MOTIVO_NO_DESHACER_BORRADO = 1                                 " & vbCrLf
sConsulta = sConsulta & "   ELSE                                        " & vbCrLf
sConsulta = sConsulta & "       IF @TIPO_PEDIDO=6   -- CASO CONTRAPEDIDOABIERTO" & vbCrLf
sConsulta = sConsulta & "           BEGIN               " & vbCrLf
sConsulta = sConsulta & "               --Partimos de que en un pedido contra abierto todas sus l�neas son del mismo pedido abierto." & vbCrLf
sConsulta = sConsulta & "               SELECT @IDORDEN_PEDIDO_ABIERTO=MAX(LP.ORDEN_PED_ABIERTO)" & vbCrLf
sConsulta = sConsulta & "               FROM LINEAS_PEDIDO LP WITH(NOLOCK)      " & vbCrLf
sConsulta = sConsulta & "               WHERE LP.ORDEN=@IDORDEN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "               SELECT @EST_PEDIDO_ABIERTO=OE.EST,@ESTA_BORRADO=OE.BAJA_LOG, @PED_ABIERTO_TIPO=PED_ABIERTO_TIPO" & vbCrLf
sConsulta = sConsulta & "               FROM ORDEN_ENTREGA OE WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "               WHERE OE.ID=@IDORDEN_PEDIDO_ABIERTO" & vbCrLf
sConsulta = sConsulta & "               " & vbCrLf
sConsulta = sConsulta & "               -- Si el pedido abierto correspondiente est� cerrado (o anulado o no vigente) o borrado no se podr� deshacer la baja del contra abierto.                    " & vbCrLf
sConsulta = sConsulta & "               IF @EST_PEDIDO_ABIERTO<>101             " & vbCrLf
sConsulta = sConsulta & "                   SET @MOTIVO_NO_DESHACER_BORRADO=2" & vbCrLf
sConsulta = sConsulta & "               ELSE" & vbCrLf
sConsulta = sConsulta & "                   IF @ESTA_BORRADO=1" & vbCrLf
sConsulta = sConsulta & "                       SET @MOTIVO_NO_DESHACER_BORRADO=3" & vbCrLf
sConsulta = sConsulta & "                   ELSE" & vbCrLf
sConsulta = sConsulta & "                   BEGIN" & vbCrLf
sConsulta = sConsulta & "                       DECLARE @LP_ID INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                       IF @LINEAS=''" & vbCrLf
sConsulta = sConsulta & "                       BEGIN" & vbCrLf
sConsulta = sConsulta & "                           DECLARE C CURSOR FOR SELECT ID FROM LINEAS_PEDIDO LP WITH(NOLOCK) WHERE ORDEN=@IDORDEN  " & vbCrLf
sConsulta = sConsulta & "                           OPEN C" & vbCrLf
sConsulta = sConsulta & "                           FETCH NEXT FROM C INTO @LP_ID" & vbCrLf
sConsulta = sConsulta & "                           WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "                           BEGIN" & vbCrLf
sConsulta = sConsulta & "                               SET @LINEAS=@LINEAS + CAST(@LP_ID AS VARCHAR)+ ','" & vbCrLf
sConsulta = sConsulta & "                               FETCH NEXT FROM C INTO @LP_ID" & vbCrLf
sConsulta = sConsulta & "                           END" & vbCrLf
sConsulta = sConsulta & "                           Close C" & vbCrLf
sConsulta = sConsulta & "                           DEALLOCATE C" & vbCrLf
sConsulta = sConsulta & "                       END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                       DECLARE @CNT INT" & vbCrLf
sConsulta = sConsulta & "                       DECLARE @POS INT=1" & vbCrLf
sConsulta = sConsulta & "                       DECLARE @LP_NUM INT=NULL" & vbCrLf
sConsulta = sConsulta & "                       DECLARE @ALGUNA_BUENA AS TINYINT=0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                       DECLARE @EST_LIN INT" & vbCrLf
sConsulta = sConsulta & "                       DECLARE @LP_PA_TOTAL FLOAT" & vbCrLf
sConsulta = sConsulta & "                       DECLARE @LP_PA_PARCIAL FLOAT" & vbCrLf
sConsulta = sConsulta & "                       DECLARE @LP_CA_TOTAL FLOAT" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "                       SET @CNT=(SELECT (LEN(@LINEAS) - LEN(REPLACE(@LINEAS, ',', ''))) / LEN(','))" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                       SET @LINEAS_NO_DESHACER_BORRADO=''" & vbCrLf
sConsulta = sConsulta & "                       SET @LINEAS_A_DESHACER_BORRADO=''" & vbCrLf
sConsulta = sConsulta & "                       SET @ALGUNA_BUENA=0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                       WHILE @POS<=@CNT" & vbCrLf
sConsulta = sConsulta & "                       BEGIN" & vbCrLf
sConsulta = sConsulta & "                           SET @LP_ID=CAST(DBO.fnSPLIT(@LINEAS,',',@POS) AS INT)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                           --Al deshacer la baja de un pedido contra abierto se deber� comprobar si se superar� la cantidad/importe de las l�neas del abierto correspondiente          " & vbCrLf
sConsulta = sConsulta & "                           IF @PED_ABIERTO_TIPO=3 --PedidoAbiertoCantidad" & vbCrLf
sConsulta = sConsulta & "                           BEGIN" & vbCrLf
sConsulta = sConsulta & "                               SELECT @LP_NUM=LP.NUM,@EST_LIN=LP_PA.EST,@LP_PA_TOTAL=ISNULL(LP_PA.CANT_PED,0),@LP_PA_PARCIAL=ISNULL(LP_PA.CANTIDAD_PED_ABIERTO,0),@LP_CA_TOTAL=ISNULL(LP.CANT_PED,0)" & vbCrLf
sConsulta = sConsulta & "                               FROM LINEAS_PEDIDO LP WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                               INNER JOIN LINEAS_PEDIDO LP_PA WITH (NOLOCK) ON LP_PA.ID=LP.LINEA_PED_ABIERTO" & vbCrLf
sConsulta = sConsulta & "                               WHERE LP.ID=@LP_ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                               IF @EST_LIN=102" & vbCrLf
sConsulta = sConsulta & "                               BEGIN" & vbCrLf
sConsulta = sConsulta & "                                   SET @LINEAS_NO_DESHACER_BORRADO=@LINEAS_NO_DESHACER_BORRADO + CAST(@LP_NUM AS VARCHAR) + ',E,'" & vbCrLf
sConsulta = sConsulta & "                                   SET @MOTIVO_NO_DESHACER_BORRADO=4" & vbCrLf
sConsulta = sConsulta & "                               END" & vbCrLf
sConsulta = sConsulta & "                               ELSE" & vbCrLf
sConsulta = sConsulta & "                                   IF (@EST_LIN=101) AND (@LP_PA_TOTAL<(@LP_PA_PARCIAL + @LP_CA_TOTAL))" & vbCrLf
sConsulta = sConsulta & "                                   BEGIN" & vbCrLf
sConsulta = sConsulta & "                                       SET @LINEAS_NO_DESHACER_BORRADO=@LINEAS_NO_DESHACER_BORRADO + CAST(@LP_NUM AS VARCHAR) + ',C,'" & vbCrLf
sConsulta = sConsulta & "                                       SET @MOTIVO_NO_DESHACER_BORRADO=4" & vbCrLf
sConsulta = sConsulta & "                                   END" & vbCrLf
sConsulta = sConsulta & "                                   ELSE" & vbCrLf
sConsulta = sConsulta & "                                   BEGIN" & vbCrLf
sConsulta = sConsulta & "                                       SET @LINEAS_A_DESHACER_BORRADO=@LINEAS_A_DESHACER_BORRADO + CAST(@LP_ID AS VARCHAR) + ','" & vbCrLf
sConsulta = sConsulta & "                                       SET @ALGUNA_BUENA=1" & vbCrLf
sConsulta = sConsulta & "                                   END" & vbCrLf
sConsulta = sConsulta & "                           END" & vbCrLf
sConsulta = sConsulta & "                           ELSE --2 PedidoAbiertoImporte" & vbCrLf
sConsulta = sConsulta & "                           BEGIN" & vbCrLf
sConsulta = sConsulta & "                               SELECT @LP_NUM=LP.NUM,@EST_LIN=LP_PA.EST,@LP_PA_TOTAL=ISNULL(LP_PA.IMPORTE_PED,0),@LP_PA_PARCIAL=ISNULL(LP_PA.IMPORTE_PED_ABIERTO,0),@LP_CA_TOTAL=ISNULL(LP.IMPORTE_PED,0)" & vbCrLf
sConsulta = sConsulta & "                               FROM LINEAS_PEDIDO LP WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                               INNER JOIN LINEAS_PEDIDO LP_PA WITH (NOLOCK) ON LP_PA.ID=LP.LINEA_PED_ABIERTO" & vbCrLf
sConsulta = sConsulta & "                               WHERE LP.ID=@LP_ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                               IF @EST_LIN=102" & vbCrLf
sConsulta = sConsulta & "                               BEGIN" & vbCrLf
sConsulta = sConsulta & "                                   SET @LINEAS_NO_DESHACER_BORRADO=@LINEAS_NO_DESHACER_BORRADO + CAST(@LP_NUM AS VARCHAR) + ',E,'" & vbCrLf
sConsulta = sConsulta & "                                   SET @MOTIVO_NO_DESHACER_BORRADO=5" & vbCrLf
sConsulta = sConsulta & "                               END" & vbCrLf
sConsulta = sConsulta & "                               ELSE" & vbCrLf
sConsulta = sConsulta & "                                   IF (@EST_LIN=101) AND (@LP_PA_TOTAL<(@LP_PA_PARCIAL + @LP_CA_TOTAL))" & vbCrLf
sConsulta = sConsulta & "                                   BEGIN" & vbCrLf
sConsulta = sConsulta & "                                       SET @LINEAS_NO_DESHACER_BORRADO=@LINEAS_NO_DESHACER_BORRADO + CAST(@LP_NUM AS VARCHAR) + ',I,'" & vbCrLf
sConsulta = sConsulta & "                                       SET @MOTIVO_NO_DESHACER_BORRADO=5" & vbCrLf
sConsulta = sConsulta & "                                   END" & vbCrLf
sConsulta = sConsulta & "                                   ELSE" & vbCrLf
sConsulta = sConsulta & "                                   BEGIN" & vbCrLf
sConsulta = sConsulta & "                                       SET @LINEAS_A_DESHACER_BORRADO=@LINEAS_A_DESHACER_BORRADO + CAST(@LP_ID AS VARCHAR) + ','" & vbCrLf
sConsulta = sConsulta & "                                       SET @ALGUNA_BUENA=1" & vbCrLf
sConsulta = sConsulta & "                                   END" & vbCrLf
sConsulta = sConsulta & "                           END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                           SET @POS=@POS+1" & vbCrLf
sConsulta = sConsulta & "                       END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                       --si todas las l�neas superan esa cantidad/importe no se podr� deshacer el borrado" & vbCrLf
sConsulta = sConsulta & "                       IF @ALGUNA_BUENA=0" & vbCrLf
sConsulta = sConsulta & "                           IF @PED_ABIERTO_TIPO=3" & vbCrLf
sConsulta = sConsulta & "                               SET @MOTIVO_NO_DESHACER_BORRADO=41" & vbCrLf
sConsulta = sConsulta & "                           ELSE" & vbCrLf
sConsulta = sConsulta & "                               SET @MOTIVO_NO_DESHACER_BORRADO=51" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                       --POR CODIGO Si hay l�neas que no superan, se dar� un mensaje mostrando las l�neas que si lo superan, y preguntando al usuario si quiere deshacer la baja del pedido y de las l�neas que s� se puede (dejando de baja las l�neas que superan el importe) o prefiere no seguir con la acci�n de deshacer baja l�gica." & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "           END " & vbCrLf
sConsulta = sConsulta & "       ELSE" & vbCrLf
sConsulta = sConsulta & "           BEGIN                   " & vbCrLf
sConsulta = sConsulta & "               --si alguna l�nea del pedido procede de un proceso que est� anulado         " & vbCrLf
sConsulta = sConsulta & "               IF EXISTS(SELECT 1" & vbCrLf
sConsulta = sConsulta & "                         FROM LINEAS_PEDIDO LP WITH (NOLOCK)                           " & vbCrLf
sConsulta = sConsulta & "                           INNER JOIN LOG_LINEAS_PEDIDO LLP WITH(NOLOCK) ON LLP.ID_LINEAS_PEDIDO=LP.ID" & vbCrLf
sConsulta = sConsulta & "                           INNER JOIN PROCE P WITH (NOLOCK) ON P.ANYO=LLP.ANYO AND P.GMN1=LLP.GMN1 AND P.COD=LLP.PROCE" & vbCrLf
sConsulta = sConsulta & "                         WHERE ORDEN=@IDORDEN AND P.EST=20)" & vbCrLf
sConsulta = sConsulta & "                   SET @MOTIVO_NO_DESHACER_BORRADO=6" & vbCrLf
sConsulta = sConsulta & "           END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SELECT  @MOTIVO_NO_DESHACER_BORRADO,ISNULL(@LINEAS_NO_DESHACER_BORRADO,''),ISNULL(@LINEAS_A_DESHACER_BORRADO,'')" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSGS_BAJA_LOGICA_LINEA_PEDIDO]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSGS_BAJA_LOGICA_LINEA_PEDIDO]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSGS_BAJA_LOGICA_LINEA_PEDIDO]" & vbCrLf
sConsulta = sConsulta & "@ID int" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   DECLARE @TIPORECEP TINYINT" & vbCrLf
sConsulta = sConsulta & "   DECLARE @ANYO SMALLINT" & vbCrLf
sConsulta = sConsulta & "   DECLARE @GMN1 VARCHAR(25)" & vbCrLf
sConsulta = sConsulta & "   DECLARE @PROCE INT" & vbCrLf
sConsulta = sConsulta & "   DECLARE @TIPOPEDIDO TINYINT" & vbCrLf
sConsulta = sConsulta & "   DECLARE @MON VARCHAR(25)" & vbCrLf
sConsulta = sConsulta & "   DECLARE @ACUMULARIMP TINYINT" & vbCrLf
sConsulta = sConsulta & "   DECLARE @CAMBIO FLOAT" & vbCrLf
sConsulta = sConsulta & "   DECLARE @PRES5 VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "   DECLARE @PED_APROV TINYINT" & vbCrLf
sConsulta = sConsulta & "   DECLARE @ESTADO SMALLINT" & vbCrLf
sConsulta = sConsulta & "   declare @LIN_ABIERTO INT" & vbCrLf
sConsulta = sConsulta & "   DECLARE @TIPOABIERTO TINYINT" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   SET NOCOUNT ON;" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @TIPORECEP=LP.TIPORECEPCION,@ANYO=LP.ANYO,@GMN1=LP.GMN1,@PROCE=LP.PROCE,@TIPOPEDIDO=OE.TIPO," & vbCrLf
sConsulta = sConsulta & "       @MON=OE.MON,@CAMBIO=OE.CAMBIO,@ESTADO=OE.EST,@LIN_ABIERTO=LP.LINEA_PED_ABIERTO" & vbCrLf
sConsulta = sConsulta & "FROM LINEAS_PEDIDO LP WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN ORDEN_ENTREGA OE WITH (NOLOCK) ON OE.ID=LP.ORDEN" & vbCrLf
sConsulta = sConsulta & "WHERE LP.ID=@ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   --Hay que poner a null anyo, gmn1, proce e item, pero se necesitan para localizar Item_adj as? que se hace al final." & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   --Si la l?nea tiene proceso de compra desvincular el proceso del pedido si el proceso no tiene" & vbCrLf
sConsulta = sConsulta & "   --m?s l?neas en el pedido o m?s pedidos     " & vbCrLf
sConsulta = sConsulta & "   IF NOT @ANYO IS NULL" & vbCrLf
sConsulta = sConsulta & "   BEGIN       " & vbCrLf
sConsulta = sConsulta & "       IF @TIPOPEDIDO<>1" & vbCrLf
sConsulta = sConsulta & "           --Pedido negociado, directos" & vbCrLf
sConsulta = sConsulta & "           --Mirar otras l?neas de pedido para el proceso" & vbCrLf
sConsulta = sConsulta & "           --CALIDAD: sin WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "           IF NOT EXISTS(SELECT ID FROM LINEAS_PEDIDO WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND ID<>@ID)    " & vbCrLf
sConsulta = sConsulta & "               BEGIN                           " & vbCrLf
sConsulta = sConsulta & "               --Si el proceso NO est? en otros pedidos se pone PED_APROV=0 Sin pedidos" & vbCrLf
sConsulta = sConsulta & "               UPDATE PROCE SET PED_APROV=0 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "               END" & vbCrLf
sConsulta = sConsulta & "           ELSE" & vbCrLf
sConsulta = sConsulta & "               BEGIN                   " & vbCrLf
sConsulta = sConsulta & "                   SELECT @PED_APROV=PED_APROV FROM PROCE WITH (NOLOCK) WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "                   " & vbCrLf
sConsulta = sConsulta & "                   IF @PED_APROV=3     --Pedido excedido" & vbCrLf
sConsulta = sConsulta & "                           --CALIDAD: Sin WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                           IF (@TIPORECEP=0 AND  NOT EXISTS (SELECT COUNT(ITEM_ADJ.ANYO)  FROM ITEM_ADJ WHERE CANT_PED>ISNULL(CANT_ADJ,0) AND ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE)) OR" & vbCrLf
sConsulta = sConsulta & "                              (@TIPORECEP=1 AND  NOT EXISTS (SELECT COUNT(ITEM_ADJ.ANYO)  FROM ITEM_ADJ WHERE IMPORTE_PED>ISNULL(ADJUDICADO,0) AND ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE))" & vbCrLf
sConsulta = sConsulta & "                               --Con pedidos" & vbCrLf
sConsulta = sConsulta & "                               UPDATE PROCE SET PED_APROV=2 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE                                             " & vbCrLf
sConsulta = sConsulta & "               END" & vbCrLf
sConsulta = sConsulta & "       ELSE" & vbCrLf
sConsulta = sConsulta & "           --Pedido de cat?logo" & vbCrLf
sConsulta = sConsulta & "           --CALIDAD: Sin WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "           IF NOT EXISTS(SELECT COUNT(ID) FROM CATALOG_LIN WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE)" & vbCrLf
sConsulta = sConsulta & "               UPDATE PROCE SET PED_APROV=0    --Sin pedidos" & vbCrLf
sConsulta = sConsulta & "               WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE  " & vbCrLf
sConsulta = sConsulta & "           ELSE" & vbCrLf
sConsulta = sConsulta & "               UPDATE PROCE SET PED_APROV=1    --En cat?logo" & vbCrLf
sConsulta = sConsulta & "               WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE                          " & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   --Actualizar la cantidad/importe pedido de la adjudicaci?n si se hab?a emitido" & vbCrLf
sConsulta = sConsulta & "   IF @ESTADO>=2 AND (@ESTADO<20 or @ESTADO=100 or @ESTADO =101)" & vbCrLf
sConsulta = sConsulta & "       IF @TIPORECEP=0     --Recepci?n por cantidad" & vbCrLf
sConsulta = sConsulta & "           UPDATE ITEM_ADJ SET CANT_PED=IA.CANT_PED-(LP.CANT_PED*LP.FC) ,PEDIDO = NULL" & vbCrLf
sConsulta = sConsulta & "           FROM ITEM_ADJ IA WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "               INNER JOIN LINEAS_PEDIDO LP WITH (NOLOCK) ON IA.ANYO=LP.ANYO AND IA.GMN1=LP.GMN1 AND IA.PROCE=LP.PROCE" & vbCrLf
sConsulta = sConsulta & "                   AND IA.ITEM=LP.ITEM AND IA.PROVE=ISNULL(LP.PROVEADJ,LP.PROVE)" & vbCrLf
sConsulta = sConsulta & "           WHERE LP.ID=@ID" & vbCrLf
sConsulta = sConsulta & "      ELSE                 --Recepci?n por importe" & vbCrLf
sConsulta = sConsulta & "           UPDATE ITEM_ADJ SET IMPORTE_PED=IA.IMPORTE_PED-(LP.IMPORTE_PED) ,PEDIDO = NULL" & vbCrLf
sConsulta = sConsulta & "           FROM ITEM_ADJ IA WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "               INNER JOIN LINEAS_PEDIDO LP WITH (NOLOCK) ON IA.ANYO=LP.ANYO AND IA.GMN1=LP.GMN1 AND IA.PROCE=LP.PROCE" & vbCrLf
sConsulta = sConsulta & "                   AND IA.ITEM=LP.ITEM AND IA.PROVE=ISNULL(LP.PROVEADJ,LP.PROVE)" & vbCrLf
sConsulta = sConsulta & "           WHERE LP.ID=@ID" & vbCrLf
sConsulta = sConsulta & "   if @LIN_ABIERTO is not null" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "       SELECT @TIPOABIERTO=OE.PED_ABIERTO_TIPO FROM LINEAS_PEDIDO LP WITH (NOLOCK) INNER JOIN ORDEN_ENTREGA OE WITH (NOLOCK) ON OE.ID=LP.ORDEN WHERE LP.ID=@LIN_ABIERTO" & vbCrLf
sConsulta = sConsulta & "       IF @TIPOABIERTO= 2 --por importe" & vbCrLf
sConsulta = sConsulta & "           UPDATE IA SET IMPORTE_PED_ABIERTO=IA.IMPORTE_PED_ABIERTO-LP.IMPORTE_PED" & vbCrLf
sConsulta = sConsulta & "           FROM LINEAS_PEDIDO IA WITH (NOLOCK) INNER JOIN LINEAS_PEDIDO LP WITH (NOLOCK) ON IA.id=LP.LINEA_PED_ABIERTO" & vbCrLf
sConsulta = sConsulta & "           WHERE LP.ID=@ID         " & vbCrLf
sConsulta = sConsulta & "       " & vbCrLf
sConsulta = sConsulta & "       if @TIPOABIERTO=3" & vbCrLf
sConsulta = sConsulta & "           UPDATE IA SET CANTIDAD_PED_ABIERTO=IA.CANTIDAD_PED_ABIERTO-(LP.CANT_PED*LP.FC)" & vbCrLf
sConsulta = sConsulta & "           FROM LINEAS_PEDIDO IA WITH (NOLOCK) INNER JOIN LINEAS_PEDIDO LP WITH (NOLOCK) ON IA.id=LP.LINEA_PED_ABIERTO" & vbCrLf
sConsulta = sConsulta & "           WHERE LP.ID=@ID" & vbCrLf
sConsulta = sConsulta & "   end" & vbCrLf
sConsulta = sConsulta & "   --Hay que poner a null anyo, gmn1, proce e item de la l?nea." & vbCrLf
sConsulta = sConsulta & "   --Baja l?gica" & vbCrLf
sConsulta = sConsulta & "DECLARE @DESCR VARCHAR(200)=NULL" & vbCrLf
sConsulta = sConsulta & "SELECT @DESCR=DESCR" & vbCrLf
sConsulta = sConsulta & "FROM ITEM I WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN LINEAS_PEDIDO LP WITH(NOLOCK) ON LP.ANYO=I.ANYO AND LP.GMN1=I.GMN1_PROCE AND LP.PROCE=I.PROCE AND LP.ITEM=I.ID" & vbCrLf
sConsulta = sConsulta & "WHERE LP.ID=@ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @DESCR IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "       UPDATE LINEAS_PEDIDO SET BAJA_LOG=1,ANYO=NULL,GMN1=NULL,PROCE=NULL,ITEM=NULL, DESCR_LIBRE=@DESCR, FEC_BAJA_LOG=GETDATE() WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "       UPDATE LINEAS_PEDIDO SET BAJA_LOG=1,ANYO=NULL,GMN1=NULL,PROCE=NULL,ITEM=NULL, FEC_BAJA_LOG=GETDATE() WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_GETPARAMETROS]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_GETPARAMETROS]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_GETPARAMETROS]" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   SELECT [ID],[NEO],[NEM],[NEPP],[NEPC],[INSTWEB],[LOCKTIMEOUT],[FUP],[SUBASTA],[NEP3]," & vbCrLf
sConsulta = sConsulta & "       [NEP4],[APROVISION],[PEDIDO],[INTEGRACION],[BD_IDIOMAS],[ADMIN_PUBLICA],[TIMEOUT_BL_PROCE]," & vbCrLf
sConsulta = sConsulta & "       [ENVIO_ASINCRONO],[PEDIDO_LIBRE],[WIN_SEC],[REMOTEGROUP],[TRASPASO_ADJ_ERP],[TRASPASO_PED_ERP]," & vbCrLf
sConsulta = sConsulta & "       [TRASLADO_APROV_ERP],[READER],[HELP_PDF],[SINC_MAT],[CONSERVAR_NUMPEDIDO],[PROCE_PROVE_GRUPOS]," & vbCrLf
sConsulta = sConsulta & "       [ACCESO_FSGS],[ACCESO_FSWS],[ACCESO_FSQA],[VARCAL_MAT],[INVITADO],[FSQA_REVISOR]," & vbCrLf
sConsulta = sConsulta & "       [REUVIR],[USAR_ART4_UON],[USAR_ORGCOMPRAS],[TRASPASO_ART_PLANIF],[PM_CARGAR_ALL_GRUPOS],[MULTIMAT]," & vbCrLf
sConsulta = sConsulta & "       [ARTGEN],[PYMES],[ACCESO_FSSM],[POOLS],[REUBICAR_AHORROS],[OBLASIG_PRES_ART_PM],[PERSONALIZACION]," & vbCrLf
sConsulta = sConsulta & "       [URL_WEBSERVICE],[HOJA_ADJS_ABIERTO],[ACCESO_FSFA],[LISTADOS_MICRO],[RECDIR_FECHA],[RECDIR_CANT]," & vbCrLf
sConsulta = sConsulta & "       [RECDIR_FECHA_EP],[RECDIR_CANT_EP],[PEDDIR_PETICIONARIO_SOL],[PEDDIRFAC],[VER_NUMEXT]," & vbCrLf
sConsulta = sConsulta & "       [WIN_SEC_WEB],[URL_CHANGE_PWD],[ACCESO_FSCN],[ACCESO_LCX],[IM_NO_COSTES_LINEA],[ACCESO_CONTRATO]," & vbCrLf
sConsulta = sConsulta & "       [FSEP_BLOQ_FEC_RECEP],[ACCESO_FSBI],[ACCESO_FSAL],[PED_EXPRESS_ART_NEGOCIADOS],[EP_AVISO_EMISION_PED]," & vbCrLf
sConsulta = sConsulta & "       [EP_DESACTIVAR_CARGA_ART],[EP_ACTIVAR_CARGA_SOL_PM],ISNULL(IM_RECEPAUTO,0) IM_RECEPAUTO,[PROVE_ERP_NC]," & vbCrLf
sConsulta = sConsulta & "       [EP_MARCAR_ACEP_PROVE],[INT_MANT_EST_TRASP_ADJ],[MOSTRAR_RECEP_SALIDA_ALMACEN],[EP_NOTIFPROVE_ADJUNTAR_PEDIDO_PDF], [USAR_RMTE_EMP], [MARCADOR_CARPETA_PLANTILLAS]," & vbCrLf
sConsulta = sConsulta & "       [MOSTRAR_FECHA_CONTABLE], [ACTIVAR_BORRADO_LOGICO_PEDIDOS], [MOSTRAR_TODOS_NIVELES_MATERIAL]" & vbCrLf
sConsulta = sConsulta & "   FROM PARGEN_INTERNO WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SELECT [ID],[ELIMLINCATALOG],[OBLHOMPEDIDOS],[OBLCODPEDIDO],[CAT_ADJESP_ART],[CAT_ADJESP_PROVE]," & vbCrLf
sConsulta = sConsulta & "   [OBL_DIST_PEDIR],[PEDPRES1],[PEDPRES2],[PEDPRES3],[PEDPRES4],[DEFPRESITEM],[SINCPRESITEM]," & vbCrLf
sConsulta = sConsulta & "   [OBLPRES1],[OBLPRES2],[OBLPRES3],[OBLPRES4],[NIVPRES1],[NIVPRES2],[NIVPRES3],[NIVPRES4],[OBLCODPEDDIR]," & vbCrLf
sConsulta = sConsulta & "   [DIRENTRCABPED],[DESGLPEDCENAPROV],[PEDIDOABIERTO],[VIAPAG],[OBLCODPEDIDO_OBL] , [CODRECEPERP] , [ACT_PED_ABIERTO], [BLOQCODPEDIDO],[OBLFECENTREGA]" & vbCrLf
sConsulta = sConsulta & "   FROM PARGEN_PED WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SELECT [ID],[IDI],[DEN]" & vbCrLf
sConsulta = sConsulta & "   FROM PARGEN_LIT WITH (NOLOCK) ORDER BY [ID], IDI" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SELECT ACTIVLOG,HISTORICO_PWD, EDAD_MIN_PWD, EDAD_MAX_PWD, COMPLEJIDAD_PWD, MIN_SIZE_PWD, USAPRES1," & vbCrLf
sConsulta = sConsulta & "      USAPRES2,USAPRES3,USAPRES4, ACTCAMPO1, ACTCAMPO2, ACTCAMPO3,ACTCAMPO4,PED_EMAIL,PED_EMAIL_MODO," & vbCrLf
sConsulta = sConsulta & "      PED_PUB_PORTAL" & vbCrLf
sConsulta = sConsulta & "   FROM PARGEN_GEST WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SELECT ACTIVA,SENTIDO FROM TABLAS_INTEGRACION_ERP WITH (NOLOCK) WHERE TABLA = 12" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SELECT ACTIVA,SENTIDO FROM TABLAS_INTEGRACION_ERP WITH (NOLOCK) WHERE TABLA = 14" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SELECT USU,PWD,FECPWD FROM WIN_ADM WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SELECT MONCEN FROM PARGEN_DEF WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SELECT ADM_GS,RUTA_CONTRATO,BUSQUEDA_AUX FROM PARGEN_PM WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SELECT COUNT(1) AS PEDIDOSERP FROM TABLAS_INTEGRACION_ERP WITH (NOLOCK) WHERE (TABLA=11 or TABLA=13 ) AND SENTIDO IN (2,3)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SELECT TABLA,MAX(SENTIDO) AS SMAX, MIN(SENTIDO) AS SMIN" & vbCrLf
sConsulta = sConsulta & "   FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "   WHERE ACTIVA=1 AND SENTIDO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   GROUP BY TABLA" & vbCrLf
sConsulta = sConsulta & "   ORDER BY TABLA ASC" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   SELECT POT_A_VAL_PROVISIONAL_SEL_PROVE" & vbCrLf
sConsulta = sConsulta & "   FROM PARGEN_QA WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FSPM_SAVE_INSTANCIA_EN_FILTRO]') AND type in (N'P', N'PC'))" & vbCrLf
sConsulta = sConsulta & "DROP PROCEDURE [dbo].[FSPM_SAVE_INSTANCIA_EN_FILTRO]" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE [dbo].[FSPM_SAVE_INSTANCIA_EN_FILTRO]" & vbCrLf
sConsulta = sConsulta & "@INSTANCIA INT," & vbCrLf
sConsulta = sConsulta & "@INSERT TINYINT," & vbCrLf
sConsulta = sConsulta & "@QA TINYINT=0," & vbCrLf
sConsulta = sConsulta & "@CONTRATO TINYINT=0" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE @INSTANCIA_L INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN1 AS NVARCHAR(10)" & vbCrLf
sConsulta = sConsulta & "   DECLARE @GMN2 AS NVARCHAR(10)" & vbCrLf
sConsulta = sConsulta & "   DECLARE @GMN3 AS NVARCHAR(10)" & vbCrLf
sConsulta = sConsulta & "   DECLARE @GMN4 AS NVARCHAR(10)" & vbCrLf
sConsulta = sConsulta & "   DECLARE @MOSTRARTODOSNIVELES TINYINT" & vbCrLf
sConsulta = sConsulta & "   SELECT @MOSTRARTODOSNIVELES=PARGEN_INTERNO.MOSTRAR_TODOS_NIVELES_MATERIAL FROM PARGEN_INTERNO WITH(NOLOCK) WHERE ID=1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @INSTANCIA_L=@INSTANCIA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Obtenemos el nombre de las tablas din?micas del filtro (falta a?adir el prefijo del idioma)." & vbCrLf
sConsulta = sConsulta & "DECLARE @TABLA NVARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "IF @QA=0" & vbCrLf
sConsulta = sConsulta & "BEGIN      " & vbCrLf
sConsulta = sConsulta & "IF @CONTRATO=0" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT @TABLA=F.NOM_TABLA FROM INSTANCIA I WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN FORM_GRUPO G2 WITH (NOLOCK) ON I.FORMULARIO=G2.FORMULARIO" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_FILTROS F WITH (NOLOCK) ON G2.FORMULARIO=F.FORMULARIO" & vbCrLf
sConsulta = sConsulta & "WHERE I.ID = @INSTANCIA_L" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT @TABLA=F.NOM_TABLA FROM INSTANCIA I WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN FORM_GRUPO G2 WITH (NOLOCK) ON I.FORMULARIO=G2.FORMULARIO" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PM_CONTR_FILTROS F WITH (NOLOCK) ON G2.FORMULARIO=F.FORMULARIO" & vbCrLf
sConsulta = sConsulta & "WHERE I.ID = @INSTANCIA_L" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT @TABLA=F.NOM_TABLA FROM INSTANCIA I WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN FORM_GRUPO G2 WITH (NOLOCK) ON I.FORMULARIO=G2.FORMULARIO" & vbCrLf
sConsulta = sConsulta & "INNER JOIN QA_FILTROS F WITH (NOLOCK) ON G2.FORMULARIO=F.FORMULARIO" & vbCrLf
sConsulta = sConsulta & "WHERE I.ID = @INSTANCIA_L" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Obtenemos las longitudes de los materiales de la tabla DIC" & vbCrLf
sConsulta = sConsulta & "DECLARE @LEN_GMN1 INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @LEN_GMN2 INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @LEN_GMN3 INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @LEN_GMN4 INT" & vbCrLf
sConsulta = sConsulta & "SELECT @LEN_GMN1=DIC.LONGITUD, @LEN_GMN2=DIC2.LONGITUD,@LEN_GMN3=DIC3.LONGITUD, @LEN_GMN4=DIC4.LONGITUD FROM DIC WITH (NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN DIC DIC2 WITH (NOLOCK) ON DIC2.NOMBRE='GMN2'" & vbCrLf
sConsulta = sConsulta & "INNER JOIN DIC DIC3 WITH (NOLOCK) ON DIC3.NOMBRE='GMN3'" & vbCrLf
sConsulta = sConsulta & "INNER JOIN DIC DIC4 WITH (NOLOCK) ON DIC4.NOMBRE='GMN4'" & vbCrLf
sConsulta = sConsulta & "WHERE DIC.NOMBRE = 'GMN1'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Guardamos los IDs en una tabla temporal" & vbCrLf
sConsulta = sConsulta & "--Es lo ?nico que cambia de los 3 casos (QA, Solicitudes y Contratos)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLINSERT NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE #G1 (ID_CAMPO INT)" & vbCrLf
sConsulta = sConsulta & "SET @SQLINSERT = N'INSERT INTO #G1 SELECT ID_CAMPO FROM '" & vbCrLf
sConsulta = sConsulta & "IF @QA=0" & vbCrLf
sConsulta = sConsulta & "IF @CONTRATO = 0" & vbCrLf
sConsulta = sConsulta & "SET @SQLINSERT = @SQLINSERT + N'UPD_VISORES_PM WITH(NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQLINSERT = @SQLINSERT + N'UPD_VISORES_CONTR WITH(NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQLINSERT = @SQLINSERT + N'UPD_VISORES_QA WITH(NOLOCK) '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLINSERT = @SQLINSERT + N'WHERE (ACCION=''I'' OR ACCION=''A'') AND (NOM_TABLA=@TABLA OR NOM_TABLA=@TABLA+''_DESGLOSE'') AND OK=0'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLINSERT, N'@TABLA NVARCHAR(100)', @TABLA=@TABLA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @LINEA INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @LINEA_ANT INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @COPIACAMPO_ID INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @CAMPO_ORIGEN INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @SUBTIPO INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @TIPO_CAMPO_GS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @VALOR_NUM FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @VALOR_TEXT NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @VALOR_FEC DATETIME" & vbCrLf
sConsulta = sConsulta & "DECLARE @VALOR_BOOL TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @VALOR_TEXT_DEN NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @VALOR_TEXT_COD NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @INTRO TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_ATRIB_GS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @TABLA_EXTERNA INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @CAMPO_DEF INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL_COD NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLCAMPOSINSERT NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @EJECUTARSQL TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @CAMPO_PADRE_DESGLOSE INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @CAMPO_PADRE_DESGLOSE_ANT INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @DENOMINACION NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @CAMPOTRATADO TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @EMPIEZAIDIOMA INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ACABADENOMINACION INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @EMPIEZADENOMINACION INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @SEPARADOR INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRESUPUESTOS NVARCHAR(800)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PORCENTAJE NVARCHAR(400)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SEPARADOR_DEN INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @SEPARADOR_PORCENTAJE INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDIOMA AS NVARCHAR(400)" & vbCrLf
sConsulta = sConsulta & "DECLARE @CAMPO_NUM_COUNT SMALLINT" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   CREATE TABLE #T_VALORES_NUM (ID SMALLINT PRIMARY KEY,VALOR FLOAT)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE curIDIOMAS CURSOR LOCAL FOR" & vbCrLf
sConsulta = sConsulta & "SELECT COD FROM IDIOMAS WITH(NOLOCK) WHERE APLICACION=1" & vbCrLf
sConsulta = sConsulta & "OPEN curIDIOMAS" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curIDIOMAS INTO @IDIOMA" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SQL=''" & vbCrLf
sConsulta = sConsulta & "SET @SQLCAMPOSINSERT=''" & vbCrLf
sConsulta = sConsulta & "SET @EJECUTARSQL=0" & vbCrLf
sConsulta = sConsulta & "SET @LINEA_ANT=1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @INSERT = 1 AND @CONTRATO = 1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N'INSERT INTO ' + @IDIOMA + N'_CONTR_FILTRO1 VALUES (@INSTANCIA_L)'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA_L INT', @INSTANCIA_L=@INSTANCIA_L" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @INSERT = 1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SQLCAMPOSINSERT=N'INSERT INTO '+@IDIOMA+N'_'+@TABLA+N' (INSTANCIA,'" & vbCrLf
sConsulta = sConsulta & "SET @SQL = N' VALUES (@INSTANCIA_L,'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=N' UPDATE ' + @IDIOMA + N'_' + @TABLA  + N' SET '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE curSOLICITUDES_CAMPOS CURSOR LOCAL FOR" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT C.ID,FC.ID CAMPO_ORIGEN,FC.SUBTIPO,FC.TIPO_CAMPO_GS,C.VALOR_TEXT,C.VALOR_NUM," & vbCrLf
sConsulta = sConsulta & "C.VALOR_FEC,C.VALOR_BOOL,C.VALOR_TEXT_COD,C.VALOR_TEXT_DEN,FC.INTRO,FC.ID_ATRIB_GS," & vbCrLf
sConsulta = sConsulta & "FC.TABLA_EXTERNA,C.COPIA_CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "FROM COPIA_CAMPO C WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN INSTANCIA I WITH(NOLOCK) ON I.ID=C.INSTANCIA AND C.NUM_VERSION=I.ULT_VERSION" & vbCrLf
sConsulta = sConsulta & "INNER JOIN FORM_CAMPO FC WITH (NOLOCK) ON FC.ID=C.COPIA_CAMPO_DEF AND FC.BAJALOG = 0" & vbCrLf
sConsulta = sConsulta & "INNER JOIN FORM_GRUPO FG WITH (NOLOCK) ON FG.ID = FC.GRUPO AND FG.BAJALOG = 0" & vbCrLf
sConsulta = sConsulta & "WHERE FC.ES_SUBCAMPO=0 AND NOT FC.SUBTIPO=9 AND I.ID=@INSTANCIA_L" & vbCrLf
sConsulta = sConsulta & "AND (NOT FC.TIPO_CAMPO_GS=106 OR FC.TIPO_CAMPO_GS IS NULL)" & vbCrLf
sConsulta = sConsulta & "AND (FC.SUBTIPO=FC.SUBTIPO OR (FC.SUBTIPO=5 AND FC.SUBTIPO IN (6,7)) OR (FC.SUBTIPO=6 AND FC.SUBTIPO=7))  --si la definici?n del campo ha cambiado de texto corto a texto medio o largo, o de texto medio a texto largo" & vbCrLf
sConsulta = sConsulta & "AND FC.ID NOT IN (SELECT ID_CAMPO FROM #G1)  -- campos que se han a?adido en el formulario pero no se ha hecho todavia ALTER en las tablas de filtros (BatchUpdates)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "      SET @CAMPO_NUM_COUNT=0       " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "OPEN curSOLICITUDES_CAMPOS" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curSOLICITUDES_CAMPOS INTO @COPIACAMPO_ID,@CAMPO_ORIGEN, @SUBTIPO, @TIPO_CAMPO_GS, @VALOR_TEXT, @VALOR_NUM, @VALOR_FEC, @VALOR_BOOL, @VALOR_TEXT_COD, @VALOR_TEXT_DEN, @INTRO, @ID_ATRIB_GS,@TABLA_EXTERNA, @CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN      " & vbCrLf
sConsulta = sConsulta & "SET @EJECUTARSQL=1" & vbCrLf
sConsulta = sConsulta & "--Reemplazamos las comillas simples en valor_text y valor_text_den" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SET @VALOR_TEXT = REPLACE(@VALOR_TEXT,'''','''''')" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT_DEN IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SET @VALOR_TEXT_DEN = REPLACE(@VALOR_TEXT_DEN,'''','''''')" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQLCAMPOSINSERT = @SQLCAMPOSINSERT + 'C_' + CONVERT(NVARCHAR,@CAMPO_ORIGEN)  + ','" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQLCAMPOSINSERT = @SQLCAMPOSINSERT + N'C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N','" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'C_' + CONVERT(NVARCHAR,@CAMPO_ORIGEN) + '='" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL +N'C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'='" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "----------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "IF @SUBTIPO = 3 -- tipo fecha" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_FEC IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'CONVERT(DATETIME,''' + CONVERT(VARCHAR,@VALOR_FEC) + ''')'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'NULL'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @SUBTIPO = 4 -- tipo booleano" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_BOOL IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + CONVERT(NVARCHAR,@VALOR_BOOL)" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @SUBTIPO = 8 -- tipo archivo" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'N''' + LEFT(@VALOR_TEXT, 800) + N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @TIPO_CAMPO_GS IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @TABLA_EXTERNA IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN                              " & vbCrLf
sConsulta = sConsulta & "IF @SUBTIPO=2 -- tipo num?rico" & vbCrLf
sConsulta = sConsulta & "                              BEGIN                                                                                                                                        " & vbCrLf
sConsulta = sConsulta & "                                   SET @CAMPO_NUM_COUNT=@CAMPO_NUM_COUNT+1                                 " & vbCrLf
sConsulta = sConsulta & "                                   INSERT INTO #T_VALORES_NUM VALUES (@CAMPO_NUM_COUNT,@VALOR_NUM)                                                                         " & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + '(SELECT VALOR FROM #T_VALORES_NUM WHERE ID=' + CAST(@CAMPO_NUM_COUNT AS NVARCHAR) + ')'                                                                                                                                 " & vbCrLf
sConsulta = sConsulta & "                               END" & vbCrLf
sConsulta = sConsulta & "ELSE    -- tipo texto o tipo lista texto" & vbCrLf
sConsulta = sConsulta & "BEGIN                          " & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1 AND @QA=0 AND @CONTRATO=0" & vbCrLf
sConsulta = sConsulta & "SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @INTRO=1 AND @VALOR_NUM IS NOT NULL --tipo lista texto" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @ID_ATRIB_GS IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @IDIOMA='SPA'" & vbCrLf
sConsulta = sConsulta & "SELECT @DENOMINACION=VALOR_TEXT_SPA" & vbCrLf
sConsulta = sConsulta & "FROM CAMPO_VALOR_LISTA WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE CAMPO=CONVERT(VARCHAR,@CAMPO_DEF) AND ORDEN=CONVERT(VARCHAR,@VALOR_NUM)" & vbCrLf
sConsulta = sConsulta & "IF @IDIOMA='ENG'" & vbCrLf
sConsulta = sConsulta & "SELECT @DENOMINACION=VALOR_TEXT_ENG" & vbCrLf
sConsulta = sConsulta & "FROM CAMPO_VALOR_LISTA WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE CAMPO=CONVERT(VARCHAR,@CAMPO_DEF) AND ORDEN=CONVERT(VARCHAR,@VALOR_NUM)" & vbCrLf
sConsulta = sConsulta & "IF @IDIOMA='GER'" & vbCrLf
sConsulta = sConsulta & "SELECT @DENOMINACION=VALOR_TEXT_GER" & vbCrLf
sConsulta = sConsulta & "FROM CAMPO_VALOR_LISTA WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE CAMPO=CONVERT(VARCHAR,@CAMPO_DEF) AND ORDEN=CONVERT(VARCHAR,@VALOR_NUM)" & vbCrLf
sConsulta = sConsulta & "IF @IDIOMA ='FRA'" & vbCrLf
sConsulta = sConsulta & "SELECT @DENOMINACION=VALOR_TEXT_FRA" & vbCrLf
sConsulta = sConsulta & "FROM CAMPO_VALOR_LISTA WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE CAMPO=CONVERT(VARCHAR,@CAMPO_DEF) AND ORDEN=CONVERT(VARCHAR,@VALOR_NUM)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @DENOMINACION IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'N''' + replace(@DENOMINACION,'''','''''') + ''''" & vbCrLf
sConsulta = sConsulta & "ELSE                                               " & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'N'''+REPLACE(@DENOMINACION,'''','''''')+''''+N',CONVERT(NVARCHAR,'+CONVERT(NVARCHAR,@VALOR_NUM)+N')'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'N'''+REPLACE(@DENOMINACION,'''','''''')+''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=CONVERT(NVARCHAR,'+CONVERT(NVARCHAR,@VALOR_NUM)+N')'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + N'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + N'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'N''' + @VALOR_TEXT + ''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + N'N'''+@VALOR_TEXT+N''''+N','+N''''+CONVERT(NVARCHAR,@VALOR_NUM)+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + N'N'''+@VALOR_TEXT+N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+ CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N''''+CONVERT(NVARCHAR,@VALOR_NUM)+N''''" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE -- tipo texto" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @SUBTIPO=6 --TEXTO MEDIO: 800" & vbCrLf
sConsulta = sConsulta & "SET @VALOR_TEXT = LEFT(@VALOR_TEXT, 800)" & vbCrLf
sConsulta = sConsulta & "IF @SUBTIPO=5 --TEXTO CORTO: 100" & vbCrLf
sConsulta = sConsulta & "SET @VALOR_TEXT = LEFT(@VALOR_TEXT, 100)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'N''' + @VALOR_TEXT + ''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + N'N'''+@VALOR_TEXT+N''''+N','+N'N'''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + N'N'''+@VALOR_TEXT+N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+ CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N'N'''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + N'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN          " & vbCrLf
sConsulta = sConsulta & "IF @SUBTIPO=2 -- tipo num?rico" & vbCrLf
sConsulta = sConsulta & "                              BEGIN" & vbCrLf
sConsulta = sConsulta & "                                   SET @CAMPO_NUM_COUNT=@CAMPO_NUM_COUNT+1                                 " & vbCrLf
sConsulta = sConsulta & "                                   INSERT INTO #T_VALORES_NUM VALUES (@CAMPO_NUM_COUNT,@VALOR_NUM)                                                                         " & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + '(SELECT VALOR FROM #T_VALORES_NUM WHERE ID=' + CAST(@CAMPO_NUM_COUNT AS NVARCHAR) + ')'                                 " & vbCrLf
sConsulta = sConsulta & "                               END" & vbCrLf
sConsulta = sConsulta & "ELSE    -- tipo texto o tipo lista texto" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1 AND @QA=0 AND @CONTRATO=0" & vbCrLf
sConsulta = sConsulta & "SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT_DEN IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'N''' + @VALOR_TEXT_DEN + ''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'N'''+@VALOR_TEXT_DEN+N''''+N','+N'N'''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'N'''+@VALOR_TEXT_DEN+N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N'N'''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_' +CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE -- campo del sistema" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @CAMPOTRATADO = 0" & vbCrLf
sConsulta = sConsulta & "---------------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "-- Formas de pago, Monedas, Familia de materiales, Unidades, Destinos, Pa?ses y Provincias" & vbCrLf
sConsulta = sConsulta & "-- FORMATO: <Idioma>|<Texto>#<Idioma>|<Texto>" & vbCrLf
sConsulta = sConsulta & "-- FORMATO MATERIALES: cod. ultimo nivel (VALOR_TEXT) +  <Idioma>|<Texto>#<Idioma>|<Texto>" & vbCrLf
sConsulta = sConsulta & "------------------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "IF @TIPO_CAMPO_GS = 101 OR @TIPO_CAMPO_GS = 102 OR @TIPO_CAMPO_GS = 103 OR @TIPO_CAMPO_GS = 105 OR @TIPO_CAMPO_GS = 109 OR @TIPO_CAMPO_GS = 107 OR @TIPO_CAMPO_GS = 108" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1 AND @QA=0 AND @CONTRATO=0" & vbCrLf
sConsulta = sConsulta & "SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+ N'_COD,'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @CAMPOTRATADO = 1" & vbCrLf
sConsulta = sConsulta & "-- Materiales" & vbCrLf
sConsulta = sConsulta & "IF @TIPO_CAMPO_GS = 103" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                                  BEGIN                                                    " & vbCrLf
sConsulta = sConsulta & "                                      IF @MOSTRARTODOSNIVELES=0" & vbCrLf
sConsulta = sConsulta & "                                      BEGIN" & vbCrLf
sConsulta = sConsulta & "                                          -- Al hacer Len de valor_text me est? quitando los espacios en blanco de la derecha" & vbCrLf
sConsulta = sConsulta & "                                          IF LEN(@VALOR_TEXT + '#')-1 = @LEN_GMN1 + @LEN_GMN2 + @LEN_GMN3 + @LEN_GMN4" & vbCrLf
sConsulta = sConsulta & "                                              SET @SQL = @SQL + N'N''' + RTRIM(RIGHT(@VALOR_TEXT,@LEN_GMN4)) + N' - '" & vbCrLf
sConsulta = sConsulta & "                                          ELSE" & vbCrLf
sConsulta = sConsulta & "                                              IF LEN(@VALOR_TEXT + '#')-1 = @LEN_GMN1 + @LEN_GMN2 + @LEN_GMN3" & vbCrLf
sConsulta = sConsulta & "                                                  SET @SQL = @SQL + N'N''' + RTRIM(RIGHT(@VALOR_TEXT,@LEN_GMN3)) + N' - '" & vbCrLf
sConsulta = sConsulta & "                                              ELSE" & vbCrLf
sConsulta = sConsulta & "                                                  IF LEN(@VALOR_TEXT + '#')-1 = @LEN_GMN1 + @LEN_GMN2" & vbCrLf
sConsulta = sConsulta & "                                                      SET @SQL = @SQL + N'N''' + RTRIM(RIGHT(@VALOR_TEXT,@LEN_GMN2)) + N' - '" & vbCrLf
sConsulta = sConsulta & "                                                  ELSE" & vbCrLf
sConsulta = sConsulta & "                                                      SET @SQL = @SQL + N'N''' + RTRIM(RIGHT(@VALOR_TEXT,@LEN_GMN1)) + N' - '" & vbCrLf
sConsulta = sConsulta & "                                       END" & vbCrLf
sConsulta = sConsulta & "                                       ELSE" & vbCrLf
sConsulta = sConsulta & "                                       BEGIN" & vbCrLf
sConsulta = sConsulta & "                                           SET @GMN1 = SUBSTRING(@VALOR_TEXT, 1, @LEN_GMN1)" & vbCrLf
sConsulta = sConsulta & "                                           SET @GMN2 = SUBSTRING(@VALOR_TEXT,@LEN_GMN1+1, @LEN_GMN2)" & vbCrLf
sConsulta = sConsulta & "                                           SET @GMN3 = SUBSTRING(@VALOR_TEXT,@LEN_GMN1+@LEN_GMN2+1, @LEN_GMN3)" & vbCrLf
sConsulta = sConsulta & "                                           SET @GMN4 = SUBSTRING(@VALOR_TEXT,@LEN_GMN1+@LEN_GMN2+@LEN_GMN3+1, @LEN_GMN4)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                                           -- Al hacer Len de valor_text me est? quitando los espacios en blanco de la derecha" & vbCrLf
sConsulta = sConsulta & "                                           IF LEN(@VALOR_TEXT + '#')-1 = @LEN_GMN1 + @LEN_GMN2 + @LEN_GMN3 + @LEN_GMN4" & vbCrLf
sConsulta = sConsulta & "                                              SET @SQL = @SQL + N'N''' + @GMN1 + N' - ' + @GMN2 + N' - ' + @GMN3 + N' - ' + @GMN4 + N' - '" & vbCrLf
sConsulta = sConsulta & "                                           ELSE" & vbCrLf
sConsulta = sConsulta & "                                              IF LEN(@VALOR_TEXT + '#')-1 = @LEN_GMN1 + @LEN_GMN2 + @LEN_GMN3" & vbCrLf
sConsulta = sConsulta & "                                                  SET @SQL = @SQL + N'N''' + @GMN1 + N' - ' + @GMN2 + N' - ' + @GMN3 + N' - '" & vbCrLf
sConsulta = sConsulta & "                                              ELSE" & vbCrLf
sConsulta = sConsulta & "                                                  IF LEN(@VALOR_TEXT + '#')-1 = @LEN_GMN1 + @LEN_GMN2" & vbCrLf
sConsulta = sConsulta & "                                                      SET @SQL = @SQL + N'N''' + @GMN1 + N' - ' + @GMN2 + N' - '" & vbCrLf
sConsulta = sConsulta & "                                                  ELSE" & vbCrLf
sConsulta = sConsulta & "                                                      SET @SQL = @SQL + N'N''' + @GMN1 + N' - '" & vbCrLf
sConsulta = sConsulta & "                                       END" & vbCrLf
sConsulta = sConsulta & "                                   END" & vbCrLf
sConsulta = sConsulta & "-- Todos" & vbCrLf
sConsulta = sConsulta & "IF RIGHT(@VALOR_TEXT_DEN,1)='#' SET @VALOR_TEXT_DEN=LEFT(@VALOR_TEXT_DEN,LEN(@VALOR_TEXT_DEN)-1)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @EMPIEZAIDIOMA=CHARINDEX(@IDIOMA + '|',@VALOR_TEXT_DEN)" & vbCrLf
sConsulta = sConsulta & "IF (@EMPIEZAIDIOMA) > 0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @ACABADENOMINACION = CHARINDEX('#',@VALOR_TEXT_DEN,@EMPIEZAIDIOMA)" & vbCrLf
sConsulta = sConsulta & "SET @EMPIEZADENOMINACION = CHARINDEX('|',@VALOR_TEXT_DEN,@EMPIEZAIDIOMA)+1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @TIPO_CAMPO_GS <> 103" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+'N'''" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ACABADENOMINACION>0" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+SUBSTRING(@VALOR_TEXT_DEN,@EMPIEZADENOMINACION,@ACABADENOMINACION-@EMPIEZADENOMINACION) + ''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+RIGHT(@VALOR_TEXT_DEN,LEN(@VALOR_TEXT_DEN)-@EMPIEZADENOMINACION+1) + ''''" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @QA=0 AND @CONTRATO=0" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N','+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N',C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N''''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT IS NOT NULL AND @TIPO_CAMPO_GS = 103" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+N','+N'N'''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N'N'''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "-- Proveedores, Departamentos, Organizaciones de Compras, Centros,Proveedor ERP" & vbCrLf
sConsulta = sConsulta & "-- FORMATO: C?digo (VALOR_TEXT) - Denominaci?n (VALOR_TEXT_DEN)" & vbCrLf
sConsulta = sConsulta & "--------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "IF @TIPO_CAMPO_GS = 11 OR @TIPO_CAMPO_GS = 100  OR @TIPO_CAMPO_GS = 122 OR @TIPO_CAMPO_GS = 123 OR @TIPO_CAMPO_GS = 124 OR @TIPO_CAMPO_GS = 143" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @CAMPOTRATADO = 1" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1 AND @QA=0 AND @CONTRATO=0" & vbCrLf
sConsulta = sConsulta & "SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'N''' + @VALOR_TEXT + ' - ' + @VALOR_TEXT_DEN + ''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'N'''+@VALOR_TEXT+N' - '+@VALOR_TEXT_DEN+N''''+N','+N'N'''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'N'''+@VALOR_TEXT+N' - '+@VALOR_TEXT_DEN+N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N'N'''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "-- Centros de coste, Partidas presupuestarias, Activos" & vbCrLf
sConsulta = sConsulta & "-- FORMATO: cod. ultimo nivel (VALOR_TEXT) +  <Idioma>|<Texto>#<Idioma>|<Texto>" & vbCrLf
sConsulta = sConsulta & "--------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "IF @TIPO_CAMPO_GS = 129 OR @TIPO_CAMPO_GS = 130 OR @TIPO_CAMPO_GS = 131 OR @TIPO_CAMPO_GS=132" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @CAMPOTRATADO=1" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1 AND @QA=0 AND @CONTRATO=0" & vbCrLf
sConsulta = sConsulta & "SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF RIGHT(@VALOR_TEXT_DEN,1)='#' SET @VALOR_TEXT_DEN=LEFT(@VALOR_TEXT_DEN,LEN(@VALOR_TEXT_DEN)-1)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SEPARADOR = CHARINDEX('#',@VALOR_TEXT,1)" & vbCrLf
sConsulta = sConsulta & "WHILE @SEPARADOR > 0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @VALOR_TEXT = RIGHT(@VALOR_TEXT,LEN(@VALOR_TEXT)-@SEPARADOR)" & vbCrLf
sConsulta = sConsulta & "SET @SEPARADOR = CHARINDEX('#',@VALOR_TEXT,1)" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SEPARADOR = CHARINDEX('|',@VALOR_TEXT,1)" & vbCrLf
sConsulta = sConsulta & "WHILE @SEPARADOR > 0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @VALOR_TEXT = RIGHT(@VALOR_TEXT,LEN(@VALOR_TEXT)-@SEPARADOR)" & vbCrLf
sConsulta = sConsulta & "SET @SEPARADOR = CHARINDEX('|',@VALOR_TEXT,1)" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'N''' + @VALOR_TEXT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Denominaci?n en el idioma" & vbCrLf
sConsulta = sConsulta & "SET @EMPIEZAIDIOMA=CHARINDEX(@IDIOMA + '|',@VALOR_TEXT_DEN)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF (@EMPIEZAIDIOMA) > 0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @ACABADENOMINACION = CHARINDEX('#',@VALOR_TEXT_DEN,@EMPIEZAIDIOMA)" & vbCrLf
sConsulta = sConsulta & "SET @EMPIEZADENOMINACION = CHARINDEX('|',@VALOR_TEXT_DEN,@EMPIEZAIDIOMA)+1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ACABADENOMINACION>0" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ' - ' + SUBSTRING(@VALOR_TEXT_DEN,@EMPIEZADENOMINACION,@ACABADENOMINACION-@EMPIEZADENOMINACION)" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ' - ' + RIGHT(@VALOR_TEXT_DEN,LEN(@VALOR_TEXT_DEN)-@EMPIEZADENOMINACION+1)" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+N','+N'N'''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N'N'''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "-- Personas, Comprador" & vbCrLf
sConsulta = sConsulta & "---------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "IF @TIPO_CAMPO_GS = 115 OR @TIPO_CAMPO_GS = 144" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @CAMPOTRATADO = 1" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1 AND @QA=0 AND @CONTRATO=0" & vbCrLf
sConsulta = sConsulta & "SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT_DEN IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'N''' + @VALOR_TEXT_DEN + ''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'N'''+@VALOR_TEXT_DEN+N''''+N','+N'N'''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'N'''+@VALOR_TEXT_DEN+N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N'N'''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "-- Almacen / Empresa" & vbCrLf
sConsulta = sConsulta & "---------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "IF @TIPO_CAMPO_GS = 125 OR @TIPO_CAMPO_GS = 149" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @CAMPOTRATADO = 1" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1 AND @QA=0 AND @CONTRATO=0" & vbCrLf
sConsulta = sConsulta & "SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT_DEN IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'N''' + @VALOR_TEXT_DEN + ''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'N'''+@VALOR_TEXT_DEN+N''''+N','+N''''+CONVERT(NVARCHAR,@VALOR_NUM)+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'N'''+@VALOR_TEXT_DEN+N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N''''+CONVERT(NVARCHAR,@VALOR_NUM)+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "------------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "-- Cod. Art?culo (viejo), Cod. Art?culo (nuevo), Denominaci?n del art?culo y Unidades Organizativas" & vbCrLf
sConsulta = sConsulta & "------------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "IF @TIPO_CAMPO_GS = 104  OR @TIPO_CAMPO_GS = 118 OR @TIPO_CAMPO_GS = 119 OR @TIPO_CAMPO_GS = 121" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @CAMPOTRATADO = 1" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1 AND @QA=0 AND @CONTRATO=0" & vbCrLf
sConsulta = sConsulta & "SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'N''' + @VALOR_TEXT + ''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'N'''+@VALOR_TEXT+N''''+N','+N'N'''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'N'''+@VALOR_TEXT +N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N'N'''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "---------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "-- Presupuestos" & vbCrLf
sConsulta = sConsulta & "-- FORMATO: Ej 2005 - PC-002  (50.00%); 2005 - PC-001 (50.00%);" & vbCrLf
sConsulta = sConsulta & "--  VALOR_TEXT_DEN: 2005 - PC-002#2005 - PC-001" & vbCrLf
sConsulta = sConsulta & "--  VALOR_TEXT: 1_11_0.5#1_10_0.5" & vbCrLf
sConsulta = sConsulta & "---------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "IF @TIPO_CAMPO_GS = 110  OR @TIPO_CAMPO_GS = 111 OR @TIPO_CAMPO_GS = 112 OR @TIPO_CAMPO_GS = 113" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @CAMPOTRATADO = 1" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1 AND @QA=0 AND @CONTRATO=0" & vbCrLf
sConsulta = sConsulta & "SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @PRESUPUESTOS = ''" & vbCrLf
sConsulta = sConsulta & "SET @SEPARADOR_DEN = CHARINDEX('#',@VALOR_TEXT_DEN)" & vbCrLf
sConsulta = sConsulta & "WHILE @SEPARADOR_DEN > 0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @PRESUPUESTOS = @PRESUPUESTOS + SUBSTRING(@VALOR_TEXT_DEN,1,@SEPARADOR_DEN-1)" & vbCrLf
sConsulta = sConsulta & "SET @VALOR_TEXT_DEN = RIGHT(@VALOR_TEXT_DEN,LEN(@VALOR_TEXT_DEN)-@SEPARADOR_DEN)" & vbCrLf
sConsulta = sConsulta & "SET @PORCENTAJE = SUBSTRING(@VALOR_TEXT,1,CHARINDEX('#',@VALOR_TEXT)-1)" & vbCrLf
sConsulta = sConsulta & "SET @SEPARADOR_PORCENTAJE = 1" & vbCrLf
sConsulta = sConsulta & "WHILE @SEPARADOR_PORCENTAJE > 0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @PORCENTAJE = RIGHT(@PORCENTAJE,LEN(@PORCENTAJE)-@SEPARADOR_PORCENTAJE)" & vbCrLf
sConsulta = sConsulta & "SET @SEPARADOR_PORCENTAJE = CHARINDEX('_',@PORCENTAJE,@SEPARADOR_PORCENTAJE+1)" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "SET @PRESUPUESTOS = @PRESUPUESTOS + '(' + CONVERT(VARCHAR,CONVERT(DECIMAL(3,2),@PORCENTAJE)*100) + '%); '" & vbCrLf
sConsulta = sConsulta & "SET @VALOR_TEXT = RIGHT(@VALOR_TEXT,LEN(@VALOR_TEXT)-CHARINDEX('#',@VALOR_TEXT))" & vbCrLf
sConsulta = sConsulta & "SET @SEPARADOR_DEN = CHARINDEX('#',@VALOR_TEXT_DEN)" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @PORCENTAJE = @VALOR_TEXT" & vbCrLf
sConsulta = sConsulta & "SET @SEPARADOR_PORCENTAJE = 1" & vbCrLf
sConsulta = sConsulta & "WHILE @SEPARADOR_PORCENTAJE > 0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @PORCENTAJE = RIGHT(@PORCENTAJE,LEN(@PORCENTAJE)-@SEPARADOR_PORCENTAJE)" & vbCrLf
sConsulta = sConsulta & "SET @SEPARADOR_PORCENTAJE = CHARINDEX('_',@PORCENTAJE,@SEPARADOR_PORCENTAJE+1)" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @PRESUPUESTOS = @PRESUPUESTOS + @VALOR_TEXT_DEN + '(' + CAST(CONVERT(FLOAT,@PORCENTAJE)*100 AS VARCHAR) + '%);'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @PRESUPUESTOS <> ''" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'N''' + @PRESUPUESTOS + ''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'N''' + @PRESUPUESTOS + N'''' + N',' + N'N''' + @VALOR_TEXT + N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'N''' + @PRESUPUESTOS + N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N'N'''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL,'+N'C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                          ---------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "-- Periodo renovaci?n y estado homologaci?n" & vbCrLf
sConsulta = sConsulta & "---------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "                          IF @TIPO_CAMPO_GS=148  OR @TIPO_CAMPO_GS=150" & vbCrLf
sConsulta = sConsulta & "                          BEGIN" & vbCrLf
sConsulta = sConsulta & "                               IF @VALOR_NUM IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                               BEGIN" & vbCrLf
sConsulta = sConsulta & "                                   SET @CAMPOTRATADO = 1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                                   IF @INSERT=1 AND @QA=0 AND @CONTRATO=0" & vbCrLf
sConsulta = sConsulta & "                                          SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+ N'_COD,'                                " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                                   IF @IDIOMA='SPA'" & vbCrLf
sConsulta = sConsulta & "                                       SELECT @DENOMINACION=VALOR_TEXT_SPA" & vbCrLf
sConsulta = sConsulta & "                                       FROM CAMPO_VALOR_LISTA WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                                       WHERE CAMPO=CONVERT(VARCHAR,@CAMPO_DEF) AND ORDEN=CONVERT(VARCHAR,@VALOR_NUM)" & vbCrLf
sConsulta = sConsulta & "                                   IF @IDIOMA='ENG'" & vbCrLf
sConsulta = sConsulta & "                                       SELECT @DENOMINACION=VALOR_TEXT_ENG" & vbCrLf
sConsulta = sConsulta & "                                       FROM CAMPO_VALOR_LISTA WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                                       WHERE CAMPO=CONVERT(VARCHAR,@CAMPO_DEF) AND ORDEN=CONVERT(VARCHAR,@VALOR_NUM)" & vbCrLf
sConsulta = sConsulta & "                                   IF @IDIOMA='GER'" & vbCrLf
sConsulta = sConsulta & "                                       SELECT @DENOMINACION=VALOR_TEXT_GER" & vbCrLf
sConsulta = sConsulta & "                                       FROM CAMPO_VALOR_LISTA WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                                       WHERE CAMPO=CONVERT(VARCHAR,@CAMPO_DEF) AND ORDEN=CONVERT(VARCHAR,@VALOR_NUM)" & vbCrLf
sConsulta = sConsulta & "                                   IF @IDIOMA ='FRA'" & vbCrLf
sConsulta = sConsulta & "                                       SELECT @DENOMINACION=VALOR_TEXT_FRA" & vbCrLf
sConsulta = sConsulta & "                                       FROM CAMPO_VALOR_LISTA WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "                                       WHERE CAMPO=CONVERT(VARCHAR,@CAMPO_DEF) AND ORDEN=CONVERT(VARCHAR,@VALOR_NUM)                               " & vbCrLf
sConsulta = sConsulta & "                                   IF @DENOMINACION IS NOT NULL    " & vbCrLf
sConsulta = sConsulta & "                                       IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "                                           SET @SQL=@SQL+N'N'''+REPLACE(@DENOMINACION,'''','''''')+''''" & vbCrLf
sConsulta = sConsulta & "                                       ELSE                                                                                                                                                                                " & vbCrLf
sConsulta = sConsulta & "                                           IF @INSERT=1                                                                                " & vbCrLf
sConsulta = sConsulta & "                                               SET @SQL=@SQL+N'N'''+REPLACE(@DENOMINACION,'''','''''')+''''+N',CONVERT(NVARCHAR,'+CONVERT(NVARCHAR,@VALOR_NUM)+N')'                                                                            " & vbCrLf
sConsulta = sConsulta & "                                           ELSE                                    " & vbCrLf
sConsulta = sConsulta & "                                               SET @SQL=@SQL+N'N'''+REPLACE(@DENOMINACION,'''','''''')+''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=CONVERT(NVARCHAR,'+CONVERT(NVARCHAR,@VALOR_NUM)+N')'                                                             " & vbCrLf
sConsulta = sConsulta & "                                   ELSE            " & vbCrLf
sConsulta = sConsulta & "                                       IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "                                           SET @SQL=@SQL + N'NULL'" & vbCrLf
sConsulta = sConsulta & "                                       ELSE                                                            " & vbCrLf
sConsulta = sConsulta & "                                           IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "                                               SET @SQL=@SQL + N'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "                                           ELSE" & vbCrLf
sConsulta = sConsulta & "                                               SET @SQL=@SQL + N'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "                               END" & vbCrLf
sConsulta = sConsulta & "                          END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CAMPOTRATADO = 0 -- Resto" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @SUBTIPO = 3 -- tipo fecha" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_FEC IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + '''' + @VALOR_FEC + ''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @SUBTIPO = 4 -- tipo booleano" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_BOOL IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + CONVERT(VARCHAR,@VALOR_BOOL)" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @SUBTIPO=2 -- tipo num?rico" & vbCrLf
sConsulta = sConsulta & "                                      BEGIN" & vbCrLf
sConsulta = sConsulta & "                                          SET @CAMPO_NUM_COUNT=@CAMPO_NUM_COUNT+1                                  " & vbCrLf
sConsulta = sConsulta & "                                           INSERT INTO #T_VALORES_NUM VALUES (@CAMPO_NUM_COUNT,@VALOR_NUM)                                                                         " & vbCrLf
sConsulta = sConsulta & "                                           SET @SQL = @SQL + '(SELECT VALOR FROM #T_VALORES_NUM WHERE ID=' + CAST(@CAMPO_NUM_COUNT AS NVARCHAR) + ')'" & vbCrLf
sConsulta = sConsulta & "                                       END" & vbCrLf
sConsulta = sConsulta & "ELSE    -- tipo texto" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1 AND @QA=0 AND @CONTRATO=0" & vbCrLf
sConsulta = sConsulta & "SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'N''' + @VALOR_TEXT + ''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'N'''+@VALOR_TEXT+N''''+N','+N'N'''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+'N'''+@VALOR_TEXT+''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+ N'_COD='+N'N'''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @QA=1 OR @CONTRATO=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+ N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ','" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curSOLICITUDES_CAMPOS INTO @COPIACAMPO_ID,@CAMPO_ORIGEN, @SUBTIPO, @TIPO_CAMPO_GS, @VALOR_TEXT, @VALOR_NUM, @VALOR_FEC, @VALOR_BOOL, @VALOR_TEXT_COD, @VALOR_TEXT_DEN, @INTRO, @ID_ATRIB_GS,@TABLA_EXTERNA, @CAMPO_DEF" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CLOSE curSOLICITUDES_CAMPOS" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curSOLICITUDES_CAMPOS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @EJECUTARSQL=1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @INSERT = 1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = LEFT(@SQLCAMPOSINSERT,LEN(@SQLCAMPOSINSERT)-1) + ') ' + LEFT(@SQL,LEN(@SQL)-1) + ')'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = LEFT(@SQL,LEN(@SQL)-1) + ' WHERE INSTANCIA = @INSTANCIA_L'" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "SET @SQL=N'IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = ''dbo'' AND  TABLE_NAME='''+@IDIOMA+N'_'+@TABLA+''')) BEGIN '+@SQL+N' END'     " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA_L INT', @INSTANCIA_L=@INSTANCIA_L    " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "          DELETE FROM #T_VALORES_NUM" & vbCrLf
sConsulta = sConsulta & "END    " & vbCrLf
sConsulta = sConsulta & "--*********************************" & vbCrLf
sConsulta = sConsulta & "--*********************************" & vbCrLf
sConsulta = sConsulta & "-- AHORA LOS CAMPOS DE LOS DESGLOSES (SI NO ES QA NI CONTRATO)" & vbCrLf
sConsulta = sConsulta & "--**********************************" & vbCrLf
sConsulta = sConsulta & "IF @QA=0 AND @CONTRATO=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SQL=''" & vbCrLf
sConsulta = sConsulta & "SET @SQLCAMPOSINSERT=''" & vbCrLf
sConsulta = sConsulta & "SET @EJECUTARSQL=0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Pongo insert a 1 para que siempre inserte, ya que si de una etapa a otra se crean nuevas lineas de desglose s?lo estar?a haciendo update de las lineas existentes" & vbCrLf
sConsulta = sConsulta & "--Ahora elimino todas las l?neas y vuelvo a insertar las que haya en ese momento, as? tambi?n controlo si se elimina alguna l?nea" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLDELETE NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @INSERT_ORIGINAL TINYINT" & vbCrLf
sConsulta = sConsulta & "SET @SQLDELETE=''" & vbCrLf
sConsulta = sConsulta & "SET @INSERT_ORIGINAL=@INSERT" & vbCrLf
sConsulta = sConsulta & "SET @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQLDELETE='DELETE '+@IDIOMA+N'_'+@TABLA+N'_DESGLOSE WHERE FORM_INSTANCIA=@INSTANCIA_L'" & vbCrLf
sConsulta = sConsulta & "SET @SQLDELETE=N'IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = ''dbo'' AND  TABLE_NAME='''+@IDIOMA+N'_'+@TABLA+N'_DESGLOSE'')) BEGIN '+@SQLDELETE+N' END'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQLDELETE, N'@INSTANCIA_L INT',@INSTANCIA_L=@INSTANCIA_L" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @INSERT = 1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SQLCAMPOSINSERT=N'INSERT INTO '+@IDIOMA+N'_'+@TABLA+N'_DESGLOSE (FORM_INSTANCIA,LINEA,DESGLOSE,'" & vbCrLf
sConsulta = sConsulta & "SET @SQL=N' VALUES (@INSTANCIA_L,@LINEA,'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=N' UPDATE '+@IDIOMA+N'_'+@TABLA+N'_DESGLOSE SET '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE curSOLICITUDES_CAMPOS_DESGLOSE CURSOR LOCAL FOR" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT CLD.LINEA,FC.ID CAMPO_ORIGEN,FC.SUBTIPO,FC.TIPO_CAMPO_GS,CLD.VALOR_TEXT,CLD.VALOR_NUM,CLD.VALOR_FEC,CLD.VALOR_BOOL," & vbCrLf
sConsulta = sConsulta & "CLD.VALOR_TEXT_COD,CLD.VALOR_TEXT_DEN,FC.INTRO,FC.ID_ATRIB_GS,FC.TABLA_EXTERNA,CC.COPIA_CAMPO_DEF," & vbCrLf
sConsulta = sConsulta & "D.CAMPO_PADRE" & vbCrLf
sConsulta = sConsulta & "FROM COPIA_CAMPO CC WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "INNER JOIN INSTANCIA I WITH(NOLOCK) ON I.ID=CC.INSTANCIA AND CC.NUM_VERSION=I.ULT_VERSION" & vbCrLf
sConsulta = sConsulta & "INNER JOIN FORM_CAMPO FC WITH (NOLOCK) ON FC.ID=CC.COPIA_CAMPO_DEF AND FC.BAJALOG = 0" & vbCrLf
sConsulta = sConsulta & "INNER JOIN FORM_GRUPO FG WITH (NOLOCK) ON FG.ID = FC.GRUPO AND FG.BAJALOG = 0" & vbCrLf
sConsulta = sConsulta & "INNER JOIN COPIA_LINEA_DESGLOSE CLD WITH(NOLOCK) ON CLD.CAMPO_HIJO=CC.ID" & vbCrLf
sConsulta = sConsulta & "INNER JOIN DESGLOSE D WITH(NOLOCK) ON D.CAMPO_HIJO=FC.ID" & vbCrLf
sConsulta = sConsulta & "WHERE FC.ES_SUBCAMPO=1 AND FC.SUBTIPO<>9 AND (FC.TIPO_CAMPO_GS<>106 OR FC.TIPO_CAMPO_GS IS NULL)" & vbCrLf
sConsulta = sConsulta & "AND (FC.SUBTIPO=FC.SUBTIPO OR (FC.SUBTIPO=5 AND FC.SUBTIPO IN (6,7)) OR (FC.SUBTIPO=6 AND FC.SUBTIPO=7))--si la definici?n del campo ha cambiado de texto corto a texto medio o largo, o de texto medio a texto largo" & vbCrLf
sConsulta = sConsulta & "AND FC.ID NOT IN (SELECT ID_CAMPO FROM #G1)  -- campos que se han a?adido en el formulario pero no se ha hecho todavia ALTER en las tablas de filtros (BatchUpdates)" & vbCrLf
sConsulta = sConsulta & "AND I.ID=@INSTANCIA_L" & vbCrLf
sConsulta = sConsulta & "ORDER BY CAMPO_PADRE,LINEA     " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "OPEN curSOLICITUDES_CAMPOS_DESGLOSE" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curSOLICITUDES_CAMPOS_DESGLOSE INTO @LINEA,@CAMPO_ORIGEN,@SUBTIPO,@TIPO_CAMPO_GS,@VALOR_TEXT," & vbCrLf
sConsulta = sConsulta & "@VALOR_NUM,@VALOR_FEC,@VALOR_BOOL,@VALOR_TEXT_COD,@VALOR_TEXT_DEN,@INTRO,@ID_ATRIB_GS,@TABLA_EXTERNA," & vbCrLf
sConsulta = sConsulta & "@CAMPO_DEF,@CAMPO_PADRE_DESGLOSE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "          SET @CAMPO_NUM_COUNT=0" & vbCrLf
sConsulta = sConsulta & "SET @CAMPO_PADRE_DESGLOSE_ANT=@CAMPO_PADRE_DESGLOSE" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLAUX NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLCAMPOSINSERTAUX NVARCHAR(MAX)" & vbCrLf
sConsulta = sConsulta & "SET @SQLAUX=@SQL" & vbCrLf
sConsulta = sConsulta & "SET @SQLCAMPOSINSERTAUX=@SQLCAMPOSINSERT" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N','" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @EJECUTARSQL=1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @LINEA<>@LINEA_ANT OR @CAMPO_PADRE_DESGLOSE<>@CAMPO_PADRE_DESGLOSE_ANT" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @EJECUTARSQL=1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @INSERT = 1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=LEFT(@SQLCAMPOSINSERT,LEN(@SQLCAMPOSINSERT)-1) + ') '+LEFT(@SQL,LEN(@SQL)-1)+')'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=LEFT(@SQL,LEN(@SQL)-1)+' WHERE FORM_INSTANCIA=@INSTANCIA_L AND DESGLOSE=@CAMPO_PADRE_DESGLOSE AND LINEA=@LINEA '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL=N'IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = ''dbo'' AND  TABLE_NAME='''+@IDIOMA+N'_'+@TABLA+N'_DESGLOSE'')) BEGIN '+@SQL+N' END'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA_L INT,@CAMPO_PADRE_DESGLOSE INT,@LINEA INT',@INSTANCIA_L=@INSTANCIA_L,@CAMPO_PADRE_DESGLOSE=@CAMPO_PADRE_DESGLOSE_ANT,@LINEA=@LINEA_ANT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQLAUX" & vbCrLf
sConsulta = sConsulta & "SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERTAUX" & vbCrLf
sConsulta = sConsulta & "SET @LINEA_ANT=@LINEA" & vbCrLf
sConsulta = sConsulta & "SET @CAMPO_PADRE_DESGLOSE_ANT=@CAMPO_PADRE_DESGLOSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N','" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "--Reemplazamos las comillas simples en valor_text y valor_text_den" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SET @VALOR_TEXT = REPLACE(@VALOR_TEXT,'''','''''')" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT_DEN IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SET @VALOR_TEXT_DEN = REPLACE(@VALOR_TEXT_DEN,'''','''''')" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT + N'C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N','" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'='" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "----------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "IF @SUBTIPO = 3 -- tipo fecha" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_FEC IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'CONVERT(DATETIME,''' + CONVERT(VARCHAR,@VALOR_FEC) + ''')'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @SUBTIPO = 4 -- tipo booleano" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_BOOL IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + CONVERT(NVARCHAR,@VALOR_BOOL)" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @SUBTIPO = 8 -- tipo archivo" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'N''' + LEFT(@VALOR_TEXT, 800) + N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @TIPO_CAMPO_GS IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @TABLA_EXTERNA IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @SUBTIPO=2 -- tipo num?rico" & vbCrLf
sConsulta = sConsulta & "                                  BEGIN" & vbCrLf
sConsulta = sConsulta & "                                       SET @CAMPO_NUM_COUNT=@CAMPO_NUM_COUNT+1                                 " & vbCrLf
sConsulta = sConsulta & "                                       INSERT INTO #T_VALORES_NUM VALUES (@CAMPO_NUM_COUNT,@VALOR_NUM)                                                                         " & vbCrLf
sConsulta = sConsulta & "                                       SET @SQL = @SQL + '(SELECT VALOR FROM #T_VALORES_NUM WHERE ID=' + CAST(@CAMPO_NUM_COUNT AS NVARCHAR) + ')'                                                                                                          " & vbCrLf
sConsulta = sConsulta & "                                   END" & vbCrLf
sConsulta = sConsulta & "ELSE    -- tipo texto o tipo lista texto" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @INTRO=1 AND @VALOR_NUM IS NOT NULL --tipo lista texto" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @ID_ATRIB_GS IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @IDIOMA='SPA'" & vbCrLf
sConsulta = sConsulta & "SELECT @DENOMINACION=VALOR_TEXT_SPA" & vbCrLf
sConsulta = sConsulta & "FROM CAMPO_VALOR_LISTA WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE CAMPO=CONVERT(VARCHAR,@CAMPO_DEF) AND ORDEN=CONVERT(VARCHAR,@VALOR_NUM)" & vbCrLf
sConsulta = sConsulta & "IF @IDIOMA='ENG'" & vbCrLf
sConsulta = sConsulta & "SELECT @DENOMINACION=VALOR_TEXT_ENG" & vbCrLf
sConsulta = sConsulta & "FROM CAMPO_VALOR_LISTA WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE CAMPO=CONVERT(VARCHAR,@CAMPO_DEF) AND ORDEN=CONVERT(VARCHAR,@VALOR_NUM)" & vbCrLf
sConsulta = sConsulta & "IF @IDIOMA='GER'" & vbCrLf
sConsulta = sConsulta & "SELECT @DENOMINACION=VALOR_TEXT_GER" & vbCrLf
sConsulta = sConsulta & "FROM CAMPO_VALOR_LISTA WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE CAMPO=CONVERT(VARCHAR,@CAMPO_DEF) AND ORDEN=CONVERT(VARCHAR,@VALOR_NUM)" & vbCrLf
sConsulta = sConsulta & "IF @IDIOMA='FRA'" & vbCrLf
sConsulta = sConsulta & "SELECT @DENOMINACION=VALOR_TEXT_FRA" & vbCrLf
sConsulta = sConsulta & "FROM CAMPO_VALOR_LISTA WITH(NOLOCK)" & vbCrLf
sConsulta = sConsulta & "WHERE CAMPO=CONVERT(VARCHAR,@CAMPO_DEF) AND ORDEN=CONVERT(VARCHAR,@VALOR_NUM)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @DENOMINACION IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'N'''+REPLACE(@DENOMINACION,'''','''''')+''''+N',CONVERT(NVARCHAR,'+CONVERT(NVARCHAR,@VALOR_NUM)+N')'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'N'''+REPLACE(@DENOMINACION,'''','''''')+''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=CONVERT(NVARCHAR,'+CONVERT(NVARCHAR,@VALOR_NUM)+N')'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + N'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + N'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + N'N'''+@VALOR_TEXT+N''''+N','+N''''+CONVERT(NVARCHAR,@VALOR_NUM)+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + N'N'''+@VALOR_TEXT+N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+ CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N''''+CONVERT(NVARCHAR,@VALOR_NUM)+N''''" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE -- tipo texto" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @SUBTIPO=6 --TEXTO MEDIO: 800" & vbCrLf
sConsulta = sConsulta & "SET @VALOR_TEXT = LEFT(@VALOR_TEXT, 800)" & vbCrLf
sConsulta = sConsulta & "IF @SUBTIPO=5 --TEXTO CORTO: 100" & vbCrLf
sConsulta = sConsulta & "SET @VALOR_TEXT = LEFT(@VALOR_TEXT, 100)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + N'N'''+@VALOR_TEXT+N''''+N','+N'N'''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + N'N'''+@VALOR_TEXT+N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+ CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N'N'''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL + N'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @SUBTIPO=2 -- tipo num?rico" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "                                       SET @CAMPO_NUM_COUNT=@CAMPO_NUM_COUNT+1                                 " & vbCrLf
sConsulta = sConsulta & "                                       INSERT INTO #T_VALORES_NUM VALUES (@CAMPO_NUM_COUNT,@VALOR_NUM)                                                                         " & vbCrLf
sConsulta = sConsulta & "                                       SET @SQL = @SQL + '(SELECT VALOR FROM #T_VALORES_NUM WHERE ID=' + CAST(@CAMPO_NUM_COUNT AS NVARCHAR) + ')'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE    -- tipo texto o tipo lista texto" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT_DEN IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'N'''+@VALOR_TEXT_DEN+N''''+N','+N'N'''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'N'''+@VALOR_TEXT_DEN+N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N'N'''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_' +CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE -- campo del sistema" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @CAMPOTRATADO = 0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "---------------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "-- Formas de pago, Monedas, Familia de materiales, Unidades, Destinos, Pa?ses y Provincias" & vbCrLf
sConsulta = sConsulta & "-- FORMATO: <Idioma>|<Texto>#<Idioma>|<Texto>" & vbCrLf
sConsulta = sConsulta & "-- FORMATO MATERIALES: cod. ultimo nivel (VALOR_TEXT) +  <Idioma>|<Texto>#<Idioma>|<Texto>" & vbCrLf
sConsulta = sConsulta & "------------------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "IF @TIPO_CAMPO_GS = 101 OR @TIPO_CAMPO_GS = 102 OR @TIPO_CAMPO_GS = 103 OR @TIPO_CAMPO_GS = 105 OR @TIPO_CAMPO_GS = 109 OR @TIPO_CAMPO_GS = 107 OR @TIPO_CAMPO_GS = 108" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+ N'_COD,'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @CAMPOTRATADO = 1" & vbCrLf
sConsulta = sConsulta & "-- Materiales" & vbCrLf
sConsulta = sConsulta & "IF @TIPO_CAMPO_GS = 103" & vbCrLf
sConsulta = sConsulta & "                                  BEGIN" & vbCrLf
sConsulta = sConsulta & "                                       IF @MOSTRARTODOSNIVELES=0" & vbCrLf
sConsulta = sConsulta & "                                       BEGIN" & vbCrLf
sConsulta = sConsulta & "                                           IF @VALOR_TEXT IS NOT NULL-- Al hacer Len de valor_text me est? quitando los espacios en blanco de la derecha" & vbCrLf
sConsulta = sConsulta & "                                              IF LEN(@VALOR_TEXT + '#')-1 = @LEN_GMN1 + @LEN_GMN2 + @LEN_GMN3 + @LEN_GMN4" & vbCrLf
sConsulta = sConsulta & "                                                  SET @SQL = @SQL + N'N''' + RTRIM(RIGHT(@VALOR_TEXT,@LEN_GMN4)) + N' - '" & vbCrLf
sConsulta = sConsulta & "                                              ELSE" & vbCrLf
sConsulta = sConsulta & "                                                  IF LEN(@VALOR_TEXT + '#')-1 = @LEN_GMN1 + @LEN_GMN2 + @LEN_GMN3" & vbCrLf
sConsulta = sConsulta & "                                                      SET @SQL = @SQL + N'N''' + RTRIM(RIGHT(@VALOR_TEXT,@LEN_GMN3)) + N' - '" & vbCrLf
sConsulta = sConsulta & "                                                  ELSE" & vbCrLf
sConsulta = sConsulta & "                                                      IF LEN(@VALOR_TEXT + '#')-1 = @LEN_GMN1 + @LEN_GMN2" & vbCrLf
sConsulta = sConsulta & "                                                          SET @SQL = @SQL + N'N''' + RTRIM(RIGHT(@VALOR_TEXT,@LEN_GMN2)) + N' - '" & vbCrLf
sConsulta = sConsulta & "                                                      ELSE" & vbCrLf
sConsulta = sConsulta & "                                                          SET @SQL = @SQL + N'N''' + RTRIM(RIGHT(@VALOR_TEXT,@LEN_GMN1)) + N' - '" & vbCrLf
sConsulta = sConsulta & "                                       END" & vbCrLf
sConsulta = sConsulta & "                                       ELSE" & vbCrLf
sConsulta = sConsulta & "                                       BEGIN" & vbCrLf
sConsulta = sConsulta & "                                           SET @GMN1 = SUBSTRING(@VALOR_TEXT, 1, @LEN_GMN1)" & vbCrLf
sConsulta = sConsulta & "                                           SET @GMN2 = SUBSTRING(@VALOR_TEXT,@LEN_GMN1+1, @LEN_GMN2)" & vbCrLf
sConsulta = sConsulta & "                                           SET @GMN3 = SUBSTRING(@VALOR_TEXT,@LEN_GMN1+@LEN_GMN2+1, @LEN_GMN3)" & vbCrLf
sConsulta = sConsulta & "                                           SET @GMN4 = SUBSTRING(@VALOR_TEXT,@LEN_GMN1+@LEN_GMN2+@LEN_GMN3+1, @LEN_GMN4)                                           " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                                           -- Al hacer Len de valor_text me est? quitando los espacios en blanco de la derecha" & vbCrLf
sConsulta = sConsulta & "                                           IF LEN(@VALOR_TEXT + '#')-1 = @LEN_GMN1 + @LEN_GMN2 + @LEN_GMN3 + @LEN_GMN4" & vbCrLf
sConsulta = sConsulta & "                                               SET @SQL = @SQL + N'N''' + @GMN1 + N' - ' + @GMN2 + N' - ' + @GMN3 + N' - ' + @GMN4 + N' - '" & vbCrLf
sConsulta = sConsulta & "                                           ELSE" & vbCrLf
sConsulta = sConsulta & "                                               IF LEN(@VALOR_TEXT + '#')-1 = @LEN_GMN1 + @LEN_GMN2 + @LEN_GMN3" & vbCrLf
sConsulta = sConsulta & "                                                   SET @SQL = @SQL + N'N''' + @GMN1 + N' - ' + @GMN2 + N' - ' + @GMN3 + N' - '" & vbCrLf
sConsulta = sConsulta & "                                               ELSE" & vbCrLf
sConsulta = sConsulta & "                                                   IF LEN(@VALOR_TEXT + '#')-1 = @LEN_GMN1 + @LEN_GMN2" & vbCrLf
sConsulta = sConsulta & "                                                       SET @SQL = @SQL + N'N''' + @GMN1 + N' - ' + @GMN2 + N' - '" & vbCrLf
sConsulta = sConsulta & "                                                   ELSE" & vbCrLf
sConsulta = sConsulta & "                                                       SET @SQL = @SQL + N'N''' + @GMN1 + N' - '" & vbCrLf
sConsulta = sConsulta & "                                       END" & vbCrLf
sConsulta = sConsulta & "                                   END" & vbCrLf
sConsulta = sConsulta & "-- Todos" & vbCrLf
sConsulta = sConsulta & "IF RIGHT(@VALOR_TEXT_DEN,1)='#' SET @VALOR_TEXT_DEN=LEFT(@VALOR_TEXT_DEN,LEN(@VALOR_TEXT_DEN)-1)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @EMPIEZAIDIOMA=CHARINDEX(@IDIOMA + '|',@VALOR_TEXT_DEN)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF (@EMPIEZAIDIOMA) > 0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @ACABADENOMINACION = CHARINDEX('#',@VALOR_TEXT_DEN,@EMPIEZAIDIOMA)" & vbCrLf
sConsulta = sConsulta & "SET @EMPIEZADENOMINACION = CHARINDEX('|',@VALOR_TEXT_DEN,@EMPIEZAIDIOMA)+1" & vbCrLf
sConsulta = sConsulta & "IF @TIPO_CAMPO_GS <> 103" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+'N'''" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ACABADENOMINACION>0" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+SUBSTRING(@VALOR_TEXT_DEN,@EMPIEZADENOMINACION,@ACABADENOMINACION-@EMPIEZADENOMINACION) + ''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+RIGHT(@VALOR_TEXT_DEN,LEN(@VALOR_TEXT_DEN)-@EMPIEZADENOMINACION+1) + ''''" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N','+N'N'''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N',C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N'N'''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT IS NOT NULL AND @TIPO_CAMPO_GS = 103" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+N','+N'N'''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N'N'''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "-- Proveedores, Departamentos, Organizaciones de Compras, Centros" & vbCrLf
sConsulta = sConsulta & "-- FORMATO: C?digo (VALOR_TEXT) - Denominaci?n (VALOR_TEXT_DEN)" & vbCrLf
sConsulta = sConsulta & "--------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "IF @TIPO_CAMPO_GS = 11 OR @TIPO_CAMPO_GS = 100  OR @TIPO_CAMPO_GS = 122 OR @TIPO_CAMPO_GS = 123 OR @TIPO_CAMPO_GS = 124" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @CAMPOTRATADO = 1" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'N'''+@VALOR_TEXT+N' - '+@VALOR_TEXT_DEN+N''''+N','+N'N'''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'N'''+@VALOR_TEXT+N' - '+@VALOR_TEXT_DEN+N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N'N'''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "-- Centros de coste, Partidas presupuestarias, Activos" & vbCrLf
sConsulta = sConsulta & "-- FORMATO: cod. ultimo nivel (VALOR_TEXT) +  <Idioma>|<Texto>#<Idioma>|<Texto>" & vbCrLf
sConsulta = sConsulta & "--------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "IF @TIPO_CAMPO_GS = 129 OR @TIPO_CAMPO_GS = 130 OR @TIPO_CAMPO_GS = 131 OR @TIPO_CAMPO_GS=132" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @CAMPOTRATADO=1" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SEPARADOR = CHARINDEX('#',@VALOR_TEXT,1)" & vbCrLf
sConsulta = sConsulta & "WHILE @SEPARADOR > 0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @VALOR_TEXT = RIGHT(@VALOR_TEXT,LEN(@VALOR_TEXT)-@SEPARADOR)" & vbCrLf
sConsulta = sConsulta & "SET @SEPARADOR = CHARINDEX('#',@VALOR_TEXT,1)" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SEPARADOR = CHARINDEX('|',@VALOR_TEXT,1)" & vbCrLf
sConsulta = sConsulta & "WHILE @SEPARADOR > 0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @VALOR_TEXT = RIGHT(@VALOR_TEXT,LEN(@VALOR_TEXT)-@SEPARADOR)" & vbCrLf
sConsulta = sConsulta & "SET @SEPARADOR = CHARINDEX('|',@VALOR_TEXT,1)" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'N''' + @VALOR_TEXT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--Denominaci?n en el idioma" & vbCrLf
sConsulta = sConsulta & "IF RIGHT(@VALOR_TEXT_DEN,1)='#' SET @VALOR_TEXT_DEN=LEFT(@VALOR_TEXT_DEN,LEN(@VALOR_TEXT_DEN)-1)" & vbCrLf
sConsulta = sConsulta & "SET @EMPIEZAIDIOMA=CHARINDEX(@IDIOMA + '|',@VALOR_TEXT_DEN)" & vbCrLf
sConsulta = sConsulta & "IF (@EMPIEZAIDIOMA) > 0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @ACABADENOMINACION = CHARINDEX('#',@VALOR_TEXT_DEN,@EMPIEZAIDIOMA)" & vbCrLf
sConsulta = sConsulta & "SET @EMPIEZADENOMINACION = CHARINDEX('|',@VALOR_TEXT_DEN,@EMPIEZAIDIOMA)+1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ACABADENOMINACION>0" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ' - ' + SUBSTRING(@VALOR_TEXT_DEN,@EMPIEZADENOMINACION,@ACABADENOMINACION-@EMPIEZADENOMINACION)" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ' - ' + RIGHT(@VALOR_TEXT_DEN,LEN(@VALOR_TEXT_DEN)-@EMPIEZADENOMINACION+1)" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+N','+N'N'''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N'N'''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "-- Personas, Comprador" & vbCrLf
sConsulta = sConsulta & "---------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "IF @TIPO_CAMPO_GS = 115 OR @TIPO_CAMPO_GS=144" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @CAMPOTRATADO = 1" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT_DEN IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'N'''+@VALOR_TEXT_DEN+N''''+N','+N'N'''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'N'''+@VALOR_TEXT_DEN+N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N'N'''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "-- Almacen / Empresa" & vbCrLf
sConsulta = sConsulta & "---------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "IF @TIPO_CAMPO_GS = 125 OR @TIPO_CAMPO_GS = 149" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @CAMPOTRATADO = 1" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT_DEN IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'N'''+@VALOR_TEXT_DEN+N''''+N','+N''''+CONVERT(NVARCHAR,@VALOR_NUM)+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'N'''+@VALOR_TEXT_DEN+N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N''''+CONVERT(NVARCHAR,@VALOR_NUM)+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "------------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "-- Cod. Art?culo (viejo), Cod. Art?culo (nuevo), Denominaci?n del art?culo y Unidades Organizativas" & vbCrLf
sConsulta = sConsulta & "------------------------------------------------------------------------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "IF @TIPO_CAMPO_GS = 104  OR @TIPO_CAMPO_GS = 118 OR @TIPO_CAMPO_GS = 119 OR @TIPO_CAMPO_GS = 121" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @CAMPOTRATADO = 1" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'N'''+@VALOR_TEXT+N''''+N','+N'N'''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'N'''+@VALOR_TEXT +N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N'N'''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "---------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "-- Presupuestos" & vbCrLf
sConsulta = sConsulta & "-- FORMATO: Ej 2005 - PC-002  (50.00%); 2005 - PC-001 (50.00%);" & vbCrLf
sConsulta = sConsulta & "--  VALOR_TEXT_DEN: 2005 - PC-002#2005 - PC-001" & vbCrLf
sConsulta = sConsulta & "--  VALOR_TEXT: 1_11_0.5#1_10_0.5" & vbCrLf
sConsulta = sConsulta & "---------------------------------------------------------------------------------------------" & vbCrLf
sConsulta = sConsulta & "IF @TIPO_CAMPO_GS = 110  OR @TIPO_CAMPO_GS = 111 OR @TIPO_CAMPO_GS = 112 OR @TIPO_CAMPO_GS = 113" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @CAMPOTRATADO = 1" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @PRESUPUESTOS = ''" & vbCrLf
sConsulta = sConsulta & "SET @SEPARADOR_DEN = CHARINDEX('#',@VALOR_TEXT_DEN)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "WHILE @SEPARADOR_DEN > 0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @PRESUPUESTOS = @PRESUPUESTOS + SUBSTRING(@VALOR_TEXT_DEN,1,@SEPARADOR_DEN-1)" & vbCrLf
sConsulta = sConsulta & "SET @VALOR_TEXT_DEN = RIGHT(@VALOR_TEXT_DEN,LEN(@VALOR_TEXT_DEN)-@SEPARADOR_DEN)" & vbCrLf
sConsulta = sConsulta & "SET @PORCENTAJE = SUBSTRING(@VALOR_TEXT,1,CHARINDEX('#',@VALOR_TEXT)-1)" & vbCrLf
sConsulta = sConsulta & "SET @SEPARADOR_PORCENTAJE = 1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "WHILE @SEPARADOR_PORCENTAJE > 0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @PORCENTAJE = RIGHT(@PORCENTAJE,LEN(@PORCENTAJE)-@SEPARADOR_PORCENTAJE)" & vbCrLf
sConsulta = sConsulta & "SET @SEPARADOR_PORCENTAJE = CHARINDEX('_',@PORCENTAJE,@SEPARADOR_PORCENTAJE+1)" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "SET @PRESUPUESTOS = @PRESUPUESTOS + '(' + CONVERT(VARCHAR,CONVERT(DECIMAL(3,2),@PORCENTAJE)*100) + '%); '" & vbCrLf
sConsulta = sConsulta & "SET @VALOR_TEXT = RIGHT(@VALOR_TEXT,LEN(@VALOR_TEXT)-CHARINDEX('#',@VALOR_TEXT))" & vbCrLf
sConsulta = sConsulta & "SET @SEPARADOR_DEN = CHARINDEX('#',@VALOR_TEXT_DEN)" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "SET @PORCENTAJE = @VALOR_TEXT" & vbCrLf
sConsulta = sConsulta & "SET @SEPARADOR_PORCENTAJE = 1" & vbCrLf
sConsulta = sConsulta & "WHILE @SEPARADOR_PORCENTAJE > 0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @PORCENTAJE = RIGHT(@PORCENTAJE,LEN(@PORCENTAJE)-@SEPARADOR_PORCENTAJE)" & vbCrLf
sConsulta = sConsulta & "SET @SEPARADOR_PORCENTAJE = CHARINDEX('_',@PORCENTAJE,@SEPARADOR_PORCENTAJE+1)" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @PRESUPUESTOS = @PRESUPUESTOS + @VALOR_TEXT_DEN + '(' + CAST(CONVERT(FLOAT,@PORCENTAJE)*100 AS VARCHAR) + '%);'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @PRESUPUESTOS <> ''" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'N''' + @PRESUPUESTOS + N'''' + N',' + N'N''' + @VALOR_TEXT + N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + N'N''' + @PRESUPUESTOS + N''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD='+N'N'''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL,'+N'C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END                            " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CAMPOTRATADO = 0 -- Resto" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @SUBTIPO = 3 -- tipo fecha" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_FEC IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + '''' + @VALOR_FEC + ''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @SUBTIPO = 4 -- tipo booleano" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_BOOL IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + CONVERT(VARCHAR,@VALOR_BOOL)" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @SUBTIPO=2 -- tipo num?rico" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_NUM IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + CONVERT(VARCHAR,@VALOR_NUM)" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + 'NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE    -- tipo texto" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQLCAMPOSINSERT=@SQLCAMPOSINSERT+N'C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+N'_COD,'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @VALOR_TEXT IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'N'''+@VALOR_TEXT+N''''+N','+N'N'''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+'N'''+@VALOR_TEXT+''''+N',C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+ N'_COD='+N'N'''+@VALOR_TEXT+N''''" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "IF @INSERT=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,NULL'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL=@SQL+N'NULL,C_'+CONVERT(NVARCHAR,ISNULL(@CAMPO_PADRE_DESGLOSE,0))+N'_'+CONVERT(NVARCHAR,ISNULL(@TIPO_CAMPO_GS,0))+N'_'+CONVERT(NVARCHAR,@CAMPO_ORIGEN)+ N'_COD=NULL'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL = @SQL + ','" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curSOLICITUDES_CAMPOS_DESGLOSE INTO @LINEA,@CAMPO_ORIGEN,@SUBTIPO,@TIPO_CAMPO_GS," & vbCrLf
sConsulta = sConsulta & "@VALOR_TEXT,@VALOR_NUM,@VALOR_FEC,@VALOR_BOOL,@VALOR_TEXT_COD,@VALOR_TEXT_DEN,@INTRO,@ID_ATRIB_GS," & vbCrLf
sConsulta = sConsulta & "@TABLA_EXTERNA,@CAMPO_DEF,@CAMPO_PADRE_DESGLOSE" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "CLOSE curSOLICITUDES_CAMPOS_DESGLOSE" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curSOLICITUDES_CAMPOS_DESGLOSE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @EJECUTARSQL=1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @INSERT = 1" & vbCrLf
sConsulta = sConsulta & "SET @SQL = LEFT(@SQLCAMPOSINSERT,LEN(@SQLCAMPOSINSERT)-1) + ') ' + LEFT(@SQL,LEN(@SQL)-1) + ')'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @SQL = LEFT(@SQL,LEN(@SQL)-1) + ' WHERE FORM_INSTANCIA=@INSTANCIA_L AND DESGLOSE=@CAMPO_PADRE_DESGLOSE AND LINEA=@LINEA '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL=N'IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = ''dbo'' AND  TABLE_NAME='''+@IDIOMA+N'_'+@TABLA+N'_DESGLOSE'')) BEGIN '+@SQL+N' END'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, N'@INSTANCIA_L INT,@CAMPO_PADRE_DESGLOSE INT,@LINEA INT', @INSTANCIA_L=@INSTANCIA_L,@CAMPO_PADRE_DESGLOSE=@CAMPO_PADRE_DESGLOSE,@LINEA=@LINEA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "              DELETE FROM #T_VALORES_NUM" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @INSERT=@INSERT_ORIGINAL" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curIDIOMAS INTO @IDIOMA" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "CLOSE curIDIOMAS" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curIDIOMAS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DROP TABLE #G1" & vbCrLf
sConsulta = sConsulta & "DROP TABLE #T_VALORES_NUM" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion3_3_3507_A3_3_3508() As Boolean
    Dim bTransaccionEnCurso As Boolean
    Dim oADOConnection As ADODB.Connection
    Dim sConsulta As String

    On Error GoTo Error
    sConsulta = "BEGIN TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = True
    ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

    '-------------------------------------------------------------------------------------------
    frmProgreso.lblDetalle = "Actualiza Tablas"
    frmProgreso.lblDetalle.Refresh
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    frmProgreso.ProgressBar1.Refresh
    V_3_5_Tablas_008

    sConsulta = "UPDATE VERSION SET NUM ='3.00.35.008'" & ", FECHA=CONVERT(datetime,'" & Format(Now, "mm-dd-yyyy hh:mm:ss") & "',110), PRODVER='3.5.2'"
    ExecuteSQL gRDOCon, sConsulta

    sConsulta = "COMMIT TRANSACTION"
    ExecuteSQL gRDOCon, sConsulta
    bTransaccionEnCurso = False

    CodigoDeActualizacion3_3_3507_A3_3_3508 = True
    Exit Function

Error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If

    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    CodigoDeActualizacion3_3_3507_A3_3_3508 = False
End Function

Private Sub V_3_5_Tablas_008()
    Dim sConsulta As String
sConsulta = "CREATE TABLE [dbo].[VAR_PROVE_UNQA_PUNT_ANT](" & vbCrLf
sConsulta = sConsulta & "   [PROVE] [varchar](20) NOT NULL," & vbCrLf
sConsulta = sConsulta & "   [UNQA] [int] NOT NULL," & vbCrLf
sConsulta = sConsulta & "   [VARCAL] [int] NOT NULL," & vbCrLf
sConsulta = sConsulta & "   [NIVEL] [int] NOT NULL," & vbCrLf
sConsulta = sConsulta & "   [PUNT] [float] NOT NULL," & vbCrLf
sConsulta = sConsulta & "   [CAL] [int] NULL," & vbCrLf
sConsulta = sConsulta & "   [FECACT] [datetime] NULL," & vbCrLf
sConsulta = sConsulta & "CONSTRAINT [PK_VAR_PROVE_UNQA_PUNT_ANT] PRIMARY KEY CLUSTERED" & vbCrLf
sConsulta = sConsulta & "(" & vbCrLf
sConsulta = sConsulta & "   [PROVE] ASC," & vbCrLf
sConsulta = sConsulta & "   [UNQA] ASC," & vbCrLf
sConsulta = sConsulta & "   [VARCAL] ASC," & vbCrLf
sConsulta = sConsulta & "   [NIVEL] ASC" & vbCrLf
sConsulta = sConsulta & ")WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]" & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.QueryTimeout = 7200
ExecuteSQL gRDOCon, sConsulta

End Sub

