VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomctl.ocx"
Begin VB.Form frmESTRORG 
   Caption         =   "Organigrama"
   ClientHeight    =   4950
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   8070
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmESTRORG.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   4950
   ScaleMode       =   0  'User
   ScaleWidth      =   8070
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdCancelar 
      BackColor       =   &H00C9D2D6&
      Caption         =   "Cancelar"
      CausesValidation=   0   'False
      Height          =   315
      Left            =   135
      TabIndex        =   3
      Top             =   4545
      Width           =   1005
   End
   Begin VB.CommandButton cmdSiguiente 
      Caption         =   "Siguiente >>"
      Height          =   315
      Left            =   6390
      TabIndex        =   1
      Top             =   4545
      Width           =   1530
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   0
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   11
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmESTRORG.frx":014A
            Key             =   "Departamento"
            Object.Tag             =   "Departamento"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmESTRORG.frx":01FA
            Key             =   "PerBaja"
            Object.Tag             =   "PerBaja"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmESTRORG.frx":051E
            Key             =   "PerCestaBaja"
            Object.Tag             =   "PerCestaBaja"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmESTRORG.frx":0842
            Key             =   "UON1"
            Object.Tag             =   "UON1"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmESTRORG.frx":08F2
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmESTRORG.frx":09B3
            Key             =   "UON3"
            Object.Tag             =   "UON3"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmESTRORG.frx":0D46
            Key             =   "UON2"
            Object.Tag             =   "UON2"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmESTRORG.frx":0DF6
            Key             =   "UON0"
            Object.Tag             =   "UON0"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmESTRORG.frx":0E95
            Key             =   "Persona"
            Object.Tag             =   "Persona"
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmESTRORG.frx":0F00
            Key             =   "PerCesta"
            Object.Tag             =   "PerCesta"
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmESTRORG.frx":0F8B
            Key             =   "SELLO"
            Object.Tag             =   "SELLO"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.TreeView tvwestrorg 
      Height          =   3930
      Left            =   105
      TabIndex        =   0
      Top             =   525
      Width           =   7785
      _ExtentX        =   13732
      _ExtentY        =   6932
      _Version        =   393217
      HideSelection   =   0   'False
      LabelEdit       =   1
      Style           =   7
      HotTracking     =   -1  'True
      ImageList       =   "ImageList1"
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label Label1 
      Caption         =   "Seleccione una unidad organizativa a la que se asignarán los pedidos ya emitidos."
      Height          =   240
      Left            =   180
      TabIndex        =   2
      Top             =   150
      Width           =   7815
   End
End
Attribute VB_Name = "frmESTRORG"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit

Public g_vUON1 As Variant
Public g_vUON2 As Variant
Public g_vUON3 As Variant
Public oUON1Seleccionada As CUnidadOrgNivel1
Public oUON2Seleccionada As CUnidadOrgNivel2
Public oUON3Seleccionada As CUnidadOrgNivel3

Public Accion As AccionesSummit

'************** Metodos privados de la clase *********

Private Sub GenerarEstructuraOrg(ByVal bOrdenadoPorDen As Boolean)

Dim scod1 As String
Dim scod2 As String
Dim scod3 As String
Dim scod4 As String

' Unidades organizativas
Dim oUnidadesOrgN1 As CUnidadesOrgNivel1
Dim oUnidadesOrgN2 As CUnidadesOrgNivel2
Dim oUnidadesOrgN3 As CUnidadesOrgNivel3

Dim oUON1 As CUnidadOrgNivel1
Dim oUON2 As CUnidadOrgNivel2
Dim oUON3 As CUnidadOrgNivel3


' Otras
Dim nodx As Node
Dim sKeyImage As String

    

    Set oUnidadesOrgN1 = oFSGSRaiz.Generar_CUnidadesOrgNivel1
    Set oUnidadesOrgN2 = oFSGSRaiz.Generar_CUnidadesOrgNivel2
    Set oUnidadesOrgN3 = oFSGSRaiz.Generar_CUnidadesOrgNivel3
     
         
        
    oUnidadesOrgN1.CargarTodasLasUnidadesOrgNivel1 , , , , , , , , , bOrdenadoPorDen, False
    oUnidadesOrgN2.CargarTodasLasUnidadesOrgNivel2 , , , , , , , , , bOrdenadoPorDen
    oUnidadesOrgN3.CargarTodasLasUnidadesOrgNivel3 , , , , , , , , , bOrdenadoPorDen
    
        
        
   '************************************************************
    'Generamos la estructura arborea
    
    ' Unidades organizativas
    gsDEN_UON0 = oFSGSRaiz.DevolverUON0
    
    Set nodx = tvwestrorg.Nodes.Add(, , "UON0", gsDEN_UON0, "UON0")
    nodx.Tag = "UON0"
    nodx.Expanded = True
        
    For Each oUON1 In oUnidadesOrgN1
        
        scod1 = oUON1.Cod & Mid$("                         ", 1, gLongitudesDeCodigos.giLongCodUON1 - Len(oUON1.Cod))
        sKeyImage = "UON1"
        
        Set nodx = tvwestrorg.Nodes.Add("UON0", tvwChild, "UON1" & scod1, CStr(oUON1.Cod) & " - " & oUON1.Den, sKeyImage)
        nodx.Tag = "UON1" & CStr(oUON1.Cod)
            
    Next
    
    For Each oUON2 In oUnidadesOrgN2
        
        scod1 = oUON2.CodUnidadOrgNivel1 & Mid$("                         ", 1, gLongitudesDeCodigos.giLongCodUON1 - Len(oUON2.CodUnidadOrgNivel1))
        scod2 = scod1 & oUON2.Cod & Mid$("                         ", 1, gLongitudesDeCodigos.giLongCodUON2 - Len(oUON2.Cod))
        sKeyImage = "UON2"
        
        Set nodx = tvwestrorg.Nodes.Add("UON1" & scod1, tvwChild, "UON2" & scod2, CStr(oUON2.Cod) & " - " & oUON2.Den, sKeyImage)
        nodx.Tag = "UON2" & CStr(oUON2.Cod)
            
    Next
    
    For Each oUON3 In oUnidadesOrgN3
        
        scod1 = oUON3.CodUnidadOrgNivel1 & Mid$("                         ", 1, gLongitudesDeCodigos.giLongCodUON1 - Len(oUON3.CodUnidadOrgNivel1))
        scod2 = scod1 & oUON3.CodUnidadOrgNivel2 & Mid$("                         ", 1, gLongitudesDeCodigos.giLongCodUON2 - Len(oUON3.CodUnidadOrgNivel2))
        scod3 = scod2 & oUON3.Cod & Mid$("                         ", 1, gLongitudesDeCodigos.giLongCodUON3 - Len(oUON3.Cod))
        sKeyImage = "UON3"
        Set nodx = tvwestrorg.Nodes.Add("UON2" & scod2, tvwChild, "UON3" & scod3, CStr(oUON3.Cod) & " - " & oUON3.Den, sKeyImage)
        nodx.Tag = "UON3" & CStr(oUON3.Cod)
            
    Next
    
    
    Set nodx = Nothing
    
    Set oUON1 = Nothing
    Set oUON2 = Nothing
    Set oUON3 = Nothing
    
    Set oUnidadesOrgN1 = Nothing
    Set oUnidadesOrgN2 = Nothing
    Set oUnidadesOrgN3 = Nothing
    
    
End Sub


Private Sub cmdCancelar_Click()
bCancelado = True
Unload Me

End Sub

Private Sub cmdSiguiente_Click()

Dim nodx As Node

Screen.MousePointer = vbHourglass

Set nodx = tvwestrorg.SelectedItem

NodoSeleccionado nodx

If nodx Is Nothing Then
    Screen.MousePointer = vbNormal
    Exit Sub
End If

Select Case Left(nodx.Tag, 4)
    Case "UON1"
        Accion = ACCUON1Mod
    Case "UON2"
        Accion = ACCUON2Mod
    Case "UON3"
        Accion = ACCUON3Mod
    Case "UON0"
        Screen.MousePointer = vbNormal
        Exit Sub
End Select

Screen.MousePointer = vbNormal
frmESTRORGDetalle.Show 1

Unload Me


End Sub

Private Sub Form_Load()
Dim nodx As Node  ' Declare an object variable for the Node.
Dim i As Integer  ' Declare a variable for use as a counter.

Me.Height = 5355
Me.Width = 8190
    



' Generar la estructura de la organizacion en el treeview
GenerarEstructuraOrg (False)

tvwestrorg.Nodes.Item("UON0").Selected = True

  
DoEvents

End Sub


Private Sub NodoSeleccionado(ByRef nodx As Node)

If Not nodx Is Nothing Then
    
    Set oUON1Seleccionada = Nothing
    Set oUON2Seleccionada = Nothing
    Set oUON3Seleccionada = Nothing
    
    Select Case Left(nodx.Tag, 4)
    
    
    Case "UON1"
            
            Set oUON1Seleccionada = oFSGSRaiz.Generar_CUnidadOrgNivel1
            oUON1Seleccionada.Cod = DevolverCod(nodx)
            g_vUON1 = DevolverCod(nodx)
            g_vUON2 = Null
            g_vUON3 = Null
            
    Case "UON2"
            
            Set oUON2Seleccionada = oFSGSRaiz.Generar_CUnidadOrgNivel2
            oUON2Seleccionada.Cod = DevolverCod(nodx)
            oUON2Seleccionada.CodUnidadOrgNivel1 = DevolverCod(nodx.Parent)
            g_vUON2 = DevolverCod(nodx)
            g_vUON1 = DevolverCod(nodx.Parent)
            g_vUON3 = Null
            
    Case "UON3"
            
            Set oUON3Seleccionada = oFSGSRaiz.Generar_CUnidadOrgNivel3
            oUON3Seleccionada.Cod = DevolverCod(nodx)
            oUON3Seleccionada.CodUnidadOrgNivel2 = DevolverCod(nodx.Parent)
            oUON3Seleccionada.CodUnidadOrgNivel1 = DevolverCod(nodx.Parent.Parent)
            g_vUON3 = DevolverCod(nodx)
            g_vUON2 = DevolverCod(nodx.Parent)
            g_vUON1 = DevolverCod(nodx.Parent.Parent)
           
    
    End Select
End If

End Sub


Public Function DevolverCod(ByVal Node As MSComctlLib.Node) As Variant

Select Case Left(Node.Tag, 4)
    
Case "UON1"
        DevolverCod = Right(Node.Tag, Len(Node.Tag) - 4)
    
Case "UON2"
        DevolverCod = Right(Node.Tag, Len(Node.Tag) - 4)
    
Case "UON3"
        DevolverCod = Right(Node.Tag, Len(Node.Tag) - 4)
    
    
End Select

End Function

