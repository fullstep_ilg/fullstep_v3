Attribute VB_Name = "basCaducidad"
Option Explicit
Private m_oConexion As CConexion

Property Set Conexion(ByVal vData As CConexion)

    Set m_oConexion = vData
End Property

Property Get Conexion() As CConexion
    Set Conexion = m_oConexion
End Property

Public Function CrearJobCaducidad() As Boolean
Dim adocommAdo As ADODB.Command
Dim adoparamPar As ADODB.Parameter

Dim sFecha As String
Dim lFecha As Long
Dim StartYear, StartMonth, StartDay As String
    
    On Error GoTo Ir_Error:
 
    
    Set adocommAdo = New ADODB.Command
    Set adocommAdo.ActiveConnection = moConexion.ADOCon
    adocommAdo.CommandType = adCmdStoredProc
    adocommAdo.CommandText = "SP_JOB_CREARJOBCADUCIDAD"
    
    
''    '****** Si no existe el job lo crea **********************
    
    'Nombre del Job
    Set adoparamPar = adocommAdo.CreateParameter("NOMBRE_JOB", adVarChar, adParamInput, 50, UCase("CADU_" & gBD))
    adocommAdo.Parameters.Append adoparamPar

    'la planificaci�n : nombre,frecuencia,hora y fecha de comienzo...
    Set adoparamPar = adocommAdo.CreateParameter("NOMBRE_SCH", adVarChar, adParamInput, 50, "S_CADUCIDAD")
    adocommAdo.Parameters.Append adoparamPar

    Set adoparamPar = adocommAdo.CreateParameter("TIPO", adInteger, adParamInput, , 4)
    adocommAdo.Parameters.Append adoparamPar

    Set adoparamPar = adocommAdo.CreateParameter("INTERVALO", adInteger, adParamInput, , 1)
    adocommAdo.Parameters.Append adoparamPar

    StartYear = DatePart("yyyy", Date)
    StartMonth = DatePart("m", Date)
    StartDay = DatePart("d", Date)
    If Len(StartMonth) < 2 Then StartMonth = "0" & StartMonth
    If Len(StartDay) < 2 Then StartDay = "0" & StartDay

    sFecha = StartYear & StartMonth & StartDay
    lFecha = CLng(sFecha)
    Set adoparamPar = adocommAdo.CreateParameter("FECINI", adInteger, adParamInput, , lFecha)
    adocommAdo.Parameters.Append adoparamPar

    sFecha = "235958"
    lFecha = CLng(sFecha)
    Set adoparamPar = adocommAdo.CreateParameter("TIMEINI", adInteger, adParamInput, , lFecha)
    adocommAdo.Parameters.Append adoparamPar

    Set adoparamPar = adocommAdo.CreateParameter("NOMBRE_STEP", adVarChar, adParamInput, 50, "STEP1")
    adocommAdo.Parameters.Append adoparamPar
    Set adoparamPar = adocommAdo.CreateParameter("NOMBRE_PROC", adVarChar, adParamInput, 50, "SP_CADUCIDAD")
    adocommAdo.Parameters.Append adoparamPar
    
    adocommAdo.Execute
                    
    CrearJobCaducidad = True

    Exit Function

Ir_Error:
    CrearJobCaducidad = False

End Function

Public Function CrearJobAhorros() As Boolean
Dim adocommAdo As ADODB.Command
Dim adoparamPar As ADODB.Parameter
Dim sFecha As String
Dim lFecha As Long
Dim StartYear, StartMonth, StartDay As String
    
    On Error GoTo Ir_Error:
    
    Set adocommAdo = New ADODB.Command
    Set adocommAdo.ActiveConnection = moConexion.ADOCon
    adocommAdo.CommandType = adCmdStoredProc
    adocommAdo.CommandText = "SP_JOB_CREARJOBAHORROS"
    
''    '****** Si no existe el job lo crea **********************
    'Nombre del Job
    Set adoparamPar = adocommAdo.CreateParameter("NOMBRE_JOB", adVarChar, adParamInput, 50, UCase("ACT_AHORROS_" & gBD))
    adocommAdo.Parameters.Append adoparamPar

    'la planificaci�n : nombre,frecuencia,hora y fecha de comienzo...
    Set adoparamPar = adocommAdo.CreateParameter("NOMBRE_SCH", adVarChar, adParamInput, 50, "ACT_AHORROS_SCH1")
    adocommAdo.Parameters.Append adoparamPar

    Set adoparamPar = adocommAdo.CreateParameter("TIPO", adInteger, adParamInput, , 4)
    adocommAdo.Parameters.Append adoparamPar

    Set adoparamPar = adocommAdo.CreateParameter("INTERVALO", adInteger, adParamInput, , 1)
    adocommAdo.Parameters.Append adoparamPar

    StartYear = DatePart("yyyy", Date)
    StartMonth = DatePart("m", Date)
    StartDay = DatePart("d", Date)
    If Len(StartMonth) < 2 Then StartMonth = "0" & StartMonth
    If Len(StartDay) < 2 Then StartDay = "0" & StartDay

    sFecha = StartYear & StartMonth & StartDay
    lFecha = CLng(sFecha)
    Set adoparamPar = adocommAdo.CreateParameter("FECINI", adInteger, adParamInput, , lFecha)
    adocommAdo.Parameters.Append adoparamPar

    sFecha = "40000"
    lFecha = CLng(sFecha)
    Set adoparamPar = adocommAdo.CreateParameter("TIMEINI", adInteger, adParamInput, , lFecha)
    adocommAdo.Parameters.Append adoparamPar


    Set adoparamPar = adocommAdo.CreateParameter("NOMBRE_STEP", adVarChar, adParamInput, 50, "ACT_AHORROS_STEP1")
    adocommAdo.Parameters.Append adoparamPar
    Set adoparamPar = adocommAdo.CreateParameter("NOMBRE_PROC", adVarChar, adParamInput, 50, "SP_AR_CALCULAR")
    adocommAdo.Parameters.Append adoparamPar

    Set adoparamPar = adocommAdo.CreateParameter("NOMBRE_OWN", adVarChar, adParamInput, 50, "summitapp")
    adocommAdo.Parameters.Append adoparamPar

    adocommAdo.Execute

    CrearJobAhorros = True

    Exit Function

Ir_Error:
    CrearJobAhorros = False

End Function

Public Function CrearJobCertificados() As Boolean
Dim adocommAdo As ADODB.Command
Dim adoparamPar As ADODB.Parameter
Dim sFecha As String
Dim lFecha As Long
Dim StartYear, StartMonth, StartDay As String

On Error GoTo Ir_Error:


    
    Set adocommAdo = New ADODB.Command
    Set adocommAdo.ActiveConnection = moConexion.ADOCon
    adocommAdo.CommandType = adCmdStoredProc
    adocommAdo.CommandText = "SP_JOB_CREARJOBCERTIFICADOS"
    
    
''    '****** Si no existe el job lo crea **********************
    
    'Nombre del Job
    Set adoparamPar = adocommAdo.CreateParameter("NOMBRE_JOB", adVarChar, adParamInput, 50, UCase("CERTIF_DESPUB_" & gBD))
    adocommAdo.Parameters.Append adoparamPar

    'la planificaci�n : nombre,frecuencia,hora y fecha de comienzo...
    Set adoparamPar = adocommAdo.CreateParameter("NOMBRE_SCH", adVarChar, adParamInput, 50, "S_CERTIF_DESPUB")
    adocommAdo.Parameters.Append adoparamPar

    Set adoparamPar = adocommAdo.CreateParameter("TIPO", adInteger, adParamInput, , 4)
    adocommAdo.Parameters.Append adoparamPar

    Set adoparamPar = adocommAdo.CreateParameter("INTERVALO", adInteger, adParamInput, , 1)
    adocommAdo.Parameters.Append adoparamPar

    StartYear = DatePart("yyyy", Date)
    StartMonth = DatePart("m", Date)
    StartDay = DatePart("d", Date)
    If Len(StartMonth) < 2 Then StartMonth = "0" & StartMonth
    If Len(StartDay) < 2 Then StartDay = "0" & StartDay

    sFecha = StartYear & StartMonth & StartDay
    lFecha = CLng(sFecha)
    Set adoparamPar = adocommAdo.CreateParameter("FECINI", adInteger, adParamInput, , lFecha)
    adocommAdo.Parameters.Append adoparamPar

    sFecha = "235900"
    lFecha = CLng(sFecha)
    Set adoparamPar = adocommAdo.CreateParameter("TIMEINI", adInteger, adParamInput, , lFecha)
    adocommAdo.Parameters.Append adoparamPar

    Set adoparamPar = adocommAdo.CreateParameter("NOMBRE_STEP", adVarChar, adParamInput, 50, "STEP1")
    adocommAdo.Parameters.Append adoparamPar
    Set adoparamPar = adocommAdo.CreateParameter("NOMBRE_PROC", adVarChar, adParamInput, 50, "FSQA_COMPROBAR_DESPUBLICACION")
    adocommAdo.Parameters.Append adoparamPar

    adocommAdo.Execute
    CrearJobCertificados = True

    Exit Function

Ir_Error:
    CrearJobCertificados = False

End Function


Public Function CrearJobTruncarLog() As Boolean
Dim adocommAdo As ADODB.Command
Dim adoparamPar As ADODB.Parameter

Dim sFecha As String
Dim lFecha As Long
Dim StartYear, StartMonth, StartDay As String
Dim sConsulta As String
    
    On Error GoTo Ir_Error:

    'Crea el proc.almacenado FS_TRUNCAR_LOG_strBD (si existe primero lo borra)
    sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[FS_TRUNCAR_LOG]') and OBJECTPROPERTY(id, N'IsProcedure') = 1) drop procedure [dbo].[FS_TRUNCAR_LOG]"
    gRDOCon.Execute sConsulta, rdExecDirect
    sConsulta = CrearStoredProcedureFS_TRUNCAR_LOG(gBD)
    gRDOCon.Execute sConsulta, rdExecDirect
    
   
    Set adocommAdo = New ADODB.Command
    'Set adocommAdo.ActiveConnection = oADOConnection
    Set adocommAdo.ActiveConnection = moConexion.ADOCon
    adocommAdo.CommandType = adCmdStoredProc
    adocommAdo.CommandText = "SP_JOB_CREARJOBTRUNCARLOG"
    
    
''    '****** Si no existe el job lo crea **********************
    'Nombre del Job
    Set adoparamPar = adocommAdo.CreateParameter("NOMBRE_JOB", adVarChar, adParamInput, 50, UCase("TRUNCAR_LOG_" & gBD))
    adocommAdo.Parameters.Append adoparamPar

    'la planificaci�n : nombre,frecuencia,hora y fecha de comienzo...
    Set adoparamPar = adocommAdo.CreateParameter("@NOMBRE_SCH", adVarChar, adParamInput, 50, "S_TRUNCAR_LOG")
    adocommAdo.Parameters.Append adoparamPar

    Set adoparamPar = adocommAdo.CreateParameter("TIPO", adInteger, adParamInput, , 8) 'SQLDMOFreq_Weekly
    adocommAdo.Parameters.Append adoparamPar

    Set adoparamPar = adocommAdo.CreateParameter("INTERVALO", adInteger, adParamInput, , 1) 'SQLDMO_WEEKDAY_TYPE.SQLDMOWeek_Sunday

    adocommAdo.Parameters.Append adoparamPar

    StartYear = DatePart("yyyy", Date)
    StartMonth = DatePart("m", Date)
    StartDay = DatePart("d", Date)
    If Len(StartMonth) < 2 Then StartMonth = "0" & StartMonth
    If Len(StartDay) < 2 Then StartDay = "0" & StartDay

    sFecha = StartYear & StartMonth & StartDay
    lFecha = CLng(sFecha)
    Set adoparamPar = adocommAdo.CreateParameter("FECINI", adInteger, adParamInput, , lFecha)
    adocommAdo.Parameters.Append adoparamPar

    sFecha = "120000"
    lFecha = CLng(sFecha)
    Set adoparamPar = adocommAdo.CreateParameter("TIMEINI", adInteger, adParamInput, , lFecha)
    adocommAdo.Parameters.Append adoparamPar

    Set adoparamPar = adocommAdo.CreateParameter("NOMBRE_STEP", adVarChar, adParamInput, 50, "STEP1")
    adocommAdo.Parameters.Append adoparamPar
    Set adoparamPar = adocommAdo.CreateParameter("NOMBRE_PROC", adVarChar, adParamInput, 50, "FS_TRUNCAR_LOG")
    adocommAdo.Parameters.Append adoparamPar

    Set adoparamPar = adocommAdo.CreateParameter("FRECRECFACT", adInteger, adParamInput, , 1)
    adocommAdo.Parameters.Append adoparamPar


    adocommAdo.Execute

    CrearJobTruncarLog = True

    Exit Function

Ir_Error:
    CrearJobTruncarLog = False

End Function


Private Function CrearStoredProcedureFS_TRUNCAR_LOG(ByVal strBase As String) As String
    Dim sConsulta As String
    
    sConsulta = "CREATE PROCEDURE FS_TRUNCAR_LOG AS" & vbCrLf
    sConsulta = sConsulta & "" & vbCrLf
    sConsulta = sConsulta & "BACKUP LOG " & strBase & " WITH TRUNCATE_ONLY" & vbCrLf
    sConsulta = sConsulta & "DBCC SHRINKDATABASE (" & strBase & ", 25)" & vbCrLf

    CrearStoredProcedureFS_TRUNCAR_LOG = sConsulta
End Function


Public Function CrearJobEliminarInstIncomp() As Boolean

Dim adocommAdo As ADODB.Command
Dim adoparamPar As ADODB.Parameter

Dim sFecha As String
Dim lFecha As Long
Dim StartYear, StartMonth, StartDay As String
    
    On Error GoTo Ir_Error:

    
    Set adocommAdo = New ADODB.Command
    Set adocommAdo.ActiveConnection = moConexion.ADOCon
    adocommAdo.CommandType = adCmdStoredProc
    adocommAdo.CommandText = "SP_JOB_CREARJOBELIMINARINSTINCOMP"
    
    
    
''    '****** Si no existe el job lo crea **********************
    
    'Nombre del Job
    Set adoparamPar = adocommAdo.CreateParameter("NOMBRE_JOB", adVarChar, adParamInput, 50, UCase("ELIM_INST_NOCREADAS_" & gBD))
    adocommAdo.Parameters.Append adoparamPar

    'la planificaci�n : nombre,frecuencia,hora y fecha de comienzo...
    Set adoparamPar = adocommAdo.CreateParameter("NOMBRE_SCH", adVarChar, adParamInput, 50, "SCHED1")
    adocommAdo.Parameters.Append adoparamPar

    Set adoparamPar = adocommAdo.CreateParameter("TIPO", adInteger, adParamInput, , 4)
    adocommAdo.Parameters.Append adoparamPar

    Set adoparamPar = adocommAdo.CreateParameter("INTERVALO", adInteger, adParamInput, , 1)
    adocommAdo.Parameters.Append adoparamPar

    StartYear = DatePart("yyyy", Date)
    StartMonth = DatePart("m", Date)
    StartDay = DatePart("d", Date)
    If Len(StartMonth) < 2 Then StartMonth = "0" & StartMonth
    If Len(StartDay) < 2 Then StartDay = "0" & StartDay

    sFecha = StartYear & StartMonth & StartDay
    lFecha = CLng(sFecha)
    Set adoparamPar = adocommAdo.CreateParameter("FECINI", adInteger, adParamInput, , lFecha)
    adocommAdo.Parameters.Append adoparamPar

    sFecha = "10000"
    lFecha = CLng(sFecha)
    Set adoparamPar = adocommAdo.CreateParameter("TIMEINI", adInteger, adParamInput, , lFecha)
    adocommAdo.Parameters.Append adoparamPar

    Set adoparamPar = adocommAdo.CreateParameter("NOMBRE_STEP", adVarChar, adParamInput, 50, "STEP1")
    adocommAdo.Parameters.Append adoparamPar
    Set adoparamPar = adocommAdo.CreateParameter("NOMBRE_PROC", adVarChar, adParamInput, 50, "FSPM_ELIMINAR_SOLICITUDES_INCOMPLETAS")
    adocommAdo.Parameters.Append adoparamPar

    adocommAdo.Execute
    
    CrearJobEliminarInstIncomp = True

    Exit Function

Ir_Error:
    CrearJobEliminarInstIncomp = False

End Function




Public Function CrearJobEliminarInstIncomp_QA() As Boolean
Dim adocommAdo As ADODB.Command

Dim adoparamPar As ADODB.Parameter

Dim sFecha As String

Dim lFecha As Long

Dim StartYear, StartMonth, StartDay As String

On Error GoTo Ir_Error:


    Set adocommAdo = New ADODB.Command

    Set adocommAdo.ActiveConnection = moConexion.ADOCon

    adocommAdo.CommandType = adCmdStoredProc

    adocommAdo.CommandText = "SP_JOB_CREARJOBELIMINARINSTINCOMP_QA"

    

    

    

''    '****** Si no existe el job lo crea **********************

    

    'Nombre del Job

    Set adoparamPar = adocommAdo.CreateParameter("NOMBRE_JOB", adVarChar, adParamInput, 50, UCase("ELIM_INST_NOCREADAS_QA_" & gBD))

    adocommAdo.Parameters.Append adoparamPar

 

    'la planificaci�n : nombre,frecuencia,hora y fecha de comienzo...

    Set adoparamPar = adocommAdo.CreateParameter("NOMBRE_SCH", adVarChar, adParamInput, 50, "SCHED1")

    adocommAdo.Parameters.Append adoparamPar

 

    Set adoparamPar = adocommAdo.CreateParameter("TIPO", adInteger, adParamInput, , 4)

    adocommAdo.Parameters.Append adoparamPar

 

    Set adoparamPar = adocommAdo.CreateParameter("INTERVALO", adInteger, adParamInput, , 1)

    adocommAdo.Parameters.Append adoparamPar

 

    StartYear = DatePart("yyyy", Date)

    StartMonth = DatePart("m", Date)

    StartDay = DatePart("d", Date)

    If Len(StartMonth) < 2 Then StartMonth = "0" & StartMonth

    If Len(StartDay) < 2 Then StartDay = "0" & StartDay

 

    sFecha = StartYear & StartMonth & StartDay

    lFecha = CLng(sFecha)

    Set adoparamPar = adocommAdo.CreateParameter("FECINI", adInteger, adParamInput, , lFecha)

    adocommAdo.Parameters.Append adoparamPar
 

    sFecha = "10000"

    lFecha = CLng(sFecha)

    Set adoparamPar = adocommAdo.CreateParameter("TIMEINI", adInteger, adParamInput, , lFecha)

    adocommAdo.Parameters.Append adoparamPar

 

    Set adoparamPar = adocommAdo.CreateParameter("NOMBRE_STEP", adVarChar, adParamInput, 50, "STEP1")

    adocommAdo.Parameters.Append adoparamPar

    Set adoparamPar = adocommAdo.CreateParameter("NOMBRE_PROC", adVarChar, adParamInput, 50, "FSQA_ELIMINAR_INSTANCIA")

    adocommAdo.Parameters.Append adoparamPar

    adocommAdo.Execute

    CrearJobEliminarInstIncomp_QA = True

    Exit Function
 

Ir_Error:

    CrearJobEliminarInstIncomp_QA = False
End Function


Public Function CrearJobRenovarExpirarContratos() As Boolean
Dim adocommAdo As ADODB.Command
Dim adoparamPar As ADODB.Parameter
Dim sConsulta As String

On Error GoTo Ir_Error:

    Set adocommAdo = New ADODB.Command

    Set adocommAdo.ActiveConnection = moConexion.ADOCon

    adocommAdo.CommandType = adCmdStoredProc

    '****** Este stored: Borra el anterior JOB que solo expiraba contratos
    '******              Crea el JOB para renovar y expirar contratos (si no existe)
    adocommAdo.CommandText = "JOB_CONTRATOS_RENOVAR_EXPIRAR"
       
    '�nico par�metro: Nombre de la BD
    Set adoparamPar = adocommAdo.CreateParameter("NOMBREBD", adVarChar, adParamInput, 50, gBD)
    adocommAdo.Parameters.Append adoparamPar

    adocommAdo.Execute

    CrearJobRenovarExpirarContratos = True

    Exit Function
 

Ir_Error:

    CrearJobRenovarExpirarContratos = False
End Function

 Public Function CrearJobRecepcionesAutomaticas() As Boolean
    Dim adocommAdo As ADODB.Command
    Dim adoparamPar As ADODB.Parameter
    Dim sFecha As String
    Dim lFecha As Long
    Dim StartYear, StartMonth, StartDay As String

    On Error GoTo Ir_Error:

    Set adocommAdo = New ADODB.Command

    Set adocommAdo.ActiveConnection = moConexion.ADOCon

    adocommAdo.CommandType = adCmdStoredProc

    adocommAdo.CommandText = "SP_JOB_CREARJOB_RECEPCION_AUTOMATICA_ENTREGAS"
    
    'Nombre del Job

    Set adoparamPar = adocommAdo.CreateParameter("NOMBRE_JOB", adVarChar, adParamInput, 100, UCase("RECEPCION_AUTOMATICA_ENTREGAS_" & gBD))

    adocommAdo.Parameters.Append adoparamPar
    
    
    'la planificaci�n : nombre,frecuencia,hora y fecha de comienzo...

    Set adoparamPar = adocommAdo.CreateParameter("NOMBRE_SCH", adVarChar, adParamInput, 50, "RECEPSCHED1")

    adocommAdo.Parameters.Append adoparamPar
    
    
    Set adoparamPar = adocommAdo.CreateParameter("TIPO", adInteger, adParamInput, , 4) 'D�a

    adocommAdo.Parameters.Append adoparamPar
    
    Set adoparamPar = adocommAdo.CreateParameter("INTERVALO", adInteger, adParamInput, , 1) 'Cada 1 d�a

    adocommAdo.Parameters.Append adoparamPar
    

    Set adoparamPar = adocommAdo.CreateParameter("TIPO2", adInteger, adParamInput, , Null) 'horas

    adocommAdo.Parameters.Append adoparamPar

    Set adoparamPar = adocommAdo.CreateParameter("INTERVALO2", adInteger, adParamInput, , Null) 'Cada 5 horas

    adocommAdo.Parameters.Append adoparamPar
    
    StartYear = DatePart("yyyy", Date)
    StartMonth = DatePart("m", Date)
    StartDay = DatePart("d", Date)
    If Len(StartMonth) < 2 Then StartMonth = "0" & StartMonth
    If Len(StartDay) < 2 Then StartDay = "0" & StartDay

    sFecha = StartYear & StartMonth & StartDay
    lFecha = CLng(sFecha)
    Set adoparamPar = adocommAdo.CreateParameter("FECINI", adInteger, adParamInput, , lFecha)
    adocommAdo.Parameters.Append adoparamPar

    sFecha = "003000"
    lFecha = CLng(sFecha)
    Set adoparamPar = adocommAdo.CreateParameter("TIMEINI", adInteger, adParamInput, , lFecha)
    adocommAdo.Parameters.Append adoparamPar
    
    Set adoparamPar = adocommAdo.CreateParameter("NOMBRE_STEP", adVarChar, adParamInput, 50, "STEP1")
    
    adocommAdo.Parameters.Append adoparamPar
    
    Set adoparamPar = adocommAdo.CreateParameter("NOMBRE_PROC", adVarChar, adParamInput, 50, "FSGS_RECEPCION_AUTOMATICA_ENTREGAS")
    
    adocommAdo.Parameters.Append adoparamPar
    
    adocommAdo.Execute
    
    CrearJobRecepcionesAutomaticas = True
    Exit Function


Ir_Error:
    CrearJobRecepcionesAutomaticas = False
End Function

Public Function CrearJobBorradoProcesoMarcadoEnPedidoAutomatico() As Boolean
    Dim adocommAdo As ADODB.Command
    Dim adoparamPar As ADODB.Parameter
    Dim sFecha As String
    Dim lFecha As Long
    Dim StartYear, StartMonth, StartDay As String

    On Error GoTo Ir_Error:

    Set adocommAdo = New ADODB.Command

    Set adocommAdo.ActiveConnection = moConexion.ADOCon

    adocommAdo.CommandType = adCmdStoredProc

    adocommAdo.CommandText = "SP_JOB_CREARJOB_BORRADO_PROCESOMARCADOENPEDIDO"
    
    'Nombre del Job

    Set adoparamPar = adocommAdo.CreateParameter("NOMBRE_JOB", adVarChar, adParamInput, 200, UCase("BORRADO_PROCESOMARCADOENPEDIDO_" & gBD))

    adocommAdo.Parameters.Append adoparamPar
    
    
    'la planificaci�n : nombre,frecuencia,hora y fecha de comienzo...

    Set adoparamPar = adocommAdo.CreateParameter("NOMBRE_SCH", adVarChar, adParamInput, 50, "RECEPSCHED1")

    adocommAdo.Parameters.Append adoparamPar
    
    
    Set adoparamPar = adocommAdo.CreateParameter("TIPO", adInteger, adParamInput, , 4) 'D�a

    adocommAdo.Parameters.Append adoparamPar
    
    Set adoparamPar = adocommAdo.CreateParameter("INTERVALO", adInteger, adParamInput, , 1) 'Cada 1 d�a
    
    adocommAdo.Parameters.Append adoparamPar
    
    StartYear = DatePart("yyyy", Date)
    StartMonth = DatePart("m", Date)
    StartDay = DatePart("d", Date)
    If Len(StartMonth) < 2 Then StartMonth = "0" & StartMonth
    If Len(StartDay) < 2 Then StartDay = "0" & StartDay

    sFecha = StartYear & StartMonth & StartDay
    lFecha = CLng(sFecha)
    
    Set adoparamPar = adocommAdo.CreateParameter("FECINI", adInteger, adParamInput, , lFecha)
    
    adocommAdo.Parameters.Append adoparamPar

    sFecha = "000000"
    lFecha = CLng(sFecha)
    
    Set adoparamPar = adocommAdo.CreateParameter("TIMEINI", adInteger, adParamInput, , lFecha)
    
    adocommAdo.Parameters.Append adoparamPar

    Set adoparamPar = adocommAdo.CreateParameter("NOMBRE_STEP", adVarChar, adParamInput, 50, "STEP1")
    
    adocommAdo.Parameters.Append adoparamPar
    
    Set adoparamPar = adocommAdo.CreateParameter("NOMBRE_PROC", adVarChar, adParamInput, 50, "SP_BORRADO_PROCESO_MARCADOSENPEDIDO")
    
    adocommAdo.Parameters.Append adoparamPar
    
    adocommAdo.Execute
    
    CrearJobBorradoProcesoMarcadoEnPedidoAutomatico = True

    Exit Function

Ir_Error:
    CrearJobBorradoProcesoMarcadoEnPedidoAutomatico = False
End Function

Public Function CrearJobModificarEstadoPedidoAbierto() As Boolean
    Dim adocommAdo As ADODB.Command
    Dim adoparamPar As ADODB.Parameter
    Dim sFecha As String
    Dim lFecha As Long
    Dim StartYear, StartMonth, StartDay As String

    On Error GoTo Ir_Error:

    Set adocommAdo = New ADODB.Command
    Set adocommAdo.ActiveConnection = moConexion.ADOCon

    adocommAdo.CommandType = adCmdStoredProc
    adocommAdo.CommandText = "FS_CREAR_JOB"
    
    'BD
    Set adoparamPar = adocommAdo.CreateParameter("BD", adVarChar, adParamInput, 50, gBD)
    adocommAdo.Parameters.Append adoparamPar
        
    'Nombre del Job
    Set adoparamPar = adocommAdo.CreateParameter("NOMBRE_JOB", adVarChar, adParamInput, 200, UCase("MODIFICACION_ESTADO_PEDIDOS_ABIERTOS_" & gBD))
    adocommAdo.Parameters.Append adoparamPar
    
    'la planificaci�n : nombre,frecuencia,hora y fecha de comienzo...
    Set adoparamPar = adocommAdo.CreateParameter("NOMBRE_SCH", adVarChar, adParamInput, 50, "SCHED1")
    adocommAdo.Parameters.Append adoparamPar
    
    Set adoparamPar = adocommAdo.CreateParameter("TIPO", adInteger, adParamInput, , 4) 'D�a
    adocommAdo.Parameters.Append adoparamPar
    
    Set adoparamPar = adocommAdo.CreateParameter("INTERVALO", adInteger, adParamInput, , 1) 'Cada 1 d�a
    adocommAdo.Parameters.Append adoparamPar
    
    StartYear = DatePart("yyyy", Date)
    StartMonth = DatePart("m", Date)
    StartDay = DatePart("d", Date)
    If Len(StartMonth) < 2 Then StartMonth = "0" & StartMonth
    If Len(StartDay) < 2 Then StartDay = "0" & StartDay

    sFecha = StartYear & StartMonth & StartDay
    lFecha = CLng(sFecha)
    
    Set adoparamPar = adocommAdo.CreateParameter("FECINI", adInteger, adParamInput, , lFecha)
    adocommAdo.Parameters.Append adoparamPar

    sFecha = "000100"
    lFecha = CLng(sFecha)
    
    Set adoparamPar = adocommAdo.CreateParameter("TIMEINI", adInteger, adParamInput, , lFecha)
    adocommAdo.Parameters.Append adoparamPar

    Set adoparamPar = adocommAdo.CreateParameter("NOMBRE_STEP", adVarChar, adParamInput, 50, "STEP1")
    adocommAdo.Parameters.Append adoparamPar
    
    Set adoparamPar = adocommAdo.CreateParameter("NOMBRE_PROC", adVarChar, adParamInput, 50, "FSGS_PED_ABIERTO_ESTADO")
    adocommAdo.Parameters.Append adoparamPar
    
    adocommAdo.Execute
    
    CrearJobModificarEstadoPedidoAbierto = True

    Exit Function

Ir_Error:
    CrearJobModificarEstadoPedidoAbierto = False
End Function

