VERSION 5.00
Begin VB.Form frmGrupo 
   BackColor       =   &H00808000&
   Caption         =   "Introduzca el c�digo y la denominaci�n del grupo"
   ClientHeight    =   1440
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   5850
   LinkTopic       =   "Form1"
   ScaleHeight     =   1440
   ScaleWidth      =   5850
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdCancelar 
      Cancel          =   -1  'True
      Caption         =   "&Cancelar"
      Height          =   345
      Left            =   3690
      TabIndex        =   5
      Top             =   990
      Width           =   1470
   End
   Begin VB.CommandButton cmdAceptar 
      Caption         =   "&Aceptar"
      Default         =   -1  'True
      Height          =   345
      Left            =   690
      TabIndex        =   4
      Top             =   990
      Width           =   1470
   End
   Begin VB.TextBox txtDen 
      Height          =   285
      Left            =   2130
      MaxLength       =   100
      TabIndex        =   1
      Top             =   525
      Width           =   3240
   End
   Begin VB.TextBox txtCod 
      Height          =   285
      Left            =   2130
      MaxLength       =   3
      TabIndex        =   0
      Top             =   135
      Width           =   1575
   End
   Begin VB.Label Label2 
      BackColor       =   &H00808000&
      Caption         =   "Denominaci�n del grupo"
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   150
      TabIndex        =   3
      Top             =   570
      Width           =   1980
   End
   Begin VB.Label Label1 
      BackColor       =   &H00808000&
      Caption         =   "C�digo del grupo"
      ForeColor       =   &H00FFFFFF&
      Height          =   195
      Left            =   150
      TabIndex        =   2
      Top             =   180
      Width           =   1530
   End
End
Attribute VB_Name = "frmGrupo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdAceptar_Click()
If IsNull(Me.txtCod) Or IsNull(Me.txtDen) Then
    MsgBox "Se deben itroducir ambos datos", vbCritical, "FULLSTEP"
    Exit Sub
End If

gsCodGrupo = Me.txtCod
gsDenGrupo = Me.txtDen

Unload Me

End Sub

Private Sub cmdCancelar_Click()

gsCodGrupo = ""
gsDenGrupo = ""

Unload Me

End Sub
