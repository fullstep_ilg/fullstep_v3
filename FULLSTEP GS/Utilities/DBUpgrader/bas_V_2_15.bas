Attribute VB_Name = "bas_V_2_15"
Option Explicit
    
Public bCancelado As Boolean
Public Function CodigoDeActualizacion2_14_00_51A2_15_00_00() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim bBool As Boolean

On Error GoTo error
    

sConsulta = "BEGIN TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = True
ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

frmProgreso.lblDetalle = "Modificamos triggers"
frmProgreso.lblDetalle.Refresh
frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_15_Triggers_0



frmProgreso.lblDetalle = "Modificamos storeds"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_15_Storeds_0


sConsulta = "UPDATE VERSION SET NUM ='2.15.00.00'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "COMMIT TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = False

CodigoDeActualizacion2_14_00_51A2_15_00_00 = True
Exit Function
    
error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_14_00_51A2_15_00_00 = False

End Function

Public Function CodigoDeActualizacion2_15_00_00A2_15_00_01() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim bBool As Boolean

On Error GoTo error
    

sConsulta = "BEGIN TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = True
ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

frmProgreso.lblDetalle = "Configurar datos de la empresa inicial"
frmProgreso.lblDetalle.Refresh
frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

bCancelado = False
frmESTRORG.Show 1
If Not bCancelado Then
    sConsulta = ""
    sConsulta = sConsulta & "IF EXISTS (SELECT name FROM sysindexes WHERE name = 'IX_ORDEN_ENTREGA_01')" & vbCrLf
    sConsulta = sConsulta & "   DROP INDEX ORDEN_ENTREGA.IX_ORDEN_ENTREGA_01" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
    sConsulta = ""
    sConsulta = sConsulta & "CREATE  INDEX [IX_ORDEN_ENTREGA_01] ON [dbo].[ORDEN_ENTREGA]([REFERENCIA]) WITH  FILLFACTOR = 80 ON [PRIMARY]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
        
    V_2_15_Storeds_01
    
    
    sConsulta = "UPDATE VERSION SET NUM ='2.15.00.01'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta

End If
sConsulta = "COMMIT TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = False

CodigoDeActualizacion2_15_00_00A2_15_00_01 = Not bCancelado
Exit Function
    
error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_15_00_00A2_15_00_01 = False

End Function

Private Sub V_2_15_Triggers_0()
Dim sConsulta As String
sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ORDEN_EST_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[ORDEN_EST_TG_INS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER ORDEN_EST_TG_INS ON dbo.ORDEN_EST " & vbCrLf
sConsulta = sConsulta & "FOR INSERT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_ORDEN_EST_Ins CURSOR LOCAL FOR SELECT EST,ID,ORDEN,PER,COMENT,EXTERNO FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_ORDEN_EST_Ins" & vbCrLf
sConsulta = sConsulta & "DECLARE @EST AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_ORDEN AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @SENTIDO AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PER AS VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @USU AS VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @EXTERNO AS BIT" & vbCrLf
sConsulta = sConsulta & "DECLARE @TIPO AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ACTIVA AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ACTIVLOG AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ORIGEN AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @COMENT AS VARCHAR(255)" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_LOG_ORDEN_ENTREGA AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVE AS VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PAG AS VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @INTEGRACION AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ACCION AS VARCHAR(2)" & vbCrLf
sConsulta = sConsulta & "DECLARE @TRASPASO_ERP AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ERP  AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @NOGRABAR AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @TRASLADO_APROV_ERP AS TINYINT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_ORDEN_EST_Ins INTO @EST,@ID,@ID_ORDEN,@PER,@COMENT,@EXTERNO" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "  IF @EXTERNO IS NULL" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "    IF (@EST = 2 OR @EST = 3 OR @EST = 4 OR @EST = 20 OR @EST = 21) " & vbCrLf
sConsulta = sConsulta & "      BEGIN -- Si es uno de los estados que nos interesan para grabar en LOG_ORDEN_ENTREGA (emitido,aceptado,rechazado,anulado,en camino)" & vbCrLf
sConsulta = sConsulta & "        -- Recuperamos el tipo de orden de entrega para ver si est� activada su integraci�n" & vbCrLf
sConsulta = sConsulta & "        SELECT  @TIPO=TIPO,@PROVE=PROVE FROM ORDEN_ENTREGA WHERE ORDEN_ENTREGA.ID = @ID_ORDEN" & vbCrLf
sConsulta = sConsulta & "        SELECT  @ACTIVLOG = ACTIVLOG FROM PARGEN_GEST WHERE ID = 1" & vbCrLf
sConsulta = sConsulta & "        SELECT  @INTEGRACION =INTEGRACION, @TRASLADO_APROV_ERP=TRASLADO_APROV_ERP FROM PARGEN_INTERNO WHERE ID = 1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "        IF @TIPO = 0 AND @EST = 2  --Emisi�n de pedidos directos" & vbCrLf
sConsulta = sConsulta & "          SELECT  @TRASPASO_ERP= TRASPASO_PED_ERP FROM PARGEN_INTERNO WHERE ID = 1" & vbCrLf
sConsulta = sConsulta & "        ELSE" & vbCrLf
sConsulta = sConsulta & "          SET @TRASPASO_ERP=0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "        IF @INTEGRACION = 0" & vbCrLf
sConsulta = sConsulta & "          SET @ACTIVA = 0" & vbCrLf
sConsulta = sConsulta & "        ELSE " & vbCrLf
sConsulta = sConsulta & "         BEGIN" & vbCrLf
sConsulta = sConsulta & "          IF @TIPO = 0 --Pedidos directos" & vbCrLf
sConsulta = sConsulta & "            SELECT @ACTIVA=ACTIVA,@SENTIDO=SENTIDO FROM TABLAS_INTEGRACION WHERE ID = 13" & vbCrLf
sConsulta = sConsulta & "          ELSE" & vbCrLf
sConsulta = sConsulta & "            SELECT @ACTIVA=ACTIVA,@SENTIDO=SENTIDO FROM TABLAS_INTEGRACION WHERE ID = 11" & vbCrLf
sConsulta = sConsulta & "         END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "        IF (@ACTIVA = 1 AND (@SENTIDO=3 OR @SENTIDO=1)) OR @ACTIVLOG = 1 OR @TRASPASO_ERP=1 OR @TRASLADO_APROV_ERP = 1" & vbCrLf
sConsulta = sConsulta & "          -- si el sentido es = 2  (solo entrada) o no est� activa la integraci�n  no grabamos en log" & vbCrLf
sConsulta = sConsulta & "          BEGIN" & vbCrLf
sConsulta = sConsulta & "            SET @NOGRABAR=0" & vbCrLf
sConsulta = sConsulta & "            IF @TRASPASO_ERP=1  OR @TRASLADO_APROV_ERP = 1 --emisi�n de pedidos directos y de aprovisionamiento" & vbCrLf
sConsulta = sConsulta & "              BEGIN" & vbCrLf
sConsulta = sConsulta & "                SET @ERP=1" & vbCrLf
sConsulta = sConsulta & "                IF @ACTIVLOG = 1 AND @ACTIVA = 1 -- Integracion y log de actividad" & vbCrLf
sConsulta = sConsulta & "                  SET @ORIGEN = 1" & vbCrLf
sConsulta = sConsulta & "                ELSE" & vbCrLf
sConsulta = sConsulta & "                  BEGIN" & vbCrLf
sConsulta = sConsulta & "                    IF @ACTIVLOG = 1" & vbCrLf
sConsulta = sConsulta & "                      SET @ORIGEN = 1      " & vbCrLf
sConsulta = sConsulta & "                    ELSE " & vbCrLf
sConsulta = sConsulta & "                      SET @NOGRABAR=1" & vbCrLf
sConsulta = sConsulta & "                 END" & vbCrLf
sConsulta = sConsulta & "              END" & vbCrLf
sConsulta = sConsulta & "            ELSE" & vbCrLf
sConsulta = sConsulta & "              begin" & vbCrLf
sConsulta = sConsulta & "                  IF @ACTIVLOG = 1 AND @ACTIVA = 1 " & vbCrLf
sConsulta = sConsulta & "                    SET @ORIGEN = 2 -- Integracion y log de actividad" & vbCrLf
sConsulta = sConsulta & "                 ELSE" & vbCrLf
sConsulta = sConsulta & "                   BEGIN" & vbCrLf
sConsulta = sConsulta & "                     IF @ACTIVLOG = 0" & vbCrLf
sConsulta = sConsulta & "                        SET @ORIGEN = 0 -- solo log de actividad" & vbCrLf
sConsulta = sConsulta & "                     ELSE" & vbCrLf
sConsulta = sConsulta & "                        SET @ORIGEN = 1 -- solo integraci�n" & vbCrLf
sConsulta = sConsulta & "                  END" & vbCrLf
sConsulta = sConsulta & "              end" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "            IF @PER IS NULL" & vbCrLf
sConsulta = sConsulta & "              SET @USU = NULL" & vbCrLf
sConsulta = sConsulta & "            ELSE" & vbCrLf
sConsulta = sConsulta & "              SET @USU = (SELECT COD FROM USU WHERE PER = @PER)" & vbCrLf
sConsulta = sConsulta & "              -- la acci�n ser� E - Cambio de estado o I Creaci�n orden de entrega)" & vbCrLf
sConsulta = sConsulta & "            SET @PAG = (SELECT PAG FROM PROVE WHERE COD = @PROVE)" & vbCrLf
sConsulta = sConsulta & "            IF @EST = 2  " & vbCrLf
sConsulta = sConsulta & "              SET @ACCION='I'" & vbCrLf
sConsulta = sConsulta & "            ELSE " & vbCrLf
sConsulta = sConsulta & "              BEGIN" & vbCrLf
sConsulta = sConsulta & "                IF @EST=20 " & vbCrLf
sConsulta = sConsulta & "                  SET @ACCION='D'" & vbCrLf
sConsulta = sConsulta & "                ELSE" & vbCrLf
sConsulta = sConsulta & "                  SET @ACCION='E'" & vbCrLf
sConsulta = sConsulta & "              END" & vbCrLf
sConsulta = sConsulta & "            IF @NOGRABAR=0" & vbCrLf
sConsulta = sConsulta & "              BEGIN" & vbCrLf
sConsulta = sConsulta & "                INSERT INTO LOG_ORDEN_ENTREGA (ACCION,ID_ORDEN_ENTREGA,ID_ORDEN_EST,ID_PEDIDO,ANYO,PROVE,NUM_PEDIDO,NUM_ORDEN,NUMEXT,TIPO,FECHA,IMPORTE,PAG,EST,INCORRECTA,COMENT,ORIGEN,PER,USU,MON,CAMBIO,EMPRESA,RECEPTOR,COD_PROVE_ERP)" & vbCrLf
sConsulta = sConsulta & "                SELECT @ACCION,@ID_ORDEN,@ID,PEDIDO,ORDEN_ENTREGA.ANYO,PROVE,PEDIDO.NUM,ORDEN_ENTREGA.NUM,NUMEXT,ORDEN_ENTREGA.TIPO,ORDEN_ENTREGA.FECHA,IMPORTE,@PAG,@EST,INCORRECTA,@COMENT,@ORIGEN,@PER,@USU, MON,CAMBIO,EMPRESA,RECEPTOR,COD_PROVE_ERP FROM ORDEN_ENTREGA INNER JOIN PEDIDO ON PEDIDO.ID = ORDEN_ENTREGA.PEDIDO WHERE ORDEN_ENTREGA.ID = @ID_ORDEN" & vbCrLf
sConsulta = sConsulta & "                SET @ID_LOG_ORDEN_ENTREGA = (SELECT MAX(ID) FROM LOG_ORDEN_ENTREGA)" & vbCrLf
sConsulta = sConsulta & "                -- En el caso de Emisi�n de Pedido Directo, todav�a no estar�n creadas las lineas de pedido, se hace en el trigger LINEAS_PEDIDO_TG_INS" & vbCrLf
sConsulta = sConsulta & "                INSERT INTO LOG_LINEAS_PEDIDO (ACCION,ID_LOG_ORDEN_ENTREGA,ID_LINEAS_PEDIDO,ID_ORDEN_EST,ART_INT,UP,PREC_UP,CANT_PED,DEST,ANYO,GMN1,GMN2,GMN3,GMN4,PROCE,ITEM,ENTREGA_OBL,FECENTREGA,FECENTREGAPROVE,EST,CAT1,CAT2,CAT3,CAT4,CAT5,OBS,FC)" & vbCrLf
sConsulta = sConsulta & "                SELECT @ACCION,@ID_LOG_ORDEN_ENTREGA,ID,@ID,ART_INT,UP,PREC_UP,CANT_PED,DEST,ANYO,GMN1,GMN2,GMN3,GMN4,PROCE,ITEM,ENTREGA_OBL,FECENTREGA,FECENTREGAPROVE,EST,CAT1,CAT2,CAT3,CAT4,CAT5,OBS,FC FROM LINEAS_PEDIDO WHERE  ORDEN = @ID_ORDEN" & vbCrLf
sConsulta = sConsulta & "              END" & vbCrLf
sConsulta = sConsulta & "            IF @ERP=1 " & vbCrLf
sConsulta = sConsulta & "              BEGIN" & vbCrLf
sConsulta = sConsulta & "                INSERT INTO LOG_ORDEN_ENTREGA (ACCION,ID_ORDEN_ENTREGA,ID_ORDEN_EST,ID_PEDIDO,ANYO,PROVE,NUM_PEDIDO,NUM_ORDEN,NUMEXT,TIPO,FECHA,IMPORTE,PAG,EST,INCORRECTA,COMENT,ORIGEN,PER,USU,MON,CAMBIO,EMPRESA,RECEPTOR,COD_PROVE_ERP)" & vbCrLf
sConsulta = sConsulta & "                SELECT @ACCION,@ID_ORDEN,@ID,PEDIDO,ORDEN_ENTREGA.ANYO,PROVE,PEDIDO.NUM,ORDEN_ENTREGA.NUM,NUMEXT,ORDEN_ENTREGA.TIPO,ORDEN_ENTREGA.FECHA,IMPORTE,@PAG,@EST,INCORRECTA,@COMENT,0,@PER,@USU, MON,CAMBIO,EMPRESA,RECEPTOR,COD_PROVE_ERP FROM ORDEN_ENTREGA INNER JOIN PEDIDO ON PEDIDO.ID = ORDEN_ENTREGA.PEDIDO WHERE ORDEN_ENTREGA.ID = @ID_ORDEN" & vbCrLf
sConsulta = sConsulta & "                SET @ID_LOG_ORDEN_ENTREGA = (SELECT MAX(ID) FROM LOG_ORDEN_ENTREGA)" & vbCrLf
sConsulta = sConsulta & "                INSERT INTO LOG_LINEAS_PEDIDO (ACCION,ID_LOG_ORDEN_ENTREGA,ID_LINEAS_PEDIDO,ID_ORDEN_EST,ART_INT,UP,PREC_UP,CANT_PED,DEST,ANYO,GMN1,GMN2,GMN3,GMN4,PROCE,ITEM,ENTREGA_OBL,FECENTREGA,FECENTREGAPROVE,EST,CAT1,CAT2,CAT3,CAT4,CAT5,OBS,FC)" & vbCrLf
sConsulta = sConsulta & "                SELECT @ACCION,@ID_LOG_ORDEN_ENTREGA,ID,@ID,ART_INT,UP,PREC_UP,CANT_PED,DEST,ANYO,GMN1,GMN2,GMN3,GMN4,PROCE,ITEM,ENTREGA_OBL,FECENTREGA,FECENTREGAPROVE,EST,CAT1,CAT2,CAT3,CAT4,CAT5,OBS,FC FROM LINEAS_PEDIDO WHERE  ORDEN = @ID_ORDEN" & vbCrLf
sConsulta = sConsulta & "              END" & vbCrLf
sConsulta = sConsulta & "          END --IF ACTIVA" & vbCrLf
sConsulta = sConsulta & "      END -- IF EST" & vbCrLf
sConsulta = sConsulta & "   END -- IF @EXTERNO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  UPDATE ORDEN_ENTREGA SET ORDEN_EST = @ID WHERE ORDEN_ENTREGA.ID = @ID_ORDEN" & vbCrLf
sConsulta = sConsulta & "  FETCH NEXT FROM curTG_ORDEN_EST_Ins INTO @EST,@ID,@ID_ORDEN,@PER,@COMENT,@EXTERNO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CLOSE curTG_ORDEN_EST_Ins" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_ORDEN_EST_Ins" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
End Sub

Private Sub V_2_15_Storeds_0()
Dim sConsulta As String

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_AR_VOLUMENES]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_AR_VOLUMENES]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "CREATE   PROCEDURE  SP_AR_VOLUMENES (@ANYO SMALLINT,@GMN1 VARCHAR(50), @PROCE INT,@FECULT DATETIME,@DEN VARCHAR(200),@RES INT OUTPUT)  AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVE AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @FSP_COD AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @VOL AS FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @SRV AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @BD AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @CODCIA AS VARCHAR(80)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL AS VARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @MONCEN AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @MONPORT AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @SRV=FSP_SRV,@BD=FSP_BD,@CODCIA=FSP_CIA FROM PARGEN_PORT WHERE ID=1" & vbCrLf
sConsulta = sConsulta & "SELECT @MONCEN=MONCEN FROM PARGEN_DEF WHERE ID=1" & vbCrLf
sConsulta = sConsulta & "SELECT @MONPORT= FSP_COD FROM MON WHERE COD= @MONCEN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE Cur_VOL Cursor FORWARD_ONLY STATIC READ_ONLY FOR " & vbCrLf
sConsulta = sConsulta & "SELECT AR_ITEM_PROVE.PROVE,FSP_COD,SUM(AR_ITEM_PROVE.PREC) FROM AR_ITEM_PROVE" & vbCrLf
sConsulta = sConsulta & "  INNER JOIN PROVE ON AR_ITEM_PROVE.PROVE= PROVE.COD WHERE AR_ITEM_PROVE.ANYO=@ANYO AND  AR_ITEM_PROVE.GMN1=@GMN1 AND AR_ITEM_PROVE.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "  AND AR_ITEM_PROVE.PROVE IN (SELECT PROVE FROM PROCE_OFE_WEB WHERE ANYO=@ANYO AND PROCE_OFE_WEB.GMN1 = @GMN1 AND PROCE_OFE_WEB.PROCE=@PROCE)" & vbCrLf
sConsulta = sConsulta & "  GROUP BY AR_ITEM_PROVE.PROVE,FSP_COD" & vbCrLf
sConsulta = sConsulta & "OPEN Cur_VOL" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM Cur_VOL INTO @PROVE,@FSP_COD,@VOL" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "   if @FSP_COD IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "     SET @SQL=@SRV + '.' + @BD + '.dbo.SP_ADD_VOLADJ'" & vbCrLf
sConsulta = sConsulta & "      EXECUTE @SQL @CODCIA,@FSP_COD,@ANYO,@GMN1,@PROCE,@VOL,@MONCEN,@MONPORT,@FECULT,@DEN" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM Cur_VOL INTO @PROVE,@FSP_COD,@VOL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "ERR_:" & vbCrLf
sConsulta = sConsulta & "Close Cur_VOL" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE Cur_VOL" & vbCrLf
sConsulta = sConsulta & "SET @RES=@@ERROR" & vbCrLf
sConsulta = sConsulta & "Return" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
End Sub


Private Sub V_2_15_Storeds_01()
Dim sConsulta As String
sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_DEVOLVER_EMPRESAS_PERSONA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_DEVOLVER_EMPRESAS_PERSONA]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_DEVOLVER_LINEAS_DE_ORDEN]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_DEVOLVER_LINEAS_DE_ORDEN]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_DEVOLVER_RECEPTORES]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_DEVOLVER_RECEPTORES]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE FSEP_DEVOLVER_EMPRESAS_PERSONA @PER VARCHAR(50), @ID INT OUTPUT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @UON1 VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @UON2 VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @UON3 VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "select @UON1 = uon1, @UON2 = uon2, @UON3 = uon3 from per where cod = @PER" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF (SELECT PED_OTRAS_EMP FROM USU WHERE PER = @PER) = 1" & vbCrLf
sConsulta = sConsulta & "  IF @UON3 IS NULL" & vbCrLf
sConsulta = sConsulta & "    IF @UON2 IS NULL" & vbCrLf
sConsulta = sConsulta & "      BEGIN" & vbCrLf
sConsulta = sConsulta & "      SELECT @ID = EMPRESA FROM UON1 WHERE COD = @UON1" & vbCrLf
sConsulta = sConsulta & "      SELECT ID, NIF, DEN, DIR, CP, POB, PAI, PROVI, ERP FROM EMP" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "    ELSE" & vbCrLf
sConsulta = sConsulta & "      BEGIN" & vbCrLf
sConsulta = sConsulta & "      SELECT @ID = EMPRESA FROM UON2 WHERE UON1 = @UON1 AND COD = @UON2" & vbCrLf
sConsulta = sConsulta & "      IF @ID IS NULL" & vbCrLf
sConsulta = sConsulta & "        SELECT @ID = EMPRESA FROM UON1 WHERE COD = @UON1" & vbCrLf
sConsulta = sConsulta & "      SELECT ID, NIF, DEN, DIR, CP, POB, PAI, PROVI, ERP FROM EMP" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "  ELSE" & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "    SELECT @ID = EMPRESA FROM UON3 WHERE UON1 = @UON1 AND UON2 = @UON2 AND COD = @UON3" & vbCrLf
sConsulta = sConsulta & "    IF @ID IS NULL" & vbCrLf
sConsulta = sConsulta & "      SELECT @ID = EMPRESA FROM UON2 WHERE UON1 = @UON1 AND COD = @UON2 " & vbCrLf
sConsulta = sConsulta & "    IF @ID IS NULL" & vbCrLf
sConsulta = sConsulta & "      SELECT @ID = EMPRESA FROM UON1 WHERE COD = @UON1" & vbCrLf
sConsulta = sConsulta & "    SELECT ID, NIF, DEN, DIR, CP, POB, PAI, PROVI, ERP FROM EMP" & vbCrLf
sConsulta = sConsulta & "    END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  IF @UON3 IS NULL" & vbCrLf
sConsulta = sConsulta & "    IF @UON2 IS NULL" & vbCrLf
sConsulta = sConsulta & "      BEGIN" & vbCrLf
sConsulta = sConsulta & "      SELECT @ID = EMPRESA FROM UON1 WHERE COD = @UON1" & vbCrLf
sConsulta = sConsulta & "      SELECT ID, NIF, DEN, DIR, CP, POB, PAI, PROVI, ERP FROM (" & vbCrLf
sConsulta = sConsulta & "      SELECT DISTINCT EMPRESA FROM (" & vbCrLf
sConsulta = sConsulta & "      SELECT EMPRESA FROM UON1 WHERE COD = @UON1" & vbCrLf
sConsulta = sConsulta & "      UNION" & vbCrLf
sConsulta = sConsulta & "      SELECT EMPRESA FROM UON2 WHERE UON1 = @UON1" & vbCrLf
sConsulta = sConsulta & "      UNION" & vbCrLf
sConsulta = sConsulta & "      SELECT EMPRESA FROM UON3 WHERE UON1 = @UON1" & vbCrLf
sConsulta = sConsulta & "      UNION" & vbCrLf
sConsulta = sConsulta & "      SELECT @ID) U)DU " & vbCrLf
sConsulta = sConsulta & "        INNER JOIN EMP E" & vbCrLf
sConsulta = sConsulta & "                ON DU.EMPRESA = E.ID" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "    ELSE" & vbCrLf
sConsulta = sConsulta & "      BEGIN" & vbCrLf
sConsulta = sConsulta & "      SELECT @ID = EMPRESA FROM UON2 WHERE UON1 = @UON1 AND COD = @UON2" & vbCrLf
sConsulta = sConsulta & "      IF @ID IS NULL" & vbCrLf
sConsulta = sConsulta & "        SELECT @ID = EMPRESA FROM UON1 WHERE COD = @UON1" & vbCrLf
sConsulta = sConsulta & "      SELECT ID, NIF, DEN, DIR, CP, POB, PAI, PROVI, ERP FROM (" & vbCrLf
sConsulta = sConsulta & "      SELECT DISTINCT EMPRESA FROM (" & vbCrLf
sConsulta = sConsulta & "      SELECT EMPRESA FROM UON2 WHERE UON1 = @UON1 AND COD = @UON2" & vbCrLf
sConsulta = sConsulta & "      UNION" & vbCrLf
sConsulta = sConsulta & "      SELECT EMPRESA FROM UON3 WHERE UON1 = @UON1 AND UON2 = @UON2" & vbCrLf
sConsulta = sConsulta & "      UNION" & vbCrLf
sConsulta = sConsulta & "      SELECT @ID) U)DU " & vbCrLf
sConsulta = sConsulta & "        INNER JOIN EMP E" & vbCrLf
sConsulta = sConsulta & "                ON DU.EMPRESA = E.ID" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "  ELSE" & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "    SELECT @ID = EMPRESA FROM UON3 WHERE UON1 = @UON1 AND UON2 = @UON2 AND COD = @UON3" & vbCrLf
sConsulta = sConsulta & "    IF @ID IS NULL" & vbCrLf
sConsulta = sConsulta & "      SELECT @ID = EMPRESA FROM UON2 WHERE UON1 = @UON1 AND COD = @UON2 " & vbCrLf
sConsulta = sConsulta & "    IF @ID IS NULL" & vbCrLf
sConsulta = sConsulta & "      SELECT @ID = EMPRESA FROM UON1 WHERE COD = @UON1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "      SELECT ID, NIF, DEN, DIR, CP, POB, PAI, PROVI, ERP FROM (" & vbCrLf
sConsulta = sConsulta & "      SELECT DISTINCT EMPRESA FROM (" & vbCrLf
sConsulta = sConsulta & "      SELECT EMPRESA FROM UON3 WHERE UON1 = @UON1 AND UON2 = @UON2 AND COD = @UON3" & vbCrLf
sConsulta = sConsulta & "      UNION" & vbCrLf
sConsulta = sConsulta & "      SELECT @ID) U)DU " & vbCrLf
sConsulta = sConsulta & "        INNER JOIN EMP E" & vbCrLf
sConsulta = sConsulta & "                ON DU.EMPRESA = E.ID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "    END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE FSEP_DEVOLVER_LINEAS_DE_ORDEN(@APROBADOR VARCHAR(50), @ORDENID INTEGER, @SOLICIT int = null ,@IDI varchar(20) = 'SPA', @MOSTRARCINTERNO tinyint = null)  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(1500)" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if @APROBADOR IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT LP.ID AS LINEAID," & vbCrLf
sConsulta = sConsulta & "                LP.ART_INT + ITEM.ART CODART," & vbCrLf
sConsulta = sConsulta & "                ART4.DEN + ITEM.DESCR  + ART_AUX.DEN + LP.DESCR_LIBRE DENART," & vbCrLf
sConsulta = sConsulta & "                LP.ART_INT + ITEM.ART + ' ' + ART4.DEN + ITEM.DESCR  + ART_AUX.DEN + + LP.DESCR_LIBRE AS ARTICULO," & vbCrLf
sConsulta = sConsulta & "                PA.COD_EXT ART_EXT," & vbCrLf
sConsulta = sConsulta & "                LP.GMN1," & vbCrLf
sConsulta = sConsulta & "                LP.GMN2," & vbCrLf
sConsulta = sConsulta & "                LP.GMN3," & vbCrLf
sConsulta = sConsulta & "                LP.GMN4," & vbCrLf
sConsulta = sConsulta & "              LP.PROVE," & vbCrLf
sConsulta = sConsulta & "                LP.DEST AS DESTINO," & vbCrLf
sConsulta = sConsulta & "                LP.UP AS UNIDAD," & vbCrLf
sConsulta = sConsulta & "                LP.PREC_UP AS PRECIOUNITARIO," & vbCrLf
sConsulta = sConsulta & "                LP.CANT_PED AS CANTIDAD," & vbCrLf
sConsulta = sConsulta & "                LP.CANT_REC AS CANTIDAD_REC," & vbCrLf
sConsulta = sConsulta & "                ITEM_ADJ.CANT_ADJ / LP.FC AS CANTADJUDICADA," & vbCrLf
sConsulta = sConsulta & "                ITEM_ADJ.CANT_PED / LP.FC AS CANTCONSUMIDA," & vbCrLf
sConsulta = sConsulta & "                LP.PREC_UP * LP.CANT_PED IMPORTE," & vbCrLf
sConsulta = sConsulta & "                LP.EST AS APROBADA," & vbCrLf
sConsulta = sConsulta & "                LP.FECEMISION AS FECHA," & vbCrLf
sConsulta = sConsulta & "                GESTIONABLE=CASE WHEN APROB_LIM.PER= @APROBADOR   THEN 1 WHEN (APROB_LIM.PER IS NULL AND SEGURIDAD.APROB_TIPO1=@APROBADOR ) THEN 1  ELSE 0 END," & vbCrLf
sConsulta = sConsulta & "                LP.CAT1," & vbCrLf
sConsulta = sConsulta & "                LP.CAT2," & vbCrLf
sConsulta = sConsulta & "                LP.CAT3," & vbCrLf
sConsulta = sConsulta & "                LP.CAT4," & vbCrLf
sConsulta = sConsulta & "                LP.CAT5," & vbCrLf
sConsulta = sConsulta & "                LP.OBS," & vbCrLf
sConsulta = sConsulta & "                case when c5.cod is null then " & vbCrLf
sConsulta = sConsulta & "                        case when c4.cod is null then " & vbCrLf
sConsulta = sConsulta & "                             case when c3.cod is null then" & vbCrLf
sConsulta = sConsulta & "                                  case when c2.cod is null then" & vbCrLf
sConsulta = sConsulta & "                                       c1.cod" & vbCrLf
sConsulta = sConsulta & "                                  else" & vbCrLf
sConsulta = sConsulta & "                                  c2.cod" & vbCrLf
sConsulta = sConsulta & "                                  end" & vbCrLf
sConsulta = sConsulta & "                             else" & vbCrLf
sConsulta = sConsulta & "                             c3.cod" & vbCrLf
sConsulta = sConsulta & "                             end" & vbCrLf
sConsulta = sConsulta & "                        else" & vbCrLf
sConsulta = sConsulta & "                        c4.cod" & vbCrLf
sConsulta = sConsulta & "                        end" & vbCrLf
sConsulta = sConsulta & "                    else" & vbCrLf
sConsulta = sConsulta & "                    c5.cod" & vbCrLf
sConsulta = sConsulta & "                    end CATEGORIA," & vbCrLf
sConsulta = sConsulta & "                case when c5.cod is null then " & vbCrLf
sConsulta = sConsulta & "                        case when c4.cod is null then " & vbCrLf
sConsulta = sConsulta & "                             case when c3.cod is null then" & vbCrLf
sConsulta = sConsulta & "                                  case when c2.cod is null then" & vbCrLf
sConsulta = sConsulta & "                                       '(' + c1.cod + ') ' + c1.den" & vbCrLf
sConsulta = sConsulta & "                                  else" & vbCrLf
sConsulta = sConsulta & "                                  '(' + c1.cod + ') ' + c1.den + ' - (' + c2.cod + ') ' + c2.den" & vbCrLf
sConsulta = sConsulta & "                                  end" & vbCrLf
sConsulta = sConsulta & "                             else" & vbCrLf
sConsulta = sConsulta & "                             '(' + c1.cod + ') ' + c1.den + ' - (' + c2.cod + ') ' + c2.den + ' - (' + c3.cod + ') ' + c3.den" & vbCrLf
sConsulta = sConsulta & "                             end" & vbCrLf
sConsulta = sConsulta & "                        else" & vbCrLf
sConsulta = sConsulta & "                        '(' + c1.cod + ') ' + c1.den + ' - (' + c2.cod + ') ' + c2.den + ' - (' + c3.cod + ') ' + c3.den + ' - (' + c4.cod + ') ' + c4.den" & vbCrLf
sConsulta = sConsulta & "                        end" & vbCrLf
sConsulta = sConsulta & "                    else" & vbCrLf
sConsulta = sConsulta & "                    '(' + c1.cod + ') ' + c1.den + ' - (' + c2.cod + ') ' + c2.den + ' - (' + c3.cod + ') ' + c3.den + ' - (' + c4.cod + ') ' + c4.den + ' - (' + c5.cod + ') ' + c5.den" & vbCrLf
sConsulta = sConsulta & "                    end CATEGORIARAMA," & vbCrLf
sConsulta = sConsulta & "                ITEM.ANYO ANYOPROCE," & vbCrLf
sConsulta = sConsulta & "                ITEM.PROCE CODPROCE," & vbCrLf
sConsulta = sConsulta & "                LE.COMENT,LPA.ADJUNTOS," & vbCrLf
sConsulta = sConsulta & "                CASE WHEN @MOSTRARCINTERNO IS NULL OR @MOSTRARCINTERNO=1 OR CC1.INTERNO=0 THEN LP.VALOR1 ELSE NULL END VALOR1," & vbCrLf
sConsulta = sConsulta & "                CASE WHEN @MOSTRARCINTERNO IS NULL OR @MOSTRARCINTERNO=1 OR CC2.INTERNO=0 THEN LP.VALOR2 ELSE NULL END VALOR2," & vbCrLf
sConsulta = sConsulta & "                CC1.DEN AS DENCAMPO1,CC2.DEN AS DENCAMPO2, PROCE.DEN AS DENPROCE,LP.OBSADJUN,LP.FECENTREGA," & vbCrLf
sConsulta = sConsulta & "                DEST.DEN AS DESTINODEN " & vbCrLf
sConsulta = sConsulta & "FROM LINEAS_PEDIDO LP" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN APROB_LIM" & vbCrLf
sConsulta = sConsulta & "            ON LP.APROB_LIM=APROB_LIM.ID" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN SEGURIDAD " & vbCrLf
sConsulta = sConsulta & "            ON LP.SEGURIDAD=SEGURIDAD.ID" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN ITEM " & vbCrLf
sConsulta = sConsulta & "            ON LP.ANYO = ITEM.ANYO" & vbCrLf
sConsulta = sConsulta & "           AND LP.GMN1=ITEM.GMN1 " & vbCrLf
sConsulta = sConsulta & "           AND LP.PROCE=ITEM.PROCE " & vbCrLf
sConsulta = sConsulta & "           AND LP.ITEM=ITEM.ID" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN ART4 " & vbCrLf
sConsulta = sConsulta & "            ON LP.GMN1=ART4.GMN1 " & vbCrLf
sConsulta = sConsulta & "           AND LP.GMN2=ART4.GMN2 " & vbCrLf
sConsulta = sConsulta & "           AND LP.GMN3=ART4.GMN3 " & vbCrLf
sConsulta = sConsulta & "           AND LP.GMN4=ART4.GMN4 " & vbCrLf
sConsulta = sConsulta & "           AND LP.ART_INT=ART4.COD" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN ITEM_ADJ " & vbCrLf
sConsulta = sConsulta & "            ON LP.ITEM=ITEM_ADJ.ITEM " & vbCrLf
sConsulta = sConsulta & "           AND LP.ANYO = ITEM_ADJ.ANYO" & vbCrLf
sConsulta = sConsulta & "           AND LP.GMN1=ITEM_ADJ.GMN1 " & vbCrLf
sConsulta = sConsulta & "           AND LP.PROCE=ITEM_ADJ.PROCE " & vbCrLf
sConsulta = sConsulta & "           AND LP.PROVE=ITEM_ADJ.PROVE" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN ART4 ART_AUX " & vbCrLf
sConsulta = sConsulta & "            ON ITEM.ART=ART_AUX.COD " & vbCrLf
sConsulta = sConsulta & "           AND ART_AUX.GMN1=ITEM.GMN1 " & vbCrLf
sConsulta = sConsulta & "           AND ART_AUX.GMN2=ITEM.GMN2 " & vbCrLf
sConsulta = sConsulta & "           AND ART_AUX.GMN3=ITEM.GMN3 " & vbCrLf
sConsulta = sConsulta & "           AND ART_AUX.GMN4=ITEM.GMN4 " & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN PROVE_ART4 PA" & vbCrLf
sConsulta = sConsulta & "            ON LP.PROVE = PA.PROVE" & vbCrLf
sConsulta = sConsulta & "           AND LP.GMN1 = PA.GMN1" & vbCrLf
sConsulta = sConsulta & "           AND LP.GMN2 = PA.GMN2" & vbCrLf
sConsulta = sConsulta & "           AND LP.GMN3 = PA.GMN3" & vbCrLf
sConsulta = sConsulta & "           AND LP.GMN4 = PA.GMN4" & vbCrLf
sConsulta = sConsulta & "           AND CASE WHEN LP.ART_INT IS NULL THEN ITEM.ART ELSE LP.ART_INT END = PA.ART" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN CATN1 C1" & vbCrLf
sConsulta = sConsulta & "            ON LP.CAT1 = C1.ID" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN CATN2 C2" & vbCrLf
sConsulta = sConsulta & "            ON LP.CAT2 = C2.ID" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN CATN3 C3" & vbCrLf
sConsulta = sConsulta & "            ON LP.CAT3 = C3.ID" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN CATN4 C4" & vbCrLf
sConsulta = sConsulta & "            ON LP.CAT4 = C4.ID" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN CATN5 C5" & vbCrLf
sConsulta = sConsulta & "            ON LP.CAT5 = C5.ID" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN LINEAS_EST LE" & vbCrLf
sConsulta = sConsulta & "            ON LP.LINEAS_EST = LE.ID" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN " & vbCrLf
sConsulta = sConsulta & "   (SELECT COUNT(*) AS ADJUNTOS,LINEA  FROM LINEAS_PEDIDO_ADJUN GROUP BY LINEAS_PEDIDO_ADJUN.LINEA) LPA ON LP.ID= LPA.LINEA " & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN CATALOG_CAMPOS AS CC1 " & vbCrLf
sConsulta = sConsulta & "            ON CC1.ID=LP.CAMPO1 AND CC1.IDIOMA=@IDI" & vbCrLf
sConsulta = sConsulta & "           AND CC1.INTERNO =  CASE WHEN @MOSTRARCINTERNO IS NULL OR @MOSTRARCINTERNO=1 THEN CC1.INTERNO ELSE 0 END" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN CATALOG_CAMPOS AS CC2 " & vbCrLf
sConsulta = sConsulta & "            ON CC2.ID=LP.CAMPO2 AND CC2.IDIOMA=@IDI" & vbCrLf
sConsulta = sConsulta & "           AND CC2.INTERNO = CASE WHEN @MOSTRARCINTERNO IS NULL OR @MOSTRARCINTERNO=1 THEN CC2.INTERNO ELSE 0 END" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN PROCE " & vbCrLf
sConsulta = sConsulta & "       ON LP.ANYO=PROCE.ANYO AND LP.GMN1=PROCE.GMN1 AND LP.PROCE=PROCE.COD" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN DEST" & vbCrLf
sConsulta = sConsulta & "       ON LP.DEST=DEST.COD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "WHERE LP.ORDEN=@ORDENID" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT LP.ID LINEAID," & vbCrLf
sConsulta = sConsulta & "                LP.ART_INT + I.ART CODART," & vbCrLf
sConsulta = sConsulta & "                LP.GMN1," & vbCrLf
sConsulta = sConsulta & "                LP.GMN2," & vbCrLf
sConsulta = sConsulta & "                LP.GMN3," & vbCrLf
sConsulta = sConsulta & "                LP.GMN4," & vbCrLf
sConsulta = sConsulta & "               LP.PROVE," & vbCrLf
sConsulta = sConsulta & "                ART4.DEN + I.DESCR  + ART_AUX.DEN + + LP.DESCR_LIBRE DENART," & vbCrLf
sConsulta = sConsulta & "                PA.COD_EXT ART_EXT," & vbCrLf
sConsulta = sConsulta & "                LP.DEST DESTINO," & vbCrLf
sConsulta = sConsulta & "                D.DEN DESTDEN," & vbCrLf
sConsulta = sConsulta & "                LP.UP UNIDAD," & vbCrLf
sConsulta = sConsulta & "                U.DEN UNIDEN," & vbCrLf
sConsulta = sConsulta & "                LP.PREC_UP PRECIOUNITARIO," & vbCrLf
sConsulta = sConsulta & "                LP.CANT_PED CANTIDAD," & vbCrLf
sConsulta = sConsulta & "                LP.CANT_REC CANT_REC," & vbCrLf
sConsulta = sConsulta & "                IA.CANT_ADJ * LP.FC AS CANTADJUDICADA," & vbCrLf
sConsulta = sConsulta & "                LP.PREC_UP * LP.CANT_PED IMPORTE," & vbCrLf
sConsulta = sConsulta & "                ISNULL(LP.EST,1) AS APROBADA," & vbCrLf
sConsulta = sConsulta & "                LP.FECEMISION FECHA," & vbCrLf
sConsulta = sConsulta & "                LP.FECENTREGA FECENTREGA," & vbCrLf
sConsulta = sConsulta & "                ISNULL(LP.EST,1) EST,LP.ENTREGA_OBL,LP.FECENTREGAPROVE," & vbCrLf
sConsulta = sConsulta & "                LP.CAT1," & vbCrLf
sConsulta = sConsulta & "                LP.CAT2," & vbCrLf
sConsulta = sConsulta & "                LP.CAT3," & vbCrLf
sConsulta = sConsulta & "                LP.CAT4," & vbCrLf
sConsulta = sConsulta & "                LP.CAT5," & vbCrLf
sConsulta = sConsulta & "                LP.OBS," & vbCrLf
sConsulta = sConsulta & "                case when c5.cod is null then " & vbCrLf
sConsulta = sConsulta & "                        case when c4.cod is null then " & vbCrLf
sConsulta = sConsulta & "                             case when c3.cod is null then" & vbCrLf
sConsulta = sConsulta & "                                  case when c2.cod is null then" & vbCrLf
sConsulta = sConsulta & "                                       c1.cod" & vbCrLf
sConsulta = sConsulta & "                                  else" & vbCrLf
sConsulta = sConsulta & "                                  c2.cod" & vbCrLf
sConsulta = sConsulta & "                                  end" & vbCrLf
sConsulta = sConsulta & "                             else" & vbCrLf
sConsulta = sConsulta & "                             c3.cod" & vbCrLf
sConsulta = sConsulta & "                             end" & vbCrLf
sConsulta = sConsulta & "                        else" & vbCrLf
sConsulta = sConsulta & "                        c4.cod" & vbCrLf
sConsulta = sConsulta & "                        end" & vbCrLf
sConsulta = sConsulta & "                    else" & vbCrLf
sConsulta = sConsulta & "                    c5.cod" & vbCrLf
sConsulta = sConsulta & "                    end CATEGORIA," & vbCrLf
sConsulta = sConsulta & "                case when c5.cod is null then " & vbCrLf
sConsulta = sConsulta & "                        case when c4.cod is null then " & vbCrLf
sConsulta = sConsulta & "                             case when c3.cod is null then" & vbCrLf
sConsulta = sConsulta & "                                  case when c2.cod is null then" & vbCrLf
sConsulta = sConsulta & "                                       '(' + c1.cod + ') ' + c1.den" & vbCrLf
sConsulta = sConsulta & "                                  else" & vbCrLf
sConsulta = sConsulta & "                                  '(' + c1.cod + ') ' + c1.den + ' - (' + c2.cod + ') ' + c2.den" & vbCrLf
sConsulta = sConsulta & "                                  end" & vbCrLf
sConsulta = sConsulta & "                             else" & vbCrLf
sConsulta = sConsulta & "                             '(' + c1.cod + ') ' + c1.den + ' - (' + c2.cod + ') ' + c2.den + ' - (' + c3.cod + ') ' + c3.den" & vbCrLf
sConsulta = sConsulta & "                             end" & vbCrLf
sConsulta = sConsulta & "                        else" & vbCrLf
sConsulta = sConsulta & "                        '(' + c1.cod + ') ' + c1.den + ' - (' + c2.cod + ') ' + c2.den + ' - (' + c3.cod + ') ' + c3.den + ' - (' + c4.cod + ') ' + c4.den" & vbCrLf
sConsulta = sConsulta & "                        end" & vbCrLf
sConsulta = sConsulta & "                    else" & vbCrLf
sConsulta = sConsulta & "                    '(' + c1.cod + ') ' + c1.den + ' - (' + c2.cod + ') ' + c2.den + ' - (' + c3.cod + ') ' + c3.den + ' - (' + c4.cod + ') ' + c4.den + ' - (' + c5.cod + ') ' + c5.den" & vbCrLf
sConsulta = sConsulta & "                    end CATEGORIARAMA," & vbCrLf
sConsulta = sConsulta & "               " & vbCrLf
sConsulta = sConsulta & "                        " & vbCrLf
sConsulta = sConsulta & "                C1.DEN CAT1DEN," & vbCrLf
sConsulta = sConsulta & "                C2.DEN CAT2DEN," & vbCrLf
sConsulta = sConsulta & "                C3.DEN CAT3DEN," & vbCrLf
sConsulta = sConsulta & "                C4.DEN CAT4DEN," & vbCrLf
sConsulta = sConsulta & "                C5.DEN CAT5DEN," & vbCrLf
sConsulta = sConsulta & "                LE.COMENT," & vbCrLf
sConsulta = sConsulta & "                I.ANYO ANYOPROCE," & vbCrLf
sConsulta = sConsulta & "                I.PROCE CODPROCE," & vbCrLf
sConsulta = sConsulta & "                I.ID ITEM,LPA.ADJUNTOS," & vbCrLf
sConsulta = sConsulta & "                CASE WHEN @MOSTRARCINTERNO IS NULL OR @MOSTRARCINTERNO=1 OR CC1.INTERNO=0 THEN LP.VALOR1 ELSE NULL END VALOR1," & vbCrLf
sConsulta = sConsulta & "                CASE WHEN @MOSTRARCINTERNO IS NULL OR @MOSTRARCINTERNO=1 OR CC2.INTERNO=0 THEN LP.VALOR2 ELSE NULL END VALOR2," & vbCrLf
sConsulta = sConsulta & "                CC1.DEN AS DENCAMPO1,CC2.DEN AS DENCAMPO2,PROCE.DEN AS DENPROCE,LP.OBSADJUN,LP.FECENTREGA" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & " FROM LINEAS_PEDIDO LP" & vbCrLf
sConsulta = sConsulta & "     INNER JOIN DEST D" & vbCrLf
sConsulta = sConsulta & "             ON LP.DEST = D.COD" & vbCrLf
sConsulta = sConsulta & "     INNER JOIN UNI U" & vbCrLf
sConsulta = sConsulta & "             ON LP.UP = U.COD" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN ITEM I " & vbCrLf
sConsulta = sConsulta & "             ON LP.ANYO = I.ANYO" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN1=I.GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND LP.PROCE=I.PROCE " & vbCrLf
sConsulta = sConsulta & "            AND LP.ITEM=I.ID" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN PROVE_ART4 PA" & vbCrLf
sConsulta = sConsulta & "             ON LP.PROVE = PA.PROVE" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN1 = PA.GMN1" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN2 = PA.GMN2" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN3 = PA.GMN3" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN4 = PA.GMN4" & vbCrLf
sConsulta = sConsulta & "            AND CASE WHEN LP.ART_INT IS NULL THEN I.ART ELSE LP.ART_INT END = PA.ART" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN ART4 " & vbCrLf
sConsulta = sConsulta & "             ON LP.GMN1=ART4.GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN2=ART4.GMN2 " & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN3=ART4.GMN3 " & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN4=ART4.GMN4 " & vbCrLf
sConsulta = sConsulta & "            AND LP.ART_INT=ART4.COD" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN ITEM_ADJ IA " & vbCrLf
sConsulta = sConsulta & "             ON LP.ITEM = IA.ITEM " & vbCrLf
sConsulta = sConsulta & "            AND LP.ANYO = IA.ANYO" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN1 = IA.GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND LP.PROCE= IA.PROCE " & vbCrLf
sConsulta = sConsulta & "            AND LP.PROVE= IA.PROVE" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN ART4 ART_AUX " & vbCrLf
sConsulta = sConsulta & "             ON ART_AUX.COD=I.ART " & vbCrLf
sConsulta = sConsulta & "            AND ART_AUX.GMN1=I.GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND ART_AUX.GMN2=I.GMN2 " & vbCrLf
sConsulta = sConsulta & "            AND ART_AUX.GMN3=I.GMN3 " & vbCrLf
sConsulta = sConsulta & "            AND ART_AUX.GMN4=I.GMN4" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN CATN1 C1" & vbCrLf
sConsulta = sConsulta & "             ON LP.CAT1 = C1.ID" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN CATN2 C2" & vbCrLf
sConsulta = sConsulta & "             ON LP.CAT2 = C2.ID" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN CATN3 C3" & vbCrLf
sConsulta = sConsulta & "             ON LP.CAT3 = C3.ID" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN CATN4 C4" & vbCrLf
sConsulta = sConsulta & "             ON LP.CAT4 = C4.ID" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN CATN5 C5" & vbCrLf
sConsulta = sConsulta & "             ON LP.CAT5 = C5.ID" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN LINEAS_EST LE" & vbCrLf
sConsulta = sConsulta & "             ON LP.LINEAS_EST = LE.ID" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN " & vbCrLf
sConsulta = sConsulta & "   (SELECT COUNT(*) AS ADJUNTOS,LINEA  FROM LINEAS_PEDIDO_ADJUN GROUP BY LINEAS_PEDIDO_ADJUN.LINEA) LPA ON LP.ID= LPA.LINEA " & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN CATALOG_CAMPOS AS CC1 " & vbCrLf
sConsulta = sConsulta & "             ON CC1.ID=LP.CAMPO1 AND CC1.IDIOMA=@IDI" & vbCrLf
sConsulta = sConsulta & "            AND CC1.INTERNO = CASE WHEN @MOSTRARCINTERNO IS NULL OR @MOSTRARCINTERNO=1 THEN CC1.INTERNO ELSE 0 END" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN CATALOG_CAMPOS AS CC2 " & vbCrLf
sConsulta = sConsulta & "            ON CC2.ID=LP.CAMPO2 AND CC2.IDIOMA=@IDI" & vbCrLf
sConsulta = sConsulta & "           AND CC2.INTERNO = CASE WHEN @MOSTRARCINTERNO IS NULL OR @MOSTRARCINTERNO=1 THEN CC2.INTERNO ELSE 0 END" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN PROCE " & vbCrLf
sConsulta = sConsulta & "       ON LP.ANYO=PROCE.ANYO AND LP.GMN1=PROCE.GMN1 AND LP.PROCE=PROCE.COD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & " WHERE LP.ORDEN=@ORDENID" & vbCrLf
sConsulta = sConsulta & "   AND CASE WHEN LP.SOLICIT IS NULL AND @SOLICIT IS NULL THEN -1 ELSE LP.SOLICIT END " & vbCrLf
sConsulta = sConsulta & "       = CASE WHEN LP.SOLICIT IS NULL AND @SOLICIT IS NULL THEN -1 ELSE ISNULL(@SOLICIT,LP.SOLICIT) END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE PROCEDURE FSEP_DEVOLVER_RECEPTORES @PER VARCHAR(50) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "declare @UON1 VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "declare @UON2 VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "declare @UON3 VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF (SELECT DEST_OTRAS_UO FROM USU WHERE PER = @PER)=1" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    select @UON1 = uon1, @UON2 = uon2, @UON3 = uon3 from per where cod = @PER" & vbCrLf
sConsulta = sConsulta & "    IF @UON3 IS NULL" & vbCrLf
sConsulta = sConsulta & "      IF @UON2 IS NULL" & vbCrLf
sConsulta = sConsulta & "        SELECT P.* FROM PER P" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN USU U" & vbCrLf
sConsulta = sConsulta & "                   ON P.COD = U.PER" & vbCrLf
sConsulta = sConsulta & "         WHERE U.FSEP = 1" & vbCrLf
sConsulta = sConsulta & "           AND P.UON1 = @UON1" & vbCrLf
sConsulta = sConsulta & "      ELSE" & vbCrLf
sConsulta = sConsulta & "        SELECT P.* FROM PER P" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN USU U" & vbCrLf
sConsulta = sConsulta & "                   ON P.COD = U.PER" & vbCrLf
sConsulta = sConsulta & "         WHERE U.FSEP = 1" & vbCrLf
sConsulta = sConsulta & "           AND P.UON1 = @UON1" & vbCrLf
sConsulta = sConsulta & "           AND P.UON2 = @UON2" & vbCrLf
sConsulta = sConsulta & "    ELSE" & vbCrLf
sConsulta = sConsulta & "      SELECT P.* FROM PER P" & vbCrLf
sConsulta = sConsulta & "         INNER JOIN USU U" & vbCrLf
sConsulta = sConsulta & "                 ON P.COD = U.PER" & vbCrLf
sConsulta = sConsulta & "       WHERE U.FSEP = 1" & vbCrLf
sConsulta = sConsulta & "         AND P.UON1 = @UON1" & vbCrLf
sConsulta = sConsulta & "         AND P.UON2 = @UON2" & vbCrLf
sConsulta = sConsulta & "         AND P.UON3 = @UON3" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  SELECT P.* FROM PER P" & vbCrLf
sConsulta = sConsulta & "     INNER JOIN USU U" & vbCrLf
sConsulta = sConsulta & "             ON P.COD = U.PER" & vbCrLf
sConsulta = sConsulta & "   WHERE U.FSEP = 1" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET QUOTED_IDENTIFIER OFF " & vbCrLf
ExecuteSQL gRDOCon, sConsulta
sConsulta = ""
sConsulta = sConsulta & "SET ANSI_NULLS ON " & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion2_15_00_01A2_15_00_02() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim bBool As Boolean

On Error GoTo error
    

sConsulta = "BEGIN TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = True
ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

frmProgreso.lblDetalle = "Modificar tablas"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

    V_2_15_Tablas_02

frmProgreso.lblDetalle = "Modificar triggers"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    
    V_2_15_Triggers_02
    
frmProgreso.lblDetalle = "Modificar stored procedures"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    
    V_2_15_Storeds_02

    
    sConsulta = "UPDATE VERSION SET NUM ='2.15.00.02'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta


sConsulta = "COMMIT TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = False

CodigoDeActualizacion2_15_00_01A2_15_00_02 = Not bCancelado
Exit Function
    
error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_15_00_01A2_15_00_02 = False

End Function

Private Sub V_2_15_Triggers_02()
Dim sConsulta As String
sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ORDEN_EST_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[ORDEN_EST_TG_INS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TRIGGER ORDEN_EST_TG_INS ON dbo.ORDEN_EST " & vbCrLf
sConsulta = sConsulta & "FOR INSERT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_ORDEN_EST_Ins CURSOR LOCAL FOR SELECT EST,ID,ORDEN,PER,COMENT,EXTERNO FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_ORDEN_EST_Ins" & vbCrLf
sConsulta = sConsulta & "DECLARE @EST AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_ORDEN AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @SENTIDO AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PER AS VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @USU AS VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @EXTERNO AS BIT" & vbCrLf
sConsulta = sConsulta & "DECLARE @TIPO AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ACTIVA AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ACTIVLOG AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ORIGEN AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @COMENT AS VARCHAR(255)" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_LOG_ORDEN_ENTREGA AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVE AS VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PAG AS VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @INTEGRACION AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ACCION AS VARCHAR(2)" & vbCrLf
sConsulta = sConsulta & "DECLARE @TRASPASO_ERP AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ERP  AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @NOGRABAR AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @TRASLADO_APROV_ERP AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @COD_ERP AS VARCHAR(200)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_ORDEN_EST_Ins INTO @EST,@ID,@ID_ORDEN,@PER,@COMENT,@EXTERNO" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "  IF @EXTERNO IS NULL" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "    IF (@EST = 2 OR @EST = 3 OR @EST = 4 OR @EST = 20 OR @EST = 21) " & vbCrLf
sConsulta = sConsulta & "      BEGIN -- Si es uno de los estados que nos interesan para grabar en LOG_ORDEN_ENTREGA (emitido,aceptado,rechazado,anulado,en camino)" & vbCrLf
sConsulta = sConsulta & "        -- Recuperamos el tipo de orden de entrega para ver si est� activada su integraci�n" & vbCrLf
sConsulta = sConsulta & "        SET @COD_ERP=NULL" & vbCrLf
sConsulta = sConsulta & "        SELECT  @TIPO=TIPO,@PROVE=PROVE, @COD_ERP=COD_PROVE_ERP FROM ORDEN_ENTREGA WHERE ORDEN_ENTREGA.ID = @ID_ORDEN" & vbCrLf
sConsulta = sConsulta & "        SELECT  @ACTIVLOG = ACTIVLOG FROM PARGEN_GEST WHERE ID = 1" & vbCrLf
sConsulta = sConsulta & "        SELECT  @INTEGRACION =INTEGRACION, @TRASLADO_APROV_ERP=TRASLADO_APROV_ERP FROM PARGEN_INTERNO WHERE ID = 1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "        IF @TIPO = 0 AND @EST = 2  --Emisi�n de pedidos directos" & vbCrLf
sConsulta = sConsulta & "             SELECT  @TRASPASO_ERP= TRASPASO_PED_ERP FROM PARGEN_INTERNO WHERE ID = 1" & vbCrLf
sConsulta = sConsulta & "        ELSE" & vbCrLf
sConsulta = sConsulta & "             SET @TRASPASO_ERP=0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "        IF @INTEGRACION = 0" & vbCrLf
sConsulta = sConsulta & "             SET @ACTIVA = 0" & vbCrLf
sConsulta = sConsulta & "        ELSE " & vbCrLf
sConsulta = sConsulta & "             BEGIN" & vbCrLf
sConsulta = sConsulta & "             IF @TIPO = 0 --Pedidos directos" & vbCrLf
sConsulta = sConsulta & "                 SELECT @ACTIVA=ACTIVA,@SENTIDO=SENTIDO FROM TABLAS_INTEGRACION WHERE ID = 13" & vbCrLf
sConsulta = sConsulta & "             ELSE" & vbCrLf
sConsulta = sConsulta & "                 SELECT @ACTIVA=ACTIVA,@SENTIDO=SENTIDO FROM TABLAS_INTEGRACION WHERE ID = 11" & vbCrLf
sConsulta = sConsulta & "             END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "        IF (@ACTIVA = 1 AND (@SENTIDO=3 OR @SENTIDO=1)) OR @ACTIVLOG = 1 OR @TRASPASO_ERP=1 OR @TRASLADO_APROV_ERP = 1" & vbCrLf
sConsulta = sConsulta & "          -- si el sentido es = 2  (solo entrada) o no est� activa la integraci�n  no grabamos en log" & vbCrLf
sConsulta = sConsulta & "          BEGIN" & vbCrLf
sConsulta = sConsulta & "            SET @NOGRABAR=0" & vbCrLf
sConsulta = sConsulta & "            IF @TRASPASO_ERP=1  OR @TRASLADO_APROV_ERP = 1 --emisi�n de pedidos directos y de aprovisionamiento" & vbCrLf
sConsulta = sConsulta & "                BEGIN" & vbCrLf
sConsulta = sConsulta & "                SET @ERP=1" & vbCrLf
sConsulta = sConsulta & "                IF @ACTIVLOG = 1 -- log de actividad" & vbCrLf
sConsulta = sConsulta & "                   SET @ORIGEN = 1" & vbCrLf
sConsulta = sConsulta & "                ELSE" & vbCrLf
sConsulta = sConsulta & "                   SET @NOGRABAR=1" & vbCrLf
sConsulta = sConsulta & "                END" & vbCrLf
sConsulta = sConsulta & "            ELSE" & vbCrLf
sConsulta = sConsulta & "                begin" & vbCrLf
sConsulta = sConsulta & "                SET @ERP=0" & vbCrLf
sConsulta = sConsulta & "                IF @ACTIVLOG = 1 " & vbCrLf
sConsulta = sConsulta & "                     begin" & vbCrLf
sConsulta = sConsulta & "                     IF @ACTIVA  = 1" & vbCrLf
sConsulta = sConsulta & "                             SET @ORIGEN = 2 -- Integracion y log de actividad" & vbCrLf
sConsulta = sConsulta & "                     ELSE" & vbCrLf
sConsulta = sConsulta & "                             SET @ORIGEN = 1 -- solo log de actividad" & vbCrLf
sConsulta = sConsulta & "                     end" & vbCrLf
sConsulta = sConsulta & "                 ELSE" & vbCrLf
sConsulta = sConsulta & "                     BEGIN" & vbCrLf
sConsulta = sConsulta & "                      IF @ACTIVA = 1" & vbCrLf
sConsulta = sConsulta & "                          SET @ORIGEN = 0 -- solo integracion" & vbCrLf
sConsulta = sConsulta & "                      ELSE" & vbCrLf
sConsulta = sConsulta & "                          SET @NOGRABAR=1" & vbCrLf
sConsulta = sConsulta & "                     END" & vbCrLf
sConsulta = sConsulta & "                end" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "            IF @PER IS NULL" & vbCrLf
sConsulta = sConsulta & "                SET @USU = NULL" & vbCrLf
sConsulta = sConsulta & "            ELSE" & vbCrLf
sConsulta = sConsulta & "                SET @USU = (SELECT COD FROM USU WHERE PER = @PER)" & vbCrLf
sConsulta = sConsulta & "            SET @PAG = (SELECT PAG FROM PROVE WHERE COD = @PROVE)" & vbCrLf
sConsulta = sConsulta & "            -- la acci�n ser� E - Cambio de estado o I Creaci�n orden de entrega)" & vbCrLf
sConsulta = sConsulta & "            IF @EST = 2  " & vbCrLf
sConsulta = sConsulta & "              SET @ACCION='I'" & vbCrLf
sConsulta = sConsulta & "            ELSE " & vbCrLf
sConsulta = sConsulta & "              BEGIN" & vbCrLf
sConsulta = sConsulta & "                IF @EST=20 " & vbCrLf
sConsulta = sConsulta & "                  SET @ACCION='D'" & vbCrLf
sConsulta = sConsulta & "                ELSE" & vbCrLf
sConsulta = sConsulta & "                  SET @ACCION='E'" & vbCrLf
sConsulta = sConsulta & "              END" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "            IF @NOGRABAR=0" & vbCrLf
sConsulta = sConsulta & "              BEGIN" & vbCrLf
sConsulta = sConsulta & "                INSERT INTO LOG_ORDEN_ENTREGA (ACCION,ID_ORDEN_ENTREGA,ID_ORDEN_EST,ID_PEDIDO,ANYO,PROVE,NUM_PEDIDO,NUM_ORDEN,NUMEXT,TIPO,FECHA,IMPORTE,PAG,EST,INCORRECTA,COMENT,ORIGEN,PER,USU,MON,CAMBIO,EMPRESA,RECEPTOR,COD_PROVE_ERP)" & vbCrLf
sConsulta = sConsulta & "                SELECT @ACCION,@ID_ORDEN,@ID,PEDIDO,ORDEN_ENTREGA.ANYO,PROVE,PEDIDO.NUM,ORDEN_ENTREGA.NUM,NUMEXT,ORDEN_ENTREGA.TIPO,ORDEN_ENTREGA.FECHA,IMPORTE,@PAG,@EST,INCORRECTA,@COMENT,@ORIGEN,@PER,@USU, MON,CAMBIO,EMPRESA,RECEPTOR,COD_PROVE_ERP FROM ORDEN_ENTREGA INNER JOIN PEDIDO ON PEDIDO.ID = ORDEN_ENTREGA.PEDIDO WHERE ORDEN_ENTREGA.ID = @ID_ORDEN" & vbCrLf
sConsulta = sConsulta & "                SET @ID_LOG_ORDEN_ENTREGA = (SELECT MAX(ID) FROM LOG_ORDEN_ENTREGA)" & vbCrLf
sConsulta = sConsulta & "                -- En el caso de Emisi�n de Pedido Directo, todav�a no estar�n creadas las lineas de pedido, se hace en el trigger LINEAS_PEDIDO_TG_INS" & vbCrLf
sConsulta = sConsulta & "                INSERT INTO LOG_LINEAS_PEDIDO (ACCION,ID_LOG_ORDEN_ENTREGA,ID_LINEAS_PEDIDO,ID_ORDEN_EST,ART_INT,UP,PREC_UP,CANT_PED,DEST,ANYO,GMN1,GMN2,GMN3,GMN4,PROCE,ITEM,ENTREGA_OBL,FECENTREGA,FECENTREGAPROVE,EST,CAT1,CAT2,CAT3,CAT4,CAT5,OBS,FC)" & vbCrLf
sConsulta = sConsulta & "                SELECT @ACCION,@ID_LOG_ORDEN_ENTREGA,ID,@ID,ART_INT,UP,PREC_UP,CANT_PED,DEST,ANYO,GMN1,GMN2,GMN3,GMN4,PROCE,ITEM,ENTREGA_OBL,FECENTREGA,FECENTREGAPROVE,EST,CAT1,CAT2,CAT3,CAT4,CAT5,OBS,FC FROM LINEAS_PEDIDO WHERE  ORDEN = @ID_ORDEN" & vbCrLf
sConsulta = sConsulta & "              END" & vbCrLf
sConsulta = sConsulta & "            IF @ERP=1 " & vbCrLf
sConsulta = sConsulta & "              BEGIN" & vbCrLf
sConsulta = sConsulta & "                IF @COD_ERP IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                    BEGIN" & vbCrLf
sConsulta = sConsulta & "                    INSERT INTO LOG_ORDEN_ENTREGA (ACCION,ID_ORDEN_ENTREGA,ID_ORDEN_EST,ID_PEDIDO,ANYO,PROVE,NUM_PEDIDO,NUM_ORDEN,NUMEXT,TIPO,FECHA,IMPORTE,PAG,EST,INCORRECTA,COMENT,ORIGEN,PER,USU,MON,CAMBIO,EMPRESA,RECEPTOR,COD_PROVE_ERP)" & vbCrLf
sConsulta = sConsulta & "                        SELECT @ACCION,@ID_ORDEN,@ID,PEDIDO,ORDEN_ENTREGA.ANYO,PROVE,PEDIDO.NUM,ORDEN_ENTREGA.NUM,NUMEXT,ORDEN_ENTREGA.TIPO,ORDEN_ENTREGA.FECHA,IMPORTE,@PAG,@EST,INCORRECTA,@COMENT,0,@PER,@USU, MON,CAMBIO,EMPRESA,RECEPTOR,COD_PROVE_ERP FROM ORDEN_ENTREGA INNER JOIN PEDIDO ON PEDIDO.ID = ORDEN_ENTREGA.PEDIDO WHERE ORDEN_ENTREGA.ID = @ID_ORDEN" & vbCrLf
sConsulta = sConsulta & "                    SET @ID_LOG_ORDEN_ENTREGA = (SELECT MAX(ID) FROM LOG_ORDEN_ENTREGA)" & vbCrLf
sConsulta = sConsulta & "                    INSERT INTO LOG_LINEAS_PEDIDO (ACCION,ID_LOG_ORDEN_ENTREGA,ID_LINEAS_PEDIDO,ID_ORDEN_EST,ART_INT,UP,PREC_UP,CANT_PED,DEST,ANYO,GMN1,GMN2,GMN3,GMN4,PROCE,ITEM,ENTREGA_OBL,FECENTREGA,FECENTREGAPROVE,EST,CAT1,CAT2,CAT3,CAT4,CAT5,OBS,FC)" & vbCrLf
sConsulta = sConsulta & "                        SELECT @ACCION,@ID_LOG_ORDEN_ENTREGA,ID,@ID,ART_INT,UP,PREC_UP,CANT_PED,DEST,ANYO,GMN1,GMN2,GMN3,GMN4,PROCE,ITEM,ENTREGA_OBL,FECENTREGA,FECENTREGAPROVE,EST,CAT1,CAT2,CAT3,CAT4,CAT5,OBS,FC FROM LINEAS_PEDIDO WHERE  ORDEN = @ID_ORDEN" & vbCrLf
sConsulta = sConsulta & "                    END" & vbCrLf
sConsulta = sConsulta & "              END" & vbCrLf
sConsulta = sConsulta & "          END --IF ACTIVA" & vbCrLf
sConsulta = sConsulta & "      END -- IF EST" & vbCrLf
sConsulta = sConsulta & "   END -- IF @EXTERNO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  UPDATE ORDEN_ENTREGA SET ORDEN_EST = @ID WHERE ORDEN_ENTREGA.ID = @ID_ORDEN" & vbCrLf
sConsulta = sConsulta & "  FETCH NEXT FROM curTG_ORDEN_EST_Ins INTO @EST,@ID,@ID_ORDEN,@PER,@COMENT,@EXTERNO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CLOSE curTG_ORDEN_EST_Ins" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_ORDEN_EST_Ins" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PROVE_ERP_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PROVE_ERP_TG_INS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TRIGGER PROVE_ERP_TG_INS ON dbo.PROVE_ERP " & vbCrLf
sConsulta = sConsulta & "FOR INSERT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO PROVE_EMPRESA_COD_ERP (PROVE, COD_ERP, EMPRESA) SELECT  P.COD,I.COD_ERP, I.EMPRESA FROM INSERTED I INNER JOIN PROVE P ON I.NIF = P.NIF" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ERP  SET FECACT = GETDATE() FROM INSERTED I INNER JOIN PROVE_ERP PE ON PE.COD_ERP = I.COD_ERP and PE.EMPRESA=I.EMPRESA" & vbCrLf
sConsulta = sConsulta & ""
ExecuteSQL gRDOCon, sConsulta



End Sub

Private Sub V_2_15_Tablas_02()
Dim sConsulta As String

sConsulta = "ALTER TABLE dbo.PROVE_ERP ADD" & vbCrLf
sConsulta = sConsulta & "   [FECACT] [datetime] NULL " & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TRIGGER PROVE_ERP_TG_INSUPD ON dbo.PROVE_ERP" & vbCrLf
sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @COD_ERP NVARCHAR(200) " & vbCrLf
sConsulta = sConsulta & "DECLARE @EMPRESA INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_Prove_erp_Ins CURSOR FOR SELECT COD_ERP,EMPRESA FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_Prove_erp_Ins" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_Prove_erp_Ins INTO @COD_ERP,@EMPRESA" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ERP SET FECACT=GETDATE() WHERE COD_ERP=@COD_ERP AND EMPRESA=@EMPRESA" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_Prove_erp_Ins INTO @COD_ERP,@EMPRESA" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_Prove_erp_Ins" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_Prove_erp_Ins" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Private Sub V_2_15_Storeds_02()
Dim sConsulta As String
sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_CONTAR_ORDENES]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_CONTAR_ORDENES]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSEP_CONTAR_ORDENES(@APROBADOR VARCHAR(50), @MAXLINEAS INTEGER, @DESDEID INTEGER," & vbCrLf
sConsulta = sConsulta & "@ANYO INTEGER, @NUMPEDIDO INTEGER, @ORDEN INTEGER, @APROBADOS INTEGER,@DENEGADOS INTEGER,@CODPROVE VARCHAR(50),@NUMPEDIDOPROVE VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & ",@DESDEFECHA VARCHAR(10), @APROVISIONADOR VARCHAR(50) ,@REFERENCIA varchar(120) = null ,@CAMPO1 int = null ,@CAMPO2 int = null," & vbCrLf
sConsulta = sConsulta & " @VALORCAMPO1 varchar(150) = null, @VALORCAMPO2 varchar(150)=null , @CATN1 int = null, @CATN2 int = null, @CATN3 int = null, @CATN4 int = null , @CATN5 int = null ," & vbCrLf
sConsulta = sConsulta & " @EMPRESA INT = NULL, @RECEPTOR VARCHAR(100) = NULL, @COD_ERP  VARCHAR(100) = NULL, @HASTAFECHA VARCHAR(10))  AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQLSTRING NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @INDICE INTEGER" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING= 'SELECT DISTINCT ORDEN_ENTREGA.ID FROM LINEAS_PEDIDO INNER JOIN SEGURIDAD" & vbCrLf
sConsulta = sConsulta & "ON LINEAS_PEDIDO.SEGURIDAD=SEGURIDAD.ID AND APROB_TIPO1='''+ @APROBADOR +''' AND LINEAS_PEDIDO.NIVEL_APROB=0'" & vbCrLf
sConsulta = sConsulta & "IF @APROBADOS=0 AND @DENEGADOS <> 0" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND LINEAS_PEDIDO.EST IN (0,20)' " & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & " IF @APROBADOS<>0 AND @DENEGADOS=0" & vbCrLf
sConsulta = sConsulta & "     SET @SQLSTRING= @SQLSTRING + ' AND LINEAS_PEDIDO.EST IN (0,1,2,3)'" & vbCrLf
sConsulta = sConsulta & " ELSE            " & vbCrLf
sConsulta = sConsulta & "     IF @APROBADOS=0 AND @DENEGADOS=0" & vbCrLf
sConsulta = sConsulta & "         SET @SQLSTRING= @SQLSTRING + ' AND LINEAS_PEDIDO.EST =0 '" & vbCrLf
sConsulta = sConsulta & "IF @CAMPO1 is NOT NULL and @VALORCAMPO1 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQLSTRING = @SQLSTRING + ' AND LINEAS_PEDIDO.CAMPO1 = ' + CONVERT(VARCHAR,@CAMPO1) + N'   AND LINEAS_PEDIDO.VALOR1 = ''' + @VALORCAMPO1 +''''" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CAMPO2 is NOT NULL and @VALORCAMPO2 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQLSTRING = @SQLSTRING + ' AND LINEAS_PEDIDO.CAMPO2 = ' + CONVERT(VARCHAR,@CAMPO2) + N'   AND LINEAS_PEDIDO.VALOR2 = ''' + @VALORCAMPO2 + ''''" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CATN1 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQLSTRING = @SQLSTRING + N'   AND LINEAS_PEDIDO.CAT1 = ' + CONVERT(VARCHAR,@CATN1) " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CATN2 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQLSTRING = @SQLSTRING + N'   AND LINEAS_PEDIDO.CAT2 = ' + CONVERT(VARCHAR,@CATN2) " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CATN3 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQLSTRING = @SQLSTRING + N'   AND LINEAS_PEDIDO.CAT3 = ' + CONVERT(VARCHAR,@CATN3) " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CATN4 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQLSTRING = @SQLSTRING + N'   AND LINEAS_PEDIDO.CAT4 = ' + CONVERT(VARCHAR,@CATN4) " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CATN5 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQLSTRING = @SQLSTRING + N'   AND LINEAS_PEDIDO.CAT5 = ' + CONVERT(VARCHAR,@CATN5) " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING= @SQLSTRING + ' INNER JOIN ORDEN_ENTREGA ON LINEAS_PEDIDO.ORDEN=ORDEN_ENTREGA.ID'" & vbCrLf
sConsulta = sConsulta & "IF @ORDEN<>0" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND ORDEN_ENTREGA.NUM=' +CONVERT(VARCHAR,@ORDEN )" & vbCrLf
sConsulta = sConsulta & "if @REFERENCIA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQLSTRING = @SQLSTRING + '   AND ORDEN_ENTREGA.REFERENCIA = ''' + @REFERENCIA + ''''" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING= @SQLSTRING + ' INNER JOIN PEDIDO ON ORDEN_ENTREGA.PEDIDO=PEDIDO.ID '" & vbCrLf
sConsulta = sConsulta & "IF @NUMPEDIDO <>0 " & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND PEDIDO.NUM=' +CONVERT(VARCHAR,@NUMPEDIDO )" & vbCrLf
sConsulta = sConsulta & "IF @APROVISIONADOR <>''" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND PEDIDO.PER=''' + @APROVISIONADOR +''' '" & vbCrLf
sConsulta = sConsulta & "if @ANYO<>0" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING=@SQLSTRING + ' AND PEDIDO.ANYO =' + CONVERT(VARCHAR,@ANYO) " & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING=@SQLSTRING + '  INNER JOIN PROVE ON ORDEN_ENTREGA.PROVE=PROVE.COD INNER JOIN PER ON PER.COD=PEDIDO.PER '" & vbCrLf
sConsulta = sConsulta & "IF @DESDEID <> 0 " & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND ORDEN_ENTREGA.ID >=' + CONVERT(VARCHAR,@DESDEID) " & vbCrLf
sConsulta = sConsulta & "IF @CODPROVE <>'' " & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND ORDEN_ENTREGA.PROVE =''' + @CODPROVE + ''''" & vbCrLf
sConsulta = sConsulta & "IF @NUMPEDIDOPROVE is not null" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND ORDEN_ENTREGA.NUMEXT = ''' + @NUMPEDIDOPROVE + ''''" & vbCrLf
sConsulta = sConsulta & "IF @DESDEFECHA is not null" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND ORDEN_ENTREGA.FECHA >= CONVERT(DATETIME,''' +  @DESDEFECHA + ''',103)'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if @HASTAFECHA is not null" & vbCrLf
sConsulta = sConsulta & "   SET @SQLSTRING = @SQLSTRING + N'   AND ORDEN_ENTREGA.FECHA <= CONVERT(datetime,''' + @HASTAFECHA + ''',103)'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @EMPRESA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQLSTRING = @SQLSTRING + N'   AND ORDEN_ENTREGA.EMPRESA=' +CONVERT(VARCHAR,@EMPRESA )" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @RECEPTOR IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQLSTRING = @SQLSTRING + N'   AND ORDEN_ENTREGA.RECEPTOR  = ''' + @RECEPTOR + '''' " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @COD_ERP IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQLSTRING = @SQLSTRING + N'   AND ORDEN_ENTREGA.COD_PROVE_ERP = ''' +  @COD_ERP + ''''" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING= @SQLSTRING + ' UNION SELECT DISTINCT ORDEN_ENTREGA.ID FROM LINEAS_PEDIDO INNER JOIN APROB_LIM" & vbCrLf
sConsulta = sConsulta & "ON LINEAS_PEDIDO.APROB_LIM=APROB_LIM.ID AND APROB_LIM.PER='''+ @APROBADOR +''''" & vbCrLf
sConsulta = sConsulta & "IF @APROBADOS=0 AND @DENEGADOS <> 0" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND LINEAS_PEDIDO.EST IN (0,20)' " & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & " IF @APROBADOS<>0 AND @DENEGADOS=0" & vbCrLf
sConsulta = sConsulta & "     SET @SQLSTRING= @SQLSTRING + ' AND LINEAS_PEDIDO.EST IN (0,1,2,3)'" & vbCrLf
sConsulta = sConsulta & " ELSE" & vbCrLf
sConsulta = sConsulta & "     IF @APROBADOS=0 AND @DENEGADOS=0    " & vbCrLf
sConsulta = sConsulta & "         SET @SQLSTRING= @SQLSTRING + ' AND LINEAS_PEDIDO.EST =0 '" & vbCrLf
sConsulta = sConsulta & "IF @CAMPO1 is NOT NULL and @VALORCAMPO1 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQLSTRING = @SQLSTRING + ' AND LINEAS_PEDIDO.CAMPO1 = ' + CONVERT(VARCHAR,@CAMPO1) + N'   AND LINEAS_PEDIDO.VALOR1 = ''' + @VALORCAMPO1 +''''" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CAMPO2 is NOT NULL and @VALORCAMPO2 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQLSTRING = @SQLSTRING + ' AND LINEAS_PEDIDO.CAMPO2 = ' + CONVERT(VARCHAR,@CAMPO2) + N'   AND LINEAS_PEDIDO.VALOR2 = ''' + @VALORCAMPO2 + ''''" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CATN1 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQLSTRING = @SQLSTRING + N'   AND LINEAS_PEDIDO.CAT1 = ' + CONVERT(VARCHAR,@CATN1) " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CATN2 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQLSTRING = @SQLSTRING + N'   AND LINEAS_PEDIDO.CAT2 = ' + CONVERT(VARCHAR,@CATN2) " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CATN3 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQLSTRING = @SQLSTRING + N'   AND LINEAS_PEDIDO.CAT3 = ' + CONVERT(VARCHAR,@CATN3) " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CATN4 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQLSTRING = @SQLSTRING + N'   AND LINEAS_PEDIDO.CAT4 = ' + CONVERT(VARCHAR,@CATN4)  " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @CATN5 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQLSTRING = @SQLSTRING + N'   AND LINEAS_PEDIDO.CAT5 = ' + CONVERT(VARCHAR,@CATN5) " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING= @SQLSTRING + ' INNER JOIN ORDEN_ENTREGA ON LINEAS_PEDIDO.ORDEN=ORDEN_ENTREGA.ID '" & vbCrLf
sConsulta = sConsulta & "IF @ORDEN<>0" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND ORDEN_ENTREGA.NUM=' +CONVERT(VARCHAR,@ORDEN )" & vbCrLf
sConsulta = sConsulta & "if @REFERENCIA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQLSTRING = @SQLSTRING + '   AND ORDEN_ENTREGA.REFERENCIA = ''' + @REFERENCIA + ''''" & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING= @SQLSTRING + ' INNER JOIN PEDIDO ON ORDEN_ENTREGA.PEDIDO=PEDIDO.ID '" & vbCrLf
sConsulta = sConsulta & "IF @NUMPEDIDO <>0 " & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND PEDIDO.NUM=' +CONVERT(VARCHAR,@NUMPEDIDO )" & vbCrLf
sConsulta = sConsulta & "IF @APROVISIONADOR <>''" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND PEDIDO.PER=''' + @APROVISIONADOR +''' '" & vbCrLf
sConsulta = sConsulta & "if @ANYO<>0" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING=@SQLSTRING + ' AND PEDIDO.ANYO =' + CONVERT(VARCHAR,@ANYO) " & vbCrLf
sConsulta = sConsulta & "SET @SQLSTRING=@SQLSTRING + '  INNER JOIN PROVE ON ORDEN_ENTREGA.PROVE=PROVE.COD INNER JOIN PER ON PER.COD=PEDIDO.PER WHERE 1=1 '" & vbCrLf
sConsulta = sConsulta & "IF @DESDEID <> 0 " & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND ORDEN_ENTREGA.ID >=' + CONVERT(VARCHAR,@DESDEID) " & vbCrLf
sConsulta = sConsulta & "IF @CODPROVE <>''" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND ORDEN_ENTREGA.PROVE =''' + @CODPROVE + ''''" & vbCrLf
sConsulta = sConsulta & "IF @NUMPEDIDOPROVE <> ''" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND ORDEN_ENTREGA.NUMEXT = ''' + @NUMPEDIDOPROVE + ''''" & vbCrLf
sConsulta = sConsulta & "IF @DESDEFECHA is not null" & vbCrLf
sConsulta = sConsulta & " SET @SQLSTRING= @SQLSTRING + ' AND ORDEN_ENTREGA.FECHA >= CONVERT(DATETIME,''' + @DESDEFECHA + ''',103)'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if @HASTAFECHA is not null" & vbCrLf
sConsulta = sConsulta & "   SET @SQLSTRING = @SQLSTRING + N'   AND ORDEN_ENTREGA.FECHA <= CONVERT(datetime,''' + @HASTAFECHA + ''',103)'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @EMPRESA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQLSTRING = @SQLSTRING + N'   AND ORDEN_ENTREGA.EMPRESA=' +CONVERT(VARCHAR,@EMPRESA )" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @RECEPTOR IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQLSTRING = @SQLSTRING + N'   AND ORDEN_ENTREGA.RECEPTOR  = ''' + @RECEPTOR + '''' " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @COD_ERP IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQLSTRING = @SQLSTRING + N'   AND ORDEN_ENTREGA.COD_PROVE_ERP = ''' +  @COD_ERP + ''''" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXECUTE sp_executesql @SQLSTRING" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


End Sub



Public Function CodigoDeActualizacion2_15_00_02A2_15_00_03() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim bBool As Boolean

On Error GoTo error
    

sConsulta = "BEGIN TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = True
ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"


frmProgreso.lblDetalle = "Modificar triggers"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    
    V_2_15_Triggers_03
    
frmProgreso.lblDetalle = "Modificar stored procedures"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    
    V_2_15_Storeds_03

    
    sConsulta = "UPDATE VERSION SET NUM ='2.15.00.03'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta


sConsulta = "COMMIT TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = False

CodigoDeActualizacion2_15_00_02A2_15_00_03 = Not bCancelado
Exit Function
    
error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_15_00_02A2_15_00_03 = False

End Function


Private Sub V_2_15_Triggers_03()
Dim sConsulta As String

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PROVE_ERP_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PROVE_ERP_TG_INS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TRIGGER PROVE_ERP_TG_INS ON dbo.PROVE_ERP " & vbCrLf
sConsulta = sConsulta & "FOR INSERT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO PROVE_EMPRESA_COD_ERP (PROVE, COD_ERP, EMPRESA) SELECT  P.COD,I.COD_ERP, I.EMPRESA FROM INSERTED I INNER JOIN PROVE P ON I.NIF = P.NIF" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ERP  SET FECACT = GETDATE() FROM INSERTED I INNER JOIN PROVE_ERP PE ON PE.COD_ERP = I.COD_ERP and PE.EMPRESA=I.EMPRESA" & vbCrLf
sConsulta = sConsulta & ""
ExecuteSQL gRDOCon, sConsulta

End Sub

Private Sub V_2_15_Storeds_03()

Dim sConsulta As String
sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_DEVOLVER_EMPRESAS_PERSONA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_DEVOLVER_EMPRESAS_PERSONA]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSEP_DEVOLVER_EMPRESAS_PERSONA @PER VARCHAR(50), @ID INT OUTPUT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @UON1 VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @UON2 VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @UON3 VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "select @UON1 = uon1, @UON2 = uon2, @UON3 = uon3 from per where cod = @PER" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF (SELECT PED_OTRAS_EMP FROM USU WHERE PER = @PER) = 1" & vbCrLf
sConsulta = sConsulta & "  IF @UON3 IS NULL" & vbCrLf
sConsulta = sConsulta & "    IF @UON2 IS NULL" & vbCrLf
sConsulta = sConsulta & "      BEGIN" & vbCrLf
sConsulta = sConsulta & "      SELECT @ID = EMPRESA FROM UON1 WHERE COD = @UON1" & vbCrLf
sConsulta = sConsulta & "      SELECT ID, NIF, DEN, DIR, CP, POB, PAI, PROVI, ERP FROM EMP ORDER BY DEN" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "    ELSE" & vbCrLf
sConsulta = sConsulta & "      BEGIN" & vbCrLf
sConsulta = sConsulta & "      SELECT @ID = EMPRESA FROM UON2 WHERE UON1 = @UON1 AND COD = @UON2" & vbCrLf
sConsulta = sConsulta & "      IF @ID IS NULL" & vbCrLf
sConsulta = sConsulta & "        SELECT @ID = EMPRESA FROM UON1 WHERE COD = @UON1" & vbCrLf
sConsulta = sConsulta & "      SELECT ID, NIF, DEN, DIR, CP, POB, PAI, PROVI, ERP FROM EMP ORDER BY DEN" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "  ELSE" & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "    SELECT @ID = EMPRESA FROM UON3 WHERE UON1 = @UON1 AND UON2 = @UON2 AND COD = @UON3" & vbCrLf
sConsulta = sConsulta & "    IF @ID IS NULL" & vbCrLf
sConsulta = sConsulta & "      SELECT @ID = EMPRESA FROM UON2 WHERE UON1 = @UON1 AND COD = @UON2 " & vbCrLf
sConsulta = sConsulta & "    IF @ID IS NULL" & vbCrLf
sConsulta = sConsulta & "      SELECT @ID = EMPRESA FROM UON1 WHERE COD = @UON1" & vbCrLf
sConsulta = sConsulta & "    SELECT ID, NIF, DEN, DIR, CP, POB, PAI, PROVI, ERP FROM EMP ORDER BY DEN" & vbCrLf
sConsulta = sConsulta & "    END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  IF @UON3 IS NULL" & vbCrLf
sConsulta = sConsulta & "    IF @UON2 IS NULL" & vbCrLf
sConsulta = sConsulta & "      BEGIN" & vbCrLf
sConsulta = sConsulta & "      SELECT @ID = EMPRESA FROM UON1 WHERE COD = @UON1" & vbCrLf
sConsulta = sConsulta & "      SELECT ID, NIF, DEN, DIR, CP, POB, PAI, PROVI, ERP FROM (" & vbCrLf
sConsulta = sConsulta & "      SELECT DISTINCT EMPRESA FROM (" & vbCrLf
sConsulta = sConsulta & "      SELECT EMPRESA FROM UON1 WHERE COD = @UON1" & vbCrLf
sConsulta = sConsulta & "      UNION" & vbCrLf
sConsulta = sConsulta & "      SELECT EMPRESA FROM UON2 WHERE UON1 = @UON1" & vbCrLf
sConsulta = sConsulta & "      UNION" & vbCrLf
sConsulta = sConsulta & "      SELECT EMPRESA FROM UON3 WHERE UON1 = @UON1" & vbCrLf
sConsulta = sConsulta & "      UNION" & vbCrLf
sConsulta = sConsulta & "      SELECT @ID) U)DU " & vbCrLf
sConsulta = sConsulta & "        INNER JOIN EMP E" & vbCrLf
sConsulta = sConsulta & "                ON DU.EMPRESA = E.ID" & vbCrLf
sConsulta = sConsulta & "      ORDER BY DEN" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "    ELSE" & vbCrLf
sConsulta = sConsulta & "      BEGIN" & vbCrLf
sConsulta = sConsulta & "      SELECT @ID = EMPRESA FROM UON2 WHERE UON1 = @UON1 AND COD = @UON2" & vbCrLf
sConsulta = sConsulta & "      IF @ID IS NULL" & vbCrLf
sConsulta = sConsulta & "        SELECT @ID = EMPRESA FROM UON1 WHERE COD = @UON1" & vbCrLf
sConsulta = sConsulta & "      SELECT ID, NIF, DEN, DIR, CP, POB, PAI, PROVI, ERP FROM (" & vbCrLf
sConsulta = sConsulta & "      SELECT DISTINCT EMPRESA FROM (" & vbCrLf
sConsulta = sConsulta & "      SELECT EMPRESA FROM UON2 WHERE UON1 = @UON1 AND COD = @UON2" & vbCrLf
sConsulta = sConsulta & "      UNION" & vbCrLf
sConsulta = sConsulta & "      SELECT EMPRESA FROM UON3 WHERE UON1 = @UON1 AND UON2 = @UON2" & vbCrLf
sConsulta = sConsulta & "      UNION" & vbCrLf
sConsulta = sConsulta & "      SELECT @ID) U)DU " & vbCrLf
sConsulta = sConsulta & "        INNER JOIN EMP E" & vbCrLf
sConsulta = sConsulta & "                ON DU.EMPRESA = E.ID" & vbCrLf
sConsulta = sConsulta & "      ORDER BY DEN" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "  ELSE" & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "    SELECT @ID = EMPRESA FROM UON3 WHERE UON1 = @UON1 AND UON2 = @UON2 AND COD = @UON3" & vbCrLf
sConsulta = sConsulta & "    IF @ID IS NULL" & vbCrLf
sConsulta = sConsulta & "      SELECT @ID = EMPRESA FROM UON2 WHERE UON1 = @UON1 AND COD = @UON2 " & vbCrLf
sConsulta = sConsulta & "    IF @ID IS NULL" & vbCrLf
sConsulta = sConsulta & "      SELECT @ID = EMPRESA FROM UON1 WHERE COD = @UON1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "      SELECT ID, NIF, DEN, DIR, CP, POB, PAI, PROVI, ERP FROM (" & vbCrLf
sConsulta = sConsulta & "      SELECT DISTINCT EMPRESA FROM (" & vbCrLf
sConsulta = sConsulta & "      SELECT EMPRESA FROM UON3 WHERE UON1 = @UON1 AND UON2 = @UON2 AND COD = @UON3" & vbCrLf
sConsulta = sConsulta & "      UNION" & vbCrLf
sConsulta = sConsulta & "      SELECT @ID) U)DU " & vbCrLf
sConsulta = sConsulta & "        INNER JOIN EMP E" & vbCrLf
sConsulta = sConsulta & "                ON DU.EMPRESA = E.ID" & vbCrLf
sConsulta = sConsulta & "      ORDER BY DEN" & vbCrLf
sConsulta = sConsulta & "    END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_DEVOLVER_TODAS_ID_ORDENES]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_DEVOLVER_TODAS_ID_ORDENES]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSEP_DEVOLVER_TODAS_ID_ORDENES @MAXLINEAS integer, @APROBADOR varchar(50) =null,@APROVISIONADOR varchar(50)  =null,@ARTINT varchar(50)  =null, @ARTDEN varchar(500)  =null, @ARTEXT varchar(500)  =null, @DESDEFECHA varchar(10)  =null, @HASTAFECHA varchar(10)  =null, @PROVECOD varchar(50)  =null,@PROVEDEN varchar(500)  =null,@NUMPEDPROVE varchar(100)  =null,@ANYO int  =null,@PEDIDO int  =null,@ORDEN int  =null,@EST int  =null ,@COINCIDENCIA int =null,@REFERENCIA varchar(120)  =null,@CAMPO1 int=null ,@CAMPO2 int=null, @VALORCAMPO1 varchar(150)=null,@VALORCAMPO2 varchar(150)=null,@CATN1 int = NULL, @CATN2 int = NULL, @CATN3 int = NULL, @CATN4 int = NULL ,@CATN5 int = NULL, @EMPRESA INT = NULL, @RECEPTOR VARCHAR(100) = NULL, @COD_ERP  VARCHAR(100) = NULL, @TIPO TINYINT = 1  AS" & vbCrLf
sConsulta = sConsulta & "declare @SQL nvarchar(4000)" & vbCrLf
sConsulta = sConsulta & "declare @SQL2 nvarchar(4000)" & vbCrLf
sConsulta = sConsulta & "declare @PARAM nvarchar(1000)" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
sConsulta = sConsulta & "set  @SQL = N'" & vbCrLf
sConsulta = sConsulta & "SELECT CONVERT(VARCHAR,OE.EST) + LEFT(''00000000'',8-LEN(CONVERT(VARCHAR,OE.ID))) + CONVERT(VARCHAR,OE.ID)" & vbCrLf
sConsulta = sConsulta & "  FROM ORDEN_ENTREGA OE" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN PROVE P" & vbCrLf
sConsulta = sConsulta & "            ON OE.PROVE = P.COD" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN PEDIDO PE" & vbCrLf
sConsulta = sConsulta & "            ON OE.PEDIDO = PE.ID '" & vbCrLf
sConsulta = sConsulta & "IF @APROBADOR IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN (SELECT DISTINCT ORDEN , S.APROB_TIPO1 PER" & vbCrLf
sConsulta = sConsulta & "                           FROM LINEAS_PEDIDO LP " & vbCrLf
sConsulta = sConsulta & "                                    INNER JOIN SEGURIDAD S" & vbCrLf
sConsulta = sConsulta & "                                            ON LP.SEGURIDAD = S.ID " & vbCrLf
sConsulta = sConsulta & "                                           AND LP.NIVEL_APROB = 0) T1" & vbCrLf
sConsulta = sConsulta & "            ON OE.ID = T1.ORDEN" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN (SELECT DISTINCT ORDEN , AL.PER PER" & vbCrLf
sConsulta = sConsulta & "                            FROM LINEAS_PEDIDO LP " & vbCrLf
sConsulta = sConsulta & "                                     INNER JOIN APROB_LIM AL" & vbCrLf
sConsulta = sConsulta & "                                             ON LP.APROB_LIM = AL.ID) T2" & vbCrLf
sConsulta = sConsulta & "            ON OE.ID = T2.ORDEN '" & vbCrLf
sConsulta = sConsulta & "set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN PER P1" & vbCrLf
sConsulta = sConsulta & "            ON PE.PER = P1.COD" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN ORDEN_EST OES" & vbCrLf
sConsulta = sConsulta & "            ON OE.ORDEN_EST = OES.ID'" & vbCrLf
sConsulta = sConsulta & "if @ARTINT is not null or @ARTDEN is not null or @ARTEXT is not null or @CATN1 is not null or @CATN2 is not null or @CATN3 is not null or @CATN4 is not null or @CATN5 is not null or @CAMPO1 is not null or @CAMPO2 is not null or @VALORCAMPO1 is not null or @VALORCAMPO2 is not null" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "   set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN (SELECT DISTINCT ORDEN" & vbCrLf
sConsulta = sConsulta & "                  FROM LINEAS_PEDIDO LP" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN ITEM I " & vbCrLf
sConsulta = sConsulta & "             ON LP.ANYO = I.ANYO" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN1=I.GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND LP.PROCE=I.PROCE " & vbCrLf
sConsulta = sConsulta & "            AND LP.ITEM=I.ID'" & vbCrLf
sConsulta = sConsulta & "      IF @ARTDEN is NOT NULL " & vbCrLf
sConsulta = sConsulta & "         set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN ART4 " & vbCrLf
sConsulta = sConsulta & "             ON LP.GMN1=ART4.GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN2=ART4.GMN2 " & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN3=ART4.GMN3 " & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN4=ART4.GMN4 " & vbCrLf
sConsulta = sConsulta & "            AND LP.ART_INT=ART4.COD" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN ART4 ART_AUX " & vbCrLf
sConsulta = sConsulta & "             ON ART_AUX.COD=I.ART " & vbCrLf
sConsulta = sConsulta & "            AND ART_AUX.GMN1=I.GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND ART_AUX.GMN2=I.GMN2 " & vbCrLf
sConsulta = sConsulta & "            AND ART_AUX.GMN3=I.GMN3 " & vbCrLf
sConsulta = sConsulta & "            AND ART_AUX.GMN4=I.GMN4'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "      IF @ARTEXT is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN PROVE_ART4 PA" & vbCrLf
sConsulta = sConsulta & "             ON LP.PROVE = PA.PROVE" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN1 = PA.GMN1" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN2 = PA.GMN2" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN3 = PA.GMN3" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN4 = PA.GMN4" & vbCrLf
sConsulta = sConsulta & "            AND CASE WHEN LP.ART_INT IS NULL THEN I.ART ELSE LP.ART_INT END = PA.ART'             " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                 WHERE 1=1'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @CAMPO1 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQL = @SQL + N'   AND LP.CAMPO1 = @CAMPO1 '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @CAMPO2 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQL = @SQL + N'   AND LP.CAMPO2 = @CAMPO2 '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @VALORCAMPO1 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQL = @SQL + N'   AND LP.VALOR1 = @VALORCAMPO1 '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @VALORCAMPO2 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQL = @SQL + N'   AND LP.VALOR2 = @VALORCAMPO2 '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @CATN1 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQL = @SQL + N'   AND LP.CAT1 = @CATN1 '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @CATN2 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQL = @SQL + N'   AND LP.CAT2 = @CATN2 '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @CATN3 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQL = @SQL + N'   AND LP.CAT3 = @CATN3 '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @CATN4 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQL = @SQL + N'   AND LP.CAT4 = @CATN4 '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @CATN5 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQL = @SQL + N'   AND LP.CAT5 = @CATN5 '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @ARTINT is NOT NULL " & vbCrLf
sConsulta = sConsulta & "     begin " & vbCrLf
sConsulta = sConsulta & "   IF @COINCIDENCIA=1 " & vbCrLf
sConsulta = sConsulta & "          set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                   AND (LP.ART_INT + I.ART like ''% ''  + @ARTINT + '' %''' +" & vbCrLf
sConsulta = sConsulta & "          ' OR LP.ART_INT + I.ART like @ARTINT  + '' %''' +" & vbCrLf
sConsulta = sConsulta & "          ' OR LP.ART_INT + I.ART like ''% '' + @ARTINT  ' + " & vbCrLf
sConsulta = sConsulta & "     ' OR LP.ART_INT + I.ART=@ARTINT)'" & vbCrLf
sConsulta = sConsulta & "        ELSE" & vbCrLf
sConsulta = sConsulta & "      set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                   AND LP.ART_INT + I.ART like + ''%'' + @ARTINT + ''%'' '" & vbCrLf
sConsulta = sConsulta & "     end" & vbCrLf
sConsulta = sConsulta & "   IF @ARTDEN is NOT NULL " & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "      IF @COINCIDENCIA=1 " & vbCrLf
sConsulta = sConsulta & "         set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                   AND (ART4.DEN + I.DESCR  + ART_AUX.DEN + LP.DESCR_LIBRE like ''% ''  + @ARTDEN + '' %''' + " & vbCrLf
sConsulta = sConsulta & "          ' OR ART4.DEN + I.DESCR  + ART_AUX.DEN + LP.DESCR_LIBRE like @ARTDEN + '' %''' +" & vbCrLf
sConsulta = sConsulta & "          ' OR ART4.DEN + I.DESCR  + ART_AUX.DEN + LP.DESCR_LIBRE like ''% '' + @ARTDEN ' + " & vbCrLf
sConsulta = sConsulta & "     ' OR ART4.DEN + I.DESCR  + ART_AUX.DEN + LP.DESCR_LIBRE =@ARTDEN)'" & vbCrLf
sConsulta = sConsulta & "      ELSE" & vbCrLf
sConsulta = sConsulta & "         set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                   AND ART4.DEN + I.DESCR  + ART_AUX.DEN + LP.DESCR_LIBRE like + ''%'' + @ARTDEN + ''%'''" & vbCrLf
sConsulta = sConsulta & "    END" & vbCrLf
sConsulta = sConsulta & "   IF @ARTEXT is NOT NULL" & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "       IF @COINCIDENCIA=1 " & vbCrLf
sConsulta = sConsulta & "         set @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                   AND (PA.COD_EXT like ''% ''  + @ARTEXT + '' %''' + " & vbCrLf
sConsulta = sConsulta & "          ' OR PA.COD_EXT like @ARTEXT + '' %''' +" & vbCrLf
sConsulta = sConsulta & "          ' OR PA.COD_EXT like ''% '' + @ARTEXT ' + " & vbCrLf
sConsulta = sConsulta & "     ' OR PA.COD_EXT =@ARTEXT)'" & vbCrLf
sConsulta = sConsulta & "       ELSE" & vbCrLf
sConsulta = sConsulta & "         set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                   AND PA.COD_EXT like + ''%'' + @ARTEXT + ''%'''  " & vbCrLf
sConsulta = sConsulta & "     END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   set  @SQL = @SQL + N') LP" & vbCrLf
sConsulta = sConsulta & "            ON OE.ID = LP.ORDEN" & vbCrLf
sConsulta = sConsulta & "'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "        " & vbCrLf
sConsulta = sConsulta & "   IF @TIPO = 1            " & vbCrLf
sConsulta = sConsulta & "   set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                 WHERE PE.TIPO=1'" & vbCrLf
sConsulta = sConsulta & "   ELSE IF @TIPO= 0" & vbCrLf
sConsulta = sConsulta & "   set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                 WHERE PE.TIPO=0'" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "   set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                 WHERE 1=1'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if @DESDEFECHA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.FECHA >= CONVERT(datetime,@DESDEFECHA,103)'" & vbCrLf
sConsulta = sConsulta & "if @HASTAFECHA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.FECHA <= CONVERT(datetime,@HASTAFECHA,103)'" & vbCrLf
sConsulta = sConsulta & "if @PROVECOD IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.PROVE = @PROVECOD'" & vbCrLf
sConsulta = sConsulta & "if @NUMPEDPROVE IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.NUMEXT = @NUMPEDPROVE '" & vbCrLf
sConsulta = sConsulta & "if @ORDEN IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.NUM = @ORDEN '" & vbCrLf
sConsulta = sConsulta & "if @ANYO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.ANYO = @ANYO '" & vbCrLf
sConsulta = sConsulta & "if @REFERENCIA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.REFERENCIA = @REFERENCIA '" & vbCrLf
sConsulta = sConsulta & "if @PEDIDO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND PE.NUM = @PEDIDO '" & vbCrLf
sConsulta = sConsulta & "if @PROVEDEN IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND P.DEN  LIKE @PROVEDEN + ''%'' '" & vbCrLf
sConsulta = sConsulta & "IF @APROBADOR IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND (T1.PER = @APROBADOR " & vbCrLf
sConsulta = sConsulta & "                           OR T2.PER = @APROBADOR) '" & vbCrLf
sConsulta = sConsulta & "if @APROVISIONADOR IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND PE.PER = @APROVISIONADOR '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @EMPRESA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.EMPRESA = @EMPRESA '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @RECEPTOR IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.RECEPTOR = @RECEPTOR '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @COD_ERP IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.COD_PROVE_ERP = @COD_ERP '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "declare @IN varchar(500)" & vbCrLf
sConsulta = sConsulta & "SET @IN = '('" & vbCrLf
sConsulta = sConsulta & "IF @EST & 1 = 1" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '0,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 2 = 2" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '1,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 4 = 4" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '2,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 8 = 8" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '3,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 16 = 16" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '4,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 32 = 32" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '5,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 64 = 64" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '6,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 128 = 128" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '20,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 256 = 256" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '21,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 512 = 512" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '22,'" & vbCrLf
sConsulta = sConsulta & "IF @IN!='('" & vbCrLf
sConsulta = sConsulta & "   set @SQL = @SQL + '" & vbCrLf
sConsulta = sConsulta & "   AND OE.EST IN ' + LEFT(@IN,LEN(@IN)-1) + ') '" & vbCrLf
sConsulta = sConsulta & "set @SQL = @SQL + '" & vbCrLf
sConsulta = sConsulta & "order by CONVERT(VARCHAR,OE.EST) + LEFT(''00000000'',8-LEN(CONVERT(VARCHAR,OE.ID))) + CONVERT(VARCHAR,OE.ID)'" & vbCrLf
sConsulta = sConsulta & "SET @SQL2 = '" & vbCrLf
sConsulta = sConsulta & "create table #IDS (id varchar(200))" & vbCrLf
sConsulta = sConsulta & "set nocount on" & vbCrLf
sConsulta = sConsulta & "DECLARE C1 CURSOR FOR " & vbCrLf
sConsulta = sConsulta & "' + @SQL + '" & vbCrLf
sConsulta = sConsulta & "DECLARE @miID varchar(200)" & vbCrLf
sConsulta = sConsulta & "declare @cont int" & vbCrLf
sConsulta = sConsulta & "set @cont=1" & vbCrLf
sConsulta = sConsulta & "OPEN C1" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C1 INTO @miID" & vbCrLf
sConsulta = sConsulta & "while @@fetch_status=0" & vbCrLf
sConsulta = sConsulta & "begin" & vbCrLf
sConsulta = sConsulta & "if @cont = 1" & vbCrLf
sConsulta = sConsulta & " insert into #IDS values (@miID)" & vbCrLf
sConsulta = sConsulta & "set @cont = @cont + 1        " & vbCrLf
sConsulta = sConsulta & "     " & vbCrLf
sConsulta = sConsulta & "if @cont = ' + CONVERT(VARCHAR,@MAXLINEAS) + ' + 1" & vbCrLf
sConsulta = sConsulta & " set @cont = 1" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C1 INTO @miID" & vbCrLf
sConsulta = sConsulta & "end" & vbCrLf
sConsulta = sConsulta & "CLOSE C1" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C1" & vbCrLf
sConsulta = sConsulta & "set nocount off" & vbCrLf
sConsulta = sConsulta & "select * from #ids" & vbCrLf
sConsulta = sConsulta & "'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @PARAM = N'@APROBADOR varchar(50), @APROVISIONADOR varchar(50), @DESDEFECHA varchar(10), @HASTAFECHA varchar(10), @ARTINT varchar(50), @ARTDEN varchar(500), @ARTEXT varchar(500), @PROVECOD varchar(50), @PROVEDEN varchar(500), @NUMPEDPROVE varchar(100),@ORDEN int, @ANYO int, @PEDIDO int, @EST int,@REFERENCIA varchar(120), @CAMPO1 int ,@CAMPO2 int, @VALORCAMPO1 varchar(150) ,@VALORCAMPO2 varchar(150) ,@CATN1 int ,@CATN2 int ,@CATN3 int, @CATN4 int , @CATN5 int, @EMPRESA INT, @RECEPTOR VARCHAR(100), @COD_ERP VARCHAR(100)'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL2, @PARAM, @APROBADOR= @APROBADOR, @APROVISIONADOR = @APROVISIONADOR, @DESDEFECHA=@DESDEFECHA, @HASTAFECHA=@HASTAFECHA, @ARTINT=@ARTINT, @ARTDEN=@ARTDEN, @ARTEXT=@ARTEXT, @PROVECOD=@PROVECOD, @PROVEDEN=@PROVEDEN, @NUMPEDPROVE=@NUMPEDPROVE, @ANYO=@ANYO, @ORDEN=@ORDEN, @PEDIDO = @PEDIDO, @EST=@EST,@REFERENCIA=@REFERENCIA , @CAMPO1=@CAMPO1 , @CAMPO2=@CAMPO2 ,@VALORCAMPO1=@VALORCAMPO1 , @VALORCAMPO2=@VALORCAMPO2 , @CATN1=@CATN1 , @CATN2=@CATN2 ,@CATN3=@CATN3 ,@CATN4=@CATN4, @CATN5=@CATN5, @EMPRESA = @EMPRESA, @RECEPTOR = @RECEPTOR, @COD_ERP = @COD_ERP" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_DEVOLVER_TODAS_ORDENES]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_DEVOLVER_TODAS_ORDENES]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSEP_DEVOLVER_TODAS_ORDENES @MAXLINEAS integer, @DESDEID varchar(50)=null, @APROBADOR varchar(50) = null,@APROVISIONADOR varchar(50) = null, @ARTINT varchar(50) = null, @ARTDEN varchar(500) = null, @ARTEXT varchar(500) = null, @DESDEFECHA varchar(10) = null, @HASTAFECHA varchar(10) = null, @PROVECOD varchar(50) = null, @PROVEDEN varchar(500) = null,@NUMPEDPROVE varchar(100) = null,@ANYO int = null,@PEDIDO int = null,@ORDEN int = null,@EST int  = null, @ID int = null ,@RECINCORRECTO int = null ,@COINCIDENCIA int =null,@REFERENCIA varchar(120) = null ,@CAMPO1 int = null ,@CAMPO2 int = null, @VALORCAMPO1 varchar(150) = null, @VALORCAMPO2 varchar(150)=null , @CATN1 int = null, @CATN2 int = null, @CATN3 int = null, @CATN4 int = null , @CATN5 int = null, @EMPRESA INT = NULL, @RECEPTOR VARCHAR(100) = NULL, @COD_ERP  VARCHAR(100) = NULL, @ENTIDAD VARCHAR(100) = 'ORDEN', @TIPO TINYINT = 1 AS " & vbCrLf
sConsulta = sConsulta & "declare @SQL nvarchar(4000)" & vbCrLf
sConsulta = sConsulta & "declare @PARAM nvarchar(1000)" & vbCrLf
sConsulta = sConsulta & "declare @SQL1 nvarchar(4000)" & vbCrLf
sConsulta = sConsulta & "declare @SQL2 nvarchar(4000)" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL OFF" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ENTIDAD = 'PROVE'" & vbCrLf
sConsulta = sConsulta & "  SET @SQL = N'SELECT DISTINCT P.COD, P.DEN , P.NIF, P.DIR, P.CP, P.PAI, P.POB, P.PROVI'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  IF @ENTIDAD = 'EMPRESA'" & vbCrLf
sConsulta = sConsulta & "    SET @SQL = N'SELECT DISTINCT E.ID, E.NIF, E.DEN, E.DIR, E.CP, E.POB, E.PROVI, E.PAI, PAI.DEN PAIDEN, PROVI.DEN PROVIDEN '" & vbCrLf
sConsulta = sConsulta & "  ELSE" & vbCrLf
sConsulta = sConsulta & "    SET @SQL = N'SELECT TOP ' + CONVERT(varchar,@MAXLINEAS) + ' CONVERT(VARCHAR,OE.EST) + LEFT(''00000000'',8-LEN(CONVERT(VARCHAR,OE.ID))) + CONVERT(VARCHAR,OE.ID), " & vbCrLf
sConsulta = sConsulta & "       OE.ID, OE.ANYO, PE.NUM NUMPEDIDO, OE.NUM NUMORDEN, OE.PROVE PROVECOD, P.DEN PROVEDEN, P.NIF," & vbCrLf
sConsulta = sConsulta & "       OE.NUMEXT, OE.FECHA FECHA, OE.EST EST, OE.IMPORTE IMPORTE, " & vbCrLf
sConsulta = sConsulta & "       P1.APE + '', '' + P1.NOM APROVISIONADOR,P1.APE,P1.NOM,OE.OBS," & vbCrLf
sConsulta = sConsulta & "       OES.COMENT, OES.FECHA FECEST,OE.IMPORTE,OE.PEDIDO,OE.TIPO, OE.INCORRECTA, PE.PER,OE.REFERENCIA,T3.ADJUNTOS, CP.COMENT COMENTPROVE, CP.EST ESTCOMENT,OE.MON, OE.EMPRESA, E.DEN DENEMPRESA, E.NIF NIFEMPRESA, OE.COD_PROVE_ERP, OE.RECEPTOR ,PER.NOM + '' '' + PER.APE NOMRECEPTOR, PER.TFNO TFNORECEPTOR, PER.EMAIL EMAILRECEPTOR, ISNULL(PER.UON3,ISNULL(PER.UON2,PER.UON1)) UONRECEPTOR, ISNULL(UON3.DEN,ISNULL(UON2.DEN,UON1.DEN)) DENUONRECEPTOR, PE.TIPO'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @SQL= @SQL + N'" & vbCrLf
sConsulta = sConsulta & "  FROM ORDEN_ENTREGA OE" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN PROVE P" & vbCrLf
sConsulta = sConsulta & "            ON OE.PROVE = P.COD" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN PEDIDO PE" & vbCrLf
sConsulta = sConsulta & "            ON OE.PEDIDO = PE.ID '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ENTIDAD = 'EMPRESA'" & vbCrLf
sConsulta = sConsulta & "  SET @SQL= @SQL + N'" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN (EMP E LEFT JOIN PAI ON E.PAI = PAI.COD LEFT JOIN PROVI ON E.PAI = PROVI.PAI AND E.PROVI = PROVI.COD) ON OE.EMPRESA = E.ID'" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  SET @SQL= @SQL + N'" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN EMP E ON OE.EMPRESA = E.ID" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN (PER LEFT JOIN UON1 ON PER.UON1 = UON1.COD LEFT JOIN UON2 ON PER.UON1 = UON2.UON1 AND PER.UON2 = UON2.COD LEFT JOIN UON3 ON PER.UON1 = UON3.UON1 AND PER.UON2 = UON3.UON2 AND PER.UON3 = UON3.COD) ON OE.RECEPTOR = PER.COD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN (SELECT OE.ID, OE.PEDIDO, OE.ORDEN, OE.EST, OE.COMENT " & vbCrLf
sConsulta = sConsulta & "                  FROM ORDEN_EST OE " & vbCrLf
sConsulta = sConsulta & "                        INNER JOIN (SELECT PEDIDO, ORDEN, MAX(ID) ID" & vbCrLf
sConsulta = sConsulta & "                                      FROM ORDEN_EST  WHERE EST IN (3, 21) " & vbCrLf
sConsulta = sConsulta & "                                    GROUP BY PEDIDO, ORDEN ) MO " & vbCrLf
sConsulta = sConsulta & "                                ON MO.ID = OE.ID) CP" & vbCrLf
sConsulta = sConsulta & "            ON OE.ID = CP.ORDEN" & vbCrLf
sConsulta = sConsulta & "'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @APROBADOR IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN (SELECT DISTINCT ORDEN , S.APROB_TIPO1 PER" & vbCrLf
sConsulta = sConsulta & "                           FROM LINEAS_PEDIDO LP " & vbCrLf
sConsulta = sConsulta & "                                    INNER JOIN SEGURIDAD S" & vbCrLf
sConsulta = sConsulta & "                                            ON LP.SEGURIDAD = S.ID " & vbCrLf
sConsulta = sConsulta & "                                           AND LP.NIVEL_APROB = 0) T1" & vbCrLf
sConsulta = sConsulta & "            ON OE.ID = T1.ORDEN" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN (SELECT DISTINCT ORDEN , AL.PER PER" & vbCrLf
sConsulta = sConsulta & "                            FROM LINEAS_PEDIDO LP " & vbCrLf
sConsulta = sConsulta & "                                     INNER JOIN APROB_LIM AL" & vbCrLf
sConsulta = sConsulta & "                                             ON LP.APROB_LIM = AL.ID) T2" & vbCrLf
sConsulta = sConsulta & "            ON OE.ID = T2.ORDEN '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN PER P1" & vbCrLf
sConsulta = sConsulta & "            ON PE.PER = P1.COD" & vbCrLf
sConsulta = sConsulta & "     LEFT JOIN ORDEN_EST OES" & vbCrLf
sConsulta = sConsulta & "            ON OE.ORDEN_EST = OES.ID'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "    LEFT JOIN (SELECT COUNT(*) AS ADJUNTOS,ORDEN  FROM orden_entrega_adjun GROUP BY orden_entrega_adjun.ORDEN) T3 ON OE.ID= T3.ORDEN '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if @ARTINT is not null or @ARTDEN is not null or @ARTEXT is not null or @CATN1 is not null or @CATN2 is not null or @CATN3 is not null or @CATN4 is not null or @CATN5 is not null or @CAMPO1 is not null or @CAMPO2 is not null or @VALORCAMPO1 is not null or @VALORCAMPO2 is not null" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "   set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN (SELECT DISTINCT ORDEN" & vbCrLf
sConsulta = sConsulta & "                  FROM LINEAS_PEDIDO LP" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN ITEM I " & vbCrLf
sConsulta = sConsulta & "             ON LP.ANYO = I.ANYO" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN1=I.GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND LP.PROCE=I.PROCE " & vbCrLf
sConsulta = sConsulta & "            AND LP.ITEM=I.ID'" & vbCrLf
sConsulta = sConsulta & "      IF @ARTDEN is NOT NULL " & vbCrLf
sConsulta = sConsulta & "         set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN ART4 " & vbCrLf
sConsulta = sConsulta & "             ON LP.GMN1=ART4.GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN2=ART4.GMN2 " & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN3=ART4.GMN3 " & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN4=ART4.GMN4 " & vbCrLf
sConsulta = sConsulta & "            AND LP.ART_INT=ART4.COD" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN ART4 ART_AUX " & vbCrLf
sConsulta = sConsulta & "             ON ART_AUX.COD=I.ART " & vbCrLf
sConsulta = sConsulta & "            AND ART_AUX.GMN1=I.GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND ART_AUX.GMN2=I.GMN2 " & vbCrLf
sConsulta = sConsulta & "            AND ART_AUX.GMN3=I.GMN3 " & vbCrLf
sConsulta = sConsulta & "            AND ART_AUX.GMN4=I.GMN4'" & vbCrLf
sConsulta = sConsulta & "      IF @ARTEXT is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN PROVE_ART4 PA" & vbCrLf
sConsulta = sConsulta & "             ON LP.PROVE = PA.PROVE" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN1 = PA.GMN1" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN2 = PA.GMN2" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN3 = PA.GMN3" & vbCrLf
sConsulta = sConsulta & "            AND LP.GMN4 = PA.GMN4" & vbCrLf
sConsulta = sConsulta & "            AND CASE WHEN LP.ART_INT IS NULL THEN I.ART ELSE LP.ART_INT END = PA.ART'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                 WHERE 1=1'" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "   IF @CAMPO1 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQL = @SQL + N'   AND LP.CAMPO1 = @CAMPO1 '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @CAMPO2 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQL = @SQL + N'   AND LP.CAMPO2 = @CAMPO2 '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @VALORCAMPO1 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQL = @SQL + N'   AND LP.VALOR1 = @VALORCAMPO1 '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @VALORCAMPO2 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQL = @SQL + N'   AND LP.VALOR2 = @VALORCAMPO2 '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @CATN1 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQL = @SQL + N'   AND LP.CAT1 = @CATN1 '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @CATN2 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQL = @SQL + N'   AND LP.CAT2 = @CATN2 '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @CATN3 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQL = @SQL + N'   AND LP.CAT3 = @CATN3 '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @CATN4 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQL = @SQL + N'   AND LP.CAT4 = @CATN4 '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   IF @CATN5 is NOT NULL" & vbCrLf
sConsulta = sConsulta & "         set @SQL = @SQL + N'   AND LP.CAT5 = @CATN5 '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  IF @ARTINT is NOT NULL " & vbCrLf
sConsulta = sConsulta & "     begin " & vbCrLf
sConsulta = sConsulta & "   IF @COINCIDENCIA=1 " & vbCrLf
sConsulta = sConsulta & "          set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                   AND (LP.ART_INT + I.ART like ''% ''  + @ARTINT + '' %''' +" & vbCrLf
sConsulta = sConsulta & "          ' OR LP.ART_INT + I.ART like @ARTINT  + '' %''' +" & vbCrLf
sConsulta = sConsulta & "          ' OR LP.ART_INT + I.ART like ''% '' + @ARTINT  ' + " & vbCrLf
sConsulta = sConsulta & "     ' OR LP.ART_INT + I.ART=@ARTINT)'" & vbCrLf
sConsulta = sConsulta & "        ELSE" & vbCrLf
sConsulta = sConsulta & "      set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                   AND LP.ART_INT + I.ART like + ''%'' + @ARTINT + ''%'' '" & vbCrLf
sConsulta = sConsulta & "     end" & vbCrLf
sConsulta = sConsulta & "   IF @ARTDEN is NOT NULL " & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "      IF @COINCIDENCIA=1 " & vbCrLf
sConsulta = sConsulta & "         set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                   AND (ART4.DEN + I.DESCR  + ART_AUX.DEN + LP.DESCR_LIBRE like ''% ''  + @ARTDEN + '' %''' + " & vbCrLf
sConsulta = sConsulta & "          ' OR ART4.DEN + I.DESCR  + ART_AUX.DEN + LP.DESCR_LIBRE like @ARTDEN + '' %''' +" & vbCrLf
sConsulta = sConsulta & "          ' OR ART4.DEN + I.DESCR  + ART_AUX.DEN + LP.DESCR_LIBRE like ''% '' + @ARTDEN ' + " & vbCrLf
sConsulta = sConsulta & "     ' OR ART4.DEN + I.DESCR  + ART_AUX.DEN + LP.DESCR_LIBRE =@ARTDEN)'" & vbCrLf
sConsulta = sConsulta & "      ELSE" & vbCrLf
sConsulta = sConsulta & "         set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                   AND ART4.DEN + I.DESCR  + ART_AUX.DEN + LP.DESCR_LIBRE like + ''%'' + @ARTDEN + ''%'''" & vbCrLf
sConsulta = sConsulta & "    END" & vbCrLf
sConsulta = sConsulta & "   IF @ARTEXT is NOT NULL" & vbCrLf
sConsulta = sConsulta & "     BEGIN" & vbCrLf
sConsulta = sConsulta & "       IF @COINCIDENCIA=1 " & vbCrLf
sConsulta = sConsulta & "         set @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                   AND (PA.COD_EXT like ''% ''  + @ARTEXT + '' %''' + " & vbCrLf
sConsulta = sConsulta & "          ' OR PA.COD_EXT like @ARTEXT + '' %''' +" & vbCrLf
sConsulta = sConsulta & "          ' OR PA.COD_EXT like ''% '' + @ARTEXT ' + " & vbCrLf
sConsulta = sConsulta & "     ' OR PA.COD_EXT =@ARTEXT)'" & vbCrLf
sConsulta = sConsulta & "       ELSE" & vbCrLf
sConsulta = sConsulta & "         set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                   AND PA.COD_EXT like + ''%'' + @ARTEXT + ''%'''  " & vbCrLf
sConsulta = sConsulta & "     END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   set  @SQL = @SQL + N') LP" & vbCrLf
sConsulta = sConsulta & "            ON OE.ID = LP.ORDEN" & vbCrLf
sConsulta = sConsulta & "'" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "   IF @TIPO = 1            " & vbCrLf
sConsulta = sConsulta & "   set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                 WHERE PE.TIPO=1'" & vbCrLf
sConsulta = sConsulta & "   ELSE IF @TIPO= 0" & vbCrLf
sConsulta = sConsulta & "   set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                 WHERE PE.TIPO=0'" & vbCrLf
sConsulta = sConsulta & "   ELSE" & vbCrLf
sConsulta = sConsulta & "   set  @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "                 WHERE 1=1'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "if @REFERENCIA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.REFERENCIA = @REFERENCIA '" & vbCrLf
sConsulta = sConsulta & "if @DESDEFECHA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.FECHA >= CONVERT(datetime,@DESDEFECHA,103)'" & vbCrLf
sConsulta = sConsulta & "if @HASTAFECHA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.FECHA <= CONVERT(datetime,@HASTAFECHA,103)'" & vbCrLf
sConsulta = sConsulta & "if @PROVECOD IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.PROVE = @PROVECOD'" & vbCrLf
sConsulta = sConsulta & "if @NUMPEDPROVE IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.NUMEXT = @NUMPEDPROVE '" & vbCrLf
sConsulta = sConsulta & "if @ORDEN IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.NUM = @ORDEN '" & vbCrLf
sConsulta = sConsulta & "if @ANYO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.ANYO = @ANYO '" & vbCrLf
sConsulta = sConsulta & "if @PEDIDO IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND PE.NUM = @PEDIDO '" & vbCrLf
sConsulta = sConsulta & "if @PROVEDEN IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND P.DEN  LIKE @PROVEDEN + ''%'' '" & vbCrLf
sConsulta = sConsulta & "IF @APROBADOR IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND (T1.PER = @APROBADOR " & vbCrLf
sConsulta = sConsulta & "                           OR T2.PER = @APROBADOR) '" & vbCrLf
sConsulta = sConsulta & "if @APROVISIONADOR IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND PE.PER = @APROVISIONADOR '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @EMPRESA IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.EMPRESA = @EMPRESA '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @RECEPTOR IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.RECEPTOR = @RECEPTOR '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @COD_ERP IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + N'   AND OE.COD_PROVE_ERP = @COD_ERP '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "declare @IN varchar(500)" & vbCrLf
sConsulta = sConsulta & "SET @IN = '('" & vbCrLf
sConsulta = sConsulta & "IF @EST & 1 = 1" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '0,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 2 = 2" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '1,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 4 = 4" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '2,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 8 = 8" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '3,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 16 = 16" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '4,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 32 = 32" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '5,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 64 = 64" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '6,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 128 = 128" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '20,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 256 = 256" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '21,'" & vbCrLf
sConsulta = sConsulta & "IF @EST & 512 = 512" & vbCrLf
sConsulta = sConsulta & "     SET @IN = @IN + '22,'" & vbCrLf
sConsulta = sConsulta & "SET @SQL1 = ''" & vbCrLf
sConsulta = sConsulta & "SET @SQL2 = ''" & vbCrLf
sConsulta = sConsulta & "IF @IN!='('" & vbCrLf
sConsulta = sConsulta & "     set @SQL1=' OE.EST IN ' + LEFT(@IN,LEN(@IN)-1) + ') '" & vbCrLf
sConsulta = sConsulta & "IF @RECINCORRECTO IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "  IF @RECINCORRECTO=1" & vbCrLf
sConsulta = sConsulta & "   set @SQL2 = ' OE.INCORRECTA=1'  " & vbCrLf
sConsulta = sConsulta & "IF @SQL1<>'' and @SQL2<>'' " & vbCrLf
sConsulta = sConsulta & "   SET @SQL= @SQL + ' AND (' + @SQL1 + ' OR ' + @SQL2 + ')'" & vbCrLf
sConsulta = sConsulta & "ELSE " & vbCrLf
sConsulta = sConsulta & "if @SQL1!=''" & vbCrLf
sConsulta = sConsulta & "   SET @SQL= @SQL + ' AND ' + @SQL1 " & vbCrLf
sConsulta = sConsulta & "else" & vbCrLf
sConsulta = sConsulta & "if @SQL2!=''" & vbCrLf
sConsulta = sConsulta & "   SET @SQL= @SQL + ' AND ' + @SQL2" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "if @ID IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + '" & vbCrLf
sConsulta = sConsulta & "   AND OE.ID=@ID '" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "if @DESDEID IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "   SET @SQL = @SQL + '" & vbCrLf
sConsulta = sConsulta & "   AND CONVERT(VARCHAR,OE.EST) + LEFT(''00000000'',8-LEN(CONVERT(VARCHAR,OE.ID))) + CONVERT(VARCHAR,OE.ID)>=@DESDEID '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ENTIDAD = 'PROVE'" & vbCrLf
sConsulta = sConsulta & "  SET @SQL =  @SQL + N'" & vbCrLf
sConsulta = sConsulta & "    ORDER BY P.COD '" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  IF @ENTIDAD = 'EMPRESA'" & vbCrLf
sConsulta = sConsulta & "    SET @SQL =  @SQL + N'" & vbCrLf
sConsulta = sConsulta & "    ORDER BY E.NIF '" & vbCrLf
sConsulta = sConsulta & "  ELSE" & vbCrLf
sConsulta = sConsulta & "    set @SQL = @SQL + N'" & vbCrLf
sConsulta = sConsulta & "    order by CONVERT(VARCHAR,OE.EST) + LEFT(''00000000'',8-LEN(CONVERT(VARCHAR,OE.ID))) + CONVERT(VARCHAR,OE.ID)'" & vbCrLf
sConsulta = sConsulta & "SET @PARAM = N'@APROBADOR varchar(50), @APROVISIONADOR varchar(50), @DESDEFECHA varchar(10), @HASTAFECHA varchar(10), @ARTINT varchar(50), @ARTDEN varchar(500), @ARTEXT varchar(500), @PROVECOD varchar(50), @PROVEDEN varchar(500), @NUMPEDPROVE varchar(100),@ORDEN int, @ANYO int, @PEDIDO int, @DESDEID varchar(50), @EST int, @ID int,@REFERENCIA varchar(120) , @CAMPO1 int ,@CAMPO2 int, @VALORCAMPO1 varchar(150) ,@VALORCAMPO2 varchar(150) ,@CATN1 int ,@CATN2 int ,@CATN3 int, @CATN4 int , @CATN5 int, @EMPRESA INT, @RECEPTOR VARCHAR(100), @COD_ERP VARCHAR(100) '" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, @PARAM, @APROBADOR= @APROBADOR, @APROVISIONADOR = @APROVISIONADOR, @DESDEFECHA=@DESDEFECHA, @HASTAFECHA=@HASTAFECHA, @ARTINT=@ARTINT, @ARTDEN=@ARTDEN, @ARTEXT =@ARTEXT , @PROVECOD=@PROVECOD, @PROVEDEN=@PROVEDEN, @NUMPEDPROVE=@NUMPEDPROVE, @ANYO=@ANYO, @ORDEN=@ORDEN, @PEDIDO = @PEDIDO,@DESDEID=@DESDEID, @EST=@EST, @ID = @ID, @REFERENCIA=@REFERENCIA , @CAMPO1=@CAMPO1 , @CAMPO2=@CAMPO2 ,@VALORCAMPO1=@VALORCAMPO1 , @VALORCAMPO2=@VALORCAMPO2 , @CATN1=@CATN1 , @CATN2=@CATN2 ,@CATN3=@CATN3 ,@CATN4=@CATN4, @CATN5=@CATN5 , @EMPRESA = @EMPRESA, @RECEPTOR = @RECEPTOR, @COD_ERP = @COD_ERP" & vbCrLf
sConsulta = sConsulta & "SET CONCAT_NULL_YIELDS_NULL ON" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub


Public Function CodigoDeActualizacion2_15_00_03A2_15_00_04() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim bBool As Boolean

On Error GoTo error
    

sConsulta = "BEGIN TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = True
ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"


    
frmProgreso.lblDetalle = "Modificar stored procedures"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
    
    V_2_15_Storeds_04

    
    sConsulta = "UPDATE VERSION SET NUM ='2.15.00.04'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    ExecuteSQL gRDOCon, sConsulta


sConsulta = "COMMIT TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = False

CodigoDeActualizacion2_15_00_03A2_15_00_04 = Not bCancelado
Exit Function
    
error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_15_00_03A2_15_00_04 = False

End Function



Private Sub V_2_15_Storeds_04()
Dim sConsulta As String

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ANYA_PROVE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_ANYA_PROVE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_ANYA_PROVE (@COD VARCHAR(100),@DEN VARCHAR(100),@NIF VARCHAR(100),@PAI VARCHAR(50),@PROVI VARCHAR(50),@CP VARCHAR(50),@POB VARCHAR(100),@DIR VARCHAR(255),@MON VARCHAR(50),@FSP_COD VARCHAR(100),@PREM  INT,@URLPROVE VARCHAR(255))  AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @PAIG VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVIG VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @MONG VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @ACTIVA BIT" & vbCrLf
sConsulta = sConsulta & "DECLARE @INT TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @SENTIDO TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ACTIVAR_COD_PROVE_ERP TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @OLDNIF VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @PAIG=(SELECT TOP 1 COD FROM PAI WHERE FSP_COD=@PAI)" & vbCrLf
sConsulta = sConsulta & "SET @PROVIG=(SELECT TOP 1 COD FROM PROVI WHERE FSP_PAI=@PAI AND FSP_COD=@PROVI)" & vbCrLf
sConsulta = sConsulta & "SET @MONG=(SELECT TOP 1 COD FROM MON WHERE FSP_COD=@MON)" & vbCrLf
sConsulta = sConsulta & "IF @PREM = 2 " & vbCrLf
sConsulta = sConsulta & "SET @PREM=1" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "SET @PREM=0" & vbCrLf
sConsulta = sConsulta & "/**************************************************************************************************************************************************************************/" & vbCrLf
sConsulta = sConsulta & "/******       integraci�n :  Primero miramos si est� activada : a nivel gral y en concreto la entidad PROVE en sentido salida o entrada/salida   ******/" & vbCrLf
sConsulta = sConsulta & "/******                              grabaremos en la tabla LOG_PROVE (origen = 0 y el usuario a blancos                                                                  ******/" & vbCrLf
sConsulta = sConsulta & "/*********************************************************************************************************************************************************************/" & vbCrLf
sConsulta = sConsulta & "SELECT @INT = INTEGRACION, @ACTIVAR_COD_PROVE_ERP = ACTIVAR_COD_PROVE_ERP FROM PARGEN_INTERNO WHERE ID=1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ACTIVAR_COD_PROVE_ERP = 1" & vbCrLf
sConsulta = sConsulta & "  SELECT @OLDNIF = NIF FROM PROVE WHERE COD = @COD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @INT = 1 " & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & " SELECT @ACTIVA=ACTIVA,@SENTIDO=SENTIDO FROM TABLAS_INTEGRACION WHERE ID=8" & vbCrLf
sConsulta = sConsulta & " IF @ACTIVA = 1 AND (@SENTIDO = 1 OR @SENTIDO=3)" & vbCrLf
sConsulta = sConsulta & "    SET @INT = 1" & vbCrLf
sConsulta = sConsulta & " ELSE" & vbCrLf
sConsulta = sConsulta & "   SET @INT = 0" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "IF ((SELECT COUNT(*) FROM PROVE WHERE COD=@COD)>0)" & vbCrLf
sConsulta = sConsulta & "begin" & vbCrLf
sConsulta = sConsulta & " UPDATE PROVE SET DEN=@DEN,NIF=@NIF,PAI=@PAIG,PROVI=@PROVIG,CP=@CP,POB=@POB,DIR=@DIR,MON=@MONG,FSP_COD=@FSP_COD,PREMIUM=@PREM,ACTIVO=@PREM,URLPROVE=@URLPROVE WHERE COD=@COD" & vbCrLf
sConsulta = sConsulta & " IF @INT = 1" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO LOG_PROVE (ACCION,COD,DEN,NIF,PAI,PROVI,CP,POB,DIR,MON,URLPROVE,ORIGEN,USU) VALUES ('U',@COD,@DEN,@NIF,@PAIG,@PROVIG,@CP,@POB,@DIR,@MONG,@URLPROVE,0,'')" & vbCrLf
sConsulta = sConsulta & "end" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "begin" & vbCrLf
sConsulta = sConsulta & " INSERT INTO PROVE (COD,DEN,NIF,PAI,PROVI,CP,POB,DIR,MON,FSP_COD,PREMIUM,ACTIVO,URLPROVE) VALUES " & vbCrLf
sConsulta = sConsulta & " (@COD,@DEN,@NIF,@PAIG,@PROVIG,@CP,@POB,@DIR,@MONG,@FSP_COD,@PREM,@PREM,@URLPROVE)" & vbCrLf
sConsulta = sConsulta & " if @INT = 1" & vbCrLf
sConsulta = sConsulta & " INSERT INTO LOG_PROVE (ACCION,COD,DEN,NIF,PAI,PROVI,CP,POB,DIR,MON,URLPROVE,ORIGEN,USU) VALUES ('I',@COD,@DEN,@NIF,@PAIG,@PROVIG,@CP,@POB,@DIR,@MONG,@URLPROVE,0,'')" & vbCrLf
sConsulta = sConsulta & "end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @ACTIVAR_COD_PROVE_ERP = 1" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    IF @OLDNIF <> @NIF " & vbCrLf
sConsulta = sConsulta & "      DELETE FROM PROVE_EMPRESA_COD_ERP FROM PROVE_EMPRESA_COD_ERP PECE INNER JOIN PROVE_ERP PE ON PECE.COD_ERP = PE.COD_ERP AND PECE.EMPRESA = PE.EMPRESA  WHERE PE.NIF = @OLDNIF AND PECE.PROVE = @COD" & vbCrLf
sConsulta = sConsulta & "    IF @NIF IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "      INSERT INTO PROVE_EMPRESA_COD_ERP (PROVE, COD_ERP, EMPRESA) SELECT  P.COD,I.COD_ERP, I.EMPRESA FROM PROVE_ERP I INNER JOIN PROVE P ON I.NIF = P.NIF WHERE P.COD=@COD AND I.NIF =@NIF AND NOT EXISTS (SELECT * FROM PROVE_EMPRESA_COD_ERP PECE WHERE P.COD=PECE.PROVE AND I.COD_ERP = PECE.COD_ERP AND I.EMPRESA = PECE.EMPRESA)" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "Return" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_DEVOLVER_RECEPTORES]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_DEVOLVER_RECEPTORES]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSEP_DEVOLVER_RECEPTORES @PER VARCHAR(50) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "declare @UON1 VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "declare @UON2 VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "declare @UON3 VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF (SELECT DEST_OTRAS_UO FROM USU WHERE PER = @PER)=0" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    select @UON1 = uon1, @UON2 = uon2, @UON3 = uon3 from per where cod = @PER" & vbCrLf
sConsulta = sConsulta & "    IF @UON3 IS NULL" & vbCrLf
sConsulta = sConsulta & "      IF @UON2 IS NULL" & vbCrLf
sConsulta = sConsulta & "        SELECT P.* FROM PER P" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN USU U" & vbCrLf
sConsulta = sConsulta & "                   ON P.COD = U.PER" & vbCrLf
sConsulta = sConsulta & "         WHERE U.FSEP = 1" & vbCrLf
sConsulta = sConsulta & "           AND P.UON1 = @UON1" & vbCrLf
sConsulta = sConsulta & "      ORDER BY P.NOM, P.APE" & vbCrLf
sConsulta = sConsulta & "      ELSE" & vbCrLf
sConsulta = sConsulta & "        SELECT P.* FROM PER P" & vbCrLf
sConsulta = sConsulta & "           INNER JOIN USU U" & vbCrLf
sConsulta = sConsulta & "                   ON P.COD = U.PER" & vbCrLf
sConsulta = sConsulta & "         WHERE U.FSEP = 1" & vbCrLf
sConsulta = sConsulta & "           AND P.UON1 = @UON1" & vbCrLf
sConsulta = sConsulta & "           AND P.UON2 = @UON2" & vbCrLf
sConsulta = sConsulta & "      ORDER BY P.NOM, P.APE" & vbCrLf
sConsulta = sConsulta & "    ELSE" & vbCrLf
sConsulta = sConsulta & "      SELECT P.* FROM PER P" & vbCrLf
sConsulta = sConsulta & "         INNER JOIN USU U" & vbCrLf
sConsulta = sConsulta & "                 ON P.COD = U.PER" & vbCrLf
sConsulta = sConsulta & "       WHERE U.FSEP = 1" & vbCrLf
sConsulta = sConsulta & "         AND P.UON1 = @UON1" & vbCrLf
sConsulta = sConsulta & "         AND P.UON2 = @UON2" & vbCrLf
sConsulta = sConsulta & "         AND P.UON3 = @UON3" & vbCrLf
sConsulta = sConsulta & "    ORDER BY P.NOM, P.APE" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  SELECT P.* FROM PER P" & vbCrLf
sConsulta = sConsulta & "     INNER JOIN USU U" & vbCrLf
sConsulta = sConsulta & "             ON P.COD = U.PER" & vbCrLf
sConsulta = sConsulta & "   WHERE U.FSEP = 1" & vbCrLf
sConsulta = sConsulta & "  ORDER BY P.NOM, P.APE" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_DEVOLVER_EMPRESAS_PERSONA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_DEVOLVER_EMPRESAS_PERSONA]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSEP_DEVOLVER_EMPRESAS_PERSONA @PER VARCHAR(50), @ID INT OUTPUT, @SOLOUO tinyint = 0" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @UON1 VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @UON2 VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @UON3 VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "select @UON1 = uon1, @UON2 = uon2, @UON3 = uon3 from per where cod = @PER" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF (SELECT PED_OTRAS_EMP FROM USU WHERE PER = @PER) = 1" & vbCrLf
sConsulta = sConsulta & "  IF @UON3 IS NULL" & vbCrLf
sConsulta = sConsulta & "    IF @UON2 IS NULL" & vbCrLf
sConsulta = sConsulta & "      BEGIN" & vbCrLf
sConsulta = sConsulta & "      SELECT @ID = EMPRESA FROM UON1 WHERE COD = @UON1" & vbCrLf
sConsulta = sConsulta & "      IF @SOLOUO=1" & vbCrLf
sConsulta = sConsulta & "        SELECT ID, NIF, DEN, DIR, CP, POB, PAI, PROVI, ERP " & vbCrLf
sConsulta = sConsulta & "          FROM EMP " & vbCrLf
sConsulta = sConsulta & "              INNER JOIN (SELECT DISTINCT EMPRESA FROM (SELECT EMPRESA FROM UON1 UNION SELECT EMPRESA FROM UON2 UNION SELECT EMPRESA FROM UON3) U)DU ON EMP.ID = DU.EMPRESA" & vbCrLf
sConsulta = sConsulta & "        ORDER BY DEN" & vbCrLf
sConsulta = sConsulta & "      ELSE" & vbCrLf
sConsulta = sConsulta & "        SELECT ID, NIF, DEN, DIR, CP, POB, PAI, PROVI, ERP FROM EMP ORDER BY DEN" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "    ELSE" & vbCrLf
sConsulta = sConsulta & "      BEGIN" & vbCrLf
sConsulta = sConsulta & "      SELECT @ID = EMPRESA FROM UON2 WHERE UON1 = @UON1 AND COD = @UON2" & vbCrLf
sConsulta = sConsulta & "      IF @ID IS NULL" & vbCrLf
sConsulta = sConsulta & "        SELECT @ID = EMPRESA FROM UON1 WHERE COD = @UON1" & vbCrLf
sConsulta = sConsulta & "      IF @SOLOUO=1" & vbCrLf
sConsulta = sConsulta & "        SELECT ID, NIF, DEN, DIR, CP, POB, PAI, PROVI, ERP " & vbCrLf
sConsulta = sConsulta & "          FROM EMP " & vbCrLf
sConsulta = sConsulta & "              INNER JOIN (SELECT DISTINCT EMPRESA FROM (SELECT EMPRESA FROM UON1 UNION SELECT EMPRESA FROM UON2 UNION SELECT EMPRESA FROM UON3) U)DU ON EMP.ID = DU.EMPRESA" & vbCrLf
sConsulta = sConsulta & "        ORDER BY DEN" & vbCrLf
sConsulta = sConsulta & "      ELSE" & vbCrLf
sConsulta = sConsulta & "        SELECT ID, NIF, DEN, DIR, CP, POB, PAI, PROVI, ERP FROM EMP ORDER BY DEN" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "  ELSE" & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "    SELECT @ID = EMPRESA FROM UON3 WHERE UON1 = @UON1 AND UON2 = @UON2 AND COD = @UON3" & vbCrLf
sConsulta = sConsulta & "    IF @ID IS NULL" & vbCrLf
sConsulta = sConsulta & "      SELECT @ID = EMPRESA FROM UON2 WHERE UON1 = @UON1 AND COD = @UON2 " & vbCrLf
sConsulta = sConsulta & "    IF @ID IS NULL" & vbCrLf
sConsulta = sConsulta & "      SELECT @ID = EMPRESA FROM UON1 WHERE COD = @UON1" & vbCrLf
sConsulta = sConsulta & "      IF @SOLOUO=1" & vbCrLf
sConsulta = sConsulta & "        SELECT ID, NIF, DEN, DIR, CP, POB, PAI, PROVI, ERP " & vbCrLf
sConsulta = sConsulta & "          FROM EMP " & vbCrLf
sConsulta = sConsulta & "              INNER JOIN (SELECT DISTINCT EMPRESA FROM (SELECT EMPRESA FROM UON1 UNION SELECT EMPRESA FROM UON2 UNION SELECT EMPRESA FROM UON3) U)DU ON EMP.ID = DU.EMPRESA" & vbCrLf
sConsulta = sConsulta & "        ORDER BY DEN" & vbCrLf
sConsulta = sConsulta & "      ELSE" & vbCrLf
sConsulta = sConsulta & "        SELECT ID, NIF, DEN, DIR, CP, POB, PAI, PROVI, ERP FROM EMP ORDER BY DEN" & vbCrLf
sConsulta = sConsulta & "    END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  IF @UON3 IS NULL" & vbCrLf
sConsulta = sConsulta & "    IF @UON2 IS NULL" & vbCrLf
sConsulta = sConsulta & "      BEGIN" & vbCrLf
sConsulta = sConsulta & "      SELECT @ID = EMPRESA FROM UON1 WHERE COD = @UON1" & vbCrLf
sConsulta = sConsulta & "      SELECT ID, NIF, DEN, DIR, CP, POB, PAI, PROVI, ERP FROM (" & vbCrLf
sConsulta = sConsulta & "      SELECT DISTINCT EMPRESA FROM (" & vbCrLf
sConsulta = sConsulta & "      SELECT EMPRESA FROM UON1 WHERE COD = @UON1" & vbCrLf
sConsulta = sConsulta & "      UNION" & vbCrLf
sConsulta = sConsulta & "      SELECT EMPRESA FROM UON2 WHERE UON1 = @UON1" & vbCrLf
sConsulta = sConsulta & "      UNION" & vbCrLf
sConsulta = sConsulta & "      SELECT EMPRESA FROM UON3 WHERE UON1 = @UON1" & vbCrLf
sConsulta = sConsulta & "      UNION" & vbCrLf
sConsulta = sConsulta & "      SELECT @ID) U)DU " & vbCrLf
sConsulta = sConsulta & "        INNER JOIN EMP E" & vbCrLf
sConsulta = sConsulta & "                ON DU.EMPRESA = E.ID" & vbCrLf
sConsulta = sConsulta & "      ORDER BY DEN" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "    ELSE" & vbCrLf
sConsulta = sConsulta & "      BEGIN" & vbCrLf
sConsulta = sConsulta & "      SELECT @ID = EMPRESA FROM UON2 WHERE UON1 = @UON1 AND COD = @UON2" & vbCrLf
sConsulta = sConsulta & "      IF @ID IS NULL" & vbCrLf
sConsulta = sConsulta & "        SELECT @ID = EMPRESA FROM UON1 WHERE COD = @UON1" & vbCrLf
sConsulta = sConsulta & "      SELECT ID, NIF, DEN, DIR, CP, POB, PAI, PROVI, ERP FROM (" & vbCrLf
sConsulta = sConsulta & "      SELECT DISTINCT EMPRESA FROM (" & vbCrLf
sConsulta = sConsulta & "      SELECT EMPRESA FROM UON2 WHERE UON1 = @UON1 AND COD = @UON2" & vbCrLf
sConsulta = sConsulta & "      UNION" & vbCrLf
sConsulta = sConsulta & "      SELECT EMPRESA FROM UON3 WHERE UON1 = @UON1 AND UON2 = @UON2" & vbCrLf
sConsulta = sConsulta & "      UNION" & vbCrLf
sConsulta = sConsulta & "      SELECT @ID) U)DU " & vbCrLf
sConsulta = sConsulta & "        INNER JOIN EMP E" & vbCrLf
sConsulta = sConsulta & "                ON DU.EMPRESA = E.ID" & vbCrLf
sConsulta = sConsulta & "      ORDER BY DEN" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "  ELSE" & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "    SELECT @ID = EMPRESA FROM UON3 WHERE UON1 = @UON1 AND UON2 = @UON2 AND COD = @UON3" & vbCrLf
sConsulta = sConsulta & "    IF @ID IS NULL" & vbCrLf
sConsulta = sConsulta & "      SELECT @ID = EMPRESA FROM UON2 WHERE UON1 = @UON1 AND COD = @UON2 " & vbCrLf
sConsulta = sConsulta & "    IF @ID IS NULL" & vbCrLf
sConsulta = sConsulta & "      SELECT @ID = EMPRESA FROM UON1 WHERE COD = @UON1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "      SELECT ID, NIF, DEN, DIR, CP, POB, PAI, PROVI, ERP FROM (" & vbCrLf
sConsulta = sConsulta & "      SELECT DISTINCT EMPRESA FROM (" & vbCrLf
sConsulta = sConsulta & "      SELECT EMPRESA FROM UON3 WHERE UON1 = @UON1 AND UON2 = @UON2 AND COD = @UON3" & vbCrLf
sConsulta = sConsulta & "      UNION" & vbCrLf
sConsulta = sConsulta & "      SELECT @ID) U)DU " & vbCrLf
sConsulta = sConsulta & "        INNER JOIN EMP E" & vbCrLf
sConsulta = sConsulta & "                ON DU.EMPRESA = E.ID" & vbCrLf
sConsulta = sConsulta & "      ORDER BY DEN" & vbCrLf
sConsulta = sConsulta & "    END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub
Public Function CodigoDeActualizacion2_15_00_04A2_15_00_05() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim bBool As Boolean

On Error GoTo error
    

sConsulta = "BEGIN TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = True
ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"


    
frmProgreso.lblDetalle = "Modificar stored procedures"
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
frmProgreso.lblDetalle.Refresh
    
    V_2_15_Storeds_05

    
frmProgreso.lblDetalle = "Actualizamos tablas"
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
frmProgreso.lblDetalle.Refresh

sConsulta = " ALTER TABLE dbo.PROCE_GRUPO ADD CONSTRAINT" & vbCrLf
sConsulta = sConsulta & "   DF_PROCE_GRUPO_ADJUDICADO DEFAULT (0) FOR ADJUDICADO"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "UPDATE PROCE_GRUPO SET ADJUDICADO=0 WHERE ADJUDICADO IS NULL"
ExecuteSQL gRDOCon, sConsulta
        
sConsulta = " ALTER TABLE dbo.PROCE_GRUPO ALTER COLUMN ADJUDICADO float NOT NULL" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
    
sConsulta = "UPDATE ORDEN_ENTREGA SET RECEPTOR=P.PER FROM ORDEN_ENTREGA OE INNER JOIN PEDIDO P ON OE.PEDIDO=P.ID AND OE.RECEPTOR IS NULL"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "UPDATE VERSION SET NUM ='2.15.00.05'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
ExecuteSQL gRDOCon, sConsulta


sConsulta = "COMMIT TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = False

CodigoDeActualizacion2_15_00_04A2_15_00_05 = Not bCancelado
Exit Function
    
error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_15_00_04A2_15_00_05 = False

End Function


Private Sub V_2_15_Storeds_05()
Dim sConsulta As String

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_AR_AHORROS_CALCULAR]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_AR_AHORROS_CALCULAR]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE   PROCEDURE  SP_AR_AHORROS_CALCULAR ( @ANYO SMALLINT,@GMN1 VARCHAR(50), @PROCE INT,@RES INT OUTPUT)  AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE @EST SMALLINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ITEM INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @CAMBIO AS FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ADJDIR SMALLINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @FECULT DATETIME" & vbCrLf
sConsulta = sConsulta & "DECLARE @DEN VARCHAR(200)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PORTAL SMALLINT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @PORTAL=INSTWEB FROM PARGEN_INTERNO WHERE ID=1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @EST=EST,@CAMBIO=CAMBIO,@ADJDIR=ADJDIR,@FECULT=FECULTREU,@DEN=DEN FROM PROCE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE AND CALCPEND=1 AND EST>=11 AND EST<20" & vbCrLf
sConsulta = sConsulta & "--IF @@FETCH_STATUS<>0 GOTO ERR_" & vbCrLf
sConsulta = sConsulta & "   IF @EST = 11 " & vbCrLf
sConsulta = sConsulta & "     BEGIN" & vbCrLf
sConsulta = sConsulta & "          DECLARE Cur_ITEM CURSOR LOCAL FOR SELECT ID FROM ITEM WHERE ITEM.ANYO=@ANYO AND ITEM. GMN1=@GMN1 AND ITEM.PROCE=@PROCE AND ITEM.EST=1 AND ITEM.CONF=1" & vbCrLf
sConsulta = sConsulta & "            AND NOT EXISTS (SELECT * FROM AR_ITEM WHERE AR_ITEM.ANYO=@ANYO AND AR_ITEM.GMN1=@GMN1 AND AR_ITEM.PROCE=@PROCE AND AR_ITEM.ITEM=ITEM.ID) " & vbCrLf
sConsulta = sConsulta & "          OPEN Cur_ITEM " & vbCrLf
sConsulta = sConsulta & "          FETCH NEXT FROM Cur_ITEM INTO @ITEM" & vbCrLf
sConsulta = sConsulta & "          WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "           BEGIN" & vbCrLf
sConsulta = sConsulta & "                 INSERT INTO AR_ITEM (ANYO,GMN1,PROCE,ITEM,GRUPO,ADJDIR,GMN2,GMN3,GMN4,PRES,PREC) " & vbCrLf
sConsulta = sConsulta & "                    SELECT ANYO,GMN1,PROCE,ID,GRUPO,@ADJDIR,GMN2,GMN3,GMN4, SUM(PRESUP), SUM(ADJUD) FROM" & vbCrLf
sConsulta = sConsulta & "                    (SELECT I.ANYO,I.GMN1,I.PROCE,I.ID,I.GRUPO,I.GMN2,I.GMN3,I.GMN4, (SUM(IA.PORCEN/100) * I.CANT * I.PREC) / @CAMBIO AS PRESUP" & vbCrLf
sConsulta = sConsulta & "                        , (SUM(IA.ADJUDICADO) / PO.CAMBIO) / @CAMBIO AS ADJUD FROM ITEM_ADJ IA INNER JOIN ITEM I ON IA.ANYO=I.ANYO AND IA.GMN1=I.GMN1 AND IA.PROCE=I.PROCE AND IA.ITEM=I.ID " & vbCrLf
sConsulta = sConsulta & "                          INNER JOIN PROCE_OFE PO ON  IA.ANYO=PO.ANYO AND IA.GMN1=PO.GMN1 AND IA.PROCE=PO.PROCE AND IA.PROVE=PO.PROVE AND IA.OFE=PO.OFE" & vbCrLf
sConsulta = sConsulta & "                           WHERE IA.ANYO= @ANYO AND IA.GMN1= @GMN1 AND IA.PROCE=@PROCE AND IA.ITEM=@ITEM AND IA.PORCEN <>0 " & vbCrLf
sConsulta = sConsulta & "                              GROUP BY I.ANYO,I.GMN1,I.PROCE,I.ID,I.GRUPO,I.GMN2,I.GMN3,I.GMN4,I.CANT , I.PREC,PO.CAMBIO) TABLA_INTER" & vbCrLf
sConsulta = sConsulta & "                              GROUP BY ANYO,GMN1,PROCE,ID,GRUPO,GMN2,GMN3,GMN4" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "                EXECUTE SP_CIERRE_PARC @ANYO,@GMN1,@PROCE,@ITEM,@RES output" & vbCrLf
sConsulta = sConsulta & "               IF @RES<>0  AND @RES IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "                   BEGIN" & vbCrLf
sConsulta = sConsulta & "                   CLOSE Cur_ITEM" & vbCrLf
sConsulta = sConsulta & "                   DEALLOCATE Cur_ITEM" & vbCrLf
sConsulta = sConsulta & "                   GOTO ERR_" & vbCrLf
sConsulta = sConsulta & "                   END" & vbCrLf
sConsulta = sConsulta & "                 " & vbCrLf
sConsulta = sConsulta & "            FETCH NEXT FROM Cur_ITEM INTO @ITEM" & vbCrLf
sConsulta = sConsulta & "            END " & vbCrLf
sConsulta = sConsulta & "            CLOSE Cur_ITEM" & vbCrLf
sConsulta = sConsulta & "            DEALLOCATE Cur_ITEM" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "            DELETE FROM AR_GRUPO WHERE ANYO= @ANYO AND GMN1= @GMN1 AND PROCE= @PROCE     " & vbCrLf
sConsulta = sConsulta & "            INSERT INTO AR_GRUPO (ANYO,GMN1,PROCE,GRUPO,PRES,PREC)  SELECT PG.ANYO,PG.GMN1,PG.PROCE,PG.COD,SUM(AI.PRES),PG.ADJUDICADO / @CAMBIO" & vbCrLf
sConsulta = sConsulta & "               FROM PROCE_GRUPO PG INNER JOIN AR_ITEM AI ON PG.ANYO=AI.ANYO AND PG.GMN1=AI.GMN1 AND PG.PROCE=AI.PROCE AND PG.COD=AI.GRUPO" & vbCrLf
sConsulta = sConsulta & "                WHERE PG.ANYO= @ANYO AND PG.GMN1= @GMN1 AND PG.PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "                GROUP BY PG.ANYO,PG.GMN1,PG.PROCE,PG.COD,PG.ADJUDICADO " & vbCrLf
sConsulta = sConsulta & "         " & vbCrLf
sConsulta = sConsulta & "            EXECUTE SP_AR_PROCE_PARC @ANYO,@GMN1,@PROCE,@RES output" & vbCrLf
sConsulta = sConsulta & "            IF @RES<>0  AND @RES IS NOT NULL GOTO ERR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "            EXECUTE SP_AR_PROVE @ANYO,@GMN1,@PROCE,@RES output" & vbCrLf
sConsulta = sConsulta & "            IF @RES<>0  AND @RES IS NOT NULL GOTO ERR_" & vbCrLf
sConsulta = sConsulta & "   " & vbCrLf
sConsulta = sConsulta & "       END " & vbCrLf
sConsulta = sConsulta & "  ELSE" & vbCrLf
sConsulta = sConsulta & "     BEGIN" & vbCrLf
sConsulta = sConsulta & "       --Ahora almacenamos el contenido en las tablas de resultados para informes" & vbCrLf
sConsulta = sConsulta & "        DELETE FROM AR_ITEM WHERE ANYO= @ANYO AND GMN1= @GMN1 AND PROCE= @PROCE" & vbCrLf
sConsulta = sConsulta & "        DELETE FROM AR_GRUPO WHERE ANYO= @ANYO AND GMN1= @GMN1 AND PROCE= @PROCE" & vbCrLf
sConsulta = sConsulta & "        DELETE FROM AR_PROCE WHERE ANYO= @ANYO AND GMN1= @GMN1 AND COD= @PROCE" & vbCrLf
sConsulta = sConsulta & "       --AR_ITEM" & vbCrLf
sConsulta = sConsulta & "        INSERT INTO AR_ITEM (ANYO,GMN1,PROCE,ITEM,GRUPO,ADJDIR,GMN2,GMN3,GMN4,PRES,PREC) " & vbCrLf
sConsulta = sConsulta & "           SELECT   ANYO,GMN1,PROCE,ID,GRUPO,@ADJDIR,GMN2,GMN3,GMN4, SUM(PRESUP),SUM( ADJUD) FROM" & vbCrLf
sConsulta = sConsulta & "          (SELECT I.ANYO,I.GMN1,I.PROCE,I.ID,I.GRUPO,I.GMN2,I.GMN3,I.GMN4, (SUM(IA.PORCEN/100) * I.CANT * I.PREC) / @CAMBIO AS PRESUP, (SUM(IA.ADJUDICADO) / PO.CAMBIO) / @CAMBIO AS ADJUD" & vbCrLf
sConsulta = sConsulta & "            FROM ITEM_ADJ IA INNER JOIN ITEM I ON IA.ANYO=I.ANYO AND IA.GMN1=I.GMN1 AND IA.PROCE=I.PROCE AND IA.ITEM=I.ID AND I.CONF=1" & vbCrLf
sConsulta = sConsulta & "             INNER JOIN PROCE_OFE PO ON  IA.ANYO=PO.ANYO AND IA.GMN1=PO.GMN1 AND IA.PROCE=PO.PROCE AND IA.PROVE=PO.PROVE AND IA.OFE=PO.OFE" & vbCrLf
sConsulta = sConsulta & "             WHERE IA.ANYO= @ANYO AND IA.GMN1= @GMN1 AND IA.PROCE=@PROCE AND IA.PORCEN <>0" & vbCrLf
sConsulta = sConsulta & "            GROUP BY I.ANYO,I.GMN1,I.PROCE,I.ID,I.GRUPO,I.GMN2,I.GMN3,I.GMN4,I.CANT , I.PREC,PO.CAMBIO) TABLA_INTER" & vbCrLf
sConsulta = sConsulta & "            GROUP BY ANYO,GMN1,PROCE,ID,GRUPO,GMN2,GMN3,GMN4" & vbCrLf
sConsulta = sConsulta & "       --AR_GRUPO" & vbCrLf
sConsulta = sConsulta & "        INSERT INTO AR_GRUPO (ANYO,GMN1,PROCE,GRUPO,PRES,PREC)  SELECT PG.ANYO,PG.GMN1,PG.PROCE,PG.COD,SUM(AI.PRES),PG.ADJUDICADO / @CAMBIO" & vbCrLf
sConsulta = sConsulta & "           FROM PROCE_GRUPO PG INNER JOIN AR_ITEM AI ON PG.ANYO=AI.ANYO AND PG.GMN1=AI.GMN1 AND PG.PROCE=AI.PROCE AND PG.COD=AI.GRUPO" & vbCrLf
sConsulta = sConsulta & "           WHERE PG.ANYO= @ANYO AND PG.GMN1= @GMN1 AND PG.PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "           GROUP BY PG.ANYO,PG.GMN1,PG.PROCE,PG.COD,PG.ADJUDICADO " & vbCrLf
sConsulta = sConsulta & "        " & vbCrLf
sConsulta = sConsulta & "         EXECUTE SP_CIERRE @ANYO,@GMN1,@PROCE,@RES output" & vbCrLf
sConsulta = sConsulta & "         IF @RES<>0  AND @RES IS NOT NULL GOTO ERR_" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "      END " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & " IF @PORTAL =2 " & vbCrLf
sConsulta = sConsulta & " BEGIN" & vbCrLf
sConsulta = sConsulta & "    EXECUTE SP_AR_VOLUMENES @ANYO,@GMN1,@PROCE,@FECULT,@DEN,@RES" & vbCrLf
sConsulta = sConsulta & "    IF @RES<>0  AND @RES IS NOT NULL  GOTO ERR_" & vbCrLf
sConsulta = sConsulta & " END " & vbCrLf
sConsulta = sConsulta & " UPDATE PROCE SET CALCPEND=0 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ERR_:" & vbCrLf
sConsulta = sConsulta & "IF @@ERROR <> 0 SET @RES=@@ERROR" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "Return" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

