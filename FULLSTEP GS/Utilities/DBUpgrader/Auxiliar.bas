Attribute VB_Name = "Auxiliar"

'*******************************************************************************************************************
'**************                                 VERSION 2.10
'*******************************************************************************************************************

Private Function CodigoDeActualizacion2_9_5A2_10_00_0() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String


On Error GoTo error
    

sConsulta = "BEGIN TRANSACTION"
gRDOCon.Execute sConsulta, rdExecDirect

gRDOCon.Execute "SET XACT_ABORT OFF", rdExecDirect
bTransaccionEnCurso = True

'CAMBIOS
frmProgreso.lblDetalle = "Creando nuevas tablas de presupuestos"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value > 9900 Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_10_CrearTablasPresupuestos 'Crear tablas de presupuestos

V_10_ActulizarITEM_PRES1
V_10_ActulizarITEM_PRES2
V_10_ActulizarITEM_PRES3
V_10_ActulizarITEM_PRES4


'FIN CAMBIOS
sConsulta = "UPDATE VERSION SET NUM ='2.10.00.0'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
gRDOCon.Execute sConsulta, rdExecDirect

gRDOCon.Execute "COMMIT TRANSACTION", rdExecDirect
bTransaccionEnCurso = False

CodigoDeActualizacion2_9_5A2_10_00_0 = True
Exit Function
    
error:

    If bTransaccionEnCurso Then
        gRDOCon.Execute "ROLLBACK TRANSACTION", rdExecDirect
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_9_5A2_10_00_0 = False

End Function

'Creamos tablas nuevas de presupuestos
Private Sub V_10_CrearTablasPresupuestos()
Dim sConsulta As String
'sConsulta = "if exists (select * from sysobjects where id = object_id(N'[dbo].[PROCE_PROVE_PET_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1) drop trigger [dbo].[PROCE_PROVE_PET_TG_INS]"
'gRDOCon.Execute sConsulta, rdExecDirect

'--- 'Updating dbo.PRES1_NIV1'
sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE [dbo].[PRES1_NIV1]" & vbCrLf
sConsulta = sConsulta & "    (" & vbCrLf
sConsulta = sConsulta & "  [ID] [int] NOT NULL IDENTITY (1, 1) ," & vbCrLf
sConsulta = sConsulta & "  [ANYO] [smallint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "  [COD] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodPRESPROY1) & ") NOT NULL ," & vbCrLf
sConsulta = sConsulta & "  [DEN] [varchar] (100) NOT NULL ," & vbCrLf
sConsulta = sConsulta & "  [UON1] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodUON1) & ") NULL ," & vbCrLf
sConsulta = sConsulta & "  [UON2] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodUON2) & ") NULL ," & vbCrLf
sConsulta = sConsulta & "  [UON3] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodUON3) & ") NULL ," & vbCrLf
sConsulta = sConsulta & "  [IMP] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "  [OBJ] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "  [FECACT] [datetime] NULL " & vbCrLf
sConsulta = sConsulta & ")" & vbCrLf
sConsulta = sConsulta & "CREATE  INDEX [IX_PRES1_NIV1_01] ON [dbo].[PRES1_NIV1]([ANYO], [DEN], [UON1], [UON2], [UON3]) ON [PRIMARY]" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[PRES1_NIV1] WITH NOCHECK ADD  CONSTRAINT [PK_PRES1_NIV1] PRIMARY KEY CLUSTERED ([ID]) ," & vbCrLf
sConsulta = sConsulta & "CONSTRAINT [UC_PRES1_NIV1] UNIQUE NONCLUSTERED ([ANYO], [COD], [UON1], [UON2], [UON3]) " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
 
sConsulta = ""
'-- 'Updating dbo.PRES2_NIV1'
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE [dbo].[PRES2_NIV1]" & vbCrLf
sConsulta = sConsulta & "    (" & vbCrLf
sConsulta = sConsulta & "  [ID] [int] NOT NULL IDENTITY (1, 1) ," & vbCrLf
sConsulta = sConsulta & "  [ANYO] [smallint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "  [COD] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodPRESCON1) & ") NOT NULL ," & vbCrLf
sConsulta = sConsulta & "  [DEN] [varchar] (100) NOT NULL ," & vbCrLf
sConsulta = sConsulta & "  [UON1] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodUON1) & ") NULL ," & vbCrLf
sConsulta = sConsulta & "  [UON2] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodUON2) & ") NULL ," & vbCrLf
sConsulta = sConsulta & "  [UON3] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodUON3) & ") NULL ," & vbCrLf
sConsulta = sConsulta & "  [IMP] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "  [OBJ] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "  [FECACT] [datetime] NULL " & vbCrLf
sConsulta = sConsulta & ")" & vbCrLf
sConsulta = sConsulta & "CREATE  INDEX [IX_PRES2_NIV1_01] ON [dbo].[PRES2_NIV1]([ANYO], [DEN], [UON1], [UON2], [UON3]) ON [PRIMARY]" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[PRES2_NIV1] WITH NOCHECK ADD  CONSTRAINT [PK_PRES2_NIV1] PRIMARY KEY CLUSTERED ([ID]) ," & vbCrLf
sConsulta = sConsulta & "CONSTRAINT [UC_PRES2_NIV1] UNIQUE NONCLUSTERED ([ANYO], [COD], [UON1], [UON2], [UON3]) " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

 sConsulta = ""
'-- 'Updating dbo.PRES3_NIV1'
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "CREATE TABLE [dbo].[PRES3_NIV1]" & vbCrLf
sConsulta = sConsulta & "    (" & vbCrLf
sConsulta = sConsulta & "  [ID] [int] NOT NULL IDENTITY (1, 1) ," & vbCrLf
sConsulta = sConsulta & "  [COD] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodPRESCONCEP31) & ") NOT NULL ," & vbCrLf
sConsulta = sConsulta & "  [DEN] [varchar] (100) NOT NULL ," & vbCrLf
sConsulta = sConsulta & "  [UON1] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodUON1) & ") NULL ," & vbCrLf
sConsulta = sConsulta & "  [UON2] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodUON2) & ") NULL ," & vbCrLf
sConsulta = sConsulta & "  [UON3] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodUON3) & ") NULL ," & vbCrLf
sConsulta = sConsulta & "  [IMP] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "  [OBJ] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "  [FECACT] [datetime] NULL " & vbCrLf
sConsulta = sConsulta & ")" & vbCrLf
sConsulta = sConsulta & "CREATE  INDEX [IX_PRES3_NIV1_01] ON [dbo].[PRES3_NIV1]([DEN], [UON1], [UON2], [UON3]) ON [PRIMARY]" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[PRES3_NIV1] WITH NOCHECK ADD  CONSTRAINT [PK_PRES3_NIV1] PRIMARY KEY CLUSTERED ([ID]) ," & vbCrLf
sConsulta = sConsulta & " CONSTRAINT [UC_PRES3_NIV1] UNIQUE NONCLUSTERED ([COD], [UON1], [UON2], [UON3]) " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

'--PRINT 'Updating dbo.PRES4_NIV1'
sConsulta = "CREATE TABLE [dbo].[PRES4_NIV1]" & vbCrLf
sConsulta = sConsulta & "    (" & vbCrLf
sConsulta = sConsulta & "  [ID] [int] NOT NULL IDENTITY (1, 1) ," & vbCrLf
sConsulta = sConsulta & "  [COD] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodPRESCONCEP41) & ") NOT NULL ," & vbCrLf
sConsulta = sConsulta & "  [DEN] [varchar] (100) NOT NULL ," & vbCrLf
sConsulta = sConsulta & "  [UON1] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodUON1) & ") NULL ," & vbCrLf
sConsulta = sConsulta & "  [UON2] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodUON2) & ") NULL ," & vbCrLf
sConsulta = sConsulta & "  [UON3] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodUON3) & ") NULL ," & vbCrLf
sConsulta = sConsulta & "  [IMP] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "  [OBJ] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "  [FECACT] [datetime] NULL " & vbCrLf
sConsulta = sConsulta & ")" & vbCrLf
sConsulta = sConsulta & " CREATE  INDEX [IX_PRES4_NIV1_01] ON [dbo].[PRES4_NIV1]([DEN], [UON1], [UON2], [UON3]) ON [PRIMARY]" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[PRES4_NIV1] WITH NOCHECK ADD  CONSTRAINT [PK_PRES4_NIV1] PRIMARY KEY CLUSTERED ([ID]) ," & vbCrLf
sConsulta = sConsulta & " CONSTRAINT [UC_PRES4_NIV1] UNIQUE NONCLUSTERED ([COD], [UON1], [UON2], [UON3]) " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

 sConsulta = ""
'--PRINT 'Updating dbo.PRES1_NIV2'
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE [dbo].[PRES1_NIV2]" & vbCrLf
sConsulta = sConsulta & "    (" & vbCrLf
sConsulta = sConsulta & "  [ID] [int] NOT NULL IDENTITY (1, 1) ," & vbCrLf
sConsulta = sConsulta & "  [ANYO] [smallint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "  [PRES1] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodPRESPROY1) & ") NOT NULL ," & vbCrLf
sConsulta = sConsulta & "  [COD] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodPRESPROY2) & ") NOT NULL ," & vbCrLf
sConsulta = sConsulta & "  [UON1] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodUON1) & ") NULL ," & vbCrLf
sConsulta = sConsulta & "  [UON2] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodUON2) & ") NULL ," & vbCrLf
sConsulta = sConsulta & "  [UON3] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodUON3) & ") NULL ," & vbCrLf
sConsulta = sConsulta & "  [DEN] [varchar] (100) NOT NULL ," & vbCrLf
sConsulta = sConsulta & "  [IMP] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "  [OBJ] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "  [FECACT] [datetime] NULL " & vbCrLf
sConsulta = sConsulta & ")" & vbCrLf
sConsulta = sConsulta & "CREATE  INDEX [IX_PRES1_NIV2_01] ON [dbo].[PRES1_NIV2]([ANYO], [PRES1], [DEN], [UON1], [UON2], [UON3]) ON [PRIMARY]" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[PRES1_NIV2] WITH NOCHECK ADD  CONSTRAINT [PK_PRES1_NIV2] PRIMARY KEY CLUSTERED ([ID]) ," & vbCrLf
sConsulta = sConsulta & " CONSTRAINT [UC_PRES1_NIV2] UNIQUE NONCLUSTERED ([ANYO], [PRES1], [COD], [UON1], [UON2], [UON3]) " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

 sConsulta = ""
'--PRINT 'Updating dbo.PRES2_NIV2'
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE [dbo].[PRES2_NIV2]" & vbCrLf
sConsulta = sConsulta & "    (" & vbCrLf
sConsulta = sConsulta & "  [ID] [int] NOT NULL IDENTITY (1, 1) ," & vbCrLf
sConsulta = sConsulta & "  [ANYO] [smallint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "  [PRES1] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodPRESCON1) & ") NOT NULL ," & vbCrLf
sConsulta = sConsulta & "  [COD] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodPRESCON2) & ") NOT NULL ," & vbCrLf
sConsulta = sConsulta & "  [DEN] [varchar] (100) NOT NULL ," & vbCrLf
sConsulta = sConsulta & "  [UON1] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodUON1) & ") NULL ," & vbCrLf
sConsulta = sConsulta & "  [UON2] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodUON2) & ") NULL ," & vbCrLf
sConsulta = sConsulta & "  [UON3] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodUON3) & ") NULL ," & vbCrLf
sConsulta = sConsulta & "  [IMP] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "  [OBJ] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "  [FECACT] [datetime] NULL " & vbCrLf
sConsulta = sConsulta & ")" & vbCrLf
sConsulta = sConsulta & "CREATE  INDEX [IX_PRES2_NIV2_01] ON [dbo].[PRES2_NIV2]([ANYO], [PRES1], [DEN], [UON1], [UON2], [UON3]) ON [PRIMARY]" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[PRES2_NIV2] WITH NOCHECK ADD  CONSTRAINT [PK_PRES2_NIV2] PRIMARY KEY CLUSTERED ([ID]) ," & vbCrLf
sConsulta = sConsulta & " CONSTRAINT [UC_PRES2_NIV2] UNIQUE NONCLUSTERED ([ANYO], [PRES1], [COD], [UON1], [UON2], [UON3]) " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

 sConsulta = ""
'--PRINT 'Updating dbo.PRES3_NIV2'
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE [dbo].[PRES3_NIV2]" & vbCrLf
sConsulta = sConsulta & "    (" & vbCrLf
sConsulta = sConsulta & "  [ID] [int] NOT NULL IDENTITY (1, 1) ," & vbCrLf
sConsulta = sConsulta & "  [PRES1] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodPRESCONCEP31) & ") NOT NULL ," & vbCrLf
sConsulta = sConsulta & "  [COD] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodPRESCONCEP32) & ") NOT NULL ," & vbCrLf
sConsulta = sConsulta & "  [DEN] [varchar] (100) NOT NULL ," & vbCrLf
sConsulta = sConsulta & "  [UON1] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodUON1) & ") NULL ," & vbCrLf
sConsulta = sConsulta & "  [UON2] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodUON2) & ") NULL ," & vbCrLf
sConsulta = sConsulta & "  [UON3] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodUON3) & ") NULL ," & vbCrLf
sConsulta = sConsulta & "  [IMP] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "  [OBJ] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "  [FECACT] [datetime] NULL " & vbCrLf
sConsulta = sConsulta & ")" & vbCrLf
sConsulta = sConsulta & "CREATE  INDEX [IX_PRES3_NIV2_01] ON [dbo].[PRES3_NIV2]([PRES1], [DEN], [UON1], [UON2], [UON3]) ON [PRIMARY]" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[PRES3_NIV2] WITH NOCHECK ADD  CONSTRAINT [PK_PRES3_NIV2] PRIMARY KEY CLUSTERED ([ID]) ," & vbCrLf
sConsulta = sConsulta & " CONSTRAINT [UC_PRES3_NIV2] UNIQUE NONCLUSTERED ([PRES1], [COD], [UON1], [UON2], [UON3]) " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

'--PRINT 'Updating dbo.PRES4_NIV2'
sConsulta = "" & vbCrLf
sConsulta = sConsulta & "CREATE TABLE [dbo].[PRES4_NIV2]" & vbCrLf
sConsulta = sConsulta & "    (" & vbCrLf
sConsulta = sConsulta & "  [ID] [int] NOT NULL IDENTITY (1, 1) ," & vbCrLf
sConsulta = sConsulta & "  [PRES1] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodPRESCONCEP41) & ") NOT NULL ," & vbCrLf
sConsulta = sConsulta & "  [COD] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodPRESCONCEP42) & ") NOT NULL ," & vbCrLf
sConsulta = sConsulta & "  [DEN] [varchar] (100) NOT NULL ," & vbCrLf
sConsulta = sConsulta & "  [UON1] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodUON1) & ") NULL ," & vbCrLf
sConsulta = sConsulta & "  [UON2] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodUON2) & ") NULL ," & vbCrLf
sConsulta = sConsulta & "  [UON3] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodUON3) & ") NULL ," & vbCrLf
sConsulta = sConsulta & "  [IMP] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "  [OBJ] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "  [FECACT] [datetime] NULL " & vbCrLf
sConsulta = sConsulta & ")" & vbCrLf
sConsulta = sConsulta & "CREATE  INDEX [IX_PRES4_NIV2_01] ON [dbo].[PRES4_NIV2]([PRES1], [DEN], [UON1], [UON2], [UON3]) ON [PRIMARY]" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[PRES4_NIV2] WITH NOCHECK ADD  CONSTRAINT [PK_PRES4_NIV2] PRIMARY KEY CLUSTERED ([ID]) ," & vbCrLf
sConsulta = sConsulta & " CONSTRAINT [UC_PRES4_NIV2] UNIQUE NONCLUSTERED ([PRES1], [COD], [UON1], [UON2], [UON3]) " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

 sConsulta = ""
'--PRINT 'Updating dbo.PRES1_NIV3'
sConsulta = sConsulta & "CREATE TABLE [dbo].[PRES1_NIV3]" & vbCrLf
sConsulta = sConsulta & "    (" & vbCrLf
sConsulta = sConsulta & "  [ID] [int] NOT NULL IDENTITY (1, 1) ," & vbCrLf
sConsulta = sConsulta & "  [ANYO] [smallint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "  [PRES1] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodPRESPROY1) & ") NOT NULL ," & vbCrLf
sConsulta = sConsulta & "  [PRES2] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodPRESPROY2) & ") NOT NULL ," & vbCrLf
sConsulta = sConsulta & "  [COD] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodPRESPROY3) & ") NOT NULL ," & vbCrLf
sConsulta = sConsulta & "  [DEN] [varchar] (100) NOT NULL ," & vbCrLf
sConsulta = sConsulta & "  [UON1] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodUON1) & ") NULL ," & vbCrLf
sConsulta = sConsulta & "  [UON2] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodUON2) & ") NULL ," & vbCrLf
sConsulta = sConsulta & "  [UON3] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodUON3) & ") NULL ," & vbCrLf
sConsulta = sConsulta & "  [IMP] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "  [OBJ] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "  [FECACT] [datetime] NULL " & vbCrLf
sConsulta = sConsulta & ")" & vbCrLf
sConsulta = sConsulta & "CREATE  INDEX [IX_PRES1_NIV3_01] ON [dbo].[PRES1_NIV3]([ANYO], [PRES1], [PRES2], [DEN], [UON1], [UON2], [UON3]) ON [PRIMARY]" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[PRES1_NIV3] WITH NOCHECK ADD  CONSTRAINT [PK_PRES1_NIV3] PRIMARY KEY CLUSTERED ([ID]) ," & vbCrLf
sConsulta = sConsulta & " CONSTRAINT [UC_PRES1_NIV3] UNIQUE NONCLUSTERED ([ANYO], [PRES1], [PRES2], [COD], [UON1], [UON2], [UON3]) "
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = ""
'--PRINT 'Updating dbo.PRES2_NIV3'
sConsulta = sConsulta & "CREATE TABLE [dbo].[PRES2_NIV3]" & vbCrLf
sConsulta = sConsulta & "    (" & vbCrLf
sConsulta = sConsulta & "  [ID] [int] NOT NULL IDENTITY (1, 1) ," & vbCrLf
sConsulta = sConsulta & "  [ANYO] [smallint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "  [PRES1] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodPRESCON1) & ") NOT NULL ," & vbCrLf
sConsulta = sConsulta & "  [PRES2] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodPRESCON2) & ") NOT NULL ," & vbCrLf
sConsulta = sConsulta & "  [COD] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodPRESCON3) & ") NOT NULL ," & vbCrLf
sConsulta = sConsulta & "  [DEN] [varchar] (100) NOT NULL ," & vbCrLf
sConsulta = sConsulta & "  [UON1] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodUON1) & ") NULL ," & vbCrLf
sConsulta = sConsulta & "  [UON2] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodUON2) & ") NULL ," & vbCrLf
sConsulta = sConsulta & "  [UON3] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodUON3) & ") NULL ," & vbCrLf
sConsulta = sConsulta & "  [IMP] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "  [OBJ] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "  [FECACT] [datetime] NULL " & vbCrLf
sConsulta = sConsulta & ")" & vbCrLf
sConsulta = sConsulta & "CREATE  INDEX [IX_PRES2_NIV3_01] ON [dbo].[PRES2_NIV3]([ANYO], [PRES1], [PRES2], [DEN], [UON1], [UON2], [UON3]) ON [PRIMARY]" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[PRES2_NIV3] WITH NOCHECK ADD  CONSTRAINT [PK_PRES2_NIV3] PRIMARY KEY CLUSTERED ([ID]) ," & vbCrLf
sConsulta = sConsulta & " CONSTRAINT [UC_PRES2_NIV3] UNIQUE NONCLUSTERED ([ANYO], [PRES1], [PRES2], [COD], [UON1], [UON2], [UON3]) " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

 sConsulta = ""
'--PRINT 'Updating dbo.PRES3_NIV3'
sConsulta = sConsulta & "CREATE TABLE [dbo].[PRES3_NIV3]" & vbCrLf
sConsulta = sConsulta & "    (" & vbCrLf
sConsulta = sConsulta & "  [ID] [int] NOT NULL IDENTITY (1, 1) ," & vbCrLf
sConsulta = sConsulta & "  [PRES1] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodPRESCONCEP31) & ") NOT NULL ," & vbCrLf
sConsulta = sConsulta & "  [PRES2] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodPRESCONCEP32) & ") NOT NULL ," & vbCrLf
sConsulta = sConsulta & "  [COD] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodPRESCONCEP33) & ") NOT NULL ," & vbCrLf
sConsulta = sConsulta & "  [DEN] [varchar] (100) NOT NULL ," & vbCrLf
sConsulta = sConsulta & "  [UON1] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodUON1) & ") NULL ," & vbCrLf
sConsulta = sConsulta & "  [UON2] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodUON2) & ") NULL ," & vbCrLf
sConsulta = sConsulta & "  [UON3] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodUON3) & ") NULL ," & vbCrLf
sConsulta = sConsulta & "  [IMP] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "  [OBJ] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "  [FECACT] [datetime] NULL " & vbCrLf
sConsulta = sConsulta & ")" & vbCrLf
sConsulta = sConsulta & "CREATE  INDEX [IX_PRES3_NIV3_01] ON [dbo].[PRES3_NIV3]([PRES1], [PRES2], [DEN], [UON1], [UON2], [UON3]) ON [PRIMARY]" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[PRES3_NIV3] WITH NOCHECK ADD  CONSTRAINT [PK_PRES3_NIV3] PRIMARY KEY CLUSTERED ([ID]) ," & vbCrLf
sConsulta = sConsulta & " CONSTRAINT [UC_PRES3_NIV3] UNIQUE NONCLUSTERED ([PRES1], [PRES2], [COD], [UON1], [UON2], [UON3]) " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

 sConsulta = ""
'--PRINT 'Updating dbo.PRES4_NIV3'
sConsulta = sConsulta & "CREATE TABLE [dbo].[PRES4_NIV3]" & vbCrLf
sConsulta = sConsulta & "    (" & vbCrLf
sConsulta = sConsulta & "  [ID] [int] NOT NULL IDENTITY (1, 1) ," & vbCrLf
sConsulta = sConsulta & "  [PRES1] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodPRESCONCEP41) & ") NOT NULL ," & vbCrLf
sConsulta = sConsulta & "  [PRES2] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodPRESCONCEP42) & ") NOT NULL ," & vbCrLf
sConsulta = sConsulta & "  [COD] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodPRESCONCEP43) & ") NOT NULL ," & vbCrLf
sConsulta = sConsulta & "  [DEN] [varchar] (100) NOT NULL ," & vbCrLf
sConsulta = sConsulta & "  [UON1] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodUON1) & ") NULL ," & vbCrLf
sConsulta = sConsulta & "  [UON2] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodUON2) & ") NULL ," & vbCrLf
sConsulta = sConsulta & "  [UON3] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodUON3) & ") NULL ," & vbCrLf
sConsulta = sConsulta & "  [IMP] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "  [OBJ] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "  [FECACT] [datetime] NULL " & vbCrLf
sConsulta = sConsulta & ")" & vbCrLf
sConsulta = sConsulta & "CREATE  INDEX [IX_PRES4_NIV3_01] ON [dbo].[PRES4_NIV3]([PRES1], [PRES2], [DEN], [UON1], [UON2], [UON3]) ON [PRIMARY]" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[PRES4_NIV3] WITH NOCHECK ADD  CONSTRAINT [PK_PRES4_NIV3] PRIMARY KEY CLUSTERED ([ID]) ," & vbCrLf
sConsulta = sConsulta & " CONSTRAINT [UC_PRES4_NIV3] UNIQUE NONCLUSTERED ([PRES1], [PRES2], [COD], [UON1], [UON2], [UON3]) " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

 sConsulta = ""
'--PRINT 'Updating dbo.PRES1_NIV4'
sConsulta = sConsulta & "CREATE TABLE [dbo].[PRES1_NIV4]" & vbCrLf
sConsulta = sConsulta & "    (" & vbCrLf
sConsulta = sConsulta & "  [ID] [int] NOT NULL IDENTITY (1, 1) ," & vbCrLf
sConsulta = sConsulta & "  [ANYO] [smallint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "  [PRES1] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodPRESPROY1) & ") NOT NULL ," & vbCrLf
sConsulta = sConsulta & "  [PRES2] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodPRESPROY2) & ") NOT NULL ," & vbCrLf
sConsulta = sConsulta & "  [PRES3] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodPRESPROY3) & ") NOT NULL ," & vbCrLf
sConsulta = sConsulta & "  [COD] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodPRESPROY4) & ") NOT NULL ," & vbCrLf
sConsulta = sConsulta & "  [DEN] [varchar] (100) NOT NULL ," & vbCrLf
sConsulta = sConsulta & "  [UON1] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodUON1) & ") NULL ," & vbCrLf
sConsulta = sConsulta & "  [UON2] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodUON2) & ") NULL ," & vbCrLf
sConsulta = sConsulta & "  [UON3] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodUON3) & ") NULL ," & vbCrLf
sConsulta = sConsulta & "  [IMP] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "  [OBJ] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "  [FECACT] [datetime] NULL " & vbCrLf
sConsulta = sConsulta & ")" & vbCrLf
sConsulta = sConsulta & "CREATE  INDEX [IX_PRES1_NIV4_01] ON [dbo].[PRES1_NIV4]([ANYO], [PRES1], [PRES2], [PRES3], [DEN], [UON1], [UON2], [UON3]) ON [PRIMARY]" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[PRES1_NIV4] WITH NOCHECK ADD  CONSTRAINT [PK_PRES1_NIV4] PRIMARY KEY CLUSTERED ([ID]) ," & vbCrLf
sConsulta = sConsulta & " CONSTRAINT [UC_PRES1_NIV4] UNIQUE NONCLUSTERED ([ANYO], [PRES1], [PRES2], [PRES3], [COD], [UON1], [UON2], [UON3]) " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

 sConsulta = ""
'--PRINT 'Updating dbo.PRES2_NIV4'
sConsulta = sConsulta & "CREATE TABLE [dbo].[PRES2_NIV4]" & vbCrLf
sConsulta = sConsulta & "    (" & vbCrLf
sConsulta = sConsulta & "  [ID] [int] NOT NULL IDENTITY (1, 1) ," & vbCrLf
sConsulta = sConsulta & "  [ANYO] [smallint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "  [PRES1] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodPRESCON1) & ") NOT NULL ," & vbCrLf
sConsulta = sConsulta & "  [PRES2] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodPRESCON2) & ") NOT NULL ," & vbCrLf
sConsulta = sConsulta & "  [PRES3] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodPRESCON3) & ") NOT NULL ," & vbCrLf
sConsulta = sConsulta & "  [COD] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodPRESCON4) & ") NOT NULL ," & vbCrLf
sConsulta = sConsulta & "  [DEN] [varchar] (100) NOT NULL ," & vbCrLf
sConsulta = sConsulta & "  [UON1] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodUON1) & ") NULL ," & vbCrLf
sConsulta = sConsulta & "  [UON2] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodUON2) & ") NULL ," & vbCrLf
sConsulta = sConsulta & "  [UON3] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodUON3) & ") NULL ," & vbCrLf
sConsulta = sConsulta & "  [IMP] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "  [OBJ] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "  [FECACT] [datetime] NULL " & vbCrLf
sConsulta = sConsulta & ")" & vbCrLf
sConsulta = sConsulta & "CREATE  INDEX [IX_PRES2_NIV4_01] ON [dbo].[PRES2_NIV4]([ANYO], [PRES1], [PRES2], [PRES3], [DEN], [UON1], [UON2], [UON3]) ON [PRIMARY]" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[PRES2_NIV4] WITH NOCHECK ADD  CONSTRAINT [PK_PRES2_NIV4] PRIMARY KEY CLUSTERED ([ID]) ," & vbCrLf
sConsulta = sConsulta & " CONSTRAINT [UC_PRES2_NIV4] UNIQUE NONCLUSTERED ([ANYO], [PRES1], [PRES2], [PRES3], [COD], [UON1], [UON2], [UON3]) " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

 sConsulta = ""
'--PRINT 'Updating dbo.PRES3_NIV4'
sConsulta = sConsulta & "CREATE TABLE [dbo].[PRES3_NIV4]" & vbCrLf
sConsulta = sConsulta & "    (" & vbCrLf
sConsulta = sConsulta & "  [ID] [int] NOT NULL IDENTITY (1, 1) ," & vbCrLf
sConsulta = sConsulta & "  [PRES1] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodPRESCONCEP31) & ") NOT NULL ," & vbCrLf
sConsulta = sConsulta & "  [PRES2] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodPRESCONCEP32) & ") NOT NULL ," & vbCrLf
sConsulta = sConsulta & "  [PRES3] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodPRESCONCEP33) & ") NOT NULL ," & vbCrLf
sConsulta = sConsulta & "  [COD] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodPRESCONCEP34) & ") NOT NULL ," & vbCrLf
sConsulta = sConsulta & "  [DEN] [varchar] (100) NOT NULL ," & vbCrLf
sConsulta = sConsulta & "  [UON1] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodUON1) & ") NULL ," & vbCrLf
sConsulta = sConsulta & "  [UON2] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodUON2) & ") NULL ," & vbCrLf
sConsulta = sConsulta & "  [UON3] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodUON3) & ") NULL ," & vbCrLf
sConsulta = sConsulta & "  [IMP] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "  [OBJ] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "  [FECACT] [datetime] NULL " & vbCrLf
sConsulta = sConsulta & ")" & vbCrLf
sConsulta = sConsulta & "CREATE  INDEX [IX_PRES3_NIV4_01] ON [dbo].[PRES3_NIV4]([PRES1], [PRES2], [PRES3], [DEN], [UON1], [UON2], [UON3]) ON [PRIMARY]" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[PRES3_NIV4] WITH NOCHECK ADD  CONSTRAINT [PK_PRES3_NIV4] PRIMARY KEY CLUSTERED ([ID]) ," & vbCrLf
sConsulta = sConsulta & " CONSTRAINT [UC_PRES3_NIV4] UNIQUE NONCLUSTERED ([PRES1], [PRES2], [PRES3], [COD], [UON1], [UON2], [UON3]) " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

 sConsulta = ""
'--PRINT 'Updating dbo.PRES4_NIV4'
sConsulta = sConsulta & "CREATE TABLE [dbo].[PRES4_NIV4]" & vbCrLf
sConsulta = sConsulta & "    (" & vbCrLf
sConsulta = sConsulta & "  [ID] [int] NOT NULL IDENTITY (1, 1) ," & vbCrLf
sConsulta = sConsulta & "  [PRES1] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodPRESCONCEP41) & ") NOT NULL ," & vbCrLf
sConsulta = sConsulta & "  [PRES2] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodPRESCONCEP42) & ") NOT NULL ," & vbCrLf
sConsulta = sConsulta & "  [PRES3] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodPRESCONCEP43) & ") NOT NULL ," & vbCrLf
sConsulta = sConsulta & "  [COD] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodPRESCONCEP44) & ") NOT NULL ," & vbCrLf
sConsulta = sConsulta & "  [DEN] [varchar] (100) NOT NULL ," & vbCrLf
sConsulta = sConsulta & "  [UON1] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodUON1) & ") NULL ," & vbCrLf
sConsulta = sConsulta & "  [UON2] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodUON2) & ") NULL ," & vbCrLf
sConsulta = sConsulta & "  [UON3] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodUON3) & ") NULL ," & vbCrLf
sConsulta = sConsulta & "  [IMP] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "  [OBJ] [float] NULL ," & vbCrLf
sConsulta = sConsulta & "  [FECACT] [datetime] NULL " & vbCrLf
sConsulta = sConsulta & ")" & vbCrLf
sConsulta = sConsulta & "CREATE  INDEX [IX_PRES4_NIV4_01] ON [dbo].[PRES4_NIV4]([PRES1], [PRES2], [PRES3], [DEN], [UON1], [UON2], [UON3]) ON [PRIMARY]" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[PRES4_NIV4] WITH NOCHECK ADD  CONSTRAINT [PK_PRES4_NIV4] PRIMARY KEY CLUSTERED ([ID]) ," & vbCrLf
sConsulta = sConsulta & " CONSTRAINT [UC_PRES4_NIV4] UNIQUE NONCLUSTERED ([PRES1], [PRES2], [PRES3], [COD], [UON1], [UON2], [UON3]) " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

'Triggers de INS UPD para FECACT
 sConsulta = ""
sConsulta = sConsulta & "CREATE TRIGGER PRES1_NIV1_TG_INSUPD ON dbo.PRES1_NIV1" & vbCrLf
sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO INT,@COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_PRES1_NIV1_Ins CURSOR FOR SELECT ANYO,COD FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_PRES1_NIV1_Ins" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PRES1_NIV1_Ins INTO @ANYO,@COD" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES1_NIV1 SET FECACT=GETDATE() WHERE ANYO=@ANYO AND COD=@COD" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PRES1_NIV1_Ins INTO @ANYO,@COD" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_PRES1_NIV1_Ins" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_PRES1_NIV1_Ins" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

 sConsulta = ""
sConsulta = sConsulta & "CREATE TRIGGER PRES2_NIV1_TG_INSUPD ON dbo.PRES2_NIV1" & vbCrLf
sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO INT,@COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_PRES2_NIV1_Ins CURSOR FOR SELECT ANYO,COD FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_PRES2_NIV1_Ins" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PRES2_NIV1_Ins INTO @ANYO,@COD" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES2_NIV1 SET FECACT=GETDATE() WHERE ANYO=@ANYO AND COD=@COD" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PRES2_NIV1_Ins INTO @ANYO,@COD" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_PRES2_NIV1_Ins" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_PRES2_NIV1_Ins" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

 sConsulta = ""
sConsulta = sConsulta & "CREATE TRIGGER PRES3_NIV1_TG_INSUPD ON dbo.PRES3_NIV1" & vbCrLf
sConsulta = sConsulta & "FOR INSERT,UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_PRES3_NIV1_Ins CURSOR FOR SELECT COD FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_PRES3_NIV1_Ins" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PRES3_NIV1_Ins INTO @COD" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES3_NIV1 SET FECACT=GETDATE() WHERE COD=@COD" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PRES3_NIV1_Ins INTO @COD" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_PRES3_NIV1_Ins" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_PRES3_NIV1_Ins" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

 sConsulta = ""
sConsulta = sConsulta & "CREATE TRIGGER PRES4_NIV1_TG_INSUPD ON dbo.PRES4_NIV1" & vbCrLf
sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_PRES4_NIV1_Ins CURSOR FOR SELECT COD FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_PRES4_NIV1_Ins" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PRES4_NIV1_Ins INTO @COD" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES4_NIV1 SET FECACT=GETDATE() WHERE  COD=@COD" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PRES4_NIV1_Ins INTO @COD" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_PRES4_NIV1_Ins" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_PRES4_NIV1_Ins" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

 sConsulta = ""
sConsulta = sConsulta & "CREATE TRIGGER PRES1_NIV2_TG_INSUPD ON dbo.PRES1_NIV2" & vbCrLf
sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO INT,@PRES1 VARCHAR(50),@COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_PRES1_NIV2_Ins CURSOR FOR SELECT ANYO,PRES1,COD FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_PRES1_NIV2_Ins" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PRES1_NIV2_Ins INTO @ANYO,@PRES1,@COD" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES1_NIV2 SET FECACT=GETDATE() WHERE ANYO=@ANYO AND PRES1=@PRES1 AND COD=@COD" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PRES1_NIV2_Ins INTO @ANYO,@PRES1,@COD" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_PRES1_NIV2_Ins" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_PRES1_NIV2_Ins" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

 sConsulta = ""
sConsulta = sConsulta & "CREATE TRIGGER PRES2_NIV2_TG_INSUPD ON dbo.PRES2_NIV2" & vbCrLf
sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO INT,@PRES1 VARCHAR(50),@COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_PRES2_NIV2_Ins CURSOR FOR SELECT ANYO,PRES1,COD FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_PRES2_NIV2_Ins" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PRES2_NIV2_Ins INTO @ANYO,@PRES1,@COD" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES2_NIV2 SET FECACT=GETDATE() WHERE ANYO=@ANYO AND PRES1=@PRES1 AND COD=@COD" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PRES2_NIV2_Ins INTO @ANYO,@PRES1,@COD" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_PRES2_NIV2_Ins" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_PRES2_NIV2_Ins" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

 sConsulta = ""
sConsulta = sConsulta & "CREATE TRIGGER PRES3_NIV2_TG_INSUPD ON dbo.PRES3_NIV2" & vbCrLf
sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRES1 VARCHAR(50), @COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_PRES3_NIV2_Ins CURSOR FOR SELECT PRES1,COD FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_PRES3_NIV2_Ins" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PRES3_NIV2_Ins INTO @PRES1, @COD" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES3_NIV2 SET FECACT=GETDATE() WHERE PRES1=@PRES1 AND COD=@COD" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PRES3_NIV2_Ins INTO @PRES1,@COD" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_PRES3_NIV2_Ins" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_PRES3_NIV2_Ins" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

 sConsulta = ""
sConsulta = sConsulta & "CREATE TRIGGER PRES4_NIV2_TG_INSUPD ON dbo.PRES4_NIV2" & vbCrLf
sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRES1 VARCHAR(50), @COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_PRES4_NIV2_Ins CURSOR FOR SELECT PRES1,COD FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_PRES4_NIV2_Ins" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PRES4_NIV2_Ins INTO @PRES1, @COD" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES4_NIV2 SET FECACT=GETDATE() WHERE PRES1=@PRES1 AND COD=@COD" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PRES4_NIV2_Ins INTO @PRES1,@COD" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_PRES4_NIV2_Ins" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_PRES4_NIV2_Ins" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

 sConsulta = ""
sConsulta = sConsulta & "CREATE TRIGGER PRES1_NIV3_TG_INSUPD ON dbo.PRES1_NIV3" & vbCrLf
sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO INT,@PRES1 VARCHAR(50),@PRES2 VARCHAR(50),@COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_PRES1_NIV3_Ins CURSOR FOR SELECT ANYO,PRES1,PRES2,COD FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_PRES1_NIV3_Ins" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PRES1_NIV3_Ins INTO @ANYO,@PRES1,@PRES2,@COD" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES1_NIV3 SET FECACT=GETDATE() WHERE ANYO=@ANYO AND PRES1=@PRES1 AND PRES2=@PRES2 AND COD=@COD" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PRES1_NIV3_Ins INTO @ANYO,@PRES1,@PRES2,@COD" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_PRES1_NIV3_Ins" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_PRES1_NIV3_Ins" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

 sConsulta = ""
sConsulta = sConsulta & "CREATE TRIGGER PRES2_NIV3_TG_INSUPD ON dbo.PRES2_NIV3" & vbCrLf
sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO INT,@PRES1 VARCHAR(50),@PRES2 VARCHAR(50),@COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_PRES2_NIV3_Ins CURSOR FOR SELECT ANYO,PRES1,PRES2,COD FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_PRES2_NIV3_Ins" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PRES2_NIV3_Ins INTO @ANYO,@PRES1,@PRES2,@COD" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES2_NIV3 SET FECACT=GETDATE() WHERE ANYO=@ANYO AND PRES1=@PRES1 AND PRES2=@PRES2 AND COD=@COD" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PRES2_NIV3_Ins INTO @ANYO,@PRES1,@PRES2,@COD" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_PRES2_NIV3_Ins" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_PRES2_NIV3_Ins" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

 sConsulta = ""
sConsulta = sConsulta & "CREATE TRIGGER PRES3_NIV3_TG_INSUPD ON dbo.PRES3_NIV3" & vbCrLf
sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRES1 VARCHAR(50), @PRES2 VARCHAR(50), @COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_PRES3_NIV3_Ins CURSOR FOR SELECT PRES1, PRES2, COD FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_PRES3_NIV3_Ins" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PRES3_NIV3_Ins INTO @PRES1, @PRES2, @COD" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES3_NIV3 SET FECACT=GETDATE() WHERE  PRES1=@PRES1 AND PRES2=@PRES2 AND COD=@COD" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PRES3_NIV3_Ins INTO @PRES1, @PRES2, @COD" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_PRES3_NIV3_Ins" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_PRES3_NIV3_Ins" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

 sConsulta = ""
sConsulta = sConsulta & "CREATE TRIGGER PRES4_NIV3_TG_INSUPD ON dbo.PRES4_NIV3" & vbCrLf
sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRES1 VARCHAR(50), @PRES2 VARCHAR(50), @COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_PRES4_NIV3_Ins CURSOR FOR SELECT PRES1, PRES2, COD FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_PRES4_NIV3_Ins" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PRES4_NIV3_Ins INTO @PRES1, @PRES2, @COD" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES4_NIV3 SET FECACT=GETDATE() WHERE  PRES1=@PRES1 AND PRES2=@PRES2 AND COD=@COD" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PRES4_NIV3_Ins INTO @PRES1, @PRES2, @COD" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_PRES4_NIV3_Ins" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_PRES4_NIV3_Ins" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

 sConsulta = ""
sConsulta = sConsulta & "CREATE TRIGGER PRES1_NIV4_TG_INSUPD ON dbo.PRES1_NIV4" & vbCrLf
sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO INT,@PRES1 VARCHAR(50),@PRES2 VARCHAR(50),@PRES3 VARCHAR(50),@COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_PRES1_NIV4_Ins CURSOR FOR SELECT ANYO,PRES1,PRES2,PRES3,COD FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_PRES1_NIV4_Ins" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PRES1_NIV4_Ins INTO @ANYO,@PRES1,@PRES2,@PRES3,@COD" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES1_NIV4 SET FECACT=GETDATE() WHERE ANYO=@ANYO AND PRES1=@PRES1 AND PRES2=@PRES2 AND PRES3=@PRES3 AND COD=@COD" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PRES1_NIV4_Ins INTO @ANYO,@PRES1,@PRES2,@PRES3,@COD" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_PRES1_NIV4_Ins" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_PRES1_NIV4_Ins" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

 sConsulta = ""
sConsulta = sConsulta & "CREATE TRIGGER PRES2_NIV4_TG_INSUPD ON dbo.PRES2_NIV4" & vbCrLf
sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO INT,@PRES1 VARCHAR(50),@PRES2 VARCHAR(50),@PRES3 VARCHAR(50),@COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_PRES2_NIV4_Ins CURSOR FOR SELECT ANYO,PRES1,PRES2,PRES3,COD FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_PRES2_NIV4_Ins" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PRES2_NIV4_Ins INTO @ANYO,@PRES1,@PRES2,@PRES3,@COD" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES2_NIV4 SET FECACT=GETDATE() WHERE ANYO=@ANYO AND PRES1=@PRES1 AND PRES2=@PRES2 AND PRES3=@PRES3 AND COD=@COD" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PRES2_NIV4_Ins INTO @ANYO,@PRES1,@PRES2,@PRES3,@COD" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_PRES2_NIV4_Ins" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_PRES2_NIV4_Ins" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

 sConsulta = ""
sConsulta = sConsulta & "CREATE TRIGGER PRES3_NIV4_TG_INSUPD ON dbo.PRES3_NIV4" & vbCrLf
sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRES1 VARCHAR(50), @PRES2 VARCHAR(50), @PRES3 VARCHAR(50), @COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_PRES3_NIV4_Ins CURSOR FOR SELECT PRES1, PRES2, PRES3, COD FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_PRES3_NIV4_Ins" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PRES3_NIV4_Ins INTO @PRES1, @PRES2, @PRES3, @COD" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES3_NIV4 SET FECACT=GETDATE() WHERE  PRES1=@PRES1 AND @PRES2=@PRES2 AND PRES3=@PRES3 AND COD=@COD" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PRES3_NIV4_Ins INTO @PRES1,@PRES2, @PRES3, @COD" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_PRES3_NIV4_Ins" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_PRES3_NIV4_Ins" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

 sConsulta = ""
sConsulta = sConsulta & "CREATE TRIGGER PRES4_NIV4_TG_INSUPD ON dbo.PRES4_NIV4" & vbCrLf
sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @PRES1 VARCHAR(50), @PRES2 VARCHAR(50), @PRES3 VARCHAR(50), @COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_PRES4_NIV4_Ins CURSOR FOR SELECT PRES1, PRES2, PRES3, COD FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_PRES4_NIV4_Ins" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PRES4_NIV4_Ins INTO @PRES1, @PRES2, @PRES3, @COD" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES4_NIV4 SET FECACT=GETDATE() WHERE  PRES1=@PRES1 AND @PRES2=@PRES2 AND PRES3=@PRES3 AND COD=@COD" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PRES4_NIV4_Ins INTO @PRES1,@PRES2, @PRES3, @COD" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_PRES4_NIV4_Ins" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_PRES4_NIV4_Ins" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

'-- Add foreign key constraints
sConsulta = ""
sConsulta = sConsulta & "ALTER TABLE [dbo].[PRES1_NIV1] ADD " & vbCrLf
sConsulta = sConsulta & "CONSTRAINT [FK_PRES1_NIV1_UON1] FOREIGN KEY ([UON1])  REFERENCES [dbo].[UON1] ([COD]) ," & vbCrLf
sConsulta = sConsulta & "CONSTRAINT [FK_PRES1_NIV1_UON2] FOREIGN KEY ([UON1], [UON2])  REFERENCES [dbo].[UON2] ([UON1], [COD]) ," & vbCrLf
sConsulta = sConsulta & "CONSTRAINT [FK_PRES1_NIV1_UON3] FOREIGN KEY ([UON1], [UON2], [UON3])  REFERENCES [dbo].[UON3] ([UON1], [UON2], [COD]) " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

 sConsulta = ""
sConsulta = sConsulta & "ALTER TABLE [dbo].[PRES2_NIV1] ADD" & vbCrLf
sConsulta = sConsulta & "CONSTRAINT [FK_PRES2_NIV1_UON1] FOREIGN KEY ([UON1])  REFERENCES [dbo].[UON1] ([COD]) ," & vbCrLf
sConsulta = sConsulta & "CONSTRAINT [FK_PRES2_NIV1_UON2] FOREIGN KEY ([UON1], [UON2])  REFERENCES [dbo].[UON2] ([UON1], [COD]) ," & vbCrLf
sConsulta = sConsulta & "ADD CONSTRAINT [FK_PRES2_NIV1_UON3] FOREIGN KEY ([UON1], [UON2], [UON3])  REFERENCES [dbo].[UON3] ([UON1], [UON2], [COD]) " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

 sConsulta = ""
sConsulta = sConsulta & "ALTER TABLE [dbo].[PRES3_NIV1] ADD" & vbCrLf
sConsulta = sConsulta & "CONSTRAINT [FK_PRES3_NIV1_UON1] FOREIGN KEY ([UON1])  REFERENCES [dbo].[UON1] ([COD]) ," & vbCrLf
sConsulta = sConsulta & "CONSTRAINT [FK_PRES3_NIV1_UON2] FOREIGN KEY ([UON1], [UON2])  REFERENCES [dbo].[UON2] ([UON1], [COD]) ," & vbCrLf
sConsulta = sConsulta & "CONSTRAINT [FK_PRES3_NIV1_UON3] FOREIGN KEY ([UON1], [UON2], [UON3])  REFERENCES [dbo].[UON3] ([UON1], [UON2], [COD]) " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

 sConsulta = ""
sConsulta = sConsulta & "ALTER TABLE [dbo].[PRES4_NIV1] ADD " & vbCrLf
sConsulta = sConsulta & "CONSTRAINT [FK_PRES4_NIV1_UON1] FOREIGN KEY ([UON1])  REFERENCES [dbo].[UON1] ([COD]) ," & vbCrLf
sConsulta = sConsulta & "CONSTRAINT [FK_PRES4_NIV1_UON2] FOREIGN KEY ([UON1], [UON2])  REFERENCES [dbo].[UON2] ([UON1], [COD]) ," & vbCrLf
sConsulta = sConsulta & "CONSTRAINT [FK_PRES4_NIV1_UON3] FOREIGN KEY ([UON1], [UON2], [UON3])  REFERENCES [dbo].[UON3] ([UON1], [UON2], [COD]) " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

 sConsulta = ""
sConsulta = sConsulta & "ALTER TABLE [dbo].[PRES1_NIV2] ADD " & vbCrLf
sConsulta = sConsulta & "CONSTRAINT [FK_PRES1_NIV2_PRES1_NIV1] FOREIGN KEY ([ANYO], [PRES1], [UON1], [UON2], [UON3])  REFERENCES [dbo].[PRES1_NIV1] ([ANYO], [COD], [UON1], [UON2], [UON3]) ," & vbCrLf
sConsulta = sConsulta & "CONSTRAINT [FK_PRES1_NIV2_UON1] FOREIGN KEY ([UON1])  REFERENCES [dbo].[UON1] ([COD]) ," & vbCrLf
sConsulta = sConsulta & "CONSTRAINT [FK_PRES1_NIV2_UON2] FOREIGN KEY ([UON1], [UON2])  REFERENCES [dbo].[UON2] ([UON1], [COD]) ," & vbCrLf
sConsulta = sConsulta & "CONSTRAINT [FK_PRES1_NIV2_UON3] FOREIGN KEY ([UON1], [UON2], [UON3])  REFERENCES [dbo].[UON3] ([UON1], [UON2], [COD]) " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

 sConsulta = ""
sConsulta = sConsulta & "ALTER TABLE [dbo].[PRES2_NIV2] ADD " & vbCrLf
sConsulta = sConsulta & "CONSTRAINT [FK_PRES2_NIV2_PRES2_NIV1] FOREIGN KEY ([ANYO], [PRES1], [UON1], [UON2], [UON3])  REFERENCES [dbo].[PRES2_NIV1] ([ANYO], [COD], [UON1], [UON2], [UON3]) ," & vbCrLf
sConsulta = sConsulta & "CONSTRAINT [FK_PRES2_NIV2_UON1] FOREIGN KEY ([UON1])  REFERENCES [dbo].[UON1] ([COD]) ," & vbCrLf
sConsulta = sConsulta & "CONSTRAINT [FK_PRES2_NIV2_UON2] FOREIGN KEY ([UON1], [UON2])  REFERENCES [dbo].[UON2] ([UON1], [COD]) ," & vbCrLf
sConsulta = sConsulta & "CONSTRAINT [FK_PRES2_NIV2_UON3] FOREIGN KEY ([UON1], [UON2], [UON3])  REFERENCES [dbo].[UON3] ([UON1], [UON2], [COD]) " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

 sConsulta = ""
sConsulta = sConsulta & "ALTER TABLE [dbo].[PRES3_NIV2] ADD " & vbCrLf
sConsulta = sConsulta & "CONSTRAINT [FK_PRES3_NIV2_PRES3_NIV1] FOREIGN KEY ([PRES1], [UON1], [UON2], [UON3])  REFERENCES [dbo].[PRES3_NIV1] ([COD], [UON1], [UON2], [UON3]) ," & vbCrLf
sConsulta = sConsulta & "CONSTRAINT [FK_PRES3_NIV2_UON1] FOREIGN KEY ([UON1])  REFERENCES [dbo].[UON1] ([COD]) ," & vbCrLf
sConsulta = sConsulta & "CONSTRAINT [FK_PRES3_NIV2_UON2] FOREIGN KEY ([UON1], [UON2])  REFERENCES [dbo].[UON2] ([UON1], [COD]) ," & vbCrLf
sConsulta = sConsulta & "CONSTRAINT [FK_PRES3_NIV2_UON3] FOREIGN KEY ([UON1], [UON2], [UON3])  REFERENCES [dbo].[UON3] ([UON1], [UON2], [COD]) " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

 sConsulta = ""
sConsulta = sConsulta & "ALTER TABLE [dbo].[PRES4_NIV2] ADD" & vbCrLf
sConsulta = sConsulta & "CONSTRAINT [FK_PRES4_NIV2_PRES4_NIV1] FOREIGN KEY ([PRES1], [UON1], [UON2], [UON3])  REFERENCES [dbo].[PRES4_NIV1] ([COD], [UON1], [UON2], [UON3]) ," & vbCrLf
sConsulta = sConsulta & "CONSTRAINT [FK_PRES4_NIV2_UON1] FOREIGN KEY ([UON1])  REFERENCES [dbo].[UON1] ([COD]) ," & vbCrLf
sConsulta = sConsulta & "CONSTRAINT [FK_PRES4_NIV2_UON2] FOREIGN KEY ([UON1], [UON2])  REFERENCES [dbo].[UON2] ([UON1], [COD]) ," & vbCrLf
sConsulta = sConsulta & "CONSTRAINT [FK_PRES4_NIV2_UON3] FOREIGN KEY ([UON1], [UON2], [UON3])  REFERENCES [dbo].[UON3] ([UON1], [UON2], [COD]) " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

 sConsulta = ""
sConsulta = sConsulta & "ALTER TABLE [dbo].[PRES1_NIV3] ADD " & vbCrLf
sConsulta = sConsulta & "CONSTRAINT [FK_PRES1_NIV3_PRES1_NIV2] FOREIGN KEY ([ANYO], [PRES1], [PRES2], [UON1], [UON2], [UON3])  REFERENCES [dbo].[PRES1_NIV2] ([ANYO], [PRES1], [COD], [UON1], [UON2], [UON3]) " & vbCrLf
sConsulta = sConsulta & "CONSTRAINT [FK_PRES1_NIV3_UON1] FOREIGN KEY ([UON1])  REFERENCES [dbo].[UON1] ([COD]) ," & vbCrLf
sConsulta = sConsulta & "CONSTRAINT [FK_PRES1_NIV3_UON2] FOREIGN KEY ([UON1], [UON2])  REFERENCES [dbo].[UON2] ([UON1], [COD]) ," & vbCrLf
sConsulta = sConsulta & "CONSTRAINT [FK_PRES1_NIV3_UON3] FOREIGN KEY ([UON1], [UON2], [UON3])  REFERENCES [dbo].[UON3] ([UON1], [UON2], [COD]) " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

 sConsulta = ""
sConsulta = sConsulta & "ALTER TABLE [dbo].[PRES2_NIV3] ADD " & vbCrLf
sConsulta = sConsulta & "CONSTRAINT [FK_PRES2_NIV3_PRES2_NIV2] FOREIGN KEY ([ANYO], [PRES1], [PRES2], [UON1], [UON2], [UON3])  REFERENCES [dbo].[PRES2_NIV2] ([ANYO], [PRES1], [COD], [UON1], [UON2], [UON3]) " & vbCrLf
sConsulta = sConsulta & "CONSTRAINT [FK_PRES2_NIV3_UON1] FOREIGN KEY ([UON1])  REFERENCES [dbo].[UON1] ([COD]) ," & vbCrLf
sConsulta = sConsulta & "CONSTRAINT [FK_PRES2_NIV3_UON2] FOREIGN KEY ([UON1], [UON2])  REFERENCES [dbo].[UON2] ([UON1], [COD]) ," & vbCrLf
sConsulta = sConsulta & "CONSTRAINT [FK_PRES2_NIV3_UON3] FOREIGN KEY ([UON1], [UON2], [UON3])  REFERENCES [dbo].[UON3] ([UON1], [UON2], [COD]) " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

 sConsulta = ""
sConsulta = sConsulta & "ALTER TABLE [dbo].[PRES3_NIV3] ADD " & vbCrLf
sConsulta = sConsulta & "CONSTRAINT [FK_PRES3_NIV3_PRES3_NIV2] FOREIGN KEY ([PRES1], [PRES2], [UON1], [UON2], [UON3])  REFERENCES [dbo].[PRES3_NIV2] ([PRES1], [COD], [UON1], [UON2], [UON3]) " & vbCrLf
sConsulta = sConsulta & "CONSTRAINT [FK_PRES3_NIV3_UON1] FOREIGN KEY ([UON1])  REFERENCES [dbo].[UON1] ([COD]) ," & vbCrLf
sConsulta = sConsulta & "CONSTRAINT [FK_PRES3_NIV3_UON2] FOREIGN KEY ([UON1], [UON2])  REFERENCES [dbo].[UON2] ([UON1], [COD]) ," & vbCrLf
sConsulta = sConsulta & "CONSTRAINT [FK_PRES3_NIV3_UON3] FOREIGN KEY ([UON1], [UON2], [UON3])  REFERENCES [dbo].[UON3] ([UON1], [UON2], [COD]) " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

 sConsulta = ""
sConsulta = sConsulta & "ALTER TABLE [dbo].[PRES4_NIV3] ADD " & vbCrLf
sConsulta = sConsulta & "CONSTRAINT [FK_PRES4_NIV3_PRES4_NIV2] FOREIGN KEY ([PRES1], [PRES2], [UON1], [UON2], [UON3])  REFERENCES [dbo].[PRES4_NIV2] ([PRES1], [COD], [UON1], [UON2], [UON3]) " & vbCrLf
sConsulta = sConsulta & "CONSTRAINT [FK_PRES4_NIV3_UON1] FOREIGN KEY ([UON1])  REFERENCES [dbo].[UON1] ([COD]) ," & vbCrLf
sConsulta = sConsulta & "CONSTRAINT [FK_PRES4_NIV3_UON2] FOREIGN KEY ([UON1], [UON2])  REFERENCES [dbo].[UON2] ([UON1], [COD]) ," & vbCrLf
sConsulta = sConsulta & "CONSTRAINT [FK_PRES4_NIV3_UON3] FOREIGN KEY ([UON1], [UON2], [UON3])  REFERENCES [dbo].[UON3] ([UON1], [UON2], [COD]) " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

 sConsulta = ""
sConsulta = sConsulta & "ALTER TABLE [dbo].[PRES1_NIV4] ADD " & vbCrLf
sConsulta = sConsulta & "CONSTRAINT [FK_PRES1_NIV4_PRES1_NIV3] FOREIGN KEY ([ANYO], [PRES1], [PRES2], [PRES3], [UON1], [UON2], [UON3])  REFERENCES [dbo].[PRES1_NIV3] ([ANYO], [PRES1], [PRES2], [COD], [UON1], [UON2], [UON3]) " & vbCrLf
sConsulta = sConsulta & "CONSTRAINT [FK_PRES1_NIV4_UON1] FOREIGN KEY ([UON1])  REFERENCES [dbo].[UON1] ([COD]) ," & vbCrLf
sConsulta = sConsulta & "CONSTRAINT [FK_PRES1_NIV4_UON2] FOREIGN KEY ([UON1], [UON2])  REFERENCES [dbo].[UON2] ([UON1], [COD]) ," & vbCrLf
sConsulta = sConsulta & "CONSTRAINT [FK_PRES1_NIV4_UON3] FOREIGN KEY ([UON1], [UON2], [UON3])  REFERENCES [dbo].[UON3] ([UON1], [UON2], [COD]) " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

 sConsulta = ""
sConsulta = sConsulta & "ALTER TABLE [dbo].[PRES2_NIV4] ADD " & vbCrLf
sConsulta = sConsulta & "CONSTRAINT [FK_PRES2_NIV4_PRES2_NIV3] FOREIGN KEY ([ANYO], [PRES1], [PRES2], [PRES3], [UON1], [UON2], [UON3])  REFERENCES [dbo].[PRES2_NIV3] ([ANYO], [PRES1], [PRES2], [COD], [UON1], [UON2], [UON3]) " & vbCrLf
sConsulta = sConsulta & "CONSTRAINT [FK_PRES2_NIV4_UON1] FOREIGN KEY ([UON1])  REFERENCES [dbo].[UON1] ([COD]) ," & vbCrLf
sConsulta = sConsulta & "CONSTRAINT [FK_PRES2_NIV4_UON2] FOREIGN KEY ([UON1], [UON2])  REFERENCES [dbo].[UON2] ([UON1], [COD]) ," & vbCrLf
sConsulta = sConsulta & "CONSTRAINT [FK_PRES2_NIV4_UON3] FOREIGN KEY ([UON1], [UON2], [UON3])  REFERENCES [dbo].[UON3] ([UON1], [UON2], [COD]) " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

 sConsulta = ""
sConsulta = sConsulta & "ALTER TABLE [dbo].[PRES3_NIV4] WITH NOCHECK ADD CONSTRAINT [FK_PRES3_NIV4_PRES3_NIV3] FOREIGN KEY ([PRES1], [PRES2], [PRES3], [UON1], [UON2], [UON3])  REFERENCES [dbo].[PRES3_NIV3] ([PRES1], [PRES2], [COD], [UON1], [UON2], [UON3]) " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[PRES3_NIV4] WITH NOCHECK ADD CONSTRAINT [FK_PRES3_NIV4_UON1] FOREIGN KEY ([UON1])  REFERENCES [dbo].[UON1] ([COD]) ," & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[PRES3_NIV4] WITH NOCHECK ADD CONSTRAINT [FK_PRES3_NIV4_UON2] FOREIGN KEY ([UON1], [UON2])  REFERENCES [dbo].[UON2] ([UON1], [COD]) ," & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[PRES3_NIV4] WITH NOCHECK ADD CONSTRAINT [FK_PRES3_NIV4_UON3] FOREIGN KEY ([UON1], [UON2], [UON3])  REFERENCES [dbo].[UON3] ([UON1], [UON2], [COD]) " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

 sConsulta = ""
sConsulta = sConsulta & "ALTER TABLE [dbo].[PRES4_NIV4] ADD " & vbCrLf
sConsulta = sConsulta & "CONSTRAINT [FK_PRES4_NIV4_PRES4_NIV3] FOREIGN KEY ([PRES1], [PRES2], [PRES3], [UON1], [UON2], [UON3])  REFERENCES [dbo].[PRES4_NIV3] ([PRES1], [PRES2], [COD], [UON1], [UON2], [UON3]) " & vbCrLf
sConsulta = sConsulta & "CONSTRAINT [FK_PRES4_NIV4_UON1] FOREIGN KEY ([UON1])  REFERENCES [dbo].[UON1] ([COD]) ," & vbCrLf
sConsulta = sConsulta & "CONSTRAINT [FK_PRES4_NIV4_UON2] FOREIGN KEY ([UON1], [UON2])  REFERENCES [dbo].[UON2] ([UON1], [COD]) ," & vbCrLf
sConsulta = sConsulta & "CONSTRAINT [FK_PRES4_NIV4_UON3] FOREIGN KEY ([UON1], [UON2], [UON3])  REFERENCES [dbo].[UON3] ([UON1], [UON2], [COD]) " & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

'Inserto los registros de las tablas antiguas
sConsulta = "INSERT INTO PRES1_NIV1 (ANYO,COD,DEN,IMP,OBJ) SELECT ANYO,COD,DEN,IMP,OBJ FROM PRES_PROY1"
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = "INSERT INTO PRES2_NIV1 (ANYO,COD,DEN,IMP,OBJ) SELECT ANYO,COD,DEN,IMP,OBJ FROM PRES_CON1"
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = "INSERT INTO PRES3_NIV1 (COD,DEN,IMP,OBJ) SELECT COD,DEN,IMP,OBJ FROM PRES_3_CON1"
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = "INSERT INTO PRES4_NIV1 (COD,DEN,IMP,OBJ) SELECT COD,DEN,IMP,OBJ FROM PRES_4_CON1"
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = "INSERT INTO PRES1_NIV2 (ANYO,PRES1,COD,DEN,IMP,OBJ) SELECT ANYO,PRES1,COD,DEN,IMP,OBJ FROM PRES_PROY2"
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = "INSERT INTO PRES2_NIV2 (ANYO,PRES1,COD,DEN,IMP,OBJ) SELECT ANYO,PRES1,COD,DEN,IMP,OBJ FROM PRES_CON2"
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = "INSERT INTO PRES3_NIV2 (PRES1,COD,DEN,IMP,OBJ) SELECT PRES1,COD,DEN,IMP,OBJ FROM PRES_3_CON2"
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = "INSERT INTO PRES4_NIV2 (PRES1,COD,DEN,IMP,OBJ) SELECT PRES1,COD,DEN,IMP,OBJ FROM PRES_4_CON2"
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = "INSERT INTO PRES1_NIV3 (ANYO,PRES1,PRES2,COD,DEN,IMP,OBJ) SELECT ANYO,PRES1,PRES2,COD,DEN,IMP,OBJ FROM PRES_PROY3"
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = "INSERT INTO PRES2_NIV3 (ANYO,PRES1,PRES2,COD,DEN,IMP,OBJ) SELECT ANYO,PRES1,PRES2,COD,DEN,IMP,OBJ FROM PRES_CON3"
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = "INSERT INTO PRES3_NIV3 (PRES1,PRES2,COD,DEN,IMP,OBJ) SELECT PRES1,PRES2,COD,DEN,IMP,OBJ FROM PRES_3_CON3"
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = "INSERT INTO PRES4_NIV3 (PRES1,PRES2,COD,DEN,IMP,OBJ) SELECT PRES1,PRES2,COD,DEN,IMP,OBJ FROM PRES_4_CON3"
gRDOCon.Execute sConsulta, rdExecDirect


sConsulta = "INSERT INTO PRES1_NIV4 (ANYO,PRES1,PRES2,PRES3,COD,DEN,IMP,OBJ) SELECT ANYO,PRES1,PRES2,PRES3,COD,DEN,IMP,OBJ FROM PRES_PROY4"
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = "INSERT INTO PRES2_NIV4 (ANYO,PRES1,PRES2,PRES3,COD,DEN,IMP,OBJ) SELECT ANYO,PRES1,PRES2,PRES3,COD,DEN,IMP,OBJ FROM PRES_CON4"
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = "INSERT INTO PRES3_NIV4 (PRES1,PRES2,PRES3,COD,DEN,IMP,OBJ) SELECT PRES1,PRES2,PRES3,COD,DEN,IMP,OBJ FROM PRES_3_CON4"
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = "INSERT INTO PRES4_NIV4 (PRES1,PRES2,PRES3,COD,DEN,IMP,OBJ) SELECT PRES1,PRES2,PRES3,COD,DEN,IMP,OBJ FROM PRES_4_CON4"
gRDOCon.Execute sConsulta, rdExecDirect


End Sub

Private Sub V_10_ActulizarITEM_PRES2()
Dim sConsulta As String

'ITEM_PRESCON
'  Remove foreign key constraints" & vbcrlf
sConsulta = "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[ITEM_PRESCON] DROP CONSTRAINT [ITEM_FK_ITEM_PRESCON]" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
 sConsulta = ""
sConsulta = sConsulta & "ALTER TABLE [dbo].[ITEM_PRESCON] DROP CONSTRAINT [PRES_CON1_FK_ITEM_PRESCON]" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
 sConsulta = ""
sConsulta = sConsulta & "ALTER TABLE [dbo].[ITEM_PRESCON] DROP CONSTRAINT [PRES_CON2_FK_ITEM_PRESCON]" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
 sConsulta = ""
sConsulta = sConsulta & "ALTER TABLE [dbo].[ITEM_PRESCON] DROP CONSTRAINT [PRES_CON3_FK_ITEM_PRESCON]" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
 sConsulta = ""
sConsulta = sConsulta & "ALTER TABLE [dbo].[ITEM_PRESCON] DROP CONSTRAINT [PRES_CON4_FK_ITEM_PRESCON]" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf

'  Script for dbo.ITEM_PRESCON" & vbcrlf
'  Foreign keys etc. will appear at the end
sConsulta = ""
sConsulta = sConsulta & "CREATE TABLE [dbo].[tmp_sc_ITEM_PRESCON]" & vbCrLf
sConsulta = sConsulta & "    (" & vbCrLf
sConsulta = sConsulta & "  [ID] [int] NOT NULL IDENTITY (1, 1) ," & vbCrLf
sConsulta = sConsulta & "  [ANYO] [smallint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "  [GMN1] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodGMN1) & ") NOT NULL ," & vbCrLf
sConsulta = sConsulta & "  [PROCE] [smallint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "  [ITEM] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "  [PRES1] [int] NULL ," & vbCrLf
sConsulta = sConsulta & "  [PRES2] [int] NULL ," & vbCrLf
sConsulta = sConsulta & "  [PRES3] [int] NULL ," & vbCrLf
sConsulta = sConsulta & "  [PRES4] [int] NULL ," & vbCrLf
sConsulta = sConsulta & "  [PORCEN] [float] NOT NULL," & vbCrLf
sConsulta = sConsulta & "  [ANYOPAR] [smallint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "  [PAR1] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodPRESCON1) & ") NOT NULL ," & vbCrLf
sConsulta = sConsulta & "  [PAR2] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodPRESCON2) & ") NULL ," & vbCrLf
sConsulta = sConsulta & "  [PAR3] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodPRESCON3) & ") NULL ," & vbCrLf
sConsulta = sConsulta & "  [PAR4] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodPRESCON4) & ") NULL " & vbCrLf
sConsulta = sConsulta & ")" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[tmp_sc_ITEM_PRESCON] WITH NOCHECK" & vbCrLf
sConsulta = sConsulta & "ADD" & vbCrLf
sConsulta = sConsulta & " CONSTRAINT [DF_ITEM_PRESCON_PORCEN] DEFAULT (0) FOR [PORCEN]" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

 sConsulta = ""
sConsulta = sConsulta & "INSERT INTO [dbo].[tmp_sc_ITEM_PRESCON] ([ANYO], [GMN1], [PROCE], [ITEM], [PORCEN], [ANYOPAR], [PAR1], [PAR2], [PAR3], [PAR4]) " & vbCrLf
sConsulta = sConsulta & "  SELECT [ANYO], [GMN1], [PROCE], [ITEM], 1, [ANYOPAR], [PAR1], [PAR2], [PAR3], [PAR4] FROM [dbo].[ITEM_PRESCON]" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

'Actualizar los datos
'Primero pongo bien los porcentajes
sConsulta = "UPDATE tmp_sc_ITEM_PRESCON SET PORCEN= TAB.PORC FROM"
sConsulta = sConsulta & " (SELECT ANYO,GMN1,PROCE,ITEM,1/CAST(COUNT(ANYOPAR) AS FLOAT) AS PORC FROM tmp_sc_ITEM_PRESCON"
sConsulta = sConsulta & " GROUP BY ANYO,GMN1,PROCE,ITEM) AS TAB"
sConsulta = sConsulta & " INNER JOIN tmp_sc_ITEM_PRESCON ON TAB.ANYO = tmp_sc_ITEM_PRESCON.ANYO AND TAB.GMN1 = tmp_sc_ITEM_PRESCON.GMN1 AND"
sConsulta = sConsulta & " TAB.PROCE = tmp_sc_ITEM_PRESCON.PROCE AND TAB.ITEM = tmp_sc_ITEM_PRESCON.ITEM"
gRDOCon.Execute sConsulta, rdExecDirect
'Pongo los ID a presupuestos
sConsulta = "UPDATE tmp_sc_ITEM_PRESCON SET PRES1= PRES2_NIV1.ID FROM"
sConsulta = sConsulta & " tmp_sc_ITEM_PRESCON INNER JOIN PRES2_NIV1 ON PRES2_NIV1.ANYO = tmp_sc_ITEM_PRESCON.ANYOPAR"
sConsulta = sConsulta & " AND PRES2_NIV1.COD = tmp_sc_ITEM_PRESCON.PAR1 WHERE tmp_sc_ITEM_PRESCON.PAR1 IS NOT NULL"
sConsulta = sConsulta & " tmp_sc_ITEM_PRESCON.PAR2 IS NULL AND tmp_sc_ITEM_PRESCON.PAR3 IS NULL AND tmp_sc_ITEM_PRESCON.PAR4 IS NULL"
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = "UPDATE tmp_sc_ITEM_PRESCON SET PRES2= PRES2_NIV2.ID FROM"
sConsulta = sConsulta & " tmp_sc_ITEM_PRESCON INNER JOIN PRES2_NIV2 ON PRES2_NIV2.ANYO = tmp_sc_ITEM_PRESCON.ANYOPAR"
sConsulta = sConsulta & " AND PRES2_NIV2.PRES1 = tmp_sc_ITEM_PRESCON.PAR1 AND PRES2_NIV2.COD = tmp_sc_ITEM_PRESCON.PAR2 WHERE tmp_sc_ITEM_PRESCON.PAR1 IS NOT NULL"
sConsulta = sConsulta & " tmp_sc_ITEM_PRESCON.PAR2 IS NOT NULL AND tmp_sc_ITEM_PRESCON.PAR3 IS NULL AND tmp_sc_ITEM_PRESCON.PAR4 IS NULL"
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = "UPDATE tmp_sc_ITEM_PRESCON SET PRES3= PRES2_NIV3.ID FROM"
sConsulta = sConsulta & " tmp_sc_ITEM_PRESCON INNER JOIN PRES2_NIV3 ON PRES2_NIV3.ANYO = tmp_sc_ITEM_PRESCON.ANYOPAR"
sConsulta = sConsulta & " AND PRES2_NIV3.PRES1 = tmp_sc_ITEM_PRESCON.PAR1 AND PRES2_NIV3.PRES2 = tmp_sc_ITEM_PRESCON.PAR2 AND PRES2_NIV3.COD = tmp_sc_ITEM_PRESCON.PAR3"
sConsulta = sConsulta & " WHERE tmp_sc_ITEM_PRESCON.PAR1 IS NOT NULL"
sConsulta = sConsulta & " tmp_sc_ITEM_PRESCON.PAR2 IS NOT NULL AND tmp_sc_ITEM_PRESCON.PAR3 IS NOT NULL AND tmp_sc_ITEM_PRESCON.PAR4 IS NULL"
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = "UPDATE tmp_sc_ITEM_PRESCON SET PRES4= PRES2_NIV4.ID FROM"
sConsulta = sConsulta & " tmp_sc_ITEM_PRESCON INNER JOIN PRES2_NIV4 ON PRES2_NIV4.ANYO = tmp_sc_ITEM_PRESCON.ANYOPAR"
sConsulta = sConsulta & " AND PRES2_NIV4.PRES1 = tmp_sc_ITEM_PRESCON.PAR1 AND PRES2_NIV4.PRES2 = tmp_sc_ITEM_PRESCON.PAR2 AND PRES2_NIV4.PRES3 = tmp_sc_ITEM_PRESCON.PAR3"
sConsulta = sConsulta & "  AND PRES2_NIV4.COD = tmp_sc_ITEM_PRESCON.PAR4 WHERE tmp_sc_ITEM_PRESCON.PAR1 IS NOT NULL"
sConsulta = sConsulta & " tmp_sc_ITEM_PRESCON.PAR2 IS NOT NULL AND tmp_sc_ITEM_PRESCON.PAR3 IS NOT NULL AND tmp_sc_ITEM_PRESCON.PAR4 IS NOT NULL"
gRDOCon.Execute sConsulta, rdExecDirect


'Eliminar tabla y renombrar
sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ITEM_PRESCON]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)" & vbCrLf
sConsulta = sConsulta & "drop table [dbo].[ITEM_PRESCON]" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = "ALTER TABLE [dbo].[ tmp_sc_ITEM_PRESCON] DROP COLUMN ANYOPAR"
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = "ALTER TABLE [dbo].[ tmp_sc_ITEM_PRESCON] DROP COLUMN PAR1"
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = "ALTER TABLE [dbo].[ tmp_sc_ITEM_PRESCON] DROP COLUMN PAR2"
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = "ALTER TABLE [dbo].[ tmp_sc_ITEM_PRESCON] DROP COLUMN PAR3"
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = "ALTER TABLE [dbo].[ tmp_sc_ITEM_PRESCON] DROP COLUMN PAR4"
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = ""
sConsulta = sConsulta & "sp_rename 'dbo.tmp_sc_ITEM_PRESCON', 'ITEM_PRESCON'" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

'PK y unique contraint" & vbcrlf
sConsulta = "ALTER TABLE [dbo].[ITEM_PRESCON] WITH NOCHECK ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [PK_ITEM_PRESCON] PRIMARY KEY  NONCLUSTERED " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   ) WITH  FILLFACTOR = 90  ON [PRIMARY] ," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [UC_ITEM_PRESCON] UNIQUE  NONCLUSTERED " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [ANYO]," & vbCrLf
sConsulta = sConsulta & "       [GMN1]," & vbCrLf
sConsulta = sConsulta & "       [PROCE]," & vbCrLf
sConsulta = sConsulta & "       [ITEM]," & vbCrLf
sConsulta = sConsulta & "       [PRES1]," & vbCrLf
sConsulta = sConsulta & "       [PRES2]," & vbCrLf
sConsulta = sConsulta & "       [PRES3]," & vbCrLf
sConsulta = sConsulta & "       [PRES4]" & vbCrLf
sConsulta = sConsulta & "   )  ON [PRIMARY] ," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [CK_ITEM_PRESCON] CHECK ([PRES1] is not null and [PRES2] is null and [PRES3] is null and [PRES4] is null or [PRES1] is null and [PRES2] is not null and [PRES3] is null and [PRES4] is null or [PRES1] is null and [PRES2] is null and [PRES3] is not null and [PRES4] is null or [PRES1] is null and [PRES2] is null and [PRES3] is null and [PRES4] is not null)" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

'FKs" & vbcrlf
sConsulta = "ALTER TABLE [dbo].[ITEM_PRESCON] ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [FK_ITEM_PRESCON_ITEM] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [ANYO]," & vbCrLf
sConsulta = sConsulta & "       [GMN1]," & vbCrLf
sConsulta = sConsulta & "       [PROCE]," & vbCrLf
sConsulta = sConsulta & "       [ITEM]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[ITEM] (" & vbCrLf
sConsulta = sConsulta & "       [ANYO]," & vbCrLf
sConsulta = sConsulta & "       [GMN1]," & vbCrLf
sConsulta = sConsulta & "       [PROCE]," & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [FK_ITEM_PRESCON_PRES2_NIV1] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [PRES1]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[PRES2_NIV1] (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [FK_ITEM_PRESCON_PRES2_NIV2] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [PRES2]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[PRES2_NIV2] (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [FK_ITEM_PRESCON_PRES2_NIV3] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [PRES3]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[PRES2_NIV3] (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [FK_ITEM_PRESCON_PRES2_NIV4] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [PRES4]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[PRES2_NIV4] (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

End Sub

Private Sub V_10_ActulizarITEM_PRES3()
Dim sConsulta As String

'ITEM_PRESCON3
'  Remove foreign key constraints" & vbcrlf
sConsulta = "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[ITEM_PRESCON3] DROP CONSTRAINT [FK_ITEM_PRESCON3_ITEM]" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
 sConsulta = ""
sConsulta = sConsulta & "ALTER TABLE [dbo].[ITEM_PRESCON3] DROP CONSTRAINT [FK_ITEM_PRESCON3_PRES_3_CON1]" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
 sConsulta = ""
sConsulta = sConsulta & "ALTER TABLE [dbo].[ITEM_PRESCON3] DROP CONSTRAINT [FK_ITEM_PRESCON3_PRES_3_CON2]" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
 sConsulta = ""
sConsulta = sConsulta & "ALTER TABLE [dbo].[ITEM_PRESCON3] DROP CONSTRAINT [FK_ITEM_PRESCON3_PRES_3_CON3]" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
 sConsulta = ""
sConsulta = sConsulta & "ALTER TABLE [dbo].[ITEM_PRESCON3] DROP CONSTRAINT [FK_ITEM_PRESCON3_PRES_3_CON4]" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf

'  Script for dbo.ITEM_PRESCON3" & vbcrlf
'  Foreign keys etc. will appear at the end
sConsulta = ""
sConsulta = sConsulta & "CREATE TABLE [dbo].[tmp_sc_ITEM_PRESCON3]" & vbCrLf
sConsulta = sConsulta & "    (" & vbCrLf
sConsulta = sConsulta & "  [ID] [int] NOT NULL IDENTITY (1, 1) ," & vbCrLf
sConsulta = sConsulta & "  [ANYO] [smallint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "  [GMN1] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodGMN1) & ") NOT NULL ," & vbCrLf
sConsulta = sConsulta & "  [PROCE] [smallint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "  [ITEM] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "  [PRES1] [int] NULL ," & vbCrLf
sConsulta = sConsulta & "  [PRES2] [int] NULL ," & vbCrLf
sConsulta = sConsulta & "  [PRES3] [int] NULL ," & vbCrLf
sConsulta = sConsulta & "  [PRES4] [int] NULL ," & vbCrLf
sConsulta = sConsulta & "  [PORCEN] [float] NOT NULL," & vbCrLf
sConsulta = sConsulta & "  [PAR1] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodPRESCONCEP31) & ") NOT NULL ," & vbCrLf
sConsulta = sConsulta & "  [PAR2] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodPRESCONCEP32) & ") NULL ," & vbCrLf
sConsulta = sConsulta & "  [PAR3] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodPRESCONCEP33) & ") NULL ," & vbCrLf
sConsulta = sConsulta & "  [PAR4] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodPRESCONCEP34) & ") NULL " & vbCrLf
sConsulta = sConsulta & ")" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[tmp_sc_ITEM_PRESCON3] WITH NOCHECK" & vbCrLf
sConsulta = sConsulta & "ADD" & vbCrLf
sConsulta = sConsulta & " CONSTRAINT [DF_ITEM_PRESCON3_PORCEN] DEFAULT (0) FOR [PORCEN]" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

 sConsulta = ""
sConsulta = sConsulta & "INSERT INTO [dbo].[tmp_sc_ITEM_PRESCON3] ([ANYO], [GMN1], [PROCE], [ITEM], [PORCEN], [PAR1], [PAR2], [PAR3], [PAR4]) " & vbCrLf
sConsulta = sConsulta & "  SELECT [ANYO], [GMN1], [PROCE], [ITEM], 1, [PRES1], [PRES2], [PRES3], [PRES4] FROM [dbo].[ITEM_PRESCON3]" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

'Actualizar los datos
'Pongo los ID a presupuestos
sConsulta = "UPDATE tmp_sc_ITEM_PRESCON3 SET PRES1= PRES3_NIV1.ID FROM"
sConsulta = sConsulta & " tmp_sc_ITEM_PRESCON3 INNER JOIN PRES3_NIV1 ON "
sConsulta = sConsulta & " PRES3_NIV1.COD = tmp_sc_ITEM_PRESCON3.PAR1 WHERE tmp_sc_ITEM_PRESCON3.PAR1 IS NOT NULL"
sConsulta = sConsulta & " tmp_sc_ITEM_PRESCON3.PAR2 IS NULL AND tmp_sc_ITEM_PRESCON3.PAR3 IS NULL AND tmp_sc_ITEM_PRESCON3.PAR4 IS NULL"
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = "UPDATE tmp_sc_ITEM_PRESCON3 SET PRES2= PRES3_NIV2.ID FROM"
sConsulta = sConsulta & " tmp_sc_ITEM_PRESCON3 INNER JOIN PRES3_NIV2 ON "
sConsulta = sConsulta & " PRES3_NIV2.PRES1 = tmp_sc_ITEM_PRESCON3.PAR1 AND PRES3_NIV2.COD = tmp_sc_ITEM_PRESCON3.PAR2 WHERE tmp_sc_ITEM_PRESCON3.PAR1 IS NOT NULL"
sConsulta = sConsulta & " tmp_sc_ITEM_PRESCON3.PAR2 IS NOT NULL AND tmp_sc_ITEM_PRESCON3.PAR3 IS NULL AND tmp_sc_ITEM_PRESCON3.PAR4 IS NULL"
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = "UPDATE tmp_sc_ITEM_PRESCON3 SET PRES3= PRES3_NIV3.ID FROM"
sConsulta = sConsulta & " tmp_sc_ITEM_PRESCON3 INNER JOIN PRES3_NIV3 ON "
sConsulta = sConsulta & " PRES3_NIV3.PRES1 = tmp_sc_ITEM_PRESCON3.PAR1 AND PRES3_NIV3.PRES2 = tmp_sc_ITEM_PRESCON3.PAR2 AND PRES3_NIV3.COD = tmp_sc_ITEM_PRESCON3.PAR3"
sConsulta = sConsulta & " WHERE tmp_sc_ITEM_PRESCON3.PAR1 IS NOT NULL"
sConsulta = sConsulta & " tmp_sc_ITEM_PRESCON3.PAR2 IS NOT NULL AND tmp_sc_ITEM_PRESCON3.PAR3 IS NOT NULL AND tmp_sc_ITEM_PRESCON3.PAR4 IS NULL"
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = "UPDATE tmp_sc_ITEM_PRESCON3 SET PRES4= PRES3_NIV4.ID FROM"
sConsulta = sConsulta & " tmp_sc_ITEM_PRESCON3 INNER JOIN PRES3_NIV4 ON "
sConsulta = sConsulta & " PRES3_NIV4.PRES1 = tmp_sc_ITEM_PRESCON3.PAR1 AND PRES3_NIV4.PRES2 = tmp_sc_ITEM_PRESCON3.PAR2 AND PRES3_NIV4.PRES3 = tmp_sc_ITEM_PRESCON3.PAR3"
sConsulta = sConsulta & "  AND PRES3_NIV4.COD = tmp_sc_ITEM_PRESCON3.PAR4 WHERE tmp_sc_ITEM_PRESCON3.PAR1 IS NOT NULL"
sConsulta = sConsulta & " tmp_sc_ITEM_PRESCON3.PAR2 IS NOT NULL AND tmp_sc_ITEM_PRESCON3.PAR3 IS NOT NULL AND tmp_sc_ITEM_PRESCON3.PAR4 IS NOT NULL"
gRDOCon.Execute sConsulta, rdExecDirect


'Eliminar tabla y renombrar
sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ITEM_PRESCON3]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)" & vbCrLf
sConsulta = sConsulta & "drop table [dbo].[ITEM_PRESCON3]" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = "ALTER TABLE [dbo].[ tmp_sc_ITEM_PRESCON3] DROP COLUMN PAR1"
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = "ALTER TABLE [dbo].[ tmp_sc_ITEM_PRESCON3] DROP COLUMN PAR2"
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = "ALTER TABLE [dbo].[ tmp_sc_ITEM_PRESCON3] DROP COLUMN PAR3"
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = "ALTER TABLE [dbo].[ tmp_sc_ITEM_PRESCON3] DROP COLUMN PAR4"
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = ""
sConsulta = sConsulta & "sp_rename 'dbo.tmp_sc_ITEM_PRESCON3', 'ITEM_PRESCON3'" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

'PK y unique contraint" & vbcrlf
sConsulta = "ALTER TABLE [dbo].[ITEM_PRESCON3] WITH NOCHECK ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [PK_ITEM_PRESCON3] PRIMARY KEY  NONCLUSTERED " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   ) WITH  FILLFACTOR = 90  ON [PRIMARY] ," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [UC_ITEM_PRESCON3] UNIQUE  NONCLUSTERED " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [ANYO]," & vbCrLf
sConsulta = sConsulta & "       [GMN1]," & vbCrLf
sConsulta = sConsulta & "       [PROCE]," & vbCrLf
sConsulta = sConsulta & "       [ITEM]," & vbCrLf
sConsulta = sConsulta & "       [PRES1]," & vbCrLf
sConsulta = sConsulta & "       [PRES2]," & vbCrLf
sConsulta = sConsulta & "       [PRES3]," & vbCrLf
sConsulta = sConsulta & "       [PRES4]" & vbCrLf
sConsulta = sConsulta & "   )  ON [PRIMARY] ," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [CK_ITEM_PRESCON3] CHECK ([PRES1] is not null and [PRES2] is null and [PRES3] is null and [PRES4] is null or [PRES1] is null and [PRES2] is not null and [PRES3] is null and [PRES4] is null or [PRES1] is null and [PRES2] is null and [PRES3] is not null and [PRES4] is null or [PRES1] is null and [PRES2] is null and [PRES3] is null and [PRES4] is not null)" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

'FKs" & vbcrlf
sConsulta = "ALTER TABLE [dbo].[ITEM_PRESCON3] ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [FK_ITEM_PRESCON3_ITEM] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [ANYO]," & vbCrLf
sConsulta = sConsulta & "       [GMN1]," & vbCrLf
sConsulta = sConsulta & "       [PROCE]," & vbCrLf
sConsulta = sConsulta & "       [ITEM]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[ITEM] (" & vbCrLf
sConsulta = sConsulta & "       [ANYO]," & vbCrLf
sConsulta = sConsulta & "       [GMN1]," & vbCrLf
sConsulta = sConsulta & "       [PROCE]," & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [FK_ITEM_PRESCON3_PRES3_NIV1] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [PRES1]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[PRES3_NIV1] (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [FK_ITEM_PRESCON3_PRES3_NIV2] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [PRES2]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[PRES3_NIV2] (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [FK_ITEM_PRESCON3_PRES3_NIV3] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [PRES3]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[PRES3_NIV3] (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [FK_ITEM_PRESCON3_PRES3_NIV4] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [PRES4]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[PRES3_NIV4] (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

End Sub

Private Sub V_10_ActulizarITEM_PRES4()
Dim sConsulta As String

'ITEM_PRESCON4
'  Remove foreign key constraints" & vbcrlf
sConsulta = "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[ITEM_PRESCON4] DROP CONSTRAINT [FK_ITEM_PRESCON4_ITEM]" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
 sConsulta = ""
sConsulta = sConsulta & "ALTER TABLE [dbo].[ITEM_PRESCON4] DROP CONSTRAINT [FK_ITEM_PRESCON4_PRES_4_CON1]" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
 sConsulta = ""
sConsulta = sConsulta & "ALTER TABLE [dbo].[ITEM_PRESCON4] DROP CONSTRAINT [FK_ITEM_PRESCON4_PRES_4_CON2]" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
 sConsulta = ""
sConsulta = sConsulta & "ALTER TABLE [dbo].[ITEM_PRESCON4] DROP CONSTRAINT [FK_ITEM_PRESCON4_PRES_4_CON3]" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
 sConsulta = ""
sConsulta = sConsulta & "ALTER TABLE [dbo].[ITEM_PRESCON4] DROP CONSTRAINT [FK_ITEM_PRESCON4_PRES_4_CON4]" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf

'  Script for dbo.ITEM_PRESCON4" & vbcrlf
'  Foreign keys etc. will appear at the end
sConsulta = ""
sConsulta = sConsulta & "CREATE TABLE [dbo].[tmp_sc_ITEM_PRESCON4]" & vbCrLf
sConsulta = sConsulta & "    (" & vbCrLf
sConsulta = sConsulta & "  [ID] [int] NOT NULL IDENTITY (1, 1) ," & vbCrLf
sConsulta = sConsulta & "  [ANYO] [smallint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "  [GMN1] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodGMN1) & ") NOT NULL ," & vbCrLf
sConsulta = sConsulta & "  [PROCE] [smallint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "  [ITEM] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "  [PRES1] [int] NULL ," & vbCrLf
sConsulta = sConsulta & "  [PRES2] [int] NULL ," & vbCrLf
sConsulta = sConsulta & "  [PRES3] [int] NULL ," & vbCrLf
sConsulta = sConsulta & "  [PRES4] [int] NULL ," & vbCrLf
sConsulta = sConsulta & "  [PORCEN] [float] NOT NULL," & vbCrLf
sConsulta = sConsulta & "  [PAR1] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodPRESCONCEP41) & ") NOT NULL ," & vbCrLf
sConsulta = sConsulta & "  [PAR2] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodPRESCONCEP42) & ") NULL ," & vbCrLf
sConsulta = sConsulta & "  [PAR3] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodPRESCONCEP43) & ") NULL ," & vbCrLf
sConsulta = sConsulta & "  [PAR4] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodPRESCONCEP44) & ") NULL " & vbCrLf
sConsulta = sConsulta & ")" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[tmp_sc_ITEM_PRESCON4] WITH NOCHECK" & vbCrLf
sConsulta = sConsulta & "ADD" & vbCrLf
sConsulta = sConsulta & " CONSTRAINT [DF_ITEM_PRESCON4_PORCEN] DEFAULT (0) FOR [PORCEN]" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

 sConsulta = ""
sConsulta = sConsulta & "INSERT INTO [dbo].[tmp_sc_ITEM_PRESCON4] ([ANYO], [GMN1], [PROCE], [ITEM], [PORCEN], [PAR1], [PAR2], [PAR3], [PAR4]) " & vbCrLf
sConsulta = sConsulta & "  SELECT [ANYO], [GMN1], [PROCE], [ITEM], 1, [PRES1], [PRES2], [PRES3], [PRES4] FROM [dbo].[ITEM_PRESCON4]" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

'Actualizar los datos
'Pongo los ID a presupuestos
sConsulta = "UPDATE tmp_sc_ITEM_PRESCON4 SET PRES1= PRES4_NIV1.ID FROM"
sConsulta = sConsulta & " tmp_sc_ITEM_PRESCON4 INNER JOIN PRES4_NIV1 ON "
sConsulta = sConsulta & " PRES4_NIV1.COD = tmp_sc_ITEM_PRESCON4.PAR1 WHERE tmp_sc_ITEM_PRESCON4.PAR1 IS NOT NULL"
sConsulta = sConsulta & " tmp_sc_ITEM_PRESCON4.PAR2 IS NULL AND tmp_sc_ITEM_PRESCON4.PAR3 IS NULL AND tmp_sc_ITEM_PRESCON4.PAR4 IS NULL"
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = "UPDATE tmp_sc_ITEM_PRESCON4 SET PRES2= PRES4_NIV2.ID FROM"
sConsulta = sConsulta & " tmp_sc_ITEM_PRESCON4 INNER JOIN PRES4_NIV2 ON "
sConsulta = sConsulta & " PRES4_NIV2.PRES1 = tmp_sc_ITEM_PRESCON4.PAR1 AND PRES4_NIV2.COD = tmp_sc_ITEM_PRESCON4.PAR2 WHERE tmp_sc_ITEM_PRESCON4.PAR1 IS NOT NULL"
sConsulta = sConsulta & " tmp_sc_ITEM_PRESCON4.PAR2 IS NOT NULL AND tmp_sc_ITEM_PRESCON4.PAR3 IS NULL AND tmp_sc_ITEM_PRESCON4.PAR4 IS NULL"
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = "UPDATE tmp_sc_ITEM_PRESCON4 SET PRES3= PRES4_NIV3.ID FROM"
sConsulta = sConsulta & " tmp_sc_ITEM_PRESCON4 INNER JOIN PRES4_NIV3 ON "
sConsulta = sConsulta & " PRES4_NIV3.PRES1 = tmp_sc_ITEM_PRESCON4.PAR1 AND PRES4_NIV3.PRES2 = tmp_sc_ITEM_PRESCON4.PAR2 AND PRES4_NIV3.COD = tmp_sc_ITEM_PRESCON4.PAR3"
sConsulta = sConsulta & " WHERE tmp_sc_ITEM_PRESCON4.PAR1 IS NOT NULL"
sConsulta = sConsulta & " tmp_sc_ITEM_PRESCON4.PAR2 IS NOT NULL AND tmp_sc_ITEM_PRESCON4.PAR3 IS NOT NULL AND tmp_sc_ITEM_PRESCON4.PAR4 IS NULL"
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = "UPDATE tmp_sc_ITEM_PRESCON4 SET PRES4= PRES4_NIV4.ID FROM"
sConsulta = sConsulta & " tmp_sc_ITEM_PRESCON4 INNER JOIN PRES4_NIV4 ON "
sConsulta = sConsulta & " PRES4_NIV4.PRES1 = tmp_sc_ITEM_PRESCON4.PAR1 AND PRES4_NIV4.PRES2 = tmp_sc_ITEM_PRESCON4.PAR2 AND PRES4_NIV4.PRES3 = tmp_sc_ITEM_PRESCON4.PAR3"
sConsulta = sConsulta & "  AND PRES4_NIV4.COD = tmp_sc_ITEM_PRESCON4.PAR4 WHERE tmp_sc_ITEM_PRESCON4.PAR1 IS NOT NULL"
sConsulta = sConsulta & " tmp_sc_ITEM_PRESCON4.PAR2 IS NOT NULL AND tmp_sc_ITEM_PRESCON4.PAR3 IS NOT NULL AND tmp_sc_ITEM_PRESCON4.PAR4 IS NOT NULL"
gRDOCon.Execute sConsulta, rdExecDirect


'Eliminar tabla y renombrar
sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ITEM_PRESCON4]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)" & vbCrLf
sConsulta = sConsulta & "drop table [dbo].[ITEM_PRESCON4]" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = "ALTER TABLE [dbo].[ tmp_sc_ITEM_PRESCON4] DROP COLUMN PAR1"
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = "ALTER TABLE [dbo].[ tmp_sc_ITEM_PRESCON4] DROP COLUMN PAR2"
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = "ALTER TABLE [dbo].[ tmp_sc_ITEM_PRESCON4] DROP COLUMN PAR3"
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = "ALTER TABLE [dbo].[ tmp_sc_ITEM_PRESCON4] DROP COLUMN PAR4"
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = ""
sConsulta = sConsulta & "sp_rename 'dbo.tmp_sc_ITEM_PRESCON4', 'ITEM_PRESCON4'" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

'PK y unique contraint" & vbcrlf
sConsulta = "ALTER TABLE [dbo].[ITEM_PRESCON4] WITH NOCHECK ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [PK_ITEM_PRESCON4] PRIMARY KEY  NONCLUSTERED " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   ) WITH  FILLFACTOR = 90  ON [PRIMARY] ," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [UC_ITEM_PRESCON4] UNIQUE  NONCLUSTERED " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [ANYO]," & vbCrLf
sConsulta = sConsulta & "       [GMN1]," & vbCrLf
sConsulta = sConsulta & "       [PROCE]," & vbCrLf
sConsulta = sConsulta & "       [ITEM]," & vbCrLf
sConsulta = sConsulta & "       [PRES1]," & vbCrLf
sConsulta = sConsulta & "       [PRES2]," & vbCrLf
sConsulta = sConsulta & "       [PRES3]," & vbCrLf
sConsulta = sConsulta & "       [PRES4]" & vbCrLf
sConsulta = sConsulta & "   )  ON [PRIMARY] ," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [CK_ITEM_PRESCON4] CHECK ([PRES1] is not null and [PRES2] is null and [PRES3] is null and [PRES4] is null or [PRES1] is null and [PRES2] is not null and [PRES3] is null and [PRES4] is null or [PRES1] is null and [PRES2] is null and [PRES3] is not null and [PRES4] is null or [PRES1] is null and [PRES2] is null and [PRES3] is null and [PRES4] is not null)" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

'FKs" & vbcrlf
sConsulta = "ALTER TABLE [dbo].[ITEM_PRESCON4] ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [FK_ITEM_PRESCON4_ITEM] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [ANYO]," & vbCrLf
sConsulta = sConsulta & "       [GMN1]," & vbCrLf
sConsulta = sConsulta & "       [PROCE]," & vbCrLf
sConsulta = sConsulta & "       [ITEM]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[ITEM] (" & vbCrLf
sConsulta = sConsulta & "       [ANYO]," & vbCrLf
sConsulta = sConsulta & "       [GMN1]," & vbCrLf
sConsulta = sConsulta & "       [PROCE]," & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [FK_ITEM_PRESCON4_PRES4_NIV1] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [PRES1]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[PRES4_NIV1] (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [FK_ITEM_PRESCON4_PRES4_NIV2] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [PRES2]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[PRES4_NIV2] (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [FK_ITEM_PRESCON4_PRES4_NIV3] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [PRES3]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[PRES4_NIV3] (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [FK_ITEM_PRESCON4_PRES4_NIV4] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [PRES4]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[PRES4_NIV4] (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

End Sub


Private Sub V_10_ActulizarITEM_PRES1()
Dim rdores As RDO.rdoResultset
Dim sConsulta As String

'ITEM_PRESPROY
'  Remove foreign key constraints" & vbcrlf
sConsulta = "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[ITEM_PRESPROY] DROP CONSTRAINT [ITEM_FK_ITEM_PRESPROY]" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
 sConsulta = ""
sConsulta = sConsulta & "ALTER TABLE [dbo].[ITEM_PRESPROY] DROP CONSTRAINT [PRES_PROY1_FK_ITEM_PRESPROY]" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
 sConsulta = ""
sConsulta = sConsulta & "ALTER TABLE [dbo].[ITEM_PRESPROY] DROP CONSTRAINT [PRES_PROY2_FK_ITEM_PRESPROY]" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
 sConsulta = ""
sConsulta = sConsulta & "ALTER TABLE [dbo].[ITEM_PRESPROY] DROP CONSTRAINT [PRES_PROY3_FK_ITEM_PRESPROY]" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect
 sConsulta = ""
sConsulta = sConsulta & "ALTER TABLE [dbo].[ITEM_PRESPROY] DROP CONSTRAINT [PRES_PROY4_FK_ITEM_PRESPROY]" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf

'  Script for dbo.ITEM_PRESPROY" & vbcrlf
'  Foreign keys etc. will appear at the end
sConsulta = ""
sConsulta = sConsulta & "CREATE TABLE [dbo].[tmp_sc_ITEM_PRESPROY]" & vbCrLf
sConsulta = sConsulta & "    (" & vbCrLf
sConsulta = sConsulta & "  [ID] [int] NOT NULL IDENTITY (1, 1) ," & vbCrLf
sConsulta = sConsulta & "  [ANYO] [smallint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "  [GMN1] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodGMN1) & ") NOT NULL ," & vbCrLf
sConsulta = sConsulta & "  [PROCE] [smallint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "  [ITEM] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "  [PRES1] [int] NULL ," & vbCrLf
sConsulta = sConsulta & "  [PRES2] [int] NULL ," & vbCrLf
sConsulta = sConsulta & "  [PRES3] [int] NULL ," & vbCrLf
sConsulta = sConsulta & "  [PRES4] [int] NULL ," & vbCrLf
sConsulta = sConsulta & "  [PORCEN] [float] NOT NULL," & vbCrLf
sConsulta = sConsulta & "  [ANYOPROY] [smallint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "  [PROY1] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodPRESPROY1) & ") NOT NULL ," & vbCrLf
sConsulta = sConsulta & "  [PROY2] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodPRESPROY2) & ") NULL ," & vbCrLf
sConsulta = sConsulta & "  [PROY3] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodPRESPROY3) & ") NULL ," & vbCrLf
sConsulta = sConsulta & "  [PROY4] [varchar] (" & CStr(gLongitudesDeCodigos.giLongCodPRESPROY4) & ") NULL " & vbCrLf
sConsulta = sConsulta & ")" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE [dbo].[tmp_sc_ITEM_PRESPROY] WITH NOCHECK" & vbCrLf
sConsulta = sConsulta & "ADD" & vbCrLf
sConsulta = sConsulta & " CONSTRAINT [DF_ITEM_PRESPROY_PORCEN] DEFAULT (0) FOR [PORCEN]" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

 sConsulta = ""
sConsulta = sConsulta & "INSERT INTO [dbo].[tmp_sc_ITEM_PRESPROY] ([ANYO], [GMN1], [PROCE], [ITEM], [PORCEN], [ANYOPROY], [PROY1], [PROY2], [PROY3], [PROY4]) " & vbCrLf
sConsulta = sConsulta & "  SELECT [ANYO], [GMN1], [PROCE], [ITEM], 1, [ANYOPROY], [PROY1], [PROY2], [PROY3], [PROY4] FROM [dbo].[ITEM_PRESPROY]" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

'Actualizar los datos
'Primero pongo bien los porcentajes
sConsulta = "UPDATE tmp_sc_ITEM_PRESPROY SET PORCEN= TAB.PORC FROM"
sConsulta = sConsulta & " (SELECT ANYO,GMN1,PROCE,ITEM,1/CAST(COUNT(ANYOPROY) AS FLOAT) AS PORC FROM tmp_sc_ITEM_PRESPROY"
sConsulta = sConsulta & " GROUP BY ANYO,GMN1,PROCE,ITEM) AS TAB"
sConsulta = sConsulta & " INNER JOIN tmp_sc_ITEM_PRESPROY ON TAB.ANYO = tmp_sc_ITEM_PRESPROY.ANYO AND TAB.GMN1 = tmp_sc_ITEM_PRESPROY.GMN1 AND"
sConsulta = sConsulta & " TAB.PROCE = tmp_sc_ITEM_PRESPROY.PROCE AND TAB.ITEM = tmp_sc_ITEM_PRESPROY.ITEM"
gRDOCon.Execute sConsulta, rdExecDirect
'Pongo los ID a presupuestos
sConsulta = "UPDATE tmp_sc_ITEM_PRESPROY SET PRES1= PRES1_NIV1.ID FROM"
sConsulta = sConsulta & " tmp_sc_ITEM_PRESPROY INNER JOIN PRES1_NIV1 ON PRES1_NIV1.ANYO = tmp_sc_ITEM_PRESPROY.ANYOPROY"
sConsulta = sConsulta & " AND PRES1_NIV1.COD = tmp_sc_ITEM_PRESPROY.PROY1 WHERE tmp_sc_ITEM_PRESPROY.PROY1 IS NOT NULL"
sConsulta = sConsulta & " tmp_sc_ITEM_PRESPROY.PROY2 IS NULL AND tmp_sc_ITEM_PRESPROY.PROY3 IS NULL AND tmp_sc_ITEM_PRESPROY.PROY4 IS NULL"
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = "UPDATE tmp_sc_ITEM_PRESPROY SET PRES2= PRES1_NIV2.ID FROM"
sConsulta = sConsulta & " tmp_sc_ITEM_PRESPROY INNER JOIN PRES1_NIV2 ON PRES1_NIV2.ANYO = tmp_sc_ITEM_PRESPROY.ANYOPROY"
sConsulta = sConsulta & " AND PRES1_NIV2.PRES1 = tmp_sc_ITEM_PRESPROY.PROY1 AND PRES1_NIV2.COD = tmp_sc_ITEM_PRESPROY.PROY2 WHERE tmp_sc_ITEM_PRESPROY.PROY1 IS NOT NULL"
sConsulta = sConsulta & " tmp_sc_ITEM_PRESPROY.PROY2 IS NOT NULL AND tmp_sc_ITEM_PRESPROY.PROY3 IS NULL AND tmp_sc_ITEM_PRESPROY.PROY4 IS NULL"
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = "UPDATE tmp_sc_ITEM_PRESPROY SET PRES3= PRES1_NIV3.ID FROM"
sConsulta = sConsulta & " tmp_sc_ITEM_PRESPROY INNER JOIN PRES1_NIV3 ON PRES1_NIV3.ANYO = tmp_sc_ITEM_PRESPROY.ANYOPROY"
sConsulta = sConsulta & " AND PRES1_NIV3.PRES1 = tmp_sc_ITEM_PRESPROY.PROY1 AND PRES1_NIV3.PRES2 = tmp_sc_ITEM_PRESPROY.PROY2 AND PRES1_NIV3.COD = tmp_sc_ITEM_PRESPROY.PROY3"
sConsulta = sConsulta & " WHERE tmp_sc_ITEM_PRESPROY.PROY1 IS NOT NULL"
sConsulta = sConsulta & " tmp_sc_ITEM_PRESPROY.PROY2 IS NOT NULL AND tmp_sc_ITEM_PRESPROY.PROY3 IS NOT NULL AND tmp_sc_ITEM_PRESPROY.PROY4 IS NULL"
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = "UPDATE tmp_sc_ITEM_PRESPROY SET PRES4= PRES1_NIV4.ID FROM"
sConsulta = sConsulta & " tmp_sc_ITEM_PRESPROY INNER JOIN PRES1_NIV4 ON PRES1_NIV4.ANYO = tmp_sc_ITEM_PRESPROY.ANYOPROY"
sConsulta = sConsulta & " AND PRES1_NIV4.PRES1 = tmp_sc_ITEM_PRESPROY.PROY1 AND PRES1_NIV4.PRES2 = tmp_sc_ITEM_PRESPROY.PROY2 AND PRES1_NIV4.PRES3 = tmp_sc_ITEM_PRESPROY.PROY3"
sConsulta = sConsulta & "  AND PRES1_NIV4.COD = tmp_sc_ITEM_PRESPROY.PROY4 WHERE tmp_sc_ITEM_PRESPROY.PROY1 IS NOT NULL"
sConsulta = sConsulta & " tmp_sc_ITEM_PRESPROY.PROY2 IS NOT NULL AND tmp_sc_ITEM_PRESPROY.PROY3 IS NOT NULL AND tmp_sc_ITEM_PRESPROY.PROY4 IS NOT NULL"
gRDOCon.Execute sConsulta, rdExecDirect


'Eliminar tabla y renombrar
sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ITEM_PRESPROY]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)" & vbCrLf
sConsulta = sConsulta & "drop table [dbo].[ITEM_PRESPROY]" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = "ALTER TABLE [dbo].[ tmp_sc_ITEM_PRESPROY] DROP COLUMN ANYOPROY"
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = "ALTER TABLE [dbo].[ tmp_sc_ITEM_PRESPROY] DROP COLUMN PROY1"
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = "ALTER TABLE [dbo].[ tmp_sc_ITEM_PRESPROY] DROP COLUMN PROY2"
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = "ALTER TABLE [dbo].[ tmp_sc_ITEM_PRESPROY] DROP COLUMN PROY3"
gRDOCon.Execute sConsulta, rdExecDirect
sConsulta = "ALTER TABLE [dbo].[ tmp_sc_ITEM_PRESPROY] DROP COLUMN PROY4"
gRDOCon.Execute sConsulta, rdExecDirect

sConsulta = ""
sConsulta = sConsulta & "sp_rename 'dbo.tmp_sc_ITEM_PRESPROY', 'ITEM_PRESPROY'" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

'PK y unique contraint" & vbcrlf
sConsulta = "ALTER TABLE [dbo].[ITEM_PRESPROY] WITH NOCHECK ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [PK_ITEM_PRESPROY] PRIMARY KEY  NONCLUSTERED " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   ) WITH  FILLFACTOR = 90  ON [PRIMARY] ," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [UC_ITEM_PRESPROY] UNIQUE  NONCLUSTERED " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [ANYO]," & vbCrLf
sConsulta = sConsulta & "       [GMN1]," & vbCrLf
sConsulta = sConsulta & "       [PROCE]," & vbCrLf
sConsulta = sConsulta & "       [ITEM]," & vbCrLf
sConsulta = sConsulta & "       [PRES1]," & vbCrLf
sConsulta = sConsulta & "       [PRES2]," & vbCrLf
sConsulta = sConsulta & "       [PRES3]," & vbCrLf
sConsulta = sConsulta & "       [PRES4]" & vbCrLf
sConsulta = sConsulta & "   )  ON [PRIMARY] ," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [CK_ITEM_PRESPROY] CHECK ([PRES1] is not null and [PRES2] is null and [PRES3] is null and [PRES4] is null or [PRES1] is null and [PRES2] is not null and [PRES3] is null and [PRES4] is null or [PRES1] is null and [PRES2] is null and [PRES3] is not null and [PRES4] is null or [PRES1] is null and [PRES2] is null and [PRES3] is null and [PRES4] is not null)" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

'FKs" & vbcrlf
sConsulta = "ALTER TABLE [dbo].[ITEM_PRESPROY] ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [FK_ITEM_PRESPROY_ITEM] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [ANYO]," & vbCrLf
sConsulta = sConsulta & "       [GMN1]," & vbCrLf
sConsulta = sConsulta & "       [PROCE]," & vbCrLf
sConsulta = sConsulta & "       [ITEM]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[ITEM] (" & vbCrLf
sConsulta = sConsulta & "       [ANYO]," & vbCrLf
sConsulta = sConsulta & "       [GMN1]," & vbCrLf
sConsulta = sConsulta & "       [PROCE]," & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [FK_ITEM_PRESPROY_PRES1_NIV1] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [PRES1]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[PRES1_NIV1] (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [FK_ITEM_PRESPROY_PRES1_NIV2] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [PRES2]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[PRES1_NIV2] (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [FK_ITEM_PRESPROY_PRES1_NIV3] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [PRES3]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[PRES1_NIV3] (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [FK_ITEM_PRESPROY_PRES1_NIV4] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [PRES4]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[PRES1_NIV4] (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
gRDOCon.Execute sConsulta, rdExecDirect

End Sub


