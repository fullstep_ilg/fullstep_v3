Attribute VB_Name = "bas_V_2_16_5"
Option Explicit
Public iTipoSinc As Integer
Private sFSP As String
Private oADOConnection As ADODB.Connection

Public Function CodigoDeActualizacion2_16_00_04A2_16_05_00() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim iRespuesta As Integer

On Error GoTo Error
sConsulta = "BEGIN TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = True
ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

frmProgreso.lblDetalle = "A�adimos campos"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
V_2_16_5_Tablas_0


frmProgreso.lblDetalle = "Actualizamos triggers"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
V_2_16_5_Triggers_0


sConsulta = "UPDATE VERSION SET NUM ='2.16.05.00'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "COMMIT TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = False

CodigoDeActualizacion2_16_00_04A2_16_05_00 = True
Exit Function
    
Error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_16_00_04A2_16_05_00 = False

End Function

Public Function CodigoDeActualizacion2_16_05_00A2_16_05_01() As Boolean
Dim sConsulta As String
Dim oRS As ADODB.Recordset
Dim iIndice As Integer
Dim sIdiomas() As String
Dim i As Integer
Dim bTransaccionEnCurso As Boolean
Dim iRespuesta As Integer
Dim oAdores As ADODB.Recordset
Dim lId As Long
Dim lACT1 As Long
Dim lACT2 As Long
Dim lACT3 As Long
Dim lACT4 As Long
Dim lProgresBar As Long
Dim iINSWEB As Integer

On Error GoTo Error

iRespuesta = MsgBox("�Desea activar la sincronizaci�n entre materiales del GS y actividades del portal?", vbQuestion + vbYesNo)
If iRespuesta = vbYes Then
    sConsulta = "���IMPORTANTE!!!  LEA CON ATENCION" & vbCrLf & vbCrLf & "EL PROCESO DE SINCRONIZACION REALIZAR� LAS SIGUIENTES ACCIONES:"
    sConsulta = sConsulta & vbCrLf
    sConsulta = sConsulta & vbCrLf & "   - ELIMINAR� LA ESTRUCTURA DE ACTIVIDADES DEL PORTAL"
    sConsulta = sConsulta & vbCrLf & "   - COPIAR� LA ESTRUCTURA DE MATERIALES DEL GS EN LAS ACTIVIDADES DEL PORTAL"
    sConsulta = sConsulta & vbCrLf & "   - PODR� ELEGIR QU� ASIGNACI�N DE MATERIALES A PROVEEDORES DESEA (GS O PORTAL)"
    sConsulta = sConsulta & vbCrLf
    sConsulta = sConsulta & vbCrLf & "�DESEA REALMENTE ACTIVAR LA SINCRONIZACI�N DE MATERIALES Y ACTIVIADES?" & vbCrLf
    sConsulta = sConsulta & vbCrLf & "Si elige 'No' se continur� con la actualizaci�n de la base de datos sin sincronizar los materiales y actividades"
    
    iRespuesta = MsgBox(sConsulta, vbQuestion + vbYesNo)
End If

Set oADOConnection = New ADODB.Connection
oADOConnection.Open "Provider=SQLOLEDB.1;Auto Translate=FALSE;Server=" & gServer & ";Database=" & gBD & ";", gUID, gPWD
oADOConnection.CursorLocation = adUseClient
oADOConnection.CommandTimeout = 120

Set oRS = New ADODB.Recordset
sConsulta = "SELECT INSTWEB FROM PARGEN_INTERNO"
oRS.Open sConsulta, oADOConnection, adOpenForwardOnly, adLockReadOnly
If Not oRS.eof Then iINSWEB = oRS(0).Value
oRS.Close

If iRespuesta = vbNo Then
    oADOConnection.Execute "BEGIN DISTRIBUTED TRAN"
    bTransaccionEnCurso = True
    oADOConnection.Execute "SET XACT_ABORT ON"
        
    If iINSWEB = 2 Then
        oRS.Open "SELECT FSP_SRV, FSP_BD FROM PARGEN_PORT", oADOConnection, adOpenForwardOnly, adLockReadOnly
        sFSP = oRS.Fields("FSP_SRV").Value & "." & oRS.Fields("FSP_BD").Value & ".dbo."
        oRS.Close
        
        '1�-Dar de alta en el GS los c�digos de idiomas del portal
        sConsulta = "SELECT * FROM " & sFSP & "IDI WHERE COD NOT IN (SELECT COD FROM IDIOMAS)"
        oRS.Open sConsulta, oADOConnection, adOpenForwardOnly, adLockReadOnly
        While Not oRS.eof
            sConsulta = "INSERT INTO IDIOMAS (COD,DEN,APLICACION) VALUES ('" & DblQuote(oRS.Fields("COD").Value) & "','" & DblQuote(oRS.Fields("DEN").Value) & "',0)" & vbCrLf
            oADOConnection.Execute sConsulta
            oRS.MoveNext
        Wend
        oRS.Close
    End If
    
    frmProgreso.lblDetalle = "Modificar las tablas GMN"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1 '4
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1

    '2�-Modificar las tablas GMN para que soporten una denominaci�n en cada idioma de portal
    sConsulta = "SELECT COD FROM IDIOMAS"
    oRS.Open sConsulta, oADOConnection, adOpenForwardOnly, adLockReadOnly
    While Not oRS.eof
        sConsulta = "ALTER TABLE [dbo].[GMN1] ADD [DEN_" & oRS.Fields("COD").Value & "] [varchar] (100) NULL" & vbCrLf
        oADOConnection.Execute sConsulta
        sConsulta = "ALTER TABLE [dbo].[GMN2] ADD [DEN_" & oRS.Fields("COD").Value & "] [varchar] (100) NULL" & vbCrLf
        oADOConnection.Execute sConsulta
        sConsulta = "ALTER TABLE [dbo].[GMN3] ADD [DEN_" & oRS.Fields("COD").Value & "] [varchar] (100) NULL" & vbCrLf
        oADOConnection.Execute sConsulta
        sConsulta = "ALTER TABLE [dbo].[GMN4] ADD [DEN_" & oRS.Fields("COD").Value & "] [varchar] (100) NULL" & vbCrLf
        oADOConnection.Execute sConsulta
        iIndice = iIndice + 1
        ReDim Preserve sIdiomas(iIndice)
        sIdiomas(iIndice) = oRS.Fields("COD").Value
        oRS.MoveNext
    Wend
    oRS.Close

    'El campo DEN se pone en todos los idiomas nuevos
    sConsulta = "UPDATE GMN1 SET DEN_SPA=DEN"
    oADOConnection.Execute sConsulta
    
    sConsulta = "UPDATE GMN2 SET DEN_SPA=DEN"
    oADOConnection.Execute sConsulta
    
    sConsulta = "UPDATE GMN3 SET DEN_SPA=DEN"
    oADOConnection.Execute sConsulta
    
    sConsulta = "UPDATE GMN4 SET DEN_SPA=DEN"
    oADOConnection.Execute sConsulta

    sConsulta = "IF EXISTS (SELECT name FROM sysindexes WHERE name = 'IX_GMN1_01')" & vbCrLf
    sConsulta = sConsulta & "   DROP INDEX GMN1.IX_GMN1_01" & vbCrLf
    sConsulta = sConsulta & "   ALTER TABLE dbo.GMN1 DROP COLUMN DEN" & vbCrLf
    sConsulta = sConsulta & "IF EXISTS (SELECT name FROM sysindexes WHERE name = 'IX_GMN2_01')" & vbCrLf
    sConsulta = sConsulta & "   DROP INDEX GMN2.IX_GMN2_01" & vbCrLf
    sConsulta = sConsulta & "IF EXISTS (SELECT name FROM sysindexes WHERE name = 'IX_GMN2_02')" & vbCrLf
    sConsulta = sConsulta & "   DROP INDEX GMN2.IX_GMN2_02" & vbCrLf
    sConsulta = sConsulta & "   ALTER TABLE dbo.GMN2 DROP COLUMN DEN" & vbCrLf
    sConsulta = sConsulta & "IF EXISTS (SELECT name FROM sysindexes WHERE name = 'IX_GMN3_01')" & vbCrLf
    sConsulta = sConsulta & "   DROP INDEX GMN3.IX_GMN3_01" & vbCrLf
    sConsulta = sConsulta & "IF EXISTS (SELECT name FROM sysindexes WHERE name = 'IX_GMN3_02')" & vbCrLf
    sConsulta = sConsulta & "   DROP INDEX GMN3.IX_GMN3_02" & vbCrLf
    sConsulta = sConsulta & "   ALTER TABLE dbo.GMN3 DROP COLUMN DEN" & vbCrLf
    sConsulta = sConsulta & "IF EXISTS (SELECT name FROM sysindexes WHERE name = 'IX_GMN4_01')" & vbCrLf
    sConsulta = sConsulta & "   DROP INDEX GMN4.IX_GMN4_01" & vbCrLf
    sConsulta = sConsulta & "IF EXISTS (SELECT name FROM sysindexes WHERE name = 'IX_GMN4_02')" & vbCrLf
    sConsulta = sConsulta & "   DROP INDEX GMN4.IX_GMN4_02" & vbCrLf
    sConsulta = sConsulta & "   ALTER TABLE dbo.GMN4 DROP COLUMN DEN" & vbCrLf
    oADOConnection.Execute sConsulta

    CodigoDeActualizacion2_16_05_00A2_16_05_01 = True
    sConsulta = "UPDATE VERSION SET NUM ='2.16.05.01'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
    oADOConnection.Execute sConsulta
    
    oADOConnection.Execute "COMMIT TRAN"
    bTransaccionEnCurso = False
    oADOConnection.Execute "SET XACT_ABORT OFF"
    
    Set oRS = Nothing
    oADOConnection.Close
    Set oADOConnection = Nothing
    Exit Function

End If

If iINSWEB = 2 Then
    frmTIPOSinProveMat.Show 1
    If iTipoSinc = 99 Then 'Cancelado
        CodigoDeActualizacion2_16_05_00A2_16_05_01 = False
        Set oRS = Nothing
        oADOConnection.Close
        Set oADOConnection = Nothing
        Exit Function
    End If
    If iTipoSinc = 0 Then iTipoSinc = 1

End If

oADOConnection.Execute "BEGIN DISTRIBUTED TRAN"
bTransaccionEnCurso = True
oADOConnection.Execute "SET XACT_ABORT ON"

If iINSWEB = 2 Then
    oRS.Open "SELECT FSP_SRV, FSP_BD FROM PARGEN_PORT", oADOConnection, adOpenForwardOnly, adLockReadOnly
    sFSP = oRS.Fields("FSP_SRV").Value & "." & oRS.Fields("FSP_BD").Value & ".dbo."
    oRS.Close
    
    lProgresBar = frmProgreso.ProgressBar1.Max
    frmProgreso.ProgressBar1.Max = 17
    frmProgreso.lblDetalle = "Eliminar actividades de nivel 5"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = 1
    
    sConsulta = "SELECT NIVELMINACT FROM PARGEN_GEST"
    oRS.Open "EXEC " & sFSP & "sp_executesql N'" & sConsulta & "'", oADOConnection, adOpenForwardOnly, adLockReadOnly
    If Not oRS.eof Then
        If oRS(0).Value = 5 Then
            sConsulta = "UPDATE PARGEN_GEST SET NIVELMINACT=4"
            oADOConnection.Execute "EXEC " & sFSP & "sp_executesql N'" & sConsulta & "'"
        End If
    End If
    oRS.Close
    
    frmProgreso.lblDetalle = "Eliminar todos los registros de las tablas de actividades"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1 '2
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
    'Copio la asignaci�n proveedores-actividades del portal
    If iTipoSinc > 1 Then
        sConsulta = "CREATE TABLE [dbo].[ACIAS_ACT1] ([CIA] [int] NOT NULL ,[ACT1] [varchar] (20) NOT NULL ) ON [PRIMARY]"
        oADOConnection.Execute sConsulta '"EXEC " & sFSP & "sp_executesql N'" & sConsulta & "'"
        sConsulta = "CREATE TABLE [dbo].[ACIAS_ACT2] ([CIA] [int] NOT NULL ,[ACT1] [varchar] (20) NOT NULL ,[ACT2] [varchar] (20) NOT NULL ) ON [PRIMARY]"
        oADOConnection.Execute sConsulta
        sConsulta = "CREATE TABLE [dbo].[ACIAS_ACT3] ([CIA] [int] NOT NULL ,[ACT1] [varchar] (20) NOT NULL ,[ACT2] [varchar] (20) NOT NULL ,[ACT3] [varchar] (20) NOT NULL ) ON [PRIMARY]"
        oADOConnection.Execute sConsulta
        sConsulta = "CREATE TABLE [dbo].[ACIAS_ACT4] ([CIA] [int] NOT NULL ,[ACT1] [varchar] (20) NOT NULL ,[ACT2] [varchar] (20) NOT NULL ,[ACT3] [varchar] (20) NOT NULL ,[ACT4] [varchar] (20) NOT NULL) ON [PRIMARY]"
        oADOConnection.Execute sConsulta
        
        sConsulta = "INSERT INTO ACIAS_ACT1 (CIA,ACT1) SELECT CA.CIA,A1.COD FROM " & sFSP & "CIAS_ACT1 CA INNER JOIN " & sFSP & "ACT1 A1 ON CA.ACT1=A1.ID"
        oADOConnection.Execute sConsulta
        
        sConsulta = "INSERT INTO ACIAS_ACT2 (CIA,ACT1,ACT2) SELECT CA.CIA,A1.COD,A2.COD FROM " & sFSP & "CIAS_ACT2 CA INNER JOIN " & sFSP & "ACT1 A1 ON CA.ACT1=A1.ID INNER JOIN " & sFSP & "ACT2 A2 ON CA.ACT1=A2.ACT1 AND CA.ACT2=A2.ID"
        oADOConnection.Execute sConsulta
        
        sConsulta = "INSERT INTO ACIAS_ACT3 (CIA,ACT1,ACT2,ACT3) SELECT CA.CIA,A1.COD,A2.COD,A3.COD FROM " & sFSP & "CIAS_ACT3 CA INNER JOIN " & sFSP & "ACT1 A1 ON CA.ACT1=A1.ID INNER JOIN " & sFSP & "ACT2 A2 ON CA.ACT1=A2.ACT1 AND CA.ACT2=A2.ID INNER JOIN " & sFSP & "ACT3 A3 ON CA.ACT1=A3.ACT1 AND CA.ACT2=A3.ACT2 AND CA.ACT3=A3.ID"
        oADOConnection.Execute sConsulta
        
        sConsulta = "INSERT INTO ACIAS_ACT4 (CIA,ACT1,ACT2,ACT3,ACT4) SELECT CA.CIA,A1.COD,A2.COD,A3.COD,A4.COD FROM " & sFSP & "CIAS_ACT4 CA INNER JOIN " & sFSP & "ACT1 A1 ON CA.ACT1=A1.ID INNER JOIN " & sFSP & "ACT2 A2 ON CA.ACT1=A2.ACT1 AND CA.ACT2=A2.ID INNER JOIN " & sFSP & "ACT3 A3 ON CA.ACT1=A3.ACT1 AND CA.ACT2=A3.ACT2 AND CA.ACT3=A3.ID INNER JOIN " & sFSP & "ACT4 A4 ON CA.ACT1=A4.ACT1 AND CA.ACT2=A4.ACT2 AND CA.ACT3=A4.ACT3 AND CA.ACT4=A4.ID"
        oADOConnection.Execute sConsulta
        
    End If
    
    'Eliminar las actividades
    oADOConnection.Execute "EXEC " & sFSP & "sp_executesql N'DELETE FROM CIAS_ACT5'"
    oADOConnection.Execute "EXEC " & sFSP & "sp_executesql N'DELETE FROM MCIAS_ACT5'"
    oADOConnection.Execute "EXEC " & sFSP & "sp_executesql N'DELETE FROM ACT5'"
    oADOConnection.Execute "EXEC " & sFSP & "sp_executesql N'DELETE FROM CIAS_ACT4'"
    oADOConnection.Execute "EXEC " & sFSP & "sp_executesql N'DELETE FROM MCIAS_ACT4'"
    oADOConnection.Execute "EXEC " & sFSP & "sp_executesql N'DELETE FROM ACT4'"
    oADOConnection.Execute "EXEC " & sFSP & "sp_executesql N'DELETE FROM CIAS_ACT3'"
    oADOConnection.Execute "EXEC " & sFSP & "sp_executesql N'DELETE FROM MCIAS_ACT3'"
    oADOConnection.Execute "EXEC " & sFSP & "sp_executesql N'DELETE FROM ACT3'"
    oADOConnection.Execute "EXEC " & sFSP & "sp_executesql N'DELETE FROM CIAS_ACT2'"
    oADOConnection.Execute "EXEC " & sFSP & "sp_executesql N'DELETE FROM MCIAS_ACT2'"
    oADOConnection.Execute "EXEC " & sFSP & "sp_executesql N'DELETE FROM ACT2'"
    oADOConnection.Execute "EXEC " & sFSP & "sp_executesql N'DELETE FROM CIAS_ACT1'"
    oADOConnection.Execute "EXEC " & sFSP & "sp_executesql N'DELETE FROM MCIAS_ACT1'"
    oADOConnection.Execute "EXEC " & sFSP & "sp_executesql N'DELETE FROM ACT1'"
    
    frmProgreso.lblDetalle = "Ajustar el tama�o de los c�digos de las tablas de actividades"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1 '2
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
    
    '1�-Ajustar el tama�o de los c�digos de las tablas de actividades y poner el mismo que tengan en materiales
    sConsulta = "EXEC " & sFSP & "sp_executesql N'ALTER TABLE [ACT1] ALTER COLUMN [COD] [varchar](" & gLongitudesDeCodigos.giLongCodGMN1 & ") NOT NULL'"
    oADOConnection.Execute sConsulta
    sConsulta = "EXEC " & sFSP & "sp_executesql N'ALTER TABLE [ACT2] ALTER COLUMN [COD] [varchar] (" & gLongitudesDeCodigos.giLongCodGMN2 & ") NOT NULL'"
    oADOConnection.Execute sConsulta
    sConsulta = "EXEC " & sFSP & "sp_executesql N'ALTER TABLE [ACT3] ALTER COLUMN [COD] [varchar] (" & gLongitudesDeCodigos.giLongCodGMN3 & ") NOT NULL'"
    oADOConnection.Execute sConsulta
    sConsulta = "EXEC " & sFSP & "sp_executesql N'ALTER TABLE [ACT4] ALTER COLUMN [COD] [varchar] (" & gLongitudesDeCodigos.giLongCodGMN4 & ") NOT NULL'"
    oADOConnection.Execute sConsulta
    
    frmProgreso.lblDetalle = "Dar de alta en el GS los c�digos de idiomas del portal"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1 '3
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1

    '2�-Dar de alta en el GS los c�digos de idiomas del portal
    sConsulta = "SELECT * FROM " & sFSP & "IDI WHERE COD NOT IN (SELECT COD FROM IDIOMAS)"
    oRS.Open sConsulta, oADOConnection, adOpenForwardOnly, adLockReadOnly
    While Not oRS.eof
        sConsulta = "INSERT INTO IDIOMAS (COD,DEN,APLICACION) VALUES ('" & DblQuote(oRS.Fields("COD").Value) & "','" & DblQuote(oRS.Fields("DEN").Value) & "',0)" & vbCrLf
        oADOConnection.Execute sConsulta
        oRS.MoveNext
    Wend
    oRS.Close

    frmProgreso.lblDetalle = "Modificar las tablas GMN"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1 '4
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1

    '3�-Modificar las tablas GMN para que soporten una denominaci�n en cada idioma de portal
    sConsulta = "SELECT COD FROM IDI"
    oRS.Open "EXEC " & sFSP & "sp_executesql N'" & sConsulta & "'", oADOConnection, adOpenForwardOnly, adLockReadOnly
    While Not oRS.eof
        sConsulta = "ALTER TABLE [dbo].[GMN1] ADD [DEN_" & oRS.Fields("COD").Value & "] [varchar] (1000) NULL" & vbCrLf
        oADOConnection.Execute sConsulta
        sConsulta = "ALTER TABLE [dbo].[GMN2] ADD [DEN_" & oRS.Fields("COD").Value & "] [varchar] (1000) NULL" & vbCrLf
        oADOConnection.Execute sConsulta
        sConsulta = "ALTER TABLE [dbo].[GMN3] ADD [DEN_" & oRS.Fields("COD").Value & "] [varchar] (1000) NULL" & vbCrLf
        oADOConnection.Execute sConsulta
        sConsulta = "ALTER TABLE [dbo].[GMN4] ADD [DEN_" & oRS.Fields("COD").Value & "] [varchar] (1000) NULL" & vbCrLf
        oADOConnection.Execute sConsulta
        sConsulta = "ALTER TABLE [dbo].[LOG_GMN] ALTER COLUMN [DEN] [varchar] (1000) NULL" & vbCrLf
        oADOConnection.Execute sConsulta
        iIndice = iIndice + 1
        ReDim Preserve sIdiomas(iIndice)
        sIdiomas(iIndice) = oRS.Fields("COD").Value
        oRS.MoveNext
    Wend
    oRS.Close
                
    'El campo DEN se pone en todos los idiomas nuevos
    sConsulta = "UPDATE GMN1 SET "
    For i = 1 To UBound(sIdiomas)
        sConsulta = sConsulta & " DEN_" & sIdiomas(i) & " = DEN,"
    Next i
    sConsulta = Left(sConsulta, Len(sConsulta) - 1) & vbCrLf
    
    sConsulta = sConsulta & "UPDATE GMN2 SET"
    For i = 1 To UBound(sIdiomas)
        sConsulta = sConsulta & " DEN_" & sIdiomas(i) & " = DEN,"
    Next i
    sConsulta = Left(sConsulta, Len(sConsulta) - 1) & vbCrLf
    
    sConsulta = sConsulta & "UPDATE GMN3 SET"
    For i = 1 To UBound(sIdiomas)
        sConsulta = sConsulta & " DEN_" & sIdiomas(i) & " = DEN,"
    Next i
    sConsulta = Left(sConsulta, Len(sConsulta) - 1) & vbCrLf
    
    sConsulta = sConsulta & "UPDATE GMN4 SET"
    For i = 1 To UBound(sIdiomas)
        sConsulta = sConsulta & " DEN_" & sIdiomas(i) & " = DEN,"
    Next i
    sConsulta = Left(sConsulta, Len(sConsulta) - 1) & vbCrLf

    sConsulta = sConsulta & "IF EXISTS (SELECT name FROM sysindexes WHERE name = 'IX_GMN1_01')" & vbCrLf
    sConsulta = sConsulta & "   DROP INDEX GMN1.IX_GMN1_01" & vbCrLf
    sConsulta = sConsulta & "   ALTER TABLE dbo.GMN1 DROP COLUMN DEN" & vbCrLf
    sConsulta = sConsulta & "IF EXISTS (SELECT name FROM sysindexes WHERE name = 'IX_GMN2_01')" & vbCrLf
    sConsulta = sConsulta & "   DROP INDEX GMN2.IX_GMN2_01" & vbCrLf
    sConsulta = sConsulta & "IF EXISTS (SELECT name FROM sysindexes WHERE name = 'IX_GMN2_02')" & vbCrLf
    sConsulta = sConsulta & "   DROP INDEX GMN2.IX_GMN2_02" & vbCrLf
    sConsulta = sConsulta & "   ALTER TABLE dbo.GMN2 DROP COLUMN DEN" & vbCrLf
    sConsulta = sConsulta & "IF EXISTS (SELECT name FROM sysindexes WHERE name = 'IX_GMN3_01')" & vbCrLf
    sConsulta = sConsulta & "   DROP INDEX GMN3.IX_GMN3_01" & vbCrLf
    sConsulta = sConsulta & "IF EXISTS (SELECT name FROM sysindexes WHERE name = 'IX_GMN3_02')" & vbCrLf
    sConsulta = sConsulta & "   DROP INDEX GMN3.IX_GMN3_02" & vbCrLf
    sConsulta = sConsulta & "   ALTER TABLE dbo.GMN3 DROP COLUMN DEN" & vbCrLf
    sConsulta = sConsulta & "IF EXISTS (SELECT name FROM sysindexes WHERE name = 'IX_GMN4_01')" & vbCrLf
    sConsulta = sConsulta & "   DROP INDEX GMN4.IX_GMN4_01" & vbCrLf
    sConsulta = sConsulta & "IF EXISTS (SELECT name FROM sysindexes WHERE name = 'IX_GMN4_02')" & vbCrLf
    sConsulta = sConsulta & "   DROP INDEX GMN4.IX_GMN4_02" & vbCrLf
    sConsulta = sConsulta & "   ALTER TABLE dbo.GMN4 DROP COLUMN DEN" & vbCrLf
    oADOConnection.Execute sConsulta
    
    Set oAdores = New ADODB.Recordset
    'Creo unos campos INT para guardar los ids de las actividades al final se borran
    sConsulta = "ALTER TABLE [dbo].[GMN1] ADD [ID] [integer] NULL" & vbCrLf
    oADOConnection.Execute sConsulta
    sConsulta = "ALTER TABLE [dbo].[GMN2] ADD [ID] [integer] NULL" & vbCrLf
    oADOConnection.Execute sConsulta
    sConsulta = "ALTER TABLE [dbo].[GMN3] ADD [ID] [integer] NULL" & vbCrLf
    oADOConnection.Execute sConsulta
    sConsulta = "ALTER TABLE [dbo].[GMN4] ADD [ID] [integer] NULL" & vbCrLf
    oADOConnection.Execute sConsulta
    
    '5�-Dar alta de los materiales en el portal, el mismo valor en las denominaciones de todos los idiomas:
    'Nivel 1
    lId = 1
    sConsulta = "SELECT COD,DEN_SPA FROM GMN1"
    oAdores.Open sConsulta, oADOConnection, adOpenForwardOnly, adLockReadOnly
    While Not oAdores.eof
        sConsulta = "INSERT INTO ACT1(ID,COD,"
        For i = 1 To UBound(sIdiomas)
            sConsulta = sConsulta & " DEN_" & sIdiomas(i) & ","
        Next i
        sConsulta = Left(sConsulta, Len(sConsulta) - 1)
        sConsulta = sConsulta & " ) VALUES (" & lId & ",'" & DblQuote(oAdores.Fields("COD").Value) & "',"
        For i = 1 To UBound(sIdiomas)
            sConsulta = sConsulta & "'" & DblQuote(oAdores.Fields("DEN_SPA").Value) & "',"
        Next i
        sConsulta = Left(sConsulta, Len(sConsulta) - 1) & " )"
        oADOConnection.Execute "EXEC " & sFSP & "sp_executesql N'" & DblQuote(sConsulta) & "'"
        
        sConsulta = "UPDATE GMN1 SET ID= " & lId & " WHERE COD='" & DblQuote(oAdores.Fields("COD").Value) & "'"
        oADOConnection.Execute sConsulta
        
        lId = lId + 1
        oAdores.MoveNext
    Wend
    oAdores.Close

    frmProgreso.lblDetalle = "Actualizar actividades del portal con materiales - Nivel 2"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1 '10
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1

    'Nivel 2
    lId = 1
    sConsulta = "SELECT GMN2.COD,GMN2.DEN_SPA,GMN1.ID GMN1ID,GMN2.GMN1 GMN1 FROM GMN2 INNER JOIN GMN1 ON GMN1.COD=GMN2.GMN1"
    oAdores.Open sConsulta, oADOConnection, adOpenForwardOnly, adLockReadOnly
    While Not oAdores.eof
        sConsulta = "INSERT INTO ACT2(ACT1,ID,COD,"
        For i = 1 To UBound(sIdiomas)
            sConsulta = sConsulta & " DEN_" & sIdiomas(i) & ","
        Next i
        sConsulta = Left(sConsulta, Len(sConsulta) - 1)
        sConsulta = sConsulta & " ) VALUES (" & oAdores.Fields("GMN1ID").Value & "," & lId & ",'" & DblQuote(oAdores.Fields("COD").Value) & "',"
        For i = 1 To UBound(sIdiomas)
            sConsulta = sConsulta & "'" & DblQuote(oAdores.Fields("DEN_SPA").Value) & "',"
        Next i
        sConsulta = Left(sConsulta, Len(sConsulta) - 1) & " )"
        oADOConnection.Execute "EXEC " & sFSP & "sp_executesql N'" & DblQuote(sConsulta) & "'"
            
        sConsulta = "UPDATE GMN2 SET ID= " & lId & " WHERE GMN1='" & DblQuote(oAdores.Fields("GMN1").Value) & "' AND COD='" & DblQuote(oAdores.Fields("COD").Value) & "'"
        oADOConnection.Execute sConsulta
        
        lId = lId + 1
        oAdores.MoveNext
        
    Wend
    oAdores.Close

    frmProgreso.lblDetalle = "Actualizar actividades del portal con materiales - Nivel 3"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1 '11
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1

    'Nivel 3
    lId = 1
    sConsulta = "SELECT GMN3.COD,GMN3.DEN_SPA,GMN1.ID GMN1ID ,GMN2.ID GMN2ID,GMN3.GMN1,GMN3.GMN2 FROM GMN3 "
    sConsulta = sConsulta & " INNER JOIN GMN2 ON GMN2.GMN1=GMN3.GMN1 AND GMN2.COD=GMN3.GMN2 "
    sConsulta = sConsulta & " INNER JOIN GMN1 ON GMN1.COD=GMN3.GMN1 "
    oAdores.Open sConsulta, oADOConnection, adOpenForwardOnly, adLockReadOnly
    While Not oAdores.eof
        sConsulta = "INSERT INTO ACT3(ACT1,ACT2,ID,COD,"
        For i = 1 To UBound(sIdiomas)
            sConsulta = sConsulta & " DEN_" & sIdiomas(i) & ","
        Next i
        sConsulta = Left(sConsulta, Len(sConsulta) - 1)
        sConsulta = sConsulta & " ) VALUES (" & oAdores.Fields("GMN1ID").Value & "," & oAdores.Fields("GMN2ID").Value & "," & lId & ",'" & DblQuote(oAdores.Fields("COD").Value) & "',"
        For i = 1 To UBound(sIdiomas)
            sConsulta = sConsulta & "'" & DblQuote(oAdores.Fields("DEN_SPA").Value) & "',"
        Next i
        sConsulta = Left(sConsulta, Len(sConsulta) - 1) & " )"
        oADOConnection.Execute "EXEC " & sFSP & "sp_executesql N'" & DblQuote(sConsulta) & "'"

        sConsulta = "UPDATE GMN3 SET ID= " & lId & " WHERE GMN1='" & DblQuote(oAdores.Fields("GMN1").Value) & "' AND GMN2='" & DblQuote(oAdores.Fields("GMN2").Value) & "' AND COD='" & DblQuote(oAdores.Fields("COD").Value) & "'"
        oADOConnection.Execute sConsulta

        lId = lId + 1
        oAdores.MoveNext
    Wend
    oAdores.Close

    frmProgreso.lblDetalle = "Actualizar actividades del portal con materiales - Nivel 4"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1 '12
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1

    'Nivel 4
    lId = 1
    sConsulta = "SELECT GMN4.COD,GMN4.DEN_SPA,GMN4.GMN1,GMN4.GMN2,GMN4.GMN3,GMN1.ID GMN1ID,GMN2.ID GMN2ID,GMN3.ID GMN3ID FROM GMN4"
    sConsulta = sConsulta & " INNER JOIN GMN3 ON GMN3.GMN1=GMN4.GMN1 AND GMN3.GMN2=GMN4.GMN2 AND GMN3.COD=GMN4.GMN3"
    sConsulta = sConsulta & " INNER JOIN GMN2 ON GMN2.GMN1=GMN4.GMN1 AND GMN2.COD=GMN4.GMN2 "
    sConsulta = sConsulta & " INNER JOIN GMN1 ON GMN1.COD=GMN4.GMN1 "
    oAdores.Open sConsulta, oADOConnection, adOpenForwardOnly, adLockReadOnly
    While Not oAdores.eof
        
        sConsulta = "INSERT INTO ACT4(ACT1,ACT2,ACT3,ID,COD,"
        For i = 1 To UBound(sIdiomas)
            sConsulta = sConsulta & " DEN_" & sIdiomas(i) & ","
        Next i
        sConsulta = Left(sConsulta, Len(sConsulta) - 1)
        sConsulta = sConsulta & " ) VALUES (" & oAdores.Fields("GMN1ID").Value & "," & oAdores.Fields("GMN2ID").Value & "," & oAdores.Fields("GMN3ID").Value & "," & lId & ",'" & DblQuote(oAdores.Fields("COD").Value) & "',"
        For i = 1 To UBound(sIdiomas)
            sConsulta = sConsulta & "'" & DblQuote(oAdores.Fields("DEN_SPA").Value) & "',"
        Next i
        sConsulta = Left(sConsulta, Len(sConsulta) - 1) & " )"
        oADOConnection.Execute "EXEC " & sFSP & "sp_executesql N'" & DblQuote(sConsulta) & "'"
        
        sConsulta = "UPDATE GMN4 SET ID= " & lId & " WHERE GMN1='" & DblQuote(oAdores.Fields("GMN1").Value) & "' AND GMN2='" & DblQuote(oAdores.Fields("GMN2").Value) & "' AND GMN3='" & DblQuote(oAdores.Fields("GMN3").Value) & "' AND COD='" & DblQuote(oAdores.Fields("COD").Value) & "'"
        oADOConnection.Execute sConsulta
        
        lId = lId + 1
        oAdores.MoveNext
    Wend
    oAdores.Close
    
    
    Dim lNumProves As Long
    Dim lContProves As Long
    Dim bCorrecto As Boolean
    
    sConsulta = "SELECT COUNT(*) FROM PROVE WHERE FSP_COD IS NOT NULL"
    oRS.Open sConsulta, oADOConnection, adOpenForwardOnly, adLockReadOnly
    If Not oRS.eof Then
        lNumProves = oRS(0).Value
    Else
        lNumProves = 0
    End If
    oRS.Close
    lContProves = 0
    
    frmProgreso.lblDetalle = "Actualizar asignaci�n de materiales de proveedores "
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1 '13
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
    'Borro de ACIAS_ACTn los registros de los materiales que no est�n en GS
    If iTipoSinc > 1 Then
        sConsulta = "DELETE FROM ACIAS_ACT1 WHERE ACT1 NOT IN (SELECT COD FROM GMN1)"
        oADOConnection.Execute sConsulta
        sConsulta = "DELETE FROM ACIAS_ACT2 WHERE NOT EXISTS (SELECT * FROM GMN2 G WHERE G.GMN1=ACIAS_ACT2.ACT1 AND G.COD=ACIAS_ACT2.ACT2)"
        oADOConnection.Execute sConsulta
        sConsulta = "DELETE FROM ACIAS_ACT3 WHERE NOT EXISTS (SELECT * FROM GMN3 G WHERE G.GMN1=ACIAS_ACT3.ACT1 AND G.GMN2=ACIAS_ACT3.ACT2 AND G.COD=ACIAS_ACT3.ACT3)"
        oADOConnection.Execute sConsulta
        sConsulta = "DELETE FROM ACIAS_ACT4 WHERE NOT EXISTS (SELECT * FROM GMN4 G WHERE G.GMN1=ACIAS_ACT4.ACT1 AND G.GMN2=ACIAS_ACT4.ACT2 AND G.GMN3=ACIAS_ACT4.ACT3 AND G.COD=ACIAS_ACT4.ACT4)"
        oADOConnection.Execute sConsulta
    End If
    
    ' 6�- Recorrer los proveedores enlazados del GS,borrar su asignaci�n de materiales y asignarles las actividades que tenga
    'registradas en el portal
    sConsulta = "SELECT PROVE.COD,PROVE.FSP_COD,CIAS.ID CIA FROM PROVE INNER JOIN " & sFSP & "CIAS CIAS ON CIAS.COD=PROVE.FSP_COD"
    sConsulta = sConsulta & " WHERE PROVE.FSP_COD IS NOT NULL"
    oRS.Open sConsulta, oADOConnection, adOpenForwardOnly, adLockReadOnly
    While Not oRS.eof
        
        lContProves = lContProves + 1
        frmProgreso.lblDetalle = "Actualizar asignaci�n de materiales de proveedores - " & lContProves & " de " & lNumProves
        frmProgreso.lblDetalle.Refresh
        frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1 '14
        If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
        
        Select Case iTipoSinc
        Case 1
            bCorrecto = True
        Case 2
            bCorrecto = ProveedoresActividadesPortal(oRS("COD").Value, oRS("CIA").Value)
        Case 3
            bCorrecto = ProveedoresTodosMateriales(oRS("COD").Value, oRS("CIA").Value)
        End Select
        If Not bCorrecto Then
    
            oADOConnection.Close
            Set oADOConnection = Nothing
            
            CodigoDeActualizacion2_16_05_00A2_16_05_01 = False
            Exit Function
        End If
        bCorrecto = ProveedoresMaterialGS(oRS("COD").Value, oRS("CIA").Value)
        If Not bCorrecto Then
            oADOConnection.Close
            Set oADOConnection = Nothing
            
            CodigoDeActualizacion2_16_05_00A2_16_05_01 = False
            Exit Function
        End If
        oRS.MoveNext
    Wend

    oRS.Close
    Set oAdores = Nothing
    'Elimino los campos ID de las tablas GMN
    sConsulta = "   ALTER TABLE dbo.GMN1 DROP COLUMN ID" & vbCrLf
    oADOConnection.Execute sConsulta
    sConsulta = "   ALTER TABLE dbo.GMN2 DROP COLUMN ID" & vbCrLf
    oADOConnection.Execute sConsulta
    sConsulta = "   ALTER TABLE dbo.GMN3 DROP COLUMN ID" & vbCrLf
    oADOConnection.Execute sConsulta
    sConsulta = "   ALTER TABLE dbo.GMN4 DROP COLUMN ID" & vbCrLf
    oADOConnection.Execute sConsulta
        
    If iTipoSinc > 1 Then
        oADOConnection.Execute "DROP TABLE ACIAS_ACT1"
        oADOConnection.Execute "DROP TABLE ACIAS_ACT2"
        oADOConnection.Execute "DROP TABLE ACIAS_ACT3"
        oADOConnection.Execute "DROP TABLE ACIAS_ACT4"
    End If
    
    sConsulta = "UPDATE PARGEN_INTERNO SET SINC_MAT=1"
    oADOConnection.Execute sConsulta
    
Else

    frmProgreso.lblDetalle = "Modificar las tablas GMN"
    frmProgreso.lblDetalle.Refresh
    frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1 '4
    If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1

    '2�-Modificar las tablas GMN para que soporten una denominaci�n en cada idioma
    sConsulta = "SELECT COD FROM IDIOMAS"
    oAdores.Open sConsulta, oADOConnection, adOpenForwardOnly, adLockReadOnly
    While Not oRS.eof
        sConsulta = "ALTER TABLE [dbo].[GMN1] ADD [DEN_" & oRS.Fields("COD").Value & "] [varchar] (100) NULL" & vbCrLf
        oADOConnection.Execute sConsulta
        sConsulta = "ALTER TABLE [dbo].[GMN2] ADD [DEN_" & oRS.Fields("COD").Value & "] [varchar] (100) NULL" & vbCrLf
        oADOConnection.Execute sConsulta
        sConsulta = "ALTER TABLE [dbo].[GMN3] ADD [DEN_" & oRS.Fields("COD").Value & "] [varchar] (100) NULL" & vbCrLf
        oADOConnection.Execute sConsulta
        sConsulta = "ALTER TABLE [dbo].[GMN4] ADD [DEN_" & oRS.Fields("COD").Value & "] [varchar] (100) NULL" & vbCrLf
        oADOConnection.Execute sConsulta
        iIndice = iIndice + 1
        ReDim Preserve sIdiomas(iIndice)
        sIdiomas(iIndice) = oRS.Fields("COD").Value
        oRS.MoveNext
    Wend
    oRS.Close

    'El campo DEN se pone en todos los idiomas nuevos
    sConsulta = "UPDATE GMN1 SET DEN_SPA=DEN"
    oADOConnection.Execute sConsulta
    
    sConsulta = "UPDATE GMN2 SET DEN_SPA=DEN"
    oADOConnection.Execute sConsulta
    
    sConsulta = "UPDATE GMN3 SET DEN_SPA=DEN"
    oADOConnection.Execute sConsulta
    
    sConsulta = "UPDATE GMN4 SET DEN_SPA=DEN"
    oADOConnection.Execute sConsulta

    sConsulta = "IF EXISTS (SELECT name FROM sysindexes WHERE name = 'IX_GMN1_01')" & vbCrLf
    sConsulta = sConsulta & "   DROP INDEX GMN1.IX_GMN1_01" & vbCrLf
    sConsulta = sConsulta & "   ALTER TABLE dbo.GMN1 DROP COLUMN DEN" & vbCrLf
    sConsulta = sConsulta & "IF EXISTS (SELECT name FROM sysindexes WHERE name = 'IX_GMN2_01')" & vbCrLf
    sConsulta = sConsulta & "   DROP INDEX GMN2.IX_GMN2_01" & vbCrLf
    sConsulta = sConsulta & "IF EXISTS (SELECT name FROM sysindexes WHERE name = 'IX_GMN2_02')" & vbCrLf
    sConsulta = sConsulta & "   DROP INDEX GMN2.IX_GMN2_02" & vbCrLf
    sConsulta = sConsulta & "   ALTER TABLE dbo.GMN2 DROP COLUMN DEN" & vbCrLf
    sConsulta = sConsulta & "IF EXISTS (SELECT name FROM sysindexes WHERE name = 'IX_GMN3_01')" & vbCrLf
    sConsulta = sConsulta & "   DROP INDEX GMN3.IX_GMN3_01" & vbCrLf
    sConsulta = sConsulta & "IF EXISTS (SELECT name FROM sysindexes WHERE name = 'IX_GMN3_02')" & vbCrLf
    sConsulta = sConsulta & "   DROP INDEX GMN3.IX_GMN3_02" & vbCrLf
    sConsulta = sConsulta & "   ALTER TABLE dbo.GMN3 DROP COLUMN DEN" & vbCrLf
    sConsulta = sConsulta & "IF EXISTS (SELECT name FROM sysindexes WHERE name = 'IX_GMN4_01')" & vbCrLf
    sConsulta = sConsulta & "   DROP INDEX GMN4.IX_GMN4_01" & vbCrLf
    sConsulta = sConsulta & "IF EXISTS (SELECT name FROM sysindexes WHERE name = 'IX_GMN4_02')" & vbCrLf
    sConsulta = sConsulta & "   DROP INDEX GMN4.IX_GMN4_02" & vbCrLf
    sConsulta = sConsulta & "   ALTER TABLE dbo.GMN4 DROP COLUMN DEN" & vbCrLf
    oADOConnection.Execute sConsulta
    
    
End If
Set oRS = Nothing

frmProgreso.lblDetalle = "Finalizando la sincronizaci�n de materiales y actividades"
frmProgreso.lblDetalle.Refresh
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1 '17
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1


sConsulta = "UPDATE VERSION SET NUM ='2.16.05.01'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
oADOConnection.Execute sConsulta

sConsulta = "COMMIT TRAN"
oADOConnection.Execute sConsulta
bTransaccionEnCurso = False

oADOConnection.Close
Set oADOConnection = Nothing

frmProgreso.ProgressBar1.Max = lProgresBar

CodigoDeActualizacion2_16_05_00A2_16_05_01 = True
Exit Function
    
Error:

    
    MsgBox oADOConnection.Errors(0).Description, vbCritical + vbOKOnly
   'Resume Next
    If NumTransaccionesAbiertasADO(oADOConnection) Then
        oADOConnection.Execute "ROLLBACK TRAN"
        oADOConnection.Execute "SET XACT_ABORT OFF"
    End If
    
    If Not oADOConnection Is Nothing Then
        oADOConnection.Close
        Set oADOConnection = Nothing
    End If
    CodigoDeActualizacion2_16_05_00A2_16_05_01 = False
End Function


Private Sub V_2_16_5_Tablas_0()
    Dim sConsulta As String
    
    'Tabla PARGEN_INTERNO
    sConsulta = "  ALTER TABLE [dbo].[PARGEN_INTERNO] ADD" & vbCrLf
    sConsulta = sConsulta & "     [SINC_MAT] [tinyint] NOT NULL CONSTRAINT [DF_PARGEN_INTERNO_SINC_MAT] DEFAULT 0," & vbCrLf
    sConsulta = sConsulta & "     [CONSERVAR_NUMPEDIDO] [tinyint] NOT NULL CONSTRAINT [DF_PARGEN_INTERNO_CONSERVAR_NUMPEDIDO] DEFAULT 0"
    ExecuteSQL gRDOCon, sConsulta
    
    'Tabla PARGEN_INTERNO
    sConsulta = "  ALTER TABLE [dbo].[IDIOMAS] ADD [APLICACION] [tinyint] NOT NULL CONSTRAINT [DF_IDIOMAS_APLICACION]  DEFAULT 1"
    ExecuteSQL gRDOCon, sConsulta

End Sub

Private Sub V_2_16_5_Triggers_0()
Dim sConsulta As String

sConsulta = ""
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CREATE TRIGGER [PARGEN_INTERNO_TG_UPD] ON [dbo].[PARGEN_INTERNO] " & vbCrLf
sConsulta = sConsulta & "FOR UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @SRV AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @BD AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @INSTWEB AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @UNA_COMP INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @SINC_MAT INT" & vbCrLf
sConsulta = sConsulta & "IF UPDATE(SINC_MAT)" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_Upd CURSOR FOR SELECT SINC_MAT FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_Upd" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_Upd INTO @SINC_MAT" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @SINC_MAT=1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @INSTWEB=(SELECT INSTWEB FROM PARGEN_INTERNO)" & vbCrLf
sConsulta = sConsulta & "IF @INSTWEB=2" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SELECT @SRV=FSP_SRV,@BD=FSP_BD FROM PARGEN_PORT WHERE ID=1" & vbCrLf
sConsulta = sConsulta & "SET @SQL= 'SELECT @UNA_COMP=UNA_COMPRADORA FROM ' + @SRV + '.' + @BD  + '.dbo.PARGEN_INTERNO'" & vbCrLf
sConsulta = sConsulta & "EXECUTE sp_executesql @SQL, N'@UNA_COMP INT OUTPUT ', @UNA_COMP =@UNA_COMP OUTPUT" & vbCrLf
sConsulta = sConsulta & "IF @UNA_COMP=1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @SQL= 'SET @UNA_COMP=(SELECT count(*) FROM ' + @SRV + '.' + @BD  + '.dbo.CIAS WHERE FCEST=3)'" & vbCrLf
sConsulta = sConsulta & "EXECUTE sp_executesql @SQL, N'@UNA_COMP INT OUTPUT ', @UNA_COMP =@UNA_COMP OUTPUT" & vbCrLf
sConsulta = sConsulta & "IF @UNA_COMP>1" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "exec sp_addmessage 50006 , 16, 'S�lo se podr� activar la sincronizaci�n de materiales/actividades en el caso de portales con una sola empresa!'" & vbCrLf
sConsulta = sConsulta & "RAISERROR (50006,16,1)" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "exec sp_addmessage 50006 , 16, 'S�lo se podr� activar la sincronizaci�n de materiales/actividades en el caso de portales con una sola empresa!'" & vbCrLf
sConsulta = sConsulta & "RAISERROR (50006,16,1)" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_Upd INTO @SINC_MAT" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "Close curTG_Upd" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_Upd" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


End Sub



Public Function CodigoDeActualizacion2_16_05_01A2_16_05_02() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim iRespuesta As Integer

On Error GoTo Error
sConsulta = "BEGIN TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = True
ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

frmProgreso.lblDetalle = "A�adimos campos"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
V_2_16_5_Tablas_2


frmProgreso.lblDetalle = "Actualizamos triggers"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
V_2_16_5_Triggers_2

frmProgreso.lblDetalle = "Actualizamos stored procedures"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
V_2_16_5_Storeds_2

frmProgreso.lblDetalle = "Actualizamos mas stored procedures"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
V_2_16_5_Storeds_2_1


sConsulta = "UPDATE VERSION SET NUM ='2.16.05.02'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "COMMIT TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = False

CodigoDeActualizacion2_16_05_01A2_16_05_02 = True
Exit Function
    
Error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_16_05_01A2_16_05_02 = False

End Function

Private Sub V_2_16_5_Tablas_2()
    Dim sConsulta As String
    
    'Tabla PROCE_DEF
    sConsulta = "  ALTER TABLE [dbo].[PROCE_DEF] ADD [UNSOLOPEDIDO] [tinyint] NOT NULL CONSTRAINT [DF_PROC_DEF_UNSOLOPEDIDO] DEFAULT 0"
    ExecuteSQL gRDOCon, sConsulta
    
    'Tabla PARGEN_GEST
    sConsulta = "  ALTER TABLE [dbo].[PARGEN_GEST] ADD [UNSOLOPEDIDO] [tinyint] NOT NULL CONSTRAINT [DF_PARGEN_GEST_UNSOLOPEDIDO] DEFAULT 0," & vbCrLf
    sConsulta = sConsulta & "   [ADJNOTIF] [tinyint] NOT NULL CONSTRAINT [DF_PARGEN_GEST_ADJNOTIF] DEFAULT 0"
    ExecuteSQL gRDOCon, sConsulta

    'Tabla PARINS_GEST
    sConsulta = "  ALTER TABLE [dbo].[PARINS_GEST] ADD [ADJNOTIF] [tinyint] NOT NULL CONSTRAINT [DF_PARINS_GEST_ADJNOTIF] DEFAULT 0"
    ExecuteSQL gRDOCon, sConsulta

    'Tabla PARINS_PROCE
    sConsulta = "  ALTER TABLE [dbo].[PARINS_PROCE] ADD [UNSOLOPEDIDO] [tinyint] NOT NULL CONSTRAINT [DF_PARINS_PROCE_UNSOLOPEDIDO] DEFAULT 0"
    ExecuteSQL gRDOCon, sConsulta

    'Tabla PROCE_PEDIDO
    sConsulta = "CREATE TABLE [dbo].[PROCE_PEDIDO] (" & vbCrLf
    sConsulta = sConsulta & "   [ANYO] [smallint] NOT NULL ," & vbCrLf
    sConsulta = sConsulta & "   [GMN1] [varchar] (" & gLongitudesDeCodigos.giLongCodGMN1 & ") NOT NULL ," & vbCrLf
    sConsulta = sConsulta & "   [PROCE] [smallint] NOT NULL ," & vbCrLf
    sConsulta = sConsulta & "   [PROVE] [varchar] (" & gLongitudesDeCodigos.giLongCodPROVE & ") NOT NULL ," & vbCrLf
    sConsulta = sConsulta & "   [PED_ANYO] [int] NOT NULL ," & vbCrLf
    sConsulta = sConsulta & "   [PED_CESTA] [int] NOT NULL ," & vbCrLf
    sConsulta = sConsulta & "   [PED_ORDEN] [int] NOT NULL ," & vbCrLf
    sConsulta = sConsulta & "   [FECACT] [datetime] NULL " & vbCrLf
    sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
 
    sConsulta = "ALTER TABLE [dbo].[PROCE_PEDIDO] WITH NOCHECK ADD " & vbCrLf
    sConsulta = sConsulta & "   CONSTRAINT [PK_PROCE_PEDIDO] PRIMARY KEY  CLUSTERED " & vbCrLf
    sConsulta = sConsulta & "   (" & vbCrLf
    sConsulta = sConsulta & "       [ANYO]," & vbCrLf
    sConsulta = sConsulta & "       [GMN1]," & vbCrLf
    sConsulta = sConsulta & "       [PROCE]," & vbCrLf
    sConsulta = sConsulta & "       [PROVE]" & vbCrLf
    sConsulta = sConsulta & "   )  ON [PRIMARY] " & vbCrLf
    ExecuteSQL gRDOCon, sConsulta
 
     'Tabla PLANTILLA_PROCE_ESP
    sConsulta = "  ALTER TABLE [dbo].[PLANTILLA_PROCE_ESP] ADD [RUTA] [varchar] (2000) NULL ," & vbCrLf
    sConsulta = sConsulta & " [DATASIZE] [int] NULL "
    ExecuteSQL gRDOCon, sConsulta


End Sub


Private Sub V_2_16_5_Triggers_2()
Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CONF_VISTAS_GRUPO_TG_INSUPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[CONF_VISTAS_GRUPO_TG_INSUPD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TRIGGER CONF_VISTAS_GRUPO_TG_INSUPD ON dbo.CONF_VISTAS_GRUPO" & vbCrLf
sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO SMALLINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN1 VARCHAR(5)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCE SMALLINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @GRUPO VARCHAR(10)" & vbCrLf
sConsulta = sConsulta & "DECLARE @USU VARCHAR(20)" & vbCrLf
sConsulta = sConsulta & "DECLARE @VISTA INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @DEFECTO INT" & vbCrLf
sConsulta = sConsulta & "IF UPDATE(DEFECTO)" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "     DECLARE curTG_CONF_VISTAS_GRUPO_Ins CURSOR FOR SELECT ANYO,GMN1,PROCE,GRUPO,USU,VISTA,DEFECTO FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "     OPEN curTG_CONF_VISTAS_GRUPO_Ins" & vbCrLf
sConsulta = sConsulta & "     FETCH NEXT FROM curTG_CONF_VISTAS_GRUPO_Ins INTO @ANYO,@GMN1,@PROCE,@GRUPO,@USU,@VISTA,@DEFECTO" & vbCrLf
sConsulta = sConsulta & "     WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "          BEGIN" & vbCrLf
sConsulta = sConsulta & "             IF @DEFECTO=1" & vbCrLf
sConsulta = sConsulta & "                  UPDATE CONF_VISTAS_GRUPO SET DEFECTO=0 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND GRUPO=@GRUPO AND USU=@USU AND VISTA<>@VISTA" & vbCrLf
sConsulta = sConsulta & "             FETCH NEXT FROM curTG_CONF_VISTAS_GRUPO_Ins INTO @ANYO,@GMN1,@PROCE,@GRUPO,@USU,@VISTA,@DEFECTO" & vbCrLf
sConsulta = sConsulta & "          END" & vbCrLf
sConsulta = sConsulta & "     Close curTG_CONF_VISTAS_GRUPO_Ins" & vbCrLf
sConsulta = sConsulta & "     DEALLOCATE curTG_CONF_VISTAS_GRUPO_Ins" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CONF_VISTAS_PROCE_TG_INSUPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[CONF_VISTAS_PROCE_TG_INSUPD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TRIGGER CONF_VISTAS_PROCE_TG_INSUPD ON dbo.CONF_VISTAS_PROCE" & vbCrLf
sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO SMALLINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN1 VARCHAR(5)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCE SMALLINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @USU VARCHAR(20)" & vbCrLf
sConsulta = sConsulta & "DECLARE @VISTA INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @DEFECTO INT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF UPDATE(DEFECTO)" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "     DECLARE curTG_CONF_VISTAS_PROCE_Ins CURSOR FOR SELECT ANYO,GMN1,PROCE,USU,VISTA,DEFECTO FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "     OPEN curTG_CONF_VISTAS_PROCE_Ins" & vbCrLf
sConsulta = sConsulta & "     FETCH NEXT FROM curTG_CONF_VISTAS_PROCE_Ins INTO @ANYO,@GMN1,@PROCE,@USU,@VISTA,@DEFECTO" & vbCrLf
sConsulta = sConsulta & "     WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "          BEGIN" & vbCrLf
sConsulta = sConsulta & "               IF @DEFECTO=1" & vbCrLf
sConsulta = sConsulta & "        UPDATE CONF_VISTAS_PROCE SET DEFECTO=0 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE AND USU=@USU AND VISTA<>@VISTA" & vbCrLf
sConsulta = sConsulta & "    FETCH NEXT FROM curTG_CONF_VISTAS_PROCE_Ins INTO @ANYO,@GMN1,@PROCE,@USU,@VISTA,@DEFECTO" & vbCrLf
sConsulta = sConsulta & "          END" & vbCrLf
sConsulta = sConsulta & "     Close curTG_CONF_VISTAS_PROCE_Ins" & vbCrLf
sConsulta = sConsulta & "     DEALLOCATE curTG_CONF_VISTAS_PROCE_Ins" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & ""
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CONF_VISTAS_ALL_TG_INSUPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[CONF_VISTAS_ALL_TG_INSUPD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "CREATE TRIGGER CONF_VISTAS_ALL_TG_INSUPD ON dbo.CONF_VISTAS_ALL" & vbCrLf
sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO SMALLINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN1 VARCHAR(5)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCE SMALLINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @USU VARCHAR(20)" & vbCrLf
sConsulta = sConsulta & "DECLARE @VISTA INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @DEFECTO INT" & vbCrLf
sConsulta = sConsulta & "IF UPDATE(DEFECTO)" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "     DECLARE curTG_CONF_VISTAS_ALL_Ins CURSOR FOR SELECT ANYO,GMN1,PROCE,USU,VISTA,DEFECTO FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "     OPEN curTG_CONF_VISTAS_ALL_Ins" & vbCrLf
sConsulta = sConsulta & "     FETCH NEXT FROM curTG_CONF_VISTAS_ALL_Ins INTO @ANYO,@GMN1,@PROCE,@USU,@VISTA,@DEFECTO" & vbCrLf
sConsulta = sConsulta & "     WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "          BEGIN" & vbCrLf
sConsulta = sConsulta & "          IF @DEFECTO=1" & vbCrLf
sConsulta = sConsulta & "               UPDATE CONF_VISTAS_ALL SET DEFECTO=0 WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE   AND USU=@USU AND VISTA<>@VISTA" & vbCrLf
sConsulta = sConsulta & "          FETCH NEXT FROM curTG_CONF_VISTAS_ALL_Ins INTO @ANYO,@GMN1,@PROCE,@USU,@VISTA,@DEFECTO" & vbCrLf
sConsulta = sConsulta & "          END" & vbCrLf
sConsulta = sConsulta & "     Close curTG_CONF_VISTAS_ALL_Ins" & vbCrLf
sConsulta = sConsulta & "     DEALLOCATE curTG_CONF_VISTAS_ALL_Ins" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & ""
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[LINEAS_PEDIDO_TG_UPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[LINEAS_PEDIDO_TG_UPD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TRIGGER LINEAS_PEDIDO_TG_UPD ON dbo.LINEAS_PEDIDO" & vbCrLf
sConsulta = sConsulta & "FOR UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ORDEN AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ORDEN_EST AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @FECENTREGAPROVE AS DATETIME" & vbCrLf
sConsulta = sConsulta & "DECLARE @ESTADO AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @EXTERNO AS BIT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ESTNEW AS SMALLINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ESTOLD AS SMALLINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @APROBADOR AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PEDIDO AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @SUMA AS FLOAT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_LINEAS_PEDIDO_Upd CURSOR LOCAL FOR SELECT I.ID,I.ORDEN,I.FECENTREGAPROVE,I.EST,I.APROBADOR,I.PEDIDO,D.EST FROM INSERTED I INNER JOIN DELETED D ON I.ID=D.ID" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_LINEAS_PEDIDO_Upd" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_LINEAS_PEDIDO_Upd INTO @ID,@ORDEN,@FECENTREGAPROVE,@ESTNEW,@APROBADOR,@PEDIDO,@ESTOLD" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "  IF UPDATE (FECENTREGAPROVE)" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "     SELECT @ORDEN_EST=ORDEN_EST FROM ORDEN_ENTREGA WHERE ID = @ORDEN" & vbCrLf
sConsulta = sConsulta & "     SET @ESTADO = (SELECT TOP 1 ESTADO FROM LOG_GRAL INNER JOIN LOG_ORDEN_ENTREGA ON LOG_GRAL.ID_TABLA = LOG_ORDEN_ENTREGA.ID WHERE LOG_ORDEN_ENTREGA.ID_ORDEN_EST = @ORDEN_EST AND (TABLA =11 OR TABLA=13) AND LOG_ORDEN_ENTREGA.ORIGEN<>3)" & vbCrLf
sConsulta = sConsulta & "      IF @ESTADO = 0" & vbCrLf
sConsulta = sConsulta & "      BEGIN" & vbCrLf
sConsulta = sConsulta & "        UPDATE LOG_LINEAS_PEDIDO SET FECENTREGAPROVE = @FECENTREGAPROVE WHERE ID_ORDEN_EST = @ORDEN_EST AND ID_LINEAS_PEDIDO = @ID" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "-- FIN INTEGRACION" & vbCrLf
sConsulta = sConsulta & "  IF UPDATE(EST)" & vbCrLf
sConsulta = sConsulta & "     IF @ESTNEW <> @ESTOLD" & vbCrLf
sConsulta = sConsulta & "      IF  (@ESTNEW=1 AND @ESTOLD<>21) OR @ESTNEW=200" & vbCrLf
sConsulta = sConsulta & "                 INSERT INTO LINEAS_EST (PEDIDO,ORDEN,LINEA,EST,FECHA,PER) VALUES (@PEDIDO,@ORDEN,@ID,@ESTNEW,GETDATE(),@APROBADOR)" & vbCrLf
sConsulta = sConsulta & "--Actulizaci�n de precio o cantidad" & vbCrLf
sConsulta = sConsulta & "  IF UPDATE(PREC_UP)  OR UPDATE(CANT_PED)" & vbCrLf
sConsulta = sConsulta & "         begin" & vbCrLf
sConsulta = sConsulta & "           SELECT @SUMA= SUM(L.CANT_PED*L.PREC_UP) FROM LINEAS_PEDIDO L  WHERE L.ORDEN=@ORDEN" & vbCrLf
sConsulta = sConsulta & "           UPDATE ORDEN_ENTREGA SET IMPORTE = @SUMA WHERE ID=@ORDEN" & vbCrLf
sConsulta = sConsulta & "         end" & vbCrLf
sConsulta = sConsulta & "UPDATE LINEAS_PEDIDO SET FECACT=GETDATE() WHERE ID=@ID" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_LINEAS_PEDIDO_Upd INTO @ID,@ORDEN,@FECENTREGAPROVE,@ESTNEW,@APROBADOR,@PEDIDO,@ESTOLD" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_LINEAS_PEDIDO_Upd" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_LINEAS_PEDIDO_Upd" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Private Sub V_2_16_5_Storeds_2()
Dim sConsulta As String

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_CIERRE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_CIERRE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_CIERRE(@ANYO SMALLINT,@GMN1 VARCHAR(20),@PROCE INTEGER, @RES INTEGER OUTPUT) AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_PROCE @ANYO,@GMN1,@PROCE,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_PROVE @ANYO,@GMN1,@PROCE,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_PROVE @ANYO,@GMN1,@PROCE,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_PRESCON3 @ANYO,@GMN1,@PROCE,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_PRESCON4 @ANYO,@GMN1,@PROCE,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_MES @ANYO,@GMN1,@PROCE,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_MES_PARCON1 @ANYO,@GMN1,@PROCE,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_MES_PARCON2 @ANYO,@GMN1,@PROCE,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_MES_PARCON3 @ANYO,@GMN1,@PROCE,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_MES_PARCON4 @ANYO,@GMN1,@PROCE,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_MES_PRESCON3_1 @ANYO,@GMN1,@PROCE,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_MES_PRESCON3_2 @ANYO,@GMN1,@PROCE,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_MES_PRESCON3_3 @ANYO,@GMN1,@PROCE,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_MES_PRESCON3_4 @ANYO,@GMN1,@PROCE,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_MES_PRESCON4_1 @ANYO,@GMN1,@PROCE,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_MES_PRESCON4_2 @ANYO,@GMN1,@PROCE,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_MES_PRESCON4_3 @ANYO,@GMN1,@PROCE,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_MES_PRESCON4_4 @ANYO,@GMN1,@PROCE,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_MES_PROY1 @ANYO,@GMN1,@PROCE,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_MES_PROY2 @ANYO,@GMN1,@PROCE,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_MES_PROY3 @ANYO,@GMN1,@PROCE,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_MES_PROY4 @ANYO,@GMN1,@PROCE,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_MES_UON1 @ANYO,@GMN1,@PROCE,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_MES_UON2 @ANYO,@GMN1,@PROCE,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_MES_UON3 @ANYO,@GMN1,@PROCE,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES IS  NULL SET @RES=0" & vbCrLf
sConsulta = sConsulta & "Return @RES" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_CIERRE_PARC]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_CIERRE_PARC]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_CIERRE_PARC(@ANYO SMALLINT,@GMN1 VARCHAR(20),@PROCE INTEGER, @ITEM INTEGER,@RES INTEGER OUTPUT) AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_PROVE_PARC @ANYO,@GMN1,@PROCE,@ITEM,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_PRESCON3_PARC @ANYO,@GMN1,@PROCE,@ITEM,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_PRESCON4_PARC @ANYO,@GMN1,@PROCE,@ITEM,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_MES_PARC @ANYO,@GMN1,@PROCE,@ITEM,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_MES_UON1_PARC @ANYO,@GMN1,@PROCE,@ITEM,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_MES_UON2_PARC @ANYO,@GMN1,@PROCE,@ITEM,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_MES_UON3_PARC @ANYO,@GMN1,@PROCE,@ITEM,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_MES_PARCON1_PARC @ANYO,@GMN1,@PROCE,@ITEM,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_MES_PARCON2_PARC @ANYO,@GMN1,@PROCE,@ITEM,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_MES_PARCON3_PARC @ANYO,@GMN1,@PROCE,@ITEM,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_MES_PARCON4_PARC @ANYO,@GMN1,@PROCE,@ITEM,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_MES_PROY1_PARC @ANYO,@GMN1,@PROCE,@ITEM,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_MES_PROY2_PARC @ANYO,@GMN1,@PROCE,@ITEM,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_MES_PROY3_PARC @ANYO,@GMN1,@PROCE,@ITEM,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_MES_PROY4_PARC @ANYO,@GMN1,@PROCE,@ITEM,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_MES_PRESCON3_1_PARC @ANYO,@GMN1,@PROCE,@ITEM,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_MES_PRESCON3_2_PARC @ANYO,@GMN1,@PROCE,@ITEM,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_MES_PRESCON3_3_PARC @ANYO,@GMN1,@PROCE,@ITEM,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_MES_PRESCON3_4_PARC @ANYO,@GMN1,@PROCE,@ITEM,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_MES_PRESCON4_1_PARC @ANYO,@GMN1,@PROCE,@ITEM,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_MES_PRESCON4_2_PARC @ANYO,@GMN1,@PROCE,@ITEM,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_MES_PRESCON4_3_PARC @ANYO,@GMN1,@PROCE,@ITEM,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES<>0 AND @RES IS NOT NULL RETURN @RES" & vbCrLf
sConsulta = sConsulta & "EXECUTE SP_AR_ITEM_MES_PRESCON4_4_PARC @ANYO,@GMN1,@PROCE,@ITEM,@RES output" & vbCrLf
sConsulta = sConsulta & "IF @RES IS NULL SET @RES=0" & vbCrLf
sConsulta = sConsulta & "Return @RES" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
'YA NO SE USA
sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_AR_ART4_ADJ]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_AR_ART4_ADJ]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
'YA NO SE USA
sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[MAT_PROVE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[MAT_PROVE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[MAT_RESTR]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[MAT_RESTR]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE MAT_RESTR  (@EQP CHAR(50),@COM CHAR(50),@ORDEN CHAR(3),@USU VARCHAR(50)) AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @SDEN AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDI AS VARCHAR(50) " & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL nvarchar(4000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF (SELECT SINC_MAT FROM PARGEN_INTERNO)=1" & vbCrLf
sConsulta = sConsulta & "  begin" & vbCrLf
sConsulta = sConsulta & "       SET @IDI=(SELECT IDIOMAPORT FROM PARINS_GEST WHERE USU=@USU)" & vbCrLf
sConsulta = sConsulta & "       SET @SDEN='DEN_' + @IDI" & vbCrLf
sConsulta = sConsulta & "  end" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  begin" & vbCrLf
sConsulta = sConsulta & "       SET @SDEN='DEN'" & vbCrLf
sConsulta = sConsulta & "  end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @EQP <>'' AND @COM <>'' AND @ORDEN = 'COD' " & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "set @SQL=N'" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT COM_GMN1.GMN1 COD1,GMN1.' + @SDEN + ' DEN1, NULL COD2,NULL DEN2,NULL COD3,NULL DEN3,NULL COD4,NULL DEN4" & vbCrLf
sConsulta = sConsulta & "FROM COM_GMN1 INNER JOIN GMN1 ON COM_GMN1.GMN1 = GMN1.COD WHERE COM_GMN1.EQP = ''' + @EQP  + '''AND COM_GMN1.COM = ''' + @COM + '''" & vbCrLf
sConsulta = sConsulta & "UNION" & vbCrLf
sConsulta = sConsulta & "(SELECT DISTINCT COM_GMN2.GMN1,GMN1.' + @SDEN + ',COM_GMN2.GMN2,GMN2.' + @SDEN + ', NULL,NULL,NULL,NULL FROM COM_GMN2 INNER JOIN GMN2 ON COM_GMN2.GMN1 = GMN2.GMN1 AND COM_GMN2.GMN2  = GMN2.COD" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN GMN1 ON GMN1.COD = COM_GMN2.GMN1 WHERE COM_GMN2.EQP = ''' + @EQP + ''' AND COM_GMN2.COM = ''' + @COM + ''')" & vbCrLf
sConsulta = sConsulta & "UNION" & vbCrLf
sConsulta = sConsulta & "(SELECT DISTINCT COM_GMN3.GMN1,GMN1.' + @SDEN + ',COM_GMN3.GMN2,GMN2.' + @SDEN + ',COM_GMN3.GMN3,GMN3.' + @SDEN + ',NULL,NULL FROM COM_GMN3 INNER JOIN GMN3 ON COM_GMN3.GMN1 = GMN3.GMN1 AND COM_GMN3.GMN2 = GMN3.GMN2 AND COM_GMN3.GMN3 = GMN3.COD" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN GMN1 ON GMN1.COD = COM_GMN3.GMN1" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN GMN2 ON GMN2.COD = COM_GMN3.GMN2  AND GMN2.GMN1= COM_GMN3.GMN1" & vbCrLf
sConsulta = sConsulta & "     WHERE COM_GMN3.EQP = ''' + @EQP + ''' AND COM_GMN3.COM = ''' + @COM + ''')" & vbCrLf
sConsulta = sConsulta & "UNION" & vbCrLf
sConsulta = sConsulta & "(SELECT DISTINCT COM_GMN4.GMN1,GMN1.' + @SDEN + ',COM_GMN4.GMN2,GMN2.' + @SDEN + ',COM_GMN4.GMN3,GMN3.' + @SDEN + ',COM_GMN4.GMN4,GMN4.' + @SDEN + ' FROM COM_GMN4 INNER JOIN GMN4 ON COM_GMN4.GMN1 = GMN4.GMN1 AND COM_GMN4.GMN2 = GMN4.GMN2 AND COM_GMN4.GMN3 = GMN4.GMN3 AND COM_GMN4.GMN4 = GMN4.COD" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN GMN1 ON GMN1.COD = COM_GMN4.GMN1" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN GMN2 ON GMN2.COD = COM_GMN4.GMN2 AND GMN2.GMN1= COM_GMN4.GMN1" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN GMN3 ON GMN3.COD = COM_GMN4.GMN3 AND GMN3.GMN1= COM_GMN4.GMN1 AND GMN3.GMN2 = COM_GMN4.GMN2" & vbCrLf
sConsulta = sConsulta & " WHERE COM_GMN4.EQP = ''' + @EQP + ''' AND COM_GMN4.COM = ''' + @COM + ''') " & vbCrLf
sConsulta = sConsulta & "ORDER BY COD1,COD2,COD3,COD4" & vbCrLf
sConsulta = sConsulta & "'" & vbCrLf
sConsulta = sConsulta & "END " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @EQP <>'' AND @COM <>'' AND @ORDEN = 'DEN'" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "set @SQL=N'" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT COM_GMN1.GMN1 COD1,GMN1.' + @SDEN + ' DEN1, NULL COD2,NULL DEN2,NULL COD3,NULL DEN3,NULL COD4,NULL DEN4" & vbCrLf
sConsulta = sConsulta & "FROM COM_GMN1 INNER JOIN GMN1 ON COM_GMN1.GMN1 = GMN1.COD WHERE COM_GMN1.EQP = ''' + @EQP  + ''' AND COM_GMN1.COM = ''' + @COM + '''" & vbCrLf
sConsulta = sConsulta & "Union" & vbCrLf
sConsulta = sConsulta & "(SELECT DISTINCT COM_GMN2.GMN1,GMN1.' + @SDEN + ',COM_GMN2.GMN2,GMN2.' + @SDEN + ', NULL,NULL,NULL,NULL FROM COM_GMN2 INNER JOIN GMN2 ON COM_GMN2.GMN1 = GMN2.GMN1 AND COM_GMN2.GMN2  = GMN2.COD" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN GMN1 ON GMN1.COD = COM_GMN2.GMN1 WHERE COM_GMN2.EQP = ''' + @EQP + ''' AND COM_GMN2.COM = ''' +  @COM + ''')" & vbCrLf
sConsulta = sConsulta & "Union" & vbCrLf
sConsulta = sConsulta & "(SELECT DISTINCT COM_GMN3.GMN1,GMN1.' + @SDEN + ',COM_GMN3.GMN2,GMN2.' + @SDEN + ',COM_GMN3.GMN3,GMN3.' + @SDEN + ',NULL,NULL FROM COM_GMN3 INNER JOIN GMN3 ON COM_GMN3.GMN1 = GMN3.GMN1 AND COM_GMN3.GMN2 = GMN3.GMN2 AND COM_GMN3.GMN3 = GMN3.COD" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN GMN1 ON GMN1.COD = COM_GMN3.GMN1" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN GMN2 ON GMN2.COD = COM_GMN3.GMN2  AND GMN2.GMN1= COM_GMN3.GMN1" & vbCrLf
sConsulta = sConsulta & "     WHERE COM_GMN3.EQP = ''' + @EQP + ''' AND COM_GMN3.COM = ''' + @COM + ''')" & vbCrLf
sConsulta = sConsulta & "Union" & vbCrLf
sConsulta = sConsulta & "(SELECT DISTINCT COM_GMN4.GMN1,GMN1.' + @SDEN + ',COM_GMN4.GMN2,GMN2.' + @SDEN +',COM_GMN4.GMN3,GMN3.' + @SDEN +',COM_GMN4.GMN4,GMN4.' + @SDEN + ' FROM COM_GMN4 INNER JOIN GMN4 ON COM_GMN4.GMN1 = GMN4.GMN1 AND COM_GMN4.GMN2 = GMN4.GMN2 AND COM_GMN4.GMN3 = GMN4.GMN3 AND COM_GMN4.GMN4 = GMN4.COD" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN GMN1 ON GMN1.COD = COM_GMN4.GMN1" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN GMN2 ON GMN2.COD = COM_GMN4.GMN2 AND GMN2.GMN1= COM_GMN4.GMN1" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN GMN3 ON GMN3.COD = COM_GMN4.GMN3 AND GMN3.GMN1= COM_GMN4.GMN1 AND GMN3.GMN2 = COM_GMN4.GMN2" & vbCrLf
sConsulta = sConsulta & " WHERE COM_GMN4.EQP = ''' + @EQP + ''' AND COM_GMN4.COM = ''' +@COM + ''')" & vbCrLf
sConsulta = sConsulta & "ORDER BY DEN1,DEN2,DEN3,DEN4" & vbCrLf
sConsulta = sConsulta & "'" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @EQP = '' AND @COM = '' AND @ORDEN = 'COD'" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "set @SQL=N'" & vbCrLf
sConsulta = sConsulta & "SELECT GMN1.COD COD1,GMN1.' + @SDEN + ' DEN1 ,GMN2.COD COD2,GMN2.' + @SDEN + ' DEN2,GMN3.COD COD3,GMN3.' + @SDEN + ' DEN3,GMN4.COD COD4,GMN4.' + @SDEN + ' DEN4 FROM GMN1" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN GMN2 ON GMN1.COD = GMN2.GMN1" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN GMN3 ON GMN1.COD = GMN3.GMN1 AND GMN2.COD = GMN3.GMN2" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN GMN4 ON GMN1.COD = GMN4.GMN1 AND GMN2.COD = GMN4.GMN2 AND GMN3.COD = GMN4.GMN3" & vbCrLf
sConsulta = sConsulta & "ORDER BY COD1,COD2,COD3,COD4" & vbCrLf
sConsulta = sConsulta & "'" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @EQP = '' AND @COM = '' AND @ORDEN = 'DEN'" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "set @SQL=N'" & vbCrLf
sConsulta = sConsulta & "SELECT GMN1.COD COD1,GMN1.' + @SDEN + ' DEN1 ,GMN2.COD COD2,GMN2.' + @SDEN + ' DEN2,GMN3.COD COD3,GMN3.' + @SDEN + ' DEN3,GMN4.COD COD4,GMN4.' + @SDEN + ' DEN4 FROM GMN1" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN GMN2 ON GMN1.COD = GMN2.GMN1" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN GMN3 ON GMN1.COD = GMN3.GMN1 AND GMN2.COD = GMN3.GMN2" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN GMN4 ON GMN1.COD = GMN4.GMN1 AND GMN2.COD = GMN4.GMN2 AND GMN3.COD = GMN4.GMN3" & vbCrLf
sConsulta = sConsulta & "ORDER BY DEN1,DEN2,DEN3,DEN4" & vbCrLf
sConsulta = sConsulta & "'" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC sp_executesql @SQL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


End Sub

Private Sub V_2_16_5_Storeds_2_1()
Dim sConsulta As String

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_VALIDAR_LIMITES]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_VALIDAR_LIMITES]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSEP_VALIDAR_LIMITES(@PEDIDO AS INTEGER,@PER AS VARCHAR(50))  AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ORDEN AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @SEGURIDAD AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @IMPORTE AS FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PADRE AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @PERPADRE AS varchar(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @NIVEL AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @SUMA AS FLOAT" & vbCrLf
sConsulta = sConsulta & "/*Se recorre las �rdenes de entrega*/" & vbCrLf
sConsulta = sConsulta & "DECLARE C1 CURSOR FOR SELECT ID FROM ORDEN_ENTREGA WHERE PEDIDO=@PEDIDO" & vbCrLf
sConsulta = sConsulta & "OPEN C1" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C1 INTO @ORDEN" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "    /*Cursor para las lineas de pedido que no est�n pendientes por l�mite de adj y que no est�n esperando a eso*/" & vbCrLf
sConsulta = sConsulta & "    DECLARE C2 CURSOR FOR  SELECT DISTINCT SEGURIDAD FROM LINEAS_PEDIDO LP1  WHERE LP1.PEDIDO=@PEDIDO  AND LP1.ORDEN=@ORDEN " & vbCrLf
sConsulta = sConsulta & "        AND NOT EXISTS (SELECT * FROM LINEAS_PEDIDO LP2 WHERE LP2.PEDIDO=LP1.PEDIDO AND LP2.ORDEN=LP1.ORDEN AND LP2.NIVEL_APROB=0" & vbCrLf
sConsulta = sConsulta & "                                            AND LP2.SEGURIDAD=LP1.SEGURIDAD)" & vbCrLf
sConsulta = sConsulta & "    OPEN C2 " & vbCrLf
sConsulta = sConsulta & "    FETCH NEXT FROM C2 INTO @SEGURIDAD" & vbCrLf
sConsulta = sConsulta & "    WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "      BEGIN" & vbCrLf
sConsulta = sConsulta & "        /* Sacamos el importe l�mite del aprovisionador, su nivel y su superior por si hay que pasarlo a aprobacion */" & vbCrLf
sConsulta = sConsulta & "        SELECT @PADRE=PADRE,@IMPORTE=IMPORTE, @NIVEL=NIVEL  FROM APROB_LIM  WHERE PER=@PER AND SEGURIDAD=@SEGURIDAD " & vbCrLf
sConsulta = sConsulta & "       /* Calculamos la suma de los importes de las l�neas de esa seguridad para ver si se pasa */" & vbCrLf
sConsulta = sConsulta & "        SELECT @SUMA=SUM(CANT_PED*PREC_UP)  FROM LINEAS_PEDIDO WHERE ORDEN=@ORDEN  AND SEGURIDAD=@SEGURIDAD --AND NIVEL_APROB=-1" & vbCrLf
sConsulta = sConsulta & "        IF @SUMA>@IMPORTE" & vbCrLf
sConsulta = sConsulta & "            BEGIN" & vbCrLf
sConsulta = sConsulta & "               IF @PADRE IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                    BEGIN" & vbCrLf
sConsulta = sConsulta & "                    SELECT @PERPADRE=PER  FROM APROB_LIM WHERE ID=@PADRE" & vbCrLf
sConsulta = sConsulta & "                    UPDATE LINEAS_PEDIDO SET NIVEL_APROB= @NIVEL-1,  APROB_LIM=@PADRE,   EST=0, APROBADOR=@PERPADRE" & vbCrLf
sConsulta = sConsulta & "                        WHERE ORDEN=@ORDEN  AND SEGURIDAD=@SEGURIDAD -- AND NIVEL_APROB=-1   " & vbCrLf
sConsulta = sConsulta & "                    END" & vbCrLf
sConsulta = sConsulta & "               ELSE" & vbCrLf
sConsulta = sConsulta & "                    UPDATE LINEAS_PEDIDO SET NIVEL_APROB= @NIVEL , EST=0,APROBADOR=NULL" & vbCrLf
sConsulta = sConsulta & "                        WHERE ORDEN=@ORDEN  AND SEGURIDAD=@SEGURIDAD --AND NIVEL_APROB=-1       " & vbCrLf
sConsulta = sConsulta & "             END" & vbCrLf
sConsulta = sConsulta & "        ELSE" & vbCrLf
sConsulta = sConsulta & "             UPDATE LINEAS_PEDIDO SET EST=1,APROBADOR=@PER   WHERE PEDIDO=@PEDIDO AND ORDEN=@ORDEN AND SEGURIDAD=@SEGURIDAD --  AND NIVEL_APROB=-1  " & vbCrLf
sConsulta = sConsulta & "       --Inserto en LINEAS_EST las lineas de pedido quedan pendientes de aprobar, las aprobadas se insertan en un trigger de LINEAS_PEDIDO" & vbCrLf
sConsulta = sConsulta & "       INSERT INTO LINEAS_EST(PEDIDO,ORDEN,LINEA,EST,FECHA,PER) SELECT PEDIDO,ORDEN,ID,EST,GETDATE(),APROBADOR FROM LINEAS_PEDIDO " & vbCrLf
sConsulta = sConsulta & "                     WHERE PEDIDO=@PEDIDO AND ORDEN=@ORDEN AND SEGURIDAD=@SEGURIDAD" & vbCrLf
sConsulta = sConsulta & "        FETCH NEXT FROM C2 INTO @SEGURIDAD" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "      CLOSE  C2" & vbCrLf
sConsulta = sConsulta & "      DEALLOCATE C2" & vbCrLf
sConsulta = sConsulta & "    FETCH NEXT FROM C1 INTO @ORDEN" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "CLOSE  C1" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C1" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_VALIDAR_ADJUDICACION]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_VALIDAR_ADJUDICACION]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSEP_VALIDAR_ADJUDICACION(@PEDIDO AS INTEGER)  AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ORDEN AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @SEGURIDAD AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @CANT_ADJ AS FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @CANT_PED AS FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @FC AS FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @CANT_PED_LP AS FLOAT" & vbCrLf
sConsulta = sConsulta & "DECLARE @LINEA AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @EST AS SMALLINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @APROB AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "/*Se recorre las �rdenes de entrega*/" & vbCrLf
sConsulta = sConsulta & "DECLARE C1 CURSOR LOCAL FOR SELECT ID FROM ORDEN_ENTREGA  WHERE PEDIDO=@PEDIDO" & vbCrLf
sConsulta = sConsulta & "OPEN C1" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C1 INTO @ORDEN" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "  BEGIN" & vbCrLf
sConsulta = sConsulta & "        UPDATE LINEAS_PEDIDO  " & vbCrLf
sConsulta = sConsulta & "           SET NIVEL_APROB=CASE WHEN ( (((LP.CANT_PED*LP.FC) + IA.CANT_PED) > IA.CANT_ADJ) AND APROB_TIPO1 IS NOT NULL)  THEN 0 ELSE -1 END, " & vbCrLf
sConsulta = sConsulta & "                       EST=CASE WHEN ( (((LP.CANT_PED*LP.FC) + IA.CANT_PED) > IA.CANT_ADJ) AND APROB_TIPO1 IS NOT NULL)  THEN 0 ELSE 1 END," & vbCrLf
sConsulta = sConsulta & "                       APROBADOR=CASE WHEN ( (((LP.CANT_PED*LP.FC) + IA.CANT_PED) > IA.CANT_ADJ) AND APROB_TIPO1 IS NOT NULL)  THEN APROB_TIPO1 ELSE NULL END" & vbCrLf
sConsulta = sConsulta & "          FROM LINEAS_PEDIDO LP " & vbCrLf
sConsulta = sConsulta & "                 INNER JOIN ITEM_ADJ IA" & vbCrLf
sConsulta = sConsulta & "                         ON LP.ANYO = IA.ANYO" & vbCrLf
sConsulta = sConsulta & "                        AND LP.GMN1 = IA.GMN1" & vbCrLf
sConsulta = sConsulta & "                        AND LP.PROCE = IA.PROCE" & vbCrLf
sConsulta = sConsulta & "                        AND LP.ITEM = IA.ITEM" & vbCrLf
sConsulta = sConsulta & "                        AND LP.PROVE = IA.PROVE" & vbCrLf
sConsulta = sConsulta & "                  LEFT JOIN SEGURIDAD S" & vbCrLf
sConsulta = sConsulta & "                         ON LP.SEGURIDAD = S.ID" & vbCrLf
sConsulta = sConsulta & "         WHERE LP.PEDIDO=@PEDIDO " & vbCrLf
sConsulta = sConsulta & "           AND LP.ORDEN=@ORDEN " & vbCrLf
sConsulta = sConsulta & "       --si hay alguna l�nea de la orden con est=0 habr� que actualizar el estado de las dem�s l�neas de esa seguridad a 21, las lineas de otras seguridades se controlan aparte*/       " & vbCrLf
sConsulta = sConsulta & "         DECLARE C2 CURSOR LOCAL FOR SELECT SEGURIDAD,ID,APROBADOR FROM LINEAS_PEDIDO WHERE PEDIDO=@PEDIDO AND ORDEN=@ORDEN AND NIVEL_APROB=0 AND EST=0" & vbCrLf
sConsulta = sConsulta & "         OPEN C2" & vbCrLf
sConsulta = sConsulta & "         FETCH NEXT FROM C2 INTO @SEGURIDAD,@LINEA,@APROB" & vbCrLf
sConsulta = sConsulta & "         WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "           BEGIN" & vbCrLf
sConsulta = sConsulta & "              --Inserto en LINEAS_EST para las ordenes que quedan pendientes de aprobar el limite de adjudicaci�n" & vbCrLf
sConsulta = sConsulta & "              INSERT INTO LINEAS_EST(PEDIDO,ORDEN,LINEA,EST,FECHA,PER) VALUES (@PEDIDO,@ORDEN,@LINEA,0,GETDATE(),@APROB)" & vbCrLf
sConsulta = sConsulta & "              UPDATE LINEAS_PEDIDO SET EST=21 WHERE PEDIDO=@PEDIDO AND ORDEN=@ORDEN AND NIVEL_APROB!=0 AND SEGURIDAD=@SEGURIDAD" & vbCrLf
sConsulta = sConsulta & "              INSERT INTO LINEAS_EST(PEDIDO,ORDEN,LINEA,EST,FECHA,PER) SELECT PEDIDO,ORDEN,ID,21,GETDATE(),@APROB FROM LINEAS_PEDIDO " & vbCrLf
sConsulta = sConsulta & "                    WHERE PEDIDO=@PEDIDO AND ORDEN=@ORDEN AND EST=21 AND SEGURIDAD=@SEGURIDAD" & vbCrLf
sConsulta = sConsulta & "              FETCH NEXT FROM C2 INTO @SEGURIDAD,@LINEA,@APROB" & vbCrLf
sConsulta = sConsulta & "          END" & vbCrLf
sConsulta = sConsulta & "        CLOSE C2" & vbCrLf
sConsulta = sConsulta & "        DEALLOCATE C2" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "      FETCH NEXT FROM C1 INTO @ORDEN" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "CLOSE  C1" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C1" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_INSERTAR_ESTADO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_INSERTAR_ESTADO]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSEP_INSERTAR_ESTADO(@PEDIDO AS INTEGER,@PER AS VARCHAR(50))  AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ORDEN AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @ESTORDEN AS SMALLINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @EST AS SMALLINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @APROB AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF (SELECT COUNT(*) FROM ORDEN_ENTREGA WHERE PEDIDO = @PEDIDO AND EST = 0) = 0 " & vbCrLf
sConsulta = sConsulta & "   INSERT INTO ORDEN_EST (PEDIDO,ORDEN,EST,FECHA,PER) " & vbCrLf
sConsulta = sConsulta & "   SELECT @PEDIDO, ID, EST,GETDATE(),@PER" & vbCrLf
sConsulta = sConsulta & "     FROM ORDEN_ENTREGA " & vbCrLf
sConsulta = sConsulta & "     WHERE PEDIDO=@PEDIDO" & vbCrLf
sConsulta = sConsulta & "ELSE --Si hay ordenes pendientes de aprobar guarda distintos registros en ORDEN_EST" & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "     /*Selecciona para las ordenes los distintos estados y aprobadores */" & vbCrLf
sConsulta = sConsulta & "    DECLARE C1 CURSOR FOR SELECT DISTINCT O.EST,L.ORDEN,L.EST,L.APROBADOR FROM LINEAS_PEDIDO L INNER JOIN ORDEN_ENTREGA O ON O.ID=L.ORDEN" & vbCrLf
sConsulta = sConsulta & "                     WHERE L.PEDIDO=@PEDIDO " & vbCrLf
sConsulta = sConsulta & "    OPEN C1" & vbCrLf
sConsulta = sConsulta & "    FETCH NEXT FROM C1 INTO @ESTORDEN,@ORDEN,@EST,@APROB" & vbCrLf
sConsulta = sConsulta & "    WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "      BEGIN" & vbCrLf
sConsulta = sConsulta & "           IF @ESTORDEN=0 " & vbCrLf
sConsulta = sConsulta & "                BEGIN" & vbCrLf
sConsulta = sConsulta & "                IF @EST=0" & vbCrLf
sConsulta = sConsulta & "                    INSERT INTO ORDEN_EST (PEDIDO,ORDEN,EST,FECHA,PER) VALUES (@PEDIDO,@ORDEN,0,GETDATE(),@APROB)" & vbCrLf
sConsulta = sConsulta & "                END" & vbCrLf
sConsulta = sConsulta & "           ELSE" & vbCrLf
sConsulta = sConsulta & "      BEGIN" & vbCrLf
sConsulta = sConsulta & "                 IF @ESTORDEN=2" & vbCrLf
sConsulta = sConsulta & "                IF (SELECT COUNT(*) FROM ORDEN_EST WHERE ORDEN=@ORDEN AND EST=2)=0 " & vbCrLf
sConsulta = sConsulta & "                            INSERT INTO ORDEN_EST (PEDIDO,ORDEN,EST,FECHA,PER) VALUES (@PEDIDO,@ORDEN,2,GETDATE(),@PER)" & vbCrLf
sConsulta = sConsulta & "                END " & vbCrLf
sConsulta = sConsulta & "           FETCH NEXT FROM C1 INTO @ESTORDEN,@ORDEN,@EST,@APROB" & vbCrLf
sConsulta = sConsulta & "      END" & vbCrLf
sConsulta = sConsulta & "    CLOSE C1" & vbCrLf
sConsulta = sConsulta & "    DEALLOCATE C1" & vbCrLf
sConsulta = sConsulta & "    END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_ACEPTAR_ORDEN_PROVE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_ACEPTAR_ORDEN_PROVE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSEP_ACEPTAR_ORDEN_PROVE @ORDEN int, @PEDIDO int,@NUMPROVE VARCHAR(100),@COMENT varchar(1500) AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "UPDATE ORDEN_ENTREGA SET NUMEXT=@NUMPROVE ,EST=3 WHERE ID=@ORDEN" & vbCrLf
sConsulta = sConsulta & "INSERT INTO ORDEN_EST (PEDIDO,ORDEN,EST,FECHA,PER,COMENT) VALUES (@PEDIDO,@ORDEN,3,GETDATE(),null,@COMENT)" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_NUEVA_RECEPCION]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_NUEVA_RECEPCION]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSEP_NUEVA_RECEPCION @ORDEN AS integer, @FECHA varchar(10), @ALBARAN varchar(100), @CORRECTO tinyint, @OBS varchar(255), @SUBSANADO tinyint,@PER varchar(40),  @NOTIF bit, @LOG tinyint, @ID int OUTPUT AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @PEDIDO int" & vbCrLf
sConsulta = sConsulta & "DECLARE @EST smallint" & vbCrLf
sConsulta = sConsulta & "DECLARE @USU varchar(40)" & vbCrLf
sConsulta = sConsulta & "SELECT @PEDIDO = PEDIDO, @EST=EST FROM ORDEN_ENTREGA WHERE ID=@ORDEN" & vbCrLf
sConsulta = sConsulta & "INSERT INTO PEDIDO_RECEP (PEDIDO,ORDEN,FECHA,ALBARAN,CORRECTO,COMENT)" & vbCrLf
sConsulta = sConsulta & "                VALUES (@PEDIDO,@ORDEN,convert(datetime,@FECHA,103),@ALBARAN,@CORRECTO,@OBS)" & vbCrLf
sConsulta = sConsulta & "if @SUBSANADO=1 AND @CORRECTO=1" & vbCrLf
sConsulta = sConsulta & "  begin" & vbCrLf
sConsulta = sConsulta & "    UPDATE ORDEN_ENTREGA" & vbCrLf
sConsulta = sConsulta & "       SET INCORRECTA=0" & vbCrLf
sConsulta = sConsulta & "     WHERE ID = @ORDEN" & vbCrLf
sConsulta = sConsulta & "    if @NOTIF = 1 " & vbCrLf
sConsulta = sConsulta & "      INSERT INTO ORDEN_COMUNIC (PEDIDO, ORDEN, APE, NOM, EMAILDIR, TIPO, FECHA, WEB, EMAIL, IMP) " & vbCrLf
sConsulta = sConsulta & "      SELECT @PEDIDO,@ORDEN, CON.APE,CON.NOM,CON.EMAIL ,2, getdate(), 1,0,0 " & vbCrLf
sConsulta = sConsulta & "        FROM CON " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN PROVE " & vbCrLf
sConsulta = sConsulta & "                   ON PROVE.COD=CON.PROVE " & vbCrLf
sConsulta = sConsulta & "           INNER JOIN ORDEN_ENTREGA " & vbCrLf
sConsulta = sConsulta & "                   ON ORDEN_ENTREGA.ID=@ORDEN" & vbCrLf
sConsulta = sConsulta & "                  AND ORDEN_ENTREGA.PROVE=PROVE.COD " & vbCrLf
sConsulta = sConsulta & "       WHERE CON.APROV=1" & vbCrLf
sConsulta = sConsulta & "  END" & vbCrLf
sConsulta = sConsulta & "else" & vbCrLf
sConsulta = sConsulta & "  IF @CORRECTO = 0 " & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "      UPDATE ORDEN_ENTREGA" & vbCrLf
sConsulta = sConsulta & "         SET INCORRECTA=1" & vbCrLf
sConsulta = sConsulta & "       WHERE ID = @ORDEN" & vbCrLf
sConsulta = sConsulta & "      if @NOTIF = 1 " & vbCrLf
sConsulta = sConsulta & "        INSERT INTO ORDEN_COMUNIC (PEDIDO, ORDEN, APE, NOM, EMAILDIR, TIPO, FECHA, WEB, EMAIL, IMP) " & vbCrLf
sConsulta = sConsulta & "        SELECT @PEDIDO,@ORDEN, CON.APE,CON.NOM,CON.EMAIL ,3, getdate(), 1,0,0 " & vbCrLf
sConsulta = sConsulta & "          FROM CON " & vbCrLf
sConsulta = sConsulta & "             INNER JOIN PROVE " & vbCrLf
sConsulta = sConsulta & "                     ON PROVE.COD=CON.PROVE " & vbCrLf
sConsulta = sConsulta & "             INNER JOIN ORDEN_ENTREGA " & vbCrLf
sConsulta = sConsulta & "                     ON ORDEN_ENTREGA.ID=@ORDEN" & vbCrLf
sConsulta = sConsulta & "                   AND ORDEN_ENTREGA.PROVE=PROVE.COD " & vbCrLf
sConsulta = sConsulta & "         WHERE CON.APROV=1" & vbCrLf
sConsulta = sConsulta & "    END" & vbCrLf
sConsulta = sConsulta & "      " & vbCrLf
sConsulta = sConsulta & "SELECT @ID =MAX(ID) FROM PEDIDO_RECEP" & vbCrLf
sConsulta = sConsulta & "/*IF @EST IN (3,4)" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "      UPDATE ORDEN_ENTREGA " & vbCrLf
sConsulta = sConsulta & "         SET EST = 5" & vbCrLf
sConsulta = sConsulta & "       WHERE ID = @ORDEN" & vbCrLf
sConsulta = sConsulta & "      INSERT INTO ORDEN_EST (PEDIDO, ORDEN, EST, FECHA, PER)" & vbCrLf
sConsulta = sConsulta & "          VALUES (@PEDIDO,@ORDEN, 5, GETDATE(),@PER)" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "*/" & vbCrLf
sConsulta = sConsulta & "IF @LOG IS NOT NULL " & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "  SET @USU = (SELECT COD FROM USU WHERE PER = @PER)" & vbCrLf
sConsulta = sConsulta & "  INSERT INTO LOG_PEDIDO_RECEP (ACCION,ID_PEDIDO_RECEP,ID_PEDIDO,ID_ORDEN_ENTREGA,TIPO,FECHA,ALBARAN,CORRECTO,COMENT,ORIGEN,PER,USU,FECACT)" & vbCrLf
sConsulta = sConsulta & "                VALUES ('I',@ID,@PEDIDO,@ORDEN,1,convert(datetime,@FECHA,103),@ALBARAN,@CORRECTO,@OBS,@LOG,@PER,@USU,GETDATE())" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_RECHAZAR_ORDEN_PROVE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_RECHAZAR_ORDEN_PROVE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSEP_RECHAZAR_ORDEN_PROVE @ORDEN int, @PEDIDO int,@COMENT varchar(1500) AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "UPDATE ORDEN_ENTREGA SET EST=21 WHERE ID=@ORDEN" & vbCrLf
sConsulta = sConsulta & "INSERT INTO ORDEN_EST (PEDIDO,ORDEN,EST,FECHA,PER,COMENT) VALUES (@PEDIDO,@ORDEN,21,GETDATE(),null,@COMENT)" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_REEMITIR_ORDEN]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_REEMITIR_ORDEN]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSEP_REEMITIR_ORDEN @ORDEN INT, @APROVISIONADOR varchar(50) AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @PEDIDO int" & vbCrLf
sConsulta = sConsulta & "SELECT @PEDIDO = PEDIDO  FROM ORDEN_ENTREGA  WHERE ID = @ORDEN" & vbCrLf
sConsulta = sConsulta & "DELETE FROM LINEAS_EST   FROM LINEAS_EST LE    INNER JOIN LINEAS_PEDIDO LP  ON LE.LINEA = LP.ID WHERE LP.ORDEN = @ORDEN   AND LP.EST = 20" & vbCrLf
sConsulta = sConsulta & "DELETE FROM LINEAS_PEDIDO_ADJUN WHERE LINEA IN (SELECT ID FROM LINEAS_PEDIDO WHERE ORDEN = @ORDEN AND EST = 20)" & vbCrLf
sConsulta = sConsulta & "DELETE FROM LINEAS_PEDIDO WHERE ORDEN = @ORDEN   AND EST = 20" & vbCrLf
sConsulta = sConsulta & "DECLARE @IMPORTE float" & vbCrLf
sConsulta = sConsulta & "SELECT @IMPORTE = SUM(PREC_UP*CANT_PED)   FROM LINEAS_PEDIDO WHERE ORDEN = @ORDEN" & vbCrLf
sConsulta = sConsulta & "UPDATE LINEAS_PEDIDO   SET NIVEL_APROB=-1 WHERE ORDEN = @ORDEN" & vbCrLf
sConsulta = sConsulta & "UPDATE ORDEN_ENTREGA   SET IMPORTE = @IMPORTE,  EST = 2  FROM ORDEN_ENTREGA OE    INNER JOIN LINEAS_PEDIDO LP  ON OE.ID = LP.ORDEN" & vbCrLf
sConsulta = sConsulta & "    WHERE oe.id = @ORDEN" & vbCrLf
sConsulta = sConsulta & "INSERT INTO ORDEN_EST (PEDIDO, ORDEN, EST, FECHA, PER)  VALUES (@PEDIDO, @ORDEN, 2, GETDATE(),@APROVISIONADOR)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_REGISTRAR_CANTIDAD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_REGISTRAR_CANTIDAD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSEP_REGISTRAR_CANTIDAD @ORDEN int, @RECEP int, @LINEA int, @CANT float, @PER varchar(40),@LOG tinyint AS" & vbCrLf
sConsulta = sConsulta & "UPDATE LINEAS_RECEP" & vbCrLf
sConsulta = sConsulta & "   SET CANT = @CANT" & vbCrLf
sConsulta = sConsulta & " WHERE ORDEN=@ORDEN" & vbCrLf
sConsulta = sConsulta & "   AND PEDIDO_RECEP = @RECEP" & vbCrLf
sConsulta = sConsulta & "   AND LINEA = @LINEA" & vbCrLf
sConsulta = sConsulta & "DECLARE @ESTANTIGUO smallint" & vbCrLf
sConsulta = sConsulta & "DECLARE @ESTNUEVO smallint" & vbCrLf
sConsulta = sConsulta & "SELECT @ESTANTIGUO = EST" & vbCrLf
sConsulta = sConsulta & "  FROM LINEAS_PEDIDO" & vbCrLf
sConsulta = sConsulta & " WHERE ID = @LINEA" & vbCrLf
sConsulta = sConsulta & "UPDATE LINEAS_PEDIDO" & vbCrLf
sConsulta = sConsulta & "   SET CANT_REC = CANT_REC + @CANT," & vbCrLf
sConsulta = sConsulta & "       EST = CASE WHEN CANT_PED = CANT_REC + @CANT THEN 3 ELSE CASE WHEN @CANT>0 THEN 2 ELSE EST END END" & vbCrLf
sConsulta = sConsulta & "  FROM LINEAS_PEDIDO" & vbCrLf
sConsulta = sConsulta & " WHERE ID = @LINEA" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "DECLARE @PEDIDO int" & vbCrLf
sConsulta = sConsulta & "SELECT @PEDIDO = PEDIDO, @ESTNUEVO = EST" & vbCrLf
sConsulta = sConsulta & "  FROM LINEAS_PEDIDO" & vbCrLf
sConsulta = sConsulta & " WHERE ID = @LINEA" & vbCrLf
sConsulta = sConsulta & "IF @ESTANTIGUO!=@ESTNUEVO" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO LINEAS_EST (PEDIDO, ORDEN, LINEA,EST,FECHA,PER)  VALUES (@PEDIDO, @ORDEN, @LINEA, @ESTNUEVO, GETDATE(),@PER)" & vbCrLf
sConsulta = sConsulta & "        " & vbCrLf
sConsulta = sConsulta & "/*IF (SELECT COUNT(*) " & vbCrLf
sConsulta = sConsulta & "      FROM LINEAS_PEDIDO " & vbCrLf
sConsulta = sConsulta & "     WHERE CANT_PED!=CANT_REC" & vbCrLf
sConsulta = sConsulta & "       AND ORDEN = @ORDEN)=0" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "     UPDATE ORDEN_ENTREGA SET EST = 6,  INCORRECTA = 0  WHERE ID = @ORDEN" & vbCrLf
sConsulta = sConsulta & "     INSERT INTO ORDEN_EST (PEDIDO, ORDEN, EST, FECHA, PER)  VALUES (@PEDIDO, @ORDEN, 6, GETDATE(),@PER)" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "*/" & vbCrLf
sConsulta = sConsulta & "IF NOT @LOG IS NULL" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & " UPDATE LOG_LINEAS_RECEP SET CANT = @CANT WHERE ID_LINEA_PEDIDO =@LINEA AND ID_PEDIDO_RECEP = @RECEP" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_REGISTRAR_ENVIO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_REGISTRAR_ENVIO]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE FSEP_REGISTRAR_ENVIO @ORDEN int AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @PEDIDO int" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT @PEDIDO = PEDIDO FROM ORDEN_ENTREGA WHERE ID=@ORDEN" & vbCrLf
sConsulta = sConsulta & "UPDATE ORDEN_ENTREGA SET EST = 4  WHERE ID = @ORDEN" & vbCrLf
sConsulta = sConsulta & "INSERT INTO ORDEN_EST (PEDIDO, ORDEN, EST, FECHA)  VALUES (@PEDIDO,@ORDEN, 4, GETDATE())" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub


Public Function CodigoDeActualizacion2_16_05_02A2_16_05_03() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim iRespuesta As Integer

On Error GoTo Error
sConsulta = "BEGIN TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = True
ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

frmProgreso.lblDetalle = "A�adimos campos"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
V_2_16_5_Tablas_3



sConsulta = "UPDATE VERSION SET NUM ='2.16.05.03'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "COMMIT TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = False

CodigoDeActualizacion2_16_05_02A2_16_05_03 = True
Exit Function
    
Error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_16_05_02A2_16_05_03 = False

End Function

Private Sub V_2_16_5_Tablas_3()
    Dim sConsulta As String
    
sConsulta = "CREATE TABLE [dbo].[LINEAS_PEDIDO_REGISTRO] (" & vbCrLf
sConsulta = sConsulta & "   [ID] [int] IDENTITY (1, 1) NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [PEDIDO] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [ORDEN] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [LINEA] [int] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [PREC_UP] [float] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [CANT_PED] [float] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [DEST] [varchar] (" & gLongitudesDeCodigos.giLongCodDEST & ") NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [ENTREGA_OBL] [tinyint] NOT NULL ," & vbCrLf
sConsulta = sConsulta & "   [FECENTREGA] [datetime] NULL ," & vbCrLf
sConsulta = sConsulta & "   [PER] [varchar] (" & gLongitudesDeCodigos.giLongCodPER & ") NULL ," & vbCrLf
sConsulta = sConsulta & "   [FECACT] [datetime] NULL " & vbCrLf
sConsulta = sConsulta & ") ON [PRIMARY]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
 
 sConsulta = "ALTER TABLE [dbo].[LINEAS_PEDIDO_REGISTRO] ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [DF_LINEAS_PEDIDO_REGISTRO_ENTREGA_OBL] DEFAULT (0) FOR [ENTREGA_OBL]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "ALTER TABLE [dbo].[LINEAS_PEDIDO_REGISTRO] ADD " & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [FK_LINEAS_PEDIDO_REGISTRO_DEST] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [DEST]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[DEST] (" & vbCrLf
sConsulta = sConsulta & "       [COD]" & vbCrLf
sConsulta = sConsulta & "   )," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [FK_LINEAS_PEDIDO_REGISTRO_LINEAS_PEDIDO] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [LINEA]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[LINEAS_PEDIDO] (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [FK_LINEAS_PEDIDO_REGISTRO_ORDEN_ENTREGA] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [ORDEN]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[ORDEN_ENTREGA] (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [FK_LINEAS_PEDIDO_REGISTRO_PEDIDO] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [PEDIDO]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[PEDIDO] (" & vbCrLf
sConsulta = sConsulta & "       [ID]" & vbCrLf
sConsulta = sConsulta & "   )," & vbCrLf
sConsulta = sConsulta & "   CONSTRAINT [FK_LINEAS_PEDIDO_REGISTRO_PER] FOREIGN KEY " & vbCrLf
sConsulta = sConsulta & "   (" & vbCrLf
sConsulta = sConsulta & "       [PER]" & vbCrLf
sConsulta = sConsulta & "   ) REFERENCES [dbo].[PER] (" & vbCrLf
sConsulta = sConsulta & "       [COD]" & vbCrLf
sConsulta = sConsulta & "   )" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

 sConsulta = "ALTER TABLE [dbo].[PLANTILLA] ADD " & vbCrLf
sConsulta = sConsulta & "    [SUBASTA] [tinyint] NOT NULL CONSTRAINT [DF_PLANTILLA_SUBASTA] DEFAULT (0)," & vbCrLf
sConsulta = sConsulta & "    [SUBASTAPROVE] [tinyint] NOT NULL CONSTRAINT [DF_PLANTILLA_SUBASTAPROVE] DEFAULT (0)," & vbCrLf
sConsulta = sConsulta & "    [SUBASTAPUJAS] [tinyint] NOT NULL CONSTRAINT [DF_PLANTILLA_SUBASTAPUJAS] DEFAULT (0)," & vbCrLf
sConsulta = sConsulta & "    [SUBASTAESPERA] [smallint] NOT NULL CONSTRAINT [DF_PLANTILLA_SUBASTAESPERA] DEFAULT (0)," & vbCrLf
sConsulta = sConsulta & "    [OFE_ADJUN] [tinyint] NOT NULL CONSTRAINT [DF_PLANTILLA_OFE_ADJUN] DEFAULT (1)," & vbCrLf
sConsulta = sConsulta & "    [GRUPO_ADJUN] [tinyint] NOT NULL CONSTRAINT [DF_PLANTILLA_GRUPO_ADJUN] DEFAULT (0)," & vbCrLf
sConsulta = sConsulta & "    [ITEM_ADJUN] [tinyint] NOT NULL CONSTRAINT [DF_PLANTILLA_ITEM_ADJUN] DEFAULT (0)," & vbCrLf
sConsulta = sConsulta & "    [SOLCANTMAX] [tinyint] NOT NULL CONSTRAINT [DF_PLANTILLA_SOLCANTMAX] DEFAULT (1)," & vbCrLf
sConsulta = sConsulta & "    [PUBLICARFINSUM] [tinyint] NOT NULL CONSTRAINT [DF_PLANTILLA_PUBLICARFINSUM] DEFAULT (1)," & vbCrLf
sConsulta = sConsulta & "    [PONDERAR] [tinyint] NOT NULL CONSTRAINT [DF_PLANTILLA_PONDERAR] DEFAULT (0)," & vbCrLf
sConsulta = sConsulta & "    [PRECALTER] [tinyint] NOT NULL CONSTRAINT [DF_PLANTILLA_PRECALTER] DEFAULT (0)," & vbCrLf
sConsulta = sConsulta & "    [CAMBIARMON] [tinyint] NOT NULL CONSTRAINT [DF_PLANTILLA_CAMBIARMON] DEFAULT (1)," & vbCrLf
sConsulta = sConsulta & "    [UNSOLOPEDIDO] [tinyint] NOT NULL CONSTRAINT [DF_PLANTILLA_UNSOLOPEDIDO] DEFAULT (0)"
ExecuteSQL gRDOCon, sConsulta


End Sub


Public Function CodigoDeActualizacion2_16_05_03A2_16_05_04() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim iRespuesta As Integer

On Error GoTo Error
sConsulta = "BEGIN TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = True
ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

frmProgreso.lblDetalle = "A�adimos campos"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
V_2_16_5_Storeds_2_2



sConsulta = "UPDATE VERSION SET NUM ='2.16.05.04'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "COMMIT TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = False

CodigoDeActualizacion2_16_05_03A2_16_05_04 = True
Exit Function
    
Error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_16_05_03A2_16_05_04 = False

End Function


Private Sub V_2_16_5_Storeds_2_2()
Dim sConsulta As String

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PROVE_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[PROVE_COD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "CREATE PROCEDURE PROVE_COD (@OLD VARCHAR(50),@NEW VARCHAR(50))  AS " & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_COD VARCHAR(50) " & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT ON " & vbCrLf
sConsulta = sConsulta & "BEGIN TRANSACTION " & vbCrLf
sConsulta = sConsulta & "BEGIN " & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR FOR SELECT COD FROM PROVE WHERE COD=@OLD " & vbCrLf
sConsulta = sConsulta & "OPEN C " & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C INTO @VAR_COD " & vbCrLf
sConsulta = sConsulta & "IF @@FETCH_STATUS=0 " & vbCrLf
sConsulta = sConsulta & "BEGIN " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CON NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART1_ADJ NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART2_ADJ NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART3_ADJ NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ADJ NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ADJ NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ADJREU NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OFE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OFE_WEB NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE DETALLE_PUJAS NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFEMIN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_ADJUN NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_WEB_ADJUN NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_WEB NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_PET NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_POT NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_EQP NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN1 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN2 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN3 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN4 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVENOTIF NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ATRIB NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATALOG_LIN NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ORDEN_ENTREGA NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_PEDIDO NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART_UP NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ESP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ESP_PORTAL NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ITEM_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_GR_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ITEM_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_GR_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GRUPO_OFE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GRUPO_OFE_REU NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_CONTR NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FAVORITOS_ORDEN_ENTREGA NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FAVORITOS_LINEAS_PEDIDO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATN1_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATN2_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATN3_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATN4_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATN5_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_EMPRESA_COD_ERP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE SET COD=@NEW WHERE COD=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE CON SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_PROVE SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART1_ADJ SET CODPROVE=@NEW WHERE CODPROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ART2_ADJ SET CODPROVE=@NEW WHERE CODPROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ART3_ADJ SET CODPROVE=@NEW WHERE CODPROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ART4_ADJ SET CODPROVE=@NEW WHERE CODPROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM SET PROVEACT=@NEW WHERE PROVEACT=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_ADJ SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_ADJREU SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_OFE SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_OFE_WEB SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE DETALLE_PUJAS SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFEMIN SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE_ADJUN SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE_WEB_ADJUN SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE_WEB SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE_PET SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE_POT SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_EQP SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_GMN1 SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_GMN2 SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_GMN3 SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_GMN4 SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART1 SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART2 SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART3 SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART4 SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVENOTIF SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE USU SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART4_ATRIB SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CATALOG_LIN SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ORDEN_ENTREGA SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE LINEAS_PEDIDO SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART_UP SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ESP SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ESP_PORTAL SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET PROVEACT=@NEW WHERE PROVEACT=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GRUPO SET PROVEACT=@NEW WHERE PROVEACT=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ALL_PROVE SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_GRUPO_PROVE SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ITEM_PROVE SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_PROCE_PROVE SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_ATRIB SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_ITEM_ATRIB SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_GR_ATRIB SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_ITEM_ADJUN SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_GR_ADJUN SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GRUPO_OFE SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GRUPO_OFE_REU SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_PROVE SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_PROVE SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_CONTR SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CATN1_PROVE  SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CATN2_PROVE  SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CATN3_PROVE  SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CATN4_PROVE  SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CATN5_PROVE  SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART4_ADJUN  SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE FAVORITOS_ORDEN_ENTREGA  SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE FAVORITOS_LINEAS_PEDIDO  SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_EMPRESA_COD_ERP SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CON CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PROVE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART1_ADJ CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART2_ADJ CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART3_ADJ CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ADJ CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ADJ CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ADJREU CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OFE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OFE_WEB CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_ADJUN CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_WEB_ADJUN CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_WEB CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_PET CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_POT CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_EQP CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN1 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN2 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN3 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN4 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE DETALLE_PUJAS CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFEMIN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ATRIB CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATALOG_LIN CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ORDEN_ENTREGA CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_PEDIDO CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART_UP CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ESP CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ESP_PORTAL CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ITEM_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_GR_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ITEM_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_GR_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GRUPO_OFE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GRUPO_OFE_REU CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_CONTR CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "--2.12" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATN1_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATN2_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATN3_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATN4_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATN5_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FAVORITOS_ORDEN_ENTREGA CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FAVORITOS_LINEAS_PEDIDO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_EMPRESA_COD_ERP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "End " & vbCrLf
sConsulta = sConsulta & "Close C " & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C " & vbCrLf
sConsulta = sConsulta & "End " & vbCrLf
sConsulta = sConsulta & "COMMIT TRANSACTION" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT OFF"
ExecuteSQL gRDOCon, sConsulta
 

End Sub



Public Function CodigoDeActualizacion2_16_05_04A2_16_05_05() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim iRespuesta As Integer

On Error GoTo Error
sConsulta = "BEGIN TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = True
ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

frmProgreso.lblDetalle = "A�adimos campos"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1
V_2_16_5_Tablas_5



sConsulta = "UPDATE VERSION SET NUM ='2.16.05.05'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "COMMIT TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = False

CodigoDeActualizacion2_16_05_04A2_16_05_05 = True
Exit Function
    
Error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_16_05_04A2_16_05_05 = False

End Function

Private Sub V_2_16_5_Tablas_5()
Dim sConsulta As String

sConsulta = "ALTER TABLE dbo.PLANTILLA ADD"
sConsulta = sConsulta & "    SUBASTABAJMINPUJA tinyint NOT NULL CONSTRAINT DF_PLANTILLA_SUBASTABAJMINPUJA DEFAULT 0"
ExecuteSQL gRDOCon, sConsulta

End Sub

Public Function CodigoDeActualizacion2_16_05_05A2_16_05_06() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim iRespuesta As Integer

On Error GoTo Error
sConsulta = "BEGIN TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = True
ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

frmProgreso.lblDetalle = "A�adimos campos"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_16_5_Storeds_6



sConsulta = "UPDATE VERSION SET NUM ='2.16.05.06'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "COMMIT TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = False

CodigoDeActualizacion2_16_05_05A2_16_05_06 = True
Exit Function
    
Error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_16_05_05A2_16_05_06 = False

End Function


Private Sub V_2_16_5_Storeds_6()


Dim sConsulta As String

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GMN1_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[GMN1_COD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE GMN1_COD (@OLD VARCHAR(50),@NEW VARCHAR(50),@INT_ORIGEN VARCHAR(1),@USUARIO VARCHAR(50))  AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_DEN VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SDEN VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDI VARCHAR (50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PARAM AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT ON" & vbCrLf
sConsulta = sConsulta & "BEGIN TRANSACTION" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART1_ADJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART2_ADJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART3_ADJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ADJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE DETALLE_PUJAS NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ADJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ADJREU NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ESP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OBJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OFE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OFE_WEB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRES NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESCON NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESPROY NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESCON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESCON4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFEMIN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_GRUPO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_ESP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_WEB_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_WEB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PER NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_PET NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_POT NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVENOTIF NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE REU_PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ESP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4_ACT NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATALOG_LIN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_PEDIDO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART_UP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_DEF NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO_DEF NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO_ESP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_GR_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_GR_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ITEM_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ITEM_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_SOBRE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GRUPO_OFE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GRUPO_OFE_REU NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USAR_GR_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_CONTR NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE SOBRE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FAVORITOS_LINEAS_PEDIDO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_NUMDEC NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_ART NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISOR NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN1 SET COD=@NEW WHERE COD=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART1_ADJ SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART2_ADJ SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART3_ADJ SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4_ADJ SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE BLOQUEO_PROCE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE COM_GMN1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE COM_GMN2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE COM_GMN3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE COM_GMN4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE DETALLE_PUJAS SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_ADJ SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_ADJREU SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_ESP SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_OBJ SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_OFE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_OFE_WEB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_PRES SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_PRESCON SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_PRESCON3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_PRESCON4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_PRESPROY SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_UON1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_UON2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_UON3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFEMIN SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES_MAT1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES_MAT2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES_MAT3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES_MAT4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_ESP SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE_ADJUN SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE_WEB_ADJUN SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE_WEB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PER SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE_PET SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE_POT SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_GMN1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_GMN2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_GMN3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_GMN4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVENOTIF SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE REU_PROCE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_PROCE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_PROVE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_GRUPO SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_UON1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_UON2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_UON3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_PRESCON3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_PRESCON4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_PROVE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4_ESP  SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN4_ACT SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART4_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN4_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4_ATRIB  SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CATALOG_LIN  SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE LINEAS_PEDIDO  SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART_UP  SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_DEF SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GRUPO SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GRUPO_DEF SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GRUPO_ESP SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PRES1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PRES2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PRES3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PRES4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_PRES1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_PRES2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_PRES3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_PRES4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_UON1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_UON2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_UON3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_UON1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_UON2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_UON3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_GR_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_GR_ADJUN SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_ITEM_ADJUN SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_ITEM_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ALL SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ALL_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ALL_PROVE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_GRUPO SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_GRUPO_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_GRUPO_PROVE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ITEM SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ITEM_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ITEM_PROVE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_PROCE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_PROCE_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_PROCE_PROVE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_PROCE_SOBRE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GRUPO_OFE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GRUPO_OFE_REU SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE USAR_GR_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_CONTR SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE SOBRE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART4_ADJUN SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE FAVORITOS_LINEAS_PEDIDO SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_NUMDEC SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES_ART SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISOR SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART1_ADJ CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART2_ADJ CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART3_ADJ CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ADJ CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE DETALLE_PUJAS CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ADJ CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ADJREU CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ESP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OBJ CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OFE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OFE_WEB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRES CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESCON CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESCON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESCON4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESPROY CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFEMIN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_ESP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_WEB_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_WEB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PER CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_PET CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_POT CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVENOTIF CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE REU_PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_GRUPO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ESP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4_ACT CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATALOG_LIN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_PEDIDO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART_UP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_DEF CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO_DEF CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO_ESP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_GR_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_GR_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ITEM_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ITEM_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_SOBRE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GRUPO_OFE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GRUPO_OFE_REU CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USAR_GR_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_CONTR CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE SOBRE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FAVORITOS_LINEAS_PEDIDO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_NUMDEC CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_ART CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISOR CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--INTEGRACION" & vbCrLf
sConsulta = sConsulta & "IF @INT_ORIGEN <> ''" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & " IF (SELECT SINC_MAT FROM PARGEN_INTERNO)=1" & vbCrLf
sConsulta = sConsulta & "  begin" & vbCrLf
sConsulta = sConsulta & "       SET @IDI=(SELECT IDIOMAPORT FROM PARINS_GEST WHERE USU=@USUARIO)" & vbCrLf
sConsulta = sConsulta & "       SET @SDEN='DEN_' + @IDI" & vbCrLf
sConsulta = sConsulta & "  end" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  begin" & vbCrLf
sConsulta = sConsulta & "       SET @SDEN='DEN'" & vbCrLf
sConsulta = sConsulta & "  end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & " SET @PARAM = N'@VAR_COD VARCHAR(50), @VAR_DEN VARCHAR(100)'" & vbCrLf
sConsulta = sConsulta & " SET @SQL='" & vbCrLf
sConsulta = sConsulta & " DECLARE C CURSOR FOR SELECT COD,' + @SDEN + ' FROM GMN1 WHERE COD=''' + @OLD + '''" & vbCrLf
sConsulta = sConsulta & " OPEN C" & vbCrLf
sConsulta = sConsulta & " FETCH NEXT FROM C INTO @VAR_COD,@VAR_DEN" & vbCrLf
sConsulta = sConsulta & " IF @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & " BEGIN" & vbCrLf
sConsulta = sConsulta & " INSERT INTO LOG_GMN (ACCION,GMN1,DEN,COD_NEW,ORIGEN,USU) VALUES (''C'',@VAR_COD,@VAR_DEN,''' + @NEW + ''',''' + @INT_ORIGEN + ''',''' + @USUARIO + ''')" & vbCrLf
sConsulta = sConsulta & " End" & vbCrLf
sConsulta = sConsulta & " Close C" & vbCrLf
sConsulta = sConsulta & " DEALLOCATE C'" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "  EXEC SP_EXECUTESQL @SQL,@PARAM, @VAR_COD = @VAR_COD, @VAR_DEN=@VAR_DEN" & vbCrLf
sConsulta = sConsulta & " End" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "COMMIT TRANSACTION" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT OFF" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GMN2_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[GMN2_COD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE GMN2_COD (@OLD VARCHAR(50),@NEW VARCHAR(50),@GMN1 VARCHAR(50),@INT_ORIGEN VARCHAR(1),@USUARIO VARCHAR(50))  AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_DEN VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SDEN VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDI VARCHAR (50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PARAM AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT ON" & vbCrLf
sConsulta = sConsulta & "BEGIN TRANSACTION" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART2_ADJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART3_ADJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ADJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ESP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4_ACT NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATALOG_LIN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_PEDIDO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART_UP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FAVORITOS_LINEAS_PEDIDO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_ART NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISOR NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "----" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN2 SET COD=@NEW WHERE GMN1=@GMN1 AND COD=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART2 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART2_ADJ SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART3 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART3_ADJ SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4_ADJ SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE COM_GMN2 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE COM_GMN3 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE COM_GMN4 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN3 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN4 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES_MAT2 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES_MAT3 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES_MAT4 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_GMN2 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_GMN3 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_GMN4 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_PROCE SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART2 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART3 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART4 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4_ESP SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN4_ACT SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4_ATRIB SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN4_ATRIB SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART4_ATRIB SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CATALOG_LIN SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE LINEAS_PEDIDO SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART_UP SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART4_ADJUN SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE FAVORITOS_LINEAS_PEDIDO SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES_ART SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISOR SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "----" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART2_ADJ CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART3_ADJ CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ADJ CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ESP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4_ACT CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATALOG_LIN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_PEDIDO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART_UP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FAVORITOS_LINEAS_PEDIDO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_ART CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISOR CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--INTEGRACION" & vbCrLf
sConsulta = sConsulta & "IF @INT_ORIGEN <> ''" & vbCrLf
sConsulta = sConsulta & "begin" & vbCrLf
sConsulta = sConsulta & "IF (SELECT SINC_MAT FROM PARGEN_INTERNO)=1" & vbCrLf
sConsulta = sConsulta & "  begin" & vbCrLf
sConsulta = sConsulta & "       SET @IDI=(SELECT IDIOMAPORT FROM PARINS_GEST WHERE USU=@USUARIO)" & vbCrLf
sConsulta = sConsulta & "       SET @SDEN='DEN_' + @IDI" & vbCrLf
sConsulta = sConsulta & "  end" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  begin" & vbCrLf
sConsulta = sConsulta & "       SET @SDEN='DEN'" & vbCrLf
sConsulta = sConsulta & "  end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @PARAM = N'@VAR_COD VARCHAR(50), @VAR_DEN VARCHAR(100)'" & vbCrLf
sConsulta = sConsulta & "SET @SQL='" & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR FOR SELECT COD,' + @SDEN + ' FROM GMN2 WHERE GMN1=''' + @GMN1 + ''' AND COD=''' + @OLD + '''" & vbCrLf
sConsulta = sConsulta & "OPEN C" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C INTO @VAR_COD,@VAR_DEN" & vbCrLf
sConsulta = sConsulta & "IF @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO LOG_GMN (ACCION,GMN1,GMN2,DEN,COD_NEW,ORIGEN,USU) VALUES (''C'',''' + @GMN1 + ''' ,@VAR_COD,@VAR_DEN, ''' + @NEW + ''', ''' + @INT_ORIGEN +''',''' + @USUARIO +''')" & vbCrLf
sConsulta = sConsulta & "----" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close C" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL,@PARAM, @VAR_COD = @VAR_COD, @VAR_DEN=@VAR_DEN" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT OFF" & vbCrLf
sConsulta = sConsulta & "COMMIT TRANSACTION" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GMN3_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[GMN3_COD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE GMN3_COD (@OLD VARCHAR(50),@NEW VARCHAR(50),@GMN1 VARCHAR(50),@GMN2 VARCHAR(50),@INT_ORIGEN VARCHAR(1),@USUARIO VARCHAR(50))  AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_DEN VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SDEN VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDI VARCHAR (50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PARAM AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT ON" & vbCrLf
sConsulta = sConsulta & "BEGIN TRANSACTION" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART3_ADJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ADJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ESP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4_ACT NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATALOG_LIN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_PEDIDO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART_UP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FAVORITOS_LINEAS_PEDIDO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_ART NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISOR NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "----" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN3 SET COD=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND COD=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART3 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART3_ADJ SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4_ADJ SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE COM_GMN3 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE COM_GMN4 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN4 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES_MAT3 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES_MAT4 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_GMN3 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_GMN4 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_PROCE SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART3 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART4 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4_ESP SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN4_ACT SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4_ATRIB SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN4_ATRIB SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CATALOG_LIN SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE LINEAS_PEDIDO SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART_UP SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART4_ATRIB SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART4_ADJUN SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE FAVORITOS_LINEAS_PEDIDO SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES_ART SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISOR SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "----" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART3_ADJ CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ADJ CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ESP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4_ACT CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATALOG_LIN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_PEDIDO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART_UP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FAVORITOS_LINEAS_PEDIDO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_ART CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISOR CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--INTEGRACION" & vbCrLf
sConsulta = sConsulta & "IF @INT_ORIGEN <> ''" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & " IF (SELECT SINC_MAT FROM PARGEN_INTERNO)=1" & vbCrLf
sConsulta = sConsulta & "  begin" & vbCrLf
sConsulta = sConsulta & "       SET @IDI=(SELECT IDIOMAPORT FROM PARINS_GEST WHERE USU=@USUARIO)" & vbCrLf
sConsulta = sConsulta & "       SET @SDEN='DEN_' + @IDI" & vbCrLf
sConsulta = sConsulta & "  end" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  begin" & vbCrLf
sConsulta = sConsulta & "       SET @SDEN='DEN'" & vbCrLf
sConsulta = sConsulta & "  end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & " SET @PARAM = N'@VAR_COD VARCHAR(50), @VAR_DEN VARCHAR(100)'" & vbCrLf
sConsulta = sConsulta & " SET @SQL='" & vbCrLf
sConsulta = sConsulta & "  DECLARE C CURSOR FOR SELECT COD,' + @SDEN +  ' FROM GMN3 WHERE GMN1='''  + @GMN1  + ''' AND GMN2=''' + @GMN2  + ''' AND COD= ''' + @OLD + '''" & vbCrLf
sConsulta = sConsulta & "  OPEN C" & vbCrLf
sConsulta = sConsulta & "  FETCH NEXT FROM C INTO @VAR_COD,@VAR_DEN" & vbCrLf
sConsulta = sConsulta & "  IF @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "   begin" & vbCrLf
sConsulta = sConsulta & "     INSERT INTO LOG_GMN (ACCION,GMN1,GMN2,GMN3,DEN,COD_NEW,ORIGEN,USU) VALUES (''C'',''' + @GMN1 + ''',''' + @GMN2  + ''',@VAR_COD,@VAR_DEN ,''' + @NEW + ''',''' + @INT_ORIGEN + ''',''' + @USUARIO + ''')" & vbCrLf
sConsulta = sConsulta & "      ----" & vbCrLf
sConsulta = sConsulta & "   End" & vbCrLf
sConsulta = sConsulta & "  Close C" & vbCrLf
sConsulta = sConsulta & "  DEALLOCATE C'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL,@PARAM, @VAR_COD = @VAR_COD, @VAR_DEN=@VAR_DEN" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "COMMIT TRANSACTION" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT OFF" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GMN4_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[GMN4_COD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE GMN4_COD (@OLD VARCHAR(50),@NEW VARCHAR(50),@GMN1 VARCHAR(50),@GMN2 VARCHAR(50),@GMN3 VARCHAR(50),@INT_ORIGEN VARCHAR(1),@USUARIO VARCHAR(50))  AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_DEN VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SDEN VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDI VARCHAR (50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PARAM AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT ON" & vbCrLf
sConsulta = sConsulta & "BEGIN TRANSACTION" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ADJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ESP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4_ACT NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATALOG_LIN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_PEDIDO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART_UP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FAVORITOS_LINEAS_PEDIDO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_ART NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISOR NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "----" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN4 SET COD=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND COD=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4 SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4_ADJ SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE COM_GMN4 SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES_MAT4 SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_GMN4 SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_PROCE SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART4 SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4_ESP SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN4_ACT SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART4_ATRIB SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN4_ATRIB SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4_ATRIB SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CATALOG_LIN SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE LINEAS_PEDIDO SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART_UP SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART4_ADJUN SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE FAVORITOS_LINEAS_PEDIDO SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES_ART SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISOR SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD" & vbCrLf
sConsulta = sConsulta & "----" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ADJ CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ESP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4_ACT CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATALOG_LIN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_PEDIDO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART_UP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FAVORITOS_LINEAS_PEDIDO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_ART CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISOR CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--INTEGRACION" & vbCrLf
sConsulta = sConsulta & "IF @INT_ORIGEN <> ''" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF (SELECT SINC_MAT FROM PARGEN_INTERNO)=1" & vbCrLf
sConsulta = sConsulta & "  begin" & vbCrLf
sConsulta = sConsulta & "       SET @IDI=(SELECT IDIOMAPORT FROM PARINS_GEST WHERE USU=@USUARIO)" & vbCrLf
sConsulta = sConsulta & "       SET @SDEN='DEN_' + @IDI" & vbCrLf
sConsulta = sConsulta & "  end" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  begin" & vbCrLf
sConsulta = sConsulta & "       SET @SDEN='DEN'" & vbCrLf
sConsulta = sConsulta & "  end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & " SET @PARAM = N'@VAR_COD VARCHAR(50), @VAR_DEN VARCHAR(100)'" & vbCrLf
sConsulta = sConsulta & " SET @SQL='" & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR FOR SELECT COD,'  + @SDEN + ' FROM GMN4 WHERE GMN1=''' + @GMN1 + ''' AND GMN2=''' + @GMN2 + ''' AND GMN3=''' + @GMN3  + ''' AND COD=''' + @OLD + '''" & vbCrLf
sConsulta = sConsulta & "OPEN C" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C INTO @VAR_COD,@VAR_DEN" & vbCrLf
sConsulta = sConsulta & "IF @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "INSERT INTO LOG_GMN (ACCION,GMN1,GMN2,GMN3,GMN4,DEN,COD_NEW,ORIGEN,USU) VALUES (''C'',''' + @GMN1 + ''',''' + @GMN2 + ''',''' + @GMN3 + ''', @VAR_COD,@VAR_DEN,''' + @NEW + ''',''' + @INT_ORIGEN + ''',''' + @USUARIO + ''')" & vbCrLf
sConsulta = sConsulta & "----" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close C" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL,@PARAM, @VAR_COD = @VAR_COD, @VAR_DEN=@VAR_DEN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT OFF" & vbCrLf
sConsulta = sConsulta & "COMMIT TRANSACTION" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub
Public Function CodigoDeActualizacion2_16_05_06A2_16_05_07() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim iRespuesta As Integer

On Error GoTo Error
sConsulta = "BEGIN TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = True
ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

frmProgreso.lblDetalle = "A�adimos campos"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_16_5_Datos_7

frmProgreso.lblDetalle = "Modificar trigger"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_16_5_Triggers_7

frmProgreso.lblDetalle = "Modificar stored procedure"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1


V_2_16_5_Storeds_7

sConsulta = "UPDATE VERSION SET NUM ='2.16.05.07'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "COMMIT TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = False

CodigoDeActualizacion2_16_05_06A2_16_05_07 = True
Exit Function
    
Error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_16_05_06A2_16_05_07 = False

End Function

Private Sub V_2_16_5_Datos_7()
Dim sConsulta As String

sConsulta = "UPDATE PLANTILLA SET SUBASTA = P.SUBASTA, SUBASTAPROVE = P.SUBASTA_PROVE,  SUBASTAPUJAS = P.SUBASTA_PUJAS, SUBASTAESPERA=P.SUBASTA_ESPERA,  OFE_ADJUN= P.OFE_ADJUN, GRUPO_ADJUN = P.GRUPO_ADJUN, ITEM_ADJUN= P.ITEM_ADJUN, SOLCANTMAX = P.SOLCANTMAX, PUBLICARFINSUM = P.PUBLICARFINSUM, PONDERAR =P.PROCE_POND, PRECALTER = P.PRECALTER, UNSOLOPEDIDO = P.UNSOLOPEDIDO, SUBASTABAJMINPUJA = P.SUBASTA_BAJMINPUJA FROM PARGEN_GEST P "
ExecuteSQL gRDOCon, sConsulta

sConsulta = "UPDATE PROCE_OFE SET USU=' ' WHERE USU IS NULL"
ExecuteSQL gRDOCon, sConsulta

    sConsulta = "UPDATE PROCE_OFE SET PORTAL=1 FROM PROCE_OFE PO INNER JOIN PROCE_OFE_WEB PW ON PO.anyo= PW.anyo AND PO.gmn1=PW.gmn1 AND"
    sConsulta = sConsulta & " PO.Proce = PW.Proce AND PO.prove = PW.prove And po.ofe = pw.ofe And po.Portal = 0"
    ExecuteSQL gRDOCon, sConsulta

End Sub

Private Sub V_2_16_5_Triggers_7()
Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PROCE_PEDIDO_TG_INSUPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PROCE_PEDIDO_TG_INSUPD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TRIGGER PROCE_PEDIDO_TG_INSUPD ON [dbo].[PROCE_PEDIDO] " & vbCrLf
sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PEDIDO SET FECACT = GETDATE() FROM PROCE_PEDIDO PP INNER JOIN INSERTED I ON PP.ANYO = I.ANYO AND PP.GMN1 = I.GMN1 AND PP.PROCE = I.PROCE AND PP.PROVE = I.PROVE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Private Sub V_2_16_5_Storeds_7()

Dim sConsulta As String

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PROVE_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[PROVE_COD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE PROVE_COD (@OLD VARCHAR(50),@NEW VARCHAR(50))  AS " & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_COD VARCHAR(50) " & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT ON " & vbCrLf
sConsulta = sConsulta & "BEGIN TRANSACTION " & vbCrLf
sConsulta = sConsulta & "BEGIN " & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR FOR SELECT COD FROM PROVE WHERE COD=@OLD " & vbCrLf
sConsulta = sConsulta & "OPEN C " & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C INTO @VAR_COD " & vbCrLf
sConsulta = sConsulta & "IF @@FETCH_STATUS=0 " & vbCrLf
sConsulta = sConsulta & "BEGIN " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CON NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART1_ADJ NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART2_ADJ NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART3_ADJ NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ADJ NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ADJ NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ADJREU NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OFE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OFE_WEB NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE DETALLE_PUJAS NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFEMIN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_ADJUN NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_WEB_ADJUN NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_WEB NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_PET NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_POT NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_EQP NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN1 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN2 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN3 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN4 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4 NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVENOTIF NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ATRIB NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATALOG_LIN NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ORDEN_ENTREGA NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_PEDIDO NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART_UP NOCHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ESP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ESP_PORTAL NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ITEM_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_GR_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ITEM_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_GR_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GRUPO_OFE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GRUPO_OFE_REU NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_CONTR NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FAVORITOS_ORDEN_ENTREGA NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FAVORITOS_LINEAS_PEDIDO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATN1_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATN2_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATN3_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATN4_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATN5_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_EMPRESA_COD_ERP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE SET COD=@NEW WHERE COD=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE CON SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_PROVE SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART1_ADJ SET CODPROVE=@NEW WHERE CODPROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ART2_ADJ SET CODPROVE=@NEW WHERE CODPROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ART3_ADJ SET CODPROVE=@NEW WHERE CODPROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ART4_ADJ SET CODPROVE=@NEW WHERE CODPROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM SET PROVEACT=@NEW WHERE PROVEACT=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_ADJ SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_ADJREU SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_OFE SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_OFE_WEB SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE DETALLE_PUJAS SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFEMIN SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE_ADJUN SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE_WEB_ADJUN SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE_WEB SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE_PET SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE_POT SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_EQP SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_GMN1 SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_GMN2 SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_GMN3 SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_GMN4 SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART1 SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART2 SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART3 SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART4 SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVENOTIF SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE USU SET PROVE=@NEW WHERE PROVE=@OLD " & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART4_ATRIB SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CATALOG_LIN SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ORDEN_ENTREGA SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE LINEAS_PEDIDO SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART_UP SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ESP SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ESP_PORTAL SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET PROVEACT=@NEW WHERE PROVEACT=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GRUPO SET PROVEACT=@NEW WHERE PROVEACT=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ALL_PROVE SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_GRUPO_PROVE SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ITEM_PROVE SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_PROCE_PROVE SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_ATRIB SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_ITEM_ATRIB SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_GR_ATRIB SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_ITEM_ADJUN SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_GR_ADJUN SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GRUPO_OFE SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GRUPO_OFE_REU SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_PROVE SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_PROVE SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_CONTR SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "--2.12" & vbCrLf
sConsulta = sConsulta & "UPDATE CATN1_PROVE  SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CATN2_PROVE  SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CATN3_PROVE  SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CATN4_PROVE  SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CATN5_PROVE  SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART4_ADJUN  SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE FAVORITOS_ORDEN_ENTREGA  SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE FAVORITOS_LINEAS_PEDIDO  SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_EMPRESA_COD_ERP SET PROVE=@NEW WHERE PROVE=@OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CON CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART1_ADJ CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART2_ADJ CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART3_ADJ CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ADJ CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ADJ CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ADJREU CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OFE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OFE_WEB CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE DETALLE_PUJAS CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFEMIN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_ADJUN CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_WEB_ADJUN CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_WEB CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_PET CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_POT CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_EQP CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN1 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN2 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN3 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN4 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USU CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4 CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVENOTIF CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ATRIB CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATALOG_LIN CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ORDEN_ENTREGA CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_PEDIDO CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART_UP CHECK CONSTRAINT ALL " & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ESP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ESP_PORTAL CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ITEM_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_GR_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ITEM_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_GR_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GRUPO_OFE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GRUPO_OFE_REU CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_CONTR CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FAVORITOS_ORDEN_ENTREGA CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FAVORITOS_LINEAS_PEDIDO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATN1_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATN2_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATN3_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATN4_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATN5_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_EMPRESA_COD_ERP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "End " & vbCrLf
sConsulta = sConsulta & "Close C " & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C " & vbCrLf
sConsulta = sConsulta & "End " & vbCrLf
sConsulta = sConsulta & "COMMIT TRANSACTION" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT OFF" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub


Public Function CodigoDeActualizacion2_16_05_07A2_16_05_08() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim iRespuesta As Integer

On Error GoTo Error
sConsulta = "BEGIN TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = True
ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"


frmProgreso.lblDetalle = "Modificar trigger"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1

V_2_16_5_Storeds_8_1

frmProgreso.lblDetalle = "Modificar stored procedure"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1


V_2_16_5_Storeds_8

sConsulta = "UPDATE VERSION SET NUM ='2.16.05.08'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "COMMIT TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = False

CodigoDeActualizacion2_16_05_07A2_16_05_08 = True
Exit Function
    
Error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_16_05_07A2_16_05_08 = False

End Function

Private Sub V_2_16_5_Storeds_8_1()
Dim sConsulta As String

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_DEVOLVER_DATOS_PROCE_PROVE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_DEVOLVER_DATOS_PROCE_PROVE]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_DEVOLVER_DATOS_PROCE_PROVE (@ANYO INT, @GMN1 VARCHAR(50), @PROCE INT, @PROVE VARCHAR(50), @EMAIL VARCHAR(200))  AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "declare @sync tinyint" & vbCrLf
sConsulta = sConsulta & "declare @pparam AS NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "declare @PORTAL AS NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "declare @sql as nvarchar(4000)" & vbCrLf
sConsulta = sConsulta & "declare @psql as nvarchar(4000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "set @sync = (select sinc_mat from pargen_interno)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "set @sql = '" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT PROCE.DEN AS PROCE_DEN, " & vbCrLf
sConsulta = sConsulta & "       PROCE.GMN2 AS PROCE_GMN2, " & vbCrLf
sConsulta = sConsulta & "       PROCE.GMN3 AS PROCE_GMN3, " & vbCrLf
sConsulta = sConsulta & "       PROCE.GMN4 AS PROCE_GMN4, '" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "set @psql = N'" & vbCrLf
sConsulta = sConsulta & "declare  cIdioma cursor local for select cod from idi" & vbCrLf
sConsulta = sConsulta & "set @idis=N''''" & vbCrLf
sConsulta = sConsulta & "open cIdioma " & vbCrLf
sConsulta = sConsulta & "declare @cod varchar(50)" & vbCrLf
sConsulta = sConsulta & "fetch next from cIdioma into @cod" & vbCrLf
sConsulta = sConsulta & "while @@fetch_status=0" & vbCrLf
sConsulta = sConsulta & "  begin" & vbCrLf
sConsulta = sConsulta & "    set @idis = @idis + " & vbCrLf
sConsulta = sConsulta & "N''" & vbCrLf
sConsulta = sConsulta & "       case when @sync=1 then GMN1.DEN_'' + @cod + '' else GMN1.DEN_SPA END AS PROCE_GMN1_DEN_'' + @cod  + '', " & vbCrLf
sConsulta = sConsulta & "       case when @sync=1 then GMN2.DEN_'' + @cod + '' else GMN2.DEN_SPA END AS PROCE_GMN2_DEN_'' + @cod + ''," & vbCrLf
sConsulta = sConsulta & "       case when @sync=1 then GMN3.DEN_'' + @cod + '' else GMN3.DEN_SPA END AS PROCE_GMN3_DEN_'' + @cod + '', " & vbCrLf
sConsulta = sConsulta & "       case when @sync=1 then GMN4.DEN_'' + @cod + '' else GMN4.DEN_SPA END AS PROCE_GMN4_DEN_'' + @cod + ''," & vbCrLf
sConsulta = sConsulta & "''" & vbCrLf
sConsulta = sConsulta & "fetch next from cIdioma into @cod" & vbCrLf
sConsulta = sConsulta & "end" & vbCrLf
sConsulta = sConsulta & "close cIdioma" & vbCrLf
sConsulta = sConsulta & "deallocate cIdioma" & vbCrLf
sConsulta = sConsulta & "'" & vbCrLf
sConsulta = sConsulta & "declare @idis as nvarchar(4000) " & vbCrLf
sConsulta = sConsulta & "set @pparam = N'@idis as nvarchar(4000) output'" & vbCrLf
sConsulta = sConsulta & "SELECT @PORTAL = FSP_SRV + '.' +  FSP_BD + '.dbo.'  FROM PARGEN_PORT " & vbCrLf
sConsulta = sConsulta & "SET @PORTAL = @PORTAL + 'sp_executesql '" & vbCrLf
sConsulta = sConsulta & "EXEC @PORTAL @psql, @pparam, @idis = @idis output" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "set @sql = @sql + @idis + N'" & vbCrLf
sConsulta = sConsulta & "       CONVERT(VARCHAR,PROCE.FECINISUB,120) AS PROCE_FECINISUB, CONVERT(VARCHAR,PROCE.FECLIMOFE,120) AS PROCE_FECLIMOFE, PROCE_PROVE.EQP AS PROCE_EQP, PROCE_PROVE.COM AS PROCE_COM," & vbCrLf
sConsulta = sConsulta & "       COM.NOM AS COM_NOM, COM.APE AS COM_APE, COM.EMAIL AS COM_EMAIL, COM.TFNO AS COM_TFNO, COM.TFNO2 AS COM_TFNO2," & vbCrLf
sConsulta = sConsulta & "       COM.FAX AS COM_FAX, COM.CAR AS COM_CAR, CON.NOM AS CON_NOM, CON.APE AS CON_APE, CON.CAR AS CON_CAR, CON.TFNO AS CON_TFNO," & vbCrLf
sConsulta = sConsulta & "       CON.TFNO2 AS CON_TFNO2, CON.FAX AS CON_FAX, CON.DEP AS CON_DEP, CON.IDI AS CON_IDI, CON.TIPOEMAIL AS CON_TIPOEMAIL" & vbCrLf
sConsulta = sConsulta & "       FROM PROCE" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN (PROCE_PROVE " & vbCrLf
sConsulta = sConsulta & "                   INNER JOIN COM " & vbCrLf
sConsulta = sConsulta & "                           ON proce_prove.EQP=COM.EQP " & vbCrLf
sConsulta = sConsulta & "                          AND proce_prove.COM=COM.COD)" & vbCrLf
sConsulta = sConsulta & "               ON PROCE.ANYO=PROCE_PROVE.ANYO " & vbCrLf
sConsulta = sConsulta & "              AND PROCE.GMN1=PROCE_PROVE.GMN1 " & vbCrLf
sConsulta = sConsulta & "              AND PROCE.COD=PROCE_PROVE.PROCE" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN CON " & vbCrLf
sConsulta = sConsulta & "               ON PROCE_PROVE.PROVE=CON.PROVE" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN GMN1 " & vbCrLf
sConsulta = sConsulta & "               ON PROCE.GMN1=GMN1.COD" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN GMN2 " & vbCrLf
sConsulta = sConsulta & "               ON GMN2.GMN1=GMN1.COD AND PROCE.GMN2=GMN2.COD" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN GMN3 " & vbCrLf
sConsulta = sConsulta & "               ON GMN3.GMN1=GMN2.GMN1 AND GMN3.GMN2=GMN2.COD AND PROCE.GMN3=GMN3.COD" & vbCrLf
sConsulta = sConsulta & "       INNER JOIN GMN4 " & vbCrLf
sConsulta = sConsulta & "               ON GMN4.GMN1=GMN3.GMN1 AND GMN4.GMN2=GMN3.GMN2 AND GMN4.GMN3=GMN3.COD AND PROCE.GMN4=GMN4.COD" & vbCrLf
sConsulta = sConsulta & "       WHERE PROCE.ANYO=@ANYO " & vbCrLf
sConsulta = sConsulta & "         AND PROCE.GMN1=@GMN1 " & vbCrLf
sConsulta = sConsulta & "         AND PROCE.COD=@PROCE " & vbCrLf
sConsulta = sConsulta & "         AND PROCE_PROVE.PROVE=@PROVE" & vbCrLf
sConsulta = sConsulta & "         AND CON.EMAIL=@EMAIL" & vbCrLf
sConsulta = sConsulta & "'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @PARAM NVARCHAR(2000)" & vbCrLf
sConsulta = sConsulta & "SET @PARAM = N'@ANYO INT, @GMN1 VARCHAR(50), @PROCE INT, @PROVE VARCHAR(50), @EMAIL VARCHAR(200), @SYNC TINYINT'" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL, @PARAM, @ANYO =@ANYO , @GMN1 = @GMN1 , @PROCE =@PROCE ,@PROVE = @PROVE , @EMAIL = @EMAIL, @SYNC = @SYNC" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_RECORDATORIO_SUBASTA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_RECORDATORIO_SUBASTA]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_RECORDATORIO_SUBASTA (@FECHA_ACTUAL DATETIME, @ANTELACION INT) AS" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SELECT PROCE_PROVE.PROVE, PROCE.ANYO, PROCE.GMN1, PROCE.COD,  PROCE.FECINISUB, " & vbCrLf
sConsulta = sConsulta & "  PROCE.GMN2 AS GMN2_COD, PROCE.GMN3 AS GMN3_COD, PROCE.GMN4 AS GMN4_COD," & vbCrLf
sConsulta = sConsulta & "  GMN1.DEN_SPA AS GMN1_DEN,  GMN2.DEN_SPA AS GMN2_DEN,  GMN3.DEN_SPA AS GMN3_DEN,  GMN4.DEN_SPA AS GMN4_DEN," & vbCrLf
sConsulta = sConsulta & "  COM.NOM AS NOM_COM, COM.APE AS APE_COM," & vbCrLf
sConsulta = sConsulta & "  COM.EMAIL AS EMAIL_COM, COM.TFNO AS TFNO_COM, COM.TFNO2 AS TFNO2_COM," & vbCrLf
sConsulta = sConsulta & "  COM.FAX AS FAX_COM, COM.CAR AS CAR_COM, PROVE.COD AS PROVE_COD," & vbCrLf
sConsulta = sConsulta & "  PROVE.DEN AS PROVE_DEN, PROCE.DEN AS PROCE_DEN," & vbCrLf
sConsulta = sConsulta & "  CONVERT(VARCHAR,PROCE.FECINISUB,120) AS PROCE_FECINISUB," & vbCrLf
sConsulta = sConsulta & "  CONVERT(VARCHAR,PROCE.FECLIMOFE,120) AS PROCE_FECLIMOFE" & vbCrLf
sConsulta = sConsulta & "FROM PROCE_DEF" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PROCE ON PROCE.GMN1=PROCE_DEF.GMN1 AND PROCE.ANYO=PROCE_DEF.ANYO AND PROCE.COD=PROCE_DEF.PROCE" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PROCE_PROVE ON PROCE_PROVE.GMN1=PROCE.GMN1 AND PROCE_PROVE.ANYO=PROCE.ANYO AND" & vbCrLf
sConsulta = sConsulta & "                          PROCE_PROVE.PROCE=PROCE.COD INNER JOIN GMN1 ON PROCE.GMN1=GMN1.COD" & vbCrLf
sConsulta = sConsulta & "INNER JOIN GMN2 ON PROCE.GMN1=GMN2.GMN1 AND PROCE.GMN2=GMN2.COD" & vbCrLf
sConsulta = sConsulta & "INNER JOIN GMN3 ON PROCE.GMN1=GMN3.GMN1 AND PROCE.GMN2=GMN3.GMN2 AND PROCE.GMN3=GMN3.COD" & vbCrLf
sConsulta = sConsulta & "INNER JOIN GMN4 ON PROCE.GMN1=GMN4.GMN1 AND PROCE.GMN2=GMN4.GMN2 AND PROCE.GMN3=GMN4.GMN3 AND" & vbCrLf
sConsulta = sConsulta & "                   PROCE.GMN4=GMN4.COD" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN COM ON PROCE.EQP=COM.EQP AND PROCE.COM=COM.COD" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PROVE ON PROCE_PROVE.PROVE=PROVE.COD" & vbCrLf
sConsulta = sConsulta & "WHERE PROCE_DEF.SUBASTA = 1 AND PROCE_DEF.SUBASTANOTIF = 0 AND PROCE_PROVE.PUB=1 AND NOT PROCE.FECINISUB IS NULL AND NOT PROCE.FECLIMOFE IS NULL AND DATEADD(mi,-@ANTELACION,PROCE.FECINISUB)<=@FECHA_ACTUAL AND @FECHA_ACTUAL<=PROCE.FECINISUB" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_NOTIF_DESPUBLICACION]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_NOTIF_DESPUBLICACION]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_NOTIF_DESPUBLICACION (@ANTELACION TINYINT, @NOHANOFERTADO tinyint, @NOVANAOFERTAR tinyint) AS" & vbCrLf
sConsulta = sConsulta & "SELECT PP.PROVE,  P.ANYO, P.GMN1, P.COD,  P.FECLIMOFE, P.GMN2 AS GMN2_COD, P.GMN3 AS GMN3_COD, P.GMN4 AS GMN4_COD, " & vbCrLf
sConsulta = sConsulta & "GMN1.DEN_SPA AS GMN1_DEN,  GMN2.DEN_SPA AS GMN2_DEN,  GMN3.DEN_SPA AS GMN3_DEN,  GMN4.DEN_SPA AS GMN4_DEN, " & vbCrLf
sConsulta = sConsulta & "COM.NOM AS NOM_COM,  COM.APE AS APE_COM,  COM.EMAIL AS EMAIL_COM,  COM.TFNO AS TFNO_COM,   COM.TFNO2 AS TFNO2_COM,  COM.FAX AS FAX_COM, " & vbCrLf
sConsulta = sConsulta & "COM.CAR AS CAR_COM, PROVE.COD AS PROVE_COD,  PROVE.DEN AS PROVE_DEN, P.DEN AS PROCE_DEN,  " & vbCrLf
sConsulta = sConsulta & "CONVERT(VARCHAR,P.FECINISUB,120) AS PROCE_FECINISUB,  CONVERT(VARCHAR,P.FECLIMOFE,120) AS PROCE_FECLIMOFE" & vbCrLf
sConsulta = sConsulta & "FROM PROCE_DEF PD" & vbCrLf
sConsulta = sConsulta & "     INNER JOIN PROCE P " & vbCrLf
sConsulta = sConsulta & "             ON P.GMN1=PD.GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND P.ANYO=PD.ANYO " & vbCrLf
sConsulta = sConsulta & "            AND P.COD=PD.PROCE" & vbCrLf
sConsulta = sConsulta & "     INNER JOIN PROCE_PROVE PP " & vbCrLf
sConsulta = sConsulta & "             ON PP.GMN1=P.GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND PP.ANYO=P.ANYO " & vbCrLf
sConsulta = sConsulta & "            AND PP.PROCE=P.COD" & vbCrLf
sConsulta = sConsulta & "     INNER JOIN GMN1 " & vbCrLf
sConsulta = sConsulta & "             ON P.GMN1=GMN1.COD" & vbCrLf
sConsulta = sConsulta & "     INNER JOIN GMN2 " & vbCrLf
sConsulta = sConsulta & "             ON P.GMN1=GMN2.GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND P.GMN2=GMN2.COD" & vbCrLf
sConsulta = sConsulta & "     INNER JOIN GMN3 " & vbCrLf
sConsulta = sConsulta & "             ON P.GMN1=GMN3.GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND P.GMN2=GMN3.GMN2 " & vbCrLf
sConsulta = sConsulta & "            AND P.GMN3=GMN3.COD" & vbCrLf
sConsulta = sConsulta & "     INNER JOIN GMN4 " & vbCrLf
sConsulta = sConsulta & "             ON P.GMN1=GMN4.GMN1 " & vbCrLf
sConsulta = sConsulta & "            AND P.GMN2=GMN4.GMN2 " & vbCrLf
sConsulta = sConsulta & "            AND P.GMN3=GMN4.GMN3 " & vbCrLf
sConsulta = sConsulta & "            AND P.GMN4=GMN4.COD" & vbCrLf
sConsulta = sConsulta & "      LEFT JOIN COM " & vbCrLf
sConsulta = sConsulta & "             ON PP.EQP=COM.EQP " & vbCrLf
sConsulta = sConsulta & "            AND PP.COM=COM.COD" & vbCrLf
sConsulta = sConsulta & "     INNER JOIN PROVE " & vbCrLf
sConsulta = sConsulta & "             ON PP.PROVE=PROVE.COD" & vbCrLf
sConsulta = sConsulta & " WHERE PD.AVISARDESPUB = 1" & vbCrLf
sConsulta = sConsulta & "   AND PP.PUB=1 " & vbCrLf
sConsulta = sConsulta & "   AND PP.AVISADODESPUB = 0" & vbCrLf
sConsulta = sConsulta & "   AND NOT P.FECLIMOFE IS NULL " & vbCrLf
sConsulta = sConsulta & "   AND DATEADD(day,-@ANTELACION,P.FECLIMOFE) < GETDATE()" & vbCrLf
sConsulta = sConsulta & "   AND GETDATE()<=P.FECLIMOFE" & vbCrLf
sConsulta = sConsulta & "   AND PP.NO_OFE = CASE WHEN @NOVANAOFERTAR=1 THEN 0 ELSE PP.NO_OFE END" & vbCrLf
sConsulta = sConsulta & "   AND PP.OFE = CASE WHEN @NOHANOFERTADO = 1 THEN 0 ELSE PP.OFE END" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_NOTIF_FIN_SUBASTA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_NOTIF_FIN_SUBASTA]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_NOTIF_FIN_SUBASTA (@FECHA_ACTUAL DATETIME) AS" & vbCrLf
sConsulta = sConsulta & "SELECT PROCE_PROVE.PROVE, PROCE.ANYO, PROCE.GMN1, PROCE.COD,  PROCE.FECLIMOFE, " & vbCrLf
sConsulta = sConsulta & "  PROCE.GMN2 AS GMN2_COD, PROCE.GMN3 AS GMN3_COD, PROCE.GMN4 AS GMN4_COD," & vbCrLf
sConsulta = sConsulta & "  GMN1.DEN_SPA AS GMN1_DEN, GMN2.DEN_SPA AS GMN2_DEN, GMN3.DEN_SPA AS GMN3_DEN," & vbCrLf
sConsulta = sConsulta & "  GMN4.DEN_SPA AS GMN4_DEN, COM.NOM AS NOM_COM, COM.APE AS APE_COM," & vbCrLf
sConsulta = sConsulta & "  COM.EMAIL AS EMAIL_COM, COM.TFNO AS TFNO_COM, COM.TFNO2 AS TFNO2_COM," & vbCrLf
sConsulta = sConsulta & "  COM.FAX AS FAX_COM, COM.CAR AS CAR_COM, PROVE.COD AS PROVE_COD," & vbCrLf
sConsulta = sConsulta & "  PROVE.DEN AS PROVE_DEN, PROCE.DEN AS PROCE_DEN," & vbCrLf
sConsulta = sConsulta & "  CONVERT(VARCHAR,PROCE.FECINISUB,120) AS PROCE_FECINISUB," & vbCrLf
sConsulta = sConsulta & "  CONVERT(VARCHAR,PROCE.FECLIMOFE,120) AS PROCE_FECLIMOFE" & vbCrLf
sConsulta = sConsulta & "FROM PROCE_DEF" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PROCE ON PROCE.GMN1=PROCE_DEF.GMN1 AND PROCE.ANYO=PROCE_DEF.ANYO AND PROCE.COD=PROCE_DEF.PROCE" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PROCE_PROVE ON PROCE_PROVE.GMN1=PROCE.GMN1 AND PROCE_PROVE.ANYO=PROCE.ANYO AND" & vbCrLf
sConsulta = sConsulta & "                          PROCE_PROVE.PROCE=PROCE.COD" & vbCrLf
sConsulta = sConsulta & "INNER JOIN GMN1 ON PROCE.GMN1=GMN1.COD" & vbCrLf
sConsulta = sConsulta & "INNER JOIN GMN2 ON PROCE.GMN1=GMN2.GMN1 AND PROCE.GMN2=GMN2.COD" & vbCrLf
sConsulta = sConsulta & "INNER JOIN GMN3 ON PROCE.GMN1=GMN3.GMN1 AND PROCE.GMN2=GMN3.GMN2 AND PROCE.GMN3=GMN3.COD" & vbCrLf
sConsulta = sConsulta & "INNER JOIN GMN4 ON PROCE.GMN1=GMN4.GMN1 AND PROCE.GMN2=GMN4.GMN2 AND PROCE.GMN3=GMN4.GMN3 AND" & vbCrLf
sConsulta = sConsulta & "                   PROCE.GMN4=GMN4.COD" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN COM ON PROCE.EQP=COM.EQP AND PROCE.COM=COM.COD" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PROVE ON PROCE_PROVE.PROVE=PROVE.COD" & vbCrLf
sConsulta = sConsulta & "WHERE PROCE_DEF.SUBASTA = 1 AND PROCE_DEF.SUBASTANOTIF < 3 AND PROCE_PROVE.PUB=1 AND NOT PROCE.FECINISUB IS NULL AND NOT PROCE.FECLIMOFE IS NULL AND @FECHA_ACTUAL>=PROCE.FECLIMOFE" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_NOTIF_INICIO_SUBASTA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_NOTIF_INICIO_SUBASTA]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_NOTIF_INICIO_SUBASTA (@FECHA_ACTUAL DATETIME) AS" & vbCrLf
sConsulta = sConsulta & "SELECT PROCE_PROVE.PROVE, PROCE.ANYO, PROCE.GMN1, PROCE.COD,  PROCE.FECINISUB, " & vbCrLf
sConsulta = sConsulta & "  PROCE.GMN2 AS GMN2_COD, PROCE.GMN3 AS GMN3_COD, PROCE.GMN4 AS GMN4_COD," & vbCrLf
sConsulta = sConsulta & "  GMN1.DEN_SPA AS GMN1_DEN, GMN2.DEN_SPA AS GMN2_DEN, GMN3.DEN_SPA AS GMN3_DEN," & vbCrLf
sConsulta = sConsulta & "  GMN4.DEN_SPA AS GMN4_DEN, COM.NOM AS NOM_COM, COM.APE AS APE_COM," & vbCrLf
sConsulta = sConsulta & "  COM.EMAIL AS EMAIL_COM, COM.TFNO AS TFNO_COM, COM.TFNO2 AS TFNO2_COM," & vbCrLf
sConsulta = sConsulta & "  COM.FAX AS FAX_COM, COM.CAR AS CAR_COM, PROVE.COD AS PROVE_COD," & vbCrLf
sConsulta = sConsulta & "  PROVE.DEN AS PROVE_DEN, PROCE.DEN AS PROCE_DEN," & vbCrLf
sConsulta = sConsulta & "  CONVERT(VARCHAR,PROCE.FECINISUB,120) AS PROCE_FECINISUB," & vbCrLf
sConsulta = sConsulta & "  CONVERT(VARCHAR,PROCE.FECLIMOFE,120) AS PROCE_FECLIMOFE" & vbCrLf
sConsulta = sConsulta & "FROM PROCE_DEF" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PROCE ON PROCE.GMN1=PROCE_DEF.GMN1 AND PROCE.ANYO=PROCE_DEF.ANYO AND PROCE.COD=PROCE_DEF.PROCE" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PROCE_PROVE ON PROCE_PROVE.GMN1=PROCE.GMN1 AND PROCE_PROVE.ANYO=PROCE.ANYO AND" & vbCrLf
sConsulta = sConsulta & "                          PROCE_PROVE.PROCE=PROCE.COD INNER JOIN GMN1 ON PROCE.GMN1=GMN1.COD" & vbCrLf
sConsulta = sConsulta & "INNER JOIN GMN2 ON PROCE.GMN1=GMN2.GMN1 AND PROCE.GMN2=GMN2.COD" & vbCrLf
sConsulta = sConsulta & "INNER JOIN GMN3 ON PROCE.GMN1=GMN3.GMN1 AND PROCE.GMN2=GMN3.GMN2 AND PROCE.GMN3=GMN3.COD" & vbCrLf
sConsulta = sConsulta & "INNER JOIN GMN4 ON PROCE.GMN1=GMN4.GMN1 AND PROCE.GMN2=GMN4.GMN2 AND PROCE.GMN3=GMN4.GMN3 AND" & vbCrLf
sConsulta = sConsulta & "                   PROCE.GMN4=GMN4.COD" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN COM ON PROCE.EQP=COM.EQP AND PROCE.COM=COM.COD" & vbCrLf
sConsulta = sConsulta & "INNER JOIN PROVE ON PROCE_PROVE.PROVE=PROVE.COD" & vbCrLf
sConsulta = sConsulta & "WHERE PROCE_DEF.SUBASTA = 1 AND PROCE_DEF.SUBASTANOTIF < 2 AND PROCE_PROVE.PUB=1 AND NOT PROCE.FECINISUB IS NULL AND NOT PROCE.FECLIMOFE IS NULL AND PROCE.FECINISUB<=@FECHA_ACTUAL AND @FECHA_ACTUAL<PROCE.FECLIMOFE" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub
Private Sub V_2_16_5_Storeds_8()
Dim sConsulta As String

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[MAT_RESTR]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[MAT_RESTR]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE MAT_RESTR  (@EQP CHAR(50),@COM CHAR(50),@ORDEN CHAR(3),@USU VARCHAR(50)) AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE @SDEN AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDI AS VARCHAR(50) " & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL nvarchar(4000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF (SELECT SINC_MAT FROM PARGEN_INTERNO)=1" & vbCrLf
sConsulta = sConsulta & "  begin" & vbCrLf
sConsulta = sConsulta & "       SET @IDI=(SELECT IDIOMAPORT FROM PARINS_GEST WHERE USU=@USU)" & vbCrLf
sConsulta = sConsulta & "       SET @SDEN='DEN_' + @IDI" & vbCrLf
sConsulta = sConsulta & "  end" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  begin" & vbCrLf
sConsulta = sConsulta & "       SET @SDEN='DEN_SPA'" & vbCrLf
sConsulta = sConsulta & "  end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @EQP <>'' AND @COM <>'' AND @ORDEN = 'COD' " & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "set @SQL=N'" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT COM_GMN1.GMN1 COD1,GMN1.' + @SDEN + ' DEN1, NULL COD2,NULL DEN2,NULL COD3,NULL DEN3,NULL COD4,NULL DEN4" & vbCrLf
sConsulta = sConsulta & "FROM COM_GMN1 INNER JOIN GMN1 ON COM_GMN1.GMN1 = GMN1.COD WHERE COM_GMN1.EQP = ''' + @EQP  + '''AND COM_GMN1.COM = ''' + @COM + '''" & vbCrLf
sConsulta = sConsulta & "UNION" & vbCrLf
sConsulta = sConsulta & "(SELECT DISTINCT COM_GMN2.GMN1,GMN1.' + @SDEN + ',COM_GMN2.GMN2,GMN2.' + @SDEN + ', NULL,NULL,NULL,NULL FROM COM_GMN2 INNER JOIN GMN2 ON COM_GMN2.GMN1 = GMN2.GMN1 AND COM_GMN2.GMN2  = GMN2.COD" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN GMN1 ON GMN1.COD = COM_GMN2.GMN1 WHERE COM_GMN2.EQP = ''' + @EQP + ''' AND COM_GMN2.COM = ''' + @COM + ''')" & vbCrLf
sConsulta = sConsulta & "UNION" & vbCrLf
sConsulta = sConsulta & "(SELECT DISTINCT COM_GMN3.GMN1,GMN1.' + @SDEN + ',COM_GMN3.GMN2,GMN2.' + @SDEN + ',COM_GMN3.GMN3,GMN3.' + @SDEN + ',NULL,NULL FROM COM_GMN3 INNER JOIN GMN3 ON COM_GMN3.GMN1 = GMN3.GMN1 AND COM_GMN3.GMN2 = GMN3.GMN2 AND COM_GMN3.GMN3 = GMN3.COD" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN GMN1 ON GMN1.COD = COM_GMN3.GMN1" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN GMN2 ON GMN2.COD = COM_GMN3.GMN2  AND GMN2.GMN1= COM_GMN3.GMN1" & vbCrLf
sConsulta = sConsulta & "     WHERE COM_GMN3.EQP = ''' + @EQP + ''' AND COM_GMN3.COM = ''' + @COM + ''')" & vbCrLf
sConsulta = sConsulta & "UNION" & vbCrLf
sConsulta = sConsulta & "(SELECT DISTINCT COM_GMN4.GMN1,GMN1.' + @SDEN + ',COM_GMN4.GMN2,GMN2.' + @SDEN + ',COM_GMN4.GMN3,GMN3.' + @SDEN + ',COM_GMN4.GMN4,GMN4.' + @SDEN + ' FROM COM_GMN4 INNER JOIN GMN4 ON COM_GMN4.GMN1 = GMN4.GMN1 AND COM_GMN4.GMN2 = GMN4.GMN2 AND COM_GMN4.GMN3 = GMN4.GMN3 AND COM_GMN4.GMN4 = GMN4.COD" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN GMN1 ON GMN1.COD = COM_GMN4.GMN1" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN GMN2 ON GMN2.COD = COM_GMN4.GMN2 AND GMN2.GMN1= COM_GMN4.GMN1" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN GMN3 ON GMN3.COD = COM_GMN4.GMN3 AND GMN3.GMN1= COM_GMN4.GMN1 AND GMN3.GMN2 = COM_GMN4.GMN2" & vbCrLf
sConsulta = sConsulta & " WHERE COM_GMN4.EQP = ''' + @EQP + ''' AND COM_GMN4.COM = ''' + @COM + ''') " & vbCrLf
sConsulta = sConsulta & "ORDER BY COD1,COD2,COD3,COD4" & vbCrLf
sConsulta = sConsulta & "'" & vbCrLf
sConsulta = sConsulta & "END " & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @EQP <>'' AND @COM <>'' AND @ORDEN = 'DEN'" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "set @SQL=N'" & vbCrLf
sConsulta = sConsulta & "SELECT DISTINCT COM_GMN1.GMN1 COD1,GMN1.' + @SDEN + ' DEN1, NULL COD2,NULL DEN2,NULL COD3,NULL DEN3,NULL COD4,NULL DEN4" & vbCrLf
sConsulta = sConsulta & "FROM COM_GMN1 INNER JOIN GMN1 ON COM_GMN1.GMN1 = GMN1.COD WHERE COM_GMN1.EQP = ''' + @EQP  + ''' AND COM_GMN1.COM = ''' + @COM + '''" & vbCrLf
sConsulta = sConsulta & "Union" & vbCrLf
sConsulta = sConsulta & "(SELECT DISTINCT COM_GMN2.GMN1,GMN1.' + @SDEN + ',COM_GMN2.GMN2,GMN2.' + @SDEN + ', NULL,NULL,NULL,NULL FROM COM_GMN2 INNER JOIN GMN2 ON COM_GMN2.GMN1 = GMN2.GMN1 AND COM_GMN2.GMN2  = GMN2.COD" & vbCrLf
sConsulta = sConsulta & "        INNER JOIN GMN1 ON GMN1.COD = COM_GMN2.GMN1 WHERE COM_GMN2.EQP = ''' + @EQP + ''' AND COM_GMN2.COM = ''' +  @COM + ''')" & vbCrLf
sConsulta = sConsulta & "Union" & vbCrLf
sConsulta = sConsulta & "(SELECT DISTINCT COM_GMN3.GMN1,GMN1.' + @SDEN + ',COM_GMN3.GMN2,GMN2.' + @SDEN + ',COM_GMN3.GMN3,GMN3.' + @SDEN + ',NULL,NULL FROM COM_GMN3 INNER JOIN GMN3 ON COM_GMN3.GMN1 = GMN3.GMN1 AND COM_GMN3.GMN2 = GMN3.GMN2 AND COM_GMN3.GMN3 = GMN3.COD" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN GMN1 ON GMN1.COD = COM_GMN3.GMN1" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN GMN2 ON GMN2.COD = COM_GMN3.GMN2  AND GMN2.GMN1= COM_GMN3.GMN1" & vbCrLf
sConsulta = sConsulta & "     WHERE COM_GMN3.EQP = ''' + @EQP + ''' AND COM_GMN3.COM = ''' + @COM + ''')" & vbCrLf
sConsulta = sConsulta & "Union" & vbCrLf
sConsulta = sConsulta & "(SELECT DISTINCT COM_GMN4.GMN1,GMN1.' + @SDEN + ',COM_GMN4.GMN2,GMN2.' + @SDEN +',COM_GMN4.GMN3,GMN3.' + @SDEN +',COM_GMN4.GMN4,GMN4.' + @SDEN + ' FROM COM_GMN4 INNER JOIN GMN4 ON COM_GMN4.GMN1 = GMN4.GMN1 AND COM_GMN4.GMN2 = GMN4.GMN2 AND COM_GMN4.GMN3 = GMN4.GMN3 AND COM_GMN4.GMN4 = GMN4.COD" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN GMN1 ON GMN1.COD = COM_GMN4.GMN1" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN GMN2 ON GMN2.COD = COM_GMN4.GMN2 AND GMN2.GMN1= COM_GMN4.GMN1" & vbCrLf
sConsulta = sConsulta & "    INNER JOIN GMN3 ON GMN3.COD = COM_GMN4.GMN3 AND GMN3.GMN1= COM_GMN4.GMN1 AND GMN3.GMN2 = COM_GMN4.GMN2" & vbCrLf
sConsulta = sConsulta & " WHERE COM_GMN4.EQP = ''' + @EQP + ''' AND COM_GMN4.COM = ''' +@COM + ''')" & vbCrLf
sConsulta = sConsulta & "ORDER BY DEN1,DEN2,DEN3,DEN4" & vbCrLf
sConsulta = sConsulta & "'" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @EQP = '' AND @COM = '' AND @ORDEN = 'COD'" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "set @SQL=N'" & vbCrLf
sConsulta = sConsulta & "SELECT GMN1.COD COD1,GMN1.' + @SDEN + ' DEN1 ,GMN2.COD COD2,GMN2.' + @SDEN + ' DEN2,GMN3.COD COD3,GMN3.' + @SDEN + ' DEN3,GMN4.COD COD4,GMN4.' + @SDEN + ' DEN4 FROM GMN1" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN GMN2 ON GMN1.COD = GMN2.GMN1" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN GMN3 ON GMN1.COD = GMN3.GMN1 AND GMN2.COD = GMN3.GMN2" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN GMN4 ON GMN1.COD = GMN4.GMN1 AND GMN2.COD = GMN4.GMN2 AND GMN3.COD = GMN4.GMN3" & vbCrLf
sConsulta = sConsulta & "ORDER BY COD1,COD2,COD3,COD4" & vbCrLf
sConsulta = sConsulta & "'" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "IF @EQP = '' AND @COM = '' AND @ORDEN = 'DEN'" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "set @SQL=N'" & vbCrLf
sConsulta = sConsulta & "SELECT GMN1.COD COD1,GMN1.' + @SDEN + ' DEN1 ,GMN2.COD COD2,GMN2.' + @SDEN + ' DEN2,GMN3.COD COD3,GMN3.' + @SDEN + ' DEN3,GMN4.COD COD4,GMN4.' + @SDEN + ' DEN4 FROM GMN1" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN GMN2 ON GMN1.COD = GMN2.GMN1" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN GMN3 ON GMN1.COD = GMN3.GMN1 AND GMN2.COD = GMN3.GMN2" & vbCrLf
sConsulta = sConsulta & "LEFT JOIN GMN4 ON GMN1.COD = GMN4.GMN1 AND GMN2.COD = GMN4.GMN2 AND GMN3.COD = GMN4.GMN3" & vbCrLf
sConsulta = sConsulta & "ORDER BY DEN1,DEN2,DEN3,DEN4" & vbCrLf
sConsulta = sConsulta & "'" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "EXEC sp_executesql @SQL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GMN1_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[GMN1_COD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE GMN1_COD (@OLD VARCHAR(50),@NEW VARCHAR(50),@INT_ORIGEN VARCHAR(1),@USUARIO VARCHAR(50))  AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_DEN VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SDEN VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDI VARCHAR (50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PARAM AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT ON" & vbCrLf
sConsulta = sConsulta & "BEGIN TRANSACTION" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART1_ADJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART2_ADJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART3_ADJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ADJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE DETALLE_PUJAS NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ADJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ADJREU NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ESP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OBJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OFE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OFE_WEB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRES NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESCON NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESPROY NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESCON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESCON4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFEMIN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_GRUPO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_ESP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_WEB_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_WEB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PER NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_PET NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_POT NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVENOTIF NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE REU_PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ESP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4_ACT NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATALOG_LIN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_PEDIDO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART_UP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_DEF NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO_DEF NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO_ESP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON1 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_GR_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_GR_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ITEM_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ITEM_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_SOBRE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GRUPO_OFE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GRUPO_OFE_REU NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USAR_GR_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_CONTR NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE SOBRE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FAVORITOS_LINEAS_PEDIDO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_NUMDEC NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_ART NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISOR NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN1 SET COD=@NEW WHERE COD=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART1_ADJ SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART2_ADJ SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART3_ADJ SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4_ADJ SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE BLOQUEO_PROCE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE COM_GMN1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE COM_GMN2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE COM_GMN3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE COM_GMN4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE DETALLE_PUJAS SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_ADJ SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_ADJREU SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_ESP SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_OBJ SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_OFE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_OFE_WEB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_PRES SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_PRESCON SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_PRESCON3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_PRESCON4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_PRESPROY SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_UON1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_UON2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM_UON3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFEMIN SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES_MAT1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES_MAT2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES_MAT3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES_MAT4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_ESP SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE_ADJUN SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE_WEB_ADJUN SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_OFE_WEB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PER SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE_PET SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PROVE_POT SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_GMN1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_GMN2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_GMN3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_GMN4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVENOTIF SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE REU_PROCE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_PROCE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_PROVE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_GRUPO SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_UON1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_UON2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_UON3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARCON4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PARPROY4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON3_4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES_PRESCON4_4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_PRESCON3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_PRESCON4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_PROVE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4_ESP  SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN4_ACT SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART4_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN4_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4_ATRIB  SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CATALOG_LIN  SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE LINEAS_PEDIDO  SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART_UP  SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_DEF SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GRUPO SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GRUPO_DEF SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GRUPO_ESP SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PRES1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PRES2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PRES3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_PRES4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_PRES1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_PRES2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_PRES3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_PRES4 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_UON1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_UON2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_GR_UON3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_UON1 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_UON2 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_UON3 SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_GR_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_GR_ADJUN SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_ITEM_ADJUN SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE OFE_ITEM_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ALL SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ALL_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ALL_PROVE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_GRUPO SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_GRUPO_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_GRUPO_PROVE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ITEM SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ITEM_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_ITEM_PROVE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_PROCE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_PROCE_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_PROCE_PROVE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISTAS_PROCE_SOBRE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GRUPO_OFE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GRUPO_OFE_REU SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE USAR_GR_ATRIB SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_CONTR SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE SOBRE SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART4_ADJUN SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE FAVORITOS_LINEAS_PEDIDO SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_NUMDEC SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES_ART SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISOR SET GMN1=@NEW WHERE GMN1=@OLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART1_ADJ CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART2_ADJ CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART3_ADJ CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ADJ CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE DETALLE_PUJAS CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ADJ CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ADJREU CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_ESP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OBJ CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OFE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_OFE_WEB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRES CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESCON CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESCON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESCON4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_PRESPROY CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFEMIN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_ESP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_WEB_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_OFE_WEB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PER CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_PET CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PROVE_POT CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVENOTIF CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE REU_PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_GRUPO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARCON4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PARPROY4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON3_4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES_PRESCON4_4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PRESCON4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ESP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4_ACT CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATALOG_LIN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_PEDIDO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART_UP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_DEF CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO_DEF CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GRUPO_ESP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_PRES4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_PRES4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_GR_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON1 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_UON3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_GR_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_GR_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ITEM_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE OFE_ITEM_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ALL_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_GRUPO_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_ITEM_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISTAS_PROCE_SOBRE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GRUPO_OFE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GRUPO_OFE_REU CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE USAR_GR_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_CONTR CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE SOBRE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FAVORITOS_LINEAS_PEDIDO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE_NUMDEC CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_ART CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISOR CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--INTEGRACION" & vbCrLf
sConsulta = sConsulta & "IF @INT_ORIGEN <> ''" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & " IF (SELECT SINC_MAT FROM PARGEN_INTERNO)=1" & vbCrLf
sConsulta = sConsulta & "  begin" & vbCrLf
sConsulta = sConsulta & "       SET @IDI=(SELECT IDIOMAPORT FROM PARINS_GEST WHERE USU=@USUARIO)" & vbCrLf
sConsulta = sConsulta & "       SET @SDEN='DEN_' + @IDI" & vbCrLf
sConsulta = sConsulta & "  end" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  begin" & vbCrLf
sConsulta = sConsulta & "       SET @SDEN='DEN_SPA'" & vbCrLf
sConsulta = sConsulta & "  end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & " SET @PARAM = N'@VAR_COD VARCHAR(50), @VAR_DEN VARCHAR(100)'" & vbCrLf
sConsulta = sConsulta & " SET @SQL='" & vbCrLf
sConsulta = sConsulta & " DECLARE C CURSOR FOR SELECT COD,' + @SDEN + ' FROM GMN1 WHERE COD=''' + @OLD + '''" & vbCrLf
sConsulta = sConsulta & " OPEN C" & vbCrLf
sConsulta = sConsulta & " FETCH NEXT FROM C INTO @VAR_COD,@VAR_DEN" & vbCrLf
sConsulta = sConsulta & " IF @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & " BEGIN" & vbCrLf
sConsulta = sConsulta & " INSERT INTO LOG_GMN (ACCION,GMN1,DEN,COD_NEW,ORIGEN,USU) VALUES (''C'',@VAR_COD,@VAR_DEN,''' + @NEW + ''',''' + @INT_ORIGEN + ''',''' + @USUARIO + ''')" & vbCrLf
sConsulta = sConsulta & " End" & vbCrLf
sConsulta = sConsulta & " Close C" & vbCrLf
sConsulta = sConsulta & " DEALLOCATE C'" & vbCrLf
sConsulta = sConsulta & " " & vbCrLf
sConsulta = sConsulta & "  EXEC SP_EXECUTESQL @SQL,@PARAM, @VAR_COD = @VAR_COD, @VAR_DEN=@VAR_DEN" & vbCrLf
sConsulta = sConsulta & " End" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "COMMIT TRANSACTION" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT OFF" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GMN2_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[GMN2_COD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE GMN2_COD (@OLD VARCHAR(50),@NEW VARCHAR(50),@GMN1 VARCHAR(50),@INT_ORIGEN VARCHAR(1),@USUARIO VARCHAR(50))  AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_DEN VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SDEN VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDI VARCHAR (50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PARAM AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT ON" & vbCrLf
sConsulta = sConsulta & "BEGIN TRANSACTION" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART2_ADJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART3_ADJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ADJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART2 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ESP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4_ACT NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATALOG_LIN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_PEDIDO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART_UP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FAVORITOS_LINEAS_PEDIDO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_ART NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISOR NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "----" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN2 SET COD=@NEW WHERE GMN1=@GMN1 AND COD=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART2 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART2_ADJ SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART3 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART3_ADJ SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4_ADJ SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE COM_GMN2 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE COM_GMN3 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE COM_GMN4 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN3 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN4 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES_MAT2 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES_MAT3 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES_MAT4 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_GMN2 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_GMN3 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_GMN4 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_PROCE SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART2 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART3 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART4 SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4_ESP SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN4_ACT SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4_ATRIB SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN4_ATRIB SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART4_ATRIB SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CATALOG_LIN SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE LINEAS_PEDIDO SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART_UP SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART4_ADJUN SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE FAVORITOS_LINEAS_PEDIDO SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES_ART SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISOR SET GMN2=@NEW WHERE GMN1=@GMN1 AND GMN2=@OLD" & vbCrLf
sConsulta = sConsulta & "----" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART2_ADJ CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART3_ADJ CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ADJ CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART2 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ESP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4_ACT CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATALOG_LIN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_PEDIDO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART_UP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FAVORITOS_LINEAS_PEDIDO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_ART CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISOR CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--INTEGRACION" & vbCrLf
sConsulta = sConsulta & "IF @INT_ORIGEN <> ''" & vbCrLf
sConsulta = sConsulta & "begin" & vbCrLf
sConsulta = sConsulta & "IF (SELECT SINC_MAT FROM PARGEN_INTERNO)=1" & vbCrLf
sConsulta = sConsulta & "  begin" & vbCrLf
sConsulta = sConsulta & "       SET @IDI=(SELECT IDIOMAPORT FROM PARINS_GEST WHERE USU=@USUARIO)" & vbCrLf
sConsulta = sConsulta & "       SET @SDEN='DEN_' + @IDI" & vbCrLf
sConsulta = sConsulta & "  end" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  begin" & vbCrLf
sConsulta = sConsulta & "       SET @SDEN='DEN_SPA'" & vbCrLf
sConsulta = sConsulta & "  end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET @PARAM = N'@VAR_COD VARCHAR(50), @VAR_DEN VARCHAR(100)'" & vbCrLf
sConsulta = sConsulta & "SET @SQL='" & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR FOR SELECT COD,' + @SDEN + ' FROM GMN2 WHERE GMN1=''' + @GMN1 + ''' AND COD=''' + @OLD + '''" & vbCrLf
sConsulta = sConsulta & "OPEN C" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C INTO @VAR_COD,@VAR_DEN" & vbCrLf
sConsulta = sConsulta & "IF @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "INSERT INTO LOG_GMN (ACCION,GMN1,GMN2,DEN,COD_NEW,ORIGEN,USU) VALUES (''C'',''' + @GMN1 + ''' ,@VAR_COD,@VAR_DEN, ''' + @NEW + ''', ''' + @INT_ORIGEN +''',''' + @USUARIO +''')" & vbCrLf
sConsulta = sConsulta & "----" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close C" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL,@PARAM, @VAR_COD = @VAR_COD, @VAR_DEN=@VAR_DEN" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT OFF" & vbCrLf
sConsulta = sConsulta & "COMMIT TRANSACTION" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GMN3_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[GMN3_COD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE GMN3_COD (@OLD VARCHAR(50),@NEW VARCHAR(50),@GMN1 VARCHAR(50),@GMN2 VARCHAR(50),@INT_ORIGEN VARCHAR(1),@USUARIO VARCHAR(50))  AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_DEN VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SDEN VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDI VARCHAR (50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PARAM AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT ON" & vbCrLf
sConsulta = sConsulta & "BEGIN TRANSACTION" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART3_ADJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ADJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART3 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ESP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4_ACT NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATALOG_LIN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_PEDIDO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART_UP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FAVORITOS_LINEAS_PEDIDO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_ART NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISOR NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "----" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN3 SET COD=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND COD=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART3 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART3_ADJ SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4_ADJ SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE COM_GMN3 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE COM_GMN4 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN4 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES_MAT3 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES_MAT4 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_GMN3 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_GMN4 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_PROCE SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART3 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART4 SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4_ESP SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN4_ACT SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4_ATRIB SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN4_ATRIB SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CATALOG_LIN SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE LINEAS_PEDIDO SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART_UP SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART4_ATRIB SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART4_ADJUN SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE FAVORITOS_LINEAS_PEDIDO SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES_ART SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISOR SET GMN3=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@OLD" & vbCrLf
sConsulta = sConsulta & "----" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART3_ADJ CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ADJ CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART3 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ESP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4_ACT CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATALOG_LIN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_PEDIDO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART_UP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FAVORITOS_LINEAS_PEDIDO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_ART CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISOR CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--INTEGRACION" & vbCrLf
sConsulta = sConsulta & "IF @INT_ORIGEN <> ''" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & " IF (SELECT SINC_MAT FROM PARGEN_INTERNO)=1" & vbCrLf
sConsulta = sConsulta & "  begin" & vbCrLf
sConsulta = sConsulta & "       SET @IDI=(SELECT IDIOMAPORT FROM PARINS_GEST WHERE USU=@USUARIO)" & vbCrLf
sConsulta = sConsulta & "       SET @SDEN='DEN_' + @IDI" & vbCrLf
sConsulta = sConsulta & "  end" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  begin" & vbCrLf
sConsulta = sConsulta & "       SET @SDEN='DEN_SPA'" & vbCrLf
sConsulta = sConsulta & "  end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & " SET @PARAM = N'@VAR_COD VARCHAR(50), @VAR_DEN VARCHAR(100)'" & vbCrLf
sConsulta = sConsulta & " SET @SQL='" & vbCrLf
sConsulta = sConsulta & "  DECLARE C CURSOR FOR SELECT COD,' + @SDEN +  ' FROM GMN3 WHERE GMN1='''  + @GMN1  + ''' AND GMN2=''' + @GMN2  + ''' AND COD= ''' + @OLD + '''" & vbCrLf
sConsulta = sConsulta & "  OPEN C" & vbCrLf
sConsulta = sConsulta & "  FETCH NEXT FROM C INTO @VAR_COD,@VAR_DEN" & vbCrLf
sConsulta = sConsulta & "  IF @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "   begin" & vbCrLf
sConsulta = sConsulta & "     INSERT INTO LOG_GMN (ACCION,GMN1,GMN2,GMN3,DEN,COD_NEW,ORIGEN,USU) VALUES (''C'',''' + @GMN1 + ''',''' + @GMN2  + ''',@VAR_COD,@VAR_DEN ,''' + @NEW + ''',''' + @INT_ORIGEN + ''',''' + @USUARIO + ''')" & vbCrLf
sConsulta = sConsulta & "      ----" & vbCrLf
sConsulta = sConsulta & "   End" & vbCrLf
sConsulta = sConsulta & "  Close C" & vbCrLf
sConsulta = sConsulta & "  DEALLOCATE C'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL,@PARAM, @VAR_COD = @VAR_COD, @VAR_DEN=@VAR_DEN" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "COMMIT TRANSACTION" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT OFF" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GMN4_COD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[GMN4_COD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE GMN4_COD (@OLD VARCHAR(50),@NEW VARCHAR(50),@GMN1 VARCHAR(50),@GMN2 VARCHAR(50),@GMN3 VARCHAR(50),@INT_ORIGEN VARCHAR(1),@USUARIO VARCHAR(50))  AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_COD VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_DEN VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SDEN VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @IDI VARCHAR (50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @SQL AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PARAM AS NVARCHAR(4000)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT ON" & vbCrLf
sConsulta = sConsulta & "BEGIN TRANSACTION" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ADJ NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4 NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ESP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4_ACT NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATALOG_LIN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_PEDIDO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART_UP NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ADJUN NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FAVORITOS_LINEAS_PEDIDO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_ART NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISOR NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "----" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN4 SET COD=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND COD=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4 SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4_ADJ SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE COM_GMN4 SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ITEM SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES_MAT4 SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_GMN4 SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_PROCE SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE AR_ITEM_MES SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART4 SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4_ESP SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN4_ACT SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART4_ATRIB SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE GMN4_ATRIB SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE ART4_ATRIB SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CATALOG_LIN SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE LINEAS_PEDIDO SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART_UP SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PROVE_ART4_ADJUN SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE FAVORITOS_LINEAS_PEDIDO SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE PRES_ART SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD" & vbCrLf
sConsulta = sConsulta & "UPDATE CONF_VISOR SET GMN4=@NEW WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@OLD" & vbCrLf
sConsulta = sConsulta & "----" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ADJ CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE COM_GMN4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ITEM CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_MAT4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_GMN4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE AR_ITEM_MES CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4 CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ESP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4_ACT CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE ART4_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CATALOG_LIN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE LINEAS_PEDIDO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART_UP CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE GMN4_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PROVE_ART4_ADJUN CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE FAVORITOS_LINEAS_PEDIDO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE PRES_ART CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "ALTER TABLE CONF_VISOR CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "--INTEGRACION" & vbCrLf
sConsulta = sConsulta & "IF @INT_ORIGEN <> ''" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF (SELECT SINC_MAT FROM PARGEN_INTERNO)=1" & vbCrLf
sConsulta = sConsulta & "  begin" & vbCrLf
sConsulta = sConsulta & "       SET @IDI=(SELECT IDIOMAPORT FROM PARINS_GEST WHERE USU=@USUARIO)" & vbCrLf
sConsulta = sConsulta & "       SET @SDEN='DEN_' + @IDI" & vbCrLf
sConsulta = sConsulta & "  end" & vbCrLf
sConsulta = sConsulta & "ELSE" & vbCrLf
sConsulta = sConsulta & "  begin" & vbCrLf
sConsulta = sConsulta & "       SET @SDEN='DEN_SPA'" & vbCrLf
sConsulta = sConsulta & "  end" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & " SET @PARAM = N'@VAR_COD VARCHAR(50), @VAR_DEN VARCHAR(100)'" & vbCrLf
sConsulta = sConsulta & " SET @SQL='" & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR FOR SELECT COD,'  + @SDEN + ' FROM GMN4 WHERE GMN1=''' + @GMN1 + ''' AND GMN2=''' + @GMN2 + ''' AND GMN3=''' + @GMN3  + ''' AND COD=''' + @OLD + '''" & vbCrLf
sConsulta = sConsulta & "OPEN C" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C INTO @VAR_COD,@VAR_DEN" & vbCrLf
sConsulta = sConsulta & "IF @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "INSERT INTO LOG_GMN (ACCION,GMN1,GMN2,GMN3,GMN4,DEN,COD_NEW,ORIGEN,USU) VALUES (''C'',''' + @GMN1 + ''',''' + @GMN2 + ''',''' + @GMN3 + ''', @VAR_COD,@VAR_DEN,''' + @NEW + ''',''' + @INT_ORIGEN + ''',''' + @USUARIO + ''')" & vbCrLf
sConsulta = sConsulta & "----" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close C" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C'" & vbCrLf
sConsulta = sConsulta & "EXEC SP_EXECUTESQL @SQL,@PARAM, @VAR_COD = @VAR_COD, @VAR_DEN=@VAR_DEN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "SET XACT_ABORT OFF" & vbCrLf
sConsulta = sConsulta & "COMMIT TRANSACTION" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub


Private Function ProveedoresActividadesPortal(ByVal sProve As String, ByVal lCia As Long)
Dim sConsulta As String
Dim bResultado As Boolean

        'Borramos los materiales que no exist�an en el portal
        sConsulta = "DELETE FROM PROVE_GMN1 WHERE PROVE_GMN1.PROVE ='" & DblQuote(sProve) & "' AND  NOT EXISTS (SELECT * FROM ACIAS_ACT1 AC WHERE AC.CIA=" & lCia & " AND AC.ACT1=PROVE_GMN1.GMN1)"
        oADOConnection.Execute sConsulta
        sConsulta = "DELETE FROM PROVE_GMN2 WHERE PROVE_GMN2.PROVE ='" & DblQuote(sProve) & "' AND  NOT EXISTS (SELECT * FROM ACIAS_ACT2 AC WHERE AC.CIA=" & lCia & " AND AC.ACT1=PROVE_GMN2.GMN1 AND AC.ACT2=PROVE_GMN2.GMN2)"
        oADOConnection.Execute sConsulta
        sConsulta = "DELETE FROM PROVE_GMN3 WHERE PROVE_GMN3.PROVE ='" & DblQuote(sProve) & "' AND  NOT EXISTS (SELECT * FROM ACIAS_ACT3 AC WHERE AC.CIA=" & lCia & " AND AC.ACT1=PROVE_GMN3.GMN1 AND AC.ACT2=PROVE_GMN3.GMN2 AND AC.ACT3=PROVE_GMN3.GMN3)"
        oADOConnection.Execute sConsulta
        sConsulta = "DELETE FROM PROVE_GMN4 WHERE PROVE_GMN4.PROVE ='" & DblQuote(sProve) & "' AND  NOT EXISTS (SELECT * FROM ACIAS_ACT4 AC WHERE AC.CIA=" & lCia & " AND AC.ACT1=PROVE_GMN4.GMN1 AND AC.ACT2=PROVE_GMN4.GMN2 AND AC.ACT3=PROVE_GMN4.GMN3 AND AC.ACT4=PROVE_GMN4.GMN4)"
        oADOConnection.Execute sConsulta
        
        'Insertamos las actividades-proveedores del portal que noe xistan en GS
        bResultado = ProveedoresTodosMateriales(sProve, lCia)
        If Not bResultado Then
            ProveedoresActividadesPortal = False
            Exit Function
        End If
        
ProveedoresActividadesPortal = True
Exit Function
        
Error:
    MsgBox oADOConnection.Errors(0).Description, vbCritical + vbOKOnly
   'Resume Next
    If NumTransaccionesAbiertasADO(oADOConnection) Then
        oADOConnection.Execute "ROLLBACK TRAN"
        oADOConnection.Execute "SET XACT_ABORT OFF"
    End If
    ProveedoresActividadesPortal = False
End Function

Private Function ProveedoresMaterialGS(ByVal sProve As String, ByVal lCia As Long) As Boolean
Dim sConsulta As String
Dim oAdores As ADODB.Recordset

        Set oAdores = New ADODB.Recordset
        'Materiales de nivel 4:
        sConsulta = "SELECT GMN1.ID ACT1, GMN2.ID ACT2, GMN3.ID ACT3, GMN4.ID ACT4 FROM PROVE_GMN4 PG INNER JOIN GMN1 ON PG.GMN1=GMN1.COD"
        sConsulta = sConsulta & " INNER JOIN GMN2 ON PG.GMN1=GMN2.GMN1 AND PG.GMN2=GMN2.COD"
        sConsulta = sConsulta & " INNER JOIN GMN3 ON PG.GMN1=GMN3.GMN1 AND PG.GMN2=GMN3.GMN2 AND PG.GMN3=GMN3.COD"
        sConsulta = sConsulta & " INNER JOIN GMN4 ON PG.GMN1=GMN4.GMN1 AND PG.GMN2=GMN4.GMN2 AND PG.GMN3=GMN4.GMN3 AND PG.GMN4=GMN4.COD"
        sConsulta = sConsulta & " WHERE PG.PROVE='" & DblQuote(sProve) & "'"

        oAdores.Open sConsulta, oADOConnection, adOpenForwardOnly, adLockReadOnly
        While Not oAdores.eof
            sConsulta = "INSERT INTO CIAS_ACT4 (CIA,ACT1,ACT2,ACT3,ACT4) VALUES (" & lCia & "," & oAdores("ACT1").Value & "," & oAdores("ACT2").Value & "," & oAdores("ACT3").Value & "," & oAdores("ACT4").Value & ")"
            oADOConnection.Execute "EXEC " & sFSP & "sp_executesql N'" & sConsulta & "'"
            oAdores.MoveNext
        Wend
        oAdores.Close


        'Actividades de nivel 3:
        sConsulta = "SELECT GMN1.ID ACT1, GMN2.ID ACT2, GMN3.ID ACT3 FROM PROVE_GMN3 PG INNER JOIN GMN1 ON PG.GMN1=GMN1.COD"
        sConsulta = sConsulta & " INNER JOIN GMN2 ON PG.GMN1=GMN2.GMN1 AND PG.GMN2=GMN2.COD"
        sConsulta = sConsulta & " INNER JOIN GMN3 ON PG.GMN1=GMN3.GMN1 AND PG.GMN2=GMN3.GMN2 AND PG.GMN3=GMN3.COD"
        sConsulta = sConsulta & " WHERE PG.PROVE='" & DblQuote(sProve) & "'"

        oAdores.Open sConsulta, oADOConnection, adOpenForwardOnly, adLockReadOnly
        While Not oAdores.eof
            sConsulta = "INSERT INTO CIAS_ACT3 (CIA,ACT1,ACT2,ACT3) VALUES (" & lCia & "," & oAdores("ACT1").Value & "," & oAdores("ACT2").Value & "," & oAdores("ACT3").Value & ")"
            oADOConnection.Execute "EXEC " & sFSP & "sp_executesql N'" & sConsulta & "'"
            oAdores.MoveNext
        Wend
        oAdores.Close

        'Actividades de nivel 2:
        sConsulta = "SELECT GMN1.ID ACT1, GMN2.ID ACT2 FROM PROVE_GMN2 PG INNER JOIN GMN1 ON PG.GMN1=GMN1.COD"
        sConsulta = sConsulta & " INNER JOIN GMN2 ON PG.GMN1=GMN2.GMN1 AND PG.GMN2=GMN2.COD"
        sConsulta = sConsulta & " WHERE PG.PROVE='" & DblQuote(sProve) & "'"

        oAdores.Open sConsulta, oADOConnection, adOpenForwardOnly, adLockReadOnly
        While Not oAdores.eof
            sConsulta = "INSERT INTO CIAS_ACT2 (CIA,ACT1,ACT2) VALUES (" & lCia & "," & oAdores("ACT1").Value & "," & oAdores("ACT2").Value & ")"
            oADOConnection.Execute "EXEC " & sFSP & "sp_executesql N'" & sConsulta & "'"
            oAdores.MoveNext
        Wend
        oAdores.Close

        'Actividades de nivel 1:
        sConsulta = "SELECT GMN1.ID ACT1 FROM PROVE_GMN1 PG INNER JOIN GMN1 ON PG.GMN1=GMN1.COD"
        sConsulta = sConsulta & " WHERE PG.PROVE='" & DblQuote(sProve) & "'"

        oAdores.Open sConsulta, oADOConnection, adOpenForwardOnly, adLockReadOnly
        While Not oAdores.eof
            sConsulta = "INSERT INTO CIAS_ACT1 (CIA,ACT1) VALUES (" & lCia & "," & oAdores("ACT1").Value & ")"
            oADOConnection.Execute "EXEC " & sFSP & "sp_executesql N'" & sConsulta & "'"
            oAdores.MoveNext
        Wend
        oAdores.Close
        
        Set oAdores = Nothing
        
ProveedoresMaterialGS = True
Exit Function
        
Error:
    MsgBox oADOConnection.Errors(0).Description, vbCritical + vbOKOnly
   'Resume Next
    If NumTransaccionesAbiertasADO(oADOConnection) Then
        oADOConnection.Execute "ROLLBACK TRAN"
        oADOConnection.Execute "SET XACT_ABORT OFF"
    End If
    ProveedoresMaterialGS = False
End Function

Private Function ProveedoresTodosMateriales(ByVal sProve As String, ByVal lCia As Long) As Boolean
Dim sConsulta As String

        'Actividades de nivel 4:
        sConsulta = "INSERT INTO PROVE_GMN4 (PROVE,GMN1,GMN2,GMN3,GMN4) SELECT '" & DblQuote(sProve) & "', AC.ACT1 ,AC.ACT2,AC.ACT3 ,AC.ACT4 FROM ACIAS_ACT4 AC WHERE AC.CIA=" & lCia
        sConsulta = sConsulta & " AND NOT EXISTS (SELECT * FROM PROVE_GMN4 PG WHERE PG.PROVE='" & DblQuote(sProve) & "' AND PG.GMN1=AC.ACT1 AND PG.GMN2=AC.ACT2 AND PG.GMN3=AC.ACT3 AND PG.GMN4=AC.ACT4)"
        oADOConnection.Execute sConsulta


        sConsulta = "INSERT INTO PROVE_GMN3 (PROVE,GMN1,GMN2,GMN3) SELECT '" & DblQuote(sProve) & "', AC.ACT1 ,AC.ACT2,AC.ACT3 FROM ACIAS_ACT3 AC WHERE AC.CIA=" & lCia
        sConsulta = sConsulta & " AND NOT EXISTS (SELECT * FROM PROVE_GMN3 PG WHERE PG.PROVE='" & DblQuote(sProve) & "' AND PG.GMN1=AC.ACT1 AND PG.GMN2=AC.ACT2 AND PG.GMN3=AC.ACT3 )"
        oADOConnection.Execute sConsulta

        'Actividades de nivel 2:
        sConsulta = "INSERT INTO PROVE_GMN2 (PROVE,GMN1,GMN2) SELECT '" & DblQuote(sProve) & "', AC.ACT1 ,AC.ACT2 FROM ACIAS_ACT2 AC WHERE AC.CIA=" & lCia
        sConsulta = sConsulta & " AND NOT EXISTS (SELECT * FROM PROVE_GMN2 PG WHERE PG.PROVE='" & DblQuote(sProve) & "' AND PG.GMN1=AC.ACT1 AND PG.GMN2=AC.ACT2)"
        oADOConnection.Execute sConsulta


        'Actividades de nivel 1:
        sConsulta = "INSERT INTO PROVE_GMN1 (PROVE,GMN1) SELECT '" & DblQuote(sProve) & "', AC.ACT1  FROM ACIAS_ACT1 AC WHERE AC.CIA=" & lCia
        sConsulta = sConsulta & " AND NOT EXISTS (SELECT * FROM PROVE_GMN1 PG WHERE PG.PROVE='" & DblQuote(sProve) & "' AND PG.GMN1=AC.ACT1)"
        oADOConnection.Execute sConsulta
        

ProveedoresTodosMateriales = True
Exit Function

Error:
    MsgBox oADOConnection.Errors(0).Description, vbCritical + vbOKOnly
   'Resume Next
    If NumTransaccionesAbiertasADO(oADOConnection) Then
        oADOConnection.Execute "ROLLBACK TRAN"
        oADOConnection.Execute "SET XACT_ABORT OFF"
    End If
    ProveedoresTodosMateriales = False
End Function

Public Function CodigoDeActualizacion2_16_05_08A2_16_05_09() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim iRespuesta As Integer

On Error GoTo Error
sConsulta = "BEGIN TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = True
ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

frmProgreso.lblDetalle = "Modificar stored procedure"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1


V_2_16_5_Storeds_9


frmProgreso.lblDetalle = "Modificar trigger"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1


V_2_16_5_Triggers_9

sConsulta = "UPDATE VERSION SET NUM ='2.16.05.09'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "COMMIT TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = False

CodigoDeActualizacion2_16_05_08A2_16_05_09 = True
Exit Function
    
Error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_16_05_08A2_16_05_09 = False

End Function


Private Sub V_2_16_5_Triggers_9()

Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ORDEN_EST_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[ORDEN_EST_TG_INS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TRIGGER ORDEN_EST_TG_INS ON dbo.ORDEN_EST " & vbCrLf
sConsulta = sConsulta & "FOR INSERT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_ORDEN_EST_Ins CURSOR LOCAL FOR SELECT EST,ID,ORDEN,PER,COMENT,EXTERNO FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_ORDEN_EST_Ins" & vbCrLf
sConsulta = sConsulta & "DECLARE @EST AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_ORDEN AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @SENTIDO AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PER AS VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @USU AS VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @EXTERNO AS BIT" & vbCrLf
sConsulta = sConsulta & "DECLARE @TIPO AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ACTIVA AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ACTIVLOG AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ORIGEN AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @COMENT AS VARCHAR(255)" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_LOG_ORDEN_ENTREGA AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVE AS VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PAG AS VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @INTEGRACION AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ACCION AS VARCHAR(2)" & vbCrLf
sConsulta = sConsulta & "DECLARE @TRASPASO_ERP AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ERP  AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @NOGRABAR AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @TRASLADO_APROV_ERP AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @COD_ERP AS VARCHAR(200)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_ORDEN_EST_Ins INTO @EST,@ID,@ID_ORDEN,@PER,@COMENT,@EXTERNO" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "  IF @EXTERNO IS NULL" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "    IF (@EST = 2 OR @EST = 3 OR @EST = 4 OR @EST = 20 OR @EST = 21) " & vbCrLf
sConsulta = sConsulta & "      BEGIN -- Si es uno de los estados que nos interesan para grabar en LOG_ORDEN_ENTREGA (emitido,aceptado,rechazado,anulado,en camino)" & vbCrLf
sConsulta = sConsulta & "        -- Recuperamos el tipo de orden de entrega para ver si est� activada su integraci�n" & vbCrLf
sConsulta = sConsulta & "        SET @COD_ERP=NULL" & vbCrLf
sConsulta = sConsulta & "        SELECT  @TIPO=TIPO,@PROVE=PROVE, @COD_ERP=COD_PROVE_ERP FROM ORDEN_ENTREGA WHERE ORDEN_ENTREGA.ID = @ID_ORDEN" & vbCrLf
sConsulta = sConsulta & "        SELECT  @ACTIVLOG = ACTIVLOG FROM PARGEN_GEST WHERE ID = 1" & vbCrLf
sConsulta = sConsulta & "        SELECT  @INTEGRACION =INTEGRACION, @TRASLADO_APROV_ERP=TRASLADO_APROV_ERP FROM PARGEN_INTERNO WHERE ID = 1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "        IF @TIPO = 0 AND @EST = 2  --Emisi�n de pedidos directos" & vbCrLf
sConsulta = sConsulta & "             SELECT  @TRASPASO_ERP= TRASPASO_PED_ERP FROM PARGEN_INTERNO WHERE ID = 1" & vbCrLf
sConsulta = sConsulta & "        ELSE" & vbCrLf
sConsulta = sConsulta & "             SET @TRASPASO_ERP=0" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "        IF @INTEGRACION = 0" & vbCrLf
sConsulta = sConsulta & "             SET @ACTIVA = 0" & vbCrLf
sConsulta = sConsulta & "        ELSE " & vbCrLf
sConsulta = sConsulta & "             BEGIN" & vbCrLf
sConsulta = sConsulta & "             IF @TIPO = 0 --Pedidos directos" & vbCrLf
sConsulta = sConsulta & "                 SELECT @ACTIVA=ACTIVA,@SENTIDO=SENTIDO FROM TABLAS_INTEGRACION WHERE ID = 13" & vbCrLf
sConsulta = sConsulta & "             ELSE" & vbCrLf
sConsulta = sConsulta & "                 SELECT @ACTIVA=ACTIVA,@SENTIDO=SENTIDO FROM TABLAS_INTEGRACION WHERE ID = 11" & vbCrLf
sConsulta = sConsulta & "             END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "        IF (@ACTIVA = 1 AND (@SENTIDO=3 OR @SENTIDO=1)) OR @ACTIVLOG = 1 OR @TRASPASO_ERP=1 OR @TRASLADO_APROV_ERP = 1" & vbCrLf
sConsulta = sConsulta & "          -- si el sentido es = 2  (solo entrada) o no est� activa la integraci�n  no grabamos en log" & vbCrLf
sConsulta = sConsulta & "          BEGIN" & vbCrLf
sConsulta = sConsulta & "            SET @NOGRABAR=0" & vbCrLf
sConsulta = sConsulta & "            IF @TRASPASO_ERP=1  OR @TRASLADO_APROV_ERP = 1 --emisi�n de pedidos directos y de aprovisionamiento" & vbCrLf
sConsulta = sConsulta & "                BEGIN" & vbCrLf
sConsulta = sConsulta & "                SET @ERP=1" & vbCrLf
sConsulta = sConsulta & "                IF @ACTIVLOG = 1 -- log de actividad" & vbCrLf
sConsulta = sConsulta & "                   SET @ORIGEN = 1" & vbCrLf
sConsulta = sConsulta & "                ELSE" & vbCrLf
sConsulta = sConsulta & "                   SET @NOGRABAR=1" & vbCrLf
sConsulta = sConsulta & "                END" & vbCrLf
sConsulta = sConsulta & "            ELSE" & vbCrLf
sConsulta = sConsulta & "                begin" & vbCrLf
sConsulta = sConsulta & "                SET @ERP=0" & vbCrLf
sConsulta = sConsulta & "                IF @ACTIVLOG = 1 " & vbCrLf
sConsulta = sConsulta & "                     begin" & vbCrLf
sConsulta = sConsulta & "                     IF @ACTIVA  = 1" & vbCrLf
sConsulta = sConsulta & "                             SET @ORIGEN = 2 -- Integracion y log de actividad" & vbCrLf
sConsulta = sConsulta & "                     ELSE" & vbCrLf
sConsulta = sConsulta & "                             SET @ORIGEN = 1 -- solo log de actividad" & vbCrLf
sConsulta = sConsulta & "                     end" & vbCrLf
sConsulta = sConsulta & "                 ELSE" & vbCrLf
sConsulta = sConsulta & "                     BEGIN" & vbCrLf
sConsulta = sConsulta & "                      IF @ACTIVA = 1" & vbCrLf
sConsulta = sConsulta & "                          SET @ORIGEN = 0 -- solo integracion" & vbCrLf
sConsulta = sConsulta & "                      ELSE" & vbCrLf
sConsulta = sConsulta & "                          SET @NOGRABAR=1" & vbCrLf
sConsulta = sConsulta & "                     END" & vbCrLf
sConsulta = sConsulta & "                end" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "            IF @PER IS NULL" & vbCrLf
sConsulta = sConsulta & "                SET @USU = NULL" & vbCrLf
sConsulta = sConsulta & "            ELSE" & vbCrLf
sConsulta = sConsulta & "                SET @USU = (SELECT COD FROM USU WHERE PER = @PER)" & vbCrLf
sConsulta = sConsulta & "            SET @PAG = (SELECT PAG FROM PROVE WHERE COD = @PROVE)" & vbCrLf
sConsulta = sConsulta & "            -- la acci�n ser� E - Cambio de estado o I Creaci�n orden de entrega)" & vbCrLf
sConsulta = sConsulta & "            IF @EST = 2  " & vbCrLf
sConsulta = sConsulta & "              SET @ACCION='I'" & vbCrLf
sConsulta = sConsulta & "            ELSE " & vbCrLf
sConsulta = sConsulta & "              BEGIN" & vbCrLf
sConsulta = sConsulta & "                IF @EST=20 " & vbCrLf
sConsulta = sConsulta & "                  SET @ACCION='D'" & vbCrLf
sConsulta = sConsulta & "                ELSE" & vbCrLf
sConsulta = sConsulta & "                  SET @ACCION='E'" & vbCrLf
sConsulta = sConsulta & "              END" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "            IF @NOGRABAR=0" & vbCrLf
sConsulta = sConsulta & "              BEGIN" & vbCrLf
sConsulta = sConsulta & "                INSERT INTO LOG_ORDEN_ENTREGA (ACCION,ID_ORDEN_ENTREGA,ID_ORDEN_EST,ID_PEDIDO,ANYO,PROVE,NUM_PEDIDO,NUM_ORDEN,NUMEXT,TIPO,FECHA,IMPORTE,PAG,EST,INCORRECTA,COMENT,ORIGEN,PER,USU,MON,CAMBIO,EMPRESA,RECEPTOR,COD_PROVE_ERP,REFERENCIA)" & vbCrLf
sConsulta = sConsulta & "                SELECT @ACCION,@ID_ORDEN,@ID,PEDIDO,ORDEN_ENTREGA.ANYO,PROVE,PEDIDO.NUM,ORDEN_ENTREGA.NUM,NUMEXT,ORDEN_ENTREGA.TIPO,ORDEN_ENTREGA.FECHA,IMPORTE,@PAG,@EST,INCORRECTA,@COMENT,@ORIGEN,@PER,@USU, MON,CAMBIO,EMPRESA,RECEPTOR,COD_PROVE_ERP,REFERENCIA FROM ORDEN_ENTREGA INNER JOIN PEDIDO ON PEDIDO.ID = ORDEN_ENTREGA.PEDIDO WHERE ORDEN_ENTREGA.ID = @ID_ORDEN" & vbCrLf
sConsulta = sConsulta & "                SET @ID_LOG_ORDEN_ENTREGA = (SELECT MAX(ID) FROM LOG_ORDEN_ENTREGA)" & vbCrLf
sConsulta = sConsulta & "                -- En el caso de Emisi�n de Pedido Directo, todav�a no estar�n creadas las lineas de pedido, se hace en el trigger LINEAS_PEDIDO_TG_INS" & vbCrLf
sConsulta = sConsulta & "                INSERT INTO LOG_LINEAS_PEDIDO (ACCION,ID_LOG_ORDEN_ENTREGA,ID_LINEAS_PEDIDO,ID_ORDEN_EST,ART_INT,UP,PREC_UP,CANT_PED,DEST,ANYO,GMN1,GMN2,GMN3,GMN4,PROCE,ITEM,ENTREGA_OBL,FECENTREGA,FECENTREGAPROVE,EST,CAT1,CAT2,CAT3,CAT4,CAT5,OBS,FC)" & vbCrLf
sConsulta = sConsulta & "                SELECT @ACCION,@ID_LOG_ORDEN_ENTREGA,ID,@ID,ART_INT,UP,PREC_UP,CANT_PED,DEST,ANYO,GMN1,GMN2,GMN3,GMN4,PROCE,ITEM,ENTREGA_OBL,FECENTREGA,FECENTREGAPROVE,EST,CAT1,CAT2,CAT3,CAT4,CAT5,OBS,FC FROM LINEAS_PEDIDO WHERE  ORDEN = @ID_ORDEN" & vbCrLf
sConsulta = sConsulta & "              END" & vbCrLf
sConsulta = sConsulta & "            IF @ERP=1 " & vbCrLf
sConsulta = sConsulta & "              BEGIN" & vbCrLf
sConsulta = sConsulta & "                IF @COD_ERP IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                    BEGIN" & vbCrLf
sConsulta = sConsulta & "                    INSERT INTO LOG_ORDEN_ENTREGA (ACCION,ID_ORDEN_ENTREGA,ID_ORDEN_EST,ID_PEDIDO,ANYO,PROVE,NUM_PEDIDO,NUM_ORDEN,NUMEXT,TIPO,FECHA,IMPORTE,PAG,EST,INCORRECTA,COMENT,ORIGEN,PER,USU,MON,CAMBIO,EMPRESA,RECEPTOR,COD_PROVE_ERP,REFERENCIA)" & vbCrLf
sConsulta = sConsulta & "                        SELECT @ACCION,@ID_ORDEN,@ID,PEDIDO,ORDEN_ENTREGA.ANYO,PROVE,PEDIDO.NUM,ORDEN_ENTREGA.NUM,NUMEXT,ORDEN_ENTREGA.TIPO,ORDEN_ENTREGA.FECHA,IMPORTE,@PAG,@EST,INCORRECTA,@COMENT,0,@PER,@USU, MON,CAMBIO,EMPRESA,RECEPTOR,COD_PROVE_ERP,REFERENCIA FROM ORDEN_ENTREGA INNER JOIN PEDIDO ON PEDIDO.ID = ORDEN_ENTREGA.PEDIDO WHERE ORDEN_ENTREGA.ID = @ID_ORDEN" & vbCrLf
sConsulta = sConsulta & "                    SET @ID_LOG_ORDEN_ENTREGA = (SELECT MAX(ID) FROM LOG_ORDEN_ENTREGA)" & vbCrLf
sConsulta = sConsulta & "                    INSERT INTO LOG_LINEAS_PEDIDO (ACCION,ID_LOG_ORDEN_ENTREGA,ID_LINEAS_PEDIDO,ID_ORDEN_EST,ART_INT,UP,PREC_UP,CANT_PED,DEST,ANYO,GMN1,GMN2,GMN3,GMN4,PROCE,ITEM,ENTREGA_OBL,FECENTREGA,FECENTREGAPROVE,EST,CAT1,CAT2,CAT3,CAT4,CAT5,OBS,FC)" & vbCrLf
sConsulta = sConsulta & "                        SELECT @ACCION,@ID_LOG_ORDEN_ENTREGA,ID,@ID,ART_INT,UP,PREC_UP,CANT_PED,DEST,ANYO,GMN1,GMN2,GMN3,GMN4,PROCE,ITEM,ENTREGA_OBL,FECENTREGA,FECENTREGAPROVE,EST,CAT1,CAT2,CAT3,CAT4,CAT5,OBS,FC FROM LINEAS_PEDIDO WHERE  ORDEN = @ID_ORDEN" & vbCrLf
sConsulta = sConsulta & "                    END" & vbCrLf
sConsulta = sConsulta & "              END" & vbCrLf
sConsulta = sConsulta & "          END --IF ACTIVA" & vbCrLf
sConsulta = sConsulta & "      END -- IF EST" & vbCrLf
sConsulta = sConsulta & "   END -- IF @EXTERNO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  UPDATE ORDEN_ENTREGA SET ORDEN_EST = @ID WHERE ORDEN_ENTREGA.ID = @ID_ORDEN" & vbCrLf
sConsulta = sConsulta & "  FETCH NEXT FROM curTG_ORDEN_EST_Ins INTO @EST,@ID,@ID_ORDEN,@PER,@COMENT,@EXTERNO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CLOSE curTG_ORDEN_EST_Ins" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_ORDEN_EST_Ins" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PROCE_PROVE_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PROCE_PROVE_TG_INS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TRIGGER PROCE_PROVE_TG_INS ON dbo.PROCE_PROVE" & vbCrLf
sConsulta = sConsulta & "FOR INSERT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN1 AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROCE AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVE AS VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @COUNT AS INTEGER" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_PROCE_PROVE_Ins CURSOR FOR SELECT ANYO,GMN1,PROCE,PROVE FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_PROCE_PROVE_Ins" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_PROVE_Ins INTO @ANYO,@GMN1,@PROCE,@PROVE" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "/*Inserta los registros correspondientes en las tablas de configuraci�n de la comparativa*/" & vbCrLf
sConsulta = sConsulta & "SELECT @COUNT =COUNT(*) FROM CONF_VISTAS_PROCE WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "IF @COUNT>0" & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO CONF_VISTAS_PROCE_PROVE (ANYO,GMN1,PROCE,PROVE,USU,VISTA, VISIBLE)" & vbCrLf
sConsulta = sConsulta & "             (SELECT ANYO,GMN1,PROCE,@PROVE,USU,VISTA,1 FROM CONF_VISTAS_PROCE WHERE ANYO=@ANYO" & vbCrLf
sConsulta = sConsulta & "   AND GMN1=@GMN1 AND PROCE=@PROCE )" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "SELECT @COUNT =COUNT(*) FROM CONF_VISTAS_ALL WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "IF @COUNT>0" & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO CONF_VISTAS_ALL_PROVE (ANYO,GMN1,PROCE,PROVE,USU,VISTA, VISIBLE)" & vbCrLf
sConsulta = sConsulta & "             (SELECT ANYO,GMN1,PROCE,@PROVE,USU,VISTA,1 FROM CONF_VISTAS_ALL WHERE ANYO=@ANYO" & vbCrLf
sConsulta = sConsulta & "   AND GMN1=@GMN1 AND PROCE=@PROCE )" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "SELECT @COUNT =COUNT(*) FROM CONF_VISTAS_GRUPO WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "IF @COUNT>0" & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO CONF_VISTAS_GRUPO_PROVE (ANYO,GMN1,PROCE,GRUPO,PROVE,USU,VISTA, VISIBLE)" & vbCrLf
sConsulta = sConsulta & "             (SELECT ANYO,GMN1,PROCE,GRUPO,@PROVE,USU,VISTA,1 FROM CONF_VISTAS_GRUPO WHERE ANYO=@ANYO" & vbCrLf
sConsulta = sConsulta & "   AND GMN1=@GMN1 AND PROCE=@PROCE )" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "SELECT @COUNT =COUNT(*) FROM CONF_VISTAS_ITEM WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE " & vbCrLf
sConsulta = sConsulta & "IF @COUNT>0" & vbCrLf
sConsulta = sConsulta & "    BEGIN" & vbCrLf
sConsulta = sConsulta & "   INSERT INTO CONF_VISTAS_ITEM_PROVE (ANYO,GMN1,PROCE,GRUPO,PROVE,USU,VISTA, VISIBLE)" & vbCrLf
sConsulta = sConsulta & "             (SELECT ANYO,GMN1,PROCE,GRUPO,@PROVE,USU,VISTA,1 FROM CONF_VISTAS_ITEM WHERE ANYO=@ANYO" & vbCrLf
sConsulta = sConsulta & "   AND GMN1=@GMN1 AND PROCE=@PROCE )" & vbCrLf
sConsulta = sConsulta & "   END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET P= case when P is null then 1 else (P+1) end  WHERE ANYO= @ANYO  AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "/* Si se han insertado proveedores, actualizamos el estado del proceso  si es que este era inferior a */" & vbCrLf
sConsulta = sConsulta & "IF (SELECT PROCE.EST FROM PROCE WHERE ANYO=@ANYO  AND GMN1=@GMN1 AND COD=@PROCE) < 4  /* 3= Validado */" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE SET EST = 4  WHERE ANYO= @ANYO  AND GMN1=@GMN1 AND COD=@PROCE" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_PROVE_Ins INTO @ANYO,@GMN1,@PROCE,@PROVE" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_PROCE_PROVE_Ins" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_PROCE_PROVE_Ins" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PROVE_GMN1_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PROVE_GMN1_TG_INS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TRIGGER PROVE_GMN1_TG_INS ON dbo.PROVE_GMN1" & vbCrLf
sConsulta = sConsulta & "FOR INSERT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVE VARCHAR(50),@GMN1 VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @NEM INT" & vbCrLf
sConsulta = sConsulta & "SET @NEM = (SELECT NEM FROM PARGEN_INTERNO WHERE ID =1)" & vbCrLf
sConsulta = sConsulta & "IF @NEM >= 2" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_Prove_Gmn1_Ins CURSOR FOR SELECT PROVE,GMN1 FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_Prove_Gmn1_Ins" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_Prove_Gmn1_Ins INTO @PROVE,@GMN1" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "INSERT INTO PROVE_GMN2 SELECT @PROVE,GMN1,COD FROM GMN2 WHERE GMN1=@GMN1 AND NOT EXISTS (SELECT * FROM PROVE_GMN2 WHERE  PROVE = @PROVE AND GMN1=@GMN1 AND GMN2 = GMN2.COD)" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_Prove_Gmn1_Ins INTO @PROVE,@GMN1" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_Prove_Gmn1_Ins" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_Prove_Gmn1_Ins" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PROVE_GMN2_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PROVE_GMN2_TG_INS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TRIGGER PROVE_GMN2_TG_INS ON dbo.PROVE_GMN2" & vbCrLf
sConsulta = sConsulta & "FOR INSERT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVE VARCHAR(50),@GMN1 VARCHAR(50),@GMN2 VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @NEM INT" & vbCrLf
sConsulta = sConsulta & "SET @NEM = (SELECT NEM FROM PARGEN_INTERNO WHERE ID =1)" & vbCrLf
sConsulta = sConsulta & "IF @NEM >= 3" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_Prove_Gmn2_Ins CURSOR FOR SELECT PROVE,GMN1,GMN2 FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_Prove_Gmn2_Ins" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_Prove_Gmn2_Ins INTO @PROVE,@GMN1,@GMN2" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "INSERT INTO PROVE_GMN3 SELECT @PROVE,GMN1,GMN2,COD FROM GMN3 WHERE GMN1=@GMN1 AND GMN2=@GMN2  AND NOT EXISTS (SELECT * FROM PROVE_GMN3 WHERE  PROVE_GMN3.PROVE = @PROVE AND PROVE_GMN3.GMN1=@GMN1 AND PROVE_GMN3.GMN2=@GMN2 AND GMN3 = GMN3.COD)" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_Prove_Gmn2_Ins INTO @PROVE,@GMN1,@GMN2" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_Prove_Gmn2_Ins" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_Prove_Gmn2_Ins" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PROVE_GMN3_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PROVE_GMN3_TG_INS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TRIGGER PROVE_GMN3_TG_INS ON dbo.PROVE_GMN3" & vbCrLf
sConsulta = sConsulta & "FOR INSERT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVE VARCHAR(50),@GMN1 VARCHAR(50),@GMN2 VARCHAR(50),@GMN3 VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE @NEM INT" & vbCrLf
sConsulta = sConsulta & "SET @NEM = (SELECT NEM FROM PARGEN_INTERNO WHERE ID =1)" & vbCrLf
sConsulta = sConsulta & "IF @NEM >= 4" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_Prove_Gmn3_Ins CURSOR FOR SELECT PROVE,GMN1,GMN2,GMN3 FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_Prove_Gmn3_Ins" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_Prove_Gmn3_Ins INTO @PROVE,@GMN1,@GMN2,@GMN3" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "INSERT INTO PROVE_GMN4 SELECT @PROVE,GMN1,GMN2,GMN3,COD FROM GMN4 WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND NOT EXISTS (SELECT * FROM PROVE_GMN4 WHERE  PROVE_GMN4.PROVE = @PROVE AND PROVE_GMN4.GMN1=@GMN1 AND PROVE_GMN4.GMN2=@GMN2  AND PROVE_GMN4.GMN3=@GMN3 AND PROVE_GMN4.GMN4 = GMN4.COD)" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_Prove_Gmn3_Ins INTO @PROVE,@GMN1,@GMN2,@GMN3" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_Prove_Gmn3_Ins" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_Prove_Gmn3_Ins" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PROVE_GMN4_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PROVE_GMN4_INS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TRIGGER PROVE_GMN4_INS ON dbo.PROVE_GMN4" & vbCrLf
sConsulta = sConsulta & "FOR INSERT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @GMN1 VARCHAR(50),@GMN2 VARCHAR(50),@GMN3 VARCHAR(50),@GMN4 VARCHAR(50),@PROVE VARCHAR(50)" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_PROVE_GMN4_Ins CURSOR FOR SELECT GMN1,GMN2,GMN3,GMN4,PROVE FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_PROVE_GMN4_Ins" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROVE_GMN4_Ins INTO @GMN1,@GMN2,@GMN3,@GMN4,@PROVE" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "INSERT INTO PROVE_ART4 (PROVE,GMN1,GMN2,GMN3,GMN4,ART,HOM) SELECT @PROVE,@GMN1,@GMN2,@GMN3,@GMN4,COD,0 FROM ART4  WHERE GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@GMN4" & vbCrLf
sConsulta = sConsulta & "       AND NOT EXISTS (SELECT * FROM PROVE_ART4 WHERE  PROVE_ART4.PROVE = @PROVE AND PROVE_ART4.GMN1=@GMN1 AND PROVE_ART4.GMN2=@GMN2  AND PROVE_ART4.GMN3=@GMN3 AND PROVE_ART4.GMN4 = @GMN4 AND PROVE_ART4.ART=ART4.COD)" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROVE_GMN4_Ins INTO @GMN1,@GMN2,@GMN3,@GMN4,@PROVE" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_PROVE_GMN4_Ins" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_PROVE_GMN4_Ins" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta
End Sub

Private Sub V_2_16_5_Storeds_9()
Dim sConsulta As String

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ANYA_PROVE_MAT1]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_ANYA_PROVE_MAT1]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_ANYA_PROVE_MAT1 (@COD VARCHAR(100),@GMN1 VARCHAR(50))  AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF NOT EXISTS (SELECT * FROM PROVE_GMN1 WHERE PROVE = @COD AND GMN1=@GMN1)" & vbCrLf
sConsulta = sConsulta & "INSERT INTO PROVE_GMN1 (PROVE,GMN1) VALUES (@COD,@GMN1)" & vbCrLf
sConsulta = sConsulta & "Return" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ANYA_PROVE_MAT2]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_ANYA_PROVE_MAT2]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_ANYA_PROVE_MAT2 (@COD VARCHAR(100),@GMN1 VARCHAR(50),@GMN2 VARCHAR(50))  AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF NOT EXISTS (SELECT * FROM PROVE_GMN2 WHERE PROVE = @COD AND GMN1=@GMN1 AND GMN2=@GMN2)" & vbCrLf
sConsulta = sConsulta & "INSERT INTO PROVE_GMN2 (PROVE,GMN1,GMN2) VALUES (@COD,@GMN1,@GMN2)" & vbCrLf
sConsulta = sConsulta & "Return" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ANYA_PROVE_MAT3]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_ANYA_PROVE_MAT3]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_ANYA_PROVE_MAT3 (@COD VARCHAR(100),@GMN1 VARCHAR(50),@GMN2 VARCHAR(50),@GMN3 VARCHAR(50))  AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF NOT EXISTS (SELECT * FROM PROVE_GMN3 WHERE PROVE = @COD AND GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3)" & vbCrLf
sConsulta = sConsulta & "INSERT INTO PROVE_GMN3 (PROVE,GMN1,GMN2,GMN3) VALUES (@COD,@GMN1,@GMN2,@GMN3)" & vbCrLf
sConsulta = sConsulta & "Return" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_ANYA_PROVE_MAT4]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[SP_ANYA_PROVE_MAT4]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE SP_ANYA_PROVE_MAT4 (@COD VARCHAR(100),@GMN1 VARCHAR(50),@GMN2 VARCHAR(50),@GMN3 VARCHAR(50),@GMN4 VARCHAR(50))  AS" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF NOT EXISTS (SELECT * FROM PROVE_GMN4 WHERE PROVE = @COD AND GMN1=@GMN1 AND GMN2=@GMN2 AND GMN3=@GMN3 AND GMN4=@GMN4)" & vbCrLf
sConsulta = sConsulta & "INSERT INTO PROVE_GMN4(PROVE,GMN1,GMN2,GMN3,GMN4) VALUES (@COD,@GMN1,@GMN2,@GMN3,@GMN4)" & vbCrLf
sConsulta = sConsulta & "Return" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


End Sub


Public Function CodigoDeActualizacion2_16_05_09A2_16_05_10() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim iRespuesta As Integer

On Error GoTo Error
sConsulta = "BEGIN TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = True
ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

frmProgreso.lblDetalle = "Modificar stored procedure"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1


V_2_16_5_Storeds_10


frmProgreso.lblDetalle = "Modificar trigger"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1


V_2_16_5_Triggers_10

frmProgreso.lblDetalle = "Modificar tabla WEBTEXT"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1


V_2_16_5_Tablas_10


sConsulta = "UPDATE VERSION SET NUM ='2.16.05.10'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "COMMIT TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = False

CodigoDeActualizacion2_16_05_09A2_16_05_10 = True
Exit Function
    
Error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_16_05_09A2_16_05_10 = False

End Function


Private Sub V_2_16_5_Storeds_10()

Dim sConsulta As String

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[USU_LOGIN]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[USU_LOGIN]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


sConsulta = "CREATE PROCEDURE USU_LOGIN(@ADM INT,@USUOLD VARCHAR(50),@PWDOLD VARCHAR(512),@USUNEW VARCHAR(50),@PWDNEW VARCHAR(512),@FECPWD DATETIME,@CHANGE tinyint,@MAIL VARCHAR(200)=NULL) AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @VAR_PWD VARCHAR(512)" & vbCrLf
sConsulta = sConsulta & "DECLARE @ERROR INT" & vbCrLf
sConsulta = sConsulta & "--SET XACT_ABORT ON" & vbCrLf
sConsulta = sConsulta & "--BEGIN TRANSACTION" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "SET @ERROR=0" & vbCrLf
sConsulta = sConsulta & "IF @ADM=1" & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR FOR SELECT PWD FROM ADM WHERE USU=@USUOLD" & vbCrLf
sConsulta = sConsulta & "Else" & vbCrLf
sConsulta = sConsulta & "IF @ADM=2" & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR FOR SELECT PWD FROM OBJSADM WHERE USU=@USUOLD" & vbCrLf
sConsulta = sConsulta & "Else" & vbCrLf
sConsulta = sConsulta & "DECLARE C CURSOR FOR SELECT PWD FROM USU WHERE COD=@USUOLD" & vbCrLf
sConsulta = sConsulta & "OPEN C" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM C INTO @VAR_PWD" & vbCrLf
sConsulta = sConsulta & "IF @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "IF @VAR_PWD<>@PWDOLD" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "--SET XACT_ABORT OFF" & vbCrLf
sConsulta = sConsulta & "--ROLLBACK TRANSACTION" & vbCrLf
sConsulta = sConsulta & "RAISERROR('Incorrect password.',16,1)" & vbCrLf
sConsulta = sConsulta & "SET @ERROR=1" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Else" & vbCrLf
sConsulta = sConsulta & "  " & vbCrLf
sConsulta = sConsulta & "IF @ADM=1" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "   ALTER TABLE ADM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "   ALTER TABLE CONF_VISTAS_PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "   ALTER TABLE CONF_VISTAS_PROCE_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "   ALTER TABLE CONF_VISTAS_PROCE_SOBRE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "   ALTER TABLE CONF_VISTAS_PROCE_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "   ALTER TABLE CONF_VISTAS_GRUPO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "   ALTER TABLE CONF_VISTAS_GRUPO_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "   ALTER TABLE CONF_VISTAS_GRUPO_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "   ALTER TABLE CONF_VISTAS_ITEM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "   ALTER TABLE CONF_VISTAS_ITEM_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "   ALTER TABLE CONF_VISTAS_ITEM_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "   ALTER TABLE CONF_VISTAS_ALL NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "   ALTER TABLE CONF_VISTAS_ALL_ATRIB NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "   ALTER TABLE CONF_VISTAS_ALL_PROVE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "   ALTER TABLE CONF_VISTA_INICIAL_PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "   ALTER TABLE CONF_VISTA_INICIAL_GRUPO NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "   ALTER TABLE PROCE_CONTR NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "   ALTER TABLE PARINS_DEF NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "   ALTER TABLE PARINS_GEST NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "   ALTER TABLE PARINS_DOT NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "   ALTER TABLE PARINS_PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "   ALTER TABLE PROCE_NUMDEC NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "   ALTER TABLE PROCE NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   UPDATE ADM SET USU=@USUNEW WHERE USU=@USUOLD" & vbCrLf
sConsulta = sConsulta & "   UPDATE ADM SET PWD=@PWDNEW,FECPWD=@FECPWD,EMAIL=@MAIL WHERE USU=@USUNEW" & vbCrLf
sConsulta = sConsulta & "   UPDATE CONF_VISTAS_PROCE SET USU=@USUNEW WHERE USU=@USUOLD" & vbCrLf
sConsulta = sConsulta & "   UPDATE CONF_VISTAS_PROCE_PROVE SET USU=@USUNEW WHERE USU=@USUOLD" & vbCrLf
sConsulta = sConsulta & "   UPDATE CONF_VISTAS_PROCE_SOBRE SET USU=@USUNEW WHERE USU=@USUOLD" & vbCrLf
sConsulta = sConsulta & "   UPDATE CONF_VISTAS_PROCE_ATRIB SET USU=@USUNEW WHERE USU=@USUOLD" & vbCrLf
sConsulta = sConsulta & "   UPDATE CONF_VISTAS_GRUPO SET USU=@USUNEW WHERE USU=@USUOLD" & vbCrLf
sConsulta = sConsulta & "   UPDATE CONF_VISTAS_GRUPO_PROVE SET USU=@USUNEW WHERE USU=@USUOLD" & vbCrLf
sConsulta = sConsulta & "   UPDATE CONF_VISTAS_GRUPO_ATRIB SET USU=@USUNEW WHERE USU=@USUOLD" & vbCrLf
sConsulta = sConsulta & "   UPDATE CONF_VISTAS_ITEM SET USU=@USUNEW WHERE USU=@USUOLD" & vbCrLf
sConsulta = sConsulta & "   UPDATE CONF_VISTAS_ITEM_PROVE SET USU=@USUNEW WHERE USU=@USUOLD" & vbCrLf
sConsulta = sConsulta & "   UPDATE CONF_VISTAS_ITEM_ATRIB SET USU=@USUNEW WHERE USU=@USUOLD" & vbCrLf
sConsulta = sConsulta & "   UPDATE CONF_VISTAS_ALL SET USU=@USUNEW WHERE USU=@USUOLD" & vbCrLf
sConsulta = sConsulta & "   UPDATE CONF_VISTAS_ALL_ATRIB SET USU=@USUNEW WHERE USU=@USUOLD" & vbCrLf
sConsulta = sConsulta & "   UPDATE CONF_VISTAS_ALL_PROVE SET USU=@USUNEW WHERE USU=@USUOLD" & vbCrLf
sConsulta = sConsulta & "   UPDATE CONF_VISTA_INICIAL_PROCE SET USU=@USUNEW WHERE USU=@USUOLD" & vbCrLf
sConsulta = sConsulta & "   UPDATE CONF_VISTA_INICIAL_GRUPO SET USU=@USUNEW WHERE USU=@USUOLD" & vbCrLf
sConsulta = sConsulta & "   UPDATE PROCE_CONTR SET USU=@USUNEW WHERE USU=@USUOLD" & vbCrLf
sConsulta = sConsulta & "   UPDATE PARINS_DEF SET USU=@USUNEW WHERE USU=@USUOLD" & vbCrLf
sConsulta = sConsulta & "   UPDATE PARINS_GEST SET USU=@USUNEW WHERE USU=@USUOLD" & vbCrLf
sConsulta = sConsulta & "   UPDATE PARINS_DOT SET USU=@USUNEW WHERE USU=@USUOLD" & vbCrLf
sConsulta = sConsulta & "   UPDATE PARINS_PROCE SET USU=@USUNEW WHERE USU=@USUOLD" & vbCrLf
sConsulta = sConsulta & "   UPDATE PROCE_NUMDEC SET USU=@USUNEW WHERE USU=@USUOLD" & vbCrLf
sConsulta = sConsulta & "   UPDATE PROCE SET USUVAL=@USUNEW WHERE USUVAL=@USUOLD" & vbCrLf
sConsulta = sConsulta & "   UPDATE PROCE SET USUVALSELPROVE=@USUNEW WHERE USUVALSELPROVE=@USUOLD" & vbCrLf
sConsulta = sConsulta & "   UPDATE PROCE SET USUVALCIERRE=@USUNEW WHERE USUVALCIERRE=@USUOLD" & vbCrLf
sConsulta = sConsulta & "   UPDATE PROCE SET USUAPER=@USUNEW WHERE USUAPER=@USUOLD" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   ALTER TABLE ADM CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "   ALTER TABLE CONF_VISTAS_PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "   ALTER TABLE CONF_VISTAS_PROCE_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "   ALTER TABLE CONF_VISTAS_PROCE_SOBRE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "   ALTER TABLE CONF_VISTAS_PROCE_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "   ALTER TABLE CONF_VISTAS_GRUPO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "   ALTER TABLE CONF_VISTAS_GRUPO_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "   ALTER TABLE CONF_VISTAS_GRUPO_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "   ALTER TABLE CONF_VISTAS_ITEM CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "   ALTER TABLE CONF_VISTAS_ITEM_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "   ALTER TABLE CONF_VISTAS_ITEM_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "   ALTER TABLE CONF_VISTAS_ALL CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "   ALTER TABLE CONF_VISTAS_ALL_ATRIB CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "   ALTER TABLE CONF_VISTAS_ALL_PROVE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "   ALTER TABLE CONF_VISTA_INICIAL_PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "   ALTER TABLE CONF_VISTA_INICIAL_GRUPO CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "   ALTER TABLE PROCE_CONTR CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "   ALTER TABLE PARINS_DEF CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "   ALTER TABLE PARINS_GEST CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "   ALTER TABLE PARINS_DOT CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "   ALTER TABLE PARINS_PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "   ALTER TABLE PROCE_NUMDEC CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "   ALTER TABLE PROCE CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "   End" & vbCrLf
sConsulta = sConsulta & "Else" & vbCrLf
sConsulta = sConsulta & "   IF @ADM=2" & vbCrLf
sConsulta = sConsulta & "     BEGIN" & vbCrLf
sConsulta = sConsulta & "     ALTER TABLE OBJSADM NOCHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "     UPDATE OBJSADM SET USU=@USUNEW WHERE USU=@USUOLD" & vbCrLf
sConsulta = sConsulta & "     ALTER TABLE OBJSADM CHECK CONSTRAINT ALL" & vbCrLf
sConsulta = sConsulta & "     UPDATE OBJSADM SET PWD=@PWDNEW,FECPWD=@FECPWD WHERE USU=@USUNEW" & vbCrLf
sConsulta = sConsulta & "     End" & vbCrLf
sConsulta = sConsulta & "   Else" & vbCrLf
sConsulta = sConsulta & "        IF @CHANGE=1" & vbCrLf
sConsulta = sConsulta & "             UPDATE USU SET PWD=@PWDNEW,FECPWD=@FECPWD,CHANGEPWD=0 WHERE COD=@USUNEW" & vbCrLf
sConsulta = sConsulta & "       ELSE" & vbCrLf
sConsulta = sConsulta & "             UPDATE USU SET PWD=@PWDNEW,FECPWD=@FECPWD WHERE COD=@USUNEW" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "IF @ERROR=0 CLOSE C" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE C" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "--IF @ERROR=0" & vbCrLf
sConsulta = sConsulta & "--COMMIT TRANSACTION" & vbCrLf
sConsulta = sConsulta & "--SET XACT_ABORT OFF" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


End Sub


Private Sub V_2_16_5_Triggers_10()
Dim sConsulta As String

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PROCE_DEF_TG_INSUPD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[PROCE_DEF_TG_INSUPD]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TRIGGER PROCE_DEF_TG_INSUPD ON dbo.PROCE_DEF" & vbCrLf
sConsulta = sConsulta & "FOR INSERT, UPDATE" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE @ANYO INT,@GMN1 VARCHAR(50),@PROCE SMALLINT" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_PROCE_DEF_Ins CURSOR FOR SELECT ANYO,GMN1,PROCE FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_PROCE_DEF_Ins" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_DEF_Ins INTO @ANYO,@GMN1,@PROCE" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "UPDATE PROCE_DEF SET FECACT=GETDATE()  WHERE ANYO=@ANYO AND GMN1=@GMN1 AND PROCE=@PROCE" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_PROCE_DEF_Ins INTO @ANYO,@GMN1,@PROCE" & vbCrLf
sConsulta = sConsulta & "End" & vbCrLf
sConsulta = sConsulta & "Close curTG_PROCE_DEF_Ins" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_PROCE_DEF_Ins" & vbCrLf
sConsulta = sConsulta & ""
ExecuteSQL gRDOCon, sConsulta

sConsulta = "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ORDEN_EST_TG_INS]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)" & vbCrLf
sConsulta = sConsulta & "drop trigger [dbo].[ORDEN_EST_TG_INS]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE TRIGGER ORDEN_EST_TG_INS ON dbo.ORDEN_EST " & vbCrLf
sConsulta = sConsulta & "FOR INSERT" & vbCrLf
sConsulta = sConsulta & "AS" & vbCrLf
sConsulta = sConsulta & "DECLARE curTG_ORDEN_EST_Ins CURSOR LOCAL FOR SELECT EST,ID,ORDEN,PER,COMENT,EXTERNO FROM INSERTED" & vbCrLf
sConsulta = sConsulta & "OPEN curTG_ORDEN_EST_Ins" & vbCrLf
sConsulta = sConsulta & "DECLARE @EST AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_ORDEN AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @SENTIDO AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PER AS VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @USU AS VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @EXTERNO AS BIT" & vbCrLf
sConsulta = sConsulta & "DECLARE @TIPO AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ACTIVA AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ACTIVLOG AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ORIGEN AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @COMENT AS VARCHAR(255)" & vbCrLf
sConsulta = sConsulta & "DECLARE @ID_LOG_ORDEN_ENTREGA AS INT" & vbCrLf
sConsulta = sConsulta & "DECLARE @PROVE AS VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @PAG AS VARCHAR(100)" & vbCrLf
sConsulta = sConsulta & "DECLARE @INTEGRACION AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ACCION AS VARCHAR(2)" & vbCrLf
sConsulta = sConsulta & "DECLARE @TRASPASO_PED_ERP AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @ERP  AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @NOGRABAR AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @TRASLADO_APROV_ERP AS TINYINT" & vbCrLf
sConsulta = sConsulta & "DECLARE @COD_ERP AS VARCHAR(200)" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "FETCH NEXT FROM curTG_ORDEN_EST_Ins INTO @EST,@ID,@ID_ORDEN,@PER,@COMENT,@EXTERNO" & vbCrLf
sConsulta = sConsulta & "WHILE @@FETCH_STATUS=0" & vbCrLf
sConsulta = sConsulta & "BEGIN" & vbCrLf
sConsulta = sConsulta & "  IF @EXTERNO IS NULL" & vbCrLf
sConsulta = sConsulta & "   BEGIN" & vbCrLf
sConsulta = sConsulta & "    IF (@EST = 2 OR @EST = 3 OR @EST = 4 OR @EST = 20 OR @EST = 21) " & vbCrLf
sConsulta = sConsulta & "      BEGIN -- Si es uno de los estados que nos interesan para grabar en LOG_ORDEN_ENTREGA (emitido,aceptado,rechazado,anulado,en camino)" & vbCrLf
sConsulta = sConsulta & "        -- Recuperamos el tipo de orden de entrega para ver si est� activada su integraci�n" & vbCrLf
sConsulta = sConsulta & "        SET @COD_ERP=NULL" & vbCrLf
sConsulta = sConsulta & "        SELECT  @TIPO=TIPO,@PROVE=PROVE, @COD_ERP=COD_PROVE_ERP FROM ORDEN_ENTREGA WHERE ORDEN_ENTREGA.ID = @ID_ORDEN" & vbCrLf
sConsulta = sConsulta & "        SELECT  @ACTIVLOG = ACTIVLOG FROM PARGEN_GEST WHERE ID = 1" & vbCrLf
sConsulta = sConsulta & "        SELECT  @INTEGRACION =INTEGRACION, @TRASLADO_APROV_ERP=TRASLADO_APROV_ERP, @TRASPASO_PED_ERP= TRASPASO_PED_ERP FROM PARGEN_INTERNO WHERE ID = 1" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "        IF @INTEGRACION = 0" & vbCrLf
sConsulta = sConsulta & "             SET @ACTIVA = 0" & vbCrLf
sConsulta = sConsulta & "        ELSE " & vbCrLf
sConsulta = sConsulta & "             BEGIN" & vbCrLf
sConsulta = sConsulta & "             IF @TIPO = 0 --Pedidos directos" & vbCrLf
sConsulta = sConsulta & "                 SELECT @ACTIVA=ACTIVA,@SENTIDO=SENTIDO FROM TABLAS_INTEGRACION WHERE ID = 13" & vbCrLf
sConsulta = sConsulta & "             ELSE" & vbCrLf
sConsulta = sConsulta & "                 SELECT @ACTIVA=ACTIVA,@SENTIDO=SENTIDO FROM TABLAS_INTEGRACION WHERE ID = 11" & vbCrLf
sConsulta = sConsulta & "             END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "        IF (@ACTIVA = 1 AND (@SENTIDO=3 OR @SENTIDO=1)) OR @ACTIVLOG = 1 OR @TRASPASO_PED_ERP=1 OR @TRASLADO_APROV_ERP = 1" & vbCrLf
sConsulta = sConsulta & "          -- si el sentido es = 2  (solo entrada) o no est� activa la integraci�n  no grabamos en log" & vbCrLf
sConsulta = sConsulta & "          BEGIN" & vbCrLf
sConsulta = sConsulta & "            SET @NOGRABAR=0" & vbCrLf
sConsulta = sConsulta & "            IF @TRASPASO_PED_ERP=1  OR @TRASLADO_APROV_ERP = 1 --emisi�n de pedidos directos y de aprovisionamiento" & vbCrLf
sConsulta = sConsulta & "                BEGIN" & vbCrLf
sConsulta = sConsulta & "                SET @ERP=1" & vbCrLf
sConsulta = sConsulta & "                IF @ACTIVLOG = 1 -- log de actividad" & vbCrLf
sConsulta = sConsulta & "                   SET @ORIGEN = 1" & vbCrLf
sConsulta = sConsulta & "                ELSE" & vbCrLf
sConsulta = sConsulta & "                   SET @NOGRABAR=1" & vbCrLf
sConsulta = sConsulta & "                END" & vbCrLf
sConsulta = sConsulta & "            ELSE" & vbCrLf
sConsulta = sConsulta & "                begin" & vbCrLf
sConsulta = sConsulta & "                SET @ERP=0" & vbCrLf
sConsulta = sConsulta & "                IF @ACTIVLOG = 1 " & vbCrLf
sConsulta = sConsulta & "                     begin" & vbCrLf
sConsulta = sConsulta & "                     IF @ACTIVA  = 1" & vbCrLf
sConsulta = sConsulta & "                             SET @ORIGEN = 2 -- Integracion y log de actividad" & vbCrLf
sConsulta = sConsulta & "                     ELSE" & vbCrLf
sConsulta = sConsulta & "                             SET @ORIGEN = 1 -- solo log de actividad" & vbCrLf
sConsulta = sConsulta & "                     end" & vbCrLf
sConsulta = sConsulta & "                 ELSE" & vbCrLf
sConsulta = sConsulta & "                     BEGIN" & vbCrLf
sConsulta = sConsulta & "                      IF @ACTIVA = 1" & vbCrLf
sConsulta = sConsulta & "                          SET @ORIGEN = 0 -- solo integracion" & vbCrLf
sConsulta = sConsulta & "                      ELSE" & vbCrLf
sConsulta = sConsulta & "                          SET @NOGRABAR=1" & vbCrLf
sConsulta = sConsulta & "                     END" & vbCrLf
sConsulta = sConsulta & "                end" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "            IF @PER IS NULL" & vbCrLf
sConsulta = sConsulta & "                SET @USU = NULL" & vbCrLf
sConsulta = sConsulta & "            ELSE" & vbCrLf
sConsulta = sConsulta & "                SET @USU = (SELECT COD FROM USU WHERE PER = @PER)" & vbCrLf
sConsulta = sConsulta & "            SET @PAG = (SELECT PAG FROM PROVE WHERE COD = @PROVE)" & vbCrLf
sConsulta = sConsulta & "            -- la acci�n ser� E - Cambio de estado o I Creaci�n orden de entrega)" & vbCrLf
sConsulta = sConsulta & "            IF @EST = 2  " & vbCrLf
sConsulta = sConsulta & "              SET @ACCION='I'" & vbCrLf
sConsulta = sConsulta & "            ELSE " & vbCrLf
sConsulta = sConsulta & "              BEGIN" & vbCrLf
sConsulta = sConsulta & "                IF @EST=20 " & vbCrLf
sConsulta = sConsulta & "                  SET @ACCION='D'" & vbCrLf
sConsulta = sConsulta & "                ELSE" & vbCrLf
sConsulta = sConsulta & "                  SET @ACCION='E'" & vbCrLf
sConsulta = sConsulta & "              END" & vbCrLf
sConsulta = sConsulta & "           " & vbCrLf
sConsulta = sConsulta & "            IF @NOGRABAR=0" & vbCrLf
sConsulta = sConsulta & "              BEGIN" & vbCrLf
sConsulta = sConsulta & "                INSERT INTO LOG_ORDEN_ENTREGA (ACCION,ID_ORDEN_ENTREGA,ID_ORDEN_EST,ID_PEDIDO,ANYO,PROVE,NUM_PEDIDO,NUM_ORDEN,NUMEXT,TIPO,FECHA,IMPORTE,PAG,EST,INCORRECTA,COMENT,ORIGEN,PER,USU,MON,CAMBIO,EMPRESA,RECEPTOR,COD_PROVE_ERP,REFERENCIA)" & vbCrLf
sConsulta = sConsulta & "                SELECT @ACCION,@ID_ORDEN,@ID,PEDIDO,ORDEN_ENTREGA.ANYO,PROVE,PEDIDO.NUM,ORDEN_ENTREGA.NUM,NUMEXT,ORDEN_ENTREGA.TIPO,ORDEN_ENTREGA.FECHA,IMPORTE,@PAG,@EST,INCORRECTA,@COMENT,@ORIGEN,@PER,@USU, MON,CAMBIO,EMPRESA,RECEPTOR,COD_PROVE_ERP,REFERENCIA FROM ORDEN_ENTREGA INNER JOIN PEDIDO ON PEDIDO.ID = ORDEN_ENTREGA.PEDIDO WHERE ORDEN_ENTREGA.ID = @ID_ORDEN" & vbCrLf
sConsulta = sConsulta & "                SET @ID_LOG_ORDEN_ENTREGA = (SELECT MAX(ID) FROM LOG_ORDEN_ENTREGA)" & vbCrLf
sConsulta = sConsulta & "                -- En el caso de Emisi�n de Pedido Directo, todav�a no estar�n creadas las lineas de pedido, se hace en el trigger LINEAS_PEDIDO_TG_INS" & vbCrLf
sConsulta = sConsulta & "                INSERT INTO LOG_LINEAS_PEDIDO (ACCION,ID_LOG_ORDEN_ENTREGA,ID_LINEAS_PEDIDO,ID_ORDEN_EST,ART_INT,UP,PREC_UP,CANT_PED,DEST,ANYO,GMN1,GMN2,GMN3,GMN4,PROCE,ITEM,ENTREGA_OBL,FECENTREGA,FECENTREGAPROVE,EST,CAT1,CAT2,CAT3,CAT4,CAT5,OBS,FC)" & vbCrLf
sConsulta = sConsulta & "                SELECT @ACCION,@ID_LOG_ORDEN_ENTREGA,ID,@ID,ART_INT,UP,PREC_UP,CANT_PED,DEST,ANYO,GMN1,GMN2,GMN3,GMN4,PROCE,ITEM,ENTREGA_OBL,FECENTREGA,FECENTREGAPROVE,EST,CAT1,CAT2,CAT3,CAT4,CAT5,OBS,FC FROM LINEAS_PEDIDO WHERE  ORDEN = @ID_ORDEN" & vbCrLf
sConsulta = sConsulta & "              END" & vbCrLf
sConsulta = sConsulta & "            IF @ERP=1 " & vbCrLf
sConsulta = sConsulta & "              BEGIN" & vbCrLf
sConsulta = sConsulta & "                IF @COD_ERP IS NOT NULL" & vbCrLf
sConsulta = sConsulta & "                    BEGIN" & vbCrLf
sConsulta = sConsulta & "                    INSERT INTO LOG_ORDEN_ENTREGA (ACCION,ID_ORDEN_ENTREGA,ID_ORDEN_EST,ID_PEDIDO,ANYO,PROVE,NUM_PEDIDO,NUM_ORDEN,NUMEXT,TIPO,FECHA,IMPORTE,PAG,EST,INCORRECTA,COMENT,ORIGEN,PER,USU,MON,CAMBIO,EMPRESA,RECEPTOR,COD_PROVE_ERP,REFERENCIA)" & vbCrLf
sConsulta = sConsulta & "                        SELECT @ACCION,@ID_ORDEN,@ID,PEDIDO,ORDEN_ENTREGA.ANYO,PROVE,PEDIDO.NUM,ORDEN_ENTREGA.NUM,NUMEXT,ORDEN_ENTREGA.TIPO,ORDEN_ENTREGA.FECHA,IMPORTE,@PAG,@EST,INCORRECTA,@COMENT,0,@PER,@USU, MON,CAMBIO,EMPRESA,RECEPTOR,COD_PROVE_ERP,REFERENCIA FROM ORDEN_ENTREGA INNER JOIN PEDIDO ON PEDIDO.ID = ORDEN_ENTREGA.PEDIDO WHERE ORDEN_ENTREGA.ID = @ID_ORDEN" & vbCrLf
sConsulta = sConsulta & "                    SET @ID_LOG_ORDEN_ENTREGA = (SELECT MAX(ID) FROM LOG_ORDEN_ENTREGA)" & vbCrLf
sConsulta = sConsulta & "                    INSERT INTO LOG_LINEAS_PEDIDO (ACCION,ID_LOG_ORDEN_ENTREGA,ID_LINEAS_PEDIDO,ID_ORDEN_EST,ART_INT,UP,PREC_UP,CANT_PED,DEST,ANYO,GMN1,GMN2,GMN3,GMN4,PROCE,ITEM,ENTREGA_OBL,FECENTREGA,FECENTREGAPROVE,EST,CAT1,CAT2,CAT3,CAT4,CAT5,OBS,FC)" & vbCrLf
sConsulta = sConsulta & "                        SELECT @ACCION,@ID_LOG_ORDEN_ENTREGA,ID,@ID,ART_INT,UP,PREC_UP,CANT_PED,DEST,ANYO,GMN1,GMN2,GMN3,GMN4,PROCE,ITEM,ENTREGA_OBL,FECENTREGA,FECENTREGAPROVE,EST,CAT1,CAT2,CAT3,CAT4,CAT5,OBS,FC FROM LINEAS_PEDIDO WHERE  ORDEN = @ID_ORDEN" & vbCrLf
sConsulta = sConsulta & "                    END" & vbCrLf
sConsulta = sConsulta & "              END" & vbCrLf
sConsulta = sConsulta & "          END --IF ACTIVA" & vbCrLf
sConsulta = sConsulta & "      END -- IF EST" & vbCrLf
sConsulta = sConsulta & "   END -- IF @EXTERNO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "  UPDATE ORDEN_ENTREGA SET ORDEN_EST = @ID WHERE ORDEN_ENTREGA.ID = @ID_ORDEN" & vbCrLf
sConsulta = sConsulta & "  FETCH NEXT FROM curTG_ORDEN_EST_Ins INTO @EST,@ID,@ID_ORDEN,@PER,@COMENT,@EXTERNO" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "END" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "CLOSE curTG_ORDEN_EST_Ins" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
sConsulta = sConsulta & "DEALLOCATE curTG_ORDEN_EST_Ins" & vbCrLf
sConsulta = sConsulta & "" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

End Sub

Private Sub V_2_16_5_Tablas_10()
Dim sConsulta As String
Dim rdores As RDO.rdoResultset

'Set rdores = gRDOCon.OpenResultset("SELECT COD FROM IDIOMAS", rdOpenForwardOnly, rdConcurReadOnly, rdExecDirect)
'While Not rdores.eof
    sConsulta = "if NOT exists (SELECT * FROM SYSCOLUMNS INNER JOIN SYSOBJECTS ON SYSOBJECTS.ID=SYSCOLUMNS.ID WHERE SYSOBJECTS.NAME='WEBTEXT' AND SYSCOLUMNS.NAME='TEXT_GER') "
    sConsulta = sConsulta & "ALTER TABLE WEBTEXT ADD TEXT_GER VARCHAR(2000) NULL"
    ExecuteSQL gRDOCon, sConsulta
'    rdores.MoveNext
'Wend
'rdores.Close
'Set rdores = Nothing
End Sub


Public Function CodigoDeActualizacion2_16_05_10A2_16_05_11() As Boolean
Dim bTransaccionEnCurso As Boolean
Dim sConsulta As String
Dim iRespuesta As Integer

On Error GoTo Error
sConsulta = "BEGIN TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = True
ExecuteSQL gRDOCon, "SET XACT_ABORT OFF"

frmProgreso.lblDetalle = "Modificar stored procedure"
frmProgreso.lblDetalle.Refresh
If frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Max Then frmProgreso.ProgressBar1.Value = 1
frmProgreso.ProgressBar1.Value = frmProgreso.ProgressBar1.Value + 1


V_2_16_5_Storeds_11


sConsulta = "UPDATE VERSION SET NUM ='2.16.05.11'" & ", FECHA=CONVERT(datetime,'" & Format(Date, "dd/mm/yyyy") & "',103)"
ExecuteSQL gRDOCon, sConsulta

sConsulta = "COMMIT TRANSACTION"
ExecuteSQL gRDOCon, sConsulta
bTransaccionEnCurso = False

CodigoDeActualizacion2_16_05_10A2_16_05_11 = True
Exit Function
    
Error:

    If NumTransaccionesAbiertas(gRDOCon) > 0 Then
        ExecuteSQL gRDOCon, "ROLLBACK TRANSACTION"
    End If
    
    MsgBox rdoErrors(rdoErrors.Count - 1).Description, vbCritical + vbOKOnly
    
    CodigoDeActualizacion2_16_05_10A2_16_05_11 = False

End Function

Private Sub V_2_16_5_Storeds_11()
Dim sConsulta As String

sConsulta = ""
sConsulta = sConsulta & "if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FSEP_NUM_PED]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)" & vbCrLf
sConsulta = sConsulta & "drop procedure [dbo].[FSEP_NUM_PED]" & vbCrLf
ExecuteSQL gRDOCon, sConsulta

sConsulta = "CREATE PROCEDURE  FSEP_NUM_PED (@PROVE VARCHAR(50),@NUMORDEN INT OUTPUT, @NUMORDENNUE INT OUTPUT)  AS" & vbCrLf
sConsulta = sConsulta & "SET @NUMORDEN=(SELECT COUNT(*) " & vbCrLf
sConsulta = sConsulta & "                 FROM ORDEN_ENTREGA OE" & vbCrLf
sConsulta = sConsulta & "                      INNER JOIN PEDIDO P" & vbCrLf
sConsulta = sConsulta & "                              ON OE.PEDIDO = P.ID" & vbCrLf
sConsulta = sConsulta & "                WHERE OE.PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "                  AND (OE.EST=2 OR OE.EST=3 OR OE.EST=4 OR OE.EST=5 or OE.EST=21) )" & vbCrLf
sConsulta = sConsulta & "SET @NUMORDENNUE=(SELECT COUNT(*) " & vbCrLf
sConsulta = sConsulta & "                    FROM ORDEN_ENTREGA OE" & vbCrLf
sConsulta = sConsulta & "                      INNER JOIN PEDIDO P" & vbCrLf
sConsulta = sConsulta & "                              ON OE.PEDIDO = P.ID" & vbCrLf
sConsulta = sConsulta & "                   WHERE OE.PROVE=@PROVE " & vbCrLf
sConsulta = sConsulta & "                     AND OE.EST=2)" & vbCrLf
ExecuteSQL gRDOCon, sConsulta


End Sub

