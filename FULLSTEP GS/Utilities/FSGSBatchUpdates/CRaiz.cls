VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CRaiz"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False


Option Explicit
Private oConexion As CConexion
Private bValidado As Boolean

Private Sub Validacion()

    If bValidado = False Then
        On Error Resume Next
        oConexion.ADOCon.Close
        Set oConexion = Nothing
        On Error GoTo 0
        Err.Raise vbObjectError, "Root Object Model", "El m�todo necesita autentificaci�n."
    End If
    
End Sub

Public Function Conectar(ByVal sInstancia As String, Optional ByVal sServidor As String, Optional ByVal sBaseDatos As String) As Integer

    ''' Abrir una conexi�n a la base de datos
    ''' con los datos de FSDB.LIC
        
    Dim summitconn As String
    Dim oADOConnection As ADODB.Connection
    Dim sKeyString As String
    
    Conectar = 0
    
'    Validacion
        
    On Error GoTo ErrorDeConexion
    
    If sServidor = "" Or sBaseDatos = "" Then
        sServidor = GetStringSetting("FSGSNotifierService", sInstancia, "Conexion", "Servidor")
        sBaseDatos = GetStringSetting("FSGSNotifierService", sInstancia, "Conexion", "BaseDeDatos")
    End If
    
    Set oADOConnection = New ADODB.Connection
    oADOConnection.Open "Provider=SQLOLEDB.1;Auto Translate=FALSE;Server=" & sServidor & ";Database=" & sBaseDatos & ";", LICDBLogin, LICDBContra
    oADOConnection.CursorLocation = adUseClient
    oADOConnection.CommandTimeout = 120

    Set oConexion = New CConexion
    Set oConexion.ADOCon = oADOConnection
    
    'Inicializo la conexion de use server pero no la abro
    
    
    Conectar = 0
    Exit Function
    
ErrorDeConexion:
    
        Conectar = 1
    
End Function

Public Function Desconectar()
    
    ''' Cerrar la conexi�n a la BD
    
    'Validacion
    
    On Error Resume Next
    
    oConexion.ADOCon.Close
    Set oConexion = Nothing
            

    
End Function

Public Function Generar_CVariablesCalidad() As CVariablesCalidad
    Validacion
    
    Set Generar_CVariablesCalidad = New CVariablesCalidad
    Set Generar_CVariablesCalidad.Conexion = oConexion

End Function

Public Function Generar_CVariableCalidad() As CVariableCalidad
    Validacion
    
    Set Generar_CVariableCalidad = New CVariableCalidad
    Set Generar_CVariableCalidad.Conexion = oConexion

End Function

'*****************************************************************************************************
'*** Descripci�n: Genera una instancia de la clase cDenominacionesPM y establece la conexi�n.    *****
'*****************************************************************************************************
Public Function Generar_CDenominacionesPM() As cDenominacionesPM
        
    Set Generar_CDenominacionesPM = New cDenominacionesPM
    Set Generar_CDenominacionesPM.Conexion = oConexion

End Function

'*****************************************************************************************************
'*** Descripci�n: Genera una instancia de la clase cVisoresPM y establece la conexi�n.           *****
'*****************************************************************************************************
Public Function Generar_CVisoresPM() As cVisoresPM
        
    Set Generar_CVisoresPM = New cVisoresPM
    Set Generar_CVisoresPM.Conexion = oConexion

End Function

''*****************************************************************************************************
''*** Descripci�n: Genera una instancia de la clase cVisoresQA y establece la conexi�n.           *****
''*****************************************************************************************************
Public Function Generar_CVisoresQA() As cVisoresQA

    Set Generar_CVisoresQA = New cVisoresQA
    Set Generar_CVisoresQA.Conexion = oConexion

End Function

''*****************************************************************************************************
''*** Descripci�n: Genera una instancia de la clase CVisoresContr y establece la conexi�n.        *****
''*****************************************************************************************************
Public Function Generar_CVisoresContr() As CVisoresContr

    Set Generar_CVisoresContr = New CVisoresContr
    Set Generar_CVisoresContr.Conexion = oConexion

End Function

