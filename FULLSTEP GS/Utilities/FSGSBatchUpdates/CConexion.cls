VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CConexion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"

Private mvarADOCon  As ADODB.Connection
'Private mvarADOErs As adodb.Errors


Friend Property Set ADOCon(ByVal vData As ADODB.Connection)
    Set mvarADOCon = vData
End Property


Friend Property Get ADOCon() As ADODB.Connection
    Set ADOCon = mvarADOCon
End Property

'Public Property Set ADOErs(ByVal vData As adodb.Errors)
'    Set mvarADOErs = vData
'End Property
'
'
'Public Property Get ADOErs() As adodb.Errors
'    Set ADOErs = mvarADOErs
'End Property


Private Sub Class_Terminate()

Dim I As Integer

Set mvarADOCon = Nothing

For I = 1 To 5
    I = DesactivarConexion(I)
Next I

End Sub
