Attribute VB_Name = "basActCodigos"
Option Explicit

Private Declare Function GetComputerName Lib "kernel32" Alias "GetComputerNameA" _
    (ByVal lpBuffer As String, nSize As Long) As Long

Public Type LongitudesDeCodigos    'jpa ojo esto es lo de Asier

    giLongCodART As Integer
    giLongCodCAL As Integer
    giLongCodCOM As Integer
    giLongCodDEP As Integer
    giLongCodDEST As Integer
    giLongCodEQP As Integer
    giLongCodGMN1 As Integer
    giLongCodGMN2 As Integer
    giLongCodGMN3 As Integer
    giLongCodGMN4 As Integer
    giLongCodMON As Integer
    giLongCodOFEEST As Integer
    giLongCodPAG As Integer
    giLongCodPAI As Integer
    giLongCodPER As Integer
    giLongCodPERF As Integer
    giLongCodPRESCON1 As Integer
    giLongCodPRESCON2 As Integer
    giLongCodPRESCON3 As Integer
    giLongCodPRESCON4 As Integer
    giLongCodPRESCONCEP31 As Integer
    giLongCodPRESCONCEP32 As Integer
    giLongCodPRESCONCEP33 As Integer
    giLongCodPRESCONCEP34 As Integer
    giLongCodPRESCONCEP41 As Integer
    giLongCodPRESCONCEP42 As Integer
    giLongCodPRESCONCEP43 As Integer
    giLongCodPRESCONCEP44 As Integer
    giLongCodPRESPROY1 As Integer
    giLongCodPRESPROY2 As Integer
    giLongCodPRESPROY3 As Integer
    giLongCodPRESPROY4 As Integer
    giLongCodPROVE As Integer
    giLongCodPROVI As Integer
    giLongCodROL As Integer
    giLongCodUNI As Integer
    giLongCodUON1 As Integer
    giLongCodUON2 As Integer
    giLongCodUON3 As Integer
    giLongCodUSU As Integer
    giLongCodACT1 As Integer
    giLongCodACT2 As Integer
    giLongCodACT3 As Integer
    giLongCodACT4 As Integer
    giLongCodACT5 As Integer
    giLongCia As Integer
    giLongCodCAT1 As Integer
    giLongCodCAT2 As Integer
    giLongCodCAT3 As Integer
    giLongCodCAT4 As Integer
    giLongCodCAT5 As Integer
    giLongCodGRUPOPROCE As Long
    giLongCodVarCal As Integer
    giLongCodViaPag As Integer
End Type

Public Type DenominacionesArticulo
    gsDEN_SPA As String
    gsDEN_ENG As String
    gsDEN_GER As String
    gsDenominacion As String
End Type


Public Type ParametrosGenerales    'jpa ojo esto es lo de Asier
    gsFSP_BD As String
    gsFSP_SRV As String
    gsFSP_CIA As String
    giFSP_CIA As Integer
    giPortal As Long
    giINSTWEB As Long
    gbUnaCompradora As Boolean
    giWinSecurity As Integer
    giWinSecurityWeb As Integer
    gsWinSecDomain As String
    gsWinSecAdminUser As String
    gsWinSecAdminPwd As String
End Type

'***********************************************************
' Tipos de datos para INTEGRACION
'***********************************************************

' Tipo de dato para las distintas entidades a integrar
Public Enum EntidadIntegracion
    Mon = 1
    Pai = 2
    Provi = 3
    Pag = 4
    Dest = 5
    Uni = 6
    art4 = 7
    Prove = 8
    Adj = 9
    Recibo = 10
    con = 101
    PED_Aprov = 11
    Rec_Aprov = 12
    PED_directo = 13
    Rec_Directo = 14
    Materiales = 15
    PresArt = 16
    
    SolicitudPM = 18
    
    Presupuestos1 = 19
    Presupuestos2 = 20
    Presupuestos3 = 21
    Presupuestos4 = 22
    Proceso = 23
    TablasExternas = 24
    ViaPag = 25
End Enum

Public Enum SentidoIntegracion
    Salida = 1
    Entrada = 2
    EntradaSalida = 3
End Enum

Public Enum OrigenIntegracion
    FSGSInt = 0
    FSGSReg = 1
    FSGSIntReg = 2
    ERP = 3
End Enum

Public gLongitudesDeCodigos As LongitudesDeCodigos 'jpa ojo esto es lo de Asier
Public gParametrosGenerales As ParametrosGenerales 'jpa ojo esto es lo de Asier

Dim m_Origen As Variant
''' Revisado por: Jbg. Fecha: 05/03/2012
''' <summary>
''' Procedimiento que devuelve las denominaciones en los distintos idiomas de un articulo
''' </summary>
''' <param name="sGMN1">GMN1 de tabla GMN4</param>
''' <param name="sGMN2">GMN2 de tabla GMN4</param>
''' <param name="sGMN3">GMN3 de tabla GMN4</param>
''' <param name="sGMN4">COD de tabla GMN4</param>
''' <returns>las denominaciones en los distintos idiomas de un articulo</returns>
''' <remarks>Llamada desde: ActualizarArticulo ; Tiempo m�ximo: 0</remarks>
Public Function DevolverDenominacionesArticulo(sGMN1 As String, sGMN2 As String, sGMN3 As String, sGMN4 As String) As DenominacionesArticulo
    Dim oDenominaciones As DenominacionesArticulo
    Dim adoComm As ADODB.Command
    Dim adoParam As ADODB.Parameter
    Dim sConsulta As String
    Dim rs As New ADODB.Recordset
    
    sConsulta = "SELECT DEN_SPA,DEN_ENG,DEN_GER FROM GMN4 WITH (NOLOCK) WHERE GMN1=? AND GMN2=? AND GMN3=? AND COD=?"
    
    Set adoComm = New ADODB.Command
    Set adoComm.ActiveConnection = g_adoconnCon
    adoComm.CommandText = sConsulta
    adoComm.CommandType = adCmdText
    adoComm.Prepared = True
    
    Set adoParam = adoComm.CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=gLongitudesDeCodigos.giLongCodGMN1, Value:=StrToVbNULL(sGMN1))
    adoComm.Parameters.Append adoParam
    Set adoParam = adoComm.CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=gLongitudesDeCodigos.giLongCodGMN2, Value:=StrToVbNULL(sGMN2))
    adoComm.Parameters.Append adoParam
    Set adoParam = adoComm.CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=gLongitudesDeCodigos.giLongCodGMN3, Value:=StrToVbNULL(sGMN3))
    adoComm.Parameters.Append adoParam
    Set adoParam = adoComm.CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=gLongitudesDeCodigos.giLongCodGMN4, Value:=StrToVbNULL(sGMN4))
    adoComm.Parameters.Append adoParam
    
    Set rs = adoComm.Execute
    
    If Not rs.eof Then
        oDenominaciones.gsDEN_SPA = rs.Fields("DEN_SPA").Value
        oDenominaciones.gsDEN_ENG = rs.Fields("DEN_ENG").Value
        oDenominaciones.gsDEN_GER = rs.Fields("DEN_GER").Value
        oDenominaciones.gsDenominacion = "SPA|" & oDenominaciones.gsDEN_SPA & "#ENG|" & oDenominaciones.gsDEN_ENG & "#GER|" & oDenominaciones.gsDEN_GER
    Else
        oDenominaciones.gsDEN_SPA = ""
        oDenominaciones.gsDEN_ENG = ""
        oDenominaciones.gsDEN_GER = ""
        oDenominaciones.gsDenominacion = ""
    End If
    DevolverDenominacionesArticulo = oDenominaciones
End Function

Public Sub DevolverLongitudesDeCodigos()
    
    Dim rs As New ADODB.Recordset
    
    rs.Open "SELECT LONGITUD,NOMBRE FROM DIC WITH (NOLOCK) ORDER BY ID", g_adoconnCon, adOpenForwardOnly, adLockReadOnly
    
    While Not rs.eof
            
        Select Case rs(1).Value
        Case "ART"
            gLongitudesDeCodigos.giLongCodART = rs(0).Value
        Case "CAL"
            gLongitudesDeCodigos.giLongCodCAL = rs(0).Value
        Case "COM"
            gLongitudesDeCodigos.giLongCodCOM = rs(0).Value
        'Case "CON"
        '    gLongitudesDeCodigos.giLongCodCON = rs(0).Value
        Case "DEP"
            gLongitudesDeCodigos.giLongCodDEP = rs(0).Value
        Case "DEST"
            gLongitudesDeCodigos.giLongCodDEST = rs(0).Value
        Case "EQP"
            gLongitudesDeCodigos.giLongCodEQP = rs(0).Value
        Case "GMN1"
            gLongitudesDeCodigos.giLongCodGMN1 = rs(0).Value
        Case "GMN2"
            gLongitudesDeCodigos.giLongCodGMN2 = rs(0).Value
        Case "GMN3"
            gLongitudesDeCodigos.giLongCodGMN3 = rs(0).Value
        Case "GMN4"
            gLongitudesDeCodigos.giLongCodGMN4 = rs(0).Value
        Case "MON"
            gLongitudesDeCodigos.giLongCodMON = rs(0).Value
        Case "OFEEST"
            gLongitudesDeCodigos.giLongCodOFEEST = rs(0).Value
        Case "PAG"
            gLongitudesDeCodigos.giLongCodPAG = rs(0).Value
        Case "PAI"
            gLongitudesDeCodigos.giLongCodPAI = rs(0).Value
        Case "PER"
            gLongitudesDeCodigos.giLongCodPER = rs(0).Value
        Case "PERF"
            gLongitudesDeCodigos.giLongCodPERF = rs(0).Value
        Case "PRESCON1"
            gLongitudesDeCodigos.giLongCodPRESCON1 = rs(0).Value
        Case "PRESCON2"
            gLongitudesDeCodigos.giLongCodPRESCON2 = rs(0).Value
        Case "PRESCON3"
            gLongitudesDeCodigos.giLongCodPRESCON3 = rs(0).Value
        Case "PRESCON4"
            gLongitudesDeCodigos.giLongCodPRESCON4 = rs(0).Value
        Case "PRESPROY1"
            gLongitudesDeCodigos.giLongCodPRESPROY1 = rs(0).Value
        Case "PRESPROY2"
            gLongitudesDeCodigos.giLongCodPRESPROY2 = rs(0).Value
        Case "PRESPROY3"
            gLongitudesDeCodigos.giLongCodPRESPROY3 = rs(0).Value
        Case "PRESPROY4"
            gLongitudesDeCodigos.giLongCodPRESPROY4 = rs(0).Value
        Case "PROVE"
            gLongitudesDeCodigos.giLongCodPROVE = rs(0).Value
        Case "PROVI"
            gLongitudesDeCodigos.giLongCodPROVI = rs(0).Value
        Case "ROL"
            gLongitudesDeCodigos.giLongCodROL = rs(0).Value
        Case "UNI"
            gLongitudesDeCodigos.giLongCodUNI = rs(0).Value
        Case "UON1"
            gLongitudesDeCodigos.giLongCodUON1 = rs(0).Value
        Case "UON2"
            gLongitudesDeCodigos.giLongCodUON2 = rs(0).Value
        Case "UON3"
            gLongitudesDeCodigos.giLongCodUON3 = rs(0).Value
        Case "USU"
            gLongitudesDeCodigos.giLongCodUSU = rs(0).Value
        Case "ACT"
            gLongitudesDeCodigos.giLongCodACT1 = rs(0).Value
        Case "SUBACT1"
            gLongitudesDeCodigos.giLongCodACT2 = rs(0).Value
        Case "SUBACT2"
            gLongitudesDeCodigos.giLongCodACT3 = rs(0).Value
        Case "SUBACT3"
            gLongitudesDeCodigos.giLongCodACT4 = rs(0).Value
        Case "SUBACT4"
            gLongitudesDeCodigos.giLongCodACT5 = rs(0).Value
        Case "PRESCONCEP31"
            gLongitudesDeCodigos.giLongCodPRESCONCEP31 = rs(0).Value
        Case "PRESCONCEP32"
            gLongitudesDeCodigos.giLongCodPRESCONCEP32 = rs(0).Value
        Case "PRESCONCEP33"
            gLongitudesDeCodigos.giLongCodPRESCONCEP33 = rs(0).Value
        Case "PRESCONCEP34"
            gLongitudesDeCodigos.giLongCodPRESCONCEP34 = rs(0).Value
        Case "PRESCONCEP41"
            gLongitudesDeCodigos.giLongCodPRESCONCEP41 = rs(0).Value
        Case "PRESCONCEP42"
            gLongitudesDeCodigos.giLongCodPRESCONCEP42 = rs(0).Value
        Case "PRESCONCEP43"
            gLongitudesDeCodigos.giLongCodPRESCONCEP43 = rs(0).Value
        Case "PRESCONCEP44"
            gLongitudesDeCodigos.giLongCodPRESCONCEP44 = rs(0).Value
        Case "CAT1"
            gLongitudesDeCodigos.giLongCodCAT1 = rs(0).Value
        Case "CAT2"
            gLongitudesDeCodigos.giLongCodCAT2 = rs(0).Value
        Case "CAT3"
            gLongitudesDeCodigos.giLongCodCAT3 = rs(0).Value
        Case "CAT4"
            gLongitudesDeCodigos.giLongCodCAT4 = rs(0).Value
        Case "CAT5"
            gLongitudesDeCodigos.giLongCodCAT5 = rs(0).Value
        Case "GRUPO"
            gLongitudesDeCodigos.giLongCodGRUPOPROCE = rs(0).Value
        Case "DIC"

        Case "CONTR"
            
        Case "VARCAL"
            gLongitudesDeCodigos.giLongCodVarCal = rs(0).Value
            
        Case "VIAPAG"
            gLongitudesDeCodigos.giLongCodViaPag = rs(0).Value
        End Select
                        
        rs.MoveNext

    Wend
        
    rs.Close
    Set rs = Nothing
        
End Sub

''' <summary>Esta funci�n cambia de codigo del proveedor</summary>
''' <param name="lID">ID del registro</param>
''' <param name="CodViejo">Codigo viejo</param>
''' <param name="CodNuevo">Codigo nuevo</param>
''' <param name="Usuario">Usuario que realiza el cambio</param>
''' <returns>True si no hay errores, False si hay errores</returns>
''' <remarks>Llamada desde: ActualizarCodigos()

Private Function ActualizarCodigoProveedor(ByVal lID As Long, ByVal CodViejo As String, ByVal CodNuevo As String, ByVal Usuario As String) As Boolean
    Dim sConsulta As String
    Dim sFSP As String
    Dim adoComm As ADODB.Command
    Dim adoParam As ADODB.Parameter
    Dim adoRes As ADODB.Recordset
    
    On Error GoTo Error
    
    g_adoconnCon.Execute "BEGIN DISTRIBUTED TRAN"
    g_adoconnCon.Execute "SET XACT_ABORT ON"
        
    'Ejecutar el SP PROVE_COD
    Set adoComm = New ADODB.Command
    With adoComm
        .ActiveConnection = g_adoconnCon
        .CommandType = adCmdStoredProc
        .CommandText = "PROVE_COD"
        .CommandTimeout = 1200   '20 min
        
        Set adoParam = .CreateParameter("OLD", adVarChar, adParamInput, 50, CodViejo)
        .Parameters.Append adoParam
        Set adoParam = .CreateParameter("NEW", adVarChar, adParamInput, 50, CodNuevo)
        .Parameters.Append adoParam
        
        .Execute
    End With
    If g_adoconnCon.Errors.Count > 0 Then Err.Raise g_adoconnCon.Errors(0).Number
    
    'Ejecutar el SP PROVE_COD del portal
    If gParametrosGenerales.giINSTWEB = 2 Then
        sFSP = gParametrosGenerales.gsFSP_SRV & "." & gParametrosGenerales.gsFSP_BD & ".dbo."
        
        Set adoComm = New ADODB.Command
        With adoComm
            Set .ActiveConnection = g_adoconnCon
            .CommandType = adCmdStoredProc
            .CommandText = sFSP & "SP_MODIF_PROVECOD"
            .CommandTimeout = 1200   '20 min
            .Prepared = True
            
            Set adoParam = .CreateParameter("OLDCOD", adVarChar, adParamInput, 20, CodViejo)
            .Parameters.Append adoParam
            Set adoParam = .CreateParameter("NEWCOD", adVarChar, adParamInput, 20, CodNuevo)
            .Parameters.Append adoParam
            Set adoParam = .CreateParameter("CIACOMP", adInteger, adParamInput, , gParametrosGenerales.giFSP_CIA)
            .Parameters.Append adoParam
                
            .Execute
        End With
        If g_adoconnCon.Errors.Count > 0 Then Err.Raise g_adoconnCon.Errors(0).Number
    End If

    If GrabarEnLogEntidad(EntidadIntegracion.Prove) Then
        sConsulta = "SELECT COD FROM ERP"
        Set adoComm = New ADODB.Command
        With adoComm
            Set .ActiveConnection = g_adoconnCon
            .CommandType = adCmdText
            .CommandText = sConsulta
            .Prepared = True
            
            Set adoRes = .Execute
        End With
                       
        While Not adoRes.eof
            Set adoComm = New ADODB.Command
            
            sConsulta = "INSERT INTO LOG_PROVE (ACCION,COD,COD_NEW,DEN,DIR,CP,POB,PAI,PROVI,MON,VAL1,VAL2,VAL3,CAL1,CAL2,CAL3,OBS,NIF,URLPROVE,PAG,PED_FORM,ORIGEN,USU,ERP)" & _
                        " SELECT ?,?,?,PROVE.DEN,PROVE.DIR,PROVE.CP,PROVE.POB,PROVE.PAI,PROVE.PROVI,PROVE.MON,PROVE.VAL1," & _
                        "PROVE.VAL2,PROVE.VAL3,PROVE.CAL1,PROVE.CAL2,PROVE.CAL3,PROVE.OBS,PROVE.NIF,PROVE.URLPROVE,PROVE.PAG," & _
                        "PROVE.PED_FORM,?,?,? FROM PROVE WITH (NOLOCK) WHERE COD=?"
            With adoComm
                Set .ActiveConnection = g_adoconnCon
                .CommandType = adCmdText
                .CommandText = sConsulta
                .Prepared = True
                
                Set adoParam = .CreateParameter("ACCION", adVarChar, adParamInput, 1, Accion_CambioCodigo)
                .Parameters.Append adoParam
                Set adoParam = .CreateParameter("COD", adVarChar, adParamInput, Len(CodViejo), CodViejo)
                .Parameters.Append adoParam
                Set adoParam = .CreateParameter("COD_NEW", adVarChar, adParamInput, Len(CodNuevo), CodNuevo)
                .Parameters.Append adoParam
                Set adoParam = .CreateParameter("ORIGEN", adTinyInt, adParamInput, Len(m_Origen), m_Origen)
                .Parameters.Append adoParam
                Set adoParam = .CreateParameter("USU", adVarChar, adParamInput, Len(Usuario), Usuario)
                .Parameters.Append adoParam
                Set adoParam = .CreateParameter("ERP", adVarChar, adParamInput, Len(adoRes.Fields("COD").Value), adoRes.Fields("COD").Value)
                .Parameters.Append adoParam
                Set adoParam = .CreateParameter("CODPROVE", adVarChar, adParamInput, Len(CodNuevo), CodNuevo)
                .Parameters.Append adoParam
                
                .Execute
            End With
            If g_adoconnCon.Errors.Count > 0 Then Err.Raise g_adoconnCon.Errors(0).Number
            
            adoRes.MoveNext
        Wend
        adoRes.Close
        Set adoRes = Nothing
    End If

    g_adoconnCon.Execute "SET XACT_ABORT OFF"
    g_adoconnCon.Execute "COMMIT TRAN", , adExecuteNoRecords

    ActualizarCodigoProveedor = True
    
Salir:
    Set adoComm = Nothing
    Set adoParam = Nothing
    Set adoRes = Nothing
    Exit Function
Error:
    GrabarErrorBDCodigo g_adoconnCon.Errors, lID
    g_adoconnCon.Execute "IF @@TRANCOUNT>0 ROLLBACK TRANSACTION"
    g_adoconnCon.Execute "SET XACT_ABORT OFF"
    ActualizarCodigoProveedor = False
    Resume Salir
End Function

''' Revisado por: Jbg. Fecha: 05/03/2012
''' <summary>Esta funci�n cambia de codigo de usuario</summary>
''' <param name="lID">ID del registro</param>
''' <param name="CodViejo">Codigo viejo</param>
''' <param name="CodNuevo">Codigo nuevo</param>
''' <param name="Usuario">Usuario que realiza el cambio</param>
''' <param name="Pwd">Pwd nuevo</param>
''' <param name="FecUsu">FecUsu</param>
''' <param name="Adm">Adm</param>
''' <returns>True si no hay errores, False si hay errores</returns>
''' <remarks>Llamada desde: ActualizarCodigos()
Public Function ActualizarCodigoUsuario(ByVal lID As Long, ByVal CodViejo As String, ByVal CodNuevo As String, ByVal Usuario As String, ByVal Pwd As String, _
        ByVal FecUsu As Date, ByVal Adm As String) As Boolean
    Dim sConsulta As String
    Dim TESError As TipoErrorSummit
    Dim adocom As ADODB.Command
    Dim oParam As ADODB.Parameter
    Dim bAccesoFSEP As Boolean
    Dim bAccesoFSWS As Boolean
    Dim bAccesoFSGS As Boolean
    Dim bAccesoFSQA As Boolean
    Dim adoRes As ADODB.Recordset
    
    On Error GoTo Error
    
    TESError.NumError = 0   'TESnoerror
        
    g_adoconnCon.Execute "BEGIN DISTRIBUTED TRAN", , adExecuteNoRecords
    g_adoconnCon.Execute "SET XACT_ABORT ON"
    
    'Obtener datos del usuario
    Set adocom = New ADODB.Command
    With adocom
        .CommandType = adCmdText
        .CommandText = "SELECT FSEP,SOL_COMPRA,FSGS,FSQA FROM USU WITH (NOLOCK) WHERE COD=?"
        Set .ActiveConnection = g_adoconnCon
        
        Set oParam = .CreateParameter("COD", adVarChar, adParamInput, Len(CodViejo), CodViejo)
        .Parameters.Append oParam
        
        Set adoRes = .Execute
    End With
    If g_adoconnCon.Errors.Count > 0 Then Err.Raise g_adoconnCon.Errors(0).Number
    
    If Not adoRes.eof Then
        bAccesoFSEP = SQLBinaryToBoolean(adoRes.Fields("FSEP").Value)
        bAccesoFSWS = SQLBinaryToBoolean(adoRes.Fields("SOL_COMPRA").Value)
        bAccesoFSGS = SQLBinaryToBoolean(adoRes.Fields("FSGS").Value)
        bAccesoFSQA = SQLBinaryToBoolean(adoRes.Fields("FSQA").Value)
    End If
    
    'Ejecutar el SP
    'La PWD  que est� en el registro es la correspondiente al nuevo c�digo, luego no hace falta recalcularla
    Set adocom = New ADODB.Command
    With adocom
        .CommandType = adCmdStoredProc
        .CommandText = "USU_COD"
        .CommandTimeout = 1200   '20 min
        Set .ActiveConnection = g_adoconnCon
        
        Set oParam = .CreateParameter("OLD", adVarChar, adParamInput, 50, CodViejo)
        .Parameters.Append oParam
        Set oParam = .CreateParameter("NEW", adVarChar, adParamInput, 50, CodNuevo)
        .Parameters.Append oParam
        Set oParam = .CreateParameter("PWD", adVarChar, adParamInput, 512, Pwd)
        .Parameters.Append oParam
        Set oParam = .CreateParameter("FEC_USU", adDate, adParamInput, , FecUsu)
        .Parameters.Append oParam
        Set oParam = .CreateParameter("ADM", adVarChar, adParamInput, Len(Adm), Adm)
        .Parameters.Append oParam
        
        .Execute
    End With
    If g_adoconnCon.Errors.Count > 0 Then Err.Raise g_adoconnCon.Errors(0).Number
    Set adocom = Nothing
    
    'Hay que renombrar el usuario de windows
    'giWinSecurity = 1 --> Usuarios locales
    'gParametrosGenerales.giWinSecurityWeb = 1 --> Usuarios locales
    If (gParametrosGenerales.giWinSecurity = 1 And bAccesoFSGS) Or ((bAccesoFSWS Or bAccesoFSQA Or bAccesoFSEP) And gParametrosGenerales.giWinSecurityWeb = 1) Then
        TESError = RenameUserInServer(CodViejo, CodNuevo)
        If TESError.NumError <> 0 Then GoTo Error   '0=TESnoerror
    End If
    
    g_adoconnCon.Execute "SET XACT_ABORT OFF"
    g_adoconnCon.Execute "COMMIT TRAN", , adExecuteNoRecords
    
    ActualizarCodigoUsuario = True
    
Salir:
    Set adocom = Nothing
    Set oParam = Nothing
    Set adoRes = Nothing
    Exit Function
Error:
    GrabarErrorBDCodigo g_adoconnCon.Errors, lID
    If TESError.NumError <> 0 Then GrabarErrorCodigo TESError, lID
    ActualizarCodigoUsuario = False
    g_adoconnCon.Execute "IF @@TRANCOUNT>0 ROLLBACK TRANSACTION"
    g_adoconnCon.Execute "SET XACT_ABORT OFF"
    Resume Salir
End Function

''' <summary>Esta funci�n renombra el usuario de windows</summary>
''' <param name="sNewCod">Codigo nuevo</param>
''' <returns>variable de tipo TipoErrorSummit con el error si lo ha habido</returns>
''' <remarks>Llamada desde: ActualizarCodigoUsuario()
Private Function RenameUserInServer(ByVal sOldCod As String, ByVal sNewCod As String) As TipoErrorSummit

    Dim AdminUsers As New AdminUsers
    Dim resAdminUsers  As Integer
    Dim strAdminUsers As String
    Dim TESError As TipoErrorSummit
    
    On Error GoTo Error:
    
    TESError.NumError = 0   'TESnoerror

    AdminUsers.WinSecAdminUser = gParametrosGenerales.gsWinSecAdminUser
    AdminUsers.WinSecAdminPwd = gParametrosGenerales.gsWinSecAdminPwd
    
    AdminUsers.Instancia = basPublic.gInstancia
    
    resAdminUsers = AdminUsers.RenameUserInServer(sOldCod, sNewCod, strAdminUsers)
    
    If resAdminUsers = 1 Then
        'Error Impersonate
        TESError = basUtilidades.GrabarError("basActCodigos.RenameUserInServer ", "Error Impersonate", strAdminUsers)
                
    ElseIf resAdminUsers = 2 Then
        'Error en renombrar
        TESError = basUtilidades.GrabarError("basActCodigos.RenameUserInServer ", "Error en rename", strAdminUsers)
    End If
    
    RenameUserInServer = TESError
    
    Set AdminUsers = Nothing
    
    Exit Function

Error:
    TESError.NumError = 100 'TESOtroerror

    basUtilidades.GrabarError "basActCodigos.RenameUserInServer ", "Error en objeto", Err.Description
    
    RenameUserInServer = TESError
    
    Set AdminUsers = Nothing

End Function

''' Revisado por: Jbg. Fecha: 05/03/2012
''' <summary>Esta funci�n obtiene los par�metyros generales necesarios para los procesos de cambio de c�digo</summary>
''' <remarks>Llamada desde: ActualizarCodigos(); Tiempo m�ximo: 0,2</remarks>
Private Sub ObtenerParametrosGenerales()
    Dim sDomainName As String * 255
    Dim sAdmPwdCryp As String
    Dim adocom As ADODB.Command
    Dim oParam As ADODB.Parameter
    Dim adoRes As ADODB.Recordset
    Dim dFechaHoraCrypt As Date
    Dim sFSP As String
    Dim sConsulta As String
      
    Set adocom = New ADODB.Command
    With adocom
        .CommandType = adCmdText
        .CommandText = "SELECT INSTWEB,WIN_SEC,WIN_SEC_WEB FROM PARGEN_INTERNO WITH (NOLOCK) WHERE ID=? "
        Set .ActiveConnection = g_adoconnCon
        
        Set oParam = .CreateParameter("ID", adInteger, adParamInput, , 1)
        .Parameters.Append oParam
        
        Set adoRes = .Execute
    End With
    If Not adoRes.eof Then
        gParametrosGenerales.giINSTWEB = NullToStr(adoRes("INSTWEB").Value)
        gParametrosGenerales.giWinSecurity = adoRes("WIN_SEC").Value
        gParametrosGenerales.giWinSecurityWeb = adoRes("WIN_SEC_WEB").Value
    End If
    adoRes.Close
                      
    With adocom
        .CommandText = "SELECT FSP_SRV, FSP_BD, FSP_CIA FROM PARGEN_PORT WITH (NOLOCK) WHERE ID=? "
        
        Set adoRes = .Execute
    End With
    Set adocom = Nothing
    Set oParam = Nothing
    
    If Not adoRes.eof Then
        gParametrosGenerales.gsFSP_BD = NullToStr(adoRes("FSP_BD").Value)
        gParametrosGenerales.gsFSP_SRV = NullToStr(adoRes("FSP_SRV").Value)
        gParametrosGenerales.gsFSP_CIA = NullToStr(adoRes("FSP_CIA").Value)
    End If
    adoRes.Close
    Set adoRes = Nothing
    
    If gParametrosGenerales.giINSTWEB = 2 Then  'Con Portal
        sFSP = gParametrosGenerales.gsFSP_SRV & "." & gParametrosGenerales.gsFSP_BD & ".dbo."
        sConsulta = "SELECT PORT,ID FROM CIAS WITH (NOLOCK) WHERE COD='" & DblQuote(gParametrosGenerales.gsFSP_CIA) & "'"
        
        Set adoRes = New ADODB.Recordset
        Set adocom = New ADODB.Command
        Set adocom.ActiveConnection = g_adoconnCon
    
        adocom.CommandText = "EXEC " & sFSP & "sp_executesql @stmt=?"
        adocom.CommandType = adCmdText
        Set oParam = adocom.CreateParameter("stmt", adLongVarWChar, adParamInput, Len(sConsulta))
        adocom.Parameters.Append oParam
        adocom.CommandTimeout = 120
        adocom.Parameters("stmt").AppendChunk sConsulta
        Set adoRes = adocom.Execute
        
        If Not adoRes.eof Then
            gParametrosGenerales.giPortal = adoRes("PORT").Value
            gParametrosGenerales.giFSP_CIA = adoRes("ID").Value
        End If
        Set adocom = Nothing
        adoRes.Close
        Set adoRes = Nothing
    End If
        
    'Obtenemos el nombre de la m�quina
    GetComputerName sDomainName, 255
    gParametrosGenerales.gsWinSecDomain = Trim(sDomainName)
    
    'Obtenemos el usuario administrador de windows
    Set adocom = New ADODB.Command
    With adocom
        .CommandType = adCmdText
        .CommandText = "SELECT USU,PWD,FECPWD FROM WIN_ADM WITH (NOLOCK)"
        Set .ActiveConnection = g_adoconnCon
        
        Set adoRes = .Execute
    End With
    If Not adoRes.eof Then
        gParametrosGenerales.gsWinSecAdminUser = adoRes("USU").Value
        sAdmPwdCryp = adoRes("PWD").Value
        '' Desencriptar contrase�a
        dFechaHoraCrypt = adoRes("FECPWD").Value
        gParametrosGenerales.gsWinSecAdminPwd = EncriptarAES(gParametrosGenerales.gsWinSecAdminUser, sAdmPwdCryp, False, 2, dFechaHoraCrypt)
    End If
    adoRes.Close
    Set adoRes = Nothing
End Sub

Public Function ActualizarBBDD() As Long
    Dim lResp As Long
    lResp = ActualizarArticulo()
    lResp = lResp * ActualizarCodigos()
End Function

''' Revisado por: Jbg. Fecha: 05/03/2012
''' <summary>
''' Actualizar Codigos de las diferentes tablas sobre las q se permite cambio de codigo
''' </summary>
''' <returns>1 - ha ido bien 0- ha ido mal</returns>
''' <remarks>Llamada desde: ActualizarBBDD ; Tiempo m�ximo: 0</remarks>
Private Function ActualizarCodigos() As Long
    Dim sConsulta As String
    Dim adoRes As Recordset
    Dim bOK As Boolean
    Dim adoComm As ADODB.Command
    Dim adoParam As ADODB.Parameter
    Dim sPwdDecript As String
    Dim sPwdEncript As String
    Dim adoResPwd As Recordset
    Dim dtFecUsu As Date
    Dim sADM As String
    Dim bEsAdmin As Boolean
    
    On Error GoTo ERROR_SALIR
    
    ActualizarCodigos = 1
    
    Set adoComm = New ADODB.Command
    With adoComm
        sConsulta = "SELECT ID, ACCION, COD, COD_NEW, USU, CAMPOAUX FROM UPD_COD WITH (NOLOCK) WHERE OK=0 AND ERROR IS NULL"
        .ActiveConnection = g_adoconnCon
        .CommandType = adCmdText
        .CommandText = sConsulta
        .CommandTimeout = 1200   '20 min
        
        Set adoRes = .Execute
    End With
    
    If Not adoRes.eof Then
        'Obtener par�metyro generales
        ObtenerParametrosGenerales
        
        Do While Not adoRes.eof
            Select Case NullToStr(adoRes.Fields("ACCION").Value)
                Case "VP"
                    bOK = ActualizarCodigoViaPago(adoRes.Fields("ID").Value, NullToStr(adoRes.Fields("COD").Value), NullToStr(adoRes.Fields("COD_NEW").Value), NullToStr(adoRes.Fields("USU")))
                Case "MO"
                    bOK = ActualizarCodigoMoneda(adoRes.Fields("ID").Value, NullToStr(adoRes.Fields("COD").Value), NullToStr(adoRes.Fields("COD_NEW").Value), NullToStr(adoRes.Fields("USU")))
                Case "UN"
                    bOK = ActualizarCodigoUnidad(adoRes.Fields("ID").Value, NullToStr(adoRes.Fields("COD").Value), NullToStr(adoRes.Fields("COD_NEW").Value), NullToStr(adoRes.Fields("USU")))
                Case "FP"
                    bOK = ActualizarCodigoFormaPago(adoRes.Fields("ID").Value, NullToStr(adoRes.Fields("COD").Value), NullToStr(adoRes.Fields("COD_NEW").Value), NullToStr(adoRes.Fields("USU")))
                Case "PA" 'Pais
                    bOK = ActualizarCodigoPais(adoRes.Fields("ID").Value, NullToStr(adoRes.Fields("COD").Value), NullToStr(adoRes.Fields("COD_NEW").Value), NullToStr(adoRes.Fields("USU")))
                Case "PV" 'Provincia
                    bOK = ActualizarCodigoProvincia(adoRes.Fields("ID").Value, NullToStr(adoRes.Fields("COD").Value), NullToStr(adoRes.Fields("COD_NEW").Value), NullToStr(adoRes.Fields("USU")), NullToStr(adoRes.Fields("CAMPOAUX").Value))
                Case "PE" 'Persona
                    bOK = ActualizarCodigoPersona(adoRes.Fields("ID").Value, NullToStr(adoRes.Fields("COD").Value), NullToStr(adoRes.Fields("COD_NEW").Value), NullToStr(adoRes.Fields("USU")))
                Case "US"
                    Set adoComm = New ADODB.Command
                    With adoComm
                        sConsulta = "SELECT USU.PWD, USU.FECPWD,USU.FEC_USU,USU.ADM FROM USU WITH (NOLOCK) WHERE USU.COD=?"
                        .ActiveConnection = g_adoconnCon
                        .CommandType = adCmdText
                        .CommandText = sConsulta
                        .CommandTimeout = 1200   '20 min
                        
                        Set adoParam = .CreateParameter("COD", adVarChar, adParamInput, 100, adoRes.Fields("COD").Value)
                        .Parameters.Append adoParam
                        
                        Set adoResPwd = .Execute
                    End With
                
                    sPwdDecript = EncriptarAES(adoRes.Fields("COD").Value, adoResPwd.Fields("PWD").Value, False, 2, adoResPwd.Fields("FECPWD").Value)
                    sPwdEncript = EncriptarAES(adoRes.Fields("COD_NEW").Value, sPwdDecript, True, 2, adoResPwd.Fields("FECPWD").Value)
                    bEsAdmin = (adoRes.Fields("COD").Value = basUtilidades.EncriptarAES(adoRes.Fields("COD").Value, adoResPwd.Fields("ADM").Value, False, _
                                1, adoResPwd.Fields("FEC_USU").Value))
                    dtFecUsu = Now
                    If bEsAdmin Then
                        sADM = EncriptarAES(adoRes.Fields("COD_NEW").Value, adoRes.Fields("COD_NEW").Value, True, 1, dtFecUsu)
                    Else
                        sADM = EncriptarAES(adoRes.Fields("COD_NEW").Value, adoRes.Fields("COD_NEW").Value, True, 2, dtFecUsu)
                    End If
                    
                    bOK = ActualizarCodigoUsuario(adoRes.Fields("ID").Value, NullToStr(adoRes.Fields("COD").Value), NullToStr(adoRes.Fields("COD_NEW").Value), _
                                NullToStr(adoRes.Fields("USU")), sPwdEncript, dtFecUsu, sADM)
                Case "PR" 'Proveedor
                    bOK = ActualizarCodigoProveedor(adoRes.Fields("ID").Value, NullToStr(adoRes.Fields("COD").Value), NullToStr(adoRes.Fields("COD_NEW").Value), NullToStr(adoRes.Fields("USU")))
            End Select
            
            If bOK Then
                MarcarCambioOK adoRes.Fields("ID").Value
                If g_adoconnCon.Errors.Count > 0 Then GoTo ERROR_SALIR
            Else
                GoTo ERROR_SIGUE
            End If
            
ERROR_SIGUE:
            adoRes.MoveNext
        Loop
        
        adoRes.Close
    End If
    
Salir:
    Set adoRes = Nothing
    Set adoComm = Nothing
    Set adoParam = Nothing
    Exit Function
ERROR_SALIR:
    If Not adoRes Is Nothing Then adoRes.Close
    ActualizarCodigos = 0
    Resume Salir
End Function

Private Function ActualizarArticulo() As Long
Dim sConsulta As String
Dim adoRes As ADODB.Recordset
Dim fldId As ADODB.Field
Dim fldCod As ADODB.Field
Dim fldCOD_NEW As ADODB.Field
Dim fldDen As ADODB.Field
Dim fldGmn1 As ADODB.Field
Dim fldGmn2 As ADODB.Field
Dim fldGmn3 As ADODB.Field
Dim fldGMN4 As ADODB.Field
Dim fldUSU As ADODB.Field
Dim fldACCION As ADODB.Field
Dim objErrores As ADODB.Errors
Dim sMat As String
Dim sAccion As String
Dim rs As ADODB.Recordset

On Error GoTo ERROR_SALIR

    ActualizarArticulo = 1
    
    sConsulta = "SELECT * FROM UPD_ART4 WITH (NOLOCK) WHERE OK=0 AND ERROR IS NULL ORDER BY ID"
    Set adoRes = New ADODB.Recordset
    adoRes.Open sConsulta, g_adoconnCon, adOpenForwardOnly, adLockReadOnly
    
    If adoRes.eof Then
        
        'La tabla UPD_ART4 est� vacia y no hay que actualizar nada
        
        'FIN
        adoRes.Close
        Set adoRes = Nothing
        Exit Function
    
    Else
        
        'La tabla UPD_ART4 tiene datos que hay que actualizar
        
        Set fldId = adoRes.Fields("ID")
        Set fldCod = adoRes.Fields("COD")
        Set fldCOD_NEW = adoRes.Fields("COD_NEW")
        Set fldDen = adoRes.Fields("DEN")
        Set fldGmn1 = adoRes.Fields("GMN1")
        Set fldGmn2 = adoRes.Fields("GMN2")
        Set fldGmn3 = adoRes.Fields("GMN3")
        Set fldGMN4 = adoRes.Fields("GMN4")
        Set fldUSU = adoRes.Fields("USU")
        Set fldACCION = adoRes.Fields("ACCION")
        'Leemos una a una las filas de la tabla UPD_ART4
        While Not adoRes.eof
            sAccion = fldACCION.Value
            If fldACCION.Value = "D" Then
                
                'Actualizar la denominaci�n
                sMat = fldGmn1.Value & Mid$("                         ", 1, gLongitudesDeCodigos.giLongCodGMN1 - Len(fldGmn1.Value))
                sMat = sMat & fldGmn2.Value & Mid$("                         ", 1, gLongitudesDeCodigos.giLongCodGMN2 - Len(fldGmn2.Value))
                sMat = sMat & fldGmn3.Value & Mid$("                         ", 1, gLongitudesDeCodigos.giLongCodGMN3 - Len(fldGmn3.Value))
                sMat = sMat & fldGMN4.Value & Mid$("                         ", 1, gLongitudesDeCodigos.giLongCodGMN4 - Len(fldGMN4.Value))

                'SIN TRANSACCION PARA QUE BLOQUEE MENOS, SI DA ERROR QUEDAR�A ALGUNA TABLA SIN ACTUALIZAR
                'Actualiza la denominaci�n de la tabla ITEM, no bloqueamos tabla, no desabilitamos triggers
                Set rs = New ADODB.Recordset
                sConsulta = "SELECT count(*) FROM ITEM I WITH (NOLOCK)"
                sConsulta = sConsulta & " INNER JOIN PROCE P WITH (NOLOCK) ON I.ANYO=P.ANYO AND I.GMN1_PROCE=P.GMN1 AND I.PROCE=P.COD "
                sConsulta = sConsulta & " WHERE I.ART = '" & DblQuote(fldCod.Value) & "' AND P.EST>=1 AND P.EST<=11"
                rs.Open sConsulta, g_adoconnCon, adOpenForwardOnly, adLockReadOnly
                
                If rs(0).Value > 0 Then
                    'Actualiza la denominaci�n de la tabla ITEM
                    sConsulta = " UPDATE ITEM SET ITEM.DESCR = " & StrToSQLNULL(fldDen.Value)
                    sConsulta = sConsulta & " FROM ITEM I WITH (NOLOCK)"
                    sConsulta = sConsulta & " INNER JOIN PROCE P WITH (NOLOCK) ON I.PROCE=P.COD AND I.ANYO=P.ANYO AND I.GMN1_PROCE=P.GMN1 AND P.EST>=1 AND P.EST<=11"
                    sConsulta = sConsulta & " WHERE I.ART = '" & DblQuote(fldCod.Value) & "'"
                    
                    g_adoconnCon.Execute sConsulta
                    If g_adoconnCon.Errors.Count > 0 Then 'Si error en UPDATE DEN
                        Set objErrores = g_adoconnCon.Errors
                        GrabarError objErrores, fldId
                        GoTo ERROR_SIGUE
                    End If
                End If
                rs.Close
                'Actualiza la denominaci�n de la tabla LINEAS_PEDIDO, no bloqueamos tabla, no desabilitamos triggers
                'Actualizamos todos los pedidos con descr y el articulo no sea generico
                sConsulta = "SELECT COUNT(*) FROM LINEAS_PEDIDO LP WITH (NOLOCK)"
                sConsulta = sConsulta & " INNER JOIN ORDEN_ENTREGA OE WITH (NOLOCK) ON LP.ORDEN=OE.ID "
                sConsulta = sConsulta & " inner join art4 a with (nolock) on a.cod=lp.art_int and a.generico=0"
                sConsulta = sConsulta & " WHERE LP.ART_INT = '" & DblQuote(fldCod.Value) & "' and lp.descr_libre is not null"
                rs.Open sConsulta, g_adoconnCon, adOpenForwardOnly, adLockReadOnly
                
                If rs(0).Value > 0 Then
                    sConsulta = " UPDATE LINEAS_PEDIDO SET LINEAS_PEDIDO.DESCR_LIBRE = " & StrToSQLNULL(fldDen.Value)
                    sConsulta = sConsulta & " FROM LINEAS_PEDIDO LP WITH (NOLOCK)"
                    sConsulta = sConsulta & " INNER JOIN ORDEN_ENTREGA OE WITH (NOLOCK) ON LP.ORDEN=OE.ID "
                    sConsulta = sConsulta & " inner join art4 a with (nolock) on a.cod=lp.art_int and a.generico=0"
                    sConsulta = sConsulta & " WHERE LP.ART_INT = '" & DblQuote(fldCod.Value) & "' and lp.descr_libre is not null"
                    
                    g_adoconnCon.Execute sConsulta
                    If g_adoconnCon.Errors.Count > 0 Then 'Si error en UPDATE DEN
                        Set objErrores = g_adoconnCon.Errors
                        GrabarError objErrores, fldId
                        GoTo ERROR_SIGUE
                    End If
                End If
                rs.Close
                'Actualiza la denominaci�n de la tabla FAVORITOS_LINEAS_PEDIDO, no bloqueamos tabla, no desabilitamos triggers
                sConsulta = "SELECT COUNT(*) FROM FAVORITOS_LINEAS_PEDIDO LP WITH (NOLOCK)"
                sConsulta = sConsulta & " WHERE LP.ART_INT = '" & DblQuote(fldCod.Value) & "'"
                rs.Open sConsulta, g_adoconnCon, adOpenForwardOnly, adLockReadOnly
                If rs(0).Value > 0 Then
                    'Actualiza la denominaci�n de la tabla FAVORITOS_LINEAS_PEDIDO
                    sConsulta = " UPDATE FAVORITOS_LINEAS_PEDIDO SET DESCR_LIBRE = " & StrToSQLNULL(fldDen.Value)
                    sConsulta = sConsulta & " FROM FAVORITOS_LINEAS_PEDIDO LP WITH (NOLOCK)"
                    sConsulta = sConsulta & " WHERE LP.ART_INT = '" & DblQuote(fldCod.Value) & "'"
                    
                    g_adoconnCon.Execute sConsulta
                    If g_adoconnCon.Errors.Count > 0 Then 'Si error en UPDATE DEN
                        Set objErrores = g_adoconnCon.Errors
                        GrabarError objErrores, fldId
                        GoTo ERROR_SIGUE
                    End If
                End If
                rs.Close
                'Actualiza la denominaci�n de la tabla CATALOG_LIN, no bloqueamos tabla, no desabilitamos triggers
                sConsulta = "SELECT COUNT(*) FROM CATALOG_LIN CL WITH (NOLOCK)"
                sConsulta = sConsulta & " WHERE CL.ART_INT = '" & DblQuote(fldCod.Value) & "'"
                rs.Open sConsulta, g_adoconnCon, adOpenForwardOnly, adLockReadOnly
                If rs(0).Value > 0 Then
                    'Actualiza la denominaci�n de la tabla CATALOG_LIN
                    sConsulta = " UPDATE CATALOG_LIN SET DEN = " & StrToSQLNULL(fldDen.Value)
                    sConsulta = sConsulta & " FROM CATALOG_LIN CL WITH (NOLOCK)"
                    sConsulta = sConsulta & " WHERE CL.ART_INT = '" & DblQuote(fldCod.Value) & "'"
                    
                    g_adoconnCon.Execute sConsulta
                    If g_adoconnCon.Errors.Count > 0 Then 'Si error en UPDATE DEN
                        Set objErrores = g_adoconnCon.Errors
                        GrabarError objErrores, fldId
                        GoTo ERROR_SIGUE
                    End If
                End If
                rs.Close
                
                'Actualiza la denominaci�n de la tabla FORM_CAMPO
                sConsulta = "SELECT COUNT(*) FROM FORM_CAMPO F2 WITH (NOLOCK) WHERE F2.TIPO_CAMPO_GS=119" _
                         & " AND F2.VALOR_TEXT ='" & DblQuote(fldCod.Value) & "' AND F2.ES_SUBCAMPO=0"
                rs.Open sConsulta, g_adoconnCon, adOpenForwardOnly, adLockReadOnly
                If rs(0).Value > 0 Then
                    
                    sConsulta = " UPDATE FORM_CAMPO SET VALOR_TEXT = " & StrToSQLNULL(fldDen.Value)
                    sConsulta = sConsulta & " FROM FORM_CAMPO F WITH (NOLOCK)"
                    sConsulta = sConsulta & " WHERE F.TIPO_CAMPO_GS=118 AND F.ES_SUBCAMPO = 0"
                    sConsulta = sConsulta & "  AND EXISTS (SELECT * FROM FORM_CAMPO F2 WITH (NOLOCK) WHERE F2.TIPO_CAMPO_GS=119 AND F2.VALOR_TEXT ='" & DblQuote(fldCod.Value) & "' AND F2.ORDEN=F.ORDEN-1 AND F2.ES_SUBCAMPO=0 AND F2.GRUPO=F.GRUPO)"
                    g_adoconnCon.Execute sConsulta
                    If g_adoconnCon.Errors.Count > 0 Then 'Si error en UPDATE DEN
                        Set objErrores = g_adoconnCon.Errors
                        GrabarError objErrores, fldId
                        GoTo ERROR_SIGUE
                    End If
                End If
                rs.Close
                'Actualiza la denominaci�n de la tabla COPIA_CAMPO
                sConsulta = "SELECT COUNT(*) FROM COPIA_CAMPO C WITH (NOLOCK) INNER JOIN FORM_CAMPO FC WITH (NOLOCK) ON C.COPIA_CAMPO_DEF=FC.ID " _
                    & " INNER JOIN INSTANCIA I WITH (NOLOCK) ON I.ID=C.INSTANCIA AND I.ULT_VERSION=C.NUM_VERSION AND I.ESTADO<100" _
                    & " WHERE FC.TIPO_CAMPO_GS=119 AND C.VALOR_TEXT ='" & DblQuote(fldCod.Value) & "' AND FC.ES_SUBCAMPO=0"
                rs.Open sConsulta, g_adoconnCon, adOpenForwardOnly, adLockReadOnly
                If rs(0).Value > 0 Then
                    
                    sConsulta = " UPDATE COPIA_CAMPO SET VALOR_TEXT = " & StrToSQLNULL(fldDen.Value)
                    sConsulta = sConsulta & " FROM COPIA_CAMPO F WITH (NOLOCK)"
                    sConsulta = sConsulta & " INNER JOIN FORM_CAMPO FC WITH (NOLOCK) ON F.COPIA_CAMPO_DEF=FC.ID"
                    sConsulta = sConsulta & " INNER JOIN INSTANCIA I WITH (NOLOCK) ON I.ID=F.INSTANCIA AND I.ULT_VERSION=F.NUM_VERSION AND I.ESTADO<100"
                    sConsulta = sConsulta & " WHERE FC.TIPO_CAMPO_GS=118 AND FC.ES_SUBCAMPO = 0 "
                    sConsulta = sConsulta & " AND EXISTS (SELECT * FROM COPIA_CAMPO F3 WITH (NOLOCK)"
                    sConsulta = sConsulta & " INNER JOIN FORM_CAMPO FC3 WITH (NOLOCK) ON F3.COPIA_CAMPO_DEF=FC3.ID WHERE"
                    sConsulta = sConsulta & " F3.INSTANCIA=F.INSTANCIA AND F3.NUM_VERSION=F.NUM_VERSION AND FC3.GRUPO=FC.GRUPO"
                    sConsulta = sConsulta & " AND FC3.TIPO_CAMPO_GS=119 AND F3.VALOR_TEXT ='" & DblQuote(fldCod.Value) & "' AND FC3.ORDEN=FC.ORDEN-1 AND FC3.ES_SUBCAMPO=0)"
                    g_adoconnCon.Execute sConsulta
                    If g_adoconnCon.Errors.Count > 0 Then 'Si error en UPDATE DEN
                        Set objErrores = g_adoconnCon.Errors
                        GrabarError objErrores, fldId
                        GoTo ERROR_SIGUE
                    End If
                End If
                rs.Close
                
                'Actualiza la denominaci�n de la tabla LINEA_DESGLOSE
                sConsulta = "SELECT COUNT(*) FROM LINEA_DESGLOSE L WITH (NOLOCK) INNER JOIN FORM_CAMPO C WITH (NOLOCK) ON L.CAMPO_HIJO=C.ID " _
                    & " WHERE C.TIPO_CAMPO_GS=119 AND L.VALOR_TEXT ='" & DblQuote(fldCod.Value) & "' AND C.ES_SUBCAMPO=1"
                rs.Open sConsulta, g_adoconnCon, adOpenForwardOnly, adLockReadOnly
                If rs(0).Value > 0 Then
                    
                    sConsulta = " UPDATE LINEA_DESGLOSE SET VALOR_TEXT = " & StrToSQLNULL(fldDen.Value)
                    sConsulta = sConsulta & " FROM LINEA_DESGLOSE F WITH (NOLOCK)"
                    sConsulta = sConsulta & " INNER JOIN FORM_CAMPO C WITH (NOLOCK) ON F.CAMPO_HIJO=C.ID"
                    sConsulta = sConsulta & " WHERE C.TIPO_CAMPO_GS=118 AND C.ES_SUBCAMPO = 1 "
                    sConsulta = sConsulta & " AND EXISTS (SELECT * FROM LINEA_DESGLOSE F2 WITH (NOLOCK)"
                    sConsulta = sConsulta & " INNER JOIN FORM_CAMPO C2 WITH (NOLOCK) ON F2.CAMPO_HIJO=C2.ID WHERE C2.TIPO_CAMPO_GS=119 AND F2.VALOR_TEXT ='" & DblQuote(fldCod.Value) & "' AND C2.ORDEN=C.ORDEN-1 AND F2.LINEA=F.LINEA AND F2.CAMPO_PADRE=F.CAMPO_PADRE AND C2.ES_SUBCAMPO=1 AND C2.GRUPO=C.GRUPO)"
                    g_adoconnCon.Execute sConsulta
                    If g_adoconnCon.Errors.Count > 0 Then 'Si error en UPDATE DEN
                        Set objErrores = g_adoconnCon.Errors
                        GrabarError objErrores, fldId
                        GoTo ERROR_SIGUE
                    End If
                End If
                rs.Close
                
                'Actualiza la denominaci�n de la tabla COPIA_LINEA_DESGLOSE
                sConsulta = "SELECT COUNT(*) FROM COPIA_LINEA_DESGLOSE L WITH (NOLOCK) INNER JOIN COPIA_CAMPO C WITH (NOLOCK) ON L.CAMPO_HIJO=C.ID " _
                    & " INNER JOIN FORM_CAMPO CD WITH (NOLOCK) ON C.COPIA_CAMPO_DEF=CD.ID " _
                    & " INNER JOIN INSTANCIA I WITH (NOLOCK) ON I.ID=C.INSTANCIA AND I.ULT_VERSION=C.NUM_VERSION AND I.ESTADO<100" _
                    & " WHERE CD.TIPO_CAMPO_GS=119 AND L.VALOR_TEXT ='" & DblQuote(fldCod.Value) & "' AND CD.ES_SUBCAMPO=1"
                rs.Open sConsulta, g_adoconnCon, adOpenForwardOnly, adLockReadOnly
                If rs(0).Value > 0 Then
                    sConsulta = " UPDATE COPIA_LINEA_DESGLOSE SET VALOR_TEXT = " & StrToSQLNULL(fldDen.Value)
                    sConsulta = sConsulta & " FROM COPIA_LINEA_DESGLOSE F WITH (NOLOCK)"
                    sConsulta = sConsulta & " INNER JOIN COPIA_CAMPO CC WITH (NOLOCK) ON CC.ID=F.CAMPO_HIJO"
                    sConsulta = sConsulta & " INNER JOIN FORM_CAMPO FC WITH (NOLOCK) ON CC.COPIA_CAMPO_DEF=FC.ID"
                    sConsulta = sConsulta & " INNER JOIN INSTANCIA I WITH (NOLOCK) ON I.ID=CC.INSTANCIA AND I.ULT_VERSION=CC.NUM_VERSION AND I.ESTADO<100"
                    sConsulta = sConsulta & " WHERE FC.TIPO_CAMPO_GS=118 AND FC.ES_SUBCAMPO = 1 "
                    sConsulta = sConsulta & " AND EXISTS (SELECT * FROM COPIA_LINEA_DESGLOSE F3 WITH (NOLOCK)"
                    sConsulta = sConsulta & " INNER JOIN COPIA_CAMPO CC3 WITH (NOLOCK) ON CC3.ID=F3.CAMPO_HIJO"
                    sConsulta = sConsulta & " INNER JOIN FORM_CAMPO FC3 WITH (NOLOCK) ON CC3.COPIA_CAMPO_DEF=FC3.ID WHERE "
                    sConsulta = sConsulta & " CC3.INSTANCIA=CC.INSTANCIA AND CC3.NUM_VERSION=CC.NUM_VERSION AND FC3.GRUPO=FC.GRUPO"
                    sConsulta = sConsulta & " AND FC3.TIPO_CAMPO_GS=119 AND F3.VALOR_TEXT ='" & DblQuote(fldCod.Value) & "' AND FC3.ORDEN=FC.ORDEN-1 AND F3.LINEA=F.LINEA AND F3.CAMPO_PADRE=F.CAMPO_PADRE AND FC3.ES_SUBCAMPO=1)"
                    g_adoconnCon.Execute sConsulta
                    If g_adoconnCon.Errors.Count > 0 Then 'Si error en UPDATE DEN
                        Set objErrores = g_adoconnCon.Errors
                        GrabarError objErrores, fldId
                        GoTo ERROR_SIGUE
                    End If
                End If
                rs.Close
                'Si ha llegado hasta aqu�, la actualizaci�n ha tenido exito y
                'borramos la fila de la tabla UPD_ART4
                sConsulta = " UPDATE UPD_ART4 SET OK=1,FECLASTTRY=GETDATE() WHERE ID = " & fldId
                g_adoconnCon.Execute sConsulta
                If g_adoconnCon.Errors.Count > 0 Then
                    GoTo ERROR_SIGUE
                End If
            
            End If
            
            If fldACCION.Value = "R" Then
                'Reubicar el Articulo
                
               g_adoconnCon.Execute "SET XACT_ABORT OFF"
               g_adoconnCon.Execute "BEGIN TRANSACTION", , adExecuteNoRecords
               
               sMat = fldGmn1.Value & Mid$("                         ", 1, gLongitudesDeCodigos.giLongCodGMN1 - Len(fldGmn1.Value))
               sMat = sMat & fldGmn2.Value & Mid$("                         ", 1, gLongitudesDeCodigos.giLongCodGMN2 - Len(fldGmn2.Value))
               sMat = sMat & fldGmn3.Value & Mid$("                         ", 1, gLongitudesDeCodigos.giLongCodGMN3 - Len(fldGmn3.Value))
               sMat = sMat & fldGMN4.Value & Mid$("                         ", 1, gLongitudesDeCodigos.giLongCodGMN4 - Len(fldGMN4.Value))
                
               Dim oDenominaciones As DenominacionesArticulo
               oDenominaciones = DevolverDenominacionesArticulo(fldGmn1.Value, fldGmn2.Value, fldGmn3.Value, fldGMN4.Value)
               
               'Se updatea COPIA_CAMPO
               sConsulta = " UPDATE cc_m SET VALOR_TEXT = '" & DblQuote(sMat) & "', VALOR_TEXT_DEN='" & DblQuote(oDenominaciones.gsDenominacion) & "'"
               sConsulta = sConsulta & " FROM COPIA_CAMPO cc WITH(NOLOCK)"
               sConsulta = sConsulta & " INNER JOIN FORM_CAMPO FC  WITH (NOLOCK) ON"
               sConsulta = sConsulta & " (FC.TIPO_CAMPO_GS=119 or FC.TIPO_CAMPO_GS=104) and FC.ID = cc.COPIA_CAMPO_DEF "
               sConsulta = sConsulta & " INNER JOIN FORM_CAMPO FC2 WITH (NOLOCK) ON  FC.GRUPO=FC2.GRUPO  and FC.ORDEN = FC2.ORDEN+1 and FC2.ES_SUBCAMPO=FC.ES_SUBCAMPO "
               sConsulta = sConsulta & " INNER JOIN COPIA_CAMPO cc_m WITH (NOLOCK) ON cc_m.COPIA_CAMPO_DEF=FC2.ID AND cc_m.INSTANCIA=cc.instancia and cc_m.NUM_VERSION=cc.NUM_VERSION "
               sConsulta = sConsulta & " where cc.VALOR_TEXT='" & DblQuote(fldCod.Value) & "'  and FC2.TIPO_CAMPO_GS=103 "
                
               g_adoconnCon.Execute sConsulta
               If g_adoconnCon.Errors.Count > 0 Then 'Si error
                    Set objErrores = g_adoconnCon.Errors
                    g_adoconnCon.Execute "ROLLBACK TRANSACTION"
                    GrabarError objErrores, fldId
                    GoTo ERROR_SIGUE
               End If
               
               'Se updatea COPIA_LINEA_DESGLOSE
               sConsulta = " UPDATE LMAT SET VALOR_TEXT = '" & DblQuote(sMat) & "', VALOR_TEXT_DEN='" & DblQuote(oDenominaciones.gsDenominacion) & "'"
               sConsulta = sConsulta & " FROM COPIA_LINEA_DESGLOSE LMAT WITH(NOLOCK) "
               sConsulta = sConsulta & " INNER JOIN COPIA_CAMPO CC WITH (NOLOCK) ON CC.ID = LMAT.CAMPO_HIJO"
               sConsulta = sConsulta & " INNER JOIN FORM_CAMPO FC WITH (NOLOCK) ON CC.COPIA_CAMPO_DEF = FC.ID AND FC.ES_SUBCAMPO=1 AND FC.TIPO_CAMPO_GS=103"
               sConsulta = sConsulta & " INNER JOIN FORM_CAMPO FCART WITH (NOLOCK) "
               sConsulta = sConsulta & " ON FCART.ORDEN = (FC.ORDEN+1) AND (FCART.TIPO_CAMPO_GS=119 OR FCART.TIPO_CAMPO_GS=104) AND FCART.ES_SUBCAMPO=1"
               sConsulta = sConsulta & " AND FC.GRUPO=FCART.GRUPO AND FCART.ES_SUBCAMPO=1"
               sConsulta = sConsulta & " INNER JOIN COPIA_CAMPO CCART WITH (NOLOCK) ON CCART.COPIA_CAMPO_DEF = FCART.ID and CCART.instancia=cc.instancia and CCART.num_version=cc.num_version"
               sConsulta = sConsulta & " INNER JOIN COPIA_LINEA_DESGLOSE LART WITH (NOLOCK) ON LART.LINEA=LMAT.LINEA AND LART.CAMPO_HIJO=CCART.ID"
               sConsulta = sConsulta & " WHERE LART.VALOR_TEXT = '" & DblQuote(fldCod.Value) & "' AND LMAT.VALOR_TEXT IS NOT NULL "
               g_adoconnCon.Execute sConsulta
               
               If g_adoconnCon.Errors.Count > 0 Then 'Si hay error
                    Set objErrores = g_adoconnCon.Errors
                    g_adoconnCon.Execute "ROLLBACK TRANSACTION"
                    GrabarError objErrores, fldId
                    GoTo ERROR_SIGUE
                End If
               
               
               'Si ha llegado hasta aqu�, la actualizaci�n ha tenido exito
                
                sConsulta = " UPDATE UPD_ART4 SET OK=1,FECLASTTRY=GETDATE() WHERE ID = " & fldId
                g_adoconnCon.Execute sConsulta
                If g_adoconnCon.Errors.Count > 0 Then
                    g_adoconnCon.Execute "ROLLBACK TRANSACTION"
                    GoTo ERROR_SIGUE
                End If
               
               g_adoconnCon.Execute "COMMIT TRANSACTION", , adExecuteNoRecords
               
            End If
            
            If fldACCION.Value = "C" Then
                
                'Actualizar el c�digo, LA TRANSACCION ESTA EN EL sTORED
                
                If ActualizarCodigoART4EsOK(fldCod, fldCOD_NEW, fldGmn1) Then
                    If GrabarEnLogEntidad(EntidadIntegracion.art4) Then
                        If Not ActualizarLOGART4EsOK(fldCod, fldCOD_NEW, fldGmn1, fldGmn2, fldGmn3, fldGMN4, fldDen, fldUSU, m_Origen) Then
                            Set objErrores = g_adoconnCon.Errors
                            GrabarError objErrores, fldId
                            GoTo ERROR_SIGUE
                        End If
                    End If
                    sConsulta = " UPDATE UPD_ART4 SET OK=1,FECLASTTRY=GETDATE() WHERE ID = " & fldId
                    g_adoconnCon.Execute sConsulta
                    If g_adoconnCon.Errors.Count > 0 Then
                        GoTo ERROR_SALIR
                    End If
                Else
                    Set objErrores = g_adoconnCon.Errors
                    GrabarError objErrores, fldId
                    GoTo ERROR_SIGUE
                End If
            End If
        
ERROR_SIGUE:

            adoRes.MoveNext

        Wend
        
        
        Set fldId = Nothing
        Set fldCod = Nothing
        Set fldDen = Nothing
        Set fldGmn1 = Nothing
        Set fldGmn2 = Nothing
        Set fldGmn3 = Nothing
        Set fldGMN4 = Nothing
        Set fldACCION = Nothing
    
        Set objErrores = Nothing
    
    End If
    
    'FIN
    adoRes.Close
    Set adoRes = Nothing
    
    Exit Function


ERROR_SALIR:
    If sAccion = "D" Then
        g_adoconnCon.Execute "IF @@TRANCOUNT>0 ROLLBACK TRANSACTION", , adExecuteNoRecords
    End If
    If Not adoRes Is Nothing Then
        adoRes.Close
        Set adoRes = Nothing
    End If
    
    Set fldId = Nothing
    Set fldCod = Nothing
    Set fldDen = Nothing
    Set fldGmn1 = Nothing
    Set fldGmn2 = Nothing
    Set fldGmn3 = Nothing
    Set fldGMN4 = Nothing
    Set fldACCION = Nothing
    
    Set objErrores = Nothing
    
    ActualizarArticulo = 0
    

End Function

Private Sub GrabarError(ByVal cErrores As ADODB.Errors, ByVal iID As Long)
Dim oError As ADODB.Error
Dim sErrorStr As String
Dim sConsulta As String

    
    'Si ha llegado hasta aqu�, en la actualizaci�n ha habido un error y
    'guardamos en la tabla UPD_ART4
    'la hora y fecha en FECLASTTRY y
    'el n�mero de error y la descripci�n del error en ERROR
    
    sErrorStr = ""
    
    'Para cada error de la colecci�n cErrores
    'escribimos el n�mero de error y su descripci�n
    For Each oError In cErrores
        sErrorStr = sErrorStr & "N� Error: " & oError.Number & "--"
        sErrorStr = sErrorStr & "Descripci�n: " & oError.Description & "."
    Next
    If Len(sErrorStr) > 200 Then
        sErrorStr = Left(sErrorStr, 200)
    End If
    
    sConsulta = " UPDATE UPD_ART4 SET"
    sConsulta = sConsulta & " FECLASTTRY = getdate(),ERROR = " & StrToSQLNULL(sErrorStr)
    sConsulta = sConsulta & " WHERE ID = " & iID
    g_adoconnCon.Execute sConsulta
    
    
End Sub

''' <summary>Esta funci�n graba un error de BD producido en un cambio de c�digo</summary>
''' <param name="cErrores">Colecci�n de errores de la conexi�n</param>
''' <param name="lID">ID del registro de cambio de c�digo de la tabla UPD_COD</param>
''' <remarks>Llamada desde: las funciones de ActualizarCodigo</remarks>

Private Sub GrabarErrorBDCodigo(ByVal cErrores As ADODB.Errors, ByVal lID As Long)
    Dim oError As ADODB.Error
    Dim sErrorStr As String
        
    sErrorStr = ""
    
    'Para cada error de la colecci�n cErrores escribimos el n�mero de error y su descripci�n
    For Each oError In cErrores
        sErrorStr = sErrorStr & "N� Error: " & oError.Number & "--"
        sErrorStr = sErrorStr & "Descripci�n: " & oError.Description & "."
    Next
    If Len(sErrorStr) > 200 Then
        sErrorStr = Left(sErrorStr, 200)
    End If
    
    GrabarErrorCambioCodigo sErrorStr, lID
End Sub

''' <summary>Esta funci�n graba un error no de BD producido en un cambio de c�digo</summary>
''' <param name="TESError">Datos del error</param>
''' <param name="lID">ID del registro de cambio de c�digo de la tabla UPD_COD</param>
''' <remarks>Llamada desde: las funciones de ActualizarCodigoUsuario</remarks>

Private Sub GrabarErrorCodigo(TESError As TipoErrorSummit, ByVal lID As Long)
    Dim sErrorStr As String
    
    sErrorStr = ""
    sErrorStr = sErrorStr & "N� Error: " & TESError.NumError & "--"
    sErrorStr = sErrorStr & "Descripci�n: " & TESError.Arg2 & "."

    If Len(sErrorStr) > 200 Then
        sErrorStr = Left(sErrorStr, 200)
    End If
    
    GrabarErrorCambioCodigo sErrorStr, lID
End Sub

''' <summary>Esta funci�n actualiza un registro de la tabla UPD_COD con el error pasado</summary>
''' <param name="sErrorStr">Descripci�n del error</param>
''' <param name="lID">ID del registro de cambio de c�digo de la tabla UPD_COD</param>
''' <remarks>Llamada desde: GrabarErrorBDCodigo y GrabarErrorCodigo</remarks>

Private Sub GrabarErrorCambioCodigo(ByVal sErrorStr As String, ByVal lID As Long)
    Dim adoComm As ADODB.Command
    
    Set adoComm = New ADODB.Command
    adoComm.ActiveConnection = g_adoconnCon
    adoComm.CommandType = adCmdText
    adoComm.CommandText = "UPDATE UPD_COD SET FECLASTTRY = ?, ERROR = ? WHERE ID = ?"
    adoComm.Parameters.Append adoComm.CreateParameter("FECLASTTRY", adDate, adParamInput, Value:=Now)
    adoComm.Parameters.Append adoComm.CreateParameter("ERROR", adVarChar, adParamInput, 200, sErrorStr)
    adoComm.Parameters.Append adoComm.CreateParameter("ID", adInteger, adParamInput, Value:=lID)
    adoComm.Execute
End Sub


''' <summary>Actualizaz el registro en la tabla UPD_COD con OK=1</summary>
''' <param name="iID">ID del registro en la tabla UPD_COD</param>

Private Sub MarcarCambioOK(ByVal iID As Long)
    Dim oError As ADODB.Error
    Dim sErrorStr As String
    Dim adoComm As ADODB.Command
    
    Set adoComm = New ADODB.Command
    With adoComm
        .ActiveConnection = g_adoconnCon
        .CommandType = adCmdText
        .CommandText = "UPDATE UPD_COD SET OK=1,FECLASTTRY=GETDATE() WHERE ID=?"
        .Parameters.Append .CreateParameter("ID", adInteger, adParamInput, Value:=iID)
        
        .Execute
    End With
End Sub

Private Function GrabarEnLogEntidad(ByVal EntidadIntegracion As Integer) As Boolean
'***********************************************************************************
'*** Descripci�n: Nos indica si hay que grabar o no el cambio en el log de       ***
'***              la entidad de integraci�n y carga en la variable               ***
'***              del m�dulo m_Origen el origen del movimiento                   ***
'*** Parametros:  Entidad de Integraci�n                                         ***
'*** Devuelve:    True (hay que grabar) False (No hay que grabar)                ***
'***********************************************************************************

    Dim sConsulta As String
    Dim adoComm As ADODB.Command
    Dim adoPar As ADODB.Parameter
    Dim adoRes As ADODB.Recordset
    Dim bActivLog As Boolean
    Dim bExportar As Boolean
    Set adoComm = New ADODB.Command
    adoComm.ActiveConnection = g_adoconnCon
    adoComm.CommandType = adCmdText
    
    sConsulta = "SELECT ACTIVLOG FROM PARGEN_GEST WITH (NOLOCK) WHERE ID=1"
    Set adoRes = New ADODB.Recordset
    adoRes.Open sConsulta, g_adoconnCon, adOpenForwardOnly, adLockReadOnly

    If adoRes.eof Then
        adoRes.Close
        GrabarEnLogEntidad = False
        Set adoRes = Nothing
        Set adoComm = Nothing
        Exit Function
    Else
        bActivLog = (adoRes.Fields("ACTIVLOG").Value = 1)
        adoRes.Close
    End If
    
    'Exporta si para alg�n ERP hay sentido Salida o EntradaSalida
    adoComm.CommandText = "SELECT DISTINCT ISNULL(TI.SENTIDO,0) AS EXPORTAR FROM TABLAS_INTEGRACION_ERP TI " & _
        " WHERE TI.TABLA = ? "
    
    Set adoPar = adoComm.CreateParameter("TABLA", adSmallInt, adParamInput, Value:=EntidadIntegracion)
    adoComm.Parameters.Append adoPar
    
    Set adoRes = adoComm.Execute
    If adoRes.eof Then
        GrabarEnLogEntidad = False
        Set adoRes = Nothing
        Set adoComm = Nothing
        Exit Function
    End If
    bExportar = False
    While Not adoRes.eof And bExportar = False
        If adoRes.Fields("EXPORTAR").Value = SentidoIntegracion.Salida Or adoRes.Fields("EXPORTAR").Value = SentidoIntegracion.EntradaSalida Then
            bExportar = True
        End If
        adoRes.MoveNext
    Wend
    adoRes.Close
    Set adoRes = Nothing
    If Not (bActivLog Or bExportar) Then
        GrabarEnLogEntidad = False
        Set adoComm = Nothing
        Exit Function
    End If
    
    If bActivLog And bExportar Then
        m_Origen = FSGSIntReg
    Else
        If bActivLog Then m_Origen = FSGSReg Else m_Origen = FSGSInt
    End If
    
    GrabarEnLogEntidad = True
End Function

''' <summary>Esta funci�n cambia de codigo la v�a de pago</summary>
''' <param name="lID">ID del registro</param>
''' <param name="CodViejo">Codigo viejo</param>
''' <param name="CodNuevo">Codigo nuevo</param>
''' <param name="Usuario">Usuario que realiza el cambio</param>
''' <returns>True si no hay errores, False si hay errores</returns>
''' <remarks>Llamada desde: ActualizarCodigos()
''' Tiempo m�ximo: 1 sec </remarks>
Private Function ActualizarCodigoViaPago(ByVal lID As Long, ByVal CodViejo As String, ByVal CodNuevo As String, ByVal Usuario As String) As Boolean
    
    Dim adocom As ADODB.Command
    Dim oParam As ADODB.Parameter
    
On Error GoTo Error:
    
    Set adocom = New ADODB.Command
    Set adocom.ActiveConnection = g_adoconnCon
    adocom.CommandType = adCmdStoredProc
    
    Set oParam = adocom.CreateParameter("OLD", adVarWChar, adParamInput, 50, CodViejo)
    adocom.Parameters.Append oParam
    Set oParam = adocom.CreateParameter("NEW", adVarWChar, adParamInput, 50, CodNuevo)
    adocom.Parameters.Append oParam
    
    adocom.CommandText = "VIA_PAG_COD"
    adocom.CommandTimeout = 1200   '20 min
    adocom.Execute
    If g_adoconnCon.Errors.Count > 0 Then GoTo Error
    
    If GrabarEnLogEntidad(EntidadIntegracion.ViaPag) Then
        If Not ActualizarLOGViaPago(CodViejo, CodNuevo, Usuario, m_Origen) Then GoTo Error
    End If
    
    ActualizarCodigoViaPago = True
    Exit Function
Error:
    GrabarErrorBDCodigo g_adoconnCon.Errors, lID
    If Not adocom Is Nothing Then Set adocom = Nothing
    ActualizarCodigoViaPago = False
End Function


''' <summary>Esta funci�n cambia de codigo la v�a de pago</summary>
''' <param name="lID">ID del registro</param>
''' <param name="CodViejo">Codigo viejo</param>
''' <param name="CodNuevo">Codigo nuevo</param>
''' <param name="Usuario">Usuario que realiza el cambio</param>
''' <returns>True si no hay errores, False si hay errores</returns>
''' <remarks>Llamada desde: ActualizarCodigos()
''' Tiempo m�ximo: 1 sec </remarks>
Private Function ActualizarCodigoFormaPago(ByVal lID As Long, ByVal CodViejo As String, ByVal CodNuevo As String, ByVal Usuario As String) As Boolean
    
    Dim adocom As ADODB.Command
    Dim oParam As ADODB.Parameter
    Dim ContRegActualizados As Long
    Dim sConsulta As String
    Dim lIdLogPAG As Long
    Dim adoParam As ADODB.Parameter
    Dim bLog As Boolean
        
On Error GoTo Error:
    
    Set adocom = New ADODB.Command
    Set adocom.ActiveConnection = g_adoconnCon
    adocom.CommandType = adCmdStoredProc
    
    Set oParam = adocom.CreateParameter("OLD", adVarWChar, adParamInput, 50, CodViejo)
    adocom.Parameters.Append oParam
    Set oParam = adocom.CreateParameter("NEW", adVarWChar, adParamInput, 50, CodNuevo)
    adocom.Parameters.Append oParam
    adocom.CommandTimeout = 1200   '20 min
    adocom.CommandText = "PAG_COD"
    
    adocom.Execute ContRegActualizados
    If g_adoconnCon.Errors.Count > 0 Then GoTo Error
    
    bLog = True
    ''' Tratamiento para el LOG de cambios y la Integraci�n
    If GrabarEnLogEntidad(EntidadIntegracion.Pag) And ContRegActualizados <> -1 Then

        ''10/09/2007(ngo):MultiERP
        Dim adoRes As ADODB.Recordset
        Set adoRes = New ADODB.Recordset
        sConsulta = "SELECT COD FROM ERP"
        adoRes.Open sConsulta, g_adoconnCon, adOpenStatic, adLockReadOnly
        While Not adoRes.eof
            sConsulta = "INSERT INTO LOG_PAG (ACCION, COD, COD_NEW, ORIGEN, USU, ERP) VALUES ('C',N'" & DblQuote(CodViejo) & "'"
            sConsulta = sConsulta & ",N'" & DblQuote(CodNuevo) & "'," & m_Origen & ",'" & DblQuote(Usuario) & "','" & DblQuote(adoRes.Fields("COD").Value) & "')"
            
            Set adocom = New ADODB.Command
            Set adocom.ActiveConnection = g_adoconnCon
            adocom.CommandType = adCmdText
            adocom.CommandText = sConsulta
            adocom.Execute
            If g_adoconnCon.Errors.Count > 0 Then
                GoTo Error
            End If
            

            Dim ador As ADODB.Recordset
            Set ador = New ADODB.Recordset
            ador.Open "SELECT MAX(ID) FROM LOG_PAG ", g_adoconnCon, adOpenForwardOnly, adLockReadOnly
            If Not ador.eof Then
                lIdLogPAG = ador.Collect(0)
            End If
            ador.Close
            Set ador = Nothing

                    
            sConsulta = "INSERT INTO LOG_MON_DEN (ID_LOG_PAG,DEN,IDI) " & _
            " SELECT " & lIdLogPAG & " AS ID, V.DEN AS DEN, V.IDIOMA AS IDI" & _
            " FROM PAG_DEN P WITH(NOLOCK)" & _
            " WHERE P.PAG=?"
            Set adocom = New ADODB.Command
            adocom.ActiveConnection = g_adoconnCon
            Set adoParam = adocom.CreateParameter("COD", adVarWChar, adParamInput, 50, Value:=CodNuevo)
            adocom.Parameters.Append adoParam
            
            adocom.CommandType = adCmdText
            adocom.CommandText = sConsulta
            adocom.Execute
                    

            If g_adoconnCon.Errors.Count > 0 Then
                GoTo Error
            End If
            
            Set adocom = Nothing
            
            
            adoRes.MoveNext
        Wend
        adoRes.Close
        Set adoRes = Nothing
    End If
    
    ActualizarCodigoFormaPago = True
    Exit Function
Error:
    GrabarErrorBDCodigo g_adoconnCon.Errors, lID
    
    If Not adocom Is Nothing Then Set adocom = Nothing
    ActualizarCodigoFormaPago = False

    If bLog Then 'Si el error se ha producido al grabar en la tabla de LOG, deshacemos los cambios a mano
        Set adocom = Nothing
        Set adocom = New ADODB.Command
        Set adocom.ActiveConnection = g_adoconnCon
        Set oParam = adocom.CreateParameter("PAR1", adVarChar, adParamInput, 50, CodNuevo)
        adocom.Parameters.Append oParam
        Set oParam = adocom.CreateParameter("PAR2", adVarChar, adParamInput, 50, CodViejo)
        adocom.Parameters.Append oParam
        adocom.CommandType = adCmdStoredProc
        adocom.CommandText = "PAG_COD"
        adocom.CommandTimeout = 1200   '20 min
        adocom.Execute
    End If

End Function



''' <summary>Esta funci�n cambia de codigo de moneda</summary>
''' <param name="lID">ID del registro</param>
''' <param name="CodViejo">Codigo viejo</param>
''' <param name="CodNuevo">Codigo nuevo</param>
''' <param name="Usuario">Usuario que realiza el cambio</param>
''' <returns>True si no hay errores, False si hay errores</returns>
''' <remarks>Llamada desde: ActualizarCodigos()
''' Tiempo m�ximo: 1 sec </remarks>
Public Function ActualizarCodigoMoneda(ByVal lID As Long, ByVal CodViejo As String, ByVal CodNuevo As String, ByVal Usuario As String) As Boolean
    
    Dim adocom As ADODB.Command
    Dim adoParam As ADODB.Parameter
    Dim oParam As ADODB.Parameter
    Dim ContRegActualizados As Long
    Dim sFSP As String
    Dim par As ADODB.Parameter
    Dim sConsulta As String
    Dim lIdLogMON As Long
    Dim adoRes As ADODB.Recordset
    
On Error GoTo Error:
    
    g_adoconnCon.Execute "BEGIN DISTRIBUTED TRAN", , adExecuteNoRecords
    
    g_adoconnCon.Execute "SET XACT_ABORT ON"
    
    ''' Preparar la SP y sus parametros
    
    Set adocom = New ADODB.Command
    Set adocom.ActiveConnection = g_adoconnCon
    adocom.CommandType = adCmdStoredProc
    
    Set par = adocom.CreateParameter("PAR1", adVarWChar, adParamInput, 50, CodViejo)
    adocom.Parameters.Append par
    Set par = adocom.CreateParameter("PAR2", adVarWChar, adParamInput, 50, CodNuevo)
    adocom.Parameters.Append par
    
    adocom.CommandText = "MON_COD"
    adocom.CommandTimeout = 1200   '20 min
    ''' Ejecutar la SP
    adocom.Execute ContRegActualizados
    If g_adoconnCon.Errors.Count > 0 Then
        GoTo Error
    End If
    Set adocom = Nothing
    
    If gParametrosGenerales.giINSTWEB = 2 Then 'ConPortal
        sFSP = gParametrosGenerales.gsFSP_SRV & "." & gParametrosGenerales.gsFSP_BD & ".dbo."
            
            sFSP = gParametrosGenerales.gsFSP_SRV & "." & gParametrosGenerales.gsFSP_BD & ".dbo."
            Set adocom = New ADODB.Command
            
            Set par = adocom.CreateParameter("OLD", adVarWChar, adParamInput, 50, CodViejo)
            adocom.Parameters.Append par
            Set par = adocom.CreateParameter("NEW", adVarWChar, adParamInput, 50, CodNuevo)
            adocom.Parameters.Append par

            Set adocom.ActiveConnection = g_adoconnCon
            adocom.CommandType = adCmdStoredProc
            adocom.CommandText = sFSP & "MON_COD"
            adocom.Prepared = True
            adocom.CommandTimeout = 1200   '20 min
            adocom.Execute
            If g_adoconnCon.Errors.Count > 0 Then
                GoTo Error
            End If
            Set adocom = Nothing
    End If
    
    ' Tratamiento para el LOG de cambios y la Integraci�n
    
    'If GrabarEnLogEntidad(EntidadIntegracion.Mon) Then

    If GrabarEnLogEntidad(EntidadIntegracion.Mon) And ContRegActualizados <> -1 Then

        Set adoRes = New ADODB.Recordset

        ''10/09/2007(ngo): MultiERP
        sConsulta = "SELECT COD FROM ERP WITH(NOLOCK)"
        adoRes.Open sConsulta, g_adoconnCon, adOpenStatic, adLockReadOnly
        While Not adoRes.eof
            'sConsulta = "INSERT INTO LOG_MON (ACCION, COD, EQUIV, EQ_ACT, COD_NEW, ORIGEN, USU, ERP) VALUES ('" & Accion_CambioCodigo & "',N'" & DblQuote(m_sCod) & "'"
            'sConsulta = "INSERT INTO LOG_MON (ACCION, COD, EQUIV, EQ_ACT, COD_NEW, ORIGEN, USU, ERP) VALUES ('C',N'" & DblQuote(CodViejo) & "'"
            'sConsulta = sConsulta & "," & DblToSQLFloat(m_dblEQUIV) & "," & BooleanToSQLBinary(m_bEQ_ACT) & ",N'" & DblQuote(CodigoNuevo) & "'," & m_udtOrigen & ",'" & DblQuote(m_sUsuario) & "','" & DblQuote(m_adores.Fields("COD").Value) & "')"
    
            sConsulta = "INSERT INTO LOG_MON (ACCION, COD, COD_NEW, ORIGEN, USU, ERP) VALUES ('C',N'" & DblQuote(CodViejo) & "'"
            sConsulta = sConsulta & ",N'" & DblQuote(CodNuevo) & "'," & m_Origen & ",'" & DblQuote(Usuario) & "','" & DblQuote(adoRes.Fields("COD").Value) & "')"
    
            'g_adoconnCon.Execute sConsulta
            Set adocom = New ADODB.Command
            Set adocom.ActiveConnection = g_adoconnCon
            adocom.CommandType = adCmdText
            adocom.CommandText = sConsulta
            adocom.Execute
            If g_adoconnCon.Errors.Count > 0 Then
                GoTo Error
            End If
            
            Dim ador As ADODB.Recordset
            Set ador = New ADODB.Recordset
            ador.Open "SELECT MAX(ID) FROM LOG_MON ", g_adoconnCon, adOpenForwardOnly, adLockReadOnly
            If Not ador.eof Then
                lIdLogMON = ador.Collect(0)
            End If
            ador.Close
            Set ador = Nothing
                                   
            
            sConsulta = "INSERT INTO LOG_MON_DEN (ID_LOG_MON,DEN,IDI) " & _
            " SELECT " & lIdLogMON & " AS ID, V.DEN AS DEN, V.IDIOMA AS IDI" & _
            " FROM MON_DEN V WITH(NOLOCK)" & _
            " WHERE V.MON=?"
            Set adocom = New ADODB.Command
            adocom.ActiveConnection = g_adoconnCon
            Set adoParam = adocom.CreateParameter("COD", adVarWChar, adParamInput, 50, Value:=CodNuevo)
            adocom.Parameters.Append adoParam
            
            adocom.CommandType = adCmdText
            adocom.CommandText = sConsulta
            adocom.Execute
            If g_adoconnCon.Errors.Count > 0 Then
                GoTo Error
            End If
            
            Set adocom = Nothing
            
            
            adoRes.MoveNext
        Wend
        adoRes.Close
    End If
    
    g_adoconnCon.Execute "SET XACT_ABORT OFF"
    g_adoconnCon.Execute "COMMIT TRAN", , adExecuteNoRecords
    

    ActualizarCodigoMoneda = True
    Exit Function
Error:
    GrabarErrorBDCodigo g_adoconnCon.Errors, lID
    If Not adocom Is Nothing Then Set adocom = Nothing
    ActualizarCodigoMoneda = False
    g_adoconnCon.Execute "IF @@TRANCOUNT>0 ROLLBACK TRANSACTION"
    g_adoconnCon.Execute "SET XACT_ABORT OFF"

End Function


''' <summary>Esta funci�n cambia de codigo de pais</summary>
''' <param name="lID">ID del registro</param>
''' <param name="CodViejo">Codigo viejo</param>
''' <param name="CodNuevo">Codigo nuevo</param>
''' <param name="Usuario">Usuario que realiza el cambio</param>
''' <returns>True si no hay errores, False si hay errores</returns>
''' <remarks>Llamada desde: ActualizarCodigos()
''' Tiempo m�ximo: 1 sec </remarks>
Public Function ActualizarCodigoPais(ByVal lID As Long, ByVal CodViejo As String, ByVal CodNuevo As String, ByVal Usuario As String) As Boolean
    
    Dim adocom As ADODB.Command
    Dim adoParam As ADODB.Parameter
    Dim oParam As ADODB.Parameter
    Dim ContRegActualizados As Long
    Dim sFSP As String
    Dim par As ADODB.Parameter
    Dim sConsulta As String
    Dim lIdLogPAI As Long
    Dim adoRes As ADODB.Recordset
    
On Error GoTo Error:
    
    g_adoconnCon.Execute "BEGIN DISTRIBUTED TRAN", , adExecuteNoRecords
    
    g_adoconnCon.Execute "SET XACT_ABORT ON"
    
    ''' Preparar la SP y sus parametros
    
    Set adocom = New ADODB.Command
    Set adocom.ActiveConnection = g_adoconnCon
    adocom.CommandType = adCmdStoredProc
    
    Set par = adocom.CreateParameter("PAR1", adVarWChar, adParamInput, 50, CodViejo)
    adocom.Parameters.Append par
    Set par = adocom.CreateParameter("PAR2", adVarWChar, adParamInput, 50, CodNuevo)
    adocom.Parameters.Append par
    
    adocom.CommandText = "PAI_COD"
    adocom.CommandTimeout = 1200   '20 min
    ''' Ejecutar la SP
    adocom.Execute ContRegActualizados
    If g_adoconnCon.Errors.Count > 0 Then
        GoTo Error
    End If
    Set adocom = Nothing
    
    If gParametrosGenerales.giINSTWEB = 2 Then 'ConPortal
        sFSP = gParametrosGenerales.gsFSP_SRV & "." & gParametrosGenerales.gsFSP_BD & ".dbo."
            
            sFSP = gParametrosGenerales.gsFSP_SRV & "." & gParametrosGenerales.gsFSP_BD & ".dbo."
            Set adocom = New ADODB.Command
            
            Set par = adocom.CreateParameter("OLD", adVarWChar, adParamInput, 50, CodViejo)
            adocom.Parameters.Append par
            Set par = adocom.CreateParameter("NEW", adVarWChar, adParamInput, 50, CodNuevo)
            adocom.Parameters.Append par

            Set adocom.ActiveConnection = g_adoconnCon
            adocom.CommandType = adCmdStoredProc
            adocom.CommandText = sFSP & "PAIS_COD"
            adocom.Prepared = True
            adocom.CommandTimeout = 1200   '20 min
            adocom.Execute
            If g_adoconnCon.Errors.Count > 0 Then
                GoTo Error
            End If
            Set adocom = Nothing
    End If
    
    ' Tratamiento para el LOG de cambios y la Integraci�n
    
    'If GrabarEnLogEntidad(EntidadIntegracion.Mon) Then

    If GrabarEnLogEntidad(EntidadIntegracion.Pai) And ContRegActualizados <> -1 Then

        Set adoRes = New ADODB.Recordset

        ''10/09/2007(ngo): MultiERP
        sConsulta = "SELECT COD FROM ERP WITH(NOLOCK)"
        adoRes.Open sConsulta, g_adoconnCon, adOpenStatic, adLockReadOnly
        While Not adoRes.eof
    
            sConsulta = "INSERT INTO LOG_PAI (ACCION, COD, COD_NEW, ORIGEN, USU, ERP) VALUES ('C',N'" & DblQuote(CodViejo) & "'"
            sConsulta = sConsulta & ",N'" & DblQuote(CodNuevo) & "'," & m_Origen & ",'" & DblQuote(Usuario) & "','" & DblQuote(adoRes.Fields("COD").Value) & "')"
    
            'g_adoconnCon.Execute sConsulta
            Set adocom = New ADODB.Command
            Set adocom.ActiveConnection = g_adoconnCon
            adocom.CommandType = adCmdText
            adocom.CommandText = sConsulta
            adocom.Execute
            If g_adoconnCon.Errors.Count > 0 Then
                GoTo Error
            End If
            
            Dim ador As ADODB.Recordset
            Set ador = New ADODB.Recordset
            ador.Open "SELECT MAX(ID) FROM LOG_PAI ", g_adoconnCon, adOpenForwardOnly, adLockReadOnly
            If Not ador.eof Then
                lIdLogPAI = ador.Collect(0)
            End If
            ador.Close
            Set ador = Nothing
                                   
            
            sConsulta = "INSERT INTO LOG_PAI_DEN (ID_LOG_PAI,DEN,IDI) " & _
            " SELECT " & lIdLogPAI & " AS ID, V.DEN AS DEN, V.IDIOMA AS IDI" & _
            " FROM PAI_DEN V WITH(NOLOCK)" & _
            " WHERE V.PAI=?"
            Set adocom = New ADODB.Command
            adocom.ActiveConnection = g_adoconnCon
            Set adoParam = adocom.CreateParameter("COD", adVarWChar, adParamInput, 50, Value:=CodNuevo)
            adocom.Parameters.Append adoParam
            
            adocom.CommandType = adCmdText
            adocom.CommandText = sConsulta
            adocom.Execute
            If g_adoconnCon.Errors.Count > 0 Then
                GoTo Error
            End If
            
            Set adocom = Nothing
            
            
            adoRes.MoveNext
        Wend
        adoRes.Close
    End If
   
         


    g_adoconnCon.Execute "SET XACT_ABORT OFF"
    g_adoconnCon.Execute "COMMIT TRAN", , adExecuteNoRecords
    

    ActualizarCodigoPais = True
    Exit Function
Error:
    GrabarErrorBDCodigo g_adoconnCon.Errors, lID
    If Not adocom Is Nothing Then Set adocom = Nothing
    ActualizarCodigoPais = False
    g_adoconnCon.Execute "IF @@TRANCOUNT>0 ROLLBACK TRANSACTION"
    g_adoconnCon.Execute "SET XACT_ABORT OFF"

End Function

''' <summary>Esta funci�n cambia de codigo de pais</summary>
''' <param name="lID">ID del registro</param>
''' <param name="CodViejo">Codigo viejo</param>
''' <param name="CodNuevo">Codigo nuevo</param>
''' <param name="Usuario">Usuario que realiza el cambio</param>
''' <returns>True si no hay errores, False si hay errores</returns>
''' <remarks>Llamada desde: ActualizarCodigos()
''' Tiempo m�ximo: 1 sec </remarks>
Public Function ActualizarCodigoProvincia(ByVal lID As Long, ByVal CodViejo As String, ByVal CodNuevo As String, ByVal Usuario As String, ByVal Pais As String) As Boolean
    
    Dim adocom As ADODB.Command
    Dim adoParam As ADODB.Parameter
    Dim oParam As ADODB.Parameter
    Dim ContRegActualizados As Long
    Dim sFSP As String
    Dim par As ADODB.Parameter
    Dim sConsulta As String
    Dim lIdLogPROVI As Long
    Dim sMoneda As String
    Dim sCod
    Dim adoRes As ADODB.Recordset
    
On Error GoTo Error:
    
    g_adoconnCon.Execute "BEGIN DISTRIBUTED TRAN", , adExecuteNoRecords
    
    g_adoconnCon.Execute "SET XACT_ABORT ON"
        
    
    ''' Preparar la SP y sus parametros
       
    Set adocom = New ADODB.Command
    Set adocom.ActiveConnection = g_adoconnCon
    adocom.CommandType = adCmdStoredProc
    
    Set par = adocom.CreateParameter("OLD", adVarWChar, adParamInput, 50, CodViejo)
    adocom.Parameters.Append par
    Set par = adocom.CreateParameter("NEW", adVarWChar, adParamInput, 50, CodNuevo)
    adocom.Parameters.Append par
    Set adoParam = adocom.CreateParameter("PAI", adVarWChar, adParamInput, 50, Pais)
    adocom.Parameters.Append adoParam

    adocom.CommandText = "PROVI_COD"
    adocom.CommandTimeout = 1200   '20 min
    ''' Ejecutar la SP
    adocom.Execute ContRegActualizados
    If g_adoconnCon.Errors.Count > 0 Then
        GoTo Error
    End If
    Set adocom = Nothing
    
    If gParametrosGenerales.giINSTWEB = 2 Then 'ConPortal
        sFSP = gParametrosGenerales.gsFSP_SRV & "." & gParametrosGenerales.gsFSP_BD & ".dbo."
            
            sFSP = gParametrosGenerales.gsFSP_SRV & "." & gParametrosGenerales.gsFSP_BD & ".dbo."
            Set adocom = New ADODB.Command
            
            Set par = adocom.CreateParameter("PAIS", adVarChar, adParamInput, 50, Pais)
            adocom.Parameters.Append par
            Set par = adocom.CreateParameter("OLD", adVarChar, adParamInput, 50, CodViejo)
            adocom.Parameters.Append par
            Set par = adocom.CreateParameter("NEW", adVarChar, adParamInput, 50, CodNuevo)
            adocom.Parameters.Append par

            Set adocom.ActiveConnection = g_adoconnCon
            adocom.CommandType = adCmdStoredProc
            adocom.CommandText = sFSP & "PROVI_COD"
            adocom.CommandTimeout = 1200   '20 min
            adocom.Prepared = True
            adocom.Execute
            If g_adoconnCon.Errors.Count > 0 Then
                GoTo Error
            End If
            Set adocom = Nothing
    End If
    
    ' Tratamiento para el LOG de cambios y la Integraci�n
    
    'If GrabarEnLogEntidad(EntidadIntegracion.Mon) Then

    If GrabarEnLogEntidad(EntidadIntegracion.Pai) And ContRegActualizados <> -1 Then

        Set adoRes = New ADODB.Recordset

        ''10/09/2007(ngo): MultiERP
        sConsulta = "SELECT COD FROM ERP WITH(NOLOCK)"
        adoRes.Open sConsulta, g_adoconnCon, adOpenStatic, adLockReadOnly
        While Not adoRes.eof
    
            sConsulta = "INSERT INTO LOG_PROVI (ACCION, PAI, COD, COD_NEW, ORIGEN, USU, ERP) VALUES ('C',N'" & DblQuote(Pais) & "',N'" & DblQuote(CodViejo) & "'"
            sConsulta = sConsulta & ",N'" & DblQuote(CodNuevo) & "'," & m_Origen & ",'" & DblQuote(Usuario) & "','" & DblQuote(adoRes.Fields("COD").Value) & "')"
    
            'g_adoconnCon.Execute sConsulta
            Set adocom = New ADODB.Command
            Set adocom.ActiveConnection = g_adoconnCon
            adocom.CommandType = adCmdText
            adocom.CommandText = sConsulta
            adocom.Execute
            If g_adoconnCon.Errors.Count > 0 Then
                GoTo Error
            End If
            
            Dim ador As ADODB.Recordset
            Set ador = New ADODB.Recordset
            ador.Open "SELECT MAX(ID) FROM LOG_PROVI ", g_adoconnCon, adOpenForwardOnly, adLockReadOnly
            If Not ador.eof Then
                lIdLogPROVI = ador.Collect(0)
            End If
            ador.Close
            Set ador = Nothing
                                   
            
            sConsulta = "INSERT INTO LOG_PROVI_DEN (ID_LOG_PROVI,DEN,IDI) " & _
            " SELECT " & lIdLogPROVI & " AS ID, V.DEN AS DEN, V.IDIOMA AS IDI" & _
            " FROM PROVI_DEN V WITH(NOLOCK)" & _
            " WHERE V.PAI= ? AND V.PROVI=?"
            Set adocom = New ADODB.Command
            adocom.ActiveConnection = g_adoconnCon
            Set adoParam = adocom.CreateParameter("PAI", adVarWChar, adParamInput, 50, Value:=Pais)
            adocom.Parameters.Append adoParam
            Set adoParam = adocom.CreateParameter("COD", adVarWChar, adParamInput, 50, Value:=CodNuevo)
            adocom.Parameters.Append adoParam
            
            adocom.CommandType = adCmdText
            adocom.CommandText = sConsulta
            adocom.Execute
            If g_adoconnCon.Errors.Count > 0 Then
                GoTo Error
            End If
            
            Set adocom = Nothing
            
            
            adoRes.MoveNext
        Wend
        adoRes.Close
    End If
   
         


    g_adoconnCon.Execute "SET XACT_ABORT OFF"
    g_adoconnCon.Execute "COMMIT TRAN", , adExecuteNoRecords
    

    ActualizarCodigoProvincia = True
    Exit Function
Error:
    GrabarErrorBDCodigo g_adoconnCon.Errors, lID
    If Not adocom Is Nothing Then Set adocom = Nothing
    ActualizarCodigoProvincia = False
    g_adoconnCon.Execute "IF @@TRANCOUNT>0 ROLLBACK TRANSACTION"
    g_adoconnCon.Execute "SET XACT_ABORT OFF"

End Function


''' <summary>Esta funci�n cambia de codigo de la persona</summary>
''' <param name="lID">ID del registro</param>
''' <param name="CodViejo">Codigo viejo</param>
''' <param name="CodNuevo">Codigo nuevo</param>
''' <param name="Usuario">Usuario que realiza el cambio</param>
''' <returns>True si no hay errores, False si hay errores</returns>
''' <remarks>Llamada desde: ActualizarCodigos()
''' Tiempo m�ximo: 1 sec </remarks>

Public Function ActualizarCodigoPersona(ByVal lID As Long, ByVal CodViejo As String, ByVal CodNuevo As String, ByVal Usuario As String) As Boolean
    Dim oCom As ADODB.Command
    Dim oPar As ADODB.Parameter

    On Error GoTo Error
    
    ActualizarCodigoPersona = False
    
    g_adoconnCon.Execute "BEGIN DISTRIBUTED TRAN", , adExecuteNoRecords
    g_adoconnCon.Execute "SET XACT_ABORT ON"
        
    Set oCom = New ADODB.Command
    With oCom
        Set .ActiveConnection = g_adoconnCon
        .CommandType = adCmdStoredProc
        .CommandText = "PER_COD"
        .CommandTimeout = 1200   '20 min
        
        Set oPar = .CreateParameter("PAR1", adVarChar, adParamInput, 20, CodViejo)
        .Parameters.Append oPar
        Set oPar = .CreateParameter("PAR2", adVarChar, adParamInput, 20, CodNuevo)
        .Parameters.Append oPar
    
        .Execute
    End With

    If g_adoconnCon.Errors.Count > 0 Then Err.Raise g_adoconnCon.Errors(0).Number
    
    g_adoconnCon.Execute "SET XACT_ABORT OFF"
    g_adoconnCon.Execute "COMMIT TRAN", , adExecuteNoRecords

    ActualizarCodigoPersona = True
    
Salir:
    Set oCom = Nothing
    Set oPar = Nothing
    Exit Function
Error:
    GrabarErrorBDCodigo g_adoconnCon.Errors, lID
    ActualizarCodigoPersona = False
    g_adoconnCon.Execute "IF @@TRANCOUNT>0 ROLLBACK TRANSACTION"
    g_adoconnCon.Execute "SET XACT_ABORT OFF"
    Resume Salir
End Function


''' <summary>Esta funci�n cambia de codigo de moneda</summary>
''' <param name="lID">ID del registro</param>
''' <param name="CodViejo">Codigo viejo</param>
''' <param name="CodNuevo">Codigo nuevo</param>
''' <param name="Usuario">Usuario que realiza el cambio</param>
''' <returns>True si no hay errores, False si hay errores</returns>
''' <remarks>Llamada desde: ActualizarCodigos()
''' Tiempo m�ximo: 1 sec </remarks>
Public Function ActualizarCodigoUnidad(ByVal lID As Long, ByVal CodViejo As String, ByVal CodNuevo As String, ByVal Usuario As String) As Boolean

    Dim TESError As TipoErrorSummit
    Dim adocom As ADODB.Command
    Dim oParam As ADODB.Parameter
    Dim ContRegActualizados As Long
    Dim bLog As Boolean
    Dim adoRes As ADODB.Recordset
    Dim sConsulta As String
    Dim lIdLogUNI As Long
    Dim par As ADODB.Parameter
    
 '******************************************
'***** M U Y   I M P O R T A N T E     ****
'*****                                 ****
'***** SI SE REALIZAN CAMBIOS          ****
'***** DESPUES DEL 21/06/2002          ****
'***** MIRAR SI HAY QUE PASARLOS A     ****
'***** FSGSINTEGRATION.DLL             ****
'******************************************
   
On Error GoTo Error:
    
    
    Set adocom = New ADODB.Command
    Set adocom.ActiveConnection = g_adoconnCon
    adocom.CommandType = adCmdStoredProc
    
    Set par = adocom.CreateParameter("OLD", adVarWChar, adParamInput, 50, CodViejo)
    adocom.Parameters.Append par
    Set par = adocom.CreateParameter("NEW", adVarWChar, adParamInput, 50, CodNuevo)
    adocom.Parameters.Append par
        
    adocom.CommandText = "UNI_COD"
    adocom.CommandTimeout = 1200   '20 min
    ''' Ejecutar la SP
    
    adocom.Execute ContRegActualizados
    If g_adoconnCon.Errors.Count > 0 Then
        GoTo Error
    End If
    
    
    
    Set adocom = Nothing
    
    Set adoRes = New ADODB.Recordset
       
    ''' Tratamiento para el LOG de cambios y la Integraci�n. Solo grabamos si se han actualizado de verdad alg�n registro
    bLog = True
    
    If GrabarEnLogEntidad(EntidadIntegracion.Uni) And ContRegActualizados <> -1 Then
        
        ''10/09/2007(ngo): MultiERP
        sConsulta = "SELECT COD FROM ERP WITH(NOLOCK)"
        adoRes.Open sConsulta, g_adoconnCon, adOpenForwardOnly, adLockReadOnly
       
        
        While Not adoRes.eof
            sConsulta = "INSERT INTO LOG_UNI (ACCION, COD, COD_NEW, ORIGEN, USU, ERP) VALUES ('C',N'" & DblQuote(CodViejo) & "'"
            sConsulta = sConsulta & ", N'" & DblQuote(CodNuevo) & "'," & m_Origen & ",'" & DblQuote(Usuario) & "','" & DblQuote(adoRes.Fields("COD").Value) & "')"
            Set adocom = New ADODB.Command
            adocom.ActiveConnection = g_adoconnCon
            adocom.CommandType = adCmdText
            adocom.CommandText = sConsulta
            adocom.Execute

            If g_adoconnCon.Errors.Count > 0 Then
                GoTo Error
            End If
            
            Dim ador As ADODB.Recordset
            Set ador = New ADODB.Recordset
            ador.Open "SELECT MAX(ID) FROM LOG_UNI ", g_adoconnCon, adOpenForwardOnly, adLockReadOnly
            If Not ador.eof Then
                lIdLogUNI = ador.Collect(0)
            End If
            ador.Close
            Set ador = Nothing
                                                                   
            sConsulta = "INSERT INTO LOG_UNI_DEN (ID_LOG_UNI,DEN,IDI) " & _
            " SELECT " & lIdLogUNI & " AS ID, U.DEN AS DEN, U.IDIOMA AS IDI" & _
            " FROM UNI_DEN U WITH(NOLOCK)" & _
            " WHERE U.UNI=?"

            Set adocom = New ADODB.Command
            adocom.ActiveConnection = g_adoconnCon
            adocom.CommandType = adCmdText
            Set par = adocom.CreateParameter("COD", adVarWChar, adParamInput, 50, Value:=CodNuevo)
            adocom.Parameters.Append par
            
            adocom.CommandText = sConsulta
            adocom.Execute
                        
            If g_adoconnCon.Errors.Count > 0 Then
                GoTo Error
            End If
            

            adoRes.MoveNext
        Wend
        
        adoRes.Close
    End If
    bLog = False
       
    
    ActualizarCodigoUnidad = True
    
    Exit Function
    
Error:
    GrabarErrorBDCodigo g_adoconnCon.Errors, lID
    
    If Not adocom Is Nothing Then Set adocom = Nothing
    ActualizarCodigoUnidad = False
   
    If bLog Then 'Si el error se ha producido al grabar en la tabla de LOG, deshacemos los cambios a mano
        Set adocom = Nothing
        Set adocom = New ADODB.Command
        Set adocom.ActiveConnection = g_adoconnCon
        Set oParam = adocom.CreateParameter("PAR1", adVarChar, adParamInput, 50, CodNuevo)
        adocom.Parameters.Append oParam
        Set oParam = adocom.CreateParameter("PAR2", adVarChar, adParamInput, 50, CodViejo)
        adocom.Parameters.Append oParam
        adocom.CommandType = adCmdStoredProc
        adocom.CommandText = "UNI_COD"
        adocom.CommandTimeout = 1200   '20 min
        adocom.Execute
    End If
     
    If Not adocom Is Nothing Then
        Set adocom = Nothing
    End If
End Function


''' <summary>Esta funci�n cambia de codigo la v�a de pago</summary>
''' <param name="CodViejo">Codigo viejo</param>
''' <param name="CodNuevo">Codigo nuevo</param>
''' <param name="Usuario">Usuario que realiza el cambio</param>
''' <returns>True si no hay errores, False si hay errores</returns>
''' <remarks>Llamada desde: ActualizarCodigoViaPago()
''' Tiempo m�ximo: 1 sec </remarks>
Private Function ActualizarLOGViaPago(ByVal CodViejo As Variant, ByVal CodNuevo As Variant, ByVal USU As Variant, ByVal Origen As Variant) As Boolean
    Dim sConsulta As String
    Dim adoRes As ADODB.Recordset
    Dim adoComm As ADODB.Command
    Dim adoParam As ADODB.Parameter
    Dim sCod As String
    Dim sDen As String
    Dim lIdLogVIA_PAG As Long
    
    On Error GoTo Error:

        sConsulta = "INSERT INTO LOG_VIA_PAG (ACCION, COD, COD_NEW, ORIGEN, USU, ERP)" & _
            " SELECT 'C' AS ACCION, ? AS COD, ? AS COD_NEW, ? AS ORIGEN, ? AS USU, E.COD AS ERP" & _
            " FROM ERP E" & _
            " INNER JOIN VIA_PAG V ON V.COD=?"
        Set adoComm = New ADODB.Command
        adoComm.ActiveConnection = g_adoconnCon
        Set adoParam = adoComm.CreateParameter("OLD", adVarWChar, adParamInput, 50, Value:=CodViejo)
        adoComm.Parameters.Append adoParam
        Set adoParam = adoComm.CreateParameter("NEW", adVarWChar, adParamInput, 50, Value:=CodNuevo)
        adoComm.Parameters.Append adoParam
        Set adoParam = adoComm.CreateParameter("ORIGEN", adTinyInt, adParamInput, Value:=Origen)
        adoComm.Parameters.Append adoParam
        Set adoParam = adoComm.CreateParameter("USU", adVarChar, adParamInput, 50, Value:=USU)
        adoComm.Parameters.Append adoParam
        Set adoParam = adoComm.CreateParameter("VIAPAG", adVarChar, adParamInput, 50, Value:=CodNuevo)
        adoComm.Parameters.Append adoParam
        
        adoComm.CommandType = adCmdText
        adoComm.CommandText = sConsulta
        adoComm.Execute
        Set adoComm = Nothing
        
        Dim ador As ADODB.Recordset
        Set ador = New ADODB.Recordset
        ador.Open "SELECT MAX(ID) FROM LOG_VIA_PAG WHERE COD = N'" & DblQuote(CodViejo) & "'", g_adoconnCon, adOpenForwardOnly, adLockReadOnly
        If Not ador.eof Then
            lIdLogVIA_PAG = ador.Collect(0)
        End If
        ador.Close
        Set ador = Nothing

        sConsulta = "INSERT INTO LOG_VIA_PAG_DEN (ID_LOG_VIA_PAG,DEN,IDI) " & _
        " SELECT " & lIdLogVIA_PAG & " AS ID_LOG_VIA_PAG, V.DEN AS DEN, V.IDIOMA AS IDI" & _
        " FROM VIA_PAG_DEN V WITH(NOLOCK)" & _
        " WHERE V.VIA_PAG=?"
        Set adoComm = New ADODB.Command
        adoComm.ActiveConnection = g_adoconnCon
        Set adoParam = adoComm.CreateParameter("COD", adVarWChar, adParamInput, 50, Value:=CodNuevo)
        adoComm.Parameters.Append adoParam
        
        adoComm.CommandType = adCmdText
        adoComm.CommandText = sConsulta
        adoComm.Execute
        Set adoComm = Nothing
        
        ActualizarLOGViaPago = (g_adoconnCon.Errors.Count = 0)
        
    Exit Function
Error:
    ActualizarLOGViaPago = False
End Function


Private Function ActualizarCodigoART4EsOK(ByVal CodViejo As Variant, ByVal CodNuevo As Variant, ByVal GMN1 As Variant) As Boolean

    ''' * Objetivo: Cambiar de codigo el art�culo
    ''' * Recibe: Codigo nuevo
    ''' * Devuelve: 1 si no hay errores, 0 si hay errores
    
    Dim adocom As ADODB.Command
    Dim oParam As ADODB.Parameter
    
    
On Error GoTo Error:
    
    ActualizarCodigoART4EsOK = False

    Set adocom = New ADODB.Command
    Set adocom.ActiveConnection = g_adoconnCon
    adocom.CommandType = adCmdStoredProc
    adocom.CommandTimeout = 1200   '20 min
       
    ''' Preparar la SP y sus par�metros
    
    Set oParam = adocom.CreateParameter("OLD", adVarChar, adParamInput, 50, CodViejo)
    adocom.Parameters.Append oParam
        
    Set oParam = adocom.CreateParameter("NEW", adVarChar, adParamInput, 50, CodNuevo)
    adocom.Parameters.Append oParam

    adocom.CommandText = "ART4_COD"
     
   
    ''' Ejecutar la SP
    adocom.Execute
    If g_adoconnCon.Errors.Count > 0 Then
        GoTo Error
    End If
    
    Set adocom = Nothing
    
    ActualizarCodigoART4EsOK = True
    
    Exit Function
    
Error:
    
    If Not adocom Is Nothing Then
        Set adocom = Nothing
    End If

End Function

Private Function ActualizarLOGART4EsOK(ByVal CodViejo As Variant, ByVal CodNuevo As Variant, ByVal GMN1 As Variant, ByVal GMN2 As Variant, ByVal GMN3 As Variant, ByVal GMN4 As Variant, ByVal Den As Variant, ByVal USU As Variant, ByVal Origen As Variant) As Boolean
Dim sConsulta As String
Dim adoRes As ADODB.Recordset

On Error GoTo Error:
    
    'Si existe en ART4_UON, inserta �nicamente para ese ERP. Si no, para todos los ERP
    sConsulta = "SELECT ERP_ORGCOMPRAS.ERP, ISNULL(TIE.SENTIDO,0) AS SENTIDO FROM ART4_UON WITH(NOLOCK)" & _
        " INNER JOIN UON2 WITH(NOLOCK) ON ART4_UON.UON1=UON2.UON1 AND ART4_UON.UON2=UON2.COD" & _
        " INNER JOIN EMP WITH(NOLOCK) ON UON2.EMPRESA=EMP.ID" & _
        " INNER JOIN ERP_ORGCOMPRAS WITH(NOLOCK) ON ERP_ORGCOMPRAS.ORGCOMPRAS=EMP.ORG_COMPRAS" & _
        " INNER JOIN TABLAS_INTEGRACION_ERP TIE WITH(NOLOCK) ON TIE.ERP=ERP_ORGCOMPRAS.ERP" & _
        " WHERE ART4_UON.ART4='" & DblQuote(CodNuevo) & "' AND TIE.TABLA=" & EntidadIntegracion.art4 & " AND ACTIVA=1"
    Set adoRes = New ADODB.Recordset
    adoRes.Open sConsulta, g_adoconnCon, adOpenForwardOnly, adLockReadOnly
    
    If Not adoRes.eof Then
        If Not IsNull(adoRes.Fields("ERP").Value) Then
            'Si ese ERP tiene sentido <> entrada se inserta en la tabla de log
            If adoRes.Fields("SENTIDO").Value = SentidoIntegracion.EntradaSalida Or adoRes.Fields("SENTIDO").Value = SentidoIntegracion.Salida Then
                sConsulta = "INSERT INTO LOG_ART4 (ACCION,GMN1,GMN2,GMN3,GMN4,COD,DEN,COD_NEW,USU,ORIGEN,ERP)"
                sConsulta = sConsulta & " VALUES ('C','" & DblQuote(GMN1) & "','" & DblQuote(GMN2) & "','" & DblQuote(GMN3) & "','" & DblQuote(GMN4) & "','" & DblQuote(CodViejo) & "','" & DblQuote(Den) & "','" & DblQuote(CodNuevo) & "','" & DblQuote(USU) & "'," & Origen & "," & StrToSQLNULL(adoRes.Fields("ERP").Value) & ")"
                g_adoconnCon.Execute sConsulta
                If g_adoconnCon.Errors.Count > 0 Then
                    GoTo Error
                End If
            End If
            adoRes.Close
            ActualizarLOGART4EsOK = True
            Exit Function
        End If
    End If
    adoRes.Close
    sConsulta = "SELECT ERP, ISNULL(SENTIDO,0) AS SENTIDO FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK) WHERE TABLA=" & EntidadIntegracion.art4 & " AND ACTIVA=1 ORDER BY ERP"
    adoRes.Open sConsulta, g_adoconnCon, adOpenForwardOnly, adLockReadOnly
    While Not adoRes.eof
        ''Si ese ERP tiene sentido <> entrada se inserta en la tabla de log
        If adoRes.Fields("SENTIDO").Value = SentidoIntegracion.EntradaSalida Or adoRes.Fields("SENTIDO").Value = SentidoIntegracion.Salida Then
            sConsulta = "INSERT INTO LOG_ART4 (ACCION,GMN1,GMN2,GMN3,GMN4,COD,DEN,COD_NEW,USU,ORIGEN,ERP)"
            sConsulta = sConsulta & " VALUES ('C','" & DblQuote(GMN1) & "','" & DblQuote(GMN2) & "','" & DblQuote(GMN3) & "','" & DblQuote(GMN4) & "','" & DblQuote(CodViejo) & "','" & DblQuote(Den) & "','" & DblQuote(CodNuevo) & "','" & DblQuote(USU) & "'," & Origen & "," & StrToSQLNULL(adoRes.Fields("ERP").Value) & ")"
            g_adoconnCon.Execute sConsulta
            If g_adoconnCon.Errors.Count > 0 Then
                GoTo Error
            End If
  End If
  adoRes.MoveNext
    Wend
    adoRes.Close
    ActualizarLOGART4EsOK = True
    
    
    Exit Function
Error:

    ActualizarLOGART4EsOK = False
    
End Function


