Attribute VB_Name = "basProcesos"
Option Explicit

'<summary>Revisa si hay alguna subasta pausada que tenga que ser reiniciada autom�ticamente y, si es as�, la reinicia</summary>
'<remarks>Llamada desde: CBatUpd.LimpiarReunionesVirtuales, Tiempo m�ximo: 0,3 seg</remarks>
    
Public Function RevisarSubastasPausadas() As Long
    Dim oComm As ADODB.Command
    
    On Error GoTo Error
    
    RevisarSubastasPausadas = 1
    
    g_adoconnCon.Execute "BEGIN DISTRIBUTED TRAN", , adExecuteNoRecords
    g_adoconnCon.Execute "SET XACT_ABORT ON"
    
    Set oComm = New ADODB.Command
    With oComm
        .ActiveConnection = g_adoconnCon
        .CommandType = adCmdStoredProc
        .CommandText = "FSGS_REVISAR_SUBASTAS_PAUSADAS"
        .CommandTimeout = 600
        .Execute
    End With
    
    If g_adoconnCon.Errors.Count > 0 Then Err.Raise g_adoconnCon.Errors(0).Number
    
    g_adoconnCon.Execute "SET XACT_ABORT OFF"
    g_adoconnCon.Execute "COMMIT TRAN", , adExecuteNoRecords
    
Salir:
    Set oComm = Nothing
    Exit Function
Error:
    RevisarSubastasPausadas = 0
    g_adoconnCon.Execute "IF @@TRANCOUNT>0 ROLLBACK TRANSACTION"
    g_adoconnCon.Execute "SET XACT_ABORT OFF"
    Resume Salir
End Function
