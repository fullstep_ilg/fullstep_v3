Attribute VB_Name = "basUtilidades"
Option Explicit

Public Const REG_SZ As Long = 1

Private Const ClaveSQLtod = "aldj�w"
Private Const ClaveADMpar = "pruiop"
Private Const ClaveADMimp = "ljkdag"
Private Const ClaveUSUpar = "agkag�"
Private Const ClaveUSUimp = "h+hlL_"

Private Enum TipoDeError
    ConexionNoEstablecida = 613
End Enum

Public Function NullToStr(ByVal ValueThatCanBeNull As Variant) As String
  
    If IsNull(ValueThatCanBeNull) Then
        NullToStr = ""
    Else
        NullToStr = ValueThatCanBeNull
    End If

End Function

Public Function DblQuote(ByVal StrToDblQuote As String) As String

    Dim Ind As Long
    Dim QPos As Long
    Dim pos As Long
    
    Dim TmpDblQuote As String
    
    TmpDblQuote = ""
    pos = 1
    
    Do While Len(StrToDblQuote) > 0
    
        QPos = InStr(pos, StrToDblQuote, "'")
        
        If QPos > 0 Then
            
            TmpDblQuote = TmpDblQuote & Mid(StrToDblQuote, pos, QPos - 1) & "''"
            StrToDblQuote = Right(StrToDblQuote, Len(StrToDblQuote) - QPos)
            
        Else
        
            TmpDblQuote = TmpDblQuote & StrToDblQuote
            Exit Do
            
        End If
            
    Loop
    
    DblQuote = TmpDblQuote
    
End Function

Function StrToSQLNULL(StrThatCanBeEmpty) As Variant
    
    If IsEmpty(StrThatCanBeEmpty) Then
        StrToSQLNULL = "NULL"
    Else
        If IsNull(StrThatCanBeEmpty) Then
            StrToSQLNULL = "NULL"
        Else
            If StrThatCanBeEmpty = "" Then
                StrToSQLNULL = "NULL"
            Else
                StrToSQLNULL = "'" & DblQuote(CStr(StrThatCanBeEmpty)) & "'"
            End If
        End If
    End If
    
End Function
Function DateToSQLTimeDate(DateThatCanBeEmpty) As Variant


If IsEmpty(DateThatCanBeEmpty) Then
    DateToSQLTimeDate = "NULL"
Else
    If IsNull(DateThatCanBeEmpty) Then
        DateToSQLTimeDate = "NULL"
    Else
        If DateThatCanBeEmpty = "" Then
            DateToSQLTimeDate = "NULL"
        Else
            DateToSQLTimeDate = "Convert(datetime,'" & Format(DateThatCanBeEmpty, "mm-dd-yyyy hh:mm:ss") & "',110)"
        End If
    End If
End If

End Function
Function DesactivarConexion(ByVal x As Integer) As Integer

    DesactivarConexion = REG_SZ * x
    'Desactivada
    
End Function



Public Function NullToDbl0(ByVal ValueThatCanBeNull As Variant) As Variant
  
    If IsNull(ValueThatCanBeNull) Then
        NullToDbl0 = 0
    Else
        NullToDbl0 = ValueThatCanBeNull
    End If

End Function


''' <summary>
''' Extrae la contrase�a del fichero FSPSDB.LIC y la desencripta.
''' </summary>
''' <param name></param>
''' <returns>La contrase�a desencriptada</returns>
''' <remarks>Llamada desde: CBatupd, CRaiz ; Tiempo m�ximo: 0,1</remarks>

Public Function LICDBContra() As String
    Dim fso As New FileSystemObject, fil As File, ts As TextStream
    Dim FechaHora As String
    Dim tmplogin As String
    Dim tmpcontra As String

    Set fil = fso.GetFile(App.Path & "\FSDB.LIC")
    Set ts = fil.OpenAsTextStream(ForReading)
    
    FechaHora = ts.ReadLine
    tmplogin = ts.ReadLine
    tmpcontra = ts.ReadLine
    ts.Close
        
    ''' Desencriptar contrase�a
    LICDBContra = EncriptarAES("", tmpcontra, False, 0, FechaHora)

End Function

''' <summary>
''' Extrae el login del fichero FSPSDB.LIC y lo desencripta.
''' </summary>
''' <param name></param>
''' <returns>La contrase�a desencriptada</returns>
''' <remarks>Llamada desde: CBatupd, CRaiz ; Tiempo m�ximo: 0,1</remarks>

Public Function LICDBLogin() As String
    Dim fso As New FileSystemObject, fil As File, ts As TextStream
    Dim FechaHora As String
    Dim tmplogin As String
    Dim tmpcontra As String

    Set fil = fso.GetFile(App.Path & "\FSDB.LIC")
    Set ts = fil.OpenAsTextStream(ForReading)
    
    FechaHora = ts.ReadLine
    tmplogin = ts.ReadLine
    tmpcontra = ts.ReadLine
    ts.Close
        
    ''' Desencriptar login
    LICDBLogin = EncriptarAES("", tmplogin, False, 0, FechaHora)
End Function




Public Function SQLBinaryToBoolean(ByVal var As Variant) As Variant
    If IsNull(var) Then
        SQLBinaryToBoolean = False
    Else
        If var = 1 Then
            SQLBinaryToBoolean = True
        Else
            SQLBinaryToBoolean = False
        End If
    End If
End Function

Function StrToVbNULL(StrThatCanBeEmpty) As Variant
    
    If IsEmpty(StrThatCanBeEmpty) Then
        StrToVbNULL = Null
    Else
        If IsNull(StrThatCanBeEmpty) Then
            StrToVbNULL = Null
        Else
            If StrThatCanBeEmpty = "" Then
                StrToVbNULL = Null
            Else
                StrToVbNULL = StrThatCanBeEmpty
            End If
        End If
    End If
    
End Function

''' <summary>Funci�n que encripta/desencripta el pwd</summary>
''' <param name="DatoUsu">C�digo del usuario</param>
''' <param name="DatoPwd">Password</param>
''' <param name="Encriptando">True or false.</param>Tipo
''' <param name="Tipo">Tipo de usuario</param>
''' <param name="Fecha">Fecha y hora de la encriptaci�n</param>
''' <returns>El pwd encriptado/desencriptado</returns>
''' <remarks>Llamada desde: basActCodigos.ObtenerParametrosGenerales</remarks>
Public Function EncriptarAES(ByVal DatoUsu As String, ByVal DatoPwd As String, ByVal Encriptando As Boolean, ByVal Tipo As Integer _
, ByVal Fecha As Variant) As String
    Dim fechahoracrypt As Date
    Dim diacrypt
    Dim oCrypt2 As cCrypt2
        
    fechahoracrypt = Fecha
    
    Set oCrypt2 = New cCrypt2
    
    oCrypt2.InBuffer = DatoPwd
    
    diacrypt = Day(Fecha)
    
    Select Case Tipo
    Case 0
        oCrypt2.Codigo = DatoUsu & ClaveSQLtod
    Case 1
        If diacrypt Mod 2 = 0 Then
            oCrypt2.Codigo = DatoUsu & ClaveADMpar
        Else
            oCrypt2.Codigo = DatoUsu & ClaveADMimp
        End If
    Case Else
        If diacrypt Mod 2 = 0 Then
            oCrypt2.Codigo = DatoUsu & ClaveUSUpar
        Else
            oCrypt2.Codigo = DatoUsu & ClaveUSUimp
        End If
    End Select
    
    oCrypt2.Fecha = fechahoracrypt
    
    'Datos
    If Encriptando Then
        oCrypt2.Encrypt
    Else
        oCrypt2.Decrypt
    End If
    
    EncriptarAES = oCrypt2.OutBuffer
                            
    Set oCrypt2 = Nothing
End Function

''' <summary>
''' Grabar un error en una comunicaci�n
''' </summary>
''' <param name="Pagina">Funci�n o pantalla q da el error</param>
''' <param name="NumError">Numero de Error VB. El Id de la tabla va en teserror.arg1</param>
''' <param name="Message">Mensaje del error VB</param>
''' <param name="Usuario">Cod Usu Conectado</param>
''' <returns>Error de haberlo</returns>
''' <remarks>Llamada desde: frmAdjAnya/cmdAceptar_Click frmObjAnya/cmdAceptar_Click frmOfePetAnya/cmdAceptar_Click
'''     basUtilidades/EnviarMensaje     basUtilidades/GrabarEnviarMensaje ; Tiempo m�ximo: 0,2</remarks>
''' Revisado por: jbg; Fecha: 13/06/2013
Public Function GrabarError(ByVal Pagina As String, ByVal NumError As String, ByVal Message As String, Optional ByVal Usuario As String = "") As TipoErrorSummit
    Dim TESError As TipoErrorSummit
    Dim rs As adodb.Recordset
    
    '********* Precondicion *******************
    If g_adoconnCon Is Nothing Then
        Err.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CConexion.GrabarError", "No se ha establecido la conexion"
    End If
    
    On Error GoTo Error:

    TESError.NumError = 0 'TESnoerror
    g_adoconnCon.Execute "BEGIN TRANSACTION"
        
    Pagina = "GSBatchUpdates: " & Pagina
        
    g_adoconnCon.Execute "INSERT INTO ERRORES (FEC_ALTA,PAGINA,USUARIO,EX_FULLNAME,EX_MESSAGE)" _
        & " VALUES(GETDATE()," & StrToSQLNULL(Pagina) & "," & StrToSQLNULL(Usuario) _
        & "," & StrToSQLNULL(NumError) & "," & StrToSQLNULL(Message) & ")"
        
    If g_adoconnCon.Errors.Count > 0 Then GoTo Error
    
    Set rs = New adodb.Recordset
    'Buscamos el numero de id maximo
    'CALIDAD: Sin WITH (NOLOCK) porque es para obtener el nuevo ID
    rs.Open "SELECT MAX(ID) AS NUM FROM ERRORES", g_adoconnCon, adOpenForwardOnly, adLockReadOnly
    If rs.eof Then
        TESError.NumError = 3 'TESDatoEliminado
        g_adoconnCon.Execute "ROLLBACK TRANSACTION"
        rs.Close
        Set rs = Nothing
        GrabarError = TESError
        Exit Function
    Else
        TESError.Arg1 = NullToDbl0(rs(0).Value)
        TESError.Arg2 = Message
    End If
    
    rs.Close
    Set rs = Nothing
    
    g_adoconnCon.Execute "COMMIT TRANSACTION"
    
    GrabarError = TESError
    
    Exit Function
    
Error:
    TESError.NumError = 100 'Tesotroerror
    TESError.Arg1 = g_adoconnCon.Errors(0).Description

    GrabarError = TESError
    
    g_adoconnCon.Execute "ROLLBACK TRANSACTION"
    
    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
End Function




