VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CVariablesCalidad"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False





Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
    
End Enum

Private mCol As Collection

Private m_oConexion As CConexion
Private m_bEOF As Boolean
Private m_lMaxId1 As Long 'Contendr� el maximo id en BD para el nivel correspondiente
Private m_lMaxId2 As Long
Private m_lMaxId3 As Long
Private m_lMaxId4 As Long
Private m_lMaxId5 As Long
Private m_vFormula As Variant 'Formula de proveedor si estoy en las vars de nivel 1
Private m_vIDFormula As Variant

'Propiedad para la puntuaci�n de los proveedores
Private m_vPuntuacion    As Variant
Private m_vCalificacionID As Variant
Private m_vPuntFecha    As Variant

Public Property Get Formula() As Variant
    Formula = m_vFormula
End Property
Public Property Let Formula(ByVal b As Variant)
    m_vFormula = b
End Property
Public Property Get FormulaID() As Variant
    FormulaID = m_vIDFormula
End Property
Public Property Let FormulaID(ByVal b As Variant)
    m_vIDFormula = b
End Property

Public Property Get MaxID1() As Long
    MaxID1 = m_lMaxId1
End Property
Public Property Let MaxID1(ByVal b As Long)
    m_lMaxId1 = b
End Property

Public Property Get MaxID2() As Long
    MaxID2 = m_lMaxId2
End Property
Public Property Let MaxID2(ByVal b As Long)
    m_lMaxId2 = b
End Property

Public Property Get MaxID3() As Long
    MaxID3 = m_lMaxId3
End Property
Public Property Let MaxID3(ByVal b As Long)
    m_lMaxId3 = b
End Property

Public Property Get MaxID4() As Long
    MaxID4 = m_lMaxId4
End Property
Public Property Let MaxID4(ByVal b As Long)
    m_lMaxId4 = b
End Property

Public Property Get MaxID5() As Long
    MaxID5 = m_lMaxId5
End Property
Public Property Let MaxID5(ByVal b As Long)
    m_lMaxId5 = b
End Property

Public Property Let Puntuacion(ByVal dato As Variant)
    m_vPuntuacion = dato
End Property
Public Property Get Puntuacion() As Variant
    Puntuacion = m_vPuntuacion
End Property
Public Property Let CalificacionID(ByVal dato As Variant)
    m_vCalificacionID = dato
End Property
Public Property Get CalificacionID() As Variant
    CalificacionID = m_vCalificacionID
End Property

Public Property Let PuntFecha(ByVal dato As Variant)
    m_vPuntFecha = dato
End Property
Public Property Get PuntFecha() As Variant
    PuntFecha = m_vPuntFecha
End Property

Public Property Get eof() As Boolean
    eof = m_bEOF
End Property
Friend Property Let eof(ByVal b As Boolean)
    m_bEOF = b
End Property

Friend Property Set Conexion(ByVal vData As CConexion)
    Set m_oConexion = vData
End Property

Friend Property Get Conexion() As CConexion
    Set Conexion = m_oConexion
End Property

Public Property Get Item(vntIndexKey As Variant) As CVariableCalidad

    ''' * Objetivo: Recuperar una moneda de la coleccion
    ''' * Recibe: Indice de la moneda a recuperar
    ''' * Devuelve: Moneda correspondiente
        
On Error GoTo NoSeEncuentra:

    ''' Devolvemos el item de la coleccion privada
    
    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:

    Set Item = Nothing
    
End Property

Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4

    ''' ! Pendiente de revision, objetivo desconocido
    
    Set NewEnum = mCol.[_NewEnum]

End Property

Public Sub Remove(vntIndexKey As Variant)

    ''' * Objetivo: Eliminar una moneda de la coleccion
    ''' * Recibe: Indice de la moneda a eliminar
    ''' * Devuelve: Nada
   
    mCol.Remove vntIndexKey
    
End Sub
Private Sub Class_Initialize()

    ''' * Objetivo: Crear la coleccion
    Set mCol = New Collection
    
End Sub
Private Sub Class_Terminate()

    ''' * Objetivo: Limpiar memoria
    
    Set mCol = Nothing
    Set m_oConexion = Nothing
End Sub

Public Property Get Count() As Long

    If mCol Is Nothing Then
        Count = 0
    Else
         Count = mCol.Count
    End If

End Property


Public Function Add(ByVal ID As Long, ByVal Nivel As Integer, ByVal Cod As String, ByVal Den As String, _
                    Optional ByVal Tipo As Variant, Optional ByVal Subtipo As Variant, Optional ByVal Formula As Variant, _
                    Optional ByVal Origen As Variant, Optional ByVal FechaActual As Variant, Optional ByVal vIndice As Variant, _
                    Optional ByVal vIDCal1 As Variant, Optional ByVal vIDCal2 As Variant, Optional ByVal vIDCal3 As Variant, Optional ByVal vIDCal4 As Variant, Optional ByVal OrigenCod As Variant, _
                    Optional ByVal Modificado As Boolean, Optional ByVal FormCod As Variant, Optional ByVal FormID As Variant, _
                    Optional ByVal vConsulta As Variant, Optional ByVal vModificacion As Variant, Optional ByVal vIDFormula As Variant, _
                    Optional ByVal vTipoPond As Variant, Optional ByVal vPeriodo As Variant, Optional ByVal vPubPortal As Variant, _
                    Optional ByVal vPuntuacion As Variant, Optional ByVal vCalificacion As Variant, Optional ByVal vPuntFecha As Variant, Optional ByVal vProveMat As Variant, _
                    Optional ByVal vMaterialQA As Variant, Optional ByVal vUnidadNegQA As Variant, Optional ByVal vUnidadNegQADen As Variant, Optional ByVal dValorDef As Double) As CVariableCalidad
                            
    Dim objnewmember As CVariableCalidad
    
    Set objnewmember = New CVariableCalidad
    
    Set objnewmember.Conexion = m_oConexion
    
    objnewmember.ID = ID
    objnewmember.Nivel = Nivel
    objnewmember.Cod = Cod
    objnewmember.Den = Den
    objnewmember.Modificado = Modificado
    
    If IsMissing(Tipo) Then
        objnewmember.Tipo = Null
    Else
        objnewmember.Tipo = Tipo
    End If
    
    If IsMissing(Subtipo) Or IsNull(Subtipo) Then
        objnewmember.Subtipo = Null
    Else
        objnewmember.Subtipo = Subtipo
    End If
    
    If IsMissing(Formula) Then
        objnewmember.Formula = Null
    Else
        objnewmember.Formula = Formula
    End If
    
    If IsMissing(Origen) Then
        objnewmember.Origen = Null
    Else
        objnewmember.Origen = Origen
    End If
    
    If IsMissing(OrigenCod) Then
        objnewmember.OrigenCod = Null
    Else
        objnewmember.OrigenCod = OrigenCod
    End If
    
    If IsMissing(FormCod) Then
        objnewmember.FormularioCod = Null
    Else
        objnewmember.FormularioCod = FormCod
    End If
    
    If IsMissing(FormID) Then
        objnewmember.FormularioID = Null
    Else
        objnewmember.FormularioID = FormID
    End If
    
    If IsMissing(vMaterialQA) Then
        objnewmember.MaterialQA = Null
    Else
        objnewmember.MaterialQA = vMaterialQA
    End If
    
    If IsMissing(vUnidadNegQA) Then
        objnewmember.UnidadNegQA = Null
    Else
        objnewmember.UnidadNegQA = vUnidadNegQA
    End If
    
    If IsMissing(vUnidadNegQADen) Then
        objnewmember.UnidadNegQADen = Null
    Else
        objnewmember.UnidadNegQADen = vUnidadNegQADen
    End If
    
    If IsMissing(dValorDef) Then
        objnewmember.ValorDefecto = 0
    Else
        objnewmember.ValorDefecto = dValorDef
    End If
    
    If IsMissing(vIDCal1) Then
        objnewmember.IdVarCal1 = Null
    Else
        objnewmember.IdVarCal1 = vIDCal1
    End If
    
    If IsMissing(vIDCal2) Then
        objnewmember.IdVarCal2 = Null
    Else
        objnewmember.IdVarCal2 = vIDCal2
    End If
    
    If IsMissing(vIDCal3) Then
        objnewmember.IdVarCal3 = Null
    Else
        objnewmember.IdVarCal3 = vIDCal3
    End If
    
    If IsMissing(vIDCal4) Then
        objnewmember.IdVarCal4 = Null
    Else
        objnewmember.IdVarCal4 = vIDCal4
    End If
    
    If IsMissing(vConsulta) Or IsNull(vConsulta) Then
        objnewmember.UsuConsultar = False
    Else
        objnewmember.UsuConsultar = vConsulta
    End If
    If IsMissing(vModificacion) Or IsNull(vModificacion) Then
        objnewmember.UsuModificar = False
    Else
        objnewmember.UsuModificar = vModificacion
    End If
    If IsMissing(vTipoPond) Then
        objnewmember.TipoPonderacion = TVariablesCalPonderacion.PondNoPonderacion
    Else
        objnewmember.TipoPonderacion = vTipoPond
    End If
    
    If IsMissing(vIDFormula) Then
        objnewmember.IDFormula = Null
    Else
        objnewmember.IDFormula = vIDFormula
    End If
        
    If IsMissing(vPeriodo) Then
        objnewmember.NCPeriodo = Null
    Else
        objnewmember.NCPeriodo = vPeriodo
    End If
    
    If IsMissing(FechaActual) Then
        objnewmember.FecAct = Null
    Else
        objnewmember.FecAct = FechaActual
    End If

    If IsMissing(vPubPortal) Or IsNull(vPubPortal) Then
        objnewmember.PublicarPortal = False
    Else
        objnewmember.PublicarPortal = vPubPortal
    End If
    
    If IsMissing(vPuntuacion) Then
        objnewmember.Puntuacion = Null
    Else
        objnewmember.Puntuacion = vPuntuacion
    End If
    If IsMissing(vCalificacion) Then
        objnewmember.CalificacionID = Null
    Else
        objnewmember.CalificacionID = vCalificacion
    End If
    If IsMissing(vPuntFecha) Then
        objnewmember.PuntFecha = Null
    Else
        objnewmember.PuntFecha = vPuntFecha
    End If
    If IsMissing(vProveMat) Then
        objnewmember.ProveMat = Null
    Else
        objnewmember.ProveMat = vProveMat
    End If
    
    If Not IsMissing(vIndice) And Not IsNull(vIndice) Then
        objnewmember.Indice = vIndice
        mCol.Add objnewmember, CStr(vIndice)
    Else
        mCol.Add objnewmember, CStr(Nivel) & CStr(ID)
    End If

    Set Add = objnewmember
    
    Set objnewmember = Nothing

End Function

Public Sub CargarVariablesProveedor(ByVal sCodProve As String)
Dim rs1 As ADODB.Recordset
Dim rs As ADODB.Recordset
Dim adocom As ADODB.Command
Dim oParam As ADODB.Parameter
Dim sConsulta As String
Dim oVarCal As CVariableCalidad
Dim bNext As Boolean
    
    If m_oConexion Is Nothing Then
        Err.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CVariablesCalidad.CargarVariablesNivel1", "No se ha establecido la conexion"
        Exit Sub
    End If
    
    Set adocom = New ADODB.Command
    Set adocom.ActiveConnection = m_oConexion.ADOCon
    
    adocom.CommandType = adCmdStoredProc
    adocom.CommandText = "FSQA_GET_VARIABLES_PROVE"
    
    Set oParam = adocom.CreateParameter("PROVE", adVarChar, adParamInput, 50, sCodProve)
    adocom.Parameters.Append oParam
    Set oParam = adocom.CreateParameter("USU", adVarChar, adParamInput, 50, Null)
    adocom.Parameters.Append oParam
    Set oParam = adocom.CreateParameter("IDI", adVarChar, adParamInput, 20, "SPA")
    adocom.Parameters.Append oParam
    
    
    ''' Ejecutar la SP
    Set rs1 = adocom.Execute
    
    If rs1.eof Then
        rs1.Close
        Set rs1 = Nothing
        Set mCol = Nothing
        Set mCol = New Collection
        Exit Sub
    Else
        m_vFormula = rs1("FORMULA").Value
        m_vIDFormula = rs1("ID_FORMULA").Value
        m_vPuntuacion = rs1("PUNT").Value
        m_vCalificacionID = rs1("CALID").Value
        m_vPuntFecha = rs1("FECHA").Value
    End If
    
    Set rs = rs1.NextRecordset
    If rs.eof Then
        rs.Close
        Set rs = Nothing
        Set mCol = Nothing
        Set mCol = New Collection
        Exit Sub
        
    Else
        Set mCol = Nothing
        Set mCol = New Collection
        
        While Not rs.eof
            Me.Add rs.Fields("ID").Value, 1, rs.Fields("COD").Value, rs.Fields("DEN").Value, , , rs.Fields("FORMULA").Value, , , , , , , , , , , , , , rs.Fields("ID_FORMULA").Value, , , , rs.Fields("PUNT").Value, rs.Fields("CALID").Value, rs.Fields("FECHA").Value, rs.Fields("MAT").Value, , , , rs.Fields("VALOR_DEF").Value
            rs.MoveNext
        Wend
    End If
    rs.Close
    
    Set rs = rs1.NextRecordset
    While Not rs.eof
        Set oVarCal = Me.Item(CStr(1) & CStr(rs.Fields("ID_VAR_CAL1").Value))
        If oVarCal.VariblesCal Is Nothing Then
            Set oVarCal.VariblesCal = New CVariablesCalidad
            Set oVarCal.VariblesCal.Conexion = m_oConexion
        End If
        oVarCal.VariblesCal.Add rs.Fields("ID").Value, 2, rs.Fields("COD").Value, rs.Fields("DEN").Value, rs.Fields("TIPO").Value, rs.Fields("SUBTIPO").Value, rs.Fields("FORMULA").Value, rs.Fields("VALOR_INT").Value, , , rs.Fields("ID_VAR_CAL1").Value, , , , , , , , , , rs.Fields("ID_FORMULA").Value, rs.Fields("TIPOPOND").Value, , , rs.Fields("PUNT").Value, rs.Fields("CALID").Value, rs.Fields("FECHA").Value, rs.Fields("MAT").Value, rs.Fields("MATERIAL_QA").Value, rs.Fields("UNIDADNEG_QA").Value, , rs.Fields("VALOR_DEF").Value
        rs.MoveNext
    Wend
    rs.Close
    
    Set rs = rs1.NextRecordset
    While Not rs.eof
        Set oVarCal = Me.Item(CStr(1) & CStr(rs.Fields("ID_VAR_CAL1").Value)).VariblesCal.Item(CStr(2) & CStr(rs.Fields("ID_VAR_CAL2").Value))
        If oVarCal.VariblesCal Is Nothing Then
            Set oVarCal.VariblesCal = New CVariablesCalidad
            Set oVarCal.VariblesCal.Conexion = m_oConexion
        End If
        oVarCal.VariblesCal.Add rs.Fields("ID").Value, 3, rs.Fields("COD").Value, rs.Fields("DEN").Value, rs.Fields("TIPO").Value, , rs.Fields("FORMULA").Value, rs.Fields("VALOR_INT").Value, , , rs.Fields("ID_VAR_CAL1").Value, rs.Fields("ID_VAR_CAL2").Value, , , , , , , , , rs.Fields("ID_FORMULA").Value, rs.Fields("TIPOPOND").Value, , , rs.Fields("PUNT").Value, rs.Fields("CALID").Value, rs.Fields("FECHA").Value, rs.Fields("MAT").Value, rs.Fields("MATERIAL_QA").Value, rs.Fields("UNIDADNEG_QA").Value, , rs.Fields("VALOR_DEF").Value
        rs.MoveNext
    Wend
    
    Set rs = rs1.NextRecordset
    While Not rs.eof
        Set oVarCal = Me.Item(CStr(1) & CStr(rs.Fields("ID_VAR_CAL1").Value)).VariblesCal.Item(CStr(2) & CStr(rs.Fields("ID_VAR_CAL2").Value)).VariblesCal.Item(CStr(3) & CStr(rs.Fields("ID_VAR_CAL3").Value))
        If oVarCal.VariblesCal Is Nothing Then
            Set oVarCal.VariblesCal = New CVariablesCalidad
            Set oVarCal.VariblesCal.Conexion = m_oConexion
        End If
        oVarCal.VariblesCal.Add rs.Fields("ID").Value, 4, rs.Fields("COD").Value, rs.Fields("DEN").Value, rs.Fields("TIPO").Value, , rs.Fields("FORMULA").Value, rs.Fields("VALOR_INT").Value, , , rs.Fields("ID_VAR_CAL1").Value, rs.Fields("ID_VAR_CAL2").Value, rs.Fields("ID_VAR_CAL3").Value, , , , , , , , rs.Fields("ID_FORMULA").Value, rs.Fields("TIPOPOND").Value, , , rs.Fields("PUNT").Value, rs.Fields("CALID").Value, rs.Fields("FECHA").Value, rs.Fields("MAT").Value, rs.Fields("MATERIAL_QA").Value, rs.Fields("UNIDADNEG_QA").Value, , rs.Fields("VALOR_DEF").Value
        rs.MoveNext
    Wend
    
    Set rs = rs1.NextRecordset
    While Not rs.eof
        Set oVarCal = Me.Item(CStr(1) & CStr(rs.Fields("ID_VAR_CAL1").Value)).VariblesCal.Item(CStr(2) & CStr(rs.Fields("ID_VAR_CAL2").Value)).VariblesCal.Item(CStr(3) & CStr(rs.Fields("ID_VAR_CAL3").Value)).VariblesCal.Item(CStr(4) & CStr(rs.Fields("ID_VAR_CAL4").Value))
        If oVarCal.VariblesCal Is Nothing Then
            Set oVarCal.VariblesCal = New CVariablesCalidad
            Set oVarCal.VariblesCal.Conexion = m_oConexion
        End If
        oVarCal.VariblesCal.Add rs.Fields("ID").Value, 5, rs.Fields("COD").Value, rs.Fields("DEN").Value, , rs.Fields("SUBTIPO").Value, rs.Fields("FORMULA").Value, rs.Fields("VALOR_INT").Value, , , rs.Fields("ID_VAR_CAL1").Value, rs.Fields("ID_VAR_CAL2").Value, rs.Fields("ID_VAR_CAL3").Value, rs.Fields("ID_VAR_CAL4").Value, , , , , , , rs.Fields("ID_FORMULA").Value, rs.Fields("TIPOPOND").Value, , , rs.Fields("PUNT").Value, rs.Fields("CALID").Value, rs.Fields("FECHA").Value, rs.Fields("MAT").Value, rs.Fields("MATERIAL_QA").Value, rs.Fields("UNIDADNEG_QA").Value, , rs.Fields("VALOR_DEF").Value
        rs.MoveNext
    Wend
    
    rs.Close
    Set rs = Nothing
    rs1.Close
    Set rs1 = Nothing
    Set adocom = Nothing
    Set oParam = Nothing
    
End Sub

