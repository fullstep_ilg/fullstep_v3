Attribute VB_Name = "basParametros"
' CONSTANTES PARA EL LOG DE CAMBIOS E INTEGRACION
Public Const Accion_Alta            As String = "I"
Public Const Accion_Baja            As String = "D"
Public Const Accion_Modificacion    As String = "U"
Public Const Accion_CambioCodigo    As String = "C"
Public Const Accion_Reenvio         As String = "R"
Public Const Accion_CambioEstado    As String = "E"
Public Const Accion_Homologacion    As String = "H" 'Esta acci�n es exclusiva para PROVEEDORES, en la adjudicacion de procesos
Public Const Accion_CambioNivel4    As String = "4" 'Esta acci�n es exclusiva para ARTICULOS, implica una reubicaci�n del art�culo
