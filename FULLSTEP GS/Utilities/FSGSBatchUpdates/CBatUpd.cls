VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CBatUpd"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Const ClaveUSUpar = "agkag�"
Private Const ClaveUSUimp = "h+hlL_"

Public Enum TVariablesCalPonderacion
    PondNCNumero = 0
    PondNCMediaPesos = 1
    PondNCSumaPesos = 2
    PondCertificado = 3
    PondIntEscContinua = 4
    PondIntFormula = 5
    PondManual = 6
    PondNoPonderacion = 99
End Enum
Public Enum TAtributoPonderacion
    SinPonderacion = 0
    EscalaContinua = 1
    EscalaDiscreta = 2
    Formula = 3
    Manual = 4
    SiNo = 5
    PorCaducidad = 6
    Automatico = 7
End Enum
Public Enum TiposDeAtributos
    TipoString = 1
    TipoNumerico = 2
    TipoFecha = 3
    TipoBoolean = 4
    TipoTextoCorto = 5
    TipoTextoMedio = 6
    TipoTextoLargo = 7
    TipoArchivo = 8
    TipoDesglose = 9
End Enum

Public Type TipoErrorSummit
    NumError As Long
    Arg1 As Variant
    Arg2 As Variant
End Type

Public Sub LimpiarReunionesVirtuales(ByVal sInstancia As String)
'******************************************************************************************
'*** Descripci�n: Procedimiento que cierra las reuniones virtuales que puedan haberse   ***
'***              quedado activas tras finalizar.  O desconecta los asistentes que      ***
'***              no han cerrado la conexi�n                                            ***
'***                                                                                    ***
'*** Par�metros : Recibe como par�metro de entrada la instancia de tipo string, que     ***
'***              se utiliza para realizar la conexi�n con la base de datos.            ***
'***                                                                                    ***
'*** Valor que devuelve: ------                                                         ***
'******************************************************************************************
    Dim sConsulta As String

    On Error GoTo Error
        If Conectar(sInstancia) Then
            If AccesoGS Then
                'Desconectamos las reuniones (y sus asistentes) que llevan m�s de un minuto sin actualizar la fecha de control
                'Guardamos en el  historico de asistentes
                sConsulta = "INSERT INTO REU_VIR_ASIST_HIST (ID_REUVIR,REU,ANYO,GMN1,PROCE,USU,FEC_CON,FEC_DIS)"
                sConsulta = sConsulta & " SELECT RVH.ID,R.REU,RA.ANYO,RA.GMN1,RA.PROCE,RA.USU,RA.FECHA_CON," & DateToSQLTimeDate(Now)
                sConsulta = sConsulta & " FROM REU_VIR_ASIST RA WITH (NOLOCK) INNER JOIN REU_VIR R WITH (NOLOCK) ON R.REU=RA.REU INNER JOIN REU_VIR_HIST RVH WITH (NOLOCK) ON RVH.REU=R.REU AND RVH.FEC_INI=R.FEC_INI"
                sConsulta = sConsulta & " WHERE RA.REU IN (SELECT REU FROM REU_VIR WITH (NOLOCK) WHERE REU_VIR.CONECT=1 AND DATEDIFF(second, REU_VIR.FECCON, GETDATE())>60)"
                g_adoconnCon.Execute sConsulta
        
                sConsulta = "DELETE REU_VIR_ASIST WHERE REU IN (SELECT REU FROM REU_VIR WITH (NOLOCK) WHERE CONECT=1 AND DATEDIFF(second, FECCON, GETDATE())>60)"
                g_adoconnCon.Execute sConsulta
                
                sConsulta = "UPDATE REU_VIR SET CONECT=0 WHERE CONECT=1 AND DATEDIFF(second, FECCON, GETDATE())>60"
                g_adoconnCon.Execute sConsulta
                
                'Desconectamos los asistentes que llevan m�s de un minuto sin actualizar la fecha de control
                'Guardamos en el  historico de asistentes
                sConsulta = "INSERT INTO REU_VIR_ASIST_HIST (ID_REUVIR,REU,ANYO,GMN1,PROCE,USU,FEC_CON,FEC_DIS) "
                sConsulta = sConsulta & " SELECT RVH.ID,R.REU,RA.ANYO,RA.GMN1,RA.PROCE,RA.USU,RA.FECHA_CON," & DateToSQLTimeDate(Now)
                sConsulta = sConsulta & " FROM REU_VIR_ASIST RA WITH (NOLOCK) INNER JOIN REU_VIR R WITH (NOLOCK) ON R.REU=RA.REU INNER JOIN REU_VIR_HIST RVH WITH (NOLOCK) ON RVH.REU=R.REU AND RVH.FEC_INI=R.FEC_INI"
                sConsulta = sConsulta & " WHERE DATEDIFF(second, RA.FECCON, GETDATE())>60"
                g_adoconnCon.Execute sConsulta
                
                sConsulta = "DELETE REU_VIR_ASIST WHERE DATEDIFF(second, FECCON, GETDATE())>60"
                g_adoconnCon.Execute sConsulta
            End If
        Desconectar
    End If
    Exit Sub
Error:
    Desconectar
End Sub

'<summary>Llama a funci�n de revisi�n de subastas</summary>
'<param name="sInstancia">Necesario para realizar la conexi�n</param>
'<remarks>Llamada desde: , Tiempo m�ximo: 0,3 seg</remarks>

Public Sub RevisarSubastas(ByVal sInstancia As String)
    Dim lRevSub As Long
    
    On Error GoTo Error
        If Conectar(sInstancia) Then
            If AccesoGS() Then
                'Llamada a la funci�n de revisi�n de subastas pausadas. Se ejecuta cada 5 seg.
                lRevSub = RevisarSubastasPausadas
            End If
            
            Desconectar
        End If
    Exit Sub
Error:
    Desconectar
End Sub

Public Sub ActualizarCodigos(ByVal sInstancia As String)
'******************************************************************************************
'*** Descripci�n: Procedimiento que lanza la actualizaci�n de c�digos                   ***
'***                                                                                    ***
'*** Par�metros : Recibe como par�metro de entrada la instancia de tipo string, que     ***
'***              se utiliza para realizar la conexi�n con la base de datos.            ***
'***                                                                                    ***
'*** Valor que devuelve: ------                                                         ***
'******************************************************************************************
    If Conectar(sInstancia) Then
        DevolverLongitudesDeCodigos
        'ActualizarProve
        ActualizarBBDD
        Desconectar
        ActualizarDenominacionesPM (sInstancia)
    Else
        basUtilidades.GrabarError "CBatUpd.ActualizarCodigos ", "Error", "Error CBatUpd.ActualizarCodigos no se pudo conectar a la base de datos. Instancia:" & sInstancia
    End If
End Sub

Private Function Conectar(ByVal sInstancia As String) As Boolean
    Dim sServidor As String
    Dim sBaseDeDatos As String

    On Error GoTo Error
    Set g_adoconnCon = New ADODB.Connection
    gInstancia = sInstancia
    sServidor = GetStringSetting("FSGSNotifierService", gInstancia, "Conexion", "Servidor")
    sBaseDeDatos = GetStringSetting("FSGSNotifierService", gInstancia, "Conexion", "BaseDeDatos")
    g_adoconnCon.Open "Provider=SQLOLEDB.1;Auto Translate=FALSE;Server=" & sServidor & ";Database=" & sBaseDeDatos & ";", LICDBLogin, LICDBContra
    g_adoconnCon.CursorLocation = adUseClient
    g_adoconnCon.CommandTimeout = 120
    Conectar = True
    Exit Function
Error:
    Desconectar
    Conectar = False
End Function

Private Sub Desconectar()
    If Not g_adoconnCon Is Nothing Then
        If g_adoconnCon.State = adStateOpen Then
            g_adoconnCon.Close
        End If
        Set g_adoconnCon = Nothing
    End If
End Sub

''' <summary>Indica si hay acceso a GS </summary>'
''' <returns>Boolean indicando si hay acceso a GS</returns>
''' <remarks>Llamada desde=; Tiempo m�ximo=0,61seg.</remarks>

Private Function AccesoGS() As Boolean
    Dim sConsulta As String
    Dim adoRes As Recordset
    Dim adoComm As ADODB.Command
    Dim adoParam As ADODB.Parameter
    
    AccesoGS = False
    
    Set adoComm = New ADODB.Command
    With adoComm
        sConsulta = "SELECT ACCESO_FSGS FROM PARGEN_INTERNO WITH (NOLOCK) WHERE ID=1"
        
        .ActiveConnection = g_adoconnCon
        .CommandType = adCmdText
        .CommandText = sConsulta
        .CommandTimeout = 120
        
        Set adoRes = .Execute
    End With
    
    ' El contenido del par�metro ACCESO_FSGS se encripta mediante la funci�n EncriptarAccesoFSWS
    'gParametrosGenerales.gbAccesoFSGS = SQLBinaryToBoolean(AdoRes("ACCESO_FSGS").Value)
    If EncriptarAccesoFSWS("GS", NullToStr(adoRes("ACCESO_FSGS").Value), False) = "FULLSTEP GS" Then AccesoGS = True
    
    Set adoRes = Nothing
    Set adoComm = Nothing
    Set adoParam = Nothing
End Function

''' <summary>Encripta/desencripta un dato</summary>'
''' <param name="sCadena">cadena para encriptar/desencriptar</param>
''' <param name="sDato">Dato a encriptar/desencriptar</param>
''' <param name="Encriptando">Indica encriptaci�n � desencriptaci�n</param>
''' <returns>dato encriptado/desencriptado</returns>
''' <remarks>Llamada desde=; Tiempo m�ximo=0,61seg.</remarks>

Private Function EncriptarAccesoFSWS(ByVal sCadena As String, ByVal sDato As String, ByVal Encriptando As Boolean) As String
    
    Dim oCrypt2 As cCrypt2
        
    Set oCrypt2 = New cCrypt2
    
    oCrypt2.InBuffer = sDato
    If Len(sCadena) Mod 2 = 0 Then
        oCrypt2.Codigo = ClaveUSUpar
    Else
        oCrypt2.Codigo = ClaveUSUimp
    End If
    oCrypt2.Fecha = DateSerial(1974, 3, 5)
    
    If Encriptando Then
        oCrypt2.Encrypt
    Else
        oCrypt2.Decrypt
    End If
            
    EncriptarAccesoFSWS = oCrypt2.OutBuffer
    
    Set oCrypt2 = Nothing
End Function

Private Sub ActualizarDenominacionesPM(ByVal sInstancia As String)
    '******************************************************************************************
    '*** Descripci�n: Procedimiento que lanza la actualizaci�n de c�digos                   ***
    '***              Tambi�n realiza las actualizaciones en las tablas din�micas de filtros***
    '***                                                                                    ***
    '*** Par�metros : Recibe como par�metro de entrada la instancia de tipo string, que     ***
    '***              se utiliza para realizar la conexi�n con la base de datos.            ***
    '***                                                                                    ***
    '*** Valor que devuelve: ------                                                         ***
    '******************************************************************************************
    Dim oDenominaciones As cDenominacionesPM
    Dim oVisores As cVisoresPM
    Dim oVisoresQA As cVisoresQA
    Dim oVisoresContr As CVisoresContr
    Dim oFSGSRaiz As CRaiz
    Dim res As Integer
    Dim sServidor As String
    Dim sBaseDeDatos As String

    If Conectar(sInstancia) Then
        'Creamos el objeto raiz
        Set oFSGSRaiz = New CRaiz
        
        sServidor = GetStringSetting("FSGSNotifierService", sInstancia, "Conexion", "Servidor")
        sBaseDeDatos = GetStringSetting("FSGSNotifierService", sInstancia, "Conexion", "BaseDeDatos")
        
        'Nos conectamos a la BD
        res = oFSGSRaiz.Conectar(sInstancia, sServidor, sBaseDeDatos)
        
        DevolverLongitudesDeCodigos
        
        Set oDenominaciones = oFSGSRaiz.Generar_CDenominacionesPM
        oDenominaciones.ActualizarDenominaciones
        Set oDenominaciones = Nothing
        
        Set oVisores = oFSGSRaiz.Generar_CVisoresPM
        oVisores.ActualizarVisoresPM
        Set oVisores = Nothing
        
        Set oVisoresQA = oFSGSRaiz.Generar_CVisoresQA
        oVisoresQA.ActualizarVisoresQA
        Set oVisoresQA = Nothing
        
        Set oVisoresContr = oFSGSRaiz.Generar_CVisoresContr
        oVisoresContr.ActualizarVisoresContr
        Set oVisoresContr = Nothing
        
        oFSGSRaiz.Desconectar
        
        Desconectar
    End If
End Sub
