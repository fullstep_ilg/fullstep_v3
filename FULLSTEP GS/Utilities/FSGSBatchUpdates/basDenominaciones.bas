Attribute VB_Name = "basDenominaciones"
Option Explicit

Public Enum TipoCampoSistema
    Proveedor = 100
    FormaPago = 101
    Moneda = 102
    Material = 103
    Unidad = 105
    Pais = 107
    Provincia = 108
    Destino = 109
    PresupuestoTipo1 = 110
    PresupuestoTipo2 = 111
    PresupuestoTipo3 = 112
    PresupuestoTipo4 = 113
    Persona = 115
    Articulo = 119
    UnidadOrganizativa = 121
    Departamento = 122
    OrganizacionCompras = 123
    Centro = 124
    Almacen = 125
    CentroCoste = 129
    Partida = 130
    Activo = 131
    TipoPedido = 132
    UnidadPedido = 142
    Comprador = 144
End Enum

Private Function ExtraerDescrIdioma(ByVal Text_Den As String, ByVal Idioma As String, Optional ByVal SepTextos As String = "#", Optional ByVal SepIdioma As String = "|") As String
'****************************************************************************************************************
'*** Autor: sra(20/03/2009)                                                                                 *****
'*** Descripci�n: Extrae la descripci�n del idioma pasado como par�metro que se encuentra en una cadena.    *****
'*** Par�metros de entrada: Text_Den: Cadena con formato <idi1>|<descr1>#<idi2>|<descr2>#...                *****
'***                        Idioma: Idioma en el que hay que extraer la descripci�n                         *****
'***                        SepTextos: Opcional. Por defecto #. Separador de las descripciones              *****
'***                        SepIdioma: Opcional. Por defecto |. Separador del idioma y de su descripci�n    *****
'*** Par�metros de salida: La descripci�n del idioma pasado como par�metro                                  *****
'*** Llamada desde: ActualizarDenominacionEnFiltros                                                         *****
'****************************************************************************************************************
    Dim sTexto As String
    Dim arrTextos() As String
    Dim I As Integer
    I = 0
    Do While InStr(1, Text_Den, SepTextos) > 0
        ReDim Preserve arrTextos(I + 1)
        arrTextos(I + 1) = Left(Text_Den, InStr(1, Text_Den, SepTextos) - 1)
        Text_Den = Right(Text_Den, Len(Text_Den) - InStr(1, Text_Den, SepTextos))
        I = I + 1
    Loop
    If Len(Text_Den) > 0 Then
        ReDim Preserve arrTextos(I + 1)
        arrTextos(I + 1) = Text_Den

    
        If UBound(arrTextos) > 1 Then
            For I = 1 To UBound(arrTextos)
                If UCase(Left(arrTextos(I), InStr(1, arrTextos(I), SepIdioma) - 1)) = Idioma Then
                    ExtraerDescrIdioma = Right(arrTextos(I), Len(arrTextos(I)) - InStr(1, arrTextos(I), SepIdioma))
                End If
            Next
        Else
            ExtraerDescrIdioma = Right(arrTextos(1), Len(arrTextos(1)) - InStr(1, arrTextos(1), SepIdioma))
        End If
    Else
        ExtraerDescrIdioma = Text_Den
    End If
End Function

Private Function DevolverMaterialConCodigoUltimoNivel(ByVal sValorText As String, ByVal sValorTextDen As String) As String
'********************************************************************************************************************
'*** Autor: sra(20/03/2009)                                                                                     *****
'*** Descripci�n: Devuelve la denominaci�n del material con el c�digo del �ltimo nivel. El n�mero de niveles    *****
'***                puede variar y su longitud tb. �sta se obtiene de lo definido en la tabla DIC.              *****
'*** Par�metros de entrada: sValorText: c�digos de la familia de materiales concatenados.                       *****
'***                        sValorTextDen: la descripci�n del �ltimo nivel de material.                         *****
'*** Par�metros de salida: El material en el formato: cod. �ltimo nivel + denominaci�n                          *****
'*** Llamada desde: ActualizarDenominacionEnFiltros                                                                                       *****
'********************************************************************************************************************
    Dim sMat As String
    Dim arrMat(4) As String
    Dim lLongCod As Integer
    Dim I As Integer

    sMat = NullToStr(sValorText)
           
    For I = 1 To 4
        arrMat(I) = ""
    Next I

    I = 1
    Do While Trim(sMat) <> ""
        Select Case I
            Case 1
                lLongCod = gLongitudesDeCodigos.giLongCodGMN1
            Case 2
                lLongCod = gLongitudesDeCodigos.giLongCodGMN2
            Case 3
                lLongCod = gLongitudesDeCodigos.giLongCodGMN3
            Case 4
                lLongCod = gLongitudesDeCodigos.giLongCodGMN4
        End Select
        arrMat(I) = Trim(Mid(sMat, 1, lLongCod))
        sMat = Mid(sMat, lLongCod + 1)
        I = I + 1
    Loop
            
    If arrMat(4) <> "" Then
        DevolverMaterialConCodigoUltimoNivel = arrMat(4) + " - " + sValorTextDen
    ElseIf arrMat(3) <> "" Then
        DevolverMaterialConCodigoUltimoNivel = arrMat(3) + " - " + sValorTextDen
    ElseIf arrMat(2) <> "" Then
        DevolverMaterialConCodigoUltimoNivel = arrMat(2) + " - " + sValorTextDen
    ElseIf arrMat(1) <> "" Then
        DevolverMaterialConCodigoUltimoNivel = arrMat(1) + " - " + sValorTextDen
    End If
End Function

Private Function DevolverPresupuestosConPorcentajes(ByVal sValorText As String, ByVal sValorTextDen As String) As String
'********************************************************************************************************************
'*** Autor: sra(20/03/2009)                                                                                     *****
'*** Descripci�n: Devuelve el presupuesto en el formato: Ej 2005 - PC-002  (50.00%); 2005 - PC-001 (50.00%);    *****
'*** Par�metros de entrada: sValorText: c�digo del/los presupuesto/s con el/los porcentaje/s                    *****
'***                        sValorTextDen: la descripci�n de los presupuestos sin el porcentaje.                *****
'*** Par�metros de salida: El/Los presupuestos correctamente formateado/s                                       *****
'*** Llamada desde: ActualizarDenominacionEnFiltros                                                                                       *****
'********************************************************************************************************************
    Dim arrPresupuestos() As String
    Dim arrDenominaciones() As String
    Dim oPresup As String
    Dim iContadorPres As Integer
    Dim arrPresup(2) As String
    Dim dPorcent As Double
    Dim sValor As String
            
    iContadorPres = 0
    Do While InStr(1, sValorText, "#") > 0
        ReDim Preserve arrPresupuestos(iContadorPres + 1)
        arrPresupuestos(iContadorPres + 1) = Left(sValorText, InStr(1, sValorText, "#") - 1)
        sValorText = Right(sValorText, Len(sValorText) - InStr(1, sValorText, "#"))
        iContadorPres = iContadorPres + 1
    Loop
    If Len(sValorText) > 0 Then
        ReDim Preserve arrPresupuestos(iContadorPres + 1)
        arrPresupuestos(iContadorPres + 1) = sValorText
    End If
    
    iContadorPres = 0
    Do While InStr(1, sValorTextDen, "#") > 0
        ReDim Preserve arrDenominaciones(iContadorPres + 1)
        arrDenominaciones(iContadorPres + 1) = Left(sValorTextDen, InStr(1, sValorTextDen, "#") - 1)
        sValorTextDen = Right(sValorTextDen, Len(sValorTextDen) - InStr(1, sValorTextDen, "#"))
        iContadorPres = iContadorPres + 1
    Loop
    If Len(sValorTextDen) > 0 Then
        ReDim Preserve arrDenominaciones(iContadorPres + 1)
        arrDenominaciones(iContadorPres + 1) = sValorTextDen
    End If
                
    iContadorPres = 0
    For iContadorPres = 1 To UBound(arrPresupuestos)
        Do While InStr(1, arrPresupuestos(iContadorPres), "_") > 0
            arrPresupuestos(iContadorPres) = Right(arrPresupuestos(iContadorPres), Len(arrPresupuestos(iContadorPres)) - InStr(1, arrPresupuestos(iContadorPres), "_"))
        Loop
        'Los presupuestos siempre llevan el porcentaje como 0.xx salvo cuando es 100% que llevan 1
        dPorcent = CDbl(arrPresupuestos(iContadorPres)) * 100
        sValor = arrDenominaciones(iContadorPres) & " (" & CStr(dPorcent) & ".00%); " & sValor
    Next iContadorPres
    
    DevolverPresupuestosConPorcentajes = sValor
End Function

Public Function ObtenerDenominacionParaFiltro(ByVal sIdioma As String, ByVal nTipoCampoGS As Integer, ByVal sCodigo As String, ByVal sNuevaDenominacion As String) As String
'********************************************************************************************************************
'*** Autor: sra(15/02/2009)                                                                                     *****
'*** Descripci�n: Obtiene la denominaci�n formateada para insertar en la tabla de filtro correspondiente.       *****
'*** Par�metros de entrada: sIdioma: idioma en el que hay que extraer la denominaci�n.                          *****
'***                        nTipoCampoGS: el tipo de campo GS que estamos tratando                              *****
'***                        sCodigo: el c�digo del campo del que vamos a obtener la denominaci�n.               *****
'***                        sNuevaDenominacion: la denominaci�n (ej: con todos los idiomas concatenados).       *****
'*** Par�metros de salida: La denominaci�n correctamente formateada                                             *****
'*** Llamada desde: ActualizarDenominacionEnFiltros (cDenominacionesPM.cls) y ActualizarVisorPM (cVisoresPM.cls)*****
'********************************************************************************************************************

    Dim sDenominacionAActualizar As String
    
    sDenominacionAActualizar = ""
    If nTipoCampoGS = 0 Then 'se trata de una tabla externa
        sDenominacionAActualizar = sNuevaDenominacion   'no se hace nada con la denominaci�n.
    Else
        Select Case nTipoCampoGS
            Case TipoCampoSistema.Material
                'FORMATO MATERIALES: cod. ultimo nivel (VALOR_TEXT) +  <Idioma>|<Texto>#<Idioma>|<Texto>
                sDenominacionAActualizar = DevolverMaterialConCodigoUltimoNivel(sCodigo, ExtraerDescrIdioma(sNuevaDenominacion, sIdioma))
            Case TipoCampoSistema.FormaPago, TipoCampoSistema.Moneda, TipoCampoSistema.Unidad, TipoCampoSistema.Destino, TipoCampoSistema.CentroCoste, TipoCampoSistema.Partida, TipoCampoSistema.TipoPedido
                'FORMATO: <Idioma>|<Texto>#<Idioma>|<Texto>
                sDenominacionAActualizar = ExtraerDescrIdioma(sNuevaDenominacion, sIdioma)
                If nTipoCampoGS = TipoCampoSistema.CentroCoste Or nTipoCampoGS = TipoCampoSistema.Partida Or nTipoCampoGS = TipoCampoSistema.TipoPedido Then
                    If InStrRev(sCodigo, "#") > 0 Then
                       sCodigo = Right(sCodigo, Len(sCodigo) - InStrRev(sCodigo, "#"))
                    End If
                    If InStrRev(sCodigo, "|") > 0 Then 'si no est� en el nivel 1, si no m�s abajo
                       sCodigo = Right(sCodigo, Len(sCodigo) - InStrRev(sCodigo, "|"))
                    End If
                    sDenominacionAActualizar = sCodigo & " - " & sDenominacionAActualizar
                End If
            Case TipoCampoSistema.Proveedor, TipoCampoSistema.Departamento, TipoCampoSistema.OrganizacionCompras, TipoCampoSistema.Centro
                'FORMATO: C�digo (VALOR_TEXT) - Denominaci�n (VALOR_TEXT_DEN)
                sDenominacionAActualizar = sCodigo & " - " & sNuevaDenominacion
            Case TipoCampoSistema.Persona, TipoCampoSistema.Almacen
                sDenominacionAActualizar = sNuevaDenominacion
            Case TipoCampoSistema.Articulo, TipoCampoSistema.UnidadOrganizativa
                sDenominacionAActualizar = sCodigo
            Case TipoCampoSistema.PresupuestoTipo1, TipoCampoSistema.PresupuestoTipo2, TipoCampoSistema.PresupuestoTipo3, TipoCampoSistema.PresupuestoTipo4
                'FORMATO: Ej 2005 - PC-002  (50.00%); 2005 - PC-001 (50.00%);
                sDenominacionAActualizar = DevolverPresupuestosConPorcentajes(sCodigo, sNuevaDenominacion)
            Case Else
                sDenominacionAActualizar = sNuevaDenominacion
        End Select
    End If
    ObtenerDenominacionParaFiltro = sDenominacionAActualizar
End Function

