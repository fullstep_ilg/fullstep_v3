VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CVariableCalidad"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
    
End Enum

Private m_oConexion As CConexion

Private m_lId As Long
Private m_iNivel As Integer
Private m_sCod As String
Private m_sDen As String

Private m_vFormula As Variant
Private m_vTipo As Variant
Private m_vSubTipo As Variant
Private m_vOrigen As Variant
Private m_vOrigenCod As Variant
Private m_vFormularioCod As Variant
Private m_vFormularioID As Variant

Private m_vIndice As Variant
Private m_vFecact As Variant

Private m_oVarsCal As CVariablesCalidad 'Variables contenidas (de nivel 2 o 3 o 4 o 5 segun el nivel de esta)

Private m_vIdVarCal1 As Variant
Private m_vIdVarCal2 As Variant
Private m_vIdVarCal3 As Variant
Private m_vIdVarCal4 As Variant

Private m_bModificado As Boolean

'Variables para el interfaz de material asignado
'Private mvarGruposMN1 As CGruposMatNivel1
'Private mvarGruposMN2 As CGruposMatNivel2
'Private mvarGruposMN3 As CGruposMatNivel3
'Private mvarGruposMN4 As CGruposMatNivel4

'Variables para la puntuación de certificados


Private m_udtPonderacion As TVariablesCalPonderacion 'Tipo de ponderación de la variable
Private m_vPeriodoNC As Variant

Private m_lIDFormula    As Variant 'ID a VAR_FORMULAS


'Variables de seguridad de usuarios
Private m_bConsultar As Boolean
Private m_bModificar As Boolean

'Variable para la publicación de puntuaciones en el portal
Private m_bPubPortal As Boolean
'Propiedad para la puntuación de los proveedores
Private m_vPuntuacion    As Variant
Private m_vCalificacionID As Variant
Private m_vPuntFecha    As Variant
Private m_vProveMaterial As Variant

'MaterialesQA
Private m_vMaterialQA As Variant
Private m_vUnidadNegQA As Variant
Private m_vUnidadNegQADen As Variant

'Valor Defecto
Private m_dValorDefecto As Double

Public Property Let NCPeriodo(ByVal dato As Variant)
    m_vPeriodoNC = dato
End Property
Public Property Get NCPeriodo() As Variant
    NCPeriodo = m_vPeriodoNC
End Property

Public Property Let IDFormula(ByVal dato As Variant)
    m_lIDFormula = dato
End Property
Public Property Get IDFormula() As Variant
    IDFormula = m_lIDFormula
End Property

Public Property Let Puntuacion(ByVal dato As Variant)
    m_vPuntuacion = dato
End Property
Public Property Get Puntuacion() As Variant
    Puntuacion = m_vPuntuacion
End Property
Public Property Let CalificacionID(ByVal dato As Variant)
    m_vCalificacionID = dato
End Property
Public Property Get CalificacionID() As Variant
    CalificacionID = m_vCalificacionID
End Property

Public Property Let PuntFecha(ByVal dato As Variant)
    m_vPuntFecha = dato
End Property
Public Property Get PuntFecha() As Variant
    PuntFecha = m_vPuntFecha
End Property
Public Property Let ProveMat(ByVal dato As Variant)
    m_vProveMaterial = dato
End Property
Public Property Get ProveMat() As Variant
    ProveMat = m_vProveMaterial
End Property

Public Property Let TipoPonderacion(ByVal dato As TVariablesCalPonderacion)
    m_udtPonderacion = dato
End Property
Public Property Get TipoPonderacion() As TVariablesCalPonderacion
    TipoPonderacion = m_udtPonderacion
End Property

Public Property Let UsuConsultar(ByVal dato As Boolean)
    m_bConsultar = dato
End Property
Public Property Get UsuConsultar() As Boolean
    UsuConsultar = m_bConsultar
End Property

Public Property Let UsuModificar(ByVal dato As Boolean)
    m_bModificar = dato
End Property
Public Property Get UsuModificar() As Boolean
    UsuModificar = m_bModificar
End Property

Public Property Let PublicarPortal(ByVal dato As Boolean)
    m_bPubPortal = dato
End Property
Public Property Get PublicarPortal() As Boolean
    PublicarPortal = m_bPubPortal
End Property


Public Property Let Modificado(ByVal dato As Boolean)
    m_bModificado = dato
End Property
Public Property Get Modificado() As Boolean
    Modificado = m_bModificado
End Property

Public Property Let IdVarCal1(ByVal dato As Variant)
    m_vIdVarCal1 = dato
End Property
Public Property Get IdVarCal1() As Variant
    IdVarCal1 = m_vIdVarCal1
End Property

Public Property Let IdVarCal2(ByVal dato As Variant)
    m_vIdVarCal2 = dato
End Property
Public Property Get IdVarCal2() As Variant
    IdVarCal2 = m_vIdVarCal2
End Property

Public Property Let IdVarCal3(ByVal dato As Variant)
    m_vIdVarCal3 = dato
End Property
Public Property Get IdVarCal3() As Variant
    IdVarCal3 = m_vIdVarCal3
End Property

Public Property Let IdVarCal4(ByVal dato As Variant)
    m_vIdVarCal4 = dato
End Property
Public Property Get IdVarCal4() As Variant
    IdVarCal4 = m_vIdVarCal4
End Property

Public Property Let ID(ByVal dato As Long)
    m_lId = dato
End Property
Public Property Get ID() As Long
    ID = m_lId
End Property

Public Property Let Nivel(ByVal dato As Integer)
    m_iNivel = dato
End Property
Public Property Get Nivel() As Integer
    Nivel = m_iNivel
End Property

Public Property Let Cod(ByVal dato As String)
    m_sCod = dato
End Property
Public Property Get Cod() As String
    Cod = m_sCod
End Property

Public Property Let Den(ByVal dato As String)
    m_sDen = dato
End Property
Public Property Get Den() As String
    Den = m_sDen
End Property

Public Property Let Formula(ByVal dato As Variant)
    m_vFormula = dato
End Property
Public Property Get Formula() As Variant
    Formula = m_vFormula
End Property

Public Property Let Tipo(ByVal dato As Variant)
    m_vTipo = dato
End Property
Public Property Get Tipo() As Variant
    Tipo = m_vTipo
End Property

Public Property Let Subtipo(ByVal dato As Variant)
    m_vSubTipo = dato
End Property
Public Property Get Subtipo() As Variant
    Subtipo = m_vSubTipo
End Property

Public Property Let Origen(ByVal dato As Variant)
    m_vOrigen = dato
End Property
Public Property Get Origen() As Variant
    Origen = m_vOrigen
End Property

Public Property Let OrigenCod(ByVal dato As Variant)
    m_vOrigenCod = dato
End Property
Public Property Get OrigenCod() As Variant
    OrigenCod = m_vOrigenCod
End Property
Public Property Let FormularioCod(ByVal dato As Variant)
    m_vFormularioCod = dato
End Property
Public Property Get FormularioCod() As Variant
    FormularioCod = m_vFormularioCod
End Property
Public Property Let FormularioID(ByVal dato As Variant)
    m_vFormularioID = dato
End Property
Public Property Get FormularioID() As Variant
    FormularioID = m_vFormularioID
End Property

Public Property Let MaterialQA(ByVal dato As Variant)
    m_vMaterialQA = dato
End Property
Public Property Get MaterialQA() As Variant
    MaterialQA = m_vMaterialQA
End Property

Public Property Let UnidadNegQA(ByVal dato As Variant)
    m_vUnidadNegQA = dato
End Property
Public Property Get UnidadNegQA() As Variant
    UnidadNegQA = m_vUnidadNegQA
End Property

Public Property Let UnidadNegQADen(ByVal dato As Variant)
    m_vUnidadNegQADen = dato
End Property
Public Property Get UnidadNegQADen() As Variant
    UnidadNegQADen = m_vUnidadNegQADen
End Property

Public Property Let ValorDefecto(ByVal dato As Double)
    m_dValorDefecto = dato
End Property
Public Property Get ValorDefecto() As Double
    ValorDefecto = m_dValorDefecto
End Property

Public Property Get FecAct() As Variant
    FecAct = m_vFecact
End Property
Public Property Let FecAct(ByVal Data As Variant)
    m_vFecact = Data
End Property

Public Property Let Indice(ByVal varIndice As Variant)
    m_vIndice = varIndice
End Property
Public Property Get Indice() As Variant
    Indice = m_vIndice
End Property

Friend Property Set Conexion(ByVal vData As CConexion)

    ''' * Objetivo: Dar valor a la conexion de la Destino
    ''' * Recibe: Conexion
    ''' * Devuelve: Nada

    Set m_oConexion = vData
    
End Property
Friend Property Get Conexion() As CConexion

    ''' * Objetivo: Devolver la conexion de la Destino
    ''' * Recibe: Nada
    ''' * Devuelve: Conexion

    Set Conexion = m_oConexion
    
End Property

Public Property Set VariblesCal(ByVal vData As CVariablesCalidad)

    Set m_oVarsCal = vData
    
End Property
Public Property Get VariblesCal() As CVariablesCalidad

    Set VariblesCal = m_oVarsCal
    
End Property


Private Sub Class_Terminate()
    On Error GoTo Error:
    
    Set m_oConexion = Nothing
    Exit Sub
Error:
    Resume Next
End Sub


