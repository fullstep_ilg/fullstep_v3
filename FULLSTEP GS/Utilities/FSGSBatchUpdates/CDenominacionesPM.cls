VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cDenominacionesPM"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Const SEPARADOR As String = "#"

Private m_oConexion As CConexion 'local copy
Private mIdioma As New Collection

Friend Property Set Conexion(ByVal vData As CConexion)
    Set m_oConexion = vData
End Property
Friend Property Get Conexion() As CConexion
    Set Conexion = m_oConexion
End Property
Private Sub Class_Terminate()
    Set m_oConexion = Nothing
End Sub
Public Function ActualizarDenominaciones() As Long
'********************************************************************************************************************
'*** Autor: sra(20/03/2009)                                                                                     *****
'*** Descripci�n: Actualiza las nuevas denominaciones en COPIA_CAMPO, COPIA_CAMPO_DESSGLOSE y                   *****
'***                        las tablas de filtros.                                                              *****
'*** Par�metros de salida: 1 si todo ha ido bien. 0 si ha habido un error.                                      *****
'*** Llamada desde: ActualizarDenominacion                                                                      *****
'********************************************************************************************************************
    Dim sConsulta As String
    Dim adoRes As Recordset
    Dim sError As String
            
    On Error GoTo ERROR_SALIR
    
    ActualizarDenominaciones = 1
    
    CargarIdiomasAplicacion
    
    sConsulta = "SELECT ID, ACCION, COD, DEN_NEW, COD_NEW FROM UPD_DEN WITH (NOLOCK) WHERE OK=0 AND ERROR IS NULL"
    Set adoRes = New ADODB.Recordset
    adoRes.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    
    Do While Not adoRes.eof
    
        'Quitamos la transaccion para que no se bloquee, si falla quedar� con ok=0
        'm_oConexion.ADOCon.Execute "BEGIN TRANSACTION", , adExecuteNoRecords
    
        If ActualizarDenominacion(NullToStr(adoRes.Fields("ACCION").Value), NullToStr(adoRes.Fields("COD").Value), NullToStr(adoRes.Fields("DEN_NEW").Value), NullToStr(adoRes.Fields("COD_NEW").Value)) Then
            'OK
            m_oConexion.ADOCon.Execute "UPDATE UPD_DEN SET OK=1,FECLASTTRY=GETDATE() WHERE ID=" & adoRes.Fields("ID").Value
'            If m_oConexion.ADOCon.Errors.Count > 0 Then _
'                        GoTo ERROR_SALIR
            'm_oConexion.ADOCon.Execute "COMMIT TRANSACTION", , adExecuteNoRecords
            
        Else
            'NOOK
            sError = ObtenerError(m_oConexion.ADOCon.Errors)
'            m_oConexion.ADOCon.Execute "ROLLBACK TRANSACTION"
            GrabarErrorDenominacion sError, adoRes.Fields("ID").Value
        End If
        
        
        adoRes.MoveNext
    Loop
                        
    adoRes.Close
    Set adoRes = Nothing
    Exit Function
    
ERROR_SALIR:
    m_oConexion.ADOCon.Execute "IF @@TRANCOUNT>0 ROLLBACK TRANSACTION"
    If Not adoRes Is Nothing Then
        adoRes.Close
        Set adoRes = Nothing
    End If
    ActualizarDenominaciones = 0
End Function

''' <summary>Actualiza la nueva denominaci�n en COPIA_CAMPO, COPIA_CAMPO_DESSGLOSE y las tablas de filtros.</summary>
''' <param name="sAccion">codigo del campo del sistema al que se ha modificado la denominaci�n</param>
''' <param name="sCodigo">valor del campo del sistema al que se ha modificado la denominaci�n</param>
''' <param name="sNuevaDenominacion">nueva denominaci�n a actualizar</param>
''' <param name="sTextCod">c�digo que se graba en VALOR_TEXT_COD. Contiene (BAJALOG/GENERICO,...)</param>
''' <returns>True si todo ha ido bien. False si ha habido un error.</returns>
''' <remarks>Llamada desde: ActualizarDenominaciones</remarks>
''' <revision>LTG 12/01/2012</revision>

Private Function ActualizarDenominacion(ByVal sAccion As String, ByVal sCodigo As String, ByVal sNuevaDenominacion As String, ByVal sTextCod As String) As Boolean
    Dim nTipoCampoGS As Integer
    Dim sWHERE As String
    Dim sWHEREComun As String
    Dim sWHEREDesglose As String
    Dim sWHERECampo As String
    Dim sConsulta As String
    
    On Error GoTo ERROR_SALIR
    
    sWHERE = ""
    sWHEREComun = ""
    sWHEREDesglose = ""
    sWHERECampo = ""
    Select Case sAccion
        Case "UN"
            nTipoCampoGS = "(" & TipoCampoSistema.Unidad & ", " & TipoCampoSistema.UnidadPedido & ")"
        Case "PG"
            nTipoCampoGS = TipoCampoSistema.FormaPago
        Case "DT"
            nTipoCampoGS = TipoCampoSistema.Destino
        Case "MON"
            nTipoCampoGS = TipoCampoSistema.Moneda
        Case "PAI"
            nTipoCampoGS = TipoCampoSistema.Pais
        Case "PRV"
            'la provincia est� relacionada con el pais
            Dim sPais As String
            Dim sProvincia As String
            Dim nSeparador As Integer
            nTipoCampoGS = TipoCampoSistema.Provincia
            nSeparador = InStr(1, sCodigo, "#")
            If nSeparador > 0 Then
                sPais = Left(sCodigo, nSeparador - 1)
                sProvincia = Mid(sCodigo, nSeparador + 1)
            End If
            'para actualizar copia_campo
            sWHERECampo = " AND F.INSTANCIA IN ( SELECT F.INSTANCIA FROM COPIA_CAMPO F WITH (NOLOCK) "
            sWHERECampo = sWHERECampo & "INNER JOIN FORM_CAMPO FC WITH (NOLOCK) ON F.COPIA_CAMPO_DEF=FC.ID "
            sWHERECampo = sWHERECampo & "INNER JOIN INSTANCIA I WITH (NOLOCK) ON I.ID=F.INSTANCIA AND ((I.ESTADO<104) OR (I.ESTADO=300) OR (I.ESTADO=301) OR (I.ESTADO=200)) "
            sWHERECampo = sWHERECampo & "WHERE FC.ES_SUBCAMPO = 0 AND FC.TIPO_CAMPO_GS=" & TipoCampoSistema.Pais & " and F.VALOR_TEXT='" & DblQuote(sPais) & "') "
            sWHERECampo = sWHERECampo & "AND F.VALOR_TEXT='" & DblQuote(sProvincia) & "'"
            'para actualizar copia_linea_desglose
            sWHEREDesglose = " AND CC.INSTANCIA IN ( SELECT CC.INSTANCIA FROM COPIA_LINEA_DESGLOSE F WITH (NOLOCK) "
            sWHEREDesglose = sWHEREDesglose & " INNER JOIN COPIA_CAMPO CC WITH (NOLOCK) ON CC.ID=F.CAMPO_HIJO "
            sWHEREDesglose = sWHEREDesglose & " INNER JOIN FORM_CAMPO FC WITH (NOLOCK) ON CC.COPIA_CAMPO_DEF=FC.ID "
            sWHEREDesglose = sWHEREDesglose & " INNER JOIN INSTANCIA I WITH (NOLOCK) ON I.ID=CC.INSTANCIA AND ((I.ESTADO<104) OR (I.ESTADO=300) OR (I.ESTADO=301) OR (I.ESTADO=200)) "
            sWHEREDesglose = sWHEREDesglose & " WHERE FC.ES_SUBCAMPO = 1 AND FC.TIPO_CAMPO_GS=" & TipoCampoSistema.Pais & " and F.VALOR_TEXT='" & DblQuote(sPais) & "') "
        Case "GMN1", "GMN2", "GMN3", "GMN4"
            nTipoCampoGS = TipoCampoSistema.Material
        Case "PER"
            nTipoCampoGS = TipoCampoSistema.Persona
        Case "COM"
            nTipoCampoGS = TipoCampoSistema.Comprador
        Case "ART"
            nTipoCampoGS = TipoCampoSistema.Articulo
        Case "ALM"
            nTipoCampoGS = TipoCampoSistema.Almacen
            sWHERE = " AND F.VALOR_NUM = " & sCodigo
        Case "CNT"
            nTipoCampoGS = TipoCampoSistema.Centro
        Case "ORG"
            nTipoCampoGS = TipoCampoSistema.OrganizacionCompras
        Case "UON1", "UON2", "UON3"
            nTipoCampoGS = TipoCampoSistema.UnidadOrganizativa
            'sustituimos el caracter "|" por " - " que es como viene en valor_text
            sCodigo = Replace(sCodigo, "|", " - ")
        Case "DEP"
            nTipoCampoGS = TipoCampoSistema.Departamento
        Case "PROV"
            nTipoCampoGS = TipoCampoSistema.Proveedor
        Case "P11", "P12", "P13", "P14"
            nTipoCampoGS = TipoCampoSistema.PresupuestoTipo1
            sCodigo = sCodigo & "_1" 'para que actualice los presupuestos simples
        Case "P21", "P22", "P23", "P24"
            nTipoCampoGS = TipoCampoSistema.PresupuestoTipo2
            sCodigo = sCodigo & "_1" 'para que actualice los presupuestos simples
        Case "P31", "P32", "P33", "P34"
            nTipoCampoGS = TipoCampoSistema.PresupuestoTipo3
            sCodigo = sCodigo & "_1" 'para que actualice los presupuestos simples
        Case "P41", "P42", "P43", "P44"
            nTipoCampoGS = TipoCampoSistema.PresupuestoTipo4
            sCodigo = sCodigo & "_1" 'para que actualice los presupuestos simples
        Case "CC"
            nTipoCampoGS = TipoCampoSistema.CentroCoste
        Case "PP"
            nTipoCampoGS = TipoCampoSistema.Partida
            sWHERE = " AND F.VALOR_TEXT LIKE '%#" & DblQuote(sCodigo) & "'"
        Case "ACTI"
            nTipoCampoGS = TipoCampoSistema.Activo
        Case Else 'Tabla externa
            nTipoCampoGS = 0
            sWHERE = " AND FC.TABLA_EXTERNA = " & sAccion 'el ID de la tabla
            sWHERE = sWHERE & " AND (COALESCE (F.VALOR_TEXT, CONVERT(varchar, F.VALOR_NUM), CONVERT(varchar, F.VALOR_FEC)) = '" & DblQuote(sCodigo) & "')"
    End Select
    
    
    'Parte de la WHERE com�n a ambas UPDATES
    If nTipoCampoGS > 0 And sAccion <> "UN" Then
        sWHEREComun = " AND FC.TIPO_CAMPO_GS=" & nTipoCampoGS
    ElseIf sAccion = "UN" Then
        sWHEREComun = " AND FC.TIPO_CAMPO_GS IN " & nTipoCampoGS
    Else
        sWHEREComun = " AND FC.TIPO_CAMPO_GS IS NULL "
    End If
    If sWHERE <> "" Then
        sWHEREComun = sWHEREComun & sWHERE
    ElseIf nTipoCampoGS <> TipoCampoSistema.Provincia Then
        sWHEREComun = sWHEREComun & " AND F.VALOR_TEXT='" & DblQuote(sCodigo) & "'"
    End If
    
    'Actualiza la denominaci�n de la tabla COPIA_CAMPO
    sConsulta = "UPDATE COPIA_CAMPO SET VALOR_TEXT_DEN = " & StrToSQLNULL(sNuevaDenominacion)
    sConsulta = sConsulta & ", VALOR_TEXT_COD = " & StrToSQLNULL(sTextCod)
    sConsulta = sConsulta & " FROM COPIA_CAMPO F WITH (NOLOCK)"
    sConsulta = sConsulta & " INNER JOIN FORM_CAMPO FC WITH (NOLOCK) ON F.COPIA_CAMPO_DEF=FC.ID"
    sConsulta = sConsulta & " INNER JOIN INSTANCIA I WITH (NOLOCK) ON I.ID=F.INSTANCIA AND ((I.ESTADO<104) OR (I.ESTADO=300) OR (I.ESTADO=301) OR (I.ESTADO=200)) "
    sConsulta = sConsulta & " WHERE FC.ES_SUBCAMPO = 0 "
    sConsulta = sConsulta & sWHEREComun & sWHERECampo
    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then
        GoTo ERROR_SALIR
    End If
    
    'Actualiza la denominaci�n de la tabla LINEA_DESGLOSE
    sConsulta = "UPDATE COPIA_LINEA_DESGLOSE SET VALOR_TEXT_DEN = " & StrToSQLNULL(sNuevaDenominacion)
    sConsulta = sConsulta & ", VALOR_TEXT_COD = " & StrToSQLNULL(sTextCod)
    sConsulta = sConsulta & " FROM COPIA_LINEA_DESGLOSE F WITH (NOLOCK)"
    sConsulta = sConsulta & " INNER JOIN COPIA_CAMPO CC WITH (NOLOCK) ON CC.ID=F.CAMPO_HIJO"
    sConsulta = sConsulta & " INNER JOIN FORM_CAMPO FC WITH (NOLOCK) ON CC.COPIA_CAMPO_DEF=FC.ID"
    sConsulta = sConsulta & " INNER JOIN INSTANCIA I WITH (NOLOCK) ON I.ID=CC.INSTANCIA AND ((I.ESTADO<104) OR (I.ESTADO=300) OR (I.ESTADO=301) OR (I.ESTADO=200)) "
    sConsulta = sConsulta & " WHERE FC.ES_SUBCAMPO = 1 "
    sConsulta = sConsulta & sWHEREComun & sWHEREDesglose
    m_oConexion.ADOCon.Execute sConsulta
    If m_oConexion.ADOCon.Errors.Count > 0 Then
        GoTo ERROR_SALIR
    End If
        
    'Actualiza la denominaciones cuando hay varios presupuestos en el mismo campo
    If nTipoCampoGS = TipoCampoSistema.PresupuestoTipo1 Or nTipoCampoGS = TipoCampoSistema.PresupuestoTipo2 Or nTipoCampoGS = TipoCampoSistema.PresupuestoTipo3 Or nTipoCampoGS = TipoCampoSistema.PresupuestoTipo4 Then
        If Not ActualizarPresupuestosMixtos(nTipoCampoGS, sCodigo, sNuevaDenominacion, sTextCod) Then
            GoTo ERROR_SALIR
        End If
        If Not ActualizarPresupuestosMixtosDesglose(nTipoCampoGS, sCodigo, sNuevaDenominacion, sTextCod) Then
            GoTo ERROR_SALIR
        End If
    End If
    
    If Not ActualizarDenominacionEnFiltros(sCodigo, nTipoCampoGS, sNuevaDenominacion) Then
        GoTo ERROR_SALIR
    End If
    
    If nTipoCampoGS = TipoCampoSistema.UnidadOrganizativa Then
        'Actualiza la denominaci�n de la tabla CENTRO_SM_DEN
        ActualizarDenominacionCentroSM sCodigo, sNuevaDenominacion
    End If
            
    ActualizarDenominacion = True
    Exit Function
    
ERROR_SALIR:
    ActualizarDenominacion = False
End Function

'********************************************************************************************************************
'*** Autor: sra(20/03/2009)                                                                                     *****
'*** Descripci�n: Actualiza la nueva denominaci�n en las tablas de filtros.                                     *****
'*** Par�metros de entrada: sCodigo: codigo del campo del sistema al que se ha modificado la denominaci�n       *****
'***                        nTipoCampoGS: el tipo de campo GS (material, proveedor,...). 0 = Tabla externa.     *****
'***                        sNuevaDenominacion: nueva denominaci�n a actualizar.                                *****
'*** Par�metros de salida: True si todo ha ido bien. False si ha habido un error.                               *****
'*** Llamada desde: ActualizarDenominacion                                                                      *****
'********************************************************************************************************************
Private Function ActualizarDenominacionEnFiltros(ByVal sCodigo As String, ByVal nTipoCampoGS As Integer, ByVal sNuevaDenominacion As String) As Boolean
    Dim sConsulta As String
    Dim iIdioma As Integer
    Dim sDenominacionAActualizar As String
    Dim adoRes As ADODB.Recordset
    Dim rsExiste As New ADODB.Recordset
        
    On Error GoTo ERROR_SALIR
    'Filtros PM
    sConsulta = "SELECT DISTINCT FS.NOM_TABLA,FC.CAMPO_ORIGEN FROM INSTANCIA I WITH (NOLOCK) "
    sConsulta = sConsulta & "INNER JOIN SOLICITUD S WITH (NOLOCK) ON I.SOLICITUD = S.ID "
    sConsulta = sConsulta & "INNER JOIN PM_FILTROS FS WITH (NOLOCK) ON S.FORMULARIO =FS.FORMULARIO "
    sConsulta = sConsulta & "INNER JOIN COPIA_CAMPO F WITH (NOLOCK) ON F.INSTANCIA=I.ID "
    sConsulta = sConsulta & "INNER JOIN FORM_CAMPO FC WITH (NOLOCK) ON F.COPIA_CAMPO_DEF=FC.ID "
    sConsulta = sConsulta & "WHERE FC.ES_SUBCAMPO = 0 AND I.ESTADO<104 AND F.VALOR_TEXT='" & DblQuote(sCodigo) & "' AND FC.TIPO_CAMPO_GS=" & nTipoCampoGS
    'Filtros QA
    sConsulta = sConsulta & " UNION "
    sConsulta = "SELECT DISTINCT FS.NOM_TABLA,FC.CAMPO_ORIGEN FROM INSTANCIA I WITH (NOLOCK) "
    sConsulta = sConsulta & "INNER JOIN SOLICITUD S WITH (NOLOCK) ON I.SOLICITUD = S.ID "
    sConsulta = sConsulta & "INNER JOIN QA_FILTROS FS WITH (NOLOCK) ON S.FORMULARIO =FS.FORMULARIO "
    sConsulta = sConsulta & "INNER JOIN COPIA_CAMPO F WITH (NOLOCK) ON F.INSTANCIA=I.ID "
    sConsulta = sConsulta & "INNER JOIN FORM_CAMPO FC WITH (NOLOCK) ON F.COPIA_CAMPO_DEF=FC.ID "
    sConsulta = sConsulta & "WHERE FC.ES_SUBCAMPO = 0 AND ((I.ESTADO=300) OR (I.ESTADO=301) OR (I.ESTADO=200)) AND F.VALOR_TEXT='" & DblQuote(sCodigo) & "' AND FC.TIPO_CAMPO_GS=" & nTipoCampoGS

    
    Set adoRes = New ADODB.Recordset
    adoRes.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    
    Do While Not adoRes.eof
        
        For iIdioma = 1 To mIdioma.Count
            sDenominacionAActualizar = ObtenerDenominacionParaFiltro(mIdioma.Item(iIdioma), nTipoCampoGS, sCodigo, sNuevaDenominacion)
                        
            'Puede que la columna ya no exista en las tablas de filtros
            sConsulta = "SELECT count(*) FROM dbo.syscolumns WHERE ID = object_id('" & mIdioma.Item(iIdioma) & "_" & adoRes("NOM_TABLA") & "') AND Name = 'C_" & adoRes("CAMPO_ORIGEN") & "'"
            rsExiste.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
            
            If rsExiste(0) > 0 Then
                sConsulta = " UPDATE " & mIdioma.Item(iIdioma) & "_" & adoRes("NOM_TABLA") & " SET C_" & adoRes("CAMPO_ORIGEN") & " = " & StrToSQLNULL(sDenominacionAActualizar)
                sConsulta = sConsulta & " FROM " & mIdioma.Item(iIdioma) & "_" & adoRes("NOM_TABLA") & " FILTRO WITH (NOLOCK) "
                sConsulta = sConsulta & " INNER JOIN INSTANCIA I WITH (NOLOCK) ON FILTRO.INSTANCIA = I.ID "
                sConsulta = sConsulta & " INNER JOIN SOLICITUD S WITH (NOLOCK) ON I.SOLICITUD = S.ID"
                sConsulta = sConsulta & " INNER JOIN COPIA_CAMPO F WITH (NOLOCK) ON F.INSTANCIA=I.ID"
                sConsulta = sConsulta & " INNER JOIN FORM_CAMPO FC WITH (NOLOCK) ON F.COPIA_CAMPO_DEF=FC.ID"
                sConsulta = sConsulta & " WHERE FC.ES_SUBCAMPO = 0 AND FC.TIPO_CAMPO_GS=" & nTipoCampoGS & " AND ((I.ESTADO<104) OR (I.ESTADO=300) OR (I.ESTADO=301) OR (I.ESTADO=200)) "
                If nTipoCampoGS = TipoCampoSistema.Almacen Then
                    sConsulta = sConsulta & " AND F.VALOR_NUM=" & sCodigo
                Else
                    sConsulta = sConsulta & " AND F.VALOR_TEXT='" & DblQuote(sCodigo) & "'"
                End If
                m_oConexion.ADOCon.Execute sConsulta
                If m_oConexion.ADOCon.Errors.Count > 0 Then
                    GoTo ERROR_SALIR
                End If
            End If

            rsExiste.Close
        Next
    
        adoRes.MoveNext
    Loop
                        
    adoRes.Close
    Set adoRes = Nothing
        
    ActualizarDenominacionEnFiltros = True
    Exit Function
    
ERROR_SALIR:
    If Not adoRes Is Nothing Then
        adoRes.Close
        Set adoRes = Nothing
    End If
    ActualizarDenominacionEnFiltros = False
End Function

Private Function ActualizarPresupuestosMixtos(ByVal nTipoCampoGS As Integer, ByVal sCodigo As String, ByVal sNuevaDenominacion As String, ByVal sTextCod As String) As Boolean
    Dim sConsulta As String
    Dim sDenominacionesActual As String
    Dim sDenominacionesNueva As String
    Dim nNumPresupuestoAReemplazar As Integer
    Dim adoPres As Recordset
        
    On Error GoTo ERROR_SALIR
    
    ActualizarPresupuestosMixtos = True
    
    sConsulta = "SELECT F.ID,VALOR_TEXT,VALOR_TEXT_DEN,VALOR_TEXT_COD FROM COPIA_CAMPO F WITH (NOLOCK) "
    sConsulta = sConsulta & " INNER JOIN FORM_CAMPO FC WITH (NOLOCK) ON F.COPIA_CAMPO_DEF=FC.ID "
    sConsulta = sConsulta & " INNER JOIN INSTANCIA I WITH (NOLOCK) ON I.ID=F.INSTANCIA AND ((I.ESTADO<104) OR (I.ESTADO=300) OR (I.ESTADO=301) OR (I.ESTADO=200)) "
    sConsulta = sConsulta & " WHERE FC.ES_SUBCAMPO = 0 AND FC.TIPO_CAMPO_GS = " & nTipoCampoGS
    sConsulta = sConsulta & " AND F.VALOR_TEXT LIKE '%" & SEPARADOR & "%' AND F.VALOR_TEXT LIKE '%" & Mid(sCodigo, 1, Len(sCodigo) - 1) & "%'" 'ej: 4_5_1, nos quedamos con 4_5_, quitamos el porcentaje
    
    Set adoPres = New ADODB.Recordset
    adoPres.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    Do While Not adoPres.eof
        nNumPresupuestoAReemplazar = 0
        Call DevolverPosicionYTextCod(NullToStr(adoPres.Fields("VALOR_TEXT").Value) & SEPARADOR, sCodigo, NullToStr(adoPres.Fields("VALOR_TEXT_COD").Value), nTipoCampoGS, nNumPresupuestoAReemplazar, sTextCod)
        
        If nNumPresupuestoAReemplazar > 0 Then
            sDenominacionesNueva = DevolverDenominacionesNueva(NullToStr(adoPres.Fields("VALOR_TEXT_DEN").Value) & SEPARADOR, sNuevaDenominacion, nNumPresupuestoAReemplazar)
        
            'Actualiza la denominaci�n de la tabla COPIA_CAMPO para ese ID
            sConsulta = "UPDATE COPIA_CAMPO SET VALOR_TEXT_DEN = " & StrToSQLNULL(sDenominacionesNueva)
            sConsulta = sConsulta & ", VALOR_TEXT_COD = " & StrToSQLNULL(sTextCod)
            sConsulta = sConsulta & " WHERE ID = " & adoPres.Fields("ID").Value
            m_oConexion.ADOCon.Execute sConsulta
            If m_oConexion.ADOCon.Errors.Count > 0 Then
                GoTo ERROR_SALIR
            End If
        End If
        adoPres.MoveNext
    Loop
    
    adoPres.Close
    Set adoPres = Nothing
    Exit Function
    
ERROR_SALIR:
    If Not adoPres Is Nothing Then
        adoPres.Close
        Set adoPres = Nothing
    End If
    ActualizarPresupuestosMixtos = False
End Function

''' <summary>Actualiza la nueva denominaci�n en COPIA_CAMPO, COPIA_CAMPO_DESSGLOSE y las tablas de filtros.</summary>
''' <param name="nTipoCampoGS">nTipoCampoGS</param>
''' <param name="sCodigo">sCodigo</param>
''' <param name="sNuevaDenominacion">sNuevaDenominacion</param>
''' <param name="sTextCod">sTextCod</param>
''' <returns>True si todo ha ido bien. False si ha habido un error.</returns>
''' <remarks>Llamada desde: ActualizarDenominacion</remarks>
''' <revision>LTG 12/01/2012</revision>

Private Function ActualizarPresupuestosMixtosDesglose(ByVal nTipoCampoGS As Integer, ByVal sCodigo As String, ByVal sNuevaDenominacion As String, _
        ByVal sTextCod As String) As Boolean
    Dim sConsulta As String
    Dim sDenominacionesNueva As String
    Dim nNumPresupuestoAReemplazar As Integer
    Dim adoPres As Recordset
        
    On Error GoTo ERROR_SALIR
    
    ActualizarPresupuestosMixtosDesglose = True
    
    sConsulta = "SELECT F.LINEA, F.CAMPO_PADRE, F.CAMPO_HIJO, F.VALOR_TEXT,F.VALOR_TEXT_DEN,F.VALOR_TEXT_COD "
    sConsulta = sConsulta & "FROM COPIA_LINEA_DESGLOSE F WITH (NOLOCK) "
    sConsulta = sConsulta & "INNER JOIN COPIA_CAMPO CC WITH (NOLOCK) ON CC.ID=F.CAMPO_HIJO "
    sConsulta = sConsulta & "INNER JOIN FORM_CAMPO FC WITH (NOLOCK) ON CC.COPIA_CAMPO_DEF=FC.ID "
    sConsulta = sConsulta & "INNER JOIN INSTANCIA I WITH (NOLOCK) ON I.ID=CC.INSTANCIA AND ((I.ESTADO<104) OR (I.ESTADO=300) OR (I.ESTADO=301) OR (I.ESTADO=200)) "
    sConsulta = sConsulta & "WHERE FC.ES_SUBCAMPO = 1 And FC.TIPO_CAMPO_GS = " & nTipoCampoGS
    sConsulta = sConsulta & "AND F.VALOR_TEXT like '%" & SEPARADOR & "%' AND F.VALOR_TEXT LIKE '%" & Mid(sCodigo, 1, Len(sCodigo) - 1) & "%'" 'ej: 4_5_1, nos quedamos con 4_5_, quitamos el porcentaje

    Set adoPres = New ADODB.Recordset
    adoPres.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    Do While Not adoPres.eof
        nNumPresupuestoAReemplazar = 0
        Call DevolverPosicionYTextCod(NullToStr(adoPres.Fields("VALOR_TEXT").Value) & SEPARADOR, sCodigo, NullToStr(adoPres.Fields("VALOR_TEXT_COD").Value), nTipoCampoGS, nNumPresupuestoAReemplazar, sTextCod)
        
        If nNumPresupuestoAReemplazar > 0 Then
            sDenominacionesNueva = DevolverDenominacionesNueva(NullToStr(adoPres.Fields("VALOR_TEXT_DEN").Value) & SEPARADOR, sNuevaDenominacion, nNumPresupuestoAReemplazar)
        
            'Actualiza la denominaci�n de la tabla COPIA_LINEA_DESGLOSE para esa LINEA-PADRE-HIJO
            sConsulta = "UPDATE COPIA_LINEA_DESGLOSE SET VALOR_TEXT_DEN = " & StrToSQLNULL(sDenominacionesNueva)
            sConsulta = sConsulta & ", VALOR_TEXT_COD = " & StrToSQLNULL(sTextCod)
            sConsulta = sConsulta & " WHERE LINEA = " & adoPres.Fields("LINEA").Value
            sConsulta = sConsulta & " AND CAMPO_PADRE = " & adoPres.Fields("CAMPO_PADRE").Value
            sConsulta = sConsulta & " AND CAMPO_HIJO = " & adoPres.Fields("CAMPO_HIJO").Value
            m_oConexion.ADOCon.Execute sConsulta
            If m_oConexion.ADOCon.Errors.Count > 0 Then
                GoTo ERROR_SALIR
            End If
                
        End If
        adoPres.MoveNext
    Loop
    
    adoPres.Close
    Set adoPres = Nothing
    Exit Function
    
ERROR_SALIR:
    If Not adoPres Is Nothing Then
        adoPres.Close
        Set adoPres = Nothing
    End If
    ActualizarPresupuestosMixtosDesglose = False
End Function

Private Function DevolverDenominacionesNueva(ByVal sDenominacionesActual As String, ByVal sNuevaDenominacion As String, ByVal nNumPresupuestoAReemplazar As Integer)
    Dim inicio As Integer
    Dim nContadorPresupuestos As Integer
    Dim sDenominacionesNueva As String
    Dim nSeparador As Integer
          
            nSeparador = 0
            nContadorPresupuestos = 0
            Do While nContadorPresupuestos < nNumPresupuestoAReemplazar
                inicio = nSeparador + 1
                nSeparador = InStr(inicio, sDenominacionesActual, SEPARADOR)
                nContadorPresupuestos = nContadorPresupuestos + 1
            Loop
            
            sDenominacionesNueva = ""
            If inicio = 1 Then
                sDenominacionesNueva = sNuevaDenominacion & Mid(sDenominacionesActual, nSeparador)
            Else
                sDenominacionesNueva = Mid(sDenominacionesActual, 1, inicio - 1) & sNuevaDenominacion & Mid(sDenominacionesActual, nSeparador)
            End If
            sDenominacionesNueva = Left(sDenominacionesNueva, Len(sDenominacionesNueva) - 1) 'para quitar la # del final
    DevolverDenominacionesNueva = sDenominacionesNueva
End Function

''' <summary>Actualiza la nueva denominaci�n en COPIA_CAMPO, COPIA_CAMPO_DESSGLOSE y las tablas de filtros.</summary>
''' <param name="sPresupuestos">sPresupuestos</param>
''' <param name="sCodigo">sCodigo</param>
''' <param name="sTextCodActual">sTextCodActual</param>
''' <param name="nTipoCampoGS">nTipoCampoGS</param>
''' <param name="nNumPresupuestoAReemplazar">nNumPresupuestoAReemplazar</param>
''' <param name="sTextCod">sTextCod</param>
''' <remarks>Llamada desde: ActualizarPresupuestosMixtosDesglose</remarks>
''' <revision>LTG 12/01/2012</revision>

Private Sub DevolverPosicionYTextCod(ByVal sPresupuestos As String, ByVal sCodigo As String, ByVal sTextCodActual As String, ByVal nTipoCampoGS As Integer, _
        ByRef nNumPresupuestoAReemplazar As Integer, ByRef sTextCod As String)
    Dim rs As Recordset
    Dim sConsulta As String
    Dim sPresupuesto As String
    Dim sRestoPresupuestos As String
    Dim nSeparador As Integer
    Dim nContadorPresupuestos As Integer
        
    nContadorPresupuestos = 0
    Do While Len(sPresupuestos) > 0
        nSeparador = InStr(1, sPresupuestos, SEPARADOR)
        
        If nSeparador > 0 Then
            nContadorPresupuestos = nContadorPresupuestos + 1
            sPresupuesto = Left(sPresupuestos, nSeparador - 1)
            sPresupuestos = Mid(sPresupuestos, nSeparador + 1)
                    
            nSeparador = InStrRev(sPresupuesto, "_")
            If nSeparador > 0 Then
                If sCodigo = Left(sPresupuesto, nSeparador) & "1" Then
                    nNumPresupuestoAReemplazar = nContadorPresupuestos
                Else 'no incluimos en la consulta el presupuesto que queremos actualizar
                    sRestoPresupuestos = sRestoPresupuestos & "'" & Left(sPresupuesto, nSeparador) & "1',"
                End If
            End If
        End If
    Loop
    
    'CAMPO: VALOR_TEXT_COD que almacena si est� de baja l�gica alguno de los presupuestos o ninguno de ellos
    If sTextCod = "0" And sTextCodActual = "1" Then
        'si VALOR_TEXT_COD estaba a 1, ya ahora llega con un 0, hay que averiguar si los dem�s presupuestos est�n en baja l�gica para cambiar su valor
        Set rs = New ADODB.Recordset
        sConsulta = "SELECT COUNT(*) FROM COPIA_CAMPO F WITH (NOLOCK)"
        sConsulta = sConsulta & " INNER JOIN FORM_CAMPO FC WITH (NOLOCK) ON F.COPIA_CAMPO_DEF=FC.ID"
        sConsulta = sConsulta & " INNER JOIN INSTANCIA I WITH (NOLOCK) ON I.ID=F.INSTANCIA AND ((I.ESTADO<104) OR (I.ESTADO=300) OR (I.ESTADO=301) OR (I.ESTADO=200)) "
        sConsulta = sConsulta & " WHERE FC.ES_SUBCAMPO = 0 And FC.TIPO_CAMPO_GS = " & nTipoCampoGS
        sConsulta = sConsulta & " AND F.VALOR_TEXT NOT LIKE '%" & SEPARADOR & "%' AND F.VALOR_TEXT_COD = '1'"
        sConsulta = sConsulta & " AND F.VALOR_TEXT IN (" & Left(sRestoPresupuestos, Len(sRestoPresupuestos) - 1) & ") "
        sConsulta = sConsulta & " UNION "
        sConsulta = sConsulta & " SELECT COUNT(*) FROM COPIA_LINEA_DESGLOSE F WITH (NOLOCK)"
        sConsulta = sConsulta & " INNER JOIN COPIA_CAMPO CC WITH (NOLOCK) ON CC.ID=F.CAMPO_HIJO"
        sConsulta = sConsulta & " INNER JOIN FORM_CAMPO FC WITH (NOLOCK) ON CC.COPIA_CAMPO_DEF=FC.ID"
        sConsulta = sConsulta & " INNER JOIN INSTANCIA I WITH (NOLOCK) ON I.ID=CC.INSTANCIA AND ((I.ESTADO<104) OR (I.ESTADO=300) OR (I.ESTADO=301) OR (I.ESTADO=200)) "
        sConsulta = sConsulta & " WHERE FC.ES_SUBCAMPO = 1 And FC.TIPO_CAMPO_GS = " & nTipoCampoGS
        sConsulta = sConsulta & " AND F.VALOR_TEXT IN (" & Left(sRestoPresupuestos, Len(sRestoPresupuestos) - 1) & ") "
        
        rs.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
              
        If rs(0).Value > 0 Then
            sTextCod = "1" 'lo cambiamos a 1 para hacer la UPDATE
        End If
    End If
    
End Sub
Private Sub GrabarErrorDenominacion(ByVal sErrorStr As String, ByVal iID As Long)
    Dim adoComm As ADODB.Command
    
    Set adoComm = New ADODB.Command
    adoComm.ActiveConnection = m_oConexion.ADOCon
    adoComm.CommandType = adCmdText
    adoComm.CommandText = "UPDATE UPD_DEN SET FECLASTTRY = ?, ERROR = ? WHERE ID = ?"
    adoComm.Parameters.Append adoComm.CreateParameter("FECLASTTRY", adDate, adParamInput, Value:=Now)
    adoComm.Parameters.Append adoComm.CreateParameter("ERROR", adVarChar, adParamInput, 200, sErrorStr)
    adoComm.Parameters.Append adoComm.CreateParameter("ID", adInteger, adParamInput, Value:=iID)
    adoComm.Execute
    
End Sub

Private Function ObtenerError(ByVal cErrores As ADODB.Errors)
    Dim oError As ADODB.Error
    Dim sErrorStr As String
    
    sErrorStr = ""
    
    'Para cada error de la colecci�n cErrores escribimos el n�mero de error y su descripci�n
    For Each oError In cErrores
        sErrorStr = sErrorStr & "N� Error: " & oError.Number & "--"
        sErrorStr = sErrorStr & "Descripci�n: " & oError.Description & "."
    Next
    If Len(sErrorStr) > 200 Then
        sErrorStr = Left(sErrorStr, 200)
    End If
    
    ObtenerError = sErrorStr
End Function

Private Sub CargarIdiomasAplicacion()
'****************************************************************************************************************
'*** Descripci�n: Carga los idiomas en una colecci�n una sola vez .                                         *****
'*** Llamada desde: ActualizarDenominaciones                                                                *****
'****************************************************************************************************************
    
    Dim rs As New ADODB.Recordset
    Dim sConsulta As String
    
    sConsulta = "SELECT COD FROM IDIOMAS WITH(NOLOCK) WHERE APLICACION=1"
    rs.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    
    While Not rs.eof
        mIdioma.Add rs.Fields("COD").Value
        rs.MoveNext
    Wend
    rs.Close
End Sub

Private Sub ActualizarDenominacionCentroSM(ByVal sCodigo As String, ByVal sNuevaDenominacion As String)
    Dim rs As New ADODB.Recordset
    Dim sConsulta As String
    Dim Result() As String
    Dim I As Integer
    Dim sUON(3) As String
    Dim sCentroSM As String
    
    sCentroSM = ""
    Result = Split(sCodigo, " - ")
    For I = 0 To UBound(Result)
        sUON(I) = Result(I)
    Next I

    sConsulta = "SELECT CENTRO_SM FROM CENTRO_SM_UON CSM WITH (NOLOCK) WHERE CSM.UON1 = '" & sUON(0) & "' AND ISNULL(CSM.UON2,'') = ISNULL('" & sUON(1) & "','') "
    sConsulta = sConsulta & " AND ISNULL(CSM.UON3,'') = ISNULL('" & sUON(2) & "','') AND ISNULL(CSM.UON4,'') = ISNULL('" & sUON(3) & "','')"
    rs.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    While Not rs.eof
        sCentroSM = rs.Fields("CENTRO_SM").Value
        rs.MoveNext
    Wend
    rs.Close
    
    If sCentroSM <> "" Then
        sConsulta = "UPDATE CENTRO_SM_DEN SET DEN = " & StrToSQLNULL(sNuevaDenominacion)
        sConsulta = sConsulta & " WHERE CENTRO_SM = '" & sCentroSM & "'"
        m_oConexion.ADOCon.Execute sConsulta
    End If
    
End Sub
