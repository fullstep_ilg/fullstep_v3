VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cVisoresPM"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Private Enum Subtipos
    TipoString = 1
    TipoNumerico = 2
    TipoFecha = 3
    TipoBoolean = 4
    TipoTextoCorto = 5
    TipoTextoMedio = 6
    TipoTextoLargo = 7
    TipoArchivo = 8
    TipoDesglose = 9
    TipoEditor = 15
End Enum

Private m_oConexion As CConexion 'local copy
Private mIdioma As New Collection

Friend Property Set Conexion(ByVal vData As CConexion)
    Set m_oConexion = vData
End Property
Friend Property Get Conexion() As CConexion
    Set Conexion = m_oConexion
End Property
Private Sub Class_Terminate()
    Set m_oConexion = Nothing
End Sub

'*****************************************************************************************************
'*** Descripci�n: Lleva a cabo las actualizaciones sobre las tablas de filtros din�micas. Para   *****
'*** ello accede a la tabla UPD_VISORES_PM.                                                      *****
'*** Llamada desde: oBatch.ActualizarDenominacionesPM                                            *****
'*****************************************************************************************************
Public Function ActualizarVisoresPM() As Long
    Dim sConsulta As String
    Dim adoRes As Recordset
    Dim sError As String
    Dim Result As Integer
            
    On Error GoTo ERROR_SALIR
    
    ActualizarVisoresPM = 1
    
    CargarIdiomasAplicacion
    
    sConsulta = "SELECT U.ID,U.ACCION,U.ID_TABLA,U.ID_CAMPO,U.SUBTIPO,U.NOM_TABLA,COALESCE(FC.TIPO_CAMPO_GS,U.TIPO_CAMPO_GS,0) TIPO_CAMPO_GS,"
    sConsulta = sConsulta & "COALESCE(D.CAMPO_PADRE,U.CAMPO_PADRE) CAMPO_PADRE,FC.TABLA_EXTERNA,ISNULL(FC.INTRO,0) INTRO "
    sConsulta = sConsulta & "FROM UPD_VISORES_PM U WITH (NOLOCK) "
    sConsulta = sConsulta & "LEFT JOIN FORM_CAMPO FC WITH(NOLOCK) ON FC.ID=U.ID_CAMPO "
    sConsulta = sConsulta & "LEFT JOIN DESGLOSE D WITH(NOLOCK) ON D.CAMPO_HIJO=FC.ID "
    sConsulta = sConsulta & "WHERE OK = 0 AND ERROR IS NULL "
    sConsulta = sConsulta & "ORDER BY U.ID"
    Set adoRes = New ADODB.Recordset
    adoRes.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly

    Do While Not adoRes.eof
        m_oConexion.ADOCon.Execute "SET XACT_ABORT OFF"
        m_oConexion.ADOCon.Execute "BEGIN TRANSACTION", , adExecuteNoRecords
    
        Result = ActualizarVisorPM(NullToStr(adoRes.Fields("ACCION").Value), NullToDbl0(adoRes.Fields("ID_CAMPO").Value), _
            NullToDbl0(adoRes.Fields("SUBTIPO").Value), NullToStr(adoRes.Fields("NOM_TABLA").Value), _
            NullToDbl0(adoRes.Fields("TIPO_CAMPO_GS").Value), NullToDbl0(adoRes.Fields("CAMPO_PADRE").Value), _
            IsNull(adoRes("TABLA_EXTERNA")), (adoRes("INTRO") = 1))
            
        If Result = 1 Then
            'OK
            m_oConexion.ADOCon.Execute "UPDATE UPD_VISORES_PM SET OK=1 WHERE ID=" & adoRes.Fields("ID").Value
            If m_oConexion.ADOCon.Errors.Count > 0 Then _
                        GoTo ERROR_SALIR
            m_oConexion.ADOCon.Execute "COMMIT TRANSACTION", , adExecuteNoRecords
        ElseIf Result = -1 Then
            'NOOK
            sError = ObtenerError(m_oConexion.ADOCon.Errors)
            m_oConexion.ADOCon.Execute "IF @@TRANCOUNT>0 ROLLBACK TRANSACTION"
            GrabarErrorVisorPM sError, adoRes.Fields("ID").Value
        Else '0
            'NOOK
            sError = "N� Error: -- Se detecta que no existe el campo. "
            If NullToStr(adoRes.Fields("ACCION").Value) = "U" Then
                sError = sError & "TipocampoGS: " & NullToDbl0(adoRes.Fields("TIPO_CAMPO_GS").Value) & " Padre: " & NullToDbl0(adoRes.Fields("CAMPO_PADRE").Value)
            ElseIf NullToStr(adoRes.Fields("ACCION").Value) = "I" Then
                sError = sError & "Habra un borrado (U) sin procesar. O se borro todo el desglose."
            Else
                sError = sError & "Habra una inserci�n (I) sin procesar. O se borro todo el desglose."
            End If
            sError = Left(sError, 200)
            m_oConexion.ADOCon.Execute "IF @@TRANCOUNT>0 ROLLBACK TRANSACTION"
            GrabarErrorVisorPM sError, adoRes.Fields("ID").Value
        End If
        adoRes.MoveNext
    Loop
                        
    adoRes.Close
    Set adoRes = Nothing
    Exit Function
    
ERROR_SALIR:
    m_oConexion.ADOCon.Execute "IF @@TRANCOUNT>0 ROLLBACK TRANSACTION"
    If Not adoRes Is Nothing Then
        adoRes.Close
        Set adoRes = Nothing
    End If
    ActualizarVisoresPM = 0
End Function

''' <summary>Actualizar la tabla (a�adir campo, eliminar campo, borrar la tabla) pasada como par�metro</summary>
''' <param name="sAcci�n">la acci�n a realizar sobre la tabla (D,U,I)</param>
''' <param name="iIdCampo">el id del campo a a�adir o eliminar</param>
''' <param name="iSubtipo">el subtipo del campo a a�adir (num�rico, boolean, fecha,...)</param>
''' <param name="sNombreTabla">el nombre de la tabla sobre la que hay que trabajar + PRE idioma</param>
''' <returns>Si la actualizaci�n ha ido bien, devuelve 1. Si ha habido errores, devuelve -1. Si la entrada se detecta q se aplicar�a en una columna inexistente, devuelve 0</returns>
''' <remarks>Llamada desde: ActualizarVisoresPM</remarks>
''' <revision>LTG 12/01/2012</revision>

Private Function ActualizarVisorPM(ByVal sAccion As String, ByVal iIdCampo As Long, ByVal iSubtipo As Integer, _
        ByVal sNombreTabla As String, ByVal iTipoCampoGS As Integer, ByVal iCampoPadre As Long, _
        ByVal bTablaExterna As Boolean, ByVal bEsLista As Boolean) As Integer
    Dim sSQL As String
    Dim sNombreTablaCompleto As String
    Dim iIdioma As Integer
    Dim sDenominacion As String
    Dim adoRes As ADODB.Recordset
    Dim bNoExec As Boolean
    
    On Error GoTo ERROR_SALIR
    
    For iIdioma = 1 To mIdioma.Count
        sNombreTablaCompleto = mIdioma.Item(iIdioma) & "_" & sNombreTabla
        bNoExec = False
           
        Select Case sAccion
            Case "D"    'se elimina la tabla indicada en sNombreTablaCompleto
                sSQL = "DROP TABLE " & sNombreTablaCompleto
                
                m_oConexion.ADOCon.Execute sSQL
                If m_oConexion.ADOCon.Errors.Count > 0 Then
                    GoTo ERROR_SALIR
                End If
                
            Case "U"    'se elimina de la tabla (sNombreTablaCompleto) el campo (iIdCampo)
                'U - Borrado de campos que no existen, mirar antes de eliminar que existen
                sSQL = "SELECT 1 FROM SYSOBJECTS INNER JOIN SYSCOLUMNS ON SYSOBJECTS.ID=SYSCOLUMNS.ID"
                sSQL = sSQL & " WHERE SYSOBJECTS.XTYPE='U'"
                If iCampoPadre = 0 Then
                    sSQL = sSQL & " AND SYSCOLUMNS.NAME='C_" & iTipoCampoGS & "_" & iIdCampo & "'"
                Else
                    sSQL = sSQL & " AND SYSCOLUMNS.NAME='C_" & iCampoPadre & "_" & iTipoCampoGS & "_" & iIdCampo & "'"
                End If
                sSQL = sSQL & " AND SYSOBJECTS.NAME='" & sNombreTablaCompleto & "'"
                Set adoRes = New ADODB.Recordset
                adoRes.Open sSQL, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
                                           
                If adoRes.eof Then bNoExec = True
                
                adoRes.Close
                Set adoRes = Nothing
                
                If iCampoPadre = 0 Then
                    sSQL = "ALTER TABLE " & sNombreTablaCompleto & " DROP COLUMN C_" & iTipoCampoGS & "_" & iIdCampo
                Else
                    sSQL = "ALTER TABLE " & sNombreTablaCompleto & " DROP COLUMN C_" & iCampoPadre & "_" & iTipoCampoGS & "_" & iIdCampo
                End If
                
                If bNoExec = False Then
                    m_oConexion.ADOCon.Execute sSQL
                    If m_oConexion.ADOCon.Errors.Count > 0 Then
                        GoTo ERROR_SALIR
                    End If
                    m_oConexion.ADOCon.Execute sSQL & "_COD"
                    If m_oConexion.ADOCon.Errors.Count > 0 Then
                        GoTo ERROR_SALIR
                    End If
                End If
                
            Case "I", "A"   'I: se a�ade a la tabla (sNombreTablaCompleto) un campo nuevo (iIdCampo)
                            'A: se modifica un campo (iIdCampo) de la tabla (sNombreTablaCompleto)
                            '(ej: de texto corto a texto largo)
                            
                If iCampoPadre = 0 And InStr(sNombreTabla, "_DESGLOSE") > 0 Then
                    '"N� Error: -2147217900--Descripci�n: Invalid column name 'INSTANCIA'."
                    'Si haces I/A de un campo de desglose y no hay Form_Campo Padre (pq has borrado el desglose) -> NO hay campo padre, pero la tabla es desglose -> falla
                    'Fallo motivo: Pregunta por instancia en una tabla q no tiene dicha columna.
                    'Explicaci�n: No padre, la instancia en columna INSTANCIA (ejemplo: tabla FORM_INSTANCIA900). Repito el caso es tabla FORM_INSTANCIA900_DESGLOSE
                    '             Hay padre, la instancia en columna FORM_INSTANCIA (ejemplo: tabla FORM_INSTANCIA900_DESGLOSE)
                    'Mal:Nombre columna Incorrecto, falta padre: ALTER TABLE SPA_FORM_INSTANCIA900_DESGLOSE ... C_0_123456 ...
                    'Mal:Casque: SELECT ... INNER JOIN ENG_FORM_INSTANCIA900_DESGLOSE F WITH (NOLOCK) ON F.INSTANCIA ...
                    bNoExec = True
                End If
                   
                If bNoExec = False Then
                            
                    If sAccion = "I" Then
                        If iCampoPadre = 0 Then
                            sSQL = "ALTER TABLE " & sNombreTablaCompleto & " ADD C_" & iTipoCampoGS & "_" & iIdCampo
                        Else
                            sSQL = "ALTER TABLE " & sNombreTablaCompleto & " ADD C_" & iCampoPadre & "_" & iTipoCampoGS & "_" & iIdCampo
                        End If
                    Else
                        If iCampoPadre = 0 Then
                            sSQL = "ALTER TABLE " & sNombreTablaCompleto & " ALTER COLUMN C_" & iTipoCampoGS & "_" & iIdCampo
                        Else
                            sSQL = "ALTER TABLE " & sNombreTablaCompleto & " ALTER COLUMN C_" & iCampoPadre & "_" & iTipoCampoGS & "_" & iIdCampo
                        End If
                    End If
                    
                    Select Case iSubtipo
                        Case Subtipos.TipoNumerico
                            If (iTipoCampoGS = 0 Or iTipoCampoGS < 100 Or iTipoCampoGS = 127) And bTablaExterna And Not bEsLista Then
                                sSQL = sSQL & " FLOAT"
                            Else
                                sSQL = sSQL & " NVARCHAR(400)"
                                If iCampoPadre = 0 Then
                                    If sAccion = "I" Then
                                        sSQL = sSQL & " ,C_" & iTipoCampoGS & "_" & iIdCampo & "_COD NVARCHAR(400)"
                                    Else
                                        sSQL = sSQL & vbCrLf
                                        sSQL = sSQL & "ALTER TABLE " & sNombreTablaCompleto & " ALTER COLUMN C_" & iTipoCampoGS & "_" & iIdCampo & "_COD NVARCHAR(400)" & vbCrLf
                                    End If
                                Else
                                    If sAccion = "I" Then
                                        sSQL = sSQL & " ,C_" & iCampoPadre & "_" & iTipoCampoGS & "_" & iIdCampo & "_COD NVARCHAR(400)"
                                    Else
                                        sSQL = sSQL & vbCrLf
                                        sSQL = sSQL & "ALTER TABLE " & sNombreTablaCompleto & " ALTER COLUMN C_" & iCampoPadre & "_" & iTipoCampoGS & "_" & iIdCampo & "_COD NVARCHAR(400)" & vbCrLf
                                    End If
                                End If
                            End If
                        Case Subtipos.TipoFecha
                            sSQL = sSQL & " DATETIME"
                        Case Subtipos.TipoBoolean
                            sSQL = sSQL & " TINYINT"
                        Case Subtipos.TipoTextoCorto
                            sSQL = sSQL & " NVARCHAR(100)"
                            If iCampoPadre = 0 Then
                                If sAccion = "I" Then
                                    sSQL = sSQL & ",C_" & iTipoCampoGS & "_" & iIdCampo & "_COD NVARCHAR(100)"
                                Else
                                    sSQL = sSQL & vbCrLf
                                    sSQL = sSQL & "ALTER TABLE " & sNombreTablaCompleto & " ALTER COLUMN C_" & iTipoCampoGS & "_" & iIdCampo & "_COD NVARCHAR(100)" & vbCrLf
                                End If
                            Else
                                If sAccion = "I" Then
                                    sSQL = sSQL & ",C_" & iCampoPadre & "_" & iTipoCampoGS & "_" & iIdCampo & "_COD NVARCHAR(100)"
                                Else
                                    sSQL = sSQL & vbCrLf
                                    sSQL = sSQL & "ALTER TABLE " & sNombreTablaCompleto & " ALTER COLUMN C_" & iCampoPadre & "_" & iTipoCampoGS & "_" & iIdCampo & "_COD NVARCHAR(100)" & vbCrLf
                                End If
                            End If
                        Case Subtipos.TipoTextoMedio
                            sSQL = sSQL & " NVARCHAR(800)"
                            If iCampoPadre = 0 Then
                                If sAccion = "I" Then
                                    sSQL = sSQL & ",C_" & iTipoCampoGS & "_" & iIdCampo & "_COD NVARCHAR(800)"
                                Else
                                    sSQL = sSQL & vbCrLf
                                    sSQL = sSQL & "ALTER TABLE " & sNombreTablaCompleto & " ALTER COLUMN C_" & iTipoCampoGS & "_" & iIdCampo & "_COD NVARCHAR(800)" & vbCrLf
                                End If
                            Else
                                If sAccion = "I" Then
                                    sSQL = sSQL & ",C_" & iCampoPadre & "_" & iTipoCampoGS & "_" & iIdCampo & "_COD NVARCHAR(800)"
                                Else
                                    sSQL = sSQL & vbCrLf
                                    sSQL = sSQL & "ALTER TABLE " & sNombreTablaCompleto & " ALTER COLUMN C_" & iCampoPadre & "_" & iTipoCampoGS & "_" & iIdCampo & "_COD NVARCHAR(800)" & vbCrLf
                                End If
                            End If
                        Case Subtipos.TipoTextoLargo, Subtipos.TipoEditor
                            sSQL = sSQL & " NVARCHAR(MAX)"
                            If iCampoPadre = 0 Then
                                If sAccion = "I" Then
                                    sSQL = sSQL & ",C_" & iTipoCampoGS & "_" & iIdCampo & "_COD NVARCHAR(MAX)"
                                Else
                                    sSQL = sSQL & vbCrLf
                                    sSQL = sSQL & "ALTER TABLE " & sNombreTablaCompleto & " ALTER COLUMN C_" & iTipoCampoGS & "_" & iIdCampo & "_COD NVARCHAR(MAX)" & vbCrLf
                                End If
                            Else
                                If sAccion = "I" Then
                                    sSQL = sSQL & ",C_" & iCampoPadre & "_" & iTipoCampoGS & "_" & iIdCampo & "_COD NVARCHAR(MAX)"
                                Else
                                    sSQL = sSQL & vbCrLf
                                    sSQL = sSQL & "ALTER TABLE " & sNombreTablaCompleto & " ALTER COLUMN C_" & iCampoPadre & "_" & iTipoCampoGS & "_" & iIdCampo & "_COD NVARCHAR(MAX)" & vbCrLf
                                End If
                            End If
                        Case Subtipos.TipoArchivo
                            sSQL = sSQL & " NVARCHAR(800)"
                    End Select
                    
                    m_oConexion.ADOCon.Execute sSQL
                    If m_oConexion.ADOCon.Errors.Count > 0 Then
                        GoTo ERROR_SALIR
                    End If
                    
                    'Puede que haya instancias con este campo ya incluido. Actualizamos el valor correspondiente
                    If iCampoPadre = 0 Then
                        sSQL = "SELECT C.ID,C.VALOR_TEXT_DEN,ISNULL(FC.TIPO_CAMPO_GS,0) AS TIPO_CAMPO_GS,I.ID AS INSTANCIA,C."
                    Else
                        sSQL = "SELECT C.ID,CLD.VALOR_TEXT_DEN,ISNULL(FC.TIPO_CAMPO_GS,0) AS TIPO_CAMPO_GS,I.ID AS INSTANCIA,CLD."
                    End If
                    Select Case iSubtipo
                        Case Subtipos.TipoNumerico
                            sSQL = sSQL & "VALOR_NUM "
                        Case Subtipos.TipoFecha
                            sSQL = sSQL & "VALOR_FEC "
                        Case Subtipos.TipoBoolean
                            sSQL = sSQL & "VALOR_BOOL "
                        Case Else
                            sSQL = sSQL & "VALOR_TEXT "
                    End Select
                    sSQL = sSQL & "AS CODIGO FROM FORM_CAMPO FC WITH(NOLOCK) "
    
                    sSQL = sSQL & "INNER JOIN COPIA_CAMPO C WITH(NOLOCK) ON FC.ID=C.COPIA_CAMPO_DEF "
                    If Not iCampoPadre = 0 Then
                        sSQL = sSQL & "INNER JOIN COPIA_LINEA_DESGLOSE CLD WITH(NOLOCK) ON CLD.CAMPO_HIJO=C.ID "
                    End If
                    sSQL = sSQL & "INNER JOIN INSTANCIA I WITH(NOLOCK) ON C.INSTANCIA=I.ID AND C.NUM_VERSION=I.ULT_VERSION "
                    If iCampoPadre = 0 Then
                        sSQL = sSQL & "INNER JOIN " & sNombreTablaCompleto & " F WITH (NOLOCK) ON F.INSTANCIA = I.ID "
                    Else
                        sSQL = sSQL & "INNER JOIN " & sNombreTablaCompleto & " F WITH (NOLOCK) ON F.FORM_INSTANCIA = I.ID "
                    End If
                    sSQL = sSQL & "WHERE FC.CAMPO_ORIGEN = " & iIdCampo
                    Select Case iSubtipo
                        Case Subtipos.TipoNumerico
                            If iCampoPadre = 0 Then
                                sSQL = sSQL & " AND C.VALOR_NUM IS NOT NULL "
                            Else
                                sSQL = sSQL & " AND CLD.VALOR_NUM IS NOT NULL "
                            End If
                        Case Subtipos.TipoFecha
                            If iCampoPadre = 0 Then
                                sSQL = sSQL & " AND C.VALOR_FEC IS NOT NULL "
                            Else
                                sSQL = sSQL & " AND CLD.VALOR_FEC IS NOT NULL "
                            End If
                        Case Subtipos.TipoBoolean
                            If iCampoPadre = 0 Then
                                sSQL = sSQL & " AND C.VALOR_BOOL IS NOT NULL "
                            Else
                                sSQL = sSQL & " AND CLD.VALOR_BOOL IS NOT NULL "
                            End If
                        Case Subtipos.TipoArchivo, Subtipos.TipoEditor
                            If iCampoPadre = 0 Then
                                sSQL = sSQL & " AND C.VALOR_TEXT IS NOT NULL "
                            Else
                                sSQL = sSQL & " AND CLD.VALOR_TEXT IS NOT NULL "
                            End If
                        Case Else
                            If iCampoPadre = 0 Then
                                sSQL = sSQL & " AND C.VALOR_TEXT IS NOT NULL "
                            Else
                                sSQL = sSQL & " AND CLD.VALOR_TEXT IS NOT NULL "
                            End If
                    End Select
                    
                    Set adoRes = New ADODB.Recordset
                    adoRes.Open sSQL, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
                    
                    Do While Not adoRes.eof
                        Select Case iSubtipo
                            Case Subtipos.TipoNumerico, Subtipos.TipoBoolean, Subtipos.TipoArchivo, Subtipos.TipoEditor
                                If iCampoPadre = 0 Then
                                    sSQL = "UPDATE " & sNombreTablaCompleto & " SET C_" & iTipoCampoGS & "_" & iIdCampo & "=" & StrToSQLNULL(adoRes("CODIGO"))
                                Else
                                    sSQL = "UPDATE " & sNombreTablaCompleto & " SET C_" & iCampoPadre & "_" & iTipoCampoGS & "_" & iIdCampo & "=" & StrToSQLNULL(adoRes("CODIGO"))
                                End If
                            Case Subtipos.TipoFecha
                                If iCampoPadre = 0 Then
                                    sSQL = "UPDATE " & sNombreTablaCompleto & " SET C_" & iTipoCampoGS & "_" & iIdCampo & "=" & DateToSQLTimeDate(adoRes("CODIGO"))
                                Else
                                    sSQL = "UPDATE " & sNombreTablaCompleto & " SET C_" & iCampoPadre & "_" & iTipoCampoGS & "_" & iIdCampo & "=" & DateToSQLTimeDate(adoRes("CODIGO"))
                                End If
                            Case Else
                                If NullToStr(adoRes("VALOR_TEXT_DEN")) <> "" Then
                                    sDenominacion = ObtenerDenominacionParaFiltro(mIdioma.Item(iIdioma), adoRes("TIPO_CAMPO_GS"), adoRes("CODIGO"), adoRes("VALOR_TEXT_DEN"))
                                Else
                                    sDenominacion = NullToStr(adoRes("CODIGO"))
                                End If
                                If iCampoPadre = 0 Then
                                    sSQL = "UPDATE " & sNombreTablaCompleto & " SET C_" & iTipoCampoGS & "_" & iIdCampo & "=" & StrToSQLNULL(sDenominacion)
                                    sSQL = sSQL & ",C_" & iTipoCampoGS & "_" & iIdCampo & "_COD=" & StrToSQLNULL(adoRes("CODIGO"))
                                Else
                                    sSQL = "UPDATE " & sNombreTablaCompleto & " SET C_" & iCampoPadre & "_" & iTipoCampoGS & "_" & iIdCampo & "=" & StrToSQLNULL(sDenominacion)
                                    sSQL = sSQL & ",C_" & iCampoPadre & "_" & iTipoCampoGS & "_" & iIdCampo & "_COD=" & StrToSQLNULL(adoRes("CODIGO"))
                                End If
                        End Select
                        
                        If iCampoPadre = 0 Then
                            sSQL = sSQL & " WHERE INSTANCIA=" & adoRes("INSTANCIA")
                        Else
                            sSQL = sSQL & " WHERE FORM_INSTANCIA=" & adoRes("INSTANCIA")
                        End If
                        m_oConexion.ADOCon.Execute sSQL
                        If m_oConexion.ADOCon.Errors.Count > 0 Then
                            GoTo ERROR_SALIR
                        End If
                    
                        adoRes.MoveNext
                    Loop
                End If
                
            Case Else
                GoTo ERROR_SALIR
        End Select
    Next
            
    ActualizarVisorPM = IIf(bNoExec, 0, 1)
    Exit Function
    
ERROR_SALIR:
    ActualizarVisorPM = -1
End Function

'****************************************************************************************************************
'*** Descripci�n: Grabamos en UPD_VISORES_PM la descripci�n de los errores y la fecha.                      *****
'*** Llamada desde: ActualizarVisoresPM                                                                     *****
'****************************************************************************************************************
Private Sub GrabarErrorVisorPM(ByVal sErrorStr As String, ByVal iID As Long)
    Dim adoComm As ADODB.Command
    
    Set adoComm = New ADODB.Command
    adoComm.ActiveConnection = m_oConexion.ADOCon
    adoComm.CommandType = adCmdText
    adoComm.CommandText = "UPDATE UPD_VISORES_PM SET FECLASTTRY = ?, ERROR = ? WHERE ID = ?"
    adoComm.Parameters.Append adoComm.CreateParameter("FECLASTTRY", adDate, adParamInput, Value:=Now)
    adoComm.Parameters.Append adoComm.CreateParameter("ERROR", adVarChar, adParamInput, 200, sErrorStr)
    adoComm.Parameters.Append adoComm.CreateParameter("ID", adInteger, adParamInput, Value:=iID)
    adoComm.Execute
    
End Sub

'****************************************************************************************************************
'*** Descripci�n: Para cada error de la colecci�n cErrores escribimos el n�mero de error y su descripci�n.  *****
'*** Llamada desde: ActualizarVisoresPM                                                                     *****
'****************************************************************************************************************
Private Function ObtenerError(ByVal cErrores As ADODB.Errors)
    Dim oError As ADODB.Error
    Dim sErrorStr As String
    
    sErrorStr = ""
    
    For Each oError In cErrores
        sErrorStr = sErrorStr & "N� Error: " & oError.Number & "--"
        sErrorStr = sErrorStr & "Descripci�n: " & oError.Description
    Next
    If Len(sErrorStr) > 200 Then
        sErrorStr = Left(sErrorStr, 200)
    End If
    
    ObtenerError = sErrorStr
End Function

'****************************************************************************************************************
'*** Descripci�n: Carga los idiomas en una colecci�n una sola vez .                                         *****
'*** Llamada desde: ActualizarVisoresPM                                                                     *****
'****************************************************************************************************************
Private Sub CargarIdiomasAplicacion()
    Dim rs As New ADODB.Recordset
    Dim sConsulta As String
    
    sConsulta = "SELECT COD FROM IDIOMAS WITH(NOLOCK) WHERE APLICACION=1"
    rs.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    
    While Not rs.eof
        mIdioma.Add rs.Fields("COD").Value
        rs.MoveNext
    Wend
    rs.Close
End Sub


