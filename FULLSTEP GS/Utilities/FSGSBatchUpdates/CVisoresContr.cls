VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CVisoresContr"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Private Enum Subtipos
    TipoString = 1
    TipoNumerico = 2
    TipoFecha = 3
    TipoBoolean = 4
    TipoTextoCorto = 5
    TipoTextoMedio = 6
    TipoTextoLargo = 7
    TipoArchivo = 8
    TipoDesglose = 9
    TipoEditor = 15
End Enum

Private m_oConexion As CConexion 'local copy
Private mIdioma As New Collection

Friend Property Set Conexion(ByVal vData As CConexion)
    Set m_oConexion = vData
End Property
Friend Property Get Conexion() As CConexion
    Set Conexion = m_oConexion
End Property
Private Sub Class_Terminate()
    Set m_oConexion = Nothing
End Sub

'*****************************************************************************************************
'*** Descripci�n: Lleva a cabo las actualizaciones sobre las tablas de filtros din�micas. Para   *****
'*** ello accede a la tabla UPD_VISORES_CONTR.                                                      *****
'*** Llamada desde: oBatch.ActualizarDenominaciones                                            *****
'*****************************************************************************************************
Public Function ActualizarVisoresContr() As Long
    Dim sConsulta As String
    Dim adoRes As Recordset
    Dim sError As String
            
    On Error GoTo ERROR_SALIR
    
    ActualizarVisoresContr = 1
    
    CargarIdiomasAplicacion
    
    sConsulta = "SELECT ID, ACCION, ID_TABLA, ID_CAMPO, NOM_TABLA, SUBTIPO FROM UPD_VISORES_CONTR WITH (NOLOCK) WHERE OK=0 AND ERROR IS NULL ORDER BY ID"
    Set adoRes = New ADODB.Recordset
    adoRes.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    
    Do While Not adoRes.eof
    
        m_oConexion.ADOCon.Execute "SET XACT_ABORT OFF"
        m_oConexion.ADOCon.Execute "BEGIN TRANSACTION", , adExecuteNoRecords
    
        If ActualizarVisorContr(NullToStr(adoRes.Fields("ACCION").Value), NullToDbl0(adoRes.Fields("ID_TABLA").Value), NullToDbl0(adoRes.Fields("ID_CAMPO").Value), NullToDbl0(adoRes.Fields("SUBTIPO").Value), NullToStr(adoRes.Fields("NOM_TABLA").Value)) Then
            'OK
            m_oConexion.ADOCon.Execute "UPDATE UPD_VISORES_CONTR SET OK=1 WHERE ID=" & adoRes.Fields("ID").Value
            If m_oConexion.ADOCon.Errors.Count > 0 Then _
                        GoTo ERROR_SALIR
            m_oConexion.ADOCon.Execute "COMMIT TRANSACTION", , adExecuteNoRecords
        Else
            'NOOK
            sError = ObtenerError(m_oConexion.ADOCon.Errors)
            m_oConexion.ADOCon.Execute "IF @@TRANCOUNT>0 ROLLBACK TRANSACTION"
            GrabarErrorVisorContr sError, adoRes.Fields("ID").Value
        End If
        
        adoRes.MoveNext
    Loop
                        
    adoRes.Close
    Set adoRes = Nothing
    Exit Function
    
ERROR_SALIR:
    m_oConexion.ADOCon.Execute "IF @@TRANCOUNT>0 ROLLBACK TRANSACTION"
    If Not adoRes Is Nothing Then
        adoRes.Close
        Set adoRes = Nothing
    End If
    ActualizarVisoresContr = 0
End Function

''' <summary>Actualizar la tabla (a�adir campo, eliminar campo, borrar la tabla) pasada como par�metro</summary>
''' <param name="sAcci�n">la acci�n a realizar sobre la tabla (D,U,I)</param>
''' <param name="iIdTabla">el id de la tabla almacenada en PM_CONTR_FILTROS</param>
''' <param name="iIdCampo">el id del campo a a�adir o eliminar</param>
''' <param name="iSubtipo">el subtipo del campo a a�adir (num�rico, boolean, fecha,...)</param>
''' <param name="sNombreTabla">el nombre de la tabla sobre la que hay que trabajar + PRE idioma</param>
''' <returns>Si la actualizaci�n ha ido bien, devuelve TRUE. Si ha habido errores FALSE</returns>
''' <remarks>Llamada desde: ActualizarVisoresContr</remarks>
''' <revision>LTG 12/01/2012</revision>

Private Function ActualizarVisorContr(ByVal sAccion As String, ByVal iIdTabla As Long, ByVal iIdCampo As Long, ByVal iSubtipo As Integer, _
        ByVal sNombreTabla As String) As Boolean
    Dim sSQL As String
    Dim sNombreTablaCompleto As String
    Dim iIdioma As Integer
    
    On Error GoTo ERROR_SALIR
    
    For iIdioma = 1 To mIdioma.Count
        sNombreTablaCompleto = mIdioma.Item(iIdioma) & "_" & sNombreTabla
           
        Select Case sAccion
            Case "D"    'se elimina la tabla indicada en sNombreTablaCompleto
                sSQL = "DROP TABLE " & sNombreTablaCompleto
            Case "U"    'se elimina de la tabla (sNombreTablaCompleto) el campo (iIdCampo)
                sSQL = "ALTER TABLE " & sNombreTablaCompleto & " DROP COLUMN C_" & iIdCampo
            Case "I", "A"   'I: se a�ade a la tabla (sNombreTablaCompleto) un campo nuevo (iIdCampo)
                            'A: se modifica un campo (iIdCampo) de la tabla (sNombreTablaCompleto)
                            '(ej: de texto corto a texto largo)
                If sAccion = "I" Then
                    sSQL = "ALTER TABLE " & sNombreTablaCompleto & " ADD C_" & iIdCampo
                Else
                    sSQL = "ALTER TABLE " & sNombreTablaCompleto & " ALTER COLUMN C_" & iIdCampo
                End If
                Select Case iSubtipo
                    Case Subtipos.TipoNumerico
                        sSQL = sSQL & " FLOAT"
                    Case Subtipos.TipoFecha
                        sSQL = sSQL & " DATETIME"
                    Case Subtipos.TipoBoolean
                        sSQL = sSQL & " TINYINT"
                    Case Subtipos.TipoTextoCorto
                        sSQL = sSQL & " NVARCHAR(100)"
                    Case Subtipos.TipoTextoMedio
                        sSQL = sSQL & " NVARCHAR(800)"
                    Case Subtipos.TipoTextoLargo, Subtipos.TipoEditor
                        sSQL = sSQL & " NVARCHAR(MAX)"
                    Case Subtipos.TipoArchivo
                        sSQL = sSQL & " NVARCHAR(800)"
                End Select
                
                m_oConexion.ADOCon.Execute sSQL
                If m_oConexion.ADOCon.Errors.Count > 0 Then
                    GoTo ERROR_SALIR
                End If
                
                'Puede que haya instancias con este campo ya incluido. Actualizamos el valor correspondiente
                sSQL = "UPDATE " & sNombreTablaCompleto & " SET C_" & iIdCampo & "=C."
                Select Case iSubtipo
                    Case Subtipos.TipoNumerico
                        sSQL = sSQL & "VALOR_NUM"
                    Case Subtipos.TipoFecha
                        sSQL = sSQL & "VALOR_FEC"
                    Case Subtipos.TipoBoolean
                        sSQL = sSQL & "VALOR_BOOL"
                    Case Else
                        sSQL = sSQL & "VALOR_TEXT"
                End Select
                
                sSQL = sSQL & " FROM FORM_CAMPO FC WITH(NOLOCK) "
                sSQL = sSQL & "INNER JOIN COPIA_CAMPO C WITH(NOLOCK) ON FC.ID=C.COPIA_CAMPO_DEF "
                sSQL = sSQL & "INNER JOIN INSTANCIA I WITH(NOLOCK) ON C.INSTANCIA=I.ID AND C.NUM_VERSION=I.ULT_VERSION "
                sSQL = sSQL & "INNER JOIN " & sNombreTablaCompleto & " F WITH(NOLOCK) ON F.INSTANCIA = I.ID "
                sSQL = sSQL & "WHERE FC.CAMPO_ORIGEN = " & iIdCampo
            Case Else
                GoTo ERROR_SALIR
        End Select
        
        m_oConexion.ADOCon.Execute sSQL
        If m_oConexion.ADOCon.Errors.Count > 0 Then
            GoTo ERROR_SALIR
        End If
    Next
            
    ActualizarVisorContr = True
    Exit Function
    
ERROR_SALIR:
    ActualizarVisorContr = False
End Function

'****************************************************************************************************************
'*** Descripci�n: Grabamos en UPD_VISORES_CONTR la descripci�n de los errores y la fecha.                   *****
'*** Llamada desde: ActualizarVisoresCONTR                                                                  *****
'****************************************************************************************************************
Private Sub GrabarErrorVisorContr(ByVal sErrorStr As String, ByVal iID As Long)
    Dim adoComm As ADODB.Command
    
    Set adoComm = New ADODB.Command
    adoComm.ActiveConnection = m_oConexion.ADOCon
    adoComm.CommandType = adCmdText
    adoComm.CommandText = "UPDATE UPD_VISORES_CONTR SET FECLASTTRY = ?, ERROR = ? WHERE ID = ?"
    adoComm.Parameters.Append adoComm.CreateParameter("FECLASTTRY", adDate, adParamInput, Value:=Now)
    adoComm.Parameters.Append adoComm.CreateParameter("ERROR", adVarChar, adParamInput, 200, sErrorStr)
    adoComm.Parameters.Append adoComm.CreateParameter("ID", adInteger, adParamInput, Value:=iID)
    adoComm.Execute
    
End Sub

'****************************************************************************************************************
'*** Descripci�n: Para cada error de la colecci�n cErrores escribimos el n�mero de error y su descripci�n.  *****
'*** Llamada desde: ActualizarVisoresContr                                                                  *****
'****************************************************************************************************************
Private Function ObtenerError(ByVal cErrores As ADODB.Errors)
    Dim oError As ADODB.Error
    Dim sErrorStr As String
    
    sErrorStr = ""
    
    For Each oError In cErrores
        sErrorStr = sErrorStr & "N� Error: " & oError.Number & "--"
        sErrorStr = sErrorStr & "Descripci�n: " & oError.Description
    Next
    If Len(sErrorStr) > 200 Then
        sErrorStr = Left(sErrorStr, 200)
    End If
    
    ObtenerError = sErrorStr
End Function

''' <summary>
''' Carga los idiomas en una colecci�n una sola vez .
''' </summary>
''' <remarks>Llamada desde:=ActualizarVisoresContr; Tiempo m�ximo=0,1</remarks>
Private Sub CargarIdiomasAplicacion()
    Dim rs As New ADODB.Recordset
    Dim sConsulta As String
    
    sConsulta = "SELECT COD FROM IDIOMAS WITH(NOLOCK) WHERE APLICACION=1"
    rs.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    
    While Not rs.eof
        mIdioma.Add rs.Fields("COD").Value
        rs.MoveNext
    Wend
    rs.Close
End Sub
