Attribute VB_Name = "basPublic"
Option Explicit

'Constantes
Public Const ClaveUSUpar = "agkag�"
Public Const ClaveUSUimp = "h+hlL_"

Public Const gsDateFMT As String = "dd/mm/yyyy"
Public Const gsDecimalFMT As String = ","
Public Const gsThousandFMT As String = "."
Public Const gsPrecisionFMT As Integer = 2
Public Const giTipoEmail As Integer = 0
Public g_sInstancia As String



Function DateToSQLDate(DateThatCanBeEmpty) As Variant


If IsEmpty(DateThatCanBeEmpty) Then
    DateToSQLDate = "NULL"
Else
    If IsNull(DateThatCanBeEmpty) Then
        DateToSQLDate = "NULL"
    Else
        If DateThatCanBeEmpty = "" Then
            DateToSQLDate = "NULL"
        Else
             DateToSQLDate = "Convert(datetime,'" & Format(DateThatCanBeEmpty, "mm-dd-yyyy") & "',110)"
        End If
    End If
End If

End Function
Function DateToSQLTimeDate(DateThatCanBeEmpty) As Variant


If IsEmpty(DateThatCanBeEmpty) Then
    DateToSQLTimeDate = "NULL"
Else
    If IsNull(DateThatCanBeEmpty) Then
        DateToSQLTimeDate = "NULL"
    Else
        If DateThatCanBeEmpty = "" Then
            DateToSQLTimeDate = "NULL"
        Else
            DateToSQLTimeDate = "Convert(datetime,'" & Format(DateThatCanBeEmpty, "mm-dd-yyyy hh:mm:ss") & "',110)"
        End If
    End If
End If

End Function

Public Function SQLBinaryToBoolean(ByVal var As Variant) As Variant
    
    If IsNull(var) Then
        SQLBinaryToBoolean = False
    Else
        If var = 1 Then
            SQLBinaryToBoolean = True
        Else
            SQLBinaryToBoolean = False
        End If
    End If
    

End Function

''' <summary>Indica si hay acceso a GS </summary>'
''' <returns>Boolean indicando si hay acceso a GS</returns>
''' <remarks>Llamada desde=; Tiempo m�ximo=0,61seg.</remarks>

Public Function AccesoGS(ByVal adoCon As ADODB.Connection) As Boolean
    Dim sConsulta As String
    Dim adoRes As Recordset
    Dim adoComm As ADODB.Command
    Dim adoParam As ADODB.Parameter
    
    AccesoGS = False
    
    Set adoComm = New ADODB.Command
    With adoComm
        sConsulta = "SELECT ACCESO_FSGS FROM PARGEN_INTERNO WITH (NOLOCK) WHERE ID=1"
        
        .ActiveConnection = adoCon
        .CommandType = adCmdText
        .CommandText = sConsulta
        .CommandTimeout = 120
        
        Set adoRes = .Execute
    End With
    
    ' El contenido del par�metro ACCESO_FSGS se encripta mediante la funci�n EncriptarAccesoFSWS
    'gParametrosGenerales.gbAccesoFSGS = SQLBinaryToBoolean(AdoRes("ACCESO_FSGS").Value)
    If EncriptarAccesoFSWS("GS", NullToStr(adoRes("ACCESO_FSGS").Value), False) = "FULLSTEP GS" Then AccesoGS = True
    
    Set adoRes = Nothing
    Set adoComm = Nothing
    Set adoParam = Nothing
End Function

''' <summary>Encripta/desencripta un dato</summary>
''' <param name="sCadena">cadena para encriptar/desencriptar</param>
''' <param name="sDato">Dato a encriptar/desencriptar</param>
''' <param name="Encriptando">Indica encriptaci�n � desencriptaci�n</param>
''' <returns>dato encriptado/desencriptado</returns>
''' <remarks>Llamada desde=AccesoGS; Tiempo m�ximo=0,61seg.</remarks>
Private Function EncriptarAccesoFSWS(ByVal sCadena As String, ByVal sDato As String, ByVal Encriptando As Boolean) As String
    Dim oCrypt2 As cCrypt2
        
    Set oCrypt2 = New cCrypt2
    
    oCrypt2.InBuffer = sDato
    If Len(sCadena) Mod 2 = 0 Then
        oCrypt2.Codigo = ClaveUSUpar
    Else
        oCrypt2.Codigo = ClaveUSUimp
    End If
    oCrypt2.Fecha = DateSerial(1974, 3, 5)
    
    If Encriptando Then
        oCrypt2.Encrypt
    Else
        oCrypt2.Decrypt
    End If
            
    EncriptarAccesoFSWS = oCrypt2.OutBuffer
    
    Set oCrypt2 = Nothing
End Function

