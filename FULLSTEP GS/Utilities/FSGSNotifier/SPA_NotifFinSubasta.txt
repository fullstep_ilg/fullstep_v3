Le recordamos que ha finalizado la subasta cuyos datos le presentamos a continuaci�n.
As� mismo le informamos de que ning�n �tem le ha sido adjudicado.

	Datos de la cia proveedora
	------------------------------------------------------
  	C�digo:		@COD_CIA
  	Denominaci�n: 	@DEN_CIA

	Datos del proveedor 
	------------------------------------------------------
	Nombre:		@NOM_PROVE
  	Apellidos:		@APE_PROVE
  	Cargo:		@CAR_PROVE
  	Tel�fono:		@TFNO1_PROVE
	Tel�fono 2:		@TFNO2_PROVE
	Fax:			@FAX_PROVE
	E-mail:		@EMAIL_PROVE
	Departamento:	@DEP_PROVE

	Datos del proceso
	------------------------------------------------------
	A�o:				@ANYO_PROCE
	Gmn1:			@GMN1_PROCE
	C�digo:			@COD_PROCE
	Denominaci�n:		@DEN_PROCE
	Material de 1er nivel:	@COD_GMN1 - @DEN_GMN1
	Material de 2� nivel:	@COD_GMN2 - @DEN_GMN2
	Material de 3er nivel:	@COD_GMN3 - @DEN_GMN3
	Material de 4� nivel:	@COD_GMN4 - @DEN_GMN4
	Material:			@MATERIAL

	Datos de la subasta
	------------------------------------------------------
	Fecha de apertura:	@FEC_APE
	Hora de apertura:		@HORA_APE
	Fecha UTC de apertura:	@FEC_UTC_APE
	Hora UTC de apertura:	@HORA_UTC_APE
	Fecha de cierre:		@FEC_CIERRE
	Hora de cierre:		@HORA_CIERRE
	Fecha UTC de cierre:	@FEC_UTC_CIERRE
	Hora UTC de cierre:	@HORA_UTC_CIERRE

<!--INICIO COMPRADOR-->
	Datos del comprador responsable 
	------------------------------------------------------
	Nombre: 	@NOM_COM
	Apellidos: 	@APE_COM
	E-mail:  	@EMAIL_COM
	Tel�fono: 	@TFNO1_COM
	Tel�fono 2:	@TFNO2_COM
	Fax:		@FAX_COM
	Cargo:	@CARGO_COM
<!--FIN COMPRADOR-->

Un saludo