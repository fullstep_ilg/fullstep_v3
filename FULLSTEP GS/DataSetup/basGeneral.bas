Attribute VB_Name = "basGeneral"
Public Const ClaveSQLtod = "aldj�w"
Public Const ClaveOBJtod = "jldfad"
Public Const ClaveADMpar = "pruiop"
Public Const ClaveADMimp = "ljkdag"
Public Const ClaveUSUpar = "agkag�"
Public Const ClaveUSUimp = "h+hlL_"

Public Const ClaveUsuPortalpar = "agkag�"
Public Const ClaveUsuPortalimpar = "h+hlL_"
Public Const ClaveCiaPortalpar = "hgkog�"
Public Const ClaveCiaPortalimpar = "j+klL_"

Public Enum TipoDeUsuario
    Administrador = 1
    Comprador = 2
    Persona = 3
    Proveedor = 4
'    UsuarioExterno = 5
End Enum

Public Function EncriptarAccesoFSWS(ByVal sCadena As String, ByVal sDato As String, ByVal Encriptando As Boolean) As String
    Dim oCrypt2 As cCrypt2
        
    Set oCrypt2 = New cCrypt2
    
    oCrypt2.InBuffer = sDato
    If Len(sCadena) Mod 2 = 0 Then
        oCrypt2.Codigo = ClaveUSUpar
    Else
        oCrypt2.Codigo = ClaveUSUimp
    End If
    oCrypt2.Fecha = DateSerial(1974, 3, 5)
    
    If Encriptando Then
        oCrypt2.Encrypt
    Else
        oCrypt2.Decrypt
    End If
            
    EncriptarAccesoFSWS = oCrypt2.OutBuffer
    
    Set oCrypt2 = Nothing
    
End Function

Public Function SustituirCaracteresInvalidos(ByVal S As String, ByVal Encriptar As Boolean) As String
Dim temp1 As String
Dim i As Integer
Dim bSumar As Boolean

    temp1 = ""
    
    If Encriptar Then
    
        temp1 = Replace(S, Chr(0), Chr(255) & Chr(255) & Chr(255))
    
    Else
        
        temp1 = Replace(S, Chr(255) & Chr(255) & Chr(255), Chr(0))
        
    End If

    SustituirCaracteresInvalidos = temp1

End Function

Public Function DblQuote(ByVal StrToDblQuote As String) As String

    Dim Ind As Long
    Dim QPos As Long
    Dim pos As Long
    
    Dim TmpDblQuote As String
    
    TmpDblQuote = ""
    pos = 1
    
    Do While Len(StrToDblQuote) > 0
    
        QPos = InStr(pos, StrToDblQuote, "'")
        
        If QPos > 0 Then
            
            TmpDblQuote = TmpDblQuote & Mid(StrToDblQuote, pos, QPos - 1) & "''"
            StrToDblQuote = Right(StrToDblQuote, Len(StrToDblQuote) - QPos)
            
        Else
        
            TmpDblQuote = TmpDblQuote & StrToDblQuote
            Exit Do
            
        End If
            
    Loop
    
    DblQuote = TmpDblQuote
    
End Function


Function StrToSQLNULL(StrThatCanBeEmpty) As Variant
    
    If IsEmpty(StrThatCanBeEmpty) Then
        StrToSQLNULL = "NULL"
    Else
        If IsNull(StrThatCanBeEmpty) Then
            StrToSQLNULL = "NULL"
        Else
            If StrThatCanBeEmpty = "" Then
                StrToSQLNULL = "NULL"
            Else
                StrToSQLNULL = "'" & DblQuote(CStr(StrThatCanBeEmpty)) & "'"
            End If
        End If
    End If
    
End Function
''' <summary>Realiza la encriptaci�n/desencriptaci�n del password del usuario</summary>
''' <param name="Seed">Semilla para encriptar � desencriptar</param>
''' <param name="Dato">Dato a encriptar � desencriptar</param>
''' <param name="Encriptando">Indica si se trata de una encriptaci�n � desencriptaci�n</param>
''' <param name="Fecha">Fecha para la encriptaci�n</param>
''' <param name="Quien">Si estas trabajando con 0-licencia FSDB.LIC 1-usuarios 2-licencia FSOS.LIC</param>
''' <param name="Tipo">Tipo De Usuario</param>
''' <returns>String con el dato encriptado/desencriptado</returns>
''' <remarks>Llamada desde: frmLogin.cmdAceptar_Click; Tiempo m�ximo:1msg</remarks>
Public Function EncriptarAES(ByVal Seed As String, ByVal Dato As String, ByVal Encriptando As Boolean, ByVal Fecha As Variant _
, ByVal Quien As Integer, Optional ByVal Tipo As TipoDeUsuario) As String
    Dim fechahoracrypt As Date
    Dim diacrypt
    Dim oCrypt2 As cCrypt2
    
    If IsMissing(Fecha) Then
        Fecha = DateSerial(1974, 3, 5)
    Else
        If Fecha = "" Then
            Fecha = DateSerial(1974, 3, 5)
        End If
    End If
        
    fechahoracrypt = Fecha
    
    Set oCrypt2 = New cCrypt2
    
    oCrypt2.InBuffer = Dato
    
    If Quien = 0 Then
        oCrypt2.Codigo = ClaveSQLtod
    ElseIf Quien = 2 Then
        oCrypt2.Codigo = ClaveOBJtod
    Else
        diacrypt = Day(Fecha)
        
        If diacrypt Mod 2 = 0 Then
            If Tipo = 1 Then
                oCrypt2.Codigo = Seed & ClaveADMpar
            Else
                oCrypt2.Codigo = Seed & ClaveUSUpar
            End If
        Else
            If Tipo = 1 Then
                oCrypt2.Codigo = Seed & ClaveADMimp
            Else
                oCrypt2.Codigo = Seed & ClaveUSUimp
            End If
        End If
    End If
        
    oCrypt2.Fecha = fechahoracrypt
    
    'Datos
    If Encriptando Then
        oCrypt2.Encrypt
    Else
        oCrypt2.Decrypt
    End If
    
    EncriptarAES = oCrypt2.OutBuffer
                            
    Set oCrypt2 = Nothing
End Function



