VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmDataSetup 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FULLSTEP GS - Configuraci�n del acceso a datos"
   ClientHeight    =   6525
   ClientLeft      =   2895
   ClientTop       =   1695
   ClientWidth     =   6585
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   ForeColor       =   &H00000000&
   Icon            =   "frmDataSetup.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6525
   ScaleWidth      =   6585
   Begin VB.Frame Frame5 
      Caption         =   "Activaci�n de productos"
      Height          =   2235
      Left            =   120
      TabIndex        =   26
      Top             =   3510
      Width           =   6315
      Begin VB.CheckBox chkBI 
         Caption         =   "FULLSTEP BI"
         Height          =   195
         Left            =   240
         TabIndex        =   31
         Top             =   1860
         Width           =   1575
      End
      Begin VB.CheckBox chkContratos 
         Caption         =   "FULLSTEP CM (Contratos)"
         Height          =   195
         Left            =   3090
         TabIndex        =   30
         Top             =   1560
         Width           =   2895
      End
      Begin VB.CheckBox chkCN 
         Caption         =   "FULLSTEP CN"
         Height          =   195
         Left            =   240
         TabIndex        =   29
         Top             =   1560
         Width           =   1575
      End
      Begin VB.CheckBox chkFA 
         Caption         =   "FULLSTEP IM (Facturas)"
         Height          =   195
         Left            =   3090
         TabIndex        =   28
         Top             =   1260
         Width           =   2895
      End
      Begin VB.CheckBox chkSM 
         Caption         =   "FULLSTEP SM"
         Height          =   195
         Left            =   240
         TabIndex        =   10
         Top             =   1260
         Width           =   1575
      End
      Begin VB.CheckBox chkQA 
         Caption         =   "FULLSTEP QA B�sico"
         Height          =   195
         Left            =   3090
         TabIndex        =   11
         Top             =   360
         Width           =   2895
      End
      Begin VB.CheckBox chkQANoConformidad 
         Caption         =   "FULLSTEP QA No conformidades"
         Height          =   255
         Left            =   3360
         TabIndex        =   13
         Top             =   960
         Width           =   2700
      End
      Begin VB.CheckBox chkQACertificados 
         Caption         =   "FULLSTEP QA Certificados"
         Height          =   195
         Left            =   3360
         TabIndex        =   12
         Top             =   660
         Width           =   2700
      End
      Begin VB.CheckBox chkPMII 
         Caption         =   "FULLSTEP PM Completo"
         Height          =   195
         Left            =   240
         TabIndex        =   9
         Top             =   960
         Width           =   2895
      End
      Begin VB.CheckBox chkPMI 
         Caption         =   "FULLSTEP PM B�sico"
         Height          =   195
         Left            =   240
         TabIndex        =   8
         Top             =   660
         Width           =   2895
      End
      Begin VB.CheckBox chkGS 
         Caption         =   "FULLSTEP GS"
         Height          =   195
         Left            =   240
         TabIndex        =   7
         Top             =   360
         Width           =   2595
      End
   End
   Begin MSComDlg.CommonDialog cmmdAyuda 
      Left            =   5160
      Top             =   3960
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.Frame Frame4 
      Height          =   1175
      Left            =   150
      TabIndex        =   23
      Top             =   2310
      Width           =   6255
      Begin VB.CommandButton cmdBuscarVisor 
         Height          =   285
         Left            =   5900
         Picture         =   "frmDataSetup.frx":0442
         Style           =   1  'Graphical
         TabIndex        =   6
         TabStop         =   0   'False
         ToolTipText     =   "Examinar"
         Top             =   660
         UseMaskColor    =   -1  'True
         Width           =   315
      End
      Begin VB.CommandButton cmdBuscarFic 
         Height          =   285
         Left            =   5900
         Picture         =   "frmDataSetup.frx":0501
         Style           =   1  'Graphical
         TabIndex        =   4
         TabStop         =   0   'False
         ToolTipText     =   "Examinar"
         Top             =   240
         UseMaskColor    =   -1  'True
         Width           =   315
      End
      Begin VB.TextBox txtVisorAyuda 
         BackColor       =   &H00FFFFFF&
         ForeColor       =   &H00000000&
         Height          =   315
         Left            =   1710
         TabIndex        =   5
         Top             =   660
         Width           =   4155
      End
      Begin VB.TextBox txtFicAyuda 
         BackColor       =   &H00FFFFFF&
         ForeColor       =   &H00000000&
         Height          =   315
         Left            =   1710
         TabIndex        =   3
         Top             =   240
         Width           =   4155
      End
      Begin VB.Label Label5 
         Caption         =   "Visor para el fichero de ayuda:"
         Height          =   375
         Left            =   120
         TabIndex        =   25
         Top             =   700
         Width           =   1600
      End
      Begin VB.Label Label2 
         Caption         =   "Fichero de ayuda:"
         Height          =   255
         Left            =   120
         TabIndex        =   24
         Top             =   300
         Width           =   1600
      End
   End
   Begin VB.Frame Frame2 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   735
      Left            =   120
      TabIndex        =   19
      Top             =   0
      Width           =   6255
      Begin VB.TextBox txtInstancia 
         BackColor       =   &H00FFFFFF&
         ForeColor       =   &H00000000&
         Height          =   315
         Left            =   1710
         MaxLength       =   50
         TabIndex        =   0
         Top             =   240
         Width           =   4155
      End
      Begin VB.Label Label1 
         Caption         =   "Nombre de instancia:"
         ForeColor       =   &H00000000&
         Height          =   255
         Left            =   120
         TabIndex        =   20
         Top             =   300
         Width           =   1515
      End
   End
   Begin VB.CommandButton cmdCerrar 
      Caption         =   "&Cerrar"
      Height          =   375
      Left            =   3030
      TabIndex        =   15
      Top             =   5970
      Width           =   1335
   End
   Begin VB.CommandButton cmdAplicar 
      Caption         =   "&Aplicar"
      Default         =   -1  'True
      Height          =   375
      Left            =   1590
      TabIndex        =   14
      Top             =   5970
      Width           =   1335
   End
   Begin VB.Frame Frame1 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   1485
      Left            =   120
      TabIndex        =   16
      Top             =   810
      Width           =   6255
      Begin VB.CheckBox chkDefecto 
         Caption         =   "Actualizar valor por defecto"
         ForeColor       =   &H00000000&
         Height          =   285
         Left            =   1710
         TabIndex        =   27
         Top             =   1110
         Width           =   3195
      End
      Begin VB.OptionButton opUsuario 
         Caption         =   "S�lo para el usuario actual"
         Height          =   255
         Left            =   60
         TabIndex        =   22
         Top             =   -30
         Value           =   -1  'True
         Width           =   2235
      End
      Begin VB.OptionButton opAplicacion 
         Caption         =   "Para todos los usuarios"
         Height          =   285
         Left            =   2580
         TabIndex        =   21
         Top             =   -60
         Width           =   1965
      End
      Begin VB.TextBox txtSrv 
         BackColor       =   &H00FFFFFF&
         ForeColor       =   &H00000000&
         Height          =   315
         Left            =   1710
         MaxLength       =   50
         TabIndex        =   1
         Top             =   240
         Width           =   4155
      End
      Begin VB.TextBox txtBD 
         BackColor       =   &H00FFFFFF&
         ForeColor       =   &H00000000&
         Height          =   315
         Left            =   1710
         MaxLength       =   50
         TabIndex        =   2
         Top             =   660
         Width           =   4155
      End
      Begin VB.Label Label4 
         Caption         =   "Nombre del servidor:"
         ForeColor       =   &H00000000&
         Height          =   255
         Left            =   120
         TabIndex        =   18
         Top             =   300
         Width           =   1515
      End
      Begin VB.Label Label3 
         Caption         =   "Base de datos:"
         ForeColor       =   &H00000000&
         Height          =   315
         Left            =   120
         TabIndex        =   17
         Top             =   720
         Width           =   1635
      End
   End
End
Attribute VB_Name = "frmDataSetup"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private mInstancia As String
Private mServidor As String
Private mBaseDatos As String

Private Const ClaveSQLtod = "aldj�w45u9'2JIHO^h+'31t4mV� H hfo fdGAAFKWETJ�51FSjD>GH18071998"

Private m_oADOConnection As ADODB.Connection

Private Sub chkPMI_Click()
    
    If chkPMI.Value = vbChecked Then
        chkPMII.Value = vbUnchecked
    End If
    
    
End Sub

Private Sub chkPMII_Click()

If chkPMII.Value = vbChecked Then
    chkPMI.Value = vbUnchecked
End If

End Sub

Private Sub chkQA_Click()
    If chkQA.Value = vbUnchecked Then
        Me.chkQACertificados.Value = vbUnchecked
        Me.chkQANoConformidad.Value = vbUnchecked
    End If
End Sub

Private Sub chkQACertificados_Click()
    If chkQACertificados.Value = vbChecked Then
        Me.chkQA.Value = vbChecked
    End If
End Sub

Private Sub chkQANoConformidad_Click()
    If chkQANoConformidad.Value = vbChecked Then
        Me.chkQA.Value = vbChecked
    End If
End Sub


''' Revisado por: blp. Fecha: 31/10/2012
''' <summary>
''' Ejecuta los cambios en la instalaci�n
''' </summary>
''' <remarks>Llamada desde: evento Click del control cmdAplicar; Tiempo m�ximo: 0,1</remarks>
Private Sub cmdAplicar_Click()
    Dim KeyValue As String * 255
    Dim oFOs As FileSystemObject
    Dim ostream As TextStream
    Dim sConsulta As String
    Dim sAcceso As String
    Dim sAccesoQA As String
    Dim sAccesoSM As String
    Dim sAccesoFA As String
    Dim sAccesoGS As String
    Dim sAccesoCN As String
    Dim sAccesoContratos As String
    Dim sAccesoBI As String
    
    'Realiza las comprobaciones necesarias:
    If txtInstancia = "" Then
        MsgBox "Falta el nombre de la instancia.", vbExclamation, "Configuraci�n de la instalaci�n"
        Exit Sub
    End If
    
    If txtSrv = "" Then
        MsgBox "Falta el nombre del servidor.", vbExclamation, "Configuraci�n de la instalaci�n"
        Exit Sub
    End If
    
    If txtBD = "" Then
        MsgBox "Falta el nombre de la base de datos.", vbExclamation, "Configuraci�n de la instalaci�n"
        Exit Sub
    End If
    
    If txtFicAyuda.Text = "" Then
        MsgBox "Falta el fichero de ayuda.", vbExclamation, "Configuraci�n de la instalaci�n"
        Exit Sub
    Else
        Set oFOs = New FileSystemObject
        If oFOs.FileExists(Trim(txtFicAyuda.Text)) = False Then
            If MsgBox("El fichero de ayuda introducido no existe.�Desea continuar de todos modos?", vbExclamation + vbYesNo, "Configuraci�n de la instalaci�n ") = vbNo Then
                Set oFOs = Nothing
                Exit Sub
            End If
        End If
        If oFOs.GetExtensionName(Trim(txtFicAyuda.Text)) <> "pdf" Then
            If MsgBox("El fichero de ayuda debe ser de tipo .pdf �Desea continuar de todos modos?", vbExclamation + vbYesNo, "Configuraci�n de la instalaci�n") = vbNo Then
                Set oFOs = Nothing
                Exit Sub
            End If
            
        End If
        Set oFOs = Nothing
    End If
    
    If txtVisorAyuda.Text = "" Then
        MsgBox "Falta el visor para el fichero de ayuda.", vbExclamation, "Configuraci�n de la instalaci�n"
        Set oFOs = Nothing
        Exit Sub
    Else
        Set oFOs = New FileSystemObject
        If oFOs.FileExists(Trim(txtVisorAyuda.Text)) = False Then
            If MsgBox("El visor para el fichero de ayuda introducido no existe.�Desea continuar de todos modos?", vbExclamation + vbYesNo, "Configuraci�n de la instalaci�n") = vbNo Then
                Set oFOs = Nothing
                Exit Sub
            End If
        End If
        If oFOs.GetExtensionName(Trim(txtVisorAyuda.Text)) <> "exe" Then
            MsgBox "El visor para el fichero de ayuda debe ser un archivo de tipo .exe", vbExclamation, "Configuraci�n de la instalaci�n"
            Set oFOs = Nothing
            Exit Sub
        End If
        Set oFOs = Nothing
    End If
    
    If opUsuario.Value = True Then
        SaveStringSetting "FULLSTEP GS", txtInstancia, "Conexion", "Servidor", txtSrv
        SaveStringSetting "FULLSTEP GS", txtInstancia, "Conexion", "BaseDeDatos", txtBD
        
        'Actualizar el valor en HKEY_USERS .DEFAULT Software ...
        If chkDefecto.Value = vbChecked Then
        
        ''' Servidor
            If Not GetAppRegValueUsers("Servidor", REG_SZ, txtInstancia, KeyValue, False) Then
                If Not CreateRegEntryUsers(REG_SZ, txtInstancia, "Servidor", txtSrv) Then
                    ''' Error
                    MsgBox "Error al escribir en el registro.", vbCritical, "Configuraci�n del acceso a datos"
                    Exit Sub
                End If
                
            End If
            If Not SetAppRegValueUsers("Servidor", REG_SZ, txtInstancia, txtSrv) Then
                '''Error
                MsgBox "Error al escribir en el registro.", vbCritical, "Configuraci�n del acceso a datos"
                Exit Sub
            End If
            
            ''' BaseDeDatos
            If Not GetAppRegValueUsers("BaseDeDatos", REG_SZ, txtInstancia, KeyValue, False) Then
                If Not CreateRegEntryUsers(REG_SZ, txtInstancia, "BaseDeDatos", txtBD) Then
                    ''' Error
                    MsgBox "Error al escribir en el registro.", vbCritical, "Configuraci�n del acceso a datos"
                    Exit Sub
                End If
            End If
            If Not SetAppRegValueUsers("BaseDeDatos", REG_SZ, txtInstancia, txtBD) Then
                '''Error
                MsgBox "Error al escribir en el registro.", vbCritical, "Configuraci�n del acceso a datos"
                Exit Sub
            End If
        End If
        
    End If
    
    Screen.MousePointer = vbHourglass
    
    On Error GoTo Error:
    
    If opAplicacion.Value = True Then
        If txtInstancia <> mInstancia Or txtSrv.Text <> mServidor Or txtBD.Text <> mBaseDatos Then
            mInstancia = txtInstancia
            mServidor = Trim(txtSrv.Text)
            mBaseDatos = Trim(txtBD.Text)
            Set oFOs = New FileSystemObject
            Set ostream = oFOs.CreateTextFile(App.Path & "\FSGSClient.ini", True)
            
            ostream.WriteLine "INSTANCIA=" & mInstancia
            ostream.WriteLine "SERVIDOR=" & mServidor
            ostream.Write "BASEDATOS=" & mBaseDatos
            ostream.Close
            Set ostream = Nothing
            Set oFOs = Nothing
        End If
    Else
        If txtInstancia <> mInstancia Or mServidor <> "" Or mBaseDatos <> "" Then
            mInstancia = txtInstancia
    
            Set oFOs = New FileSystemObject
            Set ostream = oFOs.CreateTextFile(App.Path & "\FSGSClient.ini", True)
            
            ostream.Write "INSTANCIA=" & mInstancia
            ostream.Close
            Set ostream = Nothing
            Set oFOs = Nothing
        End If
        mServidor = ""
        mBaseDatos = ""

    End If
    
    'Guardo el Registration Code de Persist ASPUser en el registro
    SavePersistRegistrationCode "RegKey", "71133-29545-30344"
    
    
    'Actualizo PARGEN_INTERNO con los paths de la ayuda y del visor de la ayuda
    'Acceso a PM encriptado:
    sAcceso = ""
    If chkPMII.Value = vbChecked Then
        sAcceso = "FULLSTEP WS 2"
    Else
        If chkPMI.Value = vbChecked Then
            sAcceso = "FULLSTEP WS 1"
        End If
    End If
    If sAcceso <> "" Then
        sAcceso = EncriptarAccesoFSWS("1", sAcceso, True)
    End If
    
    'Acceso a QA encriptado:
    sAccesoQA = ""
    If chkQA.Value = vbChecked Then
        sAccesoQA = "FULLSTEP QA"
        
        If chkQACertificados.Value = vbChecked Then
            sAccesoQA = sAccesoQA & "//CERTIF//"
        End If
        If chkQANoConformidad.Value = vbChecked Then
            sAccesoQA = sAccesoQA & "//REC//"
        End If
        sAccesoQA = EncriptarAccesoFSWS("QA", sAccesoQA, True)
    End If
    
    sAccesoSM = ""
    If chkSM.Value = vbChecked Then
        sAccesoSM = "FULLSTEP SM"
    End If
    If sAccesoSM <> "" Then
        sAccesoSM = EncriptarAccesoFSWS("SM", sAccesoSM, True)
    End If
    
    sAccesoFA = ""
    If chkFA.Value = vbChecked Then
        sAccesoFA = "FULLSTEP FA"
    End If
    If sAccesoFA <> "" Then
        sAccesoFA = EncriptarAccesoFSWS("FA", sAccesoFA, True)
    End If
    
    'Encriptaci�n del acceso a GS
    sAccesoGS = ""
    If chkGS.Value = vbChecked Then
        sAccesoGS = "FULLSTEP GS"
    End If
    If sAccesoGS <> "" Then
        sAccesoGS = EncriptarAccesoFSWS("GS", sAccesoGS, True)
    End If
    
    'Encriptaci�n del acceso a CN
    sAccesoCN = ""
    If chkCN.Value = vbChecked Then
        sAccesoCN = "FULLSTEP CN"
    End If
    If sAccesoCN <> "" Then
        sAccesoCN = EncriptarAccesoFSWS("CN", sAccesoCN, True)
    End If
    
    'Encriptaci�n del acceso a Contratos
    sAccesoContratos = ""
    If chkContratos.Value = vbChecked Then
        sAccesoContratos = "FULLSTEP CONTRATOS"
    End If
    If sAccesoContratos <> "" Then
        sAccesoContratos = EncriptarAccesoFSWS("CO", sAccesoContratos, True)
    End If
    
    'Encriptaci�n del acceso a CN
    sAccesoBI = ""
    If chkBI.Value = vbChecked Then
        sAccesoBI = "FULLSTEP BI"
    End If
    If sAccesoBI <> "" Then
        sAccesoBI = EncriptarAccesoFSWS("BI", sAccesoBI, True)
    End If

    sConsulta = "UPDATE PARGEN_INTERNO SET READER='" & DblQuote(Trim(txtVisorAyuda.Text)) & "' , HELP_PDF='" & DblQuote(Trim(txtFicAyuda.Text))
    sConsulta = sConsulta & "', ACCESO_FSWS=" & StrToSQLNULL(sAcceso) & ", ACCESO_FSQA=" & StrToSQLNULL(sAccesoQA) & ", ACCESO_FSSM=" & StrToSQLNULL(sAccesoSM)
    sConsulta = sConsulta & ", ACCESO_FSFA=" & StrToSQLNULL(sAccesoFA) & ", ACCESO_FSGS=" & StrToSQLNULL(sAccesoGS) & ", ACCESO_FSCN=" & StrToSQLNULL(sAccesoCN)
    sConsulta = sConsulta & ", ACCESO_CONTRATO=" & StrToSQLNULL(sAccesoContratos) & ", ACCESO_FSBI=" & StrToSQLNULL(sAccesoBI) & " WHERE ID=1"
    If m_oADOConnection Is Nothing Then
        If ConectarBDdeGS = 1 Then
                Screen.MousePointer = vbNormal
                MsgBox "Error al conectar a la base de datos.", vbCritical, "Configuraci�n del acceso a datos"
                Exit Sub
        End If
    End If
    If Not m_oADOConnection Is Nothing Then m_oADOConnection.Execute sConsulta
    
    Screen.MousePointer = vbNormal
    MsgBox "Datos actualizados correctamente.", vbInformation, "Configuraci�n del acceso a datos"
    
    txtSrv.ForeColor = RGB(0, 0, 255)
    txtBD.ForeColor = RGB(0, 0, 255)
    txtInstancia.ForeColor = RGB(0, 0, 255)
    txtFicAyuda.ForeColor = RGB(0, 0, 255)
    txtVisorAyuda.ForeColor = RGB(0, 0, 255)
    
    
    Exit Sub
Error:
    Screen.MousePointer = vbNormal
    
    If Err.Number = 70 Then
        MsgBox "No se pudo escribir el archivo " & App.Path & "\FSGSClient.ini.  Permiso denegado." & vbCrLf & "Necesita permiso de escritura sobre la carpeta de instalaci�n de FULLSTEP GS.", vbCritical
    Else
        MsgBox Err.Description
    End If
End Sub

Private Sub cmdBuscarFic_Click()
On Error GoTo ErrBuscar
    
    cmmdAyuda.CancelError = True
    cmmdAyuda.Flags = cdlOFNHideReadOnly
    cmmdAyuda.Filter = "PDF (*.pdf)|*.pdf"
    cmmdAyuda.FilterIndex = 0
    cmmdAyuda.FileName = ""
    cmmdAyuda.ShowOpen

    txtFicAyuda.Text = cmmdAyuda.FileName
    
    Exit Sub
    
ErrBuscar:
    Exit Sub
End Sub

Private Sub cmdBuscarVisor_Click()
On Error GoTo ErrBuscar
    
    cmmdAyuda.CancelError = True
    cmmdAyuda.Flags = cdlOFNHideReadOnly
    cmmdAyuda.Filter = "EXE (*.exe)|*.exe"
    cmmdAyuda.FilterIndex = 0
    cmmdAyuda.FileName = ""
    cmmdAyuda.ShowOpen

    txtVisorAyuda.Text = cmmdAyuda.FileName
    
    Exit Sub
    
ErrBuscar:
    Exit Sub
End Sub


Private Sub cmdCerrar_Click()
    Unload Me
End Sub

''' Revisado por: blp. Fecha: 31/10/2012
''' <summary>
''' Carga el formulario
''' </summary>
''' <remarks>Llamada desde: evento Load; Tiempo m�ximo: 0,1</remarks>
Private Sub Form_Load()
    Dim sConsulta As String
    Dim oFOs As FileSystemObject
    Dim ostream As TextStream
    Dim adoRecordset As ADODB.Recordset
    Dim res As Integer
    Dim sAcceso As String
    Dim sAccesoQA As String
    Dim sAccesoSM As String
    Dim sAccesoFA As String
    Dim sAccesoGS As String
    Dim sAccesoCN As String
    Dim sAccesoBI As String
    Dim sAccesoContratos As String

    Set oFOs = New FileSystemObject
    
    Set ostream = oFOs.OpenTextFile(App.Path & "\FSGSClient.ini", ForReading, True)
    
    mServidor = ""
    mBaseDatos = ""
    If Not ostream Is Nothing Then
        If Not ostream.AtEndOfStream Then
            mInstancia = ostream.ReadLine
            If InStr(1, mInstancia, "INSTANCIA=") Then
                mInstancia = Right(mInstancia, Len(mInstancia) - 10) '10 es el n�mero de letras de 'INSTANCIA=' +1
                If Not ostream.AtEndOfStream Then
                    mServidor = ostream.ReadLine
                    If InStr(1, mServidor, "SERVIDOR=") Then
                        mServidor = Right(mServidor, Len(mServidor) - 9)
                        If Not ostream.AtEndOfStream Then
                            mBaseDatos = ostream.ReadLine
                            If InStr(1, mBaseDatos, "BASEDATOS=") Then
                                mBaseDatos = Right(mBaseDatos, Len(mBaseDatos) - 10)
                            End If
                        End If
                    End If
                End If
            Else
                mInstancia = ""
            End If
        Else
            mInstancia = ""
        End If
    End If
    ostream.Close
    Set ostream = Nothing
    
    Set oFOs = Nothing
    
    txtInstancia = mInstancia
    If mInstancia <> "" Then
        If mServidor = "" Or mBaseDatos = "" Then
            txtSrv = GetStringSetting("FULLSTEP GS", mInstancia, "Conexion", "Servidor")
            txtBD = GetStringSetting("FULLSTEP GS", mInstancia, "Conexion", "BaseDeDatos")
            opUsuario.Value = True
        Else
            txtSrv = mServidor
            txtBD = mBaseDatos
            opAplicacion.Value = True
        End If
        txtSrv.ForeColor = RGB(0, 0, 255)
        txtBD.ForeColor = RGB(0, 0, 255)
        txtInstancia.ForeColor = RGB(0, 0, 255)
    
        
        'Hace la conexi�n a la BD de FullstepGS
        res = ConectarBDdeGS()
        If res = 1 Then
            MsgBox "No se ha podido realizar la conexi�n con el servidor de base de datos."
            Exit Sub
        End If
        
        'Obtiene el path del manual de ayuda y el del visor del manual
        sConsulta = "SELECT READER,HELP_PDF,ACCESO_FSGS, ACCESO_FSWS,ACCESO_FSQA, ACCESO_FSSM, ACCESO_FSFA,ACCESO_FSCN,ACCESO_CONTRATO,ACCESO_FSBI FROM PARGEN_INTERNO WITH(NOLOCK) WHERE ID=1"
        Set adoRecordset = New ADODB.Recordset
        adoRecordset.Open sConsulta, m_oADOConnection, adOpenForwardOnly, adLockReadOnly
        If Not adoRecordset Is Nothing Then
            txtVisorAyuda.Text = IIf(IsNull(adoRecordset.Fields("READER").Value), "", adoRecordset.Fields("READER").Value)
            txtFicAyuda.Text = IIf(IsNull(adoRecordset.Fields("HELP_PDF").Value), "", adoRecordset.Fields("HELP_PDF").Value)
            sAcceso = IIf(IsNull(adoRecordset("ACCESO_FSWS").Value), "", adoRecordset("ACCESO_FSWS").Value)
            sAccesoQA = IIf(IsNull(adoRecordset("ACCESO_FSQA").Value), "", adoRecordset("ACCESO_FSQA").Value)
            sAccesoSM = IIf(IsNull(adoRecordset("ACCESO_FSSM").Value), "", adoRecordset("ACCESO_FSSM").Value)
            sAccesoFA = IIf(IsNull(adoRecordset("ACCESO_FSFA").Value), "", adoRecordset("ACCESO_FSFA").Value)
            sAccesoGS = IIf(IsNull(adoRecordset("ACCESO_FSGS").Value), "", adoRecordset("ACCESO_FSGS").Value)
            sAccesoCN = IIf(IsNull(adoRecordset("ACCESO_FSCN").Value), "", adoRecordset("ACCESO_FSCN").Value)
            sAccesoContratos = IIf(IsNull(adoRecordset("ACCESO_CONTRATO").Value), "", adoRecordset("ACCESO_CONTRATO").Value)
            sAccesoBI = IIf(IsNull(adoRecordset("ACCESO_FSBI").Value), "", adoRecordset("ACCESO_FSBI").Value)
            adoRecordset.Close
            
            'Acceso al PM:
            sAcceso = basGeneral.EncriptarAccesoFSWS("1", sAcceso, False)
            If sAcceso = "FULLSTEP WS 1" Then
                chkPMI.Value = vbChecked
            Else
                If sAcceso = "FULLSTEP WS 2" Then
                    chkPMII.Value = vbChecked
                End If
            End If
            
            'Acceso al QA:
            sAccesoQA = EncriptarAccesoFSWS("QA", sAccesoQA, False)
            If sAccesoQA = "FULLSTEP QA" Then
                chkQA.Value = vbChecked
            Else
                If InStr(1, sAccesoQA, "//CERTIF//") Then
                    chkQACertificados.Value = vbChecked
                End If
                If InStr(1, sAccesoQA, "//REC//") Then
                    chkQANoConformidad.Value = vbChecked
                End If
            End If
            
            'Acceso al SM:
            sAccesoSM = basGeneral.EncriptarAccesoFSWS("SM", sAccesoSM, False)
            If sAccesoSM = "FULLSTEP SM" Then
                chkSM.Value = vbChecked
            End If
        
            'Acceso al FA:
            sAccesoFA = basGeneral.EncriptarAccesoFSWS("FA", sAccesoFA, False)
            If sAccesoFA = "FULLSTEP FA" Then
                chkFA.Value = vbChecked
            End If
        
            'Acceso al GS:
            sAccesoGS = basGeneral.EncriptarAccesoFSWS("GS", sAccesoGS, False)
            If sAccesoGS = "FULLSTEP GS" Then
                chkGS.Value = vbChecked
            End If
        
            'Acceso al CN:
            sAccesoCN = basGeneral.EncriptarAccesoFSWS("CN", sAccesoCN, False)
            If sAccesoCN = "FULLSTEP CN" Then
                chkCN.Value = vbChecked
            End If
        
            'Acceso a Contratos:
            sAccesoContratos = basGeneral.EncriptarAccesoFSWS("CO", sAccesoContratos, False)
            If sAccesoContratos = "FULLSTEP CONTRATOS" Then
                chkContratos.Value = vbChecked
            End If
            
            'Acceso al BI:
            sAccesoBI = basGeneral.EncriptarAccesoFSWS("BI", sAccesoBI, False)
            If sAccesoBI = "FULLSTEP BI" Then
                chkBI.Value = vbChecked
            End If
        
            txtVisorAyuda.ForeColor = RGB(0, 0, 255)
            txtFicAyuda.ForeColor = RGB(0, 0, 255)
        End If
        Set adoRecordset = Nothing
    End If
End Sub


Private Sub Form_Unload(Cancel As Integer)
    If Not m_oADOConnection Is Nothing Then
        m_oADOConnection.Close
        Set m_oADOConnection = Nothing
    End If
End Sub


Private Sub opAplicacion_Click()
    If opAplicacion.Value Then
        chkDefecto.Value = vbUnchecked
        chkDefecto.Enabled = False
    End If
End Sub

Private Sub opUsuario_Click()
    If opUsuario.Value Then
        chkDefecto.Enabled = True
    End If

End Sub

Private Sub txtBD_Change()
    txtBD.ForeColor = RGB(0, 0, 0)
End Sub

Private Sub txtSrv_Change()
    txtSrv.ForeColor = RGB(0, 0, 0)
End Sub

''' <summary>
''' Extrae el login del fichero FSDB.LIC y lo desencripta.
''' </summary>
''' <returns>La contrase�a desencriptada</returns>
''' <remarks>Llamada desde: CRbasPublic.Conectar; Tiempo m�ximo: 0,1</remarks>
Function LICDBLogin() As String
      
    Dim fso As New FileSystemObject, fil As File, ts As TextStream
    Dim FechaHora As String
    Dim tmplogin As String
    Dim tmpcontra As String

    Set fil = fso.GetFile(App.Path & "\FSDB.LIC")
    Set ts = fil.OpenAsTextStream(ForReading)
    
    FechaHora = Format(ts.ReadLine, "DD\/MM\/YYYY HH\:NN\:SS")
    
    tmplogin = ts.ReadLine
    tmpcontra = ts.ReadLine
    ts.Close
        
    ''' Desencriptar login
    LICDBLogin = EncriptarAES("", tmplogin, False, FechaHora, 0)
End Function

''' <summary>
''' Extrae la contrase�a del fichero FSDB.LIC y la desencripta.
''' </summary>
''' <returns>La contrase�a desencriptada</returns>
''' <remarks>Llamada desde: CRbasPublic.Conectar; Tiempo m�ximo: 0,1</remarks>
Function LICDBContra() As String

    Dim fso As New FileSystemObject, fil As File, ts As TextStream
    Dim FechaHora As String
    Dim tmplogin As String
    Dim tmpcontra As String

    Set fil = fso.GetFile(App.Path & "\FSDB.LIC")
    Set ts = fil.OpenAsTextStream(ForReading)
    
    FechaHora = Format(ts.ReadLine, "DD\/MM\/YYYY HH\:NN\:SS")
    
    tmplogin = ts.ReadLine
    tmpcontra = ts.ReadLine
    ts.Close
        
    ''' Desencriptar contrase�a
    LICDBContra = EncriptarAES("", tmpcontra, False, FechaHora, 0)
End Function


Private Function ConectarBDdeGS() As Integer
On Error GoTo ErrorDeConexion

    Set m_oADOConnection = New ADODB.Connection
    m_oADOConnection.Open "Provider=SQLOLEDB.1;Auto Translate=FALSE;Server=" & Trim(txtSrv.Text) & ";Database=" & Trim(txtBD) & ";", LICDBLogin, LICDBContra
    m_oADOConnection.CursorLocation = adUseClient
    m_oADOConnection.CommandTimeout = 120
    
    ConectarBDdeGS = 0
    Exit Function
    
ErrorDeConexion:
    Set m_oADOConnection = Nothing
    ConectarBDdeGS = 1
    
End Function
