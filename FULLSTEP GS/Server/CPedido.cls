VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CPedido"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Private mvarConexion As CConexion
Private mvarId As Long
Private mvarAnyo As Integer
Private mvarNumero As Long 'numero de pedido dentro del a�o
Private mvarEstado As Integer
Private mvarTipo As TipoPedido
'Private mvarUsuario As String
Private mvarIndice As Variant
Private mvarPersona As Variant
Private mvarOrdenesEntrega As COrdenesEntrega
Private mvarNotificaciones As CNotificacionesPedido
Private mvarFecha As Date

Private mvarRecordset As ADODB.Recordset
Friend Property Set Conexion(ByVal con As CConexion)
Set mvarConexion = con
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = mvarConexion
End Property

Public Property Get ID() As Long
    ID = mvarId
End Property

Public Property Let ID(ByVal Data As Long)
    mvarId = Data
End Property
Public Property Get Anyo() As Integer
    Anyo = mvarAnyo
End Property

Public Property Let Anyo(ByVal Data As Integer)
    mvarAnyo = Data
End Property
Public Property Get Numero() As Long
    Numero = mvarNumero
End Property

Public Property Let Numero(ByVal Data As Long)
    mvarNumero = Data
End Property

Public Property Get estado() As Integer
    estado = mvarEstado
End Property

Public Property Let estado(ByVal Data As Integer)
    mvarEstado = Data
End Property
Public Property Get Tipo() As TipoPedido
    Tipo = mvarTipo
End Property

Public Property Let Tipo(ByVal Data As TipoPedido)
    mvarTipo = Data
End Property
'Public Property Get Usuario() As String
'    Usuario = mvarUsuario
'End Property
'
'Public Property Let Usuario(ByVal Data As String)
'    mvarUsuario = Data
'End Property
Public Property Let Indice(ByVal varIndice As Variant)
    mvarIndice = varIndice
End Property
Public Property Get Indice() As Variant
    Indice = mvarIndice
End Property
Public Property Get OrdenesEntrega() As COrdenesEntrega
   Set OrdenesEntrega = mvarOrdenesEntrega
End Property

Public Property Set OrdenesEntrega(ByVal Data As COrdenesEntrega)
   Set mvarOrdenesEntrega = Data
End Property
Public Property Get Notificaciones() As CNotificacionesPedido
   Set Notificaciones = mvarNotificaciones
End Property

Public Property Set Notificaciones(ByVal Data As CNotificacionesPedido)
    Set mvarNotificaciones = Data
End Property
Public Property Get Persona() As Variant
    Persona = mvarPersona
End Property

Public Property Let Persona(ByVal Data As Variant)
    mvarPersona = Data
End Property
Public Property Get Fecha() As Date
    Fecha = mvarFecha
End Property

Public Property Let Fecha(ByVal Data As Date)
    mvarFecha = Data
End Property

''' <summary>
''' Carga de las notificaciones del pedido
''' </summary>
''' <remarks>Llamada desde: frmPedidos.cmdEmitir_Click;
''' frmseguimComunic.cmdRestaurar_Click;frmseguimComunic.Form_Load;
''' Tiempo m�ximo:0,1</remarks>
''' <revision>JVS 26/12/2011</revision>
Public Function CargarNotificacionesPedido()
Dim sConsulta As String
Dim rs As New ADODB.Recordset
Dim fldId As ADODB.Field
Dim fldPedido As ADODB.Field
Dim fldOrden As ADODB.Field
Dim fldApellidos As ADODB.Field
Dim fldNombre As ADODB.Field
Dim fldEmailDir As ADODB.Field
Dim fldTipo As ADODB.Field
Dim fldFecha As ADODB.Field
Dim fldWeb As ADODB.Field
Dim fldMail As ADODB.Field
Dim fldCarta As ADODB.Field

sConsulta = "SELECT ID,PEDIDO,ORDEN,APE,NOM,EMAILDIR,TIPO,FECHA,WEB,EMAIL,IMP,FECACT,REGISTRO_EMAIL,ERROR FROM ORDEN_COMUNIC WITH(NOLOCK) WHERE  ORDEN =" & mvarId
    
rs.Open sConsulta, mvarConexion.ADOCon, adOpenStatic, adLockReadOnly

If rs.eof Then
    rs.Close
    Set rs = Nothing
    Set mvarNotificaciones = Nothing
    Set mvarNotificaciones = New CNotificacionesPedido
    Exit Function
Else
    Set mvarNotificaciones = Nothing
    Set mvarNotificaciones = New CNotificacionesPedido
    Set mvarNotificaciones.Conexion = mvarConexion
    
    Set fldId = rs.Fields("ID")
    Set fldPedido = rs.Fields("PEDIDO")
    Set fldOrden = rs.Fields("ORDEN")
    Set fldApellidos = rs.Fields("APE")
    Set fldNombre = rs.Fields("NOM")
    Set fldEmailDir = rs.Fields("EMAILDIR")
    Set fldTipo = rs.Fields("TIPO")
    Set fldFecha = rs.Fields("FECHA")
    Set fldWeb = rs.Fields("WEB")
    Set fldMail = rs.Fields("EMAIL")
    Set fldCarta = rs.Fields("IMP")
    
    While Not rs.eof
        mvarNotificaciones.Add fldId.Value, fldPedido.Value, fldOrden.Value, , , , fldApellidos.Value, fldNombre.Value, NullToStr(fldEmailDir.Value), fldTipo.Value, fldFecha.Value, fldWeb.Value, fldMail.Value, fldCarta.Value
        rs.MoveNext
    Wend
    
    rs.Close
    Set rs = Nothing
    Set fldId = Nothing
    Set fldPedido = Nothing
    Set fldOrden = Nothing
    Set fldApellidos = Nothing
    Set fldNombre = Nothing
    Set fldEmailDir = Nothing
    Set fldTipo = Nothing
    Set fldFecha = Nothing
    Set fldWeb = Nothing
    Set fldMail = Nothing
    Set fldCarta = Nothing
    
End If

End Function
Private Sub Class_Terminate()
    Set mvarConexion = Nothing
    Set mvarConexionServer = Nothing
    Set mvarNotificaciones = Nothing
    Set mvarOrdenesEntrega = Nothing
End Sub
