VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CPMRoles"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
   
End Enum

Private m_Col As Collection
Private m_oConexion As CConexion
Private m_bEOF As Boolean

Public Property Get eof() As Boolean
    eof = m_bEOF
End Property
Friend Property Let eof(ByVal b As Boolean)
    m_bEOF = b
End Property

Public Property Get Item(vntIndexKey As Variant) As CPMRol
On Error GoTo NoSeEncuentra:

    Set Item = m_Col(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property

Friend Property Set Conexion(ByVal con As CConexion)
    Set m_oConexion = con
End Property

Friend Property Get Conexion() As CConexion
    Set Conexion = m_oConexion
End Property

Public Property Get Count() As Long
    If m_Col Is Nothing Then
        Count = 0
    Else
         Count = m_Col.Count
    End If
End Property

Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
     Set NewEnum = m_Col.[_NewEnum]
End Property

'Revisado por: SRA (28/10/2011)
'Descripcion:A�ade un item de la clase CPMRol
'Parametros entrada:
'       lId: Id del rol
'       lWorkflow: ID workflow
'       sDen: descripci�n del rol
'       iTipo: Tipo de rol
'       iCuandoAsignar: Cuando se hace la asignaci�n
'       lBloqueAsigna: Bloque donde se hace la asignaci�nk
'       iComoAsignar: Como se hace la asignaci�n
'       iRestringir: Restricciones
'       lCampo: Id del campo
'       lRolAsigna: Rol que hace la asignaci�n
'       sPer: Cod de la persona
'       sProve: Cod del proveedor
'       lContacto: Id del contacto
'       lConfBloqueDef: Configuraci�n del bloque
'       sUON1: Unidad organizativa de nivel 1
'       sUON2: Unidad organizativa de nivel 2
'       sUON3: Unidad organizativa de nivel 3
'       sDep: Cod del departamento
'       sEqp: Equipo
'       dtFecAct: Fecha de actualizaci�n
'       bVer_flujo: Ver flujo
'       vIndice: Indice
'       sSustituto: Sustituto
'       iTipoSustitucion: Tipo de sustituci�n
'       bVerDetallePer: Ver datos participante flujo
'       iVisibilidadObservador: Visibilidad etapas rol observador
'       bPermitirTrasladosRol: El rol va a poder realizar traslados o no.
'       bPermitirTrasladosBloqu: El rol va a poder realizar traslados o no en el bloque.
'       UON0: Se ha seleccionado la ra�z organizativa
'Llamada desde:CargarRolesWorkflow
'Tiempo ejecucion:0,1seg.
Public Function Add(ByVal lId As Long, ByVal lWorkflow As Long, ByVal sDen As String, Optional ByVal iTipo As TipoPMRol, Optional ByVal iCuandoAsignar As CuandoAsignarPMRol, Optional ByVal lBloqueAsigna As Long, Optional ByVal iComoAsignar As ComoAsignarPMRol, Optional ByVal iRestringir As RestringirPMRol, Optional ByVal lCampo As Long, Optional ByVal lRolAsigna As Long, Optional ByVal sPer As String, Optional ByVal sProve As String, Optional ByVal lContacto As Long, Optional ByVal lConfBloqueDef As Long, Optional ByVal sUON1 As String, Optional ByVal sUON2 As String, Optional ByVal sUON3 As String, Optional ByVal sDep As String, Optional ByVal sEqp As String, Optional ByVal dtFecAct As Variant, Optional bVer_flujo As Boolean, Optional ByVal vIndice As Variant, Optional ByVal sSustituto As String, Optional ByVal iTipoSustitucion As TipoSustitucion, Optional bVerDetallePer As Boolean, Optional ByVal iVisibilidadObservador As VisibilidadObservadorPMRol, Optional bPermitirTrasladosRol As Boolean, _
                Optional bPermitirTrasladosBloque As Boolean, Optional bGestor As Boolean, Optional bReceptor As Boolean, Optional bUON0 As Boolean, Optional TipoAprobador As Integer) As CPMRol
    
    'create a new object
    Dim objnewmember As CPMRol
    
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set objnewmember = New CPMRol
    With objnewmember
        Set .Conexion = m_oConexion
        .Id = lId
        .Workflow = lWorkflow
        .Den = sDen
        .Tipo = iTipo
        .CuandoAsignar = iCuandoAsignar
        .BloqueAsigna = lBloqueAsigna
        .ComoAsignar = iComoAsignar
        .Restringir = iRestringir
        .Campo = lCampo
        .RolAsigna = lRolAsigna
        .Per = sPer
        .Prove = sProve
        .Contacto = lContacto
        .ConfBloqueDef = lConfBloqueDef
        .UON0 = bUON0
        .UON1 = sUON1
        .UON2 = sUON2
        .UON3 = sUON3
        .Dep = sDep
        .Eqp = sEqp
        .FecAct = dtFecAct
        .ver_flujo = bVer_flujo
        .Sustituto = sSustituto
        .TipoSustitucion = iTipoSustitucion
        .VerDetallePer = bVerDetallePer
        .VisibilidadObservador = iVisibilidadObservador
        .PermitirTraslados = bPermitirTrasladosRol
        .PermitirTrasladosBloque = bPermitirTrasladosBloque
                
        .Gestor = bGestor
        .Receptor = bReceptor
        If Not IsMissing(TipoAprobador) And Not IsNull(TipoAprobador) Then
            .TipoAprobador = TipoAprobador
        End If
        
        If Not IsMissing(vIndice) And Not IsNull(vIndice) Then
            .Indice = vIndice
            m_Col.Add objnewmember, CStr(vIndice)
        Else
            m_Col.Add objnewmember, CStr(lId)
        End If
        

    
    End With
    
    'return the object created
    Set Add = objnewmember
    Set objnewmember = Nothing

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CPMRoles", "Add", ERR, Erl)
      Exit Function
   End If

End Function

Public Sub AddRol(ByVal oRol As CPMRol, Optional ByVal vIndice As Variant)

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If Not IsMissing(vIndice) And Not IsNull(vIndice) Then
        oRol.Indice = vIndice
        m_Col.Add oRol, CStr(vIndice)
    Else
        m_Col.Add oRol, CStr(oRol.Id)
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CPMRoles", "AddRol", ERR, Erl)
      Exit Sub
   End If
End Sub

Public Sub Remove(vntIndexKey As Variant)

If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    m_Col.Remove vntIndexKey
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CPMRoles", "Remove", ERR, Erl)
      Exit Sub
   End If

End Sub


Private Sub Class_Initialize()
    Set m_Col = New Collection
End Sub


Private Sub Class_Terminate()
    'Proceroys collection when this class is terminated
    On Error Resume Next
    Set m_Col = Nothing
    Set m_oConexion = Nothing
    
End Sub

'Revisado por: SRA (28/10/2011)
'Descripcion:Carga los roles del workflow
'Parametros entrada:
'       lIDWorkFlow: ID workflow
'Llamada desde:frmFlujosRoles.frm
'Tiempo ejecucion:0,1seg.
Public Function DevolverRolesWorkflow(ByVal lIdWorkflow As Long) As adodb.Recordset
    Dim oadorecordset As adodb.Recordset
    Dim sql As String
    
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sql = "SELECT ID, WORKFLOW, DEN, TIPO, CUANDO_ASIGNAR, BLOQUE_ASIGNA, COMO_ASIGNAR, RESTRINGIR, CAMPO, ROL_ASIGNA, PER, PROVE, CON, CONF_BLOQUE_DEF, UON1, UON2, UON3, DEP, EQP, FECACT, VER_FLUJO, SUSTITUTO, TIPO_SUSTITUCION, VER_DETALLE_PER,VISIBILIDAD_OBSERVADOR,PERMITIR_TRASLADO,GESTOR_FACTURA,RECEPTOR_FACTURA, UON0, TIPO_APROBADOR FROM PM_ROL WITH (NOLOCK) WHERE WORKFLOW = " & lIdWorkflow
    Set oadorecordset = New adodb.Recordset
    oadorecordset.CursorLocation = adUseClient
    oadorecordset.Open sql, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    oadorecordset.ActiveConnection = Nothing
        
    Set DevolverRolesWorkflow = oadorecordset
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CPMRoles", "DevolverRolesWorkflow", ERR, Erl)
      Exit Function
   End If
    
End Function

'Revisado por: SRA (28/10/2011)
'Descripcion:Carga los roles del workflow, sus participantes y las empresas relacionadas al rol
'Parametros entrada:
'       lIDWorkFlow: ID workflow
'Llamada desde:frmFlujosRoles.frm
'Tiempo ejecucion:0,3seg.

Public Sub CargarRolesWorkflow(ByVal lIdWorkflow As Long)
    Dim oRS As adodb.Recordset
    Dim oRol As CPMRol
    
        
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set oRS = DevolverRolesWorkflow(lIdWorkflow)
    While Not oRS.eof
        Set oRol = Add(NullToDbl0(oRS("ID").Value), NullToDbl0(oRS("WORKFLOW").Value), NullToStr(oRS("DEN").Value), NullToDbl0(oRS("TIPO").Value), NullToDbl0(oRS("CUANDO_ASIGNAR").Value), NullToDbl0(oRS("BLOQUE_ASIGNA").Value), NullToDbl0(oRS("COMO_ASIGNAR").Value), NullToDbl0(oRS("RESTRINGIR").Value), NullToDbl0(oRS("CAMPO").Value), NullToDbl0(oRS("ROL_ASIGNA").Value), NullToStr(oRS("PER").Value), NullToStr(oRS("PROVE").Value), NullToDbl0(oRS("CON").Value), NullToDbl0(oRS("CONF_BLOQUE_DEF").Value), NullToStr(oRS("UON1").Value), NullToStr(oRS("UON2").Value), NullToStr(oRS("UON3").Value), NullToStr(oRS("DEP").Value), NullToStr(oRS("EQP").Value), oRS("FECACT").Value, NullToDbl0(oRS("VER_FLUJO").Value), , NullToStr(oRS("SUSTITUTO").Value), NullToDbl0(oRS("TIPO_SUSTITUCION").Value), NullToDbl0(oRS("VER_DETALLE_PER").Value), NullToDbl0(oRS("VISIBILIDAD_OBSERVADOR").Value), NullToDbl0(oRS("PERMITIR_TRASLADO").Value), , NullToDbl0(oRS("GESTOR_FACTURA").Value) _
        , NullToDbl0(oRS("RECEPTOR_FACTURA").Value), NullToDbl0(oRS("UON0").Value), NullToDbl0(oRS("TIPO_APROBADOR").Value))
        oRol.CargarParticipantes
        oRol.CargarEmpresas
        oRS.MoveNext
    Wend
    oRS.Close
    Set oRol = Nothing
    Set oRS = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CPMRoles", "CargarRolesWorkflow", ERR, Erl)
      Exit Sub
   End If
End Sub

'Revisado por: SRA (31/10/2011)
'Descripcion:Carga los roles del bloque pasado como par�metro
'Parametros entrada:
'       lIDBloque: ID del bloque
'Llamada desde:frmFlujosConfEtapa.frm
'Tiempo ejecucion:0,1seg.
Public Sub CargarRolesBloque(ByVal lIdBloque As Long)
    Dim oRS As adodb.Recordset
    Dim sql As String
    
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sql = "SELECT ID, WORKFLOW, DEN, TIPO, CUANDO_ASIGNAR, BLOQUE_ASIGNA, COMO_ASIGNAR, RESTRINGIR, CAMPO, ROL_ASIGNA, PER, PROVE, CON, CONF_BLOQUE_DEF, UON1, UON2, UON3, DEP, EQP, FECACT, R.PERMITIR_TRASLADO AS PERMITIR_TRASLADO_ROL, RB.PERMITIR_TRASLADO AS PERMITIR_TRASLADO_BLOQUE FROM PM_ROL R WITH(NOLOCK) INNER JOIN PM_ROL_BLOQUE RB WITH(NOLOCK) ON R.ID = RB.ROL WHERE BLOQUE = " & lIdBloque
            
    Set oRS = New adodb.Recordset
    oRS.CursorLocation = adUseClient
    oRS.Open sql, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    oRS.ActiveConnection = Nothing
    While Not oRS.eof
        Add NullToDbl0(oRS("ID").Value), NullToDbl0(oRS("WORKFLOW").Value), NullToStr(oRS("DEN").Value), NullToDbl0(oRS("TIPO").Value), NullToDbl0(oRS("CUANDO_ASIGNAR").Value), NullToDbl0(oRS("BLOQUE_ASIGNA").Value), NullToDbl0(oRS("COMO_ASIGNAR").Value), NullToDbl0(oRS("RESTRINGIR").Value), NullToDbl0(oRS("CAMPO").Value), NullToDbl0(oRS("ROL_ASIGNA").Value), NullToStr(oRS("PER").Value), NullToStr(oRS("PROVE").Value), NullToDbl0(oRS("CON").Value), NullToDbl0(oRS("CONF_BLOQUE_DEF").Value), NullToStr(oRS("UON1").Value), NullToStr(oRS("UON2").Value), NullToStr(oRS("UON3").Value), NullToStr(oRS("DEP").Value), NullToStr(oRS("EQP").Value), oRS("FECACT").Value, , , , , , , NullToDbl0(oRS("PERMITIR_TRASLADO_ROL").Value), NullToDbl0(oRS("PERMITIR_TRASLADO_BLOQUE").Value)
        oRS.MoveNext
    Wend
    oRS.Close
    Set oRS = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CPMRoles", "CargarRolesBloque", ERR, Erl)
      Exit Sub
   End If
End Sub




