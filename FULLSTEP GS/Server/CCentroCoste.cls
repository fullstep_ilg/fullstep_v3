VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CCentroCoste"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
    
End Enum

Private m_oConexion As CConexion

Private m_sCodConcat As String   'Código con las uons concatenadas.
Private m_sCod As String         'Código del centro de coste.
Private m_sDen As String         'Denominación del centro de coste.
Private m_sCodSM As String       'Código del centro de coste de CENTRO_SM.
Private m_sDenSM As String       'Denominación del centro de coste de CENTRO_SM.
Private m_sUON1 As String
Private m_sUON2 As String
Private m_sUON3 As String
Private m_sUON4 As String
Private m_sEmpresa As String     'Empresa a la que pertenece dicho centro de coste.
Private m_sNif As String         'Nif de la empresa a la que pertenece dicho centro de coste.
Private m_lID As Long            'ID de la empresa

Public Property Let CodConcat(ByVal dato As String)
    m_sCodConcat = dato
End Property
Public Property Get CodConcat() As String
    CodConcat = m_sCodConcat
End Property

Public Property Let Cod(ByVal dato As String)
    m_sCod = dato
End Property
Public Property Get Cod() As String
    Cod = m_sCod
End Property

Public Property Let Den(ByVal dato As String)
    m_sDen = dato
End Property
Public Property Get Den() As String
    Den = m_sDen
End Property

Public Property Let COD_SM(ByVal dato As String)
    m_sCodSM = dato
End Property
Public Property Get COD_SM() As String
    COD_SM = m_sCodSM
End Property

Public Property Let DEN_SM(ByVal dato As String)
    m_sDenSM = dato
End Property
Public Property Get DEN_SM() As String
    DEN_SM = m_sDenSM
End Property

Public Property Let UON1(ByVal dato As String)
    m_sUON1 = dato
End Property
Public Property Get UON1() As String
    UON1 = m_sUON1
End Property

Public Property Let UON2(ByVal dato As String)
    m_sUON2 = dato
End Property
Public Property Get UON2() As String
    UON2 = m_sUON2
End Property

Public Property Let UON3(ByVal dato As String)
    m_sUON3 = dato
End Property
Public Property Get UON3() As String
    UON3 = m_sUON3
End Property

Public Property Let UON4(ByVal dato As String)
    m_sUON4 = dato
End Property
Public Property Get UON4() As String
    UON4 = m_sUON4
End Property

Public Property Let Empresa(ByVal dato As String)
    m_sEmpresa = dato
End Property
Public Property Get Empresa() As String
    Empresa = m_sEmpresa
End Property

Public Property Let NIF(ByVal dato As String)
    m_sNif = dato
End Property
Public Property Get NIF() As String
    NIF = m_sNif
End Property

Public Property Let Id(ByVal dato As Long)
    m_lID = dato
End Property
Public Property Get Id() As Long
    Id = m_lID
End Property


Friend Property Set Conexion(ByVal vData As CConexion)

    ''' * Objetivo: Dar valor a la conexion de la Destino
    ''' * Recibe: Conexion
    ''' * Devuelve: Nada

    Set m_oConexion = vData
    
End Property
Friend Property Get Conexion() As CConexion

    ''' * Objetivo: Devolver la conexion de la Destino
    ''' * Recibe: Nada
    ''' * Devuelve: Conexion

    Set Conexion = m_oConexion
    
End Property



Private Sub Class_Terminate()
    
    On Error GoTo ERROR:
    
    Set m_oConexion = Nothing
    Exit Sub
ERROR:
    Resume Next
    
End Sub


''' <summary>
''' Devuelve la denominación de un centro de coste.
''' </summary>
''' <param name="sUON1">UO de nivel 1.</param>
''' <param name="sUON2">UO de nivel 2.</param>
''' <param name="sUON3">UO de nivel 3.</param>
''' <param name="sUON4">UO de nivel 4.</param>
''' <returns>Un string con la denominación del centro de coste.</returns>
''' <remarks>Llamada desde: frmSolicitudes.PonerDenominacionCentroCoste; frmSolicitudBuscar.PonerDenominaciónCentroCoste </remarks>
''' <remarks>Tiempo máximo: < 1seg </remarks>

Public Function DevolverDenominacion(ByVal sUON1 As String, ByVal sUON2 As String, ByVal sUON3 As String, ByVal sUON4 As String) As String
    Dim sConsulta As String
    Dim rs As adodb.Recordset
    Dim cmd As adodb.Command
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set cmd = New adodb.Command
    Dim pmt As adodb.Parameter
    
    If sUON1 <> "" And sUON2 <> "" And sUON3 <> "" And sUON4 <> "" Then
        sConsulta = "SELECT UON1,UON2,UON3,COD AS UON4,DEN FROM UON4 WITH (NOLOCK) WHERE UON1=? AND UON2=? AND UON3=? AND COD=?"
        Set pmt = cmd.CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=Len(sUON1), Value:=sUON1)
        cmd.Parameters.Append pmt
        Set pmt = cmd.CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=Len(sUON2), Value:=sUON2)
        cmd.Parameters.Append pmt
        Set pmt = cmd.CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=Len(sUON3), Value:=sUON3)
        cmd.Parameters.Append pmt
        Set pmt = cmd.CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=Len(sUON4), Value:=sUON4)
        cmd.Parameters.Append pmt
    ElseIf sUON1 <> "" And sUON2 <> "" And sUON3 <> "" And sUON4 = "" Then
        sConsulta = "SELECT UON1,UON2,COD AS UON3,DEN FROM UON3 WITH (NOLOCK) WHERE UON1=? AND UON2=? AND COD=?"
        Set pmt = cmd.CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=Len(sUON1), Value:=sUON1)
        cmd.Parameters.Append pmt
        Set pmt = cmd.CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=Len(sUON2), Value:=sUON2)
        cmd.Parameters.Append pmt
        Set pmt = cmd.CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=Len(sUON3), Value:=sUON3)
        cmd.Parameters.Append pmt
    ElseIf sUON1 <> "" And sUON2 <> "" And sUON3 = "" And sUON4 = "" Then
        sConsulta = "SELECT UON1,COD AS UON2,DEN FROM UON2 WITH (NOLOCK) WHERE UON1=? AND COD=?"
        Set pmt = cmd.CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=Len(sUON1), Value:=sUON1)
        cmd.Parameters.Append pmt
        Set pmt = cmd.CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=Len(sUON2), Value:=sUON2)
        cmd.Parameters.Append pmt
    ElseIf sUON1 <> "" And sUON2 = "" And sUON3 = "" And sUON4 = "" Then
        sConsulta = "SELECT COD AS UON1,DEN FROM UON1 WITH (NOLOCK) WHERE COD=?"
        Set pmt = cmd.CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=Len(sUON1), Value:=sUON1)
        cmd.Parameters.Append pmt
    End If
    
    If Len(sConsulta) > 0 Then
        cmd.CommandText = sConsulta
        cmd.CommandType = adCmdText
        Set cmd.ActiveConnection = m_oConexion.ADOCon
        cmd.Prepared = True
        Set rs = cmd.Execute
        If Not rs.eof Then
            DevolverDenominacion = rs.Fields("DEN").Value
        End If
        rs.Close
    End If
    Set pmt = Nothing
    Set cmd = Nothing
    Set rs = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CCentroCoste", "DevolverDenominacion", ERR, Erl)
        Exit Function
    End If
End Function

''' <summary>
''' cARGA el centro de conste SM en la propiedad
''' </summary>
''' <remarks>Llamada desde: frmSelCenCoste.cmdAceptar_Click </remarks>
''' <remarks>Tiempo máximo: < 1seg </remarks>

Public Sub CargarCodigoSM()
    Dim sConsulta As String
    Dim rs As adodb.Recordset
    Dim cmd As adodb.Command
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set cmd = New adodb.Command
    Dim pmt As adodb.Parameter
    
    If m_sUON1 = "" Then Exit Sub
    
    sConsulta = "SELECT TOP 1 CENTRO_SM FROM CENTRO_SM_UON WITH (NOLOCK) "
    
    If m_sUON1 <> "" And m_sUON2 <> "" And m_sUON3 <> "" And m_sUON4 <> "" Then
        sConsulta = sConsulta & " WHERE UON1=? AND UON2=? AND UON3=? AND UON4=?"
        Set pmt = cmd.CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=Len(m_sUON1), Value:=m_sUON1)
        cmd.Parameters.Append pmt
        Set pmt = cmd.CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=Len(m_sUON2), Value:=m_sUON2)
        cmd.Parameters.Append pmt
        Set pmt = cmd.CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=Len(m_sUON3), Value:=m_sUON3)
        cmd.Parameters.Append pmt
        Set pmt = cmd.CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=Len(m_sUON4), Value:=m_sUON4)
        cmd.Parameters.Append pmt
    ElseIf m_sUON1 <> "" And m_sUON2 <> "" And m_sUON3 <> "" And m_sUON4 = "" Then
        sConsulta = sConsulta & " WHERE UON1=? AND UON2=? AND UON3=? AND UON4 IS NULL"
        Set pmt = cmd.CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=Len(m_sUON1), Value:=m_sUON1)
        cmd.Parameters.Append pmt
        Set pmt = cmd.CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=Len(m_sUON2), Value:=m_sUON2)
        cmd.Parameters.Append pmt
        Set pmt = cmd.CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=Len(m_sUON3), Value:=m_sUON3)
        cmd.Parameters.Append pmt
    ElseIf m_sUON1 <> "" And m_sUON2 <> "" And m_sUON3 = "" And m_sUON4 = "" Then
        sConsulta = sConsulta & " WHERE UON1=? AND UON2=? AND UON3 IS NULL"
        Set pmt = cmd.CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=Len(m_sUON1), Value:=m_sUON1)
        cmd.Parameters.Append pmt
        Set pmt = cmd.CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=Len(m_sUON2), Value:=m_sUON2)
        cmd.Parameters.Append pmt
    ElseIf m_sUON1 <> "" And m_sUON2 = "" And m_sUON3 = "" And m_sUON4 = "" Then
        sConsulta = sConsulta & " WHERE UON1=? AND UON2 IS NULL"
        Set pmt = cmd.CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=Len(m_sUON1), Value:=m_sUON1)
        cmd.Parameters.Append pmt
    End If
    
    cmd.CommandText = sConsulta
    cmd.CommandType = adCmdText
    Set cmd.ActiveConnection = m_oConexion.ADOCon
    cmd.Prepared = True
    Set rs = cmd.Execute
    If Not rs.eof Then
        m_sCodSM = rs.Fields("CENTRO_SM").Value
    End If
    rs.Close
    Set pmt = Nothing
    Set cmd = Nothing
    Set rs = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CCentroCoste", "CargarCodigoSM", ERR, Erl)
        Exit Sub
    End If
End Sub

''' <summary>Devuelve los gestores asociados al centro de coste (gestores de partidas asociadas al centro de coste)</summary>
''' <returns>Objeto CPersonas con los datos de los gestores</returns>
''' <remarks>Llamada desde: frmSelCenContables</remarks>
''' <revision>LTG 27/06/2012</revision>

Public Function DevolverGestoresAsociados() As CPersonas
    Dim oGestores As CPersonas
    Dim oCom As adodb.Command
    Dim oParam As adodb.Parameter
    Dim rsGestores As adodb.Recordset
    Dim sConsulta As String
    
    '********* Precondicion **************************************
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + 613, "CCentroCoste.DevolverGestoresAsociados", "No se ha establecido la conexion"
        Exit Function
    End If
    '*************************************************************
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    
    Set oGestores = New CPersonas
    Set oGestores.Conexion = m_oConexion
    
    sConsulta = "SELECT DISTINCT P5I.GESTOR,PER.NOM,PER.APE"
    sConsulta = sConsulta & " FROM PRES5_IMPORTES P5I WITH (NOLOCK)"
    sConsulta = sConsulta & " INNER JOIN PRES5_UON P5U WITH (NOLOCK) ON P5U.PRES0=P5I.PRES0 AND P5U.PRES1=P5I.PRES1 AND ISNULL(P5U.PRES2,'')=ISNULL(P5I.PRES2,'')"
    sConsulta = sConsulta & " AND ISNULL(P5U.PRES3,'')=ISNULL(P5I.PRES3,'') AND ISNULL(P5U.PRES4,'')=ISNULL(P5I.PRES4,'')"
    sConsulta = sConsulta & " INNER JOIN PER WITH (NOLOCK) ON PER.COD=P5I.GESTOR"
    sConsulta = sConsulta & " WHERE P5U.UON1=?"
    If m_sUON2 <> "" Then sConsulta = sConsulta & " AND P5U.UON2=?"
    If m_sUON3 <> "" Then sConsulta = sConsulta & " AND P5U.UON3=?"
    If m_sUON4 <> "" Then sConsulta = sConsulta & " AND P5U.UON4=?"
    Set oCom = New adodb.Command
    With oCom
        .ActiveConnection = m_oConexion.ADOCon
        .CommandType = adCmdText
        .CommandText = sConsulta
        
        Set oParam = .CreateParameter("@UON1", adVarChar, adParamInput, Len(m_sUON1), m_sUON1)
        .Parameters.Append oParam
        If m_sUON2 <> "" Then
            Set oParam = .CreateParameter("@UON2", adVarChar, adParamInput, Len(m_sUON2), m_sUON2)
            .Parameters.Append oParam
        End If
        If m_sUON3 <> "" Then
            Set oParam = .CreateParameter("@UON3", adVarChar, adParamInput, Len(m_sUON3), m_sUON3)
            .Parameters.Append oParam
        End If
        If m_sUON4 <> "" Then
            Set oParam = .CreateParameter("@UON4", adVarChar, adParamInput, Len(m_sUON4), m_sUON4)
            .Parameters.Append oParam
        End If
        
        Set rsGestores = .Execute
    End With
    Set oParam = Nothing
    Set oCom = Nothing
    
    If Not rsGestores Is Nothing Then
        If rsGestores.RecordCount > 0 Then
            rsGestores.MoveFirst
            While Not rsGestores.eof
                oGestores.Add rsGestores("GESTOR"), rsGestores("APE"), , rsGestores("NOM")
                
                rsGestores.MoveNext
            Wend
        End If
        Set rsGestores = Nothing
    End If
    
    Set DevolverGestoresAsociados = oGestores
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Function
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CCentroCoste", "DevolverGestoresAsociados", ERR, Erl)
        Exit Function
    End If
End Function

''' <summary>Devuelve el código y la denominación del centro de coste en formato de mostrar en pantalla y guardar en BD)</summary>
''' <remarks>Llamada desde: frmFormularios y frmDesgloseValores</remarks>
''' <revision>LTG 27/10/2014</revision>

Public Sub ValorCentroCoste(ByRef sValor As String, ByRef sCodValor As String)
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If m_sUON4 <> "" Then
        sValor = m_sUON4
    Else
        If m_sUON3 <> "" Then
            sValor = m_sUON3
        Else
            If m_sUON2 <> "" Then
                sValor = m_sUON2
            Else
                sValor = m_sUON1
            End If
        End If
    End If
    sValor = sValor & " - " & m_sDen
    If m_sCodConcat <> "" Then sValor = sValor & " (" & m_sCodConcat & ")"
    
    sCodValor = m_sUON1
    If m_sUON2 <> "" Then
        sCodValor = sCodValor & "#" & m_sUON2
        
        If m_sUON3 <> "" Then
            sCodValor = sCodValor & "#" & m_sUON3
            
            If m_sUON4 <> "" Then sCodValor = sCodValor & "#" & m_sUON4
        End If
    End If
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
    Exit Sub
ERROR:
    If ERR.Number <> 0 Then
        Call g_oErrores.TratarError("Class Module", "CCentroCoste", "ValorCentroCoste", ERR, Erl)
        Exit Sub
    End If
End Sub
