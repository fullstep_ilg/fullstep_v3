VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CGrupos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************* CGrupos **********************************
'*             Autor : Elena Mu�oz
'*             Creada : 14/01/2002
'***************************************************************

Option Explicit

Private m_oConexion As CConexion
Private m_oConexionServer As CConexionDeUseServer

Private m_bEOF As Boolean
Private mCol As Collection

Public Function Add(ByVal oProce As CProceso, ByVal sCod As String, ByVal sDen As String, Optional ByVal vIndice As Variant, Optional ByVal vDescripcion As Variant, _
Optional ByVal bDefDestino As Boolean, Optional ByVal vDestCod As Variant, Optional ByVal bDefFechasSum As Boolean, _
Optional ByVal vFechaInicioSuministro As Variant, Optional ByVal vFechaFinSuministro As Variant, Optional ByVal bDefFormaPago As Boolean, _
Optional ByVal vPagCod As Variant, Optional ByVal bDefProveActual As Boolean, Optional ByVal vProveAct As Variant, Optional ByVal bDefDistribUON As Boolean, _
Optional ByVal bHayDistribucionUON As Boolean, Optional ByVal bDefPresAnualTipo1 As Boolean, Optional ByVal bHayPresAnualTipo1 As Boolean, _
Optional ByVal bDefPresAnualTipo2 As Boolean, Optional ByVal bHayPresAnualTipo2 As Boolean, Optional ByVal bDefPresTipo1 As Boolean, _
Optional ByVal bHayPresTipo1 As Boolean, Optional ByVal bDefPresTipo2 As Boolean, Optional ByVal bHayPresTipo2 As Boolean, _
Optional ByVal bDefEspecificaciones As Boolean, Optional ByVal vEsp As Variant, Optional ByVal oEspecificaciones As CEspecificaciones, _
Optional ByVal oItems As CItems, Optional ByVal oAtributos As CAtributos, Optional ByVal vFecAct As Variant, Optional ByVal vCerrado As Variant, _
Optional ByVal vIDPlantilla As Variant, Optional ByVal bHayAtributos As Variant, Optional ByVal iNumItems As Long, _
Optional ByVal vDestDen As Variant, Optional ByVal vPagDen As Variant, Optional ByVal vProveActDen As Variant, _
Optional ByVal bDefSolicitud As Boolean, Optional ByVal vSolicId As Variant, Optional ByVal vSobre As Variant, _
Optional ByVal oAtributosEspecificacion As CAtributos, Optional ByVal lId As Long, Optional ByVal vPrecsalida As Variant, _
Optional ByVal vMinPujGanador As Variant, Optional ByVal vMinPujProve As Variant, Optional ByVal vEscalados As Variant, _
Optional ByVal vEscalaTipo As Variant, Optional ByVal vDistsNivel1, Optional ByVal vDistsNivel2, _
Optional ByVal vDistsNivel3, Optional iBloqueo As Integer = 0) As CGrupo
    
    'create a new object
    Dim objnewmember As CGrupo
If Not g_oErrores.fg_bProgramando Then On Error GoTo Error
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set objnewmember = New CGrupo
   
    Set objnewmember.Proceso = oProce
    objnewmember.Id = lId
    objnewmember.Codigo = sCod
    objnewmember.Den = sDen
    
    If IsMissing(vCerrado) Then
        objnewmember.Cerrado = Null
    Else
        objnewmember.Cerrado = vCerrado
    End If
    
    If IsMissing(vDescripcion) Then
        objnewmember.Descripcion = Null
    Else
        objnewmember.Descripcion = vDescripcion
    End If
    
    objnewmember.DefDestino = bDefDestino
    
    If IsMissing(vDestCod) Then
        objnewmember.DestCod = Null
    Else
        objnewmember.DestCod = vDestCod
    End If
    
    objnewmember.DefFechasSum = bDefFechasSum
    
    If IsMissing(vFechaInicioSuministro) Then
        objnewmember.FechaInicioSuministro = Null
    Else
        objnewmember.FechaInicioSuministro = vFechaInicioSuministro
    End If
    
    If IsMissing(vFechaFinSuministro) Then
        objnewmember.FechaFinSuministro = Null
    Else
        objnewmember.FechaFinSuministro = vFechaFinSuministro
    End If
    
    objnewmember.DefFormaPago = bDefFormaPago
    
    If IsMissing(vPagCod) Then
        objnewmember.PagCod = Null
    Else
        objnewmember.PagCod = vPagCod
    End If
    
    objnewmember.DefProveActual = bDefProveActual
    
    If IsMissing(vProveAct) Then
        objnewmember.ProveActual = Null
    Else
        objnewmember.ProveActual = vProveAct
    End If
    
    objnewmember.DefSolicitud = bDefSolicitud

    If IsMissing(vSolicId) Then
        objnewmember.SolicitudId = Null
    Else
        objnewmember.SolicitudId = vSolicId
    End If
    
    If IsMissing(vIDPlantilla) Then
        objnewmember.IdPlantilla = Null
    Else
        objnewmember.IdPlantilla = vIDPlantilla
    End If
    
    objnewmember.DefDistribUON = bDefDistribUON
    objnewmember.HayDistribucionUON = bHayDistribucionUON
    objnewmember.DefPresAnualTipo1 = bDefPresAnualTipo1
    objnewmember.HayPresAnualTipo1 = bHayPresAnualTipo1
    objnewmember.DefPresAnualTipo2 = bDefPresAnualTipo2
    objnewmember.HayPresAnualTipo2 = bHayPresAnualTipo2
    objnewmember.DefPresTipo1 = bDefPresTipo1
    objnewmember.HayPresTipo1 = bHayPresTipo1
    objnewmember.DefPresTipo2 = bDefPresTipo2
    objnewmember.HayPresTipo2 = bHayPresTipo2
    objnewmember.DefEspecificaciones = bDefEspecificaciones
    
    If Not IsMissing(vDistsNivel1) Then
        Set objnewmember.DistsNivel1 = vDistsNivel1
    End If
    
    If Not IsMissing(vDistsNivel2) Then
        Set objnewmember.DistsNivel2 = vDistsNivel2
    End If
    
    If Not IsMissing(vDistsNivel3) Then
        Set objnewmember.DistsNivel3 = vDistsNivel3
    End If
    
    If IsMissing(vEsp) Then
        objnewmember.Esp = Null
    Else
        objnewmember.Esp = vEsp
    End If
    
    Set objnewmember.Especificaciones = oEspecificaciones
    Set objnewmember.Items = oItems
    Set objnewmember.Atributos = oAtributos
    
    If Not oAtributosEspecificacion Is Nothing Then
        Set objnewmember.AtributosEspecificacion = oAtributosEspecificacion
    End If
    
    objnewmember.FecAct = vFecAct
    
    If Not IsMissing(bHayAtributos) Then
        objnewmember.HayAtributos = True
    End If
    If Not IsMissing(iNumItems) Then
        objnewmember.NumItems = iNumItems
    End If
    
    If IsMissing(vDestDen) Then
        objnewmember.DestDen = Null
    Else
        objnewmember.DestDen = vDestDen
    End If
    
    If IsMissing(vPagDen) Then
        objnewmember.PagDen = Null
    Else
        objnewmember.PagDen = vPagDen
    End If
    
    If IsMissing(vProveActDen) Then
        objnewmember.ProveActDen = Null
    Else
        objnewmember.ProveActDen = vProveActDen
    End If
    
    If IsMissing(vSobre) Then
        objnewmember.Sobre = Null
    Else
        objnewmember.Sobre = vSobre
    End If
    
    Set objnewmember.Conexion = m_oConexion
           
    If Not IsMissing(vIndice) And Not IsNull(vIndice) Then
        objnewmember.Indice = vIndice
        mCol.Add objnewmember, CStr(vIndice)
    Else
        objnewmember.Indice = mCol.Count
        'mCol.Add objnewmember, "#" & CStr(lId)
        mCol.Add objnewmember, CStr(sCod)
    End If
    
    If IsMissing(vPrecsalida) Then
        objnewmember.PrecSalida = Null
    Else
        objnewmember.PrecSalida = vPrecsalida
    End If
    
    If IsMissing(vMinPujGanador) Then
        objnewmember.MinPujGanador = Null
    Else
        objnewmember.MinPujGanador = vMinPujGanador
    End If
    
    If IsMissing(vMinPujProve) Then
        objnewmember.MinPujProve = Null
    Else
        objnewmember.MinPujProve = vMinPujProve
    End If
    
    If IsMissing(vEscalados) Then
        objnewmember.UsarEscalados = 0
    Else
        objnewmember.UsarEscalados = vEscalados
    End If
    
    If IsMissing(vEscalaTipo) Then
        objnewmember.TipoEscalados = ModoDirecto
    Else
        objnewmember.TipoEscalados = vEscalaTipo
    End If
    objnewmember.Bloqueo = iBloqueo
    
    'return the object created
    Set Add = objnewmember
    Set objnewmember = Nothing

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CGrupos", "Add", ERR, Erl)
      Exit Function
   End If

End Function

Friend Property Set Conexion(ByVal con As CConexion)
    Set m_oConexion = con
End Property

Friend Property Get Conexion() As CConexion
    Set Conexion = m_oConexion
End Property

Friend Property Set ConexionServer(ByVal con As CConexionDeUseServer)
    Set m_oConexionServer = con
End Property

Friend Property Get ConexionServer() As CConexionDeUseServer
    Set ConexionServer = m_oConexionServer
End Property

Public Property Get Count() As Long
    If mCol Is Nothing Then
        Count = 0
    Else
         Count = mCol.Count
    End If
End Property

Public Property Get eof() As Boolean
    eof = m_bEOF
End Property

Friend Property Let eof(ByVal b As Boolean)
    m_bEOF = b
End Property

Public Property Get Item(vntIndexKey As Variant) As CGrupo
Attribute Item.VB_MemberFlags = "200"
On Error GoTo NoSeEncuentra:

    'Set Item = mCol("#" & vntIndexKey)
    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property

Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
     Set NewEnum = mCol.[_NewEnum]
End Property

Public Sub Remove(vntIndexKey As Variant)

If Not g_oErrores.fg_bProgramando Then On Error GoTo Error
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    mCol.Remove vntIndexKey
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CGrupos", "Remove", ERR, Erl)
      Exit Sub
   End If

End Sub

Private Sub Class_Initialize()
    Set mCol = New Collection
End Sub

Private Sub Class_Terminate()
    'Proceroys collection when this class is terminated
    On Error Resume Next
    Set mCol = Nothing
    Set m_oConexion = Nothing
End Sub

Public Function DevolverGruposConAdjudicaciones(ByVal iAnyo As Integer, ByVal sGMN1Cod As String, ByVal iCod As Long, Optional ByVal sProve As String) As adodb.Recordset
Dim rs As adodb.Recordset
Dim adocom As adodb.Command
Dim oParam As adodb.Parameter

    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + 613, "CGrupos.DevolverGruposConAdjudicaciones", "No se ha establecido la conexion"
        Exit Function
    End If
If Not g_oErrores.fg_bProgramando Then On Error GoTo Error
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    
    
    ''' Preparar la SP y sus parametros
    Set adocom = New adodb.Command
    Set adocom.ActiveConnection = m_oConexion.ADOCon

    adocom.CommandText = "SP_DEVOLVER_GRUPOS_ADJS"

    Set oParam = adocom.CreateParameter("ANYO", adInteger, adParamInput, , iAnyo)
    adocom.Parameters.Append oParam
    Set oParam = adocom.CreateParameter("GMN1", adVarChar, adParamInput, 50, sGMN1Cod)
    adocom.Parameters.Append oParam
    Set oParam = adocom.CreateParameter("PROCE", adInteger, adParamInput, , iCod)
    adocom.Parameters.Append oParam
    If sProve <> "" Then
        Set oParam = adocom.CreateParameter("PROVE", adVarChar, adParamInput, 100, sProve)
        adocom.Parameters.Append oParam
    End If
    
    adocom.CommandType = adCmdStoredProc

    ''' Ejecutar la SP
    Set rs = adocom.Execute
    
    Set adocom.ActiveConnection = Nothing
    Set adocom = Nothing

            
    Set rs.ActiveConnection = Nothing
        
    Set DevolverGruposConAdjudicaciones = rs
    
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
Error:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CGrupos", "DevolverGruposConAdjudicaciones", ERR, Erl)
      Exit Function
   End If

End Function

Public Sub addGrupo(ByRef oGrupo As CGrupo)
If Not g_oErrores.fg_bProgramando Then On Error GoTo Error
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    mCol.Add oGrupo, oGrupo.Codigo
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
Error:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CGrupos", "addGrupo", ERR, Erl)
      Exit Sub
   End If
End Sub

Public Function Existe(ByVal Cod As String)
    Dim o As CGrupo
    Existe = False
On Error GoTo ERR
   
    Existe = False
    Set o = mCol(Cod)
    
    Existe = True
 Exit Function
 
ERR:
   Existe = False
End Function
