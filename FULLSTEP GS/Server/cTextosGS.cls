VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "cTextosGS"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Enum TipoDeError
    ConexionNoEstablecida = 613
End Enum

Private mvarConexion As CConexion

Friend Property Set Conexion(ByVal con As CConexion)
    Set mvarConexion = con
End Property

Friend Property Get Conexion() As CConexion
    Set Conexion = mvarConexion
End Property

''' <summary>
''' Devuelve el mensaje de error
''' </summary>
''' <param name="Modulo">Nombre del m�dulo</param>
''' <param name="ID">Identificador del error</param>
''' <param optional name="Idioma">Idioma</param>
''' <param optional name="StrError">Cadena de error</param>
''' <returns>String</returns>
''' <remarks>Llamada desde: FSGClient;
''' Tiempo m�ximo: < 1seg </remarks>
''' <revision>JVS 09/01/2012</revision>
Public Function MensajeError(ByVal Modulo As Integer, ByVal Id As Integer, Optional ByVal Idioma As String = "SPA", Optional ByVal StrError As String = "") As String
    Dim rs As New adodb.Recordset
    Dim sConsulta As String
    Dim sRes As String
    
    
    '********* Precondicion **************************************
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    If mvarConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "cTextosGS", "No se ha establecido la conexion"
        Exit Function
    End If
    '*************************************************************
    sRes = ""
    
    sConsulta = "SELECT TEXT_" & Idioma & " FROM TEXTOS_GS WITH(NOLOCK) WHERE MODULO=" & Modulo & " AND ID=" & Id
    
    rs.Open sConsulta, mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    
    If Not rs.eof Then
        sRes = NullToStr(rs.Fields(0).Value)
    End If
    
    If InStr(sRes, "@") <> 0 Then
        sRes = Replace(sRes, "@", StrError)
    End If

    MensajeError = sRes
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "cTextosGS", "MensajeError", ERR, Erl)
      Exit Function
   End If
End Function
