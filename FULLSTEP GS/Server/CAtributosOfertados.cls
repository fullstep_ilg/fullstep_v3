VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CAtributosOfertados"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************* CAtributosOfertados **********************************
'*             Autor : Mertxe Mart�n
'*             Creada : 16/04/2002
'****************************************************************

Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
   
End Enum

Private mCol As Collection
Private m_oConexion As CConexion
Private mvarEOF As Boolean

Friend Property Set Conexion(ByVal con As CConexion)
Set m_oConexion = con
End Property
Friend Property Get Conexion() As CConexion
Set Conexion = m_oConexion
End Property
Public Property Get Item(vntIndexKey As Variant) As CAtributoOfertado
On Error GoTo NoSeEncuentra:

    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property

Public Property Get Count() As Long

If mCol Is Nothing Then
    Count = 0
Else
     Count = mCol.Count
End If

End Property
Public Property Get eof() As Boolean
    eof = mvarEOF
End Property

Friend Property Let eof(ByVal b As Boolean)
    mvarEOF = b
End Property
Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
    Set NewEnum = mCol.[_NewEnum]
End Property


''' <summary> A�ade un elemento a la colecci�n con sus propiedades</summary>
''' <param name="IdAtribProce">ID del atributo en el proceso (PROCE_ATRIB.ID)</param>
''' <param name="UltimaOferta">Objeto Oferta de la ultima oferta</param>
''' <param name="IDitem">ID del atributo </param>
''' <param name="IdGrupro">ID del grupo al que pertenece </param>
''' <param name="valor_bool">Valor del atributo (en caso de ser booleano)</param>
''' <param name="valor_fec">Valor del atributo (en caso de ser de tipo fecha)</param>
''' <param name="valor_num">Valor del atributo (en caso de ser num�rico)</param>
''' <param name="ValorPond">Valor de ponderacion</param>
''' <param name="valor_text">Valor del atributo (en caso de ser de tipo texto)</param>
''' <param name="varIndice"></param>
''' <param name="vFecact">fecha actualizacion</param>
''' <param name="oObjeto"></param>
''' <param name="bConProv">Con proveedores</param>
''' <param name="bValidacion_ERP">Validacion erp</param>
''' <param name="Obligatorio">Si es obligatorio</param>
''' <param name="OrdenarIntegracion">Pedidos emisi�n. El orden en el q mostrar es atribs integraci�n primero.</param>
''' <returns>Atributo con las propiedades actualizadas</returns>
''' <remarks>Llamada desde: Tiempo m�ximo < 0,1</remarks>

Public Function Add(ByVal IdAtribProce As Long, Optional ByVal UltimaOferta As COferta, _
Optional ByVal idItem As Variant, Optional ByVal IdGrupo As Variant, _
Optional ByVal ValorBool As Variant, Optional ByVal ValorFec As Variant, Optional ByVal ValorNum As Variant, _
Optional ByVal ValorPond As Variant, Optional ByVal ValorText As Variant, Optional ByVal varIndice As Variant, _
Optional ByVal vFecAct As Variant, Optional ByVal oObjeto As Object, Optional ByVal bConProv As Boolean, _
Optional ByVal bValidacion_ERP As Boolean = False, Optional ByVal Obligatorio As Boolean = False, _
Optional ByVal OrdenarIntegracion As Boolean = False) As CAtributoOfertado
           
    Dim objnewmember As CAtributoOfertado
    Dim sCod As String
    
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    Set objnewmember = New CAtributoOfertado
    Set objnewmember.Conexion = m_oConexion
       
    If Not IsMissing(ValorBool) Then
        objnewmember.ValorBool = ValorBool
    Else
        objnewmember.ValorBool = Null
    End If
    
    If Not IsMissing(ValorFec) Then
        objnewmember.ValorFec = ValorFec
    Else
        objnewmember.ValorFec = Null
    End If
    
    If Not IsMissing(oObjeto) Then
        Set objnewmember.Objeto = oObjeto
    Else
        Set objnewmember.Objeto = Nothing
    End If
    
    If Not IsMissing(ValorNum) Then
        objnewmember.ValorNum = ValorNum
    Else
        objnewmember.ValorNum = Null
    End If
    
    If Not IsMissing(ValorPond) Then
        objnewmember.ValorPond = ValorPond
    Else
        objnewmember.ValorPond = Null
    End If
    
    If Not IsMissing(ValorText) Then
        objnewmember.ValorText = ValorText
    Else
        objnewmember.ValorText = Null
    End If
        
    If Not IsMissing(vFecAct) Then
        objnewmember.FecAct = vFecAct
    Else
        objnewmember.FecAct = Null
    End If
    
    objnewmember.IdAtribProce = IdAtribProce
    Set objnewmember.UltimaOferta = UltimaOferta
    
    objnewmember.Validacion_ERP = bValidacion_ERP
    
    objnewmember.Obligatorio = Obligatorio
    
    objnewmember.OrdenarIntegracion = OrdenarIntegracion
        
    'Dependiendo de si es un atributo de proceso,grupo o item lo a�ade en la colecci�n.
    If Not IsMissing(varIndice) And Not IsNull(varIndice) Then
        objnewmember.Indice = varIndice
        mCol.Add objnewmember, CStr(varIndice)
        
    ElseIf bConProv = True Then 'Esto se usa en frmDetAtribProce
        If Not IsMissing(idItem) Then  'atributos de nivel de item
            objnewmember.Item = idItem
            sCod = CStr(UltimaOferta.Prove) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(CStr(UltimaOferta.Prove)))
            sCod = sCod & CStr(idItem)
            mCol.Add objnewmember, sCod & "$" & CStr(IdAtribProce)
        ElseIf Not IsMissing(IdGrupo) Then   'atributos de nivel de grupo
            objnewmember.Grupo = IdGrupo
            sCod = CStr(UltimaOferta.Prove) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(CStr(UltimaOferta.Prove)))
            sCod = sCod & CStr(IdGrupo) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGRUPOPROCE - Len(CStr(IdGrupo)))
            mCol.Add objnewmember, sCod & CStr(IdAtribProce)
        Else  'atributos de nivel de proceso
            sCod = CStr(UltimaOferta.Prove) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodPROVE - Len(CStr(UltimaOferta.Prove)))
            mCol.Add objnewmember, sCod & CStr(IdAtribProce)
        End If
        
    ElseIf Not IsMissing(idItem) And Not IsNull(idItem) Then  'atributos de nivel de item
        objnewmember.Item = idItem
        mCol.Add objnewmember, CStr(idItem) & "$" & CStr(IdAtribProce)
        
    ElseIf Not IsMissing(IdGrupo) And Not IsNull(IdGrupo) Then   'atributos de nivel de grupo
        objnewmember.Grupo = IdGrupo
        sCod = CStr(IdGrupo) & Mid$("                         ", 1, basParametros.gLongitudesDeCodigos.giLongCodGRUPOPROCE - Len(CStr(IdGrupo)))
        sCod = sCod & CStr(IdAtribProce)
        'sCod = CStr(IdGrupo) & "_" & CStr(IdAtribProce)

        mCol.Add objnewmember, sCod
        
    Else  'atributos de nivel de proceso
        mCol.Add objnewmember, CStr(IdAtribProce)
        
    End If
        
    
    Set Add = objnewmember
    Set objnewmember = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CAtributosOfertados", "Add", ERR, Erl)
      Exit Function
   End If

End Function
Public Sub Remove(vntIndexKey As Variant)

   
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    mCol.Remove vntIndexKey
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CAtributosOfertados", "Remove", ERR, Erl)
      Exit Sub
   End If
End Sub
Private Sub Class_Initialize()
   
    '*-*'

    Set mCol = New Collection
End Sub

Private Sub Class_Terminate()
    'destroys collection when this class is terminated
    Set mCol = Nothing
    Set m_oConexion = Nothing
    
End Sub


'--<summary>
'-- Guarda los cambios realizados en un atributo de una oferta
'--</summary>
'--<param name="sOp">"I" -> Insert "U"-> Update "D" -> Delete</param>
'--<param name="oOferta">Objeto Oferta</param>
'--<param name="lAtrib">identificador de Atributo</param>
'--<param optional name="vValor">valor para el Atributo</param>
'--<param optional name="lGrupo">identificador de Grupo</param>
'--<param optional name="lItem">identificador de Item</param>
'--<param optional name="dblCambio">Cambio de la moneda</param>
'--<param optional name="bBorrarADj">Orden de borrar las Adjudicaciones</param>
'--<param optional name="oAtrib">Objeto Atributo (para el rec�lculo de la oferta)</param>
'--<param optional name="lIdEscalado">Id del escalado (si el atributo tiene escalados)</param>
'--<returns>Error "TipoErrorSummit" en caso de producirse</returns>
'--<remarks>Llamada desde: frmADJItem, frmOFERec,frmADJ, frmRESREU; Tiempo m�ximo �?</remarks>
'--<revision>DPD 30/06/2011</revision>

Public Function GuardarAtributoOfertado(ByVal sOp As String, ByVal oOferta As COferta, ByVal lAtrib As Long, Optional ByVal vValor As Variant, _
        Optional ByVal lGrupo As Long, Optional ByVal lItem As Variant, Optional ByVal dblCambio As Double, Optional ByVal bBorrarAdj As Boolean, _
        Optional ByVal oAtrib As CAtributo, Optional ByVal lIdEscalado As Long) As TipoErrorSummit
    
    Dim TESError As TipoErrorSummit
    Dim sConsulta As String
    Dim rs As adodb.Recordset
    Dim udtAmbito As TipoAmbitoProceso
    Dim bLista As Boolean
    Dim vMinimo As Variant
    Dim vMaximo As Variant
    Dim udtTipo As TiposDeAtributos
    Dim vAplicPrec As Variant
    Dim vOPPrec As Variant
    Dim adoComm As adodb.Command
    Dim adoParam As adodb.Parameter
    
    '********* Precondicion *******************
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CAtributosOfertados.GuardarAtributoOfertado", "No se ha establecido la conexion"
    End If
    '******************************************
On Error GoTo Error_Cls:
    
    m_oConexion.BEGINTRANSACTION
    sConsulta = "SELECT P.AMBITO,P.INTRO,P.MINNUM,P.MAXNUM,P.MINFEC,P.MAXFEC,D.TIPO,P.APLIC_PREC,P.OP_PREC FROM PROCE_ATRIB P WITH (NOLOCK) INNER JOIN DEF_ATRIB D WITH (NOLOCK) ON P.ATRIB=D.ID WHERE P.ID=" & lAtrib
    Set rs = New adodb.Recordset
    rs.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
    If rs.eof Then
        rs.Close
        Set rs = Nothing
        TESError.NumError = TESDatoEliminado
        TESError.Arg1 = 111 '"ATRIBUTO"
        m_oConexion.ROLLBACKTRANSACTION
        GuardarAtributoOfertado = TESError
        Exit Function
    End If
    If sOp <> "D" Then
        udtAmbito = rs("AMBITO").Value
        'Comprueba el ambito
        If Not IsMissing(lItem) Then
            If udtAmbito <> AmbItem Then
                rs.Close
                Set rs = Nothing
                TESError.NumError = TESInfActualModificada
                TESError.Arg1 = 111 '"ATRIBUTO"
                GuardarAtributoOfertado = TESError
                m_oConexion.ROLLBACKTRANSACTION
                Exit Function
            End If
        ElseIf Not IsMissing(lGrupo) And (lGrupo > 0) Then
            If udtAmbito <> AmbGrupo Then
                rs.Close
                Set rs = Nothing
                TESError.NumError = TESInfActualModificada
                TESError.Arg1 = 111 '"ATRIBUTO"
                GuardarAtributoOfertado = TESError
                m_oConexion.ROLLBACKTRANSACTION
                Exit Function
            End If
        Else
            If udtAmbito <> AmbProceso Then
                rs.Close
                Set rs = Nothing
                TESError.NumError = TESInfActualModificada
                TESError.Arg1 = 111 '"ATRIBUTO"
                GuardarAtributoOfertado = TESError
                m_oConexion.ROLLBACKTRANSACTION
                Exit Function
            End If
        End If
        'Comprueba el tipo y la introducci�n
        bLista = SQLBinaryToBoolean(rs("INTRO").Value)
        udtTipo = rs("TIPO").Value
        vAplicPrec = rs("APLIC_PREC").Value
        vOPPrec = rs("OP_PREC").Value
        If Not bLista Then
            Select Case udtTipo
            Case TipoFecha
                If Not IsDate(vValor) Then
                    rs.Close
                    Set rs = Nothing
                    TESError.NumError = TESInfActualModificada
                    TESError.Arg1 = 111 '"ATRIBUTO"
                    GuardarAtributoOfertado = TESError
                    m_oConexion.ROLLBACKTRANSACTION
                    Exit Function
                End If
                vMinimo = rs("MINFEC").Value
                vMaximo = rs("MAXFEC").Value
                If Not IsNull(vMinimo) Then
                    If vMinimo > CDate(vValor) Then
                        rs.Close
                        Set rs = Nothing
                        TESError.NumError = TESInfActualModificada
                        TESError.Arg1 = 111 '"ATRIBUTO"
                        GuardarAtributoOfertado = TESError
                        m_oConexion.ROLLBACKTRANSACTION
                        Exit Function
                    End If
                End If
                If Not IsNull(vMaximo) Then
                    If vMaximo < CDate(vValor) Then
                        rs.Close
                        Set rs = Nothing
                        TESError.NumError = TESInfActualModificada
                        TESError.Arg1 = 111 '"ATRIBUTO"
                        GuardarAtributoOfertado = TESError
                        m_oConexion.ROLLBACKTRANSACTION
                        Exit Function
                    End If
                End If
            Case TipoNumerico
                If Not IsNumeric(vValor) Then
                    rs.Close
                    Set rs = Nothing
                    TESError.NumError = TESInfActualModificada
                    TESError.Arg1 = 111 '"ATRIBUTO"
                    GuardarAtributoOfertado = TESError
                    m_oConexion.ROLLBACKTRANSACTION
                    Exit Function
                End If
                
                vMinimo = rs("MINNUM").Value
                vMaximo = rs("MAXNUM").Value
                If Not IsNull(vMinimo) Then
                    If Not IsNull(vAplicPrec) And (vOPPrec = "+" Or vOPPrec = "-") And dblCambio <> 0 Then
                        vMinimo = CDec(vMinimo) * dblCambio
                    End If
                    If vMinimo > CDbl(vValor) Then
                        rs.Close
                        Set rs = Nothing
                        TESError.NumError = TESInfActualModificada
                        TESError.Arg1 = 111 '"ATRIBUTO"
                        GuardarAtributoOfertado = TESError
                        m_oConexion.ROLLBACKTRANSACTION
                        Exit Function
                    End If
                End If
                If Not IsNull(vMaximo) Then
                    If Not IsNull(vAplicPrec) And (vOPPrec = "+" Or vOPPrec = "-") And dblCambio <> 0 Then
                        vMaximo = CDec(vMaximo) * dblCambio
                    End If
                    If vMaximo < CDbl(vValor) Then
                        rs.Close
                        Set rs = Nothing
                        TESError.NumError = TESInfActualModificada
                        TESError.Arg1 = 111 '"ATRIBUTO"
                        GuardarAtributoOfertado = TESError
                        m_oConexion.ROLLBACKTRANSACTION
                        Exit Function
                    End If
                End If
            Case TipoString
                If TypeName(vValor) <> "String" Then
                        rs.Close
                        Set rs = Nothing
                        TESError.NumError = TESInfActualModificada
                        TESError.Arg1 = 111 '"ATRIBUTO"
                        GuardarAtributoOfertado = TESError
                        m_oConexion.ROLLBACKTRANSACTION
                        Exit Function
                End If
            Case TipoBoolean
                If TypeName(vValor) <> "Boolean" Then
                        rs.Close
                        Set rs = Nothing
                        TESError.NumError = TESInfActualModificada
                        TESError.Arg1 = 111 '"ATRIBUTO"
                        GuardarAtributoOfertado = TESError
                        m_oConexion.ROLLBACKTRANSACTION
                        Exit Function
                End If
            End Select
            rs.Close
        Else
            rs.Close
            Select Case udtTipo
            Case TipoString
                If TypeName(vValor) = "String" Then
                    sConsulta = "SELECT COUNT(*) FROM PROCE_ATRIB_LISTA WITH (NOLOCK) WHERE ATRIB_ID=" & lAtrib & " AND VALOR_TEXT ='" & DblQuote(vValor) & "'"
                    rs.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
                    If rs(0).Value = 0 Then
                        rs.Close
                        Set rs = Nothing
                        TESError.NumError = TESInfActualModificada
                        TESError.Arg1 = 111 '"ATRIBUTO"
                        GuardarAtributoOfertado = TESError
                        m_oConexion.ROLLBACKTRANSACTION
                        Exit Function
                    End If
                    rs.Close
                End If
            Case TipoFecha
                If IsDate(vValor) Then
                    sConsulta = "SELECT COUNT(*) FROM PROCE_ATRIB_LISTA WITH(NOLOCK) WHERE ATRIB_ID=" & lAtrib & " AND VALOR_FEC =" & DateToSQLDate(vValor)
                    rs.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
                    If rs(0).Value = 0 Then
                        rs.Close
                        Set rs = Nothing
                        TESError.NumError = TESInfActualModificada
                        TESError.Arg1 = 111 '"ATRIBUTO"
                        GuardarAtributoOfertado = TESError
                        m_oConexion.ROLLBACKTRANSACTION
                        Exit Function
                    End If
                    rs.Close
                End If
            Case TipoNumerico
                If IsNumeric(vValor) Then
                    If Not IsNull(vAplicPrec) And (vOPPrec = "+" Or vOPPrec = "-") And dblCambio <> 0 Then   'Es la misma moneda
                        sConsulta = "SELECT COUNT(*) FROM PROCE_ATRIB_LISTA WITH(NOLOCK) WHERE ATRIB_ID=" & lAtrib & " AND VALOR_NUM =" & DblToSQLFloat(vValor / dblCambio)
                    Else
                        sConsulta = "SELECT COUNT(*) FROM PROCE_ATRIB_LISTA WITH(NOLOCK) WHERE ATRIB_ID=" & lAtrib & " AND VALOR_NUM =" & DblToSQLFloat(vValor)
                    End If
                    rs.Open sConsulta, m_oConexion.ADOCon, adOpenStatic, adLockReadOnly
                    If rs(0).Value = 0 Then
                        rs.Close
                        Set rs = Nothing
                        TESError.NumError = TESInfActualModificada
                        TESError.Arg1 = 111 '"ATRIBUTO"
                        GuardarAtributoOfertado = TESError
                        m_oConexion.ROLLBACKTRANSACTION
                        Exit Function
                    End If
                    rs.Close
                End If
            Case TipoBoolean
                If TypeName(vValor) <> "Boolean" Then
                        rs.Close
                        Set rs = Nothing
                        TESError.NumError = TESInfActualModificada
                        TESError.Arg1 = 111 '"ATRIBUTO"
                        GuardarAtributoOfertado = TESError
                        m_oConexion.ROLLBACKTRANSACTION
                        Exit Function
                End If
            End Select
        End If
    End If
    
    Set adoComm = New adodb.Command
    
    Set adoComm.ActiveConnection = m_oConexion.ADOCon
    
    With oOferta
        Set adoParam = adoComm.CreateParameter(Type:=adInteger, Direction:=adParamInput, Value:=.Anyo)
        adoComm.Parameters.Append adoParam
        
        Set adoParam = adoComm.CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=Len(.GMN1Cod), Value:=.GMN1Cod)
        adoComm.Parameters.Append adoParam
        
        Set adoParam = adoComm.CreateParameter(Type:=adInteger, Direction:=adParamInput, Value:=.Proce)
        adoComm.Parameters.Append adoParam
        
        Set adoParam = adoComm.CreateParameter(Type:=adInteger, Direction:=adParamInput, Value:=lAtrib)
        adoComm.Parameters.Append adoParam
        
        Set adoParam = adoComm.CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=Len(.Prove), Value:=.Prove)
        adoComm.Parameters.Append adoParam
        
        Set adoParam = adoComm.CreateParameter(Type:=adInteger, Direction:=adParamInput, Value:=.Num)
        adoComm.Parameters.Append adoParam
    End With
            
    If Not IsMissing(lItem) Then
        Set adoParam = adoComm.CreateParameter(Type:=adInteger, Direction:=adParamInput, Value:=lItem)
        adoComm.Parameters.Append adoParam
        Set adoParam = adoComm.CreateParameter(Type:=adInteger, Direction:=adParamInput, Value:=lIdEscalado)
        adoComm.Parameters.Append adoParam
        sConsulta = "EXEC SP_MODIF_ATRIB_OFE_ITEM @ANYO =?, @GMN1 =?, @PROCE =?, @ATRIB =?, @PROVE =?, @OFE =?, @ITEM=?, @ESC=?"
    ElseIf Not IsMissing(lGrupo) And (lGrupo > 0) Then
        Set adoParam = adoComm.CreateParameter(Type:=adSmallInt, Direction:=adParamInput, Value:=lGrupo)
        adoComm.Parameters.Append adoParam
        sConsulta = "EXEC SP_MODIF_ATRIB_OFE_GR @ANYO =?, @GMN1 =?, @PROCE =?, @ATRIB =?, @PROVE =?, @OFE =?, @GRUPO=?"
    Else
        sConsulta = "EXEC SP_MODIF_ATRIB_OFE @ANYO =?, @GMN1 =?, @PROCE =?, @ATRIB =?, @PROVE =?, @OFE =?"
    End If
    
    If sOp = "D" Then
        sConsulta = sConsulta & ",@OP=?"
    Else
    Select Case udtTipo
    Case TipoString
        sConsulta = sConsulta & ",@VALOR_TEXT=?,@OP=?"
        Set adoParam = adoComm.CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=Len(vValor), Value:=vValor)
        adoComm.Parameters.Append adoParam
    Case TipoNumerico
        sConsulta = sConsulta & ",@VALOR_NUM=?,@OP=?"
        Set adoParam = adoComm.CreateParameter(Type:=adDouble, Direction:=adParamInput, Value:=CDbl(vValor))
        adoComm.Parameters.Append adoParam
    Case TipoFecha
        sConsulta = sConsulta & ",@VALOR_FEC=?,@OP=?"
        Set adoParam = adoComm.CreateParameter(Type:=adDate, Direction:=adParamInput, Value:=CDate(vValor))
        adoComm.Parameters.Append adoParam
    Case TipoBoolean
        sConsulta = sConsulta & ",@VALOR_BOOL=?,@OP=?"
        Set adoParam = adoComm.CreateParameter(Type:=adBoolean, Direction:=adParamInput, Value:=CBool(vValor))
        adoComm.Parameters.Append adoParam
    End Select
    End If
    Set adoParam = adoComm.CreateParameter(Type:=adVarChar, Direction:=adParamInput, Size:=1, Value:=sOp)
    adoComm.Parameters.Append adoParam
 
    adoComm.CommandText = sConsulta
    Set adoComm.ActiveConnection = m_oConexion.ADOCon
    
    adoComm.CommandType = adCmdText
    
    adoComm.Execute
    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
    Set adoComm = Nothing
    Set adoParam = Nothing
    Set rs = Nothing
    
    
    If bBorrarAdj = True Then
        'Si el proceso tiene adjudicaciones y el atributo modificado es num�rico y est� aplicado al precio borramos las adjudicaciones
        Set adoComm = New adodb.Command
    
        Set adoComm.ActiveConnection = m_oConexion.ADOCon
        sConsulta = "EXEC SP_ELIMINAR_ADJUDICACIONES @ANYO=?, @GMN1=?, @PROCE=?"
        
        Set adoParam = adoComm.CreateParameter("ANYO", adSmallInt, adParamInput, , oOferta.Anyo)
        adoComm.Parameters.Append adoParam
    
        Set adoParam = adoComm.CreateParameter("GMN1", adVarChar, adParamInput, 50, oOferta.GMN1Cod)
        adoComm.Parameters.Append adoParam
    
        Set adoParam = adoComm.CreateParameter("PROCE", adInteger, adParamInput, , oOferta.Proce)
        adoComm.Parameters.Append adoParam
    
        adoComm.CommandType = adCmdText
        adoComm.CommandText = sConsulta
        adoComm.Execute
        If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
        
        Set adoComm = Nothing
        Set adoParam = Nothing
    End If
    
    'Realizamos el recalculo de la oferta si fuera necesario.
    If oOferta.Recalcular Then
        TESError = oOferta.Proceso.RecalcularOfertas(False, oAtrib, oOferta.Num, oOferta.Prove)
        If TESError.NumError <> TESnoerror Then
            GoTo Error_Cls
        End If
    End If
    oOferta.Recalcular = False

    
    m_oConexion.COMMITTRANSACTION
    TESError.NumError = TESnoerror
    GuardarAtributoOfertado = TESError

    'Actualizar campos PM
    If bBorrarAdj = True Then
        Dim oProceso As cProceso
        Set oProceso = New cProceso
        Set oProceso.Conexion = Me.Conexion
        oProceso.Anyo = oOferta.Anyo
        oProceso.GMN1Cod = oOferta.GMN1Cod
        oProceso.Cod = oOferta.Proce
        oProceso.ActualizarCamposPM
        Set oProceso = Nothing
    End If
    
    Exit Function

Error_Cls:
    If m_oConexion.ADOCon.Errors.Count > 0 Then
        GuardarAtributoOfertado = basErrores.TratarError(m_oConexion.ADOCon.Errors)
        m_oConexion.ROLLBACKTRANSACTION
        Set adoComm = Nothing
        Set adoParam = Nothing
        Set rs = Nothing
    ElseIf TESError.NumError <> TESnoerror Then
        m_oConexion.ROLLBACKTRANSACTION
        Set adoComm = Nothing
        Set adoParam = Nothing
        Set rs = Nothing
    ElseIf ERR.Number <> 0 Then
        Call g_oErrores.TratarError("M�dulo de clase", "CAtributosOfertados", "GuardarAtributoOfertado", ERR, Erl)
    End If
End Function


Public Function GuardarPonderacion(ByVal iAmbito As Integer) As TipoErrorSummit
Dim TESError As TipoErrorSummit
Dim sConsulta As String
Dim oAtrib As CAtributoOfertado

'********* Precondicion *******************
If m_oConexion Is Nothing Then
    ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CAtributosOfertados.GuardarPonderacion", "No se ha establecido la conexion"
End If
'******************************************
On Error GoTo Error_Cls:
    
    TESError.NumError = TESnoerror
    
    m_oConexion.BEGINTRANSACTION
    
    Select Case iAmbito
    
        Case AmbGrupo
            For Each oAtrib In Me
                If oAtrib.ValorPond <> "" And Not IsNull(oAtrib.ValorPond) And Not IsEmpty(oAtrib.ValorPond) Then
                    sConsulta = "UPDATE OFE_GR_ATRIB SET VALOR_POND=" & DblToSQLFloat(oAtrib.ValorPond) & " WHERE ANYO=" & oAtrib.UltimaOferta.Anyo
                    sConsulta = sConsulta & " AND GMN1='" & DblQuote(oAtrib.UltimaOferta.GMN1Cod) & "' AND PROCE=" & oAtrib.UltimaOferta.Proce
                    sConsulta = sConsulta & " AND PROVE='" & DblQuote(oAtrib.UltimaOferta.Prove) & "' AND OFE=" & oAtrib.UltimaOferta.Num
                    sConsulta = sConsulta & " AND ATRIB_ID=" & oAtrib.IdAtribProce & " AND GRUPO=" & (oAtrib.Grupo)
                    
                    m_oConexion.ADOCon.Execute sConsulta
                    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
                End If
            Next
                
        Case AmbItem
            For Each oAtrib In Me
                If oAtrib.ValorPond <> "" And Not IsNull(oAtrib.ValorPond) And Not IsEmpty(oAtrib.ValorPond) Then
                    sConsulta = "UPDATE OFE_ITEM_ATRIB SET VALOR_POND=" & DblToSQLFloat(oAtrib.ValorPond) & " WHERE ANYO=" & oAtrib.UltimaOferta.Anyo
                    sConsulta = sConsulta & " AND GMN1='" & DblQuote(oAtrib.UltimaOferta.GMN1Cod) & "' AND PROCE=" & oAtrib.UltimaOferta.Proce
                    sConsulta = sConsulta & " AND PROVE='" & DblQuote(oAtrib.UltimaOferta.Prove) & "' AND OFE=" & oAtrib.UltimaOferta.Num
                    sConsulta = sConsulta & " AND ATRIB_ID=" & oAtrib.IdAtribProce & " AND ITEM='" & DblQuote(oAtrib.Item) & "'"
                    
                    m_oConexion.ADOCon.Execute sConsulta
                    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
                End If
            Next
            
        Case AmbProceso
            For Each oAtrib In Me
                If oAtrib.ValorPond <> "" And Not IsNull(oAtrib.ValorPond) And Not IsEmpty(oAtrib.ValorPond) Then
                    sConsulta = "UPDATE OFE_ATRIB SET VALOR_POND=" & DblToSQLFloat(oAtrib.ValorPond) & " WHERE ANYO=" & oAtrib.UltimaOferta.Anyo
                    sConsulta = sConsulta & " AND GMN1='" & DblQuote(oAtrib.UltimaOferta.GMN1Cod) & "' AND PROCE=" & oAtrib.UltimaOferta.Proce
                    sConsulta = sConsulta & " AND PROVE='" & DblQuote(oAtrib.UltimaOferta.Prove) & "' AND OFE=" & oAtrib.UltimaOferta.Num
                    sConsulta = sConsulta & " AND ATRIB_ID=" & oAtrib.IdAtribProce
                    
                    m_oConexion.ADOCon.Execute sConsulta
                    If m_oConexion.ADOCon.Errors.Count > 0 Then GoTo Error_Cls
                End If
            Next
        
    End Select

    m_oConexion.COMMITTRANSACTION
    
    
    GuardarPonderacion = TESError

    Exit Function


Error_Cls:
    If m_oConexion.ADOCon.Errors.Count > 0 Then
        GuardarPonderacion = basErrores.TratarError(m_oConexion.ADOCon.Errors)
        m_oConexion.ROLLBACKTRANSACTION
    ElseIf ERR.Number <> 0 Then
        Call g_oErrores.TratarError("M�dulo de clase", "CAtributosOfertados", "GuardarPonderacion", ERR, Erl)
    End If
End Function


