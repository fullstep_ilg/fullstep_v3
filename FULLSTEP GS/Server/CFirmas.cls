VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CFirmas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

''' *** Clase: CFirmas
''' *** Creacion: 1/07/1999 (Javier Arana)

''' Variables que almacenan las propiedades de los objetos

Private mCol As Collection
Private mvarEOF As Boolean
Private mvarConexion As CConexion
Private mvarConexionServer As CConexionDeUseServer

''' Control de errores

Private Enum TipoDeError
    ConexionNoEstablecida = 613
End Enum

''' <summary>Carga la coleccion de Firmas</summary>
''' <param name="Cod">C�digo</param>
''' <param name="CaracteresInicialesDen">Caracteres iniciales de la denominaci�n</param>
''' <param name="CoincidenciaTotal">Coincidencia total</param>
''' <param name="OrdenadasPorDen">Orden por denominaci�n</param>
''' <param name="UsarIndice">Usar �ndice</param>
''' <remarks>Llamada desde: FSGSClient; Tiempo m�ximo: 1 sec </remarks>
''' <revision>LTG 04/01/2012</revision>

Public Sub CargarTodasLasFirmas(Optional ByVal Cod As Variant, Optional ByVal CaracteresInicialesDen As String, _
        Optional ByVal CoincidenciaTotal As Boolean = False, Optional ByVal OrdenadasPorDen As Boolean, Optional ByVal UsarIndice As Boolean = False)
    Dim sConsulta As String
    Dim lIndice As Long
    Dim rs As ADODB.Recordset
    Dim fldCod As ADODB.Field
    Dim fldDen As ADODB.Field
    
    ''' Precondicion
    
    If mvarConexion Is Nothing Then
        Err.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CFirmas.CargarTodasLasFirmas", "No se ha establecido la conexion"
        Exit Sub
    End If
        
    ''' Generacion de SQL a partir de los parametros
    If IsMissing(Cod) And CaracteresInicialesDen = "" Then
        
        sConsulta = "SELECT COD,DEN,FECACT FROM FIRMAS WITH (NOLOCK)"
       
    Else
        sConsulta = "SELECT COD,DEN,FECACT FROM FIRMAS WITH (NOLOCK) WHERE"
        
        If Not IsMissing(Cod) And CaracteresInicialesDen = "" Then
            If CoincidenciaTotal Then
                sConsulta = sConsulta & " COD=" & Cod
            Else
                sConsulta = sConsulta & " COD >=" & Cod
            End If
            
        Else
            If IsMissing(Cod) Then
                If CoincidenciaTotal Then
                    sConsulta = sConsulta & " DEN='" & DblQuote(CaracteresInicialesDen) & "'"
                Else
                    sConsulta = sConsulta & " DEN LIKE '" & DblQuote(CaracteresInicialesDen) & "%'"
                End If
                
            Else
                If CoincidenciaTotal Then
                    sConsulta = sConsulta & " COD=" & Cod
                    sConsulta = sConsulta & " DEN='" & DblQuote(CaracteresInicialesDen) & "'"
                Else
                    sConsulta = sConsulta & " COD >=" & Cod
                    sConsulta = sConsulta & " DEN LIKE '" & DblQuote(CaracteresInicialesDen) & "%'"
                End If
            End If
        End If
    End If
    
    If OrdenadasPorDen Then
        sConsulta = sConsulta & " ORDER BY DEN"
    Else
        sConsulta = sConsulta & " ORDER BY COD"
    End If
    
    ''' Creacion del Resultset
    
    Set rs = New ADODB.Recordset
    rs.Open sConsulta, mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    
    Set mCol = Nothing
    Set mCol = New Collection
        
    If Not rs.eof Then
        ''' Carga del Resultset en la coleccion
        
        Set fldCod = rs.Fields(0)
        Set fldDen = rs.Fields(1)
        
        If UsarIndice Then
            
            lIndice = 0
            
            While Not rs.eof
        
                Me.Add fldCod.Value, fldDen.Value, lIndice
                rs.MoveNext
                lIndice = lIndice + 1
                
            Wend
            
        Else
        
            While Not rs.eof
        
                Me.Add fldCod.Value, fldDen.Value
                rs.MoveNext
                
            Wend
            
        End If
    
        Set fldCod = Nothing
        Set fldDen = Nothing
    End If
    
    rs.Close
    Set rs = Nothing
End Sub
Public Function Add(ByVal Cod As Integer, ByVal Den As String, Optional ByVal varIndice As Variant) As CFirma
    
    ''' * Objetivo: Anyadir una Unidad a la coleccion
    ''' * Recibe: Datos de la Unidad
    ''' * Devuelve: Unidad anyadida
    
    Dim objnewmember As CFirma
    
    ''' Creacion de objeto Unidad
    
    Set objnewmember = New CFirma
   
    ''' Paso de los parametros al nuevo objeto Unidad
    
    objnewmember.Den = Den
    
    objnewmember.Cod = Cod
    Set objnewmember.Conexion = mvarConexion
    Set objnewmember.ConexionServer = mvarConexionServer
    
    If Not IsMissing(varIndice) And Not IsNull(varIndice) Then
        objnewmember.Indice = varIndice
        mCol.Add objnewmember, CStr(varIndice)
    Else
        mCol.Add objnewmember, CStr(Cod)
    End If
            
    Set Add = objnewmember
    
    Set objnewmember = Nothing

End Function
Public Property Get Item(vntIndexKey As Variant) As CFirma

    ''' * Objetivo: Recuperar una Unidad de la coleccion
    ''' * Recibe: Indice de la Unidad a recuperar
    ''' * Devuelve: Unidad correspondiente
        
On Error GoTo NoSeEncuentra:

    ''' Devolvemos el item de la coleccion privada
    
    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:

    Set Item = Nothing
    
End Property
Public Property Get eof() As Boolean

    ''' * Objetivo: Devolver variable privada EOF
    ''' * Recibe: Nada
    ''' * Devuelve: Variable privada EOF
    
    eof = mvarEOF
    
End Property
Friend Property Set ConexionServer(ByVal con As CConexionDeUseServer)
Set mvarConexionServer = con
End Property
Friend Property Get ConexionServer() As CConexionDeUseServer
Set ConexionServer = mvarConexionServer
End Property
Friend Property Set Conexion(ByVal con As CConexion)

    ''' * Objetivo: Dar valor a la variable privada Conexion
    ''' * Recibe: Conexion
    ''' * Devuelve: Nada
    
    Set mvarConexion = con
    
End Property
Friend Property Get Conexion() As CConexion

    ''' * Objetivo: Devolver variable privada Conexion
    ''' * Recibe: Nada
    ''' * Devuelve: Conexion
    
    Set Conexion = mvarConexion
    
End Property
Public Property Get Count() As Long

    ''' * Objetivo: Devolver variable privada Count
    ''' * Recibe: Nada
    ''' * Devuelve: Numero de elementos de la coleccion

    If mCol Is Nothing Then
        Count = 0
    Else
        Count = mCol.Count
    End If

End Property
Public Sub Remove(vntIndexKey As Variant)

    ''' * Objetivo: Eliminar una Unidad de la coleccion
    ''' * Recibe: Indice de la Unidad a eliminar
    ''' * Devuelve: Nada
   
    mCol.Remove vntIndexKey
    
End Sub
Private Sub Class_Initialize()

    ''' * Objetivo: Crear la coleccion
    
   

    Set mCol = New Collection
    
End Sub
Private Sub Class_Terminate()

    ''' * Objetivo: Limpiar memoria
    
    Set mCol = Nothing
    Set mvarConexion = Nothing
    Set mvarConexionServer = Nothing
End Sub
Public Function DevolverLosCodigos() As TipoDatosCombo
   
    ''' ! Pendiente de revision, segun tema de combos
    
   Dim Codigos As TipoDatosCombo
   Dim iCont As Integer
   
   ReDim Codigos.Cod(mCol.Count)
   ReDim Codigos.Den(mCol.Count)
   
    For iCont = 0 To mCol.Count - 1
        
        Codigos.Cod(iCont) = mCol(iCont + 1).Cod
        Codigos.Den(iCont) = mCol(iCont + 1).Den
    
    Next

   DevolverLosCodigos = Codigos
   
End Function

''' <summary>Carga la coleccion de Firmas</summary>
''' <param name="NumMaximo">N� max. de registros a cargar</param>
''' <param name="Cod">C�digo</param>
''' <param name="CaracteresInicialesDen">Caracteres iniciales de la denominaci�n</param>
''' <param name="CoincidenciaTotal">Coincidencia total</param>
''' <param name="OrdenadasPorDen">Orden por denominaci�n</param>
''' <param name="UsarIndice">Usar �ndice</param>
''' <remarks>Llamada desde: FSGSClient; Tiempo m�ximo: 1 sec </remarks>
''' <revision>LTG 04/01/2012</revision>

Public Sub CargarTodasLasFirmasDesde(ByVal NumMaximo As Integer, Optional ByVal Cod As Integer, Optional ByVal CaracteresInicialesDen As String, _
        Optional ByVal CoincidenciaTotal As Boolean, Optional ByVal OrdenadasPorDen As Boolean, Optional ByVal UsarIndice As Boolean)
    Dim rs As New ADODB.Recordset
    Dim fldCod As ADODB.Field
    Dim fldDen As ADODB.Field
    Dim sConsulta As String
    
    Dim lIndice As Long
    ''''''''''''''''''''''''Dim iNumMon As Integer
    
    ''' Precondicion
    
    If mvarConexion Is Nothing Then
        Err.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CFirmas.CargarTodosLosDestinos", "No se ha establecido la conexion"
        Exit Sub
    End If
    
    ''' Generacion del SQL a partir de los parametros
    
     ''' Generacion de SQL a partir de los parametros
    If IsMissing(Cod) And CaracteresInicialesDen = "" Then
        
        sConsulta = "SELECT TOP " & NumMaximo & " COD,DEN,FECACT FROM FIRMAS WITH (NOLOCK)"
       
    Else
        sConsulta = "SELECT TOP " & NumMaximo & " COD,DEN,FECACT FROM FIRMAS WITH (NOLOCK) WHERE"
        
        If Not IsMissing(Cod) And CaracteresInicialesDen = "" Then
            
            If CoincidenciaTotal Then
                sConsulta = sConsulta & " COD=" & Cod
            Else
                sConsulta = sConsulta & " COD >=" & Cod
            End If
            
        Else
            If IsMissing(Cod) Then
                        
                If CoincidenciaTotal Then
                    sConsulta = sConsulta & " DEN='" & DblQuote(CaracteresInicialesDen) & "'"
                Else
                    sConsulta = sConsulta & " DEN LIKE '" & DblQuote(CaracteresInicialesDen) & "%'"
                End If
                
            Else
                If CoincidenciaTotal Then
                    sConsulta = sConsulta & " COD=" & Cod
                    sConsulta = sConsulta & " DEN='" & DblQuote(CaracteresInicialesDen) & "'"
                Else
                    sConsulta = sConsulta & " COD >=" & Cod
                    sConsulta = sConsulta & " DEN LIKE '" & DblQuote(CaracteresInicialesDen) & "%'"
                End If
            End If
        End If
    End If
    
    If OrdenadasPorDen Then
        sConsulta = sConsulta & " ORDER BY DEN"
    Else
        sConsulta = sConsulta & " ORDER BY COD"
    End If
    
          
    ''' Crear el Resultset
    
    rs.Open sConsulta, mvarConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    
    Set mCol = Nothing
    Set mCol = New Collection
    
    If Not rs.eof Then
        ''' Llenar la coleccion a partir del Resultset
        ''' teniendo en cuenta el maximo
        
        Set fldCod = rs.Fields(0)
        Set fldDen = rs.Fields(1)
        
        If UsarIndice Then
            
            lIndice = 0
            ''''''''''''''''''''''iNumMon = 0
                    
            While Not rs.eof ''''''''''''''''''''''''And iNumMon < NumMaximo
            
                Me.Add fldCod.Value, fldDen.Value, lIndice
                rs.MoveNext
                lIndice = lIndice + 1
                ''''''''''''''''''''''iNumMon = iNumMon + 1
                
            Wend
            
            If Not rs.eof Then
                mvarEOF = False
            Else
                mvarEOF = True
            End If
                        
        Else
            
            '''''''''''''''''iNumMon = 0
            
            While Not rs.eof ''''''''''''''''''''And iNumMon < NumMaximo
            
                Me.Add fldCod.Value, fldDen.Value
                rs.MoveNext
                ''''''''''''''''''''''iNumMon = iNumMon + 1
                
            Wend
            
            If Not rs.eof Then
                mvarEOF = False
            Else
                mvarEOF = True
            End If
            
        End If
    
        Set fldCod = Nothing
        Set fldDen = Nothing
    End If
    
    rs.Close
    Set rs = Nothing
End Sub
Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"

    ''' ! Pendiente de revision, objetivo desconocido
    
    Set NewEnum = mCol.[_NewEnum]

End Property



