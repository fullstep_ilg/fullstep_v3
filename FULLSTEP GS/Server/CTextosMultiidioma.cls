VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CTextosMultiidioma"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private m_oConexion As CConexion

Friend Property Set Conexion(ByVal con As CConexion)
    Set m_oConexion = con
End Property

Friend Property Get Conexion() As CConexion
    Set Conexion = m_oConexion
End Property

''' <summary>Devuelve los textos de fin de subasta</summary>
''' <param name="lngSubTextoFin">ID de los textos</param>
''' <returns>Objeto de tipo diccionario con los textos</returns>
''' <remarks>Llamada desde: Tiempo m�ximo: 1 seg</remarks>

Public Function DevolverTextosMultiidioma(ByVal lngSubTextoFin As Long) As Dictionary
    Dim sConsulta As String
    Dim AdoRes  As adodb.Recordset
    Dim odcTextos As Dictionary
    
    On Error GoTo Error
    
    sConsulta = "SELECT IDI,TEXTO,TIPO FROM TEXTOS_MULTIIDIOMA WITH (NOLOCK) WHERE ID=" & lngSubTextoFin
    Set AdoRes = New adodb.Recordset
    AdoRes.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    If Not AdoRes.eof Then
        Set odcTextos = New Dictionary
                
        AdoRes.MoveFirst
        While Not AdoRes.eof
            odcTextos.Add AdoRes("IDI").Value, NullToStr(AdoRes("TEXTO").Value)
            AdoRes.MoveNext
        Wend
    End If
    AdoRes.Close
    
    Set DevolverTextosMultiidioma = odcTextos
        
Salir:
    Set AdoRes = Nothing
    Exit Function
Error:
    Resume Salir
End Function

''' <summary>Actualiza o inserta los textos de fin de subasta</summary>
''' <param name="intSubTextoFin">Identificador de los textos.</param>
''' <param name="dcTextos">colecci�n con los textos en cada idioma.</param>
''' <param name="sTipo">Tipo de texto.</param>
''' <returns>Variable de tipo TipoErrosSummit con el error si lo ha habido</returns>
''' <remarks>Llamada desde GuardarParametrosGenerales y GuardarParametrosInstalacion</remarks>

Public Function ActualizarTextosMultiidioma(ByRef lngSubTextoFin As Long, ByVal dcTextos As Dictionary, ByVal sTipo As String) As TipoErrorSummit
    Dim sConsulta As String
    Dim i As Integer
    Dim rs As adodb.Recordset
    Dim TESError As TipoErrorSummit
    
    On Error GoTo Error
        
    TESError.NumError = TESnoerror
        
    If Not dcTextos Is Nothing Then
        If dcTextos.Count > 0 Then
            If lngSubTextoFin = 0 Then
                'Inserci�n de textos
                
                lngSubTextoFin = 1
                
                'Obtener una nueva ID
                sConsulta = "SELECT MAX(ID) AS MAXID FROM TEXTOS_MULTIIDIOMA WITH (NOLOCK)"
                Set rs = New adodb.Recordset
                rs.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
                If Not rs.eof Then
                    lngSubTextoFin = CLng(NullToDbl0(rs("MAXID"))) + 1
                End If
                rs.Close
                
                For i = 0 To dcTextos.Count - 1
                    If dcTextos.Items(i) <> vbNullString Then
                        sConsulta = "INSERT INTO TEXTOS_MULTIIDIOMA (ID,IDI,TEXTO,TIPO) VALUES (" & _
                                    lngSubTextoFin & "," & _
                                    StrToSQLNULL(dcTextos.Keys(i)) & "," & _
                                    StrToSQLNULL(dcTextos.Items(i)) & "," & _
                                    StrToSQLNULL(sTipo) & ")"
                                    
                        TESError.Arg1 = lngSubTextoFin
                                    
                        m_oConexion.ADOCon.Execute sConsulta
                    End If
                Next
            Else
                'Actualizaci�n de textos
                
                'Obtener los textos existentes
                sConsulta = "SELECT ID,IDI,TEXTO,TIPO FROM TEXTOS_MULTIIDIOMA WITH (NOLOCK) WHERE ID=" & lngSubTextoFin
                Set rs = New adodb.Recordset
                rs.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
                
                For i = 0 To dcTextos.Count - 1
                    If rs.RecordCount > 0 Then
                        rs.MoveFirst
                        rs.Find ("IDI=" & StrToSQLNULL(dcTextos.Keys(i)))
                        If rs.eof Then
                            If dcTextos.Items(i) <> vbNullString Then
                                'No exist�a Texto en este idioma
                                sConsulta = "INSERT INTO TEXTOS_MULTIIDIOMA (ID,IDI,TEXTO,TIPO) VALUES (" & _
                                        lngSubTextoFin & "," & _
                                        StrToSQLNULL(dcTextos.Keys(i)) & "," & _
                                        StrToSQLNULL(dcTextos.Items(i)) & "," & _
                                        StrToSQLNULL(sTipo) & ")"
                                        
                                TESError.Arg1 = lngSubTextoFin
                            End If
                        Else
                            If dcTextos.Items(i) = vbNullString Then
                                'Borrado del registro
                                sConsulta = "DELETE FROM TEXTOS_MULTIIDIOMA " & _
                                        "WHERE ID=" & lngSubTextoFin & " AND IDI=" & StrToSQLNULL(dcTextos.Keys(i))
                            Else
                                sConsulta = "UPDATE TEXTOS_MULTIIDIOMA SET TEXTO=" & _
                                        StrToSQLNULL(dcTextos.Items(i)) & " " & _
                                        "WHERE ID=" & lngSubTextoFin & " AND IDI=" & StrToSQLNULL(dcTextos.Keys(i))
                            End If
                        End If
                                    
                        m_oConexion.ADOCon.Execute sConsulta
                    End If
                Next
            End If
        End If
    End If
    
Salir:
    Set rs = Nothing
    ActualizarTextosMultiidioma = TESError
    Exit Function
Error:
    TESError = basErrores.TratarError(m_oConexion.ADOCon.Errors)
    Resume Salir
End Function

Private Sub Class_Terminate()
    On Error GoTo Error:
        
    Set m_oConexion = Nothing
    
    Exit Sub
Error:
    Resume Next
End Sub

