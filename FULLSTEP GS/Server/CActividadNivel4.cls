VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CActividadNivel4"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'********************* CActividadNivel4 **********************************
'*             Autor : Javier Arana
'*             Creada : 18/11/98
'****************************************************************


Option Explicit
Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
    
End Enum

Private mvarIndice As Long 'Nos indica la posicion secuencial dentro de una Coleccion
Private mvarCod As String
Private mvarDen As String
Private mvarId As Long
Private mvarACN1 As Long
Private mvarACN2 As Long
Private mvarACN3 As Long
Private mvarCodACT1 As String
Private mvarDenACT1 As String
Private mvarCodACT2 As String
Private mvarDenACT2 As String
Private mvarCodACT3 As String
Private mvarDenACT3 As String
Private mvarNumProve As Integer
Private mvarActividadesNivel5 As CActividadesNivel5
Private mvarConexion As CConexion 'local copy
Private mvarProveedores As CProveedores
Public Property Get CodACT1() As String
    CodACT1 = mvarCodACT1
End Property
Public Property Get CodACT2() As String
    CodACT2 = mvarCodACT2
End Property
Public Property Get CodACT3() As String
    CodACT3 = mvarCodACT3
End Property
Public Property Let CodACT1(ByVal Ind As String)
    mvarCodACT1 = Ind
End Property
Public Property Let CodACT2(ByVal Ind As String)
    mvarCodACT2 = Ind
End Property
Public Property Let CodACT3(ByVal Ind As String)
    mvarCodACT3 = Ind
End Property
Public Property Get DenACT1() As String
    DenACT1 = mvarDenACT1
End Property
Public Property Get DenACT2() As String
    DenACT2 = mvarDenACT2
End Property
Public Property Get DenACT3() As String
    DenACT3 = mvarDenACT3
End Property
Public Property Let DenACT1(ByVal Ind As String)
    mvarDenACT1 = Ind
End Property
Public Property Let DenACT2(ByVal Ind As String)
    mvarDenACT2 = Ind
End Property
Public Property Let DenACT3(ByVal Ind As String)
    mvarDenACT3 = Ind
End Property

Public Property Get numProve() As Integer
    numProve = mvarNumProve
End Property
Public Property Let numProve(ByVal Ind As Integer)
    mvarNumProve = Ind
End Property

Public Property Get ACN1() As Long
    ACN1 = mvarACN1
End Property
Public Property Let ACN1(ByVal Ind As Long)
    mvarACN1 = Ind
End Property
Public Property Get ACN2() As Long
    ACN2 = mvarACN2
End Property
Public Property Let ACN2(ByVal Ind As Long)
    mvarACN2 = Ind
End Property
Public Property Get ACN3() As Long
    ACN3 = mvarACN3
End Property
Public Property Let ACN3(ByVal Ind As Long)
    mvarACN3 = Ind
End Property

Public Property Get ID() As Long
    ID = mvarId
End Property
Public Property Let ID(ByVal Ind As Long)
    mvarId = Ind
End Property
Public Property Get Indice() As Long
    Indice = mvarIndice
End Property
Public Property Let Indice(ByVal Ind As Long)
    mvarIndice = Ind
End Property


Friend Property Set Conexion(ByVal vData As CConexion)
    Set mvarConexion = vData
End Property


Friend Property Get Conexion() As CConexion
    Set Conexion = mvarConexion
End Property

Public Property Set ActividadesNivel5(ByVal vData As CActividadesNivel5)
    Set mvarActividadesNivel5 = vData
End Property


Public Property Get ActividadesNivel5() As CActividadesNivel5
    Set ActividadesNivel5 = mvarActividadesNivel5
End Property

Public Property Let Den(ByVal vData As String)
    mvarDen = vData
End Property


Public Property Get Den() As String
    Den = mvarDen
End Property



Public Property Let Cod(ByVal vData As String)
    mvarCod = vData
End Property


Public Property Get Cod() As String
    Cod = mvarCod
End Property


Private Sub Class_Terminate()
    
    Set mvarConexion = Nothing

    
End Sub


