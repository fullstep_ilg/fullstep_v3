VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CMaterialesQA"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

''' Variables privadas con la informacion de un material qa
Private m_oConexion As CConexion
Private m_oConexionServer As CConexionDeUseServer

Private mCol As Collection

''' Control de errores
Private Enum TipoDeError
    ConexionNoEstablecida = 613
End Enum

Public Function Add(ByVal Id As String, ByVal Den As String, ByVal Baja As Integer, ByVal Idioma As String) As CMaterialQA
    Dim objnewmember As CMaterialQA
    Set objnewmember = New CMaterialQA
    
    objnewmember.Id = CInt(0 & Id)
    objnewmember.Den = Den
    objnewmember.Baja = Baja
    
    Set objnewmember.Conexion = m_oConexion
    Set objnewmember.ConexionServer = m_oConexionServer
    
    objnewmember.DameEstructuraMaterial (Id = ""), Idioma
    
    mCol.Add objnewmember, Id
    
    Set objnewmember = Nothing
End Function

Public Property Set Conexion(ByVal vData As CConexion)

    ''' * Objetivo: Dar valor a la conexion de la moneda
    ''' * Recibe: Conexion
    ''' * Devuelve: Nada

    Set m_oConexion = vData
    
End Property

Public Property Get Conexion() As CConexion

       ''' * Objetivo: Devolver la conexion de la moneda
       ''' * Recibe: Nada
       ''' * Devuelve: Conexion

       Set Conexion = m_oConexion
    
End Property

Private Sub Class_Initialize()
    Set mCol = New Collection
End Sub

Private Sub Class_Terminate()
''' * Objetivo: Limpiar la memoria
    Set mCol = Nothing
    Set m_oConexion = Nothing
    Set m_oConexionServer = Nothing
End Sub
''' Revisado por: Jbg. Fecha: 13/12/2011
''' <summary>
''' Cargar Todos Los Materiales de un tipo de certificado concreto
''' </summary>
''' <param name="Idioma">Idioma</param>
''' <param name="iId">tipo de certificado</param>
''' <remarks>Llamada desde: frmVarCalMaterial/CargarMaterialesQA ; Tiempo m�ximo: 0,2</remarks>
Public Sub CargarTodosLosMateriales(Optional ByVal Idioma As String = "SPA", Optional ByVal iID As Integer = 0)
    Dim rs As New adodb.Recordset
    Dim fldId As adodb.Field
    Dim fldDen As adodb.Field
    Dim fldBaja As adodb.Field
    Dim sConsulta As String
    
    '********* Precondicion **************************************
    If m_oConexion Is Nothing Then
        ERR.Raise vbObjectError + TipoDeError.ConexionNoEstablecida, "CDestinos.CargarTodosLosMateriales", "No se ha establecido la conexion"
        Exit Sub
    End If
    '*************************************************************
    
    If iID = 0 Then
        sConsulta = "SELECT ID,DEN_" & Idioma & ",BAJA FROM MATERIALES_QA WITH(NOLOCK) WHERE BAJA=0 ORDER BY ID ASC"
    Else
        sConsulta = "SELECT DISTINCT(MQ.ID),MQ.DEN_" & Idioma & ",MQ.BAJA FROM MATERIALES_QA MQ WITH(NOLOCK)"
        sConsulta = sConsulta & " INNER JOIN MATERIAL_QA_CERTIFICADOS MQC WITH(NOLOCK) ON MQ.ID=MQC.MATERIAL"
        sConsulta = sConsulta & " AND MQC.TIPO_CERTIFICADO=" & iID
        sConsulta = sConsulta & " WHERE MQ.BAJA=0 ORDER BY MQ.ID ASC"
    End If

    rs.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    
    Set mCol = Nothing
    Set mCol = New Collection
    
    If Not rs.eof Then
        Set fldId = rs.Fields("ID")
        Set fldDen = rs.Fields("DEN_" & Idioma)
        Set fldBaja = rs.Fields("BAJA")
                
        While Not rs.eof
            Me.Add CStr(fldId.Value), fldDen.Value, fldBaja.Value, Idioma
                    
            rs.MoveNext
        Wend
        
        Set fldId = Nothing
        Set fldDen = Nothing
        Set fldBaja = Nothing
    End If
    
    rs.Close
    Set rs = Nothing
End Sub

Public Property Get Item(vntIndexKey As Variant) As CMaterialQA
On Error GoTo NoSeEncuentra:

    Set Item = mCol(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property

Public Property Set ConexionServer(ByVal con As CConexionDeUseServer)
    Set m_oConexionServer = con
End Property

Public Property Get ConexionServer() As CConexionDeUseServer
    Set ConexionServer = m_oConexionServer
End Property

Public Property Get Count() As Long
    If mCol Is Nothing Then
        Count = 0
    Else
         Count = mCol.Count
    End If
End Property

Public Sub Remove(vntIndexKey As Variant)
    mCol.Remove vntIndexKey
End Sub

Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
     Set NewEnum = mCol.[_NewEnum]
End Property

Public Property Get Materiales() As Collection
    Set Materiales = mCol
End Property

''' <summary>Realiza una copia de la colecci�n y la devuelve</summary>
''' <remarks>Llamada desde: frmVarCalMaterial</remarks>

Public Function Clone() As CMaterialesQA
    Dim oNewCol As CMaterialesQA
    Dim oMatQA As CMaterialQA
    
    If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
    
    Set oNewCol = New CMaterialesQA
    Set oNewCol.Conexion = Me.Conexion
    
    For Each oMatQA In Me
        oNewCol.Add oMatQA.Id, oMatQA.Den, oMatQA.Baja, gParametrosInstalacion.gIdioma
    Next
        
Salir:
    Set Clone = oNewCol
    Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("Class Module", "CMaterialesQA", "Clone", ERR, Erl)
      Resume Salir
   End If
End Function

