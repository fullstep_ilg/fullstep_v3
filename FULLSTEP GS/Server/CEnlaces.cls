VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CEnlaces"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Enum TipoDeError
    
    ConexionNoEstablecida = 613
   
End Enum

Private m_Col As Collection
Private m_oConexion As CConexion
Private m_bEOF As Boolean

Public Property Get eof() As Boolean
    eof = m_bEOF
End Property
Friend Property Let eof(ByVal b As Boolean)
    m_bEOF = b
End Property

Public Property Get Item(vntIndexKey As Variant) As CEnlace
On Error GoTo NoSeEncuentra:

    Set Item = m_Col(vntIndexKey)
     
    Exit Property

NoSeEncuentra:
    Set Item = Nothing
    
End Property

Friend Property Set Conexion(ByVal con As CConexion)
Set m_oConexion = con
End Property

Friend Property Get Conexion() As CConexion
Set Conexion = m_oConexion
End Property

Public Property Get Count() As Long
    If m_Col Is Nothing Then
        Count = 0
    Else
         Count = m_Col.Count
    End If
End Property

Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
     Set NewEnum = m_Col.[_NewEnum]
End Property

Public Function Add(ByVal lId As Long, ByVal lBloqueOrigen As Long, ByVal lBloqueDestino As Long, Optional ByVal lAccion As Long = 0, Optional ByVal sFormula As String, Optional ByVal iBloquea As TipoBloqueoPMEnlace = NoBloquea, Optional ByVal dtFecAct As Variant, Optional ByVal vIndice As Variant, Optional ByVal oDenominaciones As CMultiidiomas = Nothing, Optional ByVal iTipoAccion As TipoAccionBloque = 0, Optional ByVal bLlamadaExterna As Boolean = False, Optional ByVal iEstadoFactura As Integer = 0) As CEnlace
    
    'create a new object
    Dim objnewmember As CEnlace
    
    Set objnewmember = New CEnlace
    With objnewmember
        Set .Conexion = m_oConexion
    
        .Id = lId
        .BloqueOrigen = lBloqueOrigen
        .BloqueDestino = lBloqueDestino
        .Accion = lAccion
        Set .DenAccion = oDenominaciones
        .TipoAccion = iTipoAccion
        .Formula = sFormula
        .Bloquea = iBloquea
        .FecAct = dtFecAct
        .LlamadaExterna = bLlamadaExterna
        .EstadoFactura = iEstadoFactura
    
        If Not IsMissing(vIndice) And Not IsNull(vIndice) Then
            .Indice = vIndice
            m_Col.Add objnewmember, CStr(vIndice)
        Else
            m_Col.Add objnewmember, CStr(lId)
        End If
    
    End With
    
    'return the object created
    Set Add = objnewmember
    Set objnewmember = Nothing


End Function

Public Sub AddEnlace(ByVal oEnlace As CEnlace, Optional ByVal vIndice As Variant)
    If Not IsMissing(vIndice) And Not IsNull(vIndice) Then
        oEnlace.Indice = vIndice
        m_Col.Add oEnlace, CStr(vIndice)
    Else
        m_Col.Add oEnlace, CStr(oEnlace.Id)
    End If
End Sub

Public Sub Remove(vntIndexKey As Variant)
On Error GoTo ERROR

    m_Col.Remove vntIndexKey

ERROR:

End Sub

Public Sub Clear()
    Dim i As Integer
    Dim Count As Integer
    Count = m_Col.Count
    For i = 1 To Count
        m_Col.Remove 1
    Next
End Sub


Private Sub Class_Initialize()
    Set m_Col = New Collection
End Sub


Private Sub Class_Terminate()
    'Proceroys collection when this class is terminated
    On Error Resume Next
    Set m_Col = Nothing
    Set m_oConexion = Nothing
    
End Sub

Public Function DevolverEnlaces(ByVal Idi As String, Optional ByVal lIdWF As Long = 0) As ADODB.Recordset
Dim oadorecordset As ADODB.Recordset
Dim sql As String
    
    sql = "SELECT E.ID, E.BLOQUE_ORIGEN, E.BLOQUE_DESTINO, E.ACCION, '" & DblQuote(Idi) & "' AS IDI, ISNULL(AD.DEN,'') AS DEN, A.TIPO, E.FORMULA, E.BLOQUEA, E.FECACT,A.LLAMADA_EXTERNA, E.EST_FACTURA FROM PM_ENLACE E WITH (NOLOCK) INNER JOIN PM_ACCIONES A WITH (NOLOCK) ON E.ACCION = A.ID LEFT JOIN PM_ACCIONES_DEN AD WITH (NOLOCK) ON AD.ACCION = A.ID AND AD.IDI = '" & DblQuote(Idi) & "' "
    If lIdWF > 0 Then
        sql = sql & " WHERE E.BLOQUE_ORIGEN IN (SELECT ID FROM PM_BLOQUE WITH (NOLOCK) WHERE WORKFLOW = " & lIdWF & ")"
    End If
        
    Set oadorecordset = New ADODB.Recordset
    oadorecordset.CursorLocation = adUseClient
    oadorecordset.Open sql, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    oadorecordset.ActiveConnection = Nothing
        
    Set DevolverEnlaces = oadorecordset
    
End Function

Public Sub CargarEnlaces(ByVal Idi As String, Optional ByVal lIdWF As Long = 0)
    Dim oRS As ADODB.Recordset
    Dim oDenominaciones As CMultiidiomas
    Dim oEnlace As CEnlace
    
    Set oRS = DevolverEnlaces(Idi, lIdWF)
    While Not oRS.eof
        Set oDenominaciones = New CMultiidiomas
        Set oDenominaciones.Conexion = m_oConexion
        oDenominaciones.Add oRS("IDI").Value, oRS("DEN").Value
        Set oEnlace = Add(NullToDbl0(oRS("ID").Value), NullToDbl0(oRS("BLOQUE_ORIGEN").Value), NullToDbl0(oRS("BLOQUE_DESTINO").Value), NullToDbl0(oRS("ACCION").Value), NullToStr(oRS("FORMULA").Value), NullToDbl0(oRS("BLOQUEA").Value), oRS("FECACT").Value, , oDenominaciones, NullToDbl0(oRS("TIPO").Value), NullToDbl0(oRS("LLAMADA_EXTERNA").Value) > 0, NullToDbl0(oRS("EST_FACTURA").Value))
        oEnlace.CargarExtraPoints
        oRS.MoveNext
    Wend
    oRS.Close
    Set oEnlace = Nothing
    Set oRS = Nothing
End Sub

Public Sub CargarEnlacesInstancia(ByVal Idi As String, ByVal lIdWorkflow As Long)
    Dim oRS As ADODB.Recordset
    Dim sql As String
    Dim oDenominaciones As CMultiidiomas
    Dim oEnlace As CEnlace
    
    sql = "SELECT CE.ID, CE.BLOQUE_ORIGEN, CE.BLOQUE_DESTINO, CE.ACCION, '" & DblQuote(Idi) & "' AS IDI, ISNULL(CAD.DEN,'') AS DEN, CA.TIPO, CE.FORMULA, CE.BLOQUEA, CE.FECACT FROM PM_COPIA_ENLACE CE WITH (NOLOCK) INNER JOIN PM_COPIA_ACCIONES CA WITH (NOLOCK) ON CE.ACCION = CA.ID LEFT JOIN PM_COPIA_ACCIONES_DEN CAD WITH (NOLOCK) ON CAD.ACCION = CA.ID AND CAD.IDI = '" & DblQuote(Idi) & "' " & _
          "WHERE CE.BLOQUE_ORIGEN IN (SELECT ID FROM PM_COPIA_BLOQUE WITH (NOLOCK) WHERE WORKFLOW = " & lIdWorkflow & ")"
            
    Set oRS = New ADODB.Recordset
    oRS.CursorLocation = adUseClient
    oRS.Open sql, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    oRS.ActiveConnection = Nothing
    While Not oRS.eof
        Set oDenominaciones = New CMultiidiomas
        Set oDenominaciones.Conexion = m_oConexion
        oDenominaciones.Add oRS("IDI").Value, oRS("DEN").Value
        Set oEnlace = Add(NullToDbl0(oRS("ID").Value), NullToDbl0(oRS("BLOQUE_ORIGEN").Value), NullToDbl0(oRS("BLOQUE_DESTINO").Value), NullToDbl0(oRS("ACCION").Value), NullToStr(oRS("FORMULA").Value), NullToDbl0(oRS("BLOQUEA").Value), oRS("FECACT").Value, , oDenominaciones, NullToDbl0(oRS("TIPO").Value))
        oEnlace.CargarExtraPointsInstancia
        oRS.MoveNext
    Wend
    oRS.Close
    Set oEnlace = Nothing
    Set oRS = Nothing
End Sub


Public Sub CargarEnlacesEntrantesABloque(ByVal lIdBloqueDestino As Long)
    Dim oRS As ADODB.Recordset
    Dim sql As String
    Dim oEnlace As CEnlace
    
    sql = "SELECT ID, BLOQUE_ORIGEN, BLOQUE_DESTINO, ACCION, FORMULA, BLOQUEA, FECACT FROM PM_ENLACE WITH (NOLOCK) " & _
            "WHERE BLOQUE_DESTINO = " & lIdBloqueDestino
            
    Set oRS = New ADODB.Recordset
    oRS.CursorLocation = adUseClient
    oRS.Open sql, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    oRS.ActiveConnection = Nothing
    While Not oRS.eof
        Set oEnlace = Add(NullToDbl0(oRS("ID").Value), NullToDbl0(oRS("BLOQUE_ORIGEN").Value), NullToDbl0(oRS("BLOQUE_DESTINO").Value), NullToDbl0(oRS("ACCION").Value), NullToStr(oRS("FORMULA").Value), NullToDbl0(oRS("BLOQUEA").Value), oRS("FECACT").Value)
        oEnlace.CargarExtraPoints
        oRS.MoveNext
    Wend
    oRS.Close
    Set oEnlace = Nothing
    Set oRS = Nothing
End Sub


