VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 3  'UsesTransaction
END
Attribute VB_Name = "CERPInt"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit



''' Variables privadas con la informacion de un ERP
Private m_vCod As Variant
Private m_vDen As Variant
Private m_vTipo As Variant
Private m_vMapper As Variant

''' Conexion
Private m_oConexion As CConexion

''' Control de errores
Private Enum TipoDeError
    ConexionNoEstablecida = 613
End Enum

''' Recordset para el Pago a editar
Private m_adores As adodb.Recordset

''' Indice de la Pago en la coleccion

Private m_lIndice As Long

Public Property Let Cod(ByVal vData As Variant)

    ''' * Objetivo: Dar valor a la variable privada Cod
    ''' * Recibe: Valor del c�digo del ERP
    ''' * Devuelve: Nada

    m_vCod = vData
    
End Property
Public Property Get Cod() As Variant

    ''' * Objetivo: Devolver la variable privada Cod
    ''' * Recibe: Nada
    ''' * Devuelve: El Valor del c�digo del ERP
    Cod = m_vCod
    
End Property
Public Property Let Tipo(ByVal vData As Variant)

    ''' * Objetivo: Dar valor a la variable privada Tipo
    ''' * Recibe: El valor del tipo para el ERP
    ''' * Devuelve: Nada

    m_vTipo = vData
    
End Property
Public Property Get Tipo() As Variant

    ''' * Objetivo: Devolver la variable privada Tipo
    ''' * Recibe: Nada
    ''' * Devuelve: El valor del tipo para el ERP
    Tipo = m_vTipo
    
End Property
Public Property Let Den(ByVal vData As Variant)

    ''' * Objetivo: Dar valor a la variable privada Den
    ''' * Recibe: El valor de la denominaci�n del ERP
    ''' * Devuelve: Nada

    m_vDen = vData
    
End Property
Public Property Get Den() As Variant

    ''' * Objetivo: Devolver la variable privada Den
    ''' * Recibe: Nada
    ''' * Devuelve: El valor de la denominaci�n del ERP
    Den = m_vDen
    
End Property


Public Property Let Mapper(ByVal vData As Variant)

    ''' * Objetivo: Dar valor a la variable privada Mapper
    ''' * Recibe: El nombre de la dll de la Mapper
    ''' * Devuelve: Nada

    m_vMapper = vData
    
End Property
Public Property Get Mapper() As Variant

    ''' * Objetivo: Devolver la variable privada Mapper
    ''' * Recibe: Nada
    ''' * Devuelve: El nombre de la dll de la Mapper
    Mapper = m_vMapper
    
End Property

Public Property Get Indice() As Integer

    ''' * Objetivo: Devolver el indice de la Pago en la coleccion
    ''' * Recibe: Nada
    ''' * Devuelve: Indice de la Pago en la coleccion

    Indice = m_lIndice
    
End Property
Public Property Let Indice(ByVal iInd As Integer)

    ''' * Objetivo: Dar valor al indice de la Pago en la coleccion
    ''' * Recibe: Indice de la Pago en la coleccion
    ''' * Devuelve: Nada

    m_lIndice = iInd
    
End Property
Friend Property Set Conexion(ByVal vData As CConexion)

    ''' * Objetivo: Dar valor a la conexion de la Pago
    ''' * Recibe: Conexion
    ''' * Devuelve: Nada

    Set m_oConexion = vData
    
End Property
Friend Property Get Conexion() As CConexion

    ''' * Objetivo: Devolver la conexion de la Pago
    ''' * Recibe: Nada
    ''' * Devuelve: Conexion

    Set Conexion = m_oConexion
    
End Property

Private Sub Class_Terminate()

    ''' * Objetivo: Limpiar la memoria
    
    Set m_oConexion = Nothing
    
End Sub

'''<summary>Funci�n que obtiene el nombre de la mapper a partir de la empresa o de la organizaci�n de compras del erp</summary>
'''<param name="sOrgCompras">C�digo de la organizaci�n de compras</param>
'''<param name="sEmpresa">ID de la empresa</param>
'''<returns>Nombre de la mapper</returns>
'''<remarks>Llamada desde frmPedidos --> ValidacionesERP</remarks>
''' <revision>ngo 16/01/2012</revision>
Public Function ObtenerMapper(ByVal sOrgCompras As Variant, ByVal sEmpresa As Variant) As String

Dim sConsulta As String
Dim AdoRes As New adodb.Recordset


If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    ObtenerMapper = "FSGSMapper" ''por defecto devuelve el nombre est�ndar
 
    If sOrgCompras <> "" Then
        sConsulta = "SELECT MAPPER FROM ERP E WITH (NOLOCK) INNER JOIN ERP_ORGCOMPRAS EO WITH (NOLOCK) ON EO.ERP=E.COD "
        sConsulta = sConsulta & " WHERE EO.ORGCOMPRAS = '" & sOrgCompras & "'"
    Else
        If sEmpresa = "" Then
            Exit Function
        Else
            sConsulta = "SELECT MAPPER FROM ERP E WITH (NOLOCK) INNER JOIN ERP_SOCIEDAD EO WITH (NOLOCK) ON EO.ERP=E.COD "
            sConsulta = sConsulta & " INNER JOIN EMP WITH(NOLOCK) ON EMP.SOCIEDAD=EO.SOCIEDAD"
            sConsulta = sConsulta & " WHERE EMP.ERP=1 AND EMP.ID = " & sEmpresa
        End If
    End If
    
    Set AdoRes = New adodb.Recordset
    AdoRes.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    
    If Not AdoRes.eof Then
        ObtenerMapper = NullToStr(AdoRes.Fields("MAPPER").Value)
    End If
    AdoRes.Close
    Set AdoRes = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CERPInt", "ObtenerMapper", ERR, Erl)
      Exit Function
   End If
    
End Function

'''<summary>Funci�n que obtiene el c�digo del ERP a partir del ID de la empresa o de la organizaci�n de compras del erp</summary>
'''<param name="sOrgCompras">C�digo de la organizaci�n de compras</param>
'''<param name="sEmpresa">ID de la empresa</param>
'''<returns>C�digo del ERP</returns>
'''<remarks>Llamada desde frmPedidos --> blnTieneERP,
''' frmSeguimiento--> MiraMapper, ComprobarValorAtributo, ComprobarAtributos, PasaMaperAutoFacturable
'''</remarks>
''' <revision>ngo 16/01/2012</revision>
Public Function ObtenerCodERP(ByVal sOrgCompras As Variant, ByVal sEmpresa As Variant) As String

Dim sConsulta As String
Dim AdoRes As New adodb.Recordset


If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    ObtenerCodERP = ""
    
    If sOrgCompras <> "" Then
        sConsulta = "SELECT COD FROM ERP E WITH (NOLOCK) INNER JOIN ERP_ORGCOMPRAS EO WITH (NOLOCK) ON EO.ERP=E.COD "
        sConsulta = sConsulta & " WHERE EO.ORGCOMPRAS = '" & sOrgCompras & "'"
    Else
        If sEmpresa = "" Then
            Exit Function
        Else
            sConsulta = "SELECT COD FROM ERP E WITH (NOLOCK) INNER JOIN ERP_SOCIEDAD EO WITH (NOLOCK) ON EO.ERP=E.COD "
            sConsulta = sConsulta & " INNER JOIN EMP WITH(NOLOCK) ON EMP.SOCIEDAD=EO.SOCIEDAD"
            sConsulta = sConsulta & " WHERE EMP.ERP=1 AND EMP.ID = " & sEmpresa
        End If
    End If

    
    Set AdoRes = New adodb.Recordset
    AdoRes.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    
    If Not AdoRes.eof Then
        ObtenerCodERP = AdoRes.Fields("COD").Value
    End If
    AdoRes.Close
    
    Set AdoRes = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CERPInt", "ObtenerCodERP", ERR, Erl)
      Exit Function
   End If
    
End Function

''' <summary>
''' Funci�n que se utiliza a la hora de validar una adjudicaci�n para obtener las mappers con validaciones de adjudicaciones
''' </summary>
''' <param name=""></param>
''' <returns>Recordset con los nombres de las mappers</returns>
''' <remarks>Llamada desde: frmADJ.Adjudicar,  frmRESREU.Adjudicar ; Tiempo m�ximo:0,1</remarks>
''' <remarks>Revisado por ngo</remarks>
Public Function ObtenerMapperValidADJ() As adodb.Recordset
    Dim sConsulta As String
    Dim AdoRes As New adodb.Recordset
    


If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sConsulta = " SELECT MAPPER FROM ERP WITH(NOLOCK) INNER JOIN PARGEN_INTEGRACION WITH(NOLOCK) ON PARGEN_INTEGRACION.ERP=ERP.COD "
    sConsulta = sConsulta & " WHERE VALIDAR_ADJ=1"
      
    Set AdoRes = New adodb.Recordset
    AdoRes.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    
    Set AdoRes.ActiveConnection = Nothing
    Set ObtenerMapperValidADJ = AdoRes

'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CERPInt", "ObtenerMapperValidADJ", ERR, Erl)
      Exit Function
   End If
End Function

'''<summary>Funci�n que obtiene la cantidad de atributos que hay en integracion</summary>
'''<param name="resultado">cantidad de atributos</param>
'''<returns>valor booleano</returns>
'''<remarks>Llamada desde frmPedidos --> cmdEmitir</remarks>
Public Function ObtenerAtribInt(ByVal sIdEmpresa As Variant) As Boolean

    Dim sConsulta As String
    Dim AdoRes As New adodb.Recordset
    Dim Resultado As Integer
    
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    ObtenerAtribInt = False
    
    
    sConsulta = "SELECT COUNT(IA.ID) AS C FROM INT_ATRIB IA WITH(NOLOCK) "
    sConsulta = sConsulta & " INNER JOIN TABLAS_INTEGRACION_ERP TIE WITH(NOLOCK) ON IA.ERP=TIE.ERP "
    sConsulta = sConsulta & " INNER JOIN ERP_SOCIEDAD ES WITH(NOLOCK) ON ES.ERP=IA.ERP "
    sConsulta = sConsulta & " INNER JOIN EMP E WITH(NOLOCK) ON E.SOCIEDAD=ES.SOCIEDAD "
    sConsulta = sConsulta & " WHERE E.ID=" & sIdEmpresa & " AND TIE.TABLA=" & PED_directo & " "
    sConsulta = sConsulta & " AND TIE.ACTIVA=1 AND TIE.SENTIDO IN (" & salida & "," & EntradaSalida & ") AND IA.PEDIDO=1"
    
    Set AdoRes = New adodb.Recordset
    AdoRes.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    
    If Not AdoRes.eof Then
        Resultado = AdoRes.Fields("C").Value
        If Resultado > 0 Then
            ObtenerAtribInt = True
        Else
            ObtenerAtribInt = False
        End If
    End If
    AdoRes.Close
    Set AdoRes = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CERPInt", "ObtenerAtribInt", ERR, Erl)
      Exit Function
   End If
    
End Function

'''<summary>Funci�n que obtiene el tipo de destino de la integraci�n.</summary>
'''<param name="iEntidad">La entidad a integrar.</param>
'''<param name="iErp">El ERP actual.</param>
'''<returns>Integer con el tipo de destino de la integraci�n.</returns>
'''<remarks>Llamada desde: </remarks>
Public Function ObtenerTipoInt(ByVal iEntidad As Integer, ByVal ierp As Integer) As Integer
    Dim sConsulta As String
    Dim AdoRes As New adodb.Recordset
    
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sConsulta = "SELECT DESTINO_TIPO FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK)"
    sConsulta = sConsulta & " WHERE ENTIDAD=" & iEntidad & "AND ACTIVA=1 AND ERP=" & ierp
    sConsulta = sConsulta & " AND SENTIDO IN (" & SentidoIntegracion.EntradaSalida & ", "
    sConsulta = sConsulta & SentidoIntegracion.salida & ")"
    
    Set AdoRes = New adodb.Recordset
    AdoRes.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    
    If Not AdoRes.eof Then
        ObtenerTipoInt = AdoRes.Fields("DESTINO_TIPO").Value
    Else
        ObtenerTipoInt = 0
    End If
    AdoRes.Close
    Set AdoRes = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CERPInt", "ObtenerTipoInt", ERR, Erl)
      Exit Function
   End If
    
End Function

'''<summary>Funci�n que obtiene el tpo de destino de la integraci�n �nicamente si su sentido es de salida.</summary>
'''<param name="iEntidad">La entidad a integrar.</param>
'''<param name="iErp">El ERP actual.</param>
'''<returns>Integer con el tipo de destino de la integraci�n.</returns>
'''<remarks>Llamada desde: COrdenesEntrega.EmitirPedidoConLineasTemporales,
'''COrdenesEntrega.BucleEmitirPedidoConLineasTemporales</remarks>
Public Function ObtenerTipoIntSentidoSalida(ByVal iEntidad As Integer, ByVal ierp As Integer) As Integer
    Dim sConsulta As String
    Dim AdoRes As New adodb.Recordset
    
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sConsulta = "SELECT DESTINO_TIPO FROM TABLAS_INTEGRACION_ERP WITH(NOLOCK)"
    sConsulta = sConsulta & " WHERE TABLA=" & iEntidad & "AND ACTIVA=1 AND ERP=" & ierp
    sConsulta = sConsulta & " AND SENTIDO=" & SentidoIntegracion.salida
    
    Set AdoRes = New adodb.Recordset
    AdoRes.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    
    If Not AdoRes.eof Then
        ObtenerTipoIntSentidoSalida = AdoRes.Fields("ORIGEN_TIPO").Value
    Else
        ObtenerTipoIntSentidoSalida = 0
    End If
    AdoRes.Close
    Set AdoRes = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CERPInt", "ObtenerTipoIntSentidoSalida", ERR, Erl)
      Exit Function
   End If
End Function


Public Function IntPedTrasAcepActivado(ByVal ierp As Integer) As Boolean
    Dim sConsulta As String
    Dim AdoRes As New adodb.Recordset
    
    
If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
'----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
If g_oErrores.fg_bProgramando Then On Error GoTo Error_Cls:
        sConsulta = "SELECT INT_PED_TRAS_ACEP FROM PARGEN_INTEGRACION WITH(NOLOCK) "
        sConsulta = sConsulta & "WHERE ERP=" & ierp
        
        Set AdoRes = New adodb.Recordset
        AdoRes.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
        
        If Not AdoRes.eof Then
            IntPedTrasAcepActivado = SQLBinaryToBoolean(AdoRes.Fields("INT_PED_TRAS_ACEP").Value)
        Else
        'Por defecto es 0.
            IntPedTrasAcepActivado = False
        End If
        AdoRes.Close
        Set AdoRes = Nothing
        
        
        Exit Function
    
Error_Cls:
        IntPedTrasAcepActivado = False
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CERPInt", "IntPedTrasAcepActivado", ERR, Erl)
      GoTo Error_Cls
      Exit Function
   End If
    
End Function

Public Sub ObtenerParametrosWCF(ByVal iEntidadIntegracion As Integer, ByVal sERP As String, _
        ByRef iServiceBindingType As Integer, ByRef iServiceSecurityMode As Integer, _
        ByRef iClientCredentialType As Integer, ByRef iProxyCredentialType As Integer, _
        ByRef sUserName As String, ByRef sUserPassword As String)
    Dim sConsulta As String
    Dim AdoRes As New adodb.Recordset
    Dim dFechaPassword As String
    If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
   
    '----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
    sConsulta = "SELECT SERVICEBINDINGTYPE,SERVICESECURITYMODE,SERVICECLIENTCREDENTIALTYPE,SERVICEPROXYCREDENTIALTYPE,TIE.WCF_ORIG_USU,TIE.WCF_ORIG_PWD,TIE.WCF_ORIG_FECPWD "
    sConsulta = sConsulta & "FROM PARGEN_INTEGRACION P WITH(NOLOCK) "
    sConsulta = sConsulta & "INNER JOIN TABLAS_INTEGRACION_ERP TIE WITH(NOLOCK) ON TIE.ERP=P.ERP "
    sConsulta = sConsulta & "WHERE TIE.ERP=" & sERP & " AND TIE.TABLA=" & iEntidadIntegracion
    AdoRes.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    If Not AdoRes.eof Then
        iServiceBindingType = IIf(IsNull(AdoRes.Fields("SERVICEBINDINGTYPE")), "", AdoRes.Fields("SERVICEBINDINGTYPE").Value)
        iServiceSecurityMode = IIf(IsNull(AdoRes.Fields("SERVICESECURITYMODE")), "", AdoRes.Fields("SERVICESECURITYMODE").Value)
        iClientCredentialType = IIf(IsNull(AdoRes.Fields("SERVICECLIENTCREDENTIALTYPE")), "", AdoRes.Fields("SERVICECLIENTCREDENTIALTYPE").Value)
        iProxyCredentialType = IIf(IsNull(AdoRes.Fields("SERVICEPROXYCREDENTIALTYPE")), "", AdoRes.Fields("SERVICEPROXYCREDENTIALTYPE").Value)
        sUserName = IIf(IsNull(AdoRes.Fields("WCF_ORIG_USU")), "", AdoRes.Fields("WCF_ORIG_USU").Value)
        dFechaPassword = IIf(IsNull(AdoRes.Fields("WCF_ORIG_FECPWD")), "", AdoRes.Fields("WCF_ORIG_FECPWD").Value)
        sUserPassword = IIf(IsNull(AdoRes.Fields("WCF_ORIG_PWD")), "", EncriptarAES(sUserName, NullToStr(AdoRes.Fields("WCF_ORIG_PWD").Value), False, dFechaPassword, 1, TipoDeUsuario.Persona))
    End If
    AdoRes.Close
    Set AdoRes = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CERPInt", "ObtenerParametrosWCF", ERR, Erl)
      Exit Sub
   End If
End Sub

Public Function ObtenerDatosConexionWS(ByVal iEntidadIntegracion As Integer, _
        ByRef iServiceBindingType As Integer, ByRef iServiceSecurityMode As Integer, _
        ByRef iClientCredentialType As Integer, ByRef iProxyCredentialType As Integer, _
        ByRef sUserName As String, ByRef sUserPassword As String, ByRef strRutaServicio As String, Optional ByVal sERP As String = "") As Boolean
    Dim sConsulta As String
    Dim AdoRes As New adodb.Recordset
    Dim dFechaPassword As String
    If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
    
    ObtenerDatosConexionWS = False
      
   sConsulta = "SELECT RUTAXBAP FROM PARGEN_RUTAS WITH(NOLOCK) WHERE NOMBREPROY='FullstepIS'"
   AdoRes.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    If Not AdoRes.eof Then
        strRutaServicio = NullToStr(AdoRes.Fields("RUTAXBAP").Value)
    End If
   AdoRes.Close
   
   If strRutaServicio <> "" Then
        '----------INICIO-TRATAMIENTO-DE-ERRORES--------------------------------------------------------------------
        sConsulta = "SELECT TOP 1 SERVICEBINDINGTYPE,SERVICESECURITYMODE,SERVICECLIENTCREDENTIALTYPE,SERVICEPROXYCREDENTIALTYPE,TIE.WCF_ORIG_USU,TIE.WCF_ORIG_PWD,TIE.WCF_ORIG_FECPWD "
        sConsulta = sConsulta & "FROM PARGEN_INTEGRACION P WITH(NOLOCK) "
        sConsulta = sConsulta & "INNER JOIN TABLAS_INTEGRACION_ERP TIE WITH(NOLOCK) ON TIE.ERP=P.ERP "
        sConsulta = sConsulta & "WHERE TIE.Tabla = " & iEntidadIntegracion & " AND (TIE.ORIGEN_TIPO =" & TipoDestinoIntegracion.WCF & " OR TIE.DESTINO_TIPO=" & TipoDestinoIntegracion.WCF & ")"
        
        If sERP <> "" Then
            sConsulta = sConsulta & " TIE.erp = " & sERP
        End If
        
        AdoRes.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
        If Not AdoRes.eof Then
            iServiceBindingType = IIf(IsNull(AdoRes.Fields("SERVICEBINDINGTYPE")), "", AdoRes.Fields("SERVICEBINDINGTYPE").Value)
            iServiceSecurityMode = IIf(IsNull(AdoRes.Fields("SERVICESECURITYMODE")), "", AdoRes.Fields("SERVICESECURITYMODE").Value)
            iClientCredentialType = IIf(IsNull(AdoRes.Fields("SERVICECLIENTCREDENTIALTYPE")), "", AdoRes.Fields("SERVICECLIENTCREDENTIALTYPE").Value)
            iProxyCredentialType = IIf(IsNull(AdoRes.Fields("SERVICEPROXYCREDENTIALTYPE")), "", AdoRes.Fields("SERVICEPROXYCREDENTIALTYPE").Value)
            sUserName = IIf(IsNull(AdoRes.Fields("WCF_ORIG_USU")), "", AdoRes.Fields("WCF_ORIG_USU").Value)
            dFechaPassword = IIf(IsNull(AdoRes.Fields("WCF_ORIG_FECPWD")), "", AdoRes.Fields("WCF_ORIG_FECPWD").Value)
            sUserPassword = IIf(IsNull(AdoRes.Fields("WCF_ORIG_PWD")), "", EncriptarAES(sUserName, NullToStr(AdoRes.Fields("WCF_ORIG_PWD").Value), False, dFechaPassword, 1, TipoDeUsuario.Persona))
            
            ObtenerDatosConexionWS = True
        End If
        AdoRes.Close
    End If
    
    Set AdoRes = Nothing
'----------FINAL-TRATAMIENTO-DE-ERRORES---------------------------------------------------------------------
 Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CERPInt", "ObtenerDatosConexionWS", ERR, Erl)
      Exit Function
   End If
End Function



Public Sub ObtenerIDsLogInstancia(ByVal lInstancia As Long, ByRef lIDLogInstancia As Long, ByRef sERP As String)
    Dim sConsulta As String
    Dim AdoRes As New adodb.Recordset
    
    If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
    sConsulta = "SELECT TOP 1 LI.ID, LI.ERP FROM LOG_GRAL AS LG WITH(NOLOCK) "
    sConsulta = sConsulta & "INNER JOIN LOG_INSTANCIA AS LI WITH(NOLOCK) "
    sConsulta = sConsulta & "ON LG.ID_TABLA=LI.ID AND LG.TABLA=" & EntidadIntegracion.SolicitudPM & " "
    sConsulta = sConsulta & "Where LI.Instancia= " & lInstancia & " ORDER BY LG.ID DESC"
    AdoRes.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    If Not AdoRes.eof Then
        lIDLogInstancia = IIf(IsNull(AdoRes.Fields("ID")), "", AdoRes.Fields("ID").Value)
        sERP = IIf(IsNull(AdoRes.Fields("ERP")), "", AdoRes.Fields("ERP").Value)
    End If
    AdoRes.Close
    Set AdoRes = Nothing
    
    Exit Sub
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CERPInt", "ObtenerIDsLogInstancia", ERR, Erl)
      Exit Sub
   End If
End Sub

Public Function ObtenerIdBorradoADJ(ByVal iAnyo As Integer, ByVal sGMN1 As String, ByVal lProce As Long, Optional ByRef sERP As String = "") As Long
    Dim sConsulta As String
    Dim AdoRes As New adodb.Recordset
    Dim lId As Long
    
    If Not g_oErrores.fg_bProgramando Then On Error GoTo ERROR
     sConsulta = "SELECT TOP 1 ID_TABLA, LG.ERP FROM LOG_GRAL AS LG WITH(NOLOCK) INNER JOIN LOG_ITEM_ADJ AS LIA WITH(NOLOCK) "
    sConsulta = sConsulta & "ON LG.ID_TABLA=LIA.ID AND LG.TABLA=" & EntidadIntegracion.Adj & " WHERE LG.ESTADO=" & EstadoIntegracion.PendienteDeTratar & " "
     sConsulta = sConsulta & "AND LG.ORIGEN IN (" & OrigenIntegracion.FSGSInt & ", " & OrigenIntegracion.FSGSIntReg & ")"
    If sERP <> "" Then
        sConsulta = sConsulta & " AND LG.ERP=" & sERP
    End If
    sConsulta = sConsulta & " AND LIA.ACCION='" & Accion_Baja & "' AND LIA.ANYO=" & iAnyo & " AND LIA.GMN1_PROCE=" & StrToSQLNULL(sGMN1) & " AND LIA.PROCE=" & lProce
    AdoRes.Open sConsulta, m_oConexion.ADOCon, adOpenForwardOnly, adLockReadOnly
    If Not AdoRes.eof Then
        lId = IIf(IsNull(AdoRes.Fields("ID_TABLA")), 0, AdoRes.Fields("ID_TABLA").Value)
        sERP = IIf(IsNull(AdoRes.Fields("ERP")), "", AdoRes.Fields("ERP").Value)
    End If
    AdoRes.Close
    Set AdoRes = Nothing
    
    ObtenerIdBorradoADJ = lId
    Exit Function
ERROR:
   If ERR.Number <> 0 Then
      Call g_oErrores.TratarError("M�dulo de clase", "CERPInt", "ObtenerIdBorradoADJ", ERR, Erl)
      Exit Function
   End If
End Function

